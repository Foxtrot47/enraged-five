#if !__TOOL && __BANK && !__SPU
#include "atl/string.h"
#include "grcore/debugdraw.h"
#include "grcore/setup.h"
#include "system/nelem.h"
#include "vector/colors.h"
#include "vectormath/classes.h"

#include "fwscene/scan/RenderPhaseCullShape.h"
#include "fwscene/scan/ScanNodes.h"
#include "fwscene/scan/ScanNodesDebug.h"
#include "fwscene/scan/ScanDebug.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwscene/world/InteriorSceneGraphNode.h"
#include "fwscene/world/SceneGraphVisitor.h"
#include "data/callback.h"
#include "fwmaths/vectorutil.h"
#include "fwscene/scan/ScanCascadeShadows.h"



extern rage::Vec4V g_screenQuadStorage[ rage::SCAN_SCREENQUAD_STORAGE_COUNT ][ rage::fwScanNodes::STORED_SCREEN_QUAD_COUNT ];

namespace rage {

static u8					g_debugDrawShadowPortalFlags[(fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE + 7)/8];
static fwPortalCorners		g_debugDrawShadowPortals[fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE];
static int					g_debugDrawShadowPortalIndices[fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE];
static int					g_debugDrawShadowPortalCount = 0;
static int					g_debugDrawShadowIsolatedPortalIndex = -1;
int							g_debugDrawGlobalIsolatedPortalIndex = -1;
static int					g_debugCullShapeIndex = 0;
static int					g_debugRoomIndex = -1;
static int					g_interiorIndex = 0;
static float				g_frustumAlpha = 0.8f;
static float				g_frustumFarPlaneScale = 1.0f;
static bool					g_drawDebugFrustum0 = true;
static bool					g_drawDebugFrustum1 = true;
static bool					gBreakOnSelectedEntityInContainer;
static fwSceneGraphNode*	gpSceneGraphBreakNode;
static datCallback			gBreakOnSelectedEntityInContainerCallback;
static bool					g_portalStraddlingEnabled = true;
static bool					g_displayPortalStraddlingDebugDraw = false;
static bool				    g_drawUsingPlaneSetDebugDraw = false;
int							g_CullShapeToBreakOn = 12;
static bool					g_debugDrawShadowPlanes = false;


struct CDebugFrustumInfo
{
	CDebugFrustumInfo() : m_compositeMatrix(V_IDENTITY), m_screenQuadData(V_ZERO), m_mirrorPlane(V_ZERO), m_cullshapeFlags(0), m_quadPair(false) {}
	Mat44V m_compositeMatrix;
	Vec4V  m_screenQuadData;
	Vec4V  m_mirrorPlane;
	u32    m_cullshapeFlags;
	bool   m_quadPair;
};

static CDebugFrustumInfo debugFrustumInfo[SCAN_RENDER_PHASE_COUNT];
static Vec4V debugTransposedCCSPlanes[CASCADE_SHADOWS_SCAN_NUM_PLANES];


bool *fwScanNodesDebug::GetBreakOnSelectedEntityInContainer()
{
	return &gBreakOnSelectedEntityInContainer;
}

void fwScanNodesDebug::SetSetGraphNodeToBreakOn(fwSceneGraphNode *pBreakNode)
{
	gpSceneGraphBreakNode = pBreakNode;
}

fwSceneGraphNode *fwScanNodesDebug::GetSetGraphNodeToBreakOn()
{
	return gpSceneGraphBreakNode;
}

void fwScanNodesDebug::SetBreakOnSelectedEntityInContainerCallback(const datCallback& callback)
{
	gBreakOnSelectedEntityInContainerCallback = callback;
}
const datCallback& fwScanNodesDebug::GetBreakOnSelectedEntityInContainerCallback()
{
	return gBreakOnSelectedEntityInContainerCallback;
}


void fwScanNodesDebug::VisibilityBegin(fwScanNodes* scanNodes)
{
	m_scanNodes = scanNodes;

	memset(g_debugDrawShadowPortalFlags, 0, sizeof(g_debugDrawShadowPortalFlags));
	g_debugDrawShadowPortalCount = 0;
}

void fwScanNodesDebug::VisibilityEnd()
{
	const fwScanResults* scanResults = m_scanNodes->m_scanResults;

	if ( scanResults->GetMirrorVisible() )
		DebugDrawPortal( m_scanNodes->m_debugMirrorRoom, m_scanNodes->m_debugMirrorPortal, &scanResults->GetMirrorCorners(), m_scanNodes->m_gbufCullShape, true );

	// TODO -- not sure this works, need to transform it onto the shadow map?
	if ( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_FINAL_SCREENQUADS )
		scanResults->GetVisibleRoomsShadowScreenQuad().DebugDraw( Color_red );
}

const fwPortalCorners* fwScanNodesDebug::DebugDrawGetShadowPortals()
{
	return g_debugDrawShadowPortals;
}

const int* fwScanNodesDebug::DebugDrawGetShadowPortalIndices()
{
	return g_debugDrawShadowPortalIndices;
}

int *fwScanNodesDebug::DebugDrawShadowIsolatedPortalIndex()
{
	return &g_debugDrawShadowIsolatedPortalIndex;
}

int *fwScanNodesDebug::DebugDrawGlobalIsolatedPortalIndex()
{
	return &g_debugDrawGlobalIsolatedPortalIndex;
}

int *fwScanNodesDebug::DebugDrawCullShapeIdForPortalDebugging()
{
	return &g_CullShapeToBreakOn;
}

int fwScanNodesDebug::DebugDrawGetShadowPortalCount()
{
	return g_debugDrawShadowPortalCount;
}

void fwScanNodesDebug::DebugDrawPortal(const fwSceneGraphNode* currentSceneNode, const fwPortalSceneGraphNode* portalSceneNode, const fwPortalCorners* portalCorners, const fwRenderPhaseCullShape* cullShape, const bool solid, bool forceDraw)
{
	if ( !forceDraw && ( (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTALS_FROM_STARTING_NODE_ONLY ) && currentSceneNode != cullShape->GetStartingSceneNode()) )
		return;

	if ( forceDraw || ( (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTALS ) && ( cullShape->GetFlags() & CULL_SHAPE_FLAG_PRIMARY) ) )
	{
		Color32 baseColor = Color_yellow;

		if ( portalSceneNode->GetIsWaterSurface() )
			baseColor = Color_blue;
		else if ( portalSceneNode->GetIsMirrorFloor() )
			baseColor = Color_red;
		else if ( portalSceneNode->GetIsMirror() )
			baseColor = portalSceneNode->GetMirrorPriority() > 0 ? Color_magenta : Color_red;
		else if ( portalSceneNode->GetIsLinkPortal() )
			baseColor = Color_green;
		else if ( !portalSceneNode->IsEnabled() )
			baseColor = Color_cyan;

		if ( portalSceneNode->GetIsMirror() && portalSceneNode->GetMirrorCanSeeExteriorView() ) // mirror or water surface which can see the exterior
		{
			baseColor.SetRed((255 + baseColor.GetRed())/2);
			baseColor.SetGreen((255 + baseColor.GetGreen())/2);
			baseColor.SetBlue((255 + baseColor.GetBlue())/2);
		}

		if ( g_debugDrawGlobalIsolatedPortalIndex != -1 && g_debugDrawGlobalIsolatedPortalIndex != (int)portalSceneNode->GetIndex() )
			baseColor = baseColor.MultiplyAlpha(16);

		const Color32 colour = solid ? Color32(0) : baseColor;
		const Color32 filled = solid ? baseColor.MultiplyAlpha(32) : Color32(0);

		if (portalCorners == NULL)
			portalCorners = &portalSceneNode->GetCorners();

		portalCorners->DebugDraw2D( colour, filled, cullShape->GetCompositeMatrix() );

		if ( ( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTAL_INDICES ) || g_scanDebugShowPortalGraph != SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_NONE )
		{
			char text[64] = "";
			sprintf( text, "PORTAL_%d", (int)portalSceneNode->GetIndex() );
			grcDebugDraw::Text( portalCorners->GetCorner(0), Color32(255,255,0,baseColor.GetAlpha()), text );

			char oneWayText[64] = "";
			sprintf( oneWayText, "PORTAL %s", (int)portalSceneNode->GetIsOneWay() ? "OneWay" : "Not OneWay");
			grcDebugDraw::Text( portalCorners->GetCorner(3), Color32(255,255,0,baseColor.GetAlpha()), oneWayText );

			char lodOnlyText[64] = "";
			sprintf( lodOnlyText, "PORTAL %s", (int)portalSceneNode->GetRenderLodsOnly() ? "Lod Only" : "Not Lod Only");
			grcDebugDraw::Text( portalCorners->GetCorner(2), Color32(255,255,0,baseColor.GetAlpha()), lodOnlyText );
		}
		if  (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTAL_OPACITY )
		{
			char text[64] = "";
			sprintf(text, "OPACITY %.3f", portalSceneNode->GetOpacity());
			grcDebugDraw::Text( (portalCorners->GetCorner(0) + portalCorners->GetCorner(2)) * 0.5f, Color32(255,255,0,baseColor.GetAlpha()), text);

			char canClosetext[64] = "";
			sprintf(canClosetext, "Portal %s", portalSceneNode->IsEnabled() ? "Open" : "Closed");
			grcDebugDraw::Text( portalCorners->GetCorner(0), Color32(255,255,0,baseColor.GetAlpha()), canClosetext);

			char lodOnlyText[64] = "";
			sprintf( lodOnlyText, "PORTAL Closing %s", (int)portalSceneNode->GetAllowClosing() ? "Enabled" : "Disabled");
			grcDebugDraw::Text( portalCorners->GetCorner(2), Color32(255,255,0,baseColor.GetAlpha()), lodOnlyText );
		}
	}

	if ( ( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_PORTALS_IN_SHADOW_MAP ) && ( cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS ) )
	{
		if ( portalSceneNode->GetIsMirror() )
			return; // mirrors do no affect shadows

		// TODO -- save colour
		//const Color32 baseColor = portalSceneNode->IsEnabled() ? Color_yellow : Color_cyan;

		if (portalCorners == NULL)
			portalCorners = &portalSceneNode->GetCorners();

		const int portalNodeIndex = portalSceneNode->GetPortalNodeIndex();

		if ( AssertVerify ( portalNodeIndex >= 0 && portalNodeIndex <= fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE ) )
		{
			if ( ( g_debugDrawShadowPortalFlags[portalNodeIndex/8] & BIT(portalNodeIndex%8) ) == 0 )
			{
				g_debugDrawShadowPortalFlags[portalNodeIndex/8] |= BIT(portalNodeIndex%8);

				if ( AssertVerify( g_debugDrawShadowPortalCount < NELEM(g_debugDrawShadowPortals) ) )
				{
					g_debugDrawShadowPortals[g_debugDrawShadowPortalCount] = *portalCorners;
					g_debugDrawShadowPortalIndices[g_debugDrawShadowPortalCount] = portalNodeIndex;
					g_debugDrawShadowPortalCount++;
				}
			}
		}
	}
}

void fwScanNodesDebug::DebugDrawIntermediateScreenQuad(const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad)
{
	if ( !(cullShape->GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) )
		return;

	if ( (cullShape->GetScreenQuadStorageId() == SCAN_SCREENQUAD_STORAGE_PRIMARY) && (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PRIMARY_INTERMEDIATE_SCREENQUADS) )
		screenQuad.DebugDraw( Color_blue3 );

	if ( (cullShape->GetScreenQuadStorageId() == SCAN_SCREENQUAD_STORAGE_CASCADE_SHADOWS) && (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_INTERMEDIATE_SCREENQUADS) )
		screenQuad.DebugDraw( Color_blue3 );
}

void fwScanNodesDebug::DebugDrawFinalScreenQuad(const fwSceneGraphNode* sceneNode)
{
	const s16 storedScreenQuadIndex = fwScanNodes::GetStoredScreenQuadIndex( sceneNode );

	if ( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PRIMARY_FINAL_SCREENQUADS )
	{
		if ( m_scanNodes->m_gbufCullShape->GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
			fwScreenQuadPair( g_screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_PRIMARY ][ storedScreenQuadIndex ] ).DebugDraw( Color_green, Color_yellow );
		else
			fwScreenQuad( g_screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_PRIMARY ][ storedScreenQuadIndex ] ).DebugDraw( Color_green );
	}

	if ( ( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_FINAL_SCREENQUADS ) && m_scanNodes->m_cascadeShadowsCullShape )
	{
		if ( m_scanNodes->m_cascadeShadowsCullShape->GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
			fwScreenQuadPair( g_screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_CASCADE_SHADOWS ][ storedScreenQuadIndex ] ).DebugDraw( Color_green, Color_yellow );
		else
			fwScreenQuad( g_screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_CASCADE_SHADOWS ][ storedScreenQuadIndex ] ).DebugDraw( Color_green );
	}
}

bool fwScanNodesDebug::ShowPortalTraversalInfo(const fwRenderPhaseCullShape* cullShape, const fwSceneGraphNode* sceneNode, const fwPortalSceneGraphNode* portalSceneNode, const fwScreenQuad* portalScreenQuad, const char* info, int portalCount, int depth, bool bTraversePortal)
{
	bool bPortalTraversalDebugging = false;
	char name[256] = "";

	if ( portalSceneNode == NULL ) // check if portal traversal info is enabled ..
	{
		if ( g_scanDebugShowPortalGraph != SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_NONE )
		{
			if (( g_scanDebugShowPortalGraph == SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_PRIMARY    && (cullShape->GetFlags() & CULL_SHAPE_FLAG_PRIMARY        ) ) ||
				( g_scanDebugShowPortalGraph == SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_SHADOW     && (cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS) ) ||
				( g_scanDebugShowPortalGraph == SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_REFLECTION && (cullShape->GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS    ) ) ||
				( g_scanDebugShowPortalGraph == SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_MIRROR     && (cullShape->GetFlags() & CULL_SHAPE_FLAG_MIRROR         ) ) )
			{
				if ( fwSceneGraphNode::sm_GetSceneGraphNodeDebugNameFunc )
				{
					fwSceneGraphNode::sm_GetSceneGraphNodeDebugNameFunc( name, sceneNode, NULL, true, false );
					bPortalTraversalDebugging = true;
				}
			}
		}
	}
	else // .. it must be enabled, or we wouldn't get here
	{
		fwSceneGraphNode::sm_GetSceneGraphNodeDebugNameFunc( name, portalSceneNode, sceneNode, false, portalSceneNode->GetIsMirror() );

		if ( strcmp( name, "EXTERIOR" ) != 0 ) // for some reason i'm seeing a bunch of "EXTERIOR" in the hierarchy, skip these
			bPortalTraversalDebugging = true;
	}

	if (!bPortalTraversalDebugging)
		return false;

	if ( ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_PORTAL_TRAVERSAL_GRAPH_IGNORE_NON_TRAVERSED_PORTALS ) == 0 || bTraversePortal )
	{
		char temp[256] = "";

		for (int i = 0; i <= depth; i++) // indent
			strcat( temp, "      " );

		strcat( temp, name );

		if ( portalSceneNode == NULL )
			strcat( temp, atVarString( " (%d portals)", portalCount ).c_str() );
		else if ( info && info[0] != '\0' )
			strcat( temp, info );
		else if ( portalScreenQuad )
		{
			const float x0 = portalScreenQuad->Left().Getf();
			const float y0 = portalScreenQuad->Bottom().Getf();
			const float x1 = portalScreenQuad->Right().Getf();
			const float y1 = portalScreenQuad->Top().Getf();

			strcat( temp, atVarString( " - min=%.2f,%.2f, max=%.2f,%.2f", x0, y0, x1, y1 ).c_str() );
		}

		grcDebugDraw::TextFontPush(grcSetup::GetMiniFixedWidthFont());
		grcDebugDraw::AddDebugOutput( Color32(255,255,255,255), temp );
		grcDebugDraw::TextFontPop();
	}

	return true;
}

void fwScanNodesDebug::ShowDebugShadowIsolatedPortalIndex()
{
	if (g_debugDrawShadowIsolatedPortalIndex != -1)
	{
		g_debugDrawShadowIsolatedPortalIndex = Clamp(g_debugDrawShadowIsolatedPortalIndex, 0, g_debugDrawShadowPortalCount - 1);

		grcDebugDraw::AddDebugOutput(Color32(255,255,255,255), "Isolated shadow portal scene node index %d", g_debugDrawShadowPortalIndices[g_debugDrawShadowIsolatedPortalIndex]);
	}
}

void fwScanNodesDebug::SnapShotFrutumDataCallback() 
{
	g_scanDebugFlagsExtended |= SCAN_DEBUG_EXT_SNAPSHOT_FRUSTUM;
}

int *fwScanNodesDebug::GetDebugSnapShotCullShapeIndex()
{
	return &g_debugCullShapeIndex;
}

void fwScanNodesDebug::SnapShotDebugFrutumData(fwScanNodes *pScanNodes)
{
	fwSceneGraphNode *foundSceneNode = NULL;
	for(fwSceneGraphVisitor visitor(pScanNodes->m_rootSceneNode); !visitor.AtEnd(); visitor.Next())
	{
		fwSceneGraphNode *sceneNode = visitor.GetCurrent();
		if (g_debugRoomIndex == -1 && sceneNode->IsTypeExterior())
		{
			foundSceneNode = sceneNode;
			break;
		}
		else 
		{
			if (sceneNode->IsTypeRoom())
			{
				fwRoomSceneGraphNode *roomNode = static_cast<fwRoomSceneGraphNode*>(sceneNode);
				fwInteriorLocation location = roomNode->GetInteriorLocation();
				if (location.GetInteriorProxyIndex() == g_interiorIndex && location.GetRoomIndex() == g_debugRoomIndex)
				{
					foundSceneNode = sceneNode;
					break;
				}
			}
		}
	}

	if (foundSceneNode)
	{
		const fwRenderPhaseCullShape *cullShapes = pScanNodes->m_scanBaseInfo->m_cullShapes;
		u32 cullshapeCount = pScanNodes->m_scanBaseInfo->m_cullShapeCount;

		for (u32 i = 0; i < cullshapeCount; ++i)
		{
			debugFrustumInfo[i].m_compositeMatrix = cullShapes[i].GetCompositeMatrix();
			u32 cullShapeFlags = cullShapes[i].GetFlags();
			if ( cullShapeFlags & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS )
			{
				debugFrustumInfo[i].m_screenQuadData  = g_screenQuadStorage[cullShapes[i].GetScreenQuadStorageId()][fwScanNodes::GetStoredScreenQuadIndex(foundSceneNode)];
				debugFrustumInfo[i].m_quadPair = (cullShapeFlags & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS) != 0;
			}
			if (cullShapeFlags & CULL_SHAPE_FLAG_MIRROR)
			{
				debugFrustumInfo[i].m_mirrorPlane = cullShapes[i].GetMirrorPlane();
			}
			debugFrustumInfo[i].m_cullshapeFlags = cullShapeFlags;
		}
	}

	g_scanDebugFlagsExtended &= ~SCAN_DEBUG_EXT_SNAPSHOT_FRUSTUM;

	const Vec4V *ccsTransposedPlanes = pScanNodes->m_scanBaseInfo->m_cascadeShadowInfo.GetTransposedPlanesPtr();

	for (int i = 0; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; ++i)
	{
		debugTransposedCCSPlanes[i] = ccsTransposedPlanes[i];
	}
}

int* fwScanNodesDebug::GetDebugSnapShotRoomIndex()
{
	return &g_debugRoomIndex;
}

int* fwScanNodesDebug::GetDebugSnapShotInteriorIndex()
{
	return &g_interiorIndex;
}

void fwScanNodesDebug::DrawSnapShotFrustums()
{
	u32 searchFlag = 0;
	if (g_debugCullShapeIndex == 0)
	{
		searchFlag = CULL_SHAPE_FLAG_PRIMARY;
	}
	else if (g_debugCullShapeIndex == 1)
	{
		searchFlag = CULL_SHAPE_FLAG_CASCADE_SHADOWS;
	}
	else if (g_debugCullShapeIndex == 2)
	{
		searchFlag = CULL_SHAPE_FLAG_MIRROR;
	}
	else if (g_debugCullShapeIndex == 3)
	{
		searchFlag = CULL_SHAPE_FLAG_WATER_REFLECTION;
	}

	for (u32 i = 0; i < SCAN_RENDER_PHASE_COUNT; ++i)
	{
		if (debugFrustumInfo[i].m_cullshapeFlags & searchFlag)
		{
			if (debugFrustumInfo[i].m_quadPair)
			{
				fwScreenQuadPair quadPair(debugFrustumInfo[i].m_screenQuadData);
				if (quadPair.GetScreenQuad0().IsValid() && g_drawDebugFrustum0)
				{
					DrawFrustumForScreenQuad(quadPair.GetScreenQuad0(), debugFrustumInfo[i].m_compositeMatrix, Color_orange);
				}
				if (quadPair.GetScreenQuad1().IsValid() && g_drawDebugFrustum1)
				{
					DrawFrustumForScreenQuad(quadPair.GetScreenQuad1(), debugFrustumInfo[i].m_compositeMatrix, Color_DarkGreen);
				}
			}
			else
			{
				fwScreenQuad quad(debugFrustumInfo[i].m_screenQuadData);
				if (quad.IsValid())
				{
					DrawFrustumForScreenQuad(quad, debugFrustumInfo[i].m_compositeMatrix, Color_orange);
				}
				else
				{
					fwScreenQuad fullScreenQuad(SCREEN_QUAD_FULLSCREEN);
					if (searchFlag & CULL_SHAPE_FLAG_MIRROR)
					{
						float *pQ0 = (float*)(&fullScreenQuad);

						float temp = -pQ0[2];
						pQ0[2] = -pQ0[0];
						pQ0[0] = temp;
					}
					DrawFrustumForScreenQuad(fullScreenQuad, debugFrustumInfo[i].m_compositeMatrix, Color_orange);
					grcDebugDraw::Text(Vector2(40.0f, 40.0f), DD_ePCS_Pixels, Color_white, "Screen quad is invalid, drawing as if Fullscreen");
				}
			}
			break;
		}
	}

	if (g_debugDrawShadowPlanes)
	{
		Vec4V planes[CASCADE_SHADOWS_SCAN_NUM_PLANES];
	
		for (int i = 0; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i += 4)
		{
			Transpose4x4(planes[0 + i],
						 planes[1 + i],
						 planes[2 + i],
						 planes[3 + i],

						 debugTransposedCCSPlanes[0 + i],
						 debugTransposedCCSPlanes[1 + i],
						 debugTransposedCCSPlanes[2 + i],
						 debugTransposedCCSPlanes[3 + i]
						);
		}
		PlaneSetDebugDraw(planes, CASCADE_SHADOWS_SCAN_NUM_PLANES, Color32(255, 0, 0, 255));
	}
}
void fwScanNodesDebug::DrawFrustumForScreenQuad(const fwScreenQuad& quad_, Mat44V_In compositeMatrix, Color32 color)
{
	fwScreenQuad quad = quad_;
	quad.Clamp();

	color.SetAlpha(int(g_frustumAlpha * 255.0f + 0.5f));
	Mat44V invTransform;
	InvertFull( invTransform, compositeMatrix );

	const ScalarV	nearV( 0.01f );
	const ScalarV	farV(V_ONE);
	const ScalarV	oneV(V_ONE);

	Vec4V nearPoints[4] =
	{
		Vec4V( quad.Left(), quad.Bottom(), nearV, oneV ),
		Vec4V( quad.Left(), quad.Top(), nearV, oneV ),
		Vec4V( quad.Right(), quad.Top(), nearV, oneV ),
		Vec4V( quad.Right(), quad.Bottom(), nearV, oneV )
	};

	Vec4V farPoints[4] =
	{
		Vec4V( quad.Left(), quad.Bottom(), farV, oneV ),
		Vec4V( quad.Left(), quad.Top(), farV, oneV ),
		Vec4V( quad.Right(), quad.Top(), farV, oneV ),
		Vec4V( quad.Right(), quad.Bottom(), farV, oneV )
	};

	nearPoints[0] = Multiply( invTransform, nearPoints[0] );
	nearPoints[1] = Multiply( invTransform, nearPoints[1] );
	nearPoints[2] = Multiply( invTransform, nearPoints[2] );
	nearPoints[3] = Multiply( invTransform, nearPoints[3] );

	farPoints[0] = Multiply( invTransform, farPoints[0] );
	farPoints[1] = Multiply( invTransform, farPoints[1] );
	farPoints[2] = Multiply( invTransform, farPoints[2] );
	farPoints[3] = Multiply( invTransform, farPoints[3] );

	nearPoints[0] = nearPoints[0] * Invert( nearPoints[0] ).GetW();
	nearPoints[1] = nearPoints[1] * Invert( nearPoints[1] ).GetW();
	nearPoints[2] = nearPoints[2] * Invert( nearPoints[2] ).GetW();
	nearPoints[3] = nearPoints[3] * Invert( nearPoints[3] ).GetW();

	farPoints[0] = farPoints[0] * Invert( farPoints[0] ).GetW();
	farPoints[1] = farPoints[1] * Invert( farPoints[1] ).GetW();
	farPoints[2] = farPoints[2] * Invert( farPoints[2] ).GetW();
	farPoints[3] = farPoints[3] * Invert( farPoints[3] ).GetW();

	float spheresRadius = 0.05f;
 	if (g_drawUsingPlaneSetDebugDraw)
	{
		const int planeCount = 6;
		Vec4V planes[planeCount];

		const int bottomLeft  = 0;
		const int topLeft     = 1;
		const int topRight    = 2;
		const int bottomRight = 3;
// 		// Near
		planes[0] = BuildPlane(Vec3V(nearPoints[bottomRight].GetIntrin128()), 
							   Vec3V(nearPoints[bottomLeft].GetIntrin128()), 
							   Vec3V(nearPoints[topRight].GetIntrin128()));

		// Far
		planes[1] = BuildPlane(Vec3V(farPoints[bottomLeft].GetIntrin128()), 
							   Vec3V(farPoints[bottomRight].GetIntrin128()), 
							   Vec3V(farPoints[topLeft].GetIntrin128()));

		// Left 
		planes[2] = BuildPlane(Vec3V(nearPoints[bottomLeft].GetIntrin128()), 
							   Vec3V(farPoints[bottomLeft].GetIntrin128()), 
							   Vec3V(nearPoints[topLeft].GetIntrin128()));
 
		// Right
		planes[3] = BuildPlane(Vec3V(farPoints[bottomRight].GetIntrin128()), 
							   Vec3V(nearPoints[bottomRight].GetIntrin128()), 
							   Vec3V(farPoints[topRight].GetIntrin128()));

		// Top 
		planes[4] = BuildPlane(Vec3V(farPoints[topRight].GetIntrin128()), 
							   Vec3V(nearPoints[topRight].GetIntrin128()), 
							   Vec3V(farPoints[topLeft].GetIntrin128()));

		// Bottom 
		planes[5] = BuildPlane(Vec3V(nearPoints[bottomRight].GetIntrin128()), 
							   Vec3V(farPoints[bottomRight].GetIntrin128()), 
							   Vec3V(nearPoints[bottomLeft].GetIntrin128()));

		for (int i = 0; i < planeCount; ++i)
		{
			planes[i] =  -planes[i]; 
		}

		PlaneSetDebugDraw(planes, planeCount, Color32(0, 255, 0, 255)); 

	}
 	else
	{
		// Near
		grcDebugDraw::Quad(Vec3V(nearPoints[0].GetIntrin128()), 
						   Vec3V(nearPoints[1].GetIntrin128()), 
						   Vec3V(nearPoints[2].GetIntrin128()), 
						   Vec3V(nearPoints[3].GetIntrin128()), 
						   color, 
						   true);

		// Left
		Color32 leftColor(1.0f, 0.0f, 0.0f, g_frustumAlpha);
		grcDebugDraw::Quad(Vec3V(nearPoints[0].GetIntrin128()), 
						   Vec3V(nearPoints[1].GetIntrin128()),  
						   Vec3V(farPoints[1].GetIntrin128()),  
						   Vec3V(farPoints[0].GetIntrin128()),  
						   leftColor, 
						   true);
		// Right
		Color32 rightColor(0.0f, 0.0f, 1.0f, g_frustumAlpha);
		grcDebugDraw::Quad(Vec3V(nearPoints[3].GetIntrin128()), 
						   Vec3V(nearPoints[2].GetIntrin128()),  
						   Vec3V(farPoints[2].GetIntrin128()),  
						   Vec3V(farPoints[3].GetIntrin128()),  
						   rightColor, 
						   true);
		// Top
		Color32 topColor(1.0f, 1.0f, 0.0f, g_frustumAlpha);
		grcDebugDraw::Quad(Vec3V(nearPoints[1].GetIntrin128()), 
						   Vec3V(farPoints[1].GetIntrin128()),  
						   Vec3V(farPoints[2].GetIntrin128()),  
						   Vec3V(nearPoints[2].GetIntrin128()),  
						   topColor, 
						   true);
		// Bottom
		Color32 bottomColor(0.0f, 1.0f, 1.0f, g_frustumAlpha); 
		grcDebugDraw::Quad(Vec3V(nearPoints[0].GetIntrin128()), 
						   Vec3V(farPoints[0].GetIntrin128()),  
						   Vec3V(farPoints[3].GetIntrin128()),  
						   Vec3V(nearPoints[3].GetIntrin128()),  
						   bottomColor, 
						   true);
		// Far
		grcDebugDraw::Quad(Vec3V(farPoints[0].GetIntrin128()), 
						   Vec3V(farPoints[1].GetIntrin128()),  
						   Vec3V(farPoints[2].GetIntrin128()),  
						   Vec3V(farPoints[3].GetIntrin128()),  
						   color, true);

		grcDebugDraw::Sphere(Vec3V(nearPoints[0].GetIntrin128()), spheresRadius, leftColor);
		grcDebugDraw::Sphere(Vec3V(nearPoints[1].GetIntrin128()), spheresRadius, topColor);
		grcDebugDraw::Sphere(Vec3V(nearPoints[2].GetIntrin128()), spheresRadius, rightColor);
		grcDebugDraw::Sphere(Vec3V(nearPoints[3].GetIntrin128()), spheresRadius, bottomColor);


		grcDebugDraw::Sphere(Vec3V(farPoints[0].GetIntrin128()), spheresRadius, leftColor);
		grcDebugDraw::Sphere(Vec3V(farPoints[1].GetIntrin128()), spheresRadius, topColor);
		grcDebugDraw::Sphere(Vec3V(farPoints[2].GetIntrin128()), spheresRadius, rightColor);
		grcDebugDraw::Sphere(Vec3V(farPoints[3].GetIntrin128()), spheresRadius, bottomColor);
	}
}

float* fwScanNodesDebug::GetDrawFrustumForScreenQuadAlpha()
{
	return &g_frustumAlpha;
}

float* fwScanNodesDebug::GetDrawFrustumFarPlaneScale()
{
	return &g_frustumFarPlaneScale;
}

bool* fwScanNodesDebug::GetDrawFrustum0()
{
	return &g_drawDebugFrustum0;
}

bool* fwScanNodesDebug::GetDrawFrustum1()
{
	return &g_drawDebugFrustum1;
}

bool *fwScanNodesDebug::GetPortalStraddlingEnabled()
{
	return &g_portalStraddlingEnabled;
}
bool *fwScanNodesDebug::GetDisplayPortalStraddlingDebugDraw()
{
	return &g_displayPortalStraddlingDebugDraw;
}

bool *fwScanNodesDebug::GetDrawUsingPlaneSetDebugDraw()
{
	return &g_drawUsingPlaneSetDebugDraw;
}

bool *fwScanNodesDebug::GetDrawShadowPlanes()
{
	return &g_debugDrawShadowPlanes;
}

} // namespace rage

#endif // !__TOOL && __BANK && !__SPU
