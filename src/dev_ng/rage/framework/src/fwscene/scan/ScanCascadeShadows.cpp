// ===================================
// fwscene/scan/ScanCascadeShadows.cpp
// (c) 2010 RockstarNorth
// ===================================

#include "system/nelem.h"
#include "grcore/viewport.h"
#include "grcore/viewport_inline.h" // for box culler
#include "spatialdata/sphere.h"
#include "spatialdata/aabb.h"
#include "spatialdata/transposedplaneset.h"

#include "fwscene/scan/ScanCascadeShadows.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwscene/scan/VisibilityFlags.h"
#include "fwscene/scan/ScanDebug.h"
#include "fwscene/world/EntityDesc.h"
#include "fwrenderer/renderlistbuilder.h"
#include "fwmaths/vectorutil.h"

#if CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER
#include "atl/array.h"
#endif // CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER

namespace rage {

bank_u8 sGrassCascadeMask = (1<<(SUBPHASE_CASCADE_COUNT - 1)) - 1; //All but last cascade

#if !__SPU

fwScanCascadeShadowInfo::fwScanCascadeShadowInfo()
{
	m_viewToWorld             = Mat34V(V_ZERO);
	m_viewTanFOVZ             = Vec4V(V_ZERO);
	m_viewTanFOVShear         = Vec4V(V_ZERO);
	m_shadowToWorld33         = Mat33V(V_ZERO);
	m_recvrHeightMinMax       = Vec2V(V_ZERO);
	m_cascadeLodDistanceScale = Vec4V(V_ONE);

	for (int i = 0; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i++)
	{
		m_transposedPlanes[i] = Vec4V(V_ZERO);
	}

	m_forceVisibilityFlags = 0;
	m_forceNoBackfacingPlanes = false;
	m_useCascadeLodDistanceCulling = false;
	m_allowGrassCascadeCulling = true;
	m_alwaysCullGrass = false;
	m_bEnableShadowScreenSizeCheck = true;
#if __BANK
	m_UseAreaCulling = false;
#endif
}

void fwScanCascadeShadowInfo::SetCamera(Mat34V_In viewToWorld, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In z1, ScalarV_In camFOVThresh, 
										Mat33V_In shadowToWorld33, Mat33V_In worldToShadow33, const ScalarV recvrHeight[2], ScalarV_In shearX, ScalarV_In shearY)
{
	const ScalarV one(V_ONE);

	// camFOVThresh should be max(tanHFOV,tanVFOV)/(576*4) multiplied by a scaling factor
	m_viewToWorld       = viewToWorld;
	m_viewTanFOVZ       = Vec4V(tanHFOV*z1, tanVFOV*z1, -z1, camFOVThresh); // {tanHFOV*z1,tanVFOV*z1,-z1,thresh}
	m_viewTanFOVShear   = Vec4V(tanHFOV*(one - shearX), tanVFOV*(one - shearY), tanHFOV*(one + shearX), tanVFOV*(one + shearY))*z1;
	m_shadowToWorld33   = shadowToWorld33;
	m_worldToShadow33   = worldToShadow33;
	m_recvrHeightMinMax = Vec2V(recvrHeight[0], recvrHeight[1]);
#if __BANK
	m_isOrthographic    = false;
#endif
}

#if __BANK
void fwScanCascadeShadowInfo::SetCameraOrthographic(Mat34V_In viewToWorld, Vec3V_In boundsMin, Vec3V_In boundsMax, Mat33V_In shadowToWorld33)
{
	// assumes viewToWorld points straight down
	m_viewToWorld       = viewToWorld;
	m_viewTanFOVZ       = Vec4V(boundsMin, ScalarV(V_ZERO));
	m_viewTanFOVShear   = Vec4V(boundsMax, ScalarV(V_ZERO));
	m_shadowToWorld33   = shadowToWorld33;
	m_recvrHeightMinMax = Vec2V(V_ZERO);
	m_isOrthographic    = true;
}

void fwScanCascadeShadowInfo::AddWidgets(bkBank& bk)
{
	bk.PushGroup("Scan Cascade Shadow Info", false);
	{
		bk.AddToggle("Use Area Culling", &m_UseAreaCulling);
	}
}
#endif //__BANK

void fwScanCascadeShadowInfo::SetCascadeSpheres(const Vec4V spheres[CASCADE_SHADOWS_SCAN_NUM_CASCADES], Vec4V_In sphereThresholds, Vec4V_In boxThresholds)
{
	Transpose4x4(
		m_shadowSpheresX,
		m_shadowSpheresY,
		m_shadowSpheresZ,
		m_shadowSpheresR,
		spheres[0],
		spheres[1],
		spheres[2],
		spheres[3]
	);

	Vec3V mx, my, mz;
	Transpose3x3(mx, my, mz, m_shadowToWorld33.GetCol0(), m_shadowToWorld33.GetCol1(), m_shadowToWorld33.GetCol2());

	const Vec3V ax = CrossZAxis(mx); // {mx.y, -mx.x, 0}
	const Vec3V ay = CrossZAxis(my); // {my.y, -my.x, 0}
	const Vec3V az = CrossZAxis(mz); // {mz.y, -mz.x, 0}

	m_shadowBoxTestAX = Vec3V(ax.GetXY(), Dot(Abs(ax), Vec3V(V_ONE)));
	m_shadowBoxTestAY = Vec3V(ay.GetXY(), Dot(Abs(ay), Vec3V(V_ONE)));
	m_shadowBoxTestAZ = Vec3V(az.GetXY(), Dot(Abs(az), Vec3V(V_ONE)));
	m_shadowBoxTestEX = Abs(Multiply(m_shadowToWorld33, ax));
	m_shadowBoxTestEY = Abs(Multiply(m_shadowToWorld33, ay));
	m_shadowBoxTestEZ = Abs(Multiply(m_shadowToWorld33, az));

	const Vec4V thresh = m_shadowSpheresR*sphereThresholds;

	m_cascadeBoxThresholds = boxThresholds;
	m_cascadeSphereThresholds = thresh*thresh;

	CalculateCascadeDistances();
}

#if CASCADE_SHADOWS_SCAN_DEBUG

void fwScanCascadeShadowInfo::SetDebugInfo(const fwScanCascadeShadowDebug& debugInfo)
{
	m_debug = debugInfo;
}

fwScanCascadeShadowDebug::fwScanCascadeShadowDebug()
{
	m_flags_ =
		BIT(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANE_OPTIMISER) |
		BIT(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANES_A       ) |
		BIT(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANES_B       ) |
		BIT(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_GROUND_PLANES  ) |
		BIT(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_SHADOW_PORTAL  ) |
		BIT(CSM_SCAN_DEBUG_FLAG_CLIP_BACKFACING_PLANES         ) |
		BIT(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX               ) |
		BIT(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_1             ) |
		BIT(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_1_EX          ) |
		BIT(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_2             ) |
		BIT(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_EXACT_THRESHOLDS)|
		BIT(CSM_SCAN_DEBUG_FLAG_CULL_CASCADES_BY_LOD_DISTANCE  ) ;

	m_shadowVisFlagsForce = 0;
	m_shadowVisFlagsAllow = (1<<SUBPHASE_CASCADE_COUNT) - 1;

	m_cascadeLodDistanceScale = Vec4V(V_ONE);
}

#endif // CASCADE_SHADOWS_SCAN_DEBUG
#endif // !__SPU

#if CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER

class fwScanCSMSortedPlaneRef
{
public:
	fwScanCSMSortedPlaneRef() {}
	fwScanCSMSortedPlaneRef(ScalarV_In planeIndex, ScalarV_In planeLengthSqr) : m_planeIndex(planeIndex), m_planeLengthSqr(planeLengthSqr)
	{
#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
		m_points[0] = Vec3V(V_ZERO);
		m_points[1] = Vec3V(V_ZERO);
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
	}

	static s32 CompareFunc(fwScanCSMSortedPlaneRef const* a, fwScanCSMSortedPlaneRef const* b)
	{
		if (IsGreaterThanAll(a->m_planeLengthSqr, b->m_planeLengthSqr)) return -1; // sort descending
		if (IsGreaterThanAll(b->m_planeLengthSqr, a->m_planeLengthSqr)) return +1;
		return 0;
	}

	ScalarV m_planeIndex;
	ScalarV m_planeLengthSqr;
#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
	Vec3V   m_points[2]; // relative to final cascade
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
};

#endif // CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER

// test C:\spu_debug\ScanNodes_psn_beta.frag.elf in tuner ..
#define CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU     (1 && __SPU) // slightly less efficient code but saves a few kilobytes
#define CASCADE_SHADOWS_SCAN_NO_GROUND_CLIP_SPU (1 && __SPU) // removing ground clip on spu code saves a couple more kilobytes! and i think it only really helps in outdoor situations

int fwScanCascadeShadowInfo::UpdateShadowViewport(
	Vec4V                     planes[CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS],
	fwScanCSMSortedPlaneInfo* spi,
	Vec4V_In                  viewerPortal
	)
{
#if __BANK && !__SPU
	if (m_isOrthographic)
	{
		const Vec3V boundsMin = m_viewTanFOVZ.GetXYZ();
		const Vec3V boundsMax = m_viewTanFOVShear.GetXYZ();

		int planeCount = 0;

		// inefficient ..
		planes[planeCount++] = BuildPlane(boundsMin, -Vec3V(V_X_AXIS_WZERO));
		planes[planeCount++] = BuildPlane(boundsMin, -Vec3V(V_Y_AXIS_WZERO));
		planes[planeCount++] = BuildPlane(boundsMax, +Vec3V(V_X_AXIS_WZERO));
		planes[planeCount++] = BuildPlane(boundsMax, +Vec3V(V_Y_AXIS_WZERO));

		Transpose4x4(
			m_transposedPlanes[0],
			m_transposedPlanes[1],
			m_transposedPlanes[2],
			m_transposedPlanes[3],
			planes[0],
			planes[1],
			planes[2],
			planes[3]
		);

		for (int i = planeCount; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i++) // fill the rest with zero
		{
			planes[i] = Vec4V(V_ZERO);
			m_transposedPlanes[i] = Vec4V(V_ZERO);
		}

		return planeCount;
	}
#endif // __BANK && !__SPU

#if !(CASCADE_SHADOWS_SCAN_DEBUG && !__SPU)
	(void)spi;
#endif // !(CASCADE_SHADOWS_SCAN_DEBUG && !__SPU)

	const Vec4V x0y0x1y1     = viewerPortal*m_viewTanFOVShear; // {x0*tanLeft*z1,y0*tanBottom*z1,x1*tanRight*z1,y1*tanTop*z1}
	const Vec3V frustum_p00  = Transform(m_viewToWorld, GetFromTwo<Vec::X1,Vec::Y1,Vec::Z2>(x0y0x1y1, m_viewTanFOVZ)); // {x0*tanHFOV*z1,y0*tanVFOV*z1,-z1}
	const Vec3V frustum_p10  = Transform(m_viewToWorld, GetFromTwo<Vec::Z1,Vec::Y1,Vec::Z2>(x0y0x1y1, m_viewTanFOVZ)); // {x1*tanHFOV*z1,y0*tanVFOV*z1,-z1}
	const Vec3V frustum_p01  = Transform(m_viewToWorld, GetFromTwo<Vec::X1,Vec::W1,Vec::Z2>(x0y0x1y1, m_viewTanFOVZ)); // {x0*tanHFOV*z1,y1*tanVFOV*z1,-z1}
	const Vec3V frustum_p11  = Transform(m_viewToWorld, GetFromTwo<Vec::Z1,Vec::W1,Vec::Z2>(x0y0x1y1, m_viewTanFOVZ)); // {x1*tanHFOV*z1,y1*tanVFOV*z1,-z1}
	const Vec3V frustum_apex = m_viewToWorld.GetCol3();
	const Vec3V z            = m_shadowToWorld33.GetCol2();

	// p01-----------p11
	//  | \         / |
	//  |  \   py  /  |
	//  |   \     /   |
	//  |    \   /    |
	//  |     \ /     |
	//  | nx camPos px|
	//  |     / \     |
	//  |    /   \    |
	//  |   /     \   |
	//  |  /   ny  \  |
	//  | /         \ |
	// p00-----------p10

#if CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU
	#define plane_nx plane_[0]
	#define plane_ny plane_[1]
	#define plane_px plane_[2]
	#define plane_py plane_[3]
	#define plane_pz plane_[4]

	#define facing_nx facing_[0]
	#define facing_ny facing_[1]
	#define facing_px facing_[2]
	#define facing_py facing_[3]
	#define facing_pz facing_[4]

	Vec4V plane_[5];
	u32 facing_[5];

	const Vec3V plane_pt[5][3] =
	{
		{frustum_apex, frustum_p01, frustum_p00},
		{frustum_apex, frustum_p00, frustum_p10},
		{frustum_apex, frustum_p10, frustum_p11},
		{frustum_apex, frustum_p11, frustum_p01},
		{frustum_p00 , frustum_p01, frustum_p11},
	};
	for (int i = 0; i < 5; i++)
	{
		plane_[i] = BuildPlane(plane_pt[i][0], plane_pt[i][1], plane_pt[i][2]);
		facing_[i] = IsLessThanAll(Dot(plane_[i].GetXYZ(), z), ScalarV(V_ZERO));
	}
#else
	const Vec4V plane_nx = BuildPlane(frustum_apex, frustum_p01, frustum_p00); // -x CCW
	const Vec4V plane_ny = BuildPlane(frustum_apex, frustum_p00, frustum_p10); // -y
	const Vec4V plane_px = BuildPlane(frustum_apex, frustum_p10, frustum_p11); // +x
	const Vec4V plane_py = BuildPlane(frustum_apex, frustum_p11, frustum_p01); // +y
	const Vec4V plane_pz = BuildPlane(frustum_p00 , frustum_p01, frustum_p11); // +z

	const u32 facing_nx = IsLessThanAll(Dot(plane_nx.GetXYZ(), z), ScalarV(V_ZERO));
	const u32 facing_ny = IsLessThanAll(Dot(plane_ny.GetXYZ(), z), ScalarV(V_ZERO));
	const u32 facing_px = IsLessThanAll(Dot(plane_px.GetXYZ(), z), ScalarV(V_ZERO));
	const u32 facing_py = IsLessThanAll(Dot(plane_py.GetXYZ(), z), ScalarV(V_ZERO));
	const u32 facing_pz = IsLessThanAll(Dot(plane_pz.GetXYZ(), z), ScalarV(V_ZERO));
#endif

	const Vec3V centroid = (frustum_p00 + frustum_p10 + frustum_p01 + frustum_p11 + frustum_apex)*ScalarV(1.0f/5.0f);

	m_IsFrustumFacingShadow = facing_pz > 0;
	m_shadowCamPos = Multiply(m_worldToShadow33, frustum_apex).GetXY();

	int planeCount = 0;

#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
	#define ADD_SILHOUETTE_PLANE(p0,p1,centroid,type) \
		if (spi) \
		{ \
			spi->m_sortedPlanes[planeCount].m_planeType   = type; \
			spi->m_sortedPlanes[planeCount].m_planeLength = ScalarV(V_ZERO); \
			spi->m_sortedPlanes[planeCount].m_points[0]   = Vec3V(V_ZERO); \
			spi->m_sortedPlanes[planeCount].m_points[1]   = Vec3V(V_ZERO); \
		} \
		planes[planeCount] = BuildSilhouettePlane(p0, p1 - p0, z, centroid); \
		planeCount++
	#define ADD_PLANE(plane,type) \
		if (spi) \
		{ \
			spi->m_sortedPlanes[planeCount].m_planeType   = type; \
			spi->m_sortedPlanes[planeCount].m_planeLength = ScalarV(V_ZERO); \
			spi->m_sortedPlanes[planeCount].m_points[0]   = Vec3V(V_ZERO); \
			spi->m_sortedPlanes[planeCount].m_points[1]   = Vec3V(V_ZERO); \
		} \
		planes[planeCount] = plane; \
		planeCount++
#else
	#define ADD_SILHOUETTE_PLANE(p0,p1,centroid,type) \
		planes[planeCount] = BuildSilhouettePlane(p0, p1 - p0, z, centroid); \
		planeCount++
	#define ADD_PLANE(plane,type) \
		planes[planeCount] = plane; \
		planeCount++
#endif

#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANES_A))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
#if CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU
		const Vec3V frustum_pt[4] =
		{
			frustum_p00,
			frustum_p10,
			frustum_p01,
			frustum_p11,
		};
		for (int i = 0; i < 4; i++)
		{
			if (facing_[((i&1)<<1)] != facing_[(i&2)+1])
			{
				planes[planeCount++] = BuildSilhouettePlane(frustum_apex, frustum_pt[i] - frustum_apex, z, centroid);
			}
		}
#else
		if (facing_nx != facing_ny) { ADD_SILHOUETTE_PLANE(frustum_apex, frustum_p00, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_NY); }
		if (facing_px != facing_ny) { ADD_SILHOUETTE_PLANE(frustum_apex, frustum_p10, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_NY); }
		if (facing_nx != facing_py) { ADD_SILHOUETTE_PLANE(frustum_apex, frustum_p01, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PY); }
		if (facing_px != facing_py) { ADD_SILHOUETTE_PLANE(frustum_apex, frustum_p11, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_PY); }
#endif
	}

#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANES_B))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
#if CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU
		for (int i = 0; i < 4; i++)
		{
			if (facing_[i] != facing_[4])
			{
				planes[planeCount++] = BuildSilhouettePlane(plane_pt[i][1], plane_pt[i][2] - plane_pt[i][1], z, centroid);
			}
		}
#else
		if (facing_nx != facing_pz) { ADD_SILHOUETTE_PLANE(frustum_p01, frustum_p00, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PZ); }
		if (facing_ny != facing_pz) { ADD_SILHOUETTE_PLANE(frustum_p00, frustum_p10, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PZ); }
		if (facing_px != facing_pz) { ADD_SILHOUETTE_PLANE(frustum_p10, frustum_p11, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PZ); }
		if (facing_py != facing_pz) { ADD_SILHOUETTE_PLANE(frustum_p11, frustum_p01, centroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PZ); }
#endif
	}

#if !CASCADE_SHADOWS_SCAN_NO_GROUND_CLIP_SPU
#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_GROUND_PLANES)) // clip frustum against ground plane
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
		const ScalarV frustumWorldMin = Min(frustum_apex, Min(frustum_p00, frustum_p01, frustum_p10, frustum_p11)).GetZ();
		const ScalarV h0 = m_recvrHeightMinMax.GetX();

		if (IsLessThanAll(frustumWorldMin, h0))
		{
			const Vec3V face_px[] = {frustum_apex, frustum_p10, frustum_p11};
			const Vec3V face_nx[] = {frustum_apex, frustum_p01, frustum_p00};
			const Vec3V face_py[] = {frustum_apex, frustum_p11, frustum_p01};
			const Vec3V face_ny[] = {frustum_apex, frustum_p00, frustum_p10};
			const Vec3V face_pz[] = {frustum_p00 , frustum_p01, frustum_p11, frustum_p10};

			Vec3V edge_px[2];
			Vec3V edge_nx[2];
			Vec3V edge_py[2];
			Vec3V edge_ny[2];
			Vec3V edge_pz[2];

			const Vec4V groundPlane = Vec4V(Vec3V(V_Z_AXIS_WZERO), -h0);

			const bool clip_px = PolyClipEdge(edge_px, face_px, NELEM(face_px), groundPlane);
			const bool clip_nx = PolyClipEdge(edge_nx, face_nx, NELEM(face_nx), groundPlane);
			const bool clip_py = PolyClipEdge(edge_py, face_py, NELEM(face_py), groundPlane);
			const bool clip_ny = PolyClipEdge(edge_ny, face_ny, NELEM(face_ny), groundPlane);
			const bool clip_pz = PolyClipEdge(edge_pz, face_pz, NELEM(face_pz), groundPlane);

			Vec3V   clipCentroid    = Vec3V(V_ZERO);
			ScalarV clipCentroidNum = ScalarV(V_ZERO);

			if (clip_px) { clipCentroid += edge_px[0] + edge_px[1]; clipCentroidNum += ScalarV(V_TWO); }
			if (clip_nx) { clipCentroid += edge_nx[0] + edge_nx[1]; clipCentroidNum += ScalarV(V_TWO); }
			if (clip_py) { clipCentroid += edge_py[0] + edge_py[1]; clipCentroidNum += ScalarV(V_TWO); }
			if (clip_ny) { clipCentroid += edge_ny[0] + edge_ny[1]; clipCentroidNum += ScalarV(V_TWO); }
			if (clip_pz) { clipCentroid += edge_pz[0] + edge_pz[1]; clipCentroidNum += ScalarV(V_TWO); }

			clipCentroid /= clipCentroidNum;

			if (clip_nx && facing_nx != 0) { ADD_SILHOUETTE_PLANE(edge_nx[0], edge_nx[1], clipCentroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_GROUND); }
			if (clip_ny && facing_ny != 0) { ADD_SILHOUETTE_PLANE(edge_ny[0], edge_ny[1], clipCentroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NY_GROUND); }
			if (clip_px && facing_px != 0) { ADD_SILHOUETTE_PLANE(edge_px[0], edge_px[1], clipCentroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_GROUND); }
			if (clip_py && facing_py != 0) { ADD_SILHOUETTE_PLANE(edge_py[0], edge_py[1], clipCentroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PY_GROUND); }
			if (clip_pz && facing_pz != 0) { ADD_SILHOUETTE_PLANE(edge_pz[0], edge_pz[1], clipCentroid, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PZ_GROUND); }
		}
	}
#endif // !CASCADE_SHADOWS_SCAN_NO_GROUND_CLIP_SPU

	Vec4V cascadeSpheres[CASCADE_SHADOWS_SCAN_NUM_CASCADES]; // required for shadow portal, and also for silhouette plane sorting ..

	Transpose4x4(
		cascadeSpheres[0],
		cascadeSpheres[1],
		cascadeSpheres[2],
		cascadeSpheres[3],
		m_shadowSpheresX,
		m_shadowSpheresY,
		m_shadowSpheresZ,
		m_shadowSpheresR
	);

	// transform back to worldspace
#if CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU
	for (int i = 0; i < 4; i++)
	{
		cascadeSpheres[i] = TransformSphere(m_shadowToWorld33, cascadeSpheres[i]);
	}
#else
	cascadeSpheres[0] = TransformSphere(m_shadowToWorld33, cascadeSpheres[0]);
	cascadeSpheres[1] = TransformSphere(m_shadowToWorld33, cascadeSpheres[1]);
	cascadeSpheres[2] = TransformSphere(m_shadowToWorld33, cascadeSpheres[2]);
	cascadeSpheres[3] = TransformSphere(m_shadowToWorld33, cascadeSpheres[3]);
#endif

	const Vec3V   cascadeCentre = cascadeSpheres[3].GetXYZ();
	const ScalarV cascadeRadius = cascadeSpheres[3].GetW();
	const Vec3V   cascadeBasisX = m_shadowToWorld33.GetCol0()*cascadeRadius;
	const Vec3V   cascadeBasisY = m_shadowToWorld33.GetCol1()*cascadeRadius;

#if (CASCADE_SHADOWS_SCAN_DEBUG && !__SPU) || CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER
	const int silhouettePlaneCount = planeCount;
#endif // (CASCADE_SHADOWS_SCAN_DEBUG && !__SPU) || CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER

#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_CLIP_BACKFACING_PLANES))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
		if (!m_forceNoBackfacingPlanes)
		{
#if CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU
			for (int i = 0; i < 5; i++)
			{
				if (facing_[i] == 0)
				{
					planes[planeCount++] = plane_[i];
				}
			}
#else
			if (facing_nx == 0) { ADD_PLANE(plane_nx, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_NX); }
			if (facing_ny == 0) { ADD_PLANE(plane_ny, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_NY); }
			if (facing_px == 0) { ADD_PLANE(plane_px, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_PX); }
			if (facing_py == 0) { ADD_PLANE(plane_py, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_PY); }
			if (facing_pz == 0) { ADD_PLANE(plane_pz, CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_PZ); }
#endif
		}
	}

#undef ADD_SILHOUETTE_PLANE
#undef ADD_PLANE

#if !CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU
	Assertf(planeCount < CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS, "fwScanCascadeShadowInfo::UpdateViewport generated %d planes, max excess = %d!", planeCount, CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS);
#endif // !CASCADE_SHADOWS_SCAN_SMALL_CODE_SPU

#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
	if (spi)
	{
		spi->m_silhouettePolyNumVerts = 0;
		spi->m_unoptimisedPlaneCount  = planeCount;
	}
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU

#if CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER
#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANE_OPTIMISER))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
		Vec4V poly[32];
		int   polyNumVerts = 0;

		const ScalarV scale = ScalarV(V_TEN); // extend so it doesn't clip to cascade boundary

		// initial poly
		poly[polyNumVerts++] = Vec4V(cascadeCentre - cascadeBasisX*scale - cascadeBasisY*scale, ScalarV(V_NEGONE));
		poly[polyNumVerts++] = Vec4V(cascadeCentre + cascadeBasisX*scale - cascadeBasisY*scale, ScalarV(V_NEGONE));
		poly[polyNumVerts++] = Vec4V(cascadeCentre + cascadeBasisX*scale + cascadeBasisY*scale, ScalarV(V_NEGONE));
		poly[polyNumVerts++] = Vec4V(cascadeCentre - cascadeBasisX*scale + cascadeBasisY*scale, ScalarV(V_NEGONE));

		for (int i = 0; i < silhouettePlaneCount && polyNumVerts >= 3; i++)
		{
			Vec4V temp[32];
			const int tempNumVerts = PolyClipEx(temp, poly, polyNumVerts, -planes[i], ScalarV((float)i)); // store plane index int w

			for (int j = 0; j < tempNumVerts; j++)
			{
				poly[j] = temp[j];
			}

			polyNumVerts = tempNumVerts;
		}

		if (polyNumVerts >= 3)
		{
			atFixedArray<fwScanCSMSortedPlaneRef, 32 + CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS> sortedPlanes;
#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
			ScalarV perimeter(V_ZERO);
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU

			// build silhouette planes
			{
				Vec3V p0 = poly[polyNumVerts - 1].GetXYZ();

				for (int i = 0; i < polyNumVerts; i++)
				{
					const Vec3V   p1             = poly[i].GetXYZ();
					const ScalarV planeIndex     = poly[i].GetW();
					const ScalarV planeLengthSqr = MagSquared(p1 - p0);

					sortedPlanes.Push(fwScanCSMSortedPlaneRef(planeIndex, planeLengthSqr));
#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
					sortedPlanes.Top().m_points[0] = p0;
					sortedPlanes.Top().m_points[1] = p1;
					perimeter += Sqrt(planeLengthSqr);
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
					p0 = p1;

#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
					if (spi)
					{
						spi->m_silhouettePoly[spi->m_silhouettePolyNumVerts++] = p1;
					}
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
				}

				for (int i = silhouettePlaneCount; i < planeCount; i++) // add backfacing planes to sorted list
				{
					const ScalarV planeIndex  = ScalarV((float)i);
					const ScalarV planeLength = ScalarV(V_ZERO);

					sortedPlanes.Push(fwScanCSMSortedPlaneRef(planeIndex, planeLength));
				}

				sortedPlanes.QSort(0, -1, fwScanCSMSortedPlaneRef::CompareFunc);
			}

			// copy planes into temporary arrays
			Vec4V _planes[CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS];

			for (int i = 0; i < planeCount; i++)
			{
				_planes[i] = planes[i];
			}

#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
			int _planeTypes[CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS];

			if (spi)
			{
				for (int i = 0; i < planeCount; i++)
				{
					_planeTypes[i] = spi->m_sortedPlanes[i].m_planeType;
				}
			}
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU

			planeCount = 0;

			for (int i = 0; i < sortedPlanes.size(); i++) // copy planes in sorted order
			{
				const int planeIndex = (int)sortedPlanes[i].m_planeIndex.Getf();

				if (planeIndex != -1)
				{
					planes[planeCount] = _planes[planeIndex];

#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
					if (spi)
					{
						spi->m_sortedPlanes[planeCount].m_planeType   = _planeTypes[planeIndex];
						spi->m_sortedPlanes[planeCount].m_planeLength = Sqrt(sortedPlanes[i].m_planeLengthSqr)/perimeter;
						spi->m_sortedPlanes[planeCount].m_points[0]   = sortedPlanes[i].m_points[0];
						spi->m_sortedPlanes[planeCount].m_points[1]   = sortedPlanes[i].m_points[1];
					}
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU

					planeCount++;
				}
			}
		}
	}
#endif // CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER

	for (int i = planeCount; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i++) // fill the rest with zero
	{
		planes[i] = Vec4V(V_ZERO);
	}

	for (int i = 0; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i += 4)
	{
		Transpose4x4(
			m_transposedPlanes[0 + i],
			m_transposedPlanes[1 + i],
			m_transposedPlanes[2 + i],
			m_transposedPlanes[3 + i],
			planes[0 + i],
			planes[1 + i],
			planes[2 + i],
			planes[3 + i]
		);
	}
	PrecalculateRayInterection();

	return planeCount; // might be more than CASCADE_SHADOWS_SCAN_NUM_PLANES
}

static __forceinline u32 IsLargeEnoughOnScreen(Vec3V_In centre, ScalarV_In extentSqr, Vec3V_In camPos, ScalarV_In thresh)
{
	return IsGreaterThanOrEqualAll(extentSqr, thresh*MagSquared(camPos - centre));
}

static __forceinline void CalculateExactProjectedAreaOrtho_UnTransformXY(Mat33V_In mtx, Vec4V_InOut rx, Vec4V_InOut ry, Vec4V_In x, Vec4V_In y, Vec4V_In z)
{
	const ScalarV m00 = mtx.GetCol0().GetX();
	const ScalarV m01 = mtx.GetCol0().GetY();

	const ScalarV m10 = mtx.GetCol1().GetX();
	const ScalarV m11 = mtx.GetCol1().GetY();

	const ScalarV m20 = mtx.GetCol2().GetX();
	const ScalarV m21 = mtx.GetCol2().GetY();

	const Vec4V c0 = AddScaled(z*m20, y, m10);
	const Vec4V c1 = AddScaled(z*m21, y, m11);

	rx = AddScaled(c0, x, m00);
	ry = AddScaled(c1, x, m01);
}

static __forceinline ScalarV_Out CalculateExactProjectedAreaOrtho(Mat33V_In shadowToWorld33, Vec3V_In bmin, Vec3V_In bmax)
{
	// get AABB points (SoA)
	const Vec4V minmax_x   = GetFromTwo<Vec::X1, Vec::X2, Vec::X1, Vec::X2>(bmin, bmax); // x X x X
	const Vec4V minmax_y   = GetFromTwo<Vec::Y1, Vec::Y1, Vec::Y2, Vec::Y2>(bmin, bmax); // y y Y Y
	const Vec4V minmax_z_0 = Vec4V(bmin.GetZ()); // z z z z
	const Vec4V minmax_z_1 = Vec4V(bmax.GetZ()); // Z Z Z Z

	// transform points to shadow space
	// Note - could optimize this so xy rows calced once
	Vec4V x0, y0, x1, y1;
	CalculateExactProjectedAreaOrtho_UnTransformXY(shadowToWorld33, x0, y0, minmax_x, minmax_y, minmax_z_0);
	CalculateExactProjectedAreaOrtho_UnTransformXY(shadowToWorld33, x1, y1, minmax_x, minmax_y, minmax_z_1);

	// now get area of points
	// we have six quads to sum together
	// Signed Area quad = (x1-x0)*(y3-y0) -(x3-x0)*(y1-y0);
	// Points are in order x,y plane, z0, z1
	const u32 P0 = Vec::X1;
	const u32 P1 = Vec::Y1;
	const u32 P2 = Vec::Z1;
	const u32 P3 = Vec::W1;

	const u32 P4 = Vec::X2;
	const u32 P5 = Vec::Y2;
	const u32 P6 = Vec::Z2;
	const u32 P7 = Vec::W2;

	const Vec4V x0_0 = GetFromTwo<P2, P1, P2, P6>(x0, x1);
	const Vec4V x1_0 = GetFromTwo<P3, P3, P6, P2>(x0, x1);
	// const Vec4V x2_0 = GetFromTwo<P1, P7, P7, P0>(x0, x1);
	const Vec4V x3_0 = GetFromTwo<P0, P5, P3, P4>(x0, x1);

	const Vec4V x0_1 = GetFromTwo<P4, P0, P0, P7>(x0, x1);
	const Vec4V x1_1 = GetFromTwo<P5, P1, P0, P7>(x0, x1);
	// const Vec4V x2_1 = GetFromTwo<P7, P5, P0, P7>(x0, x1);
	const Vec4V x3_1 = GetFromTwo<P6, P4, P0, P7>(x0, x1);

	const Vec4V y0_0 = GetFromTwo<P2, P1, P2, P6>(y0, y1);
	const Vec4V y1_0 = GetFromTwo<P3, P3, P6, P2>(y0, y1);
	// const Vec4V y2_0 = GetFromTwo<P1, P7, P7, P0>(y0, y1);
	const Vec4V y3_0 = GetFromTwo<P0, P5, P3, P4>(y0, y1);

	const Vec4V y0_1 = GetFromTwo<P4, P0, P0, P7>(y0, y1);
	const Vec4V y1_1 = GetFromTwo<P5, P1, P0, P7>(y0, y1);
	// const Vec4V y2_1 = GetFromTwo<P7, P5, P0, P7>(y0, y1);
	const Vec4V y3_1 = GetFromTwo<P6, P4, P0, P7>(y0, y1);

	Vec4V quad0 = (x1_0 - x0_0)*(y3_0 - y0_0) -(x3_0 - x0_0)*(y1_0 - y0_0);
	Vec4V quad1 = (x1_1 - x0_1)*(y3_1 - y0_1) -(x3_1 - x0_1)*(y1_1 - y0_1);

#if 0 && __ASSERT // TODO -- is this valid or should it be removed?
	Vec4V   tv = quad0 + (quad1 & Vec4V(V_MASKXY));
	ScalarV sv = Dot(tv, Vec4V(V_ONE)); // horizontal add
	Assert(IsTrue(IsClose(sv, ScalarV(V_ZERO), ScalarV(0.2f))));
#endif // __ASSERT

	quad0 = Max(quad0, Vec4V(V_ZERO));
	quad1 = Max(quad1, Vec4V(V_ZERO)) & Vec4V(V_MASKXY);
	quad0 += quad1;

	return Dot(quad0, Vec4V(V_ONE)); // horizontal add
}

inline void fwScanCascadeShadowInfo::CalculateCascadeDistances()
{
    Vec3V spherePos0;
	Vec3V spherePos1;
	Vec3V spherePos2;
	Vec3V spherePos3;

	Transpose4x3to3x4(
		spherePos0,
		spherePos1,
		spherePos2,
		spherePos3,
		m_shadowSpheresX,
		m_shadowSpheresY,
		m_shadowSpheresZ
	);

	const Vec3V worldSphere0 = Multiply(m_shadowToWorld33, spherePos0);
	const Vec3V worldSphere1 = Multiply(m_shadowToWorld33, spherePos1);
	const Vec3V worldSphere2 = Multiply(m_shadowToWorld33, spherePos2);

	const Vec3V v0 = worldSphere0 - m_viewToWorld.GetCol3();
	const Vec3V v1 = worldSphere1 - m_viewToWorld.GetCol3();
	const Vec3V v2 = worldSphere2 - m_viewToWorld.GetCol3();

	const ScalarV mag0 = Mag(v0);
	const ScalarV mag1 = Mag(v1);
	const ScalarV mag2 = Mag(v2);

	const Vec3V dist012(mag0, mag1, mag2);
 	const Vec3V scaledRadii = m_shadowSpheresR.GetXYZ();

	m_cascadeDistances = dist012 + scaledRadii;
}

bool fwScanCascadeShadowInfo::IsShadowVisible(const spdAABB& box, const fwEntityDesc& entityDesc, u8* shadowVisFlags) const
{
	const u32 bIgnoreShadowScreenSizeFlag = entityDesc.GetDontCullSmallShadow() || !m_bEnableShadowScreenSizeCheck;
	const bool bIsGrass = entityDesc.GetEntityType() == _GTA5_ENTITY_TYPE_GRASS_BATCH;
	const Vec3V bmin = box.GetMin();
	const Vec3V bmax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(bmin));
	const Vec4V ymin = Vec4V(SplatY(bmin));
	const Vec4V zmin = Vec4V(SplatZ(bmin));
	const Vec4V xmax = Vec4V(SplatX(bmax));
	const Vec4V ymax = Vec4V(SplatY(bmax));
	const Vec4V zmax = Vec4V(SplatZ(bmax));

	const Vec4V zero(V_ZERO);

	// note that we could precalculate the IsLessThanOrEqual for the transposed planes

	Vec4V d0 = m_transposedPlanes[3];
	d0 = AddScaled(d0, m_transposedPlanes[0], SelectFT(IsLessThanOrEqual(m_transposedPlanes[0], zero), xmin, xmax));
	d0 = AddScaled(d0, m_transposedPlanes[1], SelectFT(IsLessThanOrEqual(m_transposedPlanes[1], zero), ymin, ymax));
	d0 = AddScaled(d0, m_transposedPlanes[2], SelectFT(IsLessThanOrEqual(m_transposedPlanes[2], zero), zmin, zmax));

#if __BANK && !__SPU
	if (m_isOrthographic)
	{
		return IsLessThanOrEqualAll(d0, zero)!=0;
	}
#endif // __BANK && !__SPU

	for (int i = 4; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i += 4)
	{
		Vec4V d1 = m_transposedPlanes[3 + i];
		d1 = AddScaled(d1, m_transposedPlanes[0 + i], SelectFT(IsLessThanOrEqual(m_transposedPlanes[0 + i], zero), xmin, xmax));
		d1 = AddScaled(d1, m_transposedPlanes[1 + i], SelectFT(IsLessThanOrEqual(m_transposedPlanes[1 + i], zero), ymin, ymax));
		d1 = AddScaled(d1, m_transposedPlanes[2 + i], SelectFT(IsLessThanOrEqual(m_transposedPlanes[2 + i], zero), zmin, zmax));
		d0 = Max(d0, d1);
	}

	const Vec3V   centre    = (bmax + bmin)*ScalarV(V_HALF);
	const Vec3V   extent    = (bmax - bmin)*ScalarV(V_HALF);
	const ScalarV extentSqr = MagSquared(extent);

	if (IsLessThanOrEqualAll(d0, zero) & (bIgnoreShadowScreenSizeFlag | IsLargeEnoughOnScreen(centre, extentSqr, m_viewToWorld.GetCol3(), m_viewTanFOVZ.GetW())))
	{
		VecBoolV cascadeVisibility = VecBoolV(V_T_T_T_T);

#if CASCADE_SHADOWS_SCAN_DEBUG
		if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_SPHERE_THRESHOLDS))
		{
			cascadeVisibility = IsLessThan(m_cascadeSphereThresholds, Vec4V(extentSqr)); // sphere thresholds
		}
#endif // CASCADE_SHADOWS_SCAN_DEBUG

#if CASCADE_SHADOWS_SCAN_DEBUG
		if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
		{
			cascadeVisibility &= IsShadowVisibleInCascadeBox(centre, extent);
		}

#if __BANK
#if CASCADE_SHADOWS_SCAN_DEBUG
		if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_EXACT_THRESHOLDS))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
		{
			if(m_UseAreaCulling)
			{
				const ScalarV area = CalculateExactProjectedAreaOrtho(m_shadowToWorld33, bmin, bmax);

				// only apply to the last two cascades
				cascadeVisibility &= (Vec4V(area) > m_cascadeBoxThresholds) | VecBoolV(V_T_T_F_F);
			}
		}
#endif

		u8 i = (u8)GetBoolMask(cascadeVisibility);

		i |= m_forceVisibilityFlags;
#if CASCADE_SHADOWS_SCAN_DEBUG
		i |= m_debug.m_shadowVisFlagsForce;
		i &= m_debug.m_shadowVisFlagsAllow;
#endif // CASCADE_SHADOWS_SCAN_DEBUG

		if(bIsGrass)
		{
			if(AwlaysCullGrassEnabled()) i = 0;
			else if(GrassCascadeCullingEnabled()) i &= sGrassCascadeMask;
		}

		*shadowVisFlags = i;
		return true;
	}

	return false;
}

u8 fwScanCascadeShadowInfo::IsLoddedInCascade(u8 shadowVisFlags, float lodDistance) const
{
	for (int i = 1; i < 4; i++)
	{
		const u32 bit = BIT(i);

		if (!(bit & shadowVisFlags) || (shadowVisFlags & ~bit))
		{
			continue;
		}

		const int index = i - 1; // the previous cascade is what we need to compare with
 		const float* pDist = (const float*)&m_cascadeDistances;		
		const float scaledLodDistance = lodDistance*GetCascadeLodDistanceScale(i);

		if (scaledLodDistance < pDist[index])
		{
			shadowVisFlags &= u8(~bit);
		}
	}

	return shadowVisFlags;
}

bool fwScanCascadeShadowInfo::IsShadowVisible(const spdAABB& box) const
{
	// Don't cull small shadow, see BS#614294. Cloth and rope managers
	// rely on testing whether the shadow is visible in order to determine
	// if they need to update, but the bounding box passed in does not
	// match the drawable bounding box.
	const u32 bIgnoreShadowScreenSizeFlag = 1;
	fwEntityDesc entityDesc;	//Create a dummy entity description
	entityDesc.SetDontCullSmallShadow(bIgnoreShadowScreenSizeFlag);

	u8 shadowVisFlags;
	return IsShadowVisible(box, entityDesc, &shadowVisFlags);
}

inline VecBoolV_Out fwScanCascadeShadowInfo::IsShadowVisibleInCascadeBox(Vec3V_In centre, Vec3V_In extent) const
{
	const Vec3V shadowCentre = Multiply(centre, m_shadowToWorld33);

	const Vec4V vx = m_shadowSpheresX - Vec4V(shadowCentre.GetX());
	const Vec4V vy = m_shadowSpheresY - Vec4V(shadowCentre.GetY());
	const Vec4V vr = m_shadowSpheresR;

	VecBoolV visible = VecBoolV(V_T_T_T_T);

#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_1)) // test if entity intersects cascade box (shadowspace)
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
		const ScalarV ex = Dot(Abs(m_shadowToWorld33.GetCol0()), extent); // entity extent x in shadowspace
		const ScalarV ey = Dot(Abs(m_shadowToWorld33.GetCol1()), extent); // entity extent y in shadowspace

		visible &= IsGreaterThanOrEqual(Vec4V(ex), Abs(vx) - vr);
		visible &= IsGreaterThanOrEqual(Vec4V(ey), Abs(vy) - vr);

#if CASCADE_SHADOWS_SCAN_DEBUG
		if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_1_EX))
#endif // CASCADE_SHADOWS_SCAN_DEBUG
		{
			VecBoolV contained;

			contained  = IsGreaterThanOrEqual(-Vec4V(ex), Abs(vx) - vr);
			contained &= IsGreaterThanOrEqual(-Vec4V(ey), Abs(vy) - vr);

#if __XENON || __PPU
			const VecBoolV containedMask1 = ShiftLeftBytesDouble<3*4>(VecBoolV(V_F_F_F_F), VecBoolV(contained.GetX()));
			const VecBoolV containedMask2 = ShiftLeftBytesDouble<2*4>(VecBoolV(V_F_F_F_F), VecBoolV(contained.GetY()));
			const VecBoolV containedMask3 = ShiftLeftBytesDouble<1*4>(VecBoolV(V_F_F_F_F), VecBoolV(contained.GetZ()));
#else
			const VecBoolV containedMask1 = VecBoolV(GetFromTwo<Vec::X1,Vec::X2,Vec::X2,Vec::X2>(Vec4V(V_FALSE), Vec4V(contained)).GetIntrin128());
			const VecBoolV containedMask2 = VecBoolV(GetFromTwo<Vec::X1,Vec::X1,Vec::Y2,Vec::Y2>(Vec4V(V_FALSE), Vec4V(contained)).GetIntrin128());
			const VecBoolV containedMask3 = VecBoolV(GetFromTwo<Vec::X1,Vec::X1,Vec::X1,Vec::Z2>(Vec4V(V_FALSE), Vec4V(contained)).GetIntrin128());
#endif
			const VecBoolV containedMask = Or(Or(containedMask1, containedMask2), containedMask3); // which cascades are contained by closer cascades

			visible = Andc(visible, And(containedMask, m_cascadeExclusionFlags));
		}
	}

#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_2)) // test if cascade box intersects entity (hexagon)
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	{
		const Vec3V AX = m_shadowBoxTestAX;
		const Vec3V AY = m_shadowBoxTestAY;
		const Vec3V AZ = m_shadowBoxTestAZ;
		const Vec3V EX = m_shadowBoxTestEX;
		const Vec3V EY = m_shadowBoxTestEY;
		const Vec3V EZ = m_shadowBoxTestEZ;

		visible &= IsGreaterThanOrEqual(Vec4V(Dot(EX, extent)), SubtractScaled(Abs(AddScaled(vx*AX.GetX(), vy, AX.GetY())), vr, AX.GetZ()));
		visible &= IsGreaterThanOrEqual(Vec4V(Dot(EY, extent)), SubtractScaled(Abs(AddScaled(vx*AY.GetX(), vy, AY.GetY())), vr, AY.GetZ()));
		visible &= IsGreaterThanOrEqual(Vec4V(Dot(EZ, extent)), SubtractScaled(Abs(AddScaled(vx*AZ.GetX(), vy, AZ.GetY())), vr, AZ.GetZ()));
	}

	return visible;
}

void fwScanCascadeShadowInfo::GetCascades(Mat44V_In worldToOcclusion, Vec4V_InOut x, Vec4V_InOut y, Vec4V_InOut z, Vec4V_InOut r)
{
	x = m_shadowSpheresX;
	y = m_shadowSpheresY;
	z = m_shadowSpheresZ;
	r = m_shadowSpheresR;

	Vec3V spherePos[4];

	Transpose4x3to3x4(
		spherePos[0],
		spherePos[1],
		spherePos[2],
		spherePos[3],
		m_shadowSpheresX,
		m_shadowSpheresY,
		m_shadowSpheresZ
	);

	for (int i = 0; i < 4; i++)
	{
		// first transform to world pos
		Vec3V worldSphere = Multiply(m_shadowToWorld33, spherePos[i]);
		Vec4V scpos = Multiply(worldToOcclusion, Vec4V(worldSphere, ScalarV(V_ONE)));
		scpos *= Invert(scpos.GetW());	
		spherePos[i] = scpos.GetXYZ();
	}

	Transpose3x4to4x3(
		x,
		y,
		z,
		spherePos[0],
		spherePos[1],
		spherePos[2],
		spherePos[3]
	);
}

void fwScanCascadeShadowInfo::PrecalculateRayInterection()
{
	Vec3V lightDir = GetShadowDir();
	for (int i = 0; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i += 4)
	{
		Vec4V denom= lightDir.GetX()*m_transposedPlanes[i + 0];
		denom =  AddScaled( denom, m_transposedPlanes[1 + i], lightDir.GetY());
		denom =  AddScaled( denom, m_transposedPlanes[2 + i], lightDir.GetZ());
		denom = InvertSafe(denom);  // note could be precaluated once and stored in ScanCascadeShadowInfo()
		m_precalulatedRayIntersectionDenom[i/4]=-denom;
	}
}

rage::ScalarV_Out fwScanCascadeShadowInfo::ProjectPointToViewportAlongShadowDirection( Vec3V_In projPoint ) const
{
	Vec4V mint(V_FLT_MAX);
	for (int i = 0; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i += 4)
	{
		Vec4V num=  AddScaled(  m_transposedPlanes[3 + i], m_transposedPlanes[0 + i], projPoint.GetX());
		num =  AddScaled( num, m_transposedPlanes[1 + i], projPoint.GetY());
		num =  AddScaled( num, m_transposedPlanes[2 + i], projPoint.GetZ());

		Vec4V t = num * m_precalulatedRayIntersectionDenom[i/4];
		mint = SelectFT( t > Vec4V(V_ZERO), mint, Min(mint, t) );		
	}
	ScalarV finalT = Min(Min( mint.GetX(), mint.GetY()), Min( mint.GetZ(), mint.GetW()));
	return finalT;
}

} // namespace rage
