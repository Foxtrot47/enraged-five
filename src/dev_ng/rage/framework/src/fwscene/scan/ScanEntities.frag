rage::u32	g_auxDmaTag = 0;

#include "system/dma.h"
#include <cell/spurs.h>
#include "system/dependency.h"
#include "fwscene/scan/ScanEntities.cpp"
#include "fwscene/scan/ScreenQuad.cpp"
#include "fwscene/scan/ScanDebug.cpp"
#include "fwscene/world/EntityContainer.cpp"

using rage::sysDependency;

SPUFRAG_DECL(bool, ScanEntities, sysDependency&);
SPUFRAG_IMPL(bool, ScanEntities, sysDependency& dependency) {
	return fwScanEntities::RunFromDependency( dependency );
}
