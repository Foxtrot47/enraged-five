// =================================
// fwscene/scan/ScanCascadeShadows.h
// (c) 2011 RockstarNorth
// =================================

#ifndef _INC_FWSCENE_SCAN_SCANCASCADESHADOWS_H_
#define _INC_FWSCENE_SCAN_SCANCASCADESHADOWS_H_

#define CASCADE_SHADOWS_SCAN_SILHOUETTE_PLANE_OPTIMISER (1 && !__SPU) // seems crazy, but i don't have a better solution

#define CASCADE_SHADOWS_SCAN_NUM_PLANES 16
#define CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS 20 // some extra space in case it overflows somehow
#define CASCADE_SHADOWS_SCAN_NUM_CASCADES 4 // must match CASCADE_SHADOWS_COUNT in cascadeshadows_common.fxh

CompileTimeAssert(CASCADE_SHADOWS_SCAN_NUM_PLANES > 0 && CASCADE_SHADOWS_SCAN_NUM_PLANES % 4 == 0);

#define CASCADE_SHADOWS_SCAN_DEBUG (1 && __DEV) // can switch this off for profiling in bankrelease

#if CASCADE_SHADOWS_SCAN_DEBUG
	#define CASCADE_SHADOWS_SCAN_DEBUG_ONLY(x) x
#else
	#define CASCADE_SHADOWS_SCAN_DEBUG_ONLY(x)
#endif

#include "vectormath/mat44v.h"

namespace rage { class spdAABB; }

namespace rage {

class fwEntityDesc;

#if CASCADE_SHADOWS_SCAN_DEBUG

enum
{
	CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANE_OPTIMISER     ,
	CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANES_A            ,
	CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_PLANES_B            ,
	CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_GROUND_PLANES       ,
	CSM_SCAN_DEBUG_FLAG_CLIP_SILHOUETTE_SHADOW_PORTAL       ,
	CSM_SCAN_DEBUG_FLAG_CLIP_BACKFACING_PLANES              ,

	CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_SPHERE_THRESHOLDS      ,

	CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX                    ,
	CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_1                  ,
	CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_1_EX               ,
	CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_2                  ,
	CSM_SCAN_DEBUG_FLAG_TEST_CASCADE_BOX_EXACT_THRESHOLDS   , // TODO -- consider removing this

	CSM_SCAN_DEBUG_FLAG_CULL_CASCADES_BY_LOD_DISTANCE       ,
	CSM_SCAN_DEBUG_FLAG_OVERRIDE_CASCADES_LOD_DISTANCE_SCALE,

	CSM_SCAN_DEBUG_FLAG_TEST_USING_REFERENCE_CODE           ,
};

enum // clip plane types
{
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_FIRST    ,

	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_NY    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_NY    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PY    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_PY    ,

	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_PZ    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NY_PZ    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_PZ    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PY_PZ    ,

	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_GROUND,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NY_GROUND,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PX_GROUND,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PY_GROUND,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_PZ_GROUND,

	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SHADOW_PORTAL_NX    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SHADOW_PORTAL_NY    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SHADOW_PORTAL_PX    ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SHADOW_PORTAL_PY    ,

	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_LAST     ,

	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_NX       ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_NY       ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_PX       ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_PY       ,
	CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_BACKFACING_PZ       ,
};

class fwScanCascadeShadowDebug
{
public:
#if !__SPU
	fwScanCascadeShadowDebug(); // no c'tor on SPU
#endif // !__SPU

	bool IsFlagSet(int flagIndex) const { return (m_flags_ & BIT(flagIndex)) != 0; }

	u32   m_flags_;
	u8    m_shadowVisFlagsForce;
	u8    m_shadowVisFlagsAllow;
	float m_shadowVisScale;
	Vec4V m_cascadeLodDistanceScale;
};

#endif // CASCADE_SHADOWS_SCAN_DEBUG

#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
class fwScanCSMSortedPlane
{
public:
	int     m_planeType;   // e.g. CSM_SCAN_DEBUG_CLIP_PLANE_TYPE_SILHOUETTE_NX_NY
	ScalarV m_planeLength; // valid for silhouette planes only, backfacing planes will have length = 0
	Vec3V   m_points[2];   // relative to final cascade
};
#endif // CASCADE_SHADOWS_SCAN_DEBUG && !__SPU

class fwScanCSMSortedPlaneInfo
{
public:
#if CASCADE_SHADOWS_SCAN_DEBUG && !__SPU
	typedef fwScanCSMSortedPlane T;
	T     m_sortedPlanes[CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS]; // parallel to planes computed in fwScanCascadeShadowInfo::UpdateViewport
	Vec3V m_silhouettePoly[32];
	int   m_silhouettePolyNumVerts;
	int   m_unoptimisedPlaneCount;
#else
	int dummy;
#endif
};

class fwScanCascadeShadowInfo
{
public:
#if !__SPU
	fwScanCascadeShadowInfo(); // no c'tor on SPU
	void SetCamera(
		Mat34V_In viewToWorld,
		ScalarV_In tanHFOV,
		ScalarV_In tanVFOV,
		ScalarV_In z1,
		ScalarV_In camFOVThresh,
		Mat33V_In shadowToWorld33,
		Mat33V_In worldToShadow33,
		const ScalarV recvrHeight[2],
		ScalarV_In shearX,
		ScalarV_In shear
	);

#if __BANK && !__SPU
	void SetCameraOrthographic(Mat34V_In viewToWorld, Vec3V_In boundsMin, Vec3V_In boundsMax, Mat33V_In shadowToWorld33);
	void SetUseAreaCulling(bool use){ m_UseAreaCulling = use; }
	bool IsOrthographic() const { return m_isOrthographic; }
	void AddWidgets(bkBank& bk);
#endif // __BANK && !__SPU

	void SetCascadeSpheres(const Vec4V spheres[4], Vec4V_In sphereThresholds, Vec4V_In boxThresholds);
#if CASCADE_SHADOWS_SCAN_DEBUG
	void SetDebugInfo(const fwScanCascadeShadowDebug& debugInfo);
#endif // CASCADE_SHADOWS_SCAN_DEBUG
	void SetForceVisibilityFlags(u8 flags, bool bForceNoBackfacingPlanes, VecBoolV_In cascadeExclusionFlags) { m_forceVisibilityFlags = flags; m_forceNoBackfacingPlanes = bForceNoBackfacingPlanes; m_cascadeExclusionFlags = cascadeExclusionFlags; }
#endif // !__SPU

#if CASCADE_SHADOWS_SCAN_DEBUG
	const fwScanCascadeShadowDebug& GetDebugInfo() const { return m_debug; }
#endif // CASCADE_SHADOWS_SCAN_DEBUG

	__forceinline Vec3V_Out GetShadowDir() const { return m_shadowToWorld33.GetCol2(); }

	__forceinline Vec4V*       GetTransposedPlanesPtr()       { return m_transposedPlanes; }
	__forceinline const Vec4V* GetTransposedPlanesPtr() const { return m_transposedPlanes; }
	__forceinline const Vec4V* GetRayIntersectionDenom() const { return m_precalulatedRayIntersectionDenom; }

	ScalarV_Out ProjectPointToViewportAlongShadowDirection( Vec3V_In projPoint ) const;

	int UpdateShadowViewport(
		Vec4V                     planes[CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS],
		fwScanCSMSortedPlaneInfo* spi,
		Vec4V_In                  viewerPortal
	);

	bool IsShadowVisible(const spdAABB& box, const fwEntityDesc& entityDesc, u8* shadowVisFlags) const;
	bool IsShadowVisible(const spdAABB& box) const;
	u8 IsLoddedInCascade(u8 shadowVisFlags, float lodDistance) const;

	inline VecBoolV_Out IsShadowVisibleInCascadeBox(Vec3V_In centre, Vec3V_In extent) const;
	inline void         CalculateCascadeDistances();

	inline void EnableCascadeLodDistanceCulling(bool enable) { m_useCascadeLodDistanceCulling = enable; }
	inline bool CascadeLodDistanceCullingEnabled() const;

	//Settings for low quality grass rendering
	inline void SetAwlaysCullGrass(bool shouldCull) { m_alwaysCullGrass = shouldCull; }
	inline bool AwlaysCullGrassEnabled() const { return m_alwaysCullGrass; }

	inline void EnableGrassCascadeCulling(bool enable) { m_allowGrassCascadeCulling = enable; }
	inline bool GrassCascadeCullingEnabled() const { return m_allowGrassCascadeCulling; }

	inline void EnableShadowScreenSizeCheck(bool enable) { m_bEnableShadowScreenSizeCheck = enable; }
	inline bool ShadowScreenSizeCheckEnabled() const { return m_bEnableShadowScreenSizeCheck; }

	inline void  SetCascadeLodDistanceScale(float scale, int index);
	inline float GetCascadeLodDistanceScale(int index) const;

	inline void      SetCascadeLodDistanceScalesV(Vec4V_In scales) { m_cascadeLodDistanceScale = scales; }
	inline Vec4V_Out GetCascadeLodDistanceScalesV() const;

	void GetCascades(Mat44V_In worldToOcclusion, Vec4V_InOut x, Vec4V_InOut y, Vec4V_InOut z, Vec4V_InOut r);
	Vec3V GetLightDirection() const {
		return m_shadowToWorld33.c();
	}

	const Mat33V& GetShadowToWorld33() const{
		return m_shadowToWorld33;
	}

	const Mat33V& GetWorldToShadow33() const{
		return m_worldToShadow33;
	}

	Vec2V_Out GetShadowCamPos() const{
		return m_shadowCamPos;
	}

	bool IsFrustumFacingShadow() const{
		return m_IsFrustumFacingShadow;
	}

private:
	void PrecalculateRayInterection();

	Mat34V   m_viewToWorld;
	Vec4V    m_viewTanFOVZ; // {tanHFOV*z1,tanVFOV*z1,-z1,threshold}
	Vec4V    m_viewTanFOVShear; // {-left,-bottom,right,top}*z1
	Mat33V   m_shadowToWorld33;
	Mat33V   m_worldToShadow33;
	Vec2V    m_recvrHeightMinMax;
	Vec4V    m_transposedPlanes[CASCADE_SHADOWS_SCAN_NUM_PLANES];
	Vec4V	 m_precalulatedRayIntersectionDenom[CASCADE_SHADOWS_SCAN_NUM_PLANES/2];
	Vec4V    m_shadowSpheresX; // cascade centres in shadowspace (assumes <= 4 cascades)
	Vec4V    m_shadowSpheresY;
	Vec4V    m_shadowSpheresZ;
	Vec4V    m_shadowSpheresR;
	Vec3V    m_shadowBoxTestAX; // precalc data for fast cascade box test
	Vec3V    m_shadowBoxTestAY;
	Vec3V    m_shadowBoxTestAZ;
	Vec3V    m_shadowBoxTestEX;
	Vec3V    m_shadowBoxTestEY;
	Vec3V    m_shadowBoxTestEZ;
	Vec4V    m_cascadeBoxThresholds;
	Vec4V    m_cascadeSphereThresholds; // TODO -- evaluate this and remove if it's not needed
	Vec3V    m_cascadeDistances;
	Vec4V    m_cascadeLodDistanceScale;
	VecBoolV m_cascadeExclusionFlags;

	Vec2V    m_shadowCamPos;
	bool     m_IsFrustumFacingShadow;
	u8       m_forceVisibilityFlags; // MAGDEMO -- temporary hack for player switch shadows (not sure if it's really necessary)
	bool     m_forceNoBackfacingPlanes; // hack for BS#1381352
	bool     m_useCascadeLodDistanceCulling;
	bool	 m_alwaysCullGrass;				//Low quality settings
	bool	 m_allowGrassCascadeCulling;	//State that defines whether or not entire cascade culling for grass is allowed.
	bool	 m_bEnableShadowScreenSizeCheck;
#if __BANK && !__SPU
	bool	 m_UseAreaCulling;
	bool     m_isOrthographic;
#endif // __BANK && !__SPU

#if CASCADE_SHADOWS_SCAN_DEBUG
	fwScanCascadeShadowDebug m_debug;
#endif // CASCADE_SHADOWS_SCAN_DEBUG
};

inline bool fwScanCascadeShadowInfo::CascadeLodDistanceCullingEnabled() const 
{ 
#if CASCADE_SHADOWS_SCAN_DEBUG
	if (!m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_CULL_CASCADES_BY_LOD_DISTANCE))
	{
		return false;
	}
#endif // CASCADE_SHADOWS_SCAN_DEBUG

	return m_useCascadeLodDistanceCulling; 
}

inline void fwScanCascadeShadowInfo::SetCascadeLodDistanceScale(float scale, int index)
{ 
	Assert(index >= 0 && index < 4);
	m_cascadeLodDistanceScale[index] = scale;
}

inline float fwScanCascadeShadowInfo::GetCascadeLodDistanceScale(int index) const
{
	Assert(index >= 0 && index < 4);
#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_OVERRIDE_CASCADES_LOD_DISTANCE_SCALE))
	{
		return m_debug.m_cascadeLodDistanceScale[index];
	}
#endif // CASCADE_SHADOWS_SCAN_DEBUG

	return m_cascadeLodDistanceScale[index];
}

inline Vec4V_Out fwScanCascadeShadowInfo::GetCascadeLodDistanceScalesV() const 
{ 
#if CASCADE_SHADOWS_SCAN_DEBUG
	if (m_debug.IsFlagSet(CSM_SCAN_DEBUG_FLAG_OVERRIDE_CASCADES_LOD_DISTANCE_SCALE))
	{
		return m_debug.m_cascadeLodDistanceScale;
	}
#endif // CASCADE_SHADOWS_SCAN_DEBUG

	return m_cascadeLodDistanceScale; 
}

} // namespace rage

#if __SPU
#include "fwscene/scan/ScanCascadeShadows.cpp"
#endif // __SPU

#endif // _INC_FWSCENE_SCAN_SCANCASCADESHADOWS_H_
