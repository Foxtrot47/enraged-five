//////////////
// Includes //
//////////////

#if !__TOOL

#include "fwscene/scan/Scan.h"

// framework headers
#include "fwsys/gameskeleton.h"
#include "fwscene/scan/ScanDebug.h"
#include "fwscene/scan/ScanNodes.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwscene/scan/ScanNodesDebug.h"

// rage headers
#include "profile/page.h"
#include "profile/group.h"
#include "profile/element.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "softrasterizer/scansetup.h"
#include "system/ppu_symbol.h"
#include "system/dependencyscheduler.h"
#include "system/nelem.h"
#include "system/spinlockedobject.h"
#include "system/threadtype.h"

//////////////////////
// Global variables //
//////////////////////

DECLARE_FRAG_INTERFACE( ScanNodes );
DECLARE_FRAG_INTERFACE( ScanOcclusionWrapper );
DECLARE_FRAG_INTERFACE( ScanEntities );

#if __BANK
rage::u32						g_spuPointerFixupUs;
rage::u32						g_spuNodeProcessUs;
rage::u32						g_spuExteriorEntitiesProcessUs;
#endif

rage::fwScanBaseInfo			g_scanBaseInfo ALIGNED(128);
rage::Vec4V						g_screenQuadStorage[ rage::SCAN_SCREENQUAD_STORAGE_COUNT ][ rage::fwScanNodes::STORED_SCREEN_QUAD_COUNT ] ALIGNED(128);
rage::fwPvsEntry				g_pvsEntryScratch[ rage::SCAN_MAX_ENTITIES_PER_JOB ];	// Scratch buffer for PPU visibility (PVS)
rage::fwScanResults				g_pendingScanResults ;
rage::fwScanResults				g_currentScanResults;

rage::sysDependency				g_scanNodesDependency;
rage::sysDependency				g_scanEntitiesDependencies[ MAX_SCAN_ENTITIES_DEPENDENCY_COUNT ];
rage::u32						g_scanTasksPending = 0;
#if __WIN32PC
rage::sysIpcEvent				g_scanTasksReady = NULL;
#endif
rage::sysDependency*			g_scanEntitiesDependenciesEa = g_scanEntitiesDependencies;

#if SCL_LOGGING
extern bool g_forceWait;
#endif

namespace rage {

#if __BANK
PF_PAGE( Scan, "Scan" );
PF_GROUP( Scan_JobStats );
PF_GROUP( Scan_Timings );
PF_LINK( Scan, Scan_JobStats );
PF_LINK( Scan, Scan_Timings );

PF_VALUE_INT( MaxWaitingJobs, Scan_JobStats );
PF_VALUE_INT( MaxPendingJobs, Scan_JobStats );
PF_VALUE_INT( MaxZombieJobs, Scan_JobStats );
PF_VALUE_INT( TotalNodes, Scan_JobStats );

PF_TIMER( TotalVisibilityTime, Scan_Timings );
PF_VALUE_FLOAT( SpuPointerFixupTime, Scan_Timings );
PF_VALUE_FLOAT( SpuNodeProcessTime, Scan_Timings );
PF_VALUE_FLOAT( SpuExteriorEntitiesProcessTime, Scan_Timings );
#endif // __BANK

//////////////////////
// Static variables //
//////////////////////

fwSceneGraphNode*		fwScan::ms_rootSceneNode;
float					fwScan::ms_primaryLodScale;
fwInteriorLocation		fwScan::ms_initialInteriorLocation;
Vec4V*					fwScan::ms_hiZBuffers[ SCAN_OCCLUSION_STORAGE_COUNT ];
bool					fwScan::ms_scanStarted = false;


#if __BANK
char					fwScan::ms_processVisibilityOnPpuText[32] = "";
bool					fwScan::ms_forceVisibilityOnPpu = false;
bool					fwScan::ms_forceEnableOcclusion = false;
bool					fwScan::ms_forceDisableOcclusion = false;
bool					fwScan::ms_forceEnableShadowOcclusion = false;
bool					fwScan::ms_forceDisableShadowOcclusion = false;
bkGroup*				fwScan::ms_shadowProxyGroup = NULL;
bkGroup*				fwScan::ms_reflectionProxyGroup = NULL;
#endif


void fwScan::InitClass()
{
#if __WIN32PC
	g_scanTasksReady = sysIpcCreateEvent();
#endif
}

void fwScan::ShutDownClass()
{
#if __WIN32PC
	sysIpcDeleteEvent(g_scanTasksReady);
#endif
}

////////////////////////////
// PPU / SPU scan methods //
////////////////////////////
CompileTimeAssert( (RAST_HIZ_AND_STENCIL_SIZE_BYTES&0xf) ==0 );

void fwScan::StartScanDependencies()
{
	// 8/15/12 - cthomas - Changing the priority of all scan jobs in the dependency system from Medium -> High.
	// These jobs are waited on by the main thread only a few ms after they are launched, and so need to have the 
	// highest priority possible (without being critical, reserved for only for those jobs perhaps launched and 
	// immediately waited on). At the least, changing to high priority now makes the priority equal to that of 
	// the jobs launched by the animation system right now, and animation generally has a bit more leeway time 
	// than the scan jobs.

	u32 sysDepFlags = sysDepFlag::ALLOC0 | sysDepFlag::INPUT1 | sysDepFlag::INPUT2;
	int scratchSize = fwScanNodes::SCRATCH_BUFFER_SIZE;
	int scanResultsDataSize;
	void *pScanResults;
	if (g_scanNodesDependency.m_Chain == sysDependency::kChainPpu)
	{
		sysDepFlags |= sysDepFlag::OUTPUT3;
		scanResultsDataSize = sizeof(fwScanResults);
		pScanResults = &g_pendingScanResults;
	}
	else
	{
		scratchSize += sizeof(fwScanResults);
		scanResultsDataSize = 0;
		pScanResults = NULL;
	}

	g_scanNodesDependency.m_Priority = sysDependency::kPriorityCritical;
	g_scanNodesDependency.m_Flags = sysDepFlags;
	g_scanNodesDependency.m_Params[1].m_AsPtr = fwSceneGraph::GetPoolStorage();
	g_scanNodesDependency.m_Params[2].m_AsPtr = &g_scanBaseInfo;
	g_scanNodesDependency.m_Params[3].m_AsPtr = pScanResults;
	g_scanNodesDependency.m_Params[4].m_AsPtr = ms_hiZBuffers[ SCAN_OCCLUSION_STORAGE_PRIMARY ];
	g_scanNodesDependency.m_Params[5].m_AsPtr = ms_hiZBuffers[ SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS ];
	g_scanNodesDependency.m_Params[6].m_AsPtr = fwSceneGraph::GetPoolStorage();
	g_scanNodesDependency.m_Params[7].m_AsPtr = ms_rootSceneNode;
	g_scanNodesDependency.m_Params[8].m_AsFloat = ms_primaryLodScale;
	g_scanNodesDependency.m_DataSizes[0] = scratchSize;
	g_scanNodesDependency.m_DataSizes[1] = fwSceneGraph::GetPoolStorageSize();
	g_scanNodesDependency.m_DataSizes[2] = sizeof(fwScanBaseInfo);
	g_scanNodesDependency.m_DataSizes[3] = scanResultsDataSize;

	g_scanTasksPending = 1;
	sysDependencyScheduler::Insert( &g_scanNodesDependency );
}

static bool CompareCullShapes(const fwRenderPhaseCullShape& a, const fwRenderPhaseCullShape& b)
{
	if ( (a.GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) && !(b.GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) )
		return true;

	if ( (a.GetFlags() & CULL_SHAPE_FLAG_TRAVERSE_PORTALS) && !(b.GetFlags() & CULL_SHAPE_FLAG_TRAVERSE_PORTALS) )
		return true;

	if ( (a.GetFlags() & CULL_SHAPE_FLAG_PRIMARY) && (b.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS) )
		return true;

	return false;
}

#if __WIN32PC
static int CompareCullShapesWrapper(const void *a,const void *b) 
{
	// No idea if this is sufficient since qsort wants ==0 for the "is equal" case so the sort might not be stable?
	return CompareCullShapes(*(fwRenderPhaseCullShape*)a,*(fwRenderPhaseCullShape*)b)? -1 : 1;
}
#endif

void fwScan::SortCullShapes() {
#if __WIN32PC
	// Sigh, PC compiler attempts to create an unaligned cull shape on the stack, which chokes because it uses vector insns to do the copy
	qsort(g_scanBaseInfo.m_cullShapes, g_scanBaseInfo.m_cullShapeCount, sizeof(fwRenderPhaseCullShape), CompareCullShapesWrapper);
#else
	std::sort( g_scanBaseInfo.m_cullShapes, g_scanBaseInfo.m_cullShapes + g_scanBaseInfo.m_cullShapeCount, CompareCullShapes );
#endif
}

void fwScan::ResetCullShapes() {
	g_scanBaseInfo.m_cullShapeCount = 0;
}

fwRenderPhaseCullShape& fwScan::AddCullShape()
{
	Assert( g_scanBaseInfo.m_cullShapeCount < SCAN_RENDER_PHASE_COUNT );
	return g_scanBaseInfo.m_cullShapes[ g_scanBaseInfo.m_cullShapeCount++ ];
}

fwScanBaseInfo& fwScan::GetScanBaseInfo() {
	return g_scanBaseInfo;
}

fwScanResults& fwScan::GetScanResults() {
	Assert(rage::sysThreadType::IsUpdateThread());
	return g_currentScanResults;
}

bool fwScan::GetGbufUsesScreenQuadPairs() {
	return GetScanResults().GetGbufUsesScreenQuadPairs();
}

static Vec4V_Out GetScreenQuadStorage(const fwSceneGraphNode* sceneNode)
{
	const s16		storageIndex = fwScanNodes::GetStoredScreenQuadIndex( sceneNode );
	const Vec4V		quadStorage = g_screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_PRIMARY ][ storageIndex ];

	return quadStorage;
}

fwScreenQuadPair fwScan::GetGbufScreenQuadPair(const fwSceneGraphNode* sceneNode)
{
	Assert( GetGbufUsesScreenQuadPairs() );
	return fwScreenQuadPair( GetScreenQuadStorage(sceneNode) );
}

fwScreenQuad fwScan::GetGbufScreenQuad(const fwSceneGraphNode* sceneNode)
{
	Assert( !GetGbufUsesScreenQuadPairs() );
	return fwScreenQuad( GetScreenQuadStorage(sceneNode) );
}

void fwScan::StartScan()
{
#if __BANK
	PF_START( TotalVisibilityTime );
	g_spuPointerFixupUs = 0;
	g_spuNodeProcessUs = 0;
	g_spuExteriorEntitiesProcessUs = 0;
#endif // __BANK

	Assert(!ms_scanStarted);
	ms_scanStarted = true;
	fwSceneGraph::LockSceneGraph();

	// In case cascade shadows occlusion is not enabled, just use the GBuf occlusion framebuffer for both -
	// it won't be used anyway in the entity visibility jobs.
	if ( Unlikely((g_scanMutableDebugFlags & SCAN_MUTABLE_DEBUG_ENABLE_OCCLUSION) && !(g_scanMutableDebugFlags & SCAN_MUTABLE_DEBUG_ENABLE_SHADOW_OCCLUSION)) )
		ms_hiZBuffers[ SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS ] = ms_hiZBuffers[ SCAN_OCCLUSION_STORAGE_PRIMARY ];

	fwScan::SortCullShapes();
	fwScan::StartScanDependencies();
}

void fwScan::JoinScan()
{
	sysSpinner	spinner;
	spinner.Start();


	if (!ms_scanStarted)
	{
		return;
	}

	ms_scanStarted = false;

	while (true)
	{
		volatile u32 *pScanTasksPending = &g_scanTasksPending;

		if ( *pScanTasksPending == 0 )
			break;

		spinner.Spin();
		sysIpcYield( PRIO_NORMAL );
	}

	g_currentScanResults = g_pendingScanResults;

	fwSceneGraph::UnlockSceneGraph();

#if __BANK
	PF_STOP( TotalVisibilityTime );
	PF_SET( SpuPointerFixupTime, static_cast< float >( g_spuPointerFixupUs ) / 1000 );
	PF_SET( SpuNodeProcessTime, static_cast< float >( g_spuNodeProcessUs ) / 1000 );
	PF_SET( SpuExteriorEntitiesProcessTime, static_cast< float >( g_spuExteriorEntitiesProcessUs ) / 1000 );
#endif
}

void fwScan::SetupDependencies()
{
	BANK_ONLY( fwScan::UpdateForceProcessVisibilityOnPpu(); )

#if __PPU
	if (!(g_scanDebugFlags & SCAN_DEBUG_PROCESS_VISIBILITY_ON_PPU) )
	{
		g_scanNodesDependency.Init( FRAG_SPU_CODE(ScanNodes), 0, 0 );
	}
	else
#endif
	{
		g_scanNodesDependency.Init( fwScanNodes::RunFromDependency, 0, 0 );
	}

	g_scanTasksPending = 0;
}

fwScreenQuad fwScan::GetGBuffExteriorScreenQuad()
{
	int gbuffExteriorNodeScreenQuadIndex = g_pendingScanResults.GetGbuffExteriorNodeScreenQuadIndex();
	if (gbuffExteriorNodeScreenQuadIndex == -1)
	{
		return fwScreenQuad(SCREEN_QUAD_FULLSCREEN);
	}

	fwScreenQuadPair quadPair(g_screenQuadStorage[SCAN_SCREENQUAD_STORAGE_PRIMARY][gbuffExteriorNodeScreenQuadIndex]);

	fwScreenQuad combinedQuad(quadPair.GetScreenQuad0());
	combinedQuad.Grow(quadPair.GetScreenQuad1());
	return combinedQuad;
}

/////////////////////
// Debug method(s) //
/////////////////////
#if __BANK


void fwScan::InitWidgets()
{
	bkBank * pBank = BANKMGR.FindBank( "World Scan & Search" );
	if (!pBank)
		pBank = &BANKMGR.CreateBank( "World Scan & Search" );

	pBank->PushGroup( "Scan" );

	pBank->AddText( "",												ms_processVisibilityOnPpuText, sizeof(ms_processVisibilityOnPpuText), true );
	pBank->AddToggle( "Trace container for selected",				fwScanNodesDebug::GetBreakOnSelectedEntityInContainer(), fwScanNodesDebug::GetBreakOnSelectedEntityInContainerCallback() );
	pBank->AddToggle( "Trace selected entity",						&g_scanDebugFlags, SCAN_DEBUG_TRACE_SELECTED_ENTITY );
	pBank->AddToggle( "Force Visibility on PPU",					&ms_forceVisibilityOnPpu );
	pBank->AddToggle( "Process entities on both SPU and PPU",		&g_scanDebugFlags, SCAN_DEBUG_PROCESS_VISIBILITY_ON_BOTH_PPU_AND_SPU );
	pBank->AddSeparator();
	pBank->AddSlider( "Phase Visibility Mask Override for selected entity", &g_scanPhaseVisibilityMaskOverride, 0, 65535, 1, datCallback(SetPhaseVisibilityMaskOverrideOnSelectedEntity) );
	pBank->AddButton( "Get Phase Visibility Mask for selected entity", datCallback(GetPhaseVisibilityMaskOnSelectedEntity) );
	pBank->AddSeparator();

#if SCL_LOGGING
	pBank->AddToggle( "Force Stall to output Scan Log",				&g_forceWait );
#endif

	pBank->AddToggle( "Display Streamed Peds not rendered because of missing Render GFX", &g_displyPedsMissingGfx );
	pBank->AddToggle( "Log Streamed Peds not rendered because of missing Render GFX", &g_logPedsMissingGfx );
	pBank->PushGroup( "Early kick", false );
	pBank->AddToggle( "Early kick occlusion jobs",					&g_scanDebugFlags, SCAN_DEBUG_EARLY_KICK_OCCLUSION_JOBS );
	pBank->AddToggle( "Scan linked PortalSceneGraphNodes",			&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_SCAN_LINKED_PORTALSCENEGRAPHNODE);
	pBank->PopGroup();

	pBank->PushGroup( "Rendering helpers", false );
	pBank->AddToggle( "Don't render limbo entities",				&g_scanDebugFlags, SCAN_DEBUG_DONT_RENDER_LIMBO_ENTITIES );
	pBank->AddToggle( "Don't render exterior",						&g_scanDebugFlags, SCAN_DEBUG_DONT_RENDER_EXTERIOR );
	pBank->AddToggle( "Don't render interior",						&g_scanDebugFlags, SCAN_DEBUG_DONT_RENDER_INTERIOR );
	pBank->AddToggle( "Enable Directional Light for room",			&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_ENABLE_DIRECTIONAL_LIGHT_FOR_ROOM );
	pBank->AddToggle( "Enable Portal straddling container use",		fwScanNodesDebug::GetPortalStraddlingEnabled() );
	pBank->AddToggle( "Display Portal straddling debug info",		fwScanNodesDebug::GetDisplayPortalStraddlingDebugDraw() );
	pBank->PopGroup();

	pBank->PushGroup( "Early reject", false );
	pBank->AddToggle( "Extra distance cull",						&g_scanDebugFlags, SCAN_DEBUG_EARLY_REJECT_EXTRA_DIST_CULL );
	// NOTE -- picker-controlled flags get set every frame, so setting them here does nothing
	// it would be preferable to have the picker and scan systems share the same flags in memory
	pBank->AddToggle( "PICKER CONTROLLED: Reject all but selected",	&g_scanDebugFlags, SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_SELECTED );
	pBank->AddToggle( "PICKER CONTROLLED: Reject selected",			&g_scanDebugFlags, SCAN_DEBUG_EARLY_REJECT_SELECTED );
	pBank->AddToggle( "PICKER CONTROLLED: Don't reject selected",	&g_scanDebugFlags, SCAN_DEBUG_DONT_EARLY_REJECT_SELECTED );
	pBank->AddToggle( "Don't reject any",							&g_scanDebugFlags, SCAN_DEBUG_DONT_EARLY_REJECT_ANY );
	pBank->PopGroup();

	pBank->PushGroup( "Visibility bypass", false );
	pBank->AddToggle( "Bypass excluded in renderphase",				&g_scanDebugFlags, SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_IN_RENDERPHASE );
	pBank->AddToggle( "Bypass clipped",								&g_scanDebugFlags, SCAN_DEBUG_VISIBILITY_BYPASS_CLIPPED );
	pBank->AddToggle( "Bypass excluded by LOD range",				&g_scanDebugFlags, SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_BY_LOD_RANGE );
	pBank->AddToggle( "Bypass excluded by ped/veh/obj distance",	&g_scanDebugFlags, SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_BY_PED_VEH_OBJ_DISTANCE );
	pBank->AddToggle(" Bypass node vis tags test",					&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_BYPASS_NODE_VIS_TAGS_TEST );
	pBank->PopGroup();

	pBank->PushGroup( "Occlusion", false );
	pBank->AddToggle( "Force enable occlusion",						&ms_forceEnableOcclusion );
	pBank->AddToggle( "Force disable occlusion",					&ms_forceDisableOcclusion );
	pBank->AddToggle( "Flip gbuffer occlusion results",				&g_scanDebugFlags, SCAN_DEBUG_FLIP_OCCLUSION_RESULT );
	pBank->AddToggle( "Flip shadow occlusion results",				&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_FLIP_SHADOW_OCCLUSION_RESULT );
	pBank->AddToggle( "Flip water occlusion results",				&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_FLIP_WATER_OCCLUSION_RESULT );
	pBank->AddToggle( "Trim occlusion buffers with portals etc.",	&g_scanDebugFlags, SCAN_DEBUG_TRIM_OCCLUSION_BUFFERS );
	pBank->AddToggle( "Water stencil test - reference code",		&g_scanDebugFlagsPortals, SCAN_DEBUG_USE_NEW_WATER_STENCIL_TEST_REFERENCE_CODE );
	pBank->AddToggle( "Water stencil test - non-vector code",		&g_scanDebugFlagsPortals, SCAN_DEBUG_USE_NEW_WATER_STENCIL_TEST_NONVECTOR_CODE );
	pBank->AddToggle( "Water stencil test - debug",					&g_scanDebugFlagsPortals, SCAN_DEBUG_USE_NEW_WATER_STENCIL_TEST_DEBUG );
	pBank->PopGroup();

	pBank->PushGroup( "Shadows", false );
	pBank->AddToggle( "Force enable shadow occlusion",				&ms_forceEnableShadowOcclusion );
	pBank->AddToggle( "Force disable shadow occlusion",				&ms_forceDisableShadowOcclusion );
	pBank->AddToggle( "Use exterior restricted frustum",			&g_scanDebugFlags, SCAN_DEBUG_USE_EXTERIOR_RESTRICTED_FRUSTUM );
	pBank->AddToggle( "Use swept frustum for Portal culling",		&g_scanDebugFlags, SCAN_DEBUG_USE_SWEPT_FRUSTUM_FOR_PORTAL_CULLING );
	pBank->AddToggle( "Hide non shadow casting entities",			&g_scanDebugFlags, SCAN_DEBUG_HIDE_NON_SHADOW_CASTING_ENTITIES );
	pBank->AddToggle( "Force non shadow casting entities in shadow",&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_FORCE_NON_SHADOW_CASTING_ENTITIES_IN_SHADOW );
	pBank->AddToggle( "Always add current room and room 0 to CSM",	&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_ADD_CURRENT_ROOM_AND_ROOM0_IF_CSM_ACTIVE );
	pBank->AddToggle( "Force add current room and room 0 to CSM",	&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_FORCE_ADD_CURRENT_ROOM_AND_ROOM0 );
	pBank->PopGroup();

	pBank->PushGroup( "Reflections", false );
	pBank->AddToggle( "Force non reflection entities in reflection",&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_FORCE_NON_REFLECTION_ENTITIES_IN_REFLECTION );
	pBank->PopGroup();

	pBank->PushGroup( "Streaming", false );
	pBank->AddToggle( "Enable HD stream cull shape",				&g_scanDebugFlags, SCAN_DEBUG_ENABLE_HD_STREAM_CULL_SHAPE );
	pBank->AddToggle( "Enable streaming volume cull shape",			&g_scanDebugFlags, SCAN_DEBUG_ENABLE_STREAMING_VOLUME_CULL_SHAPE );
	pBank->AddToggle( "Enable interior stream cull shape",			&g_scanDebugFlags, SCAN_DEBUG_ENABLE_INTERIOR_STREAM_CULL_SHAPE );
	pBank->PopGroup();

	pBank->PushGroup( "PreRender", false );
	pBank->AddToggle( "Enable PreRender sphere",					&g_scanDebugFlags, SCAN_DEBUG_ENABLE_PRERENDER_SPHERE );
	pBank->PopGroup();

	pBank->PushGroup( "Frustum culling", false );
	pBank->AddToggle( "Force 8-plane SoA frustum culling",			&g_scanDebugFlags, SCAN_DEBUG_FORCE_8_PLANE_SOA_FRUSTUM_CULLING );

	pBank->AddSlider( "Room index to Snapshot",						fwScanNodesDebug::GetDebugSnapShotRoomIndex(), -1, 4096, 1 );
	pBank->AddSlider( "Interior index to Snapshot",					fwScanNodesDebug::GetDebugSnapShotInteriorIndex(), 0, 4096, 1 );
	pBank->AddButton( "Snapshot Frustum/s",							fwScanNodesDebug::SnapShotFrutumDataCallback );
	pBank->AddToggle( "Draw Debug Frustum/s",						&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_DRAW_FRUSTUMS );
	pBank->AddToggle( "Draw using PlaneSetDebugDraw",				fwScanNodesDebug::GetDrawUsingPlaneSetDebugDraw() );
	pBank->AddSlider( "Debug Frustum Alpha",						fwScanNodesDebug::GetDrawFrustumForScreenQuadAlpha(), 0.0f, 1.0f, 0.05f );
	pBank->AddSlider( "Debug Frustum Far Plane Scale",				fwScanNodesDebug::GetDrawFrustumFarPlaneScale(), 0.0f, 1.0f, 0.05f );
	pBank->AddToggle( "Draw Debug Frustum 0",						fwScanNodesDebug::GetDrawFrustum0() );
	pBank->AddToggle( "Draw Debug Frustum 1",						fwScanNodesDebug::GetDrawFrustum1() );
	pBank->AddToggle( "Draw CSS Swept Frustum",						fwScanNodesDebug::GetDrawShadowPlanes() );


	const char* pFrustumCullShapeNames[] =
	{
		"Primary GBuffer",
		"Cascade Shadow",
		"Mirror"
	};

	pBank->AddCombo("CullShape to Build Frustum with",					fwScanNodesDebug::GetDebugSnapShotCullShapeIndex(), NELEM(pFrustumCullShapeNames), pFrustumCullShapeNames );
	pBank->PopGroup();

	pBank->PushGroup( "Portal traversal", false );
	pBank->AddToggle( "Don't check portal side on traversal",			&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DONT_CHECK_PORTAL_SIDE_ON_TRAVERSAL );
	pBank->AddToggle( "Traverse disabled portals",						&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_TRAVERSE_DISABLED_PORTALS );
	pBank->AddToggle( "Traverse culled portals",						&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_TRAVERSE_CULLED_PORTALS );
	pBank->AddToggle( "Disable screen quad pairs",						&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISABLE_SCREEN_QUAD_PAIRS );
	pBank->AddToggle( "Disable mirror flip",							&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISABLE_MIRROR_FLIP );
	pBank->AddToggle( "Ignore 'Render Lod Only' flag",					&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_IGNORE_RENDER_LOD_ONLY_FLAG );
	pBank->AddToggle( "Ignore Portal Opacity",							&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_IGNORE_PORTAL_OPACITY );
	pBank->AddToggle( "Override Portal Opacity",						&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_OVERRIDE_PORTAL_OPACITY );
	pBank->AddSlider( "Opacity Override",								&g_scanDebugOverridePortalOpacity, 0.0f, 1.0f, 0.1f );
	pBank->AddToggle( "Ignore Room Exterior Visibility Depth",			&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_IGNORE_ROOM_EXTERIOR_VISIBILITY_DEPTH );
	pBank->AddToggle( "Override Room Exterior Visibility Depth",		&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_OVERRIDE_ROOM_EXTERIOR_VISIBILITY_DEPTH );
	pBank->AddSlider( "Room Exterior Visibility Depth Override",		&g_scanDebugOverrideRoomExteriorVisibilityDepth, -1, 32, 1 );
	pBank->AddToggle( "Force merge water surface portals",				&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_FORCE_MERGE_WATER_SURFACE_PORTALS );
	pBank->PopGroup();

	const char* kShowPortalTraversalGraphStrings[] =
	{
		"None",
		"Primary GBuffer",
		"Cascade Shadow",
		"Reflection",
		"Mirror",
	};
	CompileTimeAssert(NELEM(kShowPortalTraversalGraphStrings) == SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_COUNT);

	pBank->PushGroup( "Portal debug draw", false );
	pBank->AddToggle( "Display portals",							&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PORTALS );
	pBank->AddToggle( "Display portals from starting node only",	&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PORTALS_FROM_STARTING_NODE_ONLY );
	pBank->AddToggle( "Display portals traversed only",				&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PORTALS_TRAVERSED_ONLY );
	pBank->AddToggle( "Display portal indices and flags",			&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PORTAL_INDICES );
	pBank->AddToggle( "Display portal opacity",						&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PORTAL_OPACITY );
	pBank->AddToggle( "Display primary intermediate screen quads",	&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PRIMARY_INTERMEDIATE_SCREENQUADS );
	pBank->AddToggle( "Display primary final screen quads",			&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_PRIMARY_FINAL_SCREENQUADS );
	pBank->AddToggle( "Display shadow portals in shadow map",		&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_PORTALS_IN_SHADOW_MAP );
	pBank->AddToggle( "Display shadow intermediate screen quads",	&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_INTERMEDIATE_SCREENQUADS );
	pBank->AddToggle( "Display shadow final screen quads",			&g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_FINAL_SCREENQUADS );

	pBank->AddSlider( "Global Isolate portal index",											   fwScanNodesDebug::DebugDrawGlobalIsolatedPortalIndex(), -1, 65535, 1 );
	pBank->AddSlider( "CullShape id to break in (Not CCS)",										   fwScanNodesDebug::DebugDrawCullShapeIdForPortalDebugging(), 0, 24, 1);
	pBank->AddToggle( "Break in Scan Nodes to debug Isolated portal",							   &g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_BREAK_TO_DEBUG_PORTALS );
	pBank->AddToggle( "Break in Scan Nodes when AddVisibility() is called on the selected portal", &g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_PORTALS_BREAK_FOR_ADD_VISIBILITY_CALLED_ON_ISOLOATED );
	pBank->AddSlider( "Shadows Isolate portal index",											   fwScanNodesDebug::DebugDrawShadowIsolatedPortalIndex(), -1, 100, 1 );

	pBank->AddToggle( "Don't traverse one way portals if all interiors are closed.", &g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_PORTALS_DONT_TRAVERSE_ONE_WAY_IF_INTERIORS_CLOSED );

	pBank->AddToggle( "Only add exterior portal containers in ProcessInteriorPortalsVisibility()", &g_scanDebugFlagsPortals, SCAN_PORTALS_DEBUG_ONLY_EXTERIOR_PORTALS_IN_PROCESS_INTERIOR );

	pBank->AddCombo( "Show portal traversal graph",					(int*)&g_scanDebugShowPortalGraph, NELEM(kShowPortalTraversalGraphStrings), kShowPortalTraversalGraphStrings );
	pBank->AddToggle( "Ignore non-traversed portals in graph",		&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_PORTAL_TRAVERSAL_GRAPH_IGNORE_NON_TRAVERSED_PORTALS);
	pBank->PopGroup();

	ms_shadowProxyGroup = pBank->PushGroup( "Shadow proxies", false );
	pBank->AddToggle( "Force shadow proxies in primary",			&g_scanDebugFlagsPortals, SCAN_DEBUG_FORCE_SHADOW_PROXIES_IN_PRIMARY );
	pBank->AddToggle( "Hide shadow proxies in shadow",				&g_scanDebugFlagsPortals, SCAN_DEBUG_HIDE_SHADOW_PROXIES_IN_SHADOW );
	pBank->AddToggle( "Hide non-proxies in shadow",					&g_scanDebugFlagsPortals, SCAN_DEBUG_HIDE_NON_SHADOW_PROXIES_IN_SHADOW );
	pBank->PopGroup();

	ms_reflectionProxyGroup = pBank->PushGroup( "Reflection proxies", false );
	pBank->AddToggle( "Force reflection proxies in primary (parab/mirror)",	&g_scanDebugFlagsPortals, SCAN_DEBUG_FORCE_REFLECTION_PROXIES_IN_PRIMARY );
	pBank->AddToggle( "Hide proxies in reflection (parab/mirror)",			&g_scanDebugFlagsPortals, SCAN_DEBUG_HIDE_REFLECTION_PROXIES_IN_REFLECTION );
	pBank->AddToggle( "Hide non-proxies in reflection (parab/mirror)",		&g_scanDebugFlagsPortals, SCAN_DEBUG_HIDE_NON_REFLECTION_PROXIES_IN_REFLECTION );
	pBank->AddToggle( "Hide proxies in water reflection",					&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_HIDE_PROXIES_IN_WATER_REFLECTION );
	pBank->AddToggle( "Hide non-proxies in water reflection",				&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_HIDE_NON_PROXIES_IN_WATER_REFLECTION );
	pBank->AddToggle( "Prereflected water proxies use gbuf frustum",		&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_WATER_PROXY_PRE_REFLECTED_USE_GBUF_FRUSTUM );
	pBank->AddToggle( "Water proxies ignore LOD mask",						&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_WATER_REFLECTION_PROXIES_IGNORE_LOD_MASK );
	pBank->AddToggle( "Water proxies ignore LOD ranges",					&g_scanDebugFlagsExtended, SCAN_DEBUG_EXT_WATER_REFLECTION_PROXIES_IGNORE_LOD_RANGES );
	pBank->PopGroup();

	pBank->PopGroup();
}

void fwScan::InitOptimizationWidgets()
{
	bkBank* pOptimisationsBank = BANKMGR.FindBank("Optimization");
	if(pOptimisationsBank)
	{
		pOptimisationsBank->AddToggle( "Don't render exterior",		&g_scanDebugFlags, SCAN_DEBUG_DONT_RENDER_EXTERIOR );
		pOptimisationsBank->AddToggle( "Don't render interior",		&g_scanDebugFlags, SCAN_DEBUG_DONT_RENDER_INTERIOR );
	}
}

void fwScan::UpdateForceProcessVisibilityOnPpu()
{
	bool	processVisibilityOnPpu = ms_forceVisibilityOnPpu || ( g_scanDebugFlagsPortals & (
		SCAN_PORTALS_DEBUG_DISPLAY_PORTALS                            |
		SCAN_PORTALS_DEBUG_DISPLAY_PORTALS_FROM_STARTING_NODE_ONLY    |
		SCAN_PORTALS_DEBUG_DISPLAY_PRIMARY_INTERMEDIATE_SCREENQUADS   |
		SCAN_PORTALS_DEBUG_DISPLAY_PRIMARY_FINAL_SCREENQUADS          |
		SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_PORTALS_IN_SHADOW_MAP       |
		SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_INTERMEDIATE_SCREENQUADS    |
		SCAN_PORTALS_DEBUG_DISPLAY_SHADOW_FINAL_SCREENQUADS           |
	0) ) ? true : false;

	if ( (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTAL_INDICES) != 0 ||
		(g_scanDebugShowPortalGraph != SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_NONE) )
	{
		processVisibilityOnPpu = true;
	}

	strcpy( ms_processVisibilityOnPpuText, processVisibilityOnPpu ? "Processing visibility on PPU" : "" );
	g_scanDebugFlags = (g_scanDebugFlags & ~SCAN_DEBUG_PROCESS_VISIBILITY_ON_PPU) |
		(processVisibilityOnPpu ? SCAN_DEBUG_PROCESS_VISIBILITY_ON_PPU : 0);
}

void fwScan::UpdateForceEnableOcclusion()
{
	Assertf( !(ms_forceEnableOcclusion && ms_forceDisableOcclusion),
		"Cannot force enable and disable occlusion at the same time" );

	Assertf( !(ms_forceEnableShadowOcclusion && ms_forceDisableShadowOcclusion),
		"Cannot force enable and disable shadow occlusion at the same time" );

	if ( ms_forceEnableOcclusion )
		g_scanMutableDebugFlags = (g_scanMutableDebugFlags & ~SCAN_MUTABLE_DEBUG_ENABLE_OCCLUSION) | SCAN_MUTABLE_DEBUG_ENABLE_OCCLUSION;

	if ( ms_forceDisableOcclusion )
		g_scanMutableDebugFlags = (g_scanMutableDebugFlags & ~SCAN_MUTABLE_DEBUG_ENABLE_OCCLUSION);

	if ( ms_forceEnableShadowOcclusion )
		g_scanMutableDebugFlags = (g_scanMutableDebugFlags & ~SCAN_MUTABLE_DEBUG_ENABLE_SHADOW_OCCLUSION) | SCAN_MUTABLE_DEBUG_ENABLE_SHADOW_OCCLUSION;

	if ( ms_forceDisableShadowOcclusion )
		g_scanMutableDebugFlags = (g_scanMutableDebugFlags & ~SCAN_MUTABLE_DEBUG_ENABLE_SHADOW_OCCLUSION);

}
#endif // __BANK

} // namespace rage

#endif // !__TOOL
