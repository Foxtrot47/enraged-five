#ifndef _INC_SCANRESULTENTRY_H_
#define _INC_SCANRESULTENTRY_H_

#include "fwutil/Flags.h"
#include "fwscene/scan/VisibilityFlags.h"
#include "fwscene/scan/ScreenQuad.h"
#include "fwscene/scan/ScanCascadeShadows.h"
#include "fwscene/world/InteriorSceneGraphNode.h"
#if __SPU
#include "fwscene/world/SceneGraph.h"
#endif // __SPU

namespace rage {

class fwSceneGraphNode;
class fwPortalSceneGraphNode;
class fwEntity;

class fwPvsEntry
{
protected:

	fwEntity*			m_pEntity;
	float				m_fDist;
	fwVisibilityFlags	m_visFlags;

public:
	void Reset()											{ memset( this, 0, sizeof(fwPvsEntry) ); }
	
	void Invalidate()										{ SetEntity(NULL); }
	void SetEntity(fwEntity* pEntity)						{ m_pEntity = pEntity; }
	fwEntity* GetEntity() const								{ return m_pEntity; }

	void SetDist(const float fDist)							{ m_fDist = fDist; }
	float GetDist() const									{ return m_fDist; }
	void AddToDist(const float fDist)						{ m_fDist += fDist; }
	
	void SetVisFlags(const fwVisibilityFlags& visFlags)		{ m_visFlags = visFlags; }
	fwVisibilityFlags& GetVisFlags()						{ return m_visFlags; }
	const fwVisibilityFlags& GetVisFlags() const			{ return m_visFlags; }

	bool operator >(const fwPvsEntry& op) const				{ return m_fDist > op.m_fDist; }
	bool operator >=(const fwPvsEntry& op) const			{ return m_fDist >= op.m_fDist; }
};

struct fwScanResultBlock {
	// Pointer to first element in the result block
	fwPvsEntry *m_PvsEntry;

	// Number of PVS entries in the block
	int m_EntryCount;
};

#define SCAN_SKY_PORTALS_FOR_REFLECTION_EXT    (1 && __DEV) // this is useful for debugging

#if SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
	#define SCAN_SKY_PORTALS_FOR_REFLECTION_EXT_ONLY(x) x
#else
	#define SCAN_SKY_PORTALS_FOR_REFLECTION_EXT_ONLY(x)
#endif

template <int MAX_SKY_PORTALS> class fwScanResultsSkyPortals
{
public:
	void ClearSkyPortals()
	{
		m_skyPortalCount = -1;
#if SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
		m_extPortalValid = false;
#endif // SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
	}

	void AddSkyPortal(const fwPortalSceneGraphNode* skyPortalNode)
	{
		if (GetIsSkyPortalVisInterior() && m_skyPortalCount < MAX_SKY_PORTALS)
		{
			const u16 portalNodeIndex = skyPortalNode->GetPortalNodeIndex();

			if ((m_skyPortalFlags[portalNodeIndex/8] & BIT(portalNodeIndex%8)) == 0)
			{
				m_skyPortalFlags[portalNodeIndex/8] |= BIT(portalNodeIndex%8);
				m_skyPortals[m_skyPortalCount++] = skyPortalNode;
			}
		} 
	}

	void SetIsSkyPortalVisInterior()									{ Assert(m_skyPortalCount == -1); m_skyPortalCount = 0; memset(m_skyPortalFlags, 0, sizeof(m_skyPortalFlags)); }
	bool GetIsSkyPortalVisInterior() const								{ return m_skyPortalCount >= 0; }
	int GetSkyPortalCount() const										{ if (GetIsSkyPortalVisInterior()) { return m_skyPortalCount; } return 0; }
	const fwPortalCorners& GetSkyPortalCorners(int index) const			{ Assert(index >= 0 && index < m_skyPortalCount && GetIsSkyPortalVisInterior()); return m_skyPortals[index]->GetCorners(); }
#if SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
	void AddExteriorToInteriorPortalCorners(const fwPortalCorners& pc)	{ m_extPortal = pc; m_extPortalValid = true; }
	const fwPortalCorners* GetExtPortalCorners() const					{ return m_extPortalValid ? &m_extPortal : NULL; }
#endif // SCAN_SKY_PORTALS_FOR_REFLECTION_EXT

#if __SPU
	void UnfixupPointers(const fwPointerFixer& fixer)
	{
		for (int i = 0; i < m_skyPortalCount; i++)
		{
			fixer.Unfix( m_skyPortals[i] );
		}
	}
#endif // __SPU

private:
	const fwPortalSceneGraphNode*	m_skyPortals[ MAX_SKY_PORTALS ];
	int								m_skyPortalCount; // will be -1 if scan starts in the exterior
	u8								m_skyPortalFlags[ (fwSceneGraph::PORTAL_SCENE_NODE_POOL_SIZE+7)/8 ];
#if SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
	fwPortalCorners					m_extPortal;
	bool							m_extPortalValid;
#endif // SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
};

class fwScanResultsSkyPortalExists
{
public:
	void ClearSkyPortals()					{ m_skyPortalCount = -1; }
	bool AddSkyPortal(const fwScreenQuad&)	{ if (GetIsSkyPortalVisInterior()) { m_skyPortalCount++; return true; } return false; }
	void SetIsSkyPortalVisInterior()		{ Assert(m_skyPortalCount == -1); m_skyPortalCount = 0; }
	bool GetIsSkyPortalVisInterior() const	{ return m_skyPortalCount >= 0; }
	int GetSkyPortalCount() const			{ if (GetIsSkyPortalVisInterior()) { return m_skyPortalCount; } return 0; }

private:
	int m_skyPortalCount; // will be -1 if scan starts in the exterior
};

class fwScanResults
{
private:

	enum {
		EXTERIOR_VISIBLE_IN_GBUF			= 1 << 0,
		GBUF_USES_SCREEN_QUAD_PAIRS			= 1 << 1,
		DIRECTIONAL_LIGHT_ENABLED_FOR_ROOM	= 1 << 2,
		MIRROR_VISIBLE						= 1 << 3,
		MIRROR_FLOOR_VISIBLE				= 1 << 4,
		MIRROR_CAN_SEE_DIRECTIONAL_LIGHT	= 1 << 5,
		MIRROR_CAN_SEE_EXTERIOR_VIEW		= 1 << 6,
		WATER_SURFACE_VISIBLE				= 1 << 7,
		WATER_SURFACE_EXTEND_TO_HORIZON		= 1 << 8,
#if __BANK
		WATER_SURFACE_HEIGHT_MISMATCH		= 1 << 9,
#endif // __BANK
	};

	Vec4V				m_exteriorShadowFrustumPlanes[ CASCADE_SHADOWS_SCAN_NUM_PLANES ];
	fwScreenQuad		m_exteriorGBufScreenQuad;
	fwScreenQuad		m_visibleRoomsShadowScreenQuad;
	fwPortalCorners		m_mirrorCorners;
	fwFlags32			m_flags;
	fwInteriorLocation	m_mirrorInteriorLocation;
	float				m_mirrorDistance;
	u8					m_mirrorPriority;
	int					m_gbuffExteriorNodeScreenQuadIndex;
#if __BANK
	int					m_mirrorFloorQuadCount;
#endif // __BANK

public:
	u32					m_startingSceneNodeInInterior;

	enum { MAX_SKY_PORTALS_PARABOLOID_REFLECTION = 64 }; // V_farmhouse currently has 52 sky portals

	fwScanResultsSkyPortals<MAX_SKY_PORTALS_PARABOLOID_REFLECTION>	m_skyPortals_ParaboloidReflection;
	fwScanResultsSkyPortalExists									m_skyPortalBounds_MirrorReflection;

	void ClearFlags()														{ m_flags.ClearAllFlags(); }

	void SetExteriorIsVisibleInGbuf()										{ m_flags.SetFlag( EXTERIOR_VISIBLE_IN_GBUF ); }
	bool GetIsExteriorVisibleInGbuf() const									{ return m_flags.IsFlagSet( EXTERIOR_VISIBLE_IN_GBUF ); }

	void SetGbufUsesScreenQuadPairs(const bool value)						{ m_flags.ChangeFlag( GBUF_USES_SCREEN_QUAD_PAIRS, value ); }
	bool GetGbufUsesScreenQuadPairs() const									{ return m_flags.IsFlagSet( GBUF_USES_SCREEN_QUAD_PAIRS ); }

	void SetDirectionaLightEnabledForRoom(const bool value)					{ m_flags.ChangeFlag( DIRECTIONAL_LIGHT_ENABLED_FOR_ROOM, value ); }
	bool GetDirectionaLightEnabledForRoom() const							{ return m_flags.IsFlagSet( DIRECTIONAL_LIGHT_ENABLED_FOR_ROOM ); }

	void SetExteriorGbufScreenQuad(const fwScreenQuad& value)				{ m_exteriorGBufScreenQuad = value; }
	const fwScreenQuad& GetExteriorGbufScreenQuad() const					{ return m_exteriorGBufScreenQuad; }

	void SetVisibleRoomsShadowScreenQuad(const fwScreenQuad& value)			{ m_visibleRoomsShadowScreenQuad = value; }
	const fwScreenQuad& GetVisibleRoomsShadowScreenQuad() const				{ return m_visibleRoomsShadowScreenQuad; }
	fwScreenQuad& GetVisibleRoomsShadowScreenQuadRef()						{ return m_visibleRoomsShadowScreenQuad; }

	bool IsEverythingInShadow() const										{ return !GetIsExteriorVisibleInGbuf() && m_visibleRoomsShadowScreenQuad.IsValid() == false && !GetDirectionaLightEnabledForRoom(); }

	Vec4V* GetExteriorShadowFrustumPlanesPtr()								{ return m_exteriorShadowFrustumPlanes; }
	const Vec4V* GetExteriorShadowFrustumPlanesPtr() const					{ return m_exteriorShadowFrustumPlanes; }

	void SetMirrorVisible()													{ m_flags.SetFlag( MIRROR_VISIBLE ); }
	bool GetMirrorVisible() const											{ return m_flags.IsFlagSet( MIRROR_VISIBLE ); }

	void SetMirrorFloorVisible()											{ m_flags.SetFlag( MIRROR_FLOOR_VISIBLE ); BANK_ONLY(m_mirrorFloorQuadCount++;) }
	bool GetMirrorFloorVisible() const										{ return m_flags.IsFlagSet( MIRROR_FLOOR_VISIBLE ); }
	void SetMirrorFloorNotVisible()											{ m_flags.ClearFlag( MIRROR_FLOOR_VISIBLE ); BANK_ONLY(m_mirrorFloorQuadCount = 0;) }

	void SetMirrorCanSeeDirectionalLight()									{ m_flags.SetFlag( MIRROR_CAN_SEE_DIRECTIONAL_LIGHT ); }
	bool GetMirrorCanSeeDirectionalLight() const							{ return m_flags.IsFlagSet( MIRROR_CAN_SEE_DIRECTIONAL_LIGHT ); }

	void SetMirrorCanSeeExteriorView()										{ m_flags.SetFlag( MIRROR_CAN_SEE_EXTERIOR_VIEW ); }
	bool GetMirrorCanSeeExteriorView() const								{ return m_flags.IsFlagSet( MIRROR_CAN_SEE_EXTERIOR_VIEW ); }

	void SetWaterSurfaceVisible()											{ m_flags.SetFlag( WATER_SURFACE_VISIBLE ); }
	bool GetWaterSurfaceVisible() const										{ return m_flags.IsFlagSet( WATER_SURFACE_VISIBLE ); }

#if __BANK
	void SetWaterSurfaceHeightMismatch()									{ m_flags.SetFlag( WATER_SURFACE_HEIGHT_MISMATCH ); }
	bool GetWaterSurfaceHeightMismatch() const								{ return m_flags.IsFlagSet( WATER_SURFACE_HEIGHT_MISMATCH ); }
#endif // __BANK

	void SetMirrorDistance(const float mirrorDistance)						{ m_mirrorDistance = mirrorDistance; }
	float GetMirrorDistance() const											{ return m_mirrorDistance; }

	void SetMirrorPriority(const u8 mirrorPriority)							{ m_mirrorPriority = mirrorPriority; }
	u8 GetMirrorPriority() const											{ return m_mirrorPriority; }

#if __BANK
	void SetMirrorFloorQuadCount(const int count)							{ m_mirrorFloorQuadCount = count; }
	int GetMirrorFloorQuadCount() const										{ return m_mirrorFloorQuadCount; }
#endif // __BANK

	fwPortalCorners& GetMirrorCornersRef()									{ return m_mirrorCorners; }
	const fwPortalCorners& GetMirrorCorners() const							{ return m_mirrorCorners; }

	void SetMirrorInteriorLocation(const fwInteriorLocation value)			{ m_mirrorInteriorLocation = value; }
	fwInteriorLocation GetMirrorInteriorLocation() const					{ return m_mirrorInteriorLocation; }

	void SetGbuffExteriorNodeScreenQuadIndex(int index)						{ m_gbuffExteriorNodeScreenQuadIndex = index; }
	int GetGbuffExteriorNodeScreenQuadIndex() const							{ return m_gbuffExteriorNodeScreenQuadIndex; }

#if __SPU
	void UnfixupPointers(const fwPointerFixer& fixer)
	{
		m_skyPortals_ParaboloidReflection.UnfixupPointers(fixer);
	}
#endif // __SPU
};

CompileTimeAssertSize( fwPvsEntry, 16, 24 );
CompileTimeAssert( ( sizeof(fwScanResults) & 0xf ) == 0 );

} // namespace rage

#endif // !defined _INC_SCANRESULTENTRY_H_
