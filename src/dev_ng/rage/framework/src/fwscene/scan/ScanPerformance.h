#ifndef __INC_SCANPERFORMANCE_H__
#define __INC_SCANPERFORMANCE_H__

#include "system/timer.h"
#include "system/interlocked.h"
#include "system/ppu_symbol.h"

#define ENABLE_SCAN_TIMERS		0

namespace rage {

#if ENABLE_SCAN_TIMERS

class fwScanTimer
{
	friend class fwScanTimerFunc;
	friend class fwScanTimerSkipFunc;

private:

	static const float	ms_avgFactor;
	
	const char*			m_name;
	const char*			m_desc;
	bool				m_enabled;
	u32					m_time;
	u32					m_frameTime;
	u32					m_totalTime;
	float				m_avgTime;

public:

	fwScanTimer(const char* name, const char* desc)
	{
		m_name = name;
		m_desc = desc;
		m_enabled = true;
		m_time = 0;
		m_frameTime = 0;
		m_totalTime = 0;
		m_avgTime = 0;
	}

	const char* GetName() const		{ return m_name; }
	const char*	GetDesc() const		{ return m_desc; }
	bool IsEnabled() const			{ return m_enabled; }
	u32 GetFrameTime() const		{ return m_frameTime; }
	u32 GetTotalTime() const		{ return m_totalTime; }
	float GetAvgTime() const		{ return m_avgTime; }

	void BeginFrame()
	{
		m_time = 0;
		m_frameTime = 0;
	}

	void EndFrame()
	{
		if ( m_enabled )
			m_frameTime += m_time;

		m_totalTime += m_frameTime;
		m_avgTime = (m_avgTime * (1 - ms_avgFactor) ) + (m_frameTime * ms_avgFactor);
	}

	void AddTime(const u32 time) {
		m_time += time;
	}

	void SubtractTime(const u32 time) {
		m_time -= time;
	}

	void AtomicAddTime(const u32 time) {
		sysInterlockedAdd( &m_time, time );
	}

	void AtomicSubtractTime(const u32 time) {
		sysInterlockedAdd( &m_time, -time );
	}
	
	void Enable()
	{
		if ( m_enabled )
			return;

		m_time = 0;
		m_enabled = true;
	}

	void Disable()
	{
		if ( !m_enabled )
			return;

		m_frameTime += m_time;
		m_time = 0;
		m_enabled = false;
	}

#if !__SPU
	void Print() {
		printf( "%s: %.3f (%.3f)", m_name, static_cast< float >(m_frameTime) / 1000, static_cast< float >(m_avgTime) / 1000 );
	}
#endif
};

class fwScanLocalTimer
{
	friend class fwScanLocalTimerFunc;
	friend class fwScanLocalTimerSkipFunc;

private:

	sysTimer	m_timer;
	u32			m_time;

public:

	fwScanLocalTimer() {
		m_time = 0;
	}

	void UpdateScanTimer(fwScanTimer* scanTimer) {
		scanTimer->AtomicAddTime( m_time );
	}
};

class fwScanLocalTimerFunc
{
private:

	fwScanLocalTimer*	m_scanLocalTimer;
	
public:

	fwScanLocalTimerFunc(fwScanLocalTimer* scanLocalTimer)
	{
		m_scanLocalTimer = scanLocalTimer;
		m_scanLocalTimer->m_timer.Reset();
	}

	~fwScanLocalTimerFunc() {
		m_scanLocalTimer->m_time += static_cast< u32 >( m_scanLocalTimer->m_timer.GetUsTime() );
	}
};

class fwScanTimerFunc
{
private:
	
	fwScanTimer*	m_scanTimer;
	sysTimer		m_timer;

public:
	
	fwScanTimerFunc(fwScanTimer* scanTimer) {
		m_scanTimer = scanTimer;
	}

	~fwScanTimerFunc()
	{
		const u32		time = static_cast< u32 >( m_timer.GetUsTimeAndReset() );
		m_scanTimer->AtomicAddTime( time );
	}
};

struct fwScanTimerSkipFunc
{
private:

	fwScanTimer*	m_scanTimer;
	sysTimer		m_timer;

public:

	fwScanTimerSkipFunc(fwScanTimer* scanTimer) {
		m_scanTimer = scanTimer;
	}

	~fwScanTimerSkipFunc()
	{
		const u32		time = static_cast< u32 >( m_timer.GetUsTimeAndReset() );
		m_scanTimer->AtomicSubtractTime( time );
	}
};

#else // ENABLE_SCAN_TIMERS

class fwScanTimer
{
public:
	fwScanTimer(const char*, const char*)	{}

	const char* GetName() const				{ return NULL; }
	const char*	GetDesc() const				{ return NULL; }
	bool IsEnabled() const					{ return false; }
	u32 GetFrameTime() const				{ return 0; }
	u32 GetTotalTime() const				{ return 0; }
	float GetAvgTime() const				{ return 0; }

	void BeginFrame()						{}
	void EndFrame()							{}
	
	void AddTime(const u32)					{}
	void SubtractTime(const u32)			{}
	void AtomicAddTime(const u32)			{}
	void AtomicSubtractTime(const u32)		{}

	void Enable()							{}
	void Disable()							{}

#if !__SPU
	void Print()							{}
#endif
};

class fwScanLocalTimer
{
public:
	fwScanLocalTimer()						{}
	void UpdateScanTimer(fwScanTimer*)		{}
};

class fwScanLocalTimerFunc
{
public:
	fwScanLocalTimerFunc(fwScanLocalTimer*)	{}
};

class fwScanTimerFunc
{
public:
	fwScanTimerFunc(fwScanTimer*)			{}
};

class fwScanTimerSkipFunc
{
public:
	fwScanTimerSkipFunc(fwScanTimer*)		{}
};

#endif // else ENABLE_SCAN_TIMERS

} // namespace rage

DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_scanNodesTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_scanEntitiesTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_scanEntitiesEarlyRejectTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_scanEntitiesClippingTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_scanEntitiesOcclusionTimer );

DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_searchNodesTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_searchEntitiesSoaIntersectTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_searchEntitiesSoaIterateTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_searchEntitiesNonSoaTimer );
DECLARE_PPU_SYMBOL( rage::fwScanTimer,	g_searchCallbacksTimer );

#endif // __INC_SCANPERFORMANCE_H__
