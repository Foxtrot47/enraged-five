#ifndef _INC_SCREENQUAD_H_
#define _INC_SCREENQUAD_H_

#include "fwmaths/PortalCorners.h"
#include "math/float16.h"

// I used V_FLT_MAX here, but apparently if you set a float to FLT_MAX on SPU, and then read it from PPU,
// it is read as NaN, thus making methods like IsValid() not behaving as expected. - AP
#define SCREEN_QUAD_LARGE	256.0f
#define USE_FIXED_POINT_SCREEN_QUAD_PAIR (__PS3)
namespace rage {

class Color32;
class spdTransposedPlaneSet8;

/////////////
// Classes //
/////////////

enum eScreenQuadInvalidInitializer		{ SCREEN_QUAD_INVALID };
enum eScreenQuadFullScreenInitializer	{ SCREEN_QUAD_FULLSCREEN };

class fwScreenQuad
{
private:

	Vec4V	m_quad;

	Vec2V_Out LeftBottom() const		{ return m_quad.GetXY(); }
	Vec2V_Out RightTop() const			{ return m_quad.GetZW(); }

public:

	fwScreenQuad()
	{ }

	explicit fwScreenQuad(eScreenQuadInvalidInitializer)
		: m_quad( SCREEN_QUAD_LARGE, SCREEN_QUAD_LARGE, -SCREEN_QUAD_LARGE, -SCREEN_QUAD_LARGE )
	{ }

	explicit fwScreenQuad(eScreenQuadFullScreenInitializer)
		: m_quad( Vec2V(V_NEGONE), Vec2V(V_ONE) )
	{ }
	
	explicit fwScreenQuad(Vec4V_In quad)
		: m_quad( quad )
	{ }
	
	explicit fwScreenQuad(ScalarV_In left, ScalarV_In bottom, ScalarV_In right, ScalarV_In top)
		: m_quad( left, bottom, right, top )
	{ }

	explicit fwScreenQuad(const fwPortalCorners& portalCorners, Mat44V_In compositeMatrix);

	void Clamp();
	bool IsValid() const;
	bool Contains(const fwScreenQuad& op) const;
	ScalarV_Out ComputeArea() const;
	void Grow(Vec2V_In point);
	void Grow(const fwScreenQuad& op);
	fwScreenQuad Intersection(const fwScreenQuad& op) const;

	void GenerateFrustum(Mat44V_In compositeMatrix, spdTransposedPlaneSet8& frustum, bool bFlipPlanes = false) const;

	void DebugDraw(const Color32& color) const;

	Vec4V_Out ToVec4V() const			{ return m_quad; }
	ScalarV_Out Left() const			{ return m_quad.GetX(); }
	ScalarV_Out Bottom() const			{ return m_quad.GetY(); }
	ScalarV_Out Right() const			{ return m_quad.GetZ(); }
	ScalarV_Out Top() const				{ return m_quad.GetW(); }
};

class fwScreenQuadPair
{
private:

	Vec4V	m_quads;

public:

	fwScreenQuadPair()
	{ }

	explicit fwScreenQuadPair(eScreenQuadInvalidInitializer)
	{
		SetScreenQuad0( fwScreenQuad(SCREEN_QUAD_INVALID) );
		SetScreenQuad1( fwScreenQuad(SCREEN_QUAD_INVALID) );
	}

	explicit fwScreenQuadPair(Vec4V_In quads)
		: m_quads( quads )
	{ }

	void SetScreenQuad0(const fwScreenQuad& quad);
	void SetScreenQuad1(const fwScreenQuad& quad);

	fwScreenQuad GetScreenQuad0() const;
	fwScreenQuad GetScreenQuad1() const;

	void Grow(const fwScreenQuad& op);
	fwScreenQuad Union() const;

	void DebugDraw(const Color32& color0, const Color32& color1) const;
};

//////////////////////
// Inline functions //
//////////////////////

inline void fwScreenQuad::Clamp()
{
	m_quad = Min( Max( m_quad, Vec4V(V_NEGONE) ), Vec4V(V_ONE) );
}

inline bool fwScreenQuad::IsValid() const
{
	return IsLessThanAll( MergeXY( m_quad, m_quad ), MergeZW( m_quad, m_quad ) ) != 0;
}

inline bool fwScreenQuad::Contains(const fwScreenQuad& op) const
{
	const VecBoolV xy = IsGreaterThanOrEqual( MergeXY( op.m_quad, op.m_quad ), MergeXY( m_quad, m_quad ) );
	const VecBoolV zw = IsLessThanOrEqual   ( MergeZW( op.m_quad, op.m_quad ), MergeZW( m_quad, m_quad ) );

	return IsTrueAll( And( xy, zw ) );
}

inline ScalarV_Out fwScreenQuad::ComputeArea() const
{
	const Vec2V		delta = RightTop() - LeftBottom();
	return delta.GetX() * delta.GetY();
}

inline void fwScreenQuad::Grow(Vec2V_In point)
{
	const Vec4V		minV( point, Vec2V(V_INF) );
	const Vec4V		maxV( Vec2V(V_NEGINF), point );

	m_quad = Max( Min( m_quad, minV ), maxV );
}

inline void fwScreenQuad::Grow(const fwScreenQuad& op)
{
	const Vec4V		minV( op.LeftBottom(), Vec2V(V_INF) );
	const Vec4V		maxV( Vec2V(V_NEGINF), op.RightTop() );

	m_quad = Max( Min( m_quad, minV ), maxV );
}

inline fwScreenQuad fwScreenQuad::Intersection(const fwScreenQuad& op) const
{
	const Vec4V		minV( Vec2V(V_INF), op.RightTop() );
	const Vec4V		maxV( op.LeftBottom(), Vec2V(V_NEGINF) );

	return fwScreenQuad( Max( Min( m_quad, minV ), maxV ) );
}


inline void fwScreenQuadPair::SetScreenQuad0(const fwScreenQuad& quad)	
{ 
#if USE_FIXED_POINT_SCREEN_QUAD_PAIR
	m_quads = FixedPoint3_13Vec4PackIntoXY( m_quads, quad.ToVec4V() ); 
#else
	m_quads = Float16Vec4PackIntoXY( m_quads, quad.ToVec4V() ); 
#endif
}

inline void fwScreenQuadPair::SetScreenQuad1(const fwScreenQuad& quad)	
{ 
#if USE_FIXED_POINT_SCREEN_QUAD_PAIR
	m_quads = FixedPoint3_13Vec4PackIntoZW( m_quads, quad.ToVec4V() ); 
#else
	m_quads = Float16Vec4PackIntoZW( m_quads, quad.ToVec4V() ); 
#endif
}

inline fwScreenQuad fwScreenQuadPair::GetScreenQuad0() const
{ 
#if USE_FIXED_POINT_SCREEN_QUAD_PAIR
	return fwScreenQuad( FixedPoint3_13Vec4UnpackFromXY( m_quads ) ); 
#else
	return fwScreenQuad( Float16Vec4UnpackFromXY( m_quads ) ); 
#endif
}

inline fwScreenQuad fwScreenQuadPair::GetScreenQuad1() const				
{ 
#if USE_FIXED_POINT_SCREEN_QUAD_PAIR
	return fwScreenQuad( FixedPoint3_13Vec4UnpackFromZW( m_quads ) ); 
#else
	return fwScreenQuad( Float16Vec4UnpackFromZW( m_quads ) ); 
#endif
}

} // namespace rage

#endif // !defined _INC_SCREENQUAD_H_
