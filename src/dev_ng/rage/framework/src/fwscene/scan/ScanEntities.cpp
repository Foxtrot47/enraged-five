#if !__TOOL

#include "grcore/debugdraw.h"
#include "math/vecshift.h" // for BIT64

#include "fwmaths/vectorutil.h"
#include "fwscene/lod/LodTypes.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwscene/scan/ScanNodes.h"
#include "fwscene/scan/ScanDebug.h"
#include "fwscene/scan/ScanPerformance.h"
#include "fwscene/scan/ScanCascadeShadows.h"
#include "fwrenderer/renderlistbuilder.h"
#include "fwrenderer/RenderPhaseBase.h"
#include "softrasterizer/softrasterizer.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/timer.h"
#include "system/dependency.h"
#include "system/ppu_symbol.h"
#include "system/membarrier.h"

#include "vectormath/classes_soa.h"
#include "vectormath/legacyconvert.h"
#include "vectormath/vecbool1v_soa.h"
#include "vectormath/vecbool2v_soa.h"
#include "vectormath/vecbool3v_soa.h"
#include "vectormath/scalarv_soa.h"
#include "vectormath/vec2v_soa.h"
#include "vectormath/vec3v_soa.h"
#include "vectormath/vec4v_soa.h"
#include "vectormath/mat44v_soa.h"
#include "vectormath/mat34v_soa.h"
#include "vectormath/classfreefuncsv_soa.h"
#include "vectormath/vectortypes.h"

#define ALTIVEC_SPU (__XENON || __PPU || __SPU)

#define VALIDATE_ALTIVEC_CODE (1 && __ASSERT)

#define ENABLE_OLD_EXACT_OCCLUSION (__BANK && !__SPU)
#define ENABLE_NON_EXACT_OCCLUSION (__BANK)

#if ALTIVEC_SPU
	#include "math\altivec.h"
#if __SPU
	#include "vmx2spu_gcc.h"
#endif
#endif

#if __SPU
#define RST_QUERY_ONLY	1
#define FRAG_TSKLIB_ONLY
#include "softrasterizer/scan.cpp"
#endif

extern rage::Vec4V		g_screenQuadStorage[ rage::SCAN_SCREENQUAD_STORAGE_COUNT ][ rage::fwScanNodes::STORED_SCREEN_QUAD_COUNT ];
BANK_ONLY(bool g_bAreaBasedTesting = (__D3D11 || RSG_ORBIS) ? true : false;)

DECLARE_PPU_SYMBOL( rage::u32, g_scanTasksPending );

#if __WIN32PC
extern sysIpcEvent g_scanTasksReady;
#endif

#define SCAN_ENTITIES_DETAILED_STATS 0
namespace ScanEntitiesStats
{
	PF_PAGE(ScanEntitiesPage,"Scan Entities");

	PF_GROUP(ScanEntities);
	PF_LINK(ScanEntitiesPage, ScanEntities);
	PF_TIMER(fwScanEntitiesRun, ScanEntities);
#if SCAN_ENTITIES_DETAILED_STATS
	PF_TIMER(ProcessEntityDescs, ScanEntities);
	PF_TIMER(IsEntityEarlyRejected, ScanEntities);
	PF_TIMER(IsEntityVisible, ScanEntities);
	PF_TIMER(IsEntityOccluded, ScanEntities);
	PF_TIMER(ProcessEntityDescsSoa, ScanEntities);
	PF_TIMER(AreEntitiesEarlyRejected, ScanEntities);
#endif
}
using namespace ScanEntitiesStats;

namespace rage {

bool fwScanEntities::RunFromDependency(const sysDependency& dependency)
{
	PF_FUNC(fwScanEntitiesRun);

	FetchScanDebugFlags();

	void*					scratchMemory = dependency.m_Params[0].m_AsPtr;
	const fwScanBaseInfo*	scanBaseInfo = static_cast< const fwScanBaseInfo* >( dependency.m_Params[1].m_AsPtr );
	const fwScanResults*	scanResults = static_cast< const fwScanResults* >( dependency.m_Params[2].m_AsPtr );
	const Vec4V*			occlusionBufferGBuf = static_cast< const Vec4V* >( dependency.m_Params[3].m_AsPtr );
	const Vec4V*			occlusionBufferCascadeShadows = static_cast< const Vec4V* >( dependency.m_Params[4].m_AsPtr );
	fwEntityDesc*			entityDescs = static_cast< fwEntityDesc* >( dependency.m_Params[5].m_AsPtr );
	const u32				entityDescCount = dependency.m_DataSizes[5] / sizeof(fwEntityDesc);
	spdTransposedPlaneSet4*	reflectionPlaneSets = (spdTransposedPlaneSet4*)dependency.m_Params[6].m_AsPtr;
	const int				packedFlags = dependency.m_Params[7].m_AsInt;
	const int				storedScreenQuadIndex = dependency.m_Params[8].m_AsInt;

	fwScanEntities			scanEntities;
	Vec4V					screenQuadStorage[ SCAN_SCREENQUAD_STORAGE_COUNT ];

#if SCL_LOGGING
	void **ppuEntDescAddr = (void**)(u32(&dependency + 3) + 8); // This is offset to m_DataBuffers in the "Instruction" struct

	u32 containerType = ((packedFlags >> 24) & SCAN_ENTITIES_FLAG_ENTITY_DESCS_SOA) ? SCL_CONTAINER_SOA : SCL_CONTAINER_ENTITY;
	SCLPushScanEntitiesBegin(containerType, __SPU ? (u32)ppuEntDescAddr[5] : (u32)entityDescs, entityDescCount);
#endif
#if __SPU
	for (int i = 0; i < SCAN_SCREENQUAD_STORAGE_COUNT; ++i)
	{
		sysDmaGet( &screenQuadStorage[i],
			CELL_SPURS_PPU_SYM(g_screenQuadStorage) + sizeof(Vec4V) * (fwScanNodes::STORED_SCREEN_QUAD_COUNT * i + storedScreenQuadIndex),
			sizeof(Vec4V),
			g_auxDmaTag );
	}
	sysDmaWait( 1 << g_auxDmaTag );
#else
	for (int i = 0; i < SCAN_SCREENQUAD_STORAGE_COUNT; ++i)
		screenQuadStorage[i] = g_screenQuadStorage[i][storedScreenQuadIndex];
#endif

	scanEntities.SetScanBaseInfo( scanBaseInfo );
	scanEntities.SetEntityDescs( entityDescs );
	scanEntities.SetEntityDescCount( entityDescCount );
	scanEntities.SetControlFlags( ( packedFlags >> 24 ) & 0xff );
	scanEntities.SetNodeVisTags( packedFlags & 0x00ffffff );
	scanEntities.SetScanResults( scanResults );
	scanEntities.SetScratchMemory( scratchMemory );
	scanEntities.SetHiZBuffer( SCAN_OCCLUSION_STORAGE_PRIMARY, occlusionBufferGBuf );
	scanEntities.SetHiZBuffer( SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS, occlusionBufferCascadeShadows );
	scanEntities.SetReflectionPlaneSets(reflectionPlaneSets);

	for (int i = 0; i < SCAN_SCREENQUAD_STORAGE_COUNT; ++i)
		scanEntities.SetScreenQuadStorage( i, screenQuadStorage[ i ] );

	SPU_ONLY( SCAN_STATS_ONLY( sysTimer	processTimer; ) )
	scanEntities.Run();
	SPU_ONLY( SCAN_STATS_ONLY( sysInterlockedAdd( (volatile u32*) CELL_SPURS_PPU_SYM( g_spuExteriorEntitiesProcessUs ), (int) processTimer.GetUsTime() ); ) )

#if SCL_LOGGING && 0
	if (entityDescCount == 8)
	{
		return true;
	}
#endif

	sysInterlockedDecrement( PPU_SYMBOL(g_scanTasksPending) );
#if __WIN32PC
	sysIpcSetEvent(g_scanTasksReady);
#endif

#if SCL_LOGGING
	SCLPushScanEntitiesEnd(containerType, __SPU ? (u32)ppuEntDescAddr[5] : (u32)entityDescs, entityDescCount);
#endif
	return true;
}

void fwScanEntities::ResetScanEntries()
{
	m_pvsEntryBuffer = m_scratchAllocator.Alloc< fwPvsEntry >( SCAN_MAX_ENTITIES_PER_JOB, 16 );
	m_pvsEntryBufferSize = SCAN_MAX_ENTITIES_PER_JOB;
	m_pvsEntryCount = 0;
}

SCAN_ENTITIES_FORCE_INLINE float fwScanEntities::CalcAlphaFade(const fwEntityDesc& entityDesc, const float fDistance)
{
	const bool bSmallObject = entityDesc.GetLodType()==LODTYPES_DEPTH_ORPHANHD && entityDesc.GetLodDistCached()<=LODTYPES_FADE_DISTF;
	const float fEntityTypeScale = ( (entityDesc.GetEntityType()==_GTA5_ENTITY_TYPE_GRASS_BATCH) ? m_scanBaseInfo->m_grassBatchLodScale : 1.0f );
	const float fScaledLodDist = fEntityTypeScale * ( (float)entityDesc.GetLodDistCached() ) * m_scanBaseInfo->m_perLodLevelScales[entityDesc.GetLodType()];

	const float fDistStartFadeUp = fScaledLodDist + ( bSmallObject ? LODTYPES_FADE_SMALL_DISTF : LODTYPES_FADE_DISTF );
	const float fFadeUpRange = ( bSmallObject ? LODTYPES_FADE_SMALL_DISTF: m_scanBaseInfo->m_fadeDistances[entityDesc.GetLodType()] );
	const float fDistStopFadeUp = fDistStartFadeUp - fFadeUpRange;

	return ((fDistStartFadeUp - Clamp(fDistance, fDistStopFadeUp, fDistStartFadeUp)) / fFadeUpRange) + 0.0001f;
}


SCAN_ENTITIES_FORCE_INLINE void fwScanEntities::AddPvsEntry(const fwEntityDesc& entityDesc, const float fDistance, fwVisibilityFlags& visFlags)
{
	visFlags.SetLodSortVal( m_scanBaseInfo->m_lodSortVals[entityDesc.GetLodType()] );
	visFlags.SetEntityType( entityDesc.GetEntityType() );

	visFlags.SetAlpha( (u32) (CalcAlphaFade(entityDesc, fDistance) * LODTYPES_ALPHA_VISIBLEF) );

	Assert( m_pvsEntryCount < m_pvsEntryBufferSize );
	fwPvsEntry& pvsEntry = m_pvsEntryBuffer[ m_pvsEntryCount++ ];

	pvsEntry.SetEntity( entityDesc.GetEntity() );
	pvsEntry.SetDist( fDistance );
	pvsEntry.SetVisFlags( visFlags );

	// 4/25/13 - cthomas Some fwScanEntities can take quite a long time to process. These also tend 
	// to be the first fwScanEntities jobs in the system, so that delays getting pvs entries to the 
	// main thread for processing, caused the main thread to idle. We'll now submit the entries in 
	// batches once the # of pvs entries exceeds a hard-coded threshold, which will get the main 
	// thread working sooner on processing these results.
	// 
	// Warning for possible future issue - before this change, we had at most 1 result block per 
	// fwScanEntities job. Because of this, MAX_RESULT_BLOCKS is set to MAX_SCAN_ENTITIES_DEPENDENCY_COUNT, 
	// which was sufficient for our purposes previously. However, in theory, we could now have more 
	// result blocks than fwScanEntities jobs. In practice, we don't seem to come anywhere close to 
	// this limit, so I've left them as-is.
	if(m_pvsEntryCount >= SCAN_NUM_PVS_ENTRIES_PER_RESULT_BLOCK)
	{
		SubmitScanEntries();
		m_pvsEntryCount = 0;
	}
}

void fwScanEntities::SubmitScanEntries()
{
	static sysCriticalSectionToken s_resultBlockCSToken;

	if ( m_pvsEntryCount == 0 )
		return;

	/* The size of the DMA transfer must be multiple of 16 bytes, so remember to add some dummy entries if
	needed (for example, if the size of fwPvsEntry is 8 and the number of entries is odd).
	"Dummy entry" means an entry with a NULL entity pointer - such entries will be ignored by post-scan code. */

	const uintptr_t	pvsDestPtr = sysInterlockedAdd( reinterpret_cast< uintptr_t volatile* >( m_scanBaseInfo->m_pvsCurrentPtr ), ptrdiff_t(sizeof(fwPvsEntry) * m_pvsEntryCount) );
#if __ASSERT
	FatalAssertf((fwPvsEntry*)pvsDestPtr >= m_scanBaseInfo->m_pvsStartingPtr && ((fwPvsEntry*)pvsDestPtr + m_pvsEntryCount) < (m_scanBaseInfo->m_pvsStartingPtr+m_scanBaseInfo->m_pvsMaxSize),"Too many PVS entries! - increase MAX_PVS_SIZE or reduce scans.\n");
#else
	FastAssert((fwPvsEntry*)pvsDestPtr >= m_scanBaseInfo->m_pvsStartingPtr && ((fwPvsEntry*)pvsDestPtr + m_pvsEntryCount) < (m_scanBaseInfo->m_pvsStartingPtr+m_scanBaseInfo->m_pvsMaxSize));
#endif

#if __SPU
	sysDmaPutAndWait( m_pvsEntryBuffer, pvsDestPtr, sizeof(fwPvsEntry) * m_pvsEntryCount, g_auxDmaTag );
#else // __SPU
	memcpy( reinterpret_cast< fwPvsEntry* >( pvsDestPtr ), m_pvsEntryBuffer, sizeof(fwPvsEntry) * m_pvsEntryCount );
#endif // else __SPU

	sysCriticalSection resultBlockCS(s_resultBlockCSToken);

	// After the transfer has been completed, fill out a "result block" structure to tell the
	// consumer that another set of results has been finished.
	unsigned int resultBlockIndex = sysInterlockedAdd(m_scanBaseInfo->m_NextResultBlock, 1);

#if __ASSERT
	FatalAssertf(resultBlockIndex < m_scanBaseInfo->m_MaxResultBlocks, "Too many result blocks! Increment MAX_RESULT_BLOCKS.");
#else
	FastAssert(resultBlockIndex < m_scanBaseInfo->m_MaxResultBlocks);
#endif

#if __SPU
	fwScanResultBlock *resultBlock = (fwScanResultBlock *) m_pvsEntryBuffer;
#else // __SPU
	fwScanResultBlock *resultBlock = &m_scanBaseInfo->m_ResultBlocks[resultBlockIndex];
#endif // __SPU

	resultBlock->m_PvsEntry = (fwPvsEntry *) pvsDestPtr;
	resultBlock->m_EntryCount = m_pvsEntryCount;

#if __SPU
	// Let's send it as a u64 so we don't have to worry about matching the lower nibble of the address.
	CompileTimeAssert(sizeof(fwScanResultBlock) == sizeof(u64));

	// Since we know resultBlock is aligned by 16, we can easily cast it to a u64 *.
	sysDmaPutUInt64(*((u64 *) resultBlock), (u32) &m_scanBaseInfo->m_ResultBlocks[resultBlockIndex], g_auxDmaTag);
#endif // __SPU

	sysMemWriteBarrier();

	// Now that we filled out the result block structure, tell the consumer that it's ready.

	// The use of the resultBlockCS critical section guarantees we will get the next open slot 
	// insert our result bock data before anyone else can possibly do it, so we only need to 
	// simply increase result block count here (previously this was a busy-loop waiting for the 
	// "next worker" to put their results in before incrementing the results count, but the 
	// busy-loop behavior was causing stalls on PC).
	sysInterlockedAdd(m_scanBaseInfo->m_ResultBlockCount, 1);
}

namespace SoAUtils {

static SCAN_ENTITIES_FORCE_INLINE FASTRETURNCHECK(ScalarV_Out)		Sqr(ScalarV_In x)		{ return x * x; }
static SCAN_ENTITIES_FORCE_INLINE FASTRETURNCHECK(Vec4V_Out)		Sqr(Vec4V_In x)			{ return x * x; }
static SCAN_ENTITIES_FORCE_INLINE FASTRETURNCHECK(SoA_ScalarV_Out)	Sqr(SoA_ScalarV_In x)	{ return x * x; }

SCAN_ENTITIES_FORCE_INLINE SoA_VecBool3V_Out IsGreaterThan(SoA_Vec3V_In bigVector, SoA_Vec3V_In smallVector)
{
	const SoA_VecBool1V		xResult = IsGreaterThan( bigVector.GetX(), smallVector.GetX() );
	const SoA_VecBool1V		yResult = IsGreaterThan( bigVector.GetY(), smallVector.GetY() );
	const SoA_VecBool1V		zResult = IsGreaterThan( bigVector.GetZ(), smallVector.GetZ() );
	const SoA_VecBool3V		result( xResult.GetIntrin128(), yResult.GetIntrin128(), zResult.GetIntrin128() );

	return result;
}

#if !__SPU

// for i in range(32): print( '\tVec4VConstant< 0x{0:08x}, 0x{0:08x}, 0x{0:08x}, 0x{0:08x} >(),'.format(1<<i) )
static const Vec4V		s_vectorMasks[32] =
{
	Vec4VConstant< 0x00000001, 0x00000001, 0x00000001, 0x00000001 >(),
	Vec4VConstant< 0x00000002, 0x00000002, 0x00000002, 0x00000002 >(),
	Vec4VConstant< 0x00000004, 0x00000004, 0x00000004, 0x00000004 >(),
	Vec4VConstant< 0x00000008, 0x00000008, 0x00000008, 0x00000008 >(),
	Vec4VConstant< 0x00000010, 0x00000010, 0x00000010, 0x00000010 >(),
	Vec4VConstant< 0x00000020, 0x00000020, 0x00000020, 0x00000020 >(),
	Vec4VConstant< 0x00000040, 0x00000040, 0x00000040, 0x00000040 >(),
	Vec4VConstant< 0x00000080, 0x00000080, 0x00000080, 0x00000080 >(),
	Vec4VConstant< 0x00000100, 0x00000100, 0x00000100, 0x00000100 >(),
	Vec4VConstant< 0x00000200, 0x00000200, 0x00000200, 0x00000200 >(),
	Vec4VConstant< 0x00000400, 0x00000400, 0x00000400, 0x00000400 >(),
	Vec4VConstant< 0x00000800, 0x00000800, 0x00000800, 0x00000800 >(),
	Vec4VConstant< 0x00001000, 0x00001000, 0x00001000, 0x00001000 >(),
	Vec4VConstant< 0x00002000, 0x00002000, 0x00002000, 0x00002000 >(),
	Vec4VConstant< 0x00004000, 0x00004000, 0x00004000, 0x00004000 >(),
	Vec4VConstant< 0x00008000, 0x00008000, 0x00008000, 0x00008000 >(),
	Vec4VConstant< 0x00010000, 0x00010000, 0x00010000, 0x00010000 >(),
	Vec4VConstant< 0x00020000, 0x00020000, 0x00020000, 0x00020000 >(),
	Vec4VConstant< 0x00040000, 0x00040000, 0x00040000, 0x00040000 >(),
	Vec4VConstant< 0x00080000, 0x00080000, 0x00080000, 0x00080000 >(),
	Vec4VConstant< 0x00100000, 0x00100000, 0x00100000, 0x00100000 >(),
	Vec4VConstant< 0x00200000, 0x00200000, 0x00200000, 0x00200000 >(),
	Vec4VConstant< 0x00400000, 0x00400000, 0x00400000, 0x00400000 >(),
	Vec4VConstant< 0x00800000, 0x00800000, 0x00800000, 0x00800000 >(),
	Vec4VConstant< 0x01000000, 0x01000000, 0x01000000, 0x01000000 >(),
	Vec4VConstant< 0x02000000, 0x02000000, 0x02000000, 0x02000000 >(),
	Vec4VConstant< 0x04000000, 0x04000000, 0x04000000, 0x04000000 >(),
	Vec4VConstant< 0x08000000, 0x08000000, 0x08000000, 0x08000000 >(),
	Vec4VConstant< 0x10000000, 0x10000000, 0x10000000, 0x10000000 >(),
	Vec4VConstant< 0x20000000, 0x20000000, 0x20000000, 0x20000000 >(),
	Vec4VConstant< 0x40000000, 0x40000000, 0x40000000, 0x40000000 >(),
	Vec4VConstant< 0x80000000, 0x80000000, 0x80000000, 0x80000000 >()
};

static SCAN_ENTITIES_FORCE_INLINE Vec4V_Out GetVectorMask(const int shift) {
	return s_vectorMasks[ shift ];
}

#else

static SCAN_ENTITIES_FORCE_INLINE Vec4V_Out GetVectorMask(const int shift)
{
	const int		mask = 1 << shift;
	const qword		qmask = si_from_int( mask );
	const qword		splat = (qword)(vec_uint4){ 0x00010203, 0x00010203, 0x00010203, 0x00010203 };
	const qword		vmask = si_shufb( qmask, qmask, splat );
	
	return Vec4V( (vec_float4) vmask );
}

#endif

SCAN_ENTITIES_FORCE_INLINE void Clear(fwTestResults_InOut results)
{
	results.m_slots[0].ZeroComponents();
	results.m_slots[1].ZeroComponents();
	results.m_slots[2].ZeroComponents();
	results.m_slots[3].ZeroComponents();
}

SCAN_ENTITIES_FORCE_INLINE fwTestResults_Out And(fwTestResults_In a, fwTestResults_In b)
{
	fwTestResults		ret;

	ret.m_slots[0] = And( a.m_slots[0], b.m_slots[0] );
	ret.m_slots[1] = And( a.m_slots[1], b.m_slots[1] );
	ret.m_slots[2] = And( a.m_slots[2], b.m_slots[2] );
	ret.m_slots[3] = And( a.m_slots[3], b.m_slots[3] );

	return ret;
}

SCAN_ENTITIES_FORCE_INLINE fwTestResults_Out Or(fwTestResults_In a, fwTestResults_In b)
{
	fwTestResults		ret;

	ret.m_slots[0] = Or( a.m_slots[0], b.m_slots[0] );
	ret.m_slots[1] = Or( a.m_slots[1], b.m_slots[1] );
	ret.m_slots[2] = Or( a.m_slots[2], b.m_slots[2] );
	ret.m_slots[3] = Or( a.m_slots[3], b.m_slots[3] );

	return ret;
}

SCAN_ENTITIES_FORCE_INLINE fwTestResults_Out Not(fwTestResults_In a)
{
	fwTestResults		ret;

	ret.m_slots[0] = InvertBits( a.m_slots[0] );
	ret.m_slots[1] = InvertBits( a.m_slots[1] );
	ret.m_slots[2] = InvertBits( a.m_slots[2] );
	ret.m_slots[3] = InvertBits( a.m_slots[3] );

	return ret;
}

SCAN_ENTITIES_FORCE_INLINE void SetResult(fwTestResults_InOut results, const int block, VecBoolV_In resultValue)
{
	const int		slot = block >> 5;
	const int		offset = (block & 0x1f);
	const Vec4V		mask = GetVectorMask( offset );
	const Vec4V		resultValueBits( resultValue );

	results.m_slots[slot] = (results.m_slots[slot] & InvertBits(mask)) | (resultValueBits & mask);
}

SCAN_ENTITIES_FORCE_INLINE VecBoolV_Out GetResult(fwTestResults_In results, const int block)
{
	const int		slot = block >> 5;
	const int		offset = (block & 0x1f);
	const Vec4V		mask = GetVectorMask( offset );

	return !IsEqualInt( (results.m_slots[slot] & mask), Vec4V(V_ZERO) );
}

SCAN_ENTITIES_FORCE_INLINE void GetBlockBoundingBox(const fwEntityDescSoA& soa, const int block, SoA_Vec3V_InOut boxMin, SoA_Vec3V_InOut boxMax)
{
	const SoA_Vec3V		position( soa.m_x[block].GetIntrin128(), soa.m_y[block].GetIntrin128(), soa.m_z[block].GetIntrin128() );

	Vec4V				extentX, extentY, extentZ, unused;
	Float16Unpack( soa.m_extentXY[block], extentX, extentY );
	Float16Unpack( soa.m_extentZAndRadius[block], extentZ, unused );

	const SoA_Vec3V		extent( extentX.GetIntrin128(), extentY.GetIntrin128(), extentZ.GetIntrin128() );
	
	boxMin = position - extent;
	boxMax = position + extent;
}

SCAN_ENTITIES_FORCE_INLINE VecBoolV_Out IsBoxIntersectingSphere(SoA_Vec3V_In boxMin, SoA_Vec3V_In boxMax, SoA_Vec4V_In sphere)
{
	const SoA_Vec3V			half( Vec4V(V_HALF).GetIntrin128() );
	SoA_Vec3V				boxPosition, boxExtent;
	Scale( boxPosition, (boxMax + boxMin), half );
	Scale( boxExtent, (boxMax - boxMin), half );

	const SoA_Vec3V			spherePosition = sphere.GetXYZ();

	SoA_Vec3V				delta;
	Abs( delta, spherePosition - boxPosition );
	delta = delta - boxExtent;
	Max( delta, delta, SoA_Vec3V(SoA_Vec3V::ZERO) );

	const SoA_ScalarV		distance2 = MagSquared( delta );
	const SoA_ScalarV		sphereRadius2 = Sqr( sphere.GetW() );
	const SoA_VecBool1V		intersect = (distance2 <= sphereRadius2 );

	VecBoolV				result;
	result.SetIntrin128( intersect.GetIntrin128() );

	return result;
}

SCAN_ENTITIES_FORCE_INLINE VecBoolV_Out IsPlaneCullingBox(SoA_Vec3V_In boxMin, SoA_Vec3V_In boxMax, SoA_Vec4V_In plane)
{
	const SoA_ScalarV		zero1( SoA_ScalarV::ZERO );
	const SoA_Vec3V			zero3( SoA_Vec3V::ZERO );
	const SoA_VecBool3V		nselect = SoAUtils::IsGreaterThan( plane.GetXYZ(), zero3 );

	SoA_Vec3V				nvertex;
	SelectFT( nvertex, nselect, boxMax, boxMin );

	SoA_ScalarV				product = plane.GetX() * nvertex.GetX() +
								plane.GetY() * nvertex.GetY() +
								plane.GetZ() * nvertex.GetZ() +
								plane.GetW();

	const SoA_VecBool1V		culled = IsGreaterThan( product, zero1 );

	VecBoolV				result;
	result.SetIntrin128( culled.GetIntrin128() );

	return result;
}

SCAN_ENTITIES_FORCE_INLINE fwTestResults_Out AreEntitiesIntersectingSphere(const fwEntityDescSoA& soa, const spdSphere& sphere, fwTestResults_In excluded)
{
	fwTestResults		results(V_FALSE);
	const SoA_Vec4V		soaSphere( sphere.GetV4().GetIntrin128() );

	for (int b = 0; b < soa.m_blockCount; ++b)
	{
		const VecBoolV		blockExcluded = GetResult( excluded, b );
		if ( IsTrueAll(blockExcluded) )
			continue;

		SoA_Vec3V		boxMin, boxMax;
		SoAUtils::GetBlockBoundingBox( soa, b, boxMin, boxMax );

		const VecBoolV	result = IsBoxIntersectingSphere( boxMin, boxMax, soaSphere );

		SetResult( results, b, And(result, !blockExcluded) );
	}

	return results;
}

SCAN_ENTITIES_FORCE_INLINE SoA_Vec4V_Out Splat(Vec4V_In vec)
{
	SoA_Vec4V	ret;
	ret.SetXIntrin128( vec.GetX().GetIntrin128() );
	ret.SetYIntrin128( vec.GetY().GetIntrin128() );
	ret.SetZIntrin128( vec.GetZ().GetIntrin128() );
	ret.SetWIntrin128( vec.GetW().GetIntrin128() );

	return ret;
}

fwTestResults_Out AreEntitiesIntersectingPlaneSet8(const fwEntityDescSoA& soa, const spdTransposedPlaneSet8& frustum, fwTestResults_In excluded)
{
	fwTestResults	results(V_FALSE);

	SoA_Vec4V		soaPlane0;
	SoA_Vec4V		soaPlane1;
	SoA_Vec4V		soaPlane2;
	SoA_Vec4V		soaPlane3;
	SoA_Vec4V		soaPlane4;
	SoA_Vec4V		soaPlane5;
	SoA_Vec4V		soaPlane6;
	SoA_Vec4V		soaPlane7;

	// it's more efficient to splat the planes directly into SoA format
	// without calling "frustum.GetPlanes" (which does a 4x4 transpose)

	soaPlane0.SetXIntrin128( frustum.GetPlanes0123X().GetX().GetIntrin128() );
	soaPlane0.SetYIntrin128( frustum.GetPlanes0123Y().GetX().GetIntrin128() );
	soaPlane0.SetZIntrin128( frustum.GetPlanes0123Z().GetX().GetIntrin128() );
	soaPlane0.SetWIntrin128( frustum.GetPlanes0123W().GetX().GetIntrin128() );

	soaPlane1.SetXIntrin128( frustum.GetPlanes0123X().GetY().GetIntrin128() );
	soaPlane1.SetYIntrin128( frustum.GetPlanes0123Y().GetY().GetIntrin128() );
	soaPlane1.SetZIntrin128( frustum.GetPlanes0123Z().GetY().GetIntrin128() );
	soaPlane1.SetWIntrin128( frustum.GetPlanes0123W().GetY().GetIntrin128() );

	soaPlane2.SetXIntrin128( frustum.GetPlanes0123X().GetZ().GetIntrin128() );
	soaPlane2.SetYIntrin128( frustum.GetPlanes0123Y().GetZ().GetIntrin128() );
	soaPlane2.SetZIntrin128( frustum.GetPlanes0123Z().GetZ().GetIntrin128() );
	soaPlane2.SetWIntrin128( frustum.GetPlanes0123W().GetZ().GetIntrin128() );

	soaPlane3.SetXIntrin128( frustum.GetPlanes0123X().GetW().GetIntrin128() );
	soaPlane3.SetYIntrin128( frustum.GetPlanes0123Y().GetW().GetIntrin128() );
	soaPlane3.SetZIntrin128( frustum.GetPlanes0123Z().GetW().GetIntrin128() );
	soaPlane3.SetWIntrin128( frustum.GetPlanes0123W().GetW().GetIntrin128() );

	soaPlane4.SetXIntrin128( frustum.GetPlanes4567X().GetX().GetIntrin128() );
	soaPlane4.SetYIntrin128( frustum.GetPlanes4567Y().GetX().GetIntrin128() );
	soaPlane4.SetZIntrin128( frustum.GetPlanes4567Z().GetX().GetIntrin128() );
	soaPlane4.SetWIntrin128( frustum.GetPlanes4567W().GetX().GetIntrin128() );

	soaPlane5.SetXIntrin128( frustum.GetPlanes4567X().GetY().GetIntrin128() );
	soaPlane5.SetYIntrin128( frustum.GetPlanes4567Y().GetY().GetIntrin128() );
	soaPlane5.SetZIntrin128( frustum.GetPlanes4567Z().GetY().GetIntrin128() );
	soaPlane5.SetWIntrin128( frustum.GetPlanes4567W().GetY().GetIntrin128() );

	soaPlane6.SetXIntrin128( frustum.GetPlanes4567X().GetZ().GetIntrin128() );
	soaPlane6.SetYIntrin128( frustum.GetPlanes4567Y().GetZ().GetIntrin128() );
	soaPlane6.SetZIntrin128( frustum.GetPlanes4567Z().GetZ().GetIntrin128() );
	soaPlane6.SetWIntrin128( frustum.GetPlanes4567W().GetZ().GetIntrin128() );

	soaPlane7.SetXIntrin128( frustum.GetPlanes4567X().GetW().GetIntrin128() );
	soaPlane7.SetYIntrin128( frustum.GetPlanes4567Y().GetW().GetIntrin128() );
	soaPlane7.SetZIntrin128( frustum.GetPlanes4567Z().GetW().GetIntrin128() );
	soaPlane7.SetWIntrin128( frustum.GetPlanes4567W().GetW().GetIntrin128() );

	for (int b = 0; b < soa.m_blockCount; ++b)
	{
		const VecBoolV		blockExcluded = GetResult( excluded, b );
		if ( IsTrueAll(blockExcluded) )
			continue;

		SoA_Vec3V			boxMin, boxMax;
		GetBlockBoundingBox( soa, b, boxMin, boxMax );

		VecBoolV			culled = blockExcluded;

		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane0 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane1 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane2 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane3 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane4 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane5 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane6 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane7 ) );

		SetResult( results, b, !culled );
	}

	return results;
}

// optimised version which uses only 5 out of 8 of the frustum planes
fwTestResults_Out AreEntitiesIntersectingPlaneSet5(const fwEntityDescSoA& soa, const spdTransposedPlaneSet8& frustum, fwTestResults_In excluded)
{
	fwTestResults	results(V_FALSE);

	SoA_Vec4V		soaPlane0;
	SoA_Vec4V		soaPlane1;
	SoA_Vec4V		soaPlane2;
	SoA_Vec4V		soaPlane3;
	SoA_Vec4V		soaPlane4;

#if __BANK
	if (g_scanDebugFlags & SCAN_DEBUG_FORCE_8_PLANE_SOA_FRUSTUM_CULLING)
	{
		return AreEntitiesIntersectingPlaneSet8(soa, frustum, excluded);
	}
#endif // __BANK

	// it's more efficient to splat the planes directly into SoA format
	// without calling "frustum.GetPlanes" (which does a 4x4 transpose)

	soaPlane0.SetXIntrin128( frustum.GetPlanes0123X().GetX().GetIntrin128() );
	soaPlane0.SetYIntrin128( frustum.GetPlanes0123Y().GetX().GetIntrin128() );
	soaPlane0.SetZIntrin128( frustum.GetPlanes0123Z().GetX().GetIntrin128() );
	soaPlane0.SetWIntrin128( frustum.GetPlanes0123W().GetX().GetIntrin128() );

	soaPlane1.SetXIntrin128( frustum.GetPlanes0123X().GetY().GetIntrin128() );
	soaPlane1.SetYIntrin128( frustum.GetPlanes0123Y().GetY().GetIntrin128() );
	soaPlane1.SetZIntrin128( frustum.GetPlanes0123Z().GetY().GetIntrin128() );
	soaPlane1.SetWIntrin128( frustum.GetPlanes0123W().GetY().GetIntrin128() );

	soaPlane2.SetXIntrin128( frustum.GetPlanes0123X().GetZ().GetIntrin128() );
	soaPlane2.SetYIntrin128( frustum.GetPlanes0123Y().GetZ().GetIntrin128() );
	soaPlane2.SetZIntrin128( frustum.GetPlanes0123Z().GetZ().GetIntrin128() );
	soaPlane2.SetWIntrin128( frustum.GetPlanes0123W().GetZ().GetIntrin128() );

	soaPlane3.SetXIntrin128( frustum.GetPlanes0123X().GetW().GetIntrin128() );
	soaPlane3.SetYIntrin128( frustum.GetPlanes0123Y().GetW().GetIntrin128() );
	soaPlane3.SetZIntrin128( frustum.GetPlanes0123Z().GetW().GetIntrin128() );
	soaPlane3.SetWIntrin128( frustum.GetPlanes0123W().GetW().GetIntrin128() );

	soaPlane4.SetXIntrin128( frustum.GetPlanes4567X().GetX().GetIntrin128() );
	soaPlane4.SetYIntrin128( frustum.GetPlanes4567Y().GetX().GetIntrin128() );
	soaPlane4.SetZIntrin128( frustum.GetPlanes4567Z().GetX().GetIntrin128() );
	soaPlane4.SetWIntrin128( frustum.GetPlanes4567W().GetX().GetIntrin128() );

	for (int b = 0; b < soa.m_blockCount; ++b)
	{
		const VecBoolV		blockExcluded = GetResult( excluded, b );
		if ( IsTrueAll(blockExcluded) )
			continue;

		SoA_Vec3V			boxMin, boxMax;
		GetBlockBoundingBox( soa, b, boxMin, boxMax );

		VecBoolV			culled = blockExcluded;

		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane0 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane1 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane2 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane3 ) );
		culled = Or( culled, IsPlaneCullingBox( boxMin, boxMax, soaPlane4 ) );

		SetResult( results, b, !culled );
	}

	return results;
}

} // namespace SoAUtils 

#if ALTIVEC_SPU
SCAN_ENTITIES_FORCE_INLINE vec_ushort8 LoadLodType(u16* address, vec_ushort8 mask, vec_ushort8 shift)
{
	vec_ushort8 usFlags	= vec_lvlx(0, address);
	
	usFlags = vec_and(usFlags, mask); 	// Remove upper bits of flags

	// Right shift to get the lod type
#if __SPU
	// in order to use rothm I must take the 2s compliment of shift
	shift = (vec_ushort8)si_sfhi((qword)shift, 0);
	usFlags = (vec_ushort8)si_rothm((qword)usFlags, (qword)shift);
#else
	usFlags = vec_vsrh(usFlags, shift);
#endif

	return usFlags;
}

SCAN_ENTITIES_FORCE_INLINE Vec4V_Out LookUpLodScale(vec_ushort8 lodType, vec_float4 scalesHDToSLOD2, vec_float4 scalesSLOD3ToSLOD4, vec_ushort8 vS2, 
									   vec_uchar16 vBMPermMask, vec_uchar16 vBMPermOffsets)
{
	// shift left by 2 to multiply by 4  
#if __SPU
	vec_ushort8 lodTypeScaled = (vec_ushort8)si_shlh((vec_char16)lodType, (vec_char16)vS2);
#else
	vec_ushort8 lodTypeScaled = vec_vslh(lodType, vS2);
#endif
	
	vec_uchar16 lookup = (vec_uchar16)vec_perm(lodTypeScaled, lodTypeScaled, vBMPermMask); // replicate lod type in each byte for each component eg 
	
	lookup = vec_add(lookup, vBMPermOffsets); // Add the offset in to look up the correct bytes

	vec_float4 lodScale = (vec_float4)vec_perm(scalesHDToSLOD2, scalesSLOD3ToSLOD4, lookup); // This will look up the appropriate m_vPerLodLevelScales

	return (Vec4V)lodScale;
}


SCAN_ENTITIES_FORCE_INLINE VecBoolV_Out CheckIsOrphaned(vec_ushort8 lodType, vec_uint4 orphaned, vec_uchar16 vPermLodType)
{
	vec_uint4 lodTypeUint = (vec_uint4)vec_perm(lodType, lodType, vPermLodType);
#if __SPU
	vec_uint4 result = spu_cmpeq(lodTypeUint, orphaned);
#else
	vec_uint4 result =(vec_uint4)vec_vcmpequw(lodTypeUint, orphaned);
#endif
	return VecBoolV((Vec::Vector_4V)result);
}


SCAN_ENTITIES_FORCE_INLINE Vec4V_Out LoadLodDist(u16* address, vec_uchar16 vPermLodDist)
{
	vec_ushort8 lodDist	= vec_lvlx(0, address);

	vec_ushort8 zero = (vec_ushort8)vec_sub(vPermLodDist, vPermLodDist);

	vec_uint4 lodDistUnit = (vec_uint4)vec_perm(lodDist, zero, vPermLodDist);

	vec_float4 lodDistFloat = vec_ctf(lodDistUnit, 0);

	return (Vec4V)lodDistFloat;
}
#endif

template <bool validCameraDistance> fwTestResults_Out fwScanEntities::AreEntitiesEarlyRejected(const fwEntityDescSoA& soa, Vec3V_In cameraPos, Vec4V* cameraDistance, bool isPrimary)
{
#if SCAN_ENTITIES_DETAILED_STATS
	PF_FUNC(AreEntitiesEarlyRejected);
#endif

#if ALTIVEC_SPU
	vec_float4 vFDefaultScale = { m_scanBaseInfo->m_defaultLodScale, m_scanBaseInfo->m_defaultLodScale, m_scanBaseInfo->m_defaultLodScale, m_scanBaseInfo->m_defaultLodScale };
	vec_ushort8 vS2  = { 2, 2, 2, 2, 0, 0, 0, 0};

	vec_ushort8 vUSLodTypeBits_0 = { 0x70, 0x70, 0x70, 0x70, 0, 0, 0, 0};
	vec_ushort8 vUSXYZW4_0		 = {    4,    4,    4,    4, 0, 0, 0, 0};

	vec_uchar16 vBMPermMask		= { 1, 1, 1, 1, 3, 3, 3, 3, 5, 5, 5, 5, 7, 7, 7, 7 };
	vec_uchar16 vBMPermOffsets	= { 0, 1, 2, 3,  0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3 };
	
	vec_uint4   orphaned		= { LODTYPES_DEPTH_ORPHANHD, LODTYPES_DEPTH_ORPHANHD, LODTYPES_DEPTH_ORPHANHD, LODTYPES_DEPTH_ORPHANHD };
	vec_uchar16 vPermLodType	= { 0, 0, 0, 1,  0, 0, 0, 3,  0, 0, 0, 5,  0, 0, 0, 7 };
	vec_uchar16 vPermLodDist	= { 16, 16, 0, 1,  16, 16, 2, 3,  16, 16, 4, 5,  16, 16, 6, 7 };


	vec_float4 scalesHDToSLOD2;
	vec_float4 scalesSLOD3ToSLOD4;
	if (isPrimary)
	{
		scalesHDToSLOD2    = (vec_float4&)m_scanBaseInfo->m_vPerLodLevelScales[0];
		scalesSLOD3ToSLOD4 = (vec_float4&)m_scanBaseInfo->m_vPerLodLevelScales[1];
	}
	else
	{
		scalesHDToSLOD2    = vFDefaultScale;
		scalesSLOD3ToSLOD4 = vFDefaultScale;
	}
#else
	Vec4V vDefaultScale( m_scanBaseInfo->m_defaultLodScale,  m_scanBaseInfo->m_defaultLodScale,  m_scanBaseInfo->m_defaultLodScale,  m_scanBaseInfo->m_defaultLodScale );
#endif

	fwTestResults	results(V_FALSE);

	const Vec4V		fadeDefaultDist( ScalarV(LODTYPES_FADE_DISTF) );
	const Vec4V		fadeSmallDist( ScalarV(LODTYPES_FADE_SMALL_DISTF) );
	Vec4V			preStreamDist; preStreamDist = Vec4V( ScalarV(isPrimary ? m_scanBaseInfo->m_rendererPreStreamDistance : 0.0f) );
	const Vec4V		cullDistMin2( SoAUtils::Sqr( ScalarV(m_scanBaseInfo->m_fCullDistMin) ) );
	const Vec4V		cullDistMax2( SoAUtils::Sqr( ScalarV(m_scanBaseInfo->m_fCullDistMax) ) );

	const Vec4V		cameraPosX( cameraPos.GetX() );
	const Vec4V		cameraPosY( cameraPos.GetY() );
	const Vec4V		cameraPosZ( cameraPos.GetZ() );

	Vec4V			vecToLodX;
	Vec4V			vecToLodY;
	Vec4V			vecToLodZ;
	Vec4V			unused;
	
	for (int b = 0; b < soa.m_blockCount; ++b)
	{
		const int		i = b << 2;
		Assertf((soa.m_entityPointers[i+0] ? !soa.m_flags[i+0].m_bIsStraddlingPortal : 1) && 
				(soa.m_entityPointers[i+1] ? !soa.m_flags[i+1].m_bIsStraddlingPortal : 1) && 
				(soa.m_entityPointers[i+2] ? !soa.m_flags[i+2].m_bIsStraddlingPortal : 1) && 
				(soa.m_entityPointers[i+3] ? !soa.m_flags[i+3].m_bIsStraddlingPortal : 1), "No entities straddling portals should be using the SOA path");
#if ALTIVEC_SPU
		vec_ushort8 lodType  = LoadLodType((u16*)&soa.m_flags[i], vUSLodTypeBits_0, vUSXYZW4_0);
		VecBoolV    orphanHd = CheckIsOrphaned(lodType, orphaned, vPermLodType);
		const Vec4V	lodDist  = LoadLodDist(&soa.m_lodDistBackups[i], vPermLodDist);
#else // ALTIVEC_SPU

		const BoolV		orphanHd0( soa.m_flags[i+0].m_bLodType == LODTYPES_DEPTH_ORPHANHD );
		const BoolV		orphanHd1( soa.m_flags[i+1].m_bLodType == LODTYPES_DEPTH_ORPHANHD );
		const BoolV		orphanHd2( soa.m_flags[i+2].m_bLodType == LODTYPES_DEPTH_ORPHANHD );
		const BoolV		orphanHd3( soa.m_flags[i+3].m_bLodType == LODTYPES_DEPTH_ORPHANHD );
		VecBoolV		orphanHd(V_F_F_F_F);
		orphanHd.SetX( orphanHd0 );
		orphanHd.SetY( orphanHd1 );
		orphanHd.SetZ( orphanHd2 );
		orphanHd.SetW( orphanHd3 );

		const Vec4V		lodDist(float(soa.m_lodDistBackups[i+0]),
								float(soa.m_lodDistBackups[i+1]), 
								float(soa.m_lodDistBackups[i+2]), 
								float(soa.m_lodDistBackups[i+3]) );
#endif // ALTIVEC_SPU

		SoAUtils::Float16Unpack( soa.m_vecToLodXY[b], vecToLodX, vecToLodY );
		SoAUtils::Float16Unpack( soa.m_vecToLodZandFlags2[b], vecToLodZ, unused );

		const Vec4V		lodPosX = soa.m_x[b] + vecToLodX;
		const Vec4V		lodPosY = soa.m_y[b] + vecToLodY;
		const Vec4V		lodPosZ = soa.m_z[b] + vecToLodZ;

		const VecBoolV	useSmallFadeDist = And( orphanHd, lodDist <= fadeDefaultDist );
		const Vec4V		fadeDist = SelectFT( useSmallFadeDist, fadeDefaultDist, fadeSmallDist );

		const Vec4V		dist2 = SoAUtils::Sqr(lodPosX - cameraPosX) + SoAUtils::Sqr(lodPosY - cameraPosY) + SoAUtils::Sqr(lodPosZ - cameraPosZ);

#if ALTIVEC_SPU
		Vec4V lodScale = LookUpLodScale(lodType, scalesHDToSLOD2, scalesSLOD3ToSLOD4, vS2, vBMPermMask, vBMPermOffsets);
#else // ALTIVEC_SPU
		const float *pPerLodLevelScales = m_scanBaseInfo->m_perLodLevelScales;
		fwEntityDescFlags *pFlags = soa.m_flags;

		// some day maybe we'll have every entity with its own scale, for every LOD level. for now though, grass batches are special
		Vec4V perEntityScale(
			( pFlags[i+0].m_nEntityType==_GTA5_ENTITY_TYPE_GRASS_BATCH ? m_scanBaseInfo->m_grassBatchLodScale : 1.0f ),
			( pFlags[i+1].m_nEntityType==_GTA5_ENTITY_TYPE_GRASS_BATCH ? m_scanBaseInfo->m_grassBatchLodScale : 1.0f ),
			( pFlags[i+2].m_nEntityType==_GTA5_ENTITY_TYPE_GRASS_BATCH ? m_scanBaseInfo->m_grassBatchLodScale : 1.0f ),
			( pFlags[i+3].m_nEntityType==_GTA5_ENTITY_TYPE_GRASS_BATCH ? m_scanBaseInfo->m_grassBatchLodScale : 1.0f ) );

		Vec4V perLodLevelScale(pPerLodLevelScales[pFlags[i+0].m_bLodType],
			pPerLodLevelScales[pFlags[i+1].m_bLodType],
			pPerLodLevelScales[pFlags[i+2].m_bLodType],
			pPerLodLevelScales[pFlags[i+3].m_bLodType]);

		Vec4V lodScale = perEntityScale * perLodLevelScale;

		if (!isPrimary)
		{
			lodScale = vDefaultScale;
		}

#endif// !ALTIVEC_SPU
		const Vec4V		visibilityDist2 = SoAUtils::Sqr(Scale(lodDist, lodScale) + fadeDist + preStreamDist);

		const VecBoolV	rejected = Or(IsGreaterThan(dist2, visibilityDist2), Or(dist2 > cullDistMax2, dist2 < cullDistMin2));

		SoAUtils::SetResult( results, b, rejected );

		// 8/15/12 - cthomas - Only set the camera distance (and take the performance cost of a square root) 
		// if the camera distance has been requested by passing in the pointer.
		if(validCameraDistance)
		{
			cameraDistance[b] = Sqrt(dist2);
		}

	}

	return results;
}

bool fwScanEntities::IsEntityEarlyRejected(const fwEntityDesc& entityDesc, Vec3V_In cameraPosition, float* optCameraDist, bool isPrimary)
{
#if SCAN_ENTITIES_DETAILED_STATS
	PF_FUNC(IsEntityEarlyRejected);
#endif

#if ENABLE_SCAN_TIMERS
	fwScanLocalTimerFunc	scanTimer( &m_earlyRejectLocalTimer );
#endif

	const float		fadeDist = ( entityDesc.GetLodType()==LODTYPES_DEPTH_ORPHANHD && entityDesc.GetLodDistCached()<=LODTYPES_FADE_DIST ) ?
									LODTYPES_FADE_SMALL_DISTF : LODTYPES_FADE_DISTF;

	const float		preStream = isPrimary ? m_scanBaseInfo->m_rendererPreStreamDistance : 0.0f;
	const float		entityTypeScale = ( (entityDesc.GetEntityType()==_GTA5_ENTITY_TYPE_GRASS_BATCH) ? m_scanBaseInfo->m_grassBatchLodScale : 1.0f );
	const float		lodScale = ( isPrimary ? m_scanBaseInfo->m_perLodLevelScales[entityDesc.GetLodType()] : m_scanBaseInfo->m_defaultLodScale ) * entityTypeScale;
	const float		dist2 = MagSquared( entityDesc.GetLodPosition() - cameraPosition ).Getf();

	const float		scaledLodDist = entityDesc.GetLodDistCached() * lodScale;
	const float		visibilityDist = scaledLodDist + fadeDist + preStream;
	const float		visibilityDist2 = visibilityDist * visibilityDist;
	
	bool bRejected = (dist2 > visibilityDist2);
	if (!bRejected)
	{
		bRejected = entityDesc.IsStraddlingPortal() && !(m_controlFlags & SCAN_ENTITIES_FLAG_STRADDLING_PORTALS_CONTAINER);
	}
	// time cycle modifier boxes can specify an entity rejection distance
	if (!bRejected)
	{
		float fCullDistMin2 = m_scanBaseInfo->m_fCullDistMin * m_scanBaseInfo->m_fCullDistMin;
		float fCullDistMax2 = m_scanBaseInfo->m_fCullDistMax * m_scanBaseInfo->m_fCullDistMax;

		bRejected = (dist2 > fCullDistMax2 || dist2 < fCullDistMin2);
	}

#if __BANK
	const u32 rejectFlags =
		SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_LISTED |
		SCAN_DEBUG_EARLY_REJECT_LISTED |
		SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_SELECTED |
		SCAN_DEBUG_EARLY_REJECT_SELECTED |
		SCAN_DEBUG_DONT_EARLY_REJECT_SELECTED |
		SCAN_DEBUG_DONT_EARLY_REJECT_ANY;

	// normally none of these flags are set, so let's only do this work if we need to
	if (g_scanDebugFlags & rejectFlags)
	{
		// are we allowed to early rejected any entity?
		if (g_scanDebugFlags & SCAN_DEBUG_DONT_EARLY_REJECT_ANY)
		{
			bRejected = false;
		}
		// are we allowed to early rejected the selected entity?
		else if ((g_scanDebugFlags & SCAN_DEBUG_DONT_EARLY_REJECT_SELECTED) && m_scanBaseInfo->m_traceEntityAddress==entityDesc.GetEntity())
		{
			bRejected = false;
		}
		

		if (!bRejected)
		{
			if (g_scanDebugFlags & (SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_LISTED | SCAN_DEBUG_EARLY_REJECT_LISTED))
			{
				const fwEntity* ep = entityDesc.GetEntity();
				bool bListed = false;

				for (int i = 0; i < m_scanBaseInfo->m_traceEntityAddressCount; i++)
				{
					if (m_scanBaseInfo->m_traceEntityAddressList[i] == ep)
					{
						bListed = true;
						break;
					}
				}

				// debug cull case #1 - reject all except listed entities
				if (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_LISTED)
				{
					bRejected = !bListed;
				}
				// debug cull case #2 - reject listed entities
				else
				{
					bRejected = bListed;
				}
			}
			// debug cull case #3 - reject all except selected entity
			else if (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_SELECTED)
			{
				bRejected = (m_scanBaseInfo->m_traceEntityAddress != entityDesc.GetEntity());
			}
			// debug cull case #4 - reject selected entity
			else if (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_SELECTED)
			{
				bRejected = (m_scanBaseInfo->m_traceEntityAddress == entityDesc.GetEntity());
			}
		}
	}
#endif // __BANK

	// sqrtf is 56 cycles, don't waste the time if we don't need it.
	if (optCameraDist)
		*optCameraDist = sqrtf(dist2);
	return bRejected;
}

CompileTimeAssert(SCAN_ENTITIES_FLAG_INTERIOR == 1);
CompileTimeAssert(LODTYPES_DEPTH_HD == 0);

// TODO POST GTAV -- this function could be implemented something like this: "mask = 1 << (lodType + (isInterior << 3));"
inline u32 LodTypeToLodMask(u32 lodType, u32 controlFlags)
{
	Assert(lodType <= LODTYPES_DEPTH_SLOD4);

	u32 mask = 1 << lodType;

	// handle orphanHD
	const u32 orphanBit = 1 << LODTYPES_DEPTH_ORPHANHD;
	mask |= (mask & orphanBit) >> LODTYPES_DEPTH_ORPHANHD;

	// if HD
	const u32 hdBit = (1 << LODTYPES_DEPTH_HD);
	u32 hd = mask & hdBit;
	hd = hd << (1 - (controlFlags & SCAN_ENTITIES_FLAG_INTERIOR));

	// if Lod
	u32 lod = mask & ~(hdBit | orphanBit);
	const u32 slod4Bit = 1 << LODTYPES_DEPTH_SLOD4;

	// if slod4 shuffle back one 1
	lod = lod >> ((lod & slod4Bit) >> LODTYPES_DEPTH_SLOD4);

	// left shift one to line up with LODMASK_LOD
	lod = lod << 1;

	return hd | lod;
}

bool fwScanEntities::IsEntityExcludedInRenderPhase(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc)
{
	if ( entityDesc.IsVisible() == false )
		return true;

	u8 lodMask = cullShape.GetLodMask();

	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_WATER_REFLECTION )
	{
		if ( entityDesc.GetIsWaterReflectionProxy() )
		{
			if ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_HIDE_PROXIES_IN_WATER_REFLECTION )
				return true;
			else if ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_WATER_REFLECTION_PROXIES_IGNORE_LOD_MASK )
				lodMask = rage::fwRenderPhase::LODMASK_NONE;
		}
		else if ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_HIDE_NON_PROXIES_IN_WATER_REFLECTION )
			return true;
		else if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_WATER_REFLECTION_PROXIES_ONLY )
			return true;

		// only buildings use lod mask
		if (entityDesc.GetVisibilityType() != _GTA5_VIS_ENTITY_LOLOD_BUILDING &&
			entityDesc.GetVisibilityType() != _GTA5_VIS_ENTITY_HILOD_BUILDING)
		{
			lodMask = rage::fwRenderPhase::LODMASK_NONE;
		}
		else if ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_WATER_REFLECTION_LOD_MASK_FORCE_ORPHAN_HD )
		{
			if ( entityDesc.GetLodType() == LODTYPES_DEPTH_ORPHANHD )
				lodMask = rage::fwRenderPhase::LODMASK_NONE;
		}
	}

	if (lodMask != rage::fwRenderPhase::LODMASK_NONE)
	{
		if (entityDesc.GetEntityType() == _GTA5_ENTITY_TYPE_LIGHT)
		{
			if (!(lodMask & rage::fwRenderPhase::LODMASK_LIGHT))
			{
				return true;
			}
		}
		else
		{
			if (!(lodMask & LodTypeToLodMask(entityDesc.GetLodType(), m_controlFlags)))
			{
				return true;
			}
		}
	}

#if __BANK
	if ( g_scanDebugFlags & SCAN_DEBUG_HIDE_NON_SHADOW_CASTING_ENTITIES )
	{
		if ( ( entityDesc.GetRenderPhaseVisibilityMask() & _GTA5_VIS_PHASE_MASK_SHADOWS ) != _GTA5_VIS_PHASE_MASK_SHADOWS )
			return true;
	}

	const u32 SCAN_DEBUG_HIDE_PROXY_MASK =
		SCAN_DEBUG_HIDE_SHADOW_PROXIES_IN_SHADOW			|
		SCAN_DEBUG_HIDE_NON_SHADOW_PROXIES_IN_SHADOW 		|
		SCAN_DEBUG_HIDE_REFLECTION_PROXIES_IN_REFLECTION	|
		SCAN_DEBUG_HIDE_NON_REFLECTION_PROXIES_IN_REFLECTION;

	if ( g_scanDebugFlagsPortals & SCAN_DEBUG_HIDE_PROXY_MASK )
	{
		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS )
		{
			if ( ( g_scanDebugFlagsPortals & SCAN_DEBUG_HIDE_SHADOW_PROXIES_IN_SHADOW ) != 0 && entityDesc.GetIsShadowProxy() )
				return true;

			if ( ( g_scanDebugFlagsPortals & SCAN_DEBUG_HIDE_NON_SHADOW_PROXIES_IN_SHADOW ) != 0 && !entityDesc.GetIsShadowProxy() )
				return true;
		}

		if ( cullShape.GetFlags() & ( CULL_SHAPE_FLAG_REFLECTIONS|CULL_SHAPE_FLAG_MIRROR|CULL_SHAPE_FLAG_WATER_REFLECTION ) )
		{
			if ( ( g_scanDebugFlagsPortals & SCAN_DEBUG_HIDE_REFLECTION_PROXIES_IN_REFLECTION ) != 0 && entityDesc.GetIsReflectionProxy() )
				return true;

			if ( ( g_scanDebugFlagsPortals & SCAN_DEBUG_HIDE_NON_REFLECTION_PROXIES_IN_REFLECTION ) != 0 && !entityDesc.GetIsReflectionProxy() )
				return true;
		}
	}
#endif // __BANK

	if ((cullShape.GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS) && 
		(entityDesc.GetEntityType() == _GTA5_ENTITY_TYPE_OBJECT || entityDesc.GetEntityType() == _GTA5_ENTITY_TYPE_DUMMY_OBJECT) && 
		!entityDesc.GetIsADoor())
	{
		return true;
	}

	if ( ( cullShape.GetEntityVisibilityMask() & ( 1 << entityDesc.GetVisibilityType() ) ) == 0 )
		return true;

	if ( ( entityDesc.GetRenderPhaseVisibilityMask() & ( 1 << cullShape.GetVisibilityType() ) ) == 0 )
	{
#if __BANK
		bool bForce = false;

		const u32 SCAN_DEBUG_FORCE_PROXY_MASK =
			SCAN_DEBUG_FORCE_SHADOW_PROXIES_IN_PRIMARY		|
			SCAN_DEBUG_FORCE_REFLECTION_PROXIES_IN_PRIMARY	;

		if ( g_scanDebugFlagsPortals & SCAN_DEBUG_FORCE_PROXY_MASK )
		{
			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
			{
				if ( ( g_scanDebugFlagsPortals & SCAN_DEBUG_FORCE_SHADOW_PROXIES_IN_PRIMARY ) != 0 && entityDesc.GetIsShadowProxy() )
					bForce = true;

				if ( ( g_scanDebugFlagsPortals & SCAN_DEBUG_FORCE_REFLECTION_PROXIES_IN_PRIMARY ) != 0 && entityDesc.GetIsReflectionProxy() )
					bForce = true;
			}
		}

		const u32 SCAN_DEBUG_FORCE_PROXY_MASK2 =
			SCAN_DEBUG_EXT_FORCE_NON_SHADOW_CASTING_ENTITIES_IN_SHADOW	|
			SCAN_DEBUG_EXT_FORCE_NON_REFLECTION_ENTITIES_IN_REFLECTION	;

		if ( g_scanDebugFlagsExtended & SCAN_DEBUG_FORCE_PROXY_MASK2 )
		{
			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS )
			{
				if ( ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_FORCE_NON_SHADOW_CASTING_ENTITIES_IN_SHADOW ) != 0 )
					bForce = true;
			}
			else if ( cullShape.GetFlags() & ( CULL_SHAPE_FLAG_REFLECTIONS|CULL_SHAPE_FLAG_MIRROR|CULL_SHAPE_FLAG_WATER_REFLECTION ) )
			{
				if ( ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_FORCE_NON_REFLECTION_ENTITIES_IN_REFLECTION ) != 0 )
					bForce = true;
			}
		}

		if ( !bForce )
#endif // __BANK
		{
			return true;
		}
	}

	if ( (cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY) && !(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_IGNORE_RENDER_LOD_ONLY_FLAG) )
	{
		if ( (m_controlFlags & SCAN_ENTITIES_RENDER_LODS_ONLY) && entityDesc.IsHighDetail() )
			return true;
	}

	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_RENDER_LOD_BUILDING_BASED_ON_INTERIOR )
	{
		if ( m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR )
		{
			if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_LOLOD_BUILDING )
				return true;
		}
		else // exterior
		{
			if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_HILOD_BUILDING )
				return true;
		}
	}

	return false;
}

bool fwScanEntities::IsEntityExcludedByLodRange(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc)
{
	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_WATER_REFLECTION )
	{
		// only buildings use lod range
		if (entityDesc.GetVisibilityType() != _GTA5_VIS_ENTITY_LOLOD_BUILDING &&
			entityDesc.GetVisibilityType() != _GTA5_VIS_ENTITY_HILOD_BUILDING)
		{
			return false;
		}
		else if ( entityDesc.GetIsWaterReflectionProxy() && (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_WATER_REFLECTION_PROXIES_IGNORE_LOD_RANGES) )
			return false;
	}
	else if ( !entityDesc.IsRootLod() && ((cullShape.GetFlags() & CULL_SHAPE_FLAG_RENDER_ONLY_ROOT_LOD)!=0) )
		return true;

	float				rangeMin, rangeMax;
	cullShape.GetLodRange( entityDesc.GetLodType(), &rangeMin, &rangeMax );

	const spdSphere		minSphere( m_primaryCameraPosition, ScalarV(rangeMin) );
	const spdSphere		maxSphere( m_primaryCameraPosition, ScalarV(rangeMax) );

	if (cullShape.GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS)
	{
		const spdSphere	entitySphere = entityDesc.GetApproxBoundingSphere();
		const bool	isIncluded = rangeMin >= 0 && rangeMax >= 0 && entitySphere.IntersectsSphere( maxSphere ) && !minSphere.ContainsSphere( entitySphere );
		return !isIncluded;
	}
	else
	{
		const spdAABB entityAabb = entityDesc.GetApproxBoundingBox();
		const bool isIncluded = rangeMin >= 0 && rangeMax >= 0 && entityAabb.IntersectsSphere( maxSphere ) && !entityAabb.ContainedBySphere( minSphere );
		return !isIncluded;
	}
}

void  fwScanEntities::CalculateProjectOcclusionOBB(const fwScanCascadeShadowInfo& cascadeShadowInfo, const spdAABB& aabb, Mat33V_In shadowToWorldMtx, ScalarV_In groundLevel, Vec3V_InOut obbMin_Out, Vec3V_InOut obbMax_Out, Mat34V_InOut OBBToWorldMtx_Out BANK_ONLY(, bool bUseProjectedShadowOcclusionClipping))
{
	Vec3V center =(aabb.GetMax()+aabb.GetMin())*Vec3V(V_HALF);
	Vec3V diag =(aabb.GetMax()-aabb.GetMin())*Vec3V(V_HALF);
	Vec3V lightDir = shadowToWorldMtx.c();
	ScalarV invDirZ = Abs(InvertSafe(lightDir.GetZ()));
	OBBToWorldMtx_Out.SetCols(
		shadowToWorldMtx.a(),
		shadowToWorldMtx.b(),
		shadowToWorldMtx.c(),
		center);
	
	// calculates extents of box based on the sun's view
	// We use he diagonal of the aabb to get the OBB to encapsulate the AABB
	Vec3V extent(Dot(Abs(shadowToWorldMtx.a()),diag), Dot(Abs(shadowToWorldMtx.b()),diag),Dot(Abs(shadowToWorldMtx.c()),diag) );

	//get the height component of the diagonal
	const Vec3V OOBBDiagonal = shadowToWorldMtx.b() * extent.GetY();
	const ScalarV OBBDiagonalHeight = Abs(OOBBDiagonal.GetZ());

	// add the OOBB Diagonal height to the center and ground level to get the right amount of distance on the ground
	ScalarV height = (center.GetZ() + OBBDiagonalHeight - groundLevel)*invDirZ;

	obbMin_Out = -extent;

	//Get top four corners of OBB cap
	Vec3V p1 = Multiply(OBBToWorldMtx_Out, Vec4V(obbMin_Out,							ScalarV(V_ONE)));
	//reference code
	//Vec3V p2= Multiply(OBBToWorldMtx_Out, Vec4V(obbMin_Out*Vec3V(-1.0f,  1.0f, 1.0f),	ScalarV(V_ONE)));
	//Vec3V p3 = Multiply(OBBToWorldMtx_Out, Vec4V(obbMin_Out*Vec3V( 1.0f, -1.0f, 1.0f),	ScalarV(V_ONE)));
	//Vec3V p4 = Multiply(OBBToWorldMtx_Out, Vec4V(obbMin_Out*Vec3V(-1.0f, -1.0f, 1.0f),	ScalarV(V_ONE)));
	Vec3V p2 = p1 + OBBToWorldMtx_Out.GetCol0()*extent.GetX()*ScalarV(V_TWO);
	Vec3V p3 = p1 + OBBToWorldMtx_Out.GetCol1()*extent.GetY()*ScalarV(V_TWO);
	Vec3V p4 = p2 + OBBToWorldMtx_Out.GetCol1()*extent.GetY()*ScalarV(V_TWO);

	if(cascadeShadowInfo.IsFrustumFacingShadow() BANK_ONLY(&& bUseProjectedShadowOcclusionClipping))
	{	
		Vec2V length(100.0f, 100.0f);
		bool skipProjectedShadowClipping = false;
		//hack to prevent popping from really large obb close to the camera
		if(IsGreaterThanAll(extent.GetXY(), length))
		{
			Vec2V shadowSpaceOBBCenter = Multiply(cascadeShadowInfo.GetWorldToShadow33(), center).GetXY();
			Vec2V OBBMin = shadowSpaceOBBCenter - extent.GetXY();
			Vec2V OBBMax = shadowSpaceOBBCenter + extent.GetXY();
			if(IsLessThanAll(OBBMin, cascadeShadowInfo.GetShadowCamPos()) && IsGreaterThanAll(OBBMax, cascadeShadowInfo.GetShadowCamPos()))
			   skipProjectedShadowClipping = true;
		}

		if(!skipProjectedShadowClipping)
		{
			//Project each point in shadow viewport and get the max distance
			ScalarV finalTA = cascadeShadowInfo.ProjectPointToViewportAlongShadowDirection(p1);
			ScalarV finalTB = cascadeShadowInfo.ProjectPointToViewportAlongShadowDirection(p2);
			ScalarV finalTC = cascadeShadowInfo.ProjectPointToViewportAlongShadowDirection(p3);
			ScalarV finalTD = cascadeShadowInfo.ProjectPointToViewportAlongShadowDirection(p4);
			ScalarV finalT	= Max(Max(finalTA, finalTB), Max(finalTC, finalTD));
			static volatile dev_float length = 100.0f;
			//project center point for large boxes, still not perfect but fixes all cases in game and should be much faster than my perfect PolyClip() solution
			if(extent.GetXf() > length || extent.GetYf() > length)
			{
				Vec3V p5 = (p1 + p4)*ScalarV(V_HALF); 
				ScalarV finalTE = cascadeShadowInfo.ProjectPointToViewportAlongShadowDirection(p5);
				finalT = Max(finalT, finalTE);
			}

			//This is an optimization to shrink the length of the OBB based on whichever is higher, the lower point on the ground or any of the projected points
			height			= Min(finalT, height);
			height			= Max(height, extent.GetZ());
		}
	}

	obbMax_Out = Vec3V(extent.GetXY(), height);
}

SCAN_ENTITIES_FORCE_INLINE bool fwScanEntities::TestProjectedOcclusion(const spdAABB& aabb, Vec3V_In cameraPos)
{
	ScalarV depthBias = ScalarV( m_scanBaseInfo->m_rasterizerDepthTestBias[ 0 ] );
	const Mat33V shadowToWorldMtx = m_scanBaseInfo->m_cascadeShadowInfo.GetShadowToWorld33();	
	const ScalarV groundLevel(m_scanBaseInfo->m_receiverShadowHeightMin);
	Vec3V obbMin, obbMax;
	Mat34V OBBToWorldMtx(V_IDENTITY);

	CalculateProjectOcclusionOBB(m_scanBaseInfo->m_cascadeShadowInfo, aabb, shadowToWorldMtx, groundLevel, obbMin, obbMax, OBBToWorldMtx BANK_ONLY(, m_scanBaseInfo->m_useProjectedShadowOcclusionClipping));

	ScalarV area =  rstTestOOBBExactEdgeList(m_scanBaseInfo->m_transform[ 0 ], 
		ScalarVFromF32(m_scanBaseInfo->m_depthRescale[ 0 ]),
		m_hiZBuffer[ 0 ] , cameraPos, OBBToWorldMtx,obbMin, obbMax, depthBias
		, m_scanBaseInfo->m_minMaxBounds[0], m_scanBaseInfo->m_minZ[0]
	DEV_ONLY(, &(m_scanBaseInfo->m_TrivialAcceptActiveFrustumCount[0]), &(m_scanBaseInfo->m_TrivialAcceptMinZCount[0]), &(m_scanBaseInfo->m_TrivialAcceptVisiblePixelCount[0]))
		BANK_ONLY(, m_scanBaseInfo->m_newOcclussionAABBExactPixelsThresholdSimple, m_scanBaseInfo->m_useTrivialAcceptTest, m_scanBaseInfo->m_useTrivialAcceptVisiblePixelTest, m_scanBaseInfo->m_useMinZClipping));

	return IsGreaterThanAll( area, ScalarV(V_ZERO))==0;
}

#if __BANK

#define FWSCAN_COMPUTE_OCCLUSION_QUERY_TYPE_BASED_ON_AREA (__D3D11 || RSG_ORBIS)

//Bank code has a lot of debug stuff.
//Non bank code is defined below which is much cleaner
//ANY Change to this function should be added to the dupe function below
SCAN_ENTITIES_FORCE_INLINE bool fwScanEntities::TestOcclusion(const spdAABB& aabb, Vec3V_In cameraPos, const int occluderPhase, const int occlusionTransform, u32 entityType)
{
	bool result = false;
	ScalarV zero(V_ZERO);

#if __DEV
	sysInterlockedIncrement( &m_scanBaseInfo->m_OccludedTestedCount[occluderPhase]);	
#endif

	ScalarV rescaleDepth = ScalarVFromF32(m_scanBaseInfo->m_depthRescale[ occlusionTransform ]);
	rstSplatMat44V splatMtx ;

	Vec3V scbmin, scbmax;
	const Vec3V extents( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, 0.f);
#if __BANK
	if(!m_scanBaseInfo->m_useNewOcclusionExact)
	{
		rstCreateSplatMat44V( m_scanBaseInfo->m_transform[ occlusionTransform ], splatMtx );
		if(!rstCalculateScreenSpaceBoundIsVis(	splatMtx, rescaleDepth, aabb.GetMin(), aabb.GetMax(), scbmin, scbmax,extents) )// pipeline these two
		{
			return true;// is offscreen
		}
	}
#endif

	// 8/9/12 - cthomas - Always use the faster HiZLarge-style functions (as opposed to the exact function). The exact function is 
	// 5 - 10x slower - ~5x slower than HiZLarge, and ~10x slower than HiZLargeApprox (with early out), and using the HiZLarge-style 
	// queries for occlusion only has an very minimal impact on GPU.
	// 11/10/12 jlandry - Use the exact functions for Peds and Vehicles

#if ENABLE_NON_EXACT_OCCLUSION
	bool useExact = ((entityType == _GTA5_ENTITY_TYPE_PED || entityType == _GTA5_ENTITY_TYPE_VEHICLE) && !m_scanBaseInfo->m_useSimpleQueries) ||
		m_scanBaseInfo->m_useForceExactOcclusionTest || m_scanBaseInfo->m_useNewOcclusionExact;

#if FWSCAN_COMPUTE_OCCLUSION_QUERY_TYPE_BASED_ON_AREA
	BANK_ONLY(if (g_bAreaBasedTesting))
	{
		float ScaleFactor = (float)(RAST_HIZ_WIDTH*RAST_HIZ_HEIGHT);
		ScalarV threshold = ScalarV( (float)m_scanBaseInfo->m_useExactSelectionSize*ScaleFactor);
		Vec2V area = (scbmax - scbmin).GetXY();
		useExact = useExact || IsTrue( threshold < area.GetX()*area.GetY());
	}
#endif //FWSCAN_COMPUTE_OCCLUSION_QUERY_TYPE_BASED_ON_AREA
#else //ENABLE_NON_EXACT_OCCLUSION
	(void)entityType;
#endif //ENABLE_NON_EXACT_OCCLUSION
	ScalarV depthBias = ScalarV( m_scanBaseInfo->m_rasterizerDepthTestBias[ occlusionTransform ] );

#if ENABLE_NON_EXACT_OCCLUSION
	if ( useExact )
#endif
	{
#if ENABLE_OLD_EXACT_OCCLUSION
		if(m_scanBaseInfo->m_useNewOcclusionExact)
#endif //ENABLE_OLD_EXACT_OCCLUSION
		{
			ScalarV area = rstTestAABBExactEdgeList( occluderPhase ==  SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS,
				m_scanBaseInfo->m_transform[ occluderPhase ], 
				ScalarVFromF32(m_scanBaseInfo->m_depthRescale[ occluderPhase ]),
				m_hiZBuffer[ occluderPhase ] , cameraPos, aabb.GetMin(), aabb.GetMax(), depthBias 
				, m_scanBaseInfo->m_minMaxBounds[occluderPhase], m_scanBaseInfo->m_minZ[occluderPhase]
				DEV_ONLY(, &(m_scanBaseInfo->m_TrivialAcceptActiveFrustumCount[occluderPhase]), &(m_scanBaseInfo->m_TrivialAcceptMinZCount[occluderPhase]), &(m_scanBaseInfo->m_TrivialAcceptVisiblePixelCount[occluderPhase]))
				BANK_ONLY(, m_scanBaseInfo->m_newOcclussionAABBExactPixelsThresholdSimple, m_scanBaseInfo->m_useTrivialAcceptTest, m_scanBaseInfo->m_useTrivialAcceptVisiblePixelTest, m_scanBaseInfo->m_useMinZClipping));
			result =  IsGreaterThanAll( area, ScalarV(V_ZERO))==0;

		}
#if ENABLE_OLD_EXACT_OCCLUSION
		else
		{

			if(rstTestAABBTrivialAccept(scbmin, scbmax, m_scanBaseInfo->m_minMaxBounds[occluderPhase], m_scanBaseInfo->m_minZ[occluderPhase], scbmin.GetZ() DEV_ONLY(, &(m_scanBaseInfo->m_TrivialAcceptActiveFrustumCount[occluderPhase]), &(m_scanBaseInfo->m_TrivialAcceptMinZCount[occluderPhase])))
#if __BANK
				&& m_scanBaseInfo->m_useTrivialAcceptTest
#endif
				)
			{
				result = false;
				return result;
			}

			ScalarV area = rstTestAABBExact( m_scanBaseInfo->m_transform[ occluderPhase ], 
								ScalarVFromF32(m_scanBaseInfo->m_depthRescale[ occluderPhase ]),
								m_hiZBuffer[ occluderPhase ] , aabb.GetMin(), aabb.GetMax(), occluderPhase == SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS ?  depthBias : ScalarV(V_ZERO) );
			result =  IsGreaterThanAll( area, ScalarV(V_ZERO))==0;

		}
#endif //ENABLE_OLD_EXACT_OCCLUSION

	}
#if ENABLE_NON_EXACT_OCCLUSION
	else
	{	
		if(rstTestAABBTrivialAccept(scbmin, scbmax, m_scanBaseInfo->m_minMaxBounds[occluderPhase], m_scanBaseInfo->m_minZ[occluderPhase], scbmin.GetZ() DEV_ONLY(, &(m_scanBaseInfo->m_TrivialAcceptActiveFrustumCount[occluderPhase]), &(m_scanBaseInfo->m_TrivialAcceptMinZCount[occluderPhase])))
#if __BANK
			&& m_scanBaseInfo->m_useTrivialAcceptTest
#endif
			)
		{
			result = false;
			return result;
		}
		// apply depth bias
		scbmin.SetZ( scbmin.GetZ() - depthBias );

		// 8/9/12 - cthomas - Use new rstScanQueryHiZLargeApprox() function, which also includes "early out" when not occluded. 
		// The new rstScanQueryHiZLargeApprox() function was found in testing to produce results that are extremely similar to 
		// the rstScanQueryHiZLarge() function - perhaps occluding 1-2 entities less per scene (out of hundreds that are found 
		// to be occluded).
		//VecBoolV isVis =  rstScanQueryHiZLarge( m_hiZBuffer[ occluderPhase ], scbmin, scbmax );
		VecBoolV isVis = rstScanQueryHiZLargeApprox( m_hiZBuffer[ occluderPhase ], scbmin, scbmax );
		isVis = isVis | VecBoolV( scbmin.GetZ() < zero); // if clip front plane depth will always be in front ( assuming frustum check already applied)
		isVis = isVis & VecBoolV( scbmax.GetZ() > zero); // but if behind front plane then chuck it

		result =  IsEqualIntAll(isVis,  VecBoolV(V_F_F_F_F) ) ? true : false;
	}
#endif //ENABLE_NON_EXACT_OCCLUSION
	
#if __DEV
	if (!result)
		sysInterlockedIncrement( &m_scanBaseInfo->m_OccludedCount[occluderPhase]);
#endif

	return result;
}

#else

//Cleaned up code for Non Bank Releases
//ANY Change to this function should be added to the dupe function Above
SCAN_ENTITIES_FORCE_INLINE bool fwScanEntities::TestOcclusion(const spdAABB& aabb, Vec3V_In cameraPos, const int occluderPhase, const int occlusionTransform)
{
	bool result = false;
	ScalarV zero(V_ZERO);
	
	ScalarV depthBias = ScalarV( m_scanBaseInfo->m_rasterizerDepthTestBias[ occlusionTransform ] );

	ScalarV area = rstTestAABBExactEdgeList( occluderPhase  ==  SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS,
		m_scanBaseInfo->m_transform[ occluderPhase ], 
		ScalarVFromF32(m_scanBaseInfo->m_depthRescale[ occluderPhase ]),
		m_hiZBuffer[ occluderPhase ] , cameraPos, aabb.GetMin(), aabb.GetMax(), depthBias
		, m_scanBaseInfo->m_minMaxBounds[occluderPhase], m_scanBaseInfo->m_minZ[occluderPhase]);
	result =  IsGreaterThanAll( area, ScalarV(V_ZERO))==0;

	return result;
}

#endif

static bool DoesEntityTouchWaterStencil(Vec3V_In bmin, Vec3V_In bmax, Mat44V_In fullCompositeMtx, int w, int h, const u32* stencil, bool reflectionVP, bool bDebugSelected)
{
	(void)bDebugSelected;
	(void)h;

	rstSplatMat44V splatMtx;
	rstCreateSplatMat44V(fullCompositeMtx, splatMtx);

	Vec3V scbmin;
	Vec3V scbmax;
	const Vec3V extents( (float)RAST_HIZ_WIDTH, (float)RAST_HIZ_HEIGHT, 0.f);

	if (!rstCalculateScreenSpaceBoundIsVis(splatMtx, ScalarV(V_ONE), bmin, bmax, scbmin, scbmax,extents))
	{
#if __BANK && !__SPU
		if (bDebugSelected) { grcDebugDraw::AddDebugOutput("selected entity failed rstCalculateScreenSpaceBoundIsVis"); }
#endif
		return false;
	}

	// TODO -- roll this shit into the matrix so we don't have to correct for it here ..
	{
		scbmin *= ScalarV(V_TWO); // the matrix was scaling by 1/2 ..
		scbmax *= ScalarV(V_TWO);
		if(reflectionVP)
		{
			const ScalarV x0 = ScalarV((float)w) - scbmax.GetX(); // the matrix was also flipping in x ..
			const ScalarV x1 = ScalarV((float)w) - scbmin.GetX();
			scbmin.SetX(x0);
			scbmax.SetX(x1);
		}
	}

	scbmin = RoundToNearestIntNegInf(scbmin);
	scbmax = RoundToNearestIntPosInf(scbmax);

	const Vec4V boundsClamp(128.0f, 128.0f, 72.0f, 72.0f);
	const Vec4V boundsInt = FloatToIntRaw<0>(Clamp(MergeXY(scbmin, scbmax), Vec4V(V_ZERO), boundsClamp));

	const int j0 = boundsInt.GetZi();
	const int j1 = boundsInt.GetWi();
#if __BANK
	const int i0 = boundsInt.GetXi();
	const int i1 = boundsInt.GetYi();
#endif // __BANK

#if __BANK && !__SPU
	if (bDebugSelected) { grcDebugDraw::AddDebugOutput("selected entity bounds is [%d..%d][%d..%d], z=[%.3f..%.3f]", i0, i1, j0, j1, scbmin.GetZf(), scbmax.GetZf()); }
#endif

	if (IsLessThanOrEqualAll(scbmax.GetZ(), ScalarV(V_ZERO)))
	{
		return false; // entire bounds is behind the camera
	}
	else if (IsLessThanOrEqualAll(scbmin.GetZ(), ScalarV(V_ZERO)))
	{
		return true; // bounds crosses z=0 plane, can't really determine coverage
	}

	bool bTouchesStencil = false;

#if __BANK
	const bool bUseReferenceCode = (g_scanDebugFlagsPortals & SCAN_DEBUG_USE_NEW_WATER_STENCIL_TEST_REFERENCE_CODE) != 0;
	const bool bUseNonVectorCode = (g_scanDebugFlagsPortals & SCAN_DEBUG_USE_NEW_WATER_STENCIL_TEST_NONVECTOR_CODE) != 0;

	const int stencilStride = (w + 31)/32;

	if (bUseReferenceCode || w != 128)
	{
		for (int j = j0; j < j1; j++)
		{
			const u32* stencilRow = &stencil[j*stencilStride];

			for (int i = i0; i < i1; i++)
			{
				if (stencilRow[i/32] & BIT(31 - i%32))
				{
					bTouchesStencil = true;
					break;
				}
			}

			if (bTouchesStencil)
			{
				break;
			}
		}
	}
	else if (bUseNonVectorCode)
	{
		const int sr = w - i1;
		const int sl = 0 + i0;
#if RSG_CPU_X64
		const u64 maskA = ClampedShiftRight64(~0ULL, sr) & ClampedShiftLeft64(~0ULL, sl - 64);
		const u64 maskB = ClampedShiftLeft64(~0ULL, sl) & ClampedShiftRight64(~0ULL, sr - 64);
#else
		u64 maskA = ((~0ULL) >> sr) & ((~0ULL) << Max<int>(0, sl - 64));
		u64 maskB = ((~0ULL) << sl) & ((~0ULL) >> Max<int>(0, sr - 64));

		if (i0 == i1)
		{
			maskA = 0;
			maskB = 0;
		}
#endif
		const int j0_2 = j0*2;
		const int j1_2 = j1*2;

		for (int j = j0_2; j < j1_2; j += 2)
		{
			if ((maskA & stencil[j + 0]) |
				(maskB & stencil[j + 1]))
			{
				bTouchesStencil = true;
				break;
			}
		}
	}
	else
#endif // __BANK
	{
		Vec4V stencilMask;

#if RSG_CPU_INTEL
		stencilMask = GenerateMaskShiftLeft128(boundsInt);
#else
		stencilMask = GenerateMaskShiftRight128(boundsInt);
#endif //RSG_CPU_INTEL

		const Vec4V* stencilVec = (const Vec4V*)stencil;

		Vec4V bits(V_ZERO);

		for (int j = j0; j < j1; j++)
		{
			bits |= stencilVec[j];
		}

		bTouchesStencil = !IsEqualIntAll(bits & stencilMask, Vec4V(V_ZERO));
	}

#if __BANK && !__SPU
	if (bDebugSelected)
	{
		const Color32 boundsColour = bTouchesStencil ? Color32(255,255,0,255) : Color32(196,196,0,255);
		const Color32 refBoxColour = bTouchesStencil ? Color32(255,0,0,255) : Color32(0,196,0,255);

		const Vec2V scale(1.0f/(float)w, 1.0f/(float)h);

		// draw projected bounding box
		{
			Vec2V points[8];

			for (int i = 0; i < 2; i++) { const ScalarV x = i ? bmax.GetX() : bmin.GetX();
			for (int j = 0; j < 2; j++) { const ScalarV y = j ? bmax.GetY() : bmin.GetY();
			for (int k = 0; k < 2; k++) { const ScalarV z = k ? bmax.GetZ() : bmin.GetZ();
			{
				Vec3V p = TransformProjective(fullCompositeMtx, Vec3V(x, y, z));

				p += p; // the matrix was scaling by 1/2 ..
				if(reflectionVP)
					p.SetX(ScalarV((float)w) - p.GetX()); // the matrix was also flipping in x ..

				points[i + j*2 + k*4] = p.GetXY();
			}}}}

			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 2; j++)
				{
					grcDebugDraw::Line(scale*points[i + j*2 + 0*4], scale*points[i + j*2 + 1*4], refBoxColour);
					grcDebugDraw::Line(scale*points[0 + i*2 + j*4], scale*points[1 + i*2 + j*4], refBoxColour);
					grcDebugDraw::Line(scale*points[j + 0*2 + i*4], scale*points[j + 1*2 + i*4], refBoxColour);
				}
			}
		}

		const Vec2V p00 = scale*Vec2V((float)i0, (float)j0);
		const Vec2V p10 = scale*Vec2V((float)i1, (float)j0);
		const Vec2V p01 = scale*Vec2V((float)i0, (float)j1);
		const Vec2V p11 = scale*Vec2V((float)i1, (float)j1);

		grcDebugDraw::Line(p00, p10, boundsColour);
		grcDebugDraw::Line(p10, p11, boundsColour);
		grcDebugDraw::Line(p11, p01, boundsColour);
		grcDebugDraw::Line(p01, p00, boundsColour);

		// display entity bounds and number of pixels touched
		{
			int numPixelsTouched = 0;

			for (int j = j0; j < j1; j++)
			{
				const u32* stencilRow = &stencil[j*stencilStride];

				for (int i = i0; i < i1; i++)
				{
					if (stencilRow[i/32] & BIT(31 - i%32))
					{
						const float inset = 0.25f;

						const Vec2V touched00 = scale*Vec2V((float)(i + 0) + inset, (float)(j + 0) + inset);
						const Vec2V touched10 = scale*Vec2V((float)(i + 1) - inset, (float)(j + 0) + inset);
						const Vec2V touched01 = scale*Vec2V((float)(i + 0) + inset, (float)(j + 1) - inset);
						const Vec2V touched11 = scale*Vec2V((float)(i + 1) - inset, (float)(j + 1) - inset);

						grcDebugDraw::Line(touched00, touched11, Color32(0,255,255,255));
						grcDebugDraw::Line(touched10, touched01, Color32(0,255,255,255));
						numPixelsTouched++;
					}
				}
			}

			grcDebugDraw::AddDebugOutput("selected entity touches %d of %d stencil pixels", numPixelsTouched, (i1 - i0)*(j1 - j0));
		}
	}
#endif // __BANK && !__SPU

	return bTouchesStencil;
}

bool fwScanEntities::IsEntityOccluded(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc)
{
#if SCAN_ENTITIES_DETAILED_STATS
	PF_FUNC(IsEntityOccluded);
#endif

#if ENABLE_SCAN_TIMERS
	fwScanLocalTimerFunc	scanTimer( &m_occlusionLocalTimer );
#endif

	const spdAABB	aabb = entityDesc.GetApproxBoundingBox();
	bool			occluded = false;

	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
	{
		occluded = TestOcclusion( aabb, cullShape.GetCameraPosition(), SCAN_OCCLUSION_STORAGE_PRIMARY, SCAN_OCCLUSION_TRANSFORM_PRIMARY BANK_ONLY(, entityDesc.GetEntityType()) );
#if __BANK
		if ( g_scanDebugFlags & SCAN_DEBUG_FLIP_OCCLUSION_RESULT ) { occluded = !occluded; }
#endif // __BANK
	}
	else if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS )
	{
		if ( Likely(g_scanMutableDebugFlags & SCAN_MUTABLE_DEBUG_ENABLE_SHADOW_OCCLUSION) ){
			Vec3V lightDirection = m_scanBaseInfo->m_cascadeShadowInfo.GetLightDirection();
			lightDirection = Normalize(lightDirection);
			occluded = TestOcclusion( aabb, lightDirection, SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS, SCAN_OCCLUSION_TRANSFORM_CASCADE_SHADOWS BANK_ONLY(, entityDesc.GetEntityType()));
			if (!occluded BANK_ONLY( && m_scanBaseInfo->m_useProjectedShadowOcclusion ) ){
				occluded = TestProjectedOcclusion( aabb, m_primaryCameraPosition);
			}
		}
#if __BANK
		if ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_FLIP_SHADOW_OCCLUSION_RESULT ) { occluded = !occluded; }
#endif // __BANK
	}
	else if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_ENABLE_WATER_OCCLUSION )
	{
		int transformIdx = SCAN_OCCLUSION_TRANSFORM_WATER_REFLECTION;
		bool reflectionVP = true;

		if ( Unlikely(entityDesc.GetRenderPhaseVisibilityMask().IsFlagSet(_GTA5_VIS_PHASE_MASK_WATER_REFLECTION_PROXY_PRE_REFLECTED)) )
		{
			transformIdx = SCAN_OCCLUSION_TRANSFORM_PRIMARY;
			reflectionVP = false;
		}
			
		const Vec4V* HiStencil = m_hiZBuffer[ SCAN_OCCLUSION_STORAGE_WATER_REFLECTION ] + RAST_HIZ_SIZE_VEC;
#if __BANK
		const bool bDebugSelected = ( m_scanBaseInfo->m_traceEntityAddress == entityDesc.GetEntity() ) && (g_scanDebugFlagsPortals & SCAN_DEBUG_USE_NEW_WATER_STENCIL_TEST_DEBUG);
#else
		const bool bDebugSelected = false;
#endif
		const Mat44V mat = m_scanBaseInfo->m_transform[ transformIdx ];

		occluded = !DoesEntityTouchWaterStencil( aabb.GetMin(), aabb.GetMax(), mat, 128, 72, (const u32*)HiStencil, reflectionVP, bDebugSelected);
		
#if __BANK
		if ( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_FLIP_WATER_OCCLUSION_RESULT ) { occluded = !occluded; }
#endif // __BANK
	}

	return occluded;
}

////////////////////////////////
// Entity methods: visibility //
////////////////////////////////

// TODO -- remove this? or refactor
/*fwTestResults_Out fwScanEntities::AreEntitiesClippedDefault(const fwRenderPhaseCullShape& cullShape, const fwEntityDescSoA& soa, fwTestResults_In excluded)
{
	fwTestResults	intersectResults;

	if ( (m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR) && (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SEPARATE_INTERIOR_SPHERE_GEOMETRY) )
		intersectResults = SoAUtils::AreEntitiesIntersectingSphere( soa, cullShape.GetInteriorSphere(), excluded );
	else
	{
		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_SPHERE )
			intersectResults = SoAUtils::AreEntitiesIntersectingSphere( soa, cullShape.GetSphere(), excluded );
		else if ( cullShape.GetFlags() & (CULL_SHAPE_FLAG_MIRROR|CULL_SHAPE_FLAG_WATER_REFLECTION) )
		{
			intersectResults = SoAUtils::AreEntitiesIntersectingPlaneSet8( soa, cullShape.GetFrustum(), excluded );

			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_WATER_REFLECTION_USE_SEPARATE_SPHERE )
				intersectResults = SoAUtils::And( intersectResults, SoAUtils::AreEntitiesIntersectingSphere( soa, cullShape.GetInteriorSphere(), excluded ) );
		}
		else
			intersectResults = SoAUtils::AreEntitiesIntersectingPlaneSet5( soa, cullShape.GetFrustum(), excluded );
	}

	return SoAUtils::Not( intersectResults );
}*/

fwTestResults_Out fwScanEntities::AreEntitiesClippedPrimary(const fwRenderPhaseCullShape& cullShape, const fwEntityDescSoA& soa, fwTestResults_In excluded)
{
	const int		storageId = SCAN_SCREENQUAD_STORAGE_PRIMARY;
	fwTestResults	visible(V_FALSE);

	if ( m_isFrustumValid[storageId][0] )
		visible = SoAUtils::AreEntitiesIntersectingPlaneSet5( soa, m_restrictedFrusta[storageId][0], excluded );

	if ( (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS) && m_isFrustumValid[storageId][1] )
		visible = SoAUtils::Or( visible, SoAUtils::AreEntitiesIntersectingPlaneSet5( soa, m_restrictedFrusta[storageId][1], excluded ) );

	return SoAUtils::Not( visible );
}

bool fwScanEntities::IsEntityClippedDefault(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc)
{
	bool			isClipped;
	const spdAABB	aabb = entityDesc.GetApproxBoundingBox();

	if ( (m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR) && (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SEPARATE_INTERIOR_SPHERE_GEOMETRY) )
	{
		Vec4V sphere = cullShape.GetInteriorSphere().GetV4();
		if (m_controlFlags & SCAN_ENTITIES_FLAG_PORTAL_TO_EXTERIOR)
		{
			const Vec4V exteriorSphere = cullShape.GetSphere().GetV4();
			sphere = Max(exteriorSphere, sphere); // assumes positions of spheres are the same and we're only affecting the radius
		}

		isClipped = !aabb.IntersectsSphere( spdSphere(sphere) );
	}
	else
	{
		const fwRenderPhaseCullShape* cs = &cullShape;

		if ( Unlikely(entityDesc.GetRenderPhaseVisibilityMask().IsFlagSet(_GTA5_VIS_PHASE_MASK_WATER_REFLECTION_PROXY_PRE_REFLECTED)) )
		{
			if ( (cullShape.GetFlags() & CULL_SHAPE_FLAG_WATER_REFLECTION) BANK_ONLY(&& (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_WATER_PROXY_PRE_REFLECTED_USE_GBUF_FRUSTUM)) )
				cs = m_gbufCullShape;
				//return !m_gbufCullShape->GetFrustum().IntersectsAABB( aabb );
		}

		isClipped = cs->IsClipped( aabb );

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_WATER_REFLECTION_USE_SEPARATE_SPHERE )
			isClipped = !( !isClipped && aabb.IntersectsSphere( cullShape.GetInteriorSphere() ) );
	}

	return isClipped;
}

bool fwScanEntities::IsEntityClippedScreenQuad(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, int storageId)
{
	if ( !(cullShape.GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) )
		return IsEntityClippedDefault( cullShape, entityDesc );

	const spdAABB	aabb = entityDesc.GetApproxBoundingBox();
	bool			isVisible = m_isFrustumValid[storageId][0] && m_restrictedFrusta[storageId][0].IntersectsAABB( aabb );

	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
		isVisible = isVisible || ( m_isFrustumValid[storageId][1] && m_restrictedFrusta[storageId][1].IntersectsAABB( aabb ) );

// 	if ( entityDesc.IsContainerLod() && aabb.ContainsPoint(cullShape.GetCameraPosition()) )
// 		isVisible = false;

	return !isVisible;
}

bool fwScanEntities::IsEntityClippedShadows(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags)
{
	const spdAABB	box = entityDesc.GetApproxBoundingBox();
	u8				shadowVisFlags = 0;
	bool			isVisible = true;

	isVisible = m_scanBaseInfo->m_cascadeShadowInfo.IsShadowVisible( box, entityDesc, &shadowVisFlags );

#if __BANK && !__SPU
	if (m_scanBaseInfo->m_cascadeShadowInfo.IsOrthographic())
	{
		if (isVisible)
		{
			visFlags.SetSubphaseVisFlags( 0x0f << SUBPHASE_CASCADE_0 );
			return false;
		}
		else
		{
			return true;
		}
	}
#endif // __BANK && !__SPU

	if (isVisible && m_scanBaseInfo->m_cascadeShadowInfo.CascadeLodDistanceCullingEnabled())
	{
		shadowVisFlags = m_scanBaseInfo->m_cascadeShadowInfo.IsLoddedInCascade(shadowVisFlags, float(entityDesc.GetLodDistCached() * m_scanBaseInfo->m_perLodLevelScales[entityDesc.GetLodType()]));
	}

	isVisible = isVisible && (shadowVisFlags != 0);

	if ( isVisible )
	{
		if (m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR)
		{
			u16 visMaskFlags = entityDesc.GetRenderPhaseVisibilityMask().GetAllFlags();

			// if not a shadow proxy
			if (!(visMaskFlags & _GTA5_VIS_PHASE_MASK_SHADOW_PROXY))
			{
				const int	storageId = SCAN_SCREENQUAD_STORAGE_CASCADE_SHADOWS;
				isVisible = m_isFrustumValid[storageId][0] && m_restrictedFrusta[storageId][0].IntersectsAABB( box );

				if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
					isVisible = isVisible || ( m_isFrustumValid[storageId][1] && m_restrictedFrusta[storageId][1].IntersectsAABB( box ) );
			}
		}
		else
		{
			const bool		visibleInAllRoomsFrustum = m_visibleRoomsShadowFrustum.IntersectsAABB( box );
			bool			visibleInRestrictedFrustum = false;

			if ( m_scanResults->GetIsExteriorVisibleInGbuf() )
			{
				spdTransposedPlaneSet4*		restrictedPlanes = (spdTransposedPlaneSet4*) m_scanResults->GetExteriorShadowFrustumPlanesPtr();

				visibleInRestrictedFrustum = true;
				for (int i = 0; i * 4 < CASCADE_SHADOWS_SCAN_NUM_PLANES; ++i)
					visibleInRestrictedFrustum = visibleInRestrictedFrustum && restrictedPlanes[i].IntersectsAABB( box );
			}

			isVisible = visibleInAllRoomsFrustum || visibleInRestrictedFrustum;
		}
	}

	visFlags.SetSubphaseVisFlags( shadowVisFlags << SUBPHASE_CASCADE_0 );
	return !isVisible;
}

bool fwScanEntities::IsEntityClippedReflections(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags)
{
	const bool isSlod3 = (entityDesc.GetLodType() == LODTYPES_DEPTH_SLOD3);
	if (isSlod3)
	{
		const spdSphere	sphere = entityDesc.GetApproxBoundingSphere();

		if (!sphere.IntersectsSphere(cullShape.GetSphere()))
		{
			return true;
		}
	}
	else
	{
		if ( IsEntityClippedDefault( cullShape, entityDesc ) )
			return true;
	}
#if ENABLE_REFLECTION_PER_FACET_CULLING
	u32 facetBits = 0;

#if __BANK
	CompileTimeAssert(SCAN_DEBUG_EXT_ENABLE_REFLECTION_FACET0_BIT > SUBPHASE_REFLECT_FACET0);
	u32 scanDebugFacetFlags = g_scanDebugFlagsExtended >> (SCAN_DEBUG_EXT_ENABLE_REFLECTION_FACET0_BIT - SUBPHASE_REFLECT_FACET0);
	if (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_ENABLE_REFLECTION_FACET_CULLING)
#endif
	{
		facetBits |= u32(m_reflectionPlaneSets[0].IntersectsAABB(entityDesc.GetApproxBoundingBox())) << SUBPHASE_REFLECT_FACET0;
		facetBits |= u32(m_reflectionPlaneSets[1].IntersectsAABB(entityDesc.GetApproxBoundingBox())) << SUBPHASE_REFLECT_FACET1;
		facetBits |= u32(m_reflectionPlaneSets[2].IntersectsAABB(entityDesc.GetApproxBoundingBox())) << SUBPHASE_REFLECT_FACET2;
		facetBits |= u32(m_reflectionPlaneSets[3].IntersectsAABB(entityDesc.GetApproxBoundingBox())) << SUBPHASE_REFLECT_FACET3;
		facetBits |= u32(m_reflectionPlaneSets[4].IntersectsAABB(entityDesc.GetApproxBoundingBox())) << SUBPHASE_REFLECT_FACET4;
		facetBits |= u32(m_reflectionPlaneSets[5].IntersectsAABB(entityDesc.GetApproxBoundingBox())) << SUBPHASE_REFLECT_FACET5;
#if __BANK
		facetBits &=scanDebugFacetFlags;
#endif
	}
#if __BANK
	else
	{
		facetBits = SUBPHASE_REFLECT_FACET_MASK;
	}
#endif
	visFlags.SetSubphaseVisFlags( facetBits, SUBPHASE_REFLECT_FACET_MASK );
#else
	const spdAABB	box = entityDesc.GetApproxBoundingBox();
	const ScalarV	z = cullShape.GetSphere().GetCenter().GetZ();
	const bool		inUpperHemi = IsLessThanAll( z, box.GetMax().GetZ() ) ? true : false;
	const bool		inLowerHemi = IsGreaterThanAll( z, box.GetMin().GetZ() ) ? true : false;
	u32				subphaseMask = 0;

	if ( inUpperHemi )
		subphaseMask |= isSlod3 ? BIT(SUBPHASE_REFLECT_UPPER_LOLOD) : BIT(SUBPHASE_REFLECT_UPPER_HILOD);

	if ( inLowerHemi )
		subphaseMask |= isSlod3 ? BIT(SUBPHASE_REFLECT_LOWER_LOLOD) : BIT(SUBPHASE_REFLECT_LOWER_HILOD);
	
	// subphase vis flags already contain cascade shadow bits, so we need to bitwise-OR in the reflection bits
	visFlags.SetSubphaseVisFlags( subphaseMask, SUBPHASE_REFLECT_MASK );
#endif
	return false; // visible
}

bool fwScanEntities::IsEntityClipped(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags)
{
	fwScanLocalTimerFunc	scanTimer( &m_clippingLocalTimer );
	
	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
		return IsEntityClippedScreenQuad( cullShape, entityDesc, SCAN_SCREENQUAD_STORAGE_PRIMARY );
	else if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS )
		return IsEntityClippedShadows( cullShape, entityDesc, visFlags );
	else if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS )
		return IsEntityClippedReflections( cullShape, entityDesc, visFlags );
	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_MIRROR )
		return IsEntityClippedScreenQuad( cullShape, entityDesc, SCAN_SCREENQUAD_STORAGE_MIRROR );
	else
		return IsEntityClippedDefault( cullShape, entityDesc );
}

// Method gathering all the visibility stages for entities
SCAN_ENTITIES_FORCE_INLINE s32 fwScanEntities::IsEntityVisible(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& visFlags)
{
	s32 resultFlags = FLAG_ISVISIBLE_RESULT_VISIBLE;

#if SCAN_ENTITIES_DETAILED_STATS
	PF_FUNC(IsEntityVisible);
#endif

#if __BANK
	if ( g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_CLIPPED )
		visFlags.SetSubphaseVisFlags(0xff); // if we're bypassing IsEntityClipped, we need to force all subphase flags to 1
#endif // __BANK

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_IN_RENDERPHASE) ) )
		if ( IsEntityExcludedInRenderPhase( cullShape, entityDesc ) )
			return 0;

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_CLIPPED) ) )
		if ( IsEntityClipped( cullShape, entityDesc, visFlags ) )
			return 0;

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_BY_LOD_RANGE) ) )
	{
		u8 lodMask = cullShape.GetLodMask();
		if (lodMask == rage::fwRenderPhase::LODMASK_NONE || !(entityDesc.GetEntityType() == _GTA5_ENTITY_TYPE_LIGHT && (lodMask & rage::fwRenderPhase::LODMASK_LIGHT)))
		{
			bool	lodRangesEnabled = false;

			if ( m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR )
				lodRangesEnabled = (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_LOD_RANGES_IN_INTERIOR) ? true : false;
			else
				lodRangesEnabled = (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_LOD_RANGES_IN_EXTERIOR) ? true : false;

			if ( lodRangesEnabled && IsEntityExcludedByLodRange( cullShape, entityDesc ) )
				return 0;
		}
	}

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_BY_PED_VEH_OBJ_DISTANCE) ) )
	{
		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_MAX_PED_VEH_OBJ_DISTANCES )
		{
			if ( entityDesc.GetUseMaxDistanceForWaterReflection() )
			{
				// TODO -- should we use the cached LOD distance in the entity desc instead of this?
				const float distSqr = MagSquared( entityDesc.GetApproxCentre() - cullShape.GetCameraPosition() ).Getf();

				if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_PED )
				{
					if ( distSqr > cullShape.GetMaxPedDistanceSqr() )
						return 0;
				}
				else if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_VEHICLE )
				{
					if ( distSqr > cullShape.GetMaxVehicleDistanceSqr() )
						return 0;
				}
				else // if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_OBJECT )
				{
					if ( distSqr > cullShape.GetMaxObjectDistanceSqr() )
						return 0;
				}
			}
		}
	}

	{
		bool	occlusionEnabled = true;

		occlusionEnabled = occlusionEnabled && (g_scanMutableDebugFlags & SCAN_MUTABLE_DEBUG_ENABLE_OCCLUSION);
		occlusionEnabled = occlusionEnabled && !(m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR);
		occlusionEnabled = occlusionEnabled && ( (cullShape.GetFlags() & (CULL_SHAPE_FLAG_ENABLE_OCCLUSION|CULL_SHAPE_FLAG_ENABLE_WATER_OCCLUSION)) );

		if ( occlusionEnabled && IsEntityOccluded( cullShape, entityDesc ) )
			resultFlags |= FLAG_ISVISIBLE_RESULT_OCCLUDED;
	}

	return resultFlags;
}

SCAN_ENTITIES_FORCE_INLINE s32 fwScanEntities::IsEntityVisibleNoClipping(const fwRenderPhaseCullShape& cullShape, const fwEntityDesc& entityDesc, fwVisibilityFlags& BANK_ONLY(visFlags))
{
	s32 resultFlags = FLAG_ISVISIBLE_RESULT_VISIBLE;

#if __BANK
	if ( g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_CLIPPED )
		visFlags.SetSubphaseVisFlags(0xff); // if we're bypassing IsEntityClipped, we need to force all subphase flags to 1
#endif // __BANK

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_IN_RENDERPHASE) ) )
		if ( IsEntityExcludedInRenderPhase( cullShape, entityDesc ) )
			return 0;

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_BY_LOD_RANGE) ) )
	{
		bool	lodRangesEnabled = false;

		if ( m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR )
			lodRangesEnabled = (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_LOD_RANGES_IN_INTERIOR) ? true : false;
		else
			lodRangesEnabled = (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_LOD_RANGES_IN_EXTERIOR) ? true : false;

		if ( lodRangesEnabled && IsEntityExcludedByLodRange( cullShape, entityDesc ) )
			return 0;
	}

	BANK_ONLY( if ( !(g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_EXCLUDED_BY_PED_VEH_OBJ_DISTANCE) ) )
	{
		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_MAX_PED_VEH_OBJ_DISTANCES )
		{
			if ( entityDesc.GetUseMaxDistanceForWaterReflection() )
			{
				// TODO -- should we use the cached LOD distance in the entity desc instead of this?
				const float distSqr = MagSquared( entityDesc.GetApproxCentre() - cullShape.GetCameraPosition() ).Getf();

				if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_PED )
				{
					if ( distSqr > cullShape.GetMaxPedDistanceSqr() )
						return 0;
				}
				else if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_VEHICLE )
				{
					if ( distSqr > cullShape.GetMaxVehicleDistanceSqr() )
						return 0;
				}
				else // if ( entityDesc.GetVisibilityType() == _GTA5_VIS_ENTITY_OBJECT )
				{
					if ( distSqr > cullShape.GetMaxObjectDistanceSqr() )
						return 0;
				}
			}
		}
	}

	{
		bool	occlusionEnabled = true;

		occlusionEnabled = occlusionEnabled && (g_scanMutableDebugFlags & SCAN_MUTABLE_DEBUG_ENABLE_OCCLUSION);
		occlusionEnabled = occlusionEnabled && !(m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR);
		occlusionEnabled = occlusionEnabled && ( (cullShape.GetFlags() & (CULL_SHAPE_FLAG_ENABLE_OCCLUSION|CULL_SHAPE_FLAG_ENABLE_WATER_OCCLUSION)) );

		if ( occlusionEnabled && IsEntityOccluded( cullShape, entityDesc ) )
			resultFlags |= FLAG_ISVISIBLE_RESULT_OCCLUDED;
	}

	return resultFlags;
}

//////////////////////////
// Entity methods: main //
//////////////////////////

void fwScanEntities::ProcessEntityDescs()
{
#if SCAN_ENTITIES_DETAILED_STATS
	PF_FUNC(ProcessEntityDescs);
#endif

	fwVisibilityFlags visFlags;

	for (u32 e = 0; e < m_entityDescCount; ++e)
	{
		const fwEntityDesc&		entityDesc = m_entityDescs[e];

		if ( entityDesc.GetEntity() == NULL )
			continue;

#if __BANK
		// trace selected entity
		if ( (g_scanDebugFlags & SCAN_DEBUG_TRACE_SELECTED_ENTITY) && m_scanBaseInfo->m_traceEntityAddress == entityDesc.GetEntity() )
			__debugbreak();
#endif // __BANK

		float		fDist;
		const bool	isRejectedDefault = IsEntityEarlyRejected( entityDesc, m_primaryCameraPosition, &fDist, true );

		bool				visibleInAnyPhase = false;
		bool				streamableInAnyPhase = false;

		visFlags.Clear();
		if ( m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR )
			visFlags.SetOptionBits( VIS_FLAG_OPTION_INTERIOR );

		for (u32 r=0; r<m_cullShapeCount; r++)
		{
			const fwRenderPhaseCullShape&	cullShape = *(m_cullShapes[r]);

			// check if we have to do a custom early rejection pass for this entity
			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_ONLY_STREAM_ENTITIES )
			{
				if ( IsEntityEarlyRejected( entityDesc, cullShape.GetCameraPosition(), NULL, false ) )
					continue;
			}
			else
			{
				if ( isRejectedDefault )
					continue;
			}

			s32 visResultFlags = IsEntityVisible(cullShape, entityDesc, visFlags);

			//////////////////////////////////////////////////////////////////////////
			if ((visResultFlags & FLAG_ISVISIBLE_RESULT_VISIBLE) != 0)
			{
				if ((visResultFlags & FLAG_ISVISIBLE_RESULT_OCCLUDED) == 0)
				{
					// not occluded
					visFlags.SetVisBit( cullShape.GetRenderPhaseId() );

					if ( !(cullShape.GetFlags() & CULL_SHAPE_FLAG_ONLY_STREAM_ENTITIES) )
						visibleInAnyPhase = true;

#if __BANK
					if ( ( g_scanDebugFlags & SCAN_DEBUG_CULL_TO_ENTITY_SELECT_QUAD ) && ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY ) )
					{
						spdAABB		box = entityDesc.GetApproxBoundingBox();
						if ( m_entitySelectFrustum.IntersectsAABB( box ) )
							visFlags.SetVisBit( m_scanBaseInfo->m_entitySelectRenderPhaseId );
					}
#endif	//__bank
				}

				if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_STREAM_ENTITIES )
					streamableInAnyPhase = true;
			}
			//////////////////////////////////////////////////////////////////////////
			
		}

		if ( !visibleInAnyPhase && !streamableInAnyPhase )
			continue;

		if ( streamableInAnyPhase )
			visFlags.SetOptionBits( VIS_FLAG_STREAM_ENTITY );

		if ( !visibleInAnyPhase )
			visFlags.SetOptionBits( VIS_FLAG_ONLY_STREAM_ENTITY );

		if ( !(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_IGNORE_RENDER_LOD_ONLY_FLAG) )
		{
			if ( visFlags.GetVisBit( m_gbufCullShape->GetRenderPhaseId() ) && (m_controlFlags & SCAN_ENTITIES_RENDER_LODS_ONLY) )
				visFlags.SetOptionBits( VIS_FLAG_RENDERED_THROUGH_LODS_ONLY_PORTAL );
		}
		visFlags.SetOptionBits(entityDesc.GetIsPedInVehicle() ? VIS_FLAG_ENTITY_IS_PED_INSIDE_VEHICLE : 0);
		visFlags.SetOptionBits(entityDesc.IsHDTexCapable() ? VIS_FLAG_ENTITY_IS_HD_TEX_CAPABLE : 0);
		visFlags.SetOptionBits(entityDesc.IsSetNeverDummy() ? VIS_FLAG_ENTITY_NEVER_DUMMY : 0);

		if ( (visFlags & m_scanBaseInfo->m_streamingVolumeMask)!=0 || ((1 << entityDesc.GetLodType()) & m_scanBaseInfo->m_lodFilterMask)!=0 )
		{
			AddPvsEntry(entityDesc, fDist, visFlags);
		}
	}
}

void fwScanEntities::ProcessSingleEntityDescSoa(const int block, const int offset, const fwEntityDesc& entityDesc, VecBoolV_In resultOrMask, fwTestResults_In defaultRejected, fwTestResults** phaseRejected, fwTestResults_In primaryClipped)
{
	if ( entityDesc.GetEntity() == NULL )
		return;

	const int index = (block << 2) + offset;

#if __BANK
	const u32 debugFlags =
		SCAN_DEBUG_TRACE_SELECTED_ENTITY |
		SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_SELECTED |
		SCAN_DEBUG_EARLY_REJECT_SELECTED |
		SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_LISTED | 
		SCAN_DEBUG_EARLY_REJECT_LISTED;

	// normally none of these flags are set, so let's only do this work if we need to
	if (g_scanDebugFlags & debugFlags)
	{
		// trace selected entity
		if ( (g_scanDebugFlags & SCAN_DEBUG_TRACE_SELECTED_ENTITY) && m_scanBaseInfo->m_traceEntityAddress == entityDesc.GetEntity() )
		{
			__debugbreak();
		}

		if ( (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_SELECTED) && m_scanBaseInfo->m_traceEntityAddress != entityDesc.GetEntity() )
		{
			return;
		}

		if ( (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_SELECTED) && m_scanBaseInfo->m_traceEntityAddress == entityDesc.GetEntity() )
		{
			return;
		}

		if (g_scanDebugFlags & (SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_LISTED | SCAN_DEBUG_EARLY_REJECT_LISTED))
		{
			const fwEntity* ep = entityDesc.GetEntity();
			bool bListed = false;

			for (int i = 0; i < m_scanBaseInfo->m_traceEntityAddressCount; i++)
			{
				if (m_scanBaseInfo->m_traceEntityAddressList[i] == ep)
				{
					bListed = true;
					break;
				}
			}

			// debug cull case #1 - reject all except listed entities
			if (!bListed && (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_ALL_EXCEPT_LISTED))
			{
				return;
			}
			// debug cull case #2 - reject listed entities
			else if (bListed && (g_scanDebugFlags & SCAN_DEBUG_EARLY_REJECT_LISTED))
			{
				return;
			}
		}
	}
#endif // __BANK

	fwVisibilityFlags	visFlags;
	bool				visibleInAnyPhase = false;
	bool				streamableInAnyPhase = false;

	visFlags.Clear();
	if ( m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR )
		visFlags.SetOptionBits( VIS_FLAG_OPTION_INTERIOR );

	for (u32 r=0; r<m_cullShapeCount; r++)
	{
		const fwRenderPhaseCullShape&	cullShape = *(m_cullShapes[r]);

		// check if we have to do a custom early rejection pass for this entity
		{
			const VecBoolV	blockRejected = phaseRejected[r] ? SoAUtils::GetResult( *phaseRejected[r], block ) : SoAUtils::GetResult( defaultRejected, block );
			const bool		rejected = IsTrueAll( Or(blockRejected, resultOrMask) );

			if ( rejected )
				continue;
		}

		s32 visResultFlags = 0;

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
		{
			const VecBoolV	blockClipped = SoAUtils::GetResult( primaryClipped, block );
			const bool		clipped = IsTrueAll( Or(blockClipped, resultOrMask) );
			
			if ( !clipped BANK_ONLY( || (g_scanDebugFlags & SCAN_DEBUG_VISIBILITY_BYPASS_CLIPPED) ) )
				visResultFlags = IsEntityVisibleNoClipping(cullShape, entityDesc, visFlags);
		}
		else
			visResultFlags = IsEntityVisible(cullShape, entityDesc, visFlags);


		//////////////////////////////////////////////////////////////////////////
		if ((visResultFlags & FLAG_ISVISIBLE_RESULT_VISIBLE) != 0)
		{
			if ((visResultFlags & FLAG_ISVISIBLE_RESULT_OCCLUDED) == 0)
			{
				// not occluded
				visFlags.SetVisBit( cullShape.GetRenderPhaseId() );

				if ( !(cullShape.GetFlags() & CULL_SHAPE_FLAG_ONLY_STREAM_ENTITIES) )
					visibleInAnyPhase = true;

#if __BANK
				if ( ( g_scanDebugFlags & SCAN_DEBUG_CULL_TO_ENTITY_SELECT_QUAD ) && ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY ) )
				{
					spdAABB		box = entityDesc.GetApproxBoundingBox();
					if ( m_entitySelectFrustum.IntersectsAABB( box ) )
						visFlags.SetVisBit( m_scanBaseInfo->m_entitySelectRenderPhaseId );
				}
#endif
			}

			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_STREAM_ENTITIES )
				streamableInAnyPhase = true;
		}
		//////////////////////////////////////////////////////////////////////////



		
	}

	if ( !visibleInAnyPhase && !streamableInAnyPhase )
		return;

	if ( streamableInAnyPhase )
		visFlags.SetOptionBits( VIS_FLAG_STREAM_ENTITY );

	if ( !visibleInAnyPhase )
		visFlags.SetOptionBits( VIS_FLAG_ONLY_STREAM_ENTITY );

	if ( !(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_IGNORE_RENDER_LOD_ONLY_FLAG) )
	{
		if ( visFlags.GetVisBit( m_gbufCullShape->GetRenderPhaseId() ) && (m_controlFlags & SCAN_ENTITIES_RENDER_LODS_ONLY) )
			visFlags.SetOptionBits( VIS_FLAG_RENDERED_THROUGH_LODS_ONLY_PORTAL );
	}

	visFlags.SetOptionBits(entityDesc.GetIsPedInVehicle() ? VIS_FLAG_ENTITY_IS_PED_INSIDE_VEHICLE : 0);
	visFlags.SetOptionBits(entityDesc.IsHDTexCapable() ? VIS_FLAG_ENTITY_IS_HD_TEX_CAPABLE : 0);
	visFlags.SetOptionBits(entityDesc.IsSetNeverDummy() ? VIS_FLAG_ENTITY_NEVER_DUMMY : 0);

	if ( (visFlags & m_scanBaseInfo->m_streamingVolumeMask)!=0 || ((1 << entityDesc.GetLodType()) & m_scanBaseInfo->m_lodFilterMask)!=0 )
	{
		AddPvsEntry(entityDesc, reinterpret_cast< float* >(m_cameraDistances)[index], visFlags);
	}
}
#if __BANK
inline bool fwScanEntities::ScanDebugDontEarlyReject(fwEntityDesc &entity)
{
	return (g_scanDebugFlags & SCAN_DEBUG_DONT_EARLY_REJECT_ANY) != 0 ||
		   ((g_scanDebugFlags & SCAN_DEBUG_DONT_EARLY_REJECT_SELECTED) && m_scanBaseInfo->m_traceEntityAddress == entity.GetEntity());
}
#endif

void fwScanEntities::ProcessEntityDescsSoA()
{
#if SCAN_ENTITIES_DETAILED_STATS
	PF_FUNC(ProcessEntityDescsSoA);
#endif
	fwEntityDescSoA		soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_entityDescs, m_entityDescCount );
	fwTestResults		defaultRejected;
	fwTestResults*		phaseRejected[ SCAN_RENDER_PHASE_COUNT ];
	fwTestResults		rejectedInAll(V_TRUE);
	fwTestResults		primaryClipped;

	//---------- Early rejection ----------//

	m_cameraDistances = m_scratchAllocator.Alloc< Vec4V >( SCAN_MAX_ENTITIES_PER_JOB >> 2, 16 );
		
	for (int p = 0; p < static_cast< int >(m_cullShapeCount); ++p)
	{
		const fwRenderPhaseCullShape&	cullShape = *(m_cullShapes[p]);

		phaseRejected[p] = NULL;
		if ( (cullShape.GetFlags() & CULL_SHAPE_FLAG_STREAM_ENTITIES) && !(cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY) )
		{
			phaseRejected[p] = m_scratchAllocator.Alloc< fwTestResults >( 16 );
			// 8/15/12 - cthomas - Pass in NULL here instead of the camera distance - saves a ton of calls to 
			// sqrt() - one for each render phase/"cull shape" - these camera positions will just be over-ridden 
			// below by the call to AreEntitiesEarlyRejected() for the "default rejected" case.
			*phaseRejected[p] = AreEntitiesEarlyRejected<false>( soa, cullShape.GetCameraPosition(), NULL, false );
			rejectedInAll = SoAUtils::And( rejectedInAll, *phaseRejected[p] );
		}
	}

	defaultRejected = AreEntitiesEarlyRejected<true>( soa, m_primaryCameraPosition, m_cameraDistances, true );
	rejectedInAll = SoAUtils::And( rejectedInAll, defaultRejected );

	//---------- Primary clipping ----------//

	primaryClipped = AreEntitiesClippedPrimary( *m_gbufCullShape, soa, defaultRejected );

	//---------- Entities iterations ----------//

#if __BANK
	bool dontRejectAny    = (g_scanDebugFlags & SCAN_DEBUG_DONT_EARLY_REJECT_ANY) != 0;
	bool dontRejectSelect = (g_scanDebugFlags & SCAN_DEBUG_DONT_EARLY_REJECT_SELECTED) != 0;

	if (dontRejectAny || dontRejectSelect)
	{
		for (int b = 0; b < soa.m_blockCount; ++b)
		{
			const VecBoolV		blockRejectedInAll = SoAUtils::GetResult( rejectedInAll, b );

			fwEntityDesc		block[4];
			fwSoAEntityContainer::GetEntityDescBlock( soa, b, block[0], block[1], block[2], block[3] );
	
			if (ScanDebugDontEarlyReject(block[0]) || IsFalse(blockRejectedInAll.GetX())) 
			{
				ProcessSingleEntityDescSoa( b, 0, block[0], VecBoolV(V_F_T_T_T), defaultRejected, phaseRejected, primaryClipped );
			}
			if (ScanDebugDontEarlyReject(block[1]) || IsFalse(blockRejectedInAll.GetY())) 
			{
				ProcessSingleEntityDescSoa( b, 1, block[1], VecBoolV(V_T_F_T_T), defaultRejected, phaseRejected, primaryClipped );
			}
			if (ScanDebugDontEarlyReject(block[2]) || IsFalse(blockRejectedInAll.GetZ())) 
			{
				ProcessSingleEntityDescSoa( b, 2, block[2], VecBoolV(V_T_T_F_T), defaultRejected, phaseRejected, primaryClipped );
			}
			if (ScanDebugDontEarlyReject(block[3]) || IsFalse(blockRejectedInAll.GetW())) 
			{
				ProcessSingleEntityDescSoa( b, 3, block[3], VecBoolV(V_T_T_T_F), defaultRejected, phaseRejected, primaryClipped );
			}
		}
	}
	else
	{
#endif
	for (int b = 0; b < soa.m_blockCount; ++b)
	{
		const VecBoolV		blockRejectedInAll = SoAUtils::GetResult( rejectedInAll, b );

		if ( IsTrueAll( blockRejectedInAll ) )
			continue;

		fwEntityDesc		block[4];
		fwSoAEntityContainer::GetEntityDescBlock( soa, b, block[0], block[1], block[2], block[3] );
		
		if ( IsFalse( blockRejectedInAll.GetX() ) )		ProcessSingleEntityDescSoa( b, 0, block[0], VecBoolV(V_F_T_T_T), defaultRejected, phaseRejected, primaryClipped );
		if ( IsFalse( blockRejectedInAll.GetY() ) )		ProcessSingleEntityDescSoa( b, 1, block[1], VecBoolV(V_T_F_T_T), defaultRejected, phaseRejected, primaryClipped );
		if ( IsFalse( blockRejectedInAll.GetZ() ) )		ProcessSingleEntityDescSoa( b, 2, block[2], VecBoolV(V_T_T_F_T), defaultRejected, phaseRejected, primaryClipped );
		if ( IsFalse( blockRejectedInAll.GetW() ) )		ProcessSingleEntityDescSoa( b, 3, block[3], VecBoolV(V_T_T_T_F), defaultRejected, phaseRejected, primaryClipped );
	}
#if __BANK
	}
#endif
}

bool fwScanEntities::Run()
{
	fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_scanEntitiesTimer) );

	// Step scratch allocator
	
	m_scratchAllocator.Init( m_scratchMemory, fwScanEntities::SCRATCH_BUFFER_SIZE );

	// Get shortcuts to useful render phases

	m_gbufCullShape = NULL;
	m_cascadeShadowCullShape = NULL;

	m_cullShapeCount = 0;

	for (int i = 0; i < static_cast< int >( m_scanBaseInfo->m_cullShapeCount ); ++i)
	{
		const fwRenderPhaseCullShape&	cullShape = m_scanBaseInfo->m_cullShapes[i];
		const u32 renderPhaseMask = 1 << cullShape.GetRenderPhaseId();

		if ( (cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS) && m_scanResults->IsEverythingInShadow() )
		{
			// If IsEverythingInShadow is true, then Cascade Shadows isn't visible. 
			// Skip the Cascade Shadows render phase.
			continue;
		}

		if( (cullShape.GetFlags() & CULL_SHAPE_FLAG_HEIGHT_MAP) && !m_scanResults->GetIsExteriorVisibleInGbuf() )
		{
			// If the exterior is not visible than there is not point in processing the entitys
			// Note that when underwater we don't care about the map, so diving in underwater interiors will work fine
			continue;
		}

		BANK_ONLY(const bool bBypassNodeVisTagsTest = (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_BYPASS_NODE_VIS_TAGS_TEST) != 0;)
		if ( (m_nodeVisTags & renderPhaseMask) BANK_ONLY(|| bBypassNodeVisTagsTest) )
		{
			m_cullShapes[m_cullShapeCount] = &cullShape;
			m_cullShapeCount++;
		}
		
		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
			m_gbufCullShape = &cullShape;

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS )
			m_cascadeShadowCullShape = &cullShape;
	}

	m_primaryCameraPosition = m_gbufCullShape->GetCameraPosition();

	// Set the restricted frusta

	for (int i = 0; i < SCAN_SCREENQUAD_STORAGE_COUNT; ++i)
	{
		m_isFrustumValid[i][0] = false;
		m_isFrustumValid[i][1] = false;
	}

	for (int i = 0; i < static_cast< int >( m_cullShapeCount ); ++i)
	{
		const fwRenderPhaseCullShape&	cullShape = *(m_cullShapes[i]);
		
		if ( !(cullShape.GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) )
			continue;

		const s16					storageId = cullShape.GetScreenQuadStorageId();
		const Vec4V&				screenQuadData = m_screenQuadStorage[ storageId ];
		const Mat44V&				compositeMatrix = cullShape.GetCompositeMatrix();
		
		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
		{
			fwScreenQuad quad0;
			fwScreenQuad quad1;

			bool mirrorPlaneFlip = (cullShape.GetFlags() & CULL_SHAPE_FLAG_MIRROR) != 0;
			if (m_controlFlags & SCAN_ENTITIES_FLAG_STRADDLING_PORTALS_CONTAINER)
			{
				quad0 = fwScreenQuad(SCREEN_QUAD_FULLSCREEN);
				quad1 = fwScreenQuad(SCREEN_QUAD_INVALID);
			}
			else
			{
				const fwScreenQuadPair	screenQuadPair( screenQuadData );
				quad0 = screenQuadPair.GetScreenQuad0();
				quad1 = screenQuadPair.GetScreenQuad1();
			}

			if ( !quad0.IsValid() && quad1.IsValid() )
				std::swap( quad0, quad1 );

			if ( quad0.IsValid() )
			{
				m_isFrustumValid[ storageId ][0] = true;
				quad0.GenerateFrustum( compositeMatrix, m_restrictedFrusta[ storageId ][0], mirrorPlaneFlip );
			}

			if ( quad1.IsValid() )
			{
				m_isFrustumValid[ storageId ][1] = true;
				quad1.GenerateFrustum( compositeMatrix, m_restrictedFrusta[ storageId ][1], mirrorPlaneFlip );
			}
		}
		else
		{
			fwScreenQuad screenQuad;
			if (m_controlFlags & SCAN_ENTITIES_FLAG_STRADDLING_PORTALS_CONTAINER)
			{
				screenQuad = fwScreenQuad(SCREEN_QUAD_FULLSCREEN);
			}
			else
			{
				screenQuad = fwScreenQuad(screenQuadData);
			}
			if ( screenQuad.IsValid() )
			{
				m_isFrustumValid[ storageId ][0] = true;
				screenQuad.GenerateFrustum( compositeMatrix, m_restrictedFrusta[ storageId ][0] );
			}
		}
	}

	// Setup the visible rooms cumulative shadow screen

	if ( !(m_controlFlags & SCAN_ENTITIES_FLAG_INTERIOR) && m_cascadeShadowCullShape )
		m_scanResults->GetVisibleRoomsShadowScreenQuad().GenerateFrustum( m_cascadeShadowCullShape->GetCompositeMatrix(), m_visibleRoomsShadowFrustum );

	// Set the entity select restricted frustum

#if __BANK
	if ( g_scanDebugFlags & SCAN_DEBUG_CULL_TO_ENTITY_SELECT_QUAD )
		m_scanBaseInfo->m_entitySelectQuad.GenerateFrustum( m_gbufCullShape->GetCompositeMatrix(), m_entitySelectFrustum );
#endif

	// Perform visibility for entities

	ResetScanEntries();

	if ( m_controlFlags & SCAN_ENTITIES_FLAG_ENTITY_DESCS_SOA )
		ProcessEntityDescsSoA();
	else
		ProcessEntityDescs();

	SubmitScanEntries();

	m_earlyRejectLocalTimer.UpdateScanTimer( PPU_SYMBOL(g_scanEntitiesEarlyRejectTimer) );
	m_clippingLocalTimer.UpdateScanTimer( PPU_SYMBOL(g_scanEntitiesClippingTimer) );
	m_occlusionLocalTimer.UpdateScanTimer( PPU_SYMBOL(g_scanEntitiesOcclusionTimer) );

	return true;
}

} // namespace rage

#endif // !__TOOL
