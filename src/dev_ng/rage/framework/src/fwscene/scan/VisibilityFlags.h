/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/scan/VisibilityFlags.h
// PURPOSE : describes the visibility (per scan phase) of an entity
// AUTHOR :  Ian Kiigan
// CREATED : 25/08/10
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef INC_VISIBILITYFLAGS_H_
#define INC_VISIBILITYFLAGS_H_

#include "fwscene/lod/LodTypes.h"

namespace rage {

enum {
	VIS_FLAG_OPTION_INTERIOR					= 1 << 0,
	VIS_FLAG_STREAM_ENTITY						= 1 << 1,
	VIS_FLAG_ONLY_STREAM_ENTITY					= 1 << 2,
	VIS_FLAG_RENDERED_THROUGH_LODS_ONLY_PORTAL	= 1 << 3,
	VIS_FLAG_ENTITY_IS_PED_INSIDE_VEHICLE		= 1 << 4,
	VIS_FLAG_ENTITY_IS_HD_TEX_CAPABLE		    = 1 << 5,
	VIS_FLAG_ENTITY_NEVER_DUMMY					= 1 << 6
};

class fwVisibilityFlags
{
public:
	enum 
	{ 
		MAX_ALPHA_VISIBILITY_BITS    =  8,
		MAX_PHASE_VISIBILITY_BITS    = 24,
		MAX_SUBPHASE_VISIBILITY_BITS = 10,
		MAX_OPTION_BITS				 = 14,
		MAX_LODTYPES_SORTVAL_BITS    =  LODTYPES_SORTVAL_BITS,
		MAX_ENTITY_TYPE_BITS		 =  4,
		MAX_TBD_BITS				 =  1
	};

	CompileTimeAssert( MAX_ALPHA_VISIBILITY_BITS + MAX_PHASE_VISIBILITY_BITS == 32 );

	inline void		Clear()										{ m_nPhaseVisFlags = m_nSubphaseVisFlags = m_nOptionFlags = 0; }

	inline void		ClearVisFlags()								{ m_nPhaseVisFlags = 0; }
	inline void		SetAllVisFlags()							{ m_nPhaseVisFlags = (1<<MAX_PHASE_VISIBILITY_BITS) - 1; }
	inline void		SetPhaseVisFlags(u32 nFlags)				{ FastAssert( nFlags < (1<<MAX_PHASE_VISIBILITY_BITS) ); m_nPhaseVisFlags = nFlags; }
	inline u32		GetPhaseVisFlags() const					{ return m_nPhaseVisFlags; }
	inline bool		IsVisibleInAnyPhase() const					{ return m_nPhaseVisFlags != 0; }

	inline void		SetVisBit(const u32 nPhaseId)				{ FastAssert( nPhaseId < MAX_PHASE_VISIBILITY_BITS ); m_nPhaseVisFlags |= (1<<nPhaseId); }
	inline void		ClearVisBit(const u32 nPhaseId)				{ FastAssert( nPhaseId < MAX_PHASE_VISIBILITY_BITS ); m_nPhaseVisFlags &= ~(1<<nPhaseId); }
	inline bool		GetVisBit(const u32 nPhaseId) const			{ FastAssert( nPhaseId < MAX_PHASE_VISIBILITY_BITS ); return ( m_nPhaseVisFlags & (1<<nPhaseId) ) != 0; }

	inline void		SetSubphaseVisFlags(u32 nFlags)				{ FastAssert( nFlags < (1<<MAX_SUBPHASE_VISIBILITY_BITS) ); m_nSubphaseVisFlags = nFlags; }
	inline void		SetSubphaseVisFlags(u32 nFlags, u32 nMask)	{ FastAssert( nFlags < (1<<MAX_SUBPHASE_VISIBILITY_BITS) ); m_nSubphaseVisFlags = nFlags | (m_nSubphaseVisFlags & ~nMask); }
	inline u32		GetSubphaseVisFlags() const					{ return m_nSubphaseVisFlags; }

	inline void		SetOptionFlags(const u32 nFlags)			{ m_nOptionFlags = nFlags; }
	inline void		ClearOptionFlags()							{ m_nOptionFlags = 0; }
	inline u32		GetOptionFlags() const						{ return m_nOptionFlags; }
	inline void		SetOptionBits(const u32 nMask)				{ m_nOptionFlags |= nMask; }
	inline void		ClearOptionBits(const u32 nMask)			{ m_nOptionFlags &= ~nMask; }

	inline void		SetLodSortVal(const u32 sortVal)			{ m_nLodSortVal = sortVal; }
	inline u32		GetLodSortVal() const						{ return m_nLodSortVal; }

	inline void		SetEntityType(const u32 entityType)			{ m_entityType = entityType; }
	inline u32		GetEntityType() const						{ return m_entityType; }

	inline void		SetAlpha(const u32 alpha)					{ m_alpha = alpha; }
	inline u32		GetAlpha() const							{ return m_alpha; }

	inline u32		operator&(u32 val) const					{ FastAssert( val < (1<<MAX_PHASE_VISIBILITY_BITS) ); return m_nPhaseVisFlags & val; }
	inline u32		operator|(u32 val) const					{ FastAssert( val < (1<<MAX_PHASE_VISIBILITY_BITS) ); return m_nPhaseVisFlags | val; }

private:
	u32 m_alpha				: MAX_ALPHA_VISIBILITY_BITS;		// 8
	u32 m_nPhaseVisFlags	: MAX_PHASE_VISIBILITY_BITS;		//24

	u32 m_nOptionFlags		: MAX_OPTION_BITS;					//14
	u32	m_nSubphaseVisFlags	: MAX_SUBPHASE_VISIBILITY_BITS;		//10
	u32 m_nLodSortVal		: MAX_LODTYPES_SORTVAL_BITS;		// 3
	u32 m_entityType		: MAX_ENTITY_TYPE_BITS;				// 4
	ATTR_UNUSED u32 m_tbd				: MAX_TBD_BITS;						// 1
};

CompileTimeAssert( sizeof(fwVisibilityFlags) == 8 );

} // namespace rage

#endif // !defined INC_VISIBILITYFLAGS_H_
