#ifndef _INC_SCANNODES_DEBUG_H_
#define _INC_SCANNODES_DEBUG_H_

#if !__TOOL && __BANK && !__SPU

namespace rage {

class fwScanNodes;
class fwSceneGraphNode;
class fwPortalSceneGraphNode;
class fwPortalCorners;
class fwRenderPhaseCullShape;
class fwScreenQuad;
class datCallback;

class fwScanNodesDebug
{
public:
	void VisibilityBegin(fwScanNodes* scanNodes);
	void VisibilityEnd();

	static const fwPortalCorners*	DebugDrawGetShadowPortals();
	static int						DebugDrawGetShadowPortalCount();
	static const int*				DebugDrawGetShadowPortalIndices();
	static int*						DebugDrawShadowIsolatedPortalIndex();
	static int*						DebugDrawGlobalIsolatedPortalIndex();
	static int*						DebugDrawCullShapeIdForPortalDebugging();

	static void DebugDrawPortal(const fwSceneGraphNode* currentSceneNode, const fwPortalSceneGraphNode* portalSceneNode, const fwPortalCorners* portalCorners, const fwRenderPhaseCullShape* cullShape, const bool solid, bool forceDraw = false);
	void DebugDrawIntermediateScreenQuad(const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad);
	void DebugDrawFinalScreenQuad(const fwSceneGraphNode* sceneNode);

	bool ShowPortalTraversalInfo(const fwRenderPhaseCullShape* cullShape, const fwSceneGraphNode* sceneNode, const fwPortalSceneGraphNode* portalSceneNode, const fwScreenQuad* portalScreenQuad, const char* info, int portalCount, int depth, bool bTraversePortal);
	void ShowDebugShadowIsolatedPortalIndex();
	
	static void SnapShotFrutumDataCallback();
	static int *GetDebugSnapShotCullShapeIndex();
	static void SnapShotDebugFrutumData(fwScanNodes *pScanNodes);
	static int* GetDebugSnapShotRoomIndex();
	static int* GetDebugSnapShotInteriorIndex();
	static void DrawSnapShotFrustums();
	static void DrawFrustumForScreenQuad(const fwScreenQuad& quad, Mat44V_In compositeMatrix, Color32 color);
	static float* GetDrawFrustumForScreenQuadAlpha();
	static float* GetDrawFrustumFarPlaneScale();
	static bool* GetDrawFrustum0();
	static bool* GetDrawFrustum1();
	static bool *GetPortalStraddlingEnabled();
	static bool *GetDisplayPortalStraddlingDebugDraw();
	static bool *GetDrawUsingPlaneSetDebugDraw();
	static bool *GetDrawShadowPlanes();

	static bool *GetBreakOnSelectedEntityInContainer();
	static void SetSetGraphNodeToBreakOn(fwSceneGraphNode *pBreakNode);
	static fwSceneGraphNode *GetSetGraphNodeToBreakOn();
	
	static void SetBreakOnSelectedEntityInContainerCallback(const datCallback& callback);
	static const datCallback& GetBreakOnSelectedEntityInContainerCallback();

private:
	fwScanNodes* m_scanNodes;
};

} // namespace rage

#endif // !__TOOL && __BANK && !__SPU
#endif // !defined _INC_SCANNODES_DEBUG_H_
