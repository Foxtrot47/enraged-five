#if !__TOOL

#if __ASSERT && __SPU
#	undef  Assert
#	undef  Assertf
#	undef  AssertMsg
#	undef  AssertMsgf
#	undef  AssertVerify
#	undef  FastAssert
#	define STOP_WITH_LINE_NUMBER()          __asm__ __volatile__("stop %0"::"i"(__LINE__<=0x3fff?__LINE__:0x3fff))
static inline __attribute__((/*__always_inline__,*/__format__(__printf__,1,2))) void EatVarArgs(const char *fmt,...){(void)fmt;}
// Branch hint Likely to save code space
//      we want
//          brnz    register,.+8
//          stop    __LINE__
//
//      not
//          brz     register, assertion_failure
//        return_from_assert:
//          ...
//        assertion_failure:
//          stop    __LINE__
//          br      return_from_assert
//
#	define Assert(EXP)                      do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();}while(0)
#	define Assertf(EXP,FMT,...)             do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();EatVarArgs(FMT,##__VA_ARGS__);}while(0)
#	define AssertMsg(EXP,FMT,...)           do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();EatVarArgs(FMT,##__VA_ARGS__);}while(0)
#	define AssertMsgf(EXP,FMT,...)          do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();EatVarArgs(FMT,##__VA_ARGS__);}while(0)
#	define AssertVerify(EXP)                (EXP)
#	define FastAssert(EXP)                  Assert(EXP)
#endif

#include "fwmaths/vectorutil.h"
#include "fwscene/scan/ScanNodes.h"
#include "fwscene/scan/ScanNodesDebug.h"
#include "fwscene/scan/ScanDebug.h"
#include "fwscene/scan/ScanPerformance.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwscene/world/WorldLimits.h"
#include "fwscene/world/SceneGraphVisitor.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwscene/world/InteriorSceneGraphNode.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/timer.h"
#include "system/codefrag_spu.h"
#include "system/ppu_symbol.h"
#include "system/dependencyscheduler.h"
#include "fwrenderer/RenderPhaseBase.h"

#if SCL_LOGGING && __SPU
#include <cell/spurs/common.h>
#endif

DECLARE_FRAG_INTERFACE( ScanEntities );

extern rage::Vec4V							g_screenQuadStorage[ rage::SCAN_SCREENQUAD_STORAGE_COUNT ][ rage::fwScanNodes::STORED_SCREEN_QUAD_COUNT ];
extern rage::fwPvsEntry						g_pvsEntryScratch[ rage::SCAN_MAX_ENTITIES_PER_JOB ];

DECLARE_PPU_SYMBOL( rage::fwScanBaseInfo,	g_scanBaseInfo );
DECLARE_PPU_SYMBOL( rage::fwScanResults,	g_pendingScanResults );
DECLARE_PPU_SYMBOL( rage::u32,				g_scanTasksPending );
DECLARE_PPU_SYMBOL( rage::sysDependency*,	g_scanEntitiesDependenciesEa );

#ifndef MAX_INT32
#define MAX_INT32 (2147483647)
#endif

#define PORTAL_TRAVERSAL_DEBUGGING (1 && __BANK && !__SPU) // can disable this for verification

#if PORTAL_TRAVERSAL_DEBUGGING
namespace rage
{
	extern int g_debugDrawGlobalIsolatedPortalIndex;
	extern int g_CullShapeToBreakOn;
}
const fwSceneGraphNode *pSceneNodeToBreakOn = NULL;
#endif // PORTAL_TRAVERSAL_DEBUGGING

#define SCAN_NODES_DETAILED_STATS 0
namespace ScanNodesStats
{
	PF_PAGE(ScanNodesPage,"Scan Nodes");

	PF_GROUP(ScanNodes);
	PF_LINK(ScanNodesPage, ScanNodes);
	PF_TIMER(fwScanNodesRun, ScanNodes);
#if SCAN_NODES_DETAILED_STATS
	PF_TIMER(ProcessEntityContainer, ScanNodes);
	PF_TIMER(ProcessEntityContainers, ScanNodes);
#endif
}
using namespace ScanNodesStats;

namespace rage {

#if __BANK && !__SPU
static fwScanNodesDebug g_scanNodesDebug;
#endif // __BANK && !__SPU
__forceinline u32 IsBoxVisibleInSweptFrustum(const Vec4V m_transposedPlanes[CASCADE_SHADOWS_SCAN_NUM_PLANES],  const spdAABB& box);

#if __SPU
static const vec_char16 s_vMask =  {     0,    1,    2,    3,
									    16,   17,   18,   19, 
									  0x80, 0x80, 0x80, 0x80, 
									  0x80, 0x80, 0x80, 0x80 };

void ProcessWithCustomStack(void* stack, u32 size, fwScanNodes& scanNodes)
{
	register vec_uint4	sp __asm__("$sp");
	const vec_uint4		prevSp = sp;

	const u32 leaveBufferSize = 512;

	u32 ustack = (u32)stack - leaveBufferSize; // TODO: it seems the code "eats" into the stack, so it's better to leave to buffer. This hack is shite btw
	size	   = size - leaveBufferSize;

	vec_uint4 vStack = spu_splats( ustack );
	vec_uint4 vSize  = spu_splats( size );

	sp = (vec_uint4)si_shufb((vec_char16)vStack, (vec_char16)vSize, s_vMask);

	scanNodes.Run();
	sp = prevSp;
}
#endif

bool fwScanNodes::RunFromDependency(const sysDependency& dependency)
{
	PF_FUNC(fwScanNodesRun);

#if SCL_LOGGING
	u32 sclAddress = SCLPushScanNodesBegin();
#endif // SCL_LOGGING

	FetchScanDebugFlags();
	fwScanNodes					scanNodes;

#if __SPU
	fwScanResults*				scanResults   = (fwScanResults*)dependency.m_Params[0].m_AsPtr; 
	u8*							scratchBuffer = (u8*)(scanResults + 1);
	const int					scratchBufferSize = dependency.m_DataSizes[0] - sizeof(fwScanResults);
#else
	u8*							scratchBuffer = (u8*)dependency.m_Params[0].m_AsPtr;
	fwScanResults*				scanResults = (fwScanResults*)dependency.m_Params[3].m_AsPtr;
	const int					scratchBufferSize = dependency.m_DataSizes[0];
#endif

	u8*							poolsMemory = static_cast< u8* >( dependency.m_Params[1].m_AsPtr );
	const u32					poolsMemorySize = dependency.m_DataSizes[1];
	fwScanBaseInfo*				scanBaseInfo = static_cast< fwScanBaseInfo* >( dependency.m_Params[2].m_AsPtr );
	sysEaPtr< Vec4V >			occlusionBufferGBuf = static_cast< Vec4V* >( dependency.m_Params[4].m_AsPtr );
	sysEaPtr< Vec4V >			occlusionBufferCascadeShadows = static_cast< Vec4V* >( dependency.m_Params[5].m_AsPtr );
	sysEaPtr< u8 >				ppuPoolsMemory = static_cast< u8* >( dependency.m_Params[6].m_AsPtr );
	fwSceneGraphNode*			rootSceneNode = static_cast< fwSceneGraphNode* >( dependency.m_Params[7].m_AsPtr );

	(void) poolsMemory;
	(void) poolsMemorySize;
	(void) ppuPoolsMemory;

#if __SPU
	SCAN_STATS_ONLY( sysTimer pointerFixupTimer; )

	fwSceneGraph::InitSpuPools( poolsMemory, ppuPoolsMemory, poolsMemorySize );
	for (u32 i = 0; i < scanBaseInfo->m_cullShapeCount; ++i)
		scanBaseInfo->m_cullShapes[i].FixupPointers( fwSceneGraph::GetPointerFixer() );
	fwSceneGraph::GetPointerFixer().Fix( rootSceneNode );

	SCAN_STATS_ONLY( sysInterlockedAdd( (volatile u32*) CELL_SPURS_PPU_SYM( g_spuPointerFixupUs ), static_cast< int >( pointerFixupTimer.GetUsTime() ) ); )
#endif

	scanNodes.SetScanBaseInfo( scanBaseInfo );
	scanNodes.SetScanResults( scanResults );
	scanNodes.SetRootSceneNode( rootSceneNode );
	scanNodes.SetHiZBufferEa( SCAN_OCCLUSION_STORAGE_PRIMARY, occlusionBufferGBuf );
	scanNodes.SetHiZBufferEa( SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS, occlusionBufferCascadeShadows );

	u32 amountScratchBufferUsed = scanNodes.AssignScratchBuffer( scratchBuffer, scratchBufferSize );
	amountScratchBufferUsed = (amountScratchBufferUsed + 15) & ~15; // round up to 16

	SPU_ONLY( SCAN_STATS_ONLY( sysTimer processTimer; ) )
#if __SPU
	void*	customStack = (void*)( (u32) scratchBuffer + scratchBufferSize );
	ProcessWithCustomStack( customStack, scratchBufferSize - amountScratchBufferUsed, scanNodes);
#else
	scanNodes.Run();
#endif
	SPU_ONLY( SCAN_STATS_ONLY( sysInterlockedAdd( (volatile u32*) CELL_SPURS_PPU_SYM( g_spuNodeProcessUs ), (int) processTimer.GetUsTime() ); ) )

	scanNodes.CheckScratchBufferGuard();


#if SCL_LOGGING
	SCLPushScanNodesEnd(sclAddress);
#endif // SCL_LOGGING

	sysInterlockedDecrement( PPU_SYMBOL(g_scanTasksPending) );

	return true;
}

u32 fwScanNodes::AssignScratchBuffer(u8* memory, const u32 ASSERT_ONLY(size))
{
	 u8*				startMemory = memory;
	u32*				beingVisitedBits[PORTAL_TRAVERSAL_TYPE_COUNT];
	u32*				visitedBits[PORTAL_TRAVERSAL_TYPE_COUNT];
	u32*				renderLodsOnlyBits;

	m_visFlags = reinterpret_cast< u32* >( memory );										memory += sizeof(u32) * fwSceneGraph::MAX_SCENE_NODE_COUNT;
	memory += 0x10 - ( reinterpret_cast< size_t >( memory ) & 0xf );
	m_screenQuadsStorage[0] = reinterpret_cast< Vec4V* >( memory );							memory += sizeof(Vec4V) * STORED_SCREEN_QUAD_COUNT;
	m_screenQuadsStorage[1] = reinterpret_cast< Vec4V* >( memory );							memory += sizeof(Vec4V) * STORED_SCREEN_QUAD_COUNT;
	beingVisitedBits[PORTAL_TRAVERSAL_TYPE_NORMAL] = reinterpret_cast< u32* >( memory );	memory += sizeof(u32) * ( (fwSceneGraph::MAX_SCENE_NODE_COUNT + 31) >> 5 );
	beingVisitedBits[PORTAL_TRAVERSAL_TYPE_MIRROR] = reinterpret_cast< u32* >( memory );	memory += sizeof(u32) * ( (fwSceneGraph::MAX_SCENE_NODE_COUNT + 31) >> 5 );
	visitedBits[PORTAL_TRAVERSAL_TYPE_NORMAL] = reinterpret_cast< u32* >( memory );			memory += sizeof(u32) * ( (fwSceneGraph::MAX_SCENE_NODE_COUNT + 31) >> 5 );
	visitedBits[PORTAL_TRAVERSAL_TYPE_MIRROR] = reinterpret_cast< u32* >( memory );			memory += sizeof(u32) * ( (fwSceneGraph::MAX_SCENE_NODE_COUNT + 31) >> 5 );
	renderLodsOnlyBits = reinterpret_cast< u32* >( memory );								memory += sizeof(u32) * ( (fwSceneGraph::MAX_SCENE_NODE_COUNT + 31) >> 5 );
	memory += 0x10 - ( reinterpret_cast< size_t >( memory ) & 0xf );
	m_scratchBufferGuard = memory;															memory += 16;

	m_beingVisited[PORTAL_TRAVERSAL_TYPE_NORMAL].Init( beingVisitedBits[PORTAL_TRAVERSAL_TYPE_NORMAL], fwSceneGraph::MAX_SCENE_NODE_COUNT );
	m_beingVisited[PORTAL_TRAVERSAL_TYPE_MIRROR].Init( beingVisitedBits[PORTAL_TRAVERSAL_TYPE_MIRROR], fwSceneGraph::MAX_SCENE_NODE_COUNT );
	m_visited[PORTAL_TRAVERSAL_TYPE_NORMAL].Init( visitedBits[PORTAL_TRAVERSAL_TYPE_NORMAL], fwSceneGraph::MAX_SCENE_NODE_COUNT );
	m_visited[PORTAL_TRAVERSAL_TYPE_MIRROR].Init( visitedBits[PORTAL_TRAVERSAL_TYPE_MIRROR], fwSceneGraph::MAX_SCENE_NODE_COUNT );
	m_renderLodsOnly.Init( renderLodsOnlyBits, fwSceneGraph::MAX_SCENE_NODE_COUNT );
	memset( m_scratchBufferGuard, 0xFE, 16 );

	u32 amountScratchBufferUsed = u32(memory - startMemory);
	Assertf( amountScratchBufferUsed <= size, "Scratch buffer size not sufficient for this job" );

	return amountScratchBufferUsed;
}

void fwScanNodes::CheckScratchBufferGuard()
{
#if __ASSERT
	for (int i = 0; i < 16; ++i)
		Assertf( m_scratchBufferGuard[i] == 0xFE, "Scratch buffer corrupted in fwScanNodes" );
#endif
}

void fwScanNodes::ClearVisData()
{
	memset( m_visFlags, 0, sizeof(u32) * fwSceneGraph::MAX_SCENE_NODE_COUNT );
	m_renderLodsOnly.Reset( 0xff );

	m_scanResults->ClearFlags();
	m_scanResults->SetVisibleRoomsShadowScreenQuad( fwScreenQuad(SCREEN_QUAD_INVALID) );
	memset( m_scanResults->GetExteriorShadowFrustumPlanesPtr(), 0, sizeof(Vec4V) * CASCADE_SHADOWS_SCAN_NUM_PLANES );
	m_scanResults->SetMirrorDistance( 0.0f );
	m_scanResults->SetMirrorPriority( 0 );
#if __BANK
	m_scanResults->SetMirrorFloorQuadCount( 0 );
#endif // __BANK
	m_scanResults->m_skyPortals_ParaboloidReflection.ClearSkyPortals();
	m_scanResults->m_skyPortalBounds_MirrorReflection.ClearSkyPortals();
	m_scanResults->m_startingSceneNodeInInterior = 0;
	m_scanResults->SetGbuffExteriorNodeScreenQuadIndex(-1);
	m_gBufferRoom0Nodes.Reset();
}

void fwScanNodes::ClearTraversalData(const fwRenderPhaseCullShape& cullShape)
{
	m_beingVisited[PORTAL_TRAVERSAL_TYPE_NORMAL].Reset();
	m_visited[PORTAL_TRAVERSAL_TYPE_NORMAL].Reset();
	m_portalStack.Reset();

	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_MIRROR )
	{
		m_beingVisited[PORTAL_TRAVERSAL_TYPE_MIRROR].Reset();
		m_visited[PORTAL_TRAVERSAL_TYPE_MIRROR].Reset();
	}

	if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
	{
		for (int i = 0; i < STORED_SCREEN_QUAD_COUNT; ++i)
			m_screenQuadPairs[i] = fwScreenQuadPair(SCREEN_QUAD_INVALID);
	}
	else
	{
		for (int i = 0; i < STORED_SCREEN_QUAD_COUNT; ++i)
			m_screenQuads[i] = fwScreenQuad(SCREEN_QUAD_INVALID);
	}
}

s16 fwScanNodes::GetStoredScreenQuadIndex(const fwSceneGraphNode* sceneNode)
{
	Assert( !sceneNode->IsTypeInterior() ); // this should never happen

	// screen quad index for EXTERIOR/STREAMED/INTERIOR is 0
	// screen quad index for ROOM/PORTAL starts at 1 ..
	return Max<s16>( 0, sceneNode->GetIndex() - fwSceneGraph::FIRST_ROOM_SCENE_NODE_INDEX + 1 );
}

void fwScanNodes::ProcessEntityContainer(const fwSceneGraphNode* sceneNode, const fwBaseEntityContainer* entityContainer, const u32 controlFlags, u32 &ppuJobCounter)
{
#if SCAN_NODES_DETAILED_STATS
	PF_FUNC(ProcessEntityContainer);
#endif

#if !__SPU && __BANK
	if (*fwScanNodesDebug::GetBreakOnSelectedEntityInContainer() && fwScanNodesDebug::GetSetGraphNodeToBreakOn() == sceneNode )
	{
		__debugbreak();
	}
#endif

	UNUSED_VAR(ppuJobCounter);

	if ( sceneNode->IsTypeStreamed() && !sceneNode->IsEnabled() )
		return;

	const s16		sceneNodeIndex = sceneNode->GetIndex();
	const s16		storedScreenQuadIndex = GetStoredScreenQuadIndex(sceneNode);
	const u32		visFlags = m_visFlags[ sceneNodeIndex ];
	u32				localControlFlags = controlFlags;

	FastAssert( (visFlags & 0xff000000) == 0x0 );
	if ( !visFlags || !entityContainer->GetEntityCount() )
		return;

	if ( entityContainer->IsTypeSoA() )
		localControlFlags |= SCAN_ENTITIES_FLAG_ENTITY_DESCS_SOA;

	u32				packedFlags = 0;
	packedFlags |= ( visFlags & 0x00ffffff);
	packedFlags |= ( localControlFlags & 0xff) << 24;

	const bool		bIsVisibleInGBuf = visFlags && (1 << m_gbufCullShape->GetRenderPhaseId());
	int				entityOffset = 0;
	int				entityCount = entityContainer->IsTypeFixed() ? entityContainer->GetStorageSize() : entityContainer->GetEntityCount();

	while ( entityCount > 0 )
	{
		sysEaPtr< fwEntityDesc >	jobEntityDescs = entityContainer->GetEntityDescArray() + entityOffset;
		int							jobCount = rage::Min( entityCount, static_cast< int >( SCAN_MAX_ENTITIES_PER_JOB ) );

		{
			const u32			flags =
				sysDepFlag::ALLOC0	|
				sysDepFlag::INPUT1	|
				sysDepFlag::INPUT2	|
				sysDepFlag::INPUT3	|
				sysDepFlag::INPUT4	|
				sysDepFlag::INPUT5  |
				sysDepFlag::INPUT6;			
			
			// First, increment the dependency count, since we'll be inserting a new fwScanEntities job.
			sysInterlockedIncrement( PPU_SYMBOL(g_scanTasksPending) );

			// Next, grab the next available sysDependency we can use from our pool of dependencies.
			Assertf( m_currentScanEntitiesDependency < MAX_SCAN_ENTITIES_DEPENDENCY_COUNT, "No more scan dependencies available to process entity visibility" );
			sysEaPtr< sysDependency >	ppuDependency = static_cast< sysDependency* >( sysInterlockedReadPointer( (void**)PPU_SYMBOL(g_scanEntitiesDependenciesEa) ) ) + m_currentScanEntitiesDependency;
			m_currentScanEntitiesDependency++;

#if __SPU
			FastAssert( !(g_scanDebugFlags & SCAN_DEBUG_PROCESS_VISIBILITY_ON_PPU) );

			sysDependency spuDependency;
			sysDependency* dependency = &spuDependency;
			dependency->Init( FRAG_SPU_CODE(ScanEntities), 0, flags );
#else
			sysEaPtr< sysDependency > dependency = ppuDependency;
			dependency->Init( fwScanEntities::RunFromDependency, 0, flags );
#endif
			// 8/15/12 - cthomas - Changing the priority of all scan jobs in the dependency system from Medium -> High.
			// These jobs are waited on by the main thread only a few ms after they are launched, and so need to have the 
			// highest priority possible (without being critical, reserved for only for those jobs perhaps launched and 
			// immediately waited on). At the least, changing to high priority now makes the priority equal to that of 
			// the jobs launched by the animation system right now, and animation generally has a bit more leeway time 
			// than the scan jobs.
			// 3/15/13 - cthomas - For purposes of knowing what needs to be pre-rendered ASAP, we'd like to prioritize 
			// entity containers that are visible in Gbuf over those that are not (as only entities visible in GBuf get 
			// pre-rendered). If this entity container is visible in GBuf, give it critical priority.
			dependency->m_Priority = (u8) (bIsVisibleInGBuf ? sysDependency::kPriorityCritical : sysDependency::kPriorityHigh);
			dependency->m_Params[1].m_AsPtr = PPU_SYMBOL(g_scanBaseInfo);
			dependency->m_Params[2].m_AsPtr = PPU_SYMBOL(g_pendingScanResults);
			dependency->m_Params[3].m_AsPtr = m_hiZBufferEa[ SCAN_OCCLUSION_STORAGE_PRIMARY ].ToPtr();
			dependency->m_Params[4].m_AsPtr = m_hiZBufferEa[ SCAN_OCCLUSION_STORAGE_CASCADE_SHADOWS ].ToPtr();
			dependency->m_Params[5].m_AsPtr = jobEntityDescs.ToPtr();
			dependency->m_Params[6].m_AsPtr = m_scanBaseInfo->m_reflectionPlaneSets;
			dependency->m_Params[7].m_AsInt = packedFlags;
			dependency->m_Params[8].m_AsInt = storedScreenQuadIndex;

			dependency->m_DataSizes[0] = fwScanEntities::SCRATCH_BUFFER_SIZE;
			dependency->m_DataSizes[1] = sizeof(fwScanBaseInfo);
			dependency->m_DataSizes[2] = sizeof(fwScanResults);
			dependency->m_DataSizes[3] = RAST_HIZ_AND_STENCIL_SIZE_BYTES;
			dependency->m_DataSizes[4] = RAST_HIZ_AND_STENCIL_SIZE_BYTES;
			dependency->m_DataSizes[5] = sizeof(fwEntityDesc) * jobCount;
			dependency->m_DataSizes[6] = sizeof(spdTransposedPlaneSet4) * 6;

#if __SPU
			sysDmaPutAndWait( dependency, ppuDependency.ToUint(), sizeof(sysDependency), g_auxDmaTag );
			sysDependencyScheduler::Insert( ppuDependency.ToPtr() );
#else
			sysDependencyScheduler::Insert( dependency.ToPtr() );
#endif

#if SCL_LOGGING
			u32 containerType;
			if (entityContainer->IsTypeDefault())
			{	
				containerType = SCL_CONTAINER_ENTITY;
			}
			else if (entityContainer->IsTypeFixed())
			{
				containerType = SCL_CONTAINER_FIXED;
			}
			else
			{
				containerType = SCL_CONTAINER_SOA;
			}
			SCLPushScanNodesAddScanEntitiesJob(containerType, (u32)jobEntityDescs.ToPtr(), jobCount);
#endif
		}

		entityOffset += jobCount;
		entityCount -= jobCount;
	}
}

void fwScanNodes::AddVisibility(fwSceneGraphNode* sceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad)
{
#if PORTAL_TRAVERSAL_DEBUGGING
	if ((g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_PORTALS_BREAK_FOR_ADD_VISIBILITY_CALLED_ON_ISOLOATED) && 
		(cullShape->GetRenderPhaseId() == (u32)g_CullShapeToBreakOn) && 
		(sceneNode == pSceneNodeToBreakOn))
	{
		__debugbreak();
	}
#endif

	m_visFlags[ sceneNode->GetIndex() ] |= ( 1 << cullShape->GetRenderPhaseId() );
	
	if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS )
	{
		if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS )
			m_screenQuadPairs[ GetStoredScreenQuadIndex(sceneNode) ].Grow( screenQuad );
		else
			m_screenQuads[ GetStoredScreenQuadIndex(sceneNode) ].Grow( screenQuad );
	}

	if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
	{
		if ( m_portalStack.GetCount() == 0 || m_portalStack.Top().m_portal->GetRenderLodsOnly() == false )
			m_renderLodsOnly.Clear( sceneNode->GetIndex() );

		if ( sceneNode->IsTypeExterior() )
		{
			m_scanResults->SetGbuffExteriorNodeScreenQuadIndex(GetStoredScreenQuadIndex(sceneNode));
			m_scanResults->SetExteriorIsVisibleInGbuf();
		}
		if ( sceneNode->IsTypeStreamed() )
		{
			m_scanResults->SetExteriorIsVisibleInGbuf();
		}
	}

	if ( (cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS) && sceneNode->IsTypeRoom() )
	{
		if ( m_visFlags[ sceneNode->GetIndex() ] & ( 1 << m_gbufCullShape->GetRenderPhaseId() ) )
			m_scanResults->GetVisibleRoomsShadowScreenQuadRef().Grow( screenQuad );
	}
}

int fwScanNodes::PushPortalsFromRoom(const fwRenderPhaseCullShape* cullShape, const fwRoomSceneGraphNode* roomSceneNode)
{
	int		portalCount = 0;

	for (fwSceneGraphNode* interiorChild = roomSceneNode->GetOwnerInteriorSceneNode()->GetFirstChild(); interiorChild != NULL; interiorChild = interiorChild->GetNext())
	{
		if ( !interiorChild->IsTypePortal() )
			continue;

		fwPortalSceneGraphNode*		portalSceneNode = static_cast< fwPortalSceneGraphNode* >( interiorChild );
		PortalEntry					portalEntry;
		const bool					traverseOneWay = portalSceneNode->GetIsOneWay() && !(cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS);

		if ( !portalSceneNode->GetNegativePortalEnd() || !portalSceneNode->GetPositivePortalEnd() )
			continue;

		portalEntry.m_portal = portalSceneNode;
		if ( portalSceneNode->GetNegativePortalEnd() == roomSceneNode )
			portalEntry.m_dest = portalSceneNode->GetPositivePortalEnd();
		else if ( !traverseOneWay && portalSceneNode->GetPositivePortalEnd() == roomSceneNode )
			portalEntry.m_dest = portalSceneNode->GetNegativePortalEnd();
		else
			continue;

		m_portalStack.Push( portalEntry );
		++portalCount;
	}

	return portalCount;
}

int fwScanNodes::PushPortalsFromExterior(const fwRenderPhaseCullShape* cullShape, const fwInteriorSceneGraphNode* interiorSceneNode)
{
	int portalCount = 0;
	s32 interiorIndex = -1;
	bool interiorClosed = false;

	for (fwSceneGraphNode* interiorChild = interiorSceneNode->GetFirstChild(); interiorChild != NULL; interiorChild = interiorChild->GetNext())
	{
		if ( !interiorChild->IsTypePortal() )
			continue;

		fwPortalSceneGraphNode*	portalSceneNode = static_cast< fwPortalSceneGraphNode* >( interiorChild );

		if (interiorIndex == -1)
		{
			interiorIndex = portalSceneNode->GetInteriorLocation().GetInteriorProxyIndex();
			interiorClosed = m_scanBaseInfo->m_closedInteriors.Find((u16)interiorIndex) != -1 && (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_PORTALS_DONT_TRAVERSE_ONE_WAY_IF_INTERIORS_CLOSED);
		}

		// If we are the shadow cullshape and we are in an interior then don't close off interiors
		if (cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS && m_gbufCullShape->GetStartingSceneNode()->IsTypeRoom())
		{
			interiorClosed = false;
		}
		const bool	portalIsOneWay = portalSceneNode->GetIsOneWay() && (interiorClosed || !(cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS));

		if ( !portalSceneNode->GetNegativePortalEnd() || !portalSceneNode->GetPositivePortalEnd() )
			continue;

		PortalEntry	portalEntry;
		portalEntry.m_portal = portalSceneNode;
		if ( portalSceneNode->GetNegativePortalEnd()->IsTypeExterior() )
			portalEntry.m_dest = portalSceneNode->GetPositivePortalEnd();
		else if ( !portalIsOneWay && portalSceneNode->GetPositivePortalEnd()->IsTypeExterior() )
			portalEntry.m_dest = portalSceneNode->GetNegativePortalEnd();
		else
			continue;

		m_portalStack.Push( portalEntry );
		++portalCount;
	}

	return portalCount;
}

void fwScanNodes::TraversePortals(const int portalCount, const fwSceneGraphNode* sceneNode, const fwRenderPhaseCullShape* cullShape, 
								  const fwScreenQuad& screenQuad, ePortalTraversalType portalTraversalType, float accumulatedPortalOpacity,
								  int decrementedDepth fwScanNodes_DEBUG(int depth))
{

	if (depth > 24)
	{
		return;
	}
#if PORTAL_TRAVERSAL_DEBUGGING
	const bool bPortalTraversalDebugging = g_scanNodesDebug.ShowPortalTraversalInfo( cullShape, sceneNode, NULL, NULL, NULL, portalCount, depth, true );
#endif // PORTAL_TRAVERSAL_DEBUGGING

	for (int i = 0; i < portalCount; m_portalStack.Pop(), ++i)
	{
		const Vec3V						camPos				= cullShape->GetCameraPosition();
		PortalEntry						portalEntry			= m_portalStack.Top();
		const fwPortalSceneGraphNode*	portalSceneNode		= portalEntry.m_portal;
		fwPortalCorners					portalCorners		= portalSceneNode->GetCorners();
		fwSceneGraphNode*				portalDest			= portalEntry.m_dest;
		fwScreenQuad					portalScreenQuad	= screenQuad;
		float							closeness;
		const float						closenessTraversalThreshold = 0.05f;
		const float						closenessFrustumCullingThreshold = 0.6f;
		ePortalTraversalType			portalTraversalTypeDest = portalTraversalType;

#if PORTAL_TRAVERSAL_DEBUGGING
		if ((g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_BREAK_TO_DEBUG_PORTALS))
		{
			int shadowIsolateIndex = *fwScanNodesDebug::DebugDrawShadowIsolatedPortalIndex();
			if ((cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS) && shadowIsolateIndex != -1)
			{
				const int* pIndices = fwScanNodesDebug::DebugDrawGetShadowPortalIndices();

				if (portalSceneNode->GetPortalNodeIndex() == pIndices[shadowIsolateIndex])
				{
					__debugbreak();
				}
			}

			if ((cullShape->GetRenderPhaseId() == (u32)g_CullShapeToBreakOn) && rage::g_debugDrawGlobalIsolatedPortalIndex != -1)
			{
				if (portalSceneNode->GetIndex() == rage::g_debugDrawGlobalIsolatedPortalIndex)
				{
					__debugbreak();
				}
			}
		}

		if (rage::g_debugDrawGlobalIsolatedPortalIndex != -1)
		{
			if (portalSceneNode->GetIndex() == rage::g_debugDrawGlobalIsolatedPortalIndex)
			{
				pSceneNodeToBreakOn = portalSceneNode;
			}
		}
#endif

		if ( portalTraversalType == PORTAL_TRAVERSAL_TYPE_MIRROR )
		{
			const Vec4V mirrorPlane = cullShape->GetMirrorPlane();
			const Vec3V corner3 = PlaneReflect(mirrorPlane, VECTOR3_TO_VEC3V(portalCorners.GetCorner(0)));
			const Vec3V corner2 = PlaneReflect(mirrorPlane, VECTOR3_TO_VEC3V(portalCorners.GetCorner(1)));
			const Vec3V corner1 = PlaneReflect(mirrorPlane, VECTOR3_TO_VEC3V(portalCorners.GetCorner(2)));
			const Vec3V corner0 = PlaneReflect(mirrorPlane, VECTOR3_TO_VEC3V(portalCorners.GetCorner(3)));

			portalCorners.SetCorner(0, RCC_VECTOR3(corner0));
			portalCorners.SetCorner(1, RCC_VECTOR3(corner1));
			portalCorners.SetCorner(2, RCC_VECTOR3(corner2));
			portalCorners.SetCorner(3, RCC_VECTOR3(corner3));
		}

#if PORTAL_TRAVERSAL_DEBUGGING
#define SET_PORTAL_TRAVERSAL(x) { info = x; }
#define END_PORTAL_TRAVERSAL(x) { info = x; bTraversePortal = false; break; }
		const char* info = "";
		bool bTraversePortal = true;
		while (1)
		{
			if ( (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTALS_TRAVERSED_ONLY) == 0 )
				g_scanNodesDebug.DebugDrawPortal( sceneNode, portalSceneNode, NULL, cullShape, false );
#else
#define SET_PORTAL_TRAVERSAL(x)
#define END_PORTAL_TRAVERSAL(x) continue
#endif
			if (	!(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_TRAVERSE_DISABLED_PORTALS)
				&&	!(cullShape->GetFlags() & CULL_SHAPE_FLAG_TRAVERSE_DISABLED_PORTALS)
				&&	!portalSceneNode->IsEnabled()	)
			{
				END_PORTAL_TRAVERSAL(" - portal disabled");
			}

			if ( portalCorners.CalcCloseness( RCC_VECTOR3(camPos), closeness ) )
				closeness = rage::Abs( closeness );
			else
				closeness = FLT_MAX;

			if ( closeness <= closenessTraversalThreshold )
			{ /* do nothing */ SET_PORTAL_TRAVERSAL(" - too close"); }
			else
			{
				if ( (cullShape->GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_FRUSTUM) && closeness <= closenessFrustumCullingThreshold )
				{ /* do nothing */ SET_PORTAL_TRAVERSAL(" - too close to frustum"); }
				else BANK_ONLY( if ( !(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_TRAVERSE_CULLED_PORTALS) ) )
				{
					bool		isClipped;
					spdAABB		cornersAabb;

					portalCorners.CalcBoundingBox( cornersAabb );

					if (sceneNode->IsTypeRoom() && cullShape->GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS)
					{
						Assertf(cullShape->GetFlags() & CULL_SHAPE_FLAG_USE_SEPARATE_INTERIOR_SPHERE_GEOMETRY, "Reflection map must use interior sphere");

						// if its a portal to the exterior we must checek with the exterior sphere
						if ((portalSceneNode->GetPositivePortalEnd() && portalSceneNode->GetPositivePortalEnd()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR) || 
							(portalSceneNode->GetNegativePortalEnd() && portalSceneNode->GetNegativePortalEnd()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR))
						{
							isClipped = !cornersAabb.IntersectsSphere( cullShape->GetSphere());
						}
						else
						{
							isClipped = !cornersAabb.IntersectsSphere( cullShape->GetInteriorSphere());
						}
					}
					else if ((g_scanDebugFlags & SCAN_DEBUG_USE_SWEPT_FRUSTUM_FOR_PORTAL_CULLING) &&  
							 (cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS))
					{
						isClipped = !IsBoxVisibleInSweptFrustum(m_scanBaseInfo->m_cascadeShadowInfo.GetTransposedPlanesPtr(), cornersAabb);
					}
					else
					{
						//B* 1876528 - a quick hack to prevent portal traversal failing when the near clip is pushed in front of a nearby
						//exterior portal
						//TODO a better fix post-GTAV would be to not use a transposed planeset for purposes of portal traversal culling - instead it should be
						//be using a pyramid - a far clip and four triangles. entity culling should still use the transposed planeset.
						if ( (cullShape->GetFlags() & CULL_SHAPE_FLAG_PRIMARY) &&
							((portalSceneNode->GetPositivePortalEnd() && portalSceneNode->GetPositivePortalEnd()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR) || 
							(portalSceneNode->GetNegativePortalEnd() && portalSceneNode->GetNegativePortalEnd()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR)) )
						{
							cornersAabb.GrowUniform( ScalarV(1.0f) );
						}
						
						isClipped = cullShape->IsClipped( cornersAabb );
					}

					if ( isClipped )
						END_PORTAL_TRAVERSAL(" - not visible");
				}

				if ( !(cullShape->GetFlags() & CULL_SHAPE_FLAG_GEOMETRY_SPHERE) && !(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DONT_CHECK_PORTAL_SIDE_ON_TRAVERSAL) )
				{
					const bool	cameraOnPositiveSide = portalCorners.IsOnPositiveSide( RCC_VECTOR3(camPos) );
					const bool	sourceOnPositiveSide = ( portalDest == portalSceneNode->GetNegativePortalEnd() );

					if ( cameraOnPositiveSide != sourceOnPositiveSide )
						END_PORTAL_TRAVERSAL(" - camera wrong side");
				}

				if (cullShape->GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS)
				{					
					portalScreenQuad = screenQuad.Intersection( fwScreenQuad( portalSceneNode->GetCorners(), cullShape->GetCompositeMatrix() ) );

					if ( !portalScreenQuad.IsValid()  && portalTraversalType != PORTAL_TRAVERSAL_TYPE_MIRROR)
						END_PORTAL_TRAVERSAL(" - clipped");
				}
			}

			if ( portalSceneNode->GetIsMirror() )
			{
				if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
				{
					bool bProcessMirror = true;

					if ( portalSceneNode->GetIsMirrorFloor() )
					{
						if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_IGNORE_HORIZONTAL_MIRRORS )
							bProcessMirror = false;
					}
					else
					{
						if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_IGNORE_VERTICAL_MIRRORS )
							bProcessMirror = false;
					}

					if ( bProcessMirror )
						ProcessMirror( camPos, portalSceneNode, &portalCorners );

					END_PORTAL_TRAVERSAL(" - ");
				}
				else if ( portalTraversalType == PORTAL_TRAVERSAL_TYPE_MIRROR ) // traverse back into this node, but without flip
				{
					portalDest = const_cast< fwSceneGraphNode* >( sceneNode );
					portalTraversalTypeDest = PORTAL_TRAVERSAL_TYPE_NORMAL;

					portalScreenQuad = screenQuad.Intersection( fwScreenQuad( portalSceneNode->GetCorners(), cullShape->GetCompositeMatrix() ) );

					SET_PORTAL_TRAVERSAL(" - flip");
				}
				else
					END_PORTAL_TRAVERSAL(" - ");
			}

			if (!(g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_IGNORE_PORTAL_OPACITY))
			{
				accumulatedPortalOpacity += (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_OVERRIDE_PORTAL_OPACITY) ? g_scanDebugOverridePortalOpacity : portalSceneNode->GetOpacity();
				if (accumulatedPortalOpacity >= 1.0f)
				{
					END_PORTAL_TRAVERSAL(" - opacity reached 100");
				}
			}

#if PORTAL_TRAVERSAL_DEBUGGING
			if ( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISPLAY_PORTALS_TRAVERSED_ONLY )
				g_scanNodesDebug.DebugDrawPortal( sceneNode, portalSceneNode, NULL, cullShape, false );

			break;
		}

		if ( bPortalTraversalDebugging )
			g_scanNodesDebug.ShowPortalTraversalInfo( cullShape, sceneNode, portalSceneNode, &portalScreenQuad, info, portalCount, depth, bTraversePortal );

		if (!bTraversePortal)
			continue;
#endif // PORTAL_TRAVERSAL_DEBUGGING

		if ( portalDest->IsTypeRoom() )
		{
			if ( (cullShape->GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS) != 0 && sceneNode->IsTypeExterior() )
			{
				m_scanResults->m_skyPortals_ParaboloidReflection.AddSkyPortal( portalSceneNode ); // wtf? TODO -- why is this necessary?

#if SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
				if ( !m_scanResults->m_skyPortals_ParaboloidReflection.GetIsSkyPortalVisInterior() )
					m_scanResults->m_skyPortals_ParaboloidReflection.AddExteriorToInteriorPortalCorners( portalCorners );
#endif // SCAN_SKY_PORTALS_FOR_REFLECTION_EXT
			}

			ProcessRoomVisibility( static_cast< fwRoomSceneGraphNode* >( portalDest ), cullShape, portalScreenQuad, portalTraversalTypeDest, accumulatedPortalOpacity, decrementedDepth - 1 fwScanNodes_DEBUG(depth + 1) );
		}
		else if ( portalDest->IsTypeExterior() )
		{
			if (!m_dontRenderExterior || (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_IGNORE_DONT_RENDER_EXTERIOR_FROM_CURRENT_ROOM))
			{
				if ((g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_IGNORE_ROOM_EXTERIOR_VISIBILITY_DEPTH) || decrementedDepth  > 0)
				{
					if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS )
						m_scanResults->m_skyPortals_ParaboloidReflection.AddSkyPortal( portalSceneNode );
					if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_MIRROR )
						m_scanResults->m_skyPortalBounds_MirrorReflection.AddSkyPortal( portalScreenQuad );

					ProcessExteriorVisibility( static_cast< fwExteriorSceneGraphNode* >( portalDest ), cullShape, portalScreenQuad, portalTraversalTypeDest fwScanNodes_DEBUG(depth + 1) );
				}
			}
		}
		else
			Assertf( false, "Unexpected scene graph node type" );
	}
}

void fwScanNodes::ProcessMirror(Vec3V_In camPos, const fwPortalSceneGraphNode* portalSceneNode, const fwPortalCorners* portalCorners)
{
	const float mirrorDistance = portalCorners->CalcDistanceToPoint( camPos ).Getf();

	if ( portalSceneNode->GetMirrorCanSeeDirectionalLight() )
		m_scanResults->SetMirrorCanSeeDirectionalLight();

	if ( portalSceneNode->GetMirrorCanSeeExteriorView() )
		m_scanResults->SetMirrorCanSeeExteriorView();

	if ( portalSceneNode->GetIsMirrorFloor() )
	{
		if ( m_scanResults->GetMirrorVisible() == false )
		{
			m_scanResults->SetMirrorVisible();
			m_scanResults->SetMirrorFloorVisible();
			m_scanResults->SetMirrorDistance( mirrorDistance );
			m_scanResults->GetMirrorCornersRef() = *portalCorners;
			m_scanResults->SetMirrorInteriorLocation( portalSceneNode->GetInteriorLocation() );
#if __BANK
			m_debugMirrorPortal = portalSceneNode;
			m_debugMirrorRoom = static_cast< fwRoomSceneGraphNode* >( portalSceneNode->GetNegativePortalEnd() );
#endif // __BANK
		}
		else if ( m_scanResults->GetMirrorFloorVisible() ) // update the corners
		{
			const Vec3V basisT = VECTOR3_TO_VEC3V(m_scanResults->GetMirrorCorners().GetCorner(0));

			// some of the water surface portals in the tunnels are incorrectly being merged together even though they are completely different heights .. this should fix that situation
			if ( portalSceneNode->GetIsWaterSurface() && Abs( basisT - VECTOR3_TO_VEC3V(portalCorners->GetCorner(0)) ).GetZf() > 0.01f BANK_ONLY( && !( g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_FORCE_MERGE_WATER_SURFACE_PORTALS ) ) )
			{
				if ( mirrorDistance < m_scanResults->GetMirrorDistance() )
				{
					m_scanResults->SetMirrorDistance( mirrorDistance );
					m_scanResults->GetMirrorCornersRef() = *portalCorners;
					m_scanResults->SetMirrorInteriorLocation( portalSceneNode->GetInteriorLocation() );
#if __BANK
					m_debugMirrorPortal = portalSceneNode;
					m_debugMirrorRoom = static_cast< fwRoomSceneGraphNode* >( portalSceneNode->GetNegativePortalEnd() );
#endif // __BANK
				}
#if __BANK
				m_scanResults->SetWaterSurfaceHeightMismatch(); // art-error, water surfaces must match height
#endif // __BANK
			}
			else // merge mirror floor portals
			{
				Vec3V basisX = VECTOR3_TO_VEC3V(m_scanResults->GetMirrorCorners().GetCorner(1)) - basisT;
				Vec3V basisY = VECTOR3_TO_VEC3V(m_scanResults->GetMirrorCorners().GetCorner(3)) - basisT;

				Vec2V minXY(V_ZERO);
				Vec2V maxXY(Mag(basisX), Mag(basisY));

				basisX = Normalize(basisX);
				basisY = Normalize(basisY);

				for (int j = 0; j < 4; j++)
				{
					const Vec3V t = VECTOR3_TO_VEC3V(portalCorners->GetCorner(j)) - basisT;
					const Vec2V v = Vec2V(Dot(t, basisX), Dot(t, basisY));

					minXY = Min(v, minXY);
					maxXY = Max(v, maxXY);
				}

				const Vec3V corner0 = basisT + basisX*minXY.GetX() + basisY*minXY.GetY();
				const Vec3V corner1 = basisT + basisX*maxXY.GetX() + basisY*minXY.GetY();
				const Vec3V corner2 = basisT + basisX*maxXY.GetX() + basisY*maxXY.GetY();
				const Vec3V corner3 = basisT + basisX*minXY.GetX() + basisY*maxXY.GetY();

				m_scanResults->SetMirrorDistance( Min<float>( mirrorDistance, m_scanResults->GetMirrorDistance() ) );
				m_scanResults->GetMirrorCornersRef().SetCorner(0, RCC_VECTOR3(corner0));
				m_scanResults->GetMirrorCornersRef().SetCorner(1, RCC_VECTOR3(corner1));
				m_scanResults->GetMirrorCornersRef().SetCorner(2, RCC_VECTOR3(corner2));
				m_scanResults->GetMirrorCornersRef().SetCorner(3, RCC_VECTOR3(corner3));
			}
		}

		if ( portalSceneNode->GetIsWaterSurface() )
		{
			m_scanResults->SetWaterSurfaceVisible();
		}
	}
	else // not a mirror floor .. track the closest one
	{
		const u8 mirrorPriority = portalSceneNode->GetMirrorPriority();

		if ( m_scanResults->GetMirrorVisible() == false || ( mirrorDistance < m_scanResults->GetMirrorDistance() || m_scanResults->GetMirrorFloorVisible() ) || mirrorPriority > m_scanResults->GetMirrorPriority() )
		{
			m_scanResults->SetMirrorVisible();
			m_scanResults->SetMirrorFloorNotVisible();
			m_scanResults->SetMirrorDistance( mirrorDistance );
			m_scanResults->SetMirrorPriority( mirrorPriority );
			m_scanResults->GetMirrorCornersRef() = *portalCorners;
			m_scanResults->SetMirrorInteriorLocation( portalSceneNode->GetInteriorLocation() );
#if __BANK
			m_debugMirrorPortal = portalSceneNode;
			m_debugMirrorRoom = static_cast< fwRoomSceneGraphNode* >( portalSceneNode->GetNegativePortalEnd() );
#endif // __BANK
		}
	}
}

void fwScanNodes::ProcessInteriorPortalsVisibility(fwInteriorSceneGraphNode* interiorSceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad)
{
	if ( !( cullShape->GetFlags() & CULL_SHAPE_FLAG_RENDER_INTERIOR ) )
		return;

	for (fwSceneGraphNode* child = interiorSceneNode->GetFirstChild(); child != NULL; child = child->GetNext())
	{
		if ( child->IsTypePortal())
		{
			if (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_ONLY_EXTERIOR_PORTALS_IN_PROCESS_INTERIOR)
			{
				fwPortalSceneGraphNode *pPortalNode = static_cast<fwPortalSceneGraphNode*>(child);
				if ((pPortalNode->GetPositivePortalEnd() && pPortalNode->GetPositivePortalEnd()->IsTypeExterior()) || 
					(pPortalNode->GetNegativePortalEnd() && pPortalNode->GetNegativePortalEnd()->IsTypeExterior()))
				{
					AddVisibility( child, cullShape, screenQuad );
				}
			}
			else
			{
				AddVisibility( child, cullShape, screenQuad );
			}
		}
	}
}

void fwScanNodes::ProcessInteriorRoomlessVisibility(fwRoomSceneGraphNode* roomSceneNode, fwInteriorSceneGraphNode* interiorSceneNode, const fwRenderPhaseCullShape* cullShape, ePortalTraversalType traversalType)
{
	if ( !( cullShape->GetFlags() & CULL_SHAPE_FLAG_RENDER_INTERIOR ) )
		return;

	if ( m_visited[traversalType].IsSet( interiorSceneNode->GetIndex() ) )
		return;

	m_beingVisited[traversalType].Set( interiorSceneNode->GetIndex() );
	m_visited[traversalType].Set( interiorSceneNode->GetIndex() );

	for (fwSceneGraphNode* child = interiorSceneNode->GetFirstChild(); child != NULL; child = child->GetNext())
	{
		if ( child->IsTypePortal() )
		{
			AddVisibility( child, cullShape, fwScreenQuad(SCREEN_QUAD_FULLSCREEN) );

			if(roomSceneNode && g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_SCAN_LINKED_PORTALSCENEGRAPHNODE)
			{
				fwPortalSceneGraphNode *portalSceneNode = static_cast<fwPortalSceneGraphNode*>(child);
				const bool traverseOneWay = portalSceneNode->GetIsOneWay() && !(cullShape->GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS);
				fwInteriorSceneGraphNode *otherInterior = NULL;
				if(portalSceneNode->GetIsLinkPortal())
				{
					if ( portalSceneNode->GetNegativePortalEnd() == roomSceneNode )
					{
						if(portalSceneNode->GetPositivePortalEnd() && portalSceneNode->GetPositivePortalEnd()->IsTypeRoom())
						{
							otherInterior = static_cast<fwRoomSceneGraphNode*>(portalSceneNode->GetPositivePortalEnd())->GetOwnerInteriorSceneNode();
						}
					}
					else if ( !traverseOneWay && portalSceneNode->GetPositivePortalEnd() == roomSceneNode )
					{
						if( portalSceneNode->GetNegativePortalEnd() && portalSceneNode->GetNegativePortalEnd()->IsTypeRoom())
						{
							otherInterior = static_cast<fwRoomSceneGraphNode*>(portalSceneNode->GetNegativePortalEnd())->GetOwnerInteriorSceneNode();
						}
					}
				}

				if(otherInterior)
				{
					for (fwSceneGraphNode* otherChild = otherInterior->GetFirstChild(); otherChild != NULL; otherChild = otherChild->GetNext())
					{
						if(otherChild->IsTypePortal())
						{
							fwPortalSceneGraphNode *otherPortalSceneNode = static_cast<fwPortalSceneGraphNode*>(otherChild);
							if( otherPortalSceneNode->GetNegativePortalEnd() == roomSceneNode ||
								otherPortalSceneNode->GetPositivePortalEnd() == roomSceneNode)
							{
								AddVisibility(otherChild, cullShape, fwScreenQuad(SCREEN_QUAD_FULLSCREEN) );
							}
						}
					}
				}
			}
		}
		else if ( child->IsTypeRoom() )
		{
			if ( g_scanDebugFlags & SCAN_DEBUG_DONT_RENDER_LIMBO_ENTITIES )
				continue;

			if ( static_cast< fwRoomSceneGraphNode* >( child )->GetInteriorLocation().GetRoomIndex() == 0 )
				AddVisibility( child, cullShape, fwScreenQuad(SCREEN_QUAD_FULLSCREEN) );
		}
		else
			Assertf( false, "Unexpected scene graph node type" );
	}

	m_beingVisited[traversalType].Clear( interiorSceneNode->GetIndex() );
}

void fwScanNodes::ProcessRoomVisibility(fwRoomSceneGraphNode* roomSceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad, 
										ePortalTraversalType portalTraversalType, float accumulatedPortalOpacity, int decrementedDepth fwScanNodes_DEBUG(int depth))
{
	if ( !( cullShape->GetFlags() & CULL_SHAPE_FLAG_RENDER_INTERIOR ) )
		return;

	if ( m_beingVisited[portalTraversalType].IsSet( roomSceneNode->GetIndex() ) )
		return;

	if ( !(cullShape->GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) && m_visited[portalTraversalType].IsSet( roomSceneNode->GetIndex() ) )
		return;

#if __BANK && !__SPU
	g_scanNodesDebug.DebugDrawIntermediateScreenQuad( cullShape, screenQuad );
#endif // __BANK && !__SPU

	m_beingVisited[portalTraversalType].Set( roomSceneNode->GetIndex() );
	m_visited[portalTraversalType].Set( roomSceneNode->GetIndex() );

	if ( portalTraversalType == PORTAL_TRAVERSAL_TYPE_NORMAL )
	{
		AddVisibility( roomSceneNode, cullShape, screenQuad );

		// All the room 0 nodes 
		if (cullShape == m_gbufCullShape)
		{
			if (m_cascadeShadowsCullShape && m_gBufferRoom0Nodes.GetCount() < GBUFFER_ROOM0_NODES)
			{
				m_gBufferRoom0Nodes.Push(roomSceneNode->GetOwnerInteriorSceneNode());
			}
		}

		ProcessInteriorRoomlessVisibility( roomSceneNode, roomSceneNode->GetOwnerInteriorSceneNode(), cullShape, portalTraversalType );
	}

	if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_TRAVERSE_PORTALS )
	{
		const int	portalCount = PushPortalsFromRoom( cullShape, roomSceneNode );
		TraversePortals( portalCount, roomSceneNode, cullShape, screenQuad, portalTraversalType, accumulatedPortalOpacity, decrementedDepth fwScanNodes_DEBUG(depth) );
	}

	m_beingVisited[portalTraversalType].Clear( roomSceneNode->GetIndex() );
}

void fwScanNodes::ProcessExteriorVisibility(fwExteriorSceneGraphNode* exteriorSceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad,
											ePortalTraversalType portalTraversalType fwScanNodes_DEBUG(int depth))
{
	if ( m_beingVisited[portalTraversalType].IsSet( exteriorSceneNode->GetIndex() ) )
		return;

	if ( !(cullShape->GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS) && m_visited[portalTraversalType].IsSet( exteriorSceneNode->GetIndex() ) )
		return;

	m_beingVisited[portalTraversalType].Set( exteriorSceneNode->GetIndex() );
	m_visited[portalTraversalType].Set( exteriorSceneNode->GetIndex() );

#if __BANK && !__SPU
	g_scanNodesDebug.DebugDrawIntermediateScreenQuad( cullShape, screenQuad );
#endif // __BANK && !__SPU

	for (fwSceneGraphVisitor visitor( exteriorSceneNode ); !visitor.AtEnd(); visitor.Next())
	{
		fwSceneGraphNode*	sceneNode = visitor.GetCurrent();

#if !__SPU && __BANK
		if (*fwScanNodesDebug::GetBreakOnSelectedEntityInContainer() && fwScanNodesDebug::GetSetGraphNodeToBreakOn() == sceneNode )
		{
			__debugbreak();
		}
#endif
		const u32	checkFlags = ( sceneNode->IsTypeExterior() || sceneNode->IsTypeStreamed() ) ? CULL_SHAPE_FLAG_RENDER_EXTERIOR : CULL_SHAPE_FLAG_RENDER_INTERIOR;
		if ( !( cullShape->GetFlags() & checkFlags ) )
			continue;

		if ( sceneNode->IsTypeExterior() )
			AddVisibility( sceneNode, cullShape, screenQuad );
		else if ( sceneNode->IsTypeStreamed() )
		{
			if (cullShape->GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS)
			{
				const spdAABB &aabb = static_cast< fwStreamedSceneGraphNode* >( sceneNode )->GetBoundingBox();
				spdSphere sphere(aabb.GetCenter(), Mag(aabb.GetExtent())); 
				if ( !cullShape->GetSphere().IntersectsSphere(sphere))
					continue;
			}
			else
			{
				if ( cullShape->IsClipped( static_cast< fwStreamedSceneGraphNode* >( sceneNode )->GetBoundingBox() ) )
					continue;
			}

			AddVisibility( sceneNode, cullShape, screenQuad );
		}
		else if ( sceneNode->IsTypeInterior() )
		{
			fwInteriorSceneGraphNode*	interiorSceneNode = static_cast< fwInteriorSceneGraphNode* >( sceneNode );

			if ( interiorSceneNode->GetChildrenCount() == 0 )
				continue;
			
			ProcessInteriorPortalsVisibility( interiorSceneNode, cullShape, screenQuad );
			
			if ( cullShape->GetFlags() & CULL_SHAPE_FLAG_TRAVERSE_PORTALS )
			{
				const int	portalCount = PushPortalsFromExterior( cullShape, interiorSceneNode );
				TraversePortals( portalCount, exteriorSceneNode, cullShape, screenQuad, portalTraversalType, 0.0f, MAX_INT32 fwScanNodes_DEBUG(depth) );
			}

			visitor.SkipChildren();
		}
		else
			Assertf( false, "Unexpected scene graph node type" );
	}

	m_beingVisited[portalTraversalType].Clear( exteriorSceneNode->GetIndex() );
}

void fwScanNodes::ProcessEntityContainers()
{
#if SCAN_NODES_DETAILED_STATS
	PF_FUNC(ProcessEntityContainers);
#endif

	u32		primaryPhaseBit = 0;
	u32		renderIfOnlyPrimaryRendersPhaseBits = 0;
	u32		ppuJobCounter = 0;

	u32 visFlagsStraddlingContainer = 0;
	for (int i = 0; i < static_cast< int >( m_scanBaseInfo->m_cullShapeCount ); ++i)
	{
		const fwRenderPhaseCullShape&	cullShape = m_scanBaseInfo->m_cullShapes[i];

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
			primaryPhaseBit = ( 1 << cullShape.GetRenderPhaseId() );

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_RENDER_ONLY_IF_PRIMARY_RENDERS )
			renderIfOnlyPrimaryRendersPhaseBits |= ( 1 << cullShape.GetRenderPhaseId() );

		visFlagsStraddlingContainer |= ( 1 << cullShape.GetRenderPhaseId() );
	}

	fwExteriorSceneGraphNode* exteriorSceneNode = NULL;

	for(fwSceneGraphVisitor visitor( m_rootSceneNode ); !visitor.AtEnd(); visitor.Next())
	{
		fwSceneGraphNode*	sceneNode = visitor.GetCurrent();
		u32&				visFlags = m_visFlags[ sceneNode->GetIndex() ];

		if ( !( visFlags & primaryPhaseBit ) )
			visFlags &= ~renderIfOnlyPrimaryRendersPhaseBits;

		if (sceneNode->IsTypeExterior())
		{
			exteriorSceneNode = static_cast< fwExteriorSceneGraphNode* >( sceneNode );
		}
		if ( !visFlags )
			continue;

#if __BANK && !__SPU
		g_scanNodesDebug.DebugDrawFinalScreenQuad( sceneNode );
#endif // __BANK && !__SPU

		u32		controlFlags = 0;
		if ( m_renderLodsOnly.IsSet( sceneNode->GetIndex() ) )
			controlFlags |= SCAN_ENTITIES_RENDER_LODS_ONLY;

		if ( sceneNode->IsTypeExterior() )
		{
			if ( !(g_scanDebugFlags & SCAN_DEBUG_DONT_RENDER_EXTERIOR) )
			{
				ProcessEntityContainer( sceneNode, exteriorSceneNode->GetStaticsContainer(), controlFlags | SCAN_ENTITIES_DONT_RUN_ON_PPU, ppuJobCounter );
				ProcessEntityContainer( sceneNode, exteriorSceneNode->GetDynamicsContainer(), controlFlags | SCAN_ENTITIES_DONT_RUN_ON_PPU, ppuJobCounter );
				ProcessEntityContainer( sceneNode, exteriorSceneNode->GetLightsContainer(), controlFlags | SCAN_ENTITIES_DONT_RUN_ON_PPU, ppuJobCounter );
			}
		}
		else if ( sceneNode->IsTypeStreamed() )
		{
			if ( !(g_scanDebugFlags & SCAN_DEBUG_DONT_RENDER_EXTERIOR) )
			{
				fwStreamedSceneGraphNode*	streamedSceneNode = static_cast< fwStreamedSceneGraphNode* >( sceneNode );
				fwBaseEntityContainer*		streamedContainer = streamedSceneNode->GetFirstContainer();
				while ( streamedContainer )
				{
					ProcessEntityContainer( sceneNode, streamedContainer, controlFlags, ppuJobCounter );
					streamedContainer = streamedContainer->GetNext();
				}
			}
		}
		else if ( sceneNode->IsTypeRoom() )
		{
			if ( !(g_scanDebugFlags & SCAN_DEBUG_DONT_RENDER_INTERIOR) )
				ProcessEntityContainer( sceneNode, static_cast< fwRoomSceneGraphNode* >( sceneNode )->GetEntityContainer(), controlFlags | SCAN_ENTITIES_FLAG_INTERIOR, ppuJobCounter );
		}
		else if ( sceneNode->IsTypePortal() )
		{
			if ( !(g_scanDebugFlags & SCAN_DEBUG_DONT_RENDER_INTERIOR) )
			{
				fwPortalSceneGraphNode *portalSceneNode = static_cast<fwPortalSceneGraphNode *>(sceneNode);
				if ((portalSceneNode->GetPositivePortalEnd() && portalSceneNode->GetPositivePortalEnd()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR) || 
					(portalSceneNode->GetNegativePortalEnd() && portalSceneNode->GetNegativePortalEnd()->GetType() == SCENE_GRAPH_NODE_TYPE_EXTERIOR))
				{
					controlFlags |= SCAN_ENTITIES_FLAG_PORTAL_TO_EXTERIOR;
				}

				ProcessEntityContainer( sceneNode, static_cast< fwPortalSceneGraphNode* >( sceneNode )->GetEntityContainer(), controlFlags | SCAN_ENTITIES_FLAG_INTERIOR, ppuJobCounter );
			}
		}
		else
			Assertf( false, "Unexpected scene graph node type" );
	}

	if (exteriorSceneNode && exteriorSceneNode->GeStradlingPortalsContainer()->GetEntityCount() > 0)
	{
		m_visFlags[exteriorSceneNode->GetIndex()] = visFlagsStraddlingContainer;
		ProcessEntityContainer( exteriorSceneNode, exteriorSceneNode->GeStradlingPortalsContainer(), SCAN_ENTITIES_FLAG_STRADDLING_PORTALS_CONTAINER | SCAN_ENTITIES_FLAG_INTERIOR, ppuJobCounter );
	}
}

bool fwScanNodes::Run()
{
	fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_scanNodesTimer) );

	m_currentScanEntitiesDependency = 0;

	//---------- Clear persisted visibility data ---------- //
	
	ClearVisData();
	m_currentScreenQuadStorage = 0;
	m_screenQuads = reinterpret_cast< fwScreenQuad* >( m_screenQuadsStorage[ m_currentScreenQuadStorage ] );
	m_screenQuadPairs = reinterpret_cast< fwScreenQuadPair* >( m_screenQuadsStorage[ m_currentScreenQuadStorage ] );

	//---------- Get shortcut pointers to useful render phases ----------//

	m_gbufCullShape = NULL;
	m_cascadeShadowsCullShape = NULL;

	for (int i = 0; i < static_cast< int >( m_scanBaseInfo->m_cullShapeCount ); ++i)
	{
		const fwRenderPhaseCullShape&	cullShape = m_scanBaseInfo->m_cullShapes[i];

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
			m_gbufCullShape = &cullShape;
		else if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CASCADE_SHADOWS )
			m_cascadeShadowsCullShape = &cullShape;
	}

	Assert(m_gbufCullShape); // this can't be NULL (but m_cascadeShadowsCullShape can be ..)

	m_primaryCameraPosition = VEC3V_TO_VECTOR3( m_gbufCullShape->GetCameraPosition() );

	//---------- Perform the visibility scene graph traversal for each phase ----------//
#if PORTAL_TRAVERSAL_DEBUGGING
	g_scanNodesDebug.ShowDebugShadowIsolatedPortalIndex();
#endif
	

#if __BANK && !__SPU
	g_scanNodesDebug.VisibilityBegin( this );
#endif // __BANK && !__SPU

	m_dontRenderExterior = false;

	for (int i = 0; i < static_cast< int >( m_scanBaseInfo->m_cullShapeCount ); ++i)
	{
		const fwRenderPhaseCullShape&	cullShape = m_scanBaseInfo->m_cullShapes[i];
		fwSceneGraphNode*				startingSceneNode = cullShape.GetStartingSceneNode();

		if (!Verifyf(startingSceneNode, "The starting scene graph node for render phase id %d is NULL", cullShape.GetRenderPhaseId()))
		{
			continue;
		}
		ClearTraversalData( cullShape );

		ePortalTraversalType portalTraversalType = PORTAL_TRAVERSAL_TYPE_NORMAL;

		if ( (cullShape.GetFlags() & CULL_SHAPE_FLAG_MIRROR) && (cullShape.GetFlags() & CULL_SHAPE_FLAG_TRAVERSE_PORTALS)
			BANK_ONLY( && !( g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_DISABLE_MIRROR_FLIP)) )
		{
			portalTraversalType = PORTAL_TRAVERSAL_TYPE_MIRROR;
		}

		if (startingSceneNode->IsTypeExterior())
		{
			ProcessExteriorVisibility(static_cast< fwExteriorSceneGraphNode* >(startingSceneNode), &cullShape, fwScreenQuad(SCREEN_QUAD_FULLSCREEN),
									  portalTraversalType fwScanNodes_DEBUG(0));
		}
		else if ( startingSceneNode->IsTypeRoom() )
		{
			m_scanResults->m_startingSceneNodeInInterior |= 1 << cullShape.GetRenderPhaseId();

			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_REFLECTIONS )
				m_scanResults->m_skyPortals_ParaboloidReflection.SetIsSkyPortalVisInterior();
			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_MIRROR )
				m_scanResults->m_skyPortalBounds_MirrorReflection.SetIsSkyPortalVisInterior();
			
			fwRoomSceneGraphNode *roomStartingSceneGraphNode = static_cast< fwRoomSceneGraphNode *>(startingSceneNode);
			if (cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY)
			{
				m_dontRenderExterior = roomStartingSceneGraphNode->GetExteriorDisabled();
			}

			m_scanResults->SetDirectionaLightEnabledForRoom((SCAN_DEBUG_EXT_ENABLE_DIRECTIONAL_LIGHT_FOR_ROOM & g_scanDebugFlagsExtended) || roomStartingSceneGraphNode->GetDirectionalLightEnabled());

			int exteriorVisibilityDepth = roomStartingSceneGraphNode->GetExteriorVisibilityDepth();
			if (g_scanDebugFlagsPortals & SCAN_PORTALS_DEBUG_OVERRIDE_ROOM_EXTERIOR_VISIBILITY_DEPTH)
			{
				exteriorVisibilityDepth = g_scanDebugOverrideRoomExteriorVisibilityDepth;
			}
			
			exteriorVisibilityDepth = exteriorVisibilityDepth == -1 ? MAX_INT32 : exteriorVisibilityDepth;
			ProcessRoomVisibility( roomStartingSceneGraphNode, &cullShape, fwScreenQuad(SCREEN_QUAD_FULLSCREEN), portalTraversalType, 0.0f, exteriorVisibilityDepth - 1 fwScanNodes_DEBUG(0) );
		}
		else
		{
			Assertf( false, "The starting scene graph node for render phase id %d is not a room or the exterior its %d",  cullShape.GetRenderPhaseId(), startingSceneNode->GetType());
		}

		//---------- In case we traversed portals for this phase, post the data to main memory ----------//

		if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_CLIP_AND_STORE_PORTALS )
		{
			if ( cullShape.GetFlags() & CULL_SHAPE_FLAG_PRIMARY )
			{
				const bool	gbufUsesPairs = (cullShape.GetFlags() & CULL_SHAPE_FLAG_USE_SCREEN_QUAD_PAIRS) ? true : false; 
				m_gbufExteriorScreenQuad = gbufUsesPairs ? m_screenQuadPairs[0].Union() : m_screenQuads[0];
				
				m_scanResults->SetGbufUsesScreenQuadPairs( gbufUsesPairs );
				m_scanResults->SetExteriorGbufScreenQuad( m_gbufExteriorScreenQuad );
			}

#if __SPU
			sysDmaWait( 1 << g_auxDmaTag );
			sysDmaPut( m_screenQuadsStorage[ m_currentScreenQuadStorage ],
				CELL_SPURS_PPU_SYM(g_screenQuadStorage) + sizeof(Vec4V) * STORED_SCREEN_QUAD_COUNT * cullShape.GetScreenQuadStorageId(),
				sizeof(Vec4V) * STORED_SCREEN_QUAD_COUNT,
				g_auxDmaTag );
#else
			memcpy( g_screenQuadStorage[ cullShape.GetScreenQuadStorageId() ], m_screenQuadsStorage[ m_currentScreenQuadStorage ], sizeof(Vec4V) * STORED_SCREEN_QUAD_COUNT );
#endif

			m_currentScreenQuadStorage = 1 - m_currentScreenQuadStorage;
			m_screenQuads = reinterpret_cast< fwScreenQuad* >( m_screenQuadsStorage[ m_currentScreenQuadStorage ] );
			m_screenQuadPairs = reinterpret_cast< fwScreenQuadPair* >( m_screenQuadsStorage[ m_currentScreenQuadStorage ] );
		}
	}

	// Since the starting scene node will never use anything other than a max (ie fullscreen quad) for culling we don't need to update the screen quad array
	if (m_cascadeShadowsCullShape && !m_scanResults->IsEverythingInShadow() && (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_ADD_CURRENT_ROOM_AND_ROOM0_IF_CSM_ACTIVE))
	{
		// Add the current room.  This probably isn't needed and if it the shell or shadow proxy should be moved to room 0
		fwSceneGraphNode* gBuffStartingSceneNode = m_gbufCullShape->GetStartingSceneNode();
		if (gBuffStartingSceneNode->IsTypeRoom())
		{
 			fwRoomSceneGraphNode *roomStartingSceneGraphNode = static_cast< fwRoomSceneGraphNode *>(gBuffStartingSceneNode);
			AddVisibility( roomStartingSceneGraphNode, m_cascadeShadowsCullShape, fwScreenQuad(SCREEN_QUAD_FULLSCREEN) );
		}

		// Add all the GBuffer room 0 nodes
		for (int i = 0; i < m_gBufferRoom0Nodes.GetCount(); ++i)
		{
			ProcessInteriorRoomlessVisibility( NULL, m_gBufferRoom0Nodes[i], m_cascadeShadowsCullShape, PORTAL_TRAVERSAL_TYPE_NORMAL );
		}
	}

#if __BANK && !__SPU
	g_scanNodesDebug.VisibilityEnd();
#endif // __BANK && !__SPU

	//---------- Compute the exterior restricted shadow frustum ----------//

	if ( g_scanDebugFlags & SCAN_DEBUG_USE_EXTERIOR_RESTRICTED_FRUSTUM )
	{
		Vec4V						planes[ CASCADE_SHADOWS_SCAN_NUM_PLANES_EXCESS ];
		fwScanCascadeShadowInfo		shadowInfoCopy = m_scanBaseInfo->m_cascadeShadowInfo;
		fwScanCSMSortedPlaneInfo	spi;
		fwScreenQuad				gbufQuad = m_gbufExteriorScreenQuad;

		shadowInfoCopy.UpdateShadowViewport( planes, &spi, gbufQuad.ToVec4V() );
		memcpy( m_scanResults->GetExteriorShadowFrustumPlanesPtr(), shadowInfoCopy.GetTransposedPlanesPtr(), sizeof(Vec4V) * CASCADE_SHADOWS_SCAN_NUM_PLANES );
	}

	//---------- Fixup visibility global results ----------//

	SPU_ONLY( m_scanResults->UnfixupPointers( fwSceneGraph::GetPointerFixer() ); )

#if !__SPU && __BANK
		if (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_SNAPSHOT_FRUSTUM)
		{
			fwScanNodesDebug::SnapShotDebugFrutumData(this);
		}

		if (g_scanDebugFlagsExtended & SCAN_DEBUG_EXT_DRAW_FRUSTUMS)
		{
			fwScanNodesDebug::DrawSnapShotFrustums();
		}
#endif

	//---------- Spawn children jobs ----------//

#if __SPU
		sysDmaPutAndWait( m_scanResults, CELL_SPURS_PPU_SYM(g_pendingScanResults), sizeof(fwScanResults), g_auxDmaTag );
#endif

	ProcessEntityContainers();

	return true;
}

__forceinline u32 IsBoxVisibleInSweptFrustum(const Vec4V m_transposedPlanes[CASCADE_SHADOWS_SCAN_NUM_PLANES],  const spdAABB& box)
{
	const Vec3V bmin = box.GetMin();
	const Vec3V bmax = box.GetMax();

	const Vec4V xmin = Vec4V(SplatX(bmin));
	const Vec4V ymin = Vec4V(SplatY(bmin));
	const Vec4V zmin = Vec4V(SplatZ(bmin));
	const Vec4V xmax = Vec4V(SplatX(bmax));
	const Vec4V ymax = Vec4V(SplatY(bmax));
	const Vec4V zmax = Vec4V(SplatZ(bmax));

	const Vec4V zero(V_ZERO);

	// note that we could precalculate the IsLessThanOrEqual for the transposed planes

	Vec4V d0 = m_transposedPlanes[3];
	d0 = AddScaled(d0, m_transposedPlanes[0], SelectFT(IsLessThanOrEqual(m_transposedPlanes[0], zero), xmin, xmax));
	d0 = AddScaled(d0, m_transposedPlanes[1], SelectFT(IsLessThanOrEqual(m_transposedPlanes[1], zero), ymin, ymax));
	d0 = AddScaled(d0, m_transposedPlanes[2], SelectFT(IsLessThanOrEqual(m_transposedPlanes[2], zero), zmin, zmax));

	for (int i = 4; i < CASCADE_SHADOWS_SCAN_NUM_PLANES; i += 4)
	{
		Vec4V d1 = m_transposedPlanes[3 + i];
		d1 = AddScaled(d1, m_transposedPlanes[0 + i], SelectFT(IsLessThanOrEqual(m_transposedPlanes[0 + i], zero), xmin, xmax));
		d1 = AddScaled(d1, m_transposedPlanes[1 + i], SelectFT(IsLessThanOrEqual(m_transposedPlanes[1 + i], zero), ymin, ymax));
		d1 = AddScaled(d1, m_transposedPlanes[2 + i], SelectFT(IsLessThanOrEqual(m_transposedPlanes[2 + i], zero), zmin, zmax));
		d0 = Max(d0, d1);
	}

	return IsLessThanOrEqualAll(d0, zero);
}

} // namespace rage

#endif // !__TOOL
