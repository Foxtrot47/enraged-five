#ifndef _INC_SCAN_H_
#define _INC_SCAN_H_

#if !__TOOL
// framework headers
#include "fwscene/world/EntityDesc.h"
#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/world/InteriorLocation.h"
#include "fwscene/scan/ScanEntities.h"

namespace rage {

class fwScanCascadeShadowInfo;
class bkGroup;

/*
PURPOSE:

This is the entry point for the visibility system. Its main features are:

 -	It is entirely job-based, with a master job (ScanNodes) traversing the entity container hierarchy and
	spawning children jobs as needed to process the visibility of entities.

 -	The master job performs quad tree node culling for exteriors and portal traversal for interiors.

 -	The visibility algorithm for exterior entities has the following stages:
		* Early rejection (simple distance-based culling)
		* Renderphase-specific exclusion (based on entity type, LOD level, etc)
		* Frustum clipping
		* Geometry-based occlusion (soon to be phased out)
		* Rasterization-based occlusion

 -	The visibility algorithm for interior entities has the following stages:
		* Renderphase-specific exclusion (based on entity type, LOD level, etc)
		* Frustum clipping against the frusta produced by the portal traversal algorithm

Visibility stages are (usually) carried out independently for each phase.
*/

class fwScan
{
private:

	static fwSceneGraphNode*				ms_rootSceneNode;
	static Vector3							ms_primaryCameraPosition;
	static float							ms_primaryLodScale;
	static fwInteriorLocation				ms_initialInteriorLocation;
	static Vec4V*							ms_hiZBuffers[ SCAN_OCCLUSION_STORAGE_COUNT ];
	static bool								ms_scanStarted;

	static void SortCullShapes();
	static void StartScanDependencies();

public:

#if __BANK
	static char		ms_processVisibilityOnPpuText[32];
	static bool		ms_forceVisibilityOnPpu;
	static bool		ms_forceEnableOcclusion;
	static bool		ms_forceDisableOcclusion;
	static bool		ms_forceEnableShadowOcclusion;
	static bool		ms_forceDisableShadowOcclusion;

	static bkGroup*	ms_shadowProxyGroup;
	static bkGroup*	ms_reflectionProxyGroup;

	static void InitWidgets();
	static void InitOptimizationWidgets();
	static void UpdateDebug()	{ }
	static void DrawDebug()		{ }
	static void UpdateForceProcessVisibilityOnPpu();
	static void UpdateForceEnableOcclusion();
#endif // __BANK
	static void InitClass();
	static void ShutDownClass();

	// PURPOSE:	Sets the root of the container hierarchy
	static void SetRootContainer(fwSceneGraphNode* value)					{ ms_rootSceneNode = value; }

	// PURPOSE: Sets the LOD scale to be used for early rejection - it changes when using spinner scopes, etc
	static void SetPrimaryLodScale(const float value)						{ ms_primaryLodScale = value; }

	// PURPOSE: Sets the interior location where the visibility is started. It is usually the same as the
	//			location of the main camera
	static void SetInitialInteriorLocation(const fwInteriorLocation value)	{ ms_initialInteriorLocation = value; }

	// PURPOSE:	Sets the Z buffer to be used for raster-based occlusion
	static void SetHiZBuffer(const int occluderPhase, Vec4V* value)			{ ms_hiZBuffers[ occluderPhase ] = value; }

	// PURPOSE:	Resets the cull shapes
	static void ResetCullShapes();

	// PURPOSE:	Add a cull shape for the next visibility run
	static fwRenderPhaseCullShape& AddCullShape();

	// PURPOSE:	Gets a reference to scan workload (containing global visibility data about the render phases,
	//			shadows culling, software rasterizer, and other)
	static fwScanBaseInfo& GetScanBaseInfo();

	static const fwScanCascadeShadowInfo& GetScanCascadeShadowInfo()		{ return GetScanBaseInfo().m_cascadeShadowInfo; }

	// PURPOSE:	Gets a reference to the structure filled with info about the visibility outcome as a whole
	static fwScanResults& GetScanResults();

	static bool GetGbufUsesScreenQuadPairs();
	static fwScreenQuadPair GetGbufScreenQuadPair(const fwSceneGraphNode* sceneNode);
	static fwScreenQuad GetGbufScreenQuad(const fwSceneGraphNode* sceneNode);

	// PURPOSE:	Starts the visibility processing
	static void StartScan();

	// PURPOSE:	Waits for the visibility processing to finish
	static void JoinScan();

	// PURPOSE: Initialize (not run) the visibility dependencies
	static void SetupDependencies();

	static fwScreenQuad GetGBuffExteriorScreenQuad();
};

} // namespace rage

#endif // !__TOOL

#endif // !defined _INC_SCAN_H_
