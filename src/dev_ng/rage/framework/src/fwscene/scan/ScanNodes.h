#ifndef _INC_SCANNODES_H_
#define _INC_SCANNODES_H_

#include "atl/bitset.h"
#include "system/dependencyscheduler.h"

#include "fwscene/scan/RenderPhaseCullShape.h"
#include "fwscene/scan/ScanCascadeShadows.h"
#include "fwscene/scan/ScanResults.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwscene/scan/ScreenQuad.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"

namespace rage {

#if 1
#define fwScanNodes_DEBUG(x) ,x
#else
#define fwScanNodes_DEBUG(x)
#endif

class fwScanNodes
{
public:

	enum
	{
		SCRATCH_BUFFER_SIZE			= 50 * 1024,
		PORTAL_STACK_SIZE			= 192,
		GBUFFER_ROOM0_NODES         = 8,
		STORED_SCREEN_QUAD_COUNT	= fwSceneGraph::MAX_SCENE_NODE_COUNT - fwSceneGraph::FIRST_ROOM_SCENE_NODE_INDEX + 1,
	};

	enum ePortalTraversalType
	{
		PORTAL_TRAVERSAL_TYPE_NORMAL,
		PORTAL_TRAVERSAL_TYPE_MIRROR, // when doing visibility for mirror, first traverse with flipped portals
		PORTAL_TRAVERSAL_TYPE_COUNT
	};

private:

	friend class fwScanNodesDebug;

	struct PortalEntry
	{
		fwPortalSceneGraphNode*		m_portal;
		fwSceneGraphNode*			m_dest;
	};

	fwScreenQuad					m_gbufExteriorScreenQuad;
	Vector3							m_primaryCameraPosition;			// Camera position for early rejection
	atFixedArray< PortalEntry, PORTAL_STACK_SIZE > m_portalStack;

	const fwScanBaseInfo*			m_scanBaseInfo;

	fwSceneGraphNode*				m_rootSceneNode;					// Root of the scene graph

	fwScanResults*					m_scanResults;

	sysEaPtr<Vec4V>					m_hiZBufferEa[ SCAN_OCCLUSION_STORAGE_COUNT ];

	u32*							m_visFlags;
	Vec4V*							m_screenQuadsStorage[2];
	atUserBitSet					m_beingVisited	[ PORTAL_TRAVERSAL_TYPE_COUNT ];
	atUserBitSet					m_visited		[ PORTAL_TRAVERSAL_TYPE_COUNT ];
	atUserBitSet					m_renderLodsOnly;
	u8*								m_scratchBufferGuard;

	fwScreenQuad*					m_screenQuads;
	fwScreenQuadPair*				m_screenQuadPairs;

	const fwRenderPhaseCullShape*	m_gbufCullShape;
	const fwRenderPhaseCullShape*	m_cascadeShadowsCullShape;
#if __BANK
	const fwPortalSceneGraphNode*	m_debugMirrorPortal;
	const fwRoomSceneGraphNode*		m_debugMirrorRoom;
#endif // __BANK
	s8								m_currentScreenQuadStorage;
	bool						    m_dontRenderExterior;

	u32								m_currentScanEntitiesDependency;
	atFixedArray < fwInteriorSceneGraphNode *, GBUFFER_ROOM0_NODES> m_gBufferRoom0Nodes;

	void ClearVisData();
	void ClearTraversalData(const fwRenderPhaseCullShape& cullShape);
	
	void ProcessEntityContainer(const fwSceneGraphNode* sceneNode, const fwBaseEntityContainer* entityContainer, const u32 controlFlags, u32 &ppuJobCounter);
	void AddVisibility(fwSceneGraphNode* sceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad);
	int PushPortalsFromRoom(const fwRenderPhaseCullShape* cullShape, const fwRoomSceneGraphNode* roomSceneNode);
	int PushPortalsFromExterior(const fwRenderPhaseCullShape* cullShape, const fwInteriorSceneGraphNode* interiorSceneNode);
	void TraversePortals(const int portalCount, const fwSceneGraphNode* sceneNode, const fwRenderPhaseCullShape* cullShape, 
						const fwScreenQuad& screenQuad, ePortalTraversalType portalTraversalType, float accumulatedPortalOpacity,
						int decrementedDepth  fwScanNodes_DEBUG(int depth));

	void ProcessMirror(Vec3V_In camPos, const fwPortalSceneGraphNode* portalSceneNode, const fwPortalCorners* portalCorners);

	void ProcessInteriorPortalsVisibility(fwInteriorSceneGraphNode* interiorSceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad);

	void ProcessInteriorRoomlessVisibility(fwRoomSceneGraphNode* roomSceneNode, fwInteriorSceneGraphNode* interiorSceneNode, const fwRenderPhaseCullShape* cullShape, ePortalTraversalType traversalType);

	void ProcessRoomVisibility(fwRoomSceneGraphNode* roomSceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad, 
							   ePortalTraversalType portalTraversalType, float accumulatedPortalOpacity, int decrementedDepth fwScanNodes_DEBUG(int depth));

	void ProcessExteriorVisibility(fwExteriorSceneGraphNode* exteriorSceneNode, const fwRenderPhaseCullShape* cullShape, const fwScreenQuad& screenQuad,
								    ePortalTraversalType portalTraversalType fwScanNodes_DEBUG(int depth));

	void ProcessEntityContainers();

public:

	static s16 GetStoredScreenQuadIndex(const fwSceneGraphNode* sceneNode);

	void SetScanBaseInfo(const fwScanBaseInfo* value)							{ m_scanBaseInfo = value; }
	void SetScanResults(fwScanResults* value)									{ m_scanResults = value; }
	void SetRootSceneNode(fwSceneGraphNode* value)								{ m_rootSceneNode = value; }
	void SetHiZBufferEa(const int occluderPhase, const sysEaPtr<Vec4V> value)	{ m_hiZBufferEa[ occluderPhase ] = value; }

	u32 AssignScratchBuffer(u8* memory, const u32 size);
	void CheckScratchBufferGuard();

	static bool RunFromDependency(const sysDependency& dependency);
	bool Run();
};

} // namespace rage

#endif // !defined _INC_SCANNODES_H_
