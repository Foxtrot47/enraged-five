#include "fwscene/scan/ScanDebug.h"

#if __BANK
#include "atl/hashstring.h"
#include "entity/entity.h"
#include "fwdebug/picker.h"
#include "grcore/debugdraw.h"
#include "spatialdata/aabb.h"
#include "string/string.h"

rage::u32			g_scanDebugFlags = SCAN_DEBUG_DEFAULT_FLAGS;
rage::u32			g_scanDebugFlagsPortals = SCAN_PORTALS_DEBUG_DEFAULT_FLAGS;
rage::u32			g_scanDebugShowPortalGraph = SCAN_DEBUG_SHOW_PORTAL_TRAVERSAL_GRAPH_NONE;
rage::u32			g_scanDebugFlagsExtended = SCAN_DEBUG_EXT_DEFAULT_FLAGS;
float				g_scanDebugOverridePortalOpacity = 0.0f;
int					g_scanDebugOverrideRoomExteriorVisibilityDepth = -1;
rage::u16		    g_scanPhaseVisibilityMaskOverride = 0;

bool g_displyPedsMissingGfx = false;
bool g_logPedsMissingGfx = false;

struct MissingGfxPed
{
	rage::atNonFinalHashString m_name;
	void* m_entityAddress;
	rage::spdAABB m_aabb;
	bool m_missingShader;
};

rage::atArray<MissingGfxPed> g_missingGfxPeds;
void ScanDebugAddPedMissingGfx(rage::u32 name, void* entityAddress, const rage::spdAABB &aabb, bool missingShader)
{
	if ((!g_displyPedsMissingGfx && !g_logPedsMissingGfx) || g_missingGfxPeds.GetCount() > 32)
	{
		return;
	}
	// Search list for existing entry
	bool found = false;
	for (int i = 0; i < g_missingGfxPeds.GetCount(); ++i)
	{
		if (entityAddress == g_missingGfxPeds[i].m_entityAddress)
		{
			found = true;
			break;
		}
	}

	if (!found)
	{
		MissingGfxPed & mPed = g_missingGfxPeds.Grow();
		mPed.m_name.SetHash(name); 
		mPed.m_entityAddress = entityAddress;
		mPed.m_aabb = aabb;
		mPed.m_missingShader = missingShader;
	}
}

void DrawPedsMissingGfx()
{
	if ((g_displyPedsMissingGfx || g_logPedsMissingGfx) && g_missingGfxPeds.GetCount())
	{
		const char *pPedFailedMessage = "Peds that failed to render due to missing CPedStreamRenderGfx";
		if (g_displyPedsMissingGfx)
		{
			rage::grcDebugDraw::AddDebugOutput(pPedFailedMessage);
		}
#if !__SPU
		if (g_logPedsMissingGfx)
		{
			Displayf("%s",pPedFailedMessage);
		}
#endif
		for (int i = 0; i < g_missingGfxPeds.GetCount(); ++i)
		{
			rage::atNonFinalHashString hashString(g_missingGfxPeds[i].m_name);
			void* address = g_missingGfxPeds[i].m_entityAddress;
			const rage::spdAABB &aabb = g_missingGfxPeds[i].m_aabb;
			bool missingShader = g_missingGfxPeds[i].m_missingShader;
			
			char missingPedInfo[128] = "";
			rage::formatf(missingPedInfo, 127, "Model :%s  Entity Address: 0x%p%s", hashString.GetCStr(), address, missingShader ? ", Missing Shader Effect" : "");

			if (g_displyPedsMissingGfx)
			{
				rage::grcDebugDraw::AddDebugOutput(missingPedInfo);
				rage::grcDebugDraw::BoxAxisAligned(aabb.GetMin(), aabb.GetMax(), rage::Color32(255, 255, 0, 255), false);
			}
#if !__SPU
			if (g_logPedsMissingGfx)
			{
				Displayf("%s",missingPedInfo);
				Displayf("Bounding Volume Min %.2f, %.2f, %.2f, Max %.2f, %.2f, %.2f", aabb.GetMin().GetXf(), aabb.GetMin().GetYf(), aabb.GetMin().GetZf(),
																	  				   aabb.GetMax().GetXf(), aabb.GetMax().GetYf(), aabb.GetMax().GetZf());
			}
#endif		
		}
	}
	g_missingGfxPeds.Reset();
}
#if !__SPU
void SetPhaseVisibilityMaskOverrideOnSelectedEntity()
{
	rage::fwEntity *pEntity = g_PickerManager.GetSelectedEntity();
	if (pEntity)
	{
		pEntity->SetRenderPhaseVisibilityMask(g_scanPhaseVisibilityMaskOverride);
		rage::fwBaseEntityContainer *pContainer = pEntity->GetOwnerEntityContainer();
		if (pContainer)
		{
			if (pContainer->IsTypeDefault())
			{
				static_cast<rage::fwEntityContainer*>(pContainer)->UpdateEntityDescriptorInNodeInternal(pEntity);

				rage::fwEntityContainer *pEntityContainer = static_cast<rage::fwEntityContainer*>(pContainer);
				for (rage::u32 i = 0; i < pEntityContainer->GetEntityCount(); i++)
				{
					if ( pEntityContainer->GetEntity(i) == pEntity )
					{
						pEntityContainer->GetEntityDesc(i)->SetRenderPhaseVisibilityMask(g_scanPhaseVisibilityMaskOverride);
						return;
					}
				}
				Assertf(false, "Failed to set Render Phase Visibility Mask for selected entity");
			}
			else if (pContainer->IsTypeFixed())
			{
				rage::fwFixedEntityContainer *pFixedContainer = static_cast<rage::fwFixedEntityContainer*>(pEntity->GetOwnerEntityContainer());
				pFixedContainer->GetEntityDescForEntity(pEntity)->SetRenderPhaseVisibilityMask(g_scanPhaseVisibilityMaskOverride);				
			}
			else if (pContainer->IsTypeSoA())
			{
				rage::fwSoAEntityContainer *pSOAContainer = static_cast<rage::fwSoAEntityContainer*>(pEntity->GetOwnerEntityContainer());
				rage::fwEntityDescSoA soa = rage::fwSoAEntityContainer::InterpretAsEntityDescSoa( pSOAContainer->GetEntityDescArray(), pSOAContainer->GetEntityCount() );

				for (rage::s32 i = 0; i < soa.m_blockCount; i++)
				{
					rage::fwEntityDesc descs[4];
					rage::fwSoAEntityContainer::GetEntityDescBlock(soa, i, descs[0], descs[1], descs[2], descs[3]);

					for (int j = 0; j < 4; j++)
					{
						if (descs[j].GetEntity() == pEntity )
						{
							descs[j].SetRenderPhaseVisibilityMask(g_scanPhaseVisibilityMaskOverride);
							
							rage::fwSoAEntityContainer::SetEntityDescBlock(soa, i, descs[0], descs[1], descs[2], descs[3]);
							return;
						}
					}
				}
				Assertf(false, "Failed to set Render Phase Visibility Mask for selected entity");
			}			
		}
	}

}
void GetPhaseVisibilityMaskOnSelectedEntity()
{
	rage::fwEntity *pEntity = g_PickerManager.GetSelectedEntity();
	if (pEntity)
	{
		g_scanPhaseVisibilityMaskOverride = pEntity->GetRenderPhaseVisibilityMask();
	}
	
}
#endif //#if !__SPU

#if SCL_LOGGING
#include "fwscene/world/EntityContainer.h"
rage::u32		    g_spuLoggingAddress;
rage::u32			g_currentLogIndex = 0;
rage::u32			g_currentFrameIndex = 0;
extern rage::u32	g_occlusionSyncTaskCounter;

#if __PPU
int ScanLogCompareEntDesP(const void *pA, const void *pB)
{
	ScanLog *pSLA = (ScanLog *)pA;
	ScanLog *pSLB = (ScanLog *)pB;
	return pSLA->m_pEntityDesc - pSLB->m_pEntityDesc;
}
void ScanLogOutputSpuInfo()
{
	volatile u32 *pCurrentLogIndex = &g_currentLogIndex;

	u32 startIndex = g_currentFrameIndex * SPU_LOGGING_PER_FRAME_SIZE;
	u32 endIndex   = *pCurrentLogIndex;
	ScanLog *pLogData = (ScanLog *)g_spuLoggingAddress;

	int scanNodesBeginFound = -1; 
	int scanNodesEndFound   = -1; 

	bool allJobsComplete = true;

	int completedSEJobCount = 0;

	int occWrapBeginFound = -1;
	int occWrapEndFound  = -1;

	int occSyncBeginFound = -1;
	int occSyncEndFound  = -1;

	int occSyncBeginFoundCount = 0;
	int occSyncEndFoundCount  = 0;


	// Count number of completed scan entities jobs
	for (rage::u32 i = startIndex; i < endIndex; ++i)
	{
		u32 flags = pLogData[i].m_flags;
		if ((flags & SCL_SCAN_ENITIES) && (flags & SCL_END))
		{
			++completedSEJobCount;
		}

		if (flags & SCL_SCAN_NODES)
		{
			if (flags & SCL_BEGIN)
			{
				scanNodesBeginFound = i;
			}
			else if (flags & SCL_END)
			{
				scanNodesEndFound = i;
			}
			else if (!(flags & SCL_ADD_JOB_SE))
			{
				Assertf(0, "SCL_SCAN_NODES flag was set but neither begin nor end was");
			}
		}

		if (flags & SCL_SCAN_OCCLUSIONWRAPPER)
		{
			if (flags & SCL_BEGIN)
			{
				occWrapBeginFound = i;
			}
			else if (flags & SCL_END)
			{
				occWrapEndFound = i;
			}
			else
			{
				Assertf(0, "SCL_SCAN_OCCLUSIONWRAPPER flag was set but neither begin nor end was");
			}
		}

		if (flags & SCL_SCAN_OCCLUSIONSYNC)
		{
			if (flags & SCL_BEGIN)
			{
				occSyncBeginFound = i;
				++occSyncBeginFoundCount;
			}
			else if (flags & SCL_END)
			{
				occSyncEndFound = i;
				++occSyncEndFoundCount;
			}
			else
			{
				Assertf(0, "SCL_SCAN_OCCLUSIONSYNC flag was set but neither begin nor end was");
			}
		}

	}


	// Check begin and end was called for scan nodes
	if (scanNodesBeginFound == -1 || scanNodesEndFound == -1)
	{
		Displayf("ScanLog: Error Scan Nodes Begin %s, Error Scan Nodes End %s", 
				  scanNodesBeginFound != -1 ? "found" : "not found", scanNodesEndFound != -1 ? "found" : "not found");
	}
	else
	{
		// Scan Nodes End stores the total number of jobs added in m_entityCount
		if (completedSEJobCount !=  pLogData[scanNodesEndFound].m_entityCount)
		{
			Displayf("ScanLog: Error Not all Scan Entities Jobs completed.  Jobs added %d, Jobs completed %d", 
					pLogData[scanNodesEndFound].m_entityCount, completedSEJobCount);

			allJobsComplete = false;
		}

	}

	// Check begin and end was called for occlusion sync
	if (occSyncBeginFound == -1 || occSyncEndFound == -1)
	{
		Displayf("ScanLog: Error Scan Occlusion Sync Begin %s, Error Scan Occlusion Sync End %s", 
			occSyncBeginFound != -1 ? "found" : "not found", occSyncEndFound != -1 ? "found" : "not found");
	}
	else
	{
		if (occSyncBeginFoundCount != 1 || occSyncEndFoundCount != 1)
		{
			Displayf("ScanLog: Error Scan Occlusion Sync ran multiple times %d", occSyncBeginFoundCount);
		}
		else
		{
			Displayf("ScanLog: Scan Occlusion Sync UserDataCount %d", pLogData[occSyncBeginFound].m_entityCount);
			Displayf("ScanLog: g_occlusionSyncTaskCounter %d, counter in sync job %d", g_occlusionSyncTaskCounter, pLogData[occSyncEndFound].m_entityCount);
		}
	}


	// Check begin and end was called for occlusion wrapper
	if (occWrapBeginFound == -1 || occWrapEndFound == -1)
	{
		Displayf("ScanLog: Error Scan Occlusion Wrapper Begin %s, Error Scan Occlusion Wrapper End %s", 
				occWrapBeginFound != -1 ? "found" : "not found", occWrapBeginFound != -1 ? "found" : "not found");
	}
	else
	{
		Displayf("ScanLog: Scan Occlusion Wrapper completed adding %d jobs, %d for SPU and %d for PPU", 
				  pLogData[occWrapEndFound].m_entityCount, pLogData[occWrapEndFound].m_pEntityDesc,
				  pLogData[occWrapEndFound].m_entityCount - pLogData[occWrapEndFound].m_pEntityDesc);
	}

	Displayf("ScanLog: Early Kick Flags %u", g_scanDebugFlags);
	Displayf("ScanLog: Early Kick Occlusion %s", (g_scanDebugFlags | SCAN_DEBUG_EARLY_KICK_OCCLUSION_JOBS) ? "yes" : "no");

	// Sort the data
	int sortStartIndex = startIndex;
	qsort(&pLogData[sortStartIndex], endIndex - sortStartIndex, sizeof(ScanLog), ScanLogCompareEntDesP);

	for (rage::u32 i = startIndex; i < endIndex; ++i)
	{
		u32 flags = pLogData[i].m_flags;
		const char *pJobType = "";
		if (flags & SCL_SCAN_NODES)
		{
			pJobType =  "Scan Nodes   ";
		}
		else if (flags & SCL_SCAN_ENITIES)
		{
			pJobType =  "Scan Entities";
		}
		else if (flags & SCL_SCAN_OCCLUSIONWRAPPER)
		{
			pJobType =  "Occ Wrapper  ";
		}
		else if (flags & SCL_SCAN_OCCLUSIONSYNC)
		{
			pJobType =  "Occ Sync     ";
		}
		else
		{
			Assertf(0,"No job type");
		}

		const char *pJobOp	 = NULL;
		if (flags & SCL_BEGIN)
		{
			pJobOp = "Begin     ";
		}
		else if (flags & SCL_END)
		{
			pJobOp = "End       ";
		}
		else// if (pLogData[i].m_flags & SCL_ADD_JOB_SE)
		{
			pJobOp = "Add SE Job";
		}


		const char *pContainerType = "      ";
		u32 dependencyAddress = 0;
		if (flags & (SCL_ADD_JOB_SE | SCL_SCAN_ENITIES))
		{
			if (flags & SCL_CONTAINER_ENTITY)
			{
				pContainerType = "Entity";
			}
			else if (flags & SCL_CONTAINER_SOA)
			{
				pContainerType = "SOA   ";
			}
			else //if (pLogData[i].m_flags & SCL_CONTAINER_FIXED)
			{
				pContainerType = "Fixed ";
			}
			dependencyAddress = pLogData[i].m_pEntityDesc;
		}

		const char *pProcessorType = (flags & SCL_SPU) ? "SPU" : "PPU";

		Displayf("ScanLog: %s, %s, %s, %3u, %u, %s, %u, 0x%x", pJobType, pJobOp, pContainerType, pLogData[i].m_entityCount,
			pLogData[i].m_timeStamp, pProcessorType, pLogData[i].m_spuID,
			dependencyAddress);
	}

	// Search for missing entries
	if (!allJobsComplete)
	{
		rage::u32 i = sortStartIndex + 2; 
		int foundCount = 0;
		int addJobSEIndex = -1;
		int SEBegin = -1;
		int SEEnd = -1;
		u32 previousEntDescPtr = pLogData[i].m_pEntityDesc;
		while (i < endIndex)
		{
			if (previousEntDescPtr != pLogData[i].m_pEntityDesc)
			{
				// We've missed an entry
				if (foundCount != 0 && foundCount < 3)
				{
					// Find an index to get data out of
					int index = 0;
					if (addJobSEIndex != -1)
					{
						index = addJobSEIndex;
					}
					else if(SEBegin != -1)
					{
						index = SEBegin;
					}
					else 
					{
						Assertf(SEEnd != -1, "Error SEEnd is invalid, Data output in log will be erroneous");
						index = SEEnd;
					}

					// Get the container type
					const char *pContainerType = ""; 
					u32 flags = pLogData[index].m_flags;
					if (flags & SCL_CONTAINER_ENTITY)
					{
						pContainerType = "Entity";
					}
					else if (flags & SCL_CONTAINER_SOA)
					{
						pContainerType = "SOA";
					}
					else
					{
						pContainerType = "Fixed";
					}
					// Note if we're missig the add job log from scan nodes because we can't tell if its a fixed container
					const char *pTypeWarning = "";
					if (index != addJobSEIndex && !(flags & SCL_CONTAINER_SOA))
					{
						pTypeWarning = "Container may be Fixed!";
					}

					Displayf("ScanLog: Entity Container 0x%x did not finish processing. Entity Count %d, Container Type %s. %s",  
							  previousEntDescPtr, pLogData[index].m_entityCount, pContainerType, pTypeWarning);
					if (addJobSEIndex == -1)
					{
						Displayf("ScanLog: Scan nodes did not add a scan log entry added for this container");
					}

					if (SEBegin == -1)
					{
						Displayf("ScanLog: Scan Entities Begin was not logged");
					}

					if (SEEnd == -1)
					{
						Displayf("ScanLog: Scan Entities End was not logged");
					}

					if (previousEntDescPtr)
					{
						if (flags & SCL_CONTAINER_SOA)
						{
							fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( (fwEntityDesc *)previousEntDescPtr, pLogData[index].m_entityCount);
							for (int e = 0; e < pLogData[index].m_entityCount; ++e)
							{
								if (soa.m_entityPointers[e])
								{
									Displayf("ScanLog: Entity - %s", soa.m_entityPointers[e]->GetModelName());
								}
								else
								{
									Displayf("ScanLog: NULL Entity");
								}
							}

						}
						else
						{
							fwEntityDesc *pDescs = (fwEntityDesc *)previousEntDescPtr;
							for (int e = 0; e < pLogData[index].m_entityCount; ++e)
							{
								if (pDescs[e].GetEntity())
								{
									Displayf("ScanLog: Entity - %s", pDescs[e].GetEntity()->GetModelName());
								}
								else
								{
									Displayf("ScanLog: NULL Entity");
								}
							}
						}
					}
					else
					{
						Displayf("ScanLog: Error Logging out entities as previousEntDescPtr is NULL");
					}
					addJobSEIndex = -1;
					SEBegin = -1;
					SEEnd = -1;
					foundCount = 0;
				}
			}

			if (pLogData[i].m_flags & SCL_ADD_JOB_SE) 
			{
				addJobSEIndex = i;
				foundCount++;
			}
			else if (pLogData[i].m_flags & SCL_SCAN_ENITIES)
			{
				if (pLogData[i].m_flags & SCL_BEGIN)
				{
					SEBegin = i;
					foundCount++;
				}
				else if (pLogData[i].m_flags & SCL_END)
				{
					SEBegin = i;
					foundCount++;
				}
				else
				{
					Assertf(0, "Error Bad Scan Logs flags, neither Begin or End is set for scan entities log");
				}

			}

			if (foundCount > 2)
			{
				addJobSEIndex = -1;
				SEBegin = -1;
				SEEnd = -1;
				foundCount = 0;
			}

			previousEntDescPtr = pLogData[i].m_pEntityDesc;
			++i;
		}
	}

	// Add code here to sort list and then find missing entries
	
}
#endif // __PPU
#endif // SCL_LOGGING

#endif
rage::u32			g_scanMutableDebugFlags = SCAN_MUTABLE_DEBUG_DEFAULT_FLAGS;
