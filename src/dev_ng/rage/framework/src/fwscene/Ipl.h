//
// fwscene/Ipl.h : common types used for loading placement files (IPL files)
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef _FWSCENE_IPL_H_
#define _FWSCENE_IPL_H_

#include "system/bit.h"

// Structure of IPL file format, in order:
//
// 1.  fwIplHeader
// 2.  fwIplBound section				(aggregated aabb for map instances)
// 3.  fwIplInstance section			(map instances)
// 4.  fwIplInteriorInstance section	(interior instances)
// 5.  fwIplZone section				(ped population zones)
// 6.  fwIplGarage section				(special building instances, such as garages, bomb shops etc)
// 7.  fwIplCarGen section				(car generators)
// 8.  fwIplTimeCyc section				(time-cycle modifier boxes)
// 9.  fwIplPathNode section			(path finding mesh nodes)
// 10. fwIplPathLink section			(path finding mesh links)
// 11. fwIplScript section				DEPRECATED
// 12. fwIplBlock section				(useful info about this region of the map, author, date etc)
// 13. fwIplMloAppend section			(appended MLO objects)
// 14. fwIplSlowZone section			(vehicle slowness zones)
// 15. fwIplPatrolNode section			(patrol path nodes)
// 16. fwIplPatrolLink section			(patrol path links)

namespace rage
{
#define IPL_VERSION					(6)			// current IPL version
#define IPL_OLD_VERSION_ENABLED		(0)			// only enabled during transition to new IPL version
#define IPL_ENTRY_NAME_LENGTH		(24)

	//
	// fwIplHeader
	//
	// Header of IPL file describes how many of each type of entry
	// are contained with the IPL file, and some other useful information
	// such as version number etc
	class fwIplHeader
	{
	public:
		s32 version;

		s32 numBounds;
		s32 numInstances;
		s32 numInteriorInstances;
		s32 numZones;
		s32 numGarages;
		s32 numCarGens;
		s32 numTimeCycs;
		s32 numPathNodes;
		s32 numPathLinks;
		s32 numRootScripts;
		s32 numMloAppend;
		s32 numSlowZones;
		s32 numBlocks;
		s32 numPatrolNodes;
		s32 numPatrolLinks;
		u32 numLodChildren;
		u32 numLightInsts;
		
		s32 unusedPadding[13];

		f32 maxLodDistance;
	};

	//
	// fwIplBounds
	//
	// Data structure representing an aabb specified in the bounds
	// section of an IPL (v6 onwards). Primarily used to specify bounds
	// of instances in IPL
	class fwIplBounds
	{
	public:
		enum
		{
			IPL_BOUNDS_TYPE_INSTANCES,
			IPL_BOUNDS_TYPE_CARGENS,

			IPL_BOUNDS_TYPE_TOTAL
		};

		u32 type;	// e.g. IPL_BOUNDS_TYPE_INSTANCES
		f32 minX;
		f32 minY;
		f32 minZ;
		f32 maxX;
		f32 maxY;
		f32 maxZ;
	};

	//
	// fwIplInstanceBase
	//
	// Data structure representing an entry in the instances section of
	// an IPL file - an instance of an object in the world including position,
	// rotation, lod information etc
	class fwIplInstanceBase
	{
	public:
		enum
		{
			FLAG_NONE 								= 0x0000,
			FLAG_STREAMING_LOWPRIORITY				= 0x0001,
			FLAG_FULLMATRIX							= 0x0002,
			FLAG_UNDERWATER							= 0x0004,
			FLAG_TUNNEL								= 0x0008,
			FLAG_TUNNELTRANSITION					= 0x0010,
			FLAG_DOESNOTTOUCHWATER					= 0x0020,
			FLAG_DOESNOTSPAWNPEDS					= 0x0040,
			FLAG_LIGHTS_CAST_STATIC_SHADOWS			= 0x0080,
			FLAG_LIGHTS_CAST_DYNAMIC_SHADOWS		= 0x0100,
			FLAG_LIGHTS_IGNORE_DAY_NIGHT_SETTINGS	= 0x0200,
			FLAG_LIGHTS_PRIORITYA					= 0x0400,
			FLAG_LIGHTS_PRIORITYB					= 0x0800,
			FLAG_LOD_ADOPTME						= 0x1000
		};

		f32 posX;
		f32 posY;
		f32 posZ;
		f32 quatX;
		f32 quatY;
		f32 quatZ;
		f32 quatW;
		u32 modelHashKey;
		u32 flags;
		s32 lodIndex;

		struct 
		{
			u32		tbd			: 28;
			u32		lightGroup	: 4;

		} extraFlags;

		f32 lodDistance;
		s32 lodTreeDepth;
		s32 numChildren;
		s32 ambientMult;
	};

	class fwIplInstance : public fwIplInstanceBase
	{
	public:
		f32 scaleXY;
		f32 scaleZ;
	};

	//
	// fwIplInteriorInstance
	//
	// Data structure representing an entry in the MLO (interior) instances section of
	// an IPL file - an instance of an object in the world including position,
	// rotation, lod information etc
	class fwIplInteriorInstance : public fwIplInstanceBase
	{
	public:
		u32	groupId;
		u32	reserved[4];	//TBD
	};

	//
	// fwIplZone
	//
	// Data structure representing an entry in the population zone section of an IPL file
	class fwIplZone
	{
	public:
		char name[IPL_ENTRY_NAME_LENGTH];
		char textLabel[10];
		f32 minX;
		f32 minY;
		f32 minZ;
		f32 maxX;
		f32 maxY;
		f32 maxZ;
		s32 type;
		s32 level;
	};

	//
	// fwIplGarage
	//
	// Data structure representing an entry in the garages section of an IPL file
	class fwIplGarage
	{
	public:
		f32 x1, y1, z1;
		f32 x2, y2;
		f32 x3, y3;
		f32 zTop;
		s32 flags;
		s32 type;
		char name[8];
	};

	//
	// fwIplCarGen
	//
	// Data structure representing an entry in the car generator section of an IPL file
	class fwIplCarGen
	{
	public:
		enum
		{
			FLAG_HIGHPRIORITY		= BIT(0),
			FLAG_IGNORE_DENSITY		= BIT(1),
			FLAG_POLICE				= BIT(2),
			FLAG_FIRETRUCK			= BIT(3),
			FLAG_AMBULANCE			= BIT(4),
			FLAG_DURING_DAY			= BIT(5),
			FLAG_AT_NIGHT			= BIT(6),
			FLAG_ALIGN_LEFT			= BIT(7),
			FLAG_ALIGN_RIGHT		= BIT(8),
			FLAG_SINGLE_PLAYER		= BIT(9),
			FLAG_NETWORK			= BIT(10)
		};

		f32 x;
		f32 y;
		f32 z;
		f32 vec1x;
		f32 vec1y;
		f32 lengthVec2;
		s32 modelHashKey;
		s32 col1;
		s32 col2;
		s32 col3;
		s32 col4;
		s32 flagVal;	// MSB - 0xF000000 (creationRule), 0x0F000000 (scenario), then generic bit-flags
		s32 alarmChance;
		s32 lockedChance;

		static inline s32 GetCreationRule(s32 flags)	{ return (flags & 0xF0000000) >> 28; }
		static inline s32 GetScenario(s32 flags)		{ return (flags & 0x0F000000) >> 24; }
	};

	//
	// fwIplTimeCyc
	//
	// Data structure representing an entry in the time cycle modifier section of an IPL file
	class fwIplTimeCyc
	{
	public:
		f32 minX;
		f32 minY;
		f32 minZ;
		f32 maxX;
		f32 maxY;
		f32 maxZ;
		f32 percentage;
		f32 range;
		s32 startHour;
		s32 endHour;
		s32 hashCode;
	};

	//
	// fwIplPathNode
	//
	// Data structure representing an entry in the path node section of an IPL file
	class fwIplPathNode
	{
	public:
		enum
		{
			FLAG_NONE 					= 0x0000,
			FLAG_HIGHWAY				= 0x0001,
			FLAG_NOGPS					= 0x0002,
			FLAG_ADDITIONALTUNNEL		= 0x0004
		};

		f32 x;
		f32 y;
		f32 z;
		s32 switchedOff;
		s32 waterNode;
		s32 roadBlock;
		s32 speed;
		s32 specialFunction;
		f32 density;
		u32 streetNameHash;
		u32 flags;
	};

	//
	// fwIplPathLink
	//
	// Data structure representing an entry in the path link section of an IPL file
	class fwIplPathLink
	{
	public:
		s32 node1; 
		s32 node2;
		f32 width;
		s32 lanes1To2; 
		s32 lanes2To1;
	};

	//
	// fwIplBlock
	//
	// Data structure representing data about a region of the map, author and time stamp info
	class fwIplBlock
	{
	public:
		u32 version;
		char name_owner_export_time[92];
		u32 flags;
		f32 x1, y1;
		f32 x2, y2;
		f32 x3, y3;
		f32 x4, y4;
	};

	//
	// fwIplMloAppend
	//
	// Data structure representing an appending MLO object in an IPL file
	class fwIplMloAppend
	{
	public:
		char name[IPL_ENTRY_NAME_LENGTH];
		s32 flags;
		s32 id;
		s32 count;
		f32 posx;
		f32 posy;
		f32 posz;
		f32 quatx;
		f32 quaty;
		f32 quatz;
		f32 quatw;
	};

	//
	// fwIplSlowZone
	//
	// Data structure representing a vehicle slowness zone in an IPL file
	class fwIplSlowZone
	{
	public:
		f32 minX, minY, minZ;
		f32 maxX, maxY, maxZ;
	};

	//
	// fwIplPatrolNode
	//
	// Data structure representing a patrol path node in an IPL file
	class fwIplPatrolNode
	{
	public:
		f32 x, y, z;
		f32 headingX, headingY, headingZ;
		s32 duration;
		u32 nodeTypeHash;
		u32 associatedBaseHash;
		u32 routeNameHash;
	};

	//
	// fwIplPatrolNode
	//
	// Data structure representing a patrol path link in an IPL file
	class fwIplPatrolLink
	{
	public:
		s32 node1;
		s32 node2;
	};

	//
	// fwIplLodChildDef
	//
	// Data structure representing a LOD parent-child link between an
	// instance in this file (at specified index) and a child container
	// with specified filename hash. This allows for creating low-detail
	// models to represent multiple containers.
	class fwIplLodChildDef
	{
	public:
		u32	childIplNameHash;
		u32	lodInstIndex;
		u32 unused[2];
	};

	CompileTimeAssert(128	== sizeof(fwIplHeader));
	CompileTimeAssert(28	== sizeof(fwIplBounds));
	CompileTimeAssert(68	== sizeof(fwIplInstance));
	CompileTimeAssert(80	== sizeof(fwIplInteriorInstance));
	CompileTimeAssert(68	== sizeof(fwIplZone));
	CompileTimeAssert(48	== sizeof(fwIplGarage));
	CompileTimeAssert(56	== sizeof(fwIplCarGen));
	CompileTimeAssert(44	== sizeof(fwIplTimeCyc));
	CompileTimeAssert(44	== sizeof(fwIplPathNode));
	CompileTimeAssert(20	== sizeof(fwIplPathLink));
	CompileTimeAssert(132	== sizeof(fwIplBlock));
	CompileTimeAssert(64	== sizeof(fwIplMloAppend));
	CompileTimeAssert(24	== sizeof(fwIplSlowZone));
	CompileTimeAssert(40	== sizeof(fwIplPatrolNode));
	CompileTimeAssert(8		== sizeof(fwIplPatrolLink));
	CompileTimeAssert(16	== sizeof(fwIplLodChildDef));

} //namespace rage

#endif //_FWSCENE_IPL_H_
