/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/interiors/Portaltypes.h
// PURPOSE : general simple type stuff to make #includes more manageable throughout
// AUTHOR :   John.
// CREATED : 30/10/08
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _INC_PORTALTYPES_H_
#define _INC_PORTALTYPES_H_

namespace rage {

enum
{
	PORTAL_HI_LOD_DIST			= 20,	// distance from portal at which contents are rendered (hi LOD)
	PORTAL_INSTANCE_DIST		= 40,	// distance from MLO bound sphere at which portal instancing takes place / portal object is visible
	GROUP_PORTAL_EXTERNAL_INST_DIST	= 80,	// distance from MLO bound sphere at which portal instancing takes place / portal object is visible (if the MLO is connecting group to world)
	GROUP_PORTAL_INTERNAL_INST_DIST	= 250,	// distance from MLO bound sphere at which portal instancing takes place / portal object is visible (if the MLO is part of a group)
	
	TUNNEL_PORTAL_EXTERNAL_INST_DIST	= 120,	// distance from MLO bound sphere at which portal instancing takes place / portal object is visible (if the MLO is connecting group to world)
	TUNNEL_PORTAL_INTERNAL_INST_DIST	= 200,	// distance from MLO bound sphere at which portal instancing takes place / portal object is visible (if the MLO is part of a group)

	MLO_LOD_DIST				= 80,	// distance from MLO bound at which processing of MLO begins (this dist + radius of MLO)
	MLO_LOD2_DIST				= 210	// distance from MLO bound at which 2nd level MLO is displayed instead
};

} // namespace rage

#endif //_INC_PORTALTYPES_H_
