//
// fwscene/Ide.h : common types used for loading object definition files (IDE files)
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef _FWSCENE_IDE_H_
#define _FWSCENE_IDE_H_

// NB: binary IDE file format not currently in use, but when we bring it back it will live here
// 2010-04-07 R*N IanK

namespace rage
{
	//
	// fwIdeHeader
	//
	// Data structure representing IDE file header
	class fwIdeHeader
	{
	public:
		s32 version;
		s32 numObjects;
		s32 unused[15];	//TBD
	};

	//
	// fwIdeObjectDef
	//
	// Data structure representing IDE object file definition
	class fwIdeObjectDef
	{
	public:
		s32 modelHashKey;
		s32 txdHashKey;
		s32 ddModelHashKey;
		f32 lodDist;
		s32 flags;
		s32 specialAttribute;
		f32 bbMinX;
		f32 bbMinY;
		f32 bbMinZ;
		f32 bbMaxX;
		f32 bbMaxY;
		f32 bbMaxZ;
		f32 bsCentreX;
		f32 bsCentreY;
		f32 bsCentreZ;
		f32 bsRadius;
	};

	CompileTimeAssert(68	== sizeof(fwIdeHeader));
	CompileTimeAssert(64	== sizeof(fwIdeObjectDef));

} //namespace rage

#endif //_FWSCENE_IDE_H_