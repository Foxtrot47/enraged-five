//
// fwscene/scene.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef FWSCENE_SCENE_H__
#define FWSCENE_SCENE_H__

namespace rage
{

class fwSceneInterface
{
public:
	virtual ~fwSceneInterface() {}
	virtual void InitPools() {}
	virtual void ShutdownPools() {}
};

class fwScene
{
public:
	static void InitClass(fwSceneInterface* sceneInterface);
	static void ShutdownClass();

	static void Init();
	static void Shutdown();

	static void LoadMap(const char* filename);
};

} // namespace rage

#endif // FWSCENE_SCENE_H__