#ifndef _INC_SEARCH_H_
#define _INC_SEARCH_H_

// Framework headers
#include "fwscene/world/WorldRepBase.h"
#include "fwscene/world/EntityContainer.h"
#include "fwscene/search/SearchVolumes.h"
#include "fwscene/search/SearchNodes.h"
#include "fwscene/search/SearchEntities.h"

// Rage headers
#include "atl/array.h"

/*
This is the main header for the world search system. Its main features are:

 -	It is job-based so searches can be run asynchronously on SPU (on PS3) or other threads (other platforms).

 -	It is data-driven: the output of a search is an array of entity pointers. Methods to invoke
	callbacks on searches result (i.e. ForAllEntitiesIntersecting) are provided for backward compatibility.

!!!BEWARE!!!

When performing an asynchronous search, the code that gets executed in the meantime MUST NOT MODIFY THE WORLD
for obvious reasons.
*/

namespace rage {

struct fwSearchConstants
{
	fwSearchVolume		m_collisionVolume;

	u32					m_typeFlags;
	u32					m_locationFlags;
	u32					m_lodFlags;
	u32					m_optionFlags;
	
	u32*				m_entriesCountPtr;
	u32					m_entriesCapacity;
	fwEntity**			m_entriesArray;
};

CompileTimeAssert( ( sizeof(fwSearchConstants) & 0xf ) == 0 );

typedef atArray< fwEntity*, 0, u32 >		fwSearchResult;

/*
PURPOSE:

This class represents a world search.
*/

class fwSearch
{
private:

	static fwSearch		ms_defaultSearch;
	
	static bool			ms_useSpinWaiting;
	static u32			ms_filterSearchMask;
	
	fwSearchConstants		m_searchConstants ;
	fwSearchResult			m_searchResult;

#if __WIN32
	u32						m_Pad;
#endif

	sysDependency			m_dependency;
	u32						m_searchModule;
	u32						m_searchPendingJobCount;

#if __STATS
	u32						m_spuSearchNodesTime;
#endif
	
#if __BANK
	bool m_searchStarted;
#endif
	void PrepareConstants(fwSearchVolume* collisionVolume, const u32 typeFlags, const u32 locationFlags, const u32 lodFlags, const u32 optionFlags);
	void PerformSearch(fwSceneGraphNode* rootSceneNode, sysDependency::ePriority priority);

public:

	// PURPOSE:	Gets the default search. The "default search" is the global search object that is used by
	//			ForAllEntitiesIntersecting.
	// RETURNS:	The default search object.
	static fwSearch& GetDefaultSearch()								{ return ms_defaultSearch; }

	// PURPOSE:	Executes the specified callback on all the entities that satisfy the search criteria.
	//			Internally, this method uses a global search object to do a synchronous search.
	// PARAMS:	see Search method
	static void ForAllEntitiesIntersecting(fwSceneGraphNode* rootSceneNode, fwSearchVolume* collisionVolume, fwIntersectingCB callback, void* data, u32 typeFlags, u32 locationFlags, u32 lodFlags, u32 optionFlags, u32 module);

	// PURPOSE:	Creates a search object with room for the specified number of entities.
	// PARAMS:	searchResultCapacity - the max. number of entities returned by this search
	fwSearch(const u32 searchResultCapacity);

	// PURPOSE:	Destroy the search object
	~fwSearch();

	// PURPOSE:	Starts an async search
	// PARAMS:	rootSceneNode - root of the container hierarchy
	//			collisionVolume - collision testing volume
	//			callback - callback to execute on each entity
	//			arbitrary - data to be passed to the callback
	//			typeFlags - flags to restrict the search based on entity type
	//			locationFlags - flags to restrict the search based on interior / exterior
	//			lodFlags - flags to restrict the search based on entity LOD level
	//			optionFlags - flags to provide further info to the search, to increase performance
	//			module - the module that is requesting the search
	//			priority - the priority at which to conduct this search. Priority is within the rage dependency system.
	void Start(fwSceneGraphNode* rootSceneNode, fwSearchVolume* collisionVolume, u32 typeFlags, u32 locationFlags, u32 lodFlags, u32 optionFlags, u32 module, sysDependency::ePriority priority = sysDependency::kPriorityMed);

	// PURPOSE:	Waits for this async search to finish
	void Finalize();

	// PURPOSE: Checks whether this search has finished
	bool IsFinished();

	// PURPOSE:	Gets the result array for this search
	fwSearchResult& GetResult()										{ return m_searchResult; }

	// PURPOSE:	Executes the given callback on the result of a finished search
	// PARAMS:	callback - callback to be executed
	//			data - arbitrary data to be passed to the callback
	void ExecuteCallbackOnResult(fwIntersectingCB callback, void* data);

#if __BANK
	static void InitWidgets();
	static void UpdateDebug();
	static void DrawDebug() {}

	static bool  ms_enableSearchDebugTest;
	static float ms_debugSearchRadius;
	static float ms_debugSearchDistanceAlongForwardVector;
	static int   ms_debugSearchSearchVolumeType;
	static Vector3 ms_debugSearhSpherePos;
	static Vector3 ms_debugSearchBoxExtents;
#endif // __BANK
};

} // namespace rage

#endif // !defined _INC_SEARCH_H_
