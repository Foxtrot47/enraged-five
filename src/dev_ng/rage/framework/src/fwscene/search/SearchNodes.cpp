#if !__TOOL

#include "fwscene/search/SearchNodes.h"
#include "fwscene/search/Search.h"
#include "fwscene/scan/ScanDebug.h"
#include "fwscene/scan/ScanPerformance.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwscene/world/ExteriorSceneGraphNode.h"
#include "fwscene/world/InteriorSceneGraphNode.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwscene/world/SceneGraphVisitor.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/timer.h"
#include "system/dependencyscheduler.h"
#include "system/ppu_symbol.h"

namespace SearchNodesStats
{
	PF_PAGE(SearchNodesPage,"Search Nodes");

	PF_GROUP(SearchNodes);
	PF_LINK(SearchNodesPage, SearchNodes);
	PF_TIMER(fwSearchNodesRun, SearchNodes);
}
using namespace SearchNodesStats;

namespace rage {

bool SearchNodes_Dependency(const sysDependency& dependency)
{
	PF_FUNC(fwSearchNodesRun);

	sysTimer							timer;
	fwSearchNodes						searchNodes;

	u8*									poolsMemory = static_cast< u8* >( dependency.m_Params[0].m_AsPtr );
	const u32							poolsMemorySize = dependency.m_DataSizes[0];
	sysEaPtr< u8 >						poolsMemoryEa = static_cast< u8* >( dependency.m_Params[1].m_AsPtr );
	fwSearchConstants*					searchConstants = static_cast< fwSearchConstants* >( dependency.m_Params[2].m_AsPtr );
	sysEaPtr< fwSearchConstants >		searchConstantsEa = static_cast< fwSearchConstants* >( dependency.m_Params[3].m_AsPtr );
	fwSceneGraphNode*					rootSceneNode = static_cast< fwSceneGraphNode* >( dependency.m_Params[4].m_AsPtr );
	sysEaPtr< u32 >						searchPendingJobCountEa = static_cast< u32* >( dependency.m_Params[5].m_AsPtr );
#if __STATS
	sysEaPtr< u32 >						spuSearchNodesTimeEa = static_cast< u32* >( dependency.m_Params[6].m_AsPtr );
#endif

#if __SPU
	fwSceneGraph::InitSpuPools( poolsMemory, poolsMemoryEa, poolsMemorySize );
	fwSceneGraph::GetPointerFixer().Fix( rootSceneNode );
#else
	(void) poolsMemory; (void) poolsMemorySize; (void) poolsMemoryEa;
#endif

	searchNodes.SetSearchConstants( searchConstants );
	searchNodes.SetSearchConstantsEa( searchConstantsEa );
	searchNodes.SetRootSceneNode( rootSceneNode );
	searchNodes.Process();

#if __STATS
	sysInterlockedAdd( spuSearchNodesTimeEa.ToPtr(), static_cast< u32 >( timer.GetUsTime() ) );
#endif

	sysInterlockedDecrement( searchPendingJobCountEa.ToPtr() );

	return true;
}

void fwSearchNodes::ProcessEntities(fwBaseEntityContainer* entityContainer)
{
	if ( entityContainer->GetEntityCount() == 0 )
		return;

	fwFlags16	searchEntitiesflags;
	searchEntitiesflags.ClearAllFlags();
	if ( entityContainer->IsTypeSoA() )
		searchEntitiesflags.SetFlag( fwSearchEntities::FLAG_ENTITY_DESC_SOA );

	u32 currentEntityDescCount = entityContainer->IsTypeFixed() ? entityContainer->GetStorageSize() : entityContainer->GetEntityCount();

	fwEntityDesc* entityDescArray = entityContainer->GetEntityDescArray();

	fwScanTimerSkipFunc		scanTimer( PPU_SYMBOL(g_searchNodesTimer) );

	fwSearchEntities		searchEntities;
	static u8				scratchBuffer[ fwSearchEntities::SCRATCH_BUFFER_SIZE ];

	searchEntities.SetSearchConstants( m_searchConstants );
	searchEntities.SetEntityDescs( entityDescArray );
	searchEntities.SetEntityDescsCount( currentEntityDescCount );
	searchEntities.SetFlags( searchEntitiesflags );
	searchEntities.AssignScratchBuffer( scratchBuffer, fwSearchEntities::SCRATCH_BUFFER_SIZE );

	searchEntities.Process();
}

void fwSearchNodes::Process()
{
	fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_searchNodesTimer) );

	for (fwSceneGraphVisitor visitor( m_rootSceneNode ); !visitor.AtEnd(); visitor.Next())
	{
		fwSceneGraphNode*	sceneNode = visitor.GetCurrent();

		if ( sceneNode->IsTypeExterior() )
		{
			if ( !( m_searchConstants->m_locationFlags & SEARCH_LOCATION_EXTERIORS ) )
				continue;

			fwExteriorSceneGraphNode*	exteriorSceneNode = static_cast< fwExteriorSceneGraphNode* >( sceneNode );

			if ( !( m_searchConstants->m_optionFlags & SEARCH_OPTION_DYNAMICS_ONLY ) )
			{
				ProcessEntities( exteriorSceneNode->GetStaticsContainer() );
				ProcessEntities( exteriorSceneNode->GetLightsContainer() );
			}

			ProcessEntities( exteriorSceneNode->GetDynamicsContainer() );
		}
		else if ( sceneNode->IsTypeStreamed() )
		{
			const fwStreamedSceneGraphNode*		streamedSceneNode = static_cast< const fwStreamedSceneGraphNode* >( sceneNode );

			bool	earlyOut = false;
			earlyOut = earlyOut || ( m_searchConstants->m_optionFlags & SEARCH_OPTION_DYNAMICS_ONLY );
			earlyOut = earlyOut || !( m_searchConstants->m_locationFlags & SEARCH_LOCATION_EXTERIORS );
			earlyOut = earlyOut || ( streamedSceneNode->GetContainsHighDetails() && !( m_searchConstants->m_lodFlags & SEARCH_LODTYPE_HIGHDETAIL ) );
			earlyOut = earlyOut || ( streamedSceneNode->GetContainsLowDetails() && !( m_searchConstants->m_lodFlags & SEARCH_LODTYPE_LOWDETAIL ) );
			earlyOut = earlyOut || ( !streamedSceneNode->GetContainsSpawnPoints() && ( m_searchConstants->m_optionFlags & SEARCH_OPTION_SPAWN_POINTS_ONLY ) );
			earlyOut = earlyOut || ( !streamedSceneNode->IsEnabled() );

			if ( earlyOut )
				continue;

			spdAABB collisionBox = m_searchConstants->m_collisionVolume.GetBoundBox();
			if ( collisionBox.IntersectsAABB( streamedSceneNode->GetBoundingBox() ) )
			{
				fwBaseEntityContainer*	streamedContainer = streamedSceneNode->GetFirstContainer();

				while ( streamedContainer )
				{
					ProcessEntities( streamedContainer );
					streamedContainer = streamedContainer->GetNext();
				}
			}
		}
		else if ( sceneNode->IsTypeRoom() || sceneNode->IsTypePortal() )
		{
			if ( !( m_searchConstants->m_locationFlags & SEARCH_LOCATION_INTERIORS ) )
				continue;

			if ( sceneNode->IsTypeRoom() )
				ProcessEntities( static_cast< fwRoomSceneGraphNode* >( sceneNode )->GetEntityContainer() );
			else
				ProcessEntities( static_cast< fwPortalSceneGraphNode* >( sceneNode )->GetEntityContainer() );
		}
		else if ( sceneNode->IsTypeInterior() )
		{ }
		else
			Assertf( false, "Unexpected type of scene graph node!" );
	}
}

} // namespace rage

#endif // !__TOOL
