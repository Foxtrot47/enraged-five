rage::u32	g_auxDmaTag = 0;

#include "system/dependency.h"
#include "system/dma.h"
#include "fwscene/search/SearchEntities.cpp"
#include "fwscene/search/SearchNodes.cpp"
#include "fwscene/search/SearchVolumes.cpp"
#include "fwscene/world/SceneGraph.cpp"
#include "fwscene/world/SceneGraphNode.cpp"
#include "fwscene/world/EntityContainer.cpp"
#include "fwscene/world/SceneGraphVisitor.cpp"
#include "vector/geometry.cpp"

#include "system/dependencyscheduler.cpp"

using rage::sysDependency;

SPUFRAG_DECL(bool, SearchNodes, sysDependency&);
SPUFRAG_IMPL(bool, SearchNodes, sysDependency& dependency) {
	return SearchNodes_Dependency( dependency );
}
