//
// filename:	SearchVolumes.h
// description:	
//

#ifndef _INC_SEARCHVOLUMES_H_
#define _INC_SEARCHVOLUMES_H_

// --- Include Files ------------------------------------------------------------

// Framework headers
#include "fwscene/world/EntityDesc.h"

// Rage headers
#include "spatialdata/aabb.h"
#include "spatialdata/sphere.h"

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// ----------------------------------------

namespace rage {

class spdAABB;

enum eSearchVolumeType
{
	SEARCH_VOLUME_IS_SPHERE_INSIDE,
	SEARCH_VOLUME_IS_SPHERE_INTERSECTING,
	SEARCH_VOLUME_IS_SPHERE_INTERSECTING_VISIBLE,
	SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX,
	SEARCH_VOLUME_IS_BOX_INSIDE,
	SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX,
	SEARCH_VOLUME_IS_BOX_INTERSECTING_BB,
	SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB
};

// ----------------------------------------------------------------------------

class fwSearchVolume
{
private:

	bool SearchVolumesIntersect(const spdSphere& entitySphere, const spdAABB& entityBox, const fwEntityDesc* pEntityDesc=NULL) const;

public:

	spdAABB				m_boundingBox;
	spdSphere			m_sphere;
	Vector3				m_vec1;
	Vector3				m_vec2;
	eSearchVolumeType	m_type;

	fwSearchVolume(const spdSphere& sphere, eSearchVolumeType type) :
		m_boundingBox( sphere ),
		m_sphere( sphere ),
		m_type( type )
	{ }

	fwSearchVolume(const spdAABB& box, eSearchVolumeType type) :
		m_boundingBox( box ),
		m_type( type )
	{ }

	fwSearchVolume(const Vector3& vec1, const Vector3& vec2, eSearchVolumeType type) :
		m_vec1( vec1 ),
		m_vec2( vec2 ),
		m_type( type )
	{
		const Vec3V boxMin = Min(RCC_VEC3V(vec1), RCC_VEC3V(vec2));
		const Vec3V boxMax = Max(RCC_VEC3V(vec1), RCC_VEC3V(vec2));
		m_boundingBox.Set(boxMin, boxMax);
	}

	fwSearchVolume() {
	}

	const spdAABB& GetBoundBox() const {
		return m_boundingBox;
	}

	void GetBoundBox(spdAABB& box) {
		box = m_boundingBox;
	}

	bool operator()(const spdSphere& sphere, const spdAABB& box) const {
		return SearchVolumesIntersect( sphere, box );
	}

	bool operator()(const fwEntityDesc* pEntityDesc) const {
		return SearchVolumesIntersect( pEntityDesc->GetApproxBoundingSphere(), pEntityDesc->GetApproxBoundingBox(), pEntityDesc );
	}

#if __BANK
	void GetStringDescription(char* str);
#endif // __BANK
};

class fwIsSphereInside : public fwSearchVolume
{
public:

	fwIsSphereInside(const spdSphere& sphere) :
		fwSearchVolume( sphere, SEARCH_VOLUME_IS_SPHERE_INSIDE )
	{ }
};

class fwIsSphereIntersecting : public fwSearchVolume
{
public:

	fwIsSphereIntersecting(const spdSphere& sphere) :
		fwSearchVolume( sphere, SEARCH_VOLUME_IS_SPHERE_INTERSECTING )
	{ }
};

class fwIsSphereIntersectingVisible : public fwSearchVolume
{
public:

	fwIsSphereIntersectingVisible(const spdSphere& sphere) :
	  fwSearchVolume( sphere, SEARCH_VOLUME_IS_SPHERE_INTERSECTING_VISIBLE )
	  { }
};

class fwIsSphereIntersectingBBApprox : public fwSearchVolume
{
public:

	fwIsSphereIntersectingBBApprox(const spdSphere& sphere) :
		fwSearchVolume( sphere, SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX )
	{ }
};

class fwIsBoxInside : public fwSearchVolume
{
public:

	fwIsBoxInside(const spdAABB& box) :
		fwSearchVolume( box, SEARCH_VOLUME_IS_BOX_INSIDE )
	{ }
};

class fwIsBoxIntersectingApprox : public fwSearchVolume
{
public:

	fwIsBoxIntersectingApprox(const spdAABB& box) :
	  fwSearchVolume( box, SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX )
	  { }
};

class fwIsBoxIntersectingBB : public fwSearchVolume
{
public:

	fwIsBoxIntersectingBB(const spdAABB& box) :
		fwSearchVolume( box, SEARCH_VOLUME_IS_BOX_INTERSECTING_BB )
	{ }
};

class fwIsLineSegIntersectingBB : public fwSearchVolume
{
public:

	fwIsLineSegIntersectingBB(const Vector3& vec1, const Vector3& vec2) :
		fwSearchVolume( vec1, vec2, SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB )
	{ }
};

} // namespace rage

#endif // !defined _INC_SEARCHVOLUMES_H_
