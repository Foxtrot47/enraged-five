#if !__TOOL

#include "fwgeovis/geovis.h"
#include "fwscene/search/SearchEntities.h"
#include "fwscene/search/Search.h"
#include "fwscene/scan/ScanPerformance.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "system/cache.h"
#include "system/interlocked.h"
#include "system/timer.h"
#include "system/dependencyscheduler.h"
#include "system/ppu_symbol.h"
#include "system/dependency.h"

#if __BANK
#if __SPU
	DECLARE_PPU_SYMBOL( rage::u32,	g_toggleNewSoASearches);
#else
	extern rage::u32 g_toggleNewSoASearches;
#endif
#endif

namespace SearchEntitiesStats
{
	PF_PAGE(SearchEntitiesPage,"Search Entities");

	PF_GROUP(SearchEntities);
	PF_LINK(SearchEntitiesPage, SearchEntities);
	PF_TIMER(fwSearchEntitiesRun, SearchEntities);
}
using namespace SearchEntitiesStats;

namespace rage {

inline void ProcessVolumeIsSphereInside(fwEntityDescSoA &soa, int blockCount, Vec4V_In x, Vec4V_In y, Vec4V_In z, Vec4V_In radius, Vec4V* hitEntPtrsV, Vec4V* entPtrsV);
inline void ProcessVolumeIsSphereIntersecting(fwEntityDescSoA &soa, int blockCount, Vec4V_In x, Vec4V_In y, Vec4V_In z, Vec4V_In radius, Vec4V* hitEntPtrsV, Vec4V* entPtrsV);
inline void ProcessVolumeIsSphereIntersectingVisible(fwEntityDescSoA &soa, int blockCount, Vec4V_In x, Vec4V_In y, Vec4V_In z, Vec4V_In radius, Vec4V* hitEntPtrsV, Vec4V* entPtrsV);
inline void ProcessVolumeIsSphereIntersectingBBApprox(fwEntityDescSoA &soa, int blockCount, Vec4V_In x, Vec4V_In y, Vec4V_In z, Vec4V_In radius, Vec4V* hitEntPtrsV, Vec4V* entPtrsV);

inline void ProcessVolumeIsBoxInside(fwEntityDescSoA &soa, int blockCount, 
									 Vec4V_In testMaxX, Vec4V_In testMaxY, Vec4V_In testMaxZ, 
									 Vec4V_In testMinX, Vec4V_In testMinY, Vec4V_In testMinZ,
									 Vec4V* hitEntityPointersV, Vec4V* entityPointersV);

inline void ProcessVolumeIsBoxIntersectingBBApprox(fwEntityDescSoA &soa, int blockCount, 
												   Vec4V_In testMaxX, Vec4V_In testMaxY, Vec4V_In testMaxZ, 
												   Vec4V_In testMinX, Vec4V_In testMinY, Vec4V_In testMinZ,
												   Vec4V* hitEntityPointersV, Vec4V* entityPointersV);

inline void ProcessVolumeIsBoxIntersectingBB(fwEntityDescSoA &soa, int blockCount, 
											 Vec4V_In testMaxX, Vec4V_In testMaxY, Vec4V_In testMaxZ, 
											 Vec4V_In testMinX, Vec4V_In testMinY, Vec4V_In testMinZ,
											 Vec4V* hitEntityPointersV, Vec4V* entityPointersV);

bool SearchEntities_Dependency(const sysDependency& dependency)
{
	PF_FUNC(fwSearchEntitiesRun);

	sysTimer			timer;
	fwSearchEntities	searchEntities;

	u8*					scratchBuffer = static_cast< u8* >( dependency.m_Params[0].m_AsPtr );
	const int			scratchBufferSize = dependency.m_DataSizes[0];
	fwSearchConstants*	searchConstants = static_cast< fwSearchConstants* >( dependency.m_Params[1].m_AsPtr );
	fwEntityDesc*		entityDescs = static_cast< fwEntityDesc* >( dependency.m_Params[2].m_AsPtr );
	const int			entityDescCount = dependency.m_DataSizes[2] / sizeof(fwEntityDesc);
	const fwFlags16		flags( dependency.m_Params[3].m_AsShort.m_Low );
	sysEaPtr< u32 >		searchPendingJobCountEa = static_cast< u32* >( dependency.m_Params[4].m_AsPtr );
#if __STATS
	sysEaPtr< u32 >		spuSearchEntitiesTimeEa = static_cast< u32* >( dependency.m_Params[5].m_AsPtr );
#endif

	searchEntities.SetSearchConstants( searchConstants );
	searchEntities.SetEntityDescs( entityDescs );
	searchEntities.SetEntityDescsCount( entityDescCount );
	searchEntities.SetFlags( flags );
	searchEntities.AssignScratchBuffer( scratchBuffer, scratchBufferSize );
	searchEntities.Process();

#if __STATS
	sysInterlockedAdd( spuSearchEntitiesTimeEa.ToPtr(), static_cast< u32 >( timer.GetUsTime() ) );
#endif

	sysInterlockedDecrement( searchPendingJobCountEa.ToPtr() );

	return true;
}

void fwSearchEntities::AssignScratchBuffer(u8* memory, const int ASSERT_ONLY(size))
{
	ASSERT_ONLY( u8*	memoryStart = memory; )

	m_searchEntries = rage_placement_new( memory ) SearchEntryArray;
	memory += sizeof( SearchEntryArray );

	Assertf( memory - memoryStart <= size, "Not enough scratch buffer allocated for SearchEntities" );
}

#if __SPU
void fwSearchEntities::ResetSearchEntries() {
	m_searchEntries->Reset();
}

void fwSearchEntities::AddSearchEntry(const sysEaPtr<fwEntity> entity) {
	m_searchEntries->Append() = entity;
}

void fwSearchEntities::SubmitSearchEntries()
{
	sysEaPtr<fwEntity>*		entries = m_searchEntries->GetElements();
	const int				entryCount = m_searchEntries->GetCount();

	if ( !entryCount )
		return;

	const int				paddedEntryCount = ( entryCount + 3 ) & ~3;
	Assert( paddedEntryCount <= m_searchEntries->GetMaxCount() );
	memset( &entries[entryCount], 0, sizeof(u32) * (paddedEntryCount - entryCount) );

	while(true)
	{
		u32	entryIndex = sysInterlockedRead( m_searchConstants->m_entriesCountPtr );

		// Ignore all the entries beyond the result capacity for this search
		Assertf( (entryIndex + paddedEntryCount) <= m_searchConstants->m_entriesCapacity, "Current world search found more entries than fitting in provided memory: further entries will be discarded." );
		if ( (entryIndex + paddedEntryCount) > m_searchConstants->m_entriesCapacity )
		{
			break;
		}

		if(sysInterlockedCompareExchange(m_searchConstants->m_entriesCountPtr, entryIndex + paddedEntryCount, entryIndex) == entryIndex)
		{
			sysDmaPutAndWait( (fwEntity*) entries, (u32)( m_searchConstants->m_entriesArray + entryIndex ), paddedEntryCount * sizeof(u32), g_auxDmaTag );
			break;
		}		
	}
}
#else
void fwSearchEntities::ResetSearchEntries() { }
void fwSearchEntities::SubmitSearchEntries() { }

void fwSearchEntities::AddSearchEntry(const sysEaPtr<fwEntity> entity)
{
	u32	entryIndex = sysInterlockedIncrement( m_searchConstants->m_entriesCountPtr ) - 1;

	// Ignore all the entities beyond the result capacity for this search
	Assertf( entryIndex < m_searchConstants->m_entriesCapacity, "Current world search found more entities than fitting in provided memory: further entities will be discarded. Index %d Capacity %d", entryIndex, m_searchConstants->m_entriesCapacity);
	if(Unlikely(entryIndex >= m_searchConstants->m_entriesCapacity))
	{
#if HEIGHTMAP_GENERATOR_TOOL
		Quitf("fwSearchEntities::AddSearchEntry overflow - capacity=%d", m_searchConstants->m_entriesCapacity);
#endif
		sysInterlockedExchange(m_searchConstants->m_entriesCountPtr, m_searchConstants->m_entriesCapacity);
		return;
	}

	m_searchConstants->m_entriesArray[ entryIndex ] = entity.ToPtr();
}
#endif


void fwSearchEntities::Process()
{
	const u32			optionFlags = m_searchConstants->m_optionFlags;
	const u32			typeFlags = m_searchConstants->m_typeFlags;
	const u32			lodFlags = m_searchConstants->m_lodFlags;
	fwSearchVolume*		collisionVolume = &m_searchConstants->m_collisionVolume;

	ResetSearchEntries();

#if __BANK
#if __SPU
	rage::u32 toggleNewSearches = sysInterlockedRead(PPU_SYMBOL(g_toggleNewSoASearches));
#else 
	rage::u32 toggleNewSearches = g_toggleNewSoASearches;
#endif //__SPU

	if (toggleNewSearches && m_flags.IsFlagSet(FLAG_ENTITY_DESC_SOA) && collisionVolume->m_type != SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB)
#else 
	if (m_flags.IsFlagSet(FLAG_ENTITY_DESC_SOA) && collisionVolume->m_type != SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB)
#endif //__BANK
	{
		fwEntityDescSoA		soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_entityDescs, m_entityDescsCount );
		ALIGNAS(16)	fwEntity* hitEntityPointers[ SEARCH_MAX_ENTITIES_PER_JOB ] ;
		
		{
			fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_searchEntitiesSoaIntersectTimer) );

			const int			blockCount = static_cast< int >( m_entityDescsCount >> 2 );
			Vec4V*				hitEntityPointersV = reinterpret_cast< Vec4V* >( hitEntityPointers );
			Vec4V*				entityPointersV = reinterpret_cast< Vec4V* >( soa.m_entityPointers );

			if (collisionVolume->m_type >= SEARCH_VOLUME_IS_SPHERE_INSIDE && 
				collisionVolume->m_type <= SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX)
			{
				const Vec4V	testSphereV = collisionVolume->m_sphere.GetV4();
				const Vec4V	testSphereX( testSphereV.GetX() );
				const Vec4V	testSphereY( testSphereV.GetY() );
				const Vec4V	testSphereZ( testSphereV.GetZ() );
				const Vec4V	testSphereRadius( testSphereV.GetW() );

				switch (collisionVolume->m_type)
				{
					case SEARCH_VOLUME_IS_SPHERE_INSIDE:
					{
						ProcessVolumeIsSphereInside(soa, blockCount, testSphereX, testSphereY, testSphereZ, testSphereRadius, hitEntityPointersV, entityPointersV);
						break;
					}
					case SEARCH_VOLUME_IS_SPHERE_INTERSECTING:
					{
						ProcessVolumeIsSphereIntersecting(soa, blockCount, testSphereX, testSphereY, testSphereZ, testSphereRadius, hitEntityPointersV, entityPointersV);
						break;
					}
					case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_VISIBLE:
					{
						ProcessVolumeIsSphereIntersectingVisible(soa, blockCount, testSphereX, testSphereY, testSphereZ, testSphereRadius, hitEntityPointersV, entityPointersV);
						break;
					}
					case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX:
					{
						ProcessVolumeIsSphereIntersectingBBApprox(soa, blockCount, testSphereX, testSphereY, testSphereZ, testSphereRadius, hitEntityPointersV, entityPointersV);
						break;
					}
					default:
					{
						Assertf(0, "Error unknown sphere search type");
					}
				}
			}
			else
			{
				const Vec4V testMaxV(collisionVolume->m_boundingBox.GetMax());
				const Vec4V	testMinV( collisionVolume->m_boundingBox.GetMin());

				const Vec4V	testMaxX( testMaxV.GetX() );
				const Vec4V	testMaxY( testMaxV.GetY() );
				const Vec4V	testMaxZ( testMaxV.GetZ() );

				const Vec4V	testMinX( testMinV.GetX() );
				const Vec4V	testMinY( testMinV.GetY() );
				const Vec4V	testMinZ( testMinV.GetZ() );

				switch (collisionVolume->m_type)
				{
					case SEARCH_VOLUME_IS_BOX_INSIDE:
					{
						ProcessVolumeIsBoxInside(soa, blockCount,
												testMaxX, testMaxY, testMaxZ,
												testMinX, testMinY, testMinZ,
												hitEntityPointersV, entityPointersV);
						break;
					}
					case SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX:
					{
						ProcessVolumeIsBoxIntersectingBBApprox(soa, blockCount, 
																testMaxX, testMaxY, testMaxZ,
																testMinX, testMinY, testMinZ,
																hitEntityPointersV, entityPointersV);
						break;
					}
					case SEARCH_VOLUME_IS_BOX_INTERSECTING_BB:
					{
						ProcessVolumeIsBoxIntersectingBB(soa, blockCount,
														testMaxX, testMaxY, testMaxZ,
														testMinX, testMinY, testMinZ,
														hitEntityPointersV, entityPointersV);
						break;
					}
					default:
					{
						Assertf(0, "Error unknown box search type");
					}
				}
			}
		}

		{
			fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_searchEntitiesSoaIterateTimer) );

			for (int i = 0; i < (int) m_entityDescsCount; ++i)
			{
				fwEntity*	entity = hitEntityPointers[i];
				if ( entity )
				{
					const fwEntityDescFlags		flags = soa.m_flags[i];
					const bool					isHighDetail = (flags.m_bLodType == LODTYPES_DEPTH_ORPHANHD || flags.m_bLodType == LODTYPES_DEPTH_HD);
					bool						entityHit = true;

					entityHit = entityHit && ( flags.m_bIsSearchable );
					entityHit = entityHit && ( typeFlags & (1 << flags.m_nEntityType) );
					entityHit = entityHit && ( lodFlags & (isHighDetail ? SEARCH_LODTYPE_HIGHDETAIL : SEARCH_LODTYPE_LOWDETAIL) );

					if ( entityHit )
						AddSearchEntry( entity );
				}
			}
		}
	}
	else
	{
		fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_searchEntitiesNonSoaTimer) );

		if ( m_flags.IsFlagSet(FLAG_ENTITY_DESC_SOA) )
		{
			const fwEntityDescSoA	soa = fwSoAEntityContainer::InterpretAsEntityDescSoa( m_entityDescs, m_entityDescsCount );

			for (int b = 0; b < static_cast< int >(m_entityDescsCount >> 2); ++b)
			{
				fwEntityDesc	block[4];
				fwSoAEntityContainer::GetEntityDescBlock( soa, b, block[0], block[1], block[2], block[3] );

				for (int i = 0; i < 4; ++i)
				{
					const fwEntityDesc&		entityDesc = block[i];
					fwEntity* const			entity = entityDesc.GetEntity();
					const bool isProcedural = entityDesc.GetIsProcedural();

					bool	entityHit = ( entity != NULL );
					entityHit = entityHit && ( entityDesc.IsSearchable() );
					entityHit = entityHit && ( typeFlags & ( 1 << entityDesc.GetEntityType() ) );
					entityHit = entityHit && ( lodFlags & ( entityDesc.IsLowDetail() ? SEARCH_LODTYPE_LOWDETAIL : SEARCH_LODTYPE_HIGHDETAIL ) );		// this should match up SearchTypes
					entityHit = entityHit &&  ( !isProcedural || (optionFlags & SEARCH_OPTION_SKIP_PROCEDURAL_ENTITIES)==0 );
					entityHit = entityHit && ( (*collisionVolume)(&entityDesc) );

					if ( entityHit )
						AddSearchEntry( entity );
				}
			}
		}
		else
		{
			for (int i = 0; i < static_cast< int >(m_entityDescsCount); ++i)
			{
				const fwEntityDesc&		entityDesc = m_entityDescs[i];
				fwEntity* const			entity = entityDesc.GetEntity();
				const bool isProcedural = entityDesc.GetIsProcedural();

				PrefetchObject( m_entityDescs + i + 1 );

				bool	entityHit = ( entity != NULL );
				entityHit = entityHit && ( entityDesc.IsSearchable() );
				entityHit = entityHit && ( typeFlags & ( 1 << entityDesc.GetEntityType() ) );
				entityHit = entityHit && ( lodFlags & ( entityDesc.IsLowDetail() ? SEARCH_LODTYPE_LOWDETAIL : SEARCH_LODTYPE_HIGHDETAIL ) );		// this should match up SearchTypes
				entityHit = entityHit && ( !isProcedural || (optionFlags & SEARCH_OPTION_SKIP_PROCEDURAL_ENTITIES)==0 );
				entityHit = entityHit && ( (*collisionVolume)(&entityDesc) );

				if ( entityHit )
					AddSearchEntry( entity );
			}
		}
	}

	SubmitSearchEntries();
}


inline void ProcessVolumeIsSphereInside(fwEntityDescSoA &soa, int blockCount, Vec4V_In testSphereX, Vec4V_In testSphereY, Vec4V_In testSphereZ, 
										Vec4V_In testSphereRadius, Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V zero(V_ZERO);
	Vec4V testSphereRadiusSq = Scale( testSphereRadius, testSphereRadius );

	for (int b = 0; b < blockCount; ++b)
	{
		const Vec4V		spheresX = soa.m_x[b];
		const Vec4V		spheresY = soa.m_y[b];
		const Vec4V		spheresZ = soa.m_z[b];

		const Vec4V		deltaX = Subtract(testSphereX, spheresX);
		const Vec4V		deltaY = Subtract(testSphereY, spheresY);
		const Vec4V		deltaZ = Subtract(testSphereZ, spheresZ);

		const Vec4V		deltaX2 = Scale( deltaX, deltaX );
		const Vec4V		deltaY2 = Scale( deltaY, deltaY );
		const Vec4V		deltaZ2 = Scale( deltaZ, deltaZ );

		const Vec4V		distance2 = Add( deltaX2, Add( deltaY2, deltaZ2 ) );
		const VecBoolV	intersect = IsLessThan( distance2, testSphereRadiusSq );

#if __64BIT
		hitEntityPointersV[0] = SelectFT( MergeXY(intersect, intersect), zero, entityPointersV[0] );
		hitEntityPointersV[1] = SelectFT( MergeZW(intersect, intersect), zero, entityPointersV[1] );
		hitEntityPointersV += 2;
		entityPointersV += 2;
#else
		hitEntityPointersV[b] = SelectFT( intersect, zero, entityPointersV[b] );
#endif
	}
}

inline void ProcessVolumeIsSphereIntersecting(fwEntityDescSoA &soa, int blockCount, Vec4V_In testSphereX, Vec4V_In testSphereY, Vec4V_In testSphereZ, 
											  Vec4V_In testSphereRadius, Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V zero(V_ZERO);

	for (int b = 0; b < blockCount; ++b)
	{
		const Vec4V		extentZandRadius = soa.m_extentZAndRadius[b];
		Vec4V			extentZandRadius01, extentZandRadius23;
		Float16Vec8Unpack( extentZandRadius01, extentZandRadius23, extentZandRadius );
		const Vec4V		spheresRadius( extentZandRadius01.GetY(), extentZandRadius01.GetW(), extentZandRadius23.GetY(), extentZandRadius23.GetW() );
		const Vec4V		spheresX = soa.m_x[b];
		const Vec4V		spheresY = soa.m_y[b];
		const Vec4V		spheresZ = soa.m_z[b];
		const Vec4V		deltaX = Subtract( spheresX, testSphereX );
		const Vec4V		deltaY = Subtract( spheresY, testSphereY );
		const Vec4V		deltaZ = Subtract( spheresZ, testSphereZ );
		const Vec4V		deltaX2 = Scale( deltaX, deltaX );
		const Vec4V		deltaY2 = Scale( deltaY, deltaY );
		const Vec4V		deltaZ2 = Scale( deltaZ, deltaZ );
		const Vec4V		distance2 = Add( deltaX2, Add( deltaY2, deltaZ2 ) );
		const Vec4V		radiiSum = Add( spheresRadius, testSphereRadius );
		const Vec4V		radiiSum2 = Scale( radiiSum, radiiSum );
		const VecBoolV	intersect = IsLessThan( distance2, radiiSum2 );

#if __64BIT
		hitEntityPointersV[0] = SelectFT( MergeXY(intersect, intersect), zero, entityPointersV[0] );
		hitEntityPointersV[1] = SelectFT( MergeZW(intersect, intersect), zero, entityPointersV[1] );
		hitEntityPointersV += 2;
		entityPointersV += 2;
#else
		hitEntityPointersV[b] = SelectFT( intersect, zero, entityPointersV[b] );
#endif
	}
}


inline void ProcessVolumeIsSphereIntersectingVisible(fwEntityDescSoA &soa, int blockCount, Vec4V_In testSphereX, Vec4V_In testSphereY, Vec4V_In testSphereZ, 
													 Vec4V_In testSphereRadius, Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V zero(V_ZERO);
	Vec4V lodTypesDist(LODTYPES_FADE_DISTF, LODTYPES_FADE_DISTF, LODTYPES_FADE_DISTF, LODTYPES_FADE_DISTF);

	for (int b = 0; b < blockCount; ++b)
	{
		const Vec4V		extentZandRadius = soa.m_extentZAndRadius[b];
		Vec4V			extentZandRadius01, extentZandRadius23;
		Float16Vec8Unpack( extentZandRadius01, extentZandRadius23, extentZandRadius );
		const Vec4V		spheresRadius( extentZandRadius01.GetY(), extentZandRadius01.GetW(), extentZandRadius23.GetY(), extentZandRadius23.GetW() );

		const Vec4V		spheresX = soa.m_x[b];
		const Vec4V		spheresY = soa.m_y[b];
		const Vec4V		spheresZ = soa.m_z[b];

		// const Vector3 delta = VEC3V_TO_VECTOR3(m_sphere.GetCenter()) - VEC3V_TO_VECTOR3(entitySphere.GetCenter());
		const Vec4V		deltaX = Subtract(testSphereX, spheresX);
		const Vec4V		deltaY = Subtract(testSphereY, spheresY);
		const Vec4V		deltaZ = Subtract(testSphereZ, spheresZ);

		// const float distance2 = diff.Mag2();
		const Vec4V		deltaX2 = Scale(deltaX, deltaX);
		const Vec4V		deltaY2 = Scale(deltaY, deltaY);
		const Vec4V		deltaZ2 = Scale(deltaZ, deltaZ);
		const Vec4V		distance2 = Add(deltaX2, Add(deltaY2, deltaZ2));

		// const float radiiSum = m_sphere.GetRadiusf() + entitySphere.GetRadiusf();
		const Vec4V		radiiSum = Add(testSphereRadius, spheresRadius);

		// const float radiiSum2 = radiiSum*radiiSum
		const Vec4V		radiiSum2 = Scale(radiiSum, radiiSum);

		// if (distance2 < radiiSum2)
		const VecBoolV	intersect = IsLessThan( distance2, radiiSum2 );

		const int		i = b << 2;

		const ScalarV	lodDist0( float(soa.m_lodDistBackups[i+0]) );
		const ScalarV	lodDist1( float(soa.m_lodDistBackups[i+1]) );
		const ScalarV	lodDist2( float(soa.m_lodDistBackups[i+2]) );
		const ScalarV	lodDist3( float(soa.m_lodDistBackups[i+3]) );
		const Vec4V		lodDist( lodDist0, lodDist1, lodDist2, lodDist3 );

		//	const float lodDistFade = (float) (pEntityDesc->GetLodDistCached() + LODTYPES_FADE_DIST);
		const Vec4V lodDistFade = Add(lodDist, lodTypesDist);

		//	const float lodDistFade2 = lodDistFade * lodDistFade;
		const Vec4V lodDistFade2 = Scale(lodDistFade, lodDistFade);

		Vec4V			vecToLodX;
		Vec4V			vecToLodY;
		Vec4V			vecToLodZ;
		Vec4V			unused;
		SoAUtils::Float16Unpack( soa.m_vecToLodXY[b], vecToLodX, vecToLodY );
		SoAUtils::Float16Unpack( soa.m_vecToLodZandFlags2[b], vecToLodZ, unused );

		const Vec4V		lodPosX = Add(spheresX, vecToLodX);
		const Vec4V		lodPosY = Add(spheresY, vecToLodY);
		const Vec4V		lodPosZ = Add(spheresZ, vecToLodZ);

		// const Vector3 lodDiff = VEC3V_TO_VECTOR3( m_sphere.GetCenter() - pEntityDesc->GetLodPosition() );
		const Vec4V		lodDiffX = Subtract(testSphereX, lodPosX);
		const Vec4V		lodDiffY = Subtract(testSphereY, lodPosY);
		const Vec4V		lodDiffZ = Subtract(testSphereZ, lodPosZ);

		// float lodDiff2 = lodDiff.Mag2();
		const Vec4V		lodDiffX2 = Scale(lodDiffX, lodDiffX);
		const Vec4V		lodDiffY2 = Scale(lodDiffY, lodDiffY);
		const Vec4V		lodDiffZ2 = Scale(lodDiffZ, lodDiffZ);
		const Vec4V		lodDiff2 = Add( lodDiffX2, Add( lodDiffY2, lodDiffZ2 ) );

		// if (distanceToLod2<=(lodDist*lodDist))
		const VecBoolV	lodDiffIntersect = IsLessThanOrEqual(lodDiff2, lodDistFade2 );

		const VecBoolV resultIntersect = And(intersect, lodDiffIntersect);


#if __64BIT
		hitEntityPointersV[0] = SelectFT( MergeXY(resultIntersect, resultIntersect), zero, entityPointersV[0] );
		hitEntityPointersV[1] = SelectFT( MergeZW(resultIntersect, resultIntersect), zero, entityPointersV[1] );
		hitEntityPointersV += 2;
		entityPointersV += 2;
#else
		hitEntityPointersV[b] = SelectFT( resultIntersect, zero, entityPointersV[b] );
#endif
	}
}

inline void ProcessVolumeIsSphereIntersectingBBApprox(fwEntityDescSoA &soa, int blockCount, Vec4V_In testSphereX, Vec4V_In testSphereY, Vec4V_In testSphereZ, 
													  Vec4V_In testSphereRadius, Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V testMinX = testSphereX - testSphereRadius;
	Vec4V testMinY = testSphereY - testSphereRadius;
	Vec4V testMinZ = testSphereZ - testSphereRadius;

	Vec4V testMaxX = testSphereX + testSphereRadius;
	Vec4V testMaxY = testSphereY + testSphereRadius;
	Vec4V testMaxZ = testSphereZ + testSphereRadius;

	ProcessVolumeIsBoxIntersectingBB(soa, blockCount, testMaxX, testMaxY, testMaxZ, testMinX, testMinY, testMinZ, hitEntityPointersV, entityPointersV);
}


inline void ProcessVolumeIsBoxIntersectingBB(fwEntityDescSoA &soa, int blockCount, 
										     Vec4V_In testMaxX, Vec4V_In testMaxY, Vec4V_In testMaxZ, 
										     Vec4V_In testMinX, Vec4V_In testMinY, Vec4V_In testMinZ,
										     Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V zero(V_ZERO);

	for (int b = 0; b < blockCount; ++b)
	{
		const Vec4V		centreX = soa.m_x[b];
		const Vec4V		centreY = soa.m_y[b];
		const Vec4V		centreZ = soa.m_z[b];

		Vec4V			extentX;
		Vec4V			extentY;
		Vec4V			extentZ;
		Vec4V			unusedRadius;
		SoAUtils::Float16Unpack( soa.m_extentXY[b], extentX, extentY );
		SoAUtils::Float16Unpack( soa.m_extentZAndRadius[b], extentZ, unusedRadius );

		Vec4V maxX = centreX + extentX;
		Vec4V maxY = centreY + extentY;
		Vec4V maxZ = centreZ + extentZ;

		Vec4V minX = centreX - extentX;
		Vec4V minY = centreY - extentY;
		Vec4V minZ = centreZ - extentZ;

		VecBoolV comp0X   = IsGreaterThanOrEqual(maxX, testMinX);
		VecBoolV comp0XY  = And(IsGreaterThanOrEqual(maxY, testMinY), comp0X);
		VecBoolV comp0XYZ = And(IsGreaterThanOrEqual(maxZ, testMinZ),comp0XY);

		VecBoolV comp1X   = IsLessThanOrEqual(minX, testMaxX);
		VecBoolV comp1XY  = And(IsLessThanOrEqual(minY, testMaxY), comp1X);
		VecBoolV comp1XYZ = And(IsLessThanOrEqual(minZ, testMaxZ),comp1XY);

		const VecBoolV intersect = And(comp0XYZ, comp1XYZ);

#if __64BIT
		hitEntityPointersV[0] = SelectFT( MergeXY(intersect, intersect), zero, entityPointersV[0] );
		hitEntityPointersV[1] = SelectFT( MergeZW(intersect, intersect), zero, entityPointersV[1] );
		hitEntityPointersV += 2;
		entityPointersV += 2;
#else
		hitEntityPointersV[b] = SelectFT( intersect, zero, entityPointersV[b] );
#endif
	}
}

inline void ProcessVolumeIsBoxIntersectingBBApprox(fwEntityDescSoA &soa, int blockCount, 
											 Vec4V_In testMaxX, Vec4V_In testMaxY, Vec4V_In testMaxZ, 
											 Vec4V_In testMinX, Vec4V_In testMinY, Vec4V_In testMinZ,
											 Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V zero(V_ZERO);

	for (int b = 0; b < blockCount; ++b)
	{

		const Vec4V		centreX = soa.m_x[b];
		const Vec4V		centreY = soa.m_y[b];
		const Vec4V		centreZ = soa.m_z[b];

		Vec4V			unusedExtentZ;
		Vec4V			radius;
		SoAUtils::Float16Unpack( soa.m_extentZAndRadius[b], unusedExtentZ, radius );

		Vec4V maxX = centreX + radius;
		Vec4V maxY = centreY + radius;
		Vec4V maxZ = centreZ + radius;

		Vec4V minX = centreX - radius;
		Vec4V minY = centreY - radius;
		Vec4V minZ = centreZ - radius;

		VecBoolV comp0X   = IsGreaterThanOrEqual(maxX, testMinX);
		VecBoolV comp0XY  = And(IsGreaterThanOrEqual(maxY, testMinY), comp0X);
		VecBoolV comp0XYZ = And(IsGreaterThanOrEqual(maxZ, testMinZ),comp0XY);

		VecBoolV comp1X   = IsLessThanOrEqual(minX, testMaxX);
		VecBoolV comp1XY  = And(IsLessThanOrEqual(minY, testMaxY), comp1X);
		VecBoolV comp1XYZ = And(IsLessThanOrEqual(minZ, testMaxZ),comp1XY);

		const VecBoolV intersect = And(comp0XYZ, comp1XYZ);

#if __64BIT
		hitEntityPointersV[0] = SelectFT( MergeXY(intersect, intersect), zero, entityPointersV[0] );
		hitEntityPointersV[1] = SelectFT( MergeZW(intersect, intersect), zero, entityPointersV[1] );
		hitEntityPointersV += 2;
		entityPointersV += 2;
#else
		hitEntityPointersV[b] = SelectFT( intersect, zero, entityPointersV[b] );
#endif
	}
}

inline void ProcessVolumeIsBoxInside(fwEntityDescSoA &soa, int blockCount, 
									 Vec4V_In testMaxX, Vec4V_In testMaxY, Vec4V_In testMaxZ, 
									 Vec4V_In testMinX, Vec4V_In testMinY, Vec4V_In testMinZ,
									 Vec4V* hitEntityPointersV, Vec4V* entityPointersV)
{
	Vec4V zero(V_ZERO);

	for (int b = 0; b < blockCount; ++b)
	{
		const Vec4V		centreX = soa.m_x[b];
		const Vec4V		centreY = soa.m_y[b];
		const Vec4V		centreZ = soa.m_z[b];

		VecBoolV comp0X   = IsGreaterThanOrEqual(centreX, testMinX);
		VecBoolV comp0XY  = And(IsGreaterThanOrEqual(centreY, testMinY), comp0X);
		VecBoolV comp0XYZ = And(IsGreaterThanOrEqual(centreZ, testMinZ),comp0XY);

		VecBoolV comp1X   = IsLessThanOrEqual(centreX, testMaxX);
		VecBoolV comp1XY  = And(IsLessThanOrEqual(centreY, testMaxY), comp1X);
		VecBoolV comp1XYZ = And(IsLessThanOrEqual(centreZ, testMaxZ),comp1XY);

		const VecBoolV intersect = And(comp0XYZ, comp1XYZ);

#if __64BIT
		hitEntityPointersV[0] = SelectFT( MergeXY(intersect, intersect), zero, entityPointersV[0] );
		hitEntityPointersV[1] = SelectFT( MergeZW(intersect, intersect), zero, entityPointersV[1] );
		hitEntityPointersV += 2;
		entityPointersV += 2;
#else
		hitEntityPointersV[b] = SelectFT( intersect, zero, entityPointersV[b] );
#endif
	}
}


} // namespace rage

#endif // !__TOOL
