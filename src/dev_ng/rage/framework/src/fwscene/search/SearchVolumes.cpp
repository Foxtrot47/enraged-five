//
#include "vector/geometry.h"

#include "fwscene/search/SearchVolumes.h"
#include "fwscene/lod/LodTypes.h"

#if __BANK
#include <stdio.h>
#endif

namespace rage {

bool fwSearchVolume::SearchVolumesIntersect(const spdSphere& entitySphere, const spdAABB& entityBox, const fwEntityDesc* pEntityDesc) const
{
	switch (m_type)
	{
		case SEARCH_VOLUME_IS_SPHERE_INSIDE:
		{
			// [SPHERE-OPTIMISE]
			const Vector3 diff = VEC3V_TO_VECTOR3(m_sphere.GetCenter()) - VEC3V_TO_VECTOR3(entitySphere.GetCenter());
			const float distance2 = diff.Mag2();
			if (distance2 < m_sphere.GetRadiusSquaredf())
				return true;
			return false;
		}

		case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_VISIBLE:
		{
			// [SPHERE-OPTIMISE]
			const Vector3 diff = VEC3V_TO_VECTOR3(m_sphere.GetCenter()) - VEC3V_TO_VECTOR3(entitySphere.GetCenter());
			const float distance2 = diff.Mag2();
			const float rangePlusRadius = m_sphere.GetRadiusf() + entitySphere.GetRadiusf();
			if (distance2 < (rangePlusRadius*rangePlusRadius))
			{
				const float lodDist = (float) (pEntityDesc->GetLodDistCached() + LODTYPES_FADE_DIST);
				const Vector3 lodDiff = VEC3V_TO_VECTOR3( m_sphere.GetCenter() - pEntityDesc->GetLodPosition() );
				float distanceToLod2 = lodDiff.Mag2();
				return distanceToLod2<=(lodDist*lodDist);
			}
			return false;
		}

		case SEARCH_VOLUME_IS_SPHERE_INTERSECTING:
		{
			// [SPHERE-OPTIMISE]
			const Vector3 diff = VEC3V_TO_VECTOR3(m_sphere.GetCenter()) - VEC3V_TO_VECTOR3(entitySphere.GetCenter());
			const float distance2 = diff.Mag2();
			const float rangePlusRadius = m_sphere.GetRadiusf() + entitySphere.GetRadiusf();
			if (distance2 < (rangePlusRadius*rangePlusRadius))
				return true;
			return false;
		}

		case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX:
			return entityBox.IntersectsAABB(spdAABB(m_sphere));

		case SEARCH_VOLUME_IS_BOX_INSIDE:
			return m_boundingBox.ContainsPoint(entitySphere.GetCenter());

		case SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX:
			return m_boundingBox.IntersectsAABB(spdAABB(entitySphere));

		case SEARCH_VOLUME_IS_BOX_INTERSECTING_BB:
			return entityBox.IntersectsAABB(m_boundingBox);

		case SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB:
		{
			return !!geomBoxes::TestSegmentToBoxMinMax(m_vec1, m_vec2, entityBox.GetMinVector3(), entityBox.GetMaxVector3());
		}

		default:
			return false;
	}
}

#if __BANK
void fwSearchVolume::GetStringDescription(char* str)
{
	const Vector3 center = VEC3V_TO_VECTOR3(m_sphere.GetCenter());

	switch (m_type)
	{
		case SEARCH_VOLUME_IS_SPHERE_INSIDE:
			sprintf( str, "IS_SPHERE_INSIDE                        sphere( %f %f %f %f )", center.x, center.y, center.z, m_sphere.GetRadiusf() );
		break;

		case SEARCH_VOLUME_IS_SPHERE_INTERSECTING:
			sprintf( str, "IS_SPHERE_INTERSECTING                  sphere( %f %f %f %f )", center.x, center.y, center.z, m_sphere.GetRadiusf() );
		break;

		case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_VISIBLE:
			sprintf( str, "IS_SPHERE_INTERSECTING_VISIBLE		   sphere( %f %f %f %f )", center.x, center.y, center.z, m_sphere.GetRadiusf() );
		break;

		case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX:
			sprintf( str, "IS_SPHERE_INTERSECTING_BB_APPROX        sphere( %f %f %f %f )", center.x, center.y, center.z, m_sphere.GetRadiusf() );
		break;

		case SEARCH_VOLUME_IS_BOX_INSIDE:
			sprintf( str, "IS_BOX_INSIDE                           box( %f %f %f, %f %f %f )", m_boundingBox.GetMinVector3().x, m_boundingBox.GetMinVector3().y, m_boundingBox.GetMinVector3().z, m_boundingBox.GetMaxVector3().x, m_boundingBox.GetMaxVector3().y, m_boundingBox.GetMaxVector3().z );
		break;

		case SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX:
			sprintf( str, "IS_BOX_INTERSECTING_APPROX              box( %f %f %f, %f %f %f )", m_boundingBox.GetMinVector3().x, m_boundingBox.GetMinVector3().y, m_boundingBox.GetMinVector3().z, m_boundingBox.GetMaxVector3().x, m_boundingBox.GetMaxVector3().y, m_boundingBox.GetMaxVector3().z );
		break;

		case SEARCH_VOLUME_IS_BOX_INTERSECTING_BB:
			sprintf( str, "IS_BOX_INTERSECTING_BB                  box( %f %f %f, %f %f %f )", m_boundingBox.GetMinVector3().x, m_boundingBox.GetMinVector3().y, m_boundingBox.GetMinVector3().z, m_boundingBox.GetMaxVector3().x, m_boundingBox.GetMaxVector3().y, m_boundingBox.GetMaxVector3().z );
		break;

		case SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB:
			sprintf( str, "IS_LINE_SEG_INTERSECTING_BB             vec1( %f %f %f ) vec2( %f %f %f )", m_vec1.x, m_vec1.y, m_vec1.z, m_vec2.x, m_vec2.y, m_vec2.z );
		break;

		default:
		break;
	}
}
#endif // __BANK

} // namespace rage
