rage::u32	g_auxDmaTag = 0;

#include "system/dependency.h"
#include "system/dma.h"
#include "fwscene/search/SearchEntities.cpp"
#include "fwscene/search/SearchVolumes.cpp"
#include "fwscene/world/EntityDesc.cpp"
#include "fwscene/world/EntityContainer.cpp"
#include "vector/geometry.cpp"

#include "system/dependencyscheduler.cpp"

using rage::sysDependency;

SPUFRAG_DECL(bool, SearchEntities, sysDependency&);
SPUFRAG_IMPL(bool, SearchEntities, sysDependency& dependency) {
	return SearchEntities_Dependency( dependency );
}
