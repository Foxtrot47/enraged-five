//////////////
// Includes //
//////////////

#if !__TOOL

#include "fwgeovis/geovis.h"
#include "fwscene/search/Search.h"
#include "fwscene/scan/ScanPerformance.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwscene/world/WorldRepBase.h"
#include "fwutil/xmacro.h"

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "profile/group.h"
#include "profile/page.h"
#include "profile/element.h"
#include "system/cache.h"
#include "system/dependency.h"
#include "system/taskheader.h"

#if __BANK
	#include "entity/entity.h"
	#include "fwscene/world/WorldRepMulti.h"
	#include "grcore/debugdraw.h"
	#include "grcore/setup.h"
#endif
//////////////////////
// Global variables //
//////////////////////

#define __VALIDATE_SEARCH_VOLUMES __DEV

namespace rage { bool SearchNodes_Dependency(const sysDependency& dependency); }

#if __BANK
rage::u32				g_toggleNewSoASearches = 1;
#endif

namespace rage {

enum {
	DEFAULT_SEARCH_RESULT_CAPACITY	= (HEIGHTMAP_GENERATOR_TOOL && HACK_RDR3) ? 8000 : 5120
};

fwSearch	fwSearch::ms_defaultSearch( DEFAULT_SEARCH_RESULT_CAPACITY );

bool		fwSearch::ms_useSpinWaiting = true;
u32			fwSearch::ms_filterSearchMask = 0;

//////////////////
// Profile data //
//////////////////

PF_PAGE( Search, "Search" );
PF_GROUP( Search_All );
PF_GROUP( Search_Filtered );
PF_GROUP( Search_ForAllEntitiesIntersecting );
PF_LINK( Search, Search_All );
PF_LINK( Search, Search_Filtered );
PF_LINK( Search, Search_ForAllEntitiesIntersecting );

PF_COUNTER( SearchCount, Search_All );
PF_COUNTER( SpuSearchCount, Search_All );
PF_COUNTER( PpuSearchCount, Search_All );
PF_COUNTER_FLOAT( SpuNodeProcessTime, Search_All );
PF_TIMER( StartTotal, Search_All );
PF_TIMER( FinalizeTotal, Search_All );

PF_COUNTER( FilteredSearchCount, Search_Filtered );
PF_COUNTER_FLOAT( FilteredSpuNodeProcessTime, Search_Filtered );
PF_TIMER( FilteredStartTotal, Search_Filtered );
PF_TIMER( FilteredFinalizeTotal, Search_Filtered );

PF_COUNTER( ForAllEntitiesIntersectingCount, Search_ForAllEntitiesIntersecting );
PF_TIMER( ForAllEntitiesIntersectingTotal, Search_ForAllEntitiesIntersecting );
PF_TIMER( ForAllEntitiesIntersectingSearch, Search_ForAllEntitiesIntersecting );
PF_TIMER( ForAllEntitiesIntersectingCallbacks, Search_ForAllEntitiesIntersecting );

#if __BANK
bool  fwSearch::ms_enableSearchDebugTest = false;
float fwSearch::ms_debugSearchRadius = 1.5f;
float fwSearch::ms_debugSearchDistanceAlongForwardVector = 3.0f;
int   fwSearch::ms_debugSearchSearchVolumeType = SEARCH_VOLUME_IS_SPHERE_INSIDE;
Vector3 fwSearch::ms_debugSearhSpherePos(0.0f, 0.0f, 0.0f);
Vector3 fwSearch::ms_debugSearchBoxExtents(fwSearch::ms_debugSearchRadius, fwSearch::ms_debugSearchRadius, fwSearch::ms_debugSearchRadius);

static const char *sShowSearchTypes[] =
{
	"Is Sphere Inside",
	"Is Sphere Intersecting",
	"Is Sphere Intersecting Visible",
	"Is Sphere Intersecting BoundBox Approx",
	"Is Box Inside",
	"Is_Box Intersecting Approx",
	"Is Box Intersecting Bounding Box",
	"Is Line Seg Intersecting Bounding Box"
};

///////////////////
// Debug methods //
///////////////////

void fwSearch::InitWidgets()
{
	bkBank * pBank = BANKMGR.FindBank( "World Scan & Search" );
	if (!pBank)
		pBank = &BANKMGR.CreateBank( "World Scan & Search" );

	pBank->PushGroup( "Search", true );
		pBank->AddToggle( "Use spin waiting", &ms_useSpinWaiting );

		pBank->AddToggle("Enable Search Debug Tests", &ms_enableSearchDebugTest );
		pBank->AddSlider("Debug radius", &ms_debugSearchRadius, 0.0f, 1000.0f, 0.05f);
		pBank->AddCombo("Search Type",&ms_debugSearchSearchVolumeType, SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB + 1, sShowSearchTypes);

		pBank->AddSlider("Sphere Pos X",&ms_debugSearhSpherePos.x, -8000.0f, 8000.0f, 0.1f);
		pBank->AddSlider("Sphere Pos Y",&ms_debugSearhSpherePos.y, -8000.0f, 8000.0f, 0.1f);
		pBank->AddSlider("Sphere Pos Z",&ms_debugSearhSpherePos.z, -8000.0f, 8000.0f, 0.1f);

		pBank->AddSlider("Bounding Box Extents X",&ms_debugSearchBoxExtents.x, 0.0f, 1000.0f, 0.05f);
		pBank->AddSlider("Bounding Box Extents Y",&ms_debugSearchBoxExtents.y, 0.0f, 1000.0f, 0.05f);
		pBank->AddSlider("Bounding Box Extents Z",&ms_debugSearchBoxExtents.z, 0.0f, 1000.0f, 0.05f);

		pBank->AddToggle("Enable New Soa Searches", &g_toggleNewSoASearches, 1);

		pBank->PushGroup( "Stats filter flags", false );
			pBank->AddToggle( "AUDIO",		&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_AUDIO );
			pBank->AddToggle( "DEFAULT",	&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_DEFAULT );
			pBank->AddToggle( "PEDS",		&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_PEDS );
			pBank->AddToggle( "PORTALS",	&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_PORTALS );
			pBank->AddToggle( "SCRIPT",		&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_SCRIPT );
			pBank->AddToggle( "STREAMING",	&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_STREAMING );
			pBank->AddToggle( "VEHICLEAI",	&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_VEHICLEAI );
			pBank->AddToggle( "DEBUG",		&ms_filterSearchMask,	WORLDREP_SEARCHMODULE_DEBUG );
		pBank->PopGroup();
	pBank->PopGroup();
}
#endif // __BANK

//////////////////
// Main methods //
//////////////////

void fwSearch::ForAllEntitiesIntersecting(fwSceneGraphNode* rootSceneNode, fwSearchVolume* collisionVolume, fwIntersectingCB callback, void* data, u32 typeFlags, u32 locationFlags, u32 lodFlags, u32 optionFlags, u32 module)
{
	PF_INCREMENT( ForAllEntitiesIntersectingCount );
	PF_FUNC( ForAllEntitiesIntersectingTotal );

	{
		PF_FUNC( ForAllEntitiesIntersectingSearch );
		// 8/15/12 - cthomas - This default search is a synchronous wrapper for kicking off an asynchronous search 
		// and waiting for it to finish. From the game-side code, this creates an API with the appearance of single-threaded, 
		// synchronous behavior. But because it uses other threads to do the work, and the default priority in the rage 
		// dependency system is "medium", when there are priority conflicts with other rage dependency jobs we end up 
		// getting very bad performance - effectively waiting for those jobs to finish first, then our search job will 
		// run. Changing this to "critical" priority since the calling thread (which should always be the main thread) 
		// will otherwise be blocked if this search job doesn't finish ASAP.
		ms_defaultSearch.Start( rootSceneNode, collisionVolume, typeFlags, locationFlags, lodFlags, optionFlags, module, sysDependency::kPriorityCritical );
		ms_defaultSearch.Finalize();
	}

	{
		PF_FUNC( ForAllEntitiesIntersectingCallbacks );
		ms_defaultSearch.ExecuteCallbackOnResult( callback, data );
	}
}

fwSearch::fwSearch(const u32 searchResultCapacity)
	: m_searchResult( 0, searchResultCapacity )
#if __BANK
	,m_searchStarted(false)
#endif
{
}

fwSearch::~fwSearch()
{ }

void fwSearch::PrepareConstants(fwSearchVolume* collisionVolume, const u32 typeFlags, const u32 locationFlags, const u32 lodFlags, const u32 optionFlags)
{
	memcpy( &(m_searchConstants.m_collisionVolume), collisionVolume, sizeof(fwSearchVolume) );

	m_searchConstants.m_typeFlags = typeFlags;
	m_searchConstants.m_locationFlags = locationFlags;
	m_searchConstants.m_lodFlags = lodFlags;
	m_searchConstants.m_optionFlags = optionFlags;

	m_searchConstants.m_entriesCountPtr = m_searchResult.GetCountPointer();
	m_searchConstants.m_entriesCapacity = m_searchResult.GetCapacity(); 
	m_searchConstants.m_entriesArray = m_searchResult.GetElements();
}

void fwSearch::PerformSearch(fwSceneGraphNode* rootSceneNode, sysDependency::ePriority priority)
{
	const u32		flags =
		sysDepFlag::INPUT0	|
		sysDepFlag::INPUT2;

	sysDependency&	dependency = m_dependency;

	dependency.Init( SearchNodes_Dependency, 0, flags );
	dependency.m_Priority = u8(priority);
	dependency.m_Params[0].m_AsPtr = fwSceneGraph::GetPoolStorage();
	dependency.m_Params[1].m_AsPtr = fwSceneGraph::GetPoolStorage();
	dependency.m_Params[2].m_AsPtr = &m_searchConstants;
	dependency.m_Params[3].m_AsPtr = &m_searchConstants;
	dependency.m_Params[4].m_AsPtr = rootSceneNode;
	dependency.m_Params[5].m_AsPtr = &m_searchPendingJobCount;
#if __STATS
	dependency.m_Params[6].m_AsPtr = &m_spuSearchNodesTime;
#endif
	dependency.m_DataSizes[0] = fwSceneGraph::GetPoolStorageSize();
	dependency.m_DataSizes[2] = sizeof(fwSearchConstants);

	sysInterlockedIncrement( &m_searchPendingJobCount );
	sysDependencyScheduler::Insert( &dependency );
}

#if __VALIDATE_SEARCH_VOLUMES
inline void ValidateVector3(const Vector3& ASSERT_ONLY(vec))
{
	Assertf(FPIsFinite(vec.x), "x component %f is invalid",vec.x);
	Assertf(FPIsFinite(vec.y), "y component %f is invalid",vec.y);
	Assertf(FPIsFinite(vec.z), "z component %f is invalid",vec.z);
}

inline void ValidateVector4(const Vector4& ASSERT_ONLY(vec))
{
	Assertf(FPIsFinite(vec.x), "x component %f is invalid",vec.x);
	Assertf(FPIsFinite(vec.y), "y component %f is invalid",vec.y);
	Assertf(FPIsFinite(vec.z), "z component %f is invalid",vec.z);
	Assertf(FPIsFinite(vec.w), "w component %f is invalid",vec.w);
}
#endif // __VALIDATE_SEARCH_VOLUMES

void fwSearch::Start(fwSceneGraphNode* rootSceneNode, fwSearchVolume* collisionVolume, u32 typeFlags, u32 locationFlags, u32 lodFlags, u32 optionFlags, u32 module, sysDependency::ePriority priority)
{
#if __BANK
	Assertf(!m_searchStarted, "Search already started");
	m_searchStarted = true;
#endif

	m_searchModule = module;

	PF_INCREMENT( SearchCount );
	PF_FUNC( StartTotal );

#if __VALIDATE_SEARCH_VOLUMES  
	Assertf(collisionVolume->m_type >= SEARCH_VOLUME_IS_SPHERE_INSIDE && 
			collisionVolume->m_type <= SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB,
			"collision volume type is invalid %d", collisionVolume->m_type);

	if (collisionVolume->m_type >= SEARCH_VOLUME_IS_SPHERE_INSIDE && 
		collisionVolume->m_type <= SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX)
	{
		Vector4 sphere = VEC4V_TO_VECTOR4(collisionVolume->m_sphere.GetV4());
		ValidateVector4(sphere);
	}
	else if (collisionVolume->m_type == SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX || 
			collisionVolume->m_type == SEARCH_VOLUME_IS_BOX_INTERSECTING_BB)
	{
		ValidateVector3(VEC3V_TO_VECTOR3(collisionVolume->m_boundingBox.GetMin()));
		ValidateVector3(VEC3V_TO_VECTOR3(collisionVolume->m_boundingBox.GetMax()));
	}
	else if (collisionVolume->m_type == SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB)
	{		
		ValidateVector4(collisionVolume->m_vec1);
		ValidateVector4(collisionVolume->m_vec2);
	}
#endif // __VALIDATE_SEARCH_VOLUMES

	if ( ms_filterSearchMask & m_searchModule )
	{
		PF_INCREMENT( FilteredSearchCount );
		PF_START( FilteredStartTotal );
	}

	m_searchResult.ResetCount();
	fwSearch::PrepareConstants( collisionVolume, typeFlags, locationFlags, lodFlags, optionFlags );

	fwSceneGraph::LockSceneGraph();

	m_searchPendingJobCount = 0;

	fwSearch::PerformSearch( rootSceneNode, priority );

	if ( ms_filterSearchMask & m_searchModule )
	{
		PF_STOP( FilteredStartTotal );
	}
}

void fwSearch::Finalize()
{
#if __STATS
	PF_FUNC( FinalizeTotal );
	if ( ms_filterSearchMask & m_searchModule )
	{
		PF_START( FilteredFinalizeTotal );
	}
#endif

#if __BANK
	if (!Verifyf(m_searchStarted, "Search not started"))
	{
		return;
	}
#endif
	ASSERT_ONLY(u32 uStart = sysTimer::GetSystemMsTime();)

	while ( sysInterlockedRead( &m_searchPendingJobCount ) > 0 )
	{
		Assertf(sysTimer::GetSystemMsTime() - uStart < 20000, "Stuck yielding forever Pending Job Count %d", m_searchPendingJobCount);
		sysIpcYield( PRIO_NORMAL );
	}

	fwSceneGraph::UnlockSceneGraph();

#if __STATS
	const float		searchNodesMs = static_cast< float >( m_spuSearchNodesTime ) / 1000;

	m_spuSearchNodesTime = 0;

	PF_INCREMENTBY( SpuNodeProcessTime, searchNodesMs );

	if ( ms_filterSearchMask & m_searchModule )
	{
		PF_STOP( FilteredFinalizeTotal );
		PF_INCREMENTBY( FilteredSpuNodeProcessTime, searchNodesMs );
	}
#endif

#if __BANK
	m_searchStarted = false;
#endif

}

bool fwSearch::IsFinished()
{
	Assertf( false, "This method is not implemented" );
	return true;
}

void fwSearch::ExecuteCallbackOnResult(fwIntersectingCB callback, void* data)
{
	fwScanTimerFunc		scanTimer( PPU_SYMBOL(g_searchCallbacksTimer) );

	fwSearchResult::iterator endIt = m_searchResult.end();
	for (fwSearchResult::iterator it = m_searchResult.begin(); it != endIt;)
	{
		fwSearchResult::iterator next = it;
		next++;

		if (next != endIt)
		{
			PrefetchDC(*next);
		}

		if ( *it && callback( *it, data ) == false )
			return;

		it = next;
	}
}
#if __BANK
void fwSearch::UpdateDebug()
{
	if (ms_enableSearchDebugTest)
	{
		// create search volume
		spdSphere searchSphere(RCC_VEC3V(ms_debugSearhSpherePos), ScalarV(ms_debugSearchRadius));
		Vector3 bMin = ms_debugSearhSpherePos - ms_debugSearchBoxExtents;
		Vector3 bMax = ms_debugSearhSpherePos + ms_debugSearchBoxExtents;
		spdAABB searchAABB(RCC_VEC3V(bMin), RCC_VEC3V(bMax));

		// Spheres
		fwIsSphereInside isSphereInside(searchSphere);
		fwIsSphereIntersecting isSphereIntersecting(searchSphere);
		fwIsSphereIntersectingVisible isSphereIntersectingVisible(searchSphere);
		fwIsSphereIntersectingBBApprox isSphereIntersectingBBApprox(searchSphere);

		// Boxes
 		fwIsBoxInside isBoxInside(searchAABB);
 		fwIsBoxIntersectingApprox isBoxIntersectingApprox(searchAABB);
 		fwIsBoxIntersectingBB isBoxIntersectingBB(searchAABB);

		// Line
 		fwIsLineSegIntersectingBB isLineSegIntersectingBB(bMin, bMax);

		fwSearchVolume *pVolume;
		bool drawBoundingSphereForEntities = false;
		switch (ms_debugSearchSearchVolumeType) 
		{
			// Spheres
			case SEARCH_VOLUME_IS_SPHERE_INSIDE:
			{
				pVolume = &isSphereInside;
				drawBoundingSphereForEntities = true;
				break;
			}
			case SEARCH_VOLUME_IS_SPHERE_INTERSECTING:
			{
				pVolume = &isSphereIntersecting;
				drawBoundingSphereForEntities = true;
				break;
			}
			case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_VISIBLE:
			{
				pVolume = &isSphereIntersectingVisible;
				drawBoundingSphereForEntities = true;
				break;
			}
			case SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX:
			{
				pVolume = &isSphereIntersectingBBApprox;
				break;
			}
			// Boxes
			case SEARCH_VOLUME_IS_BOX_INSIDE:
			{
				pVolume = &isBoxInside;
				break;
			}
			case SEARCH_VOLUME_IS_BOX_INTERSECTING_APPROX:
			{
				pVolume = &isBoxIntersectingApprox;
				break;
			}
			case SEARCH_VOLUME_IS_BOX_INTERSECTING_BB:
			{
				pVolume = &isBoxIntersectingBB;
				break;
			}
			//Lines
			case SEARCH_VOLUME_IS_LINE_SEG_INTERSECTING_BB:
			{
				pVolume = &isLineSegIntersectingBB;
				break;
			}

			default:
			{
				pVolume = NULL;
			}
		}

		int searchCount = 0;
		if (pVolume)
		{
			// start spu search
			ms_defaultSearch.Start( fwWorldRepMulti::GetSceneGraphRoot(),					// root scene graph node
									pVolume,												// search volume
									2 | 4 | 8 | 16 | 32 | 64,								// entity types
									SEARCH_LOCATION_EXTERIORS | SEARCH_LOCATION_INTERIORS,	// location flags
									SEARCH_LODTYPE_ALL,										// lod flags
									SEARCH_OPTION_NONE,										// option flags
									WORLDREP_SEARCHMODULE_DEFAULT,							// module
									sysDependency::kPriorityCritical						// priority
								  );

			ms_defaultSearch.Finalize();

			fwSearchResult::iterator endIt = ms_defaultSearch.m_searchResult.end();
			for (fwSearchResult::iterator it = ms_defaultSearch.m_searchResult.begin(); it != endIt; ++it)
			{
				fwEntity *pEntity = (*it);
				if (pEntity)
				{
					++searchCount;
					spdAABB aabb;
					pEntity->GetAABB(aabb);
					if (drawBoundingSphereForEntities)
					{
						grcDebugDraw::Sphere(aabb.GetCenter(), VEC3V_TO_VECTOR3(aabb.GetMin() - aabb.GetCenter()).Mag(), Color32(0.0f, 1.0f, 0.0f), false);
					}
					else
					{
						grcDebugDraw::BoxAxisAligned(aabb.GetMin(), aabb.GetMax(), Color32(0.0f, 1.0f, 0.0f), false);
					}
				}
			}
		}

		if (ms_debugSearchSearchVolumeType >= SEARCH_VOLUME_IS_SPHERE_INSIDE && 
			ms_debugSearchSearchVolumeType <= SEARCH_VOLUME_IS_SPHERE_INTERSECTING_BB_APPROX)
		{
			grcDebugDraw::Sphere(ms_debugSearhSpherePos, 
								 ms_debugSearchRadius, 
								 Color32(1.0f, float(g_toggleNewSoASearches), 0.0f, 1.0f), false);
		}
		else if (ms_debugSearchSearchVolumeType >= SEARCH_VOLUME_IS_BOX_INSIDE && 
				 ms_debugSearchSearchVolumeType <= SEARCH_VOLUME_IS_BOX_INTERSECTING_BB)
		{
			grcDebugDraw::BoxAxisAligned(searchAABB.GetMin(), searchAABB.GetMax(), Color32(1.0f, float(g_toggleNewSoASearches), 0.0f), false);
		}
		else
		{
			grcDebugDraw::Line(searchAABB.GetMin(), searchAABB.GetMax(), Color32(1.0f, float(g_toggleNewSoASearches), 0.0f), false);
		}
		const float _XPos = 270.0f;
		const float _YPos = 580.0f;
		const float _YInc = 10.0f;

		Vector2 startPosition(_XPos, _YPos);

		grcDebugDraw::TextFontPush(grcSetup::GetMiniFixedWidthFont());

		const atVarString searchTypeText("Search Type %s", sShowSearchTypes[ms_debugSearchSearchVolumeType]);
		grcDebugDraw::Text(startPosition, DD_ePCS_Pixels, Color32(0xFFFFFFFF), searchTypeText, true, 1.0f, 1.0f);
		startPosition.y += _YInc;


		const atVarString searchCountText("Search Count %d", searchCount);
		grcDebugDraw::Text(startPosition, DD_ePCS_Pixels, Color32(0xFFFFFFFF), searchCountText, true, 1.0f, 1.0f);

		grcDebugDraw::TextFontPop();

	}
}
#endif
} // namespace rage


#endif // !__TOOL


