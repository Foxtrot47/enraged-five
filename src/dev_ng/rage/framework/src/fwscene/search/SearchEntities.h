#ifndef _INC_SEARCHENTITIES_H_
#define _INC_SEARCHENTITIES_H_

#include "atl/array.h"
#include "system/eaptr.h"
#include "fwscene/world/EntityDesc.h"

namespace rage {

class fwEntity;
struct fwSearchConstants;

//
// SEARCH_LOCATION_FLAGS
//
// Control whether ForAllEntitiesIntersecting searches exterior map
// or interiors
//

enum SEARCH_LOCATION_FLAGS
{
	SEARCH_LOCATION_EXTERIORS			= 1 << 0,
	SEARCH_LOCATION_INTERIORS			= 1 << 1
};

//
// SEARCH_LODTYPE
//
// Control what level of detail to search for using ForAllEntitiesIntersecting
//

enum SEARCH_LODTYPE
{
	SEARCH_LODTYPE_HIGHDETAIL		= 1 << 0,
	SEARCH_LODTYPE_LOWDETAIL		= 1 << 1,
	SEARCH_LODTYPE_ALL				= SEARCH_LODTYPE_HIGHDETAIL | SEARCH_LODTYPE_LOWDETAIL
};

//
// SEARCH_OPTION_FLAGS
//
// Further option for searches, mainly to try to cull as much as possible of the scene graph
//

enum SEARCH_OPTION_FLAGS
{
	SEARCH_OPTION_NONE								= 0,
	SEARCH_OPTION_SPAWN_POINTS_ONLY					= 1 << 0,
	SEARCH_OPTION_DYNAMICS_ONLY						= 1 << 1,
	SEARCH_OPTION_FORCE_PPU_CODEPATH				= 1 << 2,
	SEARCH_OPTION_SKIP_PROCEDURAL_ENTITIES			= 1 << 3,
	BANK_ONLY( SEARCH_OPTION_LARGE					= 1 << 4 )
};

//
// Main entities search class
//

enum {
	SEARCH_MAX_ENTITIES_PER_JOB		= 320
};

class fwSearchEntities
{
private:

	typedef atFixedArray< sysEaPtr<fwEntity>, SEARCH_MAX_ENTITIES_PER_JOB > SearchEntryArray;

	fwSearchConstants*		m_searchConstants;
	fwEntityDesc*			m_entityDescs;
	u32						m_entityDescsCount;
	SearchEntryArray*		m_searchEntries;
	fwFlags16				m_flags;
	
	void ResetSearchEntries();
	void AddSearchEntry(const sysEaPtr<fwEntity> entity);
	void SubmitSearchEntries();

public:

	enum {
		SCRATCH_BUFFER_SIZE		= 18 * 1024
	};

	enum {
		FLAG_ENTITY_DESC_SOA	= 1 << 0
	};

	void SetSearchConstants(fwSearchConstants* const searchConstants)	{ m_searchConstants = searchConstants; }
	void SetEntityDescs(fwEntityDesc* const entityDescs)				{ m_entityDescs = entityDescs; }
	void SetEntityDescsCount(const u32 entityDescsCount)				{ m_entityDescsCount = entityDescsCount; }
	void SetFlags(const fwFlags16 flags)								{ m_flags = flags; }

	void AssignScratchBuffer(u8* memory, const int size);

	void Process();
};

} // namespace rage

#endif // !defined _INC_SEARCHENTITIES_H_
