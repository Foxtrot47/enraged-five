#ifndef _INC_SEARCHNODES_H_
#define _INC_SEARCHNODES_H_

#include "fwscene/search/SearchEntities.h"
#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/world/EntityContainer.h"
#include "system/dependency.h"
#include "system/dependencyscheduler.h"

namespace rage {

/*
PURPOSE:

This class encapsulates the container visit algorithm for world searches.
*/

class fwSearchNodes
{
private:
	fwSearchConstants*					m_searchConstants;
	sysEaPtr< fwSearchConstants >		m_searchConstantsEa;
	fwSceneGraphNode*					m_rootSceneNode;
		
	void ProcessEntities(fwBaseEntityContainer* entityContainer);
		
public:
	void SetSearchConstants(fwSearchConstants* searchConstants)		{ m_searchConstants = searchConstants; }
	void SetSearchConstantsEa(sysEaPtr< fwSearchConstants > ea)		{ m_searchConstantsEa = ea; }
	void SetRootSceneNode(fwSceneGraphNode* rootSceneNode)			{ m_rootSceneNode = rootSceneNode; }
		
	void Process();
};

} // namespace rage

#endif // !defined _INC_SEARCHNODES_H_
