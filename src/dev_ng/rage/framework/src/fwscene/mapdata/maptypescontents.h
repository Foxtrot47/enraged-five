//
// fwscene/stores/maptypescontents.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//


#ifndef __FWSCENE_MAPTYPES_CONTENTS_H__
#define __FWSCENE_MAPTYPES_CONTENTS_H__

#include "atl/hashstring.h"
#include "atl/array.h"

#include "fwscene/stores/psostore.h"

namespace rage
{
	class fwArchetype;
	class fwStreamedSceneGraphNode;
	class fwMapTypes;

	// PURPOSE:	The contents added to the map created from the fwMapTypes descriptions. 
	class fwMapTypesContents
	{
	public:
		fwMapTypesContents();
		virtual ~fwMapTypesContents();

		atHashString					m_name;									// TODO: make this go away

		fwPsoStoreLoadInstance& GetLoaderInstRef() { return m_loaderInst; }

	private:
		fwPsoStoreLoadInstance			m_loaderInst;
	};

}	//namespace rage

#endif // __FWSCENE_MAPTYPES_CONTENTS_H__