/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/mapdata/mapdatadebug
// PURPOSE : some bits of debug functionality for mapdatastore, best separated out
// AUTHOR :  Ian Kiigan
// CREATED : 03/06/2011
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _FWSCENE_MAPDATA_MAPDATADEBUG_H_
#define _FWSCENE_MAPDATA_MAPDATADEBUG_H_

#if __BANK

#include "atl/array.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "fwscene/stores/boxstreamerassets.h"

namespace rage
{
	class fwMapDataDebug
	{
	public:
		static void	InitWidgets(bkBank& bank);
		static void	Update();
		static s32 GetBlockIndex(s32 mapDataSlot);
		static void PrintLoadedList();
		static void PrintActiveGroups();
		static void GetActiveGroups();
		static void SetActiveGroups();
		static void SuppressSelected(atArray<s32>& mapDataSlots);
		static void UpdateDebugStats();
		static bool SuppressHdWhenFlyingFast() { return ms_bNoHdWhenFlyingFast; }
		static bool *GetSuppressHdWhenFlyingFastPtr() { return &ms_bNoHdWhenFlyingFast; }

	private:
		static void AnalyseBatchData();
		static void AnalyseCachedImaps();
		static void ToggleStreamableCB();
		static void LoadCB();
		static void UnloadCB();
		static void UpdateDebugForSelectedImapCB();
		static s32	CompareNameCB(const s32* pIndex1, const s32* pIndex2);
		static void ValidateBounds();
		static bool	IsImapGroupSlot(s32 index);
		static void RequestImapGroupCB();
		static void RemoveImapGroupCB();
		static void RemoveAndRequestImapGroupCB();
		static void OnChangeImapGroup();
		static void SwapImapGroupCB();
		static void SwapBackImapGroupCB();
		static void ManualEndImapGroupTransitionCB();
		static void ManualAbandonImapGroupTransitionCB();
		static void ManualRemoveStartImapGroupTransitionCB();

		static atArray<const char*> ms_apszDebugImapNames;
		static atArray<const char*> ms_apszImapGroupNames;
		static atArray<s32> ms_slotIndices;
		static atArray<s32> m_imapGroupSlotIndices;
		static atArray<s32> ms_suppressedImaps;

		static bool		ms_bShowEntityCounts;
		static bool		ms_bShowMatrixUse;
		static bool		ms_bShowDependencies;
		static bool		ms_bStreamable;
		static s32		ms_selectedImapComboIndex;
		static s32		ms_selectedImapGroupComboIndex;
		static s32		ms_imapGroupTransitionStart;
		static s32		ms_imapGroupTransitionEnd;
		static bool		ms_bAutomaticTransition;
		static bool		ms_bShowActiveTransitions;
		static s32		ms_transitionIndex;
		static s32		ms_debugImapIndex;
		static bool		ms_bValidateStrBounds;
		static bool		ms_bShowBadBoundsOnly;
		static bool		ms_bValidatePhyBounds;
		static bool		ms_bUsePivotsForValidation;
		static bool		ms_bSuppressedSelected;
		static bkText*	ms_pExtentsText;
		static bool		ms_bNoHdWhenFlyingFast;

	public:
		static float	ms_fInflateRadius_Low;
		static float	ms_fInflateRadius_Medium;
		static float	ms_fInflateRadius_High;

	
		static bool		ms_bShowLoadedStats;
		static u32		ms_dbgNumLoaded[fwBoxStreamerAsset::ASSET_TOTAL];
		static u32		ms_dbgNumRequired[fwBoxStreamerAsset::ASSET_TOTAL];
	};

}	//namespace rage

#endif	//__BANK

#endif	//_FWSCENE_MAPDATA_MAPDATADEBUG_H_
