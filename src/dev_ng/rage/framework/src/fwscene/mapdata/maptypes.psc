<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
 generate="psoChecks">

  <!-- Extension definition -->
  <structdef type="::rage::fwExtensionDef"  >
    <string 	name="m_name" type="atHashString" />
  </structdef>

	<enumdef type="::rage::fwArchetypeDef::eAssetType">
		<enumval name="ASSET_TYPE_UNINITIALIZED"        value="0" />
		<enumval name="ASSET_TYPE_FRAGMENT"				value="1" />
		<enumval name="ASSET_TYPE_DRAWABLE"				value="2" />
		<enumval name="ASSET_TYPE_DRAWABLEDICTIONARY"	value="3" />
		<enumval name="ASSET_TYPE_ASSETLESS"			value="4" />
	</enumdef>
	
	<!-- Archetype definition -->
	<structdef type="::rage::fwArchetypeDef"  >
    <float 		name="m_lodDist"/>
    <u32 		name="m_flags"/>
		<u32 		name="m_specialAttribute"/>
		<Vector3 	name="m_bbMin"/>
		<Vector3 	name="m_bbMax"/>
		<Vector3 	name="m_bsCentre"/>
		<float 		name="m_bsRadius"/>
    <float 		name="m_hdTextureDist" init="5.0"/>
    <string 	name="m_name" type="atHashString" ui_key="true"/>
    <string 	name="m_textureDictionary" type="atHashString" />
    <string 	name="m_clipDictionary" type="atHashString" />
    <string 	name="m_drawableDictionary" type="atHashString" />
    <string   name="m_physicsDictionary" type="atHashString" />

	<enum		name="m_assetType" type="::rage::fwArchetypeDef::eAssetType"/>
	<string 	name="m_assetName" type="atHashString" />
		
    <array  name="m_extensions" type="atArray">
      <pointer type="::rage::fwExtensionDef" policy="owner"/>
    </array>
	</structdef>
		
	<!-- Map data - defined a set of archetypes and entities for a particular map section. -->
	<structdef type="::rage::fwMapTypes"  >
    <!-- This array should be regarded as deprecated - remove at the earliest opportunity & use extension list _inside_ archetype instead -->
    <array		name="m_extensions" type="atArray">
      <pointer type="::rage::fwExtensionDef" policy="owner"/>
    </array>

    <array		name="m_archetypes" type="atArray">
      <pointer type="::rage::fwArchetypeDef" policy="owner"/>
    </array>

    <string   name="m_name" type="atHashString" />
		
		<array		name="m_dependencies" type="atArray">
			<string type="atHashString" />
		</array>

	</structdef>

</ParserSchema>
