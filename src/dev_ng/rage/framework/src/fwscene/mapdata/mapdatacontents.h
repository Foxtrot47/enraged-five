/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/mapdata/mapdatacontents.h
// PURPOSE : contains items created by a loaded mapdata file, including entity instances etc
// AUTHOR :  Ian Kiigan
// CREATED : 27/04/2011
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _FWSCENE_MAPDATA_MAPDATACONTENTS_H_
#define _FWSCENE_MAPDATA_MAPDATACONTENTS_H_

#include "atl/hashstring.h"
#include "spatialdata/aabb.h"
#include "fwtl/pool.h"

#include "fwscene/stores/psostore.h"

namespace rage
{
	class fwEntity;
	class fwMapData;
	class fwMapDataDef;
	class fwStreamedSceneGraphNode;

	// PURPOSE: Used for tracking loaded map data files, for sorting and removal
	class fwMapDataLoadedNode
	{
	public:

		FW_REGISTER_CLASS_POOL(fwMapDataLoadedNode);

		enum
		{
			INVALID_SLOT = 0xffff
		};

		fwMapDataLoadedNode() : m_slotIndex(INVALID_SLOT), m_depth(0), m_bDeleteMe(false) {}
		fwMapDataLoadedNode(u32 slotIndex, u32 depth) : m_slotIndex(slotIndex), m_depth(depth), m_bDeleteMe(false) {}

		u32 GetSlotIndex() const { return m_slotIndex; }
		u32 GetDepth() const { return m_depth; }
		bool IsMarkedDelete() const { return m_bDeleteMe; }
		void MarkDelete() { m_bDeleteMe = true; }
	private:
		u32 m_slotIndex		: 16;
		u32 m_depth			: 2;
		u32 m_bDeleteMe		: 1;
		ATTR_UNUSED u32 m_tbd			: 13;
	};

	// PURPOSE:	The contents added to the map created from the fwMapData descriptions. 
	class fwMapDataContents
	{
	public:
		fwMapDataContents();
		virtual ~fwMapDataContents();

		virtual void		Load(fwMapDataDef* pDef, fwMapData* pMapData, strLocalIndex mapDataSlotIndex) = 0;
		virtual void		Remove(strLocalIndex mapDataSlotIndex) = 0;

		inline fwEntity**	GetEntities() const									{ return m_entities; }
		inline u32			GetNumEntities() const								{ return m_numEntities; }
		inline fwStreamedSceneGraphNode* GetSceneGraphNode() const				{ return m_sceneNode; }

		inline void			SetLoadedNode(fwMapDataLoadedNode* pNode)			{ m_pLoadedNode = pNode; }
		fwMapDataLoadedNode* GetLoadedNode() const								{ return m_pLoadedNode; }

		virtual void		ConstructInteriorProxies(fwMapDataDef* pDef, fwMapData* pMapData, strLocalIndex mapDataSlotIndex) = 0;

		fwPsoStoreLoadInstance& GetLoaderInstRef() { return m_loaderInst; }

#if __BANK
		bool				GetGuid(fwEntity* pEntity, u32& guid);
		virtual void		LoadDebug(fwMapDataDef* pDef, fwMapData* pMapData) = 0;
#if RSG_PC
		static inline bool* GetAllowOrphanScalingPtr() { return &ms_bAllowOrphanScaling; }
#endif // RSG_PC
#endif	//__BANK

#if RSG_PC
		const fwMapData*	GetMapData_public() const { return m_pMapData; }
		static inline bool	GetAllowOrphanScaling() { return ms_bAllowOrphanScaling; }
		static inline void	SetAllowOrphanScaling(bool b) { ms_bAllowOrphanScaling = b; }
		static inline bool	GetEnableInstancedGrass() { return ms_bEnbleInstancedGrass; }
		static inline void	SetEnableInstancedGrass(bool b) { ms_bEnbleInstancedGrass = b; }
#else // RSG_PC
		static inline bool	GetAllowOrphanScaling() { return true; }
#endif // RSG_PC


	protected:
		void				Entities_Create(fwMapData* pMapData, fwMapDataContents* pParentContents, s32 mapDataSlotIndex);
		void				Entities_AddToWorld(fwMapData* pMapData, bool bPostponed, bool bCulled);
		void				Entities_RemoveFromWorld();
		void				Entities_Destroy();

		inline void			SetMapData(fwMapData* pMapData)	{ m_pMapData=pMapData; }
		inline fwMapData*	GetMapData() const									{ return m_pMapData; }

	private:
		void				CreateEntityArray(u32 numEntities);
		inline void			SetEntityAtIndex(fwEntity* pEntity, u32 index)		{ m_entities[index] = pEntity; }
		inline void			SetEntities(fwEntity** entities)					{ m_entities = entities; }
		inline void			SetNumEntities(u32 numEntities)						{ m_numEntities = numEntities; }
		
		fwStreamedSceneGraphNode*		m_sceneNode;
		fwEntity**						m_entities;
		u32								m_numEntities;
		fwMapDataLoadedNode*			m_pLoadedNode;
		fwMapData*						m_pMapData;
		fwPsoStoreLoadInstance			m_loaderInst;

#if RSG_PC
		static bool ms_bAllowOrphanScaling;
		static bool ms_bEnbleInstancedGrass;
#endif // RSG_PC
#if __BANK
		u32* m_guidArray;
#endif	//__BANK
	};

}	//namespace rage

#endif	//_FWSCENE_MAPDATA_MAPDATACONTENTS_H_
