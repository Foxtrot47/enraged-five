<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
 generate="psoChecks">


	<!-- Resource Ready (Optimized) Instance List Definitions -->
	<!-- Note: m_InstMat is a packed transform matrix in the format that's passed into the shader. (See format below) -->
	<!--		m_InstMat[0] = {mat34.a.x, mat34.a.y, mat34.a.z, mat34.d.x}			-->
	<!--		m_InstMat[0] = {mat34.b.x, mat34.b.y, mat34.b.z, mat34.d.y}			-->
	<!--		m_InstMat[0] = {mat34.c.x, mat34.c.y, mat34.c.z, mat34.d.z}			-->
	<structdef type="::rage::fwPropInstanceListDef::InstanceData" simple="true">
		<array name="m_InstMat" type="member" size="3">
			<Vec4V/>
		</array>
		<Color32 name="m_Tint" />
		<u32 name="m_Index" init="-1"/>
	</structdef>

	<structdef type="::rage::fwPropInstanceListDef" simple="true">
		<array name="m_InstanceList" type="atArray">
			<struct type="::rage::fwPropInstanceListDef::InstanceData"/>
		</array>
		<struct name="m_BatchAABB" type="::rage::spdAABB"/>
		<string name="m_archetypeName" type="atHashString" ui_key="true"/>		<!-- contains access to the model's bounding box. -->
		<u32 name="m_lodDist" init="0x1fff"/>
	</structdef>

	<structdef type="::rage::fwGrassInstanceListDef::InstanceData" simple="true">
		<array name="m_Position" type="member" size="3">
			<u16/>
		</array>
		<u8 name="m_NormalX"/>
		<u8 name="m_NormalY"/>
		<array name="m_Color" type="member" size="3">
			<u8/>
		</array>
		<u8 name="m_Scale"/>
		<u8 name="m_Ao" init="255"/>
		<array name="m_Pad" type="member" size="3">
			<u8/>
		</array>
	</structdef>

	<structdef type="::rage::fwGrassInstanceListDef" simple="true">
		<struct name="m_BatchAABB" type="::rage::spdAABB"/>
		<Vec3V name="m_ScaleRange" noInit="true"/>
		<string name="m_archetypeName" type="atHashString" ui_key="true"/>
		<u32 name="m_lodDist" init="0x1fff"/>
		<float name="m_LodFadeStartDist" init="8191.0f"/>
		<float name="m_LodInstFadeRange" init="1.0f" min="0.0f" max="1.0f"/>
		<float name="m_OrientToTerrain" init="0.0f" min="0.0f" max="1.0f"/>
		<array name="m_InstanceList" type="atArray" align="16" heap="physical">
			<struct type="::rage::fwGrassInstanceListDef::InstanceData"/>
		</array>
		<pad bytes="8"/> <!-- grcVertexBuffer *m_Verts -->
	</structdef>


	<!-- Instanced Map Data Object -->
	<structdef type="::rage::fwInstancedMapData">
		<string name="m_ImapLink" type="atHashString" ui_key="true"/>
		<array name="m_PropInstanceList" type="atArray">
			<struct type="::rage::fwPropInstanceListDef"/>
		</array>
		<array name="m_GrassInstanceList" type="atArray">
			<struct type="::rage::fwGrassInstanceListDef"/>
		</array>
	</structdef>

</ParserSchema>
