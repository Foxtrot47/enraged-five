//
// fwscene/mapdata/mapdata.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef __SCENE_MAPDATA_H__
#define __SCENE_MAPDATA_H__

#include "parser/macros.h"
#include "data/base.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/map.h"
#include "atl/string.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "softrasterizer/boxoccluder.h"
#include "softrasterizer/occludemodel.h"
#include "spatialdata/aabb.h"
#include "system/bit.h"
#include "entity/extensionlist.h"
#include "fwscene/lod/LodTypes.h"
#include "fwscene/mapdata/maptypes.h"
#include "fwscene/mapdata/mapinstancedata.h"


namespace rage 
{
	class fwEntity;
	class fwMapDataContents;
	class fwMapDataDef;

	// PURPOSE:	Base class for all instance definitions.
	class fwEntityDef : public datBase
	{
	public:
		enum eLoadFlags
		{
			// framework
			FLAG_FULLMATRIX								= BIT(0),
			FLAG_STREAM_LOWPRIORITY						= BIT(1),
			FLAG_DONT_INSTANCE_COLLISION				= BIT(2),
			FLAG_LOD_IS_IN_PARENT_MAPDATA				= BIT(3),
			FLAG_LOD_ADOPTME							= BIT(4),
			FLAG_IS_FIXED								= BIT(5),
			FLAG_IS_INTERIOR_LOD						= BIT(6),

			// game
			FLAG_DRAWABLELODUSEALTFADE					= BIT(15),
			FLAG_UNUSED									= BIT(16),
			FLAG_DOESNOTTOUCHWATER						= BIT(17),
			FLAG_DOESNOTSPAWNPEDS						= BIT(18),
			FLAG_LIGHTS_CAST_STATIC_SHADOWS				= BIT(19),
			FLAG_LIGHTS_CAST_DYNAMIC_SHADOWS			= BIT(20),
			FLAG_LIGHTS_IGNORE_DAY_NIGHT_SETTINGS		= BIT(21),
			FLAG_DONT_RENDER_IN_SHADOWS					= BIT(22),
			FLAG_ONLY_RENDER_IN_SHADOWS					= BIT(23),
			FLAG_DONT_RENDER_IN_REFLECTIONS				= BIT(24), // paraboloid and mirror reflections, but not water
			FLAG_ONLY_RENDER_IN_REFLECTIONS				= BIT(25), // paraboloid and mirror reflections, but not water
			FLAG_DONT_RENDER_IN_WATER_REFLECTIONS		= BIT(26),
			FLAG_ONLY_RENDER_IN_WATER_REFLECTIONS		= BIT(27),
			FLAG_DONT_RENDER_IN_MIRROR_REFLECTIONS		= BIT(28),
			FLAG_ONLY_RENDER_IN_MIRROR_REFLECTIONS		= BIT(29),
		};
		enum ePriorityLevel
		{
			PRI_REQUIRED						= 0,		// highest priority
			PRI_OPTIONAL_HIGH					= 1,
			PRI_OPTIONAL_MEDIUM					= 2,
			PRI_OPTIONAL_LOW					= 3,
			PRI_COUNT							= 4,
		};

		fwEntityDef() : m_guid(0), m_childLodDist(0.0f) {}
		virtual ~fwEntityDef();

		atHashString	m_archetypeName;
		u32 			m_flags;
		u32 			m_guid;
		Vector3 		m_position;
		Vector4 		m_rotation;
		float			m_scaleXY;
		float			m_scaleZ;

		s32 			m_parentIndex;
		float 			m_lodDist;
		float			m_childLodDist;
		eLodType		m_lodLevel;
		u32 			m_numChildren;

		ePriorityLevel	m_priorityLevel;
		atArray<fwExtensionDef*> m_extensions;

		PAR_PARSABLE;
	};

	// PURPOSE: Defines a container-LOD parent->container relationship
	class fwContainerLodDef
	{
	public:
		atHashString m_name;
		u32 m_parentIndex;

		PAR_SIMPLE_PARSABLE;
	};

	// PURPOSE:	Contains definitions for map data, both types and instances.
	class fwMapData : public datBase
	{
	public:
		enum eContentTypes
		{
			CONTENTFLAG_ENTITIES_HD				= BIT(0),
			CONTENTFLAG_ENTITIES_LOD			= BIT(1),
			CONTENTFLAG_ENTITIES_CONTAINERLOD	= BIT(2),
			CONTENTFLAG_MLO						= BIT(3),
			CONTENTFLAG_BLOCKINFO				= BIT(4),
			CONTENTFLAG_OCCLUDER				= BIT(5),
			CONTENTFLAG_ENTITIES_USING_PHYSICS	= BIT(6),
			CONTENTFLAG_LOD_LIGHTS				= BIT(7),
			CONTENTFLAG_DISTANT_LOD_LIGHTS	    = BIT(8),
			CONTENTFLAG_ENTITIES_CRITICAL		= BIT(9),
			CONTENTFLAG_INSTANCE_LIST			= BIT(10),
			
			CONTENTFLAG_MASK_RETAINED			= CONTENTFLAG_INSTANCE_LIST | CONTENTFLAG_OCCLUDER | CONTENTFLAG_LOD_LIGHTS | CONTENTFLAG_DISTANT_LOD_LIGHTS,
			CONTENTFLAG_MASK_ENTITIES			= CONTENTFLAG_ENTITIES_HD | CONTENTFLAG_ENTITIES_LOD | CONTENTFLAG_ENTITIES_CONTAINERLOD | CONTENTFLAG_MLO | CONTENTFLAG_ENTITIES_CRITICAL,
			CONTENTFLAG_MASK_LODS				= CONTENTFLAG_ENTITIES_LOD | CONTENTFLAG_ENTITIES_CONTAINERLOD,
			CONTENTFLAG_MASK_LODLIGHTS			= CONTENTFLAG_LOD_LIGHTS | CONTENTFLAG_DISTANT_LOD_LIGHTS
		};
		enum eFlags
		{
			FLAG_MANUAL_STREAM_ONLY		= BIT(0),		// some map data sections are only manually loaded / unloaded by script
			FLAG_IS_PARENT				= BIT(1)
		};

		fwMapData() {}
		virtual ~fwMapData();

		virtual fwMapDataContents* CreateMapDataContents() { return NULL; }

		virtual void DestroyOccluders() {}

		static void AssociateExtensionDef(const u32 hash, s32 factoryId);
		static s32 GetExtensionFactory(const u32 hash);

		static fwEntity* ConstructEntity(fwEntityDef* definition, s32 mapDataSlotIndex, fwArchetype* archetype, fwModelId &modelId);
		static fwEntity* ConstructEntity(const fwPropInstanceListDef* definition, s32 mapDataSlotIndex);
		static fwEntity* ConstructEntity(fwGrassInstanceListDef* definition, s32 mapDataSlotIndex);
		static void SetEntityPriorityCap(fwEntityDef::ePriorityLevel levelCap) { ms_entityLevelCap = levelCap; }
		static fwEntityDef::ePriorityLevel GetEntityPriorityCap() { return ms_entityLevelCap; }

		const atHashString& GetName() const { return m_name; }
		const atHashString& GetParent() const { return m_parent; }
		u32 GetFlags() const { return m_flags; }
		u32 GetContentFlags() const { return m_contentFlags; }
		void GetStreamingExtents(spdAABB& aabb) const { aabb.Set(RCC_VEC3V(m_streamingExtentsMin), RCC_VEC3V(m_streamingExtentsMax)); }
		void GetPhysicalExtents(spdAABB& aabb) const { aabb.Set(RCC_VEC3V(m_entitiesExtentsMin), RCC_VEC3V(m_entitiesExtentsMax)); }

		// these setters can go away once the backcompat path is gone...IK
		void SetName(const char* pszName) { m_name.SetFromString(pszName); }
		void SetParentName(const char* pszName) { m_parent.SetFromString(pszName); }
		void SetFlags(u32 generalFlags) { m_flags = generalFlags; }
		void SetContentFlags(u32 contentFlags) { m_contentFlags = contentFlags; }
		void SetStreamingExtents(Vector3::Param min, Vector3::Param max);
		void SetStreamingExtents(const spdAABB& aabb) { m_streamingExtentsMin = aabb.GetMinVector3(); m_streamingExtentsMax = aabb.GetMaxVector3(); }
		void SetPhysicalExtents(const spdAABB& aabb) { m_entitiesExtentsMin = aabb.GetMinVector3(); m_entitiesExtentsMax = aabb.GetMaxVector3(); }
		void AddEntityDef(fwEntityDef* definition);

		void ReleaseDeviceResources()												{ m_instancedData.ReleaseDeviceResources(); }


	private:
		atHashString				m_name; 
		atHashString				m_parent;

		u32							m_flags;
		u32							m_contentFlags;
		
		Vector3						m_streamingExtentsMin;
		Vector3						m_streamingExtentsMax;
		Vector3						m_entitiesExtentsMin;
		Vector3						m_entitiesExtentsMax;

	public:
		atArray<fwEntityDef*>		m_entities;				// The instances used in the map.
		atArray<fwContainerLodDef>	m_containerLods;		// container-LOD relationships (specified top-down)
		atArray<BoxOccluder>		m_boxOccluders;
		atArray<OccludeModel>		m_occludeModels;
		atArray<atHashString>		m_physicsDictionaries;	// list of all required physics dictionaries
		fwInstancedMapData			m_instancedData;
	
	private:
		static atMap<u32, s32>						ms_entityDefAssociations;
		static fwEntityDef::ePriorityLevel			ms_entityLevelCap;

	protected:
		static atMap<u32, s32>						ms_extensionDefAssociations;

		PAR_PARSABLE;
	};

} // namespace rage

#endif // __SCENE_MAPDATA_H__
