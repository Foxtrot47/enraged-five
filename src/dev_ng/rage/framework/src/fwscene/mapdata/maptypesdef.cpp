//
// fwscene/stores/maptypesdef.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.

#include "fwscene/mapdata/maptypesdef.h"

XPARAM(PartialDynArch);

namespace rage {

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		initialises map data def to known values, and passes through name
//////////////////////////////////////////////////////////////////////////
void fwMapTypesDef::Init(const strStreamingObjectNameString name)
{
	fwAssetNameDef<fwMapTypesContents>::Init(name); 
	SetIsInitialised(false);
	SetIsNativeFile(false);
	SetIsPso(false);
	SetIsInPlace(false);
	SetIsMLOType(false);
	SetIsPermanent(false);
	SetIsDependency(false);
	SetIsPermanentDLC(false);

	m_typeDataIndicesCount = 0;
}

bool	fwMapTypesDef::RequiresTempMemory() const				
{ 
	if (!PARAM_PartialDynArch.Get())
	{
		return (m_flags.m_bIsPermanent  || !m_flags.m_bContainsMLOtype);  
	} else 
	{
		return (m_flags.m_bIsPermanent);  
	}

}

bool fwMapTypesDef::PushParentTypeDef(s32 parentTypeIdx)
{
	if (Verifyf(m_typeDataIndicesCount < MAX_TYPE_DEPS, "Too many fwMapTypesDef indices: %d. Open a B* to Eric J Anderson.", MAX_TYPE_DEPS))
	{
		if(m_typeDataIndicesCount == 0)
		{
			m_typeDataIndicesCount++;
			m_TypeDataIndex.index = parentTypeIdx;
		}
		else
		{
			if(m_typeDataIndicesCount == 1)
			{
				s32 savedParentTypeIdx = m_TypeDataIndex.index;
				m_TypeDataIndex.indices = rage_new atRangeArray<s32, MAX_TYPE_DEPS>();
				(*m_TypeDataIndex.indices)[0] = savedParentTypeIdx;
			}

			(*m_TypeDataIndex.indices)[m_typeDataIndicesCount++] = parentTypeIdx; 
		}
		return(true);
	}
	return(false);
}

}
