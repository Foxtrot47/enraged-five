//
// fwscene/mapdata/maptypes.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef __FWSCENE_MAPTYPES_H__
#define __FWSCENE_MAPTYPES_H__

#include "parser/macros.h"
#include "data/base.h"

#include "atl/array.h"
#include "atl/bitset.h"
#include "atl/map.h"
#include "atl/string.h"
#include "vector/vector3.h"
#include "vector/vector4.h"

#include "entity/archetypemanager.h"
#include "entity/factory.h"


namespace rage 
{
	class fwArchetype;
	class fwMapTypesContents;
	class fwMapTypesDef;

	// PURPOSE:	Base class for all extensions.
	class fwExtensionDef : public datBase
	{
	public:
		virtual ~fwExtensionDef() {}

		atHashString 	m_name;

		PAR_PARSABLE;
	};

	// PURPOSE:	Base class for all archetype definitions.
	class fwArchetypeDef : public datBase
	{
	public:
		enum eAssetType
		{
			ASSET_TYPE_UNINITIALIZED		= 0,
			ASSET_TYPE_FRAGMENT				= 1,
			ASSET_TYPE_DRAWABLE				= 2,
			ASSET_TYPE_DRAWABLEDICTIONARY	= 3,
			ASSET_TYPE_ASSETLESS			= 4
		};

		virtual ~fwArchetypeDef();

		float		m_lodDist;
		u32			m_flags;
		u32			m_specialAttribute;

		Vector3		m_bbMin; 
		Vector3		m_bbMax;
		Vector3		m_bsCentre;
		float		m_bsRadius;

		float			m_hdTextureDist;
		atHashString	m_name; 
		atHashString	m_textureDictionary;
		atHashString	m_clipDictionary;
		atHashString	m_drawableDictionary;
		atHashString	m_physicsDictionary;

		eAssetType		m_assetType;
		atHashString	m_assetName;
	
		atArray<fwExtensionDef*>		m_extensions;

		PAR_PARSABLE;
	};

	// PURPOSE:	Contains definitions for map type data and instances.
	class fwMapTypes
	{
	public:

		enum	{
			GLOBAL				= fwFactory::GLOBAL
		};

		fwMapTypes();
		virtual ~fwMapTypes();

		static void SetMaxFactoryId(u32 id)											 { ms_maxFactoryId = id; }
		static void AssociateArchetypeDef(u32 hash, s32 factoryId);
		static void AssociateExtensionDef(u32 hash, s32 factoryId);

		virtual fwMapTypesContents* CreateMapTypesContents() {return NULL;}

		void Construct(s32 mapTypeDefIndex, fwMapTypesContents* contents) ;

	//protected:
		// PURPOSE: Overload this processing additional attached data
		virtual void PreConstruct(s32 /*mapTypeDefIndex*/, fwMapTypesContents* /*contents*/) {}
		virtual void PostConstruct(s32 /*mapTypeDefIndex*/, fwMapTypesContents* /*contents*/) {}

		void AddArchetypeDef(fwArchetypeDef* definition);
		void AddExtensionDef(fwExtensionDef* definition);

		const atHashString& GetName() const {return m_name;}

		static s32 GetFactoryId(u32 nameHash) { return ms_extensionDefAssociations[nameHash]; }

	protected: 
		virtual	void ConstructLocalExtensions(s32 , fwArchetype* , fwArchetypeDef* ) {}
		
		atArray<fwExtensionDef*>	m_extensions;			// The extensions used in the map.
		atArray<fwArchetypeDef*>	m_archetypes;			// The types used in the map.

		static atMap<u32, s32>		ms_extensionDefAssociations;
		static atMap<u32, s32>		ms_archetypeDefAssociations;

	private:
		void ConstructArchetypes(s32 mapTypeDefIndex, fwMapTypesContents* contents);
		virtual void AllocArchetypeStorage(s32 /*mapTypeDefIndex*/) {}		// alloc storage needed for an .ityp file

		// temp - migrate extension data from fwMapTypes into the correct archetypeDefs
		void MigrateExtensionsToArchetypeDef(fwArchetypeDef* archetypeDef, u32 hashKey);
		// ***
		
		atHashString				m_name; 

		// deprecated - remove at earliest opportunity (when all .ityp files have extensions inside the archetype def)
		atArray<atHashString>		m_dependencies;			// The other mapdata files this one depends on.
		// ***

		static u32					ms_maxFactoryId;
		PAR_PARSABLE;
	};


} // namespace rage

#endif // __FWSCENE_MAPTYPES_H__
