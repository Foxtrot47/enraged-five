/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/mapdata/mapdatadef.h
// PURPOSE : asset store def specific to map data
// AUTHOR :  Ian Kiigan
// CREATED : 27/04/2011
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _FWSCENE_MAPDATA_MAPDATADEF_H_
#define _FWSCENE_MAPDATA_MAPDATADEF_H_

#include "fwscene/mapdata/mapdata.h"
#include "fwscene/mapdata/mapdatacontents.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwtl/assetstore.h"
#include "spatialdata/aabb.h"
#include "streaming/streamingdefs.h"
#include "streaming/streamingmodule.h"

// maximum number of type dependencies allowed per .imap file
#define MAX_MAP_DEPS		(6)
#define MAX_MAP_CHANGES		(256)

namespace rage {

	struct fwMapDataCacheEntry;

	class fwMapDataDef : public fwAssetNameDef<fwMapDataContents>
	{
	public:

		enum eInitState
		{
			NOT_INITIALISED = 0,		// default state for all defs on startup
			REQUIRES_PRELOAD,			// initialised by cache loader, but still needs to be streamed in on startup
			FULLY_INITIALISED			// all metadata requirements etc are satisfied
		};

		~fwMapDataDef()
		{
			if(m_typeDataIndicesCount > 1)
			{
				delete m_TypeDataIndex.indices;
			}
			m_typeDataIndicesCount = 0;
		}

		void Init(const strStreamingObjectNameString name);
		void InitFromMapData(s32 slotIndex, fwMapData* pMapData, spdAABB& dstPhyBounds, spdAABB& dstStrBounds);
		void InitFromCacheEntry(s32 slotIndex, fwMapDataCacheEntry* pCacheEntry, spdAABB& dstPhyBounds, spdAABB& dstStrBounds);

		inline fwEntity**			GetEntities() const						{ return ((m_pObject!=NULL) ? m_pObject->GetEntities() : NULL); }
		inline u32					GetNumEntities() const					{ return ((m_pObject!=NULL) ? m_pObject->GetNumEntities() : 0 ); }
#if __BANK		
		inline s32					GetBlockIndex() const					{ return m_blockIndex; }
		inline void					SetBlockIndex(s32 blockIndex)			{ m_blockIndex = blockIndex; }
#endif
		inline fwMapDataDef*		GetParentDef() const					{ return m_pParentDef; }
		inline void					SetParentDef(fwMapDataDef* pParentDef)	{ m_pParentDef = pParentDef; }
		inline bool					IsLoaded() const						{ return (m_pObject!=NULL); }
		inline void					SetNumLoadedChildren(u32 numChildren) 	{ m_flags.m_numLoadedChildren = numChildren; }
		inline u32					GetNumLoadedChildren() const			{ return m_flags.m_numLoadedChildren; }
		inline void					AddChild()								{ m_flags.m_numLoadedChildren++; }
		inline void					RemoveChild()							{ m_flags.m_numLoadedChildren--; }
		inline bool					GetIsScriptManaged() const				{ return m_flags.m_bScriptManaged; }
		inline void					SetIsScriptManaged(bool bManaged)		{ m_flags.m_bScriptManaged = bManaged; }
		inline bool					GetIsCodeManaged() const				{ return m_flags.m_bCodeManaged; }
		inline void					SetIsCodeManaged(bool bManaged)			{ m_flags.m_bCodeManaged = bManaged; }
		u32							GetContentFlags() const					{ return m_contentFlags; }

		inline strLocalIndex		GetParentTypeIndex(u32 index) const		{ return (m_typeDataIndicesCount <= 1) ? strLocalIndex(m_TypeDataIndex.index) : strLocalIndex((*m_TypeDataIndex.indices)[index]); }
		inline u32					GetNumParentTypes()	const				{ return m_typeDataIndicesCount; }
		inline bool					IsSetAsParentTypeDef(s32 parentTypeIdx) 
		{ 
			if(m_typeDataIndicesCount == 0)
			{
				return false;
			}
			else if (m_typeDataIndicesCount == 1)
			{
				return m_TypeDataIndex.index == parentTypeIdx;
			}
			else
			{
				return m_TypeDataIndex.indices->ReverseFind(parentTypeIdx, m_typeDataIndicesCount - 1) != -1;
			}
		}
		inline bool					PushParentTypeDef(s32 parentTypeIdx)
		{
			if (Verifyf(m_typeDataIndicesCount < MAX_MAP_DEPS, "Too many fwMapDataDef indices: %d.", MAX_MAP_DEPS))
			{
				if(m_typeDataIndicesCount == 0)
				{
					m_typeDataIndicesCount++;
					m_TypeDataIndex.index = parentTypeIdx;
				}
				else
				{
					if(m_typeDataIndicesCount == 1)
					{
						s32 savedParentTypeIdx = m_TypeDataIndex.index;
						m_TypeDataIndex.indices = rage_new atRangeArray<s32, MAX_MAP_DEPS>();
						(*m_TypeDataIndex.indices)[0] = savedParentTypeIdx;
					}
					
					(*m_TypeDataIndex.indices)[m_typeDataIndicesCount++] = parentTypeIdx; 
				}
				return(true);
			}
			return(false);
		}

		inline fwMapDataContents*	GetContents()							{ return m_pObject; }
		inline fwMapDataContents*	GetParentContents()						{ return m_pParentDef ? m_pParentDef->GetContents() : NULL; }
		inline void					SetInitState(eInitState initState)		{ m_flags.m_initState = initState; }
		inline u32					GetInitState() const					{ return m_flags.m_initState; }
		inline bool					GetHasBlockInfo() const					{ return m_flags.m_bContainsBlockInfo; }
		inline void					SetHasBlockInfo(bool bHasBlockInfo)		{ m_flags.m_bContainsBlockInfo = bHasBlockInfo; }
		inline bool					GetIsValid() const						{ return m_flags.m_bIsValid; }
		inline void					SetIsValid(bool bValid)					{ m_flags.m_bIsValid = bValid; }
		inline bool					GetIsParent() const						{ return m_flags.m_bIsParent; }
		inline void					SetIsParent(bool bIsParent)				{ m_flags.m_bIsParent = bIsParent; }
		inline void					SetIsInPlace(bool bIsInPlace)			{ m_flags.m_bIsInPlace = bIsInPlace; }
		inline bool					GetIsInPlace() const					{ return m_flags.m_bIsInPlace; }
		inline void					SetIsMLOInstanceDef(bool bIsMLO)		{ m_flags.m_bContainsMLOInstanceDef = bIsMLO; }
		inline bool					GetIsMLOInstanceDef() const				{ return m_flags.m_bContainsMLOInstanceDef; }
		inline void					SetPostponeWorldAdd(bool bPostpone)		{ m_flags.m_bPostponeWorldAdd = bPostpone; }
		inline bool					GetPostponeWorldAdd() const				{ return m_flags.m_bPostponeWorldAdd; }
		inline void					SetDepth(u32 depth)						{ m_flags.m_depth = depth; }
		inline u32					GetDepth() const						{ return m_flags.m_depth; }

		void						CalcDepth();

		void						ResetNumMapChanges()					{ m_flags.m_numMapChanges=0; }
		u32							GetNumMapChanges() const				{ return m_flags.m_numMapChanges; }
		void						AddMapChange();
		void						RemoveMapChange();

		bool						RequiresTempMemory() const				{ return (m_contentFlags & fwMapData::CONTENTFLAG_MASK_RETAINED)==0; }

		inline bool					GetIsCulled() const						{ return m_flags.m_bCulled; }
		inline void					SetIsCulled(bool bCulled)				{ m_flags.m_bCulled = bCulled; UpdateCullState(); }
		inline bool					GetIsPinned() const						{ return m_flags.m_bPinned; }
		inline void					SetIsPinned(bool bPinned)				{ m_flags.m_bPinned = bPinned; UpdateCullState(); }
		inline bool					IsCullPermitted() const					{ return (GetIsCulled() && !GetIsPinned()); }

		inline bool					IsLodLightSpecific() const
		{
			return ( m_contentFlags & (fwMapData::CONTENTFLAG_DISTANT_LOD_LIGHTS | fwMapData::CONTENTFLAG_LOD_LIGHTS) )!=0;
		}

		inline bool					IsOccluderSpecific() const
		{
			return ( m_contentFlags & (fwMapData::CONTENTFLAG_OCCLUDER) )!=0;
		}

#if __ASSERT
		bool RequiresDebugValidation() const								{ return m_flags.m_bRequiresValidation; }
		void SetRequiresDebugValidation(bool bRequiresValidation)			{ m_flags.m_bRequiresValidation = bRequiresValidation; }
#endif	//__ASSERT

	protected:
		inline void					InitCommon(s32 slotIndex, u32 parentHash, bool bStreamable, u32 contentFlags, bool bHasBlockInfo, bool bIsParent);

	private:

		inline void UpdateCullState()
		{
			if ( m_pObject && m_pObject->GetSceneGraphNode() )
			{
				m_pObject->GetSceneGraphNode()->Enable( !IsCullPermitted() );
			}
		}

		fwMapDataDef*			m_pParentDef;		// for lod attachment

		typedef union
		{
			s32 index;
			atRangeArray<s32, MAX_MAP_DEPS>* indices;	// type data dependency for these instances
		} uTypeDataIndex;

		uTypeDataIndex m_TypeDataIndex;
		

		struct  
		{
			u32					m_numLoadedChildren			: 8;
			u32					m_bScriptManaged			: 1;
			u32					m_bCodeManaged				: 1;
			u32					m_initState					: 2;
			u32					m_bContainsBlockInfo		: 1;
			u32					m_bIsValid					: 1;
			u32					m_bIsParent					: 1;
			u32					m_bIsInPlace				: 1;
			u32					m_bPostponeWorldAdd			: 1;
			u32					m_depth						: 2;
			u32					m_numMapChanges				: 8;
			u32					m_bContainsMLOInstanceDef	: 1;
			u32					m_bCulled					: 1;
			u32					m_bPinned					: 1;
			u32					m_bRequiresValidation		: 1;		// dbg only
			u32					m_unused					: 1;
			
		} m_flags;

#if __BANK
		s32						m_blockIndex;
#endif
		u32						m_contentFlags : 16;
		u32						m_typeDataIndicesCount : 16;
	};

	struct fwMapDataCacheBlockInfo
	{
 		fwMapDataCacheBlockInfo();

		enum
		{
			BLOCKINFO_LEN_MAX = 92
		};
		u32 m_flags;
		char m_name_owner_exportedBy_time[BLOCKINFO_LEN_MAX];

	};

	struct fwMapDataCacheEntry 
	{
		fwMapDataCacheEntry() {}
		fwMapDataCacheEntry(fwMapDataDef* pDef, const spdAABB& phyBounds, const spdAABB& strBounds);
		fwMapDataCacheEntry(const void* const pEntry) { memcpy(this, pEntry, sizeof(fwMapDataCacheEntry)); }

		u32						m_nameHash;
		u32						m_parentNameHash;
		u32						m_contentFlags;
		float					m_strMinX, m_strMinY, m_strMinZ, m_strMaxX, m_strMaxY, m_strMaxZ;
		float					m_phyMinX, m_phyMinY, m_phyMinZ, m_phyMaxX, m_phyMaxY, m_phyMaxZ;
		bool					m_bDynamicStreaming;
		bool					m_bContainsBlockInfo;
		bool					m_bIsParent;

#if __BANK
		fwMapDataCacheBlockInfo m_blockInfo;
#endif
	};

} //namespace rage

#endif	//_FWSCENE_MAPDATA_MAPDATADEF_H_
	
