//
// fwscene/mapdata/mapdata.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef __SCENE_MAPINSTANCEDATA_H__
#define __SCENE_MAPINSTANCEDATA_H__

#include <functional>

#include "parser/macros.h"
#include "data/base.h"

#include "atl/array.h"
#include "atl/hashstring.h"
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/color32.h"
#include "spatialdata/aabb.h"
#include "streaming/streamingdefs.h"

namespace rage {

//Forward Declarations
class grcInstanceBuffer;
class grcVecArrayInstanceBufferList;
class grcFvf;
class grcVertexBuffer;
class rmcDrawable;

template <int numFramesToBuffer> class grcBufferedStaticInstanceBufferList;

//
// Resource Ready (Optimized) Instance List Definitions
//

// PURPOSE:	Prop instance list definition.
class fwPropInstanceListDef
{
public:
	struct InstanceData;
	typedef atArray<InstanceData> InstanceDataList;

	fwPropInstanceListDef() : m_IbList(NULL) { }
	~fwPropInstanceListDef();

	//Instance draw support
	void CreateDeviceResources() const;
	void ReleaseDeviceResources() const;
	grcInstanceBuffer *CreateInstanceBuffer() const;

public:
	//Runtime Instance Data Structure
	struct InstanceData
	{
		InstanceData();

		//Instanced Draw Interface
		void WriteInstanceData(grcVecArrayInstanceBufferList &list) const;
		void WriteInstanceData(Vec4V *ib) const;

		//Convert back to Mat34V.
		void GetMatrixCopy(Mat34V_InOut mat) const;
		Vec3V_Out GetPosition() const;

		//Convenience functions
		void SetIsVisible(bool isVisible) const	{ m_IsVisible = isVisible; }
		void SetGlobals(Vec4V_In globals) const	{ m_Globals = globals; }


		Vec4V m_InstMat[3];
		mutable Vec4V m_Globals;
		Color32 m_Tint;
		u32 m_Index;
		
		mutable bool m_IsVisible : 1;

		static const u32 sNumInstanceBufferRegisters;

		PAR_SIMPLE_PARSABLE;
	};

public:
	InstanceDataList m_InstanceList;
	spdAABB m_BatchAABB;
	atHashString m_archetypeName;
	u32 m_lodDist;


#if __D3D11 || RSG_ORBIS
	static const u32 MaxFrames = 3;
#else
	static const u32 MaxFrames = 2;
#endif

	typedef grcBufferedStaticInstanceBufferList<MaxFrames> IBList;
	const IBList *GetStaticInstanceBufferList() const	{ return m_IbList; }

private:
	//Static instance buffer list
	mutable IBList *m_IbList;

	PAR_SIMPLE_PARSABLE;
};

// PURPOSE:	Grass instance list definition.
class fwGrassInstanceListDef
{
public:
#if __WIN32 && _MSC_VER == 1500 //this is the code for VS2008
	typedef std::tr1::function<void * (fwGrassInstanceListDef &)> device_resource_create_functor_type;
	typedef std::tr1::function<void (void *)> device_resource_release_functor_type;
	typedef std::tr1::function<void (strLocalIndex, rmcDrawable *)> drawable_placement_notify_functor_type;
	typedef std::tr1::function<void (strLocalIndex)> drawable_remove_notify_functor_type;
#else
	typedef std::function<void * (fwGrassInstanceListDef &)> device_resource_create_functor_type;
	typedef std::function<void (void *)> device_resource_release_functor_type;
	typedef std::function<void (strLocalIndex, rmcDrawable *)> drawable_placement_notify_functor_type;
	typedef std::function<void (strLocalIndex)> drawable_remove_notify_functor_type;
#endif	

	static grcFvf GetFVF();
	static void PostPsoPlace(void* data);

	static void Init(const device_resource_create_functor_type &createFunc, const device_resource_release_functor_type &releaseFunc);
	static void SetDrawablePlacementNotifyFuncs(const drawable_placement_notify_functor_type &place, const drawable_remove_notify_functor_type &remove) { sm_DrawablePlacementNotifyFunc = place; sm_DrawableRemoveNotifyFunc = remove; }
	static void NotifyDrawablePlacement(strLocalIndex drawableIdx, rmcDrawable *drawable) { if(sm_DrawablePlacementNotifyFunc) sm_DrawablePlacementNotifyFunc(drawableIdx, drawable); }
	static void NotifyDrawableRemove(strLocalIndex drawableIdx) { if(sm_DrawableRemoveNotifyFunc) sm_DrawableRemoveNotifyFunc(drawableIdx); }

	fwGrassInstanceListDef() : m_ScaleRange(1.0f, 1.0f, 0.0f), m_DeviceResources(NULL)
	{
	}

	~fwGrassInstanceListDef();

	Vec3V_Out ComputeLodInstFadeRange(ScalarV_In distToCamera) const;
	inline Vec3V_Out ComputeLodInstFadeRange(float distToCamera) const	{ return ComputeLodInstFadeRange(ScalarV(distToCamera)); }

	//Creates the vertex buffer from the instance data. Should only be called outside of fwGrassInstanceListDef if a fixup occurred and prevented stream creation in PostPsoPlace
	void CreateDeviceResources();
	void ReleaseDeviceResources() const;

	void *GetDeviceResources() const	{ Assert(m_DeviceResources != NULL); return m_DeviceResources; } //If device resources are NULL, PostPsoPlace may not have been called due to a fixup.
	template <class T> T *GetDeviceResources() const	{ Assert(m_DeviceResources != NULL); return reinterpret_cast<T *>(m_DeviceResources); }

public:
	//Runtime Instance Data Structure
	struct InstanceData
	{
		u16 m_Position[3];
		u8 m_NormalX;
		u8 m_NormalY;
		u8 m_Color[3];
		u8 m_Scale;
		u8 m_Ao;
		u8 m_Pad[3];

		Vec3V_Out ComputeNormal() const;

		PAR_SIMPLE_PARSABLE;
	};

	typedef atArray<InstanceData> InstanceDataList;

public:
	//Need to add a vertex buffer
	spdAABB m_BatchAABB;
	Vec3V m_ScaleRange;				//xy = min|max range, z = scaler for random scale
	atHashString m_archetypeName;
	u32 m_lodDist;

	float m_LodFadeStartDist;	//Start range to compute LOD factor. (ie: fade between [m_LodFadeStartDist, m_lodDist])
	float m_LodInstFadeRange;	//Normalized range for adjusting instance fading window.

	float m_OrientToTerrain;

	InstanceDataList m_InstanceList;

private:
	static device_resource_create_functor_type sm_CreateDeviceResourcesFunc;
	static device_resource_release_functor_type sm_ReleaseDeviceResourcesFunc;
	static drawable_placement_notify_functor_type sm_DrawablePlacementNotifyFunc;
	static drawable_remove_notify_functor_type sm_DrawableRemoveNotifyFunc;

	mutable void *m_DeviceResources;

	PAR_SIMPLE_PARSABLE;
};



//
// Map container
//

// PURPOSE:	Contains definitions for map data, both types and instances.
class fwInstancedMapData : public datBase
{
public:
	void ReleaseDeviceResources();

	atHashString m_ImapLink;
	atArray<fwPropInstanceListDef> m_PropInstanceList;
	atArray<fwGrassInstanceListDef> m_GrassInstanceList;

	PAR_PARSABLE;
};

} // namespace rage

#endif //__SCENE_MAPINSTANCEDATA_H__
