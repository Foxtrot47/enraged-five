//
// fwscene/mapdata/maptypes.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "maptypes.h"

#include "diag/art_channel.h"
#include "parser/manager.h"
#include "maptypes_parser.h"

#include "entity/archetype.h"
#include "entity/archetypemanager.h"
#include "fwscene/stores/maptypesstore.h"

#include "diag/art_channel.h"


namespace rage {

u32 fwMapTypes::ms_maxFactoryId;
atMap<u32, s32> fwMapTypes::ms_archetypeDefAssociations;
atMap<u32, s32> fwMapTypes::ms_extensionDefAssociations;

fwArchetypeDef::~fwArchetypeDef()
{
	const int count = m_extensions.GetCount();
	for (int i = 0; i < count; ++i)
	{
		delete m_extensions[i];
	}
	m_extensions.Reset();
}

fwMapTypes::fwMapTypes() {}

fwMapTypes::~fwMapTypes()
{
	{
		const int count = m_archetypes.GetCount();
		for (int i = 0; i < count; ++i)
		{
			delete m_archetypes[i];
		}
		m_archetypes.Reset();
	}

	{
		const int count = m_extensions.GetCount();
		for (int i = 0; i < count; ++i)
		{
			delete m_extensions[i];
		}
		m_extensions.Reset();
	}
}

void fwMapTypes::AssociateArchetypeDef(u32 hash, s32 factoryId)
{
	Assertf(!ms_archetypeDefAssociations.Access(hash), "Association already exists %u -> %u", hash, factoryId);
	ms_archetypeDefAssociations[hash] = factoryId;
}

void fwMapTypes::AssociateExtensionDef(u32 hash, s32 factoryId)
{
	Assertf(!ms_extensionDefAssociations.Access(hash), "Association already exists %u -> %u", hash, factoryId);
	ms_extensionDefAssociations[hash] = factoryId;
}

void fwMapTypes::Construct(s32 mapTypeDefIndex, fwMapTypesContents* contents) 
{ 
	PreConstruct(mapTypeDefIndex, contents);
	ConstructArchetypes(mapTypeDefIndex, contents);
	PostConstruct(mapTypeDefIndex, contents);
}

void fwMapTypes::MigrateExtensionsToArchetypeDef(fwArchetypeDef* archetypeDef, u32 hashKey){

	u32 count = m_extensions.GetCount();

	for(u32 i = 0; i<count ; i++){
		if (hashKey == m_extensions[i]->m_name.GetHash()){
			Assertf(false,"This code path is deprecated! Extensions should _not_ be migrated to archetype def now. : %s",archetypeDef->m_name.GetCStr());
			archetypeDef->m_extensions.PushAndGrow(m_extensions[i]);
			m_extensions.DeleteFast(i);
			i--;
			count--;
		}
	}
}

void fwMapTypes::ConstructArchetypes(s32 mapTypeDefIndex,  fwMapTypesContents* ASSERT_ONLY(contents))
{
	Assert(mapTypeDefIndex > -1);
	Assert(contents != NULL);

	// construct a storage silo for the archetypes in this file (use the mapTypeDefIndex to define it)
	AllocArchetypeStorage(mapTypeDefIndex);

	const int count = m_archetypes.GetCount();
	for (int i = 0; i < count; ++i)
	{
		fwArchetypeDef* definition = m_archetypes[i];

#if __ASSERT && !__RESOURCECOMPILER		// This pulls in a bunch of other code...
		bool bIsMloDef = definition->parser_GetStructure()->GetNameHash() == atLiteralStringHash("CMloArchetypeDef");
		artAssertf(bIsMloDef || definition->m_lodDist>0.0f, "%s has an invalid default LOD distance - FIX IN MAX AND REEXPORT", definition->m_name.GetCStr());
		if ( !fwArchetypeManager::VerifyNameClashes( definition->m_name.GetCStr() ) )
		{
			fwModelId firstRegistered;
			fwArchetypeManager::GetArchetypeFromHashKey(definition->m_name.GetHash(), firstRegistered);
			firstRegistered.GetTypeFileName();

			artAssertf(false,"model <%s> is defined in both '%s.ityp' and '%s.ityp'. Second definition ignored. REEXPORT NEEDED FOR MAPSECTION", definition->m_name.GetCStr(), firstRegistered.GetTypeFileName(),
																					g_MapTypesStore.GetName(strLocalIndex(mapTypeDefIndex)));
			continue;
		}
#endif

		s32 factoryId = ms_archetypeDefAssociations[definition->parser_GetStructure()->GetNameHash()];

		fwArchetype* archetype = fwArchetypeManager::GetArchetypeFactory(factoryId)->CreateBaseItem(mapTypeDefIndex);

		archetype->InitArchetypeFromDefinition(strLocalIndex(mapTypeDefIndex), definition, true);		

		// temp code - until .ityp data contains extensions in each archetype
		MigrateExtensionsToArchetypeDef(definition, archetype->GetHashKey());
		// ***

		ConstructLocalExtensions(mapTypeDefIndex, archetype, definition);

#if __BANK
		archetype->DebugPostInit();
#endif // __BANK
	}
}

void fwMapTypes::AddArchetypeDef(fwArchetypeDef* definition)
{
	m_archetypes.Grow() = definition;
}

void fwMapTypes::AddExtensionDef(fwExtensionDef* definition)
{
	m_extensions.Grow() = definition;
}

} // namespace rage
