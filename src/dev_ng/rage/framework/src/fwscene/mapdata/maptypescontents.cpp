//
// fwscene/stores/maptypescontents.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "fwscene/mapdata/maptypescontents.h"
#include "fwscene/mapdata/maptypes.h"

namespace rage {

fwMapTypesContents::fwMapTypesContents() :
	m_name("")
{
}

fwMapTypesContents::~fwMapTypesContents()
{
	Assertf(!m_loaderInst.IsLoaded(), "Call Unload before deleting this");
}

} //namespace rage