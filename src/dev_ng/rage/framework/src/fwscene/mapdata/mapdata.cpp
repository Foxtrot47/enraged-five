//
// fwscene/mapdata/mapdata.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "mapdata.h"

#include "parser/manager.h"
#include "mapdata_parser.h"

#include "entity/archetype.h"
#include "entity/archetypemanager.h"
#include "fwscene/lod/LodAttach.h"
#include "fwscene/world/worldmgr.h"
#include "fwscene/world/streamedscenegraphnode.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/stores/maptypesstore.h"
#include "streaming/streamingvisualize.h"

#include "diag/art_channel.h"


namespace rage {


atMap<u32, s32>							fwMapData::ms_entityDefAssociations;
atMap<u32, s32>							fwMapData::ms_extensionDefAssociations;
fwEntityDef::ePriorityLevel				fwMapData::ms_entityLevelCap = fwEntityDef::PRI_OPTIONAL_LOW;	// anything lower priority isn't instanced

fwEntityDef::~fwEntityDef()
{
	const int count = m_extensions.GetCount();
	for (int i = 0; i < count; ++i)
	{
		delete m_extensions[i];
	}
	m_extensions.Reset();
}

fwMapData::~fwMapData()
{
	const int count = m_entities.GetCount();
	for (int i = 0; i < count; ++i)
	{
		delete m_entities[i];
	}
	m_entities.Reset();
}

void fwMapData::AssociateExtensionDef(const u32 hash, s32 factoryId)
{
	Assertf( !ms_extensionDefAssociations.Access(hash), "Association already exists %u -> %d", hash, factoryId);
	ms_extensionDefAssociations[hash] = factoryId;
}

s32 fwMapData::GetExtensionFactory(const u32 hash)
{
	Assertf( ms_extensionDefAssociations.Access(hash), "No extension factory association for %u.", hash );
	return ms_extensionDefAssociations[hash];
}

void fwMapData::SetStreamingExtents(Vector3::Param min, Vector3::Param max)
{
	m_streamingExtentsMin = min;
	m_streamingExtentsMax = max;
}

fwEntity* fwMapData::ConstructEntity(fwEntityDef* definition, s32 mapDataSlotIndex, fwArchetype* archetype, fwModelId &modelId)
{
#if !__RESOURCECOMPILER
#if __ASSERT
	u32 compareIndex;
	fwArchetype* compareAchetype = fwArchetypeManager::GetArchetypeFromHashKey(definition->m_archetypeName.GetHash(), &compareIndex);
	Assertf(compareAchetype == archetype, "fwMapData::ConstructEntity archetypes dont match");
	Assertf((!modelId.IsValid()) || ((u32)compareIndex == (u32)modelId.GetModelIndex()), "fwMapData::ConstructEntity model index dont match");
#endif // __ASSERT

	/*
	// find the parent .ityp for this .imap data
	s32 parentTypeSlotIndex = -1;
	fwMapDataDef* pMapDataDef =	INSTANCE_STORE.GetSlot(mapDataSlotIndex);
	if (pMapDataDef){
		parentTypeSlotIndex = pMapDataDef->GetParentTypeDef();
	}
	*/

	if (artVerifyf(archetype!=NULL, "Cannot find model %s at (%f, %f, %f).",
		definition->m_archetypeName.GetCStr(), definition->m_position.x, definition->m_position.y, definition->m_position.z))
	{
		if (archetype->IsStreamedArchetype())
		{
			// verify that the .ityp file defining the archetype is a dependency of this mapdata slot (or is global)
			fwMapDataDef* pMapDataDef =	INSTANCE_STORE.GetSlot(strLocalIndex(mapDataSlotIndex));
			strLocalIndex mapTypeSlotIndex = strLocalIndex(modelId.GetMapTypeDefIndex());

			
			bool bFoundAsDLCPermanent = false;
			fwMapTypesDef* pDef = g_MapTypesStore.GetSlot(mapTypeSlotIndex);
			if (pDef->GetIsPermanentDLC())
			{
				bFoundAsDLCPermanent = true;
			}

			bool bFoundAsDependency = false;
			u32 numParentTypeDefs = pMapDataDef->GetNumParentTypes();
			for(u32 idx = 0; idx < numParentTypeDefs; idx++)
			{
				fwMapTypesDef* pMapParentTypeDef = g_MapTypesStore.GetSlot(strLocalIndex(pMapDataDef->GetParentTypeIndex(idx)));
				Assert(pMapParentTypeDef);

				if ( pMapDataDef->IsSetAsParentTypeDef(mapTypeSlotIndex.Get()) || (pMapParentTypeDef && pMapParentTypeDef->IsSetAsParentTypeDef(mapTypeSlotIndex.Get())))
				{
					bFoundAsDependency = true;
					break;
				}
			}

			if (!(bFoundAsDependency || bFoundAsDLCPermanent))
			{
				Warningf("skipping model %s, because it is defined in <%s.ityp> which is not a dependency of <%s.imap>", definition->m_archetypeName.GetCStr(), 
					g_MapTypesStore.GetName(mapTypeSlotIndex), INSTANCE_STORE.GetName(mapDataSlotIndex));

				Assertf(false,"skipping model %s, because it is defined in <%s.ityp> which is not a dependency of <%s.imap>", definition->m_archetypeName.GetCStr(), 
					g_MapTypesStore.GetName(strLocalIndex(mapTypeSlotIndex)), INSTANCE_STORE.GetName(strLocalIndex(mapDataSlotIndex)));
				return(NULL);
			}
		}

		fwEntity* entity = archetype->CreateEntity();
		if (entity)
		{ 
			entity->SetModelId(modelId);
			//entity->SetArchetype(archetype);

			entity->InitEntityFromDefinition(definition, archetype, mapDataSlotIndex);
			STRVIS_ADD_MAPDATA_ENTITY(g_MapDataStore.GetStreamingIndex(strLocalIndex(mapDataSlotIndex)), entity, (int) definition->m_guid);
		}

		return(entity);
	}
#else
	(void)definition;
	(void)mapDataSlotIndex;
	(void)archetype;
	(void)modelId;
#endif
	return NULL;
}

fwEntity* fwMapData::ConstructEntity(const fwPropInstanceListDef* definition, s32 mapDataSlotIndex)
{
#if !__RESOURCECOMPILER
	u32 index;
	fwArchetype* archetype = fwArchetypeManager::GetArchetypeFromHashKey(definition->m_archetypeName.GetHash(), &index);

	if (artVerifyf(archetype!=NULL, "Cannot find batch model %s with %d instances centered at (%f, %f, %f).", definition->m_archetypeName.GetCStr(), definition->m_InstanceList.size(), 
		definition->m_BatchAABB.GetCenter().GetXf(), definition->m_BatchAABB.GetCenter().GetYf(), definition->m_BatchAABB.GetCenter().GetZf()))
	{
		fwModelId modelId;
		modelId.SetModelIndex(index);

		fwEntity* entity = archetype->CreateEntityFromDefinition(definition);
		if(entity)
		{ 
			entity->SetModelId(modelId);
			//entity->SetArchetype(archetype);

			entity->InitEntityFromDefinition(definition, archetype, mapDataSlotIndex);
		}

		return(entity);
	}
#else
	(void)definition;
	(void)mapDataSlotIndex;
#endif
	return NULL;
}

fwEntity* fwMapData::ConstructEntity(fwGrassInstanceListDef* definition, s32 mapDataSlotIndex)
{
#if !__RESOURCECOMPILER
	u32 index;
	fwArchetype* archetype = fwArchetypeManager::GetArchetypeFromHashKey(definition->m_archetypeName.GetHash(), &index);

	if (artVerifyf(archetype!=NULL, "Cannot find batch model %s with %d instances centered at (%f, %f, %f).", definition->m_archetypeName.GetCStr(), definition->m_InstanceList.size(), 
		definition->m_BatchAABB.GetCenter().GetXf(), definition->m_BatchAABB.GetCenter().GetYf(), definition->m_BatchAABB.GetCenter().GetZf()))
	{
		fwModelId modelId;
		modelId.SetModelIndex(index);

		fwEntity* entity = archetype->CreateEntityFromDefinition(definition);
		if(entity)
		{ 
			entity->SetModelId(modelId);
			//entity->SetArchetype(archetype);

			entity->InitEntityFromDefinition(definition, archetype, mapDataSlotIndex);
		}

		return(entity);
	}
#else
	(void)definition;
	(void)mapDataSlotIndex;
#endif
	return NULL;	//TODO: Implement grass batch entity construction
}

void fwMapData::AddEntityDef(fwEntityDef* definition)
{
	m_entities.Grow() = definition;
}


} // namespace rage
