<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
 generate="psoChecks">


  <enumdef type="::rage::ePriorityLevel">
    <enumval name="PRI_REQUIRED"        value="0" />
    <enumval name="PRI_OPTIONAL_HIGH"   value="1" />
    <enumval name="PRI_OPTIONAL_MEDIUM" value="2" />
    <enumval name="PRI_OPTIONAL_LOW"    value="3" />
  </enumdef>

  <enumdef type="::rage::eLodType">
    <enumval name="LODTYPES_DEPTH_HD" value="0"/>
    <enumval name="LODTYPES_DEPTH_LOD" value="1"/>
    <enumval name="LODTYPES_DEPTH_SLOD1" value="2"/>
    <enumval name="LODTYPES_DEPTH_SLOD2" value="3"/>
    <enumval name="LODTYPES_DEPTH_SLOD3" value="4"/>
    <enumval name="LODTYPES_DEPTH_ORPHANHD" value="5"/>
    <enumval name="LODTYPES_DEPTH_SLOD4" value="6"/>
  </enumdef>
  
	<!-- Entity definition -->
	<structdef type="::rage::fwEntityDef" >
		<string 	name="m_archetypeName" type="atHashString" ui_key="true"/>
		<u32 		name="m_flags" />
    <u32    name="m_guid" />
    <Vector3 	name="m_position" />
		<Vector4 	name="m_rotation" />
		<float		name="m_scaleXY" />
		<float		name="m_scaleZ" />

    <!-- passed to fwLodData -->
    <s32 	name="m_parentIndex" />
    <float 	name="m_lodDist" />
    <float  name="m_childLodDist" />
    <enum   name="m_lodLevel" type="::rage::eLodType"/>
    <u32 		name="m_numChildren" />

    <enum   name="m_priorityLevel" type="::rage::ePriorityLevel"/>
    <array		name="m_extensions" type="atArray">
      <pointer type="::rage::fwExtensionDef" policy="owner"/>
    </array>
	</structdef> 

  <!-- Container LOD definition -->
  <structdef type="::rage::fwContainerLodDef"  simple="true">
    <string		name="m_name" type="atHashString" />
    <u32 name="m_parentIndex" />
  </structdef>
	
	<!-- Map data - defined a set of archetypes and entities for a particular map section. -->
	<structdef type="::rage::fwMapData" >
		<string		name="m_name" type="atHashString" />
		<string		name="m_parent" type="atHashString" />

    <u32 name="m_flags"/>
    <u32 name="m_contentFlags"/>
			
		<Vector3 	name="m_streamingExtentsMin" />
		<Vector3 	name="m_streamingExtentsMax" />
		<Vector3 	name="m_entitiesExtentsMin" />
		<Vector3 	name="m_entitiesExtentsMax" />
		
		<array		name="m_entities" type="atArray">
			<pointer type="::rage::fwEntityDef" policy="owner"/>
		</array>

    <array name="m_containerLods" type="atArray">
      <struct type="::rage::fwContainerLodDef"/>
    </array>

    <array name="m_boxOccluders" type="atArray" align="16">
      <struct type="::rage::BoxOccluder"/>
    </array>

    <array name="m_occludeModels" type="atArray" align="16">
      <struct type="::rage::OccludeModel"/>
    </array>

    <array name="m_physicsDictionaries" type="atArray">
      <string type="atHashString"/>
    </array>

    <struct name="m_instancedData" type="::rage::fwInstancedMapData"/>
</structdef>

</ParserSchema>
