
/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/mapdata/mapdatadebug.cpp
// PURPOSE : some bits of debug functionality for mapdatastore, best separated out
// AUTHOR :  Ian Kiigan
// CREATED : 03/06/2011
//
/////////////////////////////////////////////////////////////////////////////////

#include "fwscene/mapdata/mapdatadebug.h"

#if __BANK

#include "entity/entity.h"
#include "file/asset.h"
#include "grcore/debugdraw.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/mapdata/mapdatacontents.h"
#include "fwscene/mapdata/mapdatadef.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/mapdata/maptypesdef.h"
#include "fwscene/stores/maptypesstore.h"
#include "streaming/packfilemanager.h"
#include "vector/colors.h"

XPARAM(inflateimaphigh);
XPARAM(inflateimapmedium);
XPARAM(inflateimaplow);

namespace rage
{

bool fwMapDataDebug::ms_bShowEntityCounts = false;
bool fwMapDataDebug::ms_bShowLoadedStats = false;
bool fwMapDataDebug::ms_bShowMatrixUse = false;
bool fwMapDataDebug::ms_bShowDependencies = false;
atArray<const char*> fwMapDataDebug::ms_apszDebugImapNames;
atArray<const char*> fwMapDataDebug::ms_apszImapGroupNames;
atArray<s32> fwMapDataDebug::ms_slotIndices;
atArray<s32> fwMapDataDebug::m_imapGroupSlotIndices;
s32 fwMapDataDebug::ms_selectedImapComboIndex = 0;
s32 fwMapDataDebug::ms_selectedImapGroupComboIndex = 0;
s32 fwMapDataDebug::ms_debugImapIndex = -1;
bool fwMapDataDebug::ms_bStreamable = false;
bool fwMapDataDebug::ms_bShowBadBoundsOnly = false;
bool fwMapDataDebug::ms_bValidateStrBounds = false;
bool fwMapDataDebug::ms_bValidatePhyBounds = false;
bool fwMapDataDebug::ms_bUsePivotsForValidation = true;
bool fwMapDataDebug::ms_bSuppressedSelected = false;
bool fwMapDataDebug::ms_bNoHdWhenFlyingFast = true;
atArray<s32> fwMapDataDebug::ms_suppressedImaps;
bkText* fwMapDataDebug::ms_pExtentsText = NULL;
s32 fwMapDataDebug::ms_imapGroupTransitionStart = 0;
s32 fwMapDataDebug::ms_imapGroupTransitionEnd = 0;
bool fwMapDataDebug::ms_bAutomaticTransition = true;
bool fwMapDataDebug::ms_bShowActiveTransitions = false;
s32 fwMapDataDebug::ms_transitionIndex = 0;

float fwMapDataDebug::ms_fInflateRadius_Low = 0.0f;
float fwMapDataDebug::ms_fInflateRadius_Medium = 0.0f;
float fwMapDataDebug::ms_fInflateRadius_High = 0.0f;

u32 fwMapDataDebug::ms_dbgNumLoaded[fwBoxStreamerAsset::ASSET_TOTAL];
u32 fwMapDataDebug::ms_dbgNumRequired[fwBoxStreamerAsset::ASSET_TOTAL];

static void sOutputList()
{
	atMap<u32, const char*> uniques;

	for(s32 i=0; i<g_MapTypesStore.GetSize(); i++)
	{
		fwMapTypesDef* pDef = g_MapTypesStore.GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsNativeFile())
		{
			if (pDef->GetIsPso() && pDef->GetIsFastPso())
			{
				continue;
			}

			strStreamingInfo& info = strStreamingEngine::GetInfo().GetStreamingInfoRef(g_MapTypesStore.GetStreamingIndex(strLocalIndex(i)));
			strStreamingFile* file = strPackfileManager::GetImageFileFromHandle(info.GetHandle());

			if (file)
			{
				uniques[file->m_name.GetHash()] = file->m_name.GetCStr();
			}
		}
	}
	

	char filename[255];
	fiSafeStream s = fiStream::Create("C:\\list.txt", fiDeviceLocal::GetInstance());
	if (s)
	{
		atMap<u32, const char*>::Iterator entry = uniques.CreateIterator();
		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			ASSET.RemoveExtensionFromPath(filename, sizeof(filename), entry.GetData());
			fprintf(s,"%s\n", ASSET.FileName(filename));
		}
	}
}
//////////////////////////////////////////////////////////////////////////
// FUNCTION:	InitWidgets
// PURPOSE:		add debug widgets related to map data store
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::InitWidgets(bkBank& bank)
{
	bank.PushGroup("Map Data");

	bank.PushGroup("Inflate ranges");
	{
		PARAM_inflateimaphigh.Get(ms_fInflateRadius_High);
		PARAM_inflateimapmedium.Get(ms_fInflateRadius_Medium);
		PARAM_inflateimaplow.Get(ms_fInflateRadius_Low);

		bank.AddSlider("Inflate stream dist - Low", &ms_fInflateRadius_Low, 0.0f, 1000.0f, 1.0f);
		bank.AddSlider("Inflate stream dist - Medium", &ms_fInflateRadius_Medium, 0.0f, 1000.0f, 1.0f);
		bank.AddSlider("Inflate stream dist - High", &ms_fInflateRadius_High, 0.0f, 1000.0f, 1.0f);
	}
	bank.PopGroup();

	bank.AddToggle("Suppress HD requests when moving very fast", &ms_bNoHdWhenFlyingFast);

	fwMapDataStore::GetStore().GetBoxStreamer().InitWidgets(bank, "fwMapDataStore");
	bank.AddToggle("Map Data rqd/loaded stats", &ms_bShowLoadedStats);
	bank.AddToggle("Map Data entity counts", &ms_bShowEntityCounts);
	bank.AddToggle("Map Data matrix counts", &ms_bShowMatrixUse);
	bank.AddToggle("Map Data dependencies", &ms_bShowDependencies);
	bank.AddButton("Dump loaded imap list to TTY", PrintLoadedList);
	bank.AddButton("Dump active groups to TTY", PrintActiveGroups);
	bank.AddButton("Get active groups", GetActiveGroups);
	bank.AddButton("Set active groups", SetActiveGroups);

	bank.AddButton("Analyze grass batches", AnalyseBatchData);
	bank.AddButton("Check for uncached imaps", AnalyseCachedImaps);

	ms_slotIndices.Reset();
	ms_apszDebugImapNames.Reset();
	m_imapGroupSlotIndices.Reset();
	ms_apszImapGroupNames.Reset();

	// populate IMAP list
	for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
	{	
		fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid())
		{
			ms_slotIndices.PushAndGrow(i);
		}
	}
	ms_slotIndices.QSort(0, -1, CompareNameCB);

	for(s32 i=0; i<ms_slotIndices.size(); i++)
	{	
		strLocalIndex imapIndex = strLocalIndex(ms_slotIndices[i]);
		fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(imapIndex);
		Assert(pDef);
		ms_apszDebugImapNames.PushAndGrow(pDef->m_name.GetCStr());
	}

	// populate IPL group list
	for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
	{	
		if (IsImapGroupSlot(i))
		{
			m_imapGroupSlotIndices.PushAndGrow(i);
		}
	}
	m_imapGroupSlotIndices.QSort(0, -1, CompareNameCB);
	m_imapGroupSlotIndices.PushAndGrow(-1);

	for(s32 i=0; i<m_imapGroupSlotIndices.size(); i++)
	{	
		strLocalIndex imapIndex = strLocalIndex(m_imapGroupSlotIndices[i]);
		if (imapIndex != -1)
		{
			fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(imapIndex);
			Assert(pDef);
			ms_apszImapGroupNames.PushAndGrow(pDef->m_name.GetCStr());
		}
		else
		{
			ms_apszImapGroupNames.PushAndGrow("NULL");
		}
	}

	bank.AddCombo("IMAP", &ms_selectedImapComboIndex, ms_apszDebugImapNames.GetCount(), &ms_apszDebugImapNames[0], UpdateDebugForSelectedImapCB);
	bank.AddToggle("Streamable?", &ms_bStreamable, ToggleStreamableCB);
	bank.AddButton("Load Now", LoadCB);
	bank.AddButton("Unload Now", UnloadCB);

	bank.AddSeparator();
	bank.AddToggle("Validate bounds (streaming)", &ms_bValidateStrBounds);
	bank.AddToggle("Validate bounds (entities)", &ms_bValidatePhyBounds);
	bank.AddToggle("Only display bad bounds", &ms_bShowBadBoundsOnly);
	bank.AddSeparator();

	if (m_imapGroupSlotIndices.size() > 0)
	{
		bank.AddCombo("IMAP Group", &ms_selectedImapGroupComboIndex, ms_apszImapGroupNames.GetCount(), &ms_apszImapGroupNames[0], OnChangeImapGroup);
		bank.AddButton("Request IMAP Group", RequestImapGroupCB);
		bank.AddButton("Remove IMAP Group", RemoveImapGroupCB);
		bank.AddButton("Remove and request IMAP Group", RemoveAndRequestImapGroupCB);
		static char extentsText[128] = { 0 };
		ms_pExtentsText = bank.AddText("Extents", extentsText, 128, true);
		bank.AddSeparator();

		bank.AddTitle("IMAP Group Transition");
		bank.AddCombo("Transition Start", &ms_imapGroupTransitionStart, ms_apszImapGroupNames.GetCount(), &ms_apszImapGroupNames[0]);
		bank.AddCombo("Transition End", &ms_imapGroupTransitionEnd, ms_apszImapGroupNames.GetCount(), &ms_apszImapGroupNames[0]);
		bank.AddToggle("Automatic?", &ms_bAutomaticTransition);
		bank.AddToggle("Show active transitions", &ms_bShowActiveTransitions);
		bank.AddButton("Transition Now", SwapImapGroupCB);
		bank.AddButton("Transition Back", SwapBackImapGroupCB);
		bank.AddSlider("Index", &ms_transitionIndex, 0, fwImapGroupMgr::MAX_CONCURRENT_SWAPS-1, 1);
		bank.AddButton("End Transition At Index", ManualEndImapGroupTransitionCB);
		bank.AddButton("Abandon Transition At Index", ManualAbandonImapGroupTransitionCB);
		bank.AddButton("Remove Start State At Index", ManualRemoveStartImapGroupTransitionCB);
	}
	bank.AddSeparator();

	bank.AddButton("Output list of out of data ZIPS", &sOutputList, "Run 'tools\\script\\art\\data_regenerate_map_metadata_list.bat'");
	bank.PopGroup();

	if (ms_apszDebugImapNames.size()>0)
	{
		UpdateDebugForSelectedImapCB();
	}
	
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Update
// PURPOSE:		any debug processing for map data store
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::Update()
{
	if (ms_bShowLoadedStats)
	{
		UpdateDebugStats();

		grcDebugDraw::AddDebugOutput( "ASSET_MAPDATA_HIGH: %d loaded / %d required", ms_dbgNumLoaded[fwBoxStreamerAsset::ASSET_MAPDATA_HIGH], ms_dbgNumRequired[fwBoxStreamerAsset::ASSET_MAPDATA_HIGH] );
		grcDebugDraw::AddDebugOutput( "ASSET_MAPDATA_HIGH_CRITICAL: %d loaded / %d required", ms_dbgNumLoaded[fwBoxStreamerAsset::ASSET_MAPDATA_HIGH_CRITICAL], ms_dbgNumRequired[fwBoxStreamerAsset::ASSET_MAPDATA_HIGH_CRITICAL] );
		grcDebugDraw::AddDebugOutput( "ASSET_MAPDATA_MEDIUM: %d loaded / %d required", ms_dbgNumLoaded[fwBoxStreamerAsset::ASSET_MAPDATA_MEDIUM], ms_dbgNumRequired[fwBoxStreamerAsset::ASSET_MAPDATA_MEDIUM] );
		grcDebugDraw::AddDebugOutput( "ASSET_MAPDATA_LOW: %d loaded / %d required", ms_dbgNumLoaded[fwBoxStreamerAsset::ASSET_MAPDATA_LOW], ms_dbgNumRequired[fwBoxStreamerAsset::ASSET_MAPDATA_LOW] );
	}

	if (ms_bSuppressedSelected)
	{
		for (s32 i=0; i<ms_suppressedImaps.GetCount(); i++)
		{
			strLocalIndex slotIndex = strLocalIndex(ms_suppressedImaps[i]);
			if (slotIndex != -1)
			{
				fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(slotIndex);
				grcDebugDraw::AddDebugOutputEx(false, "Suppressing: %s", pDef->m_name.GetCStr());
			}
		}
	}

	if (ms_bShowEntityCounts)
	{
		for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
		{	
			fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
			if(pDef && pDef->GetIsValid() && pDef->IsLoaded() && pDef->GetNumEntities()>0)
			{
				grcDebugDraw::AddDebugOutputEx(false, "%s has %d entities", pDef->m_name.GetCStr(), pDef->GetNumEntities());
			}
		}
	}

	if (ms_bShowMatrixUse)
	{
		for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
		{	
			fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
			if(pDef && pDef->GetIsValid() && pDef->IsLoaded() && pDef->GetNumEntities()>0)
			{
				u32 numMatrices = 0;
				u32 numEntities = pDef->GetNumEntities();
				fwEntity** entities = pDef->GetEntities();
				for (u32 ent=0; ent<numEntities; ent++)
				{
					fwEntity* pEntity = entities[ent];
					if (pEntity && pEntity->GetTransform().IsMatrix())
					{
						numMatrices++;
					}
				}
				grcDebugDraw::AddDebugOutputEx(false, "%s has %d matrices", pDef->m_name.GetCStr(), numMatrices);
			}
		}
	}

	if (ms_bShowDependencies)
	{
		// do files with dependencies first
		for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
		{	
			fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
			if(pDef && pDef->GetIsValid() && pDef->IsLoaded() && pDef->GetNumEntities()>0)
			{
				u32 numDependencies = pDef->GetNumParentTypes();
				if (numDependencies > 0)
				{
					grcDebugDraw::AddDebugOutputEx(false, "(%04d)  %s : has %d dependencies", i,  pDef->m_name.GetCStr(), numDependencies);
					for(u32 j = 0; j<numDependencies; j++){
						fwMapTypesDef* pTypeDef = g_MapTypesStore.GetSlot(strLocalIndex(pDef->GetParentTypeIndex(j)));
						grcDebugDraw::AddDebugOutputEx(false, "    (%04d)   %s", pDef->GetParentTypeIndex(j), pTypeDef->m_name.GetCStr());
					}
				}
			}
		}

		grcDebugDraw::AddDebugOutputEx(false, "---");

		// then show files with no dependencies
		for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
		{	
			fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
			if(pDef && pDef->GetIsValid() && pDef->IsLoaded() && pDef->GetNumEntities()>0)
			{
				u32 numDependencies = pDef->GetNumParentTypes();
				if (numDependencies == 0)
				{
					grcDebugDraw::AddDebugOutputEx(false, "(%04d)  %s : has %d dependencies", i,  pDef->m_name.GetCStr(), numDependencies);
				}
			}
		}
	}

	if (ms_bValidateStrBounds || ms_bValidatePhyBounds)
	{
		ValidateBounds();
	}

	if (ms_bShowActiveTransitions)
	{
		INSTANCE_STORE.GetGroupSwapper().DisplayActiveTransitions();
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetBlockIndex
// PURPOSE:		returns the debug block index associated with a mapdata slot, if one exists
//////////////////////////////////////////////////////////////////////////
s32 fwMapDataDebug::GetBlockIndex(s32 mapDataSlot)
{
	s32 blockIndex = -1;
	if (mapDataSlot!=-1)
	{
		fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(mapDataSlot));
		if (pDef)
		{
			blockIndex = pDef->GetBlockIndex();
			if (blockIndex==-1 && pDef->GetParentDef())
			{
				blockIndex = pDef->GetParentDef()->GetBlockIndex();
			}
		}
	}
	return blockIndex; 
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	PrintLoadedList
// PURPOSE:		display a list of all IPLs currently loaded
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::PrintLoadedList()
{
	fwMapDataStore& mapDataStore = fwMapDataStore::GetStore();

	for (s32 i=0; i<mapDataStore.GetCount(); i++)
	{
		fwMapDataDef* pDef = mapDataStore.GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid() && pDef->IsLoaded())
		{
			Displayf("%4d - %s (cull:%s pin:%s ign:%s",
				i, pDef->m_name.GetCStr(),
				( mapDataStore.GetBoxStreamer().GetIsCulled(i) ? "Y" : "N" ),
				( mapDataStore.GetBoxStreamer().GetIsPinned(i) ? "Y" : "N" ),
				( mapDataStore.GetBoxStreamer().GetIsIgnored(i) ? "Y" : "N" )
			);
		}
	}
}

void fwMapDataDebug::PrintActiveGroups()
{
	atArray<u32> enabledList;
	fwMapDataStore::GetStore().GetEnabledIplGroups(enabledList);

	for (s32 i=0; i<enabledList.GetCount(); i++)
	{
		u32 slotHash = enabledList[i];
		strLocalIndex slotIndex = fwMapDataStore::GetStore().FindSlotFromHashKey(slotHash); 
		fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(slotIndex);
		if (pDef && pDef->GetIsValid())
		{
			Displayf("Enabled IPL group: %s", pDef->m_name.GetCStr() );
		}
	}
}

static atArray<u32> s_enabledIplGroups;

void fwMapDataDebug::GetActiveGroups()
{
	s_enabledIplGroups.Reset();
	fwMapDataStore::GetStore().GetEnabledIplGroups(s_enabledIplGroups);
}

void fwMapDataDebug::SetActiveGroups()
{
	fwMapDataStore::GetStore().SetEnabledIplGroups(s_enabledIplGroups);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SuppressSelected
// PURPOSE:		suppress selected imap files
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::SuppressSelected(atArray<s32>& mapDataSlots)
{
	if (ms_bSuppressedSelected)
	{
		// undo suppress
		ms_bSuppressedSelected = false;

		for (s32 i=0; i<ms_suppressedImaps.GetCount(); i++)
		{
			s32 slotIndex = ms_suppressedImaps[i];
			if (slotIndex != -1)
			{
				fwMapDataStore::GetStore().SetStreamable(strLocalIndex(slotIndex), true);
			}
		}
	}
	else
	{
		if (mapDataSlots.GetCount() == 0)
		{
			return;
		}
		// do suppress
		ms_suppressedImaps.Reset();
		ms_suppressedImaps.Assume(mapDataSlots);

		for (s32 i=0; i<ms_suppressedImaps.GetCount(); i++)
		{
			strLocalIndex slotIndex = strLocalIndex(ms_suppressedImaps[i]);
			if (slotIndex != -1)
			{
				if (fwMapDataStore::GetStore().HasObjectLoaded(slotIndex))
				{
					// request removal
					fwMapDataStore::GetStore().ClearRequiredFlag(slotIndex.Get(), STRFLAG_DONTDELETE);
					fwMapDataStore::GetStore().SafeRemove(slotIndex);
				}
				fwMapDataStore::GetStore().SetStreamable(slotIndex, false);
			}
		}

		ms_bSuppressedSelected = true;
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ToggleStreamable
// PURPOSE:		flips an imaps's streamable flag
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::ToggleStreamableCB()
{
	if (ms_debugImapIndex != -1)
	{
		fwMapDataStore::GetStore().SetStreamable(strLocalIndex(ms_debugImapIndex), ms_bStreamable);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	LoadCB
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::LoadCB()
{
	if (ms_debugImapIndex != -1)
	{
		if (!fwMapDataStore::GetStore().HasObjectLoaded(strLocalIndex(ms_debugImapIndex)))
		{
			fwMapDataStore::GetStore().StreamingRequest(strLocalIndex(ms_debugImapIndex), STRFLAG_DONTDELETE);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	UnloadCB
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::UnloadCB()
{
	if (ms_debugImapIndex != -1)
	{
		if (fwMapDataStore::GetStore().HasObjectLoaded(strLocalIndex(ms_debugImapIndex)))
		{
			// make sure it is set to ignored
			if (ms_bStreamable)
			{
				ms_bStreamable = false;
				ToggleStreamableCB();
			}

			// request removal
			fwMapDataStore::GetStore().ClearRequiredFlag(ms_debugImapIndex, STRFLAG_DONTDELETE);
			fwMapDataStore::GetStore().SafeRemove(strLocalIndex(ms_debugImapIndex));
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	OnChangeImapGroup
// PURPOSE:		called when the IMAP Group combo box selection is changed
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::OnChangeImapGroup()
{
	const char* name = ms_apszImapGroupNames[ms_selectedImapGroupComboIndex];
	strLocalIndex slotIndex = strLocalIndex(fwMapDataStore::GetStore().FindSlot(name));
	if (slotIndex!=-1)
	{
		const spdAABB& bounds = fwMapDataStore::GetStore().GetPhysicalBounds(slotIndex);

		if(ms_pExtentsText)
		{
			char tmp[128];
			Vec3V vMid = Average(bounds.GetMin(), bounds.GetMax());
			sprintf(tmp, "Min: (%.1f, %.1f, %.1f) Max: (%.1f, %.1f, %.1f) Mid: (%.1f, %.1f, %.1f)",
				bounds.GetMin().GetXf(), bounds.GetMin().GetYf(), bounds.GetMin().GetZf(),
				bounds.GetMax().GetXf(), bounds.GetMax().GetYf(), bounds.GetMax().GetZf(),
				vMid.GetXf(), vMid.GetYf(), vMid.GetZf());
			ms_pExtentsText->SetString(tmp);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RequestImapGroupCB
// PURPOSE:		wraps the same code that is called by the script commands for requesting a
//				script-controlled IMAP (ie an "IMAP group")
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::RequestImapGroupCB()
{
	const char* name = ms_apszImapGroupNames[ms_selectedImapGroupComboIndex];
	strLocalIndex index = strLocalIndex(m_imapGroupSlotIndices[ms_selectedImapGroupComboIndex]);
	if (!fwMapDataStore::GetStore().GetSlot(index)->IsLoaded())
	{
		fwMapDataStore::GetStore().RequestGroup(index, atStringHash(name));
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveImapGroupCB
// PURPOSE:		wraps the same code that is called by the script commands for removing a
//				script-controlled IMAP (ie an "IMAP group")
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::RemoveImapGroupCB()
{
	const char* name = ms_apszImapGroupNames[ms_selectedImapGroupComboIndex];
	strLocalIndex index = strLocalIndex(m_imapGroupSlotIndices[ms_selectedImapGroupComboIndex]);
	if (fwMapDataStore::GetStore().GetSlot(index)->IsLoaded())
	{
		fwMapDataStore::GetStore().RemoveGroup(index, atStringHash(name));
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveAndRequestImapGroupCB
// PURPOSE:		remove and re-request selected imap group in the same frame
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::RemoveAndRequestImapGroupCB()
{
	RemoveImapGroupCB();
	RequestImapGroupCB();
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SwapImapGroupCB
// PURPOSE:		begin transition from one imap group to another
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::SwapImapGroupCB()
{
	const char* startName = ms_apszImapGroupNames[ms_imapGroupTransitionStart];
	const char* endName = ms_apszImapGroupNames[ms_imapGroupTransitionEnd];

	u32 swapIndex = 0;
	atHashString startNameHash(startName);
	atHashString endNameHash(endName);
	INSTANCE_STORE.GetGroupSwapper().CreateNewSwap(startNameHash, endNameHash, swapIndex, ms_bAutomaticTransition);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	SwapBackImapGroupCB
// PURPOSE:		begin transition from back from one imap group to another
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::SwapBackImapGroupCB()
{
	const char* startName = ms_apszImapGroupNames[ms_imapGroupTransitionStart];
	const char* endName = ms_apszImapGroupNames[ms_imapGroupTransitionEnd];
	u32 swapIndex = 0;
	atHashString startNameHash(startName);
	atHashString endNameHash(endName);
	INSTANCE_STORE.GetGroupSwapper().CreateNewSwap(endNameHash, startNameHash, swapIndex, ms_bAutomaticTransition);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ManualEndImapGroupTransitionCB
// PURPOSE:		manually jump to the end state of a non-auto imap group transition
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::ManualEndImapGroupTransitionCB()
{
	INSTANCE_STORE.GetGroupSwapper().CompleteSwap(ms_transitionIndex);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ManualAbandonImapGroupTransitionCB
// PURPOSE:		manually abandon an active non-auto imap group transition
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::ManualAbandonImapGroupTransitionCB()
{
	INSTANCE_STORE.GetGroupSwapper().AbandonSwap(ms_transitionIndex);
}

void fwMapDataDebug::ManualRemoveStartImapGroupTransitionCB()
{
	INSTANCE_STORE.GetGroupSwapper().RemoveStartState(ms_transitionIndex);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	UpdateDebugForSelectedImap
// PURPOSE:		callback to update based on newly selected imap
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::UpdateDebugForSelectedImapCB()
{
	if (ms_selectedImapComboIndex < ms_apszDebugImapNames.size())
	{
		strLocalIndex index = strLocalIndex(ms_slotIndices[ms_selectedImapComboIndex]);

		if (index != -1)
		{
			ms_debugImapIndex = index.Get();
			ms_bStreamable = fwMapDataStore::GetStore().GetIsStreamable(index);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CompareNameCB
// PURPOSE:		compare two map data defs by name, for qsort purposes
//////////////////////////////////////////////////////////////////////////
s32	fwMapDataDebug::CompareNameCB(const s32* pIndex1, const s32* pIndex2)
{
	fwMapDataDef* pDef1 = fwMapDataStore::GetStore().GetSlot(strLocalIndex(*pIndex1));
	fwMapDataDef* pDef2 = fwMapDataStore::GetStore().GetSlot(strLocalIndex(*pIndex2));
	Assert(pDef1 && pDef2);
	const char* pszName1 = pDef1->m_name.GetCStr();
	const char* pszName2 = pDef2->m_name.GetCStr();
	char achName1[256];
	char achName2[256];
	ASSET.BaseName(achName1, 32, ASSET.FileName(pszName1));
	ASSET.BaseName(achName2, 32, ASSET.FileName(pszName2));
	return stricmp(achName1, achName2);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	ValidateBounds
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::ValidateBounds()
{
	for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
	{	
		fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid() && pDef->IsLoaded())
		{
			spdAABB storedPhyBounds = fwMapDataStore::GetStore().GetPhysicalBounds(strLocalIndex(i));
			spdAABB storedStrBounds = fwMapDataStore::GetStore().GetStreamingBounds(strLocalIndex(i));

			storedStrBounds.GrowUniform( ScalarV(-50.0f) );	// remove map data pre stream distance

			spdAABB generatedPhyBounds;
			spdAABB generatedStrBounds;

			generatedPhyBounds.Invalidate();
			generatedStrBounds.Invalidate();

			fwEntity** entities = pDef->GetEntities();
			u32 numEntities = pDef->GetNumEntities();

			if (!numEntities)
			{
				continue;
			}

			for (u32 e=0; e<numEntities; e++)
			{
				fwEntity* pEntity = entities[e];
				if (pEntity)
				{
					// entity bounds
					spdAABB entityBounds;
					pEntity->GetAABB(entityBounds);
					if (entityBounds.IsValid())
					{
						if (generatedPhyBounds.IsValid())
						{
							generatedPhyBounds.GrowAABB(entityBounds);
						}
						else
						{
							generatedPhyBounds = entityBounds;
						}
					}

					// streaming bounds
					fwEntity* pBasisEntity = pEntity->GetLodData().HasLod() ? pEntity->GetLod() : pEntity;
					spdAABB basisBounds;
					pBasisEntity->GetAABB(basisBounds);
					Vector3 vPos = VEC3V_TO_VECTOR3(basisBounds.GetCenter());
					if (ms_bUsePivotsForValidation)
					{
						vPos = VEC3V_TO_VECTOR3(pBasisEntity->GetTransform().GetPosition());
					}
					const spdSphere boundsSphere(RCC_VEC3V(vPos), ScalarV((float)pEntity->GetLodDistance()));
					const spdAABB boundsAABB(boundsSphere);

					if (generatedStrBounds.IsValid())
					{
						generatedStrBounds.GrowAABB(boundsAABB);
					}
					else
					{
						generatedStrBounds = boundsAABB;
					}
				}
			}

			// print em out
			if ( ms_bValidateStrBounds )
			{
				const Vector3& vDataMin = storedStrBounds.GetMinVector3();
				const Vector3& vDataMax = storedStrBounds.GetMaxVector3();
				const Vector3& vGenMin = generatedStrBounds.GetMinVector3();
				const Vector3& vGenMax = generatedStrBounds.GetMaxVector3();

				const bool bBoundsAreAcceptable =  ( fabsf(vDataMin.x - vGenMin.x) < 0.001f ) && ( fabsf(vDataMin.y - vGenMin.y) < 0.001f ) && ( fabsf(vDataMin.z - vGenMin.z) < 0.001f );

				if (!ms_bShowBadBoundsOnly || !bBoundsAreAcceptable)
				{
					grcDebugDraw::AddDebugOutputEx(false, "%s streaming extents (data): min(%f, %f, %f) max(%f, %f, %f)",
						pDef->m_name.GetCStr(), vDataMin.x, vDataMin.y, vDataMin.z, vDataMax.x, vDataMax.y, vDataMax.z);
					grcDebugDraw::AddDebugOutputEx(false, "%s streaming extents (from code): min(%f, %f, %f) max(%f, %f, %f)",
						pDef->m_name.GetCStr(), vGenMin.x, vGenMin.y, vGenMin.z, vGenMax.x, vGenMax.y, vGenMax.z);
				}
			}
			if ( ms_bValidatePhyBounds )
			{
				const Vector3& vDataMin = storedPhyBounds.GetMinVector3();
				const Vector3& vDataMax = storedPhyBounds.GetMaxVector3();
				const Vector3& vGenMin = generatedPhyBounds.GetMinVector3();
				const Vector3& vGenMax = generatedPhyBounds.GetMaxVector3();

				const bool bBoundsAreAcceptable =  ( fabsf(vDataMin.x - vGenMin.x) < 0.001f ) && ( fabsf(vDataMin.y - vGenMin.y) < 0.001f ) && ( fabsf(vDataMin.z - vGenMin.z) < 0.001f );

				if (!ms_bShowBadBoundsOnly || !bBoundsAreAcceptable)
				{
					grcDebugDraw::AddDebugOutputEx(false, "%s entity extents (data): min(%f, %f, %f) max(%f, %f, %f)",
						pDef->m_name.GetCStr(), vDataMin.x, vDataMin.y, vDataMin.z, vDataMax.x, vDataMax.y, vDataMax.z);
					grcDebugDraw::AddDebugOutputEx(false, "%s entity extents (from code): min(%f, %f, %f) max(%f, %f, %f)",
						pDef->m_name.GetCStr(), vGenMin.x, vGenMin.y, vGenMin.z, vGenMax.x, vGenMax.y, vGenMax.z);
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	IsImapGroupSlot
// PURPOSE:		returns true if the specified slot index corresponds to an IMAP group
//////////////////////////////////////////////////////////////////////////
bool fwMapDataDebug::IsImapGroupSlot(s32 index)
{
	fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(index));
	if (pDef && pDef->GetIsValid() && fwMapDataStore::GetStore().IsScriptManaged(strLocalIndex(index)))
	{
		// script refers to IMAP groups by the HD IMAP name only
		return (pDef->GetContentFlags()&fwMapData::CONTENTFLAG_ENTITIES_HD);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	UpdateDebugStats
// PURPOSE:		update counts of loaded and required imaps
//////////////////////////////////////////////////////////////////////////
void fwMapDataDebug::UpdateDebugStats()
{
	fwMapDataStore& mapStore = fwMapDataStore::GetStore();

	for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
	{
		ms_dbgNumLoaded[i] = 0;
		ms_dbgNumRequired[i] = 0;
	}

#if 1
	// get count of all loaded imaps
	for (s32 i=0; i<mapStore.m_loadedList.GetCount(); i++)
	{
		fwMapDataLoadedNode* pLoadedNode = mapStore.m_loadedList[i];
		if (pLoadedNode)
		{
			const s32 slotIndex = (s32) pLoadedNode->GetSlotIndex();
			const u8 assetType = mapStore.m_boxStreamer.GetAssetType(slotIndex);
			ms_dbgNumLoaded[assetType] += 1;
		}
	}

	// get count of all required imaps
	fwBoxStreamerRequiredList& reqdList = mapStore.m_boxStreamer.GetRequiredList();
	atArray<s32>& rqdSlotList = reqdList.GetOldList();
	for (s32 i=0; i<rqdSlotList.GetCount(); i++)
	{
		const s32 slotIndex = rqdSlotList[i];
		const u8 assetType = mapStore.m_boxStreamer.GetAssetType(slotIndex);
		ms_dbgNumRequired[assetType] += 1;
	}

#else

	for (s32 i=0; i<mapStore.GetCount(); i++)
	{
		fwMapDataDef* pDef = mapStore.GetSlot(i);
		if (pDef && pDef->GetIsValid())
		{
			const u8 assetType = mapStore.m_boxStreamer.GetAssetType(i);

			if (pDef->IsLoaded())
			{
				ms_dbgNumLoaded[assetType] += 1;
			}
			if (mapStore.GetStreamingFlags(i) & (STR_DONTDELETE_MASK|STRFLAG_DEPENDENCY))
			{
				ms_dbgNumRequired[assetType] += 1;
			}
			
		}
	}

#endif

	for (s32 i=0; i<fwBoxStreamerAsset::ASSET_TOTAL; i++)
	{
		if (ms_dbgNumLoaded[i] > ms_dbgNumRequired[i])
		{
			ms_dbgNumRequired[i] = ms_dbgNumLoaded[i];
		}
	}
}

void fwMapDataDebug::AnalyseBatchData()
{
	bool bTestPerformed = false;

	fwMapDataStore& mapStore = fwMapDataStore::GetStore();

	u32 totalBatches = 0;
	u32 totalImaps = 0;
	u32 totalInstances = 0;
	u32 minNumBatchesPerImap = 0xffffffff;
	u32 maxNumBatchesPerImap = 0;

	u32 minInstancesPerBatch = 0xffffffff;
	u32 maxInstancesPerBatch = 0;

	float fTotalBatchVolume = 0.0f;
	float fMinBatchVolume = FLT_MAX;
	float fMaxBatchVolume = 0.0f;

	float fTotalBatchOccupancy = 0.0f;
	float fMinBatchOccupancy = FLT_MAX;
	float fMaxBatchOccupancy = 0.0f;


	for(s32 i=0; i<fwMapDataStore::GetStore().GetSize(); i++)
	{	
		fwMapDataDef* pDef = fwMapDataStore::GetStore().GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid() &&  (pDef->GetContentFlags() & fwMapData::CONTENTFLAG_INSTANCE_LIST)!=0)
		{
			
			const bool bWasAlreadyLoaded = pDef->IsLoaded();

			if (!bWasAlreadyLoaded)
			{
				mapStore.StreamingRequest( strLocalIndex(i), STRFLAG_DONTDELETE);
				strStreamingEngine::GetLoader().LoadAllRequestedObjects(false);
			}

			//////////////////////////////////////////////////////////////////////////
			// analysis
			bTestPerformed = true;
			totalImaps++;

			// number of batches
			u32 numBatches = pDef->GetNumEntities();
			u32 processedBatches = 0;

			fwEntity** entities = pDef->GetEntities();
			for (u32 j=0; j<numBatches; j++)
			{
				fwEntity* pEntity = entities[j];
				if (pEntity /*&& pEntity->GetType()==ENTITY_TYPE_GRASS_INSTANCE_LIST*/)
				{
					spdAABB aabb;
					pEntity->GetAABB(aabb);
					const Vec3V vDimensions = (aabb.GetExtent() * ScalarV(V_TWO)) ;

					// disregard broken batches
					if (vDimensions.GetZf() > 1000.0f)
					{
						continue;
					}

					processedBatches++;

					u32 numInstances = pEntity->GetNumInstances();

					totalBatches += 1;
					totalInstances += numInstances;

					if (numInstances < minInstancesPerBatch)
					{
						minInstancesPerBatch = numInstances;
					}
					if (numInstances > maxInstancesPerBatch)
					{
						maxInstancesPerBatch = numInstances;
					}


					
					const float fVolume = ( vDimensions.GetXf() * vDimensions.GetYf() * vDimensions.GetZf() );
					const float fOccupancy = ( numInstances / fVolume );

					fTotalBatchOccupancy += fOccupancy;
					fTotalBatchVolume += fVolume;

					if (fVolume < fMinBatchVolume)
					{
						fMinBatchVolume = fVolume;
					}
					if (fVolume > fMaxBatchVolume)
					{
						Displayf("New biggest batch %s (%s) (%4.2f x %4.2f x %4.2f) vol %4.2f m3",
							pDef->m_name.GetCStr(), pEntity->GetModelName(),
							vDimensions.GetXf(), vDimensions.GetYf(), vDimensions.GetZf(), fVolume );
						fMaxBatchVolume = fVolume;
					}

					if (fOccupancy < fMinBatchOccupancy)
					{
						fMinBatchOccupancy = fOccupancy;
					}
					if (fOccupancy > fMaxBatchOccupancy)
					{
						fMaxBatchOccupancy = fOccupancy;
					}


				

				}
			}

			if (processedBatches < minNumBatchesPerImap)
			{
				minNumBatchesPerImap = processedBatches;
			}
			if (processedBatches > maxNumBatchesPerImap)
			{
				maxNumBatchesPerImap = processedBatches;
			}





			//////////////////////////////////////////////////////////////////////////


			if (!bWasAlreadyLoaded)
			{
				mapStore.ClearRequiredFlag(i, STRFLAG_DONTDELETE);
				mapStore.SafeRemove( strLocalIndex(i) );
			}

		}
	}

	if (bTestPerformed)
	{
		Displayf("[GRASSBATCH] MIN num batches per IMAP = %d", minNumBatchesPerImap);
		Displayf("[GRASSBATCH] MAX num batches per IMAP = %d", maxNumBatchesPerImap);
		Displayf("[GRASSBATCH] AVG num batches per IMAP = %4.2f", ((float)totalBatches / (float)totalImaps));

		Displayf("[GRASSBATCH] MIN num instances per batch = %d", minInstancesPerBatch);
		Displayf("[GRASSBATCH] MAX num instances per batch = %d", maxInstancesPerBatch);
		Displayf("[GRASSBATCH] AVG num instances per batch = %4.2f", ((float)totalInstances / (float)totalBatches));

		Displayf("[GRASSBATCH] MIN batch volume = %4.2f m3", fMinBatchVolume);
		Displayf("[GRASSBATCH] MAX batch volume = %4.2f m3", fMaxBatchVolume);
		Displayf("[GRASSBATCH] AVG batch volume = %4.2f m3", fTotalBatchVolume / (float) totalBatches);

		Displayf("[GRASSBATCH] MIN batch occupancy = %4.8f insts per m3", fMinBatchOccupancy);
		Displayf("[GRASSBATCH] MAX batch occupancy = %4.8f insts per m3", fMaxBatchOccupancy);
		Displayf("[GRASSBATCH] AVG batch occupancy = %4.8f insts per m3", fTotalBatchOccupancy / (float) totalBatches);

	}

}

void fwMapDataDebug::AnalyseCachedImaps()
{
	Displayf("** Checking for non-cached IMAP files **");

	fwMapDataStore& mapStore = fwMapDataStore::GetStore();

	s32 numCached = 0;
	s32 numUncached = 0;
	s32 numUnknown = 0;

	for(s32 i=0; i<mapStore.GetSize(); i++)
	{	
		fwMapDataDef* pDef = mapStore.GetSlot(strLocalIndex(i));
		if (pDef && pDef->GetIsValid())
		{
			strLocalIndex localIndex(i);
			if (localIndex.IsValid())
			{
				strIndex streamingIndex = mapStore.GetStreamingIndex(localIndex);
				strStreamingInfo& info = *strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex);
				strFileHandle fileHandle = info.GetHandle();
				if (strIsValidHandle(fileHandle))
				{
					s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(fileHandle).Get();
					strStreamingFile* pImageFile = strPackfileManager::GetImageFile( (u32) imageIndex);

					if (pImageFile)
					{
						if (pImageFile->m_bLoadCompletely)
						{
							numCached++;
						}
						else
						{
							Displayf("IMAP: %s in %s is not cached in memory", pDef->m_name.GetCStr(), pImageFile->m_name.GetCStr() );
							numUncached++;
						}
					}
					else
					{
						numUnknown++;
					}
				}

			}
		}

	}

	Displayf("IMAP scan complete = numCached=%d, numUncached=%d, numUnknown=%d", numCached, numUnknown, numUnknown);

}

}	//namespace rage

#endif	//__BANK
