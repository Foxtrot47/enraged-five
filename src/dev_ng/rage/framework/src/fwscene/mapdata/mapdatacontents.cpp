/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/mapdata/mapdatacontents.cpp
// PURPOSE : contains items created by a loaded mapdata file, including entity instances etc
// AUTHOR :  Ian Kiigan
// CREATED : 27/04/2011
//
/////////////////////////////////////////////////////////////////////////////////

#include "fwscene/mapdata/mapdatacontents.h"

#include "fwscene/lod/LodAttach.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/mapdata/mapinstancedata.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/world/SceneGraphNode.h"
#include "fwscene/world/StreamedSceneGraphNode.h"
#include "fwscene/scan/ScanEntities.h"
#include "fwscene/world/worldmgr.h"
#include "streaming/streamingvisualize.h"

#include "diag/art_channel.h"
#include "parser/macros.h"
#include "parser/structure.h"

PARAM(noentitydescsoa, "Don't use SoA data structure for map entity descriptors");

namespace rage {

FW_INSTANTIATE_CLASS_POOL(fwMapDataLoadedNode, CONFIGURED_FROM_FILE, atHashString("MapDataLoadedNode",0x80740ebe));

#if RSG_PC
bool fwMapDataContents::ms_bAllowOrphanScaling = true;
bool fwMapDataContents::ms_bEnbleInstancedGrass = true;
#endif // RSG_PC

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	fwMapDataContents
// PURPOSE:		ctor
//////////////////////////////////////////////////////////////////////////
fwMapDataContents::fwMapDataContents() :
	m_entities(NULL),
	m_numEntities(0),
	m_sceneNode(NULL),
	m_pLoadedNode(NULL),
	m_pMapData(NULL)
#if __BANK
	, m_guidArray(NULL)
#endif	//__BANK
{
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	~fwMapDataContents
// PURPOSE:		dtor
//////////////////////////////////////////////////////////////////////////
fwMapDataContents::~fwMapDataContents()
{
	Assert(!m_sceneNode);
	Assert(!m_numEntities);
	Assertf(!m_loaderInst.IsLoaded(), "Call Unload before deleting this");
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CreateEntityArray
// PURPOSE:		creates an array of entity pointers, for storing IPL instances
//////////////////////////////////////////////////////////////////////////
void fwMapDataContents::CreateEntityArray(u32 numEntities)
{
	Assert(!m_entities);
	SetEntities(rage_aligned_new(16) fwEntity*[numEntities]);
	memset(GetEntities(), 0, sizeof(fwEntity*)*numEntities);
	SetNumEntities(numEntities);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Entities_Create
// PURPOSE:		create all entities for specified mapdata (inc lod attachment)
//////////////////////////////////////////////////////////////////////////
void fwMapDataContents::Entities_Create(fwMapData* pMapData, fwMapDataContents* pParentContents, s32 mapDataSlotIndex)
{
	Assertf(!GetNumEntities(), "fwMapData: trying to construct entities, but they already exist");

	fwEntity* pEntity;
	const int entityCount = pMapData->m_entities.GetCount();
#if RSG_PC
	const int entityBatchCount = ms_bEnbleInstancedGrass ? pMapData->m_instancedData.m_PropInstanceList.GetCount() : 0;
	const int grassBatchCount = ms_bEnbleInstancedGrass ? pMapData->m_instancedData.m_GrassInstanceList.GetCount() : 0;
#else
	const int entityBatchCount = pMapData->m_instancedData.m_PropInstanceList.GetCount();
	const int grassBatchCount = pMapData->m_instancedData.m_GrassInstanceList.GetCount();
#endif
	const int totalEntityCount = entityCount + entityBatchCount + grassBatchCount;

	const bool bAllowOrphanScaling = fwMapDataContents::GetAllowOrphanScaling() && 
									(	(pMapData->GetContentFlags() & fwMapData::CONTENTFLAG_ENTITIES_HD)!=0
										&& (pMapData->GetContentFlags() & (fwMapData::CONTENTFLAG_MLO | fwMapData::CONTENTFLAG_INSTANCE_LIST))==0 );

	STRVIS_REGISTER_MAPDATA(g_MapDataStore.GetStreamingIndex(strLocalIndex(mapDataSlotIndex)), *pMapData);
	STRVIS_INSTANTIATE_MAPDATA(*pMapData);

	if (totalEntityCount)
	{
		CreateEntityArray(totalEntityCount);

		fwMapDataDef* pMapDataDef = g_MapDataStore.GetSlot(strLocalIndex(mapDataSlotIndex));
		Assert(pMapDataDef);
		bool	bEntityLODDistRequiresPatching = pMapDataDef->GetIsMLOInstanceDef();

#if __BANK && !__RESOURCECOMPILER
		{
			// Debug Heap
			sysMemAutoUseDebugMemory debug;

			// create debug only guid array (non final builds)
			m_guidArray = rage_new u32[totalEntityCount];
			Assert(m_guidArray != NULL);
		}		
#endif	//__BANK && !__RESOURCECOMPILER

		s32 i;
		for (i=0; i<entityCount; i++)
		{
			fwEntityDef* pEntityDef = pMapData->m_entities[i];
			Assert(pEntityDef != NULL);

			// JW - MP HACK - Don't remove low priority items if they are fixed
			bool bIsFixed = ((pEntityDef->m_flags & fwEntityDef::FLAG_IS_FIXED) == fwEntityDef::FLAG_IS_FIXED);
			fwModelId modelId;
			fwArchetype* pArch = fwArchetypeManager::GetArchetypeFromHashKey(pEntityDef->m_archetypeName.GetHash(), modelId);
			artAssertf(pArch, "Can't find archetype '%s' in map '%s'", pEntityDef->m_archetypeName.TryGetCStr(), pMapData->GetName().TryGetCStr());
			if (pArch)
			{
				bIsFixed = bIsFixed || (pArch->CheckIsFixed()) || pArch->WillGenerateBuilding();
			}
			// end of MP HACK

			if (pEntityDef->m_priorityLevel>fwMapData::GetEntityPriorityCap() && !bIsFixed)
			{
				bool bInterior = pEntityDef->parser_GetStructure()->GetNameHash() == atLiteralStringHash("CMloInstanceDef");
				Assertf(!bInterior,"Interiors can't be priority stripped : %s in %s",pEntityDef->m_archetypeName.GetCStr(), pMapData->GetName().GetCStr());

				if (!bInterior)
				{
					continue; // don't add entities to world with priority lower than the current cap
				}
			}

			// create entity
			pEntity = fwMapData::ConstructEntity(pEntityDef, mapDataSlotIndex, pArch, modelId);
			SetEntityAtIndex(pEntity, i);

#if __BANK
			if(pEntity)
			{
				pEntity->SetDebugPriority( (int)pEntityDef->m_priorityLevel, bIsFixed );		// MP appears to set PRI_REQUIRED
			}
#endif	//__BANK


#if __BANK && !__RESOURCECOMPILER
			m_guidArray[i] = pEntityDef->m_guid;
#endif	//__BANK

			// LOD attachment
			if (pEntity)
			{
				fwEntity* lodEntity = NULL;
				if (pEntityDef->m_parentIndex != -1)
				{

					fwEntity** parentEntities = NULL;
					s32 numParentEntities = 0;
					if (pEntityDef->m_flags & fwEntityDef::FLAG_LOD_IS_IN_PARENT_MAPDATA)
					{
						if (Verifyf(pParentContents, "Can't attach %s (index %d in %s) to a LOD in a parent IMAP that isn't loaded. Tools bug",
							pEntityDef->m_archetypeName.GetCStr(), i, pMapData->GetName().GetCStr()))
						{
							parentEntities = pParentContents->GetEntities();
							numParentEntities = pParentContents->GetNumEntities();
						}
					}
					else
					{
						if (Verifyf(pEntityDef->m_parentIndex < i, "Can't attach %s (index %d in %s) to LOD in same IMAP at index %d. Tools bug",
							pEntityDef->m_archetypeName.GetCStr(), i, pMapData->GetName().GetCStr(), pEntityDef->m_parentIndex))
						{
							parentEntities = GetEntities();
							numParentEntities = (s32) GetNumEntities();
						}
					}

					bool bSafeToAttach = parentEntities!=NULL && pEntityDef->m_parentIndex<numParentEntities;
					if (Verifyf(bSafeToAttach, "fwMapData: trying to attach %s to lod index %d failed (parentEntities=%p, numParentEntities=%d)",
						pEntityDef->m_archetypeName.GetCStr(), pEntityDef->m_parentIndex, parentEntities, numParentEntities))
					{
						lodEntity = parentEntities[pEntityDef->m_parentIndex];
					}
					else
					{
						lodEntity = NULL;
					}
				}

				bool bIsContainerLod =
				(
					pEntityDef->m_lodLevel==LODTYPES_DEPTH_SLOD2 ||
					pEntityDef->m_lodLevel==LODTYPES_DEPTH_SLOD3 ||
					pEntityDef->m_lodLevel==LODTYPES_DEPTH_SLOD4
				);

				float entityDefLodDist = pEntityDef->m_lodDist;
				if (bEntityLODDistRequiresPatching)
				{
					pEntity->PatchUpLODDistances(lodEntity);
					entityDefLodDist =  Min(entityDefLodDist, ((float)pEntity->GetLodDistance()));

					// intercept this interior -  v_dockcontrol - and ensure that LOD distances are pushed out for it
					// url:bugstar:3545455
					if (lodEntity && (lodEntity->GetModelNameHash() == atHashString("PO1_07_milo_lod",0xc21b58c7)))
					{
						entityDefLodDist = 50.0f;
						pEntity->SetLodDistance(50);
						lodEntity->GetLodData().SetChildLodDistance(50);	
					}
				}

				fwLodAttach::AddEntityToLodHierarchy(pEntity, entityDefLodDist, pEntityDef->m_childLodDist, pEntityDef->m_numChildren, lodEntity, bIsContainerLod, bAllowOrphanScaling);

#if __ASSERT && 0
				// lod attachment is done. can now validate that any LODs are packed into drawable dictionaries.
				// this is to detect very common tools / content tree / asset combine faults that produce loose drawables for LODs
				// rather than drawable dictionaries, which has a big impact on streaming etc
				fwLodData& lodData = pEntity->GetLodData();
				if (lodData.HasChildren() && lodData.HasLod() && pArch->GetDrawableType()!=fwArchetype::DT_DRAWABLEDICTIONARY)
				{
					fwEntity* pParent = (fwEntity*) lodData.GetLod();
					const bool bHasSiblings = ( pParent->GetLodData().GetNumChildren() > 1 );
					Assertf(!bHasSiblings, "%s asset fault: %s (in %s) should be packed into a drawable dictionary, but it isn't. Please bug for Tools",
						pEntity->GetLodData().GetLodTypeName(), pEntity->GetModelName(), pMapData->GetName().GetCStr() );
				}
#endif
			}
		}

#if RSG_PC
		if(ms_bEnbleInstancedGrass)
#endif
		{
			atArray<fwPropInstanceListDef>::const_iterator end = pMapData->m_instancedData.m_PropInstanceList.end();
			atArray<fwPropInstanceListDef>::const_iterator iter;
			for(iter = pMapData->m_instancedData.m_PropInstanceList.begin(); iter != end; ++iter)
			{
				//No guids for now...
#if __BANK && !__RESOURCECOMPILER
				m_guidArray[i] = 0;
#endif	//__BANK
				// create entity
				const fwPropInstanceListDef *pEntityDef = &(*iter);
				pEntity = fwMapData::ConstructEntity(pEntityDef, mapDataSlotIndex);
				SetEntityAtIndex(pEntity, i++);
			}
		}

#if RSG_PC
		if(ms_bEnbleInstancedGrass)
#endif
		{
			atArray<fwGrassInstanceListDef>::iterator end = pMapData->m_instancedData.m_GrassInstanceList.end();
			atArray<fwGrassInstanceListDef>::iterator iter;
			for(iter = pMapData->m_instancedData.m_GrassInstanceList.begin(); iter != end; ++iter)
			{
				//No guids for now...
#if __BANK && !__RESOURCECOMPILER
				m_guidArray[i] = 0;
#endif	//__BANK
				// create entity
				pEntity = fwMapData::ConstructEntity(&(*iter), mapDataSlotIndex);
				SetEntityAtIndex(pEntity, i++);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Entities_Destroy
// PURPOSE:		destroys all entities in array
//////////////////////////////////////////////////////////////////////////
void fwMapDataContents::Entities_Destroy()
{
	if (m_entities)
	{
#if __BANK && !__RESOURCECOMPILER
		// destroy guid array (non-final builds only)
		if (m_guidArray)
		{
			// Debug Heap
			sysMemAutoUseDebugMemory debug;
			delete [] m_guidArray;
			m_guidArray = NULL;
		}
#endif	//__BANK

		// run over entities backwards, as they are in order of lod attachment (parent before children)
		for(s32 i=m_numEntities-1; i>=0; i--)
		{
			if (m_entities[i])
			{
				delete m_entities[i];
			}
		}
		delete[] m_entities;
		m_entities = NULL;
		SetNumEntities(0);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Entities_AddToWorld
// PURPOSE:		adds the entity array to world representation
//////////////////////////////////////////////////////////////////////////
void fwMapDataContents::Entities_AddToWorld(fwMapData* pMapData, bool bPostponed, bool bCulled)
{
	Assert(pMapData);

	if (m_numEntities)
	{
		spdAABB aabb;
		pMapData->GetPhysicalExtents(aabb);

		bool bContainsHighDetailEntities = false;
		bContainsHighDetailEntities = pMapData->GetContentFlags() & fwMapData::CONTENTFLAG_ENTITIES_HD;

		m_sceneNode = rage_new fwStreamedSceneGraphNode(bContainsHighDetailEntities, bCulled);
		m_sceneNode->SetBoundingBox(aabb);

		if ( PARAM_noentitydescsoa.Get() )
		{
			fwEntityContainer* pEntityContainer = rage_new fwEntityContainer( m_sceneNode );
			m_sceneNode->AddStreamedContainer( pEntityContainer );
			Assert(pEntityContainer);
			pEntityContainer->Preallocate((u16) m_numEntities);

			Assertf(pEntityContainer->GetStorageSize()==(size_t)m_numEntities,
				"Loading mapdata %s tried to preallocate an entity container of size %d, but got size %d",
				pMapData->GetName().GetCStr(),
				m_numEntities,
				pEntityContainer->GetStorageSize());

			for (u32 i=0; i<m_numEntities; i++)
			{
				fwEntity* pEntity = m_entities[i];
				if (pEntity)
				{
					pEntityContainer->AppendEntity(pEntity);
				}
			}
		}
		else
		{
			int validEntitiesCount = 0;
			for (u32 i=0; i<m_numEntities; i++)
			{
				fwEntity* pEntity = m_entities[i];
				if (pEntity)
				{
					++validEntitiesCount;
				}
			}
			
			int fullContainerCount = validEntitiesCount / SCAN_MAX_ENTITIES_PER_JOB;
			int	entityOffset = 0;
			for (int i = 0; i < fullContainerCount; ++i)
			{
				int processedCount = 0;
				fwSoAEntityContainer *pEntityContainer = rage_new fwSoAEntityContainer( m_sceneNode, 
																						m_entities + entityOffset, 
																						SCAN_MAX_ENTITIES_PER_JOB, 
																						m_numEntities - entityOffset, 
																						processedCount);
				m_sceneNode->AddStreamedContainer( pEntityContainer );
				entityOffset += processedCount;
			}

			int numberValidEntitiesAdded = fullContainerCount * SCAN_MAX_ENTITIES_PER_JOB;
			if (numberValidEntitiesAdded < validEntitiesCount)
			{
				int processedCount = 0;
				fwSoAEntityContainer *pEntityContainer = rage_new fwSoAEntityContainer( m_sceneNode, 
																						m_entities + entityOffset, 
																						validEntitiesCount - numberValidEntitiesAdded, 
																						m_numEntities - entityOffset, 
																						processedCount);
				Assert(pEntityContainer != NULL);
				m_sceneNode->AddStreamedContainer( pEntityContainer );
				ASSERT_ONLY(entityOffset += processedCount;)
			}

#if __ASSERT
			int roundedTotalEntitiesInContainer = 0;
			for (fwBaseEntityContainer* pContainer = m_sceneNode->GetFirstContainer(); pContainer; pContainer = pContainer->GetNext())
			{
				roundedTotalEntitiesInContainer += pContainer->GetEntityCount();
			}
			Assertf(roundedTotalEntitiesInContainer == ((validEntitiesCount + 3) & ~3), "Not all valid entities added");
#endif

		}

		if (bPostponed)
		{
			//////////////////////////////////////////////////////////////////////////
			// make sure entities are not available for dummy conversion etc
			for (u32 i=0; i<m_numEntities; i++)
			{
				fwEntity* pEntity = m_entities[i];
				if (pEntity) { pEntity->ClearBaseFlag(fwEntity::IS_VISIBLE); }
			}
			//////////////////////////////////////////////////////////////////////////
		}
		else
		{
			gWorldMgr.AddSceneNodeToWorld(m_sceneNode);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Entities_RemoveFromWorld
// PURPOSE:		removes entity array from world representation
//////////////////////////////////////////////////////////////////////////
void fwMapDataContents::Entities_RemoveFromWorld()
{
	if (m_sceneNode)
	{
		fwBaseEntityContainer* streamedContainer = m_sceneNode->GetFirstContainer();
		while (streamedContainer)
		{
			streamedContainer->RemoveAllEntities();
			streamedContainer = streamedContainer->GetNext();
		}
		m_sceneNode->DestroyAllStreamedContainers();

		if (m_sceneNode->GetIsInWorld())
		{
			gWorldMgr.RemoveSceneNodeFromWorld(m_sceneNode);
		}

		delete m_sceneNode;
		m_sceneNode = NULL;
	}
}

#if __BANK

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	GetGuid
// PURPOSE:		debug only guid retrieval. slow.
//////////////////////////////////////////////////////////////////////////
bool fwMapDataContents::GetGuid(fwEntity* pEntity, u32& guid)
{
	if (m_guidArray)
	{
		for (u32 i=0; i<m_numEntities; i++)
		{
			if (pEntity && pEntity==m_entities[i])
			{
				guid = m_guidArray[i];
				return true;
			}
		}
	}
	return false;
}

#endif	//__BANK

}	//namespace rage
