//
// fwscene/mapdata/mapinstancedata.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "mapinstancedata.h"
#include "mapinstancedata_parser.h"
#include <algorithm>

#include "fwscene/stores/psostore.h"
#include "grcore/instancebuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/fvf.h"
#include "vectormath/mat34v.h"

#if !__RESOURCECOMPILER && __ASSERT
#include "entity/archetype.h"
#endif

#if __RESOURCECOMPILER
#include "grcore/vertexbuffer.h"
#endif

namespace rage {

namespace InstListHelper
{
#if !__RESOURCECOMPILER && __ASSERT
	void ComputeBatchAABB(const fwPropInstanceListDef::InstanceDataList &list, const spdAABB &modelAABB, spdAABB &batchAABB)
	{
		batchAABB.Invalidate();

		fwPropInstanceListDef::InstanceDataList::const_iterator end = list.end();
		fwPropInstanceListDef::InstanceDataList::const_iterator iter;
		for(iter = list.begin(); iter != end; ++iter)
		{
			Vec3V position(iter->m_InstMat[0].GetW(), iter->m_InstMat[1].GetW(), iter->m_InstMat[2].GetW());
			Mat34V mat(iter->m_InstMat[0].GetXYZ(), iter->m_InstMat[1].GetXYZ(), iter->m_InstMat[2].GetXYZ(), position);
			spdAABB instAABB(modelAABB);
			instAABB.Transform(mat);
			batchAABB.GrowAABB(instAABB);
		}
	}

	void VerifyBatchAABB(const fwPropInstanceListDef::InstanceDataList &list, const atHashString &archetypeName, const spdAABB &batchAABB)
	{
		u32 index;
		if(fwArchetype *archetype = fwArchetypeManager::GetArchetypeFromHashKey(archetypeName.GetHash(), &index))
		{
			//Compute batch AABB to compare with passed in AABB
			spdAABB computedBatchAABB;
			InstListHelper::ComputeBatchAABB(list, archetype->GetBoundingBox(), computedBatchAABB);

			//Compare them.
			Assert(computedBatchAABB == batchAABB);	//This will probably always hit b/c of precision issues. Should check w/ epsilon.
		}
	}
#endif //!__RESOURCECOMPILER && __ASSERT
}

//Statics
const u32 fwPropInstanceListDef::InstanceData::sNumInstanceBufferRegisters = 5;

fwPropInstanceListDef::InstanceData::InstanceData()
{
}

void fwPropInstanceListDef::InstanceData::WriteInstanceData(grcVecArrayInstanceBufferList &list) const
{
	if(m_IsVisible)
	{
		Vector4 *ib = list.Append();	//Get pointer to instance buffer.

		if(Verifyf(ib != NULL, "WARNING! Unable to write instance buffer data - instance buffer allocation failed! There will be missing instances."))
		{
			WriteInstanceData(reinterpret_cast<Vec4V *>(ib));
		}
	}
}

void fwPropInstanceListDef::InstanceData::WriteInstanceData(Vec4V *ib) const
{
	ib[0] = m_InstMat[0];
	ib[1] = m_InstMat[1];
	ib[2] = m_InstMat[2];
	ib[3] = m_Globals;
	ib[4] = m_Tint.GetRGBA();
}

void fwPropInstanceListDef::InstanceData::GetMatrixCopy(Mat34V_InOut mat) const
{
	Vec3V position(m_InstMat[0].GetW(), m_InstMat[1].GetW(), m_InstMat[2].GetW());
	mat.SetCols(m_InstMat[0].GetXYZ(), m_InstMat[1].GetXYZ(), m_InstMat[2].GetXYZ(), position);
}

Vec3V_Out fwPropInstanceListDef::InstanceData::GetPosition() const
{
	return Vec3V(m_InstMat[0].GetW(), m_InstMat[1].GetW(), m_InstMat[2].GetW());
}

fwPropInstanceListDef::~fwPropInstanceListDef()
{
	ReleaseDeviceResources();
}

void fwPropInstanceListDef::CreateDeviceResources() const
{
	m_IbList = rage_new IBList(m_InstanceList.size(), InstanceData::sNumInstanceBufferRegisters);
}

void fwPropInstanceListDef::ReleaseDeviceResources() const
{
	delete m_IbList;
	m_IbList = NULL;
}

grcInstanceBuffer *fwPropInstanceListDef::CreateInstanceBuffer() const
{
	grcInstanceBufferBasic *first = NULL;
	if(Verifyf(m_IbList, "ERROR: Static instance buffer list should not be NULL! Did you call CreateDeviceResources()?"))
	{
		grcStaticInstanceBufferList &list = m_IbList->GetCurrentList();
		first = static_cast<grcInstanceBufferBasic *>(list.GetFirst());
		grcInstanceBufferBasic *ib = first;
		InstanceDataList::const_iterator end = m_InstanceList.end();
		InstanceDataList::const_iterator iter = m_InstanceList.begin();
		while(ib && iter != end)
		{
			u32 count = 0;
			Vec4V *buff = reinterpret_cast<Vec4V *>(ib->Lock());
			for(u32 i = 0; i < list.GetMaxCountPerBatch() && iter != end; ++i, ++iter)
			{
				if(iter->m_IsVisible)
				{
					iter->WriteInstanceData(buff);
					++count;
					buff += list.GetStrideQW();
				}
			}
			ib->Unlock(count, list.GetStrideQW());
			ib = static_cast<grcInstanceBufferBasic *>(ib->GetNext());
		}
	}

	return first;
}

////////////////////
// Grass
////////////////////
Vec3V_Out fwGrassInstanceListDef::InstanceData::ComputeNormal() const
{
	Vec2V normXY(static_cast<float>(m_NormalX) / 255.0f, static_cast<float>(m_NormalY) / 255.0f);
	normXY = (normXY * ScalarV(V_TWO)) - Vec2V(V_ONE);	//normXY represents [-1, 1] range!
	ScalarV normZ = Sqrt(ScalarV(V_ONE) - Dot(normXY, normXY));

	return Vec3V(normXY, normZ);
}

grcFvf fwGrassInstanceListDef::GetFVF()
{
	grcFvf fvf;
	fvf.SetBlendWeightChannel(true, grcFvf::grcdsShort2_unorm);
	fvf.SetTextureChannel(1, true, grcFvf::grcdsShort_unorm);
	fvf.SetTextureChannel(2, true, grcFvf::grcdsByte2_unorm);
	fvf.SetTextureChannel(3, true, grcFvf::grcdsColor);

	return fvf;
}

Vec3V_Out fwGrassInstanceListDef::ComputeLodInstFadeRange(ScalarV_In distToCamera) const
{
	const ScalarV lodDistEnd		= ScalarV(static_cast<float>(m_lodDist));
	const ScalarV lodDistBegin		= Min(ScalarV(m_LodFadeStartDist), lodDistEnd);
	const ScalarV lodDistRangeDiff	= lodDistEnd - lodDistBegin;
	ScalarV lodFactor = SelectFT(IsGreaterThan(lodDistRangeDiff, ScalarV(V_ZERO)), ScalarV(V_ZERO), Saturate((distToCamera - lodDistBegin) / lodDistRangeDiff));

	const ScalarV instFadeRange(m_LodInstFadeRange);
	const ScalarV numInst(static_cast<float>(m_InstanceList.size()));
	
	//Using this method, numInst * (1.0f - instFadeRange) never fade out! So we need to re-size the batch so all instances will eventually fade out by lodDistEnd.
	ScalarV adjustedNumInst = SelectFT(IsGreaterThan(instFadeRange, ScalarV(V_ZERO)), numInst, numInst / instFadeRange);
	Vec2V range = Vec2V(lodFactor) * Vec2V(adjustedNumInst * instFadeRange, adjustedNumInst);
	return Vec3V(range, numInst);
}

fwGrassInstanceListDef::~fwGrassInstanceListDef()
{
	fwPsoStoreLoadInstance* loadingInst = fwPsoStoreLoader::GetTopLevelInstance();
	if (!loadingInst || !loadingInst->IsInPlace())
	{
		// TODO: The parser did the allocation internally, if this inst was loaded from the parser. Can (should) the parser also do the matching free?
		parUtils::PhysicalFree((char*)m_InstanceList.GetElements());
		// Placement new-ing the array will wipe out the elements pointer we just freed
		rage_placement_new(&m_InstanceList) InstanceDataList(true);

		ReleaseDeviceResources();
	}
}

fwGrassInstanceListDef::device_resource_create_functor_type fwGrassInstanceListDef::sm_CreateDeviceResourcesFunc;
fwGrassInstanceListDef::device_resource_release_functor_type fwGrassInstanceListDef::sm_ReleaseDeviceResourcesFunc;
fwGrassInstanceListDef::drawable_placement_notify_functor_type fwGrassInstanceListDef::sm_DrawablePlacementNotifyFunc;
fwGrassInstanceListDef::drawable_remove_notify_functor_type fwGrassInstanceListDef::sm_DrawableRemoveNotifyFunc;

void fwGrassInstanceListDef::Init(const device_resource_create_functor_type &createFunc, const device_resource_release_functor_type &releaseFunc)
{
	//////////////////////////////////////////////////////////////////////////
	//When doing in-place loading no constructors are called for the objects which results in non of the "padding" data being inited to 
	// default values ... adding this delegate will allow us to init the data.
	if (fwGrassInstanceListDef::parser_GetStaticStructure())
	{
#if RSG_PC
		if(fwMapDataContents::GetEnableInstancedGrass())
#endif
		{
			atDelegate< void (void*) > PostPsoPlaceDelegate(fwGrassInstanceListDef::PostPsoPlace);
			fwGrassInstanceListDef::parser_GetStaticStructure()->AddDelegate("PostPsoPlace", PostPsoPlaceDelegate);
		}
	}

	sm_CreateDeviceResourcesFunc = createFunc;
	sm_ReleaseDeviceResourcesFunc = releaseFunc;

	/*FatalAssertf*/ Assertf(sm_CreateDeviceResourcesFunc && sm_ReleaseDeviceResourcesFunc, "fwGrassInstanceListDef::Init was passed invalid Functors! Device resources will not be created!");
}

void fwGrassInstanceListDef::PostPsoPlace(void* data)
{
	//ONLY PUT NON-PSO data init in here ...
	Assert(data);
	fwGrassInstanceListDef* initData = reinterpret_cast<fwGrassInstanceListDef*>(data);
	// PSO
	initData->CreateDeviceResources();
}

void fwGrassInstanceListDef::CreateDeviceResources()
{
	if(	Verifyf(sm_CreateDeviceResourcesFunc, "No valid Device Resource create functor bound! Device resources will not be created!") &&
		Verifyf(m_DeviceResources == NULL, "Device Resources have already been created! CreateDeviceResources should only be called outside of fwGrassInstanceListDef if a fixup occurred and thus the stream was never created."))
		m_DeviceResources = sm_CreateDeviceResourcesFunc(*this);
}

void fwGrassInstanceListDef::ReleaseDeviceResources() const
{
#if RSG_PC
	if(fwMapDataContents::GetEnableInstancedGrass())
#endif
	{
		if(Verifyf(sm_ReleaseDeviceResourcesFunc, "No valid Device Resource release functor bound! Device resources will not be released!"))
		{
			sm_ReleaseDeviceResourcesFunc(m_DeviceResources);
			m_DeviceResources = NULL;
		}
	}
}



void fwInstancedMapData::ReleaseDeviceResources()
{
	for (int x=m_PropInstanceList.GetCount()-1; x>=0; x--)
	{
		m_PropInstanceList[x].ReleaseDeviceResources();
	}

	for (int x=m_GrassInstanceList.GetCount()-1; x>=0; x--)
	{
		m_GrassInstanceList[x].ReleaseDeviceResources();
	}
}

} // namespace rage
