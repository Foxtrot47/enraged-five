//
// fwscene/stores/maptypesdef.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef __FWSCENE_MAPTYPESDEF_H__
#define __FWSCENE_MAPTYPESDEF_H__

#include "fwtl/assetstore.h"
#include "fwscene/mapdata/maptypescontents.h"

#define MAX_TYPE_DEPS		(8)

namespace rage {

// PURPOSE:	The definition of the map types. Holds the data we 
//			need to be able to decide when to stream the maptypes.
class fwMapTypesDef : public fwAssetNameDef<fwMapTypesContents>
{
public:

	~fwMapTypesDef()
	{
		if(m_typeDataIndicesCount > 1)
		{
			delete m_TypeDataIndex.indices;
		}
		m_typeDataIndicesCount = 0;
	}

	void	Init(const strStreamingObjectNameString name);

	inline bool				GetIsInitialised() const				{ return m_flags.m_bInitialised; }
	inline void				SetIsInitialised(bool bInitialised)		{ m_flags.m_bInitialised = bInitialised; }
	inline bool				GetIsNativeFile()						{ return m_flags.m_bIsNativeFile; }
	inline void				SetIsNativeFile(bool bNativeFile)		{ m_flags.m_bIsNativeFile = bNativeFile; }
	inline bool				GetIsPermanent() const					{ return m_flags.m_bIsPermanent; }
	inline void				SetIsPermanent(bool bIsPermanent)		{ m_flags.m_bIsPermanent = bIsPermanent; }
	inline bool				GetIsDependency() const					{ return m_flags.m_bIsDependency; }
	inline void				SetIsDependency(bool bIsDependency)		{ m_flags.m_bIsDependency = bIsDependency; }
	inline bool				GetIsPermanentDLC() const				{ return m_flags.m_bIsPermanentDLC; }
	inline void				SetIsPermanentDLC(bool bIsDLC)			{ m_flags.m_bIsPermanentDLC = bIsDLC; }
	inline bool				GetIsPso() const						{ return m_flags.m_bIsPso; }
	inline void				SetIsPso(bool bIsPso)					{ m_flags.m_bIsPso = bIsPso; }
	inline bool				GetIsFastPso() const					{ return m_flags.m_bIsFastPso; }
	inline void				SetIsFastPso(bool bIsFastPso)			{ m_flags.m_bIsFastPso = bIsFastPso; }
	inline bool				GetIsInPlace() const					{ return m_flags.m_bIsInPlace; }
	inline void				SetIsInPlace(bool bIsInPlace)			{ m_flags.m_bIsInPlace = bIsInPlace; }
	inline bool				GetIsDelayLoading() const				{ return m_flags.m_bDelayLoading; }
	inline void				SetIsDelayLoading(bool bDelayLoading)	{ m_flags.m_bDelayLoading = bDelayLoading; }
	inline void				SetIsMLOType(bool bIsMLO)				{ m_flags.m_bContainsMLOtype = bIsMLO; }
	inline bool				GetIsMLOType() const					{ return m_flags.m_bContainsMLOtype; }

	inline strLocalIndex	GetParentTypeIndex(strLocalIndex index) const		{ return (m_typeDataIndicesCount <= 1) ? strLocalIndex(m_TypeDataIndex.index) : strLocalIndex((*m_TypeDataIndex.indices)[index.Get()]); }

	inline s32				GetNumParentTypes()	const				{ return m_typeDataIndicesCount; }	

	inline bool				IsSetAsParentTypeDef(s32 parentTypeIdx) 
	{ 
		if(m_typeDataIndicesCount == 0)
		{
			return false;
		}
		else if (m_typeDataIndicesCount == 1)
		{
			return m_TypeDataIndex.index == parentTypeIdx;
		}
		else
		{
			return m_TypeDataIndex.indices->ReverseFind(parentTypeIdx, m_typeDataIndicesCount - 1) != -1;
		}
	}
	bool				PushParentTypeDef(s32 parentTypeIdx);

	//inline bool				RequiresTempMemory() const				{ return (m_flags.m_bIsPermanent  || !m_flags.m_bContainsMLOtype);  }
	bool				RequiresTempMemory() const;

private:



	struct  
	{
		u16					m_bInitialised			: 1;
		u16					m_bIsNativeFile			: 1;
		u16					m_bIsPermanent			: 1;		// marked as permanent through the .meta file
		u16					m_bIsDependency			: 1;		// marked as an .imap dependency through ._manifest file in .rpf
		u16					m_bIsPermanentDLC		: 1;		// is part of a DLC package and is permanent while the pack is active
		u16					m_bIsPso				: 1;
		u16					m_bIsFastPso			: 1;
		u16					m_bIsInPlace			: 1;
		u16					m_bDelayLoading			: 1;		// debug : for .ityp files which can reference other .ityp files (e.g. interiors)
		u16					m_bContainsMLOtype		: 1;
		u16					m_unused				: 6;
	} m_flags;

	u16	m_typeDataIndicesCount;
	typedef union
	{
		s32 index;
		atRangeArray<s32, MAX_TYPE_DEPS>* indices;	// type data dependency for these instances
	} uTypeDataIndex;

	uTypeDataIndex m_TypeDataIndex;
};

} // namespace rage

#endif // __FWSCENE_MAPTYPESDEF_H__