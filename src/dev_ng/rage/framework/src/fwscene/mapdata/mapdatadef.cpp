/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwscene/mapdata/mapdatadef.cpp
// PURPOSE : asset store def specific to map data
// AUTHOR :  Ian Kiigan
// CREATED : 27/04/2011
//
/////////////////////////////////////////////////////////////////////////////////

#include "fwscene/mapdata/mapdatadef.h"
#include "fwscene/stores/mapdatastore.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		initialises map data def to known values, and passes through name
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::Init(const strStreamingObjectNameString name)
{
	fwAssetNameDef<fwMapDataContents>::Init(name); 
	SetInitState(NOT_INITIALISED);
	SetParentDef(NULL);
#if __BANK
	SetBlockIndex(-1);
#endif

#if __ASSERT
	SetRequiresDebugValidation(true);
#endif	//__ASSERT

	SetNumLoadedChildren(0);
	SetIsScriptManaged(false);
	SetIsCodeManaged(false);
	m_typeDataIndicesCount = 0;
	SetIsValid(true);
	SetPostponeWorldAdd(false);
	SetDepth(0);
	ResetNumMapChanges();
	SetIsCulled(false);
	SetIsPinned(false);

	// for debug only
	SetIsParent(false);
	SetIsInPlace(false);
	SetIsMLOInstanceDef(false);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	InitFromMapData
// PURPOSE:		populates def based on a map data file
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::InitFromMapData(s32 slotIndex, fwMapData* pMapData, spdAABB& dstPhyBounds, spdAABB& dstStrBounds)
{
	Assert(pMapData);
	
	const u32 contentFlags = pMapData->GetContentFlags();
	const u32 imapFlags = pMapData->GetFlags();

	const bool bContainsBlockInfo = ((contentFlags & fwMapData::CONTENTFLAG_BLOCKINFO)!=0);
	const bool bStreamable = ((imapFlags & fwMapData::FLAG_MANUAL_STREAM_ONLY)==0);
	const bool bIsParent = ((imapFlags & fwMapData::FLAG_IS_PARENT)!=0);

	InitCommon(
		slotIndex,
		pMapData->GetParent().GetHash(),
		bStreamable,
		contentFlags,
		bContainsBlockInfo,
		bIsParent
	);

	// set box streamer bounds
	pMapData->GetPhysicalExtents( dstPhyBounds );
	pMapData->GetStreamingExtents( dstStrBounds );
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	InitFromCacheEntry
// PURPOSE:		populates def based on a map data cache entry
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::InitFromCacheEntry(s32 slotIndex, fwMapDataCacheEntry* pCacheEntry, spdAABB& dstPhyBounds, spdAABB& dstStrBounds)
{
	Assert(pCacheEntry);

	InitCommon(
		slotIndex,
		pCacheEntry->m_parentNameHash,
		pCacheEntry->m_bDynamicStreaming,
		pCacheEntry->m_contentFlags,
		pCacheEntry->m_bContainsBlockInfo,
		pCacheEntry->m_bIsParent
	);

	// set box streamer bounds
	dstPhyBounds.Set(
		Vec3V(pCacheEntry->m_phyMinX, pCacheEntry->m_phyMinY, pCacheEntry->m_phyMinZ),
		Vec3V(pCacheEntry->m_phyMaxX, pCacheEntry->m_phyMaxY, pCacheEntry->m_phyMaxZ)
	);

	dstStrBounds.Set(
		Vec3V(pCacheEntry->m_strMinX, pCacheEntry->m_strMinY, pCacheEntry->m_strMinZ),
		Vec3V(pCacheEntry->m_strMaxX, pCacheEntry->m_strMaxY, pCacheEntry->m_strMaxZ)
	);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Init
// PURPOSE:		populates def
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::InitCommon(s32 slotIndex, u32 parentHash, bool bStreamable, u32 contentFlags, bool bHasBlockInfo, bool bIsParent)
{
	// parent def
	fwMapDataDef* pParentDef = NULL;
	if (parentHash != 0)
	{
		strLocalIndex parentSlot = strLocalIndex(fwMapDataStore::GetStore().FindSlotFromHashKey(parentHash));
		if ( Verifyf(parentSlot!=-1, "Initialising %s, but its parent IMAP is missing! Tools bug", m_name.GetCStr()) )
		{
			pParentDef = fwMapDataStore::GetStore().GetSlot(parentSlot);
		}
	}
	SetParentDef(pParentDef);

	// streamable?
	SetIsScriptManaged(!bStreamable);
	fwMapDataStore::GetStore().SetStreamable(strLocalIndex(slotIndex), bStreamable);

	// etc
	Assert(contentFlags <= 0xFFFF);
	m_contentFlags = contentFlags;
	SetHasBlockInfo(bHasBlockInfo);
	SetIsParent(bIsParent);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	CalcDepth
// PURPOSE		figure out how deep in the deps hierarchy this def is
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::CalcDepth()
{
	fwMapDataDef* pParent = GetParentDef();
	u32 newDepth = 0;
	while (pParent)
	{
		newDepth++;
		pParent = pParent->GetParentDef();
	}
	SetDepth(newDepth);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddMapChange
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::AddMapChange()
{
	if (Verifyf(GetNumMapChanges()<MAX_MAP_CHANGES, "Too many map changes registered"))
	{
		m_flags.m_numMapChanges++;
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveMapChange
// PURPOSE:
//////////////////////////////////////////////////////////////////////////
void fwMapDataDef::RemoveMapChange()
{
	if (Verifyf(GetNumMapChanges()>0, "Removing a map change, but none are registered"))
	{
		m_flags.m_numMapChanges--;
	}
}

fwMapDataCacheBlockInfo::fwMapDataCacheBlockInfo()
{
	m_flags = 0;

	memset(m_name_owner_exportedBy_time, '\0', sizeof(char) * BLOCKINFO_LEN_MAX);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	fwMapDataCacheEntry
// PURPOSE:		ctor caches elements from specified fwMapDataDef
//////////////////////////////////////////////////////////////////////////
fwMapDataCacheEntry::fwMapDataCacheEntry(fwMapDataDef* pDef, const spdAABB& phyBounds, const spdAABB& strBounds)
{
	Assert(pDef);

	const fwMapDataDef* pParentDef = pDef->GetParentDef();

	m_nameHash = pDef->m_name.GetHash();
	m_parentNameHash = pParentDef ? pParentDef->m_name.GetHash() : 0;
	m_contentFlags = pDef->GetContentFlags();
	m_bDynamicStreaming = !pDef->GetIsScriptManaged();
	m_bContainsBlockInfo = pDef->GetHasBlockInfo();
	m_bIsParent = pDef->GetIsParent();

	m_strMinX = strBounds.GetMinVector3().x;
	m_strMinY = strBounds.GetMinVector3().y;
	m_strMinZ = strBounds.GetMinVector3().z;
	m_strMaxX = strBounds.GetMaxVector3().x;
	m_strMaxY = strBounds.GetMaxVector3().y;
	m_strMaxZ = strBounds.GetMaxVector3().z;

	m_phyMinX = phyBounds.GetMinVector3().x;
	m_phyMinY = phyBounds.GetMinVector3().y;
	m_phyMinZ = phyBounds.GetMinVector3().z;
	m_phyMaxX = phyBounds.GetMaxVector3().x;
	m_phyMaxY = phyBounds.GetMaxVector3().y;
	m_phyMaxZ = phyBounds.GetMaxVector3().z;

#if __BANK
	// this has no business being in the framework, but there's no other way of solving this atm
	if (m_bContainsBlockInfo && fwMapDataStore::GetInterface())
	{
		fwMapDataStore::GetInterface()->CreateCacheFromBlockInfo(m_blockInfo, pDef->GetBlockIndex());
	}
#endif	//!__FINAL
}

}	//namespace rage
