//
// fwscene/texLod.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "texLod.h"

#include "fwscene/stores/txdstore.h"
#include "fwscene/stores/drawablestore.h"
#include "fwscene/stores/fragmentstore.h"
#include "fwscene/stores/dwdstore.h"

#include "fragment/drawable.h"
#include "fragment/type.h"

namespace rage
{

// try and wrap up asset shittiness in a single class (ie. txds embedded anywhere inside an asset)
// --- fwAssetLocation ---
void fwAssetLocation::Set(eStoreType type, unsigned int slot)
{
	m_assetLoc.s.storeType = STORE_ASSET_INVALID;
	strStreamingModule* pModule = NULL;

	switch(type){
	case(STORE_ASSET_TXD):
		pModule = &g_TxdStore;
		break;
	case(STORE_ASSET_DRB):
		pModule = &g_DrawableStore;
		break;
	case(STORE_ASSET_FRG):
		pModule = &g_FragmentStore;
		break;
	case(STORE_ASSET_DWD):
		pModule = &g_DwdStore;
		break;
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetParentAsset: asset type is %d", GetStoreType());
		break;
	}

	if (pModule != NULL)
	{
		if (slot < (unsigned int)pModule->GetCount())
		{
			m_assetLoc.s.storeSlot = slot;
			m_assetLoc.s.storeType = type;
		}
	}
}

fwAssetLocation fwAssetLocation::GetParentAsset(void) const
{
	s32 slot = -1;
	eStoreType store = STORE_ASSET_TXD;

	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		slot = g_TxdStore.GetParentTxdSlot(strLocalIndex(m_assetLoc.s.storeSlot)).Get();
		break;
	case(STORE_ASSET_DRB):
		slot = g_DrawableStore.GetParentTxdForSlot(strLocalIndex(m_assetLoc.s.storeSlot)).Get();
		break;
	case(STORE_ASSET_FRG):
		slot = g_FragmentStore.GetParentTxdForSlot(strLocalIndex(m_assetLoc.s.storeSlot)).Get();
		break;
	case(STORE_ASSET_DWD):
		slot = g_DwdStore.GetParentTxdForSlot(strLocalIndex(m_assetLoc.s.storeSlot)).Get();
		break;
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetParentAsset: asset type is %d", GetStoreType());
		break;
	}

	if (slot == -1){
		store = STORE_ASSET_INVALID;
	}

	return(fwAssetLocation(store, slot));
}

strStreamingModule*	fwAssetLocation::GetStreamingModule(void) const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		NOTFINAL_ONLY(g_TxdStore.Validate(GetStoreSlot());)
		return &g_TxdStore;
	case(STORE_ASSET_DRB):
		NOTFINAL_ONLY(g_DrawableStore.Validate(GetStoreSlot());)
		return &g_DrawableStore;
	case(STORE_ASSET_FRG):
		NOTFINAL_ONLY(g_FragmentStore.Validate(GetStoreSlot());)
		return &g_FragmentStore;
	case(STORE_ASSET_DWD):
		NOTFINAL_ONLY(g_DwdStore.Validate(GetStoreSlot());)
		return &g_DwdStore;
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetStreamingModule: asset type is %d", GetStoreType());
		break;
	}

	return NULL;
}

#if !__FINAL
const char* fwAssetLocation::GetName(void) const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return(g_TxdStore.GetName(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DRB):
		return(g_DrawableStore.GetName(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_FRG):
		return(g_FragmentStore.GetName(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DWD):
		return(g_DwdStore.GetName(strLocalIndex(m_assetLoc.s.storeSlot)));
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetName: asset type is %d", GetStoreType());
		return "";
	}
}

const char* fwAssetLocation::GetStoreTypeStr(void) const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return "TXD";
	case(STORE_ASSET_DRB):
		return "DRB";
	case(STORE_ASSET_FRG):
		return "FRG";
	case(STORE_ASSET_DWD):
		return "DWD";
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetStoreTypeStr: asset type is %d", GetStoreType());
		return "";
	}
}
#endif // !__FINAL

bool fwAssetLocation::IsBeingDefragged(void) const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return(g_TxdStore.IsBeingDefragged(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DRB):
		return(g_DrawableStore.IsBeingDefragged(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_FRG):
		return(g_FragmentStore.IsBeingDefragged(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DWD):
		return(g_DwdStore.IsBeingDefragged(strLocalIndex(m_assetLoc.s.storeSlot)));
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::IsBeingDefragged: asset type is %d", GetStoreType());
		return false;
	}
}

bool fwAssetLocation::GetIsHDCapable(void) const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return(g_TxdStore.GetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DRB):
		return(g_DrawableStore.GetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_FRG):
		return(g_FragmentStore.GetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DWD):
		return(g_DwdStore.GetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot)));
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetIsHDCapable: asset type is %d", GetStoreType());
		return false;
	}
}

bool fwAssetLocation::GetIsBoundHD(void) const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return(g_TxdStore.GetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DRB):
		return(g_DrawableStore.GetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_FRG):
		return(g_FragmentStore.GetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DWD):
		return(g_DwdStore.GetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot)));
	default:
		Assertf(GetStoreType() == STORE_ASSET_INVALID, "fwAssetLocation::GetIsBoundHD: asset type is %d", GetStoreType());
		return false;
	}
}

void fwAssetLocation::SetIsHDCapable(bool val)
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		g_TxdStore.SetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	case(STORE_ASSET_DRB):
		g_DrawableStore.SetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	case(STORE_ASSET_FRG):
		g_FragmentStore.SetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	case(STORE_ASSET_DWD):
		g_DwdStore.SetIsHDCapable(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	default:
		Assertf(false, "fwAssetLocation::SetIsHDCapable: asset type is %d", GetStoreType());
	}
}

bool fwAssetLocation::HasLoaded() const
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return(g_TxdStore.HasObjectLoaded(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DRB):
		return(g_DrawableStore.HasObjectLoaded(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_FRG):
		return(g_FragmentStore.HasObjectLoaded(strLocalIndex(m_assetLoc.s.storeSlot)));
	case(STORE_ASSET_DWD):
		return(g_DwdStore.HasObjectLoaded(strLocalIndex(m_assetLoc.s.storeSlot)));
	default:
		Assertf(false, "fwAssetLocation::Validate: asset type is %d", GetStoreType());
	}

	return(false);
}

void fwAssetLocation::Validate()
{
#if !__FINAL
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		g_TxdStore.Validate(strLocalIndex(m_assetLoc.s.storeSlot));
		break;
	case(STORE_ASSET_DRB):
		g_DrawableStore.Validate(strLocalIndex(m_assetLoc.s.storeSlot));
		break;
	case(STORE_ASSET_FRG):
		g_FragmentStore.Validate(strLocalIndex(m_assetLoc.s.storeSlot));
		break;
	case(STORE_ASSET_DWD):
		g_DwdStore.Validate(strLocalIndex(m_assetLoc.s.storeSlot));
		break;
	default:
		Assertf(false, "fwAssetLocation::Validate: asset type is %d", GetStoreType());
	}
#endif //!__FINAL
}

#if __DEV
bool fwAssetLocation::PreValidate()
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return g_TxdStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	case(STORE_ASSET_DRB):
		return g_DrawableStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	case(STORE_ASSET_FRG):
		return g_FragmentStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	case(STORE_ASSET_DWD):
		return g_DwdStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	default:
		Displayf("fwAssetLocation::PreValidate: asset type is %d", GetStoreType());
	}
	return false;
}
#endif //!__DEV

bool fwAssetLocation::PreValidateTexLod()
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		return g_TxdStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	case(STORE_ASSET_DRB):
		return g_DrawableStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	case(STORE_ASSET_FRG):
		return g_FragmentStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	case(STORE_ASSET_DWD):
		return g_DwdStore.Get(strLocalIndex(m_assetLoc.s.storeSlot)) != NULL;
		break;
	default:
		Displayf("fwAssetLocation::PreValidateTexLod: asset type is %d", GetStoreType());
	}
	return false;
}

void fwAssetLocation::SetIsBoundHD(bool val)
{
	switch(m_assetLoc.s.storeType){
	case(STORE_ASSET_TXD):
		g_TxdStore.SetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	case(STORE_ASSET_DRB):
		g_DrawableStore.SetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	case(STORE_ASSET_FRG):
		g_FragmentStore.SetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	case(STORE_ASSET_DWD):
		g_DwdStore.SetIsBoundHD(strLocalIndex(m_assetLoc.s.storeSlot), val);
		break;
	default:
		Assertf(false, "fwAssetLocation::SetIsBoundHD: asset type is %d", GetStoreType());
	}
}

// get a list of txds from a loaded asset
void fwAssetLocation::GetTxdList(fwPtrListSingleLink &txdList){

	// get the target Txd(s) from the target asset
	fwTxd* pLowTxd = NULL;
	switch(GetStoreType()){
	case(STORE_ASSET_INVALID):
		break;
	case(STORE_ASSET_TXD):
		pLowTxd = g_TxdStore.Get(GetStoreSlot());
		txdList.Add(pLowTxd);
		break;
	case(STORE_ASSET_DRB):
		{
			rmcDrawable* pDrawable = g_DrawableStore.Get(GetStoreSlot());
			if (pDrawable){
				pLowTxd = pDrawable->GetShaderGroup().GetTexDict();
				if (pLowTxd){
					txdList.Add(pLowTxd);
				}
			}
		}
		break;
	case(STORE_ASSET_DWD):
		{
			const Dwd* pDwd = g_DwdStore.GetSafeFromIndex(GetStoreSlot());
			if (pDwd){
				int dwdIndex = 0;
				while(dwdIndex < pDwd->GetCount())
				{
					rmcDrawable* pDrawable = pDwd->GetEntry(dwdIndex);
					if (pDrawable){
						pLowTxd = pDrawable->GetShaderGroup().GetTexDict();
						if (pLowTxd && !txdList.IsMemberOfList(pLowTxd)){
							txdList.Add(pLowTxd);
						}
					}
					dwdIndex++;
				}
			}
		}
		break;
	case(STORE_ASSET_FRG):
		{
			const Fragment* pFragment = g_FragmentStore.GetSafeFromIndex(GetStoreSlot());

			if (pFragment){
				rmcDrawable* pDrawable = pFragment->GetCommonDrawable();
				if (pDrawable){
					pLowTxd = pDrawable->GetShaderGroup().GetTexDict();
					if (pLowTxd){
						txdList.Add(pLowTxd);
					}
				}

				int fragIndex = 0;
				while(fragIndex < pFragment->GetNumExtraDrawables())
				{
					rmcDrawable* pDrawable = pFragment->GetExtraDrawable(fragIndex);
					if (pDrawable){
						pLowTxd = pDrawable->GetShaderGroup().GetTexDict();
						if (pLowTxd && !txdList.IsMemberOfList(pLowTxd)){
							txdList.Add(pLowTxd);
						}
					}
					fragIndex++;
				}
			}
		}
		break;
	default:
		Assertf(false, "fwAssetLocation::GetTxdList: asset type is %d", GetStoreType());
	}

	return;
}

fwTexLodInterface* g_fwTexLodInterface;

void fwTexLod::InitClass(fwTexLodInterface* texLodInterface)
{
	g_fwTexLodInterface = texLodInterface;
}

void fwTexLod::ShutdownClass()
{
	if (g_fwTexLodInterface)
	{
		delete g_fwTexLodInterface;
		g_fwTexLodInterface = NULL;
	}
}

void fwTexLod::SetSwapStateSingle(bool bSwap, fwAssetLocation targetAsset)
{
	g_fwTexLodInterface->SetSwapStateSingle(bSwap, targetAsset);
}

} // namespace rage
