//
// scene/scene.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "scene.h"

#include "fwdrawlist/drawlistmgr.h"
#include "streaming/packfilemanager.h"

namespace rage
{
fwSceneInterface* g_fwSceneInterface;

void fwScene::InitClass(fwSceneInterface* sceneInterface)
{
	g_fwSceneInterface = sceneInterface;
}

void fwScene::ShutdownClass()
{
	delete g_fwSceneInterface;
	g_fwSceneInterface = NULL;
}

void fwScene::Init()
{
	g_fwSceneInterface->InitPools();
}

void fwScene::Shutdown()
{
	g_fwSceneInterface->ShutdownPools();
}

void fwScene::LoadMap(const char* UNUSED_PARAM(filename))
{
}

} // namespace rage