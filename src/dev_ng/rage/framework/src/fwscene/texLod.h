//
// fwscene/texLod.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "fwutil/PtrList.h"

//fw
#include "streaming/streaminginfo.h"
#include "streaming/streamingmodule.h"

#ifndef FWSCENE_TEXLOD_H__
#define FWSCENE_TEXLOD_H__

namespace rage
{

class strStreamingModule;

enum eStoreType{
	STORE_ASSET_INVALID		=		0,
	STORE_ASSET_TXD			=		1,
	STORE_ASSET_DRB			=		2,
	STORE_ASSET_FRG			=		3,
	STORE_ASSET_DWD			=		4
};

union assetLoc{
	struct {	
		eStoreType				storeType : 8;
		unsigned int			storeSlot : 24;
	} s;
	unsigned int			assetHash;
};

class fwAssetLocation
{
public:
	fwAssetLocation(void) { m_assetLoc.assetHash = 0;}
	fwAssetLocation(eStoreType type, unsigned int slot) { Set(type,slot); }
	fwAssetLocation(unsigned int newAssetHash) { m_assetLoc.assetHash = newAssetHash; }

	void Set(eStoreType type, unsigned int slot);

	bool operator == (const fwAssetLocation & locRHS) { return(m_assetLoc.assetHash == locRHS.m_assetLoc.assetHash); }
	operator int() const { return(m_assetLoc.assetHash); }  // to use fwAssetLocation as key in atMap<>

#if !__FINAL
	const char*			GetName(void) const;
	const char*			GetStoreTypeStr(void) const;
#endif // !__FINAL
	eStoreType			GetStoreType(void) const { return(m_assetLoc.s.storeType); }
	strLocalIndex		GetStoreSlot(void) const { return strLocalIndex(m_assetLoc.s.storeSlot); }

	void				GetTxdList(rage::fwPtrListSingleLink& txdList);
	fwAssetLocation		GetParentAsset(void) const;
	strStreamingModule*	GetStreamingModule(void) const;
	strStreamingInfo*	GetStreamingInfo()			{ return(GetStreamingModule()->GetStreamingInfo(strLocalIndex(m_assetLoc.s.storeSlot))); }
	strIndex			GetStreamingIndex()			{ return(GetStreamingModule()->GetStreamingIndex(strLocalIndex(m_assetLoc.s.storeSlot))); }
	strLocalIndex		GetLocalIndex()			{ return strLocalIndex(m_assetLoc.s.storeSlot); }

	bool				IsValid() const { return(m_assetLoc.s.storeType != STORE_ASSET_INVALID); }
	void				Invalidate() { m_assetLoc.s.storeType = STORE_ASSET_INVALID; }
	bool				HasLoaded() const;

	bool				IsBeingDefragged(void) const;
	bool				GetIsHDCapable(void) const;
	bool				GetIsBoundHD(void) const;
	void				Validate(void);
	bool				PreValidateTexLod(void);
#if __DEV
	bool				PreValidate(void);
#endif 

	void				SetIsHDCapable(bool val);
	void				SetIsBoundHD(bool val);

private:
	assetLoc			m_assetLoc;
};

class fwTexLodInterface
{
public:
	virtual ~fwTexLodInterface() {}
	virtual void SetSwapStateSingle(bool bSwap, fwAssetLocation targetAsset) = 0;
};

class fwTexLod
{
public:
	static void InitClass(fwTexLodInterface* sceneInterface);
	static void ShutdownClass();

	static void SetSwapStateSingle(bool bSwap, fwAssetLocation targetAsset);
};

} // namespace rage

#endif // FWSCENE_TEXLOD_H__
