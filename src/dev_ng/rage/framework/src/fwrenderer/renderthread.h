/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    renderThread.cpp
// PURPOSE : stuff for setting up separate render thread & functions it requires
// AUTHOR :  john w.
// CREATED : 1/8/06
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWRENDERER_RENDERTHREAD_H_
#define FWRENDERER_RENDERTHREAD_H_

#define LOG_RENDER_FLUSH (1 && RSG_PC && !__FINAL)

#include "atl/functor.h"

#if __PPU && 0
	#include "atl/queue.h"
	#include "grcore/device.h"
#endif
#include "system/ipc.h"
#include "system/threadtype.h"

#define RENDER_THREAD_CORE			(3)

#if RSG_ORBIS && !__OPTIMIZED
	#define RENDER_THREAD_STACK		(512  * 1024)
#elif (RSG_PC || RSG_ORBIS || RSG_DURANGO)
	#define RENDER_THREAD_STACK		(512 * 1024)
#elif RSG_XENON
	#define RENDER_THREAD_STACK		(128  * 1024)		// anything using XTL functions ought to be at least 64k as per MS docs
#else
	#define RENDER_THREAD_STACK		(96 * 1024)
#endif

#define MAX_NUM_RENDER_CONTEXTS	(100)

namespace rage {

// This structure is used to contain all of the events needed to manage a thread.
// It also contains functions to perform management of the thread
// This can be done however you like, this method is very clean for demonstration purposes
typedef struct _ThreadParams
{
	sysIpcSema		m_eKill;			// indicates that the thread should be killed to renderthread
	sysIpcSema		m_eRunning;			// indicates that the renderthread is running and has assumed the device
	sysIpcSema		m_eUpdateDone;		
	sysIpcSema		m_eRenderDone;
	sysIpcMutex		m_customRenderThreadMutex;	// Locked while a custom render function is rendering

	_ThreadParams();

	~_ThreadParams();

	void Init();
	void Shutdown();

	bool PollIsKilled() { return sysIpcPollSema(m_eKill); }
	bool PollIsRunning() { return sysIpcPollSema(m_eRunning); }
	void WaitTillRunning() { sysIpcWaitSema(m_eRunning); }
	bool PollUpdateDone() { return sysIpcPollSema(m_eUpdateDone); }
	void WaitTillUpdateDone() { sysIpcWaitSema(m_eUpdateDone); }
	bool PollIsRenderDone() { return sysIpcPollSema(m_eRenderDone); }
	void WaitTillRenderDone() { sysIpcWaitSema(m_eRenderDone); }

 	void SignalKill() { sysIpcSignalSema(m_eKill); }
 	void SignalRunning() { sysIpcSignalSema(m_eRunning); }
	void SignalUpdateDone() { sysIpcSignalSema(m_eUpdateDone); }
	void SignalRenderDone() { sysIpcSignalSema(m_eRenderDone); }

	void LockRenderMutex()	{ sysIpcLockMutex(m_customRenderThreadMutex); }
	void UnlockRenderMutex()	{ sysIpcUnlockMutex(m_customRenderThreadMutex); }

	bool CheckSemaIntegrity() { return((m_eUpdateDone != NULL) && (m_eRenderDone != NULL));}
} ThreadParams;

class fwRenderThreadInterface;
class fwRenderThread;
class fwRenderPhaseList;

struct RenderThreadFuncParams 
{
	fwRenderThreadInterface			*m_pInterface;
	fwRenderThread					*m_pThread;
};

typedef Functor0 RenderFn;

struct fwRenderThreadGameInterface
{
	virtual ~fwRenderThreadGameInterface()		{}
	virtual void DefaultPreRenderFunction()		{}
	virtual void DefaultPostRenderFunction()	{}
	virtual void NonDefaultPostRenderFunction(bool UNUSED_PARAM(bUpdateDone))	{}
	#if __WIN32
	virtual void FlushScaleformBuffers()		{}
	#endif
	virtual void RenderThreadInit()				{}
	virtual void RenderThreadShutdown()			{}
	virtual void PerformSafeModeOperations()	{}
	virtual void RenderFunctionChanged(RenderFn /*oldFn*/, RenderFn /*newFn*/)		{}
	virtual void Flush()						{}
};

class fwRenderThreadInterface
{
public:
	fwRenderThreadInterface(void);
	virtual ~fwRenderThreadInterface(void);

	// called from the main thread
	void Init(sysIpcPriority priority,void (*initcallback)());
	void Shutdown();

#if __PS3
	void Exit();
#endif //__PS3

	void Synchronise();

	/** PURPOSE: Called on the main thread to flush the queues. This will perform four empty frame
	 *  synchronizations where empty draw lists are being pushed and the render thread consumes whatever
	 *  there is.
	 */
	void Flush(bool forceFullFlush = false);

#if __WIN32PC
	void FlushAndDeviceCheck();
	void HandleDeviceReset();
#endif
	
	// Accessors
	ThreadParams*	GetDrawThreadParams()			{ return &m_DrawThreadParams; }

	/** PURPOSE: Called on the main thread. Will block the GPU until the fence of the previous frame
	 *  has been set.
	 */
	void GPU_IdleSection(bool duringFlush = false);
	s32 GetUpdateBuffer() { FastAssert(sysThreadType::GetCurrentThreadType() & m_UpdateBufferThreadAccess); return m_updateBuffer;}
	s32 GetRenderBuffer() { FastAssert(sysThreadType::IsRenderThread()); return m_renderBuffer;}
	s32 GetCurrentBuffer() { if(!sysThreadType::IsRenderThread()) return m_updateBuffer; else return m_renderBuffer;}

#if !__FINAL
	void SetUpdateBufferThreadAccess(int threadTypeMask)				{ m_UpdateBufferThreadAccess = threadTypeMask; }
#endif // !__FINAL

#if !__FINAL || __FINAL_LOGGING
	u32 GetRenderThreadFrameNumber() const								{ return m_RenderThreadFrameNumber; }
	void SetRenderThreadFrameNumber(u32 frameNumber)					{ m_RenderThreadFrameNumber = frameNumber; m_IsRenderThreadSynchronized = true; }
	bool IsRenderThreadSynchronized() const								{ return m_IsRenderThreadSynchronized; }
#endif // !__FINAL || __FINAL_LOGGING
	
	void SetRenderFunction(RenderFn fn);
	void SetDefaultRenderFunction();

	void SetGameInterface(fwRenderThreadGameInterface *gameInterface)	{ m_gameInterface = gameInterface; }

	RenderFn GetRenderFunction() const				{ return m_renderFunction; }
	RenderFn GetPendingRenderFunction() const		{ return m_pendingRenderFunction; }


	bool IsUsingDefaultRenderFunction();

	/** PURPOSE: This is the render thread's main loop that executes the current render thread
	 *  function and keeps going until it receives a signal telling it to quit.
	 */
	void MainLoop();
	void FlipUpdateBuffer();

	void DoRenderFunction();

	bool	IsActive() const	{	return m_bActive;	}

	void ClearFlushedState() { m_flushed = false; }

	u32 GetTimeWaitedForUpdate() { return m_TimeWaitedForUpdate; }
	u32 GetTimeWaitedForGpu() { return m_TimeWaitedForGpu; }
	u32 GetTimeWaitedForRenderThread() { return m_TimeWaitedForRenderThread; }

#if RSG_PC
	bool	IsUsingRender()	{ return m_bUsingDefaultRenderFunc; }
	sysIpcThreadId GetRenderThreadId() const { return m_threadId; }
#endif

protected:
	// This is the safe area, executed during the main thread, when we know that
	// the render thread is currently idle.
	virtual void PerformSafeModeOperations() {};

	/** PURPOSE: This is the default render thread function, enabled by calling SetDefaultRenderFunction().
	 *  It will keep popping draw lists from the queue until it gets a terminator (=NULL pointer instead
	 *  of draw list).
	 */
	void DefaultRenderFunction();

	void FlipRenderBuffer();

	fwRenderThreadGameInterface	*m_gameInterface;
	fwRenderThreadGameInterface	m_defaultGameInterface;
	RenderThreadFuncParams		m_funcParams;
	ThreadParams				m_DrawThreadParams;
	sysIpcThreadId				m_threadId;
	s32							m_updateBuffer;
	s32							m_renderBuffer;
	void						(*m_InitCallback)();
#if RSG_PC
	bool	m_bUsingDefaultRenderFunc;
#endif

#if !__FINAL
	// Only threads that have at least one of those bits in their types may access the
	// update buffer.
	int							m_UpdateBufferThreadAccess;
#endif // !__FINAL

#if !__FINAL || __FINAL_LOGGING
	u32							m_RenderThreadFrameNumber;
	bool						m_IsRenderThreadSynchronized;
#endif // !__FINAL || __FINAL_LOGGING

	RenderFn					m_pendingRenderFunction;
	RenderFn					m_pendingRenderFunctionCopy;
	RenderFn					m_renderFunction;
	bool						m_disableSyncronisation;
	bool						m_bActive;
	bool						m_flushed;
	u32							m_TimeWaitedForUpdate;
	u32							m_TimeWaitedForGpu;
	u32							m_TimeWaitedForRenderThread;
};

extern fwRenderThreadInterface	gRenderThreadInterface;

} // namespace rage


#endif // FWRENDERER_RENDERTHREAD_H_
