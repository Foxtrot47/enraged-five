﻿
//**********************************
// RenderPhaseBase.h
// (C) Rockstar North Ltd.
// description: Hi level renderer - abstracts rendering into a platform independent set of phases. Base class - derive from it per project to do useful work...
//**********************************
// History
// Created 11/2009 - John Whyte
//**********************************

#ifndef INC_RENDERPHASEBASE_H_
#define INC_RENDERPHASEBASE_H_

// C++ hdrs
// Rage hdrs

#include "atl/singleton.h"
#include "grcore/viewport.h"
#include "vector/vector3.h"

// framework headers
#include "fwrenderer/RenderSettingsBase.h"
#include "fwrenderer/renderphaseswitches.h"
#include "fwutil/flags.h"

namespace rage
{
	class bkGroup;
	class grcViewport;

extern __THREAD int ThreadIDBase;

enum {
	INVALID_RENDERPHASE_ID = -1,		// establish an invalid renderphase ID for default
	INVALID_SORT_VAL = -1,				// establish an invalid value for sort type to apply to phases
};

class fwRenderPhaseScanner
{
public:
	fwRenderPhaseScanner();

	u32 GetRenderFlags() const;
	void SetRenderFlags(u32 renderFlags)				{ m_RenderFlags = renderFlags; }

	const Vector3& GetCameraPositionForFade() const		{ return m_vecCameraPosition; } 
	const Vector3& GetCameraDirection() const			{ return m_vecCameraDir; } 

	float GetCameraFarClip() const						{ return m_CameraFarClip; }

	void SetCameraForFadeCalcs(const Vector3 &pos,const Vector3 &dir,float farclip) { m_vecCameraPosition=pos;m_vecCameraDir=dir;m_CameraFarClip=farclip; } 

	const grcViewport *GetLodViewport() const					{ return m_LodViewport; }
	void SetLodViewport(const grcViewport *viewport)			{ m_LodViewport = viewport; }

protected:
	Vector3 m_vecCameraPosition;
	Vector3 m_vecCameraDir;

	float m_CameraFarClip;

	u32 m_RenderFlags;

	// Viewport to be used for LOD considerations
	const grcViewport *m_LodViewport;
};

class fwRenderPhase
{
public:
	fwRenderPhase(const grcViewport *viewport, const char *name, int drawListType, float updateBudget);
#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	fwRenderPhase(const grcViewport *viewport, const char *name, int drawListType, float updateBudget, int startDrawListType, int endDrawListType);
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS

	virtual ~fwRenderPhase();

	virtual void BuildRenderList()	{};
	virtual void BuildDrawList()	{};

	virtual void UpdateViewport()	{};

	virtual fwRenderPhaseScanner *GetScanner()	{ return NULL; }

	void DrawListPrologue();
	void DrawListEpilogue();
#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	void DrawListPrologue(int listType);
	void DrawListEpilogue(int listType);
#endif //RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS

#if !__FINAL
	void SetName(const char *name)		{ m_Name = name; }
	const char *GetName() const			{ return m_Name.c_str(); }
#endif // !__FINAL

	void Disable()						{ m_bDisabled = true; } 
	void Enable()						{ m_bDisabled = false; } 
	bool IsDisabled() const				{ return m_bDisabled; } 
	bool *GetDisabledPtr()				{ return &m_bDisabled; }

	bool IsActive() const				{ return m_bActive; }
	void SetActive(bool active)			{ m_bActive = active; }
	
	bool IsBuildDrawListEnabled() const			{ return m_BuildDrawListEnabled;	}
	void SetBuildDrawListEnabled(bool enable)	{ m_BuildDrawListEnabled = enable;	}
#if RSG_PC
	void SetMultiGPUDrawListCounter(int frameCount)	{ m_FrameCountTillDisable = frameCount; }
	int GetMultiGPUDrawListCounter()				{ return m_FrameCountTillDisable; }
#endif

	void SetEntityListIndex(s32 list)		{ m_EntityListIndex = list; }
	s32 GetEntityListIndex() const			{ return m_EntityListIndex; }

	bool HasEntityListIndex() const		{ return m_EntityListIndex != INVALID_RENDERPHASE_ID; }

	int GetDrawListType() const			{ return m_DrawListType; }

	grcViewport &GetGrcViewport()					{ return m_GrcViewport; }
	const grcViewport &GetGrcViewport() const		{ return m_GrcViewport; }
	void SetGrcViewport(const grcViewport &vp)		{ m_GrcViewport = vp; }

	// PURPOSE: Sets the visibility entity mask: a mask of bits specifying which entity subsets we want to render in this render phase
	void SetEntityVisibilityMask(const fwFlags16& value) { m_entityVisibilityMask = value; }
	// PURPOSE: Gets the visibility entity mask: a mask of bits specifying which entity subsets we want to render in this render phase
	const fwFlags16& GetEntityVisibilityMask() const { return m_entityVisibilityMask; }
	// PURPOSE: Gets the visibility entity mask: a mask of bits specifying which entity subsets we want to render in this render phase
	fwFlags16& GetEntityVisibilityMask() { return m_entityVisibilityMask; }

	// PURPOSE: Sets the visibility shape subset, that is a arbitrary game-specific id of the 'kind' of the phase. Entity may exclude theirselves from rendering based on this
	void SetVisibilityType(const u16 value) { m_visibilityType = value; }
	// PURPOSE: Gets the visibility shape subset, that is a arbitrary game-specific id of the 'kind' of the phase. Entity may exclude theirselves from rendering based on this
	u16 GetVisibilityType() const { return m_visibilityType; }

	enum LodMask
	{
		LODMASK_HD_INTERIOR	= BIT(0), // Note HD includes Orphaned HD
		LODMASK_HD_EXTERIOR	= BIT(1),
		LODMASK_HD			= LODMASK_HD_INTERIOR|LODMASK_HD_EXTERIOR,
		LODMASK_LOD			= BIT(2),
		LODMASK_SLOD1		= BIT(3),
		LODMASK_SLOD2		= BIT(4),
		LODMASK_SLOD3		= BIT(5),
		LODMASK_SLOD4		= BIT(6),
		LODMASK_LIGHT		= BIT(7),
		LODMASK_NONE		= 0xFF
	};

	// PURPOSE: Set the lods enabled for this phase (eLodTypeFlags), defaults to 0xFF which represents all. Note using either LODTYPES_FLAG_ORPHANHD or LODTYPES_FLAG_HD will implicitly enable both flags
	void SetLodMask(u8 lodMask) { m_lodMask = lodMask;}
	u8 GetLodMask() { return m_lodMask;}

#if __BANK
	void RegisterGroup(bkGroup *group) {m_WidgetGroup = group;}

	virtual void AddWidgets(bkGroup &/*group*/) {}

	static u32 GetRenderSettingsOverrideMask()			{ return sm_RenderSettingsOverride; }

	static void SetRenderSettingsOverrideMask(u32 mask)	{ sm_RenderSettingsOverride = mask; }
#endif // __BANK

private:
	// Type of draw list to create
	int m_DrawListType;

	s32 m_EntityListIndex;

	// The entity list ID that will be passed to the render list builder.
	fwFlags16 m_entityVisibilityMask;
	u16 m_visibilityType;

	grcViewport  m_GrcViewport;

#if !__FINAL
	ConstString m_Name;
#endif // !__FINAL

#if !__FINAL || __FINAL_LOGGING
	// Max number of milliseconds we're supposed to spend updating
	float m_UpdateBudget;
#endif // !__FINAL || __FINAL_LOGGING

	u8 m_lodMask;
	bool m_bDisabled;

	// If true, this render phase has been updated without an early out within the last frame.
	bool m_bActive;
	bool m_BuildDrawListEnabled;
#if RSG_PC
	int m_FrameCountTillDisable;
#endif

#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS && __ASSERT
	int m_startDrawListType;
	int m_endDrawListType;
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS && __ASSERT

#if __BANK
	// Global override for render settings to turn individual flags off
	// A cleared bit indicates an override to disable the setting
	static u32 sm_RenderSettingsOverride;
	bkGroup *m_WidgetGroup;
#endif // __BANK

};

class fwRenderPhaseManager
{
public:
	fwRenderPhaseManager(int maxRenderPhases);

	void RegisterRenderPhase(fwRenderPhase &renderPhase);

	void BuildRenderList();

	void BuildDrawList();

	void UpdateViewports();

	void ResetRenderPhaseList()						{ m_RenderPhases.Resize(0); }

	int GetRenderPhaseCount() const					{ return m_RenderPhases.GetCount(); }

	fwRenderPhase &GetRenderPhase(int phase)		{ return *m_RenderPhases[phase]; }

#if __BANK
	void ShowScanResults(bool toScreen) const;

	void AddWidgets(bkGroup &group);

	void AddRenderPhaseToWidgets(fwRenderPhase &group);

	void RemoveRenderPhaseWidget(bkGroup *removeGroup);

	bool HasDebugDraw() const;

	void DebugDraw();
#endif // __BANK

#if __ASSERT
	void SetEntityListSanityCheck(bool bEnabled);
#endif 

private:
	void PrintLine(bool toScreen, const char *fmt, ...) const;

	atArray<fwRenderPhase *> m_RenderPhases;

#if __BANK
	// The main group that contains all render phases
	bkGroup *m_BankGroup;
#endif // __BANK

#if __ASSERT
	bool m_EnableEntityListCheck;
#endif
};

inline u32 fwRenderPhaseScanner::GetRenderFlags() const
{
	return m_RenderFlags BANK_ONLY(& fwRenderPhase::GetRenderSettingsOverrideMask());
}


typedef atSingleton<fwRenderPhaseManager> g_RenderPhaseManager;
#define RENDERPHASEMGR g_RenderPhaseManager::InstanceRef()

} //namespace rage

#endif //INC_RENDERPHASEBASE_H_

