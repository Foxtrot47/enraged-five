
#include "renderlistbuilder.h"

#include "bank/bkmgr.h"
#include "input/mouse.h"
#include "profile/cputrace.h"
#include "profile/timebars.h"
#include "system/nelem.h"

#include "entity/drawdata.h"
#include "entity/entity.h"
#include "grcore/debugdraw.h"
#include "fwutil/xmacro.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwdrawlist/drawlist_channel.h"
#include "fwpheffects/ropemanager.h"
#include "fwscene/world/EntityDesc.h"
#include "fwscene/scan/VisibilityFlags.h"
#include "fwrenderer/renderlistgroup.h"
#include "fwscene/scan/scan.h"
#include "fwdebug/picker.h"

DRAWLIST_OPTIMISATIONS()

namespace rage {

// there are three separate defines for the number of subphase vis flags .. make sure they agree
CompileTimeAssert(SUBPHASE_MAX == fwVisibilityFlags::MAX_SUBPHASE_VISIBILITY_BITS);
CompileTimeAssert((int)SUBPHASE_MAX == (int)fwEntity::fwRenderFlag_USER_FLAG_SHIFT);

//////////////////////
// Extern & globals //
//////////////////////
#if RAGE_INSTANCED_TECH
DECLARE_MTR_THREAD bool fwRenderListBuilder::sm_bInstancing = false;
#endif

#if __BANK
static int s_RenderPhaseIsolate = 0; // 0=all, >0 corresponds to eGTADrawListType + 1
static bool s_RenderPhaseIsolateSkip = false; // skip instead of isolate
static bool s_RenderPhaseIsolateDisableOnly = false; // s_RenderPassDisable/s_RenderBucketDisable only affects s_RenderPhaseIsolate
static atArray<ConstString> s_RenderPassNames;
static atArray<ConstString> s_RenderBucketNames;
static u32 s_RenderPassDisable; // changed to u32 so it can be referenced externally
static atBitSet s_RenderBucketDisable;

u16 *fwRenderListBuilder::sm_InstanceCounts;
int fwRenderListBuilder::sm_ShadowMapDebugIndex;
int fwRenderListBuilder::sm_InstanceCountPageSize;
bool fwRenderListBuilder::sm_ShowInstanceCounts;
bool fwRenderListBuilder::ms_breakInAddToDrawListEntityListCore = false;

bool g_enablePedSortBias = true;
void PedSortBiasCallback()
{
	if (g_enablePedSortBias)
	{
		fwRenderListDesc::SetSortType(0, fwRenderListDesc::SORT_ASCENDING_PEDS_BIASED);
	}
	else
	{
		fwRenderListDesc::SetSortType(0, fwRenderListDesc::SORT_ASCENDING);
	}
}
#endif // __BANK

fwRenderListBuilder::FlushProtypesCallback *fwRenderListBuilder::ms_callback = NULL;
bool fwRenderListBuilder::ms_scissorExterior = false;

// Render mode mapping
struct DrawModeToRenderMode {
	fwRenderMode m_MappedRenderMode[RM_MAX];
#if __BANK
	const char *m_DebugName;
#endif // __BANK

	DrawModeToRenderMode() {
	}

	DrawModeToRenderMode(fwRenderMode normalRenderMode, fwRenderMode noFadingRenderMode, fwRenderMode forwardRenderMode, const char *BANK_ONLY(debugName)) {
		m_MappedRenderMode[0] = normalRenderMode;
		m_MappedRenderMode[1] = noFadingRenderMode;
		m_MappedRenderMode[2] = forwardRenderMode;
		CompileTimeAssert(RM_MAX == 3);
#if __BANK
		m_DebugName = debugName;
#endif // __BANK
	}
};

// Map a draw mode to a render mode
static atArray<DrawModeToRenderMode> s_DrawModeToRenderMode;

#define DEFAULT_ALPHA_THRESHOLD 4
#if __DEV
// Global alpha threshold.
static int s_DebugAlphaThreshold=DEFAULT_ALPHA_THRESHOLD;
#endif // __DEV

#if __BANK
static int s_NumRenderPasses;
static int s_NumRenderBuckets;
#endif // __BANK

fwRenderListBuilder::AddEntityFlags::AddEntityFlags() :
#if ENABLE_ADD_ENTITY_CALLBACKS
  addEntityCallback(NULL),
#endif // ENABLE_ADD_ENTITY_CALLBACKS
  renderStateSetup(NULL)
, renderStateCleanup(NULL)
, customFlagsChangedCallback(NULL)
, requiredBaseFlags(0)
, excludeBaseFlags(0)
, requiredRenderFlags(0)
, excludeRenderFlags(0)
, drawDataAddParams(NULL)
, useAlpha(false)
{
}

/** PURPOSE: Set information about a specific draw mode.
 *  NOTE that the render mode mapping is a temporary construct and will be made more flexible
 *  soon.

 *  PARAMS:
 *   drawMode - Draw mode to set information of.
 *   renderMode0 - Render mode to use for this draw mode if RM_NORMAL is used.
 *   renderMode1 - Render mode to use for this draw mode if RM_NO_FADE is used.
 *   renderMode2 - Render mode to use for this draw mode if RM_NOT_DEFERRED is used.
 *   name - Debug name for this draw mode.
 */
void fwRenderListBuilder::SetDrawModeInfo(fwDrawMode drawMode, fwRenderMode normalRenderMode, fwRenderMode noFadingRenderMode, fwRenderMode forwardRenderMode, const char *name)
{
	s_DrawModeToRenderMode[drawMode] = DrawModeToRenderMode(normalRenderMode, noFadingRenderMode, forwardRenderMode, name);
}

/** PURPOSE: This function takes a pass list object, goes through every single pass in it,
 *  evaluates the filters, and calls AddToDrawListEntityList if the pass manages to pass all
 *  filters. If subphasePass is not SUBPHASE_NONE, AddToDrawListEntityListShadow will be called instead.
 *
 *  For information on how the filters work, see fwDrawListAddPass.
 *
 * PARAMS:
 *  passList - An array of DrawListAddPass elements to evaluate. This object contains a list of passes
 *             that are executed in order.
 *  elementCount - Number of elements in the passList array.
 *  list - This is the index of the render list whose entities will be scanned and added if they are
 *         not rejected.
 *  filter - A bit mask of filters. The "filterMask" and "filterExcludeMask" members of the DrawListAddPass
 *           will check against this value filter out passes. If filter contains any bits of "filterExcludeMask",
 *           or if filter contains none of the bits in "filterMask", the pass will not be executed.
 *  drawMode - This is the draw mode to use for the pass. Using a look-up table (and the modifier "renderModeMapping"
 *             in the pass list entry), the draw mode will be converted to a render mode, which will be passed
 *             down to the AddToDrawListEntityList function.
 *  subphasePass - This is the shadow pass currently being executed, or SUBPHASE_NONE if this is not a shadow pass.
 */
int fwRenderListBuilder::ExecutePassList(fwDrawListAddPass *passList, int elementCount, s32 list, int filter, fwDrawMode drawMode, AddEntityListToDrawListCallback addEntityToDrawListCallback, int subphasePass RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStart) RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStride))
{
	int ret = 0;

	while (elementCount--) {
		// Does the draw mode match?
		// What about the filter? Getting non-0 here in either value indicates a missing flag.
		u32 includeTest = (filter & passList->filterMask) ^ passList->filterMask;
		u32 excludeTest = (filter & passList->filterExcludeMask);
		u32 drawModeTest = (drawMode & passList->drawModeMask) ^ drawMode;

		if (!(includeTest | excludeTest | drawModeTest)) {
			// We passed the test. Execute the list.
			// Find out how to map the draw mode that was passed in to a render mode.
			fwRenderMode renderMode = s_DrawModeToRenderMode[drawMode].m_MappedRenderMode[passList->renderModeMapping];

#if __BANK
			bool bExecute = true;

			// Check the debugging option to turn individual passes off.
			if ((s_RenderPassDisable & BIT(passList->pass)) != 0 || s_RenderBucketDisable.IsSet(passList->bucket))
			{
				if (!s_RenderPhaseIsolateDisableOnly || s_RenderPhaseIsolate == dlDrawListMgr::GetCurrDLType() + 1)
				{
					bExecute = false;
				}
			}
			else if (s_RenderPhaseIsolate != 0 && s_RenderPhaseIsolateSkip != (s_RenderPhaseIsolate != dlDrawListMgr::GetCurrDLType() + 1))
			{
				if (!s_RenderPhaseIsolateDisableOnly)
				{
					bExecute = false;
				}
			}

			if (bExecute)
#endif // __BANK
			{
				// Call the user-provided callback.
				ret += addEntityToDrawListCallback(list, passList->pass, passList->bucket, renderMode, subphasePass, passList->generalFlags RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(entityStart) RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(entityStride));
			}
		}

		// Move on to the next list.
		passList++;
	}
	return ret;
}


/** PURPOSE: Initialization for the render list builder system.
 *
 *  PARAMS:
 *   numRenderPasses - The total number of render passes that this game has.
 *   highestDrawModeFlag - The highest possible value for the draw mode.
 */
void fwRenderListBuilder::Init(int BANK_ONLY(numRenderPasses), int highestDrawModeFlag, int BANK_ONLY(highestRenderBucket))
{
#if __BANK
	s_NumRenderPasses = numRenderPasses;

	s_RenderPassNames.Resize(numRenderPasses);
//	s_RenderPassDisable.Init(numRenderPasses);

	s_NumRenderBuckets = highestRenderBucket+1;
	s_RenderBucketNames.Resize(s_NumRenderBuckets);
	s_RenderBucketDisable.Init(s_NumRenderBuckets);

	sm_InstanceCountPageSize = s_NumRenderBuckets * s_NumRenderPasses;
	sm_InstanceCounts = rage_new u16[sm_InstanceCountPageSize * (SUBPHASE_MAX+1)];
#endif // __BANK

	s_DrawModeToRenderMode.Resize(highestDrawModeFlag+1);
	sysMemSet(s_DrawModeToRenderMode.GetElements(), 0, sizeof(int) * (highestDrawModeFlag+1));
}

void fwRenderListBuilder::Shutdown()
{

}

/** PURPOSE: Returns the number of entities in the passed list.
 *	PARAM:
 *   list - The index of the entity list to query.
 *   id - The render pass to query.
*/
int fwRenderListBuilder::EntityListSize(s32 list, fwRenderPassId id)
{
	fwRenderListDesc& renderList = gRenderListGroup.GetRenderListForPhase(list);
	fwEntityList &entityList = renderList.GetList(id);
	return entityList.GetCount();
}

/** PURPOSE: This function will go through all the entities in a particular entity list,
 *  apply some filters to them, and call the draw handler of every entity that passes the test.
 *
 *  PARAMS:
 *   list - The index of the entity list to scan.
 *   id - The render pass to use.
 *   bucket - The bucket to use.
 *   renderMode - The render mode to use.
 *   subphasePass -
 *   addEntityFlags - This is a structure that contains information about how to filter the
 *                    entities, how to set up the render states, and more. See AddEntityFlags.
 */
int fwRenderListBuilder::AddToDrawListEntityListCore(s32 list, fwRenderPassId id, fwRenderBucket bucket, 
													  fwRenderMode renderMode, int subphasePass, const AddEntityFlags &addEntityFlags, fwRenderGeneralFlags generalFlags
													  RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entitySubListStart) RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entitySubListCount))
{
	bool renderStatesSetup = false;
	fwDrawDataAddParams *drawParams = addEntityFlags.drawDataAddParams;
	Assert(drawParams);

	drawParams->AlphaFade = 255;

	u32 requiredBaseFlags = addEntityFlags.requiredBaseFlags;
	u32 excludeBaseFlags = addEntityFlags.excludeBaseFlags;
	u32 requiredRenderFlags = addEntityFlags.requiredRenderFlags;
	u32 excludeRenderFlags = addEntityFlags.excludeRenderFlags;
	bool useAlpha = addEntityFlags.useAlpha;
#if ENABLE_ADD_ENTITY_CALLBACKS
	AddEntityCallback addEntityCallback = addEntityFlags.addEntityCallback;
#endif // ENABLE_ADD_ENTITY_CALLBACKS

	fwRenderListDesc& renderList = gRenderListGroup.GetRenderListForPhase(list);

	fwEntityList &entityList = renderList.GetList(id);
	int entityCount = entityList.GetCount();
	int renderedEntityCount = 0;

	u16 previousCustomFlags = 0;

	u32 earlyOut1 = ((entityList.GetBaseFlagsUnion() & requiredBaseFlags) ^ requiredBaseFlags);
	u32 earlyOut2 = (entityList.GetBaseFlagsIntersection() & excludeBaseFlags);
	u32 earlyOut3 = ((entityList.GetRenderFlagsUnion() & requiredRenderFlags) ^ requiredRenderFlags);
	u32 earlyOut4 = (entityList.GetRenderFlagsIntersection() & excludeRenderFlags);

	entityCount = (earlyOut1 | earlyOut2 | earlyOut3 | earlyOut4) ? 0 : entityCount;

	// Use a 3-pass lookahead prefetch on the entity to give the prefetch enough time to be effective.
	const s32 entityLookAheadForPrefetch = 3;
	const s32 prefetchLimit = entityCount - entityLookAheadForPrefetch;
	// Start the prefetch of the entity lists elements one cache line forward.
	const fwEntityList::CElement* elementStartWithCacheLineOffset = entityList.GetElements() + 8;

#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	s32 subListSize = entityCount / entitySubListCount;
	s32 startEntity = subListSize*entitySubListStart;
	s32 remainder = entityCount - subListSize*entitySubListCount;

	if(entitySubListStart == entitySubListCount-1)
		subListSize += remainder;

	for(s32 i=startEntity; i<(startEntity + subListSize); i++)
#else // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	for(s32 i=0; i<entityCount; i++) 
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	{
		// Prefetch the next cache line of elements inside the entity list.
		PrefetchDC(elementStartWithCacheLineOffset + i);
		if(Likely(i < prefetchLimit))
		{
			const fwEntity* pNextEntityToPrefetch = entityList.GetEntity(i+entityLookAheadForPrefetch);
			PrefetchDC(pNextEntityToPrefetch);
			PrefetchDC(pNextEntityToPrefetch + 1);
		}

		const u32 baseFlags = entityList.GetEntityBaseFlags(i);
		const u32 exFlagTest = baseFlags & excludeBaseFlags;	// If this is not 0, we should not draw since at least
		// one exclusion flag is set.
		const u32 inFlagTest = (baseFlags & requiredBaseFlags) ^ requiredBaseFlags;	// If this is not 0, we should not draw since
		// at least one required flag is not set.

		if (inFlagTest | exFlagTest) {
			// At least one flag check failed. Do not draw.
			continue;
		}

		const u32 renderFlags = entityList.GetEntityRenderFlags(i);
		u32 requireTest = 0;
		u32 excludeTest = (renderFlags & excludeRenderFlags);	// non-0 means illegal flag set

#if RAGE_INSTANCED_TECH
		u8 viewportInstancedRenderBit = 0;
#define INST_MASKING(ReRFlag, RFlag, NUM_INST, PHASE_OFFBIT) \
{ \
for (int i = 0; i < NUM_INST; i++) \
{ \
	u32 temp = ReRFlag; \
	temp |= BIT(PHASE_OFFBIT + i); \
	requireTest = (RFlag & temp) ^ temp; \
	if (requireTest == 0) \
		viewportInstancedRenderBit |= BIT(i); \
}\
if (viewportInstancedRenderBit != 0) \
	requireTest = 0; \
else \
	requireTest = 1; \
}

		if (sm_bInstancing)
		{
			if (subphasePass > SUBPHASE_NONE && subphasePass <= SUBPHASE_CASCADE_3)
				INST_MASKING(requiredRenderFlags,renderFlags,4,SUBPHASE_CASCADE_0)
			else if (subphasePass == SUBPHASE_REFLECT_FACET0)
				INST_MASKING(0,renderFlags,6,SUBPHASE_REFLECT_FACET0)
			else
				Assertf(0,"invalid subphase type.\n");
		}
		else
#endif
		{
			requireTest = (renderFlags & requiredRenderFlags) ^ requiredRenderFlags;	// non-0 means missing fla
		}

		if ((requireTest | excludeTest) == 0) {
			fwEntity* pEntity = entityList.GetEntity(i);

			fwDrawData* pDrawHandler = pEntity->GetDrawHandlerPtr();

			if (Unlikely(!pDrawHandler)){
				continue;
			}
			PrefetchDC(pDrawHandler);

#if __BANK
			// trace selected entity
			if (ms_breakInAddToDrawListEntityListCore && pEntity == g_PickerManager.GetSelectedEntity())
			{
				__debugbreak();
			}
#endif // __BANK

			if (!renderStatesSetup) {
				if (addEntityFlags.renderStateSetup) {
					addEntityFlags.renderStateSetup(id, bucket, renderMode, subphasePass, generalFlags);
				}

				renderStatesSetup = true;
			}

			if (useAlpha) {
				drawParams->AlphaFade = pEntity->GetAlpha();

				if (drawParams->AlphaFade < DEV_SWITCH(s_DebugAlphaThreshold, DEFAULT_ALPHA_THRESHOLD)) {
					continue;
				}
			}

#if ENABLE_ADD_ENTITY_CALLBACKS
			if (addEntityCallback) 
			{
				Assert(subphasePass<SUBPHASE_NONE);		// per entity call back are expensive in general, but not too bad for the point and hemisphere light per facet dynamic passes (the list are very small, since the vis pass cull all but the object into the light sphere)
				const float sortVal = entityList.GetSortVal(i);
				if (!addEntityCallback(id, bucket, renderMode, subphasePass, sortVal, pEntity)) 
				{
					continue;
				}
			}
#endif // ENABLE_ADD_ENTITY_CALLBACKS

#if __BANK
			// Keep track of this instance.
			int spPass = (subphasePass<=SUBPHASE_NONE) ? SUBPHASE_NONE : subphasePass;
			if (spPass >= SUBPHASE_NONE && spPass < SUBPHASE_MAX)
			{
				sm_InstanceCounts[(spPass+1) * sm_InstanceCountPageSize + bucket + (id * s_NumRenderBuckets)]++;
			}
			else
			{
				Assertf(0, "subphasePass is %d, expected -1 or [0..%d]", spPass, SUBPHASE_MAX - 1);
			}
#endif // __BANK

#if !__FINAL
			gDrawListMgr->IncrementDrawListEntityCount();
#endif // !__FINAL

#if ENTITY_GPU_TIME
#if ENTITY_GPU_TIME == 1
			bool bEntityGPUTime = false;
#endif // ENTITY_GPU_TIME == 1

			if (pEntity == dlCmdEntityGPUTimePush::GetEntity() BANK_ONLY(&& renderMode != _GTA5_rmDebugMode))
			{
#if ENTITY_GPU_TIME == 1
				bEntityGPUTime = true;
				DLC(dlCmdEntityGPUTimePush, ());
#elif ENTITY_GPU_TIME == 2
				if (dlCmdEntityGPUTimePush::GetFrame())
				{
					continue;
				}
#endif // ENTITY_GPU_TIME == 2
			}
#endif // ENTITY_GPU_TIME

			const u16 customFlags = entityList.GetEntityCustomFlags(i) & ~(fwEntityList::CUSTOM_FLAGS_PED_IN_VEHICLE | fwEntityList::CUSTOM_FLAGS_HD_TEX_CAPABLE);  // These bits aren't used for render state changes
			if (Unlikely(addEntityFlags.customFlagsChangedCallback && (previousCustomFlags != customFlags)))
			{
				addEntityFlags.customFlagsChangedCallback(id, bucket, renderMode, subphasePass, generalFlags, previousCustomFlags, customFlags);
			}
			previousCustomFlags = customFlags;

#if RAGE_INSTANCED_TECH
			if (sm_bInstancing)
				pEntity->SetviewportInstancedRenderBit(viewportInstancedRenderBit);
			else
				pEntity->SetviewportInstancedRenderBit(0xff);
#endif

			pDrawHandler->AddToDrawList(pEntity, drawParams);
			renderedEntityCount++;

#if !__FINAL
#if ENTITY_GPU_TIME == 1
			if (bEntityGPUTime)
			{
				DLC(dlCmdEntityGPUTimePop, ());
			}
#endif // ENTITY_GPU_TIME == 1
#endif // !__FINAL
		}
	}

	if (addEntityFlags.customFlagsChangedCallback && previousCustomFlags != 0)
	{
		addEntityFlags.customFlagsChangedCallback(id, bucket, renderMode, subphasePass, generalFlags, previousCustomFlags, 0);
	}

	if (renderStatesSetup && addEntityFlags.renderStateCleanup) {
		addEntityFlags.renderStateCleanup(id, bucket, renderMode, subphasePass, generalFlags);
	}

	return renderedEntityCount;
}

#if __BANK
/** PURPOSE: Add some of the widgets to the optimization bank
 */
void fwRenderListBuilder::AddOptimizationWidgets()
{
	bkBank* pOptimisationsBank = BANKMGR.FindBank("Optimization");
	if(pOptimisationsBank)
	{
		for (int x=0; x<s_NumRenderPasses; x++) {
			char debugName[64];
			if( s_RenderPassNames[x] == "Tree" )
			{
				formatf(debugName, "Disable %s bucket", s_RenderPassNames[x].c_str());
				pOptimisationsBank->AddToggle(debugName, &s_RenderPassDisable, BIT(x));
			}
		}

		for (int x=0; x<s_NumRenderBuckets; x++) {
			const char *bucketName = s_RenderBucketNames[x].c_str();

			if (bucketName)
			{
				char debugName[64];

				formatf(debugName, "Disable %s bucket", bucketName);
				pOptimisationsBank->AddToggle(debugName, &s_RenderBucketDisable, (unsigned char) x);
			}
		}
	}
}


/** PURPOSE: Add basic debugging widgets for the render list builder.
 */
void fwRenderListBuilder::AddWidgets(bkBank &bank)
{
	const int numDrawListTypes = gDrawListMgr->GetDrawListTypeCount();
	const char** drawListNames = rage_new const char*[numDrawListTypes + 1];

	for (int i = 0; i < numDrawListTypes; i++)
	{
		drawListNames[i + 1] = gDrawListMgr->GetDrawListName(i);
	}

	drawListNames[0] = "All";
	drawListNames[1] = "DL_RENDERPHASE";

	bank.AddCombo("Render Phase Isolate", &s_RenderPhaseIsolate, numDrawListTypes + 1, drawListNames);
	bank.AddToggle("Render Phase Skip (not isolate)", &s_RenderPhaseIsolateSkip);
	bank.AddToggle("Render Phase Isolate Disable Only", &s_RenderPhaseIsolateDisableOnly);

	delete[] drawListNames;

	bank.PushGroup("Render Pass Debugging");

	for (int x=0; x<s_NumRenderPasses; x++) {
		char debugName[64];

		formatf(debugName, "Disable %s", s_RenderPassNames[x].c_str());
		bank.AddToggle(debugName, &s_RenderPassDisable, BIT(x));
	}

	bank.AddToggle("Enabled Ped Sort Bias", &g_enablePedSortBias, datCallback(PedSortBiasCallback));
	bank.PopGroup();

	bank.PushGroup("Render Bucket Debugging");

	for (int x=0; x<s_NumRenderBuckets; x++) {
		const char *bucketName = s_RenderBucketNames[x].c_str();

		if (bucketName)
		{
			char debugName[64];

			formatf(debugName, "Disable %s", bucketName);
			bank.AddToggle(debugName, &s_RenderBucketDisable, (unsigned char) x);
		}
	}

	bank.PopGroup();

	bank.AddToggle("Display Instance Counts", &sm_ShowInstanceCounts);
	bank.AddSlider("Shadow Map To Debug", &sm_ShadowMapDebugIndex, 0, SUBPHASE_MAX, 1);

#if __DEV
	bank.AddSlider("Alpha Threshold", &s_DebugAlphaThreshold, 0, 255, 1);
#endif // __DEV
}

/** PURPOSE: Returns the total number of render passes.
 */
int fwRenderListBuilder::GetNumRenderPasses()
{
	return s_NumRenderPasses;
}

/** PURPOSE: Assign a name for debugging purposes to a render pass.
 *  The name will be copied to an internal buffer, so it's safe to pass a temporary
 *  string.
 *
 *  NOTE: Init() must have been called prior to calling this function.
 *
 *  PARAMS:
 *   pass - Render pass index to assign.
 *   name - Name for the render pass. Will be copied, so temporary strings are OK.
 */
void fwRenderListBuilder::SetRenderPassName(int pass, const char *name)
{
	s_RenderPassNames[pass] = name;
}

/** PURPOSE: Return a debug name for a render pass.
 *
 *  PARAMS:
 *    pass - Render pass index to get the name for.
 */
const char *fwRenderListBuilder::GetRenderPassName(int pass)
{
	return s_RenderPassNames[pass];
}

u32& fwRenderListBuilder::GetRenderPassDisableBits()
{
	return s_RenderPassDisable;
}

/** PURPOSE: Assign a name for debugging purposes to a render bucket.
 *  The name will be copied to an internal buffer, so it's safe to pass a temporary
 *  string.
 *
 *  NOTE: Init() must have been called prior to calling this function.
 *
 *  PARAMS:
 *   bucket - Render bucket index to assign.
 *   name - Name for the render pass. Will be copied, so temporary strings are OK.
 */
void fwRenderListBuilder::SetRenderBucketName(int bucket, const char *name)
{
	s_RenderBucketNames[bucket] = name;
}

void fwRenderListBuilder::ResetRenderStats()
{
	sysMemSet(sm_InstanceCounts, 0, sizeof(u16) * sm_InstanceCountPageSize * (SUBPHASE_MAX+1));
}

void fwRenderListBuilder::DebugDraw()
{
	if (sm_ShowInstanceCounts)
	{
		// Print header.
		char shadowMapName[32];

		if (sm_ShadowMapDebugIndex != 0)
		{
			formatf(shadowMapName, "Shadow Map %d", sm_ShadowMapDebugIndex - 1);
		}
		else
		{
			safecpy(shadowMapName, "Non-Shadows Rendering");
		}

		grcDebugDraw::AddDebugOutputEx(false, "Instance Counts for %s", shadowMapName);
		grcDebugDraw::AddDebugOutputEx(false, "");

		char line[256];

		// Get the pointer to the data.
		u16 *instanceCountCursor = sm_InstanceCounts;

		instanceCountCursor += sm_ShadowMapDebugIndex * sm_InstanceCountPageSize;

		line[0] = 0;

		// Print the name of each column.
		for (int x=0; x<s_NumRenderBuckets; x++)
		{
			if (s_RenderBucketNames[x].c_str())
			{
				char name[16];
				formatf(name, "%-10s", s_RenderBucketNames[x].c_str());
				safecat(line, name);
			}
		}

		grcDebugDraw::AddDebugOutputEx(false, "%20s%s", "", line);

		// And here we go.

		for (int x=0; x<s_NumRenderPasses; x++)
		{
			line[0] = 0;

			for (int y=0; y<s_NumRenderBuckets; y++)
			{
				if (s_RenderBucketNames[y].c_str())
				{
					char value[32];

					formatf(value, "%-10d", *instanceCountCursor);
					safecat(line, value);
				}

				instanceCountCursor++;
			}

			grcDebugDraw::AddDebugOutputEx(false, "%-24s%s", s_RenderPassNames[x].c_str(), line);
		}
	}
}

#endif // __BANK

} // namespace rage
