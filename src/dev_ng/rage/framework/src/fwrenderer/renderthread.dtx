@@fwRenderThreadInterface
<TITLE fwRenderThreadInterface Overview>
fwRenderThreadInterface is the main class that allows the main thread to interact with the render
thread. It also manages the creation and destruction of the thread itself.

There is always a global instance of fwRenderThreadInterface available through the global
gRenderThreadInterface object.
* Interface With Game-Specific Code *
A game has the option to create its own subclassed instance of fwRenderThreadGameInterface,
which override any of the provided callbacks. This should be done before the render thread
is created.
* Render Thread Creation *
The render thread is created by calling fwRenderThreadInterface::Init(). This will create the
render thread and start it.
* Render Thread Cycle *
The render thread allows for specifying a render thread function. This is a function that is called
every frame on the render thread to perform custom rendering jobs.

fwRenderThreadInterface provides a default render function that is typically used during normal
operation.

The function may be switched at any time using SetRenderFunction; the default function can be enabled
with SetDefaultRenderFunction.
* Interaction Between Update Thread And Render Thread *
During the period when the default render thread function is active, the two threads interact using
the following cycle:
<table>
<tr><td><b>Update Thread</b></td><td><b>Render Thread</b></td></tr>
<tr>
<td>
* <b>First frame:</b>
* Current State: UPDATE_PRE_DRAWLIST.
* Update Thread Begins to create draw lists.
* Current state: UPDATE_CREATING_DRAWLISTS.
* Draw lists are pushed onto a blocking queue as they finish.
  * <i>GetBuildDrawListQ().Push(pCurrBuildingList) in dlDrawListMgr::EndDrawList()</i>
* Terminate the draw list queue by pushing a NULL draw list.
  * <i>dlDrawListMgr::TerminateDrawLists()</i>
* Signal the update thread semaphore.
  * <i>m_DrawThreadParams.SignalUpdateDone()</i>
* Current state: UPDATE_WAITING_FOR_RT
* The update thread will do its regular update - scripts, logic, physics, etc.
* Wait for render thread to signal it's done.
</td>
<td>
* User-provided pre-draw callback (from game interface)
* The render thread picks up the draw lists as they are pushed on the queue and executed.
* Keep executing draw lists until a terminator (NULL pointer) is polled.
* User-provided post-draw callback (from game interface)
* Render thread will switch render function if update thread had requested to do so.
* Call <i>fwRenderThreadInterface::FlipRenderBuffer()</i>.
* Wait for the update thread to signal it's done.
  * <i>m_DrawThreadParams.WaitTillUpdateDone() in DefaultRenderFunction()
* Signal the render thread semaphore.
  * <i>m_DrawThreadParams.SignalRenderDone()</i>
</td>
</tr>
<tr>
<td>
  * <i>m_DrawThreadParams.WaitTillRenderDone()</i>
* Switch the buffer indices.
  * <i>dlDrawCommandBuffer::FlipBuffers()</i>
* <b>Safe Area</b> - Critical operations happen here, render thread is idle.
  * <i>fwRenderThreadInterface::Synchronise()</i>
* User-provided safe-mode operations
  * <i>PerformSafeModeOperations()</i>
</td>
<td>
* Render thread is idle.
</td>
</tr>
</table>

As soon as a custom render thread function is enabled, the pattern looks a bit different.
During this mode, the render thread is generally expected to operate on its own while the
update thread is under no obligation to synchronize data on a regular interval.

This is useful for loading screens where we have a smooth loading animation while the
update thread can take several seconds per iteration due to blocking loads and other
initialization code.

Here is the rough layout:

<table>
<tr><td><b>Update Thread</b></td><td><b>Render Thread</b></td></tr>
<tr>
<td>
* Update Thread is faffing about and doing its thing.
* Draw lists are pushed onto the draw list queue.
  * <i>GetBuildDrawListQ().Push(pCurrBuildingList) in dlDrawListMgr::EndDrawList()</i>
</td>
<td>
* Looping in <i>fwRenderThreadInterface::MainLoop()</i>.
* Switch render function if update thread had requested to do so.
* Poll to see if update thread has signaled the semaphore.
* Render thread executes the user-provided custom render function.
* No further action if update thread hasn't signalled yet.
* ...
* Switch render function if update thread had requested to do so.
* Poll to see if update thread has signaled the semaphore.
* Render thread executes the user-provided custom render function.
* No further action if update thread hasn't signalled yet.
* ...
</td>
</tr>
<tr>
<td>
* Eventually, the update thread will get to the main loop.
* Signal the update thread semaphore.
  * <i>m_DrawThreadParams.SignalUpdateDone()</i>
* Wait for render thread to signal it's done.
  * <i>m_DrawThreadParams.WaitTillRenderDone()</i>
* <i>fwRenderThreadInterface::Synchronise()</i>.
* User-provided safe-mode operations
  * <i>PerformSafeModeOperations()</i>
</td>
<td>
* Looping in <i>fwRenderThreadInterface::MainLoop()</i>.
* Poll to see if update thread has signaled the semaphore.
* If the signal has been set, poll and discard all draw lists.
  * <i>dlDrawListMgr::RemoveAll()</i>
* Signal the render thread semaphore.
  * <i>m_DrawThreadParams.SignalRenderDone()</i>
* Render thread executes the user-provided custom render function.
* No further action if update thread hasn't signalled yet.
* ...
* Switch render function if update thread had requested to do so.
* Poll to see if update thread has signaled the semaphore.
* Render thread executes the user-provided custom render function.
* No further action if update thread hasn't signalled yet.
* ...
</td>
</tr>
</table>

* Flushing *
It is possible to call fwRenderThreadInterface::Flush() from within the update
thread if there are no draw lists currently being built.

Flushing will perform four page flips in rapid succession. The sequence for each of
those four iterations in the update thread is as follows:

* Get end fence handle, block on it and clear it.
* Remove all references.
* Reset forced technique.
* Flip buffers, which will add a NULL draw list.
* Signal that update thread is done.
* Wait for render thread to indicate it's done.

* Fence And Reference Counting *
The render thread holds a triple-buffered list of GPU fence handles to ensure that
references are not released until the GPU is done working the objects in question.

The fence is created on the render thread through the dlCmdInsertEndFence
command, which the update thread should add after all rendering has been done (GTA-specific: This
is done in CSystem::AddDrawList_EndRender).

The update thread can call fwRenderThreadInterface::GPU_IdleSection() to wait for
the proper fence, i.e. the one that was created two frames ago. GPU_IdleSelection
will automatically call dlDrawListMgr::RemoveAllRefs, which will decrement the
reference counts on all related objects.

The order of operations with the fence is as follows:
<table>
<tr><td><b>Update Thread</b></td><td><b>Render Thread</b></td></tr>
<tr>
<td>
* Block for fence 0, if there is one.
* Begin creating draw list B for the next frame.
* As there are textures and models being used during the draw list
  creation, they will be added to the texture/model reference list 0.
* Wait for render thread to finish rendering.
</td>
<td>
* Render the previously generated draw list A.
* One of the last commands of draw list A is to set the fence 2.
* Signal that render thread is done.
</td>
</tr>
<tr>
<td>
* Flip the buffer indices.
* Block for fence 1, if there is one.
* Begin creating draw list C for the next frame.
* As there are textures and models being used during the draw list
  creation, they will be added to the texture/model reference list 1.
* Wait for the render thread to finish rendering draw list B.
</td>
<td>
* Render the draw list B.
* One of the last commands of draw list B is to set fence 0.
* Signal that the render thread is done.
</td>
</tr>
<tr>
<td>
* Flip the buffer indices.
* Block for fence 2, if there is one (note how that was inserted
  two frames ago via draw list A).
* Begin creating draw list D for the next frame.
* As there are textures and models being used during the draw list
  creation, they will be added to the texture/model reference list 2.
* Wait for the render thread to finish rendering draw list C.
</td>
<td>
* Render the draw list C.
* One of the last commands of draw list C is to set fence 1.
* Signal that the render thread is done.
</td>
</tr>
</table>

