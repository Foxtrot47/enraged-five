
//**********************************
// RenderSettingsBase.h
// (C) Rockstar North Ltd.
// description:	Hi level renderer - abstracts rendering into a platform independent set of phases.
// Started - 11/2005 - Derek Ward
//**********************************
// History
// Created 11/2009 - John Whyte
//**********************************

#ifndef INC_RENDERSETTINGSBASE_H_
#define INC_RENDERSETTINGSBASE_H_

namespace rage
{
//-------------------------------------------------------------
// Settings describing what/how is desired to be rendered
// Rendersettings presently operate on a multi tier basis where 
// CViewportManager settings can override CViewport settings
// which in turn can override RenderPhase settings.
class CRenderSettingsBase
{
public:
	CRenderSettingsBase() { m_flags = 0; m_LODScaleValue = 1.0f; }
	~CRenderSettingsBase() {}
	
	u32			GetFlags() const					{ return m_flags;			}
	void		SetFlags(u32 f)						{ m_flags = f;				}
	const u32*	GetFlagsPtr() const					{ return &m_flags;			}
	u32*		GetFlagsPtr() 						{ return &m_flags;			}
	u32			ExcludeFlags(u32 f)					{ return (m_flags &= (~f));	}
	u32			IncludeFlags(u32 f)					{ return (m_flags |= f);	}
	u32			ToggleFlags(u32 f)					{ return (m_flags ^= f);	}	
	u32			QueryFlags(u32 f) const				{ return (m_flags & f);		} // No need for a host of access functions simply use this.
	bool		bQueryFlags(u32 f) const			{ return (m_flags & f) != 0; }
	float		GetLODScale(void) const				{ return m_LODScaleValue;	}
	void		SetLODScale(float LODScale)			{ m_LODScaleValue = LODScale;}
	float*		GetLODScalePtr(void)				{ return(&m_LODScaleValue); }

	CRenderSettingsBase CombineWith(CRenderSettingsBase setting) const
	{
		CRenderSettingsBase ret;
		ret.SetFlags(GetFlags()&setting.GetFlags());
		return ret;
	}

protected:
	u32		 m_flags;
	float	m_LODScaleValue;
};

} // namespace rage

#endif //INC_RENDERSETTINGSBASE_H_

