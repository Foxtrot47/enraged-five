﻿//**********************************
// RenderPhase.cpp 
// (C) Rockstar North Ltd.
// Started - 11/2009 - John Whyte
// Attempt at creating a base class for renderphases than we can derive proper, working renderphases from in a sensible way
//**********************************
#include "fwrenderer/RenderPhaseBase.h"

#include "entity/entity.h"
#include "entity/archetypemanager.h"
#include "grcore/debugdraw.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwrenderer/renderlistbuilder.h"
#include "fwrenderer/renderlistgroup.h"
#include "system/param.h"

namespace rage
{

#if __BANK
static bool s_ShowScanResults = false;
static bool s_ShowScanParameters = false;
static int s_PhaseBeingDebugged = -1;
static int s_PassBeingDebugged = 0;

u32 fwRenderPhase::sm_RenderSettingsOverride = ~0U;
#endif // __BANK


fwRenderPhase::fwRenderPhase(const grcViewport *viewport, const char *NOTFINAL_ONLY(name), int drawListType, float
#if !__FINAL || __FINAL_LOGGING
	updateBudget
#endif
	)
: m_DrawListType(drawListType)
, m_bDisabled(false)
, m_bActive(false)
, m_EntityListIndex(INVALID_RENDERPHASE_ID)
, m_lodMask(LODMASK_NONE)
, m_BuildDrawListEnabled(true)
#if RSG_PC
, m_FrameCountTillDisable(0)
#endif
#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS && __ASSERT
, m_startDrawListType(0)
, m_endDrawListType(0)
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS && __ASSERT
#if __BANK
, m_WidgetGroup(NULL)
#endif
{
	if (viewport)
	{
		m_GrcViewport = *viewport;
	}

	m_entityVisibilityMask.SetAllFlags();

#if !__FINAL
	SetName(name);
#endif // !__FINAL

#if !__FINAL || __FINAL_LOGGING
	m_UpdateBudget = updateBudget;
#endif // !__FINAL || __FINAL_LOGGING
}

#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
fwRenderPhase::fwRenderPhase(const grcViewport *viewport, const char *NOTFINAL_ONLY(name), int drawListType, float 
#if !__FINAL || __FINAL_LOGGING
	updateBudget
#endif
	, int ASSERT_ONLY(startDrawListType), int ASSERT_ONLY(endDrawListType))
: m_DrawListType(drawListType)
, m_bDisabled(false)
, m_bActive(false)
, m_EntityListIndex(INVALID_RENDERPHASE_ID)
, m_lodMask(LODMASK_NONE)
, m_BuildDrawListEnabled(true)
#if RSG_PC
, m_FrameCountTillDisable(0)
#endif
#if __ASSERT
, m_startDrawListType(startDrawListType)
, m_endDrawListType(endDrawListType)
#endif // __ASSERT
#if __BANK
, m_WidgetGroup(NULL)
#endif
{
	Assertf((startDrawListType != 0 && endDrawListType != 0), "fwRenderPhase::fwRenderPhase()...Can`t have draw list types of zero.");
	if (viewport)
	{
		m_GrcViewport = *viewport;
	}

	m_entityVisibilityMask.SetAllFlags();

#if !__FINAL
	SetName(name);
#endif // !__FINAL

#if !__FINAL || __FINAL_LOGGING
	m_UpdateBudget = updateBudget;
#endif // !__FINAL || __FINAL_LOGGING
}
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS

fwRenderPhase::~fwRenderPhase() {
#if __BANK
	RENDERPHASEMGR.RemoveRenderPhaseWidget(m_WidgetGroup);
#endif
}

#define NEEDS_DEBUG_DRAW_BATCHER_HANDLING   (DEBUG_DRAW)

#if NEEDS_DEBUG_DRAW_BATCHER_HANDLING
static grcDebugDraw::BatcherHandle s_batcherHandles[grcDebugDraw::MAX_LINGER_BUFFERS];
#endif // NEEDS_DEBUG_DRAW_BATCHER_HANDLING

static void DebugDrawPrologue()
{
#if NEEDS_DEBUG_DRAW_BATCHER_HANDLING
	for (int i=0; i<grcDebugDraw::MAX_LINGER_BUFFERS; ++i)
	{
		const auto buffer = (grcDebugDraw::LingerBuffer)i;
		if (grcDebugDraw::IsExpectingSubRenderThreadDebugDraw(buffer))
		{
			const grcDebugDraw::BatcherHandle batcherHandle = grcDebugDraw::AllocateSubRenderBatcher(buffer);
			s_batcherHandles[buffer] = batcherHandle;
			DLC_Add(grcDebugDraw::SetSubRenderBatcher, buffer, batcherHandle);
		}
	}
#endif // NEEDS_DEBUG_DRAW_BATCHER_HANDLING
}

#if NEEDS_DEBUG_DRAW_BATCHER_HANDLING

struct ForcedCleanupCallbackData
{
	grcDebugDraw::LingerBuffer  buffer;
	grcDebugDraw::BatcherHandle batcherHandle;
};

static void ForcedCleanupCallback(void *data_)
{
	auto *data = (ForcedCleanupCallbackData*)data_;
	grcDebugDraw::ClearSubRenderBatcher2(data->buffer, data->batcherHandle);
}

#endif // NEEDS_DEBUG_DRAW_BATCHER_HANDLING

static void DebugDrawEpilogue()
{
#if NEEDS_DEBUG_DRAW_BATCHER_HANDLING
	for (int i=0; i<grcDebugDraw::MAX_LINGER_BUFFERS; ++i)
	{
		const auto buffer = (grcDebugDraw::LingerBuffer)i;
		if (grcDebugDraw::IsExpectingSubRenderThreadDebugDraw(buffer))
		{
			DLC_Add(grcDebugDraw::ClearSubRenderBatcher, buffer);
			auto *data = (ForcedCleanupCallbackData*)(gDrawListMgr->AddForcedCleanupCallback(ForcedCleanupCallback, sizeof(ForcedCleanupCallbackData)));
			data->buffer = buffer;
			data->batcherHandle = s_batcherHandles[buffer];
		}
	}
	DLC_Add(grcDebugDraw::ClearRenderState);
#endif // NEEDS_DEBUG_DRAW_BATCHER_HANDLING
}

void fwRenderPhase::DrawListPrologue()
{
#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	Assertf((m_startDrawListType == 0) && (m_endDrawListType == 0), "fwRenderPhase::DrawListPrologue()...Renderphase outputs mutliple draw lists.");
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	PF_PUSH_TIMEBAR_BUDGETED(gDrawListMgr->GetDrawListName(m_DrawListType), m_UpdateBudget);
	DLC(dlCmdNewDrawList, (m_DrawListType));
	DebugDrawPrologue();
}

void fwRenderPhase::DrawListEpilogue()
{
	DebugDrawEpilogue();
#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	Assertf((m_startDrawListType == 0) && (m_endDrawListType == 0), "fwRenderPhase::DrawListPrologue()...Renderphase outputs mutliple draw lists.");
#endif // RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS
	gDrawListMgr->EndCurrentDrawList();
	PF_POP_TIMEBAR();
}


#if RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS

void fwRenderPhase::DrawListPrologue(int listType)
{
	Assertf((listType >= m_startDrawListType) && (listType <= m_endDrawListType), "fwRenderPhase::DrawListPrologue()...Draw list type out if range.");
	// Accumulate to the same "master" list type as before.
	PF_PUSH_TIMEBAR_BUDGETED(gDrawListMgr->GetDrawListName(m_DrawListType), m_UpdateBudget);
	// Start a list of the passed type.
	DLC(dlCmdNewDrawList, (listType));
	DebugDrawPrologue();
}

void fwRenderPhase::DrawListEpilogue(int ASSERT_ONLY(listType))
{
	DebugDrawEpilogue();
	Assertf((listType >= m_startDrawListType) && (listType <= m_endDrawListType), "fwRenderPhase::DrawListPrologue()...Draw list type out if range.");
	// End a draw list as usual.
	gDrawListMgr->EndCurrentDrawList();
	PF_POP_TIMEBAR();
}

#endif //RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS


fwRenderPhaseManager::fwRenderPhaseManager(int maxRenderPhases)
#if __BANK
: m_BankGroup(NULL)
#endif // __BANK
{

#if __ASSERT
	m_EnableEntityListCheck = true;
#endif

	m_RenderPhases.Reserve(maxRenderPhases);
}

#if __ASSERT
void fwRenderPhaseManager::SetEntityListSanityCheck(bool bEnabled)
{
	m_EnableEntityListCheck = bEnabled;
}
#endif

void fwRenderPhaseManager::BuildRenderList()
{
	int count = m_RenderPhases.GetCount();

	for (int x=0; x<count; x++)
	{
		fwRenderPhase &phase = *m_RenderPhases[x];

		if (!phase.IsDisabled())
		{
			phase.BuildRenderList();
		}

#if __ASSERT
		// there are certain cases (e.g.: screenshot taking) where we need to disable one or several render phases (but *not* all of them) 
		// that share the same entity list; in these cases, the asserts below will trigger even though everything's fine.
		if (m_EnableEntityListCheck)
		{
			if ( phase.HasEntityListIndex() && gRenderListGroup.GetRenderListForPhase( phase.GetEntityListIndex() ).IsInitRenderListBuildCalled() )
				Assertf( phase.IsActive(), "InitRenderListBuild called on render phase %s but phase is not active", phase.GetName() );
			else
				Assertf( !phase.IsActive(), "InitRenderListBuild NOT called on render phase %s but phase is active", phase.GetName() );
		}
#endif
	}
}

void fwRenderPhaseManager::BuildDrawList()
{
	int count = m_RenderPhases.GetCount();

	for (int x=0; x<count; x++)
	{
		fwRenderPhase &phase = *m_RenderPhases[x];

		bool bBuildDrawListEnabled = phase.IsBuildDrawListEnabled();
		if (!phase.IsDisabled() && bBuildDrawListEnabled)
		{
			phase.BuildDrawList();
		}
	}
}

void fwRenderPhaseManager::UpdateViewports()
{
	int count = m_RenderPhases.GetCount();

	for (int x=0; x<count; x++)
	{
		fwRenderPhase &phase = *m_RenderPhases[x];

		phase.SetActive(false);

		if (!phase.IsDisabled())
		{
			phase.UpdateViewport();
		}
	}
}


#if __BANK

#if __DEV
bool g_ReverseSort = false;
bool g_IgnoreDrawSortFlags = false;
#endif // __DEV

void fwRenderPhaseManager::AddWidgets(bkGroup &bank)
{
	m_BankGroup = bank.AddGroup("Render Phases");
#if __DEV
	m_BankGroup->AddToggle("Reverse Sort", &g_ReverseSort);
	m_BankGroup->AddToggle("Ignore Draw Sort Flags", &g_IgnoreDrawSortFlags);
	m_BankGroup->AddSeparator();
#endif // __DEV
	m_BankGroup->AddSlider("Phase To Debug", &s_PhaseBeingDebugged, -1, 64, 1);
	m_BankGroup->AddSlider("Pass To Debug", &s_PassBeingDebugged, 0, 64, 1);
	m_BankGroup->AddToggle("Show Phase's Scan Parameters", &s_ShowScanParameters);
	bkGroup *scanGroup = m_BankGroup->AddGroup("Scan Results");
	scanGroup->AddToggle("Show Scan Results", &s_ShowScanResults);

	// Add all render phases that were registered so far
	int count = m_RenderPhases.GetCount();

	for (int x=0; x<count; x++)
	{
		AddRenderPhaseToWidgets(*m_RenderPhases[x]);
	}
}

void fwRenderPhaseManager::AddRenderPhaseToWidgets(fwRenderPhase &renderPhase)
{
	// Don't add it if the widgets haven't been created yet. The call to AddWidgets() later
	// is going to take care of everything that has been registered but not added to the widgets
	// yet, so we're fine.
	if (!m_BankGroup)
	{
		return;
	}

	bkGroup *group = m_BankGroup->AddGroup(renderPhase.GetName());
	group->AddToggle("Disable", renderPhase.GetDisabledPtr());
	renderPhase.RegisterGroup(group);
	renderPhase.AddWidgets(*group);
}

void fwRenderPhaseManager::RemoveRenderPhaseWidget(bkGroup *removeGroup)
{
	if (!m_BankGroup || !removeGroup)
	{
		return;
	}

	m_BankGroup->Remove(*removeGroup);
}

void fwRenderPhaseManager::PrintLine(bool toScreen, const char *fmt, ...) const
{
	char finalLine[256];

	va_list list;
	va_start(list, fmt);
	vsprintf(finalLine, fmt, list);

	if (toScreen)
	{
		grcDebugDraw::AddDebugOutputEx(false, finalLine);
	}
	else
	{
		Displayf("%s", finalLine);
	}

	va_end(list);
}

void fwRenderPhaseManager::ShowScanResults(bool toScreen) const
{
	int count = gRenderListGroup.GetNumRenderPhasesRegistered();
	int passCount = fwRenderListBuilder::GetNumRenderPasses();

	// Create the table header.
	char header[256];
	header[0] = 0;

	for (int pass=0; pass<passCount; pass++)
	{
		char passName[7];
		char paddedName[8];

		safecpy(passName, fwRenderListBuilder::GetRenderPassName(pass));

		formatf(paddedName, "%-6s ", passName);
		safecat(header, paddedName);
	}

	PrintLine(toScreen, "%-38s%s", "", header);


	for (int x=0; x<count; x++)
	{
		fwRenderListDesc& listDesc = gRenderListGroup.GetRenderListForPhase(x);

		char passes[256];

		passes[0] = 0;

		for (int pass=0; pass<passCount; pass++)
		{
			char singleLine[16];
			formatf(singleLine, "%-6d ", listDesc.GetNumberOfEntities(pass));
			safecat(passes, singleLine);
		}

		const char *name = "";
		const char *activeString = " ";

		fwRenderPhase *rp = listDesc.GetRenderPhaseNew();
		if (rp)
		{
			name = rp->GetName();

			if (rp->IsActive())
			{
				activeString = "*";
			}
		}

		PrintLine(toScreen, "%sPhase %-2d (%-25s): %s", activeString, x, name, passes);
	}

	if (s_PhaseBeingDebugged >= 0 && s_PhaseBeingDebugged < gRenderListGroup.GetNumRenderPhasesRegistered())
	{
		fwRenderListDesc& listDesc = gRenderListGroup.GetRenderListForPhase(s_PhaseBeingDebugged);

		if (s_PassBeingDebugged < fwRenderListBuilder::GetNumRenderPasses())
		{
			fwEntityList &list = listDesc.GetList(s_PassBeingDebugged);

			int count = list.GetCount();

			if (count > 0)
			{
				fwEntityList::CElement *element = list.GetElements();

				PrintLine(toScreen, "%-20s %-10s %-10s %s", "Entity Model", "Sort Val", "Base Flags", "Render Flags");

				for (int x=0; x<count; x++)
				{
					const char *name;

					if (fwEntity *entity = element->m_pEntity)
					{
						name = (entity->GetModelIndex() != fwModelId::MI_INVALID) ? element->m_pEntity->GetModelName() : "[invalid archetype]";
					}
					else
					{
						name = "[no entity]";
					}

					PrintLine(toScreen, "%-20s %-10.2f %010x %010x", name, element->m_sortVal, element->m_BaseFlags, element->m_RenderFlags);
					element++;
				}
			}			
		}
	}
}

bool fwRenderPhaseManager::HasDebugDraw() const
{
	return s_ShowScanResults || s_ShowScanParameters;
}

void fwRenderPhaseManager::DebugDraw()
{
	if (s_ShowScanResults)
	{
		gRenderListGroup.WaitForSortEntityListJob();
		ShowScanResults(true);
	}

	if (s_ShowScanParameters)
	{
		if (s_PhaseBeingDebugged >= 0 && s_PhaseBeingDebugged < gRenderListGroup.GetNumRenderPhasesRegistered())
		{
			fwRenderListDesc& listDesc = gRenderListGroup.GetRenderListForPhase(s_PhaseBeingDebugged);
			const grcViewport *viewport = NULL;
			u32 renderFlags = 0;
			Vec3V lodCamPos, lodCamDir;
			
			fwRenderPhase *phase = listDesc.GetRenderPhaseNew();

			if (phase)
			{
				fwRenderPhaseScanner *scanner = phase->GetScanner();
				
				if (Verifyf(scanner, "Render phase with attached list doesn't have a scanner"))
				{
					viewport = &phase->GetGrcViewport();
					renderFlags = scanner->GetRenderFlags();
					lodCamPos = RCC_VEC3V(scanner->GetCameraPositionForFade());
					lodCamDir = RCC_VEC3V(scanner->GetCameraDirection());
				}
			}

			if (viewport)
			{
				Vec3V camPos = viewport->GetCameraMtx().GetCol3();
				Vec3V lookAt = viewport->GetCameraMtx().GetCol1();

				grcDebugDraw::AddDebugOutput("Phase viewport position: %.2f %.2f %.2f, look at: %.2f %.2f %.2f",
					camPos.GetXf(), camPos.GetYf(), camPos.GetZf(),
					lookAt.GetXf(), lookAt.GetYf(), lookAt.GetZf());
				grcDebugDraw::AddDebugOutput("Render Flags: %08x", renderFlags);
				grcDebugDraw::AddDebugOutput("LOD Camera Position: %.2f %.2f %.2f", lodCamPos.GetXf(), lodCamPos.GetYf(), lodCamPos.GetZf());
				grcDebugDraw::AddDebugOutput("LOD Camera Direction: %.2f %.2f %.2f", lodCamDir.GetXf(), lodCamDir.GetYf(), lodCamDir.GetZf());
			}
		}
	}
}
#endif // __BANK


void fwRenderPhaseManager::RegisterRenderPhase(fwRenderPhase &renderPhase)
{
	Assertf(m_RenderPhases.Find(&renderPhase) == -1, "Renderphase '%s' registered twice", renderPhase.GetName());
	m_RenderPhases.Append() = &renderPhase;

#if __BANK
	AddRenderPhaseToWidgets(renderPhase);
#endif // __BANK
}



fwRenderPhaseScanner::fwRenderPhaseScanner()
: m_LodViewport(NULL)
, m_RenderFlags(0)
{
}


} //namespace rage
