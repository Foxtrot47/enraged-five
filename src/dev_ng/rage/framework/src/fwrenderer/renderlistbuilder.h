#ifndef _FWRENDERER_RENDERLISTBUILDER_H_
#define _FWRENDERER_RENDERLISTBUILDER_H_

#include "fwrenderer/renderlistgroup.h"
#include "fwrenderer/renderphaseswitches.h"

namespace rage {

class bkBank;
class fwDrawDataAddParams;
class fwEntity;
struct fwDrawListAddPass;

#define ENABLE_ADD_ENTITY_SHADOW_CALLBACK (0)
#define ENABLE_ADD_ENTITY_LOCALSHADOW_CALLBACK (1)
#define ENABLE_ADD_ENTITY_ALPHA_CALLBACK (1)
#define ENABLE_ADD_ENTITY_GBUFFER_CALLBACK (0)
#define ENABLE_ADD_ENTITY_CALLBACKS (ENABLE_ADD_ENTITY_GBUFFER_CALLBACK || ENABLE_ADD_ENTITY_SHADOW_CALLBACK || ENABLE_ADD_ENTITY_LOCALSHADOW_CALLBACK || ENABLE_ADD_ENTITY_ALPHA_CALLBACK)

#define ENABLE_REFLECTION_PER_FACET_CULLING 1

enum fwRenderModeMapping {
	RM_NORMAL		= 0,
	RM_NO_FADE		= 1,	// Use a "NoFade" render mode if applicable
	RM_NOT_DEFERRED = 2,	// Use a forward-rendering render mode
	RM_MAX			= 3,
};

enum fwRenderSubphase {
	SUBPHASE_ALPHA_HYBRID = -6,
	SUBPHASE_ALPHA_INTERIOR = -5,
	SUBPHASE_PARABOLOID_STATIC_AND_DYNAMIC  = -4,
	SUBPHASE_PARABOLOID_STATIC_ONLY  = -3,
	SUBPHASE_PARABOLOID_DYNAMIC_ONLY = -2,
	SUBPHASE_NONE                    = -1,
	SUBPHASE_CASCADE_0               = 0,
	SUBPHASE_CASCADE_1               = 1,
	SUBPHASE_CASCADE_2               = 2,
	SUBPHASE_CASCADE_3               = 3,
	SUBPHASE_REFLECT_UPPER_LOLOD     = 4,
	SUBPHASE_REFLECT_LOWER_LOLOD     = 5,
	SUBPHASE_REFLECT_UPPER_HILOD     = 6,
	SUBPHASE_REFLECT_LOWER_HILOD     = 7,

	SUBPHASE_REFLECT_FACET0    = 4,
	SUBPHASE_REFLECT_FACET1    = 5,
	SUBPHASE_REFLECT_FACET2    = 6,
	SUBPHASE_REFLECT_FACET3    = 7,

	SUBPHASE_REFLECT_FACET4    = 8,
	SUBPHASE_REFLECT_FACET5    = 9,

	SUBPHASE_MAX                     = 10, // Hard limit on number of subphase passes


	SUBPHASE_CASCADE_MASK  = BIT(SUBPHASE_CASCADE_0) | BIT(SUBPHASE_CASCADE_1) | BIT(SUBPHASE_CASCADE_2) | BIT(SUBPHASE_CASCADE_3),
	SUBPHASE_CASCADE_COUNT = 4,

	SUBPHASE_REFLECT_MASK  = BIT(SUBPHASE_REFLECT_UPPER_LOLOD) | BIT(SUBPHASE_REFLECT_LOWER_LOLOD) | BIT(SUBPHASE_REFLECT_UPPER_HILOD) | BIT(SUBPHASE_REFLECT_LOWER_HILOD),
#if !ENABLE_REFLECTION_PER_FACET_CULLING
	SUBPHASE_REFLECT_COUNT = 4,
#endif

	SUBPHASE_REFLECT_FACET_MASK  =	BIT(SUBPHASE_REFLECT_FACET0) | 
									BIT(SUBPHASE_REFLECT_FACET1) | 
									BIT(SUBPHASE_REFLECT_FACET2) | 
									BIT(SUBPHASE_REFLECT_FACET3) |
									BIT(SUBPHASE_REFLECT_FACET4) |
									BIT(SUBPHASE_REFLECT_FACET5),
#if ENABLE_REFLECTION_PER_FACET_CULLING
									SUBPHASE_REFLECT_COUNT = 6
#endif
};

enum {
	_GTA5_rmDebugMode = 0x0400,
};

/** PURPOSE: An array of fwDrawListAddPass objects is being passed to ExecutePassList, which
    will go through them one by one, filter them, and call a callback of choice if the entry
	passes the tests. The tests are:

	drawModeMask - The drawMode passed into ExecutePassList must have at least one bit set that
	               is in drawModeMask.
    filterMask - The filter value passed into ExecutePassList must have all bits set that are
	             set in filterMask.
    filterExcludeMask - The filter value passed into ExecutePassList must not have any bit set
	                    that is set in filterExcludeMask.
*/
struct fwDrawListAddPass {
	// This is the render pass this row is representing. If the test succeeds, this value
	// will be passed into the callback function.
	fwRenderPassId pass;

	// This is the render bucket this row is representing. If the test succeeds, this value
	// will be passed into the callback function.
	fwRenderBucket bucket;

	// The drawMode passed into ExecutePassList must have at least one bit set that is in drawModeMask.
	u32 drawModeMask;

	// The filter value passed into ExecutePassList must have all bits set that are set in filterMask.
	u32 filterMask;

	// The filter value passed into ExecutePassList must not have any bit set that is set in filterExcludeMask.
	u32 filterExcludeMask;

	// The mapping from the drawMode passed into ExecutePassList to the render mode.
	fwRenderModeMapping renderModeMapping;

	// General Flags for controlling each pass
	fwRenderGeneralFlags generalFlags;
};


class fwRenderListBuilder
{
public:
#if ENABLE_ADD_ENTITY_CALLBACKS
	typedef bool (*AddEntityCallback)(fwRenderPassId id, fwRenderBucket bucket, fwRenderMode renderMode, int subphasePass, float sortVal, fwEntity *entity);
#endif // ENABLE_ADD_ENTITY_CALLBACKS
	typedef void (*RenderStateCallback)(fwRenderPassId id, fwRenderBucket bucket, fwRenderMode renderMode, int subphasePass, fwRenderGeneralFlags generalFlags);
	typedef int (*AddEntityListToDrawListCallback)(s32 list, fwRenderPassId id, fwRenderBucket bucket, fwRenderMode renderMode, int subphasePass, fwRenderGeneralFlags generalFlags RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStart) RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStride));
	typedef void (*CustomFlagsChangedCallback)(fwRenderPassId id, fwRenderBucket bucket, fwRenderMode renderMode, int subphasePass, fwRenderGeneralFlags generalFlags, u16 previousCustomFlags, u16 currentCustomFlags);

	/** PURPOSE: Helper struct for AddToDrawListEntityListCore that contains instructions on
	 *  how to filter entities, and other information.
	 */
	struct AddEntityFlags {
		AddEntityFlags();

#if ENABLE_ADD_ENTITY_CALLBACKS
		// This callback will be called for every entity that passes the test.
		AddEntityCallback addEntityCallback;
#endif // ENABLE_ADD_ENTITY_CALLBACKS

		// This callback will be called before the first entity that is being added. If no
		// entities pass the test, this callback will not be called.
		RenderStateCallback renderStateSetup;

		// This callback will be called after all entities have been added, IF the
		// render state setup callback had been called earlier.
		RenderStateCallback renderStateCleanup;

		// This callback will be called when the custom flags for the previous entity differ
		// from the current entity
		CustomFlagsChangedCallback customFlagsChangedCallback;

		// Parameters that will be passed down to AddToDrawList
		fwDrawDataAddParams *drawDataAddParams;

		u32 requiredBaseFlags;			// All of these base flags must be set, or the entity won't draw
		u32 excludeBaseFlags;			// Either of those base flags will disqualify the entity
		u32 requiredRenderFlags;		// All of these render flags must be set, or the entity won't draw
		u32 excludeRenderFlags;			// Either of those render flags will disqualify the entity
		bool useAlpha;					// If true, the entity's alpha value will be used
	};

public:
	typedef int fwDrawFilter;
	typedef int fwDrawMode;

	static void Init(int numRenderPasses, int highestDrawModeFlag, int highestRenderPass);

	static int ExecutePassList(fwDrawListAddPass *passList, int elementCount, s32 list, int filter, fwDrawMode drawMode, AddEntityListToDrawListCallback addEntityCallback, int subphasePass RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStart = 0) RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStride = 1));

	static int EntityListSize(s32 list, fwRenderPassId id);
	static int AddToDrawListEntityListCore(s32 list, fwRenderPassId id, fwRenderBucket bucket, fwRenderMode renderMode, int subphasePass, const AddEntityFlags &addEntityFlags, fwRenderGeneralFlags generalFlags RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStart = 0) RENDER_PHASE_SUPPORT_MULIPLE_DRAW_LISTS_ONLY_PARAM(int entityStride = 1));

	static void SetDrawModeInfo(fwDrawMode drawMode, fwRenderMode normalRenderMode, fwRenderMode noFadingRenderMode, fwRenderMode forwardRenderMode, const char *name);

#if RAGE_INSTANCED_TECH
	static void SetInstancingMode(bool bInstancing)	{ sm_bInstancing = bInstancing; }
	static bool GetInstancingMode() { return sm_bInstancing; }
#endif

	static void Shutdown();

	typedef void (FlushProtypesCallback)(bool forceAllocateNewBufer);
	static void SetFlushPrototypesCallback(FlushProtypesCallback callback) { ms_callback = callback; }
	static void EnableScissoringExterior(bool enable) { ms_scissorExterior = enable;}
#if __BANK
	static void DebugDraw();

	static void ResetRenderStats();

	static void	AddOptimizationWidgets();
	static void AddWidgets(bkBank &bank);
	static void SetRenderPassName(int pass, const char *name);
	static void SetRenderBucketName(int bucket, const char *name);

	static int GetNumRenderPasses();
	static const char *GetRenderPassName(int pass);

	static u32& GetRenderPassDisableBits();
#endif

private:
#if RAGE_INSTANCED_TECH
	static DECLARE_MTR_THREAD bool sm_bInstancing;
#endif
#if __BANK
	// Three-Dimensional array of number of entities rendered for each subphase, bucket, pass.
	static u16 *sm_InstanceCounts;

	// Number of u16s per 2D in the InstanceCount 3D array
	static int sm_InstanceCountPageSize;

	// Index of the shadow map being analyzed in the debug draw
	static int sm_ShadowMapDebugIndex;

	static bool sm_ShowInstanceCounts;
public:
	static bool ms_breakInAddToDrawListEntityListCore;
#endif // __BANK
	static FlushProtypesCallback *ms_callback;
	static bool ms_scissorExterior;
};

} // namespace rage

#endif // !defined _RENDERER_RENDERLISTBUILDERNEW_H_
