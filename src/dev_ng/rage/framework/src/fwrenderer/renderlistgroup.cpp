// Title	:	RenderListGroup.h
// Authors	:	John Whyte, Alessandro Piva
// Started	:	31/07/2006

//////////////
// Includes //
//////////////

#include "renderlistgroup.h"

#include "system/dependencyscheduler.h"

#include "renderListSortJob.h"
#include "renderPhaseBase.h"

#include "entity/entity.h"
#include "fwsys/gameskeleton.h"

#include <algorithm>

namespace rage {

/////////////
// Globals //
/////////////

fwRenderListGroup	gRenderListGroup;

atArray<fwRenderPhaseInfo> gRenderPhases;
atArray<fwRenderListDesc::eRenderListSort> fwRenderListDesc::sm_listSort;

static fwSortEntityListSpuData s_sortEntityListSpuData ALIGNED(128);

sysDependency g_RenderListSortDependency[NUM_RENDER_LIST_SORT_JOBS];
u32	g_RenderListSortDependenciesRunning;
bool g_RenderListSortInProgress = false;

/////////////////////////
// fwEntityList methods //
/////////////////////////
void fwEntityList::AddEntity(fwEntity* pEntity, u32 baseFlags, u16 renderFlags, u32 subphaseVisFlags, float sortVal, u16 flags)
{
	// We should not be adding entities while the sort job is pending
	Assert(!fwRenderListGroup::IsSortInProgress());

	CElement element;
	element.m_pEntity = pEntity;
	element.m_sortVal = sortVal;
	element.m_BaseFlags = baseFlags;
	element.m_RenderFlags = renderFlags;

	Assert((subphaseVisFlags          & ~fwEntity::fwRenderFlag_SUBPHASE_MASK) == 0); // Need to allocate more bits in fwRenderFlag_SUBPHASE_MASK
	Assert((pEntity->GetRenderFlags() &  fwEntity::fwRenderFlag_SUBPHASE_MASK) == 0); // render flags collision with subphase mask

	element.m_RenderFlags |= (u16)subphaseVisFlags;
	element.m_CustomFlags = flags;

	UpdateListFlags(element);
	m_entityArray.PushAndGrow(element);
}

#if __DEV
extern bool g_ReverseSort;
extern bool g_IgnoreDrawSortFlags;
#endif // __DEV

/////////////////////////////
// fwRenderListDesc methods //
/////////////////////////////

void fwRenderListDesc::Reset()
{
	SetRenderPhase( (fwRenderPhase *) NULL );
}

void fwRenderListDesc::Init(unsigned initMode, int renderPassCount)
{
    if(initMode == INIT_CORE || initMode == INIT_AFTER_MAP_LOADED)
    {
        Reset();
    }

	u32 flags = 0;
	for(int i = 0; i < NUM_RENDER_LIST_SORT_JOBS; i++)
	{
		g_RenderListSortDependency[i].Init( RenderListSortAsync::RunFromDependency, 0, flags );
		g_RenderListSortDependency[i].m_Priority = sysDependency::kPriorityMed;
	}

	m_entityLists.Resize(renderPassCount);
}

void fwRenderListDesc::Shutdown(unsigned shutdownMode)
{
    if(shutdownMode == SHUTDOWN_CORE)
    {
		int count = m_entityLists.GetCount();
	    for(s32 i=0; i<count; i++)
		    m_entityLists[i].Shutdown();
    }
    else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
    {
        Reset();
    }
}

void fwRenderListDesc::InitRenderListBuild()
{
	ASSERT_ONLY( m_initRenderListBuildCalled = true; )

	int count = m_entityLists.GetCount();

	for(s32 i=0; i<count; i++)
		m_entityLists[i].Clear();
}

// name:		fwRenderListDesc::PostRenderListBuild
// description:	Once render list is built, place post processes in here

void fwRenderListDesc::PostRenderListBuild() {
	
	int count = m_entityLists.GetCount();
	for (int x=0; x<count; x++)
	{
		Assertf(sm_listSort[x] != SORT_NOTSET,"sorting not set on RL %d",x);
		SortList(x);
	}
}

void fwRenderListDesc::SortList(int renderPassId)
{
#if SORT_RENDERLISTS_ON_SPU
	if(m_entityLists[renderPassId].GetCount() >= 2)
	{
		int listIndex = s_sortEntityListSpuData.m_ListCount;
		if(AssertVerify(listIndex < MAX_RENDER_LISTS_TO_SORT))
		{
			s_sortEntityListSpuData.m_aElementPtr[listIndex] = m_entityLists[renderPassId].GetElements();
			s_sortEntityListSpuData.m_aElementCount[listIndex] = m_entityLists[renderPassId].GetCount();
			s_sortEntityListSpuData.m_aSortType[listIndex] = sm_listSort[renderPassId];
			s_sortEntityListSpuData.m_ListCount++;
		}
	}
#else
	OldSortList(renderPassId);
#endif
}

//////////////////////////////////////////////////////////////////////////
#if !SORT_RENDERLISTS_ON_SPU

static bool g_UsePartitionSort = true;

void fwRenderListDesc::OldSortList(int renderPassId)
{
	switch (sm_listSort[renderPassId]) {
	case SORT_ASCENDING:
		m_entityLists[renderPassId].OldSortAscending(0, m_entityLists[renderPassId].GetCount());
		break;

	case SORT_DESCENDING:
		m_entityLists[renderPassId].OldSortDescending(0, m_entityLists[renderPassId].GetCount());
		break;

	case SORT_NOTSET:
		Assertf(sm_listSort[renderPassId] != SORT_NOTSET,"sorting not set on RL %d",renderPassId);
		break;

	case SORT_ASCENDING_PEDS_BIASED:
		m_entityLists[renderPassId].OldSortAscendingPedsBiased(0, m_entityLists[renderPassId].GetCount());
		break;

	default:
		// SORT_NONE, presumably.
		m_entityLists[renderPassId].OldSortDrawLast(0, m_entityLists[renderPassId].GetCount());		// still need to sort by draw last flag
		break;
	}
}

void fwEntityList::OldSortAscending(s32 start, s32 end)
{
	fwEntityList::ListIterator sItor = m_entityArray.begin() + start;
	fwEntityList::ListIterator eItor = m_entityArray.begin() + end;

	if (g_UsePartitionSort)
	{
#if __DEV
		if (g_IgnoreDrawSortFlags)
		{
			std::sort(sItor, eItor, CElement::SortAscendingSimpleRef);
		}
		else
#endif // __DEV
		{
			fwEntityList::ListIterator startExterior			 = std::partition(sItor, eItor,							    rage::fwEntityList::CElement::SortInterior);
			fwEntityList::ListIterator startNotDrawFirstInterior = std::partition(sItor, startExterior,					    rage::fwEntityList::CElement::ShouldDrawFirst);
			fwEntityList::ListIterator startDrawLastInterior	 = std::partition(startNotDrawFirstInterior, startExterior, rage::fwEntityList::CElement::ShouldNotDrawLast);

			fwEntityList::ListIterator startNotDrawFirstExterior = std::partition(startExterior, eItor,			    rage::fwEntityList::CElement::ShouldDrawFirst);
			fwEntityList::ListIterator startDrawLastExterior	 = std::partition(startNotDrawFirstExterior, eItor, rage::fwEntityList::CElement::ShouldNotDrawLast);

			std::sort(sItor,					 startNotDrawFirstInterior, rage::fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startNotDrawFirstInterior, startDrawLastInterior,     rage::fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startDrawLastInterior,     startExterior,		        rage::fwEntityList::CElement::SortAscendingSimpleRef);

			std::sort(startExterior,			 startNotDrawFirstExterior, rage::fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startNotDrawFirstExterior, startDrawLastExterior,		rage::fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startDrawLastExterior,	 eItor,						rage::fwEntityList::CElement::SortAscendingSimpleRef);
		}
	}
	else
	{
		std::sort(sItor, eItor, CElement::SortAscendingRef);
	}

#if __DEV
	if (g_ReverseSort)
	{
		end--;
		while (start < end)
		{
			std::swap(m_entityArray[start], m_entityArray[end]);
			start++;
			end--;
		}
	}
#endif // __DEV
}

void fwEntityList::OldSortDescending(s32 start, s32 end)
{
	fwEntityList::ListIterator sItor = m_entityArray.begin() + start;
	fwEntityList::ListIterator eItor = m_entityArray.begin() + end;

	if (g_UsePartitionSort)
	{
#if __DEV
		if (g_IgnoreDrawSortFlags)
		{
			std::sort(sItor, eItor, CElement::SortDescendingSimpleRef);
		}
		else
#endif // __DEV
		{
			fwEntityList::ListIterator startExterior			 = std::partition(sItor, eItor,							    rage::fwEntityList::CElement::SortInterior);
			fwEntityList::ListIterator startNotDrawFirstInterior = std::partition(sItor, startExterior,					    rage::fwEntityList::CElement::ShouldDrawFirst);
			fwEntityList::ListIterator startDrawLastInterior	 = std::partition(startNotDrawFirstInterior, startExterior, rage::fwEntityList::CElement::ShouldNotDrawLast);

			fwEntityList::ListIterator startNotDrawFirstExterior = std::partition(startExterior, eItor,			    rage::fwEntityList::CElement::ShouldDrawFirst);
			fwEntityList::ListIterator startDrawLastExterior	 = std::partition(startNotDrawFirstExterior, eItor, rage::fwEntityList::CElement::ShouldNotDrawLast);

			std::sort(sItor,					 startNotDrawFirstInterior, rage::fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startNotDrawFirstInterior, startDrawLastInterior,     rage::fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startDrawLastInterior,     startExterior,		        rage::fwEntityList::CElement::SortDescendingSimpleRef);

			std::sort(startExterior,			 startNotDrawFirstExterior, rage::fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startNotDrawFirstExterior, startDrawLastExterior,		rage::fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startDrawLastExterior,	 eItor,						rage::fwEntityList::CElement::SortDescendingSimpleRef);
		}
	}
	else
	{
		std::sort(sItor, eItor, CElement::SortDescendingRef);
	}

#if __DEV
	if (g_ReverseSort)
	{
		end--;
		while (start < end)
		{
			std::swap(m_entityArray[start], m_entityArray[end]);
			start++;
			end--;
		}
	}
#endif // __DEV
}

// make sure that entities with drawlast flag set are pushed to back of array
void fwEntityList::OldSortDrawLast(s32 start, s32 end)
{
	if (start >= end-1){
		return;
	}

	// pick up any entries tagged as drawlast and push them to the back
	s32 curEntry = end-1;
	s32 swapIdx = curEntry;
	do{
		if (GetEntityBaseFlags(curEntry) & fwEntity::DRAW_LAST){
			rage::fwEntityList::CElement temp = m_entityArray[swapIdx];
			m_entityArray[swapIdx] = m_entityArray[curEntry];
			m_entityArray[curEntry] = temp;
			swapIdx--;
		}
		curEntry--;
	} while (curEntry > start);
}

void fwEntityList::OldSortAscendingPedsBiased(s32 start, s32 end)
{
	fwEntityList::ListIterator sItor = m_entityArray.begin() + start;
	fwEntityList::ListIterator eItor = m_entityArray.begin() + end;

	if (g_UsePartitionSort)
	{
#if __DEV
		if (g_IgnoreDrawSortFlags)
		{
			std::sort(sItor, eItor, CElement::SortAscendingSimpleRefPedBias);
		}
		else
#endif // __DEV
		{
			fwEntityList::ListIterator startExterior			 = std::partition(sItor, eItor,							    rage::fwEntityList::CElement::SortInterior);
			fwEntityList::ListIterator startNotDrawFirstInterior = std::partition(sItor, startExterior,		 			    rage::fwEntityList::CElement::ShouldDrawFirst);
			fwEntityList::ListIterator startDrawLastInterior	 = std::partition(startNotDrawFirstInterior, startExterior, rage::fwEntityList::CElement::ShouldNotDrawLast);

			fwEntityList::ListIterator startNotDrawFirstExterior = std::partition(startExterior, eItor,				rage::fwEntityList::CElement::ShouldDrawFirst);
			fwEntityList::ListIterator startDrawLastExterior	 = std::partition(startNotDrawFirstExterior, eItor, rage::fwEntityList::CElement::ShouldNotDrawLast);

			std::sort(sItor,					 startNotDrawFirstInterior, rage::fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startNotDrawFirstInterior, startDrawLastInterior,     rage::fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startDrawLastInterior,     startExterior,		        rage::fwEntityList::CElement::SortAscendingSimpleRefPedBias);

			std::sort(startExterior,			 startNotDrawFirstExterior, rage::fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startNotDrawFirstExterior, startDrawLastExterior,		rage::fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startDrawLastExterior,	 eItor,						rage::fwEntityList::CElement::SortAscendingSimpleRefPedBias);
		}
	}
	else
	{
		std::sort(sItor, eItor, CElement::SortAscendingSimpleRefPedBias);
	}

#if __DEV
	if (g_ReverseSort)
	{
		end--;
		while (start < end)
		{
			std::swap(m_entityArray[start], m_entityArray[end]);
			start++;
			end--;
		}
	}
#endif // __DEV
}
#endif	//!SORT_RENDERLISTS_ON_SPU
//////////////////////////////////////////////////////////////////////////

void fwEntityPreRenderList::SortAndRemoveDuplicates()
{
	if ( GetCount() == 0 )
		return;

	std::sort( begin(), end() );
	const int count = ptrdiff_t_to_int(std::unique( begin(), end() ) - begin());

	Resize( count );
}

void fwEntityPreRenderList::RemoveElements(fwEntityPreRenderList& other)
{
	if ( GetCount() == 0 || other.GetCount() == 0 )
		return;

	fwEntity** resultMem = Alloca(fwEntity*, GetCount());
	atUserArray<fwEntity*> result(resultMem, (u16)GetCount());

	const int count = ptrdiff_t_to_int(std::set_difference( begin(), end(), other.begin(), other.end(), result.begin() ) - result.begin());
	result.Resize( count );
	Resize( count );
	std::copy( result.begin(), result.end(), begin() );
}

void fwRenderListDesc::Clear()
{
	ASSERT_ONLY( m_initRenderListBuildCalled = false; )

	int count = m_entityLists.GetCount();
	for(s32 i=0; i<count; i++)
		m_entityLists[i].Clear();
}

void fwRenderListDesc::InitSortArray(int renderPassCount) {
	sm_listSort.Resize(renderPassCount);
	sysMemSet(sm_listSort.GetElements(), 0, sizeof(eRenderListSort) * renderPassCount);
}

//////////////////////////////
// fwRenderListGroup methods //
//////////////////////////////

void fwRenderListGroup::Init(unsigned initMode, int renderPhaseCount, int renderPhaseTypeCount, int renderPassCount)
{
	Assertf(m_RenderList.GetCount() == 0, "Render list group being initialized twice");

	fwRenderListDesc::InitSortArray(renderPassCount);
	m_MaxRenderPhaseCount = renderPhaseCount;
	m_RenderPhaseTypeCount = renderPhaseTypeCount;

	m_RenderList.Resize(renderPhaseCount);
	m_RenderPhaseTypeCounts.Resize(renderPhaseTypeCount);

	gRenderPhases.Resize(renderPhaseTypeCount);

	m_numBitsUsed = 0;

    if(initMode == INIT_CORE || initMode == INIT_AFTER_MAP_LOADED)
    {
	    for(s32 i = 0; i < renderPhaseCount; i++)
		    m_RenderList[i].Init(initMode, renderPassCount);

        if(initMode == INIT_CORE)
        {
			m_AlreadyPreRenderedList.Init(512);
	        m_PreRenderList.Init(512);
			m_PreRender2List.Init(512);
        }

	    ResetRegisteredRenderPhases();
    }

	m_ThreadAccessMask = sysThreadType::THREAD_TYPE_UPDATE;
}

void fwRenderListGroup::Shutdown(unsigned shutdownMode)
{
    if(shutdownMode == SHUTDOWN_CORE || shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
    {
	    for(s32 i = 0; i < m_MaxRenderPhaseCount; i++)
		    m_RenderList[i].Shutdown(shutdownMode);
	    
        if(shutdownMode == SHUTDOWN_CORE)
        {
			m_AlreadyPreRenderedList.Shutdown();
            m_PreRenderList.Shutdown();
			m_PreRender2List.Shutdown();
        }

	    ResetRegisteredRenderPhases();
    }

	m_RenderPhaseTypeCounts.Reset();
	m_RenderList.Reset();
}

void fwRenderListGroup::Clear()
{
	for(s32 i = 0; i < m_MaxRenderPhaseCount; i++)
		m_RenderList[i].Clear();
	
	m_AlreadyPreRenderedList.Clear();
	m_PreRenderList.Clear();
	m_PreRender2List.Clear();
}

s32 fwRenderListGroup::RegisterRenderPhase(const fwRenderPhaseType type, fwRenderPhase* const pRP)
{
	FastAssert( sysThreadType::IsUpdateThread() );

	fwRenderPhaseInfo &renderPhase = gRenderPhases[type];

	FastAssert( m_RenderPhaseTypeCounts[type] < renderPhase.m_MaxCount );
	FastAssert( m_NumRenderPhases < (u32) m_MaxRenderPhaseCount );

	s32	id = renderPhase.m_StartBit + m_RenderPhaseTypeCounts[type];
	m_RenderPhaseTypeCounts[type]++;

	m_NumRenderPhases++;

	GetRenderListForPhase( id ).SetRenderPhase( pRP );
	m_RenderPhases.PushAndGrow( pRP );

	return id;
}

void fwRenderListGroup::ClearRenderPhasePointersNew()
{
	int count = gRenderListGroup.GetNumRenderPhasesRegistered();

	for (int x=0; x<count; x++)
	{
		GetRenderListForPhase(x).SetRenderPhase((fwRenderPhase *) NULL);
	}
}

void fwRenderListGroup::StartSortEntityListJob()
{
	Assert(!g_RenderListSortInProgress);
	if(s_sortEntityListSpuData.m_ListCount == 0)
	{
		return;
	}

	g_RenderListSortInProgress = true;

	int numLists = s_sortEntityListSpuData.m_ListCount;
	int numListsPerJob = numLists / NUM_RENDER_LIST_SORT_JOBS;

	if(numListsPerJob)
	{
		g_RenderListSortDependenciesRunning = NUM_RENDER_LIST_SORT_JOBS;

		for(int i = 0; i < NUM_RENDER_LIST_SORT_JOBS; i++)
		{

			g_RenderListSortDependency[i].m_Params[0].m_AsPtr = &s_sortEntityListSpuData;
			g_RenderListSortDependency[i].m_Params[1].m_AsInt = i*numListsPerJob;
			g_RenderListSortDependency[i].m_Params[2].m_AsInt = (i == NUM_RENDER_LIST_SORT_JOBS-1) ? numLists : (i+1)*numListsPerJob;
			g_RenderListSortDependency[i].m_Params[3].m_AsPtr = &g_RenderListSortDependenciesRunning;

			sysDependencyScheduler::Insert( &g_RenderListSortDependency[i] );
		}
	}
	else
	{
		g_RenderListSortDependency[0].m_Params[0].m_AsPtr = &s_sortEntityListSpuData;
		g_RenderListSortDependency[0].m_Params[1].m_AsInt = 0;
		g_RenderListSortDependency[0].m_Params[2].m_AsInt = numLists;
		g_RenderListSortDependency[0].m_Params[3].m_AsPtr = &g_RenderListSortDependenciesRunning;

		g_RenderListSortDependenciesRunning = 1;

		sysDependencyScheduler::Insert( &g_RenderListSortDependency[0] );
	}
}

void fwRenderListGroup::WaitForSortEntityListJob()
{
	PF_PUSH_TIMEBAR_IDLE("Wait For Sort Entity List Jobs");

	while(true)
	{
		volatile u32 *pDependenciesRunning = &g_RenderListSortDependenciesRunning;
		if(*pDependenciesRunning == 0)
		{
			break;
		}

		sysIpcYield(PRIO_NORMAL);
	}

	PF_POP_TIMEBAR();

	g_RenderListSortInProgress = false;
	s_sortEntityListSpuData.m_ListCount = 0;
}

bool fwRenderListGroup::IsSortInProgress()
{
	return g_RenderListSortInProgress;
}

void fwRenderListGroup::ResetRegisteredRenderPhases()
{
	FastAssert(sysThreadType::IsUpdateThread());
	m_NumRenderPhases = 0;
	memset( m_RenderPhaseTypeCounts.GetElements(), 0, sizeof(u32) * m_RenderPhaseTypeCount );
	m_RenderPhases.Resize(0);
}

void fwRenderListGroup::SetRenderPhaseTypeInfo(const fwRenderPhaseType renderPhaseType, const u32 maxCount)
{
	fwRenderPhaseInfo &renderPhaseInfo = gRenderPhases[ renderPhaseType ];

	Assertf((m_numBitsUsed+maxCount)<=24, "No room for %d bits for RP type %d", maxCount, renderPhaseType);

	renderPhaseInfo.m_StartBit = m_numBitsUsed;
	renderPhaseInfo.m_MaxCount = maxCount;
	renderPhaseInfo.m_Masks = (( 1 << maxCount ) - 1 ) << m_numBitsUsed;

	m_numBitsUsed += maxCount;

	//Displayf("Type %d Start %d MaxCount %d, Mask 0x%x", renderPhaseType, renderPhaseInfo.m_StartBit, renderPhaseInfo.m_MaxCount, renderPhaseInfo.m_Masks);
}

void fwRenderListGroup::GetRenderPhaseTypeInfo(const fwRenderPhaseType renderPhaseType, u32* startBit, u32* maxCount, u32* masks)
{
	const fwRenderPhaseInfo		&renderPhaseInfo = gRenderPhases[ renderPhaseType ];

	*startBit = renderPhaseInfo.m_StartBit;
	*maxCount = renderPhaseInfo.m_MaxCount;
	*masks = renderPhaseInfo.m_Masks;
}

u32 fwRenderListGroup::GetRenderPhaseTypeMask(const fwRenderPhaseType renderPhaseType) {
	return gRenderPhases[ renderPhaseType ].m_Masks;
}

} // namespace rage
