// RenderListSortJob
// jlandry: 25/10/2011

#include "fwrenderer/renderListSortJob.h"

#include "fwrenderer/renderlistgroup.h"

namespace rage {

static bool g_UsePartitionSort = true;
#if __DEV
extern bool g_IgnoreDrawSortFlags;
extern bool g_ReverseSort;
#endif

typedef fwEntityList::CElement* iterator;
void SortAscending(fwEntityList::CElement *pElements, int count)
{
	iterator sItor = pElements;
	iterator eItor = pElements+count;

	if (g_UsePartitionSort)
	{
#if __DEV
		if (g_IgnoreDrawSortFlags)
		{
			std::sort(sItor, eItor, fwEntityList::CElement::SortAscendingSimpleRef);
		}
		else
#endif // __DEV
		{
			iterator startExterior			   = std::partition(sItor, eItor,							  fwEntityList::CElement::SortInterior);
			iterator startNotDrawFirstInterior = std::partition(sItor, startExterior,					  fwEntityList::CElement::ShouldDrawFirst);
			iterator startDrawLastInterior	   = std::partition(startNotDrawFirstInterior, startExterior, fwEntityList::CElement::ShouldNotDrawLast);

			iterator startNotDrawFirstExterior = std::partition(startExterior, eItor,			  fwEntityList::CElement::ShouldDrawFirst);
			iterator startDrawLastExterior	   = std::partition(startNotDrawFirstExterior, eItor, fwEntityList::CElement::ShouldNotDrawLast);

			std::sort(sItor,					 startNotDrawFirstInterior, fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startNotDrawFirstInterior, startDrawLastInterior,     fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startDrawLastInterior,     startExterior,		        fwEntityList::CElement::SortAscendingSimpleRef);

			std::sort(startExterior,			 startNotDrawFirstExterior, fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startNotDrawFirstExterior, startDrawLastExterior,		fwEntityList::CElement::SortAscendingSimpleRef);
			std::sort(startDrawLastExterior,	 eItor,						fwEntityList::CElement::SortAscendingSimpleRef);
		}
	}
	else
	{
		std::sort(sItor, eItor, fwEntityList::CElement::SortAscendingRef);
	}

#if __DEV
	if (g_ReverseSort)
	{
		u32 start = 0;
		u32 end = count-1;
		while (start < end)
		{
			std::swap(pElements[start], pElements[end]);
			start++;
			end--;
		}
	}
#endif // __DEV
}

void SortDescending(fwEntityList::CElement *pElements, int count)
{
	iterator sItor = pElements;
	iterator eItor = pElements + count;

	if (g_UsePartitionSort)
	{
#if __DEV
		if (g_IgnoreDrawSortFlags)
		{
			std::sort(sItor, eItor, fwEntityList::CElement::SortDescendingSimpleRef);
		}
		else
#endif // __DEV
		{
			iterator startExterior			   = std::partition(sItor, eItor,							  fwEntityList::CElement::SortInterior);
			iterator startNotDrawFirstInterior = std::partition(sItor, startExterior,					  fwEntityList::CElement::ShouldDrawFirst);
			iterator startDrawLastInterior	   = std::partition(startNotDrawFirstInterior, startExterior, fwEntityList::CElement::ShouldNotDrawLast);

			iterator startNotDrawFirstExterior = std::partition(startExterior, eItor,			  fwEntityList::CElement::ShouldDrawFirst);
			iterator startDrawLastExterior	   = std::partition(startNotDrawFirstExterior, eItor, fwEntityList::CElement::ShouldNotDrawLast);

			std::sort(sItor,					 startNotDrawFirstInterior, fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startNotDrawFirstInterior, startDrawLastInterior,     fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startDrawLastInterior,     startExterior,		        fwEntityList::CElement::SortDescendingSimpleRef);

			std::sort(startExterior,			 startNotDrawFirstExterior, fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startNotDrawFirstExterior, startDrawLastExterior,		fwEntityList::CElement::SortDescendingSimpleRef);
			std::sort(startDrawLastExterior,	 eItor,						fwEntityList::CElement::SortDescendingSimpleRef);
		}
	}
	else
	{
		std::sort(sItor, eItor, fwEntityList::CElement::SortDescendingRef);
	}

#if __DEV
	if (g_ReverseSort)
	{
		u32 start = 0;
		u32 end = count-1;
		while (start < end)
		{
			std::swap(pElements[start], pElements[end]);
			start++;
			end--;
		}
	}
#endif // __DEV
}

// make sure that entities with drawlast flag set are pushed to back of array
void SortDrawLast(fwEntityList::CElement *pElements, int count)
{
	s32 start = 0;
	s32 end = count;
	if (start >= end-1){
		return;
	}

	// pick up any entries tagged as drawlast and push them to the back
	s32 curEntry = end-1;
	s32 swapIdx = curEntry;
	do{
		if (pElements[curEntry].m_BaseFlags & fwEntity::DRAW_LAST){
			fwEntityList::CElement temp = pElements[swapIdx];
			pElements[swapIdx] = pElements[curEntry];
			pElements[curEntry] = temp;
			swapIdx--;
		}
		curEntry--;
	} while (curEntry > start);
}

// B*644704 Make peds in vehilces render after the vehicle
void SortAscendingPedsBiased(fwEntityList::CElement *pElements, int count)
{
	iterator sItor = pElements;
	iterator eItor = pElements+count;

	if (g_UsePartitionSort)
	{
#if __DEV
		if (g_IgnoreDrawSortFlags)
		{
			std::sort(sItor, eItor, fwEntityList::CElement::SortAscendingSimpleRefPedBias);
		}
		else
#endif // __DEV
		{
			iterator startExterior			   = std::partition(sItor, eItor,							  fwEntityList::CElement::SortInterior);
			iterator startNotDrawFirstInterior = std::partition(sItor, startExterior,					  fwEntityList::CElement::ShouldDrawFirst);
			iterator startDrawLastInterior	   = std::partition(startNotDrawFirstInterior, startExterior, fwEntityList::CElement::ShouldNotDrawLast);

			iterator startNotDrawFirstExterior = std::partition(startExterior, eItor,			  fwEntityList::CElement::ShouldDrawFirst);
			iterator startDrawLastExterior	   = std::partition(startNotDrawFirstExterior, eItor, fwEntityList::CElement::ShouldNotDrawLast);

			std::sort(sItor,					 startNotDrawFirstInterior, fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startNotDrawFirstInterior, startDrawLastInterior,     fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startDrawLastInterior,     startExterior,		        fwEntityList::CElement::SortAscendingSimpleRefPedBias);

			std::sort(startExterior,			 startNotDrawFirstExterior, fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startNotDrawFirstExterior, startDrawLastExterior,		fwEntityList::CElement::SortAscendingSimpleRefPedBias);
			std::sort(startDrawLastExterior,	 eItor,						fwEntityList::CElement::SortAscendingSimpleRefPedBias);
		}
	}
	else
	{
		std::sort(sItor, eItor, fwEntityList::CElement::SortAscendingSimpleRefPedBias);
	}

#if __DEV
	if (g_ReverseSort)
	{
		u32 start = 0;
		u32 end = count-1;
		while (start < end)
		{
			std::swap(pElements[start], pElements[end]);
			start++;
			end--;
		}
	}
#endif // __DEV
}

bool RenderListSortAsync::RunFromDependency(const sysDependency& dependency)
{
	const fwSortEntityListSpuData* pData = static_cast<const fwSortEntityListSpuData*>(dependency.m_Params[0].m_AsPtr);
	int startIndex = dependency.m_Params[1].m_AsInt;
	int endIndex = dependency.m_Params[2].m_AsInt;
	u32* pDependenciesRunningEa	= static_cast< u32* >( dependency.m_Params[3].m_AsPtr );

	for(int i = startIndex; i < endIndex; i++)
	{
		fwRenderListDesc::eRenderListSort sortType = pData->m_aSortType[i];
		int elementCount = pData->m_aElementCount[i];

		fwEntityList::CElement *pElements = pData->m_aElementPtr[i];
		
		switch(sortType)
		{
			case fwRenderListDesc::SORT_ASCENDING:
				SortAscending(pElements, elementCount);
				break;

			case fwRenderListDesc::SORT_DESCENDING:
				SortDescending(pElements, elementCount);
				break;

			case fwRenderListDesc::SORT_NOTSET:
				break;

			case fwRenderListDesc::SORT_ASCENDING_PEDS_BIASED:
				SortAscendingPedsBiased(pElements, elementCount);
				break;
			default:
				// SORT_NONE, presumably.
				SortDrawLast(pElements, elementCount);
				break;
		}
	}

	sysInterlockedDecrement(pDependenciesRunningEa);

	return true;
}

} // namespace rage