//
// fwrenderer/instancing.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "instancing.h"
#include "system/threadtype.h"

#if RSG_DURANGO
#include "system/d3d11.h"
#include "grcore/wrapper_d3d.h"
#endif // RSG_DURANGO

#if RSG_ORBIS
#include "grcore/gfxcontext_gnm.h"
#endif // RSG_ORBIS

namespace rage {

//New factory functions for instance buffer.
grcInstanceBuffer *fwDLInstanceBuffer::DLCreate()
{
	static const u32 instBuffSize = static_cast<u32>(sizeof(fwDLInstanceBuffer));
	DrawListAddress dataAddress;
	void *buff = gDCBuffer->AddDataBlock(NULL, instBuffSize, dataAddress);
	fwDLInstanceBuffer *ib = rage_placement_new(buff) fwDLInstanceBuffer(dataAddress);

	return static_cast<grcInstanceBuffer *>(ib);
}

void fwDLInstanceBuffer::DLDestroy(const grcInstanceBuffer *)
{
	//Drawlist handles destruction, so no need to do anything here.
}

void fwDLInstanceBuffer::DLNextFrame()
{
	grcInstanceBuffer::AdvanceCurrentFrame();
}

void fwDLInstanceBuffer::InitClass()
{
	parent_type::InitClass();
	grcInstanceBuffer::sm_Create = DLCreate;
	grcInstanceBuffer::sm_Destroy = DLDestroy;
	grcInstanceBuffer::sm_NextFrame = DLNextFrame;
}

void fwDLInstanceBuffer::ShutdownClass()
{
	grcInstanceBuffer::ResetFactoryFunctors();
	parent_type::ShutdownClass();
}

fwDLInstanceBuffer::fwDLInstanceBuffer(DrawListAddress offset)
: mDLAddress(offset)
{
}

// PURPOSE:	Sets next link in chain for management
void fwDLInstanceBuffer::SetNext(grcInstanceBuffer *n)
{
	if(n)
		m_DLNext = static_cast<fwDLInstanceBuffer *>(n)->GetDrawListAddress();
	else
		m_DLNext.SetNULL();

	//Call parent so we have a simple pointer on update thread
	parent_type::SetNext(n);
}

// PURPOSE: Returns next buffer in list (as set up by grcInstanceBufferList)
grcInstanceBuffer *fwDLInstanceBuffer::GetNext()
{
	grcInstanceBuffer *next = NULL;
	if(sysThreadType::IsRenderThread())
	{
		if(!m_DLNext.IsNULL())
		{
			static const u32 instBuffSize = static_cast<u32>(sizeof(fwDLInstanceBuffer));
			fwDLInstanceBuffer *ib = reinterpret_cast<fwDLInstanceBuffer *>(gDCBuffer->GetDataBlock(instBuffSize, m_DLNext));
			next = static_cast<grcInstanceBuffer *>(ib);
		}
	}
	else
		next = parent_type::GetNext();

	return next;
}

//////////////////////////////////////////////
//
// Pool Allocation Instance Buffer
//

namespace IBStatic
{
	class fwIBAllocator : public grcInstanceBufferBasic::Allocator
	{
		virtual void *Allocate(size_t UNUSED_PARAM(size))
		{
			if(Verifyf(	fwPoolInstanceBuffer::GetPool()->GetNoOfFreeSpaces() > 0,
						"WARNING! %s Pool Full! Some instanced geometry will not be rendered! (Consider increasing %s PoolSize in common/data/gameconfig.xml.  Current size = %d)",
						fwPoolInstanceBuffer::GetPool()->GetName(), fwPoolInstanceBuffer::GetPool()->GetName(), fwPoolInstanceBuffer::GetPool()->GetSize()	))
			{
				fwPoolInstanceBuffer *ib = rage_new fwPoolInstanceBuffer;
				return reinterpret_cast<void *>(static_cast<grcInstanceBuffer *>(ib));
			}

			return NULL; //Failed to allocate instance buffer - Pool is full!
		}

		virtual void Free(void *buf)
		{
			fwPoolInstanceBuffer *ib = static_cast<fwPoolInstanceBuffer *>(reinterpret_cast<grcInstanceBuffer *>(buf));
			delete ib;
		}
	};

	fwIBAllocator *GetAllocator()
	{
		static fwIBAllocator sAllocator;
		return &sAllocator;
	}
}

FW_INSTANTIATE_CLASS_POOL(fwPoolInstanceBuffer, CONFIGURED_FROM_FILE, atHashString("InstanceBuffer",0x220ec274));

void fwPoolInstanceBuffer::InitClass()
{
	fwPoolInstanceBuffer::InitPool(MEMBUCKET_RENDER);
	parent_type::InitClass(IBStatic::GetAllocator());
}

void fwPoolInstanceBuffer::ShutdownClass()
{
	fwPoolInstanceBuffer::ShutdownPool();
	parent_type::ShutdownClass();
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// fwGPUMemInstanceBuffer
//
#if RSG_DURANGO || RSG_ORBIS

void fwGPUMemInstanceBuffer::InitClass()
{
	grcInstanceBuffer::SetFactoryFunctors(fwGPUMemInstanceBuffer::Create, fwGPUMemInstanceBuffer::Destroy, fwGPUMemInstanceBuffer::NextFrame, UINT_MAX, 4096);
}


void fwGPUMemInstanceBuffer::ShutdownClass()
{
	grcInstanceBuffer::ResetFactoryFunctors();
}


fwGPUMemInstanceBuffer::fwGPUMemInstanceBuffer() : grcInstanceBuffer()
{
}


fwGPUMemInstanceBuffer::~fwGPUMemInstanceBuffer()
{
}


void *fwGPUMemInstanceBuffer::Lock() 
{ 
	Assertf(0, "Not implemented"); return NULL; 
}


void fwGPUMemInstanceBuffer::Unlock(size_t finalCount,size_t elemSizeQw)
{
	m_Count = finalCount;
	m_ElemSizeQW = elemSizeQw;
	Unlock();
}


void *fwGPUMemInstanceBuffer::Lock(size_t finalCount, size_t elemSizeQw)
{
	m_Count = (u32)finalCount;
	m_ElemSizeQW = (u32)elemSizeQw;
	u32 bufSize = m_Count*m_ElemSizeQW*16;
	void *pMem = (void *)gDCBuffer->AllocateObjectFromPagedMemory<u8>(DPT_LIFETIME_GPU_PHYSICAL, bufSize, true);

#if RSG_DURANGO
	m_Data = pMem;
#elif RSG_ORBIS
	m_CB.initAsConstantBuffer(pMem, bufSize);
#endif
	return pMem;
}


void fwGPUMemInstanceBuffer::Unlock()
{
}


const void *fwGPUMemInstanceBuffer::GetData() const 
{
	// Not sure if we need this.
	return NULL;
}


grcInstanceBuffer* fwGPUMemInstanceBuffer::Create() 
{
	fwGPUMemInstanceBuffer *pRet = gDCBuffer->AllocateObjectFromPagedMemory<fwGPUMemInstanceBuffer>(DPT_LIFETIME_GPU, sizeof(fwGPUMemInstanceBuffer), true);
	pRet = new(pRet) fwGPUMemInstanceBuffer();
	pRet->m_Next = NULL;
	return pRet;
}


void fwGPUMemInstanceBuffer::Destroy(const grcInstanceBuffer *ib)
{
	(void)ib;
}


void fwGPUMemInstanceBuffer::NextFrame()
{
	grcInstanceBuffer::AdvanceCurrentFrame();
}


void fwGPUMemInstanceBuffer::Bind(PS3_ONLY(spuCmd_grcDevice__DrawInstancedPrimitive &cmd))
{
#if RSG_DURANGO
	grcDevice::SetConstantBuffer<VS_TYPE>(INSTANCE_CONSTANT_BUFFER_SLOT, m_Data);
#elif RSG_ORBIS
	gfxc.setConstantBuffers(grcVertexProgram::GetCurrent()->GetGnmStage(), INSTANCE_CONSTANT_BUFFER_SLOT, 1, GetConstantBuffers());
#endif
}

#endif // RSG_DURANGO || RSG_ORBIS


} // namespace rage
