// Title	:	RenderListGroup.h
// Authors	:	John Whyte, Alessandro Piva
// Started	:	31/07/2006

#ifndef FWRENDER_RENDERLISTGROUP_H
#define	FWRENDER_RENDERLISTGROUP_H

//////////////
// Includes //
//////////////

// Rage Headers
#include "atl/array.h"
#include "system/threadtype.h"

#include "entity/entity.h"

#define SORT_RENDERLISTS_ON_SPU (1)

#define NUM_RENDER_LIST_SORT_JOBS	4

namespace rage {

//////////////////////////
// Forward declarations //
//////////////////////////

class fwEntity;
class fwRenderListGroup;
class fwRenderPhaseBase;
class fwRenderPhase;

typedef int fwRenderPassId;       // see RenderPassId
typedef int fwRenderBucket;       // see RenderBucketTypes
typedef int fwRenderMode;         // see eRenderMode
typedef u32 fwRenderGeneralFlags; // see eRenderGeneralFlags
typedef int fwRenderPhaseType;    // see RenderPhaseType

/////////////////////////
// Global declarations //
/////////////////////////

extern fwRenderListGroup		gRenderListGroup;


struct fwRenderPhaseInfo
{
	u32 m_StartBit;
	u32 m_MaxCount;
	u32 m_Masks;
};

//////////////////
// fwEntityList //
//////////////////

// description:	List of entities and a sorting value which can be used to sort the entities

#define PUSH_TO_BACK_DISTANCE   (10000.0f)

class fwEntityList
{
public:
	enum {
			CUSTOM_FLAGS_PED_IN_VEHICLE_BIT				= 0,
			CUSTOM_FLAGS_HD_TEX_CAPABLE_BIT				= 1,
			CUSTOM_FLAGS_INTERIOR_SORT_BIT				= 2,
			CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD_BIT	= 3,

			CUSTOM_FLAGS_LOD_SORT_VAL_BIT0				= 4,  // Don't reorder these!!!
			CUSTOM_FLAGS_LOD_SORT_VAL_BIT1				= 5, 
			CUSTOM_FLAGS_LOD_SORT_VAL_BIT2				= 6, 
			CUSTOM_FLAGS_LOD_SORT_ENABLED_BIT			= 7, 

			CUSTOM_FLAGS_PED_IN_VEHICLE					= (1 << CUSTOM_FLAGS_PED_IN_VEHICLE_BIT),
			CUSTOM_FLAGS_HD_TEX_CAPABLE					= (1 << CUSTOM_FLAGS_HD_TEX_CAPABLE_BIT),
			CUSTOM_FLAGS_INTERIOR_SORT					= (1 << CUSTOM_FLAGS_INTERIOR_SORT_BIT),
			CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD		= (1 << CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD_BIT),

			CUSTOM_FLAGS_LOD_SORT_VAL_MASK				= ((1 << CUSTOM_FLAGS_LOD_SORT_VAL_BIT0) |
														   (1 << CUSTOM_FLAGS_LOD_SORT_VAL_BIT1) |
														   (1 << CUSTOM_FLAGS_LOD_SORT_VAL_BIT2)),

			CUSTOM_FLAGS_LOD_SORT_ENABLED				= (1 << CUSTOM_FLAGS_LOD_SORT_ENABLED_BIT),

			// aliases
			CUSTOM_FLAGS_WATER_REFLECTION_BIT				= CUSTOM_FLAGS_INTERIOR_SORT_BIT,
			CUSTOM_FLAGS_WATER_REFLECTION					= CUSTOM_FLAGS_INTERIOR_SORT,
			CUSTOM_FLAGS_WATER_REFLECTION_PRE_REFLECTED_BIT	= CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD_BIT,
			CUSTOM_FLAGS_WATER_REFLECTION_PRE_REFLECTED		= CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD,

			LL_OFFSET = 32 
		};

	#define	CUSTOM_FLAGS_LOD_SORT_VAL_MASK_LL ((1LL << (CUSTOM_FLAGS_LOD_SORT_VAL_BIT0 + LL_OFFSET)) | \
											   (1LL << (CUSTOM_FLAGS_LOD_SORT_VAL_BIT1 + LL_OFFSET)) | \
											   (1LL << (CUSTOM_FLAGS_LOD_SORT_VAL_BIT2 + LL_OFFSET)))

	#define CUSTOM_FLAGS_LOD_SORT_ENABLED_LL    (1LL << (CUSTOM_FLAGS_LOD_SORT_ENABLED_BIT + LL_OFFSET))

	#define CUSTOM_FLAGS_INVERT_LOD_SORT_VAL_LL (5LL << (CUSTOM_FLAGS_LOD_SORT_VAL_BIT0  + LL_OFFSET))  //u32 fwLodData::ms_sortVals[LODTYPES_DEPTH_TOTAL] = { 5, //LODTYPES_DEPTH_HD .... };

	#define CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD_LL (1LL << (CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD_BIT  + LL_OFFSET))

	#define POSITIVE_DIST_MASK					(0x7FFFFFFFLL)

	#define	CUSTOM_FLAGS_SORT_MASK_LL			(CUSTOM_FLAGS_LOD_SORT_ENABLED_LL | CUSTOM_FLAGS_LOD_SORT_VAL_MASK_LL | POSITIVE_DIST_MASK | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD_LL)
	#define	CUSTOM_FLAGS_SORT_MASK_PED_BIAS_LL	((1LL << (CUSTOM_FLAGS_PED_IN_VEHICLE_BIT + LL_OFFSET)) | CUSTOM_FLAGS_SORT_MASK_LL)
	

	class CElement
	{
	public:
		fwEntity*	m_pEntity;
		u32			m_BaseFlags;	// all fwEntity::flags stored in fwEntity::m_baseFlags must fit here!
#if __BE 
		u16			m_RenderFlags;
		u16			m_CustomFlags;
		float		m_sortVal;
#else
		float		m_sortVal;
		u16			m_CustomFlags;
		u16			m_RenderFlags;
#endif
		static inline u32 GetLodSortValue(u16 customFlags)
		{
			const u32 hd = 5;
			u32 lodSortVal = hd - ((customFlags & fwEntityList::CUSTOM_FLAGS_LOD_SORT_VAL_MASK) >> fwEntityList::CUSTOM_FLAGS_LOD_SORT_VAL_BIT0);
			return lodSortVal;
		}
		static inline bool SortAscendingRef(const CElement& pElement1, const CElement& pElement2)
		{
			float value1 = pElement1.m_sortVal + (((pElement1.m_BaseFlags & fwEntity::DRAW_LAST) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f);
			float value2 = pElement2.m_sortVal + (((pElement2.m_BaseFlags & fwEntity::DRAW_LAST) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f);
			value1 -= ((pElement1.m_BaseFlags & fwEntity::DRAW_FIRST_SORTED) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f;
			value2 -= ((pElement2.m_BaseFlags & fwEntity::DRAW_FIRST_SORTED) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f;
			return (value1 < value2);
		}

		static inline bool SortDescendingRef(const CElement& pElement1, const CElement& pElement2)
		{
			float value1 = pElement1.m_sortVal - (((pElement1.m_BaseFlags & fwEntity::DRAW_LAST) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f);
			float value2 = pElement2.m_sortVal - (((pElement2.m_BaseFlags & fwEntity::DRAW_LAST) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f);
			value1 += ((pElement1.m_BaseFlags & fwEntity::DRAW_FIRST_SORTED) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f;
			value2 += ((pElement2.m_BaseFlags & fwEntity::DRAW_FIRST_SORTED) != 0) ? PUSH_TO_BACK_DISTANCE : 0.0f;
			return (value1 > value2);
		}

		static inline bool SortAscendingSimpleRef(const CElement& element1, const CElement& element2)
		{
			u64 e1 = (*((u64*)(&element1.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_LL);
			u64 e2 = (*((u64*)(&element2.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_LL);
			
			Assertf((element1.m_CustomFlags & (CUSTOM_FLAGS_LOD_SORT_ENABLED | CUSTOM_FLAGS_LOD_SORT_VAL_MASK | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD)) == (e1 >> LL_OFFSET), "custom flag 1 Fail");
			Assertf((element2.m_CustomFlags & (CUSTOM_FLAGS_LOD_SORT_ENABLED | CUSTOM_FLAGS_LOD_SORT_VAL_MASK | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD)) == (e2 >> LL_OFFSET), "custom flag 2 Fail");

			Assertf((*(u32*)&element1.m_sortVal)  == (e1 & POSITIVE_DIST_MASK), "sort val 1 Fail");
			Assertf((*(u32*)&element2.m_sortVal)  == (e2 & POSITIVE_DIST_MASK), "sort val 2 Fail");

			return e1 < e2;
		}

		static inline bool SortDescendingSimpleRef(const CElement& element1, const CElement& element2)
		{
			// The code below would make it descending sorts with lodSortValues work is used by descending cutout pass
#if 1
			u64 e1 = *((u64*)(&element1.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_LL;
			u64 e2 = *((u64*)(&element2.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_LL;

			// Note I'm not masking out the CUSTOM_FLAGS_LOD_SORT_ENABLED_LL bit because it should be the same across all elements 
			u64 sortMask = (e1 & CUSTOM_FLAGS_LOD_SORT_ENABLED_LL);
			sortMask = (sortMask >> 1) | (sortMask >> 2) | (sortMask >> 3);

			u64 maskedCustmFlagsInvertLodSortVal = CUSTOM_FLAGS_INVERT_LOD_SORT_VAL_LL & sortMask;

			u64 e1InvertedLodSortVal = maskedCustmFlagsInvertLodSortVal - (e1 & sortMask);
			u64 e2InvertedLodSortVal = maskedCustmFlagsInvertLodSortVal - (e2 & sortMask);

			e1 = (e1 & ~sortMask) | e1InvertedLodSortVal;
			e2 = (e2 & ~sortMask) | e2InvertedLodSortVal;

			return e2 < e1;
#else

#if __ASSERT
			u64 e1 = *((u64*)(&element1.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_LL;
			u64 e2 = *((u64*)(&element2.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_LL;
			Assertf(!(e1 & CUSTOM_FLAGS_LOD_SORT_ENABLED_LL) && !(e2 & CUSTOM_FLAGS_LOD_SORT_ENABLED_LL), "Custom lod sort should is currently not supported for descending searches");

			Assertf((element1.m_CustomFlags & (CUSTOM_FLAGS_LOD_SORT_ENABLED | CUSTOM_FLAGS_LOD_SORT_VAL_MASK | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD)) == (e1 >> LL_OFFSET), "custom flag 1 Fail");
			Assertf((element2.m_CustomFlags & (CUSTOM_FLAGS_LOD_SORT_ENABLED | CUSTOM_FLAGS_LOD_SORT_VAL_MASK | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD)) == (e2 >> LL_OFFSET), "custom flag 2 Fail");

			Assertf((*(u32*)&element1.m_sortVal)  == (e1 & POSITIVE_DIST_MASK), "sort val 1 Fail");
			Assertf((*(u32*)&element2.m_sortVal)  == (e2 & POSITIVE_DIST_MASK), "sort val 2 Fail");

#endif

			return PositiveFloatLessThan(element2.m_sortVal, element1.m_sortVal);
#endif
		}

		static inline bool ShouldNotDrawLast(const CElement& element)
		{
			return (element.m_BaseFlags & fwEntity::DRAW_LAST)==0;
		}
		static inline bool ShouldDrawFirst(const CElement& element)
		{
			return (element.m_BaseFlags & fwEntity::DRAW_FIRST_SORTED)!=0;
		}
		static inline bool SortInterior(const CElement& element)
		{
			return (element.m_CustomFlags & CUSTOM_FLAGS_INTERIOR_SORT)!=0;
		}

		static inline bool SortAscendingSimpleRefPedBias(const CElement& element1, const CElement& element2)
		{
			u64 e1 = (*((u64*)(&element1.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_PED_BIAS_LL);
			u64 e2 = (*((u64*)(&element2.m_BaseFlags + 1)) & CUSTOM_FLAGS_SORT_MASK_PED_BIAS_LL);

			Assertf((element1.m_CustomFlags & (CUSTOM_FLAGS_LOD_SORT_ENABLED | CUSTOM_FLAGS_LOD_SORT_VAL_MASK | CUSTOM_FLAGS_PED_IN_VEHICLE | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD)) == (e1 >> LL_OFFSET), "custom flag 1 Fail");
			Assertf((element2.m_CustomFlags & (CUSTOM_FLAGS_LOD_SORT_ENABLED | CUSTOM_FLAGS_LOD_SORT_VAL_MASK | CUSTOM_FLAGS_PED_IN_VEHICLE | CUSTOM_FLAGS_USE_EXTERIOR_SCREEN_QUAD)) == (e2 >> LL_OFFSET), "custom flag 2 Fail");

			Assertf((*(u32*)&element1.m_sortVal)  == (e1 & POSITIVE_DIST_MASK), "sort val 1 Fail");
			Assertf((*(u32*)&element2.m_sortVal)  == (e2 & POSITIVE_DIST_MASK), "sort val 2 Fail");

			return e1 < e2;
		}
	};

	typedef rage::atArray<CElement>::iterator ListIterator;

	fwEntityList()										{ ResetListFlags(); }

	void Init(s32 size)									{ m_entityArray.Reserve(size); ResetListFlags(); }
	void Shutdown()										{ m_entityArray.Reset(); ResetListFlags(); }

	void Clear()										{ m_entityArray.Resize(0); ResetListFlags(); }

	void AddEntity(fwEntity* pEntity, u32 baseFlags, u16 renderFlags, u32 subphaseVisFlags, float sortVal, u16 flags);

	s32 GetCount() const								{ return m_entityArray.GetCount(); }
	fwEntity* GetEntity(const s32 i) const				{ return m_entityArray[i].m_pEntity; }
	u32 GetEntityBaseFlags(const s32 i) const			{ return m_entityArray[i].m_BaseFlags; }
	u32 GetEntityRenderFlags(const s32 i) const			{ return m_entityArray[i].m_RenderFlags; }
	float GetSortVal(const s32 i) const					{ return m_entityArray[i].m_sortVal; }
	u16 GetEntityCustomFlags(const s32 i) const			{ return m_entityArray[i].m_CustomFlags; }
	void EraseEntity(const s32 i)						{ m_entityArray[i].m_pEntity = NULL; }

	CElement* GetElements()								{ return m_entityArray.GetElements(); }

	// Get the union or intersection of all the base/render flag bits for all entities in the list
	u32 GetBaseFlagsUnion() const						{ return m_nBaseFlagsUnion; }
	u32 GetBaseFlagsIntersection() const				{ return m_nBaseFlagsIntersection; }
	u32 GetRenderFlagsUnion() const						{ return m_nRenderFlagsUnion; }
	u32 GetRenderFlagsIntersection() const				{ return m_nRenderFlagsIntersection; }
	
	// Reset the per-list base and render flag unions and intersections
	inline void ResetListFlags()
	{
		m_nBaseFlagsUnion = 0x0;
		m_nBaseFlagsIntersection = 0xFFFFFFFF;
		m_nRenderFlagsUnion = 0x0;
		m_nRenderFlagsIntersection = 0xFFFFFFFF;
	}

	// Update the per-list base and render flag unions and intersections for a given entity
	inline void UpdateListFlags(const fwEntityList::CElement& rElement)
	{
		m_nBaseFlagsUnion |= rElement.m_BaseFlags;
		m_nBaseFlagsIntersection &= rElement.m_BaseFlags;
		m_nRenderFlagsUnion |= rElement.m_RenderFlags;
		m_nRenderFlagsIntersection &= rElement.m_RenderFlags;
	}

#if !SORT_RENDERLISTS_ON_SPU
	void OldSortAscending(s32 start, s32 end);
	void OldSortDescending(s32 start, s32 end);
	void OldSortDrawLast(s32 start, s32 end);
	void OldSortAscendingPedsBiased(s32 start, s32 end);
#endif

private:

	// Union and intersection of all the base & render flag bits. This allows us to early-reject an entire list
	// based on filter flags at BuildDrawList time.
	u32 m_nBaseFlagsUnion;
	u32 m_nBaseFlagsIntersection;
	u32 m_nRenderFlagsUnion;
	u32 m_nRenderFlagsIntersection;

	atArray<CElement>	m_entityArray;
};




//////////////////////////
// fwEntityPreRenderList //
//////////////////////////

// description:	Same as fwEntityList but prerender-related; no sortval needed

class fwEntityPreRenderList
{
public:

	typedef rage::atArray<fwEntity*>::iterator ListIterator;

	fwEntityPreRenderList()				{ }

	void Init(s32 size)					{ m_entityArray.Reserve(size); }
	void Shutdown()						{ m_entityArray.Reset(); }

	void Resize(s32 size)				{ m_entityArray.Resize(size); }
	void Clear()						{ m_entityArray.Resize(0); }
	void AddEntity(fwEntity* pEntity)	{ m_entityArray.PushAndGrow(pEntity); }
	s32 GetCount() const				{ return m_entityArray.GetCount(); }
	fwEntity* GetEntity(s32 i)			{ return m_entityArray[i]; }
	void EraseEntity(s32 i)				{ m_entityArray[i] = NULL; }

	fwEntity** begin()					{ return m_entityArray.begin(); }
	fwEntity** end()					{ return m_entityArray.end(); }

	void SortAndRemoveDuplicates();
	void RemoveElements(fwEntityPreRenderList& other);

private:

	atArray<fwEntity*>	m_entityArray;
};

//////////////////////
// fwRenderListDesc //
//////////////////////

// description:	Class used to store all the render list info

class fwRenderListDesc
{
	friend class fwRenderListGroup;

public:
	enum eRenderListSort {
		SORT_NOTSET = 0,
		SORT_NONE,
		SORT_ASCENDING,
		SORT_DESCENDING,
		SORT_ASCENDING_PEDS_BIASED  // This was put in for B*644704 to make peds in vehilces render after the vehicle
	};

	void Init(unsigned initMode, int renderPassCount);
	void Shutdown(unsigned shutdownMode);
	void InitRenderListBuild();
	void PostRenderListBuild();
	void SortList(int renderPassId);
	void Clear();

#if !SORT_RENDERLISTS_ON_SPU
	void OldSortList(int renderPassId);
#endif

	ASSERT_ONLY( bool IsInitRenderListBuildCalled() const { return m_initRenderListBuildCalled; } )

	void AddEntity(fwEntity* pEntity, u32 baseFlags, u16 renderFlags, u32 subphaseVisFlags, float sortVal, fwRenderPassId id, u16 flags)
	{
		m_entityLists[id].AddEntity(pEntity, baseFlags, renderFlags, subphaseVisFlags, sortVal, flags);
	}

	void AddEntity(fwEntity* pEntity, u32 subphaseVisFlags, float sortVal, fwRenderPassId id, u16 flags)
	{
		m_entityLists[id].AddEntity(pEntity, pEntity->GetBaseFlags(), pEntity->GetRenderFlags(), subphaseVisFlags, sortVal, flags);
	}

	s32 GetNumberOfEntities(const fwRenderPassId id) const					{ return m_entityLists[id].GetCount(); }
	fwEntity* GetEntity(const u32 i, const fwRenderPassId id)				{ return m_entityLists[id].GetEntity(i); }
	u32 GetEntityBaseFlags(const u32 i, const fwRenderPassId id) const		{ return m_entityLists[id].GetEntityBaseFlags(i); }
	u32 GetEntityRenderFlags(const u32 i, const fwRenderPassId id) const	{ return m_entityLists[id].GetEntityRenderFlags(i); }
	float GetEntitySortVal(const u32 i, const fwRenderPassId id) const		{ return m_entityLists[id].GetSortVal(i); }
	u16 GetEntityCustomFlags(const u32 i, const fwRenderPassId id) const	{ return m_entityLists[id].GetEntityCustomFlags(i); }

	void EraseEntity(const s32 i, const fwRenderPassId id)					{ m_entityLists[id].EraseEntity(i); }
	void ClearRenderList(const fwRenderPassId id)							{ m_entityLists[id].Clear(); }

	fwEntityList& GetList(const s32 id)										{ return m_entityLists[id]; }

	void SetRenderPhase(fwRenderPhase* const pRP)							{ m_pRPNew = pRP; }
	fwRenderPhase* GetRenderPhaseNew()										{ return m_pRPNew; } 

	static void InitSortArray(int renderPassCount);

	static void SetSortType(int renderPass, eRenderListSort sort)			{ sm_listSort[renderPass] = sort; }

	static void WaitForAllSortTasksToComplete();

private:
	void Reset();

	atArray<fwEntityList>	m_entityLists;

	fwRenderPhase*		m_pRPNew;		//which render phase is using this list, just for debug info

	ASSERT_ONLY( bool		m_initRenderListBuildCalled; )

	static atArray<eRenderListSort>	sm_listSort;
};

//////////////////////
// fwRenderListGroup //
//////////////////////

// description: Group of render lists
// 1 global instance of this, so really it's a singleton

class fwRenderListGroup
{
	friend class fwRenderListDesc;

public:

	void Init(unsigned initMode, int renderPhaseCount, int renderPhaseTypeCount, int renderPassCount);
	void Shutdown(unsigned shutdownMode);
	void Clear();

	void SetRenderPhaseTypeInfo(const fwRenderPhaseType renderPhaseType, const u32 maxCount);
	void GetRenderPhaseTypeInfo(const fwRenderPhaseType renderPhaseType, u32* startBit, u32* maxCount, u32* masks);
	u32 GetRenderPhaseTypeMask(const fwRenderPhaseType renderPhaseType);

	s32 RegisterRenderPhase(const fwRenderPhaseType type, fwRenderPhase* const pRP);

	s32 GetNumRenderPhasesRegistered() const				{ FastAssert(sysThreadType::IsUpdateThread()); return m_RenderList.GetCount(); }
	void ResetRegisteredRenderPhases();

	void ClearRenderPhasePointersNew();

	void StartSortEntityListJob();
	void WaitForSortEntityListJob();

	void SetThreadAccessMask(int threadAccess)				{ m_ThreadAccessMask = threadAccess; }

	fwRenderListDesc& GetRenderListForPhase(const s32 i)	{ FastAssert(!IsSortInProgress()); FastAssert((sysThreadType::GetCurrentThreadType() & m_ThreadAccessMask) != 0); return m_RenderList[i]; }
	fwEntityPreRenderList& GetAlreadyPreRenderedList()		{ FastAssert(sysThreadType::IsUpdateThread()); return m_AlreadyPreRenderedList; }
	fwEntityPreRenderList& GetPreRenderList()				{ FastAssert(sysThreadType::IsUpdateThread()); return m_PreRenderList; }
	fwEntityPreRenderList& GetPreRender2List()				{ FastAssert(sysThreadType::IsUpdateThread()); return m_PreRender2List; }

	static bool IsSortInProgress();

private:
	int							m_MaxRenderPhaseCount;
	int							m_RenderPhaseTypeCount;
	u32							m_NumRenderPhases;
	atArray<u32>				m_RenderPhaseTypeCounts;
	atArray<fwRenderPhase*>		m_RenderPhases;
	int							m_numBitsUsed;

	int							m_ThreadAccessMask;			// which threads are allowed to access the phase render lists

	atArray<fwRenderListDesc>	m_RenderList;
	fwEntityPreRenderList		m_AlreadyPreRenderedList;
	fwEntityPreRenderList		m_PreRenderList;
	fwEntityPreRenderList		m_PreRender2List;
};

#define MAX_RENDER_LISTS_TO_SORT	64
struct fwSortEntityListSpuData
{
	rage::fwRenderListDesc::eRenderListSort	m_aSortType[MAX_RENDER_LISTS_TO_SORT];
	rage::fwEntityList::CElement*			m_aElementPtr[MAX_RENDER_LISTS_TO_SORT];
	u32										m_aElementCount[MAX_RENDER_LISTS_TO_SORT];
	u32										m_ListCount;
	u32										m_Padding[3];
};


} // namespace rage


#endif // defined RENDERLISTGROUP_H
