/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    renderThread.cpp
// PURPOSE : stuff for setting up separate render thread & functions it requires
// AUTHOR :  john w.
// CREATED : 1/8/06
//
/////////////////////////////////////////////////////////////////////////////////

#include "renderthread.h"
#if __PPU
// PS3 headers
#include <cell/gcm.h>
#elif RSG_ORBIS
#include "grcore/gnmx.h"
#include "grcore/wrapper_gnm.h"
#include <kernel.h>
#endif
#include <ctime>

// Rage headers
#include "grcore/config.h"
#include "grcore/device.h"
#include "grcore/gfxcontextprofile.h"
#include "grmodel/geometry.h"
#include "grmodel/setup.h"
#include "grprofile/timebars.h"
#include "input/input.h"
#include "profile/cputrace.h"
#include "profile/telemetry.h"
#include "streaming/streamingvisualize.h"
#include "system/cache.h"
#include "system/criticalsection.h"
#include "system/exception.h"
#include "system/hangdetect.h"
#include "system/param.h"
#include "system/memory.h"
#include "system/service.h"
#include "system/xtl.h"
#if __D3D
#include "system/d3d9.h"
#endif
#if RSG_ORBIS
#include "grcore/gfxcontext_gnm.h"
#endif

// Framework headers
#include "grcore/debugdraw.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwsys/timer.h"
#include "vfx/ptfx/PtFxManager.h"

// ====================== Platform Dependent =========================

#if RSG_XENON
// Ensure render thread stack size is a multiple of 64k so 
// that the stack is allocated in 64k page(s) to reduce TLB misses.
CompileTimeAssert(RENDER_THREAD_STACK % (64*1024) == 0);
#endif

XPARAM(randomseed);
// ====================== Platform Dependent =========================

#if GRCGFXCONTEXTPROFILE_ENABLE
// Debug flag to trigger dump of grcGfxContext profile data out to TTY.  This is
// not hooked up to a bank widget, since profiling this kind of stuff in bank
// builds is bad (will give you misleading results often enough that the profile
// simply can't be trusted).  Intended to be used in profile builds.
volatile bool DUMP_GRCGFXCONTEXT_PROFILE;
#endif

namespace rage {


#if __WIN32PC
	XPARAM(d3dmt);
#endif

PRE_DECLARE_THREAD_FUNC(RenderThreadEntryPoint);

PARAM(breakForRTBug, "[RenderThread] Break if protect buffer overwritten");

#if !__FINAL && RSG_LOST_FOCUS_SUPPORT
XPARAM(BlockOnLostFocus);
#endif

// declare singletons
fwRenderThreadInterface	gRenderThreadInterface;			// external interface to the thread

//static float	g_fCurrentTimer = 0.0f;
static sysCriticalSectionToken g_SetRenderFunctionToken;


#if !__FINAL
#if __PS3
static void RsxCallback(const uint32_t cause);
#elif __XENON
static void GpuCallback(DWORD cause);
#endif // __XENON
#endif // !__FINAL

_ThreadParams::_ThreadParams()
: m_eKill(NULL)
, m_eRunning(NULL)
, m_eUpdateDone(NULL)
, m_eRenderDone(NULL)
{
}

void _ThreadParams::Init()
{
	Assert(!m_eKill);		// Initialized twice?

	m_eKill = sysIpcCreateSema(false);
	m_eRunning = sysIpcCreateSema(false);
	m_eUpdateDone = sysIpcCreateSema(false);
	m_eRenderDone = sysIpcCreateSema(false);
	m_customRenderThreadMutex = sysIpcCreateMutex();
}

_ThreadParams::~_ThreadParams()
{
	Shutdown();
}

void _ThreadParams::Shutdown()
{
	if (m_eKill) {
		sysIpcDeleteSema(m_eKill);
		sysIpcDeleteSema(m_eRunning);
		sysIpcDeleteSema(m_eUpdateDone);
		sysIpcDeleteSema(m_eRenderDone);
		sysIpcDeleteMutex(m_customRenderThreadMutex);

		m_eKill = NULL;
		m_eRunning = NULL;
		m_eUpdateDone = NULL;
		m_eRenderDone = NULL;
	}
}


//-----------------------------------------------
// DEBUG
#if LOG_RENDER_FLUSH

#include "fwsys/timer.h"
#include "system/timer.h"

class CTempDebugLog
{
public:
#define CTEMPDEBUGLOGENTRYCOUNT	128
	struct CTempLogEntry
	{
		s64			time;
		char		desc[128];
	};

	CTempDebugLog()
	{
		m_Head = 0;
		m_Count = 0;
	}

	void Push(const char *msg)
	{
		m_logEntries[m_Head].time = sysTimer::GetTicks();
		strcpy(m_logEntries[m_Head].desc, msg);
		m_Head++;
		if( m_Head >= CTEMPDEBUGLOGENTRYCOUNT )
		{
			m_Head = 0;
		}
		m_Count++;
	}

	void Dump(const char *pTitle)
	{
		Displayf("CTempDebugLog - Dumping Log: %s", pTitle);
		int count = Min(m_Count, CTEMPDEBUGLOGENTRYCOUNT);
		int tailIdx = (m_Head - count) % CTEMPDEBUGLOGENTRYCOUNT;
		for(int i=0; i<count;i++)
		{
			Displayf("Time = %ul :- %s", m_logEntries[tailIdx].time, m_logEntries[tailIdx].desc );
			tailIdx++;
			if( tailIdx >= CTEMPDEBUGLOGENTRYCOUNT )
				tailIdx = 0;
		}
	}

	CTempLogEntry	m_logEntries[CTEMPDEBUGLOGENTRYCOUNT];
	int				m_Head;
	int				m_Count;
};

CTempDebugLog				g_MainThreadTempDebugLog;
CTempDebugLog				g_RenderThreadTempDebugLog;
volatile	bool			g_InFlush = false;	// Only log during a flush

bool ThreadSyncLogsCompare(const CTempDebugLog::CTempLogEntry& a, const CTempDebugLog::CTempLogEntry& b) {return a.time < b.time;}

void	SortAndDumpThreadSyncLogs(CTempDebugLog &log1, CTempDebugLog &log2)
{
	CTempDebugLog::CTempLogEntry entries[CTEMPDEBUGLOGENTRYCOUNT+CTEMPDEBUGLOGENTRYCOUNT];
	int	inIDX = 0;

	Displayf("DUMP THREAD SYNC LOGS");

	// LOG1
	int count = Min(log1.m_Count, CTEMPDEBUGLOGENTRYCOUNT);
	int tailIdx = (log1.m_Head - count) % CTEMPDEBUGLOGENTRYCOUNT;
	for(int i=0; i<count;i++)
	{
		entries[inIDX] = log1.m_logEntries[tailIdx];
		tailIdx++;
		if( tailIdx >= CTEMPDEBUGLOGENTRYCOUNT )
			tailIdx = 0;
		inIDX++;
	}
	// LOG2
	count = Min(log2.m_Count, CTEMPDEBUGLOGENTRYCOUNT);
	tailIdx = (log2.m_Head - count) % CTEMPDEBUGLOGENTRYCOUNT;
	for(int i=0; i<count;i++)
	{
		entries[inIDX] = log2.m_logEntries[tailIdx];
		tailIdx++;
		if( tailIdx >= CTEMPDEBUGLOGENTRYCOUNT )
			tailIdx = 0;
		inIDX++;
	}

	std::sort(&entries[0], &entries[0] + inIDX, ThreadSyncLogsCompare);

	for(int i=0;i<inIDX;i++)
	{
		Displayf("Time = %ul: %s", entries[i].time, entries[i].desc);
	}

}

void	DumpRenderThreadSyncLogs()
{
//	g_MainThreadTempDebugLog.Dump( "Main Thread" );
//	g_RenderThreadTempDebugLog.Dump( "Render Thread" );

	SortAndDumpThreadSyncLogs(g_MainThreadTempDebugLog, g_RenderThreadTempDebugLog);
}

#endif	//LOG_RENDER_FLUSH

//-----------------------------------------------



// --- fwRenderThreadInterface ------------------------------
// handle any setup & interface to the render thread proper

fwRenderThreadInterface::fwRenderThreadInterface(void)
{
	m_updateBuffer = 0;
	m_renderBuffer = 0;
	m_renderFunction = NULL;
	m_pendingRenderFunction = NULL;
	m_disableSyncronisation = false;
	m_bActive = false;
	m_flushed = false;
	m_TimeWaitedForUpdate = 0;
	m_TimeWaitedForGpu = 0;
#if RSG_PC
	m_bUsingDefaultRenderFunc = false;
#endif
#if !__FINAL
	m_UpdateBufferThreadAccess = sysThreadType::THREAD_TYPE_UPDATE;
	m_RenderThreadFrameNumber = 0;
	m_IsRenderThreadSynchronized = false;
#endif // !__FINAL

	m_gameInterface = &m_defaultGameInterface;
}

fwRenderThreadInterface::~fwRenderThreadInterface(void)
{
}


// init - called from main thread (to create the new thread)
void fwRenderThreadInterface::Init(sysIpcPriority priority,void (*initcallback)())
{
#if HANG_DETECT_THREAD && RSG_DURANGO
	// Durango is can very slow to load all the shaders, so temporarily increase
	// the timeout.  Slowness looks like it may be an XDK mismatch between
	// compiled shaders and installed firmware causing the shaders to be
	// recompiled.
	const u32 prevHangDetectTimeLimit = sysHangDetectCountdown;
	sysHangDetectCountdown = 120;
#endif

	m_DrawThreadParams.Init();

	m_InitCallback = initcallback;

	// Invalidate the pgDictionary linked list ownership.  The render thread
	// initialization code will steal access ownership for the pgDictionary
	// linked lists.
	pgDictionaryBase::SetListOwnerThread(sysIpcCurrentThreadIdInvalid);

	// Create the draw thread
	m_threadId = sysIpcCreateThread(RenderThreadEntryPoint, this, RENDER_THREAD_STACK, priority, "[RAGE] RenderThread", RENDER_THREAD_CORE, "Render");

#if __ASSERT
	Assertf(m_threadId != sysIpcThreadIdInvalid, "Could not create the render thread - out of memory?");
#else
	if (m_threadId == sysIpcThreadIdInvalid)
	{
		Quitf(ERR_GFX_RENDERTHREAD,"Could not create the render thread - out of memory?");
	}
#endif
		
	// wait until the thread has started running & taken over the device
	g_SysService.UpdateClass();
	while(!m_DrawThreadParams.PollIsRunning())
	{
		sysIpcSleep(500);
		g_SysService.UpdateClass();
	}

	// Now that render thread initialization has completed, take back owner ship
	// of the pgDictionary linked lists.
	pgDictionaryBase::SetListOwnerThread(g_CurrentThreadId);

	m_bActive = true;
	m_flushed = false;

#if HANG_DETECT_THREAD && RSG_DURANGO
	sysHangDetectCountdown = prevHangDetectTimeLimit;
#endif

#if HANG_DETECT_THREAD && LOG_RENDER_FLUSH && RSG_PC
	sysHangDetectSetCrashFunc(DumpRenderThreadSyncLogs);
#endif //LOG_RENDER_FLUSH && RSG_PC

}

void fwRenderThreadInterface::Shutdown()
{
	// request kill of the render thread
	m_DrawThreadParams.SignalKill();

	// mini flush
	m_DrawThreadParams.SignalUpdateDone();
	gDrawListMgr->TerminateDrawLists();

#if __D3D11 && RSG_PC
	GRCDEVICE.UnlockDeviceResetAvailability();

	GRCDEVICE.UnregisterSessionNotification();
#endif
	// wait till it's dead
	sysIpcWaitThreadExit(m_threadId);

	m_DrawThreadParams.Shutdown();

	m_bActive = false;
}

bool fwRenderThreadInterface::IsUsingDefaultRenderFunction()
{
	if (m_renderFunction)
	{
		return false;
	}

	return true;
}

void fwRenderThreadInterface::Synchronise(){

	if (!gDrawListMgr->IsDrawListCreationDone()) {
		gDrawListMgr->BeginDrawListCreation();
		gDrawListMgr->EndDrawListCreation();
	}

	// Tell the render thread that we're now waiting for the signal.
	m_DrawThreadParams.SignalUpdateDone();

	// check that nothing trashed the semaphore handles. Seen a bug where this happened for some reason.
	// If this fires we should try and determine what caused it (and may have to add more calls throughout code).
	Assertf(m_DrawThreadParams.CheckSemaIntegrity(),"Semaphore handles have been trashed in this last frame. Fatal.\n");

	grcSetupInstance->BeginUpdateWait();

	u32 time = sysTimer::GetSystemMsTime();

#if 1 // !__D3D11
	PF_START_TIMEBAR_IDLE("Wait for render");
	// override bar colour for ragetrace
	RAGETRACE_POP();
	RAGETRACE_ONLY(static pfTraceCounterId ctr("Wait for render", Color32(0));)
	_RAGETRACE_PUSH(ctr);
#endif // !__D3D11

	Assert(gDrawListMgr->IsDrawListCreationDone());

#if __D3D11 && RSG_PC
	GRCDEVICE.UnlockDeviceResetAvailability();
#endif

	// wait for render from previous frame
	{
		AUTO_HANG_DETECT_CRASH_ZONE;
		m_DrawThreadParams.WaitTillRenderDone();	// semaphore is available first time round so we don't block (no render queued!)
	}
	grcSetupInstance->EndUpdateWait();
	gDrawListMgr->SetToPreDrawListState();

	Assertf(gDCBuffer->GetCurrListId() == 0, "Thread synchronization is occuring while render thread is still executing draw lists");

	// -------------------------------------------------------------
	// *** main thread safe area ***  
	// render thread is guaranteed to be blocked here
#if __D3D11 && RSG_PC
	GRCDEVICE.LockDeviceResetAvailability();
#endif

	D3D11_ONLY(GRCDEVICE.LockContext());
	
#if RSG_PC && __D3D11
	PF_START_TIMEBAR("DoPendingBufferUpdates");
	if (GRCDEVICE.CheckDeviceStatus())
	{
		// Is the pending render function NOT the default one ?
		if(m_pendingRenderFunctionCopy != NULL)
		{
			// Perform any pending texture/buffer updates now whilst we have the context locked (we don`t have a draw list to place them into).
			dlCmdGrcDeviceUpdateBuffer::DoPendingBufferUpdates();
		}
	}
#endif // RSG_PC && __D3D11

 	// pgDictionaryBase::SetListOwnerThread(sysIpcGetCurrentThreadId());
	m_gameInterface->PerformSafeModeOperations();
	// pgDictionaryBase::SetListOwnerThread(sysIpcCurrentThreadIdInvalid);
	D3D11_ONLY(GRCDEVICE.UnlockContext());

	m_TimeWaitedForRenderThread = sysTimer::GetSystemMsTime() - time;
}

// block main thread if GPU is still executing the previous frame then execute code which need to alter graphics data
void fwRenderThreadInterface::GPU_IdleSection(bool UNUSED_PARAM(duringFlush))
{
	Assert(sysThreadType::IsUpdateThread());

	gDrawListMgr->FlipUpdateFenceIdx();

#if !RSG_PC
	// check if previous frame GPU has finished. If not then wait...
	grcFenceHandle fenceHandle = gDrawListMgr->GetEndFenceHandle();
#endif // !RSG_PC
	
#if __D3D11
	PF_START_TIMEBAR_IDLE("Wait for render");
	// override bar colour for ragetrace
	RAGETRACE_POP();
	RAGETRACE_ONLY(static pfTraceCounterId ctr("Wait for render", Color32(0));)
	_RAGETRACE_PUSH(ctr);
#endif // __D3D11

	u32 time = sysTimer::GetSystemMsTime();

	grcSetupInstance->BeginUpdateWait();
#if !RSG_PC
	D3D11_ONLY(GRCDEVICE.LockContext());

#if __D3D11
	PF_START_TIMEBAR_IDLE("Wait for GPU");
#endif // __D3D11

	GRCDEVICE.CpuWaitOnFence(fenceHandle);
	gDrawListMgr->ClearEndFenceHandle();
	D3D11_ONLY(GRCDEVICE.UnlockContext());
#endif // !RSG_PC
	grcSetupInstance->EndUpdateWait();

	m_TimeWaitedForGpu = sysTimer::GetSystemMsTime() - time;

	PF_START_TIMEBAR("RemoveAllRefs");
 	// pgDictionaryBase::SetListOwnerThread(sysIpcGetCurrentThreadId());
	gDrawListMgr->RemoveAllRefs();		// refs are held as long as possible - clear just before about to be re-used
	// pgDictionaryBase::SetListOwnerThread(sysIpcCurrentThreadIdInvalid);

	// Process any deferred events on Durango
#if RSG_DURANGO
	g_SysService.DispatchDeferredEvents();
#endif
}

void fwRenderThreadInterface::FlipUpdateBuffer()
{
	m_updateBuffer = 1 - m_updateBuffer;
}

void fwRenderThreadInterface::FlipRenderBuffer()
{
	m_renderBuffer = 1 - m_renderBuffer;
}


void 
fwRenderThreadInterface::Flush(bool forceFullFlush /* = false*/)
{
	STRVIS_SET_MARKER_TEXT("Render Thread Flush");

	Assert(sysThreadType::IsUpdateThread()); // Don't even think about it.

	// In response to some startup errors, Flush can be called before the
	// drawlist manager has been created (eg. B*2133927).  In that case, there
	// is nothing to be flushed, so just return.
	if (Unlikely(!gDrawListMgr))
	{
		return;
	}

	Assert(false == gDrawListMgr->IsBuildingDrawList()); // A rather bad idea.

	if (m_flushed && !forceFullFlush)
	{
		// Drawlists are all idle (else m_flushed would be false), so just reset
		// all the drawlists ref counts that may have been added since the last
		// flush.
#if __D3D11 && RSG_PC
		GRCDEVICE.UnlockDeviceResetAvailability();
#endif
		gDrawListMgr->RemoveAllRefs(dlDrawListMgr::FLUSH_WITHOUT_RENDERING);
#if __D3D11 && RSG_PC
		GRCDEVICE.LockDeviceResetAvailability();
#endif
		return;
	}

#if LOG_RENDER_FLUSH
	g_InFlush = true;
	g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): Flush Started");
#endif	//LOG_RENDER_FLUSH

	// Retain the state - if we were done with the draw lists, match that before we exit.
	bool drawListsDone = gDrawListMgr->IsDrawListCreationDone();

	if (!drawListsDone) {
		gDrawListMgr->BeginDrawListCreation();
		gDrawListMgr->EndDrawListCreation();
	}

#if __WIN32PC
	HandleDeviceReset();
#endif

#if LOG_RENDER_FLUSH
	g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): Entering Flush Loop");
#endif	//LOG_RENDER_FLUSH

	// Wait for the GPU to be actually done, we wouldn't like to be remove data from under it.
	for(int i=0; i<4; ++i)
	{

#if LOG_RENDER_FLUSH
		// Hacky for speed
		char TempStringToKobble[] = "LOG_RENDER_FLUSH (MAIN_THREAD): Flush Loop count = 0";
		TempStringToKobble[51] = '0'+(char)i;
		g_MainThreadTempDebugLog.Push(TempStringToKobble);
#endif	//LOG_RENDER_FLUSH

		// This is essentially a normal GPU_IdleSection/Update/Synchronise loop
		GPU_IdleSection(true);

#if LOG_RENDER_FLUSH
		g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): Calling SignalUpdateDone()");
#endif	//LOG_RENDER_FLUSH
		m_DrawThreadParams.SignalUpdateDone();

		Assert(gDrawListMgr->IsDrawListCreationDone());

		g_SysService.UpdateClass();

#if __D3D11 && RSG_PC
		GRCDEVICE.UnlockDeviceResetAvailability();
#endif

		if (m_bActive)
		{

#if LOG_RENDER_FLUSH
			g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): Enter PollIsRenderDone()");
#endif	//LOG_RENDER_FLUSH

			while(!GetDrawThreadParams()->PollIsRenderDone())
			{
				sysIpcSleep(1);
				g_SysService.UpdateClass();
			}

#if LOG_RENDER_FLUSH
			g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): Exit PollIsRenderDone()");
#endif	//LOG_RENDER_FLUSH

		}

#if __D3D11 && RSG_PC
		GRCDEVICE.LockDeviceResetAvailability();
#endif

		gDrawListMgr->SetToPreDrawListState();

		// If we started with the lists not created yet, keep it that way.
		if (i < 3 || drawListsDone) {
			gDrawListMgr->BeginDrawListCreation();
			gDrawListMgr->EndDrawListCreation();

#if LOG_RENDER_FLUSH
			g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): New Drawlist created");
#endif	//LOG_RENDER_FLUSH

		}
	}

	m_gameInterface->Flush();

	// Make sure IsSafeToDelete doesn't trigger false positives after a Flush
	GRCDEVICE.IncrementSyncCounter();
	GRCDEVICE.IncrementSyncCounter();

	m_flushed = true;

#if !__FINAL
	grcSetup::ResetTicker();
#endif // !__FINAL

#if LOG_RENDER_FLUSH
	g_InFlush = false;
	g_MainThreadTempDebugLog.Push("LOG_RENDER_FLUSH (MAIN_THREAD): Flush Ended");
#endif	//LOG_RENDER_FLUSH

}

#if __WIN32PC
void fwRenderThreadInterface::FlushAndDeviceCheck()
{
	gRenderThreadInterface.Flush();

#if RSG_PC && __D3D11
	D3D11_ONLY(GRCDEVICE.LockContext());

	PF_START_TIMEBAR("DoPendingBufferUpdates");
	if (GRCDEVICE.CheckDeviceStatus())
	{
		// Is the pending render function NOT the default one ?
		//			if(m_pendingRenderFunctionCopy != NULL)
		{
			// Perform any pending texture/buffer updates now whilst we have the context locked (we don`t have a draw list to place them into).
			dlCmdGrcDeviceUpdateBuffer::DoPendingBufferUpdates();
		}
	}

	D3D11_ONLY(GRCDEVICE.UnlockContext());
#endif // RSG_PC && __D3D11
}

void fwRenderThreadInterface::HandleDeviceReset()
{
#if __D3D11
	if (!GRCDEVICE.IsReady())
	{
		gDrawListMgr->RemoveAll();
	}
#endif
}
#endif

void fwRenderThreadInterface::MainLoop(){

	// Take ownership of the pgDictionary linked list
	pgDictionaryBase::SetListOwnerThread(g_CurrentThreadId);

	(*m_InitCallback)();

	RAGETRACE_INITTHREAD("Render", 256, 1);
	PF_INIT_TIMEBARS("Render", 1280, 0.0f);
	PF_INIT_GPUBAR("Gpu", 256);
	USE_MEMBUCKET(MEMBUCKET_RENDER);

#if __PPU
	sysTimer timer;
#endif

	// Invalidate the pgDictionary linked list ownership.  As soon as we call
	// m_DrawThreadParams.SignalRunning, the main thread will take back
	// ownership.
	pgDictionaryBase::SetListOwnerThread(sysIpcCurrentThreadIdInvalid);

	// Signal that thread is running
	m_DrawThreadParams.SignalRunning();

	m_gameInterface->RenderThreadInit();

#if !__FINAL
#if __PS3
	cellGcmSetUserHandler(RsxCallback);
#endif // __PS3
#endif // !__FINAL

	// Loop until we are told to stop
	while (!m_DrawThreadParams.PollIsKilled())
	{
#if BACKTRACE_ENABLED
		BACKTRACE_TEST_CRASH;
#endif
		TELEMETRY_SET_THREAD_NAME(m_threadId, "Render");

#if GRCGFXCONTEXTPROFILE_ENABLE
		if (DUMP_GRCGFXCONTEXT_PROFILE)
		{
			DUMP_GRCGFXCONTEXT_PROFILE = false;
			gGrcGfxContextProfiler.dump();
		}
#endif

		// If there is a pending render function
		g_SetRenderFunctionToken.Lock();
		if(m_pendingRenderFunction != m_renderFunction)
		{
			m_renderFunction = m_pendingRenderFunction;
		}
		g_SetRenderFunctionToken.Unlock();

#if RSG_LOST_FOCUS_SUPPORT
		INPUT.UpdateLocale();
		GRCDEVICE.Manage();
#if !__RESOURCECOMPILER
		GRCDEVICE.CheckDeviceStatus();
#endif // !__RESOURCECOMPILER
		HANG_DETECT_SAVEZONE_ENTER();

		static int fallthroughTillPause = 5;

#if __FINAL || RSG_DURANGO
		while (GRCDEVICE.BlockUpdateThread() && fallthroughTillPause == 0)
#else
		while (GRCDEVICE.BlockUpdateThread() && PARAM_BlockOnLostFocus.Get() && fallthroughTillPause == 0)
#endif
		{
			fwTimer::StartLostFocusPause();
			INPUT.UpdateLocale();
			GRCDEVICE.Manage();
#if !__RESOURCECOMPILER
			GRCDEVICE.CheckDeviceStatus();
#endif // !__RESOURCECOMPILER

			// Must handle suspend resume on the render thread on Durango!
			DURANGO_ONLY(GRCDEVICE.HandleSuspendResume());
			sysIpcSleep(50);
		}

#if __FINAL || RSG_DURANGO
		if (GRCDEVICE.BlockUpdateThread())
#else
		if (GRCDEVICE.BlockUpdateThread() && PARAM_BlockOnLostFocus.Get())
#endif
		{
			fwTimer::StartLostFocusPause();
			fallthroughTillPause--;
		}
		else
		{
			if (fwTimer::IsLostFocusPaused())
			{
				fwTimer::EndLostFocusPause();
			}
			fallthroughTillPause = 5;
		}

		HANG_DETECT_SAVEZONE_EXIT("Focus Lost");
#endif
		// If we have set a render function
		DoRenderFunction();
	}

	m_DrawThreadParams.SignalRenderDone();

	m_gameInterface->RenderThreadShutdown();

#if RSG_PC
	GRCDEVICE.KillQueryFocusThread();
#endif

#if __XENON
	Shutdown();
#endif //__XENON
}

void fwRenderThreadInterface::DoRenderFunction()
{
	if(m_renderFunction)
	{
#if !__FINAL
		m_IsRenderThreadSynchronized = false;
#endif // !__FINAL

		m_DrawThreadParams.LockRenderMutex();

		// alternative render functions don't use the drawlists. Poll
		// to see if a drawlist has been setup. If one has after the render function has been called 
		// empty the drawlist and send a render complete message
#if __WIN32PC && __D3D11
		if (GRCDEVICE.IsReady())
#endif
		{
			m_renderFunction();

#if RSG_DURANGO || RSG_ORBIS
			// Sleep here to give other threads some time to run on the core when a custom render function 
			// is being executed. The custom render function could be quite expensive, and may cause the 
			// render thread to starve out lower priority threads, thereby causing them to take a very long 
			// amount of real-time to finish executing (what should take < 33 ms to execute).
			//
			// For reference, see B* 2107124 & B* 2109381. Only doing this for console platforms at the moment, 
			// as this should only be an issue on platforms with thread core affinity (which is only the consoles 
			// right now).
			sysIpcSleep(2);
#endif
		}

		bool bUpdateDone = m_DrawThreadParams.PollUpdateDone();
		m_gameInterface->NonDefaultPostRenderFunction(bUpdateDone);
		m_DrawThreadParams.UnlockRenderMutex();

		if (bUpdateDone) 
		{
#if !RSG_PC
			gDrawListMgr->SetSkippedFence();
#endif // !RSG_PC
			// empty drawlists at this point
			gDrawListMgr->RemoveAll();

			// Clear out the debug draw buffers so they don't stick around
			// and end up getting executed much later when their data is stale.
			DEBUG_DRAW_ONLY(grcDebugDraw::DiscardRenderBuffers());

			FlipRenderBuffer();
			gDrawListMgr->FlipRenderFenceIdx();
			m_DrawThreadParams.SignalRenderDone();

			// We may have to go again when doing early draw list kicks.
		}
	}
	else
	{
		DefaultRenderFunction();
	}
}

#if RSG_PC
static bool dumpDrawListsTillEmpty = false;
static int maxForceDrawListEmpty = 5;  //There should always be an empty drawlist when the device is lost and then recovered, but just in case that doesn't happen don't force discard for too long.
#endif

void fwRenderThreadInterface::DefaultRenderFunction()
{
#if __WIN32PC
#if __FINAL || (defined(__RGSC_DLL) && __RGSC_DLL)
	if (!GRCDEVICE.IsReady() WIN32PC_ONLY(|| (dumpDrawListsTillEmpty)) || (!GRCDEVICE.GetHasFocus() && GRCDEVICE.IsAllowPauseOnFocusLoss()) /* || GRCDEVICE.BlockUpdateThread()*/)
#else
	if (!GRCDEVICE.IsReady() WIN32PC_ONLY(|| (dumpDrawListsTillEmpty)) || (PARAM_BlockOnLostFocus.Get() && !GRCDEVICE.GetHasFocus() && GRCDEVICE.IsAllowPauseOnFocusLoss() /* || GRCDEVICE.BlockUpdateThread()*/))
#endif
	{
#if RSG_PC
		//There should always be an empty when a devicelost occurs, but just in case we only forcedump the drawlist a max of 5 frames.
		//This seems to be needed because occasionally the device comes back super quick and we have an extra drawlist left over from the previous resolution that needs to be dumped still.
		if (gDrawListMgr->HasPendingDrawLists() && maxForceDrawListEmpty/*!GRCDEVICE.IsReady()*/)
			dumpDrawListsTillEmpty = true;
		else 
			dumpDrawListsTillEmpty = false;

		if (GRCDEVICE.IsReady()) maxForceDrawListEmpty--;

		m_bUsingDefaultRenderFunc = false;
#endif

#if LOG_RENDER_FLUSH
		if( g_InFlush )
		{
			g_RenderThreadTempDebugLog.Push("LOG_RENDER_FLUSH (RENDER_THREAD): DefaultRenderFunction() (1) calling WaitTillUpdateDone()");
		}
#endif

		m_DrawThreadParams.WaitTillUpdateDone();		// block here until update is done

#if LOG_RENDER_FLUSH
		if( g_InFlush )
		{
			g_RenderThreadTempDebugLog.Push("LOG_RENDER_FLUSH (RENDER_THREAD): DefaultRenderFunction() (1) WaitTillUpdateDone() complete");
		}
#endif


		// empty drawlists at this point
		gDrawListMgr->RemoveAll();

		m_gameInterface->NonDefaultPostRenderFunction(true);
		m_gameInterface->FlushScaleformBuffers();

		// clear any references to textures from the immediate mode shaders
		FlipRenderBuffer();

		gDrawListMgr->FlipRenderFenceIdx();
		DEBUG_DRAW_ONLY(grcDebugDraw::DiscardRenderBuffers());
	}
	else
#endif
	{
#if RSG_PC
		maxForceDrawListEmpty = 5;

		m_bUsingDefaultRenderFunc = true;
#endif

		m_gameInterface->DefaultPreRenderFunction();

		// execute all built drawlists at this point
		gDrawListMgr->ExecuteAndRemoveAll();

		m_gameInterface->DefaultPostRenderFunction();

		// clear any references to textures from the immediate mode shaders
		FlipRenderBuffer();

		gDrawListMgr->FlipRenderFenceIdx();
#if __D3D11
		PF_START_TIMEBAR_IDLE("Wait for Main");

		// override bar colour for ragetrace
		RAGETRACE_ONLY(static pfTraceCounterId id("Wait for Main", Color32(0));)
		_RAGETRACE_START(id);
#endif // __D3D11

		u32 time = sysTimer::GetSystemMsTime();

#if LOG_RENDER_FLUSH
		if( g_InFlush )
		{
			g_RenderThreadTempDebugLog.Push("LOG_RENDER_FLUSH (RENDER_THREAD): DefaultRenderFunction() (2) calling WaitTillUpdateDone()");
		}
#endif
		m_DrawThreadParams.WaitTillUpdateDone();		// block here until update is done

#if LOG_RENDER_FLUSH
		if( g_InFlush )
		{
			g_RenderThreadTempDebugLog.Push("LOG_RENDER_FLUSH (RENDER_THREAD): DefaultRenderFunction() (2) WaitTillUpdateDone() complete");
		}
#endif

		m_TimeWaitedForUpdate = sysTimer::GetSystemMsTime() - time;
	}
#if !__FINAL
	m_RenderThreadFrameNumber++;
#endif // !__FINAL

	m_DrawThreadParams.SignalRenderDone();

	// Sleep to let other threads on this processor execute
	sysIpcSleep(0);

#if !__D3D11
	// NOTE: Didn't want to touch/break this if it's being happily used, but looks to 
	// me like it needs to follow the same DX11 path above for console too.
	PF_START_TIMEBAR_IDLE("Wait for Main");

	// override bar colour for ragetrace
	RAGETRACE_ONLY(static pfTraceCounterId id("Wait for Main", Color32(0));)
	_RAGETRACE_START(id);
#endif // !__D3D11
}

void fwRenderThreadInterface::SetRenderFunction(RenderFn fn)
{
	Assert(!gDrawListMgr->IsBuildingDrawList());
	sysCriticalSection cs(g_SetRenderFunctionToken);

	if (m_pendingRenderFunction != fn)
	{
		m_gameInterface->RenderFunctionChanged(m_pendingRenderFunction, fn);
		m_pendingRenderFunction = fn;
#if !__FINAL
		grcSetup::ResetTicker();
#endif // !__FINAL
	}
	m_pendingRenderFunctionCopy = fn;
}
void fwRenderThreadInterface::SetDefaultRenderFunction()
{
	SetRenderFunction(NULL);
}

DECLARE_THREAD_FUNC(RenderThreadEntryPoint)
{
	// Yes - srand/rand is threaded
	unsigned seed = (unsigned)time(0);
	if (PARAM_randomseed.Get())
	{
		seed = PARAM_randomseed.Get();
	}
	srand(seed);

	// Get the sample object
	fwRenderThreadInterface* pRT = (fwRenderThreadInterface*)ptr;

	sysThreadType::AddCurrentThreadType(sysThreadType::THREAD_TYPE_RENDER);

	pRT->MainLoop();
}

} // namespace rage
