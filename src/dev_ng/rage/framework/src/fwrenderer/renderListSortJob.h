#ifndef RENDERLIST_SORT_JOB_H_
#define RENDERLIST_SORT_JOB_H_

#include "system/dependency.h"

namespace rage {

class RenderListSortAsync
{
public:
	static bool RunFromDependency(const rage::sysDependency& dependency);
};

}

#endif
