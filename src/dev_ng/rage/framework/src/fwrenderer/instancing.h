//
// fwrenderer/instancing.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_FWINSTANCING_H_
#define INC_FWINSTANCING_H_

#include "grcore/instancebuffer.h"
#include "fwdrawlist/drawlist.h"
#include "fwtl/pool.h"

#if !(__D3D11 || RSG_ORBIS)
	#define IB_ALLOW_DL_ALLOCATION 1
#else
	#define IB_ALLOW_DL_ALLOCATION 0
#endif

namespace rage
{
class fwDLInstanceBuffer : public grcInstanceBufferBasic {
public:
	typedef grcInstanceBufferBasic parent_type;

	//Note: To have instance buffers use drawlist memory, you must call fwInstanceBuffer::InitClass()/fwInstanceBuffer::ShutdownClass() (rather than the grcInstanceBuffer:: version) so that proper factory functions are setup.
	static void InitClass();
	static void ShutdownClass();

	// PURPOSE:	Sets next link in chain for management
	virtual void SetNext(grcInstanceBuffer *n);

	// PURPOSE: Returns next buffer in list (as set up by grcInstanceBufferList)
	virtual grcInstanceBuffer *GetNext();

	//Accessing draw list offset.
	DrawListAddress GetDrawListAddress() const	{ return mDLAddress; }

	virtual AllocationType GetAllocationType() const	{ return ALLOC_DRAWLIST; }

protected:
	fwDLInstanceBuffer(DrawListAddress offset);
	virtual ~fwDLInstanceBuffer()	{ }

private:
	static grcInstanceBuffer *DLCreate();
	static void DLDestroy(const grcInstanceBuffer *);
	static void DLNextFrame();

	//Don't think we need a dlSharedDataInfo b/c I wouldn't think instance buffers would ever really be shared...
	DrawListAddress mDLAddress;

	//Next buffer in list chain, except stored as a DrawListAddress.
	DrawListAddress m_DLNext;
};

//Fwd Declare
namespace IBStatic
{
	class fwIBAllocator;
}

class fwPoolInstanceBuffer : public grcInstanceBufferBasic {
public:
	typedef grcInstanceBufferBasic parent_type;

	//Note: To have instance buffers use pool memory, you must call fwInstanceBuffer::InitClass()/fwInstanceBuffer::ShutdownClass() (rather than the grcInstanceBuffer:: version) so that proper allocator is setup.
	static void InitClass();
	static void ShutdownClass();

	FW_REGISTER_CLASS_POOL(fwPoolInstanceBuffer);

protected:
	friend class IBStatic::fwIBAllocator;
	fwPoolInstanceBuffer()			{ }
	virtual ~fwPoolInstanceBuffer()	{ }
};

#if RSG_DURANGO || RSG_ORBIS

class ALIGNAS(16) fwGPUMemInstanceBuffer : public grcInstanceBuffer
{
public:
	static void InitClass();
	static void ShutdownClass();

protected:
	fwGPUMemInstanceBuffer();
	virtual ~fwGPUMemInstanceBuffer();

public:
	void *Lock();
	void Unlock(size_t finalCount,size_t elemSizeQw);

	void *Lock(size_t finalCount, size_t elemSizeQw);
	void Unlock();

	const void *GetData() const;

	void SetNext(grcInstanceBuffer *n) { m_Next = static_cast<fwGPUMemInstanceBuffer *>(n); }
	grcInstanceBuffer* GetNext() { return m_Next; }
	size_t GetCount() const { return m_Count; }
	size_t GetElemSizeQW() const { return m_ElemSizeQW; }

	void Bind(PS3_ONLY(spuCmd_grcDevice__DrawInstancedPrimitive &params));
#if RSG_ORBIS
	sce::Gnm::Buffer *GetConstantBuffers() const { return const_cast<sce::Gnm::Buffer*>(&m_CB); }
#endif // RSG_ORBIS

private:
	static grcInstanceBuffer* Create();
	static void Destroy(const grcInstanceBuffer*);
	static fwGPUMemInstanceBuffer *NewABuffer();
public:
	static void NextFrame();

private:
	u32 m_Count, m_ElemSizeQW;

#if RSG_DURANGO
	void *m_Data;
#elif RSG_ORBIS
	sce::Gnm::Buffer m_CB;
#endif
	// Next buffer in list chain (as used by grcInstanceBufferList)
	fwGPUMemInstanceBuffer *m_Next;
};

#endif // RSG_DURANGO || RSG_ORBIS


#if IB_ALLOW_DL_ALLOCATION
typedef fwDLInstanceBuffer fwInstanceBuffer;
#else // IB_ALLOW_DL_ALLOCATION

#if RSG_ORBIS || RSG_DURANGO
typedef fwGPUMemInstanceBuffer fwInstanceBuffer;
#else // RSG_ORBIS || RSG_DURANGO
typedef fwPoolInstanceBuffer fwInstanceBuffer;
#endif // RSG_ORBIS || RSG_DURANGO

#endif // IB_ALLOW_DL_ALLOCATION

} // namespace rage

#endif //INC_FWINSTANCING_H_

