//
// fwevent/eventregdref.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FW_EVENT_REGDREF_H
#define FW_EVENT_REGDREF_H

// Framework headers
#include "fwtl/regdrefs.h"

// Turns on extended event count trapping - various extras at the cost
// of a slightly bigger base class.
#if !__FINAL
// Turn this on to run continue leak checking
#define EVENT_EXTENDED_REF_COUNT_INFO	0
#else // !__FINAL
#define EVENT_EXTENDED_REF_COUNT_INFO	0
#endif // !__FINAL

#if EVENT_EXTENDED_REF_COUNT_INFO
#include "fwtl/ExtRefAwareBase.h"
#endif



namespace rage
{

#if EVENT_EXTENDED_REF_COUNT_INFO
	typedef fwExtRefAwareBase fwEventBaseClass;		// TODO
#else
	typedef fwRefAwareBase fwEventBaseClass;
#endif

// Forward declarations
class fwEvent;

// PURPOSE: Forms a layer of abstraction between taskRegdRef's and a potential
// different AI task ref aware base class.  Most functions can be allowed to
// come through. Only constructors + anything return *this really needs to 
// be altered.
template <typename T>
class eventRegdRef : public fwRegdRef<T, fwEventBaseClass>
{
private:
	typedef fwRegdRef<T, fwEventBaseClass> tBase;

public:
	inline eventRegdRef();
	inline explicit eventRegdRef(T* p);
	inline explicit eventRegdRef( eventRegdRef<T> const & rhs );
	inline eventRegdRef& operator=(eventRegdRef const &rhs );
	inline eventRegdRef& operator=(T* rhs);
};

// PURPOSE: Type defines for standard framework aiEvent* pointers
typedef	eventRegdRef<fwEvent>									RegdfwEvent;
typedef	eventRegdRef<const fwEvent>								RegdConstfwEvent;

///////////////////////////////////////////////////////////////////////////////
// taskRegdRef inline - really just pass through to the fwRegdRef base class

template <typename T>
eventRegdRef<T>::eventRegdRef() : tBase()
{
}

template <typename T>
eventRegdRef<T>::eventRegdRef(T* p) : tBase(p)
{
}

template <typename T>
eventRegdRef<T>::eventRegdRef(eventRegdRef<T> const & rhs ) : tBase( rhs )
{
}

template <typename T>
eventRegdRef<T>& eventRegdRef<T>::operator=(eventRegdRef<T> const &rhs )
{
	tBase::operator=(rhs);
	return *this;
}

template <typename T>
eventRegdRef<T>& eventRegdRef<T>::operator=(T* rhs )
{
	tBase::operator=(rhs);
	return *this;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FW_EVENT_REGDREF_H
