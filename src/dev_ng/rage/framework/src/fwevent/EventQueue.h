// 
// fwEventQueue.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWEVENTQUEUE_H
#define FWEVENTQUEUE_H

#include "atl/queue.h"
#include "fwevent/event.h"
#include "fwevent/eventregdref.h"
#include "ai/task/taskchannel.h"

namespace rage {

template<int _Size, typename _Parent = void>
class fwEventQueue;

template<int _Size>
class fwEventQueue<_Size, void>
{
public:

	fwEventQueue()
#if __ASSERT
		: m_bDeletionGuard(false)
#endif // __ASSERT
	{
	}

	virtual ~fwEventQueue()
	{
		FlushAll();
	}

	virtual fwEvent* Add(const fwEvent& event)
	{
		// Check for a duplicate event already on the queue
		fwEvent* pQueueEvent = NULL;

		if (HasDuplicateEvent(event, &pQueueEvent))
		{
			return pQueueEvent;
		}

		if(m_Queue.IsFull())
		{
			FlushExpiredEvents();
		}

		if(m_Queue.IsFull())
		{
			// Try to make space by removing a lower priority event
			fwEvent* pLowestEvent = GetLowestPriorityEventThatCanBeRemoved();
			if(pLowestEvent && pLowestEvent->GetEventPriority() < event.GetEventPriority())
			{
				Remove(*pLowestEvent);
			}
		}

		if(!m_Queue.IsFull())
		{
			eventRegdRef<fwEvent> pClone;
			pClone = event.Clone();

			if (pClone)
			{
#if !__FINAL
				pClone->m_EventType = event.GetEventType();
#endif
				m_Queue.Push(pClone);
				return pClone;
			}
		}

		// Can't add event
		return NULL;
	}

	void Remove(fwEvent& event)
	{
		int index;
		if (Find(&event, &index))
		{
			Delete(index);
		}
		else
		{
			Assertf(0, "Event not found");
		}
	}

	void TickEvents()
	{
		for (int i=0; i<m_Queue.GetCount(); i++)
		{
			if (AssertVerify(m_Queue[i]))
			{
				m_Queue[i]->Tick();
			}
		}
	}

	void UnTickEvents()
	{
		for (int i=0; i<m_Queue.GetCount(); i++)
		{
			if (AssertVerify(m_Queue[i]))
			{
				m_Queue[i]->UnTick();
			}
		}
	}

	virtual void FlushAll() 
	{
		for (int i=0; i<m_Queue.GetCount(); i++)
		{
			if (AssertVerify(m_Queue[i]))
			{
				Delete(i, false);
			}
		}

		m_Queue.Reset();
	}

	void FlushExpiredEvents()
	{
		int curr = 0;

		while (curr < m_Queue.GetCount())
		{
			if (AssertVerify(m_Queue[curr]) && !m_Queue[curr]->GetBeingProcessed() && m_Queue[curr]->HasExpired())
			{
				Delete(curr);
			}
			else
			{
				curr++;
			}
		}		
	}

	void FlushEventsOfType(const int iEventType)
	{
		int curr = 0;

		while (curr < m_Queue.GetCount())
		{
			if (AssertVerify(m_Queue[curr]) && m_Queue[curr]->GetEventType() == iEventType)
			{
				Delete(curr);
			}
			else
			{
				curr++;
			}
		}
	}

	//
	// Event querying
	//

	int GetNumEvents() const 
	{ 
		return m_Queue.GetCount(); 
	}

	bool IsFull() const
	{
		return m_Queue.IsFull();
	}

	fwEvent* GetEventOfType(const int iEventType) const
	{
		for (int i=0; i<m_Queue.GetCount(); i++)
		{
			if (AssertVerify(m_Queue[i]) && m_Queue[i]->GetEventType() == iEventType)
			{
				return m_Queue[i];
			}
		}

		return NULL;
	}

	fwEvent* GetEventByIndex(const int iEventIndex) const 
	{ 
		return m_Queue[iEventIndex]; 
	}

	virtual fwEvent* GetLowestPriorityEventThatCanBeRemoved() const
	{
		fwEvent* pLowestPriorityEvent = NULL;
		int iLowestPriority = 0;

		for(int i=0; i<m_Queue.GetSize(); i++)
		{
			fwEvent* pEvent = m_Queue[i];

			if(AssertVerify(pEvent))
			{
				if(!pEvent->GetBeingProcessed() && (!pLowestPriorityEvent || pEvent->GetEventPriority() <= iLowestPriority))
				{
					iLowestPriority = pEvent->GetEventPriority();
					pLowestPriorityEvent = pEvent;
				}
			}
		}

		return pLowestPriorityEvent;
	}

	bool HasEvent(const fwEvent& event) const 
	{ 
		return Find(const_cast<fwEvent*>(&event)); 
	}

	bool HasEventOfType(const fwEvent& event) const
	{
		return GetEventOfType(event.GetEventType()) != NULL;
	}

	bool HasDuplicateEvent(const fwEvent& event, fwEvent** ppQueueEvent = NULL) const
	{
		for (int i=0; i<m_Queue.GetCount(); i++)
		{
			fwEvent* pQueueEvent = m_Queue[i];

			if(AssertVerify(pQueueEvent) && *pQueueEvent == event)
			{
				if (ppQueueEvent)
					*ppQueueEvent = pQueueEvent;

				return true;
			}
		}

		return false;
	}

#if __DEV
	void Print() const
	{
		taskDisplayf("event queue : size:%d", m_Queue.GetSize());

		s32 count = 0;
		for (s32 i=0; i < m_Queue.GetCount(); i++)
		{
			if (m_Queue[i])
			{
				taskDisplayf("%d: %s - priority:%d, persist:%s, time:%d/%d", i, m_Queue[i]->GetName().c_str(), m_Queue[i]->GetEventPriority(), m_Queue[i]->IsPersistant() ? "true": "false", m_Queue[i]->GetAccumulatedTime(), m_Queue[i]->GetLifeTime());
				count++;
			}
		}
		taskDisplayf("%d events in the queue", count);
	}
#endif //__DEV

#if __ASSERT
	// PURPOSE: Guard against deletion
	void SetDeletionGuard(const bool bGuard) { m_bDeletionGuard = bGuard; }
#endif // __ASSERT

protected:

	// PURPOSE: Find an object in the queue
	// PARAMS: 
	//		t - Object to be found,
	//		indexOut - Index of object, if present.	
	// RETURNS: True on success; false if object not found
	// NOTES: Reason this is here is that using atQueue::Find with RegdRefs causes ambiguity
	// as both overload operator==.  By going through this function, we make sure that it 
	// knows it's just the pointer type it's comparing against.
	bool Find(fwEvent *t, int* indexOut=NULL) const
	{
		int n=0;
		for (; n<m_Queue.GetCount(); n++)
		{
			if (m_Queue[n].Get() == t)
			{
				if (indexOut)
					*indexOut = n;
				return true;
			}
		}
		return false;
	}

	// PURPOSE: Delete event
	void Delete(const int index, const bool deleteFromQueue = true)
	{
		Assert(!m_bDeletionGuard);
		delete m_Queue[index];
		if(deleteFromQueue)
			m_Queue.Delete(index);
	}

private:

	// Queue
	atQueue<RegdfwEvent, _Size> m_Queue;

#if __ASSERT
	bool m_bDeletionGuard;
#endif // __ASSERT
};

template<int _Size, typename _Type>
class fwEventQueueCriticalSection
{
public:
	fwEventQueueCriticalSection(const fwEventQueue<_Size, _Type>* eventQueue) : m_EventQueue(eventQueue)
	{
		eventQueue->Lock();
	}

	~fwEventQueueCriticalSection()
	{
		m_EventQueue->Unlock();
	}

private:
	const fwEventQueue<_Size, _Type>* m_EventQueue;
};

template<int _Size, typename _Type>
class fwEventQueueProtectedSection
{
public:
	fwEventQueueProtectedSection(const fwEventQueue<_Size, _Type>* eventQueue) : m_EventQueue(eventQueue)
	{
		eventQueue->EnterProtectedSection();
	}

	~fwEventQueueProtectedSection()
	{
		m_EventQueue->ExitProtectedSection();
	}

private:
	const fwEventQueue<_Size, _Type>* m_EventQueue;
};

#define FWEVENTQUEUE_CRITICAL_SECTION fwEventQueueCriticalSection<_Size, _Parent> ps(this);
#define FWEVENTQUEUE_PROTECTED_SECTION fwEventQueueProtectedSection<_Size, _Parent> ps(this);

template<int _Size, typename _Parent>
class fwEventQueue : public fwEventQueue<_Size, void>, public _Parent
{
public:
	fwEventQueue()
		: fwEventQueue<_Size, void>()
		, _Parent()
	{
	}

	virtual ~fwEventQueue()
	{
	}

	virtual fwEvent* Add(const fwEvent& event)
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		return fwEventQueue<_Size, void>::Add(event);
	}

	void Remove(fwEvent& event)
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::Remove(event);
	}

	void TickEvents()
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::TickEvents();
	}

	void UnTickEvents()
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::UnTickEvents();
	}

	virtual void FlushAll() 
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::FlushAll();
	}

	void FlushExpiredEvents()
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::FlushExpiredEvents();
	}

	void FlushEventsOfType(const int iEventType)
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::FlushEventsOfType(iEventType);
	}

	int GetNumEvents() const 
	{ 
		FWEVENTQUEUE_CRITICAL_SECTION
		return fwEventQueue<_Size, void>::GetNumEvents();
	}

	bool IsFull() const
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		return fwEventQueue<_Size, void>::IsFull();
	}

	fwEvent* GetEventOfType(const int iEventType) const
	{
		FWEVENTQUEUE_PROTECTED_SECTION
		return fwEventQueue<_Size, void>::GetEventOfType(iEventType);
	}

	fwEvent* GetEventByIndex(const int iEventIndex) const 
	{ 
		FWEVENTQUEUE_PROTECTED_SECTION
		return fwEventQueue<_Size, void>::GetEventByIndex(iEventIndex);
	}

	virtual fwEvent* GetLowestPriorityEventThatCanBeRemoved() const
	{
		FWEVENTQUEUE_PROTECTED_SECTION
		return fwEventQueue<_Size, void>::GetLowestPriorityEventThatCanBeRemoved();
	}

	bool HasEvent(const fwEvent& event) const 
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		return fwEventQueue<_Size, void>::HasEvent(event);
	}

	bool HasEventOfType(const fwEvent& event) const
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		return fwEventQueue<_Size, void>::HasEventOfType(event);
	}

	bool HasDuplicateEvent(const fwEvent& event, fwEvent** ppQueueEvent = NULL) const
	{
		FWEVENTQUEUE_PROTECTED_SECTION
		return fwEventQueue<_Size, void>::HasDuplicateEvent(event, ppQueueEvent);
	}

#if __DEV
	void Print() const
	{
		FWEVENTQUEUE_CRITICAL_SECTION
		fwEventQueue<_Size, void>::Print();
	}
#endif //__DEV

	
	bool Find(fwEvent *t, int* indexOut=NULL) const
	{
		FWEVENTQUEUE_PROTECTED_SECTION
		return fwEventQueue<_Size, void>::Find(t, indexOut);
	}

	void Delete(const int index, const bool deleteFromQueue = true)
	{
		FWEVENTQUEUE_PROTECTED_SECTION
		fwEventQueue<_Size, void>::Delete(index, deleteFromQueue);
	}
};

#undef FWEVENTQUEUE_PROTECTED_SECTION

} // namespace rage

#endif // FWEVENTQUEUE_H
