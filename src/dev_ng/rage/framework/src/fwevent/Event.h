// 
// fwEvent.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWEVENT_H
#define FWEVENT_H

// Rage headers
#if !__FINAL
#include "atl/string.h"
#endif

#include "fwevent/eventregdref.h"
#include "system/ipc.h"
#include "system/threadtype.h"


namespace rage {

///////////////////////////////////////////////////////////////////////////////


//======================================================================
// fwEvent - base class for events
//======================================================================
class fwEvent : public fwEventBaseClass
{
	typedef fwEvent*		fwEventPtr;
public:

	fwEvent()
	: m_AccumulatedTime(0)
	, m_IsPersistent(false)
	, m_FlagForDeletion(false)
	, m_BeingProcessed(false)
	{
#if !__FINAL
		m_EventType = 0;
#endif
	}

	virtual ~fwEvent()
	{
	}

	virtual fwEvent* Clone() const = 0;

	virtual bool operator==(const fwEvent&) const = 0;

	virtual int GetEventType() const = 0;
	virtual int GetEventPriority() const = 0;
	virtual int GetLifeTime() const  { return 0; }

	// writes the event data to the given buffer
	virtual bool RetrieveData(u8* UNUSED_PARAM(data), const unsigned UNUSED_PARAM(sizeOfData)) const  = 0;

#if !__NO_OUTPUT
	// Debug function to return event name as a string for error output
	virtual atString GetName() const = 0;
#endif // !__FINAL

	virtual bool HasExpired() const
	{
		return (!m_IsPersistent && m_AccumulatedTime > GetLifeTime()) || m_FlagForDeletion;
	}

	bool IsPersistant() const				{ return m_IsPersistent; }
	void ForcePersistence(const bool val)	{ m_IsPersistent = val; }

	bool GetFlagForDeletion() const			{ return m_FlagForDeletion; }
	void SetFlagForDeletion(bool val)		{ m_FlagForDeletion = val; }

	bool GetBeingProcessed() const			{ return m_BeingProcessed; }
	void SetBeingProcessed(bool val)		{ m_BeingProcessed = val; }

	void ResetAccumulatedTime()				{ m_AccumulatedTime = 0; }

	// Does this event have priority over the otherEvent? Used if the queue overflows.
	virtual bool HasPriority(const fwEvent& otherEvent) const { return GetEventPriority() >= otherEvent.GetEventPriority(); }

	// Timer
	void Tick()						{ m_AccumulatedTime++; }
	void UnTick()					{ m_AccumulatedTime--; }
	int	 GetAccumulatedTime() const	{ return m_AccumulatedTime; }

#if !__FINAL
	// Debug variable storing the event type
	int m_EventType;
#endif // !__FINAL

private:
	
	fwEvent(const fwEvent&) {}
	fwEvent& operator=(const fwEvent&) { return *this; }

	// Event timer
	int m_AccumulatedTime;

	// Flags
	bool m_IsPersistent : 1;
	bool m_FlagForDeletion : 1;
	bool m_BeingProcessed : 1;
};

//======================================================================
// fwEventWithData - an event that contains a single struct
//======================================================================

template<class DerivedClass, class EventData>
class fwEventWithData : public fwEvent
{
public:

	fwEventWithData(const EventData& eventData) : m_EventData(eventData) {}

	virtual fwEvent* Clone() const { return rage_new DerivedClass(m_EventData); }

	virtual bool operator==(const fwEvent& event) const 
	{ 
		if (event.GetEventType() == this->GetEventType())
		{
			return (memcmp(static_cast<const fwEventWithData&>(event).m_EventData, this->m_EventData, sizeof(EventData)) == 0);
		}

		return false;
	}

	virtual bool RetrieveData(u8* data, const unsigned sizeOfData) const 
	{ 
		if (AssertVerify(data) && 
			Verifyf(sizeOfData == sizeof(EventData), "%s: Size of struct is wrong. Size \"%u\" should be \"%u\".", GetName().c_str, sizeOfData, sizeof(EventData)))
		{
			sysMemCpy(data, &this->m_EventData, sizeof(EventData));
		}

		return false;	
	}

protected:

	EventData m_EventData;
};

} // namespace rage


#endif // FWEVENT_H
