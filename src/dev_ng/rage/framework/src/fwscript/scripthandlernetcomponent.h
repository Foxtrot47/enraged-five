//
// scriptHandlerNetComponent.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NET_SCRIPT_HANDLER_BASE_H
#define NET_SCRIPT_HANDLER_BASE_H

// framework includes
#include "fwnet/nettypes.h"
#include "fwscript/scriptid.h"
#include "fwsys/timer.h"

// rage includes
#include "atl/dlist.h"
#include "atl/inlist.h"
#include "data/base.h"
#include "data/bitbuffer.h"
#include "fwtl/pool.h"

namespace rage
{

class scriptHandler;
class netPlayer;
class netObject;
class netScriptMgrBase;
struct ReceivedMessageData;
class msgScriptJoin;
class msgScriptJoinAck;
class msgScriptJoinHostAck;
class msgScriptHandshake;
class msgScriptHandshakeAck;
class msgScriptLeave;
class msgScriptLeaveAck;
class msgScriptMigrateHost;
class msgScriptMigrateHostFailAck;
class msgScriptHostRequest;
class msgScriptNewHost;
class msgScriptVerifyHost;
class msgScriptVerifyHostAck;
class msgScriptBotJoin;
class msgScriptBotJoinAck;
class msgScriptBotHandshake;
class msgScriptBotHandshakeAck;
class msgScriptBotLeave;
class netHostBroadcastDataHandlerBase;
class netPlayerBroadcastDataHandlerBase;

typedef s16 ScriptParticipantId;
typedef s16 ScriptSlotNumber;
typedef u16 HostToken;

/*
Description of the script handler state machine and message protocol:

A candidate is another player who is also trying to run the script but has not yet been accepted by the host into the script session.
A participant is a player who has been accepted into the script session by the host and who is now running the script properly.
A host is the player arbitrating this script session.

A new script starts and registers itself with the script manager. A script handler is assigned to it and the script is put in a waiting state.

The handler runs through the following states:

State NETSCRIPT_INTERNAL_JOIN:

Add ourselves as a candidate
Send msgScriptJoin to all machines in scope
If machines in scope, go to NETSCRIPT_INTERNAL_JOINING
Else, make ourselves host and go to NETSCRIPT_INTERNAL_PLAYING

State NETSCRIPT_INTERNAL_JOINING:

Wait for all machines to respond with either a msgScriptJoinAck (from a candidate or participant) or a msgScriptJoinHostAck (from the host).
If script has been terminated, go to NETSCRIPT_INTERNAL_LEAVE
If we are accepted by host, go to NETSCRIPT_INTERNAL_ACCEPTED
If we are rejected by host, go to NETSCRIPT_INTERNAL_LEAVE
If there are participants but no host, start again, go to NETSCRIPT_INTERNAL_JOIN
If there are only candidates:
If we are the candidate with the lowest gamer id, make ourselves host and go to NETSCRIPT_INTERNAL_PLAYING
Else start again, go to NETSCRIPT_INTERNAL_JOIN
If there are no other candidates, make ourselves host and go to NETSCRIPT_INTERNAL_PLAYING

State NETSCRIPT_INTERNAL_ACCEPTED

If there are any candidates or participants send a msgScriptHandshake to each one. Go to NETSCRIPT_INTERNAL_HANDSHAKING
Else we must be host (other players have left), go to NETSCRIPT_INTERNAL_PLAYING

State NETSCRIPT_INTERNAL_HANDSHAKING

Wait for all machines to respond with a msgScriptHandshakeAck
Else go to NETSCRIPT_INTERNAL_PLAYING

State NETSCRIPT_INTERNAL_PLAYING

The script is allowed to proceed.
Wait for the script to terminate, then go to NETSCRIPT_INTERNAL_LEAVE

State NETSCRIPT_INTERNAL_LEAVE

Send a msgScriptLeave to all candidates and participants
Go to NETSCRIPT_INTERNAL_LEAVING

State NETSCRIPT_INTERNAL_LEAVING

Wait for all machines to respond with a msgScriptLeaveAck.
Go to NETSCRIPT_INTERNAL_TERMINATING

State NETSCRIPT_INTERNAL_TERMINATING

Wait for the script to terminate.
Shutdown.
*/

//PURPOSE
//  Created by a script handler when a network game starts. Manages the joining and hosting of a currently running script which a number of 
//  machines are running the script simultaneously. One machine arbitrates (hosts) the script and accepts joiners. All machines synchronise 
//  the script's state between them using net array handlers.
class scriptHandlerNetComponent
{
public:

	static const unsigned MAX_NUM_NET_COMPONENTS					= MAX_NUM_ACTIVE_NETWORK_SCRIPTS;
	static const int	  INVALID_PARTICIPANT_ID					= -1;
	static const int	  INVALID_SLOT								= -1;
	static const int	  INVALID_HOST_TOKEN						= 0;
	static const int	  TIME_HOSTING_BEFORE_FIRST_HOST_MIGRATION	= 60000; // the length of time a new host will host the script session before attempting to migrate the host
	static const int	  TIME_BETWEEN_HOST_MIGRATION_ATTEMPTS	    = 10000;  // the length of time between host migration attempts
	static const int	  TIME_BETWEEN_HOST_VERIFY_ATTEMPTS			= 1000;   // the length of time between local host verify attempts
	static const int	  TIME_BETWEEN_HOST_REQUESTS				= 250;   // the length of time between the sending of host request messages

	// set via a tunable for B*2944584
	static bool ms_useNoHostFix;

public:

    // external script states queried by script commands
    enum eExternalState
    {
		NETSCRIPT_NOT_ACTIVE,
		NETSCRIPT_JOINING,
        NETSCRIPT_PLAYING,
        NETSCRIPT_TERMINATED,
        NETSCRIPT_FAILED_SESSION_FULL,
		NETSCRIPT_FAILED_NO_JOIN_IN_PROGRESS,
		NETSCRIPT_FAILED_TEAM_FULL,
		NETSCRIPT_FINISHED
    };

	enum eRejectionReason 
	{
		REJECTED_INVALID = -1,
		REJECTED_CANNOT_PROCESS,
		REJECTED_NO_ROOM_IN_SCRIPT_SESSION,
		REJECTED_NO_JOIN_IN_PROGRESS,
		REJECTED_NO_ROOM_IN_TEAM,
		NUM_REJECTION_REASONS
	};

	enum ePendingHostContext
	{
		PENDING_HOST_NONE,
		PENDING_HOST_JOINING,
		PENDING_HOST_MIGRATION,
		PENDING_HOST_VERIFY_LOCAL,
		PENDING_HOST_VERIFY_REMOTE
	};

protected:

    // internal script handler states only used within handler
    enum eInternalState
    {
		NETSCRIPT_INTERNAL_START,
		NETSCRIPT_INTERNAL_JOIN,
        NETSCRIPT_INTERNAL_JOINING,
		NETSCRIPT_INTERNAL_RESTARTING,
		NETSCRIPT_INTERNAL_RESTARTED,
		NETSCRIPT_INTERNAL_ACCEPTED,
		NETSCRIPT_INTERNAL_HANDSHAKING,
		NETSCRIPT_INTERNAL_READY_TO_PLAY,
		NETSCRIPT_INTERNAL_PLAYING,
		NETSCRIPT_INTERNAL_LEAVE,
        NETSCRIPT_INTERNAL_LEAVING,
		NETSCRIPT_INTERNAL_TERMINATING,
		NETSCRIPT_INTERNAL_FINISHED,
    };

public:

	//PURPOSE
    // Holds info on players participating in the script session.
    class participantData
    {
    public:

		explicit participantData(const netPlayer& player)
			:  m_Player(&player)
			,  m_ParticipantId(INVALID_PARTICIPANT_ID)
			,  m_SlotNumber(INVALID_SLOT)
			,  m_TimeJoined(0)
		{
		}

		participantData(const netPlayer& player, ScriptParticipantId pid, ScriptSlotNumber sn)
			:  m_Player(&player)
			,  m_ParticipantId(pid)
			,  m_SlotNumber(sn)
			,  m_TimeJoined(fwTimer::GetSystemTimeInMilliseconds())
		{
			Assert(pid>=0);
			Assert(sn>=0);
		}

		FW_REGISTER_CLASS_POOL(participantData);

        const netPlayer*		GetPlayer() const				{ Assert(m_Player); return m_Player; }
        ScriptParticipantId     GetParticipantId() const		{ Assert(m_Player); Assert(m_ParticipantId != INVALID_PARTICIPANT_ID); return m_ParticipantId;}
		ScriptSlotNumber        GetSlotNumber() const			{ Assert(m_Player); Assert(m_SlotNumber != INVALID_SLOT);  return m_SlotNumber;}
		u32						GetTimeInScriptSession() const	{ Assert(m_Player); Assert(m_TimeJoined != 0); return fwTimer::GetSystemTimeInMilliseconds() - m_TimeJoined; }

	public:

		inlist_node<participantData> m_ListLink;

    protected:

        const netPlayer*		m_Player;
        ScriptParticipantId     m_ParticipantId;    // an incrementing id assigned by the host to each new participant. Used to determine the new host when the previous host leaves.
        ScriptSlotNumber        m_SlotNumber;       // the slot this participant is using (the index used when accessing the script broadcast data)
		u32						m_TimeJoined;		// the system time the participant joined the script session
	};

	typedef inlist<participantData, &participantData::m_ListLink> ParticipantsList;

public:

    scriptHandlerNetComponent(scriptHandler& parentHandler);
 
	virtual ~scriptHandlerNetComponent();

	//PURPOSE
	//	Updates the component.
	//RETURNS
	//  true if the parent handler can be destroyed
    virtual bool Update();
 
	bool						IsActive() const					{ return m_State != NETSCRIPT_INTERNAL_FINISHED;}
	bool						IsPlaying() const					{ return m_State == NETSCRIPT_INTERNAL_PLAYING;}
	void						SetMaxNumParticipants(u32 num);
	u32							GetMaxNumParticipants() const		{ return static_cast<u32>(m_MaxNumParticipants);}
	PlayerFlags					GetPlayerParticipants() const		{ return m_playerParticipants; }
	const netPlayer*			GetHost() const						{ return m_HostData ? m_HostData->GetPlayer() : 0; }
	int							GetHostSlot() const					{ return m_HostData ? m_HostData->GetSlotNumber() : -1; }
	HostToken					GetHostToken() const				{ return m_HostToken; }
	bool						IsHostMigrating() const				{ return m_PendingHost != NULL;}
	bool						IsHostLocal() const;
	const netPlayer*			GetPendingHost() const				{ return m_PendingHost; }
	int							GetLocalSlot() const				{ return m_MySlotNumber; }
	ScriptParticipantId			GetLocalParticipantId() const;
	unsigned                    GetNumParticipants() const			{ return m_NumParticipants; }
	bool						IsAcceptingPlayers() const			{ return m_AcceptingPlayers; }
	void                        AcceptPlayers(bool bAccept)			{ m_AcceptingPlayers = bAccept; }
	bool						IsWaitingForAcks() const			{ return m_WaitingForAcks != 0; }

	//PURPOSE
	//  Sets the team reservations for a team based script.
	//PARAMS
	//	numTeams	- the number of teams involved in the script
	//	teamSizes	- the number of participant slots reserved for each team
	void SetTeamReservations(u32 numTeams, u32 teamReservations[]);

	//PURPOSE
    // Called when the script this handler represents is terminating
	//RETURNS
	//  true if the handler can be destroyed
    bool Terminate();

	//PURPOSE
	// This returns the external state of the handler, used by the script it represents. The script has to wait until this function
	// returns NETSCRIPT_INTERNAL_PLAYING before proceeding, otherwise it must terminate if it gets a fail state returned.
    eExternalState  GetState() const;

	//PURPOSE
    // Called when the given player joins a game session
    virtual void PlayerHasJoined(const netPlayer& player);

	//PURPOSE
	// Called when the given player leaves the game session
	virtual void PlayerHasLeft(const netPlayer& player) { HandleLeavingPlayer(player, true); }

	//PURPOSE
	// Returns true if the script is able to migrate the host
	bool CanMigrateScriptHost() const;

	//PURPOSE
	// Called when we want to migrate hosting to another machine
	virtual void MigrateScriptHost(const netPlayer& player);

	//PURPOSE
	// Returns true if there is a participant using the given slot
	bool IsParticipantActive(int slotNumber) const;

	//PURPOSE
    // Returns the slot allocated to the given player by the host
    int GetSlotParticipantIsUsing(const netPlayer& player) const;

	//PURPOSE
	// Returns the player using the given slot
    const netPlayer* GetParticipantUsingSlot(int slotNumber) const;

	//PURPOSE
	// Returns the time the given player has been participating in the script session
	u32 GetTimePlayerHasBeenAParticipant(int slotNumber) const;

	//PURPOSE
	// Returns true if the given player is in scope of this script (may potentially be running it)
    bool IsPlayerInScope(const netPlayer& player) const;

	//PURPOSE
	// Returns true if the given player is a candidate of this script (is trying to run the script but not yet accepted into the script session)
    bool IsPlayerACandidate(const netPlayer& player) const;

	//PURPOSE
	// Returns true if the given player is a participant of this script (has been accepted into the script session and is running the script properly)
    bool IsPlayerAParticipant(const netPlayer& player) const;

	//PURPOSE
	// Returns true if the given player is a participant of this script (has been accepted into the script session and is running the script properly)
	bool IsPlayerAParticipant(PhysicalPlayerIndex playerIndex) const;

	//PURPOSE
	// Called by the script manager once it sends a message to the given player
    void MessageSentToPlayer(const netPlayer& player) { SetWaitingForAck(player); }

    // Handlers dealing with all the different messages sent between script handlers
    virtual void HandleJoinMsg					(const msgScriptJoin& msg,         const ReceivedMessageData &messageData);
    virtual void HandleJoinAckMsg				(const msgScriptJoinAck& msg,      const ReceivedMessageData &messageData);
    virtual void HandleJoinHostAckMsg			(const msgScriptJoinHostAck& msg,  const ReceivedMessageData &messageData);
    virtual void HandleHandshakeMsg				(const msgScriptHandshake& msg,    const ReceivedMessageData &messageData);
    virtual void HandleHandshakeAckMsg			(const msgScriptHandshakeAck& msg, const ReceivedMessageData &messageData);
    virtual void HandleLeaveMsg					(const msgScriptLeave& msg,        const ReceivedMessageData &messageData);
	virtual void HandleLeaveAckMsg				(const msgScriptLeaveAck& msg,     const ReceivedMessageData &messageData);
	virtual void HandleBotJoinMsg				(const msgScriptBotJoin& msg,      const ReceivedMessageData &messageData);
	virtual void HandleBotJoinAckMsg			(const msgScriptBotJoinAck& msg,   const ReceivedMessageData &messageData);
	virtual void HandleBotHandshakeMsg			(const msgScriptBotHandshake& msg, const ReceivedMessageData &messageData);
	virtual void HandleBotHandshakeAckMsg		(const msgScriptBotHandshakeAck& msg, const ReceivedMessageData &messageData);
	virtual void HandleBotLeaveMsg				(const msgScriptBotLeave& msg,      const ReceivedMessageData &messageData);
	virtual void HandleMigrateHostMsg			(const msgScriptMigrateHost& msg,  const ReceivedMessageData &messageData);
	virtual void HandleMigrateHostFailAckMsg	(const msgScriptMigrateHostFailAck& msg,  const ReceivedMessageData &messageData);
	virtual void HandleHostRequestMsg			(const msgScriptHostRequest& msg,  const ReceivedMessageData &messageData);
	virtual void HandleNewHostMsg				(const msgScriptNewHost& msg,		 const ReceivedMessageData &messageData);
	virtual void HandleVerifyHostMsg			(const msgScriptVerifyHost& msg,	 const ReceivedMessageData &messageData);
	virtual void HandleVerifyHostAckMsg			(const msgScriptVerifyHostAck& msg,   const ReceivedMessageData &messageData);

	//PURPOSE
	// Returns true if the script has finished for all players
	bool GetScriptHasFinished() const	{ return m_ScriptFinished; }

	//PURPOSE
	// Called by the script informing the handler that the script session is finishing for all players
	void SetScriptHasFinished()			{ m_ScriptFinished = true; }

	//PURPOSE
	// Returns true if a new player bot can be permitted to try joining the script session
	//PARAMS
	// bAssert - if set this will assert if the bot can not be added
	bool CanAddPlayerBot(const netPlayer& botPlayer, bool bAssert = true);

	//PURPOSE
	// Tries to add a player bot to the script session
	void AddPlayerBot(const netPlayer& botPlayer);

	//PURPOSE
	// Removes a player bot from the script session
	void RemovePlayerBot(const netPlayer& botPlayer);

	//PURPOSE
	// Flags the net component to start hosting or request to host if there is already a host
	void RequestToBeHost();

#if __BANK
	//PURPOSE

	//PURPOSE
	//  Forcibly resends all broadcast data
	void ForceResendOfAllBroadcastData();
#endif

	virtual void RegisterHostBroadcastData(unsigned* address, unsigned size, bool HasHighFrequencyUpdates = false BANK_ONLY(, const char* debugArrayName = 0));
	virtual void RegisterPlayerBroadcastData(unsigned* address, unsigned size, bool HasHighFrequencyUpdates = false BANK_ONLY(, const char* debugArrayName = 0));

	void TagForDebugPlayerBroadcastData(bool bTagged);

	int  GetNumHostBroadcastDatasRegistered() const { return m_NumHostBroadcastData; }
	int  GetNumPlayerBroadcastDatasRegistered() const { return m_NumPlayerBroadcastData; }

	netHostBroadcastDataHandlerBase*	GetHostBroadcastDataHandler(unsigned handlerNum);
	netPlayerBroadcastDataHandlerBase*	GetPlayerBroadcastDataHandler(unsigned handlerNum, unsigned participantSlot);
	netPlayerBroadcastDataHandlerBase*	GetLocalPlayerBroadcastDataHandler(unsigned handlerNum);

	void FinishBroadcastingData();

	u32 GetSizeOfHostBroadcastData() const { return m_sizeOfHostBroadcastData; }

	//PURPOSE
	// Prevents host migration for one frame
	void PreventHostMigration() { m_PreventHostMigration = true; }

#if __BANK
	void AddBandwidthIn(unsigned dataRead);
	void AddBandwidthOut(unsigned dataWritten);

	u32 GetTimeHandlerActive() const { return m_timeHandlerActive; }
	u32 GetTotalBandwidthIn() const { return m_totalBandwidthIn; }
	u32 GetTotalBandwidthOut() const { return m_totalBandwidthOut; }
	u32 GetAverageBandwidthIn() const { return m_timeHandlerActive > 0 ? (u32)((float)m_totalBandwidthIn / ((float)m_timeHandlerActive/1000.0f)) : 0; } 
	u32 GetAverageBandwidthOut() const { return m_timeHandlerActive > 0 ? (u32)((float)m_totalBandwidthOut / ((float)m_timeHandlerActive/1000.0f)) : 0; } 
	u32 GetPeakBandwidthIn() const { return m_peakBandwidthIn; }
	u32 GetPeakBandwidthOut() const { return m_peakBandwidthOut; }
#endif

protected:

	//PURPOSE
	// Sets the next internal state of the handler
    void SetNextState(eInternalState state);

	//PURPOSE
	// Adds a new player to the candidates list
    void AddPlayerAsCandidate(const netPlayer& player);

	//PURPOSE
	// Removes the given player from the candidates list
    void RemovePlayerAsCandidate(const netPlayer& player);

	//PURPOSE
	// Returns true when the net component can proceed with the joining process
	virtual bool CanStartJoining();

	//PURPOSE
	// Returns true when the net component can proceed with the hosting
	virtual bool CanStartHosting();

	//PURPOSE
	// Returns the next available slot for a new participant
	ScriptSlotNumber GetNextFreeParticipantSlot(const netPlayer& player);

	//PURPOSE
	// Tries to adds a new player to the participants list.
	//PARAMS
	// player - the player being added as a participant
	// participantId - the participant id assigned to the player, if successful
	// slotNumber - the slot number assigned to the player, if successful
	// rejection - if the player cannot be added, this will be set with the reason why.
	//RETURNS
	//	True if the player was successfully added
	virtual bool TryToAddPlayerAsParticipant(const netPlayer& player, ScriptParticipantId& participantId, ScriptSlotNumber& slotNumber, eRejectionReason& rejection);

	//PURPOSE
	// Adds a new or candidate player to the participants list, and off the candidates list if necessary.
	//PARAMS
	// player - the player being added as a participant
	// participantId - the participant id assigned to the player
	// slotNumber - the slot number assigned to the player
    virtual bool AddPlayerAsParticipant(const netPlayer& player, ScriptParticipantId participantId, ScriptSlotNumber slotNumber);
 
	//PURPOSE
	// Removes the given player from the participants list
	virtual void RemovePlayerAsParticipant(const netPlayer& player);

	//PURPOSE
	// Called when a player is leaving, either the script session (via a leave message) or the network session
	//PARAMS
	// player - the leaving player
	// playerLeavingSession - if true, the player is leaving the network session, and therefore all scripts
	virtual void HandleLeavingPlayer(const netPlayer& player, bool playerLeavingSession);

	//PURPOSE
	// Flushes the candidates list
	//PARAMS
	// bLog - if set, this action is written to the script log file
    void RemoveAllCandidates(bool bLog = true);

	//PURPOSE
	// Flushes the participants list
	//PARAMS
	// bLog - if set, this action is written to the script log file
	void RemoveAllParticipants(bool bLog = true);

	//PURPOSE
	// Determines the host from all candidates when we have no participants. The candidate with the lowest gamer id becomes the host of the script.
    const netPlayer* DetermineHostFromCandidates() const;

	//PURPOSE
	// Called when the host of the script leaves. Determines the new host from the player in the participants list with the lowest
	// participant id (the player who has been participating in the script session the longest)
    const netPlayer* DetermineHostFromParticipants() const;

	//PURPOSE
	// Sets the new host of this script
    virtual void SetNewScriptHost(const netPlayer* pPlayer, HostToken hostToken, bool bBroadcast = true);

	//PURPOSE
	// Returns the participant data for the given slot number
	participantData* GetParticipantDataForSlot(int slotNumber);
	const participantData* GetParticipantDataForSlot(int slotNumber) const;

	//PURPOSE
	// Returns the participant data for the given player
	participantData* GetParticipantsData(const netPlayer& player);
	const participantData* GetParticipantsData(const netPlayer& player) const;

	//PURPOSE
	// Sets the internal state to NETSCRIPT_INTERNAL_START and does any necessary cleanup
	virtual void RestartJoiningProcess();

	//PURPOSE
	// Returns true if we are waiting for an ack for a message we sent to the given player
    bool IsWaitingForAck(const netPlayer& player) const;

	//PURPOSE
	//  Sets a flag indicating that we are waiting for an ack for a message we sent to the given player
	void SetWaitingForAck(const netPlayer& player);

	//PURPOSE
	// Clears the flag indicating that we are waiting for an ack for a message we sent to the given player
	void ClearWaitingForAck(const netPlayer& player);

	//PURPOSE
	// Called when the net component enters a playing state
	virtual void StartPlaying() {}

	//PURPOSE
	// Called when the net component is about to leave the script session
	//RETURNS
	// True when the cleanup is complete and the component is free to leave the session
	virtual bool DoLeaveCleanup();

	//PURPOSE
	// Called when the net component is about to terminate after leaving
	//RETURNS
	// True when the cleanup is complete and the component is free to terminate
	virtual bool DoTerminationCleanup();

	//PURPOSE
	// Informs the broadcast data array handlers about a new participant
	void InformBroadcastDataArraysOfNewParticipant(const participantData& participant);

	//PURPOSE
	// Informs the broadcast data array handlers about a leaving participant
	void InformBroadcastDataArraysOfLeavingParticipant(const participantData& participant);

	//PURPOSE
	//  Called when we are assuming hosting due to the previous host dropping out
	void VerifyLocalHost();

	//PURPOSE
	// Manages host migration
	void HandleHostMigration();

	//PURPOSE
	// Called when an attempt to migrate the hosting to another machine failed
	void HostMigrationFailed(bool bPlayerLeft);

	//PURPOSE
	// Sets the pending host information, when we are expecting this player to host
	void SetPendingHost(const netPlayer& pendingHost, ePendingHostContext context);

	//PURPOSE
	// Clears the pending host information
	void ClearPendingHost();

protected:

	scriptHandler&						m_ParentHandler;		   // the script handler containing this network component
    eInternalState						m_State;                   // the current internal state the script handler is in
    PlayerFlags							m_WaitingForAcks;          // flags for each player indicating whether we are waiting for an ack from them
    ParticipantsList					m_Candidates;              // the list of players trying to participate in this script (trying to run the script but not accepted by the host yet)
 	const participantData* 				m_HostData;                // the participant hosting (arbitrating) this script
	ScriptSlotNumber					m_MySlotNumber;			   // the slot that our player is assigned (this is an optimization to avoid searching the participants list too often)
	PlayerFlags							m_activeParticipants;	   // current active partipants (a flag for each participant slot). An optimisation to avoid searching participant list.
	PlayerFlags							m_playerParticipants;	   // players currently participating (a flag for each physical player index). An optimisation to avoid searching participant list.
	HostToken							m_HostToken;			   // a token that is incremented every time a host migrates	
	const netPlayer*					m_LastHost;				   // the previous player who was hosting
	const netPlayer*					m_PendingHost;			   // set when we are joining or the host is migrating away from this machine
	const netPlayer*					m_PreferredInitialHost;    // set when we are joining and we received a host request from a candidate
	u32									m_sizeOfHostBroadcastData; // the total size of the host broadcast data associated with this script

#if __BANK
	// broadcast data array handler bandwidth:
	RecorderId							m_bandwidthRecorderId;		
	u32									m_timeHandlerActive;		// the time the handler has been active with other players in scope
	u32									m_totalBandwidthIn;			// the total bandwidth recorded coming in
	u32									m_totalBandwidthOut;		// the total bandwidth recorded going out
	u32									m_peakBandwidthIn;			// the peak bandwidth recorded coming in
	u32									m_peakBandwidthOut;			// the peak bandwidth recorded going out
#endif

	// the array of players participating in this script (accepted by the host)
	atFixedArray<participantData*, MAX_NUM_PHYSICAL_PLAYERS>	m_ParticipantsArray; 
	s32									m_HostMigrationTimer;	   // time until the next host migration, to avoid thrashing
	u8									m_MaxNumParticipants;	   // the maximum number of participants allowed for this script
	u8									m_NumTeams;				   // the number of teams that are participating in this script
	atFixedArray<u8, MAX_NUM_TEAMS>		m_TeamReservations;		   // the number of participant slots reserved for each team
    u8									m_NumParticipants;         // the current number of participants
    u8									m_NumCandidates;           // the current number of candidates
	s8									m_RejectionReason;		   // set when we are rejected from a script session by the host
	u8									m_NumHostBroadcastData;	   // the number of host broadcast datas registered with this script
	u8									m_NumPlayerBroadcastData;  // the number of player broadcast datas registered with this script
	u8									m_PendingHostContext;	   // the reason why a host is pending
	bool								m_Terminated : 1;          // set when the script is terminating 
	bool								m_AcceptingPlayers : 1;	   // if set players are permitted to join the script session when a network game is running
    bool								m_Accepted : 1;            // set when we get an accepted response from the host
	bool								m_PlayerJoined : 1;        // set when a new player joins the session during the joining process
	bool								m_ScriptFinished : 1;	   // set when the script is finishing for all players. The script has to inform the handler about this.
	bool								m_HostRequested : 1;	   // set when we have requested to host the script session
	bool								m_PendingHostingFailed : 1;// set when we fail to host a session after verifying to become the new host
	mutable bool						m_PreventHostMigration : 1;// set when we want to prevent host migration. This is a reset flag this is only set for one frame and must be repeatedly set when migration is to be prevented.
	bool								m_VerifyHostBroadcastData : 1; // set when we need to send checksums for all the broadcast data to the new host (set when the host changes during initialisation)
};

} // namespace rage

#endif  // NET_SCRIPT_HANDLER_BASE_H
