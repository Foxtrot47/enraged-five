//
// scriptObjInfo.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef SCRIPT_OBJ_INFO_H
#define SCRIPT_OBJ_INFO_H

// framework includes
#include "fwnet/netserialisers.h"
#include "fwscript/scripthandlernetcomponent.h"

namespace rage
{
// script object ID defines
#define SIZEOF_HANDLER_OBJECT_ID (16)
#define INVALID_SCRIPT_OBJECT_ID (0)
#define SCRIPT_OBJECT_HOST_SLOT_INDEX (255)			// the slot index assigned to host created objects
#define SCRIPT_OBJECT_UNNETWORKED_SLOT_INDEX (254)	// the slot index assigned to non-networked script objects
#define MAKE_SCRIPT_OBJECT_ID(slot, index) (slot<<SIZEOF_HANDLER_OBJECT_ID | index); // builds a composite script object ID based on the specified slot and index
#define GET_SLOT_FROM_SCRIPT_OBJECT_ID(scriptObjectID) (scriptObjectID>>SIZEOF_HANDLER_OBJECT_ID)
#define GET_INDEX_FROM_SCRIPT_OBJECT_ID(scriptObjectID)(scriptObjectID&((1<<SIZEOF_HANDLER_OBJECT_ID)-1))
#define IS_HOST_OBJECT_ID(scriptObjectID) (GET_SLOT_FROM_SCRIPT_OBJECT_ID(scriptObjectID)==SCRIPT_OBJECT_HOST_SLOT_INDEX)
#define IS_UNNETWORKED_OBJECT_ID(scriptObjectID) (GET_SLOT_FROM_SCRIPT_OBJECT_ID(scriptObjectID)==SCRIPT_OBJECT_UNNETWORKED_SLOT_INDEX)
#define SIZEOF_SCRIPT_OBJECT_ID (32)

typedef u32 ScriptObjectId;

class scriptInfoBase;
class scriptIdBase;

//PURPOSE
// Holds script information related to a network object
class scriptObjInfoBase
{
public:

	scriptObjInfoBase() : 
    m_ScriptObjectId(INVALID_SCRIPT_OBJECT_ID),
	m_HostToken(0)
	{
	}

	scriptObjInfoBase(const ScriptObjectId scriptObjectId, HostToken hostToken) : 
	m_ScriptObjectId(scriptObjectId),
	m_HostToken(hostToken)
	{
		Assertf(scriptObjectId != INVALID_SCRIPT_OBJECT_ID, "Trying to construct a scriptObjInfo with an invalid script object id");
	}

	scriptObjInfoBase(const scriptObjInfoBase& scriptInfo) :
	m_ScriptObjectId(scriptInfo.m_ScriptObjectId),
	m_HostToken(scriptInfo.m_HostToken)
	{
		Assertf(scriptInfo.IsValid(), "Trying to construct a scriptObjInfo from an invalid script info");
	}

	virtual ~scriptObjInfoBase() {}

	virtual scriptIdBase&		GetScriptId()					= 0;
	virtual const scriptIdBase&	GetScriptId() const				= 0;
	ScriptObjectId				GetObjectId() const				{ return m_ScriptObjectId; }
	HostToken					GetHostToken() const			{ return m_HostToken; }
	bool						IsValid() const					{ return m_ScriptObjectId != INVALID_SCRIPT_OBJECT_ID; }
	void						Invalidate()					{ m_ScriptObjectId = INVALID_SCRIPT_OBJECT_ID; }

	scriptObjInfoBase& operator=(const scriptObjInfoBase& other)
	{
		if (this != &other)
		{
			From(other);
		}
		return *this;
	}

	bool operator==(const scriptObjInfoBase& other) const
	{
		return Equals(other);
	}

	bool operator!=(const scriptObjInfoBase& other) const
	{
		return !Equals(other);
	}

	bool IsScriptHostObject() const
	{
		return (GET_SLOT_FROM_SCRIPT_OBJECT_ID(m_ScriptObjectId) == SCRIPT_OBJECT_HOST_SLOT_INDEX);
	}

	bool IsScriptNonNetworkedObject() const
	{
		return (GET_SLOT_FROM_SCRIPT_OBJECT_ID(m_ScriptObjectId) == SCRIPT_OBJECT_UNNETWORKED_SLOT_INDEX);
	}

	bool IsScriptClientObject() const
	{
		u32 slot = GET_SLOT_FROM_SCRIPT_OBJECT_ID(m_ScriptObjectId);
		return (slot != SCRIPT_OBJECT_HOST_SLOT_INDEX && slot != SCRIPT_OBJECT_UNNETWORKED_SLOT_INDEX);
	}

	void GetLogName(char *buffer, unsigned bufferSize) const
    {
        unsigned slotIndex   = GET_SLOT_FROM_SCRIPT_OBJECT_ID(m_ScriptObjectId);
        unsigned objectIndex = GET_INDEX_FROM_SCRIPT_OBJECT_ID(m_ScriptObjectId);

        if(slotIndex == SCRIPT_OBJECT_HOST_SLOT_INDEX)
        {
            formatf(buffer, bufferSize, "SCRIPT_HOST_ID_%d", objectIndex);
        }
        else if (slotIndex == SCRIPT_OBJECT_UNNETWORKED_SLOT_INDEX)
		{
			formatf(buffer, bufferSize, "SCRIPT_UNNETWORKED_ID_%d", objectIndex);
		}
		else
        {
            formatf(buffer, bufferSize, "SCRIPT_CLIENT_ID_%d_%d", slotIndex, objectIndex);
        }
    }

	void Serialise(CSyncDataBase& serialiser)
	{
		bool bLargeHostToken = (m_HostToken >= (1<<SIZEOF_HOST_TOKEN));

		GetScriptId().Serialise(serialiser);
		SERIALISE_UNSIGNED(serialiser, m_ScriptObjectId, SIZEOF_SCRIPT_OBJECT_ID, "Script Object ID");

		SERIALISE_BOOL(serialiser, bLargeHostToken);

		if (bLargeHostToken || serialiser.GetIsMaximumSizeSerialiser())
		{
			SERIALISE_UNSIGNED(serialiser, m_HostToken, sizeof(HostToken)<<3, "Host token");
		}
		else
		{
			SERIALISE_UNSIGNED(serialiser, m_HostToken, SIZEOF_HOST_TOKEN, "Host token");
		}
	}

protected:

	virtual void From(const scriptObjInfoBase& other)
	{
		m_ScriptObjectId = other.GetObjectId();
		m_HostToken = other.GetHostToken();
	}

	virtual bool Equals(const scriptObjInfoBase& other) const 
	{
		return (m_ScriptObjectId == other.GetObjectId());
	}

protected:

	static const unsigned SIZEOF_HOST_TOKEN = 3;

	ScriptObjectId      m_ScriptObjectId;       // the ID assigned to this object from a script handler
	HostToken			m_HostToken;			// the token of the host that created this object
};

//PURPOSE
// Holds script information related to a network object
// ScriptIdType - The script id specific to your project.
template<class ScriptIdType>
class scriptObjInfo : public scriptObjInfoBase
{
public:

	scriptObjInfo(const ScriptIdType &scriptId, const ScriptObjectId scriptObjectId, HostToken hostToken) : 
	  scriptObjInfoBase(scriptObjectId, hostToken),
	  m_ScriptId(scriptId)
	  {
	  }

	  scriptObjInfo(const scriptObjInfo& other) : 
	  scriptObjInfoBase(static_cast<const scriptObjInfoBase&>(other)),
	  m_ScriptId(other.m_ScriptId)
	  {
	  }

	  scriptObjInfo(const scriptObjInfoBase& other) : 
	  scriptObjInfoBase(other)
	  {
		  const scriptObjInfo* scrObjInfo = SafeCast(const scriptObjInfo, &other);

		  if (scrObjInfo)
		  {
			  m_ScriptId	= scrObjInfo->m_ScriptId;
			  m_HostToken	= scrObjInfo->m_HostToken;
		  }
	  }

	  scriptObjInfo() 
	  {
	  }

	  scriptIdBase&		 GetScriptId() 				{ return m_ScriptId; }
	  const scriptIdBase& GetScriptId() const		{ return m_ScriptId; }

protected:

	virtual void From(const scriptObjInfoBase& other)
	{
		scriptObjInfoBase::From(other);

		const scriptObjInfo* scrObjInfo = SafeCast(const scriptObjInfo, &other);

		if (scrObjInfo)
		{
			m_ScriptId = scrObjInfo->m_ScriptId;
		}
	}

	virtual bool Equals(const scriptObjInfoBase& other) const 
	{
		if (scriptObjInfoBase::Equals(other))
		{
			const scriptObjInfo* scrObjInfo = SafeCast(const scriptObjInfo, &other);

			if (scrObjInfo)
			{
				return (m_ScriptId == scrObjInfo->m_ScriptId);
			}
		}

		return false;
	}

protected:

	ScriptIdType		m_ScriptId;					// the script that created this object
};

} // namespace rage

#endif  // SCRIPT_OBJ_INFO_H
