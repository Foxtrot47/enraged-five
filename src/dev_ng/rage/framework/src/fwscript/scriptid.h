//
// scriptId.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef SCRIPT_ID_H
#define SCRIPT_ID_H

// framework includes
#include "fwnet/netchannel.h"
#include "fwnet/netserialisers.h"

// rage includes
#include "atl/hashstring.h"
#include "data/bitbuffer.h"
#include "script/value.h"

namespace rage
{

class scrThread;
class Vector3;
class netLoggingInterface;

//PURPOSE
//  Used to identify a unique instance of a script
class scriptIdBase
{
public:

	scriptIdBase() {}
	virtual ~scriptIdBase() {}

	//PURPOSE
	// Initialises the script id from a script thread
	virtual void Init(const scrThread& scriptThread) = 0;

	//PURPOSE
	// Returns true if the script id has been initialised
	virtual bool IsValid() const = 0;

	//PURPOSE
	// Generates a hash to uniquely identify this script name
	virtual const atHashValue GetScriptNameHash() const = 0;

	//PURPOSE
	// Generates a hash to uniquely identify this script id
	virtual atHashValue GetScriptIdHash() const = 0;

	//PURPOSE
	// Returns the string used to represent the script id in the network logs
	virtual const char* GetLogName() const = 0;

	//PURPOSE
	// Reads the script id data from a bit buffer
	virtual void Read(datBitBuffer &bb) = 0;

	//PURPOSE
	// Writes the script id data to a bit buffer
	virtual void Write(datBitBuffer &bb) const = 0;

	//PURPOSE
	// Returns the current size of the script id data
	virtual unsigned GetSizeOfMsgData() const = 0;

	//PURPOSE
	// Returns the maximum size of the script id data
	virtual unsigned GetMaximumSizeOfMsgData() const = 0;

	//PURPOSE
	// Logs the script id data in a network log
	virtual void Log(netLoggingInterface &log) = 0;

	scriptIdBase& operator=(const scriptIdBase& idBase)
	{
		if (this != &idBase)
		{
			From(idBase);
		}
		return *this;
	}

	bool operator==(const scriptIdBase& idBase) const
	{
		return Equals(idBase);
	}

	bool operator!=(const scriptIdBase& idBase) const
	{
		return !Equals(idBase);
	}

	// EJ: This is an ugly hack - fix by adding proper virtual functions!
	//
	void Serialise(CSyncDataBase& serialiser)
	{
		switch (serialiser.GetType())
		{
			case CSyncDataBase::SYNC_DATA_READ:
			{
				Read(reinterpret_cast<CSyncDataReader&>(serialiser).GetBitBuffer());
				break;
			}
			case CSyncDataBase::SYNC_DATA_WRITE:
			{
				Write(reinterpret_cast<CSyncDataReader&>(serialiser).GetBitBuffer());
				break;
			}
			case CSyncDataBase::SYNC_DATA_LOG:
			{
				Log(*serialiser.GetLog());
				break;
			}
			case CSyncDataBase::SYNC_DATA_CALC:
			{
				SERIALISE_DATABLOCK(serialiser, NULL, static_cast<int>(GetMaximumSizeOfMsgData()));
				break;
			}
			default:
			{
				Assertf(0, "Called a scriptIdBase::Serialise with an unspecified serialiser"); 
			}
		}
	}

	void Serialise(datImportBuffer &bb)
	{
		Read(static_cast<datBitBuffer&>(bb));
	}

	void Serialise(datExportBuffer &bb) 
	{
		Write(static_cast<datBitBuffer&>(bb));
	}

	void Serialise(datExportBuffer &bb) const
	{
		Write(static_cast<datBitBuffer&>(bb));
	}

protected:

	virtual void From(const scriptIdBase& other) = 0;
	virtual bool Equals(const scriptIdBase& other) const = 0;
	virtual void SetNameFromHash() = 0;
};

//PURPOSE
//  A basic script id, containing the script name or program id only
class scriptId : public scriptIdBase
{
public:

	static const unsigned SCRIPT_NAME_LEN		= 32;
	static const unsigned SIZEOF_NAME_HASH		= 32;

public:

	scriptId()
	{
		m_ScriptName[0] = 0;
	}

	explicit scriptId(const char* scriptName)
	{
		m_ScriptNameHash.SetFromString(scriptName);

		Assertf(strlen(scriptName) <= SCRIPT_NAME_LEN, "scriptId::SCRIPT_NAME_LEN is not large enough");

		strncpy(m_ScriptName, scriptName, SCRIPT_NAME_LEN);
	}

	// Initialises the script id by setting the hash directly. In this case the name is not available.
	explicit scriptId(const atHashString& scriptHash)
	{
		m_ScriptNameHash = scriptHash;
		m_ScriptName[0] = 0;
	}

	scriptId(const scrThread& scriptThread)
	{
		InitScriptId(scriptThread);
	}

	scriptId(const scriptIdBase& other)
	{
		const scriptId* scrId = SafeCast(const scriptId, &other);

		if (scrId)
		{
			m_ScriptNameHash	= scrId->m_ScriptNameHash;

			safecpy(m_ScriptName, scrId->m_ScriptName);
		}
	} 

	scriptId(const scriptId& other)
	{
		m_ScriptNameHash	= other.m_ScriptNameHash;
		safecpy(m_ScriptName, other.m_ScriptName);
	}

	//PURPOSE
	// Initialises the script id from a script thread
	virtual void Init(const scrThread& scriptThread)
	{
		InitScriptId(scriptThread);
	}

	//PURPOSE
	// Returns true if the script id has been initialised
	virtual bool IsValid() const
	{ 
		return (m_ScriptNameHash.IsNotNull());
	}

	//PURPOSE
	// Resets the script id
	void Invalidate()
	{
		m_ScriptNameHash.Clear();
		m_ScriptName[0]		= 0;
	}

	const char* GetScriptName() const	{ return m_ScriptName; }

	//PURPOSE
	// Generates a hash to uniquely identify this script name
	virtual const atHashValue GetScriptNameHash() const { return m_ScriptNameHash; }

	//PURPOSE
	// Generates a hash to uniquely identify this script id
	virtual atHashValue GetScriptIdHash() const { return m_ScriptNameHash; }

	//PURPOSE
	// Returns the string used to represent the script id in the network logs
	virtual const char* GetLogName() const { return m_ScriptName; }

	//PURPOSE
	// Reads the script id data from a bit buffer
	virtual void Read(datBitBuffer &bb);

	//PURPOSE
	// Writes the script id data to a bit buffer
	virtual void Write(datBitBuffer &bb) const;

	//PURPOSE
	// Returns the current size of the script id data
	virtual unsigned GetSizeOfMsgData() const
	{
		return SIZEOF_NAME_HASH;
	}

	//PURPOSE
	// Returns the maximum size of the script id data
	virtual unsigned GetMaximumSizeOfMsgData() const
	{
		return GetSizeOfMsgData();
	}

	//PURPOSE
	// Logs the script id data in a network log
	virtual void Log(netLoggingInterface &log);

protected:

	void InitScriptId(const scrThread& scriptThread);

protected:

	virtual void From(const scriptIdBase& other)
	{
		const scriptId* scrId = SafeCast(const scriptId, &other);

		if (scrId)
		{
			m_ScriptNameHash	= scrId->m_ScriptNameHash;
			safecpy(m_ScriptName, scrId->m_ScriptName);
		}
	}

	virtual bool Equals(const scriptIdBase& other) const 
	{
		const scriptId* scrId = SafeCast(const scriptId, &other);

		if (scrId)
		{
			return (IsValid() && scrId->m_ScriptNameHash == m_ScriptNameHash);
		}

		return false;
	}

protected:

	atHashValue	m_ScriptNameHash;				// the script name hash 

	char	m_ScriptName[SCRIPT_NAME_LEN];	// the script name (used in dev builds for logging and help with debugging) 
};

} // namespace rage

#endif  // SCRIPT_ID_H
