//
// netScriptHandlerMessages.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwscript/scripthandlermessages.h"

#include "fwnet/netlog.h"
#include "fwnet/optimisations.h"

namespace rage
{

NET_MESSAGE_IMPL( msgScriptJoin );
NET_MESSAGE_IMPL( msgScriptJoinAck );
NET_MESSAGE_IMPL( msgScriptJoinHostAck );
NET_MESSAGE_IMPL( msgScriptHandshake );
NET_MESSAGE_IMPL( msgScriptHandshakeAck );
NET_MESSAGE_IMPL( msgScriptLeave );
NET_MESSAGE_IMPL( msgScriptLeaveAck );
NET_MESSAGE_IMPL( msgScriptBotJoin );
NET_MESSAGE_IMPL( msgScriptBotJoinAck );
NET_MESSAGE_IMPL( msgScriptBotHandshake );
NET_MESSAGE_IMPL( msgScriptBotHandshakeAck );
NET_MESSAGE_IMPL( msgScriptBotLeave );
NET_MESSAGE_IMPL( msgScriptMigrateHost );
NET_MESSAGE_IMPL( msgScriptMigrateHostFailAck );
NET_MESSAGE_IMPL( msgScriptHostRequest );
NET_MESSAGE_IMPL( msgScriptNewHost );
NET_MESSAGE_IMPL( msgScriptVerifyHost );
NET_MESSAGE_IMPL( msgScriptVerifyHostAck );

NETWORK_OPTIMISATIONS()

void msgScriptJoinAck::Log(netLoggingInterface& log) const
{
	switch (m_AckCode)
	{
	case SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT:
		log.WriteDataValue("REJECTED", "Not running script");
		break;
	case SCRIPT_ACK_CODE_PARTICIPANT:
		log.WriteDataValue("Participant id", "%d", m_ParticipantId);
		log.WriteDataValue("Slot num", "%d", m_SlotNumber);
		break;
	case SCRIPT_ACK_CODE_CANDIDATE:
		log.WriteDataValue("Candidate", "true");
		break;
	case SCRIPT_ACK_CODE_NOT_PROCESSED:
		log.WriteDataValue("REJECTED", "Couldn't be processed");
		break;
	default:
		Assert(0);
		break;
	}
}

void msgScriptJoinHostAck::Log(netLoggingInterface& log) const
{
	if (m_Accepted)
	{
		log.WriteDataValue("Host participant id", "%d", m_HostParticipantId);
		log.WriteDataValue("Host slot num", "%d", m_HostSlotNumber);
		log.WriteDataValue("Client participant id", "%d", m_ClientParticipantId);
		log.WriteDataValue("Client slot num", "%d", m_ClientSlotNumber);
		log.WriteDataValue("Host token", "%d", m_HostToken);
	}
	else 
	{
		switch (m_RejectionReason)
		{
		case scriptHandlerNetComponent::REJECTED_INVALID:
			gnetAssert(0);
			break;
		case scriptHandlerNetComponent::REJECTED_CANNOT_PROCESS:
			log.WriteDataValue("Rejected", "Cannot process");
			break;
		case scriptHandlerNetComponent::REJECTED_NO_ROOM_IN_SCRIPT_SESSION:
			log.WriteDataValue("Rejected", "No room in script session");
			break;
		case scriptHandlerNetComponent::REJECTED_NO_JOIN_IN_PROGRESS:
			log.WriteDataValue("Rejected", "No join in progress");
			break;
		case scriptHandlerNetComponent::REJECTED_NO_ROOM_IN_TEAM:
			log.WriteDataValue("Rejected", "No room in team");
			break;
		case scriptHandlerNetComponent::NUM_REJECTION_REASONS:
			gnetAssert(0);
			break;
		}
	}
}

void msgScriptHandshake::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Participant id", "%d", m_ParticipantId);
	log.WriteDataValue("Slot num", "%d", m_SlotNumber);
}

void msgScriptHandshakeAck::Log(netLoggingInterface& log) const
{
	switch (m_AckCode)
	{
	case SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT:
		log.WriteDataValue("REJECTED", "Not running script");
		break;
	case SCRIPT_ACK_CODE_PARTICIPANT:
		log.WriteDataValue("Participant id", "%d", m_ParticipantId);
		log.WriteDataValue("Slot num", "%d", m_SlotNumber);

		if (m_Host)
		{
			log.WriteDataValue("Host", "true");
			log.WriteDataValue("Host Token", "%d", m_HostToken);
		}
		break;
	case SCRIPT_ACK_CODE_CANDIDATE:
		log.WriteDataValue("Candidate", "true");
		break;
	case SCRIPT_ACK_CODE_NOT_PROCESSED:
		log.WriteDataValue("REJECTED", "Couldn't be processed");
		break;
	default:
		Assert(0);
		break;
	}
}

void msgScriptBotJoinAck::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Participant id", "%d", m_ParticipantId);
	log.WriteDataValue("Slot num", "%d", m_SlotNumber);
}

void msgScriptBotHandshake::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Participant id", "%d", m_ParticipantId);
	log.WriteDataValue("Slot num", "%d", m_SlotNumber);
}

void msgScriptMigrateHost::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Host token", "%d", m_HostToken);
	log.WriteDataValue("Size of net array data", "%d", m_SizeOfNetArrayData);
}

void msgScriptNewHost::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Host token", "%d", m_HostToken);
}

void msgScriptVerifyHost::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Host token", "%d", m_HostToken);
}

void msgScriptVerifyHostAck::Log(netLoggingInterface& log) const
{
	log.WriteDataValue("Rejected", "%s", m_Rejected ? "true" : "false");

	if (m_Rejected)
	{
		log.WriteDataValue("Host token", "%d", m_HostToken);
		log.WriteDataValue("Valid host", "%s", m_ValidHost ? "true" : "false");
	}
}

} // namespace rage
