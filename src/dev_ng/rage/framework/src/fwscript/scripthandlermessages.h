//
// netScriptHandlerMessages.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NET_SCRIPT_HANDLER_MESSAGES_H
#define NET_SCRIPT_HANDLER_MESSAGES_H

// framework headers
#include "fwscript/scripthandlernetcomponent.h"
#include "fwscript/scriptid.h"

// rage headers
#include "net/message.h"

namespace rage
{

class netLoggingInterface;

// used by join and handshake acks:
enum eAckCode
{
	SCRIPT_ACK_CODE_INVALID = -1,
	SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT,
	SCRIPT_ACK_CODE_PARTICIPANT,
	SCRIPT_ACK_CODE_CANDIDATE,
	SCRIPT_ACK_CODE_NOT_PROCESSED,
	SCRIPT_ACK_CODE_NUM_CODES
};

static unsigned const SIZEOF_ACK_CODE = datBitsNeeded<SCRIPT_ACK_CODE_NUM_CODES>::COUNT;
static unsigned const SIZEOF_TEAM = datBitsNeeded<MAX_NUM_TEAMS>::COUNT + 1;

//PURPOSE
// Sent when a player is trying to start a new script
class msgScriptJoin
{
public:

	NET_MESSAGE_DECL(msgScriptJoin, MSG_SCRIPT_JOIN);

	msgScriptJoin(scriptIdBase& scriptId, bool bRequestToHost = false) : 
		m_ScriptId(&scriptId), 
		m_Team(INVALID_TEAM),
		m_RequestToHost(bRequestToHost)
	{ 
	}

	msgScriptJoin(scriptIdBase& scriptId, int team, bool bRequestToHost = false) : 
		m_ScriptId(&scriptId), 
		m_Team(team),
		m_RequestToHost(bRequestToHost)
	{ 
	}

	NET_MESSAGE_SER(bb, message)
	{
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerInt(message.m_Team, SIZEOF_TEAM);

		if (!bb.SerBool(message.m_RequestToHost))
		{
			success = false;
		}

		return success;
	}

	int GetMessageDataBitSize() const
	{
		return (m_ScriptId->GetSizeOfMsgData() + SIZEOF_TEAM);
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const
	{
	}

	const char* GetSendStr() const		{ return "SENDING_JOIN"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_JOIN"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*		m_ScriptId;
	int					m_Team;
	bool				m_RequestToHost;
};

//PURPOSE
// Sent in reply to a msgScriptJoin from a non-scripthost
class msgScriptJoinAck
{
public:

	NET_MESSAGE_DECL(msgScriptJoinAck, MSG_SCRIPT_JOIN_ACK);

	msgScriptJoinAck(scriptIdBase& scriptId) :
		m_ScriptId(&scriptId),
		m_AckCode(SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	msgScriptJoinAck(scriptIdBase& scriptId, ScriptParticipantId participantId, ScriptSlotNumber slotNumber) :
		m_ScriptId(&scriptId),
		m_AckCode(SCRIPT_ACK_CODE_PARTICIPANT),
		m_ParticipantId(participantId),
		m_SlotNumber(slotNumber)
	{
	}

	msgScriptJoinAck(scriptIdBase& scriptId, unsigned ackCode) :
		m_ScriptId(&scriptId),
		m_AckCode(ackCode),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
		Assert(ackCode != SCRIPT_ACK_CODE_PARTICIPANT);
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif

		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerUns(message.m_AckCode, SIZEOF_ACK_CODE);
	
		if (message.m_AckCode == SCRIPT_ACK_CODE_PARTICIPANT)
		{
			if (!bb.SerUns(message.m_ParticipantId, sizeof(ScriptParticipantId)<<3))
			{
				success = false;
			}

			if (!bb.SerUns(message.m_SlotNumber, sizeof(ScriptSlotNumber)<<3))
			{
				success = false;
			}
		}

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return success;
	}

	int GetMessageDataBitSize() const
	{
		int size = m_ScriptId->GetSizeOfMsgData() + SIZEOF_ACK_CODE;

		if (m_AckCode == SCRIPT_ACK_CODE_PARTICIPANT)
		{
			size += sizeof(ScriptParticipantId)<<3;
			size += sizeof(ScriptSlotNumber)<<3;
		}
		
		return size;
	}

	void Log(netLoggingInterface& log) const;

	const char* GetSendStr() const		{ return "SENDING_JOIN_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_JOIN_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	u32						m_AckCode;
	ScriptParticipantId		m_ParticipantId;
	ScriptSlotNumber		m_SlotNumber;
};

//PURPOSE
// Sent in reply to a msgScriptJoin from a script host
class msgScriptJoinHostAck
{
public:

	NET_MESSAGE_DECL(msgScriptJoinHostAck, MSG_SCRIPT_JOIN_HOST_ACK);

	static const unsigned SIZEOF_REJECTION_REASON = datBitsNeeded<scriptHandlerNetComponent::NUM_REJECTION_REASONS>::COUNT;

	msgScriptJoinHostAck(scriptIdBase& scriptId) 
	: m_ScriptId(&scriptId)
	, m_Accepted(false)
	, m_RejectionReason(scriptHandlerNetComponent::REJECTED_INVALID)
	, m_HostParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID)
	, m_HostSlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	, m_ClientParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID)
	, m_ClientSlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	// constructor for accepted response
	msgScriptJoinHostAck(scriptIdBase& scriptId,	
						  ScriptParticipantId hostParticipantId, ScriptSlotNumber hostSlotNumber, 
						  ScriptParticipantId clientParticipantId, ScriptSlotNumber clientSlotNumber,
						  HostToken hostToken) 
	: m_ScriptId(&scriptId)
	, m_Accepted(true)
	, m_HostToken(hostToken)
	, m_RejectionReason(scriptHandlerNetComponent::REJECTED_INVALID)
	, m_HostParticipantId(hostParticipantId)
	, m_HostSlotNumber(hostSlotNumber)
	, m_ClientParticipantId(clientParticipantId)
	, m_ClientSlotNumber(clientSlotNumber)
	{
	}

	// constructor for rejected or ignored response
	msgScriptJoinHostAck(scriptIdBase& scriptId, 
						  ScriptParticipantId hostParticipantId, ScriptSlotNumber hostSlotNumber, 
						  scriptHandlerNetComponent::eRejectionReason rejectedReason)
	: m_ScriptId(&scriptId)
	, m_Accepted(false)
	, m_HostToken(0)
	, m_RejectionReason(rejectedReason)
	, m_HostParticipantId(hostParticipantId)
	, m_HostSlotNumber(hostSlotNumber)
	, m_ClientParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID)
	, m_ClientSlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerUns(message.m_HostParticipantId, sizeof(ScriptParticipantId)<<3) &&
					   bb.SerUns(message.m_HostSlotNumber, sizeof(ScriptSlotNumber)<<3) &&
					   bb.SerBool(message.m_Accepted);

		if (message.m_Accepted)
		{
			if (!bb.SerUns(message.m_ClientParticipantId, sizeof(ScriptParticipantId)<<3))
			{
				success = false;
			}

			if (!bb.SerUns(message.m_ClientSlotNumber, sizeof(ScriptSlotNumber)<<3))
			{
				success = false;
			}

			if (!bb.SerUns(message.m_HostToken, sizeof(message.m_HostToken)<<3))
			{
				success = false;
			}
		}
		else
		{
			if (!bb.SerUns(message.m_RejectionReason, SIZEOF_REJECTION_REASON))
			{
				success = false;
			}
		}

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return success;
	}

	int GetMessageDataBitSize() const
	{
		int size = m_ScriptId->GetSizeOfMsgData() + (sizeof(ScriptParticipantId)<<3) + (sizeof(ScriptSlotNumber)<<3) + 1;
	
		if (m_Accepted)
		{
			size += (sizeof(ScriptParticipantId)<<3) + (sizeof(ScriptSlotNumber)<<3) + (sizeof(m_HostToken)<<3);
		}
		else
		{
			size += SIZEOF_REJECTION_REASON;
		}

		return size;
	}

	void Log(netLoggingInterface& log) const;

	const char* GetSendStr() const		{ return "SENDING_JOIN_HOST_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_JOIN_HOST_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*										m_ScriptId;
	bool												m_Accepted;
	HostToken 											m_HostToken;
	scriptHandlerNetComponent::eRejectionReason			m_RejectionReason;
	ScriptParticipantId									m_HostParticipantId;
	ScriptSlotNumber									m_HostSlotNumber;
	ScriptParticipantId									m_ClientParticipantId;
	ScriptSlotNumber									m_ClientSlotNumber;
};

//PURPOSE
//  Sent by a script client to all other clients when the client is accepted by the script host
class msgScriptHandshake
{
public:

	NET_MESSAGE_DECL(msgScriptHandshake, MSG_SCRIPT_HANDSHAKE);

	msgScriptHandshake(scriptIdBase& scriptId) :
		m_ScriptId(&scriptId),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	msgScriptHandshake(scriptIdBase& scriptId, ScriptParticipantId participantId, ScriptSlotNumber slotNumber) :
		m_ScriptId(&scriptId),
		m_ParticipantId(participantId),
		m_SlotNumber(slotNumber)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerUns(message.m_ParticipantId, sizeof(ScriptParticipantId)<<3) &&
					   bb.SerUns(message.m_SlotNumber, sizeof(ScriptSlotNumber)<<3);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return success;
	}

	int GetMessageDataBitSize() const
	{
		return m_ScriptId->GetSizeOfMsgData() + (sizeof(ScriptParticipantId)<<3) + (sizeof(ScriptSlotNumber)<<3);
	}

	void Log(netLoggingInterface& log) const;

	const char* GetSendStr() const		{ return "SENDING_HANDSHAKE"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_HANDSHAKE"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	ScriptParticipantId		m_ParticipantId;
	ScriptSlotNumber		m_SlotNumber;
};

//PURPOSE
// An ack for a msgScriptHandshake msg
class msgScriptHandshakeAck
{
public:

	NET_MESSAGE_DECL(msgScriptHandshakeAck, MSG_SCRIPT_HANDSHAKE_ACK);

	msgScriptHandshakeAck(scriptIdBase& scriptId) :
		m_ScriptId(&scriptId),
		m_AckCode(SCRIPT_ACK_CODE_NUM_CODES),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT),
		m_HostToken(0),
		m_Host(false)
	{
	}

	msgScriptHandshakeAck(scriptIdBase& scriptId, ScriptParticipantId participantId, ScriptSlotNumber slotNumber) :
		m_ScriptId(&scriptId),
		m_AckCode(SCRIPT_ACK_CODE_PARTICIPANT),
		m_ParticipantId(participantId),
		m_SlotNumber(slotNumber),
		m_HostToken(0),
		m_Host(false)
	{
	}

	msgScriptHandshakeAck(scriptIdBase& scriptId, ScriptParticipantId participantId, ScriptSlotNumber slotNumber, bool host, HostToken hostToken) :
		m_ScriptId(&scriptId),
		m_AckCode(SCRIPT_ACK_CODE_PARTICIPANT),
		m_ParticipantId(participantId),
		m_SlotNumber(slotNumber),
		m_HostToken(hostToken),
		m_Host(host)
	{
	}

	msgScriptHandshakeAck(scriptIdBase& scriptId, unsigned ackCode) :
		m_ScriptId(&scriptId),
		m_AckCode(ackCode),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT),
		m_HostToken(0),
		m_Host(false)
	{
		Assert(ackCode != SCRIPT_ACK_CODE_PARTICIPANT);
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerUns(message.m_AckCode, SIZEOF_ACK_CODE);

		if (message.m_AckCode == SCRIPT_ACK_CODE_PARTICIPANT)
		{
			bool success = bb.SerUns(message.m_ParticipantId, sizeof(ScriptParticipantId)<<3) &&
						   bb.SerUns(message.m_SlotNumber, sizeof(ScriptSlotNumber)<<3) &&
						   bb.SerBool(message.m_Host);

			if (message.m_Host)
			{
				if (!bb.SerUns(message.m_HostToken, sizeof(HostToken)<<3))
				{
					success = false;
				}
			}
		}

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return success;
	}

	int GetMessageDataBitSize() const
	{
		int size = m_ScriptId->GetSizeOfMsgData() + SIZEOF_ACK_CODE;

		if (m_AckCode == SCRIPT_ACK_CODE_PARTICIPANT)
		{
			size += (sizeof(ScriptParticipantId)<<3) + (sizeof(ScriptSlotNumber)<<3) + 1;

			if (m_Host)
			{
				size += sizeof(HostToken)<<3;
			}
		}

		return size;
	}

	void Log(netLoggingInterface& log) const;

	const char* GetSendStr() const		{ return "SENDING_HANDSHAKE_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_HANDSHAKE_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	u32						m_AckCode;
	ScriptParticipantId		m_ParticipantId;
	ScriptSlotNumber		m_SlotNumber;
	HostToken				m_HostToken;			
	bool					m_Host;
};

//PURPOSE
// Sent when a script participant is leaving the script session
class msgScriptLeave
{
public:

	NET_MESSAGE_DECL(msgScriptLeave, MSG_SCRIPT_LEAVE);

	msgScriptLeave(scriptIdBase& scriptId) :
		m_ScriptId(&scriptId)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return m_ScriptId->GetSizeOfMsgData();
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const
	{
	}

	const char* GetSendStr() const		{ return "SENDING_LEAVE"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_LEAVE"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*	m_ScriptId;
};

//PURPOSE
// An ack for a msgScriptLeave msg
class msgScriptLeaveAck
{
public:

	NET_MESSAGE_DECL(msgScriptLeaveAck, MSG_SCRIPT_LEAVE_ACK);

	msgScriptLeaveAck(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);
	
#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return m_ScriptId->GetSizeOfMsgData();
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const
	{
	}

	const char* GetSendStr() const		{ return "SENDING_LEAVE_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_LEAVE_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*	m_ScriptId;
};


//PURPOSE
// Sent when a bot player is trying to start a new script
class msgScriptBotJoin
{
public:

	NET_MESSAGE_DECL(msgScriptBotJoin, MSG_SCRIPT_BOT_JOIN);

	msgScriptBotJoin(scriptIdBase& scriptId) : 
	m_ScriptId(&scriptId), 
	m_Team(INVALID_TEAM) 
	{ 
	}

	msgScriptBotJoin(scriptIdBase& scriptId, int team) : 
	m_ScriptId(&scriptId), 
	m_Team(team) 
	{ 
	}

	NET_MESSAGE_SER(bb, message)
	{
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerInt(message.m_Team, SIZEOF_TEAM);

		return success;
	}

	int GetMessageDataBitSize() const
	{
		return (m_ScriptId->GetSizeOfMsgData() + SIZEOF_TEAM);
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const
	{
	}

	const char* GetSendStr() const		{ return "SENDING_BOT_JOIN"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_BOT_JOIN"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*		m_ScriptId;
	int					m_Team;
};

//PURPOSE
// Sent in reply to a msgScriptBotJoin from a script host
class msgScriptBotJoinAck
{
public:

	NET_MESSAGE_DECL(msgScriptBotJoinAck, MSG_SCRIPT_BOT_JOIN_ACK);

	static const unsigned SIZEOF_REJECTION_REASON = datBitsNeeded<scriptHandlerNetComponent::NUM_REJECTION_REASONS>::COUNT;

	msgScriptBotJoinAck(scriptIdBase& scriptId) :
		m_ScriptId(&scriptId),
		m_Accepted(false),
		m_RejectionReason(scriptHandlerNetComponent::REJECTED_INVALID),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	// constructor for accepted response
	msgScriptBotJoinAck(scriptIdBase& scriptId, ScriptParticipantId participantId, ScriptSlotNumber slotNumber) :
		m_ScriptId(&scriptId),
		m_Accepted(true),
		m_RejectionReason(scriptHandlerNetComponent::REJECTED_INVALID),
		m_ParticipantId(participantId),
		m_SlotNumber(slotNumber)
	{
	}

	// constructor for rejected or ignored response
	msgScriptBotJoinAck(scriptIdBase& scriptId, scriptHandlerNetComponent::eRejectionReason rejectedReason) :
		m_ScriptId(&scriptId),
		m_Accepted(false),
		m_RejectionReason(rejectedReason),
		m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
		m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerBool(message.m_Accepted);

		if (message.m_Accepted)
		{
			if (!bb.SerUns(message.m_ParticipantId, sizeof(ScriptParticipantId)<<3))
			{
				success = false;
			}

			if (!bb.SerUns(message.m_SlotNumber, sizeof(ScriptSlotNumber)<<3))
			{
				success = false;
			}
		}
		else
		{
			if (!bb.SerUns(message.m_RejectionReason, SIZEOF_REJECTION_REASON))
			{
				success = false;
			}
		}

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return success;
	}

	int GetMessageDataBitSize() const
	{
		int size = m_ScriptId->GetSizeOfMsgData() + 1;

		if (m_Accepted)
		{
			size += (sizeof(ScriptParticipantId)<<3) + (sizeof(ScriptSlotNumber)<<3);
		}
		else
		{
			size += SIZEOF_REJECTION_REASON;
		}

		return size;
	}

	void Log(netLoggingInterface& log) const;

	const char* GetSendStr() const		{ return "SENDING_BOT_JOIN_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_BOT_JOIN_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*										m_ScriptId;
	bool												m_Accepted;
	scriptHandlerNetComponent::eRejectionReason			m_RejectionReason;
	ScriptParticipantId									m_ParticipantId;
	ScriptSlotNumber									m_SlotNumber;
};


//PURPOSE
//  Sent by a player bot to all other participants when the bot is accepted by the script host
class msgScriptBotHandshake
{
public:

	NET_MESSAGE_DECL(msgScriptBotHandshake, MSG_SCRIPT_BOT_HANDSHAKE);

	msgScriptBotHandshake(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId),
	m_ParticipantId(scriptHandlerNetComponent::INVALID_PARTICIPANT_ID),
	m_SlotNumber(scriptHandlerNetComponent::INVALID_SLOT)
	{
	}

	msgScriptBotHandshake(scriptIdBase& scriptId, ScriptParticipantId participantId, ScriptSlotNumber slotNumber) :
	m_ScriptId(&scriptId),
	m_ParticipantId(participantId),
	m_SlotNumber(slotNumber)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bool success = bb.SerUns(message.m_ParticipantId, sizeof(ScriptParticipantId)<<3) &&
			bb.SerUns(message.m_SlotNumber, sizeof(ScriptSlotNumber)<<3);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return success;
	}

	int GetMessageDataBitSize() const
	{
		return m_ScriptId->GetSizeOfMsgData() + (sizeof(ScriptParticipantId)<<3) + (sizeof(ScriptSlotNumber)<<3);
	}

	void Log(netLoggingInterface& log) const;

	const char* GetSendStr() const		{ return "SENDING_BOT_HANDSHAKE"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_BOT_HANDSHAKE"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	ScriptParticipantId		m_ParticipantId;
	ScriptSlotNumber		m_SlotNumber;
};

//PURPOSE
//  Sent in response to a bot handshake, when the original handshake could not be processed
class msgScriptBotHandshakeAck
{
public:

	NET_MESSAGE_DECL(msgScriptBotHandshakeAck, MSG_SCRIPT_BOT_HANDSHAKE_ACK);

	msgScriptBotHandshakeAck(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return m_ScriptId->GetSizeOfMsgData();
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const {}

	const char* GetSendStr() const		{ return "SENDING_BOT_HANDSHAKE_REJECTION"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_BOT_HANDSHAKE_REJECTION"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
};

//PURPOSE
// Sent when a bot participant is leaving the script session
class msgScriptBotLeave
{
public:

	NET_MESSAGE_DECL(msgScriptBotLeave, MSG_SCRIPT_BOT_LEAVE);

	msgScriptBotLeave(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return m_ScriptId->GetSizeOfMsgData();
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const {}

	const char* GetSendStr() const		{ return "SENDING_BOT_LEAVE"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_BOT_LEAVE"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
};

// Sent from the existing host to another machine informing it that it is the new host of the script. 
class msgScriptMigrateHost
{
public:

	NET_MESSAGE_DECL(msgScriptMigrateHost, MSG_SCRIPT_MIGRATE_HOST);

	msgScriptMigrateHost(scriptIdBase& scriptId, HostToken hostToken, u32 sizeOfNetArrayData):
	m_ScriptId(&scriptId),
	m_HostToken(hostToken),
	m_SizeOfNetArrayData(sizeOfNetArrayData)
	{
	}

	msgScriptMigrateHost(scriptIdBase& scriptId):
	m_ScriptId(&scriptId),
	m_HostToken(0),
	m_SizeOfNetArrayData(0)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bb.SerUns(message.m_HostToken, sizeof(HostToken)<<3);
		bb.SerUns(message.m_SizeOfNetArrayData, sizeof(message.m_SizeOfNetArrayData)<<3);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return  m_ScriptId->GetSizeOfMsgData() + (sizeof(HostToken)<<3) + (sizeof(m_SizeOfNetArrayData)<<3);
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const;

	const char* GetSendStr() const		{ return "SENDING_MIGRATE_HOST"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_MIGRATE_HOST"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	HostToken				m_HostToken;			// the current host token, incremented every time the host migrates
	u32						m_SizeOfNetArrayData;	// the total size of all the net array data that the sending host is currently arbitrating over
};

class msgScriptMigrateHostFailAck
{
public:

	NET_MESSAGE_DECL(msgScriptMigrateHostFailAck, MSG_SCRIPT_MIGRATE_HOST_FAIL_ACK);

	msgScriptMigrateHostFailAck(scriptIdBase& scriptId):
	m_ScriptId(&scriptId)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return  m_ScriptId->GetSizeOfMsgData();
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const {}

	const char* GetSendStr() const		{ return "SENDING_MIGRATE_HOST_FAIL_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_MIGRATE_HOST_FAIL_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
};

//PURPOSE
// Sent from a script client to the current host requesting to become the new host. 
class msgScriptHostRequest
{
public:

	NET_MESSAGE_DECL(msgScriptHostRequest, MSG_SCRIPT_HOST_REQUEST);

	msgScriptHostRequest(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return  m_ScriptId->GetSizeOfMsgData();
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const
	{
	}

	const char* GetSendStr() const		{ return "SENDING_HOST_REQUEST"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_HOST_REQUEST"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*	m_ScriptId;
};

// Sent from a new host informing all other machines that it is the new host of the script. 
class msgScriptNewHost
{
public:

	NET_MESSAGE_DECL(msgScriptNewHost, MSG_SCRIPT_NEW_HOST);

	msgScriptNewHost(scriptIdBase& scriptId, HostToken hostToken) :
	m_ScriptId(&scriptId),
	m_HostToken(hostToken)
	{
	}

	msgScriptNewHost(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId),
	m_HostToken(0)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bb.SerUns(message.m_HostToken, sizeof(HostToken)<<3);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return  m_ScriptId->GetSizeOfMsgData() + (sizeof(HostToken)<<3);
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const;

	const char* GetSendStr() const		{ return "SENDING_NEW_HOST"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_NEW_HOST"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	HostToken				m_HostToken;	// the current host token, incremented every time the host migrates
};

// Sent from a new potential host verifying that it can host the script. This happens when the previous host bombs out and a remaining machine
// assumes hosting.
class msgScriptVerifyHost
{
public:

	NET_MESSAGE_DECL(msgScriptVerifyHost, MSG_SCRIPT_VERIFY_HOST);

	msgScriptVerifyHost(scriptIdBase& scriptId, HostToken hostToken) :
	m_ScriptId(&scriptId),
	m_HostToken(hostToken)
	{
	}

	msgScriptVerifyHost(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId),
	m_HostToken(0)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bb.SerUns(message.m_HostToken, sizeof(HostToken)<<3);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		return  m_ScriptId->GetSizeOfMsgData() + (sizeof(HostToken)<<3);
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const;

	const char* GetSendStr() const		{ return "SENDING_VERIFY_HOST"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_VERIFY_HOST"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	HostToken				m_HostToken;	// the current host token, incremented every time the host migrates
};

class msgScriptVerifyHostAck
{
public:

	NET_MESSAGE_DECL(msgScriptVerifyHostAck, MSG_SCRIPT_VERIFY_HOST_ACK);

	msgScriptVerifyHostAck(scriptIdBase& scriptId, bool rejected, HostToken hostToken, bool validHost) :
	m_ScriptId(&scriptId),
	m_Rejected(rejected),
	m_HostToken(hostToken),
	m_ValidHost(validHost)
	{
	}

	msgScriptVerifyHostAck(scriptIdBase& scriptId) :
	m_ScriptId(&scriptId),
	m_Rejected(false),
	m_HostToken(0),
	m_ValidHost(false)
	{
	}

	NET_MESSAGE_SER(bb, message)
	{
#if __ASSERT
		int oldCursorPos = bb.GetCursorPos();
#endif
		message.m_ScriptId->Serialise(bb);

		bb.SerBool(message.m_Rejected);

		if (message.m_Rejected)
		{
			bb.SerBool(message.m_ValidHost);
		}

		bb.SerUns(message.m_HostToken, sizeof(HostToken)<<3);

#if __ASSERT
		int bitsWritten = bb.GetCursorPos() - oldCursorPos;
		Assert(bitsWritten == message.GetMessageDataBitSize());
#endif
		return true;
	}

	int GetMessageDataBitSize() const
	{
		int size = m_ScriptId->GetSizeOfMsgData() + 1 + (sizeof(HostToken)<<3);

		if (m_Rejected)
		{
			size ++;
		}

		return size;
	}

	void Log(netLoggingInterface& UNUSED_PARAM(log)) const;

	const char* GetSendStr() const		{ return "SENDING_VERIFY_HOST_ACK"; }
	const char* GetReceiveStr() const	{ return "RECEIVED_VERIFY_HOST_ACK"; }
	const char* GetScriptName() const	{ return m_ScriptId->GetLogName(); }

	scriptIdBase*			m_ScriptId;
	bool					m_Rejected;		// if true the sender rejects the new host because the host token is old
	HostToken				m_HostToken;	// sent if the sender rejects the new host
	bool					m_ValidHost;	// sent if the sender rejects the new host because the sender is a valid host
};

} // namespace rage

#endif  // NET_SCRIPT_HANDLER_MESSAGES_H
