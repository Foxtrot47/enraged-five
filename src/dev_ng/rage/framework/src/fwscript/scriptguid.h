//
// fwscript/scriptguid.h : an extension given to any entity that a script is referencing (including ambient entities)
//						  Used to assign a GUID to an entity (the id is the pool index of the extension).
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//
#ifndef FWSCRIPT_GUID_H_
#define FWSCRIPT_GUID_H_

#include "entity/extensiblebase.h"
#include "entity/extensionlist.h"
#include "script/scrchannel.h"
#include "fwtl/pool.h"

namespace rage {

class fwScriptGuid : public fwExtension
{
public:

	EXTENSIONID_DECL(fwScriptGuid, fwExtension);

	fwScriptGuid(fwExtensibleBase& base);
	~fwScriptGuid();

	FW_REGISTER_CLASS_POOL(fwScriptGuid); 

	fwExtensibleBase* GetBase() const { return m_pBase; }

	// PURPOSE: Create GUID for an fwExtendableBase object or return the one created for it already
	static int CreateGuid(fwExtensibleBase& base);
	// PURPOSE: Delete GUID from an fwExtendableBase object
	static void DeleteGuid(fwExtensibleBase& base);
	// PURPOSE: Return GUID from fwExtendableBase object
	static int GetGuidFromBase(fwExtensibleBase& base);
	// PURPOSE: Return fwExtendableBase object from a GUID
	static fwExtensibleBase* GetBaseFromGuid(int guid);
	// PURPOSE: Return object from a GUID with checks to ensure it is of the correct type
	template<class _T> static _T* FromGuid(int guid);
	// PURPOSE: Return object from a GUID with checks to ensure it is of the correct type
	template<class _T> static bool IsType(int guid);

#if __BANK	
	// Used for pool full callback
	fwExtensibleBase	*GetBase()	{ return m_pBase; }
#endif	// __BANK

private:
	fwExtensibleBase*	m_pBase;	
};

template<class _T> _T* fwScriptGuid::FromGuid(int guid)
{
	scrVerifyf(_T::template RTTIExistsCheck<_T>(), "Check the type has RTTI");
	fwExtensibleBase* pBase = fwScriptGuid::GetBaseFromGuid(guid);

	if (pBase)
	{
		// Need to ensure class _T is setup with the rtti macros in fwutil/rtti.h to get a correct result. Trying to work out how 
		// I can get the compiler to catch this
		if(scrVerifyf(pBase->GetIsClassId(_T::GetStaticClassId()), "Guid %d(%s) is not a %s", guid, pBase->GetClassId().GetCStr(), _T::GetStaticClassId().GetCStr()))
		{
			return (_T*)pBase;
		}
	}
	return NULL;
}

template<class _T> bool fwScriptGuid::IsType(int guid)
{
	scrVerifyf(_T::template RTTIExistsCheck<_T>(), "Check the type has RTTI");
	fwExtensibleBase* pBase = fwScriptGuid::GetBaseFromGuid(guid);
	if (pBase)
	{
		return (pBase->GetIsClassId(_T::GetStaticClassId()));
	}
	return false;
}


#if __BANK
template<> void fwPool<fwScriptGuid>::PoolFullCallback();
#endif

} // namespace rage

#endif //FWSCRIPT_GUID_H_