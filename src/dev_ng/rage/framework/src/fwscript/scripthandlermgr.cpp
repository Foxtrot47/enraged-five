//
// scriptHandlerMgr.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwscript/scriptHandlerMgr.h"

// framework includes
#include "fwnet/netarraymgr.h"
#include "fwnet/netchannel.h"
#include "fwnet/netinterface.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobject.h"
#include "fwnet/netplayer.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"

#include "fwscript/scriptobjinfo.h"

// rage includes
#include "net/event.h"

PARAM(scriptBandwidthLogging, "Log individual script bandwidth");

namespace rage
{

NETWORK_OPTIMISATIONS()

// ================================================================================================================
// scriptHandlerMgr - Manages the array of current active scripts on a player's machine
// ================================================================================================================

scriptHandlerMgr::scriptHandlerMgr(netBandwidthMgr* bandwidthMgr)  
: m_BandwidthMgr(bandwidthMgr)
, m_IsInitialised(false)
, m_IsNetworkInitialised(false)
, m_DeletingHandlers(false)
, m_Log(0) 
, m_BandwidthLog(0)
, m_HostMigrationTimer(0)
#if __BANK
, m_NumScriptManagerMessagesSent(0)
#endif // __BANK
{
	// Initialize our message handler.  This will be registered with a
	// netConnectionmanager in Init().
	m_Dlgt.Bind(this, &scriptHandlerMgr::OnMessageReceived);

#if __DEV
	m_JoinMsgRecorderId			= INVALID_RECORDER_ID;
	m_HandshakeMsgRecorderId	= INVALID_RECORDER_ID;
	m_LeaveMsgRecorderId		= INVALID_RECORDER_ID;
	m_AckMsgRecorderId			= INVALID_RECORDER_ID;
	m_MigrateHostMsgRecorderId  = INVALID_RECORDER_ID;
	m_numMessageBandwidthRecorders = 0;
	m_numScriptsLogged			= 0;
#endif
}

void scriptHandlerMgr::Init()
{
	if (AssertVerify(!m_IsInitialised))
	{
		gnetAssert(m_scriptHandlers.GetNumUsed() == 0);

		scriptHandler::atDScriptObjectNode::InitPool( MEMBUCKET_SYSTEM );

        BANK_ONLY(m_NumScriptManagerMessagesSent = 0);

		m_IsInitialised = true;
	}
}

void scriptHandlerMgr::NetworkInit()
{
	if (gnetVerify(!m_IsNetworkInitialised))
	{
		// Register our message handler.
		netInterface::AddDelegate(&m_Dlgt);

		static bool s_HasCreatedLog = false; 
		m_Log = rage_new netLog("ScriptManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                     netInterface::GetDefaultLogFileBlockingMode());
		
		if (PARAM_scriptBandwidthLogging.Get())
		{
			m_BandwidthLog = rage_new netLog("ScriptBandwidth.csv", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
																	netInterface::GetDefaultLogFileBlockingMode());
		}

		s_HasCreatedLog = true; 
		
		scriptHandlerNetComponent::participantData::InitPool( MEMBUCKET_SYSTEM );

#if __BANK
		if (m_BandwidthMgr)
		{
			// Register bandwidth stats
			m_BandwidthMgr->RegisterStatistics(m_BandwidthStats, "Script Manager");

			// allocate bandwidth recorders for the different script manager messages
			m_JoinMsgRecorderId			= m_BandwidthStats.AllocateBandwidthRecorder("Script join msgs");
			m_HandshakeMsgRecorderId	= m_BandwidthStats.AllocateBandwidthRecorder("Script handshake msgs");
			m_LeaveMsgRecorderId		= m_BandwidthStats.AllocateBandwidthRecorder("Script leave msgs");
			m_AckMsgRecorderId			= m_BandwidthStats.AllocateBandwidthRecorder("Script ack msgs");
			m_MigrateHostMsgRecorderId	= m_BandwidthStats.AllocateBandwidthRecorder("Script migrate host msgs");

			m_numMessageBandwidthRecorders	= m_BandwidthStats.GetNumBandwidthRecordersAllocated();
			m_numScriptsLogged				= 0;
		}
#endif	

		scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

		static const int MAX_HANDLERS_TO_DELETE = 50;
		atHashValue handlersToDelete[MAX_HANDLERS_TO_DELETE];
		u32 numHandlersToDelete = 0;

		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			scriptHandler* handler = entry.GetData(); 

			if (handler && handler->RequiresANetworkComponent())
			{
				if (!handler->GetThread() && gnetVerify(numHandlersToDelete < MAX_HANDLERS_TO_DELETE))
				{
					gnetAssertf(0, "The script handler for %s still exists with no thread when entering MP. Deleting", handler->GetLogName());

					handlersToDelete[numHandlersToDelete++] = entry.GetKey();
				}
			}
		}

		for (u32 i=0; i<numHandlersToDelete; i++)
		{
			DeleteHandlerWithHash(handlersToDelete[i]);
		}

		m_IsNetworkInitialised = true;
	}
}

void scriptHandlerMgr::Update()
{
	static const int MAX_HANDLERS_TO_DELETE = 50;

	atHashValue handlersToDelete[MAX_HANDLERS_TO_DELETE];
	u32 numHandlersToDelete = 0;

	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (Verifyf(handler, "scriptHandlerMgr - A handler in the handler map has been illegally deleted, hash is %u", entry.GetKey().GetHash()))
		{
			Assert(handler->GetScriptId().GetScriptIdHash() == entry.GetKey());

			if (handler->Update() && numHandlersToDelete < MAX_HANDLERS_TO_DELETE)
			{
				handlersToDelete[numHandlersToDelete++] = entry.GetKey();
			}
		}
	}

	for (u32 i=0; i<numHandlersToDelete; i++)
	{
		DeleteHandlerWithHash(handlersToDelete[i]);
	}
}

void scriptHandlerMgr::NetworkUpdate()
{
	static const int MAX_HANDLERS_TO_DELETE = 50;

	atHashValue handlersToDelete[MAX_HANDLERS_TO_DELETE];
	u32 numHandlersToDelete = 0;

	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler && handler->NetworkUpdate() && numHandlersToDelete < MAX_HANDLERS_TO_DELETE)
		{
			handlersToDelete[numHandlersToDelete++] = entry.GetKey();
		}
	}

	for (u32 i=0; i<numHandlersToDelete; i++)
	{
		DeleteHandlerWithHash(handlersToDelete[i]);
	}

	ManageHostMigration();
}

void scriptHandlerMgr::NetworkShutdown()
{
	if(m_IsNetworkInitialised)
	{
		static const int MAX_HANDLERS_TO_DELETE = 50;

		atHashValue handlersToDelete[MAX_HANDLERS_TO_DELETE];
		u32 numHandlersToDelete = 0;

		scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			scriptHandler* handler = entry.GetData(); 

			if (handler)
			{
				if (handler->GetNetworkComponent())
				{
					handler->DestroyNetworkComponent();
				}

				// delete the handler immediately if there is no thread, this is so that the handlers are cleaned up immediately when doing a 
				// session transition
				if (handler->Update())
				{
					if (AssertVerify(numHandlersToDelete < MAX_HANDLERS_TO_DELETE))
					{
						handlersToDelete[numHandlersToDelete++] = entry.GetKey();
					}
				} 
			}
		}

		for (u32 i=0; i<numHandlersToDelete; i++)
		{
			DeleteHandlerWithHash(handlersToDelete[i]);
		}

		netInterface::RemoveDelegate(&m_Dlgt);

		scriptHandlerNetComponent::participantData::ShutdownPool();

        delete m_Log;
        m_Log = 0;

		if (m_BandwidthLog)
		{
			delete m_BandwidthLog;
			m_BandwidthLog = 0;
		}

		m_IsNetworkInitialised = false;
	}
}

void scriptHandlerMgr::Shutdown()
{
	if(m_IsInitialised)
	{
		if (m_IsNetworkInitialised)
		{
			NetworkShutdown();
		}

		m_DeletingHandlers = true;

		scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

		for (entry.Start(); !entry.AtEnd(); entry.Next())
		{
			scriptHandler* handler = entry.GetData(); 

			if (handler)
			{
				handler->Shutdown();
				delete handler;
			}
		}

		m_scriptHandlers.Reset();

		m_DeletingHandlers = false;

		scriptHandler::atDScriptObjectNode::ShutdownPool();

		m_IsInitialised = false;
	}
}

void scriptHandlerMgr::RegisterScript(scrThread& scriptThread)
{
	scriptIdBase& scriptId = GetScriptId(scriptThread);

	scriptHandler* pExistingHandler = GetScriptHandler(scriptId);

	// we can't have 2 network script handlers with the same id
	if (gnetVerifyf(!pExistingHandler, "Another thread with the same script id %s is already registered", scriptId.GetLogName()) &&
		gnetVerifyf(!GetScriptHandler(scriptId), "Script thread %s is already registered", scriptId.GetLogName()))
	{
		scriptHandler* newHandler = CreateScriptHandler(scriptThread);

		if (gnetVerify(newHandler))
		{
#if !__NO_OUTPUT
			scriptHandler** ppHandler = m_scriptHandlers.Access(scriptId.GetScriptIdHash());
			if (ppHandler)
			{
				char logNameOfNewHandler[200];
				safecpy(logNameOfNewHandler, newHandler->GetLogName(), NELEM(logNameOfNewHandler));	//	CGameScriptId::GetLogName() fills in a static string internally so I need to make a copy of one of the LogName's to display in the assert below

				scriptHandler *pScriptHandler = static_cast<scriptHandler*>(*ppHandler);

				Quitf("scriptHandlerMgr::RegisterScript - about to add %s(%p) script with hash %u. %s(%p) already exists in the map with this hash",
					logNameOfNewHandler, newHandler, scriptId.GetScriptIdHash().GetHash(),
					pScriptHandler->GetLogName(), pScriptHandler);
			}
#endif	//	!__NO_OUTPUT

			m_scriptHandlers.Insert(scriptId.GetScriptIdHash(), newHandler);
		}
	}
}

void scriptHandlerMgr::UnregisterScript(scrThread& scriptThread)
{
	scriptIdBase& scriptId = GetScriptId(scriptThread);

	scriptHandler* handler = GetScriptHandler(scriptId);

	if (AssertVerify(handler) && handler->Terminate())
	{
		DeleteHandlerWithHash(handler->GetScriptId().GetScriptIdHash());
	}
}

void scriptHandlerMgr::OnMessageReceived(const ReceivedMessageData &messageData)
{
    unsigned msgId = 0;

	if (gnetVerify(messageData.IsValid()))
	{
		if(netMessage::GetId(&msgId, messageData.m_MessageData, messageData.m_MessageDataSize))
		{
			scriptHandler *pHandler = 0;
			bool bHandled = true;

			if (msgId == msgScriptJoin::MSG_ID())
			{
				pHandler = HandleScriptJoinMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_JoinMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptJoinAck::MSG_ID())
			{
				pHandler = HandleScriptJoinAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptJoinHostAck::MSG_ID())
			{
				pHandler = HandleScriptJoinHostAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptHandshake::MSG_ID())
			{
				pHandler = HandleScriptHandshakeMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_HandshakeMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptHandshakeAck::MSG_ID())
			{
				pHandler = HandleScriptHandshakeAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptLeave::MSG_ID())
			{
				pHandler = HandleScriptLeaveMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_LeaveMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptLeaveAck::MSG_ID())
			{
				pHandler = HandleScriptLeaveAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptBotJoin::MSG_ID())
			{
				pHandler = HandleScriptBotJoinMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_JoinMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptBotJoinAck::MSG_ID())
			{
				pHandler = HandleScriptBotJoinAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptBotHandshake::MSG_ID())
			{
				pHandler = HandleScriptBotHandshakeMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_HandshakeMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptBotHandshakeAck::MSG_ID())
			{
				pHandler = HandleScriptBotHandshakeAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptBotLeave::MSG_ID())
			{
				pHandler = HandleScriptBotLeaveMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_LeaveMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptMigrateHost::MSG_ID())
			{
				pHandler = HandleScriptMigrateHostMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_MigrateHostMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptMigrateHostFailAck::MSG_ID())
			{
				pHandler = HandleScriptMigrateHostFailAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_MigrateHostMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptHostRequest::MSG_ID())
			{
				pHandler = HandleScriptHostRequestMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_MigrateHostMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptNewHost::MSG_ID())
			{
				pHandler = HandleScriptNewHostMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_MigrateHostMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptVerifyHost::MSG_ID())
			{
				pHandler = HandleScriptVerifyHostMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_MigrateHostMsgRecorderId, messageData.m_MessageDataSize));
			}
			else if (msgId == msgScriptVerifyHostAck::MSG_ID())
			{
				pHandler = HandleScriptVerifyHostAckMsg(messageData);

				BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecorderId, messageData.m_MessageDataSize));
			}
			else
			{
				bHandled = false;
			}

			if (bHandled && GetLog())
			{
				if(!pHandler)
				{
					GetLog()->WriteDataValue("IGNORE", "No handler for this script");
				}

				GetLog()->LineBreak();
			}
		}
	}
}

scriptHandler *scriptHandlerMgr::HandleScriptJoinMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
    if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptJoin msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

            pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && pHandler->GetNetworkComponent())
		    {
			    pHandler->GetNetworkComponent()->HandleJoinMsg(msg, messageData);
		    }
		    else
		    {
			    // send an ack back
			    msgScriptJoinAck ack(*msg.m_ScriptId, SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT);
			    SendMessageToPlayer(*messageData.m_FromPlayer, ack);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptJoinAckMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptJoinAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (AssertVerify(pHandler) && AssertVerify(pHandler->GetNetworkComponent())) // there must be a handler as the handler cannot shutdown until all acks have been received
		    {
			    pHandler->GetNetworkComponent()->HandleJoinAckMsg(msg, messageData);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptJoinHostAckMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptJoinHostAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler) // there may not be a handler if the handshake was sent to a joining player and then the script immediately terminated
		    {
			    pHandler->GetNetworkComponent()->HandleJoinHostAckMsg(msg, messageData);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptHandshakeMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptHandshake msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && pHandler->GetNetworkComponent())
		    {
			    pHandler->GetNetworkComponent()->HandleHandshakeMsg(msg, messageData);
		    }
		    else
		    {
			    // send an ack back
			    msgScriptHandshakeAck ack(*msg.m_ScriptId, false);
			    SendMessageToPlayer(*messageData.m_FromPlayer, ack);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptHandshakeAckMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptHandshakeAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && AssertVerify(pHandler->GetNetworkComponent())) 
		    {
			    pHandler->GetNetworkComponent()->HandleHandshakeAckMsg(msg, messageData);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptLeaveMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptLeave msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && pHandler->GetNetworkComponent())
		    {
			    pHandler->GetNetworkComponent()->HandleLeaveMsg(msg, messageData);
		    }
		    else
		    {
			    // acknowledge this message
			    msgScriptLeaveAck ack(*msg.m_ScriptId);
			    SendMessageToPlayer(*messageData.m_FromPlayer, ack);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptLeaveAckMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptLeaveAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && pHandler->GetNetworkComponent())
		    {
			    pHandler->GetNetworkComponent()->HandleLeaveAckMsg(msg, messageData);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptBotJoinMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should only send these messages to non-bots
	if(gnetVerify(!toPlayer.IsBot() && fromPlayer.IsBot()))
	{
		msgScriptBotJoin msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
			    NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
			    msg.Log(*GetLog());
            }

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler && pHandler->GetNetworkComponent())
			{
				pHandler->GetNetworkComponent()->HandleBotJoinMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptBotJoinAckMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should only receive these messages from non-bots
	if(gnetVerify(toPlayer.IsBot() && !fromPlayer.IsBot()))
	{
		msgScriptBotJoinAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
			    NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
			    msg.Log(*GetLog());
            }

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler) 
			{
				pHandler->GetNetworkComponent()->HandleBotJoinAckMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptBotHandshakeMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should only send these messages to non-bots
	if(gnetVerify(!toPlayer.IsBot() && fromPlayer.IsBot()))
	{
		msgScriptBotHandshake msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
			    NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
			    msg.Log(*GetLog());
            }

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler && pHandler->GetNetworkComponent())
			{
				pHandler->GetNetworkComponent()->HandleBotHandshakeMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptBotHandshakeAckMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should only receive these messages from non-bots
	if(gnetVerify(toPlayer.IsBot() && !fromPlayer.IsBot()))
	{
		msgScriptBotHandshakeAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
			    NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
			    msg.Log(*GetLog());
            }

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler) 
			{
				pHandler->GetNetworkComponent()->HandleBotHandshakeAckMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptBotLeaveMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should only send these messages to non-bots
	if(gnetVerify(!toPlayer.IsBot() && fromPlayer.IsBot()))
	{
		msgScriptBotLeave msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
        {
            if(GetLog())
            {
			    NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
			    msg.Log(*GetLog());
            }

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler) 
			{
				pHandler->GetNetworkComponent()->HandleBotLeaveMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptMigrateHostMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptMigrateHost msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && pHandler->GetNetworkComponent())
		    {
			    pHandler->GetNetworkComponent()->HandleMigrateHostMsg(msg, messageData);
		    }
			else
			{
				// acknowledge this message
				msgScriptMigrateHostFailAck ack(*msg.m_ScriptId);
				SendMessageToPlayer(*messageData.m_FromPlayer, ack);
			}
		}
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptMigrateHostFailAckMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
	{
		msgScriptMigrateHostFailAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
			if(GetLog())
			{
				NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
				msg.Log(*GetLog());
			}

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler && pHandler->GetNetworkComponent())
			{
				pHandler->GetNetworkComponent()->HandleMigrateHostFailAckMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptHostRequestMsg(const ReceivedMessageData &messageData)
{
    scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
    {
        msgScriptHostRequest msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
            if(GetLog())
            {
		        NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
		        msg.Log(*GetLog());
            }

		    pHandler = GetScriptHandler(*msg.m_ScriptId);

		    if (pHandler && pHandler->GetNetworkComponent())
		    {
			    pHandler->GetNetworkComponent()->HandleHostRequestMsg(msg, messageData);
		    }
        }
    }

    return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptNewHostMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
	{
		msgScriptNewHost msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
			if(GetLog())
			{
				NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
				msg.Log(*GetLog());
			}

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler && pHandler->GetNetworkComponent())
			{
				pHandler->GetNetworkComponent()->HandleNewHostMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptVerifyHostMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
	{
		msgScriptVerifyHost msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
			if(GetLog())
			{
				NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
				msg.Log(*GetLog());
			}

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler && pHandler->GetNetworkComponent())
			{
				pHandler->GetNetworkComponent()->HandleVerifyHostMsg(msg, messageData);
			}
			else
			{
				msgScriptVerifyHostAck newHostAck(*msg.m_ScriptId);
				SendMessageToPlayer(fromPlayer, newHostAck);	
			}
		}
	}

	return pHandler;
}

scriptHandler *scriptHandlerMgr::HandleScriptVerifyHostAckMsg(const ReceivedMessageData &messageData)
{
	scriptHandler *pHandler = 0;

	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// bots should not sent or receive these messages
	if(gnetVerify(!toPlayer.IsBot() && !fromPlayer.IsBot()))
	{
		msgScriptVerifyHostAck msg(GetMsgScriptId());

		if(AssertVerify(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
		{
			if(GetLog())
			{
				NetworkLogUtils::WriteMessageHeader(*GetLog(), true, messageData.m_NetSequence, *messageData.m_FromPlayer, msg.GetReceiveStr(), msg.GetScriptName());
				msg.Log(*GetLog());
			}

			pHandler = GetScriptHandler(*msg.m_ScriptId);

			if (pHandler && pHandler->GetNetworkComponent())
			{
				pHandler->GetNetworkComponent()->HandleVerifyHostAckMsg(msg, messageData);
			}
		}
	}

	return pHandler;
}

void scriptHandlerMgr::WriteScriptHeader(netLoggingInterface *LOGGING_ONLY(log), const scriptIdBase& LOGGING_ONLY(scriptId), const char *LOGGING_ONLY(headerText), ...)
{
#if ENABLE_NETWORK_LOGGING
    if(log)
    {
        char buffer[netLog::MAX_LOG_STRING];
	    va_list args;
	    va_start(args, headerText);
	    vsprintf(buffer, headerText, args);
	    va_end(args);

        log->Log("\t== %s: %s\r\n", scriptId.GetLogName(), buffer);
    }
#endif // #if ENABLE_NETWORK_LOGGING
}

void scriptHandlerMgr::PlayerHasJoined(const netPlayer& player)
{
    if(GetLog())
    {
	    NetworkLogUtils::WriteLogEvent(*GetLog(), "PLAYER_HAS_JOINED", "%s", player.GetLogName());
    }

	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler && handler->GetNetworkComponent())
		{
			handler->GetNetworkComponent()->PlayerHasJoined(player);
		}
	}
}

void scriptHandlerMgr::PlayerHasLeft(const netPlayer& player)
{
    if(GetLog())
    {
	    NetworkLogUtils::WriteLogEvent(*GetLog(), "PLAYER_HAS_LEFT", "%s", player.GetLogName());
    }

	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler && handler->GetNetworkComponent())
		{
			handler->GetNetworkComponent()->PlayerHasLeft(player);
		}
	}
}

void scriptHandlerMgr::RegisterExistingThreads()
{
	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler->GetThread() && handler->RequiresANetworkComponent() && !handler->GetNetworkComponent())
		{
#if __ASSERT
			NETWORK_QUITF(0, "Warning: MP script %s launched before MP has started", handler->GetLogName());
#endif
			handler->CreateNetworkComponent();
		}
	}
}

void scriptHandlerMgr::RemoveScriptResource(ScriptResourceId resourceId)
{
	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler)
		{
			handler->RemoveScriptResource(resourceId, false, true);
		}
	}
}

void scriptHandlerMgr::RemoveScriptResource(ScriptResourceType resourceType, ScriptResourceRef resourceRef)
{
	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler)
		{
			handler->RemoveScriptResource(resourceType, resourceRef, false, true);
		}
	}
}

void scriptHandlerMgr::RemoveAllScriptResourcesOfType(ScriptResourceType type)
{
	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler)
		{
			handler->RemoveAllScriptResourcesOfType(type);
		}
	}
}

scriptResource* scriptHandlerMgr::GetScriptResource(ScriptResourceType resourceType, ScriptResourceRef resourceRef, scriptHandler* handlerToIgnore)
{
	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler != handlerToIgnore)
		{
			scriptResource* resource = handler->GetScriptResource(resourceType, resourceRef);

			if (resource)
			{
				return resource;
			}
		}
	}

	return 0;
}

scriptHandler* scriptHandlerMgr::GetScriptHandlerForResource(ScriptResourceType resourceType, ScriptResourceRef resourceRef, scriptHandler* handlerToIgnore)
{
	scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = entry.GetData(); 

		if (handler != handlerToIgnore)
		{
			scriptResource* resource = handler->GetScriptResource(resourceType, resourceRef);

			if (resource)
			{
				return handler;
			}
		}
	}

	return 0;
}

void scriptHandlerMgr::DeleteHandlerWithHash(atHashValue hash)
{
	m_DeletingHandlers = true;

	scriptHandler* handler = *m_scriptHandlers.Access(hash);

	if (Verifyf(handler, "scriptHandlerMgr::DeleteHandlerWithHash - failed to find handler with hash %u", hash.GetHash()))
	{
		if (!m_scriptHandlers.Delete(hash))
		{
			FatalAssertf(0, "scriptHandlerMgr::DeleteHandlerWithHash - failed to delete handler %s at key %u (%p)\n", handler->GetLogName(), hash.GetHash(), handler);
		}

		handler->Shutdown();
		delete handler;
	}

	m_DeletingHandlers = false;
}

#if __BANK
RecorderId scriptHandlerMgr::AllocateBandwidthRecorderForScript(const char* scriptName) 
{
	RecorderId id = m_BandwidthStats.GetBandwidthRecorder(scriptName);

	if (id == INVALID_RECORDER_ID)
	{
		id = m_BandwidthStats.AllocateBandwidthRecorder(scriptName); 
	}

	return id;
}

void scriptHandlerMgr::AddBandwidthOutForScript(RecorderId scriptRecorderId, unsigned bandwidthOut)
{
	// this bandwidth has already been counted by the array handler, so don't add it to the totals
	m_BandwidthStats.AddBandwidthOut(scriptRecorderId, bandwidthOut, false);
}

void scriptHandlerMgr::AddBandwidthInForScript(RecorderId scriptRecorderId, unsigned bandwidthIn)
{
	// this bandwidth has already been counted by the array handler, so don't add it to the totals
	m_BandwidthStats.AddBandwidthIn(scriptRecorderId, bandwidthIn, false);
}

void scriptHandlerMgr::UpdateBandwidthLog()
{
	if (!m_BandwidthLog)
	{
		return;
	}

	u32 numScriptRecordersAllocated = m_BandwidthStats.GetNumBandwidthRecordersAllocated() - m_numMessageBandwidthRecorders;

	if (numScriptRecordersAllocated == 0)
	{
		return;
	}

	if(netBandwidthStatistics::GetLastFrameSorted() != fwTimer::GetFrameCount())
	{
		return;
	}

	float totalIn = 0.0f;
	float totalOut = 0.0f;

	for (u32 i=m_numMessageBandwidthRecorders; i<m_BandwidthStats.GetNumBandwidthRecordersAllocated(); i++)
	{
		const netBandwidthRecorderWrapper* pBandwidthRecorder = m_BandwidthStats.GetBandwidthRecorder((RecorderId)i);

		if (AssertVerify(pBandwidthRecorder))
		{
			totalIn += pBandwidthRecorder->GetBandwidthInRate();
			totalOut += pBandwidthRecorder->GetBandwidthOutRate();
		}
	}

	if (totalIn == 0.0f && totalOut == 0.0f)
	{
		return;
	}

	static const unsigned sizeOfLogStr = netLogImpl::MAX_LOG_STRING;

	char logStr[sizeOfLogStr];

	logStr[0] = '\0';

	if (numScriptRecordersAllocated > m_numScriptsLogged)
	{
		m_numScriptsLogged = numScriptRecordersAllocated;

		formatf(logStr, sizeOfLogStr, ", Total in, Total out");

		for (u32 i=m_numMessageBandwidthRecorders; i<m_BandwidthStats.GetNumBandwidthRecordersAllocated(); i++)
		{
			const netBandwidthRecorderWrapper* pBandwidthRecorder = m_BandwidthStats.GetBandwidthRecorder((RecorderId)i);

			if (AssertVerify(pBandwidthRecorder))
			{
				snprintf(logStr, sizeOfLogStr, "%s, %s in, %s out", logStr, pBandwidthRecorder->GetName(), pBandwidthRecorder->GetName());
			}
		}

		snprintf(logStr, sizeOfLogStr, "%s\n", logStr);

		m_BandwidthLog->Log(logStr);
	}


	logStr[0] = '\0';

	formatf(logStr, sizeOfLogStr, ", %d, %d", (int)totalIn, (int)totalOut);

	for (u32 i=m_numMessageBandwidthRecorders; i<m_BandwidthStats.GetNumBandwidthRecordersAllocated(); i++)
	{
		const netBandwidthRecorderWrapper* pBandwidthRecorder = m_BandwidthStats.GetBandwidthRecorder((RecorderId)i);

		if (AssertVerify(pBandwidthRecorder))
		{
			snprintf(logStr, sizeOfLogStr, "%s, %d, %d", logStr, (int)pBandwidthRecorder->GetBandwidthInRate(), (int)pBandwidthRecorder->GetBandwidthOutRate());
		}
	}

	snprintf(logStr, sizeOfLogStr, "%s\n", logStr);

	m_BandwidthLog->Log(logStr);
}

#endif // __BANK

#if	__BANK
void scriptHandlerMgr::SpewObjectAndResourceInfo() const
{
	Displayf("Start of scriptHandlerMgr::SpewObjectAndResourceInfo");

	scriptHandlerMap::ConstIterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = (entry.GetData()); 

		if (handler)
		{
			handler->SpewObjectAndResourceInfo();
		}
	}

	Displayf("End of scriptHandlerMgr::SpewObjectAndResourceInfo");
}

void scriptHandlerMgr::SpewResourceInfoForType(ScriptResourceType resourceTypeFilter) const
{
	Displayf("Start of scriptHandlerMgr::SpewResourceInfoForType");

	scriptHandlerMap::ConstIterator entry = m_scriptHandlers.CreateIterator();

	for (entry.Start(); !entry.AtEnd(); entry.Next())
	{
		scriptHandler* handler = (entry.GetData()); 

		handler->SpewAllResourcesOfType(resourceTypeFilter);
	}

	Displayf("End of scriptHandlerMgr::SpewResourceInfoForType");
}
#endif	//	__BANK

int SortPlayerByNetArrayDataSize(const void *paramA, const void *paramB)
{
	const netPlayer* playerA = *((netPlayer **)(paramA));
	const netPlayer* playerB = *((netPlayer **)(paramB));

	if (playerA->GetSizeOfNetArrayData() < playerB->GetSizeOfNetArrayData())
	{
		return -1;
	}
	else if(playerA->GetSizeOfNetArrayData() > playerB->GetSizeOfNetArrayData())
	{
		return 1;
	}

	return 0;
}

int SortHandlerByHostBroadcastDataSize(const void *paramA, const void *paramB)
{
	const scriptHandler* handlerA = *((scriptHandler **)(paramA));
	const scriptHandler* handlerB = *((scriptHandler **)(paramB));

	if (handlerA->GetNetworkComponent()->GetSizeOfHostBroadcastData() > handlerB->GetNetworkComponent()->GetSizeOfHostBroadcastData())
	{
		return -1;
	}
	else if(handlerA->GetNetworkComponent()->GetSizeOfHostBroadcastData() < handlerB->GetNetworkComponent()->GetSizeOfHostBroadcastData())
	{
		return 1;
	}

	return 0;
}

void scriptHandlerMgr::ManageHostMigration()
{
	static const int TIME_BETWEEN_HOST_MIGRATION_ATTEMPTS = 2000;  // the length of time between host migration attempts
	static float NET_ARRAY_DATA_RATIO = 0.9f;

	const netPlayer* pLocalPlayer = netInterface::GetLocalPlayer();

	if (pLocalPlayer && netInterface::GetNumPhysicalPlayers() > 1)
	{
		m_HostMigrationTimer += fwTimer::GetTimeStepInMilliseconds();

		if (m_HostMigrationTimer > TIME_BETWEEN_HOST_MIGRATION_ATTEMPTS)
		{
			m_HostMigrationTimer = 0;

			const netPlayer* sortedPlayers[MAX_NUM_PHYSICAL_PLAYERS];
			u32 numSortedPlayers = 0;
			const scriptHandler* sortedHandlers[MAX_NUM_ACTIVE_NETWORK_SCRIPTS];
			u32 numSortedHandlers = 0;

			unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
			const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

			u32 localPlayerData = pLocalPlayer->GetSizeOfNetArrayData();

			Assertf(netInterface::GetArrayManager().GetSizeOfLocallyArbitratedArrayData() == localPlayerData, "Cached size of local player data (%u) differs from real size of arbitrated data (%u)", localPlayerData, netInterface::GetArrayManager().GetSizeOfLocallyArbitratedArrayData());

			for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
			{
				const netPlayer *remotePlayer = remotePhysicalPlayers[index];

				if (remotePlayer && !remotePlayer->IsBot() && remotePlayer->GetSizeOfNetArrayData() <  localPlayerData * NET_ARRAY_DATA_RATIO)
				{
					sortedPlayers[numSortedPlayers++] = remotePlayer;
				}
			}

			if (numSortedPlayers > 1)
			{
				qsort(&sortedPlayers[0], numSortedPlayers, sizeof(netPlayer*), &SortPlayerByNetArrayDataSize);
			}

			scriptHandlerMap::Iterator entry = m_scriptHandlers.CreateIterator();

			u32 totalNumScriptHandlers = 0;

			for (entry.Start(); !entry.AtEnd(); entry.Next())
			{
				scriptHandler* handler = (entry.GetData()); 

				if (handler && handler->GetNetworkComponent())
				{
					totalNumScriptHandlers++;

					if (handler->GetNetworkComponent()->CanMigrateScriptHost())
					{
						sortedHandlers[numSortedHandlers++] = handler;
					}
				}
			}

			if (numSortedHandlers > 1)
			{
				qsort(&sortedHandlers[0], numSortedHandlers, sizeof(scriptHandler*), &SortHandlerByHostBroadcastDataSize);
			}

			if (totalNumScriptHandlers > 1)
			{
				for (unsigned p=0; p<numSortedPlayers; p++)
				{
					const netPlayer *pPlayer = sortedPlayers[p];

					for (unsigned h=0; h<numSortedHandlers; h++)
					{
						const scriptHandler *pHandler = sortedHandlers[h];

						if (pHandler && pHandler->GetNetworkComponent()->IsPlayerAParticipant(pPlayer->GetPhysicalPlayerIndex()))
						{
							if (pPlayer->GetSizeOfNetArrayData() + pHandler->GetNetworkComponent()->GetSizeOfHostBroadcastData() < localPlayerData * NET_ARRAY_DATA_RATIO)
							{
								// we must wait until all machines are up to date with the host broadcast data and for all the host objects to be cloned on all 
								// participant machines before trying to migrate the host
								if (pHandler->AreAllParticipantsUpToDateWithHostState())
								{
									pHandler->GetNetworkComponent()->MigrateScriptHost(*pPlayer);
								}

								return;
							}
						}
					}
				}
			}
		}
	}
}

} // namespace rage
