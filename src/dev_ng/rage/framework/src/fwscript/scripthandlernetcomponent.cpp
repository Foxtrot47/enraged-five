//
// scriptHandlerNetComponent.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwscript/scriptHandlerNetComponent.h"

#include "fwsys/timer.h"

#include "fwmaths/random.h"
#include "fwnet/netArrayHandlerTypes.h"
#include "fwnet/netArrayMgr.h"
#include "fwnet/netLogUtils.h"
#include "fwnet/netObjectMgrBase.h"
#include "fwnet/optimisations.h"
#include "fwscript/scripthandlermessages.h"
#include "fwscript/scripthandlermgr.h"
#include "fwscript/scriptinterface.h"
#include "fwscript/scriptobjinfo.h"

#if __DEV
#include "fwmaths/random.h"
#endif

namespace rage
{
NETWORK_OPTIMISATIONS()

FW_INSTANTIATE_CLASS_POOL(scriptHandlerNetComponent::participantData, MAX_NUM_PHYSICAL_PLAYERS*scriptHandlerNetComponent::MAX_NUM_NET_COMPONENTS, atHashString("participantData",0x355a8431));

bool scriptHandlerNetComponent::ms_useNoHostFix = true;

scriptHandlerNetComponent::scriptHandlerNetComponent(scriptHandler& parentHandler)
: m_ParentHandler(parentHandler)
, m_State(NETSCRIPT_INTERNAL_START)	
, m_WaitingForAcks(0)
, m_HostData(0)
, m_MaxNumParticipants(0)
, m_NumParticipants(0)
, m_NumTeams(0)
, m_NumCandidates(0)
, m_TeamReservations(MAX_NUM_TEAMS)
, m_MySlotNumber(INVALID_SLOT)
, m_activeParticipants(0)
, m_playerParticipants(0)
, m_ParticipantsArray(MAX_NUM_PHYSICAL_PLAYERS)
, m_HostMigrationTimer(0)
, m_RejectionReason(REJECTED_INVALID)
, m_NumHostBroadcastData(0)
, m_NumPlayerBroadcastData(0)
, m_Terminated(false)
, m_AcceptingPlayers(true)
, m_Accepted(false)
, m_PlayerJoined(false)
, m_ScriptFinished(false)
, m_HostRequested(false)
, m_PendingHostingFailed(false)
, m_PreventHostMigration(false)
, m_LastHost(NULL)
, m_HostToken(INVALID_HOST_TOKEN)
, m_PendingHost(NULL)
, m_PreferredInitialHost(NULL)
, m_sizeOfHostBroadcastData(0)
, m_VerifyHostBroadcastData(false)
, m_PendingHostContext(PENDING_HOST_NONE)
#if __BANK
, m_bandwidthRecorderId(INVALID_RECORDER_ID)
, m_timeHandlerActive(0)
, m_totalBandwidthIn(0)
, m_totalBandwidthOut(0)
, m_peakBandwidthIn(0)
, m_peakBandwidthOut(0)
#endif
{
	for (int i=0; i<MAX_NUM_TEAMS; i++)
	{
		m_TeamReservations[i] = 0;
	}

	for (int i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
	{
		m_ParticipantsArray[i] = NULL;
	}

#if __BANK
	m_bandwidthRecorderId = scriptInterface::GetScriptManager().AllocateBandwidthRecorderForScript(static_cast<scriptId&>(parentHandler.GetScriptId()).GetScriptName());
	Assert(m_bandwidthRecorderId != INVALID_RECORDER_ID);
#endif
}

scriptHandlerNetComponent::~scriptHandlerNetComponent()
{
	RemoveAllCandidates(false);
	RemoveAllParticipants(false);

	if (!gnetVerifyf(netInterface::IsShuttingDown() || (m_NumHostBroadcastData == 0 && m_NumPlayerBroadcastData == 0), "Destroying the net component for %s which has not unregistered the script broadcast data!", m_ParentHandler.GetLogName()))
	{
		netArrayManager_Script& arrayMgr = static_cast<netArrayManager_Script&>(netInterface::GetArrayManager());
		arrayMgr.UnregisterAllScriptBroadcastData(m_ParentHandler);
	}
}

void scriptHandlerNetComponent::SetMaxNumParticipants(u32 num)		
{ 
	gnetAssert(num<=MAX_NUM_PHYSICAL_PLAYERS); 
	m_MaxNumParticipants = static_cast<u8>(num);
    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Max num participants set to %d", m_MaxNumParticipants);
}

bool scriptHandlerNetComponent::IsHostLocal() const					
{ 
	return m_HostData ? (m_HostData->GetPlayer() && m_HostData->GetPlayer()->IsLocal()) : false;
}

ScriptParticipantId scriptHandlerNetComponent::GetLocalParticipantId() const
{
	ScriptParticipantId id = 0;

	const participantData* pData = GetParticipantDataForSlot(m_MySlotNumber);

	if (AssertVerify(pData))
	{
		id = pData->GetParticipantId();
	}

	return id;
}

void scriptHandlerNetComponent::SetTeamReservations(u32 numTeams, u32 teamReservations[])
{
	if (gnetVerify(numTeams == MAX_NUM_TEAMS) &&
		gnetVerifyf(m_MaxNumParticipants > 0, "scriptHandlerNetComponent::SetNumTeams: Max num participants has not been set"))
	{
		u32 count = 0;

		for (u32 i=0; i<MAX_NUM_TEAMS; i++)
		{
			m_TeamReservations[i] = static_cast<u8>(teamReservations[i]);
			count += m_TeamReservations[i];
            scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Team %d reservation = %d", m_MaxNumParticipants, i, m_TeamReservations[i]);
		}

		gnetAssertf(count <= m_MaxNumParticipants, "scriptHandlerNetComponent::SetTeamReservations: the total number of team reservations is greater than the max num participants");
	}
}

bool scriptHandlerNetComponent::Terminate()
{
	if (gnetVerify(IsActive()) && !m_Terminated)
	{
        scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Handler deactivated");

		FinishBroadcastingData();

		m_Terminated = true;

		return false;
	}

	return true;
}

scriptHandlerNetComponent::eExternalState	scriptHandlerNetComponent::GetState() const
{
	if (m_Terminated)
		return NETSCRIPT_TERMINATED;

	if (m_State == NETSCRIPT_INTERNAL_PLAYING)
		return NETSCRIPT_PLAYING;

	if (m_State == NETSCRIPT_INTERNAL_START)
		return NETSCRIPT_NOT_ACTIVE;

	switch (m_RejectionReason)
	{
		case REJECTED_INVALID:
		case REJECTED_CANNOT_PROCESS:
		case NUM_REJECTION_REASONS:
			break;
		case REJECTED_NO_ROOM_IN_SCRIPT_SESSION:
			return NETSCRIPT_FAILED_SESSION_FULL;
		case REJECTED_NO_JOIN_IN_PROGRESS:
			return NETSCRIPT_FAILED_NO_JOIN_IN_PROGRESS;
		case REJECTED_NO_ROOM_IN_TEAM:
			return NETSCRIPT_FAILED_TEAM_FULL;
	}

	Assert (m_State < NETSCRIPT_INTERNAL_PLAYING);

	return NETSCRIPT_JOINING;
}

void scriptHandlerNetComponent::PlayerHasJoined(const netPlayer& player)
{
	// send a handshake even if we are hosting the script: the previous host may have left and the joining player may already have been accepted into the
	// session before we are aware of him.
	if (IsActive() && !player.IsBot())
	{
		if (m_State == NETSCRIPT_INTERNAL_JOINING)
		{
			// flag this, so that the joining process can be restarted to include the new player if we fail to find a host
			m_PlayerJoined = true;
		}
		else if (m_State <= NETSCRIPT_INTERNAL_PLAYING)
		{
			if (IsPlayerAParticipant(*netInterface::GetLocalPlayer()))
			{
				// send a handshake to the joining player, he will add us as a candidate or participant if he is running the script
				participantData* pMyData = GetParticipantsData(*netInterface::GetLocalPlayer());

				if (pMyData)
				{
					msgScriptHandshake msg(m_ParentHandler.GetScriptId(), pMyData->GetParticipantId(), pMyData->GetSlotNumber());
					scriptInterface::GetScriptManager().SendMessageToPlayer(player, msg);
				}
			}
		}
	}
}

void scriptHandlerNetComponent::HandleLeavingPlayer(const netPlayer& player, bool playerLeavingSession)
{
	if (IsActive())
	{
		if (playerLeavingSession || (m_State == NETSCRIPT_INTERNAL_LEAVING || m_State == NETSCRIPT_INTERNAL_RESTARTING))
		{
			ClearWaitingForAck(player);
		}

		if (IsPlayerACandidate(player))
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Handling leaving player %s for %s", player.GetLogName(), m_ParentHandler.GetScriptId().GetLogName());

			RemovePlayerAsCandidate(player);
		}
		else if (IsPlayerAParticipant(player))
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Handling leaving player %s for %s", player.GetLogName(), m_ParentHandler.GetScriptId().GetLogName());

			bool bWasHost = m_PendingHost == &player;

			RemovePlayerAsParticipant(player);

			if (m_State == NETSCRIPT_INTERNAL_JOINING && bWasHost && gnetVerify(!m_PendingHost) && IsPlayerAParticipant(*netInterface::GetLocalPlayer()))
			{
				// if the host leaves while we are joining and we were accepted by him, remove ourselves as a valid participant and place ourselves back on the candidate list. The joining process will be restarted.
				scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Moving local player back to the candidate list, the host has left");
				RemovePlayerAsParticipant(*netInterface::GetLocalPlayer());
				AddPlayerAsCandidate(*netInterface::GetLocalPlayer());
			}
		}
	}
}

bool scriptHandlerNetComponent::Update()
{
#if __BANK
	if (GetNumParticipants() > 1)
	{
		m_timeHandlerActive += fwTimer::GetTimeStepInMilliseconds();
	}
#endif

	switch (m_State)
	{
	case NETSCRIPT_INTERNAL_START:
		{
			if (m_Terminated)
			{
				SetNextState(NETSCRIPT_INTERNAL_FINISHED);
			}
			else if (CanStartJoining())
			{
				SetNextState(NETSCRIPT_INTERNAL_JOIN);
			}
		}
		break;
	case NETSCRIPT_INTERNAL_JOIN:
		{
			// add ourselves as a candidate
			AddPlayerAsCandidate(*netInterface::GetLocalPlayer());

			m_PendingHost			= NULL;
			m_HostToken				= INVALID_HOST_TOKEN;
			m_Accepted				= false;
			m_RejectionReason		= REJECTED_INVALID;
			m_WaitingForAcks		= 0;
			m_PreferredInitialHost	= m_HostRequested ? netInterface::GetLocalPlayer() : NULL;
	
			// send a join message to all players in scope
			msgScriptJoin msg(m_ParentHandler.GetScriptId(), netInterface::GetLocalPlayer()->GetTeam(), m_HostRequested);

			scriptInterface::GetScriptManager().SendMessageToAllPlayersInScope(*this, msg);

			if (m_WaitingForAcks)
			{
				// there are players in scope, wait for replies
				SetNextState(NETSCRIPT_INTERNAL_JOINING);

				m_PlayerJoined = false;
			}
			else if (!m_Terminated)
			{
			    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "No other players in scope, we become host");

				// and we become host.
				SetNewScriptHost(netInterface::GetLocalPlayer(), 1, false);

				SetNextState(NETSCRIPT_INTERNAL_READY_TO_PLAY);
			}
			else
			{
				SetNextState(NETSCRIPT_INTERNAL_LEAVE);
			}
		}
		break;
	case NETSCRIPT_INTERNAL_JOINING:
		{
			if (m_WaitingForAcks == 0)
			{
				// waiting for all responses for the messages sent in the _STARTING state
				if (m_Terminated)
				{
					// the script has been terminated
					SetNextState(NETSCRIPT_INTERNAL_LEAVE);
				}
				else if (m_Accepted)
				{
					// the host may have just left, in this case we need to start again
					if (!m_PendingHost)
					{
						scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "The pending host has left, trying again");

						SetNextState(NETSCRIPT_INTERNAL_RESTARTING);
					}
					else
					{
						// accepted by host
						SetNextState(NETSCRIPT_INTERNAL_ACCEPTED);
					}
				}
				else if (m_RejectionReason > REJECTED_CANNOT_PROCESS)
				{
					// rejected by host
					SetNextState(NETSCRIPT_INTERNAL_LEAVE);
				}
				else if (m_NumParticipants > 0 || m_PlayerJoined || m_RejectionReason == REJECTED_CANNOT_PROCESS || !CanStartJoining())
				{
				    if (m_PlayerJoined)
					    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "A new player has joined, trying again");
				    else if (m_NumParticipants > 0)
					    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Found participants but no host, trying again");
				    else if (m_RejectionReason == REJECTED_CANNOT_PROCESS)
						scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "The host cannot process our join request, trying again");
					else
					    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Found no participants but it is not safe to host, trying again");

					// found participants but no host, or new players have joined the session, so try again
					SetNextState(NETSCRIPT_INTERNAL_RESTARTING);
				}
				else if (CanStartHosting())
				{
					if (m_NumCandidates > 1)
					{
						// found candidates but no participants. Make ourselves host if we have lowest peer id
						const netPlayer* pNewHost = m_PreferredInitialHost ? m_PreferredInitialHost : DetermineHostFromCandidates();

						if (pNewHost && pNewHost->IsMyPlayer())
						{
						    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Found %d candidates but no host, we become host due to lowest gamer id", m_NumCandidates);

							SetNewScriptHost(pNewHost, 1, false);

							// flush candidate list (existing candidates will be restarting)
							RemoveAllCandidates();

							SetNextState(NETSCRIPT_INTERNAL_READY_TO_PLAY);
						}
						else
						{
                            scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Found %d candidates but no host, try again (there is a candidate with a lower gamer id)", m_NumCandidates);

							// try again
							SetNextState(NETSCRIPT_INTERNAL_RESTARTING);
						}
					}
					else
					{
						// no-one else playing so we become host
					    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "No one else is running this script, we become host");

						SetNewScriptHost(netInterface::GetLocalPlayer(), 1, false);

						SetNextState(NETSCRIPT_INTERNAL_READY_TO_PLAY);
					}
				}
				else
				{
                    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Found no host, but can't start hosting, try again");

					SetNextState(NETSCRIPT_INTERNAL_RESTARTING);
				}
			}
		}
		break;
	case NETSCRIPT_INTERNAL_RESTARTING:
		{
			m_WaitingForAcks = 0;

			// If we have been accepted by a previous host, we need to sent a message to all other players
			// informing them that we have left before restarting
			// This is because we may be given a different participant slot by the new host.
			if (m_MySlotNumber != INVALID_SLOT)
			{
				msgScriptLeave msg(m_ParentHandler.GetScriptId());
				scriptInterface::GetScriptManager().SendMessageToAllParticipants(*this, msg, true);
				scriptInterface::GetScriptManager().SendMessageToAllCandidates(*this, msg);
			}

			// remove ourselves as a participant so that we do not send out any participant data while we are restarting
			if (IsPlayerAParticipant(*netInterface::GetLocalPlayer()))
			{
				RemovePlayerAsParticipant(*netInterface::GetLocalPlayer());
			}

			SetNextState(NETSCRIPT_INTERNAL_RESTARTED);
		}
		break;
	case NETSCRIPT_INTERNAL_RESTARTED:
		{
			if (m_WaitingForAcks == 0)
			{
				RestartJoiningProcess();
			}	
		}
		break;
	case NETSCRIPT_INTERNAL_ACCEPTED:
		{
			if (m_Terminated)
			{
				// the script has been terminated
				SetNextState(NETSCRIPT_INTERNAL_LEAVE);
			}
			else if (!m_PendingHost)
			{
				// the host may have left, restart the joining process
				scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "The host has left, restart joining process");

				SetNextState(NETSCRIPT_INTERNAL_RESTARTING);
			}
			else
			{
				SetNewScriptHost(m_PendingHost, m_HostToken, false);

				if (m_NumCandidates > 0 || m_NumParticipants > 0)
				{
					participantData* pMyData = GetParticipantsData(*netInterface::GetLocalPlayer());

					if (gnetVerify(pMyData))
					{
						// accepted by the host, handshake with all other players involved in the script
						msgScriptHandshake msg(m_ParentHandler.GetScriptId(), pMyData->GetParticipantId(), pMyData->GetSlotNumber());

						scriptInterface::GetScriptManager().SendMessageToAllParticipants(*this, msg, true, false);
						scriptInterface::GetScriptManager().SendMessageToAllCandidates(*this, msg);

						SetNextState(NETSCRIPT_INTERNAL_HANDSHAKING);
					}
				}
				else
				{
					gnetAssert(GetHost() == netInterface::GetLocalPlayer());
					SetNextState(NETSCRIPT_INTERNAL_READY_TO_PLAY);
				}
			}
		}
		break;
	case NETSCRIPT_INTERNAL_HANDSHAKING:
		{
			// waiting for all players to acknowledge our participation
			if (m_WaitingForAcks == 0)
			{
				if (m_Terminated)
				{
					// the script has been terminated
					SetNextState(NETSCRIPT_INTERNAL_LEAVE);
				}
				else
				{
					SetNextState(NETSCRIPT_INTERNAL_READY_TO_PLAY);
				}
			}
		}
		break;
	case NETSCRIPT_INTERNAL_READY_TO_PLAY:
		{
			if (m_Terminated)
			{
				// the script has been terminated
				SetNextState(NETSCRIPT_INTERNAL_LEAVE);
			}
			else
			{
				// the host may have left while we were handshaking, so determine the new one
				if (!GetHost() && !IsHostMigrating())
				{
					if (m_NumParticipants == 1)
					{
						// there is only us left in the script session, so we become host
						SetNewScriptHost(netInterface::GetLocalPlayer(), m_HostToken);
					}
					else 
					{
						const netPlayer* pNextHost = DetermineHostFromParticipants();

						if (pNextHost && pNextHost->IsLocal())
						{
							// we are assuming we will be the next host due to the previous host dropping out. In this case we need to verify that
							// we can host (there may be another valid host)
							VerifyLocalHost();
						}
					}
				}

				if (GetHost())
				{
					StartPlaying();
					SetNextState(NETSCRIPT_INTERNAL_PLAYING);
				}
				else
				{
					scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot start playing - there is no host");
					
					if (m_PendingHost && m_PendingHost->IsLocal())
					{
						HandleHostMigration();
					}
				}
			}
		}
		break;
	case NETSCRIPT_INTERNAL_PLAYING:
		{
			// waiting for the script to terminate.
			if (m_Terminated)
			{
				SetNextState(NETSCRIPT_INTERNAL_LEAVE);
			}
			else 
			{
				HandleHostMigration();
			}
		}
		break;
	case NETSCRIPT_INTERNAL_LEAVE:
		{
			if (DoLeaveCleanup())
			{
				// we have been rejected or the script has terminated, inform all other players 
				msgScriptLeave msg(m_ParentHandler.GetScriptId());

				m_WaitingForAcks = 0;

				scriptInterface::GetScriptManager().SendMessageToAllParticipants(*this, msg, true);
				scriptInterface::GetScriptManager().SendMessageToAllCandidates(*this, msg);

				SetNextState(NETSCRIPT_INTERNAL_LEAVING);
			}
		}
		break;
	case NETSCRIPT_INTERNAL_LEAVING:
		{
			// waiting for all players to acknowledge our leaving. We must do this to prevent confusing the host by potentially sending 
			// two joins if the script is restarted
			if (m_WaitingForAcks == 0)
			{
				SetNextState(NETSCRIPT_INTERNAL_TERMINATING);
			}
		}
		break;
	case NETSCRIPT_INTERNAL_TERMINATING:
		{
			// wait for the script to terminate
			if (DoTerminationCleanup())
			{
                scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "TERMINATED");

				// unregister all broadcast data associated with this script
				if (dynamic_cast<netArrayManager_Script*>(&netInterface::GetArrayManager()))
				{
					netArrayManager_Script& arrayMgr = static_cast<netArrayManager_Script&>(netInterface::GetArrayManager());

					arrayMgr.UnregisterAllScriptBroadcastData(m_ParentHandler);

					m_NumHostBroadcastData = m_NumPlayerBroadcastData = 0;
				}

				// finished
				SetNextState(NETSCRIPT_INTERNAL_FINISHED);
			}
		}
		break;
	case NETSCRIPT_INTERNAL_FINISHED:
		return true;
	}

	return false;
}


bool scriptHandlerNetComponent::CanMigrateScriptHost() const
{
	bool bCanMigrate = false;

	if (!m_PreventHostMigration &&
		!m_PendingHost &&
		m_MaxNumParticipants > 2 && 
		m_NumParticipants > 1 && 
		IsHostLocal() && 
		m_HostMigrationTimer == 0)
	{
		bCanMigrate = true;
	}

#if ENABLE_NETWORK_BOTS
	// if there are any bots in the session, prevent script host migration as it does not currently cope with bots in the session 
	if (netInterface::GetNumTotalActiveBots() > 0)
	{
		bCanMigrate = false; 
	}
#endif

	if (!bCanMigrate)
	{
		// reset the flag preventing migration. This must be repeatedly set while we want to disallow migration. 
		m_PreventHostMigration = false;
	}

	return bCanMigrate;
}

void scriptHandlerNetComponent::MigrateScriptHost(const netPlayer& player)
{
	Assert(CanMigrateScriptHost());
	Assert(IsHostLocal());
	Assert(IsPlayerAParticipant(player.GetPhysicalPlayerIndex()));

	msgScriptMigrateHost migrateHost(m_ParentHandler.GetScriptId(), m_HostToken, netInterface::GetLocalPlayer()->GetSizeOfNetArrayData());
	scriptInterface::GetScriptManager().SendMessageToPlayer(player, migrateHost);

	SetPendingHost(player, PENDING_HOST_MIGRATION);
	m_HostMigrationTimer = TIME_BETWEEN_HOST_MIGRATION_ATTEMPTS;

	// clear the host while the migration is taking place
	SetNewScriptHost(NULL, 0);
}

bool scriptHandlerNetComponent::IsParticipantActive(int slotNumber) const
{
	bool bParticipantActive = (m_activeParticipants & (1<<slotNumber)) != 0;

	gnetAssert(bParticipantActive == (GetParticipantUsingSlot(slotNumber) != NULL));

	return bParticipantActive;
}

int scriptHandlerNetComponent::GetSlotParticipantIsUsing(const netPlayer& player) const
{
	if (player.IsMyPlayer())
	{
		gnetAssertf(m_MySlotNumber != INVALID_SLOT , "GetSlotParticipantIsUsing: Our player is not a participant");

		return m_MySlotNumber;
	}

	const participantData* pData = GetParticipantsData(player);

	gnetAssertf(pData, "GetSlotParticipantIsUsing: Player %s is not a participant", player.GetLogName());

	if (pData)
	{
		return static_cast<int>(pData->GetSlotNumber());
	}

	return INVALID_SLOT;
}

const netPlayer* scriptHandlerNetComponent::GetParticipantUsingSlot(int slotNumber) const
{
	if (slotNumber == m_MySlotNumber)
	{
		return netInterface::GetPlayerMgr().GetMyPlayer();
	}

	const participantData* pData = GetParticipantDataForSlot(slotNumber);

	if (pData)
	{
		return pData->GetPlayer();
	}

	return NULL;
}

u32 scriptHandlerNetComponent::GetTimePlayerHasBeenAParticipant(int slotNumber) const
{
	const participantData* pData = GetParticipantDataForSlot(slotNumber);

	if (pData)
	{
		return pData->GetTimeInScriptSession();
	}

	return 0;
}

void scriptHandlerNetComponent::HandleJoinMsg(const msgScriptJoin& msg, const ReceivedMessageData &messageData)
{
	netLoggingInterface *log = scriptInterface::GetScriptManager().GetLog();

	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());
	
	netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// we may not be up to date with the player's team at this point
	fromPlayer.SetTeam(msg.m_Team);

	participantData* pMyData = NULL;

	bool bIAmParticipant = IsPlayerAParticipant(*netInterface::GetLocalPlayer()); 

	if (bIAmParticipant)
	{
		pMyData = GetParticipantsData(*netInterface::GetLocalPlayer());
		
		if (!gnetVerify(pMyData))
			return;
	}

	eAckCode ackCode = SCRIPT_ACK_CODE_INVALID;

	// only used by host:
	bool bAcceptedByHost = false;
	ScriptParticipantId newParticipantId = INVALID_PARTICIPANT_ID;
	ScriptSlotNumber newSlotNumber = INVALID_SLOT;
	eRejectionReason rejection =  REJECTED_INVALID;

	if (m_State > NETSCRIPT_INTERNAL_LEAVE)
	{
		// once we have started the leaving process for this script, pretend we are not running it.
		log->WriteDataValue("IGNORE", "We are leaving");
		ackCode = SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT;
	}
	else if (fromPlayer.GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX)
	{
		// we can't process the join if the player does not have a valid physical player index
		log->WriteDataValue("CAN'T PROCESS", "Player has is not physical");
		ackCode = SCRIPT_ACK_CODE_NOT_PROCESSED;
	}
	else if (!bIAmParticipant)
	{
		// add this player as a candidate, otherwise we may make ourselves host prematurely (we may have received this join after
		// a join ack from the player that had the player not running the script. ie the player may have started the script immediately
		// after it got the join message from our player)
		AddPlayerAsCandidate(fromPlayer);

		if (msg.m_RequestToHost && gnetVerifyf(!m_PreferredInitialHost, "%s: more than one player is requesting to host!", m_ParentHandler.GetScriptId().GetLogName()))
		{
			m_PreferredInitialHost = &fromPlayer;
		}

		// we are running the script but are not yet participating in it
		ackCode = SCRIPT_ACK_CODE_CANDIDATE;
	}
	else if (GetHost() && GetHost()->IsLocal())
	{
		if (m_PendingHost != NULL)
		{
			// we can't process the join if the host is migrating
			log->WriteDataValue("CAN'T PROCESS", "Host is migrating");
			ackCode = SCRIPT_ACK_CODE_NOT_PROCESSED;
		}
		else
		{
			bAcceptedByHost = TryToAddPlayerAsParticipant(fromPlayer, newParticipantId, newSlotNumber, rejection);
		}
	}
	else
	{
		if (IsPlayerACandidate(fromPlayer))
		{
            if(log)
            {
			    log->WriteDataValue("WARNING", "Player is already a candidate");
            }
		}
		else 
		{
			AddPlayerAsCandidate(fromPlayer);
		}
	}

	// acknowledge this message
	if (ackCode != SCRIPT_ACK_CODE_INVALID)
	{
		// failed response
		msgScriptJoinAck ack(*msg.m_ScriptId, ackCode);
		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
	}
	else if (bAcceptedByHost)
	{
		// host accepted response
		msgScriptJoinHostAck ack(m_ParentHandler.GetScriptId(), 
								pMyData->GetParticipantId(), pMyData->GetSlotNumber(), 
								newParticipantId, newSlotNumber, m_HostToken);

		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
	}
	else if (rejection != REJECTED_INVALID)
	{
		// host rejected response
		msgScriptJoinHostAck ack(m_ParentHandler.GetScriptId(), 
								pMyData->GetParticipantId(), pMyData->GetSlotNumber(), 
								rejection);

		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
	}
	else 
	{
		// client accepted response
		msgScriptJoinAck ack(m_ParentHandler.GetScriptId(), pMyData->GetParticipantId(), pMyData->GetSlotNumber());
		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
	}
}

void scriptHandlerNetComponent::HandleJoinAckMsg(const msgScriptJoinAck& msg, const ReceivedMessageData &messageData)
{
	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (m_State < NETSCRIPT_INTERNAL_JOINING)
	{
		// we may have restarted the joining process if another player joined the session, so ignore this ack
		if(log)
        {
            log->WriteDataValue("IGNORED", "Not in a joining state");
        }
		return;
	}

	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(m_State == NETSCRIPT_INTERNAL_JOINING || msg.m_AckCode == SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT); 

	switch (msg.m_AckCode)
	{
	case SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT:
		{
			ClearWaitingForAck(fromPlayer);
		}
		break;
	case SCRIPT_ACK_CODE_PARTICIPANT:
		{
			ClearWaitingForAck(fromPlayer);
			AddPlayerAsParticipant(fromPlayer, msg.m_ParticipantId, msg.m_SlotNumber);
		}
		break;
	case SCRIPT_ACK_CODE_CANDIDATE:
		{
			ClearWaitingForAck(fromPlayer);
			AddPlayerAsCandidate(fromPlayer);
		}
		break;
	case SCRIPT_ACK_CODE_NOT_PROCESSED:
		{
			// the original join could not be processed, so send it again
			msgScriptJoin msg(m_ParentHandler.GetScriptId());
			scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, msg);
		}
		break;
	default:
		gnetAssertf(0, "Unknown ack code");
	}
}

void scriptHandlerNetComponent::HandleJoinHostAckMsg(const msgScriptJoinHostAck& msg, const ReceivedMessageData &messageData)
{
	netLoggingInterface *log = scriptInterface::GetScriptManager().GetLog();

	if (m_State < NETSCRIPT_INTERNAL_JOINING)
	{
		// we may have restarted the joining process if another player joined the session, so ignore this ack
		if(log)
        {
            log->WriteDataValue("IGNORED", "Not in a joining state");
        }
		return; 
	}

	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);

	netPlayer* pMyPlayer = netInterface::GetLocalPlayer();
	gnetAssert(pMyPlayer);

	ClearWaitingForAck(fromPlayer);

	m_Accepted				= false;
	m_RejectionReason		= REJECTED_INVALID;

	gnetAssert(m_State == NETSCRIPT_INTERNAL_JOINING); 

	if (msg.m_Accepted)
	{
		AddPlayerAsParticipant(fromPlayer, msg.m_HostParticipantId, msg.m_HostSlotNumber);
	
		const netPlayer* hostPlayer = m_PendingHost;

		if (hostPlayer)
		{
			if (AssertVerify(hostPlayer != &fromPlayer))
			{
			    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Host conflict!");

				// two hosts discovered! Dump the host with the lowest token
				Assert(m_HostToken != msg.m_HostToken);

				if (m_HostToken < msg.m_HostToken)
				{
					SetPendingHost(fromPlayer, PENDING_HOST_JOINING);
					m_HostToken	= msg.m_HostToken;

				    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "This player is the real host");
				}
				else
				{
				    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "This player is not the real host");
				}
			}
		}
		else
		{
			SetPendingHost(fromPlayer, PENDING_HOST_JOINING);
			m_HostToken	  = msg.m_HostToken;
		}

		// add ourselves as a participant
		AddPlayerAsParticipant(*pMyPlayer, msg.m_ClientParticipantId, msg.m_ClientSlotNumber);

		m_Accepted = true;
	}
	else
	{
		m_RejectionReason = static_cast<s8>(msg.m_RejectionReason);
	}
}

void scriptHandlerNetComponent::HandleHandshakeMsg(const msgScriptHandshake& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;
	const netPlayer& toPlayer = *messageData.m_ToPlayer;

	eAckCode ackCode = SCRIPT_ACK_CODE_INVALID;

	// we can't process the handshake if the player does not have a valid physical player index 
	if (fromPlayer.GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX)
	{
		ackCode = SCRIPT_ACK_CODE_NOT_PROCESSED;
	}
	else if (!IsPlayerAParticipant(fromPlayer))
	{
		if (m_State >= NETSCRIPT_INTERNAL_JOINING && m_State <= NETSCRIPT_INTERNAL_PLAYING)
		{
			if (!AddPlayerAsParticipant(fromPlayer, msg.m_ParticipantId, msg.m_SlotNumber))
			{
				ackCode = SCRIPT_ACK_CODE_NOT_PROCESSED;
			}
		}
	}
	else
	{
#if __DEV
		participantData* pData = GetParticipantsData(fromPlayer);

		if (gnetVerify(pData))
		{
			gnetAssert(pData->GetParticipantId() == msg.m_ParticipantId);
			gnetAssert(pData->GetSlotNumber() == msg.m_SlotNumber);
		}
#endif
	}

	// if we are shutting down this script, pretend we are not running it 
	if (ackCode == SCRIPT_ACK_CODE_INVALID && (!IsPlayerAParticipant(*netInterface::GetLocalPlayer()) || m_State > NETSCRIPT_INTERNAL_LEAVE))
	{
		if (m_State <= NETSCRIPT_INTERNAL_PLAYING)
			ackCode = SCRIPT_ACK_CODE_CANDIDATE;
		else
			ackCode = SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT;
	}

	// acknowledge this message
	if (ackCode != SCRIPT_ACK_CODE_INVALID)
	{
		msgScriptHandshakeAck ack(m_ParentHandler.GetScriptId(), ackCode);
		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
	}
	else
	{
		participantData* pMyData = GetParticipantsData(toPlayer);

		if (gnetVerify(pMyData))
		{
			if (IsHostLocal())
			{
				msgScriptHandshakeAck ack(m_ParentHandler.GetScriptId(), pMyData->GetParticipantId(), pMyData->GetSlotNumber(), true, m_HostToken);
				scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
			}
			else
			{
				msgScriptHandshakeAck ack(m_ParentHandler.GetScriptId(), pMyData->GetParticipantId(), pMyData->GetSlotNumber());
				scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
			}
		}
	}
}

void scriptHandlerNetComponent::HandleHandshakeAckMsg(const msgScriptHandshakeAck& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);

	switch (msg.m_AckCode)
	{
	case SCRIPT_ACK_CODE_NOT_RUNNING_SCRIPT:
		{
			if (m_State == NETSCRIPT_INTERNAL_HANDSHAKING)
			{
				if (IsPlayerAParticipant(fromPlayer))
				{
					// player may have just left 
					if (gnetVerify(!(GetHost() && GetHost()->IsLocal())))
					{
						RemovePlayerAsParticipant(fromPlayer);
					}
				}
				else if (IsPlayerACandidate(fromPlayer))
				{
					RemovePlayerAsCandidate(fromPlayer);
				}
			}

			ClearWaitingForAck(fromPlayer);
		}
		break;

	case SCRIPT_ACK_CODE_PARTICIPANT:
		{
			ClearWaitingForAck(fromPlayer);

			bool bParticipant = IsPlayerAParticipant(fromPlayer);

			// handle the case where the player has left and rejoined in between us sending a join and a handshake. 
			if (bParticipant)
			{
				participantData* pData = GetParticipantsData(fromPlayer);

				if (gnetVerify(pData))
				{
					if (pData->GetParticipantId() != msg.m_ParticipantId || 
						pData->GetSlotNumber() != msg.m_SlotNumber)
					{
						RemovePlayerAsParticipant(fromPlayer);
						bParticipant = false;
					}
				}
			}

			if (!bParticipant)
			{
				AddPlayerAsParticipant(fromPlayer, msg.m_ParticipantId, msg.m_SlotNumber);
			}

			// this machine may have become the host before they were aware of our machine as a participant
			if (msg.m_Host)
			{
				if (&fromPlayer == GetHost())
				{
					gnetAssert(msg.m_HostToken == m_HostToken);
				}
				else 
				{
					gnetAssert(msg.m_HostToken > m_HostToken);
					SetNewScriptHost(&fromPlayer, msg.m_HostToken);
				}
			}
		}
		break;

	case SCRIPT_ACK_CODE_CANDIDATE:
		{
			// it is possible that the player left and immediately tried to rejoin the script session in between processing a join and a handshake from us
			if (m_State == NETSCRIPT_INTERNAL_HANDSHAKING)
			{
				if (IsPlayerAParticipant(fromPlayer))
				{
					RemovePlayerAsParticipant(fromPlayer);
				}
			}

			ClearWaitingForAck(fromPlayer);
		}
		break;

	case SCRIPT_ACK_CODE_NOT_PROCESSED:
		{
			if (m_State <= NETSCRIPT_INTERNAL_PLAYING)
			{
				// the original handshake could not be processed, so send it again
				participantData* pMyData = GetParticipantsData(*netInterface::GetLocalPlayer());

				if (gnetVerify(pMyData))
				{
					msgScriptHandshake msg(m_ParentHandler.GetScriptId(), pMyData->GetParticipantId(), pMyData->GetSlotNumber());
					scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, msg);
				}
			}
		}
		break;

	default:
		gnetAssertf(0, "Unknown ack code");
	}
}

void scriptHandlerNetComponent::HandleLeaveMsg(const msgScriptLeave& NET_ASSERTS_ONLY(msg), const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	HandleLeavingPlayer(fromPlayer, false);

	// acknowledge this message
	msgScriptLeaveAck ack(m_ParentHandler.GetScriptId());

	scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack);
}

void scriptHandlerNetComponent::HandleLeaveAckMsg(const msgScriptLeaveAck& NET_ASSERTS_ONLY(msg), const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	if (m_State == NETSCRIPT_INTERNAL_LEAVING || m_State == NETSCRIPT_INTERNAL_RESTARTED)
	{
		ClearWaitingForAck(fromPlayer);
	}
}

void scriptHandlerNetComponent::HandleBotJoinMsg(const msgScriptBotJoin& msg, const ReceivedMessageData &messageData)
{
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	netPlayer& fromPlayer = *messageData.m_FromPlayer;

	if (IsHostLocal() && 
		m_State == NETSCRIPT_INTERNAL_PLAYING && 
		fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX) 
	{
		// we may not be up to date with the player's team at this point
		fromPlayer.SetTeam(msg.m_Team);

		participantData* pMyData = GetParticipantsData(*netInterface::GetLocalPlayer());

		if (gnetVerify(pMyData))
		{
			ScriptParticipantId newParticipantId = INVALID_PARTICIPANT_ID;
			ScriptSlotNumber newSlotNumber = INVALID_SLOT;
			eRejectionReason rejection =  REJECTED_INVALID;

			if (TryToAddPlayerAsParticipant(fromPlayer, newParticipantId, newSlotNumber, rejection))
			{
				// send an ack back with the bot's participant info
				msgScriptBotJoinAck ack(m_ParentHandler.GetScriptId(), newParticipantId, newSlotNumber);
				scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack, netInterface::GetLocalPlayer());
			}
			else
			{
				// send an ack back rejecting the bot
				msgScriptBotJoinAck ack(m_ParentHandler.GetScriptId(), rejection);
				scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack, netInterface::GetLocalPlayer());
			}
		}
	}
}

void scriptHandlerNetComponent::HandleBotJoinAckMsg(const msgScriptBotJoinAck& msg, const ReceivedMessageData &messageData)
{
	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	const netPlayer& fromPlayer = *messageData.m_FromPlayer;
	const netPlayer& toPlayer = *messageData.m_ToPlayer;

	if (GetHost() == &fromPlayer)
	{
		if (msg.m_Accepted)
		{
			if (!IsPlayerAParticipant(toPlayer))
			{
				// add the bot as a participant
				AddPlayerAsParticipant(toPlayer, msg.m_ParticipantId, msg.m_SlotNumber);
			}
			
			// handshake with all other non-bot participants informing them about the bot joining
			unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
            const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

            for(unsigned index = 0; index < numPhysicalPlayers; index++)
            {
                const netPlayer *pPlayer = allPhysicalPlayers[index];

				if (!pPlayer->IsBot() && !pPlayer->IsLocal() && IsPlayerAParticipant(*pPlayer))
				{
					msgScriptBotHandshake handshake(m_ParentHandler.GetScriptId(), msg.m_ParticipantId, msg.m_SlotNumber);
					scriptInterface::GetScriptManager().SendMessageToPlayer(*pPlayer, handshake, &toPlayer);
				}
			}
		}
		else
		{
			gnetAssert(!IsPlayerAParticipant(toPlayer));

			switch (msg.m_RejectionReason)
			{
			case REJECTED_CANNOT_PROCESS:
				// try again
				if (CanAddPlayerBot(toPlayer))
				{
					AddPlayerBot(toPlayer);
				}
				break;
			case REJECTED_NO_ROOM_IN_SCRIPT_SESSION:
				gnetAssertf(0, "Bot %s could not join script %s - there is no room for any more participants", toPlayer.GetLogName(), m_ParentHandler.GetScriptId().GetLogName());
				break;
			case REJECTED_NO_JOIN_IN_PROGRESS:
				gnetAssertf(0, "Bot %s could not join script %s - the script is not allowing join in progress", toPlayer.GetLogName(), m_ParentHandler.GetScriptId().GetLogName());
				break;
			case REJECTED_NO_ROOM_IN_TEAM:
				gnetAssertf(0, "Bot %s could not join script %s - there is no room for any more participants in the bot's team ", toPlayer.GetLogName(), m_ParentHandler.GetScriptId().GetLogName());
				break;
			default:
				gnetAssertf(0, "Unrecognised rejection reason");
			}
		}
	}
	else
	{
        if(log)
        {
		    log->WriteDataValue("FAILED", "Host has changed");
        }
	}
}

void scriptHandlerNetComponent::HandleBotHandshakeMsg(const msgScriptBotHandshake& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	if (fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX && 
		m_State >= NETSCRIPT_INTERNAL_PLAYING)
	{
		if (m_State == NETSCRIPT_INTERNAL_PLAYING)
		{
			if (!IsPlayerAParticipant(fromPlayer))
			{
				AddPlayerAsParticipant(fromPlayer, msg.m_ParticipantId, msg.m_SlotNumber);
			}
			else
			{
#if __DEV
				participantData* pData = GetParticipantsData(fromPlayer);

				if (gnetVerify(pData))
				{
					gnetAssert(pData->GetParticipantId() == msg.m_ParticipantId);
					gnetAssert(pData->GetSlotNumber() == msg.m_SlotNumber);
				}
#endif
			}
		}
	}
	else
	{
		// only send an ack if we couldn't process the handshake - the bot will send it again
		msgScriptBotHandshakeAck ack(m_ParentHandler.GetScriptId());
		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack, &toPlayer);
		return;
	}
}

void scriptHandlerNetComponent::HandleBotHandshakeAckMsg(const msgScriptBotHandshakeAck& UNUSED_PARAM(msg), const ReceivedMessageData &messageData)
{
	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	// we only receive a bot handshake ack if the initial handshake could not be processed. Send it again if necessary
	if (m_State == NETSCRIPT_INTERNAL_PLAYING && 
		gnetVerify(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX) &&
		IsPlayerAParticipant(toPlayer) && 
		IsPlayerAParticipant(fromPlayer))
	{
		participantData* pData = GetParticipantsData(toPlayer);

		if (gnetVerify(pData))
		{
			msgScriptBotHandshake handshake(m_ParentHandler.GetScriptId(), pData->GetParticipantId(), pData->GetSlotNumber());
			scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, handshake, &toPlayer);
		}
	}
}

void scriptHandlerNetComponent::HandleBotLeaveMsg(const msgScriptBotLeave& UNUSED_PARAM(msg), const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	if (gnetVerify(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX) &&
		IsPlayerAParticipant(fromPlayer))
	{
		RemovePlayerAsParticipant(fromPlayer);
	}
}

void scriptHandlerNetComponent::HandleMigrateHostMsg(const msgScriptMigrateHost& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& toPlayer = *messageData.m_ToPlayer;
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	bool accepted = false;

	if (msg.m_HostToken >= m_HostToken) 
	{
		if (m_State == NETSCRIPT_INTERNAL_PLAYING && !m_Terminated)
		{
			if (GetHost() != &fromPlayer)
			{
				LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Message is not from the current host (%s)", GetHost() ? GetHost()->GetLogName() : "-none-"));
			}
			else if (m_HostRequested || (toPlayer.GetSizeOfNetArrayData() + m_sizeOfHostBroadcastData < msg.m_SizeOfNetArrayData))
			{
				SetNewScriptHost(netInterface::GetLocalPlayer(), msg.m_HostToken+1);

				accepted = true;
			}
			else
			{
				LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "This machine will be arbitrating over too much array data (%d)", toPlayer.GetSizeOfNetArrayData() + m_sizeOfHostBroadcastData));
			}
		}
		else
		{
			LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Handler is not in a playing state"));
		}
	}
	else
	{
		LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Old host token. Current = %d", m_HostToken));
	}

	if (!accepted)
	{
		msgScriptMigrateHostFailAck ack(m_ParentHandler.GetScriptId());
		scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, ack, &toPlayer);
	}
}

void scriptHandlerNetComponent::HandleMigrateHostFailAckMsg(const msgScriptMigrateHostFailAck& NET_ASSERTS_ONLY(msg), const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	// the pending host can be null if if it was a participant who has just left
	if (gnetVerify(!m_PendingHost || m_PendingHost == &fromPlayer))
	{
		HostMigrationFailed(false);
	}
}

void scriptHandlerNetComponent::HandleHostRequestMsg(const msgScriptHostRequest& NET_ASSERTS_ONLY(msg), const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	if (GetHost() != &fromPlayer)
	{
		if (IsHostLocal())
		{
			if (gnetVerifyf(!m_PreventHostMigration, "Another machine is requesting to host %s when it has been flagged to not allow host migration", m_ParentHandler.GetScriptId().GetLogName()) &&
				gnetVerifyf(!m_HostRequested, "Another machine is requesting to host %s when our machine is also requesting to host", m_ParentHandler.GetScriptId().GetLogName()))
			{
				if (m_ParentHandler.AreAllParticipantsUpToDateWithHostState(true))
				{
					// force immediate migration
					m_HostMigrationTimer = 0;

					MigrateScriptHost(fromPlayer);
				}
				else
				{
					LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "All participants are not up to date with the host state"));
				}
			}
		}
		else
		{
			LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "We are not hosting"));
		}
	}
	else
	{
		LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Player is already hosting"));
	}
}

void scriptHandlerNetComponent::HandleNewHostMsg(const msgScriptNewHost& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	if (GetHost() == &fromPlayer)
	{
		if (gnetVerify(msg.m_HostToken >= m_HostToken))
		{
			m_HostToken = msg.m_HostToken;
		}
	}
	else if (msg.m_HostToken > m_HostToken)
	{	
		if (m_State < NETSCRIPT_INTERNAL_ACCEPTED)
		{
			// we have been accepted into the session by a previous host, we will have to restart the joining process again
			if (m_PendingHost != &fromPlayer)
			{
				ClearPendingHost();
			}
		}
		else
		{	
			SetNewScriptHost(&fromPlayer, msg.m_HostToken);
		}
	}
	else 
	{
		Assertf(0, "%s : Received new host message from invalid host %s (token: %d). Current host %s (token : %d)", m_ParentHandler.GetLogName(), fromPlayer.GetLogName(), msg.m_HostToken, GetHost() ? GetHost()->GetLogName() : "-none", m_HostToken);
		LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**ERROR**", "Host token is less than current (%d)", m_HostToken));
	}
}

void scriptHandlerNetComponent::HandleVerifyHostMsg(const msgScriptVerifyHost& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	bool bRejected		= false;
	bool bValidHost		= false;

	if (GetHost() && GetHost()->IsLocal())
	{
		LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "We are the host"));
		bValidHost = true;
		bRejected = true;
	}
	else if (m_State < NETSCRIPT_INTERNAL_ACCEPTED)
	{
		// we have been accepted into the session by a previous host, we will have to restart the joining process again
		if (m_PendingHost != &fromPlayer)
		{
			ClearPendingHost();
		}
	}
	else if (m_State < NETSCRIPT_INTERNAL_READY_TO_PLAY)
	{
		if (m_PendingHost != &fromPlayer)
		{	
			// if we are still in the handshaking state we can't let the new host start hosting because they may accept other participants into the session
			// before becoming aware of us as a participant
			LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "We are still handshaking"));
			bRejected = true;
		}
	}
	else if (m_State < NETSCRIPT_INTERNAL_LEAVING)
	{
		if (GetHost())
		{
			if (msg.m_HostToken <= m_HostToken)
			{
				LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Old host token. Current token = %d. Current host : %s", m_HostToken, GetHost()->GetLogName()));
				bRejected = true;
			}
		}
		else if (m_LastHost && m_LastHost != &fromPlayer && msg.m_HostToken <= m_HostToken) 
		{
			if (m_LastHost->IsLocal())
			{
				if (m_PendingHost && m_PendingHost->IsLocal())
				{
					LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "The previous host was local. Hosting is being resumed"));
					bRejected = true;
				}
			}
			else 
			{
				LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Waiting for the previous host (%s) to resume hosting", m_LastHost->GetLogName()));
				bRejected = true;
			}
		}

		if (!bRejected && ms_useNoHostFix)
		{
			if (m_PendingHost && m_PendingHost != &fromPlayer)
			{
				if (m_PendingHost->IsLocal())
				{
					LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "We are a pending host (context: %d)", m_PendingHostContext));
				}
				else
				{
					LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("**Rejected**", "Another host (%s) is pending (context: %d)", m_PendingHost->GetLogName(), m_PendingHostContext));
				}

				bRejected = true;
			}
			else 
			{
				SetPendingHost(fromPlayer, PENDING_HOST_VERIFY_REMOTE);
			}
		}
	}

	msgScriptVerifyHostAck verifyHostAck(m_ParentHandler.GetScriptId(), bRejected, m_HostToken, bValidHost);
	scriptInterface::GetScriptManager().SendMessageToPlayer(fromPlayer, verifyHostAck);	
}

void scriptHandlerNetComponent::HandleVerifyHostAckMsg(const msgScriptVerifyHostAck& msg, const ReceivedMessageData &messageData)
{
	const netPlayer& fromPlayer = *messageData.m_FromPlayer;

	gnetAssert(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	gnetAssert(msg.m_ScriptId && *msg.m_ScriptId == m_ParentHandler.GetScriptId());

	ClearWaitingForAck(fromPlayer);

	if (msg.m_Rejected)
	{
		if (msg.m_ValidHost)
		{
			if (!GetHost() || GetHost() != &fromPlayer)
			{	
				// there is already a valid host. This can happen if there was a host migration immediately before the previous host bombed out
				// and we were not informed and consequently tried to host due to having the lowest participant id
				SetNewScriptHost(&fromPlayer, msg.m_HostToken);
			}
		}
		else
		{
			// we have failed to be accepted as the new host because one or more machines are still connected to a host with a higher host token. 
			m_PendingHostingFailed = true;
		}
	}
	else if (msg.m_HostToken > m_HostToken)
	{
		Assert(m_PendingHost);

		// Another machine may have no host but a higher host token. This can happen if a new host managed to send out a NewHost message to only a 
		// few machines before bombing out, and we never got it. We need to make sure our new token will be higher than this so that we are accepted
		// as the new host.
		m_HostToken = msg.m_HostToken+1;
	}
}

bool scriptHandlerNetComponent::CanAddPlayerBot(const netPlayer& botPlayer, bool DEV_ONLY(bAssert))
{
	if (gnetVerifyf(botPlayer.IsBot(), "scriptHandlerNetComponent::CanAddPlayerBot: The player is not a bot") &&
		botPlayer.IsLocal() &&
		m_State == NETSCRIPT_INTERNAL_PLAYING && 
		GetHost() &&
		IsPlayerAParticipant(*netInterface::GetLocalPlayer()) &&
		!IsPlayerAParticipant(botPlayer))
	{
		return true;
	}

#if __DEV
	if (bAssert && !IsPlayerAParticipant(botPlayer))
	{
		if (gnetVerifyf(!bAssert || botPlayer.IsLocal(), "Bot %s could not be added to script %s - the bot is not local", botPlayer.GetLogName(), m_ParentHandler.GetLogName()))
		{
			bAssert = false;
		}
		if (gnetVerifyf(!bAssert || m_State == NETSCRIPT_INTERNAL_PLAYING, "Bot %s could not be added to script %s - the script is not in a playing state", botPlayer.GetLogName(), m_ParentHandler.GetLogName()))
		{
			bAssert = false;
		}
		if (gnetVerifyf(!bAssert || GetHost(), "Bot %s could not be added to script %s - the script has no host", botPlayer.GetLogName(), m_ParentHandler.GetLogName()))
		{
			bAssert = false;
		}
		if (gnetVerifyf(!bAssert || IsPlayerAParticipant(*netInterface::GetLocalPlayer()), "Bot %s could not be added to script %s - the local player is not a participant", botPlayer.GetLogName(), m_ParentHandler.GetLogName()))
		{
			bAssert = false;
		}

		gnetAssertf(!bAssert, "Bot %s could not be added to script %s - reason unspecified", botPlayer.GetLogName(), m_ParentHandler.GetLogName());
	}
#endif

	return false;
}

void scriptHandlerNetComponent::AddPlayerBot(const netPlayer& botPlayer)
{
	if (CanAddPlayerBot(botPlayer))
	{
		// send a join to the script host
        msgScriptBotJoin joinMsg(m_ParentHandler.GetScriptId(), botPlayer.GetTeam());
		scriptInterface::GetScriptManager().SendMessageToPlayer(*GetHost(), joinMsg, &botPlayer);
	}
}

void scriptHandlerNetComponent::RemovePlayerBot(const netPlayer& botPlayer)
{
	if (gnetVerify(botPlayer.IsBot()) &&
		gnetVerify(botPlayer.IsLocal()) &&
		IsPlayerAParticipant(botPlayer) &&
		gnetVerify(m_State == NETSCRIPT_INTERNAL_PLAYING))
	{
		// send a leave to all non-bot players
		unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
        const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

        for(unsigned index = 0; index < numPhysicalPlayers; index++)
        {
            const netPlayer *player = allPhysicalPlayers[index];

			if (!player->IsBot() && IsPlayerAParticipant(*player))
			{
				msgScriptBotLeave leaveMsg(m_ParentHandler.GetScriptId());
				scriptInterface::GetScriptManager().SendMessageToPlayer(*player, leaveMsg, &botPlayer);
			}
		}
	}
}

void scriptHandlerNetComponent::RegisterHostBroadcastData(unsigned* address, unsigned size, bool HasHighFrequencyUpdates BANK_ONLY(, const char* debugArrayName)) 
{
	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (log)
	{
		NetworkLogUtils::WriteLogEvent(*log, "REG_HOST_BROADCAST_DATA", "%s      %d", m_ParentHandler.GetScriptId().GetLogName(), m_NumHostBroadcastData);
	}

#if __ASSERT
	for (int i=0; i<m_NumHostBroadcastData; i++)
	{
		netScriptBroadcastDataHandlerBase* pHostArrayHandler = GetHostBroadcastDataHandler(i);

		if (gnetVerify(pHostArrayHandler))
		{
			if (pHostArrayHandler->GetArray() == address)
			{
				gnetAssertf(0, "%s: This host broadcast data array is already registered", m_ParentHandler.GetLogName());
				return;
			}
		}
	}
#endif

	gnetAssert(dynamic_cast<netArrayManager_Script*>(&netInterface::GetArrayManager()));
 	netHostBroadcastDataHandlerBase* pHostHandler = static_cast<netArrayManager_Script*>(&netInterface::GetArrayManager())->RegisterScriptHostBroadcastData(address, size, m_ParentHandler, m_NumHostBroadcastData++, HasHighFrequencyUpdates BANK_ONLY(, debugArrayName));

	if (pHostHandler)
	{
		u32 handlerSize = pHostHandler->GetNumElementsInUse() * pHostHandler->GetMaxElementSizeInBytes();

		m_sizeOfHostBroadcastData += handlerSize;

		if (log)
		{
			log->WriteDataValue("Size", "%u", handlerSize);
			log->WriteDataValue("Num elements", "%u", pHostHandler->GetNumArrayElements());
			log->WriteDataValue("Num elements in use", "%u", pHostHandler->GetNumElementsInUse());

			if (pHostHandler->RequiresHighFrequencyUpdates())
			{
				log->WriteDataValue("High frequency", "true");
			}
		}

		if (IsHostLocal())
		{
#if ENABLE_NETWORK_LOGGING
			if (log)
			{
				log->WriteDataValue("Locally arbitrated data size", "%u", netInterface::GetLocalPlayer()->GetSizeOfNetArrayData());
			}
#endif // ENABLE_NETWORK_LOGGING
		}
		else if (m_VerifyHostBroadcastData)
		{
			if (log)
			{
				log->WriteDataValue("Verify data (send checksum)", "true");
			}

			pHostHandler->SendChecksumToNewArbitrator();
		}
	}
}

void scriptHandlerNetComponent::RegisterPlayerBroadcastData(unsigned* address, unsigned size, bool HasHighFrequencyUpdates BANK_ONLY(, const char* debugArrayName)) 
{ 
	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (log)
	{
		NetworkLogUtils::WriteLogEvent(*log, "REG_PLAYER_BROADCAST_DATA", "%s      %d", m_ParentHandler.GetScriptId().GetLogName(), m_NumPlayerBroadcastData);
	}

#if __ASSERT
	for (int i=0; i<m_NumPlayerBroadcastData; i++)
	{
		netScriptBroadcastDataHandlerBase* pPlayerArrayHandler = GetPlayerBroadcastDataHandler(i, 0);

		if (gnetVerify(pPlayerArrayHandler))
		{
			if (pPlayerArrayHandler->GetArray() == address)
			{
				gnetAssertf(0, "%s: This player broadcast data array is already registered", m_ParentHandler.GetLogName());
				return;
			}
		}
	}
#endif

	if (log)
	{
		log->WriteDataValue("Size", "%u", size);
	}

	gnetAssert(dynamic_cast<netArrayManager_Script*>(&netInterface::GetArrayManager()));
	static_cast<netArrayManager_Script*>(&netInterface::GetArrayManager())->RegisterScriptPlayerBroadcastData(address, size, m_ParentHandler, m_NumPlayerBroadcastData++, HasHighFrequencyUpdates BANK_ONLY(, debugArrayName));
}


void scriptHandlerNetComponent::TagForDebugPlayerBroadcastData(bool bTag)
{
	for (int i=0; i<m_NumPlayerBroadcastData; i++)
	{
		for (int slot = 0; slot < m_MaxNumParticipants; slot++)
		{
			netPlayerBroadcastDataHandlerBase* playerHandler = GetPlayerBroadcastDataHandler(i, slot);

			if (gnetVerify(playerHandler))
			{
				if(bTag)
				{
					if (!playerHandler->GetBDBackupBuffer())
					{
						playerHandler->CreateBDBackupBuffer();
					}
#if __ASSERT
					else
					{
						gnetAssertf(0, "%s: Trying to tag player %d, slot %d when broadcast data is already tagged", m_ParentHandler.GetLogName(),i, slot );
					}
#endif
				}
				else
				{
					if (playerHandler->GetBDBackupBuffer())
					{
						playerHandler->RemoveBDBackupBuffer();
					}
#if __ASSERT
					else
					{
						gnetAssertf(0, "%s: Trying to remove tag for player %d, slot %d when broadcast data is not tagged", m_ParentHandler.GetLogName(),i, slot );
					}
#endif
				}
			}
		}
	}
}


netHostBroadcastDataHandlerBase* scriptHandlerNetComponent::GetHostBroadcastDataHandler(unsigned handlerNum)
{
	netHostBroadcastDataHandlerBase* pHostArrayHandler = NULL;

	if (m_NumHostBroadcastData > 0 && gnetVerify(handlerNum < m_NumHostBroadcastData))
	{
		pHostArrayHandler = static_cast<netArrayManager_Script&>(netInterface::GetArrayManager()).GetScriptHostBroadcastDataArrayHandler(m_ParentHandler.GetScriptId(), handlerNum);
	}

	return pHostArrayHandler;
}

netPlayerBroadcastDataHandlerBase* scriptHandlerNetComponent::GetPlayerBroadcastDataHandler(unsigned handlerNum, unsigned participantSlot)
{
	netPlayerBroadcastDataHandlerBase* pPlayerArrayHandler = NULL;

	if (m_NumPlayerBroadcastData > 0 && gnetVerify(handlerNum < m_NumPlayerBroadcastData))
	{
		pPlayerArrayHandler = static_cast<netArrayManager_Script&>(netInterface::GetArrayManager()).GetScriptPlayerBroadcastDataArrayHandler(m_ParentHandler.GetScriptId(), handlerNum, participantSlot);
	}

	return pPlayerArrayHandler;
}

netPlayerBroadcastDataHandlerBase* scriptHandlerNetComponent::GetLocalPlayerBroadcastDataHandler(unsigned handlerNum)
{
	netPlayerBroadcastDataHandlerBase* pPlayerArrayHandler = NULL;

	if (m_NumPlayerBroadcastData > 0 && gnetVerify(handlerNum < m_NumPlayerBroadcastData))
	{
		pPlayerArrayHandler = static_cast<netArrayManager_Script&>(netInterface::GetArrayManager()).GetScriptPlayerBroadcastDataArrayHandler(m_ParentHandler.GetScriptId(), handlerNum, m_MySlotNumber);
	}

	return pPlayerArrayHandler;
}


void scriptHandlerNetComponent::FinishBroadcastingData()
{
	// inform the broadcast data array handlers that the array data that they are using will soon be freed memory  
	for (unsigned i=0; i<m_NumHostBroadcastData; i++)
	{
		netHostBroadcastDataHandlerBase* pHandler = GetHostBroadcastDataHandler(i);

		if (gnetVerify(pHandler))
		{
			pHandler->SetArrayDataInvalid();
		}
	}

	for (unsigned i=0; i<m_NumPlayerBroadcastData; i++)
	{
		for (int slot = 0; slot < m_MaxNumParticipants; slot++)
		{
			netPlayerBroadcastDataHandlerBase* pHandler = GetPlayerBroadcastDataHandler(i, slot);

			if (gnetVerify(pHandler))
			{
				pHandler->SetArrayDataInvalid();
			}
		}
	}
}

#if __BANK
void scriptHandlerNetComponent::AddBandwidthIn(unsigned dataRead)
{
	if (dataRead > m_peakBandwidthIn)
	{
		m_peakBandwidthIn = dataRead;
	}

	m_totalBandwidthIn += dataRead;

	if (AssertVerify(m_bandwidthRecorderId != INVALID_RECORDER_ID))
	{
		scriptInterface::GetScriptManager().AddBandwidthInForScript(m_bandwidthRecorderId, dataRead);
	}
}

void scriptHandlerNetComponent::AddBandwidthOut(unsigned dataWritten)
{
	if (dataWritten > m_peakBandwidthOut)
	{
		m_peakBandwidthOut = dataWritten;
	}

	m_totalBandwidthOut += dataWritten;

	if (AssertVerify(m_bandwidthRecorderId != INVALID_RECORDER_ID))
	{
		scriptInterface::GetScriptManager().AddBandwidthOutForScript(m_bandwidthRecorderId, dataWritten);
	}
}
#endif // __DEV

void scriptHandlerNetComponent::RequestToBeHost()
{
	if (!(GetHost() && GetHost()->IsLocal()))
	{
		netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

		if (log)
		{
			NetworkLogUtils::WriteLogEvent(*log, "HOST_REQUEST", "%s", m_ParentHandler.GetScriptId().GetLogName());
		}

		m_HostRequested = true;
	}
}

#if __BANK
void scriptHandlerNetComponent::ForceResendOfAllBroadcastData()
{
	if (GetHost() && GetHost()->IsLocal())
	{
		for (unsigned i=0; i<m_NumHostBroadcastData; i++)
		{
			netHostBroadcastDataHandlerBase* pHandler = GetHostBroadcastDataHandler(i);

			if (gnetVerify(pHandler))
			{
				pHandler->DirtyAllElements();
			}
		}
	}

	for (unsigned i=0; i<m_NumPlayerBroadcastData; i++)
	{
		netPlayerBroadcastDataHandlerBase* pHandler = GetLocalPlayerBroadcastDataHandler(i);

		if (gnetVerify(pHandler))
		{
			pHandler->DirtyAllElements();
		}
	}
}
#endif // __BANK

void scriptHandlerNetComponent::SetNextState(eInternalState state)
{
	m_State = state;

    switch (m_State)
    {
    case NETSCRIPT_INTERNAL_START:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state START");
	    break;
    case NETSCRIPT_INTERNAL_JOIN:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state JOIN");
	    break;
    case NETSCRIPT_INTERNAL_JOINING:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state JOINING");
	    break;
	case NETSCRIPT_INTERNAL_ACCEPTED:
		scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state ACCEPTED");
		break;
	case NETSCRIPT_INTERNAL_RESTARTING:
		scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state RESTARTING");
		break;
	case NETSCRIPT_INTERNAL_RESTARTED:
		scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state RESTARTED");
		break;
    case NETSCRIPT_INTERNAL_HANDSHAKING:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state HANDSHAKING");
	    break;
    case NETSCRIPT_INTERNAL_READY_TO_PLAY:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state READY_TO_PLAY");
	    break;
    case NETSCRIPT_INTERNAL_PLAYING:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state PLAYING");
	    break;
    case NETSCRIPT_INTERNAL_LEAVE:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state LEAVE");
	    break;
    case NETSCRIPT_INTERNAL_LEAVING:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state LEAVING");
	    break;
    case NETSCRIPT_INTERNAL_TERMINATING:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state TERMINATING");
	    break;
    case NETSCRIPT_INTERNAL_FINISHED:
	    scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Setting state FINISHED");
	    break;
    default:
	    gnetAssert(0);
    }
}

bool scriptHandlerNetComponent::IsPlayerInScope(const netPlayer& /*player*/) const
{
	// all players in scope for now, in future only nearby players may be in scope, depending on the script type
	return true;
}

bool scriptHandlerNetComponent::IsPlayerACandidate(const netPlayer& player) const
{
	ParticipantsList::const_iterator curr = m_Candidates.begin();
	ParticipantsList::const_iterator end = m_Candidates.end();

	for (; curr != end; ++curr)
	{
		const participantData* pCandidate = *curr;

		if (pCandidate->GetPlayer() == &player)
			return true;
	}

	return false;
}

bool scriptHandlerNetComponent::IsPlayerAParticipant(const netPlayer& player) const
{
	bool bIsAParticipant = false;

	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		bIsAParticipant = IsPlayerAParticipant(player.GetPhysicalPlayerIndex());
	}

	return bIsAParticipant;
}

bool scriptHandlerNetComponent::IsPlayerAParticipant(PhysicalPlayerIndex playerIndex) const
{
	bool bIsAParticipant = (m_playerParticipants & (1<<playerIndex)) != 0;

	return bIsAParticipant;
}

void scriptHandlerNetComponent::AddPlayerAsCandidate(const netPlayer& player)
{
	if (!gnetVerifyf(!IsPlayerAParticipant(player), "Player is on both the participant and candidate lists"))
	{
		RemovePlayerAsParticipant(player);
	}

	if (!IsPlayerACandidate(player) && gnetVerify(!player.IsBot())) // bots should never be candidates
	{
		participantData* pNewCandidate = rage_new participantData(player);

		if (gnetVerify(pNewCandidate))
		{
			m_Candidates.push_back(pNewCandidate);
			m_NumCandidates++;

			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Player %s added as a candidate. Num candidates now = %d", player.GetLogName(), m_NumCandidates);
		}
	}
}

void scriptHandlerNetComponent::RemovePlayerAsCandidate(const netPlayer& player)
{
	ParticipantsList::iterator curr = m_Candidates.begin();
	ParticipantsList::iterator end = m_Candidates.end();

	for (; curr != end; ++curr)
	{
		participantData* pCandidate = *curr;

		if (pCandidate->GetPlayer() == &player)
		{
			m_Candidates.erase(pCandidate);
			delete pCandidate;

			gnetAssert(m_NumCandidates > 0);
			m_NumCandidates--;

			if (m_PreferredInitialHost == &player)
			{
				m_PreferredInitialHost = NULL;
			}

			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Player %s removed as a candidate. Num candidates now = %d", player.GetLogName(), m_NumCandidates);

			return;
		}
	}

	gnetAssertf(false, "RemovePlayerAsCandidate: Player is not a candidate!");
}

bool scriptHandlerNetComponent::CanStartJoining()
{
	// if there are any pending players we need to wait for them to become active (they may be running this script). Bots are ignored.
	unsigned                 numPendingPlayers = netInterface::GetPlayerMgr().GetNumPendingPlayers();
    const netPlayer * const *pendingPlayers    = netInterface::GetPlayerMgr().GetPendingPlayers();
    
	for(unsigned index = 0; index < numPendingPlayers; index++)
    {
        const netPlayer *pPlayer = pendingPlayers[index];

		if (!pPlayer->IsBot())
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Can't start joining - waiting on pending players");
			return false;
		}
	}

	// we must also wait for all active players to become physical. The script handler code does not currently handle sessions where we may be
	// connected to active players that are not physical (eg in a roaming session). It assumes all players will be physical. This is work to be
	// done in the future.
	if (netInterface::GetPlayerMgr().GetNumActivePlayers() != netInterface::GetPlayerMgr().GetNumPhysicalPlayers())
	{
		scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Can't start joining - waiting on active players to become physical");
		return false;
	}
	return true;
}

bool scriptHandlerNetComponent::CanStartHosting()
{
	return scriptHandlerNetComponent::CanStartJoining();
}

ScriptSlotNumber scriptHandlerNetComponent::GetNextFreeParticipantSlot(const netPlayer& player)
{
	ScriptSlotNumber slotNum = INVALID_SLOT;

	if (m_MaxNumParticipants == MAX_NUM_SCRIPT_PARTICIPANTS)
	{
		// if the maximum number of participants equals the maximum number of players, then just use the physical player index for the slot number.
		// This helps the scripts run more optimally, as they do not need to keep doing the conversion between player ids and participant ids in this case.
		if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
		{
			slotNum = static_cast<ScriptSlotNumber>(player.GetPhysicalPlayerIndex());

			NETWORK_QUITF(slotNum < m_MaxNumParticipants, "Script max num participants is limited to %d, and a player is joining with a physical index greater than this (%d)", m_MaxNumParticipants, slotNum);
		}
	}
	else
	{
		// Slots are allocated in reverse order to throw up script bugs where script participant slots are confused with player slots 
		// (if both slots are allocated in the same way then players joining a script can have the same participant slot number as their player 
		// index)
		for (ScriptSlotNumber slot = m_MaxNumParticipants-1; slot>=0; slot--)
		{
			if (!m_ParticipantsArray[slot])
			{
				slotNum = slot;
				break;
			}
		}
	}

	gnetAssertf(slotNum != INVALID_SLOT , "scriptHandlerNetComponent::GetNextFreeParticipantSlot: All slots full!");

	return slotNum;
}

bool scriptHandlerNetComponent::TryToAddPlayerAsParticipant(const netPlayer& player, ScriptParticipantId& participantId, ScriptSlotNumber& slotNumber, eRejectionReason& rejection)
{
	bool bAccepted = false;

	netLoggingInterface *log = scriptInterface::GetScriptManager().GetLog();

	participantId = INVALID_PARTICIPANT_ID;
	slotNumber = INVALID_SLOT;
	rejection = REJECTED_INVALID;

	if (gnetVerify(GetHost() && GetHost()->IsLocal()))
	{
		if (IsPlayerAParticipant(player))
		{
			participantData* pParticipantData = GetParticipantsData(player);

			if (gnetVerify(pParticipantData))
			{
				participantId = pParticipantData->GetParticipantId();
				slotNumber = pParticipantData->GetSlotNumber();
				bAccepted = true;
			}
		}
		else
		{
			if (IsPlayerACandidate(player))
			{
				RemovePlayerAsCandidate(player);
			}

			if (!m_AcceptingPlayers)
			{
				log->WriteDataValue("REJECTED", "Not accepting players");
				rejection = REJECTED_NO_JOIN_IN_PROGRESS;
			}
			else if (m_NumParticipants >= m_MaxNumParticipants)
			{
				log->WriteDataValue("REJECTED", "No room for player");
				rejection = REJECTED_NO_ROOM_IN_SCRIPT_SESSION;
			}
			else if (m_NumCandidates > 0)
			{
				// If the host has migrated to us then we may have players on our candidates list. These candidates may potentially become participants
				// as the previous host will have processed their request. We need to wait for an accept or leave message from them. Also, we need to 
				// know the next participation number to use.
				log->WriteDataValue("IGNORED", "Waiting for responses from candidates");
				rejection = REJECTED_CANNOT_PROCESS;
			}
			else
			{
				// find the highest participation id allocated to the existing participants
				ScriptParticipantId highestParticipantId = 0;

				unsigned teamParticipants = 0;

				for (ScriptSlotNumber slot = 0; slot < m_MaxNumParticipants; slot++)
				{
					participantData* pParticipant = m_ParticipantsArray[slot];

					if (pParticipant)
					{
						if (pParticipant->GetParticipantId() > highestParticipantId)
						{
							highestParticipantId = pParticipant->GetParticipantId(); 
						}

						if (player.GetTeam() != INVALID_TEAM && pParticipant->GetPlayer()->GetTeam() == player.GetTeam())
						{
							teamParticipants++;
						}
					}
				}

				// determine if there is space for this participant, and it is not reserved for another team
				u32 otherTeamReservations = 0;

				for (int i=0; i<MAX_NUM_TEAMS; i++)
				{
					if (i != player.GetTeam())
					{
						otherTeamReservations += m_TeamReservations[i];
					}
				}

				if (m_NumParticipants + otherTeamReservations >= m_MaxNumParticipants)
				{
					gnetAssert(player.GetTeam() == INVALID_TEAM || teamParticipants >= m_TeamReservations[player.GetTeam()]);

					log->WriteDataValue("REJECTED", "No room for player, remaining slots reserved for other teams");
					rejection = REJECTED_NO_ROOM_IN_TEAM;
				}
				else
				{
					highestParticipantId++;

					// find a free slot for the player
					ScriptSlotNumber freeSlot = GetNextFreeParticipantSlot(player);

					// player is accepted into the session
					if (gnetVerify(freeSlot != INVALID_SLOT) && AddPlayerAsParticipant(player, highestParticipantId, freeSlot))
					{
						participantId = highestParticipantId;
						slotNumber = freeSlot;
						bAccepted = true;
					}
					else
					{
						gnetAssertf(0, "Host failed to add player as a participant");
					}
				}
			}
		}
	}

	gnetAssert(bAccepted || rejection != REJECTED_INVALID);

	if (bAccepted && IsHostLocal())
	{
		// reset time as host so we don't try and migrate the host immediately after a player joins 
		if (m_HostMigrationTimer < TIME_HOSTING_BEFORE_FIRST_HOST_MIGRATION)
		{
			m_HostMigrationTimer = TIME_HOSTING_BEFORE_FIRST_HOST_MIGRATION;
		}

	}

	return bAccepted;
}


bool scriptHandlerNetComponent::AddPlayerAsParticipant(const netPlayer& player, ScriptParticipantId participantId, ScriptSlotNumber slotNumber)
{
	bool bFailed = false;

	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (log)
	{
		NetworkLogUtils::WriteLogEvent(*log, "ADDING_PARTICIPANT", "%s      %s", m_ParentHandler.GetScriptId().GetLogName(), player.GetLogName());
	}

	if (!gnetVerifyf(slotNumber >= 0 && slotNumber < m_MaxNumParticipants, "scriptHandlerNetComponent::AddPlayerAsParticipant: invalid slot"))
	{
		bFailed = true;
	}
	else
	{
		if (IsPlayerACandidate(player))
		{
			RemovePlayerAsCandidate(player);
		}

		if (!IsPlayerAParticipant(player))
		{
			// if there is another participant in this slot with a lower participant id, then dump the participant 
			participantData* pData = GetParticipantDataForSlot(slotNumber);

			if (pData && gnetVerify(pData->GetPlayer()))
			{
				if (pData->GetParticipantId() < participantId)
				{
					scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Conflict for slot %d, removing previous participant", slotNumber);
					RemovePlayerAsParticipant(*pData->GetPlayer());
				}
				else
				{
					scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Failed to add player %s as a participant. Player %s is using this slot", player.GetLogName(), pData->GetPlayer()->GetLogName());

					gnetAssertf(!player.IsMyPlayer(), "Failed to add our local player as a participant");

					bFailed = true;
				}
			}

			if (!bFailed)
			{
				participantData* pNewParticipant = rage_new participantData(player, participantId, slotNumber);

				if (gnetVerify(pNewParticipant))
				{
					Assert(!m_ParticipantsArray[slotNumber]);

					m_ParticipantsArray[slotNumber] = pNewParticipant;
					m_NumParticipants++;

					if (gnetVerify(!(m_activeParticipants & (1<<slotNumber))))
					{
						m_activeParticipants |= (1<<slotNumber);
					}
					
					if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
					{
						m_playerParticipants |= (1<<player.GetPhysicalPlayerIndex());
					}

					if (log)
					{
						log->WriteDataValue("Participant id", "%u", participantId);
						log->WriteDataValue("Slot number", "%u", slotNumber);
						log->WriteDataValue("Num participants now", "%u", m_NumParticipants);
					}
	
					InformBroadcastDataArraysOfNewParticipant(*pNewParticipant);

					if (player.IsMyPlayer())
					{
						m_MySlotNumber = slotNumber;


#if ENABLE_NETWORK_LOGGING
						if (log)
						{
							log->WriteDataValue("Locally arbitrated data size", "%u", netInterface::GetLocalPlayer()->GetSizeOfNetArrayData());
						}
#endif // ENABLE_NETWORK_LOGGING

					}

					if (!player.IsLocal() && !player.IsBot())
					{
						for (ScriptSlotNumber slot =0; slot<m_MaxNumParticipants; slot++)
						{
							const participantData* pParticipant = m_ParticipantsArray[slot];

							if (pParticipant)
							{
								if (gnetVerify(pParticipant->GetPlayer()) && 
									pParticipant->GetPlayer()->IsBot() && 
									pParticipant->GetPlayer()->IsLocal())
								{
									msgScriptBotHandshake handshake(m_ParentHandler.GetScriptId(), pParticipant->GetParticipantId(), pParticipant->GetSlotNumber());
									scriptInterface::GetScriptManager().SendMessageToPlayer(player, handshake, pParticipant->GetPlayer());
								}
							}
						}
					}
				}
			}
		}
		else
		{
#if __DEV
			participantData* pData = GetParticipantsData(player);

			if (gnetVerify(pData))
			{
				gnetAssert(pData->GetParticipantId() == participantId);
				gnetAssert(pData->GetSlotNumber() == slotNumber);
			}
#endif
		}
	}

	return !bFailed;
}

void scriptHandlerNetComponent::RemovePlayerAsParticipant(const netPlayer& player)
{
	participantData* pParticipant = GetParticipantsData(player);

	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (log)
	{
		NetworkLogUtils::WriteLogEvent(*log, "REMOVING_PARTICIPANT", "%s      %s", m_ParentHandler.GetScriptId().GetLogName(), player.GetLogName());
	}

	bool wasHost = (GetHost() == &player);

	if (gnetVerifyf(pParticipant, "RemovePlayerAsParticipant: Player is not a participant!"))
	{
		InformBroadcastDataArraysOfLeavingParticipant(*pParticipant);

		if (gnetVerify(m_activeParticipants & (1<<pParticipant->GetSlotNumber())))
		{
			m_activeParticipants &= ~(1<<pParticipant->GetSlotNumber());
		}

		if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
		{
			m_playerParticipants &= ~(1<<player.GetPhysicalPlayerIndex());
		}

		m_ParticipantsArray[pParticipant->GetSlotNumber()] = NULL;
		delete pParticipant;
		pParticipant = NULL;

		gnetAssert(m_NumParticipants > 0);
		m_NumParticipants--;

		if (log)
		{
			log->WriteDataValue("Num participants now", "%u", m_NumParticipants);
		}

		if (wasHost)
		{
			if (m_NumHostBroadcastData == 0)
			{
				// if the host leaves during initialisation, make sure we verify the broadcast data once it is registered as the new host will assume we are up to date
				scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Flag checksum verification of host variables array handler");
				m_VerifyHostBroadcastData = true;
			}

			m_HostData = NULL;
		}

		// if this is a real player, also remove all the bots local to that player's machine
		if (!player.IsBot())
		{
			for (ScriptSlotNumber slot =0; slot<m_MaxNumParticipants; slot++)
			{
				const participantData* pBotParticipant = m_ParticipantsArray[slot];

				if (pBotParticipant &&
					gnetVerify(pBotParticipant->GetPlayer()) && 
					pBotParticipant->GetPlayer()->IsBot() && 
					pBotParticipant->GetPlayer()->GetConnectionId() == player.GetConnectionId())
				{
					RemovePlayerAsParticipant(*pBotParticipant->GetPlayer());
				}
			}
		}

		if (m_LastHost == &player)
		{
			m_LastHost = NULL;
		}

		if (m_PendingHost == &player && ms_useNoHostFix)
		{
			if (m_State < NETSCRIPT_INTERNAL_ACCEPTED)
			{
				// clear any accepted or rejected state from this host. Once the joining process finishes it will be restarted 
				// if there are still other participants
				m_Accepted = false;
				m_RejectionReason = REJECTED_INVALID;
			}

			if (m_PendingHostContext == PENDING_HOST_MIGRATION)
			{
				HostMigrationFailed(true);
			}
			else
			{
				ClearPendingHost();
			}
		}

		if (!GetHost() && !player.IsMyPlayer())
		{
			if (m_State == NETSCRIPT_INTERNAL_PLAYING && !m_Terminated) // check the terminated flag too here as we don't want to start hosting if the thread has gone and we haven't entered a leaving state yet. 
			{
				if (log)
				{
					log->WriteDataValue("Last host", "%s", m_LastHost ? m_LastHost->GetLogName() : "-none-");
				}

				if (!m_PendingHost)
				{
					if (m_NumParticipants == 1)
					{
						scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "We become host, as we are the only participant left");

						// there is only us left in the script session, so we become host
						SetNewScriptHost(netInterface::GetLocalPlayer(), m_HostToken+1);
					}
					else if (m_LastHost)
					{
						if (m_LastHost->IsLocal())
						{
							// we were the last host, so we resume hosting
							VerifyLocalHost(); 
						}
						// otherwise, wait for the last host to verify hosting
					}
					else
					{
						const netPlayer* pNextHost = DetermineHostFromParticipants();

						if (pNextHost && pNextHost->IsLocal())
						{
							// we may already be in the process of trying to host
							if (m_PendingHost)
							{
								Assert(m_PendingHost->IsLocal());
							}
							else 
							{
								// we are assuming we will be the next host due to the previous host dropping out. In this case we need to verify that
								// we can host (there may be another valid host)
								VerifyLocalHost();
							}
						}
					}
				}
			}
		}
	}

	if (m_PendingHost == &player && !ms_useNoHostFix)
	{
		if (m_State < NETSCRIPT_INTERNAL_ACCEPTED)
		{
			// clear any accepted or rejected state from this host. Once the joining process finishes it will be restarted 
			// if there are still other participants
			m_Accepted = false;
			m_RejectionReason = REJECTED_INVALID;
		}

		HostMigrationFailed(true);
	}
}

void scriptHandlerNetComponent::RemoveAllCandidates(bool bLog)
{
	if (m_NumCandidates > 0)
	{
		if (bLog)
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Removing all candidates");
		}

		ParticipantsList::iterator curr = m_Candidates.begin();
		
		while(curr != m_Candidates.end())
		{
			participantData* pCandidate = *curr;
			curr = m_Candidates.erase(curr);
			delete pCandidate;
		}

		m_NumCandidates = 0;
	}
}

void scriptHandlerNetComponent::RemoveAllParticipants(bool bLog)
{
	if (m_NumParticipants > 0)
	{
		if (bLog)
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Removing all participants");
		}

		for (ScriptSlotNumber slot =0; slot<m_MaxNumParticipants; slot++)
		{
			const participantData* pParticipant = m_ParticipantsArray[slot];

			if (pParticipant)
			{
				m_ParticipantsArray[slot] = NULL;
				delete pParticipant;
			}
		}

		m_NumParticipants = 0;
		m_playerParticipants = 0;
		m_activeParticipants = 0;

		m_MySlotNumber = INVALID_SLOT;
	}
}

scriptHandlerNetComponent::participantData* scriptHandlerNetComponent::GetParticipantsData(const netPlayer& player)
{
	for (ScriptSlotNumber slot = m_MaxNumParticipants-1; slot>=0; slot--)
	{
		participantData* pParticipant = m_ParticipantsArray[slot];

		if (pParticipant && pParticipant->GetPlayer() == &player)
		{
			return pParticipant;
		}
	}

	gnetAssertf(false, "GetParticipantsData: Player is not a participant!");

	return NULL;
}

const scriptHandlerNetComponent::participantData* scriptHandlerNetComponent::GetParticipantsData(const netPlayer& player) const
{
	return const_cast<scriptHandlerNetComponent*>(this)->GetParticipantsData(const_cast<netPlayer&>(player));
}

scriptHandlerNetComponent::participantData* scriptHandlerNetComponent::GetParticipantDataForSlot(int slotNumber)
{
	participantData* pData = NULL;

	if (gnetVerifyf(slotNumber >=0 && slotNumber < m_MaxNumParticipants, "GetParticipantDataForSlot: Invalid participant slot %d", slotNumber))
	{
		pData = m_ParticipantsArray[slotNumber];
	}

	return pData;
}

const scriptHandlerNetComponent::participantData* scriptHandlerNetComponent::GetParticipantDataForSlot(int slotNumber) const
{
	return const_cast<scriptHandlerNetComponent*>(this)->GetParticipantDataForSlot(slotNumber);
}

void scriptHandlerNetComponent::RestartJoiningProcess()
{
	// flush lists
	RemoveAllCandidates();
	RemoveAllParticipants();

	SetNewScriptHost(NULL, 0);

	SetNextState(NETSCRIPT_INTERNAL_START);
}

bool scriptHandlerNetComponent::IsWaitingForAck(const netPlayer& player) const	
{ 
	gnetAssert(player.GetPhysicalPlayerIndex() < (sizeof(m_WaitingForAcks)<<3));
	return ((m_WaitingForAcks & (1<<player.GetPhysicalPlayerIndex())) != 0); 
}

void scriptHandlerNetComponent::SetWaitingForAck(const netPlayer& player)			
{ 
	gnetAssert(!player.IsBot());
	gnetAssert(player.GetPhysicalPlayerIndex() < (sizeof(m_WaitingForAcks)<<3));
	m_WaitingForAcks |= (1<<player.GetPhysicalPlayerIndex()); 
}

void scriptHandlerNetComponent::ClearWaitingForAck(const netPlayer& player)		
{ 
	gnetAssert(player.GetPhysicalPlayerIndex() < (sizeof(m_WaitingForAcks)<<3));
	m_WaitingForAcks &= ~(1<<player.GetPhysicalPlayerIndex()); 
}

bool scriptHandlerNetComponent::DoLeaveCleanup()
{
	bool bSynced = true;

	// we must wait until all other participants have the current state of the host & player broadcast data before terminating 
	if (GetHost() && GetHost()->IsLocal())
	{
		for (unsigned i=0; i<m_NumHostBroadcastData; i++)
		{
			netHostBroadcastDataHandlerBase* pHandler = GetHostBroadcastDataHandler(i);

			if (gnetVerify(pHandler) && gnetVerify(pHandler->IsArrayLocallyArbitrated()) && !pHandler->IsSyncedWithAllPlayers())
			{
#if ENABLE_NETWORK_LOGGING
				PlayerFlags unsyncedPlayers = pHandler->GetUnsyncedPlayers();
				Assert(unsyncedPlayers != 0);

				for (u32 j=0; j<m_MaxNumParticipants; j++)
				{
					if ((unsyncedPlayers & (1<<j)) != 0)
					{
						participantData* pParticipantData = m_ParticipantsArray[j];

						if (Verifyf(pParticipantData, "Host broadcast handler %d for %s is flagged as unsynced for participant slot %d, which has no associated participant", i, m_ParentHandler.GetLogName(), j))
						{
							const netPlayer* pPlayer = pParticipantData->GetPlayer();

							if (Verifyf(pPlayer, "Host broadcast handler %d for %s is flagged as unsynced for participant slot %d, which has no associated player", i, m_ParentHandler.GetLogName(), j))
							{
								scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot terminate: waiting for host broadcast data to sync on %s", pPlayer->GetLogName());
							}
							else
							{
								scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot terminate: waiting for host broadcast data to sync on %s (*** NO PLAYER ***)", pPlayer->GetLogName());
							}
						}
						else
						{
							scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot terminate: waiting for host broadcast data to sync for particpant %d (*** NOT A PARTICIPANT!! ***)", j);
						}
					}
				}

				bSynced = false;
#else
				return false;
#endif // ENABLE_NETWORK_LOGGING			
			}
		}
	}

	for (unsigned i=0; i<m_NumPlayerBroadcastData; i++)
	{
		netPlayerBroadcastDataHandlerBase* pHandler = GetLocalPlayerBroadcastDataHandler(i);

		if (gnetVerify(pHandler) && gnetVerify(pHandler->IsArrayLocallyArbitrated()) && !pHandler->IsSyncedWithAllPlayers())
		{
#if ENABLE_NETWORK_LOGGING
			PlayerFlags unsyncedPlayers = pHandler->GetUnsyncedPlayers();
			Assert(unsyncedPlayers != 0);

			for (u32 j=0; j<m_MaxNumParticipants; j++)
			{
				if ((unsyncedPlayers & (1<<j)) != 0)
				{
					participantData* pParticipantData = m_ParticipantsArray[j];

					if (Verifyf(pParticipantData, "Player broadcast handler %d for %s is flagged as unsynced for participant slot %d, which has no associated participant", i, m_ParentHandler.GetLogName(), j))
					{
						const netPlayer* pPlayer = pParticipantData->GetPlayer();

						if (Verifyf(pPlayer, "Player broadcast handler %d for %s is flagged as unsynced for participant slot %d, which has no associated player", i, m_ParentHandler.GetLogName(), j))
						{
							scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot terminate: waiting for local player broadcast data to sync on %s", pPlayer->GetLogName());
						}
						else
						{
							scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot terminate: waiting for player broadcast data to sync on %s (*** NO PLAYER ***)", pPlayer->GetLogName());
						}
					}
					else
					{
						scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Cannot terminate: waiting for player broadcast data to sync on participant %d (*** NOT A PARTICIPANT!! ***)",j);
					}
				}
			}
			bSynced = false;
#else
			return false;
#endif // ENABLE_NETWORK_LOGGING
		}
	}

	return bSynced;
}

bool scriptHandlerNetComponent::DoTerminationCleanup() 
{ 
	// wait for the script thread to terminate
	return m_Terminated; 
}

void scriptHandlerNetComponent::InformBroadcastDataArraysOfNewParticipant(const participantData& participant)
{
	const netPlayer* pNewPlayer = participant.GetPlayer();

	if (gnetVerify(pNewPlayer))
	{
		if (m_NumHostBroadcastData > 0)
		{
			for (int i=0; i<m_NumHostBroadcastData; i++)
			{
				netScriptBroadcastDataHandlerBase *hostHandler = GetHostBroadcastDataHandler(i);

				if (gnetVerify(hostHandler))
				{	
					hostHandler->PlayerHasJoined(*pNewPlayer);
				}
			}
		}

		if (m_NumPlayerBroadcastData > 0)
		{
			for (int i=0; i<m_NumPlayerBroadcastData; i++)
			{
				for (int slot = 0; slot < m_MaxNumParticipants; slot++)
				{
					netPlayerBroadcastDataHandlerBase* playerHandler = GetPlayerBroadcastDataHandler(i, slot);

					if (gnetVerify(playerHandler))
					{
						// inform the handler about the joining player
						playerHandler->PlayerHasJoined(*pNewPlayer);

						// set up the handler for the new participant 
						if (slot == participant.GetSlotNumber())
						{
							gnetAssert(!playerHandler->GetPlayerArbitrator());

							// a local bot needs to swap out a remote broadcast data handler for a local one, because it will need to send updates to other players
							if (pNewPlayer->IsLocal() && gnetVerify(pNewPlayer->IsBot()) && !playerHandler->IsLocal())
							{
								netArrayManager_Script* pArrayMgr = static_cast<netArrayManager_Script*>(&netInterface::GetArrayManager());

								pArrayMgr->SwapLocalAndRemotePlayerBroadcastDataHandlers(*playerHandler);

								scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Player array handler %d allocated for bot", i);
							}
							else
							{
								playerHandler->SetPlayerArbitrator(participant.GetPlayer());

								scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Player array handler %d allocated", i);
							}

						}
					}
				}
			}
		}
		else
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "No player array handler to allocate");
		}
	}
}

void scriptHandlerNetComponent::InformBroadcastDataArraysOfLeavingParticipant(const participantData& participant)
{
	const netPlayer* pLeavingPlayer = participant.GetPlayer();

	if (gnetVerify(pLeavingPlayer))
	{
		if (m_NumHostBroadcastData > 0)
		{
			for (int i=0; i<m_NumHostBroadcastData; i++)
			{
				netScriptBroadcastDataHandlerBase *hostHandler = GetHostBroadcastDataHandler(i);

				if (gnetVerify(hostHandler))
				{	
					hostHandler->PlayerHasLeft(*pLeavingPlayer);
				}
			}
		}

		if (m_NumPlayerBroadcastData > 0)
		{
			for (int i=0; i<m_NumPlayerBroadcastData; i++)
			{
				for (int slot = 0; slot < m_MaxNumParticipants; slot++)
				{
					netPlayerBroadcastDataHandlerBase* playerHandler = GetPlayerBroadcastDataHandler(i, slot);

					if (gnetVerify(playerHandler))
					{
						// inform the handler about the leaving player
						playerHandler->PlayerHasLeft(*pLeavingPlayer);

						if (slot == participant.GetSlotNumber())
						{
							scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Player array handler deallocated", i);

							// if a local bot leaves we can swap out his local broadcast data handler for a remote one
							if (pLeavingPlayer->IsLocal() && gnetVerify(pLeavingPlayer->IsBot()) && gnetVerify(playerHandler->IsLocal()))
							{
								netArrayManager_Script* pArrayMgr = static_cast<netArrayManager_Script*>(&netInterface::GetArrayManager());

								pArrayMgr->SwapLocalAndRemotePlayerBroadcastDataHandlers(*playerHandler);

								scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Player array handler %d deallocated for bot", i);
							}
						}
					}
				}
			}
		}
		else
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "No player array handler to deallocate");
		}
	}
}

const netPlayer* scriptHandlerNetComponent::DetermineHostFromParticipants() const
{
	const participantData* pNextHost = NULL;

	// the host is always the participant with the lowest participant id. 
	for (ScriptSlotNumber slot =0; slot<m_MaxNumParticipants; slot++)
	{
		const participantData* pParticipant = m_ParticipantsArray[slot];

		if (pParticipant && !(pParticipant->GetPlayer() && pParticipant->GetPlayer()->IsBot()))
		{
			if ((pParticipant != m_HostData) && (!pNextHost || (pParticipant->GetParticipantId() < pNextHost->GetParticipantId())))
			{
				pNextHost = pParticipant;
			}
		}
	}

	scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Determine host from remaining participants: %s", (pNextHost && pNextHost->GetPlayer()) ? pNextHost->GetPlayer()->GetLogName() : "-none");

	if (gnetVerifyf(pNextHost, "scriptHandlerNetComponent::DetermineHostFromParticipants() - failed to determine the next host"))
	{
		return pNextHost->GetPlayer();
	}

	return NULL;
}

const netPlayer* scriptHandlerNetComponent::DetermineHostFromCandidates() const
{
	const netPlayer* pHost = NULL;

	ParticipantsList::const_iterator curr = m_Candidates.begin();
	ParticipantsList::const_iterator end = m_Candidates.end();

	for (; curr != end; ++curr)
	{
		const participantData* pCandidate = *curr;

		if (!pHost || pCandidate->GetPlayer()->GetRlGamerId() < pHost->GetRlGamerId())
		{
			pHost = pCandidate->GetPlayer();
		}
	}

	return pHost;
}

void scriptHandlerNetComponent::SetNewScriptHost(const netPlayer* pPlayer, HostToken hostToken, bool bBroadcast)
{
	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (log)
	{
		NetworkLogUtils::WriteLogEvent(*log, "SETTING_NEW_HOST", "%s      %s", m_ParentHandler.GetScriptId().GetLogName(), pPlayer ? pPlayer->GetLogName() : "-none-");
	}

	// bots should never become the host
	gnetAssert(!pPlayer || !pPlayer->IsBot());

	// we can't become host while we are leaving, as this can cause problems with the broadcast data. See bug fix comment:
	// "An array size in the host broadcast data was screwed from a bad update from the host of the script. The host had become host while the 
	// script was in a leaving state and the host broadcast data was flagged as invalid, due to it being freed as the thread terminated. 
	// InitialiseSyncDataBuffers was called from HandleChangeOfArbitration(), which grabbed the current state of the array from the freed memory. 
	// Now the host is just set to NULL when it migrates locally while the script is in a leaving state."
	if (!gnetVerify(!pPlayer || !pPlayer->IsMyPlayer() || m_State < NETSCRIPT_INTERNAL_LEAVING))
	{
		pPlayer = NULL;
	}

	// the host token should always increase with each new host
	if (pPlayer && !pPlayer->IsLocal() && m_State > NETSCRIPT_INTERNAL_ACCEPTED)
	{
		Assert(hostToken > m_HostToken);
	}

	if (pPlayer)
	{
		ClearPendingHost();
	}

	if (GetHost())
	{
		m_LastHost = GetHost();
	}

	if (log)
	{
		log->WriteDataValue("Token", "%d", hostToken);
		log->WriteDataValue("Last host", "%s", m_LastHost ? m_LastHost->GetLogName() : "-none-");
	}

	if (pPlayer && pPlayer->IsMyPlayer())
	{
		// we can never host without a thread. This can cause some weird broadcast data issues as the data associated with the array handlers has gone,
		// and they may try to send an update using stale data.
		if (!gnetVerifyf(!m_Terminated, "Trying to host %s when the thread has gone", m_ParentHandler.GetLogName()))
		{
			return;
		}

		m_Accepted = true;
		m_HostRequested = false;

		if (IsPlayerACandidate(*pPlayer))
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "We are host, make us a participant");
			gnetAssert(m_NumParticipants==0);
			AddPlayerAsParticipant(*pPlayer, 0, GetNextFreeParticipantSlot(*pPlayer));
		}

		// no need to handshake if we are the host (the other participants will be restarting the joining process)   
		if (m_State == NETSCRIPT_INTERNAL_ACCEPTED)
			SetNextState(NETSCRIPT_INTERNAL_PLAYING);

		m_HostMigrationTimer = TIME_HOSTING_BEFORE_FIRST_HOST_MIGRATION;

		if (bBroadcast)
		{
			// inform all the other players that we are the new host. 
			msgScriptNewHost msg(m_ParentHandler.GetScriptId(), hostToken);
			scriptInterface::GetScriptManager().SendMessageToAllParticipants(*this, msg);
		}
	}
	else
	{
		m_HostMigrationTimer = 0;
	}

	if (pPlayer)
	{
		m_HostData = GetParticipantsData(*pPlayer);
		m_HostToken = hostToken; 
	}
	else
	{
		m_HostData = 0;
	}

	if (m_NumHostBroadcastData == 0)
	{
		scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "No host variables array handler to change ownership to new host");

		// if the host changes during initialisation, make sure we verify the broadcast data once it is registered as the new host will assume we are up to date
		if (m_LastHost)
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Flag checksum verification of host variables array handler");
			m_VerifyHostBroadcastData = true;
		}
	}
	else
	{
		ScriptObjectId nextFreeObjectId = 0;

		u32 sizeOfArrayData = 0;

		for (int i=0; i<m_NumHostBroadcastData; i++)
		{
			netHostBroadcastDataHandlerBase* pHostArrayHandler = GetHostBroadcastDataHandler(i);

			if (gnetVerify(pHostArrayHandler))
			{
				scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Host variables array handler %d ownership set to new host", i);

				pHostArrayHandler->SetPlayerArbitrator(GetHost());

				// retrieve the number of objects created by the previous host, so that we do not create objects with duplicate ids. Also, if the previous
				// host has created some objects but we never got the corresponding host broadcast data update, these will be removed when we create them again
				if (pHostArrayHandler->GetNextFreeHostObjectId() > nextFreeObjectId)
					nextFreeObjectId = pHostArrayHandler->GetNextFreeHostObjectId();

				sizeOfArrayData += pHostArrayHandler->GetNumElementsInUse() * pHostArrayHandler->GetMaxElementSizeInBytes();
			}
		}

		if (IsHostLocal() || (m_LastHost && m_LastHost->IsLocal()))
		{
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), m_ParentHandler.GetScriptId(), "Size of host array data: %u. Locally arbitrated data now = %u", sizeOfArrayData, netInterface::GetLocalPlayer()->GetSizeOfNetArrayData());
		}

		if (nextFreeObjectId != 0)
		{
			m_ParentHandler.SetNextFreeHostObjectId(nextFreeObjectId);
		}
	}
}

void scriptHandlerNetComponent::VerifyLocalHost() 
{
	Assert(m_PendingHost == 0);

	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	if (log)
	{
		NetworkLogUtils::WriteLogEvent(*log, "VERIFYING_NEW_LOCAL_HOST", "%s", m_ParentHandler.GetScriptId().GetLogName());
	}

	SetPendingHost(*netInterface::GetLocalPlayer(), PENDING_HOST_VERIFY_LOCAL);
	m_PendingHostingFailed	= false;
	m_HostMigrationTimer	= TIME_BETWEEN_HOST_VERIFY_ATTEMPTS;

	// Send a verify message to all participants. If they all accept we will become the host.
	msgScriptVerifyHost msg(m_ParentHandler.GetScriptId(), m_HostToken);
	scriptInterface::GetScriptManager().SendMessageToAllParticipants(*this, msg, true);
}

void scriptHandlerNetComponent::HandleHostMigration()
{
	if (m_HostMigrationTimer > 0)
	{
		m_HostMigrationTimer -= fwTimer::GetTimeStepInMilliseconds();

		if (m_HostMigrationTimer < 0)
		{
			m_HostMigrationTimer = 0;
		}
	}

	// check to see if a local host migration has succeeded
	if (m_PendingHost && m_PendingHost->IsLocal())
	{
		Assert(!GetHost());

		if (m_WaitingForAcks == 0)
		{
			if (m_PendingHostingFailed)
			{
				if (m_HostMigrationTimer == 0)
				{
					netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

					if (log)
					{
						NetworkLogUtils::WriteLogEvent(*log, "VERIFYING_NEW_LOCAL_HOST_FAILED", "%s", m_ParentHandler.GetScriptId().GetLogName());
					}

					// Our hosting was rejected but we never found the real host, who is still connected to some other participants. This host
					// will be in the process of leaving, so we need to keep trying until we find the host or are accepted as the new host.
					ClearPendingHost();
					VerifyLocalHost();
				}
			}
			else if (!m_Terminated)
			{
				// we have successfully become the new host
				SetNewScriptHost(m_PendingHost, m_HostToken+1);
			}
		}
	}
	else if (m_HostRequested && !IsHostLocal() && GetHost())
	{
		if (m_HostMigrationTimer == 0)
		{
			// send a request every 250ms until we are hosting
			msgScriptHostRequest msg(m_ParentHandler.GetScriptId());
			scriptInterface::GetScriptManager().SendMessageToPlayer(*GetHost(), msg);
			m_HostMigrationTimer = TIME_BETWEEN_HOST_REQUESTS;
		}
	}
}

void scriptHandlerNetComponent::HostMigrationFailed(bool bPlayerLeft)
{
	netLoggingInterface* log = scriptInterface::GetScriptManager().GetLog();

	// m_PendingHostContext can be NONE, if a new host was set after the migrate host message was sent (because the pending host machine left)
	gnetAssert(m_PendingHostContext == PENDING_HOST_MIGRATION || m_PendingHostContext == PENDING_HOST_NONE);

	if (m_PendingHostContext == PENDING_HOST_MIGRATION)
	{
		// make ourselves the host again
		if (m_State == NETSCRIPT_INTERNAL_PLAYING && !m_Terminated)
		{
			if (log)
			{
				NetworkLogUtils::WriteLogEvent(*log, "HOST_MIGRATION_FAILED", "%s", m_ParentHandler.GetScriptId().GetLogName());
			}

			// we may be hosting again if the player left before we got his response to the host migration 
			if (!GetHost())
			{
				// set the host token to one more than the token that this player would have used. This is because the player may have received the 
				// migration message and sent out a new host message to some machines. One of them may try and host when they detect he has left. Also, 
				// we need to broadcast the fact that we are hosting again to the other machines that may have received his message.
				SetNewScriptHost(netInterface::GetLocalPlayer(), bPlayerLeft ? m_HostToken+2 : m_HostToken, bPlayerLeft);
			}
			else
			{
				Assert(GetHost()->IsLocal());
			}

			// prevent migration for a while
			if (m_HostMigrationTimer < TIME_BETWEEN_HOST_MIGRATION_ATTEMPTS)
			{
				m_HostMigrationTimer = TIME_BETWEEN_HOST_MIGRATION_ATTEMPTS;
			}
		}
		else if (m_PendingHostContext == PENDING_HOST_MIGRATION || !ms_useNoHostFix)
		{
			ClearPendingHost();
		}
	}
}

void scriptHandlerNetComponent::SetPendingHost(const netPlayer& pendingHost, ePendingHostContext context)
{
	gnetAssertf(!m_PendingHost || m_PendingHost==&pendingHost, "Setting pending host %s for script %s when we already have a pending host (%s)", pendingHost.GetLogName(), m_ParentHandler.GetLogName(), m_PendingHost->GetLogName());
	gnetAssertf(context != PENDING_HOST_NONE, "Setting pending host %s for script %s with no context", pendingHost.GetLogName(), m_ParentHandler.GetLogName());

	m_PendingHost = &pendingHost; 
	m_PendingHostContext = (u8)context; 
}

void scriptHandlerNetComponent::ClearPendingHost() 
{ 
	m_PendingHost = NULL; 
	m_PendingHostContext = PENDING_HOST_NONE; 
}

} // namespace rage
