//
// scriptHandlerMgr.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef SCRIPT_MGR_BASE_H
#define SCRIPT_MGR_BASE_H

// framework headers
#include "fwnet/netBandwidthMgr.h"
#include "fwnet/netBandwidthStats.h"
#include "fwnet/netLog.h"
#include "fwnet/netInterface.h"
#include "fwnet/netLogUtils.h"
#include "fwnet/netPlayer.h"
#include "fwnet/netPlayerMgrBase.h"
#include "fwscript/scriptHandler.h"
#include "fwscript/scriptHandlerMessages.h"
#include "fwscript/scriptId.h"
#include "fwnet/netchannel.h"
// rage headers
#include "atl/map.h"

namespace rage
{
class netPlayer;
class netEvent;

typedef atMap<atHashValue, scriptHandler*> scriptHandlerMap;

//PURPOSE
// Manages an array of current active scripts on a player's machine
class scriptHandlerMgr
{
	friend class scriptHandler;
	friend class scriptHandlerNetComponent;

public:

	scriptHandlerMgr(netBandwidthMgr* bandwidthMgr = NULL);
	
	virtual ~scriptHandlerMgr() {}

	virtual void Init();
	virtual void Update();
	virtual void Shutdown();

	virtual void NetworkInit();		// called when a network game starts
	virtual void NetworkUpdate();	// called during a network game
	virtual void NetworkShutdown(); // called when a network game ends

	//PURPOSE
	// Returns a project specific script id for the given thread
	virtual scriptIdBase&		GetScriptId(const scrThread& scriptThread) const = 0;

	//PURPOSE
	// Creates a project-specific script handler to manage the given thread. 
	virtual scriptHandler* CreateScriptHandler(scrThread& scriptThread) = 0;
	
	netLoggingInterface  *GetLog()										{ return m_Log; }
	netLoggingInterface  *GetBandwidthLog()								{ return m_BandwidthLog; }
	netBandwidthMgr      *GetBandwidthMgr()								{ return m_BandwidthMgr; }
	
	bool IsDeletingHandlers() const										{ return m_DeletingHandlers; }

	//PURPOSE
	// Returns the script handler corresponding to the given script id
	virtual scriptHandler* GetScriptHandler(const scriptIdBase& scrId) 
	{
		scriptHandler** handler = m_scriptHandlers.Access(scrId.GetScriptIdHash());
		return handler ? *handler : NULL;
	}

	//PURPOSE
	// Called by the script when it starts up. Allocates a free network script handler to the script.
	virtual void RegisterScript(scrThread& scriptThread);

	//PURPOSE
	// Called by the script when it terminates. Informs its allocated network script handler to start shutting down.
	virtual void UnregisterScript(scrThread& scriptThread);

	//PURPOSE
	// Used to log network script events
    void WriteScriptHeader(netLoggingInterface *log, const scriptIdBase& scriptId, const char *headerText, ...);

	//PURPOSE
	// Called when a player joins the game session
	virtual void PlayerHasJoined(const netPlayer& player);

	//PURPOSE
	// Called when a player leaves the game session
	virtual void PlayerHasLeft(const netPlayer& player);

	// Called when a network game starts
	void RegisterExistingThreads();

	//PURPOSE
	//	Finds a the given script resource and removes it from any handler's list 
	void RemoveScriptResource(ScriptResourceId resourceId);

	//PURPOSE
	//	Finds a the given script resource and removes it from any handler's list 
	void RemoveScriptResource(ScriptResourceType resourceType, ScriptResourceRef resourceRef);

	//PURPOSE
	//	Removes the all the resources on every script handler's resource list with the given type
	void RemoveAllScriptResourcesOfType(ScriptResourceType type);

	//PURPOSE
	//	Returns the resource with the given reference. Searches all the handler resource lists.
	//PARAMS
	// resourceType - the type of the resource
	// resourceRef - the reference num of the resource
	// handlerToIgnore - if set, this handler's resource list is not searched
	scriptResource* GetScriptResource(ScriptResourceType resourceType, ScriptResourceRef resourceRef, scriptHandler* handlerToIgnore = NULL);

	//PURPOSE
	//	Returns the handler that owns the resource with the given reference. Searches all the handler resource lists.
	//PARAMS
	// resourceType - the type of the resource
	// resourceRef - the reference num of the resource
	// handlerToIgnore - if set, this handler's resource list is not searched
	scriptHandler* GetScriptHandlerForResource(ScriptResourceType resourceType, ScriptResourceRef resourceRef, scriptHandler* handlerToIgnore = NULL);

    //PURPOSE
    // Returns the number of entities that the script is potentially going to create
    virtual unsigned GetNumRequiredEntities() const { return 0; }

#if __BANK

    //PURPOSE
    // Returns the number of script manager messages sent since the last call to ResetMessageCounts();
    unsigned GetNumScriptManagerMessagesSent() const { return m_NumScriptManagerMessagesSent; }

    //PURPOSE
    // Reset the counts of script manager messages sent
    void ResetMessageCounts() { m_NumScriptManagerMessagesSent = 0; }

	netBandwidthStatistics& GetBandwidthStatistics() { return m_BandwidthStats; }

	RecorderId AllocateBandwidthRecorderForScript(const char* scriptName);

	void AddBandwidthOutForScript(RecorderId scriptRecorderId, unsigned bandwidthOut);
	void AddBandwidthInForScript(RecorderId scriptRecorderId, unsigned bandwidthIn);

	void UpdateBandwidthLog();

#endif

#if __BANK
	// spews debug info to the TTY on all the script handler objects and resources
	virtual void SpewObjectAndResourceInfo() const;

	// spews debug info to the TTY on all the script handler resources of the given type
	virtual void SpewResourceInfoForType(ScriptResourceType resourceTypeFilter) const;
#endif	//	__BANK

protected:

	//PURPOSE
	// Returns a project-specific script id to be used when reading script manager messages
	virtual scriptIdBase&	  GetMsgScriptId() = 0;

	void DeleteHandlerWithHash(atHashValue hash);

    //PURPOSE
    // Handles an incoming network message
    //PARAMS
    // messageData - Data describing the incoming message
    void OnMessageReceived(const ReceivedMessageData &messageData);

	//PURPOSE
	// Sends a script handler message to the given player
	template<typename T>
	bool SendMessageToPlayer(const netPlayer& player, const T& msg, const netPlayer* fromPlayer = NULL)
	{
		u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
		unsigned size;
		netSequence seq;

		if (gnetVerify(msg.Export(buf, sizeof(buf), &size)))
		{
			if (netInterface::GetPlayerMgr().SendBuffer(&player, buf, size, NET_SEND_RELIABLE, &seq, fromPlayer))
			{
#if __BANK
                if(GetLog())
                {
				    NetworkLogUtils::WriteMessageHeader(*GetLog(), false, seq, player, msg.GetSendStr(), msg.GetScriptName());
				    msg.Log(*GetLog());
                }

				if (msg.GetMsgId() == msgScriptJoin::MSG_ID() || 
					msg.GetMsgId() == msgScriptBotJoin::MSG_ID())
				{
					m_BandwidthStats.AddBandwidthOut(m_JoinMsgRecorderId, msg.GetMessageDataBitSize());
				}
				else if (msg.GetMsgId() == msgScriptHandshake::MSG_ID() || 
						 msg.GetMsgId() == msgScriptBotHandshake::MSG_ID())
				{
					m_BandwidthStats.AddBandwidthOut(m_HandshakeMsgRecorderId, msg.GetMessageDataBitSize());
				}
				else if (msg.GetMsgId() == msgScriptLeave::MSG_ID() || 
						 msg.GetMsgId() == msgScriptBotLeave::MSG_ID())
				{
					m_BandwidthStats.AddBandwidthOut(m_LeaveMsgRecorderId, msg.GetMessageDataBitSize());
				}
				else if (msg.GetMsgId() == msgScriptJoinAck::MSG_ID() || 
						 msg.GetMsgId() == msgScriptJoinHostAck::MSG_ID() || 
						 msg.GetMsgId() == msgScriptBotJoinAck::MSG_ID() || 
						 msg.GetMsgId() == msgScriptHandshakeAck::MSG_ID() || 
						 msg.GetMsgId() == msgScriptBotHandshakeAck::MSG_ID() ||
						 msg.GetMsgId() == msgScriptLeaveAck::MSG_ID() ||
						 msg.GetMsgId() == msgScriptMigrateHostFailAck::MSG_ID() || 
						 msg.GetMsgId() == msgScriptVerifyHostAck::MSG_ID())
				{
					m_BandwidthStats.AddBandwidthOut(m_AckMsgRecorderId, msg.GetMessageDataBitSize());
				}
				else if (msg.GetMsgId() == msgScriptMigrateHost::MSG_ID() || 
						 msg.GetMsgId() == msgScriptHostRequest::MSG_ID() ||
						 msg.GetMsgId() == msgScriptNewHost::MSG_ID() ||
						 msg.GetMsgId() == msgScriptVerifyHost::MSG_ID())
				{
					m_BandwidthStats.AddBandwidthOut(m_MigrateHostMsgRecorderId, msg.GetMessageDataBitSize());
				}
				else
				{
					Assertf(0, "Unrecognised script manager message");
				}

                m_NumScriptManagerMessagesSent++;
#endif		
				return true;
			}
#if !__NO_OUTPUT
			else
			{
				gnetWarning("Failed to send message to %s with connection ID: 0x%08x", player.GetLogName(), player.GetConnectionId());
			}
#endif
		}

		return false;
	}

	//PURPOSE
	// Sends a script handler message to all players in scope (eg. nearby)
	template<typename T>
	void SendMessageToAllPlayersInScope(scriptHandlerNetComponent& handler, const T& msg)
	{
	    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

            if(remotePlayer->GetConnectionId() >= 0 && !remotePlayer->IsBot())
            {
		        if (handler.IsPlayerInScope(*remotePlayer))
		        {
                    if (SendMessageToPlayer(*remotePlayer, msg))
					{
						handler.MessageSentToPlayer(*remotePlayer);
					}
		        }
            }
        }
	}

	//PURPOSE
	// Sends a script handler message to all current script candidates 
	template<typename T>
	void SendMessageToAllCandidates(scriptHandlerNetComponent& handler, const T& msg)
	{
	    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

		    if (handler.IsPlayerACandidate(*remotePlayer))
		    {
				if (SendMessageToPlayer(*remotePlayer, msg))
				{
					handler.MessageSentToPlayer(*remotePlayer);
				}
		    }
        }
	}

	//PURPOSE
	// Sends a script handler message to all participants in the script
	template<typename T>
	void SendMessageToAllParticipants(scriptHandlerNetComponent& handler, const T& msg, bool bRequiresAck = false, bool bIncludeHost = true)
	{
	    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

		if (bRequiresAck)
		{
			Assert(!handler.IsWaitingForAcks());
		}

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

		    if (handler.IsPlayerAParticipant(*remotePlayer) && !remotePlayer->IsBot() && (bIncludeHost || remotePlayer != handler.GetHost()))
		    {
				if (SendMessageToPlayer(*remotePlayer, msg) && bRequiresAck)
				{
					handler.MessageSentToPlayer(*remotePlayer);
				}
			}
        }
	}

private:

    scriptHandler *HandleScriptJoinMsg(const ReceivedMessageData &messageData);
    scriptHandler *HandleScriptJoinAckMsg(const ReceivedMessageData &messageData);
    scriptHandler *HandleScriptJoinHostAckMsg(const ReceivedMessageData &messageData);
    scriptHandler *HandleScriptHandshakeMsg(const ReceivedMessageData &messageData);
    scriptHandler *HandleScriptHandshakeAckMsg(const ReceivedMessageData &messageData);
    scriptHandler *HandleScriptLeaveMsg(const ReceivedMessageData &messageData);
    scriptHandler *HandleScriptLeaveAckMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptBotJoinMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptBotJoinAckMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptBotHandshakeMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptBotHandshakeAckMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptBotLeaveMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptMigrateHostMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptMigrateHostFailAckMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptHostRequestMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptNewHostMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptVerifyHostMsg(const ReceivedMessageData &messageData);
	scriptHandler *HandleScriptVerifyHostAckMsg(const ReceivedMessageData &messageData);

	void ManageHostMigration();

protected:

	// a unique message handler only used by the script manager
	NetworkPlayerEventDelegate m_Dlgt;

	// set when the manager is initialised
	bool m_IsInitialised;
	bool m_IsNetworkInitialised;
	bool m_DeletingHandlers;

	// The bandwidth manager
	netBandwidthMgr* m_BandwidthMgr;          

	// logging object for the script manager class
	netLog *m_Log;

	// logging object for the script bandwidth
	netLog *m_BandwidthLog;

	// a hash table of the script handlers, accessed via the hash of a script id
	scriptHandlerMap m_scriptHandlers;

	// the time between the host migration attempts
	s32	m_HostMigrationTimer;	   

#if __BANK
    unsigned m_NumScriptManagerMessagesSent; // the number of script manager messages sent since the last call to ResetMessageCounts()

	netBandwidthStatistics m_BandwidthStats;

	// bandwidth recorders for the different message types
	RecorderId m_JoinMsgRecorderId;
	RecorderId m_HandshakeMsgRecorderId;
	RecorderId m_LeaveMsgRecorderId;
	RecorderId m_AckMsgRecorderId;
	RecorderId m_MigrateHostMsgRecorderId;

	u32	m_numMessageBandwidthRecorders;

	u32 m_numScriptsLogged;

#endif
};

}// namespace rage

#endif  // SCRIPT_MGR_BASE_H
