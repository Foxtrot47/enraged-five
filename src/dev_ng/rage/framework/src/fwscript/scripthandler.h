//
// scriptHandler.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef SCRIPT_HANDLER_H
#define SCRIPT_HANDLER_H

// framework includes
#include "fwscript/scriptid.h"
#include "fwscript/scriptobjinfo.h"
#include "fwtl/pool.h"

// rage includes
#include "atl/dlist.h"
#include "atl/inlist.h"
#include "data/base.h"

namespace rage
{
class scriptHandlerNetComponent;
class scriptHandler;
class netPlayer;
class netObject;
class fwEntity;

typedef u32 ScriptResourceId;
typedef u16 ScriptResourceType;
typedef s32 ScriptResourceRef;

//PURPOSE
// Interface class for an object that can be created by a script and registered with a script handler
//
// *****************************************************************************
// *** THIS IS AN INTERFACE CLASS ONLY - DO NOT ADD ANY DATA TO THIS CLASS!! ***
// *****************************************************************************
//
class scriptHandlerObject
{
public:

	virtual ~scriptHandlerObject() {}

	virtual unsigned					GetType() const = 0;
	virtual void						CreateScriptInfo(const scriptIdBase& scrId, const ScriptObjectId& objectId, const HostToken hostToken) = 0;
	virtual void						SetScriptInfo(const scriptObjInfoBase& scrInfo) = 0;
	virtual scriptObjInfoBase*			GetScriptInfo() = 0;
	virtual const scriptObjInfoBase*	GetScriptInfo() const = 0;
	virtual void						SetScriptHandler(scriptHandler* handler) = 0;
	virtual scriptHandler*				GetScriptHandler() const = 0;
	virtual void						ClearScriptHandler() = 0;
	virtual void						OnRegistration(bool newObject, bool hostObject) = 0;
	virtual void						OnUnregistration() = 0;
	virtual bool						IsCleanupRequired() const = 0;
	virtual void						Cleanup() = 0;
	virtual netObject*					GetNetObject() const = 0;
	virtual fwEntity*					GetEntity() const = 0;
	virtual bool						HostObjectCanBeCreatedByClient() const { return false; } // if true clients are allowed to create host objects locally - this is used for certain types of objects that are all declared locally on every machine when the script starts up
#if __BANK
	virtual void						SpewDebugInfo(const char* scriptName) const = 0;
#endif
	bool IsHostObject() const			{ return IS_HOST_OBJECT_ID(GetScriptInfo()->GetObjectId()); }
	bool IsUnnetworkedObject() const	{ return IS_UNNETWORKED_OBJECT_ID(GetScriptInfo()->GetObjectId()); }
};

//PURPOSE
// A class holding info on a game resource created and referenced by the script, and which may need cleaned up when the script terminates.
// Examples of game resources would be models, particle effects, animations, etc.
// Classes derived from this are expected to assign m_Reference when they create the resource.
class scriptResource
{
	friend scriptHandler;

public:

	static const int INVALID_RESOURCE_ID = 0; // invalid ids must be 0 (which is also NULL in the scripts)

public:

	scriptResource(ScriptResourceType type, ScriptResourceRef ref) : m_Type(type), m_Id(INVALID_RESOURCE_ID), m_Reference(ref) {}
	virtual ~scriptResource() {}

	ScriptResourceType			GetType() const							{ return m_Type; }
	ScriptResourceId			GetId() const							{ return m_Id; }
	void						SetId(ScriptResourceId id)				{ m_Id = id; }
	const ScriptResourceRef		GetReference() const					{ return m_Reference; }
	void						SetReference(ScriptResourceRef ref) 	{ m_Reference = ref; }

	//PURPOSE
	//	Gets the name of the resource. Used for debugging.
	virtual const char* GetResourceName() const = 0;

	//PURPOSE
	//	Gets the invalid reference for this resource. This can differ for different resources (usually 0 or -1).
	virtual ScriptResourceRef GetInvalidReference() const = 0;

	//PURPOSE
	//  Creates a new dynamically allocated instance of the resource
	virtual scriptResource* Clone() const = 0;

	//PURPOSE
	//	Creates the resource. Returns true if the creation was successful. Creation can also be done in the constructor and this method used to
	// return whether it was successful.
	virtual bool Create() = 0;

	//PURPOSE
	//	Destroys the resource.
	virtual void Destroy() = 0;

	//PURPOSE
	//	Detaches the resource from the script. It will persist after the script terminates. Only used by some resources.
	virtual void Detach() { Assertf(0, "Detach not defined for this resource"); } 

	//PURPOSE
	//	If this returns true, the resource is detached when the script terminates, otherwise it is destroyed.
	virtual bool DetachOnCleanup() const { return false; }

	//PURPOSE
	//	If this returns true, the resource is only destroyed if no other scripts are referencing it.
	virtual bool LeaveForOtherScripts() const { return false; }

protected:

	ScriptResourceType	m_Type;			// the type of the resource
	ScriptResourceId	m_Id;			// the id of the resource assigned by the script handler it is associated with. Unique to this resource instance.
	ScriptResourceRef	m_Reference;	// the reference used to identify the resource

	inlist_node<scriptResource> m_ListLink;

private:

	scriptResource(const scriptResource&) {}
	scriptResource& operator=(const scriptResource&) { return *this; }
};

// a handy define to use in your resource declaration
#define COMMON_RESOURCE_DEF(className, baseClassName, resourceType, resourceName, invalidReference)\
	static const ScriptResourceType RESOURCE_TYPE		= resourceType;\
	static const ScriptResourceRef	INVALID_REFERENCE	= invalidReference;\
	className(ScriptResourceType type, ScriptResourceRef reference) : baseClassName(type, reference) {}\
	virtual const char*			GetResourceName() const		{ return resourceName ; }\
	virtual ScriptResourceRef	GetInvalidReference() const	{ return INVALID_REFERENCE; }\
	virtual scriptResource*		Clone() const				{ return rage_new className(GetType(), GetReference()); }\

//PURPOSE
//  Associated with a running script thread, keeps track of any objects created by the thread and assigns ids to them. Script handlers are
//  primarily required for the network game, where they are used to determine which machine is arbitrating over a script that is running on
//  multiple machines and is synchronised between them. The script handler needs to assign its own ids to objects for 2 reasons: because the global 
//  ids used in the network game are too large to be used by the scripts, and because the ids sometimes need to be assigned before a network game 
//  is running. The ids are used in any communication between script handlers over the network, so that all the script handlers can refer to the same 
//  object. 
//
//  When a network game starts, a network component is created by the script handlers that can persist during the network game. This deals with 
//  the hosting and joining of a script session abnd handles any message passing, etc.
//
//  Script handlers can persist after the thread terminates, and so they have to be independent entities from scrThreads. 
//
class scriptHandler
{
public:

	//PURPOSE
	// This is a convenience class for allocating the list nodes from a pool
	class atDScriptObjectNode : public atDNode<scriptHandlerObject*, datBase>
	{
	public:
		FW_REGISTER_CLASS_POOL(atDScriptObjectNode);
	};

	typedef inlist<scriptResource, &scriptResource::m_ListLink> ResourceList;

public:

    scriptHandler(scrThread& scriptThread);
	virtual ~scriptHandler();

	//PURPOSE
	//	Updates the handler.
	//RETURNS
	//  true if the handler can be destroyed
	virtual bool Update();

	//PURPOSE
	//	Updates the network component of the handler.
	//RETURNS
	//  true if the handler can be destroyed
	virtual bool NetworkUpdate();

	//PURPOSE
	//	Shutdown the handler. 
	virtual void Shutdown();

	//PURPOSE
	// Returns the id of the script this handler is associated with. Derived script handler classes must contain a project specific script id.
	virtual scriptIdBase&		GetScriptId() = 0;
	virtual const scriptIdBase&	GetScriptId() const = 0;

	scrThread*					GetThread()									{ return m_Thread; }
	const scrThread*			GetThread() const							{ return m_Thread; }
	const char*					GetLogName() const							{ return GetScriptId().GetLogName(); }
	scriptHandlerNetComponent*	GetNetworkComponent() const					{ return m_NetComponent; }
	bool						IsTerminated() const						{ return m_Thread == NULL; }
	void						SetNextFreeHostObjectId(ScriptObjectId id)	{ Assert(id >= m_NextFreeHostObjectId); m_NextFreeHostObjectId = id; }

	//PURPOSE
	// Returns true if this script handler has to be networked.  
	virtual bool RequiresANetworkComponent() const = 0;

	//PURPOSE
	// Called when a network game starts. Creates the network component of the script handler, which handles message passing, participant lists, etc.
	// This is pure virtual so that you can create your own project-specific version of the network component.
	// maxNumParticipants - the maximum number of players that can simultaneously participate in the script session
	virtual void CreateNetworkComponent() = 0;

	//PURPOSE
	// Called when a network game ends. Removes the network component of the script handler.
	virtual void DestroyNetworkComponent();

	//PURPOSE
    // Called when the script this handler represents is terminating
	//RETURNS
	//  true if the handler can be destroyed
    virtual bool Terminate();

	//PURPOSE
	// Registers an object created by the script with the handler, and creates a script info for it.
	//PARAMS
	// hostObject - If true, this object is an object created by the host of the script. If false, the object has been created locally.
	//				Host created script objects are critical to the script and exist on all machines running the script, eg a flag in a capture the flag game.
	//				Local objects are only required for use by a single machine, eg a car created for a player in a race.
	// networked - If true, this new object is not networked. hostObject is ignored in this case, as a new local non-networked id will be generated
	//             for this entity
    virtual bool RegisterNewScriptObject(scriptHandlerObject &object, bool hostObject, bool networked);

	//PURPOSE
	// Registers an object which has already been registered with a handler and had a script info created for this script.
	void RegisterExistingScriptObject(scriptHandlerObject &object);

	//PURPOSE
	// Used when switching resource thread ownership
	void RegisterExistingScriptResource(scriptResource* resource);

	//PURPOSE
	// Used when switching resource thread ownership
	void UnregisterScriptResource(scriptResource* resource);

	//PURPOSE
	// Unregisters an object created by the script with the handler
    virtual void UnregisterScriptObject(scriptHandlerObject &object);

    //PURPOSE
	// Returns the network object with the specified script ID that was created by this script - if it exists
    scriptHandlerObject *GetScriptObject(ScriptObjectId objectId);

	//PURPOSE
	// Adds a new resource to the resource list
	//RETURNS
	//  The cloned resource that was added to the resource list. NULL if registering was unsuccessful.
	virtual scriptResource* RegisterScriptResource(scriptResource& resource, bool *pbResourceHasJustBeenAddedToList = NULL);

	//PURPOSE
	//	Removes the resource on the resource list with the given id and possibly destroys the resource
	//PARAMS
	//	resourceId - the id of the resource to be removed
	//	bDetach - if set the resource is only detached from the script handler, and is left to persist.
	//	bAssert - if set the method will assert if the script resource does not exist
	//RETURNS
	//	true if the resource was found and removed
	virtual bool RemoveScriptResource(ScriptResourceId resourceId, bool bDetach = false, bool bAssert = true, ScriptResourceType resourceType = (ScriptResourceType)-1);

	//PURPOSE
	//	Removes the resource on the resource list matching the given parameters and possibly destroys the resource
	//PARAMS
	//	type - the type of the resource to be removed
	//  ref - the reference num of the resource to be removed
	//	bDetach - if set the resource is only detached from the script handler, and is left to persist.
	//	bAssert - if set the method will assert if the script resource does not exist
	//RETURNS
	//	true if the resource was found and removed
	virtual bool RemoveScriptResource(ScriptResourceType type, const ScriptResourceRef ref, bool bDetach = false, bool bAssert = true);

	//PURPOSE
	//	Removes the all the resource on the resource list with the given type
	void RemoveAllScriptResourcesOfType(ScriptResourceType type);

	//PURPOSE
	//  Returns the resource on the resource list with the given id
	scriptResource* GetScriptResource(ScriptResourceId resourceId);

	//PURPOSE
	//  Returns the resource on the resource list matching the given parameters
	scriptResource* GetScriptResource(ScriptResourceType type, const ScriptResourceRef ref);

	//PURPOSE
	//	Returns the resource reference for the resource matching the given id. 
	ScriptResourceRef GetResourceReferenceFromId(ScriptResourceId id);

	//PURPOSE
	//  Returns the number of resource on the resource list of the given type
	unsigned GetNumScriptResourcesOfType(ScriptResourceType type) const;

    //PURPOSE
    //  Calls the callback for each of the script resources in the resource list
    typedef bool (*Callback)(const scriptResource* resource, void* data);
    void ForAllScriptResources(Callback cb, void* data) const;

	//PURPOSE
	// Allocates a new id for a host object
	ScriptObjectId AllocateNewHostObjectId();

#if __BANK
	// spews debug info to the TTY on the script handler objects and resources
	virtual void SpewObjectAndResourceInfo() const {}
	virtual void SpewAllResourcesOfType(ScriptResourceType UNUSED_PARAM(resourceType)) const {}
#endif	//	__BANK

	// returns true if the host broadcast data is synced with all participabts and all the host objects are cloned on those participants  
	bool AreAllParticipantsUpToDateWithHostState(bool bLogFailure = false) const;

protected:

	//PURPOSE
	// Returns a unique resource id to be assigned to a newly registered script resource. This is virtual so that the simple id system
	// can be overloaded with a more complex system if necessary.
	virtual ScriptResourceId GetNextFreeResourceId(scriptResource& UNUSED_PARAM(resource)) { return ++m_NextFreeResourceId; }

	//PURPOSE
	// Registers an object which has already been registered with a handler and had a script info created for this script.
	virtual void RegisterExistingScriptObjectInternal(scriptHandlerObject &object);

	//PURPOSE
	// Called when the object is unregistered from the script handler. Detaches the object from the handler and possibly cleans up the script state
	// of the object.
	void DetachScriptObject(scriptHandlerObject &object);

	//PURPOSE
	// Called from DetachScriptObject. Performs a cleanup of the script state of the object.
	virtual void CleanupScriptObject(scriptHandlerObject &object);

	//PURPOSE
	// Called from DetachScriptObject. Performs a cleanup of the script state of the object. Returns true if successful.
	virtual bool DestroyScriptResource(scriptResource& resource);

	//PURPOSE
	// Called when a script object with the same id as a new object is found
	virtual void RemoveOldScriptObject(scriptHandlerObject &object);

public:

	inlist_node<scriptHandler>				m_ListLink;				// The list link used by the script handler list in the script handler manager.

protected:

	scrThread*								m_Thread;						// the thread this handler is associated with
    atDList<scriptHandlerObject*, datBase>	m_ObjectList;					// a list of objects created by the script associated with this handler
	ResourceList						    m_ResourceList;					// a list of resources that have been created by the script
	scriptHandlerNetComponent*				m_NetComponent;					// the network component, which exists during a network game 
	ScriptObjectId							m_NextFreeHostObjectId;			// the next free id to be assigned to a host script object created by the script associated with this handler.
	ScriptObjectId							m_NextFreeClientObjectId;		// the next free id to be assigned to a client script object created by the script associated with this handler.
	ScriptObjectId							m_NextFreeNonNetworkedObjectId;	// the next free id to be assigned to a non-networked script object created by the script associated with this handler.
	ScriptResourceId						m_NextFreeResourceId;			// the next free id to be assigned to a script resource created by the script associated with this handler.

private:

    scriptHandler(const scriptHandler&) {}

	atDScriptObjectNode *CreateNewScriptObjectNode();
};

} // namespace rage

#endif  // NET_SCRIPT_HANDLER_BASE_H
