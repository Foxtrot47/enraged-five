//
// scriptInterface.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwscript/scriptInterface.h"

#include "fwnet/optimisations.h"
#include "fwscript/scriptHandlerMgr.h"
#include "fwscript/scriptobjinfo.h"

namespace rage
{

NETWORK_OPTIMISATIONS()

bool                scriptInterface::m_IsInitialised	= false; 
scriptHandlerMgr*	scriptInterface::m_ScriptMgr		= 0;

void scriptInterface::Init(scriptHandlerMgr &scriptMgr)
{
	Assert(!m_IsInitialised);
	Assert(m_ScriptMgr == 0);

	m_ScriptMgr = &scriptMgr;
	m_IsInitialised = true;
}

void scriptInterface::Shutdown()
{
	Assert(m_IsInitialised);

	m_ScriptMgr = 0;
	m_IsInitialised = false;
}

bool scriptInterface::IsScriptRegistered(const scriptIdBase& scriptId)
{
	Assert(m_ScriptMgr);
	return (m_ScriptMgr->GetScriptHandler(scriptId) != NULL);
}

bool scriptInterface::IsNetworkScript(const scriptIdBase& scriptId)
{
    bool isNetworkScript = false;

    scriptHandler *handler = m_ScriptMgr->GetScriptHandler(scriptId);

    if(handler && handler->RequiresANetworkComponent())
    {
        isNetworkScript = true;
    }

    return isNetworkScript;
}

void scriptInterface::RegisterScript(scrThread& scriptThread)
{
	Assert(m_ScriptMgr);
	m_ScriptMgr->RegisterScript(scriptThread);
}

void scriptInterface::UnregisterScript(scrThread& scriptThread)
{
	Assert(m_ScriptMgr);
	m_ScriptMgr->UnregisterScript(scriptThread);
}

const netPlayer* scriptInterface::GetHostOfScript(const scriptIdBase& scriptId) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler))
	{
		if (pScriptHandler->GetNetworkComponent())
		{
			return pScriptHandler->GetNetworkComponent()->GetHost();
		}
		else
		{
			return netInterface::GetLocalPlayer();
		}
	}

	return NULL;
}

unsigned scriptInterface::GetNumScriptParticipants(const scriptIdBase& scriptId) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler))
	{
		if (pScriptHandler->GetNetworkComponent())
		{
			return pScriptHandler->GetNetworkComponent()->GetNumParticipants();
		}
		else
		{
			return 1;
		}
	}

	return 0;
}

unsigned scriptInterface::GetMaxNumScriptParticipants(const scriptIdBase& scriptId) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler) && pScriptHandler->GetNetworkComponent())
	{
		return pScriptHandler->GetNetworkComponent()->GetMaxNumParticipants();
	}

	return 1;
}

bool scriptInterface::IsPlayerAParticipant(const scriptIdBase& scriptId, const netPlayer& player) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler))
	{
		if (pScriptHandler->GetNetworkComponent())
		{
			return pScriptHandler->GetNetworkComponent()->IsPlayerAParticipant(player);
		}
		else
		{
			return (&player == netInterface::GetLocalPlayer());
		}
	}

	return false;
}

bool scriptInterface::IsPlayerAParticipant(const scriptIdBase& scriptId, PhysicalPlayerIndex playerIndex) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler))
	{
		if (pScriptHandler->GetNetworkComponent())
		{
			return pScriptHandler->GetNetworkComponent()->IsPlayerAParticipant(playerIndex);
		}
	}

	return false;
}

unsigned scriptInterface::GetSlotParticipantIsUsing(const scriptIdBase& scriptId, const netPlayer& player) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler))
	{
		if (pScriptHandler->GetNetworkComponent())
		{
			return pScriptHandler->GetNetworkComponent()->GetSlotParticipantIsUsing(player);
		}
		else
		{
			// If the network component does not exist, then our local player will automatically use slot 0. 
			Assert(player.IsLocal());
			return 0;
		}
	}

	return 0;
}

const netPlayer* scriptInterface::GetParticipantUsingSlot(const scriptIdBase& scriptId, unsigned slot) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler))
	{
		if (pScriptHandler->GetNetworkComponent())
		{
			return pScriptHandler->GetNetworkComponent()->GetParticipantUsingSlot(slot);
		}
		else if (slot == 0)
		{
			// If the network component does not exist, then our local player will automatically use slot 0. 
			return netInterface::GetLocalPlayer();	
		}
	}

	return NULL;
}

PlayerFlags	scriptInterface::GetParticipants(const scriptIdBase& scriptId)
{
	PlayerFlags participants = 0;

	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler) && pScriptHandler->GetNetworkComponent())
	{
		participants = pScriptHandler->GetNetworkComponent()->GetPlayerParticipants();
	}

	return participants;
}

void scriptInterface::AcceptPlayers(const scriptIdBase& scriptId, bool bAccept)
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (AssertVerify(pScriptHandler) && AssertVerify(pScriptHandler->GetNetworkComponent()))
	{
		pScriptHandler->GetNetworkComponent()->AcceptPlayers(bAccept);
	}
}

void scriptInterface::RegisterNewScriptObject(const scriptIdBase& scriptId, scriptHandlerObject *object, bool hostObject, bool networked)
{
	Assert(m_ScriptMgr);

	if(Verifyf(object, "Trying to register a NULL network object with a script handler"))
    {
        scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	    if (pScriptHandler)
	    {
            pScriptHandler->RegisterNewScriptObject(*object, hostObject, networked);
        }
    }
}

void scriptInterface::RegisterExistingScriptObject(const scriptIdBase& scriptId, scriptHandlerObject *object)
{
	Assert(m_ScriptMgr);

	if(Verifyf(object, "Trying to register a NULL network object with a script handler"))
	{
		scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

		if (pScriptHandler)
		{
			pScriptHandler->RegisterExistingScriptObject(*object);
		}
	}
}

void scriptInterface::UnregisterScriptObject(scriptHandlerObject& object)
{
	Assert(m_ScriptMgr);

    if(Verifyf(object.GetScriptInfo(), "Trying to unregister a non-script network object from a script handler"))
    {
        scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(object.GetScriptInfo()->GetScriptId());

	    if (pScriptHandler)
        {
			pScriptHandler->UnregisterScriptObject(object);
        }
    }
}

scriptHandlerObject *scriptInterface::GetScriptObject(const scriptIdBase& scriptId, ScriptObjectId objectId)
{
	Assert(m_ScriptMgr);

    scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (Verifyf(pScriptHandler, "No script handler found for supplied script id!"))
	{
        return pScriptHandler->GetScriptObject(objectId);
    }

    return NULL;
}

ScriptObjectId scriptInterface::GetScriptIDFromScriptObject(scriptHandlerObject& object)
{
	Assert(m_ScriptMgr);

    ScriptObjectId scriptObjectID = INVALID_SCRIPT_OBJECT_ID;

    const scriptObjInfoBase *scriptInfo = object.GetScriptInfo();

    if(scriptInfo)
    {
       scriptObjectID = scriptInfo->GetObjectId();
    }

    return scriptObjectID;
}

unsigned scriptInterface::GetNumRequiredScriptEntities()
{
    Assert(m_ScriptMgr);
    return m_ScriptMgr->GetNumRequiredEntities();
}

scriptHandlerNetComponent::eExternalState scriptInterface::GetScriptState(const scriptIdBase& scriptId) 
{
	Assert(m_ScriptMgr);

	scriptHandler* pScriptHandler = m_ScriptMgr->GetScriptHandler(scriptId);

	if (pScriptHandler && pScriptHandler->GetNetworkComponent())
	{
		return pScriptHandler->GetNetworkComponent()->GetState();
	}

	return scriptHandlerNetComponent::NETSCRIPT_PLAYING;
}

scriptHandlerMgr& scriptInterface::GetScriptManager()
{
	Assert(m_ScriptMgr);
	return *m_ScriptMgr;
}

} // namespace rage


