//
// fwscript/scriptguid.cpp : an extension given to any entity that a script is referencing (including ambient entities)
//						  Used to assign a GUID to an entity (the id is the pool index of the extension).
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
#include "scriptguid.h"
#include "entity/entity.h"

namespace rage {

#define MAX_NUM_GUIDS 700

FW_INSTANTIATE_CLASS_POOL(fwScriptGuid, MAX_NUM_GUIDS, atHashString("fwScriptGuid",0x7311a8d7));
AUTOID_IMPL(fwScriptGuid);

fwScriptGuid::fwScriptGuid(fwExtensibleBase& base) 
: m_pBase(&base)
{
	// add this extension to the entity's extension list
	base.GetExtensionList().Add(*this);
}

fwScriptGuid::~fwScriptGuid()
{
	scrDebugf2("Destroyed GUID (%d) %d/%d", fwScriptGuid::GetPool()->GetIndex(this), fwScriptGuid::GetPool()->GetNoOfUsedSpaces(), fwScriptGuid::GetPool()->GetSize());
};

int fwScriptGuid::CreateGuid(fwExtensibleBase& base)
{
	int guid = 0;
	bool bNew = false;

	if (!base.GetExtension<fwScriptGuid>())
	{
		rage_new fwScriptGuid(base);
		bNew = true;
	}

	fwScriptGuid* pGuid = static_cast<fwScriptGuid*>(base.GetExtension<fwScriptGuid>());

	if (scrVerifyf(pGuid, "Failed to get GUID"))
	{
		guid = fwScriptGuid::GetPool()->GetIndex(pGuid);
	}

	if(bNew) {
		scrDebugf2("Created new GUID (%d) %d/%d for %s", guid, fwScriptGuid::GetPool()->GetNoOfUsedSpaces(), fwScriptGuid::GetPool()->GetSize(), base.GetClassId().TryGetCStr());
	}
	return guid;
}

void fwScriptGuid::DeleteGuid(fwExtensibleBase& base)
{
	base.DestroyExtension<fwScriptGuid>();
}

int fwScriptGuid::GetGuidFromBase(fwExtensibleBase& base)
{
	fwScriptGuid* pGuid = static_cast<fwScriptGuid*>(base.GetExtension<fwScriptGuid>());
	if (pGuid)
	{
		return fwScriptGuid::GetPool()->GetIndex(pGuid);
	}
	return 0;
}

fwExtensibleBase* fwScriptGuid::GetBaseFromGuid(int guid)
{
	if (!Verifyf(guid != -1,"invalid guid in GetBaseFromGuid - check if earlier script command incorrectly returned -1"))
		return NULL;

	fwScriptGuid* pGuid = fwScriptGuid::GetPool()->GetAt(guid);

	if (pGuid)
	{
		scrAssertf(pGuid->GetBase(), "Found a GUID extension with no parent!");
		return pGuid->GetBase();
	}
	return NULL;
}

#if __BANK
template<> void fwPool<fwScriptGuid>::PoolFullCallback() 
{
	Displayf("********************************************");
	Displayf("** fwScriptGuid POOL FULL CALLBACK OUTPUT **");

	for (s32 i = 0; i < fwScriptGuid::GetPool()->GetSize(); ++i)
	{
		fwScriptGuid* pScriptGUID = fwScriptGuid::GetPool()->GetSlot(i);
		if (pScriptGUID)
		{
			fwExtensibleBase *pExtBase = pScriptGUID->GetBase();

			if ( pExtBase )
			{
				// Get the class name
				const char *pGUIClassName = pExtBase->GetClassId().GetCStr();

				// Get the debug name for entities
				const char* pModelName = "";
				if ( pExtBase->GetIsClass<fwEntity>() )
				{
					fwEntity* pEntity = static_cast<fwEntity*>(pExtBase);
					pModelName = pEntity->GetModelName();
					Displayf("Slot %i: Type: %s, Model Name:%s Memory Address Object:%p  Memory Address GUID: %p", 
						i, pGUIClassName, pModelName, pExtBase, pScriptGUID);
				}
				// For other objects just write type and memory
				else
				{
					Displayf("Slot %i: Type: %s, Memory Address Object:%p  Memory Address GUID: %p", 
						i, pGUIClassName, pExtBase, pScriptGUID);
				}
			}
			// No base
			else
			{
				Displayf("Slot %i: No object. Memory Address GUID: %p", i, pScriptGUID);
			}
		}
	}
}
#endif // __BANK


} // namespace rage
