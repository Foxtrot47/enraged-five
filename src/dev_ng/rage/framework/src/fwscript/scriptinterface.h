//
//
// scriptInterface.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef SCRIPT_INTERFACE_H
#define SCRIPT_INTERFACE_H

#include "fwscript/scripthandlernetcomponent.h"
#include "fwscript/scriptObjInfo.h"

namespace rage
{
class scriptHandlerMgr;
class scriptHandlerObject;
class scriptIdBase;
class scriptId;
class netPlayer;
class netObject;

class scriptInterface
{
public:

	static void					Init(scriptHandlerMgr &scriptMgr);
	static void					Shutdown();

	static bool					IsScriptRegistered(const scriptIdBase& scriptId);
    static bool					IsNetworkScript(const scriptIdBase& scriptId);
	static void					RegisterScript(scrThread& scriptThread);
	static void					UnregisterScript(scrThread& scriptThread);
	static const netPlayer*		GetHostOfScript(const scriptIdBase& scriptId);
	static unsigned				GetNumScriptParticipants(const scriptIdBase& scriptId);
	static unsigned				GetMaxNumScriptParticipants(const scriptIdBase& scriptId);
	static bool					IsPlayerAParticipant(const scriptIdBase& scriptId, const netPlayer& player);
	static bool					IsPlayerAParticipant(const scriptIdBase& scriptId, PhysicalPlayerIndex playerIndex);
	static unsigned				GetSlotParticipantIsUsing(const scriptIdBase& scriptId, const netPlayer& player);
	static const netPlayer*		GetParticipantUsingSlot(const scriptIdBase& scriptId, unsigned slot);
	static PlayerFlags			GetParticipants(const scriptIdBase& scriptId);
	static void					RemotelyActivateScript(const scriptIdBase& scriptId);
	static void					AcceptPlayers(const scriptIdBase& scriptId, bool bAccept);
	static void					RegisterNewScriptObject(const scriptIdBase& scriptId, scriptHandlerObject *object, bool hostObject, bool networked);
	static void					RegisterExistingScriptObject(const scriptIdBase& scriptId, scriptHandlerObject *object);
    static void					UnregisterScriptObject(scriptHandlerObject& object);
    static scriptHandlerObject*	GetScriptObject(const scriptIdBase& scriptId, ScriptObjectId objectId);
    static ScriptObjectId		GetScriptIDFromScriptObject(scriptHandlerObject &object);
    static unsigned             GetNumRequiredScriptEntities();
 
	scriptHandlerNetComponent::eExternalState  GetScriptState(const scriptIdBase& scriptId);

	static scriptHandlerMgr&	GetScriptManager();

private:

	static bool                  m_IsInitialised; // Indicates whether the interface has been initialised
	static scriptHandlerMgr		*m_ScriptMgr;     // The script manager
};

} // namespace rage

#endif  //NET_SCRIPT_INTERFACE_H
