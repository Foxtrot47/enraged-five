//
//
// scriptId.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwscript/scriptId.h"

// rage includes
#include "script/thread.h"

// framework includes
#include "fwnet/optimisations.h"

namespace rage
{

NETWORK_OPTIMISATIONS()

void scriptId::Read(datBitBuffer &bb)
{
	u32 hash = 0;

	bb.ReadUns(hash, SIZEOF_NAME_HASH);

	bool bGetName = (hash != m_ScriptNameHash.GetHash()) || m_ScriptName[0] == 0;

	m_ScriptNameHash.SetHash(hash);

	if (bGetName)
	{
		SetNameFromHash();
	}
}

void scriptId::Write(datBitBuffer &bb) const
{
	u32 hash = m_ScriptNameHash.GetHash();

	bb.WriteUns(hash, SIZEOF_NAME_HASH);
}

void scriptId::Log(netLoggingInterface &log)
{
	log.WriteDataValue("Script name", m_ScriptName);
}

void scriptId::InitScriptId(const scrThread& scriptThread)
{
	// GetScriptName() is not const
	scrThread& thrd = const_cast<scrThread&>(scriptThread);

	m_ScriptNameHash.SetFromString(thrd.GetScriptName());

	Assertf(strlen(thrd.GetScriptName()) <= SCRIPT_NAME_LEN, "Script name too long: %s", thrd.GetScriptName());
	strncpy(m_ScriptName, thrd.GetScriptName(), SCRIPT_NAME_LEN);
}

} // namespace rage
