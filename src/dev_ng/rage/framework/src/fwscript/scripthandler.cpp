//
// scriptHandler.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwscript/scriptHandler.h"

#include "fwnet/netarrayhandlertypes.h"
#include "fwnet/netarraymgr.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobject.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/optimisations.h"
#include "fwscript/scripthandlermessages.h"
#include "fwscript/scripthandlermgr.h"
#include "fwscript/scriptinterface.h"
#include "fwscript/scriptobjinfo.h"

namespace rage
{

NETWORK_OPTIMISATIONS()

FW_INSTANTIATE_CLASS_POOL_SPILLOVER(scriptHandler::atDScriptObjectNode, CONFIGURED_FROM_FILE, 0.29f, atHashString("atDScriptObjectNode",0x3eea2da9));

scriptHandler::scriptHandler(scrThread& scriptThread)
: m_Thread(&scriptThread)
, m_NetComponent(NULL)
, m_NextFreeHostObjectId(0)
, m_NextFreeClientObjectId(0)
, m_NextFreeNonNetworkedObjectId(0)
, m_NextFreeResourceId(0)
{
	gnetAssert(m_Thread);
}

scriptHandler::~scriptHandler()
{
	gnetAssert(m_ObjectList.GetHead() == NULL);
	gnetAssert(m_NetComponent == NULL);

	if (m_NetComponent)
	{
		DestroyNetworkComponent();
	}

	NETWORK_QUITF(scriptInterface::GetScriptManager().IsDeletingHandlers(), "Invalid deletion of script handler %s", GetLogName());
}

bool scriptHandler::Update()
{
	if (!m_NetComponent && !m_Thread)
	{
		return true;
	}

	return false;
}

bool scriptHandler::NetworkUpdate()
{
	if (m_NetComponent)
	{
		return m_NetComponent->Update();
	}

	return false;
}

void scriptHandler::Shutdown()
{
	// cleanup script object list
	atDNode<scriptHandlerObject*, datBase> *node = m_ObjectList.GetHead();

	while (node)
	{
		atDNode<scriptHandlerObject*, datBase> *nextNode = node->GetNext();
		m_ObjectList.PopNode(*node);
		DetachScriptObject(*node->Data);
		delete node;
		node = nextNode;
	}

	while (!m_ResourceList.empty())
	{
		scriptResource* resource = m_ResourceList.front();

		m_ResourceList.pop_front();

		// some resources are only detached at this stage, and persist after the script terminates.
		if (resource->DetachOnCleanup())
		{
			resource->Detach();
		}
		else
		{
			DestroyScriptResource(*resource);
		}

		delete resource;
	}

	if (m_NetComponent)
	{
		DestroyNetworkComponent();
	}
}

void scriptHandler::DestroyNetworkComponent()
{
	if (gnetVerify(m_NetComponent))
	{
		delete m_NetComponent;
		m_NetComponent = NULL;
	}
}

bool scriptHandler::Terminate()
{
	m_Thread = NULL;

	if (m_NetComponent)
	{
		return m_NetComponent->Terminate();
	}

	return true;
}

bool scriptHandler::RegisterNewScriptObject(scriptHandlerObject &object, bool hostObject, bool networked)
{
	if(gnetVerifyf(!object.GetScriptInfo(), "%s: Trying to register an existing script object (%d) as a new script object", GetLogName(), object.GetScriptInfo()->GetObjectId()) &&
		gnetVerifyf(!object.GetScriptHandler(), "%s : Trying to register a new script object that is already registered with a handler", GetLogName()))
    {
		atDNode<scriptHandlerObject*, datBase> *pNewNode = CreateNewScriptObjectNode();

		if (gnetVerifyf(pNewNode, "Ran out of atDScriptObjectNodes!"))
		{
			if (networked && m_NetComponent && m_NetComponent->GetState() != scriptHandlerNetComponent::NETSCRIPT_PLAYING)
			{
				gnetAssertf(0, "%s: Registering a script object while the script is not in a playing state. This will cause other issues.", GetLogName());
			}

			ScriptObjectId newId = INVALID_SCRIPT_OBJECT_ID;
		
			if (!networked)
			{
				bool bFoundAFreeSlot = false;
				while (!bFoundAFreeSlot)
				{
					if (m_NextFreeNonNetworkedObjectId == (1 << SIZEOF_HANDLER_OBJECT_ID) - 1)
					{
						m_NextFreeNonNetworkedObjectId = 0;
					}

					newId = MAKE_SCRIPT_OBJECT_ID(SCRIPT_OBJECT_UNNETWORKED_SLOT_INDEX, ++m_NextFreeNonNetworkedObjectId);

					if (!GetScriptObject(newId))
					{
						bFoundAFreeSlot = true;
					}
				}

				gnetAssertf(!object.GetNetObject(), "%s: Registering a non-networked script object that has a network object", GetLogName());
			}
			else if (hostObject)
			{
				newId = AllocateNewHostObjectId();
			}
			else 
			{
				// If the network component does not exist, then our local player will automatically use slot 0. If the network component 
				// becomes active due to a network game starting, then our player MUST remain in slot 0 and become the host of the script.
				int slotNumber = 0;

				if (m_NetComponent)
				{
					slotNumber = m_NetComponent->GetSlotParticipantIsUsing(*netInterface::GetLocalPlayer());
				}

				if(gnetVerifyf(slotNumber >= 0, "%s: Local player has an invalid slot number for this script handler!", GetLogName()))
				{
					if (m_NextFreeClientObjectId == (1<<SIZEOF_HANDLER_OBJECT_ID)-1)
					{
						m_NextFreeClientObjectId = 0;
					}

					newId = MAKE_SCRIPT_OBJECT_ID(slotNumber, ++m_NextFreeClientObjectId);
					Assert(newId != INVALID_SCRIPT_OBJECT_ID);
				}
			}

			if (newId != INVALID_SCRIPT_OBJECT_ID)
			{
				HostToken hostToken = 0;
			
				if (m_NetComponent && hostObject)
				{
					hostToken = m_NetComponent->GetHostToken();

					if (!object.HostObjectCanBeCreatedByClient())
					{
						Assert(m_NetComponent->IsHostLocal());
					}
				}

				object.CreateScriptInfo(GetScriptId(), newId, hostToken);
				object.SetScriptHandler(this);

				object.OnRegistration(true, hostObject);

				// assert first so log files are flushed
				scriptHandlerObject* pExistingObject = GetScriptObject(newId);

				if (pExistingObject)
				{
					// don't assert for client objects as there can be valid cases where a client object created by a previous participant exists
					Assertf(!hostObject, "%s: RegisterNewScriptObject (%s) - A host object (%s) already exists with this id (%d)", GetLogName(), pExistingObject->GetNetObject() ? pExistingObject->GetNetObject()->GetLogName() : "??", object.GetNetObject() ? object.GetNetObject()->GetLogName() : "??", object.GetScriptInfo()->GetObjectId());

					RemoveOldScriptObject(*pExistingObject);
				}

				gnetAssert(object.GetScriptInfo());
				gnetAssert(object.GetScriptInfo()->GetScriptId() == GetScriptId());
				gnetAssert(object.GetScriptHandler() == this);

				pNewNode->Data = &object;
				m_ObjectList.Append(*pNewNode);

				return true;
			}
		}
    }

	return false;
}

void scriptHandler::RegisterExistingScriptObject(scriptHandlerObject &object)
{
	// don't register the object if the network component is not in a playing state. Once the component enters a playing state it will
	// register all existing script objects then.
	if (object.IsUnnetworkedObject() || (gnetVerify(GetNetworkComponent()) && GetNetworkComponent()->GetState() == scriptHandlerNetComponent::NETSCRIPT_PLAYING))
	{
		RegisterExistingScriptObjectInternal(object);
	}
}

void scriptHandler::UnregisterScriptObject(scriptHandlerObject &object)
{
	gnetAssertf(object.GetScriptInfo(), "%s : Trying to unregister an object with no script info!", GetLogName());

	if (object.GetScriptInfo()->GetScriptId() != GetScriptId())
	{
#if __ASSERT
		// scriptId.GetLogName() uses a static string to return the name, and this will cause the assert to be incorrect as it will use the same string for both names
		char objectScriptName[100];
		formatf(objectScriptName, 100, "%s", object.GetScriptInfo()->GetScriptId().GetLogName());
		gnetAssertf(0, "%s : Trying to unregister an object (%d) that has a different script id (%s)!", GetLogName(), object.GetScriptInfo()->GetObjectId(), objectScriptName);
#endif // __ASSERT
		//return;
	}

	object.OnUnregistration();

    atDNode<scriptHandlerObject*, datBase> *node = m_ObjectList.GetHead();

    while (node)
    {
        if (node->Data == &object)
        {
            break;
        }

		node = node->GetNext();
    }

    if(node)
    {
        m_ObjectList.PopNode(*node);
		DetachScriptObject(object);
        delete node;
    }
	else
	{
		object.ClearScriptHandler();

		if (!GetNetworkComponent() || GetNetworkComponent()->GetState() >= scriptHandlerNetComponent::NETSCRIPT_PLAYING)
		{
			gnetAssertf(0,"%s : Trying to unregister a script object (%d) is not on the object list", GetLogName(), object.GetScriptInfo()->GetObjectId());
		}
	}
}

scriptHandlerObject *scriptHandler::GetScriptObject(ScriptObjectId objectId)
{
    atDNode<scriptHandlerObject*, datBase> *node = m_ObjectList.GetHead();

    while (node)
    {
        const scriptObjInfoBase *scriptInfo = node->Data->GetScriptInfo();

        if (scriptInfo && scriptInfo->GetObjectId() == objectId)
        {
            return node->Data;
        }

        node = node->GetNext();
    }

    return NULL;
}

scriptResource* scriptHandler::RegisterScriptResource(scriptResource& resource, bool *pbResourceHasJustBeenAddedToList)
{
	if (pbResourceHasJustBeenAddedToList)
	{
		*pbResourceHasJustBeenAddedToList = false;
	}
	if (resource.Create() && 
		gnetVerifyf(!GetScriptResource(resource.GetType(), resource.GetReference()), "RegisterScriptResource: Script resource with type %d and ref %d already exists", resource.GetType(), resource.GetReference()) &&
		gnetVerifyf(resource.GetReference() != resource.GetInvalidReference(), "RegisterScriptResource: Trying to register a script resource of type %d with an invalid reference", resource.GetType()))
	{
		scriptResource* pClone = resource.Clone();

		if (gnetVerifyf(pClone, "Ran out of script resources"))
		{
			pClone->SetId(GetNextFreeResourceId(resource));
			m_ResourceList.push_back(pClone);

			if (pbResourceHasJustBeenAddedToList)
			{
				*pbResourceHasJustBeenAddedToList = true;
			}
		}

		return pClone;
	}

	return NULL;
}

void scriptHandler::RegisterExistingScriptResource(scriptResource* resource)
{
	Assert(resource);
	m_ResourceList.push_back(resource);
}

void scriptHandler::UnregisterScriptResource(scriptResource* resource)
{
	Assert(resource);
	m_ResourceList.erase(resource);
}

bool scriptHandler::RemoveScriptResource(ScriptResourceType type, const ScriptResourceRef ref, bool bDetach, bool ASSERT_ONLY(bAssert))
{
	scriptResource* resource = GetScriptResource(type, ref);

	if (resource)
	{
		m_ResourceList.erase(resource);

		if (bDetach)
		{
			resource->Detach();
		}
		else
		{
			DestroyScriptResource(*resource);
		}

		delete resource;
		return true;
	}
	else
	{
		ASSERT_ONLY(gnetAssertf(!bAssert, "RemoveScriptResource : Could not find script resource with type %d and ref %d", type, ref);)
	}

	return false;
}

bool scriptHandler::RemoveScriptResource(ScriptResourceId resourceId, bool bDetach, bool ASSERT_ONLY(bAssert), ScriptResourceType ASSERT_ONLY(resourceType))
{
	scriptResource* resource = GetScriptResource(resourceId);

	if (resource)
	{
		ASSERT_ONLY(gnetAssertf(resourceType == (ScriptResourceType)-1 || resource->GetType() == resourceType, "RemoveScriptResource : Script resource with id %d does not have the expected resource type %d", resourceId, resourceType);)

		m_ResourceList.erase(resource);

		if (bDetach)
		{
			resource->Detach();
		}
		else
		{
			DestroyScriptResource(*resource);
		}

		delete resource;
		return true;
	}
	else
	{
		ASSERT_ONLY(gnetAssertf(!bAssert, "RemoveScriptResource : Could not find script resource with id %d", resourceId);)
	}

	return false;
}

void scriptHandler::RemoveAllScriptResourcesOfType(ScriptResourceType type)
{
	ResourceList::iterator curr = m_ResourceList.begin();
	ResourceList::const_iterator end = m_ResourceList.end();

	scriptResource* resource = NULL;

	while (curr != end)
	{
		resource = *curr;

		if (resource->GetType() == type)
		{
			m_ResourceList.erase(curr);
			DestroyScriptResource(*resource);
			delete resource;

			curr = m_ResourceList.begin();
		}
		else
		{
			++curr;
		}
	}
}

scriptResource* scriptHandler::GetScriptResource(ScriptResourceId id)
{
	ResourceList::iterator curr = m_ResourceList.begin();
	ResourceList::const_iterator end = m_ResourceList.end();

	scriptResource* resource = NULL;

	for(; curr != end; ++curr)
	{
		resource = *curr;

		if (resource->GetId() == id)
		{
			return resource;	
		}
	}

	return NULL;
}

scriptResource* scriptHandler::GetScriptResource(ScriptResourceType type, const ScriptResourceRef ref)
{
	ResourceList::iterator curr = m_ResourceList.begin();
	ResourceList::const_iterator end = m_ResourceList.end();

	scriptResource* resource = NULL;

	for(; curr != end; ++curr)
	{
		resource = *curr;

		if (resource->GetType() == type && resource->GetReference() == ref)
		{
			return resource;	
		}
	}

	return NULL;
}

ScriptResourceRef scriptHandler::GetResourceReferenceFromId(ScriptResourceId id)
{
	scriptResource* resource = GetScriptResource(id);

	if (gnetVerifyf(resource, "Script resource with given id does not exist"))
	{
		return resource->GetReference();
	}

	return -1;
}

unsigned scriptHandler::GetNumScriptResourcesOfType(ScriptResourceType type) const
{
	unsigned numResources = 0;

	ResourceList::const_iterator curr = m_ResourceList.begin();
	ResourceList::const_iterator end = m_ResourceList.end();

	const scriptResource* resource = NULL;
	for(; curr != end; ++curr)
	{
		resource = *curr;

		if (resource->GetType() == type)
		{
			numResources++;	
		}
	}

	return numResources;
}

void scriptHandler::ForAllScriptResources(Callback cb, void* data) const
{
    ResourceList::const_iterator curr = m_ResourceList.begin();
    ResourceList::const_iterator end = m_ResourceList.end();

    const scriptResource* resource = NULL;
    for(; curr != end; ++curr)
    {
        resource = *curr;

        if(!cb(resource, data))
            return;
    }
}

ScriptObjectId scriptHandler::AllocateNewHostObjectId()
{
	++m_NextFreeHostObjectId;

	if (m_NextFreeHostObjectId == (1<<SIZEOF_HANDLER_OBJECT_ID))
	{
		m_NextFreeHostObjectId = 0;
	}

	ScriptObjectId newId = MAKE_SCRIPT_OBJECT_ID(SCRIPT_OBJECT_HOST_SLOT_INDEX, m_NextFreeHostObjectId);
	Assert(newId != INVALID_SCRIPT_OBJECT_ID);

	// inform host broadcast array handler that the next free id has changed so it is sent out with any broadcast data updates. Any
	// subsequent hosts will create any new script objects using a higher id
	if (m_NetComponent)
	{
		for (s32 i=0; i<m_NetComponent->GetNumHostBroadcastDatasRegistered(); i++)
		{
			netHostBroadcastDataHandlerBase* pHostBroadcastHandler = m_NetComponent->GetHostBroadcastDataHandler(i);

			if (pHostBroadcastHandler)
			{
				pHostBroadcastHandler->SetNextFreeHostObjectId(m_NextFreeHostObjectId);
			}
		}
	}

	return newId;
}


bool scriptHandler::AreAllParticipantsUpToDateWithHostState(bool bLogFailure) const
{
	bool upToDate = true;

	if (m_NetComponent)
	{
		if (!static_cast<netArrayManager_Script&>(netInterface::GetArrayManager()).IsAllHostBroadcastDataSynced(GetScriptId()))
		{
			if (bLogFailure)
			{
				LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("Reason", "Broadcast data not synced"));
			}
			upToDate = false;
		}
		else
		{
			const atDNode<scriptHandlerObject*, datBase> *node = m_ObjectList.GetHead();

			while (node)
			{
				scriptHandlerObject* scrObj = node->Data;
				netObject* netObj = node->Data->GetNetObject();

				if (scrObj->IsHostObject() && netObj && !netObj->IsClone())
				{
					PlayerFlags participantFlags = m_NetComponent->GetPlayerParticipants();
					PlayerFlags clonedState = netObj->GetClonedState();

					participantFlags &= ~(1<<netInterface::GetLocalPhysicalPlayerIndex());

					clonedState &= participantFlags;

					if (clonedState != participantFlags)
					{
						if (bLogFailure)
						{
							LOGGING_ONLY(scriptInterface::GetScriptManager().GetLog()->WriteDataValue("Reason", "%s not cloned (participants:%d, cloned state:%d)", netObj->GetLogName(), participantFlags, clonedState));
						}
						upToDate = false;
						break;
					}
				}

				node = node->GetNext();
			}		
		}
	}

	return upToDate;
}

scriptHandler::atDScriptObjectNode *scriptHandler::CreateNewScriptObjectNode()
{
	if (atDScriptObjectNode::GetPool()->GetNoOfFreeSpaces() == 0)
	{
		DEV_ONLY(scriptInterface::GetScriptManager().SpewObjectAndResourceInfo());
		NETWORK_QUITF(0, "scriptHandler::CreateNewScriptObjectNode - all %d slots in the atDScriptObjectNode pool are full. Check the list above to see if scripts are holding on to entities that they don't need. Otherwise increase the value in common/data/gameconfig.xml", atDScriptObjectNode::GetPool()->GetSize());

		return NULL;
	}

	return rage_new atDScriptObjectNode;
}


void scriptHandler::RegisterExistingScriptObjectInternal(scriptHandlerObject &object)
{
	if(gnetVerifyf(object.GetScriptInfo(), "%s : Trying to register a non script object as an existing script object", GetLogName()) &&
	   gnetVerifyf(!object.GetScriptHandler(), "%s : Trying to register an existing script object (%d) that is already registered with a handler", GetLogName(), object.GetScriptInfo()->GetObjectId()))
	{
		gnetAssertf(!GetScriptObject(object.GetScriptInfo()->GetObjectId()), "%s: RegisterExistingScriptObject - An object already exists with this id (%d)", GetLogName(), object.GetScriptInfo()->GetObjectId());
		gnetAssertf(object.GetScriptInfo()->GetScriptId() == GetScriptId(), "%s: RegisterExistingScriptObject - This object belongs to another script (%d)", GetLogName(), object.GetScriptInfo()->GetObjectId());

		atDNode<scriptHandlerObject*, datBase> *pNewNode = CreateNewScriptObjectNode();

		if (gnetVerifyf(pNewNode, "Ran out of atDScriptObjectNodes!"))
		{
			pNewNode->Data = &object;
			m_ObjectList.Append(*pNewNode);

			object.SetScriptHandler(this);
			object.OnRegistration(false, object.IsHostObject());

			if (object.IsUnnetworkedObject())
			{
				ScriptObjectId objectId = GET_INDEX_FROM_SCRIPT_OBJECT_ID(object.GetScriptInfo()->GetObjectId());

				if (objectId > m_NextFreeNonNetworkedObjectId)
				{
					m_NextFreeNonNetworkedObjectId = objectId;
				}
			}
		}
	}
}

void scriptHandler::DetachScriptObject(scriptHandlerObject &object)
{
	bool cleanup = object.IsCleanupRequired();

	// detach the object from the handler
	object.ClearScriptHandler();

	// Determine whether the object needs to be cleaned up properly. This is not done if the object is still required to persist as a script object
	// for the remaining participants, who may still be running the script normally.
	if (cleanup)
	{
		CleanupScriptObject(object);
	}
}

void scriptHandler::CleanupScriptObject(scriptHandlerObject &object)
{
	object.Cleanup();
}

bool scriptHandler::DestroyScriptResource(scriptResource& resource)
{
	bool bDestroy = true;

	if (resource.LeaveForOtherScripts())
	{
		// search other handler lists to see if any other handlers are still referencing this resource. The resource is left to persist it so.
		if (scriptInterface::GetScriptManager().GetScriptResource(resource.GetType(), resource.GetReference(), this))
		{
			bDestroy = false;
		}
	}

	if (bDestroy)
	{
		resource.Destroy();
	}

	return bDestroy;
}

void scriptHandler::RemoveOldScriptObject(scriptHandlerObject &object)
{
	UnregisterScriptObject(object);
}

} // namespace rage
