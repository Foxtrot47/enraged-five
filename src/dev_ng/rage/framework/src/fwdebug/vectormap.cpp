/////////////////////////////////////////////////////////////////////////////////
// FILE :    vectormap.cpmovementPortionOfCollision
// PURPOSE : A debug map that can be viewed on top of the game
// AUTHOR(s) :  Obbe, Adam Croston
// CREATED : 31/08/06
/////////////////////////////////////////////////////////////////////////////////
#include "vectormap.h"

// Rage headers
#include "bank/bank.h"
#include "grcore/device.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mapper.h"
#include "math/vecMath.h"
#include "spatialdata/aabb.h"
#include "vector/colors.h"

// Framework headers
#include "grcore/debugdraw.h"
#include "fwmaths/angle.h"
#include "fwmaths/random.h"
#include "fwmaths/rect.h"
#include "fwsys/timer.h"


#if __BANK

namespace rage {

fwVectorMap *fwVectorMap::ms_Instance;

static Vector2 s_WorldLimitMin(-8192.0f, -8192.0f);
static Vector2 s_WorldLimitMax( 8192.0f,  8192.0f);

static float s_ScreenspaceMinThreshold = 2.0f;

// Default these values to a 720p screen - they will be initialized properly
// in code using the actual resolution.
static float s_ComputedXThreshold = 2.0f / 1280.0f;
static float s_ComputedYThreshold = 2.0f / 720.0f;

PARAM(displayvectormap, "Enable display of the vector map");

void fwVectorMap::CreateDefaultSingleton()
{
	if (!ms_Instance)
	{
		ms_Instance = rage_new fwVectorMap();
	}
}


//////////////////////////////////////////////////////////////////////////////////
// fwVectorMap
//////////////////////////////////////////////////////////////////////////////////
bool	fwVectorMap::m_bAllowDisplayOfMap				= true;
bool	fwVectorMap::m_bAllowDisplayOfTextWithMap		= true;

float	fwVectorMap::m_FocusWorldX						= 0.0f;
float	fwVectorMap::m_FocusWorldY						= 0.0f;
float	fwVectorMap::m_MapRotationRads					= 0.0f;
float	fwVectorMap::m_Zoom								= 30.0f;		// 1.0f is standard. > 1.0f means zoomed in.

bool	fwVectorMap::m_bMapShouldBeDrawnThisFrame		= false;


/** PURPOSE: Initialize the vector map.
 */
void fwVectorMap::Init()
{
}

/** PURPOSE: Destroy the vector map object.
 */
void fwVectorMap::Shutdown()
{
}

/** PURPOSE: This is the internal virtual update function.
 *  Users should call fwVectorMap::Update, which will internally call this function.
 *  The base implementation handles the core functionality, such as moving/zooming the map,
 *  and calls a few draw functions.
 */
void fwVectorMap::UpdateCore()
{
	// Possibly modify the pan and zoom values
	if (ioMapper::DebugKeyPressed(KEY_1))
	{
		ZoomIn();
	}
	if (ioMapper::DebugKeyPressed(KEY_2))
	{
		ZoomOut();
	}
	if (ioMapper::DebugKeyPressed(KEY_3))
	{
		ShiftLeft();
	}
	if (ioMapper::DebugKeyPressed(KEY_4))
	{
		ShiftRight();
	}
	if (ioMapper::DebugKeyPressed(KEY_5))
	{
		ShiftUp();
	}
	if (ioMapper::DebugKeyPressed(KEY_6))
	{
		ShiftDown();
	}
	// Possibly toggle text display.
	if (ioMapper::DebugKeyPressed(KEY_7))
	{
		m_bAllowDisplayOfTextWithMap = !m_bAllowDisplayOfTextWithMap;
	}

	UpdateFocus();
	UpdateRotation();

	// The draw function really only calls a few fwDebugDraw functions (which are
	// cached calls themselves).
	Draw();
}

/** PURPOSE: This is a virtual function that will update the focus (i.e. center
 *  point) of the vector map. By default, it doesn't do anything, but subclasses
 *  can do awesome things here, like following the player.
 */
void fwVectorMap::UpdateFocus()
{
	// Don't do anything - keep the previous focus.
}

/** PURPOSE: This is a virtual function that will update the rotation of the map.
 *  By default, the map will always rotate itself to north-up, but subclasses
 *  can go nuts here and do things like rotating with the player.
 */
void fwVectorMap::UpdateRotation()
{
	m_MapRotationRads = 0.0f;
}

/** PURPOSE: This is a virtual function that handles some drawing functionality
 *  of the map. Users shouldn't call this directly, this function is called by
 *  fwVectorMap::Draw().
 */
void fwVectorMap::DrawCore()
{
	m_bMapShouldBeDrawnThisFrame = false;
}

/** PURPOSE: This is a helper function that will draw a line on the vector map.
 *  This function will perform basic culling and avoid drawing the line if it is
 *  off-screen.
 *
 *  PARAMS:
 *   v1 - Starting position of the line in 3D world coordinates.
 *   v2 - Ending position of the line in 3D world coordinates.
 *   col1 - Color of the line at v1.
 *   col2 - Color of the line at v2.
 *   bForceMap - Force drawing this line, even if the map is turned off.
 */
void fwVectorMap::DrawLine(const Vector3& v1, const Vector3& v2, Color32 col1, Color32 col2, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	float startMapX, startMapY, endMapX, endMapY;
	ConvertPointWorldToMap(v1.x, v1.y, startMapX, startMapY);
	ConvertPointWorldToMap(v2.x, v2.y, endMapX, endMapY);

	Vector2 startPos(startMapX, startMapY);
	Vector2 endPos(endMapX,endMapY);

	if (!IsBoundingBoxCulled(startPos, endPos)) {
		grcDebugDraw::Line(startPos, endPos, col1, col2);
	}
}

/** PURPOSE: This is a helper function that will draw a thick line on the vector map.
 *  This function will perform basic culling and avoid drawing the line if it is
 *  off-screen.
 *
 *  PARAMS:
 *   v1 - Starting position of the line in 3D world coordinates.
 *   v2 - Ending position of the line in 3D world coordinates.
 *   col1 - Color of the line at v1.
 *   col2 - Color of the line at v2.
 *   bForceMap - Force drawing this line, even if the map is turned off.
 */
void fwVectorMap::DrawLineThick(const Vector3& v1, const Vector3& v2, Color32 col1, Color32 col2, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	float startMapX, startMapY, endMapX, endMapY;
	ConvertPointWorldToMap(v1.x, v1.y, startMapX, startMapY);
	ConvertPointWorldToMap(v2.x, v2.y, endMapX, endMapY);

	// Nearly arbitrary offsets- just used to make sure the extra lines are at
	// least a pixel away from the center lines.
	const float unitX = 1.0f/320.0f;
	const float unitY = 1.0f/240.0f;

	if (!IsBoundingBoxCulled(Vector2(startMapX, startMapY), Vector2(endMapX, endMapY))) {
		grcDebugDraw::Line(Vector2(startMapX,			startMapY),			Vector2(endMapX,		endMapY),		col1, col2);
		grcDebugDraw::Line(Vector2(startMapX,			startMapY+unitY),	Vector2(endMapX,		endMapY+unitY),	col1, col2);
		grcDebugDraw::Line(Vector2(startMapX,			startMapY-unitY),	Vector2(endMapX,		endMapY-unitY),	col1, col2);
		grcDebugDraw::Line(Vector2(startMapX+unitX,	startMapY),			Vector2(endMapX+unitX,	endMapY),		col1, col2);
		grcDebugDraw::Line(Vector2(startMapX-unitX,	startMapY),			Vector2(endMapX-unitX,	endMapY),		col1, col2);
	}
}

/** PURPOSE: This is a helper function that will draw a line on the vector map.
 *  This function will perform basic culling and avoid drawing the line if it is
 *  off-screen.
 *
 *  PARAMS:
 *   v1 - Corner 1 of the quad in 3D world coordinates.
 *   v2 - Corner 2 of the quad in 3D world coordinates.
 *   v3 - Corner 3 of the quad in 3D world coordinates.
 *   v4 - Corner 4 of the quad in 3D world coordinates.
 *   color - Color of the quad.
 *   bForceMap - Force drawing this line, even if the map is turned off.
 */
void fwVectorMap::DrawQuad(const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, Color32 color, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	float v1x, v1y, v2x, v2y, v3x, v3y, v4x, v4y;
	ConvertPointWorldToMap(v1.x, v1.y, v1x, v1y);
	ConvertPointWorldToMap(v2.x, v2.y, v2x, v2y);
	ConvertPointWorldToMap(v3.x, v3.y, v3x, v3y);
	ConvertPointWorldToMap(v4.x, v4.y, v4x, v4y);

	Vector2 mapV1(v1x, v1y);
	Vector2 mapV2(v2x, v2y);
	Vector2 mapV3(v3x, v3y);
	Vector2 mapV4(v4x, v4y);

	Vector2 minPos(Min(v1x, v2x, v3x, v4x), Min(v1y, v2y, v3y, v4y));
	Vector2 maxPos(Max(v1x, v2x, v3x, v4x), Max(v1y, v2y, v3y, v4y));

	if (!IsBoundingBoxCulled(minPos, maxPos)) {
		grcDebugDraw::Quad(mapV1, mapV2, mapV3, mapV4, color, false);
	}
}

/** PURPOSE: This is a helper function that will draw a triangle on the vector map.
 *  This function will perform basic culling and avoid drawing the line if it is
 *  off-screen.
 *
 *  PARAMS:
 *   a - Corner 1 of the triangle in 3D world coordinates.
 *   b - Corner 2 of the triangle in 3D world coordinates.
 *   c - Corner 3 of the triangle in 3D world coordinates.
 *   col - Color of the triangle.
 *   solid - If true, this will be a solid triangle, wireframe otherwise.
 *   bForceMap - Force drawing this line, even if the map is turned off.
 */
void fwVectorMap::DrawPoly(const Vector3& a, const Vector3& b, const Vector3& c, Color32 col, bool solid, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapA;
	ConvertPointWorldToMap(a, mapA);
	Vector2 mapB;
	ConvertPointWorldToMap(b, mapB);
	Vector2 mapC;
	ConvertPointWorldToMap(c, mapC);

	Vector2 minPos(Min(mapA.x, mapB.x, mapC.x), Min(mapA.y, mapB.y, mapC.y));
	Vector2 maxPos(Max(mapA.x, mapB.x, mapC.x), Max(mapA.y, mapB.y, mapC.y));

	if (!IsBoundingBoxCulled(minPos, maxPos)) {
		grcDebugDraw::Poly(mapA, mapB, mapC, col, solid);
	}
}


/** PURPOSE: This is a helper function that will draw a triangle on the vector map.
 *  This function will perform basic culling and avoid drawing the line if it is
 *  off-screen.
 *
 *  PARAMS:
 *   a - Corner 1 of the triangle in 3D world coordinates.
 *   b - Corner 2 of the triangle in 3D world coordinates.
 *   c - Corner 3 of the triangle in 3D world coordinates.
 *   col1 - Color of the triangle at a.
 *   col1 - Color of the triangle at b.
 *   col1 - Color of the triangle at c.
 *   solid - If true, this will be a solid triangle, wireframe otherwise.
 *   bForceMap - Force drawing this line, even if the map is turned off.
 */
void fwVectorMap::DrawPoly(const Vector3& a, const Vector3& b, const Vector3& c, Color32 col1, Color32 col2, Color32 col3, bool solid, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapA;
	ConvertPointWorldToMap(a, mapA);
	Vector2 mapB;
	ConvertPointWorldToMap(b, mapB);
	Vector2 mapC;
	ConvertPointWorldToMap(c, mapC);
	
	Vector2 minPos(Min(mapA.x, mapB.x, mapC.x), Min(mapA.y, mapB.y, mapC.y));
	Vector2 maxPos(Max(mapA.x, mapB.x, mapC.x), Max(mapA.y, mapB.y, mapC.y));

	if (!IsBoundingBoxCulled(minPos, maxPos)) {
		grcDebugDraw::Poly(mapA, mapB, mapC, col1, col2, col3, solid);
	}
}

/** PURPOSE: Draw a cross on the map.
 *  PARAMS:
 *    center - Center position of the cross in world coordinates.
 *    col - Color of the cross.
 *    size - Size of the cross (in world units).
 *    bForceMap - Force drawing this cross, even if the map is turned off.
 */
void fwVectorMap::DrawMarker(const Vector3& center, Color32 col, float size, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapCenter;
	ConvertPointWorldToMap(center, mapCenter);
	float mapSize = 0.0f;
	ConvertSizeWorldToMap(size, mapSize);

	grcDebugDraw::Cross(mapCenter, mapSize, col);
}

/** PURPOSE: Draw a circle on the map.
 *  PARAMS:
 *    center - Center position of the circle in world coordinates.
 *    radius - Radius of the circle in world units.
 *    col - Color of the circle.
 *    solid - If true, this is a solid circle.
 *    bForceMap - Force drawing this circle, even if the map is turned off.
 */
void fwVectorMap::DrawCircle(const Vector3& center, float radius, Color32 col, bool solid, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapCenter;
	ConvertPointWorldToMap(center, mapCenter);
	float mapRadius = 0.0f;
	ConvertSizeWorldToMap(radius, mapRadius);

	grcDebugDraw::Circle(mapCenter, mapRadius, col, solid);
}

/** PURPOSE: Draw an arc on the map.
 *  PARAMS:
 *    center - Center position of the circle in world coordinates.
 *    radius - Radius of the circle in world units.
 *	  startTheta - Start theta of arc
 *	  endTheta - End theta of arc
 *    col - Color of the circle.
 *    solid - If true, this is a solid circle.
 *    bForceMap - Force drawing this circle, even if the map is turned off.
 */
void fwVectorMap::DrawArc(const Vector3& center, float radius, float startTheta, float endTheta, Color32 col, bool solid, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapCenter;
	ConvertPointWorldToMap(center, mapCenter);
	float mapRadius = 0.0f;
	ConvertSizeWorldToMap(radius, mapRadius);

	grcDebugDraw::Arc(mapCenter, mapRadius, col, startTheta, endTheta, solid, 20);
}


/** PURPOSE: Draw an axis-aligned rectangle on the map.
 *  PARAMS:
 *    min - Top-left coordinates of rectangle in world coordinates.
 *    max - Bottom-right coordinates of rectangle in world coordinates.
 *    col - Color of the rectangle.
 *    solid - If true, this is a solid rectangle.
 *    bForceMap - Force drawing this rectangle, even if the map is turned off.
 */
void fwVectorMap::DrawRectAxisAligned(const Vector3& min, const Vector3& max, Color32 col, bool solid, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapTrans(0.0f, 0.0f);
	{
		Vector3 worldOrigin(0.0f, 0.0f, 0.0f);
		ConvertPointWorldToMap(worldOrigin, mapTrans);
	}

	Vector2 mapXBasis(0.0f, 0.0f);
	{
		Vector3 worldXBasis(1.0f, 0.0f, 0.0f);
		ConvertPointWorldToMap(worldXBasis, mapXBasis);
		mapXBasis -= mapTrans;
	}

	Vector2 mapYBasis(0.0f, 0.0f);
	{
		Vector3 worldYBasis(1.0f, 0.0f, 0.0f);
		ConvertPointWorldToMap(worldYBasis, mapYBasis);
		mapYBasis -= mapTrans;
	}

	Vector2 localMin(min.x, min.y);
	Vector2 localMax(max.x, max.y);
	grcDebugDraw::RectOriented(localMin, localMax, mapXBasis, mapYBasis, mapTrans, col, solid);
}


/** PURPOSE: Draw a circular pie wedge on the map.
 *  PARAMS:
 *    center - Center position of the wedge.
 *    radiusInner - Inner radius of the wedge.
 *    radiusOuter - Outer radius of the wedge.
 *    thetaStart - Starting theta of the wedge.
 *    thetaEnd - Ending theta of the wedge.
 *    numSegments - Number of segments for the rounded part of the wedge.
 *    col - Color of the wedge.
 *    bForceMap - Force drawing this wedge, even if the map is turned off.
 */
void fwVectorMap::DrawWedge(	const Vector3& center,
							float	radiusInner,
							float	radiusOuter,
							float	thetaStart,
							float	thetaEnd,
							s32	numSegments,
							Color32	col,
							bool	bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	// Draw the wedge.
	const float		thetaStep	= (thetaEnd - thetaStart) / static_cast<float>(numSegments);
	for(int i = 0; i < numSegments; ++i)
	{
		const float		theta0	= thetaStart + static_cast<float>(i) * thetaStep;
		const float		theta1	=  theta0 + thetaStep;

		const float		st0 = rage::Sinf(theta0);
		const float		ct0 = rage::Cosf(theta0);
		const float		st1 = rage::Sinf(theta1);
		const float		ct1 = rage::Cosf(theta1);

		const float		r0x0	= -radiusInner * st0;
		const float		r0y0	= radiusInner * ct0;
		const Vector3	r0p0	(center.x + r0x0, center.y + r0y0, 0.0f);

		const float		r0x1	= -radiusInner * st1;
		const float		r0y1	= radiusInner * ct1;
		const Vector3	r0p1	(center.x + r0x1, center.y + r0y1, 0.0f);

		const float		r1x0	= -radiusOuter * st0;
		const float		r1y0	= radiusOuter * ct0;
		const Vector3	r1p0	(center.x + r1x0, center.y + r1y0, 0.0f);

		const float		r1x1	= -radiusOuter * st1;
		const float		r1y1	= radiusOuter * ct1;
		const Vector3	r1p1	(center.x + r1x1, center.y + r1y1, 0.0f);

		DrawPoly(r0p0, r0p1, r1p0, col, bForceMap);
		DrawPoly(r1p0, r0p1, r1p1, col, bForceMap);
	}
}


/** PURPOSE: Draws a simple triangle to indicate a remote camera.
 *  PARAMS:
 *    camMatrix - Matrix of the camera to render.
 *    farClip - Far clipping plane of the camera.
 *    tanHFov - FOV of the camera.
 *    col - Color for the camera symbol.
 *    bForceMap - Force drawing this camera, even if the map is turned off.
 */
void fwVectorMap::DrawRemotePlayerCamera(const Matrix34 &camMatrix, float farClip, float tanHFov, Color32 col, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

    // this camera matrix is in the form specified from a grcViewport object (where the b Vector is up)
    const Vector3	vRight		= camMatrix.a;
	const Vector3	vForward	= -camMatrix.c;
	const Vector3	vPos		= camMatrix.d;
	const float		farPlane	= farClip;

	const Vector3	vFarPlaneRight	= vPos + (vForward * farPlane) + (vRight * tanHFov * farPlane);
	const Vector3	vFarPlaneLeft	= vPos + (vForward * farPlane) - (vRight * tanHFov * farPlane);
	DrawLine(vPos, vFarPlaneRight, col, col, false);
	DrawLine(vPos, vFarPlaneLeft, col, col, false);
	DrawLine(vFarPlaneRight, vFarPlaneLeft, col, col, false);
}

/** PURPOSE: Draws two enclosed arcs that encompass the visible view and close distance of the remote player (the area that would return true
 *			 for IsCloseToOrVisibleToRemotePlayer
 *  PARAMS:
 *    camMatrix - Matrix of the camera to render.
 *    farClip - Close radius around player that point doesn't need to be visible.
 *    farClip - Far radius around player that point needs to be visible.
 *    tanHFov - FOV of the camera.
 *    col - Color for the camera symbol.
 *	  fArcOffset - the local players camera angle
 *    bForceMap - Force drawing this camera, even if the map is turned off.
 */
void fwVectorMap::DrawRemotePlayerCameraCloseVisibleBounds(const Matrix34 &camMatrix, float nearClip, float farClip, float tanHFov, Color32 col, float fArcOffset, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bMapShouldBeDrawnThisFrame){return;}

	// this camera matrix is in the form specified from a grcViewport object (where the b Vector is up)
	const Vector3	vRight		= camMatrix.a;
	const Vector3	vForward	= -camMatrix.c;
	const Vector3	vPos		= camMatrix.d;

	Vector3	toRightPoint 	= vForward + (vRight * tanHFov);
	toRightPoint.Normalize();

	Vector3	toLeftPoint		= vForward - (vRight * tanHFov);
	toLeftPoint.Normalize();

	DrawLine(vPos + toRightPoint * nearClip, vPos + toRightPoint * farClip, col, col, false);
	DrawLine(vPos + toLeftPoint * nearClip, vPos + toLeftPoint * farClip, col, col, false);

	float thetaEnd = fwAngle::LimitRadianAngle( rage::Atan2f(toRightPoint.x, toRightPoint.y) + fArcOffset );
	float thetaStart = fwAngle::LimitRadianAngle( rage::Atan2f(toLeftPoint.x, toLeftPoint.y) + fArcOffset );
	DrawArc(vPos, farClip, thetaStart, thetaEnd, col);

	DrawCircle(vPos, 2.0f, Color_white, true);	
	DrawArc(vPos, nearClip, thetaEnd, thetaStart, col, false);
}

/** PURPOSE: Draws a string on the map.
 *  PARAMS:
 *    pos - Position of the string in world coordinates (top left corner of the string).
 *    String - String to render.
 *    col - Color of the string to render.
 *    drawBGQuad - If true, there will be a background quad behind the text.
 *    bForceMap - Force drawing this string, even if the map is turned off.
 */
void fwVectorMap::DrawString(const Vector3& pos, const char *String, Color32 col, bool drawBGQuad, bool bForceMap)
{
	m_bMapShouldBeDrawnThisFrame = m_bMapShouldBeDrawnThisFrame || bForceMap;		// Force the map itself to be drawn
	if(!m_bAllowDisplayOfMap || !m_bAllowDisplayOfTextWithMap || !m_bMapShouldBeDrawnThisFrame){return;}

	Vector2 mapPos;
	ConvertPointWorldToMap(pos, mapPos);

	grcDebugDraw::Text(mapPos, col, String, drawBGQuad);
}

void fwVectorMap::Draw3DBoundingBoxOnVMap(const spdAABB& bb, Color32 color)
{
	Draw2DBoundingBoxOnVMap(fwRect(bb), color);
}

void fwVectorMap::Draw2DBoundingBoxOnVMap(const fwRect& bb, Color32 colour)
{
	Vector3 p1(bb.left, bb.top, 0.0f);
	Vector3 p2(bb.right, bb.top, 0.0f);
	Vector3 p3(bb.right, bb.bottom, 0.0f);
	Vector3 p4(bb.left, bb.bottom, 0.0f);

	DrawQuad(p1, p2, p3, p4, colour);
}




static float WorldXSpan = s_WorldLimitMax.x - s_WorldLimitMin.x;
static float WorldYSpan = s_WorldLimitMax.y - s_WorldLimitMin.y;

/** PURPOSE: Convert a scalar value from world units to screen coordinates.
 *  PARAMS:
 *    worldSize - Scalar value in world units.
 *    mapSize - This variable will be set to the converted value in screen units.
 */
void fwVectorMap::ConvertSizeWorldToMap(float worldSize, float &mapSize)
{
	mapSize = (worldSize * m_Zoom) / WorldXSpan;
}

/** PURPOSE: Convert a scalar value from screen units to world units.
 *  PARAMS:
 *    mapSize - Scalar value in screen units.
 *    worldSize - This variable will be set to the converted value in world units.
 */
void fwVectorMap::ConvertSizeMapToWorld(float mapSize, float& worldSize)
{
	worldSize = (mapSize * WorldXSpan) / m_Zoom;
}

/** PURPOSE: Convert a 2D coordinate from world units to screen units.
 *  This includes the current translation and rotation of the map.
 *  PARAMS:
 *    worldX - X position in world units to convert.
 *    worldY - Y position in world units to convert.
 *    mapX - This variable will be set to the converted X value in screen units.
 *    mapY - This variable will be set to the converted Y value in screen units.
 */
void fwVectorMap::ConvertPointWorldToMap(float worldX, float worldY, float &mapX, float &mapY)
{
	const float tX = worldX - m_FocusWorldX;
	const float tY = worldY - m_FocusWorldY;

	const float ztX = tX * m_Zoom;
	const float ztY = tY * m_Zoom;

	const float sinTheta = rage::Sinf(m_MapRotationRads);
	const float cosTheta = rage::Cosf(m_MapRotationRads);

	const float rztX = (ztX * cosTheta) - (ztY * sinTheta);
	const float rztY = (ztY * cosTheta) + (ztX * sinTheta);

	const float mapPortionX = ( rztX + (0.5f*WorldXSpan)) / WorldXSpan;
	const float mapPortionY = (-rztY + (0.5f*WorldYSpan)) / WorldYSpan;

	static const Vector2 aspect = grcDebugDraw::Get2DAspect();
	mapX = (mapPortionX / aspect.y) + (0.5f - (0.5f / aspect.y));
	mapY = mapPortionY;
}

/** PURPOSE: Convert a 2D coordinate from screen units to world units.
*  This includes the current translation and rotation of the map.
 *  PARAMS:
 *    mapX - X position in screen units to convert.
 *    mapY - Y position in screen units to convert.
 *    worldX - This variable will be set to the converted X value in world units.
 *    worldY - This variable will be set to the converted Y value in world units.
 */
void fwVectorMap::ConvertPointMapToWorld(float mapX, float mapY, float& worldX, float& worldY)
{
	static const Vector2 aspect = grcDebugDraw::Get2DAspect();
	const float mapPortionX = (mapX - (0.5f - (0.5f / aspect.y))) * aspect.y;
	const float mapPortionY = mapY;

	const float rztX =   (mapPortionX * WorldXSpan) - (0.5f*WorldXSpan);
	const float rztY = -((mapPortionY * WorldYSpan) - (0.5f*WorldYSpan));

	const float sinTheta = rage::Sinf(-m_MapRotationRads);
	const float cosTheta = rage::Cosf(-m_MapRotationRads);

	const float ztX = (rztX * cosTheta) - (rztY * sinTheta);
	const float ztY = (rztY * cosTheta) + (rztX * sinTheta);

	const float tX = ztX / m_Zoom;
	const float tY = ztY / m_Zoom;

	worldX = tX + m_FocusWorldX;
	worldY = tY + m_FocusWorldY;
}

/** PURPOSE: Convert a 3D coordinate from world units to screen units.
 *  This includes the current translation and rotation of the map.
 *  (Note that the Z component of the 3D coordinate is ignored)
 *  PARAMS:
 *    worldPos - Position in world coordinates to convert.
 *    mapPos - This variable will be set to the converted 2D coordinate in screen units.
 */
void fwVectorMap::ConvertPointWorldToMap(const Vector3& worldPos, Vector2& mapPos)
{
	ConvertPointWorldToMap(worldPos.x, worldPos.y, mapPos.x, mapPos.y);
}

/** PURPOSE: Convert a 2D coordinate from screen units to world units.
 *  This includes the current translation and rotation of the map.
 *  (Note that the Z component of the resulting 3D coordinate will always be 0.0f)
 *  PARAMS:
 *    mapPos - Position in screen coordinates to convert.
 *    WorldPos - This variable will be set to the converted 3D coordinate in world units,
 *               the Z component being 0.0f.
 */
void fwVectorMap::ConvertPointMapToWorld(const Vector2& mapPos, Vector3& worldPos)
{
	ConvertPointMapToWorld(mapPos.x, mapPos.y, worldPos.x, worldPos.y);
	worldPos.z = 0.0f;
}

/** PURPOSE: Set up the dimensions of the vector map.
 *  This should be called once during initialization, although it is legal to change
 *  the dimensions at any time during run-time (although it will cause the map
 *  to re-scale).
 */
void fwVectorMap::SetWorldDimensions(const Vector2 &min, const Vector2 &max)
{
	s_WorldLimitMin = min;
	s_WorldLimitMax = max;

	WorldXSpan = s_WorldLimitMax.x - s_WorldLimitMin.x;
	WorldYSpan = s_WorldLimitMax.y - s_WorldLimitMin.y;
}

/** RETURNS: True if the given bounding box is either completely off-screen, or
 *  so small that it falls beneath the screenspace threshold. Note that pos1 and
 *  pos2 do not need to be sorted, the function will automatically gather the
 *  minimum and maximum values.
 *
 * PARAMS:
 *  pos1 - Position 1 of the bounding box in screen units.
 *  pos2 - Position 2 of the bounding box in screen units.
 */
bool fwVectorMap::IsBoundingBoxCulled(const Vector2 &pos1, const Vector2 &pos2)
{
	float minX = Min(pos1.x, pos2.x);
	float minY = Min(pos1.y, pos2.y);
	float maxX = Max(pos1.x, pos2.x);
	float maxY = Max(pos1.y, pos2.y);

	float xDiff = maxX - minX;
	float yDiff = maxY - minY;

	// Apply the screenspace threshold.
	if (xDiff < s_ComputedXThreshold && yDiff < s_ComputedYThreshold) {
		return true;
	}

	// Perform a simple guard band test.
	if (maxY < 0.0f || maxX < 0.0f || minX > 1.0f || minY > 1.0f) {
		return true;
	}

	return false;
}

/** PURPOSE: Compute the internal values that help perform a quick screenspace
 *  threshold test. This function needs to be called once during initialization, and
 *  every time the screenspace threshold value or screen resolution is changed.
 */
void fwVectorMap::RecomputeScreenspaceThresholds()
{
	s_ComputedXThreshold = s_ScreenspaceMinThreshold / (float) GRCDEVICE.GetWidth();
	s_ComputedYThreshold = s_ScreenspaceMinThreshold / (float) GRCDEVICE.GetHeight();
}

/** PURPOSE: Calling this function will force the map to be rendered this frame,
 *  even if there are no lines or anything else scheduled to be rendered.
 */
void fwVectorMap::ForceMapDisplay()
{
	m_bMapShouldBeDrawnThisFrame = true;
}

/** PURPOSE: This function will set all internal values to the default settings.
 *  Note that this function is virtual.
 */
void fwVectorMap::CleanSettingsCore()
{
	m_Zoom = 1.0f;
	m_FocusWorldX = 0.0f;
	m_FocusWorldY = 0.0f;
	m_MapRotationRads = 0.0f;
}


/** PURPOSE: This function will set all internal values to the decent settings
 *  that have been proven useful for debugging.
 *  Note that this function is virtual.
 */
void fwVectorMap::GoodSettingsCore()
{
	m_Zoom = 30.0f;
}

/** PURPOSE: Zoom in to the specified level
 */
void fwVectorMap::SetZoom(float zoom)
{
    m_Zoom = zoom;
}

/** PURPOSE: Slightly zoom in.
 */
void fwVectorMap::ZoomIn()
{
	// Use different zooming level steps when zooming in past 1.0.
	if(m_Zoom >= 1.0f)
	{
		m_Zoom *= 1.5f;
	}
	else
	{
		m_Zoom *= 2.0f;
	}
}

/** PURPOSE: Slightly zoom out.
 */
void fwVectorMap::ZoomOut()
{
	// Use different zooming level steps when zooming out past 1.0.
	if(m_Zoom >= 1.5f)
	{
		m_Zoom /= 1.5f;
	}
	else
	{
		m_Zoom /= 2.0f;
	}
	m_Zoom = rage::Max(0.25f, m_Zoom);
}

/** PURPOSE: Slightly pan to the left.
 */
void fwVectorMap::ShiftLeft()
{
	m_FocusWorldX += 300.0f / m_Zoom;
}

/** PURPOSE: Slightly pan to the right.
 */
void fwVectorMap::ShiftRight()
{
	m_FocusWorldX -= 300.0f / m_Zoom;
}

/** PURPOSE: Slightly pan up.
 */
void fwVectorMap::ShiftUp()
{
	m_FocusWorldY -= 300.0f / m_Zoom;
}

/** PURPOSE: Slightly pan down.
 */
void fwVectorMap::ShiftDown()
{
	m_FocusWorldY += 300.0f / m_Zoom;
}

/** PURPOSE: Add all the basic widgets for the vector map.
 *  This will also call the virtual function AddGameSpecificWidgets() to allow
 *  subclasses to add game-specific widgets.
 */
void fwVectorMap::AddWidgets(bkBank& bank)
{
	CreateDefaultSingleton();

	bank.PushGroup("Vector map", false);

	bank.AddToggle("Allow Display of Map", &m_bAllowDisplayOfMap);
	bank.AddToggle("Allow Display of Text with Map", &m_bAllowDisplayOfTextWithMap);

	bank.AddButton("Pick default settings", CallCleanSettings);
	bank.AddButton("Pick good settings", CallGoodSettings);

	bank.AddButton("Zoom in (1)", ZoomIn);
	bank.AddButton("Zoom out (2)", ZoomOut);
	bank.AddButton("Shift left (3)", ShiftLeft);
	bank.AddButton("Shift right (4)", ShiftRight);
	bank.AddButton("Shift up (5)", ShiftUp);
	bank.AddButton("Shift down (6)", ShiftDown);

	bank.AddSlider("Screenspace Minimum Threshold", &s_ScreenspaceMinThreshold, 0.0f, 20.0f, 0.1f, datCallback(RecomputeScreenspaceThresholds));

	ms_Instance->AddGameSpecificWidgets(bank);

	bank.PopGroup();

	RecomputeScreenspaceThresholds();

	if (PARAM_displayvectormap.Get())
	{
		CallGoodSettings();
	}
}

} // namespace rage


#endif // __DEV
