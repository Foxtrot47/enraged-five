/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwdebug/picker.cpp
// PURPOSE : wrapper for all picker types
//				Three pickers are already included - world probe, intersecting entities, entity render
// AUTHOR :  
// CREATED : 15/04/11
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef FWDEBUG_PICKER_H
#define FWDEBUG_PICKER_H

#if __BANK

// Rage headers
#include "atl/array.h"
#include "data/base.h"		// for widget callbacks; essentially free since this class already has virtuals anyway

// Framework headers
#include "fwscene/search/SearchVolumes.h"
#include "fwscene/world/WorldRepBase.h"
#include "fwtl/regdrefs.h"

namespace rage
{
// Forward declarations
class fwEntity;
class bkGroup;

enum ePickerShowHideMode
{
	PICKER_SHOW_ALL = -1,
	PICKER_ISOLATE_SELECTED_ENTITY,
	PICKER_ISOLATE_SELECTED_ENTITY_NOALPHA,
	PICKER_ISOLATE_ENTITIES_IN_LIST,
	PICKER_HIDE_SELECTED_ENTITY,
	PICKER_HIDE_ENTITIES_IN_LIST,
};

/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerInterfaceSettings - doesn't currently contain any settings of its own.
//								Just acts as a base class in case the game-side interface does have settings.
//
/////////////////////////////////////////////////////////////////////////////////
class fwPickerInterfaceSettings
{
public :
	fwPickerInterfaceSettings()	{}

	virtual ~fwPickerInterfaceSettings() {}
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerInterface
//
/////////////////////////////////////////////////////////////////////////////////
class fwPickerInterface: public datBase
{
public:
	fwPickerInterface() {}
  	virtual ~fwPickerInterface() {}

	virtual void Init(unsigned /*initMode*/) {}
	virtual void Shutdown(unsigned /*shutdownMode*/) {}

	//	Add any game-specific widgets in to the game-side version of these functions
	virtual void AddGeneralWidgets(bkBank* /*pWidgetBank*/) {}
	virtual void AddOptionsWidgets(bkBank* /*pWidgetBank*/) {}

	//	Callback function that is called whenever the "Enable picking" toggle or "Picker Type" combo is changed
	virtual void OnWidgetChange() {}

	// Callback function that is called whenever the "Display results window" toggle is changed
	virtual void OnDisplayResultsWindow() {}

	// The list of selected entities will only be updated on a left mouse click if this returns false
	virtual bool DoesUiHaveMouseFocus() { return false; }

	// If this returns true then a left mouse click won't clear the list of entities i.e. the current entities
	//	will be added to the existing list
	virtual bool AllowMultipleSelection() { return false; }

	//	Called every frame that the picker is enabled
	virtual void Update() {}

	//	Called after the left mouse click
	virtual void OutputResults() = 0;

	// Called when a new system has taken over as controller of the picker
	virtual void PickerHasNewController() {}

	virtual ePickerShowHideMode GetShowHideMode() { return PICKER_SHOW_ALL; }

	//	When a system wants to take control of the picker, it can call this 
	//	to set the widget settings to suit its needs.
	//	This is optional.
	virtual void SetPickerSettings(fwPickerInterfaceSettings *pNewSettings);

	//	This is called to determine whether g_pPickMe is a valid entity
	//	pointer that can safely be added to the picker results list
	virtual bool IsValidEntityPtr(fwEntity* /*pToTest*/) { return false; }
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwBasePickerSettings - 
//
/////////////////////////////////////////////////////////////////////////////////
class fwBasePickerSettings
{
public:
	fwBasePickerSettings() {}

	virtual ~fwBasePickerSettings() {}
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwBasePicker
//
/////////////////////////////////////////////////////////////////////////////////
class fwBasePicker
{
public :
	fwBasePicker(s32 PickerType)
		: m_PickerType(PickerType) {}

	virtual ~fwBasePicker() {}

	virtual fwEntity* GetHoverEntity() { return NULL; }

	virtual bkGroup *AddWidgetGroup(bkBank *pBank) = 0;

	virtual void PerformSearch() = 0;

	virtual void SetPickerSettings(fwBasePickerSettings* UNUSED_PARAM(pNewSettings)) {}

	virtual void SetSelectedEntityInResultsArray();

	s32 GetPickerType() { return m_PickerType; }
private:
	s32 m_PickerType;
};


/////////////////////////////////////////////////////////////////////////////////
//
// PickerFlagArray - used by two of the three pickers
//
/////////////////////////////////////////////////////////////////////////////////
class fwPickerFlagArray
{
public:
	void AddFlag(const char *pMaskName, u32 MaskValue);

	void AddToggleWidgets(bkBank *pBank);

	void SetFlagState(u32 FlagToSet, bool bState);
	bool IsFlagSet(u32 FlagToTest);

	void WriteBitField(u32 &BitField);
	void ReadBitField(u32 BitField);

private :
	struct sPickerFlagData
	{
		const char *pName;
		u32 Value;
		bool bToggleWidgetState;
	};

	static const u32 MaxEntityTypeMasks = 32;
	atFixedArray<sPickerFlagData, MaxEntityTypeMasks> m_PickerFlagArray;
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwEntityRenderPicker
//
/////////////////////////////////////////////////////////////////////////////////
class fwEntityRenderPicker : public fwBasePicker
{
public:
	fwEntityRenderPicker(s32 PickerType) 
		: fwBasePicker(PickerType) {}

	virtual fwEntity* GetHoverEntity();

	virtual bkGroup *AddWidgetGroup(bkBank *pBank);

	virtual void PerformSearch();

private:
	virtual fwEntity* GetEntityFromRenderPicker() = 0;
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwWorldProbePickerSettings - 
//
/////////////////////////////////////////////////////////////////////////////////
class fwWorldProbePickerSettings : public fwBasePickerSettings
{
	friend class fwWorldProbePicker;

public:
	fwWorldProbePickerSettings()
		: fwBasePickerSettings(), m_ArchetypeMasks(0)
	{
	}

	fwWorldProbePickerSettings(u32 ArchetypeMasks)
		: fwBasePickerSettings(), m_ArchetypeMasks(ArchetypeMasks)
	{
	}

	virtual ~fwWorldProbePickerSettings() {}

private:
	u32 m_ArchetypeMasks;
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwWorldProbePicker
//
/////////////////////////////////////////////////////////////////////////////////
class fwWorldProbePicker : public fwBasePicker
{
public:
	fwWorldProbePicker(s32 PickerType)
		: fwBasePicker(PickerType) {}

	virtual fwEntity* GetHoverEntity();

	virtual bkGroup *AddWidgetGroup(bkBank *pBank);

	virtual void PerformSearch();

	void AddArchetypeMask(u32 MaskValue, const char *pMaskName);

	//	When a system wants to take control of the picker, it can call this 
	//	to set the widget settings to suit its needs.
	//	This is optional.
	virtual void SetPickerSettings(fwBasePickerSettings* pNewSettings);

private:
	fwPickerFlagArray m_ArchetypeMasks;

// Private functions
	fwEntity *FindEntityUnderMouse();

	virtual fwEntity* GetEntityUnderMouse(s32 iFlags) = 0;
};



/////////////////////////////////////////////////////////////////////////////////
//
// fwIntersectingEntitiesPickerSettings - 
//
/////////////////////////////////////////////////////////////////////////////////
class fwIntersectingEntitiesPickerSettings : public fwBasePickerSettings
{
	friend class fwIntersectingEntitiesPicker;

public:
	fwIntersectingEntitiesPickerSettings()
		: fwBasePickerSettings(), m_bSearchExteriors(false), m_bSearchInteriors(false),
		m_bPerformSphericalScan(false), m_RadiusOfSphericalScan(100)
	{
	}

	fwIntersectingEntitiesPickerSettings(bool bSearchExteriors, bool bSearchInteriors, bool bPerformSphericalScan, s32 RadiusOfSphericalScan)
		: fwBasePickerSettings(), m_bSearchExteriors(bSearchExteriors), m_bSearchInteriors(bSearchInteriors),
			m_bPerformSphericalScan(bPerformSphericalScan), m_RadiusOfSphericalScan(RadiusOfSphericalScan)
	{
	}

	virtual ~fwIntersectingEntitiesPickerSettings() {}

private:
	bool m_bSearchExteriors;
	bool m_bSearchInteriors;
	bool m_bPerformSphericalScan;
	s32 m_RadiusOfSphericalScan;
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwIntersectingEntitiesPicker
//
/////////////////////////////////////////////////////////////////////////////////
typedef atArray<fwEntity*> IntersectingEntitiesArray;

class fwIntersectingEntitiesPicker : public fwBasePicker
{
public:
	fwIntersectingEntitiesPicker(s32 PickerType);

	virtual bkGroup *AddWidgetGroup(bkBank *pBank);

	virtual void PerformSearch();

	//	When a system wants to take control of the picker, it can call this 
	//	to set the widget settings to suit its needs.
	//	This is optional.
	virtual void SetPickerSettings(fwBasePickerSettings* pNewSettings);

private:
	bool m_bSearchExteriors;
	bool m_bSearchInteriors;
	bool m_bPerformSphericalScan;
	ATTR_UNUSED bool m_bDisableEarlyReject;
	s32 m_RadiusOfSphericalScan;
	char m_SearchFilter[80];

	static IntersectingEntitiesArray ms_apSearchResults;

// Private functions
	static bool FindEntitiesOfTypeCB(fwEntity* pEntity, void* UNUSED_PARAM(data));

	virtual void GetMousePointing(Vector3& vMouseNear, Vector3& vMouseFar) = 0;
	virtual void GetCentreOfSphericalScan(Vector3& vSphereCentre) = 0;
	
	virtual void ForAllEntitiesIntersecting(fwSearchVolume* pColVol, fwIntersectingCB cb, void* data, s32 typeFlags, s32 locationFlags, s32 lodFlags) = 0;

	virtual void SortArrayOfIntersectingEntities(IntersectingEntitiesArray& ResultsArray) = 0;
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerManagerSettings - 
//
/////////////////////////////////////////////////////////////////////////////////
class fwPickerManagerSettings
{
	friend class fwPickerManager;

public:
	fwPickerManagerSettings()
		: m_ActivePickerType(-1), m_bEnabled(false), 
			m_bDisplayNameOfHoverEntity(false), m_EntityTypeMasks(0), m_bFlashSelectedEntity(true)
	{
	}

	fwPickerManagerSettings(s32 ActivePickerType, bool bEnabled, bool bDisplayNameOfHoverEntity, u32 EntityTypeMasks, bool bFlashSelectedEntity)
		: m_ActivePickerType(ActivePickerType), m_bEnabled(bEnabled), 
			m_bDisplayNameOfHoverEntity(bDisplayNameOfHoverEntity), m_EntityTypeMasks(EntityTypeMasks), m_bFlashSelectedEntity(bFlashSelectedEntity)
	{
	}

private:
	s32 m_ActivePickerType;
	bool m_bEnabled;
	bool m_bDisplayNameOfHoverEntity;
	bool m_bFlashSelectedEntity;

	u32 m_EntityTypeMasks;
};


/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerManager
//
/////////////////////////////////////////////////////////////////////////////////

typedef atArray< fwRegdRef<fwEntity> > PickerResultsArray;



class fwPickerManager
{
public:

//	**** These are the functions that most people will be interested in to make use of the picker
	fwEntity *GetHoverEntity();
	s32 GetIndexOfSelectedEntity();
	void SetIndexOfSelectedEntity(s32 NewSelectedEntityIndex);
	fwEntity *GetSelectedEntity();
	s32 GetNumberOfEntities();
	fwEntity *GetEntity(s32 Index);

	s32 GetIndexOfEntityWithinResultsArray(const fwEntity *pEntityToFind);

	//	Call this to pass control of the picker widget from one "owner" to another
	void TakeControlOfPickerWidget(const char *pOwnerName);

	//	When a system wants to take control of the picker, it can call these
	//	to set the widget settings to suit its needs.
	//	This is optional.
	void SetPickerManagerSettings(fwPickerManagerSettings *pNewSettings);
	void SetPickerInterfaceSettings(fwPickerInterfaceSettings *pNewSettings);
	void SetPickerSettings(s32 PickerType, fwBasePickerSettings *pNewSettings);

//	****

	static void StaticInit(unsigned initMode);
	static void StaticShutdown(unsigned shutdownMode);

	fwPickerManager();

	void InitClass(fwPickerInterface *pPickerInterface);
	void ShutdownClass();

	// Call this every frame
	void Update();

	// Call once for each picker at the start of the game
	void AddPicker(fwBasePicker *pNewPicker, const char *pPickerName);

	// Call this once at the start of the game for each entity type that should have a toggle widget
	void AddEntityTypeMask(u32 MaskValue, const char *pMaskName);

	// Required by fwIntersectingEntitiesPicker::PerformSearch to check which entity type flags have been set
	void SetBitFieldOfEntityTypes(s32 &BitFieldToSet);

	void SetEnabled(bool bEnabled);
	bool IsEnabled() { return m_bEnabled; }
	bool IsPickerOfTypeEnabled(s32 PickerType);

	void OnDisplayResultsWindow();

	bool IsCurrentOwner(const char *pOwnerName);
	const char *GetOwnerName() { return m_CurrentOwnerName; }

	void AddEntityToPickerResults(const fwEntity *pEntityToAdd, bool bClearListFirst, bool bSetEntityAsSelected);

	void ResetList(bool bCheckForMultipleSelections);
	void AddEntityToList(const fwEntity *pEntityToAdd, bool bOnlyAddIfEntityFlagsMatch, bool bAllowToggle);

	ePickerShowHideMode GetShowHideMode();

	bool IsFlashingSelectedEntity() { return m_bFlashSelectedEntity; }
	bool IsFlashingInAllPhases() { return m_bFlashInAllPhases; }
	bool IsEarlyRejectEnabled() { return m_bAllowEarlyReject; }
	bool IsUiEnabled() { return m_bUiEnabled; }
	void SetUiEnabled(bool bEnabled) { m_bUiEnabled=bEnabled; }
	void SetCooloff(u32 coolOff) { m_coolOff = coolOff; }

	fwPickerInterface *GetInterface() { return m_pPickerInterface; }

private:
	fwPickerInterface *m_pPickerInterface;

	static const s32 MAX_PICKERS = 5;
	atArray<fwBasePicker *> m_PickerArray;
	const char *m_PickerNameArray[MAX_PICKERS];

	bkBank *m_pPickerBank;
	bkGroup *m_pPickerWidgetGroup;	//	The widget group containing all the picker widgets
	bkGroup *m_pWidgetGroupForSelectedPickerType;	//	The widget group containing widgets particular to the current picker type

	s32 m_ActivePicker;
	bool m_bEnabled;
	bool m_bUiEnabled;
	bool m_bUiHasMouseFocus;
	bool m_bDisplayNameOfHoverEntity;
	bool m_bFlashSelectedEntity;
	bool m_bFlashInAllPhases;
	bool m_bAllowEarlyReject;
	u32 m_coolOff;

	static const s32 MaxLengthOfOwnerName = 24;
	char m_CurrentOwnerName[MaxLengthOfOwnerName];
	atLiteralHashValue m_CurrentOwnerNameHash;

	fwPickerFlagArray m_EntityTypeMasks;

	s32 m_IndexOfSelectedEntity;

	PickerResultsArray m_apEntities;
	fwRegdRef<fwEntity> m_pHoverEntity;


// Private functions
	void Init(unsigned initMode);
	void Shutdown(unsigned shutdownMode);

	void AddOptionsWidgets(bkBank *pBank);
	void AddWidgetGroupForSelectedPickerType();

	void OnWidgetChange();

	bool DoesEntityMatchSelectedEntityFlags(const fwEntity *pEntity);

	bool DoesEntityExistInArray(const fwEntity *pEntityToCheck);
	void RemoveEntityFromArray(const fwEntity *pEntityToRemove);

	void SetHoverEntity(fwEntity *pEntity);

	s32 GetPickerIndex(s32 PickerType);
	fwBasePicker *GetPicker(s32 PickerType);
};

} // namespace rage

extern rage::fwPickerManager g_PickerManager;

//	Set this in the debugger watch window to an entity pointer that you want to add to the picker results list
extern rage::fwEntity *g_pPickMe;


/////////////////////////////////////////////////////////////////////////////////
// Debug break macro.
// Used to make it easier to create switched breaks on the selected entity under
// various conditions.
// It is used like:
//		void MyClass::ProcessControl()
//		{
//			DEV_BREAK_IF_SELECTED( MyClass::ms_bBreakOnProcessControlOfSelectedEntity, this );
//			...
//		}
/////////////////////////////////////////////////////////////////////////////////
#if __DEV
#define DEV_BREAK_IF_SELECTED( controlParam, entityToTestIfIsSelected )											\
	Assertf(!(controlParam) || g_PickerManager.GetSelectedEntity() != entityToTestIfIsSelected, "Breaking on " #controlParam " for selected entity " #entityToTestIfIsSelected );
#else // __DEV
#define DEV_BREAK_IF_SELECTED( x, y )
#endif // __DEV



/////////////////////////////////////////////////////////////////////////////////
// Debug break macro.
// Used to make it easier to create switched breaks on any entity in the picker results list
//	under various conditions.
// It is used like:
//		void MyClass::ProcessControl()
//		{
//			DEV_BREAK_IF_IN_PICKER_RESULTS_LIST( MyClass::ms_bBreakOnProcessControlOfEntityInPickerResults, this );
//			...
//		}
/////////////////////////////////////////////////////////////////////////////////
#if __DEV
#define DEV_BREAK_IF_IN_PICKER_RESULTS_LIST( controlParam, entityToTestIfIsInPickerResults )											\
	Assertf(!(controlParam) || g_PickerManager.GetIndexOfEntityWithinResultsArray(entityToTestIfIsInPickerResults) == -1, "Breaking on " #controlParam " for entity in picker results" #entityToTestIfIsInPickerResults );
#else // __DEV
#define DEV_BREAK_IF_IN_PICKER_RESULTS_LIST( x, y )
#endif // __DEV


#endif //__BANK

#endif // FWDEBUG_PICKER_H


