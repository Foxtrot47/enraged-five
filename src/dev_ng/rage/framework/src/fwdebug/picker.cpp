/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwdebug/picker.cpp
// PURPOSE : wrapper for all picker types
//				Three pickers are already included - world probe, intersecting entities, entity render
// AUTHOR :  
// CREATED : 15/04/11
//
/////////////////////////////////////////////////////////////////////////////////

#if __BANK

#include "picker.h"

// Rage headers
#include "bank/bkmgr.h"
#include "grcore/debugdraw.h"
#include "grcore/setup.h"
#include "input/mouse.h"
#include "string/stringutil.h"
#include "vector/colors.h"

// Framework headers
#include "entity/entity.h"
#include "fwdebug/debugbank.h"
#include "fwscene/search/SearchEntities.h"
#include "fwsys/gameskeleton.h"

// Global instance of the picker manager
rage::fwPickerManager g_PickerManager;

// Set this in the debugger watch window to an entity pointer that you want to add to the picker results list
rage::fwEntity *g_pPickMe = NULL;


namespace rage {

IntersectingEntitiesArray fwIntersectingEntitiesPicker::ms_apSearchResults;

/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerInterface
//
/////////////////////////////////////////////////////////////////////////////////

void fwPickerInterface::SetPickerSettings(fwPickerInterfaceSettings* UNUSED_PARAM(pNewSettings))
{
//	Nothing to do here yet - fwPickerInterface has no settings
//	if (Verifyf(pNewSettings, "fwPickerInterface::SetPickerSettings - NewSettings pointer is NULL"))
//	{
//	}
}


/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerManager
//
/////////////////////////////////////////////////////////////////////////////////

void fwPickerManager::StaticInit(unsigned initMode)
{
	g_PickerManager.Init(initMode);
}

void fwPickerManager::StaticShutdown(unsigned shutdownMode)
{
	g_PickerManager.Shutdown(shutdownMode);
}


fwPickerManager::fwPickerManager()
{
	m_pPickerInterface = NULL;

	m_pPickerBank = NULL;
	m_pPickerWidgetGroup = NULL;
	m_pWidgetGroupForSelectedPickerType = NULL;

	m_ActivePicker = 0;
	m_bEnabled = false;
	m_bUiEnabled = true;
	m_bUiHasMouseFocus = true;
	m_bDisplayNameOfHoverEntity = false;
	m_bFlashSelectedEntity = false;
	m_bFlashInAllPhases = false;
	m_bAllowEarlyReject = true;

	m_CurrentOwnerName[0] = '\0';
	m_CurrentOwnerNameHash.Clear();

	m_IndexOfSelectedEntity = -1;

	m_apEntities.Reset();
	m_pHoverEntity = NULL;
}

void fwPickerManager::InitClass(fwPickerInterface *pPickerInterface)
{
	m_pPickerInterface = pPickerInterface;

	m_pPickerBank = &BANKMGR.CreateBank("Picker");

	if (Verifyf(m_pPickerInterface, "fwPickerManager::InitClass - no picker interface has been set"))
	{
		m_pPickerInterface->AddGeneralWidgets(m_pPickerBank);
	}

	AddOptionsWidgets(m_pPickerBank);
}

void fwPickerManager::ShutdownClass()
{
	delete m_pPickerInterface;
	m_pPickerInterface = NULL;

	if (m_pPickerBank)
	{
		BANKMGR.DestroyBank(*m_pPickerBank);
	}
	m_pPickerBank = NULL;
	m_pPickerWidgetGroup = NULL;
	m_pWidgetGroupForSelectedPickerType = NULL;
}

void fwPickerManager::Init(unsigned initMode)
{
	if(initMode == INIT_SESSION)
	{
		m_bUiHasMouseFocus = false;
		g_pPickMe = NULL;
	}

	if (Verifyf(m_pPickerInterface, "fwPickerManager::Init - no picker interface has been set"))
	{
		m_pPickerInterface->Init(initMode);
	}
}

void fwPickerManager::Shutdown(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_SESSION)
	{
		SetEnabled(false);

		m_IndexOfSelectedEntity = -1;

		m_apEntities.Reset();
		m_pHoverEntity = NULL;
	}

	if (Verifyf(m_pPickerInterface, "fwPickerManager::Shutdown - no picker interface has been set"))
	{
		m_pPickerInterface->Shutdown(shutdownMode);
	}
}


void fwPickerManager::AddPicker(fwBasePicker *pNewPicker, const char *pPickerName)
{
	s32 CurrentNumberOfPickers = m_PickerArray.GetCount();

#if __ASSERT
	s32 picker_loop = 0;
	while (picker_loop < CurrentNumberOfPickers)
	{
		Assertf(m_PickerArray[picker_loop]->GetPickerType() != pNewPicker->GetPickerType(), "fwPickerManager::AddPicker - a picker already exists of type %d", pNewPicker->GetPickerType());
		picker_loop++;
	}
#endif	//	__ASSERT

	if (Verifyf(CurrentNumberOfPickers < MAX_PICKERS, "fwPickerManager::AddPicker - too many pickers"))
	{
		m_PickerArray.PushAndGrow(pNewPicker);
		m_PickerNameArray[CurrentNumberOfPickers] = pPickerName;
	}
}


bool fwPickerManager::IsPickerOfTypeEnabled(s32 PickerType)
{
	if (m_bEnabled)
	{
		if ( (m_ActivePicker >= 0) && (m_ActivePicker < m_PickerArray.GetCount()) )
		{
			if (m_PickerArray[m_ActivePicker]->GetPickerType() == PickerType)
			{
				return true;
			}
		}
	}

	return false;
}

s32 fwPickerManager::GetPickerIndex(s32 PickerType)
{
	const s32 CurrentNumberOfPickers = m_PickerArray.GetCount();
	s32 picker_loop = 0;
	while (picker_loop < CurrentNumberOfPickers)
	{
		if (m_PickerArray[picker_loop]->GetPickerType() == PickerType)
		{
			return picker_loop;
		}

		picker_loop++;
	}

	return -1;
}

fwBasePicker *fwPickerManager::GetPicker(s32 PickerType)
{
	s32 PickerIndex = GetPickerIndex(PickerType);
	if (PickerIndex >= 0)
	{
		return m_PickerArray[PickerIndex];
	}

	return NULL;
}


void fwPickerManager::SetPickerManagerSettings(fwPickerManagerSettings *pNewSettings)
{
	if (Verifyf(pNewSettings, "fwPickerManager::SetPickerManagerSettings - NewSettings pointer is NULL"))
	{
		m_ActivePicker = GetPickerIndex(pNewSettings->m_ActivePickerType);
		SetEnabled(pNewSettings->m_bEnabled);
		m_bDisplayNameOfHoverEntity = pNewSettings->m_bDisplayNameOfHoverEntity;
		m_bFlashSelectedEntity = pNewSettings->m_bFlashSelectedEntity;
		m_EntityTypeMasks.ReadBitField(pNewSettings->m_EntityTypeMasks);
	}
}

void fwPickerManager::SetPickerInterfaceSettings(fwPickerInterfaceSettings *pNewSettings)
{
	if (Verifyf(pNewSettings, "fwPickerManager::SetPickerInterfaceSettings - NewSettings pointer is NULL"))
	{
		if (Verifyf(m_pPickerInterface, "fwPickerManager::SetPickerInterfaceSettings - picker manager has no interface"))
		{
			m_pPickerInterface->SetPickerSettings(pNewSettings);
		}
	}
}

void fwPickerManager::SetPickerSettings(s32 PickerType, fwBasePickerSettings *pNewSettings)
{
	if (Verifyf(pNewSettings, "fwPickerManager::SetPickerSettings - NewSettings pointer is NULL"))
	{
		fwBasePicker *pPicker = GetPicker(PickerType);
		if (Verifyf(pPicker, "fwPickerManager::SetPickerSettings - couldn't find picker of type %d", PickerType))
		{
			pPicker->SetPickerSettings(pNewSettings);
		}
	}
}

void fwPickerManager::SetEnabled(bool bEnabled)
{
	m_bEnabled = bEnabled;
	OnWidgetChange();
}

bool fwPickerManager::IsCurrentOwner(const char *pOwnerName)
{
	if (m_bEnabled)
	{
		atLiteralHashValue OwnerNameHash(pOwnerName);

		if (OwnerNameHash == m_CurrentOwnerNameHash)
		{
			return true;
		}
	}

	return false;
}


void fwPickerManager::TakeControlOfPickerWidget(const char *pOwnerName)
{
//	if (!IsCurrentOwner(pOwnerName))
	{
		safecpy(m_CurrentOwnerName, pOwnerName, MaxLengthOfOwnerName);
		m_CurrentOwnerNameHash.SetFromString(pOwnerName);

		//	Is this too hacky? Call the two callbacks just in case any of the relevant widgets have changed
		//	state through the calls to the three fwPickerManager::SetPicker...Settings functions
		OnDisplayResultsWindow();	//	Do I need to do this? OnWidgetChange() now calls OnDisplayResultsWindow()
		OnWidgetChange();

		if (Verifyf(m_pPickerInterface, "fwPickerManager::TakeControlOfPickerWidget - no picker interface has been set"))
		{
			m_pPickerInterface->PickerHasNewController();
		}
	}
}


void fwPickerManager::AddEntityTypeMask(u32 MaskValue, const char *pMaskName)
{
	m_EntityTypeMasks.AddFlag(pMaskName, MaskValue);
}

void fwPickerManager::SetBitFieldOfEntityTypes(s32 &BitFieldToSet)
{
	m_EntityTypeMasks.WriteBitField((u32&)BitFieldToSet);
}

void fwPickerManager::Update()
{
	if (g_pPickMe != NULL)
	{
		if (!m_bEnabled)
		{
			SetEnabled(true);
		}

		if (Verifyf(m_pPickerInterface, "fwPickerManager::Update - no picker interface has been set"))
		{
			if (m_pPickerInterface->IsValidEntityPtr(g_pPickMe))
			{
				AddEntityToPickerResults(g_pPickMe, false, true);
			}
		}
		g_pPickMe = NULL;
	}


	if (m_bEnabled)
	{
		if ( (m_ActivePicker >= 0) && (m_ActivePicker < m_PickerArray.GetCount()) )
		{
			fwBasePicker *picker = m_PickerArray[m_ActivePicker];

			if (picker)
			{
				fwEntity* entity = picker->GetHoverEntity();
				if (entity)
				{
					SetHoverEntity(entity);
				}
			}
		}

		if (m_bDisplayNameOfHoverEntity)
		{
			if (GetHoverEntity() && GetHoverEntity()->GetArchetype())
			{
				u32 hoverEntityModelIndex = GetHoverEntity()->GetModelIndex();
				if ( (hoverEntityModelIndex != fwModelId::MI_INVALID)
					&& (hoverEntityModelIndex < fwArchetypeManager::GetMaxArchetypeIndex()) )
				{
					Vector2 pos = Vector2((float)ioMouse::GetX(), (float)(ioMouse::GetY() - 8));

					grcDebugDraw::TextFontPush(grcSetup::GetMiniFixedWidthFont());
					grcDebugDraw::Text(pos, DD_ePCS_Pixels, Color_white, GetHoverEntity()->GetModelName(), true, 1.0f, 1.0f);

					grcDebugDraw::TextFontPop();
				}
			}
		}

		// TODO -- click-drag for multiple continuous selections or deselections
		bool bPerformSearch = false;
		if (ioMouse::GetPressedButtons()&ioMouse::MOUSE_LEFT)
		{
			bPerformSearch = true;

			//	GSW_FIX
			//		if (CLodDebug::HasJustUsedClickEvent())
			//		{
			//			bPerformSearch = false;			
			//		}

			if (Verifyf(m_pPickerInterface, "fwPickerManager::Update - no picker interface has been set"))
			{
				if (m_pPickerInterface->DoesUiHaveMouseFocus())
				{
					m_bUiHasMouseFocus = true;
				}
			}
		}
		else
		{
			m_bUiHasMouseFocus = false;
		}

		if (m_coolOff)
		{
			m_coolOff--;
			bPerformSearch = false;
		}
		if (bPerformSearch && (!m_bUiHasMouseFocus))
		{
			if ( (m_ActivePicker >= 0) && (m_ActivePicker < m_PickerArray.GetCount()) )
			{
				m_PickerArray[m_ActivePicker]->PerformSearch();
				m_PickerArray[m_ActivePicker]->SetSelectedEntityInResultsArray();
			}

			if (Verifyf(m_pPickerInterface, "fwPickerManager::Update - no picker interface has been set"))
			{
				m_pPickerInterface->OutputResults();
			}
		}
	}


// Give the interface a chance to Update even when the picker is disabled
	if (Verifyf(m_pPickerInterface, "fwPickerManager::Update - no picker interface has been set"))
	{
		m_pPickerInterface->Update();
	}
}


void fwPickerManager::AddOptionsWidgets(bkBank *pBank)
{
	if (AssertVerify(pBank))
	{
		m_pPickerWidgetGroup = pBank->PushGroup("Picker Options", false);

			pBank->AddToggle("Enable picking", &m_bEnabled, datCallback(MFA(fwPickerManager::OnWidgetChange), (datBase*)this) );
			pBank->AddToggle("Enable UI", &m_bUiEnabled);
			pBank->AddToggle("Display name of hover entity", &m_bDisplayNameOfHoverEntity);
			pBank->AddToggle("Flash selected entity", &m_bFlashSelectedEntity);
			pBank->AddToggle("Flash in all phases", &m_bFlashInAllPhases);
			pBank->AddToggle("Reject selected for visibility", &m_bAllowEarlyReject);

			pBank->AddTitle("Types to search for");

			m_EntityTypeMasks.AddToggleWidgets(pBank);

			pBank->AddTitle("Options");

			if (Verifyf(m_pPickerInterface, "fwPickerManager::AddOptionsWidgets - no picker interface has been set"))
			{
				m_pPickerInterface->AddOptionsWidgets(pBank);
			}

			pBank->AddSeparator();

			if (m_PickerArray.GetCount() > 0)
			{
				pBank->AddCombo("Picker Type", &m_ActivePicker, m_PickerArray.GetCount(), m_PickerNameArray, datCallback(MFA(fwPickerManager::OnWidgetChange), (datBase*)this) );
			}

			AddWidgetGroupForSelectedPickerType();

		pBank->PopGroup();
	}
}


void fwPickerManager::AddWidgetGroupForSelectedPickerType()
{
	if (m_pWidgetGroupForSelectedPickerType)
	{
		m_pPickerBank->DeleteGroup(*m_pWidgetGroupForSelectedPickerType);
		m_pWidgetGroupForSelectedPickerType = NULL;
	}

	if ( (m_ActivePicker >= 0) && (m_ActivePicker < m_PickerArray.GetCount()) )
	{
		m_pWidgetGroupForSelectedPickerType = m_PickerArray[m_ActivePicker]->AddWidgetGroup(m_pPickerBank);
	}
}

void fwPickerManager::OnDisplayResultsWindow()
{
	if (Verifyf(m_pPickerInterface, "fwPickerManager::OnDisplayResultsWindow - no picker interface has been set"))
	{
		m_pPickerInterface->OnDisplayResultsWindow();
	}
}

void fwPickerManager::OnWidgetChange()
{
	m_pPickerBank->SetCurrentGroup(*m_pPickerWidgetGroup);
	AddWidgetGroupForSelectedPickerType();
	m_pPickerBank->UnSetCurrentGroup(*m_pPickerWidgetGroup);

	if (Verifyf(m_pPickerInterface, "fwPickerManager::OnWidgetChange - no picker interface has been set"))
	{
		m_pPickerInterface->OnWidgetChange();
	}

	OnDisplayResultsWindow();
}

void fwPickerManager::AddEntityToPickerResults(const fwEntity *pEntityToAdd, bool bClearListFirst, bool bSetEntityAsSelected)
{
	if (bClearListFirst)
	{
		ResetList(false);
	}

	AddEntityToList(pEntityToAdd, false, false);

	if (bSetEntityAsSelected)
	{
		SetIndexOfSelectedEntity(GetIndexOfEntityWithinResultsArray(pEntityToAdd));
	}

	if (Verifyf(m_pPickerInterface, "fwPickerManager::AddEntityToPickerResults - no picker interface has been set"))
	{
		m_pPickerInterface->OutputResults();
	}
}

void fwPickerManager::ResetList(bool bCheckForMultipleSelections)
{
	if (Verifyf(m_pPickerInterface, "fwPickerManager::ResetList - no picker interface has been set"))
	{
		if (!bCheckForMultipleSelections || !m_pPickerInterface->AllowMultipleSelection())
		{
			m_IndexOfSelectedEntity = -1;

			m_apEntities.Reset();
		}
	}
}

bool fwPickerManager::DoesEntityMatchSelectedEntityFlags(const fwEntity *pEntity)
{
	if (pEntity)
	{
		u32 EntityTypeMask = 1 << pEntity->GetType();

		if (m_EntityTypeMasks.IsFlagSet(EntityTypeMask))
		{
			return true;
		}
	}

	return false;
}

bool fwPickerManager::DoesEntityExistInArray(const fwEntity *pEntityToCheck)
{
	for (s32 loop = 0; loop < m_apEntities.GetCount(); loop++)
	{
		if (m_apEntities[loop] == pEntityToCheck)
		{
			return true;
		}
	}

	return false;
}

void fwPickerManager::RemoveEntityFromArray(const fwEntity *pEntityToRemove)
{
	for (s32 loop = 0; loop < m_apEntities.GetCount(); loop++)
	{
		if (m_apEntities[loop] == pEntityToRemove)
		{
			if (loop < m_apEntities.GetCount() - 1)
			{
				m_apEntities[loop] = m_apEntities.Pop();
			}
			else
			{
				m_apEntities.Pop();
			}
		}
	}
}

void fwPickerManager::AddEntityToList(const fwEntity *pEntityToAdd, bool bOnlyAddIfEntityFlagsMatch, bool bAllowToggle)
{
	if (pEntityToAdd)
	{
		if (pEntityToAdd->IsArchetypeSet())
		{
			if (DoesEntityMatchSelectedEntityFlags(pEntityToAdd) || !bOnlyAddIfEntityFlagsMatch)
			{
				if (!DoesEntityExistInArray(pEntityToAdd))
				{
					fwRegdRef<fwEntity> tempRegdFwEnt(const_cast<fwEntity*>(pEntityToAdd));
					m_apEntities.PushAndGrow(tempRegdFwEnt);
				}
				else if (bAllowToggle && m_pPickerInterface->AllowMultipleSelection())
				{
					RemoveEntityFromArray(pEntityToAdd);
				}
			}
		}
	}
}

void fwPickerManager::SetHoverEntity(fwEntity *pEntity)
{
	if (pEntity)
	{
		if (DoesEntityMatchSelectedEntityFlags(pEntity))
		{
			m_pHoverEntity = pEntity;
		}
		else
		{
			m_pHoverEntity = NULL;
		}
	}
	else
	{
		m_pHoverEntity = NULL;
	}
}

fwEntity *fwPickerManager::GetHoverEntity()
{
	if (m_bEnabled)
	{
		return m_pHoverEntity;
	}

	return NULL;
}

s32 fwPickerManager::GetIndexOfSelectedEntity()
{
	if (m_bEnabled)
	{
		return m_IndexOfSelectedEntity;
	}

	return -1;
}

void fwPickerManager::SetIndexOfSelectedEntity(s32 NewSelectedEntityIndex)
{
	m_IndexOfSelectedEntity = NewSelectedEntityIndex;
}

fwEntity *fwPickerManager::GetSelectedEntity()
{
	if (m_bEnabled)
	{
		fwEntity *pToTest = GetEntity(GetIndexOfSelectedEntity());
		bool isValid = m_pPickerInterface->IsValidEntityPtr(pToTest);
		return isValid ? pToTest : NULL;
	}

	return NULL;
}

s32 fwPickerManager::GetNumberOfEntities()
{
	if (m_bEnabled)
	{
		return m_apEntities.GetCount();
	}

	return 0;
}

fwEntity *fwPickerManager::GetEntity(s32 Index)
{
	if (m_bEnabled)
	{
		if (Index >= 0 && Index < fwPickerManager::GetNumberOfEntities())
		{
			return m_apEntities[Index];
		}
	}

	return NULL;
}

ePickerShowHideMode fwPickerManager::GetShowHideMode()
{
	if (m_bEnabled)
	{
		if (Verifyf(m_pPickerInterface, "fwPickerManager::GetShowHideMode - no picker interface has been set"))
		{
			return m_pPickerInterface->GetShowHideMode();
		}
	}

	return PICKER_SHOW_ALL;
}

s32 fwPickerManager::GetIndexOfEntityWithinResultsArray(const fwEntity *pEntityToFind)
{
	if (m_bEnabled && pEntityToFind)
	{
		const s32 NumberOfResults = m_apEntities.GetCount();
		s32 loop = 0;
		while (loop < NumberOfResults)
		{
			if (m_apEntities[loop] == pEntityToFind)
			{
				return loop;
			}
			loop++;
		}
	}

	return -1;
}

/////////////////////////////////////////////////////////////////////////////////
//
// fwPickerFlagArray
//
/////////////////////////////////////////////////////////////////////////////////

void fwPickerFlagArray::AddFlag(const char *pMaskName, u32 MaskValue)
{
	if Verifyf(!m_PickerFlagArray.IsFull(), "fwPickerFlagArray::AddFlag - too many Masks")
	{
		sPickerFlagData NewPickerFlag;
		NewPickerFlag.Value = MaskValue;
		NewPickerFlag.pName = pMaskName;
		NewPickerFlag.bToggleWidgetState = false;
		m_PickerFlagArray.Push(NewPickerFlag);
	}
}

void fwPickerFlagArray::SetFlagState(u32 FlagToSet, bool bState)
{
	bool bFound = false;
	for (s32 loop = 0; loop < m_PickerFlagArray.GetCount(); loop++)
	{
		if ((m_PickerFlagArray[loop].Value & FlagToSet) != 0)
		{
			if (Verifyf(!bFound, "fwPickerFlagArray::SetFlagState - already found a match for this flag %u", FlagToSet))
			{
				m_PickerFlagArray[loop].bToggleWidgetState = bState;
				bFound = true;
			}
		}
	}

	Assertf(bFound, "fwPickerFlagArray::SetFlagState - no match found for this flag %u", FlagToSet);
}

bool fwPickerFlagArray::IsFlagSet(u32 FlagToTest)
{
	bool bReturnValue = false;
	bool bFound = false;
	for (s32 loop = 0; loop < m_PickerFlagArray.GetCount(); loop++)
	{
		if ((m_PickerFlagArray[loop].Value & FlagToTest) != 0)
		{
			if (Verifyf(!bFound, "fwPickerFlagArray::IsFlagSet - already found a match for this flag %u", FlagToTest))
			{
				bReturnValue = m_PickerFlagArray[loop].bToggleWidgetState;
				bFound = true;
			}
		}
	}

	return bReturnValue;
}

void fwPickerFlagArray::AddToggleWidgets(bkBank *pBank)
{
	if (Verifyf(pBank, "fwPickerFlagArray::AddToggleWidgets - no widget bank specified"))
	{
		for (s32 loop = 0; loop < m_PickerFlagArray.GetCount(); loop++)
		{
			pBank->AddToggle(m_PickerFlagArray[loop].pName, &m_PickerFlagArray[loop].bToggleWidgetState);
		}
	}
}

void fwPickerFlagArray::WriteBitField(u32 &BitField)
{
	BitField = 0;
	for (s32 loop = 0; loop < m_PickerFlagArray.GetCount(); loop++)
	{
		if (m_PickerFlagArray[loop].bToggleWidgetState)
		{
			BitField |= m_PickerFlagArray[loop].Value;
		}
	}
}

void fwPickerFlagArray::ReadBitField(u32 BitField)
{
	for (s32 loop = 0; loop < m_PickerFlagArray.GetCount(); loop++)
	{
		if ((m_PickerFlagArray[loop].Value & BitField) != 0)
		{
			m_PickerFlagArray[loop].bToggleWidgetState = true;
		}
		else
		{
			m_PickerFlagArray[loop].bToggleWidgetState = false;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
//
// fwBasePicker
//
/////////////////////////////////////////////////////////////////////////////////
void fwBasePicker::SetSelectedEntityInResultsArray()
{
	g_PickerManager.SetIndexOfSelectedEntity(g_PickerManager.GetNumberOfEntities() - 1);
}

/////////////////////////////////////////////////////////////////////////////////
//
// fwEntityRenderPicker
//
/////////////////////////////////////////////////////////////////////////////////
fwEntity* fwEntityRenderPicker::GetHoverEntity()
{
	return GetEntityFromRenderPicker();
}

bkGroup *fwEntityRenderPicker::AddWidgetGroup(bkBank *pBank)
{
	bkGroup *pNewWidgetGroup = pBank->PushGroup("Entity Render", true);

	pBank->PopGroup();

	return pNewWidgetGroup;
}


void fwEntityRenderPicker::PerformSearch()
{
	fwEntity* pEntity = GetEntityFromRenderPicker();

	g_PickerManager.ResetList(true);
	g_PickerManager.AddEntityToList(pEntity, true, true);
}


/////////////////////////////////////////////////////////////////////////////////
//
// fwWorldProbePicker
//
/////////////////////////////////////////////////////////////////////////////////
void fwWorldProbePicker::AddArchetypeMask(u32 MaskValue, const char *pMaskName)
{
	m_ArchetypeMasks.AddFlag(pMaskName, MaskValue);
}

void fwWorldProbePicker::SetPickerSettings(fwBasePickerSettings *pNewSettings)
{
	if (Verifyf(pNewSettings, "fwWorldProbePicker::SetPickerSettings - NewSettings pointer is NULL"))
	{
		fwWorldProbePickerSettings *pWorldProbeSettings = dynamic_cast<fwWorldProbePickerSettings*>(pNewSettings);
		if (Verifyf(pWorldProbeSettings, "fwWorldProbePicker::SetPickerSettings - the supplied settings are not world probe picker settings"))
		{
			m_ArchetypeMasks.ReadBitField(pWorldProbeSettings->m_ArchetypeMasks);
		}
	}
}

fwEntity *fwWorldProbePicker::FindEntityUnderMouse()
{
	s32 archetypeFlags = 0;
	m_ArchetypeMasks.WriteBitField((u32&)archetypeFlags);

	return GetEntityUnderMouse(archetypeFlags);
}

fwEntity* fwWorldProbePicker::GetHoverEntity()
{
	return FindEntityUnderMouse();
}

bkGroup *fwWorldProbePicker::AddWidgetGroup(bkBank *pBank)
{
	bkGroup *pNewWidgetGroup = pBank->PushGroup("World Probe", true);

	pBank->AddTitle("Types to search for");

	m_ArchetypeMasks.AddToggleWidgets(pBank);

	pBank->PopGroup();

	return pNewWidgetGroup;
}

void fwWorldProbePicker::PerformSearch()
{
	fwEntity* probeEntity = FindEntityUnderMouse();

	g_PickerManager.ResetList(true);
	g_PickerManager.AddEntityToList(probeEntity, true, true);
}


/////////////////////////////////////////////////////////////////////////////////
//
// fwIntersectingEntitiesPicker
//
/////////////////////////////////////////////////////////////////////////////////
fwIntersectingEntitiesPicker::fwIntersectingEntitiesPicker(s32 PickerType)
	: fwBasePicker(PickerType)
{
	m_bSearchExteriors = true;
	m_bSearchInteriors = true;
	m_bPerformSphericalScan = false;
	m_RadiusOfSphericalScan = 100;
	m_SearchFilter[0] = '\0';
}

const s32 MAX_RADIUS = 4500;

const char* g_EntitySearchFilter = NULL;

bkGroup *fwIntersectingEntitiesPicker::AddWidgetGroup(bkBank *pBank)
{
	bkGroup *pNewWidgetGroup = pBank->PushGroup("Intersecting Entities", true);

	pBank->AddSeparator();

	pBank->AddTitle("Misc options");
	pBank->AddToggle("Search exteriors",				&m_bSearchExteriors);
	pBank->AddToggle("Search interiors",				&m_bSearchInteriors);
	pBank->AddToggle("Spherical Search",				&m_bPerformSphericalScan);
	pBank->AddSlider("Spherical Search radius",			&m_RadiusOfSphericalScan, 1, MAX_RADIUS, 1);
	pBank->AddText("Search filter",						&m_SearchFilter[0], sizeof(m_SearchFilter), false);
	pBank->PopGroup();

	g_EntitySearchFilter = m_SearchFilter;

	return pNewWidgetGroup;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	FindEntitiesOfTypeCB
// PURPOSE:		callback to push search result onto an atArray
//////////////////////////////////////////////////////////////////////////
bool fwIntersectingEntitiesPicker::FindEntitiesOfTypeCB(fwEntity* pEntity, void* UNUSED_PARAM(data))
{
	if (pEntity)
	{
		if (g_EntitySearchFilter == NULL ||
			g_EntitySearchFilter[0] == '\0' ||
			stristr(pEntity->GetModelName(), g_EntitySearchFilter) != NULL)
		{
			ms_apSearchResults.PushAndGrow(pEntity);
		}

		return true;
	}

	return false;
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	fwIntersectingEntitiesPicker::PerformSearch
// PURPOSE:		performs a world search and stores results
//////////////////////////////////////////////////////////////////////////
void fwIntersectingEntitiesPicker::PerformSearch()
{
	ms_apSearchResults.Reset();

	Vector3	vLineSeg1, vLineSeg2;
	GetMousePointing(vLineSeg1, vLineSeg2);
	fwIsLineSegIntersectingBB lineIntersecting(vLineSeg1, vLineSeg2);

	Vector3 vSphereCentre;
	GetCentreOfSphericalScan(vSphereCentre);
	spdSphere hdSphere(RCC_VEC3V(vSphereCentre), ScalarV((float)m_RadiusOfSphericalScan));
	fwIsSphereIntersectingVisible hdSearchSphere(hdSphere);

	const s32 lodFlags = SEARCH_LODTYPE_ALL;

	s32 locationFlags = 0;
	if (m_bSearchExteriors)
	{
		locationFlags |= SEARCH_LOCATION_EXTERIORS;
	}
	if (m_bSearchInteriors)
	{
		locationFlags |= SEARCH_LOCATION_INTERIORS;
	}

	s32 typeFlags = 0;
	g_PickerManager.SetBitFieldOfEntityTypes(typeFlags);

	if (m_bPerformSphericalScan)
	{
		ForAllEntitiesIntersecting(&hdSearchSphere, FindEntitiesOfTypeCB, NULL, typeFlags, locationFlags, lodFlags);
	}
	else
	{
		ForAllEntitiesIntersecting(&lineIntersecting, FindEntitiesOfTypeCB, NULL, typeFlags, locationFlags, lodFlags);
	}

	SortArrayOfIntersectingEntities(ms_apSearchResults);

	g_PickerManager.ResetList(true);

	for (s32 i=0; i<ms_apSearchResults.GetCount(); i++)
	{
		g_PickerManager.AddEntityToList(ms_apSearchResults[i], true, false);
	}
}


void fwIntersectingEntitiesPicker::SetPickerSettings(fwBasePickerSettings *pNewSettings)
{
	if (Verifyf(pNewSettings, "fwIntersectingEntitiesPicker::SetPickerSettings - NewSettings pointer is NULL"))
	{
		fwIntersectingEntitiesPickerSettings *pIntersectingEntitiesSettings = dynamic_cast<fwIntersectingEntitiesPickerSettings*>(pNewSettings);
		if (Verifyf(pIntersectingEntitiesSettings, "fwIntersectingEntitiesPicker::SetPickerSettings - the supplied settings are not intersecting entities picker settings"))
		{
			m_bSearchExteriors = pIntersectingEntitiesSettings->m_bSearchExteriors;
			m_bSearchInteriors = pIntersectingEntitiesSettings->m_bSearchInteriors;
			m_bPerformSphericalScan = pIntersectingEntitiesSettings->m_bPerformSphericalScan;
			m_RadiusOfSphericalScan = pIntersectingEntitiesSettings->m_RadiusOfSphericalScan;
		}
	}
}


} // namespace rage

#endif //__BANK

