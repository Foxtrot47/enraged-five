
#if __BANK

#include "fwdebug/debugbank.h"

#include "bank/bkmgr.h"


namespace rage {

//////////////////////////////////////////////////////////////////////////
//	AnimDebugBank implementation
//////////////////////////////////////////////////////////////////////////

fwDebugBank* fwDebugBank::CreateBank(const char * bankName, fwDebugBankWidgetCallback onActivate, fwDebugBankWidgetCallback onShutdown, fwDebugBankPersistantCallback createPersistant)
{

	//if there already exists a bank with this name, destry it
	bkBank* pExistingBank = BANKMGR.FindBank(bankName);
	if (pExistingBank)
	{
		Assertf(0, "Existing bank found with name %s. It probably wasn't shutdown during a level restart.", bankName);
		BANKMGR.DestroyBank(*pExistingBank);
	}

	fwDebugBank* pBank = rage_new fwDebugBank(bankName, onActivate, onShutdown, createPersistant);

	BANKMGR.AddChild(*pBank);

	pBank->FinishCreate();

	pBank->AddPersistantWidgets();

	pBank->AddToggleButton(bankName);

	return pBank;
}

//////////////////////////////////////////////////////////////////////////

void fwDebugBank::Shutdown()
{
	if (IsActive())
	{
		ToggleActive(); //shut main widgets down first
	}

	BANKMGR.DestroyBank(*this);
}

//////////////////////////////////////////////////////////////////////////

void fwDebugBank::ToggleActive()
{
	m_bActivated = !m_bActivated;

	if (m_bActivated )
	{ 
		if (m_activateCallback)
		{
			m_activateCallback();
		}
	}
	else
	{
		if (m_shutdownCallback)
		{
			m_shutdownCallback();
		}
	}
}

//////////////////////////////////////////////////////////////////////////

bool fwDebugBank::IsActive()
{
	return m_bActivated;
}

//////////////////////////////////////////////////////////////////////////

void fwDebugBank::RemoveAllMainWidgets()
{
	bkWidget* pWidget = GetFirstMainWidget();
	while (pWidget)
	{
		pWidget->Destroy();
		pWidget = GetFirstMainWidget();
	}
}

} // namespace rage

#endif // __BANK
