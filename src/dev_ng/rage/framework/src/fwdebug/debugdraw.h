/////////////////////////////////////////////////////////////////////////////////
// FILE		: debugdraw.h
// PURPOSE	: To provide in game visual helpers for debugging.
// AUTHOR	: G. Speirs, Adam Croston, Alex Hadjadj
// STARTED	: 10/08/99
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWDEBUGDRAW_DEBUGDRAW_H
#define FWDEBUGDRAW_DEBUGDRAW_H

// rage headers
#include "grcore/im.h"			// For __IM_BATCHER
#include "vector/color32.h"
#include "vector/vector3.h"
#include "vectormath/legacyconvert.h"

#include "grcore/debugdraw.h"

// Game header
#define DEBUG_DRAW (__IM_BATCHER)


#if !DEBUG_DRAW
#define DEBUG_DRAW_ONLY(...)
#else
#define DEBUG_DRAW_ONLY(...)	__VA_ARGS__

namespace rage {

namespace fwDebugDraw {
	void Init();
}


} // namespace rage

#endif // DEBUG_DRAW

#endif // FWDEBUGDRAW_DEBUGDRAW_H
