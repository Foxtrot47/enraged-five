#ifndef FWANIMATION_DEBUG_DEBUGBANK_H
#define FWANIMATION_DEBUG_DEBUGBANK_H

#if __BANK

#include "atl/functor.h"
#include "atl/string.h"
#include "bank/bank.h"
#include "bank/button.h"
#include "bank/text.h"


namespace rage {

class bkWidget;
class fwDebugBank;


//////////////////////////////////////////////////////////////////////////
//	CAnimDebugBank
//	Provides an activate button with a callback mechanism that can be 
//	used to intialise and shut down the banks widgets as required
//	This is preferable with large banks since it circumv
//////////////////////////////////////////////////////////////////////////

typedef Functor0					fwDebugBankWidgetCallback;
typedef Functor1<fwDebugBank*>	fwDebugBankPersistantCallback;

class fwDebugBank : public bkBank
{
	friend class bkWidget;

public:

	fwDebugBank(const char * name, fwDebugBankWidgetCallback onActivate = NULL, fwDebugBankWidgetCallback onShutdown = NULL, fwDebugBankPersistantCallback createPersistant = NULL):
		bkBank(name, NullCB, NullCB),
		m_pButton(NULL),
		m_bActivated(false)
	{
		m_activateCallback = onActivate;
		m_shutdownCallback = onShutdown;
		m_persistantCallback = createPersistant;
	}

	// PURPOSE: To be called from InitLevelWidgets. By default will
	//			create a check box which can be used to build the rest of the bank.
	static fwDebugBank* CreateBank(const char * bankName, fwDebugBankWidgetCallback onActivate = NULL, fwDebugBankWidgetCallback onShutdown = NULL, fwDebugBankPersistantCallback createPersistant = NULL);

	// PURPOSE: To be called from ShutdownLevelWidgets. By default will destroy the
	//			stored bank, and any widgets within it.
	void Shutdown();

	// PURPOSE: Activates or deactivates the bank's main widgets
	void ToggleActive();

	// PURPOSE: Returns true if the bank's main widgets have been created, false otherwise
	bool IsActive();

	//////////////////////////////////////////////////////////////////////////
	//	Direct access methods
	//////////////////////////////////////////////////////////////////////////

	// PURPOSE: Provides a pointer to the activate button widget
	inline const bkButton* GetActivateButton() { return m_pButton;}

	// PURPOSE: Provides a pointer to the activate button widget
	inline bkWidget* GetFirstMainWidget() { return m_pButton->GetNext();}

	// PURPOSE: Provides a pointer to the activate button widget
	void RemoveAllMainWidgets();
protected:
	
	inline void AddToggleButton(const char* pBankName )
	{
		atString buttonText;
		buttonText += "Toggle ";
		buttonText += pBankName;
		buttonText += " bank";
		m_pButton = AddButton(buttonText.c_str(), datCallback(MFA(fwDebugBank::ToggleActive), (datBase*)this));
	}

	inline void AddPersistantWidgets()
	{
		if (m_persistantCallback)
		{
			m_persistantCallback(this);
		}
	}

	bkButton* m_pButton;		// Pointer to the activate button
	bool m_bActivated;			// Is the bank currently active or not

	fwDebugBankPersistantCallback m_persistantCallback;	// The callback to trigger when the bank is activated
	fwDebugBankWidgetCallback m_activateCallback;	// The callback to trigger when the bank is activated
	fwDebugBankWidgetCallback m_shutdownCallback;	// The callback to trigger when the bank is shutdown
};

} // namespace rage

#endif // __BANK


#endif // FWANIMATION_DEBUG_DEBUGBANK_H