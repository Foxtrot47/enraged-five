/////////////////////////////////////////////////////////////////////////////////
// FILE :    vectormap.h
// PURPOSE : A debug map that can be viewed on top of the game
// AUTHOR :  Obbe, Adam Croston
// CREATED : 31/08/06
/////////////////////////////////////////////////////////////////////////////////
#ifndef FWDEBUG_VECTORMAP_H
#define FWDEBUG_VECTORMAP_H

// rage headers
#include "vector/color32.h"
#include "vector/vector3.h"
namespace rage
{
	class bkBank;
	class fwRect;
	class spdAABB;
	class Vector2;

#if __BANK
class fwVectorMap
{
public:
	virtual ~fwVectorMap()									{}
	virtual void AddGameSpecificWidgets(bkBank &/*bank*/)	{}

	static void Init();
	static void Shutdown();
	static void Update()									{ fwVectorMap::GetInstance().UpdateCore(); }
	static void Draw()										{ fwVectorMap::GetInstance().DrawCore(); }
	static void CleanSettings()								{ fwVectorMap::GetInstance().CleanSettingsCore(); }
	static void GoodSettings()								{ fwVectorMap::GetInstance().GoodSettingsCore(); }

	static void SetWorldDimensions(const Vector2 &min, const Vector2 &max);

	static void DrawLine(const Vector3& v1, const Vector3& v2, Color32 col1, Color32 col2, bool bForceMap = true);
	static inline void DrawLine(const Vec2f& v1, const Vec2f& v2, Color32 col1, Color32 col2, bool bForceMap = true);
	static void DrawLineThick(const Vector3& v1, const Vector3& v2, Color32 col1, Color32 col2, bool bForceMap = true);
	static void DrawQuad(const Vector3& v1, const Vector3& v2, const Vector3 &v3, const Vector3 &v4, Color32 color, bool bForceMap = true);
	static void DrawPoly(const Vector3& a, const Vector3& b, const Vector3& c, Color32 col, bool solid = true, bool bForceMap = true);
	static void DrawPoly(const Vector3& a, const Vector3& b, const Vector3& c, Color32 col1, Color32 col2, Color32 col3, bool solid = true, bool bForceMap = true);
	static void DrawCircle(const Vector3& center, float radius, Color32 col2, bool solid = true, bool bForceMap = true);
	static void DrawArc(const Vector3& center, float radius, float startTheta, float endTheta, Color32 col, bool solid = false, bool bForceMap = true);
	static void DrawRectAxisAligned(const Vector3& min, const Vector3& max, Color32 col, bool solid = true, bool bForceMap = true);
	static void DrawWedge(const Vector3& center, float radiusInner, float radiusOuter, float thetaStart, float thetaEnd, s32 numSegments, Color32 col, bool bForceMap = true);
	static void DrawMarker(const Vector3& center, Color32 col2, float size = 1.0f, bool bForceMap = true);
    static void DrawRemotePlayerCamera(const Matrix34 &camMatrix, float farClip, float tanHFov, Color32 col, bool bForceMap = true);
	static void DrawRemotePlayerCameraCloseVisibleBounds(const Matrix34 &camMatrix, float nearClip, float farClip, float tanHFov, Color32 col, float fArcOffset = 0.0f, bool bForceMap = true);
	static void DrawString(const Vector3& pos, const char *String, Color32 col, bool drawBGQuad, bool bForceMap = true);

	static void Draw3DBoundingBoxOnVMap(const spdAABB& bb, Color32 color);
	static void Draw2DBoundingBoxOnVMap(const fwRect& bb, Color32 color);



	static void ConvertSizeWorldToMap(float worldSize, float& mapSize);
	static void ConvertSizeMapToWorld(float mapSize, float& worldSize);
	static void ConvertPointWorldToMap(float worldX, float worldY, float &mapX, float &mapY);
	static void ConvertPointMapToWorld(float mapX, float mapY, float& worldX, float& worldY);
	static void ConvertPointWorldToMap(const Vector3& worldPos, Vector2& mapPos);
	static void ConvertPointMapToWorld(const Vector2& mapPos, Vector3& worldPos);

	static void ForceMapDisplay();

    static void SetZoom(float zoom);
	static void ZoomIn();
	static void ZoomOut();
	static void ShiftLeft();
	static void ShiftRight();
	static void ShiftUp();
	static void ShiftDown();

	static void AddWidgets(bkBank& bank);

	static bool m_bAllowDisplayOfMap;
	static bool m_bAllowDisplayOfTextWithMap;

	static fwVectorMap *ms_Instance;				// The singleton for this vector map - could be subclassed.

	static fwVectorMap &GetInstance()
	{
		if (ms_Instance)
		{
			return *ms_Instance;
		}

		ms_Instance = rage_new fwVectorMap();
		return *ms_Instance;
	}

protected:
	virtual void	UpdateCore();
	virtual void	DrawCore();
	virtual void	CleanSettingsCore();
	virtual void	GoodSettingsCore();

	virtual void	UpdateFocus();
	virtual void	UpdateRotation();

	static bool		IsBoundingBoxCulled(const Vector2 &pos1, const Vector2 &pos2);
	static void		RecomputeScreenspaceThresholds();

	static void		CreateDefaultSingleton();		// Create the standard RAGE fwVectorMap singleton if there isn't a subclassed one already.
	static void		CallCleanSettings()					{ fwVectorMap::GetInstance().CleanSettings(); }
	static void		CallGoodSettings()					{ fwVectorMap::GetInstance().GoodSettings(); }

	static float	m_FocusWorldX, m_FocusWorldY;	// The coordinates that get mapped to the center of the screen
	static float	m_MapRotationRads;				// 0.0f is standard up.
	static float	m_Zoom;							// 1.0f is standard. > 1.0f means zoomed in.

	static bool		m_bMapShouldBeDrawnThisFrame;
};

inline void fwVectorMap::DrawLine(const Vec2f& v1, const Vec2f& v2, Color32 col1, Color32 col2, bool bForceMap /* = true */)
{
	Vector3 v1a(v1.GetX(), v1.GetY(), 0.0f);
	Vector3 v2a(v2.GetX(), v2.GetY(), 0.0f);
	DrawLine(v1a, v2a, col1, col2, bForceMap);
}

#else //  __DEV
// DOM-IGNORE-BEGIN
class fwVectorMap
{
public:
	static __forceinline void Init(bkBank& UNUSED_PARAM(bank)) {};
	static __forceinline void Shutdown() {};
	static __forceinline void Update() {};

	static __forceinline void DrawLine(const Vector3& UNUSED_PARAM(v1), const Vector3& UNUSED_PARAM(v2), Color32 UNUSED_PARAM(col1), Color32 UNUSED_PARAM(col2), bool UNUSED_PARAM(bForceMap) = true ) {};
	static __forceinline void DrawLine(const Vec2f& UNUSED_PARAM(v1), const Vec2f& UNUSED_PARAM(v2), Color32 UNUSED_PARAM(col1), Color32 UNUSED_PARAM(col2), bool UNUSED_PARAM(bForceMap) = true ) {};
	static __forceinline void DrawLineThick(const Vector3& UNUSED_PARAM(v1), const Vector3& UNUSED_PARAM(v2), Color32 UNUSED_PARAM(col1), Color32 UNUSED_PARAM(col2), bool UNUSED_PARAM(bForceMap) = true ) {};
	static __forceinline void DrawPoly(const Vector3& UNUSED_PARAM(a), const Vector3& UNUSED_PARAM(b), const Vector3& UNUSED_PARAM(c), Color32 UNUSED_PARAM(col), bool UNUSED_PARAM(solid) = true, bool UNUSED_PARAM(bForceMap) = true ) {};
	static __forceinline void DrawPoly(const Vector3& UNUSED_PARAM(a), const Vector3& UNUSED_PARAM(b), const Vector3& UNUSED_PARAM(c), Color32 UNUSED_PARAM(col1), Color32 UNUSED_PARAM(col2), Color32 UNUSED_PARAM(col3), bool UNUSED_PARAM(solid) = true, bool UNUSED_PARAM(bForceMap) = true) {};
	static __forceinline void DrawCircle(const Vector3& UNUSED_PARAM(center), float UNUSED_PARAM(radius), Color32 UNUSED_PARAM(col2), bool UNUSED_PARAM(bSolid) = false, bool UNUSED_PARAM(bForceMap) = true  ) {};
	static __forceinline void DrawArc(const Vector3& UNUSED_PARAM(center), float UNUSED_PARAM(radius), float UNUSED_PARAM(startTheta), float UNUSED_PARAM(endTheta), Color32 UNUSED_PARAM(col), bool UNUSED_PARAM(solid) = true, bool UNUSED_PARAM(bForceMap) = true) {};
	static __forceinline void DrawRectAxisAligned(const Vector3& UNUSED_PARAM(min), const Vector3& UNUSED_PARAM(max), Color32 UNUSED_PARAM(col), bool UNUSED_PARAM(solid) = true, bool UNUSED_PARAM(bForceMap) = true) {};
	static __forceinline void DrawWedge(const Vector3&  UNUSED_PARAM(center), float UNUSED_PARAM(radiusInner), float UNUSED_PARAM(radiusOuter), float UNUSED_PARAM(thetaStart), float UNUSED_PARAM(thetaEnd), s32 UNUSED_PARAM(numSegments), Color32 UNUSED_PARAM(col), bool UNUSED_PARAM(bForceMap) = true ) {};
	static __forceinline void DrawMarker(const Vector3& UNUSED_PARAM(center), Color32 UNUSED_PARAM(col2), float UNUSED_PARAM(Scale), bool UNUSED_PARAM(bForceMap) = true  ) {};
	static __forceinline void DrawRemotePlayerCamera(const Matrix34 & UNUSED_PARAM(camMatrix), float UNUSED_PARAM(farClip), float UNUSED_PARAM(tanHFov), Color32 UNUSED_PARAM(col), bool UNUSED_PARAM(bForceMap) = true) {};
	static __forceinline void DrawRemotePlayerCameraCloseVisibleBounds(const Matrix34 & UNUSED_PARAM(camMatrix), float UNUSED_PARAM(nearClip), float UNUSED_PARAM(farClip), float UNUSED_PARAM(tanHFov), Color32 UNUSED_PARAM(col),  float UNUSED_PARAM(fAngleOffset) = 0.0f, bool UNUSED_PARAM(bForceMap) = true);
	static __forceinline void MakeEventRipple(const Vector3& UNUSED_PARAM(pos), float UNUSED_PARAM(finalRadius), unsigned int UNUSED_PARAM(lifetimeMs), const Color32 UNUSED_PARAM(baseColor), bool UNUSED_PARAM(bForceMap) = true) {};
	static __forceinline void DrawString(const Vector3& UNUSED_PARAM(pos), const char * UNUSED_PARAM(String), Color32 UNUSED_PARAM(col), bool UNUSED_PARAM(drawBGQuad), bool UNUSED_PARAM(bForceMap) = true) {};

	static __forceinline void Draw3DBoundingBoxOnVMap(const spdAABB& UNUSED_PARAM(bb), Color32 UNUSED_PARAM(color)) {};
	static __forceinline void Draw2DBoundingBoxOnVMap(const fwRect& UNUSED_PARAM(bb), Color32 UNUSED_PARAM(color)) {};


	static __forceinline void ConvertSizeWorldToMap(float UNUSED_PARAM(worldSize), float& UNUSED_PARAM(mapSize)) {};
	static __forceinline void ConvertSizeMapToWorld(float UNUSED_PARAM(mapSize), float& UNUSED_PARAM(worldSize)) {};
	static __forceinline void ConvertPointWorldToMap(float UNUSED_PARAM(worldX), float UNUSED_PARAM(worldY), float &UNUSED_PARAM(mapX), float &UNUSED_PARAM(mapY)) {};
	static __forceinline void ConvertPointMapToWorld(float UNUSED_PARAM(mapX), float UNUSED_PARAM(mapY), float& UNUSED_PARAM(worldX), float& UNUSED_PARAM(worldY)) {};
	static __forceinline void ConvertPointWorldToMap(const Vector3& UNUSED_PARAM(worldPos), Vector2& UNUSED_PARAM(mapPos)) {};
	static __forceinline void ConvertPointMapToWorld(const Vector2& UNUSED_PARAM(mapPos), Vector3& UNUSED_PARAM(worldPos)) {};

	static __forceinline void ForceMapDisplay() {};

	static __forceinline void CleanSettings() {};
	static __forceinline void GoodSettings() {};
    static __forceinline void SetZoom(float UNUSED_PARAM(zoom)) {};
	static __forceinline void ZoomIn() {};
	static __forceinline void ZoomOut() {};
	static __forceinline void ShiftLeft() {};
	static __forceinline void ShiftRight() {};
	static __forceinline void ShiftUp() {};
	static __forceinline void ShiftDown() {};
};
// DOM-IGNORE-END
#endif //  __DEV

} // namespace rage

#endif // FWDEBUG_VECTORMAP_H
