/////////////////////////////////////////////////////////////////////////////////
// FILE		: debugdraw.cpp
// PURPOSE	: To provide in game visual helpers for debugging.
// AUTHOR	: G. Speirs, Adam Croston, Alex Hadjadj
// STARTED	: 10/08/99
/////////////////////////////////////////////////////////////////////////////////
#include "debugdraw.h"

// Rage headers
#include "grcore/debugdraw.h"
#include "grcore/im.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/threadtype.h"

// Framework Headers

//RENDER_OPTIMISATIONS();
//OPTIMISATIONS_OFF();

#if DEBUG_DRAW


namespace rage
{

namespace fwDebugDraw
{

struct DebugDrawAllocationInterface : public grcDebugDraw::MemoryAllocationInterface
{
	virtual ~DebugDrawAllocationInterface() {}

	virtual void Init()
	{
	};

	virtual void Shutdown()
	{
	};

	virtual void *Alloc(size_t size)
	{
		USE_DEBUG_MEMORY();
		return rage_aligned_new(16) char[size];
	}

	virtual void Free(void *mem)
	{
		USE_DEBUG_MEMORY();
		delete[] (char*)mem;
	}
};

static DebugDrawAllocationInterface g_FrameworkDebugDrawAllocationInterface;

void Init()
{
	grcDebugDraw::SetMemoryAllocationInterface(&g_FrameworkDebugDrawAllocationInterface);
}

} // namespace fwDebugDraw

} // namespace rage

#endif // DEBUG_DRAW
