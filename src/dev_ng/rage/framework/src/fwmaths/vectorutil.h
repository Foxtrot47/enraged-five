// ======================
// fwmaths/vectorutil.h
// (c) 2011 RockstarNorth
// ======================

#ifndef _FWMATHS_VECTORUTIL_H_
#define _FWMATHS_VECTORUTIL_H_

#include "vectormath/classes.h"
#include "math/amath.h" // for Clamp<T>
#include "math/vecmath.h" // moved Sin/Cos approx functions here
#include "math/altivec2.h"
#include "math/vecshift.h" // stuff used to be here
#include "spatialdata/aabb.h"
#include "system/codecheck.h"

#include "fwmaths/Rect.h"
#include "fwutil/xmacro.h"

namespace rage {

class Color32;
class grcViewport;
class spdTransposedPlaneSet4;
class spdTransposedPlaneSet8;

// ================================================================================================

void CalcLinearTable(Vec3V* table, Vec3V_In v0, Vec3V_In v1, ScalarV_In step, int n);
void CalcSinCosTable(Vec3V* table, ScalarV_In b, int n); // calculates {sin,cos,1} triples
void CalcSinCosTable(Vec3V* table, int n);

__forceinline void CalcSinCosTable(Vec2V* table, ScalarV_In b, int n) { CalcSinCosTable(reinterpret_cast<Vec3V*>(table), b, n); }
__forceinline void CalcSinCosTable(Vec2V* table, int n) { CalcSinCosTable(reinterpret_cast<Vec3V*>(table), n); }

__forceinline Vec2V_Out SinCos(ScalarV_In theta) // returns {sin(theta),cos(theta)}
{
	// sin(x + pi/2) = cos(x)
	// cos(x - pi/2) = sin(x)
	return Sin(Vec2V(theta, theta + ScalarV(V_PI_OVER_TWO)));
//	return Cos(Vec2V(theta - ScalarV(V_PI_OVER_TWO), theta));
}

__forceinline Vec2V_Out SinCosFast(ScalarV_In theta) // returns {sin(theta),cos(theta)}
{
	// sin(x + pi/2) = cos(x)
	// cos(x - pi/2) = sin(x)
	return SinFast(Vec2V(theta, theta + ScalarV(V_PI_OVER_TWO)));
//	return CosFast(Vec2V(theta - ScalarV(V_PI_OVER_TWO), theta));
}

// ================================================================================================

__forceinline Vec3V_Out CrossXAxis(Vec3V_In v) { return Vec3V(ScalarV(V_ZERO), v.GetZ(), -v.GetY()); } // same as Cross(v, Vec3V(V_X_AXIS_WZERO))
__forceinline Vec3V_Out CrossYAxis(Vec3V_In v) { return Vec3V(-v.GetZ(), ScalarV(V_ZERO), v.GetX()); } // same as Cross(v, Vec3V(V_Y_AXIS_WZERO))
__forceinline Vec3V_Out CrossZAxis(Vec3V_In v) { return Vec3V(v.GetY(), -v.GetX(), ScalarV(V_ZERO)); } // same as Cross(v, Vec3V(V_Z_AXIS_WZERO))

__forceinline ScalarV_Out PlaneDistanceTo(Vec4V_In plane, Vec3V_In p)
{
#if __XENON // dot products are fast
	return Dot(plane, Vec4V(p, ScalarV(V_ONE)));
#else
	return Dot(plane.GetXYZ(), p) + plane.GetW();
#endif
}

__forceinline Vec4V_Out PlaneNormalise(Vec4V_In plane)
{
	return plane*InvMag(plane.GetXYZ());
}

__forceinline Vec3V_Out PlaneProject(Vec4V_In plane, Vec3V_In p)
{
	return SubtractScaled(p, plane.GetXYZ(), PlaneDistanceTo(plane, p));
}

__forceinline Vec3V_Out PlaneProjectVector(Vec4V_In plane, Vec3V_In v)
{
	return SubtractScaled(v, plane.GetXYZ(), Dot(plane.GetXYZ(), v));
}

__forceinline Vec3V_Out PlaneReflect(Vec4V_In plane, Vec3V_In p)
{
	return SubtractScaled(p, plane.GetXYZ()*ScalarV(V_TWO), PlaneDistanceTo(plane, p));
}

__forceinline Vec3V_Out PlaneReflectVector(Vec4V_In plane, Vec3V_In v)
{
	return SubtractScaled(v, plane.GetXYZ()*ScalarV(V_TWO), Dot(plane.GetXYZ(), v));
}

__forceinline Mat33V_Out PlaneReflectMatrix(Vec4V_In plane, Mat33V_In m)
{
	return Mat33V(
		PlaneReflectVector(plane, m.GetCol0()),
		PlaneReflectVector(plane, m.GetCol1()),
		PlaneReflectVector(plane, m.GetCol2())
	);
}

__forceinline Mat34V_Out PlaneReflectMatrix(Vec4V_In plane, Mat34V_In m)
{
	return Mat34V(
		PlaneReflectVector(plane, m.GetCol0()),
		PlaneReflectVector(plane, m.GetCol1()),
		PlaneReflectVector(plane, m.GetCol2()),
		PlaneReflect      (plane, m.GetCol3())
	);
}

__forceinline Vec4V_Out BuildPlane(Vec3V_In p, Vec3V_In n)
{
	return Vec4V(n, -Dot(p, n));
}

__forceinline Vec4V_Out BuildPlane(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2)
{
	return BuildPlane(p0, Normalize(Cross(p1 - p0, p2 - p0)));
}

__forceinline Vec4V_Out BuildSilhouettePlane(Vec3V_In p, Vec3V_In edge, Vec3V_In shadowDir, Vec3V_In centroid)
{
	const Vec4V plane = BuildPlane(p, NormalizeSafe(Cross(edge, shadowDir), Vec3V(V_FLT_SMALL_5), Vec3V(V_ZERO)));
	const ScalarV sig = PlaneDistanceTo(plane, centroid);

	return Xor(plane, Vec4V(Andc(ScalarV(V_80000000), sig))); // negates plane if sig is positive
}

__forceinline Vec4V_Out TransformOrthoPlane(Mat33V_In mat, Vec4V_In plane) // UNTESTED -- transforms a plane by an orthonormal 3x3 matrix
{
	const Vec3V   n = Multiply(mat, plane.GetXYZ());
	const ScalarV d = plane.GetW();

	return Vec4V(n, d);
}

__forceinline Vec4V_Out TransformOrthoPlane(Mat34V_In mat, Vec4V_In plane) // UNTESTED -- transforms a plane by an orthonormal 3x4 matrix
{
	const Vec3V   n = Transform3x3(mat, plane.GetXYZ());
	const ScalarV d = plane.GetW() - Dot(n, mat.d());

	return Vec4V(n, d);
}

__forceinline Vec3V_Out ComputeIntersectionOf3Planes(Vec4V_In plane1, Vec4V_In plane2, Vec4V_In plane3)
{
	// http://astronomy.swin.edu.au/~pbourke/geometry/3planes/
	const Vec3V c23 = Cross(plane2.GetXYZ(), plane3.GetXYZ());
	const Vec3V c31 = Cross(plane3.GetXYZ(), plane1.GetXYZ());
	const Vec3V c12 = Cross(plane1.GetXYZ(), plane2.GetXYZ());

	const ScalarV d1 = SplatW(plane1);
	const ScalarV d2 = SplatW(plane2);
	const ScalarV d3 = SplatW(plane3);

	const Vec3V p = AddScaled(AddScaled(Scale(c23, d1), c31, d2), c12, d3); // d1*c23 + d2*c31 + d3*c12

	return InvScale(Negate(p), Dot(plane1.GetXYZ(), c23));
}

__forceinline Vec4V_Out TransformSphere(Mat33V_In mat, Vec4V_In sphere)
{
	return Vec4V(Multiply(mat, sphere.GetXYZ()), sphere.GetW());
}

__forceinline Vec4V_Out TransformSphere(Mat34V_In mat, Vec4V_In sphere)
{
	return Vec4V(Transform(mat, sphere.GetXYZ()), sphere.GetW());
}

__forceinline Vec3V_Out TransformProjective(Mat44V_In mat, Vec3V_In v)
{
	const Vec4V h = Multiply(mat, Vec4V(v, ScalarV(V_ONE)));
	return h.GetXYZ()/h.GetW();
}

__forceinline Mat34V_Out RageCameraMtxToGameCameraMtx(Mat34V_In mat)
{
	return Mat34V(mat.a(), Negate(mat.c()), mat.b(), mat.d());
}

__forceinline Mat34V_Out GameCameraMtxToRageCameraMtx(Mat34V_In mat)
{
	return Mat34V(mat.a(), mat.c(), Negate(mat.b()), mat.d());
}


#if RSG_DURANGO
#pragma warning(push)
#pragma warning(disable:4723) // warning C4723: potential divide by 0
#endif
// ================================================================================================

__forceinline float PerspectiveInterp(float z0, float z1, float t, float perspective)
{
	const float z_lin = z0 + (z1 - z0)*t;

	// This was doing a Min before, but that's still two compares anyway and caused the
	// compiler to emit a bogus "possible divide by zero" warning on 2012/Durango.
	if (z0 <= 0.0f || z1 <= 0.0f)
	{
		return z_lin;
	}

	const float z_exp = z0*powf(z1/z0, t);

	return z_lin + (z_exp - z_lin)*perspective;
}
#if RSG_DURANGO
#pragma warning(pop)
#endif


// ================================================================================================
// === clip

int  PolyClip                         (Vec3V* dst, int dstCountMax, const Vec3V* src, int srcCount, Vec4V_In plane);
bool PolyClipEdge                     (Vec3V edge[2], const Vec3V* src, int srcCount, Vec4V_In plane);
int  PolyClipEx                       (Vec4V* dst, const Vec4V* src, int srcCount, Vec4V_In plane, ScalarV_In planeId); // saves planeId in w component, assumes dst array is large enough
int  PolyClipToFrustum                (Vec3V* dst, const Vec3V* src, int srcCount, Mat44V_In viewProj);
int  PolyClipToFrustumWithoutNearPlane(Vec3V* dst, const Vec3V* src, int srcCount, Mat44V_In viewProj);

// ================================================================================================

namespace Vec {

// V3MinElementMask({x,y,z}) returns:
//    V_T_F_F_F if x == y == z
//    V_T_F_F_F if x <= min(y,z) && x < y (x is lowest, or x==y are both lower than z)
//    V_F_T_F_F if y <= min(z,x) && y < z (y is lowest, or y==z are both lower than x)
//    V_F_F_T_F if z <= min(x,y) && z < x (z is lowest, or z==x are both lower than y)
//
__forceinline Vector_4V_Out V3MinElementMask(Vector_4V_In xyz)
{
	const Vector_4V yzx        = V4Permute<Y,Z,X,W>(xyz);
	const Vector_4V zxy        = V4Permute<Z,X,Y,W>(xyz);
	const Vector_4V xyz_LT_yzx = V4IsLessThanV(xyz, yzx);
	const Vector_4V xyz_LE_zxy = V4IsLessThanOrEqualV(xyz, zxy);
	const Vector_4V a          = V4And(xyz_LT_yzx, xyz_LE_zxy);

	// a.x: x < y && x <= z
	// a.y: y < z && y <= x
	// a.z: z < x && z <= y
	// a is the min element mask, except if x==y==z in which case a will be zero

#if RSG_CPU_SPU
	const Vector_4V axyz   = (Vector_4V)si_orx((qword)a);
#else
	const Vector_4V ax     = V4SplatX(a);
	const Vector_4V ay     = V4SplatY(a);
	const Vector_4V az     = V4SplatZ(a);
	const Vector_4V axy    = V4Or(ax, ay);
	const Vector_4V axyz   = V4Or(axy, az);
#endif
	const Vector_4V result = V4SelectFT(axyz, V4VConstant(V_T_F_F_F), xyz);

	return result;
}

// less clever, possibly faster for non-SPU code (although it relies on more constants)
// results may not match first version exactly if there isn't a single minimum element
//
__forceinline Vector_4V_Out V3MinElementMask_alt(Vector_4V_In xyz)
{
	const Vector_4V x       = V4SplatX(xyz);
	const Vector_4V y       = V4SplatY(xyz);
	const Vector_4V z       = V4SplatZ(xyz);
	const Vector_4V y_LT_x  = V4IsLessThanV(y, x);
	const Vector_4V min_xy  = V4Min(x, y);
	const Vector_4V z_LT_xy = V4IsLessThanV(z, min_xy);
	const Vector_4V temp_1  = V4SelectFT(y_LT_x, V4VConstant(V_T_F_F_F), V4VConstant(V_F_T_F_F));
	const Vector_4V temp_2  = V4SelectFT(z_LT_xy, temp_1, V4VConstant(V_F_F_T_F));

	return temp_2;
}

} // namespace Vec

__forceinline VecBoolV_Out MinElementMask    (Vec3V_In v) { return VecBoolV(Vec::V3MinElementMask    (v.GetIntrin128())); }
__forceinline VecBoolV_Out MinElementMask_alt(Vec3V_In v) { return VecBoolV(Vec::V3MinElementMask_alt(v.GetIntrin128())); }

inline Vec3V_Out FindClosestPointOnAABB(const spdAABB& box, Vec3V_In point)
{
	if (box.ContainsPoint(point))
	{
		// const Vec3V boxMin = box.GetMin();
		// const Vec3V boxMax = box.GetMax();
		const Vec3V centre = box.GetCenter();
		const Vec3V extent = box.GetExtent();

		const Vec3V v = point - centre;
		const Vec3V d = extent - Abs(v); // distance to face
		const Vec3V s = v & Vec3V(V_80000000); // sign

		const Vec3V dmin = Vec3V(MinElement(d));
		const Vec3V dcmp = Vec3V(IsEqual(dmin, d));

		// note that if dcmp contains more than one TRUE element the resulting point will be on an edge or a corner - in practice this is very unlikely to happen
		return point + (dcmp & (d | s));
	}
	else
	{
		return Clamp(point, box.GetMin(), box.GetMax());
	}
}

// ================================================================================================

// http://www.ahinson.com/algorithms_general/Sections/InterpolationRegression/RegressionPlanar.pdf
class fwBestFitPlane
{
public:
	fwBestFitPlane() : m_sum_x1_y1_z1_1(V_ZERO), m_sum_xx_yy_zz(V_ZERO), m_sum_xy_yz_zx(V_ZERO) {}

	__forceinline void AddPoint(Vec3V_In p)
	{
		m_sum_x1_y1_z1_1 += Vec4V(p, ScalarV(V_ONE));
		m_sum_xx_yy_zz   += p*p;
		m_sum_xy_yz_zx   += p*p.Get<Vec::Y,Vec::Z,Vec::X>();
	}

	Vec4V_Out GetPlane() const;

private:
	Vec4V m_sum_x1_y1_z1_1;
	Vec3V m_sum_xx_yy_zz;
	Vec3V m_sum_xy_yz_zx;
};

// 2D barycentric
// http://en.wikipedia.org/wiki/Barycentric_coordinate_system_(mathematics)
inline Vec3V_Out BarycentricCoords(Vec2V_In p0, Vec2V_In p1, Vec2V_In p2, Vec2V_In p3)
{
	const ScalarV q  = Invert((p2 - p3).GetY()*(p1 - p3).GetX() + (p3 - p2).GetX()*(p1 - p3).GetY());
	const ScalarV b1 = q*    ((p2 - p3).GetY()*(p0 - p3).GetX() + (p3 - p2).GetX()*(p0 - p3).GetY());
	const ScalarV b2 = q*    ((p3 - p1).GetY()*(p0 - p3).GetX() + (p1 - p3).GetX()*(p0 - p3).GetY());

	return Vec3V(b1, b2, ScalarV(V_ONE) - b1 - b2);
}

// 3D barycentric
// http://en.wikipedia.org/wiki/Barycentric_coordinate_system_(mathematics)
inline Vec4V_Out BarycentricCoords(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2, Vec3V_In p3, Vec3V_In p4)
{
	Mat33V mat(p1 - p4, p2 - p4, p3 - p4);
	Mat33V inv;
	InvertFull(inv, mat);
	const Vec3V b = Multiply(inv, p0 - p4);
	return Vec4V(b, ScalarV(V_ONE) - b.GetX() - b.GetY() - b.GetZ());
}

// http://www.terathon.com/code/tangent.html
// note that s,t vectors returned are not normalised
inline FASTRETURNCHECK(bool) CalculateTriangleTangentSpace(Vec3V_InOut s, Vec3V_InOut t, Vec3V_In v1, Vec3V_In v2, Vec3V_In v3, Vec2V_In w1, Vec2V_In w2, Vec2V_In w3, ScalarV_In threshold = ScalarV(V_FLT_SMALL_4))
{
	const Vec3V v21 = v2 - v1;
	const Vec3V v31 = v3 - v1;
	const Vec2V w21 = w2 - w1;
	const Vec2V w31 = w3 - w1;

	const ScalarV s1 = w21.GetX();
	const ScalarV t1 = w21.GetY();
	const ScalarV s2 = w31.GetX();
	const ScalarV t2 = w31.GetY();

	const ScalarV denom = s1*t2 - s2*t1;

	if (IsGreaterThanAll(Abs(denom), threshold))
	{
		const ScalarV r = Invert(denom);

		s = (v21*t2 - v31*t1)*r;
		t = (v31*s1 - v21*s2)*r;

		return true;
	}

	s = Vec3V(V_ZERO);
	t = Vec3V(V_ZERO);

	return false;
}

template <typename T> inline ScalarV_Out CalculateTriangleArea(const T& p0, const T& p1, const T& p2)
{
	const ScalarV a = Mag(p1 - p0);
	const ScalarV b = Mag(p2 - p1);
	const ScalarV c = Mag(p0 - p2);
	const ScalarV s = (a + b + c)*ScalarV(V_HALF);

	return Sqrt(Max(ScalarV(V_ZERO), s*(s - a)*(s - b)*(s - c)));
}

inline void CalculateFrustumBounds(Vec3V_InOut boundsMin, Vec3V_InOut boundsMax, Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In z0, ScalarV_In z1)
{
	const Vec3V x = +cameraMtx.GetCol0();
	const Vec3V y = +cameraMtx.GetCol1();
	const Vec3V z = -cameraMtx.GetCol2();
	const Vec3V t = +cameraMtx.GetCol3();

	const Vec3V e = Abs(x)*tanHFOV + Abs(y)*tanVFOV;

	boundsMin = t + Min((z - e)*z0, (z - e)*z1);
	boundsMax = t + Max((z + e)*z0, (z + e)*z1);

	// this code is not as efficient, but i'm leaving it for reference ..
	//
	//const Vec3V p0 = Transform(cameraMtx, Vec3V(-tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*z0);
	//const Vec3V p1 = Transform(cameraMtx, Vec3V(+tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*z0);
	//const Vec3V p2 = Transform(cameraMtx, Vec3V(-tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*z0);
	//const Vec3V p3 = Transform(cameraMtx, Vec3V(+tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*z0);
	//const Vec3V p4 = Transform(cameraMtx, Vec3V(-tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*z1);
	//const Vec3V p5 = Transform(cameraMtx, Vec3V(+tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*z1);
	//const Vec3V p6 = Transform(cameraMtx, Vec3V(-tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*z1);
	//const Vec3V p7 = Transform(cameraMtx, Vec3V(+tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*z1);
	//
	//boundsMin = Min(Min(p0, p1, p2, p3), Min(p4, p5, p6, p7));
	//boundsMax = Max(Max(p0, p1, p2, p3), Max(p4, p5, p6, p7));
}

inline void CalculateFrustumBoundsAsymmetrical(Vec3V_InOut boundsMin, Vec3V_InOut boundsMax, Mat34V_In cameraMtx, const fwRect& tanBounds, ScalarV_In z0, ScalarV_In z1)
{
	const ScalarV x0(tanBounds.left  ); // tanHFOV*(-1 + shear.x)
	const ScalarV y0(tanBounds.bottom); // tanVFOV*(-1 + shear.y)
	const ScalarV x1(tanBounds.right ); // tanHFOV*(+1 + shear.x)
	const ScalarV y1(tanBounds.top   ); // tanVFOV*(+1 + shear.y)

	const Vec3V p0 = Transform(cameraMtx, Vec3V(x0, y0, ScalarV(V_NEGONE))*z0);
	const Vec3V p1 = Transform(cameraMtx, Vec3V(x1, y0, ScalarV(V_NEGONE))*z0);
	const Vec3V p2 = Transform(cameraMtx, Vec3V(x0, y1, ScalarV(V_NEGONE))*z0);
	const Vec3V p3 = Transform(cameraMtx, Vec3V(x1, y1, ScalarV(V_NEGONE))*z0);
	const Vec3V p4 = Transform(cameraMtx, Vec3V(x0, y0, ScalarV(V_NEGONE))*z1);
	const Vec3V p5 = Transform(cameraMtx, Vec3V(x1, y0, ScalarV(V_NEGONE))*z1);
	const Vec3V p6 = Transform(cameraMtx, Vec3V(x0, y1, ScalarV(V_NEGONE))*z1);
	const Vec3V p7 = Transform(cameraMtx, Vec3V(x1, y1, ScalarV(V_NEGONE))*z1);

	boundsMin = Min(Min(p0, p1, p2, p3), Min(p4, p5, p6, p7));
	boundsMax = Max(Max(p0, p1, p2, p3), Max(p4, p5, p6, p7));
}

void CalculateFrustumBounds(Vec3V_InOut boundsMin, Vec3V_InOut boundsMax, const grcViewport& viewport, bool bUseAsymmetrical = false);

spdTransposedPlaneSet4 GenerateFrustum4(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV);
spdTransposedPlaneSet8 GenerateFrustum8(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In nearClip, ScalarV_In farClip);

spdTransposedPlaneSet4 GenerateConeFrustum4(Vec3V_In pos, Vec3V_In dir, Vec3V_In tangent, ScalarV_In tanHalfAngle);
spdTransposedPlaneSet8 GenerateConeFrustum8(Vec3V_In pos, Vec3V_In dir, Vec3V_In tangent, ScalarV_In tanHalfAngle);

Mat44V_Out CalculatePerspectiveProjectionMatrix(ScalarV_In nearClip, ScalarV_In farClip);

#if !__SPU
spdTransposedPlaneSet4 GenerateSweptBox2D(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In farClip, Vec3V_In sweep);
spdTransposedPlaneSet4 GenerateSweptBox2D(const grcViewport& viewport, Vec3V_In sweep);
// TODO -- GenerateSweptFrustum appears to be broken at the moment ..
spdTransposedPlaneSet8 GenerateSweptFrustum(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In farClip, Vec3V_In sweep BANK_PARAM(int* pActualPlaneCount = NULL) BANK_PARAM(Vec4V* extraPlanes = NULL) BANK_PARAM(u32 flags = 0x0003ffff));
spdTransposedPlaneSet8 GenerateSweptFrustum(const grcViewport& viewport, Vec3V_In sweep BANK_PARAM(int* pActualPlaneCount = NULL));
#if __BANK
void PlaneSetDebugDraw(const Vec4V* planes, int numPlanes, const Color32& colour);
void PlaneSetDebugDraw(const spdTransposedPlaneSet4& planeSet, const Color32& colour);
void PlaneSetDebugDraw(const spdTransposedPlaneSet8& planeSet, const Color32& colour);
#endif // __BANK
#endif // !__SPU

bool TestLineSegmentAgainstViewport(Vec3V_In p0, Vec3V_In p1, const grcViewport& viewport);

} // namespace rage

#endif // _FWMATHS_VECTORUTIL_H_
