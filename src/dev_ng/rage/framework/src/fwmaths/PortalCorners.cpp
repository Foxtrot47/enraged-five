// Rage headers
#include "grcore/debugdraw.h"
#include "grcore/viewport.h"
#include "spatialdata/sphere.h"
#include "spatialdata/plane.h"

// Framework headers
#include "fwmaths/PortalCorners.h"
#include "fwmaths/vectorutil.h"
#include "fwmaths/geomutil.h"
#include "timecycle/optimisations.h"

TIMECYCLE_OPTIMISATIONS()

namespace rage {
#if __BANK
bool bShowTCScreenSpacePortals = false;
bool bShowTCScreenSpaceBorders = false;
float fDebugRenderScale = 0.25f;
#endif // __BANK

void fwPortalCorners::SetCorners(const Vector3 corners[4])
{
	float w0 = m_corners[0].GetW(); 
	float w1 = m_corners[1].GetW(); 
	float w2 = m_corners[2].GetW(); 
	float w3 = m_corners[3].GetW(); 

	m_corners[0] = corners[0];
	m_corners[1] = corners[1];
	m_corners[2] = corners[2];
	m_corners[3] = corners[3];

	m_corners[0].SetW(w0);
	m_corners[1].SetW(w1);
	m_corners[2].SetW(w2);
	m_corners[3].SetW(w3);
}

// calculate a bounding sphere for the portal (isn't accurate, as only uses 2 corners)
void fwPortalCorners::CalcBoundingSphere(spdSphere& boundSphere) const
{
	const Vec3V		centroidOffset = VECTOR3_TO_VEC3V(m_corners[0] - m_corners[2])*ScalarV(V_HALF);
	const Vec3V		centroid = RCC_VEC3V(m_corners[0]) - centroidOffset;
	const ScalarV	radius = Mag(centroidOffset);

	boundSphere.Set(centroid, radius);
}

void fwPortalCorners::CalcBoundingBox(spdAABB& boundBox) const
{
	const Vec3V		a = VECTOR3_TO_VEC3V( m_corners[0] );
	const Vec3V		b = VECTOR3_TO_VEC3V( m_corners[1] );
	const Vec3V		c = VECTOR3_TO_VEC3V( m_corners[2] );
	const Vec3V		d = VECTOR3_TO_VEC3V( m_corners[3] );
	const Vec3V		bbMin = Min( Min(a,b), Min(c,d) );
	const Vec3V		bbMax = Max( Max(a,b), Max(c,d) );
	boundBox.Set( bbMin, bbMax );
}

static void PCClipPlane(Vec4V_In clipPlane, atRangeArray<Vec3V,8>  *src, int vertCountSrc, atRangeArray<Vec3V,8>  *dst, int &vertCountDst)
{
	Vec3V   p0 = (*src)[vertCountSrc - 1];
	ScalarV d0 = PlaneDistanceTo(clipPlane, p0);
	u32     g0 = IsGreaterThanAll(d0, ScalarV(V_ZERO));

	for (int i = 0; i < vertCountSrc; i++)
	{
		const Vec3V   p1 = (*src)[i];
		const ScalarV d1 = PlaneDistanceTo(clipPlane, p1);
		const u32     g1 = IsGreaterThanAll(d1, ScalarV(V_ZERO));

		
		if (g1 != g0) 
		{ 
			Vec3V res = (p0*d1 - p1*d0)/(d1 - d0);
			res.SetW(ScalarV(V_ONE));
			(*dst)[vertCountDst++] = res; 
		}
		if (g1      ) 
		{	
			Vec3V res = p1;
			res.SetW(ScalarV(V_ZERO));
			(*dst)[vertCountDst++] = res; 
		}


		p0 = p1;
		d0 = d1;
		g0 = g1;
	}
}

bank_float minFudgeDist = 0.25f; // matches portal detection fudge.

float fwPortalCorners::CalcScreenSurface(const grcViewport& viewPort) const
{
	spdSphere sphere;
	CalcBoundingSphere(sphere);

	if( cullOutside == viewPort.IsSphereVisible(sphere.GetV4()) )
		return 0.0f;

	
	// Transform into camera space
	Vec3V base[4];
	float surface = 0.0f;
	
	for(int ii=0;ii<4;ii++)
	{
		base[ii] = VECTOR3_TO_VEC3V(m_corners[ii]);
	}

	atRangeArray<Vec3V,8> vertListA;
	int vertCountA = 0;
	atRangeArray<Vec3V,8> vertListB;
	int vertCountB = 0;
	
	
	vertListA[vertCountA] = base[0];
	vertCountA++;

	vertListA[vertCountA] = base[1];
	vertCountA++;

	vertListA[vertCountA] = base[2];
	vertCountA++;

	vertListA[vertCountA] = base[3];
	vertCountA++;

	atRangeArray<Vec3V,8>  *src = &vertListA;
	int vertCountSrc = vertCountA;
	atRangeArray<Vec3V,8>  *dst = &vertListB;
	int vertCountDst = vertCountB;
	int i;

	bool clipped = false;
	for(int clipId = 0;clipId <= grcViewport::CLIP_PLANE_NEAR;clipId++)
	{
		Vec4V clipPlane = viewPort.GetFrustumClipPlane(clipId);

		PCClipPlane(clipPlane, src, vertCountSrc, dst, vertCountDst);
		
		if( vertCountDst < 3 )
		{
			clipped = true;
			break;
		}
			
		atRangeArray<Vec3V,8>  *tmp = src;
		src = dst;
		dst = tmp;

		vertCountSrc = vertCountDst;
		vertCountDst = 0;
	}

	float dist = 10000.0f;
	bool closeness = CalcCloseness(VEC3V_TO_VECTOR3(viewPort.GetCameraPosition()), dist);

	if( clipped == false && (closeness == false || dist > viewPort.GetNearClip()) )
	{
		atRangeArray<Vec3V,8>  results;
		int transCount = 0;

		Mat44V fullComp = viewPort.GetFullCompositeMtx();
		
		// Transform & Project
		const Vec3V lowBound(V_ZERO);
		const Vec3V highBound((float)GRCDEVICE.GetWidth(),(float)GRCDEVICE.GetHeight(),1.0f);
		const Vec3V halfHighBound = highBound *Vec3V(V_HALF);
		
		for(int ii=0;ii<vertCountSrc;ii++)
		{
			const Vec4V transformed = Multiply(fullComp,Vec4V((*src)[ii],ScalarV(V_ONE)));
			Vec3V projected = transformed.GetXYZ() / transformed.GetW();

			// Dodgy precision fixes...
			
			// Offset
			Vec3V offset = (projected - halfHighBound) * Vec3V(halfHighBound);
			projected += offset * (*src)[ii].GetW();
			
			// Clamp results
			results[transCount] = Clamp(projected, lowBound, highBound);
			
			transCount++;
		}

		surface = 0.0f;
		for(i = 0;i<transCount-1;i++)
		{
			surface += (results[i].GetXf() * results[i+1].GetYf() - results[i].GetYf() * results[i+1].GetXf());
		}

		surface += (results[i].GetXf() * results[0].GetYf() - results[i].GetYf() * results[0].GetXf());

		surface *= 0.5f;

		
		
#if __BANK
		if( bShowTCScreenSpacePortals )
		{
			Vec2V ratio(1.0f/(float)GRCDEVICE.GetWidth(),1.0f/(float)GRCDEVICE.GetHeight());
			
			ratio *= ScalarV(fDebugRenderScale);
			Vec2V offset = Vec2V(0.5f,0.5f) * ScalarV(Clamp(1.0f - fDebugRenderScale,0.0f,1.0f));
			if( bShowTCScreenSpaceBorders )
			{
				grcDebugDraw::Line(offset + Vec2V(0.0f,0.0f) * ScalarV(fDebugRenderScale), offset + Vec2V(0.0f,1.0f) * ScalarV(fDebugRenderScale), Color32(0,0,0xff));
				grcDebugDraw::Line(offset + Vec2V(0.0f,1.0f) * ScalarV(fDebugRenderScale), offset + Vec2V(1.0f,1.0f) * ScalarV(fDebugRenderScale), Color32(0,0,0xff));
				grcDebugDraw::Line(offset + Vec2V(1.0f,1.0f) * ScalarV(fDebugRenderScale), offset + Vec2V(1.0f,0.0f) * ScalarV(fDebugRenderScale), Color32(0,0,0xff));
				grcDebugDraw::Line(offset + Vec2V(1.0f,0.0f) * ScalarV(fDebugRenderScale), offset + Vec2V(0.0f,0.0f) * ScalarV(fDebugRenderScale), Color32(0,0,0xff));
			}			
			
			Color32 col1 = Color32(0x00,0xFF,0x00);
			Color32 col2 = Color32(0xFF,0x00,0x00);
			for(i = 0;i<transCount-1;i++)
			{
				grcDebugDraw::Line(offset + results[i].GetXY()*ratio,offset + results[i+1].GetXY()*ratio, (*src)[i].GetWf() == 1.0f ? col2 : col1, (*src)[i+1].GetWf() == 1.0f ? col2 : col1 );
			}

			grcDebugDraw::Line(offset + results[i].GetXY()*ratio,offset + results[0].GetXY()*ratio, (*src)[i].GetWf() == 1.0f ? col2 : col1, (*src)[0].GetWf() == 1.0f ? col2 : col1);
			
		}
#endif // __BANK		
	}
	else if( closeness && Abs(dist) < minFudgeDist)
	{
		const float height = (float)GRCDEVICE.GetHeight();
		const float width = (float)GRCDEVICE.GetWidth();

		const Vec3V line1 = VECTOR3_TO_VEC3V(m_corners[0]) - VECTOR3_TO_VEC3V(m_corners[1]);
		const Vec3V line2 = VECTOR3_TO_VEC3V(m_corners[1]) - VECTOR3_TO_VEC3V(m_corners[2]);

		const Vec3V cross = Cross(line1, line2);
		const Vec3V normal = NormalizeFast(cross);
		
		const float dotp = Dot(normal, viewPort.GetCameraMtx().GetCol2()).Getf();

		const float DPFactor = Lerp(dotp,0.5f,1.0f);
		surface = height*width*DPFactor;
	}
	
#if __BANK	
	if( bShowTCScreenSpacePortals )
	{
		float surf = Abs(surface);

		const float height = (float)GRCDEVICE.GetHeight();
		const float width = (float)GRCDEVICE.GetWidth();

		const Vec3V line1 = VECTOR3_TO_VEC3V(m_corners[0]) - VECTOR3_TO_VEC3V(m_corners[1]);
		const Vec3V line2 = VECTOR3_TO_VEC3V(m_corners[1]) - VECTOR3_TO_VEC3V(m_corners[2]);

		const Vec3V cross = Cross(line1, line2);
		const Vec3V normal = NormalizeFast(cross);
		
		const float dotp = Dot(normal, viewPort.GetCameraMtx().GetCol2()).Getf();

		const float DPFactor = Lerp(dotp,0.5f,1.0f);
		const float guessedSurface = height*width*DPFactor;
		
		grcDebugDraw::AddDebugOutput("%p : %d - %d - %f - %f - %f - %f",this,clipped,closeness,dist,surf,guessedSurface,dotp);

	}
#endif // __BANK	
	return Abs(surface);
}

// determine if the given line segment intersects with the portal - but only in the direction of movement
bool fwPortalCorners::HasPassedThrough(const fwGeom::fwLine& testLine, const bool backfaceCull) const
{
	if (testLine.IsCollidingWithTri(m_corners[0], m_corners[1], m_corners[2], backfaceCull)) {
		return(true);
	}
	if (testLine.IsCollidingWithTri(m_corners[2], m_corners[3], m_corners[0], backfaceCull)) {
		return(true);
	}

	return(false);
}

bool fwPortalCorners::HasPassedThrough(const Vec3V& start, const Vec3V& end) const
{
	const Vec3V corner0 = GetCornerV(0);
	const Vec3V corner1 = GetCornerV(1);
	const Vec3V corner2 = GetCornerV(2);
	
	const Vec3V line1 = corner0 - corner1;
	const Vec3V line2 = corner1 - corner2;
	
	const Vec3V cross = Cross(line1, line2);
	const Vec3V normal = NormalizeFast(cross);

	spdPlane plane(corner0,normal);
	
	Vec3V out;
	BoolV res = plane.IntersectLineV(start,end,out);

	ScalarV dot1 = Dot(corner0 - out,line1);
	ScalarV dot2 = Dot(corner0 - out,line2);
	
	BoolV dot1Gr = IsGreaterThanOrEqual(dot1,ScalarV(V_ZERO));
	BoolV dot1Le = IsLessThanOrEqual(dot1,MagSquared(line1));
	BoolV dot2Gr = IsGreaterThanOrEqual(dot2,ScalarV(V_ZERO));
	BoolV dot2Le = IsLessThanOrEqual(dot2,MagSquared(line2));

	BoolV dot1Valid = And(dot1Gr,dot1Le);
	BoolV dot2Valid = And(dot2Gr,dot2Le);
		
	BoolV result = And(res,And(dot1Valid,dot2Valid));
	
	return result.Getb();
}

// determine if the given point is on the visible side of this portal (portal normal towards the point)
bool fwPortalCorners::IsOnPositiveSide(const Vector3& testPos) const
{
	Vector3 line1 = (m_corners[0] - m_corners[1]);
	Vector3 line2 = (m_corners[1] - m_corners[2]);

	Vector3 normal;
	normal.Cross(line1, line2);

	if (normal.Dot((testPos - m_corners[1])) < 0.0f){
		return(false);
	} else {
		return(true);
	}
}

// determine if the point is very close (10cm) to the surface of the portal and return which side it's on.
// Use 3 points in poly (1,2,3); calc closest point A from test to line 1->2, then calc closest point from
// test to line A->3. Returns false if closest point isn't on the poly.
bool fwPortalCorners::CalcCloseness(const Vector3& testPos, float &dist) const {

	fwGeom::fwLine line1(m_corners[0], m_corners[1]);
	fwGeom::fwLine line2(m_corners[3], m_corners[2]);
	dist = 10000.0f;

	float	ratioOnLine;
	Vector3	closestPointOnLine1;
	ratioOnLine = line1.FindClosestPointOnLine( testPos, closestPointOnLine1 );

	// fail if closest point is at either end of line (because closest point is not within poly)
	if (ratioOnLine <= 0.00001f || ratioOnLine >= 0.99999f){
		return(false);
	}

	Vector3	closestPointOnLine2;
	ratioOnLine = line2.FindClosestPointOnLine( testPos, closestPointOnLine2 );

	// fail if closest point is at either end of line (because closest point is not within poly)
	if (ratioOnLine <= 0.00001f || ratioOnLine >= 0.99999f){
		return(false);
	}

	fwGeom::fwLine	line3(closestPointOnLine1,closestPointOnLine2);
	Vector3 closestPointOnQuad;
	ratioOnLine = line3.FindClosestPointOnLine( testPos, closestPointOnQuad);

	// fail if closest point is at either end of line (because closest point is not within poly)
	if (ratioOnLine <= 0.00001f || ratioOnLine >= 0.99999f){
		return(false);
	}

	// return the distance of the closest point on the portal surface
	dist = (closestPointOnQuad - testPos).Mag();

	// modify distance depending on which side of the poly we are on
	if ( !IsOnPositiveSide(testPos)){
		dist = -dist;
	}

	return(true);
}

// determine the distance between the point and the portal quad (assume portal is rectangular)
ScalarV_Out fwPortalCorners::CalcDistanceToPoint(Vec3V_In p) const
{
	const Vec3V centre = VECTOR3_TO_VEC3V(m_corners[0] + m_corners[2])*ScalarV(V_HALF); // world centre
	const Vec3V basisX = VECTOR3_TO_VEC3V(m_corners[1] - m_corners[0]);
	const Vec3V basisY = VECTOR3_TO_VEC3V(m_corners[3] - m_corners[0]);
	const Vec3V basisZ = Cross(basisX, basisY);

	Vec3V m0, m1, m2; Transpose3x3(m0, m1, m2, basisX, basisY, basisZ);

	const Vec3V basisM = Sqrt(m0*m0 + m1*m1 + m2*m2); // magnitude of {basisX,basisY,basisZ}
	const Vec3V extent = Vec3V(basisM.GetXY(), ScalarV(V_ZERO))*ScalarV(V_HALF); // local extent

	const Vec3V v = p - centre;

	return Mag(Max(Vec3V(V_ZERO), Abs(m0*v.GetX() + m1*v.GetY() + m2*v.GetZ())/basisM - extent));
}

ScalarV_Out fwPortalCorners::CalcSimpleDistanceToPointSquared(Vec3V_In p) const
{
	const Vec3V corner0 = GetCornerV(0);
	const Vec3V corner2 = GetCornerV(2);

	const Vec3V centroidOffset = (corner0 - corner2)*ScalarV(V_HALF);
	const Vec3V center = corner0 - centroidOffset;

	const Vec3V centerToPos = (p - center);
	const ScalarV sphereDistSq = MagSquared(centerToPos);
	
	return sphereDistSq;
}

int fwPortalCorners::ClipToFrustumWithoutNearPlane(Vec2V out[4 + 5], Mat44V_In compositeMtx) const
{
	Vec3V clipped[4 + 5];
	const int count = PolyClipToFrustumWithoutNearPlane(clipped, (const Vec3V*)m_corners, 4, compositeMtx);

	for (int i = 0; i < count; i++)
	{
		const Vec4V temp = Multiply(compositeMtx, Vec4V(clipped[i], ScalarV(V_ONE)));
		out[i] = temp.GetXY()/temp.GetW(); // [-1..1]
	}

	return count;
}

float fwPortalCorners::CalcAmbientScaleFeather(Vec3V_In coords) const
{
	const ScalarV radPFactor(1.25f * 1.25f);
	
	// Grab corners
	const Vec3V corner0 = GetCornerV(0);
	const Vec3V corner1 = GetCornerV(1);
	const Vec3V corner2 = GetCornerV(2);
	/// const Vec3V corner3 = GetCornerV(3);
	
	// Calc portal's normal and width vector
	const Vec3V axis1 = corner0 - corner1;
	const Vec3V axis2 = corner1 - corner2;
	
	const BoolV unitSelect = IsGreaterThan(Abs(axis1.GetZ()),Abs(axis2.GetZ()));
	const Vec3V line1 = SelectFT(unitSelect,axis1,axis2);
	const Vec3V line2 = SelectFT(unitSelect,axis2,axis1);
	
	const Vec3V cross = Cross(line1, line2);
	const Vec3V normal = NormalizeFast(cross);
	const Vec3V unit1 = NormalizeFast(line1);
	const Vec3V unit2 = NormalizeFast(line2);
	
	// Portal center
	const Vec3V centroidOffset = (corner0 - corner2)*ScalarV(V_HALF);
	const Vec3V center = corner0 - centroidOffset;

	// Project center to Pos onto plans
	const Vec3V centerToPos = (coords - center);
	const ScalarV dotNormal = Dot(normal,centerToPos);
	const ScalarV dotUnit1 = Dot(unit1,centerToPos);
	const Vec3V projVec1 = dotNormal*normal + dotUnit1*unit1;
	const Vec3V projNormal1 = NormalizeFast(projVec1);

	const ScalarV magLine1Sq = MagSquared(line1);
	const ScalarV magLine2Sq = MagSquared(line2);
	const ScalarV minLengthSq = Min(magLine1Sq,magLine2Sq);
	const ScalarV diagLengthSq = MagSquared(corner0 - corner2);

	// Lerp radius based on projected normal v normal Dot.
	const ScalarV dotPNormalprojNormal = Clamp(Dot(normal,projNormal1),ScalarV(V_ZERO),ScalarV(V_ONE));
	const ScalarV usedRadiusSq = Lerp(dotPNormalprojNormal,diagLengthSq * ScalarV(V_QUARTER),minLengthSq * ScalarV(V_QUARTER));
	
	// Get everything ready
	const ScalarV sphereDistSq = MagSquared(projVec1);
	const ScalarV outerRadiusSq = usedRadiusSq * ScalarV(radPFactor);
	
	ScalarV falloff(V_ZERO);
	if(IsLessThanOrEqualAll(sphereDistSq,outerRadiusSq) )
	{
		// Main Fade
		const ScalarV innerRadiusSq = usedRadiusSq;
		const ScalarV mainFade = ScalarV(V_ONE) - Clamp((sphereDistSq - innerRadiusSq)/(outerRadiusSq - innerRadiusSq),ScalarV(V_ZERO),ScalarV(V_ONE));
		
		// Secondary Fade
		const ScalarV dotUnit2 = Dot(unit2,centerToPos);
		const Vec3V projVec2 = dotNormal*normal + dotUnit2*unit2;
		const ScalarV secSphereDistSq = MagSquared(projVec2);
		const ScalarV secRadiusSq = magLine2Sq* ScalarV(V_QUARTER);
		const ScalarV secOuterRadiusSq = secRadiusSq * ScalarV(radPFactor);
		
		const ScalarV secondaryFade = ScalarV(V_ONE) - Clamp((secSphereDistSq - secRadiusSq)/(secOuterRadiusSq - secRadiusSq),ScalarV(V_ZERO),ScalarV(V_ONE));
		
		falloff = mainFade * secondaryFade;
	}
	
	return falloff.Getf();
}


float fwPortalCorners::CalcExpandedAmbientScaleFeather(Vec3V_In coords) const
{
	const ScalarV radPFactor(2.0f * 2.0f);
	
	// Grab corners
	const Vec3V corner0 = GetCornerV(0);
	const Vec3V corner1 = GetCornerV(1);
	const Vec3V corner2 = GetCornerV(2);
	/// const Vec3V corner3 = GetCornerV(3);
	
	// Calc portal's normal and width vector
	const Vec3V axis1 = corner0 - corner1;
	const Vec3V axis2 = corner1 - corner2;
	
	const BoolV unitSelect = IsGreaterThan(Abs(axis1.GetZ()),Abs(axis2.GetZ()));
	const Vec3V line1 = SelectFT(unitSelect,axis1,axis2);
	const Vec3V line2 = SelectFT(unitSelect,axis2,axis1);
	
	const Vec3V cross = Cross(line1, line2);
	const Vec3V normal = NormalizeFast(cross);
	const Vec3V unit1 = NormalizeFast(line1);
	const Vec3V unit2 = NormalizeFast(line2);
	
	// Portal center
	const Vec3V centroidOffset = (corner0 - corner2)*ScalarV(V_HALF);
	const Vec3V center = corner0 - centroidOffset;

	// Project center to Pos onto plans
	const Vec3V centerToPos = (coords - center);
	const ScalarV dotNormal = Dot(normal,centerToPos);
	const ScalarV dotUnit1 = Dot(unit1,centerToPos);
	const Vec3V projVec1 = dotNormal*normal + dotUnit1*unit1;
	const Vec3V projNormal1 = NormalizeFast(projVec1);

	const ScalarV magLine1Sq = MagSquared(line1);
	const ScalarV magLine2Sq = MagSquared(line2);
	const ScalarV minLengthSq = Min(magLine1Sq,magLine2Sq);
	const ScalarV diagLengthSq = MagSquared(corner0 - corner2);

	// Lerp radius based on projected normal v normal Dot.
	const ScalarV dotPNormalprojNormal = Clamp(Dot(normal,projNormal1),ScalarV(V_ZERO),ScalarV(V_ONE));
	const ScalarV usedRadiusSq = Lerp(dotPNormalprojNormal,diagLengthSq * ScalarV(V_QUARTER),minLengthSq * ScalarV(V_QUARTER));
	
	// Get everything ready
	const ScalarV sphereDistSq = MagSquared(projVec1);
	const ScalarV outerRadiusSq = usedRadiusSq * ScalarV(radPFactor);
	
	ScalarV falloff(V_ZERO);
	if(IsLessThanOrEqualAll(sphereDistSq,outerRadiusSq) )
	{
		// Main Fade
		const ScalarV innerRadiusSq = usedRadiusSq;
		const ScalarV mainFade = ScalarV(V_ONE) - Clamp((sphereDistSq - innerRadiusSq)/(outerRadiusSq - innerRadiusSq),ScalarV(V_ZERO),ScalarV(V_ONE));
		
		// Secondary Fade
		const ScalarV dotUnit2 = Dot(unit2,centerToPos);
		const Vec3V projVec2 = dotNormal*normal + dotUnit2*unit2;
		const ScalarV secSphereDistSq = MagSquared(projVec2);
		const ScalarV secRadiusSq = magLine2Sq* ScalarV(V_QUARTER);
		const ScalarV secOuterRadiusSq = secRadiusSq * ScalarV(radPFactor);
		
		const ScalarV secondaryFade = ScalarV(V_ONE) - Clamp((secSphereDistSq - secRadiusSq)/(secOuterRadiusSq - secRadiusSq),ScalarV(V_ZERO),ScalarV(V_ONE));
		
		falloff = mainFade * secondaryFade;
	}
	
	return falloff.Getf();
}

void fwPortalCorners::CreateExtendedLineForPortalTraversalCheck(const Vector3& origStart, const Vector3& end, fwGeom::fwLine& line)
{
	Vector3 delta = end - origStart;
	float deltaMag = delta.Mag();
	float extensionLength = Max(deltaMag*0.01f, 0.1f);
	delta *= (extensionLength / deltaMag);
	Vector3 start = origStart - delta;

	line.Set(start, end);
}

#if __BANK && !__SPU
void fwPortalCorners::DebugDraw2D(const Color32& colour, const Color32& filled, Mat44V_In compositeMtx) const
{
	Vec2V temp[4 + 5];
	const int count = ClipToFrustumWithoutNearPlane(temp, compositeMtx);

	for (int i = 0; i < count; i++) // convert to [0..1]
	{
		temp[i] = Vec2V(V_HALF) + Vec2V(0.5f, -0.5f)*temp[i];
	}

	if (filled.GetAlpha() > 0)
	{
		for (int i = 2; i < count; i++)
		{
			grcDebugDraw::Poly(temp[0], temp[i - 1], temp[i], filled);
		}
	}

	if (colour.GetAlpha() > 0)
	{
		for (int i = 0; i < count; i++)
		{
			grcDebugDraw::Line(temp[i], temp[(i + 1)%count], colour);
		}
	}
}
#endif // __BANK && !__SPU

} // namespace rage
