//
// name:		angle.h
// description:	Class containing useful angle related functions
#ifndef INC_ANGLE_H_
#define INC_ANGLE_H_

#include "math/amath.h"
#include "math/angmath.h"

#define ANGLE_TO_VECTOR_X(ANGLE) -(rage::Sinf((ANGLE)*(PI/180.0f))) 
#define ANGLE_TO_VECTOR_Y(ANGLE) (rage::Cosf((ANGLE)*(PI/180.0f)))

// JA Changed to const float from #define to resolve clash with #define in scalarv.h
const float TWO_PI = (PI * 2.0f);  
const float THREE_PI = (PI * 3.0f);  
#define HALF_PI (PI * 0.5f)
#define QUARTER_PI (PI * 0.25f)
#define EIGHTH_PI (PI * 0.125f)
//
//

#define DEGREES_TO_RADIANS(x) (x*(PI/180.0f))
#define RADIANS_TO_DEGREES(x) (x*(180.0f/PI))

namespace rage {

class fwAngle
{
public:
	static inline float GetDegreeAngleBetweenPoints(const float fDestX, const float fDestY, const float fSrcX, const float fSrcY);
	static inline float GetRadianAngleBetweenPoints(const float fDestX, const float fDestY, const float fSrcX, const float fSrcY);
	static float LimitDegreeAngle(float fAngle);
	static inline float LimitRadianAngle(float fAngle); // Expects value inside the range (-3Pi, 3Pi), asserts if otherwise.
	static inline float LimitRadianAngleFast(float fAngle); // Value _must_ be inside the range (-3Pi, 3Pi).
	static inline float LimitRadianAngleSafe(float fAngle); // Value can be any value, but large values might cause slowness...
	static inline float LimitRadianAngle0to2PiSafe(float fAngle); // Value can be any value, but large values might cause slowness...

	static float LimitRadianAngleForPitch(float fLimitAngle);
	static float LimitRadianAngleForPitchSafe(float fLimitAngle);
	static float Lerp(float from,float to,float maxstep);
	static float LerpTowards(float from,float to,float perc);

	static inline float GetATanOfXY(float X, float Y);

	static int GetNodeHeadingFromVector(float x, float y);
    static int GetNodeHeadingFromRadianAngle(float fAngle);
};


// Name			:	GetDegreeAngleBetweenPoints
// Purpose		:	Returns the angle between 2 points
// Parameters	:	fDestX, fDestY - destination point x and y coords
//					fSrcX, fSrcY - source point x and y coords
// Returns		:	The angle FROM point (fSrcX,fSrcY), TO point (fDestX, fDestY)
inline float fwAngle::GetDegreeAngleBetweenPoints(const float fDestX, const float fDestY, const float fSrcX, const float fSrcY)
{
   return (GetRadianAngleBetweenPoints(fDestX,fDestY,fSrcX,fSrcY) * (180.0f/PI));
   
} // end - fwAngle::GetDegreeAngleBetweenPoints


// Name			:	GetDegreeAngleBetweenPoints
// Purpose		:	Returns the angle between 2 points. 
// Parameters	:	fDestX, fDestY - destination point x and y coords
//					fSrcX, fSrcY - source point x and y coords
// Returns		:	The angle FROM point (fSrcX,fSrcY), TO point (fDestX, fDestY)
inline float fwAngle::GetRadianAngleBetweenPoints(const float fDestX, const float fDestY, const float fSrcX, const float fSrcY)
{
	// Get XY differences
	const float fXdiff = fSrcX - fDestX; 
	const float fNegYdiff = fDestY - fSrcY;
	return rage::Atan2f(fXdiff, fNegYdiff);
} // end - fwAngle::GetRadianAngleBetweenPoints


// Name			:	LimitRadianAngle
// Purpose		:	Takes the supplied angle and returns the equivalent angle between -PI and PI inclusive
//					Expects value inside the range (-3Pi, 3Pi).
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent on [-PI, PI]
inline float fwAngle::LimitRadianAngle(float fLimitAngle)
{
	FastAssert(fLimitAngle > -THREE_PI);
	FastAssert(fLimitAngle < THREE_PI);

	return Selectf(-fLimitAngle + PI, Selectf(fLimitAngle + PI, fLimitAngle, fLimitAngle + TWO_PI), fLimitAngle - TWO_PI);

//	return (fLimitAngle > PI)?(fLimitAngle-TWO_PI):((fLimitAngle < -PI)?(fLimitAngle+TWO_PI):(fLimitAngle));
} // end - fwAngle::LimitRadianAngle


// Name			:	LimitRadianAngleFast
// Purpose		:	Takes the supplied angle and returns the equivalent angle between -PI and PI inclusive
//					VALUE MUST BE INSIDE THE RANGE (-3Pi, 3Pi).
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent on [-PI, PI]
inline float fwAngle::LimitRadianAngleFast(float fLimitAngle)
{
	return Selectf(-fLimitAngle + PI, Selectf(fLimitAngle + PI, fLimitAngle, fLimitAngle + TWO_PI), fLimitAngle - TWO_PI);
//	return (fLimitAngle > PI)?(fLimitAngle-TWO_PI):((fLimitAngle < -PI)?(fLimitAngle+TWO_PI):(fLimitAngle));
} // end - fwAngle::LimitRadianAngleFast


// Name			:	LimitRadianAngle0to2PiSafe
// Purpose		:	Takes the supplied angle and returns the equivalent angle between 0 and 2*PI inclusive
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent on [0, 2*PI]
inline float fwAngle::LimitRadianAngle0to2PiSafe(float fLimitAngle)
{
	FastAssert(fLimitAngle == fLimitAngle);// Make sure it's not NaN.
	fLimitAngle = CanonicalizeAngle(fLimitAngle);
	return Selectf(fLimitAngle, fLimitAngle, fLimitAngle + TWO_PI);
} // end - fwAngle::LimitRadianAngle0to2PiSafe


// Name			:	LimitRadianAngleSafe
// Purpose		:	Takes the supplied angle and returns the equivalent angle between -PI and PI inclusive
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent on [-PI, PI]
inline float fwAngle::LimitRadianAngleSafe(float fLimitAngle)
{
	FastAssert(fLimitAngle == fLimitAngle);// Make sure it's not NaN.

	const float recomputedAngle = CanonicalizeAngle(fLimitAngle);

	// The call to CanonicalizeAngle() above may have changed the angle slightly
	// even if it already was in the desired range. This is somewhat undesirable and a change
	// in behavior compared to an older version of LimitRadianAngleSafe(), which introduced
	// at least one bug. So, to be safe, we now use the original angle unless it's outside
	// of the -PI..PI range. Note that we could have branched and avoided the CanonicalizeAngle()
	// call, not entirely sure which way is faster.
	const float selectLow = Selectf(fLimitAngle + PI, fLimitAngle, recomputedAngle);
	return Selectf(PI - fLimitAngle, selectLow, recomputedAngle);
} // end - fwAngle::LimitRadianAngleSafe


// Name			:	GetATanOfXY
// Purpose		:	Gets ATan of supplied X and Y
// Parameters	:	X, Y
// Returns		:	ATan Radians of X and Y
inline float fwAngle::GetATanOfXY(float X, float Y)
{
	return rage::Atan2f(Y,X) + Selectf(Y, 0, TWO_PI);
}

} // namespace rage
#endif // INC_ANGLE_H_










