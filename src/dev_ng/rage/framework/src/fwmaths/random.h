//
// name:		random.h
// description:	Random number generators
#ifndef INC_RANDOM_H_
#define INC_RANDOM_H_

#define USE_RAGE_RAND (__SPU)

// C++ headers
#include "stdlib.h"

// Rage headers
#include "math/random.h"
#include "math/channel.h"


// Game headers
//#include "debug/Debug.h"


#define RAND_MAX_16			65535
#if RAND_MAX_16 > RAND_MAX
#undef RAND_MAX_16
#define RAND_MAX_16	RAND_MAX
#endif

namespace rage {
//
//
//
class fwRandom
{
public:
#if USE_RAGE_RAND
	static mthRandom ms_Rand;
	static void SetRandomSeed(u32 seed) {ms_Rand.Reset(seed);}
	static int GetRandomNumber16(void) {return ms_Rand.GetInt() & RAND_MAX_16;}
	static int GetRandomNumber(void) {return ms_Rand.GetInt() & RAND_MAX;}
#else
	static void SetRandomSeed(u32 seed) {srand(seed); }
	static int GetRandomNumber16(void) {return (rand() & 0xffff);}
	static int GetRandomNumber(void) {return (rand());}
#endif
	static float GetRandomNumberInRange(float min, float max);
	static int GetRandomNumberInRange(int min, int max);
	static bool GetRandomTrueFalse();
};

#if USE_RAGE_RAND
inline float fwRandom::GetRandomNumberInRange(float min, float max) 
{
	float num = ms_Rand.GetFloat();
	return (num * (max - min)) + min;
}

inline int fwRandom::GetRandomNumberInRange(int min, int max) 
{
	return int((u64(ms_Rand.GetInt()) * u64(max - min)) >> 31) + min;
}

inline bool fwRandom::GetRandomTrueFalse() 
{
	return ms_Rand.GetBool();
}
#else
inline float fwRandom::GetRandomNumberInRange(float min, float max) 
{
	float num = GetRandomNumber() / (float)(RAND_MAX);
	return (num * (max - min)) + min;
}

inline int fwRandom::GetRandomNumberInRange(int min, int max) 
{
	// man page for rand() says you shouldn't get random number like rand()%range, it
	// should be done as below
	float num = GetRandomNumber16() / (float)(RAND_MAX_16+1);
	return (int)(num * (max - min)) + min;
}

inline bool fwRandom::GetRandomTrueFalse() 
{
	if(GetRandomNumber() < RAND_MAX/2)
		return true;
	return false;
}
#endif


class SemiShuffledSequence
{
public:
	/////////////////////////////////////////////////////////////////////////////////
	// FUNCTION :	SemiShuffledSequence
	// PURPOSE :	If we want to go through a sequence of numbers in random order we
	//				 can use this function to set stuff up.
	// PARAMETERS :	NumElements - the number of elements to "shuffle".
	// RETURNS :	Nothing
	/////////////////////////////////////////////////////////////////////////////////
	SemiShuffledSequence(int NumElements)
	{
		SetNumElements(NumElements);
	}
	
	/////////////////////////////////////////////////////////////////////////////////
	// FUNCTION :	Reset
	// PURPOSE :	Reset the random order of the elements.
	// PARAMETERS :	None
	// RETURNS :	Nothing
	/////////////////////////////////////////////////////////////////////////////////
	void Reset()
	{
		m_sequenceRandomOffset = fwRandom::GetRandomNumber() % m_sequenceNumElements;

		if (fwRandom::GetRandomNumber() & 16)
		{
			m_bSequenceOtherWay = true;
		}
		else
		{
			m_bSequenceOtherWay = false;
		}
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////
	// FUNCTION :	SetNumElements
	// PURPOSE :	Sets the number of elements.
	// PARAMETERS :	None
	// RETURNS :	Nothing
	/////////////////////////////////////////////////////////////////////////////////
	void SetNumElements(const int NumElements)
	{
		mthAssertf((NumElements > 0), "NumElements must be greater than zero.");

		m_sequenceNumElements = NumElements;

		Reset();
	}


	/////////////////////////////////////////////////////////////////////////////////
	// FUNCTION : GetElement
	// PURPOSE :  If we want to go through a sequence of numbers in random order we
	//			  can use this function to find a specific value.
	/////////////////////////////////////////////////////////////////////////////////
	int GetElement(int Element)
	{
		mthAssertf((Element < m_sequenceNumElements), "Element must be less than NumElements.");
		mthAssertf((Element >= 0), "Element must be greater or equal to zero.");

		if (m_bSequenceOtherWay)
		{
			return( (m_sequenceRandomOffset + Element) % m_sequenceNumElements);
		}
		return( (m_sequenceRandomOffset - Element + m_sequenceNumElements) % m_sequenceNumElements);
	}
	
	int GetNumElements() const
	{
		return m_sequenceNumElements;
	}

protected:
	int m_sequenceNumElements;
	int m_sequenceRandomOffset;
	bool  m_bSequenceOtherWay;

};


// the scripts need their own unique thread-safe random number generator
class CRandomScripts
{
public:
	static mthRandom ms_Rand;
	static void SetRandomSeed(u32 seed) {ms_Rand.Reset(seed);}
	static int GetRandomNumber16(void) {return ms_Rand.GetInt() & RAND_MAX_16;}
	static int GetRandomNumber(void) {return ms_Rand.GetInt() & RAND_MAX;}
	static float GetRandomNumberInRange(float min, float max);
	static int GetRandomNumberInRange(int min, int max);
	static bool GetRandomTrueFalse();
};

inline float CRandomScripts::GetRandomNumberInRange(float min, float max) 
{
	float num = ms_Rand.GetFloat();
	return (num * (max - min)) + min;
}

inline int CRandomScripts::GetRandomNumberInRange(int min, int max) 
{
	return int((u64(ms_Rand.GetInt()) * u64(max - min)) >> 31) + min;
}

inline bool CRandomScripts::GetRandomTrueFalse() 
{
	return ms_Rand.GetBool();
}

// adding an independent RNG for the casino games based on a kosher MWC implementation
class CRandomMWCScripts
{
public:
	static mthRandomMWC ms_RandMWC;
	static void SetRandomSeed(u32 seed) {ms_RandMWC.Reset(seed);}
	static int GetRandomNumberInRange(int min, int max);
	static int GetInt(void);
	static u32 GetRawInt(void);
};

inline u32 CRandomMWCScripts::GetRawInt() 
{
	return ms_RandMWC.GetRawInt();
}

inline int CRandomMWCScripts::GetInt() 
{
	return ms_RandMWC.GetInt();
}


inline int CRandomMWCScripts::GetRandomNumberInRange(int min, int max) 
{
	return int((u64((ms_RandMWC.GetInt())) * u64(max - min)) >> 31) + min;
}

} // namespace rage 
#endif
