//
// name:		angle.cpp
// description:	common angle related methods

// Rage headers
#include "math\amath.h"
#include "math\channel.h"
#include "angle.h"

namespace rage {

// Name			:	LimitDegreeAngle
// Purpose		:	Takes the supplied angle and returns the equivalent angle between -180 and 180
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent between -180 and 180
float fwAngle::LimitDegreeAngle(float fLimitAngle)
{
#if __WIN32 && !__FINAL
	if (_isnan(fLimitAngle)) {
		mthWarningf("fwAngle::LimitDegreeAngle - NaN!");
		return 0.0f;
	}
#endif
	fLimitAngle = Max(-1500.0f, fLimitAngle);
	fLimitAngle = Min(1500.0f, fLimitAngle);

	while (fLimitAngle >= 180.0f)
		fLimitAngle -= 360.0f;

	while (fLimitAngle < -180.0f)
		fLimitAngle += 360.0f;

   return(fLimitAngle);
} // end - fwAngle::LimitDegreeAngle


// Name			:	LimitRadianAngleForPitch
// Purpose		:	Takes the supplied pitch angle and returns the equivalent angle between -PI/2 and PI/2 inclusive
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent on [-PI/2, PI/2]
float fwAngle::LimitRadianAngleForPitch(float fLimitAngle)
{
	//First limit to [-PI , PI].
	fLimitAngle = LimitRadianAngle(fLimitAngle);

	if(fLimitAngle > HALF_PI)
	{
		fLimitAngle = PI - fLimitAngle;
	}
	else if(fLimitAngle < -HALF_PI)
	{
		fLimitAngle = -PI - fLimitAngle;
	}

	return(fLimitAngle);
} // end - fwAngle::LimitRadianAngleForPitch

// Name			:	LimitRadianAngleForPitchSafe
// Purpose		:	Takes the supplied pitch angle and returns the equivalent angle between -PI/2 and PI/2 inclusive
// Parameters	:	fLimitAngle - the angle to process
// Returns		:	The supplied angles equivalent on [-PI/2, PI/2]
float fwAngle::LimitRadianAngleForPitchSafe(float fLimitAngle)
{
	//First limit to [-PI , PI].
	fLimitAngle = LimitRadianAngleSafe(fLimitAngle);

	if(fLimitAngle > HALF_PI)
	{
		fLimitAngle = PI - fLimitAngle;
	}
	else if(fLimitAngle < -HALF_PI)
	{
		fLimitAngle = -PI - fLimitAngle;
	}

	return(fLimitAngle);
} // end - fwAngle::LimitRadianAngleForPitch

// Name			:	Lerp
// Returns		:	Lerp Radians
float fwAngle::Lerp(float from,float to,float maxStep)
{
	float	ret;

	mthAssert(maxStep>=0.0f);// doesnt make much sense to lerp negative


	float	DeltaAngle = to - from;

	while (DeltaAngle > PI) DeltaAngle -= 2.0f*PI;
	while (DeltaAngle < -PI) DeltaAngle += 2.0f*PI;

	if(fabsf(DeltaAngle)< maxStep)
	{
		ret=LimitRadianAngle(to); // we made it within this step
	}
	else
	{
		//limit delta to maxStep

		if(DeltaAngle<0.0f)
			from-=maxStep;
		else
			from+=maxStep;

		ret=LimitRadianAngle(from);
	}

	return ret;

}

// Name			:	LerpTowards
// Returns		:	Lerp Radians
float fwAngle::LerpTowards(float from,float to,float perc)
{
	float	DeltaAngle = to - from;

	while (DeltaAngle > PI) DeltaAngle -= 2.0f*PI;
	while (DeltaAngle < -PI) DeltaAngle += 2.0f*PI;


	float	ActualAngle = from + DeltaAngle * perc;

	while (ActualAngle > PI) ActualAngle -= 2.0*PI;
	while (ActualAngle < -PI) ActualAngle += 2.0*PI;

	return ActualAngle;

}

//////////////////////////////////////////////////////////////////////
// Function returns the heading direction for path node selection
// (0 to 7: North clockwise) of the given global 2d direction vector
//////////////////////////////////////////////////////////////////////
int fwAngle::GetNodeHeadingFromVector(float x, float y)
{
	float fHeading = GetRadianAngleBetweenPoints(x,y,0.0f,0.0f);
	
	return GetNodeHeadingFromRadianAngle(fHeading);
}

//////////////////////////////////////////////////////////////////////
// Function returns the heading direction for path node selection
// (0 to 7: North clockwise) of the given angle
//////////////////////////////////////////////////////////////////////
int fwAngle::GetNodeHeadingFromRadianAngle(float fAngle)
{
    // first get heading into interval 0->2Pi
	if(fAngle < 0.0f)
		fAngle += TWO_PI;
	// then reverse direction
	fAngle = TWO_PI - fAngle;
	// subtract half an interval of the node heading range
	fAngle = fAngle + (TWO_PI/(8*2));
	if(fAngle >= TWO_PI)
		fAngle -= TWO_PI;

    return static_cast<int>(rage::Floorf(8.0f*(fAngle/TWO_PI)));
}

} // namespace rage
