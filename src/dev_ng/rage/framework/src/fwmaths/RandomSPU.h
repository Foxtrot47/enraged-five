//
// CRandomSPU - simple library to add fwRandom functionality for SPU;
//
//	20/03/2007	-	Andrzej:	- initial;
//
//
#ifndef __CRANDOM_SPU_H__
#define __CRANDOM_SPU_H__
#if __SPU

#include "spu_mfcio.h"	// spu_read_decrementer()...

namespace rage {

//
//
//
//
class fwRandom
{
public:
	static void Initialise()
	{
		init_MT(spu_read_decrementer());
		//spu_write_decrementer(0x70000000);
	}

	static void SetRandomSeed(u32 seed)
	{
		init_MT(seed);
	}

	static int	GetRandomNumberInRange(int a,int b)
	{
		float	r1=rand_real1_MT();
		return  (int)((r1*(float)(b-a))+(int)a);
	}
	static float GetRandomNumberInRange(float a,float b)
	{
		float	r1=rand_real1_MT();
		return  ((r1*(b-a))+a);
	}
	
	static vec_float4 GetRandomNumberInRange_f4(vec_float4 a, vec_float4 b)
	{
		vec_float4 r0	= rand_real1_MT_f4();
		vec_float4 bma	= spu_sub(b, a);
		vec_float4 r1	= spu_madd(r0, bma, a);	// r1 = r0*(b-a) + a
		return r1;
	}
};

} // namespace rage

#endif //__SPU...
#endif //__CRANDOM_SPU_H__...
