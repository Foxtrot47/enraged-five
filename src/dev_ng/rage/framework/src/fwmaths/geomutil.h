// ======================
// fwmaths/geomutil.h
// (c) 2011 RockstarNorth
// ======================

#ifndef _FWMATHS_GEOMUTIL_H_
#define _FWMATHS_GEOMUTIL_H_

/* ==================================================================
// This file is a temporary place for geometry utility code which is
// not vectorised (or not well vectorised) and should probably be
// replaced with function from rage geomVectors, geomBoxes etc.
// =============================================================== */

#include "vector/vector3.h"
#include "vector/geometry.h" // for geomSegments::CollideRayTriangle

namespace rage { class spdSphere; }
namespace rage { class spdAABB; }

namespace rage {
namespace fwGeom {

class fwLine
{
public:
	fwLine() {}
	fwLine(const Vector3& start, const Vector3& end) : m_start(start), m_end(end) {}
	void Set(const Vector3& start, const Vector3& end) { m_start = start; m_end = end; }
	float Distance( void ) const { return m_start.Dist(m_end); }

	// used by PortalCorners only
	bool IsCollidingWithTri(const Vector3& p1, const Vector3& p2, const Vector3& p3, bool backfaceCulling = false) const
	{
		float t = 0.0f;
		Vector3 ray = (m_end-m_start);
		bool intersect = geomSegments::CollideRayTriangle (p1, p2, p3, m_start, ray, &t, backfaceCulling);

		// check that the line intersects and the intersection lies between the start and end points
		return((intersect && (t > 0.0f) && (t<1.0f)));
	}

	// used by PortalCorners, and a handful of other systems ..
	// also consider 2D version, e.g. CInAirHelper::GetClosestPointOnLineXY
	//-------------------------------------------------------------------------
	// Calculates the closest point on the line to the given point
	// also returns the proportion of that point along the line
	//-------------------------------------------------------------------------
	float FindClosestPointOnLine( const Vector3& vPos, Vector3& vClosestPoint ) const
	{
		Vector3 ab = m_end-m_start;

		float lenABSq = ab.x*ab.x + ab.y*ab.y + ab.z*ab.z;

		float d = ( (vPos.x - m_start.x) * (m_end.x - m_start.x) +
					(vPos.y - m_start.y) * (m_end.y - m_start.y) +
					(vPos.z - m_start.z) * (m_end.z - m_start.z) ) / lenABSq;

		d = Clamp(d, 0.0f, 1.0f);

		vClosestPoint.x = m_start.x + (m_end.x - m_start.x) * d;
		vClosestPoint.y = m_start.y + (m_end.y - m_start.y) * d;
		vClosestPoint.z = m_start.z + (m_end.z - m_start.z) * d;
		return d;
	}

	__forceinline ScalarV_Out FindClosestPointOnLineV(Vec3V_In vPos, Vec3V_InOut vClosestPoint) const
	{
		Vec3V start = RCC_VEC3V(m_start);
		Vec3V end = RCC_VEC3V(m_end);

		Vec3V ab = end - start;

		ScalarV invLenABSq = InvMagSquared(ab);

		ScalarV distAlongLineSq = Dot(vPos - start, end - start);
		ScalarV tAlongLine = Saturate(distAlongLineSq * invLenABSq);
		vClosestPoint = AddScaled(start, ab, tAlongLine);
		return tAlongLine;
	}

	Vector3 m_start;
	Vector3 m_end;
};

float DistToLineSqr(const Vector3 &vecStart, const Vector3 &vecEnd, const Vector3 &testPoint);
__forceinline float DistToLine(const Vector3 &vecStart, const Vector3 &vecEnd, const Vector3 &testPoint) { return rage::Sqrtf(DistToLineSqr(vecStart, vecEnd, testPoint)); }
float DistToMathematicalLine(const Vector3 &vecStart, const Vector3 &vecEnd, const Vector3 &testPoint);
float DistAlongLine2D(float LineStartX, float LineStartY, float LineDirX, float LineDirY, float PointX, float PointY);
bool Test2DLineAgainst2DLine(float Start1X, float Start1Y, float Delta1X, float Delta1Y, float Start2X, float Start2Y, float Delta2X, float Delta2Y);

bool IntersectSphereRay(const spdSphere& sphere, const Vector3& v, const Vector3& w, Vector3& v1, Vector3& v2);
bool IntersectSphereEdge(const spdSphere& sphere, const Vector3& start, const Vector3& end, Vector3& v1, Vector3& v2);

bool IntersectBoxRay(const spdAABB& box, const Vector3 &origin, const Vector3 &direction);

__forceinline ScalarV_Out DistToMathematicalLineV(Vec3V_In vecStart, Vec3V_In vecEnd, Vec3V_In testPoint)
{
	Vec3V direction = vecEnd - vecStart;
	Vec3V vec = testPoint - vecStart;

	ScalarV invLineLengthSqr = InvMagSquared(direction);

	ScalarV distAlongLineTimesLength = Dot(vec, direction);
	ScalarV distAlongLineSqrTimesLineLengthSqr = square(distAlongLineTimesLength);
	ScalarV distAlongLineSqr = distAlongLineSqrTimesLineLengthSqr * invLineLengthSqr;
	ScalarV distToLineSqr = MagSquared(vec) - distAlongLineSqr;
	return SqrtSafe(distToLineSqr);
}

} // namespace fwGeom
} // namespace rage

#endif // _FWMATHS_GEOMUTIL_H_
