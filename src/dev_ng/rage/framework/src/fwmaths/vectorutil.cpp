// ======================
// fwmaths/vectorutil.cpp
// (c) 2011 RockstarNorth
// ======================

#include "grcore/debugdraw.h"
#include "grcore/viewport.h"
#include "spatialdata/transposedplaneset.h"
#include "system/nelem.h"
#include "vector/color32.h"

#include "fwmaths/vectorutil.h"

namespace rage {

// ================================================================================================

void CalcLinearTable(Vec3V* table, Vec3V_In v0, Vec3V_In v1, ScalarV_In step, int n)
{
	const Vec3V dv = (v1 - v0)*step;
	Vec3V v = v0;

	for (int i = 0; i < n; i++)
	{
		table[i] = v;
		v += dv;
	}
}

// uses Goertzel's algorithm (2nd order resonant filter)
// for full cycle use ScalarV(2.0f*PI/(float)n) for 'b'
// http://shkodra.perkthimeonline.com/e107_files/public/1217192542_1833_FT83635_fast-math-functions_p1.pdf
void CalcSinCosTable(Vec3V* table, ScalarV_In b, int n)
{
	const Vec4V p = Vec4V(ScalarV(V_ZERO), ScalarV(V_PI_OVER_TWO), ScalarV(V_ZERO), ScalarV(V_PI_OVER_TWO));
	const Vec4V q = Vec4V(ScalarV(V_TWO ), ScalarV(V_TWO        ), ScalarV(V_ONE ), ScalarV(V_ONE        ));

	Vec4V v = Sin(SubtractScaled(p, q, b)); // precalculate two {sin,cos} pairs in parallel
	const ScalarV cosb2 = v.GetW()*ScalarV(V_TWO);

	// v = {s2,c2,s1,c1}

	for (int i = 0; i < n; i++)
	{
		const Vec2V v2 = v.GetZW()*cosb2 - v.GetXY();

		table[i] = Vec3V(v2, ScalarV(V_ONE)); // {sin,cos,1}

		// this looks awkward but it should be a single instruction ..
		v = GetFromTwo<Vec::Z1,Vec::W1,Vec::X2,Vec::Y2>(v, Vec4V(v2));

		//s[i] = cb*s1 - s2;
		//c[i] = cb*c1 - c2;
		//s2 = s1;
		//c2 = c1;
		//s1 = s[i];
		//c1 = c[i];
	}
}

#if RSG_DEV

void CalcSinCosTable_REFERENCE(Vec3V* table, ScalarV_In b, int n)
{
	for (int i = 0; i < n; i++)
	{
		const float theta = b.Getf()*(float)i;

		table[i] = Vec3V(sinf(theta), cosf(theta), 1.0f);
	}
}

#endif // RSG_DEV

void CalcSinCosTable(Vec3V* table, int n)
{
	CalcSinCosTable(table, ScalarV(2.0f*PI/(float)n), n);
}

// ================================================================================================
// === clip

int PolyClip(Vec3V* dst, int dstCountMax, const Vec3V* src, int srcCount, Vec4V_In plane)
{
	int dstCount = 0;

	Vec3V   p0 = src[srcCount - 1];
	ScalarV d0 = PlaneDistanceTo(plane, p0);
	u32     g0 = IsGreaterThanOrEqualAll(d0, ScalarV(V_ZERO));

	for (int i = 0; i < srcCount; i++)
	{
		const Vec3V   p1 = src[i];
		const ScalarV d1 = PlaneDistanceTo(plane, p1);
		const u32     g1 = IsGreaterThanOrEqualAll(d1, ScalarV(V_ZERO));

		if (dstCount < dstCountMax && g1 != g0) { dst[dstCount++] = (p0*d1 - p1*d0)/(d1 - d0); }
		if (dstCount < dstCountMax && g1      ) { dst[dstCount++] = p1; }

		p0 = p1;
		d0 = d1;
		g0 = g1;
	}

	return dstCount;
}

bool PolyClipEdge(Vec3V edge[2], const Vec3V* src, int srcCount, Vec4V_In plane)
{
	int dstCount = 0;

	Vec3V   p0 = src[srcCount - 1];
	ScalarV d0 = PlaneDistanceTo(plane, p0);
	u32     g0 = IsGreaterThanOrEqualAll(d0, ScalarV(V_ZERO));

	for (int i = 0; i < srcCount; i++)
	{
		const Vec3V   p1 = src[i];
		const ScalarV d1 = PlaneDistanceTo(plane, p1);
		const u32     g1 = IsGreaterThanOrEqualAll(d1, ScalarV(V_ZERO));

		if (g1 != g0) { edge[dstCount++] = (p0*d1 - p1*d0)/(d1 - d0); }

		if (dstCount >= 2)
		{
			break; // shouldn't happen if src is convex
		}

		p0 = p1;
		d0 = d1;
		g0 = g1;
	}

	return dstCount == 2;
}

int PolyClipEx(Vec4V* dst, const Vec4V* src, int srcCount, Vec4V_In plane, ScalarV_In planeId)
{
	int dstCount = 0;

	Vec4V   p0 = src[srcCount - 1];
	ScalarV d0 = PlaneDistanceTo(plane, p0.GetXYZ());
	u32     g0 = IsGreaterThanOrEqualAll(d0, ScalarV(V_ZERO));

	for (int i = 0; i < srcCount; i++)
	{
		const Vec4V   p1 = src[i];
		const ScalarV d1 = PlaneDistanceTo(plane, p1.GetXYZ());
		const u32     g1 = IsGreaterThanOrEqualAll(d1, ScalarV(V_ZERO));

		if (g1 != g0)
		{
			const Vec3V p2 = (p0.GetXYZ()*d1 - p1.GetXYZ()*d0)/(d1 - d0);

			if (g0) { dst[dstCount++] = Vec4V(p2, p1.GetW()); } // entering negative halfspace
			else    { dst[dstCount++] = Vec4V(p2, planeId);   } // exiting negative halfspace
		}

		if (g1)
		{
			dst[dstCount++] = p1;
		}

		p0 = p1;
		d0 = d1;
		g0 = g1;
	}

	return dstCount;
}

int PolyClipToFrustum(Vec3V* dst, const Vec3V* src, int srcCount, Mat44V_In viewProj)
{
	const int kMaxPoints = 32;
	Vec3V buf[kMaxPoints];

	Assert(srcCount + 6 <= kMaxPoints);

	const Vec4V tmp0 = MergeXY(viewProj.a(), viewProj.c());
	const Vec4V tmp1 = MergeXY(viewProj.b(), viewProj.d());
	const Vec4V tmp2 = MergeZW(viewProj.a(), viewProj.c());
	const Vec4V tmp3 = MergeZW(viewProj.b(), viewProj.d());
	const Vec4V row0 = MergeXY(tmp0, tmp1);
	const Vec4V row1 = MergeZW(tmp0, tmp1);
	const Vec4V row2 = MergeXY(tmp2, tmp3);
	const Vec4V row3 = MergeZW(tmp2, tmp3);

	srcCount = PolyClip(buf, kMaxPoints, src, srcCount, row3 + row0); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(dst, kMaxPoints, buf, srcCount, row3 - row0); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(buf, kMaxPoints, dst, srcCount, row3 + row1); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(dst, kMaxPoints, buf, srcCount, row3 - row1); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(buf, kMaxPoints, dst, srcCount,        row2); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(dst, kMaxPoints, buf, srcCount, row3 - row2);

	return srcCount;
}

int PolyClipToFrustumWithoutNearPlane(Vec3V* dst, const Vec3V* src, int srcCount, Mat44V_In viewProj)
{
	const int kMaxPoints = 32;
	Vec3V buf[kMaxPoints];
	Vec3V bf2[kMaxPoints];

	Assert(srcCount + 5 <= kMaxPoints);

	const Vec4V tmp0 = MergeXY(viewProj.a(), viewProj.c());
	const Vec4V tmp1 = MergeXY(viewProj.b(), viewProj.d());
	const Vec4V tmp2 = MergeZW(viewProj.a(), viewProj.c());
	const Vec4V tmp3 = MergeZW(viewProj.b(), viewProj.d());
	const Vec4V row0 = MergeXY(tmp0, tmp1);
	const Vec4V row1 = MergeZW(tmp0, tmp1);
	const Vec4V row2 = MergeXY(tmp2, tmp3);
	const Vec4V row3 = MergeZW(tmp2, tmp3);

	srcCount = PolyClip(buf, kMaxPoints, src, srcCount, row3 + row0); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(dst, kMaxPoints, buf, srcCount, row3 - row0); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(buf, kMaxPoints, dst, srcCount, row3 + row1); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(bf2, kMaxPoints, buf, srcCount, row3 - row1); if (srcCount < 3) { return 0; }
	srcCount = PolyClip(dst, kMaxPoints, bf2, srcCount, row3 - row2);

	return srcCount;
}

// ================================================================================================

Vec4V_Out fwBestFitPlane::GetPlane() const
{
	const ScalarV x1 = m_sum_x1_y1_z1_1.GetX();
	const ScalarV y1 = m_sum_x1_y1_z1_1.GetY();
	const ScalarV z1 = m_sum_x1_y1_z1_1.GetZ();
	const ScalarV _1 = m_sum_x1_y1_z1_1.GetW();

	const ScalarV xx = m_sum_xx_yy_zz.GetX();
	const ScalarV yy = m_sum_xx_yy_zz.GetY();
	const ScalarV zz = m_sum_xx_yy_zz.GetZ();

	const ScalarV xy = m_sum_xy_yz_zx.GetX();
	const ScalarV yz = m_sum_xy_yz_zx.GetY();
	const ScalarV zx = m_sum_xy_yz_zx.GetZ();

	const Mat33V mx(Vec3V(xx, xy, x1), Vec3V(xy, yy, y1), Vec3V(x1, y1, _1));
	const Mat33V my(Vec3V(yy, yz, y1), Vec3V(yz, zz, z1), Vec3V(y1, z1, _1));
	const Mat33V mz(Vec3V(zz, zx, z1), Vec3V(zx, xx, x1), Vec3V(z1, x1, _1));

	const ScalarV ax = Abs(Determinant(mx));
	const ScalarV ay = Abs(Determinant(my));
	const ScalarV az = Abs(Determinant(mz));

	const ScalarV a = Max(ax, ay, az);

	Vec4V plane;

	if (IsEqualAll(a, ax))
	{
		const Vec3V ABC = UnTransformFull(mx, Vec3V(zx, yz, z1)); // A*x + B*y - z + C == 0
		plane = GetFromTwo<Vec::X1,Vec::Y1,Vec::X2,Vec::Z1>(ABC, Vec3V(V_NEGONE)); // {A,B,-1,C}
	}
	else if (IsEqualAll(a, ay))
	{
		const Vec3V ABC = UnTransformFull(my, Vec3V(xy, zx, x1)); // A*y + B*z - x + C == 0
		plane = GetFromTwo<Vec::W2,Vec::X1,Vec::Y1,Vec::Z1>(Vec4V(ABC), Vec4V(V_NEGONE)); // {-1,A,B,C} (one instr on 360/ps3)
	}
	else // if (IsEqualAll(a, az))
	{
		const Vec3V ABC = UnTransformFull(mz, Vec3V(yz, xy, y1)); // A*z + B*x - y + C == 0
		plane = GetFromTwo<Vec::Y1,Vec::X2,Vec::X1,Vec::Z1>(ABC, Vec3V(V_NEGONE)); // {B,-1,A,C}
	}

	return plane*InvMag(plane.GetXYZ());	
}

// ================================================================================================

void CalculateFrustumBounds(Vec3V_InOut boundsMin, Vec3V_InOut boundsMax, const grcViewport& viewport, bool bUseAsymmetrical)
{
	if (viewport.IsPerspective())
	{
		if (bUseAsymmetrical)
		{
			CalculateFrustumBoundsAsymmetrical(
				boundsMin,
				boundsMax,
				viewport.GetCameraMtx(),
				viewport.GetTanFOVRect(),
				ScalarV(viewport.GetNearClip()),
				ScalarV(viewport.GetFarClip())
			);
		}
		else
		{
			CalculateFrustumBounds(
				boundsMin,
				boundsMax,
				viewport.GetCameraMtx(),
				ScalarV(viewport.GetTanHFOV()),
				ScalarV(viewport.GetTanVFOV()),
				ScalarV(viewport.GetNearClip()),
				ScalarV(viewport.GetFarClip())
			);
		}
	}
	else // orthographic
	{
		// TODO -- support orthographic viewport for tiled screen capture with shadows
		boundsMin = Vec3V(V_ZERO);
		boundsMax = Vec3V(V_ZERO);
	}
}

// ================================================================================================

spdTransposedPlaneSet4 GenerateFrustum4(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV)
{
	const ScalarV zero(V_ZERO);
	const ScalarV p1(V_ONE);
	const ScalarV n1(V_NEGONE);

	const Vec3V camPos = +cameraMtx.GetCol3();

	const ScalarV qh = InvSqrt(AddScaled(p1, tanHFOV, tanHFOV));
	const ScalarV qv = InvSqrt(AddScaled(p1, tanVFOV, tanVFOV));

	const Vec4V plane0 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(n1, zero, tanHFOV)*qh)); // left
	const Vec4V plane1 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(p1, zero, tanHFOV)*qh)); // right
	const Vec4V plane2 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(zero, p1, tanVFOV)*qv)); // top
	const Vec4V plane3 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(zero, n1, tanVFOV)*qv)); // bottom

	spdTransposedPlaneSet4 tps;
	tps.SetPlanes(plane0, plane1, plane2, plane3);
	return tps;
}

spdTransposedPlaneSet8 GenerateFrustum8(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In nearClip, ScalarV_In farClip)
{
	const ScalarV zero(V_ZERO);
	const ScalarV p1(V_ONE);
	const ScalarV n1(V_NEGONE);

	const Vec3V camPos = +cameraMtx.GetCol3();
	const Vec3V camDir = -cameraMtx.GetCol2();

	const ScalarV qh = InvSqrt(AddScaled(p1, tanHFOV, tanHFOV));
	const ScalarV qv = InvSqrt(AddScaled(p1, tanVFOV, tanVFOV));

	const Vec4V plane0 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(n1, zero, tanHFOV)*qh)); // left
	const Vec4V plane1 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(p1, zero, tanHFOV)*qh)); // right
	const Vec4V plane2 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(zero, p1, tanVFOV)*qv)); // top
	const Vec4V plane3 = BuildPlane(camPos, Transform3x3(cameraMtx, Vec3V(zero, n1, tanVFOV)*qv)); // bottom
	const Vec4V plane4 = BuildPlane(AddScaled(camPos, camDir, nearClip), -camDir);
	const Vec4V plane5 = BuildPlane(AddScaled(camPos, camDir, farClip), camDir);

	spdTransposedPlaneSet8 tps;
	tps.SetPlanes(plane0, plane1, plane2, plane3, plane4, plane5, Vec4V(V_ZERO), Vec4V(V_ZERO));
	return tps;
}

spdTransposedPlaneSet4 GenerateConeFrustum4(Vec3V_In pos, Vec3V_In dir, Vec3V_In tangent, ScalarV_In tanHalfAngle)
{
	Assert(Abs<float>(MagSquared(dir).Getf() - 1.0f) <= 0.01f);
	Assert(Abs<float>(MagSquared(tangent).Getf() - 1.0f) <= 0.01f);
	Assert(Abs<float>(Dot(dir, tangent).Getf()) <= 0.01f);

	// note basisX,Y are scaled by cos(coneAngle/2), and basisZ is scaled by sin(coneAngle/2)
	const Vec2V sinCos = Vec2V(tanHalfAngle, ScalarV(V_ONE))*InvSqrt(AddScaled(ScalarV(V_ONE), tanHalfAngle, tanHalfAngle));//SinCos(coneAngle*ScalarV(V_HALF));
	const Vec3V basisX = tangent*sinCos.GetY();
	const Vec3V basisY = Cross(tangent, dir);
	const Vec3V basisZ = -dir*sinCos.GetX();

	const Vec4V planes[] =
	{
		BuildPlane(pos, basisZ + basisX),
		BuildPlane(pos, basisZ + basisY),
		BuildPlane(pos, basisZ - basisX),
		BuildPlane(pos, basisZ - basisY)
	};

	spdTransposedPlaneSet4 tps;
	tps.SetPlanes(planes);
	return tps;
}

spdTransposedPlaneSet8 GenerateConeFrustum8(Vec3V_In pos, Vec3V_In dir, Vec3V_In tangent, ScalarV_In tanHalfAngle)
{
	Assert(Abs<float>(MagSquared(dir).Getf() - 1.0f) <= 0.01f);
	Assert(Abs<float>(MagSquared(tangent).Getf() - 1.0f) <= 0.01f);
	Assert(Abs<float>(Dot(dir, tangent).Getf()) <= 0.01f);

	// note basisX,Y are scaled by cos(coneAngle/2), and basisZ is scaled by sin(coneAngle/2)
	const Vec2V sinCos = Vec2V(tanHalfAngle, ScalarV(V_ONE))*InvSqrt(AddScaled(ScalarV(V_ONE), tanHalfAngle, tanHalfAngle));//SinCos(coneAngle*ScalarV(V_HALF));
	const Vec3V basisX = tangent*sinCos.GetY();
	const Vec3V basisY = Cross(tangent, dir);
	const Vec3V basisZ = -dir*sinCos.GetX();
	const ScalarV sqrt0_5 = ScalarVConstant<FLOAT_TO_INT(0.70710678118655f)>(); // sqrtf(0.5f)

	const Vec4V planes[] =
	{
		BuildPlane(pos, basisZ + basisX),
		BuildPlane(pos, basisZ + basisY),
		BuildPlane(pos, basisZ - basisX),
		BuildPlane(pos, basisZ - basisY),
		BuildPlane(pos, basisZ + (basisX + basisY)*sqrt0_5),
		BuildPlane(pos, basisZ + (basisX - basisY)*sqrt0_5),
		BuildPlane(pos, basisZ - (basisX + basisY)*sqrt0_5),
		BuildPlane(pos, basisZ - (basisX - basisY)*sqrt0_5)
	};

	spdTransposedPlaneSet8 tps;
	tps.SetPlanes(planes);
	return tps;
}

Mat44V_Out CalculatePerspectiveProjectionMatrix(ScalarV_In nearClip, ScalarV_In farClip)
{
	const ScalarV zNorm(farClip/(nearClip - farClip));
	const ScalarV zNearNorm = nearClip*zNorm;

	Mat44V projection;
	projection.SetCol0(Vec4V(V_X_AXIS_WZERO));
	projection.SetCol1(Vec4V(V_Y_AXIS_WZERO));
	projection.SetCol2(Vec4V(Vec2V(V_ZERO), zNorm, ScalarV(V_NEGONE)));
	projection.SetCol3(Vec4V(Vec2V(V_ZERO), zNearNorm, ScalarV(V_ZERO)));
	return projection;
}

// ================================================================================================

#if !__SPU

spdTransposedPlaneSet4 GenerateSweptBox2D(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In farClip, Vec3V_In sweep)
{
	const Vec3V basisX = Normalize(Vec3V(sweep.GetXY(), ScalarV(V_ZERO)));
	const Vec3V basisY = CrossZAxis(basisX);

	// TODO -- this is not very efficient code .. we could use CalculateFrustumBounds but we'd need to rotate the matrix ..
	const Vec3V points[4] =
	{
		Transform(cameraMtx, Vec3V(-tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*farClip),
		Transform(cameraMtx, Vec3V(+tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*farClip),
		Transform(cameraMtx, Vec3V(-tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*farClip),
		Transform(cameraMtx, Vec3V(+tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*farClip),
	};

	Vec2V vmin(V_FLT_MAX);
	Vec2V vmax = -vmin;

	for (int i = 0; i < 4; i++)
	{
		const Vec3V p = points[i];
		const Vec2V v = Vec2V(Dot(p, basisX), Dot(p, basisY));

		vmin = Min(v, vmin);
		vmax = Max(v, vmax);
	}

	for (int i = 0; i < 4; i++)
	{
		const Vec3V p = points[i] + sweep;
		const Vec2V v = Vec2V(Dot(p, basisX), Dot(p, basisY));

		vmin = Min(v, vmin);
		vmax = Max(v, vmax);
	}

	const Vec4V planes[] =
	{
		Vec4V(+basisX, -vmax.GetX()),
		Vec4V(+basisY, -vmax.GetY()),
		Vec4V(-basisX, +vmin.GetX()),
		Vec4V(-basisY, +vmin.GetY()),
	};

	spdTransposedPlaneSet4 frustum;
	frustum.SetPlanes(planes);
	return frustum;
}

spdTransposedPlaneSet4 GenerateSweptBox2D(const grcViewport& viewport, Vec3V_In sweep)
{
	const Mat34V cameraMtx = viewport.GetCameraMtx();

	const ScalarV tanHFOV(viewport.GetTanHFOV());
	const ScalarV tanVFOV(viewport.GetTanVFOV());
	const ScalarV farClip(viewport.GetFarClip());

	return GenerateSweptBox2D(cameraMtx, tanHFOV, tanVFOV, farClip, sweep);
}

spdTransposedPlaneSet8 GenerateSweptFrustum(Mat34V_In cameraMtx, ScalarV_In tanHFOV, ScalarV_In tanVFOV, ScalarV_In farClip, Vec3V_In sweep BANK_PARAM(int* pActualPlaneCount) BANK_PARAM(Vec4V* extraPlanes) BANK_PARAM(u32 flags))
{
	const Vec3V frustum_p00  = Transform(cameraMtx, Vec3V(-tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*farClip);
	const Vec3V frustum_p10  = Transform(cameraMtx, Vec3V(+tanHFOV, -tanVFOV, ScalarV(V_NEGONE))*farClip);
	const Vec3V frustum_p01  = Transform(cameraMtx, Vec3V(-tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*farClip);
	const Vec3V frustum_p11  = Transform(cameraMtx, Vec3V(+tanHFOV, +tanVFOV, ScalarV(V_NEGONE))*farClip);
	const Vec3V frustum_apex = cameraMtx.GetCol3();

	const Vec4V plane_nx = BuildPlane(frustum_apex, frustum_p01, frustum_p00); // -x CCW
	const Vec4V plane_ny = BuildPlane(frustum_apex, frustum_p00, frustum_p10); // -y
	const Vec4V plane_px = BuildPlane(frustum_apex, frustum_p10, frustum_p11); // +x
	const Vec4V plane_py = BuildPlane(frustum_apex, frustum_p11, frustum_p01); // +y
	const Vec4V plane_pz = BuildPlane(frustum_p00 , frustum_p01, frustum_p11); // +z

	const Vec4V swept_nx = BuildPlane(frustum_apex + sweep, frustum_p01 + sweep, frustum_p00 + sweep); // -x CCW
	const Vec4V swept_ny = BuildPlane(frustum_apex + sweep, frustum_p00 + sweep, frustum_p10 + sweep); // -y
	const Vec4V swept_px = BuildPlane(frustum_apex + sweep, frustum_p10 + sweep, frustum_p11 + sweep); // +x
	const Vec4V swept_py = BuildPlane(frustum_apex + sweep, frustum_p11 + sweep, frustum_p01 + sweep); // +y
	const Vec4V swept_pz = BuildPlane(frustum_p00  + sweep, frustum_p01 + sweep, frustum_p11 + sweep); // +z

	const ScalarV facing_nx = Dot(plane_nx.GetXYZ(), -sweep);
	const ScalarV facing_ny = Dot(plane_ny.GetXYZ(), -sweep);
	const ScalarV facing_px = Dot(plane_px.GetXYZ(), -sweep);
	const ScalarV facing_py = Dot(plane_py.GetXYZ(), -sweep);
	const ScalarV facing_pz = Dot(plane_pz.GetXYZ(), -sweep);

	const Vec3V centroid = (frustum_p00 + frustum_p10 + frustum_p01 + frustum_p11 + frustum_apex)*ScalarV(1.0f/5.0f) + sweep*ScalarV(V_HALF);
	const ScalarV thresh(V_FLT_SMALL_4);

#if __BANK
	if (flags & BIT(0x12))
	{
		grcDebugDraw::Sphere(frustum_p00         , 0.5f, Color32(255,  0,255,255), false);
		grcDebugDraw::Sphere(frustum_p10         , 0.5f, Color32(255,  0,255,255), false);
		grcDebugDraw::Sphere(frustum_p01         , 0.5f, Color32(255,  0,255,255), false);
		grcDebugDraw::Sphere(frustum_p11         , 0.5f, Color32(255,  0,255,255), false);
		grcDebugDraw::Sphere(frustum_apex        , 0.5f, Color32(255,  0,255,255), false);
		grcDebugDraw::Sphere(frustum_p00  + sweep, 0.5f, Color32(255,128,255,255), false);
		grcDebugDraw::Sphere(frustum_p10  + sweep, 0.5f, Color32(255,128,255,255), false);
		grcDebugDraw::Sphere(frustum_p01  + sweep, 0.5f, Color32(255,128,255,255), false);
		grcDebugDraw::Sphere(frustum_p11  + sweep, 0.5f, Color32(255,128,255,255), false);
		grcDebugDraw::Sphere(frustum_apex + sweep, 0.5f, Color32(255,128,255,255), false);
		grcDebugDraw::Sphere(centroid            , 0.5f, Color32(255,255,255,255), false);
	}
#endif // __BANK

	Vec4V planes[13]; // actual count will always be less than this ..
	int planeCount = 0;

	if (IsLessThanAll(facing_nx*facing_ny, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x00)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_apex, frustum_p00, sweep, centroid); }
	if (IsLessThanAll(facing_px*facing_ny, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x01)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_apex, frustum_p10, sweep, centroid); }
	if (IsLessThanAll(facing_nx*facing_py, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x02)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_apex, frustum_p01, sweep, centroid); }
	if (IsLessThanAll(facing_px*facing_py, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x03)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_apex, frustum_p11, sweep, centroid); }

	if (IsLessThanAll(facing_nx*facing_pz, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x04)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_p01, frustum_p00, sweep, centroid); }
	if (IsLessThanAll(facing_ny*facing_pz, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x05)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_p00, frustum_p10, sweep, centroid); }
	if (IsLessThanAll(facing_px*facing_pz, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x06)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_p10, frustum_p11, sweep, centroid); }
	if (IsLessThanAll(facing_py*facing_pz, ScalarV(V_ZERO)) BANK_ONLY(&& (flags & BIT(0x07)))) { planes[planeCount++] = BuildSilhouettePlane(frustum_p11, frustum_p01, sweep, centroid); }

	if (IsGreaterThanAll(facing_nx, thresh) BANK_ONLY(&& (flags & BIT(0x08)))) { planes[planeCount++] = plane_nx; } else if (IsLessThanAll(facing_nx, -thresh) BANK_ONLY(&& (flags & BIT(0x0d)))) { planes[planeCount++] = swept_nx; }
	if (IsGreaterThanAll(facing_ny, thresh) BANK_ONLY(&& (flags & BIT(0x09)))) { planes[planeCount++] = plane_ny; } else if (IsLessThanAll(facing_ny, -thresh) BANK_ONLY(&& (flags & BIT(0x0e)))) { planes[planeCount++] = swept_ny; }
	if (IsGreaterThanAll(facing_px, thresh) BANK_ONLY(&& (flags & BIT(0x0a)))) { planes[planeCount++] = plane_px; } else if (IsLessThanAll(facing_px, -thresh) BANK_ONLY(&& (flags & BIT(0x0f)))) { planes[planeCount++] = swept_px; }
	if (IsGreaterThanAll(facing_py, thresh) BANK_ONLY(&& (flags & BIT(0x0b)))) { planes[planeCount++] = plane_py; } else if (IsLessThanAll(facing_py, -thresh) BANK_ONLY(&& (flags & BIT(0x10)))) { planes[planeCount++] = swept_py; }
	if (IsGreaterThanAll(facing_pz, thresh) BANK_ONLY(&& (flags & BIT(0x0c)))) { planes[planeCount++] = plane_pz; } else if (IsLessThanAll(facing_pz, -thresh) BANK_ONLY(&& (flags & BIT(0x11)))) { planes[planeCount++] = swept_pz; }

	for (int i = planeCount; i < 8; i++)
	{
		planes[i] = Vec4V(V_ZERO);
	}

#if __BANK
	if (pActualPlaneCount)
	{
		*pActualPlaneCount = planeCount;
	}

	if (extraPlanes)
	{
		for (int i = 8; i < planeCount; i++)
		{
			extraPlanes[i] = planes[i];
		}
	}
#endif // __BANK

	spdTransposedPlaneSet8 frustum;
	frustum.SetPlanes(planes);
	return frustum;
}

spdTransposedPlaneSet8 GenerateSweptFrustum(const grcViewport& viewport, Vec3V_In sweep BANK_PARAM(int* pActualPlaneCount))
{
	const Mat34V cameraMtx = viewport.GetCameraMtx();

	const ScalarV tanHFOV(viewport.GetTanHFOV());
	const ScalarV tanVFOV(viewport.GetTanVFOV());
	const ScalarV farClip(viewport.GetFarClip());

	return GenerateSweptFrustum(cameraMtx, tanHFOV, tanVFOV, farClip, sweep BANK_PARAM(pActualPlaneCount));
}

#if __BANK

void PlaneSetDebugDraw(const Vec4V* planes, int numPlanes, const Color32& colour)
{
	const int maxPlanes = 32;

	if (AssertVerify(numPlanes <= maxPlanes) && colour.GetAlpha() > 0)
	{
		for (int i = 0; i < numPlanes; i++)
		{
			const float nx = planes[i].GetXf();
			const float ny = planes[i].GetYf();
			const float nz = planes[i].GetZf();
			const float nw = planes[i].GetWf();

			const Vec3V absNormal = Abs(planes[i].GetXYZ());
			const float absMax = MaxElement(absNormal).Getf();

			float x00, y00, z00;
			float x10, y10, z10;
			float x01, y01, z01;
			float x11, y11, z11;

			if (absMax == absNormal.GetZf()) // project onto XY plane
			{
				x00 = +16000.0f; y00 = +16000.0f; z00 = -(nx*x00 + ny*y00 + nw)/nz;
				x10 = -16000.0f; y10 = +16000.0f; z10 = -(nx*x10 + ny*y10 + nw)/nz;
				x01 = +16000.0f; y01 = -16000.0f; z01 = -(nx*x01 + ny*y01 + nw)/nz;
				x11 = -16000.0f; y11 = -16000.0f; z11 = -(nx*x11 + ny*y11 + nw)/nz;
			}
			else if (absMax == absNormal.GetXf()) // project onto YZ plane
			{
				y00 = +16000.0f; z00 = +16000.0f; x00 = -(ny*y00 + nz*z00 + nw)/nx;
				y10 = -16000.0f; z10 = +16000.0f; x10 = -(ny*y10 + nz*z10 + nw)/nx;
				y01 = +16000.0f; z01 = -16000.0f; x01 = -(ny*y01 + nz*z01 + nw)/nx;
				y11 = -16000.0f; z11 = -16000.0f; x11 = -(ny*y11 + nz*z11 + nw)/nx;
			}
			else // project onto ZX plane
			{
				z00 = +16000.0f; x00 = +16000.0f; y00 = -(nz*z00 + nx*x00 + nw)/ny;
				z10 = -16000.0f; x10 = +16000.0f; y10 = -(nz*z10 + nx*x10 + nw)/ny;
				z01 = +16000.0f; x01 = -16000.0f; y01 = -(nz*z01 + nx*x01 + nw)/ny;
				z11 = -16000.0f; x11 = -16000.0f; y11 = -(nz*z11 + nx*x11 + nw)/ny;
			}

			Vec3V temp0[4 + maxPlanes];
			Vec3V temp1[4 + maxPlanes];
			Vec3V* clippedSrc = temp0;
			Vec3V* clippedDst = temp1;
			int clippedCount = 0;

			clippedSrc[clippedCount++] = Vec3V(x00, y00, z00);
			clippedSrc[clippedCount++] = Vec3V(x10, y10, z10);
			clippedSrc[clippedCount++] = Vec3V(x11, y11, z11);
			clippedSrc[clippedCount++] = Vec3V(x01, y01, z01);

			for (int j = 0; j < numPlanes && clippedCount >= 3; j++)
			{
				if (j != i)
				{
					if (MaxElement(Abs(planes[i] - planes[j])).Getf() < 0.001f ||
						MaxElement(Abs(planes[i] + planes[j])).Getf() < 0.001f)
					{
						continue; // planes are nearly coincident
					}

					clippedCount = PolyClip(clippedDst, 12, clippedSrc, clippedCount, -planes[j]);
					std::swap(clippedSrc, clippedDst);
				}
			}

			if (clippedCount >= 3)
			{
				for (int j = 0; j < clippedCount; j++)
				{
					grcDebugDraw::Line(clippedSrc[j], clippedSrc[(j + 1)%clippedCount], colour);
				}
			}
		}
	}
}

void PlaneSetDebugDraw(const spdTransposedPlaneSet4& planeSet, const Color32& colour)
{
	Vec4V planes[4];
	planeSet.GetPlanes(planes);
	PlaneSetDebugDraw(planes, NELEM(planes), colour);
}

void PlaneSetDebugDraw(const spdTransposedPlaneSet8& planeSet, const Color32& colour)
{
	Vec4V planes[8];
	planeSet.GetPlanes(planes);
	PlaneSetDebugDraw(planes, NELEM(planes), colour);
}

#endif // __BANK
#endif // !__SPU

bool TestLineSegmentAgainstViewport(Vec3V_In p0, Vec3V_In p1, const grcViewport& viewport)
{
	const Vec4V negPlanes[] = // planes point inside, so we'll reverse the test ..
	{
		viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_LEFT  ),
		viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_RIGHT ),
		viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_TOP   ),
		viewport.GetFrustumClipPlane(grcViewport::CLIP_PLANE_BOTTOM),
	};

	Vec3V temp0 = p0;
	Vec3V temp1 = p1;

	for (int i = 0; i < NELEM(negPlanes); i++)
	{
		const ScalarV d0 = PlaneDistanceTo(negPlanes[i], temp0);
		const ScalarV d1 = PlaneDistanceTo(negPlanes[i], temp1);
		const u32 g0 = IsLessThanOrEqualAll(d0, ScalarV(V_ZERO)); // 1 if temp0 is outside
		const u32 g1 = IsLessThanOrEqualAll(d1, ScalarV(V_ZERO)); // 1 if temp1 is outside

		if ((g0&g1) == 1)
		{
			return false; // completely outside
		}
		else if ((g0|g1) == 0)
		{
			// completely inside, do nothing
		}
		else
		{
			const Vec3V clip = (p0*d1 - p1*d0)/(d1 - d0);

			if (g0)
			{
				temp1 = clip;
			}
			else // g1
			{
				temp0 = clip;
			}
		}
	}

	return true;
}

} // namespace rage
