// Title	:	Rect.h
// Author	:	Richard Jobling
// Started	:	10/03/99

#ifndef _RECT_H_
#define _RECT_H_

// Rage headers
#include "vector/vector2.h"
#include "spatialdata/aabb.h"

namespace rage {

// why another 2D box class? fwRect's "top" and "bottom" are too confusing
class ALIGNAS(16) fwBox2D
{
public:
	__forceinline fwBox2D() {}
	__forceinline fwBox2D(const Vector2& point) : x0(point.x), y0(point.y), x1(point.x), y1(point.y) {}
	__forceinline fwBox2D(float x0_, float y0_, float x1_, float y1_) : x0(x0_), y0(y0_), x1(x1_), y1(y1_) {}

	__forceinline bool IsInside(const Vector2& point) const;
	__forceinline void Grow(const Vector2& point);
	__forceinline void Grow(const fwBox2D& box);

	float x0,y0,x1,y1;
} ;

__forceinline bool fwBox2D::IsInside(const Vector2& point) const
{
	return (point.x >= x0 && point.y >= y0 && point.x <= x1 && point.y <= y1);
}

__forceinline void fwBox2D::Grow(const Vector2& point)
{
	x0 = Min<float>(x0, point.x);
	y0 = Min<float>(y0, point.y);
	x1 = Max<float>(x1, point.x);
	y1 = Max<float>(y1, point.y);
}

__forceinline void fwBox2D::Grow(const fwBox2D& box)
{
	x0 = Min<float>(x0, box.x0);
	y0 = Min<float>(y0, box.y0);
	x1 = Max<float>(x1, box.x1);
	y1 = Max<float>(y1, box.y1);
}

// ================================================================================================

class fwRect
{
public:

	fwRect();
	fwRect(float left, float bottom, float right, float top);
	fwRect(const Vector2 pos, float size);
	fwRect(const Vector2 pos, float halfwidth, float halfheight);
	fwRect(const spdAABB& box);

	void Init();
	fwRect& Add(float x, float y);
	fwRect& operator+=(const Vector2& v);
	fwRect& operator+=(const fwRect& r);
	
	void Grow(float x, float y);
	bool IsInside(const Vector2& v) const;
	bool IsInside(const fwRect& r) const;
	
	bool DoesIntersectApprox(const Vector2& v, float radius) const;
	bool DoesIntersect(const Vector2& v, float width, float height) const;
	bool DoesIntersect(const fwRect& r) const;
	
	void GetCentre(float& x, float& y) const;
	
	bool IsInvalidOrZeroArea() const {return (left > right || bottom > top);}
	friend bool operator==(const fwRect& r1, const fwRect& r2) { return ((r1.left == r2.left) && (r1.bottom == r2.bottom) && (r1.right == r2.right) && (r1.top == r2.top)); }
	friend bool operator!=(const fwRect& r1, const fwRect& r2) { return !(r1 == r2); }

	float GetWidth() const { return right - left; }
	float GetHeight() const { return top - bottom; }
	float GetLeft() const {return left;}
	float GetRight() const {return right;}
	float GetTop() const {return top;}
	float GetBottom() const {return bottom;}

	float left, top, right, bottom;
};

inline fwRect::fwRect()
{
	Init();
}

inline fwRect::fwRect(float leftArg, float bottomArg, float rightArg, float topArg)
{
	this->left = leftArg;
	this->bottom = bottomArg;
	this->right = rightArg;
	this->top = topArg;
}

inline fwRect::fwRect(const Vector2 pos, float size)
{
	this->left = pos.x - size;
	this->bottom = pos.y - size;
	this->right = pos.x + size;
	this->top = pos.y + size;
}

inline fwRect::fwRect(const Vector2 pos, float halfwidth, float halfheight)
{
	this->left = pos.x - halfwidth;
	this->bottom = pos.y - halfheight;
	this->right = pos.x + halfwidth;
	this->top = pos.y + halfheight;
}

inline fwRect::fwRect(const spdAABB& box)
{
	this->left = box.GetMin().GetXf();
	this->bottom = box.GetMin().GetYf();
	this->right = box.GetMax().GetXf();
	this->top = box.GetMax().GetYf();
}

inline void fwRect::Init()
{
	left = 1000000.0f;
	bottom = 1000000.0f;
	right = -1000000.0f;
	top = -1000000.0f;
}

inline fwRect& fwRect::Add(float x, float y)
{
	if(x < left)
		left = x;
	if(x > right)
		right = x;
	if(y < bottom)
		bottom = y;
	if(y > top)
		top = y;
	return *this;	
}

inline fwRect& fwRect::operator+=(const Vector2& v)
{
	if(v.x < left)
		left = v.x;
	if(v.x > right)
		right = v.x;
	if(v.y < bottom)
		bottom = v.y;
	if(v.y > top)
		top = v.y;
	return *this;	
}

inline fwRect& fwRect::operator+=(const fwRect& r)
{
	if(r.left < left)
		left = r.left;
	if(r.right > right)
		right = r.right;
	if(r.bottom < bottom)
		bottom = r.bottom;
	if(r.top > top)
		top = r.top;
	return *this;
}

inline void fwRect::Grow(float x, float y)
{
	left -= x;
	right += x;
	bottom -= y;
	top += y;
}

//
// name:		IsInside
// description:	Returns if point is inside the rectangle
inline bool fwRect::IsInside(const Vector2& v) const
{
	if(v.x < left || v.x > right || v.y < bottom || v.y > top)
		return false;
	return true;	
}

inline bool fwRect::IsInside(const fwRect& r) const
{
	if(left <= r.left && right >= r.right && bottom <= r.bottom && top >= r.top)
		return true;
	return false;	
}

//
// name:		DoesIntersectApprox
// description:	Returns if the following volumes intersect with the rectangle
inline bool fwRect::DoesIntersectApprox(const Vector2& v, float radius) const
{
	if(v.x < left-radius || v.x > right+radius || v.y < bottom-radius || v.y > top+radius)
		return false;
	return true;	
}

inline bool fwRect::DoesIntersect(const Vector2& v, float width, float height) const
{
	width /= 2.0f;
	height /= 2.0f;
	if(v.x < left-width || v.x > right+width || v.y < bottom-height || v.y > top+height)
		return false;
	return true;	
}

inline bool fwRect::DoesIntersect(const fwRect& r) const
{
	if(left > r.right || right < r.left || bottom > r.top || top < r.bottom)
		return false;
	return true;	
}

inline void fwRect::GetCentre(float& x, float& y) const 
{
	x = (left+right)/2.0f; 
	y = (top+bottom)/2.0f;
}

} // namespace rage

#endif


