#ifndef INC_RANGE_H_
#define INC_RANGE_H_

// --- Include Files ------------------------------------------------------------

// Game headers
#include "fwmaths/Random.h"

namespace rage {

// --- Structure/Class Definitions ----------------------------------------------

template<typename T>
class fwRange
{
public:

	//
	// Constructors
	//

	fwRange()
	{
		// Uninitialised
	}

	fwRange(T value)
		: m_min(value)
		, m_max(value)
	{
	}

	fwRange(T min, T max)
		: m_min(min)
		, m_max(max)
	{
	}

	fwRange(const fwRange& other)
		: m_min(other.m_min)
		, m_max(other.m_max)
	{
	}

	//
	// Accessors
	//

	T GetValue() const { return fwRandom::GetRandomNumberInRange(m_min, m_max); }

private:

	// Min/Max
	T m_min;
	T m_max;
};

// s16 specialisation
template<>
inline s16 fwRange<s16>::GetValue() const { return static_cast<s16>(fwRandom::GetRandomNumberInRange(m_min, m_max)); }

} // namespace rage 

#endif // INC_RANGE_H_
