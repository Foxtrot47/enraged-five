// ======================
// fwmaths/kDOP18.h
// (c) 2012 RockstarNorth
// ======================

#ifndef _FWMATHS_KDOP18_H_
#define _FWMATHS_KDOP18_H_

#include "vectormath/vec4v.h"

namespace rage {

class fwEntity;
class grcViewport;
class rmcDrawable;
class spdTransposedPlaneSet4;
class spdTransposedPlaneSet8;
class Color32;

class fwkDOP18Face
{
public:
	static const int MAX_POINTS_PER_FACE = 8;

	Vec4V m_plane;
	Vec3V m_points[MAX_POINTS_PER_FACE];
	int   m_pointCount;

	fwkDOP18Face() {}
	fwkDOP18Face(Vec4V_In plane, Vec3V_In p0, Vec3V_In p1, Vec3V_In p2, Vec3V_In p3, Vec3V_In p4, Vec3V_In p5, Vec3V_In p6, Vec3V_In p7, float tolerance, bool bFlip);

	bool HasArea(float tolerance) const; // true if face has non-zero area
};

class fwkDOP18Vertices // 96 points
{
public:
	Vec3V pxpypz_0, nxpypz_0, pxnypz_0, nxnypz_0, pxpynz_0, nxpynz_0, pxnynz_0, nxnynz_0;
	Vec3V pypzpx_0, nypzpx_0, pynzpx_0, nynzpx_0, pypznx_0, nypznx_0, pynznx_0, nynznx_0;
	Vec3V pzpxpy_0, nzpxpy_0, pznxpy_0, nznxpy_0, pzpxny_0, nzpxny_0, pznxny_0, nznxny_0;

	Vec3V pypxpz_0, pynxpz_0, nypxpz_0, nynxpz_0, pypxnz_0, pynxnz_0, nypxnz_0, nynxnz_0;
	Vec3V pzpypx_0, pznypx_0, nzpypx_0, nznypx_0, pzpynx_0, pznynx_0, nzpynx_0, nznynx_0;
	Vec3V pxpzpy_0, pxnzpy_0, nxpzpy_0, nxnzpy_0, pxpzny_0, pxnzny_0, nxpzny_0, nxnzny_0;

	Vec3V pxpypz_1, nxpypz_1, pxnypz_1, nxnypz_1, pxpynz_1, nxpynz_1, pxnynz_1, nxnynz_1;
	Vec3V pypzpx_1, nypzpx_1, pynzpx_1, nynzpx_1, pypznx_1, nypznx_1, pynznx_1, nynznx_1;
	Vec3V pzpxpy_1, nzpxpy_1, pznxpy_1, nznxpy_1, pzpxny_1, nzpxny_1, pznxny_1, nznxny_1;

	Vec3V pypxpz_1, pynxpz_1, nypxpz_1, nynxpz_1, pypxnz_1, pynxnz_1, nypxnz_1, nynxnz_1;
	Vec3V pzpypx_1, pznypx_1, nzpypx_1, nznypx_1, pzpynx_1, pznynx_1, nzpynx_1, nznynx_1;
	Vec3V pxpzpy_1, pxnzpy_1, nxpzpy_1, nxnzpy_1, pxpzny_1, pxnzny_1, nxpzny_1, nxnzny_1;
};

class fwkDOP18VerticesSoA // 24 bundles of 4 transposed points (stored in 60 vectors)
{
public:
	Vec4V xyz_p0_x, xyz_p0_y, xyz_p0_z, xyz_n0_z;
	Vec4V yzx_p0_y, yzx_p0_z, yzx_p0_x, yzx_n0_x;
	Vec4V zxy_p0_z, zxy_p0_x, zxy_p0_y, zxy_n0_y;

	Vec4V yxz_p0_y, yxz_p0_x, yxz_p0_z, yxz_n0_z;
	Vec4V zyx_p0_z, zyx_p0_y, zyx_p0_x, zyx_n0_x;
	Vec4V xzy_p0_x, xzy_p0_z, xzy_p0_y, xzy_n0_y;

	Vec4V xyz_p1_x, xyz_p1_y, xyz_p1_z, xyz_n1_x, xyz_n1_y, xyz_n1_z;
	Vec4V yzx_p1_y, yzx_p1_z, yzx_p1_x, yzx_n1_y, yzx_n1_z, yzx_n1_x;
	Vec4V zxy_p1_z, zxy_p1_x, zxy_p1_y, zxy_n1_z, zxy_n1_x, zxy_n1_y;

	Vec4V yxz_p1_y, yxz_p1_x, yxz_p1_z, yxz_n1_y, yxz_n1_x, yxz_n1_z;
	Vec4V zyx_p1_z, zyx_p1_y, zyx_p1_x, zyx_n1_z, zyx_n1_y, zyx_n1_x;
	Vec4V xzy_p1_x, xzy_p1_z, xzy_p1_y, xzy_n1_x, xzy_n1_z, xzy_n1_y;
};

class fwkDOP18
{
public:
	static const int MAX_VERTS = 96;
	static const int MAX_FACES = 18;

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4201) // warning C4201: nonstandard extension used : nameless struct/union
#endif // __WIN32
	union // axis
	{
		struct { float px, nx, py, ny, pz, nz; };
		struct { float m_axis[6]; };
	};

	union // diag
	{
		struct { float pxpy, nxpy, pxny, nxny, pypz, nypz, pynz, nynz, pzpx, nzpx, pznx, nznx; };
		struct { float m_diag[12]; };
	};
#if __WIN32
#pragma warning(pop)
#endif // __WIN32

	void SetEmpty();
	bool IsEmpty() const;

	void AddPoints(const Vec3V* points, int pointCount);
	void AddPoint(Vec3V_In p);
#if __BANK && !__SPU
	int CreateFromDrawable(const fwEntity* pEntity, const rmcDrawable* pDrawable, const spdAABB* box = NULL);
	static const fwkDOP18* GetCachedFromDrawable(const fwEntity* pEntity, const rmcDrawable* pDrawable);
#endif // __BANK && !__SPU

	void QuantiseDiagonals(u8 diag[12]) const;
	void UnQuantiseDiagonals(const u8 diag[12]);

	void BuildVerticesSoA(fwkDOP18VerticesSoA& v) const;
	void BuildVertices(Vec3V verts[MAX_VERTS]) const;
	int BuildVerticesAndFaces(Vec3V verts[MAX_VERTS], fwkDOP18Face faces[MAX_FACES], float tolerance) const; // returns number of faces

	bool IntersectsPoint(Vec3V_In point) const;
	bool Intersects(const fwkDOP18& kDOP18) const;
#if __BANK
	bool IntersectsFrustum_REFERENCE(const spdTransposedPlaneSet4& sps) const; // this is incredibly slow .. not for production code
	bool IntersectsFrustum_REFERENCE(const spdTransposedPlaneSet8& sps) const; // this is incredibly slow .. not for production code
#if !__SPU
	void DebugDrawEdges(const Color32& colour) const;
	void DebugDrawBound(const Color32& colour, const grcViewport& vp, bool bUseOnlyAABBBound = false) const;
#endif // !__SPU
#endif // __BANK
};

CompileTimeAssert(sizeof(fwkDOP18Vertices) == fwkDOP18::MAX_VERTS*sizeof(Vec3V));

} // namespace rage

#endif // _FWMATHS_KDOP18_H_
