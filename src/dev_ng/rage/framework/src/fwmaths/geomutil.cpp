// ======================
// fwmaths/geomutil.cpp
// (c) 2011 RockstarNorth
// ======================

#include "phBound/BoundSphere.h"
#include "phCore/Segment.h"
#include "physics/shapetest.h"
#include "spatialdata/sphere.h"
#include "spatialdata/aabb.h"

#include "fwmaths/Vector.h"
#include "fwmaths/geomutil.h"

namespace rage {
namespace fwGeom {

// Name			:	DistToLineSqr
// Purpose		:	Calculates the squared distance of a point to a line
// Returns		:	distance squared

float DistToLineSqr(const Vector3 &vecStart, const Vector3 &vecEnd, const Vector3 &testPoint)
{
	Vector3 vecDirection = vecEnd - vecStart;
	Vector3 vec = testPoint - vecStart;

	float fDistAlongLineTimesLineLength = DotProduct(vec, vecDirection);

	if (fDistAlongLineTimesLineLength <= 0.0f) // Point on start side of line
	{
		return vec.Mag2();
	}

	float fLineLengthSqr = vecDirection.Mag2();

	if (fDistAlongLineTimesLineLength >= fLineLengthSqr) // Point on end side of line
	{
		return (testPoint - vecEnd).Mag2();
	}

	float fDistAlongLineSqrTimesLineLengthSqr = fDistAlongLineTimesLineLength*fDistAlongLineTimesLineLength;
	float fDistAlongLineSqr = fDistAlongLineSqrTimesLineLengthSqr/fLineLengthSqr;
	float fDistToLineSqr = vec.Mag2() - fDistAlongLineSqr;

	if (fDistToLineSqr <= 0.0f) // should never be < 0, but just to be safe
	{
		return 0.0f;
	}
	else
	{
		return fDistToLineSqr;
	}
}

// Name			:	DistToMathematicalLine
// Purpose		:	Calculates the distance of a point to an infinite line
// Returns		:	distance

float DistToMathematicalLine(const Vector3 &vecStart, const Vector3 &vecEnd, const Vector3 &testPoint)
{
	Vector3 vecDirection = vecEnd - vecStart;
	Vector3 vec = testPoint - vecStart;

	float fLineLengthSqr = vecDirection.Mag2();

	float fDistAlongLineTimesLineLength = DotProduct(vec, vecDirection);
	float fDistAlongLineSqrTimesLineLengthSqr = fDistAlongLineTimesLineLength*fDistAlongLineTimesLineLength;
	float fDistAlongLineSqr = fDistAlongLineSqrTimesLineLengthSqr/fLineLengthSqr;
	float fDistToLineSqr = vec.Mag2() - fDistAlongLineSqr;

	if (fDistToLineSqr <= 0.0f) // should never be < 0, but just to be safe
	{
		return 0.0f;
	}
	else
	{
		return rage::Sqrtf(fDistToLineSqr);
	}
}

// Name			:	DistAlongLine2D
// Purpose		:	Works out how far along a 2d line the given point is.
// Parameters	:	LineDir must have length 1
// Returns		:	distance

float DistAlongLine2D(float LineStartX, float LineStartY, float LineDirX, float LineDirY, float PointX, float PointY)
{
	float	DeltaX = PointX - LineStartX;
	float	DeltaY = PointY - LineStartY;

	float fDistAlongLine = DeltaX * LineDirX + DeltaY * LineDirY;

	return fDistAlongLine;
}

// FUNCTION : Test2DLineAgainst2DLine
// PURPOSE :  Tests whether 2 line segments intersect.
//			  It works by finding out whether for line 1 both points of line 2 are on separate side of the line
//			  and it will do the same thing for the other line.

bool Test2DLineAgainst2DLine(float Start1X, float Start1Y, float Delta1X, float Delta1Y, float Start2X, float Start2Y, float Delta2X, float Delta2Y)
{
	float	DotPr1 = (Start2X - Start1X) * Delta1Y - (Start2Y - Start1Y) * Delta1X;
	float	DotPr2 = (Start2X - Start1X + Delta2X) * Delta1Y - (Start2Y - Start1Y + Delta2Y) * Delta1X;
	if (DotPr1 * DotPr2 > 0.0f) return false;

	DotPr1 = (Start1X - Start2X) * Delta2Y - (Start1Y - Start2Y) * Delta2X;
	DotPr2 = (Start1X - Start2X + Delta1X) * Delta2Y - (Start1Y - Start2Y + Delta1Y) * Delta2X;
	if (DotPr1 * DotPr2 > 0.0f) return false;
	return true;
}


//Solve ax^2 + bx + c = 0 with solutions xplus and xminus
static __forceinline bool SolveQuadratic(const float a, const float b, const float c, float& xminus, float& xplus)
{
	const float alpha=b*b-4*a*c;
	if(alpha<0)
	{
		return false;
	}
	else
	{
		const float beta=rage::Sqrtf(alpha);
		xplus=0.5f*(-b+beta)/a;
		xminus=0.5f*(-b-beta)/a;
		return true;
	}	
}

bool IntersectSphereRay(const spdSphere& sphere, const Vector3& v, const Vector3& w, Vector3& v1, Vector3& v2)
{
	// [SPHERE-OPTIMISE] optimise this when moving this to spdSphere
	const Vector3 p=VEC3V_TO_VECTOR3(sphere.GetCenter());
	const float r=sphere.GetRadiusf();

	const Vector3 vp=v-p;

	const float a=1.0f;
	const float b=Dot(RCC_VEC3V(vp),RCC_VEC3V(w)).Getf()*2.0f;
	const float c=MagSquared(RCC_VEC3V(vp)).Getf()-r*r;

	float tMinus;
	float tPlus;
	if(!SolveQuadratic(a,b,c,tMinus,tPlus))
	{
		return false;
	}

	v1=v+w*tMinus;
	v2=v+w*tPlus;

	return true;	
}

// NB: Have to convert to & from model-space each side of the test.
bool IntersectSphereEdge(const spdSphere& sphere, const Vector3& start, const Vector3& end, Vector3& v1, Vector3& v2)
{
	//mthAssert(rage::FPIsFinite(start.x)&&rage::FPIsFinite(start.y)&&rage::FPIsFinite(start.z));
	//mthAssert(rage::FPIsFinite(end.x)&&rage::FPIsFinite(end.y)&&rage::FPIsFinite(end.z));

	phBoundSphere phSphere(sphere.GetRadiusf());
	phIntersection phIsect[2];
	phSegment seg;
	seg.Set(start - VEC3V_TO_VECTOR3(sphere.GetCenter()), end - VEC3V_TO_VECTOR3(sphere.GetCenter()));	// Get the segment coords local to the sphere

	phShapeTest<phShapeEdge> shapeTest;
	shapeTest.InitEdge(seg, phIsect, 2);
	if(shapeTest.TestOneObject(phSphere))
	{
		// Put the intersection points back into worldspace
		v1 = RCC_VECTOR3(phIsect[0].GetPosition()) + VEC3V_TO_VECTOR3(sphere.GetCenter());
		v2 = RCC_VECTOR3(phIsect[1].GetPosition()) + VEC3V_TO_VECTOR3(sphere.GetCenter());

		phSphere.Release(false);
		return true;
	}
	else
	{
		phSphere.Release(false);
		return false;
	}
}

bool IntersectBoxRay(const spdAABB& box, const Vector3 &origin, const Vector3 &direction)
{
	// Taken from http://www.cs.utah.edu/~awilliam/box/

	const Vector3	inv_direction = Vector3( 1.0f / direction.x, 1.0f / direction.y, 1.0f / direction.z );
	const s32		sign[3] = { inv_direction.x < 0, inv_direction.y < 0, inv_direction.z < 0 };
	const Vector3	parameters[2] = { box.GetMinVector3(), box.GetMaxVector3() };
	const float		t0 = 0;

	float tmin, tmax, tymin, tymax, tzmin, tzmax;

	tmin = (parameters[sign[0]].x - origin.x) * inv_direction.x;
	tmax = (parameters[1-sign[0]].x - origin.x) * inv_direction.x;
	tymin = (parameters[sign[1]].y - origin.y) * inv_direction.y;
	tymax = (parameters[1-sign[1]].y - origin.y) * inv_direction.y;
	if ( (tmin > tymax) || (tymin > tmax) ) 
		return false;
	if (tymin > tmin)
		tmin = tymin;
	if (tymax < tmax)
		tmax = tymax;
	tzmin = (parameters[sign[2]].z - origin.z) * inv_direction.z;
	tzmax = (parameters[1-sign[2]].z - origin.z) * inv_direction.z;
	if ( (tmin > tzmax) || (tzmin > tmax) ) 
		return false;
	if (tzmin > tmin)
		tmin = tzmin;
	if (tzmax < tmax)
		tmax = tzmax;
	return (tmax > t0);
}

} // namespace fwGeom
} // namespace rage

#if 0

static void __DistanceToLineTest() // for validating distance-to-line functions using an image
{
	const int w = 512;
	const int h = 512;

	grcImage* image = grcImage::Create(w, h, 1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
	float* data = rage_new float[w*h];

	for (int test = 0; test < 6; test++)
	{
		float  dmin = +999999.0f;
		float  dmax = -999999.0f;

		const Vector3 p0 = Vector3(0.375f, 0.375f, 0.0f);
		const Vector3 p1 = Vector3(0.875f, 0.500f, 0.0f);
		const Vector3 pv = p1 - p0;

		Vector3 pn = pv; pn.Normalize();

		for (int j = 0; j < h; j++)
		{
			for (int i = 0; i < w; i++)
			{
				const float x =        (float)i/(float)(w - 1);
				const float y = 1.0f - (float)j/(float)(h - 1);
				const Vector3 p = Vector3(x, y, 0.0f);

				float d = 0.0f;

				switch (test)
				{
				case 0: d =       geomDistances::DistanceSegToPoint    (p0, pv, p); break;
				case 1: d =              fwGeom::DistToLine            (p0, p1, p); break;
				case 2: d = sqrtf(             __DistToLineSqr         (p0, p1, p)); break;
				case 3: d =              fwGeom::DistToMathematicalLine(p0, p1, p); break; // inf
				case 4: d =       geomDistances::DistanceLineToPoint   (RCC_VEC3V(p0), RCC_VEC3V(pv), RCC_VEC3V(p)).Getf(); break; // inf, uses CROSS PRODUCTS
				case 5: d = sqrtf(geomDistances::Distance2LineToPoint  (p0, pv, p)); break; // inf
				}

				data[i + j*w] = d;
				dmin = Min<float>(d, dmin);
				dmax = Max<float>(d, dmax);

				data[i + j*w] += powf(d*8.0f - floorf(d*8.0f), 64.0f);
				data[i + j*w] += powf(Max<float>(0.0f, 1.0f - (p - p0).Mag()*20.0f), 32.0f);
				data[i + j*w] += powf(Max<float>(0.0f, 1.0f - (p - p1).Mag()*20.0f), 32.0f);

				Vector3 pA = p - p0; pA.Normalize();
				Vector3 pB = p - p1; pB.Normalize();
				data[i + j*w] += powf(Max<float>(0.0f, 1.0f - Abs<float>(pA.Dot(pn))*20.0f), 64.0f)*0.5f;
				data[i + j*w] += powf(Max<float>(0.0f, 1.0f - Abs<float>(pB.Dot(pn))*20.0f), 64.0f)*0.5f;
			}
		}

		Displayf("TESTME(%d): dmin=%f, dmax=%f", test, dmin, dmax);

		if (dmin < dmax)
		{
			Color32* pixels = (Color32*)image->GetBits();

			for (int j = 0; j < h; j++)
			{
				for (int i = 0; i < w; i++)
				{
					const float d = Clamp<float>((data[i + j*w] - dmin)/(dmax - dmin), 0.0f, 1.0f); // [0..1]
					const int di = (int)(d*255.0f + 0.5f);

					pixels[i + j*w] = Color32(di, di, di, 255);
				}
			}

			image->SaveDDS(atVarString("c:/dump/image_%d.dds", test));
		}
	}

	delete[] data;
	image->Release();
}

#endif // 0
