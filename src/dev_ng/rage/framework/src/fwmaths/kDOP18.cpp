// ======================
// fwmaths/kDOP18.cpp
// (c) 2012 RockstarNorth
// ======================

#include "spatialdata/kdop2d.h" // for GetUniquePointsInLoop
#include "spatialdata/transposedplaneset.h"
#include "system/nelem.h"

#include "softrasterizer/softrasterizer.h"

#include "fwmaths/kDOP18.h"
#include "fwmaths/vectorutil.h"
#include "fwutil/xmacro.h"

// stuff we need to build kDOP18 from a drawable
#if __BANK && !__SPU
#include "grcore/debugdraw.h"
#if __PS3
#include "grcore/edgeExtractgeomspu.h"
#endif // __PS3
#include "grmodel/geometry.h"
#include "rmcore/drawable.h"
#include "entity/entity.h"
#endif // __BANK && !__SPU

namespace rage {

fwkDOP18Face::fwkDOP18Face(Vec4V_In plane, Vec3V_In p0, Vec3V_In p1, Vec3V_In p2, Vec3V_In p3, Vec3V_In p4, Vec3V_In p5, Vec3V_In p6, Vec3V_In p7, float tolerance, bool bFlip)
{
	const Vec3V temp[MAX_POINTS_PER_FACE] = {p0,p1,p2,p3,p4,p5,p6,p7};
	m_plane = plane;
	m_pointCount = GetUniquePointsInLoop<Vec3V,MAX_POINTS_PER_FACE>(m_points, temp, ScalarV(tolerance));

	if (bFlip)
	{
		for (int i = 0; i < m_pointCount - i - 1; i++)
		{
			std::swap(m_points[i], m_points[m_pointCount - i - 1]);
		}
	}
}

bool fwkDOP18Face::HasArea(float tolerance) const // true if face has non-zero area
{
	if (m_pointCount > 2)
	{
		Vec3V p0 = m_points[m_pointCount - 2];
		Vec3V p1 = m_points[m_pointCount - 1];

		for (int i = 0; i < m_pointCount; i++)
		{
			const Vec3V p2 = m_points[i];

			if (MagSquared(Cross(p2 - p1, p1 - p0)).Getf() > tolerance)
			{
				return true;
			}

			p0 = p1;
			p1 = p2;
		}
	}

	return false;
}

void fwkDOP18::SetEmpty()
{
	for (int i = 0; i < NELEM(m_axis); i++)
	{
		m_axis[i] = -FLT_MAX;
	}

	for (int i = 0; i < NELEM(m_diag); i++)
	{
		m_diag[i] = -FLT_MAX;
	}
}

bool fwkDOP18::IsEmpty() const
{
	if (px + nx < 0.0f) return true;
	if (py + ny < 0.0f) return true;
	if (pz + nz < 0.0f) return true;

	return false;
}

void fwkDOP18::AddPoints(const Vec3V* points, int pointCount)
{
	for (int i = 0; i < pointCount; i++)
	{
		AddPoint(points[i]);
	}
}

void fwkDOP18::AddPoint(Vec3V_In p)
{
	const float x = p.GetXf();
	const float y = p.GetYf();
	const float z = p.GetZf();

	// faces
	px = Max<float>(px, +x);
	nx = Max<float>(nx, -x);
	py = Max<float>(py, +y);
	ny = Max<float>(ny, -y);
	pz = Max<float>(pz, +z);
	nz = Max<float>(nz, -z);

	// edges
	pxpy = Max<float>(pxpy, +x+y);
	nxpy = Max<float>(nxpy, -x+y);
	pxny = Max<float>(pxny, +x-y);
	nxny = Max<float>(nxny, -x-y);
	pypz = Max<float>(pypz, +y+z);
	nypz = Max<float>(nypz, -y+z);
	pynz = Max<float>(pynz, +y-z);
	nynz = Max<float>(nynz, -y-z);
	pzpx = Max<float>(pzpx, +z+x);
	nzpx = Max<float>(nzpx, -z+x);
	pznx = Max<float>(pznx, +z-x);
	nznx = Max<float>(nznx, -z-x);

	// corners
	//pxpypz = Max<float>(pxpypz, +x+y+z);
	//nxpypz = Max<float>(nxpypz, -x+y+z);
	//pxnypz = Max<float>(pxnypz, +x-y+z);
	//nxnypz = Max<float>(nxnypz, -x-y+z);
	//pxpynz = Max<float>(pxpynz, +x+y-z);
	//nxpynz = Max<float>(nxpynz, -x+y-z);
	//pxnynz = Max<float>(pxnynz, +x-y-z);
	//nxnynz = Max<float>(nxnynz, -x-y-z);
}

#if __BANK && !__SPU

int fwkDOP18::CreateFromDrawable(const fwEntity* pEntity, const rmcDrawable* pDrawable, const spdAABB* box)
{
	SetEmpty();

	int numPoints = 0;

	const rmcLodGroup& group = pDrawable->GetLodGroup();

	if (group.ContainsLod(LOD_HIGH))
	{
		const Mat34V matrix      = pEntity->GetMatrix();
		const grmModel& model    = group.GetLodModel0(LOD_HIGH);
		const int modelGeomCount = model.GetGeometryCount();

		for (int geomIndex = 0; geomIndex < modelGeomCount; geomIndex++)
		{
			grmGeometry& geom = model.GetGeometry(geomIndex);
#if __PS3
			if (geom.GetType() == grmGeometry::GEOMETRYEDGE)
			{
				grmGeometryEdge *geomEdge = reinterpret_cast<grmGeometryEdge*>(&geom);

#if HACK_GTA4_MODELINFOIDX_ON_SPU && USE_EDGE
				CGta4DbgSpuInfoStruct gtaSpuInfoStruct;
				gtaSpuInfoStruct.gta4RenderPhaseID = 0x02; // called by Object
				gtaSpuInfoStruct.gta4ModelInfoIdx  = 0;
				gtaSpuInfoStruct.gta4ModelInfoType = 0;
#endif // HACK_GTA4_MODELINFOIDX_ON_SPU && USE_EDGE

				const int KDOP_TEST_EXTRACT_MAX_INDICES  = 20*1024*3;
				const int KDOP_TEST_EXTRACT_MAX_VERTICES = 20*1024;

				// check up front how many verts are in processed geometry and assert if too many
				int totalI = 0;
				int totalV = 0;

				for (int i = 0; i < geomEdge->GetEdgeGeomPpuConfigInfoCount(); i++)
				{
					totalI += geomEdge->GetEdgeGeomPpuConfigInfos()[i].spuConfigInfo.numIndexes;
					totalV += geomEdge->GetEdgeGeomPpuConfigInfos()[i].spuConfigInfo.numVertexes;
				}

				if (totalI > KDOP_TEST_EXTRACT_MAX_INDICES)
				{
					Assertf(0, "%s: index buffer has more indices (%d) than system can handle (%d)", pEntity->GetModelName(), totalI, KDOP_TEST_EXTRACT_MAX_INDICES);
					return 0;
				}

				if (totalV > KDOP_TEST_EXTRACT_MAX_VERTICES)
				{
					Assertf(0, "%s: vertex buffer has more verts (%d) than system can handle (%d)", pEntity->GetModelName(), totalV, KDOP_TEST_EXTRACT_MAX_VERTICES);
					return 0;
				}

				static Vec4V* extractVertStreams[CExtractGeomParams::obvIdxMax] ;
				static Vec4V* extractVerts = NULL;
				static u16*   extractIndices = NULL;

				// allocate
				{
					USE_DEBUG_MEMORY();

					if (extractVerts   == NULL) { extractVerts   = rage_aligned_new(16) Vec4V[KDOP_TEST_EXTRACT_MAX_VERTICES*1]; } // pos-only
					if (extractIndices == NULL) { extractIndices = rage_aligned_new(16) u16[KDOP_TEST_EXTRACT_MAX_INDICES]; }
				}

				memset(&extractVertStreams[0], 0, sizeof(extractVertStreams));

				int numVerts = 0;

				geomEdge->GetVertexAndIndex(
					(Vector4*)extractVerts,
					KDOP_TEST_EXTRACT_MAX_VERTICES,
					(Vector4**)extractVertStreams,
					extractIndices,
					KDOP_TEST_EXTRACT_MAX_INDICES,
					NULL,//BoneIndexesAndWeights,
					0,//sizeof(BoneIndexesAndWeights),
					NULL,//&BoneIndexOffset,
					NULL,//&BoneIndexStride,
					NULL,//&BoneOffset1,
					NULL,//&BoneOffset2,
					NULL,//&BoneOffsetPoint,
					(u32*)&numVerts,
#if HACK_GTA4_MODELINFOIDX_ON_SPU
					&gtaSpuInfoStruct,
#endif // HACK_GTA4_MODELINFOIDX_ON_SPU
					NULL,
					CExtractGeomParams::extractPos
				);

				for (int i = 0; i < numVerts; i++)
				{
					AddPoint(Transform(matrix, extractVerts[i].GetXYZ()));
					numPoints++;
				}
			}
			else
#endif // __PS3
			{
				grcVertexBuffer* vb = geom.GetVertexBuffer(true);

				Assert(vb);

				PS3_ONLY(++g_AllowVertexBufferVramLocks); // PS3: attempting to edit VBs in VRAM
				grcVertexBufferEditor vertexBufferEditor(vb, true, true); // lock=true, readOnly=true

				for (int i = 0; i < vb->GetVertexCount(); i++)
				{
					AddPoint(Transform(matrix, VECTOR3_TO_VEC3V(vertexBufferEditor.GetPosition(i))));
					numPoints++;
				}

				PS3_ONLY(--g_AllowVertexBufferVramLocks); // PS3: finished with editing VBs in VRAM
			}
		}
	}

	if (numPoints > 0 && box)
	{
		// since the compressed entity desc's aabb may be slightly different than the
		// bounds computed for the kDOP, we need to force the kDOP bounds to match it
		px = +box->GetMax().GetXf();
		py = +box->GetMax().GetYf();
		pz = +box->GetMax().GetZf();
		nx = -box->GetMin().GetXf();
		ny = -box->GetMin().GetYf();
		nz = -box->GetMin().GetZf();

		pxpy = Min<float>(px + py, pxpy);
		nxpy = Min<float>(nx + py, nxpy);
		pxny = Min<float>(px + ny, pxny);
		nxny = Min<float>(nx + ny, nxny);

		pypz = Min<float>(py + pz, pypz);
		nypz = Min<float>(ny + pz, nypz);
		pynz = Min<float>(py + nz, pynz);
		nynz = Min<float>(ny + nz, nynz);

		pzpx = Min<float>(pz + px, pzpx);
		nzpx = Min<float>(nz + px, nzpx);
		pznx = Min<float>(pz + nx, pznx);
		nznx = Min<float>(nz + nx, nznx);
	}

	return numPoints;
}

const fwkDOP18* fwkDOP18::GetCachedFromDrawable(const fwEntity* pEntity, const rmcDrawable* pDrawable)
{
	static atMap<u32,fwkDOP18> s_entity_kDOPs;

	const u32 modelIndex = pEntity->GetModelIndex();
	Mat34V matrix = pEntity->GetMatrix();

	// clear the w components to zero so they don't affect the hash
	matrix.GetCol0Ref().SetW(ScalarV(V_ZERO));
	matrix.GetCol1Ref().SetW(ScalarV(V_ZERO));
	matrix.GetCol2Ref().SetW(ScalarV(V_ZERO));
	matrix.GetCol3Ref().SetW(ScalarV(V_ZERO));

	u32 hash = 0;
	hash = atDataHash((const char*)&modelIndex, sizeof(modelIndex), hash);
	hash = atDataHash((const char*)&matrix, sizeof(matrix), hash);

	const fwkDOP18* pkDOP = s_entity_kDOPs.Access(hash);

	if (pkDOP == NULL)
	{
		fwkDOP18 kDOP;
		const int numPoints = kDOP.CreateFromDrawable(pEntity, pDrawable);

		if (numPoints > 0)
		{
			s_entity_kDOPs[hash] = kDOP;
			pkDOP = s_entity_kDOPs.Access(hash);
		}
	}

	return pkDOP;
}

#endif // __BANK && !__SPU

static u8 fwkDOP18_Quantise(float x, float x0, float x1)
{
	return (u8)(0.5f + 255.0f*Clamp<float>((x - x0)/(x1 - x0), 0.0f, 1.0f));
}

void fwkDOP18::QuantiseDiagonals(u8 diag[12]) const
{
	diag[ 0] = fwkDOP18_Quantise(pxpy, -(nx + ny), px + py);
	diag[ 1] = fwkDOP18_Quantise(nxpy, -(px + ny), nx + py);
	diag[ 2] = fwkDOP18_Quantise(pxny, -(nx + py), px + ny);
	diag[ 3] = fwkDOP18_Quantise(nxny, -(px + py), nx + ny);
	diag[ 4] = fwkDOP18_Quantise(pypz, -(ny + nz), py + pz);
	diag[ 5] = fwkDOP18_Quantise(nypz, -(py + nz), ny + pz);
	diag[ 6] = fwkDOP18_Quantise(pynz, -(ny + pz), py + nz);
	diag[ 7] = fwkDOP18_Quantise(nynz, -(py + pz), ny + nz);
	diag[ 8] = fwkDOP18_Quantise(pzpx, -(nz + nx), pz + px);
	diag[ 9] = fwkDOP18_Quantise(nzpx, -(pz + nx), nz + px);
	diag[10] = fwkDOP18_Quantise(pznx, -(nz + px), pz + nx);
	diag[11] = fwkDOP18_Quantise(nznx, -(pz + px), nz + nx);
}

static float fwkDOP18_UnQuantise(u8 x, float x0, float x1)
{
	return x0 + (x1 - x0)*(float)x/255.0f;
}

void fwkDOP18::UnQuantiseDiagonals(const u8 diag[12])
{
	pxpy = fwkDOP18_UnQuantise(diag[ 0], -(nx + ny), px + py);
	nxpy = fwkDOP18_UnQuantise(diag[ 1], -(px + ny), nx + py);
	pxny = fwkDOP18_UnQuantise(diag[ 2], -(nx + py), px + ny);
	nxny = fwkDOP18_UnQuantise(diag[ 3], -(px + py), nx + ny);
	pypz = fwkDOP18_UnQuantise(diag[ 4], -(ny + nz), py + pz);
	nypz = fwkDOP18_UnQuantise(diag[ 5], -(py + nz), ny + pz);
	pynz = fwkDOP18_UnQuantise(diag[ 6], -(ny + pz), py + nz);
	nynz = fwkDOP18_UnQuantise(diag[ 7], -(py + pz), ny + nz);
	pzpx = fwkDOP18_UnQuantise(diag[ 8], -(nz + nx), pz + px);
	nzpx = fwkDOP18_UnQuantise(diag[ 9], -(pz + nx), nz + px);
	pznx = fwkDOP18_UnQuantise(diag[10], -(nz + px), pz + nx);
	nznx = fwkDOP18_UnQuantise(diag[11], -(pz + px), nz + nx);
}

#define xyz_n0_x xyz_p0_x
#define xyz_n0_y xyz_p0_y
#define yzx_n0_y yzx_p0_y
#define yzx_n0_z yzx_p0_z
#define zxy_n0_z zxy_p0_z
#define zxy_n0_x zxy_p0_x
#define yxz_n0_y yxz_p0_y
#define yxz_n0_x yxz_p0_x
#define zyx_n0_z zyx_p0_z
#define zyx_n0_y zyx_p0_y
#define xzy_n0_x xzy_p0_x
#define xzy_n0_z xzy_p0_z

// ==========================================================================================
// This function computes all possible 96 vertices without branching, stored as 24 bundles
// of 4 transposed points (SoA format). Note that a kDOP18 cannot actually have 96 unique
// vertices - the most I've seen is 64 which is probably the limit (but I don't have proof).
// It might be possible to reduce this in some intelligent way but I haven't figured out how.
// ==========================================================================================
void fwkDOP18::BuildVerticesSoA(fwkDOP18VerticesSoA& s) const
{
	const Vec4V pnpn = MergeXY(Vec4V(V_ONE), Vec4V(V_NEGONE)); // {+1,-1,+1,-1}
	const Vec4V ppnn = Vec4V  (Vec2V(V_ONE), Vec2V(V_NEGONE)); // {+1,+1,-1,-1}

	// unpack axis
	const Vec4V x_pnpn(px, nx, px, nx);
	const Vec4V y_pnpn(py, ny, py, ny);
	const Vec4V z_pnpn(pz, nz, pz, nz);

	const Vec4V x_ppnn = MergeXY(x_pnpn, x_pnpn);
	const Vec4V y_ppnn = MergeXY(y_pnpn, y_pnpn);
	const Vec4V z_ppnn = MergeXY(z_pnpn, z_pnpn);

	const Vec4V x_pppp = Vec4V(SplatX(x_pnpn));
	const Vec4V y_pppp = Vec4V(SplatX(y_pnpn));
	const Vec4V z_pppp = Vec4V(SplatX(z_pnpn));

	const Vec4V x_nnnn = Vec4V(SplatY(x_pnpn));
	const Vec4V y_nnnn = Vec4V(SplatY(y_pnpn));
	const Vec4V z_nnnn = Vec4V(SplatY(z_pnpn));

	// unpack diag
	const Vec4V xy(pxpy, nxpy, pxny, nxny);
	const Vec4V yz(pypz, nypz, pynz, nynz);
	const Vec4V zx(pzpx, nzpx, pznx, nznx);

	const Vec4V xy_x = xy - x_pnpn;
	const Vec4V yz_y = yz - y_pnpn;
	const Vec4V zx_z = zx - z_pnpn;

	const Vec4V xy_y = xy - y_ppnn;
	const Vec4V yz_z = yz - z_ppnn;
	const Vec4V zx_x = zx - x_ppnn;

#define _xxyy Get<Vec::X,Vec::X,Vec::Y,Vec::Y>()
#define _zzww Get<Vec::Z,Vec::Z,Vec::W,Vec::W>()
#define _xzxz Get<Vec::X,Vec::Z,Vec::X,Vec::Z>()
#define _ywyw Get<Vec::Y,Vec::W,Vec::Y,Vec::W>()

	s.xyz_p0_x = x_pnpn*pnpn; s.xyz_p0_y = xy_x*ppnn; s.xyz_p0_z = Min(zx_x._xzxz, yz._xxyy - xy_x); s.xyz_n0_z = -Min(zx_x._ywyw, yz._zzww - xy_x);
	s.yzx_p0_y = y_pnpn*pnpn; s.yzx_p0_z = yz_y*ppnn; s.yzx_p0_x = Min(xy_y._xzxz, zx._xxyy - yz_y); s.yzx_n0_x = -Min(xy_y._ywyw, zx._zzww - yz_y);
	s.zxy_p0_z = z_pnpn*pnpn; s.zxy_p0_x = zx_z*ppnn; s.zxy_p0_y = Min(yz_z._xzxz, xy._xxyy - zx_z); s.zxy_n0_y = -Min(yz_z._ywyw, xy._zzww - zx_z);

	s.yxz_p0_y = y_ppnn*ppnn; s.yxz_p0_x = xy_y*pnpn; s.yxz_p0_z = Min(yz_y._xxyy, zx._xzxz - xy_y); s.yxz_n0_z = -Min(yz_y._zzww, zx._ywyw - xy_y);
	s.zyx_p0_z = z_ppnn*ppnn; s.zyx_p0_y = yz_z*pnpn; s.zyx_p0_x = Min(zx_z._xxyy, xy._xzxz - yz_z); s.zyx_n0_x = -Min(zx_z._zzww, xy._ywyw - yz_z);
	s.xzy_p0_x = x_ppnn*ppnn; s.xzy_p0_z = zx_x*pnpn; s.xzy_p0_y = Min(xy_x._xxyy, yz._xzxz - zx_x); s.xzy_n0_y = -Min(xy_x._zzww, yz._ywyw - zx_x);

	const Vec4V xyz_p = Min(y_ppnn - xy_x, z_pppp - s.xyz_p0_z, (yz._xxyy - xy_x - s.xyz_p0_z)*ScalarV(V_HALF));
	const Vec4V yzx_p = Min(z_ppnn - yz_y, x_pppp - s.yzx_p0_x, (zx._xxyy - yz_y - s.yzx_p0_x)*ScalarV(V_HALF));
	const Vec4V zxy_p = Min(x_ppnn - zx_z, y_pppp - s.zxy_p0_y, (xy._xxyy - zx_z - s.zxy_p0_y)*ScalarV(V_HALF));

	const Vec4V xyz_n = Min(y_ppnn - xy_x, z_nnnn + s.xyz_n0_z, (yz._zzww - xy_x + s.xyz_n0_z)*ScalarV(V_HALF));
	const Vec4V yzx_n = Min(z_ppnn - yz_y, x_nnnn + s.yzx_n0_x, (zx._zzww - yz_y + s.yzx_n0_x)*ScalarV(V_HALF));
	const Vec4V zxy_n = Min(x_ppnn - zx_z, y_nnnn + s.zxy_n0_y, (xy._zzww - zx_z + s.zxy_n0_y)*ScalarV(V_HALF));

	const Vec4V yxz_p = Min(x_pnpn - xy_y, z_pppp - s.yxz_p0_z, (zx._xzxz - xy_y - s.yxz_p0_z)*ScalarV(V_HALF));
	const Vec4V zyx_p = Min(y_pnpn - yz_z, x_pppp - s.zyx_p0_x, (xy._xzxz - yz_z - s.zyx_p0_x)*ScalarV(V_HALF));
	const Vec4V xzy_p = Min(z_pnpn - zx_x, y_pppp - s.xzy_p0_y, (yz._xzxz - zx_x - s.xzy_p0_y)*ScalarV(V_HALF));

	const Vec4V yxz_n = Min(x_pnpn - xy_y, z_nnnn + s.yxz_n0_z, (zx._ywyw - xy_y + s.yxz_n0_z)*ScalarV(V_HALF));
	const Vec4V zyx_n = Min(y_pnpn - yz_z, x_nnnn + s.zyx_n0_x, (xy._ywyw - yz_z + s.zyx_n0_x)*ScalarV(V_HALF));
	const Vec4V xzy_n = Min(z_pnpn - zx_x, y_nnnn + s.xzy_n0_y, (yz._ywyw - zx_x + s.xzy_n0_y)*ScalarV(V_HALF));

#undef _xxyy
#undef _zzww
#undef _xzxz
#undef _ywyw

	s.xyz_p1_x = s.xyz_p0_x - xyz_p*pnpn; s.xyz_p1_y = s.xyz_p0_y + xyz_p*ppnn; s.xyz_p1_z = s.xyz_p0_z + xyz_p;
	s.yzx_p1_y = s.yzx_p0_y - yzx_p*pnpn; s.yzx_p1_z = s.yzx_p0_z + yzx_p*ppnn; s.yzx_p1_x = s.yzx_p0_x + yzx_p;
	s.zxy_p1_z = s.zxy_p0_z - zxy_p*pnpn; s.zxy_p1_x = s.zxy_p0_x + zxy_p*ppnn; s.zxy_p1_y = s.zxy_p0_y + zxy_p;

	s.xyz_n1_x = s.xyz_n0_x - xyz_n*pnpn; s.xyz_n1_y = s.xyz_n0_y + xyz_n*ppnn; s.xyz_n1_z = s.xyz_n0_z - xyz_n;
	s.yzx_n1_y = s.yzx_n0_y - yzx_n*pnpn; s.yzx_n1_z = s.yzx_n0_z + yzx_n*ppnn; s.yzx_n1_x = s.yzx_n0_x - yzx_n;
	s.zxy_n1_z = s.zxy_n0_z - zxy_n*pnpn; s.zxy_n1_x = s.zxy_n0_x + zxy_n*ppnn; s.zxy_n1_y = s.zxy_n0_y - zxy_n;

	s.yxz_p1_y = s.yxz_p0_y - yxz_p*ppnn; s.yxz_p1_x = s.yxz_p0_x + yxz_p*pnpn; s.yxz_p1_z = s.yxz_p0_z + yxz_p;
	s.zyx_p1_z = s.zyx_p0_z - zyx_p*ppnn; s.zyx_p1_y = s.zyx_p0_y + zyx_p*pnpn; s.zyx_p1_x = s.zyx_p0_x + zyx_p;
	s.xzy_p1_x = s.xzy_p0_x - xzy_p*ppnn; s.xzy_p1_z = s.xzy_p0_z + xzy_p*pnpn; s.xzy_p1_y = s.xzy_p0_y + xzy_p;

	s.yxz_n1_y = s.yxz_n0_y - yxz_n*ppnn; s.yxz_n1_x = s.yxz_n0_x + yxz_n*pnpn; s.yxz_n1_z = s.yxz_n0_z - yxz_n;
	s.zyx_n1_z = s.zyx_n0_z - zyx_n*ppnn; s.zyx_n1_y = s.zyx_n0_y + zyx_n*pnpn; s.zyx_n1_x = s.zyx_n0_x - zyx_n;
	s.xzy_n1_x = s.xzy_n0_x - xzy_n*ppnn; s.xzy_n1_z = s.xzy_n0_z + xzy_n*pnpn; s.xzy_n1_y = s.xzy_n0_y - xzy_n;
}

static void fwkDOP18_Transpose(fwkDOP18Vertices& v, const fwkDOP18VerticesSoA& s)
{
	// xyz0
	Transpose4x3to3x4(v.pxpypz_0, v.nxpypz_0, v.pxnypz_0, v.nxnypz_0, s.xyz_p0_x, s.xyz_p0_y, s.xyz_p0_z);
	Transpose4x3to3x4(v.pxpynz_0, v.nxpynz_0, v.pxnynz_0, v.nxnynz_0, s.xyz_n0_x, s.xyz_n0_y, s.xyz_n0_z);
	// yzx0
	Transpose4x3to3x4(v.pypzpx_0, v.nypzpx_0, v.pynzpx_0, v.nynzpx_0, s.yzx_p0_x, s.yzx_p0_y, s.yzx_p0_z);
	Transpose4x3to3x4(v.pypznx_0, v.nypznx_0, v.pynznx_0, v.nynznx_0, s.yzx_n0_x, s.yzx_n0_y, s.yzx_n0_z);
	// zxy0
	Transpose4x3to3x4(v.pzpxpy_0, v.nzpxpy_0, v.pznxpy_0, v.nznxpy_0, s.zxy_p0_x, s.zxy_p0_y, s.zxy_p0_z);
	Transpose4x3to3x4(v.pzpxny_0, v.nzpxny_0, v.pznxny_0, v.nznxny_0, s.zxy_n0_x, s.zxy_n0_y, s.zxy_n0_z);
	// yxz0
	Transpose4x3to3x4(v.pypxpz_0, v.pynxpz_0, v.nypxpz_0, v.nynxpz_0, s.yxz_p0_x, s.yxz_p0_y, s.yxz_p0_z);
	Transpose4x3to3x4(v.pypxnz_0, v.pynxnz_0, v.nypxnz_0, v.nynxnz_0, s.yxz_n0_x, s.yxz_n0_y, s.yxz_n0_z);
	// zyx0
	Transpose4x3to3x4(v.pzpypx_0, v.pznypx_0, v.nzpypx_0, v.nznypx_0, s.zyx_p0_x, s.zyx_p0_y, s.zyx_p0_z);
	Transpose4x3to3x4(v.pzpynx_0, v.pznynx_0, v.nzpynx_0, v.nznynx_0, s.zyx_n0_x, s.zyx_n0_y, s.zyx_n0_z);
	// xzy0
	Transpose4x3to3x4(v.pxpzpy_0, v.pxnzpy_0, v.nxpzpy_0, v.nxnzpy_0, s.xzy_p0_x, s.xzy_p0_y, s.xzy_p0_z);
	Transpose4x3to3x4(v.pxpzny_0, v.pxnzny_0, v.nxpzny_0, v.nxnzny_0, s.xzy_n0_x, s.xzy_n0_y, s.xzy_n0_z);
	// xyz1
	Transpose4x3to3x4(v.pxpypz_1, v.nxpypz_1, v.pxnypz_1, v.nxnypz_1, s.xyz_p1_x, s.xyz_p1_y, s.xyz_p1_z);
	Transpose4x3to3x4(v.pxpynz_1, v.nxpynz_1, v.pxnynz_1, v.nxnynz_1, s.xyz_n1_x, s.xyz_n1_y, s.xyz_n1_z);
	// yzx1
	Transpose4x3to3x4(v.pypzpx_1, v.nypzpx_1, v.pynzpx_1, v.nynzpx_1, s.yzx_p1_x, s.yzx_p1_y, s.yzx_p1_z);
	Transpose4x3to3x4(v.pypznx_1, v.nypznx_1, v.pynznx_1, v.nynznx_1, s.yzx_n1_x, s.yzx_n1_y, s.yzx_n1_z);
	// zxy1
	Transpose4x3to3x4(v.pzpxpy_1, v.nzpxpy_1, v.pznxpy_1, v.nznxpy_1, s.zxy_p1_x, s.zxy_p1_y, s.zxy_p1_z);
	Transpose4x3to3x4(v.pzpxny_1, v.nzpxny_1, v.pznxny_1, v.nznxny_1, s.zxy_n1_x, s.zxy_n1_y, s.zxy_n1_z);
	// yxz1
	Transpose4x3to3x4(v.pypxpz_1, v.pynxpz_1, v.nypxpz_1, v.nynxpz_1, s.yxz_p1_x, s.yxz_p1_y, s.yxz_p1_z);
	Transpose4x3to3x4(v.pypxnz_1, v.pynxnz_1, v.nypxnz_1, v.nynxnz_1, s.yxz_n1_x, s.yxz_n1_y, s.yxz_n1_z);
	// zyx1
	Transpose4x3to3x4(v.pzpypx_1, v.pznypx_1, v.nzpypx_1, v.nznypx_1, s.zyx_p1_x, s.zyx_p1_y, s.zyx_p1_z);
	Transpose4x3to3x4(v.pzpynx_1, v.pznynx_1, v.nzpynx_1, v.nznynx_1, s.zyx_n1_x, s.zyx_n1_y, s.zyx_n1_z);
	// xzy1
	Transpose4x3to3x4(v.pxpzpy_1, v.pxnzpy_1, v.nxpzpy_1, v.nxnzpy_1, s.xzy_p1_x, s.xzy_p1_y, s.xzy_p1_z);
	Transpose4x3to3x4(v.pxpzny_1, v.pxnzny_1, v.nxpzny_1, v.nxnzny_1, s.xzy_n1_x, s.xzy_n1_y, s.xzy_n1_z);
}

void fwkDOP18::BuildVertices(Vec3V verts[MAX_VERTS]) const
{
	fwkDOP18VerticesSoA s;
	BuildVerticesSoA(s);
	fwkDOP18_Transpose(*reinterpret_cast<fwkDOP18Vertices*>(verts), s);

#if 1 && __DEV // test using scalar code - please keep this around it might be useful

	fwkDOP18Vertices test;
	memset(&test, 0, sizeof(test));

	// xy
	const float pxpy_px = pxpy - px;
	const float nxpy_nx = nxpy - nx;
	const float pxny_px = pxny - px;
	const float nxny_nx = nxny - nx;

	const float pxpy_py = pxpy - py;
	const float nxpy_py = nxpy - py;
	const float pxny_ny = pxny - ny;
	const float nxny_ny = nxny - ny;

	// yz
	const float pypz_py = pypz - py;
	const float nypz_ny = nypz - ny;
	const float pynz_py = pynz - py;
	const float nynz_ny = nynz - ny;

	const float pypz_pz = pypz - pz;
	const float nypz_pz = nypz - pz;
	const float pynz_nz = pynz - nz;
	const float nynz_nz = nynz - nz;

	// zx
	const float pzpx_pz = pzpx - pz;
	const float nzpx_nz = nzpx - nz;
	const float pznx_pz = pznx - pz;
	const float nznx_nz = nznx - nz;

	const float pzpx_px = pzpx - px;
	const float nzpx_px = nzpx - px;
	const float pznx_nx = pznx - nx;
	const float nznx_nx = nznx - nx;

	// xyz
	const float px_py_pz_z = Min<float>(pzpx_px, pypz - pxpy_px); test.pxpypz_0 = Vec3V(+px, +pxpy_px, +px_py_pz_z);
	const float nx_py_pz_z = Min<float>(pznx_nx, pypz - nxpy_nx); test.nxpypz_0 = Vec3V(-nx, +nxpy_nx, +nx_py_pz_z);
	const float px_ny_pz_z = Min<float>(pzpx_px, nypz - pxny_px); test.pxnypz_0 = Vec3V(+px, -pxny_px, +px_ny_pz_z);
	const float nx_ny_pz_z = Min<float>(pznx_nx, nypz - nxny_nx); test.nxnypz_0 = Vec3V(-nx, -nxny_nx, +nx_ny_pz_z);

	const float px_py_nz_z = Min<float>(nzpx_px, pynz - pxpy_px); test.pxpynz_0 = Vec3V(+px, +pxpy_px, -px_py_nz_z);
	const float nx_py_nz_z = Min<float>(nznx_nx, pynz - nxpy_nx); test.nxpynz_0 = Vec3V(-nx, +nxpy_nx, -nx_py_nz_z);
	const float px_ny_nz_z = Min<float>(nzpx_px, nynz - pxny_px); test.pxnynz_0 = Vec3V(+px, -pxny_px, -px_ny_nz_z);
	const float nx_ny_nz_z = Min<float>(nznx_nx, nynz - nxny_nx); test.nxnynz_0 = Vec3V(-nx, -nxny_nx, -nx_ny_nz_z);

	// yzx            
	const float py_pz_px_x = Min<float>(pxpy_py, pzpx - pypz_py); test.pypzpx_0 = Vec3V(+py_pz_px_x, +py, +pypz_py);
	const float ny_pz_px_x = Min<float>(pxny_ny, pzpx - nypz_ny); test.nypzpx_0 = Vec3V(+ny_pz_px_x, -ny, +nypz_ny);
	const float py_nz_px_x = Min<float>(pxpy_py, nzpx - pynz_py); test.pynzpx_0 = Vec3V(+py_nz_px_x, +py, -pynz_py);
	const float ny_nz_px_x = Min<float>(pxny_ny, nzpx - nynz_ny); test.nynzpx_0 = Vec3V(+ny_nz_px_x, -ny, -nynz_ny);

	const float py_pz_nx_x = Min<float>(nxpy_py, pznx - pypz_py); test.pypznx_0 = Vec3V(-py_pz_nx_x, +py, +pypz_py);
	const float ny_pz_nx_x = Min<float>(nxny_ny, pznx - nypz_ny); test.nypznx_0 = Vec3V(-ny_pz_nx_x, -ny, +nypz_ny);
	const float py_nz_nx_x = Min<float>(nxpy_py, nznx - pynz_py); test.pynznx_0 = Vec3V(-py_nz_nx_x, +py, -pynz_py);
	const float ny_nz_nx_x = Min<float>(nxny_ny, nznx - nynz_ny); test.nynznx_0 = Vec3V(-ny_nz_nx_x, -ny, -nynz_ny);

	// zxy            
	const float pz_px_py_y = Min<float>(pypz_pz, pxpy - pzpx_pz); test.pzpxpy_0 = Vec3V(+pzpx_pz, +pz_px_py_y, +pz);
	const float nz_px_py_y = Min<float>(pynz_nz, pxpy - nzpx_nz); test.nzpxpy_0 = Vec3V(+nzpx_nz, +nz_px_py_y, -nz);
	const float pz_nx_py_y = Min<float>(pypz_pz, nxpy - pznx_pz); test.pznxpy_0 = Vec3V(-pznx_pz, +pz_nx_py_y, +pz);
	const float nz_nx_py_y = Min<float>(pynz_nz, nxpy - nznx_nz); test.nznxpy_0 = Vec3V(-nznx_nz, +nz_nx_py_y, -nz);

	const float pz_px_ny_y = Min<float>(nypz_pz, pxny - pzpx_pz); test.pzpxny_0 = Vec3V(+pzpx_pz, -pz_px_ny_y, +pz);
	const float nz_px_ny_y = Min<float>(nynz_nz, pxny - nzpx_nz); test.nzpxny_0 = Vec3V(+nzpx_nz, -nz_px_ny_y, -nz);
	const float pz_nx_ny_y = Min<float>(nypz_pz, nxny - pznx_pz); test.pznxny_0 = Vec3V(-pznx_pz, -pz_nx_ny_y, +pz);
	const float nz_nx_ny_y = Min<float>(nynz_nz, nxny - nznx_nz); test.nznxny_0 = Vec3V(-nznx_nz, -nz_nx_ny_y, -nz);

	// yxz            
	const float py_px_pz_z = Min<float>(pypz_py, pzpx - pxpy_py); test.pypxpz_0 = Vec3V(+pxpy_py, +py, +py_px_pz_z);
	const float py_nx_pz_z = Min<float>(pypz_py, pznx - nxpy_py); test.pynxpz_0 = Vec3V(-nxpy_py, +py, +py_nx_pz_z);
	const float ny_px_pz_z = Min<float>(nypz_ny, pzpx - pxny_ny); test.nypxpz_0 = Vec3V(+pxny_ny, -ny, +ny_px_pz_z);
	const float ny_nx_pz_z = Min<float>(nypz_ny, pznx - nxny_ny); test.nynxpz_0 = Vec3V(-nxny_ny, -ny, +ny_nx_pz_z);

	const float py_px_nz_z = Min<float>(pynz_py, nzpx - pxpy_py); test.pypxnz_0 = Vec3V(+pxpy_py, +py, -py_px_nz_z);
	const float py_nx_nz_z = Min<float>(pynz_py, nznx - nxpy_py); test.pynxnz_0 = Vec3V(-nxpy_py, +py, -py_nx_nz_z);
	const float ny_px_nz_z = Min<float>(nynz_ny, nzpx - pxny_ny); test.nypxnz_0 = Vec3V(+pxny_ny, -ny, -ny_px_nz_z);
	const float ny_nx_nz_z = Min<float>(nynz_ny, nznx - nxny_ny); test.nynxnz_0 = Vec3V(-nxny_ny, -ny, -ny_nx_nz_z);

	// zyx            
	const float pz_py_px_x = Min<float>(pzpx_pz, pxpy - pypz_pz); test.pzpypx_0 = Vec3V(+pz_py_px_x, +pypz_pz, +pz);
	const float pz_ny_px_x = Min<float>(pzpx_pz, pxny - nypz_pz); test.pznypx_0 = Vec3V(+pz_ny_px_x, -nypz_pz, +pz);
	const float nz_py_px_x = Min<float>(nzpx_nz, pxpy - pynz_nz); test.nzpypx_0 = Vec3V(+nz_py_px_x, +pynz_nz, -nz);
	const float nz_ny_px_x = Min<float>(nzpx_nz, pxny - nynz_nz); test.nznypx_0 = Vec3V(+nz_ny_px_x, -nynz_nz, -nz);

	const float pz_py_nx_x = Min<float>(pznx_pz, nxpy - pypz_pz); test.pzpynx_0 = Vec3V(-pz_py_nx_x, +pypz_pz, +pz);
	const float pz_ny_nx_x = Min<float>(pznx_pz, nxny - nypz_pz); test.pznynx_0 = Vec3V(-pz_ny_nx_x, -nypz_pz, +pz);
	const float nz_py_nx_x = Min<float>(nznx_nz, nxpy - pynz_nz); test.nzpynx_0 = Vec3V(-nz_py_nx_x, +pynz_nz, -nz);
	const float nz_ny_nx_x = Min<float>(nznx_nz, nxny - nynz_nz); test.nznynx_0 = Vec3V(-nz_ny_nx_x, -nynz_nz, -nz);

	// xzy            
	const float px_pz_py_y = Min<float>(pxpy_px, pypz - pzpx_px); test.pxpzpy_0 = Vec3V(+px, +px_pz_py_y, +pzpx_px);
	const float px_nz_py_y = Min<float>(pxpy_px, pynz - nzpx_px); test.pxnzpy_0 = Vec3V(+px, +px_nz_py_y, -nzpx_px);
	const float nx_pz_py_y = Min<float>(nxpy_nx, pypz - pznx_nx); test.nxpzpy_0 = Vec3V(-nx, +nx_pz_py_y, +pznx_nx);
	const float nx_nz_py_y = Min<float>(nxpy_nx, pynz - nznx_nx); test.nxnzpy_0 = Vec3V(-nx, +nx_nz_py_y, -nznx_nx);

	const float px_pz_ny_y = Min<float>(pxny_px, nypz - pzpx_px); test.pxpzny_0 = Vec3V(+px, -px_pz_ny_y, +pzpx_px);
	const float px_nz_ny_y = Min<float>(pxny_px, nynz - nzpx_px); test.pxnzny_0 = Vec3V(+px, -px_nz_ny_y, -nzpx_px);
	const float nx_pz_ny_y = Min<float>(nxny_nx, nypz - pznx_nx); test.nxpzny_0 = Vec3V(-nx, -nx_pz_ny_y, +pznx_nx);
	const float nx_nz_ny_y = Min<float>(nxny_nx, nynz - nznx_nx); test.nxnzny_0 = Vec3V(-nx, -nx_nz_ny_y, -nznx_nx);

	float d;

	// xyz
	d = Min<float>(py - pxpy_px, pz - px_py_pz_z, (pypz - pxpy_px - px_py_pz_z)/2.0f); test.pxpypz_1 = test.pxpypz_0 + Vec3V(-d,+d,+d);
	d = Min<float>(py - nxpy_nx, pz - nx_py_pz_z, (pypz - nxpy_nx - nx_py_pz_z)/2.0f); test.nxpypz_1 = test.nxpypz_0 + Vec3V(+d,+d,+d);
	d = Min<float>(ny - pxny_px, pz - px_ny_pz_z, (nypz - pxny_px - px_ny_pz_z)/2.0f); test.pxnypz_1 = test.pxnypz_0 + Vec3V(-d,-d,+d);
	d = Min<float>(ny - nxny_nx, pz - nx_ny_pz_z, (nypz - nxny_nx - nx_ny_pz_z)/2.0f); test.nxnypz_1 = test.nxnypz_0 + Vec3V(+d,-d,+d);

	d = Min<float>(py - pxpy_px, nz - px_py_nz_z, (pynz - pxpy_px - px_py_nz_z)/2.0f); test.pxpynz_1 = test.pxpynz_0 + Vec3V(-d,+d,-d);
	d = Min<float>(py - nxpy_nx, nz - nx_py_nz_z, (pynz - nxpy_nx - nx_py_nz_z)/2.0f); test.nxpynz_1 = test.nxpynz_0 + Vec3V(+d,+d,-d);
	d = Min<float>(ny - pxny_px, nz - px_ny_nz_z, (nynz - pxny_px - px_ny_nz_z)/2.0f); test.pxnynz_1 = test.pxnynz_0 + Vec3V(-d,-d,-d);
	d = Min<float>(ny - nxny_nx, nz - nx_ny_nz_z, (nynz - nxny_nx - nx_ny_nz_z)/2.0f); test.nxnynz_1 = test.nxnynz_0 + Vec3V(+d,-d,-d);

	// yzx
	d = Min<float>(pz - pypz_py, px - py_pz_px_x, (pzpx - pypz_py - py_pz_px_x)/2.0f); test.pypzpx_1 = test.pypzpx_0 + Vec3V(+d,-d,+d);
	d = Min<float>(pz - nypz_ny, px - ny_pz_px_x, (pzpx - nypz_ny - ny_pz_px_x)/2.0f); test.nypzpx_1 = test.nypzpx_0 + Vec3V(+d,+d,+d);
	d = Min<float>(nz - pynz_py, px - py_nz_px_x, (nzpx - pynz_py - py_nz_px_x)/2.0f); test.pynzpx_1 = test.pynzpx_0 + Vec3V(+d,-d,-d);
	d = Min<float>(nz - nynz_ny, px - ny_nz_px_x, (nzpx - nynz_ny - ny_nz_px_x)/2.0f); test.nynzpx_1 = test.nynzpx_0 + Vec3V(+d,+d,-d);

	d = Min<float>(pz - pypz_py, nx - py_pz_nx_x, (pznx - pypz_py - py_pz_nx_x)/2.0f); test.pypznx_1 = test.pypznx_0 + Vec3V(-d,-d,+d);
	d = Min<float>(pz - nypz_ny, nx - ny_pz_nx_x, (pznx - nypz_ny - ny_pz_nx_x)/2.0f); test.nypznx_1 = test.nypznx_0 + Vec3V(-d,+d,+d);
	d = Min<float>(nz - pynz_py, nx - py_nz_nx_x, (nznx - pynz_py - py_nz_nx_x)/2.0f); test.pynznx_1 = test.pynznx_0 + Vec3V(-d,-d,-d);
	d = Min<float>(nz - nynz_ny, nx - ny_nz_nx_x, (nznx - nynz_ny - ny_nz_nx_x)/2.0f); test.nynznx_1 = test.nynznx_0 + Vec3V(-d,+d,-d);

	// zxy
	d = Min<float>(px - pzpx_pz, py - pz_px_py_y, (pxpy - pzpx_pz - pz_px_py_y)/2.0f); test.pzpxpy_1 = test.pzpxpy_0 + Vec3V(+d,+d,-d);
	d = Min<float>(px - nzpx_nz, py - nz_px_py_y, (pxpy - nzpx_nz - nz_px_py_y)/2.0f); test.nzpxpy_1 = test.nzpxpy_0 + Vec3V(+d,+d,+d);
	d = Min<float>(nx - pznx_pz, py - pz_nx_py_y, (nxpy - pznx_pz - pz_nx_py_y)/2.0f); test.pznxpy_1 = test.pznxpy_0 + Vec3V(-d,+d,-d);
	d = Min<float>(nx - nznx_nz, py - nz_nx_py_y, (nxpy - nznx_nz - nz_nx_py_y)/2.0f); test.nznxpy_1 = test.nznxpy_0 + Vec3V(-d,+d,+d);

	d = Min<float>(px - pzpx_pz, ny - pz_px_ny_y, (pxny - pzpx_pz - pz_px_ny_y)/2.0f); test.pzpxny_1 = test.pzpxny_0 + Vec3V(+d,-d,-d);
	d = Min<float>(px - nzpx_nz, ny - nz_px_ny_y, (pxny - nzpx_nz - nz_px_ny_y)/2.0f); test.nzpxny_1 = test.nzpxny_0 + Vec3V(+d,-d,+d);
	d = Min<float>(nx - pznx_pz, ny - pz_nx_ny_y, (nxny - pznx_pz - pz_nx_ny_y)/2.0f); test.pznxny_1 = test.pznxny_0 + Vec3V(-d,-d,-d);
	d = Min<float>(nx - nznx_nz, ny - nz_nx_ny_y, (nxny - nznx_nz - nz_nx_ny_y)/2.0f); test.nznxny_1 = test.nznxny_0 + Vec3V(-d,-d,+d);

	// yxz
	d = Min<float>(px - pxpy_py, pz - py_px_pz_z, (pzpx - pxpy_py - py_px_pz_z)/2.0f); test.pypxpz_1 = test.pypxpz_0 + Vec3V(+d,-d,+d);
	d = Min<float>(nx - nxpy_py, pz - py_nx_pz_z, (pznx - nxpy_py - py_nx_pz_z)/2.0f); test.pynxpz_1 = test.pynxpz_0 + Vec3V(-d,-d,+d);
	d = Min<float>(px - pxny_ny, pz - ny_px_pz_z, (pzpx - pxny_ny - ny_px_pz_z)/2.0f); test.nypxpz_1 = test.nypxpz_0 + Vec3V(+d,+d,+d);
	d = Min<float>(nx - nxny_ny, pz - ny_nx_pz_z, (pznx - nxny_ny - ny_nx_pz_z)/2.0f); test.nynxpz_1 = test.nynxpz_0 + Vec3V(-d,+d,+d);

	d = Min<float>(px - pxpy_py, nz - py_px_nz_z, (nzpx - pxpy_py - py_px_nz_z)/2.0f); test.pypxnz_1 = test.pypxnz_0 + Vec3V(+d,-d,-d);
	d = Min<float>(nx - nxpy_py, nz - py_nx_nz_z, (nznx - nxpy_py - py_nx_nz_z)/2.0f); test.pynxnz_1 = test.pynxnz_0 + Vec3V(-d,-d,-d);
	d = Min<float>(px - pxny_ny, nz - ny_px_nz_z, (nzpx - pxny_ny - ny_px_nz_z)/2.0f); test.nypxnz_1 = test.nypxnz_0 + Vec3V(+d,+d,-d);
	d = Min<float>(nx - nxny_ny, nz - ny_nx_nz_z, (nznx - nxny_ny - ny_nx_nz_z)/2.0f); test.nynxnz_1 = test.nynxnz_0 + Vec3V(-d,+d,-d);

	// zyx
	d = Min<float>(py - pypz_pz, px - pz_py_px_x, (pxpy - pypz_pz - pz_py_px_x)/2.0f); test.pzpypx_1 = test.pzpypx_0 + Vec3V(+d,+d,-d);
	d = Min<float>(ny - nypz_pz, px - pz_ny_px_x, (pxny - nypz_pz - pz_ny_px_x)/2.0f); test.pznypx_1 = test.pznypx_0 + Vec3V(+d,-d,-d);
	d = Min<float>(py - pynz_nz, px - nz_py_px_x, (pxpy - pynz_nz - nz_py_px_x)/2.0f); test.nzpypx_1 = test.nzpypx_0 + Vec3V(+d,+d,+d);
	d = Min<float>(ny - nynz_nz, px - nz_ny_px_x, (pxny - nynz_nz - nz_ny_px_x)/2.0f); test.nznypx_1 = test.nznypx_0 + Vec3V(+d,-d,+d);

	d = Min<float>(py - pypz_pz, nx - pz_py_nx_x, (nxpy - pypz_pz - pz_py_nx_x)/2.0f); test.pzpynx_1 = test.pzpynx_0 + Vec3V(-d,+d,-d);
	d = Min<float>(ny - nypz_pz, nx - pz_ny_nx_x, (nxny - nypz_pz - pz_ny_nx_x)/2.0f); test.pznynx_1 = test.pznynx_0 + Vec3V(-d,-d,-d);
	d = Min<float>(py - pynz_nz, nx - nz_py_nx_x, (nxpy - pynz_nz - nz_py_nx_x)/2.0f); test.nzpynx_1 = test.nzpynx_0 + Vec3V(-d,+d,+d);
	d = Min<float>(ny - nynz_nz, nx - nz_ny_nx_x, (nxny - nynz_nz - nz_ny_nx_x)/2.0f); test.nznynx_1 = test.nznynx_0 + Vec3V(-d,-d,+d);

	// xzy
	d = Min<float>(pz - pzpx_px, py - px_pz_py_y, (pypz - pzpx_px - px_pz_py_y)/2.0f); test.pxpzpy_1 = test.pxpzpy_0 + Vec3V(-d,+d,+d);
	d = Min<float>(nz - nzpx_px, py - px_nz_py_y, (pynz - nzpx_px - px_nz_py_y)/2.0f); test.pxnzpy_1 = test.pxnzpy_0 + Vec3V(-d,+d,-d);
	d = Min<float>(pz - pznx_nx, py - nx_pz_py_y, (pypz - pznx_nx - nx_pz_py_y)/2.0f); test.nxpzpy_1 = test.nxpzpy_0 + Vec3V(+d,+d,+d);
	d = Min<float>(nz - nznx_nx, py - nx_nz_py_y, (pynz - nznx_nx - nx_nz_py_y)/2.0f); test.nxnzpy_1 = test.nxnzpy_0 + Vec3V(+d,+d,-d);

	d = Min<float>(pz - pzpx_px, ny - px_pz_ny_y, (nypz - pzpx_px - px_pz_ny_y)/2.0f); test.pxpzny_1 = test.pxpzny_0 + Vec3V(-d,-d,+d);
	d = Min<float>(nz - nzpx_px, ny - px_nz_ny_y, (nynz - nzpx_px - px_nz_ny_y)/2.0f); test.pxnzny_1 = test.pxnzny_0 + Vec3V(-d,-d,-d);
	d = Min<float>(pz - pznx_nx, ny - nx_pz_ny_y, (nypz - pznx_nx - nx_pz_ny_y)/2.0f); test.nxpzny_1 = test.nxpzny_0 + Vec3V(+d,-d,+d);
	d = Min<float>(nz - nznx_nx, ny - nx_nz_ny_y, (nynz - nznx_nx - nx_nz_ny_y)/2.0f); test.nxnzny_1 = test.nxnzny_0 + Vec3V(+d,-d,-d);

	for (int i = 0; i < MAX_VERTS; i++)
	{
		const float err = MaxElement(Abs(verts[i] - ((const Vec3V*)&test)[i])).Getf();
		Assertf(err <= 0.001f, "kDOP vertex %d err = %f", i, err);
	}

#endif // __DEV
}

static void fwkDOP18_AddFace(fwkDOP18Face faces[fwkDOP18::MAX_FACES], int& faceCount, const fwkDOP18Face& face, float tolerance)
{
	if (face.HasArea(tolerance))
	{
		faces[faceCount++] = face;
	}
}

int fwkDOP18::BuildVerticesAndFaces(Vec3V verts[MAX_VERTS], fwkDOP18Face faces[MAX_FACES], float tolerance) const
{
	int faceCount = 0;

	if (!IsEmpty())
	{
		fwkDOP18Vertices v;
		BuildVertices(reinterpret_cast<Vec3V*>(&v));

		if (verts)
		{
			memcpy(verts, &v, sizeof(v));
		}

		if (faces) // note: the plane normals are intentionally left un-normalised
		{
			#define ADD_FACE(nx,ny,nz,d,p0,p1,p2,p3,p4,p5,p6,p7,flip) \
				fwkDOP18_AddFace(faces, faceCount, fwkDOP18Face( \
					Vec4V((float)nx, (float)ny, (float)nz, d), p0,p1,p2,p3,p4,p5,p6,p7, tolerance, flip), tolerance)

			// x,y,z
			ADD_FACE(+1, 0, 0, px  , v.pxpynz_0,v.pxpypz_0,v.pxpzpy_0,v.pxpzny_0,v.pxnypz_0,v.pxnynz_0,v.pxnzny_0,v.pxnzpy_0, true );
			ADD_FACE(-1, 0, 0, nx  , v.nxpynz_0,v.nxpypz_0,v.nxpzpy_0,v.nxpzny_0,v.nxnypz_0,v.nxnynz_0,v.nxnzny_0,v.nxnzpy_0, false);
			ADD_FACE( 0,+1, 0, py  , v.pypznx_0,v.pypzpx_0,v.pypxpz_0,v.pypxnz_0,v.pynzpx_0,v.pynznx_0,v.pynxnz_0,v.pynxpz_0, true );
			ADD_FACE( 0,-1, 0, ny  , v.nypznx_0,v.nypzpx_0,v.nypxpz_0,v.nypxnz_0,v.nynzpx_0,v.nynznx_0,v.nynxnz_0,v.nynxpz_0, false);
			ADD_FACE( 0, 0,+1, pz  , v.pzpxny_0,v.pzpxpy_0,v.pzpypx_0,v.pzpynx_0,v.pznxpy_0,v.pznxny_0,v.pznynx_0,v.pznypx_0, true );
			ADD_FACE( 0, 0,-1, nz  , v.nzpxny_0,v.nzpxpy_0,v.nzpypx_0,v.nzpynx_0,v.nznxpy_0,v.nznxny_0,v.nznynx_0,v.nznypx_0, false);

			// xy,yz,zx
			ADD_FACE(+1,+1, 0, pxpy, v.pypxnz_0,v.pypxpz_0,v.pypxpz_1,v.pxpypz_1,v.pxpypz_0,v.pxpynz_0,v.pxpynz_1,v.pypxnz_1, true );
			ADD_FACE(-1,+1, 0, nxpy, v.pynxnz_0,v.pynxpz_0,v.pynxpz_1,v.nxpypz_1,v.nxpypz_0,v.nxpynz_0,v.nxpynz_1,v.pynxnz_1, false);
			ADD_FACE(+1,-1, 0, pxny, v.nypxnz_0,v.nypxpz_0,v.nypxpz_1,v.pxnypz_1,v.pxnypz_0,v.pxnynz_0,v.pxnynz_1,v.nypxnz_1, false);
			ADD_FACE(-1,-1, 0, nxny, v.nynxnz_0,v.nynxpz_0,v.nynxpz_1,v.nxnypz_1,v.nxnypz_0,v.nxnynz_0,v.nxnynz_1,v.nynxnz_1, true );
			ADD_FACE( 0,+1,+1, pypz, v.pzpynx_0,v.pzpypx_0,v.pzpypx_1,v.pypzpx_1,v.pypzpx_0,v.pypznx_0,v.pypznx_1,v.pzpynx_1, true );
			ADD_FACE( 0,-1,+1, nypz, v.pznynx_0,v.pznypx_0,v.pznypx_1,v.nypzpx_1,v.nypzpx_0,v.nypznx_0,v.nypznx_1,v.pznynx_1, false);
			ADD_FACE( 0,+1,-1, pynz, v.nzpynx_0,v.nzpypx_0,v.nzpypx_1,v.pynzpx_1,v.pynzpx_0,v.pynznx_0,v.pynznx_1,v.nzpynx_1, false);
			ADD_FACE( 0,-1,-1, nynz, v.nznynx_0,v.nznypx_0,v.nznypx_1,v.nynzpx_1,v.nynzpx_0,v.nynznx_0,v.nynznx_1,v.nznynx_1, true );
			ADD_FACE(+1, 0,+1, pzpx, v.pxpzny_0,v.pxpzpy_0,v.pxpzpy_1,v.pzpxpy_1,v.pzpxpy_0,v.pzpxny_0,v.pzpxny_1,v.pxpzny_1, true );
			ADD_FACE(+1, 0,-1, nzpx, v.pxnzny_0,v.pxnzpy_0,v.pxnzpy_1,v.nzpxpy_1,v.nzpxpy_0,v.nzpxny_0,v.nzpxny_1,v.pxnzny_1, false);
			ADD_FACE(-1, 0,+1, pznx, v.nxpzny_0,v.nxpzpy_0,v.nxpzpy_1,v.pznxpy_1,v.pznxpy_0,v.pznxny_0,v.pznxny_1,v.nxpzny_1, false);
			ADD_FACE(-1, 0,-1, nznx, v.nxnzny_0,v.nxnzpy_0,v.nxnzpy_1,v.nznxpy_1,v.nznxpy_0,v.nznxny_0,v.nznxny_1,v.nxnzny_1, true );

			#undef ADD_FACE
		}
	}

	return faceCount;
}

bool fwkDOP18::IntersectsPoint(Vec3V_In point) const
{
	fwkDOP18 kDOP18;
	kDOP18.AddPoint(point);
	return Intersects(kDOP18);
}

bool fwkDOP18::Intersects(const fwkDOP18& kDOP18) const
{
	if (px + kDOP18.nx < 0.0f ||
		nx + kDOP18.px < 0.0f ||
		py + kDOP18.ny < 0.0f ||
		ny + kDOP18.py < 0.0f ||
		pz + kDOP18.nz < 0.0f ||
		nz + kDOP18.pz < 0.0f)
	{
		return false;
	}

	if (pxpy + kDOP18.nxny < 0.0f ||
		nxpy + kDOP18.pxny < 0.0f ||
		pxny + kDOP18.nxpy < 0.0f ||
		nxny + kDOP18.pxpy < 0.0f ||
		pypz + kDOP18.nynz < 0.0f ||
		nypz + kDOP18.pynz < 0.0f ||
		pynz + kDOP18.nypz < 0.0f ||
		nynz + kDOP18.pypz < 0.0f ||
		pzpx + kDOP18.nznx < 0.0f ||
		nzpx + kDOP18.pznx < 0.0f ||
		pznx + kDOP18.nzpx < 0.0f ||
		nznx + kDOP18.pzpx < 0.0f)
	{                      
		return false;
	}

//	if (pxpypz + kDOP18.nxnynz < 0.0f ||
//		nxpypz + kDOP18.pxnynz < 0.0f ||
//		pxnypz + kDOP18.nxpynz < 0.0f ||
//		nxnypz + kDOP18.pxpynz < 0.0f ||
//		pxpynz + kDOP18.nxnypz < 0.0f ||
//		nxpynz + kDOP18.pxnypz < 0.0f ||
//		pxnynz + kDOP18.nxpypz < 0.0f ||
//		nxnynz + kDOP18.pxpypz < 0.0f)
//	{
//		return false;
//	}

	return true;
}

#if __BANK

// =======================================================================================
// TODO -- these functions are incredibly inefficient and should not be used in production
//         code. however i think it might be possible to implement these efficiently by
//         precomputing a matrix(?) based on the frustum orientation.
//
//         see "Easy Realignment of k-DOP Bounding Volumes"
//         http://www.graphicsinterface.org/proceedings/2003/117/paper117.pdf
// =======================================================================================

bool fwkDOP18::IntersectsFrustum_REFERENCE(const spdTransposedPlaneSet4& sps) const
{
	Vec3V verts[MAX_VERTS];
	BuildVertices(verts);

	Vec4V planes[4];
	sps.GetPlanes(planes);

	for (int i = 0; i < NELEM(planes); i++)
	{
		if (IsEqualAll(planes[i], Vec4V(V_ZERO)))
		{
			continue; // don't bother testing against zero plane
		}

		bool bOutside = true;

		for (int j = 0; j < NELEM(verts); j++)
		{
			if (IsLessThanOrEqualAll(PlaneDistanceTo(planes[i], verts[j]), ScalarV(V_ZERO)))
			{
				bOutside = false;
				break;
			}
		}

		if (bOutside)
		{
			return false;
		}
	}

	return true;
}

bool fwkDOP18::IntersectsFrustum_REFERENCE(const spdTransposedPlaneSet8& sps) const
{
	Vec3V verts[MAX_VERTS];
	BuildVertices(verts);

	Vec4V planes[8];
	sps.GetPlanes(planes);

	for (int i = 0; i < NELEM(planes); i++)
	{
		if (IsEqualAll(planes[i], Vec4V(V_ZERO)))
		{
			continue; // don't bother testing against zero plane
		}

		bool bOutside = true;

		for (int j = 0; j < NELEM(verts); j++)
		{
			if (IsLessThanOrEqualAll(PlaneDistanceTo(planes[i], verts[j]), ScalarV(V_ZERO)))
			{
				bOutside = false;
				break;
			}
		}

		if (bOutside)
		{
			return false;
		}
	}

	return true;
}

#if !__SPU

void fwkDOP18::DebugDrawEdges(const Color32& colour) const
{
	fwkDOP18Face faces[MAX_FACES];
	Vec3V        verts[MAX_VERTS];

	const int numFaces = BuildVerticesAndFaces(verts, faces, 0.0001f);

	for (int i = 0; i < numFaces; i++)
	{
		const fwkDOP18Face& face = faces[i];

		for (int j = 0; j < face.m_pointCount; j++)
		{
			grcDebugDraw::Line(face.m_points[j], face.m_points[(j + 1)%face.m_pointCount], colour);
		}
	}
}

void fwkDOP18::DebugDrawBound(const Color32& colour, const grcViewport& vp, bool bUseOnlyAABBBound) const
{
	if (bUseOnlyAABBBound)
	{
		fwkDOP18 aabb = *this;

		aabb.pxpy = px + py;
		aabb.nxpy = nx + py;
		aabb.pxny = px + ny;
		aabb.nxny = nx + ny;
		aabb.pypz = py + pz;
		aabb.nypz = ny + pz;
		aabb.pynz = py + nz;
		aabb.nynz = ny + nz;
		aabb.pzpx = pz + px;
		aabb.nzpx = nz + px;
		aabb.pznx = pz + nx;
		aabb.nznx = nz + nx;

		aabb.DebugDrawBound(colour, vp, false);
		return;
	}

	rstSplatMat44V splatMtx;
	rstCreateSplatMat44V(vp.GetCompositeMtx(), splatMtx);

	fwkDOP18VerticesSoA s;
	BuildVerticesSoA(s);

	Mat44V groups[24];
	Vec4V* p = (Vec4V*)groups;

	// xyz0
	FullTransform2V4(splatMtx, p, s.xyz_p0_x, s.xyz_p0_y, s.xyz_p0_z); p += 4;
	FullTransform2V4(splatMtx, p, s.xyz_n0_x, s.xyz_n0_y, s.xyz_n0_z); p += 4;
	// yzx0
	FullTransform2V4(splatMtx, p, s.yzx_p0_x, s.yzx_p0_y, s.yzx_p0_z); p += 4;
	FullTransform2V4(splatMtx, p, s.yzx_n0_x, s.yzx_n0_y, s.yzx_n0_z); p += 4;
	// zxy0
	FullTransform2V4(splatMtx, p, s.zxy_p0_x, s.zxy_p0_y, s.zxy_p0_z); p += 4;
	FullTransform2V4(splatMtx, p, s.zxy_n0_x, s.zxy_n0_y, s.zxy_n0_z); p += 4;
	// yxz0
	FullTransform2V4(splatMtx, p, s.yxz_p0_x, s.yxz_p0_y, s.yxz_p0_z); p += 4;
	FullTransform2V4(splatMtx, p, s.yxz_n0_x, s.yxz_n0_y, s.yxz_n0_z); p += 4;
	// zyx0
	FullTransform2V4(splatMtx, p, s.zyx_p0_x, s.zyx_p0_y, s.zyx_p0_z); p += 4;
	FullTransform2V4(splatMtx, p, s.zyx_n0_x, s.zyx_n0_y, s.zyx_n0_z); p += 4;
	// xzy0
	FullTransform2V4(splatMtx, p, s.xzy_p0_x, s.xzy_p0_y, s.xzy_p0_z); p += 4;
	FullTransform2V4(splatMtx, p, s.xzy_n0_x, s.xzy_n0_y, s.xzy_n0_z); p += 4;
	// xyz1
	FullTransform2V4(splatMtx, p, s.xyz_p1_x, s.xyz_p1_y, s.xyz_p1_z); p += 4;
	FullTransform2V4(splatMtx, p, s.xyz_n1_x, s.xyz_n1_y, s.xyz_n1_z); p += 4;
	// yzx1
	FullTransform2V4(splatMtx, p, s.yzx_p1_x, s.yzx_p1_y, s.yzx_p1_z); p += 4;
	FullTransform2V4(splatMtx, p, s.yzx_n1_x, s.yzx_n1_y, s.yzx_n1_z); p += 4;
	// zxy1
	FullTransform2V4(splatMtx, p, s.zxy_p1_x, s.zxy_p1_y, s.zxy_p1_z); p += 4;
	FullTransform2V4(splatMtx, p, s.zxy_n1_x, s.zxy_n1_y, s.zxy_n1_z); p += 4;
	// yxz1
	FullTransform2V4(splatMtx, p, s.yxz_p1_x, s.yxz_p1_y, s.yxz_p1_z); p += 4;
	FullTransform2V4(splatMtx, p, s.yxz_n1_x, s.yxz_n1_y, s.yxz_n1_z); p += 4;
	// zyx1
	FullTransform2V4(splatMtx, p, s.zyx_p1_x, s.zyx_p1_y, s.zyx_p1_z); p += 4;
	FullTransform2V4(splatMtx, p, s.zyx_n1_x, s.zyx_n1_y, s.zyx_n1_z); p += 4;
	// xzy1
	FullTransform2V4(splatMtx, p, s.xzy_p1_x, s.xzy_p1_y, s.xzy_p1_z); p += 4;
	FullTransform2V4(splatMtx, p, s.xzy_n1_x, s.xzy_n1_y, s.xzy_n1_z); p += 4;

	Vec4V minx(V_FLT_MAX);
	Vec4V miny(V_FLT_MAX);
	Vec4V minz(V_FLT_MAX);
	Vec4V maxx = -minx;
	Vec4V maxy = -miny;
	Vec4V maxz = -minz;

	for (int i = 0; i < NELEM(groups); i++)
	{
		const Vec4V w = Abs(Invert(groups[i].GetCol3()));
		const Vec4V x = groups[i].GetCol0()*w;
		const Vec4V y = groups[i].GetCol1()*w;
		const Vec4V z = groups[i].GetCol2();

		minx = Min(x, minx);
		miny = Min(y, miny);
		minz = Min(z, minz);

		maxx = Max(x, maxx);
		maxy = Max(y, maxy);
		maxz = Max(z, maxz);
	}

	const float x0 = MinElement(minx).Getf();
	const float y0 = MinElement(miny).Getf();
	const float z0 = MinElement(minz).Getf();
	const float x1 = MaxElement(maxx).Getf();
	const float y1 = MaxElement(maxy).Getf();
	const float z1 = MaxElement(maxz).Getf();

	Color32 c = colour;

	if (z1 < 0.0f)
	{
		c = Color32(0,255,255,colour.GetAlpha()); // behind z=0 plane
	}
	else if (z0 < 0.0f)
	{
		c = Color32(255,0,0,colour.GetAlpha()); // clipped by z=0 plane
	}

	grcDebugDraw::Line(Vec2V(0.5f + 0.5f*x0, 0.5f - 0.5f*y0), Vec2V(0.5f + 0.5f*x1, 0.5f - 0.5f*y0), c);
	grcDebugDraw::Line(Vec2V(0.5f + 0.5f*x1, 0.5f - 0.5f*y0), Vec2V(0.5f + 0.5f*x1, 0.5f - 0.5f*y1), c);
	grcDebugDraw::Line(Vec2V(0.5f + 0.5f*x1, 0.5f - 0.5f*y1), Vec2V(0.5f + 0.5f*x0, 0.5f - 0.5f*y1), c);
	grcDebugDraw::Line(Vec2V(0.5f + 0.5f*x0, 0.5f - 0.5f*y1), Vec2V(0.5f + 0.5f*x0, 0.5f - 0.5f*y0), c);
}

#endif // !__SPU
#endif // __BANK

#undef xyz_n0_x
#undef xyz_n0_y
#undef yzx_n0_y
#undef yzx_n0_z
#undef zxy_n0_z
#undef zxy_n0_x
#undef yxz_n0_y
#undef yxz_n0_x
#undef zyx_n0_z
#undef zyx_n0_y
#undef xzy_n0_x
#undef xzy_n0_z

} // namespace rage
