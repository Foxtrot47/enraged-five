#ifndef INC_PORTALCORNERS_H_
#define INC_PORTALCORNERS_H_

// Rage headers
#include "vector/vector4.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

// Framework headers
#include "fwutil/Flags.h"

namespace rage {

class Color32;
class spdSphere;
class spdAABB;
class grcViewport;
namespace fwGeom { class fwLine; }

class fwPortalCorners
{
private:

	Vector3		m_corners[4]; // note -- we also store stuff in w

public:

	void SetCorner(const s32 index, const Vector3& corner);
	Vector3 GetCorner(const s32 index) const { return m_corners[ index ]; }
	Vec3V_Out GetCornerV(const s32 index) const { return RCC_VEC3V(m_corners[ index ]); }
	void SetCorners(const Vector3 corners[4]);

	void SetCornerW(const s32 index, const float w) { m_corners[index].SetW(w); }
	float GetCornerW(const s32 index) const { return m_corners[index].GetW(); }
	void SetCornerWi(const s32 index, const u32 w) { m_corners[index].SetWAsUnsignedInt(w); }
	u32 GetCornerWi(const s32 index) const { return m_corners[index].GetWAsUnsignedInt(); }
	fwFlags32& GetCornerWFlagsRef(const s32 index) { return ((fwFlags32*)&m_corners[index])[3]; }
	const fwFlags32& GetCornerWFlagsRef(const s32 index) const { return ((fwFlags32*)&m_corners[index])[3]; }

	bool HasPassedThrough(const fwGeom::fwLine& testLine, const bool backfaceCull = true) const;
	bool HasPassedThrough(const Vec3V& start, const Vec3V& end) const;
	bool IsOnPositiveSide(const Vector3& testPos) const;
	bool CalcCloseness(const Vector3& testPos, float &dist) const;
	ScalarV_Out CalcDistanceToPoint(Vec3V_In p) const;
	ScalarV_Out CalcSimpleDistanceToPointSquared(Vec3V_In p) const;

	int ClipToFrustumWithoutNearPlane(Vec2V out[4 + 5], Mat44V_In compositeMtx) const;

	void CalcBoundingSphere(spdSphere& boundSphere) const;
	void CalcBoundingBox(spdAABB& boundBox) const;
	float CalcScreenSurface(const grcViewport& viewPort) const;
	float CalcAmbientScaleFeather(Vec3V_In coords) const;
	float CalcExpandedAmbientScaleFeather(Vec3V_In coords) const;

	static void CreateExtendedLineForPortalTraversalCheck(const Vector3& origStart, const Vector3& end, fwGeom::fwLine& line);

#if __BANK && !__SPU
	void DebugDraw2D(const Color32& colour, const Color32& filled, Mat44V_In compositeMtx) const;
#endif // __BANK && !__SPU
};

inline void fwPortalCorners::SetCorner(const s32 index, const Vector3& paramCorner)
{ 
	Vector3 &corner = m_corners[index];
	float w = corner.GetW(); 
	corner = paramCorner; 
	corner.SetW(w);
}

} // namespace rage

#endif // !defined INC_PORTALCORNERS_H_
