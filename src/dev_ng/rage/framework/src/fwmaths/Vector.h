// Title	:	Vector.h
// Author	:	Adam Fowler
// Started	:	23/02/05

#ifndef INC_VECTOR_H_
#define INC_VECTOR_H_

// Rage headers
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"
#include "math/random.h"

#include "fwmaths/geomutil.h" // moved code from here to geomutil, so include this for now

namespace rage {

inline Vector3 operator*(const class Matrix34& m, const Vector3& v)
{
	Vector3 v2;
	m.Transform(v, v2);
	return v2;
}

inline float DotProduct(const Vector2& v1, const Vector2& v2) 
{ 
	return v1.Dot(v2); 
}

inline float DotProduct(const Vector3& v1, const Vector3& v2) 
{ 
	return v1.Dot(v2); 
}

inline Vector3 CrossProduct(const Vector3& v1, const Vector3& v2) 
{
	Vector3 vec; 
	vec.Cross(v1,v2); 
	return vec;
}

// TODO -- consider using mthVecRand instead to avoid LHS stalls (these functions are called by very few systems already)
inline void GetRandomVector(Vector3& vec)
{
	vec.x = g_DrawRand.GetRanged(-1.0f, 1.0f);
	vec.y = g_DrawRand.GetRanged(-1.0f, 1.0f);
	vec.z = g_DrawRand.GetRanged(-1.0f, 1.0f);
	vec.NormalizeSafe();
}

inline void GetRandomVector(Vec3f& vec)
{
	vec.SetX(g_DrawRand.GetRanged(-1.0f, 1.0f));
	vec.SetY(g_DrawRand.GetRanged(-1.0f, 1.0f));
	vec.SetZ(g_DrawRand.GetRanged(-1.0f, 1.0f));
	vec = NormalizeSafe(vec, SQRT3INVERSE);
}

inline void GetRandomVector(Vec3V& vec)
{
	vec.SetX(g_DrawRand.GetRanged(-1.0f, 1.0f));
	vec.SetY(g_DrawRand.GetRanged(-1.0f, 1.0f));
	vec.SetZ(g_DrawRand.GetRanged(-1.0f, 1.0f));
	vec = NormalizeSafe(vec, Vec3V(V_X_AXIS_WZERO));
}

inline void GetRandomVector2d(Vector2& vec)
{
	vec.x = g_DrawRand.GetRanged(-1.0f, 1.0f);
	vec.y = g_DrawRand.GetRanged(-1.0f, 1.0f);
	vec.NormalizeSafe();
}

inline float NormalizeAndMag(Vector3& vec)
{
	const ScalarV vMagSqr = MagSquared(RCC_VEC3V(vec));
	const ScalarV vMagInv = InvSqrt(vMagSqr);
	vec *= SCALARV_TO_VECTOR3(vMagInv);
	return 1.0f/vMagInv.Getf();
}

} // namespace rage

#endif // INC_VECTOR_H_
