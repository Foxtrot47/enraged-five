/////////////////////////////////////////////////////////////////////////////////
// Title	:	KeyholeTests.h
// Author	:	Adam Croston
// Started	:	02/12/08
//
// These functions are designed to simply tests against population control
// keyhole shapes, but might be usefult to others.
// Combining the two tests of bands and wedges in different ways can fully
// catergorize any link passed in.
/////////////////////////////////////////////////////////////////////////////////
#ifndef KEYHOLE_TESTS_H_
#define KEYHOLE_TESTS_H_

// game includes
#include "fwmaths/angle.h"
#include "fwmaths/vector.h"

namespace rage {

/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	DoesPointTouchBand
// PURPOSE :	To test a point against a circular band.
// PARAMETERS :	centre - the center of the circular band.
//				r0 - the inner radius of the band.
//				r1 - the outer radius of the band.
//				p0 - the point to test.
// RETURNS :	Whether or not the point touches the band.
/////////////////////////////////////////////////////////////////////////////////
inline bool DoesPointTouchBand(	const Vector2& centre,
								const float r0,
								const float r1,
								const Vector2& p0)
{
	const float		r0Sqr = (r0 * r0);
	const float		r1Sqr = (r1 * r1);

	const Vector2	p0C(centre - p0);
	const float		p0CMagSqr = p0C.Mag2();

	const bool		p0InsideR0 = (p0CMagSqr < r0Sqr);
	const bool		p0InsideR1 = (p0CMagSqr < r1Sqr);

	return (p0InsideR1 && !p0InsideR0);
}


/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	DoesPointTouchRayWedge
// PURPOSE :	To test a point against an angular wedge that extends to
//				infinity.
// PARAMETERS :	centre - the centre/ start of the wedge.
//				dir - the the centre direction of the wedge.
//				cosHalfAngle - the cosine of the half angle of the wedge.
//				p0 - the point to test.
// RETURNS :	Whether or not the point touches the wedge.
/////////////////////////////////////////////////////////////////////////////////
inline bool DoesPointTouchRayWedge(const Vector2& centre,
								   const Vector2& dir,
								   const float cosHalfAngle,
								   const Vector2& p0)
{
	Vector2 centerToP0Dir(p0 - centre);
	centerToP0Dir.Normalize();
	const float dotProdToP0 = dir.Dot(centerToP0Dir);
	return (dotProdToP0 > cosHalfAngle);
}


/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	DoesLineTouchBand
// PURPOSE :	To test a line segment against a circular band.
// PARAMETERS :	centre - the center of the circular band.
//				r0 - the inner radius of the band.
//				r1 - the outer radius of the band.
//				p0 - the start of the line segment to test.
//				p1 - the end of the line segment to test.
// RETURNS :	Whether or not any of the line segment touches the band.
/////////////////////////////////////////////////////////////////////////////////
inline bool DoesLineTouchBand(	const Vector2& centre,
								const float r0,
								const float r1,
								const Vector2& p0,
								const Vector2& p1)
{
	const float		r0Sqr = (r0 * r0);
	const float		r1Sqr = (r1 * r1);

	const Vector2	p0C(centre - p0);
	const float		p0CMagSqr = p0C.Mag2();
	const Vector2	p1C(centre - p1);
	const float		p1CMagSqr = p1C.Mag2();

	const bool		p0InsideR0 = (p0CMagSqr < r0Sqr);
	const bool		p0InsideR1 = (p0CMagSqr < r1Sqr);
	const bool		p1InsideR0 = (p1CMagSqr < r0Sqr);
	const bool		p1InsideR1 = (p1CMagSqr < r1Sqr);

	if(p0InsideR0 && p1InsideR0)
	{
		// Both are inside r0.
		return false;
	}
	else if(p0InsideR0 != p1InsideR0)
	{
		// One is inside r0 and the other must be outside r0.
		return true;
	}
	else if(p0InsideR1 && p1InsideR1)
	{
		// Both are inside r1.
		return true;
	}
	else if(p0InsideR1 != p1InsideR1)
	{
		// One is inside r1 and the other must be outside r1.
		return true;
	}
	else
	{
		// Both are outside r1.

		// Check if the line they make touches the circle of r1.
		const Vector2 p0p1(p1 - p0);
		Vector2 p0p1Dir;
		p0p1Dir.Normalize(p0p1);

		if(p0C.Dot(p0p1Dir) < 0.0f)
		{
			// The closest point on the line to centre is p0.
			// We already know that p0InsideR1 is false...
			return false;
		}
		else if(p1C.Dot(p0p1Dir) > 0.0f)
		{
			// The closest point on the line to centre is p1.
			// We already know that p1InsideR1 is false...
			return false;
		}
		else
		{
			// The closest point is somewhere along the line.
			const Vector2 p0CPerpp0p1 = p0C - (p0p1Dir * p0C.Dot(p0p1Dir));
			return (p0CPerpp0p1.Mag2() < r1Sqr);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	DoesLineTouchDoubleBand
// PURPOSE :	To test a point against two circular bands, an inner and an outer band with the same centre.
// PARAMETERS :	centre - the center of the circular bands.
//				radii - a float[4] with the inner min, inner max, outer min and outer max radii	
//				p0 - the start of the line segment to test.
//				p1 - the end of the line segment to test.
// RETURNS :	-1 if no intersection
//				1 if line intersects inner band
//				2 if line intersects outer band
//				3 if line intersects the space between inner max and outer min radii
/////////////////////////////////////////////////////////////////////////////////
inline int DoesLineTouchDoubleBand(	const Vector2& centre,
									const float* radii,
									const Vector2& p0,
									const Vector2& p1)
{
	// pass 0, check the whole circle defined by max outer radius (radii[3])
	const float		r3Sqr = (radii[3] * radii[3]);

	const Vector2	p0C(centre - p0);
	const float		p0CMagSqr = p0C.Mag2();
	const Vector2	p1C(centre - p1);
	const float		p1CMagSqr = p1C.Mag2();

	const bool		p0InsideR3 = (p0CMagSqr < r3Sqr);
	const bool		p1InsideR3 = (p1CMagSqr < r3Sqr);

	if(!((p0InsideR3 && p1InsideR3) || (p0InsideR3 != p1InsideR3)))
	{
		// Both are outside r3.

		// Check if the line they make touches the circle of r3.
		const Vector2 p0p1(p1 - p0);
		Vector2 p0p1Dir;
		p0p1Dir.Normalize(p0p1);

		if(p0C.Dot(p0p1Dir) < 0.0f)
		{
			// The closest point on the line to centre is p0.
			// We already know that p0InsideR1 is false...
			return -1;
		}
		else if(p1C.Dot(p0p1Dir) > 0.0f)
		{
			// The closest point on the line to centre is p1.
			// We already know that p1InsideR1 is false...
			return -1;
		}
		else
		{
			// The closest point is somewhere along the line.
			const Vector2 p0CPerpp0p1 = p0C - (p0p1Dir * p0C.Dot(p0p1Dir));
			if (p0CPerpp0p1.Mag2() >= r3Sqr)
			{
				return -1;
			}
		}
	}

	// pass 2, check between min and max outer radius
	const float		r2Sqr = (radii[2] * radii[2]);

	const bool		p0InsideR2 = (p0CMagSqr < r2Sqr);
	const bool		p1InsideR2 = (p1CMagSqr < r2Sqr);

	if(!(p0InsideR2 && p1InsideR2))
	{
		if((p0InsideR2 != p1InsideR2) || (p0InsideR3 && p1InsideR3) || (p0InsideR3 != p1InsideR3))
		{
			// One point is either inside r2, r3 or both are inside r3
			return 2;
		}
		else
		{
			// Both are outside r3.

			// Check if the line they make touches the circle of r3.
			const Vector2 p0p1(p1 - p0);
			Vector2 p0p1Dir;
			p0p1Dir.Normalize(p0p1);

			if(!(p0C.Dot(p0p1Dir) < 0.0f) || (p1C.Dot(p0p1Dir) > 0.0f))
			{
				// The closest point is somewhere along the line.
				const Vector2 p0CPerpp0p1 = p0C - (p0p1Dir * p0C.Dot(p0p1Dir));
				if (p0CPerpp0p1.Mag2() < r3Sqr)
				{
					return 2;
				}
			}
		}
	}

	// pass 1, check between min and max inner radius
	const float		r0Sqr = (radii[0] * radii[0]);
	const float		r1Sqr = (radii[1] * radii[1]);

	const bool		p0InsideR0 = (p0CMagSqr < r0Sqr);
	const bool		p0InsideR1 = (p0CMagSqr < r1Sqr);
	const bool		p1InsideR0 = (p1CMagSqr < r0Sqr);
	const bool		p1InsideR1 = (p1CMagSqr < r1Sqr);

	if(!(p0InsideR0 && p1InsideR0))
	{
		if((p0InsideR0 != p1InsideR0) || (p0InsideR1 && p1InsideR1) || (p0InsideR1 != p1InsideR1))
		{
			// One point is either inside r0, r1 or both are inside r1
			return 1;
		}
		else
		{
			// Both are outside r1.

			// Check if the line they make touches the circle of r1.
			const Vector2 p0p1(p1 - p0);
			Vector2 p0p1Dir;
			p0p1Dir.Normalize(p0p1);

			if(!(p0C.Dot(p0p1Dir) < 0.0f || p1C.Dot(p0p1Dir) > 0.0f))
			{
				// The closest point is somewhere along the line.
				const Vector2 p0CPerpp0p1 = p0C - (p0p1Dir * p0C.Dot(p0p1Dir));
				if (p0CPerpp0p1.Mag2() < r1Sqr)
				{
					return 1;
				}
			}
		}
	}
	

	// pass 3, check against max inner radius and min outer radius
	if(p0InsideR1 && p1InsideR1)
	{
		// Both are inside r1.
		return -1;
	}
	else if((p0InsideR1 != p1InsideR1) || (p0InsideR2 && p1InsideR2) || (p0InsideR2 != p1InsideR2))
	{
		// One point is either inside r1, r2 or both are inside r2
		return 3;
	}
	else
	{
		// Both are outside r2.

		// Check if the line they make touches the circle of r2.
		const Vector2 p0p1(p1 - p0);
		Vector2 p0p1Dir;
		p0p1Dir.Normalize(p0p1);

		if(!(p0C.Dot(p0p1Dir) < 0.0f) || (p1C.Dot(p0p1Dir) > 0.0f))
		{
			// The closest point is somewhere along the line.
			const Vector2 p0CPerpp0p1 = p0C - (p0p1Dir * p0C.Dot(p0p1Dir));
			if (p0CPerpp0p1.Mag2() < r3Sqr)
			{
				return 3;
			}
		}
	}

	return -1;
}

/////////////////////////////////////////////////////////////////////////////////
// FUNCTION :	DoesLineTouchRayWedge
// PURPOSE :	To test a line segment against an angular wedge that extends to
//				infinity.
// PARAMETERS :	centre - the centre/ start of the wedge.
//				dir - the the centre direction of the wedge.
//				cosHalfAngle - the cosine of the half angle of the wedge.
//				p0 - the start of the line segment to test.
//				p1 - the end of the line segment to test.
// RETURNS :	Whether or not any of the line segment touches the wedge.
/////////////////////////////////////////////////////////////////////////////////
inline bool DoesLineTouchRayWedge(const Vector2& centre,
								  const Vector2& dir,
								  const float cosHalfAngle,
								  const Vector2& p0,
								  const Vector2& p1)
{
	bool p0InView = false;
	Vector2 centerToP0Dir(p0 - centre);
	centerToP0Dir.Normalize();
	const float dotProdToP0 = dir.Dot(centerToP0Dir);
	if(dotProdToP0 > cosHalfAngle)
	{
		p0InView = true; 
	}

	bool p1InView = false;
	Vector2 centerToP1Dir(p1 - centre);
	centerToP1Dir.Normalize();
	const float dotProdToP1 = dir.Dot(centerToP1Dir);
	if(dotProdToP1 > cosHalfAngle)
	{
		p1InView = true; 
	}

	// Check if either is in the wedge.
	if(p0InView || p1InView)
	{
		return true;
	}

	// Check if the line crosses the view frustum.

	// Check if they are both behind the frustum.
	if((dotProdToP0 < 0) && (dotProdToP1 < 0))
	{
		return false;
	}

	// At least one is not behind the frustum.

	// Check if the line they make cuts through the frustum.
	const Vector2	p0C(centre - p0);
	const Vector2	p1C(centre - p1);
	const Vector2	p0p1(p1 - p0);
	Vector2 p0p1Dir;
	p0p1Dir.Normalize(p0p1);

	if(p0C.Dot(p0p1Dir) < 0.0f)
	{
		// The closest point on the line to centre is p0.
		// We already know that p0InView is false...
		return false;
	}
	else if(p1C.Dot(p0p1Dir) > 0.0f)
	{
		// The closest point on the line to centre is p1.
		// We already know that p1InView is false...
		return false;
	}
	else
	{
		// The closest point is somewhere along the line.
		const Vector2 p0p1Closest = p0 + (p0p1Dir * p0C.Dot(p0p1Dir));

		bool dotProdTop0p1ClosestInView = false;
		Vector2 centerTop0p1ClosestDir(p0p1Closest - centre);
		centerTop0p1ClosestDir.Normalize();
		const float dotProdTop0p1Closest = dir.Dot(centerTop0p1ClosestDir);
		if(dotProdTop0p1Closest > cosHalfAngle)
		{
			dotProdTop0p1ClosestInView = true; 
		}

		return dotProdTop0p1ClosestInView;
	}
}

} // namespace rage

#endif // KEYHOLE_TESTS_H_
