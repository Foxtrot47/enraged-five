/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    drawlist.h
// PURPOSE : intermediate data between game entities and command buffers. Gives us
// independent data which can be handed off to the render thread
// AUTHOR :  john.
// CREATED : 30/8/06
//
/////////////////////////////////////////////////////////////////////////////////


#ifndef FWDRAWLIST_DRAWLIST_H_
#define FWDRAWLIST_DRAWLIST_H_

// Rage Headers
#include "atl/map.h"
#include "atl/atfunctor.h"
#include "crskeleton/skeleton.h"
#include "data/base.h"
#include "grcore/effect_config.h"
#include "grcore/effect.h"
#include "grcore/im.h"
#include "grcore/setup.h"
#include "grcore/viewport.h"
#include "system/memops.h"
#include "system/memory.h"
#include "vector/quaternion.h"
#include "profile/element.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/threadtype.h"

// Framework headers
#include "fwmaths/rect.h"
#include "streaming/streamingdefs.h"

#define ENTITY_GPU_TIME (__BANK ? 2 : 0)

namespace rage {
class bkBank;
class dlCmdBase;
class dlCmdNewDrawList;
class fragInst;
class fwEntity;
class ptxEffectInst;
class rmcInstanceData;
class grmShader;
class grmGeometry;
class grmModel;
class grmMatrixSet;
class phInst;
class rmcDrawable;
class grcSetup;
class grcRenderTarget;
struct grcResolveFlags;

// ====================== Platform Dependent =========================
#define MAX_DRAWLIST_PAGES					((RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 128 : 48)
#define MAX_ARCHETYPE_REFS					((RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 8192 : 2800)
#define MAX_HD_ARCHETYPE_REFS				((RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 600 : 160)
#define MAX_CSET_REFS						((RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 8192 : 2350)
// ====================== Platform Dependent =========================

#define MAX_ARCHETYPE_HD_REFS				(96)

#define DRAW_COMMAND_BLOCK_SIZE				(4)

#define MAX_EMERGENCY_PAGES					(2)
#define DEFAULT_DRAWLIST_PAGE_SIZE			(128*1024)
#define DEFAULT_MIN_DRAWLIST_PAGE_SIZE		(16*1024)		// Default minimum size for a draw list page

#define MAX_DRAW_LIST_SIZE					(DEFAULT_DRAWLIST_PAGE_SIZE * MAX_DRAWLIST_PAGES) 
#define MAX_DATA_BLOCK_BYTES				((((DRAW_COMMAND_BLOCK_SIZE << 12) - sizeof(dlCmdDataBlock)) & 0xfffffff0) - 1)

#define MAX_DL_SHARED_MEM_TYPES	(5)

// Number of frames to keep a history of the draw list address ranges of.
// This should be 3 to support triple-buffered pages, 4 to support quad-buffered pages, etc.
#define MAX_DRAWLIST_ADDR_HISTORY	(3)

#define INSTR_TRACKING	(0)

// Highest possible instruction ID.
// NOTE: If you change this value, you will need to change the number of bits
// for m_instructionId in dlCmdBase as well!!
#define JUMPTABLE_SIZE	(256)

#define DETAIL_SSAO_GPU_TIMEBARS	(0)


// wrap up the use of the 'new' on the constructor in a define - makes it easier to spot
#define GET_DLC(var, type, constructor) do { PAD_DLC<type>(); var = ADD_DLC(new type constructor); } while (false)
#define DLC(type, constructor) do { PAD_DLC<type>(); ADD_DLC(new type constructor); } while (false)
#define DLC_NOPAD(type, constructor) do { ADD_DLC(new type constructor); } while (false)
#define DLC_XENON(type, constructor) XENON_ONLY(DLC(type, constructor))
#define DLC_PS3(type, constructor) PS3_ONLY(DLC(type, constructor))

#if __BANK
#define DLC_REGISTER(cmdType)	RegisterCommand(cmdType::INSTRUCTION_ID, cmdType::ExecuteStatic, cmdType::GetExtraDebugInfo, #cmdType)
#define DLC_REGISTER_EXTERNAL(cmdType) do { FastAssert(gDCBuffer); gDCBuffer->RegisterCommand(cmdType::INSTRUCTION_ID, cmdType::ExecuteStatic, cmdType::GetExtraDebugInfo, #cmdType); } while (false)
#else // __BANK
#define DLC_REGISTER(cmdType)	RegisterCommand(cmdType::INSTRUCTION_ID, cmdType::ExecuteStatic, NULL, NULL)
#define DLC_REGISTER_EXTERNAL(cmdType) do { FastAssert(gDCBuffer); gDCBuffer->RegisterCommand(cmdType::INSTRUCTION_ID, cmdType::ExecuteStatic, NULL, NULL); } while (false)
#endif // __BANK

// DO NJET SUBMIT: Move this somewhere else.
enum eSkelMatrixMode
{
	SKEL_NORMAL,
	SKEL_MODEL_RELATIVE,
};

// These are the system types as used by RAGE - the application is free
// to add additional types (preferably by defining them as DL_MEMTYPE_CUSTOM + 0
// and beyond). The resulting values must be less than MAX_DL_SHARED_MEM_TYPES.
// Note that you must register additional types with dlDrawListMgr::RegisterSharedMemType.
enum eSharedMemType
{
	DL_MEMTYPE_SHADER,
	DL_MEMTYPE_MATRIXSET,
	DL_MEMTYPE_COMMAND_HELPER,
    DL_MEMTYPE_ENTITY,

	DL_MEMTYPE_CUSTOM
};


enum eInstructionId {
	DC_NewDrawList					= 1,
	DC_EndDrawList					= 2,

	// generic data storage command
	DC_DataBlock					= 3,

	DC_BeginDraw					= 4,
	DC_EndDraw						= 5,
	DC_ComputeShaderBatch			= 6,

	DC_AllocScopePush               = 7,
	DC_AllocScopePop                = 8,

	// shader related commands
	DC_SetGlobalVariableF			= 10,
	DC_SetGlobalVariableV4			= 11,
	DC_SetGlobalVariableM44			= 12,
	DC_SetGlobalVariableRT			= 13,
	DC_SetGlobalVarArrayFloat		= 14,
	DC_SetShaderGroupVariable		= 15,
	DC_SetShaderGroupVarArray		= 16,
	DC_PushForcedTechnique			= 17,
	DC_PopForcedTechnique			= 18,
	DC_SetGlobalVarArrayV4			= 19,
	DC_SetGlobalVariableI			= 20,

	// hud related commands
	DC_DrawTriShape					= 26,

	// low level graphics commands
	DC_LockRenderTarget				= 30,
	DC_UnlockRenderTarget			= 31,
	DC_GenerateMipMaps				= 32,
	// 32
	DC_SetCurrentViewport			= 33,
	DC_ClearRenderTarget			= 34,
	DC_SetClipPlane					= 35,
	DC_SetClipPlaneEnable			= 36,
	DC_SetShaderGPRAlloc			= 37,
	DC_GRCDEVICE_SetRenderState		= 38,
	DC_RAGE_SetRenderState			= 39,
	DC_LightStateSetEnabled			= 40,
	DC_InsertEndFence				= 41,
	DC_SetCurrentViewportNULL		= 42,
	DC_SetScissor					= 43,
	DC_DisableScissor				= 44,
#if __D3D11
	DC_LockOrUnlockContext			= 45,
	DC_UpdateBuffer					= 46,
#endif //__D3D11

	// call back instructions (we want rid of these - don't want to be jumping back into code that isn't obviously RT code)
	DC_CB_GenericNoArgs				= 50,
	DC_CB_GenericArgs				= 51,

	// debug instructions
	DC_AddDrawListMarker			= 60,
	DC_PushTimebar					= 61,
	DC_PopTimebar					= 62,
	DC_PushGPUTimebar				= 63,
	DC_PopGPUTimebar				= 64,
#if ENTITY_GPU_TIME
	DC_EntityGPUTimePush			= 65,
	DC_EntityGPUTimePop				= 66,
	DC_EntityGPUTimeFlush			= 67,
#endif // ENTITY_GPU_TIME
	DC_PushMarker					= 68,
	DC_PopMarker					= 69,

	// misc
	DC_SetGeometryVertexOffsets		= 75,
	DC_CreateRenderListGroup		= 76,
	DC_SetBucketAndRenderMode		= 77,
	DC_SetGPUDropRenderSettings		= 78,	

	DC_SwitchPage					= 80,

	DC_GrcDbgPush					= 81,
	DC_GrcDbgPop					= 82,

	// State commands
	DC_SetRasterizerState			= 90,
	DC_SetDepthStencilState			= 91,
	DC_SetDepthStencilStateEx		= 92,
	DC_SetBlendState				= 93,
	DC_SetBlendStateEx				= 94,
	DC_SetStates					= 95,
	DC_SetStatesEx					= 96,

	// State lock/unlock commands.
	DC_LockDepthStencilState		= 100,
	DC_UnlockDepthStencilState		= 101,
	DC_LockRasterizerState			= 102,
	DC_UnlockRasterizerState		= 103,
	DC_LockBlendState				= 104,
	DC_UnlockBlendState				= 105,
	DC_LockStates					= 106,
	DC_UnlockStates					= 107,

	// PS3 only instructions
#if __PPU
	DC_CcellGcmSetTransferData		= 115,
	DC_BeginAdaptiveZCulling		= 116,
	DC_EndAdaptiveZCulling			= 117,
	DC_InvalidateZCulling			= 118,
	// 119
	DC_CEdgeShadowGlobalParams		= 120,
	DC_CEdgeShadowType				= 121,
#endif //__PPU...
#if __XENON
	DC_SetScissorXenon				= 122,
#endif
	DC_BeginConditionalRender		= 123,
	DC_EndConditionalRender			= 124,
	
	DC_SetArrayView					= 125,
	DC_NOP							= 127,

	// The application can put commands here
	DC_UserCommands					= 128,

	// NY project adds commands starting at this point
	DC_NumBaseDrawCommands			= 240,
};

typedef int eDrawListType;

enum eDrawListPageType {
	DPT_SIMPLE_RING_BUFFER              = 0,        // Used for the ring buffer only
	DPT_LIFETIME_RENDER_THREAD          = 1,        // Used for things that need to stay until render thread is done
	DPT_LIFETIME_GPU                    = 2,        // Used for things that need to stay until the GPU is done

#if ONE_STREAMING_HEAP
	// No need to separate virtual and physical memory
	DPT_LIFETIME_RENDER_THREAD_PHYSICAL = DPT_LIFETIME_RENDER_THREAD,
	DPT_LIFETIME_GPU_PHYSICAL           = DPT_LIFETIME_GPU,
#else // ONE_STREAMING_HEAP
	DPT_LIFETIME_RENDER_THREAD_PHYSICAL = 3,
	DPT_LIFETIME_GPU_PHYSICAL           = 4,
#endif // ONE_STREAMING_HEAP

	DPT_MAX_TYPES
};

struct dlDrawListType {
	int m_Lifetime;				// Lifetime of data in this type. 1 = double-buffered, 2 = triple-buffered.
	bool m_Physical;			// If true, this memory needs to come from the physical heap
#if __BANK
	const char *m_Name;			// Friendly name for this type
#endif // __BANK
};

typedef void (*dlCmdCallback)(dlCmdBase &);

// A callback for extra debugging information of a DLC command.
// The buffer is provided as a convenience to store the result in, but any string may be returned.
// It is acceptable to return NULL.
typedef const char *(*dlCmdExtraDebugCallback)(dlCmdBase &, char *buffer, size_t bufferSize, Color32 &color);

/** PURPOSE: This is a pointer into a draw list. It's comprised from a page index and
 *  and offset within that page. It can be turned into an actual pointer using
 *  dlDrawCommandBuffer::ResolveDrawListAddress().
 *
 *  Note that a DrawListAddress can be NULL (which is internally 0xffffffff), which is
 *  the equivalent of a NULL pointer.
 */
struct DrawListAddress {
	// PURPOSE: The Parameter is used to pass a DrawListAddress by value to a function
	// in a register.
	typedef u32 Parameter;

	enum {
		PAGEINDEX_MASK = 0xff000000,
		OFFSET_MASK = 0x00ffffff,
		NULL_DRAW_LIST_ADDRESS = 0xffffffff,
		PAGEINDEX_OFFSET = 24,
	};

	DrawListAddress()				{ m_DrawListAddress = (u32)NULL_DRAW_LIST_ADDRESS; }
	DrawListAddress(u32 address)	{ m_DrawListAddress = address; }
	DrawListAddress(int pageIndex, u32 address)	{ m_DrawListAddress = (((u32) pageIndex) << PAGEINDEX_OFFSET) | address; }
	DrawListAddress(const DrawListAddress &addr)	{ m_DrawListAddress = addr.m_DrawListAddress; }

	// PURPOSE: Retrieve the page index of this address.
	int GetPageIndex() const		{ return ((int) m_DrawListAddress) >> PAGEINDEX_OFFSET; }

	// PURPOSE: Retrieve the offset within the page of this address.
	u32 GetOffset() const			{ return m_DrawListAddress & OFFSET_MASK; }
	void SetOffset(u32 offset)		{ m_DrawListAddress &= ~OFFSET_MASK; m_DrawListAddress |= offset; }

	// PURPOSE: Move the address forward by a given number of bytes. Will trigger a trap if
	// this causes the DrawListAddress to exceed the boundaries of the page.
	void MoveOffset(s32 distance);

	// PURPOSE: Set this address to NULL (which is internally 0xffffffff).
	void SetNULL()					{ m_DrawListAddress = (u32)NULL_DRAW_LIST_ADDRESS; }

	// PURPOSE: Check to see if this address is NULL (internally 0xffffffff).
	bool IsNULL() const				{ return m_DrawListAddress == NULL_DRAW_LIST_ADDRESS; }

	operator Parameter() const		{ return m_DrawListAddress; }
	bool operator !=(const DrawListAddress &addr) const	{ return m_DrawListAddress != addr.m_DrawListAddress; }

private:
	u32 m_DrawListAddress;			// Combined page index and offset
};

inline void DrawList_MoveOffset(u32 &current,u32 offset) { 
	Assert((((current & DrawListAddress::OFFSET_MASK) + offset) & ~DrawListAddress::OFFSET_MASK) == 0);
	current += offset;
}

/** PURPOSE: This is a control structure for one draw list page.
 *  A page is a block of memory in the streaming heap that will be used for draw list data
 *  (although any system is allowed to allocate draw list pages and use them for whatever purpose
 *  they see fit).
 *
 *  A page can be an "emergency page", which means that it will remain permanently in memory
 *  and will never be used unless the system is completely out of memory during an allocation
 *  and has no other recourse than using the emergency page.
 */
class dlDrawListPage {
public:
	dlDrawListPage();

	// PURPOSE: Physically allocate memory for this page, and initialize the members of the object.
	// This function might block if there is not enough memory available, and it may still fail.
	bool InitAndAllocate(eDrawListPageType type, u32 size);

	// PURPOSE: Free the memory that was  used for this page.
	void Free();

	// PURPOSE: Get the pointer to the memory of this page, NULL if this page is not allocated (IsAllocated() == false).
	char *GetStartPointer() const			{ return m_Page; }

	// PURPOSE: Get the page index to the next page for this data type.
	int GetNextPage() const					{ return (int) m_NextPage; }

	// PURPOSE: Set the page index to the next page for this data type.
	void SetNextPage(int pageIndex)			{ m_NextPage = (s16) pageIndex; }

	// PURPOSE: Get the data type of this page.
	eDrawListPageType GetType() const		{ return m_PageType; }

	void SetType(eDrawListPageType type)	{ m_PageType = type; }

	// PURPOSE: Recycle this page, i.e. indicate that it is now being actively used again.
	void Recycle()							{ m_NextPage = -1; }

	// PURPOSE: Returns TRUE if this page has physical memory attached to it, FALSE if not.
	bool IsAllocated() const				{ return m_Page != NULL; }

	// PURPOSE: Set the timestamp for this page, i.e. the frame number it's being used for.
	void SetFrame(u32 frame)				{ m_Frame = frame; }

	// PURPOSE: Returns the timestamp (frame number) this page is being used for.
	u32 GetFrame() const					{ return m_Frame; }

	// PURPOSE: Returns TRUE if this is an emergency page, i.e. it is always in memory
	// and will only be used if there is no other way to fullfil an allocation request.
	bool IsEmergency() const				{ return (m_IsEmergency != 0); }

	// PURPOSE: Designate this page to be an emergency page (or not), i.e. it is always in memory
	// and will only be used if there is no other way to fullfil an allocation request.
	void SetEmergency(bool isEmergency)		{ m_IsEmergency = (u16) isEmergency; }

	// PURPOSE: Get the size of this page in bytes.
	u32 GetSize() const						{ return m_Size; }

private:
	char *	m_Page;			// Start of the page
	eDrawListPageType m_PageType;		// Page type
	u16		m_IsEmergency;	// If true, this is an allocated page for emergency use only
	s16		m_NextPage;		// Index within the page pool of the next page

	u32		m_Frame;		// The frame that this page is being used for
	u32		m_Size;			// Size of this page in bytes
};

class dlCmdBase;

/** PURPOSE: Every object that could be shared inside the command buffer needs to
 * contain one of these for tracking purposes. The application should
 * treat this struct as a black box, its data is managed by the draw list
 * system.
 */
struct dlSharedDataInfo
{
	// The offset of this data block inside the current draw buffer.
	// Valid only if m_Timestamp is current.
	DrawListAddress m_Offset;

	// The timestamp for the offset - needs to match the timestamp
	// in dlDrawCommandBuffer, or else m_Offset is invalid.
	u32 m_Timestamp;

	dlSharedDataInfo() {
		m_Timestamp = 0;
	}
};

/** PURPOSE: This is a node in the id->sharedDataInfo map. This is only used for
 * those data types that require a shared data info map. Those who
 * can should embed the dlSharedDataInfo directly in their object
 * for better performance.
 */
struct dlSharedDataMapEntry
{
	const void* m_ID;
	dlSharedDataInfo m_SharedDataInfo;
#if __BANK
	size_t m_debugData;
#endif // #__BANK
};

/* PURPOSE: Internal structure to keep track of the shared memory data blocks - 
 * mostly for stats tracking.
 */
struct dlSharedMemData {
#if __STATS
	u32 m_Usage;
	u32 m_PeakUsage;
	u16 m_Count;
	u16 m_PeakCount;
	pfValueT<int> *m_EKGDataSizeMarker;
	pfValueT<int> *m_EKGCountMarker;
#endif // __STATS

	atArray<dlSharedDataMapEntry> m_SharedDataMap;

	dlSharedMemData();
	~dlSharedMemData();

	// Do a linear search for the shared data by its ID
	dlSharedDataInfo *LookupSharedData(const void* id);

	// Allocate a new shared data block
	dlSharedDataInfo &AllocateSharedData(const void* id);

#if __BANK
	dlSharedDataInfo &AllocateSharedData(const void* id, size_t debugData);
#endif // __BANK

	// Reset all allocated shared data information
	void ResetSharedData();


	void Shutdown();
	void InitSharedDataMap(int size);
#if __BANK
	const atArray<dlSharedDataMapEntry>& GetSharedDataMap() { return m_SharedDataMap; }
#endif // __BANK
};

#if __BANK
struct dlInstructionUsageStats {
	u32	m_UsageCount;			// Number of times this instruction has been used this frame

	u32 m_MemoryUsage;			// Amount of memory used for this instruction

	const char *m_Name;			// Friendly name for this instruction
};
#endif // __BANK

class sysPushMemAllocator
{
public:
	sysPushMemAllocator(sysMemAllocator* pAllocator) : m_previous(sysMemAllocator::GetCurrent()) {sysMemAllocator::SetCurrent(*pAllocator);}
	~sysPushMemAllocator() {sysMemAllocator::SetCurrent(m_previous);}
private:
	// keep both compilers happy...
	sysPushMemAllocator &operator=(const sysPushMemAllocator &) { /* NoOp */ Assert(false); return *this; }
	sysMemAllocator& m_previous;
};

/** PURPOSE: This is the main class that manages the draw lists and draw command buffers.
 */
class dlDrawCommandBuffer : public datBase {

	friend class dlCmdNewDrawList;

public:
	dlDrawCommandBuffer();
	virtual ~dlDrawCommandBuffer() {}

	virtual void Initialise();
	virtual void Shutdown();
	virtual void Reset();

	// PURPOSE: Return a pointer to the current write address into the main DLC ring buffer.
	void*	GetCurrWriteAddr() const { return ResolveDrawListAddress(m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER]); }

	// PURPOSE: Move the current write address forward by a certain number of bytes.
	// Note that this may not exceed the boundaries of the current draw list page.
	void	AddCurrWriteAddr(s32 size) { m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER].MoveOffset(size); }

	void	RegisterCommand(int instructionId, dlCmdCallback callback, dlCmdExtraDebugCallback extraDebugCallback, const char *name);
	void	OverrideCommand(int instructionId, dlCmdCallback callback, dlCmdExtraDebugCallback extraDebugCallback);

	// Add padding if necessary to align the current write position
	inline void	AlignWriteBuffer(u32 alignment);

	bool	CanFitData(s32 size) const;

	// processing of entries in the draw command buffer
	bool	ExecuteListAtAddr(DrawListAddress::Parameter addr, u32 finalIdx, u32 listId);
	bool	CheckListIntegrity(u32 addr, u32 finalIdx);

	void	UnlockReadBuffer();

	void	RegisterSharedMemType(int sharedMemType, const char *typeName);
	dlSharedMemData &GetSharedMemData(int sharedMemType)				{ return m_SharedMemData[sharedMemType]; }

	DrawListAddress::Parameter	LookupSharedData(int sharedMemType, const dlSharedDataInfo &sharedDataInfo);
	dlSharedDataInfo *LookupSharedDataById(int sharedMemType, const void* id);

	void*	AddSharedDataBlock(int sharedMemType, const void *memPtr, u32 size);

	void	AllocateSharedData(int sharedMemType, dlSharedDataInfo &sharedDataInfo, u32 size, DrawListAddress::Parameter offset);

	void*	GetDataBlock(u32 size, DrawListAddress::Parameter pos = 0xffffffff);	// get the ptr to the block of data at current or specified position in buffer (when drawlist is running)
	void*	GetDataBlockEx(dlCmdBase *cmd, DrawListAddress::Parameter pos = 0xffffffff);	// get the ptr to the block of data at specified position or after cmd (when drawlist is NOT running)
	void*	AddDataBlock(const void* memPtr, u32 size, DrawListAddress &offset);		// add an aligned block of data into the draw list & return aligned size
	void*	AddDataBlock(const void* memPtr, u32 size)				{ DrawListAddress dummy; return AddDataBlock(memPtr, size, dummy); }

	void	EnsureSpaceAvailable(s32 size, bool bSkipValidListCheck = false);

	// PURPOSE: Get a fresh new draw list page of a certain type. Will recycle an old page if available or allocate a new one.
	// NOTE: This function may block if there are currently no pages available and wait for the render thread.
	dlDrawListPage *AllocateDrawListPage(eDrawListPageType type, u32 minPageSize = DEFAULT_MIN_DRAWLIST_PAGE_SIZE, u32 preferredPageSize = DEFAULT_DRAWLIST_PAGE_SIZE, bool mayFail = false);

	dlDrawListPage &GetDrawListPage(int index)						{ return m_DrawListPages[index]; }
	const dlDrawListPage &GetDrawListPage(int index) const			{ return m_DrawListPages[index]; }

	// PURPOSE: Set the current write cursor position for a certain draw list type.
	void SetCurrentDrawListPosition(eDrawListPageType type, DrawListAddress::Parameter address)	{ m_DrawListCurrentPosition[type] = address; }

	// PURPOSE: Set the current read cursor position for a certain draw list type.
	void SetCurrentDrawListReadPosition(DrawListAddress::Parameter address)	{ m_CurrReadAddr = address; }

	// PURPOSE: This will allocate or recycle a new draw list page and link it to the current page of this type.
	// If this is the ring buffer, it will also add a DC_SwitchPage command to automatically move the PC during execution.
	DrawListAddress::Parameter MoveToNewPage(eDrawListPageType type, u32 minPageSize = DEFAULT_MIN_DRAWLIST_PAGE_SIZE, u32 preferredPageSize = DEFAULT_DRAWLIST_PAGE_SIZE, bool mayFail = false);

	// PURPOSE: All the pages in the range given - except the last one - will be put on the recycle list so they can be reused.
	void MarkPagesForRecycling(DrawListAddress::Parameter startAddr, DrawListAddress::Parameter endAddr);

	// PURPOSE: Go through all pages, increase their age, and delete the ones that have not been used in a while.
	void AgeAndDeleteOldPages();

	// PURPOSE: Returns the current read buffer start address for the main draw list command buffer.
	// This is the address where the render thread will begin reading the draw lists from.
	DrawListAddress::Parameter GetReadBufferStartAddr(eDrawListPageType type) const				{ return m_ReadBufferStartAddr[type][0]; }

	// PURPOSE: Returns the current read buffer end address for the main draw list command buffer.
	// This is the address where the render thread will stop reading the draw lists from.
	DrawListAddress::Parameter GetReadBufferEndAddr(eDrawListPageType type) const					{ return  m_ReadBufferEndAddr[type][0]; }

	// Resolve the given address into a pointer within a draw list page. The address must point to a valid page that has not been
	// freed up.
	char *	ResolveDrawListAddress(DrawListAddress::Parameter address) const;

	// PURPOSE: Allocate memory from the paged draw list type. The returned pointer will be within one of the pages.
	// Currently, the size may not be bigger than the fixed page size since the data will be guaranteed to be within one page.
	char *	AllocatePagedMemory(eDrawListPageType type, size_t size, bool mayFail, int alignment, DrawListAddress &offset);

	char *	AllocatePagedMemory(eDrawListPageType type, size_t size, bool mayFail = false, int alignment = 1) { DrawListAddress dummy; return AllocatePagedMemory(type, size, mayFail, alignment, dummy); }

	// PURPOSE: Same as AllocateObjectFromPagedMemory, but will automatically cast the result to the desired type, and will ensure that the proper alignment
	// will be used by passing __alignof(T) as the alignment requirement.
	template<typename T>
	T *AllocateObjectFromPagedMemory(eDrawListPageType type, size_t size, bool mayFail, DrawListAddress &offset)		{ return reinterpret_cast<T*>(AllocatePagedMemory(type, size, mayFail, __alignof(T), offset)); }

	template<typename T>
	T *AllocateObjectFromPagedMemory(eDrawListPageType type, size_t size, bool mayFail = false)		{ DrawListAddress dummy; return AllocateObjectFromPagedMemory<T>(type, size, mayFail, dummy); }

	// PURPOSE: Free the memory of this page and mark it as available.
	void FreeDrawListPage(int pageIndex);

	// PURPOSE: Returns the index of this draw list page.
	int GetPageIndex(dlDrawListPage *page) const;

	// PURPOSE: Return the index of the draw list currently being executed on the render thread, or
	// 0 if there is no list currently being executed.
	u32	GetCurrListId(void){return(m_currListId);}

	void	CheckWritePos(void);

	virtual void	FlipBuffers(void);

#if !__FINAL
	virtual void	DumpMemoryInfo();
#endif // !__FINAL

	DrawListAddress::Parameter		LookupWriteSkelCopy(dlSharedDataInfo &sharedDataInfo);			// note : it doesn't look up the _read_ buffer one!

#if __BANK
	dlInstructionUsageStats &GetInstructionUsageStats(int instructionId)	{ return m_InstructionUsageStats[instructionId]; }

	void AddWidgets(bkBank &bank);

	void UpdateDebugDraw();

	// PURPOSE: Store the entire draw buffer in a separate buffer for analysis
	void FreezeCommandBuffer();

	// PURPOSE: Make sure the next FlipBuffer will freeze the buffer
	void FreezeCommandBufferRequest();

	// PURPOSE: Dump frozen buffer to TTY
	void DumpFrozenBuffer();

	// PURPOSE: Called when the user changes the current debug instruction ID
	void UpdateDebugInstructionId();

	// PURPOSE: True if we should randomly make an allocation fail as part of a stress test
	bool ShouldRandomlyFailAlloc();
#endif // __BANK

#if !__FINAL
	// PURPOSE: Write the entire command buffer to the TTY.
	void	DumpCommandBuffer(bool writeBuffer);
	void	DumpCommandBuffer(DrawListAddress addr, DrawListAddress interestAddr = DrawListAddress(), u32 maxPrint = 0x1000);
	void	DumpCommandBuffer(dlCmdBase *cmd, dlCmdBase *interestAddr = NULL, u32 maxPrint = 0x1000);

	// PURPOSE: Create a string that disassembles a dlCmdBase command
	const char *DisassembleCommand(dlCmdBase &base, char *buffer, size_t bufferSize, Color32 &outColor);

	// PURPOSE: Write information about all draw list pages to the TTY.
	void	DumpDrawListPages();

	// PURPOSE: Show a list of all draw list pages.
	void	ShowPageDebug(bool toScreen);
#endif // !__FINAL

#if __BANK
	void	IncrementCommandCount()								{ m_CmdCount++; }
#endif

#if __BANK
	utimer_t	AddInstructionConstructionTime(int instructionId, utimer_t time) 	{ return m_InstructionConstructionTimings[instructionId] += time; }
	utimer_t	GetInstructionExecutionTiming(int instructionId) const		{ return m_InstructionExecutionTimings[instructionId]; }
	utimer_t	GetInstructionConstructionTiming(int instructionId) const	{ return m_InstructionConstructionTimings[instructionId]; }
#endif // __BANK

#if __BANK
	static u32		ms_maxBufSize;
#endif // __BANK

#if __DEV
	static u32			ms_currentInstructionId;
	static const char*	ms_currentInstructionName;
#endif // __DEV

	// Helper functions
	template<class T>
	T*  AddArray( T* begin, int amt);

	template<class T>
	void GetArray( T*& begin, int amt );

	template<class T>
	void  AddArrayMemory( T*& begin, int amt);

	u32	GetBuildRenderPhase(void) { return(m_buildingRenderPhaseType);}
	void	SetBuildRenderPhase(u32 idx) { m_buildingRenderPhaseType = idx;}
	u32 GetTimeStamp() { return m_Timestamp; }

protected:
	//char*	m_pDrawCommandBuffer;

	u32		m_SharedDataSize;					// Number of bytes in the shared buffer (in the one currently being written by the update thread)

	atRangeArray<dlCmdCallback, JUMPTABLE_SIZE>	m_JumpTable;	// List of every registered command, and its implementation

#if __BANK
	atRangeArray<dlCmdExtraDebugCallback, JUMPTABLE_SIZE>	m_ExtraDebugJumpTable;	// An optional callback for additional debug information
#endif // __BANK

	atRangeArray<dlDrawListPage, MAX_DRAWLIST_PAGES> m_DrawListPages;	// Master list of all draw list pages, available and used
	atFixedArray<int, MAX_DRAWLIST_PAGES> m_DrawListFreeList;	// Indices of available pages within m_DrawListPages
	atFixedArray<int, MAX_DRAWLIST_PAGES> m_DrawListIdleList;	// Indices of allocated pages within m_DrawListPages that are not in use
	atFixedArray<int, MAX_EMERGENCY_PAGES> m_EmergencyPageList;	// Indices of allocated pages that may be used in emergencies.

	atRangeArray<int, DPT_MAX_TYPES> m_DrawListFirstPage;		// First page of each type
	atRangeArray<DrawListAddress, DPT_MAX_TYPES> m_DrawListCurrentPosition;	// Current position of this draw list

#if __BANK
	atRangeArray<utimer_t, JUMPTABLE_SIZE> m_InstructionExecutionTimings;	// Time spent executing each instruction in ns
	atRangeArray<utimer_t, JUMPTABLE_SIZE> m_InstructionConstructionTimings;	// Time spent constructing each instruction in ns
	atRangeArray<int, DPT_MAX_TYPES> m_DrawListPageUsage;		// Number of pages used for each type
#endif // __BANK

#if __BANK
	enum eUsageStatsSortingCriteria {
		USAGE_SORT_BY_ID,
		USAGE_SORT_BY_COUNT,
		USAGE_SORT_BY_SIZE,
		USAGE_SORT_BY_CONSTRUCTION_TIME,
		USAGE_SORT_BY_EXECUTION_TIME,

		USAGE_SORT_TYPES
	};
	atRangeArray<dlInstructionUsageStats, JUMPTABLE_SIZE> m_InstructionUsageStats;	// Debug statistics on individual instructions

	char *	m_pFrozenDebugBuffer;				// The user can choose to freeze the current buffer and store it here.
	u32 *	m_pFrozenDebugBufferOffsets;		// Array of offsets into m_pFrozenDebugBuffer for each instruction
	u32		m_FrozenBufferCursor;				// Current instruction - this is an index into m_pFrozenDebugBufferOffset.
	u32		m_FrozenInstructionCount;			// Number of instructions in the frozen buffer
	char	m_FrozenSearchString[32];			// Substring to search for in the frozen debug display
	float	m_FrozenCursorMoveSpeed;			// Speed at which the cursor is moving up/down
	float	m_FrozenCursor;						// Current position of the cursor

	int		m_UsageStatsSortingCriteria;		// This is a eUsageStatsSort value
	int		m_CurrentDebugInstructionId;		// Instruction ID currently being analyzed
	int		m_RandomAllocFailRate;				// If >0, make allocations randomly fail to stress-test the system

	char	m_CurrentDebugInstructionName[64];

	bool	m_RenderFrozenBuffer;				// Render frozen data instead of the current buffer
	bool	m_DrawFrozenUntilCursorOnly;		// If true, we will only render the frozen buffer until the cursor
	bool	m_FreezeBufferRequest;				// If true, the next flip buffer should duplicate the buffer as well
	bool	m_AddInstructionMarkers;			// Add PIX tags around each occurence of the current instruction
	bool	m_DisableEmergencyPages;			// Stress test: Do not use the emergency pages

	bool	m_ShowInstructionUsage;
	bool	m_ShowFrozenBuffer;
	bool	m_ShowPageDebug;
#endif // __BANK

	static DECLARE_MTR_THREAD DrawListAddress::Parameter m_CurrReadAddr;

	// Position to the beginning within the buffer where the render thread is reading from.
	// [0] is the most recent one, [1] the one last frame, etc.
	DrawListAddress m_ReadBufferStartAddr[DPT_MAX_TYPES][MAX_DRAWLIST_ADDR_HISTORY];

	// Position to the end within the buffer where the render thread is reading from.
	// [0] is the most recent one, [1] the one last frame, etc.
	DrawListAddress m_ReadBufferEndAddr[DPT_MAX_TYPES][MAX_DRAWLIST_ADDR_HISTORY];

	DrawListAddress m_StartWriteAddr[DPT_MAX_TYPES];		// Position in the command buffer where writing began in the last frame

	sysCriticalSectionToken m_DrawListPageCritSec;	// Critical section for draw list page lists
	sysCriticalSectionToken m_AllocMemoryCritSec;

	// The draw list currently being executed on the render thread, or 0 if there is none being executed.
	static DECLARE_MTR_THREAD u32	m_currListId;

#if __DEV
	static DECLARE_MTR_THREAD u32	m_execCommandIdx;
#endif // __DEV

	u32	m_buildingRenderPhaseType;

	// Current timestamp - used to validate dlSharedDataInfo objects
	u32 m_Timestamp;

	// Last timestamp consumed by the render thread
	u32 m_ConsumedTimestamp;

	// Maximum age of a draw list page before it gets freed up
	u32 m_MaxPageAge;

#if __BANK
	u32		m_CmdCount;							// Number of commands added
#endif

#if __STATS
	
	u32		m_SharedObjectCount;				// Number of shared objects in this frame
	u32		m_PeakSharedDataSize;				// Highest ever value for m_SharedDataSize
	u32		m_PaddingWaste;						// Amount of memory spent on padding
	u32		m_EndOfPageWaste;					// Amount of memory wasted due to padding at the end of a page
	u32		m_DrawListSize;						// Size of the draw list currently being created, not counting end-of-page waste
	u32		m_PageAllocations;					// Number of times a page has been allocated
	u32		m_PageFrees;						// Number of times a page has been freed up
	u32		m_CurrentPageStartOffset;			// The offset at which we began writing into the current page for the current frame
	u32		m_PeakPageCount;					// Max # of draw list pages ever allocated
#endif // __STATS

	atRangeArray<dlSharedMemData, MAX_DL_SHARED_MEM_TYPES> m_SharedMemData;

	atMap<u32,u32> m_SkelCopies;	//maintain a map of the skeleton data -> drawlist entry that has been copied off this frame

#if __BANK
	static const char *sm_UsageStatsSortingStrings[USAGE_SORT_TYPES];
#endif // __BANK

	// RETURNS: The current write cursor at any given draw list type.
	// NOTE: This function is protected, since usage by user code would almost certainly be a threading bug.
	DrawListAddress::Parameter GetCurrentDrawListPosition(eDrawListPageType type)	{ return m_DrawListCurrentPosition[type]; }
};

extern dlDrawCommandBuffer *gDCBuffer;

// ------ available draw commands --------
// don't increase this - I'm assuming I can pack the bit field into a u64 for now
#define MAX_FRAG_CHILDREN	(64)
void PopulateBitFields(fragInst* pFrag, u64& damagedBits, u64& destroyedBits);

// base draw command
class dlCmdBase{

	friend class dlDrawListMgr;

public:
	enum {
		REQUIRED_ALIGNMENT = 0,
	};

#if __DEV
	inline dlCmdBase();
#endif // __DEV
	static void* operator new(size_t nSize, bool bSkipValidListCheck=false);
	void	Init(u32 instructionId, s32 commandSize);
	s32	GetCommandSizeStatic() const {return m_commandSize*DRAW_COMMAND_BLOCK_SIZE;}

#if __DEV
	static void	ResetCounter() {m_currCommandIdx = 1;}
	static u32 GetCounter()	{ return m_currCommandIdx; }
	u32	GetCommandIdx() const		{ return(m_commandIdx); }
#else // __DEV
	u32	GetCommandIdx() const		{ return(0); }
#endif // !__DEV

	bool IsEndOfDrawList() const	{ return GetInstructionIdStatic() == DC_EndDrawList; }
	bool IsDataBlock() const		{ return GetInstructionIdStatic() == DC_DataBlock; }

	static const char *GetExtraDebugInfo(dlCmdBase & /*base*/, char * /*buffer*/, size_t /*bufferSize*/, Color32 &/*color*/) { return NULL; }

	u32	GetInstructionIdStatic() const {return m_instructionId;}
#if __DEV
	static u32	ms_oldWritePos;
#endif //_DEV

#if INSTR_TRACKING
	void	RegisterMemUse() { m_aMemUsedPadded[GetInstructionIdStatic()] += GetCommandSizeStatic(); m_aMemUsed[GetInstructionIdStatic()] += GetCommandSize(); 
	m_aFreq[GetInstructionIdStatic()]++; m_aInstrCountByPhase[GetInstructionIdStatic()][gDCBuffer->GetBuildRenderPhase()]++;}
	u32	GetRegisteredSize() { return GetCommandSizeStatic(); }
	void	RegisterMemConsumed(u32 memUsed) { m_aMemConsumed[GetInstructionIdStatic()] += memUsed;}

	static void	ClearMemUse();
	static void	RequestDumpData() {m_bDumpData = true;}

	static void	InitDebugMem(u32 size);
	static void ResetDebugMem(void);
#else //INSTR_TRACKING
	void	RegisterMemUse() { }
	u32	GetRegisteredSize() { return 0;}
	void	RegisterMemConsumed(u32) { }
	static void	ClearMemUse()	{}
	static void	RequestDumpData() {}
	static void	InitDebugMem(u32) {}
	static void ResetDebugMem(void) {}
#endif //INSTR_TRACKING

private:
#if __DEV
	static u32	m_currCommandIdx;
	u32			m_commandIdx : 12;
#endif // __DEV
	u32			m_commandSize : 12; // update MAX_DATA_BLOCK_BYTES and dlCmdBase ctor if you change this
	u32			m_instructionId : 8;

#if __DEV
	void	BuildListBreakCheck();
#endif //__DEV

#if __DEV
#if INSTR_TRACKING
	static atArray<u32>	m_aMemUsed;
	static atArray<u32>	m_aMemUsedPadded;
	static atArray<u32>	m_aFreq;
	static atArray<u32>	m_aMemConsumed;
	static u16			m_aInstrCountByPhase[(DC_NumBaseDrawCommands + 100)][DL_MAX_TYPES];
#endif //INSTR_TRACKING
	static bool	m_bDumpData;
#endif //_DEV
};

template<class T> inline T* ADD_DLC(T* dc);
template<class T> inline void PAD_DLC();

inline void dlCmdBase::Init(u32 instructionId, s32 commandSize)
{
	m_instructionId = instructionId;
	Assertf(m_instructionId == instructionId, "Instruction ID overflow - we need more bits for m_instructionId, or use a lower INSTRUCTION_ID value for %d", instructionId);
	u32 size = (u32) commandSize / DRAW_COMMAND_BLOCK_SIZE;

	m_commandSize = size;
	Assertf(m_commandSize == size, "Command size overflow - we need more bits for m_commandSize, or use smaller data packets");

#if __DEV
	RegisterMemUse();
//	RegisterMemConsumed(gDCBuffer->GetDrawListOffset() - ms_oldWritePos);
	RegisterMemConsumed(commandSize);
#endif //__DEV

#if __BANK
	gDCBuffer->IncrementCommandCount();
#endif // __BANK

#if __BANK
	dlInstructionUsageStats &usageStats = gDCBuffer->GetInstructionUsageStats(instructionId);
	usageStats.m_UsageCount++;
	usageStats.m_MemoryUsage += commandSize;
#endif // __BANK
}

// -- control commands --
// create a new draw list command
class dlCmdNewDrawList : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_NewDrawList,
	};

	dlCmdNewDrawList(eDrawListType type);

	s32 GetCommandSize()  {return(sizeof(*this));}
	void Execute();
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdNewDrawList &) cmd).Execute(); }
	static const char *GetExtraDebugInfo(dlCmdBase & base, char * buffer, size_t bufferSize, Color32 &color);

	s32	GetDrawListIdx() const {return(m_drawListIdx);}
	eDrawListType	GetDrawListType() const { return m_type; }
	static int		GetCurrDrawListLODFlagShift() { return m_currDrawListLODFlagShift; }
	static int		GetCurrDrawListLODFlagShift_UpdateThread() { return m_currDrawListLODFlagShift_UpdateThread; }
#if __BANK
	static void		SetCurrDrawListLODFlagShift(int shift) { m_currDrawListLODFlagShift = shift; }
#endif // __BANK
private:
	static u32					m_currDrawListIdx;
	static DECLARE_MTR_THREAD int	m_currDrawListLODFlagShift;
	static int					m_currDrawListLODFlagShift_UpdateThread;
#if !__FINAL || __FINAL_LOGGING
	u32			m_FrameNumber;
#endif // !__FINAL || __FINAL_LOGGING
#if RSG_PC
	u32			m_FrameCounts;
#endif
	u32			m_drawListIdx;
	eDrawListType	m_type;
};


// mark termination of draw list
class dlCmdEndDrawList : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_EndDrawList,
	};

	dlCmdEndDrawList();

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdEndDrawList &) cmd).Execute(); }
	void Execute();
private:
};

class dlCmdBeginDraw : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_BeginDraw,
	};

	dlCmdBeginDraw(grcSetup* setup, bool clear);

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdBeginDraw &) cmd).Execute(); }
	void Execute();
private:
	grcSetup*	m_Setup;
	bool		m_Clear;
};

class dlCmdEndDraw : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_EndDraw,
	};

	dlCmdEndDraw(grcSetup* setup);

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdEndDraw &) cmd).Execute(); }
	void Execute();
private:
	grcSetup* m_Setup;
};

//Compute shader batching interface
class dlComputeShaderBatch;	//Fwd declare for friend
class dlComputeCmdBase : public dlCmdBase{
public:
	typedef atDelegate0<void> functor_type;
	dlComputeCmdBase(const functor_type &dispatchFunc, const functor_type &copyStructureCountFunc = functor_type() DURANGO_ONLY(, bool needsAutomaticGpuFlush = false));

	void DispatchComputeShader();
	void CopyStructureCount();

private:
	friend dlComputeShaderBatch;
	mutable dlComputeCmdBase *m_Next;
	functor_type m_DispatchFunc;
	functor_type m_CopyStructureCountFunc;
	DURANGO_ONLY(bool m_NeedsAutomaticGpuFlush);
	ORBIS_ONLY(bool m_FlushAfterDispatch);
};

//This draw list command allows you to build up a linked list of compute shader jobs to batch
class dlComputeShaderBatch : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_ComputeShaderBatch,
	};

	static void push_back(dlComputeCmdBase *cmd);
	static void EndRender()	{ sm_Current = NULL; ORBIS_ONLY(sm_CurrGDSOffset = 0); }	//Note, resetting GDS counter here, so it's the user's responsibility to sync all CS issues within a single drawlist.

	ORBIS_ONLY(static u32 GetGDSOffsetForCounter());

	inline dlComputeShaderBatch() : m_Head(NULL), m_Tail(NULL) DURANGO_ONLY(, m_HeadNoFlush(NULL), m_TailNoFlush(NULL)) { sm_Current = this; }

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlComputeShaderBatch &) cmd).Execute(); }
	void Execute();

private:
	static dlComputeShaderBatch *sm_Current;
	ORBIS_ONLY(static u32 sm_CurrGDSOffset);

	dlComputeCmdBase *m_Head, *m_Tail;
	DURANGO_ONLY(dlComputeCmdBase *m_HeadNoFlush, *m_TailNoFlush);
};

class dlCmdAllocScopePush : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_AllocScopePush,
	};

	s32 GetCommandSize() {return sizeof(*this);}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdAllocScopePush&)cmd).Execute(); }
	void Execute();
};

class dlCmdAllocScopePop : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_AllocScopePop,
	};

	s32 GetCommandSize() {return sizeof(*this);}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdAllocScopePop&)cmd).Execute(); }
	void Execute();
};

// mark a block of data in the command buffer
class dlCmdDataBlock : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_DataBlock,
		REQUIRED_ALIGNMENT = 16
	};

	dlCmdDataBlock(u32 size);

	s32 GetCommandSize()  {return(sizeof(dlCmdDataBlock) + m_dataSize);}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdDataBlock &) cmd).Execute(); }
	void Execute();
private:

	u32	m_dataSize;
	ATTR_UNUSED u8 m_Padding[8];		// To make sure the actual data is aligned by 16 bytes
};

// mark a block of data in the command buffer
class dlCmdNop : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_NOP,
	};

	static void ExecuteStatic(dlCmdBase & /*cmd*/) {  }
};


template<class T>
T*  dlDrawCommandBuffer::AddArray( T* begin, int amt)
{
	int size = ( amt ) * sizeof( T );
	return (T*)AddDataBlock( begin, size);
}

template<class T>
void dlDrawCommandBuffer::GetArray( T*& begin, int amt )
{
	int  dataSize = ( amt ) * sizeof( T );
	u32	size = dataSize;
	begin = (T*)GetDataBlock( size);
}
template<class T>
void  dlDrawCommandBuffer::AddArrayMemory( T*& begin, int amt)
{
	int size = ( amt ) * sizeof( T );
	begin = AddDataBlock( 0, size);
}

/// -- end of entity drawing commands ---

class dlCmdSetArrayView : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetArrayView,
	};

	s32 GetCommandSize() {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetArrayView &) cmd).Execute(); }
	void Execute();

	dlCmdSetArrayView(int arrayIndex = 0);

private:
	int					m_arrayIndex;
};

// -- render target commands --
// set up render target draw command
class dlCmdLockRenderTarget : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_LockRenderTarget,
	};

	s32 GetCommandSize() {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdLockRenderTarget &) cmd).Execute(); }
	void Execute();

	dlCmdLockRenderTarget(s32 idx, grcRenderTarget* colourRT, grcRenderTarget* depthRT, int arrayIndex = 0);

private:
	s32	m_idx;
	grcRenderTarget*	m_pColourRT;
	grcRenderTarget*	m_pdepthRT;
	int					m_arrayIndex;
};

class dlCmdUnLockRenderTarget : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_UnlockRenderTarget,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdUnLockRenderTarget &) cmd).Execute(); }
	void Execute();

	dlCmdUnLockRenderTarget(s32 idx, grcResolveFlags* flags = NULL);

private:
	s32	m_idx;
	grcResolveFlags m_flags;
	bool	m_flagsValid;

	friend class dlCmdUnLockRenderTarget_NoResolveXENON;
};

class dlCmdUnLockRenderTarget_NoResolveXENON : public dlCmdUnLockRenderTarget{

public:
	dlCmdUnLockRenderTarget_NoResolveXENON(s32 idx) : dlCmdUnLockRenderTarget(idx) {
#if __XENON
		m_flags.NeedResolve = false;
		m_flagsValid = true;
#endif // __XENON
	}
};

#if RSG_ORBIS
class dlCmdGenerateMipMaps : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_GenerateMipMaps,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdGenerateMipMaps &) cmd).Execute(); }
	void Execute();

	dlCmdGenerateMipMaps(grcRenderTarget* renderTarget);

private:
	grcRenderTarget* m_pRenderTarget;
};
#endif

// render target clear command
class dlCmdSetCurrentViewport : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetCurrentViewport,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetCurrentViewport &) cmd).Execute(); }
	void Execute();

	inline dlCmdSetCurrentViewport(const grcViewport* m_pCurrViewport);

	static const grcViewport *GetCurrentViewport_UpdateThread() { Assert(sysThreadType::IsUpdateThread()); return sm_CurrSetViewportUpdateThread; }

private:
	static const grcViewport *sm_CurrSetViewportUpdateThread;
	bool		m_bIsVPNull;
	grcViewport m_CurrViewport;
};

// render target clear command
class dlCmdClearRenderTarget : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_ClearRenderTarget,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdClearRenderTarget &) cmd).Execute(); }
	void Execute();

	dlCmdClearRenderTarget(bool bEnableClearColour, Color32 clearColour, bool bEnableClearDepth, float clearDepth, bool bEnableClearStencil, u32 clearStencil);

private:
	Color32		m_clearColour;
	float		m_clearDepth;
	u32			m_clearStencil;
#if __DEV
	u16			m_creatorStack;
#endif
	bool		m_bEnableClearColour:1;
	bool		m_bEnableClearDepth:1;
	bool		m_bEnableClearStencil:1;
};

// -- Clip plane --
// set clip plane
class dlCmdSetClipPlane : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetClipPlane,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetClipPlane &) cmd).Execute(); }
	void Execute();

	dlCmdSetClipPlane(u32 idx, Vec4V_In clipPlane);

private:
	u32		m_idx;
	Vec4V	m_clipPlane;
};

// enable clip planes
class dlCmdSetClipPlaneEnable : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetClipPlaneEnable,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetClipPlaneEnable &) cmd).Execute(); }
	void Execute();

	dlCmdSetClipPlaneEnable(u32 mask);

private:
	// Top 16-bits are used for anti-tamper, bottom 16-bits are the clip plane mask
	u32		m_mask;
};

// set shader allocation
class dlCmdSetShaderGPRAlloc : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetShaderGPRAlloc,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetShaderGPRAlloc &) cmd).Execute(); }
	void Execute();

	dlCmdSetShaderGPRAlloc(u32 arg1, u32 arg2);

private:
	u32		m_arg1;
	u32		m_arg2;
};

#if __PPU
// -- Cell commands --
// set up render target draw command
class dlCmdCellGcmSetTransferData : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_CcellGcmSetTransferData,
	};

	dlCmdCellGcmSetTransferData(	u8 mode, 
		u32 dstOffset, u32 dstPitch, 
		u32 srcOffset, u32 srcPitch, 
		u32 bytesPerRow, u32 rowCount);
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdCellGcmSetTransferData &) cmd).Execute(); }
	void Execute();

	s32 GetCommandSize()  {return(sizeof(*this));}

private:
	u8	m_mode;
	u32	m_dstOffset;
	u32	m_dstPitch;
	u32	m_srcOffset;
	u32	m_srcPitch;
	u32	m_bytesPerRow;
	u32	m_rowCount;
};
// set up grcEffect global variables
// ---

#if USE_EDGE
// set up global shadow map params for Edge:
class dlCmdEdgeShadowGlobalParams : public dlCmdBase
{
public:
	enum {
		INSTRUCTION_ID = DC_CEdgeShadowGlobalParams,
	};

	dlCmdEdgeShadowGlobalParams(const Matrix44 &shadowMatrix);
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdEdgeShadowGlobalParams &) cmd).Execute(); }
	void Execute();

	s32 GetCommandSize()  {return(sizeof(*this));}

	static void ExecuteNow(const Matrix44 &shadowMatrix);

private:
	Matrix44 m_shadowMatrix;
};

//
// set current shadow type for Edge:
//
class dlCmdEdgeShadowType : public dlCmdBase
{
public:
	enum {
		INSTRUCTION_ID = DC_CEdgeShadowType,
	};

	dlCmdEdgeShadowType(eEdgeShadowType type);
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdEdgeShadowType &) cmd).Execute(); }
	void Execute();

	s32 GetCommandSize()  {return(sizeof(*this));}

private:
	eEdgeShadowType m_shadowType;
};
#endif //USE_EDGE...

#endif // __PPU

// setting single global variables
template <class T_arg1>
class T_SetGlobalVar_1Arg : public dlCmdBase{
public:

	T_SetGlobalVar_1Arg(grcEffectGlobalVar globalVar, const T_arg1 &arg1)  { m_globalVar = globalVar; m_arg1 = arg1;}

protected:
	grcEffectGlobalVar		m_globalVar;
	T_arg1					m_arg1;
};

class dlCmdSetGlobalVar_F : public T_SetGlobalVar_1Arg<float> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVariableF,
	};

	dlCmdSetGlobalVar_F(grcEffectGlobalVar globalVar, float arg1) : T_SetGlobalVar_1Arg<float>(globalVar, arg1) {}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetGlobalVar_F &) cmd).Execute(); }
	void Execute()	{ grcEffect::SetGlobalVar(m_globalVar, m_arg1); }

	s32 GetCommandSize() {return(sizeof(*this));}
};

class dlCmdSetGlobalVar_I : public T_SetGlobalVar_1Arg<int> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVariableI,
	};

	dlCmdSetGlobalVar_I(grcEffectGlobalVar globalVar, int arg1) : T_SetGlobalVar_1Arg<int>(globalVar, arg1) {}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetGlobalVar_F &) cmd).Execute(); }
	void Execute()	{ grcEffect::SetGlobalVar(m_globalVar, m_arg1); }

	s32 GetCommandSize() {return(sizeof(*this));}
};

class dlCmdSetGlobalVar_V4 : public T_SetGlobalVar_1Arg<rage::Vec4V> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVariableV4,
	};

	dlCmdSetGlobalVar_V4(grcEffectGlobalVar globalVar, Vec4V_In arg1) : T_SetGlobalVar_1Arg<Vec4V>(globalVar, arg1) {}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetGlobalVar_V4 &) cmd).Execute(); }
	void Execute()	{ grcEffect::SetGlobalVar(m_globalVar, m_arg1); }

	s32 GetCommandSize() {return(sizeof(*this));}
};

class dlCmdSetGlobalVar_M44 : public T_SetGlobalVar_1Arg<rage::Mat44V> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVariableM44,
	};

	dlCmdSetGlobalVar_M44(grcEffectGlobalVar globalVar, const Mat44V &arg1) : T_SetGlobalVar_1Arg<Mat44V>(globalVar, arg1) {}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetGlobalVar_M44 &) cmd).Execute(); }
	void Execute()	{ grcEffect::SetGlobalVar(m_globalVar, m_arg1); }

	s32 GetCommandSize() {return(sizeof(*this));}
};

class dlCmdSetGlobalVar_RT : public T_SetGlobalVar_1Arg<rage::grcTexture *> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVariableRT,
	};

	dlCmdSetGlobalVar_RT(grcEffectGlobalVar globalVar, grcTexture *arg1) : T_SetGlobalVar_1Arg<grcTexture *>(globalVar, arg1) {}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetGlobalVar_RT &) cmd).Execute(); }
	void Execute()	{ grcEffect::SetGlobalVar(m_globalVar, m_arg1); }

	s32 GetCommandSize() {return(sizeof(*this));}
};

inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, int arg1)	{ DLC(dlCmdSetGlobalVar_I, (var, arg1)); }
inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, float arg1)	{ DLC(dlCmdSetGlobalVar_F, (var, arg1)); }
inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, Vec4V_In arg1)	{ DLC(dlCmdSetGlobalVar_V4, (var, arg1)); }
inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, Vec4f_In arg1)	{ DLC(dlCmdSetGlobalVar_V4, (var, (Vec4V&)arg1)); }
inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, const Mat44V &arg1)	{ DLC(dlCmdSetGlobalVar_M44, (var, arg1)); }
inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, grcTexture *arg1)	{ DLC(dlCmdSetGlobalVar_RT, (var, arg1)); }
inline void DLC_SET_GLOBAL_VAR(grcEffectGlobalVar var, grcRenderTarget *arg1)	{ DLC(dlCmdSetGlobalVar_RT, (var, (grcTexture *) arg1)); }



// setting global variable arrays - need to copy off the array data to DL and use it at execution
template <class T_arg1>
class T_SetGlobalVarArray : public dlCmdBase{
public:

	void Execute(){
//		T_arg1 *args = (T_arg1 *) (this+1);
		T_arg1* pData = (T_arg1 *) gDCBuffer->GetDataBlock(sizeof(T_arg1) * m_count);
		grcEffect::SetGlobalVar(m_globalVar, pData, m_count);
	}

	T_SetGlobalVarArray(grcEffectGlobalVar globalVar, const T_arg1* arg1, int count)  {
		m_globalVar = globalVar;
		m_count = count;

		Assert(arg1);
		// copy the data into the draw list directly
		gDCBuffer->AddDataBlock(arg1, sizeof(T_arg1) * m_count);
//		T_arg1 *args = (T_arg1 *) (this+1);
//		memcpy(args, arg1, sizeof(T_arg1) * m_count);
	}

//	s32 GetCommandSize() {return(sizeof(*this) + sizeof(T_arg1) * m_count);}
	s32 GetCommandSize() {return(sizeof(*this)); }

private:
	grcEffectGlobalVar	m_globalVar;
	int					m_count;
	//u32					m_Pad;
};

class dlCmdSetGlobalVarArrayFloat : public T_SetGlobalVarArray<float> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVarArrayFloat,
	};

	dlCmdSetGlobalVarArrayFloat(grcEffectGlobalVar globalVar, const float* arg1, int count)
				: T_SetGlobalVarArray<float>(globalVar, arg1, count)  { }

	static void ExecuteStatic(dlCmdBase &cmd)	{ ((dlCmdSetGlobalVarArrayFloat &) cmd).Execute(); }
};

class dlCmdSetGlobalVarArrayV4 : public T_SetGlobalVarArray<Vec4V> {
public:
	enum {
		INSTRUCTION_ID = DC_SetGlobalVarArrayV4,
	};

	dlCmdSetGlobalVarArrayV4(grcEffectGlobalVar globalVar, const Vec4V* arg1, int count)
		: T_SetGlobalVarArray<Vec4V>(globalVar, arg1, count) { }

	static void ExecuteStatic(dlCmdBase &cmd)	{ ((dlCmdSetGlobalVarArrayV4 &) cmd).Execute(); }
};


inline void DLC_SET_GLOBAL_VAR_ARRAY(grcEffectGlobalVar var, const float *arg1, int count)		{ DLC(dlCmdSetGlobalVarArrayFloat, (var, arg1, count)); }
inline void DLC_SET_GLOBAL_VAR_ARRAY(grcEffectGlobalVar var, const Vec4V *arg1, int count)	{ DLC(dlCmdSetGlobalVarArrayV4, (var, arg1, count)); }
inline void DLC_SET_GLOBAL_VAR_ARRAY(grcEffectGlobalVar var, const Vec4f *arg1, int count)	{ DLC(dlCmdSetGlobalVarArrayV4, (var, (Vec4V*)arg1, count)); }

// -------

class dlCmdGrcLightStateSetEnabled : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_LightStateSetEnabled,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdGrcLightStateSetEnabled &) cmd).Execute(); }
	void Execute();
	dlCmdGrcLightStateSetEnabled(bool enableLighting);

private:
	bool m_enableLighting;
};

#define DECLARE_SIMPLE_DC(name) \
	enum { INSTRUCTION_ID = DC_##name }; \
	s32 GetCommandSize()  {return(sizeof(*this));} \
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmd##name &) cmd).Execute(); } \
	void Execute()

class dlCmdSetRasterizerState : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetRasterizerState);
	dlCmdSetRasterizerState(grcRasterizerStateHandle rasterizer);
private:
	grcRasterizerStateHandle m_rasterizer;
};

class dlCmdSetDepthStencilState : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetDepthStencilState);
	dlCmdSetDepthStencilState(grcDepthStencilStateHandle depth);
private:
	grcDepthStencilStateHandle m_depth;
};

class dlCmdSetDepthStencilStateEx : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetDepthStencilStateEx);
	dlCmdSetDepthStencilStateEx(grcDepthStencilStateHandle depth, u32 stencilRef);
private:
	grcDepthStencilStateHandle m_depth;
	u32 m_stencilRef;
};

class dlCmdSetBlendState : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetBlendState);
	dlCmdSetBlendState(grcBlendStateHandle blend);
private:
	grcBlendStateHandle m_blend;
};

class dlCmdSetBlendStateEx : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetBlendStateEx);
	dlCmdSetBlendStateEx(grcBlendStateHandle blend, float blendFactors[4],u64 sampleMask);
private:
	u64 m_sampleMask;
	grcBlendStateHandle m_blend;
	u32 m_blendFactors;
};

class dlCmdSetStates : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetStates);
	dlCmdSetStates(grcRasterizerStateHandle rasterizer, grcDepthStencilStateHandle depth, grcBlendStateHandle blend);
private:
	grcRasterizerStateHandle m_rasterizer;
	grcDepthStencilStateHandle m_depth;
	grcBlendStateHandle m_blend;
};

class dlCmdSetStatesEx : public dlCmdBase{
public:
	DECLARE_SIMPLE_DC(SetStatesEx);
	dlCmdSetStatesEx(grcRasterizerStateHandle rasterizer, grcDepthStencilStateHandle depth, u32 stencilRef, grcBlendStateHandle blend, float blendFactors[4], u64 sampleMask);
private:
	u64 m_sampleMask;
	grcRasterizerStateHandle m_rasterizer;
	grcDepthStencilStateHandle m_depth;
	u32 m_stencilRef;	
	grcBlendStateHandle m_blend;
	u32 m_blendFactors;
};



// insert a fence into the GPU command buffer & store the handle to pass back to main thread
class dlCmdInsertEndFence : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_InsertEndFence,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdInsertEndFence &) cmd).Execute(); }
	void Execute();

	dlCmdInsertEndFence();

private:
};

typedef void (*DrawListCB_NoArgs)(void);

// -- simple call back DL commands which can be handled by templates --
// commands with no arguments
class dlCmdCallBackNoArg : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_CB_GenericNoArgs,
	};

	s32 GetCommandSize() {return(sizeof(*this));}
	void Execute() { DEV_ONLY(dlDrawCommandBuffer::ms_currentInstructionName = m_Name); m_callBackFunc(); }
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdCallBackNoArg &) cmd).Execute(); }

	dlCmdCallBackNoArg(DrawListCB_NoArgs cb, const char *BANK_ONLY(name))  { Assert(cb); m_callBackFunc = cb; BANK_ONLY(m_Name = name); }

#if __BANK
	static const char *GetExtraDebugInfo(dlCmdBase & base, char * /*buffer*/, size_t /*bufferSize*/, Color32 & /*color*/)	{ return ((dlCmdCallBackNoArg &) base).GetName(); }
	const char *GetName() const { return m_Name; }
#endif // __BANK

private:
	DrawListCB_NoArgs		m_callBackFunc;

#if __BANK
	const char *			m_Name;
#endif // __BANK
};


typedef void (*GenericCallbackCaller)(void *base);

template<class T_func>
struct T_CbGeneric0ArgExecutor {
	static void Execute(void *data) {
		T_CbGeneric0ArgExecutor<T_func> *thisPtr = (T_CbGeneric0ArgExecutor<T_func> *)data;
		thisPtr->m_Func();
	}

	T_func m_Func;
};

template<class T_func, class type1>
struct T_CbGeneric1ArgExecutor {
	static void Execute(void *data) {
		T_CbGeneric1ArgExecutor<T_func, type1> *thisPtr = (T_CbGeneric1ArgExecutor<T_func, type1> *)data;
		thisPtr->m_Func(thisPtr->m_arg1);
	}

	T_func m_Func;
	type1 m_arg1;
};

class dlCmdGenericArgBase : public dlCmdBase {
public:
	enum {
		INSTRUCTION_ID = DC_CB_GenericArgs,
	};
	static inline void ExecuteStatic(dlCmdBase &cmd);

#if __BANK
	static const char *GetExtraDebugInfo(dlCmdBase & base, char * /*buffer*/, size_t /*bufferSize*/, Color32 & /*color*/)	{ return ((dlCmdGenericArgBase &) base).GetName(); }
	const char* GetName() const { return m_Name; }
	inline dlCmdGenericArgBase(const char* name) : m_Name(name) {}
#endif // __BANK

protected:
	GenericCallbackCaller m_CallbackCaller;
	// MAKE THAT ONE BANK ONLY AND I'LL SHOOT YOU IN THE FACE
	// Alignement for the win.
	const char* m_Name;		// a friendly name for this callback
	char m_Pad[4]; // TODO: Temporary padding to make sure vectors work. Needs to be improved.
};

template <class T_func>
class T_CB_Generic_0Arg : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_0Arg(T_func cb, const char *name);

protected:
	T_CbGeneric0ArgExecutor<T_func> m_Executor;
};

template <class T_func, class T_arg>
class T_CB_Generic_1Arg : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_1Arg(T_func cb, const char *name, const T_arg &arg1);

protected:
	T_CbGeneric1ArgExecutor<T_func, T_arg> m_Executor;
};

template<class T_func, class type1, class type2>
struct T_CbGeneric2ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric2ArgsExecutor<T_func, type1, type2> *thisPtr = (T_CbGeneric2ArgsExecutor<T_func, type1, type2> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
};

template <class T_func, class T_arg1, class T_arg2>
class T_CB_Generic_2Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_2Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2);

protected:
	T_CbGeneric2ArgsExecutor<T_func, T_arg1, T_arg2> m_Executor;
};


template<class T_func, class type1, class type2, class type3>
struct T_CbGeneric3ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric3ArgsExecutor<T_func, type1, type2, type3> *thisPtr = (T_CbGeneric3ArgsExecutor<T_func, type1, type2, type3> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3>
class T_CB_Generic_3Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_3Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3);

protected:
	T_CbGeneric3ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3> m_Executor;
};


template<class T_func, class type1, class type2, class type3, class type4>
struct T_CbGeneric4ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric4ArgsExecutor<T_func, type1, type2, type3, type4> *thisPtr = (T_CbGeneric4ArgsExecutor<T_func, type1, type2, type3, type4> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3, thisPtr->m_arg4);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
	type4 m_arg4;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4>
class T_CB_Generic_4Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_4Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4);

protected:
	T_CbGeneric4ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4> m_Executor;
};

template<class T_func, class type1, class type2, class type3, class type4, class type5>
struct T_CbGeneric5ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric5ArgsExecutor<T_func, type1, type2, type3, type4, type5> *thisPtr = (T_CbGeneric5ArgsExecutor<T_func, type1, type2, type3, type4, type5> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3, thisPtr->m_arg4, thisPtr->m_arg5);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
	type4 m_arg4;
	type5 m_arg5;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5>
class T_CB_Generic_5Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_5Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5);

protected:
	T_CbGeneric5ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5> m_Executor;
};

template<class T_func, class type1, class type2, class type3, class type4, class type5, class type6>
struct T_CbGeneric6ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric6ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6> *thisPtr = (T_CbGeneric6ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3, thisPtr->m_arg4, thisPtr->m_arg5, thisPtr->m_arg6);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
	type4 m_arg4;
	type5 m_arg5;
	type6 m_arg6;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6>
class T_CB_Generic_6Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_6Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6);

protected:
	T_CbGeneric6ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6> m_Executor;
};

template<class T_func, class type1, class type2, class type3, class type4, class type5, class type6, class type7>
struct T_CbGeneric7ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric7ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6, type7> *thisPtr = (T_CbGeneric7ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6, type7> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3, thisPtr->m_arg4, thisPtr->m_arg5, thisPtr->m_arg6, thisPtr->m_arg7);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
	type4 m_arg4;
	type5 m_arg5;
	type6 m_arg6;
	type7 m_arg7;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6, class T_arg7>
class T_CB_Generic_7Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_7Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6, const T_arg7 &arg7);

protected:
	T_CbGeneric7ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7> m_Executor;
};


template<class T_func, class type1, class type2, class type3, class type4, class type5, class type6, class type7, class type8>
struct T_CbGeneric8ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric8ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6, type7, type8> *thisPtr = (T_CbGeneric8ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6, type7, type8> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3, thisPtr->m_arg4, thisPtr->m_arg5, thisPtr->m_arg6, thisPtr->m_arg7, thisPtr->m_arg8);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
	type4 m_arg4;
	type5 m_arg5;
	type6 m_arg6;
	type7 m_arg7;
	type8 m_arg8;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6, class T_arg7, class T_arg8>
class T_CB_Generic_8Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_8Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6, const T_arg7 &arg7, const T_arg8 &arg8);

protected:
	T_CbGeneric8ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7, T_arg8> m_Executor;
};

template<class T_func, class type1, class type2, class type3, class type4, class type5, class type6, class type7, class type8, class type9>
struct T_CbGeneric9ArgsExecutor {
	static void Execute(void *data) {
		T_CbGeneric9ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6, type7, type8, type9> *thisPtr = (T_CbGeneric9ArgsExecutor<T_func, type1, type2, type3, type4, type5, type6, type7, type8, type9> *)data;
		thisPtr->m_Func(thisPtr->m_arg1, thisPtr->m_arg2, thisPtr->m_arg3, thisPtr->m_arg4, thisPtr->m_arg5, thisPtr->m_arg6, thisPtr->m_arg7, thisPtr->m_arg8, thisPtr->m_arg9);
	}

	T_func m_Func;
	type1 m_arg1;
	type2 m_arg2;
	type3 m_arg3;
	type4 m_arg4;
	type5 m_arg5;
	type6 m_arg6;
	type7 m_arg7;
	type8 m_arg8;
	type9 m_arg9;
};

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6, class T_arg7, class T_arg8, class T_arg9>
class T_CB_Generic_9Args : public dlCmdGenericArgBase{
public:
	s32 GetCommandSize() { return(sizeof(*this)); }
	inline T_CB_Generic_9Args(T_func cb, const char *name, const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6, const T_arg7 &arg7, const T_arg8 &arg8, const T_arg9 &arg9);

protected:
	T_CbGeneric9ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7, T_arg8, T_arg9> m_Executor;
};

template<class T>
void DLC_AddInternal( T func, const char *name )
{
	FastAssert(func);
	DLC(dlCmdCallBackNoArg, ( func, name ));
}
template<class T, class Arg1 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1)
{
	FastAssert(func);
	typedef T_CB_Generic_1Arg<T, Arg1 > CbType;
	DLC(CbType, ( func, name, arg1 ));
}
template<class T, class Arg1,class Arg2 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2 )
{
	FastAssert(func);
	typedef T_CB_Generic_2Args<T, Arg1,Arg2 > CbType;
	DLC(CbType, ( func, name, arg1, arg2 ));
}
template<class T, class Arg1,class Arg2, class Arg3 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3 )
{
	FastAssert(func);
	typedef T_CB_Generic_3Args<T, Arg1,Arg2, Arg3 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4 )
{
	FastAssert(func);
	typedef T_CB_Generic_4Args<T, Arg1,Arg2, Arg3, Arg4 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 )
{
	FastAssert(func);
	typedef T_CB_Generic_5Args<T, Arg1,Arg2, Arg3, Arg4, Arg5 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6 )
{
	FastAssert(func);
	typedef T_CB_Generic_6Args<T, Arg1,Arg2, Arg3, Arg4, Arg5, Arg6 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6, class Arg7 >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6,const Arg7& arg7 )
{
	FastAssert(func);
	typedef T_CB_Generic_7Args<T, Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6, arg7 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6, class Arg7, class Arg8  >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6,const Arg7& arg7 ,const Arg8& arg8 )
{
	FastAssert(func);
	typedef T_CB_Generic_8Args<T, Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6, class Arg7, class Arg8, class Arg9  >
void DLC_AddInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5, const Arg6& arg6, const Arg7& arg7, const Arg8& arg8, const Arg9& arg9 )
{
	FastAssert(func);
	typedef T_CB_Generic_9Args<T, Arg1,Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9 ));
}


#if 0 && __XENON // append function and file names to string for better debugging (but it may be expensive)
#define DLC_Add(_cmd, ...) DLC_AddInternal(_cmd, #_cmd " -- " __FUNCTION__ " -- " __FILE__, ##__VA_ARGS__)
#elif __FINAL
#define DLC_Add(_cmd, ...) DLC_AddInternal(_cmd, NULL, ##__VA_ARGS__)
#else
#define DLC_Add(_cmd, ...) DLC_AddInternal(_cmd, #_cmd, ##__VA_ARGS__)
#endif

template<class T>
void DLC_DelegateInternal( T func, const char *name )
{
	typedef T_CB_Generic_0Arg<T> CbType;
	DLC(CbType, (  func, name ));
}

template<class T, class Arg1 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1)
{
	typedef T_CB_Generic_1Arg<T, Arg1 > CbType;
	DLC(CbType, ( func, name, arg1 ));
}

template<class T, class Arg1,class Arg2 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2 )
{
	typedef T_CB_Generic_2Args<T, Arg1, Arg2 > CbType;
	DLC(CbType, ( func, name, arg1, arg2 ));
}

template<class T, class Arg1,class Arg2, class Arg3 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3 )
{
	typedef T_CB_Generic_3Args<T, Arg1, Arg2,Arg3 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3 ));
}

template<class T, class Arg1,class Arg2, class Arg3, class Arg4 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4 )
{
	typedef T_CB_Generic_4Args<T, Arg1, Arg2, Arg3, Arg4 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 )
{
	typedef T_CB_Generic_5Args<T, Arg1, Arg2, Arg3, Arg4, Arg5 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6 )
{
	typedef T_CB_Generic_6Args<T, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6, class Arg7 >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6,const Arg7& arg7 )
{
	typedef T_CB_Generic_7Args<T, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6, arg7 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6, class Arg7, class Arg8  >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6,const Arg7& arg7 ,const Arg8& arg8 )
{
	typedef T_CB_Generic_8Args<T, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8 ));
}
template<class T, class Arg1,class Arg2, class Arg3, class Arg4, class Arg5, class Arg6, class Arg7, class Arg8, class Arg9  >
void DLC_DelegateInternal( T func, const char *name, const Arg1& arg1, const Arg2& arg2, const Arg3& arg3, const Arg4& arg4, const Arg5& arg5 ,const Arg6& arg6,const Arg7& arg7 ,const Arg8& arg8, const Arg8& arg9 )
{
	typedef T_CB_Generic_9Args<T, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9 > CbType;
	DLC(CbType, ( func, name, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9 ));
}
//////////////////////////////////////////////////////////////////////////
//
//	Usage:
//	
//	DLC_Delegate( ( <function prototype> ), ( <bound function> ), arguments ... )
//	
//
//	Examples:
//
//	DLC_Delegate(void(void), (&CShadows::PrepareShadows));
//	DLC_Delegate(void(float, bool), (&CShadows::SetParameters), 1.0f, true);
//
//	DLC_Delegate(void(const Vector3&), (this, &CLights::SetLightDirection), Vector3(0.0f, 0.0f, -1.0f));
//
//	The Shizzle - Cheap and easy double buffering with benefits.
//
//	// Make a copy of the object to create a double buffer
//	fwSampleGame* clone = static_cast<fwSampleGame*>(gDCBuffer->AddSharedDataBlock(DL_MEMTYPE_COMMAND_HELPER, this, sizeof(fwSampleGame)));
//
//	// Store calls to the buffered copy's member functions.
//	DLC_Delegate( void(void), (clone, &fwSampleGame::SetupCamera) );
//
//	...
//
//	// Super sexy
//	DLC_Delegate( void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleGame::DrawGrid), Vector3(0.0f, 0.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) );
//
//////////////////////////////////////////////////////////////////////////


#define DLC_Delegate(_type, _bind, ...)	 \
{ \
	typedef atDelegate< _type > _Delegate;\
	_Delegate _del _bind;\
	DLC_DelegateInternal( _del, #_bind, ##__VA_ARGS__ );\
}

// set scissor state
class dlCmdGrcDeviceSetScissor : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_SetScissor,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdGrcDeviceSetScissor &) cmd).Execute(); }
	void Execute();
	dlCmdGrcDeviceSetScissor(u32 x, u32 y, u32 width, u32 height);

private:
	u32 m_x, m_y, m_width, m_height;
};

#if __XENON
class dlCmdXenonSetScissor : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_SetScissorXenon,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdXenonSetScissor &) cmd).Execute(); }
	void Execute();
	dlCmdXenonSetScissor(u32 x, u32 y, u32 width, u32 height);

private:
	u32 m_left;
	u32 m_top;
	u32 m_right;
	u32 m_bottom;
};
#endif

class dlCmdBeginConditionalRender : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_BeginConditionalRender,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdBeginConditionalRender &) cmd).Execute(); }
	void Execute();
	dlCmdBeginConditionalRender(grcConditionalQuery query);

private:
	grcConditionalQuery m_query;
};

class dlCmdEndConditionalRender : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_EndConditionalRender,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdEndConditionalRender &) cmd).Execute(); }
	void Execute();
	dlCmdEndConditionalRender(grcConditionalQuery query);

private:
	grcConditionalQuery m_query;
};

#if RSG_PC && __D3D11

// Locks or unlocks the context.
class dlCmdGrcDeviceLockOrUnlockContext : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_LockOrUnlockContext,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdGrcDeviceLockOrUnlockContext &) cmd).Execute(); }
	void Execute();
	dlCmdGrcDeviceLockOrUnlockContext(bool LockOrUnlock);

private:
	// True to Lock() false to Unlock().
	bool m_LockOrUnlock;
};

#define DL_CMD_GRCDEVICE_UPDATEBUFFER_MAX_DEFERRED (1024)

// Updates a vertex, index buffer or texture.
class dlCmdGrcDeviceUpdateBuffer : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_UpdateBuffer,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdGrcDeviceUpdateBuffer &) cmd).Execute(); }
	void Execute();
	dlCmdGrcDeviceUpdateBuffer(void *pBufferObject, void *pMemPtr, u32 OffsetOrLayer, u32 SizeOrMipLevel, bool IsTexture);
public:
	static void AddBufferUpdate(void *pBufferObject, void *pMemPtr, u32 Offset, u32 Size, bool uponCancelMemoryHint);
	static void AddTextureUpdate(void *pBufferObject, void *pMemPtr, u32 Offset, u32 Size, bool uponCancelMemoryHint);
	static void *AllocTempMemory(u32 size);
private:
	static void AddUpdateCommandToDrawList(void *pBufferObject, void *pMemPtr, u32 OffsetOrLayer, u32 SizeOrMipLevel, bool IsTexture, bool uponCancelMemoryHint);
public:
	static void CancelBufferUpdate(void *pBufferObject);
	static void DoPendingBufferUpdates();
	static void OnBeginRender();
	static void OnEndRender();

private:
	typedef struct BUFFER_UPDATE_DETAILS
	{
		// Buffer to update.
		void *pBuffer;
		// Source memory.
		void *pMemPtr;
		u32 IsTexture	: 1,
			UponCanelHint : 1,
			OffsetOrLayer : 30;
		u32 SizeOrMipLevel;
	} BUFFER_UPDATE_DETAILS;

	BUFFER_UPDATE_DETAILS m_Details;
	
	// Storage for when we have to defer adding commands until a draw list is available.
	static u32 sm_NoOfDeferredUpdates;
	static BUFFER_UPDATE_DETAILS m_DeferredUpdates[DL_CMD_GRCDEVICE_UPDATEBUFFER_MAX_DEFERRED];
};

#endif // RSG_PC && __D3D11


// Disable scissor state
class dlCmdGrcDeviceDisableScissor : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_DisableScissor,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdGrcDeviceDisableScissor &) cmd).Execute(); }
	void Execute();
	dlCmdGrcDeviceDisableScissor();
};

// set viewport to NULL (without storing any data!)
class dlCmdSetCurrentViewportToNULL : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetCurrentViewportNULL,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	void Execute()  { grcViewport::SetCurrent(NULL); }
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetCurrentViewportToNULL &) cmd).Execute(); }

private:
};

// draw an immediate mode tri fan
class dlCmdDrawTriShape : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_DrawTriShape,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdDrawTriShape &) cmd).Execute(); }
	void Execute();

	dlCmdDrawTriShape(Vector2* pos, Vector2* texCoords, const grcTexture* pTex, Color32 col,  grcDrawMode shape, u32 count);

private:
	Vector2 m_pos[6];
	Vector2 m_texCoord[6];
	const grcTexture* m_pTex;
	Color32 m_col;
	u32 m_count;
	grcDrawMode m_shape;
};

// ShaderTechnique Stack
class dlCmdShaderFxPushForcedTechnique : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PushForcedTechnique,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdShaderFxPushForcedTechnique &) cmd).Execute(); }
	void Execute();

	dlCmdShaderFxPushForcedTechnique(const s32 technique);

private:
	s32 m_newTechnique;
};

class dlCmdShaderFxPopForcedTechnique : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PopForcedTechnique,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdShaderFxPopForcedTechnique &) cmd).Execute(); }
	void Execute();
};


#if __DEV
#define DRAWLISTMARKERSIZE 128

class dlCmdAddDrawListMarker : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_AddDrawListMarker,
	};

	dlCmdAddDrawListMarker(const char *marker);

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdAddDrawListMarker &) cmd).Execute(); }
	void Execute();
private:
	char m_id[DRAWLISTMARKERSIZE];
};
#endif // __DEV

#if !__FINAL
class dlCmdPushTimebar : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PushTimebar,
	};

	dlCmdPushTimebar(const char *name, const float budget = 0.0f);

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdPushTimebar &) cmd).Execute(); }
	void Execute();
private:
	const char *m_name;
	float m_budget;
};

class dlCmdPopTimebar : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PopTimebar,
	};

	dlCmdPopTimebar();
	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdPopTimebar &) cmd).Execute(); }
	void Execute();
};

enum GPUTimebarIds 
{
	GT_SKY = 0,
	GT_PUDDLES,
	GT_WATER,
	GT_EMISSIVES,
	GT_DEPTHPOSTFX,
	GT_FORWARD,
	GT_DEPTHRESOLVE,
	GT_SSA,
	GT_DISTANTLIGHTS,	
	GT_DISPLACEMENT,
	GT_CORONAS,
	GT_VISUALEFFECTS,
	GT_VOLUMETRICEFFECT,
	GT_POSTFX,
	GT_DEBUG,
#if !DETAIL_SSAO_GPU_TIMEBARS
	GT_SSAO,
#endif
	GT_SSAO_PREPARE,
	GT_SSAO_COMPUTE,
	GT_SSAO_FILTER,
	GT_AMBIENT_VOLUMES,
	GT_DIRECTIONAL,
	GT_SCENE_LIGHTS,
	GT_LOD_LIGHTS,
	GT_AMBIENT_LIGHTS,
	GT_OCC_QUERIES,
	GT_SKIN_LIGHTING,
	GT_FOGRAY,
	GT_FOLIAGE_LIGHTING,
	GT_HORIZON_OBJECTS,
	GT_CASCADE_SHADOWS,
	GT_CASCADE_PARTICLE_SHADOWS,
	GT_CASCADE_REVEAL,
	GT_LOCAL_SHADOWS,
	GT_REFLECTION,
	GT_REFLECTIONBLUR,
	GT_GBUFFER,
	GT_LATE_INTERIOR_ALPHA,
	GT_NUM_GPU_TIMEBARS
};

#define DLCPushTimebar(name) DLC(dlCmdPushTimebar, (name))
#define DLCPushTimebar_Budgeted(name,budget) DLC(dlCmdPushTimebar, (name,budget))
#define DLCPopTimebar() DLC( dlCmdPopTimebar, ())

class dlCmdPushGPUTimebar : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PushGPUTimebar,
	};

	enum TimebarIds {
		TIMER_COUNT = GT_NUM_GPU_TIMEBARS
	};

	dlCmdPushGPUTimebar(const int timerIdx, const char *name, const float budget = 0.0f);

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdPushGPUTimebar &) cmd).Execute(); }
	void Execute();
private:
	const char *m_name;
	float m_budget;
	int m_timerIdx;
};

class dlCmdPopGPUTimebar : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PopGPUTimebar,
	};

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdPopGPUTimebar &) cmd).Execute(); }
	void Execute();
};

#define DLCPushGPUTimebar(idx,name) DLC(dlCmdPushGPUTimebar, (idx,name))
#define DLCPushGPUTimebar_Budgeted(idx,name,budget) DLC(dlCmdPushGPUTimebar, (idx,name,budget))
#define DLCPopGPUTimebar() DLC(dlCmdPopGPUTimebar, ())

#if ENTITY_GPU_TIME
class dlCmdEntityGPUTimePush  : public dlCmdBase { public: DECLARE_SIMPLE_DC(EntityGPUTimePush ); static void SetEntity(const fwEntity*); static const fwEntity* GetEntity(); static bool GetFrame(); };
class dlCmdEntityGPUTimePop   : public dlCmdBase { public: DECLARE_SIMPLE_DC(EntityGPUTimePop  ); };
class dlCmdEntityGPUTimeFlush : public dlCmdBase { public: DECLARE_SIMPLE_DC(EntityGPUTimeFlush); static void SetSetup(grcSetup* setup); static float GetTime(); };
#endif // ENTITY_GPU_TIME

#undef DECLARE_SIMPLE_DC

class dlCmdPushMarker : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PushMarker,
	};

	dlCmdPushMarker(const char *name);

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdPushMarker &) cmd).Execute(); }
	void Execute();

private:
	const char *m_name;
};

class dlCmdPopMarker : public dlCmdBase{
public:
	enum {
		INSTRUCTION_ID = DC_PopMarker,
	};

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdPopMarker &) cmd).Execute(); }
	void Execute();
};

#define DLCPushMarker(name) DLC(dlCmdPushMarker, (name))
#define DLCPopMarker() DLC( dlCmdPopMarker, ())

#else //  !__FINAL

#define DLCPushTimebar(name) /* NoOp */
#define DLCPushTimebar_Budgeted(name,budget) /* NoOp */
#define DLCPopTimebar() /* NoOp */

#define DLCPushGPUTimebar(idx,name) /* NoOp */
#define DLCPushGPUTimebar_Budgeted(idx,name,budget) /* NoOp */
#define DLCPopGPUTimebar() /* NoOp */

#define DLCPushMarker(name) /* NoOp */
#define DLCPopMarker() /* NoOp */

enum GPUTimebarIds // keep the compiler happy
{
	GT_SSAO_PREPARE,
	GT_SSAO_COMPUTE,
	GT_SSAO_FILTER,
};

#endif // !__FINAL

class dlCmdSetGeometryVertexOffsets : public dlCmdBase {
public:
	enum {
		INSTRUCTION_ID = DC_SetGeometryVertexOffsets,
	};

	dlCmdSetGeometryVertexOffsets(grmGeometry* pGeom, grcVertexBuffer* pVB);

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetGeometryVertexOffsets &) cmd).Execute(); }
	void Execute();

private:
	grmGeometry* m_pGeometry;
	grcVertexBuffer* m_pVertexBuffer;
};

#if __PPU
class dlCmdBeginAdaptiveZCulling : public dlCmdBase {
public:
	enum {
		INSTRUCTION_ID = DC_BeginAdaptiveZCulling,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdBeginAdaptiveZCulling &) cmd).Execute(); }
	void Execute();

	dlCmdBeginAdaptiveZCulling(int i);
private:
	// adaptive Z culling global variables
	u32 m_Dir;
	u32 m_Step;
	float m_Prev;
	u32 m_MoveForward;
	u32 m_PushBack;
};

class dlCmdEndAdaptiveZCulling : public dlCmdBase {
public:
	enum {
		INSTRUCTION_ID = DC_EndAdaptiveZCulling,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdEndAdaptiveZCulling &) cmd).Execute(); }
	void Execute();
};

class dlCmdInvalidateZCulling : public dlCmdBase {
public:
	enum {
		INSTRUCTION_ID = DC_InvalidateZCulling,
	};

	s32 GetCommandSize()  {return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdInvalidateZCulling &) cmd).Execute(); }
	void Execute();
};

#endif // __PPU

class dlCmdSwitchPage : public dlCmdBase
{
public:
	enum {
		INSTRUCTION_ID = DC_SwitchPage,
	};

	dlCmdSwitchPage(DrawListAddress::Parameter newPage)		{ m_NewAddress = newPage; }
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSwitchPage &) cmd).Execute(); }
	void Execute();
	s32 GetCommandSize() {return sizeof(*this);}

	DrawListAddress m_NewAddress;
};

class dlCmdSetBucketsAndRenderMode : public dlCmdBase{

public:
	enum {
		INSTRUCTION_ID = DC_SetBucketAndRenderMode,
	};

	dlCmdSetBucketsAndRenderMode(const u32 bucket, const u32 subBucket, const int rm);
	dlCmdSetBucketsAndRenderMode(const u32 bucket, const int rm);
	dlCmdSetBucketsAndRenderMode(const u32 subBucket);

	s32 GetCommandSize()	{return(sizeof(*this));}
	static void ExecuteStatic(dlCmdBase &cmd) { ((dlCmdSetBucketsAndRenderMode &) cmd).Execute(); }
	void Execute();
private:
	u32 m_bucket;
	u32 m_subBucketMask;
	int m_renderMode;
	
	void SetUpdateBuckets() const;
	void SetRtBuckets() const;
};


#if GRCDBG_IMPLEMENTED
	class dlCmdGrcDbgPush : public dlCmdBase{
	public:
		enum {
			INSTRUCTION_ID = DC_GrcDbgPush,
		};

		dlCmdGrcDbgPush(const char *name)					{ m_Name = name; }

		s32 GetCommandSize()  {return(sizeof(*this));}
		void Execute();
		static void ExecuteStatic(dlCmdBase &cmd)			{ GRCDBG_PUSH(((dlCmdGrcDbgPush &) cmd).m_Name); }
		static const char *GetExtraDebugInfo(dlCmdBase & base, char * buffer, size_t bufferSize, Color32 &color);

	private:
		const char *m_Name;
	};

	inline const char *dlCmdGrcDbgPush::GetExtraDebugInfo(dlCmdBase & base, char * /*buffer*/, size_t /*bufferSize*/, Color32 &color)
	{
		color = Color32(0xffff00ff);
		return ((dlCmdGrcDbgPush &) base).m_Name;
	}


	class dlCmdGrcDbgPop : public dlCmdBase{
	public:
		enum {
			INSTRUCTION_ID = DC_GrcDbgPop,
		};

		s32 GetCommandSize()  {return(sizeof(*this));}
		static void ExecuteStatic(dlCmdBase & /*cmd*/)			{ GRCDBG_POP(); }
	};

	class DlcGrcDbgAutoPush {
	public:
		DlcGrcDbgAutoPush(const char* name) {DLC_Add(GrcDbgPush, name);}
		~DlcGrcDbgAutoPush() {DLC_Add(GrcDbgPop);}
	private:
		static void GrcDbgPush(const char* name) {GRCDBG_PUSH(name);}
		static void GrcDbgPop()                  {GRCDBG_POP();}
	};

	#define DLC_GRCDBG_AUTOPUSH(name)   DlcGrcDbgAutoPush dlcGrcDbgAutoPush##__LINE__(name);
	#define DLC_GRCDBG_PUSH(name)       DLC(dlCmdGrcDbgPush, ((const char*)name))
	#define DLC_GRCDBG_POP()            DLC(dlCmdGrcDbgPop, ())
#else
	#define DLC_GRCDBG_AUTOPUSH(name)   (void)0
	#define DLC_GRCDBG_PUSH(name)       (void)0
	#define DLC_GRCDBG_POP()            (void)0
#endif


#if HACK_GTA4_MODELINFOIDX_ON_SPU
	extern void					DbgSetDrawableModelIdxForSpu(u16);
	extern void					DbgCleanDrawableModelIdxForSpu();
#else

	static __forceinline void	DbgSetDrawableModelIdxForSpu(u16)		{ /*do nothing*/	}
	static __forceinline void	DbgCleanDrawableModelIdxForSpu()		{ /*do nothing*/	}
#endif


XENON_ONLY( void dlCmdEnableHiZ( bool bSetHiZWriteState ); )
XENON_ONLY( void dlCmdEnableAutomaticHiZ( bool bSetHiZWriteState ); )
XENON_ONLY( void dlCmdDisableHiZ( bool bSetHiZWriteState ); )
XENON_ONLY( void dlCmdSetHiZ( bool readState, bool writeState); )
// --- inline functions --------------------------------------------------------------------------------------------

inline void dlCmdGenericArgBase::ExecuteStatic(dlCmdBase &cmd)
{
	dlCmdGenericArgBase *basePtr = &((dlCmdGenericArgBase &) cmd);

	DEV_ONLY(dlDrawCommandBuffer::ms_currentInstructionName = basePtr->m_Name);

	// The arguments for the callback are in the subclass of dlCmdGenericArgBase,
	// so we need to skip all the data of this class itself.
	void *dataPtr = basePtr + 1;

	basePtr->m_CallbackCaller(dataPtr);
}

template <class T_func>
inline T_CB_Generic_0Arg<T_func>::T_CB_Generic_0Arg(T_func cb, const char *BANK_ONLY(name)) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_CallbackCaller = T_CbGeneric0ArgExecutor<T_func>::Execute;
}

template <class T_func, class T_arg>
inline T_CB_Generic_1Arg<T_func, T_arg>::T_CB_Generic_1Arg(T_func cb, const char *BANK_ONLY(name), const T_arg &arg1) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_CallbackCaller = T_CbGeneric1ArgExecutor<T_func, T_arg>::Execute;
}

template <class T_func, class T_arg1, class T_arg2>
inline T_CB_Generic_2Args<T_func, T_arg1, T_arg2>::T_CB_Generic_2Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_CallbackCaller = T_CbGeneric2ArgsExecutor<T_func, T_arg1, T_arg2>::Execute;
}

template <class T_func, class T_arg1, class T_arg2, class T_arg3>
inline T_CB_Generic_3Args<T_func, T_arg1, T_arg2, T_arg3>::T_CB_Generic_3Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_CallbackCaller = T_CbGeneric3ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3>::Execute;
}


template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4>
inline T_CB_Generic_4Args<T_func, T_arg1, T_arg2, T_arg3, T_arg4>::T_CB_Generic_4Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_Executor.m_arg4 = arg4;
	m_CallbackCaller = T_CbGeneric4ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4>::Execute;
}


template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5>
inline T_CB_Generic_5Args<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5>::T_CB_Generic_5Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_Executor.m_arg4 = arg4;
	m_Executor.m_arg5 = arg5;
	m_CallbackCaller = T_CbGeneric5ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5>::Execute;
}


template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6>
inline T_CB_Generic_6Args<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6>::T_CB_Generic_6Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_Executor.m_arg4 = arg4;
	m_Executor.m_arg5 = arg5;
	m_Executor.m_arg6 = arg6;
	m_CallbackCaller = T_CbGeneric6ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6>::Execute;
}


template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6, class T_arg7>
inline T_CB_Generic_7Args<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7>::T_CB_Generic_7Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6, const T_arg7 &arg7) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_Executor.m_arg4 = arg4;
	m_Executor.m_arg5 = arg5;
	m_Executor.m_arg6 = arg6;
	m_Executor.m_arg7 = arg7;
	m_CallbackCaller = T_CbGeneric7ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7>::Execute;
}

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6, class T_arg7, class T_arg8>
inline T_CB_Generic_8Args<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7, T_arg8>::T_CB_Generic_8Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6, const T_arg7 &arg7, const T_arg8 &arg8) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_Executor.m_arg4 = arg4;
	m_Executor.m_arg5 = arg5;
	m_Executor.m_arg6 = arg6;
	m_Executor.m_arg7 = arg7;
	m_Executor.m_arg8 = arg8;
	m_CallbackCaller = T_CbGeneric8ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7, T_arg8>::Execute;
}

template <class T_func, class T_arg1, class T_arg2, class T_arg3, class T_arg4, class T_arg5, class T_arg6, class T_arg7, class T_arg8, class T_arg9>
inline T_CB_Generic_9Args<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7, T_arg8, T_arg9>::T_CB_Generic_9Args(T_func cb, const char *BANK_ONLY(name), const T_arg1 &arg1, const T_arg2 &arg2, const T_arg3 &arg3, const T_arg4 &arg4, const T_arg5 &arg5, const T_arg6 &arg6, const T_arg7 &arg7, const T_arg8 &arg8, const T_arg9 &arg9) BANK_ONLY(: dlCmdGenericArgBase(name))
{
	m_Executor.m_Func = cb;
	m_Executor.m_arg1 = arg1;
	m_Executor.m_arg2 = arg2;
	m_Executor.m_arg3 = arg3;
	m_Executor.m_arg4 = arg4;
	m_Executor.m_arg5 = arg5;
	m_Executor.m_arg6 = arg6;
	m_Executor.m_arg7 = arg7;
	m_Executor.m_arg8 = arg8;
	m_Executor.m_arg9 = arg9;
	m_CallbackCaller = T_CbGeneric9ArgsExecutor<T_func, T_arg1, T_arg2, T_arg3, T_arg4, T_arg5, T_arg6, T_arg7, T_arg8, T_arg9>::Execute;
}

// Add padding if necessary to align the current write position
inline void dlDrawCommandBuffer::AlignWriteBuffer(u32 alignment)
{
	DrawListAddress &address = m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER];

	u32 mask = ~(-((s32)alignment));
	u32 writePosition = address.GetOffset();
	u32 currentOffset = writePosition & mask;

	if (currentOffset) {
		// Add padding.
		dlCmdNop* memPtr = (dlCmdNop *) gDCBuffer->GetCurrWriteAddr();
		::new (memPtr) dlCmdNop();
		u32 requiredPadding = alignment - currentOffset;
		memPtr->Init(DC_NOP, requiredPadding);
		address.MoveOffset(requiredPadding);

#if __STATS
		m_PaddingWaste += requiredPadding;
#endif // __STATS
	}
}

#if __DEV
inline dlCmdBase::dlCmdBase() {
// 	if (m_currCommandIdx >= (1 << 13)) {
// 		gDCBuffer->DumpCommandBuffer(true);
// 		Assertf(false, "More than %d draw list command issued in one frame. Check the TTY for a full list. We're rendering too much, or we need more bits for dlCmdBase::m_commandIdx", m_currCommandIdx);
// 	}
	u32 cmdIdx = m_currCommandIdx++;
	m_commandIdx = cmdIdx;
	//AssertMsg(m_commandIdx == cmdIdx, "Command index overflow - we need more bits for dlCmdBase::m_commandIdx");

	m_commandSize = 4095; // Change this if m_commandSize becomes something other than 12 bits wide.
	m_instructionId = 0;
	//BuildListBreakCheck();
}
#endif //__DEV

// PURPOSE: Returns TRUE if the given number of bytes will fit in the command buffer (including additional 16 bytes
// to add the SWITCHPAGE command)
inline bool dlDrawCommandBuffer::CanFitData(s32 size) const
{
	return (m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER].GetOffset() + size + 16 <= GetDrawListPage(m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER].GetPageIndex()).GetSize());
}

// - set current viewport - 
// Copy off the viewport - it may get changed by the main thread at any point otherwise...
inline dlCmdSetCurrentViewport::dlCmdSetCurrentViewport(const grcViewport* pViewport)
: m_CurrViewport(grcViewport::DoNotInitialize)
{

	m_bIsVPNull = false;
	if (pViewport){
		m_CurrViewport = *pViewport;
		Assertf(m_CurrViewport.GetNearClip() < m_CurrViewport.GetFarClip(),
			"Viewport Near %f Far %f Far must be further than near", m_CurrViewport.GetNearClip(), m_CurrViewport.GetFarClip());
		Assert(m_CurrViewport.GetWindow().GetNormX() < 2.0f);
		sm_CurrSetViewportUpdateThread = &m_CurrViewport;
	} else {
		m_bIsVPNull = true;
		sm_CurrSetViewportUpdateThread = NULL;
	}
}


template<class T> inline T* ADD_DLC(T* dc)
{
#if __BANK
	utimer_t timeStart = sysTimer::GetTicks();
#endif // __BANK

	// No virtual functions allowed!!!
#if __WIN32
	CompileTimeAssert(__has_virtual_destructor(T) == false);
#endif // __WIN32

	size_t alignment = __alignof(T);

	if (T::REQUIRED_ALIGNMENT > 0) {
		alignment = (size_t) T::REQUIRED_ALIGNMENT;
	}

	if (alignment > sizeof(dlCmdBase)) {
		Assertf((((size_t) dc) & (alignment-1)) == 0, "Adding a command of type %d which requires an alignment of %d, which isn't met - did you add a command directly via ADD_DLC but didn't call PAD_DLC prior to that?", (int) T::INSTRUCTION_ID, (int) alignment);
	}

	dc->Init(T::INSTRUCTION_ID, dc->GetCommandSize());

#if __BANK
	utimer_t timeEnd = sysTimer::GetTicks();
	timeEnd -= timeStart;
	gDCBuffer->AddInstructionConstructionTime(T::INSTRUCTION_ID, (utimer_t) timeEnd);
#endif // __BANK

	return dc;
}

template<class T> inline void PAD_DLC()
{
	size_t alignment = __alignof(T);

	if (T::REQUIRED_ALIGNMENT > 0) {
		alignment = (size_t) T::REQUIRED_ALIGNMENT;
	}

	if (alignment > sizeof(dlCmdBase)) {
		gDCBuffer->AlignWriteBuffer((int) alignment);
	}
}

inline int dlDrawCommandBuffer::GetPageIndex(dlDrawListPage *page) const
{
	int result = ptrdiff_t_to_int(page - &m_DrawListPages[0]);
	TrapGE((u32)result, (u32)MAX_DRAWLIST_PAGES);
	return result;
}

/** PURPOSE: This function converts a DrawListAddress into an actual pointer into memory.
 *
 *  PARAMS:
 *    address - DrawListAddress to convert.
 *
 *  RETURNS: A pointer into memory. NOTE that this function currently does not check to see
 *           if the address is "NULL" (0xffffffff) - if this is a possibly, check with
 *           DrawListAddress::IsNULL() first.
 */
inline char *dlDrawCommandBuffer::ResolveDrawListAddress(DrawListAddress::Parameter address) const
{
	DrawListAddress listAddress(address);

	const dlDrawListPage &page = m_DrawListPages[listAddress.GetPageIndex()];
	//TrapEQ((u32) page.GetStartPointer(), (u32) 0);
	Assertf(page.GetStartPointer(), "Error resolving draw list page address %x - page is not allocated", address);

	return page.GetStartPointer() + listAddress.GetOffset();
}

inline void DrawListAddress::MoveOffset(s32 distance)
{
	SetOffset(GetOffset() + distance);

	// trap was showing up as a non-trivial cost due to a LHS
	TrapGE(GetOffset(), gDCBuffer->GetDrawListPage(GetPageIndex()).GetSize());
}



} // namespace rage

#endif // FWDRAWLIST_DRAWLIST_H_
