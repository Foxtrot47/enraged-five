//
// FILE :    drawlistMgr.cpp
// PURPOSE : manage drawlist & execute operations on them
// AUTHOR :  john.
// CREATED : 30/8/06
//
/////////////////////////////////////////////////////////////////////////////////
#include "drawlistmgr.h"

#include "drawlist_channel.h"


// Rage headers
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "bank/button.h"
#include "bank/combo.h"
#include "bank/slider.h"
#include "grmodel/shaderfx.h"
#include "grmodel/matrixset.h"
#include "grcore/debugdraw.h"
#include "grcore/font.h"
#include "grcore/gcmdebug.h"
#include "grcore/gfxcontext.h"
#include "grcore/gfxcontextprofile.h"
#include "grcore/gputimer.h"
#include "grcore/setup.h"
#include "grprofile/drawmanager.h"
#include "grprofile/gcmtrace.h"
#include "grprofile/timebars.h"
#include "rmcore/lodgroup.h"
#include "physics/inst.h"
#include "profile/cputrace.h"
#include "profile/timebars.h"
#include "profile/trace.h"
#include "profile/page.h"
#include "profile/group.h"
#include "system/dependencyscheduler.h"
#include "system/interlocked.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/threadtype.h"
#include "grprofile/pix.h"
#include "vector/colors.h"

#include "entity/entity.h"
#include "entity/archetype.h"
#include "entity/archetypemanager.h"

#include "fwrenderer/renderthread.h"
#include "fwscene/stores/txdstore.h"
#include "fwscene/stores/dwdstore.h"
#include "fwscene/stores/fragmentstore.h"
#include "fwscene/stores/drawablestore.h"
#include "fwscene/stores/maptypesstore.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwutil/xmacro.h"
#include "fwpheffects/clothmanager.h"
#include "entity/drawdata.h"

#if	__PPU
#include "grcore/wrapper_gcm.h"
using namespace cell::Gcm;

extern "C" {

	void snStartMarker(unsigned int uID, const char *pText);
	void snStopMarker(unsigned int uID);
}

#endif

DRAWLIST_OPTIMISATIONS()


#if 0
#define DEBUGF_FENCES	Displayf
#else
#define DEBUGF_FENCES(...)
#endif


// Use this instead of just __BANK, since it can be useful to (locally) enable
// in other builds.
#define ENABLE_SINGLE_THREADED_RENDERING_DEBUG              (__BANK && MULTIPLE_RENDER_THREADS)


namespace rage {

__THREAD dlDrawListInfo* pCurrBuildingList = NULL;
__THREAD dlDrawListInfo* pCurrExecutingList = NULL;

DECLARE_MTR_THREAD int			dlDrawListMgr::m_rtRenderMode;
DECLARE_MTR_THREAD u32			dlDrawListMgr::m_rtBucket;
DECLARE_MTR_THREAD u32			dlDrawListMgr::m_rtSubBucketMask;
DECLARE_MTR_THREAD s32			dlDrawListMgr::m_forcedTechniqueStack[FENCE_COUNT][FORCED_TECHNIQUE_STACK_SIZE];
			  s32			dlDrawListMgr::m_forcedTechniqueStackIdx[FENCE_COUNT][NUMBER_OF_RENDER_THREADS];

//PARAM(RT, "[RenderThread] Use render thread");
PARAM(DRAWLIST, "[DrawLists] Use drawlists to render");
#if __BANK
PARAM(extraStreamingIndices, "[DrawLists] allocate extra streaming indices");
#endif // __BANK

#if __BANK
DECLARE_MTR_THREAD u16 dlDrawListMgr::m_DrawableStatContext;
bool bShowDrawlistExec = false;
bool bCheckListsOnceBuilt = false;
atArray<s32> dlDrawListMgr::ms_drawListStats;
atArray<float> dlDrawListMgr::ms_drawListExecutionTiming[2];        // Amount of ms spent executing this draw list
atArray<float> dlDrawListMgr::ms_drawListCreationTiming[2];         // Amount of ms spent creating this draw list
utimer_t dlDrawListMgr::ms_drawListCreationStartTime = 0;           // Temporary value to measure the amount of time spent creating this draw list
s32 dlDrawListMgr::ms_numDrawlists = 0;
eDrawListType dlDrawListMgr::ms_CurrDLType;                         // Type of current draw list being created
bool dlDrawListMgr::ms_ShaderReloadRequested = false;
bool bSafeMode = false;
bool bPedFastDraw = false;
bool bShowFrameNumbers = false;
#if __DEV
atArray<int> dlDrawListMgr::ms_instructionCount;                    // Number of instructions in each list type
bool dlDrawListMgr::ms_enableStrIndicesValidation = false;
#endif // __DEV
#endif //__BANK

#if !__FINAL
int dlDrawListMgr::ms_suppressAddTypeFileReferences = 0;
#endif // !__FINAL

PF_PAGE(DLManagerArch_Page, "DL Archetype AddRef");
PF_GROUP(DLManagerArch_Group);
PF_LINK(DLManagerArch_Page, DLManagerArch_Group);

PF_TIMER(DLManagerArch__RemoveAllRefs, DLManagerArch_Group);
PF_VALUE_INT(DLManagerArch__CSERefCount, DLManagerArch_Group);

#if __BANK
static bool s_AsyncDrawLists = true;
#endif // __BANK

// global instance for the draw list manager
dlDrawListMgr	*gDrawListMgr;

extern dlDrawCommandBuffer *gDCBuffer;

#if RAGETRACE
atArray<pfTraceCounterId> gpuDrawListTrace;
atArray<pfTraceCounterId> cpuDrawListTrace;
#endif

static atArray<ConstString> s_DrawListNames;
static atArray<int> s_DrawListLODFlagShifts;
static atArray<u8> s_DrawListCPUs;

#if ENABLE_SINGLE_THREADED_RENDERING_DEBUG
static bool s_ForceSubRenderThread0;
#endif

#if !__FINAL
struct gpuTimer {
	grcGpuTimer timer;
	float result;
	int measureFrom;
	ConstString name;
};
static atArray<gpuTimer> s_DrawListGpuTimers;
#if DEBUG_ENTITY_GPU_COST

struct gpuTimerForEntity : gpuTimer
{
	float maxTime;
	float averageTime;
	int maxTimeCount;
	bool Enabled;
};
static atArray<gpuTimerForEntity> s_DebugEntityGpuTimers;
static eDebugEntityGpuCostMode s_DebugEntityGPUCostMode = DEBUG_ENT_GPU_COST_PEAK;
#endif
static atArray<u32> s_DrawListEntityCounts;

#if __PPU && DRAWABLESPU_STATS
static atArray<spuDrawableStats> s_DrawListSpuDrawableStats[2];
static u32 s_StatsWriteIdx;
#elif DRAWABLE_STATS
static atArray<drawableStats> s_DrawListDrawableStats[2];
static u32 s_StatsWriteIdx;
#endif 

#endif

#if RAGE_TIMEBARS

static atArray<float> s_tbUpdateBudget;
static atArray<float> s_tbRenderBudget;
static atArray<float> s_tbGpuBudget;
static atArray<bool>  s_tbTimebar;

#if !__FINAL
static void DrawListGpuTimeStart(int dliType) {
	s_DrawListGpuTimers[dliType].timer.Start();
}

static void DrawListGpuTimeStop(int dliType) {
	grcGpuTimeStamp &start = s_DrawListGpuTimers[s_DrawListGpuTimers[dliType].measureFrom].timer.GetStart();
	grcGpuTimeStamp &stop = s_DrawListGpuTimers[dliType].timer.GetStop();
	stop.Write();
	float result = grcGpuTimeStamp::ElapsedMs(start, stop);
	s_DrawListGpuTimers[dliType].result = result;
	if( s_tbTimebar[dliType] ) {
		PF_INSERT_GPUBAR_BUDGETED(s_DrawListGpuTimers[dliType].name.c_str(), (result<33.3f?result:0.0f), s_tbGpuBudget[dliType]);
	}
}
#else
static void DrawListGpuTimeStart(int ) {}
static void DrawListGpuTimeStop(int ) {}
#endif

#define TIMEBAR_ONLY(_x) _x

#else // RAGE_TIMEBARS

#define TIMEBAR_ONLY(_x)

#endif // RAGE_TIMEBARS

#if GRCGFXCONTEXTPROFILE_ENABLE
	static const char *gfxContextProfilerJobNameCallback(u8 jobId) {
		return dlDrawListMgr::GetDrawListName(jobId);
	}
#endif

dlDrawListMgr::dlDrawListMgr(){

	m_numReadyLists = 0;
	m_drawListIdx = 0;

	pCurrBuildingList = NULL;
	pCurrExecutingList = NULL;
 
	m_fenceUpdateIdx = 0;
	m_fenceRenderIdx = 3;
	m_streamingIndicesBufferIndex = 0;

	m_UpdateThreadState = UPDATE_INITIALIZED;

	for (int x=0; x<FENCE_COUNT; x++) {
		m_aClothRefs[x].Reset();
		m_aCharClothRefs[x].Reset();

		m_endFenceHandle[x] = GRCDEVICE.AllocFence();
		m_fenceStatus[x] = FENCE_NONE;
		for(int y=0; y<NUMBER_OF_RENDER_THREADS; y++)
			m_forcedTechniqueStackIdx[x][y] = -1;
	}

	m_aMapDataRefs.Reset();

	for (int x=0; x<STREAMING_INDICES_BUFFER_COUNT; x++)
	{
		m_aStrIndicesCache[x].Reset();
	}
	for (int x=0; x<FENCE_COUNT; x++) 
	{
		m_customShaderEffectTypes[x].Reset();
		m_HDArchetypeRefs[x].Reset();
	}

	m_RatioTilesCulled[0] = 0;
	m_NumOfShadowTiles = 0;

	m_rtBucket = m_updtBucket = 0;
	m_rtSubBucketMask = m_updtSubBucketMask = 0xff;
	m_ratchet = 0;

	m_Cloths.Reset();
	m_CharCloths.Reset();

#if __BANK
	m_ShowDrawListStats = false;
	m_ShowDrawListStatsIndex = -1;
	m_ShowDrawListStatsFlags = 0xffffffff; // all flags
	m_ShowDrawListStatsFlags2 = 0xffffffff; // all flags
#endif // __BANK

#if DEBUG_ENTITY_GPU_COST
	m_ShowDebugEntityCostStats = false;
#endif

#if __ASSERT
	m_IsEndingDrawList = false;
	m_IsDrawListCreationAllowed = false;
#endif // __ASSERT

#if !__FINAL || __FINAL_LOGGING
	m_UpdateThreadFrameNumber = 0;
#endif // !__FINAL

#if __DEV
	m_FrameNumber = 1;
#endif // __DEV

#if GRCGFXCONTEXTPROFILE_ENABLE
	grcGfxContextProfiler::jobNameCallback = gfxContextProfilerJobNameCallback;
#endif
}

dlDrawListMgr::~dlDrawListMgr(){
}

void dlDrawListMgr::Init(int drawListTypeCount){
	USE_MEMBUCKET(MEMBUCKET_RENDER);

#if RSG_ORBIS
	// If this assert fires, increase the "context count" argument passed to
	// grcGfxContext::initRing by grcDevice::InitClass.  The header file
	// declaration for grcGfxContext::getContextPoolSize has a description of
	// what this is all for, and the proper long term solution.
	FatalAssert(drawListTypeCount <= grcGfxContext::getContextPoolSize());
#endif

	++drawListTypeCount;		// slot for totals

	// If the application hasn't created a gDCBuffer for us yet,
	// create a stock RAGE command buffer.
	if (!gDCBuffer)
	{
		gDCBuffer = rage_new dlDrawCommandBuffer();
	}

	m_DrawListPool.Init(MAX_DRAWLISTS);

	m_DrawListTypeCount = drawListTypeCount;
	s_DrawListNames.Resize(drawListTypeCount);
	s_DrawListNames[drawListTypeCount-1] = "TOTALS";

	s_DrawListLODFlagShifts.Resize(drawListTypeCount);
	sysMemSet(s_DrawListLODFlagShifts.GetElements(), 0, drawListTypeCount*sizeof(int));

	s_DrawListCPUs.Resize(drawListTypeCount);
	sysMemSet(s_DrawListCPUs.GetElements(), SUBCPUS_SYNC_MAIN, drawListTypeCount*sizeof(u8));

#if !__FINAL
	s_DrawListGpuTimers.Resize(drawListTypeCount);
	for(int i = 0; i < drawListTypeCount; i++) {
		s_DrawListGpuTimers[i].result = 0.0f;
		s_DrawListGpuTimers[i].measureFrom = i;
	}
#if DEBUG_ENTITY_GPU_COST
	s_DebugEntityGpuTimers.Resize(drawListTypeCount);
	for(int i = 0; i < drawListTypeCount; i++) {
		s_DebugEntityGpuTimers[i].result = 0.0f;
		s_DebugEntityGpuTimers[i].maxTime = 0.0f;
		s_DebugEntityGpuTimers[i].averageTime = 0.0f;
		s_DebugEntityGpuTimers[i].maxTimeCount = 0;
		s_DebugEntityGpuTimers[i].Enabled = true;
	}
#endif
    s_DrawListEntityCounts.Resize(drawListTypeCount);
    for(int i = 0; i < drawListTypeCount; i++) {
        s_DrawListEntityCounts[i] = 0;
    }

#if __PPU && DRAWABLESPU_STATS
	s_StatsWriteIdx = 0;
	s_DrawListSpuDrawableStats[0].Resize(drawListTypeCount);
	s_DrawListSpuDrawableStats[1].Resize(drawListTypeCount);	
	for(int i = 0; i < drawListTypeCount; i++) {
		sysMemSet(&s_DrawListSpuDrawableStats[0][i], 0, sizeof(spuDrawableStats));
		sysMemSet(&s_DrawListSpuDrawableStats[1][i], 0, sizeof(spuDrawableStats));	
	}
#elif DRAWABLE_STATS
	s_StatsWriteIdx = 0;
	s_DrawListDrawableStats[0].Resize(drawListTypeCount);
	s_DrawListDrawableStats[1].Resize(drawListTypeCount);	
	for(int i = 0; i < drawListTypeCount; i++) {
		sysMemSet(&s_DrawListDrawableStats[0][i], 0, sizeof(drawableStats));
		sysMemSet(&s_DrawListDrawableStats[1][i], 0, sizeof(drawableStats));	
	}
#endif

#endif

#if __BANK
	ms_drawListStats.Resize(drawListTypeCount);

	sysMemSet(ms_drawListStats.GetElements(), 0, drawListTypeCount * sizeof(s32));

	for (int x=0; x<NELEM(ms_drawListCreationTiming); x++)
	{
		ms_drawListExecutionTiming[x].Resize(drawListTypeCount);
		ms_drawListCreationTiming[x].Resize(drawListTypeCount);
	}

	m_statSaveNumPhases = 0;
	m_statSavePhases = NULL;
	m_statSaveTargetBuffer = NULL;
#endif // __BANK

#if __DEV
	ms_instructionCount.Resize(drawListTypeCount);
#endif // __DEV

#if RAGETRACE
	gpuDrawListTrace.Resize(drawListTypeCount);
	cpuDrawListTrace.Resize(drawListTypeCount);
#endif // RAGETRACE

#if RAGE_TIMEBARS
	s_tbUpdateBudget.Resize(drawListTypeCount);
	s_tbRenderBudget.Resize(drawListTypeCount);
	s_tbGpuBudget.Resize(drawListTypeCount);
	s_tbTimebar.Resize(drawListTypeCount);
#endif // RAGE_TIMEBARS


	gDCBuffer->Initialise();
//	GetBuildDrawListQ().Reset();

	for (int x=0; x<FENCE_COUNT; x++) {
		m_aClothRefs[x].Reserve(MAX_CLOTH_REFS);
		m_aCharClothRefs[x].Reserve(MAX_CLOTH_REFS);
	}

#if __BANK
	int extraStreamingIndices = 0;
	PARAM_extraStreamingIndices.Get(extraStreamingIndices);
#endif // __BANK

	m_aMapDataRefs.Reserve(16);

	for (int x=0; x<STREAMING_INDICES_BUFFER_COUNT; x++)
	{
		m_aStrIndicesCache[x].Reserve(MAX_ARCHETYPE_REFS + MAX_ARCHETYPE_HD_REFS + MAX_TXD_REFS BANK_ONLY(+ extraStreamingIndices));
	}

	for (int x=0; x<FENCE_COUNT; x++) 
	{
		m_customShaderEffectTypes[x].Reserve(MAX_CSET_REFS);
		m_HDArchetypeRefs[x].Reserve(MAX_HD_ARCHETYPE_REFS);
	}

#if COMMERCE_CONTAINER
	m_ClothsPool.Init(MAX_CLOTH, 0.62f, true);
	m_Cloths.Reserve(MAX_CLOTH / 2);
	
	m_CharClothsPool.Init(MAX_CLOTH, true);
	m_CharCloths.Reserve(MAX_CLOTH / 2);
#else
	m_ClothsPool.Init(MAX_CLOTH, 0.62f);
	m_Cloths.Reserve(MAX_CLOTH / 2);
	
	m_CharClothsPool.Init(MAX_CLOTH);
	m_CharCloths.Reserve(MAX_CLOTH / 2);
#endif
	

#if !__TOOL
	m_dependencies.Reset();
	m_dependencyInfos.Resize(MAX_DRAWLIST_DEPENDENCY_INFOS);
	for(int i = 0; i < MAX_DRAWLIST_DEPENDENCY_INFOS; i++)
	{
		m_dependencyInfos[i].m_FinishedCount = 0;
		m_dependencyInfos[i].m_ExpectedCount = 0;
		m_dependencyInfos[i].m_Sema = sysIpcCreateSema(0);
	}
	m_dependencyInfoIndex = 0;
	m_nextJoinDrawListIndex = 0;
#endif // !__TOOL

#if __BANK
	ms_ShaderReloadRequested = false;
#endif //_-BANK

#if RAGETRACE
	for(int i=0; i<drawListTypeCount; ++i)
	{
		gpuDrawListTrace[i].Init("");
		// same color and name for CPU trace:
		cpuDrawListTrace[i] = gpuDrawListTrace[i];
	}
#endif

#if __BANK
	m_HighestDrawableLOD_debug = DLOD_DEFAULT;
	m_HighestFragmentLOD_debug = DLOD_DEFAULT;
	m_HighestVehicleLOD_debug = VLOD_DEFAULT;
	m_HighestPedLOD_debug = PLOD_DEFAULT;
#endif // __BANK

	for(int i=0; i<NUMBER_OF_RENDER_THREADS + 1; i++)
	{
		m_LODInfo[i].m_HighestDrawableLOD = DLOD_DEFAULT;
		m_LODInfo[i].m_HighestFragmentLOD = DLOD_DEFAULT;
		m_LODInfo[i].m_HighestVehicleLOD = VLOD_DEFAULT;
		m_LODInfo[i].m_HighestPedLOD = PLOD_DEFAULT;
	}

	//gDLCacheMgr.Init();
}

void dlDrawListMgr::Shutdown(){

	// Have all Rendering finished ?
	for (int x=0; x<FENCE_COUNT; x++) {
		dlAssert(m_fenceStatus[x] == FENCE_NONE);
		GRCDEVICE.CpuFreeFence(m_endFenceHandle[x]);
	}

	m_aMapDataRefs.Reset();

	for (int x=0; x<STREAMING_INDICES_BUFFER_COUNT; x++) 
	{
		m_aStrIndicesCache[x].Reset();
	}

	for (int x=0; x<FENCE_COUNT; x++) 
	{
		m_customShaderEffectTypes[x].Reset();
		m_HDArchetypeRefs[x].Reset();	
	}

	gDCBuffer->Shutdown();

	m_DrawListPool.Reset();
	m_ClothsPool.Reset();
	m_CharClothsPool.Reset();
}



#if DRAWABLE_STATS
const drawableStats& dlDrawListMgr::GetDrawableStats(int phase)
{
	const drawableStats& stats = s_DrawListDrawableStats[!s_StatsWriteIdx][phase];
	return stats;
}
#elif __PS3 && DRAWABLESPU_STATS
const spuDrawableStats&	dlDrawListMgr::GetDrawableStats(int phase)
{
	const spuDrawableStats& stats = s_DrawListSpuDrawableStats[!s_StatsWriteIdx][phase];
	return stats;
}
#endif



// reset buffer and all list info
void dlDrawListMgr::Reset(){
	gDCBuffer->Reset();
	//GetBuildDrawListQ().Reset();

	for (int x=0; x<FENCE_COUNT; x++) {
		m_aClothRefs[x].Reset();
		m_aCharClothRefs[x].Reset();
	}

	m_aMapDataRefs.Reset();

	for (int x=0; x<STREAMING_INDICES_BUFFER_COUNT; x++) 
	{
		m_aStrIndicesCache[x].Reset();
	}

	for (int x=0; x<FENCE_COUNT; x++) 
	{
		m_customShaderEffectTypes[x].Reset();
		m_HDArchetypeRefs[x].Reset();
	}
}

/** PURPOSE: Set some debugging information for a draw list. This function must be called
 *  after Initialise() has been called. NOTE that this function will also (re-)initialize
 *  the gpuDrawListTrace and cpuDrawListTrace for this draw list type if the name has at
 *  least four characters.
 *
 *  PARAMS:
 *   drawListIndex - Index of the draw list to set the information of.
 *   name - Name of the draw list. This may be a temporary string, it will be copied into a ConstString.
 *   subcpus - Mask of sub cpus that can execute the drawlist (or one of the SUBCPUS_xxx values)
 *   updateBudget - Maximum number of ms that may be spent to create this draw list in a frame.
 *   renderBudget - Maximum number of ms that may be spent to execute this draw list in a frame.
 *   gpuBudget - Maximum number of ms that may be spent to render this draw list in a frame on gpu.
 */
void dlDrawListMgr::SetDrawListInfo(int drawListIndex, const char *name, u8 subcpus, int LODFlagShift, bool bEnableGPUCostEntityDebug, float TIMEBAR_ONLY(updateBudget), float TIMEBAR_ONLY(renderBudget), float TIMEBAR_ONLY(gpuBudget), bool TIMEBAR_ONLY(timebar)) {
	s_DrawListNames[drawListIndex] = name;
	s_DrawListLODFlagShifts[drawListIndex] = LODFlagShift;
#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
	Assert(subcpus==SUBCPUS_MAIN || subcpus==SUBCPUS_SYNC_MAIN || (subcpus&~SUBCPUS_ALL)==0);
#else
	Assert(subcpus==SUBCPUS_SYNC_MAIN || (subcpus&~SUBCPUS_ALL)==0);
#endif
	s_DrawListCPUs[drawListIndex] = subcpus;

#if !__FINAL
	s_DrawListGpuTimers[drawListIndex].name = name;
#endif // !__FINAL

#if RAGETRACE
	if (strlen(name) > 3) {
		gpuDrawListTrace[drawListIndex].Init(&name[3]);
		cpuDrawListTrace = gpuDrawListTrace;
	}
#endif // RAGETRACE

#if RAGE_TIMEBARS
	s_tbUpdateBudget[drawListIndex] = updateBudget;
	s_tbRenderBudget[drawListIndex] = renderBudget;
	s_tbGpuBudget[drawListIndex] = gpuBudget;
	s_tbTimebar[drawListIndex] = timebar;
#endif //  RAGE_TIMEBARS
#if DEBUG_ENTITY_GPU_COST
	s_DebugEntityGpuTimers[drawListIndex].Enabled = bEnableGPUCostEntityDebug;
#else
	(void) bEnableGPUCostEntityDebug;
#endif
}

void dlDrawListMgr::GroupDrawListTimebars(int firstDrawListIndex, int lastDrawListIndex, const char *name) {
#if __FINAL
	(void) firstDrawListIndex;
	(void) lastDrawListIndex;
	(void) name;
#else
	s_DrawListGpuTimers[lastDrawListIndex].measureFrom = firstDrawListIndex;
	s_DrawListGpuTimers[lastDrawListIndex].name = name;
#if RAGE_TIMEBARS
	s_tbTimebar[lastDrawListIndex] = true;
#endif
#endif
}

void dlDrawListMgr::SetDrawListSubCPUs(int drawListIndex, u8 subcpus) {
#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
	Assert(subcpus==SUBCPUS_MAIN || subcpus==SUBCPUS_SYNC_MAIN || (subcpus&~SUBCPUS_ALL)==0);
#else
	Assert(subcpus==SUBCPUS_SYNC_MAIN || (subcpus&~SUBCPUS_ALL)==0);
#endif
	s_DrawListCPUs[drawListIndex] = subcpus;
}

/*static*/ const char *dlDrawListMgr::GetDrawListName(int drawListIndex) {
	return s_DrawListNames[drawListIndex].c_str();
}

int dlDrawListMgr::GetDrawListLODFlagShift(int drawListIndex) const {
	return s_DrawListLODFlagShifts[drawListIndex];
}

#if !__FINAL
float dlDrawListMgr::GetDrawListGPUTime(int drawListIndex) const {
	return s_DrawListGpuTimers[drawListIndex].result;
}

#if DEBUG_ENTITY_GPU_COST
void dlDrawListMgr::SetDebugEntityGPUTime(int drawListIndex, float result)
{
	s_DebugEntityGpuTimers[drawListIndex].result = result;
}
float dlDrawListMgr::GetDebugEntityGPUTime(int drawListIndex) const
{
	return s_DebugEntityGpuTimers[drawListIndex].result;
}

void dlDrawListMgr::StartDebugEntityTimer()
{
	if(pCurrExecutingList && s_DebugEntityGpuTimers[pCurrExecutingList->m_type].Enabled)
	{
		s_DebugEntityGpuTimers[pCurrExecutingList->m_type].timer.Start();
	}
}
void dlDrawListMgr::StopDebugEntityTimer()
{
	if(pCurrExecutingList && s_DebugEntityGpuTimers[pCurrExecutingList->m_type].Enabled)
	{
		float result = s_DebugEntityGpuTimers[pCurrExecutingList->m_type].timer.Stop();
		s_DebugEntityGpuTimers[pCurrExecutingList->m_type].result += result;
	}
}

void dlDrawListMgr::UpdateDebugEntityGPUCostMaxAndAverage()
{
	if(pCurrExecutingList && s_DebugEntityGpuTimers[pCurrExecutingList->m_type].Enabled)
	{
		if (s_DebugEntityGpuTimers[pCurrExecutingList->m_type].averageTime == 0.0f)
		{
			// This was just reset, so set it straight to the current time so it doesn't take
			// a second for it to "warm up".
			s_DebugEntityGpuTimers[pCurrExecutingList->m_type].averageTime = s_DebugEntityGpuTimers[pCurrExecutingList->m_type].result;
		}
		else
		{
			s_DebugEntityGpuTimers[pCurrExecutingList->m_type].averageTime = (s_DebugEntityGpuTimers[pCurrExecutingList->m_type].averageTime * AVERAGE_TIMEBAR_FACTOR) 
				+ (s_DebugEntityGpuTimers[pCurrExecutingList->m_type].result) * (1.0f - AVERAGE_TIMEBAR_FACTOR);
		}
		// set maxtime
		if ( s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTime < s_DebugEntityGpuTimers[pCurrExecutingList->m_type].result)
		{
			s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTime = s_DebugEntityGpuTimers[pCurrExecutingList->m_type].result;
			s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTimeCount = 0;
		}
		else
		{
			s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTimeCount++;
			if ( s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTimeCount >= 100)
			{
				s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTime	= s_DebugEntityGpuTimers[pCurrExecutingList->m_type].result;
				s_DebugEntityGpuTimers[pCurrExecutingList->m_type].maxTimeCount = 0;
			}
		}
	}


}
void dlDrawListMgr::SetDebugEntityGPUCostMode(eDebugEntityGpuCostMode mode)
{
	s_DebugEntityGPUCostMode = mode;
}
eDebugEntityGpuCostMode dlDrawListMgr::GetDebugEntityGPUCostMode()
{
	return s_DebugEntityGPUCostMode;
}
void dlDrawListMgr::ResetGPUCostTimers()
{
	for (int i = 0; i < GetDrawListTypeCount(); i++)
	{
		s_DebugEntityGpuTimers[i].averageTime = 0.0f;
		s_DebugEntityGpuTimers[i].maxTime = 0.0f;
		s_DebugEntityGpuTimers[i].maxTimeCount = 0;
	}
}
#endif
u32 dlDrawListMgr::GetDrawListEntityCount(int drawListIndex) const {
    return s_DrawListEntityCounts[drawListIndex];
}

void dlDrawListMgr::IncrementDrawListEntityCount() {
#if __BANK
    ++s_DrawListEntityCounts[ms_CurrDLType];
#endif
}

void dlDrawListMgr::ResetDrawListEntityCounts() {
    for(int i = 0; i < m_DrawListTypeCount; i++) {
        s_DrawListEntityCounts[i] = 0;
    }
}

u32 dlDrawListMgr::GetTotalIndices() const {
#if __BANK
	return m_statSaveTargetBuffer?m_statSaveTargetBuffer[0].totalIndices:0; 
#else
	return 0;
#endif 
}

u32 dlDrawListMgr::GetGBufIndices() const {
#if __BANK
	return m_statSaveTargetBuffer?m_statSaveTargetBuffer[1].indices:0; // Oh lord, forgive me for I have sinned.
#else
return 0;
#endif 
}
#endif // !__FINAL

#if RAGE_TIMEBARS
float dlDrawListMgr::GetTimebarUpdateBudget(int drawListIndex) const {
	return s_tbUpdateBudget[drawListIndex];
}

float dlDrawListMgr::GetTimebarRenderBudget(int drawListIndex) const {
	return s_tbRenderBudget[drawListIndex];
}
#endif // RAGE_TIMEBARS

#if !__FINAL
void dlDrawListMgr::DebugDraw(){
#if __BANK
	gDCBuffer->UpdateDebugDraw();
	DumpDebugData();
#endif // __BANK
}
#endif // !__FINAL

void * dlDrawListMgr::AddForcedCleanupCallback(void (*func)(void*), size_t userDataSize){
	dlDrawListInfo * info = pCurrBuildingList;
	Assert(info);
	dlForcedCleanupCallback * cb = (dlForcedCleanupCallback*)(gDCBuffer->AddDataBlock(NULL, (u32)(sizeof(dlForcedCleanupCallback)+16+userDataSize)));
	cb->next = info->m_ForcedCleanupCallbacks;
	cb->func = func;
	info->m_ForcedCleanupCallbacks = cb;
	return (void*)(((uptr)(cb+1)+15)&~15);
}

bool dlDrawListMgr::NewDrawList(eDrawListType type, DrawListAddress::Parameter offset){

#if __DEV
	static u32 lastFrameNumber = 0;
#endif // __DEV

	dlAssertf(m_IsDrawListCreationAllowed, "Trying to create a draw list of type %s outside the main draw list creation", s_DrawListNames[type].c_str());

#if __ASSERT
	if( pCurrBuildingList != NULL )
	{
		Warningf("drawlist %s not closed while trying to create drawlist %s", s_DrawListNames[pCurrBuildingList->m_type].c_str(),
																				s_DrawListNames[type].c_str());
		dlAssertf(pCurrBuildingList == NULL, "Attempting to nest NewDrawList calls.");
	}
#endif // __ASSERT
	m_DrawListPoolCs.Lock();
	dlDrawListInfo &newList = *m_DrawListPool.New();
	m_DrawListPoolCs.Unlock();		

	newList.m_offset = offset;
	newList.m_type = type;
	newList.m_Id = ++m_drawListIdx;
	newList.m_state = DLS_BUILDING;
	newList.m_listEndIdx = 0;
	newList.m_DepInfoIndex = -1;
	newList.m_ForcedCleanupCallbacks = NULL;

#if __DEV
	newList.m_FrameNumber = m_FrameNumber;

	if (lastFrameNumber != m_FrameNumber) {
		lastFrameNumber = m_FrameNumber;
//		Displayf("Now creating lists with frame number %d", lastFrameNumber);
	}
	newList.m_StrIndicesCacheIndex = m_streamingIndicesBufferIndex;
#endif // __DEV

#if ENABLE_PIX_TAGGING || __BANK // Even if ENABLE_PIX_TAGGING is off, we need the list name for the bank-only code below.
	char listName[64];
	formatf(listName, "%s %d", s_DrawListNames[type].c_str(), GetUpdateThreadFrameNumber());
	PIXBegin(0, listName);
#endif // !__FINAL

#if __BANK
	if (bShowFrameNumbers)
	{
	#if __PPU
		snStartMarker(type&31, listName);  
	#endif
	}
#endif // __BANK

	pCurrBuildingList = &newList;
	//pCurrBuildingList = &m_drawLists.Top();

	dlAssert(newList.m_Id == pCurrBuildingList->m_Id);		// check we've a ptr to the right info...

	//Add compute batcher for this drawlist.
	DLC(dlComputeShaderBatch, ());

	return(true);
}

/** PURPOSE: Terminate the current draw list.
 *  This will signal the render thread that this draw list is ready for consumption.
 *
 *  PARAM:
 *    listEndIdx - The command index of the dlCmdEndDrawList command. This will be used in
 *                 __DEV builds for verification.
 */
void dlDrawListMgr::EndDrawList(u32 listEndIdx){

	dlAssert(pCurrBuildingList);

	pCurrBuildingList->m_state = DLS_BUILT;
	pCurrBuildingList->m_listEndIdx = listEndIdx;

#if __DEV
	if (bCheckListsOnceBuilt){
		dlAssert(CheckBuildListIntegrity());
	}

	ms_instructionCount[ms_CurrDLType] += dlCmdBase::GetCounter();
	dlCmdBase::ResetCounter();		// reset instruction counter
#endif //__DEV

#if __BANK
	if (bShowFrameNumbers)
	{
	#if __PPU
		snStopMarker(pCurrBuildingList->m_type&31);  
	#endif
	}
#endif // __BANK

#if !__FINAL
	PIXEnd();
#endif // !__FINAL

	// Submit the dependency jobs that we set up during this draw list
#if !__TOOL
	if(SubmitPendingDependencies())
	{
		pCurrBuildingList->m_DepInfoIndex = m_dependencyInfoIndex++;
	}
#endif // !__TOOL

	// As soon as it's built, push it onto the queue so the render thread can go for it.
	GetBuildDrawListQ().Push(pCurrBuildingList);

	pCurrBuildingList = NULL;

#if __BANK
	utimer_t timeTaken = sysTimer::GetTicks() - ms_drawListCreationStartTime;
	ms_drawListCreationStartTime = 0;

	u32 frameIndex = m_UpdateThreadFrameNumber & 1;

	ms_drawListCreationTiming[frameIndex][ms_CurrDLType] += (float) timeTaken * sysTimerConsts::TicksToMilliseconds;
#endif // __BANK
}

/** PURPOSE: Terminate the current draw list. This function must be called while a draw list
 *  is currently being created. This function will first add a dlCmdEndDrawList command to the
 *  command draw list, then notify the render thread that this draw list is ready for consumption.
 */
void dlDrawListMgr::EndCurrentDrawList() {
	dlAssert(pCurrBuildingList);		// No list being created?
	
	ASSERT_ONLY(m_IsEndingDrawList = true;)

	dlCmdBase *endDrawCmd;
	GET_DLC(endDrawCmd, dlCmdEndDrawList, ());
	EndDrawList(endDrawCmd->GetCommandIdx());

	ASSERT_ONLY(m_IsEndingDrawList = false;)

	dlComputeShaderBatch::EndRender();
}

void dlDrawListMgr::TerminateDrawLists() {
	Assert(m_UpdateThreadState == UPDATE_CREATING_DRAWLISTS);
	GetBuildDrawListQ().Push(NULL);
	m_UpdateThreadState = UPDATE_WAITING_FOR_RT;

#if __DEV
	m_FrameNumber++;
#endif // __DEV
}

void dlDrawListMgr::BeginDrawListCreation() {
#if __ASSERT
	m_IsDrawListCreationAllowed = true;
#endif // __ASSERT

	Assert(m_UpdateThreadState == UPDATE_PRE_DRAWLIST || m_UpdateThreadState == UPDATE_INITIALIZED);
	m_UpdateThreadState = UPDATE_CREATING_DRAWLISTS;

	m_nextJoinDrawListIndex = 0;

	gRenderThreadInterface.ClearFlushedState();

#if __BANK
	u32 frameIndex = m_UpdateThreadFrameNumber & 1;
	sysMemSet(ms_drawListCreationTiming[frameIndex].GetElements(), 0, m_DrawListTypeCount * sizeof(float));
#endif // __BANK
}


void dlDrawListMgr::EndDrawListCreation() {
#if __ASSERT
	m_IsDrawListCreationAllowed = false;
#endif // __ASSERT

	JoinAllDrawlistDependencies();

	TerminateDrawLists();

	FlipBufferIdx();

	gRenderThreadInterface.FlipUpdateBuffer();

#if !__FINAL || __FINAL_LOGGING
	m_UpdateThreadFrameNumber++;
#endif // !__FINAL
}

#if MULTIPLE_RENDER_THREADS
struct subrenderJob
{
	dlDrawListInfo *input;
#if MULTIPLE_RENDER_THREADS_PRESERVE_DL_NAMES
	const char* drawListName; // Must not be on the stack; passed to timebars!
#endif // MULTIPLE_RENDER_THREADS_PRESERVE_DL_NAMES
#if DRAWABLE_STATS
	drawableStats stats;
	int dliType;
#endif // DRAWABLE_STATS

	grcContextCommandList *output;
	sysIpcEvent *finishedEvent;
	volatile u32 grabbed;
#if GRCGFXCONTEXT_CONTROL_SUPPORTED
	bool isDone;
	bool usedCapacityReleased;
	u32 prevPending;                // does not count jobs that are being delayed to execute on CPU_MAIN
	grcGfxContextControl control;
#endif
};

#define SUB_JOB_LIST_SIZE		(128)
#define	SUBRENDER_THREAD_STACK	(RENDER_THREAD_STACK)

static subrenderJob s_SubrenderJobs[SUB_JOB_LIST_SIZE];
static u32 s_SubrenderJobsAlloced;

#if GRCGFXCONTEXT_CONTROL_SUPPORTED
// The throttling of segment allocations via grcGfxContextControl needs some
// explanation.
//
// The basic reason for it is to prevent deadlocks with multithreaded rendering.
// Say we have two subrender threads, each processing a subrender job.  The
// order that the jobs will be submitted to the gpu is predetermined.  If due to
// unlucky timing, the thread working on the later job ends up allocating all
// the memory, the first thread will block.  Since the second thread cannot
// submit to the gpu until the first has, the system deadlocks.
//
// A simple solution to this problem would be for each thread to have its own
// pool of segments to allocate from.  That prevents the deadlock, but it also
// scales poorly as the number of threads is increased, and requires a lot more
// memory.  Even if that memory is available, we can better use it to prevent
// stalling the gpu, by allowing contexts that are just about to be submitted to
// the gpu to allocate more.
//
// This code has gone through a few iterations, and it probably simplest to
// explain the current version by explaining the history on how it evolved.
// Initially, everythread just allocated as much as it wanted, as fast as it
// could, and this was leading to the deadlocks described above.
//
// After that initial version, the concept of throttling how much each thread
// could allocate based on its position in the queue was introduced.  The thread
// creating data that will first be seen by the gpu was allowed to allocate up
// to half, the next thread, a quater, then an eigth, etc.
//
// At that stage, the code was then correct, and could no longer deadlock, but
// there was some performance issues.  The main issue was that a thread that was
// slow on the cpu, but generated very little commands for the gpu would hold
// everything up.  So a big improvement to address that was to allow the
// redistribution of budgets as soon as thread completed.  Once a thread was
// done, anything not used in its budget, could be added to the budgets for
// later threads.
//
// That was much improved, but still occasionally had threads stalling due to
// the budgets, when there was otherwise segments still available to be
// allocated.  The current implementation is just a better tuned version.  The
// fixed function of half, quater, eigth, etc has been replaced with a lookup
// table function to give a bit more even distribution.  Also a hybrid approach
// with the original free-for-all implementation has been added.  Now there is
// some additional segments that any thread can race to allocate.  This allows
// very close to the maximum performance (where no thread stalls if there are
// segments available), and still prevents any deadlocks because each thread is
// garunteed a certain number of segments.
//
enum { SEGMENTS_RESERVED_FOR_PRIMARY_RENDER_THREAD = 2 };
static const float FREE_FOR_ALL = 0.25f;
static sysCriticalSectionToken s_SubrenderJobControlUpdate;
static u32 s_UnusedThrottle;
static u32 s_SubrenderJobsPending;
static u32 s_SubrenderJobsRedistributed;
static u32 s_SubrenderJobBudgets[4];

static inline u32 GetJobBudget(u32 queuePosition)
{
	return queuePosition<NELEM(s_SubrenderJobBudgets) ? s_SubrenderJobBudgets[queuePosition] : 0;
}

static void InitJobBudgets(u32 totalSegmentsForSubJobs)
{
	u32 freeForAll = (u32)((float)totalSegmentsForSubJobs*FREE_FOR_ALL);
	if (freeForAll > totalSegmentsForSubJobs) {
		freeForAll = 0;
	}
	grcGfxContextControl::setGlobalFreeForAll(freeForAll);
	totalSegmentsForSubJobs -= freeForAll;

	sysMemSet(s_SubrenderJobBudgets, 0, sizeof(s_SubrenderJobBudgets));
	u32 b0 = totalSegmentsForSubJobs/3;
	u32 b123 = totalSegmentsForSubJobs*2/9;
	u32 tot = b0+3*b123;
	u32 rem = totalSegmentsForSubJobs-tot;
	u32 b1  = b123+rem;
	u32 b23 = b123;
	s_SubrenderJobBudgets[0] = b0;
	s_SubrenderJobBudgets[1] = b1;
	s_SubrenderJobBudgets[2] = b23;
	s_SubrenderJobBudgets[3] = b23;
}

class SubrenderJobControlUpdateLock
{
public:
	SubrenderJobControlUpdateLock()
		: m_lock(s_SubrenderJobControlUpdate)
	{
	}

#if __ASSERT
	~SubrenderJobControlUpdateLock()
	{
		const u32 numJobs = s_SubrenderJobsAlloced;
		const u32 totalSegments = grcGfxContext::getTotalSegmentCount() - SEGMENTS_RESERVED_FOR_PRIMARY_RENDER_THREAD;

#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
#define CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB(JOB)  if (!((JOB)->finishedEvent)) continue; else (void)0
#else
#define CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB(JOB)  (void)0
#endif

		// The primary goal of the budgetting is to prevent later (in the order
		// the gpu will see them) jobs consuming all the available memory.
		// Regardless of the algorithm used for budgetting, the check that
		// maxPending+laterSegments<=totalSegments must never fail, else
		// there is a threading race condition and the code could deadlock.
		u32 idx;
		u32 laterSegments = s_UnusedThrottle;
		for (idx=numJobs; idx-->0; )
		{
			const subrenderJob *const j = s_SubrenderJobs+idx;
			const grcGfxContextControl::States c = j->control.readStates();
			CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB(j);
			if (j->isDone)
			{
				if (j->usedCapacityReleased)
				{
					for (; idx-->0; )
					{
						const subrenderJob *const j2 = s_SubrenderJobs+idx;
						CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB(j2);
						FatalAssert(j2->isDone);
						FatalAssert(j2->usedCapacityReleased);
					}
					break;
				}
				laterSegments += c.getCurrPending();
			}
			else
			{
				// Ensure that the total segment budget is not exceeded.
				const u32 maxPending = c.getMaxPending();
				FatalAssert(maxPending+laterSegments <= totalSegments);
				laterSegments += maxPending;
			}
		}

		// These following checks just validate that the budgetting algorithm as
		// currently implemented is working as expected.  Changes to the code
		// may require changes to these checks.

		// Skip over jobs that have the used capacity already released.
		for (idx=0; idx<numJobs; ++idx)
		{
			const subrenderJob *const j = s_SubrenderJobs+idx;
			CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB(j);
			if (!j->usedCapacityReleased)
				break;
		}
		const u32 completed = idx;

		// Check remaining jobs are budgetted as expected.
		u32 prevPendingJobs = 0;
		for (; idx<numJobs; ++idx)
		{
			const subrenderJob *const j = s_SubrenderJobs+idx;
			const grcGfxContextControl::States c = j->control.readStates();
			CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB(j);
			if (!j->isDone)
			{
				FatalAssert(j->prevPending == prevPendingJobs);
				const u32 maxPending = c.getMaxPending();
				const u32 idxMaxPending = GetJobBudget(idx-completed);
				const u32 prevJobsMaxPending = GetJobBudget(prevPendingJobs);
				const u32 freeForAll = c.getFreeForAll();
				FatalAssert(maxPending >= idxMaxPending);
				Assert(maxPending <= prevJobsMaxPending+freeForAll);
				++prevPendingJobs;
			}
			else
			{
				FatalAssert(!j->usedCapacityReleased);
			}
		}
#undef CONTINUE_IF_PRIMARY_RENDER_THREAD_JOB
	}
#endif

private:
	sysCriticalSection m_lock;
};

static void UpdateSubrenderJobMaxPending(subrenderJob *j, u32 *spareCapacity)
{
	FatalAssert(j->finishedEvent);
	const u32 maxPendingBasedOnPrevJobsPending = GetJobBudget(j->prevPending);
	grcGfxContextControl::States cstate = j->control.readStates();
	for (;;)
	{
		const u32 currMaxPending = cstate.getMaxPending();
		const u32 freeForAll = cstate.getFreeForAll();
		const u32 currBudgetedMaxPending = currMaxPending-freeForAll;
		Assertf(currBudgetedMaxPending <= maxPendingBasedOnPrevJobsPending, "%d current budgeted pending job over %d max pending", currBudgetedMaxPending, maxPendingBasedOnPrevJobsPending);
		const u32 updatedBudgetedMaxPending = Min(currBudgetedMaxPending+*spareCapacity, maxPendingBasedOnPrevJobsPending);
		const u32 budgetIncrease = updatedBudgetedMaxPending-currBudgetedMaxPending;
		const u32 updatedMaxPending = Max(updatedBudgetedMaxPending, currMaxPending);
		const u32 updatedFreeForAll = freeForAll>budgetIncrease ? freeForAll-budgetIncrease : 0;
		grcGfxContextControl::States update = cstate;
		update.setMaxPending(updatedMaxPending);
		update.setFreeForAll(updatedFreeForAll);
		const grcGfxContextControl::States mem = j->control.casStates(cstate, update);
		if (cstate == mem)
		{
			*spareCapacity -= updatedBudgetedMaxPending-currBudgetedMaxPending;
			u32 globalFreeForAll;
			u32 additionalGlobalFreeForAll = freeForAll-updatedFreeForAll;
			if (additionalGlobalFreeForAll)
				globalFreeForAll = grcGfxContextControl::addGlobalFreeForAllRetNew(additionalGlobalFreeForAll);
			else
				globalFreeForAll = grcGfxContextControl::getGlobalFreeForAll();
			if ((updatedMaxPending>currMaxPending || globalFreeForAll) && cstate.getStalledThread())
				j->control.wakeStalledThread();
			break;
		}
		cstate = mem;
	}
}

static void SubrenderJobDone(subrenderJob *job)
{
	SubrenderJobControlUpdateLock lock;

	// All jobs from the start of the queue that have not yet had
	// the budget for their allocated segments redistributed, can
	// have that done now.  While their command buffers have not
	// necissarily yet been kicked to the gpu, it is still safe to
	// redistribute since no previous jobs are still pending.
	u32 spareCapacity = s_UnusedThrottle;
	job->isDone = true;
	subrenderJob *const j2End = s_SubrenderJobs+s_SubrenderJobsAlloced;
	subrenderJob *j2;
	#if __ASSERT
		// Check that the jobs we are skipping over (up to s_SubrenderJobsRedistributed) are truely finished.
		for (j2=s_SubrenderJobs; j2<s_SubrenderJobs+s_SubrenderJobsRedistributed; ++j2)
		{
			FatalAssert((j2->isDone && j2->usedCapacityReleased)
				|| (MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD && !j2->finishedEvent));
		}
	#endif
	for (j2=s_SubrenderJobs+s_SubrenderJobsRedistributed; j2<j2End; ++j2)
	{
		if (!j2->isDone
			#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
				&& j2->finishedEvent
			#endif
			)
		{
			// We found an unfinished sub render thread job - can't re-allocate used capacity for
			// any finished jobs until this one is completed, so we're done here. Break out.
			break;
		}

		if (!j2->usedCapacityReleased)
		{
			const grcGfxContextControl::States cstate = j2->control.readStates();
			const u32 freeForAll = cstate.getFreeForAll();
			const u32 currPending = cstate.getCurrPending();
			FatalAssert(currPending >= freeForAll);
			spareCapacity += currPending-freeForAll;
			grcGfxContextControl::addGlobalFreeForAll(freeForAll);
			j2->usedCapacityReleased = true;
		}
	}

	// We stopped iteration of the first loop at either the first
	// still pending job, or the end of the entire queue.  Save that
	// index off so that next job that completes doesn't have to
	// scan them all again.
	s_SubrenderJobsRedistributed = j2-s_SubrenderJobs;

	// If we stopped before the job we just completed, then loop up
	// to there redistributing throttle limits.
	FatalAssert(j2==j2End || !j2->isDone);
	for (; j2<job; ++j2)
	{
		if (!j2->isDone
		  #if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
			&& j2->finishedEvent
		  #endif
		)
		{
			UpdateSubrenderJobMaxPending(j2, &spareCapacity);
		}
	}

	// For the remaining jobs after the job that just completed,
	// unused throttle from just completed job can also be
	// redistributed.  Also need to decrement the prevPending
	// counters.
	spareCapacity += job->control.readStates().getMaxPending()-job->control.readStates().getCurrPending();
	for (; j2<j2End; ++j2)
	{
		if (!j2->isDone
		  #if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
			&& j2->finishedEvent
		  #endif
		)
		{
			FatalAssert(j2->prevPending);
			--(j2->prevPending);
			UpdateSubrenderJobMaxPending(j2, &spareCapacity);
		}
	}

	s_UnusedThrottle = spareCapacity;
	--s_SubrenderJobsPending;
}

#endif // GRCGFXCONTEXT_CONTROL_SUPPORTED


struct subrenderInfo 
{
	sysIpcThreadId m_Thread;
	char m_ThreadName[16];
	sysIpcEvent m_Finished;				// set by worker thread so that parent thread knows command list is complete.
	sysMessageQueue<subrenderJob*, SUB_JOB_LIST_SIZE> m_RenderQ;
};

static subrenderInfo s_subrenderInfo[MULTIPLE_RENDER_THREADS];
volatile u32 s_completedLists;
static sysIpcEvent s_subJobFinishedEvents[SUB_JOB_LIST_SIZE];



static void SubRenderThread(void *index)
{
	sysThreadType::AddCurrentThreadType(sysThreadType::THREAD_TYPE_RENDER);

	subrenderInfo &that = s_subrenderInfo[(size_t)index];
	g_RenderThreadIndex = (u8) (size_t) index + MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD; // Main thread uses data index 0 if executing DLs.
	g_IsSubRenderThread = true;

	GRCDEVICE.CreateContextForThread();

	PF_INIT_TIMEBARS(that.m_ThreadName, 1024, 0.0f);

	for (;;)
	{
		bool firstJob = true;

#if ENABLE_PIX_TAGGING && RSG_DURANGO
		u32 brightness = ((size_t)index) * 48;
		PIXBeginCN( 0, 0xff000000 | (255-brightness)<<16, "Subrender:%d", (size_t)index);
#endif // ENABLE_PIX_TAGGING && RSG_DURANGO

		for (;;)
		{
			// We shove one NULL per subrender thread into the message queue so they will all quit.
			subrenderJob *job = that.m_RenderQ.Pop();
			if (!job)
				break;

			// Try to acquire the job.  Job may have been pushed into multiple thread's
			// queues, so the first thread to go through here grabs the job.
			if (sysInterlockedOr(&job->grabbed, 1) != 0)
				continue;

			dlDrawListInfo *drawListInfo = pCurrExecutingList = job->input;
			grcGfxContextProfiler_JobBegin((u8)(drawListInfo->m_type));

			// Assign per-thread stats buffer pointer
			#if DRAWABLE_STATS
				sysMemSet(&job->stats, 0, sizeof(drawableStats));
				GRCDEVICE.SetFetchStatsBuffer(&job->stats);
			#endif // DRAWABLE_STATS

			DIAG_CONTEXT_MESSAGE(s_DrawListNames[drawListInfo->m_type]);

			// Begin our local context
			GRCDEVICE.BeginContext(GRCGFXCONTEXT_CONTROL_SUPPORTED_ONLY(&job->control));

			if (firstJob)
			{
				PF_FRAMEINIT_TIMEBARS(gRenderThreadInterface.GetRenderThreadFrameNumber());
				firstJob = false;
			}

			RAGE_TIMEBARS_ONLY(DrawListGpuTimeStart(drawListInfo->m_type);)

			PF_PUSH_TIMEBAR(s_DrawListNames[drawListInfo->m_type]);

			// Default to NULL. All drawlists must ensure that they set up their own viewport.
			grcViewport::SetCurrent(NULL);

			// Same thing with stateblocks.
			grcStateBlock::MakeDirty();
			grcStateBlock::Default();
			grmMatrixSet::MakeDirty();
			grcEffect::ClearCachedState();
			dlDrawListMgr::ResetHighestLOD(true);
#if RSG_ORBIS
			GRCDEVICE.InitSubThread();
			grcEffect::ResetGlobalTextures();
#endif

			// Execute the buffer
			gDCBuffer->ExecuteListAtAddr(drawListInfo->m_offset, drawListInfo->m_listEndIdx, drawListInfo->m_Id);

			PF_POP_TIMEBAR();

			RAGE_TIMEBARS_ONLY(DrawListGpuTimeStop(drawListInfo->m_type);)

			// Capture the buffer
			job->output = GRCDEVICE.EndContext();
			sysInterlockedIncrement(&s_completedLists);

			// Clean it up
			gDrawListMgr->CleanupDrawList(drawListInfo);

			grcGfxContextProfiler_JobEnd();

#if GRCGFXCONTEXT_CONTROL_SUPPORTED
			SubrenderJobDone(job);
#endif

			// Say the job has been completed.
			sysIpcSetEvent(*job->finishedEvent);
		}

		// Tell parent it's ready.
		sysIpcSetEvent(that.m_Finished);

#if ENABLE_PIX_TAGGING && RSG_DURANGO
		PIXEnd();
#endif // ENABLE_PIX_TAGGING && RSG_DURANGO
	}
}
#endif

void dlDrawListMgr::CleanupDrawList(dlDrawListInfo *drawListInfo)
{
	drawListInfo->m_state = DLS_EXECUTED;
	pCurrExecutingList = NULL;

	ResetDrawListInfo(*drawListInfo);

	m_DrawListPoolCs.Lock();
	m_DrawListPool.Delete(drawListInfo);	// Won't call the constructor, which we don't have.
	m_DrawListPoolCs.Unlock();		
}

// check if any of the unprocessed draw lists begin at an address between the two given
void dlDrawListMgr::ExecuteAndRemoveAll(void){
#if MULTIPLE_RENDER_THREADS
	static bool subrendersStarted;
	if (!subrendersStarted)
	{
		for (int i=0; i<MULTIPLE_RENDER_THREADS; i++) 
		{
			int threadIdx = i;

			s_subrenderInfo[i].m_Finished = sysIpcCreateEvent();
			formatf(s_subrenderInfo[i].m_ThreadName,"Subrender:%d",i);

			// 11/7/14 - cthomas - sub-render threads on consoles now have lowest priority possible (idle prio), 
			// and no thread affinity (pass -1 to sysIpcCreateThread), so they can roam onto any core that has 
			// free time available.
			rage::sysIpcPriority subRenderThreadPrio = PRIO_IDLE;
			s_subrenderInfo[i].m_Thread = sysIpcCreateThread(SubRenderThread, (void*)(size_t)threadIdx, SUBRENDER_THREAD_STACK, subRenderThreadPrio, s_subrenderInfo[i].m_ThreadName, -1);
		}

		for(int i=0; i<SUB_JOB_LIST_SIZE; i++)
		{
			s_subJobFinishedEvents[i] = sysIpcCreateEvent();
		}
		subrendersStarted = true;
	}
	s_completedLists = 0;
#endif

	/// DrawListAddress startAddr = gDCBuffer->GetReadBufferStartAddr(DPT_SIMPLE_RING_BUFFER);
	/// DrawListAddress endAddr = gDCBuffer->GetReadBufferEndAddr(DPT_SIMPLE_RING_BUFFER);

#if __BANK
	if( true == ms_ShaderReloadRequested )	
	{
#if __PPU
		grcFenceHandle fence = GRCDEVICE.InsertFence();
		GRCDEVICE.BlockOnFence(fence);
#endif // __PPU
#if __ASSERT
		grcEffect::SetAllowShaderLoading(true);
#endif // __ASSERT

		grcEffect::ReloadAll();
		ms_ShaderReloadRequested = false;

#if __ASSERT
		grcEffect::SetAllowShaderLoading(false);
#endif // __ASSERT
	}

	u32 frameNumberIndex = gRenderThreadInterface.GetRenderThreadFrameNumber() & 1;
	sysMemSet(ms_drawListExecutionTiming[frameNumberIndex].GetElements(), 0, m_DrawListTypeCount * sizeof(float));
#if __DEV
	sysMemSet(&ms_instructionCount[0], 0, m_DrawListTypeCount * sizeof(int));
#endif // __DEV
#endif // __BANK

	dlDrawListInfo *drawListInfo = NULL;

	PF_BEGIN_GPUBAR(gRenderThreadInterface.GetRenderThreadFrameNumber());

#if GRCGFXCONTEXT_CONTROL_SUPPORTED
	// -n so there is always something free for main render thread between
	// deferred contexts.
	const u32 totalSegmentsForSubJobs = grcGfxContext::getTotalSegmentCount() - SEGMENTS_RESERVED_FOR_PRIMARY_RENDER_THREAD;
	InitJobBudgets(totalSegmentsForSubJobs);
	grcGfxContext::setMaxMaxPending(GetJobBudget(0));
#endif

#if MULTIPLE_RENDER_THREADS
	GRCDEVICE.BeginCommandList();
#endif

	// Grab all draw lists in turn, and execute them on this thread or a sub-render thread
	while (true) {
		if (!PIXIsGPUCapturing())
		{
			PF_START_TIMEBAR_IDLE("Wait for next DL");
		}

		drawListInfo = GetRenderDrawListQ().Pop();

		if (!drawListInfo) {
#if MULTIPLE_RENDER_THREADS
			if (!PIXIsGPUCapturing()) {
				PF_START_TIMEBAR_IDLE("Found list terminator");
			}
#endif
			DEBUGF_FENCES("FOUND TERMINATOR");
			// We terminate the list by pushing a NULL pointer.
			break;
		}
#if MULTIPLE_RENDER_THREADS
		else {
			if (!PIXIsGPUCapturing()) {
				PF_START_TIMEBAR_IDLE("Prime fence handle");
			}
		}
#endif

		// If we're starting the very first draw list, mark down that
		// we're actually processing the draw list this frame and will
		// insert a GPU fence eventually.
#if !RSG_PC
		gDrawListMgr->PrimeEndFenceHandle();
#endif

#if __BANK
		utimer_t timeStart = sysTimer::GetTicks();
#endif // __BANK

		drawListInfo->m_state = DLS_EXECUTING;
		pCurrExecutingList = drawListInfo;
		int dliType = drawListInfo->m_type;		// cache this because the structure might be gone by time we get to exec timing code

#if !RAGE_TIMEBARS
		PIXBeginC(1, 0x00FFFF,s_DrawListNames[dliType]);
#endif
#if !__FINAL
#if __PPU && DRAWABLESPU_STATS
		sysMemSet(&s_DrawListSpuDrawableStats[s_StatsWriteIdx][dliType], 0, sizeof(spuDrawableStats));
		GRCDEVICE.SetFetchStatsBuffer(&s_DrawListSpuDrawableStats[s_StatsWriteIdx][dliType]);
#elif DRAWABLE_STATS
		sysMemSet(&s_DrawListDrawableStats[s_StatsWriteIdx][dliType], 0, sizeof(drawableStats));
		GRCDEVICE.SetFetchStatsBuffer(&s_DrawListDrawableStats[s_StatsWriteIdx][dliType]);
#endif
#endif

#if !MULTIPLE_RENDER_THREADS
		PF_PUSH_TIMEBAR_BUDGETED(s_DrawListNames[dliType], s_tbRenderBudget[dliType]);
#endif

		RAGETRACE_POP();
		_RAGETRACE_PUSH(cpuDrawListTrace[dliType]);
		_RAGETRACE_GPU_PUSH(gpuDrawListTrace[dliType]);

		GRCDBG_PUSH(s_DrawListNames[dliType]);

#if __BANK
		if (bShowFrameNumbers)
		{
			char listName[64];
			formatf(listName, "%s %d", s_DrawListNames[dliType].c_str(), GetUpdateThreadFrameNumber());
			PIXBegin(0, listName);
#if __PPU
			snStartMarker(dliType&31, listName);  
#endif
		}
#endif // __BANK

		bool ranOnMainThread = true;

		// Ensure any dependencies (such as CopyOffMatrixSet jobs) for this
		// drawlist are done
		if(pCurrExecutingList->m_DepInfoIndex >= 0)
		{
			gDrawListMgr->JoinDrawlistDependencies(pCurrExecutingList->m_DepInfoIndex);
		}

#if MULTIPLE_RENDER_THREADS
		u8 cpuMask = s_DrawListCPUs[dliType];
		if (cpuMask != SUBCPUS_SYNC_MAIN)
		{
			pCurrExecutingList = NULL;

			subrenderJob *job;
			{
				GRCGFXCONTEXT_CONTROL_SUPPORTED_ONLY(SubrenderJobControlUpdateLock lock;)

				job = s_SubrenderJobs+s_SubrenderJobsAlloced;
				job->input   = drawListInfo;
				job->output  = NULL;
				job->grabbed = 0;
				// other stuff needs to inherit here?
			#if MULTIPLE_RENDER_THREADS_PRESERVE_DL_NAMES
				job->drawListName = gDrawListMgr->GetDrawListName(drawListInfo->m_type);
			#endif // MULTIPLE_RENDER_THREADS_PRESERVE_DL_NAMES
			#if DRAWABLE_STATS
				job->dliType = dliType;
			#endif
			#if GRCGFXCONTEXT_CONTROL_SUPPORTED
				job->isDone = false;
				job->usedCapacityReleased = false;
				job->control.init();
				job->control.setSequenceNumber(GRCDEVICE.GetAndIncContextSequenceNumber());
			#endif

				if (!PIXIsGPUCapturing())
				{
					PF_START_TIMEBAR_IDLE("Pushing subrender job");
				}

			#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
				if(s_DrawListCPUs[dliType] == SUBCPUS_MAIN)
				{
					job->finishedEvent = NULL;
				#if GRCGFXCONTEXT_CONTROL_SUPPORTED
					job->control.setInitialMaxMaxPending();
					s_UnusedThrottle += GetJobBudget(s_SubrenderJobsAlloced);
				#endif
				}
				else
			#endif // MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
				{
				#if GRCGFXCONTEXT_CONTROL_SUPPORTED
					job->prevPending = s_SubrenderJobsPending;
					// Initialize the max pending value.  There is always space
					// for GetJobBudget(s_SubrenderJobsAlloced).  Ideally this
					// would be set to GetJobBudget(s_SubrenderJobsPending), but
					// that depends on how much space previously completed (but
					// not yet submitted to the gpu) jobs have allocated.
					u32 maxPendingBasedOnJobsAlloced = GetJobBudget(s_SubrenderJobsAlloced);
					u32 maxPendingBasedOnJobsPending = GetJobBudget(s_SubrenderJobsPending);
					++s_SubrenderJobsPending;
					u32 maxPending = Min(maxPendingBasedOnJobsAlloced+s_UnusedThrottle, maxPendingBasedOnJobsPending);
					job->control.setInitialMaxPending(maxPending);
					s_UnusedThrottle -= maxPending - maxPendingBasedOnJobsAlloced;
				#endif

				#if ENABLE_SINGLE_THREADED_RENDERING_DEBUG
					cpuMask = s_ForceSubRenderThread0 ? 1 : cpuMask;
				#endif

					job->finishedEvent = &s_subJobFinishedEvents[s_SubrenderJobsAlloced];
					for (u8 cpu=0; cpu<MULTIPLE_RENDER_THREADS; ++cpu)
					{
						if ((cpuMask&(1<<cpu)) != 0)
						{
							s_subrenderInfo[cpu].m_RenderQ.Push(job);
						}
					}
				}

				++s_SubrenderJobsAlloced;
			}

			ranOnMainThread = false;
		}
		else
		{
			if(s_SubrenderJobsAlloced)
			{
				// Signal end of drawlist submission for all sub-render threads
				for (int i=0; i<MULTIPLE_RENDER_THREADS; i++)
				{
					s_subrenderInfo[i].m_RenderQ.Push(NULL);
				}

				unsigned nextCommandList = 0;

				while(nextCommandList < s_SubrenderJobsAlloced)
				{
				#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
					// Is this job/drawlist to be executed on the main thread?
					if(s_SubrenderJobs[nextCommandList].finishedEvent == NULL)
					{
						// End the last command list.
						GRCDEVICE.EndCommandList();

						// Set the currently executing drawlist.
						dlDrawListInfo *drawListInfo = pCurrExecutingList = s_SubrenderJobs[nextCommandList].input;

					#if __D3D11
						GRCDEVICE.ResetContext();
					#endif // __D3D11

						// Pretend to be a sub thread.
						g_IsSubRenderThread = true;
						g_RenderThreadIndex = 0;

						RAGE_TIMEBARS_ONLY(DrawListGpuTimeStart(drawListInfo->m_type);)

						PF_PUSH_TIMEBAR(s_DrawListNames[drawListInfo->m_type]);

						grcStateBlock::MakeDirty();
						grcStateBlock::Default();
						grmMatrixSet::MakeDirty();
						grcEffect::ClearCachedState();
						dlDrawListMgr::ResetHighestLOD(true);

						// Execute the buffer
						gDCBuffer->ExecuteListAtAddr(drawListInfo->m_offset, drawListInfo->m_listEndIdx, drawListInfo->m_Id);

						PF_POP_TIMEBAR();

						RAGE_TIMEBARS_ONLY(DrawListGpuTimeStop(drawListInfo->m_type);)

						// Clean it up
						gDrawListMgr->CleanupDrawList(drawListInfo);

						// Cease pretending to be a sub-thread.
						g_IsSubRenderThread = false;
						g_RenderThreadIndex = 0;

						// Begin command lists again.
						GRCDEVICE.BeginCommandList();
					}
					else
				#endif // MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
					{
						subrenderJob *job = s_SubrenderJobs+nextCommandList;

					#if GRCGFXCONTEXT_CONTROL_SUPPORTED
						// If the context is still generating command buffers,
						// then since it is the next context to execute, allow
						// it to start submitting as it goes.  This will allow
						// extremely large contexts to be built, as segments
						// that have been executed by the GPU can then be
						// recycled.
						job->control.allowPartialSubmit();

						#if __ASSERT
						{
							SubrenderJobControlUpdateLock lock;
							// With free-for-all allocations, the job's max pending value may be
							// larger than GetJobBudget(0), but it should never be any less.
							FatalAssert(job->isDone || job->control.getMaxPending()>=GetJobBudget(0));
						}
						#endif
					#endif

						// Wait for the next job to be completed.
						sysIpcWaitEvent(*job->finishedEvent);

					#if MULTIPLE_RENDER_THREADS_PRINT_DL_NAMES
						grcDisplayf("dlDrawListMgr::ExecuteAndRemoveAll()...Before END %s\n", job->drawListName);
					#endif // MULTIPLE_RENDER_THREADS_PRINT_DL_NAMES

					// Combine stats from this sub-render
					#if DRAWABLE_STATS
						s_DrawListDrawableStats[s_StatsWriteIdx][job->dliType] += job->stats;
					#endif // DRAWABLE_STATS

					#if MULTIPLE_RENDER_THREAD_MARK_DL_EXECUTION
						PF_PUSH_TIMEBAR(job->drawListName);
					#endif // MULTIPLE_RENDER_THREAD_MARK_DL_EXECUTION

					// Execute the command list built on the subrender thread
					#if RSG_ORBIS
						// Because we have now allowed the context to submit
						// segments as it is building on Orbis, don't call
						// GRCDEVICE::ExecuteCommandList, as we don't want
						// anything else to get mixed in between.

						// TODO: Can we just change ExecuteCommandList to do this instead ???
						grcContextCommandList *const cmdList = job->output;
						if (cmdList)
						{
							cmdList->submitPending(grcGfxContext::SUBMIT_FULL);
						}
					#else
						GRCDEVICE.ExecuteCommandList(job->output);
					#endif

					#if MULTIPLE_RENDER_THREAD_MARK_DL_EXECUTION
						PF_POP_TIMEBAR();
					#endif // MULTIPLE_RENDER_THREAD_MARK_DL_EXECUTION

					#if MULTIPLE_RENDER_THREADS_PRINT_DL_NAMES
						grcDisplayf("dlDrawListMgr::ExecuteAndRemoveAll()...After END %s\n", job->drawListName);
					#endif // MULTIPLE_RENDER_THREADS_PRINT_DL_NAMES

					}
					nextCommandList++;
				}
				s_SubrenderJobsAlloced = 0;
			#if GRCGFXCONTEXT_CONTROL_SUPPORTED
				s_UnusedThrottle = 0;
				s_SubrenderJobsRedistributed = 0;
			#endif
				s_completedLists = 0;

				// Wait for the subrender threads to finish their work.
				for (int i=0; i<MULTIPLE_RENDER_THREADS; i++)
				{
					sysIpcWaitEvent(s_subrenderInfo[i].m_Finished);
				}
			}
#else
		{
#endif

		#if MULTIPLE_RENDER_THREADS
			GRCDEVICE.EndCommandList();
		#endif

			RAGE_TIMEBARS_ONLY(DrawListGpuTimeStart(drawListInfo->m_type);)
		#if MULTIPLE_RENDER_THREADS
			PF_PUSH_TIMEBAR_BUDGETED(s_DrawListNames[dliType], s_tbRenderBudget[dliType]);
		#endif
			grcStateBlock::MakeDirty();
			grcStateBlock::Default();
			grmMatrixSet::MakeDirty();
			grcEffect::ClearCachedState();
			dlDrawListMgr::ResetHighestLOD(true);
			gDCBuffer->ExecuteListAtAddr(drawListInfo->m_offset, drawListInfo->m_listEndIdx, drawListInfo->m_Id);
		#if MULTIPLE_RENDER_THREADS
			PF_POP_TIMEBAR();
		#endif
			RAGE_TIMEBARS_ONLY(DrawListGpuTimeStop(drawListInfo->m_type);)

		#if MULTIPLE_RENDER_THREADS
			GRCDEVICE.BeginCommandList();
		#endif
		}

#if __BANK
		utimer_t timeEnd = sysTimer::GetTicks();
		float timeMs = (float) (timeEnd - timeStart) * sysTimer::GetTicksToMilliseconds();
		ms_drawListExecutionTiming[frameNumberIndex][dliType] += timeMs;

		if (bShowFrameNumbers)
		{
			PIXEnd();
#if __PPU
			snStopMarker(dliType&31);  
#endif
		}
#endif // __BANK
		RAGETRACE_GPU_POP();

#if !MULTIPLE_RENDER_THREADS
		PF_POP_TIMEBAR();
#endif

#if !RAGE_TIMEBARS
		PIXEnd();
#endif

#if DEBUG_ENTITY_GPU_COST
		UpdateDebugEntityGPUCostMaxAndAverage();
#endif

		GRCDBG_POP();

		if (ranOnMainThread)
			CleanupDrawList(drawListInfo);
	}
#if !RSG_PC

#if MULTIPLE_RENDER_THREADS
	GRCDEVICE.EndCommandList();
#endif

	// Check the fence.
	if (m_fenceStatus[m_fenceRenderIdx] == FENCE_UNSET) {
		// Okay, this is bad. We had a fully working draw list, but nobody set the fence?
		// Are there references?
		if (m_aClothRefs[m_fenceRenderIdx].GetCount() != 0 || m_aCharClothRefs[m_fenceRenderIdx].GetCount() != 0) 
		{
			Assertf(false, "A draw list with references has been executed, but it never inserted a GPU fence. Please verify the code creating the draw list.");
		}

		// Hack it to NONE so the update thread won't complain about it.
		m_fenceStatus[m_fenceRenderIdx] = FENCE_NONE;
	}
#endif // !RSG_PC

	gDCBuffer->UnlockReadBuffer();


#if __PPU && DRAWABLESPU_STATS
	s_StatsWriteIdx = !s_StatsWriteIdx;
#elif DRAWABLE_STATS
	s_StatsWriteIdx = !s_StatsWriteIdx;
#endif

}

bool dlDrawListMgr::HasPendingDrawLists() const {
	dlDrawListInfo *drawListInfo;
	return (GetRenderDrawListQ().GetHead(drawListInfo));	
}


void dlDrawListMgr::RemoveAll(void){
	dlDrawListInfo *drawListInfo;
	if (!GetRenderDrawListQ().GetHead(drawListInfo)) {
		dlDebugf1("No lists this frame (remove all)");
#if __WIN32PC
		return;
#endif
	}

	while (true)
	{
		dlDrawListInfo *info = GetRenderDrawListQ().Pop();

		if (!info) {
			break;
		}
		dlForcedCleanupCallback *cb = info->m_ForcedCleanupCallbacks;
		while (cb) {
			void *data = (void*)(((uptr)(cb+1)+15)&~15);
			cb->func(data);
			cb = cb->next;
		}
		if (info->m_DepInfoIndex >= 0) {
			// Ensure any dependencies (such as CopyOffMatrixSet jobs) for this
			// drawlist are done, and that the semaphore count is reset
			JoinDrawlistDependencies(info->m_DepInfoIndex);
		}
		ResetDrawListInfo(*info);
		m_DrawListPoolCs.Lock();
		m_DrawListPool.Delete(info);	// Won't call the constructor, which we don't have.
		m_DrawListPoolCs.Unlock();		
	}

	gDCBuffer->UnlockReadBuffer();
}

dlDrawListInfo executionFence;

// add a reference to a txd (because it is used in a draw list)
void	dlDrawListMgr::AddTxdReference(u32 txdId){

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(txdId > 0);

	strLocalIndex idx = strLocalIndex(txdId);

	// store refs to stuff we are drawing, if we are trying to draw them in a separate thread
	if (idx.Get() > 0) 
	{
		g_TxdStore.AddRef(idx, REF_RENDER);
		gDrawListMgr->AddRefCountedModuleIndex(idx.Get(), &g_TxdStore);
	}
}

void	dlDrawListMgr::AddDwdReference(u32 dwdId){

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(dwdId > 0);
	dlAssert(dwdId < 65535);
	strLocalIndex idx = /*static_cast<u16>*/strLocalIndex(dwdId);

	// store refs to stuff we are drawing, if we are trying to draw them in a separate thread
	if (idx.Get() > 0) 
	{
		g_DwdStore.AddRef(idx, REF_RENDER);
		gDrawListMgr->AddRefCountedModuleIndex(idx.Get(), &g_DwdStore);
	}
}

void	dlDrawListMgr::AddFragReference(u32 fragId){

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(fragId > 0);
	dlAssert(fragId < 65535);
	strLocalIndex idx = /*static_cast<u16>*/strLocalIndex(fragId);

	// store refs to stuff we are drawing, if we are trying to draw them in a separate thread
	if (idx.Get() > 0) 
	{
		Assertf(idx.Get() < g_FragmentStore.GetCount(),"can't add ref to invalid frag idx : %d", idx.Get());
		if (idx.Get() < g_FragmentStore.GetCount())
		{
			g_FragmentStore.AddRef(idx, REF_RENDER);
			gDrawListMgr->AddRefCountedModuleIndex(idx.Get(), &g_FragmentStore);
		}
	}
}

void	dlDrawListMgr::AddDrawableReference(u32 drawableId){

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(drawableId > 0);
	dlAssert(drawableId < 65535);
	strLocalIndex idx = /*static_cast<u16>*/strLocalIndex(drawableId);

	// store refs to stuff we are drawing, if we are trying to draw them in a separate thread
	if (idx.Get() > 0) 
	{
		g_DrawableStore.AddRef(idx, REF_RENDER);
		gDrawListMgr->AddRefCountedModuleIndex(idx.Get(), &g_DrawableStore);
	}
}

void	dlDrawListMgr::AddClothReference(void* pEnvCloth)
{
	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(pEnvCloth);
	int i;
	for(i=0;i<m_Cloths.GetCount();i++)
	{
		if( m_Cloths[i]->m_envCloth == pEnvCloth )
			break;
	}
	dlAssertf(i < m_Cloths.GetCount(),"Environment Cloth 0x%p not found in reference list", pEnvCloth );

	const bool bSpaceToAdd = m_aClothRefs[m_fenceUpdateIdx].GetCount() < m_aClothRefs[m_fenceUpdateIdx].GetCapacity();
	dlAssertf(bSpaceToAdd, "dlDrawListMgr::AddClothReference - m_aClothRefs[%i] is full.", m_fenceUpdateIdx);

	if(bSpaceToAdd)
	{
		m_Cloths[i]->m_refCount++;
		m_aClothRefs[m_fenceUpdateIdx].Push( m_Cloths[i] );
	}	
}


void	dlDrawListMgr::AddCharClothReference(clothVariationData* clothVarData)
{
	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(clothVarData);
	int i;
	for(i=0;i<m_CharCloths.GetCount();i++)
	{
		if( m_CharCloths[i]->m_clothVarData == clothVarData )
			break;
	}
	dlAssertf(i < m_CharCloths.GetCount(),"cloth variation 0x%p data not found in reference list", clothVarData);

	const bool bSpaceToAdd = m_aCharClothRefs[m_fenceUpdateIdx].GetCount() < m_aCharClothRefs[m_fenceUpdateIdx].GetCapacity();
	dlAssertf(bSpaceToAdd, "dlDrawListMgr::AddCharClothReference - m_aCharClothRefs[%i] is full.", m_fenceUpdateIdx);

	if(bSpaceToAdd)
	{
		m_CharCloths[i]->m_refCount++;
		m_aCharClothRefs[m_fenceUpdateIdx].Push( m_CharCloths[i] );
	}
}

// add a reference to map data (because some resident memory is used in a draw list)
void dlDrawListMgr::AddMapDataReference(u32 mapDataIdx)
{
	//Note:	This does differ slightly from AddArchetypeReference(). fwArchetypeStreamingModule is sneaky... Uses fwArchetype's refcount var, so it's much larger. (REF_RENDER is only a portion
	//		of the full 32-bit refcount.) What this means is, we can overflow *much* quicker, and this does in fact happen if we have a large amount of instance lists. But we really only need
	//		a single addref to prevent the map from streaming out. So we enforce this here.
	strStreamingModule* streamingModule = g_MapDataStore.GetStreamingModule();
	if(m_aMapDataRefs.Find(mapDataIdx) == -1)
	{
		g_MapDataStore.AddRef(strLocalIndex(mapDataIdx), REF_RENDER);
		gDrawListMgr->AddRefCountedModuleIndex(mapDataIdx, streamingModule);
		m_aMapDataRefs.PushAndGrow(mapDataIdx);
	}
}

// add a reference to a model info (because it is used in a draw list)
void dlDrawListMgr::AddArchetypeReference(u32 modelIdx)
{
	dlAssert(sysThreadType::IsUpdateThread());
	Assert(modelIdx < 65535);

	// store refs to stuff we are drawing, if we are trying to draw them in a separate thread
	fwArchetype* ptr = fwArchetypeManager::GetArchetype(modelIdx);

	// add a ref, make sure we clear it in the future
	ptr->AddRef();
	strStreamingModule* streamingModule = fwArchetypeManager::GetStreamingModule();
	fwModelId modelId((strLocalIndex(modelIdx)));
	s32 objIndex = modelId.ConvertToStreamingIndex().Get();
	gDrawListMgr->AddRefCountedModuleIndex(objIndex, streamingModule);
}

// add a reference to a model info (because it is used in a draw list)
void dlDrawListMgr::AddArchetype_HD_Reference(u32 modelIdx){

	dlAssert(sysThreadType::IsUpdateThread());
	Assert(modelIdx < 65535);

	// store refs to stuff we are drawing, if we are trying to draw them in a separate thread
	fwArchetype* ptr = fwArchetypeManager::GetArchetype(modelIdx);

	// add a ref, make sure we clean it in the future
	ptr->AddRef();
	strStreamingModule* streamingModule = fwArchetypeManager::GetStreamingModule();
	fwModelId modelId((strLocalIndex(modelIdx)));
	s32 objIndex = modelId.ConvertToStreamingIndex().Get();
	gDrawListMgr->AddRefCountedModuleIndex(objIndex, streamingModule);

	// add an HD ref, make sure we clean it in the future
	ptr->AddHDRef(true);
	gDrawListMgr->AddRefCountedArchetypeHD(ptr);
}

#if !__FINAL
void dlDrawListMgr::SuppressAddTypeFileReferencesBegin()
{
	dlAssert(sysThreadType::IsUpdateThread());
	Assert(ms_suppressAddTypeFileReferences >= 0);
	ms_suppressAddTypeFileReferences++;
}
void dlDrawListMgr::SuppressAddTypeFileReferencesEnd()
{
	dlAssert(sysThreadType::IsUpdateThread());
	Assert(ms_suppressAddTypeFileReferences >= 1);
	ms_suppressAddTypeFileReferences--;
}
#endif // !__FINAL

// add a reference to an .ityp slot (because it is defines an archetype ref'ed in RT)
void dlDrawListMgr::AddTypeFileReference(u32 mapTypeDataSlotIdx){

	dlAssert(sysThreadType::IsUpdateThread());
	Assert(mapTypeDataSlotIdx < 65535);

#if !__FINAL
	if (ms_suppressAddTypeFileReferences > 0)
		return;
#endif // !__FINAL

	strStreamingModule* streamingModule = g_MapTypesStore.GetStreamingModule();
	g_MapTypesStore.AddRef(strLocalIndex(mapTypeDataSlotIdx), REF_RENDER);
	AddRefCountedModuleIndex(mapTypeDataSlotIdx, streamingModule);
}


sysCriticalSectionToken		s_LockEnvClothRefList;
sysCriticalSectionToken		s_LockCharClothRefList;

void dlDrawListMgr::ClothAdd(void* pEnvCloth)
{
	sysCriticalSection critSec(s_LockEnvClothRefList);

#if __DEV
	for(int i=0;i<m_Cloths.GetCount();i++)
	{
		dlAssertf( m_Cloths[i]->m_envCloth != pEnvCloth, "Duplicated environment cloth 0x%p in reference list", pEnvCloth);
	}
#endif // __DEV	

#if __ASSERT
	if (m_ClothsPool.IsFull())
	{
		// print the whole cloth pool for debug purposes
		Displayf("Attempting to allocate memory but m_ClothsPool is full !");
		
		for (int poolIdx = 0; poolIdx < m_ClothsPool.GetCount(); ++poolIdx)
		{
			environmentCloth* envCloth = (environmentCloth*)m_Cloths[poolIdx]->m_envCloth;
			Displayf("    m_Cloths[%d] : refCount (%d) - removeRequested (%s) - memPtr (0x%p) - env cloth pos (%f, %f, %f)", 
				poolIdx,
				m_Cloths[poolIdx]->m_refCount,
				m_Cloths[poolIdx]->m_removeRequested ? "true" : "false",
				m_Cloths[poolIdx]->m_memPtr,
				envCloth->GetFramePosition().GetX(),
				envCloth->GetFramePosition().GetY(),
				envCloth->GetFramePosition().GetZ());
		}
	}
#endif

	clothRecord* cloth = (clothRecord*)m_ClothsPool.New();
	Assert( cloth );
	cloth->m_envCloth = pEnvCloth;
	cloth->m_memPtr	= 0;
	cloth->m_refCount = 0;
	cloth->m_removeRequested = false;

	Assert( m_ClothsPool.GetCount() < MAX_CLOTH );
	Assert( m_Cloths.GetCount() < MAX_CLOTH );
	m_Cloths.PushAndGrow(cloth);
}

void dlDrawListMgr::ClothRemove(void* pEnvCloth, void* preAllocatedMemory)
{
	sysCriticalSection critSec(s_LockEnvClothRefList);

	int i;
	for(i=0;i<m_Cloths.GetCount();i++)
	{
		if( m_Cloths[i]->m_envCloth == pEnvCloth )
			break;
	}
	if(dlVerifyf(i < m_Cloths.GetCount(),"Environment cloth 0x%p not found in reference list", pEnvCloth))
	{
		m_Cloths[i]->m_removeRequested = true;
		m_Cloths[i]->m_memPtr = preAllocatedMemory;
		m_Cloths[i]->m_envCloth = pEnvCloth;
	}
}


void dlDrawListMgr::CharClothAdd(clothVariationData *pClothVarData)
{
	sysCriticalSection critSec(s_LockCharClothRefList);

#if __DEV
	for(int i=0;i<m_CharCloths.GetCount();i++)
	{
		dlAssertf( m_CharCloths[i]->m_clothVarData != pClothVarData, "Duplicated cloth variation data in Cloth reference list");
	}
#endif // __DEV	

	charClothRecord* cloth = (charClothRecord*)m_CharClothsPool.New();
	Assert( cloth );
	cloth->m_clothVarData = pClothVarData;
	cloth->m_refCount = 0;
	cloth->m_removeRequested = false;

	Assert( m_CharClothsPool.GetCount() < MAX_CLOTH );
	Assert( m_CharCloths.GetCount() < MAX_CLOTH );
	m_CharCloths.PushAndGrow(cloth);
}


void dlDrawListMgr::CharClothRemove(clothVariationData * pClothVarData)
{
	sysCriticalSection critSec(s_LockCharClothRefList);

	int i;
	for(i=0;i<m_CharCloths.GetCount();i++)
	{
		if( m_CharCloths[i]->m_clothVarData == pClothVarData )
			break;
	}
	dlAssertf(i < m_CharCloths.GetCount(),"cloth variation data not found in reference list");
	m_CharCloths[i]->m_removeRequested = true;
}


sysDependency& dlDrawListMgr::CreateDrawlistDependency()
{
#if !__TOOL
	if (Unlikely(m_dependencies.IsFull()))
	{
		Quitf(ERR_DEFAULT, "CreateDrawlistDependency out of dependencies, increase MAX_DEPENDENCY_COUNT");
	}
	return m_dependencies.Append();
#else // !__TOOL
	Quitf("This function is not meant to be called by a tool.");
	return *((sysDependency *) NULL);
#endif // !__TOOL
}

void dlDrawListMgr::JoinAllDrawlistDependencies()
{
#if !__TOOL
	for(u32 index = 0; index < m_dependencyInfoIndex; index++)
	{
		u32 expectedCount = m_dependencyInfos[index].m_ExpectedCount;
		volatile u32* pFinishedDependencyCount = &m_dependencyInfos[index].m_FinishedCount;
		while ( *pFinishedDependencyCount !=  expectedCount)
		{
			sysIpcYield( PRIO_NORMAL );
		}
	}

	m_dependencies.Reset();
	m_submittedDependencyCount = 0;
	m_dependencyInfoIndex = 0;
#endif // !__TOOL
}

void dlDrawListMgr::JoinDrawlistDependencies(int dependencyInfoIndex)
{
#if !__TOOL
	int index = m_nextJoinDrawListIndex;
	for(; index <= dependencyInfoIndex; index++)
	{
		sysIpcWaitSema(m_dependencyInfos[index].m_Sema);
	}
	m_nextJoinDrawListIndex = index;
#endif // !__TOOL
}

bool dlDrawListMgr::SubmitPendingDependencies()
{
	u32 numDependencies = m_dependencies.GetCount();
	u32 numToSubmit = numDependencies - m_submittedDependencyCount;
	if(numToSubmit == 0)
	{
		return false;
	}

	FatalAssertf(m_dependencyInfoIndex < MAX_DRAWLIST_DEPENDENCY_INFOS, "More than %d drawlists have dependencies - increase MAX_DRAWLIST_DEPENDENCY_INFOS!", MAX_DRAWLIST_DEPENDENCY_INFOS);

	m_dependencyInfos[m_dependencyInfoIndex].m_FinishedCount = 0;
	m_dependencyInfos[m_dependencyInfoIndex].m_ExpectedCount = numToSubmit;

	for(u32 i = m_submittedDependencyCount; i < numDependencies; i++)
	{
		m_dependencies[i].m_Params[7].m_AsPtr = &(m_dependencyInfos[m_dependencyInfoIndex].m_FinishedCount);
		m_dependencies[i].m_Params[8].m_AsUInt = numToSubmit;
		m_dependencies[i].m_Params[9].m_AsPtr = &(m_dependencyInfos[m_dependencyInfoIndex].m_Sema);
		
		sysDependencyScheduler::Insert( &m_dependencies[i] );
	}

	m_submittedDependencyCount = numDependencies;
	
	return true;
}


void DumpMemCB(void){
	dlCmdBase::RequestDumpData();
}


/* PURPOSE: Call this from the render thread to let the update thread know that we didn't
 * insert a GPU fence this frame. The update thread will use this information in two
 * frames for integrity checks.
 */
void dlDrawListMgr::SetSkippedFence()
{
	DEBUGF_FENCES("Setting skipped fence, render index=%d", m_fenceRenderIdx);
	dlAssert(sysThreadType::IsRenderThread());
	Assertf(m_fenceStatus[m_fenceRenderIdx] == FENCE_NONE || m_fenceStatus[m_fenceRenderIdx] == FENCE_DUMMY, "Fence %d should not be set, but status is %d", m_fenceRenderIdx, m_fenceStatus[m_fenceRenderIdx]);
	m_fenceStatus[m_fenceRenderIdx] = FENCE_SKIPPED;
}

/* RETURNS: True if the render thread indicated two frames ago that it didn't
 * insert a GPU fence for us to block on.
 */
bool dlDrawListMgr::IsFenceSkipped() const
{
	dlAssert(sysThreadType::IsUpdateThread());
	DEBUGF_FENCES("Checking for skip update index %d has status %d", m_fenceUpdateIdx, m_fenceStatus[m_fenceUpdateIdx]);
	return m_fenceStatus[m_fenceUpdateIdx] == FENCE_SKIPPED || m_fenceStatus[m_fenceUpdateIdx] == FENCE_DUMMY;
}

grcFenceHandle	dlDrawListMgr::GetEndFenceHandle(void) 
{
	DEBUGF_FENCES("Reading end fence, update index=%d", m_fenceUpdateIdx);

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(m_fenceStatus[m_fenceUpdateIdx] != FENCE_UNSET);
	return(m_endFenceHandle[m_fenceUpdateIdx]);
}
void	dlDrawListMgr::ClearEndFenceHandle(void) 
{ 
	DEBUGF_FENCES("Clearing end fence, update index=%d", m_fenceUpdateIdx);

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(m_fenceStatus[m_fenceUpdateIdx] != FENCE_UNSET);
	dlAssert(!GRCDEVICE.IsFencePending(m_endFenceHandle[m_fenceUpdateIdx]));
	m_fenceStatus[m_fenceUpdateIdx] = FENCE_NONE;
}

void dlDrawListMgr::AddDummyFence()
{
	int previousFence = m_fenceUpdateIdx - 1;
	if (previousFence < 0) {
		previousFence = FENCE_COUNT - 1;
	}

	DEBUGF_FENCES("Adding dummy fence to index %d", previousFence);

	dlAssert(sysThreadType::IsUpdateThread());
	dlAssert(m_fenceStatus[previousFence] == FENCE_NONE);
	m_fenceStatus[previousFence] = FENCE_DUMMY;
}

/* PURPOSE: Call this from the render thread to assign a fence handle. The update thread
 * will block on this two frames later.
 */
void	dlDrawListMgr::InsertEndFence() 
{ 
	DEBUGF_FENCES("Setting end fence, render index=%d", m_fenceRenderIdx);

	grcFenceHandle fenceHandle = m_endFenceHandle[m_fenceRenderIdx];
	Assert(!GRCDEVICE.IsFencePending(fenceHandle));
	GRCDEVICE.CpuMarkFencePending(fenceHandle);
	GRCDEVICE.GpuMarkFenceDone(fenceHandle);
	Assert(m_fenceStatus[m_fenceRenderIdx] == FENCE_UNSET); 
	m_fenceStatus[m_fenceRenderIdx] = FENCE_SET;
}

/* PURPOSE: Call this from the render thread to indicate that we're currently in the process
 * of rendering and will be inserting a GPU fence when we're done.
 */
void dlDrawListMgr::PrimeEndFenceHandle()
{
	DEBUGF_FENCES("Priming skipped fence, render index=%d", m_fenceRenderIdx);

	dlAssert(sysThreadType::IsRenderThread());
//	Assertf(m_fenceStatus[m_fenceRenderIdx] == FENCE_NONE , "Fence %d should not be set, but status is %d", m_fenceRenderIdx, m_fenceStatus[m_fenceRenderIdx]);
	dlAssert(m_fenceStatus[m_fenceRenderIdx] != FENCE_SKIPPED && m_fenceStatus[m_fenceRenderIdx] != FENCE_DUMMY);

	if (m_fenceStatus[m_fenceRenderIdx] == FENCE_NONE) {
		m_fenceStatus[m_fenceRenderIdx] = FENCE_UNSET;
	}
}

void dlDrawListMgr::PushForcedTechnique(s32 techId)
{
	dlAssert(sysThreadType::IsRenderThread());

	m_forcedTechniqueStackIdx[m_fenceRenderIdx][g_RenderThreadIndex]++;
	dlAssert(m_forcedTechniqueStackIdx[m_fenceRenderIdx][g_RenderThreadIndex] < FORCED_TECHNIQUE_STACK_SIZE);
	m_forcedTechniqueStack[m_fenceRenderIdx][m_forcedTechniqueStackIdx[m_fenceRenderIdx][g_RenderThreadIndex]] = techId;
}

s32 dlDrawListMgr::PopForcedTechnique(void)
{
	dlAssert(sysThreadType::IsRenderThread());

	s32 value = m_forcedTechniqueStack[m_fenceRenderIdx][m_forcedTechniqueStackIdx[m_fenceRenderIdx][g_RenderThreadIndex]];
	m_forcedTechniqueStackIdx[m_fenceRenderIdx][g_RenderThreadIndex]--;
	dlAssert(m_forcedTechniqueStackIdx[m_fenceRenderIdx][g_RenderThreadIndex] >= -1);
	return value;
}

void dlDrawListMgr::ResetForcedTechnique()
{
	dlAssert(sysThreadType::IsUpdateThread());
	for(int i=0; i<NUMBER_OF_RENDER_THREADS; i++)
		m_forcedTechniqueStackIdx[m_fenceUpdateIdx][i] = -1;
}

void dlDrawListMgr::FlipBufferIdx(void)
{
	dlAssert(sysThreadType::IsUpdateThread());
	ASSERT_ONLY(for(int i=0; i<NUMBER_OF_RENDER_THREADS; i++))
		Assert(m_forcedTechniqueStackIdx[m_fenceUpdateIdx][i] == -1);

	gDCBuffer->FlipBuffers(); 
	ResetForcedTechnique();
}

void dlDrawListMgr::FlipUpdateFenceIdx()
{
	m_fenceUpdateIdx = (m_fenceUpdateIdx + 1) % FENCE_COUNT;
	m_streamingIndicesBufferIndex = (m_streamingIndicesBufferIndex + 1) % STREAMING_INDICES_BUFFER_COUNT;
	DEBUGF_FENCES("Advancing update fence, now %d", m_fenceUpdateIdx);
}

void dlDrawListMgr::FlipRenderFenceIdx(void)
{
	m_fenceRenderIdx = (m_fenceRenderIdx + 1) % FENCE_COUNT;
	DEBUGF_FENCES("Advancing render fence, now %d", m_fenceRenderIdx);
}

/* RETURNS: TRUE if there are any references to textures or cloth objects in the update thread
 * index (i.e. those that were rendered in the last frame).
 */
bool dlDrawListMgr::HasRefs() const {
	dlAssert(sysThreadType::IsUpdateThread());
	return (m_aClothRefs[m_fenceUpdateIdx].GetCount() || m_aCharClothRefs[m_fenceUpdateIdx].GetCount()); 
}

// assume draw lists have finished rendering, so (hopefully) safely remove all references to model infos
void	dlDrawListMgr::RemoveAllRefs(u32 flags){
	(void) flags;

	dlAssert(sysThreadType::IsUpdateThread());


	PF_FUNC(DLManagerArch__RemoveAllRefs);

	int fenceUpdateIdx = m_fenceUpdateIdx;
	int streamingIndicesBufferIndex = m_streamingIndicesBufferIndex;

	for(s32 i=0; i<m_aClothRefs[fenceUpdateIdx].GetCount(); i++){
		m_aClothRefs[fenceUpdateIdx][i]->m_refCount--;
	}

	m_aClothRefs[fenceUpdateIdx].Resize(0);

	for(s32 i=0; i<m_aCharClothRefs[fenceUpdateIdx].GetCount(); i++){
		m_aCharClothRefs[fenceUpdateIdx][i]->m_refCount--;
	}

	m_aCharClothRefs[fenceUpdateIdx].Resize(0);

	m_aMapDataRefs.ResetCount();

	atArray<ReferencedStreamable> &arrayStrIndices = m_aStrIndicesCache[streamingIndicesBufferIndex];
#if __DEV
	static s32 strIndicesRefCountPeak = 2000;

	if (arrayStrIndices.GetCount() > strIndicesRefCountPeak){
		strIndicesRefCountPeak = arrayStrIndices.GetCount();
		dlDebugf1("New peak in streaming indices refs: %d", strIndicesRefCountPeak);
	}
#endif //__DEV

	int count = arrayStrIndices.GetCount();
	for(s32 i=0; i<count; i++)
	{
		strLocalIndex moduleIndex = strLocalIndex(arrayStrIndices[i].m_moduleIndex);
		strStreamingModule* pModule = arrayStrIndices[i].m_Module;
		pModule->RemoveRef(moduleIndex, REF_RENDER);
	}
	m_aStrIndicesCache[streamingIndicesBufferIndex].Resize(0);

	atArray<fwArchetype*> &HDArchetypeRefs = m_HDArchetypeRefs[fenceUpdateIdx];
	//PF_SET(DLManagerArch__ArhcetypeHDRefCount, HDArchetypeRefs.GetCount());

	for(s32 i=0; i<HDArchetypeRefs.GetCount(); i++)
	{
		HDArchetypeRefs[i]->RemoveHDRef(true);
	}
	HDArchetypeRefs.Resize(0);

	atArray<fwCustomShaderEffectBaseType*> &customShaderEffectTypes = m_customShaderEffectTypes[fenceUpdateIdx];
	PF_SET(DLManagerArch__CSERefCount, customShaderEffectTypes.GetCount());

	for(s32 i=0; i<customShaderEffectTypes.GetCount(); i++)
	{
		customShaderEffectTypes[i]->RemoveRef();
	}

#if __DEV
	static s32 customeShaderEffectTypesPeak = 1000;

	if (customShaderEffectTypes.GetCount() > customeShaderEffectTypesPeak){
		customeShaderEffectTypesPeak = customShaderEffectTypes.GetCount();
		dlDebugf1("New peak in Custom Shader Effect Types: %d", customeShaderEffectTypesPeak);
	}
#endif //__DEV
	customShaderEffectTypes.Resize(0);

	dlAssertf((flags & FLUSH_WITHOUT_RENDERING) != 0 || m_fenceUpdateIdx != m_fenceRenderIdx, "The update thread got out of sync with the render thread's fence index. This could result in objects being released too early. Index=%d", m_fenceUpdateIdx);
}

dlDrawListInfo* dlDrawListMgr::GetBuildExecDLInfo(void) {
	dlAssert(!sysThreadType::IsRenderThread());
	return(pCurrBuildingList);
}

dlDrawListInfo* dlDrawListMgr::GetCurrExecDLInfo(void) { 
	dlAssert(sysThreadType::IsRenderThread());
	return(pCurrExecutingList);
}

#if __DEV
s32	dlDrawListMgr::GetCurrBuildListIdx() {
	if (pCurrBuildingList) 
		return(pCurrBuildingList->m_Id); 
	else 
		return(-1);
}

s32	dlDrawListMgr::GetCurrExecListIdx() {
	if (pCurrExecutingList) 
		return(pCurrExecutingList->m_Id); 
	else 
		return(-1); 
}

bool	dlDrawListMgr::CheckBuildListIntegrity(s32 listEndIdx/*=-1*/){
	dlAssert(pCurrBuildingList);

	if (listEndIdx == -1){
		listEndIdx = pCurrBuildingList->m_listEndIdx;
	}

	return(gDCBuffer->CheckListIntegrity(pCurrBuildingList->m_offset, listEndIdx));
}
#endif //__DEV

CompileTimeAssert(dlDrawListMgr::DLOD_HIGH == LOD_HIGH + 0);
CompileTimeAssert(dlDrawListMgr::DLOD_MED == LOD_MED + 0);
CompileTimeAssert(dlDrawListMgr::DLOD_LOW == LOD_LOW + 0);
CompileTimeAssert(dlDrawListMgr::DLOD_VLOW == LOD_VLOW + 0);

CompileTimeAssert(dlDrawListMgr::VLOD_HIGH == LOD_HIGH + 1);
CompileTimeAssert(dlDrawListMgr::VLOD_MED == LOD_MED + 1);
CompileTimeAssert(dlDrawListMgr::VLOD_LOW == LOD_LOW + 1);

__forceinline int GetLODInfoIndex(bool bRenderThread)
{
	return bRenderThread ? (g_RenderThreadIndex + 1) : 0;
}

void dlDrawListMgr::SetHighestDrawableLOD(bool bRenderThread, eHighestDrawableLOD drawableLOD)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestDrawableLOD == DLOD_DEFAULT || drawableLOD == DLOD_DEFAULT);
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestDrawableLOD = (int)drawableLOD;

	if (!bRenderThread)
	{
		DLC_Add(SetHighestDrawableLOD, true, drawableLOD);
	}
}

void dlDrawListMgr::SetHighestFragmentLOD(bool bRenderThread, eHighestDrawableLOD fragmentLOD) // not vehicles
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestFragmentLOD == DLOD_DEFAULT || fragmentLOD == DLOD_DEFAULT);
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestFragmentLOD = (int)fragmentLOD;

	if (!bRenderThread)
	{
		DLC_Add(SetHighestFragmentLOD, true, fragmentLOD);
	}
}

void dlDrawListMgr::SetHighestVehicleLOD(bool bRenderThread, eHighestVehicleLOD vehicleLOD)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestVehicleLOD == VLOD_DEFAULT || vehicleLOD == VLOD_DEFAULT);
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestVehicleLOD = (int)vehicleLOD;

	if (!bRenderThread)
	{
		DLC_Add(SetHighestVehicleLOD, true, vehicleLOD);
	}
}

void dlDrawListMgr::SetHighestPedLOD(bool bRenderThread, eHighestPedLOD pedLOD)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestPedLOD == PLOD_DEFAULT || pedLOD == PLOD_DEFAULT);
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestPedLOD = (int)pedLOD;

	if (!bRenderThread)
	{
		DLC_Add(SetHighestPedLOD, true, pedLOD);
	}
}

void dlDrawListMgr::SetHighestLOD(bool bRenderThread, eHighestDrawableLOD drawableLOD, eHighestDrawableLOD fragmentLOD, eHighestVehicleLOD vehicleLOD, eHighestPedLOD pedLOD)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestDrawableLOD == DLOD_DEFAULT || drawableLOD == DLOD_DEFAULT);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestFragmentLOD == DLOD_DEFAULT || fragmentLOD == DLOD_DEFAULT);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestVehicleLOD == VLOD_DEFAULT || vehicleLOD == VLOD_DEFAULT);
	dlAssert(gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestPedLOD == PLOD_DEFAULT || pedLOD == PLOD_DEFAULT);
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestDrawableLOD = (int)drawableLOD;
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestFragmentLOD = (int)fragmentLOD;
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestVehicleLOD = (int)vehicleLOD;
	gDrawListMgr->m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestPedLOD = (int)pedLOD;

	if (!bRenderThread)
	{
		DLC_Add(SetHighestLOD, true, drawableLOD, fragmentLOD, vehicleLOD, pedLOD);
	}
}

void dlDrawListMgr::ResetHighestLOD(bool bRenderThread)
{
	SetHighestLOD(bRenderThread, DLOD_DEFAULT, DLOD_DEFAULT, VLOD_DEFAULT, PLOD_DEFAULT);
}

void dlDrawListMgr::AdjustDrawableLOD(bool bRenderThread, u32& lod)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	int highestDrawableLOD = m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestDrawableLOD;

	BANK_ONLY(highestDrawableLOD = Max<int>(m_HighestDrawableLOD_debug, highestDrawableLOD));
	if ((u32)highestDrawableLOD > lod) { lod = (u32)highestDrawableLOD; }
}

void dlDrawListMgr::AdjustFragmentLOD(bool bRenderThread, u32& lod, bool bIsVehicle) // includes vehicles if they are not HD
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);

	if (bIsVehicle)
	{
		int highestVehicleLOD = m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestVehicleLOD;
		BANK_ONLY(highestVehicleLOD = Max<int>(m_HighestVehicleLOD_debug, highestVehicleLOD));
		if ((u32)highestVehicleLOD > lod + 1) { lod = (u32)highestVehicleLOD - 1; }
	}
	else
	{
		int highestFragmentLOD = m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestFragmentLOD;
		BANK_ONLY(highestFragmentLOD = Max<int>(m_HighestFragmentLOD_debug, highestFragmentLOD));
		if ((u32)highestFragmentLOD > lod) { lod = (u32)highestFragmentLOD; }
	}
}

void dlDrawListMgr::AdjustVehicleHD(bool bRenderThread, bool& bAllowHD)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	bool allowVehicleHD = m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestVehicleLOD <= VLOD_HD;
	BANK_ONLY(allowVehicleHD = (m_HighestVehicleLOD_debug <= VLOD_HD) && allowVehicleHD);
	if (!allowVehicleHD) { bAllowHD = false; }
}

void dlDrawListMgr::AdjustPedLOD(bool bRenderThread, u32& lod)
{
	dlAssert(sysThreadType::IsRenderThread() == bRenderThread);
	int highestPedLOD = m_LODInfo[GetLODInfoIndex(bRenderThread)].m_HighestPedLOD;
	BANK_ONLY(highestPedLOD = Max<int>(m_HighestPedLOD_debug, highestPedLOD));
	if ((u32)highestPedLOD > lod) { lod = (u32)highestPedLOD; }
}

/*----------------- widget stuff ----------------*/
#if __BANK

void dlDrawListMgr::RegisterDrawlist(eDrawListType DLType)
{
	dlAssertf(ms_drawListCreationStartTime == 0, "Creating two draw lists at the same time?");
	ms_numDrawlists++;
	ms_drawListStats[DLType]++;
	ms_drawListCreationStartTime = sysTimer::GetTicks();
	ms_CurrDLType = DLType;
}

enum
{
	DRAWLISTSTATS_NAME                    ,
	DRAWLISTSTATS_NUMLISTS                ,
	DRAWLISTSTATS_CREATION                ,
	DRAWLISTSTATS_EXECUTION               ,
	DRAWLISTSTATS_GPU                     ,
	DRAWLISTSTATS_DLC                     ,
	DRAWLISTSTATS_GCM                     ,
	DRAWLISTSTATS_MACRO                   ,
	DRAWLISTSTATS_GCMRING                 ,
	DRAWLISTSTATS_MISSINGTECHNIQUE        ,
	DRAWLISTSTATS_MODELSCULLED            ,
	DRAWLISTSTATS_MODELSDRAWN             ,
	DRAWLISTSTATS_GEOMSCULLED             ,
	DRAWLISTSTATS_GEOMSDRAWN              ,
	DRAWLISTSTATS_DRAWABLEDRAWCALLS       ,
	DRAWLISTSTATS_DRAWABLEDRAWSKINNEDCALLS,
	DRAWLISTSTATS_EDGEJOBS                ,
	DRAWLISTSTATS_EDGEREJECT              ,
	DRAWLISTSTATS_EDGEOUTPUT              ,
	DRAWLISTSTATS_GCMDRAWCALLS            ,
	DRAWLISTSTATS_GCMDRAWINDICES          ,
	DRAWLISTSTATS_TOTALIDX                ,
	DRAWLISTSTATS_TOTALPRIM               ,
	DRAWLISTSTATS_VISIBLEIDX              ,
	DRAWLISTSTATS_CULLCLIPPEDIDX          ,
	DRAWLISTSTATS_SPUWAIT                 ,
	DRAWLISTSTATS_NUMCOLUMNS              ,
	DRAWLISTSTATS_ALLCOLUMNS              = (1 << DRAWLISTSTATS_NUMCOLUMNS) - 1,

	// extra flags
	DRAWLISTSTATS_RENDERING               = DRAWLISTSTATS_NUMCOLUMNS,
	DRAWLISTSTATS_CONTEXT                 ,
	DRAWLISTSTATS_SHOWKILOCOUNTERS        , // show counters in k
	DRAWLISTSTATS_COUNT
};

enum
{
	DRAWLISTSTATS_NO_CATEGORY             ,
	DRAWLISTSTATS_PEDS                    ,
	DRAWLISTSTATS_VEHICLES                ,
	DRAWLISTSTATS_PROPS                   ,
	DRAWLISTSTATS_LOD                     ,
	DRAWLISTSTATS_SLOD1                   ,
	DRAWLISTSTATS_SLOD2                   ,
	DRAWLISTSTATS_SLOD3                   ,
	DRAWLISTSTATS_SLOD4                   ,
	DRAWLISTSTATS_VEG                     ,
	DRAWLISTSTATS2_COUNT
};
CompileTimeAssert(DRAWLISTSTATS_COUNT <= 32); // needs to fit in a u32
CompileTimeAssert(DRAWLISTSTATS2_COUNT <= 32); // needs to fit in a u32

static const u32 s_TerseFlags =
		(1 << DRAWLISTSTATS_NAME) |
		(1 << DRAWLISTSTATS_TOTALIDX) |
		(1 << DRAWLISTSTATS_TOTALPRIM) |
		(1 << DRAWLISTSTATS_SHOWKILOCOUNTERS) |
		(1 << DRAWLISTSTATS_RENDERING);

void dlDrawListMgr::SetStatsHighlight(bool bEnable, const char* name, bool bShowRenderingStats, bool bShowContextStats, bool bShowKiloCounters)
{
	m_ShowDrawListStats = bEnable;

	if (bShowRenderingStats) { m_ShowDrawListStatsFlags |=  BIT(DRAWLISTSTATS_RENDERING); }
	else                     { m_ShowDrawListStatsFlags &= ~BIT(DRAWLISTSTATS_RENDERING); }
	if (bShowContextStats)   { m_ShowDrawListStatsFlags |=  BIT(DRAWLISTSTATS_CONTEXT); }
	else                     { m_ShowDrawListStatsFlags &= ~BIT(DRAWLISTSTATS_CONTEXT); }
	if (bShowKiloCounters)   { m_ShowDrawListStatsFlags |=  BIT(DRAWLISTSTATS_SHOWKILOCOUNTERS); }
	else                     { m_ShowDrawListStatsFlags &= ~BIT(DRAWLISTSTATS_SHOWKILOCOUNTERS); }

	if (name)
	{
		for (int i = 0; i < GetDrawListTypeCount(); i++)
		{
			if (stricmp(s_DrawListNames[i].c_str(), name) == 0)
			{
				m_ShowDrawListStatsIndex = i;
				return;
			}
		}

		m_ShowDrawListStatsIndex = -1;
	}
}

#ifndef DRAWABLESPU_STATS
#define DRAWABLESPU_STATS 0
#endif

#ifndef DRAWABLE_STATS
#define DRAWABLE_STATS 0
#endif

void dlDrawListMgr::SetupLODWidgets(bkGroup& bank, int* highestDrawableLOD, int* highestFragmentLOD, int* highestVehicleLOD, int* highestPedLOD)
{
	if (highestDrawableLOD == NULL &&
		highestFragmentLOD == NULL &&
		highestVehicleLOD == NULL &&
		highestPedLOD == NULL)
	{
		bank.AddTitle("See Also: 'Scene/PostScan/LOD Based Cull'");
	}

	const char* highestDrawableLODStrings[] =
	{
		"DLOD_HIGH (default)",
		"DLOD_MED",
		"DLOD_LOW",
		"DLOD_VLOW",
	};
	CompileTimeAssert(NELEM(highestDrawableLODStrings) == DLOD_COUNT);

	const char* highestVehicleLODStrings[] =
	{
		"VLOD_HD (default)",
		"VLOD_HIGH",
		"VLOD_MED",
		"VLOD_LOW",
        "VLOD_LOWER",
	};
	CompileTimeAssert(NELEM(highestVehicleLODStrings) == VLOD_COUNT);

	const char* highestPedLODStrings[] =
	{
		"PLOD_DEFAULT",
		"PLOD_LOD",
		"PLOD_SLOD",
	};
	CompileTimeAssert(NELEM(highestPedLODStrings) == PLOD_COUNT);

	if (highestDrawableLOD == NULL) { highestDrawableLOD = &m_HighestDrawableLOD_debug; }
	if (highestFragmentLOD == NULL) { highestFragmentLOD = &m_HighestFragmentLOD_debug; }
	if (highestVehicleLOD == NULL) { highestVehicleLOD = &m_HighestVehicleLOD_debug; }
	if (highestPedLOD == NULL) { highestPedLOD = &m_HighestPedLOD_debug; }

	if (highestDrawableLOD != (int*)1) { bank.AddCombo("Highest Drawable LOD", highestDrawableLOD, NELEM(highestDrawableLODStrings), highestDrawableLODStrings); }
	if (highestFragmentLOD != (int*)1) { bank.AddCombo("Highest Fragment LOD", highestFragmentLOD, NELEM(highestDrawableLODStrings), highestDrawableLODStrings); }
	if (highestVehicleLOD != (int*)1) { bank.AddCombo("Highest Vehicle LOD", highestVehicleLOD, NELEM(highestVehicleLODStrings), highestVehicleLODStrings); }
	if (highestPedLOD != (int*)1) { bank.AddCombo("Highest Ped LOD", highestPedLOD, NELEM(highestPedLODStrings), highestPedLODStrings); }
}

void dlDrawListMgr::TerseMode(void *that)
{
	((dlDrawListMgr*)that)->m_ShowDrawListStatsFlags = s_TerseFlags;
}

bool dlDrawListMgr::SetupWidgets(bkBank& bank)
{
	bank.PushGroup("Draw Lists");
	{
		bank.AddToggle("Show Draw List Stats", &m_ShowDrawListStats);
		bank.AddSlider("Show Draw List Index", &m_ShowDrawListStatsIndex, -1, 32, 1);
		bank.AddButton("Terse Display",datCallback(dlDrawListMgr::TerseMode,(void*)this),"Wayland's Button");

		bank.AddToggle("  " STRING(DRAWLISTSTATS_NAME                    ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_NAME                    );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_NUMLISTS                ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_NUMLISTS                );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_CREATION                ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_CREATION                );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_EXECUTION               ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_EXECUTION               );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_GPU                     ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_GPU                     );
#if __DEV
		bank.AddToggle("  " STRING(DRAWLISTSTATS_DLC                     ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_DLC                     );
#endif // __DEV
#if DRAWABLE_STATS
		bank.AddToggle("  " STRING(DRAWLISTSTATS_MISSINGTECHNIQUE        ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_MISSINGTECHNIQUE        );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_DRAWABLEDRAWCALLS       ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_DRAWABLEDRAWCALLS       );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_DRAWABLEDRAWSKINNEDCALLS), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_DRAWABLEDRAWSKINNEDCALLS);
		bank.AddToggle("  " STRING(DRAWLISTSTATS_MODELSCULLED            ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_MODELSCULLED            );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_MODELSDRAWN             ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_MODELSDRAWN             );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_GEOMSCULLED             ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_GEOMSCULLED             );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_GEOMSDRAWN              ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_GEOMSDRAWN              );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_TOTALIDX                ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_TOTALIDX                );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_TOTALPRIM               ), &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_TOTALPRIM               );
		
		bank.AddToggle("  " STRING(DRAWLISTSTATS_PEDS                    ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_PEDS                   );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_VEHICLES                ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_VEHICLES               );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_PROPS                   ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_PROPS                  );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_LOD                     ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_LOD                    );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_SLOD1                   ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_SLOD1                  );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_SLOD2                   ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_SLOD2                  );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_SLOD3                   ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_SLOD3                  );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_SLOD4                   ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_SLOD4                  );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_VEG                     ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_VEG                    );
		bank.AddToggle("  " STRING(DRAWLISTSTATS_NO_CATEGORY             ), &m_ShowDrawListStatsFlags2, 1 << DRAWLISTSTATS_NO_CATEGORY            );
#endif // DRAWABLE_STATS

		bank.AddToggle("Show Rendering Stats", &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_RENDERING);
		bank.AddToggle("Show Context Stats", &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_CONTEXT);
		bank.AddToggle("Show Kilo Counters", &m_ShowDrawListStatsFlags, 1 << DRAWLISTSTATS_SHOWKILOCOUNTERS);

		bank.AddSeparator();

		bank.AddToggle("Show Draw List Execution", &bShowDrawlistExec);
		bank.AddToggle("Check Lists Once Built", &bCheckListsOnceBuilt);
		bank.AddButton("Dump Out Mem Usage", DumpMemCB);
		bank.AddToggle("Asynchronous Draw Lists", &s_AsyncDrawLists);
#if ENABLE_SINGLE_THREADED_RENDERING_DEBUG
		bank.AddToggle("Force Single Threaded Rendering", &s_ForceSubRenderThread0);
#endif
		bank.AddToggle("Show Frame Numbers", &bShowFrameNumbers);
		//bank.AddButton("Regenerate DL Caches", RegenDLCachesCB);
#if __DEV
		bank.AddToggle("Enable Validation of streaming indices cache", &ms_enableStrIndicesValidation);
#endif
		gDCBuffer->AddWidgets(bank);
	}
	bank.PopGroup();

	bank.PushGroup("LOD", false);
	{
		SetupLODWidgets(bank);
	}
	bank.PopGroup();

	return(true);
}

void dlDrawListMgr::RegisterStatExtraction(u32 numPhases, const char** phases, drawlistStatData* targetBuffer)
{
	Assertf(!numPhases || (phases && targetBuffer), "dlDrawListMgr::RegisterStatExtraction: Bad buffers specified!");
	m_statSaveNumPhases = numPhases;
	m_statSavePhases = phases;
	m_statSaveTargetBuffer = targetBuffer;
}

#if DRAWABLESPU_STATS || DRAWABLE_STATS

static inline u32 TO_KILO(u32 x) { return (x + 1023) >> 10; }

static atString GetStatHeading(int chars, int charsKilo, bool bShowKiloCounters, const char* stat)
{
	return atVarString(atVarString(" %%%ds", bShowKiloCounters ? charsKilo : chars).c_str(), stat);
}

static atString GetStatStr(int chars, int charsKilo, bool bShowKiloCounters, u32 stat)
{
	return atVarString(atVarString(" %%%ds", bShowKiloCounters ? charsKilo : chars).c_str(), atVarString("%d%s", bShowKiloCounters ? TO_KILO(stat) : stat, bShowKiloCounters ? "k" : "").c_str());
}

#endif // DRAWABLESPU_STATS || DRAWABLE_STATS

void dlDrawListMgr::DumpDebugData(void)
{
#if DRAWABLESPU_STATS || DRAWABLE_STATS
	if (m_statSaveNumPhases > 0 && m_statSavePhases && m_statSaveTargetBuffer)
	{
		m_statSaveTargetBuffer[0].totalDrawCalls = 0;
		m_statSaveTargetBuffer[0].totalIndices = 0;

		for (s32 i = 0; i < GetDrawListTypeCount(); ++i)
		{
			if (ms_drawListStats[i] == 0) { continue; }

		#if DRAWABLESPU_STATS
			spuDrawableStats& stats = s_DrawListSpuDrawableStats[!s_StatsWriteIdx][i];
			// save the total across all phases in first entry
			m_statSaveTargetBuffer[0].totalDrawCalls += stats.DrawableDrawCalls + stats.DrawableDrawSkinnedCalls;
			m_statSaveTargetBuffer[0].totalIndices += stats.TotalIndices;
		#else
			drawableStats& stats = s_DrawListDrawableStats[!s_StatsWriteIdx][i];
			m_statSaveTargetBuffer[0].totalDrawCalls += (u16)stats.TotalDrawCalls;
			m_statSaveTargetBuffer[0].totalIndices += (u16) stats.TotalIndices;
		#endif // DRAWABLESPU_STATS

			for (u32 f = 0; f < m_statSaveNumPhases; ++f)
			{
				const char *const str0 = m_statSavePhases[f];
				if (str0 == NULL)
					continue;

				const char *const str1 = s_DrawListNames[i].c_str();
				if (str1 == NULL)
					continue;

				if (strcmp(str0, str1))
					continue;

			#if DRAWABLESPU_STATS
				drawlistStatData& data = m_statSaveTargetBuffer[f];
				data.drawCalls = stats.DrawableDrawCalls;
				data.skinnedDrawCalls = stats.DrawableDrawSkinnedCalls;
				data.entityDrawCalls = stats.EntityDrawCalls;
				data.indices = stats.TotalIndices;
			#else
				drawlistStatData& data = m_statSaveTargetBuffer[f];
				data.drawCalls = (u16)stats.TotalDrawCalls;
				data.skinnedDrawCalls = 0;
				data.entityDrawCalls = 0;
				data.indices = stats.TotalIndices;
			#endif

				break;
			}

			if ( (!m_ShowDrawListStats || m_ShowDrawListStatsFlags == 0 || m_ShowDrawListStatsFlags2 == 0)
#if DEBUG_ENTITY_GPU_COST
				&& ! m_ShowDebugEntityCostStats
#endif
				)
				ms_drawListStats[i] = 0;
		}
	}
#endif // DRAWABLESPU_STATS || DRAWABLE_STATS

#if DEBUG_ENTITY_GPU_COST
	if(m_ShowDebugEntityCostStats)
	{
		grcDebugDraw::TextFontPush(grcSetup::GetMiniFixedWidthFont());
		grcDebugDraw::AddDebugOutputEx(false, "DEBUG ENTITY COST STATS");
		atString line;
		line += atVarString(" %21s", "Drawlist Name"       );

		switch(s_DebugEntityGPUCostMode)
		{
		case DEBUG_ENT_GPU_COST_AVERAGE:
			line += atVarString(" %7s" , "GPU Average"        );
			break;
		case DEBUG_ENT_GPU_COST_IMMEDIATE:
			line += atVarString(" %7s" , "GPU Immediate"        );
			break;
		case DEBUG_ENT_GPU_COST_PEAK:
			line += atVarString(" %7s" , "GPU Peak"        );
			break;
		default:
			line += atVarString(" %7s" , "GPU"        );
		}
		grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
		grcDebugDraw::AddDebugOutputSeparator(3);
		const int ti = GetDrawListTypeCount()-1;

		int shown = 3;
		float result;
		for (int i = 0; i < GetDrawListTypeCount(); i++)
		{
			if (ms_drawListStats[i] == 0 || !s_DebugEntityGpuTimers[i].Enabled) { continue; }
			line += atVarString(" %21s", atVarString("%s"     , s_DrawListNames[i].c_str()    ).c_str());

			switch(s_DebugEntityGPUCostMode)
			{
			case DEBUG_ENT_GPU_COST_AVERAGE:
				result = s_DebugEntityGpuTimers[i].averageTime;
				break;
			case DEBUG_ENT_GPU_COST_IMMEDIATE:
				result = s_DebugEntityGpuTimers[i].result;
				break;
			case DEBUG_ENT_GPU_COST_PEAK:
				result = s_DebugEntityGpuTimers[i].maxTime;
				break;
			default:
				result = 0;
			}
			line += atVarString(" %7s" , atVarString("%.2fms" , result ).c_str());
			grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";

			if (i != ti)
			{
				ms_drawListStats[ti] += ms_drawListStats[i];
				s_DebugEntityGpuTimers[ti].result += s_DebugEntityGpuTimers[i].result;
				s_DebugEntityGpuTimers[ti].averageTime += s_DebugEntityGpuTimers[i].averageTime;
				s_DebugEntityGpuTimers[ti].maxTime += s_DebugEntityGpuTimers[i].maxTime;
			}
			else
			{
				s_DebugEntityGpuTimers[ti].averageTime = 0;
				s_DebugEntityGpuTimers[ti].maxTime = 0;

			}
			s_DebugEntityGpuTimers[i].result = 0;
			if ((++shown & 3) == 3 || i == ti-1)
			{
				grcDebugDraw::AddDebugOutputSeparator(3);
			}
			if (!m_ShowDrawListStats || (m_ShowDrawListStatsFlags & DRAWLISTSTATS_ALLCOLUMNS) == 0)
				ms_drawListStats[i] = 0;
		}
		ms_drawListStats[ti] = 0;

		grcDebugDraw::TextFontPop();
	}
#endif

	if (m_ShowDrawListStats && ((m_ShowDrawListStatsFlags & DRAWLISTSTATS_ALLCOLUMNS) != 0 || m_ShowDrawListStatsFlags2 != 0))
	{
#if DRAWABLESPU_STATS || DRAWABLE_STATS
		const bool bShowKiloCounters = (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_SHOWKILOCOUNTERS)) != 0;
#endif // DRAWABLESPU_STATS || DRAWABLE_STATS

		grcDebugDraw::AddDebugOutputEx(false, "Draw List Stats");

		atString line;

		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_NAME                    )) { line += atVarString(" %21s", "Name"       ); }
		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_NUMLISTS                )) { line += atVarString(" %2s" , "N"          ); }
		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_CREATION                )) { line += atVarString(" %7s" , "Create"     ); }
		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_EXECUTION               )) { line += atVarString(" %7s" , "Exec"       ); }
		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_GPU                     )) { line += atVarString(" %7s" , "GPU"        ); }
#if __DEV
		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_DLC                     )) { line += atVarString(" %5s" , "DLC"        ); }
#endif // __DEV

#if DRAWABLE_STATS
		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_RENDERING))
		{
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_MISSINGTECHNIQUE        )) { line += atVarString(" %4s" , "Miss"       ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_DRAWABLEDRAWCALLS       )) { line += atVarString(" %5s" , "Draw"       ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_DRAWABLEDRAWSKINNEDCALLS)) { line += atVarString(" %5s",  "DrSkn"      ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_MODELSCULLED            )) { line += atVarString(" %6s" , "ModCul"     ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_MODELSDRAWN             )) { line += atVarString(" %6s" , "ModDrw"     ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_GEOMSCULLED             )) { line += atVarString(" %6s" , "GeoCul"     ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_GEOMSDRAWN              )) { line += atVarString(" %6s" , "GeoDrw"     ); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_TOTALIDX                )) { line += GetStatHeading(9, 8, bShowKiloCounters, "TotalIdx"); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_TOTALPRIM               )) { line += GetStatHeading(9, 9, bShowKiloCounters, "TotalPrim"); }
		}

		if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_CONTEXT))
		{
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_PEDS                   )) { line += atVarString(" %4s" , "Peds"       ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_VEHICLES               )) { line += atVarString(" %3s" , "Veh"        ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_PROPS                  )) { line += atVarString(" %5s" , "Props"      ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_LOD                    )) { line += atVarString(" %3s" , "Lod"        ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD1                  )) { line += atVarString(" %5s" , "SLod1"      ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD2                  )) { line += atVarString(" %5s" , "SLod2"      ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD3                  )) { line += atVarString(" %5s" , "SLod3"      ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD4                  )) { line += atVarString(" %5s" , "SLod4"      ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_VEG                    )) { line += atVarString(" %3s" , "Veg"        ); }
			if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_NO_CATEGORY            )) { line += atVarString(" %4s" , "None"       ); }
		}
#endif // DRAWABLE_STATS

		grcDebugDraw::AddDebugOutputEx(false, line.c_str()); line = "";
		grcDebugDraw::AddDebugOutputSeparator(3);

		int shown = 3;

		const int ti = GetDrawListTypeCount()-1;

		u32 frameIndex = (m_UpdateThreadFrameNumber-1) & 1;

		for (int i = 0; i < GetDrawListTypeCount(); i++)
		{
			if (ms_drawListStats[i] == 0) { continue; }
#if __DEV
			const int instCount = ms_instructionCount[i];
#endif // __DEV
#if DRAWABLESPU_STATS
			const spuDrawableStats& stats = s_DrawListSpuDrawableStats[!s_StatsWriteIdx][i];
#elif DRAWABLE_STATS
			const drawableStats& stats = s_DrawListDrawableStats[!s_StatsWriteIdx][i];
#endif
#if DRAWABLESPU_STATS
			// EDGE currently will only possibly run on the first four SPU's.
			const u32 longestWait = Max<u32>(
				Max<u32>(stats.EdgeOutputWaitTicks[0], stats.EdgeOutputWaitTicks[1]),
				Max<u32>(stats.EdgeOutputWaitTicks[2], stats.EdgeOutputWaitTicks[3]));
#endif // DRAWABLESPU_STATS

			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_NAME                    )) { line += atVarString(" %21s", atVarString("%s"     , s_DrawListNames[i].c_str()    ).c_str()); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_NUMLISTS                )) { line += atVarString(" %2s" , atVarString("%d"     , ms_drawListStats[i]           ).c_str()); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_CREATION                )) { line += atVarString(" %7s" , atVarString("%.2fms" , ms_drawListCreationTiming[frameIndex][i]  ).c_str()); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_EXECUTION               )) { line += atVarString(" %7s" , atVarString("%.2fms" , ms_drawListExecutionTiming[frameIndex][i] ).c_str()); }
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_GPU                     )) { line += atVarString(" %7s" , atVarString("%.2fms" , s_DrawListGpuTimers[i].result ).c_str()); }
#if __DEV
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_DLC                     )) { line += atVarString(" %5s" , atVarString("%d"     , instCount                     ).c_str()); }
#endif // __DEV

			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_RENDERING))
			{
#if DRAWABLE_STATS
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_MISSINGTECHNIQUE        )) { line += atVarString(" %4s" , atVarString("%d"     , stats.MissingTechnique        ).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_DRAWABLEDRAWCALLS       )) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawableDrawCalls       ).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_DRAWABLEDRAWSKINNEDCALLS)) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawableDrawSkinnedCalls).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_MODELSCULLED            )) { line += atVarString(" %6s" , atVarString("%d"     , stats.ModelsCulled            ).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_MODELSDRAWN             )) { line += atVarString(" %6s" , atVarString("%d"     , stats.ModelsDrawn             ).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_GEOMSCULLED             )) { line += atVarString(" %6s" , atVarString("%d"     , stats.GeomsCulled             ).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_GEOMSDRAWN              )) { line += atVarString(" %6s" , atVarString("%d"     , stats.GeomsDrawn              ).c_str()); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_TOTALIDX                )) { line += GetStatStr(9, 8, bShowKiloCounters, stats.TotalIndices); }
				if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_TOTALPRIM               )) { line += GetStatStr(9, 9, bShowKiloCounters, stats.TotalPrimitives); }
#endif // DRAWABLE_STATS
			}

#if DRAWABLE_STATS
			if (m_ShowDrawListStatsFlags & (1 << DRAWLISTSTATS_CONTEXT))
			{
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_PEDS                   )) { line += atVarString(" %4s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_PEDS]       ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_VEHICLES               )) { line += atVarString(" %3s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_VEHICLES]   ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_PROPS                  )) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_PROPS]      ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_LOD                    )) { line += atVarString(" %3s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_LOD]        ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD1                  )) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_SLOD1]      ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD2                  )) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_SLOD2]      ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD3                  )) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_SLOD3]      ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_SLOD4                  )) { line += atVarString(" %5s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_SLOD4]      ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_VEG                    )) { line += atVarString(" %3s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_VEG]        ).c_str()); }
				if (m_ShowDrawListStatsFlags2 & (1 << DRAWLISTSTATS_NO_CATEGORY            )) { line += atVarString(" %4s" , atVarString("%d"     , stats.DrawCallsPerContext[DCC_NO_CATEGORY]).c_str()); }
			}
#endif // DRAWABLE_STATS

			const Color32 colour = (i == m_ShowDrawListStatsIndex || m_ShowDrawListStatsIndex == -1) ? Color32(0.9f, 0.9f, 0.9f) : Color32(0.5f, 0.5f, 0.5f);
			grcDebugDraw::AddDebugOutputEx(false, colour, line.c_str()); line = "";

			if (i != ti)
			{
				ms_drawListStats[ti] += ms_drawListStats[i];
				ms_drawListCreationTiming[frameIndex][ti] += ms_drawListCreationTiming[frameIndex][i];
				ms_drawListExecutionTiming[frameIndex][ti] += ms_drawListExecutionTiming[frameIndex][i];
				s_DrawListGpuTimers[ti].result += s_DrawListGpuTimers[i].result;
				DEV_ONLY(ms_instructionCount[ti] += ms_instructionCount[i]);
#if DRAWABLE_STATS
				drawableStats& total = s_DrawListDrawableStats[!s_StatsWriteIdx][ti];
				total.TotalPrimitives += stats.TotalPrimitives;
				total.TotalIndices += stats.TotalIndices;
				total.MissingTechnique += stats.MissingTechnique;
				total.DrawableDrawCalls += stats.DrawableDrawCalls;
				total.DrawableDrawSkinnedCalls += stats.DrawableDrawSkinnedCalls;
				total.ModelsCulled += stats.ModelsCulled;
				total.ModelsDrawn += stats.ModelsDrawn;
				total.GeomsCulled += stats.GeomsCulled;
				total.GeomsDrawn += stats.GeomsDrawn;
				total.TotalIndices += stats.TotalIndices;

				for (int j = 0; j < DCC_MAX_CONTEXT; j++)
				{
					total.DrawCallsPerContext[j] += stats.DrawCallsPerContext[j];
				}
#endif // DRAWABLE_STATS
			}
			else
			{
				s_DrawListGpuTimers[ti].result = 0;
#if DRAWABLE_STATS
				drawableStats& total = s_DrawListDrawableStats[!s_StatsWriteIdx][ti];
				memset(&total,0,sizeof(total));
#endif
			}

			ms_drawListStats[i] = 0;

			if ((++shown & 3) == 3 || i == ti-1)
			{
				grcDebugDraw::AddDebugOutputSeparator(3);
			}
		}

		// Add some artist-friendly help if we're in Terse Mode.
		if (m_ShowDrawListStatsFlags == s_TerseFlags)
		{
			const Color32 colour = Color32(0.9f, 0.9f, 0.9f);
			grcDebugDraw::AddDebugOutputSeparator(3);
			grcDebugDraw::AddDebugOutputEx(false,colour,"TotalIdx is the number of indices sent down directly to GPU, all assumed visible.");
		}
	}
}
#endif //__BANK

} // namespace rage
