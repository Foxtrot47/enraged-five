// 
// fwdrawlist/drawlist_channel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWDRAWLIST_DRAWLIST_CHANNEL_H 
#define FWDRAWLIST_DRAWLIST_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(drawlist)

#define dlAssert(cond)						RAGE_ASSERT(drawlist,cond)
#define dlAssertf(cond,fmt,...)				RAGE_ASSERTF(drawlist,cond,fmt,##__VA_ARGS__)
#define dlFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(drawlist,cond,fmt,##__VA_ARGS__)
#define dlVerifyf(cond,fmt,...)				RAGE_VERIFYF(drawlist,cond,fmt,##__VA_ARGS__)
#define dlErrorf(fmt,...)					RAGE_ERRORF(drawlist,fmt,##__VA_ARGS__)
#define dlWarningf(fmt,...)					RAGE_WARNINGF(drawlist,fmt,##__VA_ARGS__)
#define dlDisplayf(fmt,...)					RAGE_DISPLAYF(drawlist,fmt,##__VA_ARGS__)
#define dlDebugf1(fmt,...)					RAGE_DEBUGF1(drawlist,fmt,##__VA_ARGS__)
#define dlDebugf2(fmt,...)					RAGE_DEBUGF2(drawlist,fmt,##__VA_ARGS__)
#define dlDebugf3(fmt,...)					RAGE_DEBUGF3(drawlist,fmt,##__VA_ARGS__)
#define dlLogf(severity,fmt,...)			RAGE_LOGF(drawlist,severity,fmt,##__VA_ARGS__)

#define DRAWLIST_OPTIMISATIONS_OFF	0

#if DRAWLIST_OPTIMISATIONS_OFF
#define DRAWLIST_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define DRAWLIST_OPTIMISATIONS()
#endif	//DRAWLIST_OPTIMISATIONS_OFF

#endif // FWDRAWLIST_DRAWLIST_CHANNEL_H
