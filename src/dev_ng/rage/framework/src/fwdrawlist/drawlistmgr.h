//
// FILE :    fwdrawlist/drawlistmgr.h
// PURPOSE : manage drawlist & execute operations on them
// AUTHOR :  john.
// CREATED : 30/8/06
//
/////////////////////////////////////////////////////////////////////////////////



#ifndef FWDRAWLIST_DRAWLISTMGR_H_
#define FWDRAWLIST_DRAWLISTMGR_H_

#include "drawlist.h"

#include "atl/pool.h"
#include "grcore/effect_mrt_config.h"
#include "profile/timebars.h"
#include "system/criticalsection.h"
#include "system/messagequeue.h"
#include "system/dependency.h"
#include "system/threadtype.h"

#include "fwrenderer/renderthread.h"
#include "streaming/streamingdefs.h"
#include "streaming/streamingengine.h"

namespace rage {

class clothVariationData;
class fwCustomShaderEffectBaseType;
class strStreamingModule;
class fwArchetype;

// at any time we can have many built and executed lists, but only 1 each at most of building and executing
enum eDrawListState {

	DLS_BUILDING = 1,			// rage_new commands are added to this draw list
	DLS_BUILT,					// no more rage_new commands are to be added to this draw list. Ready for execution.
	DLS_EXECUTING,				// commands are being executed from this draw list.
	DLS_EXECUTED,				// all of the commands in this draw list have been executed.
};

#define MAX_DRAWLISTS		(128)
#define MAX_TXD_REFS		(32)
#define MAX_DRAWLIST_DEPENDENCY_INFOS	(40)
#define FORCED_TECHNIQUE_STACK_SIZE (2)
#define AVERAGE_CLOTH_DRAWS_PER_FRAME (4)
#define MAX_CLOTH		(160 * AVERAGE_CLOTH_DRAWS_PER_FRAME)
#define MAX_CLOTH_REFS	(MAX_CLOTH * 2 * 2)
#define FENCE_COUNT			(4)
#define STREAMING_INDICES_BUFFER_COUNT (__PS3 ? (FENCE_COUNT - 1) : FENCE_COUNT)
//Enabling GPU Cost for entity only for PS3 right now.
#define DEBUG_ENTITY_GPU_COST (__PS3 && __BANK) 

#if DEBUG_ENTITY_GPU_COST
enum eDebugEntityGpuCostMode
{
	DEBUG_ENT_GPU_COST_IMMEDIATE = 0,
	DEBUG_ENT_GPU_COST_AVERAGE,
	DEBUG_ENT_GPU_COST_PEAK,
	DEBUG_ENT_GPU_COST_TOTAL
};
#endif
struct clothRecord {
	s32 m_refCount;
	void* m_memPtr;
	void* m_envCloth;
	bool m_removeRequested;
};

struct charClothRecord {
	clothVariationData* m_clothVarData;
	s32 m_refCount;
	bool m_removeRequested;
};

struct dlForcedCleanupCallback {
	dlForcedCleanupCallback *	next;
	void						(*func)(void*);
	// user data for function call follows starting at the next 16-byte aligned address
};

class dlDrawListInfo{

public:
	DrawListAddress				m_offset;					// offset specifies location in the draw command buffer of start of this draw list
	u32							m_listEndIdx;				// idx of the final instruction
	eDrawListType				m_type;						// type of draw list (associated with which render phase?)
	eDrawListState				m_state;					// current state of this draw list
	s32							m_Id;						// unique id for this draw list
	s32							m_DepInfoIndex;				// index for the dependency info for this draw list (-1 if there were no dependencies)
	void *						m_UserData;					// Subclasses can put their junk in here
	dlForcedCleanupCallback *	m_ForcedCleanupCallbacks;	// Linked list of callbacks to be executed when dlDrawListMgr::RemoveAll is called
#if __DEV
	u32							m_FrameNumber;				// The frame number - tagged by the main thread upon creation
	u32							m_StrIndicesCacheIndex;
#endif // __DEV
};

typedef sysMessageQueue<dlDrawListInfo *, MAX_DRAWLISTS> DrawListQ;

extern __THREAD dlDrawListInfo* pCurrBuildingList;
extern __THREAD dlDrawListInfo* pCurrExecutingList;

class dlDrawListMgr{

	friend class dlCmdBase;
	friend class dlcCmdNewDrawList;
	friend class dlcCmdEndDrawList;

	struct ReferencedStreamable
	{
		ReferencedStreamable() : m_Module(NULL), m_moduleIndex(0xffff)	{ }
		ReferencedStreamable(u32 index, strStreamingModule *module) : m_Module(module), m_moduleIndex(index)	{ }

		strStreamingModule *m_Module;
		u32					m_moduleIndex;

		bool IsEquivalent(const u32 &modIndex, const strStreamingModule* pMod) const	{ return((pMod == m_Module) && (modIndex == m_moduleIndex)); }
		bool operator ==(const ReferencedStreamable &rhs) const	{ return ((rhs.m_Module == m_Module) && (rhs.m_moduleIndex == m_moduleIndex)); }
	};

public:
	dlDrawListMgr();
	virtual ~dlDrawListMgr();

	void Init(int drawListTypeCount);
	virtual void Shutdown();
	virtual void Reset();

	virtual bool NewDrawList(eDrawListType type, DrawListAddress::Parameter offset);

	// Terminate current draw list.
	void EndCurrentDrawList();

	// for render thread we execute draw lists in parallel with main thread, but clean up only when safe
	void ExecuteAndRemoveAll();

	void CleanupDrawList(dlDrawListInfo *);

	// reset a draw list info after we no longer need it
	virtual void ResetDrawListInfo(dlDrawListInfo & /*drawListInfo*/) { }

#if DRAWABLE_STATS
	const drawableStats& GetDrawableStats(int phase);
#elif __PS3 && DRAWABLESPU_STATS
	const spuDrawableStats&	GetDrawableStats(int phase);
#endif

	enum
	{
#if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD
		// Indicates that a drawlist must be executed on the main render thread.
		// Only valid if MULTIPLE_RENDER_THREADS_ALLOW_DRAW_LISTS_ON_RENDER_THREAD is set.
		SUBCPUS_MAIN            = 0x00,
#endif

		// Indicates that a drawlist is for synchronization only, and causes the
		// main render thread to block on all drawlists up to this point having
		// completed.
		SUBCPUS_SYNC_MAIN       = 0x80,

		// Not really a magic value, just a handy mask of all sub render threads.
		SUBCPUS_ALL             = (1<<MULTIPLE_RENDER_THREADS)-1,
	};

	// Set debugging info for a draw list
	// subcpus is a bit mask of the subrender threads (0 up to a max of 6) that the drawlist is allowed to be executed on,
	// or one of the SUBCPUS_xxx special values.
	void SetDrawListInfo(int drawListIndex, const char *name, u8 subcpus, int LODFlagShift, bool bEnableGPUCostEntityDebug, float updateBudget, float renderBudget, float gpuBudget, bool timebar);
	void SetDrawListSubCPUs(int drawListIndex, u8 subcpus);
	void GroupDrawListTimebars(int firstDrawListIndex, int lastDrawListIndex, const char *name);

	// PURPOSE: Return the debugging name of a draw list.
	static const char *GetDrawListName(int drawListIndex);

	int GetDrawListLODFlagShift(int drawListIndex) const;

#if !__FINAL
	// PURPOSE: Return the GPU time of a draw list.
	virtual float GetDrawListGPUTime(int drawListIndex) const;

#if DEBUG_ENTITY_GPU_COST
	void SetDebugEntityGPUTime(int drawListIndex, float result);
	float GetDebugEntityGPUTime(int drawListIndex) const;
	void StartDebugEntityTimer();
	void StopDebugEntityTimer();
	void UpdateDebugEntityGPUCostMaxAndAverage();
	void SetDebugEntityGPUCostMode(eDebugEntityGpuCostMode mode);
	eDebugEntityGpuCostMode GetDebugEntityGPUCostMode();
	void ResetGPUCostTimers();
#endif

    // PURPOSE: Return the number of entities contributing to a draw list.
    u32 GetDrawListEntityCount(int drawListIndex) const;

    // PURPOSE: Increments the number of entities contributing to the draw list being constructed.
    void IncrementDrawListEntityCount();

    // PURPOSE: Reset the number counts of entities contributing to draw lists.
    void ResetDrawListEntityCounts();

	u32 GetTotalIndices() const;

	u32 GetGBufIndices() const;
#endif // !__FINAL

#if RAGE_TIMEBARS
	float GetTimebarUpdateBudget(int drawListIndex) const;
	float GetTimebarRenderBudget(int drawListIndex) const;
#endif // RAGE_TIMEBARS

	void RemoveAll();

	bool HasPendingDrawLists() const;
	
#if __BANK
	void RegisterDrawlist(eDrawListType DLType);
#else //__BANK
	void RegisterDrawlist(eDrawListType) {}
#endif //__BANK

#if __DEV
	s32	GetCurrBuildListIdx() ;
	s32	GetCurrExecListIdx() ;

	bool	CheckBuildListIntegrity(s32 lastIdx = -1);
#endif //__DEV

	void	FlipBufferIdx(void);
	void	FlipRenderFenceIdx(void);
	void	FlipUpdateFenceIdx();

	DrawListQ&	GetBuildDrawListQ(void) { return(m_DrawList);}
	DrawListQ&	GetRenderDrawListQ(void) {return(m_DrawList);}
	const DrawListQ&	GetRenderDrawListQ(void) const {return(m_DrawList);}

	void	TerminateDrawLists();

	// PURPOSE: Let the system know that we're starting to create draw lists for this frame.
	void	BeginDrawListCreation();

	// PURPOSE: Let the system know that we're done creating draw lists for this frame.
	void	EndDrawListCreation();
	
	void	AddTxdReference(u32);
	void	AddDwdReference(u32);
	void	AddFragReference(u32);
	void	AddDrawableReference(u32);

	void	AddClothReference(void* pEnvCloth);
	void	AddCharClothReference(clothVariationData* clothVarData);

	void	AddTypeFileReference(u32 mapTypeDataSlotIdx);

	void	AddMapDataReference(u32);
	void	AddArchetypeReference(u32);
	void	AddArchetype_HD_Reference(u32);

	// Flags for RemoveAllRefs
	enum
	{
		// Remove refs without the usual flipping.  This causes the most
		// recently added refs to be removed immediately rather than after the
		// standard n-frame delay.  Render thread must be idle for this to be
		// safe.
		FLUSH_WITHOUT_RENDERING         = 0x00000001,
	};
	virtual void	RemoveAllRefs(u32 flags=0);

	bool	HasRefs() const;

	dlDrawListInfo* GetCurrExecDLInfo(void);
	dlDrawListInfo* GetBuildExecDLInfo(void);

	int		GetDrawListTypeCount() const				{ return m_DrawListTypeCount; }

	// Render thread: Set the GPU fence handle that has just been inserted.
	void	InsertEndFence();

	// Update thread: Get the GPU fence handle that the render thread set two frames ago.
	grcFenceHandle	GetEndFenceHandle(void);

	// Update thread: Clear the GPU fence handle that the update thread just blocked on.
	void	ClearEndFenceHandle(void);

	// Render thread: Indicate that this thread will eventually insert a GPU fence.
	void	PrimeEndFenceHandle();

	// Render thread: Indicate that this thread will not insert a GPU fence this frame.
	void	SetSkippedFence();

	// Update thread: Add a dummy fence so two frames from now, we'll think there is a valid
	// fence. Should only be used when flushing the system.
	void	AddDummyFence();

	// Update thread: Check whether the render thread didn't insert a GPU fence two frames ago.
	bool	IsFenceSkipped() const;

	void PushForcedTechnique(s32 techId);
	
	s32 PopForcedTechnique(void);
	
	void ResetForcedTechnique();

	__forceinline bool IsBuildingDrawList(void);
	__forceinline bool IsExecutingDrawList(void);
	__forceinline bool IsBuildingDrawList(eDrawListType drawListType);
	__forceinline bool IsExecutingDrawList(eDrawListType drawListType);

	float GetRatioTilesCulled(void){ return m_RatioTilesCulled[GetShadowTile()];}
	void SetRatioTilesCulled(float fNum){ m_RatioTilesCulled[GetShadowTile()] = fNum;}

	int GetShadowTile(void){ return m_NumOfShadowTiles;}
	void SetShadowTile(int Num){ m_NumOfShadowTiles = Num;}

	enum eHighestDrawableLOD
	{
		DLOD_HIGH,
		DLOD_MED,
		DLOD_LOW,
		DLOD_VLOW,
		DLOD_COUNT,

		DLOD_DEFAULT = DLOD_HIGH,
	};

	enum eHighestVehicleLOD
	{
		VLOD_HD,
		VLOD_HIGH,
		VLOD_MED,
		VLOD_LOW,
        VLOD_LOWER,
		VLOD_COUNT,

		VLOD_DEFAULT = VLOD_HD,
	};

	enum eHighestPedLOD
	{
		PLOD_DEFAULT,
		PLOD_LOD,
		PLOD_SLOD,
		PLOD_COUNT
	};

#if __BANK
	int m_HighestDrawableLOD_debug;
	int m_HighestFragmentLOD_debug;
	int m_HighestVehicleLOD_debug;
	int m_HighestPedLOD_debug;
	static DECLARE_MTR_THREAD u16 m_DrawableStatContext;

	u16 GetDrawableStatContext(void) { return m_DrawableStatContext;}
	void SetDrawableStatContext(u16 stat) { m_DrawableStatContext = stat; }

#if DEBUG_ENTITY_GPU_COST
	void SetShowGPUStatsForEntity(bool bEnable){m_ShowDebugEntityCostStats = bEnable;}
	bool GetShowGPUStatsForEntity(){return m_ShowDebugEntityCostStats;}
#endif
#endif // __BANK

	struct PER_THREAD_LOD_INFO
	{
		int m_HighestDrawableLOD;
		int m_HighestFragmentLOD;
		int m_HighestVehicleLOD;
		int m_HighestPedLOD;
	};

	// One for the game thread, NUMBER_OF_RENDER_THREADS for the rendering thread side.
	PER_THREAD_LOD_INFO m_LODInfo[NUMBER_OF_RENDER_THREADS + 1];

	static void SetHighestDrawableLOD(bool bRenderThread, eHighestDrawableLOD drawableLOD);
	static void SetHighestFragmentLOD(bool bRenderThread, eHighestDrawableLOD fragmentLOD); // not vehicles
	static void SetHighestVehicleLOD(bool bRenderThread, eHighestVehicleLOD vehicleLOD);
	static void SetHighestPedLOD(bool bRenderThread, eHighestPedLOD pedLOD);
	static void SetHighestLOD(bool bRenderThread, eHighestDrawableLOD drawableLOD, eHighestDrawableLOD fragmentLOD, eHighestVehicleLOD vehicleLOD, eHighestPedLOD pedLOD);
	static void ResetHighestLOD(bool bRenderThread);

	void AdjustDrawableLOD(bool bRenderThread, u32& lod);
	void AdjustFragmentLOD(bool bRenderThread, u32& lod, bool bIsVehicle);
	void AdjustVehicleHD(bool bRenderThread, bool& bAllowHD);
	void AdjustPedLOD(bool bRenderThread, u32& lod);

	const Vector3& GetCalcPosDrawableLOD(void) { return m_calcPosDrawableLOD;}
	void SetCalcPosDrawableLOD(Vector3& pos) { m_calcPosDrawableLOD = pos; }

	int				GetRtRenderMode(void) { return	m_rtRenderMode; }
	void			SetRtRenderMode(int rm) { m_rtRenderMode = rm; }

	u32				GetRtBucket(void) { return m_rtBucket; }
	void			SetRtBucket(u32 bm) { m_rtBucket = bm; }

	u32				GetRtSubBucketMask(void) { return m_rtSubBucketMask; }
	void			SetRtSubBucketMask(u32 bm) { m_rtSubBucketMask = bm; }

	u32				GetRtBucketMask(void) { return PackBucketMask(m_rtBucket,m_rtSubBucketMask); }

	int				GetUpdateRenderMode(void) { return	m_updtRenderMode; }
	void			SetUpdateRenderMode(int rm) { m_updtRenderMode = rm; }

	u32				GetUpdateBucket(void) { return m_updtBucket; }
	void			SetUpdateBucket(u32 bm) { m_updtBucket = bm; }

	u32				GetUpdateSubBucketMask(void) { return m_updtSubBucketMask; }
	void			SetUpdateSubBucketMask(u32 bm) { m_updtSubBucketMask = bm; }

	u32				GetUpdateBucketMask(void) { return PackBucketMask(m_updtBucket,m_updtSubBucketMask); }
	
	u32				GetDefaultSubBucketMask() { return m_defSubBucketMask; }
	void			SetDefaultSubBucketMask(u32 sbm) { m_defSubBucketMask = sbm; }
	
	static u32		PackBucketMask(u32 b, u32 bm) { return ((bm << 8) & 0xff00) | ((1U << b) & 0xff); }
#if !__FINAL
	void			DebugDraw();
#endif // !__FINAL

	void *			AddForcedCleanupCallback(void (*func)(void*), size_t userDataSize);

#if __ASSERT
	// PURPOSE: TRUE if we're currently ending the draw list (i.e. we're inside EndCurrentDrawList).
	bool			IsEndingDrawList() const		{ return m_IsEndingDrawList; }

	// PURPOSE: TRUE if it's OK to create new draw lists.
	bool IsDrawListCreationAllowed() const			{ return m_IsDrawListCreationAllowed; }
#endif // __ASSERT

#if __BANK
	static void RequestShaderReload() { ms_ShaderReloadRequested = true; }
	void SetupLODWidgets(bkGroup& bank, int* highestDrawableLOD = NULL, int* highestFragmentLOD = NULL, int* highestVehicleLOD = NULL, int* highestPedLOD = NULL);
	virtual bool SetupWidgets(bkBank& bank);

	struct drawlistStatData
	{
		u32 totalIndices;
		u32 indices;
		u16 drawCalls;
		u16 skinnedDrawCalls;
		u16 totalDrawCalls;
		u16 entityDrawCalls;

	};
	void RegisterStatExtraction(u32 numPhases, const char** phases, drawlistStatData* targetBuffer);
#endif // __BANK

	size_t GetClothPoolFreeCount()		{ return m_ClothsPool.GetFreeCount(); }
	void ClothAdd(void* pEnvCloth);
	void ClothRemove(void* pEnvCloth, void* preAllocatedMemory);

	void CharClothAdd(clothVariationData* pClothVarData);
	void CharClothRemove(clothVariationData* pClothVarData);

	// WARNING: These sysDependency's are used simply as a lightweight(ish) way
	// to kick individual jobs.  They must not be linked together in dependency
	// graphs.  Once the dependency has been inserted into the dependency
	// scheduler, it should no longer be dereferenced by calling code, as the
	// sysDependency struct may be recycled in a future call to
	// CreateDrawlistDependency.
	//
	// This may be changed at a later stage if _really_ needed (maybe adding a
	// non-recyclable flag), but at the moment that additional complication is
	// unnecessary.
	//
	sysDependency& CreateDrawlistDependency();

	void JoinAllDrawlistDependencies();
	void JoinDrawlistDependencies(int depInfoIndex);

	// Submit pending dependencies for the current drawlist; returns true if dependencies were submitted, false otherwise
	bool SubmitPendingDependencies();

	enum eFenceStatus
	{
		FENCE_NONE,		// There is no fence for this frame, and there won't be.
		FENCE_UNSET,	// There will be a fence for this frame, but it's not set yet.
		FENCE_SET,		// There is a fence for this frame, ready to be blocked on.
		FENCE_SKIPPED,	// We didn't actually render anything this frame.
		FENCE_DUMMY,	// Dummy fence - there's no fence, but we know that we're safe. Used during flush.
	};

	bool IsDrawListCreationDone() const		{ return m_UpdateThreadState == UPDATE_WAITING_FOR_RT; }

	void SetToPreDrawListState()			{ m_UpdateThreadState = UPDATE_PRE_DRAWLIST; }

#if !__FINAL || __FINAL_LOGGING
	u32	GetUpdateThreadFrameNumber() const	{ return m_UpdateThreadFrameNumber; }
#endif // !__FINAL

	void AddRefCountedModuleIndex(u32 objIndex, strStreamingModule* pModule);
	void AddRefCountedCustomShaderEffectType(fwCustomShaderEffectBaseType *pType);
	void AddRefCountedArchetypeHD(fwArchetype* pArch);
	void SetRatchet(bool ratchet) { Assert(!ratchet); m_ratchet |= ratchet;}

	bool ValidateModuleIndexHasBeenCached(s32 moduleIndex, strStreamingModule* pModule);

protected:
	// Terminate the current draw list.
	void EndDrawList(u32 listEndIdx);

	enum UpdateThreadState {
		// Initial state.
		UPDATE_INITIALIZED,

		// We haven't started creating draw lists yet
		UPDATE_PRE_DRAWLIST,

		// We're in the process of creating draw lists
		UPDATE_CREATING_DRAWLISTS,

		// We pushed the terminating draw list and are going to block for the RT soon
		UPDATE_WAITING_FOR_RT,
	}							m_UpdateThreadState;


#if !__FINAL || __FINAL_LOGGING
	// Running counter on the update thread. Incremented each time we create a new draw list.
	u32	m_UpdateThreadFrameNumber;
#endif // !__FINAL

	u32	m_numReadyLists;

	u32	m_drawListIdx;
	u32 m_fenceUpdateIdx;
	u32 m_fenceRenderIdx;
	u32 m_streamingIndicesBufferIndex;

	Vector3			m_calcPosDrawableLOD;

	DrawListQ					m_DrawList;
	atPool<dlDrawListInfo>		m_DrawListPool;

	atArray<clothRecord*>		m_aClothRefs[FENCE_COUNT];
	atArray<charClothRecord*>	m_aCharClothRefs[FENCE_COUNT];

	atArray<u32>							m_aMapDataRefs;

	atArray<ReferencedStreamable>			m_aStrIndicesCache[STREAMING_INDICES_BUFFER_COUNT];
	atArray<fwCustomShaderEffectBaseType*>	m_customShaderEffectTypes[FENCE_COUNT];
	atArray<fwArchetype*>					m_HDArchetypeRefs[FENCE_COUNT];

	atPool<clothRecord>			m_ClothsPool;
	atArray<clothRecord*>		m_Cloths;

	atPool<charClothRecord>		m_CharClothsPool;
	atArray<charClothRecord*>	m_CharCloths;

	sysCriticalSectionToken		m_DrawListPoolCs;

	grcFenceHandle	m_endFenceHandle[FENCE_COUNT];		// store the handle for the end fence for the current / previous frame
	eFenceStatus	m_fenceStatus[FENCE_COUNT];

	// adaptive Z culling variables
	//u32 m_NumTilesTested;
	//u32 m_NumTilesCulled;
	float		m_RatioTilesCulled[8];
	u32			m_NumOfShadowTiles;

	// PURPOSE: Number of different draw list types
	int			m_DrawListTypeCount;

	// render mode & bucket context whilst executing on the render thread
	static DECLARE_MTR_THREAD int			m_rtRenderMode;
	static DECLARE_MTR_THREAD u32			m_rtBucket;
	// These are the upper 8 bits the bucket mask (see BUCKETMASK_MATCH in effect_config.h).
	static DECLARE_MTR_THREAD u32			m_rtSubBucketMask;
	static DECLARE_MTR_THREAD s32			m_forcedTechniqueStack[FENCE_COUNT][FORCED_TECHNIQUE_STACK_SIZE];
	static				 s32			m_forcedTechniqueStackIdx[FENCE_COUNT][NUMBER_OF_RENDER_THREADS];
	
	// render mode & bucket context whilst building draw list on the update thread
	int			m_updtRenderMode;
	u32			m_updtBucket;
	u32			m_updtSubBucketMask;
	
	// default SubBucketMask;
	u32			m_defSubBucketMask;
	
	// dependency array and count for all drawlist-related SPU stuff
	enum { MAX_DEPENDENCY_COUNT = 512 };

	struct DrawListDependencyInfo
	{
		u32			m_FinishedCount;
		u32			m_ExpectedCount;
		sysIpcSema	m_Sema;
	};

	atFixedArray< sysDependency, MAX_DEPENDENCY_COUNT >		m_dependencies;
	atFixedArray< DrawListDependencyInfo, MAX_DRAWLIST_DEPENDENCY_INFOS>	m_dependencyInfos;

	// HACK talk to klaas before changing this
	u32			m_submittedDependencyCount:31;
	u32			m_ratchet:1;
	// HACK talk to klaas before changing this
	u32			m_dependencyInfoIndex;
	u32			m_nextJoinDrawListIndex;

#if __DEV
	// The frame number that the update thread will tag all new draw lists with
	u32			m_FrameNumber;
#endif // __DEV

#if __ASSERT
	bool		m_IsEndingDrawList;

	// If true, it's okay to create new draw lists
	bool		m_IsDrawListCreationAllowed;
#endif // __ASSERT

#if DEBUG_ENTITY_GPU_COST
	bool		m_ShowDebugEntityCostStats;
#endif
#if __BANK
	bool		m_ShowDrawListStats;
	int			m_ShowDrawListStatsIndex; // highlight index
	u32			m_ShowDrawListStatsFlags;
	u32			m_ShowDrawListStatsFlags2;

	u32					m_statSaveNumPhases;
	const char**		m_statSavePhases;
	drawlistStatData*	m_statSaveTargetBuffer;

	void DumpDebugData();
	static void TerseMode(void *that);
public:
	void SetStatsHighlight(bool bEnable, const char* name = NULL, bool bShowRenderingStats = true, bool bShowContextStats = true, bool bShowKiloCounters = true); // public interface to set the stats mode
	static eDrawListType GetCurrDLType() { return ms_CurrDLType; }
#if __DEV
	static bool StrIndicesValidationEnabled() { return ms_enableStrIndicesValidation; }
#endif // __DEV
protected:

	static atArray<s32>		ms_drawListStats;
	static s32				ms_numDrawlists;
	static eDrawListType	ms_CurrDLType;					// Type of current draw list being created
	static bool				ms_ShaderReloadRequested;
	static atArray<float>	ms_drawListExecutionTiming[2];	// Amount of ms spent executing this draw list
	static atArray<float>	ms_drawListCreationTiming[2];	// Amount of ms spent creating this draw list
	static utimer_t			ms_drawListCreationStartTime;	// Temporary value to measure the amount of time spent creating this draw list
#if __DEV
	static atArray<int>		ms_instructionCount;			// Number of DLC instructions in each list type
	static bool				ms_enableStrIndicesValidation;
#endif // __DEV
#endif //__BANK

#if !__FINAL
public:
	static void SuppressAddTypeFileReferencesBegin();
	static void SuppressAddTypeFileReferencesEnd();
protected:
	static int				ms_suppressAddTypeFileReferences;
#endif // !__FINAL
};

// determine if we are going to add commands to a DL or execute them immediately
// ie. if the current thread is building a DL then add a command
__forceinline bool dlDrawListMgr::IsBuildingDrawList(void)
{
	//check to see if we are currently building a draw list
	return (NULL != pCurrBuildingList);
}

__forceinline bool dlDrawListMgr::IsExecutingDrawList(void)
{
	//check to see if we are currently building a draw list
	return (NULL != pCurrExecutingList);
}

__forceinline bool dlDrawListMgr::IsBuildingDrawList(eDrawListType drawListType)
{
	Assert(sysThreadType::IsUpdateThread());
	return (pCurrBuildingList && pCurrBuildingList->m_type==drawListType);
}

__forceinline bool dlDrawListMgr::IsExecutingDrawList(eDrawListType drawListType)
{
	Assert(sysThreadType::IsRenderThread());
	return (pCurrExecutingList && pCurrExecutingList->m_type==drawListType);
}


inline void dlDrawListMgr::AddRefCountedModuleIndex(u32 objIndex, strStreamingModule *module)
{
//	FastAssert((u32) objIndex < 0xffff);

	atArray<ReferencedStreamable> &archStrIndices = m_aStrIndicesCache[m_streamingIndicesBufferIndex];
	const bool bSpace = archStrIndices.GetCount() < archStrIndices.GetCapacity();
	Assertf(bSpace, "dlDrawListMgr::AddRefCountedModuleIndex - m_aStrIndicesCache[%i] is full.", m_streamingIndicesBufferIndex);
	if (bSpace)
	{
		ReferencedStreamable &refStream = archStrIndices.Append();
		refStream.m_moduleIndex = objIndex;
		refStream.m_Module = module;
	}
}

inline void dlDrawListMgr::AddRefCountedCustomShaderEffectType(fwCustomShaderEffectBaseType *pType)
{
	atArray<fwCustomShaderEffectBaseType*> &types = m_customShaderEffectTypes[m_fenceUpdateIdx];
	const bool bSpace = types.GetCount() < types.GetCapacity();
	Assertf(bSpace, "dlDrawListMgr::AddRefCountedCustomShaderEffectType - m_customShaderEffectTypes[%i] is full.", m_fenceUpdateIdx % 2);
	if (bSpace)
	{
		types.Push(pType);
	}
}

inline void dlDrawListMgr::AddRefCountedArchetypeHD(fwArchetype *pArch)
{
	atArray<fwArchetype*> &types = m_HDArchetypeRefs[m_fenceUpdateIdx];
	const bool bSpace = types.GetCount() < types.GetCapacity();

	if (bSpace)
	{
		types.Push(pArch);
	}
#if __ASSERT
	else
	{

		if (types.Find(pArch) == -1)
		{
			// only assert if we haven't got an entry for this archetype HD - as that's a real problem.
			Assertf(bSpace, "dlDrawListMgr::AddRefCountedArchetypeHD - m_HDArchetypeRefs[%i] is full.", m_fenceUpdateIdx % 2);
		}
	}
#endif //__ASSERT
}

// inline bool dlDrawListMgr::ValidateStreamingIndexHasBeenCached(strIndex index)
// {
// #if __DEV 
// 	if (ms_enableStrIndicesValidation)
// 	{
// 		atArray<ReferencedStreamable> &archStrIndices = m_aStrIndicesCache[pCurrExecutingList->m_StrIndicesCacheIndex];
// 
// 		int count = archStrIndices.GetCount();
// 		for (int i=0; i<count; ++i)
// 		{
// 			if (archStrIndices.GetElements()[i] == index)
// 			{
// 				return true;
// 			}
// 		}
// 
// 		return false;
// 	}
// 
// 	return true;
// #else
// 	(void)index;
// 	return true;
// #endif
// }

inline bool dlDrawListMgr::ValidateModuleIndexHasBeenCached(s32 DEV_ONLY(moduleIndex), strStreamingModule* DEV_ONLY(pModule))
{
#if __DEV 
	Assert(moduleIndex > -1);
	Assert(moduleIndex < 0xffff);
	if (ms_enableStrIndicesValidation)
	{
		atArray<ReferencedStreamable> &archStrIndices = m_aStrIndicesCache[pCurrExecutingList->m_StrIndicesCacheIndex];

		int count = archStrIndices.GetCount();
		for (int i=0; i<count; ++i)
		{
			if (archStrIndices.GetElements()[i].IsEquivalent((u16)moduleIndex, pModule))
			{
				return true;
			}
		}

		return false;
	}

	return true;
#else
	return true;
#endif
}

const s32 gDrawListSwitch = 1;			// 0 = default rendering, 1= drawlist rendering

extern dlDrawListMgr	*gDrawListMgr;

} //namespace rage

#endif // FWDRAWLIST_DRAWLISTMGR_H_
