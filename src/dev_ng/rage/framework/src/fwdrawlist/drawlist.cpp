﻿/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    drawlist.cpp
// PURPOSE : intermediate data between game entities and command buffers. Gives us
// independent data which can be handed off to the render thread
// AUTHOR :  john.
// CREATED : 30/8/06
//
/////////////////////////////////////////////////////////////////////////////////
#include "drawList.h"

#include "drawlist_channel.h"
#include "drawlistmgr.h"

#include "bank/bank.h"
#include "diag/output.h"
#include "fragment/manager.h"
#include "fragment/tune.h"
#include "grcore/allocscope.h"
#include "grcore/debugdraw.h"
#include "grcore/config.h"
#include "grcore/device.h"
#include "grcore/effect.h"
#include "grcore/font.h"
#include "grcore/gfxcontext.h"
#include "grcore/light.h"
#include "grcore/viewport.h"
#include "grcore/setup.h"
#include "grcore/texturedefault.h"
#include "grcore/d3dwrapper.h"
#include "grmodel/shaderfx.h"
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "grprofile/timebars.h"
#include "rmcore/instance.h"
#include "math/random.h"
#include "fwrenderer/renderlistbuilder.h"
#include "fwrenderer/RenderPhaseBase.h"
#include "fwrenderer/renderthread.h"
//#include "streaming/streamingdebug.h"
#include "streaming/streamingengine.h"
#include "system/xtl.h"
#include "system/cache.h"
#include "system/magicnumber.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/param.h"
#include "rmptfx/ptxmanager.h"
#include "fwsys/timer.h"

#if __DEV
#include "system/stack.h"
#endif

#if __D3D
#include "system/d3d9.h"
#endif

#if __D3D11
#include "grcore/buffer_d3d11.h"
#include "grcore/texture_d3d11.h"
#endif //__D3D11

#include "profile/cputrace.h"
#include "grprofile/gcmtrace.h"
#include "profile/group.h"
#include "profile/page.h"

#if	__PPU
#include "grcore/wrapper_gcm.h"
using namespace cell::Gcm;
#include "system/replay.h"
#endif

#if __PS3 && SPU_GCM_FIFO
#include "grmodel/grmodelspu.h"
#endif

#define MAX_COPIED_COMMAND_HELPER	(16)		// only used in a sample?
#define MAX_COPIED_SKELETONS	(400)

#define MAX_DRAWLIST_PAGE_AGE	(25)

#define PAD_DATA_BLOCKS 1

DRAWLIST_OPTIMISATIONS()

RAGE_DEFINE_CHANNEL(drawlist)


namespace rage {

PF_PAGE(DrawListPage,"Draw Command Buffers");
PF_GROUP(DrawLists);
PF_LINK(DrawListPage, DrawLists);

PF_VALUE_INT(CommandBufferUsage, DrawLists);
PF_VALUE_INT(SharedMemBlocks, DrawLists);
PF_VALUE_INT(AvgCommandSize, DrawLists);
PF_VALUE_INT(PeakSharedDataSize, DrawLists);
PF_VALUE_INT(SharedDataSize, DrawLists);
PF_VALUE_INT(PaddingWaste, DrawLists);
PF_VALUE_INT(InstructionCount, DrawLists);
PF_VALUE_INT(RingBufferPages, DrawLists);
PF_VALUE_INT(LifetimeRenderThreadPages, DrawLists);
PF_VALUE_INT(LifetimeGpuPages, DrawLists);
PF_VALUE_INT(EndOfPageWaste, DrawLists);
PF_VALUE_INT(PageAllocations, DrawLists);
PF_VALUE_INT(PageFrees, DrawLists);
PF_VALUE_INT(PeakPageCount, DrawLists);
PF_VALUE_INT(EmergencyPages, DrawLists);
PF_VALUE_INT(MatrixSetSharedDataCount, DrawLists);
PF_VALUE_INT(MatrixSetSharedDataUsage, DrawLists);

#if __DEV
u32 dlCmdBase::m_currCommandIdx = 1; 

// Name of the last draw list that was being processed on the render thread
const char *g_CurrentDrawListTypeName;
#endif // __DEV

u32				  dlCmdNewDrawList::m_currDrawListIdx = 0;
DECLARE_MTR_THREAD int dlCmdNewDrawList::m_currDrawListLODFlagShift = 0;
int				  dlCmdNewDrawList::m_currDrawListLODFlagShift_UpdateThread = 0;

dlDrawListType s_DrawListTypes[DPT_MAX_TYPES] = {
	{	0, false, BANK_ONLY("Ring Buffer") },                       // DPT_SIMPLE_RING_BUFFER
	{	0, false, BANK_ONLY("Lifetime Render Thread") },            // DPT_LIFETIME_RENDER_THREAD
	{	2, false, BANK_ONLY("Lifetime GPU") },                      // DPT_LIFETIME_GPU
#if !ONE_STREAMING_HEAP
	{	0, true,  BANK_ONLY("Lifetime Render Thread Physical") },   // DPT_LIFETIME_RENDER_THREAD_PHYSICAL
	{	2, true,  BANK_ONLY("Lifetime GPU Physical") },             // DPT_LIFETIME_GPU_PHYSICAL
#endif
};

// Make sure s_DrawListTypes and eDrawListPageType are synced up.
CompileTimeAssert(NELEM(s_DrawListTypes) == DPT_MAX_TYPES);

#if __BANK
u32 dlDrawCommandBuffer::ms_maxBufSize = 0;
static mthRandom s_DebugRandom;

#if __DEV
dlCmdBase *g_PrevDrawListEntry = NULL;
u32         dlDrawCommandBuffer::ms_currentInstructionId = 0;
const char* dlDrawCommandBuffer::ms_currentInstructionName = "";
#endif



const char *dlDrawCommandBuffer::sm_UsageStatsSortingStrings[USAGE_SORT_TYPES] = {
	"By Instruction ID",
	"By Count",
	"By Size",
	"By Time Spent Constructing",
	"By Time Spent Executing",
};

#endif //__BANK

#if __DEV
u32 dlCmdBase::ms_oldWritePos = 0; 

#if INSTR_TRACKING
atArray<u32>	dlCmdBase::m_aMemUsed;
atArray<u32>	dlCmdBase::m_aMemUsedPadded;
atArray<u32>	dlCmdBase::m_aFreq;
atArray<u32>	dlCmdBase::m_aMemConsumed;
u16				dlCmdBase::m_aInstrCountByPhase[(DC_NumBaseDrawCommands + 100)][DL_MAX_TYPES];
bool			dlCmdBase::m_bDumpData;
#endif //INSTR_TRACKING
#endif //__DEV

#if __DEV && __PPU
#include <sys/gpio.h>
bool bLightsToScreen;
#endif

dlDrawCommandBuffer *gDCBuffer;
#if __DEV
DECLARE_MTR_THREAD u32 dlDrawCommandBuffer::m_execCommandIdx = 0;
#endif // __DEV



dlDrawListPage::dlDrawListPage()
: m_Page(NULL)
, m_PageType(DPT_SIMPLE_RING_BUFFER)
, m_NextPage(-1)
, m_Frame(0)
, m_Size(0)
, m_IsEmergency(0)
{
}

bool dlDrawListPage::InitAndAllocate(eDrawListPageType type, u32 size) {
	dlAssertf(!m_Page, "Draw list page being allocated twice - this is an awesome memory leak.");

	USE_MEMBUCKET(MEMBUCKET_RENDER);

#if __BANK
	if (gDCBuffer->ShouldRandomlyFailAlloc()) {
		dlDebugf3("Stress test - performed a randomly failed allocation.");
		m_Page = NULL;
	} else
#endif // __BANK
	{
		eMemoryType memType = MEMTYPE_RESOURCE_VIRTUAL;

#if !ONE_STREAMING_HEAP
		if (s_DrawListTypes[type].m_Physical) {
			memType = MEMTYPE_RESOURCE_PHYSICAL;
		}
#endif // !ONE_STREAMING_HEAP

		MEM_USE_USERDATA(MEMUSERDATA_DRAWPAGE + type);
		m_Page = (char *) strStreamingEngine::GetAllocator().Allocate(size, 128, memType);
	}
	m_PageType = type;
	m_NextPage = -1;
	m_Size = size;

	return (m_Page != NULL);
}

void dlDrawListPage::Free() {
	MEM_USE_USERDATA(MEMUSERDATA_DRAWPAGE + GetType());

	strStreamingEngine::GetAllocator().Free(m_Page);
	m_Page = NULL;

#if __DEV
	m_Size = 0;	// Technically not necessary, but makes debug output more readable
#endif // __DEV
}


dlSharedMemData::dlSharedMemData()
#if __STATS
: m_PeakUsage(0)
, m_Usage(0)
, m_Count(0)
, m_PeakCount(0)
, m_EKGDataSizeMarker(NULL)
, m_EKGCountMarker(NULL)
#endif // __STATS
{
}

dlSharedMemData::~dlSharedMemData()
{
	Shutdown();
}

void dlSharedMemData::Shutdown()
{
	m_SharedDataMap.Reset();

#if __STATS
	delete m_EKGDataSizeMarker;
	delete m_EKGCountMarker;
	m_EKGDataSizeMarker = NULL;
	m_EKGCountMarker = NULL;
#endif // __STATS
}

void dlSharedMemData::InitSharedDataMap(int size)
{
	m_SharedDataMap.Reserve(size);
}

dlSharedDataInfo *dlSharedMemData::LookupSharedData(const void* id)
{
	int count = m_SharedDataMap.GetCount();
	dlSharedDataMapEntry *entry = m_SharedDataMap.GetElements();

	while (count--)
	{
		if (entry->m_ID == id)
		{
			return &entry->m_SharedDataInfo;
		}

		entry++;
	}

	return NULL;
}

dlSharedDataInfo &dlSharedMemData::AllocateSharedData(const void* id)
{
#if __STATS && __ASSERT
	s32 dummyValue;
	dlAssertf(m_SharedDataMap.GetCount() < m_SharedDataMap.GetCapacity(), "Shared data map for %s if full (%d entries) - use a bigger value for InitSharedDataMap.", m_EKGDataSizeMarker->GetName(dummyValue), m_SharedDataMap.GetCount());
#endif // __STATS

	dlSharedDataMapEntry &mapEntry = m_SharedDataMap.Append();
	mapEntry.m_ID = id;
	return mapEntry.m_SharedDataInfo;
}

#if __BANK
dlSharedDataInfo &dlSharedMemData::AllocateSharedData(const void* id, size_t debugData)
{
#if __STATS && __ASSERT
	s32 dummyValue;
	dlAssertf(m_SharedDataMap.GetCount() < m_SharedDataMap.GetCapacity(), "Shared data map for %s if full (%d entries) - use a bigger value for InitSharedDataMap.", m_EKGDataSizeMarker->GetName(dummyValue), m_SharedDataMap.GetCount());
#endif // __STATS

	dlSharedDataMapEntry &mapEntry = m_SharedDataMap.Append();
	mapEntry.m_ID = id;
	mapEntry.m_debugData = debugData;
	return mapEntry.m_SharedDataInfo;
}
#endif // __BANK

void dlSharedMemData::ResetSharedData()
{
	m_SharedDataMap.Resize(0);
}


//#######################################
// --- dlDrawCommandBuffer stuff ---

dlDrawCommandBuffer::dlDrawCommandBuffer()
: m_Timestamp(1)
, m_ConsumedTimestamp(0)
, m_MaxPageAge(MAX_DRAWLIST_PAGE_AGE)
#if __BANK
, m_DisableEmergencyPages(false)
, m_RandomAllocFailRate(0)
#endif // __BANK
{
}

DECLARE_MTR_THREAD DrawListAddress::Parameter dlDrawCommandBuffer::m_CurrReadAddr;
DECLARE_MTR_THREAD u32	dlDrawCommandBuffer::m_currListId;


void dlDrawCommandBuffer::Initialise(){

	sysMemSet(&m_JumpTable[0], 0, sizeof(dlCmdCallback) * JUMPTABLE_SIZE);

#if __BANK
	sysMemSet(&m_ExtraDebugJumpTable[0], 0, sizeof(dlCmdExtraDebugCallback) * JUMPTABLE_SIZE);
	sysMemSet(&m_InstructionUsageStats, 0, sizeof(m_InstructionUsageStats));
	m_pFrozenDebugBuffer = NULL;
	m_pFrozenDebugBufferOffsets = NULL;
	m_UsageStatsSortingCriteria = USAGE_SORT_BY_ID;

	m_ShowFrozenBuffer = false;
	m_ShowInstructionUsage = false;
	m_ShowPageDebug = false;
#endif // __BANK


	// Initialize all system commands.
	DLC_REGISTER(dlCmdNewDrawList);
	DLC_REGISTER(dlCmdEndDrawList);
	DLC_REGISTER(dlComputeShaderBatch);
	DLC_REGISTER(dlCmdDataBlock);
	DLC_REGISTER(dlCmdBeginDraw);
	DLC_REGISTER(dlCmdEndDraw);

	DLC_REGISTER(dlCmdAllocScopePush);
	DLC_REGISTER(dlCmdAllocScopePop);

	DLC_REGISTER(dlCmdLockRenderTarget);
	DLC_REGISTER(dlCmdUnLockRenderTarget);
	DLC_REGISTER(dlCmdSetRasterizerState);
	DLC_REGISTER(dlCmdSetDepthStencilState);
	DLC_REGISTER(dlCmdSetDepthStencilStateEx);
	DLC_REGISTER(dlCmdSetBlendState);
	DLC_REGISTER(dlCmdSetBlendStateEx);
	DLC_REGISTER(dlCmdSetStates);
	DLC_REGISTER(dlCmdSetStatesEx);

	DLC_REGISTER(dlCmdSetCurrentViewport);
	DLC_REGISTER(dlCmdClearRenderTarget);
	DLC_REGISTER(dlCmdSetClipPlane);
	DLC_REGISTER(dlCmdSetClipPlaneEnable);
	DLC_REGISTER(dlCmdSetShaderGPRAlloc);
	DLC_REGISTER(dlCmdGrcLightStateSetEnabled);
	DLC_REGISTER(dlCmdInsertEndFence);
	DLC_REGISTER(dlCmdSetCurrentViewportToNULL);

#if __DEV
	DLC_REGISTER(dlCmdAddDrawListMarker);
#endif // __DEV
	DLC_REGISTER(dlCmdDrawTriShape);

#if !__FINAL
	DLC_REGISTER(dlCmdPushTimebar);
	DLC_REGISTER(dlCmdPopTimebar);
	DLC_REGISTER(dlCmdPushGPUTimebar);
	DLC_REGISTER(dlCmdPopGPUTimebar);
#if ENTITY_GPU_TIME
	DLC_REGISTER(dlCmdEntityGPUTimePush);
	DLC_REGISTER(dlCmdEntityGPUTimePop);
	DLC_REGISTER(dlCmdEntityGPUTimeFlush);
#endif // ENTITY_GPU_TIME
	DLC_REGISTER(dlCmdPushMarker);
	DLC_REGISTER(dlCmdPopMarker);
#endif // !__FINAL

	DLC_REGISTER(dlCmdSetGeometryVertexOffsets);

	DLC_REGISTER(dlCmdSetBucketsAndRenderMode);

	DLC_REGISTER(dlCmdNop);
	DLC_REGISTER(dlCmdSwitchPage);

#if GRCDBG_IMPLEMENTED
	DLC_REGISTER(dlCmdGrcDbgPush);
	DLC_REGISTER(dlCmdGrcDbgPop);
#endif

	/// DLC_REGISTER(dlCmdPopDefaultSamplerState);

	DLC_REGISTER(dlCmdShaderFxPushForcedTechnique);
	DLC_REGISTER(dlCmdShaderFxPopForcedTechnique);

#if __PPU
	DLC_REGISTER(dlCmdCellGcmSetTransferData);
	DLC_REGISTER(dlCmdBeginAdaptiveZCulling);
	DLC_REGISTER(dlCmdEndAdaptiveZCulling);
	DLC_REGISTER(dlCmdInvalidateZCulling);
	DLC_REGISTER(dlCmdEdgeShadowType);
#endif // __PPU

#if __XENON
	DLC_REGISTER(dlCmdXenonSetScissor);
#endif
	DLC_REGISTER(dlCmdBeginConditionalRender);
	DLC_REGISTER(dlCmdEndConditionalRender);

	DLC_REGISTER(dlCmdSetArrayView);

	DLC_REGISTER(dlCmdCallBackNoArg);
	DLC_REGISTER(dlCmdGenericArgBase);

	DLC_REGISTER(dlCmdSetGlobalVar_F);
	DLC_REGISTER(dlCmdSetGlobalVar_V4);
	DLC_REGISTER(dlCmdSetGlobalVar_M44);
	DLC_REGISTER(dlCmdSetGlobalVar_RT);

	DLC_REGISTER(dlCmdSetGlobalVarArrayFloat);
	DLC_REGISTER(dlCmdSetGlobalVarArrayV4);

	DLC_REGISTER(dlCmdGrcDeviceSetScissor);
	DLC_REGISTER(dlCmdGrcDeviceDisableScissor);

#if RSG_PC &&__D3D11
	DLC_REGISTER(dlCmdGrcDeviceLockOrUnlockContext);
	DLC_REGISTER(dlCmdGrcDeviceUpdateBuffer);
#endif // RSG_PC &&__D3D11

	// All draw lists are up for grabs.
	for (int x=0; x<MAX_DRAWLIST_PAGES; x++) {
		m_DrawListFreeList.Append() = x;
	}

	// We don't have any pages for any data at this point.
	// TODO: Once we have systems that use the other pages, add a starting page to all of them!
	for (int x=0; x<DPT_MAX_TYPES; x++) {
		m_DrawListFirstPage[x] = -1;
		m_DrawListCurrentPosition[x].SetNULL();

#if __BANK
		m_DrawListPageUsage[x] = 0;
#endif // __BANK
	}

	Reset();

	RegisterSharedMemType(DL_MEMTYPE_SHADER, "Shaders");
	RegisterSharedMemType(DL_MEMTYPE_MATRIXSET, "Matrix Sets");
	RegisterSharedMemType(DL_MEMTYPE_COMMAND_HELPER, "Command Helper");
    RegisterSharedMemType(DL_MEMTYPE_ENTITY, "Entity Draw Data");

	m_SharedMemData[DL_MEMTYPE_MATRIXSET].InitSharedDataMap(MAX_COPIED_SKELETONS);
	m_SharedMemData[DL_MEMTYPE_COMMAND_HELPER].InitSharedDataMap(MAX_COPIED_COMMAND_HELPER);

	m_currListId = 0;
	STATS_ONLY( m_SharedObjectCount = 0; )
	BANK_ONLY( m_CmdCount = 0; )
}

void dlDrawCommandBuffer::Shutdown(){

	// Destroy all pages.
	for (int x=0; x<DPT_MAX_TYPES; x++) {
		int firstPage = m_DrawListFirstPage[x];

		while (firstPage != -1) {
			int nextPage = m_DrawListPages[firstPage].GetNextPage();
			FreeDrawListPage(firstPage);
			firstPage = nextPage;
		}

		m_DrawListFirstPage[x] = -1;
	}

	for (int x=0; x < m_DrawListIdleList.GetCount(); x++) {
		int page = m_DrawListIdleList[x];
		m_DrawListIdleList[x] = -1;
		FreeDrawListPage(page);
	}

#if __BANK
	{
		USE_DEBUG_MEMORY();
		delete[] m_pFrozenDebugBuffer;
		delete[] m_pFrozenDebugBufferOffsets;
		m_pFrozenDebugBuffer = NULL;
		m_pFrozenDebugBufferOffsets = NULL;
	}
#endif // __BANK

	for (int x=0; x<MAX_DL_SHARED_MEM_TYPES; x++) {
		m_SharedMemData[x].Shutdown();
	}
}

// for reset just start adding at start & overwrite stuff
void dlDrawCommandBuffer::Reset(){

	// Destroy all pages.
	for (int x=0; x<DPT_MAX_TYPES; x++) {
		int firstPage = m_DrawListFirstPage[x];

		while (firstPage != -1) {
			int nextPage = m_DrawListPages[firstPage].GetNextPage();
			FreeDrawListPage(firstPage);
			firstPage = nextPage;
		}

		m_DrawListFirstPage[x] = -1;
		m_StartWriteAddr[x].SetNULL();
	}

	for (int x=0; x < m_DrawListIdleList.GetCount(); x++) {
		int page = m_DrawListIdleList[x];
		m_DrawListIdleList[x] = -1;
		FreeDrawListPage(page);
	}

	// Start with one page.
	// TODO: Create a page for each type once we actually support all types.
	dlDrawListPage *page = AllocateDrawListPage(DPT_SIMPLE_RING_BUFFER);
	int pageIndex = GetPageIndex(page);
	m_DrawListFirstPage[DPT_SIMPLE_RING_BUFFER] = pageIndex;
	m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER] = DrawListAddress(pageIndex, 0);
	m_StartWriteAddr[DPT_SIMPLE_RING_BUFFER] = m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER];


	// We're doing a memset 0xffffffff here - that assumes that this will set the
	// values to NULL_DRAW_LIST_ADDRESS.
	CompileTimeAssert(DrawListAddress::NULL_DRAW_LIST_ADDRESS == 0xffffffff);
	sysMemSet(m_ReadBufferStartAddr, 0xff, sizeof(m_ReadBufferStartAddr));
	sysMemSet(m_ReadBufferEndAddr, 0xff, sizeof(m_ReadBufferEndAddr));

	m_SharedDataSize = 0;

#if __STATS
	m_PeakSharedDataSize = 0;
	m_PaddingWaste = 0;
	m_EndOfPageWaste = 0;
	m_DrawListSize = 0;
	m_PageAllocations = 0;
	m_PageFrees = 0;
	m_CurrentPageStartOffset = 0;
	m_PeakPageCount = 0;
#endif // __STATS
#if __BANK
	sysMemSet(&m_InstructionConstructionTimings[0], 0, sizeof(m_InstructionConstructionTimings));
	sysMemSet(&m_InstructionExecutionTimings[0], 0, sizeof(m_InstructionExecutionTimings));
#endif // __BANK

#if __BANK
	{
		USE_DEBUG_MEMORY();
		delete[] m_pFrozenDebugBuffer;
		delete[] m_pFrozenDebugBufferOffsets;
	}
	m_pFrozenDebugBuffer = NULL;
	m_pFrozenDebugBufferOffsets = NULL;
	m_FrozenBufferCursor = 0;
	m_FrozenInstructionCount = 0;
	m_FrozenCursorMoveSpeed = 0.0f;
	m_FrozenCursor = 0.0f;
	m_FrozenSearchString[0] = 0;
	m_CurrentDebugInstructionId = 0;
	m_CurrentDebugInstructionName[0] = 0;

	m_RenderFrozenBuffer = false;
	m_DrawFrozenUntilCursorOnly = false;
	m_FreezeBufferRequest = false;
	m_AddInstructionMarkers = false;
#endif // __BANK

	dlCmdBase::ClearMemUse();
}

void dlDrawCommandBuffer::RegisterCommand(int instructionId, dlCmdCallback callback, dlCmdExtraDebugCallback BANK_ONLY(extraDebugCallback), const char *BANK_ONLY(name))
{
	dlAssertf(m_JumpTable[instructionId] == NULL, "Trying to register a draw list command on instruction ID %d, but it's already defined. Use OverrideCommand if you want to replace an existing command.", instructionId);
	m_JumpTable[instructionId] = callback;
#if __BANK
	m_InstructionUsageStats[instructionId].m_Name = name;
	m_ExtraDebugJumpTable[instructionId] = extraDebugCallback;
#endif // __BANK
}

void dlDrawCommandBuffer::OverrideCommand(int instructionId, dlCmdCallback callback, dlCmdExtraDebugCallback BANK_ONLY(extraDebugCallback))
{
	m_JumpTable[instructionId] = callback;

#if __BANK
	m_ExtraDebugJumpTable[instructionId] = extraDebugCallback;
#endif // __BANK
}

void dlDrawCommandBuffer::RegisterSharedMemType(int STATS_ONLY(memType), const char *STATS_ONLY(typeName))
{
#if __STATS
	bool wasOnUpdateThread = sysThreadType::IsUpdateThread();
	if (!wasOnUpdateThread)
	{
		// Even if we aren't on the update thread (we're probably not), the update thread will be blocked waiting for this to happen.
		// TODO: Can we dispatch calls to this function back onto the update thread, instead of messing with the flags here?
		sysThreadType::AddCurrentThreadType(sysThreadType::THREAD_TYPE_UPDATE);
	}

	dlSharedMemData &memData = m_SharedMemData[memType];

	dlAssertf(!memData.m_EKGDataSizeMarker, "Shared mem type '%s' initialized twice", typeName);
	memData.m_EKGDataSizeMarker = rage_new pfValueT<int>(typeName, PFGROUP_DrawLists, true);

	char countName[64];
	formatf(countName, "%s count", typeName);
	memData.m_EKGCountMarker = rage_new pfValueT<int>(countName, PFGROUP_DrawLists, true);

	if (!wasOnUpdateThread)
	{
		sysThreadType::ClearCurrentThreadType(sysThreadType::THREAD_TYPE_UPDATE);
	}
#endif // __STATS

}

#if !__FINAL
struct DisableAllocatorStackFrameSpew
{
	~DisableAllocatorStackFrameSpew()				{ strStreamingEngine::GetAllocator().EnableStackTraces(true); }

	void DisableSpew()								{ strStreamingEngine::GetAllocator().EnableStackTraces(false); }
};
#endif // !__FINAL


/** PURPOSE: Get a fresh new draw list page of a certain type. Will recycle an old page
 *  if available or allocate a new one.
 *
 *  NOTE: This function may block if there are currently no pages available and wait for the render thread.
 *
 *  PARAMS:
 *   type - The type of page to allocate. See eDrawListPageType.
 *   minPageSize - The minimum page size to allocate. The function will try to allocate a page with the
 *                 size of preferredPageSize, but if memory is fragmented or low, it will try lower
 *                 sizes but never go beyond minPageSize.
 *   preferredPageSize - The preferred size for the page. Could go down to minPageSize if memory is
 *                       low or fragmented.
 *   mayFail - Don't try TOO hard to get a page, and silently fail if a page could not be allocated.
 *
 * RETURNS:
 *   A pointer to the page that has been allocated. Currently, this can never return NULL, although
 *   the function may block indefinitely if the memory situation is beyond repair.
 */
dlDrawListPage *dlDrawCommandBuffer::AllocateDrawListPage(eDrawListPageType type, u32 minPageSize, u32 preferredPageSize, bool mayFail)
{
#if !__FINAL
	DisableAllocatorStackFrameSpew disableSpew;
#endif // !__FINAL

	int drawListIndex = 0;
	dlDrawListPage *page = NULL;
	int attempt = 0;
	bool isPhysical = s_DrawListTypes[type].m_Physical;

	// Obtain a free page slot.
	while (true) {
		m_DrawListPageCritSec.Lock();

		// First, let's try to score those that are currently in limbo.
		int idlePages = m_DrawListIdleList.GetCount();
		int idlePageIndex = -1;

		for (int x=0; x<idlePages; x++) {
			dlDrawListPage &page = GetDrawListPage(m_DrawListIdleList[x]);
			if (page.GetSize() >= minPageSize) {
				// Got one! But is it compatible?
				if (s_DrawListTypes[page.GetType()].m_Physical == isPhysical) {
					idlePageIndex = x;
					break;
				}
			}
		}

		if (idlePageIndex != -1) {
			// Got one!
			int idlePage = m_DrawListIdleList[idlePageIndex];
			m_DrawListIdleList.DeleteFast(idlePageIndex);
			m_DrawListPageCritSec.Unlock();

			dlDrawListPage *page = &m_DrawListPages[idlePage];

			if (attempt > 0) {
				dlDebugf2("Found a recycled draw list page after waiting for %dms", attempt);
			}

			dlDebugf3("Recycling draw list page %d for frame %d", idlePage, m_Timestamp);
			page->Recycle();
			page->SetFrame(m_Timestamp);
			page->SetType(type);

			sysMemAllocator::GetMaster().SetUserData(page->GetStartPointer(), MEMUSERDATA_DRAWPAGE + type);

			return page;
		}

		// There are no idle pages, so we'll have to grab a new one.
		if (m_DrawListFreeList.GetCount() == 0) {
			if (++attempt == 1) {
				if (mayFail) {
					// If we get here, it means that there are lots of pages in use already. The caller
					// doesn't seem to be THAT bent on getting a page, so let's just fail here.
					m_DrawListPageCritSec.Unlock();
					return NULL;
				}

				dlErrorf("No more draw list page slots available - waiting for the render thread to free one up");
			}

#if !__FINAL
			if (attempt == 80) {
				DumpDrawListPages();
			}
#endif // !__FINAL

			dlAssertf(attempt != 80, "Looks dim on the draw list page - it seems like there have been too many requests for pages. Try increasing MAX_DRAWLIST_PAGES.");
			m_DrawListPageCritSec.Unlock();
			sysIpcSleep(1);
			continue;
		}

		// Pick the first draw list we can find.
		drawListIndex = m_DrawListFreeList[0];
		m_DrawListFreeList.DeleteFast(0);

#if __STATS
		m_PeakPageCount = Max(m_PeakPageCount, (u32) (MAX_DRAWLIST_PAGES - m_DrawListFreeList.GetCount()));
#endif // __STATS

		m_DrawListPageCritSec.Unlock();

		page = &m_DrawListPages[drawListIndex];
		page->SetFrame(m_Timestamp);

		// Try to allocate the page. If we run out of memory, we'll keep decrementing the size
		// until we get the memory.
		bool result = false;
		u32 pageSize = preferredPageSize;

		do {
			result = page->InitAndAllocate(type, pageSize);
			pageSize >>= 1;

#if !__FINAL
			// Disable further spew from the allocator
			disableSpew.DisableSpew();
#endif // !__FINAL

		} while (!result && pageSize >= minPageSize);

		if (!result) {
			// Out of memory!
			// Put the page back - we may be able to recycle in a bit.
			m_DrawListPageCritSec.Lock();
			m_DrawListFreeList.Append() = drawListIndex;

			if (mayFail) {
				// The user is OK with failure, so let's not go all the way to the emergency fund.
				m_DrawListPageCritSec.Unlock();
				return NULL;
			}

			// Maybe we can draw one from the emergency fund?
			if (BANK_ONLY(!m_DisableEmergencyPages &&) m_EmergencyPageList.GetCount() != 0) {
				// YES - that's why we like to save for rainy days.
				int emergencyPages = m_EmergencyPageList.GetCount();
				for (int x=0; x<emergencyPages; x++) {
					dlDrawListPage *page = &GetDrawListPage(m_EmergencyPageList[x]);
					if (page->GetSize() >= minPageSize && s_DrawListTypes[page->GetType()].m_Physical == isPhysical) {
						// Found one.
						drawListIndex = m_EmergencyPageList[x];
						m_EmergencyPageList.DeleteFast(x);
						m_DrawListPageCritSec.Unlock();

						dlDebugf2("Drawing page %d from the emergency page list", drawListIndex);
						dlAssert(page->IsEmergency());

						page->SetEmergency(false);
						page->SetType(type);
						page->Recycle();
						page->SetFrame(m_Timestamp);
						dlAssert(page->IsAllocated());   	// Emergency pages are expected to be allocated and ready for use

						sysMemAllocator::GetMaster().SetUserData(page->GetStartPointer(), MEMUSERDATA_DRAWPAGE + type);

						// We have a page. Let's use it.
						return page;
					}
				}
			}

			m_DrawListPageCritSec.Unlock();

			if (++attempt == 1) {
				dlErrorf("Out of streaming memory for a draw list page of type %d - waiting for the render thread to free one up", type);
			}

#if !__FINAL
			if (attempt == 160) {
				DumpMemoryInfo();
			}
#endif // !__FINAL

			dlAssertf(attempt != 160, "Looks dim on the draw list page - it seems like streaming memory is full and we can't evict anything.");
			sysIpcSleep(1);

			continue;
		}

		// We got a page - let's use it.
		break;
	}


	dlDebugf3("Allocated new draw list page %d for frame %d", drawListIndex, m_Timestamp);

#if __STATS
	m_PageAllocations++;
	m_DrawListPageUsage[type]++;
#endif // __STATS

	return page;
}

void dlDrawCommandBuffer::FreeDrawListPage(int pageIndex)
{
	dlDrawListPage &page = m_DrawListPages[pageIndex];

#if __STATS
	m_DrawListPageUsage[page.GetType()]--;
	m_PageFrees++;
#endif // __STATS

	page.Free();

	// Put it back on the free list.
	dlAssertf(m_DrawListFreeList.Find(pageIndex) == -1, "Page %d is put on the emergency list twice - this will cause a crash.", pageIndex);
	dlAssertf(m_DrawListIdleList.Find(pageIndex) == -1, "Page %d is erroneously on the idle list", pageIndex);
	m_DrawListFreeList.Append() = pageIndex;

	if (m_DrawListFirstPage[page.GetType()] == pageIndex)
	{
		m_DrawListFirstPage[page.GetType()] = page.GetNextPage();
	}
}

/** PURPOSE: This function will return a pointer into the draw list pages that has a certain amount
 *  of bytes reserved for the caller. If not enough memory is available, a new draw list page will
 *  be allocated.
 *
 *  This function could be compared with a regular memory allocation function, except that it may
 *  or may not internally allocate any memory, and the returned memory will be freed up automatically
 *  after a certain amount of frames (as specified in the draw list type specified in "type").
 *
 *  PARAMS:
 *    type - The type of draw list page to allocate. See eDrawListPageType.
 *    size - The amount of bytes to reserve.
 *    mayFail - Don't try TOO hard to get a page, and silently fail if a page could not be allocated.
 *    alignment - Alignment to use in bytes. Padding bytes may be added if this is greater than 1.
 *                NOTE that this padding will only be applied within the page - new pages are assumed to be properly
 *                aligned already, but since pages are allocated using the buddy heap, an alignment of 4KB can be
 *                expected.
 *    offset - Output draw list address.
 *    
 *
 *  RETURNS:
 *    A pointer somewhere into a draw list page that has a continuous buffer of "size" bytes
 *    that may be modified at will by the user. The memory will remain resident for as long as
 *    the policy of the draw list type specified in "type". Currently, this function cannot
 *    return NULL, but it can block if memory is low, and it could stall forever if the memory
 *    situation is beyond control.
 */
char *dlDrawCommandBuffer::AllocatePagedMemory(eDrawListPageType type, size_t size, bool mayFail, int alignment, DrawListAddress &offset) {
	// NOTE: This is only necessary if we expect to allocate from the render thread too... Arguably all allocations should happen on the update thread.
	SYS_CS_SYNC(m_AllocMemoryCritSec); 

	offset = DrawListAddress();

	Assert(size&0xfffffffc);	// is size aligned on 4?
	// Align the size by 4.
	size += 3;
	size &= 0xfffffffc;

	// Overhead for page switching - we need to add a command in the ring buffer.
	// Other types don't need to add anything in the page itself to indicate a
	// page switch.
	int pageSwitchOverhead = (type == DPT_SIMPLE_RING_BUFFER) ? 16 : 0;

	// Also get the next power of two, in case we need to allocate a new page.
	u32 alignedSize = DEFAULT_DRAWLIST_PAGE_SIZE;

	while (size > alignedSize) {
		alignedSize <<= 1;
	}

	// How are we looking in this buffer?
	DrawListAddress curAddr = m_DrawListCurrentPosition[type];

	if (curAddr.IsNULL()) {
		// Never been used - we need to create a new page.
		dlDrawListPage *page = AllocateDrawListPage(type, (u32)size, (u32)alignedSize, mayFail);
		
		if (!page) {
			dlAssert(mayFail);	// This function should never return NULL unless mayFail is true
			return NULL;
		}
		
		curAddr = DrawListAddress(GetPageIndex(page), (u32) size);
		offset = DrawListAddress(GetPageIndex(page), 0);

		SetCurrentDrawListPosition(type, curAddr);
		return page->GetStartPointer();
	}

	dlDrawListPage &curPage = GetDrawListPage(curAddr.GetPageIndex());

	// Do we need padding?
	int paddingOverhead = -(s32)(uptr) ResolveDrawListAddress(curAddr) & (alignment-1);

	// Is there enough space in the current page?
	const int available = static_cast<int>(curPage.GetSize()) - static_cast<int>(curAddr.GetOffset()) - pageSwitchOverhead - paddingOverhead;

	// HACK: Game crashes if we use the entire page up. Leave 1 byte for the gods...
	if (available <= static_cast<int>(size))
	{
		// No - move to a new page then.
		curAddr = MoveToNewPage(type, (u32)size, (u32)alignedSize, mayFail);

		if (curAddr.IsNULL()) {
			dlAssert(mayFail);	// This function should never return NULL unless mayFail is true
			return NULL;
		}
	}
	else
	{
		// Get the pointer to the data, and move the cursor forward.
		curAddr.MoveOffset(s32(paddingOverhead));
	}

	offset = curAddr;
	char *result = ResolveDrawListAddress(curAddr);
	curAddr.MoveOffset(s32(size));
	SetCurrentDrawListPosition(type, curAddr);

	return result;
}


// execute the draw list at the given address (handle the wrap around when list goes off end of buffer)
bool	dlDrawCommandBuffer::ExecuteListAtAddr(DrawListAddress::Parameter addr, u32 ASSERT_ONLY(DEV_ONLY(finalIdx)), u32 listId){

#if GRCCONTEXT_ALLOC_SCOPES_SUPPORTED
	grcGfxContext::lockAllocScope();
#endif

#if __DEV
	m_execCommandIdx = 1;				// restart command counter
#endif // __DEV
	bool	bListTerminated = false;

	//Assert(addr < MAX_DRAW_LIST_SIZE);
	//m_currReadPos = addr;
	char *currReadAddr = ResolveDrawListAddress(addr);

	m_currListId = listId;
	Assert(listId);

	PrefetchDC(currReadAddr);
	PrefetchDC(currReadAddr + 0x80);
	m_CurrReadAddr = addr;

#if __BANK
	int pixInstructionId = (m_AddInstructionMarkers) ? m_CurrentDebugInstructionId : -1;
#endif // __BANK

	while(!bListTerminated){
		currReadAddr = ResolveDrawListAddress(m_CurrReadAddr);
		PrefetchDC(currReadAddr + 0x100);
		PrefetchDC(currReadAddr + 0x180);


		// pull out the command at the current position
		dlCmdBase& drawListEntry = reinterpret_cast<dlCmdBase&>(*currReadAddr);

		// update read position _before_ executing command, because we may want to get a data block after current command
		s32 cmdSize = drawListEntry.GetCommandSizeStatic();
		//currReadAddr += cmdSize;
		DrawList_MoveOffset(m_CurrReadAddr,cmdSize);
		//Assert(m_currReadPos <= MAX_DRAW_LIST_SIZE);


		// debug stuff - make sure that the expected command is being picked up
#if __DEV
// 		if (m_execCommandIdx != drawListEntry.GetCommandIdx()) {
// 			DumpCommandBuffer(false);
// 		}

//		dlAssertf(m_execCommandIdx == drawListEntry.GetCommandIdx(), "IDX mismatch: %d vs %d at addr %x (block size %d)", m_execCommandIdx, drawListEntry.GetCommandIdx(), (DrawListAddress::Parameter) m_CurrReadAddr, cmdSize);
		m_execCommandIdx++;		// this is done by reading data blocks too...										
#endif // __DEV

		int instructionId = drawListEntry.GetInstructionIdStatic();
		dlCmdCallback cmdCallback = m_JumpTable[instructionId];

		dlAssertf(cmdCallback, "A draw list is using command %d, which has never been defined. The draw list is corrupted, or the command has not been registered with DLC_REGISTER. PC=%p, list ID=%d, idx=%d, final idx=%d", instructionId, &drawListEntry, listId, 0 DEV_ONLY(+ drawListEntry.GetCommandIdx()), 0 + DEV_ONLY(finalIdx));

#if __BANK
		if (instructionId == pixInstructionId) {
			PIXBegin(0, m_InstructionUsageStats[instructionId].m_Name);
		}
#endif // __BANK

#if __DEV
		ms_currentInstructionId = instructionId; // save this so we can figure out what DC we're in
		ms_currentInstructionName = "";
#endif // __DEV

#if __BANK
		utimer_t timeStart = sysTimer::GetTicks();
#endif // __BANK

		#define VALIDATE_CALLBACK_POINTER (!__FINAL && !__PROFILE)

		if (!VALIDATE_CALLBACK_POINTER || cmdCallback) {
			cmdCallback(drawListEntry);
		}
#if VALIDATE_CALLBACK_POINTER
		else DumpCommandBuffer(addr, m_CurrReadAddr);
#endif

#if __BANK
		utimer_t timeEnd = sysTimer::GetTicks();
		timeEnd -= timeStart;
		m_InstructionExecutionTimings[instructionId] += (utimer_t) timeEnd;

		// Uncomment this to identify expensive callbacks.
/*		if (instructionId == DC_CB_GenericNoArgs || instructionId == DC_CB_GenericArgs) {
			if ((float) timeEnd * sysTimerConsts::TicksToMilliseconds > 0.1f) {
				// Call it again so we can see it in a debugger. This will break the game!
				Displayf("%.2f", (float) timeEnd * sysTimerConsts::TicksToMilliseconds);
				if (cmdCallback) {
					cmdCallback(drawListEntry);
				}
			}
		}*/
#endif // __BANK

#if __BANK
		if (instructionId == pixInstructionId) {
			PIXEnd();
		}
#endif // __BANK

		// check for list termination
		if (drawListEntry.IsEndOfDrawList()) {
#if __DEV
			dlAssert(finalIdx == drawListEntry.GetCommandIdx());		// check that last command is an end list command
#endif // __DEV
			bListTerminated = true;
		}
	#if __DEV
		g_PrevDrawListEntry = &drawListEntry;	// debug: remember prev drawListEntry
	#endif
	}
#if __DEV
	g_PrevDrawListEntry = NULL;
#endif

	// We're no longer processing this list.
	m_currListId = 0;

	// Check for leaking stuff
	Assert(grmShader::GetForcedTechniqueGroupId() == -1);
	Assert(grmModel::GetForceShader() == NULL);

#if GRCCONTEXT_ALLOC_SCOPES_SUPPORTED
	grcGfxContext::unlockAllocScope();
#endif

	return(true);		// was able to execute the desired draw list successfully
}

#if !__FINAL
void dlDrawCommandBuffer::DumpMemoryInfo()
{
	DumpDrawListPages();
}

const char *dlDrawCommandBuffer::DisassembleCommand(dlCmdBase &base, char *buffer, size_t bufferSize, Color32 &outColor) {

	u32 size = base.GetCommandSizeStatic();
#if __BANK
	u32 instructionID = base.GetInstructionIdStatic();
	dlInstructionUsageStats &instInfo = GetInstructionUsageStats(instructionID);
#endif
	char payload[128];
	payload[0] = 0;

	u32 *payloadData = (u32 *) &base;

	// Skip the header, all the information has been decoded already.
	payloadData++;

	// Don't clutter the screen with too  much data.
	int counter = size >> 2;
	counter = Min(counter, 5);

	while (--counter > 0) {
		u32 word = *(payloadData++);

		char hexValStr[10];
		formatf(hexValStr, "%08x ", word);
		safecat(payload, hexValStr);
	}

	char extraDebugBuffer[128];
	const char *extraDebug = "";

	dlCmdExtraDebugCallback extraDebugCallback =
	#if __BANK
		 m_ExtraDebugJumpTable[instructionID];
	#else
		NULL;
	#endif
	if (extraDebugCallback) {
		extraDebug = extraDebugCallback(base, extraDebugBuffer, sizeof(extraDebugBuffer), outColor);

		if (!extraDebug) {
			extraDebug = "";
		}
	}

	int cmdId = 0;
#if __DEV
	cmdId = base.GetCommandIdx();
#endif // __DEV

#if __BANK
	formatf(buffer, bufferSize, "0x%016p,%-4d,%-3d,%-30s,%s,%s", &base, cmdId, size, instInfo.m_Name, payload, extraDebug);
#else
	formatf(buffer, bufferSize, "0x%016p,%-4d,%-3d,%s,%s", &base, cmdId, size, payload, extraDebug);
#endif
	return buffer;
}

namespace dlStatic
{
	void DisplayDumpCmdHeader()
	{
		Displayf("==== BEGIN COMMAND BUFFER DUMP ====");
		Displayf("  Memory Address  ,Command ID,Size,Command,Data Payload,Debug Info");
	}

	void DisplayDumpCmdFooter()
	{
		Displayf("==== END COMMAND BUFFER DUMP ====");
	}
}

void	dlDrawCommandBuffer::DumpCommandBuffer(bool writeBuffer) {
	dlStatic::DisplayDumpCmdHeader();

	DrawListAddress position;
	DrawListAddress endPos;

	if (!writeBuffer) {
		position = m_ReadBufferStartAddr[DPT_SIMPLE_RING_BUFFER][0];
		endPos = m_ReadBufferEndAddr[DPT_SIMPLE_RING_BUFFER][0];
	} else {
		position = m_StartWriteAddr[DPT_SIMPLE_RING_BUFFER];
		endPos = GetCurrentDrawListPosition(DPT_SIMPLE_RING_BUFFER);
	}

	while (position != endPos) {
		dlCmdBase *cmd = (dlCmdBase *) ResolveDrawListAddress(position);
		u32 size = cmd->GetCommandSizeStatic();
		u32 instructionID = cmd->GetInstructionIdStatic();

		Color32 dummyColor;

		char line[192];
		DisassembleCommand(*cmd, line, sizeof(line), dummyColor);
		Displayf("%s", line);

		position.MoveOffset(size);

		if (size == 0)
		{
			Errorf("ERROR: Zero size in element");
			break;
		}

		if (instructionID == DC_SwitchPage) {
			dlCmdSwitchPage *switchPageCmd = (dlCmdSwitchPage *) cmd;
			DrawListAddress newPos = switchPageCmd->m_NewAddress;
			Displayf("---- Switching to new page (index %d) -----", newPos.GetPageIndex());
			position = newPos;
		}
	}

	dlStatic::DisplayDumpCmdFooter();
}

void	dlDrawCommandBuffer::DumpCommandBuffer(DrawListAddress addr, DrawListAddress interestAddr, u32 maxPrint)
{
	if(addr.IsNULL())
		return;

	dlStatic::DisplayDumpCmdHeader();

	u32 currPrint = 0;
	DrawListAddress curr = addr;
	while(currPrint < maxPrint)
	{
		bool isInteresting = (curr == interestAddr);
		if(isInteresting)
		{
			Displayf("");
			Displayf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}

		dlCmdBase *cmd = reinterpret_cast<dlCmdBase *>(ResolveDrawListAddress(curr));
		u32 size = cmd->GetCommandSizeStatic();
		u32 instructionID = cmd->GetInstructionIdStatic();
		
		static Color32 sDummyColor;
		static Color32 sImportantColor(0.0f, 0.0f, 1.0f);
		char line[192];
		DisassembleCommand(*cmd, line, sizeof(line), isInteresting ? sDummyColor : sImportantColor);
		Displayf("%s", line);

		curr.MoveOffset(size);

		if(isInteresting)
		{
			Displayf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			Displayf("");
		}

		if (size == 0)
		{
			Errorf("ERROR: Zero size in element");
			break;
		}

		if (instructionID == DC_SwitchPage) {
			dlCmdSwitchPage *switchPageCmd = static_cast<dlCmdSwitchPage *>(cmd);
			DrawListAddress newPos = switchPageCmd->m_NewAddress;
			Displayf("---- Switching to new page (index %d) -----", newPos.GetPageIndex());
			curr = newPos;
		}

		// check for list termination
		if(cmd->IsEndOfDrawList())
			break;

		++currPrint;
	}

	dlStatic::DisplayDumpCmdFooter();
}

void	dlDrawCommandBuffer::DumpCommandBuffer(dlCmdBase *cmd, dlCmdBase *interestAddr, u32 maxPrint)
{
	dlStatic::DisplayDumpCmdHeader();

	u32 currPrint = 0;
	dlCmdBase *curr = cmd;
	while(curr && currPrint < maxPrint)
	{
		bool isInteresting = (curr == interestAddr);
		if(isInteresting)
		{
			Displayf("");
			Displayf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}

		u32 size = curr->GetCommandSizeStatic();
		u32 instructionID = curr->GetInstructionIdStatic();

		static Color32 sDummyColor;
		static Color32 sImportantColor(0.0f, 0.0f, 1.0f);
		char line[192];
		DisassembleCommand(*cmd, line, sizeof(line), isInteresting ? sDummyColor : sImportantColor);
		Displayf("%s", line);

		if(isInteresting)
		{
			Displayf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
			Displayf("");
		}

		if (size == 0)
		{
			Errorf("ERROR: Zero size in element");
			break;
		}

		if (instructionID == DC_SwitchPage) {
			dlCmdSwitchPage *switchPageCmd = static_cast<dlCmdSwitchPage *>(curr);
			DrawListAddress newPos = switchPageCmd->m_NewAddress;
			Displayf("---- Switching to new page (index %d) -----", newPos.GetPageIndex());
			curr = reinterpret_cast<dlCmdBase *>(ResolveDrawListAddress(newPos));
		}

		// check for list termination
		if(curr->IsEndOfDrawList())
			break;

		curr = reinterpret_cast<dlCmdBase *>(reinterpret_cast<char *>(curr) + (size * 4));

		++currPrint;
	}

	dlStatic::DisplayDumpCmdFooter();
}

void	dlDrawCommandBuffer::DumpDrawListPages() {
	Displayf("===== BEGIN DRAW LIST PAGE DUMP ====");
	Displayf("Index,Type,Size,Next Index,Data,Age,Frame,Emergency");

	OUTPUT_ONLY(u32 currentStamp = m_Timestamp);

	for (int x=0; x<MAX_DRAWLIST_PAGES; x++) {
		dlDrawListPage &page = m_DrawListPages[x];

#if __BANK
		const char *typeName = s_DrawListTypes[page.GetType()].m_Name;
#else // __BANK
		char typeName[8];
		formatf(typeName, "%d", page.GetType());
#endif // __BANK

		if (page.IsAllocated()) {
			Displayf("%d,%s,%dK,%d,%p,%d,%d,%s", x, typeName, page.GetSize() / 1024, page.GetNextPage(),
				page.GetStartPointer(), currentStamp - page.GetFrame(), page.GetFrame(),
				page.IsEmergency() ? "EMERGENCY" : "");
		} else {
			Displayf("%d,FREE,,,,,%d,%d,", x, currentStamp - page.GetFrame(), page.GetFrame());
		}
	}

	char freeList[192];
	char number[8];
	safecpy(freeList, "Free List: ");

	for (int x=0; x<m_DrawListFreeList.GetCount(); x++) {
		formatf(number, "%d ", m_DrawListFreeList[x]);
		safecat(freeList, number);
	}

	Displayf("%s", freeList);

	safecpy(freeList, "Idle List: ");

	for (int x=0; x<m_DrawListIdleList.GetCount(); x++) {
		formatf(number, "%d ", m_DrawListIdleList[x]);
		safecat(freeList, number);
	}

	Displayf("%s", freeList);

	Displayf("===== END DRAW LIST PAGE DUMP ====");
}
#endif // !__FINAL

// check integrity of the list (up to the desired point)
bool	dlDrawCommandBuffer::CheckListIntegrity(u32 /*addr*/, u32 /*finalIdx*/){
/*
#if __DEV
	u32	currCommandIdx = 1;
#endif // __DEV
	bool	bListTerminated = false;

	//Assert(addr < MAX_DRAW_LIST_SIZE);
	u32 currReadPos = addr;

	while(!bListTerminated){
		// pull out the command at the current write position
		dlCmdBase& drawListEntry = reinterpret_cast<dlCmdBase&>(m_pDrawCommandBuffer[currReadPos]);

		// update read position _before_ executing command, because we may want to get a data block after current command
		currReadPos += drawListEntry.GetCommandSizeStatic();
		Assert(currReadPos < MAX_DRAW_LIST_SIZE);

		// debug stuff - make sure that the expected command is being picked up
#if __DEV
		Assert(currCommandIdx == drawListEntry.GetCommandIdx());
		currCommandIdx++;
#endif // __DEV

		// check for list termination
		if (drawListEntry.IsEndOfDrawList()) {
#if __DEV
			Assert(finalIdx == drawListEntry.GetCommandIdx());
#endif // __DEV
			bListTerminated = true;
		}
	}
*/
	return(true);		// was able to check through the desired draw list successfully
}

// remove the next command out of the list and check it's a data block, then skip over the data to the next command
void* dlDrawCommandBuffer::GetDataBlock(u32 size, DrawListAddress::Parameter address/*=-1*/)
{
	dlCmdBase* base = NULL;

	DrawListAddress position(address);
	if (!position.IsNULL())
	{
		base = reinterpret_cast<dlCmdBase*>(ResolveDrawListAddress(position));

		// Skip the NOP block.
		if (base->GetInstructionIdStatic() == DC_NOP) 
		{
			position.MoveOffset(base->GetCommandSizeStatic());
			base = reinterpret_cast<dlCmdBase*>(ResolveDrawListAddress(position));
		}
	}
	else
	{
		base = reinterpret_cast<dlCmdBase*>(ResolveDrawListAddress(m_CurrReadAddr));

		// Skip the NOP block.
		if (base->GetInstructionIdStatic() == DC_NOP) 
		{
			DrawList_MoveOffset(m_CurrReadAddr,base->GetCommandSizeStatic());
			base = reinterpret_cast<dlCmdBase*>(ResolveDrawListAddress(m_CurrReadAddr));
			DEV_ONLY(m_execCommandIdx++;)
		}

		// Does this data block happen to be at the end of the queue?
		if (base->GetInstructionIdStatic() == DC_SwitchPage) 
		{
			// Gotta follow the page.
			m_CurrReadAddr = reinterpret_cast<dlCmdSwitchPage*>(base)->m_NewAddress;
			base = reinterpret_cast<dlCmdBase*>(ResolveDrawListAddress(m_CurrReadAddr));
			DEV_ONLY(m_execCommandIdx++;)
		}

		ASSERT_ONLY(position = m_CurrReadAddr;)

		// increment read position over the block to the next instruction
#if PAD_DATA_BLOCKS
		size += 0xf;
		size &= 0xfffffff0;
#endif
		const u32 blockSize = sizeof(dlCmdDataBlock) + size;
		DrawList_MoveOffset(m_CurrReadAddr,blockSize);
		DEV_ONLY(m_execCommandIdx++;)	// data block is counted as a command, so inc command idx to keep in sync
	}

	// get the data block entry off of the buffer
	char* pDataBlock = ((char*)base) + sizeof(dlCmdDataBlock);

	// Prefetch the next cache line. Data block are often packed close to each other, so this start 
	// getting the cache prepped with data in case we call GetDataBlock() again very soon. If not, 
	// this is still beneficial, as we'll be prefetching the next cache line on the draw list command 
	// buffer, which will be used for something else.
	PrefetchDC(pDataBlock + 128);
	
#if __ASSERT
	if (!base || !base->IsDataBlock()) 
	{
		dlAssertf(false, "Expected data block at offset %x", (DrawListAddress::Parameter) position);
		DumpCommandBuffer(false);
	}
#endif // __ASSERT

	return (void*)pDataBlock;
}

// get the ptr to the block of data at specified position in buffer or AFTER cmd (if pos=-1)
// works like GetDataBlock(), but drawlist can't be running (m_CurrReadAddr is not used/touched)
void* dlDrawCommandBuffer::GetDataBlockEx(dlCmdBase *cmd, DrawListAddress::Parameter _addr)
{
DrawListAddress address(_addr);

dlCmdBase *currCmd = NULL;
	if(address.IsNULL())
	{	
		Assert(cmd);
		// goto the next command, check it's a data block and extract wanted data:
		currCmd = (dlCmdBase*)(((u8*)cmd) + cmd->GetCommandSizeStatic());
	}
	else
	{
		currCmd = (dlCmdBase*)ResolveDrawListAddress(address); 
	}

	// any NOPs?
	if(currCmd->GetInstructionIdStatic() == DC_NOP)
	{
		const u32 nopSize = currCmd->GetCommandSizeStatic();
		currCmd = (dlCmdBase*)(((u8*)currCmd) + nopSize);
	}

	// Does this data block happen to be at the end of the queue?
	if(currCmd->GetInstructionIdStatic() == DC_SwitchPage)
	{
		// Gotta follow the page.
		DrawListAddress nextPageOffset = ((dlCmdSwitchPage*)currCmd)->m_NewAddress;
		currCmd = (dlCmdBase*)ResolveDrawListAddress(nextPageOffset);
	}

void* pData = NULL;

	Assertf(currCmd->IsDataBlock(), "GetDataBlockEx(): Error finding DataBlock!");
	if(currCmd->IsDataBlock())
	{
		dlCmdDataBlock *dataCmd = (dlCmdDataBlock*)currCmd;
		pData = ((u8*)dataCmd) + sizeof(dlCmdDataBlock);
	}

	return pData;
}

#if __ASSERT
dlCmdBase* prevCommand = NULL;
dlCmdBase* currCommand = NULL;
#endif

/** PURPOSE: Finds the position within the draw list pages of a shared object that has been added this frame.
 *
 *  PARAMS:
 *    dataType - Type of the data being shared. See eSharedMemType.
 *    sharedDataInfo - Object that identifies the object being shared. This is typically initialized via
 *                     AllocateSharedData.
 *
 *  RETURNS:
 *    A DrawListAddress that points to the position within the draw list pages, or NULL_DRAW_LIST_ADDRESS
 *    if this particular object has not been added to the command buffer in this frame.
 */
DrawListAddress::Parameter dlDrawCommandBuffer::LookupSharedData(int /*dataType*/, const dlSharedDataInfo &sharedDataInfo)
{
	// Do we already have a record for this object?
	if (m_Timestamp == sharedDataInfo.m_Timestamp) {
		// Yes. Use it.
		return sharedDataInfo.m_Offset;
	}

	// Nope.
	return (u32)DrawListAddress::NULL_DRAW_LIST_ADDRESS;
}

/** PURPOSE: Find the dlSharedDataInfo object that is identified by a certain ID.
 *  For this, the dlSharedDataInfo must have been allocated
 *  using dlSharedMemData::AllocateSharedData() with a user-provided ID.
 *
 *  PARAMS:
 *   dataType - Type of data being shared. See eSharedMemType.
 *   id - ID that was given the object when it was allocated.
 */
dlSharedDataInfo *dlDrawCommandBuffer::LookupSharedDataById(int dataType, const void* id)
{
	return m_SharedMemData[dataType].LookupSharedData(id);
}

/** PURPOSE: This function will populate a sharedDataInfo object. It is supposed to be called to register
 *  a new shared object that has been put into the draw list.
 *
 *  PARAMS:
 *  dataType - the type of data that is being shared here. See eSharedMemType.
 *  sharedDataInfo - an dlSharedDataInfo object that will be set up by this function.
 *  size - The size of the data block that is being shared.
 *  offset - The position within the draw list pages where the shared data is stored.
 */
void dlDrawCommandBuffer::AllocateSharedData(int STATS_ONLY(dataType), dlSharedDataInfo &sharedDataInfo, u32 STATS_ONLY(size), DrawListAddress::Parameter offset)
{
	// Don't add data twice.
#if __STATS
	dlAssert(DrawListAddress(LookupSharedData(dataType, sharedDataInfo)).IsNULL());
#endif

	// Update the shared data info block.
	sharedDataInfo.m_Offset = offset;
	sharedDataInfo.m_Timestamp = m_Timestamp;

#if __STATS
	m_SharedMemData[dataType].m_Usage += size;
	m_SharedMemData[dataType].m_Count++;
	m_SharedObjectCount++;
#endif // __STATS
}

/** PURPOSE: This function should be called if there is not enough space in the current page
 *  to fit the data that is intended to be written. This command will allocate a new draw page and,
 *  in case of DPT_SIMPLE_RING_BUFFER, add a dlCmdSwitchPage command that will automatically link
 *  to the new page.
 *
 *  After this call, the current write position will point to the beginning of the new page.
 *
 *  Note that this function is normally pretty fast, but if there is absolutely no memory available,
 *  it may block until memory has been reclaimed.
 *
 * PARAMS:
 *  type - The stream type that needs a new page. See eDrawListPageType.
 *  minPageSize - The minimum amount of data that needs to be added. Normally, the system will try
 *                to allocate a page of preferredPageSize, but if memory is low or
 *                fragmented, it will go for a smaller amount. However, there will always be enough space
 *                for minPageSize.
 *  preferredPageSize - The page size that the system will try to allocate. If there is not enough
 *                      memory, it will reduce the page size (until it reaches minPageSize).
 *  mayFail - Don't try TOO hard to get a page, and silently fail if a page could not be allocated.
 *
 * RETURNS:
 *  A DrawListAddress to the beginning of the new page.	This address will be invalid (i.e. IsNULL() returns
 *  true) if no memory could be allocated. This can ONLY happen if mayFail is true.
 */
DrawListAddress::Parameter dlDrawCommandBuffer::MoveToNewPage(eDrawListPageType type, u32 minPageSize, u32 preferredPageSize, bool mayFail) {
	Assert(type!=DPT_SIMPLE_RING_BUFFER || (sysThreadType::GetCurrentThreadType() & sysThreadType::THREAD_TYPE_UPDATE) != 0);

	dlDrawListPage *page = AllocateDrawListPage(type, minPageSize, preferredPageSize, mayFail);

	if (!page) {
		dlAssert(mayFail); // This may only happen if mayFail is true.
		return DrawListAddress();  	// Return the NULL equivalent
	}

	int newPageIndex = GetPageIndex(page);
	DrawListAddress address(newPageIndex, 0);

	DrawListAddress currentAddr(gDCBuffer->GetCurrentDrawListPosition(type));
	int currentPage = currentAddr.GetPageIndex();
	dlDrawListPage &currentPageObj = gDCBuffer->GetDrawListPage(currentPage);

	// Add the command for it.
	// We can't use the standard "operator new", or else it would fail the
	// buffer overflow check and try to wrap the buffer again.
	if (type == DPT_SIMPLE_RING_BUFFER) {
		dlCmdSwitchPage *switchCmd = (dlCmdSwitchPage *) gDCBuffer->GetCurrWriteAddr();
		::new(switchCmd) dlCmdSwitchPage(address);

		ADD_DLC(switchCmd);

		// Tally up how much we're wasting here.
	#if __STATS
		m_EndOfPageWaste += currentPageObj.GetSize() - currentAddr.GetOffset();
		m_DrawListSize += currentAddr.GetOffset() - m_CurrentPageStartOffset;
		m_CurrentPageStartOffset = 0;
	#endif // __STATS

		// Special case - if this was the very first command, it won't be included in the draw
		// list, so we need to remove its index.
#if __DEV
		if (dlCmdBase::GetCounter() == 2) {
			dlCmdBase::ResetCounter();
		}
#endif // __DEV
	}

	currentPageObj.SetNextPage(newPageIndex);

	// Now move the actual cursor.
	m_DrawListCurrentPosition[type] = address;

	return address;
}

/** PURPOSE: Add a block of data to the draw list command buffer if it hasn't already
 *  been added this frame. This function will add a dlCmdDataBlock command, followed by the data,
 *  if this is the first time the object has been shared.
 *
 *  NOTE: It is more efficient to maintain a dlSharedDataInfo object for your shared data
 *  if you have the option to do so.
 *
 *  PARAMS:
 *   sharedMemType - Type of data being shared. See eSharedMemType.
 *   memPtr - Pointer to the data to be copied into the command buffer.
 *   size - Size of the data in memPtr. Needs to be divisible by 4.
 *
 *  RETURNS: A pointer to the dlCmdDataBlock in the draw list (either the one that has just
 *  been created if this was the first time, or the one that has previously been copied with
 *  AddSharedDataBlock()).
 */
void* dlDrawCommandBuffer::AddSharedDataBlock(int sharedMemType, const void *memPtr, u32 size) {

	// Did we already use this?
	const void* id = memPtr;

	dlSharedDataInfo *sharedDataInfo = LookupSharedDataById(sharedMemType, id);

	if (sharedDataInfo) {
		// We already shared it before.
		DrawListAddress address = LookupSharedData(sharedMemType, *sharedDataInfo);

		// Is it still valid?
		if (!address.IsNULL()) {
			return ResolveDrawListAddress(address);
		}
	} else {
		// Let's share it.
		sharedDataInfo = &GetSharedMemData(sharedMemType).AllocateSharedData(id);
	}

    // inconsistent with documentation and other return path
    // should be returning pointer to dlCmdDataBlock but AddDataBlock returns the
    // actual data block following the dlCmdDataBlock
	return AddDataBlock(memPtr, size, sharedDataInfo->m_Offset);
}


/** PURPOSE: Copy the block of memory into the draw list command. To accomplish this, the function
 *  will first add a command of the type dlCmdDataBlock, and followed by the raw data.
 *
 *  PARAMS:
 *   srcMemPtr - Block of data that is to be copied into the DLC.
 *   size - Size of the data specified in srcMemPtr. Must be divisible by 4.
 *   offset - Reference to a DrawListAddress that will be set to the dlCmdDataBlock.
 *
 * RETURNS: Pointer to the actual data block in the draw list (not the dlCmdDataBlock).
 */
void* dlDrawCommandBuffer::AddDataBlock(const void* srcMemPtr, u32 size, DrawListAddress &offset){

	Assert(sysThreadType::IsUpdateThread());

	// Pad the block first, before we write down the offset.
	PAD_DLC<dlCmdDataBlock>();

	// Now that the write buffer points to an aligned address, store it.
	offset = m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER];

#if PAD_DATA_BLOCKS
	size += 0xf;
	size &= 0xfffffff0;
#endif

	// If we hit the end of the draw list, we need to loop.
	if (!CanFitData(size + 16)) {
		// We can't fit it - we need to loop around.
		offset = MoveToNewPage(DPT_SIMPLE_RING_BUFFER);
	}

	// create the command in the buffer to mark data (has been explicitly padded above)
	DLC_NOPAD(dlCmdDataBlock, (size));			// (this will inc m_currWritePos by sizeof(dlCmdDataBlock)

	void* pDataBlock = GetCurrWriteAddr();
	void* pData = pDataBlock;

	// copy over memory if desired
	if (srcMemPtr){
		// copy the data into the buffer (after the padded data block command)
		sysMemCpy(pData, srcMemPtr, size);
	}

	// update the write ptr to skip over the padded data block 
	m_DrawListCurrentPosition[DPT_SIMPLE_RING_BUFFER].MoveOffset(size);

	dlAssert(((size_t) pData & 15) == 0);

	return (pData);
}

/** PURPOSE: Clear the flags that prevented the update thread from writing to a certain
 *  part of the draw buffer queue.
 */
void	dlDrawCommandBuffer::UnlockReadBuffer() {

	// Don't unlock it twice.
	if (m_ConsumedTimestamp == m_Timestamp) {
		dlDebugf2("Draw list frame %d being unlocked twice", m_Timestamp);
		return;
	}

	m_ConsumedTimestamp = m_Timestamp;


	for (int x=0; x<DPT_MAX_TYPES; x++) {
		dlDrawListType &type = s_DrawListTypes[x];

		int historyIndex = type.m_Lifetime;

		// Put everything up to the current read position on the idle list so it can be reclaimed.
		DrawListAddress::Parameter startAddr = m_ReadBufferStartAddr[x][historyIndex];
		DrawListAddress::Parameter endAddr = m_ReadBufferEndAddr[x][historyIndex];

		if (!DrawListAddress(startAddr).IsNULL()) {
			dlDebugf3("Type %d: Marking for recycling: %x to %x", x, (DrawListAddress::Parameter) startAddr, (DrawListAddress::Parameter) endAddr);
			MarkPagesForRecycling(startAddr, endAddr);
		}

		m_ReadBufferStartAddr[x][historyIndex] = endAddr;
	}

	AgeAndDeleteOldPages();

	// If we're short on emergency pages, try to fill them up again.
	if (m_EmergencyPageList.GetCount() < MAX_EMERGENCY_PAGES) {
		dlDebugf3("Trying to fill up the emergency pages");

		// Find a slot for the new emergency page first.
		m_DrawListPageCritSec.Lock();

		if (m_DrawListFreeList.GetCount() > 0) {
			int drawListIndex = m_DrawListFreeList[0];
			m_DrawListFreeList.DeleteFast(0);
			m_DrawListPageCritSec.Unlock();

			// Politely ask for a page. If we can't get it, we can't get it.
#if __STATS
			m_PeakPageCount = Max(m_PeakPageCount, (u32) (MAX_DRAWLIST_PAGES - m_DrawListFreeList.GetCount()));
#endif // __STATS

			dlDrawListPage &page = GetDrawListPage(drawListIndex);

			if (!page.InitAndAllocate(DPT_SIMPLE_RING_BUFFER, DEFAULT_DRAWLIST_PAGE_SIZE)) {
				dlDebugf2("Failed to get a preventive emergency page");
				// Put the slot back. We'll try again next frame.
				m_DrawListPageCritSec.Lock();
				m_DrawListFreeList.Append() = drawListIndex;
				m_DrawListPageCritSec.Unlock();
			} else {
				dlDebugf2("Allocated preventive emergency page index %d", drawListIndex);
				// We got the page!
				page.SetEmergency(true);
				dlAssert(page.GetType() == DPT_SIMPLE_RING_BUFFER); // Pretty much guaranteed, InitAndAllocate does that, just keeping this in for now to make sure nothing broke
				page.Recycle();
				page.SetFrame(m_Timestamp);

				m_DrawListPageCritSec.Lock();
				dlAssertf(m_EmergencyPageList.Find(drawListIndex) == -1, "Page %d is put on the emergency list twice - this will cause a crash.", drawListIndex);
				dlAssertf(m_DrawListIdleList.Find(drawListIndex) == -1, "Page %d is erroneously on the idle list", drawListIndex);
				dlAssertf(m_DrawListFreeList.Find(drawListIndex) == -1, "Page %d is erroneously on the free list", drawListIndex);
				m_EmergencyPageList.Append() = drawListIndex;
				m_DrawListPageCritSec.Unlock();
			}
		} else {
			m_DrawListPageCritSec.Unlock();
		}
	}
}

void	dlDrawCommandBuffer::MarkPagesForRecycling(DrawListAddress::Parameter startAddr, DrawListAddress::Parameter endAddr) {
	int startPage = DrawListAddress(startAddr).GetPageIndex();
	int endPage = DrawListAddress(endAddr).GetPageIndex();

	SYS_CS_SYNC(m_DrawListPageCritSec);

	while (startPage != endPage) {
		dlDrawListPage &page = m_DrawListPages[startPage];
		dlDebugf3("Marking for recycling: %d", startPage);
		dlAssertf(m_DrawListIdleList.Find(startPage) == -1, "Page %d is put on the idle list twice - this will cause a crash (start=%x, end=%x).", startPage, startAddr, endAddr);
		dlAssertf(m_EmergencyPageList.Find(startPage) == -1, "Page %d is erroneously on the emergency page list", startPage);
		dlAssertf(m_DrawListFreeList.Find(startPage) == -1, "Page %d is erroneously on the free list", startPage);
		m_DrawListIdleList.Append() = startPage;

		if (m_DrawListFirstPage[page.GetType()] == startPage)
		{
			m_DrawListFirstPage[page.GetType()] = page.GetNextPage();
		}
		startPage = page.GetNextPage();

		Assertf(startPage != -1, "Somehow missed the end of the list of list type %d, start=%x, end=%x", page.GetType(), startAddr, endAddr);
	}
}

/** PURPOSE: Goes through every single draw list page that is currently allocated and increases its age.
 *  If the age hits m_MaxPageAge (=MAX_DRAWLIST_PAGE_AGE), the page will be immediately freed up.
 */
void	dlDrawCommandBuffer::AgeAndDeleteOldPages() {

	u32 curFrame = m_Timestamp;

	SYS_CS_SYNC(m_DrawListPageCritSec);

	for (int x=0; x<MAX_DRAWLIST_PAGES; x++) {
		dlDrawListPage &page = m_DrawListPages[x];

		if (page.IsAllocated() && !page.IsEmergency()) {
			u32 age = curFrame - page.GetFrame();

			if (age > m_MaxPageAge) {
				// It's no longer up for grabs then.
				int index = m_DrawListIdleList.Find(x);

				if (index != -1) {
					m_DrawListIdleList.DeleteFast(index);
				}

				dlDebugf2("Display list page %d is old (%d) - freeing up. Its idle index was %d", x, age, index);

				// If we need an emergency page, this is it. IF it is RscVirt, we don't RscPhys in our emergency list.
				if (m_EmergencyPageList.GetCount() < m_EmergencyPageList.GetMaxCount() && s_DrawListTypes[page.GetType()].m_Physical == false) {
					dlDebugf2("Putting page %d on the emergency list.", x);
					dlAssertf(m_EmergencyPageList.Find(x) == -1, "Page %d is put on the emergency list twice - this will cause a crash.", x);
					dlAssertf(m_DrawListIdleList.Find(x) == -1, "Page %d is erroneously on the idle list", x);
					dlAssertf(m_DrawListFreeList.Find(x) == -1, "Page %d is erroneously on the free list", x);
					m_EmergencyPageList.Append() = x;
					page.SetEmergency(true);
				} else {
					// Physically free this page - we no longer need its memory.
					FreeDrawListPage(x);
				}
			}
		}
	}
}

/** PURPOSE: This function needs to be called after a draw list has been completely finished.
 *  It will collect stats and update all the internal variables, like the start and end addresses
 *  for the render thread.
 *
 *  NOTE: The render thread may not be executing draw lists while this function is being called!
 */
void	dlDrawCommandBuffer::FlipBuffers(void) { 

	// Freeze buffer if requested.
#if __BANK
	if (m_FreezeBufferRequest) {
		FreezeCommandBuffer();
		m_FreezeBufferRequest = false;
	}
#endif // __BANK

	// Shift the buffer history down.
	for (int type=0; type<DPT_MAX_TYPES; type++) {
		for (int x=MAX_DRAWLIST_ADDR_HISTORY-1; x>0; x--) {
			m_ReadBufferStartAddr[type][x] = m_ReadBufferStartAddr[type][x-1];
			m_ReadBufferEndAddr[type][x] = m_ReadBufferEndAddr[type][x-1];
		}

		DrawListAddress currentWriteAddr = GetCurrentDrawListPosition((eDrawListPageType) type);
		m_ReadBufferEndAddr[type][0] = currentWriteAddr;
		m_ReadBufferStartAddr[type][0] = m_StartWriteAddr[type];
		m_StartWriteAddr[type] = currentWriteAddr;

		int curPageIndex = m_ReadBufferStartAddr[type][0].GetPageIndex();

		// Also, refresh the age on the end pos - this will be the start of the next frame, and
		// if the next frame has that little data that it won't need another page, nobody will
		// update the age, so it may be evicted while it is still in use.
		if (curPageIndex != -1) {
			m_DrawListPages[curPageIndex].SetFrame(m_Timestamp);
		}
	}


	// Write out the stats.
#if __STATS

	m_DrawListSize += m_ReadBufferEndAddr[DPT_SIMPLE_RING_BUFFER][0].GetOffset() - m_CurrentPageStartOffset;
	m_CurrentPageStartOffset = m_ReadBufferEndAddr[DPT_SIMPLE_RING_BUFFER][0].GetOffset();

	PF_SET(CommandBufferUsage, m_DrawListSize);
	PF_SET(SharedMemBlocks, m_SharedObjectCount);

	PF_SET(MatrixSetSharedDataCount, m_SharedMemData[DL_MEMTYPE_MATRIXSET].m_Count);
	PF_SET(MatrixSetSharedDataUsage, (m_SharedMemData[DL_MEMTYPE_MATRIXSET].m_Usage / 1024));

	u32 cmdCounter = (m_CmdCount) ? m_CmdCount : 1;
	PF_SET(AvgCommandSize, m_DrawListSize / cmdCounter);

	m_PeakSharedDataSize = Max(m_SharedDataSize, m_PeakSharedDataSize);
	PF_SET(PeakSharedDataSize, m_PeakSharedDataSize);
	PF_SET(SharedDataSize, m_SharedDataSize);
	PF_SET(PaddingWaste, m_PaddingWaste);
	PF_SET(EndOfPageWaste, m_EndOfPageWaste);
	PF_SET(InstructionCount, m_CmdCount);

	PF_SET(RingBufferPages, m_DrawListPageUsage[DPT_SIMPLE_RING_BUFFER]);
	PF_SET(LifetimeRenderThreadPages, m_DrawListPageUsage[DPT_LIFETIME_RENDER_THREAD]);
	PF_SET(LifetimeGpuPages, m_DrawListPageUsage[DPT_LIFETIME_GPU]);

	PF_SET(PageAllocations, m_PageAllocations);
	PF_SET(PageFrees, m_PageFrees);
	PF_SET(PeakPageCount, m_PeakPageCount);

	PF_SET(EmergencyPages, m_EmergencyPageList.GetCount());

	m_SharedObjectCount = 0;
	m_PaddingWaste = 0;
	m_EndOfPageWaste = 0;
	m_DrawListSize = 0;
#endif // __STATS

#if __BANK
	m_CmdCount = 0;

	fwRenderListBuilder::DebugDraw();
	fwRenderListBuilder::ResetRenderStats();
#endif // __BANK

#if !__FINAL
	gDrawListMgr->DebugDraw();
#endif // !__FINAL

	for (int x=0; x<MAX_DL_SHARED_MEM_TYPES; x++) {
		dlSharedMemData &memData = m_SharedMemData[x];

#if __STATS
		if (memData.m_EKGDataSizeMarker) {
			memData.m_EKGDataSizeMarker->Set(memData.m_Usage);
			memData.m_EKGCountMarker->Set(memData.m_Count);
		} else {
			dlAssertf(memData.m_Usage == 0, "Shared memory type %d has been used without a prior call to RegisterSharedMemType.", x);
		}

		memData.m_PeakUsage = Max(memData.m_Usage, memData.m_PeakUsage);
		memData.m_Usage = 0;

		memData.m_PeakCount = Max(memData.m_PeakCount, memData.m_Count);
		memData.m_Count = 0;
#endif // __STATS

		memData.ResetSharedData();
	}

#if __BANK
	sysMemSet(&m_InstructionConstructionTimings, 0, sizeof(m_InstructionConstructionTimings));
	sysMemSet(&m_InstructionExecutionTimings, 0, sizeof(m_InstructionExecutionTimings));
#endif // __BANK

	m_SharedDataSize = 0;

	dlCmdBase::ClearMemUse();

	m_Timestamp++;
}

DrawListAddress::Parameter	dlDrawCommandBuffer::LookupWriteSkelCopy(dlSharedDataInfo &sharedDataInfo){ 

//	Assert(CSystem::IsThisThreadId(SYS_THREAD_UPDATE));		// this command is forbidden outwith the main thread

	return gDCBuffer->LookupSharedData(DL_MEMTYPE_MATRIXSET, sharedDataInfo);
}



// make sure that the buffer has enough space for this command
void  dlDrawCommandBuffer::EnsureSpaceAvailable(s32 /*size*/, bool /*ASSERT_ONLY(bSkipValidListCheck)*/ /*=false*/)
{
/*
// need enough space for the command we are checking and for end of ring buffer marker
	if ((m_currWritePos + size) >= MAX_DRAW_LIST_SIZE)
	{
		static bool bDumpOnce = true;

		if (bDumpOnce){
			Assertf(false, "Draw command buffer has overflowed. Retry to dump buffer info.");
	#if __DEV
			dlCmdBase::RequestDumpData();
			dlCmdBase::ClearMemUse();
			Assertf(false, "Buffer info dumped OK.");
	#endif //__DEV
			bDumpOnce = false;
		}

		m_currWritePos = 0;
	}

	// check that we have started a list correctly (not adding entries outwith lists, or adding entries during execution)
	Assert((bSkipValidListCheck) || (gDrawListMgr->GetCurrBuildListIdx() != -1));	
*/
}


#if __DEV
//static s32 s_BreakOnWriteOffset = -1;
#endif // __DEV

#if __BANK
void dlDrawCommandBuffer::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Draw Command Buffer");
#if __PFDRAW
	bank.AddToggle("Show Usage Stats", &m_ShowInstructionUsage);
	bank.AddCombo("Usage Stats Sorting Criteria", &m_UsageStatsSortingCriteria, USAGE_SORT_TYPES, sm_UsageStatsSortingStrings);
#endif // __PFDRAW

	bank.PushGroup("Draw List Pages");
#if __STATS
	bank.AddToggle("Show Page Debug Info", &m_ShowPageDebug);
#endif // __STATS
	bank.AddSlider("Max Draw List Page Age Before Eviction", &m_MaxPageAge, 1, 1000, 1);
	bank.PopGroup();

	bank.AddButton("Freeze Command Buffer", datCallback(MFA(dlDrawCommandBuffer::FreezeCommandBufferRequest), this));

#if __PFDRAW
	bank.AddToggle("Show Frozen Buffer", &m_ShowFrozenBuffer);
	//bank.AddToggle("Render Frozen Buffer (DANGEROUS!)", &m_RenderFrozenBuffer);
	//bank.AddToggle("Render Frozen Buffer Up To Cursor Only", &m_DrawFrozenUntilCursorOnly);
	bank.AddSlider("Frozen Buffer Jog/Shuttle", &m_FrozenCursorMoveSpeed, -15.0f, 15.0f, 0.001f);
	bank.AddText("Frozen Buffer Search String", m_FrozenSearchString, sizeof(m_FrozenSearchString));
#endif // __PFDRAW

	bank.AddButton("Dump Frozen Buffer To TTY", datCallback(MFA(dlDrawCommandBuffer::DumpFrozenBuffer), this));

	bank.AddSlider("Current Debug Instruction ID", &m_CurrentDebugInstructionId, 0, JUMPTABLE_SIZE-1, 1, datCallback(MFA(dlDrawCommandBuffer::UpdateDebugInstructionId), this));
	bank.AddText("Debug Instruction Name", m_CurrentDebugInstructionName, sizeof(m_CurrentDebugInstructionName), true);
	bank.AddToggle("Add timing markers for instruction", &m_AddInstructionMarkers);

	bank.PushGroup("Stress Test");
	bank.AddToggle("Disable Emergency Pages", &m_DisableEmergencyPages);
	bank.AddSlider("Random Allocation Fail Rate", &m_RandomAllocFailRate, 0, 127, 1);
	bank.PopGroup();

	bank.PopGroup();
}

bool dlDrawCommandBuffer::ShouldRandomlyFailAlloc() {
	if (m_RandomAllocFailRate > 0) {
		if (s_DebugRandom.GetRanged(0, 127) < m_RandomAllocFailRate) {
			return true;
		}
	}

	return false;
}

void dlDrawCommandBuffer::UpdateDebugInstructionId()
{
	const char *instructionName = m_InstructionUsageStats[m_CurrentDebugInstructionId].m_Name;

	if (instructionName) {
		safecpy(m_CurrentDebugInstructionName, instructionName);
	} else {
		safecpy(m_CurrentDebugInstructionName, "[undefined]");
	}
}

void dlDrawCommandBuffer::FreezeCommandBufferRequest()
{
	m_FreezeBufferRequest = true;
}

void dlDrawCommandBuffer::DumpFrozenBuffer() {
	if (!m_pFrozenDebugBuffer) {
		Errorf("There is no frozen buffer. You need to freeze one via RAG first.");
	} else {
		int count = m_FrozenInstructionCount;

		Displayf("Index,Size,Command,Data,Extra Debug Info");

		for (int x=0; x<count; x++) {
			u32 offset = m_pFrozenDebugBufferOffsets[x];
			dlCmdBase &base = (dlCmdBase &) m_pFrozenDebugBuffer[offset];

			Color32 dummyColor;
			char line[192];
			DisassembleCommand(base, line, sizeof(line), dummyColor);
			Displayf("%s", line);
		}
	}
}

void dlDrawCommandBuffer::FreezeCommandBuffer()
{
	USE_DEBUG_MEMORY();

	// Delete the previous buffer.
	delete[] m_pFrozenDebugBuffer;
	delete[] m_pFrozenDebugBufferOffsets;

	Displayf("Freezing command buffer...");

	// Find out how big the command buffer is.
	u32 bufferSize = DEFAULT_DRAWLIST_PAGE_SIZE;

	DrawListAddress currentAddress(m_StartWriteAddr[DPT_SIMPLE_RING_BUFFER]);
	DrawListAddress endAddress(GetCurrentDrawListPosition(DPT_SIMPLE_RING_BUFFER));

	int endPageIndex = endAddress.GetPageIndex();
	int currentPageIndex = currentAddress.GetPageIndex();

	while (currentPageIndex != endPageIndex) {
		// TODO: Use the right size here.
		bufferSize += DEFAULT_DRAWLIST_PAGE_SIZE;

		currentAddress = DrawListAddress(GetDrawListPage(currentPageIndex).GetNextPage(), 0);
		currentPageIndex = currentAddress.GetPageIndex();
	}

	m_pFrozenDebugBuffer = rage_new char[bufferSize];
	m_pFrozenDebugBufferOffsets = rage_new u32[m_CmdCount];

	// Create offsets into the buffer and copy it over at the same time.
	int commands = m_CmdCount;
	int currentOffset = 0;

	m_FrozenInstructionCount = commands;

	currentAddress = m_StartWriteAddr[DPT_SIMPLE_RING_BUFFER];

	for (int x=0; x<commands; x++) {
		dlCmdBase &cmd = (dlCmdBase &) *ResolveDrawListAddress(currentAddress);
		sysMemCpy(&m_pFrozenDebugBuffer[currentOffset], &cmd, cmd.GetCommandSizeStatic());
		m_pFrozenDebugBufferOffsets[x] = currentOffset;

		if (cmd.GetInstructionIdStatic() == DC_SwitchPage) {
			currentAddress = ((dlCmdSwitchPage &) cmd).m_NewAddress;
		} else {
			currentAddress.MoveOffset(cmd.GetCommandSizeStatic());
		}

		currentOffset += cmd.GetCommandSizeStatic();
	}
}

int UsageSortByCount(int a, int b) {
	dlInstructionUsageStats &usageA = gDCBuffer->GetInstructionUsageStats(a);
	dlInstructionUsageStats &usageB = gDCBuffer->GetInstructionUsageStats(b);

	if (usageA.m_UsageCount < usageB.m_UsageCount) {
		return 1;
	}

	return 0;
}

int UsageSortBySize(int a, int b) {
	dlInstructionUsageStats &usageA = gDCBuffer->GetInstructionUsageStats(a);
	dlInstructionUsageStats &usageB = gDCBuffer->GetInstructionUsageStats(b);

	if (usageA.m_MemoryUsage < usageB.m_MemoryUsage) {
		return 1;
	}

	return 0;
}

#if __BANK
int UsageSortByConstructionTime(int a, int b) {
	utimer_t time1 = gDCBuffer->GetInstructionConstructionTiming(a);
	utimer_t time2 = gDCBuffer->GetInstructionConstructionTiming(b);

	if (time1 < time2) {
		return 1;
	}

	return 0;
}

int UsageSortByExecutionTime(int a, int b) {
	utimer_t time1 = gDCBuffer->GetInstructionExecutionTiming(a);
	utimer_t time2 = gDCBuffer->GetInstructionExecutionTiming(b);

	if (time1 < time2) {
		return 1;
	}

	return 0;
}
#endif // __BANK

void dlDrawCommandBuffer::UpdateDebugDraw()
{
#if __PFDRAW

	const unsigned debugDrawFlags = grcDebugDraw::FIXED_WIDTH | grcDebugDraw::NO_BG_QUAD | grcDebugDraw::RAW_COORDS;

	if (m_ShowInstructionUsage) {

		const Color32 UNUSED(0xff888888);
		const Color32 USED(0xffffffff);
		const Color32 EXPENSIVE(0xffffcccc);

		float xPos = 0.15f * (float) GRCDEVICE.GetWidth();
		float xColumn = 0.4f * (float) GRCDEVICE.GetWidth();
		float yPos = 0.15f * (float) GRCDEVICE.GetHeight();
		float yStep = (float) grcFont::GetCurrent().GetHeight();
		float yBottom = 0.9f * (float) GRCDEVICE.GetHeight();

		grcDebugDraw::PrintToScreenCoors("Draw Command Buffer Instruction Stats", (s32) xPos, (s32) yPos, USED, debugDrawFlags);
		yPos += yStep;

		// Display the legend.
		float xLegendCol = 0.1f * GRCDEVICE.GetWidth();
		grcDebugDraw::PrintToScreenCoors("UNUSED", (s32) xPos, (s32) yPos, UNUSED, debugDrawFlags);
		grcDebugDraw::PrintToScreenCoors("USED", (s32) (xPos + xLegendCol), (s32) yPos, USED, debugDrawFlags);
		grcDebugDraw::PrintToScreenCoors("EXPENSIVE", (s32) (xPos + xLegendCol * 2), (s32) yPos, EXPENSIVE, debugDrawFlags);

		yPos += yStep + 5.0f;

		float yTop = yPos;

		// Perform the sorting
		int mappedIndex[JUMPTABLE_SIZE];

		for (int x=0; x<JUMPTABLE_SIZE; x++) {
			mappedIndex[x] = x;
		}

		switch (m_UsageStatsSortingCriteria) {
			case USAGE_SORT_BY_ID:
				// Sorted by ID already - that's what we want.
				break;

			case USAGE_SORT_BY_COUNT:
				{
					std::sort(&mappedIndex[0], &mappedIndex[JUMPTABLE_SIZE], &UsageSortByCount);
				}
				break;

			case USAGE_SORT_BY_SIZE:
				{
					std::sort(&mappedIndex[0], &mappedIndex[JUMPTABLE_SIZE], &UsageSortBySize);
				}
				break;

			case USAGE_SORT_BY_CONSTRUCTION_TIME:
				{
#if __BANK
					std::sort(&mappedIndex[0], &mappedIndex[JUMPTABLE_SIZE], &UsageSortByConstructionTime);
#endif // __BANK
				}
				break;

			case USAGE_SORT_BY_EXECUTION_TIME:
				{
#if __BANK
					std::sort(&mappedIndex[0], &mappedIndex[JUMPTABLE_SIZE], &UsageSortByExecutionTime);
#endif // __BANK
				}
				break;

			default:
				dlAssert(0);
		}

		for (int x=0; x<JUMPTABLE_SIZE; x++) {
			// Only show actually defined commands.
			int index = mappedIndex[x];

			if (m_JumpTable[index]) {
				dlInstructionUsageStats &usage = GetInstructionUsageStats(index);

				Color32 newColor(USED);

				if (usage.m_UsageCount == 0) {
					newColor = UNUSED;
				} else if (usage.m_MemoryUsage > 5 * 1024) {
					newColor = EXPENSIVE;
				}

#if __BANK
				float constructionTime = (float) m_InstructionConstructionTimings[index] * sysTimerConsts::TicksToMilliseconds;
				float executionTime = (float) m_InstructionExecutionTimings[index] * sysTimerConsts::TicksToMilliseconds;
#else
				float constructionTime = -1.0f;
				float executionTime = -1.0f;
#endif

				char infoString[128];
				formatf(infoString, "%3d %2.2fms %2.2fms %s: %d (%dK)", index, constructionTime, executionTime, usage.m_Name, usage.m_UsageCount, usage.m_MemoryUsage / 1024);
				grcDebugDraw::PrintToScreenCoors(infoString, (s32) xPos, (s32) yPos, newColor, debugDrawFlags);
				yPos += yStep;

				// Wrap if we hit the bottom of the screen.
				if (yPos >= yBottom) {
					yPos = yTop;
					xPos += xColumn;
				}
			}
		}
	}

	if (m_ShowFrozenBuffer) {
		float xPos = 0.15f * (float) GRCDEVICE.GetWidth();
		float yPos = 0.5f * (float) GRCDEVICE.GetHeight();
		if (!m_pFrozenDebugBuffer) {
			grcDebugDraw::PrintToScreenCoors("You need to freeze the command buffer first before analyzing it.", (s32) xPos, (s32) yPos, Color_white, debugDrawFlags);
		} else {
			const Color32 NORMAL(0xffffffff);
			const Color32 CURRENT(0xff00ff00);
			const Color32 STRINGMATCH(0xff0088cc);
			char lcSearchString[sizeof(m_FrozenSearchString)];
			char lcCommandName[64];

			float yTop = 0.15f * (float) GRCDEVICE.GetHeight();
			float yBottom = 0.9f * (float) GRCDEVICE.GetHeight();
			float yHeight = (float) grcFont::GetCurrent().GetHeight();
			float fadeLength = yHeight;

			grcDebugDraw::PrintToScreenCoors("IDX   Size                        Command Payload", (s32) xPos, (s32) yTop, Color_white, debugDrawFlags);
			yTop += yHeight + 5.0f;

			float yTopBorder = yTop + (yHeight * 2.0f);
			float yBottomBorder = yBottom - (yHeight * 2.0f);

			// How many instructions can we show above the cursor?
			int readAhead = (int) ((yPos - yTop) / yHeight);

			// Get the index at the center of the cursor.
			int cursorIndex = (int) m_FrozenCursor;

			// Make sure that we cannot go past the beginnning of the buffer.
			readAhead = Min(cursorIndex, readAhead);

			// Move up to the top.
			yPos -= fmodf(m_FrozenCursor, 1.0f) * yHeight;
			yPos -= (float) readAhead * yHeight;

			// Make the search string lower case so we can do a substring more easily.
			// NOTE that we matched the size at compile-time, so strcpy is fine here.
			strcpy(lcSearchString, m_FrozenSearchString);
			strlwr(lcSearchString);
			

			u32 currentIndex = (u32) (cursorIndex - readAhead);

			while (yPos < yBottom && currentIndex < m_FrozenInstructionCount) {
				// Get the next command.
				char extraDebugBuffer[128];
				u32 offset = m_pFrozenDebugBufferOffsets[currentIndex];
				dlCmdBase &base = (dlCmdBase &) m_pFrozenDebugBuffer[offset];
				dlInstructionUsageStats &instInfo = GetInstructionUsageStats(base.GetInstructionIdStatic());
				const char *extraDebug = "";

				Color32 newColor(NORMAL);

				dlCmdExtraDebugCallback extraDebugCallback = m_ExtraDebugJumpTable[base.GetInstructionIdStatic()];

				if (extraDebugCallback) {
					extraDebug = extraDebugCallback(base, extraDebugBuffer, sizeof(extraDebugBuffer), newColor);

					if (!extraDebug) {
						extraDebug = "";
					}
				}

				// Highlight the commands that match the search string.
				if (lcSearchString[0]) {
					safecpy(lcCommandName, instInfo.m_Name);
					strlwr(lcCommandName);

					if (strstr(lcCommandName, lcSearchString)) {
						newColor = STRINGMATCH;
					}
				}

				// Highlight the center one.
				if (currentIndex == (u32) cursorIndex) {
					newColor = CURRENT;
				}

				// Fade around the borders.
				float fadeVal = yPos - yTopBorder;
				fadeVal = Min(fadeVal, yBottomBorder - yPos);
				fadeVal = Clamp(fadeVal, 0.0f, fadeLength);
				fadeVal /= fadeLength;
				newColor.SetAlpha((int) (fadeVal * 255.0f));

				char payload[128];
				payload[0] = 0;

				int size = base.GetCommandSizeStatic();

				u32 *payloadData = (u32 *) &base;

				// Skip the header, all the information has been decoded already.
				payloadData++;

				// Don't clutter the screen with too  much data.
				int counter = size >> 2;
				counter = Min(counter, 5);

				while (--counter > 0) {
					u32 word = *(payloadData++);

					char hexValStr[10];
					formatf(hexValStr, "%08x ", word);
					safecat(payload, hexValStr);
				}

				char line[192];
				formatf(line, "%4d %5d %32s %-38s %s", currentIndex, size, instInfo.m_Name, payload, extraDebug);
				grcDebugDraw::PrintToScreenCoors(line, (s32) xPos, (s32) yPos, newColor, debugDrawFlags);

				yPos += yHeight;
				currentIndex++;
			}
		}
	}

	if (m_ShowPageDebug) {
		const Color32 ALLOCATED(0xffffffff);
		const Color32 FREE(0xffaaaaaa);

		float xPos = 0.15f * (float) GRCDEVICE.GetWidth();

		float yPos = 0.15f * (float) GRCDEVICE.GetHeight();
		float yHeight = (float) grcFont::GetCurrent().GetHeight();

		grcDebugDraw::PrintToScreenCoors("IDX   Size  Status                Age Frame", (s32) xPos, (s32) yPos, ALLOCATED, debugDrawFlags);
		yPos += yHeight + 5.0f;

		u32 curFrame = m_Timestamp;

		for (int x=0; x<MAX_DRAWLIST_PAGES; x++) {
			dlDrawListPage &page = GetDrawListPage(x);
			char line[192];

			const char *status = "[free]";
			int age = 0;

			int frame = page.GetFrame();

			Color32 color(FREE);

			if (page.IsAllocated()) {
				color = ALLOCATED;
				eDrawListPageType type = page.GetType();
				status = s_DrawListTypes[type].m_Name;
				age = curFrame - frame;
			}

			grcColor(color);

			char size[8];
			formatf(size, "%dK", page.GetSize() / 1024);

			formatf(line, "%-5d %-5s %-21s %3d %3d %s", x, size, status, age, frame, (page.IsEmergency()) ? "EMERGENCY" : "");
			grcDebugDraw::PrintToScreenCoors(line, (s32) xPos, (s32) yPos, color, debugDrawFlags);

			yPos += yHeight;
		}
	}

#endif // __PFDRAW

	// Update the frozen buffer.
	// First, slow down the movement if we're hitting boundaries.
	if (m_FrozenCursor < 0.0f && m_FrozenCursorMoveSpeed < 0.0f) {
		m_FrozenCursorMoveSpeed *= 0.2f;
	}

	if (m_FrozenCursor > (float) m_FrozenInstructionCount && m_FrozenCursorMoveSpeed > 0.0f) {
		m_FrozenCursorMoveSpeed *= 0.2f;
	}

	m_FrozenCursor += m_FrozenCursorMoveSpeed;



	for (int x=0; x<JUMPTABLE_SIZE; x++) {
		dlInstructionUsageStats &usageStats = GetInstructionUsageStats(x);

		usageStats.m_UsageCount = 0;
		usageStats.m_MemoryUsage = 0;
	}
}

#endif // __BANK



// new operator for base class - passes back the right position in the ring buffer
void* dlCmdBase::operator new(size_t size, bool /*bSkipValidListCheck*/ /*=false*/)
{
	FastAssert((sysThreadType::GetCurrentThreadType()&sysThreadType::THREAD_TYPE_UPDATE) != 0);	// DLC commands are allowed only on Update Thread!

	// don't create new commands in the render thread!
	// If we hit the end of the draw list, we need to loop.
	if (!gDCBuffer->CanFitData((u32)size)) {
		// We can't fit it - we need to move to a new page.
		gDCBuffer->MoveToNewPage(DPT_SIMPLE_RING_BUFFER);
	}

	void* memPtr = gDCBuffer->GetCurrWriteAddr();
	dlAssert(memPtr);

#if __ASSERT
	prevCommand = currCommand;
	currCommand = reinterpret_cast<dlCmdBase*>(memPtr);
#endif

#if __DEV
	/*if( gDCBuffer->GetDrawListOffset() == u32(s_BreakOnWriteOffset) )
	{
		__debugbreak();
	}*/
#endif // __DEV

	dlAssertf(size < 10000, "Very large size for draw list command. Something probably very wrong...");

	// now we have the memPtr, we want to increment the write address ready for the data block in the constructor
	gDCBuffer->AddCurrWriteAddr((u32)size);		

	// if aborting then reset the current position (trashing buffer contents)
	/*if (gDCBuffer->AbortCheck()){
		memPtr = gDCBuffer->GetCurrWriteAddr();			// buffer overflow - trash it (don't use!)
	}*/

	return(memPtr);
}

#if __DEV
#if INSTR_TRACKING
void	dlCmdBase::ClearMemUse()	{
	u32 total = 0;
	u32 totalConsumed = 0;
	for(u32 i=0;i<DC_NumBaseDrawCommands;i++) {
		if (m_bDumpData){
			Displayf("Instr: %d,  freq: %d, total: %d, padded: %d, consumed %d\n", i,m_aFreq[i],m_aMemUsed[i],m_aMemUsedPadded[i], m_aMemConsumed[i]);
			Displayf("Breakdown -> ");
			for(u32 j=0;j<DL_MAX_TYPES;j++){
				if (m_aInstrCountByPhase[i][j] > 0){
					Displayf("[%d : %d] ",j, m_aInstrCountByPhase[i][j]);
				}
			}
			total+=m_aMemUsedPadded[i];
			totalConsumed += m_aMemConsumed[i];
			Displayf("\n");
		}

		m_aMemUsed[i] = m_aFreq[i] = m_aMemUsedPadded[i] = m_aMemConsumed[i] = 0; 
	}

	if (m_bDumpData){
		Displayf("Total padded: %d   total consumed %d \n",total, totalConsumed);
	}

	for(u32 i=0; i<DC_NumBaseDrawCommands; i++){
		for(u32 j=0; j<DL_MAX_TYPES;j++){
			m_aInstrCountByPhase[i][j] = 0;
		}
	}

	m_bDumpData = false;
}

void dlCmdBase::InitDebugMem(u32 size){

	ResetDebugMem();

	m_aMemUsed.Resize(size);
	m_aMemUsedPadded.Resize(size);
	m_aFreq.Resize(size);
	m_aMemConsumed.Resize(size);
}

void dlCmdBase::ResetDebugMem(void){
	m_aMemUsed.Reset();
	m_aMemUsedPadded.Reset();
	m_aFreq.Reset();
	m_aMemConsumed.Reset();

	for(u32 i=0; i<DC_NumBaseDrawCommands; i++){
		for(u32 j=0; j<DL_MAX_TYPES;j++){
			m_aInstrCountByPhase[i][j] = 0;
		}
	}
}
#endif //INSTR_TRACKING
#endif //__DEV

// break point for position in drawlist
#if __DEV
void	dlCmdBase::BuildListBreakCheck(){

	static	s32 breakCommandIdx = -1;
	static	s32 breakListIdx = -1;

	s32 drawList = gDrawListMgr->GetCurrBuildListIdx();
	if ((static_cast<s16>(m_commandIdx) == breakCommandIdx) && (drawList == breakListIdx)){
		printf("break");
	}
}
#endif //__DEV

// data block 
// mark as data (don't try to execute it!
dlCmdDataBlock::dlCmdDataBlock(u32 dataSize){

	m_dataSize = dataSize;
}

// allow execution of data block - it doesn't do anything but keeps the command idx of the buffer in sync
void dlCmdDataBlock::Execute(){
	//Assert(false);			// something went wrong if we tried to execute this...
}

//#################################################################################
// --- draw list control commands --- 
// create new draw list
dlCmdNewDrawList::dlCmdNewDrawList(eDrawListType type){

	m_type = type;
	gDCBuffer->SetBuildRenderPhase(type);

#if !__FINAL || __FINAL_LOGGING
	m_FrameNumber = gDrawListMgr->GetUpdateThreadFrameNumber();
#endif // !__FINAL || __FINAL_LOGGING

#if RSG_PC
	// HACK - talk to Klaas before changing this
	// Since fwTimer::GetNonPausableCamFrameCount() should be equal to fwTimer::GetSystemFrameCount(), unless
	// the Knock() anti-tamper has been triggered, this value should always be divisible by 2.
	m_FrameCounts = fwTimer::GetNonPausableCamFrameCount() + fwTimer::GetSystemFrameCount();
	Assert((m_FrameCounts & 1) == 0);
	// HACK - talk to Klaas before changing this
#endif

	m_drawListIdx = ++m_currDrawListIdx;

	// Get offset of start of list (which would be _before_ this command was added to list.
	// While GetCurrentDrawListPosition is generally not thread safe, DPT_SIMPLE_RING_BUFFER is only accessed from the main thread.
	DrawListAddress listOffset(gDCBuffer->GetCurrentDrawListPosition(DPT_SIMPLE_RING_BUFFER));
	listOffset.MoveOffset(-GetCommandSize());

	//s32 listOffset = gDCBuffer->GetDrawListOffset() - GetCommandSize();
	// register that we've started a new list with the list manager (store type & start location of list)
	gDrawListMgr->NewDrawList(type, listOffset);

	// debug stuff (add to stats for draw lists)
	gDrawListMgr->RegisterDrawlist(type);

	m_currDrawListLODFlagShift_UpdateThread = gDrawListMgr->GetDrawListLODFlagShift(type);
}

void dlCmdNewDrawList::Execute(){
	dlAssert(m_drawListIdx == gDCBuffer->GetCurrListId());		// make sure we're executing the list we want to be...

#if RSG_PC
	// HACK talk to klaas before changing this
	// Since fwTimer::GetNonPausableCamFrameCount() should be equal to fwTimer::GetSystemFrameCount(), unless
	// the Knock() anti-tamper has been triggered, this value should always be divisible by 2
	// So (m_FrameCounts & 1) == 0, and m_currDrawListLODFlagShift should always == gDrawListMgr->GetDrawListLODFlagShift(m_type).
	Assert((m_FrameCounts & 1) == 0);
	m_currDrawListLODFlagShift = (m_FrameCounts & 1) + gDrawListMgr->GetDrawListLODFlagShift(m_type);
	gDrawListMgr->SetRatchet(!!(m_currDrawListLODFlagShift - gDrawListMgr->GetDrawListLODFlagShift(m_type)));
	m_currDrawListLODFlagShift -= (m_FrameCounts & 1);
	// HACK talk to klaas before changing this
#else
	m_currDrawListLODFlagShift = gDrawListMgr->GetDrawListLODFlagShift(m_type);
#endif

#if !__FINAL || __FINAL_LOGGING
	if (gRenderThreadInterface.IsRenderThreadSynchronized()) {
		Assertf(gRenderThreadInterface.GetRenderThreadFrameNumber() == m_FrameNumber, "Update and render thread are out of sync! Update thread has been pushing draw list %d, but render thread expected list %d.",
			m_FrameNumber, gRenderThreadInterface.GetRenderThreadFrameNumber());
	} else {
		// This is the first time we're consuming a draw list since we switched back to synchronized mode.
		dlDebugf2("Synchronized udpate and render thread - frame number %d", m_FrameNumber);
		gRenderThreadInterface.SetRenderThreadFrameNumber(m_FrameNumber);
	}
#endif // !__FINAL || __FINAL_LOGGING

#if __DEV
	char listName[64];
	formatf(listName, "%s %d", gDrawListMgr->GetDrawListName(m_type), m_FrameNumber);
	GRCDBG_PUSH(listName);
//	extern bool bShowDrawlistExec;
//	if (bShowDrawlistExec){
//		gDrawListMgr->PrintDrawListType(m_type);
//	}

	g_CurrentDrawListTypeName = gDrawListMgr->GetDrawListName(m_type);
#endif //__DEV

#if DEBUG_SEALING_OF_DRAWLISTS
	grcTextureFactory::GetInstance().SetDrawListDebugString(gDrawListMgr->GetDrawListName(m_type));

	if(grcTextureFactory::GetInstance().AreAnyRenderTargetsSet())
	{
		grcDisplayf("dlCmdNewDrawList::dlCmdNewDrawList()...At start of DL:%s", gDrawListMgr->GetDrawListName(m_type));
		grcTextureFactory::GetInstance().OutputSetRenderTargets();
		grcDisplayf("========\n");
	}
#endif // DEBUG_SEALING_OF_DRAWLISTS
}

const char *dlCmdNewDrawList::GetExtraDebugInfo(dlCmdBase & base, char * /*buffer*/, size_t /*bufferSize*/, Color32 &color) {
	color = Color32(0x00ff00ff);
	return gDrawListMgr->GetDrawListName(((dlCmdNewDrawList &) base).GetDrawListType());
}


// --- Mark end of draw list ---
// create new draw list
dlCmdEndDrawList::dlCmdEndDrawList(){
	Assertf(gDrawListMgr->IsEndingDrawList(), "dlCmdEndDrawList may not be added directly. Please use gDrawListMgr->EndCurrentDrawList() instead.");
}

void dlCmdEndDrawList::Execute(){

#if DEBUG_SEALING_OF_DRAWLISTS
	if(grcTextureFactory::GetInstance().AreAnyRenderTargetsSet())
	{
		grcDisplayf("dlCmdEndDrawList::Execute()...At end of DL:%s", grcTextureFactory::GetInstance().GetDrawListDebugString());
		grcTextureFactory::GetInstance().OutputSetRenderTargets();
		grcDisplayf("========\n");
	}
	if(grcTextureFactory::GetInstance().HaveAnyDrawsBeenIssuedWithNoTargetsSet() == true)
	{
		grcDisplayf("dlCmdEndDrawList::Execute()...WARNING!!! DL %s issued draw calls with no render targets set.", grcTextureFactory::GetInstance().GetDrawListDebugString());
	}
#endif // DEBUG_SEALING_OF_DRAWLISTS
	
#if __DEV
	GRCDBG_POP();
#endif 
	// gDrawListMgr->MarkAsExecuted();		// notify mgr that this draw list has finished executing
}

dlCmdBeginDraw::dlCmdBeginDraw(grcSetup* setup, bool clear) : m_Setup(setup), m_Clear(clear) {}

void dlCmdBeginDraw::Execute()
{
	m_Setup->BeginDraw(m_Clear);
}

dlCmdEndDraw::dlCmdEndDraw(grcSetup* setup) : m_Setup(setup) {}

void dlCmdEndDraw::Execute()
{
	m_Setup->EndDraw();
}

//#################################################################################
// --- Compute Shader Batching --- 
dlComputeCmdBase::dlComputeCmdBase(const functor_type &dispatchFunc, const functor_type &copyStructureCountFunc DURANGO_ONLY(, bool needsAutomaticGpuFlush))
: m_Next(NULL)
, m_DispatchFunc(dispatchFunc)
, m_CopyStructureCountFunc(copyStructureCountFunc)
DURANGO_ONLY(, m_NeedsAutomaticGpuFlush(needsAutomaticGpuFlush))
ORBIS_ONLY(, m_FlushAfterDispatch(false))
{
	Assertf(dispatchFunc.IsBound(), "dlComputeCmdBase constructed with an invalid dispatch functor! Will not be able to run CS!");
	dlComputeShaderBatch::push_back(this);
}

void dlComputeCmdBase::DispatchComputeShader()
{
	if(Verifyf(m_DispatchFunc.IsBound(), "Dispatch functor is invalid! CS will not run!"))
		m_DispatchFunc();

	ORBIS_ONLY(if(m_FlushAfterDispatch) GRCDEVICE.SynchronizeComputeToGraphics());
}

void dlComputeCmdBase::CopyStructureCount()
{
	if(m_CopyStructureCountFunc.IsBound())
		m_CopyStructureCountFunc();
}

dlComputeShaderBatch *dlComputeShaderBatch::sm_Current = NULL;
ORBIS_ONLY(u32 dlComputeShaderBatch::sm_CurrGDSOffset = 0);

void dlComputeShaderBatch::push_back(dlComputeCmdBase *cmd)
{
	if(cmd && Verifyf(sm_Current, "WARNING: Trying to push a CS job before a dlComputeShaderBatch object has been created for the frame! CS will not run!"))
	{
#if RSG_ORBIS
		if(sm_CurrGDSOffset + (EFFECT_UAV_BUFFER_COUNT * 4) >= 0x8000)
		{
			//Flush & Reset GDS offset.
			cmd->m_FlushAfterDispatch = true;
			sm_CurrGDSOffset = 0;
		}
#endif //RSG_ORBIS

#	if RSG_DURANGO
		if(!cmd->m_NeedsAutomaticGpuFlush)
		{
			if(sm_Current->m_HeadNoFlush)
			{
				FatalAssert(sm_Current->m_TailNoFlush);
				sm_Current->m_TailNoFlush->m_Next = cmd;
				sm_Current->m_TailNoFlush = sm_Current->m_TailNoFlush->m_Next;
			}
			else
			{
				sm_Current->m_HeadNoFlush = sm_Current->m_TailNoFlush = cmd;
			}
		}
		else
#	endif //RSG_DURANGO
		{
			if(sm_Current->m_Head)
			{
				FatalAssert(sm_Current->m_Tail);
				sm_Current->m_Tail->m_Next = cmd;
				sm_Current->m_Tail = sm_Current->m_Tail->m_Next;
			}
			else
			{
				sm_Current->m_Head = sm_Current->m_Tail = cmd;
			}
		}
	}
}

#if RSG_ORBIS
u32 dlComputeShaderBatch::GetGDSOffsetForCounter()
{
	u32 currOffset = sm_CurrGDSOffset;
	sm_CurrGDSOffset += 4;

	Assert(currOffset < 0x8000);
	return currOffset;
}
#endif //RSG_ORBIS

void dlComputeShaderBatch::Execute()
{
	GRC_ALLOC_SCOPE_AUTO_PUSH_POP();
	PF_PUSH_MARKER("Compute Shader Batch");
	ORBIS_ONLY(gfxc.setShaderType(sce::Gnm::kShaderTypeCompute));

#if RSG_DURANGO
	if(m_HeadNoFlush)
	{
		GRCDEVICE.CSEnableAutomaticGpuFlush(false);
		dlComputeCmdBase *currNoFlush = m_HeadNoFlush;
		while(currNoFlush)
		{
			currNoFlush->DispatchComputeShader();
			currNoFlush = currNoFlush->m_Next;
		}
		GRCDEVICE.CSEnableAutomaticGpuFlush(true);

		//Flush Compute.
		PF_PUSH_MARKER("Flush Compute Shaders (No Hazard Tracking)");
		g_grcCurrentContext->GpuSendPipelinedEvent(D3D11X_GPU_PIPELINED_EVENT_CS_PARTIAL_FLUSH);
		g_grcCurrentContext->InsertWaitUntilIdle(0);
		g_grcCurrentContext->FlushGpuCacheRange(static_cast<UINT>(D3D11_FLUSH_DEFAULT_PFP), 0, 0);
		PF_POP_MARKER();
	}
#endif //RSG_DURANGO

	dlComputeCmdBase *curr = m_Head;
	while(curr)
	{
		curr->DispatchComputeShader();
		curr = curr->m_Next;
	}

	ORBIS_ONLY(gfxc.setShaderType(sce::Gnm::kShaderTypeGraphics));


	PF_PUSH_MARKER("Copy Structure Count Batch");
#if RSG_DURANGO
	curr = m_HeadNoFlush;
	while(curr)
	{
		curr->CopyStructureCount();
		curr = curr->m_Next;
	}
#endif //RSG_DURANGO

	curr = m_Head;
	while(curr)
	{
		curr->CopyStructureCount();
		curr = curr->m_Next;
	}
	PF_POP_MARKER();
	PF_POP_MARKER();
}

//#################################################################################
// --- alloc scope commands ---

#if GRCCONTEXT_ALLOC_SCOPES_SUPPORTED

enum { MAX_DLC_ALLOC_SCOPES = 4 };
static grcContextAllocScope s_dlcCmdAllocScopes[NUMBER_OF_RENDER_THREADS][MAX_DLC_ALLOC_SCOPES];
static unsigned s_dlcCmdAllocNumScopes[NUMBER_OF_RENDER_THREADS];

// - alloc scope push -
void dlCmdAllocScopePush::Execute()
{
	const unsigned rti = g_RenderThreadIndex;
	const unsigned idx = s_dlcCmdAllocNumScopes[rti];
	FatalAssert(idx < MAX_DLC_ALLOC_SCOPES);
	s_dlcCmdAllocNumScopes[rti] = idx+1;
	s_dlcCmdAllocScopes[rti][idx].pushScope();
}

// - alloc scope pop -
void dlCmdAllocScopePop::Execute()
{
	const unsigned rti = g_RenderThreadIndex;
	const unsigned idx1 = s_dlcCmdAllocNumScopes[rti];
	FatalAssert(idx1 > 0);
	const unsigned idx = idx1-1;
	s_dlcCmdAllocNumScopes[rti] = idx;
	s_dlcCmdAllocScopes[rti][idx].popScope();
}

#else
void dlCmdAllocScopePush::Execute()
{
}
void dlCmdAllocScopePop::Execute()
{
}
#endif

//#################################################################################
// --- render target commands --- 
// - lock render target - 
dlCmdLockRenderTarget::dlCmdLockRenderTarget(s32 idx, grcRenderTarget* colourRT, grcRenderTarget* depthRT, int arrayIndex)
{
	// both targets shouldn't be NULL, and colour cannot be null on PC (otherwise debug runtime says "Direct3D9: (ERROR) :Illegal to set render target 0 to NULL)"
	dlAssert((colourRT!=NULL)||(!(__WIN32PC && __D3D9)&&(depthRT!=NULL)));

	m_idx = idx;
	m_pColourRT = colourRT;
	m_pdepthRT = depthRT;
	m_arrayIndex = arrayIndex;
}

void dlCmdLockRenderTarget::Execute(){

	grcTextureFactory::GetInstance().LockRenderTarget(m_idx, m_pColourRT, m_pdepthRT, m_arrayIndex);
}

// - unlock render target - 
dlCmdUnLockRenderTarget::dlCmdUnLockRenderTarget(s32 idx, grcResolveFlags* pFlags/*=NULL*/) {
	m_idx = idx;

	m_flagsValid = false;

	// should be OK if pFlags is NULL - looks like the grcResolveFlags struct is initialised to sensible values in constructor
	if (pFlags){
		m_flags = *pFlags;
		m_flagsValid = true;
	}
}

void dlCmdUnLockRenderTarget::Execute(){
	if (m_flagsValid){
		grcTextureFactory::GetInstance().UnlockRenderTarget(m_idx, &m_flags);
	} else {
		grcTextureFactory::GetInstance().UnlockRenderTarget(m_idx);
	}
}

dlCmdSetArrayView::dlCmdSetArrayView(int arrayIndex)
{
	m_arrayIndex = arrayIndex;
}

void dlCmdSetArrayView::Execute(){

	grcTextureFactory::GetInstance().SetArrayView(m_arrayIndex);
}

#if RSG_ORBIS
dlCmdGenerateMipMaps::dlCmdGenerateMipMaps(grcRenderTarget *renderTarget) {
	m_pRenderTarget = renderTarget;
}

void dlCmdGenerateMipMaps::Execute(){
	m_pRenderTarget->GenerateMipmaps();
}
#endif

const grcViewport *dlCmdSetCurrentViewport::sm_CurrSetViewportUpdateThread = NULL;

void dlCmdSetCurrentViewport::Execute(){

	if (m_bIsVPNull){
		grcViewport::SetCurrent(NULL);
	} else {
#		if GRCCONTEXT_ALLOC_SCOPES_SUPPORTED && __ASSERT
			// Setting the viewport should be valid up until the end of the
			// current context.  So if the current allocation scope is locked
			// (and .: probably the top level context scope), temporarily unlock
			// it just so any allocations won't assert.  This code should
			// compile to nothing in non-assert builds, but wrapping in an #if
			// __ASSERT just to be explicit.
			const bool wasLocked = grcGfxContext::isLockedAllocScope();
			if (wasLocked) {
				grcGfxContext::unlockAllocScope();
			}
#		endif

		dlAssert(m_CurrViewport.GetWindow().GetNormX() < 2.0f);
		grcViewport::SetCurrent(&m_CurrViewport);

#		if GRCCONTEXT_ALLOC_SCOPES_SUPPORTED && __ASSERT
			if (wasLocked) {
				grcGfxContext::lockAllocScope();
			}
#		endif
	}
}

// -------------------------
// - clear render targets -

// clear back buffer
dlCmdClearRenderTarget::dlCmdClearRenderTarget(bool bEnableClearColour, Color32 clearColour, bool bEnableClearDepth, float clearDepth, bool bEnableClearStencil, u32 clearStencil){

	m_bEnableClearColour = bEnableClearColour;
	m_clearColour = clearColour;
	
	m_bEnableClearDepth =  bEnableClearDepth;
	m_clearDepth = clearDepth;

	m_bEnableClearStencil = bEnableClearStencil;
	m_clearStencil = clearStencil;
	
#if __DEV
	m_creatorStack = sysStack::RegisterBacktrace();
#endif
}

bool had_error;

void  dlCmdClearRenderTarget::Execute(){

	GRC_ALLOC_SCOPE_AUTO_PUSH_POP()
#if __PS3
	 //as long as viewport set correctly this is fine
	GRCDEVICE.Clear(	m_bEnableClearColour,	m_clearColour, 
						m_bEnableClearDepth,	m_clearDepth, 
						m_bEnableClearStencil,	m_clearStencil);
#else //__PS3
	if (m_bEnableClearColour || m_bEnableClearDepth || m_bEnableClearStencil){
		//as long as viewport set correctly this is fine
		GRCDEVICE.Clear(	m_bEnableClearColour,	m_clearColour, 
			m_bEnableClearDepth,	m_clearDepth, 
			m_bEnableClearStencil,	m_clearStencil);
	}
#if __DEV
	if (had_error)
		sysStack::PrintRegisteredBacktrace(m_creatorStack);
#endif
#endif //__PS3

}

// -------------------------
// - clip planes -

// define clip plane
dlCmdSetClipPlane::dlCmdSetClipPlane(u32 idx, Vec4V_In clipPlane) {

	m_idx = idx;
	m_clipPlane = clipPlane;
}

void  dlCmdSetClipPlane::Execute(){
	// copied over from CRenderPhaseClearBackbuffer::Render() in RenderPhaseStd.cpp
	GRCDEVICE.SetClipPlane(m_idx, m_clipPlane);
}

// enable / disable clip planes
dlCmdSetClipPlaneEnable::dlCmdSetClipPlaneEnable(u32 mask) {

#if RSG_PC
	// Under normal (non-tampered) conditions, GetNonPausableCamFrameCount() == GetSystemFrameCount().
	// Which means that under normal confitions, (m_mask & 0x00010000) == 0
	m_mask = GRCDEVICE.MakeKillSwitch(mask, (fwTimer::GetNonPausableCamFrameCount() + fwTimer::GetSystemFrameCount()) & 1);
#else
	m_mask = mask;
#endif
}

void  dlCmdSetClipPlaneEnable::Execute(){
	// copied over from CRenderPhaseClearBackbuffer::Render() in RenderPhaseStd.cpp
#if RSG_PC
	GRCDEVICE.SetKillSwitch(m_mask);
	GRCDEVICE.SetClipPlaneEnable(GRCDEVICE.ExtractKillSwitchData(m_mask));
#else
	GRCDEVICE.SetClipPlaneEnable(m_mask & 0x0000FFFF);
#endif
}

// - set shader allocation -
dlCmdSetShaderGPRAlloc::dlCmdSetShaderGPRAlloc(u32 arg1, u32 arg2){
	m_arg1 = arg1;
	m_arg2 = arg2;
}
void  dlCmdSetShaderGPRAlloc::Execute(){
#if __XENON
	GRCDEVICE.SetShaderGPRAllocation(m_arg1, m_arg2);
#endif //__XENON
}

// -SetLightingMode
dlCmdGrcLightStateSetEnabled::dlCmdGrcLightStateSetEnabled(bool lightingMode){
	m_enableLighting = lightingMode;
}
void dlCmdGrcLightStateSetEnabled::Execute() { grcLightState::SetEnabled(m_enableLighting); }

// -SetScissor
dlCmdGrcDeviceSetScissor::dlCmdGrcDeviceSetScissor(u32 x, u32 y, u32 width, u32 height) {
	m_x = x; 
	m_y = y; 
	m_width = width; 
	m_height = height;
}

void dlCmdGrcDeviceSetScissor::Execute() { GRCDEVICE.SetScissor(m_x, m_y, m_width, m_height); }

#if __XENON
dlCmdXenonSetScissor::dlCmdXenonSetScissor(u32 x, u32 y, u32 width, u32 height) 
{
	grcAssertf(x >= 0 && x <= 8192,"Invalid x in dlCmdXenonSetScissor(), x (%d) should be between 0 and 8192 inclusive",x);
	grcAssertf(y >= 0 && y <= 8192,"Invalid y in dlCmdXenonSetScissor(), y (%d) should be between 0 and 8192 inclusive",y);
	grcAssertf(width >= 0 && width <= 8192,"Invalid width in dlCmdXenonSetScissor() width (%d) should be between 0 and 8192 inclusive",width);
	grcAssertf(height >= 0 && height <= 8192,"Invalid height in gdlCmdXenonSetScissor(), height (%d) should be between 0 and 8192 inclusive",height);

	m_left   = x;
	m_right  = x + width;
	m_top    = y;
	m_bottom = y + height;

#if __ASSERT
	u32 gameWidth  = (u32)GRCDEVICE.GetWidth();
	u32 gameHeight = (u32)GRCDEVICE.GetHeight();
#endif
	Assertf(m_right <= gameWidth, "w=%d", gameWidth);
	Assertf(m_bottom <= gameHeight, "h=%d", gameHeight);
}

void dlCmdXenonSetScissor::Execute() 
{ 
	RECT rect = { m_left, m_top, m_right, m_bottom };

	GRCDEVICE.GetCurrent()->SetScissorRect(&rect); 
}
#endif

void dlCmdBeginConditionalRender::Execute()
{
	GRCDEVICE.BeginConditionalRender(m_query);
}

dlCmdBeginConditionalRender::dlCmdBeginConditionalRender(grcConditionalQuery query)
{
	m_query = query;
}

void dlCmdEndConditionalRender::Execute()
{
	GRCDEVICE.EndConditionalRender(m_query);
}

dlCmdEndConditionalRender::dlCmdEndConditionalRender(grcConditionalQuery query)
{
	m_query = query;
}


#if RSG_PC && __D3D11

// Locks or unlocks the context.
dlCmdGrcDeviceLockOrUnlockContext::dlCmdGrcDeviceLockOrUnlockContext(bool LockOrUnlock) {
	m_LockOrUnlock = LockOrUnlock;
}

void dlCmdGrcDeviceLockOrUnlockContext::Execute() { m_LockOrUnlock ? GRCDEVICE.LockContext() : GRCDEVICE.UnlockContext(); }


// Storage for when we have to defer adding commands until a draw list is available.
u32 dlCmdGrcDeviceUpdateBuffer::sm_NoOfDeferredUpdates = 0;
dlCmdGrcDeviceUpdateBuffer::BUFFER_UPDATE_DETAILS dlCmdGrcDeviceUpdateBuffer::m_DeferredUpdates[DL_CMD_GRCDEVICE_UPDATEBUFFER_MAX_DEFERRED];

// Updates a vertex or index buffer.
dlCmdGrcDeviceUpdateBuffer::dlCmdGrcDeviceUpdateBuffer(void *pBuffer, void *pMemPtr, u32 OffsetOrLayer, u32 SizeOrMipLevel, bool IsTexture) 
{
	AssertMsg( pBuffer && pMemPtr, "Invalid d3d buffer" );

#if __ASSERT
	u32 uFakeCount = 0;
	uFakeCount += ((u8*)pMemPtr)[0];
	Assert(uFakeCount < ~0);

	if(IsTexture == false)
	{
		grcBufferD3D11 * pd3dBuffer = (grcBufferD3D11 *)pBuffer;
		Assert( pd3dBuffer->GetD3DBuffer() );
		AssertMsg(pd3dBuffer->GetD3DBuffer() != (ID3D11Buffer*)0xefefefef, "DX11: Adding an already expired buffer for update" );
		if(pd3dBuffer->GetD3DBuffer() == (ID3D11Buffer*)0xefefefef )
			return;
		if(pd3dBuffer->GetD3DBuffer() == (ID3D11Buffer*)0xcdcdcdcd )
			return;
	}
	else
	{
		grcTextureD3D11 * pd3dTexture = (grcTextureD3D11 *)pBuffer;
		Assert( pd3dTexture->GetTexturePtr() );

		AssertMsg(pd3dTexture->GetTexturePtr() != (grcTextureObject*)0xefefefef, "DX11: Adding an already expired texture for update" );
		if(pd3dTexture->GetTexturePtr() == (grcTextureObject*)0xefefefef )
			return;
		if(pd3dTexture->GetTexturePtr() == (grcTextureObject*)0xcdcdcdcd )
			return;
	}
#endif // __ASSERT

	m_Details.pBuffer = pBuffer;
	m_Details.pMemPtr = pMemPtr;
	m_Details.IsTexture = IsTexture;
	m_Details.OffsetOrLayer = OffsetOrLayer;
	m_Details.SizeOrMipLevel = SizeOrMipLevel;
}

void dlCmdGrcDeviceUpdateBuffer::Execute() 
{ 
	if(m_Details.IsTexture == false)
	{
		((grcBufferD3D11 *)m_Details.pBuffer)->UpdateGPUCopy(m_Details.pMemPtr, m_Details.OffsetOrLayer, m_Details.SizeOrMipLevel);
	}
	else
	{
		((grcTextureD3D11 *)m_Details.pBuffer)->UpdateGPUCopy(m_Details.OffsetOrLayer, m_Details.SizeOrMipLevel, m_Details.pMemPtr);
	}
}


void dlCmdGrcDeviceUpdateBuffer::AddBufferUpdate(void *pBufferObject, void *pMemPtr, u32 Offset, u32 Size, bool uponCancelMemoryHint)
{
	AddUpdateCommandToDrawList(pBufferObject, pMemPtr, Offset, Size, false, uponCancelMemoryHint);
}

void dlCmdGrcDeviceUpdateBuffer::AddTextureUpdate(void *pTextureObject, void *pMemPtr, u32 Layer, u32 MipLevel, bool uponCancelMemoryHint)
{
	AddUpdateCommandToDrawList(pTextureObject, pMemPtr, Layer, MipLevel, true, uponCancelMemoryHint);
}

void *dlCmdGrcDeviceUpdateBuffer::AllocTempMemory(u32 size)
{
	// This is actually keeping the pages alive one frame too long since the
	// allocation is done on the render thread or subrender threads.  But,
	// AllocTempMemory should be able to be deleted soon(ish) after B*1778350.
	return (void *)gDCBuffer->AllocateObjectFromPagedMemory<char *>(DPT_LIFETIME_GPU_PHYSICAL, size, false);
}

sysCriticalSectionToken		s_LockDeferredUpdateList;

void dlCmdGrcDeviceUpdateBuffer::AddUpdateCommandToDrawList(void *pBufferObject, void *pMemPtr, u32 OffsetOrLayer, u32 SizeOrMipLevel, bool IsTexture, bool uponCancelMemoryHint) 
{	
	sysCriticalSection critSec(s_LockDeferredUpdateList);

	// We`ll need to defer this update.
	if(sm_NoOfDeferredUpdates < DL_CMD_GRCDEVICE_UPDATEBUFFER_MAX_DEFERRED)
	{
		BUFFER_UPDATE_DETAILS *pDest = &m_DeferredUpdates[sm_NoOfDeferredUpdates++];
		pDest->pBuffer = pBufferObject;
		pDest->pMemPtr = pMemPtr;
		pDest->IsTexture = IsTexture;
		pDest->UponCanelHint = uponCancelMemoryHint;
		pDest->OffsetOrLayer = OffsetOrLayer;
		pDest->SizeOrMipLevel = SizeOrMipLevel;

#if __ASSERT
		u32 uFakeCount = 0;
		uFakeCount += ((u8*)pMemPtr)[0];
		Assert(uFakeCount < ~0);
#endif // __ASSERT
	}
	ASSERT_ONLY(else{ AssertMsg( 0, "dlCmdGrcDeviceUpdateBuffer::AddBufferUpdateCommandToDrawList()...Need more capacity for deferred buffer updates!"); } )
}


void dlCmdGrcDeviceUpdateBuffer::CancelBufferUpdate(void *pBufferObject)
{
	sysCriticalSection critSec(s_LockDeferredUpdateList);

	u32 i;

	// NOTE:- This list has very few entries.
	for(i=0; i<sm_NoOfDeferredUpdates; i++)
	{
		if(m_DeferredUpdates[i].pBuffer == pBufferObject)
			m_DeferredUpdates[i].pBuffer = NULL;
		// We may have multiple updates of the buffer so don`t early out.
	}
}


void dlCmdGrcDeviceUpdateBuffer::OnBeginRender()
{
	sysCriticalSection critSec(s_LockDeferredUpdateList);

	if(sm_NoOfDeferredUpdates)
	{
		// We don`t expect to stall on executing this command. It`s inserted to ensure we have a valid context pointer.
		DLC(dlCmdGrcDeviceLockOrUnlockContext, (true));

		// Add any deferred commands.
		for(u32 i=0; i<sm_NoOfDeferredUpdates; i++)
		{
			if(m_DeferredUpdates[i].pBuffer)
			{
				DLC(dlCmdGrcDeviceUpdateBuffer, (m_DeferredUpdates[i].pBuffer, m_DeferredUpdates[i].pMemPtr, m_DeferredUpdates[i].OffsetOrLayer, m_DeferredUpdates[i].SizeOrMipLevel, m_DeferredUpdates[i].IsTexture)); 
			}
		}

		DLC(dlCmdGrcDeviceLockOrUnlockContext, (false));

		sm_NoOfDeferredUpdates = 0;
	}
}

void dlCmdGrcDeviceUpdateBuffer::OnEndRender()
{
}


// This function is called by the game thread, inside the safe area, when a draw function other than the default one is active
// as no drawlist is available on which to place the GPU update commands,
void dlCmdGrcDeviceUpdateBuffer::DoPendingBufferUpdates()
{
	sysCriticalSection critSec(s_LockDeferredUpdateList);

	if(sm_NoOfDeferredUpdates)
	{
		PF_AUTO_PUSH_TIMEBAR("DoPendingBufferUpdates");

		for(u32 i=0; i<sm_NoOfDeferredUpdates; i++)
		{
			if(m_DeferredUpdates[i].pBuffer)
			{
				if(m_DeferredUpdates[i].IsTexture == true)
				{
					((grcTextureD3D11 *)m_DeferredUpdates[i].pBuffer)->UpdateGPUCopy_WithThreadSyncing(m_DeferredUpdates[i].OffsetOrLayer, m_DeferredUpdates[i].SizeOrMipLevel, m_DeferredUpdates[i].pMemPtr);
				}
				else
				{
					((grcBufferD3D11 *)m_DeferredUpdates[i].pBuffer)->UpdateGPUCopy_WithThreadSyncing(m_DeferredUpdates[i].pMemPtr, m_DeferredUpdates[i].OffsetOrLayer, m_DeferredUpdates[i].SizeOrMipLevel);
				}
			}
		}
		sm_NoOfDeferredUpdates = 0;
	}
}

#endif // RSG_PC && __D3D11

// -DisableScissor
dlCmdGrcDeviceDisableScissor::dlCmdGrcDeviceDisableScissor() {
}

void dlCmdGrcDeviceDisableScissor::Execute() { GRCDEVICE.DisableScissor(); }


// --- dlCmdInsertEndFence ---
dlCmdInsertEndFence::dlCmdInsertEndFence() {}
// put the end fence into the GPU command buffer
void  dlCmdInsertEndFence::Execute()
{ 	
#if !RSG_PC
	// don't want to do this if not actually using a render thread
	gDrawListMgr->InsertEndFence();
#endif
}

// draw radar map section
// (assumes that the texture dictionary the texture is in has a ref added beforehand)
dlCmdDrawTriShape::dlCmdDrawTriShape(Vector2* pos, Vector2* texCoord, const grcTexture* pTex, Color32 col, grcDrawMode shape, u32 count){

	dlAssert(count <= 6);

	for(u32 i=0;i<count;i++){
		m_pos[i] = pos[i];
		m_texCoord[i] = texCoord[i];
	}
	m_pTex = pTex;
	m_col = col;
	m_count = count;

	m_shape = shape;
}

void dlCmdDrawTriShape::Execute(){

	grcBindTexture(m_pTex);
	grcBegin(m_shape, m_count);
	for (u32 i = 0; i < m_count ; i++)
	{
		grcVertex(m_pos[i].x, m_pos[i].y, 0.0f, 0, 0, -1, m_col, m_texCoord[i].x, m_texCoord[i].y);
	}
	grcEnd();
}

// ShaderTechnique Stack
void dlCmdShaderFxPushForcedTechnique::Execute()
{
	const s32 previousGroupId = grmShaderFx::GetForcedTechniqueGroupId();
	gDrawListMgr->PushForcedTechnique(previousGroupId);
	grmShaderFx::SetForcedTechniqueGroupId(m_newTechnique);
}

dlCmdShaderFxPushForcedTechnique::dlCmdShaderFxPushForcedTechnique(const s32 technique)
{
	m_newTechnique = technique;
}


void dlCmdShaderFxPopForcedTechnique::Execute(void)
{
	const s32 previousGroupId = gDrawListMgr->PopForcedTechnique();
	grmShaderFx::SetForcedTechniqueGroupId(previousGroupId);
}

#if __DEV

static bool s_DrawListMarkerPrintOutOnExecute = true;

dlCmdAddDrawListMarker::dlCmdAddDrawListMarker(const char *marker)
{
	rage::safecpy(m_id,marker,DRAWLISTMARKERSIZE);
}

void dlCmdAddDrawListMarker::Execute()
{
	if( s_DrawListMarkerPrintOutOnExecute )
	{
		Displayf("DrawList Marker %s",m_id);
	}
}

#endif // __DEV

#if !__FINAL
static bool s_DisplayPushBars = false;

dlCmdPushTimebar::dlCmdPushTimebar(const char *name, const float budget)
{
	PF_PUSH_TIMEBAR_BUDGETED(name, budget);
	Assertf(!sysIpcIsStackAddress(name), "Timebar name string '%s' is on the stack.", name);
	m_name = name;
	m_budget = budget;
}

void dlCmdPushTimebar::Execute()
{
	PF_PUSH_TIMEBAR_BUDGETED(m_name,m_budget);
	if (s_DisplayPushBars)
		Displayf("Bar: %s", m_name);
}

dlCmdPopTimebar::dlCmdPopTimebar()
{
	PF_POP_TIMEBAR();
}

void dlCmdPopTimebar::Execute()
{
	PF_POP_TIMEBAR();
	if (s_DisplayPushBars)
		Displayf("Unbar");
}

static grcGpuTimer GPUTimebarTimers[dlCmdPushGPUTimebar::TIMER_COUNT];
static DECLARE_MTR_THREAD const char *GPUTimebarName = NULL;
static DECLARE_MTR_THREAD int GPUTimebarTimerIdx = -1;
static DECLARE_MTR_THREAD float GPUTimerbarBudget;

dlCmdPushGPUTimebar::dlCmdPushGPUTimebar(const int timerIdx, const char *name, const float budget)
{
	Assertf(!sysIpcIsStackAddress(name), "GPU timebar name string '%s' is on the stack.", name);
	m_name = name;
	m_budget = budget;
	m_timerIdx = timerIdx;
	Assert(m_name != NULL);
	Assert(timerIdx < dlCmdPushGPUTimebar::TIMER_COUNT);
}

void dlCmdPushGPUTimebar::Execute()
{
	Assertf(GPUTimebarName == NULL, "Should be NULL but %s is active still when pushing %s", GPUTimebarName, m_name);
	GPUTimebarName = m_name;
	GPUTimebarTimerIdx = m_timerIdx;
	GPUTimerbarBudget = m_budget;
	GPUTimebarTimers[GPUTimebarTimerIdx].Start();

	if (s_DisplayPushBars)
		Displayf("GpuBar: %s", m_name);
}

void dlCmdPopGPUTimebar::Execute()
{
	Assert(GPUTimebarName != NULL);
#if RAGE_TIMEBARS
	float result = GPUTimebarTimers[GPUTimebarTimerIdx].Stop();
	PF_INSERT_GPUBAR_BUDGETED(GPUTimebarName,(result<33.3f?result:0.0f),GPUTimerbarBudget);
#endif
	GPUTimebarName = NULL;
	if (s_DisplayPushBars)
		Displayf("UnGpuBar");
}

#if ENTITY_GPU_TIME

static const int       kEntityGPUTimerCount   = 32;
static int             sEntityGPUTimerIndex    = 0;
static grcGpuTimer     sEntityGPUTimer[kEntityGPUTimerCount];
#if ENTITY_GPU_TIME == 1
static float           sEntityGPUTimeAccum     = 0.0f;
#endif // ENTITY_GPU_TIME == 1
static float           sEntityGPUTime          = 0.0f;
static u32             sEntityGPUFrame         = 0;
static float           sEntityGPUFrameTime[32] = {0.0f};
static int             sEntityGPUFramePeriod   = 8; // max = 16
static grcSetup*       sEntityGPUSetup         = NULL;
static const fwEntity* sEntityGPUEntity        = NULL;

void            dlCmdEntityGPUTimePush::Execute() { sEntityGPUTimer[sEntityGPUTimerIndex].Start(); }
void            dlCmdEntityGPUTimePush::SetEntity(const fwEntity* pEntity) { sEntityGPUEntity = pEntity; }
const fwEntity* dlCmdEntityGPUTimePush::GetEntity() { return sEntityGPUEntity; }
bool            dlCmdEntityGPUTimePush::GetFrame() { return (int)sEntityGPUFrame >= sEntityGPUFramePeriod; }
//int&          dlCmdEntityGPUTimePush::GetFramePeriodRef() { return sEntityGPUFramePeriod; }

void dlCmdEntityGPUTimePop::Execute()
{
#if ENTITY_GPU_TIME == 1
	sEntityGPUTimeAccum += sEntityGPUTimer[sEntityGPUTimerIndex++].Stop();
	sEntityGPUTimerIndex %= kEntityGPUTimerCount;
#endif // ENTITY_GPU_TIME == 1
}

void dlCmdEntityGPUTimeFlush::Execute()
{
#if ENTITY_GPU_TIME == 1
	sEntityGPUTime = sEntityGPUTimeAccum;
	sEntityGPUTimeAccum = 0.0f;
#elif ENTITY_GPU_TIME == 2
	sEntityGPUFrameTime[sEntityGPUFrame++] = sEntityGPUSetup ? sEntityGPUSetup->GetGpuTime() : 0.0f;

	if ((int)sEntityGPUFrame >= sEntityGPUFramePeriod*2)
	{
		sEntityGPUFrame = 0;
	}

	float t = 0.0f;

	for (int i = 0; i < sEntityGPUFramePeriod*2; i++)
	{
		t += sEntityGPUFrameTime[i]*(i < sEntityGPUFramePeriod ? -1.0f : 1.0f);
	}

	sEntityGPUTime = Abs<float>(t/(float)sEntityGPUFramePeriod);

	if (sEntityGPUSetup)
	{
		//sEntityGPUSetup->SetEntTime(sEntityGPUTime);
	}
#endif // ENTITY_GPU_TIME == 2
}

void dlCmdEntityGPUTimeFlush::SetSetup(grcSetup* setup)
{
	sEntityGPUSetup = setup;
}

float dlCmdEntityGPUTimeFlush::GetTime()
{
	return sEntityGPUTime;
}

#endif // ENTITY_GPU_TIME

dlCmdPushMarker::dlCmdPushMarker(const char *name)
{
	Assertf(!sysIpcIsStackAddress(name), "Marker name string '%s' is on the stack.", name);
	m_name = name;
}

void dlCmdPushMarker::Execute()
{
	PF_PUSH_MARKER(m_name);
#if RSG_ORBIS	// Should be safe to enable on other platforms but I'm not sure if we want to pay the performance hit all the time.
	BANK_ONLY(diagContextMessage::Push("DL: %s",m_name,NULL,NULL));
#endif
	if (s_DisplayPushBars)
		Displayf("Marker: %s", m_name);
}

void dlCmdPopMarker::Execute()
{
#if RSG_ORBIS
	BANK_ONLY(diagContextMessage::Pop());
#endif
	PF_POP_MARKER();
	if (s_DisplayPushBars)
		Displayf("UnMarker");
}
#endif // !__FINAL

dlCmdSetGeometryVertexOffsets::dlCmdSetGeometryVertexOffsets(grmGeometry* pGeom, grcVertexBuffer* pVB)
{
	m_pGeometry = pGeom;
	m_pVertexBuffer = pVB;
}

void dlCmdSetGeometryVertexOffsets::Execute()
{
#if !__PPU
	m_pGeometry->SetOffsets(m_pVertexBuffer);
#endif //__PPU
}

void dlCmdSwitchPage::Execute()
{
	gDCBuffer->SetCurrentDrawListReadPosition(m_NewAddress);
}

#if __PPU
//#################################################################################
// -- Cell commands -- 
dlCmdCellGcmSetTransferData::dlCmdCellGcmSetTransferData(	u8 mode, 
															u32 dstOffset, u32 dstPitch, 
															u32 srcOffset, u32 srcPitch, 
															u32 bytesPerRow, u32 rowCount)
{
	m_mode = mode;
	m_dstOffset = dstOffset;
	m_dstPitch = dstPitch;
	m_srcOffset = srcOffset;
	m_srcPitch = srcPitch;
	m_bytesPerRow = bytesPerRow;
	m_rowCount = rowCount;
}

void dlCmdCellGcmSetTransferData::Execute()
{
	gcm::SetTransferData(GCM_CONTEXT,
						 m_mode,
						 m_dstOffset, m_dstPitch,
						 m_srcOffset, m_srcPitch,
						 m_bytesPerRow, m_rowCount);
}

dlCmdBeginAdaptiveZCulling::dlCmdBeginAdaptiveZCulling(int i)
{
	// initialize the values for the adaptive Z culling algorithm
	m_Dir = 1;
	m_Step = 0x0001;
	m_Prev = 0.00f;

	gDrawListMgr->SetShadowTile(i);
}

void dlCmdBeginAdaptiveZCulling::Execute()
{
	float RatioTilesCulled = gDrawListMgr->GetRatioTilesCulled();

	GRCDEVICE.BeginAdaptiveZCulling(m_MoveForward, m_Dir, m_Step, 
								    m_Prev, RatioTilesCulled);
}

void dlCmdEndAdaptiveZCulling::Execute()
{
	const float RatioTilesCulled = GRCDEVICE.EndAdaptiveZCulling();

	gDrawListMgr->SetRatioTilesCulled(RatioTilesCulled);
}

#if __PS3
void dlCmdInvalidateZCulling::Execute()
{
	GRCDEVICE.InvalidateZCulling();
}
#endif //__PS3

#if USE_EDGE

// set up global shadow map params for Edge:
dlCmdEdgeShadowGlobalParams::dlCmdEdgeShadowGlobalParams(const Matrix44 &shadowMatrix)
{
	m_shadowMatrix = shadowMatrix;
}

void dlCmdEdgeShadowGlobalParams::Execute()
{
#if SPU_GCM_FIFO && 1 
	SPU_COMMAND(GTA4__SetShadowMatrix,0);
	cmd->shadowMatrix = m_shadowMatrix;
#endif
}

//
//
// non-DLC version of CEdgeShadowGlobalParams: does exactly what is says on the tin (tm):
//
void dlCmdEdgeShadowGlobalParams::ExecuteNow(const Matrix44 &shadowMatrix)
{
//	Assert(CSystem::IsThisThreadId(SYS_THREAD_RENDER));
#if 1 && SPU_GCM_FIFO
	SPU_COMMAND(GTA4__SetShadowMatrix,0);
	cmd->shadowMatrix = shadowMatrix;
#endif //SPU_GCM_FIFO...
}// end of dlCmdEdgeShadowGlobalParams::ExecuteNow()...


// set up global shadow map params for Edge:
dlCmdEdgeShadowType::dlCmdEdgeShadowType(eEdgeShadowType shadowType)
{
	m_shadowType = shadowType;
}

void dlCmdEdgeShadowType::Execute()
{
#if SPU_GCM_FIFO
	SPU_COMMAND(GTA4__SetShadowType,0);
	cmd->shadowType = (u32)m_shadowType; // eEdgeShadowType
#endif
}

#endif //USE_EDGE...

//
//
//
//

#endif // __PPU

void dlCmdSetBucketsAndRenderMode::SetUpdateBuckets() const
{
	gDrawListMgr->SetUpdateBucket(m_bucket);
	gDrawListMgr->SetUpdateRenderMode(m_renderMode);
	gDrawListMgr->SetUpdateSubBucketMask(m_subBucketMask);
}

void dlCmdSetBucketsAndRenderMode::SetRtBuckets() const
{
	gDrawListMgr->SetRtBucket(m_bucket);
	gDrawListMgr->SetRtRenderMode(m_renderMode);
	gDrawListMgr->SetRtSubBucketMask(m_subBucketMask);
}

dlCmdSetBucketsAndRenderMode::dlCmdSetBucketsAndRenderMode(const u32 bucket, const u32 subBucketMask, const int renderMode)
{
	m_bucket = bucket;
	m_subBucketMask = subBucketMask;
	m_renderMode = renderMode;
	
	SetUpdateBuckets();
}

dlCmdSetBucketsAndRenderMode::dlCmdSetBucketsAndRenderMode(const u32 bucket, const int renderMode)
{
	m_bucket = bucket;
	m_subBucketMask = gDrawListMgr->GetDefaultSubBucketMask();
	m_renderMode = renderMode;
	
	SetUpdateBuckets();
}

dlCmdSetBucketsAndRenderMode::dlCmdSetBucketsAndRenderMode(const u32 subBucketMask)
{
	m_bucket = gDrawListMgr->GetUpdateBucket();
	m_renderMode = gDrawListMgr->GetUpdateBucket();
	m_subBucketMask = subBucketMask;
	
	SetUpdateBuckets();
}

void dlCmdSetBucketsAndRenderMode::Execute() { 
	SetRtBuckets();
}

//
//dlCmdSetSubBucketMask::dlCmdSetSubBucketMask(const u32 subBucketMask){
//	gDrawListMgr->SetUpdateSubBucketMask(subBucketMask);
//	u32 bucketMask = ((subBucketMask << 8) & 0xff00) | ((1U << gDrawListMgr->GetUpdateBucket()) & 0xff);
//	gDrawListMgr->SetUpdateBucketMask(bucketMask);
//	m_SubBucketMask = subBucketMask;
//}
//void dlCmdSetSubBucketMask::Execute() { 
//	gDrawListMgr->SetRtSubBucketMask(m_SubBucketMask); 
//	u32 bucketMask = ((m_SubBucketMask << 8) & 0xff00) | ((1U << gDrawListMgr->GetRtBucket()) & 0xff);
//	gDrawListMgr->SetRtBucketMask(bucketMask);
//}


dlCmdSetRasterizerState::dlCmdSetRasterizerState(grcRasterizerStateHandle rasterizer)
{
	m_rasterizer = rasterizer;
}

void dlCmdSetRasterizerState::Execute()
{
	grcStateBlock::SetRasterizerState(m_rasterizer);
}

dlCmdSetDepthStencilState::dlCmdSetDepthStencilState(grcDepthStencilStateHandle depth)
{
	m_depth = depth;
}

void dlCmdSetDepthStencilState::Execute()
{
	grcStateBlock::SetDepthStencilState(m_depth);
}

dlCmdSetDepthStencilStateEx::dlCmdSetDepthStencilStateEx(grcDepthStencilStateHandle depth, u32 stencilRef)
{
	m_depth = depth;
	m_stencilRef = stencilRef;
}

void dlCmdSetDepthStencilStateEx::Execute()
{
	grcStateBlock::SetDepthStencilState(m_depth,m_stencilRef);
}

dlCmdSetBlendState::dlCmdSetBlendState(grcBlendStateHandle blend)
{
	m_blend = blend;
}

void dlCmdSetBlendState::Execute()
{
	grcStateBlock::SetBlendState(m_blend);	
}

dlCmdSetBlendStateEx::dlCmdSetBlendStateEx(grcBlendStateHandle blend, float blendFactors[4],u64 sampleMask)
{
	m_blend = blend;
	m_blendFactors = grcStateBlock::PackBlendFactors(blendFactors);
	m_sampleMask = sampleMask;
}

void dlCmdSetBlendStateEx::Execute()
{
	grcStateBlock::SetBlendState(m_blend,m_blendFactors,m_sampleMask);
}

dlCmdSetStates::dlCmdSetStates(grcRasterizerStateHandle rasterizer, grcDepthStencilStateHandle depth, grcBlendStateHandle blend)
{
	m_rasterizer = rasterizer;
	m_depth = depth;
	m_blend = blend;
}

void dlCmdSetStates::Execute()
{
	grcStateBlock::SetStates(m_rasterizer, m_depth, m_blend);
}

dlCmdSetStatesEx::dlCmdSetStatesEx(grcRasterizerStateHandle rasterizer, grcDepthStencilStateHandle depth, u32 stencilRef, grcBlendStateHandle blend, float blendFactors[4], u64 sampleMask)
{
	m_rasterizer = rasterizer;
	m_depth = depth;
	m_stencilRef = stencilRef;
	m_blend = blend;
	m_blendFactors = grcStateBlock::PackBlendFactors(blendFactors);
	m_sampleMask = sampleMask;
}

void dlCmdSetStatesEx::Execute()
{
	grcStateBlock::SetRasterizerState(m_rasterizer);
	grcStateBlock::SetDepthStencilState(m_depth,m_stencilRef);
	grcStateBlock::SetBlendState(m_blend,m_blendFactors,m_sampleMask);
}



#if 0
dlCmdPopDefaultSamplerState::dlCmdPopDefaultSamplerState(grceSamplerState samplerState)
: m_SamplerState(samplerState)
{
}

void dlCmdPopDefaultSamplerState::Execute() {
	grcEffect::PopDefaultSamplerState(m_SamplerState);
}
#endif


#if __XENON

void dlCmdEnableHiZ( bool bSetHiZWriteState )
{
	GRCDEVICE.GetCurrent()->SetRenderState( D3DRS_HIZENABLE, TRUE );
	if ( bSetHiZWriteState )
	{
		GRCDEVICE.GetCurrent()->SetRenderState( D3DRS_HIZWRITEENABLE, TRUE );
	}

}
void dlCmdEnableAutomaticHiZ( bool bSetHiZWriteState )
{
	GRCDEVICE.GetCurrent()->SetRenderState( D3DRS_HIZENABLE, D3DHIZ_AUTOMATIC );
	if ( bSetHiZWriteState )
	{
		GRCDEVICE.GetCurrent()->SetRenderState( D3DRS_HIZWRITEENABLE, D3DHIZ_AUTOMATIC );
	}
}
void dlCmdDisableHiZ( bool bSetHiZWriteState )
{
	GRCDEVICE.GetCurrent()->SetRenderState( D3DRS_HIZENABLE, FALSE );
	if ( bSetHiZWriteState )
	{
		GRCDEVICE.GetCurrent()->SetRenderState( D3DRS_HIZWRITEENABLE, FALSE );
	}

}
void dlCmdSetHiZ( bool readState, bool writeState)
{
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_HIZENABLE,			readState);
	GRCDEVICE.GetCurrent()->SetRenderState(D3DRS_HIZWRITEENABLE,	writeState);
}
#endif

} // namespace rage

