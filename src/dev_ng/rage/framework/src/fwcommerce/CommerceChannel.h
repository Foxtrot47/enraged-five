// 
// fwcommerce/CommerceChannel.h 
// 


#ifndef FWCOMMERCE_COMMERCE_CHANNEL_H 
#define FWCOMMERCE_COMMERCE_CHANNEL_H

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(commerce)

#define commerceAssert(cond)                  RAGE_ASSERT(commerce,cond)
#define commerceAssertf(cond,fmt,...)         RAGE_ASSERTF(commerce,cond,fmt,##__VA_ARGS__)
#define commerceFatalAssertf(cond,fmt,...)    RAGE_FATALASSERTF(commerce,cond,fmt,##__VA_ARGS__)
#define commerceVerify(cond)                  RAGE_VERIFY(commerce,cond)
#define commerceVerifyf(cond,fmt,...)         RAGE_VERIFYF(commerce,cond,fmt,##__VA_ARGS__)
#define commerceErrorf(fmt,...)               RAGE_ERRORF(commerce,fmt,##__VA_ARGS__)
#define commerceWarningf(fmt,...)             RAGE_WARNINGF(commerce,fmt,##__VA_ARGS__)
#define commerceDisplayf(fmt,...)             RAGE_DISPLAYF(commerce,fmt,##__VA_ARGS__)
#define commerceDebugf1(fmt,...)              RAGE_DEBUGF1(commerce,fmt,##__VA_ARGS__)
#define commerceDebugf2(fmt,...)              RAGE_DEBUGF2(commerce,fmt,##__VA_ARGS__)
#define commerceDebugf3(fmt,...)              RAGE_DEBUGF3(commerce,fmt,##__VA_ARGS__)
#define commerceLogf(severity,fmt,...)        RAGE_LOGF(commerce,severity,fmt,##__VA_ARGS__)

#endif // FWCOMMERCE_COMMERCE_CHANNEL_H