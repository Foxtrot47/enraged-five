#ifndef COMMERCEUTIL_H
#define COMMERCEUTIL_H

#include "atl/string.h"

#include "CommerceData.h"

typedef bool (*isCommerceProductinstalledCallbackPtr)(const rage::cCommerceProductData *productData);

namespace rage {

const s32 COMMERCE_ALLOCATOR_ALIGNMENT = 32;

class sysMemAllocator;

class cCommerceUtil
{
public:
	static cCommerceUtil* Instance();

	virtual ~cCommerceUtil();

    virtual bool Init( int userIndex = 0, const char *skuFolder = NULL );
    virtual bool PlatformInit( int /*userIndex = 0*/, const char* /*skuFolder = NULL*/ ) { return true; }
    virtual void Shutdown();

    virtual void Update();

    virtual bool IsContentInfoPopulated() const;

    bool IsCategoryInfoPopulated() const { return m_CategoryDataPopulated; }
    
    virtual bool IsInDataFetchState() const { return IsPlatformInDataFetchState() || IsRosInDataFetchState() || m_PendingPlatformDataRequest; }
    virtual bool IsPlatformInDataFetchState() const { return false; }
    virtual bool IsRosInDataFetchState() const { return m_CommerceData.IsInDataFetchState(); }

    virtual void AbortDataFetch() { m_CommerceData.AbortDataFetch(); }
	virtual bool IsAbortInProgress() { return false; } 
    virtual bool IsInErrorState() const { return m_CommerceData.IsInErrorState(); }
    virtual void ForceErrorState() {}

	void SuppressItemId(const char* idToSuppress) { m_CommerceData.SuppressItemId(idToSuppress); } 
	void SuppressTag(const char* tagToSuppress) { m_CommerceData.SuppressTag(tagToSuppress); } 

	void SetSkuDir(const char* skuDir) { m_CommerceData.SetSkuDir(skuDir); }

	void AddSubscriptionId(const char* subscriptionId) { m_CommerceData.AddSubscriptionId(subscriptionId); }
	bool HasSubscriptionId(const char* subscriptionId) const { return m_CommerceData.HasSubscriptionId(subscriptionId); }
	bool HasSubscriptionDependency(const cCommerceProductData* itemToCheck) const { return m_CommerceData.HasSubscriptionDependency(itemToCheck); }
	bool HasMissingSubscriptionDependencies(const cCommerceProductData* itemToCheck) const { return m_CommerceData.HasMissingSubscriptionDependencies(itemToCheck); }
	bool HasSubscriptionExclusion(const cCommerceProductData* itemToCheck) const { return m_CommerceData.HasSubscriptionExclusion(itemToCheck); }
	bool HasActiveSubscriptionExclusions(const cCommerceProductData* itemToCheck) const { return m_CommerceData.HasActiveSubscriptionExclusions(itemToCheck); }
	bool HasSubscriptionUpsell(const cCommerceProductData* itemToCheck) const { return m_CommerceData.HasSubscriptionUpsell(itemToCheck); }
	bool HasMissingSubscriptionUpsells(const cCommerceProductData* itemToCheck) const { return m_CommerceData.HasMissingSubscriptionUpsells(itemToCheck); }

    void SetIsInstalledCallback( isCommerceProductinstalledCallbackPtr a_CallbackFuncPtr  );

    void SetSortingTag(const char* newTag); 

    void SetAllocator( sysMemAllocator *allocator ) { m_pCommerceAllocator = allocator; }

    //**********************************
    //Rockstar data functions (blue box)
    //**********************************
 
    //This function starts the process of downloading and parsing the ROS side commerce data (this is done in the cCommerceData class)
    void DoDataRequest( bool fetchROSData = true );

    virtual const char *GetItemName( unsigned a_Index ) const;    
    virtual const char *GetItemDescription( unsigned a_Index ) const;
    //Note: GetProductLongDescription is not const as it may start a data fetch if the long name is not populated
    //virtual const char *GetItemLongDescription( unsigned a_Index );
    virtual const char *GetItemId( unsigned a_Index ) const;
	
    virtual const char *GetItemLongDescription( const cCommerceProductData* product );

    virtual const cCommerceItemData* GetItemData( unsigned a_Index ) const;
    virtual const cCommerceItemData* GetItemData( const char* itemName, cCommerceItemData::eItemType itemType = cCommerceItemData::ITEM_TYPE_NUM_TYPES ) const;
	virtual const cCommerceItemData* GetItemDataByItemId(const char* itemId) const;

    virtual const int GetNumItems() const;

    //Takes a product and an array, fills the array with IDs of produts which contain it. Returns the number of products containing the input product 
    //(should alway be 1 or greater if the product is valid, as the array will have the products own ID in it)
    int PopulateArrayWithIdsWhichIncludeThisProduct( const cCommerceProductData * a_Product,atArray<atString> &a_IdArray, bool a_includeVirtuals = false );
    int GetNumContainingProducts( const cCommerceProductData * a_Product, bool a_includeVirtuals = false );

    //**********************************
    //Platform data functions (red box)
    //**********************************

    //This function starts the process of downloading and parsing the platform side commerce data 
    //(this is done in the platform specific derived class of cCommerceUtil, ie cXboxCommerceUtil)

    virtual bool IsPlatformReady() const;

    virtual bool DoPlatformDataRequest() { return true; }

    virtual void DoPlatformLevelDebugDump();

    virtual bool IsInCheckout() const { return false; }
	virtual bool IsErrorEncountered() const { return false; }
	virtual bool IsInRedemption() const { return false; }

    virtual bool IsProductPurchased( int /*a_Index*/ ) const { return false; }
    virtual bool IsProductPurchased( const cCommerceProductData* ) const { return false; }

    virtual bool IsProductPurchasable( int /*a_Index*/ ) const { return false; }
    virtual bool IsProductPurchasable( const cCommerceProductData* ) const { return false; }

    virtual float GetProductPrice( unsigned /*a_Index*/, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const { return 0.0f; }
    virtual float GetProductPrice( const cCommerceProductData*, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const { return 0.0f; }

    //virtual bool IsProductInstalled( int /*a_Index*/ ) const { return false; }
    virtual bool IsProductInstalled( const cCommerceProductData* ) const;

    virtual bool IsProductReleased( int /*a_Index*/ ) const { return false; }
    virtual bool IsProductReleased( const cCommerceProductData* ) const { return false; }

	virtual bool HasProductDetails( const cCommerceProductData*) const;

	virtual bool ShouldAllowWithoutProduct( const cCommerceProductData* ) const;
    virtual bool ShouldHideProduct( const cCommerceProductData* ) const;

    //Platform side operations
    virtual void CheckoutProduct( unsigned /*a_Index*/ ) {}
    virtual void CheckoutProduct( const cCommerceProductData* ) {}

#if __WIN32PC
    //This is platform level package end entitlement code, we should move this out of the commerce manager ASAP when updating PC commerce.
	virtual void DownloadProduct( const cCommerceProductData* ) {}
	virtual bool IsDownloadingProduct( const cCommerceProductData* ) { return false; }

	virtual bool		ContentNeedsActivation() const { return false; }
	virtual void		SetContentNeedsActivation(bool /*needsActivation*/) {}
	virtual const char* GetCurrentActivationCode() const { return NULL; }
	virtual void		SetActivationComplete(bool /*success*/, long /*resultCode*/) {}

	virtual void		SetGameshieldProjectID(int /*lpjID*/) {}
#endif

    virtual bool ProductCodeEntry() { return false; }

    virtual void SetLineBreakString( const atString&  ) {}

    virtual bool HasWhitelistingFailed() { return m_WhitelistingFailed; }   

    virtual bool DidLastCheckoutSucceed() { return m_DidLastCheckoutSucceed; }

    //******************************************
    // Functions using both R* and Platform data
    //******************************************

    //Purpose: This functions combines the R* data with platform data to create set of category objects. These objects contain arrays of indices which can be passed into the data retrieval
    //functions of cCommerceUtil.
    void PopulateCategories();

	// Error Codes
	enum eCommerceErrorCode
	{
		CERROR_NONE					= 0,
		CERROR_ACTIVATION			= 100, // PC/Arvado
		CERROR_ACT_CANCELLED		= 101,
		CERROR_MALFORMED			= 102,
		CERROR_DURING_CHECKOUT1		= 103,
		CERROR_DURING_CHECKOUT2		= 104,
		CERROR_DURING_CHECKOUT3		= 105,
		CERROR_VERIFY_CONTENT		= 106,
		CERROR_ENTITLEMENT			= 107,
		CERROR_REDEMPTION			= 108,
		CERROR_CONTACT_ARVADO		= 109,
		CERROR_STEAM_INITIALIZE		= 200, // Steam
		CERROR_STEAM_ACTIVATION		= 201,
		CERROR_STEAM_ACT_CANCELLED	= 202,
		CERROR_STEAM_MALFORMED		= 203,
		CERROR_STEAM_REDEMPTION		= 204,
		CERROR_STEAM_ENTITLEMENT	= 205
	};

	eCommerceErrorCode GetErrorCode() { return m_ErrorCode; }

    //In case the commerce solution needs a parent product or category identifier, this is where we pass it.
    virtual void SetRootProductIdentifier( const char* /*rootProductIdentifier*/ ) {}

protected:

    cCommerceUtil();

    //Place holder mem functions for porting
    void* CommerceAlloc( unsigned );
    void CommerceFree( void* );

    isCommerceProductinstalledCallbackPtr GetProductInstalledCB() const { return m_IsProductInstalledCBPtr; } 

    virtual void MergePlatformAndRosData() {}

protected:
    //New cCommerceData class for fetching and storing R* hosted commerce information.
    cCommerceData m_CommerceData;
	eCommerceErrorCode m_ErrorCode;
    bool m_WhitelistingFailed;
    bool m_DidLastCheckoutSucceed;

private:     

    //Static singleton instance of this class
    static cCommerceUtil* m_BaseInstance;

    enum eBaseCommerceState
    {
        BASE_STATE_INITIAL,
        BASE_STATE_PENDING_DATA,
        BASE_STATE_ERROR,
        BASE_STATE_POPULATED,
        BASE_STATE_NUM
    } m_BaseState;

    bool m_CategoryDataPopulated;
    bool m_PendingPlatformDataRequest;

    sysMemAllocator *m_pCommerceAllocator;

    isCommerceProductinstalledCallbackPtr m_IsProductInstalledCBPtr;  

    int PopulateIncludesArrayInternal( const cCommerceProductData * a_Product,atArray<atString> &a_IdArray, bool a_includeVirtuals = false, int depth = 0 );
    void SortCategory( cCommerceCategoryData* pCategory );

    atString m_SortingTag;
};

cCommerceUtil* CommerceUtilInstance( bool forceBase = false );


} //namespace Rage

#endif