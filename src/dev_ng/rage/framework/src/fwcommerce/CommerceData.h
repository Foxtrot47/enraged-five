#ifndef COMMERCEDATA_H
#define COMMERCEDATA_H

// Rage includes

#include "atl/map.h"
#include "atl/string.h"
#include "data/growbuffer.h"
#include "data/sax.h"
#include "net/status.h"

//The commerce data class is used to retrieve and store the Rockstar side commerce data (as opposed to sony/ms), both in terms of products and categories.

namespace rage
{
	//FWD Declare
	class fiStream;



//Simple class to store strings for multilanguage commerce data.
class cCommerceStringTable
{
public:
    cCommerceStringTable();
    virtual ~cCommerceStringTable() {}

    void Init();
    void Shutdown();

    bool IsStringPresent( const atString a_StringCode ) const;
    const atString& GetString( const atString a_StringCode ) const;
    
    //Function to add string. Returns true on successful add, false on fail (string already exists?)
    bool AddString( const atString a_StringCode, const atString a_String );

private:

    void DumpStringTableInfo();

    atMap<atString,atString> m_StringMap;

    //String to be returned on error. Defaults to empty, can be chaged to suit requirements.
    atString m_ErrorString;
};

//Structure for storing product data.
// m_ProductId: Unique identifier for this product
// m_Name: Name of product
// m_ShortDesc: Short description of product
// m_LongDesc: Long description of product. WARNING: Must be obtained from Sony platform on PS3 in order to be TRC compliant.
// m_PlatformProductId: Platform side identifier for this product, used to link to the actual product on the Sony/MS/PC side. 
//						Example: OfferId on Xbox, Product identifier on PS3. Essential for an actual product, non essential for a virtual of "coming soon: product
// 
// m_DependencyId: Platform side identifier of a dependency for this product. The product should not be displayed UNLESS the dependency is owned.
// m_ExclusionId: Platform side identifier of an exclusion for this product. The product should not be displayed IF the dependency is owned.
// m_CategoryMembership: The identifier of a category that thisd 
// m_ReleaseTime: Posix release time.


//TODO: Switch these over to classes with a common base class of cCommerceItemData


 
class cCommerceItemData
{
public:
    cCommerceItemData();
    virtual ~cCommerceItemData() {}

    virtual void Init();

    virtual void ResetEnumerationFlags();

    enum eItemType
    {
        ITEM_TYPE_PRODUCT,
        ITEM_TYPE_CATEGORY,
        ITEM_TYPE_NUM_TYPES
    };

    virtual eItemType GetItemType() const = 0;

    atString &GetId() { return m_Id; }
    atString &GetName() { return m_Name; }
    atString &GetDescription() { return m_Description; }
    atString &GetLongDescription() { return m_LongDescription; }

    const atString &GetId() const { return m_Id; }
    const atString &GetName() const { return m_Name; }
    const atString &GetDescription() const { return m_Description; }
    const atString &GetLongDescription() const { return m_LongDescription; }

    atArray<atString> &GetCategoryMemberships() { return m_CategoryMemberships; }
    const atArray<atString> &GetCategoryMemberships() const { return m_CategoryMemberships; }

    atArray<atString> &GetImagePaths() { return m_ImagePaths; }
    const atArray<atString> &GetImagePaths() const { return m_ImagePaths; }

	const int GetImageTemplate() const {return m_ImageTemplate; }
	void SetImageTemplate(int inTemplate) { m_ImageTemplate = inTemplate; }

    void SetDependencyFlag( bool dependencyFlag ) { m_DependencyEnumerated = dependencyFlag; }
    const bool GetDependencyFlag() const { return m_DependencyEnumerated; }

    void SetEnumDependencyFlag( bool enumDependencyFlag ) { m_EnumDependencyEnumerated = enumDependencyFlag; }
    const bool GetEnumDependencyFlag() const { return m_EnumDependencyEnumerated; }

    void SetExclusionFlag( bool exclusionFlag ) { m_ExclusionEnumerated = exclusionFlag; }
    const bool GetExclusionFlag() const { return m_ExclusionEnumerated; }

    void SetEnumExclusionFlag( bool enumExclusionFlag ) { m_EnumExclusionEnumerated = enumExclusionFlag; }
    const bool GetEnumExclusionFlag() const { return m_EnumExclusionEnumerated; }

    atArray<atString> &GetDependencyIds() { return m_DependencyIds; }
    const atArray<atString> &GetDependencyIds() const { return m_DependencyIds; }

    atArray<atString> &GetEnumDependencyIds() { return m_EnumDependencyIds; }
    const atArray<atString> &GetEnumDependencyIds() const { return m_EnumDependencyIds; }

    atArray<atString> &GetExclusionIds() { return m_ExclusionIds; }
    const atArray<atString> &GetExclusionIds() const { return m_ExclusionIds; }

    atArray<atString> &GetEnumExclusionIds() { return m_EnumExclusionIds; }
    const atArray<atString> &GetEnumExclusionIds() const { return m_EnumExclusionIds; }

    void AddPriorityTag(const char* tagString, int priority);
    int GetPriorityForTag(const char* tagString) const;
	int HasEntryForTag( const char* tagString ) const;
protected:
    bool m_Initialised;
private:

    atString m_Id;
    atString m_Name;
    atString m_Description;
    atString m_LongDescription;

    bool                m_DependencyEnumerated;
    bool                m_EnumDependencyEnumerated;
    bool                m_EnumExclusionEnumerated;
    bool                m_ExclusionEnumerated;

    atArray<atString>	m_CategoryMemberships;
    atArray<atString>   m_ImagePaths;
	int					m_ImageTemplate;

    atArray<atString>	m_DependencyIds;
    atArray<atString>   m_EnumDependencyIds;
    atArray<atString>   m_EnumExclusionIds;
    atArray<atString>	m_ExclusionIds;

    atMap<atString,int> m_PriorityTagMap;
};

class cCommerceCategoryData : public cCommerceItemData
{
public:
    eItemType GetItemType() const { return ITEM_TYPE_CATEGORY; }

    int GetNumProducts() const { return m_ItemIndexArray.GetCount(); }
    int GetItemIndex( int a_CatProdIndex ) const;

    void SetItemArraySize( int a_NewProdArraySize );
    void AddItemIndex( int a_NewIndex );

    //Access for sorting. Will go away next refactor.
    atArray<int> &GetItemIndexArray() { return m_ItemIndexArray; }

private:
    atArray<int> m_ItemIndexArray;
};

class cCommerceProductData : public cCommerceItemData
{
public:
    cCommerceProductData();

    void Init();
    void ResetEnumerationFlags();

    eItemType GetItemType() const { return ITEM_TYPE_PRODUCT; }

    atString &GetPlatformId() { return m_PlatformProductId; }
    const atString &GetPlatformId() const { return m_PlatformProductId; }

    atArray<atString> &GetIncludedInProductIds() { return m_IncludedInProductIds; }
    const atArray<atString> &GetIncludedInProductIds() const { return m_IncludedInProductIds; }

    atArray<atString> &GetContainedPackageNames() { return m_ContainedPackageNames; }
    const atArray<atString> &GetContainedPackageNames() const { return m_ContainedPackageNames; }

	atArray<atString> &GetSubscriptionDependencies() { return m_SubscriptionDependencies; }
	const atArray<atString> &GetSubscriptionDependencies() const { return m_SubscriptionDependencies; }

	atArray<atString> &GetSubscriptionExclusions() { return m_SubscriptionExclusions; }
	const atArray<atString>& GetSubscriptionExclusions() const { return m_SubscriptionExclusions; }

    atArray<atString>& GetSubscriptionUpsells() { return m_SubscriptionUpsells; }
	const atArray<atString>& GetSubscriptionUpsells() const { return m_SubscriptionUpsells; }

    u64 GetReleaseTime() const { return m_ReleaseTime; }
    void SetReleaseTime( u64 releaseTime ) { m_ReleaseTime = releaseTime; }

    void SetEnumeratedFlag( bool enumeratedFlag ) { m_ProductEnumerated = enumeratedFlag; }
    const bool GetEnumeratedFlag() const { return m_ProductEnumerated; }

    void SetDisplayVirtualFlag( bool displayVirtualFlag ) { m_DisplayWhenVirtual = displayVirtualFlag; }
    bool IsVirtualProduct() const { return m_DisplayWhenVirtual; }

    void SetVirtualIfInstalledFlag( bool virtualIfInstalled ) { m_VirtualIfInstalled = virtualIfInstalled; }
    bool IsVirtualIfInstalled() const { return m_VirtualIfInstalled; }

    atString &GetConsumableId() { return m_ConsumableId; }
    const atString &GetConsumableId() const { return m_ConsumableId; }
    bool IsConsumable() const { return m_ConsumableId.GetLength() > 0; }
    
	bool IsComingSoon() const { return m_bComingSoon; }
	bool SetIsComingSoon(bool bComingSoon) { return m_bComingSoon = bComingSoon; }

#if __WIN32PC
	void SetIsDownloading( bool downloading ) { m_bDownloading = downloading; }
	bool IsDownloading() const { return m_bDownloading; }
#endif
private:

    atString			m_PlatformProductId;
    atString            m_ConsumableId;
    
    atArray<atString>   m_IncludedInProductIds;
	atArray<atString>   m_ContainedPackageNames;
	atArray<atString>   m_SubscriptionDependencies;
	atArray<atString>   m_SubscriptionExclusions;
	atArray<atString>   m_SubscriptionUpsells;
    u64					m_ReleaseTime;

    bool                m_ProductEnumerated;

    bool                m_DisplayWhenVirtual;
	bool				m_bComingSoon;
    bool                m_VirtualIfInstalled;
    
#if __WIN32PC
	bool				m_bDownloading;
#endif
};

class cCommerceData
{	
public:
	cCommerceData();
	~cCommerceData();

	void Init( int a_LocalGamerIndex, const char* skuFolder );
	void Shutdown();

	//Purpose: Updates the commerce data retrieval, in particular checking the time to refresh the cloud device when retrieving the commerce data XML.
	void Update();

	//Purpose: Check to see if data has been fetched from the cloud system and successfully parsed at least once.
	bool HasValidData() const { return m_HasValidData; }

    bool IsInErrorState() const { return m_IsInErrorState; }

	//Purpose: Start a fetch of the commerce date stored on the cloud system followed by a parse. 
	//Returns: true if the class was in a state to start a new data fetch, false otherwise.
	bool StartDataFetch();

    cCommerceItemData *GetItemData( int a_Index );
    const cCommerceItemData *GetItemData( int a_Index ) const;
	const cCommerceItemData* GetItemData( const char* itemName, cCommerceItemData::eItemType itemType = cCommerceItemData::ITEM_TYPE_NUM_TYPES ) const;
	const cCommerceItemData* GetItemDataByItemId( const char* itemId ) const;
    const int GetSizeOfDataArray() const;

    const bool IsInDataFetchState() const;
    void AbortDataFetch();

	void SuppressItemId( const char * idToSuppress );

	void SuppressTag( const char * tagToSuppress );

	void SetSkuDir( const char* newSkuDirString ) { m_SkuDirString = newSkuDirString; }

	void AddSubscriptionId(const char* subscriptionId);
	bool HasSubscriptionId(const char* subscriptionId) const;
	bool HasSubscriptionDependency(const cCommerceProductData* itemToCheck) const;
	bool HasMissingSubscriptionDependencies(const cCommerceProductData* itemToCheck) const;
	bool HasSubscriptionExclusion(const cCommerceProductData* itemToCheck) const;
	bool HasActiveSubscriptionExclusions(const cCommerceProductData* itemToCheck) const;
	bool HasSubscriptionUpsell(const cCommerceProductData* itemToCheck) const;
	bool HasMissingSubscriptionUpsells(const cCommerceProductData* itemToCheck) const;

	//Commerce Data constants
	enum
	{
		CATEGORY_ID_MAX_LEN = 255,
        IMAGE_PATH_MAX_LEN = 512,
		PRODUCT_ID_MAX_LEN = 255,
		PLATFORM_PRODUCT_ID_MAX_LEN = 255,
		SUBSCRIPTION_ID_MAX_LEN = 255,
        INVALID_PRIORITY_TAG_VALUE = -1
	};

protected:
	class cCommerceSaxReader : public datSaxReader
	{
	public:
		cCommerceSaxReader(): datSaxReader()
			, m_State(STATE_EXPECT_ROOT)
			, m_pCommercedata(NULL)
            , m_pCurrentItemData(NULL)
            , m_LastPriorityTagValue(INVALID_PRIORITY_TAG_VALUE)
		{
			ResetCurrentData();
		}

		void Start(cCommerceData* pCommerceData);

	private:
		virtual void startElement(const char* url, const char* localName, const char* qName);
		virtual void endElement(const char* url, const char* localName, const char* qName);
		virtual void attribute(const char* tagName, const char* attrName, const char* attrVal);
		virtual void characters(const char* ch, const int start, const int length);

		void startProductSubElement( const char* url, const char* localName, const char* qName );
		void startCategorySubElement( const char* url, const char* localName, const char* qName );

		void ResetCurrentData();

		enum State
		{
			STATE_EXPECT_ROOT,
			STATE_EXPECT_PRODS_OR_CATS_TAG,
			STATE_EXPECT_PRODUCT,
			STATE_EXPECT_PRODUCT_SUB_ELEMENT,
			STATE_READING_PRODNAME ,
			STATE_READING_SHORTDESC ,
			STATE_READING_LONGDESC ,
			
			STATE_EXPECT_DEPENDENCY,
			STATE_READING_DEPENDENCY,

			STATE_EXPECT_EXCLUSION,
			STATE_READING_EXCLUSION,

            STATE_EXPECT_ENUM_DEPENDENCY,
            STATE_READING_ENUM_DEPENDENCY,

            STATE_EXPECT_ENUM_EXCLUSION,
            STATE_READING_ENUM_EXCLUSION,

            STATE_EXPECT_INCLUDEDINID,
            STATE_READING_INCLUDEDINID,

			STATE_EXPECT_CATEGORYMEMBERSHIP,
			STATE_READING_CATEGORYMEMBERSHIP,

            STATE_EXPECT_PRIORITY_TAG,
			STATE_READING_PRIORITY_TAG,

			STATE_EXPECT_SUBSCRIPTION_DEPENDENCY,
			STATE_READING_SUBSCRIPTION_DEPENDENCY,

			STATE_EXPECT_SUBSCRIPTION_EXCLUSION,
			STATE_READING_SUBSCRIPTION_EXCLUSION,

			STATE_EXPECT_SUBSCRIPTION_UPSELL,
			STATE_READING_SUBSCRIPTION_UPSELL,

            STATE_EXPECT_IMAGEPATH,
            STATE_READING_IMAGEPATH,

            STATE_EXPECT_CONTAINED_PACKAGE_NAME,
            STATE_READING_CONTAINED_PACKAGE_NAME,

			STATE_EXPECT_CATEGORY,
			STATE_EXPECT_CATEGORY_SUB_ELEMENT,
			STATE_READING_CATNAME,
			STATE_READING_CATDESC,

            STATE_EXPECT_STRING,
            STATE_READING_STRING
		} m_State;

		cCommerceData *m_pCommercedata;

        cCommerceItemData *m_pCurrentItemData;

        //Holding variables for populating priority tags
        int m_LastPriorityTagValue;
        atString m_LastPriorityTagString;
	};

private:

    bool m_bInitialised;

	void ResetData();

	bool IsItemInSuppressList( const cCommerceItemData* itemToCheck ) const;

    //Parser state functions
	void UpdateParserWaitingState();
	void UpdateParserStartupState();
	void UpdateParserReceivingState();
	void UpdateParserCleanupState();
	void UpdateParserLocalFileState();

	enum eParserState
	{
		COMMERCE_XML_WAITING,
		COMMERCE_XML_STARTUP,
		COMMERCE_XML_RECEIVING,
		COMMERCE_XML_LOCAL_FILE,
		COMMERCE_XML_CLEANUP,
		COMMERCE_XML_NUM_STATES
	};

	eParserState m_ParserState;

	int m_LocalGamerIndex;

    //Newly made data structure.
    atArray<cCommerceItemData*> m_pCommerceItemDataArray;

	bool m_NeedToRefreshXMLFromCloud;

	//Buffer which is used in the XML retrieval process.
	datGrowBuffer m_GrowBuffer;

	netStatus m_CommerceXMLFetchStatus;

	cCommerceSaxReader m_saxReader;

	fiStream* m_pLocalFileStream;

    atString m_SkuDirString;

	bool m_HasValidData;
    bool m_IsInErrorState;

	atArray<atString> m_IdsToSuppress;
	atArray<atString> m_TagsToSuppress;
	atArray<atString> m_SubscriptionIds;

    //String table for localised string lookups.
    cCommerceStringTable m_StringTable;

    int m_CredentialFailCount;
};

}

#endif//COMMERCEDATA_H