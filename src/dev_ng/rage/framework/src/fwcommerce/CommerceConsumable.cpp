#include "CommerceConsumable.h"

#include "CommerceChannel.h"
#include "Durango/DurangoCommerceConsumable.h"
#include "Orbis/OrbisCommerceConsumable.h"
#include "PS3/PS3CommerceConsumable.h"
#include "SCS/SCSCommerceConsumable.h"
#include "Xbox360/XboxCommerceConsumable.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

const int INITIAL_SIZE_OF_TRANSACTION_RESULT_MAP = 29;


consumableTransactionId cCommerceConsumableTransaction::m_NextTransactionId = 1;

cCommerceConsumableManager* cCommerceConsumableManager::mp_BaseInstance = NULL;

cCommerceConsumableTransaction::cCommerceConsumableTransaction() :
    m_Id(TRANSACTION_ID_UNASSIGNED),
    m_CurrentConsumableUser(-1)
{

}

cCommerceConsumableTransaction::~cCommerceConsumableTransaction()
{
    Shutdown();
}

void cCommerceConsumableTransaction::Init()
{
    m_Id = TRANSACTION_ID_UNASSIGNED;
}

void cCommerceConsumableTransaction::Shutdown()
{

}

void cCommerceConsumableTransaction::Update()
{

}

void cCommerceConsumableTransaction::Reset()
{
    m_Id = TRANSACTION_ID_UNASSIGNED;
}

bool cCommerceConsumableTransaction::HasCompleted() const
{
    return false;
}

bool cCommerceConsumableTransaction::HasSucceeded() const
{
    return false;
}

bool cCommerceConsumableTransaction::StartTransaction( const char* /*consumableIdentifier*/, int /*amountToConsume*/ )
{
    //Sophisticated, I know.
    m_Id = m_NextTransactionId++;

    return true;
}


/******************************************************************************************************************************/
//Consumables manager
/******************************************************************************************************************************/

cCommerceConsumableManager* cCommerceConsumableManager::Instance() 
{
    if ( mp_BaseInstance == NULL )
    {
        mp_BaseInstance = rage_new cCommerceConsumableManager;
    }

    return mp_BaseInstance;
}


cCommerceConsumableManager::cCommerceConsumableManager() :
    mp_CurrentTransaction(NULL),
    m_CurrentConsumableUser(-1),
    m_CurrentOwnershipDataDirty(true)
{
    m_TransactionResultMap.Create(INITIAL_SIZE_OF_TRANSACTION_RESULT_MAP);
}

cCommerceConsumableManager::~cCommerceConsumableManager()
{
    if ( mp_CurrentTransaction )
    {
        delete mp_CurrentTransaction;
    }
}

bool cCommerceConsumableManager::PreconsumeChecks()
{
    if ( mp_CurrentTransaction == NULL )
    {
        return false;
    }

    //Check if there is a transaction available in the pool
    if ( mp_CurrentTransaction->GetId() != TRANSACTION_ID_UNASSIGNED )
    {
        AssertMsg(false,"No consumable transactions available. Something is probably spamming new requests rather than checking for previous operations completion");
        return false;
    }

    commerceAssertf(GetCurrentConsumableUser() != -1, "Invalid current user specified when cCommerceConsumableManager::Consume called.");

    if (GetCurrentConsumableUser() == -1)
    {
        return false;   
    }

    return true;
}

consumableTransactionId cCommerceConsumableManager::SetupTransaction(const char* consumableIdentifier, int amountToConsume)
{
    mp_CurrentTransaction->SetCurrentConsumableUser(GetCurrentConsumableUser());
    mp_CurrentTransaction->StartTransaction(consumableIdentifier,amountToConsume);
    return mp_CurrentTransaction->GetId(); 
}

consumableTransactionId cCommerceConsumableManager::Consume( const char* consumableIdentifier, int amountToConsume )
{
    if (!PreconsumeChecks())
    {
        return TRANSACTION_ID_ERROR;
    }

    return SetupTransaction(consumableIdentifier,amountToConsume);
}

void cCommerceConsumableManager::Init()
{

}

void cCommerceConsumableManager::Update()
{
	if ( mp_CurrentTransaction != NULL )
	{
		if ( mp_CurrentTransaction->GetId() != TRANSACTION_ID_UNASSIGNED )
		{
			mp_CurrentTransaction->Update();
		}

		//Check for completion and store the result.
		if ( mp_CurrentTransaction->GetId() != TRANSACTION_ID_UNASSIGNED && mp_CurrentTransaction->HasCompleted() )
		{
			bool *result = m_TransactionResultMap.Access( mp_CurrentTransaction->GetId() );
			if ( result == NULL )
			{
				m_TransactionResultMap.Insert( mp_CurrentTransaction->GetId(), mp_CurrentTransaction->HasSucceeded() );
			}
			else
			{
				*result = mp_CurrentTransaction->HasSucceeded(); 
			}

			mp_CurrentTransaction->Reset();
		}
	}

	UpdateEntitlementDataRefresh();
}

bool cCommerceConsumableManager::HasTransactionCompleted( consumableTransactionId transactionId ) const
{
    return ( m_TransactionResultMap.Access( transactionId ) != NULL );
}

bool cCommerceConsumableManager::WasTransactionSuccessful( consumableTransactionId transactionId ) const
{
    const bool *result = m_TransactionResultMap.Access( transactionId );

    if ( result == NULL )
    {
        //No transaction result stored probably hasn't completed yet.
        return false;
    }

    return *result;
}

void cCommerceConsumableManager::DumpCurrentOwnershipData() const
{
    commerceDebugf1("-------------------------------");
    commerceDebugf1("DUMPING CURRENT OWNERSHIP DATA");
    commerceDebugf1("-------------------------------");
    commerceDebugf1("*No ownership data to dump, this is a base implementation*");
}

void cCommerceConsumableManager::SetOwnershipDataDirty()
{
	if(!m_CurrentOwnershipDataDirty)
	{
		commerceDebugf1("SetOwnershipDataDirty");
		m_CurrentOwnershipDataDirty = true;
	}
}

void cCommerceConsumableManager::UpdateEntitlementDataRefresh()
{
	if ( GetCurrentConsumableUser() == -1 )
	{
		//We have an unknown user. Next time we have a known user, we will want to update the consumable info.
		m_CurrentOwnershipDataDirty = true;
		return;
	}
	
	if ( !m_CurrentOwnershipDataDirty )
    {
        //No need to refresh
        return;
    }

    StartOwnershipDataFetch();
    m_CurrentOwnershipDataDirty = false;
}

cCommerceConsumableManager* CommerceConsumablesInstance( bool forceBase )
{
    if ( forceBase )
    {
        return cCommerceConsumableManager::Instance();
    }

#if __XENON
	return cXboxCommerceConsumableManager::Instance();
#elif __PS3
	return cPS3CommerceConsumableManager::Instance();
#elif RSG_ORBIS
	return cOrbisCommerceConsumableManager::Instance();
#elif RSG_DURANGO
	return cDurangoCommerceConsumableManager::Instance();
#elif RSG_PC
	return cSCSCommerceConsumableManager::Instance();
#else
    return cCommerceConsumableManager::Instance();
#endif
}

} //namespace rage