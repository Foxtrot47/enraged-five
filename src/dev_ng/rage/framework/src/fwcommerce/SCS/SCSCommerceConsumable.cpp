#include "SCSCommerceConsumable.h"
#include "../CommerceChannel.h"
#include "net/task.h"
#include "rline/rltask.h"

#if RSG_PC
namespace rage
{

cSCSCommerceConsumableManager *cSCSCommerceConsumableManager::mp_Instance = NULL;

cCommerceConsumableManager* rage::cSCSCommerceConsumableManager::Instance()
{
	if ( mp_Instance == NULL )
	{
		mp_Instance = rage_new cSCSCommerceConsumableManager;
	}

	return mp_Instance;
}


void cSCSCommerceConsumableManager::Init()
{
	m_CurrentEntitlementDataState = ENTITLEMENT_DATA_STATE_UNINITIALISED;

    m_EntitlementResponse.Reset();
	m_EntitlementDataFetchStatus.Reset();

    m_ConsumeCallback = NULL;
    m_ConsumeCompleteCallback = NULL;

	cCommerceConsumableManager::Init();
}

void cSCSCommerceConsumableManager::Shutdown()
{
	cCommerceConsumableManager::Shutdown();
}

cSCSCommerceConsumableManager::cSCSCommerceConsumableManager() :
    m_ConsumeCallback(NULL)
{
    mp_CurrentTransaction = rage_new cScsCommerceConsumableTransaction;
}

cSCSCommerceConsumableManager::~cSCSCommerceConsumableManager()
{
	Shutdown();

    delete mp_CurrentTransaction;
    mp_CurrentTransaction = NULL;
}

bool cSCSCommerceConsumableManager::StartOwnershipDataFetch()
{
    if (m_EntitlementDataFetchStatus.Pending())
    {
        return false;
    }

    m_EntitlementResponse.Reset();
#if !PRELOADED_SOCIAL_CLUB
    if (!rlRosEntitlement::GetEntitlements(GetCurrentConsumableUser(), "en", "UNUSED", &m_EntitlementResponse, &m_EntitlementDataFetchStatus ))
    {
        m_CurrentEntitlementDataState = ENTITLEMENT_DATA_STATE_ERROR;
        return false;
    }
    else
#endif
    {
        m_CurrentEntitlementDataState = ENTITLEMENT_DATA_FETCHING_DATA;
        return true;
    }
}

void cSCSCommerceConsumableManager::UpdateEntitlementDataFetch()
{
    if (m_EntitlementDataFetchStatus.Pending())
    {
        return;
    }

    if (m_EntitlementDataFetchStatus.Succeeded())
    {
        m_CurrentEntitlementDataState = ENTITLEMENT_DATA_STATE_POPULATED;
    }

    if (m_EntitlementDataFetchStatus.Failed())
    {
        m_CurrentEntitlementDataState = ENTITLEMENT_DATA_STATE_ERROR;
    }

    m_EntitlementDataFetchStatus.Reset();
}

void cSCSCommerceConsumableManager::Update()
{
    switch(m_CurrentEntitlementDataState)
    {
    case(ENTITLEMENT_DATA_FETCHING_DATA):
        UpdateEntitlementDataFetch();
        break;
    default:
        break;
    }

    cCommerceConsumableManager::Update();
}

consumableTransactionId cSCSCommerceConsumableManager::SetupTransaction( const char* consumableIdentifier, int amountToConsume )
{
    //Add the instance Ids to the transaction
    if (GetConsumableLevel(consumableIdentifier) < amountToConsume)
    {
        return TRANSACTION_ID_ERROR;
    }

    int amountRequired = amountToConsume;

    for (int s = 0; s < m_EntitlementResponse.m_EntitlementInstanceArray.GetCount() && amountRequired > 0; s++)
    {
        if (strcmp(m_EntitlementResponse.m_EntitlementInstanceArray[s].m_Code.c_str(), consumableIdentifier) == 0)
        {
            //This is the correct entitlement.
            int numToConsumeFromThisInstance = rage::Min(m_EntitlementResponse.m_EntitlementInstanceArray[s].m_Count, amountRequired);

            mp_CurrentTransaction->AddConsumeInstance(consumableIdentifier, m_EntitlementResponse.m_EntitlementInstanceArray[s].m_InstanceId ,numToConsumeFromThisInstance);
            amountRequired -= numToConsumeFromThisInstance;
        }
    }
    
    static_cast<cScsCommerceConsumableTransaction*>(mp_CurrentTransaction)->SetConsumptionCallback(m_ConsumeCallback);
    static_cast<cScsCommerceConsumableTransaction*>(mp_CurrentTransaction)->SetConsumingUserId(GetCurrentConsumableUser());

    return cCommerceConsumableManager::SetupTransaction(consumableIdentifier, amountToConsume); 
}

int cSCSCommerceConsumableManager::GetConsumableLevel(const char* aConsumableId)
{
    if ( m_CurrentEntitlementDataState != ENTITLEMENT_DATA_STATE_POPULATED )
    {
        return 0;
    }

    int numberOfConsumable = 0;

    for (int s = 0; s < m_EntitlementResponse.m_EntitlementInstanceArray.GetCount(); s++)
    {
        if (strcmp(m_EntitlementResponse.m_EntitlementInstanceArray[s].m_Code.c_str(), aConsumableId) == 0)
        {
            numberOfConsumable += m_EntitlementResponse.m_EntitlementInstanceArray[s].m_Count;
        }
    }

    return numberOfConsumable;
}

bool cSCSCommerceConsumableManager::IsConsumableKnown(const char* aConsumableId)
{
    if ( m_CurrentEntitlementDataState != ENTITLEMENT_DATA_STATE_POPULATED )
    {
        return false;
    }

    bool foundConsumable = false;

    for (int s = 0; s < m_EntitlementResponse.m_EntitlementInstanceArray.GetCount(); s++)
    {
        if (strcmp(m_EntitlementResponse.m_EntitlementInstanceArray[s].m_Code.c_str(), aConsumableId) == 0)
        {
            foundConsumable = true;
        }
    }

    return foundConsumable;
}

//Start commerce consumable transaction code

cScsCommerceConsumableTransaction::cScsCommerceConsumableTransaction() :
	m_CurrentTransactionState(SCS_TRANSACTION_UNINITIALISED),
    m_ConsumeCallback(NULL),
    m_ConsumptionCompleteCallback(NULL)
{
	m_ConsumableId.Reset();
	m_DoConsumptionStatus.Reset();
}

void cScsCommerceConsumableTransaction::Init()
{
	m_CurrentTransactionState = SCS_TRANSACTION_UNINITIALISED;
	m_ConsumableId.Reset();
	m_DoConsumptionStatus.Reset();
	cCommerceConsumableTransaction::Init();
}


bool cScsCommerceConsumableTransaction::StartTransaction( const char* consumableIdentifier, int amountToConsume )
{
	if (consumableIdentifier == NULL)
	{
		return false;
	}

	if ( amountToConsume <= 0 )
	{
		//You want to consume nothing or less than nothing?!?
		return false;
	}

	if ( m_DoConsumptionStatus.Pending() )
	{
		return false;
	}

    m_CurrentInstanceIndex = 0;

	m_ConsumableId = consumableIdentifier;
	m_CurrentTransactionState = SCS_TRANSACTION_NOT_STARTED;
	return cCommerceConsumableTransaction::StartTransaction(consumableIdentifier,amountToConsume);
}

void cScsCommerceConsumableTransaction::Update()
{
	switch(m_CurrentTransactionState)
	{
    case(SCS_TRANSACTION_NOT_STARTED):
        StartNextTransactionInstance();
        break;
	case(SCS_TRANSACTION_PENDING):
		UpdatePending();
        break;
	default:
		break;
	}

	cCommerceConsumableTransaction::Update();
}  

void cScsCommerceConsumableTransaction::UpdatePending()
{
	if (m_DoConsumptionStatus.Pending())
	{
		return;
	}

	if ( m_DoConsumptionStatus.Succeeded() )
	{
        
        bool success = true;

        if (m_ConsumptionCompleteCallback)
        {
            success = (m_ConsumptionCompleteCallback)();
        }
        //Start the next instance
        StartNextTransactionInstance();

		return;
	}
	else
	{
		commerceErrorf("Error consuming entitlement [%s]",m_ConsumableId.c_str());
		m_CurrentTransactionState = SCS_TRANSACTION_ERROR;
		return;
	}
}

void cScsCommerceConsumableTransaction::StartNextTransactionInstance()
{
    commerceAssert(!m_DoConsumptionStatus.Pending());
    if (m_DoConsumptionStatus.Pending())
    {
        return;
    }

    if ( m_CurrentInstanceIndex < m_EntitlementConsumeInstances.GetCount() )
    {
        bool success = (m_ConsumeCallback)( m_ConsumingUser
                                            ,m_EntitlementConsumeInstances[m_CurrentInstanceIndex].m_EntitlementCode
                                            , m_EntitlementConsumeInstances[m_CurrentInstanceIndex].m_EntitlementInstanceId
                                            , m_EntitlementConsumeInstances[m_CurrentInstanceIndex].m_NumToConsume
                                            , &m_DoConsumptionStatus);

        if (!success)   
        {
            commerceErrorf("Error consuming entitlement [%s][%ld]Amount:[%d]",m_EntitlementConsumeInstances[m_CurrentInstanceIndex].m_EntitlementCode.c_str()
                                                                            , m_EntitlementConsumeInstances[m_CurrentInstanceIndex].m_EntitlementInstanceId
                                                                            , m_EntitlementConsumeInstances[m_CurrentInstanceIndex].m_NumToConsume);
            m_CurrentTransactionState = SCS_TRANSACTION_ERROR;
            return;
        }

        m_CurrentInstanceIndex++;
        m_CurrentTransactionState = SCS_TRANSACTION_PENDING;
    }
    else
    {
        m_CurrentTransactionState = SCS_TRANSACTION_COMPLETE;
    }
}

void cScsCommerceConsumableTransaction::Reset()
{
    m_EntitlementConsumeInstances.Reset();
    cCommerceConsumableTransaction::Reset();
}

void cScsCommerceConsumableTransaction::AddConsumeInstance(const char* entitlementId, s64 instanceId, int numToConsume)
{
    sEntitlementConsumeInstance consumeInstance;

    consumeInstance.m_EntitlementInstanceId = instanceId;
    consumeInstance.m_NumToConsume = numToConsume;
    consumeInstance.m_EntitlementCode = entitlementId;
    consumeInstance.m_Consumed = false;

    m_EntitlementConsumeInstances.PushAndGrow(consumeInstance);
}

void cScsCommerceConsumableTransaction::Cancel()
{
    if (!m_DoConsumptionStatus.Pending())
        return;

    if (netTask::HasTask(&m_DoConsumptionStatus))
    {
        netTask::Cancel(&m_DoConsumptionStatus);
    }
    else if (rlGetTaskManager() && rlGetTaskManager()->FindTask(&m_DoConsumptionStatus) != NULL)
    {
        rlGetTaskManager()->CancelTask(&m_DoConsumptionStatus);
    } 
}

}//namespace rage
#endif