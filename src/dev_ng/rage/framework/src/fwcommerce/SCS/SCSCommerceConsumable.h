#ifndef SCSCOMMERCECONSUMABLE_H
#define SCSCOMMERCECONSUMABLE_H

#if RSG_PC
#include "../CommerceConsumable.h"
#include "rline/entitlement/rlrosentitlement.h"

namespace rage
{

typedef bool     (*scsConsumableManagerConsumeCallback)(int localGamerIndex, const char* codeToConsume, s64 instanceIdToConsume, int numToConsume, netStatus* status);
typedef bool     (*scsConsumableManagerConsumeCompleteCallback)();

//TODO: Refactor this class along with the Orbis and Durango equivalents into one cNetStatusCommerceConsumableTransaction, then have the platforms only override StartTransaction.
class cScsCommerceConsumableTransaction : public cCommerceConsumableTransaction
{
public:
	cScsCommerceConsumableTransaction();

	void Init();
	void Update();
    void Reset();


	bool StartTransaction( const char* consumableIdentifier, int amountToConsume );
    

	bool HasCompleted() const { return ((m_CurrentTransactionState == SCS_TRANSACTION_COMPLETE)||(m_CurrentTransactionState == SCS_TRANSACTION_ERROR)); }
	bool HasSucceeded() const { return (m_CurrentTransactionState == SCS_TRANSACTION_COMPLETE);}

	void SetConsumingUserId( int userId ) { m_ConsumingUser = userId; }

    void AddConsumeInstance( const char* entitlementId, s64 instanceId, int numToConsume );

    void SetConsumptionCallback( scsConsumableManagerConsumeCallback newConsumptionCallback ) { m_ConsumeCallback = newConsumptionCallback; }
    void SetConsumptionCompleteCallback( scsConsumableManagerConsumeCompleteCallback newConsumptionCompleteCallback ) { m_ConsumptionCompleteCallback = newConsumptionCompleteCallback; }

    void Cancel();

private:
	void UpdatePending();
    void StartNextTransactionInstance();

	enum eScsTransactionState
	{
		SCS_TRANSACTION_UNINITIALISED,
        SCS_TRANSACTION_NOT_STARTED,
		SCS_TRANSACTION_PENDING,
		SCS_TRANSACTION_COMPLETE,
		SCS_TRANSACTION_ERROR
	};

	eScsTransactionState m_CurrentTransactionState;

	netStatus m_DoConsumptionStatus;
	atString m_ConsumableId;
    int m_CurrentInstanceIndex;

    struct sEntitlementConsumeInstance
    {
    public:
        sEntitlementConsumeInstance() :
            m_EntitlementInstanceId(0),
            m_NumToConsume(0),
            m_Consumed(false)
        {
        }

        

        int m_NumToConsume;
        s64 m_EntitlementInstanceId;
        atString m_EntitlementCode;
        bool m_Consumed;
    };

    atArray<sEntitlementConsumeInstance> m_EntitlementConsumeInstances;

	int m_ConsumingUser;

    scsConsumableManagerConsumeCallback m_ConsumeCallback;
    scsConsumableManagerConsumeCompleteCallback m_ConsumptionCompleteCallback;
};


class cSCSCommerceConsumableManager : public cCommerceConsumableManager
{
public:
	static cCommerceConsumableManager* Instance();
	virtual ~cSCSCommerceConsumableManager();

	void Init();	
	void Shutdown();
    void Update();

	bool StartOwnershipDataFetch();
	bool IsOwnershipDataPopulated() const { return ((m_CurrentEntitlementDataState == ENTITLEMENT_DATA_STATE_POPULATED) && cCommerceConsumableManager::IsOwnershipDataPopulated()); }
    consumableTransactionId SetupTransaction( const char* consumableIdentifier, int amountToConsume );

    int GetConsumableLevel( const char* aConsumableId );
    bool IsConsumableKnown( const char* aConsumableId);

    void SetConsumptionCallback( scsConsumableManagerConsumeCallback newConsumptionCallback ) { m_ConsumeCallback = newConsumptionCallback; }

protected:
	cSCSCommerceConsumableManager();
    void UpdateEntitlementDataFetch();

private:

    //Set out consumption callback here, since it will vary per game.
    scsConsumableManagerConsumeCallback m_ConsumeCallback;
    scsConsumableManagerConsumeCompleteCallback m_ConsumeCompleteCallback;


    //Our data store for the current entitlement data
	rlV2EntitlementResponse m_EntitlementResponse;

	//Static singleton instance of this class
	static cSCSCommerceConsumableManager* mp_Instance;

	enum eAssetDataState
	{
		ENTITLEMENT_DATA_STATE_UNINITIALISED,
		ENTITLEMENT_DATA_FETCHING_DATA,
		ENTITLEMENT_DATA_STATE_POPULATED,
		ENTITLEMENT_DATA_STATE_ERROR,
		ENTITLEMENT_DATA_STATE_NUM_STATES
	};

	eAssetDataState m_CurrentEntitlementDataState;
	netStatus m_EntitlementDataFetchStatus;
	
};

}

#endif	//RSG_PC
#endif //SCSCOMMERCECONSUMABLE_H