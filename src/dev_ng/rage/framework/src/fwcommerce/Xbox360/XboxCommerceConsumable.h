#ifndef XBOXCOMMERCECONSUMABLE__H
#define XBOXCOMMERCECONSUMABLE__H

#if __XENON

#include "../CommerceConsumable.h"

struct _XMARKETPLACE_ASSET_ENUMERATE_REPLY;
typedef _XMARKETPLACE_ASSET_ENUMERATE_REPLY XMARKETPLACE_ASSET_ENUMERATE_REPLY, *PXMARKETPLACE_ASSET_ENUMERATE_REPLY;

struct _XOVERLAPPED;
typedef _XOVERLAPPED *PXOVERLAPPED;

typedef void*             HANDLE;

typedef unsigned long DWORD;

namespace rage
{

class cXboxCommerceConsumableTransaction : public cCommerceConsumableTransaction
{
public:

    cXboxCommerceConsumableTransaction();
    ~cXboxCommerceConsumableTransaction();

    void Init();

    void Update();

    bool HasCompleted() const { return ((m_CurrentTransactionState == X_TRANSACTION_COMPLETE)||(m_CurrentTransactionState == X_TRANSACTION_ERROR)); }
    bool HasSucceeded() const { return (m_CurrentTransactionState == X_TRANSACTION_COMPLETE);}
    
    bool StartTransaction( const char* consumableIdentifier, int amountToConsume );

    

private:
    

    void UpdatePending();

    enum eXboxTransactionState
    {
        X_TRANSACTION_UNINITIALISED,
        X_TRANSACTION_PENDING,
        X_TRANSACTION_COMPLETE,
        X_TRANSACTION_ERROR
    };

    eXboxTransactionState m_CurrentTransactionState;

    PXOVERLAPPED mp_ConsumeOverlapped;
};

class cXboxCommerceConsumableManager : public cCommerceConsumableManager
{
public:
    static cXboxCommerceConsumableManager* Instance();
    ~cXboxCommerceConsumableManager();

    bool StartOwnershipDataFetch();
    bool IsOwnershipDataPopulated() const { return ((m_CurrentAssetDataState == ASSET_DATA_STATE_DATA_POPULATED) && cCommerceConsumableManager::IsOwnershipDataPopulated()); }

    void Update();

    void DumpCurrentOwnershipData() const;

    int GetConsumableLevel( const char* aConsumableId );

    static DWORD GetAssetIdFromString( const char* consumableIdentifier );

protected:
    cXboxCommerceConsumableManager();

private:
    void UpdateAssetEnumeration();
    void ProcessAssetEnumerationReply();

    //Static singleton instance of this class
    static cXboxCommerceConsumableManager* mp_Instance;

    XMARKETPLACE_ASSET_ENUMERATE_REPLY *mp_AssetEnumerateData;
    DWORD m_AssetEnumerateBufferSize;

    PXOVERLAPPED mp_EnumerateOverlapped;
   
    

    HANDLE m_AssetEnumerationHandle;

    enum eAssetDataState
    {
        ASSET_DATA_STATE_UNINITIALISED,
        ASSET_DATA_STATE_ENUMERATING,
        ASSET_DATA_STATE_DATA_POPULATED,
        ASSET_DATA_STATE_ERROR,
        ASSET_DATA_STATE_NUM_STATES
    };

    eAssetDataState m_CurrentAssetDataState;
    bool m_XEnumerateNextUpdate;

    atMap<DWORD,DWORD> m_AssetOwnershipMap;
};

} //namespace rage

#endif //__XENON

#endif //XBOXCOMMERCECONSUMABLE__H