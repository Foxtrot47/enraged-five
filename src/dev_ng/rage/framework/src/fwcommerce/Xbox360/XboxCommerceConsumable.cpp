#if __XENON

#include "XboxCommerceConsumable.h"

#include "../CommerceChannel.h"

#include "atl/string.h"
#include "system/xtl.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()



namespace rage
{
    

// Max number of offers and assets to enumerate
const int MAX_ENUMERATION_RESULTS = 10;

cXboxCommerceConsumableManager* cXboxCommerceConsumableManager::mp_Instance = NULL;

cXboxCommerceConsumableManager* cXboxCommerceConsumableManager::Instance()
{
    if ( mp_Instance == NULL )
    {
        mp_Instance = rage_new cXboxCommerceConsumableManager;
    }

    return mp_Instance;
}

cXboxCommerceConsumableManager::cXboxCommerceConsumableManager() :
    mp_AssetEnumerateData(NULL),
    mp_EnumerateOverlapped(NULL),
    m_CurrentAssetDataState(ASSET_DATA_STATE_UNINITIALISED),
    m_XEnumerateNextUpdate(false)
{
     mp_CurrentTransaction = rage_new cXboxCommerceConsumableTransaction;

     mp_EnumerateOverlapped = rage_new XOVERLAPPED;     
}

cXboxCommerceConsumableManager::~cXboxCommerceConsumableManager()
{
    if ( mp_AssetEnumerateData != NULL )
    {
        delete mp_AssetEnumerateData;
        mp_AssetEnumerateData = NULL;
    }
}

bool cXboxCommerceConsumableManager::StartOwnershipDataFetch()
{
    if ( m_CurrentAssetDataState == ASSET_DATA_STATE_ENUMERATING )
    {
        return false;
    }

    DWORD dwErr;
   
    m_AssetOwnershipMap.Reset();

    dwErr = XMarketplaceCreateAssetEnumerator( GetCurrentConsumableUser(), MAX_ENUMERATION_RESULTS, &m_AssetEnumerateBufferSize, &m_AssetEnumerationHandle );

    if ( dwErr != ERROR_SUCCESS )
    {
        commerceErrorf("Call to XMarketplaceCreateAssetEnumerator failed. dwErr is 0x%x",dwErr);
        m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
        return false;
    }

    if ( mp_AssetEnumerateData != NULL )
    {
        delete mp_AssetEnumerateData;
        mp_AssetEnumerateData = NULL;
    }

    if( mp_AssetEnumerateData == NULL )
    {
        mp_AssetEnumerateData = ( XMARKETPLACE_ASSET_ENUMERATE_REPLY* )rage_new BYTE[m_AssetEnumerateBufferSize];
    }
    
    ZeroMemory( mp_AssetEnumerateData, m_AssetEnumerateBufferSize );

    m_XEnumerateNextUpdate = true;
    m_CurrentAssetDataState = ASSET_DATA_STATE_ENUMERATING;

    return true;
}

void cXboxCommerceConsumableManager::Update()
{
    

    switch(m_CurrentAssetDataState)
    {
    case(ASSET_DATA_STATE_ENUMERATING):
        UpdateAssetEnumeration();
        break;
    default:
        break;
    }

    cCommerceConsumableManager::Update();
}

void cXboxCommerceConsumableManager::UpdateAssetEnumeration()
{

    // Once we have called XEnumerate then next batch of results is ready 
    // when the overlapped operation is complete.
    if ( !m_XEnumerateNextUpdate && XHasOverlappedIoCompleted( mp_EnumerateOverlapped ) )
    {
        DWORD dwCount = 0;
        DWORD dwStatus = XGetOverlappedResult( mp_EnumerateOverlapped, &dwCount, TRUE );

        if ( ERROR_SUCCESS == dwStatus )
        {
            ProcessAssetEnumerationReply();
            m_XEnumerateNextUpdate = TRUE;
        }
        else
        {
            HRESULT errorStatusHandle = XGetOverlappedExtendedError( mp_EnumerateOverlapped );

            if ( errorStatusHandle != HRESULT_FROM_WIN32(ERROR_NO_MORE_FILES) )
            {
                commerceErrorf( "UpdateAssetEnumeration: XGetOverlappedExtendedError failed error code 0x%x  dwStatus from XGetOverlappedResult is 0x%x",errorStatusHandle, dwStatus );
            }

            // ERROR_NO_MORE_FILES is expected when the enumeration process has completed
            commerceAssertf ( errorStatusHandle == HRESULT_FROM_WIN32( ERROR_NO_MORE_FILES ) 
                || errorStatusHandle == HRESULT_FROM_WIN32( ERROR_NO_SUCH_USER )
                || errorStatusHandle == HRESULT_FROM_WIN32( WSAECONNRESET )
                || errorStatusHandle == HRESULT_FROM_WIN32( WSAEHOSTUNREACH )
                , "UpdateAssetEnumeration: Unexpected m_hrErrorStatus: 0x%x dwStatus from XGetOverlappedResult is 0x%x", errorStatusHandle, dwStatus );

            CloseHandle( m_AssetEnumerationHandle );
            m_AssetEnumerationHandle = INVALID_HANDLE_VALUE;

            //We are now done with our data buffer, clean up.
            delete mp_AssetEnumerateData;
            mp_AssetEnumerateData = NULL;

            commerceDisplayf("cXboxCommerceConsumableManager::UpdateAssetEnumeration: Completed commerce consumables ownership level enumeration.");
            m_CurrentAssetDataState = ASSET_DATA_STATE_DATA_POPULATED;
                        
        }
    }

    // Repeatedly call XEnumerate until we get ERROR_NO_MORE_FILES. m_XEnumerateNextUpdate
    // indicates if we still need to call XEnumerate
    if ( m_XEnumerateNextUpdate )
    {
        ZeroMemory( mp_EnumerateOverlapped, sizeof( *mp_EnumerateOverlapped ) );

        DWORD dwError = XEnumerate( m_AssetEnumerationHandle, mp_AssetEnumerateData, m_AssetEnumerateBufferSize, NULL, mp_EnumerateOverlapped );
        m_XEnumerateNextUpdate = false;

        // ERROR_IO_PENDING is expected for asynchronous I/O
        Assert( ERROR_IO_PENDING == dwError );
        if ( ERROR_IO_PENDING != dwError )
        {
            commerceErrorf("cXboxCommerceConsumableManager::UpdateAssetEnumeration: XEnumerate failed with code 0x%x\n",dwError);
        }
    }    
}

void cXboxCommerceConsumableManager::ProcessAssetEnumerationReply()
{
    for (u32 iAssets = 0; iAssets < mp_AssetEnumerateData->assetPackage.cAssets; iAssets++)
    {
        commerceDisplayf("cXboxCommerceConsumableManager::ProcessAssetEnumerationReply. Asset: %d Quantity: %d",mp_AssetEnumerateData->assetPackage.aAssets[iAssets].dwAssetID, mp_AssetEnumerateData->assetPackage.aAssets[iAssets].dwQuantity);
        DWORD *result = m_AssetOwnershipMap.Access( mp_AssetEnumerateData->assetPackage.aAssets[iAssets].dwAssetID );
        if ( result == NULL )
        {
            m_AssetOwnershipMap.Insert( mp_AssetEnumerateData->assetPackage.aAssets[iAssets].dwAssetID, mp_AssetEnumerateData->assetPackage.aAssets[iAssets].dwQuantity );
        }
        else
        {
            *result = mp_AssetEnumerateData->assetPackage.aAssets[iAssets].dwQuantity;
        }
    }
}

void cXboxCommerceConsumableManager::DumpCurrentOwnershipData() const
{
    commerceDebugf1("-------------------------------");
    commerceDebugf1("DUMPING CURRENT OWNERSHIP DATA");
    commerceDebugf1("-------------------------------");

    if ( IsOwnershipDataPopulated() == false )
    {
        commerceDebugf1("*No ownership data to dump, No valid data downloaded*");
        return;
    }

    for(atMap<DWORD, DWORD>::ConstIterator it = m_AssetOwnershipMap.CreateIterator(); !it.AtEnd(); it.Next())
    {
        commerceDebugf1("Asset ID [0x%x] Inventory level [%d]",it.GetKey(),it.GetData());
    }
    
}

int cXboxCommerceConsumableManager::GetConsumableLevel( const char* aConsumableId )
{
    if ( !IsOwnershipDataPopulated() )
    {
        return 0;
    }

    DWORD assetId = GetAssetIdFromString(aConsumableId);

    DWORD *result = m_AssetOwnershipMap.Access( assetId );
    if ( result == NULL )
    {
        return 0;
    }
    else
    {
        return *result;
    }
}

cXboxCommerceConsumableTransaction::cXboxCommerceConsumableTransaction() :
    mp_ConsumeOverlapped(NULL) 
{
    mp_ConsumeOverlapped = rage_new XOVERLAPPED;
}


cXboxCommerceConsumableTransaction::~cXboxCommerceConsumableTransaction()
{
    delete mp_ConsumeOverlapped;
}

void cXboxCommerceConsumableTransaction::Init()
{
    m_CurrentTransactionState = X_TRANSACTION_UNINITIALISED;

    cCommerceConsumableTransaction::Init();
}

bool cXboxCommerceConsumableTransaction::StartTransaction( const char* consumableIdentifier, int amountToConsume )
{
    if (consumableIdentifier == NULL)
    {
        return false;
    }

    if ( amountToConsume <= 0 )
    {
        //You want to consume nothing or less than nothing?!?
        return false;
    }

    
    
    //Put the identifier in here
    DWORD assetId = cXboxCommerceConsumableManager::GetAssetIdFromString( consumableIdentifier );

    if (assetId == 0)
    {
        commerceErrorf("0 returned from GetAssetIdFromString(). Identifier: %s",consumableIdentifier);
        return false;
    }

    XMARKETPLACE_ASSET asset;

    asset.dwAssetID = assetId;
    asset.dwQuantity = amountToConsume;
    

    ZeroMemory( mp_ConsumeOverlapped, sizeof( *mp_ConsumeOverlapped ) );

    DWORD retVal = XMarketplaceConsumeAssets(GetCurrentConsumableUser(),1,&asset,mp_ConsumeOverlapped);


    commerceAssertf(retVal == ERROR_IO_PENDING,"XMarketplaceConsumeAssets failed with a return of 0x%x",retVal);

    if (ERROR_IO_PENDING != retVal)
    {   
        return false;
    }

    m_CurrentTransactionState = X_TRANSACTION_PENDING;

    commerceDebugf3("Starting commerce asset consumption ID:0x%x  Amount:%d",assetId,amountToConsume);

    return cCommerceConsumableTransaction::StartTransaction(consumableIdentifier,amountToConsume);
}

DWORD cXboxCommerceConsumableManager::GetAssetIdFromString( const char* consumableIdentifier )
{
    atString identifierString(consumableIdentifier);
    if ( identifierString.GetLength() == 0 )
    {
        return 0;
    }

    DWORD retVal = 0;
    if ( identifierString.StartsWith("0x") )
    {
        sscanf( identifierString, "0x%x", &retVal );
    }
    else
    {
        sscanf( identifierString, "%d", &retVal );
    }

    return retVal;
}

void cXboxCommerceConsumableTransaction::Update()
{
    switch(m_CurrentTransactionState)
    {
    case(X_TRANSACTION_PENDING):
        UpdatePending();
    default:
        break;
    }

    cCommerceConsumableTransaction::Update();
}   

void cXboxCommerceConsumableTransaction::UpdatePending()
{
    DWORD dwResult;
    DWORD dwErr = XGetOverlappedResult( mp_ConsumeOverlapped, &dwResult, FALSE );
    if ( dwErr != ERROR_IO_INCOMPLETE )
    {
        //Cant assert on this, there are a wide range of potential errors that can be returned by the Checkout utility.
        //AssertM( dwErr == ERROR_SUCCESS || dwErr == ERROR_CANCELLED, ( "Unknown error returned from checkout: 0x%x", dwErr ) );
        if ( dwErr != ERROR_SUCCESS && dwErr != ERROR_CANCELLED )
        {
            commerceErrorf( "Error code from consume asset overlapped operation: 0x%x\n", dwErr );
            m_CurrentTransactionState = X_TRANSACTION_ERROR;
        }
        else
        {
            commerceDebugf3("Xbox Commerce Asset consumption completed successfully");
            m_CurrentTransactionState = X_TRANSACTION_COMPLETE;
            CommerceConsumablesInstance()->SetOwnershipDataDirty();
        }
    }
}

} //namespace rage

#endif