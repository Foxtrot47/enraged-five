#if __XENON

#include "XboxCommerceUtil.h"

#include "../CommerceChannel.h"

#include "fwnet/optimisations.h"
#include "rline/rltitleid.h"
#include "string/unicode.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/xtl.h"


NETWORK_OPTIMISATIONS()
//OPTIMISATIONS_OFF()

namespace rage
{

cXboxCommerceManager* cXboxCommerceManager::m_Instance = NULL;

extern const rlTitleId* g_rlTitleId;

const int TOTAL_POOL_SIZE = 128 * 1024;

const int FREE_PURCHASE_OWNED_DISPLAY_PRICE_BUNDLE_ONE_DEPENDANT = 1778;
const int FREE_PURCHASE_OWNED_DISPLAY_PRICE = 1777;

const int ABORT_THREAD_STACK_SIZE = 4096;

static sysCriticalSectionToken s_dataAbortCritSecToken;

//----------------------------------------------------------
cXboxCommerceManager::cXboxCommerceManager() :
    m_ContentEnumerationInProgress( false ),
    m_CurrentState( MODE_TOP ) ,
    m_bEnumerateNext( false ),
    m_hrErrorStatus( S_OK ),
    m_pBuffer( NULL ),
    m_pOverlapped( NULL ),
    m_pCheckoutOverlapped( NULL ),
    m_ContentInfoPopulated( false ),
    m_NumEnumeratedOffers( 0 ),
    m_NumOfferIdsToManuallyEnumerate( 0 ),
    m_NumShownMetaDataRecords( 0 ),
    m_CurrentCommerceUserIndex( 0 ),
    m_Initialised( false ),
	m_DataFetchAbortInProgress( false )  
{
    //XboxEnvironment::Instance().AllocateEnumerationMemory();

	if ( m_Instance == NULL )
	{
		m_Instance = this;
	}

    m_pOverlapped = rage_new XOVERLAPPED;
    ZeroMemory( m_pOverlapped, sizeof( XOVERLAPPED ) );

    m_pCheckoutOverlapped = rage_new XOVERLAPPED;
    ZeroMemory( m_pCheckoutOverlapped, sizeof( XOVERLAPPED ) );


    for ( int i = 0; i < MAX_NUM_OFFERS; i++ )
    {
        m_pMarketplaceOffers[ i ] = NULL; 
    }
} 


//----------------------------------------------------------
cXboxCommerceManager::~cXboxCommerceManager()
{   
    //XboxEnvironment::Instance().FreeEnumerationMemory();
	SYS_CS_SYNC(s_dataAbortCritSecToken);
    if ( m_pOverlapped )
    {
        delete m_pOverlapped;
        m_pOverlapped = NULL;
    }

    if ( m_pCheckoutOverlapped )
    {
        delete m_pCheckoutOverlapped;
        m_pCheckoutOverlapped = NULL;
    }

    Shutdown();

	m_Instance = NULL;
}

bool cXboxCommerceManager::Init( int userIndex, const char* skuFolder )
{
    if ( m_Initialised )
    {
        commerceErrorf("Trying to initialise the commerce manager when it has already been intialised.");
        commerceAssert( false );
        return false;
    }

    m_Initialised = true;
    
    m_CurrentCommerceUserIndex = userIndex;

	m_DataFetchAbortInProgress = false;

    cCommerceUtil::Init( userIndex, skuFolder );

    return true;
}

//----------------------------------------------------------
void cXboxCommerceManager::Shutdown()
{
    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to shutdown commerce manager without initialising." );
        return;
    }

    FreeOffers();
   
    m_ContentEnumerationInProgress = false;
    m_CurrentState = MODE_TOP;
    m_bEnumerateNext = false;
    m_hrErrorStatus = S_OK;
    m_pBuffer = NULL;
    m_pOverlapped = NULL;
    m_pCheckoutOverlapped = NULL;
    m_ContentInfoPopulated = false;
    m_NumEnumeratedOffers = 0;
    m_NumOfferIdsToManuallyEnumerate = 0;
    m_NumShownMetaDataRecords = 0;
    m_CurrentCommerceUserIndex = 0;
    m_Initialised = false;

    cCommerceUtil::Shutdown();
}

//----------------------------------------------------------
void cXboxCommerceManager::Update()
{
    cCommerceUtil::Update();

    if ( !m_Initialised )
    {
        return;
    }

    

    switch( m_CurrentState )
    {
    case( MODE_ENUMERATE_CONTENT ):
    case( MODE_ENUMERATE_MANUAL_CONTENT ): 
        UpdateEnumerateContent();
        break;
    case( MODE_CHECKOUT ):
        UpdateCheckout();
        break;
    default:
        break;
    }
}

//----------------------------------------------------------
bool cXboxCommerceManager::DoPlatformDataRequest()
{
	SYS_CS_SYNC(s_dataAbortCritSecToken);

	if ( m_DataFetchAbortInProgress )
	{
		return false;
	}

    if ( !cCommerceUtil::IsContentInfoPopulated() )
    {
        return false;
    }

    FreeOffers();

    m_ContentInfoPopulated = false;

	
    ZeroMemory( m_pOverlapped, sizeof( XOVERLAPPED ) );
    m_pOverlapped->hEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
    if( m_pOverlapped->hEvent == NULL )
    {
        commerceAssert( false );
    }

    PopulateOffersToEnumerateArray();

    m_NumEnumeratedOffers = 0;

    //SetupAutoEnumeration();

    SetupManualEnumeration();

    return true;

    //THIS WILL REQUIRE A POPULATED BLUE BOX
    //
}

void cXboxCommerceManager::SetupManualEnumeration()
{
	SYS_CS_SYNC(s_dataAbortCritSecToken);
    ZeroMemory( m_pOverlapped, sizeof( XOVERLAPPED ) );
    m_pOverlapped->hEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
    if( m_pOverlapped->hEvent == NULL )
    {
        commerceAssert( false );
    }

    DWORD bufferSize = m_dwBufferSize;

    // Enumerate at most MAX_ENUMERATION_RESULTS items
    DWORD dwError = XMarketplaceCreateCurrencyOfferEnumeratorByOffering
        (
        GetCurrentCommerceUserIndex(), // Get the offer data for this player
        NUM_OFFERS_PER_ENUM_PASS,                       // Number of results per call to XEnumerate
        m_OfferIdsToManuallyEnumerate,
        m_NumOfferIdsToManuallyEnumerate,
        &bufferSize,                        // Size of buffer needed for results
        &m_hEnumeration                         // Enumeration handle
        );

    if ( dwError != ERROR_SUCCESS )
    {
        commerceErrorf("XMarketplaceCreateOfferEnumerator failed with code %d\n",dwError);
        return;
    }

    m_dwBufferSize = bufferSize;

    if ( m_pBuffer != NULL )
    {
        CommerceFree( m_pBuffer );
        m_pBuffer = NULL;
    }

    //Create the actual buffer
    m_pBuffer =  (BYTE*)CommerceAlloc( m_dwBufferSize );
    commerceAssert( m_pBuffer );

    m_bEnumerateNext = true;

    m_CurrentState = MODE_ENUMERATE_MANUAL_CONTENT; 
}

//----------------------------------------------------------
void cXboxCommerceManager::UpdateEnumerateContent()
{
	SYS_CS_SYNC(s_dataAbortCritSecToken);
    // Once we have called XEnumerate then next batch of results is ready 
    // when the overlaped operation is complete.
    if ( !m_bEnumerateNext && XHasOverlappedIoCompleted( m_pOverlapped ) )
    {
        DWORD dwCount = 0;
        DWORD dwStatus = XGetOverlappedResult( m_pOverlapped, &dwCount, TRUE );

        if ( ERROR_SUCCESS == dwStatus )
        {
            PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO pTempOfferData = ( PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO )m_pBuffer;
            for (DWORD dw = 0; dw < dwCount; ++dw)
            {
                //if ( m_CurrentState != MODE_ENUMERATE_MANUAL_CONTENT || pTempOfferData->fUserHasPurchased )
                {
                    AddOffer( &pTempOfferData[dw] );            
                }
                
            }
            m_bEnumerateNext = TRUE;
        }
        else
        {
            m_hrErrorStatus = XGetOverlappedExtendedError( m_pOverlapped );
            
            if ( m_hrErrorStatus != HRESULT_FROM_WIN32(ERROR_NO_MORE_FILES) )
            {
                commerceErrorf( "XGetOverlappedExtendedError failed error code 0x%x  dwStatus from XGetOverlappedResult is 0x%x",m_hrErrorStatus, dwStatus );
            }

            // ERROR_NO_MORE_FILES is expected when the enumeration process has completed
            //
            //  OK. I haven't lost my mind. According to MS, an overlapped call to a marketplace enumerator will occasionally return
            //  ERROR_FUNCTION_FAILED, and that is fine. Text from docs here:
            // 
            //  If enumerating offers asynchronously it is possible the API may return ERROR_FUNCTION_FAILED after successfully enumerating offers. 
            //  If this occurs the offers returned are valid and the error indicates there are no more valid offers remaining.
            //
            //
            //  Thanks Microsoft!!!!

            commerceAssertf ( m_hrErrorStatus == HRESULT_FROM_WIN32( ERROR_NO_MORE_FILES ) 
                        || m_hrErrorStatus == HRESULT_FROM_WIN32( ERROR_NO_SUCH_USER )
                        || m_hrErrorStatus == HRESULT_FROM_WIN32( WSAECONNRESET )
                        || m_hrErrorStatus == HRESULT_FROM_WIN32( WSAEHOSTUNREACH )
                        || dwStatus == ERROR_FUNCTION_FAILED
                        , "Unexpected m_hrErrorStatus: 0x%x dwStatus from XGetOverlappedResult is 0x%x", m_hrErrorStatus, dwStatus );
            
            //Pull the offers out of MS's crazy ERROR_FUNCTION_FAILED state
            if (dwStatus == ERROR_FUNCTION_FAILED)
            {
                commerceDebugf1("Processing offers returned with a dwStatus == ERROR_FUNCTION_FAILED result. %d offers enumerated.",dwCount);
                PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO pTempOfferData = ( PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO )m_pBuffer;
                for (DWORD dw = 0; dw < dwCount; ++dw)
                {
                    AddOffer( &pTempOfferData[dw] );            
                }
            }

            CloseHandle( m_hEnumeration );
            m_hEnumeration = INVALID_HANDLE_VALUE;

            if ( m_CurrentState == MODE_ENUMERATE_MANUAL_CONTENT || m_NumOfferIdsToManuallyEnumerate == 0 )
            {
                //We are done. Run through the list of metadata to populate our list of what to show.
                
                //Population is now done at the commerceUtil level.
                //PopulateMetadataToShowArray();
                
                
                m_CurrentState = MODE_NEUTRAL;
                m_ContentEnumerationInProgress = false;
                m_ContentInfoPopulated = true;
            }
            else if ( m_CurrentState == MODE_ENUMERATE_CONTENT )
            {
                SetupManualEnumeration();
            }
            else
            {
                Assert( false );
            }
        }
    }

    // Repeatedly call XEnumerate until we get ERROR_NO_MORE_FILES. m_bEnumerateNext
    // indicates if we still need to call XEnumerate
    if ( m_bEnumerateNext )
    {
        DWORD dwError = XEnumerate( m_hEnumeration, m_pBuffer, m_dwBufferSize, NULL, m_pOverlapped );
        m_bEnumerateNext = FALSE;

        // ERROR_IO_PENDING is expected for asynchronous I/O
        Assert( ERROR_IO_PENDING == dwError );
        if ( ERROR_IO_PENDING != dwError )
        {
            commerceErrorf("XEnumerate failed with code 0x%x\n",dwError);
        }
    }
}

//----------------------------------------------------------
int cXboxCommerceManager::GetNumProducts() const
{
    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call GetNumProducts without initialising commerce manager." );
        Assert( false );
        return 0;
    }

    if ( IsContentInfoPopulated() )
    {
        return m_NumShownMetaDataRecords;
    }
    else
    {
        return 0;
    }
}

//----------------------------------------------------------
void cXboxCommerceManager::AddOffer( const PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO a_Offer )
{
    commerceAssert( m_NumEnumeratedOffers < MAX_NUM_OFFERS );

    if ( m_NumEnumeratedOffers >= MAX_NUM_OFFERS )
    {
        return;
    }

    // Calculate the size needed for the structure
    DWORD dwBufferSize = sizeof( XMARKETPLACE_CURRENCY_CONTENTOFFER_INFO );
    // arrange for extra storage at the end to hold the currency string
	dwBufferSize += ( a_Offer->dwCurrencyPriceLength  + 1 ) * sizeof(WCHAR);

    // Allocate and initialize
    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO pOfferData = (PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO) CommerceAlloc( dwBufferSize );
    commerceAssert( pOfferData );

    if ( pOfferData == NULL )
    {
        return;
    }

    ZeroMemory( pOfferData, dwBufferSize );
    XMemCpy( pOfferData, a_Offer, sizeof( XMARKETPLACE_CURRENCY_CONTENTOFFER_INFO ) );

    // Copy the Offer Name
    pOfferData->wszCurrencyPrice = (WCHAR *)&pOfferData[1];
    XMemCpy(pOfferData->wszCurrencyPrice, a_Offer->wszCurrencyPrice, a_Offer->dwCurrencyPriceLength * sizeof(WCHAR) );
	
	//Terminate the string properly, since it isnt in the source data.
	pOfferData->wszCurrencyPrice[a_Offer->dwCurrencyPriceLength] = 0;

    // Add the record to the array of offers
    m_pMarketplaceOffers[ m_NumEnumeratedOffers ] = pOfferData;
    
    m_NumEnumeratedOffers++;
}

//----------------------------------------------------------
void cXboxCommerceManager::FreeOffers()
{
    for (int i = 0; i < MAX_NUM_OFFERS; i++ )
    {
        if (m_pMarketplaceOffers[ i ] != NULL )
        {
            CommerceFree( m_pMarketplaceOffers[ i ] );
            m_pMarketplaceOffers[ i ] = NULL;
        }
    }

    m_NumEnumeratedOffers = 0;
    m_ContentInfoPopulated = false;
}

//----------------------------------------------------------
bool cXboxCommerceManager::IsProductPurchased( int a_Index ) const
{
    Assert( IsContentInfoPopulated() );
    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );

    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }
            
    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    return IsProductPurchased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
    /*
    if ( GetOfferInfoForIndex( a_Index ) )
    {
        //Force the owned flag to behave the way we would like it to, regardless of MS backend fields.
        // IF the price is 0 AND ( The unused metadate price field is either of our preset values ) AND the dependancy is enumerated

        bool forceComingSoonOwned = GetOfferInfoForIndex( a_Index )->dwPointsPrice == 0 
            &&  ( m_MetaDataRecords[ m_MetaDataRecordsToShowIndices[ a_Index ] ].displayPrice == FREE_PURCHASE_OWNED_DISPLAY_PRICE || m_MetaDataRecords[ m_MetaDataRecordsToShowIndices[ a_Index ] ].displayPrice ==  FREE_PURCHASE_OWNED_DISPLAY_PRICE_BUNDLE_ONE_DEPENDANT )
            && m_MetaDataRecords[ m_MetaDataRecordsToShowIndices[ a_Index ] ].isDependancyEnumerated; 

        return ( GetOfferInfoForIndex( a_Index )->fUserHasPurchased  != 0 || forceComingSoonOwned );   

  }
    else
    {
        return false;
    }
    */
}

bool cXboxCommerceManager::IsProductPurchased( const cCommerceProductData* a_pProductData ) const
{
    if (GetOfferInfo( a_pProductData ) == NULL)
    {
        return false;
    }

    return ( GetOfferInfo( a_pProductData )->fUserHasPurchased != 0 );
}

/*
bool cXboxCommerceManager::IsProductReleasedFromMetadataIndex( int a_MetaDataIndex ) const
{
    Assert( a_MetaDataIndex < m_NumMetaDataRecords );

    return IsProductReleasedFromMetadata( m_MetaDataRecords[ a_MetaDataIndex ] );
}
    
bool cXboxCommerceManager::IsProductReleasedFromMetadata( const OfferMetaData & a_MetaData ) const
{
    SYSTEMTIME UTCSysTime;
    GetSystemTime( &UTCSysTime );

    if ( a_MetaData.releaseYear > UTCSysTime.wYear )
    {
        return false;
    }
    else if ( a_MetaData.releaseYear < UTCSysTime.wYear )
    {
        return true;
    }
    //same year, check month
    else if ( a_MetaData.releaseMonth > UTCSysTime.wMonth )
    {
        return false;
    }
    else if ( a_MetaData.releaseMonth < UTCSysTime.wMonth )
    {
        return true;
    }
    //same month, check day
    else if ( a_MetaData.releaseDay > UTCSysTime.wDay )
    {
        return false;
    }
    else if ( a_MetaData.releaseDay < UTCSysTime.wDay )
    {
        return true;
    }
    // same day! Its release day, so let it through.
    else
    {
        return true;
    }
}
*/

//----------------------------------------------------------
bool cXboxCommerceManager::IsProductReleased( int /*a_Index*/ ) const
{
    Assert( IsContentInfoPopulated() );
    //Assert( static_cast<int>( a_Index ) < GetNumProducts() );

    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call IsProductReleased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert(false);
    return false;
    //IsProductReleasedFromMetadataIndex ( m_MetaDataRecordsToShowIndices[ a_Index ] );

}

//----------------------------------------------------------
bool cXboxCommerceManager::IsProductPurchasable( int a_Index ) const
{
    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductPurchasable( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cXboxCommerceManager::IsProductPurchasable( const cCommerceProductData* a_pProductData) const
{
    return a_pProductData->GetEnumeratedFlag();
}

//----------------------------------------------------------
void cXboxCommerceManager::UpdateCheckout()
{
    DWORD dwResult;
    DWORD dwErr = XGetOverlappedResult( m_pCheckoutOverlapped, &dwResult, FALSE );
    if ( dwErr != ERROR_IO_INCOMPLETE )
    {
        //Cant assert on this, there are a wide range of potential errors that can be returned by the Checkout utility.
        //AssertM( dwErr == ERROR_SUCCESS || dwErr == ERROR_CANCELLED, ( "Unknown error returned from checkout: 0x%x", dwErr ) );
        if ( dwErr != ERROR_SUCCESS && dwErr != ERROR_CANCELLED )
        {
            //Odd error, throw it out to tty so we can see it.
            commerceErrorf( "ERROR CODE RETURNED FROM CHECKOUT: %d\n", dwErr );
        }

		CloseHandle(m_pCheckoutOverlapped->hEvent);

        m_DidLastCheckoutSucceed = (dwErr == ERROR_SUCCESS);

        m_CurrentState = MODE_NEUTRAL;
    }
}

//----------------------------------------------------------
void cXboxCommerceManager::CheckoutProduct( u32 a_Index )
{
    Assert( m_CurrentState == MODE_NEUTRAL );
   
    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call CheckoutProduct without initialising commerce manager." );
        Assert( false );
        return ;
    }

    Assert( IsContentInfoPopulated() );

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return;
    }

    return CheckoutProduct( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

void cXboxCommerceManager::CheckoutProduct( const cCommerceProductData* a_pProductdata )
{
    ZeroMemory( m_pCheckoutOverlapped, sizeof( XOVERLAPPED ) );
    m_pCheckoutOverlapped->hEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
    if( m_pCheckoutOverlapped->hEvent == NULL )
    {
        commerceErrorf( "Failed to create Overlapped event.\n" );
        Assert( false );
        return;
    }

    if ( !a_pProductdata->GetEnumeratedFlag() )
    {
        return;
    }

    if ( GetOfferInfo( a_pProductdata ) == NULL )
    {
        return;
    }

    //We never refetch consumables
    bool refetch = (IsProductPurchased( a_pProductdata ) ) && !a_pProductdata->IsConsumable();

    DWORD dwErr = ERROR_SUCCESS;

    DWORD dwEntryPoint;
    if ( refetch )
    {
        dwEntryPoint = XSHOWMARKETPLACEDOWNLOADITEMS_ENTRYPOINT_FREEITEMS;
    }
    else
    {
        dwEntryPoint = XSHOWMARKETPLACEDOWNLOADITEMS_ENTRYPOINT_PAIDITEMS;
    }

    ULONGLONG fullOfferId = ProductDataStringToOfferId( a_pProductdata->GetPlatformId() );

    dwErr = XShowMarketplaceDownloadItemsUI(
        GetCurrentCommerceUserIndex(),
        dwEntryPoint,
        &( fullOfferId ),        // the OfferIDs for the items we're downloading
        1,    // the number of OfferIDs to download
        &m_hrErrorStatus,
        m_pCheckoutOverlapped                   // XShowMarketplaceDownloadItemsUI must be called asynchronously
        );


    m_CurrentState = MODE_CHECKOUT;

    Assert( ERROR_IO_PENDING == dwErr );
}

bool cXboxCommerceManager::ProductCodeEntry()
{
    XShowTokenRedemptionUI( GetCurrentCommerceUserIndex() );
    m_DidLastCheckoutSucceed = true;
	return true;
}

float cXboxCommerceManager::GetProductPrice( u32 a_Index, char* a_pOutputString, u32 a_SizeOfOutputString ) const
{
    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
        Assert( false );
        return -1.0f;
    }

    Assert( IsContentInfoPopulated() );

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return -1.0f;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return -1.0f;
    }

    return GetProductPrice( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ), a_pOutputString, a_SizeOfOutputString );
}

float cXboxCommerceManager::GetProductPrice( const cCommerceProductData* a_pProductData, char* a_pOutputString, unsigned a_SizeOfOutputString ) const
{
    if ( !a_pProductData->GetEnumeratedFlag() )
    {
        return -1.0f;
    }

    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO offerInfo = GetOfferInfo( a_pProductData );

    float price = 0.0f;

    if ( offerInfo == NULL )
    {
        return -1.0f;
    }
    else
    {
        price = 1.0f; //Thanks MS, for removing the numeric value of the price. Most helpful.
		if (a_pOutputString && a_SizeOfOutputString > 0)
		{
			WideToUtf8(a_pOutputString,(rage::char16*)(offerInfo->wszCurrencyPrice),a_SizeOfOutputString);
		}

    }

    return price;
}


bool cXboxCommerceManager::IsOfferIdAllreadyOnManualCheckList( ULONGLONG offerIdToCheck )
{
    bool result = false;
    for ( int i = 0; i < m_NumOfferIdsToManuallyEnumerate; i++ )
    {
        if ( m_OfferIdsToManuallyEnumerate[ i ] == offerIdToCheck )
        {
            result = true;
        }
    }

    return result;
}

void cXboxCommerceManager::UpdateCommerceDataWithEnumeratedContent( ULONGLONG offerId, bool /*a_IsOwned*/  )
{
    

    for ( int i = 0; i < m_CommerceData.GetSizeOfDataArray(); i++ )
    {
        if ( m_CommerceData.GetItemData( i )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
        {
            //Not a product, ergo does not need to be checked.
            continue;
        }

        cCommerceProductData* productData = static_cast<cCommerceProductData*>(m_CommerceData.GetItemData( i ));

        ULONGLONG dataEnumOfferId = ProductDataStringToOfferId( productData->GetPlatformId() );

        if ( dataEnumOfferId != 0 && dataEnumOfferId == offerId )
        {
            productData->SetEnumeratedFlag( true );
        }

        //TODO: Check for deps and exs
        /*
        if ( m_MetaDataRecords[i].dependantOfferId == offerId && a_IsOwned )
        {
            m_MetaDataRecords[i].isDependancyEnumerated = true;
        }

        if ( m_MetaDataRecords[i].excludedOfferId == offerId && a_IsOwned )
        {
            m_MetaDataRecords[i].isExclusionEnumerated = true;
        }
        */
    }
}

/*
//This is now done on the cCommerceUtil level. Keeping code here for now for reference purposes.
void cXboxCommerceManager::PopulateMetadataToShowArray()
{
    m_NumShownMetaDataRecords = 0;

    for ( int i = 0; i < m_NumMetaDataRecords; i++ )
    {

        OfferMetaData metaData = m_MetaDataRecords[ i ];

        if ( ShouldMetaDataShow( metaData, true ) )
        {
            m_MetaDataRecordsToShowIndices[ m_NumShownMetaDataRecords ] = i;
            m_NumShownMetaDataRecords++;
        }
    }

}
*/
/*
PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO cXboxCommerceManager::GetOfferInfoForIndex( u32 a_Index ) const
{
    Assert( IsContentInfoPopulated() );
    Assert( static_cast<int>( a_Index ) < GetNumProducts() );

    if ( static_cast<int>( a_Index ) >= GetNumProducts() )
    {
        return NULL;
    }

    ULONGLONG linkedOfferId = m_MetaDataRecords[ m_MetaDataRecordsToShowIndices[ a_Index ] ].linkedOfferId;
    if (  linkedOfferId == 0x0 )
    {
        //Not linked.
        return NULL;
    }
    else if ( !m_MetaDataRecords[ m_MetaDataRecordsToShowIndices[ a_Index ] ].isLinkEnumerated )
    {
        //linked offer wasnt found.
        return NULL;
    }

    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO pOffer = NULL;

    for (int i = 0; i < m_NumEnumeratedOffers; i++ )
    {
        if ( linkedOfferId == m_pMarketplaceOffers[ i ]->qwOfferID )
        {
            pOffer = m_pMarketplaceOffers[ i ];
        }
    }

    return pOffer;
}
*/

PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO cXboxCommerceManager::GetOfferInfo( const cCommerceProductData* pProductInfo ) const
{
    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return NULL;
    }
 
    if ( !pProductInfo->GetEnumeratedFlag() )
    {
        //Didnt tie to an actual product, return NULL
        return NULL;
    }

    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO retptr = NULL;

    for ( int i = 0; i < m_NumEnumeratedOffers; i++ )
    {
        if ( ProductDataStringToOfferId( pProductInfo->GetPlatformId() ) == m_pMarketplaceOffers[i]->qwOfferID )
        {
            //Found the correct offer.
            retptr = m_pMarketplaceOffers[i];
            break;
        }
    }

    return retptr;
}

void cXboxCommerceManager::SetupAutoEnumeration()
{
    DWORD bufferSize = m_dwBufferSize;
    // Enumerate at most MAX_ENUMERATION_RESULTS items
    DWORD dwError = XMarketplaceCreateCurrencyOfferEnumerator
        (
        GetCurrentCommerceUserIndex(),       // Get the offer data for this player
        XMARKETPLACE_OFFERING_TYPE_CONTENT,   // Marketplace content (can combine values using ||)
        0xffffffff,                           // Retrieve all content catagories
        MAX_NUM_OFFERS,                       // Number of results per call to XEnumerate
        &bufferSize,                        // Size of buffer needed for results
        &m_hEnumeration                         // Enumeration handle
        );

    if ( dwError != ERROR_SUCCESS )
    {
        commerceErrorf("XMarketplaceCreateOfferEnumerator failed with code %d\n",dwError);
        commerceAssert( false );
        return;
    }

    m_dwBufferSize = bufferSize;

    if ( m_pBuffer != NULL )
    {
        CommerceFree( m_pBuffer );
        m_pBuffer = NULL;
    }

    //Create the actual buffer
    m_pBuffer =  (BYTE*)CommerceAlloc( m_dwBufferSize );
    Assert( m_pBuffer );

    m_bEnumerateNext = true;

    m_CurrentState = MODE_ENUMERATE_CONTENT;
}


// bool cXboxCommerceManager::ShouldMetaDataShow( const OfferMetaData &a_MetaData, bool /*useMSFix = false*/ )
// {
//     bool addToShowList = true;
// 
//     //Handle linked product conditions. If the link is not present we will show any products with future release dates which are linked, and hide any with past ones (we have a problem as their actual content is not present.)
//     if ( !a_MetaData.isLinkEnumerated  && IsProductReleasedFromMetadata( a_MetaData ) )
//     {
//         addToShowList = false;
//     }
// 
//     if ( a_MetaData.dependantOfferId  && addToShowList )
//     {
//         //Hide offers with a dependancy
//         addToShowList = false;
// 
//         if ( a_MetaData.isDependancyEnumerated )
//         {
//             //Unless their dependendancy is present
//             addToShowList = true;
//         }
// 
// 
//         Assert( false );
//         //TODO: properly implement this as a part of the overall solution.
//         /*
//         if ( a_MetaData.displayPrice == FREE_PURCHASE_OWNED_DISPLAY_PRICE_BUNDLE_ONE_DEPENDANT &&
//             !XboxEnvironment::Instance().IsBundleKeyMounted( 1 ) )
//         {
//             //Even if we have the dependency enumerated, we need to hide this line if key 1 has not been mounted.
//             addToShowList = false;
//         }
//         */
//     }
// 
//     //TODO: properly implement this cludge as a part of the overall solution.
//     //This will be worked through with Dan Conti when I am in NE.
//     Assert( false );
//     /*
//     //Really horrible cludge to make MS happy.
//     //We are going to treat any product which has an enumerated linked product specially.
//     //These products will be added if they are installed. 
//     //This hack will not occur if the product would have been displayed normally by a later metadata line.
//     if ( useMSFix && a_MetaData.linkedOfferId && a_MetaData.isLinkEnumerated )
//     {
//         bool wouldShow = false;
//         for ( int i = 0; i < m_NumMetaDataRecords; i++ )
//         {
//             if ( ShouldMetaDataShow( m_MetaDataRecords[ i ] ) )
//             {
//                 wouldShow = true;
//             }
//         }
// 
//         if ( !wouldShow && XboxEnvironment::Instance().GetMountedDLCMask() &  a_MetaData.containedDLCMask )
//         {
//             addToShowList = true;
//         }
//     }
//     */
// 
// 
// 
//     //Exclusions take priority over everything
//     if ( a_MetaData.excludedOfferId && a_MetaData.isExclusionEnumerated )
//     {
//         addToShowList = false;
//     }
// 
// 
// 
//     //Check if a product with this name has allready been added. If so, the first one takes priority
//     for ( int j = 0; j < m_NumShownMetaDataRecords; j++ )
//     {
//         if ( strcmp( a_MetaData.productIdentifier, m_MetaDataRecords[ m_MetaDataRecordsToShowIndices[ j ] ].productIdentifier ) == 0 )
//         {
//             //Allready here.
//             addToShowList = false;
//         }
//     }
// 
//     return addToShowList;
// }

void cXboxCommerceManager::AbortDataFetch()
{
	commerceDisplayf("Entering cXboxCommerceManager::AbortDataFetch() thread");
	SYS_CS_SYNC(s_dataAbortCritSecToken);
	//Prevent this being hammered
	if (m_DataFetchAbortInProgress)
	{
		return;
	}

    cCommerceUtil::AbortDataFetch();

    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call AbortDataFetch without initialising commerce manager." );
        commerceAssert( false );
        return;
    }

    
	if ( !IsPlatformInDataFetchState() )
	{
		commerceDisplayf("Returning from cXboxCommerceManager::AbortDataFetch due to lack of data fetch state");
		return;
	}

	commerceDisplayf("Creating cXboxCommerceManager::AbortDataFetch() thread");
	sysIpcThreadId dataFetchAbortThreadId = sysIpcCreateThread( AbortDataFetchThread, NULL , ABORT_THREAD_STACK_SIZE, PRIO_NORMAL, "XboxCommerceDataFetchAbort" ); 
	if ( dataFetchAbortThreadId == sysIpcThreadIdInvalid )
	{
		AssertMsg( false, "Thread create failed" );
		return;
	}
	
	
	m_DataFetchAbortInProgress = true;
}

void cXboxCommerceManager::AbortDataFetchThread( void * /*pData*/ )
{
	SYS_CS_SYNC(s_dataAbortCritSecToken);
    cXboxCommerceManager* instance = cXboxCommerceManager::Instance();
	
	if ( instance->m_pOverlapped && instance->m_pOverlapped->hEvent != NULL )
	{
		commerceDisplayf("AbortDataFetchThread");
		//There is a valid overlapped structure present. Send the cancel to it.
		DWORD result = XCancelOverlapped( instance->m_pOverlapped );
		commerceDisplayf("AbortDataFetchThread");
		result = result;
		commerceAssert( result == ERROR_SUCCESS );
	}

	commerceDisplayf("cXboxCommerceManager::AbortDataFetch() thread completed");
	instance->m_DataFetchAbortInProgress = false;
}

cXboxCommerceManager* cXboxCommerceManager::Instance()
{
	if ( m_Instance == NULL )
	{
		m_Instance = rage_new cXboxCommerceManager;
	}

	return m_Instance;
}

ULONGLONG cXboxCommerceManager::ProductDataStringToOfferId( const atString &offerIdString ) const
{
    if ( offerIdString.GetLength() == 0 )
    {
        return 0;
    }

    ULONGLONG fullOfferId = g_rlTitleId->m_XblTitleId;
    fullOfferId = fullOfferId << 32;

    DWORD tempVal;
    sscanf( offerIdString, "0x%x", &tempVal );

    fullOfferId |= tempVal;

    return fullOfferId;
}

bool cXboxCommerceManager::IsContentInfoPopulated() const
{
    return ( m_ContentInfoPopulated && cCommerceUtil::IsContentInfoPopulated() );
}

void cXboxCommerceManager::MergePlatformAndRosData()
{
    for ( int i=0; i< m_NumEnumeratedOffers; i++)
    {
        UpdateCommerceDataWithEnumeratedContent( m_pMarketplaceOffers[i]->qwOfferID, m_pMarketplaceOffers[i]->fUserHasPurchased != 0 );
    }

    for ( int s=0; s <  m_CommerceData.GetSizeOfDataArray(); s++ )
    {
        cCommerceItemData* itemData = m_CommerceData.GetItemData( s );

        //By default the dependencys are set to having been found
        itemData->SetDependencyFlag( true );

        for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
        {
            //There is a dependency, set the flag to false
            itemData->SetDependencyFlag( false );

            ULONGLONG dependencyOfferId = ProductDataStringToOfferId( itemData->GetDependencyIds()[ iDeps ]  );

            for ( int iOffers = 0; iOffers < m_NumEnumeratedOffers; iOffers++ )
            {
                
                
                if (  m_pMarketplaceOffers[ iOffers ]->qwOfferID == dependencyOfferId && m_pMarketplaceOffers[ iOffers ]->fUserHasPurchased )
                {
                    //Found this dependency, set the flag and get out.
                    itemData->SetDependencyFlag( true );
                    break;
                }
            }
        }

        //By default enumeration dependencies are set to true
        itemData->SetEnumDependencyFlag( true );

        for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
        {
            //There is a dependency, set the flag to false
            itemData->SetEnumDependencyFlag( false );

            ULONGLONG enumDependencyOfferId = ProductDataStringToOfferId( itemData->GetEnumDependencyIds()[ iEnumDeps ] );

            for ( int iOffers = 0; iOffers < m_NumEnumeratedOffers; iOffers++ )
            {
                
                if ( m_pMarketplaceOffers[ iOffers ]->qwOfferID == enumDependencyOfferId )
                {
                    //Found this enumeration dependency, set the flag and get out.
                    itemData->SetEnumDependencyFlag( true );
                    break;
                }
            }
        }

        //By default the exclusions flag is set to false (no exclusions found)
        itemData->SetExclusionFlag( false );
       
        for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
        {
            ULONGLONG exclusionOfferId = ProductDataStringToOfferId( itemData->GetExclusionIds()[ iExcs ]  );

            for ( int iOffers = 0; iOffers < m_NumEnumeratedOffers; iOffers++ )
            {               
                if ( m_pMarketplaceOffers[ iOffers ]->qwOfferID == exclusionOfferId && m_pMarketplaceOffers[ iOffers ]->fUserHasPurchased )
                {
                    //Found this exclusion, set the flag and get out.
                    itemData->SetExclusionFlag( true );
                    break;
                }
            }
        }

        //By default the enum exclusions flag is set to false (no exclusions found)
        itemData->SetEnumExclusionFlag( false );

        for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
        {
            ULONGLONG enumExclusionOfferId = ProductDataStringToOfferId( itemData->GetEnumExclusionIds()[ iEnumExcs ]  );

            for ( int iOffers = 0; iOffers < m_NumEnumeratedOffers; iOffers++ )
            {               
                if ( m_pMarketplaceOffers[ iOffers ]->qwOfferID == enumExclusionOfferId && m_pMarketplaceOffers[ iOffers ]->fUserHasPurchased )
                {
                    //Found this exclusion, set the flag and get out.
                    itemData->SetEnumExclusionFlag( true );
                    break;
                }
            }
        }
    }
}

void cXboxCommerceManager::PopulateOffersToEnumerateArray()
{
    m_NumOfferIdsToManuallyEnumerate = 0;
   //Loop through all of the ros products and add all of the IDs they have.

    for (int s = 0; s < m_CommerceData.GetSizeOfDataArray(); s++ )
    {
        cCommerceItemData* itemData = m_CommerceData.GetItemData( s );

        for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
        {
            if ( !IsOfferIdAllreadyOnManualCheckList( ProductDataStringToOfferId( itemData->GetDependencyIds()[ iDeps ] ) ) )
            {
                if ( m_NumOfferIdsToManuallyEnumerate == MAX_NUM_MANUAL_ENUMERATION_IDS )
                {
                    commerceAssertf(false,"Too many offerIds to manually enumerate. Increase MAX_NUM_MANUAL_ENUMERATION_IDS");
                    return;
                }
                m_OfferIdsToManuallyEnumerate[ m_NumOfferIdsToManuallyEnumerate++ ] = ProductDataStringToOfferId( itemData->GetDependencyIds()[ iDeps ] );
            }
        }

        for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
        {
            if ( !IsOfferIdAllreadyOnManualCheckList( ProductDataStringToOfferId( itemData->GetEnumDependencyIds()[ iEnumDeps ] ) ) )
            {
                if ( m_NumOfferIdsToManuallyEnumerate == MAX_NUM_MANUAL_ENUMERATION_IDS )
                {
                    commerceAssertf(false,"Too many offerIds to manually enumerate. Increase MAX_NUM_MANUAL_ENUMERATION_IDS");
                    return;
                }
                m_OfferIdsToManuallyEnumerate[ m_NumOfferIdsToManuallyEnumerate++ ] = ProductDataStringToOfferId( itemData->GetEnumDependencyIds()[ iEnumDeps ] );
            }
        }

        for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
        {
            if ( !IsOfferIdAllreadyOnManualCheckList( ProductDataStringToOfferId( itemData->GetExclusionIds()[ iExcs ] ) ) )
            {
                if ( m_NumOfferIdsToManuallyEnumerate == MAX_NUM_MANUAL_ENUMERATION_IDS )
                {
                    commerceAssertf(false,"Too many offerIds to manually enumerate. Increase MAX_NUM_MANUAL_ENUMERATION_IDS");
                    return;
                }
                m_OfferIdsToManuallyEnumerate[ m_NumOfferIdsToManuallyEnumerate++ ] = ProductDataStringToOfferId( itemData->GetExclusionIds()[ iExcs ] );
            }
        }

        for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
        {
            if ( !IsOfferIdAllreadyOnManualCheckList( ProductDataStringToOfferId( itemData->GetEnumExclusionIds()[ iEnumExcs ] ) ) )
            {
                if ( m_NumOfferIdsToManuallyEnumerate == MAX_NUM_MANUAL_ENUMERATION_IDS )
                {
                    commerceAssertf(false,"Too many offerIds to manually enumerate. Increase MAX_NUM_MANUAL_ENUMERATION_IDS");
                    return;
                }
                m_OfferIdsToManuallyEnumerate[ m_NumOfferIdsToManuallyEnumerate++ ] = ProductDataStringToOfferId( itemData->GetEnumExclusionIds()[ iEnumExcs ] );
            }
        }

        //Only products have actual ids
        if ( itemData->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
        {
            continue; 
        }

        
        cCommerceProductData* productData = static_cast<cCommerceProductData*>( m_CommerceData.GetItemData( s ) );
        
        if ( !IsOfferIdAllreadyOnManualCheckList( ProductDataStringToOfferId( productData->GetPlatformId() ) ) )
        {
            if ( m_NumOfferIdsToManuallyEnumerate == MAX_NUM_MANUAL_ENUMERATION_IDS )
            {
                commerceAssertf(false,"Too many offerIds to manually enumerate. Increase MAX_NUM_MANUAL_ENUMERATION_IDS");
                return;
            }
            m_OfferIdsToManuallyEnumerate[ m_NumOfferIdsToManuallyEnumerate++ ] = ProductDataStringToOfferId( productData->GetPlatformId() ); 
        }
    }
}

void cXboxCommerceManager::DoPlatformLevelDebugDump()
{
    if (!m_Initialised)
    {
        commerceDebugf1("Attempted Xbox commerce platform data dump. Platform commerce not initialised.\n");
        return;
    }

    commerceDebugf1("Xbox commerce platform data dump.\n");
    commerceDebugf1("Number of enumerated offers: %d\n", m_NumEnumeratedOffers);
    commerceDebugf1("Number of offerIDs to manually enumerate: %d\n", m_NumOfferIdsToManuallyEnumerate);
}

} //namespace rage

#endif