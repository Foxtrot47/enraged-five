#ifndef XBOXCOMMERCEUTIL
#define XBOXCOMMERCEUTIL

#if __XENON

#pragma once

#include "system/ipc.h"


#include "../CommerceUtil.h"

//A brief summary of the XboxCommerceManager
//
//Due to the limited functionality given to us by the MS commerce backend related to filtering products based on existing product ownership
//we drive the product listing for the xbox using the commerce config files stored on R* servers, and then tie the product lines in these R* config
//files to the actual products hosted on the MS backend using the OfferId.

//First the config file(s) are checked for offerIds in the linked Id, dependant Id, and excluded Id fields.
//These IDs are used to build an enumeration list (a significant optimisation over automatically enumerating all products on the MS backend).
//Once a list of MS backend products has been enumerated and stored in m_pMarketplaceOffers the function PopulateMetadataToShowArray() is called to
//fill the array of products to actually display to the game.

//This is done by runnign through three criterea, with increasing priority: 
//1) Does this product line have a dependancy, and is that dependancy mounted. If so, show this product line.
//2) Does this product line have an exclusion, and is that exclusion enumerated. If so, do not display this product line.
//3) Is a product already being shown with the same productIdentifier as this product line. If so, do not display this product line.


//Avoid having to include xtl.h
struct _XMARKETPLACE_CURRENCY_CONTENTOFFER_INFO;
typedef _XMARKETPLACE_CURRENCY_CONTENTOFFER_INFO XMARKETPLACE_CURRENCY_CONTENTOFFER_INFO, *PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO;

struct _XOVERLAPPED;
typedef _XOVERLAPPED *PXOVERLAPPED;

typedef void*             HANDLE;

typedef long HRESULT;

typedef unsigned __int64 ULONGLONG;

namespace rage
{

class cXboxCommerceManager : public cCommerceUtil
{
public:
    
	static cXboxCommerceManager* Instance();
    ~cXboxCommerceManager();
   
    virtual bool Init( int userIndex = 0, const char *skuFolder = NULL );
    void Shutdown();
    void Update();

    bool DoPlatformDataRequest();
   
    /*
    const char *GetProductDescription( unsigned a_Index ) const;
    const char *GetProductLongDescription( unsigned a_Index );
    const char *GetProductName( unsigned a_Index ) const;
    const char * GetProductId( unsigned a_Index ) const;
    */

    bool IsContentInfoPopulated() const;

    int GetNumProducts() const;

    bool IsProductPurchased( int a_Index ) const;
    bool IsProductPurchased( const cCommerceProductData* a_pProductData ) const;

    bool IsProductReleased( int a_Index ) const;

    bool IsProductPurchasable( int a_Index ) const;
    bool IsProductPurchasable( const cCommerceProductData* a_pProductData) const;
    
    void CheckoutProduct( unsigned a_Index );
    void CheckoutProduct( const cCommerceProductData* );

    bool IsInCheckout() const { return ( m_CurrentState == MODE_CHECKOUT ); }

    bool ProductCodeEntry();

    float GetProductPrice( unsigned a_Index, char* a_pOutputString, unsigned a_SizeOfOutputString ) const;
    float GetProductPrice( const cCommerceProductData*, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const;

    void ParseCommerceFiles();

    void SetCurrentCommerceUserIndex( int a_UserIndex ) { m_CurrentCommerceUserIndex = a_UserIndex; }
    
    void AbortDataFetch();

    bool IsPlatformInDataFetchState() const { return ( m_CurrentState == MODE_ENUMERATE_CONTENT || m_CurrentState == MODE_ENUMERATE_MANUAL_CONTENT ); }

    void DoPlatformLevelDebugDump();

protected:
    virtual void MergePlatformAndRosData();

private:

	cXboxCommerceManager();

	//Static singleton instance of this class
	static cXboxCommerceManager* m_Instance;

    void AddOffer( const PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO a_Offer );
    void FreeOffers( );

    enum eCommerceMode {
        MODE_TOP = 0,
        MODE_ENUMERATE_CONTENT,
        MODE_ENUMERATE_MANUAL_CONTENT,
        MODE_ERROR_ENCOUNTERED,
        MODE_NEUTRAL,
        MODE_CHECKOUT,
        MODE_MAX
    };

    eCommerceMode m_CurrentState;

    void SetupAutoEnumeration();
    void SetupManualEnumeration();
    void UpdateEnumerateContent();
    void UpdateCheckout();

    bool IsOfferIdAllreadyOnManualCheckList( u64 offerIdToCheck );
    void UpdateCommerceDataWithEnumeratedContent( u64 offerId, bool a_IsOwned );

   // void PopulateMetadataToShowArray();
    //bool IsProductReleasedFromMetadataIndex( int a_MetaDataIndex ) const;
    //bool IsProductReleasedFromMetadata( const OfferMetaData & a_MetaData ) const;

    int GetCurrentCommerceUserIndex() { return m_CurrentCommerceUserIndex; }

    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO GetOfferInfoForIndex( u32 a_Index ) const;
    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO GetOfferInfo( const cCommerceProductData* /*pProductInfo*/ ) const;
    
    ULONGLONG ProductDataStringToOfferId( const atString & ) const;
    void PopulateOffersToEnumerateArray();

    //bool ShouldMetaDataShow( const OfferMetaData &a_MetaData, bool useMSFix );
    
	static void AbortDataFetchThread( void * );
	bool IsAbortInProgress() { return m_DataFetchAbortInProgress; } 

    enum eLicenseMaskFields
    {
        CONTROL_BITS_MASK =             0xF0000000,
        OFFER_IDENTIFICATION_MASK =     0x0FF00000,
        OFFER_REQUIRED_MASK =           0x000FF000,
        OFFER_EXCLUDED_PACKAGE_MASK =   0x00000FF0
    };

    enum
    {
        COMING_SOON_CATEGORY = 0x80000000
    };

    enum
    {
        MAX_NUM_OFFERS = 64,
        NUM_OFFERS_PER_ENUM_PASS = 15,
        MAX_NUM_MANUAL_ENUMERATION_IDS = 50,
        MAX_NUM_META_DATA_RECORDS = 30,
    };

    PXMARKETPLACE_CURRENCY_CONTENTOFFER_INFO m_pMarketplaceOffers[ MAX_NUM_OFFERS ];

    HANDLE      m_hEnumeration; // Enumeration handle
    PXOVERLAPPED m_pOverlapped;   // Overlapped structure for asynchronous I/O
    PXOVERLAPPED m_pCheckoutOverlapped;
    unsigned       m_dwBufferSize; // Size of the enumeration buffer
    unsigned char*       m_pBuffer;      // Pointer to the enumeration buffer
    bool        m_bEnumerateNext;
    HRESULT     m_hrErrorStatus;  // Error status of the current enumeration

    bool m_ContentEnumerationInProgress;
    bool m_ContentInfoPopulated;
    int m_NumEnumeratedOffers;

    u64 m_OfferIdsToManuallyEnumerate[ MAX_NUM_MANUAL_ENUMERATION_IDS ];
    unsigned short m_NumOfferIdsToManuallyEnumerate;
    
    int m_NumShownMetaDataRecords;

    int m_CurrentCommerceUserIndex;

    bool m_Initialised;

	bool m_DataFetchAbortInProgress;
};

}

#endif //__XENON

#endif
