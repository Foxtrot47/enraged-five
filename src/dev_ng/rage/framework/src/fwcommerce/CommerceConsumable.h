#ifndef COMMERCECONSUMABLE_H
#define COMMERCECONSUMABLE_H

#include "atl/map.h"

namespace rage
{

const int COMMERCE_CONSUMABLE_LEVEL_ERROR = -1;

typedef int consumableTransactionId;
const consumableTransactionId TRANSACTION_ID_ERROR = -1;
const consumableTransactionId TRANSACTION_ID_UNASSIGNED = -1;

class cCommerceConsumableTransaction
{
public:
    cCommerceConsumableTransaction();
    virtual ~cCommerceConsumableTransaction();

    virtual void Init();
    virtual void Shutdown();
    virtual void Update();
    virtual bool HasCompleted() const;
    virtual bool HasSucceeded() const;
    virtual void Reset();

    virtual void Cancel() {}

    virtual bool StartTransaction( const char* consumableIdentifier, int amountToConsume );

    consumableTransactionId GetId() const { return m_Id; }

    //To support multiple different consumes per request.
    virtual void AddConsumeInstance( const char*, s64, int ) {}

    void SetCurrentConsumableUser( int userIndex ) { m_CurrentConsumableUser = userIndex; }

protected:
    int GetCurrentConsumableUser() { return m_CurrentConsumableUser; } 

private:

	int m_CurrentConsumableUser;
    consumableTransactionId m_Id;

    static consumableTransactionId m_NextTransactionId;
};

class cCommerceConsumableManager
{
public:

    static cCommerceConsumableManager* Instance();
    virtual ~cCommerceConsumableManager();

    virtual void Init();
	virtual void Shutdown() {}

    virtual void Update();

    virtual consumableTransactionId Consume( const char* consumableIdentifier, int amountToConsume );

    virtual bool HasTransactionCompleted ( consumableTransactionId transactionId ) const;
    virtual  bool WasTransactionSuccessful ( consumableTransactionId transactionId ) const ;

    virtual int GetConsumableLevel( const char* /*aConsumableId*/ ) { return 0; }
    virtual bool IsConsumableKnown( const char* /*aConsumableId*/) { return false; }

    void SetCurrentConsumableUser( int userIndex ) { m_CurrentConsumableUser = userIndex; }
     
    virtual bool StartOwnershipDataFetch() { return true; }
    virtual bool IsOwnershipDataPopulated() const { return !m_CurrentOwnershipDataDirty; }
	virtual bool IsOwnershipDataPending() const { return m_CurrentOwnershipDataDirty; }

    virtual void DumpCurrentOwnershipData() const;

	void SetOwnershipDataDirty();
    bool IsOwnershipDataDirty() const { return m_CurrentOwnershipDataDirty; }

    void CancelAllTransactions() { if (mp_CurrentTransaction) mp_CurrentTransaction->Cancel(); }

protected:
    cCommerceConsumableManager();
    int GetCurrentConsumableUser() { return m_CurrentConsumableUser; }
    virtual bool PreconsumeChecks();
    virtual consumableTransactionId SetupTransaction(const char* consumableIdentifier, int amountToConsume);

    cCommerceConsumableTransaction *mp_CurrentTransaction;
private:

    void UpdateEntitlementDataRefresh();
    
    enum
    {
        TRANSACTION_ERROR_ID = -2,
        INVALID_TRANSACTION_ID = -1
    };

    //Map to store the results of past transactions.
    atMap<int,bool> m_TransactionResultMap;

    static cCommerceConsumableManager *mp_BaseInstance;
    int m_CurrentConsumableUser;

    bool m_CurrentOwnershipDataDirty;
};

cCommerceConsumableManager* CommerceConsumablesInstance(bool forceBase = false);

}

#endif //COMMERCECONSUMABLE_H