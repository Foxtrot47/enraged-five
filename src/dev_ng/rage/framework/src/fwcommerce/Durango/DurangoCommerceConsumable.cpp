#if RSG_DURANGO

#include "DurangoCommerceConsumable.h"
#include "../CommerceChannel.h"


//implementation notes

//InventoryService.ConsumeInventoryItemAsync for the actual consumption
//InventoryService.GetInventoryItemsAsync
//
// ConsumeInventoryItemAsync needs a ^Inventory, so we will need to keep those refs (thanks MS.)

namespace rage
{
sysCriticalSectionToken cDurangoCommerceConsumableManager::sm_ConstrainedCs;
ServiceDelegate cDurangoCommerceConsumableManager::sm_Delegate;
bool cDurangoCommerceConsumableManager::sm_IsGameConstrained = false;
cDurangoCommerceConsumableManager* cDurangoCommerceConsumableManager::mp_Instance = NULL;

cDurangoCommerceConsumableManager* cDurangoCommerceConsumableManager::Instance()
{
	if ( mp_Instance == NULL )
	{
		mp_Instance = rage_new cDurangoCommerceConsumableManager;
	}
	return mp_Instance;
}

cDurangoCommerceConsumableManager::cDurangoCommerceConsumableManager() :
	m_CurrentAssetDataState(ASSET_DATA_STATE_UNINITIALISED),
	m_HasValidData(false)
{
	mp_CurrentTransaction = rage_new cDurangoCommerceConsumableTransaction;

}

cDurangoCommerceConsumableManager::~cDurangoCommerceConsumableManager()
{

}

void cDurangoCommerceConsumableManager::Init()
{
	m_CurrentAssetDataState = ASSET_DATA_STATE_UNINITIALISED;
	sm_Delegate.Bind(&cDurangoCommerceConsumableManager::HandleConstrainedChanged);
	g_SysService.AddDelegate(&sm_Delegate);
	cCommerceConsumableManager::Init();
}

void cDurangoCommerceConsumableManager::Shutdown()
{
	g_SysService.RemoveDelegate(&sm_Delegate);
	sm_Delegate.Reset();
}

void cDurangoCommerceConsumableManager::HandleConstrainedChanged( sysServiceEvent* evt )
{
	if(evt != NULL)
	{
		if(evt->GetType() == sysServiceEvent::CONSTRAINED)
		{
			sysCriticalSection lock(sm_ConstrainedCs);
			sm_IsGameConstrained = true;
		}
		else if(evt->GetType() == sysServiceEvent::UNCONSTRAINED)
		{
			sysCriticalSection lock(sm_ConstrainedCs);
			sm_IsGameConstrained = false;
			if (!Instance()->IsOwnershipDataDirty())
			{
				Instance()->SetOwnershipDataDirty();				
			}
		}
	}
}
void cDurangoCommerceConsumableManager::Update()
{
	switch(m_CurrentAssetDataState)
	{
	case(ASSET_DATA_FETCHING_DATA_DURABLE):
		UpdateDurableInventoryEnumerate();
		break;
	case(ASSET_DATA_FETCHING_DATA_CONSUMABLE):
		UpdateConsumableInventoryEnumerate();
		break;
	default:
		break;
	}

	cCommerceConsumableManager::Update();
}

int cDurangoCommerceConsumableManager::GetConsumableLevel( const char* aConsumableId )
{
	if ( ASSET_DATA_STATE_DATA_POPULATED != m_CurrentAssetDataState )
	{
		return 0;
	}

	if ( aConsumableId == NULL )
	{
		return 0;
	}

	int foundIndex = -1;

	for (int i=0; i < m_PlatformInventoryData.GetInventoryItemArray().GetCount(); i++)
	{
		if ( strcmp(aConsumableId, m_PlatformInventoryData.GetInventoryItemArray()[i].GetProductId()) == 0 )
		{
			foundIndex = i;
			break;
		}
	}

	if (foundIndex == -1)
	{
		return 0;
	}
	else
	{
		return m_PlatformInventoryData.GetInventoryItemArray()[foundIndex].GetConsumableBalance();
	}
}

bool cDurangoCommerceConsumableManager::IsConsumableKnown( const char* aConsumableId)
{
    if ( ASSET_DATA_STATE_DATA_POPULATED != m_CurrentAssetDataState )
    {
        return 0;
    }

    if ( aConsumableId == NULL )
    {
        return 0;
    }

    int foundIndex = -1;

    for (int i=0; i < m_PlatformInventoryData.GetInventoryItemArray().GetCount(); i++)
    {
        if ( strcmp(aConsumableId, m_PlatformInventoryData.GetInventoryItemArray()[i].GetProductId()) == 0 )
        {
            foundIndex = i;
            break;
        }
    }

    if (foundIndex == -1)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool cDurangoCommerceConsumableManager::StartOwnershipDataFetch()
{
	commerceDebugf1("StartOwnershipDataFetch");
	StartInventoryFetch();
	return true;
}

bool cDurangoCommerceConsumableManager::IsOwnershipDataPending() const
{
	bool isPendingState = (m_CurrentAssetDataState == ASSET_DATA_FETCHING_DATA_DURABLE) || (m_CurrentAssetDataState == ASSET_DATA_FETCHING_DATA_CONSUMABLE);
	return isPendingState || cCommerceConsumableManager::IsOwnershipDataPending();
}

void cDurangoCommerceConsumableManager::DumpCurrentOwnershipData() const
{

}

void cDurangoCommerceConsumableManager::StartInventoryFetch()
{
	if (m_PlayerInventoryFetchStatus.Pending())
	{
		return;
	}

	commerceDebugf1("StartInventoryFetch :: Requesting Durable Inventory Items");

	//Clear existing data
	m_PlatformInventoryData.Reset();
	m_PlayerInventoryFetchStatus.Reset();

	//Fetch the durables first.
	g_rlXbl.GetPlatformCommerceManager()->GetInventoryItems(GetCurrentConsumableUser(), &m_PlatformInventoryData, COMMERCE_INVENTORY_ITEM_TYPE_DURABLE, &m_PlayerInventoryFetchStatus);

	m_CurrentAssetDataState = ASSET_DATA_FETCHING_DATA_DURABLE;
}

void cDurangoCommerceConsumableManager::UpdateDurableInventoryEnumerate()
{
	if (m_PlayerInventoryFetchStatus.Pending())
	{
		return;
	}

	commerceDebugf1("UpdateDurableInventoryEnumerate :: Requesting Consumble Inventory Items");

	//Reset the status
	m_PlayerInventoryFetchStatus.Reset();

	//Start the consumable search
	g_rlXbl.GetPlatformCommerceManager()->GetInventoryItems(GetCurrentConsumableUser(), &m_PlatformInventoryData, COMMERCE_INVENTORY_ITEM_TYPE_CONSUMABLE, &m_PlayerInventoryFetchStatus);

	m_CurrentAssetDataState = ASSET_DATA_FETCHING_DATA_CONSUMABLE;
}

void cDurangoCommerceConsumableManager::UpdateConsumableInventoryEnumerate()
{
	if (m_PlayerInventoryFetchStatus.Pending())
	{
		return;
	}

	commerceDebugf1("UpdateConsumableInventoryEnumerate :: Completed Inventory Requests");

	//Reset the status
	m_PlayerInventoryFetchStatus.Reset();

	m_HasValidData = true;
	m_CurrentAssetDataState = ASSET_DATA_STATE_DATA_POPULATED;
}

cDurangoCommerceConsumableTransaction::cDurangoCommerceConsumableTransaction() :
	m_CurrentTransactionState(DURANGO_TRANSACTION_UNINITIALISED)
{
	m_ConsumableId.Reset();
	m_DoConsumptionStatus.Reset();
}

void cDurangoCommerceConsumableTransaction::Init()
{
	m_CurrentTransactionState = DURANGO_TRANSACTION_UNINITIALISED;
	m_ConsumableId.Reset();
	m_DoConsumptionStatus.Reset();
	cCommerceConsumableTransaction::Init();
}

bool cDurangoCommerceConsumableTransaction::StartTransaction( const char* consumableIdentifier, int amountToConsume )
{
	if (consumableIdentifier == NULL)
	{
		return false;
	}

	if ( amountToConsume <= 0 )
	{
		//You want to consume nothing or less than nothing?!?
		return false;
	}

	if ( m_DoConsumptionStatus.Pending() )
	{
		return false;
	}

	//START THE CONSUMPTION
	g_rlXbl.GetPlatformCommerceManager()->ConsumeFromItem( GetCurrentConsumableUser(), consumableIdentifier, amountToConsume, &m_DoConsumptionStatus  );

	m_ConsumableId = consumableIdentifier;
	m_CurrentTransactionState = DURANGO_TRANSACTION_PENDING;
	return cCommerceConsumableTransaction::StartTransaction(consumableIdentifier,amountToConsume);
}

void cDurangoCommerceConsumableTransaction::Update()
{
	switch(m_CurrentTransactionState)
	{
	case(DURANGO_TRANSACTION_PENDING):
		UpdatePending();
	default:
		break;
	}

	cCommerceConsumableTransaction::Update();
}  

void cDurangoCommerceConsumableTransaction::UpdatePending()
{
	if (m_DoConsumptionStatus.Pending())
	{
		return;
	}

	if ( m_DoConsumptionStatus.Succeeded() )
	{
		m_CurrentTransactionState = DURANGO_TRANSACTION_COMPLETE;
		return;
	}
	else
	{
		commerceErrorf("Error consuming entitlement [%s]",m_ConsumableId.c_str());
		m_CurrentTransactionState = DURANGO_TRANSACTION_ERROR;
		return;
	}
}

}

#endif