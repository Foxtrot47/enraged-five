#ifndef DURANGOCOMMERCECONSUMABLE
#define DURANGOCOMMERCECONSUMABLE

#if RSG_DURANGO

namespace rage
{
	class cDurangoCommerceConsumableTransaction : public cCommerceConsumableTransaction
	{
	public:
		cDurangoCommerceConsumableTransaction();

		void Init();
		void Update();

		bool StartTransaction( const char* consumableIdentifier, int amountToConsume );

		bool HasCompleted() const { return ((m_CurrentTransactionState == DURANGO_TRANSACTION_COMPLETE)||(m_CurrentTransactionState == DURANGO_TRANSACTION_ERROR)); }
		bool HasSucceeded() const { return (m_CurrentTransactionState == DURANGO_TRANSACTION_COMPLETE);}

		void SetConsumingUserId( int userId ) { m_ConsumingUser = userId; }

	private:
		void UpdatePending();

		enum eDurangoTransactionState
		{
			DURANGO_TRANSACTION_UNINITIALISED,
			DURANGO_TRANSACTION_PENDING,
			DURANGO_TRANSACTION_COMPLETE,
			DURANGO_TRANSACTION_ERROR
		};

		eDurangoTransactionState m_CurrentTransactionState;

		netStatus m_DoConsumptionStatus;
		atString m_ConsumableId;

		int m_ConsumingUser;
	};

class cDurangoCommerceConsumableManager : public cCommerceConsumableManager
{
public:
	static cDurangoCommerceConsumableManager* Instance();
	static sysCriticalSectionToken sm_ConstrainedCs;
	static ServiceDelegate sm_Delegate;
	static bool sm_IsGameConstrained;
	static void HandleConstrainedChanged( sysServiceEvent* evt );
	virtual ~cDurangoCommerceConsumableManager();

	void Init();
	void Shutdown();
	void Update();

	//returns the consumable level. Returns -1 on error.
	int GetConsumableLevel( const char* aConsumableId );
    bool IsConsumableKnown( const char* aConsumableId);

	bool StartOwnershipDataFetch();
	bool IsOwnershipDataPending() const;
	bool IsOwnershipDataPopulated() const { return ((m_CurrentAssetDataState == ASSET_DATA_STATE_DATA_POPULATED) && cCommerceConsumableManager::IsOwnershipDataPopulated()); }

	void DumpCurrentOwnershipData() const;
protected:
	cDurangoCommerceConsumableManager();

private:
	//Static singleton instance of this class
	static cDurangoCommerceConsumableManager* mp_Instance;

	void StartInventoryFetch();
	void UpdateDurableInventoryEnumerate();
	void UpdateConsumableInventoryEnumerate();

	enum eDurangoAssetDataState
	{
		ASSET_DATA_STATE_UNINITIALISED,
		ASSET_DATA_FETCHING_DATA_DURABLE,
		ASSET_DATA_FETCHING_DATA_CONSUMABLE,
		ASSET_DATA_STATE_DATA_POPULATED,
		ASSET_DATA_STATE_ERROR,
		ASSET_DATA_STATE_NUM_STATES
	};

	eDurangoAssetDataState m_CurrentAssetDataState;

	bool m_HasValidData;

	netStatus m_PlayerInventoryFetchStatus;
	cCommercePlatformInventoryDetails m_PlatformInventoryData;
};

}

#endif //RSG_DURANGO

#endif //DURANGOCOMMERCECONSUMABLE

