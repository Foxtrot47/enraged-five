#if RSG_DURANGO

#include "DurangoCommerceUtil.h"
#include "../CommerceChannel.h"

#include "rline/rlxbl.h"
#include "net/task.h"

PARAM(netCommerceUseCurrencyCode, "[commerce] Always replace display price with the currency string");

namespace rage
{

const float INVALID_DURANGO_COMMERCE_PRICE_VALUE = -1.0f;

cDurangoCommerceManager* cDurangoCommerceManager::m_Instance = NULL;

cDurangoCommerceManager::cDurangoCommerceManager() :
	m_Initialised(false),
	m_LocalGamerIndex(0),
	m_CurrentMode(MODE_TOP),
	m_ContentInfoPopulated(false),
	m_CurrentDetailFetchBaseIndex(0)
{
	m_CatalogFetchStatus.Reset();
	m_CatalogDetailFetchStatus.Reset();
	m_CheckoutStatus.Reset();
	m_CodeEntryStatus.Reset();
    m_ConsumableCatalogFetchStatus.Reset();
    m_PlayerDurableInventoryFetchStatus.Reset();
    m_RootProductIdentifier.Reset();
}

cDurangoCommerceManager::~cDurangoCommerceManager()
{
	Shutdown();

	m_Instance = NULL;
}

bool cDurangoCommerceManager::Init( int userIndex, const char* skuFolder )
{
	m_LocalGamerIndex = userIndex;
	m_ContentInfoPopulated = false;
	m_CurrentMode = MODE_TOP;

	m_CatalogFetchStatus.Reset();
	m_CatalogDetailFetchStatus.Reset();
	m_CheckoutStatus.Reset();
	m_CodeEntryStatus.Reset();
    m_ConsumableCatalogFetchStatus.Reset();
    m_PlayerDurableInventoryFetchStatus.Reset();
	m_CurrentDetailFetchBaseIndex = 0;

	m_Initialised = true;

	return cCommerceUtil::Init(userIndex, skuFolder);
}

void cDurangoCommerceManager::Shutdown()
{
	AbortDataFetch();

	if (m_CheckoutStatus.Pending())
	{
		netTask::Cancel(&m_CheckoutStatus);
	}
	
	if (m_CodeEntryStatus.Pending())
	{
		netTask::Cancel(&m_CodeEntryStatus);
	}

    if (m_PlayerDurableInventoryFetchStatus.Pending())
    {
        netTask::Cancel(&m_PlayerDurableInventoryFetchStatus);
    }
}

cDurangoCommerceManager* cDurangoCommerceManager::Instance()
{
	if ( m_Instance == NULL )
	{
		m_Instance = rage_new cDurangoCommerceManager;
	}

	return m_Instance;
}

bool cDurangoCommerceManager::DoPlatformDataRequest()
{
	commerceAssertf(!m_CatalogFetchStatus.Pending(),"Dont request platform data whilst a previous request is in progress");

	if ( m_CatalogFetchStatus.Pending() )
	{
		return false;
	}

	m_CatalogFetchStatus.Reset();
	m_PlatformItemData.Reset();

	g_rlXbl.GetPlatformCommerceManager()->GetCatalog(m_LocalGamerIndex, m_RootProductIdentifier.c_str(), &m_PlatformItemData, COMMERCE_INVENTORY_ITEM_TYPE_DURABLE, &m_CatalogFetchStatus);

    //Start an inventory fetch alongside this.
    m_PlatformDurableInventoryData.Reset();
    g_rlXbl.GetPlatformCommerceManager()->GetInventoryItems(m_LocalGamerIndex, &m_PlatformDurableInventoryData, COMMERCE_INVENTORY_ITEM_TYPE_DURABLE, &m_PlayerDurableInventoryFetchStatus);

	m_CurrentMode = MODE_ENUMERATE_CONTENT_DURABLE;

	return true;
}

void cDurangoCommerceManager::Update()
{
	cCommerceUtil::Update();

	switch(m_CurrentMode)
	{
	case(MODE_ENUMERATE_CONTENT_DURABLE):
		UpdateEnumerateContentDurable();
		break;
	case(MODE_ENUMERATE_CONTENT_CONSUMABLE):
		UpdateEnumerateContentConsumable();
		break;
	case(MODE_ENUMERATE_CONTENT_DETAIL):
		UpdateEnumerateContentDetail();
		break;
    case(MODE_ENUMERATE_INVENTORY):
        UpdateEnumerateInventory();
        break;
	case(MODE_CHECKOUT):
		UpdateCheckout();
		break;
	case(MODE_PROMOCODE):
		UpdateCodeEntry();
		break;
	default:
		break;
	}	
}

void cDurangoCommerceManager::UpdateEnumerateContentDurable()
{
	if ( m_CatalogFetchStatus.Pending() )
	{
		return;
	}

	if ( m_CatalogFetchStatus.Failed() )
	{
		m_CurrentMode = MODE_ERROR_ENCOUNTERED;
		m_ContentInfoPopulated = false;
		commerceErrorf("Population of platform commerce data failed.");
		return;
	}

	m_CatalogFetchStatus.Reset();
    m_ConsumableCatalogFetchStatus.Reset();

	g_rlXbl.GetPlatformCommerceManager()->GetCatalog(m_LocalGamerIndex, m_RootProductIdentifier.c_str(), &m_PlatformItemData, COMMERCE_INVENTORY_ITEM_TYPE_CONSUMABLE, &m_ConsumableCatalogFetchStatus);

	m_CurrentMode = MODE_ENUMERATE_CONTENT_CONSUMABLE;
}

void cDurangoCommerceManager::UpdateEnumerateContentConsumable()
{
    if ( m_ConsumableCatalogFetchStatus.Pending() )
	{
		return;
	}

	if ( m_ConsumableCatalogFetchStatus.Failed() )
	{
		m_CurrentMode = MODE_ERROR_ENCOUNTERED;
		m_ContentInfoPopulated = false;
		commerceErrorf("Population of platform commerce data failed.");
		return;
	}

	//Now we do a pass on all products to get details
	int numToFetch = MAX_PRODUCT_DETAILS_TO_FETCH;

	if ( m_PlatformItemData.GetItemArray().GetCount() < MAX_PRODUCT_DETAILS_TO_FETCH )
	{
		numToFetch = m_PlatformItemData.GetItemArray().GetCount();
	}

	if ( numToFetch == 0 )
	{
		m_CurrentMode = MODE_NEUTRAL;
		m_ContentInfoPopulated = true;
	}
	else
	{
		g_rlXbl.GetPlatformCommerceManager()->GetCatalogDetails(m_LocalGamerIndex,  &m_PlatformItemData, 0, numToFetch, &m_CatalogDetailFetchStatus);

		m_CurrentDetailFetchBaseIndex = 0;

		m_CurrentMode = MODE_ENUMERATE_CONTENT_DETAIL;
	}
}

void cDurangoCommerceManager::UpdateEnumerateContentDetail()
{
	if ( m_CatalogDetailFetchStatus.Pending() )
	{
		return;
	}

	if ( m_CatalogFetchStatus.Failed() )
	{
		m_CurrentMode = MODE_ERROR_ENCOUNTERED;
		m_ContentInfoPopulated = false;
		commerceErrorf("Population of platform commerce *detail* data failed.");
		return;
	}

	//See if we need to go again with a higher start index.

	bool done = ( m_CurrentDetailFetchBaseIndex + MAX_PRODUCT_DETAILS_TO_FETCH >= m_PlatformItemData.GetItemArray().GetCount() );

	if ( done )
	{
		m_CurrentMode = MODE_ENUMERATE_INVENTORY;
	}
	else
	{
		m_CurrentDetailFetchBaseIndex += MAX_PRODUCT_DETAILS_TO_FETCH;
		int numToFetch = MAX_PRODUCT_DETAILS_TO_FETCH;

		if ( m_PlatformItemData.GetItemArray().GetCount() - m_CurrentDetailFetchBaseIndex < MAX_PRODUCT_DETAILS_TO_FETCH )
		{
			numToFetch = m_PlatformItemData.GetItemArray().GetCount() - m_CurrentDetailFetchBaseIndex;
		}

		g_rlXbl.GetPlatformCommerceManager()->GetCatalogDetails(m_LocalGamerIndex, &m_PlatformItemData, m_CurrentDetailFetchBaseIndex, numToFetch, &m_CatalogDetailFetchStatus);
	}
}

void cDurangoCommerceManager::UpdateEnumerateInventory()
{
    if ( m_PlayerDurableInventoryFetchStatus.Pending() )
    {
        return;
    }

    if ( m_CatalogFetchStatus.Failed() )
    {
        m_CurrentMode = MODE_ERROR_ENCOUNTERED;
        m_ContentInfoPopulated = false;
        commerceErrorf("Population of platform commerce inventory data failed.");
        return;
    }

    //We have an inventory, check for owned durables and fix up.
    //Iterate through ownerships, find a matching product, and set the appropriate flag.

    for (int i = 0; i < m_PlatformDurableInventoryData.GetInventoryItemArray().GetCount(); i++)
    {
        if ( !m_PlatformDurableInventoryData.GetInventoryItemArray()[i].GetOwned() )
        {
            continue;   
        }

        for ( int iItems = 0; iItems < m_PlatformItemData.GetItemArray().GetCount(); iItems++)
        {
            if (m_PlatformItemData.GetItemArray()[iItems].m_ProductId == m_PlatformDurableInventoryData.GetInventoryItemArray()[i].GetProductId())
            {
                m_PlatformItemData.GetItemArray()[iItems].m_IsOwned = true;
            }
        }
    }

    //Data fetch is done.
    m_CurrentMode = MODE_NEUTRAL;
    m_ContentInfoPopulated = true;
}

bool cDurangoCommerceManager::IsInErrorState() const
{
	bool retval = ( m_CurrentMode == MODE_ERROR_ENCOUNTERED );
	retval = retval || cCommerceUtil::IsInErrorState();
	return retval;
}

bool cDurangoCommerceManager::IsContentInfoPopulated() const
{
	return ( m_ContentInfoPopulated && cCommerceUtil::IsContentInfoPopulated() );
}

void cDurangoCommerceManager::UpdateCommerceDataWithEnumeratedContent( const char* idString, bool /*a_IsOwned*/  )
{
	for ( int i = 0; i < m_CommerceData.GetSizeOfDataArray(); i++ )
	{
		if ( m_CommerceData.GetItemData( i )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
		{
			//Not a product, ergo does not need to be checked.
			continue;
		}

		cCommerceProductData* productData = static_cast<cCommerceProductData*>(m_CommerceData.GetItemData( i ));

		if ( idString && strcmp(productData->GetPlatformId().c_str(), idString) == 0 )
		{
			productData->SetEnumeratedFlag( true );
		}
	}
}

void cDurangoCommerceManager::MergePlatformAndRosData()
{
	for ( int i=0; i< m_PlatformItemData.GetItemArray().GetCount(); i++)
	{
        //Since MS has a charming habit of returning malformed products, check that they really are giving us an actual product here.
        if ( m_PlatformItemData.GetItemArray()[i].m_SignedOfferId.GetLength() > 0  )
        {
            UpdateCommerceDataWithEnumeratedContent( m_PlatformItemData.GetItemArray()[i].m_Id, false );
        }
	}

	for ( int s=0; s <  m_CommerceData.GetSizeOfDataArray(); s++ )
	{
		cCommerceItemData* itemData = m_CommerceData.GetItemData( s );

		//By default the dependencys are set to having been found
		itemData->SetDependencyFlag( true );

		for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetDependencyFlag( false );

			for ( int iOffers = 0; iOffers < m_PlatformItemData.GetItemArray().GetCount(); iOffers++ )
			{
				if (  m_PlatformItemData.GetItemArray()[iOffers].m_Id == itemData->GetDependencyIds()[ iDeps ] && m_PlatformItemData.GetItemArray()[iOffers].m_IsOwned )
				{
					//Found this dependency, set the flag and get out.
					itemData->SetDependencyFlag( true );
					break;
				}
			}
		}

		//By default enumeration dependencies are set to true
		itemData->SetEnumDependencyFlag( true );

		for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetEnumDependencyFlag( false );

			for ( int iOffers = 0; iOffers < m_PlatformItemData.GetItemArray().GetCount(); iOffers++ )
			{

				if ( m_PlatformItemData.GetItemArray()[iOffers].m_Id == itemData->GetEnumDependencyIds()[ iEnumDeps ] )
				{
					//Found this enumeration dependency, set the flag and get out.
					itemData->SetEnumDependencyFlag( true );
					break;
				}
			}
		}

		//By default the exclusions flag is set to false (no exclusions found)
		itemData->SetExclusionFlag( false );

		for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
		{
			for ( int iOffers = 0; iOffers < m_PlatformItemData.GetItemArray().GetCount(); iOffers++ )
			{               
				if ( m_PlatformItemData.GetItemArray()[iOffers].m_Id == itemData->GetExclusionIds()[ iExcs ] && m_PlatformItemData.GetItemArray()[iOffers].m_IsOwned )
				{
					//Found this exclusion, set the flag and get out.
					itemData->SetExclusionFlag( true );
					break;
				}
			}
		}

		//By default the enum exclusions flag is set to false (no exclusions found)
		itemData->SetEnumExclusionFlag( false );

		for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
		{
			for ( int iOffers = 0; iOffers < m_PlatformItemData.GetItemArray().GetCount(); iOffers++ )
			{               
				if ( m_PlatformItemData.GetItemArray()[iOffers].m_Id == itemData->GetEnumExclusionIds()[ iEnumExcs ] )
				{
					//Found this exclusion, set the flag and get out.
					itemData->SetEnumExclusionFlag( true );
					break;
				}
			}
		}
	}
}

//----------------------------------------------------------

//I should consider moving this down to the base class

void cDurangoCommerceManager::CheckoutProduct( u32 a_Index )
{

	commerceAssert( m_CurrentMode == MODE_NEUTRAL );

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return;
	}

	return CheckoutProduct( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

void cDurangoCommerceManager::CheckoutProduct( const cCommerceProductData* a_pProductdata )
{
	commerceAssert(a_pProductdata != NULL);
	if ( a_pProductdata == NULL )
	{
		return;
	}

	if ( m_CurrentMode != MODE_NEUTRAL )
	{
		return;
	}

	if ( !a_pProductdata->GetEnumeratedFlag() )
	{
		return;
	}

	int foundIndex = -1;

	for (int i=0; i < m_PlatformItemData.GetItemArray().GetCount(); i++)
	{
		if ( a_pProductdata->GetPlatformId() == m_PlatformItemData.GetItemArray()[i].m_Id )
		{
			foundIndex = i;
			break;
		}
	}

	if ( foundIndex == -1 )
	{
		commerceErrorf("Trying to checkout product %s which does not have a corresponding Xbox One platform product with ID %s", a_pProductdata->GetId(), a_pProductdata->GetPlatformId());
		return;
	}

	if (!IsProductPurchasable(a_pProductdata))
	{
		commerceErrorf("Product has Xbox One platform item but the item has no offer");
		return;
	}



	//Do checkout.
	if (g_rlXbl.GetPlatformCommerceManager()->DoProductPurchaseCheckout( m_LocalGamerIndex, m_PlatformItemData.GetItemArray()[foundIndex].m_SignedOfferId, &m_CheckoutStatus))
	{
		m_CurrentMode = MODE_CHECKOUT;
	}
}

void cDurangoCommerceManager::UpdateCheckout()
{
	if ( m_CheckoutStatus.Pending() )
	{
		return;
	}

	if ( m_CheckoutStatus.Failed() )
	{
		commerceErrorf("Call to XB1 checkout failed");
		m_CurrentMode = MODE_NEUTRAL;
		return;
	}

	m_CurrentMode = MODE_NEUTRAL;
}

void cDurangoCommerceManager::UpdateCodeEntry()
{
	if ( m_CodeEntryStatus.Pending() )
	{
		return;
	}

	if ( m_CodeEntryStatus.Failed() )
	{
		commerceErrorf("Call to XB1 code entry failed");
		m_CurrentMode = MODE_NEUTRAL;
		return;
	}

	m_CurrentMode = MODE_NEUTRAL;
}

const cCommercePlatformItem* cDurangoCommerceManager::GetPlatformItem( const cCommerceProductData* a_pProductData ) const
{
	int foundItemIndex = -1;
	for (int i = 0; i < m_PlatformItemData.GetItemArray().GetCount(); i++)
	{
		if ( a_pProductData->GetPlatformId() == m_PlatformItemData.GetItemArray()[i].m_Id )
		{
			foundItemIndex = i;
			break;
		}
	}

	if ( foundItemIndex == -1 )
	{
		commerceErrorf("Trying to find a platform item for a commerce product that does not have one. Product RoS side ID is %s, platform ID is %s", a_pProductData->GetId().c_str(), a_pProductData->GetPlatformId().c_str());
		return NULL;
	}
	else
	{
		return &(m_PlatformItemData.GetItemArray()[foundItemIndex]);
	}		
}

//----------------------------------------------------------
bool cDurangoCommerceManager::IsProductPurchased( int a_Index ) const
{
    commerceAssert( IsContentInfoPopulated() );
    commerceAssert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );

    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }
            
    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    return IsProductPurchased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cDurangoCommerceManager::IsProductPurchased( const cCommerceProductData* a_pProductData ) const
{
	if (GetPlatformItem( a_pProductData ) == NULL)
	{
		return false;
	}

	return ( GetPlatformItem( a_pProductData )->m_IsOwned );
}

//----------------------------------------------------------
bool cDurangoCommerceManager::IsProductPurchasable( int a_Index ) const
{
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
		Assert( false );
		return false;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return false;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return false;
	}

	return IsProductPurchasable( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cDurangoCommerceManager::IsProductPurchasable( const cCommerceProductData* a_pProductData) const
{
	if (GetPlatformItem( a_pProductData ) == NULL)
	{
		return false;
	}

	bool hasValidAvailability = (GetPlatformItem(a_pProductData)->m_SignedOfferId.GetLength() != 0);

	return a_pProductData->GetEnumeratedFlag() && hasValidAvailability;
}

float cDurangoCommerceManager::GetProductPrice( u32 a_Index, char* a_pOutputString, u32 a_SizeOfOutputString ) const
{
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
		Assert( false );
		return INVALID_DURANGO_COMMERCE_PRICE_VALUE;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return INVALID_DURANGO_COMMERCE_PRICE_VALUE;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return INVALID_DURANGO_COMMERCE_PRICE_VALUE;
	}

	return GetProductPrice( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ), a_pOutputString, a_SizeOfOutputString );
}

float cDurangoCommerceManager::GetProductPrice( const cCommerceProductData* a_pProductData, char* a_pOutputString, unsigned a_SizeOfOutputString ) const
{
	if ( !a_pProductData->GetEnumeratedFlag() )
	{
		return INVALID_DURANGO_COMMERCE_PRICE_VALUE;
	}

	const cCommercePlatformItem* platformItem = GetPlatformItem(a_pProductData);

	if (platformItem == nullptr)
	{
		return INVALID_DURANGO_COMMERCE_PRICE_VALUE;
	}

	if (platformItem->m_AvailablilityId.GetLength() == 0)
	{
		return INVALID_DURANGO_COMMERCE_PRICE_VALUE;
	}

	// the display price is formatted price
	const char* displayPrice = platformItem->m_DisplayPrice.c_str();

#if RSG_GDK
	const unsigned ISRAELI_SHEKEL_UNICODE = 0x20AA;
	const unsigned SAUDI_ARABIAN_RIYAL_UNICODE = 0x0631;
	const unsigned TURKISH_LIRA_UNICODE = 0x20BA;

	unicode::utf32 currencyUnicode = unicode::Utf8_GetUtf32(displayPrice, 0);

	const bool isMatchingUnicode =
		currencyUnicode == ISRAELI_SHEKEL_UNICODE ||
		currencyUnicode == SAUDI_ARABIAN_RIYAL_UNICODE ||
		currencyUnicode == TURKISH_LIRA_UNICODE;

	const bool isMatchingCurrencyCode =
		strcmp(platformItem->m_CurrencyCode, "ILS") == 0 ||
		strcmp(platformItem->m_CurrencyCode, "SAR") == 0 ||
		strcmp(platformItem->m_CurrencyCode, "TL") == 0;

	// there are a few currencies that we cannot display so use the currency code and price for those
	if(isMatchingUnicode || isMatchingCurrencyCode || PARAM_netCommerceUseCurrencyCode.Get())
	{
		formatf(a_pOutputString, a_SizeOfOutputString, "%.2f %s", platformItem->m_Price, platformItem->m_CurrencyCode.c_str());
		commerceDebugf1("GetProductPrice :: Replacing formatted string. Formatted: %s, CurrencyCode: %s (%s), CurrencyUnicode: 0x%04x (%s), Output: %s",
			displayPrice,
			platformItem->m_CurrencyCode.c_str(),
			isMatchingCurrencyCode ? "Matched" : "Not Matched",
			currencyUnicode,
			isMatchingUnicode ? "Matched" : "Not Matched",
			a_pOutputString);
	}
	else
#endif
	{
		safecpy(a_pOutputString, displayPrice, a_SizeOfOutputString);
	}
	
	return platformItem->m_Price;
}

void cDurangoCommerceManager::AbortDataFetch()
{
    if (m_CatalogFetchStatus.Pending())
    {
        netTask::Cancel(&m_CatalogFetchStatus);
    }

    if (m_ConsumableCatalogFetchStatus.Pending())
    {
        netTask::Cancel(&m_ConsumableCatalogFetchStatus);
    }

    if (m_CatalogDetailFetchStatus.Pending())
    {
        netTask::Cancel(&m_CatalogDetailFetchStatus);
    }
}

bool cDurangoCommerceManager::ProductCodeEntry()
{
	if ( m_CodeEntryStatus.Pending() )
	{
		return false;
	}

	//Do promo entry.
	if (g_rlXbl.GetPlatformCommerceManager()->DoCodeRedemption( m_LocalGamerIndex, &m_CodeEntryStatus))
	{
		m_CurrentMode = MODE_PROMOCODE;
		return true;
	}
	else
	{
		return false;
	}
}



}

#endif //RSG_DURANGO