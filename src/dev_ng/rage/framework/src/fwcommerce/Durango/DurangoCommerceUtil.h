#ifndef DURANGOCOMMERCEUTIL
#define DURANGOCOMMERCEUTIL

#if RSG_DURANGO

#pragma once

#include "../CommerceUtil.h"

#include "net/status.h"
#include "rline/durango/rlxbl_interface.h"


namespace rage
{


class cDurangoCommerceManager : public cCommerceUtil
{
public:
	cDurangoCommerceManager();
	virtual ~cDurangoCommerceManager();

	virtual bool Init( int userIndex = 0, const char *skuFolder = NULL );
	void Shutdown();

	void Update();

	static cDurangoCommerceManager* Instance();

	bool DoPlatformDataRequest();
	bool IsPlatformReady() const { return ( true ); } 

	bool IsInErrorState() const;

	bool IsContentInfoPopulated() const;
	bool IsPlatformContentInfoPopulated() const { return m_ContentInfoPopulated; }

	void CheckoutProduct( unsigned a_Index );
	void CheckoutProduct( const cCommerceProductData* );

	bool IsInCheckout() const { return ( m_CurrentMode == MODE_CHECKOUT  || m_CurrentMode == MODE_PROMOCODE); }

	bool ProductCodeEntry();

	bool IsProductPurchased( int a_Index ) const;
	bool IsProductPurchased( const cCommerceProductData* a_pProductData ) const;

	bool IsProductPurchasable( int a_Index ) const;
	bool IsProductPurchasable( const cCommerceProductData* a_pProductData) const;

	float GetProductPrice( unsigned a_Index, char* a_pOutputString, unsigned a_SizeOfOutputString ) const;
	float GetProductPrice( const cCommerceProductData*, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const;

	void AbortDataFetch();

	bool IsPlatformInDataFetchState() const { return ( m_CurrentMode == MODE_ENUMERATE_CONTENT_DURABLE || m_CurrentMode == MODE_ENUMERATE_CONTENT_CONSUMABLE || m_CurrentMode == MODE_ENUMERATE_CONTENT_DETAIL ); }

	void SetRootProductIdentifier( const char* rootProductIdentifier  ) { m_RootProductIdentifier = rootProductIdentifier; }

protected:
	void MergePlatformAndRosData();

private:
	enum eCommerceMode
	{
		MODE_TOP = 0,
		MODE_ENUMERATE_CONTENT_DURABLE,
		MODE_ENUMERATE_CONTENT_CONSUMABLE,
		MODE_ENUMERATE_CONTENT_DETAIL,
        MODE_ENUMERATE_INVENTORY,
		MODE_ERROR_ENCOUNTERED,
		MODE_NEUTRAL,
		MODE_CHECKOUT,
		MODE_PROMOCODE,
		MODE_MAX
	};



	//Update functions
	void UpdateEnumerateContentDurable();
	void UpdateEnumerateContentConsumable();
	void UpdateEnumerateContentDetail();
    void UpdateEnumerateInventory();

	void UpdateCheckout();
	void UpdateCodeEntry();

	void UpdateCommerceDataWithEnumeratedContent( const char* idString, bool /*a_IsOwned*/ );
	
	
	const cCommercePlatformItem* GetPlatformItem( const cCommerceProductData* a_pProductData ) const;
	
	//Static singleton instance of this class
	static cDurangoCommerceManager* m_Instance;

	bool m_Initialised;

	eCommerceMode m_CurrentMode;

	int m_LocalGamerIndex;
	bool m_ContentInfoPopulated;

	netStatus m_CatalogFetchStatus;
    netStatus m_ConsumableCatalogFetchStatus;
	netStatus m_CatalogDetailFetchStatus;
	netStatus m_CheckoutStatus;
    netStatus m_PlayerDurableInventoryFetchStatus;
	
	netStatus m_CodeEntryStatus;

	//I'm going to use the platform commerce data directly here.
	cCommercePlatformDetails m_PlatformItemData;

	cCommercePlatformInventoryDetails m_PlatformDurableInventoryData;

	//Variables for multiple pass detail fetches.
	int m_CurrentDetailFetchBaseIndex;

	atString m_RootProductIdentifier;
};

} //namespace rage

#endif //RSG_DURANGO

#endif //DURANGOCOMMERCEUTIL
