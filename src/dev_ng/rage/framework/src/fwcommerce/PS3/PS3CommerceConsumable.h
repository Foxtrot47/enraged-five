#ifndef PS3COMMERCECONSUMABLE__H
#define PS3COMMERCECONSUMABLE__H

#if __PS3

#include "../CommerceConsumable.h"
#include "atl/string.h"

namespace rage
{


class cPS3CommerceConsumableTransaction : public cCommerceConsumableTransaction
{
public:
    cPS3CommerceConsumableTransaction();

    void Init();
    void Update();

    bool StartTransaction( const char* consumableIdentifier, int amountToConsume );

    bool HasCompleted() const { return ((m_CurrentTransactionState == PS3_TRANSACTION_COMPLETE)||(m_CurrentTransactionState == PS3_TRANSACTION_ERROR)); }
    bool HasSucceeded() const { return (m_CurrentTransactionState == PS3_TRANSACTION_COMPLETE);}

private:
    void UpdatePending();

    enum ePS3TransactionState
    {
        PS3_TRANSACTION_UNINITIALISED,
        PS3_TRANSACTION_PENDING,
        PS3_TRANSACTION_COMPLETE,
        PS3_TRANSACTION_ERROR
    };
    
    ePS3TransactionState m_CurrentTransactionState;

    int m_ExpectedPostTransactionValue;
    atString m_ConsumableId;
};

class cPS3CommerceConsumableManager : public cCommerceConsumableManager
{
public:
    static cPS3CommerceConsumableManager* Instance();

    void Init();
    void Update();

    //returns the consumable level. Returns -1 on error.
    int GetConsumableLevel( const char* /*aConsumableId*/ );

    bool StartOwnershipDataFetch();
    bool IsOwnershipDataPopulated() const { return ((m_CurrentAssetDataState == ASSET_DATA_STATE_DATA_POPULATED) && cCommerceConsumableManager::IsOwnershipDataPopulated()); }

    void DumpCurrentOwnershipData() const;

protected:
    cPS3CommerceConsumableManager();

private:
    void UpdateWaitingToStartTicketRefresh();
    void UpdateRefreshingTicket();
    
    //Static singleton instance of this class
    static cPS3CommerceConsumableManager* mp_Instance;

    enum eAssetDataState
    {
        ASSET_DATA_STATE_UNINITIALISED,
        ASSET_DATA_WAITING_TO_START_TICKET_REFRESH,
        ASSET_DATA_STATE_REFRESHING_TICKET,
        ASSET_DATA_STATE_DATA_POPULATED,
        ASSET_DATA_STATE_ERROR,
        ASSET_DATA_STATE_NUM_STATES
    };

    eAssetDataState m_CurrentAssetDataState;
};

} //namespace rage

#endif

#endif //PS3COMMERCECONSUMABLE__H
