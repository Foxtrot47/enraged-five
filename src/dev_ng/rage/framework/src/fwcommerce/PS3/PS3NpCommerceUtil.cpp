#if __PS3

#include "PS3NpCommerceUtil.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


// C headers
#include <sys/memory.h>
#include <cell/ssl.h>
#include <cell/sysmodule.h>
#include <netex/net.h>
#include <netex/libnetctl.h>
#include <sysutil/sysutil_common.h>
#include <cell/rtc.h>
#include <np.h> 



#include "rline/rlnp.h"
#include "rline/rltitleid.h"
#include "system/ipc.h"

#include "../CommerceChannel.h"

extern sys_memory_container_t g_commerce_container;

#pragma comment(lib, "http_stub")
#pragma comment(lib, "ssl_stub")
#pragma comment(lib, "sysutil_np_commerce2_stub")


//OPTIMISATIONS_OFF()

namespace rage
{

cPS3CommerceManager* cPS3CommerceManager::m_Instance = NULL;

const int DATA_FETCH_THREAD_STACK_SIZE = ( 16 * 1024 );

const int HTTP_POOL_SIZE  = ( 32 * 1024U );
const int COOKIE_POOL_SIZE = ( 32 * 1024U );
const int SSL_POOLSIZE  = ( 256 * 1024U );

//const int RESULTS_POOLSIZE = SCE_NP_COMMERCE2_RECV_BUF_SIZE;
//const int MISC_POOLSIZE = ( 64 * 1024U );
//const int TOTAL_POOL_SIZE = HTTP_POOL_SIZE + COOKIE_POOL_SIZE + SSL_POOLSIZE + MISC_POOLSIZE + (2 * RESULTS_POOLSIZE);
 
//const int INVALID_REQUEST_INDEX = 0xDEADBEEF;
const int CATALOG_REQUEST_ID = -1;

const int SONY_LINEBREAK_CODE_LEN = 4;

cPS3CommerceManager::cPS3CommerceManager() 
    :    m_ContentResultBufferType( CONTENT_RESULT_CODE_NONE ),
    
    m_CurrentMode( MODE_TOP ),
    m_ContentInfoPopulated( false ),
    m_RequestId( 0 ),
    mp_SslPool( NULL ),
    mp_HttpPool( NULL ),
    mp_CookiePool( NULL ),
    mp_CommerceResultAllocation( NULL ),
    m_bSessionCreated( false ),
    m_CategoryContentResultInitialised( false ),
    m_FetchingData( false ),
    m_IsInitialised( false ),
    m_IsContextCreated( false ),
    m_pCurrentRequestOffer( NULL ),
    m_pLastInfoRequestOffer( NULL ),
    m_NeedToRequeryData( false ),
    m_NumMetaDataRecords( 0 ),
    m_DataOperationAborted( false )
{
    if ( m_Instance == NULL )
    {
        m_Instance = this;
    }

    m_DataFetchThreadId = sysIpcThreadIdInvalid;
}   

//----------------------------------------------------------
cPS3CommerceManager::~cPS3CommerceManager()
{
   // delete mp_MemPool;
   // mp_MemPool = NULL;
    
    Shutdown();

    m_Instance = NULL;
}

//----------------------------------------------------------
int cPS3CommerceManager::LoadSslCertificates()
{
    char *buf = NULL;
    size_t size = 0;
    int ret = 0;

    ret = cellSslCertificateLoader( CELL_SSL_LOAD_CERT_ALL, NULL, 0, &size );
    if( ret < 0) 
    {
        commerceErrorf( "cellSslCertificateLoader() failed. ret = 0x%x\n", ret );
        goto error;
    }

    memset( &m_CaList, 0, sizeof( m_CaList ) );
    m_CaList.ptr = static_cast<char*>( CommerceAlloc( size ) );
    if( NULL == m_CaList.ptr )
    {               
        commerceErrorf("malloc failed for cert pinter... \n");
        ret = -1;
        goto error;
    }
    buf = (char*)( m_CaList.ptr );
    m_CaList.size = size;

    ret = cellSslCertificateLoader( CELL_SSL_LOAD_CERT_ALL, buf, size, NULL );
    if( ret < 0 )
    {
        commerceErrorf( "cellSslCertificateLoader() failed. ret = 0x%x\n", ret );
        goto error;
    }   

    

    return 0;

error:  
    UnloadCertificates();

    return ret;
}   

//----------------------------------------------------------
void cPS3CommerceManager::UnloadCertificates()
{
    if( m_CaList.ptr != NULL )
    {
        CommerceFree( m_CaList.ptr );
    }
}

//----------------------------------------------------------
bool cPS3CommerceManager::Init( int userIndex, const char* skuFolder )
{   
    int ret;
    //Make sure to check m_IsInitialised

    if ( m_IsInitialised )
    {
        commerceErrorf("Trying to initialise the commerce manager when it has already been intialised.");
        commerceAssert( false );
        return false;
    }

    m_IsInitialised = true;

    m_DataOperationAborted = false;

    for ( int iRecords = 0; iRecords < MAX_NUM_META_DATA_RECORDS; iRecords++ )
    {
        memset( &m_MetaDataRecords[ iRecords ] , 0, sizeof( OfferMetaData ) );

        m_MetaDataRecords[ iRecords ].longProductDesc[0] = 0;
    }

    m_LineBreakSubsString.Reset();
	m_LineBreakSubsString = "\n";

    cCommerceUtil::Init( userIndex, skuFolder );
    
    PassMemoryContainerToSystem( g_commerce_container );
    
//    sys_memory_info_t memInfo;
 //   sys_memory_get_user_memory_size( &memInfo );
    //We unload these modules here so that we can squeeze the commerce module into main thread memory
//     cellSysmoduleUnloadModule( CELL_SYSMODULE_VDEC_MPEG2 );
//     cellSysmoduleUnloadModule( CELL_SYSMODULE_AVCONF_EXT );
//     cellSysmoduleUnloadModule( CELL_SYSMODULE_SYSUTIL_GAME );
//     cellSysmoduleUnloadModule( CELL_SYSMODULE_SYSUTIL_SAVEDATA );
//     
    
    
   // sys_memory_get_user_memory_size( &memInfo );
 /*  
    ret = cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_NP_COMMERCE2 );
    if ( ret != CELL_OK )
    {
        commerceErrorf( "Error loading commerce module: 0x%x\n", ret );
        Assert( false );   
        return false;
    }
    
    //sys_memory_get_user_memory_size( &memInfo );

    ret = cellSysmoduleSetMemcontainer( m_CommerceCheckoutAllocation );
    if ( ret != CELL_OK )
    {
        Assert( false );
        return false;
    }

    ret = cellSysmoduleLoadModule( CELL_SYSMODULE_HTTPS );
    if ( ret != CELL_OK )
    {
        Assert( false );
        return false;
    }

    ret = cellSysmoduleLoadModule( CELL_SYSMODULE_RTC );
    if ( ret != CELL_OK )
    {
        Assert( false );
        return false;
    }

    ret = cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_GAME );
    if ( ret != CELL_OK )
    {
        Assert( false );
        return false;
    }
        
    ret = cellSysmoduleSetMemcontainer( SYS_MEMORY_CONTAINER_ID_INVALID );
    if ( ret != CELL_OK )
    {
        Assert( false );
    }
    */
    InitialiseHttpPool();
    ret = cellHttpInit( GetHttpPool(), GetHttpPoolSize() );
    if( ret < 0 )
    { 
        Assert( false );   
        return false; 
    }

    InitialiseCookiePool();
    if ( GetCookiePool() == NULL ) 
    { 
        Assert( false );
        return false; 
    } 

    ret = cellHttpInitCookie( GetCookiePool(), GetCookiePoolSize() ); 
    if ( ret < 0 ) 
    { 
        Assert( false );
        return false; 
    } 

    ret = LoadSslCertificates();
    if ( ret < 0 )
    {
        Assert( false );
        return false; 
    }
    
    InitialiseSslPool();    
    if ( GetSslPool() == NULL ) 
    {                                                                      
        Assert( false );
        return false; 
    }

    ret = cellSslInit( GetSslPool(), GetSslPoolSize() ); 
    if( ret < 0 )
    { 
        Assert( false );
        return false; 
    }   

    ret = cellHttpsInit( 1, &m_CaList );
    if( ret < 0 )
    {
        commerceErrorf( "cellHttpsInit() failed. ret = 0x%x\n", ret );
        Assert(false);
        return false;
    }

    ret = sceNpCommerce2Init( );
    if( ret < 0 )
    {
        //Necessary debug spam, may be rare fail cases in QA.
        commerceErrorf( "sceNpCommerce2Init() failed. ret = 0x%x\n", ret );
        Assert(false);
        return false;
    }

    if ( !CreateCommerceSession() )
    {
        m_CurrentMode = MODE_ERROR_ENCOUNTERED;
    }
    else
    {
        InitialiseCommerceResultAllocation();
        m_CurrentMode = MODE_CREATE_SESSION;
    }


    //Create mutexes
    m_FetchDataMutex = sysIpcCreateMutex();

    return true;
}

//----------------------------------------------------------
void cPS3CommerceManager::Shutdown()
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to shutdown commerce manager without initialising." );
        commerceAssert( false );
        return;
    }

    Assert( !m_FetchingData );

    

    //Run one more update cycle to ensure any pending shutdowns are completed
    Update();

    //Delete mutexes
    sysIpcDeleteMutex( m_FetchDataMutex );

    unsigned result;
    result = cellHttpsEnd();
    if ( result != 0 )
    {
        commerceErrorf("cellHttpsEnd () failed.    ret = 0x%x\n", result);
        Assert( false );
    }

    TerminateCommerceSession( );



    result = cellSslEnd();
    if ( result != 0 )
    {
        commerceErrorf("cellSslEnd () failed.    ret = 0x%x\n", result);
        Assert( false );
    }

    result = cellHttpEndCookie();
    if ( result != 0 )
    {
        commerceErrorf("cellHttpEndCookie () failed. ret = 0x%x\n", result);
        Assert( false );
    }
    
    result = cellHttpEnd();
    if ( result != 0 )
    {
        commerceErrorf("cellHttpEnd () failed. ret = 0x%x\n", result);
        Assert( false );
    }   

    ReleaseMemoryPools();

    sys_memory_info_t info;
    sys_memory_container_get_size( &info, m_CommerceCheckoutAllocation );
/*      
    cellSysmoduleUnloadModule( CELL_SYSMODULE_SYSUTIL_NP_COMMERCE2 );

    cellSysmoduleUnloadModule( CELL_SYSMODULE_SYSUTIL_GAME );
    cellSysmoduleUnloadModule( CELL_SYSMODULE_RTC );
   
    cellSysmoduleUnloadModule( CELL_SYSMODULE_HTTPS );

    sys_memory_container_get_size( &info, m_CommerceCheckoutAllocation );

    int ret = cellSysmoduleSetMemcontainer( SYS_MEMORY_CONTAINER_ID_INVALID );
    if ( ret != CELL_OK )
    {
        Assert( false );
    }
*/

/*
    AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_VDEC_MPEG2 ) == CELL_OK );
    AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_AVCONF_EXT ) == CELL_OK );
    AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_GAME ) == CELL_OK );
    AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_SAVEDATA ) == CELL_OK );
*/

    //Reinitialise member variables back to initial states.
    m_ContentResultBufferType = CONTENT_RESULT_CODE_NONE;
    m_pCurrentRequestOffer = NULL;
    m_CurrentMode = MODE_TOP;
    m_ContentInfoPopulated = false;
    m_RequestId = 0;
    mp_SslPool = NULL;
    mp_HttpPool = NULL;
    mp_CookiePool = NULL;
    mp_CommerceResultAllocation = NULL;
    m_bSessionCreated = false;
    m_CategoryContentResultInitialised = false;
    m_FetchingData = false;
    m_IsInitialised = false;
    m_IsContextCreated = false;
    m_pLastInfoRequestOffer = NULL;
    m_NeedToRequeryData = false;
    m_NumMetaDataRecords = 0;
    m_DataOperationAborted = false;
    m_LineBreakSubsString.Reset();

    cCommerceUtil::Shutdown();
}

//----------------------------------------------------------
void cPS3CommerceManager::Commerce2Handler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg)
{
    commerceDisplayf("Commerce2Handler reached, current mode is %d event is 0x%x\n",cPS3CommerceManager::Instance()->m_CurrentMode,event);
    switch(cPS3CommerceManager::Instance()->m_CurrentMode)
    {
    case( MODE_CREATE_SESSION ):
        Commerce2CreateSessionHandler( ctx_id, subject_id, event, error_code, arg );
        break;
    case( MODE_CHECKOUT ):
        Commerce2CheckoutHandler( ctx_id, subject_id, event, error_code, arg );
        break;
    case( MODE_PROMO_CODE ):
        Commerce2CodeEntryHandler( ctx_id, subject_id, event, error_code, arg );
        break;
    case( MODE_DOWNLOAD_LIST):
        Commerce2DownloadListHandler( ctx_id, subject_id, event, error_code, arg );
        break;
    }
}

//----------------------------------------------------------
void cPS3CommerceManager::Commerce2CreateSessionHandler( unsigned /*ctx_id*/, unsigned /*subject_id*/, int event, int /*error_code*/, void* /*arg*/ )
{
    commerceDisplayf("Commerce2CreateSessionHandler called, event is 0x%x\n",event);
    switch( event )
    {
    case(SCE_NP_COMMERCE2_EVENT_CREATE_SESSION_DONE):
        cPS3CommerceManager::Instance()->m_CurrentMode = MODE_CREATE_SESSION_FINISHED;
        break;
    case(SCE_NP_COMMERCE2_EVENT_CREATE_SESSION_ABORT):          
    case(SCE_NP_COMMERCE2_EVENT_REQUEST_ERROR):
        //ERROR
		cPS3CommerceManager::Instance()->m_CurrentMode = MODE_CREATE_SESSION_FINISHED_WITH_ERROR;
        break;
    default:
        break;
    }
}

//----------------------------------------------------------
void cPS3CommerceManager::Commerce2CheckoutHandler( unsigned /*ctx_id*/, unsigned /*subject_id*/, int event, int /*error_code*/, void* /*arg*/ )
{

    commerceDisplayf("Commerce2CheckoutHandler called, event is 0x%x\n",event);
    switch( event )
    {
    case SCE_NP_COMMERCE2_EVENT_DO_CHECKOUT_BACK:
    case SCE_NP_COMMERCE2_EVENT_REQUEST_ERROR:
        cPS3CommerceManager::Instance()->m_DidLastCheckoutSucceed = false;
        cPS3CommerceManager::Instance()->m_CurrentMode = MODE_CHECKOUT_DONE;
        break;
    case SCE_NP_COMMERCE2_EVENT_DO_CHECKOUT_SUCCESS: 
        cPS3CommerceManager::Instance()->m_DidLastCheckoutSucceed = true;
        cPS3CommerceManager::Instance()->m_CurrentMode = MODE_CHECKOUT_DONE;
        break;
    case(SCE_NP_COMMERCE2_EVENT_DO_CHECKOUT_STARTED):
        break;
    default:
        break;
    }
}

//----------------------------------------------------------
void cPS3CommerceManager::Commerce2CodeEntryHandler( unsigned /*ctx_id*/, unsigned /*subject_id*/, int event, int /*error_code*/, void* /*arg*/ )
{
    commerceDisplayf("Commerce2CodeEntryHandler called, event is 0x%x\n",event);
    switch( event )
    {
    case SCE_NP_COMMERCE2_EVENT_DO_PRODUCT_CODE_SUCCESS:
        cPS3CommerceManager::Instance()->m_DidLastCheckoutSucceed = true;
        cPS3CommerceManager::Instance()->m_CurrentMode = MODE_PROMO_CODE_DONE;
        break;
    case SCE_NP_COMMERCE2_EVENT_DO_PRODUCT_CODE_BACK:
    case SCE_NP_COMMERCE2_EVENT_REQUEST_ERROR:
        cPS3CommerceManager::Instance()->m_DidLastCheckoutSucceed = false;
        cPS3CommerceManager::Instance()->m_CurrentMode = MODE_PROMO_CODE_DONE;
        break;
    case(SCE_NP_COMMERCE2_EVENT_DO_CHECKOUT_STARTED):
        break;
    default:
        break;
    }
}

//----------------------------------------------------------
void cPS3CommerceManager::Commerce2DownloadListHandler( unsigned /*ctx_id*/, unsigned /*subject_id*/, int event, int /*error_code*/, void* /*arg*/ )
{

    commerceDisplayf("Commerce2DownloadListHandler called, event is 0x%x\n",event);
    switch( event )
    {
    case SCE_NP_COMMERCE2_EVENT_DO_DL_LIST_SUCCESS:
		cPS3CommerceManager::Instance()->m_DidLastCheckoutSucceed = true;
		cPS3CommerceManager::Instance()->m_CurrentMode = MODE_DOWNLOAD_LIST_DONE;
		break;
    case SCE_NP_COMMERCE2_EVENT_REQUEST_ERROR:
	case SCE_NP_COMMERCE2_EVENT_DO_CHECKOUT_BACK:
		cPS3CommerceManager::Instance()->m_DidLastCheckoutSucceed = false;
        cPS3CommerceManager::Instance()->m_CurrentMode = MODE_DOWNLOAD_LIST_DONE;
        break;
    case(SCE_NP_COMMERCE2_EVENT_DO_CHECKOUT_STARTED):
        break;
    default:
        break;
    }
}


//Memory allocation

//----------------------------------------------------------
void cPS3CommerceManager::InitialiseSslPool()
{
    Assert(mp_SslPool == NULL);
    mp_SslPool = static_cast<char*>( CommerceAlloc( SSL_POOLSIZE ) );
}

//----------------------------------------------------------
void cPS3CommerceManager::InitialiseHttpPool()
{
    Assert(mp_HttpPool == NULL);
    mp_HttpPool = static_cast<char*>( CommerceAlloc( HTTP_POOL_SIZE ) );
}

//----------------------------------------------------------
void cPS3CommerceManager::InitialiseCookiePool()
{
    Assert(mp_CookiePool == NULL);
    mp_CookiePool = static_cast<char*>( CommerceAlloc( COOKIE_POOL_SIZE ) );    
}

//----------------------------------------------------------
void cPS3CommerceManager::ReleaseMemoryPools()
{
    if (mp_SslPool)
    {
        CommerceFree( mp_SslPool );
        mp_SslPool = NULL;
    }

    if ( mp_HttpPool )
    {
        CommerceFree( mp_HttpPool );
        mp_HttpPool = NULL;
    }

    if ( mp_CookiePool )
    {
        CommerceFree( mp_CookiePool );
        mp_CookiePool = NULL;
    }

    if ( mp_CommerceResultAllocation )
    {
        CommerceFree( mp_CommerceResultAllocation );
        mp_CommerceResultAllocation = NULL;
    }   

    UnloadCertificates();
}

//----------------------------------------------------------
int cPS3CommerceManager::GetSslPoolSize()
{
    return SSL_POOLSIZE;
}

//----------------------------------------------------------
int cPS3CommerceManager::GetHttpPoolSize()
{
    return HTTP_POOL_SIZE;
}

//----------------------------------------------------------
int cPS3CommerceManager::GetCookiePoolSize()
{
    return COOKIE_POOL_SIZE;
}

//----------------------------------------------------------
void cPS3CommerceManager::InitialiseCommerceResultAllocation()
{
    mp_CommerceResultAllocation = static_cast<char*>( CommerceAlloc( SCE_NP_COMMERCE2_RECV_BUF_SIZE ) );   
}

int cPS3CommerceManager::GetCommerceResultAllocationSize()
{
    return SCE_NP_COMMERCE2_RECV_BUF_SIZE;
}

//----------------------------------------------------------
char* cPS3CommerceManager::GetCommerceResultAllocation()
{
    return mp_CommerceResultAllocation;
}

//----------------------------------------------------------
//END TEMP POOL FUNCTIONS
//----------------------------------------------------------
bool cPS3CommerceManager::DoPlatformDataRequest()
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call DoCommerceCategoryContentRequest without initialising commerce manager." );
        Assert( false );
        return false;
    }

    if ( IsInErrorState() )
    {
        return false;
    }

    cPS3CommerceManager::CreateCategoryInfoFetchThread( CATALOG_REQUEST_ID );

    return true;
}

//----------------------------------------------------------
void cPS3CommerceManager::DoCommerceProductInfoRequest( OfferMetaData* offerData )
{
    Assert( IsContentInfoPopulated() );

    

    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call DoCommerceProductInfoRequest without initialising commerce manager." );
        Assert( false );
        return;
    }

    if ( !IsContentInfoPopulated() )
    {
        return;
    }

    if ( m_CurrentMode != MODE_NEUTRAL )
    {
        return;
    }

    if ( offerData->isVirtualProduct )
    {
        return;
    }

    if ( IsInErrorState() )
    {
        return;
    }

    if ( !sysIpcTryLockMutex( GetFetchDataMutex() ) )
    {
        return;
    }

    //Check for invalid thread id
    if ( m_DataFetchThreadId != sysIpcThreadIdInvalid )
    {
        return;
    }

    int result;
    result = sceNpCommerce2GetProductInfoCreateReq( m_ContextId, &m_RequestId );
    if ( result != 0 )
    {
        commerceErrorf( "sceNpCommerce2GetProductInfoCreateReq failed with code 0x%x\n",result );
        Assert( false );
    }

/*
    //When new product data is fetched it will invalidate some of the old info, so this must be refreshed from the category info
    InitialiseProductDataFromCategoryResult();
*/

    // start a thread instead
    Assert( m_FetchingData == false );
    m_FetchingData = true;
    m_pCurrentRequestOffer = offerData;
    int requestedProductIndex = -1;

    for ( int i=0; i < m_NumMetaDataRecords; i++ )
    {
        if ( offerData == &m_MetaDataRecords[ i ] )
        {
            requestedProductIndex = i;
            break;
        }
    }

    commerceAssert( requestedProductIndex != -1 );

    //Check for return of invalid thread id
    m_DataFetchThreadId = sysIpcCreateThread( FetchDataThread, (void*)requestedProductIndex , DATA_FETCH_THREAD_STACK_SIZE, PRIO_NORMAL, "NPCommerceFDThreadProd" ); 
    if ( m_DataFetchThreadId == sysIpcThreadIdInvalid )
    {
        AssertMsg( false, "Thread create failed" );
        return;
    }

    AssertVerify( sysIpcUnlockMutex( GetFetchDataMutex() ) );

    m_ContentResultBufferType = CONTENT_RESULT_CODE_PRODUCT;
    m_CurrentMode = MODE_FETCH_PRODUCT_INFO;
}
 
//----------------------------------------------------------
void cPS3CommerceManager::Update()
{
    int result;

    cCommerceUtil::Update();

    if ( !m_IsInitialised )
    {
        return;
    }

    

    switch (m_CurrentMode)
    {     
        case(MODE_CREATE_SESSION_FINISHED):
            result = sceNpCommerce2CreateSessionFinish( cPS3CommerceManager::Instance()->m_ContextId, &cPS3CommerceManager::Instance()->m_SessionInfo );
            if ( result != 0 )
            {
                commerceErrorf("sceNpCommerce2CreateSessionFinish() failed. ret = 0x%x\n", result);
                Assert(false);
            }
            else    
            {
                m_bSessionCreated = true;
                cPS3CommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            }
            break;
		case(MODE_CREATE_SESSION_FINISHED_WITH_ERROR):
			result = sceNpCommerce2CreateSessionFinish( cPS3CommerceManager::Instance()->m_ContextId, &cPS3CommerceManager::Instance()->m_SessionInfo );
			commerceDisplayf("Commerce2CreateSessionHandler fail retval is 0x%x reported in MODE_CREATE_SESSION_FINISHED_WITH_ERROR\n",result);
			cPS3CommerceManager::Instance()->m_CurrentMode = MODE_ERROR_ENCOUNTERED;
			break;
        case( MODE_CHECKOUT_DONE ):
            sceNpCommerce2DoCheckoutFinishAsync( m_ContextId );
/*
            AssertVerify( cellSysmoduleSetMemcontainer( m_CommerceCheckoutAllocation ) == CELL_OK );
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_RTC ) == CELL_OK );
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_HTTPS ) == CELL_OK );      
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_GAME ) == CELL_OK );
            AssertVerify( cellSysmoduleSetMemcontainer( SYS_MEMORY_CONTAINER_ID_INVALID ) == CELL_OK );
  */         
            CreateGamedataFolder();
            cPS3CommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            break;
        case( MODE_DOWNLOAD_LIST_DONE ):
            sceNpCommerce2DoDlListFinishAsync ( m_ContextId );
/*
            AssertVerify( cellSysmoduleSetMemcontainer( m_CommerceCheckoutAllocation ) == CELL_OK );
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_RTC ) == CELL_OK );
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_HTTPS ) == CELL_OK );       
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_GAME ) == CELL_OK );
            AssertVerify( cellSysmoduleSetMemcontainer( SYS_MEMORY_CONTAINER_ID_INVALID ) == CELL_OK );
  */
            CreateGamedataFolder();

            cPS3CommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            break;
        case( MODE_PROMO_CODE_DONE ):
            sceNpCommerce2DoProductCodeFinishAsync( m_ContextId );
/*          
            AssertVerify( cellSysmoduleSetMemcontainer( m_CommerceCheckoutAllocation ) == CELL_OK );
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_RTC ) == CELL_OK );
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_HTTPS ) == CELL_OK );      
            AssertVerify( cellSysmoduleLoadModule( CELL_SYSMODULE_SYSUTIL_GAME ) == CELL_OK );
            AssertVerify( cellSysmoduleSetMemcontainer( SYS_MEMORY_CONTAINER_ID_INVALID ) == CELL_OK );
  */
            CreateGamedataFolder();
            cPS3CommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            break;    
        case( MODE_FETCH_CATEGORY_INFO ):
            UpdatePopulateContentInfo();
            break;
        case( MODE_FETCH_PRODUCT_INFO ):
            UpdatePopulateProductInfo();
            break;
        case( MODE_SESSION_TIMEOUT ):
            UpdateSessionTimeout();
            break;
        case( MODE_NEUTRAL ):
            UpdateModeNeutral();
            break;
    }
}

//----------------------------------------------------------
sys_memory_container_t cPS3CommerceManager::GetCommerceCheckoutUtilAllocation()
{
    return m_CommerceCheckoutAllocation;
}

//----------------------------------------------------------
const char * cPS3CommerceManager::GetItemLongDescription( const cCommerceProductData* product )
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call GetProductLongDescription without initialising commerce manager." );
        Assert( false );
        return NULL;
    }

    if( !IsPlatformReady() )
    {
        return( NULL );
    }

    if ( !product->GetEnumeratedFlag() )
    {
        return product->GetLongDescription();  
    }
    else
    {
        const OfferMetaData* offerData = GetOfferInfo( product ); 
        if ( offerData == NULL )
        {
            //This is an enumerated product, but no product is found.
            //Something has gone seriously wacky.
            commerceAssert( false );
            return NULL;
        }

        if ( offerData->longProductDesc[0] != 0 )
        {
            //Allready populated, send it back.
            return offerData->longProductDesc;
        }
        else
        {
            //Are we already fetching other data?
            if ( !IsFetchingData() )
            {
                //Put in our own request for data 
                DoCommerceProductInfoRequest( const_cast<OfferMetaData*>(offerData) );
            }
        }
    }
    return NULL;
}

//----------------------------------------------------------
void cPS3CommerceManager::CheckoutProduct( unsigned a_Index )
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call CheckoutProduct without initialising commerce manager." );
        Assert( false );
        return;
    }
    
    if ( m_CurrentMode != MODE_NEUTRAL )
    {
        return;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return;
    }

    return CheckoutProduct( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}


void cPS3CommerceManager::CheckoutProduct( const cCommerceProductData* a_pProductdata )
{
    if ( !a_pProductdata->GetEnumeratedFlag() )
    {
        return;
    }

    if ( GetOfferInfo( a_pProductdata ) == NULL )
    {
        return;
    }
    
    const OfferMetaData *pOfferData = GetOfferInfo( a_pProductdata );

    if ( !pOfferData->hasValidSku )
    {
        return;
    }

    bool refetch = IsProductPurchased( a_pProductdata ) && !a_pProductdata->IsConsumable();

    if ( refetch )
    {
        const char *skuIdArray[1];
        skuIdArray[0] = pOfferData->skuId;

        atString serviceId(rlNp::GetTitleId().GetServiceId());

        
        int result = sceNpCommerce2DoDlListStartAsync( m_ContextId, serviceId.c_str() , skuIdArray, 1, GetCommerceCheckoutUtilAllocation() );
        if ( result != 0 )
        {
            commerceErrorf( "sceNpCommerce2DoDlListStartAsync failed with code 0x%x\n",result );
            Assert( false );
        }

        m_CurrentMode = MODE_DOWNLOAD_LIST;
    }
    else
    {
        const char *skuIdArray[1];
        skuIdArray[0] = pOfferData->skuId;
        int result = sceNpCommerce2DoCheckoutStartAsync( m_ContextId, skuIdArray, 1, GetCommerceCheckoutUtilAllocation() );
        if ( result != 0 )
        {
            commerceErrorf( "sceNpCommerce2DoCheckoutStartAsync failed with code 0x%x\n",result );
            Assert( false );
        }

        m_CurrentMode = MODE_CHECKOUT;
    }

}

//----------------------------------------------------------
void cPS3CommerceManager::UpdatePopulateContentInfo()
{
    if ( IsFetchingData() )
    {
        //Category fetch has not completed.
        return;
    }

    //Thread has finished, clean it up.
    sysIpcWaitThreadExit( m_DataFetchThreadId );

    m_DataFetchThreadId = sysIpcThreadIdInvalid;

    m_CurrentMode = MODE_NEUTRAL;

    //Check if the operation was aborted early.
    if ( m_DataOperationAborted )
    {
        m_DataOperationAborted = false;
        sceNpCommerce2DestroyReq( m_RequestId );
        m_RequestId = 0;    
        m_pCurrentRequestOffer = NULL;
        return;
    }

    int result = sceNpCommerce2GetCategoryContentsGetResult( m_RequestId, GetCommerceResultAllocation(), GetCommerceResultAllocationSize(), &m_ContentResultsBufferSize);
    if ( result < 0)
    {
        if ( result == SCE_NP_COMMERCE2_SERVER_ERROR_SESSION_EXPIRED )
        {
            //Session expired. Record the request index and put us into the session recreate state.
            m_pLastInfoRequestOffer = NULL;
            m_NeedToRequeryData = true;

            m_bSessionCreated = false;
            m_CurrentMode = MODE_SESSION_TIMEOUT;

            //Clear up and get out.
            int returnVal = sceNpCommerce2DestroyReq( m_RequestId );
            m_RequestId = 0;

            if ( returnVal < 0 )
            {
                commerceAssertf(false, "sceNpCommerce2DestroyReq failed with code 0x%x\n",returnVal );
                
            }

            return;
        }
        else if ( result == SCE_NP_COMMERCE2_SERVER_ERROR_ACCESS_PERMISSION_DENIED )
        {
            //And the academy award for most common issue with commerce goes to........
            commerceErrorf( "SCE_NP_COMMERCE2_SERVER_ERROR_ACCESS_PERMISSION_DENIED RECEIVED. This almost certainly means your PSN account is not whitelisted for commerce access." );
            
            m_WhitelistingFailed = true;
        }
        else
        {
            commerceErrorf( "sceNpCommerce2GetCategoryContentsGetResult failed with code 0x%x\n",result );
			m_RequestId = 0;    
			m_pCurrentRequestOffer = NULL;
			ForceErrorState();
			sceNpCommerce2DestroyReq( m_RequestId );
			return;
            //Assert( false );
        }
    }

    //Populate the game category info
    //SceNpCommerce2GetCategoryContentsResult categoryContentResult;
    memset( &m_CategoryContentResult, 0, sizeof( SceNpCommerce2GetCategoryContentsResult ) );

	if (GetCommerceResultAllocation() == NULL)
	{
		commerceAssertf(false,"We did not recieve our Memory allocation. DO NOT IGNORE THIS ASSERT! ENTER A BUG AND MESSAGE ROSS CHILDS");
		return;
	}

    result = sceNpCommerce2InitGetCategoryContentsResult( &m_CategoryContentResult, GetCommerceResultAllocation(), m_ContentResultsBufferSize );
    if (result != 0)
    {
        commerceErrorf( "sceNpCommerce2InitGetCategoryContentsResult failed with code 0x%x\n",result );
        //Assert( false );
    }

	memset( &m_CategoryInfo, 0, sizeof( SceNpCommerce2CategoryInfo ) );
    result = sceNpCommerce2GetCategoryInfo( &m_CategoryContentResult, &m_CategoryInfo );
    if (result != 0 )
    {
        commerceErrorf( "sceNpCommerce2GetCategoryInfo failed with code 0x%x\n", result );
        //Assert( false );
    }

    InitialiseProductDataFromCategoryResult();


    

    result = sceNpCommerce2DestroyGetCategoryContentsResult( &m_CategoryContentResult );
    if ( result != 0 )
    {
        commerceErrorf( "sceNpCommerce2InitGetCategoryContentsResult failed with code 0x%x\n",result );
        Assert( false );
    }

    sceNpCommerce2DestroyReq( m_RequestId );

    
    m_RequestId = 0;    
    m_pCurrentRequestOffer = NULL;
    m_ContentInfoPopulated = true;
    
}

//----------------------------------------------------------
void cPS3CommerceManager::UpdatePopulateProductInfo()
{
    if (IsFetchingData())
    {
        //Product info fetch has not completed.
        return;
    }

    //Thread has finished, clean it up.
    sysIpcWaitThreadExit( m_DataFetchThreadId );
    m_DataFetchThreadId = sysIpcThreadIdInvalid;

    Assert( IsContentInfoPopulated() );

    m_CurrentMode = MODE_NEUTRAL;

    //Check if the operation was aborted early.
    if ( m_DataOperationAborted )
    {
        m_DataOperationAborted = false;
        m_pCurrentRequestOffer = NULL;
        sceNpCommerce2DestroyReq( m_RequestId );
        m_RequestId = 0;
        return;
    }

    unsigned result = sceNpCommerce2GetProductInfoGetResult( m_RequestId, GetCommerceResultAllocation(), GetCommerceResultAllocationSize(), &m_ContentResultsBufferSize);
    if ( result != 0)
    {
        if ( result == SCE_NP_COMMERCE2_SERVER_ERROR_SESSION_EXPIRED )
        {
            //Session expired. Record the request index and put us into the session recreate state.
            m_pLastInfoRequestOffer = m_pCurrentRequestOffer;
            m_NeedToRequeryData = true;

            m_bSessionCreated = false;
            m_CurrentMode = MODE_SESSION_TIMEOUT;

            //Clear up and get out.
            int returnVal = sceNpCommerce2DestroyReq( m_RequestId );
            m_RequestId = 0;
            
            if ( returnVal < 0 )
            {
                commerceErrorf( "sceNpCommerce2DestroyReq failed with code 0x%x\n",returnVal );
                Assert( false );
            }

            return;
        }
        else
        {
            commerceErrorf( "sceNpCommerce2GetProductInfoGetResult failed with code 0x%x\n",result );
            Assert( false );
			m_RequestId = 0;    
			m_pCurrentRequestOffer = NULL;
			ForceErrorState();
			sceNpCommerce2DestroyReq( m_RequestId );
			return;
        }
    }

    //Populate the game category info
    SceNpCommerce2GetProductInfoResult productInfoResult;
    memset( &productInfoResult, 0, sizeof( SceNpCommerce2GetProductInfoResult ) );

    result = sceNpCommerce2InitGetProductInfoResult( &productInfoResult, GetCommerceResultAllocation(), m_ContentResultsBufferSize );
    if (result != 0)
    {
        commerceErrorf( "sceNpCommerce2InitGetProductInfoResult failed with code 0x%x\n",result );
        Assert( false );
    }

    SceNpCommerce2GameProductInfo gameProductInfo;
    sceNpCommerce2GetGameProductInfo( &productInfoResult, &gameProductInfo );

    safecpy( m_pCurrentRequestOffer->longProductDesc, gameProductInfo.productLongDescription, META_DATA_LONG_DESC_LEN );

    //Fix up the html tags. Thanks Sony.
    FixHTMLLineBreaks( m_pCurrentRequestOffer );
    RemoveHTMLTags( m_pCurrentRequestOffer );

    //Clean up

    result = sceNpCommerce2DestroyGetProductInfoResult( &productInfoResult );
    if (result != 0)
    {
        commerceErrorf( "sceNpCommerce2DestroyGetProductInfoResultResult failed with code 0x%x\n",result );
        Assert( false );
    }

    

    m_pCurrentRequestOffer = NULL;
    sceNpCommerce2DestroyReq( m_RequestId );
    m_RequestId = 0;
}

//----------------------------------------------------------
void cPS3CommerceManager::FetchDataThread( void *pData )
{
    cPS3CommerceManager* instance = cPS3CommerceManager::Instance();
    AssertVerify( sysIpcLockMutex( m_Instance->GetFetchDataMutex() ) );

    unsigned requestId = instance->m_RequestId;

    //For now we need to fudge this as Sony have given us a different ServiceID for commerce :-(

    atString serviceId(rlNp::GetTitleId().GetServiceId());

    int catOrProdRequestIndex = ( int )pData;
    

    //We are doing the old category second, so that new content only visible to the DE is seen at the top of the list.
    //TODO: Add category AND per product list ordering priority
    if ( catOrProdRequestIndex == CATALOG_REQUEST_ID )
    {
        //Category content request
        commerceDisplayf("Thread requesting data for index %d\n",catOrProdRequestIndex);
        unsigned result = sceNpCommerce2GetCategoryContentsStart( requestId, serviceId.c_str(), 0, SCE_NP_COMMERCE2_GETCAT_MAX_COUNT);
        if ( result != 0)
        {
            if ( result == SCE_NP_COMMERCE2_ERROR_ABORTED )
            {
                instance->m_DataOperationAborted = true;
            }
            else
            {
                instance->m_DataOperationAborted = true;
                commerceErrorf( "sceNpCommerce2GetCategoryContentsStart failed with code 0x%x\n",result );
                Assert( false );
            }
            
        }
    }
    else
    {

        //product info request
        int result = sceNpCommerce2GetProductInfoStart( requestId, NULL, cPS3CommerceManager::Instance()->m_MetaDataRecords[ catOrProdRequestIndex ].productIdentifier );
        if ( result != 0)
        {
            if ( result == SCE_NP_COMMERCE2_ERROR_ABORTED || result == CELL_HTTP_ERROR_NET_SSL_RECV_EINTR )
            {
                instance->m_DataOperationAborted = true;
            }
            else
            {
                instance->m_DataOperationAborted = true;
                commerceErrorf( "sceNpCommerce2GetProductInfoStart failed with code 0x%x\n",result );
                Assert( false );
            }
        }
    }

    cPS3CommerceManager::Instance()->m_FetchingData = false;    

    AssertVerify( sysIpcUnlockMutex( m_Instance->GetFetchDataMutex() ) );
}

//----------------------------------------------------------
void cPS3CommerceManager::InitialiseProductDataFromCategoryResult()
{
    for (unsigned iContent = 0; iContent < m_CategoryInfo.countOfProduct; iContent++)
    {
        SceNpCommerce2GameProductInfo gameProductInfo;
        SceNpCommerce2ContentInfo contentInfo;
        int result = sceNpCommerce2GetContentInfo( &m_CategoryContentResult, iContent, &contentInfo  );
        if ( result < 0 )
        {
            commerceErrorf("sceNpCommerce2GetContentInfo() failed. ret = 0x%x\n", result);
            Assert( false );
        }

        sceNpCommerce2GetGameProductInfoFromContentInfo( &contentInfo, &gameProductInfo );



        AddNewMetadataRecordForGameProductInfo( &gameProductInfo );

       
    }
}

//----------------------------------------------------------
bool cPS3CommerceManager::IsProductPurchased( int a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductPurchased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cPS3CommerceManager::IsProductPurchased( const cCommerceProductData* pProductData) const
{
    if ( pProductData == NULL )
    {
        return false;
    }

    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }

    const OfferMetaData *pOfferInfo = GetOfferInfo( pProductData );

    if ( pOfferInfo == NULL )
    {
        return false;
    }

    return pOfferInfo->hasPurchased;  
}

//----------------------------------------------------------
bool cPS3CommerceManager::IsProductReleased( int a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductReleased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cPS3CommerceManager::IsProductReleased( const cCommerceProductData* pProductData ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    if ( pProductData == NULL )
    {
        return false;
    }

    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }

    const OfferMetaData *pOfferInfo = GetOfferInfo( pProductData );

    if ( pOfferInfo == NULL )
    {
        return false;
    }

    CellRtcTick currentTick;

    //RLPOSIXTIME
    int result = sceNpManagerGetNetworkTime(&currentTick);
    if ( result != CELL_OK )
    {
        commerceErrorf( "cellRtcGetCurrentTick failed with error code 0x%x\n",result );
        return false;
    }

    CellRtcDateTime currentDateTime;
    result = cellRtcSetTick( &currentDateTime, &currentTick );
    if ( result < 0 )
    {
        commerceErrorf( "cellRtcSetTick failed with error code 0x%x\n",result );
        Assert( false );
        return false;
    }

    /*
    if ( GetShownDataByIndex( a_Index )->isVirtualProduct )
    {
        int year = cellRtcGetYear( &currentDateTime );
        int month = cellRtcGetMonth( &currentDateTime );
        int day = cellRtcGetDay( &currentDateTime );

        if ( GetShownDataByIndex( a_Index )->releaseYear > year )
        {
            return false;
        }
        else if ( GetShownDataByIndex( a_Index )->releaseYear < year )
        {
            return true;
        }
        //same year, check month
        else if ( GetShownDataByIndex( a_Index )->releaseMonth > month )
        {
            return false;
        }
        else if ( GetShownDataByIndex( a_Index )->releaseMonth < month )
        {
            return true;
        }
        //same month, check day
        else if ( GetShownDataByIndex( a_Index )->releaseDay > day )
        {
            return false;
        }
        else if ( GetShownDataByIndex( a_Index )->releaseDay < day )
        {
            return true;
        }
        // same day! Its release day, so let it through.
        else
        {
            return true;
        }
    }
    else
    */

    {
        //To keep Sony happy, we dont mess with their release date data.
        return true;
    }
}   

//----------------------------------------------------------
bool cPS3CommerceManager::IsProductPurchasable( int a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductPurchasable( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cPS3CommerceManager::IsProductPurchasable( const cCommerceProductData* pProductData ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }
    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }

    const OfferMetaData *pOfferInfo = GetOfferInfo( pProductData );

    if ( pOfferInfo == NULL )
    {
        return false;
    }

    return pOfferInfo->isPurchasable && pOfferInfo->hasValidSku;
}

//----------------------------------------------------------
bool cPS3CommerceManager::ProductCodeEntry()
{
    

    if ( m_CurrentMode != MODE_NEUTRAL )    
    {
        return false;
    }

    //TODO: Add a state which respresents "session created but category info not fetched".
    if ( !IsPlatformContentInfoPopulated() )
    {
        //If the content info is not populate we are fetching it, and going into code entry will blow our state machine all to hell.
        return false;
    }
/* 
#ifdef TB_FINAL
    //This does not execute properly in non final dev builds due to sonys reset procedure.
    sceNpCommerce2ExecuteStoreBrowse(SCE_NP_COMMERCE2_STORE_BROWSE_TYPE_PRODUCT_CODE, NULL, 0);
#endif
    */
/*
    cellSysmoduleUnloadModule( CELL_SYSMODULE_RTC );
    cellSysmoduleUnloadModule( CELL_SYSMODULE_HTTPS );
    cellSysmoduleUnloadModule( CELL_SYSMODULE_SYSUTIL_GAME );
  */   
    SceNpCommerce2ProductCodeParam codeEntryParams;

    codeEntryParams.size = sizeof( SceNpCommerce2ProductCodeParam );
    codeEntryParams.inputMode = SCE_NP_COMMERCE2_PRODUCT_CODE_INPUT_MODE_USER_INPUT;

    int result = sceNpCommerce2DoProductCodeStartAsync( m_ContextId, GetCommerceCheckoutUtilAllocation(), &codeEntryParams );
    if ( result != 0 )
    {
        commerceErrorf( "sceNpCommerce2DoProductCodeStartAsync failed with code 0x%x\n",result );
        Assert( false );
    }   
    
    m_CurrentMode = MODE_PROMO_CODE;
	return true;
    
}

//----------------------------------------------------------
bool cPS3CommerceManager::CreateCommerceSession( )
{
    int ret;
    
    SceNpId npId;

    ret = sceNpManagerGetNpId( &npId );
    if( ret < 0 )
    {
        commerceErrorf( "sceNpManagerGetNpId() failed. ret = 0x%x\n", ret );
        return false;
    }

    Assert( !m_IsContextCreated );

    ret = sceNpCommerce2CreateCtx( SCE_NP_COMMERCE2_VERSION, &npId, cPS3CommerceManager::Commerce2Handler, NULL, &m_ContextId);    
    if( ret < 0 )
    {
        commerceErrorf( "sceCommerce2CreateCtx() failed. ret = 0x%x\n", ret );
        return false;
    }

    m_IsContextCreated = true;

    ret = sceNpCommerce2CreateSessionStart( m_ContextId );
    if( ret < 0 )
    {
        commerceErrorf("sceNpCommerce2CreateSessionStart() failed. ret = 0x%x\n", ret);
        return false;
    }

    return true;
}

//----------------------------------------------------------
bool cPS3CommerceManager::TerminateCommerceSession( )
{
    unsigned result;

    if ( m_IsContextCreated )
    {
        m_IsContextCreated = false;

        result = sceNpCommerce2DestroyCtx( m_ContextId );
        if ( result != 0 )
        {
            commerceErrorf("sceNpCommerce2DestroyCtx() failed. ret = 0x%x\n", result);
            Assert( false );
            return false;
        }
    }



    result = sceNpCommerce2Term( );
    if ( result != 0 )
    {
        commerceErrorf("sceNpCommerce2Term() failed. ret = 0x%x\n", result);
        Assert( false );
        return false;   
    }
    
    return true;
}

float cPS3CommerceManager::GetProductPrice( u32 a_Index, char* a_pOutputString, u32 a_SizeOfOutputString ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
        Assert( false );
        return -1.0f;
    }

    Assert( IsContentInfoPopulated() );

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return -1.0f;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return -1.0f;
    }

    return GetProductPrice( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ), a_pOutputString, a_SizeOfOutputString );
}

float cPS3CommerceManager::GetProductPrice( const cCommerceProductData* a_pProductData, char* a_pOutputString, unsigned a_SizeOfOutputString ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call GetProductPrice without initialising commerce manager." );
        Assert( false );
        return -1.0f;
    }

    if ( a_pProductData == NULL )
    {
        return -1.0f;
    }
        
    if ( !a_pProductData->GetEnumeratedFlag() )
    {
        return -1.0f;
    }

    unsigned price;
   
    const OfferMetaData *pOfferData = GetOfferInfo( a_pProductData );

    if ( pOfferData == NULL )
    {
        return -1.0f;
    }

    if ( pOfferData->hasValidSku )
    {
        
        if ( a_pOutputString != NULL )
        {
            int result = sceNpCommerce2GetPrice( m_ContextId,  a_pOutputString, a_SizeOfOutputString, pOfferData->displayPrice );

            if ( result < 0 )
            {
                commerceErrorf("sceNpCommerce2GetPrice() failed. ret = 0x%x\n", result);
                Assert( false );
                return -1.0f;
            }
        }
        
        price = pOfferData->displayPrice;
    }
    else
    {
        a_pOutputString = NULL;
        price = pOfferData->displayPrice;
    }

    return price;
    
}

void cPS3CommerceManager::UpdateSessionTimeout()
{
    m_bSessionCreated = false;

    int ret = sceNpCommerce2CreateSessionStart( m_ContextId );
    if( ret < 0 )
    {
        commerceErrorf("sceNpCommerce2CreateSessionStart() failed. ret = 0x%x\n", ret);
        m_CurrentMode = MODE_ERROR_ENCOUNTERED;
        return;
    }

    m_CurrentMode = MODE_CREATE_SESSION;
}

bool cPS3CommerceManager::UpdateModeNeutral()
{
    if ( m_NeedToRequeryData )
    {
        //We have a query outstanding from a session timeout, handle it.
        m_NeedToRequeryData = false;
        
        if ( m_pLastInfoRequestOffer == NULL )
        {
            DoPlatformDataRequest();
        }
        else
        {
            DoCommerceProductInfoRequest( m_pLastInfoRequestOffer );
        }
       
        m_pLastInfoRequestOffer = NULL;
    }

    return true;
}

/*
const char * cPS3CommerceManager::GetProductId( unsigned a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call GetProductId without initialising commerce manager." );
        Assert( false );
        return NULL;
    }

    Assert( a_Index < GetNumProducts() );
    if ( a_Index >= GetNumProducts() )
    {
        return NULL;
    }

    return GetShownDataByIndex( a_Index )->productIdentifier;
}
*/

void cPS3CommerceManager::CreateGamedataFolder()
{
     //This should be handled on a per game level. Retained here as a reminder of what is involved.
    /*
    if ( PS3Environment::Instance().IsGameDataPathValid( PS3Environment::DLC_AND_PATCHES ) )
    {
        //No need for a change.
        return;
    }

    if ( !PS3Environment::Instance().CreateGameData( PS3Environment::DLC_AND_PATCHES, "", false ) )
    {
        //DLC folder still not present, dont modify the file mapping.
        return;
    }

    //Now we have our properly mounted DLC directory, we modify our placeholder mapping to point to it.
    FileAccessor & fa( FileAccessor::Instance() );
    AStaticString<> dlcFinalPath;
    dlcFinalPath.Format( "%s/final/ps3/dlc", PS3Environment::Instance().GetGameDataPath( PS3Environment::DLC_AND_PATCHES ) );
    fa.ModifyFileSystem( AStaticString<>("dlc"), dlcFinalPath );
    */
}

void cPS3CommerceManager::DoLanguageSubstitutionPass()
{
    //for ( int iMetaData = 0; iMetaData < m_NumMetaDataRecords; iMetaData++ )
    //{
    //    for ( int iLangSubLines = 0; iLangSubLines < m_NumLanguageSubs; iLangSubLines++ )
    //    {
    //        if ( strcmp( m_MetaDataRecords[ iMetaData ].langSubCode, m_MetaDataLanguageLines[ iLangSubLines ].m_LangSubCode ) == 0 )
    //        {
    //            //We have a match. Copy the language sub lines into the DLC metadata.
    //            safecpy( m_MetaDataRecords[ iMetaData ].productName, m_MetaDataLanguageLines[ iLangSubLines ].m_LangSubName, META_DATA_NAME_LEN );
    //            safecpy( m_MetaDataRecords[ iMetaData ].longProductDesc, m_MetaDataLanguageLines[ iLangSubLines ].m_LangSubLongDesc, META_DATA_LONG_DESC_LEN );
    //        }
    //    }
    //} 
}

void cPS3CommerceManager::AbortDataFetch()
{
    cCommerceUtil::AbortDataFetch();

    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call AbortDataFetch without initialising commerce manager." );
        Assert( false );
        return;
    }

    commerceDebugf3("Aborting commerce data fetch.\n");

    if ( !m_DataOperationAborted  && m_RequestId != 0 )
    {
        int result = sceNpCommerce2AbortReq( m_RequestId );
        if ( result < 0 )
        {
            commerceErrorf( "sceNpCommerce2AbortReq failed with ret code of 0x%x\n", result );
        }
        
        AssertMsg( result >= 0, "sceNpCommerce2AbortReq failed" );        
    }
    
    m_FetchingData = false;
}

bool cPS3CommerceManager::GetSkuInfo( SceNpCommerce2GameProductInfo *productInfo, SceNpCommerce2GameSkuInfo *outSkuInfo )
{
    Assert( productInfo );

    bool foundValid = false;

    for ( int i = 0; i < productInfo->countOfSku; i++ )
    {
        int result = sceNpCommerce2GetGameSkuInfoFromGameProductInfo( productInfo, i, outSkuInfo ); 
        if ( result < 0 )
        {
            commerceErrorf("sceNpCommerce2GetGameSkuInfoFromGameProductInfo() failed skuID %d. ret = 0x%x\n", i, result);
        }
        else
        {
            foundValid = true;
            break;
        }
    }

    return foundValid;
}

void cPS3CommerceManager::ForceErrorState()
{
    m_CurrentMode = MODE_ERROR_ENCOUNTERED;
}

void cPS3CommerceManager::AddNewMetadataRecordForGameProductInfo( SceNpCommerce2GameProductInfo *a_GameProductInfo )
{
    if ( m_NumMetaDataRecords == MAX_NUM_META_DATA_RECORDS )
    {
        Assert( false );
        return;
    }

    OfferMetaData newOfferData;

    memset( &newOfferData, 0, sizeof( OfferMetaData ) );

    newOfferData.isVirtualProduct = false;


    //product id
    safecpy( newOfferData.productIdentifier, a_GameProductInfo->productId, META_DATA_SHORT_DESC_LEN );

    //Short description
    safecpy( newOfferData.productName, a_GameProductInfo->productName, META_DATA_NAME_LEN );

    
    //Long description
    if ( a_GameProductInfo->dataType == SCE_NP_COMMERCE2_GAME_PRODUCT_DATA_TYPE_NORMAL )
    {
        safecpy( newOfferData.longProductDesc, a_GameProductInfo->productLongDescription, META_DATA_LONG_DESC_LEN );
    }
    
    //TODO add description of malformed products
    //price
    if ( a_GameProductInfo->countOfSku > 0 )
    {
        SceNpCommerce2GameSkuInfo gameSkuInfo;
        newOfferData.hasValidSku = GetSkuInfo( a_GameProductInfo, &gameSkuInfo );
        
        if ( newOfferData.hasValidSku )
        {
            newOfferData.displayPrice =  gameSkuInfo.price;

            newOfferData.hasPurchased =  ( ( gameSkuInfo.annotation & SCE_NP_COMMERCE2_SKU_ANN_PURCHASED_CANNOT_PURCHASE_AGAIN ) ||  
                ( gameSkuInfo.annotation & SCE_NP_COMMERCE2_SKU_ANN_PURCHASED_CAN_PURCHASE_AGAIN ) );

            newOfferData.isPurchasable = ( gameSkuInfo.purchasabilityFlag == SCE_NP_COMMERCE2_SKU_PURCHASABILITY_FLAG_ON );

            safecpy( newOfferData.skuId, gameSkuInfo.skuId, SCE_NP_COMMERCE2_SKU_ID_LEN + 1 );
        }
    }
    else
    {
        newOfferData.hasValidSku = false;
        newOfferData.hasPurchased = false;
        newOfferData.isPurchasable = false;
    }
    

    //release tick
    newOfferData.releaseTick = a_GameProductInfo->releaseDate;

    //Dont add a new record for something we have already enumerated.
    int findResult = FindExistingRecord( newOfferData );

    if ( findResult == -1 )
    {
        //Check we have enough room to add the new data
        if ( m_NumMetaDataRecords == MAX_NUM_META_DATA_RECORDS )
        {
            Assert( false );
            return;
        }

        commerceDebugf3("Adding record for newly enumerated data: %s\n", a_GameProductInfo->productId);

        m_MetaDataRecords[ m_NumMetaDataRecords++ ] = newOfferData;
    }
    else
    {
        if ( m_MetaDataRecords[ findResult ].longProductDesc[0] != NULL )
        {
            //allready has long desc downloaded, dont waste it.
            safecpy( newOfferData.longProductDesc, m_MetaDataRecords[ findResult ].longProductDesc, META_DATA_LONG_DESC_LEN );
        }
        m_MetaDataRecords[ findResult ] = newOfferData;
    }
    
}


int cPS3CommerceManager::FindExistingRecord( OfferMetaData &a_RecordToMatch )
{
    int result = -1;
    for ( int i = 0; i < m_NumMetaDataRecords; i++ )
    {
        if ( strcmp( m_MetaDataRecords[ i ].productIdentifier, a_RecordToMatch.productIdentifier ) == 0 && ( m_MetaDataRecords[ i ].isVirtualProduct == a_RecordToMatch.isVirtualProduct ) )
        {
            //We have a match.
            result = i;
        }
    }

    return result;
}

void cPS3CommerceManager::CreateCategoryInfoFetchThread( int categoryId )
{
    if ( m_CurrentMode != MODE_NEUTRAL )
    {
        //We do not want to be cutting the legs out of the state machine half way through, no-siree-bob.
        return;
    }
    
    if ( !sysIpcTryLockMutex( GetFetchDataMutex() ) )
    {
        return;
    }

    if ( m_DataFetchThreadId != sysIpcThreadIdInvalid )
    {
        return;
    }

    

    int result;
    result = sceNpCommerce2GetCategoryContentsCreateReq( m_ContextId, &m_RequestId );
    if ( result != 0 )
    {
        commerceErrorf( "sceNpCommerce2GetCategoryContentsCreateReq failed with code 0x%x\n",result );
        Assert( false );
    }

    // start a thread instead
    m_DataFetchThreadId = sysIpcThreadIdInvalid;

    Assert( m_FetchingData == false );
    m_FetchingData = true;
    m_pCurrentRequestOffer = NULL;

    //Add fail handling for thread creation check for sysIpcThreadIdInvalid
    m_DataFetchThreadId = sysIpcCreateThread( FetchDataThread, (void*)categoryId , DATA_FETCH_THREAD_STACK_SIZE, PRIO_NORMAL, "NPCommerceFDThreadCat" ); 
    if ( m_DataFetchThreadId == sysIpcThreadIdInvalid )
    {
        AssertMsg( false, "Thread create failed" );
        return;
    }

    AssertVerify( sysIpcUnlockMutex( GetFetchDataMutex() ) );

    m_ContentInfoPopulated = false;
    m_ContentResultBufferType = CONTENT_RESULT_CODE_CATEGORY;
    m_CurrentMode = MODE_FETCH_CATEGORY_INFO;
}

cPS3CommerceManager* cPS3CommerceManager::Instance()
{
	if ( m_Instance == NULL )
	{
		m_Instance = rage_new cPS3CommerceManager;
	}

	return m_Instance;
}






const cPS3CommerceManager::OfferMetaData *cPS3CommerceManager::GetOfferInfo( const cCommerceProductData* pProductInfo ) const
{
    //Check we have valid commerce data.
    Assert( cCommerceUtil::IsContentInfoPopulated() );
    if ( !cCommerceUtil::IsContentInfoPopulated() )
    {
        return NULL;
    }

    commerceAssert( pProductInfo != NULL );
    if ( pProductInfo == NULL )
    {
        return NULL;
    }

    if ( !pProductInfo->GetEnumeratedFlag() )
    {
        //Didnt tie to an actual product, return NULL
        return NULL;
    }

    int foundIndex = -1;

    for ( int i = 0; i < m_NumMetaDataRecords; i++ )
    {
        if ( pProductInfo->GetPlatformId() == m_MetaDataRecords[i].productIdentifier )
        {
            //Found the correct offer.
            foundIndex = i;
            break;
        }
    }

    if ( foundIndex == -1 )
    {
        return NULL;
    }
    else
    {
        return &m_MetaDataRecords[foundIndex];
    } 
}

void cPS3CommerceManager::MergePlatformAndRosData()
{
    for ( int i=0; i< m_NumMetaDataRecords; i++)
    {
        UpdateCommerceDataWithEnumeratedContent( m_MetaDataRecords[i].productIdentifier, m_MetaDataRecords[i].hasPurchased );
    }

    for ( int s=0; s <  m_CommerceData.GetSizeOfDataArray(); s++ )
    {
        cCommerceItemData* itemData = m_CommerceData.GetItemData( s );
       
        //By default the dependencys are set to having been found
        itemData->SetDependencyFlag( true );

        for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
        {
            //There is a dependency, set the flag to false
            itemData->SetDependencyFlag( false );
            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {

                if ( ( strcmp(m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetDependencyIds()[ iDeps ].c_str()) == 0 )
                    && m_MetaDataRecords[ iOffers ].hasPurchased )
                {
                    //Found this dependency, set the flag and get out.
                    itemData->SetDependencyFlag( true );
                    break;
                }
            }
        }

        //By default enumeration dependencies
        itemData->SetEnumDependencyFlag( true );

        for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
        {
            //There is a dependency, set the flag to false
            itemData->SetEnumDependencyFlag( false );

            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {
                if ( strcmp( m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetEnumDependencyIds()[ iEnumDeps ] ) == 0)
                {
                    //Found this enumeration dependency, set the flag and get out.
                    itemData->SetEnumDependencyFlag( true );
                    break;
                }
            }
        }

        //By default the exclusions flag is set to false (no exclusions found)
        itemData->SetExclusionFlag( false );
        for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
        {
            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {
                if ( ( strcmp(m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetExclusionIds()[ iExcs ].c_str()) == 0 )
                    && m_MetaDataRecords[ iOffers ].hasPurchased )
                {
                    //Found this exclusion, set the flag and get out.
                    itemData->SetExclusionFlag( true );
                    break;
                }
            }
        }

        //By default the enumerated exclusions flag is set to false (no exclusions found)
        itemData->SetEnumExclusionFlag( false );
        for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
        {
            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {
                if ( strcmp( m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetEnumExclusionIds()[ iEnumExcs ] ) == 0)           
                {
                    //Found this exclusion, set the flag and get out.
                    itemData->SetEnumExclusionFlag( true );
                    break;
                }
            }
        }
    }
}

void cPS3CommerceManager::UpdateCommerceDataWithEnumeratedContent( const char* a_ProductIdentifier, bool /*a_IsOwned*/ )
{
    for ( int i = 0; i < m_CommerceData.GetSizeOfDataArray(); i++ )
    {
        if ( m_CommerceData.GetItemData( i )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
        {
            //Not a product, ergo does not need to be checked.
            continue;
        }
        
        cCommerceProductData* productData = static_cast<cCommerceProductData*>(m_CommerceData.GetItemData( i ));
        if ( productData->GetPlatformId() == a_ProductIdentifier )
        {
            productData->SetEnumeratedFlag( true );
        }
    }
}

bool cPS3CommerceManager::IsContentInfoPopulated() const
{
    return ( m_ContentInfoPopulated && cCommerceUtil::IsContentInfoPopulated() );
}

bool cPS3CommerceManager::IsInErrorState() const
{
    bool retval = ( m_CurrentMode == MODE_ERROR_ENCOUNTERED );
    retval = retval || cCommerceUtil::IsInErrorState();
    return retval;
}

void cPS3CommerceManager::SetLineBreakString( const atString& newSubsString )
{
    if ( newSubsString.GetLength() >  SONY_LINEBREAK_CODE_LEN )
    {
        commerceErrorf("Not setting the subs code in SetLineBreakString. Cannot be longer than %d chars.", SONY_LINEBREAK_CODE_LEN );
        return;
    }

    m_LineBreakSubsString = newSubsString;
}

void cPS3CommerceManager::FixHTMLLineBreaks( OfferMetaData *productData )
{
    if ( m_LineBreakSubsString.GetLength() == 0 )
    {
        return;
    }

    commerceAssert( productData );
    atString tmp = atString( productData->longProductDesc );
    
    tmp.Replace( "<BR>", m_LineBreakSubsString.c_str() );
    tmp.Replace( "<br>", m_LineBreakSubsString.c_str() );
    
    //If anyone uses this tag they should be fired out of a cannon into the sun.
    tmp.Replace( "<bR>", m_LineBreakSubsString.c_str() );

    tmp.Replace( "<Br>", m_LineBreakSubsString.c_str() );

    safecpy( productData->longProductDesc, tmp.c_str() , META_DATA_LONG_DESC_LEN );
}

void cPS3CommerceManager::RemoveHTMLTags( OfferMetaData *productData )
{
    commerceAssert( productData );

    atString tmp = atString(productData->longProductDesc);

    int startIndex = -1;
    int endIndex = -1;

    do 
    {
        startIndex = -1;
        endIndex = -1;

        startIndex = tmp.IndexOf( '<' );
        endIndex = tmp.IndexOf( '>' );

        while ( endIndex < startIndex && endIndex != -1 )
        {
            endIndex = tmp.IndexOf( '>', endIndex + 1 );            
        }

        if ( startIndex != -1 && endIndex != -1 )
        {
            atString tag;
            tag.Set( tmp, startIndex, endIndex - startIndex + 1 );
            tmp.Replace( tag.c_str() , "" );
        }
    } while ( startIndex != -1 && endIndex != -1 );

    safecpy( productData->longProductDesc, tmp.c_str() , META_DATA_LONG_DESC_LEN );
}

void cPS3CommerceManager::DoPlatformLevelDebugDump()
{
    if (!m_IsInitialised)
    {
        commerceDebugf1("Attempted PS3 commerce platform data dump. Platform commerce not initialised.\n");
        return;
    }

    commerceDebugf1("PS3 commerce platform data dump.\n");
    commerceDebugf1("Session is created: %d\n", m_bSessionCreated);
    commerceDebugf1("Content info populated: %d\n", m_ContentInfoPopulated);
    commerceDebugf1("Number of offer meta data records: %d\n", m_NumMetaDataRecords);
}

}
#endif //__PS3

