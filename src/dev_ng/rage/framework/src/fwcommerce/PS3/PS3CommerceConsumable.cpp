#if __PS3

#include "PS3CommerceConsumable.h"
#include "../CommerceChannel.h"
#include "fwnet/optimisations.h"
#include "rline/rlnp.h"


#include <np.h> 

NETWORK_OPTIMISATIONS()

namespace rage
{

cPS3CommerceConsumableManager* cPS3CommerceConsumableManager::mp_Instance = NULL;

cPS3CommerceConsumableManager* cPS3CommerceConsumableManager::Instance()
{
    if ( mp_Instance == NULL )
    {
        mp_Instance = rage_new cPS3CommerceConsumableManager;
    }

    return mp_Instance;
}


cPS3CommerceConsumableManager::cPS3CommerceConsumableManager() :
    m_CurrentAssetDataState(ASSET_DATA_STATE_UNINITIALISED)
{
    mp_CurrentTransaction = rage_new cPS3CommerceConsumableTransaction;
}

void cPS3CommerceConsumableManager::Init()
{
    m_CurrentAssetDataState = ASSET_DATA_STATE_UNINITIALISED;
    cCommerceConsumableManager::Init();
}

void cPS3CommerceConsumableManager::Update()
{
    switch(m_CurrentAssetDataState)
    {
    case(ASSET_DATA_STATE_REFRESHING_TICKET):
        UpdateRefreshingTicket();
        break;
    case(ASSET_DATA_WAITING_TO_START_TICKET_REFRESH):
        UpdateWaitingToStartTicketRefresh();
    default:
        break;
    }

    cCommerceConsumableManager::Update();
}

int cPS3CommerceConsumableManager::GetConsumableLevel( const char* aConsumableId )
{
    if ( g_rlNp.IsTicketRequestPending() )
    {
        return 0;
    }

    if ( !g_rlNp.IsEntitlementTicketValid() )
    {
        return 0;
    }

    if ( aConsumableId == NULL )
    {
        return 0;
    }

    //Now pull the actual entitlement level out
    SceNpEntitlement entitlement;
    if (sceNpManagerGetEntitlementById(aConsumableId, &entitlement) == 0)
    {
        return entitlement.remaining_count;
    }
    else
    {
        //Error handling
        //SCE_NP_ERROR_NOT_INITIALIZED
        //SCE_NP_ERROR_INVALID_ARGUMENT
        //SCE_NP_ERROR_OUT_OF_MEMORY
        //SCE_NP_ERROR_ID_NOT_FOUND
        //SCE_NP_ERROR_INVALID_STATE
    }

    return 0;
}

void cPS3CommerceConsumableManager::UpdateWaitingToStartTicketRefresh()
{
    if (g_rlNp.IsTicketRequestPending())
    {
        return;
    }

    if (!g_rlNp.RefreshEntitlementTicket())
    {
        m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
    }
    else
    {
        m_CurrentAssetDataState = ASSET_DATA_STATE_REFRESHING_TICKET;
    }
}


void cPS3CommerceConsumableManager::UpdateRefreshingTicket()
{
    if (g_rlNp.IsTicketRequestPending())
    {
        return;
    }

    if ( g_rlNp.IsEntitlementTicketValid() )
    {
        m_CurrentAssetDataState = ASSET_DATA_STATE_DATA_POPULATED;
    }
    else
    {
        m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
    }
}

void cPS3CommerceConsumableManager::DumpCurrentOwnershipData() const
{
    commerceDebugf1("-------------------------------");
    commerceDebugf1("DUMPING CURRENT OWNERSHIP DATA");
    commerceDebugf1("-------------------------------");

    if ( IsOwnershipDataPopulated() == false )
    {
        commerceDebugf1("*No ownership data to dump, No valid data downloaded*");
        return;
    }

    int numEntitlements = sceNpManagerGetEntitlementIdList(NULL,0);

    SceNpEntitlementId *entitlementArray = rage_new SceNpEntitlementId[numEntitlements];


    numEntitlements = sceNpManagerGetEntitlementIdList(entitlementArray,numEntitlements);

    for (s32 i=0; i<numEntitlements; i++)
    {
        SceNpEntitlement entitlement;
        sceNpManagerGetEntitlementById((const char*)(entitlementArray[i].data), &entitlement);
        commerceDebugf3("Entitlement %d: %s : %d",i, entitlementArray[i].data, entitlement.remaining_count);
    }

    delete [] entitlementArray;
}

bool cPS3CommerceConsumableManager::StartOwnershipDataFetch()
{
    if ( m_CurrentAssetDataState == ASSET_DATA_WAITING_TO_START_TICKET_REFRESH || m_CurrentAssetDataState == ASSET_DATA_STATE_REFRESHING_TICKET )
    {
        return false;
    }

    m_CurrentAssetDataState = ASSET_DATA_WAITING_TO_START_TICKET_REFRESH;

    return true;
}



cPS3CommerceConsumableTransaction::cPS3CommerceConsumableTransaction() :
    m_CurrentTransactionState(PS3_TRANSACTION_UNINITIALISED),
    m_ExpectedPostTransactionValue(0)
{
    m_ConsumableId.Reset();
}

void cPS3CommerceConsumableTransaction::Init()
{
    m_CurrentTransactionState = PS3_TRANSACTION_UNINITIALISED;
    m_ConsumableId.Reset();
    cCommerceConsumableTransaction::Init();

}


bool cPS3CommerceConsumableTransaction::StartTransaction( const char* consumableIdentifier, int amountToConsume )
{
    if (consumableIdentifier == NULL)
    {
        return false;
    }

    if ( amountToConsume <= 0 )
    {
        //You want to consume nothing or less than nothing?!?
        return false;
    }

    //Get the current level of the requested consumable
    if (g_rlNp.IsTicketRequestPending() || !g_rlNp.IsEntitlementTicketValid())
    {
        commerceErrorf("Trying to consume without a valid entitlement ticket");
        return false;
    }

    SceNpEntitlement entitlement;
    int retVal = sceNpManagerGetEntitlementById(consumableIdentifier, &entitlement); 
    if ( retVal == SCE_NP_ERROR_ID_NOT_FOUND )
    {
        commerceErrorf("Trying to consume a consumable which is not present. This may mean the consumable level is 0.");
        m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
        return false;
    }
    else if ( retVal != 0 )
    {
        commerceErrorf("Error retrieving entitlement ID level for consumption: 0x%x",retVal);
        m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
        return false;
    }

    if ( entitlement.remaining_count < amountToConsume )
    {
        commerceErrorf("Trying to consume a consumable which has an insufficient level. You may want to refetch entitlement data.");
        m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
        return false;
    }

    m_ExpectedPostTransactionValue = entitlement.remaining_count - amountToConsume;

    if (!g_rlNp.ConsumeEntitlement(consumableIdentifier,amountToConsume))
    {
        m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
        return false;
    }

    m_ConsumableId = consumableIdentifier;
    m_CurrentTransactionState = PS3_TRANSACTION_PENDING;
    return cCommerceConsumableTransaction::StartTransaction(consumableIdentifier,amountToConsume);
}

void cPS3CommerceConsumableTransaction::Update()
{
    switch(m_CurrentTransactionState)
    {
    case(PS3_TRANSACTION_PENDING):
        UpdatePending();
    default:
        break;
    }

    cCommerceConsumableTransaction::Update();
}  

void cPS3CommerceConsumableTransaction::UpdatePending()
{
    if ( g_rlNp.IsTicketRequestPending() )
    {
        return;
    }

    //Verify that the new consumable level is what we expect
    if (!g_rlNp.IsEntitlementTicketValid())
    {
        commerceErrorf("Entitlement ticket is not valid after consumption attempt.");
        m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
        return;
    }

    SceNpEntitlement entitlement;
    int retVal = sceNpManagerGetEntitlementById(m_ConsumableId.c_str(), &entitlement); 
    if ( retVal == SCE_NP_ERROR_ID_NOT_FOUND && m_ExpectedPostTransactionValue == 0)
    {
        //We cant find the consumable, but we are expecting that since we are consuming all of it.
        entitlement.remaining_count = 0;
    }
    else if ( retVal != 0 )
    {
        commerceErrorf("Error retrieving entitlement ID level for check: 0x%x",retVal);
        m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
        return;
    }

    if ( entitlement.remaining_count == m_ExpectedPostTransactionValue || ( retVal ==  SCE_NP_ERROR_ID_NOT_FOUND && m_ExpectedPostTransactionValue == 0 ) )
    {
        //Success
        m_CurrentTransactionState = PS3_TRANSACTION_COMPLETE;
        return;
    }

    m_CurrentTransactionState = PS3_TRANSACTION_ERROR;
}

} //namespace rage
#endif