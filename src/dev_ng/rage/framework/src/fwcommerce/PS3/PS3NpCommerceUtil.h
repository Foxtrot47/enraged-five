#ifndef PS3NPCOMMERCEUTIL
#define PS3NPCOMMERCEUTIL

#if __PS3

#pragma once

#include <np/commerce2.h> 
#include <cell/http.h> 

#include "system/ipc.h"

#include "../CommerceUtil.h"

namespace rage
{

class cPS3CommerceManager : public cCommerceUtil
{
public:

    virtual ~cPS3CommerceManager();
    
    static cPS3CommerceManager* Instance();

    virtual bool Init( int userIndex = 0, const char *skuFolder = NULL );
    void Shutdown();
    
    bool DoPlatformDataRequest();

    
    
    
    bool IsPlatformReady() const { return ( m_bSessionCreated ); } 
    bool IsContentInfoPopulated() const;
    bool IsPlatformContentInfoPopulated() const { return m_ContentInfoPopulated; }


    bool IsProductPurchased( int a_Index ) const;
    bool IsProductPurchased( const cCommerceProductData* ) const;


    bool IsProductReleased( int a_Index ) const;
    bool IsProductReleased( const cCommerceProductData* ) const;


    bool IsProductPurchasable( int a_Index ) const;
    bool IsProductPurchasable( const cCommerceProductData* ) const;

    virtual const char *GetItemLongDescription( const cCommerceProductData* product ); //Can't be const.

    void CheckoutProduct( unsigned a_Index );
    void CheckoutProduct( const cCommerceProductData* );

    bool IsInCheckout() const { return ( m_CurrentMode == MODE_CHECKOUT || m_CurrentMode == MODE_CHECKOUT_DONE || m_CurrentMode == MODE_DOWNLOAD_LIST || m_CurrentMode == MODE_PROMO_CODE ); }
    bool IsPlatformInDataFetchState() const { return ( m_CurrentMode == MODE_FETCH_CATEGORY_INFO || m_CurrentMode == MODE_FETCH_PRODUCT_INFO || m_FetchingData ); }
    bool IsInErrorState() const;
    void ForceErrorState();

    void AbortDataFetch();

    void Update();

    bool ProductCodeEntry();

    void PassMemoryContainerToSystem(sys_memory_container_t a_Container) { m_CommerceCheckoutAllocation = a_Container; }
    sys_memory_container_t GetMemoryContainerFromSystem() { return m_CommerceCheckoutAllocation; }

    float GetProductPrice( unsigned a_Index, char* a_pOutputString, unsigned a_SizeOfOutputString ) const;
    float GetProductPrice( const cCommerceProductData*, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const;
    
    //const char * GetProductId( unsigned a_Index ) const;

    void SetLineBreakString( const atString&  );

    void DoPlatformLevelDebugDump();

protected:
    void MergePlatformAndRosData();


private:
    
    cPS3CommerceManager();
    
    //Static singleton instance of this class
    static cPS3CommerceManager* m_Instance;


    enum eCommerceMode {
        MODE_TOP = 0,
        MODE_OPTIONS,   
        MODE_MSGDIALOG,
        MODE_NETSTART,
        MODE_NP_INIT,
        MODE_NP_TERM,
        MODE_CREATE_SESSION,
        MODE_CATEGORY,
        MODE_FETCH_CATEGORY_INFO,
        MODE_CONTENT_INFO,
        MODE_SKU_LIST, 
        MODE_FETCH_PRODUCT_INFO,
        MODE_PRODUCT_INFO,
        MODE_CHECKOUT,
        MODE_CHECKOUT_DONE,
        MODE_PROMO_CODE,
        MODE_PROMO_CODE_DONE,
        MODE_DOWNLOAD_LIST,
        MODE_DOWNLOAD_LIST_DONE,
        MODE_NEUTRAL,
        MODE_CREATE_SESSION_FINISHED,
		MODE_CREATE_SESSION_FINISHED_WITH_ERROR,
        MODE_ERROR_ENCOUNTERED,
        MODE_SESSION_TIMEOUT,
        MODE_MAX
    };

    enum eCommerceResultsAllocContentMode
    {
        CONTENT_RESULT_CODE_CATEGORY,
        CONTENT_RESULT_CODE_PRODUCT,
        CONTENT_RESULT_CODE_NONE,
        CONTENT_RESULT_CODE_MAX
    };

    eCommerceResultsAllocContentMode m_ContentResultBufferType;
    unsigned m_ContentResultsBufferSize;
    

    eCommerceMode m_CurrentMode;
    bool  m_ContentInfoPopulated; 
    
    //Private utility functions

    //Update functions
    void UpdatePopulateContentInfo();

    void InitialiseProductDataFromCategoryResult();
    void UpdatePopulateProductInfo();
    
    void UpdateSessionTimeout();

    bool UpdateModeNeutral();

    int LoadSslCertificates();
    void UnloadCertificates();
   
    bool CreateCommerceSession();
    bool TerminateCommerceSession();

    bool IsFetchingData() const { return m_FetchingData; }

    void CreateGamedataFolder();

    void CreateCategoryInfoFetchThread( int categoryId );

    //bool GetSkuInfo( int productIndex, SceNpCommerce2GameSkuInfo *outSkuInfo );
    bool GetSkuInfo( SceNpCommerce2GameProductInfo *productInfo, SceNpCommerce2GameSkuInfo *outSkuInfo );
 
    

    unsigned m_ContextId;
    unsigned m_RequestId;
    SceNpCommerce2SessionInfo m_SessionInfo;

    //Event Handlers
    static void Commerce2Handler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg);
    static void Commerce2CreateSessionHandler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg );
    static void Commerce2CheckoutHandler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg );
    static void Commerce2CodeEntryHandler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg );
    static void Commerce2DownloadListHandler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg );

    //Temp mem stores and accessors until I get given this memory properly.
    void InitialiseSslPool();
    int GetSslPoolSize();
    char* GetSslPool()  { return mp_SslPool; }
    
    void InitialiseHttpPool();
    int GetHttpPoolSize();
    char* GetHttpPool() { return mp_HttpPool; }
    
    void InitialiseCookiePool();
    int GetCookiePoolSize();
    char* GetCookiePool() { return mp_CookiePool; }    
    
    void InitialiseCommerceResultAllocation();
    int GetCommerceResultAllocationSize();
    char* GetCommerceResultAllocation();

    sys_memory_container_t GetCommerceCheckoutUtilAllocation();

    void ReleaseMemoryPools();

    

    char* mp_SslPool;
    char* mp_HttpPool;
    char* mp_CookiePool;
    CellHttpsData m_CaList;

    char* mp_CommerceResultAllocation;
    sys_memory_container_t m_CommerceCheckoutAllocation;

    bool m_bSessionCreated;

    //Data extracted from the sony CategoryContentResult
    SceNpCommerce2CategoryInfo m_CategoryInfo;
    
    SceNpCommerce2GetCategoryContentsResult m_CategoryContentResult;
    bool m_CategoryContentResultInitialised;

    //Thread stuff for blocking NP functions

    static void FetchDataThread( void *pData );
    sysIpcMutex GetFetchDataMutex() { return m_FetchDataMutex; }
    

    sysIpcThreadId m_DataFetchThreadId;
    sysIpcMutex m_FetchDataMutex;
    bool m_FetchingData;

    
    
    bool m_IsInitialised;
    bool m_IsContextCreated;

    
    bool m_NeedToRequeryData;

    //Meta Data functions
    void UpdateMetadataWithEnumeratedContent( SceNpCommerce2GameProductInfo *a_GameProductInfo, bool a_IsOwned );
    void AddNewMetadataRecordForGameProductInfo( SceNpCommerce2GameProductInfo *a_GameProductInfo );

    void UpdateCommerceDataWithEnumeratedContent( const char* a_ProductIdentifier, bool a_IsOwned );

    void DoLanguageSubstitutionPass();

    

    //Meta Data variables
    enum
    {
        MAX_NUM_META_DATA_RECORDS = 40,
        META_DATA_NAME_LEN = 256,
        META_DATA_SHORT_DESC_LEN = 64,
        META_DATA_LONG_DESC_LEN = 4096,
        META_DATA_PRODUCT_ID_LEN = 48
    };

	//H@ck to get the ps3 compileing
	enum
	{
		LANGSUB_CODE_LENGTH = 10,
	};
	//END H@ck  Remove this stuff.

    enum e_MetaDataParamOrder
    {
        META_DATA_PARAM_TITLE = 0,
        META_DATA_PARAM_ID,
        META_DATA_PARAM_LONGDESC,
        META_DATA_PARAM_LINKED_ID,
        META_DATA_PARAM_DEPENDANT_ID,
        META_DATA_PARAM_EXCLUDED_ID,
        META_DATA_PARAM_DISPLAY_PRICE,
        META_DATA_PARAM_RELEASE_DATE,
        META_DATA_PARAM_INCLUDED_BIG_MASK,
        META_DATA_PARAM_LANG_CODE,
        META_DATA_PARAM_NUM_PARAMS
    };

    struct OfferMetaData
    {
        
        char productName[ META_DATA_NAME_LEN ];
        char productIdentifier[ META_DATA_SHORT_DESC_LEN ];
        char longProductDesc[ META_DATA_LONG_DESC_LEN ];
        
        char linkedProductId[ META_DATA_PRODUCT_ID_LEN ];
        char dependantProductId[ META_DATA_PRODUCT_ID_LEN ];
        char excludedProductId[ META_DATA_PRODUCT_ID_LEN ];

        char skuId[ SCE_NP_COMMERCE2_SKU_ID_LEN + 1 ];

        unsigned displayPrice;
        char releaseDay;
        char releaseMonth;
        short releaseYear;
        unsigned containedDLCMask;  

        //This is for actual backend products
        CellRtcTick releaseTick;

        bool isVirtualProduct;
        bool hasValidSku;

        bool hasPurchased;
        bool isPurchasable;
    };

    const OfferMetaData *GetOfferInfo( const cCommerceProductData* pProductInfo ) const;
    int FindExistingRecord( OfferMetaData &a_RecordToMatch );
    void DoCommerceProductInfoRequest( OfferMetaData* offerData );
    
    void FixHTMLLineBreaks( OfferMetaData *productData );
    void RemoveHTMLTags( OfferMetaData *productData );
    
    OfferMetaData *m_pCurrentRequestOffer;
    OfferMetaData *m_pLastInfoRequestOffer;
    OfferMetaData m_MetaDataRecords[ MAX_NUM_META_DATA_RECORDS ];
    
    int m_NumMetaDataRecords;
    
    bool m_DataOperationAborted;

    atString m_LineBreakSubsString;
};






static void Commerce2Handler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg);
static void Commerce2CreateSessionHandler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg);

} //namespace rage

#endif //__PS3

#endif //PS3NPCOMMERCEUTIL
