#ifndef ARVATO_COMMERCE_H_
#define ARVATO_COMMERCE_H_

#if __WIN32PC

#include "atl/string.h"
#include "atl/map.h"

#include "ArvatoHttpTask.h"

namespace rage
{
	enum eArvatoState
	{
		AS_IDLE,
		AS_DOWNLOADING_CATALOGUE,
		AS_CHECKOUT,
		AS_ERROR
	};

	class cArvatoCommerce
	{
	public:
		cArvatoCommerce();

		void Init(int localGamerIndex = 0);
		void Update();
		void Shutdown();

		bool RetrieveCatalogue();
		bool Checkout(const char* skuName, const char* authToken);

		bool IsRequstInProgress()		{ return m_RequestInProgress; }
		bool IsCheckoutInProgress()		{ return m_CurrentState == AS_CHECKOUT; }

		char* GetCurrentCheckoutURL()	{ return m_CurrentCheckoutURL; }

		eArvatoState GetState()			{ return m_CurrentState; }

		void GetPurchasedProducts(atArray<u32> *purchasedProducts);
		cArvatoProduct*	GetProduct(const char* skuName);

		bool IsContentAvailable()	{ return m_ArvatoProducts.GetNumUsed() > 0; }

	private:
		void UpdateCatalogueRetrieval();
		void UpdateCheckout();

	private:
		bool	m_Initialized;
		bool	m_RequestInProgress;
		int		m_LocalGamerIndex;

		char	m_Culture[64];
		char	m_CurrentCheckoutURL[256];

		netStatus						m_ArvatoHttpStatus;
		eArvatoState					m_CurrentState;
		atMap<u32, cArvatoProduct*>		m_ArvatoProducts;
	};
}
#endif

#endif

