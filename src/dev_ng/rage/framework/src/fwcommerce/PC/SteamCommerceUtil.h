#ifndef _STEAM_COMMERCE_H_
#define _STEAM_COMMERCE_H_

#if __WIN32PC && __STEAM_BUILD
#pragma once

#include "fwcommerce/CommerceUtil.h"
#include "rgsc/rgsc/rgsc/public_interface/commerce_interface.h"

#include "../../3rdParty/Steam/public/steam/steam_api.h"

#include "rline/entitlement/rlrosentitlement.h"
#include "rline/entitlement/rlrosentitlementtask.h"
#include "atl/map.h"

namespace rage
{

class cSteamProduct
{
public:
	AppId_t AppID;
	bool	IsAvailable;
	char	dlcName[256];

	cSteamProduct()
	{
		AppID = 0;
		IsAvailable = false;
		dlcName[0] = '\0';
	}
};

class cSteamCommerceManager : public cCommerceUtil
{
public:
	static cSteamCommerceManager* Instance();
	~cSteamCommerceManager();

	virtual bool	Init( int userIndex = 0, const char *skuFolder = NULL );
	void	Shutdown();
	void	Update();

	virtual void	CheckoutProduct( unsigned a_Index );
	virtual void	CheckoutProduct( const cCommerceProductData* );

	virtual float	GetProductPrice( const cCommerceProductData*, char*, unsigned  ) const { return -1.0f; }

	virtual bool	IsProductPurchasable( const cCommerceProductData* a_pProductData) const;
	virtual bool	IsProductPurchasable( int a_Index ) const;

	virtual bool	IsProductPurchased( int a_Index ) const;
	virtual bool	IsProductPurchased( const cCommerceProductData* a_pProductData ) const;

	virtual void	DownloadProduct(const cCommerceProductData* a_pProductData);

	virtual bool	IsContentInfoPopulated() const;

	virtual bool	DoPlatformDataRequest();
	virtual void	MergePlatformAndRosData();
	virtual bool	ProductCodeEntry();

	AppId_t			GetSteamAppID(const cCommerceProductData*) const;
	AppId_t			GetSteamAppID(const char* appID) const;

	cSteamProduct*	GetSteamProduct(AppId_t appID);

	bool	IsErrorEncountered() const { return m_CurrentState == MODE_ERROR_ENCOUNTERED; }
	bool	IsInRedemption() const { return (m_CurrentState == MODE_REDEEMING); }

	enum eCommerceMode {
		MODE_TOP = 0,
		MODE_ENUMERATE_CONTENT,
		MODE_ENUMERATE_MANUAL_CONTENT,
		MODE_ERROR_ENCOUNTERED,
		MODE_NEUTRAL,
		MODE_STEAM_UI,
		MODE_REDEEMING,
		MODE_REDEEM_UI,
		MODE_MAX
	};

	enum eRedemptionCodeMessage
	{
		REDEEM_ITEM_UNLOCKED = 36,
		REDEEM_ITEM_DOWNLOAD = 37,
		REDEEM_CODE_IN_USE = 38,
		REDEEM_INVALID_CODE = 39,
		REDEEM_GENERIC_ERROR = 40
	};

private:

	cSteamCommerceManager();

	//Static singleton instance of this class
	static cSteamCommerceManager* m_Instance;

	static void OnBladeUIRedeemCodeSubmit( const char* redemptionCode, const rgsc::ICommerceManagerLatestVersion::RedemptionAction action );
	static void OnBladeUIRedeemCodeSubmitEmpty( const char* redemptionCode, const rgsc::ICommerceManagerLatestVersion::RedemptionAction action );

	void SetRedeemCode(const char* redeemCode);
	void SubmitRedeemCode();

	virtual bool		ContentNeedsActivation() const { return m_ContentNeedsActivation; }
	virtual void		SetContentNeedsActivation(bool needsActivation) { m_ContentNeedsActivation = needsActivation; }
	virtual const char*	GetCurrentActivationCode() const { return m_RedeemCode; }
	void				SetActivationComplete(bool success, long resultCode);

	virtual void		SetGameshieldProjectID(int lpjID);

    void UpdateCommerceDataWithEnumeratedContent();

	void UpdateRedemption();
	void UpdateEnumeration();

	void TriggerRosSync();
   
	atMap<AppId_t, cSteamProduct*> m_SteamProducts;

	// RGSC UI redeem code interface pointer
	rgsc::ICommerceManagerLatestVersion* m_RedeemCodeInterface;

	bool m_ContentInfoPopulated;

	bool m_Initialised;
	bool m_OverlayEnabled;
	eCommerceMode m_CurrentState;

	char m_RedeemCode[64];
	bool m_SubmitRedeemCode;
	netStatus m_RedeemCodeStatus;

	bool m_ContentNeedsActivation;
	bool m_RegisteringRedeemCode;
	bool m_ActivatingRedeemCode;

	int m_CurrentCommerceUserIndex;
	int m_gameshieldProjectID;

	bool m_IsRedeemCodeActivated;
	char m_RedeemCodeSkuName[256];
};

} // namespace rage

#endif // __WIN32PC
#endif // _STEAM_COMMERCE_H_
