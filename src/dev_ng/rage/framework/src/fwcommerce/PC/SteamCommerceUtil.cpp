#include "file/file_config.h"

//Fix for the missing __STEAM define
#ifndef __STEAM_BUILD
#define __STEAM_BUILD 0
#endif

#if __WIN32PC && __STEAM_BUILD

#include "fwcommerce/pc/SteamCommerceUtil.h"
#include "../../3rdParty/Steam/public/steam/steam_api.h"
#include "../CommerceChannel.h"

#include "system/xtl.h"

#include "rline/rlpc.h"
#include "rline/rltitleid.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "rline/entitlement/rlrosentitlementtask.h"

#include "system/memory.h"

#include "rgsc/rgsc/rgsc/public_interface/commerce_interface.h"

namespace rage
{

cSteamCommerceManager* cSteamCommerceManager::m_Instance = NULL;

//----------------------------------------------------------
cSteamCommerceManager::cSteamCommerceManager() :
	m_RedeemCodeInterface(NULL),
	m_ContentInfoPopulated(false),
	m_Initialised(false),
	m_OverlayEnabled(false),
	m_CurrentState(MODE_NEUTRAL),
	m_SubmitRedeemCode(false),
	m_CurrentCommerceUserIndex(-1)
{
	if ( m_Instance == NULL )
	{
		m_Instance = this;
	}
}

//----------------------------------------------------------
cSteamCommerceManager::~cSteamCommerceManager()
{
	Shutdown();

	m_Instance = NULL;
}

//----------------------------------------------------------
cSteamCommerceManager* cSteamCommerceManager::Instance()
{
	if ( m_Instance == NULL )
	{
		m_Instance = rage_new cSteamCommerceManager;
	}

	return m_Instance;
}

//----------------------------------------------------------
bool cSteamCommerceManager::Init( int userIndex, const char* skuFolder)
{
	if ( m_Initialised )
	{
		commerceErrorf("Trying to initialise the commerce manager when it has already been intialised.");
		commerceAssert( false );
		return false;
	}

	m_ContentInfoPopulated = false;
	m_Initialised = true;
	m_CurrentState = MODE_NEUTRAL;

	m_RedeemCode[0] = '\0';
	m_SubmitRedeemCode = false;

	m_ContentNeedsActivation = false;
	m_RegisteringRedeemCode = false;
	m_ActivatingRedeemCode = false;
	m_IsRedeemCodeActivated = false;

	m_RedeemCodeSkuName[0] = '\0';

	m_CurrentCommerceUserIndex = userIndex;
	m_OverlayEnabled = SteamUtils()->IsOverlayEnabled();

	m_RedeemCodeInterface = g_rlPc.GetCommerceManager();

	if (!m_RedeemCodeInterface)
	{
		commerceErrorf("Could not get a handle to the redemption code interface");
		commerceAssert( false );
		return false;
	}

	cCommerceUtil::Init(userIndex, skuFolder);

	return true;
}

//----------------------------------------------------------
void cSteamCommerceManager::Shutdown()
{
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to shutdown commerce manager without initialising." );
		return;
	}

	m_RedeemCodeInterface = NULL;
	m_ContentInfoPopulated = false;
	m_Initialised = false;
	m_OverlayEnabled = false;
	m_CurrentState = MODE_NEUTRAL;
	m_SubmitRedeemCode = false;
	m_ActivatingRedeemCode = false;
	m_SubmitRedeemCode = false;
	m_CurrentCommerceUserIndex = -1;
	m_SteamProducts.Reset();

	cCommerceUtil::Shutdown();
}

//----------------------------------------------------------
void cSteamCommerceManager::Update()
{
    cCommerceUtil::Update();

	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call Update without initialising commerce manager." );
		Assert( false );
		return;
	}

	switch (m_CurrentState)
	{
	case MODE_REDEEMING:
		UpdateRedemption();
		break;
	case MODE_ENUMERATE_CONTENT:
		UpdateEnumeration();
		break;
	default:
		break;
	}

	
}

//----------------------------------------------------------
void cSteamCommerceManager::CheckoutProduct( unsigned a_Index )
{
	Assert( m_CurrentState == MODE_NEUTRAL );

	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call CheckoutProduct without initialising commerce manager." );
		Assert( false );
		return ;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return;
	}

	return CheckoutProduct( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

//----------------------------------------------------------
void cSteamCommerceManager::CheckoutProduct( const cCommerceProductData* a_pProductdata )
{
	if ( !a_pProductdata || !a_pProductdata->GetEnumeratedFlag() )
	{
		return;
	}

	// TODO - Right now store overlay just shows our main game AppID
	//u32 steamAppID = GetSteamAppID(a_pProductdata);
	u32 steamAppID = 204100;
	if (true)
	{
		SteamFriends()->ActivateGameOverlayToStore((AppId_t)steamAppID);
	}
	else
	{
		m_CurrentState = MODE_ERROR_ENCOUNTERED;
		commerceErrorf( "Steam - Invalid AppID for PlatformId: %s", a_pProductdata->GetPlatformId().c_str() );
		Assert( false );
		return;
	}
}

//Removed to make the steam version use the base classes function for now, since Steam enumeration is not being used.
//NOTE!! This may need to be re-enabled in the future.
/*
//----------------------------------------------------------
bool cSteamCommerceManager::IsProductInstalled( const cCommerceProductData* a_pProductdata ) const
{
	if ( !a_pProductdata || !a_pProductdata->GetEnumeratedFlag() )
	{
		return false;
	}

	u32 steamAppId = GetSteamAppID(a_pProductdata);
	if (steamAppId != 0)
	{
		return SteamApps()->BIsDlcInstalled((AppId_t)steamAppId);
	}
	else
	{
		commerceErrorf( "Steam - Invalid AppID for PlatformId: %s", a_pProductdata->GetPlatformId().c_str() );
		Assert( false );
		return false;
	}
}
*/
//----------------------------------------------------------
bool cSteamCommerceManager::IsProductPurchasable( int a_Index ) const
{
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
		Assert( false );
		return false;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return false;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return false;
	}

	return IsProductPurchasable( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

//----------------------------------------------------------
bool cSteamCommerceManager::IsProductPurchasable( const cCommerceProductData* a_pProductData) const
{
	return a_pProductData->GetEnumeratedFlag();
}

//----------------------------------------------------------
bool cSteamCommerceManager::IsProductPurchased( int a_Index ) const
{
	Assert( IsContentInfoPopulated() );
	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );

	// TODO - make sure index can't go outside of the array (everywhere)
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return false;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return false;
	}

	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
		Assert( false );
		return false;
	}

	return IsProductPurchased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

//----------------------------------------------------------
bool cSteamCommerceManager::IsProductPurchased( const cCommerceProductData* a_pProductData ) const
{
	rlRosEntitlementData* entitlement = rlRosEntitlement::GetCurrentEntitlementData();

	if (!entitlement)
	{
		return false;
	}

	rlRosEntitlementSku* sku = entitlement->GetSkuByName(a_pProductData->GetPlatformId());

	if (!sku)
	{
		return false;
	}

	return true;
}

//----------------------------------------------------------
void cSteamCommerceManager::DownloadProduct(const cCommerceProductData* a_pProductData)
{
	if (!a_pProductData)
	{
		commerceErrorf("Could not download product");
		return;
	}

	AppId_t appID = GetSteamAppID(a_pProductData);

	SteamApps()->InstallDLC(appID);
}



//----------------------------------------------------------

void cSteamCommerceManager::UpdateCommerceDataWithEnumeratedContent( )
{
    for ( int i = 0; i < m_CommerceData.GetSizeOfDataArray(); i++ )
    {
        if ( m_CommerceData.GetItemData( i )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
        {
            //Not a product, ergo does not need to be checked.
            continue;
        }

        cCommerceProductData* productData = static_cast<cCommerceProductData*>(m_CommerceData.GetItemData( i ));
        AppId_t appID = GetSteamAppID(productData);
        cSteamProduct** steamProduct = m_SteamProducts.Access(appID);

        if (productData && steamProduct && *steamProduct)
        {
            if ((*steamProduct)->IsAvailable)
                productData->SetEnumeratedFlag(true);
        }
    }
}

void cSteamCommerceManager::MergePlatformAndRosData()
{
	rlRosEntitlementData* entitlementData = rlRosEntitlement::GetCurrentEntitlementData();

    
    UpdateCommerceDataWithEnumeratedContent();
    

	if (!entitlementData)
	{
		return;
	}

	for ( int s=0; s <  m_CommerceData.GetSizeOfDataArray(); s++ )
	{
		cCommerceItemData* itemData = m_CommerceData.GetItemData( s );

        commerceAssert( itemData );

		//By default the dependencys are set to having been found
		itemData->SetDependencyFlag( true );

		for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetDependencyFlag( false );

			atString dependency = itemData->GetDependencyIds()[ iDeps ];
			/*
            AppId_t depdendencyAppID = GetSteamAppID(dependency.c_str());
			cSteamProduct* product = GetSteamProduct(depdendencyAppID);

			if (!product)
				continue;
            */
			rlRosEntitlementSku* sku = entitlementData->GetSkuByName(dependency.c_str());

			if (!sku)
				continue;

			if (/*product &&*/ sku)
			{
				itemData->SetDependencyFlag( true );
			}
		}

		//By default enumeration dependencies are set to true
		itemData->SetEnumDependencyFlag( true );

		for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetEnumDependencyFlag( false );

			atString enumDependencySku = itemData->GetEnumDependencyIds()[ iEnumDeps ];
			AppId_t enumDepdendencyAppID = GetSteamAppID(enumDependencySku.c_str());
			cSteamProduct* product = GetSteamProduct(enumDepdendencyAppID);

			if (product)
			{
				itemData->SetEnumDependencyFlag( true );
			}
		}

		//By default the exclusions flag is set to false (no exclusions found)
		itemData->SetExclusionFlag( false );

		for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetExclusionFlag( false );

			atString exclusionSku = itemData->GetExclusionIds()[ iExcs ];
/*
            AppId_t exclusionAppID = GetSteamAppID(exclusionSku.c_str());
			cSteamProduct* product = GetSteamProduct(exclusionAppID);

			if (!product)
				continue;
*/
			rlRosEntitlementSku* sku = entitlementData->GetSkuByName(exclusionSku.c_str());

			if (!sku)
				continue;

			if (/*product &&*/ sku)
			{
				itemData->SetExclusionFlag( true );
			}
		}

		//By default the enum exclusions flag is set to false (no exclusions found)
		itemData->SetEnumExclusionFlag( false );

		for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetEnumExclusionFlag( false );

			atString enumExclusionSku = itemData->GetEnumExclusionIds()[ iEnumExcs ];
			AppId_t enumExclusionAppID = GetSteamAppID(enumExclusionSku.c_str());
			cSteamProduct* product = GetSteamProduct(enumExclusionAppID);

			if (!product)
				continue;

			rlRosEntitlementSku* sku = entitlementData->GetSkuByName(enumExclusionSku.c_str());

			if (!sku)
				continue;

			if (product && sku)
			{
				itemData->SetEnumExclusionFlag( true );
			}
		}
	}
}

//----------------------------------------------------------
bool cSteamCommerceManager::DoPlatformDataRequest()
{
	Assert(m_CurrentState == MODE_NEUTRAL);

	m_ContentInfoPopulated = false;

	rlRosEntitlement::RefreshEntitlementData(m_CurrentCommerceUserIndex);

	m_SteamProducts.Reset();

	int dlcCount = SteamApps()->GetDLCCount();
	for (int i = 0; i < dlcCount; i++)
	{
		cSteamProduct* steamProduct = rage_new cSteamProduct();

		if (steamProduct)
		{
			if (SteamApps()->BGetDLCDataByIndex(i, &steamProduct->AppID, &steamProduct->IsAvailable, steamProduct->dlcName, sizeof(steamProduct->dlcName)))
			{
				m_SteamProducts.Insert(steamProduct->AppID, steamProduct);
			}
			else
			{
				delete steamProduct;
			}
		}
	}

	// Still wait for entitlement to update
	m_CurrentState = MODE_ENUMERATE_CONTENT;

	return true;
}

//----------------------------------------------------------
bool cSteamCommerceManager::IsContentInfoPopulated() const
{
	return ( m_ContentInfoPopulated && cCommerceUtil::IsContentInfoPopulated() );
}

//----------------------------------------------------------
bool cSteamCommerceManager::ProductCodeEntry()
{
	// TODO - Temporary UI to show a product code entry box.
	if (g_rlPc.IsUiShowing() == false && m_RedeemCodeInterface)
	{
		m_CurrentState = MODE_REDEEM_UI;
		m_RedeemCodeInterface->ShowRedemptionCodeUi(OnBladeUIRedeemCodeSubmit);
		return true;
	}

	return false;
}

//----------------------------------------------------------
void cSteamCommerceManager::OnBladeUIRedeemCodeSubmit( const char* redemptionCode, const rgsc::ICommerceManagerLatestVersion::RedemptionAction action )
{
	// TODO - Temporary UI to show a product code entry box.
	if (redemptionCode != NULL && action == rgsc::ICommerceManagerLatestVersion::REDEMPTION_ENTERED_CODE)
	{
		commerceDebugf3("Redeem Code: %s", redemptionCode);
		cSteamCommerceManager::Instance()->SetRedeemCode(redemptionCode);
		cSteamCommerceManager::Instance()->m_CurrentState = MODE_REDEEMING;
	}
	else if (action == rgsc::ICommerceManagerLatestVersion::REDEMPTION_USER_CANCELLED && cSteamCommerceManager::Instance()->m_CurrentState != MODE_REDEEMING)
	{
		cSteamCommerceManager::Instance()->m_CurrentState = MODE_NEUTRAL;
	}
}

//----------------------------------------------------------
void cSteamCommerceManager::SetRedeemCode(const char* redeemCode)
{
	if (!redeemCode)
	{
		commerceErrorf("Invalid redeem code");
		m_SubmitRedeemCode = false;
		return;
	}

	safecpy(m_RedeemCode, redeemCode, sizeof(m_RedeemCode));
	m_SubmitRedeemCode = true;
}

//----------------------------------------------------------
void cSteamCommerceManager::SubmitRedeemCode()
{
	int redeemCodeLength = strlen(m_RedeemCode);

	if ( redeemCodeLength == 0 || redeemCodeLength > 19)
	{
		commerceErrorf("Malformed redeem code");

		if (m_RedeemCodeInterface)
			m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_INVALID_CODE, OnBladeUIRedeemCodeSubmitEmpty);

		m_CurrentState = MODE_ERROR_ENCOUNTERED;
		return;
	}

	if (!rlRosEntitlement::RegisterPurchaseBySN(m_CurrentCommerceUserIndex, m_gameshieldProjectID, m_RedeemCode, &m_IsRedeemCodeActivated, m_RedeemCodeSkuName, sizeof(m_RedeemCodeSkuName), &m_RedeemCodeStatus))
	{
		commerceErrorf("Could not register serial number on ROS services");
		m_ErrorCode = CERROR_REDEMPTION;
		m_CurrentState = MODE_ERROR_ENCOUNTERED;
		return;
	}

	m_RegisteringRedeemCode = true;
}

//----------------------------------------------------------
void cSteamCommerceManager::SetActivationComplete(bool success, long UNUSED_PARAM(resultCode))
{
	if (success)
	{
		if (!rlRosEntitlement::RegisterActivationBySN(m_CurrentCommerceUserIndex, m_gameshieldProjectID, m_RedeemCode, "", &m_RedeemCodeStatus))
		{
			commerceErrorf("Could not activate serial number on ROS services");
			m_ErrorCode = CERROR_REDEMPTION;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			return;
		}

		m_ActivatingRedeemCode = true;
	}
}

//----------------------------------------------------------
void cSteamCommerceManager::SetGameshieldProjectID(int lpjID)
{
	m_gameshieldProjectID = lpjID;
}

//----------------------------------------------------------
void cSteamCommerceManager::UpdateRedemption()
{
	if (m_SubmitRedeemCode)
	{
		m_SubmitRedeemCode = false;
		SubmitRedeemCode();
	}

	if (m_RegisteringRedeemCode)
	{
		if (m_RedeemCodeStatus.Succeeded())
		{
			m_RegisteringRedeemCode = false;
			SetContentNeedsActivation(true);
		}
		else if (m_RedeemCodeStatus.Failed())
		{
			if (m_RedeemCodeStatus.GetResultCode() == RLEN_ERROR_SERIAL_ALREADY_ACTIVATED)
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_CODE_IN_USE, OnBladeUIRedeemCodeSubmitEmpty);
			}
			else if (m_RedeemCodeStatus.GetResultCode() == RLEN_ERROR_SERIAL_DOES_NOT_EXIST)
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_INVALID_CODE, OnBladeUIRedeemCodeSubmitEmpty);
			}
			else
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_GENERIC_ERROR, OnBladeUIRedeemCodeSubmitEmpty);
			}

			// Already redeemed code or could not connect to server.
			m_ErrorCode = CERROR_REDEMPTION;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_RegisteringRedeemCode = false;
		}
	}

	if (m_ActivatingRedeemCode)
	{
		if (m_RedeemCodeStatus.Succeeded())
		{
			if (m_RedeemCodeInterface)
				m_RedeemCodeInterface->RedemptionCodeResult(1, REDEEM_ITEM_UNLOCKED, OnBladeUIRedeemCodeSubmitEmpty);

			// Re-scan entitlement
			m_CurrentState = MODE_NEUTRAL;
			m_ActivatingRedeemCode = false;
		}
		else if (m_RedeemCodeStatus.Failed())
		{
			if (m_RedeemCodeStatus.GetResultCode() == RLEN_ERROR_SERIAL_ALREADY_ACTIVATED)
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_CODE_IN_USE, OnBladeUIRedeemCodeSubmitEmpty);
			}
			else
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_GENERIC_ERROR, OnBladeUIRedeemCodeSubmitEmpty);
			}

			// Already redeemed code or could not connect to server.
			m_ErrorCode = CERROR_REDEMPTION;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_ActivatingRedeemCode = false;
		}		
	}
}

//----------------------------------------------------------
void cSteamCommerceManager::OnBladeUIRedeemCodeSubmitEmpty( const char* UNUSED_PARAM(redemptionCode), const rgsc::ICommerceManagerLatestVersion::RedemptionAction UNUSED_PARAM(action) )
{
	// Empty callback for redeem code result
}
//----------------------------------------------------------
void cSteamCommerceManager::UpdateEnumeration()
{
	if (!rlRosEntitlement::IsCheckingForEntitlement())
	{
		if (rlRosEntitlement::IsEntitlementUpToDate())
		{
			m_CurrentState = MODE_NEUTRAL;
			m_ContentInfoPopulated = true;
		}
		else
		{
			m_ErrorCode = CERROR_STEAM_ENTITLEMENT;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_ContentInfoPopulated = false;
		}
	}
}

//----------------------------------------------------------
AppId_t cSteamCommerceManager::GetSteamAppID( const cCommerceProductData* a_pProductdata ) const
{
	if (!a_pProductdata)
		return (AppId_t)0;

	atString platformId = a_pProductdata->GetPlatformId();
	return (AppId_t)atoi(platformId.c_str());
}

//----------------------------------------------------------
AppId_t cSteamCommerceManager::GetSteamAppID( const char* appID ) const
{
	if (!appID)
		return (AppId_t)0;

	return (AppId_t)atoi(appID);
}

//----------------------------------------------------------
cSteamProduct* cSteamCommerceManager::GetSteamProduct(AppId_t appID)
{
	if (appID == 0)
	{
		commerceErrorf("Could not find steam product AppID: %d", appID);
		return NULL;
	}

	cSteamProduct** product = m_SteamProducts.Access(appID);

	if (product && *product)
		return *product;

	return NULL;
}

//----------------------------------------------------------
void cSteamCommerceManager::TriggerRosSync()
{
	// Tell ROS to go check for Steam entitlement

	/*
	const rlRosCredentials& cred = rlRos::GetCredentials(m_CurrentCommerceUserIndex);
	cred.GetTicket();
	*/
}

} // namespace rage

#endif // __WIN32PC