#ifndef ARVATO_HTTP_TASK
#define ARVATO_HTTP_TASK

#if __WIN32PC

#include "atl/map.h"
#include "atl/string.h"
#include "rline/rlhttptask.h"
#include "parser/tree.h"

namespace rage
{

class cArvatoProduct
{
public:
	atString	m_ProductName;
	atString	m_ProductSku;
	float		m_Price;
	bool		m_Purchased;

	bool IsPurchased() { return m_Purchased; }
};

class rlArvatoBaseHttpTask : public rlHttpTask
{
public:
	RL_TASK_DECL(rlArvatoBaseHttpTask);

	bool Configure();

protected:
	virtual bool UseHttps() const { return true; }

	virtual const char* GetUrlHostName();
	virtual bool GetServicePath(char* svcPath, const unsigned maxLen);
	virtual const char* GetServiceMethod() const = 0;

	virtual bool ProcessResponse(const char* response, int& resultCode);
	virtual bool ProcessSuccess(const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const parTreeNode* node);

	virtual bool GenerateRequestHash(char* request, unsigned int len, char* hash, unsigned int hashLength);
};

class rlArvatoCatalogHttpTask : public rlArvatoBaseHttpTask
{
public:
	RL_TASK_DECL(rlArvatoCatalogHttpTask);

	bool Configure(const char* culture, atMap<u32, cArvatoProduct*> *catalogListing);

protected:
	virtual const char* GetServiceMethod() const;

	virtual bool ProcessSuccess(const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const parTreeNode* node);

	atMap<u32, cArvatoProduct*>	*m_CatalogListing;
};

class rlArvatoCheckoutHttpTask : public rlArvatoBaseHttpTask
{
public:
	RL_TASK_DECL(rlArvatoCheckoutHttpTask);

	bool Configure(	const char* culture, 
					const char* user,
					const char* productSku, 
					char* checkoutUrl,
					unsigned int checkoutUrlSize );

protected:
	virtual const char* GetServiceMethod() const;

	virtual bool ProcessSuccess(const parTreeNode* node, int& resultCode);
	virtual void ProcessError(const parTreeNode* node);

	char*			m_CheckoutUrl;
	unsigned int	m_CheckoutUrlLen;
};

} // namespace rage

#endif // __WIN32PC

#endif // ARVATO_HTTP_TASK


