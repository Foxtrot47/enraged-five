#if __WIN32PC

#include "ArvatoCommerce.h"
#include "ArvatoHttpTask.h"

#include "atl/map.h"
#include "atl/string.h"
#include "diag/seh.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "system/xtl.h"
#include "string/stringhash.h"

#include "rline/rl.h"
#include "rline/rlpc.h"
#include "rline/ros/rlros.h"

#include "../CommerceChannel.h"

#define XML_ENTRY_LENGTH	256

namespace rage
{

#define RLARVATO_CREATETASK(T, status, ...) 												\
	bool success = false;																	\
	rlTaskBase* task = NULL;                                                                \
	rtry																					\
	{																						\
		T* newTask = rlGetTaskManager()->CreateTask<T>();									\
		rverify(newTask,catchall,);															\
		task = newTask;																		\
		rverify(rlTaskBase::Configure(newTask, __VA_ARGS__, status),						\
					catchall, rlError("Failed to configure task"));                         \
		rverify(rlGetTaskManager()->AppendSerialTask(task), catchall, );       			    \
		success = true;	                                                                    \
	}																						\
	rcatchall																				\
	{																						\
		if(task)																			\
		{																					\
			rlGetTaskManager()->DestroyTask(task);								            \
		}																					\
	}																						\
	return success;	

cArvatoCommerce::cArvatoCommerce() :
	m_Initialized(false),
	m_RequestInProgress(false),
	m_CurrentState(AS_IDLE)
{
	
}

void cArvatoCommerce::Init(int localGamerIndex)
{
	m_Initialized = true;
	m_CurrentState = AS_IDLE;
	m_LocalGamerIndex = localGamerIndex;

	m_ArvatoProducts.Reset();
	m_ArvatoHttpStatus.Reset();

	const char* languageCode = rlGetLanguageCode(rlGetLanguage());

	if (!languageCode)
	{
		commerceErrorf("Invalid array to fill with purchased products");
		return;
	}

	safecpy(m_Culture, languageCode);
}

void cArvatoCommerce::Shutdown()
{
	m_Initialized = false;
	m_CurrentState = AS_IDLE;
	m_LocalGamerIndex = -1;

	m_ArvatoProducts.Reset();
	
	// Kill any pending HTTP requests.
	if (m_ArvatoHttpStatus.Pending())
	{
		rlGetTaskManager()->CancelTask(&m_ArvatoHttpStatus);
	}
}

void cArvatoCommerce::Update()
{
	switch(m_CurrentState)
	{
	case AS_DOWNLOADING_CATALOGUE:
		UpdateCatalogueRetrieval();
		break;
	case AS_CHECKOUT:
		UpdateCheckout();
		break;
	default:
		break;
	}
}

void cArvatoCommerce::GetPurchasedProducts(atArray<u32> *purchasedProducts)
{
	if (!purchasedProducts)
	{
		commerceErrorf("Invalid array to fill with purchased products");
		return;
	}

	atMap<u32, cArvatoProduct*>::Iterator iter = m_ArvatoProducts.CreateIterator();
	while(iter)
	{
		cArvatoProduct *pProduct = iter.GetData();
		
		if (pProduct && pProduct->IsPurchased())
		{
			purchasedProducts->PushAndGrow(atStringHash(pProduct->m_ProductSku));
		}
	}
}

cArvatoProduct*	cArvatoCommerce::GetProduct(const char* skuName)
{
	if (!skuName)
	{
		commerceErrorf("Invalid SKU name for product");
		return NULL;
	}

	u32 hash = atStringHash(skuName);
	cArvatoProduct** product = m_ArvatoProducts.Access(hash);

	if (product && *product)
		return *product;

	return NULL;
}

bool cArvatoCommerce::RetrieveCatalogue()
{
	if (m_ArvatoHttpStatus.Pending())
	{
		commerceErrorf("Current Arvato task still pending.");
		return false;
	}

	if(!RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex))
	{
		m_CurrentState = AS_ERROR;
		return false;
	}

	const rlRosCredentials& creds = rlRos::GetCredentials( m_LocalGamerIndex );

	if (!creds.GetTicket())
	{
		m_CurrentState = AS_ERROR;
		return false;
	}

	// Clear out the products so we can re-query
	m_ArvatoProducts.Reset();

	m_CurrentState = AS_DOWNLOADING_CATALOGUE;
	m_RequestInProgress = true;

	RLARVATO_CREATETASK(rlArvatoCatalogHttpTask,
						&m_ArvatoHttpStatus,
						m_Culture,
						&m_ArvatoProducts);
}

void cArvatoCommerce::UpdateCatalogueRetrieval()
{
	Assert( m_CurrentState == AS_DOWNLOADING_CATALOGUE );

	if (m_ArvatoHttpStatus.Succeeded())
	{
		m_CurrentState = AS_IDLE;
		m_RequestInProgress = false;
		m_ArvatoHttpStatus.Reset();
	}
	else if (m_ArvatoHttpStatus.Failed() || m_ArvatoHttpStatus.Canceled())
	{
		m_CurrentState = AS_ERROR;
		m_RequestInProgress = false;
		m_ArvatoHttpStatus.Reset();
	}
}

bool cArvatoCommerce::Checkout(const char* skuName, const char* authToken)
{
	if(!RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex))
	{
		commerceErrorf("Could not find valid gamer index from SC user.");
		m_CurrentState = AS_ERROR;
		return false;
	}

	m_CurrentState = AS_CHECKOUT;
	m_RequestInProgress = true;

	// Clear the last checkout URL if there is one.
	m_CurrentCheckoutURL[0] = '\0';

	RLARVATO_CREATETASK(rlArvatoCheckoutHttpTask,
						&m_ArvatoHttpStatus,
						m_Culture,
						authToken,
						skuName,
						m_CurrentCheckoutURL,
						(unsigned int)sizeof(m_CurrentCheckoutURL));
}

void cArvatoCommerce::UpdateCheckout()
{
	Assert( m_CurrentState == AS_CHECKOUT );

	if (m_ArvatoHttpStatus.Succeeded())
	{
		m_CurrentState = AS_IDLE;
		m_RequestInProgress = false;
		m_ArvatoHttpStatus.Reset();
	}
	else if (m_ArvatoHttpStatus.Failed() || m_ArvatoHttpStatus.Canceled())
	{
		m_CurrentState = AS_ERROR;
		m_RequestInProgress = false;
		m_ArvatoHttpStatus.Reset();
	}
}

}

#endif

