#ifndef PCCOMMERCEUTIL
#define PCCOMMERCEUTIL

#if __WIN32PC
#pragma once

#include "ArvatoCommerce.h"

#include "../CommerceUtil.h"
//#include "rgsc/rgsc/rgsc/public_interface/commerce_interface.h"

#include "rline/entitlement/rlrosentitlement.h"

namespace rage
{
	class cPCCommerceManager : public cCommerceUtil
	{
	public:
		static cPCCommerceManager* Instance();
		~cPCCommerceManager();

		virtual bool	Init( int userIndex = 0, const char *skuFolder = NULL );
		void	Shutdown();
		void	Update();

		bool	DoPlatformDataRequest();
		void	MergePlatformAndRosData();

		bool	ProductCodeEntry();

		void	CheckoutProduct( unsigned a_Index );
		void	CheckoutProduct( const cCommerceProductData* );

		void	UpdateCheckout();
		void	UpdateEnumeration();
		void	UpdateRedemption();

		bool	IsProductPurchased( int a_Index ) const;
		bool	IsProductPurchased( const cCommerceProductData* a_pProductData ) const;

		bool	IsProductPurchasable( int a_Index ) const;
		bool	IsProductPurchasable( const cCommerceProductData* a_pProductData) const;

		float	GetProductPrice( unsigned a_Index, char* a_pOutputString, unsigned a_SizeOfOutputString ) const;
		float   GetProductPrice( const cCommerceProductData*, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const;

		bool	IsInCheckout() const { return ( m_CurrentState == MODE_CHECKOUT ); }
		bool	IsErrorEncountered() const { return m_CurrentState == MODE_ERROR_ENCOUNTERED; }
		bool	IsInRedemption() const { return (m_CurrentState == MODE_REDEEMING); }

		virtual bool	IsPlatformReady() const;

		virtual void	DownloadProduct( const cCommerceProductData* a_pProductData);
		virtual bool	IsDownloadingProduct(const cCommerceProductData* UNUSED_PARAM(a_pProductData)) { return m_Downloading; }
		
		virtual bool		ContentNeedsActivation() const { return m_ContentNeedsActivation; }
		virtual void		SetContentNeedsActivation(bool needsActivation) { m_ContentNeedsActivation = needsActivation; }
		virtual const char*	GetCurrentActivationCode() const { return m_RedeemCode; }
		virtual void		SetActivationComplete(bool success, long resultCode);

		virtual void		SetGameshieldProjectID(int lpjID);

		bool	IsContentInfoPopulated() const;

		void	SetRedeemCode(const char* redeemCode);
		void	SubmitRedeemCode();

		rlRosEntitlementSku* GetRosSku(const char* skuName);

		enum eCommerceMode {
			MODE_TOP = 0,
			MODE_ENUMERATE_CONTENT,
			MODE_ENUMERATE_MANUAL_CONTENT,
			MODE_ERROR_ENCOUNTERED,
			MODE_NEUTRAL,
			MODE_CHECKOUT,
			MODE_DOWNLOADING,
			MODE_REDEEMING,
			MODE_REDEEM_UI,
			MODE_MAX
		};

		enum eRedemptionCodeMessage
		{
			REDEEM_ITEM_UNLOCKED = 36,
			REDEEM_ITEM_DOWNLOAD = 37,
			REDEEM_CODE_IN_USE = 38,
			REDEEM_INVALID_CODE = 39,
			REDEEM_GENERIC_ERROR = 40
		};

	private:

		cPCCommerceManager();

		//Static singleton instance of this class
		static cPCCommerceManager* m_Instance;
		
		// RGSC UI redeem code interface pointer
		rgsc::ICommerceManagerLatestVersion* m_RedeemCodeInterface;
		static void OnBladeUIRedeemCodeSubmit( const char* redemptionCode, const rgsc::ICommerceManagerLatestVersion::RedemptionAction action);
		static void OnBladeUIRedeemCodeSubmitEmpty( const char* redemptionCode, const rgsc::ICommerceManagerLatestVersion::RedemptionAction action );

		// Downloader progress callbacks
		static void	ProductDownloadProgress(int progress, const char* skuName);
		static void	ProductDownloadComplete(const char* skuName);
		static void ProductDownloadError(const char* errorCode, const char* skuName);

        void UpdateCommerceDataWithEnumeratedContent();

		bool m_Initialised;
		bool m_Downloading;

		int m_CurrentCommerceUserIndex;

		bool m_ContentInfoPopulated;

		bool m_ContentNeedsActivation;

		eCommerceMode m_CurrentState;

		cArvatoCommerce* m_ArvatoCommerce;

		const cCommerceProductData*	m_CurrentCheckoutProduct;
		bool						m_CheckingOut;
		bool						m_CheckoutUIShowing;
		bool						m_CheckoutUIWaiting;
		
		atArray<const char*> m_DownloadTokens;
		atArray<const char*> m_DownloadTokenSkus;
		netStatus m_DownloadTokenStatus;

		char m_RedeemCode[64];
		bool m_SubmitRedeemCode;
		bool m_IsRedeemCodeActivated;
		char m_RedeemCodeSkuName[256];
		netStatus m_RedeemCodeStatus;
		bool m_RegisteringRedeemCode;
		bool m_ActivatingRedeemCode;

		char m_SCAuthToken[256];
		netStatus m_SCAuthTokenStatus;

		int m_gameshieldProjectID;
	};
}


#endif // __WIN32PC

#endif // PCCOMMERCEUTIL

