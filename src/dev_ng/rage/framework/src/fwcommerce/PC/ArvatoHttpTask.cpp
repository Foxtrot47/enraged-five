#if __WIN32PC

#include "net/http.h"
#include "diag/seh.h"
#include "data/base64.h"

#include "parser/manager.h"
#include "ArvatoHttpTask.h"

// HMACSHA256 Algorithm required for Arvato requests
#include "../../3rdParty/cyassl-2.0.0rc2/ctaocrypt/include/ctc_types.h"
#include "../../3rdParty/cyassl-2.0.0rc2/ctaocrypt/include/ctc_hmac.h"

namespace rage
{

#define ARVATO_SHARED_KEY "QRh6OZINJ0P2sfIo1BQ/svGgODXw0fJklgiLIrdE91StV99YVZnjEivKXPHv0U9vXiVFDZXs+doWtPolL+10Cg=="
#define ARVATO_DOMAIN "www.rockstarwarehouse.com"

#define XML_ENTRY_LENGTH 256

#define	PRODUCT_NAME_XML_TAG	"name"
#define PRODUCT_SKU_XML_TAG		"sku"
#define PRODUCT_PRICE_XML_TAG	"price"

///////////////////////////////////////////////////////////////////////////////
//  rlArvatoBaseHttpTask
///////////////////////////////////////////////////////////////////////////////
bool
rlArvatoBaseHttpTask::Configure()
{
	rtry
	{
		rverify(rlHttpTask::Configure(NULL, NULL, NULL, false),
				catchall, 
				rlTaskError("Failed to configure base class"));

		rverify(AddRequestHeaderValue("Content-Type", "text/xml"),
				catchall,
				rlTaskError("Failed to set content-type."));
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char*
rlArvatoBaseHttpTask::GetUrlHostName()
{
	return ARVATO_DOMAIN;
}

bool
rlArvatoBaseHttpTask::GetServicePath(char* svcPath, const unsigned maxLen)
{
	formatf(svcPath,
			maxLen,
			"igs/%s",
			GetServiceMethod());

	return true;
}

bool
rlArvatoBaseHttpTask::ProcessResponse(const char* response, int& resultCode)
{
	bool success = false;
	parTree* tree = 0;

	resultCode = 0;

	INIT_PARSER;

	rtry
	{
		rverify(response, catchall,);

		// treat the response as a memory file so we can use the XML parser
		char filename[64];
		fiDeviceMemory::MakeMemoryFileName(filename, sizeof(filename), response, strlen(response), false, NULL);

		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
		tree = PARSER.LoadTree(filename, "xml");
		PARSER.Settings().SetFlag(parSettings::READ_SAFE_BUT_SLOW, false);

		rcheck(tree && tree->GetRoot(), catchall, rlTaskError("Failed to load tree"));

		if (strcmp(tree->GetRoot()->GetElement().GetName(), "error") == 0)
		{
			ProcessError(tree->GetRoot());
			success = false;
		}
		else
		{
			rcheck(ProcessSuccess(tree->GetRoot(), resultCode),
				catchall,
				rlTaskError("ProcessSuccess failed"));
		}

		success = true;
	}
	rcatchall
	{
		success = false;
	}

	delete tree;
	tree = NULL;

	SHUTDOWN_PARSER;

	return success;
}

bool
rlArvatoBaseHttpTask::ProcessSuccess(const parTreeNode* node, int& resultCode)
{
	return true;
}

void
rlArvatoBaseHttpTask::ProcessError(const parTreeNode* node)
{
}

bool
rlArvatoBaseHttpTask::GenerateRequestHash(char* request, unsigned int requestLength, char* hash, unsigned int hashLength)
{
	rtry
	{
		rverify(request, catchall, rlTaskError("rlArvatoBaseHttpTask: Invalid request to perform HMACSHA256 hash generation"));
		rverify(requestLength > 0, catchall, rlTaskError("rlArvatoBaseHttpTask: Invalid request length to perform HMACSHA256 hash generation"));
		rverify(hash, catchall, rlTaskError("rlArvatoBaseHttpTask: Invalid buffer to hold hash value"));
		rverify(hashLength > 0, catchall, rlTaskError("rlArvatoBaseHttpTask: Invalid buffer length to hold HMACSHA256 hash value"));

		int maxKeyBytes = datBase64::GetMaxDecodedSize(ARVATO_SHARED_KEY);
		u8* keyBytes = (u8*)alloca(maxKeyBytes);
		unsigned keyBytesLen;

		rverify(keyBytes, catchall, rlError("rlArvatoBaseHttpTask: Failed to alloc %d key bytes", maxKeyBytes));

		rverify(datBase64::Decode(ARVATO_SHARED_KEY, maxKeyBytes, keyBytes, &keyBytesLen),
				catchall,
				rlError("rlArvatoBaseHttpTask: Failed to decode shared key value (%s)", ARVATO_SHARED_KEY));

		Hmac HMac;
		u8 SHA256Hash[32];

		HmacSetKey(&HMac, SHA256, (const byte*)keyBytes, keyBytesLen);
		HmacUpdate(&HMac, (byte*)request, (word32)requestLength);
		HmacFinal(&HMac, (byte*)SHA256Hash);

		rverify(datBase64::Encode(SHA256Hash, 32, hash, hashLength, NULL),
				catchall,
				rlError("rlArvatoBaseHttpTask: Failed to encode HMACSHA256 request"));
	}
	rcatchall
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
//  rlArvatoCatalogHttpTask
///////////////////////////////////////////////////////////////////////////////
bool
rlArvatoCatalogHttpTask::Configure(const char* culture,
								   atMap<u32, cArvatoProduct*> *catalogListing)
{
	rtry
	{
		rverify(culture, catchall,);
		rverify(catalogListing, catchall,);

		rverify(rlArvatoBaseHttpTask::Configure(), 
				catchall,
				rlTaskError("rlArvatoCatalogHttpTask: Failed to initialize ArvatoBaseHttpTask"));

		char catalogRequest[2048];
		formatf(catalogRequest, "<catalogRequest culture=\"%s\" name=\"maxPayne3\" />", culture);
		int requestLength = strlen(catalogRequest);

		m_CatalogListing = catalogListing;

		rverify(requestLength > 0,
				catchall,
				"rlArvatoCatalogHttpTask: Invalid request size");

		char hash[128];
		unsigned int maxHashSize = datBase64::GetMaxEncodedSize(32);
		GenerateRequestHash(catalogRequest, requestLength, hash, maxHashSize);

		rverify(AddRequestHeaderValue("X_Arvato_Key", hash),
				catchall,
				rlTaskError("rlArvatoCatalogHttpTask: Failed to set X_Arvato_Key header value"));

		rverify(AppendContent(catalogRequest, requestLength),
				catchall,
				rlTaskError("rlArvatoCatalogHttpTask: Failed to append XML request"));
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char*
rlArvatoCatalogHttpTask::GetServiceMethod() const
{
	return "catalog";
}

bool
rlArvatoCatalogHttpTask::ProcessSuccess(const parTreeNode* pRootNode, int& resultCode)
{
	rtry
	{
		rverify(pRootNode, catchall, 
				rlTaskError("rlArvatoCatalogHttpTask: Could not find root node for Catalogue XML response"));

		for(parTreeNode::ChildNodeIterator nodeIt = pRootNode->BeginChildren();
			*nodeIt != NULL;
			++nodeIt)
		{
			const parTreeNode &productNode = *(*nodeIt);

			char prodName[XML_ENTRY_LENGTH] = { 0 };
			char prodSku[XML_ENTRY_LENGTH] = { 0 };

			cArvatoProduct *newProduct = rage_new cArvatoProduct();

			rverify(newProduct, catchall,);

			const char *productName = productNode.GetElement().FindAttributeStringValue(PRODUCT_NAME_XML_TAG, "", prodName, XML_ENTRY_LENGTH, true);
			const char *productSku = productNode.GetElement().FindAttributeStringValue(PRODUCT_SKU_XML_TAG, "", prodSku, XML_ENTRY_LENGTH, true);
			float productPrice = productNode.GetElement().FindAttributeFloatValue(PRODUCT_PRICE_XML_TAG, 0.0f);

			rverify(productName && productSku, catchall, 
					rlTaskError("rlArvatoCatalogHttpTask: Invalid catalog product XML entry"));

			newProduct->m_ProductName = productName;
			newProduct->m_ProductSku = productSku;
			newProduct->m_Price = productPrice;

			u32 skuHash = atStringHash(newProduct->m_ProductSku.c_str());
			m_CatalogListing->Insert(skuHash, newProduct);
		}
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void
rlArvatoCatalogHttpTask::ProcessError(const parTreeNode* node)
{
}

///////////////////////////////////////////////////////////////////////////////
//  rlArvatoCatalogHttpTask
///////////////////////////////////////////////////////////////////////////////
bool 
rlArvatoCheckoutHttpTask::Configure(const char* culture, 
									const char* user,
									const char* productSku, 
									char* checkoutUrl,
									unsigned int checkoutUrlLen )
{
	rtry
	{
		rverify(culture, catchall,);
		rverify(user, catchall,);
		rverify(productSku, catchall,);
		rverify(checkoutUrl, catchall,);

		m_CheckoutUrl = checkoutUrl;
		m_CheckoutUrlLen = checkoutUrlLen;

		rverify(rlArvatoBaseHttpTask::Configure(), 
				catchall,
				rlTaskError("rlArvatoCheckoutHttpTask:Failed to initialize ArvatoBaseHttpTask"));

		char checkoutRequest[2048];
		formatf(checkoutRequest, "<cartInfo culture=\"%s\"><user>%s</user><productSKU>%s</productSKU></cartInfo>", 
			culture, user, productSku);

		int requestLength = strlen(checkoutRequest);

		rverify(requestLength > 0,
				catchall,
				"rlArvatoCheckoutHttpTask: Invalid request size");

		char hash[128];
		unsigned int maxHashSize = datBase64::GetMaxEncodedSize(32);
		GenerateRequestHash(checkoutRequest, requestLength, hash, maxHashSize);

		rverify(AddRequestHeaderValue("X_Arvato_Key", hash),
				catchall,
				rlTaskError("rlArvatoCheckoutHttpTask:: Failed to set X_Arvato_Key header value"));

		rverify(AppendContent(checkoutRequest, requestLength),
				catchall,
				rlTaskError("rlArvatoCheckoutHttpTask: Failed to append XML request"));
	}
	rcatchall
	{
		return false;
	}

	return true;
}

const char*
rlArvatoCheckoutHttpTask::GetServiceMethod() const
{
	return "checkout";
}

bool
rlArvatoCheckoutHttpTask::ProcessSuccess(const parTreeNode* node, int& resultCode)
{
	rtry
	{
		rverify(node, catchall, 
			rlTaskError("rlArvatoCheckoutHttpTask: Could not find root node for Checkout XML response"));
		rverify(node->GetData(), catchall, 
			rlTaskError("rlArvatoCheckoutHttpTask: Could not find URL for Checkout XML response"));
		
		unsigned numConsumed;

		memset(m_CheckoutUrl, 0, m_CheckoutUrlLen);

		bool decodeResult = netHttpRequest::UrlDecode(m_CheckoutUrl, &m_CheckoutUrlLen, node->GetData(), strlen(node->GetData()) + 1, &numConsumed);

		rverify(decodeResult, catchall,
			rlTaskError("rlArvatoCheckoutHttpTask: Could not decode checkout url"));
	}
	rcatchall
	{
		return false;
	}

	return true;
}

void
rlArvatoCheckoutHttpTask::ProcessError(const parTreeNode* OUTPUT_ONLY(node))
{
#if !__NO_OUTPUT
	parTreeNode* pNode = node->FindChildWithName("message");
	const char* errorMessage = pNode->GetData();

	rlTaskError("rlArvatoCheckoutHttpTask Failed: %s", errorMessage);
#endif
}

} // namespace rage

#endif // __WIN32PC

