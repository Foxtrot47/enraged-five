#if __WIN32PC
#include "PCCommerceUtil.h"

#include "system/xtl.h"

#include "rline/rlpc.h"
#include "rline/rltitleid.h"
#include "rline/entitlement/rlrosentitlement.h"
#include "rline/entitlement/rlrosentitlementtask.h"
#include "rline/socialclub/rlsocialclub.h"

#include "system/memory.h"

#include "rgsc/rgsc/rgsc/public_interface/commerce_interface.h"

#include "../CommerceChannel.h"

namespace rage
{

cPCCommerceManager* cPCCommerceManager::m_Instance = NULL;

cPCCommerceManager::cPCCommerceManager() :
	m_Initialised( false ),
	m_CurrentCommerceUserIndex(0),
	m_CurrentState(MODE_NEUTRAL),
	m_RedeemCodeInterface(NULL),
	m_ArvatoCommerce(NULL),
	m_ContentInfoPopulated(false),
	m_CheckingOut(false)
{
	if ( m_Instance == NULL )
	{
		m_Instance = this;
	}
}

//----------------------------------------------------------
cPCCommerceManager::~cPCCommerceManager()
{
	Shutdown();

	m_Instance = NULL;
}

//----------------------------------------------------------
cPCCommerceManager* cPCCommerceManager::Instance()
{
	if ( m_Instance == NULL )
	{
		m_Instance = rage_new cPCCommerceManager;
	}

	return m_Instance;
}

//----------------------------------------------------------
bool cPCCommerceManager::Init(int userIndex, const char* skuFolder)
{
	if ( m_Initialised )
	{
		commerceErrorf("Trying to initialise the commerce manager when it has already been intialised.");
		commerceAssert( false );
		return false;
	}

	m_Initialised = true;
	m_CurrentCommerceUserIndex = userIndex;
	m_CurrentState = MODE_NEUTRAL;

	m_CheckoutUIShowing = false;
	m_CheckingOut = false;
	m_CheckoutUIWaiting = false;

	m_ContentNeedsActivation = false;

	m_SCAuthToken[0] = '\0';
	m_RedeemCode[0] = '\0';

	m_RegisteringRedeemCode = false;
	m_ActivatingRedeemCode = false;

	m_gameshieldProjectID = -1;
	
	// Locations we are pulling in our product data.
	m_ArvatoCommerce = rage_new cArvatoCommerce();

	m_RedeemCodeInterface = g_rlPc.GetCommerceManager();

	if (!m_RedeemCodeInterface)
	{
		commerceErrorf("Could not get a handle to the redemption code interface");
		commerceAssert( false );
		return false;
	}

	// Hook up our callbacks to the downloader so we can get progress on our downloads
	rlPCPipe* downloadPipe = g_rlPc.GetDownloaderPipe();
	if (downloadPipe)
		downloadPipe->AddCallbacks(ProductDownloadProgress, ProductDownloadComplete, ProductDownloadError);

	cCommerceUtil::Init(userIndex, skuFolder);

	if (!m_ArvatoCommerce)
	{
		commerceErrorf("Could not initialize Arvato commerce interface");
		return false;
	}

	m_ArvatoCommerce->Init(userIndex);

	return true;
}

//----------------------------------------------------------
void cPCCommerceManager::Shutdown()
{
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to shutdown commerce manager without initialising." );
		return;
	}

	m_Initialised = false;
	m_RedeemCodeInterface = NULL;
	m_CurrentCommerceUserIndex = 0;
	m_CurrentState = MODE_NEUTRAL;

	// Shutdown our download progress callbacks
	rlPCPipe* downloadPipe = g_rlPc.GetDownloaderPipe();
	if (downloadPipe)
		downloadPipe->RemoveCallbacks();

	if (m_ArvatoCommerce)
	{
		m_ArvatoCommerce->Shutdown();
		delete m_ArvatoCommerce;
	}

	cCommerceUtil::Shutdown();
}

//----------------------------------------------------------
void cPCCommerceManager::Update()
{
    cCommerceUtil::Update();

	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call Update without initialising commerce manager." );
		Assert( false );
		return;
	}

	if (m_ArvatoCommerce)
	{
		m_ArvatoCommerce->Update();

		switch (m_CurrentState)
		{
		case MODE_CHECKOUT:
			UpdateCheckout();
			break;
		case MODE_ENUMERATE_CONTENT:
			UpdateEnumeration();
			break;
		case MODE_REDEEMING:
			UpdateRedemption();
			break;
		case MODE_ERROR_ENCOUNTERED:
		case MODE_NEUTRAL:
		default:
			break;
		}
	}

	
}

//----------------------------------------------------------
bool cPCCommerceManager::DoPlatformDataRequest()
{
	Assert(m_CurrentState == MODE_NEUTRAL);

	m_ContentInfoPopulated = false;

	/*
	if (!m_ArvatoCommerce->RetrieveCatalogue())
	{
		commerceErrorf( "Could not retrieve catalog from Arvato" );
		Assert( false );
		return false;
	}
	*/

	rlRosEntitlement::RefreshEntitlementData(m_CurrentCommerceUserIndex);

	m_CurrentState = MODE_ENUMERATE_CONTENT;

	return true;
}

//----------------------------------------------------------

void cPCCommerceManager::UpdateCommerceDataWithEnumeratedContent( )
{
    for ( int i = 0; i < m_CommerceData.GetSizeOfDataArray(); i++ )
    {
        if ( m_CommerceData.GetItemData( i )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
        {
            //Not a product, ergo does not need to be checked.
            continue;
        }

        cCommerceProductData* productData = static_cast<cCommerceProductData*>(m_CommerceData.GetItemData( i ));

        if (productData)
        {
            cArvatoProduct* product = m_ArvatoCommerce->GetProduct(productData->GetPlatformId().c_str());

            // We have an Arvato product from their catalogue.
            if (product)
            {
                productData->SetEnumeratedFlag(true);
            }
        }
    }
}

//----------------------------------------------------------
void cPCCommerceManager::MergePlatformAndRosData()
{
	rlRosEntitlementData* entitlementData = rlRosEntitlement::GetCurrentEntitlementData();

    UpdateCommerceDataWithEnumeratedContent();

	if (!entitlementData)
	{
		return;
	}

	for ( int s=0; s <  m_CommerceData.GetSizeOfDataArray(); s++ )
	{
		cCommerceItemData* itemData = m_CommerceData.GetItemData( s );

        commerceAssert( itemData );

		//By default the dependencys are set to having been found
		itemData->SetDependencyFlag( true );

		for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetDependencyFlag( false );

			atString dependency = itemData->GetDependencyIds()[ iDeps ];
			/*
            cArvatoProduct* product = m_ArvatoCommerce->GetProduct(dependency.c_str());

			if (!product)
				continue;
            */

			rlRosEntitlementSku* sku = entitlementData->GetSkuByName(dependency.c_str());

			if (!sku)
				continue;

			if (/*product &&*/ sku)
			{
				itemData->SetDependencyFlag( true );
			}
		}

		//By default enumeration dependencies are set to true
		itemData->SetEnumDependencyFlag( true );

		for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetEnumDependencyFlag( false );

			atString enumDependencySku = itemData->GetEnumDependencyIds()[ iEnumDeps ];
			cArvatoProduct* product = m_ArvatoCommerce->GetProduct(enumDependencySku.c_str());

			if (product)
			{
				itemData->SetEnumDependencyFlag( true );
			}
		}

		//By default the exclusions flag is set to false (no exclusions found)
		itemData->SetExclusionFlag( false );

		for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetExclusionFlag( false );

			atString exclusionSku = itemData->GetExclusionIds()[ iExcs ];

            /*
            cArvatoProduct* product = m_ArvatoCommerce->GetProduct(exclusionSku.c_str());

			if (!product)
				continue;
            */

			rlRosEntitlementSku* sku = entitlementData->GetSkuByName(exclusionSku.c_str());

			if (!sku)
				continue;

			if (/*product &&*/ sku)
			{
				itemData->SetExclusionFlag( true );
			}
		}

		//By default the enum exclusions flag is set to false (no exclusions found)
		itemData->SetEnumExclusionFlag( false );

		for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
		{
			//There is a dependency, set the flag to false
			itemData->SetEnumExclusionFlag( false );

			atString enumExclusionSku = itemData->GetEnumExclusionIds()[ iEnumExcs ];
			cArvatoProduct* product = m_ArvatoCommerce->GetProduct(enumExclusionSku.c_str());

			if (!product)
				continue;

			rlRosEntitlementSku* sku = entitlementData->GetSkuByName(enumExclusionSku.c_str());

			if (!sku)
				continue;

			if (product && sku)
			{
				itemData->SetEnumExclusionFlag( true );
			}
		}
	}
}

//----------------------------------------------------------
bool cPCCommerceManager::ProductCodeEntry()
{
	// TODO - Temporary UI to show a product code entry box.
	if (g_rlPc.IsUiShowing() == false && m_RedeemCodeInterface)
	{
		m_CurrentState = MODE_REDEEM_UI;
		m_RedeemCodeInterface->ShowRedemptionCodeUi(OnBladeUIRedeemCodeSubmit);
		return true;
	}

	return false;
}

//----------------------------------------------------------
void cPCCommerceManager::OnBladeUIRedeemCodeSubmit( const char* redemptionCode, const rgsc::ICommerceManagerLatestVersion::RedemptionAction action )
{
	// TODO - Temporary UI to show a product code entry box.
	if (redemptionCode != NULL && action == rgsc::ICommerceManagerLatestVersion::REDEMPTION_ENTERED_CODE)
	{
		commerceDebugf3("Redeem Code: %s", redemptionCode);
		cPCCommerceManager::Instance()->SetRedeemCode(redemptionCode);
		cPCCommerceManager::Instance()->m_CurrentState = MODE_REDEEMING;
	}
	else if (action == rgsc::ICommerceManagerLatestVersion::REDEMPTION_USER_CANCELLED && cPCCommerceManager::Instance()->m_CurrentState != MODE_REDEEMING)
	{
		cPCCommerceManager::Instance()->m_CurrentState = MODE_NEUTRAL;
	}
}

//----------------------------------------------------------
void cPCCommerceManager::SetRedeemCode(const char* redeemCode)
{
	if (!redeemCode)
	{
		commerceErrorf("Invalid redeem code");
		m_SubmitRedeemCode = false;
		return;
	}

	safecpy(m_RedeemCode, redeemCode, sizeof(m_RedeemCode));
	m_SubmitRedeemCode = true;
}

//----------------------------------------------------------
void cPCCommerceManager::SubmitRedeemCode()
{
	int redeemCodeLength = strlen(m_RedeemCode);

	if ( redeemCodeLength == 0 || redeemCodeLength > 19)
	{
		commerceErrorf("Malformed redeem code");

		if (m_RedeemCodeInterface)
			m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_INVALID_CODE, OnBladeUIRedeemCodeSubmitEmpty);

		m_CurrentState = MODE_ERROR_ENCOUNTERED;
		return;
	}

	if (!rlRosEntitlement::RegisterPurchaseBySN(m_CurrentCommerceUserIndex, m_gameshieldProjectID, m_RedeemCode, &m_IsRedeemCodeActivated, m_RedeemCodeSkuName, sizeof(m_RedeemCodeSkuName), &m_RedeemCodeStatus))
	{
		commerceErrorf("Could not register serial number on ROS services");
		m_ErrorCode = CERROR_REDEMPTION;
		m_CurrentState = MODE_ERROR_ENCOUNTERED;
		return;
	}

	m_RegisteringRedeemCode = true;
}

//----------------------------------------------------------
void cPCCommerceManager::SetActivationComplete(bool success, long UNUSED_PARAM(resultCode))
{
	if (success)
	{
		if (!rlRosEntitlement::RegisterActivationBySN(m_CurrentCommerceUserIndex, m_gameshieldProjectID, m_RedeemCode, "", &m_RedeemCodeStatus))
		{
			commerceErrorf("Could not activate serial number on ROS services");
			m_ErrorCode = CERROR_REDEMPTION;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			return;
		}

		m_ActivatingRedeemCode = true;
	}
}

//----------------------------------------------------------
void cPCCommerceManager::SetGameshieldProjectID(int lpjID)
{
	m_gameshieldProjectID = lpjID;
}

//----------------------------------------------------------
bool cPCCommerceManager::IsProductPurchased( int a_Index ) const
{
    Assert( IsContentInfoPopulated() );
    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );

    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }
            
    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    if ( !m_Initialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    return IsProductPurchased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

//----------------------------------------------------------
bool cPCCommerceManager::IsProductPurchased( const cCommerceProductData* a_pProductData ) const
{
	rlRosEntitlementData* entitlement = rlRosEntitlement::GetCurrentEntitlementData();

	if (!entitlement)
	{
		return false;
	}

	rlRosEntitlementSku* sku = entitlement->GetSkuByName(a_pProductData->GetPlatformId());

	if (!sku)
	{
		return false;
	}

    return true;
}

//----------------------------------------------------------
bool cPCCommerceManager::IsProductPurchasable( int a_Index ) const
{
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
		Assert( false );
		return false;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return false;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return false;
	}

	return IsProductPurchasable( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

//----------------------------------------------------------
bool cPCCommerceManager::IsProductPurchasable( const cCommerceProductData* a_pProductData) const
{
	return a_pProductData->GetEnumeratedFlag();
}

//----------------------------------------------------------
float cPCCommerceManager::GetProductPrice( u32 a_Index, char* a_pOutputString, u32 a_SizeOfOutputString ) const
{ 
	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
		Assert( false );
		return -1.0f;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return -1.0f;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return -1.0f;
	}

	return GetProductPrice( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ), a_pOutputString, a_SizeOfOutputString );
}

//----------------------------------------------------------
float cPCCommerceManager::GetProductPrice( const cCommerceProductData* a_pProductData, char* a_pOutputString, unsigned a_SizeOfOutputString ) const
{
	if ( !a_pProductData->GetEnumeratedFlag() )
	{
		return -1;
	}

	float price = -1.0f;
	cArvatoProduct* product = m_ArvatoCommerce->GetProduct(a_pProductData->GetPlatformId().c_str());

	if (product)
	{
		price = product->m_Price;
	}

	if (a_pOutputString && a_SizeOfOutputString > 0)
	{
		formatf( a_pOutputString, a_SizeOfOutputString, "$%.2f",  price );
	}

	return price;
}

//----------------------------------------------------------
void cPCCommerceManager::CheckoutProduct( u32 a_Index )
{
	Assert( m_CurrentState == MODE_NEUTRAL );

	if ( !m_Initialised )
	{
		commerceErrorf( "Trying to call CheckoutProduct without initialising commerce manager." );
		Assert( false );
		return ;
	}

	Assert( IsContentInfoPopulated() );

	Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
	if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
	{    
		return;
	}

	if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
	{
		return;
	}

	return CheckoutProduct( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

//----------------------------------------------------------
void cPCCommerceManager::CheckoutProduct( const cCommerceProductData* a_pProductdata )
{
	if ( !a_pProductdata || !a_pProductdata->GetEnumeratedFlag() )
	{
		return;
	}

	m_SCAuthToken[0] = '\0';

	// Grab the SC Auth Token before we proceed to the checkout
	bool success = rlSocialClub::GetScAuthToken(m_CurrentCommerceUserIndex,
												m_SCAuthToken,
												sizeof(m_SCAuthToken),
												&m_SCAuthTokenStatus);

	m_CheckingOut = false;

	if (success)
	{
		m_CurrentState = MODE_CHECKOUT;
		m_CurrentCheckoutProduct = a_pProductdata;
	}
	else
	{
		m_ErrorCode = CERROR_DURING_CHECKOUT2;
		m_CurrentState = MODE_ERROR_ENCOUNTERED;
		m_CurrentCheckoutProduct = NULL;

		commerceErrorf("Could not get SC Auth Token for current SC user");
		return;
	}
}

//----------------------------------------------------------
void cPCCommerceManager::UpdateCheckout()
{
	// Check to see if we're still waiting for our auth token call to return
	if (!m_CheckingOut)
	{
		if (m_SCAuthTokenStatus.Succeeded())
		{
			if (m_ArvatoCommerce && m_CurrentCheckoutProduct)
			{
				if (!m_ArvatoCommerce->Checkout(m_CurrentCheckoutProduct->GetPlatformId(), m_SCAuthToken))
				{
					m_CurrentState = MODE_ERROR_ENCOUNTERED;
				}
				else
				{
					m_CurrentState = MODE_CHECKOUT;
					m_CheckingOut = true;
					m_CheckoutUIShowing = false;
					m_CheckoutUIWaiting = false;
				}
			}
			else
			{
				m_CurrentState = MODE_ERROR_ENCOUNTERED;
				commerceErrorf( "Trying to checkout product without a valid vendor" );
				Assert( false );
				return;
			}
		}
		else if (m_SCAuthTokenStatus.Failed())
		{
			m_ErrorCode = CERROR_DURING_CHECKOUT3;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
		}
	}
	else
	{
		if (!m_ArvatoCommerce->IsRequstInProgress() && !m_CheckoutUIShowing)
		{
			if (m_ArvatoCommerce->GetState() == AS_ERROR)
			{
				m_ErrorCode = CERROR_CONTACT_ARVADO;
				m_CurrentState = MODE_ERROR_ENCOUNTERED;
				return;
			}

			commerceDebugf3("Showing checkout page with URL: %s", m_ArvatoCommerce->GetCurrentCheckoutURL());

			g_rlPc.ShowCommerceUi(m_ArvatoCommerce->GetCurrentCheckoutURL());
			//g_rlPc.ShowCommerceUi("http://int-ingame.rockstarwarehouse.com/Checkout.aspx%3fmid%3d61%26c%3den-US");
			
			m_CheckoutUIShowing = true;

			return;
		}
		else if (m_ArvatoCommerce->IsRequstInProgress() && !m_CheckoutUIWaiting)
		{
			commerceDebugf3("Showing checkout page with blank URL for spinner.");
			g_rlPc.ShowCommerceUi("");
			m_CheckoutUIWaiting = true;
		}

		if (m_CheckoutUIShowing && !g_rlPc.IsUiShowing())
		{
			m_CheckingOut = false;
			m_CheckoutUIShowing = false;
			m_CheckoutUIWaiting = false;
			m_CurrentState = MODE_NEUTRAL;
		}
	}
}

//----------------------------------------------------------
void cPCCommerceManager::UpdateEnumeration()
{
	//if (!m_ArvatoCommerce->IsRequstInProgress() && !rlRosEntitlement::IsCheckingForEntitlement())
	if (!rlRosEntitlement::IsCheckingForEntitlement())
	{
		/*
		if (m_ArvatoCommerce->GetState() == AS_ERROR)
		{
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_ErrorCode = CERROR_VERIFY_CONTENT;
		}
		*/

		if (rlRosEntitlement::IsEntitlementUpToDate())
		{
			m_CurrentState = MODE_NEUTRAL;
			m_ContentInfoPopulated = true;
		}
		else
		{
			m_ErrorCode = CERROR_ENTITLEMENT;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_ContentInfoPopulated = false;
		}
	}
}

//----------------------------------------------------------
void cPCCommerceManager::UpdateRedemption()
{
	if (m_SubmitRedeemCode)
	{
		m_SubmitRedeemCode = false;
		SubmitRedeemCode();
	}

	if (m_RegisteringRedeemCode)
	{
		if (m_RedeemCodeStatus.Succeeded())
		{
			m_RegisteringRedeemCode = false;
			SetContentNeedsActivation(true);
		}
		else if (m_RedeemCodeStatus.Failed())
		{
			if (m_RedeemCodeStatus.GetResultCode() == RLEN_ERROR_SERIAL_ALREADY_ACTIVATED)
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_CODE_IN_USE, OnBladeUIRedeemCodeSubmitEmpty);
			}
			else if (m_RedeemCodeStatus.GetResultCode() == RLEN_ERROR_SERIAL_DOES_NOT_EXIST)
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_INVALID_CODE, OnBladeUIRedeemCodeSubmitEmpty);
			}
			else
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_GENERIC_ERROR, OnBladeUIRedeemCodeSubmitEmpty);
			}

			// Already redeemed code or could not connect to server.
			m_ErrorCode = CERROR_REDEMPTION;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_RegisteringRedeemCode = false;
		}
	}

	if (m_ActivatingRedeemCode)
	{
		if (m_RedeemCodeStatus.Succeeded())
		{
			if (m_RedeemCodeInterface)
				m_RedeemCodeInterface->RedemptionCodeResult(1, REDEEM_ITEM_UNLOCKED, OnBladeUIRedeemCodeSubmitEmpty);

			// Re-scan entitlement
			m_CurrentState = MODE_NEUTRAL;
			m_ActivatingRedeemCode = false;
		}
		else if (m_RedeemCodeStatus.Failed())
		{
			if (m_RedeemCodeStatus.GetResultCode() == RLEN_ERROR_SERIAL_ALREADY_ACTIVATED)
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_CODE_IN_USE, OnBladeUIRedeemCodeSubmitEmpty);
			}
			else
			{
				if (m_RedeemCodeInterface)
					m_RedeemCodeInterface->RedemptionCodeResult(0, REDEEM_GENERIC_ERROR, OnBladeUIRedeemCodeSubmitEmpty);
			}

			// Already redeemed code or could not connect to server.
			m_ErrorCode = CERROR_REDEMPTION;
			m_CurrentState = MODE_ERROR_ENCOUNTERED;
			m_ActivatingRedeemCode = false;
		}		
	}
}

//----------------------------------------------------------
void cPCCommerceManager::OnBladeUIRedeemCodeSubmitEmpty( const char* UNUSED_PARAM(redemptionCode), const rgsc::ICommerceManagerLatestVersion::RedemptionAction UNUSED_PARAM(action) )
{
	// Empty callback for redeem code result
}

//----------------------------------------------------------
bool cPCCommerceManager::IsContentInfoPopulated() const
{
	return ( m_ContentInfoPopulated && cCommerceUtil::IsContentInfoPopulated() );
}

//----------------------------------------------------------
bool cPCCommerceManager::IsPlatformReady() const
{
	return (m_CurrentState == MODE_NEUTRAL);
}

//----------------------------------------------------------
rlRosEntitlementSku* cPCCommerceManager::GetRosSku(const char* skuName)
{
	rlRosEntitlementData* entitlement = rlRosEntitlement::GetCurrentEntitlementData();

	if (!entitlement)
	{
		return NULL;
	}

	return entitlement->GetSkuByName(skuName);
}

//----------------------------------------------------------
void cPCCommerceManager::DownloadProduct(const cCommerceProductData* a_pProductData)
{
	Assert(m_CurrentState == MODE_NEUTRAL);

	rlPCPipe* downloadPipe = g_rlPc.GetDownloaderPipe();
	rlRosEntitlementSku* sku = GetRosSku(a_pProductData->GetPlatformId().c_str());

	if (downloadPipe && sku)
	{
		char downloadString[4096];
		formatf(downloadString, 4096, "DOWN:%s:%s", sku->m_SkuName, sku->m_DownloadToken);

		downloadPipe->Write((const char*)downloadString);
	}
	else
	{
		commerceErrorf( "Could not get valid downloader pipe to connect to." );
		Assert( false );
		return;
	}
}

//----------------------------------------------------------
void cPCCommerceManager::ProductDownloadProgress(int OUTPUT_ONLY(progress), const char* OUTPUT_ONLY(skuName))
{
	commerceDebugf3("Arvato - %s -> Download Progress: %d", skuName, progress);
}

//----------------------------------------------------------
void cPCCommerceManager::ProductDownloadComplete(const char* skuName)
{
	commerceDebugf3("Arvato - Download Complete: %s", skuName);

	rgsc::ICommerceManagerLatestVersion* commerceInterface = g_rlPc.GetCommerceManager();

	if (commerceInterface)
	{
		commerceInterface->DownloadComplete(skuName, true);
	}
}

//----------------------------------------------------------
void cPCCommerceManager::ProductDownloadError(const char* OUTPUT_ONLY(errorCode), const char* skuName)
{
	commerceDebugf3("Arvato - %s -> Download Error: %s", skuName, errorCode);

	rgsc::ICommerceManagerLatestVersion* commerceInterface = g_rlPc.GetCommerceManager();

	if (commerceInterface)
	{
		commerceInterface->DownloadComplete(skuName, false);
	}
}

}

#endif // __WIN32PC

