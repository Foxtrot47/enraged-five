
#include "CommerceData.h"

#include <stdlib.h>

//rage headers
#include "file/asset.h"
#include "net/task.h"
#include "rline/cloud/rlcloud.h"
#include "rline/ros/rlros.h"

#include "CommerceChannel.h"

PARAM(commerceLocalCatalogueFile, "Local catalogue file");
PARAM(commerceCloudFile, "If present, specifies cloud file");

//OPTIMISATIONS_OFF()

//Fix for the missing __STEAM define
#ifndef __STEAM_BUILD
#define __STEAM_BUILD 0
#endif

namespace rage
{

const int COMMERCE_ITEM_DATA_ARRAY_SIZE = 100;
const int INITIAL_SIZE_OF_ID_SUPPRESSION_ARRAY = 10;
const int INITIAL_SIZE_OF_TAG_SUPPRESSION_ARRAY = 10;
const int INITIAL_SIZE_OF_SUBSCRIPTION_ID_ARRAY = 1;

const int CREDENTIAL_FAIL_COUNT_FETCH_ABORT_LEVEL = 300;

cCommerceData::cCommerceData() :
	m_ParserState(COMMERCE_XML_WAITING),
	m_bInitialised(false),
	m_LocalGamerIndex(-1),
	m_NeedToRefreshXMLFromCloud(false),
	m_HasValidData(false),
    m_IsInErrorState(false),
	m_pLocalFileStream(nullptr),
    m_CredentialFailCount(0)
{
	m_CommerceXMLFetchStatus.Reset();
}

cCommerceData::~cCommerceData()
{
	Shutdown();
}

void cCommerceData::Init( int a_LocalGamerIndex, const char* skuFolder )
{
	if ( m_bInitialised )
	{
		commerceAssert(false);
		return;
	}

	commerceDebugf1("CommerceData::Init");
	
	m_bInitialised = true;

	commerceAssert( RL_IS_VALID_LOCAL_GAMER_INDEX(a_LocalGamerIndex) );
	m_LocalGamerIndex = a_LocalGamerIndex;

    m_HasValidData = false;
    m_IsInErrorState = false;

    m_SkuDirString.Reset();
    if ( skuFolder != nullptr )
    {
        m_SkuDirString = skuFolder;
    }
    
	m_IdsToSuppress.Reset();
	m_IdsToSuppress.Reserve(INITIAL_SIZE_OF_ID_SUPPRESSION_ARRAY);

	m_TagsToSuppress.Reset();
	m_TagsToSuppress.Reserve(INITIAL_SIZE_OF_TAG_SUPPRESSION_ARRAY);

	m_SubscriptionIds.Reset();
	m_SubscriptionIds.Reserve(INITIAL_SIZE_OF_TAG_SUPPRESSION_ARRAY);

    m_CredentialFailCount = 0;

    ResetData();
}

void cCommerceData::Shutdown()
{
	if ( !m_bInitialised )
	{
		return;
	}

	commerceDebugf1("CommerceData::Shutdown");

    if ( m_CommerceXMLFetchStatus.Pending() )
    {
        rlCloud::Cancel(&m_CommerceXMLFetchStatus);
        //netTask::Cancel(&m_CommerceXMLFetchStatus);
    }

	m_bInitialised = false;
	
	ResetData();
	m_HasValidData = false;
    m_IsInErrorState = false;
	
	m_ParserState = COMMERCE_XML_WAITING;
	m_LocalGamerIndex = -1;
	m_NeedToRefreshXMLFromCloud = false;
}

void cCommerceData::ResetData()
{
	commerceDebugf1("CommerceData::ResetData");
	
	//Delete the actual data items
    for ( int i = 0; i < m_pCommerceItemDataArray.GetCount(); i++ )
    {
        delete m_pCommerceItemDataArray[ i ];
    }

    //Clear up the array
    m_pCommerceItemDataArray.Reset();
}

void cCommerceData::Update()
{
	switch( m_ParserState )
	{
	case(COMMERCE_XML_WAITING):
		UpdateParserWaitingState();
		break;
	case(COMMERCE_XML_STARTUP):
		UpdateParserStartupState();
		break;
	case(COMMERCE_XML_RECEIVING):
		UpdateParserReceivingState();
		break;
	case(COMMERCE_XML_CLEANUP):
		UpdateParserCleanupState();
		break;
	default:
		commerceAssert(false);
	}
}

void cCommerceData::UpdateParserWaitingState()
{
	if(!RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex))
	{
		return;
	}

	if (!m_NeedToRefreshXMLFromCloud)
	{
		return;
	}

    //If we're loading a local file, we don't need credentials
    BANK_ONLY(bool bNeedCredentials = !PARAM_commerceLocalCatalogueFile.Get());

	const rlRosCredentials& creds = rlRos::GetCredentials( m_LocalGamerIndex );
	if(!creds.IsValid() BANK_ONLY(&& bNeedCredentials) )
	{
        m_CredentialFailCount++;

        if ( m_CredentialFailCount > CREDENTIAL_FAIL_COUNT_FETCH_ABORT_LEVEL && !m_IsInErrorState )
        {
            commerceErrorf("UpdateParserWaitingState :: Could not get ROS credentials for cCommerceData fetch, aborting and setting errorstate");
            m_ParserState = COMMERCE_XML_WAITING;
            m_IsInErrorState = true;
        }
        
		return;
	}
	else if (creds.IsValid() && !rlRos::HasPrivilege(m_LocalGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ))
	{
		// no privileges to read from the cloud
		return;
	}

	if (m_NeedToRefreshXMLFromCloud)
	{
		m_NeedToRefreshXMLFromCloud = false;
        m_IsInErrorState = false;
        m_HasValidData = false;
		m_ParserState = COMMERCE_XML_STARTUP;
		ResetData();

        //Temp setup for testing.
        //TODO: Update for actual usage

        m_pCommerceItemDataArray.Reserve(COMMERCE_ITEM_DATA_ARRAY_SIZE);
	}
}

void cCommerceData::UpdateParserStartupState()
{
	if(!RL_IS_VALID_LOCAL_GAMER_INDEX(m_LocalGamerIndex))
	{
		//No valid gamer index, flee.
		commerceErrorf("UpdateParserStartupState :: No local gamer index for download of Commerce data xml");
		m_ParserState = COMMERCE_XML_CLEANUP;
		return;
	}

	m_GrowBuffer.Init(nullptr, datGrowBuffer::NULL_TERMINATE);
	m_GrowBuffer.Preallocate(1024);

    atString cloudFilePath("store");

    if ( m_SkuDirString.GetLength() != 0 )
    {
        cloudFilePath += "/";
        cloudFilePath += m_SkuDirString;
    }

	const char* DEFAULT_CLOUD_FILE = "commerceData.xml";
	const char* commerceDataFilename = nullptr;
#if !__FINAL
	if (!PARAM_commerceCloudFile.Get(commerceDataFilename))
#endif
	{
		commerceDataFilename = DEFAULT_CLOUD_FILE;
	}

    cloudFilePath += "/";
	cloudFilePath += commerceDataFilename;

	const char* fileName = nullptr;

	const rlRosCredentials& creds = rlRos::GetCredentials( m_LocalGamerIndex );
	BANK_ONLY(bool bNeedCredentials = !PARAM_commerceLocalCatalogueFile.Get());

	if(PARAM_commerceLocalCatalogueFile.Get(fileName) && fileName && strlen(fileName) > 0)
	{
		commerceDebugf1("Loading catalogue file from local source: %s", fileName);
		m_pLocalFileStream = ASSET.Open(fileName, ""); //fiStream::Open(fileName,fiDeviceLocal::GetInstance());
		if (commerceVerifyf(m_pLocalFileStream, "Failed to open local catalogue file %s", fileName))
		{
			m_CommerceXMLFetchStatus.Reset();
			m_CommerceXMLFetchStatus.SetPending();
			m_saxReader.Start(this);
			m_ParserState = COMMERCE_XML_RECEIVING;
		}
		else
		{
			m_CommerceXMLFetchStatus.Reset();
			m_CommerceXMLFetchStatus.SetFailed();
			m_ParserState = COMMERCE_XML_CLEANUP;
		}

	}
	else if(!creds.IsValid() BANK_ONLY(&& bNeedCredentials) )
	{
		commerceErrorf("UpdateParserStartupState :: We lost our valid credentials along the way, probably a sign out.");
		m_GrowBuffer.Clear();
		m_ParserState = COMMERCE_XML_WAITING;
		m_IsInErrorState = true;
	}
	else if ( rlCloud::GetTitleFile( cloudFilePath,
                                    RLROS_SECURITY_DEFAULT,
                                    0,  //ifModifiedSincePosixTime
                                    NET_HTTP_OPTIONS_NONE,
                                    m_GrowBuffer.GetFiDevice(),
                                    m_GrowBuffer.GetFiHandle(),
                                    nullptr,   //rlCloudFileInfo
                                    nullptr,   //allocator
                                    &m_CommerceXMLFetchStatus ) )
	{
		m_saxReader.Start(this);
		m_ParserState = COMMERCE_XML_RECEIVING;

		//TODO: Initialise a timeout reference value.
	}
	else
	{
		commerceErrorf("UpdateParserStartupState :: Failed to start download of commerce data file.");
        m_GrowBuffer.Clear();
        m_ParserState = COMMERCE_XML_WAITING;
        m_IsInErrorState = true;
	}
}

void cCommerceData::UpdateParserLocalFileState()
{
	if(PARAM_commerceLocalCatalogueFile.Get() && m_CommerceXMLFetchStatus.Pending())
	{
		if (m_pLocalFileStream)
		{
			//Read it in as chunks, just like the cloud file would.
			static const int READ_LEN = 512;
			char bounceBuffer[READ_LEN];
			int numRead = m_pLocalFileStream->Read(bounceBuffer, READ_LEN);
			if (numRead > 0)
			{
				commerceDebugf3("%s", bounceBuffer);
				m_GrowBuffer.Append(bounceBuffer, numRead);
			}
			
			//If we didn't read anything, or we didn't read as much as we aske for, it means we're done.
			if (numRead == 0 || numRead < READ_LEN || m_CommerceXMLFetchStatus.Canceled())
			{
				commerceDebugf1("Finished parsing local file %s", m_pLocalFileStream->GetName());
				m_pLocalFileStream->Close();
				m_pLocalFileStream = nullptr;

				if (m_CommerceXMLFetchStatus.Pending())
				{
					m_CommerceXMLFetchStatus.SetSucceeded();
				}
			}
		}
	}	
}

void cCommerceData::UpdateParserReceivingState()
{
	UpdateParserLocalFileState();

    if ( m_saxReader.Failed() )
    {
        //Something has gone wrong in the sax reader, probably an interruption or error code
        m_IsInErrorState = true;
        m_ParserState = COMMERCE_XML_CLEANUP;
    }
    else 
    if ( m_CommerceXMLFetchStatus.Succeeded() /*|| m_CommerceXMLFetchStatus.Pending()*/ )
	{
		if ( m_GrowBuffer.Length() != 0 )
		{
			const char* response = (const char*) m_GrowBuffer.GetBuffer();
			const unsigned responseLen = m_GrowBuffer.Length();
                  
			if(response && responseLen)
			{
#if !__NO_OUTPUT
                commerceDisplayf("Processing data of length %d. Full content:", responseLen);
				diagLoggedPrintLn(response, responseLen);
#endif
				const unsigned numConsumed = m_saxReader.Parse(response, 0, responseLen);
				m_GrowBuffer.Remove(0, numConsumed);
			}
		}
	}
    else if ( m_CommerceXMLFetchStatus.Failed() )
    {
        commerceErrorf("Commerce XML file fetch from cloud failed. Code: %d", m_CommerceXMLFetchStatus.GetResultCode());

        //Get cunning: if we have specified a SKU specific file, try the base version.
        if ( m_SkuDirString.GetLength() != 0 )
        {
            m_SkuDirString.Reset();
            m_NeedToRefreshXMLFromCloud = true;
        }
        else
        {
            m_IsInErrorState = true;
        }
        
        m_ParserState = COMMERCE_XML_CLEANUP;
    }
    else if ( m_CommerceXMLFetchStatus.Canceled() )
    {
        m_IsInErrorState = true;
    }

	if ( m_CommerceXMLFetchStatus.Succeeded() && m_GrowBuffer.Length() == 0 )
	{
		commerceDebugf3("Commerce Data retrieved and parsed.");
		m_HasValidData = true;
		m_ParserState = COMMERCE_XML_CLEANUP;
	}
}

void cCommerceData::UpdateParserCleanupState()
{
    if ( !m_saxReader.Failed() )
    {
        m_saxReader.End();
    }
	
	m_GrowBuffer.Clear();
	m_ParserState = COMMERCE_XML_WAITING;
}

bool cCommerceData::StartDataFetch()
{
	if ( m_ParserState == COMMERCE_XML_WAITING )
	{
		m_NeedToRefreshXMLFromCloud = true;
        m_CredentialFailCount = 0;
		return true;
	}
	else
	{
		commerceDisplayf("Tried to call StartDataFetch when m_ParserState was not COMMERCE_XML_WAITING");
		return false;
	}
}

cCommerceItemData * cCommerceData::GetItemData( int a_Index )
{
    if ( !HasValidData() )
    {
        commerceErrorf("Trying to retrieve data without valid parse.");
        return nullptr;
    }

    if ( a_Index < 0 || a_Index >= m_pCommerceItemDataArray.GetCount() )
    {
        commerceErrorf( "Invalid index to cCommerceData::GetItemData" );
        commerceAssert(false);
        return nullptr;
    }

    return m_pCommerceItemDataArray[ a_Index ];
}

const cCommerceItemData * cCommerceData::GetItemData( int a_Index ) const
{
    if ( !HasValidData() )
    {
        commerceErrorf("Trying to retrieve data without valid parse.");
        return nullptr;
    }

    if ( a_Index < 0 || a_Index >= m_pCommerceItemDataArray.GetCount() )
    {
        commerceErrorf( "Invalid index to cCommerceData::GetItemData" );
        commerceAssert(false);
        return nullptr;
    }

    return m_pCommerceItemDataArray[ a_Index ];
}

const cCommerceItemData* cCommerceData::GetItemData( const char* itemName, cCommerceItemData::eItemType itemType /*= cCommerceItemData::ITEM_TYPE_NUM_TYPES */ ) const
{
    if ( !HasValidData() )
    {
        commerceErrorf("Trying to retrieve data without valid parse.");
        return nullptr;
    }

    for(int i = 0; i < m_pCommerceItemDataArray.GetCount(); i++)
    {
        //If we're filtering for only a certain type, handle the filter here.  Makes it quicker if we know what we're looking for
        const cCommerceItemData* iterItem = m_pCommerceItemDataArray[i];
        if (itemType == cCommerceItemData::ITEM_TYPE_NUM_TYPES || iterItem->GetItemType() == itemType)
        {
            if(iterItem->GetId() == itemName)
            {
                return iterItem;
            }
        }
    }

    return nullptr;
}

const cCommerceItemData* cCommerceData::GetItemDataByItemId(const char* itemId) const
{
	if (!HasValidData())
	{
		commerceErrorf("Trying to retrieve data without valid parse.");
		return nullptr;
	}

	for (int i = 0; i < m_pCommerceItemDataArray.GetCount(); i++)
	{
		//If we're filtering for only a certain type, handle the filter here.  Makes it quicker if we know what we're looking for
		const cCommerceItemData* iterItem = m_pCommerceItemDataArray[i];
		if (iterItem->GetId() == itemId)
		{
			return iterItem;
		}
	}

	return nullptr;
}

const int cCommerceData::GetSizeOfDataArray() const
{
    return m_pCommerceItemDataArray.GetCount();
}

void cCommerceData::AbortDataFetch()
{
    if ( m_CommerceXMLFetchStatus.GetStatus() == netStatus::NET_STATUS_PENDING )
    {
        rlCloud::Cancel( &m_CommerceXMLFetchStatus );
        m_CommerceXMLFetchStatus.SetCanceled(); 
    }
}

const bool cCommerceData::IsInDataFetchState() const
{
    return ( m_CommerceXMLFetchStatus.GetStatus() == netStatus::NET_STATUS_PENDING );
}

bool cCommerceData::IsItemInSuppressList( const cCommerceItemData* itemToCheck ) const
{
	if (itemToCheck == nullptr)
	{
		return false;
	}

	for ( int iSuppressedIds = 0; iSuppressedIds < m_IdsToSuppress.GetCount(); iSuppressedIds++ )
	{
		if ( itemToCheck->GetId() == m_IdsToSuppress[iSuppressedIds] )
		{
			commerceDebugf1("IsItemInSuppressList :: Suppressing %s, Id %s found", itemToCheck->GetName().c_str(), m_IdsToSuppress[iSuppressedIds].c_str());
			return true;
		}
	}

	for ( int iSuppressedTags = 0; iSuppressedTags < m_TagsToSuppress.GetCount(); iSuppressedTags++ )
	{
		if ( itemToCheck->HasEntryForTag(m_TagsToSuppress[iSuppressedTags]) )
		{
			commerceDebugf1("IsItemInSuppressList :: Suppressing %s, Tag %s found", itemToCheck->GetName().c_str(), m_TagsToSuppress[iSuppressedTags].c_str());
			return true;
		}
	}

	return false;
}

void cCommerceData::AddSubscriptionId(const char* subscriptionId)
{
	if(subscriptionId == nullptr)
		return;

	commerceDebugf1("AddSubscriptionId :: %s", subscriptionId);

	m_SubscriptionIds.Grow() = subscriptionId;
}

bool cCommerceData::HasSubscriptionId(const char* subscriptionId) const
{
	if(subscriptionId == nullptr)
		return false;

	for(int iSubscriptionId = 0; iSubscriptionId < m_SubscriptionIds.GetCount(); iSubscriptionId++)
	{
		if(m_SubscriptionIds[iSubscriptionId] == subscriptionId)
			return true;
	}

	// not found
	return false;
}

bool cCommerceData::HasSubscriptionDependency(const cCommerceProductData* itemToCheck) const
{
	// this is true if we have any subscription dependencies
	return itemToCheck && (itemToCheck->GetSubscriptionDependencies().GetCount() > 0);
}

bool cCommerceData::HasMissingSubscriptionDependencies(const cCommerceProductData* itemToCheck) const
{
	// assume false if the item is invalid
	if(!itemToCheck)
		return false;

	// check for subscription dependencies - all dependencies need to be fulfilled for this to pass
	const atArray<atString>& subscriptionDependencies = itemToCheck->GetSubscriptionDependencies();
	for(int iSubscriptionDependency = 0; iSubscriptionDependency < subscriptionDependencies.GetCount(); iSubscriptionDependency++)
	{
		// check for the dependency in our subscriptionId list
		const bool foundDependency = HasSubscriptionId(subscriptionDependencies[iSubscriptionDependency]);
		if(!foundDependency)
		{
			commerceDebugf1("HasMissingSubscriptionDependencies :: %s - Dependency %s not found", itemToCheck->GetName().c_str(), subscriptionDependencies[iSubscriptionDependency].c_str());
			return true;
		}
	}

	return false;
}

bool cCommerceData::HasSubscriptionExclusion(const cCommerceProductData* itemToCheck) const
{
	// this is true if we have any subscription exclusions
	return itemToCheck && (itemToCheck->GetSubscriptionExclusions().GetCount() > 0);
}

bool cCommerceData::HasActiveSubscriptionExclusions(const cCommerceProductData* itemToCheck) const
{
	// check for subscription exclusions - we are looking for any active exclusions
	const atArray<atString>& subscriptionExclusions = itemToCheck->GetSubscriptionExclusions();
	for(int iSubscriptionExclusion = 0; iSubscriptionExclusion < subscriptionExclusions.GetCount(); iSubscriptionExclusion++)
	{
		const bool foundExclusion = HasSubscriptionId(subscriptionExclusions[iSubscriptionExclusion]);
		if(foundExclusion)
		{
			// if we find an exclusion, suppress the item
			commerceDebugf1("HasActiveSubscriptionExclusions :: %s - Exclusion %s found", itemToCheck->GetName().c_str(), subscriptionExclusions[iSubscriptionExclusion].c_str());
			return true;
		}
	}

	return false;
}

bool cCommerceData::HasSubscriptionUpsell(const cCommerceProductData* itemToCheck) const
{
	// this is true if we have any subscription upsells
	return itemToCheck && (itemToCheck->GetSubscriptionUpsells().GetCount() > 0);
}

bool cCommerceData::HasMissingSubscriptionUpsells(const cCommerceProductData* itemToCheck) const
{
	// assume false if the item is invalid
	if(!itemToCheck)
		return false;

	// check for subscription upsells - all upsells need to be fulfilled for this to pass
	const atArray<atString>& subscriptionUpsells = itemToCheck->GetSubscriptionUpsells();
	for(int iSubscriptionUpsell = 0; iSubscriptionUpsell < subscriptionUpsells.GetCount(); iSubscriptionUpsell++)
	{
		// check for the upsell in our subscriptionId list
		const bool foundUpsell = HasSubscriptionId(subscriptionUpsells[iSubscriptionUpsell]);
		if(!foundUpsell)
		{
			commerceDebugf1("HasMissingSubscriptionUpsells :: %s - Upsell %s not found", itemToCheck->GetName().c_str(), subscriptionUpsells[iSubscriptionUpsell].c_str());
			return true;
		}
	}

	return false;
}

void cCommerceData::SuppressItemId( const char * idToSuppress )
{
	if (idToSuppress == nullptr)
	{
		return;
	}

	commerceDebugf1("SuppressItemId :: %s", idToSuppress);
	
	m_IdsToSuppress.Grow() = idToSuppress;
}

void cCommerceData::SuppressTag( const char * tagToSuppress )
{
	if (tagToSuppress == nullptr)
	{
		return;
	}

	commerceDebugf1("SuppressTag :: %s", tagToSuppress);

	m_TagsToSuppress.Grow() = tagToSuppress;
}

void cCommerceData::cCommerceSaxReader::ResetCurrentData()
{
    if ( m_pCurrentItemData )
    {
        m_pCurrentItemData->Init();
    }

    m_LastPriorityTagString.Reset();
    m_LastPriorityTagValue = INVALID_PRIORITY_TAG_VALUE;
}

void cCommerceData::cCommerceSaxReader::Start( cCommerceData* pCommerceData )
{
	commerceAssert(pCommerceData);

	m_pCommercedata = pCommerceData;
	m_State = STATE_EXPECT_ROOT;
	Begin();
}

void cCommerceData::cCommerceSaxReader::startElement( const char* url, const char* localName, const char* qName )
{
	switch( static_cast<int>(m_State) )
	{
	case STATE_EXPECT_ROOT:
		if(0 == stricmp("RageCommerceData", qName))
		{
			m_State = STATE_EXPECT_PRODS_OR_CATS_TAG;
		}
		else 
		{
			commerceErrorf("Error parsing [STATE_EXPECT_ROOT] %s", qName);
		}
		break;
	case STATE_EXPECT_PRODS_OR_CATS_TAG:
		if (0 == stricmp("Products", qName) )
		{
			m_State = STATE_EXPECT_PRODUCT;
		}
		else if ( 0 == stricmp("Categories", qName) )
		{
			m_State = STATE_EXPECT_CATEGORY;
		}
        else if ( 0 == stricmp("Strings", qName) )
        {
            m_State = STATE_EXPECT_STRING;
        }
		else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_PRODS_OR_CATS_TAG] %s", qName);
		}
		break;
	case STATE_EXPECT_PRODUCT:
		if ( 0 == stricmp("Product", qName) )
		{
            //Create an instance of the of commerce product data.
            //This will be deleted in the commerce data reset function.
            commerceAssert( m_pCurrentItemData == nullptr );
            m_pCurrentItemData = rage_new cCommerceProductData;

			//Ensure that the product data structure is cleared
			m_pCurrentItemData->Init();
            
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;	
		}
		else 
		{
			commerceErrorf("Error parsing [STATE_EXPECT_PRODUCT] %s", qName);
		}
		break;
	case STATE_EXPECT_PRODUCT_SUB_ELEMENT:
		startProductSubElement( url, localName, qName );
		break;
	case STATE_EXPECT_DEPENDENCY:
		if ( 0 == stricmp("Dependency", qName) )
		{
			m_State = STATE_READING_DEPENDENCY;	
		}
		else 
		{
			commerceErrorf("Error parsing [STATE_EXPECT_DEPENDENCY] %s", qName);
		}
		break;
    case STATE_EXPECT_ENUM_DEPENDENCY:
        if ( 0 == stricmp("EnumDependency", qName) )
        {
            m_State = STATE_READING_ENUM_DEPENDENCY;	
        }
        else 
        {
            commerceErrorf("Error parsing [STATE_EXPECT_ENUM_DEPENDENCY] %s", qName);
        }
        break;
	case STATE_EXPECT_EXCLUSION:
		if ( 0 == stricmp("Exclusion", qName) )
		{
			m_State = STATE_READING_EXCLUSION;
		}
		else 
		{
			commerceErrorf("Error parsing [STATE_EXPECT_EXCLUSION] %s", qName);
		}
		break;
    case STATE_EXPECT_ENUM_EXCLUSION:
        if ( 0 == stricmp("EnumExclusion", qName) )
        {
            m_State = STATE_READING_ENUM_EXCLUSION;
        }
        else 
        {
            commerceErrorf("Error parsing [STATE_EXPECT_ENUM_EXCLUSION] %s", qName);
        }
        break;
    case STATE_EXPECT_INCLUDEDINID:
        if ( 0 == stricmp("IncludedIn", qName) )
        {
            m_State = STATE_READING_INCLUDEDINID;
        }
        else 
        {
            commerceErrorf("Error parsing [STATE_EXPECT_INCLUDEDINID] %s", qName);
        }
        break;
	case STATE_EXPECT_CATEGORYMEMBERSHIP:
		if ( 0 == stricmp("CatMembership", qName) )
		{
			m_State = STATE_READING_CATEGORYMEMBERSHIP;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_CATEGORYMEMBERSHIP] %s", qName);
		}
		break;
    case STATE_EXPECT_IMAGEPATH:
        if ( 0 == stricmp("ImagePath", qName) )
        {
            m_State = STATE_READING_IMAGEPATH;
        }
        else
        {
            commerceErrorf("Error parsing [STATE_EXPECT_IMAGEPATH] %s", qName);
        }
        break;
    case STATE_EXPECT_CONTAINED_PACKAGE_NAME:
        if ( 0 == stricmp("ContainedPackageName", qName))
        {
            m_State = STATE_READING_CONTAINED_PACKAGE_NAME;
        }
        else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_CONTAINED_PACKAGE_NAME] %s", qName);
        }
        break;
    case STATE_EXPECT_PRIORITY_TAG:
        if ( 0 == stricmp("PriorityTag", qName))
        {
            m_State = STATE_READING_PRIORITY_TAG;
        }
        else
        {
            commerceErrorf("Error parsing [STATE_EXPECT_PRIORITY_TAG] %s", qName);
        }
        break;
	case STATE_EXPECT_SUBSCRIPTION_DEPENDENCY:
		if (0 == stricmp("SubscriptionDependency", qName))
		{
			m_State = STATE_READING_SUBSCRIPTION_DEPENDENCY;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_SUBSCRIPTION_DEPENDENCY] %s", qName);
		}
		break;
	case STATE_EXPECT_SUBSCRIPTION_EXCLUSION:
		if (0 == stricmp("SubscriptionExclusion", qName))
		{
			m_State = STATE_READING_SUBSCRIPTION_EXCLUSION;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_SUBSCRIPTION_EXCLUSION] %s", qName);
		}
		break;
	case STATE_EXPECT_SUBSCRIPTION_UPSELL:
		if (0 == stricmp("SubscriptionUpsell", qName))
		{
			m_State = STATE_READING_SUBSCRIPTION_UPSELL;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_SUBSCRIPTION_UPSELL] %s", qName);
		}
		break;
	case STATE_EXPECT_CATEGORY:
		if ( 0 == stricmp("Category", qName) )
		{
            //Create an instance of the of commerce product data.
            //This will be deleted in the commerce data reset function.
            commerceAssert( m_pCurrentItemData == nullptr );
            m_pCurrentItemData = rage_new cCommerceCategoryData;
            m_pCurrentItemData->Init();

			m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;	
		}
		else 
		{
			commerceErrorf("Error parsing [STATE_EXPECT_CATEGORY] %s", qName);
		}
		break;
	case STATE_EXPECT_CATEGORY_SUB_ELEMENT:
		startCategorySubElement( url, localName, qName );
		break;
	default:
		commerceErrorf("Error parsing [STATE_EXPECT_CATEGORY_SUB_ELEMENT] %s", qName);
	}
}

void cCommerceData::cCommerceSaxReader::startProductSubElement( const char* /*OUTPUT_ONLY(url)*/, const char* /*localName*/, const char* qName )
{
	if (0 == stricmp("Name", qName) )
	{
		m_State = STATE_READING_PRODNAME;
	}
	else if (0 == stricmp("ShortDesc", qName) )
	{
		m_State = STATE_READING_SHORTDESC;
	}
	else if (0 == stricmp("LongDesc", qName) )
	{
		m_State = STATE_READING_LONGDESC;
	}
	else if (0 == stricmp("Dependencies", qName) )
	{
		m_State = STATE_EXPECT_DEPENDENCY;
	}
    else if (0 == stricmp("EnumDependencies", qName) )
    {
        m_State = STATE_EXPECT_ENUM_DEPENDENCY;
    }
	else if (0 == stricmp("Exclusions", qName) )
	{
		m_State = STATE_EXPECT_EXCLUSION;
	}
    else if (0 == stricmp("EnumExclusions", qName) )
    {
        m_State = STATE_EXPECT_ENUM_EXCLUSION;
    }
    else if (0 == stricmp("IncludedInIDs" , qName) )
    {
        m_State = STATE_EXPECT_INCLUDEDINID;
    }
	else if (0 == stricmp("CatMemberships", qName) )
	{
		m_State = STATE_EXPECT_CATEGORYMEMBERSHIP;
	}
    else if (0 == stricmp("ImagePaths", qName) )
    {
        m_State = STATE_EXPECT_IMAGEPATH;
    }
    else if (0  == stricmp("ContainedPackageNames", qName)  )
    {
        m_State = STATE_EXPECT_CONTAINED_PACKAGE_NAME;
    }
    else if (0 == stricmp("PriorityTags", qName) )
    {
        m_State = STATE_EXPECT_PRIORITY_TAG;
	}
	else if (0 == stricmp("SubscriptionDependencies", qName))
	{
		m_State = STATE_EXPECT_SUBSCRIPTION_DEPENDENCY;
	}
	else if (0 == stricmp("SubscriptionExclusions", qName))
	{
		m_State = STATE_EXPECT_SUBSCRIPTION_EXCLUSION;
	}
	else if (0 == stricmp("SubscriptionUpsells", qName))
	{
		m_State = STATE_EXPECT_SUBSCRIPTION_UPSELL;
	}
	else
	{
		commerceErrorf("Error parsing (startProductSubElement) %s", qName);
	}
}

void cCommerceData::cCommerceSaxReader::startCategorySubElement( const char* /*OUTPUT_ONLY(url)*/, const char* /*localName*/, const char* qName )
{
	if (0 == stricmp("Name", qName))
	{
		m_State = STATE_READING_CATNAME;
	}
	else if (0 == stricmp("Desc", qName))
	{
		m_State = STATE_READING_CATDESC;
	}
    else if (0 == stricmp("CatMemberships", qName) )
    {
        m_State = STATE_EXPECT_CATEGORYMEMBERSHIP;
    }
    else if (0 == stricmp("ImagePaths", qName))
    {
        m_State = STATE_EXPECT_IMAGEPATH;
    }
    else if (0 == stricmp("Dependencies", qName) )
    {
        m_State = STATE_EXPECT_DEPENDENCY;
    }
    else if (0 == stricmp("EnumDependencies", qName) )
    {
        m_State = STATE_EXPECT_ENUM_DEPENDENCY;
    }
    else if (0 == stricmp("Exclusions", qName) )
    {
        m_State = STATE_EXPECT_EXCLUSION;
    }
    else if (0 == stricmp("EnumExclusions", qName) )
    {
        m_State = STATE_EXPECT_ENUM_EXCLUSION;
    }
    else if (0 == stricmp("PriorityTags", qName) )
    {
        m_State = STATE_EXPECT_PRIORITY_TAG;
    }
	else
	{
		commerceErrorf("Error parsing (startCategorySubElement) %s", qName);
	}
}

void cCommerceData::cCommerceSaxReader::endElement( const char* /*OUTPUT_ONLY(url)*/, const char* /*localName*/, const char* qName )
{
	switch( static_cast<int>(m_State) )
	{
	case(STATE_READING_CATDESC):
		if (0 == stricmp("Desc", qName))
		{
			m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_CATDESC] %s", qName);
		}
		break;
	case(STATE_READING_CATNAME):
		if (0 == stricmp("Name", qName))
		{
			m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_CATNAME] %s", qName);
		}
		break;
	case(STATE_EXPECT_CATEGORY_SUB_ELEMENT):
		if (0 == stricmp("Category", qName))
		{		
            //Push the populated category data onto the commerce data array.
            commerceAssert( m_pCurrentItemData );
            commerceAssert( m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_CATEGORY );

			if (m_pCommercedata->IsItemInSuppressList(m_pCurrentItemData))
			{
				commerceDebugf1("[STATE_EXPECT_CATEGORY_SUB_ELEMENT] :: Not adding %s, IsItemInSuppressList", m_pCurrentItemData->GetName().c_str());
				delete m_pCurrentItemData;
			}
			else
			{
				commerceDebugf1("[STATE_EXPECT_CATEGORY_SUB_ELEMENT] :: Adding %s", m_pCurrentItemData->GetName().c_str());
				m_pCommercedata->m_pCommerceItemDataArray.Grow() = m_pCurrentItemData;
			}

            //We are now done with our stored pointer of the category data, nullptr it so that we can confirm cleanup.
            m_pCurrentItemData = nullptr;

			m_State = STATE_EXPECT_CATEGORY;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_CATEGORY_SUB_ELEMENT] %s", qName);
		}
		break;
	case(STATE_EXPECT_CATEGORY):
		if (0 == stricmp("Categories", qName))
		{
			m_State = STATE_EXPECT_PRODS_OR_CATS_TAG;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_CATEGORY] %s", qName);
		}
		break;
	case(STATE_READING_CATEGORYMEMBERSHIP):
		if (0 == stricmp("CatMembership", qName))
		{
			//TODO: Push out the cat membership
			m_State = STATE_EXPECT_CATEGORYMEMBERSHIP;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_CATEGORYMEMBERSHIP] %s", qName);
		}
		break;	
    case(STATE_READING_IMAGEPATH):
        if (0 == stricmp("ImagePath", qName))
        {
            m_State = STATE_EXPECT_IMAGEPATH;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_READING_IMAGEPATH] %s", qName);
        }
        break;	
	case(STATE_EXPECT_CATEGORYMEMBERSHIP):
		if (0 == stricmp("CatMemberships", qName))
		{
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_CATEGORYMEMBERSHIP] %s", qName);
		}
		break;
    case(STATE_EXPECT_IMAGEPATH):
        if (0 == stricmp("ImagePaths", qName))
        {
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_EXPECT_IMAGEPATH] %s", qName);
        }
        break;
    case(STATE_EXPECT_CONTAINED_PACKAGE_NAME):
        if (0 == stricmp("ContainedPackageNames", qName))
        {
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_EXPECT_CONTAINED_PACKAGE_NAME] %s", qName);
        }
        break;
    case(STATE_READING_CONTAINED_PACKAGE_NAME):
        if (0 == stricmp("ContainedPackageName", qName))
        {
            m_State = STATE_EXPECT_CONTAINED_PACKAGE_NAME;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_READING_CONTAINED_PACKAGE_NAME] %s", qName);
        }
        break;	
    case(STATE_EXPECT_PRIORITY_TAG):
        if (0 == stricmp("PriorityTags", qName))
        {
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_EXPECT_PRIORITY_TAG] %s", qName);
        }
        break;
    case(STATE_READING_PRIORITY_TAG):
        if (0 == stricmp("PriorityTag", qName))
        {
            //Do we have a valid value and tag?
            if ( m_LastPriorityTagValue != INVALID_PRIORITY_TAG_VALUE && m_LastPriorityTagString.GetLength() > 0 )
            {
                m_pCurrentItemData->AddPriorityTag(m_LastPriorityTagString, m_LastPriorityTagValue);
            }
            //Clean up our holding variables
            m_LastPriorityTagValue = INVALID_PRIORITY_TAG_VALUE;
            m_LastPriorityTagString.Reset();

            m_State = STATE_EXPECT_PRIORITY_TAG;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_READING_PRIORITY_TAG] %s", qName);
        }
        break;	
	case(STATE_READING_EXCLUSION):
		if (0 == stricmp("Exclusion", qName))
		{
			m_State = STATE_EXPECT_EXCLUSION;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_EXCLUSION] %s", qName);
		}
		break;
	case(STATE_EXPECT_EXCLUSION):
		if (0 == stricmp("Exclusions", qName))
		{
			if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_EXCLUSION] %s", qName);
		}
		break;
    case(STATE_READING_ENUM_EXCLUSION):
        if (0 == stricmp("EnumExclusion", qName))
        {
            m_State = STATE_EXPECT_ENUM_EXCLUSION;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_READING_ENUM_EXCLUSION] %s", qName);
        }
        break;
    case(STATE_EXPECT_ENUM_EXCLUSION):
        if (0 == stricmp("EnumExclusions", qName))
        {
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_EXPECT_ENUM_EXCLUSION] %s", qName);
        }
        break;
    case(STATE_READING_INCLUDEDINID):
        if (0 == stricmp("IncludedIn", qName))
        {
            m_State = STATE_EXPECT_INCLUDEDINID;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_READING_INCLUDEDINID] %s", qName);
        }
        break;
    case(STATE_EXPECT_INCLUDEDINID):
        if (0 == stricmp("IncludedInIDs", qName))
        {
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_EXPECT_INCLUDEDINID] %s", qName);
        }
        break;
	case(STATE_READING_DEPENDENCY):
		if (0 == stricmp("Dependency", qName))
		{
			m_State = STATE_EXPECT_DEPENDENCY;
		}
		else
		{
			commerceErrorf("Error parsing End ELement [STATE_READING_DEPENDENCY] %s", qName);
		}
		break;
	case(STATE_EXPECT_DEPENDENCY):
		if (0 == stricmp("Dependencies", qName))
		{
            if(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT)
                m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
            else
                m_State = STATE_EXPECT_CATEGORY_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_DEPENDENCY] %s", qName);
		}
		break;
    case(STATE_READING_ENUM_DEPENDENCY):
        if (0 == stricmp("EnumDependency", qName))
        {
            m_State = STATE_EXPECT_ENUM_DEPENDENCY;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_READING_ENUM_DEPENDENCY] %s", qName);
        }
        break;
    case(STATE_EXPECT_ENUM_DEPENDENCY):
        if (0 == stricmp("EnumDependencies", qName))
        {
            m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
        }
        else
        {
            commerceErrorf("Error parsing End Element [STATE_EXPECT_ENUM_DEPENDENCY] %s", qName);
        }
        break;
	case(STATE_READING_LONGDESC):
		if (0 == stricmp("LongDesc", qName))
		{
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_READING_LONGDESC] %s", qName);
		}
		break;
	case(STATE_READING_SHORTDESC):
		if (0 == stricmp("ShortDesc", qName))
		{
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_READING_SHORTDESC] %s", qName);
		}
		break;
	case(STATE_READING_PRODNAME):
		if (0 == stricmp("Name", qName))
		{
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_READING_PRODNAME] %s", qName);
		}
		break;
	case(STATE_EXPECT_PRODUCT_SUB_ELEMENT):
		if (0 == stricmp("Product", qName) )
		{
            //Push the populated category data onto the commerce data array.
            commerceAssert( m_pCurrentItemData );
            commerceAssert( m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT );
            
			if ( m_pCommercedata->IsItemInSuppressList(m_pCurrentItemData) )
			{
				commerceDebugf1("[STATE_EXPECT_PRODUCT_SUB_ELEMENT] :: Not adding %s, IsItemInSuppressList", m_pCurrentItemData->GetName().c_str());
				delete m_pCurrentItemData;
			}
			else if (m_pCommercedata->HasActiveSubscriptionExclusions(static_cast<cCommerceProductData*>(m_pCurrentItemData)))
			{
				// adding a note that we allow products with HasMissingSubscriptionDependencies - we control visibility on those at the store level
				commerceDebugf1("[STATE_EXPECT_PRODUCT_SUB_ELEMENT] :: Not adding %s, HasActiveSubscriptionExclusions", m_pCurrentItemData->GetName().c_str());
				delete m_pCurrentItemData;
			}
			else
			{
				commerceDebugf1("[STATE_EXPECT_PRODUCT_SUB_ELEMENT] :: Adding %s", m_pCurrentItemData->GetName().c_str());
				m_pCommercedata->m_pCommerceItemDataArray.Grow() = m_pCurrentItemData;
			}

            //We are now done with our stored pointer of the product data, nullptr it so that we can confirm cleanup.
            m_pCurrentItemData = nullptr;

			m_State = STATE_EXPECT_PRODUCT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_PRODUCT_SUB_ELEMENT] %s", qName);
		}
		break;
	case(STATE_EXPECT_PRODUCT):
		if (0 == stricmp("Products", qName) )
		{
			m_State = STATE_EXPECT_PRODS_OR_CATS_TAG;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_PRODUCT] %s", qName);
		}
		break;
    case(STATE_EXPECT_STRING):
        if (0 == stricmp("Strings", qName) )
        {
            m_State = STATE_EXPECT_PRODS_OR_CATS_TAG;
        }
        break;
	case(STATE_EXPECT_PRODS_OR_CATS_TAG):
		if (0 == stricmp("RageCommerceData", qName) )
		{
			m_State = STATE_EXPECT_ROOT;
		}
		else
		{
			commerceErrorf("Error parsing [STATE_EXPECT_PRODS_OR_CATS_TAG] %s", qName);
		}
		break;
	case(STATE_READING_SUBSCRIPTION_DEPENDENCY):
		if (0 == stricmp("SubscriptionDependency", qName))
		{
			m_State = STATE_EXPECT_SUBSCRIPTION_DEPENDENCY;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_SUBSCRIPTION_DEPENDENCY] %s", qName);
		}
		break;
	case(STATE_EXPECT_SUBSCRIPTION_DEPENDENCY):
		if (0 == stricmp("SubscriptionDependencies", qName))
		{
			commerceAssert(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT);
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_SUBSCRIPTION_DEPENDENCY] %s", qName);
		}
		break;
	case(STATE_READING_SUBSCRIPTION_EXCLUSION):
		if (0 == stricmp("SubscriptionExclusion", qName))
		{
			m_State = STATE_EXPECT_SUBSCRIPTION_EXCLUSION;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_SUBSCRIPTION_EXCLUSION] %s", qName);
		}
		break;
	case(STATE_EXPECT_SUBSCRIPTION_EXCLUSION):
		if (0 == stricmp("SubscriptionExclusions", qName))
		{
			commerceAssert(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT);
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_SUBSCRIPTION_EXCLUSION] %s", qName);
		}
		break;
	case(STATE_READING_SUBSCRIPTION_UPSELL):
		if (0 == stricmp("SubscriptionUpsell", qName))
		{
			m_State = STATE_EXPECT_SUBSCRIPTION_UPSELL;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_READING_SUBSCRIPTION_UPSELL] %s", qName);
		}
		break;
	case(STATE_EXPECT_SUBSCRIPTION_UPSELL):
		if (0 == stricmp("SubscriptionUpsells", qName))
		{
			commerceAssert(m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT);
			m_State = STATE_EXPECT_PRODUCT_SUB_ELEMENT;
		}
		else
		{
			commerceErrorf("Error parsing End Element [STATE_EXPECT_SUBSCRIPTION_UPSELL] %s", qName);
		}
		break;
	}
}	

void cCommerceData::cCommerceSaxReader::attribute( const char* /*tagName*/, const char* attrName, const char* attrVal )
{
	switch( static_cast<int>(m_State) )
	{
	case(STATE_EXPECT_CATEGORY_SUB_ELEMENT):
		if(0 == stricmp("id", attrName))
		{
			if ( strlen(attrVal) > CATEGORY_ID_MAX_LEN )
			{
				commerceErrorf("Malformed id attribute in Category tag");
			}
			else
			{
                m_pCurrentItemData->GetId() = attrVal;
			}
		}
		break;
	case(STATE_EXPECT_PRODUCT_SUB_ELEMENT):
		if(0 == stricmp("id", attrName))
		{
			if ( strlen(attrVal) > PRODUCT_ID_MAX_LEN )
			{
				commerceErrorf("Malformed id attribute in Product tag");
			}
			else
			{
                m_pCurrentItemData->GetId() = attrVal;
			}
		}
        else if (0 == stricmp("displayVirtual", attrName))
        {
            bool bDisplayVirtual = !( strlen(attrVal) > 0 && (attrVal[0] == 'f' || attrVal[0] == 'F' || attrVal[0] == '0'));
            static_cast<cCommerceProductData*>( m_pCurrentItemData )->SetDisplayVirtualFlag(bDisplayVirtual);
        }
        else if (0 == stricmp("virtualIfInstalled", attrName))
        {
            bool bVirtualIfInstalled = !( strlen(attrVal) > 0 && (attrVal[0] == 'f' || attrVal[0] == 'F' || attrVal[0] == '0'));
            static_cast<cCommerceProductData*>( m_pCurrentItemData )->SetVirtualIfInstalledFlag(bVirtualIfInstalled);
        }
		else if ( 0 == stricmp("comingsoon", attrName) )
		{
			bool bComingSoon = !( strlen(attrVal) > 0 && (attrVal[0] == 'f' || attrVal[0] == 'F' || attrVal[0] == '0'));
			static_cast<cCommerceProductData*>( m_pCurrentItemData )->SetIsComingSoon(bComingSoon);
		}
		else if ( 0 == stricmp("platformProductId", attrName) )
		{
			if ( strlen(attrVal) > PLATFORM_PRODUCT_ID_MAX_LEN )
			{
				commerceErrorf("Malformed platformProductId attribute in Product tag");
			}
			else
			{
                commerceAssert( m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT );
                static_cast<cCommerceProductData*>( m_pCurrentItemData )->GetPlatformId() = attrVal;
			}
		}
        else if ( 0 == stricmp("consumableId", attrName) )
        {
            if ( strlen(attrVal) > PRODUCT_ID_MAX_LEN )
            {
                commerceErrorf("Malformed consumable id attribute in Product tag");
            }
            else
            {
                commerceAssert( m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT );
                static_cast<cCommerceProductData*>( m_pCurrentItemData )->GetConsumableId() = attrVal;
            }
        }
        else if ( 0 == stricmp("releaseTime", attrName) )
        {
            char* bp;
#if __WIN32
            u64 releaseTime = _strtoui64( attrVal, &bp, 0);
#else
            u64 releaseTime = strtoll( attrVal, &bp, 0);
#endif
            static_cast<cCommerceProductData*>( m_pCurrentItemData )->SetReleaseTime( releaseTime );
        }
		break;

	case(STATE_READING_DEPENDENCY):
		if (0 == stricmp("id", attrName))
		{
			if ( strlen(attrVal) > PLATFORM_PRODUCT_ID_MAX_LEN )
			{
				commerceErrorf("Malformed dependency Id attribute in Dependency tag");
			}
			else
			{
				atString dependencyId = atString(attrVal);              
                m_pCurrentItemData->GetDependencyIds().Append() = dependencyId;
            }
		}
		break;
    case(STATE_READING_ENUM_DEPENDENCY):
        if (0 == stricmp("id", attrName))
        {
            if ( strlen(attrVal) > PLATFORM_PRODUCT_ID_MAX_LEN )
            {
                commerceErrorf("Malformed dependency Id attribute in Dependency tag");
            }
            else
            {
                atString enumDependencyId = atString(attrVal);
               m_pCurrentItemData->GetEnumDependencyIds().Append() = enumDependencyId;
            }
        }
        break;
	case(STATE_READING_EXCLUSION):
		if (0 == stricmp("id", attrName))
		{
			if ( strlen(attrVal) > PLATFORM_PRODUCT_ID_MAX_LEN )
			{
				commerceErrorf("Malformed dependency Id attribute in Exclusion tag");
			}
			else
			{
				atString exclusionId = atString(attrVal);
                
                m_pCurrentItemData->GetExclusionIds().Append() = exclusionId;
			}
		}
		break;
    case(STATE_READING_ENUM_EXCLUSION):
        if (0 == stricmp("id", attrName))
        {
            if ( strlen(attrVal) > PLATFORM_PRODUCT_ID_MAX_LEN )
            {
                commerceErrorf("Malformed dependency Id attribute in EnumExclusion tag");
            }
            else
            {
                atString enumExclusionId = atString(attrVal);
                
                m_pCurrentItemData->GetEnumExclusionIds().Append() = enumExclusionId;
            }
        }
        break;
    case(STATE_READING_INCLUDEDINID):
        if (0 == stricmp("id", attrName))
        {
            if ( strlen(attrVal) > PRODUCT_ID_MAX_LEN )
            {
                commerceErrorf("Malformed dependency Id attribute in IncludedIn tag");
            }
            else
            {
                atString includedInId = atString(attrVal);
                commerceAssert( m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT );
                static_cast<cCommerceProductData*>( m_pCurrentItemData )->GetIncludedInProductIds().Append() = includedInId;
            }
        }
        break;
	case(STATE_READING_CATEGORYMEMBERSHIP):
		if (0 == stricmp("id", attrName))
		{
			if ( strlen(attrVal) > CATEGORY_ID_MAX_LEN)
			{
				commerceErrorf("Malformed category Id attribute in CatMembership tag");
			}
			else
			{
				atString categoryId = atString(attrVal);
                m_pCurrentItemData->GetCategoryMemberships().Append() = categoryId;
			}
		}
		break;
    case(STATE_READING_IMAGEPATH):
        if (0 == stricmp("path", attrName))
        {
            if ( strlen(attrVal) > IMAGE_PATH_MAX_LEN)
            {
                commerceErrorf("Malformed category path attribute in ImagePath tag");
            }
            else
            {
                atString categoryId = atString(attrVal);
                m_pCurrentItemData->GetImagePaths().Append() = categoryId;
            }
        }
		break;
	case(STATE_EXPECT_IMAGEPATH):
		if (0 == stricmp("template", attrName))
		{
			int iTemplate = atoi(attrVal);
			m_pCurrentItemData->SetImageTemplate(iTemplate);
		}	
        break;
    case(STATE_READING_CONTAINED_PACKAGE_NAME):
        if (0 == stricmp("name", attrName))
        {
            commerceAssert( m_pCurrentItemData->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT );
            if (m_pCurrentItemData->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT)
            {
                return;
            }
            atString packageName = atString(attrVal);
            static_cast<cCommerceProductData*>( m_pCurrentItemData )->GetContainedPackageNames().Append() = packageName;
        }
        break;
    case(STATE_READING_PRIORITY_TAG):
        if (0 == stricmp("name", attrName))
        {
            m_LastPriorityTagString = atString(attrVal);
        }
        else if (0 == stricmp("priority", attrName))
        {
            m_LastPriorityTagValue = atoi(attrVal);
        }
        break;
	case(STATE_READING_SUBSCRIPTION_DEPENDENCY):
		if (0 == stricmp("id", attrName))
		{
			if (strlen(attrVal) > SUBSCRIPTION_ID_MAX_LEN)
			{
				commerceErrorf("Malformed subscription Id attribute in SubscriptionDependency tag");
			}
			else
			{
				atString subscriptionId = atString(attrVal);
				static_cast<cCommerceProductData*>(m_pCurrentItemData)->GetSubscriptionDependencies().Append() = subscriptionId;
			}
		}
		break;
	case(STATE_READING_SUBSCRIPTION_EXCLUSION):
		if (0 == stricmp("id", attrName))
		{
			if (strlen(attrVal) > SUBSCRIPTION_ID_MAX_LEN)
			{
				commerceErrorf("Malformed subscription Id attribute in SubscriptionExclusion tag");
			}
			else
			{
				atString subscriptionId = atString(attrVal);
				static_cast<cCommerceProductData*>(m_pCurrentItemData)->GetSubscriptionExclusions().Append() = subscriptionId;
			}
		}
		break;
	case(STATE_READING_SUBSCRIPTION_UPSELL):
		if (0 == stricmp("id", attrName))
		{
			if (strlen(attrVal) > SUBSCRIPTION_ID_MAX_LEN)
			{
				commerceErrorf("Malformed subscription Id attribute in SubscriptionUpsell tag");
			}
			else
			{
				atString subscriptionId = atString(attrVal);
				static_cast<cCommerceProductData*>(m_pCurrentItemData)->GetSubscriptionUpsells().Append() = subscriptionId;
			}
		}
		break;
	}
}

void cCommerceData::cCommerceSaxReader::characters( const char* ch, const int start, const int length )
{   
    atString temp;
    temp.Set( ch, istrlen(ch) , start, length );

	switch( static_cast<int>(m_State) )
	{
	case(STATE_READING_CATNAME):
        m_pCurrentItemData->GetName() += temp;
		commerceDebugf3( "Set current category name to: %s", m_pCurrentItemData->GetName().c_str() );
		break;
	case(STATE_READING_CATDESC):        
        m_pCurrentItemData->GetDescription() += temp;
        commerceDebugf3( "Set current category description to: %s", m_pCurrentItemData->GetDescription().c_str() );
		break;
	case(STATE_READING_PRODNAME):        
        m_pCurrentItemData->GetName() += temp;
        commerceDebugf3( "Set current product name to: %s", m_pCurrentItemData->GetName().c_str() );
		break;
	case(STATE_READING_SHORTDESC):       
        m_pCurrentItemData->GetDescription() += temp;
		commerceDebugf3( "Set current product short desc to: %s", m_pCurrentItemData->GetDescription().c_str() );
		break;
	case(STATE_READING_LONGDESC):        
        m_pCurrentItemData->GetLongDescription() += temp;
		commerceDebugf3( "Appended current product long desc. Now reads: %s", m_pCurrentItemData->GetLongDescription().c_str() );
		break;
	}
}

cCommerceItemData::cCommerceItemData() 
	: m_Initialised( false ),
    m_DependencyEnumerated( true ),
    m_EnumDependencyEnumerated( true ),
    m_ExclusionEnumerated( false ),
    m_EnumExclusionEnumerated( false ),
	m_ImageTemplate(0)
{

}

void cCommerceItemData::Init()
{
    if ( !m_Initialised )
    {
        m_CategoryMemberships.Reserve(10);
        m_ImagePaths.Reserve(10);
        m_Initialised = true;
		m_ImageTemplate = 0;

        m_ExclusionIds.Reserve(10);
        m_DependencyIds.Reserve(10);
        m_EnumDependencyIds.Reserve(10);
        m_EnumExclusionIds.Reserve(10);
    }

    m_PriorityTagMap.Reset();
    ResetEnumerationFlags();
}

void cCommerceItemData::ResetEnumerationFlags()
{
    m_DependencyEnumerated = true;
    m_EnumDependencyEnumerated = true;
    m_ExclusionEnumerated = false;
    m_EnumExclusionEnumerated = false;
}

void cCommerceItemData::AddPriorityTag( const char* tagString, int priority )
{
    atString tag(tagString);
    int *result = m_PriorityTagMap.Access(tag);

    if ( result == nullptr )
    {
        m_PriorityTagMap.Insert(tag,priority);
    }
    else
    {
        *result = priority;
    }
}

int cCommerceItemData::GetPriorityForTag( const char* tagString ) const
{
    atString tag(tagString);
    const int *result = m_PriorityTagMap.Access(tag);

    if ( result == nullptr )
    {
        return 0;
    }
    else
    {
        return *result;
    }
}

int cCommerceItemData::HasEntryForTag( const char* tagString ) const
{
	atString tag(tagString);
	const int *result = m_PriorityTagMap.Access(tag);

	return ( result != nullptr );
}

cCommerceProductData::cCommerceProductData() :
    m_ReleaseTime( 0 ),
    m_ProductEnumerated( false ),
    m_DisplayWhenVirtual( false ),
    m_VirtualIfInstalled( false ),
	m_bComingSoon(false)
{
    m_ContainedPackageNames.Reset();
    m_ContainedPackageNames.Reserve(10);

	m_SubscriptionDependencies.Reset();
	m_SubscriptionDependencies.Reserve(2);

	m_SubscriptionExclusions.Reset();
	m_SubscriptionExclusions.Reserve(2);

	m_SubscriptionUpsells.Reset();
	m_SubscriptionUpsells.Reserve(2);
}

void cCommerceProductData::Init()
{
    if ( !m_Initialised )
    {
        m_IncludedInProductIds.Reserve(10);
    }

    m_ReleaseTime = 0;
	m_bComingSoon = false;

    ResetEnumerationFlags();

    m_DisplayWhenVirtual = false;
    m_VirtualIfInstalled = false;

    m_ConsumableId.Reset();
    
    m_ContainedPackageNames.Reset();
    m_ContainedPackageNames.Reserve(10);
    
    cCommerceItemData::Init();
}

void cCommerceProductData::ResetEnumerationFlags()
{
    m_ProductEnumerated = false; 

    cCommerceItemData::ResetEnumerationFlags();
}

void cCommerceCategoryData::AddItemIndex( int a_NewIndex )
{
    m_ItemIndexArray.Append() = a_NewIndex;
}

void cCommerceCategoryData::SetItemArraySize( int a_NewProdArraySize )
{
    m_ItemIndexArray.Reset();
    m_ItemIndexArray.Reserve( a_NewProdArraySize );
}

int cCommerceCategoryData::GetItemIndex( int a_CatProdIndex ) const
{
    if ( a_CatProdIndex >= m_ItemIndexArray.GetCount() )
    {
        return -1;
    }
    else
    {
        return m_ItemIndexArray[ a_CatProdIndex ];
    }
}

cCommerceStringTable::cCommerceStringTable()
{
    m_ErrorString.Reset();
}

void cCommerceStringTable::Init()
{
    m_StringMap.Reset();
}

void cCommerceStringTable::Shutdown()
{
    m_StringMap.Reset();
}

bool cCommerceStringTable::IsStringPresent( const atString a_StringCode ) const
{
    const atString *retString = m_StringMap.Access( a_StringCode );
    return (retString != nullptr);
}

const atString& cCommerceStringTable::GetString( const atString a_StringCode ) const
{
    const atString *retString = m_StringMap.Access( a_StringCode );
    
    if ( retString )
    {
        return *retString;
    }
    else
    {
        commerceErrorf("Attempt to return string %s that does not exist. Returning error string.\n", a_StringCode.c_str());
        return m_ErrorString;
    }
}

bool cCommerceStringTable::AddString( const atString a_StringCode, const atString a_String )
{
    atString *existingString = m_StringMap.Access( a_StringCode );    

    if ( existingString )
    {
        //String code is already in the map.
        *existingString = a_String;
    }
    else
    {
        //String code is new, re-add.
        m_StringMap.Insert( a_StringCode, a_String );
    }

    return true;
}

void cCommerceStringTable::DumpStringTableInfo()
{
    commerceDebugf1("Commerce string table debug dump\n");
    commerceDebugf1("Strings stored: %d\n", m_StringMap.GetNumUsed());
}

}
