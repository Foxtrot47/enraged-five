#include "CommerceUtil.h"

#include <string.h>

#include "rline/rl.h"

//Sorry PC guys. I will enable this once the RoS Entitlement and rgsc code is brought across into the GTAV tree.
/*
#include "pc/PCCommerceUtil.h"
#include "pc/SteamCommerceUtil.h"
*/

#include "PS3/PS3NpCommerceUtil.h"
#include "Xbox360/XboxCommerceUtil.h"
#include "Orbis/OrbisCommerceUtil.h"
#include "Durango/DurangoCommerceUtil.h"
#include "CommerceChannel.h"

//#include "fwsys/meminfo.h"
#include "system/memory.h"
#include "system/param.h"

RAGE_DEFINE_CHANNEL(commerce)

#define MASTER_CATEGORY_AT_CAT_INDEX_0 0

PARAM(commerceIgnoreEnumeratedFlag, "[commerce] Add products whether they are enumerated or not");

namespace rage
{

cCommerceUtil* cCommerceUtil::m_BaseInstance = nullptr;

const int CONTAINING_ITEM_MAX_TEST_ARRAY_SIZE = 10;
const int MAX_POPULATE_RECURSE_DEPTH = 10;

cCommerceUtil::cCommerceUtil() :
    m_BaseState( BASE_STATE_INITIAL ),
    m_IsProductInstalledCBPtr( nullptr ),
    m_CategoryDataPopulated( false ),
    m_PendingPlatformDataRequest( false ),  
    m_pCommerceAllocator( nullptr ),
	m_ErrorCode ( CERROR_NONE ),
    m_WhitelistingFailed(false),
    m_DidLastCheckoutSucceed(false)
{
    if ( m_BaseInstance == nullptr )
    {
        m_BaseInstance = this;
    }

    m_SortingTag.Reset();
}

cCommerceUtil::~cCommerceUtil( )
{
    Shutdown();

    m_BaseInstance = nullptr;
}
 
bool cCommerceUtil::Init( int userIndex, const char *skuFolder )
{ 
	//TODO: Properly find the signed in user.
	m_CommerceData.Init( userIndex, skuFolder );
	m_BaseState = BASE_STATE_INITIAL;
	m_ErrorCode = CERROR_NONE;
	
    m_WhitelistingFailed = false;

    m_SortingTag.Reset();

    return true;
} 

void cCommerceUtil::Shutdown()
{
	m_CommerceData.Shutdown();

    m_BaseState = BASE_STATE_INITIAL;
	m_ErrorCode = CERROR_NONE;
    m_IsProductInstalledCBPtr = nullptr;
    m_CategoryDataPopulated = false;
    m_PendingPlatformDataRequest = false;
    m_pCommerceAllocator = nullptr;
} 

bool cCommerceUtil::IsPlatformReady() const
{
    return true;
} 

const char * cCommerceUtil::GetItemName( unsigned index ) const
{
    commerceAssert( m_CommerceData.HasValidData() );

    if ( !m_CommerceData.HasValidData() )
    {
        return nullptr;
    }

    return m_CommerceData.GetItemData( index )->GetName();
}

const char * cCommerceUtil::GetItemDescription( unsigned index ) const
{
    commerceAssert( m_CommerceData.HasValidData() );

    if ( !m_CommerceData.HasValidData() )
    {
        return nullptr;
    }

    return m_CommerceData.GetItemData( index )->GetDescription();
}

const char * cCommerceUtil::GetItemLongDescription( const cCommerceProductData* product )
{
    commerceAssert( m_CommerceData.HasValidData() );

    if ( !m_CommerceData.HasValidData() )
    {
        return nullptr;
    }

    return product->GetLongDescription();
}

const char * cCommerceUtil::GetItemId( unsigned a_Index ) const
{
    commerceAssert( m_CommerceData.HasValidData() );

    if ( !m_CommerceData.HasValidData() )
    {
        return nullptr;
    }

    return m_CommerceData.GetItemData( a_Index )->GetId();
}

const cCommerceItemData* cCommerceUtil::GetItemData( unsigned a_Index ) const
{
	commerceAssert( m_CommerceData.HasValidData() );

    if ( !m_CommerceData.HasValidData() )
    {
        return nullptr;
    }

	return m_CommerceData.GetItemData( a_Index );
}

const cCommerceItemData* cCommerceUtil::GetItemData( const char* itemName, cCommerceItemData::eItemType itemType /*= cCommerceItemData::ITEM_TYPE_NUM_TYPES */ ) const
{
    return m_CommerceData.GetItemData(itemName, itemType);
}

const cCommerceItemData* cCommerceUtil::GetItemDataByItemId(const char* itemId) const
{
	return m_CommerceData.GetItemDataByItemId(itemId);
}

void cCommerceUtil::SetIsInstalledCallback( isCommerceProductinstalledCallbackPtr a_CallbackFuncPtr )
{
    m_IsProductInstalledCBPtr = a_CallbackFuncPtr;
}

void* cCommerceUtil::CommerceAlloc( unsigned a_Size )
{
    void * pMem;
    sysMemUseMemoryBucket b(MEMBUCKET_DEFAULT);

    if ( m_pCommerceAllocator )
    {
        pMem = m_pCommerceAllocator->Allocate( a_Size, COMMERCE_ALLOCATOR_ALIGNMENT, MEMTYPE_GAME_VIRTUAL );
    }
    else
    {
        pMem = sysMemAllocator::GetCurrent().Allocate( a_Size, COMMERCE_ALLOCATOR_ALIGNMENT, MEMTYPE_GAME_VIRTUAL );
    }

    Assert( pMem );
    return pMem;
}

void cCommerceUtil::CommerceFree( void* a_AllocedPtr )
{
    sysMemUseMemoryBucket b(MEMBUCKET_DEFAULT);

    if ( m_pCommerceAllocator )
    {
        m_pCommerceAllocator->Free( a_AllocedPtr );
    }
    else
    {
        sysMemAllocator::GetCurrent().Free( a_AllocedPtr );
    }
}

void cCommerceUtil::Update()
{
    m_CommerceData.Update();

    if ( IsPlatformReady() && m_PendingPlatformDataRequest )
    {
        if ( DoPlatformDataRequest() )
        {
            m_PendingPlatformDataRequest = false;
        }
    }

    if ( IsInErrorState() && m_PendingPlatformDataRequest )
    {
        //Something went wrong in the initialisation of the util. Stop waiting to do a platform data request.
        m_PendingPlatformDataRequest = false;
    }

    if ( BASE_STATE_PENDING_DATA == m_BaseState )
    {
        if ( IsPlatformReady() && IsContentInfoPopulated() )
        {
            m_BaseState = BASE_STATE_POPULATED;

            MergePlatformAndRosData();

            PopulateCategories();
        }
    }
}

bool cCommerceUtil::IsContentInfoPopulated() const
{
    return m_CommerceData.HasValidData();
}

void cCommerceUtil::DoDataRequest( bool fetchROSData )
{
    if ( BASE_STATE_PENDING_DATA != m_BaseState )
    {
		commerceDebugf1("CommerceUtil::DoDataRequest");
		
		if (fetchROSData)
        {
            m_CommerceData.StartDataFetch();
        }

        //Set this so that we can check that proper platform init has occured first 
        //and retry if required.
        m_PendingPlatformDataRequest = true;

        //this action invalidates category data
        m_CategoryDataPopulated = false;

        m_BaseState = BASE_STATE_PENDING_DATA;
    }
}

void cCommerceUtil::PopulateCategories()
{
    //RossC: NOT FOR FUTURE REFAC: I think I should move the majority of this function in cCommerceData, since it is a RoS side data manipulation

    if ( !m_CommerceData.HasValidData() )
    {
        commerceErrorf("No valid data parsed into the cCommerceData class.");
        commerceAssert( false );
        return;
    }

	commerceDebugf1("CommerceUtil::PopulateCategories");

	//int categoryCount = m_CommerceData.GetSizeOfCategoryDataArray();

    int itemCount = m_CommerceData.GetSizeOfDataArray();

    //Run through the category data items and populate their product index arrays

    for ( int iCat = 0; iCat < itemCount; iCat++ )
    {
        if ( m_CommerceData.GetItemData( iCat )->GetItemType() != cCommerceItemData::ITEM_TYPE_CATEGORY )
        {
            //Not a category, ergo does not need to be populated.
            continue;
        }

        //Double check, guard against future stoopid code changes.
        commerceAssert( m_CommerceData.GetItemData( iCat )->GetItemType() == cCommerceItemData::ITEM_TYPE_CATEGORY );

        cCommerceCategoryData* pCategory = static_cast<cCommerceCategoryData*>(m_CommerceData.GetItemData( iCat ));
        pCategory->SetItemArraySize( m_CommerceData.GetSizeOfDataArray() );

        //Loop through all products 
        for ( int iProdData = 0; iProdData < m_CommerceData.GetSizeOfDataArray(); iProdData++ )
        {
            bool addToCategory = false;

            //Loop through all category memberships for this product
            const cCommerceItemData* product = m_CommerceData.GetItemData( iProdData );
            for ( int iCatMemberships = 0; iCatMemberships < product->GetCategoryMemberships().GetCount(); iCatMemberships++ )
            {
                if ( product->GetCategoryMemberships()[ iCatMemberships ] == pCategory->GetId() )
                {
                    addToCategory = true;
                }
                else
                {
                    continue;
                }

                if ( product->GetItemType() == cCommerceItemData::ITEM_TYPE_PRODUCT )
                {
                    const cCommerceProductData* productData = static_cast<const cCommerceProductData*>(product);

                    const bool hasProduct = productData->GetEnumeratedFlag() || PARAM_commerceIgnoreEnumeratedFlag.Get();
                    const bool isVirtualProduct = productData->IsVirtualProduct() || (productData->IsVirtualIfInstalled() && IsProductInstalled(productData)); 
                    const bool hasAnySubscriptionUpsell = m_CommerceData.HasSubscriptionUpsell(productData);

					// If we do not have an actual product attached, do not show unless:
                    //  - This is a virtual product (either IsVirtualProduct or IsVirtualIfInstalled *and* IsProductInstalled)
                    //  - This has a subscription upsell (allows upsell of the subscription)
					if (!hasProduct && !(isVirtualProduct || hasAnySubscriptionUpsell))
                    {
                        commerceDebugf1("CommerceUtil::PopulateCategories: NoProduct - Not adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
						addToCategory = false;
                    }

                    if ( productData->GetReleaseTime() > rlGetPosixTime() )
                    {
                        //This products release date has not been reached. Do not show.
						commerceDebugf1("CommerceUtil::PopulateCategories: Expired - Not adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
						addToCategory = false;
                    }
                }

                if ( !product->GetDependencyFlag() )
                {
                    //We don't have at least one of our dependencies. Do not add.
					commerceDebugf1("CommerceUtil::PopulateCategories: GetEnumDependencyFlag - Not adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
					addToCategory = false;
                }

                if ( !product->GetEnumDependencyFlag() )
                {
                    //We don't have at least one of our enumeration dependencies. Do not add.
					commerceDebugf1("CommerceUtil::PopulateCategories: GetEnumDependencyFlag - Not adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
					addToCategory = false;
                }

                if ( product->GetExclusionFlag() )
                {
                    //At least one of our exclusions was enumerated
					commerceDebugf1("CommerceUtil::PopulateCategories: GetExclusionFlag - Not adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
					addToCategory = false;
                }

                if ( product->GetEnumExclusionFlag() )
                {
                    //At least one of our enum exclusions was enumerated
					commerceDebugf1("CommerceUtil::PopulateCategories: GetEnumExclusionFlag - Not adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
					addToCategory = false;
                }

                OUTPUT_ONLY( if ( addToCategory ) )
                {
                    commerceDebugf1("CommerceUtil::PopulateCategories: Adding product %s [%s] to category[%d] %s %s", product->GetName().c_str(), product->GetId().c_str(), iCat, pCategory->GetId().c_str(), pCategory->GetName().c_str());
                }
            }

            if ( addToCategory )
            {
                //This product is a member of this category, append its index to the item list
				pCategory->AddItemIndex(iProdData);
            }
        }

        //Sort this category based on the currently set sorting tag
        SortCategory(pCategory);
    }

    m_CategoryDataPopulated = true;
}

int cCommerceUtil::PopulateArrayWithIdsWhichIncludeThisProduct( const cCommerceProductData * a_Product,atArray<atString> &a_IdArray, bool a_includeVirtuals )
{
    if ( a_Product == nullptr )
    {
        return 0;
    }

//     Pure virtual product, no go. Comment out to test without valid red box
//     if ( !a_Product->GetEnumeratedFlag() )
//     {
//         return 0;
//     }

    return PopulateIncludesArrayInternal( a_Product, a_IdArray, a_includeVirtuals );
}

int cCommerceUtil::GetNumContainingProducts( const cCommerceProductData * a_Product, bool a_includeVirtuals )
{
    atArray<atString> testArray;

    testArray.Reserve( CONTAINING_ITEM_MAX_TEST_ARRAY_SIZE );

    return PopulateArrayWithIdsWhichIncludeThisProduct( a_Product, testArray, a_includeVirtuals );
}

int cCommerceUtil::PopulateIncludesArrayInternal( const cCommerceProductData * a_Product, atArray<atString> &a_IdArray, bool a_includeVirtuals, int depth /*= 0 */ )
{
    if ( a_IdArray.GetCapacity() == a_IdArray.GetCount() )
    {
        //Provided array is full. Bail.
        return 0;
    }

    int retCount = 0;

    //Add this products ID if it is valid COMMENTED OUT FOR TESTING WITHOUT VALID REDBOX
    bool validForAdd = true;
    
    if ( !a_Product->GetEnumeratedFlag() )
    {
        if ( a_Product->IsVirtualProduct() && a_includeVirtuals )
        {
            validForAdd = true;
        }
        else if ( a_Product->IsVirtualIfInstalled() && IsProductInstalled( a_Product ) && a_includeVirtuals )
        {
            validForAdd = true;
        }
		else if (m_CommerceData.HasSubscriptionUpsell(a_Product))
		{
			validForAdd = true;
		}
        else
        {
            //We have no corresponding platform product and we are not marked virtual, so we are not valid
            validForAdd = false;
        }
    }

    if ( !a_Product->GetDependencyFlag() )
    {
        //We do not have at least one dependency
        validForAdd = false;
    }

    if ( !a_Product->GetEnumDependencyFlag() )
    {
        //We do not have at least one enumeration dependency
        validForAdd = false;
    }

    if ( a_Product->GetExclusionFlag() )
    {
        //We have at least one exclusion.
        validForAdd = false;
    }

    if ( a_Product->GetEnumExclusionFlag() )
    {
        //We have at least one enum exclusion
        validForAdd = false;
    }

    if ( validForAdd )
    {
        a_IdArray.Append() = a_Product->GetId();
        retCount++;
    }

    depth++;

    if (depth >= MAX_POPULATE_RECURSE_DEPTH)
    {
        return retCount;
    }

    for ( int i = 0; i < a_Product->GetIncludedInProductIds().GetCount(); i++ )
    {
        bool alreadyAdded = false;
        for ( int j = 0; j < a_IdArray.GetCount(); j++ )
        {
            if ( a_Product->GetIncludedInProductIds()[i] == a_IdArray[j] )
            {
                alreadyAdded = true;
            }
        }

        if (alreadyAdded)
        {
            //Already added, don't check and add again
            continue;
        }

        const cCommerceProductData *productData = static_cast<const cCommerceProductData*>( GetItemData( a_Product->GetIncludedInProductIds()[i], cCommerceItemData::ITEM_TYPE_PRODUCT ) );
        
        if (productData == nullptr)
        {
            continue;
        }

        retCount += PopulateIncludesArrayInternal( productData, a_IdArray, a_includeVirtuals, depth );
    }

    return retCount;
}

bool cCommerceUtil::IsProductInstalled( const cCommerceProductData* pProductData ) const
{
    commerceAssert( m_CommerceData.HasValidData() );

    if ( !m_CommerceData.HasValidData() )
    {
        return false;
    }

    if ( pProductData == nullptr )
    {
        return false;
    }

    //We can have things which are installed but arent enumerated. Thanks Voucher codes!!
    /*
    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }
    */

    isCommerceProductinstalledCallbackPtr installedCB = GetProductInstalledCB();

    if ( installedCB == nullptr )
    {
        commerceErrorf("No product installed CB set. Forcing return value to false.\n");
        return false;
    }
    else if ( installedCB( pProductData ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool cCommerceUtil::HasProductDetails(const cCommerceProductData* pProductData) const
{
    return pProductData != nullptr;
}

bool cCommerceUtil::ShouldAllowWithoutProduct(const cCommerceProductData* pProductData) const
{
    return HasMissingSubscriptionUpsells(pProductData) || PARAM_commerceIgnoreEnumeratedFlag.Get();
}

bool cCommerceUtil::ShouldHideProduct(const cCommerceProductData* pProductData) const
{
    return HasMissingSubscriptionDependencies(pProductData);
}

cCommerceUtil* cCommerceUtil::Instance()
{
    if ( m_BaseInstance == nullptr )
    {
        m_BaseInstance = rage_new cCommerceUtil;
    }

    return m_BaseInstance;
}

void cCommerceUtil::DoPlatformLevelDebugDump()
{
    commerceDebugf1("No platform data to print, using base level commerce class\n");
}

const int cCommerceUtil::GetNumItems() const
{
    if ( IsCategoryInfoPopulated() )
    {
        return m_CommerceData.GetSizeOfDataArray();
    }
    else
    {
        return 0;
    }
}

void cCommerceUtil::SortCategory( cCommerceCategoryData* pCategory )
{
    //As with populate category data, this is square in the crosshairs of the next refactor to move in commercedata
    //Check if we have a sorting tag defined. No need to sort if not.
    if ( m_SortingTag.GetLength() == 0 )
    {
        return;
    }

    atArray<int> &catItemArray = pCategory->GetItemIndexArray();

    //Sort here for now. If the catagory item array was to an array of pointers to items I could use atArray::QSort without running into issues
    
    //Dumb sort
    bool sortOccured = false;
    do 
    {
        sortOccured = false;
        for ( int i = 0; i < catItemArray.GetCount() - 1; i++ )
        {
            if ( GetItemData(catItemArray[i])->GetPriorityForTag(m_SortingTag.c_str()) < GetItemData(catItemArray[i+1])->GetPriorityForTag(m_SortingTag.c_str()) )
            {
                std::swap(catItemArray[i],catItemArray[i+1]);
                sortOccured = true;
            }
        }
    } while ( sortOccured );
}

void cCommerceUtil::SetSortingTag( const char* newTag )
{
    if ( newTag == nullptr )
    {
        m_SortingTag.Reset();
    }
    else
    {
        m_SortingTag = atString(newTag);
    }
}

cCommerceUtil* CommerceUtilInstance( bool forceBase )
{
    if ( forceBase )
    {
        return cCommerceUtil::Instance();
    }

#if __XENON
	return cXboxCommerceManager::Instance();
#elif __PS3
	return cPS3CommerceManager::Instance();
#elif RSG_ORBIS
	return cOrbisCommerceManager::Instance();
#elif RSG_DURANGO
	return cDurangoCommerceManager::Instance();
/*
//Sorry PC guys. I will enable this once the RoS Entitlement and rgsc code is brought across into the GTAV tree.
#elif __WIN32PC
#if __STEAM_BUILD
	return cSteamCommerceManager::Instance();
#else
	return cPCCommerceManager::Instance();
#endif
*/
#else
    return cCommerceUtil::Instance();
#endif
}

} //namespace rage
