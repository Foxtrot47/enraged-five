#if RSG_ORBIS

#include "OrbisCommerceUtil.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "data/rson.h"
#include "rline/rlnp.h"
#include "rline/rltitleid.h"
#include "rline/rlpresence.h"
#include "string/string.h"
#include "system/ipc.h"

#include "../CommerceChannel.h"

//OPTIMISATIONS_OFF()

//These defines don't seem to be used at 
#define SCE_NP_COMMERCE2_SKU_ANN_PURCHASED_CANNOT_PURCHASE_AGAIN  0x80000000
#define SCE_NP_COMMERCE2_SKU_ANN_PURCHASED_CAN_PURCHASE_AGAIN     0x40000000

namespace rage
{

cOrbisCommerceManager* cOrbisCommerceManager::m_Instance = NULL;

const int DATA_FETCH_THREAD_STACK_SIZE = ( 16 * 1024 );
 
//const int INVALID_REQUEST_INDEX = 0xDEADBEEF;
const int CATALOG_REQUEST_ID = -1;

const int SONY_LINEBREAK_CODE_LEN = 4;

//JSON Constants
const int COMMERCE_ITEM_CONTAINER_TYPE_LENGTH = 16;

cOrbisCommerceManager::cOrbisCommerceManager() 
    :    m_ContentResultBufferType( CONTENT_RESULT_CODE_NONE ),
    
    m_CurrentMode( MODE_TOP ),
    m_ContentInfoPopulated( false ),
    m_RequestId( 0 ),
    mp_CommerceResultAllocation( NULL ),
    m_CategoryContentResultInitialised( false ),
    m_FetchingData( false ),
    m_IsInitialised( false ),
    m_IsContextCreated( false ),
    m_pCurrentRequestOffer( NULL ),
    m_pLastInfoRequestOffer( NULL ),
    m_NeedToRequeryData( false ),
    m_NumMetaDataRecords( 0 ),
    m_DataOperationAborted( false )
{
    if ( m_Instance == NULL )
    {
        m_Instance = this;
    }

	m_MetaDataRecords.Reserve(MAX_NUM_META_DATA_RECORDS);
}   

//----------------------------------------------------------
cOrbisCommerceManager::~cOrbisCommerceManager()
{
   // delete mp_MemPool;
   // mp_MemPool = NULL;
    
    Shutdown();

    m_Instance = NULL;

	if ( m_CatalogFetchStatus.Pending() )
	{
		netTask::Cancel(&m_CatalogFetchStatus);
	}

	if ( m_ProductInfoFetchStatus.Pending() )
	{
		netTask::Cancel(&m_ProductInfoFetchStatus);
	}

	commerceDisplayf("Destroying OrbisCommerceManager object.");
}

//----------------------------------------------------------
bool cOrbisCommerceManager::Init( int userIndex, const char* skuFolder )
{   
    //Make sure to check m_IsInitialised

    if ( m_IsInitialised )
    {
        commerceErrorf("Trying to initialise the commerce manager when it has already been intialised.");
        commerceAssert( false );
        return false;
    }

    m_IsInitialised = true;

    m_DataOperationAborted = false;

    m_LineBreakSubsString.Reset();
	m_LineBreakSubsString = "\n";

    cCommerceUtil::Init( userIndex, skuFolder );
    
	//We go straight to neutral on orbis since we are lacking the concept of sessions.
	m_CurrentMode = MODE_NEUTRAL;
    
	m_CatalogFetchStatus.Reset();
	m_ProductInfoFetchStatus.Reset();

    return true;
}

//----------------------------------------------------------
void cOrbisCommerceManager::Shutdown()
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to shutdown commerce manager without initialising." );
        commerceAssert( false );
        return;
    }

    Assert( !m_FetchingData );

    

    //Run one more update cycle to ensure any pending shutdowns are completed
    Update();

    //Reinitialise member variables back to initial states.
    m_ContentResultBufferType = CONTENT_RESULT_CODE_NONE;
    m_pCurrentRequestOffer = NULL;
    m_CurrentMode = MODE_TOP;
    m_ContentInfoPopulated = false;
    m_RequestId = 0;  
    m_CategoryContentResultInitialised = false;
    m_FetchingData = false;
    m_IsInitialised = false;
    m_IsContextCreated = false;
    m_pLastInfoRequestOffer = NULL;
    m_NeedToRequeryData = false;
    m_NumMetaDataRecords = 0;
    m_DataOperationAborted = false;
    m_LineBreakSubsString.Reset();

    cCommerceUtil::Shutdown();
}

//----------------------------------------------------------
void cOrbisCommerceManager::Commerce2Handler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg)
{
    
}
 
bool cOrbisCommerceManager::DoPlatformDataRequest()
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call DoCommerceCategoryContentRequest without initialising commerce manager." );
        Assert( false );
        return false;
    }

    if ( IsInErrorState() )
    {
        return false;
    }

	DoWebApiCatalogRequest();

	m_CurrentMode = MODE_FETCH_CATEGORY_INFO;



    return true;
}

//----------------------------------------------------------
void cOrbisCommerceManager::DoCommerceProductInfoRequest( OfferMetaData* offerData )
{
    Assert( IsContentInfoPopulated() );

    

    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call DoCommerceProductInfoRequest without initialising commerce manager." );
        Assert( false );
        return;
    }

    if ( !IsContentInfoPopulated() )
    {
        return;
    }

    if ( m_CurrentMode != MODE_NEUTRAL )
    {
        return;
    }

    if ( offerData->isVirtualProduct )
    {
        return;
    }

    if ( IsInErrorState() )
    {
        return;
    }



	commerceAssert(false);

    Assert( m_FetchingData == false );

	
	
	m_FetchingData = true;
    m_pCurrentRequestOffer = offerData;
    int requestedProductIndex = -1;

    for ( int i=0; i < m_NumMetaDataRecords; i++ )
    {
        if ( offerData == &m_MetaDataRecords[ i ] )
        {
            requestedProductIndex = i;
            break;
        }
    }

    commerceAssert( requestedProductIndex != -1 );

	m_ContentResultBufferType = CONTENT_RESULT_CODE_PRODUCT;
    m_CurrentMode = MODE_FETCH_PRODUCT_INFO;
}
 
//----------------------------------------------------------
void cOrbisCommerceManager::Update()
{
    cCommerceUtil::Update();

    if ( !m_IsInitialised )
    {
        return;
    }

    

    switch (m_CurrentMode)
    {
		case(MODE_CREATE_SESSION_FINISHED_WITH_ERROR):
			cOrbisCommerceManager::Instance()->m_CurrentMode = MODE_ERROR_ENCOUNTERED;
			break;
		case( MODE_CHECKOUT ):
			UpdateCheckout();
			break;
        case( MODE_CHECKOUT_DONE ):       
            cOrbisCommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            break;
		case( MODE_DOWNLOAD_LIST ):
			UpdateDownloadList();
			break;
        case( MODE_DOWNLOAD_LIST_DONE ):
            cOrbisCommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            break;
		case( MODE_PROMO_CODE ):
			UpdatePromoCode();
			break;
        case( MODE_PROMO_CODE_DONE ):
            cOrbisCommerceManager::Instance()->m_CurrentMode = MODE_NEUTRAL;
            break;    
        case( MODE_FETCH_CATEGORY_INFO ):
            UpdatePopulateContentInfo();
            break;
		case( MODE_FETCH_ALL_PRODUCT_INFO ):
			UpdateFetchAllProductInfo();
			break;
        case( MODE_FETCH_PRODUCT_INFO ):
            UpdatePopulateProductInfo();
            break;
        case( MODE_NEUTRAL ):
            UpdateModeNeutral();
            break;
		default:
			break;
    }
}

//----------------------------------------------------------
const char * cOrbisCommerceManager::GetItemLongDescription( const cCommerceProductData* product )
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call GetProductLongDescription without initialising commerce manager." );
        Assert( false );
        return NULL;
    }

    if( !IsPlatformReady() )
    {
        return( NULL );
    }

    if ( !product->GetEnumeratedFlag() )
    {
        return product->GetLongDescription();  
    }
    else
    {
        const OfferMetaData* offerData = GetOfferInfo( product ); 
        if ( offerData == NULL )
        {
            //This is an enumerated product, but no product is found.
            //Something has gone seriously wacky.
            commerceAssert( false );
            return NULL;
        }

        if ( offerData->longProductDesc[0] != 0 )
        {
            //Allready populated, send it back.
            return offerData->longProductDesc;
        }
        else
        {
            //Are we already fetching other data?
            if ( !IsFetchingData() )
            {
                //Put in our own request for data 
                DoCommerceProductInfoRequest( const_cast<OfferMetaData*>(offerData) );
            }
        }
    }
    return NULL;
}

//----------------------------------------------------------
void cOrbisCommerceManager::CheckoutProduct( unsigned a_Index )
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call CheckoutProduct without initialising commerce manager." );
        Assert( false );
        return;
    }
    
    if ( m_CurrentMode != MODE_NEUTRAL )
    {
        return;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return;
    }

    return CheckoutProduct( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}


void cOrbisCommerceManager::CheckoutProduct( const cCommerceProductData* a_pProductdata )
{
    if ( !a_pProductdata->GetEnumeratedFlag() )
    {
        return;
    }

    if ( GetOfferInfo( a_pProductdata ) == NULL )
    {
        return;
    }
    
    const OfferMetaData *pOfferData = GetOfferInfo( a_pProductdata );

    if ( !pOfferData->hasValidSku )
    {
        return;
    }

    bool refetch = IsProductPurchased( a_pProductdata ) && !a_pProductdata->IsConsumable();

	const char *skuIdArray[1];
	atString fullLabelString;
	fullLabelString += pOfferData->productIdentifier;
	fullLabelString += "-";
	fullLabelString += pOfferData->skuProductLabel;

	skuIdArray[0] = fullLabelString;

	const int localGamerIndex = rlPresence::GetActingUserIndex();
	int userId = g_rlNp.GetUserServiceId(localGamerIndex);
    if ( refetch )
    {
		//atString serviceId(rlNp::GetTitleId().GetServiceId());	
		g_rlNp.GetCommonDialog().ShowCommerceDownloadDialog(userId,1,skuIdArray);
        m_CurrentMode = MODE_DOWNLOAD_LIST;
    }
    else
    {
		g_rlNp.GetCommonDialog().ShowCommerceCheckoutDialog(userId,1,skuIdArray);
        m_CurrentMode = MODE_CHECKOUT;
    }
}

//----------------------------------------------------------
void cOrbisCommerceManager::UpdatePopulateContentInfo()
{
    if (m_CatalogFetchStatus.Pending())
	{
		return;
	}
    
	if ( m_CatalogFetchStatus.Failed() )
	{
		commerceErrorf("Commerce webapi call for catalog data fetch failed.");
		m_CurrentMode = MODE_ERROR_ENCOUNTERED;
		m_CatalogFetchStatus.Reset();
		return;
	}
	
	if ( m_CatalogFetchStatus.Canceled() )
	{
		m_CurrentMode = MODE_NEUTRAL;
		m_CatalogFetchStatus.Reset();
		return;
	}

	ParseCatalogData();
	m_CategoryRequestDetail.GetBuffer().Clear();

	DoWebApiAllProductRequest();


	m_CatalogFetchStatus.Reset();
}


void cOrbisCommerceManager::UpdateFetchAllProductInfo()
{
	if ( m_ProductInfoFetchStatus.Pending() )
	{
		return;
	}

	if ( m_ProductInfoFetchStatus.Failed() )
	{
		commerceErrorf("Commerce webapi call for UpdateFetchAllProductInfo failed.");
		m_CurrentMode = MODE_ERROR_ENCOUNTERED;
		m_ProductInfoFetchStatus.Reset();
		return;
	}

	if ( m_ProductInfoFetchStatus.Canceled() )
    {
        m_ProductInfoFetchStatus.Reset();
        m_ProductRequestDetail.Reset();
        m_FetchingData = false;
        m_CurrentMode = MODE_NEUTRAL;
        return;
    }
	
	ParseProductData();
	m_ProductRequestDetail.Reset();
	m_ProductInfoFetchStatus.Reset();
	m_ContentInfoPopulated = true;
	m_FetchingData = false;
	m_CurrentMode = MODE_NEUTRAL;
}
//----------------------------------------------------------
void cOrbisCommerceManager::UpdatePopulateProductInfo()
{
    if (IsFetchingData())
    {
        //Product info fetch has not completed.
        return;
    }

	commerceAssert(false);
}

//----------------------------------------------------------
bool cOrbisCommerceManager::IsProductPurchased( int a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductPurchased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cOrbisCommerceManager::IsProductPurchased( const cCommerceProductData* pProductData) const
{
    if ( pProductData == NULL )
    {
        return false;
    }

    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }

    const OfferMetaData *pOfferInfo = GetOfferInfo( pProductData );

    if ( pOfferInfo == NULL )
    {
        return false;
    }

    return pOfferInfo->hasPurchased;  
}

//----------------------------------------------------------
bool cOrbisCommerceManager::IsProductReleased( int a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductReleased( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cOrbisCommerceManager::IsProductReleased( const cCommerceProductData* pProductData ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    if ( pProductData == NULL )
    {
        return false;
    }

    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }

    const OfferMetaData *pOfferInfo = GetOfferInfo( pProductData );

    if ( pOfferInfo == NULL )
    {
        return false;
    }

    
	commerceAssertf(false,"Bring across orbis time funcs");
    //RLPOSIXTIME
/*    
	u64 currentTick;

	int result = sceNpManagerGetNetworkTime(&currentTick);
    if ( result != CELL_OK )
    {
        commerceErrorf( "cellRtcGetCurrentTick failed with error code 0x%x\n",result );
        return false;
    }

    CellRtcDateTime currentDateTime;
    result = cellRtcSetTick( &currentDateTime, &currentTick );
    if ( result < 0 )
    {
        commerceErrorf( "cellRtcSetTick failed with error code 0x%x\n",result );
        Assert( false );
        return false;
    }
	*/
    //To keep Sony happy, we dont mess with their release date data.
    return true;
}   

//----------------------------------------------------------
bool cOrbisCommerceManager::IsProductPurchasable( int a_Index ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchased without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return false;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return false;
    }

    return IsProductPurchasable( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ) );
}

bool cOrbisCommerceManager::IsProductPurchasable( const cCommerceProductData* pProductData ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
        Assert( false );
        return false;
    }

    Assert( IsContentInfoPopulated() );
    if ( !IsContentInfoPopulated() )
    {
        return false;
    }
    if ( !pProductData->GetEnumeratedFlag() )
    {
        return false;
    }

    const OfferMetaData *pOfferInfo = GetOfferInfo( pProductData );

    if ( pOfferInfo == NULL )
    {
        return false;
    }

    return pOfferInfo->isPurchasable && pOfferInfo->hasValidSku;
}

//----------------------------------------------------------
bool cOrbisCommerceManager::ProductCodeEntry()
{
    if ( m_CurrentMode != MODE_NEUTRAL )    
    {
        return false;
    }

    //TODO: Add a state which represents "session created but category info not fetched".
    if ( !IsPlatformContentInfoPopulated() )
    {
        //If the content info is not populate we are fetching it, and going into code entry will blow our state machine all to hell.
        return false;
    }

	const int localGamerIndex = rlPresence::GetActingUserIndex();
	int userId = g_rlNp.GetUserServiceId(localGamerIndex);
	g_rlNp.GetCommonDialog().ShowCommerceCodeEntryDialog(userId);
    
    m_CurrentMode = MODE_PROMO_CODE;
	return true;
 
}

float cOrbisCommerceManager::GetProductPrice( u32 a_Index, char* a_pOutputString, u32 a_SizeOfOutputString ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call IsProductPurchasable without initialising commerce manager." );
        Assert( false );
        return -1.0f;
    }

    Assert( IsContentInfoPopulated() );

    Assert( static_cast<int>( a_Index ) < m_CommerceData.GetSizeOfDataArray() );
    if ( static_cast<int>( a_Index ) >= m_CommerceData.GetSizeOfDataArray() )
    {    
        return -1.0f;
    }

    if ( m_CommerceData.GetItemData( a_Index )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
    {
        return -1.0f;
    }

    return GetProductPrice( static_cast<const cCommerceProductData*>( m_CommerceData.GetItemData( a_Index ) ), a_pOutputString, a_SizeOfOutputString );
}

float cOrbisCommerceManager::GetProductPrice( const cCommerceProductData* a_pProductData, char* a_pOutputString, unsigned a_SizeOfOutputString ) const
{
    if ( !m_IsInitialised )
    {
        commerceErrorf( "Trying to call GetProductPrice without initialising commerce manager." );
        Assert( false );
        return -1.0f;
    }

    if ( a_pProductData == NULL )
    {
        return -1.0f;
    }
        
    if ( !a_pProductData->GetEnumeratedFlag() )
    {
        return -1.0f;
    }

    unsigned int price = 0;
   
    const OfferMetaData *pOfferData = GetOfferInfo( a_pProductData );

    if ( pOfferData == NULL )
    {
        return -1.0f;
    }

	price = pOfferData->price;

    if ( pOfferData->hasValidSku )
    {
        
        if ( a_pOutputString != NULL )
        {
			strncpy( a_pOutputString, pOfferData->displayPrice, a_SizeOfOutputString);
        }
    }
    else
    {
        a_pOutputString = NULL;
    }

    return price;
    
}

bool cOrbisCommerceManager::UpdateModeNeutral()
{
    if ( m_NeedToRequeryData )
    {
        //We have a query outstanding from a session timeout, handle it.
        m_NeedToRequeryData = false;
        
        if ( m_pLastInfoRequestOffer == NULL )
        {
            DoPlatformDataRequest();
        }
        else
        {
            DoCommerceProductInfoRequest( m_pLastInfoRequestOffer );
        }
       
        m_pLastInfoRequestOffer = NULL;
    }

    return true;
}

void cOrbisCommerceManager::DoLanguageSubstitutionPass()
{
    //for ( int iMetaData = 0; iMetaData < m_NumMetaDataRecords; iMetaData++ )
    //{
    //    for ( int iLangSubLines = 0; iLangSubLines < m_NumLanguageSubs; iLangSubLines++ )
    //    {
    //        if ( strcmp( m_MetaDataRecords[ iMetaData ].langSubCode, m_MetaDataLanguageLines[ iLangSubLines ].m_LangSubCode ) == 0 )
    //        {
    //            //We have a match. Copy the language sub lines into the DLC metadata.
    //            safecpy( m_MetaDataRecords[ iMetaData ].productName, m_MetaDataLanguageLines[ iLangSubLines ].m_LangSubName, META_DATA_NAME_LEN );
    //            safecpy( m_MetaDataRecords[ iMetaData ].longProductDesc, m_MetaDataLanguageLines[ iLangSubLines ].m_LangSubLongDesc, META_DATA_LONG_DESC_LEN );
    //        }
    //    }
    //} 
}

void cOrbisCommerceManager::AbortDataFetch()
{
    cCommerceUtil::AbortDataFetch();

    if ( !m_IsInitialised ) 
    {
        commerceErrorf( "Trying to call AbortDataFetch without initialising commerce manager." );
        Assert( false );
        return;
    }


    commerceDebugf3("Aborting commerce data fetch.\n");

    if ( m_CatalogFetchStatus.Pending() )
    {
        netTask::Cancel(&m_CatalogFetchStatus);
    }

    if ( m_ProductInfoFetchStatus.Pending() )
    {
        netTask::Cancel(&m_ProductInfoFetchStatus);
    }

    m_FetchingData = false;
}

void cOrbisCommerceManager::ForceErrorState()
{
    m_CurrentMode = MODE_ERROR_ENCOUNTERED;
}

int cOrbisCommerceManager::FindExistingRecord( OfferMetaData &a_RecordToMatch )
{
    int result = -1;
    for ( int i = 0; i < m_NumMetaDataRecords; i++ )
    {
        if ( strcmp( m_MetaDataRecords[ i ].productIdentifier, a_RecordToMatch.productIdentifier ) == 0 && ( m_MetaDataRecords[ i ].isVirtualProduct == a_RecordToMatch.isVirtualProduct ) )
        {
            //We have a match.
            result = i;
        }
    }

    return result;
}

cOrbisCommerceManager* cOrbisCommerceManager::Instance()
{
	if ( m_Instance == NULL )
	{
		m_Instance = rage_new cOrbisCommerceManager;
	}

	return m_Instance;
}

const cOrbisCommerceManager::OfferMetaData *cOrbisCommerceManager::GetOfferInfo( const cCommerceProductData* pProductInfo ) const
{
    //Check we have valid commerce data.
    Assert( cCommerceUtil::IsContentInfoPopulated() );
    if ( !cCommerceUtil::IsContentInfoPopulated() )
    {
        return NULL;
    }

    commerceAssert( pProductInfo != NULL );
    if ( pProductInfo == NULL )
    {
        return NULL;
    }

    if ( !pProductInfo->GetEnumeratedFlag() )
    {
        //Didnt tie to an actual product, return NULL
        return NULL;
    }

    int foundIndex = -1;

    for ( int i = 0; i < m_NumMetaDataRecords; i++ )
    {
        if ( pProductInfo->GetPlatformId() == m_MetaDataRecords[i].productIdentifier )
        {
            //Found the correct offer.
            foundIndex = i;
            break;
        }
    }

    if ( foundIndex == -1 )
    {
        return NULL;
    }
    else
    {
        return &m_MetaDataRecords[foundIndex];
    } 
}

void cOrbisCommerceManager::MergePlatformAndRosData()
{
    for ( int i=0; i< m_NumMetaDataRecords; i++)
    {
        UpdateCommerceDataWithEnumeratedContent( m_MetaDataRecords[i].productIdentifier, m_MetaDataRecords[i].hasPurchased );
    }

    for ( int s=0; s <  m_CommerceData.GetSizeOfDataArray(); s++ )
    {
        cCommerceItemData* itemData = m_CommerceData.GetItemData( s );
       
        //By default the dependencys are set to having been found
        itemData->SetDependencyFlag( true );

        for ( int iDeps = 0; iDeps < itemData->GetDependencyIds().GetCount(); iDeps++ )
        {
            //There is a dependency, set the flag to false
            itemData->SetDependencyFlag( false );
            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {

                if ( ( strcmp(m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetDependencyIds()[ iDeps ].c_str()) == 0 )
                    && m_MetaDataRecords[ iOffers ].hasPurchased )
                {
                    //Found this dependency, set the flag and get out.
                    itemData->SetDependencyFlag( true );
                    break;
                }
            }
        }

        //By default enumeration dependencies
        itemData->SetEnumDependencyFlag( true );

        for ( int iEnumDeps = 0; iEnumDeps < itemData->GetEnumDependencyIds().GetCount(); iEnumDeps++ )
        {
            //There is a dependency, set the flag to false
            itemData->SetEnumDependencyFlag( false );

            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {
                if ( strcmp( m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetEnumDependencyIds()[ iEnumDeps ] ) == 0)
                {
                    //Found this enumeration dependency, set the flag and get out.
                    itemData->SetEnumDependencyFlag( true );
                    break;
                }
            }
        }

        //By default the exclusions flag is set to false (no exclusions found)
        itemData->SetExclusionFlag( false );
        for ( int iExcs = 0; iExcs < itemData->GetExclusionIds().GetCount(); iExcs++ )
        {
            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {
                if ( ( strcmp(m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetExclusionIds()[ iExcs ].c_str()) == 0 )
                    && m_MetaDataRecords[ iOffers ].hasPurchased )
                {
                    //Found this exclusion, set the flag and get out.
                    itemData->SetExclusionFlag( true );
                    break;
                }
            }
        }

        //By default the enumerated exclusions flag is set to false (no exclusions found)
        itemData->SetEnumExclusionFlag( false );
        for ( int iEnumExcs = 0; iEnumExcs < itemData->GetEnumExclusionIds().GetCount(); iEnumExcs++ )
        {
            for ( int iOffers = 0; iOffers < m_NumMetaDataRecords; iOffers++ )
            {
                if ( strcmp( m_MetaDataRecords[ iOffers ].productIdentifier, itemData->GetEnumExclusionIds()[ iEnumExcs ] ) == 0)           
                {
                    //Found this exclusion, set the flag and get out.
                    itemData->SetEnumExclusionFlag( true );
                    break;
                }
            }
        }
    }
}

void cOrbisCommerceManager::UpdateCommerceDataWithEnumeratedContent( const char* a_ProductIdentifier, bool /*a_IsOwned*/ )
{
    for ( int i = 0; i < m_CommerceData.GetSizeOfDataArray(); i++ )
    {
        if ( m_CommerceData.GetItemData( i )->GetItemType() != cCommerceItemData::ITEM_TYPE_PRODUCT )
        {
            //Not a product, ergo does not need to be checked.
            continue;
        }
        
        cCommerceProductData* productData = static_cast<cCommerceProductData*>(m_CommerceData.GetItemData( i ));
        if ( productData->GetPlatformId() == a_ProductIdentifier )
        {
            productData->SetEnumeratedFlag( true );
        }
    }
}

bool cOrbisCommerceManager::IsContentInfoPopulated() const
{
    return ( m_ContentInfoPopulated && cCommerceUtil::IsContentInfoPopulated() );
}

bool cOrbisCommerceManager::IsInErrorState() const
{
    bool retval = ( m_CurrentMode == MODE_ERROR_ENCOUNTERED );
    retval = retval || cCommerceUtil::IsInErrorState();
    return retval;
}

void cOrbisCommerceManager::SetLineBreakString( const atString& newSubsString )
{
    if ( newSubsString.GetLength() >  SONY_LINEBREAK_CODE_LEN )
    {
        commerceErrorf("Not setting the subs code in SetLineBreakString. Cannot be longer than %d chars.", SONY_LINEBREAK_CODE_LEN );
        return;
    }

    m_LineBreakSubsString = newSubsString;
}

void cOrbisCommerceManager::FixHTMLLineBreaks( OfferMetaData *productData )
{
    if ( m_LineBreakSubsString.GetLength() == 0 )
    {
        return;
    }

    commerceAssert( productData );
    atString tmp = atString( productData->longProductDesc );
    
    tmp.Replace( "<BR>", m_LineBreakSubsString.c_str() );
    tmp.Replace( "<br>", m_LineBreakSubsString.c_str() );
    
    //If anyone uses this tag they should be fired out of a cannon into the sun.
    tmp.Replace( "<bR>", m_LineBreakSubsString.c_str() );

    tmp.Replace( "<Br>", m_LineBreakSubsString.c_str() );

    safecpy( productData->longProductDesc, tmp.c_str() , META_DATA_LONG_DESC_LEN );
}

void cOrbisCommerceManager::RemoveHTMLTags( OfferMetaData *productData )
{
    commerceAssert( productData );

    atString tmp = atString(productData->longProductDesc);

    int startIndex = -1;
    int endIndex = -1;

    do 
    {
        startIndex = -1;
        endIndex = -1;

        startIndex = tmp.IndexOf( '<' );
        endIndex = tmp.IndexOf( '>' );

        while ( endIndex < startIndex && endIndex != -1 )
        {
            endIndex = tmp.IndexOf( '>', endIndex + 1 );            
        }

        if ( startIndex != -1 && endIndex != -1 )
        {
            atString tag;
            tag.Set( tmp, startIndex, endIndex - startIndex + 1 );
            tmp.Replace( tag.c_str() , "" );
        }
    } while ( startIndex != -1 && endIndex != -1 );

    safecpy( productData->longProductDesc, tmp.c_str() , META_DATA_LONG_DESC_LEN );
}

void cOrbisCommerceManager::DoPlatformLevelDebugDump()
{
    if (!m_IsInitialised)
    {
        commerceDebugf1("Attempted PS3 commerce platform data dump. Platform commerce not initialised.\n");
        return;
    }

    commerceDebugf1("PS3 commerce platform data dump.\n");
    commerceDebugf1("Content info populated: %d\n", m_ContentInfoPopulated);
    commerceDebugf1("Number of offer meta data records: %d\n", m_NumMetaDataRecords);
}

void cOrbisCommerceManager::DoWebApiCatalogRequest()
{
	commerceAssert(m_CatalogFetchStatus.None());

	if (!m_CatalogFetchStatus.None())
	{
		return;
	}

	int userIndex = rlPresence::GetActingUserIndex();

	m_CategoryRequestDetail.GetBuffer().Clear();
	g_rlNp.GetWebAPI().GetCatalog(userIndex, &m_CategoryRequestDetail, &m_CatalogFetchStatus);
}

bool cOrbisCommerceManager::ParseCatalogData()
{
	m_NumMetaDataRecords = 0;
	m_MetaDataRecords.ResetCount();

	commerceDebugf1("DataBuffer: %s",static_cast<const char*>(m_CategoryRequestDetail.GetBuffer().GetBuffer()));
	RsonReader rr(static_cast<const char*>(m_CategoryRequestDetail.GetBuffer().GetBuffer()), m_CategoryRequestDetail.GetBuffer().Length());
	rr.GetFirstMember(&rr);

	if (!rr.HasMember("total_results"))
	{
		commerceErrorf("Catalog results do not contain required total_results member.");
		return false;
	}

	
	RsonReader rrTotal;
	int numItems = rr.GetMember("total_results", &rrTotal);
	rrTotal.AsInt(numItems);

	RsonReader rrLinks;

	rr.GetMember("links", &rrLinks);
	
	RsonReader rrItem;
	rrLinks.GetFirstMember(&rrItem);

	bool done = false;
	for (s32 i=0; i < numItems && !done; i++, done = !rrItem.GetNextSibling(&rrItem))
	{
		RsonReader itemType;
		char containerType[COMMERCE_ITEM_CONTAINER_TYPE_LENGTH];
		bool foundContainerType = rrItem.ReadString("container_type", containerType, COMMERCE_ITEM_CONTAINER_TYPE_LENGTH );
		if (!foundContainerType || strcmp(containerType,"product") != 0)
		{
			continue;
		}

		char productId[META_DATA_PRODUCT_ID_LEN];
		bool foundProductId = rrItem.ReadString("label", productId, META_DATA_PRODUCT_ID_LEN );
		if (!foundProductId)
		{
			commerceErrorf("Malformed JSON from Sony commerce, missing label tag in product container");
			continue;
		}

		char productName[META_DATA_NAME_LEN];
		bool foundProductName = rrItem.ReadString("name", productName, META_DATA_NAME_LEN );
		if (!foundProductName)
		{
			commerceErrorf("Malformed JSON from Sony commerce, missing name tag in product container");
			continue;
		}

		OfferMetaData newRecord;
		strncpy(newRecord.productIdentifier,productId, META_DATA_SHORT_DESC_LEN);
		strncpy(newRecord.productName,productName, META_DATA_NAME_LEN);
		newRecord.longProductDesc[0] = 0;

		if ( m_MetaDataRecords.GetCount() == m_MetaDataRecords.GetCapacity() )
		{
			commerceDisplayf("Expanding the size of the metadata array from %d to %d.", m_MetaDataRecords.GetCount(), m_MetaDataRecords.GetCount() + MAX_NUM_META_DATA_RECORDS);
			m_MetaDataRecords.Grow() = newRecord;
		}
		else
		{
			m_MetaDataRecords.Push(newRecord);
		}

		//I should pull m_NumMetaDataRecords out and use the array count function now that this has been moved to atArray.
		m_NumMetaDataRecords++;
	}

	//We are done with the detail data
	m_CategoryRequestDetail.GetBuffer().Clear();

	return true;
}

void cOrbisCommerceManager::UpdatePromoCode()
{
	if (g_rlNp.GetCommonDialog().GetStatus() ==  SCE_COMMON_DIALOG_STATUS_FINISHED)
	{
		m_CurrentMode = MODE_PROMO_CODE_DONE;

		const SceNpCommerceDialogResult* dialogResult = g_rlNp.GetCommonDialog().GetCommerceResult(); 
		if (  dialogResult->result == SCE_COMMON_DIALOG_RESULT_USER_CANCELED )
		{
			m_DidLastCheckoutSucceed = false;
		}
		else
		{
			m_DidLastCheckoutSucceed = true;
		}

	}
}

void cOrbisCommerceManager::UpdateCheckout()
{
	if (g_rlNp.GetCommonDialog().GetStatus() ==  SCE_COMMON_DIALOG_STATUS_FINISHED)
	{
		m_CurrentMode = MODE_CHECKOUT_DONE;

		const SceNpCommerceDialogResult* dialogResult = g_rlNp.GetCommonDialog().GetCommerceResult(); 
		if ( dialogResult->result == SCE_COMMON_DIALOG_RESULT_USER_CANCELED )
		{
			m_DidLastCheckoutSucceed = false;
		}
		else
		{
			m_DidLastCheckoutSucceed = true;
		}
	}
}

void cOrbisCommerceManager::UpdateDownloadList()
{
	if (g_rlNp.GetCommonDialog().GetStatus() ==  SCE_COMMON_DIALOG_STATUS_FINISHED)
	{
		m_CurrentMode = MODE_DOWNLOAD_LIST_DONE;
	}
}

void cOrbisCommerceManager::DoWebApiAllProductRequest()
{
	if ( m_NumMetaDataRecords == 0 )
	{
		m_ContentInfoPopulated = true;
		m_FetchingData = false;
		m_CurrentMode = MODE_NEUTRAL;
		return;
	}

	m_ProductRequestDetail.Reset();

	for (int i=0; i < m_NumMetaDataRecords; i++)
	{
		m_ProductRequestDetail.AddRequestedProduct(m_MetaDataRecords[i].productIdentifier);
	}

	int userIndex = rlPresence::GetActingUserIndex();
	g_rlNp.GetWebAPI().GetProductInfo(userIndex, &m_ProductRequestDetail, &m_ProductInfoFetchStatus);

	m_CurrentMode = MODE_FETCH_ALL_PRODUCT_INFO;
}

bool cOrbisCommerceManager::ParseProductData()
{
	commerceDebugf1("DataBuffer: %s",static_cast<const char*>(m_ProductRequestDetail.GetBuffer().GetBuffer()));
	RsonReader rr(static_cast<const char*>(m_ProductRequestDetail.GetBuffer().GetBuffer()), m_ProductRequestDetail.GetBuffer().Length());
	RsonReader rrProduct;
	rr.GetFirstMember(&rrProduct);


	bool done = false;
	while ( !done || m_ProductRequestDetail.GetBuffer().Length() == 0 )
	{
		RsonReader itemType;
		char containerType[COMMERCE_ITEM_CONTAINER_TYPE_LENGTH];
		bool foundContainerType = rrProduct.ReadString("container_type", containerType, COMMERCE_ITEM_CONTAINER_TYPE_LENGTH );
		if (!foundContainerType || strcmp(containerType,"product") != 0)
		{
			//Needs to advance the reader here so we don't get stuck in the while loop in this case.
			done = !rrProduct.GetNextSibling(&rrProduct);
			continue;
		}

		RsonReader rrLabel;
		char label[META_DATA_PRODUCT_ID_LEN];
		label[0] = 0;
		bool foundLabel = rrProduct.ReadString("label", label, META_DATA_PRODUCT_ID_LEN );

		if (!foundLabel)
		{
			commerceErrorf("JSON product without label.");
            done = !rrProduct.GetNextSibling(&rrProduct);
			continue;
		}

		OfferMetaData *offerMetaData = FindOfferMetadataFromID( label );

		if ( offerMetaData == NULL )
		{
			commerceErrorf("ID %s not found in existing data from catalog fetch.", label);
            done = !rrProduct.GetNextSibling(&rrProduct);
			continue;
		}

		RsonReader rrLongDesc;
		char longDesc[META_DATA_LONG_DESC_LEN];
		longDesc[0] = 0;
		bool foundLongDesc = rrProduct.ReadString("long_desc", longDesc, META_DATA_LONG_DESC_LEN );
		if (foundLongDesc)
		{
			strncpy(offerMetaData->longProductDesc, longDesc, META_DATA_LONG_DESC_LEN);
			commerceDisplayf("Long desc string found: %s",longDesc);
		}
		else
		{
			commerceDisplayf("Product found with no longdesc");
			//We add a space in here to stop the long description reading as empty and refetching. This situation should be very much dev only.
			strncpy(offerMetaData->longProductDesc, " ", META_DATA_LONG_DESC_LEN);
		}

		RsonReader rrSkuArray;
		bool foundSkus = false;
		foundSkus = rrProduct.GetMember("skus", &rrSkuArray);

		done = !rrProduct.GetNextSibling(&rrProduct);

		if ( foundSkus )
		{
			rrSkuArray.GetFirstMember(&rrSkuArray);

			RsonReader rrSkuVal;
			
			if (rrSkuArray.GetMember("is_purchaseable", &rrSkuVal))
			{
				rrSkuVal.AsBool(offerMetaData->isPurchasable);
				offerMetaData->hasValidSku = true;
			}
			else
			{
				offerMetaData->isPurchasable = false;
			}

			if (rrSkuArray.GetMember("price", &rrSkuVal))
			{
				rrSkuVal.AsInt(offerMetaData->price);
			}
			else
			{
				offerMetaData->price = 0;
			}

			if (rrSkuArray.GetMember("display_price", &rrSkuVal))
			{
				rrSkuVal.AsString(offerMetaData->displayPrice, META_DISPLAY_PRICE_LEN);
			}
			else
			{
				offerMetaData->displayPrice[0] = 0;
			}

			if (rrSkuArray.GetMember("annotation", &rrSkuVal))
			{
				int annotation;
				rrSkuVal.AsInt(annotation);

				offerMetaData->hasPurchased =  ( ( annotation & SCE_NP_COMMERCE2_SKU_ANN_PURCHASED_CANNOT_PURCHASE_AGAIN ) || 
					( annotation & SCE_NP_COMMERCE2_SKU_ANN_PURCHASED_CAN_PURCHASE_AGAIN ) );

			}
			else
			{
				offerMetaData->hasPurchased = false;
			}

			if (rrSkuArray.GetMember("label",&rrSkuVal))
			{
				rrSkuVal.AsString(offerMetaData->skuProductLabel, META_DATA_PRODUCT_ID_LEN);
			}
			else
			{
				offerMetaData->hasValidSku = false;
			}
		}
		else
		{
			commerceErrorf("No sku data block found for product");
		}
	}

	//We no longer need this data
	m_ProductRequestDetail.GetBuffer().Clear();

	return true;
}

cOrbisCommerceManager::OfferMetaData* cOrbisCommerceManager::FindOfferMetadataFromID( const char* id )
{
	OfferMetaData *returnOfferData = NULL;

	for ( int i=0; i < m_NumMetaDataRecords; i++ )
	{
		if ( strcmp(id,m_MetaDataRecords[i].productIdentifier) == 0 )
		{
			returnOfferData = &m_MetaDataRecords[i];
			break;
		}
	}

	return returnOfferData;
}

}
#endif //RSG_ORBIS

