#if RSG_ORBIS

#include "OrbisCommerceConsumable.h"
#include "../CommerceChannel.h"
#include "fwnet/optimisations.h"
#include "rline/rlnp.h"
#include "rline/rlpresence.h"

#include <np.h> 

NETWORK_OPTIMISATIONS()

namespace rage
{

cOrbisCommerceConsumableManager* cOrbisCommerceConsumableManager::mp_Instance = NULL;

cOrbisCommerceConsumableManager* cOrbisCommerceConsumableManager::Instance()
{
    if ( mp_Instance == NULL )
    {
        mp_Instance = rage_new cOrbisCommerceConsumableManager;
    }

    return mp_Instance;
}


cOrbisCommerceConsumableManager::cOrbisCommerceConsumableManager() :
    m_CurrentAssetDataState(ASSET_DATA_STATE_UNINITIALISED),
	m_HasValidData(false)
{
	m_EntitlementRecords.Reserve(INITIAL_ENTITLEMENT_RECORDS_ARRAY_SIZE);
	
	mp_CurrentTransaction = rage_new cOrbisCommerceConsumableTransaction;

	m_EntitlementDataFetchStatus.Reset();
}


cOrbisCommerceConsumableManager::~cOrbisCommerceConsumableManager()
{
	if ( mp_CurrentTransaction )
	{
		delete mp_CurrentTransaction;
		mp_CurrentTransaction = NULL;
	}
}

void cOrbisCommerceConsumableManager::Init()
{
    m_CurrentAssetDataState = ASSET_DATA_STATE_UNINITIALISED;
    cCommerceConsumableManager::Init();
}

void cOrbisCommerceConsumableManager::Update()
{
    switch(m_CurrentAssetDataState)
    {
    case(ASSET_DATA_FETCHING_DATA):
        UpdateFetchingData();
        break;
    default:
        break;
    }

    cCommerceConsumableManager::Update();
}

int cOrbisCommerceConsumableManager::GetConsumableLevel( const char* aConsumableId )
{
    if ( aConsumableId == NULL )
    {
        return 0;
    }


	//Check that we have a valid response from the web api, then query that data.
    if (!m_HasValidData)
	{
		commerceAssertf(false,"Shouldn't query consumables without valid data present.");
		//You have none of the thing we have no information about.
		return 0;
	}

	int foundRecordIndex = -1;
	for (int i=0; i < m_EntitlementRecords.GetCount(); i++)
	{
		if (strcmp(aConsumableId,m_EntitlementRecords[i].m_Label) == 0)
		{
			foundRecordIndex = i;
			break;
		}
	}

	if ( foundRecordIndex == -1 )
	{
		//commerceErrorf("Searched for consumable ID [%s] which was not found", aConsumableId);
		return 0;
	}
	else
	{
		return m_EntitlementRecords[foundRecordIndex].m_UseLimit;
	}

    return 0;
}

bool cOrbisCommerceConsumableManager::IsConsumableKnown( const char* aConsumableId )
{
    if ( aConsumableId == NULL )
    {
        return false;
    }


    //Check that we have a valid response from the web api, then query that data.
    if (!m_HasValidData)
    {
        commerceErrorf("Shouldn't query consumables without valid data present.");
        //You have none of the thing we have no information about.
        return false;
    }
   
    int foundRecordIndex = -1;
    for (int i=0; i < m_EntitlementRecords.GetCount(); i++)
    {
        if (strcmp(aConsumableId,m_EntitlementRecords[i].m_Label) == 0)
        {
            foundRecordIndex = i;
            break;
        }
    }

    if ( foundRecordIndex == -1 )
    {
        //commerceErrorf("Searched for consumable ID [%s] which was not found", aConsumableId);
        return false;
    }
    else
    {
        return true;
    }
}

void cOrbisCommerceConsumableManager::DumpCurrentOwnershipData() const
{
    commerceDebugf1("-------------------------------");
    commerceDebugf1("DUMPING CURRENT OWNERSHIP DATA");
    commerceDebugf1("-------------------------------");

    if ( IsOwnershipDataPopulated() == false )
    {
        commerceDebugf1("*No ownership data to dump, No valid data downloaded*");
        return;
    }

	//Spit out our collected data.
    for ( int i=0; i < m_EntitlementRecords.GetCount(); i++ )
	{
		commerceDebugf1("Label [%s] Type [%s] Use Count [%d]  Use Limit [%d]", 
										m_EntitlementRecords[i].m_Label.c_str(), 
										m_EntitlementRecords[i].m_Type.c_str(), 
										m_EntitlementRecords[i].m_UseCount, 
										m_EntitlementRecords[i].m_UseLimit );
	}
}

bool cOrbisCommerceConsumableManager::StartOwnershipDataFetch()
{
	if ( m_CurrentAssetDataState == ASSET_DATA_FETCHING_DATA )
    {
        return false;
    }


	m_HasValidData = false;

	int localGamerIndex = rlPresence::GetActingUserIndex();
	if ( g_rlNp.GetWebAPI().GetEntitlements(localGamerIndex, &m_EntitlementDataDetail, &m_EntitlementDataFetchStatus) )
	{
		m_CurrentAssetDataState = ASSET_DATA_FETCHING_DATA;
		return true;
	}
	else
	{
		//retry and error handle
		m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
		return false;
	}

    return true;
}

void cOrbisCommerceConsumableManager::UpdateFetchingData()
{
	//Check to see if the web api has given us back our entitlement data.
	if (m_EntitlementDataFetchStatus.Pending())
	{
		return;
	}

	if (m_EntitlementDataFetchStatus.Failed())
	{
		m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
		commerceErrorf("Attempt to fetch entitlement data from webapi failed");
		return;
	}

	commerceDebugf1("DataBuffer: %s",static_cast<const char*>(m_EntitlementDataDetail.GetBuffer().GetBuffer()));
	RsonReader rr(static_cast<const char*>(m_EntitlementDataDetail.GetBuffer().GetBuffer()), m_EntitlementDataDetail.GetBuffer().Length());
	RsonReader rrEntitlement;

	m_EntitlementRecords.Reset();

	if (!rr.HasMember("total_results"))
	{
		m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
		commerceErrorf("Entitlement results do not contain required total_results member.");
		return;
	}


	RsonReader rrTotal;
	int numItems = 0;
	bool foundNumItems = rr.GetMember("total_results", &rrTotal);
	
	if ( foundNumItems )
	{
		rrTotal.AsInt(numItems);
	}
	


	if (numItems == 0)
	{
		//We have the data we want, it is just empty.
		m_CurrentAssetDataState = ASSET_DATA_STATE_DATA_POPULATED;
		m_HasValidData = true;
		return;
	}

	RsonReader rrEntitlementArray;
	
	if (!rr.GetMember("entitlements", &rrEntitlementArray))
	{
		m_CurrentAssetDataState = ASSET_DATA_STATE_ERROR;
		commerceErrorf("Entitlement result does not contain an entitlement array, but has more than 0 total_results. Very weird, also bad.");
		return;
	}


	rrEntitlementArray.GetFirstMember(&rrEntitlement);
	
	do 
	{
		char entitlementId[ENTITLEMENT_ID_MAX_LENGTH];
		bool foundEntitlementId = rrEntitlement.ReadString("id", entitlementId, ENTITLEMENT_ID_MAX_LENGTH );
		if (!foundEntitlementId)
		{
			continue;
		}

		OrbisEntitlementDataRecord newRecord;

		newRecord.m_Label = entitlementId;

		bool isConsumable = false;
		bool foundIsConsumable = rrEntitlement.ReadBool("is_consumable", isConsumable);
		if (foundIsConsumable)
		{
			newRecord.m_IsConsumable = isConsumable;
		}
		else
		{
			commerceErrorf("Consumable flag not found in entitlements data");
		}

		int useCount = 0;
		if (rrEntitlement.ReadInt("use_count", useCount))
		{
			newRecord.m_UseCount = useCount;
		}
		else
		{
			commerceErrorf("Use count for id[%s] not found in entitlements data", newRecord.m_Label.c_str());
		}
		
		int useLimit = 0;
		if (rrEntitlement.ReadInt("use_limit", useLimit))
		{
			newRecord.m_UseLimit = useLimit;
		}
		else
		{
			commerceErrorf("Use limit for id[%s] not found in entitlements data", newRecord.m_Label.c_str());
		}

		commerceDebugf1("Adding record ID [%s] use count [%d] use limit [%d]",newRecord.m_Label.c_str(),newRecord.m_UseCount,newRecord.m_UseLimit);
		m_EntitlementRecords.PushAndGrow(newRecord);
	}
	while ( rrEntitlement.GetNextSibling(&rrEntitlement) );

	//Now that it has been parsed, discard the data from the buffer.
	m_EntitlementDataDetail.Reset();

	m_CurrentAssetDataState = ASSET_DATA_STATE_DATA_POPULATED;
	m_HasValidData = true;
}


cOrbisCommerceConsumableTransaction::cOrbisCommerceConsumableTransaction() :
    m_CurrentTransactionState(ORBIS_TRANSACTION_UNINITIALISED)
{
    m_ConsumableId.Reset();
	m_EntitlementDataSetLevelStatus.Reset();
}

void cOrbisCommerceConsumableTransaction::Init()
{
    m_CurrentTransactionState = ORBIS_TRANSACTION_UNINITIALISED;
    m_ConsumableId.Reset();
    cCommerceConsumableTransaction::Init();
}


bool cOrbisCommerceConsumableTransaction::StartTransaction( const char* consumableIdentifier, int amountToConsume )
{
    if (consumableIdentifier == NULL)
    {
        return false;
    }

    if ( amountToConsume <= 0 )
    {
        //You want to consume nothing or less than nothing?!?
        return false;
    }

	int userIndex = rlPresence::GetActingUserIndex();
	g_rlNp.GetWebAPI().SetEntitlementCount(userIndex, &m_SetEntitlementsDetail, consumableIdentifier, amountToConsume, &m_EntitlementDataSetLevelStatus);

    m_ConsumableId = consumableIdentifier;
    m_CurrentTransactionState = ORBIS_TRANSACTION_PENDING;
    return cCommerceConsumableTransaction::StartTransaction(consumableIdentifier,amountToConsume);
}

void cOrbisCommerceConsumableTransaction::Update()
{
    switch(m_CurrentTransactionState)
    {
    case(ORBIS_TRANSACTION_PENDING):
        UpdatePending();
    default:
        break;
    }

    cCommerceConsumableTransaction::Update();
}  

void cOrbisCommerceConsumableTransaction::UpdatePending()
{
	if (m_EntitlementDataSetLevelStatus.Pending())
	{
		return;
	}

	if ( m_SetEntitlementsDetail.DidCallSucceed() )
	{
		m_CurrentTransactionState = ORBIS_TRANSACTION_COMPLETE;
		return;
	}
	else
	{
		commerceErrorf("Error consuming entitlement [%s]",m_ConsumableId.c_str());
		m_CurrentTransactionState = ORBIS_TRANSACTION_ERROR;
		return;
	}
}

} //namespace rage
#endif