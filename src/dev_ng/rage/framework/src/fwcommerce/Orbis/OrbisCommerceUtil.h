#ifndef ORBISCOMMERCEUTIL
#define ORBISCOMMERCEUTIL

#if RSG_ORBIS

#pragma once

#include "system/ipc.h"

#include "../CommerceUtil.h"

#include "rline/rlnpwebapiworkitem.h"


#include <rtc.h>

namespace rage
{

class cOrbisCommerceManager : public cCommerceUtil
{
public:

    virtual ~cOrbisCommerceManager();
    
    static cOrbisCommerceManager* Instance();

    virtual bool Init( int userIndex = 0, const char *skuFolder = NULL );
    void Shutdown();
    
    bool DoPlatformDataRequest();

    
    
    
    bool IsPlatformReady() const { return ( true ); } 
    bool IsContentInfoPopulated() const;
    bool IsPlatformContentInfoPopulated() const { return m_ContentInfoPopulated; }


    bool IsProductPurchased( int a_Index ) const;
    bool IsProductPurchased( const cCommerceProductData* ) const;


    bool IsProductReleased( int a_Index ) const;
    bool IsProductReleased( const cCommerceProductData* ) const;


    bool IsProductPurchasable( int a_Index ) const;
    bool IsProductPurchasable( const cCommerceProductData* ) const;

    virtual const char *GetItemLongDescription( const cCommerceProductData* product ); //Can't be const.

    void CheckoutProduct( unsigned a_Index );
    void CheckoutProduct( const cCommerceProductData* );

    bool IsInCheckout() const { return ( m_CurrentMode == MODE_CHECKOUT || m_CurrentMode == MODE_CHECKOUT_DONE || m_CurrentMode == MODE_DOWNLOAD_LIST || m_CurrentMode == MODE_PROMO_CODE ); }
    bool IsPlatformInDataFetchState() const { return ( m_CurrentMode == MODE_FETCH_CATEGORY_INFO || m_CurrentMode == MODE_FETCH_PRODUCT_INFO || m_FetchingData ); }
    bool IsInErrorState() const;
    void ForceErrorState();

    void AbortDataFetch();

    void Update();

    bool ProductCodeEntry();

    float GetProductPrice( unsigned a_Index, char* a_pOutputString, unsigned a_SizeOfOutputString ) const;
    float GetProductPrice( const cCommerceProductData*, char* /*a_pOutputString*/, unsigned /*a_SizeOfOutputString*/ ) const;
    
    //const char * GetProductId( unsigned a_Index ) const;

    void SetLineBreakString( const atString&  );

    void DoPlatformLevelDebugDump();

protected:
    void MergePlatformAndRosData();


private:
    
    cOrbisCommerceManager();
    
    //Static singleton instance of this class
    static cOrbisCommerceManager* m_Instance;

    enum eCommerceMode {
        MODE_TOP = 0,
        MODE_OPTIONS,   
        MODE_MSGDIALOG,
        MODE_NETSTART,
        MODE_NP_INIT,
        MODE_NP_TERM,
        MODE_CREATE_SESSION,
        MODE_CATEGORY,
        MODE_FETCH_CATEGORY_INFO,
		MODE_FETCH_ALL_PRODUCT_INFO,
        MODE_CONTENT_INFO,
        MODE_SKU_LIST, 
        MODE_FETCH_PRODUCT_INFO,
        MODE_PRODUCT_INFO,
        MODE_CHECKOUT,
        MODE_CHECKOUT_DONE,
        MODE_PROMO_CODE,
        MODE_PROMO_CODE_DONE,
        MODE_DOWNLOAD_LIST,
        MODE_DOWNLOAD_LIST_DONE,
        MODE_NEUTRAL,
        MODE_CREATE_SESSION_FINISHED,
		MODE_CREATE_SESSION_FINISHED_WITH_ERROR,
        MODE_ERROR_ENCOUNTERED,
        MODE_SESSION_TIMEOUT,
        MODE_MAX
    };

    enum eCommerceResultsAllocContentMode
    {
        CONTENT_RESULT_CODE_CATEGORY,
        CONTENT_RESULT_CODE_PRODUCT,
        CONTENT_RESULT_CODE_NONE,
        CONTENT_RESULT_CODE_MAX
    };

    eCommerceResultsAllocContentMode m_ContentResultBufferType;
    unsigned m_ContentResultsBufferSize;
    

    eCommerceMode m_CurrentMode;
    bool  m_ContentInfoPopulated; 
    
    //Private utility functions

    //Update functions
    void UpdatePopulateContentInfo();

    void InitialiseProductDataFromCategoryResult();
    void UpdatePopulateProductInfo();

	void UpdateFetchAllProductInfo();
    
    bool UpdateModeNeutral();
	void UpdatePromoCode();
	void UpdateCheckout();
	void UpdateDownloadList();
   
    bool IsFetchingData() const { return m_FetchingData; }

    unsigned m_ContextId;
    unsigned m_RequestId;
   
    //Event Handlers
    static void Commerce2Handler( unsigned ctx_id, unsigned subject_id, int event, int error_code, void* arg);

	//Webapi helpers
	void DoWebApiCatalogRequest();
	void DoWebApiAllProductRequest();
	bool ParseCatalogData();
	bool ParseProductData();

    rlNpGetCatalogDetail m_CategoryRequestDetail;
	rlNpGetProductDetail m_ProductRequestDetail;

  

    char* mp_CommerceResultAllocation;

	struct CategoryInfo
	{
		int countOfProducts;
	};

	struct ProductInfo
	{
		char productId[256];
		char productName[256];
		SceRtcTick releaseDate;
	};

    //Data extracted from the sony CategoryContentResult
    CategoryInfo m_CategoryInfo;
    
    bool m_CategoryContentResultInitialised;

	bool m_FetchingData;

    
    
    bool m_IsInitialised;
    bool m_IsContextCreated;

    
    bool m_NeedToRequeryData;
	
    //Meta Data functions
    void UpdateMetadataWithEnumeratedContent( ProductInfo *a_GameProductInfo, bool a_IsOwned );
    void AddNewMetadataRecordForGameProductInfo( ProductInfo *a_GameProductInfo );

    void UpdateCommerceDataWithEnumeratedContent( const char* a_ProductIdentifier, bool a_IsOwned );

    void DoLanguageSubstitutionPass();

    

    //Meta Data variables
    enum
    {
        MAX_NUM_META_DATA_RECORDS = 40,
        META_DATA_NAME_LEN = 256,
        META_DATA_SHORT_DESC_LEN = 64,
        META_DATA_LONG_DESC_LEN = 4096,
        META_DATA_PRODUCT_ID_LEN = 48,
		META_DISPLAY_PRICE_LEN = 32
    };

    enum e_MetaDataParamOrder
    {
        META_DATA_PARAM_TITLE = 0,
        META_DATA_PARAM_ID,
        META_DATA_PARAM_LONGDESC,
        META_DATA_PARAM_LINKED_ID,
        META_DATA_PARAM_DEPENDANT_ID,
        META_DATA_PARAM_EXCLUDED_ID,
        META_DATA_PARAM_DISPLAY_PRICE,
        META_DATA_PARAM_RELEASE_DATE,
        META_DATA_PARAM_INCLUDED_BIG_MASK,
        META_DATA_PARAM_LANG_CODE,
        META_DATA_PARAM_NUM_PARAMS
    };

    struct OfferMetaData
    {
        
		OfferMetaData()
		{
			productName[0] = 0;
			productIdentifier[0] = 0;
			skuProductLabel[0] = 0;
			longProductDesc[0] = 0;
			linkedProductId[0] = 0;
			dependantProductId[0] = 0;
			excludedProductId[0] = 0;
			displayPrice[0] = 0;
			price = 0;
			releaseDay = 0;
			releaseMonth = 0;
			releaseYear = 0;
			containedDLCMask = 0;
			isVirtualProduct = false;
			hasValidSku = false;
			hasPurchased = false;
			isPurchasable = false;

		}

        char productName[ META_DATA_NAME_LEN ];
        char productIdentifier[ META_DATA_SHORT_DESC_LEN ];
		char skuProductLabel[ META_DATA_PRODUCT_ID_LEN  ];
        char longProductDesc[ META_DATA_LONG_DESC_LEN ];
        
        char linkedProductId[ META_DATA_PRODUCT_ID_LEN ];
        char dependantProductId[ META_DATA_PRODUCT_ID_LEN ];
        char excludedProductId[ META_DATA_PRODUCT_ID_LEN ];

        char displayPrice[META_DISPLAY_PRICE_LEN];
		int	price;
        char releaseDay;
        char releaseMonth;
        short releaseYear;
        unsigned containedDLCMask;  

        //This is for actual backend products
        SceRtcTick releaseTick;

        bool isVirtualProduct;
        bool hasValidSku;

        bool hasPurchased;
        bool isPurchasable;
    };

    const OfferMetaData *GetOfferInfo( const cCommerceProductData* pProductInfo ) const;
    int FindExistingRecord( OfferMetaData &a_RecordToMatch );
    void DoCommerceProductInfoRequest( OfferMetaData* offerData );
	OfferMetaData* FindOfferMetadataFromID( const char* id );
    
    void FixHTMLLineBreaks( OfferMetaData *productData );
    void RemoveHTMLTags( OfferMetaData *productData );
    
    OfferMetaData *m_pCurrentRequestOffer;
    OfferMetaData *m_pLastInfoRequestOffer;

    //In the process of being burnt with fire
    atArray<OfferMetaData> m_MetaDataRecords;
    
    int m_NumMetaDataRecords;
    
    bool m_DataOperationAborted;

    atString m_LineBreakSubsString;

	netStatus m_CatalogFetchStatus;
	netStatus m_ProductInfoFetchStatus;
};

} //namespace rage

#endif //RSG_ORBIS

#endif //ORBISCOMMERCEUTIL
