#ifndef ORBISCOMMERCECONSUMABLE__H
#define ORBISCOMMERCECONSUMABLE__H

#if RSG_ORBIS

#include "../CommerceConsumable.h"

#include "atl/string.h"
#include "rline/rlnpwebapiworkitem.h"

namespace rage
{


class cOrbisCommerceConsumableTransaction : public cCommerceConsumableTransaction
{
public:
    cOrbisCommerceConsumableTransaction();

    void Init();
    void Update();

    bool StartTransaction( const char* consumableIdentifier, int amountToConsume );

    bool HasCompleted() const { return ((m_CurrentTransactionState == ORBIS_TRANSACTION_COMPLETE)||(m_CurrentTransactionState == ORBIS_TRANSACTION_ERROR)); }
    bool HasSucceeded() const { return (m_CurrentTransactionState == ORBIS_TRANSACTION_COMPLETE);}

private:
    void UpdatePending();

    enum eOrbisTransactionState
    {
        ORBIS_TRANSACTION_UNINITIALISED,
        ORBIS_TRANSACTION_PENDING,
        ORBIS_TRANSACTION_COMPLETE,
        ORBIS_TRANSACTION_ERROR
    };
    
    eOrbisTransactionState m_CurrentTransactionState;

	netStatus m_EntitlementDataSetLevelStatus;
	rlNpSetEntitlementsDetail m_SetEntitlementsDetail;
    atString m_ConsumableId;
};

class cOrbisCommerceConsumableManager : public cCommerceConsumableManager
{
public:
    static cOrbisCommerceConsumableManager* Instance();

	virtual ~cOrbisCommerceConsumableManager();

    void Init();
    void Update();

    //returns the consumable level. Returns -1 on error.
    int GetConsumableLevel( const char* aConsumableId );

    //Returns true if the entitlement is found in platform return results
    bool IsConsumableKnown( const char* aConsumableId );

    bool StartOwnershipDataFetch();
    bool IsOwnershipDataPopulated() const { return ((m_CurrentAssetDataState == ASSET_DATA_STATE_DATA_POPULATED) && cCommerceConsumableManager::IsOwnershipDataPopulated()); }

    void DumpCurrentOwnershipData() const;

protected:
    cOrbisCommerceConsumableManager();

private:
	void UpdateFetchingData();

    //Static singleton instance of this class
    static cOrbisCommerceConsumableManager* mp_Instance;

    enum eAssetDataState
    {
        ASSET_DATA_STATE_UNINITIALISED,
        ASSET_DATA_FETCHING_DATA,
        ASSET_DATA_STATE_DATA_POPULATED,
        ASSET_DATA_STATE_ERROR,
        ASSET_DATA_STATE_NUM_STATES
    };

	eAssetDataState m_CurrentAssetDataState;

	struct OrbisEntitlementDataRecord
	{
		atString	m_ActiveDate;
		atString	m_Label;
		atString	m_Type;
		int			m_UseCount;
		int			m_UseLimit;
		bool		m_IsConsumable;
	};

	bool m_HasValidData;

	netStatus m_EntitlementDataFetchStatus;
	
	rlNpGetEntitlementsDetail m_EntitlementDataDetail;

	enum 
	{
		INITIAL_ENTITLEMENT_RECORDS_ARRAY_SIZE = 32,
		ENTITLEMENT_ID_MAX_LENGTH = 64

	};

	atArray<OrbisEntitlementDataRecord> m_EntitlementRecords;
};

} //namespace rage

#endif

#endif //ORBISCOMMERCECONSUMABLE__H
