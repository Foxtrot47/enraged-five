/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : recordinginterface_win.h
// PURPOSE : Windows implementation of the recording interface
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

#ifndef _RECORDINGINTERFACE_WIN_H_
#define _RECORDINGINTERFACE_WIN_H_

// rage
#include "atl/atfixedstring.h"
#include "file/limits.h"
#include "system/messagequeue.h"
#include "recordinginterface.h"
#include "video/mediaencoder.h"

#define REC_TASK_USE_WORKER_THREAD 0
#define REC_TASK_DELAY_TO_RENDER_SYNC 1

namespace rage
{
	class grcRenderTarget;
	class grcTexture;

	class RecordingTaskWindows : public RecordingTask
	{
	public: // methods

		RecordingTaskWindows( char const * const outputPath );
		~RecordingTaskWindows();

		void	StartRecording( const char *pName, bool const startPaused, bool const synchronized, bool const hasModdedContent );
		void	PauseRecording();
		void	ResumeRecording();
		void	StopRecording();
		void	CancelRecording();

		void	Update();
		void	UpdateErrorCode();
		bool	IsPaused() const;
		bool	IsReadyForSamples() const;
		bool	IsAudioFrameCaptureAllowed() const;

		void PushVideoFrame( grcRenderTarget const& source, float const frameDuration );

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		static void	InitWorkerThread();
		static void	ShutdownWorkerThread();
#endif

		static inline void SetRecordingDataProvider( IVideoRecordingDataProvider* provider )
		{
			sm_recordingDataProvider = provider;
		}

	private: // declarations and variables
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD

		struct sWorkerData
		{
			RecordingTaskWindows*	m_task;
			eWorkerAction			m_action;
		};

		static sysIpcThreadId							sm_WorkerThread;
		static sysMessageQueue<sWorkerData, 8, true>	sm_WorkerCommandQueue;

#endif

		static IVideoRecordingDataProvider*				sm_recordingDataProvider;

		MediaEncoder		m_encoder;
		bool				m_cancelled;
		char const * const  m_outputPath;

	private: // methods

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		static void	RecorderWorkerThread(void *ptr);
#endif

		void ProcessAction( eWorkerAction const action );

		inline MediaEncoder& GetEncoder() { return m_encoder; }
		inline bool GetIsCancelled() const { return m_cancelled; }	
	};

	class VideoRecordingInterfaceWindows : public VideoRecordingInterface
	{
	public: // methods

		VideoRecordingInterfaceWindows();
		virtual ~VideoRecordingInterfaceWindows();

		void	EnableRecording();
		void	DisableRecording();

		RecordingTask *CreateRecordingTask();

		void DeleteRecordingTask(RecordingTask *pTask)
		{
			delete pTask;
		}

		inline static void SetOutputPath( char const * const path ) { ms_outputPath = path; }
		inline static char const * GetOutputPath() { return ms_outputPath.c_str(); }

	private: // declarations and variables
		static atFixedString< RAGE_MAX_PATH > ms_outputPath;

	private: // methods

	};

}

#endif // _RECORDINGINTERFACE_WIN_H_

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER
