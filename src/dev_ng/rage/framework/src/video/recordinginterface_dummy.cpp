#if ( !RSG_DURANGO && !RSG_ORBIS && !RSG_PC )

// For platforms that don't support video recording.

#include "recordinginterface_dummy.h"

namespace rage
{
	// VideoRecordingInterface creation 
	VideoRecordingInterface *VideoRecordingInterface::Create()
	{
		return NULL;
	}

	VideoRecordingInterfaceDummy::VideoRecordingInterfaceDummy() : VideoRecordingInterface()
	{

	}

	VideoRecordingInterfaceDummy::~VideoRecordingInterfaceDummy()
	{

	}

	void VideoRecordingInterfaceDummy::EnableRecording()
	{

	}

	void VideoRecordingInterfaceDummy::DisableRecording()
	{

	}

	void VideoRecordingInterfaceDummy::Update()
	{

	}

}

#endif