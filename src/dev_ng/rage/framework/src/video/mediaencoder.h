/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder.h
// PURPOSE : Class for encoding video/audio streams.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_WIN_H_
#define INC_MEDIAENCODER_WIN_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_params.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "atl/string.h"
#include "audiohardware/device.h"
#include "system/bit.h"
#include "system/ipc.h"

// framework
#include "mediaencoder_buffers.h"

#include "mediatimer.h"

// If defined, we output general encoder statistics
//#define LOG_ENCODER_STATS

// If defined, we output timing information for video frame buffering for later submission at the end of encoding
//#define LOG_VIDEO_FRAME_PUSH

// If defined, we output timing information for video frame submission to media encoder at the end of encoding
//#define LOG_VIDEO_FRAME_SUBMISSION

// If defined, we populate our video with dummy data rather than grab it from the game.
//#define USE_DUMMY_VIDEO_DATA

//#define USE_DUMMY_AUDIO_DATA

// If defined, we zero out our audio data to check for gaps between sample buffers.
//#define ZERO_AUDIO_DATA

#if RSG_PC || RSG_DURANGO

struct IMFSinkWriter;
typedef IMFSinkWriter EncoderType;

#endif

namespace rage
{
	class grcTexture;

	// TODO - If we allow multiple video recordings, create high-level coordinator for mix capturer.

	enum eEncoderState
	{
		ENCODER_INVALID,		// No state, free to be utilized

		ENCODER_PREPARING,		// In the pr
		ENCODER_ACTIVE,			// Ready to or Actively decoding streams			
		ENCODER_CLEANUP,		// Encoder canceled/ended/errored, pending release of assets
		ENCODER_PENDING_RESET	// Encoder finished, pending reset
	};

	class MediaEncoder : public IAudioMixCapturer 
	{
	private: // forward declarations
		struct ThreadData;

	public: // declarations and variables

#if defined(LOG_VIDEO_FRAME_PUSH)
		static double ms_totalFramePushedTime;
		static u64	  ms_totalFramesPushed;
#endif

#if defined(LOG_VIDEO_FRAME_SUBMISSION)
		static double ms_totalFrameSubmitTime;
		static u64	  ms_totalFramesSubmitted;
#endif

		//! Class to inject "abandon writing" to sample write loops (currently on Durango)
		class SampleWriteMonitor
		{
		public: // methods
			SampleWriteMonitor( MediaEncoder::ThreadData const& threadData )
				: m_threadData( threadData )
			{
			}

			~SampleWriteMonitor() 
			{

			}

			bool ShouldAbandonLoop() const;

		private: // declarations and variables
			MediaEncoder::ThreadData const& m_threadData;
		};

	public: // methods

		MediaEncoder();
		virtual ~MediaEncoder();

		bool StartCapture( const char * const filename, const char * const path, bool const startPaused, bool const synchronized, MediaEncoderParams const& captureParameters );
		bool IsCapturing() const;
		bool IsPaused() const;
		void PauseCapture();
		void ResumeCapture();
		bool StopCapture( bool const cancelled );

		void PushVideoFrame( grcTexture const& source, float const frameDuration );
		void CaptureMix( s16 const * const samples, size_t const sampleCount, size_t const channels );

		static void PrepareRenderThread();
		static void CleanupRenderThread();

		inline bool HasError()							const { return m_threadData.HasError(); }
		inline MediaCommon::eErrorState GetLastError()	const { return m_threadData.GetLastError(); }

		bool IsReadyToProcessSamples()					const { return m_threadData.IsReadyToProcessSamples(); }
		bool IsAudioFrameCaptureAllowed()				const { return m_threadData.IsAudioFrameCaptureAllowed(); }

	private: // declarations and variables

		enum EncoderThreadFlags
		{
			ENC_THREAD_FLAGS_NONE					= 0,
			ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN		= BIT(0),
			ENC_THREAD_FLAGS_SUB_SHUTDOWN_CONFIRM	= BIT(1),
			ENC_THREAD_FLAGS_SUB_HAD_ERRORS			= BIT(2),
		};

		struct ThreadData
		{
			atString						m_targetUri;
			MediaEncoderParams				m_encodeParams;
			MediaEncoderBuffers				m_buffers;
			u64								m_startTimeNs;
			u64								m_killTimeNs;
			u64								m_pauseTimeNs;
			s64								m_currentVideoFrameNs;
			s64								m_currentAudioFrameNs;
#if RSG_PC || RSG_DURANGO
			EncoderType*					m_encoderType;
#endif
			unsigned long					m_videoStreamIndex;
			unsigned long					m_audioStreamIndex;
			u32								m_threadMessaging;
			u32								m_subThreadMessaging;
			MediaCommon::eErrorState		m_errorState;
			eEncoderState					m_encoderState;
			bool							m_killFlag	: 1;
			bool							m_drainFlag	: 1;
			bool							m_pauseFlag : 1;

			ThreadData()
				: m_targetUri()
				, m_encodeParams()
				, m_buffers()
				, m_startTimeNs( 0 )
				, m_killTimeNs( 0 )
				, m_pauseTimeNs( 0 )
				, m_currentVideoFrameNs( 0 )
				, m_currentAudioFrameNs( 0 )
#if RSG_PC || RSG_DURANGO
				, m_encoderType( NULL )
#endif
				, m_videoStreamIndex( 0 )
				, m_audioStreamIndex( 0 )
				, m_threadMessaging( ENC_THREAD_FLAGS_NONE )
				, m_subThreadMessaging( ENC_THREAD_FLAGS_NONE )
				, m_encoderState( ENCODER_INVALID )
				, m_errorState( MediaCommon::MEDIA_ERROR_NONE )
				, m_killFlag( false )
				, m_drainFlag( false )
				, m_pauseFlag( false )
			{

			}

			void Reset()
			{
				m_targetUri.Clear();
				m_encodeParams.Reset();
				m_buffers.Shutdown();
	
				m_startTimeNs = 0;
				m_killTimeNs = 0;
				m_pauseTimeNs = 0;
				m_currentVideoFrameNs = 0;
				m_currentAudioFrameNs = 0;
#if RSG_PC || RSG_DURANGO
				m_encoderType = 0;
#endif
				m_videoStreamIndex = 0;
				m_audioStreamIndex = 0;
				m_threadMessaging = ENC_THREAD_FLAGS_NONE;
				m_subThreadMessaging = ENC_THREAD_FLAGS_NONE;
				m_encoderState = ENCODER_INVALID;
				m_killFlag = false;
				m_drainFlag = false;
				m_pauseFlag = false;
			}

			void ResetError()
			{
				m_errorState = MediaCommon::MEDIA_ERROR_NONE;
			}

			bool IsVideoFrameCaptureAllowed() const;
			bool IsAudioFrameCaptureAllowed() const;

			bool HasError()							const { return m_errorState != MediaCommon::MEDIA_ERROR_NONE; }
			MediaCommon::eErrorState GetLastError() const { return m_errorState; }

			bool IsReadyToProcessSamples() const { return m_encoderState >= ENCODER_ACTIVE && m_encoderState < ENCODER_CLEANUP; }

			void OnStopCapture( bool const cancelled )
			{
				m_pauseFlag = false;

				//! Set the flush flag so we capture all remaining buffers up to the kill time
				m_drainFlag = !cancelled;
				m_killTimeNs = MediaTimer::GetConstantTimeNs();

				m_killFlag = true;
				m_buffers.FlushPartialAudioBuffer();
			}

			void OnPauseCapture()
			{
				m_pauseFlag = true;
				m_pauseTimeNs = MediaTimer::GetConstantTimeNs();
			}
		};

		struct ThreadDataWrapper
		{
			ThreadData&				m_threadData;
			bool					m_isVideo;
			sysIpcThreadId			m_threadId;

			ThreadDataWrapper( ThreadData& threadDataRef, bool const isVideo )
				: m_threadData( threadDataRef )
				, m_isVideo( isVideo )
				, m_threadId( NULL )
			{

			}
		};

		ThreadData					m_threadData;
		ThreadDataWrapper			m_videoWrapper;
		ThreadDataWrapper			m_audioWrapper;

#if defined( USE_DUMMY_VIDEO_DATA )

		static grcTexture*		sc_dummyVideoTexture;

#endif

#if defined( USE_DUMMY_AUDIO_DATA )

		static rage::u8 const*	sc_dummyAudioBuffer;
		static u32 const		sc_dummyAudioBufferSampleCount;
		static u32 const		sc_dummyAudioBufferSize;
		static u32 const		sc_dummyAudioBufferChannelCount;

#endif

	private: // methods

		static void PrintOutVideoFramePushData();
		static void PrintOutVideoFrameSubmissionData();
		static bool GenerateTargetUri( char const * const filename, char const * const path, MediaEncoderParams const& params, atString& out_uri );
		static void GenerateDummyVideoData();
		static void GenerateDummyAudioData();
		static void CleanupDummyVideoData();

		void OnRecordingCancelled();
		bool PushVideoFrameInternal( grcTexture const& source, float const frameDuration );
		void CaptureMixInternal( s16 const * const samples, size_t const sampleCount );

		// === PLATFORM SPECIFIC FUNCTIONS BELOW ===
		static void	EncoderThreadFunc( void* dataPtr );
		static void VideoThreadFunc( ThreadData& threadData );
		static void AudioThreadFunc( ThreadData& threadData );
		static void GenerateDummyTexture( grcTexture*& out_texture );
	};

}

#endif // (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER

#endif // INC_MEDIAENCODER_WIN_H_
