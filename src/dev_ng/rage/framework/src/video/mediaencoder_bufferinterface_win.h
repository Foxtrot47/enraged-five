/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_bufferinterface_win.h
// PURPOSE : Class for injecting a IMFMediaBuffer directly into our buffering system
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_BUFFER_INTERFACE_WIN_H_
#define INC_MEDIAENCODER_BUFFER_INTERFACE_WIN_H_

#if ( RSG_PC || RSG_DURANGO )

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_params.h" // Include this before Windows to ensure we have the correct MF version

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// Windows
#pragma warning(push)
#pragma warning(disable: 4668)
#include <WinSock2.h>
#include <Windows.h>
#include <mfapi.h>
#include <mfidl.h>
#include <mfreadwrite.h>
#include <winerror.h>
#pragma warning(pop)

// rage
#include "grcore/texture.h"
#include "system/d3d11.h"

// framework
#include "video_channel.h"

namespace rage
{
	//! Should only be initialized / shutdown on the same thread, and that thread needs to be
	//! COM initialized and set-up for media-foundation API access.
	class MediaBufferBlobInterfaceWin
	{
	public:
		MediaBufferBlobInterfaceWin()
			: m_mediaBuffer( NULL )
			, m_bufferPtr( NULL )
			, m_size( 0 )
		{
		}

		~MediaBufferBlobInterfaceWin() 
		{
			Shutdown();
		};

		bool Initialize( size_t const sourceSize )
		{
			bool success = false;

			if( Verifyf( !IsInitialized(), "MediaBufferBlobInterfaceWin::Initialize - Double initialization!" ) )
			{
				HRESULT hr = MFCreateMemoryBuffer( (DWORD)sourceSize, &m_mediaBuffer );
				success = Verifyf( SUCCEEDED(hr), "MediaBufferBlobInterfaceWin::Initialize - MFCreateMemoryBuffer - Failed with 0x%08lx", hr ) && m_mediaBuffer;
			}

			return success;
		}

		inline bool IsInitialized()
		{
			return m_mediaBuffer != NULL;
		}

		void Shutdown()
		{
			if( m_mediaBuffer != NULL )
			{
				Unlock();
				ReleaseMediaObject( m_mediaBuffer );
			}

			m_size = 0;
		}

		inline bool Lock()
		{
			bool success = false;

			if( m_mediaBuffer && !IsLocked() )
			{
				DWORD maxSize = 0;
				HRESULT hr = m_mediaBuffer->Lock( &m_bufferPtr, &maxSize, NULL );
				success = Verifyf( SUCCEEDED(hr), "MediaBufferBlobInterfaceWin::Lock - Failed with 0x%08lx", hr ) && m_bufferPtr;

				m_size = success ? maxSize : 0;
			}

			return success;
		}

		inline bool IsLocked() const { return m_bufferPtr != NULL && m_size > 0; }

		inline void Unlock()
		{
			if( IsLocked() )
			{
				m_bufferPtr = NULL;
				m_size = 0;
				ASSERT_ONLY( HRESULT hr = )m_mediaBuffer->Unlock();
				videoAssertf( SUCCEEDED(hr), "MediaBufferBlobInterfaceWin::Unlock - Failed with 0x%08lx", hr );
			}
		}

		inline void SetSizeUsed( size_t const sizeUsed ) 
		{
			if( m_mediaBuffer && m_bufferPtr )
			{
				m_mediaBuffer->SetCurrentLength( (DWORD)sizeUsed );
			}
		}

		inline u8* GetBytes() { return m_bufferPtr; }
		inline u8 const* GetBytesReadOnly() const { return m_bufferPtr; }

		inline size_t GetSize() const
		{
			return m_size;
		}

		inline IMFMediaBuffer* GetMediaBuffer()
		{
			return m_mediaBuffer;
		}

	private:
		IMFMediaBuffer* m_mediaBuffer;
		u8*				m_bufferPtr;
		size_t			m_size;
	};

#if RSG_DURANGO

	class MediaBufferTextureInterfaceWin
	{
	public:
		MediaBufferTextureInterfaceWin()
			: m_mediaBuffer( NULL )
		{
		}

		~MediaBufferTextureInterfaceWin() 
		{
			Shutdown();
		};

		inline bool Initialize( grcTextureObject* texture )
		{
			bool success = false;

			if( Verifyf( !IsInitialized(), "MediaBufferTextureInterfaceWin::Initialize - Double initialization!" ) &&
				Verifyf( texture, "MediaBufferTextureInterfaceWin::Initialize - NULL texture provided" ) )
			{
				HRESULT hr;

				ID3D11Texture2D* dxTexture = (ID3D11Texture2D*)texture;
				hr = MFCreateDXGISurfaceBufferX( __uuidof(ID3D11Texture2D), dxTexture, 0, FALSE, &m_mediaBuffer );

				Assertf( SUCCEEDED(hr), "MediaBufferTextureInterfaceWin::Initialize MFCreateDXGISurfaceBuffer - Failed with 0x%08lx", hr );

				success = IsInitialized();
			}

			return success;
		}

		bool IsInitialized()
		{
			return m_mediaBuffer != NULL;
		}

		void Shutdown()
		{
			ReleaseMediaObject( m_mediaBuffer );
		}

		IMFMediaBuffer* GetMediaBuffer()
		{
			return m_mediaBuffer;
		}

	private:
		IMFMediaBuffer* m_mediaBuffer;
	};

#endif // RSG_DURANGO

}

#endif // RSG_PC && !__RESOURCECOMPILER && !__RGSC_DLL

#endif // ( RSG_PC || RSG_DURANGO )

#endif // INC_MEDIAENCODER_BUFFER_INTERFACE_WIN_H_
