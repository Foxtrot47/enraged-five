/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediadecoder.h
// PURPOSE : Class for decoding video/audio streams.
// NOTES   : For the foreseeable future we are just decoding a single video at a time.
//			 If we want to truly support multiple playback and let it scale, then the threading
//			 design needs to be reconsidered per-platform. 
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#ifndef INC_MEDIADECODER_H_
#define INC_MEDIADECODER_H_

// rage
#include "atl/string.h"
#include "audiohardware/mixer.h"
#include "math/amath.h"
#include "system/criticalsection.h"
#include "system/ipc.h"

// framework
#include "media_common.h"
#include "mediadecoder_display_frame.h"
#include "mediadecoder_format_info.h"

#if RSG_PC || RSG_DURANGO

#define THREADED_DECODER 1

struct IMFSample;
typedef IMFSample PendingSampleType;

#endif

#define MEDIA_DECODER_AUDIO_TIMER_ID (6)

namespace rage
{
	class audReferencedRingBuffer;
	class grcTexture;

	class MediaDecoder
	{
	public: // declarations and variables
		enum eDecodeState
		{
			DECODER_INVALID,	// No state, free to be utilized

			DECODER_PREPARING,			// Some platforms need time to spin-up the decoding thread/s
			DECODER_MIN_DETAIL,			// Basic information extracted from file.
			DECODER_BUFFERS_CREATED,	// Created buffering space
			DECODER_ACTIVE,				// Ready to or Actively decoding streams

			DECODER_ERROR,				// Decoder hit an error state				
			DECODER_CLEANUP				// Decoding canceled/ended, pending release of assets
		};

		enum ePlayState
		{
			PLAYSTATE_INVALID,				// Playstate undetermined. Likely we have encountered an error.

			PLAYSTATE_PAUSED,				// Pause playback, wait for further commands

			PLAYSTATE_SEEK_THEN_PAUSE,
			PLAYSTATE_SEEK_THEN_PLAY,

			PLAYSTATE_PLAYING,				// Actively decoding/updating video buffers
			PLAYSTATE_EOF,					// End of the file!

			PLAYSTATE_CLEANUP				// In the process of cleaning up. Abandon ship.
		};

	public: // methods
		
		MediaDecoder();
		~MediaDecoder()
		{
			Shutdown();
		}

		bool Initialize( const char * const path, bool const captureThumbnailThenWait, bool const noAudio );
		bool IsInitialized();
		void Shutdown();

		inline bool HasFileInfo() const
		{
			return GetDecoderState() >= DECODER_MIN_DETAIL && GetDecoderState() < DECODER_ERROR;
		}

		bool HasCreatedBuffers() const
		{
			eDecodeState const decodeState = GetDecoderState();
			return decodeState == DECODER_BUFFERS_CREATED;
		}

		bool IsPendingActivate() const
		{
			return HasCreatedBuffers();
		}

		void MarkAsActive();

		bool ReadyToOrDecoding() const
		{
			eDecodeState const decodeState = GetDecoderState();
			return decodeState == DECODER_ACTIVE && decodeState < DECODER_ERROR;
		} 

		void Update() 
		{  
			UpdatePlatformSpecific();
		}

		bool Seek( u64 const timeNs );
		bool Play();
		bool IsPaused() const;
		bool IsPendingPlay() const;
        bool IsPendingPause() const;
        bool IsAtStart() const;
		bool IsAtEnd() const;
		bool Pause();

		void PopulateTextureFrame( s64& inout_textureIdentifier, grcTexture* yTexture, grcTexture* uTexture, grcTexture* vTexture );

		static bool PeekVideoDetails( const char * const path, u32& out_width, u32& out_height, u32& out_durationMs, bool* out_hasModdedContent = NULL );

		inline u32 GetVideoSourceWidth() const					{ return GetFormatInfo().m_displayWidth; }
		inline u32 GetVideoSourceHeight() const					{ return GetFormatInfo().m_displayHeight; }
		inline size_t GetFileSize() const						{ return static_cast<size_t>(GetFormatInfo().m_fileSize); }
		inline u64 GetDurationMs() const						{ return NANOSECONDS_TO_MILLISECONDS( GetFormatInfo().m_durationNs ); }
		inline u32 GetAudioChannelCount() const					{ return GetFormatInfo().m_audioChannels; }
		inline u32 GetAudioSampleRate() const					{ return GetFormatInfo().m_audioSampleRate; }
		inline bool HasError() const							{ return GetLastError() != MediaCommon::MEDIA_ERROR_NONE; }

#if defined(THREADED_DECODER) && THREADED_DECODER
		inline audReferencedRingBuffer* GetAudioBuffer()		{ return m_threadData.m_audioBuffer; }
		inline eDecodeState GetDecoderState() const				{ return m_threadData.m_decoderState; }
		inline MediaCommon::eErrorState GetLastError() const	{ return m_threadData.m_errorState; }
		inline ePlayState GetPlayState() const					{ return m_threadData.m_playState; }
		inline ePlayState GetPendingPlayState() const			{ return m_threadData.m_pendingPlayState; }
        inline u64 GetCurrentTimeMs() const						{ return NANOSECONDS_TO_MILLISECONDS( ONE_H_NS_UNITS_TO_NANOSECONDS( m_threadData.m_currentBackbufferPlaybackTimeOneHNs ) ); }
        inline u64 GetSeekTimeMs() const						{ return NANOSECONDS_TO_MILLISECONDS( ONE_H_NS_UNITS_TO_NANOSECONDS( m_threadData.m_pendingSeekTimeNs ) ); }
#else
		inline audReferencedRingBuffer* GetAudioBuffer()		{ return m_audioBuffer; }
		inline eDecodeState GetDecoderState() const				{ return m_decoderState; }
		inline MediaCommon::eErrorState GetLastError() const	{ return m_errorState; }
		inline ePlayState GetPlayState() const					{ return m_playState; }
		inline ePlayState GetPendingPlayState() const			{ return m_playState; }
        inline u64 GetCurrentTimeMs() const						{ return NANOSECONDS_TO_MILLISECONDS( ONE_H_NS_UNITS_TO_NANOSECONDS( m_currentBackbufferPlaybackTimeOneHNs ) ); }
        inline u64 GetSeekTimeMs() const						{ return NANOSECONDS_TO_MILLISECONDS( ONE_H_NS_UNITS_TO_NANOSECONDS( m_pendingSeekTimeNs ) ); }
#endif // defined(THREADED_DECODER) && THREADED_DECODER

	private: // declarations and variables
		sysCriticalSectionToken		m_videoBufferCsToken;

#if defined(THREADED_DECODER) && THREADED_DECODER

		// Wraps all the data we need to share between threads
		struct ThreadData
		{
			MediaDecoderFormatInfo		m_formatInfo;
			MediaDecoderDisplayFrame	m_displayFrame;
			sysIpcEvent					m_wakeThreadEvent;		
			audReferencedRingBuffer*	m_audioBuffer;
			PendingSampleType*			m_pendingVideoSample;
			atString					m_targetUri;
			eDecodeState				m_decoderState;
			MediaCommon::eErrorState	m_errorState;
			ePlayState					m_playState;
			ePlayState					m_pendingPlayState;
			u64							m_pendingSeekTimeNs;
			u64							m_currentBackbufferPlaybackTimeOneHNs;
			bool						m_noAudio : 1;
			bool						m_captureThumbnail : 1;

			ThreadData()
				: m_formatInfo()
				, m_targetUri()
				, m_audioBuffer( NULL )
				, m_pendingVideoSample( NULL )
				, m_decoderState( DECODER_INVALID )
				, m_errorState( MediaCommon::MEDIA_ERROR_NONE )
				, m_wakeThreadEvent( NULL )
				, m_playState( PLAYSTATE_INVALID )
				, m_pendingPlayState( PLAYSTATE_INVALID )
				, m_currentBackbufferPlaybackTimeOneHNs( 0 )
				, m_noAudio( false )
				, m_captureThumbnail( false )
			{

			}

			~ThreadData()
			{
				Cleanup();
			}

			void Initialize( const char * const path, bool const captureThumbnail );
			void Cleanup();
			
			void ResetError()
			{
				m_errorState = MediaCommon::MEDIA_ERROR_NONE;
			}

			bool HasError()	const										{ return m_errorState != MediaCommon::MEDIA_ERROR_NONE; }
			void SetLastError( MediaCommon::eErrorState const error )	{ m_errorState = error; }
			MediaCommon::eErrorState GetLastError() const				{ return m_errorState; }

			void SetDecodeState( eDecodeState const decodeState );
			void SetDecodeStateDirect( eDecodeState const decodeState )	{ m_decoderState = decodeState; }
			eDecodeState GetDecodeState() const							{ return m_decoderState; }

			void SetPendingPlayState( ePlayState const pendingState );
			inline void SetPlaystateDirect( ePlayState const pendingState )
			{
				m_playState = m_playState != PLAYSTATE_CLEANUP ? pendingState : m_playState;
				m_pendingPlayState = m_pendingPlayState == PLAYSTATE_INVALID ? pendingState : m_pendingPlayState;
			}

			bool HasValidPlayStateForPlaybackLoop() const 
			{ 
				return m_playState != PLAYSTATE_CLEANUP && m_playState != PLAYSTATE_INVALID && !HasError(); 
			}
		};

		ThreadData					m_threadData;
		sysIpcThreadId				m_decoderThread;

#else // defined(THREADED_DECODER) && THREADED_DECODER

		MediaDecoderFormatInfo		m_formatInfo;
		MediaDecoderDisplayFrame	m_displayFrame;
		audReferencedRingBuffer*	m_audioBuffer;
		eDecodeState				m_decoderState;
		MediaCommon::eErrorState	m_errorState;
		ePlayState					m_playState;
		u64							m_pendingSeekTimeNs;
		u64							m_currentBackbufferPlaybackTimeOneHNs;
		bool						m_noAudio : 1;

#endif // defined(THREADED_DECODER) && THREADED_DECODER

	private: // methods

		bool InitializePlatformSpecific( const char * const path, bool const captureThumbnailThenWait );
		void ShutdownPlatformSpecific();

		void UpdatePlatformSpecific();
		void CleanupPlatformSpecific();

		bool GenerateDisplayFramesPlatformSpecific();
		void CleanupDisplayFramesPlatformSpecific();

		inline sysCriticalSectionToken& GetVideoBufferCSToken() { return m_videoBufferCsToken; }
		inline void ResetError() { SetErrorState( MediaCommon::MEDIA_ERROR_NONE ); }

#if defined(THREADED_DECODER) && THREADED_DECODER
		inline void SetDecoderState( eDecodeState const decodeState )			{ m_threadData.SetDecodeState( decodeState ); }
		inline void SetDecoderStateDirect( eDecodeState const decodeState )		{ m_threadData.SetDecodeStateDirect( decodeState ); }
		inline void SetErrorState( MediaCommon::eErrorState const errorState )	{ m_threadData.SetLastError( errorState ); }
		inline void SetPlayState( ePlayState const playState )					{ m_threadData.SetPendingPlayState( playState ); }
		inline void SetTargetSeekTime( u64 const seekTimeNs )					{ m_threadData.m_pendingSeekTimeNs = seekTimeNs; }
		inline void SetAudioDisabled( bool const audioDisabled )				{ m_threadData.m_noAudio = audioDisabled; }

		inline bool IsAudioDisabled() const										{ return m_threadData.m_noAudio; }

		inline MediaDecoderFormatInfo& GetFormatInfo()							{ return m_threadData.m_formatInfo; }
		inline MediaDecoderFormatInfo const& GetFormatInfo() const				{ return m_threadData.m_formatInfo; }
		inline MediaDecoderDisplayFrame& GetDisplayFrame() 						{ return m_threadData.m_displayFrame; }
		inline MediaDecoderDisplayFrame const& GetDisplayFrame() const			{ return m_threadData.m_displayFrame; }

		static void DecoderThreadFunc( void* dataPtr );
#else
		inline void SetDecoderState( eDecodeState const decodeState )			{ m_decoderState = decodeState; }
		inline void SetDecoderStateDirect( eDecodeState const decodeState )		{ m_decoderState = decodeState; }
		inline void SetErrorState( MediaCommon::eErrorState const errorState )	{ m_errorState = errorState; }
		inline void SetPlayState( ePlayState const playState )					{ m_playState = playState; }
		inline void SetTargetSeekTime( u64 const seekTimeNs )					{ m_pendingSeekTimeNs = seekTimeNs; }
		inline void SetAudioDisabled( bool const audioDisabled )				{ m_noAudio = audioDisabled; }

		inline bool IsAudioDisabled() const										{ return m_noAudio; }

		inline MediaDecoderFormatInfo& GetFormatInfo()							{ return m_formatInfo; }
		inline MediaDecoderFormatInfo const& GetFormatInfo() const				{ return m_formatInfo; }
		inline MediaDecoderDisplayFrame& GetDisplayFrame() 						{ return m_displayFrame; }
		inline MediaDecoderDisplayFrame const& GetDisplayFrame() const			{ return m_displayFrame; }

#endif // defined(THREADED_DECODER) && THREADED_DECODER
	};

} // namespace rage

#endif // INC_MEDIADECODER_H_

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED