/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : sinkwriter.cpp
// PURPOSE : Wrapper for IMFSinkWriter, but ASF implimentation for Vista
//
// AUTHOR  : andrew.keeble / J.Strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "sinkwriter.h"

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

#pragma warning(push)
#pragma warning(disable: 4668)
#include <shlwapi.h>
#include <dmort.h>
#include <dshow.h>
#include <mfapi.h>
#include <mferror.h>
#include <propvarutil.h>
#include <wmcodecdsp.h>
#pragma warning(pop)

// rage
#include "system/new.h"
#include "system/interlocked.h"

// framework
#include "media_common.h"
#include "video_channel.h"

namespace rage
{

#if RSG_PC

	HRESULT ExtractVideoWindowSize( IMFTransform* transform, ULONG& out_size )
	{
		HRESULT hr = E_INVALIDARG;
		if( transform )
		{
			IWMCodecLeakyBucket* leakyBucket = NULL;
			hr = transform->QueryInterface( IID_PPV_ARGS( &leakyBucket ) );
			if( videoVerifyf( SUCCEEDED(hr), "ExtractVideoWindowSize - IMFTransform::QueryInterface (IWMCodecLeakyBucket) failed with 0x%08lx", hr) )
			{
				hr = leakyBucket->GetBufferSizeBits( &out_size );

				out_size = 
					videoVerifyf( SUCCEEDED(hr), "ExtractVideoWindowSize - IWMCodecLeakyBucket::GetBufferSizeBits failed with 0x%08lx", hr) ?
					out_size : 3000; // 3000 is the MSDN recommended default, so use that as fallback

			}

			ReleaseMediaObject( leakyBucket );
		}

		return hr;
	}

	//! Setup encoder properties based on encoding mode
	HRESULT SetEncodingProperties( GUID const guidMajorType, ASFSinkWriterWrapper::eEncodingMode const encodingMode, IMFTransform* transform, IPropertyStore* pProps )
	{
		HRESULT hr = E_INVALIDARG;
		if( pProps )
		{
			PROPVARIANT var;

			switch( encodingMode )
			{
			case ASFSinkWriterWrapper::ENCODING_CBR:
				{
					// Set VBR to false
					InitPropVariantFromBoolean( FALSE, &var );
					hr = pProps->SetValue( MFPKEY_VBRENABLED, var );
					videoDisplayf( "SetEncodingProperties - Setting MFPKEY_VBRENABLED returned 0x%08lx", hr );
					PropVariantClear( &var );

					// Set the video buffer window.
					if( IsEqualGUID( guidMajorType, MFMediaType_Video ) )
					{
						ULONG windowSize;
						hr = ExtractVideoWindowSize( transform, windowSize );
						videoDisplayf( "SetEncodingProperties - ExtractVideoWindowSize returned 0x%08lx", hr );

						InitPropVariantFromInt32( windowSize, &var);
						hr = pProps->SetValue( MFPKEY_VIDEOWINDOW, var );
						videoDisplayf( "SetEncodingProperties - Setting MFPKEY_VIDEOWINDOW returned 0x%08lx", hr );
						PropVariantClear( &var );
					}

					hr = S_OK;
					break;
				}

			case ASFSinkWriterWrapper::ENCODING_VBR:
				{
					// Set VBR to true
					InitPropVariantFromBoolean( TRUE, &var );
					hr = pProps->SetValue( MFPKEY_VBRENABLED, var );
					videoDisplayf( "SetEncodingProperties - Setting MFPKEY_VBRENABLED returned 0x%08lx", hr );

					// Number of encoding passes is 1.

					InitPropVariantFromInt32( 1, &var );
					hr = pProps->SetValue( MFPKEY_PASSESUSED, var);
					videoDisplayf( "SetEncodingProperties - Setting MFPKEY_PASSESUSED returned 0x%08lx", hr );
					PropVariantClear( &var );

					// Set the quality level.
					if( IsEqualGUID( guidMajorType, MFMediaType_Video )  )
					{
						InitPropVariantFromUInt32( 100, &var );
						pProps->SetValue( MFPKEY_VBRQUALITY, var );
						videoDisplayf( "SetEncodingProperties - Setting MFPKEY_VBRQUALITY returned 0x%08lx", hr );
						PropVariantClear( &var );
					}
					else if( IsEqualGUID( guidMajorType, MFMediaType_Audio ) )
					{
						InitPropVariantFromUInt32( 100, &var);
						hr = pProps->SetValue( MFPKEY_DESIRED_VBRQUALITY, var );
						videoDisplayf( "SetEncodingProperties - Setting MFPKEY_DESIRED_VBRQUALITY returned 0x%08lx", hr );
						PropVariantClear( &var );
					}

					hr = S_OK;
					break;
				}

			default:
				{
					videoAssertf( false, "SetEncodingProperties - Invalid encoding mode %u requested", encodingMode );
					hr = MF_E_INVALIDREQUEST;
					break;
				}
			}
		}

		return hr;
	}

	HRESULT AddPrivateVideoDataFromTransform( IMFMediaType* videoType, IMFTransform* videoTransform )
	{
		HRESULT hr = E_INVALIDARG;

		if( videoType && videoTransform )
		{
			DMO_MEDIA_TYPE mtOut = { 0 };

			// Get the DMO Media type info first
			hr = MFInitAMMediaTypeFromMFMediaType( videoType, FORMAT_VideoInfo, (AM_MEDIA_TYPE*)&mtOut );
			if( videoVerifyf( SUCCEEDED(hr), "AddPrivateVideoDataFromTransform - MFInitAMMediaTypeFromMFMediaType failed with 0x%08lx", hr ) )
			{
				IWMCodecPrivateData *pPrivData = NULL;

				hr = videoTransform->QueryInterface( IID_PPV_ARGS(&pPrivData) );
				if( videoVerifyf( SUCCEEDED(hr), "AddPrivateVideoDataFromTransform - IMFTransform::QueryInterface (IWMCodecPrivateData) failed with 0x%08lx", hr ) )
				{
					// Set up the output type
					hr = pPrivData->SetPartialOutputType( &mtOut );
					if( videoVerifyf( SUCCEEDED(hr), "AddPrivateVideoDataFromTransform - IWMCodecPrivateData::SetPartialOutputType failed with 0x%08lx", hr ) )
					{
						ULONG bufferSize = 0;
						hr = pPrivData->GetPrivateData( NULL, &bufferSize );
						if( videoVerifyf( SUCCEEDED(hr), "AddPrivateVideoDataFromTransform - IWMCodecPrivateData::GetPrivateData (size) failed with 0x%08lx", hr ) )
						{
							bool const c_shouldCreateBuffer = bufferSize > 0;
							BYTE* dataBuffer = c_shouldCreateBuffer ? rage_new BYTE[ bufferSize ] : NULL;
							hr = c_shouldCreateBuffer && dataBuffer ? S_OK : E_OUTOFMEMORY;
							if( !c_shouldCreateBuffer || videoVerifyf( SUCCEEDED(hr), "AddPrivateVideoDataFromTransform - Failed to allocate buffer for private data of size %u", bufferSize ) )
							{
								// Grab and add the data to the media type.
								hr = pPrivData->GetPrivateData( dataBuffer, &bufferSize );
								if( videoVerifyf( SUCCEEDED(hr), "AddPrivateVideoDataFromTransform - IWMCodecPrivateData::GetPrivateData (data) failed with 0x%08lx", hr ) )
								{
									hr = videoType->SetBlob( MF_MT_USER_DATA, dataBuffer, bufferSize );
									videoDisplayf( "AddPrivateVideoDataFromTransform - IMFMediaType::SetBlob (MF_MT_USER_DATA) returned with 0x%08lx", hr );
								}

								if( dataBuffer )
								{
									delete [] dataBuffer;
								}
							}
						}
					}
				}

				ReleaseMediaObject( pPrivData );
			}

			MoFreeMediaType( &mtOut );
		}

		return hr;
	}

	HRESULT CreateTransformForOutputMediaType( IMFTransform** out_transform, IMFMediaType* mediaType )
	{
		HRESULT hr = out_transform == NULL ? E_POINTER : E_INVALIDARG;

		if( mediaType && out_transform && *out_transform == NULL )
		{
			GUID guidMajorType = GUID_NULL;
			GUID guidSubType = GUID_NULL;

			hr = mediaType->GetMajorType( &guidMajorType );
			videoDisplayf( "CreateTransformForOutputMediaType -  IMFMediaType::GetMajorType returned 0x%08lx", hr );

			hr = mediaType->GetGUID( MF_MT_SUBTYPE, &guidSubType ); 
			videoDisplayf( "CreateTransformForOutputMediaType - GetGUID - MF_MT_SUBTYPE returned with 0x%08lx", hr );

			if( !IsEqualGUID( guidMajorType, GUID_NULL ) && !IsEqualGUID( guidSubType, GUID_NULL ) )
			{
				UINT32 count = 0;
				CLSID* pClsids;

				MFT_REGISTER_TYPE_INFO outputInfo = { guidMajorType, guidSubType };
				GUID const c_category = IsEqualGUID( guidMajorType, MFMediaType_Video ) ? MFT_CATEGORY_VIDEO_ENCODER : MFT_CATEGORY_AUDIO_ENCODER;

				//! Do not add MFT_ENUM_FLAG_LOCALMFT, or we'll enumerate ourselves! 
				UINT32 const c_enumFlags = MFT_ENUM_FLAG_SYNCMFT | MFT_ENUM_FLAG_SORTANDFILTER;

				hr = MFTEnum(
					c_category,
					c_enumFlags,
					NULL,
					&outputInfo,		
					NULL, 
					&pClsids, 
					&count);

				if( SUCCEEDED(hr) )
				{
					// It is possible to succeed and find zero supported codecs so drop out appropriately,
					// otherwise lets run with the first supported codec we find
					hr = count == 0 ? MF_E_TOPO_CODEC_NOT_FOUND :
						CoCreateInstance( pClsids[0], NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(out_transform) );
				}

				CoTaskMemFree( pClsids );
			}
		}

		return hr;
	}

	//! Create and attach a video stream to the ASF Profile
	HRESULT CreateVideoStream( IMFTransform*& out_videoTransform, IMFMediaType* videoType, WORD const streamNumber )
	{
		HRESULT hr = E_INVALIDARG;
		if( videoType && out_videoTransform == NULL )
		{
			hr = MF_E_INVALIDSTREAMNUMBER;
			if( ASFSinkWriterWrapper::IsPotentiallyValidStreamNumber( streamNumber ) )
			{
				hr = CreateTransformForOutputMediaType( &out_videoTransform, videoType );
				videoAssertf( SUCCEEDED(hr), "CreateVideoStream - Creating video transform failed with 0x%08lx", hr);
			}
		}

		return hr;
	}

	//! Create and attach an audio stream to the ASF Profile
	HRESULT CreateAudioStream( IMFTransform*& out_audioTransform, IMFMediaType* audioType, WORD const streamNumber )
	{
		HRESULT hr = E_INVALIDARG;
		if( audioType && out_audioTransform == NULL )
		{
			hr = MF_E_INVALIDSTREAMNUMBER;
			if( ASFSinkWriterWrapper::IsPotentiallyValidStreamNumber( streamNumber ) )
			{
				hr = CreateTransformForOutputMediaType( &out_audioTransform, audioType );
				videoAssertf( SUCCEEDED(hr), "CreateAudioStream - Creating audio transform failed with 0x%08lx", hr);
			}
		}

		return hr;
	}

#endif // RSG_PC

	//=== SinkWriterCreator Interfaces ===

	HRESULT SinkWriterCreator::CreateSinkWriter( LPCWSTR pwszOutputURL, IMFByteStream *pByteStream, IMFAttributes *pAttributes, IMFSinkWriter **ppSinkWriter )
	{
#if RSG_PC
		HRESULT result = ppSinkWriter == NULL ? E_POINTER : E_FAIL;
		if( ppSinkWriter )
		{
			videoAssertf( *ppSinkWriter == NULL, "SinkWriterCreator::CreateSinkWriter - Leaking interface, ppSinkWriter non-null" );

			if( MediaCommon::IsWindows7OrGreater() )
			{
				result = MFCreateSinkWriterFromURL( pwszOutputURL, pByteStream, pAttributes, ppSinkWriter );
			}
			else
			{
				ASFSinkWriterWrapper* sinkWriter = rage_new ASFSinkWriterWrapper();
				result = sinkWriter ? sinkWriter->Initialize( pwszOutputURL ) : E_OUTOFMEMORY;

				if( FAILED( result ) )
				{
					ReleaseMediaObject( sinkWriter );
					videoDisplayf( "SinkWriterCreator::CreateSinkWriter - Failed ASFSinkWriter creation with 0x%08lx", result );
				}

				*ppSinkWriter = static_cast<IMFSinkWriter*>(sinkWriter);
			}
		}

		return result;
#else

		return MFCreateSinkWriterFromURL( pwszOutputURL, pByteStream, pAttributes, ppSinkWriter );

#endif
	}


#if RSG_PC

	//=== ASFSinkWriterWrapper Interfaces ===
	ASFSinkWriterWrapper::ASFSinkWriterWrapper()
		: m_profile( NULL )
		, m_contentInfo( NULL )
		, m_videoTransform( NULL )
		, m_audioTransform( NULL )
		, m_videoOutputType( NULL )
		, m_audioOutputType( NULL )
		, m_mediaSinkActivate( NULL )
		, m_mediaSink( NULL )
		, m_presentationClock( NULL )
		, m_outputURL( NULL )
		, m_refCount( 1 )
		, m_encodingMode( ENCODING_CBR )
	{

	}

	ASFSinkWriterWrapper::~ASFSinkWriterWrapper()
	{
		videoAssertf( m_refCount == 0, "ASFSinkWriterWrapper Destructor - Failed sanity check with ref-count of %u", m_refCount );
		Cleanup();
	}

	ULONG STDMETHODCALLTYPE ASFSinkWriterWrapper::AddRef()
	{
		return rage::sysInterlockedIncrement( &m_refCount ); 
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::QueryInterface( REFIID riid, _COM_Outptr_ void **ppvObject )
	{
		HRESULT result = ppvObject == NULL ? E_POINTER : E_NOINTERFACE;
		if( ppvObject )
		{
			videoAssertf( *ppvObject == NULL, "ASFSinkWriterWrapper::QueryInterface - Leaking interface, ppvObject non-null" );

			if( riid == IID_IUnknown )
			{
				*ppvObject = static_cast<IUnknown*>(this);
				AddRef();
			}
			else if( riid == IID_IMFSinkWriter )
			{
				*ppvObject = static_cast<IMFSinkWriter*>(this);
				AddRef();
			}
			else
			{
				*ppvObject = NULL;
			}

			// If we set the object by now, assume we done good!
			result = *ppvObject != NULL ? S_OK : result;
		}

		return result;
	}

	ULONG STDMETHODCALLTYPE ASFSinkWriterWrapper::Release()
	{
		u32 const c_refCountRemaining = rage::sysInterlockedDecrement( &m_refCount );

		if( c_refCountRemaining == 0 )
		{
			delete this;
		}

		return c_refCountRemaining;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::AddStream( IMFMediaType* pTargetMediaType, DWORD* pdwStreamIndex )
	{
		if( IsWriting() )
		{
			// We don't support adding streams after writing has started
			return MF_E_INVALIDREQUEST;
		}

		HRESULT hr = pdwStreamIndex == NULL ? E_INVALIDARG : S_OK;
		if( pdwStreamIndex )
		{
			GUID guidMajorType = GUID_NULL;
			hr = pTargetMediaType->GetMajorType(&guidMajorType);
			if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - pTargetMediaType->GetMajorType failed with 0x%08lx", hr) )
			{
				s32 streamNumber = -1;
				IMFMediaType** mediaTypeToSet = NULL;

				if( IsEqualGUID( guidMajorType, MFMediaType_Video ) )
				{
					if( IsVideoStreamSetup() )
					{
						hr = MF_E_INVALIDREQUEST;
						videoDisplayf( "ASFSinkWriterWrapper::AddStream - Attempting to double add video stream?" );
					}
					else
					{
						streamNumber = 1;
						mediaTypeToSet = &m_videoOutputType;
						hr = CreateVideoStream( m_videoTransform, pTargetMediaType, (WORD)streamNumber );
						videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - CreateVideoStream failed with 0x%08lx", hr);
					}
					
				}
				else if( IsEqualGUID( guidMajorType, MFMediaType_Audio ) )
				{
					if( IsAudioStreamSetup() )
					{
						hr = MF_E_INVALIDREQUEST;
						videoDisplayf( "ASFSinkWriterWrapper::AddStream - Attempting to double add audio stream?" );
					}
					else
					{
						streamNumber = 2;
						mediaTypeToSet = &m_audioOutputType;
						hr = CreateAudioStream( m_audioTransform, pTargetMediaType, (WORD)streamNumber );
						videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - CreateAudioStream failed with 0x%08lx", hr);
					}
				}

				if( SUCCEEDED(hr) )
				{
					*pdwStreamIndex = static_cast<DWORD>( streamNumber ) - 1;

					// Store the output type, as we'll need it later once the input streams are set
					pTargetMediaType->AddRef();
					*mediaTypeToSet = pTargetMediaType;
				}
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::BeginWriting()
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( videoVerifyf( IsInitialized(), "ASFSinkWriterWrapper::BeginWriting - Not initialized?" ) &&
			videoVerifyf( !IsWriting(), "ASFSinkWriterWrapper::BeginWriting - already called BeginWriting" ) )
		{
			hr = BeginWritingInternal();
			if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::BeginWriting - Activation failed with 0x%08lx", hr ) )
			{
				if ( videoVerifyf( AreBothStreamsReadyForWriting() && m_mediaSinkActivate, "ASFSinkWriterWrapper::BeginWriting - not able to begin. "
					"m_pActivate = %s, video stream %s, audio stream %s", m_mediaSinkActivate ? "exists" : "null", 
					IsVideoStreamReadyForWriting() ? "ready" : "invalid",
					IsAudioStreamReadyForWriting() ? "ready" : "invalid" ) )
				{
					hr = m_videoTransform->ProcessMessage( MFT_MESSAGE_NOTIFY_BEGIN_STREAMING, 0 );
					if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
						"IMFTransform::ProcessMessage (MFT_MESSAGE_NOTIFY_BEGIN_STREAMING - Video) failed with 0x%08lx", hr ) )
					{
						hr = m_audioTransform->ProcessMessage( MFT_MESSAGE_NOTIFY_BEGIN_STREAMING, 0 );
						if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
							"IMFTransform::ProcessMessage (MFT_MESSAGE_NOTIFY_BEGIN_STREAMING- Audio) failed with 0x%08lx", hr ) )
						{
							hr = MFCreatePresentationClock( &m_presentationClock );
							if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
								"MFCreatePresentationClock failed with 0x%08lx", hr ) )
							{
								IMFPresentationTimeSource* timeSource = NULL;
								hr = MFCreateSystemTimeSource( &timeSource );
								if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
									"MFCreateSystemTimeSource failed with 0x%08lx", hr ) )
								{
									hr = m_presentationClock->SetTimeSource( timeSource );
									if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
										"IMFPresentationClock::SetTimeSource failed with 0x%08lx", hr ) )
									{
										hr = m_mediaSinkActivate->ActivateObject( IID_PPV_ARGS( &m_mediaSink ) );
										if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
											"IMFActivate::ActivateObject (IMFMediaSink) failed with 0x%08lx", hr ) )
										{
											hr = m_mediaSink->SetPresentationClock( m_presentationClock );
											videoAssertf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
												"IMFMediaSink::SetPresentationClock failed with 0x%08lx", hr );

											hr = m_presentationClock->Start(0);
											videoAssertf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::BeginWriting - "
												"IMFPresentationClock::Start failed with 0x%08lx", hr );
										}
									}
								}

								ReleaseMediaObject( timeSource );
							}
						}
					}
				}
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::Finalize()
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{
			bool drainStreams = true;
			HRESULT videoDrainHr;
			HRESULT audioDrainHr;

			// drain video
			do 
			{
				videoDrainHr = WriteSampleInternal( 0, NULL, true );
				audioDrainHr = WriteSampleInternal( 1, NULL, true );

				drainStreams = !( videoDrainHr == MF_E_TRANSFORM_NEED_MORE_INPUT && audioDrainHr == MF_E_TRANSFORM_NEED_MORE_INPUT );
			} 
			while ( drainStreams );

			hr = m_videoTransform->ProcessMessage( MFT_MESSAGE_NOTIFY_END_STREAMING, 0 );
			videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::Finalize - IMFTransform::ProcessMessage "
				"(MFT_MESSAGE_NOTIFY_END_STREAMING, video )returned with 0x%08lx", hr );

			hr = m_audioTransform->ProcessMessage( MFT_MESSAGE_NOTIFY_END_STREAMING, 0 );
			videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::Finalize - IMFTransform::ProcessMessage "
				"(MFT_MESSAGE_NOTIFY_END_STREAMING, audio )returned with 0x%08lx", hr );

			hr = m_presentationClock->Stop();
			videoAssertf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::Finalize - "
				"IMFPresentationClock::Stop failed with 0x%08lx", hr );

			hr = PlaceMarkerInternal( 0, MFSTREAMSINK_MARKER_ENDOFSEGMENT, NULL, NULL );
			videoAssertf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::Finalize - "
				"PlaceMarkerInternal (video) failed with 0x%08lx", hr );
			
			hr = PlaceMarkerInternal( 1, MFSTREAMSINK_MARKER_ENDOFSEGMENT, NULL, NULL );
			videoAssertf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::Finalize - "
				"PlaceMarkerInternal (audio) failed with 0x%08lx", hr );

			FinalizeInternal();

			hr = m_mediaSink->Shutdown();
			videoAssertf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::Finalize - "
				"IMFMediaSink::Shutdown failed with 0x%08lx", hr );

			EndWritingInternal();
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::Flush( DWORD dwStreamIndex )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{
			bool const c_allStreams = dwStreamIndex == (DWORD)MF_SINK_WRITER_ALL_STREAMS;
			hr = MF_E_INVALIDSTREAMNUMBER;

			if( IsValidStreamIndex( (WORD)dwStreamIndex ) || c_allStreams )
			{
				hr = E_FAIL;

				bool const c_flushVideo = c_allStreams || IsVideoStreamIndex( (WORD)dwStreamIndex );
				bool const c_flushAudio = c_allStreams || IsAudioStreamIndex( (WORD)dwStreamIndex );

				if( c_flushVideo )
				{
					hr = m_videoTransform->ProcessMessage( MFT_MESSAGE_COMMAND_FLUSH, 0 );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::Flush - IMFTransform::ProcessMessage "
						"(MFT_MESSAGE_COMMAND_FLUSH, video )returned with 0x%08lx", hr );

					FlushStreamSinkInternal( 0 );
				}

				if( c_flushAudio )
				{
					hr = m_audioTransform->ProcessMessage( MFT_MESSAGE_COMMAND_FLUSH, 0 );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::Flush - IMFTransform::ProcessMessage "
						"(MFT_MESSAGE_COMMAND_FLUSH, audio )returned with 0x%08lx", hr );

					FlushStreamSinkInternal( 1 );
				}
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::GetServiceForStream( DWORD dwStreamIndex, REFGUID guidService, REFIID riid, LPVOID *ppvObject )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsInitialized() )
		{
			if( IsEqualGUID( guidService, GUID_NULL ) )
			{
				if( IsWriting() && dwStreamIndex == static_cast<DWORD>( MF_SINK_WRITER_MEDIASINK ) )
				{
					// Meta-data comes from the content info on an ASF if we want it to be persisted correctly
					bool const c_useContentInfo = ( riid == IID_IMFMetadataProvider ) == TRUE;

					hr = c_useContentInfo ? 
						m_contentInfo->QueryInterface( riid, ppvObject ) : m_mediaSink->QueryInterface( riid, ppvObject );
				}
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::GetStatistics( DWORD dwStreamIndex, MF_SINK_WRITER_STATISTICS* pStats )
	{
		HRESULT hr = pStats == NULL ? E_INVALIDARG : MF_E_INVALIDREQUEST;

		if( IsInitialized() && pStats )
		{
			hr = MF_E_INVALIDSTREAMNUMBER;

			if( IsValidStreamIndex( (WORD)dwStreamIndex ) || dwStreamIndex == MF_SINK_WRITER_ALL_STREAMS  )
			{
				hr = E_NOTIMPL;
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::NotifyEndOfSegment( DWORD dwStreamIndex )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{
			bool const c_allStreams = dwStreamIndex == (DWORD)MF_SINK_WRITER_ALL_STREAMS;
			hr = MF_E_INVALIDSTREAMNUMBER;

			if( IsValidStreamIndex( (WORD)dwStreamIndex ) || c_allStreams )
			{
				hr = E_FAIL;

				bool const c_drainVideo = c_allStreams || IsVideoStreamIndex( (WORD)dwStreamIndex );
				bool const c_drainAudio = c_allStreams || IsAudioStreamIndex( (WORD)dwStreamIndex );

				if( c_drainVideo )
				{
					hr = m_videoTransform->ProcessMessage( MFT_MESSAGE_NOTIFY_END_OF_STREAM, 0 );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::NotifyEndOfSegment - IMFTransform::ProcessMessage "
						"(MFT_MESSAGE_NOTIFY_END_OF_STREAM, video )returned with 0x%08lx", hr );

					hr = m_videoTransform->ProcessMessage( MFT_MESSAGE_COMMAND_DRAIN, 0 );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::NotifyEndOfSegment - IMFTransform::ProcessMessage "
						"(MFT_MESSAGE_COMMAND_DRAIN, video )returned with 0x%08lx", hr );
				}

				if( c_drainAudio )
				{
					hr = m_audioTransform->ProcessMessage( MFT_MESSAGE_NOTIFY_END_OF_STREAM, 0 );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::NotifyEndOfSegment - IMFTransform::ProcessMessage "
						"(MFT_MESSAGE_NOTIFY_END_OF_STREAM, audio )returned with 0x%08lx", hr );

					hr = m_audioTransform->ProcessMessage( MFT_MESSAGE_COMMAND_DRAIN, 0 );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::NotifyEndOfSegment - IMFTransform::ProcessMessage "
						"(MFT_MESSAGE_COMMAND_DRAIN, audio )returned with 0x%08lx", hr );
				}
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::PlaceMarker( DWORD dwStreamIndex, LPVOID pvContext )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsInitialized() )
		{
			hr = MF_E_INVALIDSTREAMNUMBER;

			if( IsValidStreamIndex( (WORD)dwStreamIndex ) )
			{
				(void)pvContext;
				hr = E_NOTIMPL;
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::SendStreamTick( DWORD dwStreamIndex, LONGLONG llTimestamp )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsInitialized() )
		{
			hr = MF_E_INVALIDSTREAMNUMBER;

			if( IsValidStreamIndex( (WORD)dwStreamIndex ) )
			{
				(void)llTimestamp;
				hr = E_NOTIMPL;
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::SetInputMediaType( DWORD dwStreamIndex, IMFMediaType *pInputMediaType, IMFAttributes *pEncodingParameters )
	{
		(void)pEncodingParameters;

		if( IsWriting() )
		{
			// We don't support adding streams after writing has started
			return MF_E_INVALIDREQUEST;
		}

		HRESULT hr = IsPotentiallyValidStreamIndex( (WORD)dwStreamIndex ) ? E_INVALIDARG : MF_E_INVALIDSTREAMNUMBER;
		if( pInputMediaType && IsPotentiallyValidStreamIndex( (WORD)dwStreamIndex ) )	
		{
			bool const c_isVideoStream = IsVideoStreamIndex( (WORD)dwStreamIndex );
			bool const c_isAudioStream = IsAudioStreamIndex( (WORD)dwStreamIndex );

			IMFTransform* targetTransform = c_isVideoStream ? m_videoTransform : c_isAudioStream ? m_audioTransform : NULL;

			// MUST be pointer ref as we may substitute it out later and don't want to leak anything
			IMFMediaType*& outputType = c_isVideoStream ? m_videoOutputType : m_audioOutputType;

			bool const c_readyToAddInput = targetTransform && outputType;
			if( c_readyToAddInput )
			{
				hr = targetTransform->SetInputType( 0, pInputMediaType, 0 );
				if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - IMFTransform::SetInputType (%s) failed with 0x%08lx",
					c_isVideoStream ? "video" : c_isAudioStream ? "audio" : "unknown?", hr) )
				{
					// Optimism - Try the base output type
					hr = targetTransform->SetOutputType( 0, outputType, 0 );

					//! Realism - Find the best-fit output as fallback
					if( FAILED( hr ) )
					{
						IMFMediaType* matchingType = NULL;
						hr = FindClosestMatchingType( matchingType, outputType, targetTransform );

						if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - Fallback output type for %s failed with 0x%08lx",
							c_isVideoStream ? "video" : c_isAudioStream ? "audio" : "unknown?", hr) )
						{
							hr = targetTransform->SetOutputType( 0, matchingType, 0 );
							videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - IMFTRansform::SetOutputType returned with 0x%08lx", hr );

							ReleaseMediaObject( outputType );
							outputType = matchingType;
						}
					}

					if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - IMFTransform::SetOutputType (%s) failed with 0x%08lx",
						c_isVideoStream ? "video" : c_isAudioStream ? "audio" : "unknown?", hr) )
					{
						WORD const streamNumber = (WORD)dwStreamIndex + 1;
						IMFASFStreamConfig* streamConfig = NULL;

						//Create a new stream with the audio type
						hr = m_profile->CreateStream( outputType, &streamConfig );
						if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - IMFASFProfile::CreateStream failed with 0x%08lx", hr ) )
						{
							//Set a valid stream number
							hr = streamConfig->SetStreamNumber( streamNumber );
							if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - IMFASFStreamConfig::SetStreamNumber failed with 0x%08lx", hr ) )
							{
								//Add the stream to the profile
								hr = m_profile->SetStream( streamConfig );
								if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - "
									"IMFASFProfile::SetStream failed with 0x%08lx", hr ) )
								{
									GUID guidMajorType = GUID_NULL;
									hr = outputType->GetMajorType( &guidMajorType );
									if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - "
										"pTargetMediaType->GetMajorType failed with 0x%08lx", hr) )
									{
										//Get stream's encoding property
										IPropertyStore* streamProperties = NULL;
										hr = m_contentInfo->GetEncodingConfigurationPropertyStore( streamNumber, &streamProperties );
										if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - "
											"m_pContentInfo->GetEncodingConfigurationPropertyStore failed with 0x%08lx", hr) )
										{
											hr = SetEncodingProperties( guidMajorType, m_encodingMode, targetTransform, streamProperties );
											videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - SetEncodingProperties failed with 0x%08lx", hr);
										}

										ReleaseMediaObject( streamProperties );
									}
								}
							}
						}

						ReleaseMediaObject( streamConfig );
					}
				}
			}
			else
			{
				hr = MF_E_INVALIDSTREAMNUMBER;
			}
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::WriteSample( DWORD dwStreamIndex, IMFSample *pSample )
	{
		return WriteSampleInternal( dwStreamIndex, pSample, false );
	}

	ASFSinkWriterWrapper::FinalizationCallback::FinalizationCallback( sysIpcEvent& asynchOpCompleteEvent, IMFFinalizableMediaSink& callee  )
		: m_asynchOpCompleteEvent( asynchOpCompleteEvent )
		, m_callee( callee )
		, m_refCount( 1 )
	{

	}

	ASFSinkWriterWrapper::FinalizationCallback::~FinalizationCallback()
	{
		videoAssertf( m_refCount == 0, "SinkWriterFinalizeCallback Destructor - Failed sanity check with ref-count of %u", m_refCount );
	}

	ULONG	STDMETHODCALLTYPE ASFSinkWriterWrapper::FinalizationCallback::AddRef()
	{
		return rage::sysInterlockedIncrement( &m_refCount );
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::FinalizationCallback::QueryInterface( REFIID riid, _COM_Outptr_ void **ppvObject )
	{
		HRESULT result = ppvObject == NULL ? E_POINTER : E_NOINTERFACE;
		if( ppvObject )
		{
			videoAssertf( *ppvObject == NULL, "AsyncEvent::QueryInterface - Leaking interface, ppvObject non-null" );

			if( riid == IID_IUnknown )
			{
				*ppvObject = static_cast<IUnknown*>(this);
				AddRef();
			}
			else if( riid == IID_IMFAsyncCallback )
			{
				*ppvObject = static_cast<IMFAsyncCallback*>(this);
				AddRef();
			}
			else
			{
				*ppvObject = NULL;
			}

			// If we set the object by now, assume we done good!
			result = *ppvObject != NULL ? S_OK : result;
		}

		return result;
	}

	ULONG	STDMETHODCALLTYPE ASFSinkWriterWrapper::FinalizationCallback::Release()
	{
		u32 const c_refCountRemaining = rage::sysInterlockedDecrement( &m_refCount );

		if( c_refCountRemaining == 0 )
		{
			delete this;
		}

		return c_refCountRemaining;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::FinalizationCallback::GetParameters( DWORD* pdwFlags, DWORD* pdwQueue )
	{
		HRESULT hr = E_INVALIDARG;

		if( pdwFlags && pdwQueue )
		{
			*pdwFlags = ( MFASYNC_FAST_IO_PROCESSING_CALLBACK | MFASYNC_SIGNAL_CALLBACK );
			*pdwQueue = MFASYNC_CALLBACK_QUEUE_TIMER;
		}

		return hr;
	}

	HRESULT STDMETHODCALLTYPE ASFSinkWriterWrapper::FinalizationCallback::Invoke( IMFAsyncResult* pAsyncResult )
	{
		HRESULT hr = pAsyncResult == NULL ? E_INVALIDARG : MF_E_INVALIDREQUEST;

		hr = m_callee.EndFinalize( pAsyncResult );
		videoAssertf( SUCCEEDED( hr ), "AsyncEvent::Invoke - IMFAsyncResult was 0x%08lx", hr);

		rage::sysIpcSetEvent( m_asynchOpCompleteEvent );

		return hr;
	}

	HRESULT ASFSinkWriterWrapper::Initialize( LPCWSTR const outputURL )
	{
		HRESULT hr = IsInitialized() || IsWriting() ? MF_E_INVALIDREQUEST : outputURL == NULL ? E_INVALIDARG : MFCreateASFProfile( &m_profile );
		if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::Initialize - MFCreateASFProfile failed with 0x%08lx", hr) )
		{
			// Create an empty ContentInfo object
			hr = MFCreateASFContentInfo( &m_contentInfo );

			if( videoVerifyf( SUCCEEDED( hr ), "ASFSinkWriterWrapper::Initialize - MFCreateASFContentInfo failed with 0x%08lx", hr) )
			{
				m_outputURL = SysAllocString( outputURL );
				hr = m_outputURL ? S_OK : E_OUTOFMEMORY;
			}
		}

		return hr;
	}

	bool ASFSinkWriterWrapper::IsInitialized()
	{
		return m_outputURL != NULL;
	}

	void ASFSinkWriterWrapper::Cleanup()
	{
		EndWritingInternal();

		ReleaseMediaObject( m_profile );
		ReleaseMediaObject( m_contentInfo );

		ReleaseMediaObject( m_videoTransform );
		ReleaseMediaObject( m_audioTransform );
		ReleaseMediaObject( m_videoOutputType );
		ReleaseMediaObject( m_audioOutputType );

		if( m_outputURL )
		{
			SysFreeString( m_outputURL );
			m_outputURL = NULL;
		}
	}

	bool ASFSinkWriterWrapper::IsVideoStreamSetup() const
	{
		return m_videoTransform != NULL && m_videoOutputType != NULL;
	}

	bool ASFSinkWriterWrapper::IsAudioStreamSetup() const
	{
		return m_audioTransform != NULL && m_audioOutputType != NULL;
	}

	bool ASFSinkWriterWrapper::IsVideoStreamReadyForWriting() const
	{
		bool ready = false;

		if( IsVideoStreamSetup() )
		{
			DWORD inStreams(0);
			DWORD outStreams(0);

			HRESULT const c_hr = m_videoTransform->GetStreamCount( &inStreams, &outStreams );
			ready = SUCCEEDED( c_hr ) && inStreams > 0 && outStreams > 0;
		}

		return ready;
	}

	bool ASFSinkWriterWrapper::IsAudioStreamReadyForWriting() const
	{
		bool ready = false;

		if( IsAudioStreamSetup() )
		{
			DWORD inStreams(0);
			DWORD outStreams(0);

			HRESULT const c_hr = m_audioTransform->GetStreamCount( &inStreams, &outStreams );
			ready = SUCCEEDED( c_hr ) && inStreams > 0 && outStreams > 0;
		}

		return ready;
	}

	HRESULT ASFSinkWriterWrapper::BeginWritingInternal()
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( !IsWriting() && IsInitialized() )
		{
			videoAssertf( m_contentInfo, "ASFSinkWriterWrapper::Activate - No m_pContentInfo" );
			videoAssertf( m_profile, "ASFSinkWriterWrapper::Activate - No m_pProfile" );

			//Now we need to set global properties that will be set on the media sink
			IPropertyStore* globalProperties = NULL;
			hr = m_contentInfo ? m_contentInfo->GetEncodingConfigurationPropertyStore( 0, &globalProperties ) : E_FAIL;
			if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::Activate - m_pContentInfo->SetProfile failed with 0x%08lx", hr) )
			{
				//Auto adjust bitrate - This way we don't need to set up the leaky-bucket parameters ourselves.
				{
					PROPVARIANT var;
					InitPropVariantFromBoolean( TRUE, &var );
					hr = globalProperties->SetValue( MFPKEY_ASFMEDIASINK_AUTOADJUST_BITRATE, var );
					videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::BeginWritingActivation - Setting MFPKEY_ASFMEDIASINK_AUTOADJUST_BITRATE "
										"failed with 0x%08lx", hr);
					PropVariantClear( &var );
				}

				if( videoVerifyf( SUCCEEDED(hr),"BeginWritingActivation - Setting MFPKEY_ASFMEDIASINK_AUTOADJUST_BITRATE returned 0x%08lx", hr ) )
				{
					//Initialize with the profile;
					hr = m_profile ? m_contentInfo->SetProfile( m_profile ) : hr;
					if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::BeginWritingActivation - m_pContentInfo->SetProfile failed with 0x%08lx", hr) )
					{
						//Create the activation object for the  file sink
						hr = MFCreateASFMediaSinkActivate( m_outputURL, m_contentInfo, &m_mediaSinkActivate );
						videoAssertf( SUCCEEDED(hr), "ASFSinkWriterWrapper::BeginWritingActivation - MFCreateASFMediaSinkActivate failed with 0x%08lx", hr );
					}
				}
				
				ReleaseMediaObject( globalProperties );
			}
		}

		return hr;
	}

	void ASFSinkWriterWrapper::EndWritingInternal()
	{
		ReleaseMediaObject( m_mediaSinkActivate );

		MFShutdownObject( m_mediaSink );
		ReleaseMediaObject( m_mediaSink );

		MFShutdownObject( m_presentationClock );
		ReleaseMediaObject( m_presentationClock );
	}

	HRESULT ASFSinkWriterWrapper::WriteSampleInternal( DWORD dwStreamIndex, IMFSample *pSample, bool const drainPendingOutput )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{	
			hr = MF_E_INVALIDSTREAMNUMBER;
			if( IsValidStreamIndex( (WORD)dwStreamIndex ) )	
			{
				bool const c_isVideoStream = IsVideoStreamIndex( (WORD)dwStreamIndex );
				bool const c_isAudioStream = IsAudioStreamIndex( (WORD)dwStreamIndex );

				IMFTransform* targetTransform = c_isVideoStream ? m_videoTransform : c_isAudioStream ? m_audioTransform : NULL;
				if( targetTransform )
				{
					hr = pSample ? targetTransform->ProcessInput( 0, pSample, 0 ) : drainPendingOutput ? S_OK : E_INVALIDARG;
					if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::WriteSample - IMFTransform::ProcessInput failed with 0x%08lx", hr) )
					{
						IMFSample* outputSample = NULL;
						hr = ProcessOutput( &outputSample, targetTransform, 0 );
						if( SUCCEEDED( hr ) )
						{
							IMFStreamSink* pStreamSink = NULL;
							hr = m_mediaSink->GetStreamSinkByIndex( dwStreamIndex, &pStreamSink );
							if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::WriteSampleInternal - "
								"IMFMediaSink::GetStreamSinkByIndex failed with 0x%08lx", hr) )
							{
								hr = pStreamSink->ProcessSample( outputSample );
								//videoDisplayf( "ASFSinkWriterWrapper::WriteSampleInternal - IMFStreamSink::ProcessSample returned 0x%08lx", hr );
							}

							ReleaseMediaObject(pStreamSink);
						}
						else if( hr == MF_E_TRANSFORM_NEED_MORE_INPUT )
						{
							// If we are draining then we need to indicate we are pending more input, if not draining
							// we are doing a standard sample write and need to maintain the IMFSinkWriter interface and return S_OK
							hr = drainPendingOutput ? MF_E_TRANSFORM_NEED_MORE_INPUT : S_OK;
						}
						else
						{
							videoAssertf( false, "ASFSinkWriterWrapper::WriteSample - Failed processing output with 0x%08lx", hr );
						}

						ReleaseMediaObject( outputSample );
					}
				}
			}
		}

		return hr;
	}

	HRESULT ASFSinkWriterWrapper::PlaceMarkerInternal( DWORD dwStreamIndex, MFSTREAMSINK_MARKER_TYPE const markerType, 
		PROPVARIANT const * markerValue, PROPVARIANT const * contextValue )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{	
			hr = MF_E_INVALIDSTREAMNUMBER;
			if( IsValidStreamIndex( (WORD)dwStreamIndex ) )	
			{
				IMFStreamSink* pStreamSink = NULL;
				hr = m_mediaSink->GetStreamSinkByIndex( dwStreamIndex, &pStreamSink );
				if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::PlaceMarker - IMFMediaSink::GetStreamSinkByIndex failed with 0x%08lx", hr) )
				{
					hr = pStreamSink->PlaceMarker( markerType, markerValue, contextValue );
					//videoDisplayf( "ASFSinkWriterWrapper::WriteSampleInternal - IMFStreamSink::PlaceMarker returned 0x%08lx", hr );
				}

				ReleaseMediaObject(pStreamSink);
			}		
		}

		return hr;
	}

	HRESULT ASFSinkWriterWrapper::FlushStreamSinkInternal( DWORD dwStreamIndex )
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{	
			hr = MF_E_INVALIDSTREAMNUMBER;
			if( IsValidStreamIndex( (WORD)dwStreamIndex ) )	
			{
				IMFStreamSink* pStreamSink = NULL;
				hr = m_mediaSink->GetStreamSinkByIndex( dwStreamIndex, &pStreamSink );
				if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::FlushStreamSinkInternal - IMFMediaSink::GetStreamSinkByIndex failed with 0x%08lx", hr) )
				{
					hr = pStreamSink->Flush();
					//videoDisplayf( "ASFSinkWriterWrapper::FlushStreamSinkInternal - IMFStreamSink::Flush returned 0x%08lx", hr );
				}

				ReleaseMediaObject(pStreamSink);
			}		
		}

		return hr;
	}

	HRESULT ASFSinkWriterWrapper::FinalizeInternal()
	{
		HRESULT hr = MF_E_INVALIDREQUEST;

		if( IsWriting() )
		{
			// Assume success, this might not be a sink requiring finalizing
			hr = S_OK;

			IMFFinalizableMediaSink* finalizableSink = NULL;
			m_mediaSink->QueryInterface( IID_PPV_ARGS( &finalizableSink ) );

			if( finalizableSink )
			{
				sysIpcEvent finalizationEvent = sysIpcCreateEvent();
				hr = finalizationEvent ? S_OK : E_OUTOFMEMORY;

				if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::FinalizeInternal - Failed to create finalization event 0x%08lx", hr) )
				{
					FinalizationCallback* finalizationCallback = rage_new FinalizationCallback( finalizationEvent, *finalizableSink );
					hr = finalizationCallback ? S_OK : E_OUTOFMEMORY;

					if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::FinalizeInternal - Failed to create finalization callback 0x%08lx", hr) )
					{
						// Call finalize on ourselves
						hr = finalizableSink->BeginFinalize( finalizationCallback, NULL );
						if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::FinalizeInternal - "
							"IMFFinalizableMediaSink::BeginFinalize failed with 0x%08lx", hr) )
						{
							// Current implementation we are on our own non-UI thread, so let's just wait to be signaled
							sysIpcWaitEvent( finalizationEvent );
						}
						
					}

					// NOTE - We don't release the callback here as the finalizable sink has taken ownership of it and we don't want a double free
				}
				
				sysIpcDeleteEvent( finalizationEvent );
			}

			ReleaseMediaObject( finalizableSink );
		}

		return hr;
	}

	HRESULT ASFSinkWriterWrapper::FindClosestMatchingType( IMFMediaType*& out_matchingType, IMFMediaType* in_type, IMFTransform* transform )
	{
		HRESULT hr = in_type && transform && out_matchingType == NULL ? MF_E_INVALIDMEDIATYPE : E_INVALIDARG;

		GUID guidMajorType = GUID_NULL;
		hr =  in_type ? in_type->GetMajorType( &guidMajorType ) : hr;
		if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::AddStream - pTargetMediaType->GetMajorType failed with 0x%08lx", hr) )
		{
			bool const c_isVideoType = IsEqualGUID( guidMajorType, MFMediaType_Video ) == TRUE;

			//! Extract bitrate field to check for based on type
			GUID const &c_bitrateRate = c_isVideoType ? MF_MT_AVG_BITRATE : MF_MT_AUDIO_AVG_BYTES_PER_SECOND;
			u32 expectedBitrate = 0;
			hr = in_type->GetUINT32( c_bitrateRate, &expectedBitrate );
			bool const c_hasTargetBitrate = videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::SetInputMediaType - "
				"GetUINT32 (Bitrate) returned with 0x%08lx", hr );

			for( DWORD iType = 0; in_type && transform && out_matchingType == NULL; ++iType )
			{
				hr = transform->GetOutputAvailableType( 0, iType, &out_matchingType );
				videoDisplayf( "ASFSinkWriterWrapper::SetInputMediaType - IMFTransform::GetOutputAvailableType returned with 0x%08lx", hr );

				if( hr == MF_E_NO_MORE_TYPES )
				{
					break;
				}
				else
				{
					//! If we have a target bit-rate, then apply it
					hr = c_hasTargetBitrate ? out_matchingType->SetUINT32( c_bitrateRate , expectedBitrate ) : S_OK;
					videoDisplayf( "ASFSinkWriterWrapper::SetInputMediaType - SetUINT32 (MF_MT_AVG_BITRATE) returned with 0x%08lx", hr );

					if( c_isVideoType )
					{
						hr = AddPrivateVideoDataFromTransform( out_matchingType, transform );
						videoDisplayf( "ASFSinkWriterWrapper::SetInputMediaType - AddPrivateVideoDataFromTransform returned with 0x%08lx", hr );
					}

					hr = transform->SetOutputType( 0, out_matchingType, MFT_SET_TYPE_TEST_ONLY );
					videoDisplayf( "ASFSinkWriterWrapper::SetInputMediaType - IMFTransform::SetOutputType returned with 0x%08lx", hr );

					if( FAILED(hr) )
					{
						ReleaseMediaObject( out_matchingType );
					}
				}
			}
		}

		return hr;
	}

	HRESULT ASFSinkWriterWrapper::ProcessOutput( IMFSample** out_sample, IMFTransform* transform, DWORD const streamId )
	{
		HRESULT hr = out_sample == NULL ? E_POINTER : E_INVALIDARG;

		if( transform && out_sample && *out_sample == NULL )
		{
			DWORD dwStatus;

			MFT_OUTPUT_STREAM_INFO mftStreamInfo = { 0 };
			MFT_OUTPUT_DATA_BUFFER mftOutputData = { 0 };

			hr = transform->GetOutputStreamInfo( streamId, &mftStreamInfo );
			if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::ProcessOutput - IMFTransform::GetOutputStreamInfo failed with 0x%08lx", hr) )
			{
				IMFMediaBuffer* outputBuffer = NULL;
				hr = MFCreateMemoryBuffer( mftStreamInfo.cbSize, &outputBuffer );
				if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::ProcessOutput - MFCreateMemoryBuffer failed with 0x%08lx", hr) )
				{
					IMFSample* outputSample = NULL;
					hr = MFCreateSample( &outputSample );
					if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::ProcessOutput - MFCreateSample failed with 0x%08lx", hr) )
					{
						hr = outputSample->AddBuffer( outputBuffer );
						if( videoVerifyf( SUCCEEDED(hr), "ASFSinkWriterWrapper::ProcessOutput - IMFSample::AddBuffer failed with 0x%08lx", hr) )
						{
							// Set output data information
							mftOutputData.pSample = outputSample;
							mftOutputData.dwStreamID = streamId;

							hr = transform->ProcessOutput( 0, 1, &mftOutputData, &dwStatus );
							if( SUCCEEDED( hr ) )
							{
								outputSample->AddRef();
								*out_sample = outputSample;
							}
							else 
							{
								// MF_E_TRANSFORM_NEED_MORE_INPUT is still a valid return value
								videoAssertf( hr == MF_E_TRANSFORM_NEED_MORE_INPUT, 
									"ASFSinkWriterWrapper::ProcessOutput - IMFTransform::ProcessOutput failed with 0x%08lx", hr);
							}
						}
					}

					ReleaseMediaObject( outputSample );
				}

				ReleaseMediaObject( outputBuffer );
			}
		}

		return hr;
	}

#endif // RSG_PC

} // namespace rage

#endif // ( RSG_PC || RSG_DURANGO ) && !__RESOURCECOMPILER && !__RGSC_DLL
