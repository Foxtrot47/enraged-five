/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlaybackSettings.h
// PURPOSE : Common header for video playback settings
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_VIDEO_PLAYBACK_SETTINGS_H_
#define INC_VIDEO_PLAYBACK_SETTINGS_H_

#include "mediaencoder_params.h"

#define VIDEO_PLAYBACK_ENABLED		( ( 1 && RSG_PC ) || ( 1 && RSG_DURANGO ) || ( 1 && RSG_ORBIS ) )

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
#	define VIDEO_PLAYBACK_ONLY(x)  x
#else
#	define VIDEO_PLAYBACK_ONLY(x)
#endif

#define MAX_CONCURRENT_VIDEOS	(1)
#define MIN_VIDEO_DURATION_MS	(1000)

#define MAX_VIDEO_WIDTH ( MediaEncoderParams::GetMaxOutputWidth() )
#define MAX_VIDEO_HEIGHT ( MediaEncoderParams::GetMaxOutputHeight() )

#endif // INC_VIDEO_PLAYBACK_SETTINGS_H_
