#ifndef INC_MEDIAENCODER_BUFFER_INTERFACE_RAGE_H_
#define INC_MEDIAENCODER_BUFFER_INTERFACE_RAGE_H_

#include "mediaencoder_params.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

#include "streaming/streamingengine.h"

namespace rage
{
	static u8* MediaBufferBlob_Alloc(size_t size,size_t align)
	{
#if USE_FLEX_MEMORY_FOR_BUFFERS
		return static_cast<u8*>(MEMMANAGER.GetFlexAllocator()->Allocate(size, align));
#else
		(void)align;
		u8* ptr = rage_aligned_new(align) u8[size];
		return ptr;
#endif
	}

	static void MediaBufferBlob_Free(u8* ptr)
	{
		if (ptr)
		{
#if USE_FLEX_MEMORY_FOR_BUFFERS
			MEMMANAGER.GetFlexAllocator()->Free(ptr);
#else
			delete ptr;
#endif
		}
	}

	// it's orbis really, I'm just trying to get this to work
	class MediaBufferBlobInterfaceRage
	{
	public:
		MediaBufferBlobInterfaceRage()
			: m_bufferPtr( NULL )
			, m_size( 0 )
			, m_sizeUsed( 0 )
			, m_bufferAvailable( false )
		{
		}

		~MediaBufferBlobInterfaceRage() 
		{
			Shutdown();

			MediaBufferBlob_Free(m_bufferPtr);
			m_bufferPtr = NULL;
		};

		bool Initialize( size_t const sourceSize )
		{
			bool success = true;

			/// sooooooo temp. 
			if (!m_bufferPtr)
			{
				m_bufferPtr = MediaBufferBlob_Alloc(sourceSize, 16);
			}

			m_sizeUsed = 0;
			m_bufferAvailable = false;

			return success;
		}

		inline bool IsInitialized()
		{
			return m_size > 0;
		}

		void Shutdown()
		{
			m_size = 0;
			m_bufferAvailable = false;
		}

		inline bool Lock()
		{
			m_size = sizeof(m_bufferPtr);
			m_bufferAvailable = true;
			m_sizeUsed = 0;
			return true;
		}

		inline bool IsLocked() const { return m_size > 0; }

		inline void Unlock()
		{
			m_size = 0;
		}

		inline void SetSizeUsed( size_t const sizeUsed ) 
		{
			m_sizeUsed = sizeUsed;
		}

		inline u8* GetBytes() { return m_bufferAvailable ? m_bufferPtr : NULL; }
		inline u8 const* GetBytesReadOnly() const { return m_bufferAvailable && m_sizeUsed ? m_bufferPtr : NULL; }

		inline size_t GetSize() const
		{
			return m_size;
		}

	private:
		u8*				m_bufferPtr;
		size_t			m_size;
		size_t			m_sizeUsed;
		bool			m_bufferAvailable;
	};
}

#endif // USES_MEDIA_ENCODER && !__RESOURCECOMPILER && !__RGSC_DLL

#endif // INC_MEDIAENCODER_BUFFER_INTERFACE_RAGE_H_
