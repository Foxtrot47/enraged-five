/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : IImageLoader.h
// PURPOSE : Interface for loading images
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef IIMAGE_LOADER_H_
#define IIMAGE_LOADER_H_

#include "atl/hashstring.h"

namespace rage
{

class IImageLoader
{
public:
	virtual bool isImageRequested( atFinalHashString const& textureName ) const = 0;
	virtual bool isImageLoaded( atFinalHashString const& textureName ) const = 0;
	virtual bool hasImageRequestFailed( atFinalHashString const& textureName ) const = 0;

    virtual bool canRequestImages() const = 0;

	virtual bool requestImage( char const * const filename, atFinalHashString const& textureName, u32 const downscale ) = 0;
	virtual void releaseImage( atFinalHashString const& textureName ) = 0;
	virtual void releaseAllImages() = 0;
};

}

#endif // IIMAGE_LOADER_H_
