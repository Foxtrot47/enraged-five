/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : sinkwriter.h
// PURPOSE : Wrapper for IMFSinkWriter, but ASF implimentation for Vista
//
// AUTHOR  : andrew.keeble / J.Strain
//
// NOTE : When we fold out support for Vista it may be worth converting this class to be an MP4/AAC
//		  equivalent replacement. More work to maintain, but puts us less at the mercy of Microsoft improvements
//		  not being exposed upstream (e.g. even the IMFSinkWriter interface on 7 hides MP4 features from us)
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if ( RSG_PC || RSG_DURANGO ) && !__RESOURCECOMPILER && !__RGSC_DLL

#ifndef _SINKWRITER_H_
#define _SINKWRITER_H_

// framework
#include "media_common.h"

// Windows
#pragma warning(push)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <guiddef.h>
#include <mfobjects.h> // THIS FUCKING FILE IS POPPING ONE WARNING STACK TOO MANY, AND SETTING WARNING 4200 to error. HENCE THE DOUBLE TAP.
#include <Mfidl.h>
#include <mfreadwrite.h>
#include <winerror.h>
#include <wmcontainer.h>
#pragma warning(pop)
#pragma warning(pop)

// rage
#include "system/ipc.h"

namespace rage
{
	class SinkWriterCreator
	{
	public:	
		static HRESULT CreateSinkWriter( LPCWSTR pwszOutputURL, IMFByteStream *pByteStream, IMFAttributes *pAttributes, IMFSinkWriter **ppSinkWriter );
	};

#if RSG_PC

	class ASFSinkWriterWrapper : public IMFSinkWriter
	{
	public: // declarations and variables
		enum eEncodingMode 
		{
			ENCODING_INVALID	= 0x0,
			ENCODING_CBR		= 0x1,
			ENCODING_VBR		= 0x2,
		};

	public: // methods
		ASFSinkWriterWrapper();
		virtual ~ASFSinkWriterWrapper();

		// IUnknown Interface
		virtual ULONG	STDMETHODCALLTYPE AddRef();
		virtual HRESULT STDMETHODCALLTYPE QueryInterface( REFIID riid, _COM_Outptr_ void **ppvObject );
		virtual ULONG	STDMETHODCALLTYPE Release();

		// IMFSinkWriter Interface
		virtual HRESULT STDMETHODCALLTYPE AddStream( IMFMediaType *pTargetMediaType, DWORD *pdwStreamIndex );
		virtual HRESULT STDMETHODCALLTYPE BeginWriting();
		virtual HRESULT STDMETHODCALLTYPE Finalize();
		virtual HRESULT STDMETHODCALLTYPE Flush( DWORD dwStreamIndex );
		virtual HRESULT STDMETHODCALLTYPE GetServiceForStream( DWORD dwStreamIndex, REFGUID guidService, REFIID riid, LPVOID *ppvObject );
		virtual HRESULT STDMETHODCALLTYPE GetStatistics( DWORD dwStreamIndex, MF_SINK_WRITER_STATISTICS *pStats );
		virtual HRESULT STDMETHODCALLTYPE NotifyEndOfSegment( DWORD dwStreamIndex );
		virtual HRESULT STDMETHODCALLTYPE PlaceMarker( DWORD dwStreamIndex, LPVOID pvContext );
		virtual HRESULT STDMETHODCALLTYPE SendStreamTick( DWORD dwStreamIndex, LONGLONG llTimestamp );
		virtual HRESULT STDMETHODCALLTYPE SetInputMediaType( DWORD dwStreamIndex, IMFMediaType *pInputMediaType, IMFAttributes *pEncodingParameters );
		virtual HRESULT STDMETHODCALLTYPE WriteSample( DWORD dwStreamIndex, IMFSample *pSample );
		
		// Our custom interface extensions
		static bool IsPotentiallyValidStreamNumber( WORD const streamNumber ) { return streamNumber > 0 && streamNumber <= 127; }
		static bool IsPotentiallyValidStreamIndex( WORD const streamIndex ) { return streamIndex >= 0 && streamIndex < 127; }

	private: // declarations and variables
		friend class SinkWriterCreator;

		//! Internal class for handling finalization events
		class FinalizationCallback : public IMFAsyncCallback
		{
		public:
			FinalizationCallback( sysIpcEvent& asynchOpCompleteEvent, IMFFinalizableMediaSink& callee );
			virtual  ~FinalizationCallback();

			// IUnknown Interface
			virtual ULONG	STDMETHODCALLTYPE AddRef();
			virtual HRESULT STDMETHODCALLTYPE QueryInterface( REFIID riid, _COM_Outptr_ void **ppvObject );
			virtual ULONG	STDMETHODCALLTYPE Release();

			// IMFAsyncCallback Interface
			virtual HRESULT STDMETHODCALLTYPE GetParameters( DWORD* pdwFlags, DWORD* pdwQueue );
			virtual HRESULT STDMETHODCALLTYPE Invoke( IMFAsyncResult* pAsyncResult );

		private:
			sysIpcEvent&				m_asynchOpCompleteEvent;
			IMFFinalizableMediaSink&	m_callee;
			u32							m_refCount;		
		};

		IMFASFProfile*			m_profile;
		IMFASFContentInfo*		m_contentInfo;

		IMFTransform*			m_videoTransform;
		IMFTransform*			m_audioTransform;
		IMFMediaType*			m_videoOutputType;
		IMFMediaType*			m_audioOutputType;

		IMFActivate*			m_mediaSinkActivate;
		IMFMediaSink*			m_mediaSink;
		IMFPresentationClock*	m_presentationClock;

		BSTR					m_outputURL;
		u32						m_refCount;
		eEncodingMode			m_encodingMode;

	private: // methods

		// Our custom interface extensions
		HRESULT Initialize( LPCWSTR const pwszOutputURL );
		bool IsInitialized();
		void Cleanup();

		bool IsVideoStreamSetup() const;
		bool IsAudioStreamSetup() const;
		bool AreBothStreamsSetup() const { return IsVideoStreamSetup() && IsAudioStreamSetup(); }

		bool IsVideoStreamReadyForWriting() const;
		bool IsAudioStreamReadyForWriting() const;
		bool AreBothStreamsReadyForWriting() const { return IsVideoStreamReadyForWriting() && IsAudioStreamReadyForWriting(); }

		bool IsValidStreamNumber( WORD const streamNumber ) const { return streamNumber > 0 && streamNumber <= 2; }
		bool IsValidStreamIndex( WORD const streamIndex ) const { return streamIndex >= 0 && streamIndex <= 1; }

		static bool IsVideoStreamIndex( WORD const streamIndex ) { return streamIndex == 0; }
		static bool IsAudioStreamIndex( WORD const streamIndex ) { return streamIndex == 1; }

		bool IsWriting() const { return m_mediaSink != NULL; }

		HRESULT BeginWritingInternal();
		void EndWritingInternal();

		HRESULT WriteSampleInternal( DWORD dwStreamIndex, IMFSample *pSample, bool const drainPendingOutput );

		HRESULT PlaceMarkerInternal( DWORD dwStreamIndex, MFSTREAMSINK_MARKER_TYPE const markerType, 
			PROPVARIANT const * markerValue, PROPVARIANT const * contextValue );

		HRESULT FlushStreamSinkInternal( DWORD dwStreamIndex );

		HRESULT FinalizeInternal();

		static HRESULT FindClosestMatchingType( IMFMediaType*& out_matchingType, IMFMediaType* in_type, IMFTransform* transform );
		static HRESULT ProcessOutput( IMFSample** out_sample, IMFTransform* transform, DWORD const streamId );
	};

#endif // RSG_PC

} // namespace rage

#endif // _SINKWRITER_H_

#endif // ( RSG_PC || RSG_DURANGO ) && !__RESOURCECOMPILER && !__RGSC_DLL
