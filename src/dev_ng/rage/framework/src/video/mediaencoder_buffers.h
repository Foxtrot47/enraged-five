/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_buffers.h
// PURPOSE : Class for storing buffer data for encoding audio/video
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_BUFFERS_H_
#define INC_MEDIAENCODER_BUFFERS_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_params.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "atl/queue.h"
#include "audiohardware/device.h"
#include "audiohardware/mixer.h"
#include "grcore/stateblock.h"
#include "media_buffer.h"
#include "mediaencoder_params.h"
#include "mediaencoder_bufferpool.h"
#include "system/ipc.h"
#include "system/messagequeue.h"

namespace rage
{
	// Forward declaration
	class grcRenderTarget;
	class grcTexture;
	class grcViewport;

	//! Collection of media buffers for encoding
	class MediaEncoderBuffers
	{
	public:
		MediaEncoderBuffers()
			: m_bufferPool()
			, m_videoBufferQueue()
			, m_audioBufferQueue()			
			, m_synchronized( false )
		{
		}

		~MediaEncoderBuffers()
		{
			Shutdown();
		}

		bool Initialize( MediaEncoderParams const& captureParameters );
		bool IsInitialized() const;
		void Shutdown();

		bool PushVideoFrame( grcTexture const& source, float const frameDuration );
#if defined(VIDEO_ENCODE_BUFFERS_AS_TEXTURES) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES
		bool PushRawVideoFrame( grcTexture& source, float const frameDuration );
#endif
		void PushAudioBuffer( s16 const* audioBuffer, size_t const sampleCount );

		// Game audio is built into larger buffers, so we may have a partial buffer to flush
		// if we are killing encoding
		void FlushPartialAudioBuffer();

		void PushEOFBuffer();
		bool IsEOFBuffer( MediaBufferBase const * const buffer ) const;

		bool PopVideoBuffer( MediaBufferBase*& out_buffer );
		bool PopAudioBuffer( MediaBufferBase*& out_buffer );
		void ReleaseBuffer( MediaBufferBase*& buffer );

		void DiscardAllBuffers();

		inline bool HasVideoBuffers() const { return !m_videoBufferQueue.IsEmpty(); }
		inline bool HasAudioBuffers() const { return !m_audioBufferQueue.IsEmpty(); }

		void SetSynchronized( bool synchronized ) { m_synchronized = synchronized; }

	private: // declarations and variables

		enum eEncoderCommandBuffers
		{
			BUFFER_EOF = 1,
			MAX_COMMAND_BUFFERS
		};

		MediaEncoderBufferPool								m_bufferPool;

		typedef sysMessageQueue< MediaBufferVideoEncode*, MediaEncoderBufferPool::sc_videoBufferCount, true > VideoQueue;
		VideoQueue		m_videoBufferQueue;

		typedef sysMessageQueue< MediaBufferAudioEncode*, MediaEncoderBufferPool::sc_audioBufferCount, true > AudioQueue;
		AudioQueue		m_audioBufferQueue;

		sysCriticalSectionToken								m_framePushCsToken;
		bool												m_synchronized;

	private: // methods
		void FlushGivenAudioBuffer( MediaBufferAudioEncode* audioBuffer );

		bool IsVideoFrameAvailable() const;
		bool IsAudioBufferAvailable() const;

		bool WaitForVideoFrame() const;
		bool WaitForAudioBuffer() const;

#if defined(VIDEO_ENCODE_BUFFERS_AS_BLOBS) && VIDEO_ENCODE_BUFFERS_AS_BLOBS
		bool PushVideoFrameBlob( u8 const* source, size_t const padding, u64 const durationNs );
#elif defined(VIDEO_ENCODE_BUFFERS_AS_TEXTURES) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES
		bool PushVideoFrameRawTexture( grcTextureObject* texture, u64 const durationNs );
#endif

	};
}

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER

#endif // INC_MEDIAENCODER_BUFFERS_H_
