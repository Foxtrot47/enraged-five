/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediadecoder_win.cpp
// PURPOSE : Class for decoding video/audio streams. Common functionality
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediadecoder.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

// rage
#include "audioengine/engine.h"
#include "audioengine/soundmanager.h"
#include "audiohardware/ringbuffer.h"
#include "vector/color32.h"

// framework
#include "video/media_common.h"
#include "video/video_channel.h"

namespace rage
{
	bool MediaDecoder::Initialize( const char * const path, bool const captureThumbnailThenWait, bool const noAudio )
	{
		bool success = false;

		SYS_CS_SYNC( GetVideoBufferCSToken() );

		if( Verifyf( !IsInitialized(), "MediaDecoder::Initialize - Double initialization" ) )
		{
			ResetError();

			success = InitializePlatformSpecific( path, captureThumbnailThenWait );
			SetAudioDisabled( noAudio );
		}

		return success;
	}

	bool MediaDecoder::IsInitialized()
	{
		SYS_CS_SYNC( GetVideoBufferCSToken() );

		return GetDecoderState() != DECODER_INVALID
#if defined(THREADED_DECODER) && THREADED_DECODER
			&& m_decoderThread != NULL;
#else
			;
#endif
	}

	void MediaDecoder::Shutdown()
	{
		SYS_CS_SYNC( GetVideoBufferCSToken() );

		ShutdownPlatformSpecific();
		CleanupPlatformSpecific();

		Assertf( GetDecoderState() == DECODER_INVALID, "MediaDecoder::Shutdown - Decoder shut down but still has state" );
	}

	void MediaDecoder::MarkAsActive()
	{
		SetDecoderState( DECODER_ACTIVE );
	}

	bool MediaDecoder::Seek( u64 const timeNs )
	{
		bool success = false;

		if( ReadyToOrDecoding() )
		{
			SetTargetSeekTime( Clamp<u64>( timeNs, 0, GetFormatInfo().m_durationNs ) );
			SetPlayState( GetPlayState() == PLAYSTATE_PLAYING || IsAtEnd() ? PLAYSTATE_SEEK_THEN_PLAY : PLAYSTATE_SEEK_THEN_PAUSE );
			success = true;
		}

		return success;
	}

	bool MediaDecoder::Play()
	{
		bool success = false;

		if( IsInitialized() )
		{
			SetPlayState( PLAYSTATE_PLAYING );
			success = true;
		}

		return success;
	}

	bool MediaDecoder::IsPaused() const
	{
		return ( GetPlayState() == PLAYSTATE_PAUSED || GetPlayState() == PLAYSTATE_SEEK_THEN_PAUSE || GetPendingPlayState() == PLAYSTATE_PAUSED );
	}

	bool MediaDecoder::IsPendingPlay() const
	{
		ePlayState const c_pendingState = GetPendingPlayState();
		return c_pendingState == PLAYSTATE_SEEK_THEN_PLAY || c_pendingState == PLAYSTATE_PLAYING;
	}

	bool MediaDecoder::IsPendingPause() const
	{
		ePlayState const c_pendingState = GetPendingPlayState();
		return c_pendingState == PLAYSTATE_SEEK_THEN_PAUSE || c_pendingState == PLAYSTATE_PAUSED;
	}

    bool MediaDecoder::IsAtStart() const
    {
        ePlayState const c_currentState = GetPlayState();
        ePlayState const c_pendingState = GetPendingPlayState();
        u64 const c_currentTimeMs = GetCurrentTimeMs();
        u64 const c_seekTimeMs = GetSeekTimeMs();

        bool c_atStart = ( c_pendingState != PLAYSTATE_EOF && c_currentState != PLAYSTATE_EOF ) &&
            ( ( c_pendingState != PLAYSTATE_PLAYING && c_currentState != PLAYSTATE_PLAYING && c_currentTimeMs == 0 ) || 
            ( ( c_pendingState == PLAYSTATE_SEEK_THEN_PAUSE || c_currentState == PLAYSTATE_SEEK_THEN_PAUSE ) && c_seekTimeMs == 0 ) );

        return c_atStart;
    }

	bool MediaDecoder::IsAtEnd() const
	{
		return GetPlayState() == PLAYSTATE_EOF;
	}

	bool MediaDecoder::Pause()
	{
		bool success = false;

		if( ReadyToOrDecoding() )
		{
			SetPlayState( PLAYSTATE_PAUSED );
			g_AudioEngine.GetSoundManager().Pause( MEDIA_DECODER_AUDIO_TIMER_ID );	
			success = true;
		}

		return success;
	}

	void MediaDecoder::PopulateTextureFrame( s64& inout_textureIdentifier, grcTexture* yTexture, grcTexture* uTexture, grcTexture* vTexture )
	{
		SYS_CS_SYNC( GetVideoBufferCSToken() );

		MediaDecoderDisplayFrame const& frame = GetDisplayFrame();
		s64 const c_frameIdentifier = frame.GetId();
		if( yTexture && uTexture && vTexture && frame.IsInitialized() && inout_textureIdentifier != c_frameIdentifier )
		{
			frame.PopulateTextureFrame( yTexture, uTexture, vTexture );
			inout_textureIdentifier = c_frameIdentifier;
		}
	}

#if defined(THREADED_DECODER) && THREADED_DECODER

	void MediaDecoder::ThreadData::Initialize( const char * const path, bool const captureThumbnail )
	{
		Assertf( m_wakeThreadEvent == NULL, "ThreadData::Initialize - Thread event not cleaend up correctly?" );
		m_wakeThreadEvent = sysIpcCreateEvent();

		m_targetUri = path;
		m_captureThumbnail = captureThumbnail;
	}

	void MediaDecoder::ThreadData::Cleanup()
	{
		Assertf( !m_displayFrame.IsInitialized(), "ThreadData::Cleanup - Display frame not cleaned up?" );
		Assertf( m_audioBuffer == NULL, "ThreadData::Cleanup - Audio buffer leaked?" );
		Assertf( m_pendingVideoSample == NULL, "ThreadData::Cleanup - Pending sample leaked?" );

		m_formatInfo.Reset();
		m_targetUri.Clear();
		m_decoderState = DECODER_INVALID;
		m_playState = PLAYSTATE_INVALID;
		m_pendingPlayState = PLAYSTATE_INVALID;
		m_currentBackbufferPlaybackTimeOneHNs = 0;
		m_noAudio = m_captureThumbnail = false;

		if( m_wakeThreadEvent )
		{
			sysIpcDeleteEvent( m_wakeThreadEvent );
			m_wakeThreadEvent = NULL;
		}
	}

	void MediaDecoder::ThreadData::SetDecodeState( eDecodeState const decodeState )
	{ 
		m_decoderState = decodeState; 
		sysIpcSetEvent( m_wakeThreadEvent );
	}

	void MediaDecoder::ThreadData::SetPendingPlayState( ePlayState const pendingState )
	{
		m_pendingPlayState = m_pendingPlayState != PLAYSTATE_CLEANUP ? pendingState : m_pendingPlayState;

		// Wake the thread on these conditions only, other conditions we'd have already been busy spinning
		if( m_pendingPlayState == PLAYSTATE_CLEANUP || m_playState == PLAYSTATE_EOF || m_playState == PLAYSTATE_PAUSED )
		{
			sysIpcSetEvent( m_wakeThreadEvent );
		}
	}

#endif // defined(THREADED_DECODER) && THREADED_DECODER

} // rage

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
