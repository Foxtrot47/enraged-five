/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : media_transcoding_allocator.cpp
// PURPOSE : Class for wrapping encoder/decoder allocations
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if ( RSG_DURANGO && 1 ) && !__RESOURCECOMPILER && !__RGSC_DLL

#include "media_transcoding_allocator.h"

#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>

#pragma warning(pop)

// rage
#include "diag/tracker.h"
#include "system/memory.h"
#include "system/memvisualize.h"
#include "system/externalallocator.h"
#include "system/new.h"
#include "system/xtl.h"

// framework
#include "video_channel.h"

namespace rage
{
	MediaTranscodingMemorySystem MEDIA_MEMORY_WRAPPER;

#if RSG_DURANGO

	MediaTranscodingMemorySystem* MediaTranscodingMemorySystem::ms_XMemWrapperInstance = NULL;

#endif

	MediaTranscodingMemorySystem::MediaTranscodingMemorySystem()
		: m_smallObjectMemoryBlock( NULL )
		, m_smallObjectWorkspace( NULL )
		, m_smallObjectAllocator( NULL )
		, m_memoryBlock( NULL )
		, m_workspace( NULL )
		, m_allocator( NULL)
        , m_decodingAllocator( NULL )
        , m_decodingRefCount( 0 )
	{

	}

	MediaTranscodingMemorySystem::~MediaTranscodingMemorySystem()
	{
		Shutdown();
	}

#if defined(USE_MMF_DECODE_HOOK_ONLY) && USE_MMF_DECODE_HOOK_ONLY

    void MediaTranscodingMemorySystem::Init( sysMemAllocator* decodingAllocator )
    {
        m_decodingAllocator = decodingAllocator;
        m_decodingRefCount = 0;
    }

#else

	void MediaTranscodingMemorySystem::Init( size_t const smallObjMemSize, size_t const maxSmallObjPointers, size_t const memSize, size_t const maxPointers, sysMemAllocator* decodingAllocator )
	{
		videoAssert( !isInitialized() );

		m_smallObjectWorkspace = sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_GAME_VIRTUAL )->Allocate( COMPUTE_WORKSPACE_SIZE( maxSmallObjPointers ), 0 );
		videoAssert( m_smallObjectWorkspace );

		m_smallObjectMemoryBlock = sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_GAME_VIRTUAL )->Allocate( smallObjMemSize, 4 );	
		videoAssert( m_smallObjectMemoryBlock );

		m_smallObjectAllocator = rage_new sysMemExternalAllocator( smallObjMemSize, m_smallObjectMemoryBlock, maxSmallObjPointers, m_smallObjectWorkspace );
		videoAssert( m_smallObjectAllocator );

		m_workspace = sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_GAME_VIRTUAL )->Allocate( COMPUTE_WORKSPACE_SIZE( maxPointers ), 0 );
		videoAssert( m_workspace );

		m_memoryBlock = sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_GAME_VIRTUAL )->Allocate( memSize, 4 );	
		videoAssert( m_memoryBlock );

		m_allocator = rage_new sysMemExternalAllocator( memSize, m_memoryBlock, maxPointers, m_workspace );
		videoAssert( m_allocator );

#if RAGE_TRACKING

        diagTracker* t = diagTracker::GetCurrent();
        if( t && sysMemVisualize::GetInstance().HasVideoTranscoding() )
        {
            t->InitHeap( "Media Transcoding (SmallObjects)", m_smallObjectMemoryBlock, smallObjMemSize );
            t->InitHeap( "Media Transcoding", m_memoryBlock, memSize );
        }

#endif // RAGE_TRACKING

        m_decodingAllocator = decodingAllocator;
        m_decodingRefCount = 0;
	}

#endif // defined(USE_MMF_DECODE_HOOK_ONLY) && USE_MMF_DECODE_HOOK_ONLY

	void MediaTranscodingMemorySystem::Shutdown()
	{
        m_decodingAllocator = NULL;
        m_decodingRefCount = 0;

		if( m_allocator )
		{
			delete m_allocator;
			m_allocator = NULL;
		}

		if( m_smallObjectAllocator )
		{
			delete m_smallObjectAllocator;
			m_smallObjectAllocator = NULL;
		}

		if( m_workspace )
		{
			delete m_workspace;
			m_workspace = NULL;
		}

		if( m_smallObjectWorkspace  )
		{
			delete m_smallObjectWorkspace;
			m_smallObjectWorkspace = NULL;
		}

		if( m_memoryBlock )
		{
			sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_GAME_VIRTUAL )->Free( m_memoryBlock );
		}

		if( m_smallObjectMemoryBlock )
		{
			sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_GAME_VIRTUAL )->Free( m_smallObjectMemoryBlock );
		}
	}

	void* MediaTranscodingMemorySystem::allocate( size_t const size, size_t const align )
	{
		return allocateInternal( size, align );
	}

	void  MediaTranscodingMemorySystem::free( void *address )
	{
		freeInternal( address );
	}

    void MediaTranscodingMemorySystem::setDecoding( bool const isDecoding )
    { 
        m_decodingRefCount += m_decodingAllocator != NULL && isDecoding ? 1 : -1; 
        Assertf( m_decodingRefCount >= 0, "MediaTranscodingMemorySystem::setDecoding - Negative ref count. Too many releases?" );
    }

    bool MediaTranscodingMemorySystem::isDecoding() const
    {
        return m_decodingRefCount > 0;
    }

#if RSG_DURANGO

	void MediaTranscodingMemorySystem::SetXMemInstance( MediaTranscodingMemorySystem* memoryWrapper )
	{
		ms_XMemWrapperInstance = memoryWrapper;
	}

	void* MediaTranscodingMemorySystem::sysMemXMemAllocHook( size_t sizeBytes, u64 attributes )
	{
		void* result = ms_XMemWrapperInstance ? ms_XMemWrapperInstance->mmfAllocInternal( sizeBytes, attributes ) : XMemAllocDefault(sizeBytes, attributes);
		return result;
	}

	void MediaTranscodingMemorySystem::sysMemXMemFreeHook( void *address, u64 attributes )
	{
		if( ms_XMemWrapperInstance )
		{
			ms_XMemWrapperInstance->mmfFreeInternal( address, attributes );
		}
		else
		{
			XMemFreeDefault( address, attributes );
		}
	}

#endif // RSG_DURANGO

	void* MediaTranscodingMemorySystem::allocateInternal( size_t const size, size_t const align )
	{
		USE_MEMBUCKET( MEMBUCKET_UI );
		MEM_USE_USERDATA( MEMUSERDATA_VIDEO_TRANSCODING );

        sysMemAllocator * allocatorToUse = isDecoding() ? m_decodingAllocator : size <= ( 4 * 1024 ) ? m_smallObjectAllocator : m_allocator;
		void * address = allocatorToUse ? allocatorToUse->Allocate( size, align ) : NULL;
		return address;
	}

	bool MediaTranscodingMemorySystem::freeInternal( void *address )
	{
        bool freed = false;

		USE_MEMBUCKET( MEMBUCKET_UI );
		MEM_USE_USERDATA( MEMUSERDATA_VIDEO_TRANSCODING );

        bool const c_fromDecoding = m_decodingAllocator && m_decodingAllocator->IsValidPointer( address );
        bool const c_fromSmall = m_smallObjectAllocator ? m_smallObjectAllocator->IsValidPointer( address ) : false;
        bool const c_fromNormal = m_allocator ? m_allocator->IsValidPointer( address ) : false;

        sysMemAllocator * allocatorToUse = c_fromDecoding ? m_decodingAllocator : c_fromSmall ? m_smallObjectAllocator : c_fromNormal ? m_allocator : NULL;
        if( allocatorToUse )
        {
            allocatorToUse->Free( address );
            freed = true;
        }

        return freed;
	}

#if RSG_DURANGO

	void* MediaTranscodingMemorySystem::mmfAllocInternal( size_t const sizeBytes, u64 const attributes )
	{
		XALLOC_ATTRIBUTES xAttribs;
		xAttribs.dwAttributes = attributes;

		size_t const c_alignment = sysMemXMemCalculateAlignment( xAttribs.s.dwAlignment );

		void * address = allocateInternal( sizeBytes, c_alignment );
        address = address == NULL ? XMemAllocDefault(sizeBytes, attributes) : address;
		return address;
	}

	void MediaTranscodingMemorySystem::mmfFreeInternal( void* address, u64 const attributes )
	{
		XALLOC_ATTRIBUTES xAttribs;
		xAttribs.dwAttributes = attributes;

		if( !freeInternal( address ) )
        {
            XMemFreeDefault( address, attributes );
        }
	}

#endif // RSG_DURANGO

} // namespace rage

#endif // (RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL
