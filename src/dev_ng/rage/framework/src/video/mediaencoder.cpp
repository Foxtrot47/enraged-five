/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder.cpp
// PURPOSE : Class for encoding video/audio streams. Common functionality.
// NOTES   : Currently Xb1 / PC only.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediaencoder.h"

#if ( USES_MEDIA_ENCODER ) && !__RESOURCECOMPILER

// rage
#include "audiohardware/driver.h"
#include "file/asset.h"
#include "grcore/device.h"
#include "grcore/texture.h"
#include "system/timer.h"

// framework
#include "media_common.h"
#include "video_channel.h"

namespace rage
{

#if defined( USE_DUMMY_VIDEO_DATA )
	rage::grcTexture* MediaEncoder::sc_dummyVideoTexture	= NULL;
#endif

#if defined( USE_DUMMY_AUDIO_DATA )
	rage::u8 const* MediaEncoder::sc_dummyAudioBuffer		= NULL;
	u32 const MediaEncoder::sc_dummyAudioBufferSize			= rage::MediaCommon::sc_gameAudioBufferSize;
	u32 const MediaEncoder::sc_dummyAudioBufferSampleCount	= rage::MediaCommon::sc_sampleCountPerGameBuffer;
	u32 const MediaEncoder::sc_dummyAudioBufferChannelCount	= rage::MediaCommon::sc_audioChannels;
#endif

#if defined(LOG_VIDEO_FRAME_PUSH)
	double	MediaEncoder::ms_totalFramePushedTime			= 0.0;
	u64		MediaEncoder::ms_totalFramesPushed				= 0;
#endif

#if defined(LOG_VIDEO_FRAME_SUBMISSION)
	double	MediaEncoder::ms_totalFrameSubmitTime			= 0.0;
	u64		MediaEncoder::ms_totalFramesSubmitted			= 0;
#endif

	bool MediaEncoder::SampleWriteMonitor::ShouldAbandonLoop() const
	{
		return m_threadData.HasError();
	}

	bool MediaEncoder::ThreadData::IsAudioFrameCaptureAllowed() const
	{
		u64 const c_currentTimeNs = MediaTimer::GetConstantTimeNs();

		return m_buffers.IsInitialized() && !HasError() &&
			( m_threadMessaging & ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN ) == 0 &&
			( !m_killFlag || ( m_killFlag && c_currentTimeNs < m_killTimeNs ) );
	}

	//----------------------------------------------------------------------
	//	MediaEncoder
	//----------------------------------------------------------------------
	MediaEncoder::MediaEncoder() 
		: m_threadData()
		, m_videoWrapper( m_threadData, true )
		, m_audioWrapper( m_threadData, false )
	{
		
	}

	MediaEncoder::~MediaEncoder()
	{
		if( IsCapturing() )
		{
			// Flag it as cancelled since if we are still capturing during shutdown likely we have an invalid file to be cleaned up
			StopCapture( true ); 
		}
	}

	bool MediaEncoder::StartCapture( const char * const filename, const char * const path,
		bool const startPaused, bool const synchronized, MediaEncoderParams const& captureParameters )
	{
		bool success = false;

		videoAssertf( m_videoWrapper.m_threadId == NULL && m_audioWrapper.m_threadId == NULL , "MediaEncoderWindows::Initialize - Encoder already capturing!" );
		videoAssertf( filename, "MediaEncoderWindows::Initialize - Null file name!" );
		videoAssertf( path, "MediaEncoderWindows::Initialize - Null path!" );
		if( m_videoWrapper.m_threadId == NULL && m_audioWrapper.m_threadId == NULL && filename && path )
		{
			m_threadData.Reset();
			m_threadData.ResetError();
			m_threadData.m_pauseFlag = startPaused;
			m_threadData.m_buffers.SetSynchronized( synchronized );
#if !RSG_ORBIS
			if( videoVerifyf( GenerateTargetUri( filename, path, captureParameters, m_threadData.m_targetUri ), 
				"MediaEncoderWindows::StartCapture - Unable to generate URI for %s/%s. Longer than MAX_PATH, perhaps?", filename, path ) )
#endif
			{
#if !RSG_ORBIS
				// Ensure our directory exists, as the Media Foundation API doesn't!
				ASSET.CreateLeadingPath( m_threadData.m_targetUri );
#endif
				m_threadData.m_encodeParams = captureParameters;

				m_audioWrapper.m_threadId = sysIpcCreateThread( &EncoderThreadFunc, &m_audioWrapper, sysIpcMinThreadStackSize, 
					PRIO_BELOW_NORMAL, "MediaEncoderThreadAudio", 0, "MediaEncoderThreadAudio" );

				m_videoWrapper.m_threadId = sysIpcCreateThread( &EncoderThreadFunc, &m_videoWrapper, sysIpcMinThreadStackSize, 
					PRIO_BELOW_NORMAL, "MediaEncoderThreadVideo", 0, "MediaEncoderThreadVideo" );

				success = IsCapturing();
#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
				if( success )
				{
					audDriver::StartAudioCapture( *this, startPaused );
				}
#endif
			};

		}

		return success;
	}

	bool MediaEncoder::IsCapturing() const
	{
		return m_videoWrapper.m_threadId != NULL && m_audioWrapper.m_threadId != NULL;
	}

	bool MediaEncoder::IsPaused() const
	{
		return m_threadData.m_pauseFlag;
	}

	void MediaEncoder::PauseCapture()
	{
		if( !IsPaused() && IsCapturing() )
		{
			audDriver::GetMixer()->m_ShouldStopFixedFrameRender = true;
			m_threadData.OnPauseCapture();
		}
	}

	void MediaEncoder::ResumeCapture()
	{
		if( IsPaused() && IsCapturing() )
		{
			audDriver::GetMixer()->m_ShouldStartFixedFrameRender = true;
			m_threadData.m_pauseFlag = false;
		}
	}

	bool MediaEncoder::StopCapture( bool const cancelled )
	{
		bool success = false;
		
		if( IsCapturing() )
		{
			m_threadData.OnStopCapture( cancelled );
			m_threadData.m_buffers.PushEOFBuffer();
#if defined(AUDIO_CAPTURE_ENABLED) && AUDIO_CAPTURE_ENABLED
			audDriver::StopAudioCapture();
#endif
			sysIpcWaitThreadExit( m_audioWrapper.m_threadId );
			sysIpcWaitThreadExit( m_videoWrapper.m_threadId );

			m_videoWrapper.m_threadId = NULL;
			m_audioWrapper.m_threadId = NULL;

			success = true;
			if( cancelled )
			{
				OnRecordingCancelled();
			}

			m_threadData.Reset();
		}
		else
		{
			videoDisplayf( "MediaEncoderWindows::StopCapture - Stopping capture when encoder is not capturing. Check logs for previous failure notifications..." );
		}

		return success;
	}

	void MediaEncoder::PushVideoFrame( grcTexture const& source, float const frameDuration )
	{
		if( IsCapturing() && m_threadData.IsVideoFrameCaptureAllowed() )
		{
#if defined(LOG_VIDEO_FRAME_PUSH)
			utimer_t videoPushFrame = sysTimer::GetTicks();
			bool const wasFramePushed =
#endif

			PushVideoFrameInternal( source, frameDuration );

#if defined(LOG_VIDEO_FRAME_PUSH)
			if( wasFramePushed )
			{
				ms_totalFramePushedTime += (sysTimer::GetTicks() - videoPushFrame) * sysTimer::GetTicksToMilliseconds();
				++ms_totalFramesPushed;
			}
#endif
		}
	}

	void MediaEncoder::CaptureMix( s16 const * const samples, size_t const sampleCount, size_t const channels )
	{
		if( IsCapturing() && !audDriver::GetMixer()->IsPaused(0) && m_threadData.IsAudioFrameCaptureAllowed() )
		{
			audDriver::GetMixer()->AddAudioTimeNs(MediaCommon::sc_gameAudioBufferDurationNs); // 1.0f/0.0000001875f

			if( videoVerifyf( samples, "MediaEncoderBuffers::CaptureMix - NULL Samples" ) &&
				videoVerifyf( sampleCount == MediaCommon::sc_sampleCountPerGameBuffer, "MediaEncoderBuffers::CaptureMix - Invalid sample count provided" ) && 
				videoVerifyf( channels == MediaCommon::sc_audioChannels, "MediaEncoderBuffers::CaptureMix - Invalid channel count" ) )
			{
#if defined(USE_DUMMY_AUDIO_DATA)
				CaptureMixInternal( (s16*)sc_dummyAudioBuffer, sampleCount );
#else
				CaptureMixInternal( samples, sampleCount );
#endif
			}
		}
	}

	void MediaEncoder::PrintOutVideoFramePushData()
	{
#if defined(LOG_VIDEO_FRAME_PUSH)
		videoDisplayf( "MediaEncoderWindows::EncoderThreadFunc - %u frames pushed, average time %f ms", 
			ms_totalFramesPushed, ms_totalFramePushedTime / (double)ms_totalFramesPushed );
		ms_totalFramesPushed = 0;
		ms_totalFramePushedTime = 0.0;
#endif
	}

	void MediaEncoder::PrintOutVideoFrameSubmissionData()
	{
#if defined(LOG_VIDEO_FRAME_SUBMISSION)
		videoDisplayf( "MediaEncoderWindows::EncoderThreadFunc - %u frames submitted, average time %f ms", 
			ms_totalFramesSubmitted, ms_totalFrameSubmitTime / (double)ms_totalFramesSubmitted );

		ms_totalFramesSubmitted = 0;
		ms_totalFrameSubmitTime = 0.0;
#endif
	}

	bool MediaEncoder::GenerateTargetUri( char const * const filename, char const * const path, 
		MediaEncoderParams const& params, atString& out_uri )
	{
		char buffer[ RAGE_MAX_PATH ];

		bool const success = formatf_n( buffer, "%s\\%s.%s", 
			path, filename, MediaCommon::GetFormatExtension( params.GetVideoEncodingFormat() ) ) > 0;
		out_uri = buffer;

		return success;
	}

	void MediaEncoder::GenerateDummyVideoData()
	{
#if defined( USE_DUMMY_VIDEO_DATA )
		GenerateDummyTexture( sc_dummyVideoTexture );
#endif
	}

	void MediaEncoder::GenerateDummyAudioData()
	{
#if defined( USE_DUMMY_AUDIO_DATA )
		static u8 sc_dummyBuffer[ sc_dummyAudioBufferSize ];		
		sc_dummyAudioBuffer = sc_dummyBuffer;

#if defined(ZERO_AUDIO_DATA)
		sysMemZeroBytes<MediaCommon::sc_gameAudioBufferSize>( sc_dummyBuffer );
#else
		if(videoVerifyf(MediaCommon::sc_bitsPerGameAudioSample == 16, "Dummy audio data generation is expecting to fill a buffer with 16-bit samples"))
		{
			u32 i = 0;
			const u32 uniqueSampleCount = sc_dummyAudioBufferSampleCount/sc_dummyAudioBufferChannelCount;

			// Generate a sine wave with matching data on each output channel
			for(u32 sample = 0; sample < uniqueSampleCount; sample++)
			{
				f32 sineWave = Sinf((sample/(f32)uniqueSampleCount) * PI * 2.0f);

				for(u32 channel = 0; channel < sc_dummyAudioBufferChannelCount; channel++)
				{
					s16 sample = (s16)(0x7FFF * sineWave);
					((s16*)sc_dummyBuffer)[i++] = sample;
				}
			}
		}
#endif
#endif // defined( USE_DUMMY_AUDIO_DATA )
	}

	void MediaEncoder::CleanupDummyVideoData()
	{
#if defined( USE_DUMMY_VIDEO_DATA )

		if( sc_dummyVideoTexture )
		{
			int const c_refCount = sc_dummyVideoTexture->GetRefCount();
			sc_dummyVideoTexture->DecRef();

			if( c_refCount == 1 )
			{
				sc_dummyVideoTexture = NULL;
			}
		}

#endif // defined( USE_DUMMY_VIDEO_DATA )
	}

	bool MediaEncoder::PushVideoFrameInternal( grcTexture const& source, float const frameDuration )
	{
		bool wasPushed(false);

#if defined( USE_DUMMY_VIDEO_DATA )

#if defined( VIDEO_ENCODE_BUFFERS_AS_TEXTURES )
		wasPushed = m_threadData.m_buffers.PushRawVideoFrame( *sc_dummyVideoTexture, frameDuration );
#else
		wasPushed = m_threadData.m_buffers.PushVideoFrame( *sc_dummyVideoTexture, frameDuration );	
#endif // defined( VIDEO_ENCODE_BUFFERS_AS_TEXTURES )
		
		(void)source;
		(void)frameDuration;
#else
		wasPushed = m_threadData.m_buffers.PushVideoFrame( source, frameDuration );	
#endif // defined( USE_DUMMY_VIDEO_DATA )

		return wasPushed;
	}

	void MediaEncoder::CaptureMixInternal( s16 const * const samples, size_t const sampleCount )
	{
		m_threadData.m_buffers.PushAudioBuffer( samples, sampleCount );
	}

} // namespace rage

#endif // ( USES_MEDIA_ENCODER ) && !__RESOURCECOMPILER
