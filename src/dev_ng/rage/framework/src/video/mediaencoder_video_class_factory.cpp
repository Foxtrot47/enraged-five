/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_video_class_factory.cpp
// PURPOSE : Class factory for injecting some parameters for video encoding
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediaencoder_video_class_factory.h"

#if RSG_PC && !__RESOURCECOMPILER

#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <ks.h>
#include <codecapi.h>
#include <Dshow.h>
#include <mfapi.h>
#include <mferror.h>
#include <Mfidl.h>
#pragma warning(pop)

// rage
#include "math/amath.h"

// framework
#include "video_channel.h"

namespace rage
{
	VideoEncoderClassFactory::VideoEncoderClassFactory()
		: m_registerImport( NULL )
		, m_unregisterImport( NULL )
		, m_refCount( 1 )
		, m_lockCount( 0 )
		, m_qualityLevel( DEFAULT_TRANSFORM_QUALITY_LEVEL )
		, m_speedVsQualityLevel( DEFAULT_TRANSFORM_SPEED_VS_QUALITY_LEVEL )
		, m_isWmv( false )
	{

	}

	VideoEncoderClassFactory::~VideoEncoderClassFactory()
	{
		videoAssertf( m_refCount == 0, "VideoEncoderClassFactory Destructor - Failed sanity check with ref-count of %u", m_refCount );
		Shutdown();
	}

	void VideoEncoderClassFactory::Initialize( bool const isWmv, u32 const qualityLevel, u32 const speedVsQualityLevel )
	{
		if( m_registerImport == NULL )
		{
			videoDisplayf( "VideoEncoderClassFactory::Initialize - Searching for Windows 7+ interface functions" );

			m_qualityLevel = rage::Clamp<u32>( qualityLevel, 0, 100 );
			m_speedVsQualityLevel = rage::Clamp<u32>( speedVsQualityLevel, 0, 100 );
			m_isWmv = isWmv;

			HMODULE const c_mfModule = GetModuleHandle( TEXT("Mfplat") );
			if( c_mfModule != NULL )
			{
				m_registerImport = (RegisterLocalImport)GetProcAddress( c_mfModule, "MFTRegisterLocal" );
				m_unregisterImport = (UnregisterLocalImport)GetProcAddress( c_mfModule, "MFTUnregisterLocal" );

				InitializeEncoderHooks();
			}
			else
			{
				videoDisplayf( "VideoEncoderClassFactory::Initialize - Failed to get module handle!" );
			}
		}

		videoDisplayf( "VideoEncoderClassFactory::Initialize - Result was %s", IsValid() ? "successful" : "a failure. Are you running Vista?" );
	}

	void VideoEncoderClassFactory::Shutdown()
	{
		CleanupEncoderHooks();
		m_registerImport = NULL;
		m_unregisterImport = NULL;
	}

	ULONG VideoEncoderClassFactory::AddRef()
	{ 
		return ++m_refCount; 
	}

	HRESULT VideoEncoderClassFactory::QueryInterface( REFIID riid, _COM_Outptr_ void **ppvObject )
	{
		HRESULT result = ppvObject == NULL ? E_POINTER : E_NOINTERFACE;
		if( ppvObject && IsValid() )
		{
			videoAssertf( *ppvObject == NULL, "VideoEncoderClassFactory::QueryInterface - Leaking interface, ppvObject non-null" );

			if( riid == IID_IUnknown )
			{
				*ppvObject = static_cast<IUnknown*>(this);
				AddRef();
			}
			else if( riid == IID_IClassFactory )
			{
				*ppvObject = static_cast<IClassFactory*>(this);
				AddRef();
			}
			else
			{
				*ppvObject = NULL;
			}

			// If we set the object by now, assume we done good!
			result = *ppvObject != NULL ? S_OK : result;
		}

		return result;
	}

	ULONG VideoEncoderClassFactory::Release()
	{
		u32 const c_refCountRemaining = --m_refCount;

		if( c_refCountRemaining == 0 )
		{
			delete this;
		}

		return c_refCountRemaining;
	}

	HRESULT VideoEncoderClassFactory::CreateInstance( IUnknown *pUnkOuter, REFIID riid, void **ppvObject )
	{
		HRESULT hr = E_NOINTERFACE;

		// Only support creation for non-aggregate, or aggregates of IUnknown
		if( pUnkOuter == NULL || riid == IID_IUnknown )
		{
			// Grab the base IMFTransform for the type we are generating
			IMFTransform* pEncoder = NULL;
			hr = FindEncoder( m_isWmv ? MediaCommon::GetWmvVersionGUID() : MFVideoFormat_H264, &pEncoder );
			if( videoVerifyf( SUCCEEDED(hr), "VideoEncoderClassFactory::CreateInstance - Unable to find suitable base IMFTransform!" ) )
			{
				ICodecAPI* pCodecApi = NULL;
				hr = pEncoder->QueryInterface<ICodecAPI>(&pCodecApi);
				if( videoVerifyf( SUCCEEDED(hr), "VideoEncoderClassFactory::CreateInstance - Base Transform doesn't support ICodecAPI?" ) )
				{
					// Now set up any custom parameters that we can't change post IMFSinkWriter creation...
					VARIANT var;

					VariantInit(&var);
					var.vt = VT_UI4;
					var.ulVal = eAVEncCommonRateControlMode_Quality;
					hr = pCodecApi->SetValue( &CODECAPI_AVEncCommonRateControlMode, &var );
					videoDisplayf( "VideoEncoderClassFactory::CreateInstance - ICodecAPI::SetValue (CODECAPI_AVEncCommonRateControlMode) "
						"completed with 0x%08lx", hr );
					VariantClear(&var);

					VariantInit(&var);
					var.vt = VT_UI4;
					var.ulVal = m_qualityLevel;
					hr = pCodecApi->SetValue( &CODECAPI_AVEncCommonQuality, &var );
					videoDisplayf( "VideoEncoderClassFactory::CreateInstance - ICodecAPI::SetValue (CODECAPI_AVEncCommonQuality, Value %u) "
						"completed with 0x%08lx", m_qualityLevel, hr );
					VariantClear(&var);

					if( MediaCommon::IsWindows8OrGreater() )
					{
						VariantInit(&var);
						var.vt = VT_UI4;
						var.ulVal = m_speedVsQualityLevel;
						hr = pCodecApi->SetValue( &CODECAPI_AVEncCommonQualityVsSpeed, &var );
						videoDisplayf( "VideoEncoderClassFactory::CreateInstance - ICodecAPI::SetValue (CODECAPI_AVEncCommonQualityVsSpeed, Value %u) "
							"completed with 0x%08lx", m_speedVsQualityLevel, hr );
						VariantClear(&var);
					}

					hr = pEncoder->QueryInterface( riid, ppvObject );
					videoAssertf( SUCCEEDED(hr), "VideoEncoderClassFactory::CreateInstance - QueryInterface for IUnknown failed with 0x%08lx", hr );
				}

				ReleaseMediaObject( pCodecApi );
			}

			ReleaseMediaObject( pEncoder );
		}

		return hr;
	}

	// Basic contract implementation. We never register as a proper COM object or use this in a DCOM scenario, so just doing the minimum
	HRESULT VideoEncoderClassFactory::LockServer( BOOL fLock )
	{
		m_lockCount += fLock == TRUE ? 1 : -1;

		bool const c_isLastRefRelease = m_lockCount == 0;
		return CoLockObjectExternal( this, m_lockCount > 0, c_isLastRefRelease ? TRUE : FALSE );
	}

	void VideoEncoderClassFactory::InitializeEncoderHooks()
	{
		if( IsValid() )
		{
			MFT_REGISTER_TYPE_INFO const c_infoInput = { MFMediaType_Video, MFVideoFormat_NV12 };
			MFT_REGISTER_TYPE_INFO const c_infoOutput = { MFMediaType_Video, m_isWmv ? MediaCommon::GetWmvVersionGUID() : MFVideoFormat_H264 };

			HRESULT const c_hr = m_registerImport( this, MFT_CATEGORY_VIDEO_ENCODER, L"VideoEncoderClassFactory", 0, 1, &c_infoInput, 1, &c_infoOutput );
			videoVerifyf( SUCCEEDED(c_hr), "VideoEncoderClassFactory::CleanupEncoderHooks - Unregister failed with 0x%08lx", c_hr );
		}
	}

	void VideoEncoderClassFactory::CleanupEncoderHooks()
	{
		if( IsValid() )
		{
			HRESULT const c_hr = m_unregisterImport( this );
			videoVerifyf( SUCCEEDED(c_hr), "VideoEncoderClassFactory::CleanupEncoderHooks - Unregister failed with 0x%08lx", c_hr );
		}
	}

	HRESULT VideoEncoderClassFactory::FindEncoder( const GUID& subtype, IMFTransform **ppEncoder )
	{
		HRESULT hr = S_OK;

		UINT32 count = 0;
		CLSID* pClsids;

		MFT_REGISTER_TYPE_INFO inputInfo = { MFMediaType_Video, MFVideoFormat_NV12 };
		MFT_REGISTER_TYPE_INFO outputInfo = { MFMediaType_Video, subtype };

		//! Do not add MFT_ENUM_FLAG_LOCALMFT, or we'll enumerate ourselves! 
		UINT32 const c_enumFlags = MFT_ENUM_FLAG_SYNCMFT | MFT_ENUM_FLAG_ASYNCMFT | MFT_ENUM_FLAG_SORTANDFILTER | MFT_ENUM_FLAG_HARDWARE;

		hr = MFTEnum(
			MFT_CATEGORY_VIDEO_ENCODER,
			c_enumFlags,
			&inputInfo,
			&outputInfo,		
			NULL, 
			&pClsids, 
			&count);

		if( SUCCEEDED(hr) )
		{
			// It is possible to succeed and find zero supported codecs so drop out appropriately,
			// otherwise lets run with the first supported codec we find
			hr = count == 0 ? MF_E_TOPO_CODEC_NOT_FOUND :
				CoCreateInstance( pClsids[0], NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(ppEncoder) );
		}

		CoTaskMemFree( pClsids );

		return hr;
	}

} // namespace rage

#endif // RSG_PC && !__RESOURCECOMPILER
