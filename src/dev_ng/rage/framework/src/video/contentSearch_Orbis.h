#ifndef __CONTENT_SEARCH_ORBIS_H__
#define __CONTENT_SEARCH_ORBIS_H__

#if RSG_ORBIS

// Content Search for Orbis.
#include <content_search.h>

namespace rage
{

class CContentSearch
{
public:
	static void Init(unsigned initMode);
	static void Shutdown(unsigned shutdownMode);

	static bool GetVideoInfos(u32 offset, u32 maxSizeOfList, u32 *o_numOfContent, SceContentSearchContentInfo *o_infos);
	static bool GetMetaData(SceContentSearchContentInfo *infos, s32 *o_mimeType = NULL, u64 *o_lastUpdatedTime = NULL, s32 *o_height = NULL, s32 *o_width = NULL, u32 *o_size = NULL, u32 *o_duration = NULL, u32 outputStringLength = 255, char* o_copyright = NULL, char* o_subtitle = NULL);

private:

	static bool	m_InitSuccess;
	static SceContentSearchApplicationIndex m_applicationIndex;
};

} // namespace rage

#endif	//RSG_ORBIS

#endif //__CONTENT_SEARCH_ORBIS_H__
