#ifndef _GREATESTMOMENT_DUMMY_H_
#define _GREATESTMOMENT_DUMMY_H_

#if ( !RSG_DURANGO )

#include "greatestmoment.h"

namespace rage
{

class fwGreatestMomentDummy: public fwGreatestMoment
{
public:

	fwGreatestMomentDummy();
	virtual ~fwGreatestMomentDummy();

	void	RecordGreatestMoment(u32 userIndex, const char* id, const char* displayName, s32 startTime, u32 duration);
	void	EnableRecording();
	void	DisableRecording();

protected:
	void	InitInstance(unsigned initMode);
	void	UpdateInstance();
	void	ShutdownInstance(unsigned shutdownMode);

private:
};

}

#endif
#endif // _GREATESTMOMENT_DUMMY_H_
