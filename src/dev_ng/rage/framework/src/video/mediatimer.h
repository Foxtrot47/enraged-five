/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediatimer.h
// PURPOSE : Class for timing multimedia (encoding/decoding) related functionality
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIA_TIMER_H_
#define INC_MEDIA_TIMER_H_

// rage
#include "math/amath.h"

// framework
#include "media_common.h"

#define MEDIA_TIMER_SYSTEM ( !RSG_PC && !RSG_DURANGO )

namespace rage
{
	class MediaTimer
	{
	public: // methods
		MediaTimer()
			: m_timerState( TIMER_STATE_STOPPED )
			, m_currentTimeOneHNs( 0 )
		{
			
		}

		~MediaTimer()
		{
			
		}

		inline void Start( u64 const startOffsetOneHNs = 0 )
		{
			m_lastSystemTimeOneHNs = GetConstantTimeOneHNs();
			m_currentTimeOneHNs = startOffsetOneHNs;
			m_timerState = TIMER_STATE_ACTIVE;
		}

		inline void Pause()
		{
			m_timerState = IsActive() ? TIMER_STATE_PAUSED : m_timerState;
		}

		inline void Stop()
		{
			m_timerState = TIMER_STATE_STOPPED;
			m_currentTimeOneHNs = 0;
		}

		inline u64 GetTimeOneHNs()
		{
			return IsPausedOrStopped() ? m_currentTimeOneHNs : UpdateAndGetTimeOneHNs();
		}

		inline u64 GetTimeNs()
		{
			u64 const c_timeOneHNs = GetTimeOneHNs();
			return ONE_H_NS_UNITS_TO_NANOSECONDS( c_timeOneHNs );
		}

		//! Returns number of nanoseconds elapsed since some point in time
		static u64 GetConstantTimeNs();
		static u64 GetConstantTimeOneHNs();

		// State accessors
		inline bool IsStopped() const { return m_timerState == TIMER_STATE_STOPPED; }
		inline bool IsActive() const { return m_timerState == TIMER_STATE_ACTIVE; }
		inline bool IsPaused() const { return m_timerState == TIMER_STATE_PAUSED; }

		inline bool IsPausedOrStopped() const { return IsPaused() || IsStopped(); }

	private: // declarations and variables
		enum eTimerState
		{
			TIMER_STATE_STOPPED,
			TIMER_STATE_ACTIVE,
			TIMER_STATE_PAUSED
		};

		eTimerState		m_timerState;
		u64				m_currentTimeOneHNs;
		u64				m_lastSystemTimeOneHNs;

	private: // methods

		inline u64 UpdateAndGetTimeOneHNs()
		{
			u64 const c_currentTimeOneHNs = GetConstantTimeOneHNs();
			u64 const c_diffOneHNs = c_currentTimeOneHNs - m_lastSystemTimeOneHNs;

			m_lastSystemTimeOneHNs = c_currentTimeOneHNs;
			m_currentTimeOneHNs += c_diffOneHNs;

			return m_currentTimeOneHNs;
		}
	};	

} // namespace rage

#endif // INC_MEDIA_TIMER_H_
