/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediadecoder_display_frame.h
// PURPOSE : Class for holding a decoded frame of video to be displayed
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#ifndef INC_MEDIADECODER_DISPLAY_FRAME_H_
#define INC_MEDIADECODER_DISPLAY_FRAME_H_

// rage
#include "math/amath.h"

// framework
#include "media_common.h"
#include "mediadecoder_format_info.h"

namespace rage
{
	class grcTexture;

	class MediaDecoderDisplayFrame
	{
	public: // methods
		MediaDecoderDisplayFrame();
		~MediaDecoderDisplayFrame();

		bool Initialize( MediaDecoderFormatInfo const& formatInfo );
		bool IsInitialized() const { return m_initialized; }
		void Shutdown();

		void CopyNV12IntoFrame( u8 const * srcNv12, size_t const srcSize, MediaDecoderFormatInfo const& formatInfo );
		void PopulateTextureFrame( grcTexture* yTexture, grcTexture* uTexture, grcTexture* vTexture ) const;

		void SetId( s64 const id ) { m_id = id; }
		s64 GetId() const { return m_id; }

	private: // declarations and variables

		u8*			m_yBuffer;
		u8*			m_uBuffer;
		u8*			m_vBuffer;
		s64			m_id;
		u32			m_frameWidth;
		u32			m_frameHeight;
		bool		m_initialized;

	private: // methods
		static void CopyNV12IntoFrameInternal( u8* destY, u8* destU, u8* destV, u8 const * srcNv12, size_t const srcSize, MediaDecoderFormatInfo const& formatInfo, bool const useAlignedBuffers );
		static void CopyBufferToTexture( grcTexture* texture, u8 const * const bufferSource, u32 const bufferWidth, u32 const bufferHeight );
	};

} // namespace rage

#endif // INC_MEDIADECODER_DISPLAY_FRAME_H_

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED