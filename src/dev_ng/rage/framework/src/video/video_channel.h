//
// video/video_channel.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __video_channel
#define __video_channel video_chan

#ifndef INC_VIDEO_CHANNEL_H
#define INC_VIDEO_CHANNEL_H

#include "diag/channel.h"

namespace rage
{
	RAGE_DECLARE_CHANNEL(video_chan)					// Defined in MediaCommon.cpp

	#define videoAssert(cond)							RAGE_ASSERT(video_chan,cond)
	#define videoAssertf(cond,fmt,...)					RAGE_ASSERTF(video_chan,cond,fmt,##__VA_ARGS__)
	#define videoVerify(cond)							RAGE_VERIFY(video_chan, cond)
	#define videoVerifyf(cond,fmt,...)					RAGE_VERIFYF(video_chan,cond,fmt,##__VA_ARGS__)
	#define videoErrorf(fmt,...)						RAGE_ERRORF(video_chan,fmt,##__VA_ARGS__)
	#define videoWarningf(fmt,...)						RAGE_WARNINGF(video_chan,fmt,##__VA_ARGS__)
	#define videoDisplayf(fmt,...)						RAGE_DISPLAYF(video_chan,fmt,##__VA_ARGS__)
	#define videoDebugf1(fmt,...)						RAGE_DEBUGF1(video_chan,fmt,##__VA_ARGS__)
	#define videoDebugf2(fmt,...)						RAGE_DEBUGF2(video_chan,fmt,##__VA_ARGS__)
	#define videoDebugf3(fmt,...)						RAGE_DEBUGF3(video_chan,fmt,##__VA_ARGS__)
	#define videoLogf(severity,fmt,...)					RAGE_LOGF(video_chan,severity,fmt,##__VA_ARGS__)
	#define videoCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,video_chan,severity,fmt,##__VA_ARGS__)

} // namespace rage

#endif // INC_VIDEO_CHANNEL_H
