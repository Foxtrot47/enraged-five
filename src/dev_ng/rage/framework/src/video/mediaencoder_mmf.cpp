/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_mmf.cpp
// PURPOSE : Class for encoding video/audio streams on Microsoft platforms
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediaencoder.h"
#include "system/pix.h"

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if ( RSG_PC || RSG_DURANGO ) && !__RESOURCECOMPILER && !__RGSC_DLL

#if RSG_PC
PARAM(disableEncodeVerify,"MP4 Encoder: Disable verify key");
#endif

#if RSG_PC
// library needed for changing metadata with PropertyStore
#pragma comment(lib, "shlwapi.lib") 
#endif

// Windows
#pragma warning(push)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <mfapi.h>
#include <Mfidl.h>
#include <mfobjects.h> // THIS FUCKING FILE IS POPPING ONE WARNING STACK TOO MANY, AND SETTING WARNING 4200 to error. HENCE THE DOUBLE TAP.
#include <Mfreadwrite.h>
#include <ks.h>
#include <propsys.h>
#include <codecapi.h>
#include <winerror.h>
#if RSG_PC
#include <propvarutil.h>
#include <propkey.h>
#include <wmsdkidl.h>
#include <wmcodecdsp.h>
#endif
#pragma warning(pop)
#pragma warning(pop)

// rage
#include "audiohardware/driver.h"
#include "file/asset.h"
#include "grcore/device.h"
#include "string/unicode.h"
#include "system/param.h"
#include "system/timer.h"
#if RSG_PC
#include "fwlocalisation/textUtil.h"
#include "string/stringutil.h"
#include "fwmaths/Random.h"
#endif

// framework
#include "media_common.h"
#include "media_transcoding_allocator.h"
#include "mediaencoder_video_class_factory.h"
#include "sinkwriter.h"
#include "video_channel.h"

// Uses the unicode converter
#define UTF8_TO_UTF16(x) reinterpret_cast<const wchar_t* const>(UTF8_TO_WIDE(x))

PARAM(mmfNoDeleteCorruptFiles, "Allow corrupt video files to remain on disk so they can be reviewed for bugs");

//PRAGMA_OPTIMIZE_OFF()

namespace rage
{

	// PURPOSE: Prepares a IMFMediaType object for our final video output based on our encoder parameters
	// PARAMS:
	//		mediaType - Pointer to a IMFMediaType we want to populate based on our encoder params.
	//		params - Parameters representing how we intend to encode our video
	// RETURNS:
	//		Boolean indicating success if true, false if failed.
	static bool PrepareOutputVideoMediaType( IMFMediaType*& mediaType, rage::MediaEncoderParams const& params )
	{
		bool success = false;

		videoAssertf( mediaType, "PrepareOutputVideoMediaType - Null media type provided" );
		if( mediaType )
		{
			HRESULT hr = mediaType->SetGUID( MF_MT_MAJOR_TYPE, MFMediaType_Video ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - SetGUID - MF_MT_MAJOR_TYPE failed with 0x%08lx", hr );

			MediaCommon::eVideoFormat const c_videoFormat = params.GetVideoEncodingFormat();

			hr = mediaType->SetGUID( MF_MT_SUBTYPE, MediaCommon::ConvertVideoFormatToGUID( c_videoFormat ) ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - SetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AVG_BITRATE, params.GetQualityLevelVideoBitRate() ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - SetUINT32 - MF_MT_AVG_BITRATE failed with 0x%08lx", hr ) && success;

			rage::u32 numerator, denominator;
			MediaCommon::ConvertFPSToNumeratorAndDenominator( params.GetOutputFpsFloat(), numerator, denominator );

			hr = MFSetAttributeRatio( mediaType, MF_MT_FRAME_RATE, numerator, denominator );   
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - MFSetAttributeRatio - MF_MT_FRAME_RATE failed with 0x%08lx", hr ) && success;

			hr = MFSetAttributeSize( mediaType, MF_MT_FRAME_SIZE, params.GetOutputWidth(), params.GetOutputHeight() );   
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - MFSetAttributeSize - MF_MT_FRAME_SIZE failed with 0x%08lx", hr ) && success;

			if( c_videoFormat == MediaCommon::VIDEO_FORMAT_H264 )
			{
				hr = mediaType->SetUINT32( MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive ); 
				success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - SetUINT32 - MF_MT_INTERLACE_MODE failed with 0x%08lx", hr ) && success;

#if RSG_DURANGO
				u32 const c_profileLevel = eAVEncH264VProfile_Main;
				u32 const c_mpegLevel = eAVEncH264VLevel4_1;
#else
				// Support for high-profile encoding on Windows 8+
				bool const c_supportsHighProfile = MediaCommon::IsWindows8OrGreater();
			
				u32 const c_profileLevel = c_supportsHighProfile ? eAVEncH264VProfile_High : eAVEncH264VProfile_Main;
				u32 const c_mpegLevel =  eAVEncH264VLevel4_2;
#endif

				hr = mediaType->SetUINT32( MF_MT_MPEG2_PROFILE, c_profileLevel );
				success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - SetUINT32 - MF_MT_MPEG2_PROFILE failed with 0x%08lx", hr ) && success;

				hr = mediaType->SetUINT32( MF_MT_MPEG2_LEVEL, c_mpegLevel );
				success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - SetUINT32 - MF_MT_MPEG2_LEVEL failed with 0x%08lx", hr ) && success;
			}
		
			hr = MFSetAttributeRatio( mediaType, MF_MT_PIXEL_ASPECT_RATIO, 1, 1 );   
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputVideoMediaType - MFSetAttributeRatio - MF_MT_PIXEL_ASPECT_RATIO failed with 0x%08lx", hr ) && success;
		}

		return success;
	}

	// PURPOSE: Prepares a IMFMediaType object for our final audio output based on our encoder parameters
	// PARAMS:
	//		mediaType - Pointer to a IMFMediaType we want to populate based on our encoder params.
	//		params - Parameters representing how we intend to encode our audio
	// RETURNS:
	//		Boolean indicating success if true, false if failed.
	static bool PrepareOutputAudioMediaType( IMFMediaType*& mediaType, rage::MediaEncoderParams const& params )
	{
		bool success = false;

		videoAssertf( mediaType, "PrepareOutputAudioMediaType - Null media type provided" );
		if( mediaType )
		{
			rage::u32 const bytesPerSecond = params.GetAudioOutputBytesPerSecond();
			rage::u32 const blockAlign = params.GetAudioOutputBlockAlign();

			HRESULT hr = mediaType->SetGUID( MF_MT_MAJOR_TYPE, MFMediaType_Audio ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetGUID - MF_MT_MAJOR_TYPE failed with 0x%08lx", hr );

			MediaCommon::eAudioFormat const c_audioFormat = params.GetAudioEncodingFormat();

			hr = mediaType->SetGUID( MF_MT_SUBTYPE, MediaCommon::ConvertAudioFormatToGUID( c_audioFormat ) ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_SAMPLES_PER_SECOND, rage::kMixerNativeSampleRate ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_SAMPLES_PER_SECOND failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_NUM_CHANNELS, rage::MediaCommon::sc_audioChannels ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_NUM_CHANNELS failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_BITS_PER_SAMPLE, rage::MediaCommon::sc_bitsPerGameAudioSample ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_BITS_PER_SAMPLE failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_AVG_BYTES_PER_SECOND, bytesPerSecond ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_AVG_BYTES_PER_SECOND failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_BLOCK_ALIGNMENT, blockAlign ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_BLOCK_ALIGNMENT failed with 0x%08lx", hr ) && success;

			if( c_audioFormat == MediaCommon::AUDIO_FORMAT_AAC )
			{
				hr = mediaType->SetUINT32( MF_MT_AAC_PAYLOAD_TYPE, 0 ); 
				success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AAC_PAYLOAD_TYPE failed with 0x%08lx", hr ) && success;

				hr = mediaType->SetUINT32( MF_MT_AAC_AUDIO_PROFILE_LEVEL_INDICATION, 0x29 ); 
				success = videoVerifyf( SUCCEEDED(hr), "PrepareOutputAudioMediaType - SetUINT32 - MF_MT_AAC_AUDIO_PROFILE_LEVEL_INDICATION failed with 0x%08lx", hr ) && success;
			}
			else
			{
				hr = mediaType->SetUINT32( MF_MT_FIXED_SIZE_SAMPLES, TRUE ); 
				success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - SetUINT32 - MF_MT_FIXED_SIZE_SAMPLES failed with 0x%08lx", hr ) && success;
			}
		}

		return success;
	}

#if RSG_PC
	static const rage::u32 s_maxTitleNameLength = 512;
	static char s_titleName[s_maxTitleNameLength];

	static bool WriteMetadata( IMFSinkWriter* writer, const char * const titleName, rage::MediaEncoderParams const& params, u64 const durationOneHNs )
	{
#if !__FINAL
		if (PARAM_disableEncodeVerify.Get())
		{
			(void)writer;
			(void)titleName;
			(void)params;

			return true;
		}
#endif

		// need to create all data first and then write metadata
		// You must release property stores quickly
		// http://blogs.msdn.com/b/benkaras/archive/2006/10/13/gotcha-you-must-release-property-stores-quickly.aspx

		rage::u32 titleNameLength = static_cast<rage::u32>(strlen(titleName));

		const rage::u32 maxKeyTitleNameLength = 32;
		if (titleNameLength > maxKeyTitleNameLength)
			titleNameLength = maxKeyTitleNameLength;

		static const rage::u32 keylength = 100;
		char keyBuffer[keylength+1];
		char checksumBuffer[12];
		{
			FILETIME systemTime;
			::GetSystemTimeAsFileTime( reinterpret_cast< FILETIME* >( &systemTime ) );
			videoDisplayf("WriteMetadata - systemTime - high - %u", systemTime.dwHighDateTime);
			videoDisplayf("WriteMetadata - systemTime - low - %u", systemTime.dwLowDateTime);

			formatf(keyBuffer, keylength, "%010u%010u%010u%02u", systemTime.dwHighDateTime, systemTime.dwLowDateTime, params.GetEstimatedDurationMs(), titleNameLength);
			videoDisplayf("WriteMetadata - keyBuffer - %s", keyBuffer);

			// max data length can be: 10 + 10 + 10 + 2 + 32 = 64
			rage::u32 bufferLength = static_cast<rage::u32>(strlen(keyBuffer));

			// fill rest of buffer with random
			for (rage::u32 i = bufferLength; i < keylength; ++i)
			{
				int odds = fwRandom::GetRandomNumberInRange(0, 100);
				if (odds < 70) // digits ...want more of these
					keyBuffer[i] = static_cast<char>(fwRandom::GetRandomNumberInRange(48, 57));
				else if (odds < 75) // caps.
					keyBuffer[i] = static_cast<char>(fwRandom::GetRandomNumberInRange(65, 90));
				else if (odds < 85) // lower case
					keyBuffer[i] = static_cast<char>(fwRandom::GetRandomNumberInRange(97, 122));
				else // going to need a lot of zeros to blend in
					keyBuffer[i] = '0';
			}
			keyBuffer[keylength] = '\0';

			// checksum
			// use first 80 chars. up to 64 can be valid. half the random characters will get used. 

			wchar_t* checksum = reinterpret_cast<wchar_t*>(keyBuffer);

			rage::u32  total = 0;
			// pick a weird number to wrap around on, rather than UINT_MAX, to make it tricker
			const rage::u32 wrapAround = (UINT_MAX / 11) * 9;
			for (rage::u32 i = 0; i < 40; ++i)
			{
				total += static_cast<rage::u32>(checksum[i]);

				if (total > wrapAround)
					total -= wrapAround;
			}

			formatf(checksumBuffer, 12, "%u", total);

			// mix up resolvable key massively!!! ...but still resolvable
			rage::u32 w = 4;
			rage::u32 z = 6;

			rage::u32 random = 0;
			double randomFloat = 0;
			for (rage::u32 i = keylength - 1; i > 0; i--)
			{
				z = 36969 * (z & 65535) + (z >> 16);
				w = 18000 * (w & 65535) + (w >> 16);

				random = (z << 16) + w;
				randomFloat = (random + 1.0) * 2.328306435454494e-10;

				rage::u8 j = (rage::u8) (randomFloat*(i+1));
				rage::u8 t = keyBuffer[j];
				keyBuffer[j] = keyBuffer[i];
				keyBuffer[i] = t;
			}
			keyBuffer[keylength] = '\0';
		}

		HRESULT hr = S_OK;

		if( MediaCommon::IsWindows7OrGreater() )
		{
			//0xc00d36ba is MF_E_UNSUPPORTED_SERVICE
			//E_NOINTERFACE No such interface supported.
			IPropertyStore* pPropertyStore = NULL;	
			hr = writer->GetServiceForStream( static_cast<DWORD>(MF_SINK_WRITER_MEDIASINK), MF_PROPERTY_HANDLER_SERVICE, IID_PPV_ARGS(&pPropertyStore));
			if( videoVerifyf( SUCCEEDED(hr), "WriteMetadata - IMFSourceReader::GetServiceForStream "
				"failed with 0x%08lx", hr ) )
			{
				PROPVARIANT variant;

				{
					USES_CONVERSION;
					wchar_t const * const wideChecksum = UTF8_TO_UTF16(checksumBuffer);
					InitPropVariantFromString(wideChecksum, &variant);
					pPropertyStore->SetValue(PKEY_Identity_UniqueID, variant);
					PropVariantClear( &variant );
				}

				{
					USES_CONVERSION;
					wchar_t const * const wideName = UTF8_TO_UTF16(keyBuffer);
					InitPropVariantFromString(wideName, &variant);
					pPropertyStore->SetValue(PKEY_Media_UniqueFileIdentifier, variant);
					PropVariantClear( &variant );
				}


				{
					InitPropVariantFromString(params.GetCopyright(), &variant);
					pPropertyStore->SetValue(PKEY_Copyright, variant);
					PropVariantClear( &variant );
				}

				{
					InitPropVariantFromString(params.GetGameTitle(), &variant);
					pPropertyStore->SetValue(PKEY_Media_SubTitle, variant);
					PropVariantClear( &variant );
				}


				{
					USES_CONVERSION;
					wchar_t const * const wideName = UTF8_TO_UTF16(titleName);
					InitPropVariantFromString(wideName, &variant);
					pPropertyStore->SetValue(PKEY_Title, variant);
					PropVariantClear( &variant );
				}

				// only add this at all if tampered ... perhaps add to key in future
				if (params.GetModdedContent())
				{
					InitPropVariantFromUInt32(1, &variant);
					pPropertyStore->SetValue(PKEY_Media_AverageLevel, variant);
					PropVariantClear( &variant );
				}
			}

			ReleaseMediaObject( pPropertyStore );
		}
		else
		{
			IMFMetadataProvider* pMetaDataProvider = NULL; 
			hr = writer->GetServiceForStream( static_cast<DWORD>(MF_SINK_WRITER_MEDIASINK), GUID_NULL, IID_PPV_ARGS(&pMetaDataProvider) );
			if( videoVerifyf( SUCCEEDED(hr), "WriteMetadata - IMFSourceReader::GetServiceForStream "
				"failed with 0x%08lx", hr ) )
			{
				IMFMetadata* pMetadata = NULL;
				hr = pMetaDataProvider->GetMFMetadata( NULL, 0, 0, &pMetadata );
				if( videoVerifyf( SUCCEEDED(hr), "WriteMetadata - IMFSourceReader::GetMFMetadata "
					"failed with 0x%08lx", hr ) )
				{
					#define GUID_BUFFER_SIZE (64)
					#define METADATA_BUFFER_SIZE (128)
					wchar_t propKeyNameBuffer[GUID_BUFFER_SIZE];
					rage::char16 metadataBuffer[METADATA_BUFFER_SIZE];
					PROPVARIANT variant;

					{
						int const c_inLen = (int)strlen( checksumBuffer );
						rage::Utf8ToWide( metadataBuffer, checksumBuffer, c_inLen );

						InitPropVariantFromString( (wchar_t*)metadataBuffer, &variant );
						PSStringFromPropertyKey( PKEY_Identity_UniqueID, propKeyNameBuffer, GUID_BUFFER_SIZE );
						pMetadata->SetProperty( propKeyNameBuffer, &variant );

						PropVariantClear( &variant );
					}

					{
						int const c_inLen = (int)strlen( keyBuffer );
						rage::Utf8ToWide( metadataBuffer, keyBuffer, c_inLen );
						InitPropVariantFromString( (wchar_t*)metadataBuffer, &variant );

						PSStringFromPropertyKey( PKEY_Media_UniqueFileIdentifier, propKeyNameBuffer, GUID_BUFFER_SIZE );
						pMetadata->SetProperty( propKeyNameBuffer, &variant );

						PropVariantClear( &variant );
					}

					{
						InitPropVariantFromString( params.GetCopyright(), &variant );
						pMetadata->SetProperty( g_wszWMCopyright, &variant );
						PropVariantClear( &variant );
					}

					{
						InitPropVariantFromString( params.GetGameTitle(), &variant );

						PSStringFromPropertyKey( PKEY_Media_SubTitle, propKeyNameBuffer, GUID_BUFFER_SIZE );
						pMetadata->SetProperty( propKeyNameBuffer, &variant );
						pMetadata->SetProperty( g_wszWMSubTitleDescription, &variant );

						PropVariantClear( &variant );
					}


					{
						int const c_inLen = (int)strlen( titleName );
						rage::Utf8ToWide( metadataBuffer, titleName, c_inLen );
						InitPropVariantFromString( (wchar_t*)metadataBuffer, &variant );

						PSStringFromPropertyKey( PKEY_Title, propKeyNameBuffer, GUID_BUFFER_SIZE );
						pMetadata->SetProperty( propKeyNameBuffer, &variant );
						pMetadata->SetProperty( g_wszWMTitle, &variant );

						PropVariantClear( &variant );
					}

					{
						InitPropVariantFromUInt64( durationOneHNs, &variant );

						PSStringFromPropertyKey( PKEY_Media_Duration, propKeyNameBuffer, GUID_BUFFER_SIZE );
						pMetadata->SetProperty( propKeyNameBuffer, &variant );

						PropVariantClear( &variant );
					}
					
				}

				ReleaseMediaObject( pMetadata );
			}

			ReleaseMediaObject( pMetaDataProvider );
			
		}

		return (hr == S_OK);
	}
#endif

	/* Windows 8+ way of providing encoder settings.
	static bool PrepareVideoStreamAttributes( IMFAttributes*& inout_attributes )
	{
		bool success = false;

		if( videoVerifyf( inout_attributes == NULL, "PrepareVideoStreamAttributes - Non-null attributes. Potential leak?" ) )
		{
			PROPVARIANT prop;
			IPropertyStore *pPropertyStore = NULL;

			HRESULT hr = PSCreateMemoryPropertyStore( IID_PPV_ARGS(&pPropertyStore) );
			if( videoVerifyf( SUCCEEDED(hr), "PrepareVideoStreamAttributes - PSCreateMemoryPropertyStore failed with 0x%08lx", hr ) )
			{
				InitPropVariantFromBoolean( TRUE, &prop );
				hr = pPropertyStore->SetValue( MFPKEY_VBRENABLED, prop );
				videoDisplayf( SUCCEEDED(hr), "PrepareVideoStreamAttributes - IPropertyStore::SetValue (MFPKEY_VBRENABLED) failed with 0x%08lx", hr );
				PropVariantClear( &prop );

				InitPropVariantFromUInt32( DEFAULT_TRANSFORM_QUALITY_LEVEL, &prop );
				hr = pPropertyStore->SetValue( MFPKEY_VBRQUALITY, prop );
				videoDisplayf( SUCCEEDED(hr), "PrepareVideoStreamAttributes - IPropertyStore::SetValue (MFPKEY_VBRQUALITY) failed with 0x%08lx", hr );
				PropVariantClear( &prop );

				hr = MFCreateAttributes( &inout_attributes, 1 );
				hr = inout_attributes ? inout_attributes->SetUnknown( MF_SINK_WRITER_ENCODER_CONFIG, pPropertyStore ) : hr;
				success = videoVerifyf( SUCCEEDED(hr), "PrepareVideoStreamAttributes - "
					"IMFAttributes::SetUnknown (MF_SINK_WRITER_ENCODER_CONFIG) failed with 0x%08lx", hr );

				ReleaseMediaObject( pPropertyStore );
			}
		}

		return success;
	}*/

	// PURPOSE: Prepares the underlying encoding API and returns a number of objects as out parameters that we need for encoding.
	// PARAMS:
	//		name - URI to the output video we are generating
	//		params - Parameters representing how we intend to encode our video
	//		out_writer - Out pointer to a IMFSinkWriter object, used for writing the video file
	//		out_videoStreamIndex - Index used to identify which stream in our file should contain video data
	//		out_audioStreamIndex - Index used to identify which stream in our file should contain audio data
	// RETURNS:
	//		True if successful, false otherwise.
	static HRESULT PrepareForRecording( const char * const outputPath, rage::MediaEncoderParams const& params, IMFDXGIDeviceManager* dxDeviceManager, 
		IMFSinkWriter*& out_writer, DWORD& out_videoStreamIndex, DWORD& out_audioStreamIndex )
	{
		bool success = false;
		IMFSinkWriter* writer( NULL );
		DWORD videoStreamIndex( 0 );
		DWORD audioStreamIndex( 0 );
		HRESULT hr = E_INVALIDARG;

		videoAssertf( outputPath, "PrepareForRecording - No name provided for output!" );
		videoAssertf( writer == NULL, "PrepareForRecording - IMFSinkWriter is not NULL!" );
		if( outputPath && out_writer == NULL )
		{
			IMFAttributes* writerAttributes( NULL );
			if( MediaCommon::PrepareAttributes( writerAttributes, dxDeviceManager, false ) )
			{
				USES_CONVERSION;
				wchar_t const * const widePath = UTF8_TO_UTF16(outputPath);

				// Lazy Alloc - 72  bytes XTL
				hr = SinkWriterCreator::CreateSinkWriter( widePath, NULL, writerAttributes, &writer );
				if( hr == E_ACCESSDENIED || hr == ERROR_SHARING_VIOLATION )
				{
					videoWarningf( "PrepareForRecording - SinkWriterCreator::CreateSinkWriter failed with non-fatal result 0x%08lx, URI: %ls", hr, widePath );
				}
				else if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - SinkWriterCreator::CreateSinkWriter failed with 0x%08lx, URI: %ls", hr, widePath ) )
				{
					videoDisplayf( "PrepareForRecording - Created file URI: %ls", widePath );

					IMFMediaType* outputVideoMediaType( NULL );   
					IMFMediaType* inputVideoMediaType( NULL );
					IMFMediaType* outputAudioMediaType( NULL );   
					IMFMediaType* inputAudioMediaType( NULL );

					//! Create and set we are making a video format
					hr = MFCreateMediaType( &outputVideoMediaType );
					if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - MFCreateMediaType failed to make output video type with 0x%08lx", hr ) )
					{
						videoDisplayf( "PrepareForRecording - Created output media type (video)" );

						hr = MFCreateMediaType( &inputVideoMediaType );
						if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - MFCreateMediaType failed to make input video type with 0x%08lx", hr ) )
						{
							videoDisplayf( "PrepareForRecording - Created input media type (video)" );

							if( videoVerifyf( PrepareOutputVideoMediaType( outputVideoMediaType, params ), 
								"PrepareForRecording - Unable to prepare output video type" ) && 
								videoVerifyf( MediaCommon::PrepareGameCompatibleOutputVideoMediaType( inputVideoMediaType, 
								params.GetOutputWidth(), params.GetOutputHeight(), params.GetOutputFpsFloat() ), 
								"PrepareForRecording - Unable to prepare input video type" ) )
							{
								videoDisplayf( "PrepareForRecording - Prepared video type parameters" );

								hr = writer->AddStream( outputVideoMediaType, &videoStreamIndex );   
								if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - AddStream - Unable to create output video stream. Failed with 0x%08lx", hr ) )
								{
									videoDisplayf( "PrepareForRecording - Added output stream (video)" );

									IMFAttributes* videoAttributes = NULL;
									//PrepareVideoStreamAttributes( videoAttributes );

									// Lazy Alloc - 544 Bytes XTL, 20480 bytes Graphics (happens on first stream addition)
									hr = writer->SetInputMediaType( videoStreamIndex, inputVideoMediaType, videoAttributes );
									success = videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - SetInputMediaType failed to with 0x%08lx ; Your output video format likely doesn't accept this input format.", hr );
								
									ReleaseMediaObject( videoAttributes );
								}
							}
						}
					}

					//! Successful so far, so now lets set up the audio types
					if( success )
					{
						success = false;
						hr = MFCreateMediaType( &outputAudioMediaType );
						if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - MFCreateMediaType failed to make output audio type with 0x%08lx", hr ) )
						{
							videoDisplayf( "PrepareForRecording - Created output media type (audio)" );

							hr = MFCreateMediaType( &inputAudioMediaType );
							if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - MFCreateMediaType failed to make input audio type with 0x%08lx", hr ) )
							{
								videoDisplayf( "PrepareForRecording - Created input media type (audio)" );

								if( videoVerifyf( PrepareOutputAudioMediaType( outputAudioMediaType, params ), 
									"PrepareForRecording - Unable to prepare output audio type" ) &&
									videoVerifyf( MediaCommon::PrepareGameCompatibleOutputAudioMediaType( inputAudioMediaType ), 
									"PrepareForRecording - Unable to prepare input audio type" ) )
								{
									videoDisplayf( "PrepareForRecording - Created audio type parameters" );

									hr = writer->AddStream( outputAudioMediaType, &audioStreamIndex );   
									if( videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - AddStream - Unable to create output audio stream. Failed with 0x%08lx", hr ) )
									{
										videoDisplayf( "PrepareForRecording - Added output stream (audio)" );

										hr = writer->SetInputMediaType( audioStreamIndex, inputAudioMediaType, NULL );
										success = videoVerifyf( SUCCEEDED(hr), "PrepareForRecording - SetInputMediaType failed to with 0x%08lx ; Your output audio format likely doesn't accept this input format.", hr );
									}
								}
							}
						}
					}

					ReleaseMediaObject( outputVideoMediaType );
					ReleaseMediaObject( inputVideoMediaType );
					ReleaseMediaObject( outputAudioMediaType );
					ReleaseMediaObject( inputAudioMediaType );
				}

			}

			ReleaseMediaObject( writerAttributes );
		}

#if RSG_PC
		if( success && outputPath )
		{
			const char* filename = ASSET.FileName(outputPath);
			safecpy( s_titleName, filename );
			fwTextUtil::TruncateFileExtension( s_titleName );
		}
#endif

		if( success )
		{
			videoDisplayf( "PrepareForRecording - Succeeded." );

			out_writer = writer;
			out_videoStreamIndex = videoStreamIndex;
			out_audioStreamIndex = audioStreamIndex;
		}
		else
		{
			videoDisplayf( "PrepareForRecording - Failed. Check log for further details" );
			ReleaseMediaObject( writer );
		}

		return hr;
	}

	// PURPOSE: Gets statistics from the given IMFSinkWriter and displays them in debug out
	void PrintMfStats( MF_SINK_WRITER_STATISTICS const& stats )
	{
		(void)stats;

#if defined( LOG_ENCODER_STATS )

		videoDisplayf( "MF_SINK_WRITER_STATISTICS.llLastTimestampReceived: %d", stats.llLastTimestampReceived );
		videoDisplayf( "MF_SINK_WRITER_STATISTICS.llLastTimestampEncoded: %d", stats.llLastTimestampEncoded );
		videoDisplayf( "MF_SINK_WRITER_STATISTICS.llLastTimestampProcessed: %d", stats.llLastTimestampProcessed );
		videoDisplayf( "MF_SINK_WRITER_STATISTICS.dwByteCountQueued: %u", stats.dwByteCountQueued );
		videoDisplayf( "MF_SINK_WRITER_STATISTICS.qwByteCountProcessed: %u", stats.qwByteCountProcessed );
		videoDisplayf( "MF_SINK_WRITER_STATISTICS.dwNumOutstandingSinkSampleRequests: %u", stats.dwNumOutstandingSinkSampleRequests );

#endif
	}

	// PURPOSE: Gets statistics from the given IMFSinkWriter and action on them
	bool UpdateEncoderStats( IMFSinkWriter* writer, DWORD const streamIndex, MF_SINK_WRITER_STATISTICS& out_stats )
	{
		HRESULT const hr = writer ? writer->GetStatistics( streamIndex, &out_stats ) : E_INVALIDARG;
		bool const c_success = videoVerifyf( SUCCEEDED(hr), "UpdateEncoderStats - IMFSinkWriter::GetStatistics failed with 0x%08lx for stream %u",
			hr, streamIndex );

		return c_success;
	}

	static bool WriteSampleToStream( IMFSinkWriter& writer, IMFSample*& sample, DWORD const streamIndex 
		DURANGO_ONLY(, MediaEncoder::SampleWriteMonitor const& sampleMonitor ) )
	{
		bool success = false;
		HRESULT hr( S_OK );

		if( sample )
		{

#if !__NO_OUTPUT && RSG_DURANGO
			bool warnSamplePending = true;
#endif

#if RSG_DURANGO
			while( !sampleMonitor.ShouldAbandonLoop() && ( (hr = writer.WriteSample( streamIndex, sample )) == E_PENDING ) )
			{
#if !__NO_OUTPUT
				videoCondLogf( warnSamplePending, DIAG_SEVERITY_WARNING, 
					"WriteSampleToStream - Pending write on sample %p. HR 0x%08lx, stream %u", sample, hr, streamIndex );
#endif

#if !__NO_OUTPUT
				warnSamplePending = false;
#endif

				rage::sysIpcSleep( 1 );
			}
#elif RSG_PC
			hr = writer.WriteSample( streamIndex, sample );
#endif

			success = videoVerifyf( hr == S_OK, "WriteSampleToStream - WriteSample returned an error 0x%08lx", hr );
			ReleaseMediaObject( sample );
		}

		return success;
	}

	// PURPOSE: Updates the given IMFSinkWriter object with new video data for our current encoding task
	// PARAMS:
	//		writer						- IMFSinkWriter object we are using to write our output video
	//		buffer						- Buffer that represents the frame we are writing
	//		videoFrameStartOneHNs		- Start time of this frame in 100 nano-second units
	//		videoFrameDurationOneHNs	- Duration of this frame in 100 nano-second units
	//		videoStreamIndex			- Index of which stream to write video data to
	// RETURNS:
	//		True if frame successfully written, false otherwise.
	static bool UpdateVideoCapture( IMFSinkWriter& writer, MediaBufferBase*& buffer, MediaEncoderBuffers& bufferOwner, LONGLONG const videoFrameStartOneHNs, 
		UINT64 const videoFrameDurationOneHNs, DWORD const videoStreamIndex, u64 const keyframeInterval, u64& frameCount DURANGO_ONLY(, MediaEncoder::SampleWriteMonitor const& sampleMonitor ) )
	{
		bool success = false;

		if( videoVerifyf( buffer, "UpdateVideoCapture - Null frame provided!" ) )
		{
			MediaBufferVideoEncode* videoBuffer = static_cast<MediaBufferVideoEncode*>(buffer);
			HRESULT hr;

#if defined( VIDEO_ENCODE_BUFFERS_AS_BLOBS ) && VIDEO_ENCODE_BUFFERS_AS_BLOBS
			MediaBufferBlobInterfaceWin& externalBufferInterface = videoBuffer->GetExternalBufferInterface();
			IMFMediaBuffer* externalBuffer( externalBufferInterface.GetMediaBuffer() );
#elif defined( VIDEO_ENCODE_BUFFERS_AS_TEXTURES ) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES
			MediaBufferTextureInterfaceWin& externalBufferInterface = videoBuffer->GetExternalBufferInterface();
			IMFMediaBuffer* externalBuffer( externalBufferInterface.GetMediaBuffer() );

			if( videoBuffer->HasFence() )
			{
				GRCDEVICE.CpuWaitOnFence( videoBuffer->GetFenceRef() );
			}
#endif
			IMFSample* sample( NULL );

			hr = MFCreateSample( &sample );
			if( videoVerifyf( SUCCEEDED(hr), "UpdateVideoCapture - MFCreateSample failed with 0x%08lx", hr ) )
			{
				hr = sample->AddBuffer( externalBuffer );
				if( videoVerifyf( SUCCEEDED(hr), "UpdateVideoCapture - IMFSample::AddBuffer failed with 0x%08lx", hr ) )
				{
					bufferOwner.ReleaseBuffer( buffer );
					// Release the buffer now that the sample has added a ref. Do not use any elements of the buffer from here on.

					hr = sample->SetSampleTime( videoFrameStartOneHNs );
					success = videoVerifyf( SUCCEEDED(hr), "UpdateVideoCapture - IMFSample::SetSampleTime failed with 0x%08lx", hr );

					hr = sample->SetSampleDuration( videoFrameDurationOneHNs );
					success = videoVerifyf( SUCCEEDED(hr), "UpdateVideoCapture - IMFSample::SetSampleDuration failed with 0x%08lx", hr ) && success;

					BOOL const c_isKeyFrame = ( frameCount % keyframeInterval == 0 ) ? TRUE : FALSE;
					hr = sample->SetUINT32( MFSampleExtension_CleanPoint, c_isKeyFrame );
					videoAssertf( SUCCEEDED(hr), "UpdateVideoCapture - IMFSample::SetUINT32 (MFSampleExtension_CleanPoint) failed with 0x%08lx", hr );

					videoDebugf3("UpdateVideoCapture: Frame %LLu (%LLu + %LLu = %LLu)", frameCount, (UINT64)videoFrameStartOneHNs, videoFrameDurationOneHNs, (UINT64)videoFrameStartOneHNs + videoFrameDurationOneHNs);
				}
			}

			success = videoVerifyf( WriteSampleToStream( writer, sample, videoStreamIndex DURANGO_ONLY(, sampleMonitor ) ), "UpdateVideoCapture : Failed to write sample to stream.");

			if( success )
			{
				++frameCount;
			}
		}

		return success;
	}

	// PURPOSE: Updates the given IMFSinkWriter object with new audio data for our current encoding task
	// PARAMS:
	//		writer						- IMFSinkWriter object we are using to write our output video
	//		buffer						- Media buffer to write data frome
	//		audioFrameStartOneHNs		- Start time for this frame, in 100 nano-second units
	//		audioFrameDurationOneHNs	- Duration of this frame in 100 nano-second units
	//		audioStreamIndex			- Index of which stream to write audio data to
	// RETURNS:
	//		True if frame successfully written, false otherwise.
	bool UpdateAudioFrameCapture( IMFSinkWriter& writer, MediaBufferBase*& buffer, MediaEncoderBuffers& bufferOwner, LONGLONG const audioFrameStartOneHNs, LONGLONG const audioFrameDurationOneHNs,
		DWORD const audioStreamIndex DURANGO_ONLY(, MediaEncoder::SampleWriteMonitor const& sampleMonitor ) )
	{
		bool success = false;

		if( videoVerifyf( buffer, "UpdateAudioFrameCapture - Null audio buffer provided!" ) )
		{
			MediaBufferAudioEncode* audioBuffer = static_cast<MediaBufferAudioEncode*>(buffer);
			//size_t bufferSize = audioBuffer->GetUsedSize();

			MediaBufferBlobInterfaceWin& bufferInterface = audioBuffer->GetExternalBufferInterface();
			IMFMediaBuffer* externalBuffer( bufferInterface.GetMediaBuffer() );

			HRESULT hr;
			IMFSample* sample( NULL );

			hr = MFCreateSample( &sample );
			if( videoVerifyf( SUCCEEDED(hr), "UpdateAudioFrameCapture - MFCreateSample failed with 0x%08lx", hr ) )
			{
				hr = sample->AddBuffer( externalBuffer );
				if( videoVerifyf( SUCCEEDED(hr), "UpdateAudioFrameCapture - IMFSample::AddBuffer failed with 0x%08lx", hr ) )
				{
					bufferOwner.ReleaseBuffer( buffer );
					// Release the buffer now that the sample has added a ref. Do not use any elements of the buffer from here on.

					hr = sample->SetSampleTime( audioFrameStartOneHNs );
					success = videoVerifyf( SUCCEEDED(hr), "UpdateAudioFrameCapture - IMFSample::SetSampleTime failed with 0x%08lx", hr );

					hr = sample->SetSampleDuration( audioFrameDurationOneHNs );
					success = videoVerifyf( SUCCEEDED(hr), "UpdateAudioFrameCapture - IMFSample::SetSampleDuration failed with 0x%08lx", hr ) && success;

					videoDebugf3("UpdateAudioCapture: Frame %LLu + %LLu = %LLu = %LLu chasing (%LLu)", 
						(UINT64)audioFrameStartOneHNs, (UINT64)audioFrameDurationOneHNs, (UINT64)audioFrameStartOneHNs + (UINT64)audioFrameDurationOneHNs, 
						NANOSECONDS_TO_ONE_H_NS_UNITS(audDriver::GetMixer()->GetTotalAudioTimeNs()), 
						NANOSECONDS_TO_ONE_H_NS_UNITS(audDriver::GetMixer()->GetTotalVideoTimeNs()));
				}
			}

			success = WriteSampleToStream( writer, sample, audioStreamIndex DURANGO_ONLY(, sampleMonitor ) );
		}

		return success;
	}

	// PURPOSE: Close down and clean-up the given recording elements.
	// PARAMS:
	//		writer - IMFSinkWriter object doing the recording we want to close down.
	bool CleanupRecording( IMFSinkWriter*& writer, bool const cancelling )
	{
		bool success = false;

		if( videoVerifyf( writer, "MediaEncoderWindows::CleanupRecording - Null writer" ) )
		{
			HRESULT hr;

			if( cancelling )
			{
				hr = writer->Flush( (DWORD)MF_SINK_WRITER_ALL_STREAMS );
				videoDisplayf( "CleanupRecording - IMFSinkWriter::Flush - Completed with 0x%08lx", hr );
				success = true;
			}
			else
			{
				hr = writer->NotifyEndOfSegment( (DWORD)MF_SINK_WRITER_ALL_STREAMS );
				videoDisplayf( "CleanupRecording - IMFSinkWriter::NotifyEndOfSegment - Completed with 0x%08lx", hr );

                hr = writer->Finalize();
                videoDisplayf( "CleanupRecording - IMFSinkWriter::Finalize - Completed with 0x%08lx", hr );
				success = videoVerifyf( SUCCEEDED(hr), "CleanupRecording - IMFSinkWriter::Finalize failed with 0x%08lx", hr );
			}

			ReleaseMediaObject( writer );
		}

		return success;
	}

	inline bool GetNextMediaBuffer( rage::MediaEncoderBuffers& bufferInterface, 
		rage::MediaBufferBase*& out_buffer, bool const isFocusingVideo )
	{
		bool hasBuffer = isFocusingVideo ? 
			bufferInterface.PopVideoBuffer( out_buffer ) : bufferInterface.PopAudioBuffer( out_buffer );

		return hasBuffer;
	}

	void MediaEncoder::OnRecordingCancelled()
	{
		USES_CONVERSION;
		wchar_t const * const wideName = UTF8_TO_UTF16( m_threadData.m_targetUri.c_str() );
		DeleteFileW( wideName );
	}

	void MediaEncoder::EncoderThreadFunc( void* dataPtr )
	{
		videoAssertf( dataPtr, "MediaEncoderWindows::EncoderThreadFunc - No thread data provided!" );
		if( dataPtr )
		{
            DURANGO_ONLY( videoAssertf( !MEDIA_MEMORY_WRAPPER.isDecoding(), "MediaEncoder::EncoderThreadFunc - Encoder is attempting to use decoding-only memory! Bad bad bad." ) );

			ThreadDataWrapper* threadDataWrapper( static_cast<ThreadDataWrapper*>( dataPtr ) );
			bool const isProcessingVideo = threadDataWrapper->m_isVideo;
			ThreadData& threadData = threadDataWrapper->m_threadData;
			MediaCommon::ePrepareState threadPrepareState( MediaCommon::STATE_NONE );

			videoDisplayf("Starting MediaEncoder_win::EncoderThreadFunc - Type %s", isProcessingVideo ? "Video" : "Audio" );

			threadPrepareState = MediaCommon::PrepareThread();
			videoAssertf( threadPrepareState == MediaCommon::STATE_PREPARED, "MediaEncoderWindows::EncoderThreadFunc - Thread preparations failed at level %d", (s32)threadPrepareState );
			if( threadPrepareState == MediaCommon::STATE_PREPARED )
			{
				// Video Thread is the main thread for encoding, audio thread waits for it to set things up then starts pumping data like mad
				if( isProcessingVideo )
				{
					VideoThreadFunc( threadData );
				}
				else
				{
					AudioThreadFunc( threadData );
				}
			}
			else
			{
				threadData.m_errorState = MediaCommon::MEDIA_ERROR_UNKNOWN;
			}

			MediaCommon::CleanupThread( threadPrepareState );
			videoAssertf( threadPrepareState == MediaCommon::STATE_NONE, "MediaEncoderWindows::EncoderThreadFunc - Failed to clean up thread! State left as %d", (s32)threadPrepareState );
		}
	}

	void MediaEncoder::VideoThreadFunc( MediaEncoder::ThreadData& threadData )
	{
		MediaEncoderParams& params = threadData.m_encodeParams;
		IMFSinkWriter*& sinkWriter = threadData.m_encoderType;

		u64 videoFrameCount( 0 );
		u64 keyframeInterval = params.GetKeyframeInterval();
		bool cancelled = false;
		threadData.m_encoderState = ENCODER_PREPARING;

		if( videoVerifyf( threadData.m_buffers.Initialize( params ), "MediaEncoderWindows::VideoThreadFunc - Unable to initialize buffers!" ) )
		{
			HRESULT hr;

			IMFDXGIDeviceManager* dxDeviceManager( NULL );
			MediaCommon::GetDxDeviceManager( dxDeviceManager );

#if RSG_PC
			VideoEncoderClassFactory* videoClassFactory = rage_new VideoEncoderClassFactory();
			if( videoClassFactory )
			{
				videoClassFactory->Initialize( params.IsOutputtingWMV(), params.GetQualityLevelPercent(), params.GetSpeedVsQualityPercent() );
			}
#endif

			hr = PrepareForRecording( threadData.m_targetUri.c_str(), params, dxDeviceManager,
				sinkWriter, threadData.m_videoStreamIndex, threadData.m_audioStreamIndex );

			if( hr == E_ACCESSDENIED )
			{
				threadData.m_errorState = MediaCommon::MEDIA_ERROR_ACCESS_DENIED;
				threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
				threadData.m_killFlag = true;
			}
			else if( hr == ERROR_SHARING_VIOLATION )
			{
				threadData.m_errorState = MediaCommon::MEDIA_ERROR_SHARING_VIOLATION;
				threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
				threadData.m_killFlag = true;
			}
			else if( videoVerifyf( SUCCEEDED( hr ),
				"MediaEncoderWindows::EncoderThreadFunc - Failed recording preparations" ) )
			{		
				bool allFrameWritesValid = true;

				hr = sinkWriter->BeginWriting();
				if( videoVerifyf( SUCCEEDED(hr), "IMFSinkWriter::BeginWriting failed to with 0x%08lx", hr ) )
				{
					videoDisplayf( "MediaEncoder::VideoThreadFunc - Began writing file" );

					DURANGO_ONLY( MediaEncoder::SampleWriteMonitor sampleMonitor( threadData ); )

					MediaBufferBase* buffer( NULL );
					threadData.m_encoderType = sinkWriter;
					threadData.m_startTimeNs = MediaTimer::GetConstantTimeNs();

					threadData.m_encoderState = ENCODER_ACTIVE;

					while( sinkWriter && allFrameWritesValid && ( !threadData.m_killFlag || threadData.m_drainFlag ) )
					{
						GetNextMediaBuffer( threadData.m_buffers, buffer, true );

						if( threadData.m_buffers.IsEOFBuffer( buffer ) )
						{
							threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
							threadData.m_buffers.ReleaseBuffer( buffer );
							break;
						}

						PF_PUSH_MARKER("VideoThreadFunc Process Buffer");
						if( buffer )
						{
							u64 const bufferCreationTimeNs = buffer->GetCreationTimeNs();
							u64 bufferDurationNs = buffer->GetDurationNs();		

							bool const c_discardBuffer = ( bufferCreationTimeNs < threadData.m_startTimeNs ) ||
								( threadData.m_pauseFlag && bufferCreationTimeNs >= threadData.m_pauseTimeNs ) ||
								( threadData.m_killFlag && !threadData.m_drainFlag );

							if( !c_discardBuffer )
							{
								u64 const c_bufferDurationOneHNs = NANOSECONDS_TO_ONE_H_NS_UNITS( bufferDurationNs );
								MediaBufferBase::eBufferType const bufferType = buffer->GetType();

								if( bufferType == MediaBufferBase::BUFFER_VIDEO )
								{
#if defined(LOG_VIDEO_FRAME_SUBMISSION)
									utimer_t videoSubmissionStartFrame = sysTimer::GetTicks();
#endif
									allFrameWritesValid = UpdateVideoCapture( *sinkWriter, buffer, threadData.m_buffers, 
										NANOSECONDS_TO_ONE_H_NS_UNITS(threadData.m_currentVideoFrameNs), c_bufferDurationOneHNs, 
										threadData.m_videoStreamIndex, keyframeInterval, videoFrameCount DURANGO_ONLY(, sampleMonitor) );

									if( !allFrameWritesValid )
									{
										threadData.m_errorState = MediaCommon::MEDIA_ERROR_OOM;
									}

									threadData.m_currentVideoFrameNs += bufferDurationNs;//c_bufferDurationOneHNs;

#if defined(LOG_VIDEO_FRAME_SUBMISSION)
									ms_totalFrameSubmitTime += (sysTimer::GetTicks() - videoSubmissionStartFrame) * sysTimer::GetTicksToMilliseconds();
									++ms_totalFramesSubmitted;
#endif
								}
								else
								{
									videoAssertf( false, "Unknown buffer type %u!", bufferType );
								}
							}

							threadData.m_buffers.ReleaseBuffer( buffer );
						}
						PF_POP_MARKER();
					}
				}
				else
				{
					threadData.m_errorState = MediaCommon::MEDIA_ERROR_UNKNOWN;
				}

				threadData.m_encoderState = ENCODER_CLEANUP;

				// Flag audio thread to shut down in-case of failure conditions that mean we never flagged it for shutdown
				// in the above loop
				threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;

				//! Wait until the audio thread has confirmed it is safe to shut down
				while( ( threadData.m_subThreadMessaging & ENC_THREAD_FLAGS_SUB_SHUTDOWN_CONFIRM ) == 0 )
				{
					sysIpcSleep( 1 );
				}

				
				allFrameWritesValid = allFrameWritesValid && ( ( threadData.m_subThreadMessaging & ENC_THREAD_FLAGS_SUB_HAD_ERRORS ) == 0 );

#if RSG_PC
				if (allFrameWritesValid)
				{
					WriteMetadata(sinkWriter, s_titleName, params, NANOSECONDS_TO_ONE_H_NS_UNITS(threadData.m_currentVideoFrameNs ));
				}
#endif

				cancelled = !allFrameWritesValid || ( threadData.m_killFlag && !threadData.m_drainFlag );
				bool const c_cleanupSucceeded = CleanupRecording( sinkWriter, cancelled );	
				threadData.m_errorState = c_cleanupSucceeded ? threadData.m_errorState : MediaCommon::MEDIA_ERROR_UNKNOWN;

				videoDisplayf( "Video Encode %s and ended with error state %u. Exported Duration %u milliseconds (Audio %u milliseconds)", 
					c_cleanupSucceeded ? "was successful" : "failed", threadData.m_errorState,
					NANOSECONDS_TO_MILLISECONDS( threadData.m_currentVideoFrameNs ), 
					NANOSECONDS_TO_MILLISECONDS( threadData.m_currentAudioFrameNs ) );
			}
			else
			{
				threadData.m_errorState = MediaCommon::MEDIA_ERROR_UNKNOWN;
				threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
				threadData.m_killFlag = true;
			}

			threadData.m_encoderState = ENCODER_CLEANUP;

#if RSG_PC
			if( videoClassFactory )
			{
				videoClassFactory->Shutdown();
				videoClassFactory->Release();
			}
#endif
			ReleaseMediaObject( dxDeviceManager );
		}
		else
		{
			threadData.m_errorState = MediaCommon::MEDIA_ERROR_OOM;
			threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
			threadData.m_killFlag = true;
		}

		threadData.m_encoderState = ENCODER_CLEANUP;

		//! If we had errors, then delete the file as it's going to be useless anyway
		if( cancelled || ( threadData.m_errorState != MediaCommon::MEDIA_ERROR_NONE && !PARAM_mmfNoDeleteCorruptFiles.Get() ) )
		{
			USES_CONVERSION;
			wchar_t const * const widePath = UTF8_TO_UTF16( threadData.m_targetUri.c_str() );

			DeleteFileW( widePath );
		}

		threadData.m_buffers.Shutdown();

		PrintOutVideoFramePushData();
		PrintOutVideoFrameSubmissionData();

		threadData.m_encoderState = ENCODER_PENDING_RESET;
	}

	void MediaEncoder::AudioThreadFunc( MediaEncoder::ThreadData& threadData )
	{
		GenerateDummyAudioData();

		MediaBufferBase* buffer( NULL );
		IMFSinkWriter*& sinkWriter = threadData.m_encoderType;
		bool frameWritesValid = true;

		DURANGO_ONLY( MediaEncoder::SampleWriteMonitor sampleMonitor( threadData ); )

		while( frameWritesValid && ( !threadData.m_killFlag || threadData.m_drainFlag ) )
		{
			if( sinkWriter )
			{
				GetNextMediaBuffer( threadData.m_buffers, buffer, false );

				if( threadData.m_buffers.IsEOFBuffer( buffer ) )
				{
					threadData.m_buffers.ReleaseBuffer( buffer );
					break;
				}

				PF_PUSH_MARKER("AudioThreadFunc Process Buffer");
				if( buffer )
				{
					u64 const bufferCreationTimeNs = buffer->GetCreationTimeNs();
					u64 const c_bufferDurationNs = buffer->GetDurationNs();

					bool discardBuffer = ( bufferCreationTimeNs < threadData.m_startTimeNs ) || 
						( threadData.m_pauseFlag && bufferCreationTimeNs >= threadData.m_pauseTimeNs ) ||
						( threadData.m_killFlag && !threadData.m_drainFlag ) ||
						( threadData.m_killFlag && threadData.m_drainFlag && bufferCreationTimeNs >= threadData.m_killTimeNs );

					if( !discardBuffer )
					{
						u64 const c_bufferDurationOneHNs = NANOSECONDS_TO_ONE_H_NS_UNITS( c_bufferDurationNs );
						MediaBufferBase::eBufferType const bufferType = buffer->GetType();

						if( bufferType == MediaBufferBase::BUFFER_AUDIO )
						{
							frameWritesValid = UpdateAudioFrameCapture( *sinkWriter, buffer, threadData.m_buffers, NANOSECONDS_TO_ONE_H_NS_UNITS(threadData.m_currentAudioFrameNs), 
								c_bufferDurationOneHNs, threadData.m_audioStreamIndex DURANGO_ONLY(, sampleMonitor) );

							if( !frameWritesValid )
							{
								threadData.m_errorState = MediaCommon::MEDIA_ERROR_OOM;
							}

							threadData.m_currentAudioFrameNs += c_bufferDurationNs;//c_bufferDurationOneHNs;
						}
						else
						{
							videoAssertf( false, "Unknown buffer type %u!", bufferType );
						}
					}

					threadData.m_buffers.ReleaseBuffer( buffer );
				}
				PF_POP_MARKER();
				
			}
		}

		threadData.m_subThreadMessaging |= frameWritesValid ? 0 : ENC_THREAD_FLAGS_SUB_HAD_ERRORS;
		threadData.m_subThreadMessaging |= ENC_THREAD_FLAGS_SUB_SHUTDOWN_CONFIRM;
	}

} // namespace rage

#endif // RSG_PC && !__RESOURCECOMPILER
