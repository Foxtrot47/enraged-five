/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : media_buffer.h
// PURPOSE : Class representing a single buffer of encoder data
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_BUFFER_H_
#define INC_MEDIAENCODER_BUFFER_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_params.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "grcore/device.h"

// framework
#if RSG_ORBIS
#include "mediaencoder_bufferinterface_rage.h"
#else
#include "mediaencoder_bufferinterface_win.h"
#endif
#include "mediaencoder_colourconversion.h"
#include "video_channel.h"

namespace rage
{
	class MediaEncoderBufferPool;

	//! Base for other media buffer types
	class MediaBufferBase
	{
	public: // declarations and variables
		enum eBufferType
		{
			BUFFER_INVALID = -1,

			BUFFER_AUDIO,
			BUFFER_VIDEO,

			BUFFER_MAX
		};

	public: // methods
		MediaBufferBase();
		virtual ~MediaBufferBase();

		virtual bool IsInitialized() const { return IsValidBufferType( m_type ); }
		virtual void Reinitialize() = 0;
		virtual void Shutdown();

		inline u64 GetDurationNs() const { return m_durationNs; }
		inline void SetDurationNs( u64 const durationNs ) { m_durationNs = durationNs; }
		inline u64 GetCreationTimeNs() const { return m_creationTimeNs; }
		inline void SetCreationTimeNs( u64 const creationTimeNs ) { m_creationTimeNs = creationTimeNs; }

		inline eBufferType GetType() const { return m_type; }
		static inline bool IsValidBufferType( eBufferType const type )
		{
			return type > BUFFER_INVALID && type < BUFFER_MAX;
		}

	protected: // methods
		inline void SetType( eBufferType const type ) { m_type = type; }

	private: // declarations and variables

		eBufferType		m_type;
		u64				m_creationTimeNs;
		u64				m_durationNs;

	private: // methods
		
		//! Disallow copying
		MediaBufferBase( MediaBufferBase const& other );
		MediaBufferBase& operator=(const MediaBufferBase& rhs);
	};

	//! Base for other media buffer types
	class MediaBufferBlob : public MediaBufferBase
	{
	public: // methods
		MediaBufferBlob();
		virtual ~MediaBufferBlob() {}

		virtual bool Initialize( size_t const sourceSize, eBufferType const type ) = 0;
		virtual bool IsInitialized() const 
		{ 
			return m_size > 0 && MediaBufferBase::IsInitialized(); 
		}

		virtual void Reinitialize()
		{
			size_t const bufferSize = GetSize();
			eBufferType const type = GetType();

			Shutdown();
			videoVerifyf( Initialize( bufferSize, type ), "MediaBufferBlob::Reinitialize Unable to reinitialize!" ) ;
		}

		virtual void Shutdown();

		u8 const* GetBytesReadOnly() const { return m_buffer; }

		virtual void Set( u8 const * const source, size_t const sourceSize );
		virtual void Set( u8 const * const source, size_t const elements, size_t const elementSize, size_t const padding );

		virtual void Append( u8 const * const source, size_t const sourceSize );

		inline size_t GetSize() const { return m_size; }

		void SetSizeUsed( size_t const sizeUsed );
		inline size_t GetUsedSize() const { return m_usedSize; }
		inline size_t GetSizeRemaining() const { return GetSize() - GetUsedSize(); }

	protected: // declarations and variables
		friend class MediaEncoderBufferPool;

		u8*				m_buffer;
		size_t			m_size;

	protected: // methods
		inline void SetUsedSize( size_t const usedSize ) { m_usedSize = usedSize; }
		u8* GetBytes() { return m_buffer; }

	private: // declarations and variables

		size_t			m_usedSize;
		u64				m_creationTimeNs;

	private: // methods

		//! Disallow copying
		MediaBufferBlob( MediaBufferBlob const& other );
		MediaBufferBlob& operator=(const MediaBufferBlob& rhs);
	};

	//! Base for other media buffer types
	class MediaBufferTexture : public MediaBufferBase
	{
	public: // methods
		MediaBufferTexture();
		virtual ~MediaBufferTexture() {}

		void Initialize();
		virtual void Reinitialize() { Shutdown(); Initialize(); }
		bool Set( grcTextureObject* texture, grcFenceHandle fence );
		bool IsSet() const 
		{ 
			return GetTexture() != NULL; 
		}

		void Shutdown();

		grcTextureObject const* GetTexture() const { return m_texture; }

		inline grcFenceHandle& GetFenceRef() { return m_fence; }
		inline bool HasFence() const { return m_fence != NULL; }

	private: // declarations and variables
		grcTextureObject*	m_texture;
		grcFenceHandle		m_fence;

	private: // methods

		//! Disallow copying
		MediaBufferTexture( MediaBufferTexture const& other );
		MediaBufferTexture& operator=(const MediaBufferTexture& rhs);
	};

	//! Media Buffer blob that uses external allocation
	template< class _externalBufferBlobType >
	class MediaBufferExternalBlob : public MediaBufferBlob
	{
	public: // methods
		MediaBufferExternalBlob() : MediaBufferBlob() {};
		virtual ~MediaBufferExternalBlob() {};

		virtual bool Initialize( size_t const sourceSize, eBufferType const type )
		{
			Shutdown();

			if( videoVerifyf( sourceSize > 0, "MediaBufferExternal::Initialize - Zero size source" ) && 
				videoVerifyf( IsValidBufferType( type ), "MediaBufferExternal::Initialize - Unknown type %u", type ) )
			{
				if( videoVerifyf( m_externalBufferInterface.Initialize( sourceSize ), 
					"MediaBufferExternal::Initialize - Unable to initialize external allocator!" ) )
				{
					m_size = sourceSize;
					SetType( type );
				}
			}

			return IsInitialized();
		}

		virtual void Shutdown()
		{
			m_externalBufferInterface.Shutdown();
			MediaBufferBlob::Shutdown();
		}

		virtual void Set( u8 const * const source, size_t const sourceSize )
		{
			m_externalBufferInterface.Lock();
			m_buffer = m_externalBufferInterface.GetBytes();
			MediaBufferBlob::Set( source, sourceSize );
			m_buffer = NULL;
			m_externalBufferInterface.SetSizeUsed( GetUsedSize() );
			m_externalBufferInterface.Unlock();
		}

		virtual void Set( u8 const * const source, size_t const elements, size_t const elementSize, size_t const padding )
		{
			m_externalBufferInterface.Lock();
			m_buffer = m_externalBufferInterface.GetBytes();
			MediaBufferBlob::Set( source, elements, elementSize, padding );
			m_buffer = NULL;
			m_externalBufferInterface.SetSizeUsed( GetUsedSize() );
			m_externalBufferInterface.Unlock();
		}

		virtual void Append( u8 const * const source, size_t const sourceSize )
		{
			m_externalBufferInterface.Lock();
			m_buffer = m_externalBufferInterface.GetBytes();
			MediaBufferBlob::Append( source, sourceSize );
			m_buffer = NULL;
			m_externalBufferInterface.SetSizeUsed( GetUsedSize() );
			m_externalBufferInterface.Unlock();
		}

		_externalBufferBlobType& GetExternalBufferInterface() { return m_externalBufferInterface; }

	private: // declarations and variables
		_externalBufferBlobType m_externalBufferInterface;

	private: // methods

		//! Disallow copying
		MediaBufferExternalBlob( MediaBufferExternalBlob const& other );
		MediaBufferExternalBlob& operator=(const MediaBufferExternalBlob& rhs);
	};

	//! Media Buffer texture that uses external allocation
	template< class _externalBufferTextureType >
	class MediaBufferExternalTexture : public MediaBufferTexture
	{
	public: // methods
		MediaBufferExternalTexture() : MediaBufferTexture() {};
		virtual ~MediaBufferExternalTexture() {};

		virtual bool Set( grcTextureObject* texture, grcFenceHandle fence )
		{
			if( MediaBufferTexture::Set( texture, fence ) )
			{
				m_externalBufferInterface.Initialize( texture );
			}

			return IsSet();
		}

		virtual void Shutdown()
		{
			m_externalBufferInterface.Shutdown();
			MediaBufferTexture::Shutdown();
		}

		_externalBufferTextureType& GetExternalBufferInterface() { return m_externalBufferInterface; }

	private: // declarations and variables
		_externalBufferTextureType m_externalBufferInterface;

	private: // methods

		//! Disallow copying
		MediaBufferExternalTexture( MediaBufferExternalTexture const& other );
		MediaBufferExternalTexture& operator=(const MediaBufferExternalTexture& rhs);
	};

	template< class _externalBufferBlobType >
	class MediaBufferBlobNV12FromRGB32 : public MediaBufferExternalBlob<_externalBufferBlobType>
	{
	public:
		void InitializeWithSize( u32 const outputWidth, u32 const outputHeight )
		{
			size_t const c_YSize =  ( outputWidth * outputHeight );
			size_t const c_otherSize =  c_YSize / 4;
			size_t const c_frameSize = c_YSize + c_otherSize + c_otherSize;

			MediaBufferExternalBlob<_externalBufferBlobType>::Initialize( c_frameSize, MediaBufferBase::BUFFER_VIDEO );
			m_outputWidth = outputWidth;
			m_outputHeight = outputHeight;
		}

		virtual void Set( u8 const * const source, size_t const sourceSize )
		{
			(void)sourceSize;
			SetFromRGBABlob( source, 0 );
		}

		virtual void Set( u8 const * const source, size_t const elements, size_t const elementSize, size_t const widthPadding )
		{
			(void)elements;
			(void)elementSize;
			SetFromRGBABlob( source, widthPadding );
		}

		void SetFromRGBABlob( u8 const * const source, size_t const widthPadding )
		{
			_externalBufferBlobType& externalInterface = MediaBufferExternalBlob<_externalBufferBlobType>::GetExternalBufferInterface();

			externalInterface.Lock();
			u8* externalBuffer = externalInterface.GetBytes();
			size_t const c_totalSize = MediaBufferExternalBlob<_externalBufferBlobType>::GetSize();

			if( videoVerifyf( externalBuffer, "MediaBufferBlobRGBToNV12Converion::Set - NULL dest data" ) &&
				videoVerifyf( source, "MediaBufferBlobRGBToNV12Converion::Set - NULL source data" ) )
			{
				MediaEncoderColorConversion::ConvertRGBToNV12_CPU( externalBuffer, source, m_outputWidth, m_outputHeight, (int)widthPadding );
				MediaBufferExternalBlob<_externalBufferBlobType>::SetUsedSize( c_totalSize );
			}

			externalInterface.SetSizeUsed( c_totalSize );
			externalInterface.Unlock();
		}

		virtual void Append( u8 const * const source, size_t const sourceSize )
		{
			videoAssertf( false, "MediaBufferBlobRGBToNV12Converion::Append - not supported!" );
			(void)source;
			(void)sourceSize;
		}

	protected:
		// Hiding this from public view, as it's not valid externally
		using MediaBufferExternalBlob<_externalBufferBlobType>::Initialize;

	private:
		u32 m_outputWidth;
		u32 m_outputHeight;
	};


#if RSG_ORBIS
	typedef MediaBufferBlobNV12FromRGB32<MediaBufferBlobInterfaceRage> MediaBufferVideoEncode;
	typedef MediaBufferExternalBlob<MediaBufferBlobInterfaceRage> MediaBufferAudioEncode;
#else

#if RSG_DURANGO
	typedef MediaBufferExternalTexture<MediaBufferTextureInterfaceWin> MediaBufferVideoEncode;
#elif RSG_PC
	typedef MediaBufferBlobNV12FromRGB32<MediaBufferBlobInterfaceWin> MediaBufferVideoEncode;
#endif

	typedef MediaBufferExternalBlob<MediaBufferBlobInterfaceWin> MediaBufferAudioEncode;
#endif
}

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER

#endif // INC_MEDIAENCODER_BUFFER_H_
