/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_bufferpool.h
// PURPOSE : Class for accessing a pool of available buffers
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_BUFFERPOOL_H_
#define INC_MEDIAENCODER_BUFFERPOOL_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_params.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "atl/bitset.h"

//framework
#include "media_buffer.h"
#include "mediaencoder_params.h"
#include "mediaencoder_texture_pool.h"

namespace rage
{
	class MediaEncoderBufferPool
	{
	public: // declarations and variables
		static u32 const sc_audioBufferCount = 196;
#if RSG_DURANGO
		static u32 const sc_videoBufferCount = MEDIA_EXPORT_MAX_TEXTURES - 2;
#elif RSG_ORBIS
		static u32 const sc_videoBufferCount = 2;
#else // was PC
		static u32 const sc_videoBufferCount = 6;
#endif
		static u32 const sc_totalBufferCount = sc_audioBufferCount + sc_videoBufferCount;
		
	public: // methods
		bool Initialize( MediaEncoderParams const& captureParameters );
		bool IsInitialized() const;
		void Shutdown();
		
		bool IsVideoQueueFull() const;
		MediaBufferVideoEncode* TryGetVideoBuffer();
		void ReleaseVideoBuffer( MediaBufferVideoEncode*& buffer );
		
		bool IsAudioQueueFull() const;
		MediaBufferAudioEncode* TryGetAudioBuffer( size_t const sizeNeeded );
		void ReleaseAudioBuffer( MediaBufferAudioEncode*& buffer );

		MediaBufferAudioEncode* TryGetLastReturnedAudioBuffer( size_t const sizeNeeded );
		inline void InvalidateLastReturnedAudioBuffer() { m_lastReturnedAudioBuffer = -1; }

		void ReleaseUnknownBufferType( MediaBufferBase*& buffer );
		
	private: // declarations and variables		
		
		MediaBufferVideoEncode				m_videoBuffers[ sc_videoBufferCount ];
		atFixedBitSet<sc_videoBufferCount>	m_videoBufferUsedFlags;

		MediaBufferAudioEncode				m_audioBuffers[ sc_audioBufferCount ];
		atFixedBitSet<sc_audioBufferCount>	m_audioBufferUsedFlags;

		s32									m_lastReturnedAudioBuffer;
		bool								m_initialized;

	private: // methods
		
		void InitializeVideoBuffers( MediaEncoderParams const& captureParameters );
		void InitializeAudioBuffers( size_t const sizeNeededPerFrame );

		void ReleaseBuffersInternal();
	};

}

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER

#endif // INC_MEDIAENCODER_BUFFERPOOL_H_
