/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_texture_pool.h
// PURPOSE : Class for allocating/managing a pool of textures for media encoding
//
// NOTES : This is only available on Xb1, and it circumvents RAGE texture creation
//		   to work around a bug in the Microsoft June XDK.
//
//		   Also, this class will need rework if we ever support simultaneous encoding.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_TEXTURE_POOL_H_
#define INC_MEDIAENCODER_TEXTURE_POOL_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if (RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "atl/array.h"

// framework
#include "media_common.h"

struct ID3D11Texture2D;

#define MEDIA_EXPORT_MAX_TEXTURES 6

namespace rage
{
	class MediaEncoderTexturePool
	{
	public:
		MediaEncoderTexturePool();
		~MediaEncoderTexturePool();

		bool Initialize( u32 const c_width, u32 const c_height );
		bool AreTexturesAllocated() const;
		void Shutdown();

		ID3D11Texture2D* GetNextTexture();

	private:
		ID3D11Texture2D*	ms_texturePool[ MEDIA_EXPORT_MAX_TEXTURES ];
		u32					ms_nextTextureToReturn;
	};

	extern MediaEncoderTexturePool ENCODER_TEXTURE_POOL;

}

#endif // (RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

#endif // INC_MEDIAENCODER_TEXTURE_POOL_H_
