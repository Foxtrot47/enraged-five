#ifndef _GREATESTMOMENT_DURANGO_H_
#define _GREATESTMOMENT_DURANGO_H_

#if RSG_DURANGO

#include "greatestmoment.h"

namespace rage
{

class fwGreatestMomentTaskDurango : public fwGreatestMomentTask
{
public:

	fwGreatestMomentTaskDurango();
	~fwGreatestMomentTaskDurango();

	void	RecordTimespan(u32 userIndex, const char *id, const char* displayName, s32 startOffset, u32 duration);
	void	StartRecording(u32 userIndex, const char *id);
	void	StopRecording();
	void	CancelRecording();

	u32		GetHashID();
	void	Update();

private:
	void RecordLimitExceeded(Windows::Xbox::Media::Capture::ApplicationClipCapture^ appCapture);

	void Failed();
	void Completed(Windows::Foundation::IAsyncOperationWithProgress< Windows::Xbox::Media::Capture::ApplicationClip^, Windows::Xbox::Media::Capture::ApplicationClip^ >^ operation, Windows::Foundation::AsyncStatus status);


	Windows::Xbox::Media::Capture::ApplicationClip			^m_Clip;			// Filled in on successful clip creation
	Windows::Xbox::Media::Capture::ApplicationClipInfo		^m_ClipInfo;
	Windows::Xbox::Media::Capture::ApplicationClipCapture	^m_ClipCapture;
	Windows::Xbox::System::User								^m_User;
};

class fwGreatestMomentDurango: public fwGreatestMoment
{
public:
	static fwGreatestMomentDurango& GetPlatformInstance();

	void	EnableRecording();
	void	DisableRecording();
	void	RecordGreatestMoment(u32 userIndex, const char* id, const char* displayName, s32 startTime, u32 duration);

protected:
	void	InitInstance(unsigned initMode);
	void	UpdateInstance();
	void	ShutdownInstance(unsigned shutdownMode);

private:
	fwGreatestMomentDurango();
	virtual ~fwGreatestMomentDurango();

	atFixedArray<fwGreatestMomentTaskDurango, 2> m_MomentTasks;

};

}	//namespace rage

#endif	//RSG_DURANGO

#endif // _GREATESTMOMENT_DURANGO_H_
