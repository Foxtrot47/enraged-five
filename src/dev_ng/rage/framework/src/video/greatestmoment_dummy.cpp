#if ( !RSG_DURANGO )

// For platforms that don't support video recording.

#include "greatestmoment_dummy.h"

namespace rage
{
	fwGreatestMoment& fwGreatestMoment::GetInstance()
	{
		static fwGreatestMomentDummy greatestMoment;
		return greatestMoment;
	}

	fwGreatestMomentDummy::fwGreatestMomentDummy() : fwGreatestMoment()
	{
	}

	fwGreatestMomentDummy::~fwGreatestMomentDummy()
	{
	}

	void fwGreatestMomentDummy::RecordGreatestMoment(u32 UNUSED_PARAM(userIndex), const char* UNUSED_PARAM(id), const char* UNUSED_PARAM(DisplayName), s32 UNUSED_PARAM(startTime), u32 UNUSED_PARAM(duration))
	{
	}

	void fwGreatestMomentDummy::EnableRecording()
	{
	}

	void fwGreatestMomentDummy::DisableRecording()
	{
	}

	void fwGreatestMomentDummy::InitInstance(unsigned UNUSED_PARAM(initMode))
	{
	}

	void fwGreatestMomentDummy::UpdateInstance()
	{
	}

	void fwGreatestMomentDummy::ShutdownInstance(unsigned UNUSED_PARAM(shutdownMode))
	{
	}

}

#endif