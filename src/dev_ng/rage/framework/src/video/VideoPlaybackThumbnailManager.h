/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlaybackThumbnailManager.h
// PURPOSE : Class for maintaining thumbnails generated from video playback instances
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "video/VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#ifndef INC_VIDEO_PLAYBACK_THUMBNAIL_MANAGER_H_
#define INC_VIDEO_PLAYBACK_THUMBNAIL_MANAGER_H_

// rage
#include "atl/array.h"
#include "atl/string.h"
#include "atl/hashstring.h"

// framework
#include "fwscene/stores/imposedTextureDictionary.h"
#include "video/ImageLoader.h"
#include "video/VideoPlayback.h"

namespace rage
{
	class grcRenderTarget;
	class grcViewport;

	class VideoPlaybackThumbnailManager
	{
	public: // declarations and variable 

#define VIDEO_THUMBS_MAX_TXD_ENTRIES (2)

	public: // methods

		static void Init( unsigned initMode );
		static bool IsInitialized();
		static void Shutdown( unsigned shutdownMode );

		static void Activate();
		static bool IsActive();
		static void Deactivate();

		static void Update();
		static void UpdateThumbnailFrames();

        static bool canRequestImages();

		static bool isRequestingImage();
		static bool isImageRequested( atFinalHashString const& textureName );
		static bool isImageLoaded( atFinalHashString const& textureName );
		static bool hasImageRequestFailed( atFinalHashString const& textureName );

		static bool requestImage( char const * const filename, atFinalHashString const& textureName );
		static void releaseImage( atFinalHashString const& textureName );
		static void releaseAllImages();

	private: // declarations and variables

		struct VideoThumbnailBuffer
		{
			enum eLoadState
			{
				LOAD_STATE_INVALID,
				LOAD_STATE_LOADING,
				LOAD_STATE_POPULATED,
				LOAD_STATE_FAILED
			};

			fwImposedTextureDictionary	m_textureDictionary;
			atString					m_file;
			u32							m_id;
			grcRenderTarget*			m_target;
#if RSG_PC
			grcTexture*					m_texture;
#endif
			u32							m_loadState;
            u32                         m_refCount;
			
			VideoThumbnailBuffer()
				: m_textureDictionary()
				, m_file()
				, m_id()
				, m_target( NULL )
#if RSG_PC
				, m_texture( NULL )
#endif
				, m_loadState( LOAD_STATE_INVALID )
                , m_refCount( 1 )
			{

			}

			~VideoThumbnailBuffer()
			{
				Cleanup();
			}

			inline bool IsFree() const				{ return m_id == 0; }

			inline bool IsLoaded() const			{ return m_loadState == LOAD_STATE_POPULATED; }
			inline void SetLoaded()					{ m_loadState = LOAD_STATE_POPULATED; }

			inline bool HasLoadFailed() const		{ return m_loadState == LOAD_STATE_FAILED; }
			inline void SetLoadFailed()				{ m_loadState = LOAD_STATE_FAILED; }
			
			inline bool RequiresLoading() const		{ return !IsFree() && !IsLoaded() && !HasLoadFailed(); }
			inline void SetLoadAttempted()			{ m_loadState = LOAD_STATE_LOADING; }

			inline bool RequiresAllocation() const { return m_target == NULL; }

            u32 Release();

		private:
			VideoThumbnailBuffer( VideoThumbnailBuffer const& other );
			VideoThumbnailBuffer& operator=(const VideoThumbnailBuffer& rhs);

            void Cleanup();
		};

		typedef atFixedArray< VideoThumbnailBuffer*, VIDEO_THUMBS_MAX_TXD_ENTRIES > VideoThumbnailEntries;

		static grcViewport*					    ms_conversionViewport;
        static VideoThumbnailEntries		    ms_thumbnailEntries;
		static VideoThumbnailEntries			ms_thumbsToRemove;

		static VideoInstanceHandle			    ms_playbackHandle;
        static VideoThumbnailBuffer*            ms_stagingEntry;
        static sysCriticalSectionToken		    ms_stagingCsToken;

	private: // methods

		static bool CreateThumbnailTarget( grcRenderTarget*& thumbnailTarget WIN32PC_ONLY(, grcTexture*& thumbnailTexture) );
		static bool CreateViewport( grcViewport*& conversionViewport );
		//static bool CreateThumbnailTexture( grcTexture*& thumbnailTexture, void* srcData );
		static void DestroyThumbnailTarget( grcRenderTarget*& thumbnailTarget WIN32PC_ONLY(, grcTexture*& thumbnailTexture) );
		static void DestroyViewport( grcViewport*& conversionViewport );

		static VideoThumbnailBuffer* getThumbnailEntryInternal( atFinalHashString const& textureName );
		static VideoThumbnailBuffer* getThumbnailEntryInternal( u32 const textureHash );

		static void ReleaseThumbnailInternal( VideoThumbnailBuffer*& inout_thumbnailEntry );
		static void PerformReleaseImageList();
	};

	//! Adapts our video playback thumbnail manager to be used in an ImageLoader context
	class VideoPlaybackThumbnailManagerAdapter : public rage::IImageLoader
	{
	public:
		VideoPlaybackThumbnailManagerAdapter() {}
		virtual ~VideoPlaybackThumbnailManagerAdapter() {}

		virtual bool isImageRequested( atFinalHashString const& textureName ) const
		{
			return VideoPlaybackThumbnailManager::isImageRequested( textureName );
		}

		virtual bool isImageLoaded( atFinalHashString const& textureName ) const
		{
			return VideoPlaybackThumbnailManager::isImageLoaded( textureName );
		}

		virtual bool hasImageRequestFailed( atFinalHashString const& textureName ) const
		{
			return VideoPlaybackThumbnailManager::hasImageRequestFailed( textureName );
		}

        virtual bool canRequestImages() const
        {
            return VideoPlaybackThumbnailManager::canRequestImages();
        }

		virtual bool requestImage( char const * const filename, atFinalHashString const& textureName, u32 const downscale )
		{	
			(void)downscale;
			return VideoPlaybackThumbnailManager::requestImage( filename, textureName );
		}

		virtual void releaseImage( atFinalHashString const& streamingName )
		{
			VideoPlaybackThumbnailManager::releaseImage( streamingName );
		}

		virtual void releaseAllImages()
		{
			VideoPlaybackThumbnailManager::releaseAllImages();
		}

	};

	static VideoPlaybackThumbnailManagerAdapter g_videoThumbnailLoader;

} // namespace rage

#endif // INC_VIDEO_PLAYBACK_THUMBNAIL_MANAGER_H_

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
