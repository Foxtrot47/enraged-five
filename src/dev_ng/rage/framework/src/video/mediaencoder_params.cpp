/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_params.cpp
// PURPOSE : Parameters for encoding video/audio streams.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediaencoder_params.h"

#if  !__RESOURCECOMPILER

#if (RSG_PC || RSG_DURANGO)

#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <mfapi.h>
#include <Mfidl.h>
#pragma warning(pop)

#endif // RSG_PC

// framework
#include "video_channel.h"

namespace rage
{
    enum eDurationQualityTier
    {
        DURATION_QUALITY_ZERO = 0,
        DURATION_QUALITY_ONE,
        DURATION_QUALITY_TWO,

        MAX_DURATION_QUALITY_TIER
    };

    enum eDurationQualityThresholds
    {
        DURATION_QUALITY_THRESHOLD_ZERO = 10,
        DURATION_QUALITY_THRESHOLD_ONE = 20,
        DURATION_QUALITY_THRESHOLD_TWO = 30,

        MAX_DURATION_QUALITY_THRESHOLD
    };

#define QUALITY_LEVEL_TIERS_TOTAL ( MAX_DURATION_QUALITY_TIER * rage::MediaEncoderParams::QUALITY_MAX )
#define DEFAULT_QUALITY_TIER (DURATION_QUALITY_ZERO)

/* Windows 8 and above specific. I lack the ability to tune these on my Win7 machine, so 
       it currently uses the standard quality percent values. */
//#define USE_WIN8_ENCODE_SPEED_VALUES 1

	//! Quality tiers are time based, split as higher to lower. So the longer a video, the lower the quality gets 
	//! to keep file sizes sane.
	static u32 const s_videoQualityLevelBitRatesWMV[ QUALITY_LEVEL_TIERS_TOTAL ] = 
	{
		1280000, 2560000, 5120000, 10240000,
		1280000, 2560000, 5120000, 10240000,
		1280000, 2560000, 5120000, 10240000
	};


	static u32 const s_videoQualityLevelBitRatesOthers[ QUALITY_LEVEL_TIERS_TOTAL ] =
	{
#if RSG_DURANGO
		4500000, 6000000, 6700000,  7200000,
		3600000, 4500000, 6000000,  6700000,
		2400000, 3600000, 4500000,  6000000
#else
		//! On PC as we use quality based encoding on Win 7+, and WMV on Vista
		3600000, 5000000, 720000,  10240000,
		3600000, 5000000, 720000,  10240000,
		3600000, 5000000, 720000,  10240000
#endif
	};

	static u32 const s_videoQualityLevelPercent[ QUALITY_LEVEL_TIERS_TOTAL ] = 
	{
		25, 50, 75, 100,
		20, 45, 69, 90,
		15, 35, 64, 80
	};

#if defined( USE_WIN8_ENCODE_SPEED_VALUES ) && USE_WIN8_ENCODE_SPEED_VALUES
	
    static u32 const s_videoSpeedVsQualityPercent[ QUALITY_LEVEL_TIERS_TOTAL ] = 
	{
        25, 50, 75, 100,
        20, 45, 69, 90,
        15, 35, 64, 80,
	};

#endif // defined( USE_WIN8_ENCODE_SPEED_VALUES ) && USE_WIN8_ENCODE_SPEED_VALUES

	static char const * const s_qualityLevelDisplayNames[ rage::MediaEncoderParams::QUALITY_MAX ] = 
	{
		"VEUI_QUAL_LOW", "VEUI_QUAL_MED", "VEUI_QUAL_HIGH", "VEUI_QUAL_ULTRA"
	};

	static float const s_outputFps[ rage::MediaEncoderParams::OUTPUT_FPS_MAX ] = 
	{
		29.97f, 59.94f
	};

    static u32 const s_outputFpsKeyframeInterval[ rage::MediaEncoderParams::OUTPUT_FPS_MAX ] = 
    {
        15, 30
    };

	static char const * const s_fpsDisplayNames[ rage::MediaEncoderParams::OUTPUT_FPS_MAX ] = 
	{
		"VEUI_FPS_THIRTY", "VEUI_FPS_SIXTY"
	};

	static u32 s_audioOutputBlockAlignWMA[ MediaEncoderParams::QUALITY_MAX ] = 
	{
		4096, 6827, 8192, 8192
	};

	static u32 s_audioOutputBlockAlignOthers[ MediaEncoderParams::QUALITY_MAX ] = 
	{
		16, 16, 16, 16
	};

	static u32 s_audioOutputBitrateQualityWMA[ rage::MediaEncoderParams::QUALITY_MAX ] = 
	{
		12000, 20001, 24000, 24000
	};

	static u32 s_audioOutputBitrateQualityOthers[ rage::MediaEncoderParams::QUALITY_MAX ] = 
	{
#if RSG_DURANGO
		12000, 12000, 12000, 20000
#else
        12000, 20000, 24000, 24000
#endif
	};

	float MediaEncoderParams::GetOutputFrameDurationMs( MediaEncoderParams::eOutputFps const outputFps )
	{
		float const c_outputFpsFloat = GetOutputFpsFloat( outputFps );
		float const c_result = 1000.f / c_outputFpsFloat;
		return c_result;
	}

	float MediaEncoderParams::GetOutputFpsFloat( MediaEncoderParams::eOutputFps const outputFps )
	{
		return videoVerifyf( outputFps >= MediaEncoderParams::OUTPUT_FPS_FIRST && outputFps < MediaEncoderParams::OUTPUT_FPS_MAX, 
			"MediaEncoderParams::GetOutputFps - Requesting out of bounds fps %u", (u32)outputFps ) ? 
			s_outputFps[ outputFps ] : s_outputFps[ MediaEncoderParams::OUTPUT_FPS_DEFAULT ];
	}

	u32 MediaEncoderParams::GetKeyframeInterval( MediaEncoderParams::eOutputFps const outputFps )
	{
		return videoVerifyf( outputFps >= MediaEncoderParams::OUTPUT_FPS_FIRST && outputFps < MediaEncoderParams::OUTPUT_FPS_MAX, 
			"MediaEncoderParams::GetKeyframeInterval - Requesting out of bounds fps %u", (u32)outputFps ) ? 
			s_outputFpsKeyframeInterval[ outputFps ] : s_outputFpsKeyframeInterval[ MediaEncoderParams::OUTPUT_FPS_DEFAULT ];
	}

	char const * MediaEncoderParams::GetOutputFpsDisplayName( MediaEncoderParams::eOutputFps const outputFps )
	{
		return videoVerifyf( outputFps >= MediaEncoderParams::OUTPUT_FPS_FIRST && outputFps < MediaEncoderParams::OUTPUT_FPS_MAX, 
			"MediaEncoderParams::GetOutputFpsDisplayName - Requesting out of bounds fps %u", (u32)outputFps ) ? 
			s_fpsDisplayNames[ outputFps ] : s_fpsDisplayNames[ MediaEncoderParams::OUTPUT_FPS_DEFAULT ];
	}

	u32 MediaEncoderParams::GetDurationQualityTier( u32 const durationMs )
	{
		u32 result = DURATION_QUALITY_ZERO;

		u32 const c_timeMinutes = ( durationMs / 1000 ) / 60;

		if( c_timeMinutes >= DURATION_QUALITY_THRESHOLD_ONE )
		{
			result = DURATION_QUALITY_TWO;
		}
		else if( c_timeMinutes >= DURATION_QUALITY_THRESHOLD_ZERO )
		{
			result = DURATION_QUALITY_ONE;
		}

		return result;
	}

	u32 MediaEncoderParams::GetQualityLevelVideoBitRate( MediaCommon::eVideoFormat const format, MediaEncoderParams::eQualityLevel const qualityLevel, u32 const durationMs )
	{
		u32 const * const c_bitrateArray = IsOutputtingWMV( format ) ? s_videoQualityLevelBitRatesWMV : s_videoQualityLevelBitRatesOthers;

		u32 const c_qualityTier = GetDurationQualityTier( durationMs );
		s32 const c_effectiveQualityIndex = ( c_qualityTier * rage::MediaEncoderParams::QUALITY_MAX ) + qualityLevel;

		return videoVerifyf( c_effectiveQualityIndex >= 0 && c_effectiveQualityIndex < QUALITY_LEVEL_TIERS_TOTAL, 
			"MediaEncoderParams::GetQualityLevelVideoBitRate - Requesting out of bounds quality %d, with tier %u and raw level %d", c_effectiveQualityIndex, c_qualityTier, (s32)qualityLevel ) ? 
			c_bitrateArray[ c_effectiveQualityIndex ] : c_bitrateArray[ MediaEncoderParams::QUALITY_DEFAULT * DEFAULT_QUALITY_TIER ];
	}

	u32 MediaEncoderParams::GetQualityLevelPercent( MediaEncoderParams::eQualityLevel const qualityLevel, u32 const durationMs )
	{
		u32 const c_qualityTier = MediaCommon::IsWindows8OrGreater() ? 0 : GetDurationQualityTier( durationMs );
		s32 const c_effectiveQualityIndex = ( c_qualityTier * rage::MediaEncoderParams::QUALITY_MAX ) + qualityLevel;

		return videoVerifyf( c_effectiveQualityIndex >= 0 && c_effectiveQualityIndex < QUALITY_LEVEL_TIERS_TOTAL,  
			"MediaEncoderParams::GetQualityLevelPercent - Requesting out of bounds quality %d, with tier %u and raw level %d", c_effectiveQualityIndex, c_qualityTier, (s32)qualityLevel ) ?
			s_videoQualityLevelPercent[ c_effectiveQualityIndex ] : s_videoQualityLevelPercent[ MediaEncoderParams::QUALITY_DEFAULT * DEFAULT_QUALITY_TIER ];
	}

	u32 MediaEncoderParams::GetSpeedVsQualityPercent( MediaEncoderParams::eQualityLevel const qualityLevel, u32 const durationMs )
	{
#if defined( USE_WIN8_ENCODE_SPEED_VALUES ) && USE_WIN8_ENCODE_SPEED_VALUES
		u32 const c_qualityTier = MediaCommon::IsWindows8OrGreater() ? 0 : GetDurationQualityTier( durationMs );
		s32 const c_effectiveQualityIndex = ( c_qualityTier * rage::MediaEncoderParams::QUALITY_MAX ) + qualityLevel;

		return videoVerifyf( c_effectiveQualityIndex >= 0 && c_effectiveQualityIndex < QUALITY_LEVEL_TIERS_TOTAL,  
			"MediaEncoderParams::GetSpeedVsQualityPercent - Requesting out of bounds quality %d, with tier %u and raw level %d", c_effectiveQualityIndex, c_qualityTier, (s32)qualityLevel ) ?
			s_videoSpeedVsQualityPercent[ c_effectiveQualityIndex ] : s_videoSpeedVsQualityPercent[ MediaEncoderParams::QUALITY_DEFAULT * DEFAULT_QUALITY_TIER ];

#else

		return GetQualityLevelPercent( qualityLevel, durationMs );

#endif // defined( USE_WIN8_ENCODE_SPEED_VALUES ) && USE_WIN8_ENCODE_SPEED_VALUES
	}

	char const * MediaEncoderParams::GetQualityLevelDisplayName( MediaEncoderParams::eQualityLevel const qualityLevel )
	{
		return videoVerifyf( qualityLevel >= MediaEncoderParams::QUALITY_FIRST && qualityLevel < MediaEncoderParams::QUALITY_MAX, 
			"MediaEncoderParams::GetQualityLevelDisplayName - Requesting out of bounds quality %u", (u32)qualityLevel ) ? 
			s_qualityLevelDisplayNames[ qualityLevel ] : s_qualityLevelDisplayNames[ MediaEncoderParams::QUALITY_DEFAULT ];
	}

	void MediaEncoderParams::SetOutputFormat( MediaCommon::eVideoFormat const videoFormat )
	{
		bool const c_isSupported = MediaCommon::IsSupportedVideoFormat( videoFormat );
		videoAssertf( c_isSupported, "MediaEncoderParams::SetOutputFormat - Attempting to set unsupported format %u for export", videoFormat );

		m_videoEncodingFormat = c_isSupported ? videoFormat : MediaCommon::VIDEO_FORMAT_INVALID;
		m_audioEncodingFormat = MediaCommon::GetDefaultAudioFormatForVideoFormat( m_videoEncodingFormat );
	}

	bool MediaEncoderParams::IsOutputtingWMV( MediaCommon::eVideoFormat const format )
	{
		return format == MediaCommon::VIDEO_FORMAT_WMV;
	}

	bool MediaEncoderParams::IsOutputtingWMA( MediaCommon::eAudioFormat const format )
	{
		return format == MediaCommon::AUDIO_FORMAT_WMA;
	}

	u32 MediaEncoderParams::GetAudioOutputBlockAlign( MediaCommon::eAudioFormat const format, MediaEncoderParams::eQualityLevel const qualityLevel )
	{
		u32 const * const c_blockAlignArray = IsOutputtingWMA( format ) ? s_audioOutputBlockAlignWMA : s_audioOutputBlockAlignOthers;

		return videoVerifyf( qualityLevel >= MediaEncoderParams::QUALITY_FIRST && qualityLevel < MediaEncoderParams::QUALITY_MAX, 
			"MediaEncoderParams::GetAudioOutputBlockAlign - Requesting out of bounds quality %u", (u32)qualityLevel ) ? 
			c_blockAlignArray[ qualityLevel ] : c_blockAlignArray[ MediaEncoderParams::QUALITY_DEFAULT ];
	}

	u32 MediaEncoderParams::GetAudioOutputBytesPerSecond( MediaCommon::eAudioFormat const format, MediaEncoderParams::eQualityLevel const qualityLevel )
	{
		u32 const * const c_audioBitrateArray = IsOutputtingWMA( format ) ? s_audioOutputBitrateQualityWMA : s_audioOutputBitrateQualityOthers;

		return videoVerifyf( qualityLevel >= MediaEncoderParams::QUALITY_FIRST && qualityLevel < MediaEncoderParams::QUALITY_MAX, 
			"MediaEncoderParams::GetAudioOutputBytesPerSecond - Requesting out of bounds quality %u", (u32)qualityLevel ) ? 
			c_audioBitrateArray[ qualityLevel ] : c_audioBitrateArray[ MediaEncoderParams::QUALITY_DEFAULT ];
	}

	size_t MediaEncoderParams::EstimateSizeOfRecording( u32 const durationMs, MediaEncoderParams::eQualityLevel const qualityLevel, 
		MediaCommon::eVideoFormat const videoFormat, MediaCommon::eAudioFormat const audioFormat )
	{
		u32 const c_durationSeconds = ( durationMs / 1000 ) + 1;

		size_t const c_videoBytesPerSecond = rage::MediaEncoderParams::GetQualityLevelVideoBitRate( videoFormat, qualityLevel, durationMs ) / 8;
		size_t const c_videoBytesTotal = c_videoBytesPerSecond * c_durationSeconds;

		size_t const c_audioBytesPerSecond = rage::MediaEncoderParams::GetAudioOutputBytesPerSecond( audioFormat, qualityLevel );
		size_t const c_audioBytesTotal = c_audioBytesPerSecond * c_durationSeconds;

		size_t const c_outputSize = c_videoBytesTotal + c_audioBytesTotal;
		return c_outputSize;
	}

}

#endif // !__RESOURCECOMPILER
