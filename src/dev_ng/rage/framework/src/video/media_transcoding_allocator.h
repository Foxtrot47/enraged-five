/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : media_transcoding_allocator.h
// PURPOSE : Class for wrapping encoder/decoder allocations
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIA_TRANSCODING_ALLOCATOR_H_
#define INC_MEDIA_TRANSCODING_ALLOCATOR_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

// TODO - Currently Xb1 only, might localize all transcoding API allocations to come from here for better tracking?
#if ( RSG_DURANGO && 1 ) && !__RESOURCECOMPILER && !__RGSC_DLL 

//! Flag if we are using the override at all
#define USE_MMF_XMEM_OVERRIDE_INSTANCE 1

//! Flag that we only want to override the decoding allocations
#define USE_MMF_DECODE_HOOK_ONLY 1

namespace rage
{
	class sysMemAllocator;

	class MediaTranscodingMemorySystem
	{
	public:
		MediaTranscodingMemorySystem();
		~MediaTranscodingMemorySystem();

#if defined(USE_MMF_DECODE_HOOK_ONLY) && USE_MMF_DECODE_HOOK_ONLY
        void Init( sysMemAllocator*	decodingAllocator );
        bool isInitialized() const { return m_decodingAllocator != NULL; }
#else
        void Init( size_t const smallObjMemSize, size_t const maxSmallObjPointers, size_t const memSize, size_t const maxPointers, sysMemAllocator*	decodingAllocator );
        bool isInitialized() const { return m_allocator != NULL && m_smallObjectAllocator != NULL; }
#endif

		void Shutdown();

		void* allocate( size_t const size, size_t const align );
		void free( void *address );

        void setDecoding( bool const isDecoding );
        bool isDecoding() const;

#if RSG_DURANGO

		static void SetXMemInstance( MediaTranscodingMemorySystem* memoryWrapper );

		static void* sysMemXMemAllocHook( size_t sizeBytes, u64 attributes );
		static void	 sysMemXMemFreeHook( void *address, u64 attributes );

#endif

	private: // declarations and variables
		void*				m_smallObjectMemoryBlock;
		void*				m_smallObjectWorkspace;
		sysMemAllocator*	m_smallObjectAllocator;

		void*				m_memoryBlock;
		void*				m_workspace;
		sysMemAllocator*	m_allocator;

        sysMemAllocator*	m_decodingAllocator;
        s32                 m_decodingRefCount;

#if RSG_DURANGO
		static MediaTranscodingMemorySystem* ms_XMemWrapperInstance;
#endif

	private: // methods

		void* allocateInternal( size_t const size, size_t const align );
		bool  freeInternal( void *address );

#if RSG_DURANGO
		void* mmfAllocInternal( size_t const sizeBytes, u64 const attributes );
		void mmfFreeInternal( void* address, u64 const attributes );
#endif

	};

	extern MediaTranscodingMemorySystem MEDIA_MEMORY_WRAPPER;
}

#else

#endif // (RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

#endif // INC_MEDIA_TRANSCODING_ALLOCATOR_H_
