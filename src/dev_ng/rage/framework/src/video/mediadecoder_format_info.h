/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediadecoder_format_info.h
// PURPOSE : Class for holding information about a video file that we want to decode
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#ifndef INC_MEDIADECODER_FORMAT_INFO_H_
#define INC_MEDIADECODER_FORMAT_INFO_H_

// rage
#include "math/amath.h"

// framework
#include "media_common.h"

namespace rage
{
	struct MediaDecoderFormatInfo
	{
		u64							m_fileSize;
		u32							m_displayWidth;
		u32							m_displayHeight;
		u32							m_audioChannels;
		u32							m_audioBitsPerSample;
		u32							m_audioSampleRate;
		u64							m_durationNs;
		float						m_frameRate;
		MediaCommon::eVideoFormat	m_videoFormat;
		MediaCommon::eAudioFormat	m_audioFormat;

		MediaDecoderFormatInfo()
			: m_fileSize( 0 )
			, m_displayWidth( 0 )
			, m_displayHeight( 0 )
			, m_audioChannels( 0 )
			, m_audioBitsPerSample( 0 )
			, m_audioSampleRate( 0 )
			, m_durationNs( 0 )
			, m_frameRate( 0.f )
			, m_videoFormat( MediaCommon::VIDEO_FORMAT_INVALID )
			, m_audioFormat( MediaCommon::AUDIO_FORMAT_INVALID )
		{

		}

		// Values not checked here are presentation details only, so we can still be valid even if they are not...
        inline bool IsVideoDataValid() const { return m_displayWidth > 0 && m_displayHeight > 0 && 
            m_frameRate > 0.f && MediaCommon::IsSupportedVideoFormat( m_videoFormat ); }

        inline bool IsAudioDataValid() const { return m_audioChannels <= 2 && m_audioBitsPerSample == MediaCommon::sc_bitsPerGameAudioSample && 
            m_audioSampleRate == rage::kMixerNativeSampleRate && MediaCommon::IsSupportedAudioFormat( m_audioFormat ); }

		inline bool IsValid() const { return IsVideoDataValid() && IsAudioDataValid(); }

		inline void Reset()
		{
			m_fileSize = 0;
			m_displayWidth = 0;
			m_displayHeight = 0;
			m_audioChannels = 0;
			m_audioBitsPerSample = 0;
			m_audioSampleRate = 0;
			m_durationNs = 0;
			m_frameRate = 0.f;
			m_videoFormat = MediaCommon::VIDEO_FORMAT_INVALID;
			m_audioFormat = MediaCommon::AUDIO_FORMAT_INVALID;
		}

		inline u32 GetDecodeAlignedWidth() const 
		{ 
			u32 const c_diff = ( m_displayWidth % 16 );
			return m_displayWidth + ( c_diff > 0 ? 16 - c_diff : 0 ); 
		}

		inline u32 GetDecodeAlignedHeight() const 
		{ 
			u32 const c_diff = ( m_displayHeight % 16 );
			return m_displayHeight + ( c_diff > 0 ? 16 - c_diff : 0 ); 
		}

		inline size_t GetBGRARowSize() const { return m_displayWidth * 4; }
		inline size_t GetBGRABufferSize() const { return GetBGRARowSize() * m_displayHeight; } 

		inline size_t Get420YRowSize() const { return m_displayWidth; }
		inline size_t Get420UVRowSize() const { return Get420YRowSize(); }

		inline size_t GetDecodeAligned420YRowSize() const { return GetDecodeAlignedWidth(); }
		inline size_t GetDecodeAligned420UVRowSize() const { return GetDecodeAlignedWidth(); }

		inline size_t Get420YBufferSize() const { return Get420YRowSize() * m_displayHeight; } 
		inline size_t Get420UVBufferSize() const { return Get420YBufferSize() / 2; }  

		inline size_t GetDecodeAligned420YBufferSize() const { return GetDecodeAligned420YRowSize() * GetDecodeAlignedHeight(); } 
		inline size_t GetDecodeAligned420UVBufferSize() const { return GetDecodeAligned420YBufferSize() / 2; }  

		inline size_t GetAudioBufferSize() const { return MediaCommon::sc_decoderAudioBufferSize; }

        bool IsH264Video() const { return m_videoFormat == MediaCommon::VIDEO_FORMAT_H264; }
	};

} // namespace rage

#endif // INC_MEDIADECODER_FORMAT_INFO_H_

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
