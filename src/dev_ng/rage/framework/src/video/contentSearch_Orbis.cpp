#if RSG_ORBIS

#include <scebase_common.h>
#include <libsysmodule.h>

#pragma comment(lib,"libSceContentSearch_stub_weak.a")

#include "contentSearch_Orbis.h"

#include "system/new.h"

namespace rage
{

bool CContentSearch::m_InitSuccess = false;
SceContentSearchApplicationIndex CContentSearch::m_applicationIndex = 0;

void CContentSearch::Init(unsigned)
{
	Assertf(m_InitSuccess == false, "CContentSearch::Init() - Already Initialised");

	if( Verifyf(sceSysmoduleLoadModule(SCE_SYSMODULE_CONTENT_SEARCH) == SCE_OK, "ERROR: Load of SCE_SYSMODULE_CONTENT_SEARCH module failed") )
	{
		// Initialize ContentSearch
		SceContentSearchInitParam initparam;
		initparam.memorySize = 512 * 1024; // from sample

		if( Verifyf(sceContentSearchInit(&initparam) == SCE_OK, "sceContentSearchInit failed") )
		{
			m_InitSuccess = true;
		}
	}
}

// SceContentSearchContentInfo infos[numOfContent];
// memset(infos, 0, sizeof(SceContentSearchContentInfo) * numOfContent);

bool CContentSearch::GetVideoInfos(u32 offset, u32 maxSizeOfList, u32 *o_numOfContent, SceContentSearchContentInfo *o_info)
{
	if (m_applicationIndex == 0) 
	{
		//E Get application index for caller application
		SceContentSearchApplicationIndex myIndex;
		if (Verifyf(sceContentSearchGetMyApplicationIndex(&myIndex) == SCE_OK, "sceContentSearchGetMyApplicationIndex failed") )
		{
			m_applicationIndex = myIndex;
		}
		else
		{
			return false;
		}
	}

	SceContentSearchContentColumnSet columnSet;
	memset(&columnSet, 0, sizeof(SceContentSearchContentColumnSet));

	SceContentSearchContentColumn columns[2];
	memset(&columns, 0, sizeof(SceContentSearchContentColumn) * 2);
	columns[0].columnOperator = SCE_CONTENT_SEARCH_COLUMN_VALUE_OPERATOR_EQUAL;
	columns[0].columnType = SCE_CONTENT_SEARCH_CONTENT_COLUMN_TYPE_CONTENT_TYPE;

	//E Column condition : Content type = video
	SceContentSearchColumnValue values[2];
	memset(&values, 0, sizeof(SceContentSearchColumnValue) * 2);
	values[0].vInt = SCE_CONTENT_SEARCH_CONTENT_TYPE_VIDEO;
	values[0].size = 4;

	columns[0].value = &values[0];
	columns[0].logicalConnector = SCE_CONTENT_SEARCH_LOGICAL_CONNECTOR_NONE;

	//E Column condition : Application index for the caller application
	columns[1].columnOperator = SCE_CONTENT_SEARCH_COLUMN_VALUE_OPERATOR_EQUAL;
	columns[1].columnType = SCE_CONTENT_SEARCH_CONTENT_COLUMN_TYPE_APPLICATION_INDEX;
	values[1].vInt = m_applicationIndex;
	values[1].size = 4;
	columns[1].value = &values[1];

	//E AND conjunction with the immediately previous column condition
	columns[1].logicalConnector = SCE_CONTENT_SEARCH_LOGICAL_CONNECTOR_AND;

	columnSet.columns = &columns[0];
	columnSet.logicalConnector = SCE_CONTENT_SEARCH_LOGICAL_CONNECTOR_NONE;
	columnSet.columnsLength = 2;

	//E Specify ascending order by content title for the display order
	SceContentSearchContentOrderByCondition orderByConditions;
	orderByConditions.columnType = SCE_CONTENT_SEARCH_CONTENT_SORT_COLUMN_TYPE_TITLE;
	orderByConditions.sortType = SCE_CONTENT_SEARCH_SORT_TYPE_ASCENDANT;
	int32_t orderByConditionsLength = 1;

	//E Up to ten pieces of information on the contents which coincide with conditions are obtained. 
	SceContentSearchUpdateId lastUpdateId = 0;
	s64 numOfContent = 0;
	if ( Verifyf(sceContentSearchSearchContent(&columnSet, 1, &orderByConditions, orderByConditionsLength, offset, maxSizeOfList, &numOfContent, o_info, &lastUpdateId)  == SCE_OK, "sceContentSearchSearchContent() Failed"))
	{
		// not sure why this function returns an s64 for the number of entries?
		*o_numOfContent = static_cast<u32>(numOfContent);
		return true;
	}

	return false;
}

bool CContentSearch::GetMetaData(SceContentSearchContentInfo *infos, s32 *o_mimeType, u64 *o_lastUpdatedTime, s32 *o_height, s32 *o_width, u32 *o_size, u32 *o_duration, u32 outputStringLength, char* o_copyright, char* o_subtitle)
{
	SceContentSearchMetadataId metadataId;
	if ( Verifyf(sceContentSearchOpenMetadata( infos->contentPath , &metadataId) == SCE_OK, "CContentSearch::GetMetaData - sceContentSearchOpenMetadata failed") )
	{
		//E Please refer to the 'ContentSearch Library Reference' about the metadata field that can be obtained from the MP4 content.
		const char *mp4FieldList[]= {
			SCE_CONTENT_SEARCH_METADATA_FIELD_MIME,
//			SCE_CONTENT_SEARCH_METADATA_FIELD_TITLE,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_CREATED_TIME,
 			SCE_CONTENT_SEARCH_METADATA_FIELD_LASTUPDATED_TIME,
			SCE_CONTENT_SEARCH_METADATA_FIELD_SIZE,
			SCE_CONTENT_SEARCH_METADATA_FIELD_WIDTH,
			SCE_CONTENT_SEARCH_METADATA_FIELD_HEIGHT,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_COMMENT,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_AUDIO_BITRATE,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_AUDIO_CHANNEL_TYPE,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_AUDIO_MIME_TYPE,
			SCE_CONTENT_SEARCH_METADATA_FIELD_COPYRIGHT,
//			SCE_CONTENT_SEARCH_METADATA_FIELD_DESCRIPTION,
			SCE_CONTENT_SEARCH_METADATA_FIELD_DURATION,
 			SCE_CONTENT_SEARCH_METADATA_FIELD_SUB_TITLE,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_TIMEZONE_OFFSET,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_VIDEO_BITRATE,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_CHAPTER_INDEX,
// 			SCE_CONTENT_SEARCH_METADATA_FIELD_MEDIA_TIMESCALE,
		};

		// this must match the field list above
		enum
		{
			MetaData_MimeType = 0,
			MetaData_LastUpdatedTime,
			MetaData_Size,
			MetaData_Width,
			MetaData_Height,
			MetaData_Copyright,
			MetaData_Duration,
			MetaData_Subtitle
		};

		int field_num = sizeof(mp4FieldList) / sizeof(char*);
		char **file_list = (char**)mp4FieldList;

		for( int field_index = 0; field_index < field_num ; ++field_index )
		{
			// check to see if we even need to bother to check
			if ((field_index == MetaData_MimeType && !o_mimeType) ||
				(field_index == MetaData_LastUpdatedTime && !o_lastUpdatedTime) ||
				(field_index == MetaData_Size && !o_size) || 
				(field_index == MetaData_Width && !o_width) || 
				(field_index == MetaData_Height && !o_height) || 
				(field_index == MetaData_Duration && !o_duration) || 
				(field_index == MetaData_Copyright && !o_copyright) ||
				(field_index == MetaData_Subtitle && !o_subtitle))
			{
				continue;
			}


			SceContentSearchMetadataType metadataType;
			int32_t size;

			//E  Gets the information of type of the value and the buffer size necessary if result type a BLOB or TEXT.
			//   SCE_CONTENT_SEARCH_ERROR_NOT_EXIST_FIELD is returned if the field is not exist in the content.
			if ( sceContentSearchGetMetadataFieldInfo(metadataId, file_list[field_index], &metadataType, &size) )
			{
				// things can validly have no data. usually things like comments. 
				continue;
			}

			SceContentSearchMetadataValue metadataValue;
			memset( &metadataValue, 0, sizeof(SceContentSearchMetadataValue) );
			if( metadataType == SCE_CONTENT_SEARCH_METADATA_TEXT )
			{
				//E Please prepare the required buffer, if the type of value is TEXT or BLOB. 
				metadataValue.vText = rage_new char[size];
				metadataValue.size = size;
			}
			else if( metadataType == SCE_CONTENT_SEARCH_METADATA_BLOB )
			{
				// we're not currently checking for anything that's a blob
				continue;

// 				//E Please prepare the required buffer, if the type of value is TEXT or BLOB. 
// 				metadataValue.vBlob = (char*) malloc(size);
// 				metadataValue.size = size;
			}

			if ( Verifyf(sceContentSearchGetMetadataValue(metadataId, file_list[field_index], &metadataValue) == SCE_OK, "CContentSearch::GetMetaData - sceContentSearchGetMetadataValue failed") )
			{
				if( metadataType == SCE_CONTENT_SEARCH_METADATA_TEXT )
				{
					// pointer validity has already been tested...but no harm doing it again
					if (field_index == MetaData_Copyright && o_copyright)
					{
						Assertf(metadataValue.size <= outputStringLength, "CContentSearch::GetMetaData - string longer than outputstring length of %d - %d chars - %s - %s", outputStringLength, size, file_list[field_index], infos->contentPath);
						strcpy_s(o_copyright, outputStringLength, metadataValue.vText);
					}

					else if (field_index == MetaData_Subtitle && o_subtitle)
					{
						Assertf(metadataValue.size <= outputStringLength, "CContentSearch::GetMetaData - string longer than outputstring length of %d - %d chars - %s - %s", outputStringLength, size, file_list[field_index], infos->contentPath);
						strcpy_s(o_subtitle, outputStringLength, metadataValue.vText);
					}

					// then free dat
					delete metadataValue.vText;
				}
				else if ( metadataType == SCE_CONTENT_SEARCH_METADATA_INT )
				{
					// pointer validity has already been tested...but no harm doing it again
					if (field_index == MetaData_MimeType && o_mimeType)
					{
						*o_mimeType = static_cast<s32>(metadataValue.vInt);
					}
					else if (field_index == MetaData_Size && o_size)
					{
						*o_size = static_cast<u32>(metadataValue.vInt);
					}
					else if (field_index == MetaData_Width && o_width)
					{
						*o_width = static_cast<s32>(metadataValue.vInt);
					}
					else if (field_index == MetaData_Height && o_height)
					{
						*o_height = static_cast<s32>(metadataValue.vInt);
					}
					else if (field_index == MetaData_Duration && o_duration)
					{
						*o_duration = static_cast<u32>(metadataValue.vInt);
					}
				}
				else if( metadataType == SCE_CONTENT_SEARCH_METADATA_TICK )
				{
					if (field_index == MetaData_LastUpdatedTime && o_lastUpdatedTime)
					{
						*o_lastUpdatedTime = static_cast<SceRtcTick>(metadataValue.vTick).tick;
					}
				}
				// we're not currently checking for anything that's a blob
// 				else if( metadataType == SCE_CONTENT_SEARCH_METADATA_BLOB )
// 				{
// 					free(metadataValue.vBlob);
// 				}
			}
			else
			{
				// let's be on the safe side ...release stuff even if it fails
				if( metadataType == SCE_CONTENT_SEARCH_METADATA_TEXT )
				{
					if (metadataValue.vText)
						delete metadataValue.vText;
				}

				// make sure close metadata called on fail too
				sceContentSearchCloseMetadata(metadataId);

				// if it's failed with one, it'll fail with the lot of 'em
				return false;
			}
		}

		sceContentSearchCloseMetadata(metadataId);

		return true;
	}

	return false;
}

void CContentSearch::Shutdown(unsigned)
{
	if(m_InitSuccess)
	{
		if( Verifyf( sceContentSearchTerm() == SCE_OK,"sceContentSearchTerm() Failed"))
		{
			if( Verifyf(sceSysmoduleUnloadModule(SCE_SYSMODULE_CONTENT_SEARCH) == SCE_OK, "sceSysmoduleUnloadModule(SCE_SYSMODULE_CONTENT_SEARCH) Failed") )
			{
				m_InitSuccess = false;
			}
		}
	}
}

} // namespace rage

#endif	//RSG_ORBIS
