#ifndef __CONTENT_DELETE_ORBIS_H__
#define __CONTENT_DELETE_ORBIS_H__

#if RSG_ORBIS

// Content Delete for Orbis. Very Basic Wrapper, may need better 

class CContentDelete
{
public:
	static void Init(unsigned initMode);
	static void Shutdown(unsigned shutdownMode);

	static bool DoDeleteVideo(const char *pMoviePath);
	static bool DoDeleteVideo(rage::s64 contentId);

private:	
	static bool	m_InitSuccess;
};

#endif	//RSG_ORBIS

#endif //__CONTENT_DELETE_ORBIS_H__
