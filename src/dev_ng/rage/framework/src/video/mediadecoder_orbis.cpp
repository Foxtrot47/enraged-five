#include "mediadecoder.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#if RSG_ORBIS

#include <libsysmodule.h>
#include <sceavplayer.h>
#include <sceavplayer_ex.h>
#include <audioout.h>
#include <_fs.h>

#include "video/mediaencoder_params.h"
#include "video/video_channel.h"
#include "system/memory.h"
#include "system/memmanager.h"
#include "paging/streamer.h"
#include "audiohardware/ringbuffer.h"
#include "audiohardware/driver.h"

#include "string/string.h"

#include <pthread.h>
#include "audiohardware/device_orbis.h"

#include "recordinginterface_orbis.h"

#pragma comment(lib, "libSceAvPlayer_stub_weak.a")


namespace rage
{
	namespace AVPlayer
	{
		void* ms_player = NULL;
 		bool ms_bAudioRunning = false;

		static void eventCallback(void* jumpback, int32_t argEventId, int32_t argSourceId, void* argEventData)
		{
			switch ( argEventId )
			{
			case SCE_AVPLAYER_STATE_READY :
				{
					//E The number of streams are supplied here, if you are not seeing video or hearing audio the first place to check is here to ensure that
					//E both audio and video streams are detected and enabled.
					const int streamCount = sceAvPlayerStreamCount(ms_player);
					int audio_stream = -1;
					if (streamCount > 0) {
						for (int i = 0; i < streamCount; ++i)
						{
							SceAvPlayerStreamInfo StreamInfo;

							int ret = sceAvPlayerGetStreamInfo(ms_player, i, &StreamInfo);
							videoAssert(ret == 0);
							if (StreamInfo.type == SCE_AVPLAYER_VIDEO)
							{
								//E Enabling the video, if you don't want the video to playback simply don't enable it and audio will play as expected.
 								ret = sceAvPlayerEnableStream(ms_player, i);
								videoAssert(ret == 0);
							} 
							else 
							if(StreamInfo.type == SCE_AVPLAYER_AUDIO && ms_bAudioRunning)
							{
								//E Select wanted audio streams - in this case just the first audio stream found
							//	static int audio_stream = -1;
								
								if (audio_stream < 0)
								{
									audio_stream = i;

									//E Enabling the audio, if you don't want the audio to playback simply don't enable it and video will play as expected.
									ret = sceAvPlayerEnableStream(ms_player, i);
 									videoAssert(ret == 0);
								}
							}
						}

						//E Start the player, this command is basically the "Play" command.
						sceAvPlayerStart(ms_player); ///< "Play"
					}
				}
				videoDisplayf("Orbis AVPlayer MP4: State Changed to SCE_AVPLAYER_STATE_READY\n");
				break;
			case SCE_AVPLAYER_STATE_PLAY:
				videoDisplayf("Orbis AVPlayer MP4: State Changed to SCE_AVPLAYER_STATE_PLAY\n");
				break;
			case SCE_AVPLAYER_STATE_STOP:
				videoDisplayf("Orbis AVPlayer MP4: State Changed to SCE_AVPLAYER_STATE_STOP\n");
				break;
			case SCE_AVPLAYER_STATE_PAUSE:
				videoDisplayf("Orbis AVPlayer MP4: State Changed to SCE_AVPLAYER_STATE_PAUSE\n");
				break;
			case SCE_AVPLAYER_STATE_BUFFERING:
				videoDisplayf("Orbis AVPlayer MP4: State Changed to SCE_AVPLAYER_STATE_BUFFERING\n");
				break;
			default:
				videoDisplayf("Orbis AVPlayer MP4: Unhandled Event %d\n", argEventId);
				break;
			};
		}

// the method of reading the video file using pgStreamer doesn't work on files within av_content ...where the videos are taken from now
#define USE_PG_STREAMER_TO_READ 0

		// sceAvPlayerInit requires a number of function replacements for memory and filereading
		struct sAVPlayerMovieHandle
		{
			static sysMemAllocator* ms_allocator;

#if USE_PG_STREAMER_TO_READ
			static pgStreamer::Handle ms_handle;
			static sysIpcSema ms_sema;
			static u64 ms_seekPos;
			static u32 ms_fileSize;
			static bool ms_open;

			static s32 Open(void* UNUSED_PARAM(ptr), const char* filename)
			{
				if (ms_open)
				{
					Errorf("NativeMovie handle already open!");
					return pgStreamer::Error;
				}

				if ((ms_handle = pgStreamer::Open(filename, &ms_fileSize, 0)) == pgStreamer::Error)
					return pgStreamer::Error;

				ms_seekPos = 0;
				ms_sema = sysIpcCreateSema(false);
				ms_open = true;
				return 0;
			}

			static s32 Close(void* UNUSED_PARAM(ptr))
			{
				if (!ms_open)
				{
					Errorf("NativeMovie handle isn't open!");
					return pgStreamer::Error;
				}

				pgStreamer::Close(ms_handle);
				sysIpcDeleteSema(ms_sema);
				ms_handle = pgStreamer::Error;
				ms_open = false;
				return 0;
			}

			static s32 ReadOffset(void* UNUSED_PARAM(ptr), u8* buf, u64 offset, u32 len)
			{
				if (offset + len > ms_fileSize)
				{
					Warningf("Read past end of file, truncated from %u to %" SIZETFMT "u", len,(size_t)(ms_fileSize - offset));
					len = (u32)(ms_fileSize - offset);
				}

				datResourceChunk list = { 0, buf, len };
				// The read can fail if the queue is full, so keep retrying periodically.
				while (pgStreamer::Read(ms_handle, &list, 1, (u32)offset, ms_sema, pgStreamer::CRITICAL) == NULL)
					sysIpcSleep(10);
				offset += len;
				sysIpcWaitSema(ms_sema);
				return len;
			}

			static u64 Size(void* UNUSED_PARAM(ptr))
			{
				return (u64)ms_fileSize;
 			}
#else			
			static SceUID g_FileHandle;

			static int Open(void* UNUSED_PARAM(ptr), const char* argFilename)
			{
				g_FileHandle = sceKernelOpen(argFilename, SCE_KERNEL_O_RDONLY, SCE_KERNEL_S_INONE);
				return (int)g_FileHandle;
			}

			static int Close(void* UNUSED_PARAM(ptr))
			{
				return sceKernelClose(g_FileHandle);
			}

			static int ReadOffset(void* UNUSED_PARAM(ptr), uint8_t* argBuffer, uint64_t argPosition, uint32_t argLength)
			{
				// -1 (or any negative value) is an error
				// 0 is no data available which will trigger buffering eventually (After roughly 1 second of no data buffering will kick in)
				int64_t ActualRead = 0;

				ActualRead = sceKernelPread(g_FileHandle, argBuffer, argLength, argPosition);

				if (ActualRead < 0)
					return -1; // Actual read error

				if (ActualRead == argLength)
					return (int)ActualRead; // Good read, use this data
				else
					return 0; // Buffering..
			}

			static uint64_t Size(void* UNUSED_PARAM(ptr))
			{
				int64_t filesize;

				if(0>sceKernelLseek(g_FileHandle, 0, SCE_KERNEL_SEEK_SET)) {
					return 0;
				}

				filesize = sceKernelLseek(g_FileHandle, 0, SCE_KERNEL_SEEK_END);
				if (0>filesize) {
					return 0;
				}
				if(0>sceKernelLseek(g_FileHandle, 0, SCE_KERNEL_SEEK_SET)) {
					return 0;
				}
				return (uint64_t)filesize;
			}
#endif

			static void* Allocate(void* UNUSED_PARAM(ptr), u32 alignment, u32 size)
			{
				videoAssert(ms_allocator);
				return ms_allocator->Allocate(size, alignment);
			}

			static void Deallocate(void* UNUSED_PARAM(ptr), void* pMem)
			{
				videoAssert(ms_allocator);
				ms_allocator->Free(pMem);
			}
		};

		sysMemAllocator* sAVPlayerMovieHandle::ms_allocator = NULL;

#if USE_PG_STREAMER_TO_READ
		pgStreamer::Handle sAVPlayerMovieHandle::ms_handle = pgStreamer::Error;
		sysIpcSema sAVPlayerMovieHandle::ms_sema;
		u64 sAVPlayerMovieHandle::ms_seekPos = 0;
		u32 sAVPlayerMovieHandle::ms_fileSize = 0;
		bool sAVPlayerMovieHandle::ms_open = false;
#else
		SceUID sAVPlayerMovieHandle::g_FileHandle = SCE_UID_INVALID_UID;
#endif

		int InitLibraries()
		{
			s32 returnCode = sceSysmoduleLoadModule(SCE_SYSMODULE_AV_PLAYER);

			if (returnCode != SCE_OK) {
				videoDisplayf("ERROR: sceSysmoduleLoadModule(SCE_SYSMODULE_AV_PLAYER) 0x%08x\n", returnCode);
			}

			return returnCode;
		}

		int ShutdownLibraries()
		{
			s32 returnCode = sceSysmoduleUnloadModule(SCE_SYSMODULE_AV_PLAYER);

			if (returnCode != SCE_OK) {
				videoDisplayf("ERROR: sceSysmoduleUnloadModule(SCE_SYSMODULE_AV_PLAYER) 0x%08x\n", returnCode);
			}

			return returnCode;
		}

#define NUM_PCM_SAMPLES			(1024)
#define BASE_SAMPLE_RATE		(48000)

		s32 ms_audioHandle = -1;
		sysIpcThreadId ms_audioThread = 0;

		void OutputSound(u8* data, u32 sampleRate, u32 channelCount)
		{
			if (ms_audioHandle < 0)
				return;

			if (!Verifyf(sampleRate == 0 || sampleRate == BASE_SAMPLE_RATE, "NativeMovie sample rate mismatch. Got %d, expected %d", sampleRate, BASE_SAMPLE_RATE))
				return;

			if (!Verifyf(channelCount == 0 || channelCount == 2, "NativeMovie channel count mismatch. Got %d, expected %d", channelCount, 2))
				return;

			sceAudioOutOutput(ms_audioHandle, data);
		}

		void AudioThread(void* UNUSED_PARAM(ptr))
		{
			// allocate an empty sound frame
			u8* noSound = (u8*)rage_aligned_new(0x20) char[4096 * 4];
			sysMemSet(noSound, 0, 4096 * 4);

			SceAvPlayerFrameInfo audioFrame;
			sysMemSet(&audioFrame, 0, sizeof(SceAvPlayerFrameInfo));

			while (ms_bAudioRunning)
			{
				if (sceAvPlayerGetAudioData((SceAvPlayerHandle)ms_player, &audioFrame))
					OutputSound((u8*)audioFrame.pData, audioFrame.details.audio.sampleRate, audioFrame.details.audio.channelCount);
				else
					OutputSound(noSound, 0, 0);
			}

			sceAudioOutOutput(ms_audioHandle, NULL);
			sceAudioOutClose(ms_audioHandle);
			delete[] noSound;
		}

		bool InitAudio()
		{
			ms_audioHandle = -1;

			// sceAudioOutInit() has already been done in Audio code, but if it wasn't ...you'd do it here

			ms_audioHandle = sceAudioOutOpen(SCE_USER_SERVICE_USER_ID_SYSTEM, SCE_AUDIO_OUT_PORT_TYPE_MAIN, 0, NUM_PCM_SAMPLES, BASE_SAMPLE_RATE, SCE_AUDIO_OUT_PARAM_FORMAT_S16_STEREO);

			if (!Verifyf(ms_audioHandle >= 0, "sceAudioOutOpen failed! errorCode: 0x%08x", ms_audioHandle))
			{
				ms_audioHandle = -1;
				return false;
			}

			ms_bAudioRunning = true;

			// might have to make Priority 641 or less, as it should be less than SceVdecSyntaxReaderThread's priority of 642
			// doesn't appear to be an issue though as isn't being run on same core
			ms_audioThread = sysIpcCreateThread(&AudioThread, NULL, 64 << 10, PRIO_MP4, "MP4 Audio Thread");

			if (ms_audioThread != 0)
			{
				// set audio thread to a lower priority. we don't need it as we have our own simpler thread
				// ...and we want to steal it's core for decoding
				// yet, we still need it to tick over as disabling and enabling it a pain
				static_cast<audMixerDeviceOrbis*>( audDriver::GetMixer() )->SetLowerPriority(true);
			}
			else
			{
				// audio thread is valid if you made it this far, so if the thread failed ...kill the handle
				sceAudioOutOutput(ms_audioHandle, NULL);
				sceAudioOutClose(ms_audioHandle);
				ms_bAudioRunning = false;
			}

			return ms_bAudioRunning;
		}

#define USE_LARGER_DEMUX_BUFFER 1

		bool InitAvPlayer()
		{
			SceAvPlayerInitData playerInit;
			memset(&playerInit, 0, sizeof(SceAvPlayerInitData));
#if __FINAL
			playerInit.debugLevel = SCE_AVPLAYER_DBG_NONE;
#else
			playerInit.debugLevel = SCE_AVPLAYER_DBG_ALL;
#endif
			playerInit.basePriority = 170; // needs to be greater than render thread, 125-170
			playerInit.numOutputVideoFrameBuffers = 2; // adjust to compensate for latency, 2-3 should always work
			playerInit.autoStart = true;
			playerInit.defaultLanguage = "eng";

			playerInit.fileReplacement.objectPointer = NULL;
			playerInit.fileReplacement.open = sAVPlayerMovieHandle::Open;
			playerInit.fileReplacement.close = sAVPlayerMovieHandle::Close;
			playerInit.fileReplacement.readOffset = sAVPlayerMovieHandle::ReadOffset;
			playerInit.fileReplacement.size = sAVPlayerMovieHandle::Size;

			playerInit.memoryReplacement.objectPointer = NULL;
			playerInit.memoryReplacement.allocate = sAVPlayerMovieHandle::Allocate;
			playerInit.memoryReplacement.deallocate = sAVPlayerMovieHandle::Deallocate;
			playerInit.memoryReplacement.allocateTexture = sAVPlayerMovieHandle::Allocate;
			playerInit.memoryReplacement.deallocateTexture = sAVPlayerMovieHandle::Deallocate;

			playerInit.eventReplacement.objectPointer = NULL;
			playerInit.eventReplacement.eventCallback = AVPlayer::eventCallback;
			playerInit.autoStart = false;

			ms_player = sceAvPlayerInit(&playerInit);
			if (!Verifyf(ms_player, "sceAvPlayerInit failed!"))
				return false;	

#if USE_LARGER_DEMUX_BUFFER
			// previously found that having this on can make API calls that longer than 33ms in some cases
			// seems to be okay now, but might change when firmware changes (it's happened before)
			// but we need this now, for when screen is dark for a long time and buffering hits when picture comes back B*2541012
			SceAvPlayerPostInitData playerPostInit;
			memset(&playerPostInit, 0, sizeof(SceAvPlayerPostInitData));
			// can be a max of 8Mb. 2.5 Mb was just enough to stop stalls on example in bug
			playerPostInit.demuxVideoBufferSize = (6 * 512 * 1024);
			s32 returnCode = sceAvPlayerPostInit(ms_player, &playerPostInit);

			return (returnCode == SCE_OK);
#else
			return true;
#endif
		}

		bool ms_bAvPlayerStarted = false;
		bool ms_bAvPlayerFailed = false;
		bool ms_bAvPlayerInitialised = false;

		static void* InitAvPlayerStatic(void* UNUSED_PARAM(arg))
		{
			if (InitAvPlayer())
			{
				ms_bAvPlayerInitialised = true;
			}
			else
			{
				ms_bAvPlayerFailed = true;
			}

			return NULL;
		}

		ScePthread m_outputThread;
		static bool ms_paused = false;

		int Init()
		{
			// we're unsure what memory we're using yet. at the moment just trying to match what replay uses
			sAVPlayerMovieHandle::ms_allocator = sysMemManager::GetInstance().GetReplayAllocator();

			// sceAvPlayerInit makes the AV player take on the cpu affinity of the thread that calls it
			// we need to apply this over a few cpus off the main thread, so we create a thread with the cpu affinity we want
			// and then run sceAvPlayerInit on it ...killing the thread off as soon as it's done. 
			// ...as Naughty Dog suggest they do on the sce forums https://ps4.scedev.net/forums/thread/40443/

			// need to use proper sce threading code, as sysIpc doesn't allow for multiple CPU affinity
			ScePthreadAttr threadattr;
			scePthreadAttrInit(&threadattr);
			scePthreadAttrSetstacksize(&threadattr, 1024 * 1024);
			SceKernelCpumask mask = (1 << 1) | (1 << 2) | (1 << 5);
			scePthreadAttrSetaffinity(&threadattr, mask);
			SceKernelSchedParam p = { SCE_KERNEL_PRIO_FIFO_DEFAULT - PRIO_BELOW_NORMAL };
			scePthreadAttrSetschedparam(&threadattr,  &p);
			s32 result = scePthreadCreate(&m_outputThread, &threadattr, AVPlayer::InitAvPlayerStatic, NULL, "av_init_thread");
			scePthreadAttrDestroy(&threadattr);

			if (result < 0) {
				return -1;
			}

			ms_bAvPlayerStarted = true;
			ms_paused = false;
			return SCE_OK;
		}

		void SetPause(bool pause)
		{
			if (ms_paused != pause)
			{
				if (pause)
					sceAvPlayerPause((SceAvPlayerHandle)ms_player);
				else
					sceAvPlayerResume((SceAvPlayerHandle)ms_player);
				ms_paused = pause;
			}
		}

		bool IsPaused()
		{
			return ms_paused;
		}

		int PlayFile(const char * const path)
		{
			videoAssert(ms_player);
			videoAssert(path);
			return sceAvPlayerAddSource((SceAvPlayerHandle)ms_player, path);
		}

		bool GetStreamData(SceAvPlayerStreamInfo* o_streamInfo, SceAvPlayerStreamType streamType)
		{
			if (!ms_player || !sceAvPlayerIsActive((SceAvPlayerHandle)ms_player))
				return false;

			const int streamCount = sceAvPlayerStreamCount(ms_player);
			if (streamCount > 0)
			{
				for (int i = 0; i < streamCount; ++i)
				{
					int ret = sceAvPlayerGetStreamInfo(ms_player, i, o_streamInfo);
					if (ret != 0)
					{
						videoDisplayf("AVPlayer::GetStreamData - failed getting stream this frame. AVPlayer timing issue. should work fine next frame.  - %d - 0x%08x", streamCount, ret);
						return false;
					}
					// return the first one that matches the required type
					if (o_streamInfo->type == streamType)
					{
						return true;
					} 
				}
			}

			return false;
		}

		bool GetVideoData(SceAvPlayerFrameInfo* o_videoInfo)
		{
			return sceAvPlayerGetVideoData((SceAvPlayerHandle)ms_player, o_videoInfo);
		}

		u64 GetCurrentVideoTime()
		{
			return static_cast<u64>(sceAvPlayerCurrentTime((SceAvPlayerHandle)ms_player));
		}

		bool IsVideoActive()
		{
			return sceAvPlayerIsActive((SceAvPlayerHandle)ms_player);
		}
		
		void Shutdown()
		{
			// get audio thread back up to speed as we need it again
			if ( ms_bAudioRunning && static_cast<audMixerDeviceOrbis*>( audDriver::GetMixer() )->IsRunningLowerPriority() )
			{
				static_cast<audMixerDeviceOrbis*>( audDriver::GetMixer() )->SetLowerPriority(false);
			}

			scePthreadJoin(m_outputThread, NULL);

 			ms_bAudioRunning = false;
 			sysIpcWaitThreadExit(ms_audioThread);
			ms_audioThread = 0;

			sceAvPlayerClose((SceAvPlayerHandle)ms_player);
		}


	}// namespace AVPlayer

	MediaDecoder::MediaDecoder()
	{

	}

	static bool hasPath = false;
	static char pathname[RAGE_MAX_PATH];

	bool MediaDecoder::InitializePlatformSpecific( const char * const path, bool const captureThumbnailThenWait )
	{
		(void)captureThumbnailThenWait;

		if (GetDecoderState() != DECODER_INVALID)
			return false;

		// the decoder state is more for threading ...which will be done later
		// so it may look daft doing this until then, but may as well structure it now
		SetDecoderStateDirect( DECODER_PREPARING );

		// set up libraries
		s32 returnCode = AVPlayer::InitLibraries();
		videoAssertf(returnCode == SCE_OK, "MediaDecoder::InitializePlatformSpecific - avPlayerInitLibraries fail - 0x%08x", returnCode);

		// set up avPlayer
		if (returnCode == SCE_OK)
		{
			returnCode = AVPlayer::Init();
			videoAssertf(returnCode == SCE_OK, "MediaDecoder::InitializePlatformSpecific - avPlayerInit() fail. No player created");

			if (returnCode == SCE_OK && path)
			{
				hasPath = true;
				formatf(pathname, RAGE_MAX_PATH, "%s", path);
			}

			// Audio and Loading the movie now done later, once AvPlayer Init thread is completed creating AvPlayer
			// see inside UpdatePlatformSpecific
		}

		// if we're not succeeds, set decoder as invalid
		if (returnCode != SCE_OK)
		{
			SetDecoderStateDirect( DECODER_INVALID );
		}
		else
		{
			// B*2365314 - stop video sharing during video playback 
			RecordingTaskOrbis::ProhibitShareRecording(true);
		}

		return IsInitialized();
	}

	void MediaDecoder::ShutdownPlatformSpecific()
	{
		if (IsInitialized())
		{
			AVPlayer::Shutdown();
			AVPlayer::ShutdownLibraries();

			RecordingTaskOrbis::ProhibitShareRecording(false);
		}

		SetDecoderStateDirect( DECODER_INVALID );
	}

	void MediaDecoder::UpdatePlatformSpecific() 
	{
		// avPlayer goes off and does everything, so we shouldn't need to do any threading ourselves for update like we do for other platforms
		if (GetDecoderState() == DECODER_PREPARING)
		{
			// need to finish initalisation once thread has run sceAvPlayerInit() on the cores we want
			if (AVPlayer::ms_bAvPlayerStarted)
			{
				if (AVPlayer::ms_bAvPlayerInitialised)
				{
					s32 returnCode = SCE_OK;
					if (!IsAudioDisabled())
					{
						if (!AVPlayer::InitAudio())
						{
							videoAssertf(true, "MediaDecoder::InitializePlatformSpecific - InitAudio failed");
							SetAudioDisabled(true);
							// best to just disable audio 
						}
					}

					// load MP4 from path
					if (returnCode == SCE_OK)
					{
						videoAssert(hasPath);

						returnCode = AVPlayer::PlayFile(pathname);
						if (returnCode != SCE_OK)
						{
							videoAssertf(true, "MediaDecoder::InitializePlatformSpecific - Filename [ %s ] not found - errorCode: 0x%08x", pathname, returnCode);
						}
						else 
						{
							videoDisplayf("MediaDecoder::InitializePlatformSpecific - PLAYBACK STARTED");
						}
					}
					hasPath = false;

					// if we're not succeeds, set decoder as invalid
					if (returnCode != SCE_OK)
					{
						AVPlayer::ms_bAvPlayerFailed = true;
					}

					AVPlayer::ms_bAvPlayerStarted = false;
					AVPlayer::ms_bAvPlayerInitialised = false;
				}
				
				if (AVPlayer::ms_bAvPlayerFailed)
				{
					SetDecoderStateDirect( DECODER_ERROR );
					AVPlayer::ms_bAvPlayerStarted = false;
					AVPlayer::ms_bAvPlayerFailed = false;
				}

				return;
			}

			// Just fill in dummy data, or unchanging data like resolution
			// we did used to get the info from the audio and video streams, but reading them actually moves the video along
			// and was causing eventual sync issues that would make the video completely stall 
			
			// don't care about these yet
			m_formatInfo.m_fileSize = 0; // doesn't seem to be getting used yet

			// only really used as validation. but videos created on PS4 are 30FPS
			m_formatInfo.m_frameRate = MediaEncoderParams::GetOutputFpsFloat( MediaEncoderParams::OUTPUT_FPS_THIRTY ); 

			// audio is done within own audio thread. storing audio info isn't needed

			// properly used and the mediadecoder generic code will crash without these
			m_formatInfo.m_durationNs = 10; // time doesn't matter, as long as it's enough for system to okay it
			m_formatInfo.m_displayWidth = 1280;
			m_formatInfo.m_displayHeight = 720;
			m_formatInfo.m_videoFormat = MediaCommon::VIDEO_FORMAT_H264;
			m_formatInfo.m_audioFormat = MediaCommon::AUDIO_FORMAT_AAC;

			GenerateDisplayFramesPlatformSpecific();
			SetDecoderStateDirect( DECODER_BUFFERS_CREATED );
			videoDisplayf("UpdatePlatformSpecific - DECODER_PREPARING - buffers created");
		}
		else if (GetDecoderState() == DECODER_ACTIVE)
		{
			if (!AVPlayer::IsPaused() && AVPlayer::IsVideoActive())
			{
				SceAvPlayerFrameInfo videoInfo;
				bool gotFrame = true;
				// video data may not be ready due to being processed, but we have no way of knowing so use while loop
				videoDisplayf("UpdatePlatformSpecific - DECODER_ACTIVE - AVPlayer::GetVideoData");
				while (!AVPlayer::GetVideoData(&videoInfo)) 
				{
					if (!AVPlayer::IsVideoActive())
					{
						gotFrame = false;
						break;
					}
				}

				if (gotFrame)
				{
					videoDisplayf("UpdatePlatformSpecific - DECODER_ACTIVE - gotFrame");
					m_formatInfo.m_displayWidth = videoInfo.details.video.width;
					m_formatInfo.m_displayHeight = videoInfo.details.video.height;

					if( videoVerifyf( m_displayFrame.IsInitialized(), "PopulateDisplayFrame - Invalid frame given" ) )
					{
						// PS4 can't provide us with the size of the frame, so we generate it ourselves
						size_t const c_srcSize = m_formatInfo.GetDecodeAligned420YBufferSize() + m_formatInfo.GetDecodeAligned420UVBufferSize();
						m_displayFrame.CopyNV12IntoFrame( videoInfo.pData, c_srcSize, m_formatInfo);
						m_displayFrame.SetId( (s64)videoInfo.timeStamp );
					}
				}
			}
			
			if (!AVPlayer::IsVideoActive())
			{
				// if not paused and not active, we must be at the end of the file
				SetPlayState(PLAYSTATE_EOF);
			}
			else
			{
				AVPlayer::SetPause(GetPlayState() == PLAYSTATE_PAUSED);
			}
		}
	}

	void MediaDecoder::CleanupPlatformSpecific()
	{
		m_formatInfo.Reset();
		m_playState = PLAYSTATE_INVALID;
		m_currentBackbufferPlaybackTimeOneHNs = 0;

		CleanupDisplayFramesPlatformSpecific();
	}

	bool MediaDecoder::GenerateDisplayFramesPlatformSpecific()
	{
		return m_displayFrame.Initialize( GetFormatInfo() );
	}

	void MediaDecoder::CleanupDisplayFramesPlatformSpecific()
	{
		m_displayFrame.Shutdown();
	}

	bool MediaDecoder::PeekVideoDetails( const char * const path, u32& out_width, u32& out_height, u32& out_durationMs, bool* )
	{
		// TODO: to be replaced by new sceSearchContent ...for now, it needs a dummy duration for the video to run
		UNUSED_PARAM(path);
		out_width = 1280;
		out_height = 720;
		out_durationMs = 10;

		return true;
	}

} // namespace rage

#endif // RSG_ORBIS

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
