/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : media_common.cpp
// PURPOSE : Container for common functionality between the encode and decode paths
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "media_common.h"

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

// Windows

#pragma warning(push)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <mfapi.h>
#include <Mfidl.h>
#include <Mfreadwrite.h>
#include <mfobjects.h> // THIS FUCKING FILE IS POPPING ONE WARNING STACK TOO MANY, AND SETTING WARNING 4200 to error. HENCE THE DOUBLE TAP.
#include <winerror.h>
#pragma warning(pop)
#pragma warning(pop)

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "grcore/texture.h"
#include "math/amath.h"
#include "vector/color32.h"

// framework
#include "video_channel.h"

namespace rage
{
	RAGE_DEFINE_CHANNEL(video_chan);

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

	GUID const& s_wmvVersionGuid = MFVideoFormat_WMV3;
	GUID const& s_wmaVersionGuid = MFAudioFormat_WMAudioV9;

	GUID const* s_supportedVideoGuids[ MediaCommon::VIDEO_FORMAT_MAX ] =
	{
		&MFVideoFormat_H264, 
		&s_wmvVersionGuid
	};

	GUID const* s_supportedAudioGuids[ MediaCommon::AUDIO_FORMAT_MAX ] =
	{
		&MFAudioFormat_AAC, 
		&s_wmaVersionGuid
	};

#endif // RSG_PC && !__RESOURCECOMPILER && !__RGSC_DLL

	static char const* s_videoFormatExtensions[ MediaCommon::VIDEO_FORMAT_MAX ] = 
	{
		"mp4", 
		"wmv"
	};

	// PURPOSE: Prepares the callee thread for encoding/decoding video files
	// RETURNS:
	//		Returns a ePrepareState value representing the success of the prepare operation
	// NOTES:
	//		Each platform operates slightly differently, but if all succeeds this should return STATE_PREPARED.
	MediaCommon::ePrepareState MediaCommon::PrepareThread()
	{
		ePrepareState finalState = MediaCommon::STATE_NONE;

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

		HRESULT hr = CoInitializeEx( NULL, COINIT_MULTITHREADED );

		//! This thread was already in apartment mode, so just initialize like that
		//! since if COM is already initialized on our thread, there aint much we can do about that at the point we hit here...
		if( hr == RPC_E_CHANGED_MODE )
		{
			hr = CoInitializeEx( NULL, COINIT_APARTMENTTHREADED );
		}

		if( videoVerifyf( SUCCEEDED(hr), "MediaCommon::PrepareThread - Unable to initialize COM - 0x%08lx", hr) )
		{
			finalState = MediaCommon::STATE_COM;

			hr = MFStartup( MF_VERSION );
			if( videoVerifyf( SUCCEEDED(hr), "MediaCommon::PrepareThread - Unable to start up Media Foundation! 0x%08lx", hr ) )
			{
				finalState = MediaCommon::STATE_PREPARED;
			}
		}
#else
		// TODO - Not implemented for other platforms yet
		finalState = MediaCommon::STATE_PREPARED;
#endif
		return finalState;
	}

	// PURPOSE: Cleans up the callee thread based on the given prepare state
	// PARAMS:
	//		prepareState - Preparation state this thread is at before calling
	// NOTES:
	//		The prepareState value should be that returned from PrepareThread, to ensure the correct clean-up occurs.
	//		Passing in an arbitrary prepare state will result in undefined behavior.
	void MediaCommon::CleanupThread( ePrepareState& prepareState )
	{
#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL
		if( prepareState >= STATE_MEDIA_FOUNDATION )
		{
			ASSERT_ONLY(HRESULT hr =) MFShutdown();
			videoAssertf( SUCCEEDED(hr), "MediaCommon::CleanupThread - MFShutdown failed with 0x%08lx", hr );
		}

		if( prepareState >= STATE_COM )
		{
			CoUninitialize();
		}
#else
		// TODO - Not implemented for other platforms yet
#endif
		prepareState = STATE_NONE;
	}

	int MediaCommon::MediaMulDiv( s64 const number, s64 const numerator, s64 const denominator )
	{
		s64 mulDivResult = ( number * numerator ) / denominator;
		mulDivResult = mulDivResult > INT_MAX || mulDivResult < INT_MIN ? -1 : mulDivResult;

		return (int)mulDivResult;
	}

	bool MediaCommon::IsSupportedVideoFormat( eVideoFormat const videoFormat )
	{
		bool const c_result = ( IsMp4Supported() && videoFormat == VIDEO_FORMAT_H264 ) || ( IsWmvSupported() && videoFormat == VIDEO_FORMAT_WMV );
		return c_result;
	}

	bool MediaCommon::IsSupportedAudioFormat( eAudioFormat const audioFormat )
	{
		bool const c_result = ( IsMp4Supported() && audioFormat == AUDIO_FORMAT_AAC ) || ( IsWmvSupported() && audioFormat == AUDIO_FORMAT_WMA );
		return c_result;
	}

	bool MediaCommon::IsMp4Supported()
	{
#if RSG_DURANGO || RSG_ORBIS
		return true;
#elif RSG_PC
		return IsWindows7OrGreater();
#else
		return false;
#endif
	}

	bool MediaCommon::IsWmvSupported()
	{
#if SUPPORT_WMV_ENCODING
		return true;
#else
		return false;
#endif
	}

	MediaCommon::eVideoFormat MediaCommon::GetDefaultVideoFormat()
	{
		return IsMp4Supported() ? VIDEO_FORMAT_H264 : IsWmvSupported() ? VIDEO_FORMAT_WMV : VIDEO_FORMAT_INVALID;
	}

	MediaCommon::eAudioFormat MediaCommon::GetDefaultAudioFormatForVideoFormat( eVideoFormat const videoFormat )
	{
		return videoFormat == VIDEO_FORMAT_H264 && IsMp4Supported() ? AUDIO_FORMAT_AAC : 
			videoFormat == VIDEO_FORMAT_WMV && IsWmvSupported() ? AUDIO_FORMAT_WMA : AUDIO_FORMAT_INVALID;
	}

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

	// PURPOSE: Prepares a IMFMediaType object to be compatible with the format the game requires for encoding video
	// PARAMS:
	//		mediaType - Pointer to a IMFMediaType we want to populate
	// RETURNS:
	//		Boolean indicating success if true, false if failed.
	bool MediaCommon::PrepareGameCompatibleOutputVideoMediaType( IMFMediaType*& mediaType, rage::u32 const width, rage::u32 const height, float const fps )
	{
		bool success = false;

		videoAssertf( mediaType, "PrepareGameCompatibleOutputVideoMediaType - Null media type provided" );
		if( mediaType )
		{
			HRESULT hr = mediaType->SetGUID( MF_MT_MAJOR_TYPE, MFMediaType_Video ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - SetGUID - MF_MT_MAJOR_TYPE failed with 0x%08lx", hr );

			GUID const videoFormat = MFVideoFormat_NV12;
			hr = mediaType->SetGUID( MF_MT_SUBTYPE, videoFormat ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - SetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;
		
			hr = mediaType->SetUINT32( MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - SetUINT32 - MF_MT_INTERLACE_MODE failed with 0x%08lx", hr ) && success;

			hr = MFSetAttributeSize( mediaType, MF_MT_FRAME_SIZE, width, height );   
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - MFSetAttributeSize - MF_MT_FRAME_SIZE failed with 0x%08lx", hr ) && success;
		
			hr = MFSetAttributeRatio( mediaType, MF_MT_PIXEL_ASPECT_RATIO, 1, 1 );   
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - MFSetAttributeSize - MF_MT_PIXEL_ASPECT_RATIO failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_ALL_SAMPLES_INDEPENDENT, TRUE ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - SetUINT32 - MF_MT_ALL_SAMPLES_INDEPENDENT failed with 0x%08lx", hr ) && success;

			rage::u32 numerator, denominator;
			MediaCommon::ConvertFPSToNumeratorAndDenominator( fps, numerator, denominator );

			hr = MFSetAttributeRatio( mediaType, MF_MT_FRAME_RATE, numerator, denominator );   
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - MFSetAttributeRatio - MF_MT_FRAME_RATE failed with 0x%08lx", hr ) && success;
		}

		return success;
	}

	// PURPOSE: Prepares a IMFMediaType object be compatible with the format the game requires for encoding audio
	// PARAMS:
	//		mediaType - Pointer to a IMFMediaType we want to populate
	// RETURNS:
	//		Boolean indicating success if true, false if failed.
	bool MediaCommon::PrepareGameCompatibleOutputAudioMediaType( IMFMediaType*& mediaType )
	{
		bool success = false;

		videoAssertf( mediaType, "PrepareGameCompatibleOutputAudioMediaType - Null media type provided" );
		if( mediaType )
		{
			HRESULT hr = mediaType->SetGUID( MF_MT_MAJOR_TYPE, MFMediaType_Audio ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetGUID - MF_MT_MAJOR_TYPE failed with 0x%08lx", hr );

			hr = mediaType->SetGUID( MF_MT_SUBTYPE, MFAudioFormat_PCM ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;
		
			hr = mediaType->SetUINT32( MF_MT_AUDIO_BITS_PER_SAMPLE, rage::MediaCommon::sc_bitsPerGameAudioSample ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_BITS_PER_SAMPLE failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_SAMPLES_PER_SECOND, rage::kMixerNativeSampleRate ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_SAMPLES_PER_SECOND failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_NUM_CHANNELS, rage::MediaCommon::sc_audioChannels ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_NUM_CHANNELS failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_AVG_BYTES_PER_SECOND, rage::MediaCommon::sc_encoderAudioBytesPerSecond ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_AVG_BYTES_PER_SECOND failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_BLOCK_ALIGNMENT, rage::MediaCommon::sc_encoderAudioBlockAlign ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputAudioMediaType - SetUINT32 - MF_MT_AUDIO_AVG_BYTES_PER_SECOND failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_FIXED_SIZE_SAMPLES, TRUE ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleOutputVideoMediaType - SetUINT32 - MF_MT_FIXED_SIZE_SAMPLES failed with 0x%08lx", hr ) && success;
		}

		return success;
	}

	// PURPOSE: Prepares a IMFMediaType object to be compatible with the format the game requires for decoding video
	// PARAMS:
	//		mediaType - Pointer to a IMFMediaType we want to populate
	// RETURNS:
	//		Boolean indicating success if true, false if failed.
	bool MediaCommon::PrepareGameCompatibleInputVideoMediaType( IMFMediaType*& mediaType )
	{
		bool success = false;

		videoAssertf( mediaType, "PrepareGameCompatibleInputVideoMediaType - Null media type provided" );
		if( mediaType )
		{
			HRESULT hr = mediaType->SetGUID( MF_MT_MAJOR_TYPE, MFMediaType_Video ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleInputVideoMediaType - SetGUID - MF_MT_MAJOR_TYPE failed with 0x%08lx", hr );

			hr = mediaType->SetGUID( MF_MT_SUBTYPE, MFVideoFormat_NV12 ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleInputVideoMediaType - SetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;
		}

		return success;
	}

	// PURPOSE: Prepares a IMFMediaType object be compatible with the format the game requires for decoding audio
	// PARAMS:
	//		mediaType - Pointer to a IMFMediaType we want to populate
	// RETURNS:
	//		Boolean indicating success if true, false if failed.
	bool MediaCommon::PrepareGameCompatibleInputAudioMediaType( IMFMediaType*& mediaType )
	{
		bool success = false;
		videoAssertf( mediaType, "PrepareGameCompatibleInputAudioMediaType - Null media type provided" );
		if( mediaType )
		{
			HRESULT hr = mediaType->SetGUID( MF_MT_MAJOR_TYPE, MFMediaType_Audio ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleInputAudioMediaType - SetGUID - MF_MT_MAJOR_TYPE failed with 0x%08lx", hr );

			hr = mediaType->SetGUID( MF_MT_SUBTYPE, MFAudioFormat_PCM ); 
			success = videoVerifyf( SUCCEEDED(hr), "PrepareGameCompatibleInputAudioMediaType - SetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;

			hr = mediaType->SetUINT32( MF_MT_AUDIO_SAMPLES_PER_SECOND, rage::kMixerNativeSampleRate ); 
			success = Verifyf( SUCCEEDED(hr), "PrepareGameCompatibleInputAudioMediaType - SetUINT32 - MF_MT_AUDIO_SAMPLES_PER_SECOND failed with 0x%08lx", hr ) && success;	
		}

		return success;
	}

	GUID const& MediaCommon::GetWmvVersionGUID()
	{
		return IsWmvSupported() ? s_wmvVersionGuid : GUID_NULL;
	}

	GUID const& MediaCommon::ConvertVideoFormatToGUID( MediaCommon::eVideoFormat const format )
	{
		return videoVerifyf( format >= MediaCommon::VIDEO_FORMAT_FIRST && format < MediaCommon::VIDEO_FORMAT_MAX, 
			"MediaCommon::ConvertVideoEncodingToGUID - About to go out of bounds!" ) ? 
			*s_supportedVideoGuids[ format ] : *s_supportedVideoGuids[ MediaCommon::VIDEO_FORMAT_FIRST ];
	}

	MediaCommon::eVideoFormat MediaCommon::ConvertGUIDToVideoFormat( GUID const& videoTypeGuid )
	{
		MediaCommon::eVideoFormat result = VIDEO_FORMAT_INVALID;

		if( IsEqualGUID( MFVideoFormat_H264, videoTypeGuid ) )
		{
			result = VIDEO_FORMAT_H264;
		}
		else if( IsEqualGUID( MFVideoFormat_WMV1, videoTypeGuid ) || IsEqualGUID( MFVideoFormat_WMV2, videoTypeGuid ) || 
			IsEqualGUID( MFVideoFormat_WMV3, videoTypeGuid ) ||IsEqualGUID( MFVideoFormat_WVC1, videoTypeGuid ) )
		{
			result = VIDEO_FORMAT_WMV;
		}

		return result;
	}

	GUID const& MediaCommon::ConvertAudioFormatToGUID( MediaCommon::eAudioFormat const format )
	{
		return videoVerifyf( format >= MediaCommon::AUDIO_FORMAT_FIRST && format < MediaCommon::AUDIO_FORMAT_MAX, 
			"MediaCommon::ConvertAudioEncodingToGUID - About to go out of bounds!" ) ? 
			*s_supportedAudioGuids[ format ] : *s_supportedAudioGuids[ MediaCommon::AUDIO_FORMAT_FIRST ];
	}

	MediaCommon::eAudioFormat MediaCommon::ConvertGUIDToAudioFormat( GUID const& audioTypeGuid )
	{
		MediaCommon::eAudioFormat result = AUDIO_FORMAT_INVALID;

		if( IsEqualGUID( MFAudioFormat_AAC, audioTypeGuid ) )
		{
			result = AUDIO_FORMAT_AAC;
		}
		else if( IsEqualGUID( MFAudioFormat_WMAudioV8, audioTypeGuid ) || IsEqualGUID( MFAudioFormat_WMAudioV9, audioTypeGuid ) || 
			IsEqualGUID( MFAudioFormat_WMASPDIF, audioTypeGuid ) ||IsEqualGUID( MFAudioFormat_WMAudio_Lossless, audioTypeGuid ) )
		{
			result = AUDIO_FORMAT_WMA;
		}

		return result;
	}

	// PURPOSE: Attempts to create a IMFDXGIDeviceManager instance for encoder usage
	// PARAMS:
	//		out_dxDeviceManager - Out pointer to potentially created device manager
	// RETURNS:
	//		True if function succeeded, false otherwise.
	// NOTES:
	//		On PC, this succeeds but returns a NULL device, as we need to support vista still.
	bool MediaCommon::GetDxDeviceManager( IMFDXGIDeviceManager*& out_dxDeviceManager )
	{
		bool success = false;

		if( videoVerifyf( out_dxDeviceManager == NULL, "GetDxDeviceManager - non-null IMFDXGIDeviceManager provided!" ) )
		{
#if RSG_PC
			success = true;
#elif RSG_DURANGO

			grcDeviceHandle* device = GRCDEVICE.GetCurrent();
			if( videoVerifyf( device, "GetDxDeviceManager - Unable to get graphics device" ) )
			{
				IGraphicsUnknown* unknownDevice( NULL );
				HRESULT hr = device->QueryInterface( __uuidof(IGraphicsUnknown), (void**)&unknownDevice );
				if( videoVerifyf( SUCCEEDED(hr), "QueryInterface (IGraphicsUnknown) - Failed with 0x%08lx", hr ) )
				{
					UINT32 uResetToken = 0;
					IMFDXGIDeviceManager* dXGIManager( NULL );

					hr = MFCreateDXGIDeviceManager( &uResetToken, &dXGIManager );
					if( videoVerifyf( SUCCEEDED(hr), "MFCreateDXGIDeviceManager - Failed with 0x%08lx", hr ) )
					{
						hr = MFResetDXGIDeviceManagerX( dXGIManager, unknownDevice, uResetToken );
						if( videoVerifyf( SUCCEEDED(hr), "MFResetDXGIDeviceManagerX - Failed with 0x%08lx", hr ) )
						{
							out_dxDeviceManager = dXGIManager;
							success = true;
						}
					}

					if( !success )
					{
						ReleaseMediaObject( dXGIManager );
					}
				}

				ReleaseMediaObject( unknownDevice );
			}
#endif
		}

		return success;
	}

	// PURPOSE: Prepare attributes applied to source reader/sink writer for encoding
	// PARAMS:
	//		out_attributes - Attributes object created
	//		dxDeviceManager - Device Manager to set as an attribute
	// RETURNS:
	//		True if function succeeded, false otherwise.
	bool MediaCommon::PrepareAttributes( IMFAttributes*& out_attributes, IMFDXGIDeviceManager* dxDeviceManager, bool const reading )
	{
		if( videoVerifyf( out_attributes == NULL, "PrepareAttributes - Non-NULL IMFAttributes provided!" ) )
		{
			IMFAttributes* attributes( NULL );
			HRESULT hr;

			hr = MFCreateAttributes( &attributes, RSG_DURANGO ? 3 : 2 );
			if( videoVerifyf( SUCCEEDED(hr), "PrepareAttributes - MFCreateAttributes failed with 0x%08lx", hr ) )
			{
#if RSG_DURANGO
				if( dxDeviceManager )
				{
					IUnknown* unkDeviceMgr( NULL );

					hr = dxDeviceManager->QueryInterface( IID_PPV_ARGS(&unkDeviceMgr) );
					if( videoVerifyf( SUCCEEDED(hr), "PrepareAttributes - QueryInterface failed for IMFDXGIDeviceManager with 0x%08lx", hr ) )
					{
						hr = attributes->SetUnknown( reading ? MF_SOURCE_READER_D3D_MANAGER : MF_SINK_WRITER_D3D_MANAGER, unkDeviceMgr );
						videoAssertf( SUCCEEDED(hr), "PrepareAttributes - IMFAttributes::SetUnknown (MF_SOURCE_READER_D3D_MANAGER)"
							" failed with 0x%08lx", hr );
					}

					ReleaseMediaObject( unkDeviceMgr );
				}
#else
				(void)dxDeviceManager;
#endif

				{
					hr = attributes->SetUINT32( MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS, TRUE );
					videoDisplayf( "PrepareAttributes - IMFAttributes::SetUINT32 (MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS)"
						" completed with 0x%08lx", hr );

					if( reading )
					{
						hr = attributes->SetUINT32( MF_SOURCE_READER_ENABLE_VIDEO_PROCESSING , FALSE );
						videoDisplayf( "PrepareAttributes - IMFAttributes::SetUINT32 (MF_SOURCE_READER_ENABLE_VIDEO_PROCESSING)"
							" completed with 0x%08lx", hr );
					}
				}

				out_attributes = attributes;
			}
		}

		return out_attributes != NULL;
	}

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

	char const* MediaCommon::GetFormatExtension( MediaCommon::eVideoFormat const format )
	{
		return videoVerifyf( format >= MediaCommon::VIDEO_FORMAT_FIRST && format < MediaCommon::VIDEO_FORMAT_MAX, 
			"MediaCommon::GetFormatExtension - About to go out of bounds!" ) ? 
			s_videoFormatExtensions[ format ] : s_videoFormatExtensions[ MediaCommon::VIDEO_FORMAT_FIRST ];
	}

	void rage::MediaCommon::ConvertFPSToNumeratorAndDenominator( float const fps, u32& out_numerator, u32& out_denominator )
	{
		// Support standard HD frame rate of 24 FPS
		if( fps >= 23.975f && fps <= 23.977f )
		{
			out_numerator = 24000;
			out_denominator = 1001;
		}
		else if( fps >= 29.96f && fps <= 29.98f )
		{
			out_numerator = 30000;
			out_denominator = 1001;
		}
		else if( fps >= 59.93f && fps <= 59.95f )
		{
			out_numerator = 60000;
			out_denominator = 1001;
		}
		else
		{
			// Otherwise we only support whole frame-rates, so drop any decimal points. Sorry PAL Encodings!
			out_numerator = (u32)fps;
			out_denominator = 1;
		}
	}

	void MediaCommon::ConvertNumeratorAndDenominatorToFPS( float& out_fps, u32 const out_numerator, u32 const out_denominator )
	{
		out_fps = (float)out_numerator / (float)out_denominator;
	}

	// PURPOSE: Converts a 4:2:2 YUY2 into the two separate RGB pixels encoded
	// PARAMS:
	//		srcYuy2 - Pointer to the source yuy2 data to decode
	//		rgb1	- Pointer to the destination for RGB pixel 1
	//		rgb2	- Pointer to the destination for RGB pixel 2
	void MediaCommon::ConvertYUY2ToRGB( u8 const* srcYuy2, u8* rgb1, u8* rgb2 )
	{
		videoAssertf( srcYuy2, "MediaCommon::ConvertYUY2ToRGB - NULL source YUY2 data" );
		videoAssertf( rgb1, "MediaCommon::ConvertYUY2ToRGB - NULL rgb1 destination" );
		videoAssertf( rgb2, "MediaCommon::ConvertYUY2ToRGB - NULL rgb2 destination" );
		videoAssertf( rgb2 != rgb1, "MediaCommon::ConvertYUY2ToRGB - rgb1 and rgb2 are the same pixel" );

		YUY2Frame const* yuyFrame = (YUY2Frame const*)srcYuy2;
		ConvertYUVToRGB( yuyFrame->y1, yuyFrame->u, yuyFrame->v, rgb1 );
		ConvertYUVToRGB( yuyFrame->y2, yuyFrame->u, yuyFrame->v, rgb2 );
	}

	void MediaCommon::ConvertYUVToRGB( u8 const y, u8 const u, u8 const v, u8* rgbDest )
	{
		videoAssertf( rgbDest, "MediaCommon::ConvertYUVToRGB - NULL rgb destination" );

		// Taken the conversion here from MSDN, since this was developed for PC video decoding primarily...
		// http://msdn.microsoft.com/en-us/library/ms893078.aspx

		u32 const C = y - 16;
		u32 const D = u - 128;
		u32 const E = v - 128;

		rgbDest[0] = (u8)Clamp<u32>( ( ( 298 * C           + 409 * E + 128) >> 8), 0, 255 );
		rgbDest[1] = (u8)Clamp<u32>( ( ( 298 * C - 100 * D - 208 * E + 128) >> 8), 0, 255 );
		rgbDest[2] = (u8)Clamp<u32>( ( ( 298 * C + 516 * D           + 128) >> 8), 0, 255 );
	}

	void MediaCommon::ConvertRGBToYUV( u8 const * const rgb, u8& y, u8& u, u8& v )
	{
		videoAssertf( rgb, "MediaCommon::ConvertRGBAToYUV - NULL rgba source" );

		u8 const r = rgb[0];
		u8 const g = rgb[1];
		u8 const b = rgb[2];

		y = (u8)Clamp<u32>( ( (  66 * r + 129 * g +  25 * b + 128) >> 8) +  16, 0, 255 );
		u = (u8)Clamp<u32>( ( ( -38 * r -  74 * g + 112 * b + 128) >> 8) + 128, 0, 255 );
		v = (u8)Clamp<u32>( ( ( 112 * r -  94 * g -  18 * b + 128) >> 8) + 128, 0, 255 );
	}

	void MediaCommon::FillTexture( grcTexture* texture, u8 const fillValue )
	{
		grcTextureLock oLock;
		if( texture && texture->LockRect( 0, 0, oLock ) )
		{
			u8* lineBase = (u8*)oLock.Base;
			u32 const c_lineSize = oLock.Width * (oLock.BitsPerPixel / 8);

			for( int line = 0; line < oLock.Height; ++line, lineBase += oLock.Pitch )
			{
				sysMemSet( lineBase, fillValue, c_lineSize );
			}

			texture->UnlockRect( oLock );
		}
	}

	void MediaCommon::InitilizeXXXATexture( grcTexture* texture )
	{
		grcTextureLock oLock;
		if( texture && texture->LockRect( 0, 0, oLock ) )
		{
			u8* lineBase = (u8*)oLock.Base;
			rage::Color32 const c( 0, 0, 0, 255 );

			for( int line = 0; line < oLock.Height; ++line, lineBase += oLock.Pitch )
			{
				u32* dest = (u32*)lineBase;

				for( int x = 0; x < oLock.Width; ++x )
				{
					dest[x] = c.GetDeviceColor();
				}
			}

			texture->UnlockRect( oLock );
		}
	}

#if RSG_PC || RSG_DURANGO
	size_t MediaCommon::GetExtraBytesRequiredForEncode()
	{
		size_t extraBytesRequired = 0;
		
#if RSG_PC
		if(IsWindows8OrGreater())
		{
			// On windows 8 and above we need to reserve an extra 100 MBs to prevent crashes.
			// Reserving a little over 300, just in case.
			extraBytesRequired = 304857600;
		}
#endif // RSG_PC

		return extraBytesRequired;
	}
#endif // RSG_PC || RSG_DURANGO

#if RSG_PC

	// Extracted from VersionHelpers.h until we move to the W8.1 SDK or greater
	bool MediaCommon::IsWindowsVersionOrGreater( u16 const wMajorVersion, u16 const wMinorVersion, u16 const wServicePackMajor )
	{
		OSVERSIONINFOEXW osvi = { sizeof(osvi), 0, 0, 0, 0, {0}, 0, 0 };
		DWORDLONG        const dwlConditionMask = VerSetConditionMask(
			VerSetConditionMask(
			VerSetConditionMask(
			0, VER_MAJORVERSION, VER_GREATER_EQUAL),
			VER_MINORVERSION, VER_GREATER_EQUAL),
			VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL);

		osvi.dwMajorVersion = wMajorVersion;
		osvi.dwMinorVersion = wMinorVersion;
		osvi.wServicePackMajor = wServicePackMajor;

		return VerifyVersionInfoW( &osvi, VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR, dwlConditionMask) != FALSE;
	}

	bool MediaCommon::IsWindows7OrGreater()
	{
		return IsWindowsVersionOrGreater( HIBYTE(_WIN32_WINNT_WIN7), LOBYTE(_WIN32_WINNT_WIN7), 0 );
	}

	bool MediaCommon::IsWindows8OrGreater()
	{
		#define LOCAL_WIN32_WINNT_WIN8                   0x0602
		return IsWindowsVersionOrGreater( HIBYTE(LOCAL_WIN32_WINNT_WIN8), LOBYTE(LOCAL_WIN32_WINNT_WIN8), 0 );
	}

	bool MediaCommon::IsWindows8Point1OrGreater()
	{
		#define LOCAL_WIN32_WINNT_WINBLUE                0x0603
		return IsWindowsVersionOrGreater( HIBYTE(LOCAL_WIN32_WINNT_WINBLUE), LOBYTE(LOCAL_WIN32_WINNT_WINBLUE), 0 );
	}

#endif // ( RSG_PC )

} // namespace rage
