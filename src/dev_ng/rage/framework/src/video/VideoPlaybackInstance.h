/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlaybackInstance.h
// PURPOSE : Class for controlling the playback of a given video.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "video/VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#ifndef INC_VIDEO_PLAYBACK_INSTANCE_H_
#define INC_VIDEO_PLAYBACK_INSTANCE_H_

// rage
#include "grcore/effect_typedefs.h"
#include "vectormath/vec4v.h"

// framework
#include "mediadecoder.h"
#include "fwui/Common/fwuiScaling.h"

namespace rage
{
	class audEntity;
	class audSound;
	class grcEffect;
	class grcTexture;

	class VideoPlaybackInstance
	{
	public: // declarations and variables
		enum ePlaybackState
		{
			PLAYBACK_INVALID,				// Free to be used

			PLAYBACK_PREPARING,				// Setting up internal state to be ready to decode
			PLAYBACK_READY_OR_DECODING,		// All ready to do some (or already) decoding

			PLAYBACK_ERROR,					// Playback hit an error state and cannot continue
			PLAYBACK_CLEANUP				// Shutting down and releasing resources
		};

	public: // methods

		VideoPlaybackInstance();
		~VideoPlaybackInstance();

		static void SetAudioEntityForPlayback( audEntity* entity );
		static void SetAudioBucketId( u32 const bucketId );

		bool	InitShaders();
		void	ShutdownShaders();

		bool Initialize( const char * const path, bool const captureThumbnailThenWait, bool const noAudio );
		bool IsInitialized();

		bool IsSuitableForRendering() const { return HasCreatedTextures() && m_state >= PLAYBACK_READY_OR_DECODING && m_state < PLAYBACK_CLEANUP; }
		bool HasValidFrame() const { return IsSuitableForRendering() && m_textureIdentifier >= 0; }

		bool HasError() const;

		void Update();
		void UpdatePreRender();

		void RenderToGivenSize( int const rtWidth, int const rtHeight, fwuiScaling::eMode const scaleMode );

		size_t GetFileSize() const;
		u64 GetDurationMs() const;
		u64 GetCurrentTimeMs() const;

		bool Seek( u64 const timeMs );
		
		bool Play();
        bool IsPaused() const;
        bool IsAtStartOfFile() const;
		bool IsAtEndOfFile() const;
		bool Pause();
		bool IsPlaybackActive() const;

		void Shutdown();

		void RenderVideoOverlay( float const x, float const y, float const x2, float const y2 );

#if __BANK
		void RenderVideoOverlayBank();
#endif // __BANK

	private: // declarations and variables
		static audEntity*			ms_audioEntity;
		static u32					ms_audioBucketId;

		MediaDecoder				m_mediaDecoder;
		u32							m_textureWidth;
		u32							m_textureHeight;
		s64							m_textureIdentifier;

		grcTexture*					m_yPlane;
		grcTexture*					m_uPlane;
		grcTexture*					m_vPlane;

		bool						m_Reset;

		grcEffect*					m_pEffect;
		grcEffectVar				m_nYPlaneId;
		grcEffectVar				m_ncRPlaneId;
		grcEffectVar				m_ncBPlaneId;
		grcEffectVar				m_nUVScalarId;
		grcEffectTechnique			m_tOpaque;

		audSound*					m_sound;
		ePlaybackState				m_state;
		bool						m_noAudio : 1;

	private: // methods
		bool HasCreatedTextures() const;

#if RSG_ORBIS
		inline bool HasCreatedSound() const { return true; }
#else
		inline bool HasCreatedSound() const { return m_sound != NULL || m_noAudio; }
#endif
		bool CreateTextures();
		void DestroyTextures();

		bool CreateSound();
		void DestroySound();

		Vec4V_Out GetUVScalar();
	};

} // namespace rage

#endif // INC_VIDEO_PLAYBACK_INSTANCE_H_

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
