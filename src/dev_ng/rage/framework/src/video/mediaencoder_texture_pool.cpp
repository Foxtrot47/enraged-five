/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_texture_pool.cpp
// PURPOSE : Class for allocating/managing a pool of textures for media encoding
//
// NOTES : This is only available on Xb1, and it circumvents RAGE texture creation
//		   to work around a bug in the Microsoft June XDK.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if (RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "grcore/device.h"
#include "system/d3d11.h"

// framework
#include "mediaencoder_texture_pool.h"
#include "video_channel.h"

#pragma warning(disable: 4668)
#include <Windows.h>
#pragma warning(error: 4668)

namespace rage
{
	rage::MediaEncoderTexturePool ENCODER_TEXTURE_POOL;

	MediaEncoderTexturePool::MediaEncoderTexturePool()
		: ms_nextTextureToReturn( 0 )
	{
		sysMemZeroBytes<sizeof(ms_texturePool)>( ms_texturePool );
	}

	MediaEncoderTexturePool::~MediaEncoderTexturePool()
	{
		Shutdown();
	}

	bool MediaEncoderTexturePool::Initialize( u32 const c_width, u32 const c_height )
	{
		if( !AreTexturesAllocated() )
		{
			HRESULT hr;
			ID3D11Device* device = static_cast<ID3D11Device*>( GRCDEVICE.GetCurrent() );
			if( device )
			{
				D3D11_TEXTURE2D_DESC nv12Desc = {};
				nv12Desc.Usage =  D3D11_USAGE_STAGING;
				nv12Desc.Format = DXGI_FORMAT_NV12;
				nv12Desc.BindFlags = D3D11_BIND_VIDEO_ENCODER;
#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING
				nv12Desc.CPUAccessFlags = 0;
#else
				nv12Desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE; // For whilst we are allowing SW Colour Conversion
#endif
				nv12Desc.MiscFlags =  D3D11_RESOURCE_MISC_SHARED;

				// encoder required the width and height to be even
				nv12Desc.Width = ( c_width / 2 ) * 2;  
				nv12Desc.Height = ( c_height / 2 ) * 2; 
				nv12Desc.ArraySize = 1;
				nv12Desc.MipLevels = 1;
				nv12Desc.SampleDesc.Count = 1;
				nv12Desc.SampleDesc.Quality = 0;

				for( u32 index = 0; index < MEDIA_EXPORT_MAX_TEXTURES; ++index )
				{
					ID3D11Texture2D*& nv12Texture = ms_texturePool[ index ];
					hr = device->CreateTexture2D( &nv12Desc, NULL, &nv12Texture );
					bool const c_success = SUCCEEDED(hr) && nv12Texture != NULL;
					videoAssertf( c_success, "CreateTexture2D for NV12 texture - Failed with 0x%08lx", hr );

					if( !c_success )
					{
						// Texture creation failed, so clean up what we can and abandon ship
						Shutdown();
						break;
					}
				}
			}
		}
		else
		{
			videoDisplayf( "Call to MediaEncoderTexturePool::AllocateTextures when textures already initialized. Logic problem?" );
		}

		return AreTexturesAllocated();
	}

	bool MediaEncoderTexturePool::AreTexturesAllocated() const
	{
		bool allocated = true;

		for( u32 index = 0; allocated && index < MEDIA_EXPORT_MAX_TEXTURES; ++index )
		{
			allocated = allocated && ms_texturePool[ index ] != NULL;
		}

		return allocated;
	}

	void MediaEncoderTexturePool::Shutdown()
	{
		for( u32 index = 0; index < MEDIA_EXPORT_MAX_TEXTURES; ++index )
		{
			ID3D11Texture2D*& currentTexture = ms_texturePool[ index ];
			ReleaseMediaObject( currentTexture );
		}
	}

	ID3D11Texture2D* MediaEncoderTexturePool::GetNextTexture()
	{
		videoAssertf( ms_nextTextureToReturn < MEDIA_EXPORT_MAX_TEXTURES, "GetNextTexture - Next texture index is out of range" );
		videoAssertf( ms_texturePool[ms_nextTextureToReturn] != NULL, "GetNextTexture - Next texture index %u is not allocated!", ms_nextTextureToReturn );

		ID3D11Texture2D* nextTexture = ms_texturePool[ms_nextTextureToReturn++];
		ms_nextTextureToReturn = ms_nextTextureToReturn >= MEDIA_EXPORT_MAX_TEXTURES ? 0 : ms_nextTextureToReturn;

		return nextTexture;
	}

} // namespace rage

#endif // (RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL
