#if RSG_ORBIS

#include <scebase_common.h>
#include <_kernel.h>
#include <_fs.h>
#include <libsysmodule.h>
#include <sdk_version.h>

#include <content_delete.h>

#pragma comment(lib,"libSceContentDelete_stub_weak.a")

#include "contentDelete_Orbis.h"

using namespace rage;

bool CContentDelete::m_InitSuccess = false;

void CContentDelete::Init(unsigned)
{
	Assertf(m_InitSuccess == false, "ERROR: CContentDelete::Init() - Already Inited");

	if( Verifyf(sceSysmoduleLoadModule(SCE_SYSMODULE_CONTENT_DELETE) == SCE_OK, "ERROR: Load of SCE_SYSMODULE_CONTENT_DELETE module failed") )
	{
		// Initialize ContentDelete
		SceContentDeleteInitParam initParam;
		initParam.heapSize = SCE_CONTENT_DELETE_HEAP_SIZE;
		if (Verifyf(sceContentDeleteInitialize(&initParam) == SCE_OK, "ERROR: Load of sceContentDeleteInitialize failed") )
		{
			m_InitSuccess = true;
		}
	}
}

bool CContentDelete::DoDeleteVideo(const char *pMoviePath)
{
	int ret = sceContentDeleteByPath(pMoviePath);
	return (ret == SCE_OK);
}

bool CContentDelete::DoDeleteVideo(rage::s64 contentId)
{
	int ret = sceContentDeleteById(contentId);
	return (ret == SCE_OK);
}

void CContentDelete::Shutdown(unsigned)
{
	if(m_InitSuccess)
	{
		// Really need to sceContentExportCancel() then sceContentExportFinish() any content handles currently active.
		if( Verifyf( sceContentDeleteTerminate() == SCE_OK,"ERROR:- CContentDelete::Shutdown() - sceContentDeleteTerminate() Failed"))
		{
			if( Verifyf(sceSysmoduleUnloadModule(SCE_SYSMODULE_CONTENT_DELETE) == SCE_OK, "ERROR:- CContentDelete::Shutdown() - sceSysmoduleUnloadModule(SCE_SYSMODULE_CONTENT_DELETE) Failed") )
			{
				m_InitSuccess = false;
			}
		}
	}
}

#endif	//RSG_ORBIS
