/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlayback.h
// PURPOSE : Controller class for requesting/accessing playback of a video
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "video/VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#ifndef INC_VIDEO_PLAYBACK_H_
#define INC_VIDEO_PLAYBACK_H_

// rage
#include "atl/string.h"
#include "atl/hashstring.h"
#include "system/service.h"

// framework
#include "fwscene/stores/imposedTextureDictionary.h"
#include "fwui/Common/fwuiScaling.h"
#include "VideoPlaybackInstance.h"

namespace rage
{
	typedef int VideoInstanceHandle;
	class audEntity;

	class VideoPlayback
	{
	public: // methods

		static void Init(unsigned initMode);
		static void Shutdown(unsigned shutdownMode);

		static void SetAudioEntityForPlayback( audEntity* entity );
		static void SetAudioBucketId( u32 const bucketId );

		static bool Activate();
		static bool IsActive();
		static void Update();
		static void UpdatePreRender();
		static void Deactivate();

		static bool IsPlaybackInstanceAvailabile();
		static bool IsPlaybackActive(bool allowPaused = true);

		inline static bool IsValidHandle( VideoInstanceHandle const handle ) { return IsActive() && handle >= 0 && handle < GetMaxInstances(); }
		
		static bool HasValidFrame( VideoInstanceHandle const handle );
		static bool HasError( VideoInstanceHandle const handle );
		static void RenderToGivenSize( VideoInstanceHandle const handle, int const rtWidth, int const rtHeight, fwuiScaling::eMode const scaleMode );

		static bool IsVideoSupported( const char * const path, u32 const maxWidth, u32 const maxHeight, u32 const minDurationMs, 
			u32 * const out_width, u32 * const out_height, u32 * const out_durationMs );

		static VideoInstanceHandle PrepareVideo( const char * const path, bool const captureThumbnailThenWait, bool const ignoreAudioTrack );

		static size_t GetFileSize( VideoInstanceHandle const handle );

		static u64 GetDurationMs( VideoInstanceHandle const handle );
		static u64 GetCurrentTimeMs( VideoInstanceHandle const handle );

		static bool Seek( VideoInstanceHandle const handle, u64 const timeMs );

		static VideoInstanceHandle PlayVideo( const char * const path, bool const ignoreAudioTrack = false );
		static bool	PlayVideo( VideoInstanceHandle const handle );

        static bool IsAtStartOfVideo( VideoInstanceHandle const handle );
		static bool IsAtEndOfVideo( VideoInstanceHandle const handle );
		static bool	IsPaused( VideoInstanceHandle const handle );
		static bool	PauseVideo( VideoInstanceHandle const handle );

		static void CleanupVideo( VideoInstanceHandle& handle );

#if __BANK
		static void RenderVideoOverlayBank();
#endif // __BANK

	private: // declarations and variables
		static VideoPlaybackInstance		ms_playbackInstances[MAX_CONCURRENT_VIDEOS];
		static ServiceDelegate				ms_serviceDelegate;

	private: // methods

		inline static int GetMaxInstances() { return MAX_CONCURRENT_VIDEOS; }

		inline static VideoPlaybackInstance& GetPlaybackInstance( VideoInstanceHandle const handle )
		{
			Assertf( IsValidHandle( handle ), "VideoPlayback::GetPlaybackInstance - Handle out of range, about to A/V" );
			return ms_playbackInstances[handle];
		}

		inline static VideoPlaybackInstance* TryGetPlaybackInstance( VideoInstanceHandle const handle )
		{
			return IsValidHandle( handle ) ? &ms_playbackInstances[handle] : NULL;
		}

		static VideoInstanceHandle FindUnusedPlaybackInstance();
		static void ShutdownAllPlaybackInstances();

		static void sysOnServiceEvent( rage::sysServiceEvent* evnt);

#if __BANK
		static void InitBank();
		static void CreateBank();

		static void PreparePlaybackCB();
		static void PauseOrResumePlaybackCB();
		static void SeekMidwayCB();
		static void StopPlaybackCB();

		static void ShutdownBank();
#endif // __BANK

	};

} // namespace rage

#endif // INC_VIDEO_PLAYBACK_H_

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
