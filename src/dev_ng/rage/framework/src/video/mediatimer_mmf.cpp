/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediatimer_mmf.cpp
// PURPOSE : Media Timer implementation for platforms using the Microsoft Media Foundation
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediatimer.h"

#if (!MEDIA_TIMER_SYSTEM) && !__RESOURCECOMPILER

#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <mfapi.h>
#include <Mfidl.h>
#pragma warning(pop)

// framework
#include "video_channel.h"

namespace rage
{
	u64 MediaTimer::GetConstantTimeNs()
	{
		u64 const c_sysTimeOneHNs = GetConstantTimeOneHNs();
		u64 const c_sysTimeNs = ONE_H_NS_UNITS_TO_NANOSECONDS( c_sysTimeOneHNs );

		return c_sysTimeNs;
	}

	u64 MediaTimer::GetConstantTimeOneHNs()
	{
		MFTIME const c_sysTimeOneHNs = MFGetSystemTime();
		return (u64)c_sysTimeOneHNs;
	}

} // namespace rage

#endif // (!MEDIA_TIMER_SYSTEM) && !__RESOURCECOMPILER
