/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_win.cpp
// PURPOSE : Class for encoding video/audio streams. Xb1 Specific.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediaencoder.h"

#if RSG_DURANGO && !__RESOURCECOMPILER

// Windows
#pragma warning(disable: 4668)
#include <Windows.h>
#include <winerror.h>
#pragma warning(error: 4668)

// rage
#include "grcore/texture.h"
#include "system/ipc.h"

// framework
#include "media_common.h"
#include "mediaencoder_params.h"
#include "mediaencoder_texture_pool.h"
#include "video_channel.h"

namespace rage
{

	void MediaEncoder::PrepareRenderThread()
	{
		ASSERT_ONLY( bool const c_result = ) 
			ENCODER_TEXTURE_POOL.Initialize( MEDIA_ENCODER_DEFAULT_WIDTH, MEDIA_ENCODER_DEFAULT_HEIGHT );

		videoAssertf( c_result,
			"MediaEncoder::PrepareRenderThread - Failed to create texture pool. Videos rendered will be audio only!" );

		GenerateDummyVideoData();
	}

	void MediaEncoder::CleanupRenderThread()
	{
		ENCODER_TEXTURE_POOL.Shutdown();
		CleanupDummyVideoData();
	}

	bool MediaEncoder::ThreadData::IsVideoFrameCaptureAllowed() const
	{
		return m_buffers.IsInitialized() && !HasError() && !m_pauseFlag && ( !m_killFlag || ( m_killFlag && m_drainFlag ) ) &&
			( m_threadMessaging & ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN ) == 0;
	}

	void MediaEncoder::GenerateDummyTexture( grcTexture*& out_texture )
	{
#if defined( USE_DUMMY_VIDEO_DATA )

// NV12 is an 8-bit Y plane followed by an interleaved 8-bit U/V plane with 2x2 sub-sampling
#define ALIGNED_WIDTH ( ( MEDIA_ENCODER_DEFAULT_WIDTH / 2 ) * 2 )
#define ALIGNED_HEIGHT ( ( MEDIA_ENCODER_DEFAULT_HEIGHT / 2 ) * 2 )
#define Y_BUFF_SIZE ( ALIGNED_WIDTH * ALIGNED_HEIGHT )
#define U_BUFF_SIZE ( Y_BUFF_SIZE / 4 )
#define V_BUFF_SIZE ( U_BUFF_SIZE )
#define BUFF_SIZE ( Y_BUFF_SIZE + U_BUFF_SIZE + V_BUFF_SIZE )

		if( out_texture == NULL )
		{
			static u8 sc_dummyBuffer[ BUFF_SIZE ];

			// Set the colour planes to make a green frame
			for ( size_t i = 0; i < Y_BUFF_SIZE; ++i )
			{
				sc_dummyBuffer[i] = 145;
			}

			for ( size_t i = 0; i < U_BUFF_SIZE; ++i )
			{
				(sc_dummyBuffer + Y_BUFF_SIZE)[i] = 54;
			}

			for ( size_t i = 0; i < V_BUFF_SIZE; ++i )
			{
				(sc_dummyBuffer + Y_BUFF_SIZE + U_BUFF_SIZE)[i] = 34;
			}

			//! This texture create may assert about threading. We don't care, this is dev only
			grcTextureFactory::TextureCreateParams params( 
				grcTextureFactory::TextureCreateParams::VIDEO,
				grcTextureFactory::TextureCreateParams::LINEAR,
				grcsRead|grcsWrite, NULL,
				grcTextureFactory::TextureCreateParams::NORMAL );

			out_texture =
				grcTextureFactory::GetInstance().Create( 
				ALIGNED_WIDTH, 
				ALIGNED_HEIGHT, 
				grctfNV12, sc_dummyBuffer, 1, &params);

			videoAssertf( out_texture, "MediaEncoder::GenerateDummyTexture - Unable to create dummy texture!" );	
		}

		out_texture->AddRef();
#else
		(void)out_texture;
#endif // defined( USE_DUMMY_VIDEO_DATA )
	}

} // namespace rage

#endif // RSG_DURANGO && !__RESOURCECOMPILER
