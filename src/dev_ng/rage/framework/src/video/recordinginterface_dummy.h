#if ( !RSG_DURANGO && !RSG_ORBIS && !RSG_PC )

#include "recordinginterface.h"

namespace rage
{

class VideoRecordingInterfaceDummy: public VideoRecordingInterface
{
public:

	VideoRecordingInterfaceDummy();
	virtual ~VideoRecordingInterfaceDummy();


	void	EnableRecording();
	void	DisableRecording();

	void	Update();


	RecordingTask *CreateRecordingTask()
	{
		return 	NULL;
	}

	void DeleteRecordingTask(RecordingTask *)
	{
	}

	void DeleteClip(u32)
	{

	}

protected:
private:
};

}

#endif
