/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediatimer_system.cpp
// PURPOSE : Media Timer implementation for platforms using rage system timer
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediatimer.h"

#if defined( MEDIA_TIMER_SYSTEM ) && MEDIA_TIMER_SYSTEM

// rage
#include "system/timer.h"

// framework
#include "media_common.h"
#include "video_channel.h"

namespace rage
{

	u64 MediaTimer::GetConstantTimeNs()
	{
		u32	const c_sysTimeMs = sysTimer::GetSystemMsTime();
		u64 const c_sysTimeNs = MILLISECONDS_TO_NANOSECONDS( c_sysTimeMs );

		return c_sysTimeNs;
	}

	u64 MediaTimer::GetConstantTimeOneHNs()
	{
		u64 const c_sysTimeNs = GetConstantTimeNs();
		u64 const c_sysTimeOneHNs = NANOSECONDS_TO_ONE_H_NS_UNITS( c_sysTimeNs );

		return c_sysTimeOneHNs;
	}

} // namespace rage

#endif // defined( MEDIA_TIMER_SYSTEM ) && MEDIA_TIMER_SYSTEM