/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlaybackInstance.cpp
// PURPOSE : Class for controlling the playback of a given video.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "video/VideoPlaybackSettings.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

// rage
#include "audioengine/engine.h"
#include "audioengine/entity.h"
#include "audiosoundtypes/externalstreamsound.h"
#include "audiosoundtypes/sound.h"
#include "grcore/image.h"
#include "grcore/texture.h"
#include "grcore/quads.h"
#include "grcore/resourcecache.h"
#include "grcore/viewport.h"
#include "math/simplemath.h"
#include "system/memory.h"
#include "system/pix.h"

// framework
#include "fwlocalisation/templateString.h"
#include "video/video_channel.h"
#include "video/VideoPlaybackInstance.h"

#if RSG_PC
#include "grcore/texture_d3d11.h"
#endif

namespace rage
{
	audEntity* VideoPlaybackInstance::ms_audioEntity( NULL );
	u32 VideoPlaybackInstance::ms_audioBucketId( 0 );

#define VIDEO_PLAYBACK_SOUND_NAME ( "VIDEO_PLAYBACK_STEREO_SOUND" )

	VideoPlaybackInstance::VideoPlaybackInstance()
		: m_mediaDecoder()
		, m_textureWidth( 0 )
		, m_textureHeight( 0 )
		, m_textureIdentifier( -1 )
		, m_yPlane( NULL )
		, m_uPlane( NULL )
		, m_vPlane( NULL )
		, m_pEffect( NULL )
		, m_nYPlaneId( grcevNONE )
		, m_ncRPlaneId( grcevNONE )
		, m_ncBPlaneId( grcevNONE )
		, m_nUVScalarId( grcevNONE )
		, m_tOpaque( grcetNONE )
		, m_sound( NULL )
		, m_state( PLAYBACK_INVALID )
	{
		
	}

	VideoPlaybackInstance::~VideoPlaybackInstance()
	{
		Shutdown();
	}

	void VideoPlaybackInstance::SetAudioEntityForPlayback( audEntity* entity )
	{
		ms_audioEntity = entity;
	}

	void VideoPlaybackInstance::SetAudioBucketId( u32 const bucketId )
	{
		ms_audioBucketId = bucketId;
	}

	bool VideoPlaybackInstance::InitShaders()
	{
		if (m_pEffect == NULL) 
		{
			m_pEffect = grcEffect::LookupEffect( atStringHash( "rage_bink" ) );
			if( Verifyf(m_pEffect != NULL, "VideoPlaybackInstance::InitShaders Bink shader not loaded?") )
			{
				m_nYPlaneId		= m_pEffect->LookupVar("YPlane");
				m_ncRPlaneId	= m_pEffect->LookupVar("cRPlane");
				m_ncBPlaneId	= m_pEffect->LookupVar("cBPlane");
				m_nUVScalarId	= m_pEffect->LookupVar("UVScalar");
				m_tOpaque		= m_pEffect->LookupTechnique("bink_opaque");
			}
		}

		return m_pEffect != NULL;
	}

	void VideoPlaybackInstance::ShutdownShaders()
	{
		m_pEffect = NULL;
	}

	bool VideoPlaybackInstance::Initialize( const char * const path, bool const captureThumbnailThenWait, bool const noAudio )
	{
		bool success = false;

		if( videoVerifyf( !IsInitialized(), "VideoPlaybackInstance::Initialize - Double initialization" ) )
		{
			success = m_mediaDecoder.Initialize( path, captureThumbnailThenWait, noAudio );
			success = success && InitShaders();
			m_state = success ? PLAYBACK_PREPARING : PLAYBACK_ERROR;
			m_noAudio = noAudio;
		}

		return success;
	}

	bool VideoPlaybackInstance::IsInitialized()
	{
		return m_mediaDecoder.IsInitialized() && m_state != PLAYBACK_INVALID;
	}

	bool VideoPlaybackInstance::HasError() const
	{
		return m_state == PLAYBACK_ERROR;
	}

	void VideoPlaybackInstance::Update()
	{
		if( !HasError() )
		{
			if( m_mediaDecoder.IsPendingActivate() )
			{
				if( !HasCreatedSound() )
				{
					m_state = videoVerifyf( CreateSound(), "VideoPlaybackInstance::UpdateRT - Unable to create audio decoding space" ) ? m_state : PLAYBACK_ERROR;
				}

				if( !HasCreatedTextures() )
				{
					m_state = m_state != PLAYBACK_ERROR && videoVerifyf( CreateTextures(), 
						"VideoPlaybackInstance::UpdateRT - Unable to generate textures or audio decoding space" ) ? PLAYBACK_READY_OR_DECODING : PLAYBACK_ERROR;
				}

				m_mediaDecoder.MarkAsActive();
			}
			else if( m_mediaDecoder.GetDecoderState() != MediaDecoder::DECODER_INVALID )
			{
				m_mediaDecoder.Update();
				m_state = m_mediaDecoder.HasError() ? PLAYBACK_ERROR : m_state;
			}
		}
	}

	void VideoPlaybackInstance::UpdatePreRender()
	{
		if( !HasError() )
		{
			if( HasCreatedTextures() && m_mediaDecoder.ReadyToOrDecoding() )
			{
				s64 oldIdentifier = m_textureIdentifier;
				m_mediaDecoder.PopulateTextureFrame( m_textureIdentifier, m_yPlane, m_uPlane, m_vPlane );

				if( m_sound && oldIdentifier == -1 && oldIdentifier != m_textureIdentifier )
				{
					m_sound->Play();
				}
			}
		}
	}

	void VideoPlaybackInstance::RenderToGivenSize( int const rtWidth, int const rtHeight, fwuiScaling::eMode const scaleMode )
	{
		bool const c_readyToRender = m_mediaDecoder.ReadyToOrDecoding();

		int renderWidth = 0;
		int renderHeight = 0;
		
		if( c_readyToRender )
		{
			int const c_sourceWidth = m_mediaDecoder.GetVideoSourceWidth();
			int const c_sourceHeight = m_mediaDecoder.GetVideoSourceHeight();

			fwuiScaling::CalculateDimensions( scaleMode, renderWidth, renderHeight, 
				c_sourceWidth, c_sourceHeight, rtWidth, rtHeight );
		}

		bool const c_centerImage = fwuiScaling::ShouldCenterImageForScaleMode( scaleMode );

		float const c_x1 = c_centerImage ? (rtWidth - renderWidth ) / 2.f : 0.f;
		float const c_y1 = c_centerImage ? (rtHeight - renderHeight ) / 2.f : 0.f;

		float const c_x2 = c_x1 + renderWidth;
		float const c_y2 = c_y1 + renderHeight;

		if( !c_readyToRender || renderWidth < rtWidth || renderHeight < rtHeight || !IsSuitableForRendering() )
		{
			grcBindTexture( NULL );

			// Fill background as black, since we are rendering smaller than the frame size,
			// or we don't yet have a frame to render
			grcDrawSingleQuadf( 0.f, 0.f, (float)rtWidth, (float)rtHeight, 0.f, 
				0.f, 0.f, 1.f, 1.f, 
				Color32( 0, 0, 0, 255 ) );
		}

		if( c_readyToRender )
		{
			RenderVideoOverlay( c_x1, c_y1, c_x2, c_y2 );
		}
	}

	size_t VideoPlaybackInstance::GetFileSize() const
	{
		size_t const c_fileSize = m_mediaDecoder.HasFileInfo() ? m_mediaDecoder.GetFileSize() : 0;
		return c_fileSize;
	}

	u64 VideoPlaybackInstance::GetDurationMs() const
	{
		u64 const c_duration = m_mediaDecoder.HasFileInfo() ? m_mediaDecoder.GetDurationMs() : 0;
		return c_duration;
	}

	u64 VideoPlaybackInstance::GetCurrentTimeMs() const
	{
		u64 const c_currentTime = m_mediaDecoder.GetCurrentTimeMs();
		return c_currentTime;
	}

	bool VideoPlaybackInstance::Seek( u64 const timeMs )
	{
		bool const success = m_mediaDecoder.Seek( MILLISECONDS_TO_NANOSECONDS( timeMs ) );
		return success;
	}

	bool VideoPlaybackInstance::Play()
	{
		bool const success = m_mediaDecoder.Play();
		return success;
	}

	bool VideoPlaybackInstance::IsPaused() const
	{
		bool const success = ( m_mediaDecoder.IsPaused() && !m_mediaDecoder.IsPendingPlay() ) || 
										m_mediaDecoder.IsAtEnd() || m_mediaDecoder.IsPendingPause();
		return success;
	}

    bool VideoPlaybackInstance::IsAtStartOfFile() const
    {
        bool const c_sof = m_mediaDecoder.IsAtStart();
        return c_sof;
    }

	bool VideoPlaybackInstance::IsAtEndOfFile() const
	{
		bool const c_eof = m_mediaDecoder.IsAtEnd();
		return c_eof;
	}

	bool VideoPlaybackInstance::Pause()
	{
		bool const success = m_mediaDecoder.Pause();
		return success;
	}

	bool VideoPlaybackInstance::IsPlaybackActive() const
	{
		return (m_mediaDecoder.GetDecoderState() == MediaDecoder::DECODER_ACTIVE);
	}

	void VideoPlaybackInstance::Shutdown()
	{
		m_state = PLAYBACK_CLEANUP;
		m_mediaDecoder.Shutdown();
		DestroyTextures();
		DestroySound();
		m_state = PLAYBACK_INVALID;
		m_textureIdentifier = -1;

		ShutdownShaders();
	}

	void VideoPlaybackInstance::RenderVideoOverlay( float const x, float const y, float const x2, float const y2 )
	{
		if( IsSuitableForRendering() )
		{
			PF_PUSH_MARKER("VideoPlayback");

#if RSG_PC
			if(m_Reset)
			{
				((grcTextureDX11*)m_yPlane)->UpdateGPUCopyFromBackingStore();
				((grcTextureDX11*)m_uPlane)->UpdateGPUCopyFromBackingStore();
				((grcTextureDX11*)m_vPlane)->UpdateGPUCopyFromBackingStore();
				
				m_Reset = false;
			}
#endif

			if( m_pEffect )
			{
				m_pEffect->SetVar( m_nYPlaneId, m_yPlane );
				m_pEffect->SetVar( m_ncRPlaneId, m_vPlane );
				m_pEffect->SetVar( m_ncBPlaneId, m_uPlane );
				m_pEffect->SetVar( m_nUVScalarId, GetUVScalar() );

				if( m_pEffect->BeginDraw( m_tOpaque, true) ) 
				{
					m_pEffect->BeginPass(0);
				}
			}

			grcDrawSingleQuadf( x, y, x2, y2, 
				0.f, 0.f, 0.f, 1.f, 1.f, Color32(255,255,255,255) );

			if( m_pEffect )
			{
				m_pEffect->EndPass();
				m_pEffect->EndDraw();

				m_pEffect->SetVar( m_nYPlaneId, (grcTexture*)NULL );
				m_pEffect->SetVar( m_ncRPlaneId, (grcTexture*)NULL );
				m_pEffect->SetVar( m_ncBPlaneId, (grcTexture*)NULL );
			}

			PF_POP_MARKER();
		}
	}

#if __BANK

	void VideoPlaybackInstance::RenderVideoOverlayBank()
	{
		PF_PUSH_MARKER("VideoPlaybackDebug");

		float const c_width = m_mediaDecoder.GetVideoSourceWidth() / 2.f;
		float const c_height = m_mediaDecoder.GetVideoSourceHeight() / 2.f;

		PUSH_DEFAULT_SCREEN();

		RenderVideoOverlay( 0.f, 0.f, c_width, c_height );

		POP_DEFAULT_SCREEN();

		PF_POP_MARKER();
	}

#endif // __BANK

	bool VideoPlaybackInstance::HasCreatedTextures() const
	{
		return m_yPlane != NULL && m_uPlane != NULL && m_vPlane != NULL;
	}

	bool VideoPlaybackInstance::CreateTextures()
	{
		bool success = false;

		if( videoVerifyf( !HasCreatedTextures(), "VideoPlaybackInstance::CreateTextures - Double texture initialization" ) &&
			videoVerifyf( m_mediaDecoder.HasFileInfo(), 
			"VideoPlaybackInstance::CreateTextures - Media Decoder not ready for texture creation" ) )
		{
			grcTextureFactory::TextureCreateParams oParams( grcTextureFactory::TextureCreateParams::SYSTEM,
				grcTextureFactory::TextureCreateParams::LINEAR, grcsDiscard | grcsWrite );	

			u32 const c_videoWidth = m_mediaDecoder.GetVideoSourceWidth();
			u32 const c_videoHeight = m_mediaDecoder.GetVideoSourceHeight();

			m_textureWidth = c_videoWidth;//rage::GetNextPow2( c_videoWidth - 1 );
			m_textureHeight = c_videoHeight;//rage::GetNextPow2( c_videoHeight - 1 );

			u32 const c_videoHalfTexWidth = m_textureWidth / 2;
			u32 const c_videoHalfTexHeight = m_textureHeight / 2;

			ASSERT_ONLY(RESOURCE_CACHE_ONLY(grcResourceCache::GetInstance().SetSafeResourceCreate(true)));

			// Y and Alpha planes use full size
			m_yPlane = grcTextureFactory::GetInstance().Create( 
				m_textureWidth, m_textureHeight, grctfL8, NULL, 1, &oParams );
		//	videoVerifyf( m_yPlane, "VideoPlaybackInstance::CreateTextures - Unable to create Y Plane" );

			//! But U and V planes use half-size
			m_uPlane = grcTextureFactory::GetInstance().Create( 
				c_videoHalfTexWidth, c_videoHalfTexHeight, grctfL8, NULL, 1, &oParams );
		//	videoVerifyf( m_uPlane, "VideoPlaybackInstance::CreateTextures - Unable to create U Plane" );

			m_vPlane = grcTextureFactory::GetInstance().Create( 
				c_videoHalfTexWidth, c_videoHalfTexHeight, grctfL8, NULL, 1, &oParams );
		//	videoVerifyf( m_vPlane, "VideoPlaybackInstance::CreateTextures - Unable to create V Plane" );

			ASSERT_ONLY(RESOURCE_CACHE_ONLY(grcResourceCache::GetInstance().SetSafeResourceCreate(false)));

		//	success = HasCreatedTextures();
			if( videoVerifyf( m_yPlane, "VideoPlaybackInstance::CreateTextures - Unable to create Y Plane" ) &&
				videoVerifyf( m_uPlane, "VideoPlaybackInstance::CreateTextures - Unable to create U Plane" ) &&
				videoVerifyf( m_vPlane, "VideoPlaybackInstance::CreateTextures - Unable to create V Plane" )
				)
			{
				//! Default fill as black
				success = true;

				MediaCommon::FillTexture( m_yPlane, 16 );
				MediaCommon::FillTexture( m_uPlane, 128 );
				MediaCommon::FillTexture( m_vPlane, 128 );

				m_Reset = true;
			}

			if( !success )
			{
				DestroyTextures();
			}
		}

		return success;
	}

	void VideoPlaybackInstance::DestroyTextures()
	{
		if( m_yPlane )
		{
			m_yPlane->Release();
			m_yPlane = NULL;
		}

		if( m_uPlane )
		{
			m_uPlane->Release();
			m_uPlane = NULL;
		}

		if( m_vPlane )
		{
			m_vPlane->Release();
			m_vPlane = NULL;
		}
	}

	bool VideoPlaybackInstance::CreateSound()
	{
#if RSG_ORBIS
		return true;
#else
		bool success = m_noAudio;

		if( !m_noAudio && videoVerifyf( ms_audioEntity, "VideoPlaybackInstance::CreateSound - No audio entity set!" ) &&
			videoVerifyf( !HasCreatedSound(), "VideoPlaybackInstance::CreateSound - Double sound initialization" ) &&
			videoVerifyf( m_mediaDecoder.HasFileInfo(), 
			"VideoPlaybackInstance::CreateSound - Media Decoder not ready for audio creation" ) )
		{	
			audSoundInitParams initParams;

			Assign(initParams.BucketId, ms_audioBucketId );
			initParams.TimerId = MEDIA_DECODER_AUDIO_TIMER_ID;
			initParams.Category = g_AudioEngine.GetCategoryManager().GetCategoryPtr(ATSTRINGHASH("FRONTEND_MENU", 0xBF516EEE));

			ms_audioEntity->CreateSound_PersistentReference( VIDEO_PLAYBACK_SOUND_NAME, &m_sound, &initParams );
			if( videoVerifyf( m_sound, "VideoPlaybackInstance::CreateSound - reateSound_PersistentReference - Failed" ) )
			{
				videoAssertf( m_sound->GetSoundTypeID() == ExternalStreamSound::TYPE_ID, "VideoPlaybackInstance::CreateSound - "
					"unexpected sound type %u created!", m_sound->GetSoundTypeID() );

				audExternalStreamSound *strSound = reinterpret_cast<audExternalStreamSound*>(m_sound);	

				success = strSound->InitStreamPlayer( m_mediaDecoder.GetAudioBuffer(), m_mediaDecoder.GetAudioChannelCount(), 
					m_mediaDecoder.GetAudioSampleRate() );

				if( success )
				{
					strSound->SetGain(20.f);
					strSound->Prepare();
				}
			}
		}

		return success;
#endif
	}

	void VideoPlaybackInstance::DestroySound()
	{
		if( m_sound )
		{
			if( m_sound->GetSoundTypeID() == ExternalStreamSound::TYPE_ID )
			{
				audExternalStreamSound *strSound = reinterpret_cast<audExternalStreamSound*>(m_sound);	
				strSound->StopStreamPlayback();
			}

			m_sound->StopAndForget();
			m_sound = NULL;
		}
	}

	rage::Vec4V_Out VideoPlaybackInstance::GetUVScalar()
	{
		float const c_videoSourceWidth = (float)m_mediaDecoder.GetVideoSourceWidth();
		float const c_videoSourceHeight = (float)m_mediaDecoder.GetVideoSourceHeight();

		const ScalarV movieWidth( c_videoSourceWidth );
		const ScalarV movieHeight( c_videoSourceHeight );
		const ScalarV YABufferWidth( movieWidth );
		const ScalarV YABufferHeight( movieHeight );
		const ScalarV cRcBBufferWidth( c_videoSourceWidth / 2.f );
		const ScalarV cRcBBufferHeight( c_videoSourceHeight / 2.f );

		const Vec4V movieSize( movieWidth, movieHeight, movieWidth * ScalarV(V_HALF), movieHeight * ScalarV(V_HALF) );
		const Vec4V bufferSize( YABufferWidth, YABufferHeight, cRcBBufferWidth, cRcBBufferHeight );
		const Vec4V UVScalar = movieSize/bufferSize;

		return UVScalar;
	}

} // namespace rage

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
