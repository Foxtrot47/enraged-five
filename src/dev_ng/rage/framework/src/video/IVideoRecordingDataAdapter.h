/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : IVideoRecordingDataProvider.h
// PURPOSE : Interface for providing data to/from the video recording interface
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef IVIDEO_RECORDING_DATA_PROVIDER_H_
#define IVIDEO_RECORDING_DATA_PROVIDER_H_

// rage
#include "atl/hashstring.h"

// framework
#include "video/mediaencoder_params.h"

namespace rage
{

	class IVideoRecordingDataProvider
	{
	public:
		virtual void GetGameFramebufferDimensions( u32& out_width, u32& out_height ) const = 0;
		virtual void GetVideoOutputDimensions( u32& out_width, u32& out_height ) const = 0;

		virtual MediaEncoderParams::eQualityLevel GetRecordingQualityLevel() const = 0;
		virtual MediaEncoderParams::eOutputFps GetRecordingFps() const = 0;
		virtual float GetExportFrameDurationMs() const = 0;
		virtual MediaCommon::eVideoFormat GetFormatRequested() const = 0;

		virtual bool IsRecordingVideo() const = 0;
		virtual bool IsRecordingPaused() const = 0;

		virtual bool IsRecordingReadyForSamples() const = 0;
		virtual bool IsAudioFrameCaptureAllowed() const = 0;
		virtual bool HasRecordingErrored() const = 0;

		virtual u32 GetEstimatedDurationMs() const = 0;
		virtual const wchar_t* GetGameTitle() const = 0;
		virtual const wchar_t* GetCopyright() const = 0;
	};

}

#endif // IVIDEO_RECORDING_DATA_PROVIDER_H_
