/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_bufferpool.cpp
// PURPOSE : Class for accessing a pool of available buffers
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_bufferpool.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "system/memops.h"

// framework
#include "media_common.h"


namespace rage
{
	bool MediaEncoderBufferPool::Initialize( MediaEncoderParams const& captureParameters )
	{
		Shutdown();

		InitializeVideoBuffers( captureParameters );
		InitializeAudioBuffers( MediaCommon::sc_encoderAudioMaxBufferSize );
		m_initialized = true;

		return IsInitialized();
	}

	bool MediaEncoderBufferPool::IsInitialized() const
	{
		return m_initialized;
	}

	void MediaEncoderBufferPool::Shutdown()
	{
		InvalidateLastReturnedAudioBuffer();
		ReleaseBuffersInternal();
		m_initialized = false;
	}

	bool MediaEncoderBufferPool::IsVideoQueueFull() const
	{
		bool full = true;

		for( u32 index = 0; index < sc_videoBufferCount; ++index )
		{
			MediaBufferVideoEncode const& currentBuffer = m_videoBuffers[ index ];
			if( !m_videoBufferUsedFlags.IsSet( index ) &&
				currentBuffer.IsInitialized() )
			{
				full = false;
				break;
			}
		}

		return full;
	}

	MediaBufferVideoEncode* MediaEncoderBufferPool::TryGetVideoBuffer()
	{
		MediaBufferVideoEncode* result = NULL;

		for( u32 index = 0; index < sc_videoBufferCount; ++index )
		{
			MediaBufferVideoEncode& currentBuffer = m_videoBuffers[ index ];
			if( !m_videoBufferUsedFlags.IsSet( index ) &&
				currentBuffer.IsInitialized() )
			{
				m_videoBufferUsedFlags.Set( index );
				result = &currentBuffer;
				break;
			}
		}

		return result;
	}

	void MediaEncoderBufferPool::ReleaseVideoBuffer( MediaBufferVideoEncode*& buffer )
	{
		for( u32 index = 0;  buffer && index < sc_videoBufferCount; ++index )
		{
			MediaBufferVideoEncode& currentBuffer = m_videoBuffers[ index ];
			if( &currentBuffer == buffer )
			{
				buffer = NULL;
				currentBuffer.Reinitialize();
				m_videoBufferUsedFlags.Clear( index );
				break;
			}
		}
	}

	bool MediaEncoderBufferPool::IsAudioQueueFull() const
	{
		bool full = true;

		for( u32 index = 0; index < sc_audioBufferCount; ++index )
		{
			MediaBufferAudioEncode const& currentBuffer = m_audioBuffers[ index ];
			if( !m_audioBufferUsedFlags.IsSet( index ) &&
				currentBuffer.IsInitialized() )
			{
				full = false;
				break;
			}
		}

		return full;
	}

	MediaBufferAudioEncode* MediaEncoderBufferPool::TryGetAudioBuffer( size_t const sizeNeeded )
	{
		MediaBufferAudioEncode* result = NULL;

		for( u32 index = 0; index < sc_audioBufferCount; ++index )
		{
			MediaBufferAudioEncode& currentBuffer = m_audioBuffers[ index ];
			if( !m_audioBufferUsedFlags.IsSet( index ) &&
				currentBuffer.IsInitialized() && 
				Verifyf( currentBuffer.GetSize() >= sizeNeeded, "MediaEncoderBufferPool::TryGetAudioBuffer - Needed size:%u" SIZETFMT 
				"but got size %u" SIZETFMT, static_cast<u32>(sizeNeeded), static_cast<u32>(currentBuffer.GetSize()) ) )
			{
				m_audioBufferUsedFlags.Set( index );
				m_lastReturnedAudioBuffer = index;
				result = &currentBuffer;
				break;
			}
		}

		return result;
	}

	MediaBufferAudioEncode* MediaEncoderBufferPool::TryGetLastReturnedAudioBuffer( size_t const sizeNeeded )
	{
		MediaBufferAudioEncode* result = NULL;

		if( m_lastReturnedAudioBuffer >= 0 && m_lastReturnedAudioBuffer < (s32)sc_audioBufferCount &&
			m_audioBufferUsedFlags.IsSet( m_lastReturnedAudioBuffer ) )
		{
			MediaBufferAudioEncode& buffer = m_audioBuffers[m_lastReturnedAudioBuffer];
			result = buffer.GetSizeRemaining() >= sizeNeeded ? &buffer : NULL;
		}

		return result;
	}

	void MediaEncoderBufferPool::ReleaseAudioBuffer( MediaBufferAudioEncode*& buffer )
	{
		for( u32 index = 0;  buffer && index < sc_audioBufferCount; ++index )
		{
			MediaBufferAudioEncode& currentBuffer = m_audioBuffers[ index ];
			if( &currentBuffer == buffer )
			{
				buffer = NULL;
				currentBuffer.Reinitialize();
				m_audioBufferUsedFlags.Clear( index );
				break;
			}
		}
	}

	void MediaEncoderBufferPool::ReleaseUnknownBufferType( MediaBufferBase*& buffer )
	{
		if( buffer )
		{
			if( buffer->GetType() == MediaBufferBase::BUFFER_VIDEO )
			{
				ReleaseVideoBuffer( (MediaBufferVideoEncode*&)buffer );
			}
			else if( buffer->GetType() == MediaBufferBase::BUFFER_AUDIO )
			{
				ReleaseAudioBuffer( (MediaBufferAudioEncode*&)buffer );
			}
			else
			{
				Assertf( false, "MediaEncoderBufferPool::ReleaseUnknownBufferType - Attempting to release " 
					"Unknown buffer type %u", buffer->GetType() );
			}

			buffer = NULL;
		}
	}

	void MediaEncoderBufferPool::InitializeVideoBuffers( MediaEncoderParams const& captureParameters )
	{
		for( u32 index = 0; index < sc_videoBufferCount; ++index )
		{
			MediaBufferVideoEncode& currentBuffer = m_videoBuffers[ index ];
			if( Verifyf( !currentBuffer.IsInitialized(), "MediaEncoderBufferPool::InitializeVideoBuffers - Double initialization!" ) )
			{
#if defined(VIDEO_ENCODE_BUFFERS_AS_BLOBS) && VIDEO_ENCODE_BUFFERS_AS_BLOBS

#if RSG_PC || RSG_ORBIS
				currentBuffer.InitializeWithSize( captureParameters.GetOutputWidth(), captureParameters.GetOutputHeight() );
#else
				currentBuffer.Initialize( captureParameters.GetOutputBufferSizeBytes(), MediaBufferBase::BUFFER_VIDEO );
#endif

#elif defined(VIDEO_ENCODE_BUFFERS_AS_TEXTURES) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES
				currentBuffer.Initialize();
				(void)captureParameters;
#endif
				m_videoBufferUsedFlags.Clear(index);
			}
		}
	}

	void MediaEncoderBufferPool::InitializeAudioBuffers( size_t const sizeNeededPerFrame )
	{
		for( u32 index = 0; index < sc_audioBufferCount; ++index )
		{
			MediaBufferAudioEncode& currentBuffer = m_audioBuffers[ index ];
			if( Verifyf( !currentBuffer.IsInitialized(), "MediaEncoderBufferPool::InitializeAudioBuffers - Double initialization!" ) )
			{
				currentBuffer.Initialize( sizeNeededPerFrame, MediaBufferBase::BUFFER_AUDIO );
				m_audioBufferUsedFlags.Clear(index);
			}
		}
	}

	void MediaEncoderBufferPool::ReleaseBuffersInternal()
	{
		for( u32 index = 0; index < sc_videoBufferCount; ++index )
		{
			m_videoBuffers[ index ].Shutdown();
			m_videoBufferUsedFlags.Clear(index);
		}

		for( u32 index = 0; index < sc_audioBufferCount; ++index )
		{
			m_audioBuffers[ index ].Shutdown();
			m_audioBufferUsedFlags.Clear(index);
		}
	}

} // namespace rage

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER
