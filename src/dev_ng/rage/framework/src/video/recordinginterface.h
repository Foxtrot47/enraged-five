#ifndef _RECORDINGINTERFACE_H_
#define _RECORDINGINTERFACE_H_

// rage
#include "atl/array.h"
#include "atl/string.h"

// framework
#include "IVideoRecordingDataAdapter.h"

#if __BANK
#define SAVE_VIDEO 1
#else
#define SAVE_VIDEO (RSG_DURANGO)
#endif

#if SAVE_VIDEO
#define SAVE_VIDEO_ONLY(...)  __VA_ARGS__
#else
#define SAVE_VIDEO_ONLY(...)
#endif

namespace rage
{

	// Covers both the Timespan record, and Start/Stop recording
	class RecordingTask
	{
	public:

		enum RECORD_STATE
		{
			RECORD_STATE_NOT_RECORDING = 0,
			RECORD_STATE_RECORDING_STARTED,				// Start/Stop functionality
			RECORD_STATE_RECORDING_STOPPED,				// Start/Stop functionality
			RECORD_STATE_SUCCESS,
			RECORD_STATE_CANCELLED
		};

		enum ERROR_STATE
		{
			ERROR_STATE_NONE,

			ERROR_STATE_OOM,
			ERROR_STATE_OUT_OF_SPACE,
			ERROR_STATE_ACCESS_DENIED,
			ERROR_STATE_SHARING_VIOLATION,

			ERROR_STATE_UNKNOWN,						// Catch-all, but try to pick something better :)
		};

		enum eWorkerAction
		{
			ACTION_INVALID = -1,

			ACTION_START_RECORDING,
			ACTION_PAUSE_RECORDING,
			ACTION_RESUME_RECORDING,
			ACTION_STOP_RECORDING,

			ACTION_MAX
		};

		RecordingTask() 
			: m_RecordState( RECORD_STATE_NOT_RECORDING)
			, m_NextCommand( ACTION_INVALID )
			, m_errorState( ERROR_STATE_NONE )
			, m_RefCount( 0 )
			, m_startPaused( false )
			, m_synchronized( false )
			, m_hasModdedContent( false )
#if SAVE_VIDEO
			, m_bSaveToDisc( false )
#endif
		{
		}

		virtual ~RecordingTask() 
		{
		}

		virtual void	StartRecording( const char *pName, bool const startPaused, bool const synchronized, bool const hasModdedContent ) = 0;
		virtual void	PauseRecording() { return; }
		virtual void	ResumeRecording() { return; }
		virtual void	StopRecording() = 0;
		virtual void	CancelRecording() = 0;
		virtual void	Update() = 0;
		virtual bool	IsPaused()			const = 0;
		virtual bool	IsReadyForSamples() const = 0;
		virtual bool	IsAudioFrameCaptureAllowed() const { return true; };

		inline u32		GetStatus()			const	{ return m_RecordState; }
		inline bool		HasIssuedStart()	const	{ return GetStatus() >= RECORD_STATE_RECORDING_STARTED; }
		inline bool		IsComplete()		const	{ return (m_RefCount==0); }

		bool			HasError()			const	{ return HasErrorCode(); }
		bool			HasErrorCode()		const	{ return GetLastError() != ERROR_STATE_NONE; }
		ERROR_STATE		GetLastError()		const	{ return m_errorState; }
		void			ResetLastError()			{ m_errorState = ERROR_STATE_NONE; }


		bool			WasSuccessfull()	const	{ return GetStatus() == RECORD_STATE_SUCCESS; }

#if SAVE_VIDEO
		// Bank Functionality, Save to disc
		void SetSaveToDisc(const char *pFilenameAndPathWithExtention)
		{
			m_ClipSaveName = pFilenameAndPathWithExtention;
			m_bSaveToDisc = true;
		}
#endif

	protected:

		u32			m_RecordState;
		eWorkerAction m_NextCommand;
		ERROR_STATE	m_errorState;

		s32			m_RefCount;		// One for each thread that starts up off the initial capture
		void		IncRefCount()
		{
			m_RefCount++; 
		}

		void		DecRefCount()
		{
			Assertf(m_RefCount >= 1, "RecordingTask::DecRefCount() - trying to dec a refcount of zero!");
			m_RefCount--; 
		}


		inline char const * GetClipName() const { return m_ClipName.c_str();  }
		atString	m_ClipName;				// The name of the clip
		bool		m_startPaused;
		bool		m_synchronized;
		bool		m_hasModdedContent;

#if SAVE_VIDEO
		atString	m_ClipSaveName;
		bool		m_bSaveToDisc;
#endif

	};

	class VideoRecordingInterface
	{
	public:

		static VideoRecordingInterface *Create();

		VideoRecordingInterface()
		{
		}

		virtual ~VideoRecordingInterface() 
		{
		}

		virtual	void	EnableRecording() = 0;
		virtual	void	DisableRecording() = 0;

		virtual RecordingTask *CreateRecordingTask() = 0;
		virtual void	DeleteRecordingTask(RecordingTask *pTask) = 0;

	protected:
	private:

	};

}

#endif // _RECORDINGINTERFACE_H_