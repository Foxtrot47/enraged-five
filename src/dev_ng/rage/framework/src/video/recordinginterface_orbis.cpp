#if RSG_ORBIS

#include <scebase_common.h>
#include <_kernel.h>
#include <_fs.h>
#include <libsysmodule.h>
#include <sdk_version.h>
#include <ces.h>

#pragma comment(lib,"libSceVideoRecording_stub_weak.a")
#pragma comment(lib,"libSceCes.a")

#include "file/asset.h"
#include "string/unicode.h"
#include "string/stringhash.h"
#include "math/amath.h"
#include "system/ipc.h"

#include "audiohardware/driver.h"

#include "contentExport_Orbis.h"
#include "recordinginterface_orbis.h"

#include "video/video_channel.h"

// DON'T COMMIT
//OPTIMISATIONS_OFF();		// Doesn't work on Orbis

namespace rage
{

int ConvertUTF8ToUTF16be(const char* source, uint16_t* dest, int maxStringLength)
{
	const uint8_t* utf8addr;
	utf8addr = reinterpret_cast<const uint8_t*>(source);

	uint32_t stringIndex = 0;
	uint16_t* stringPtr = dest;
	uint32_t len8 = 0;
	uint32_t len16 = 0;
	uint16_t ucode = 0;
	for (stringIndex = 0; stringIndex < maxStringLength; stringIndex+=len16)
	{
		sceCesUtf8ToUtf16be( utf8addr, 4, &len8, &ucode, 8, &len16);

		*stringPtr = ucode; 
		stringPtr+=len16;

		utf8addr += len8;

		if (utf8addr[0] == '\0')
			break;

	}

	return stringIndex;
}

bool VideoRecordingSetInfo(int32_t infoType, const char* stringContent)
{
	int ret = -1;

	const int maxStringLength = 255;
	uint16_t stringToSet[maxStringLength];
	int stringLength = ConvertUTF8ToUTF16be(stringContent, stringToSet, maxStringLength);

	if (stringLength > 0)
	{
		ret = sceVideoRecordingSetInfo(infoType,
			stringToSet, sizeof(uint16_t)*(stringLength+1));
	}
	
	return (ret >= SCE_OK);
}

//----------------------------------------------------------------------
//	VideoRecordingInterface
//----------------------------------------------------------------------
VideoRecordingInterface *VideoRecordingInterface::Create()
{
	return rage_new VideoRecordingInterfaceOrbis;
}

//----------------------------------------------------------------------
//	RecordingTaskOrbis
//----------------------------------------------------------------------

RecordingTaskOrbis::RecordingTaskOrbis() : RecordingTask()
#if	USES_MEDIA_ENCODER
	, m_encoder()
#endif
{
	m_Cancelled = false;
	m_isPaused = false;
	m_isInInitialPause = false;
}

RecordingTaskOrbis::~RecordingTaskOrbis()
{
	// Clean up anything

}

void RecordingTaskOrbis::Reset()
{
	m_RecordState = RECORD_STATE_NOT_RECORDING;
	m_NextCommand = ACTION_INVALID;
	m_errorState = ERROR_STATE_NONE;
	m_RefCount = 0;
	m_startPaused = false;
	m_synchronized = false;
	m_hasModdedContent = false;
#if SAVE_VIDEO
	m_bSaveToDisc = false;
#endif
	m_Cancelled = false;
	m_isPaused = false;
	m_isInInitialPause = false;
//	m_encoder ...should be fine being reused. 
}

void	RecordingTaskOrbis::StartRecording( const char *pName, bool const startPaused, bool const synchronized, bool const hasModdedContent )
{
	m_ClipName = pName;
	m_isInInitialPause = m_isPaused = startPaused;
	m_synchronized = synchronized;
	m_hasModdedContent = hasModdedContent;
	m_hasThumbnail = false;
	m_thumbnailCheckDelay = 0;
	m_isThumbnailReady = false;
	m_errorState = ERROR_STATE_NONE;

	// We're starting, add a ref so we can't be cleaned up
	IncRefCount();

#if USES_MEDIA_ENCODER
	RecordingTaskOrbis::ProhibitShareRecording(true);
#endif

	m_RecordState = RECORD_STATE_RECORDING_STARTED;

#if REC_TASK_DELAY_TO_RENDER_SYNC
	m_NextCommand = ACTION_START_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	sWorkerData newData = {this, ACTION_START_RECORDING};
	sm_WorkerCommandQueue.Push(newData);
#else
	ProcessAction( ACTION_START_RECORDING );
#endif

#endif

}

void	RecordingTaskOrbis::StopRecording()
{
	if( videoVerifyf(m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to Stop a video that wasn't started" ) )
	{
#if REC_TASK_DELAY_TO_RENDER_SYNC
		m_NextCommand = ACTION_STOP_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		sWorkerData newData = {this, ACTION_STOP_RECORDING};
		sm_WorkerCommandQueue.Push(newData);
#else
		ProcessAction( ACTION_STOP_RECORDING );
#endif

#endif
	}


}

void	RecordingTaskOrbis::CancelRecording()
{
	if( videoVerifyf(m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to Cancel a video that wasn't started" ) )
	{
		m_Cancelled = true;
		StopRecording();
	}
}

void	RecordingTaskOrbis::PauseRecording()
{
	if( videoVerifyf(!m_isPaused, "Trying to Resume a video that was already paused") && Verifyf(m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to Pause a video that wasn't started" ) )
	{
	//	m_isPaused = true;

#if REC_TASK_DELAY_TO_RENDER_SYNC
		m_NextCommand = ACTION_PAUSE_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		sWorkerData newData = {this, ACTION_PAUSE_RECORDING};
		sm_WorkerCommandQueue.Push(newData);
#else
		ProcessAction( ACTION_PAUSE_RECORDING );
#endif

#endif
	}
}

void	RecordingTaskOrbis::ResumeRecording()
{
	if( videoVerifyf(m_isPaused, "Trying to Resume a video that wasn't paused") && Verifyf(m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to Resume a video that wasn't started" ) )
	{
	//	m_isPaused = false;

#if REC_TASK_DELAY_TO_RENDER_SYNC
		m_NextCommand = ACTION_RESUME_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		sWorkerData newData = {this, ACTION_RESUME_RECORDING};
		sm_WorkerCommandQueue.Push(newData);
#else
		ProcessAction( ACTION_RESUME_RECORDING );
#endif

#endif
	}
}

void	RecordingTaskOrbis::Update()
{
#if REC_TASK_DELAY_TO_RENDER_SYNC
	if (m_NextCommand != ACTION_INVALID)
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD	
	{
		sWorkerData newData = {this, m_NextCommand };
		sm_WorkerCommandQueue.Push(newData);
	}
#else
	{
		ProcessAction( m_NextCommand );
	}
#endif
	m_NextCommand = ACTION_INVALID;
#endif

	if( !m_isInInitialPause )
	{
		if (m_hasThumbnail && m_thumbnailCheckDelay < eValues::ThumbnailCheckFrameDelay)
		{
			++m_thumbnailCheckDelay;
		}

		if( m_RecordState == RECORD_STATE_RECORDING_STARTED)
		{
			int ret = sceVideoRecordingGetStatus();
			if (ret < SCE_OK) {
				videoDisplayf("Error: sceVideoRecordingGetStatus(), ret 0x%08x\n", ret);
				m_errorState = (ret == SCE_VIDEO_RECORDING_ERROR_NO_SPACE) ? ERROR_STATE_OUT_OF_SPACE : ERROR_STATE_UNKNOWN;
			}
		}
	}

	UpdateErrorCode();
}

bool RecordingTaskOrbis::IsPaused() const
{
// #if	USES_MEDIA_ENCODER
// 	return m_encoder.IsPaused();
// #else
	return m_isPaused;
//#endif
}

bool RecordingTaskOrbis::IsReadyForSamples() const
{
#if USES_MEDIA_ENCODER
	return m_encoder.IsReadyToProcessSamples();
#else
	return true;
#endif
}

bool RecordingTaskOrbis::IsAudioFrameCaptureAllowed() const
{
#if USES_MEDIA_ENCODER
	return m_encoder.IsAudioFrameCaptureAllowed();
#else
	return true;
#endif
}

#if	USES_MEDIA_ENCODER
void RecordingTaskOrbis::PushVideoFrame( grcRenderTarget const& source, float const frameDuration )
{
	m_encoder.PushVideoFrame( (grcTexture&)source, frameDuration );
}

void RecordingTaskOrbis::UpdateErrorCode()
{
	MediaEncoder& encoder( GetEncoder() );

	if( encoder.HasError() )
	{
		switch( encoder.GetLastError() )
		{
		case MediaCommon::MEDIA_ERROR_OOM:
			{
				m_errorState = ERROR_STATE_OOM;
				break;
			}
		case MediaCommon::MEDIA_ERROR_ACCESS_DENIED:
			{
				m_errorState = ERROR_STATE_ACCESS_DENIED;
				break;
			}
		case MediaCommon::MEDIA_ERROR_SHARING_VIOLATION:
			{
				m_errorState = ERROR_STATE_SHARING_VIOLATION;
				break;
			}
		case MediaCommon::MEDIA_ERROR_UNKNOWN:
		default:
			{
				m_errorState = ERROR_STATE_UNKNOWN;
				break;
			}
		}
	}
}
#endif

void RecordingTaskOrbis::AddMetadata(const char* gameName, const char* gameNameShort)
{
	m_gameName = gameName;
	m_gameNameShort = gameNameShort;
}

void	RecordingTaskOrbis::PrepareForExport(const char* pThumbnail)
{
	if (pThumbnail)
	{
		m_ThumbnailPath = pThumbnail;
		m_hasThumbnail = true;
		m_isThumbnailReady = false;
		m_thumbnailCheckDelay = 0;
	}

}

IVideoRecordingDataProvider* RecordingTaskOrbis::sm_recordingDataProvider = NULL;

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD	
sysIpcThreadId	RecordingTaskOrbis::sm_WorkerThread = NULL;
sysMessageQueue<RecordingTaskOrbis::sWorkerData, 8, true> RecordingTaskOrbis::sm_WorkerCommandQueue;

void	RecordingTaskOrbis::InitWorkerThread()
{
	if( sm_WorkerThread == NULL )
	{
		sm_WorkerThread = sysIpcCreateThread(&RecorderWorkerThread, NULL, 64 << 10, PRIO_BELOW_NORMAL, "RecorderWorkerThread");

	}
}

void	RecordingTaskOrbis::ShutdownWorkerThread()
{
	RecordingTaskOrbis::sWorkerData newData = {NULL, ACTION_STOP_RECORDING};
	sm_WorkerCommandQueue.Push(newData);
	sysIpcWaitThreadExit(sm_WorkerThread);
}

void RecordingTaskOrbis::RecorderWorkerThread(void *UNUSED_PARAM(ptr))
{
	sWorkerData			workerData;
	RecordingTaskOrbis *pTask;

	Displayf("Starting RecordingTaskOrbis::RecorderWorkerThread()");

	while (1)
	{
		// Get the next
		workerData	= sm_WorkerCommandQueue.Pop();
		pTask = workerData.pTask;
		if (!pTask)
			break;

		pTask->ProcessAction( workerData.action );
	}
}

#endif

void RecordingTaskOrbis::ProhibitShareRecording(bool prohibit)
{
	uint32_t chapter = prohibit ? SCE_VIDEO_RECORDING_CHAPTER_PROHIBIT : SCE_VIDEO_RECORDING_CHAPTER_CHANGE;
	sceVideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_CHAPTER, &chapter, sizeof(chapter));
}

void RecordingTaskOrbis::ProcessAction( eWorkerAction const action )
{
#if	USES_MEDIA_ENCODER
		MediaEncoder& encoder = GetEncoder();

		switch( action )
		{
		case ACTION_START_RECORDING:
			{
				bool success = false;
				s32 ret = SCE_OK;
				videoAssertf( !encoder.IsCapturing(), "RecordingTaskOrbis::RecorderWorkerThread - Encoder already in use." );
				if( !encoder.IsCapturing() )
				{
					char	fileNameAndPath[256];
					sprintf(fileNameAndPath, "%s%s%s", SCE_VIDEO_RECORDING_PATH_GAME, m_ClipName.c_str(), ".mp4");

					videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_START_RECORDING");

					SceVideoRecordingParam param;
					sceVideoRecordingParamInit(&param);
					param.audioInput = SCE_VIDEO_RECORDING_AUDIO_INPUT_FLOAT;
					param.videoInput = SCE_VIDEO_RECORDING_VIDEO_INPUT_NV12;
#if HOUR_RECORDING
					param.ringSec = (60 * 60);
#endif
					s32 memSize = static_cast<s32>(sceVideoRecordingQueryMemSize(&param));
					videoAssertf(memSize <= VIDEO_RECORDING_MAX_MEM_SIZE, "RecordingTaskOrbis::RecorderWorkerThread - requesting too much memory %d", memSize);

					ret = sceVideoRecordingOpen(fileNameAndPath, &param, &m_RecordBuffer, memSize);
					if(ret == SCE_OK) 
					{
						videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): sceVideoRecordingStart called");
						ret = sceVideoRecordingStart();
						videoAssertf(ret == SCE_OK, "RecordingTaskOrbis::RecorderWorkerThread(): sceVideoRecordingStart return code: 0x%x", ret);
						if(ret == SCE_OK) 
						{
							videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): sceVideoRecordingStart success");
							MediaEncoderParams encodingParams;

							if( sm_recordingDataProvider )
							{
                                u32 gameWidth, gameHeight;
                                sm_recordingDataProvider->GetGameFramebufferDimensions( gameWidth, gameHeight );
                                encodingParams.SetOutputDimensions( gameWidth, gameHeight );
								encodingParams.SetOutputQuality( sm_recordingDataProvider->GetRecordingQualityLevel() );
								encodingParams.SetOutputFps( sm_recordingDataProvider->GetRecordingFps() );
								encodingParams.SetOutputFormat( sm_recordingDataProvider->GetFormatRequested() );
								encodingParams.SetEstimatedDurationMs( sm_recordingDataProvider->GetEstimatedDurationMs() );
								encodingParams.SetGameTitle( sm_recordingDataProvider->GetGameTitle() );
								encodingParams.SetCopyright( sm_recordingDataProvider->GetCopyright() );
							}

							success = videoVerifyf( encodingParams.IsValid(), "RecordingTaskOrbis::ProcessAction - Invalid encoder parameters specified" ) ? 
								encoder.StartCapture( m_ClipName.c_str(), fileNameAndPath, m_isInInitialPause, m_synchronized, encodingParams ) : false;

							videoDisplayf( success ? "Recording Started OK :)" : "Recording task got bad params :(" );
						}

						if( m_isPaused )
						{
							if( !encoder.IsPaused() )
							{
								encoder.PauseCapture();
							}
						}
						
					}
				}

				if( !success )
				{
					videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_START_RECORDING Failed. See above logs for more detail.");
					m_errorState = (ret == SCE_VIDEO_RECORDING_ERROR_NO_SPACE) ? ERROR_STATE_OUT_OF_SPACE : ERROR_STATE_UNKNOWN;
				}

				break;
			}

		case ACTION_PAUSE_RECORDING:
			{
				videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_PAUSE_RECORDING");

				m_isInInitialPause = false;

				m_isPaused = true;
				if( !encoder.IsPaused() )
				{
					videoDisplayf("Recording Stopped OK - On Pause");
					encoder.PauseCapture();
				}
				

				break;
			}

		case ACTION_RESUME_RECORDING:
			{
				videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_RESUME_RECORDING");

				m_isInInitialPause = false;
				
				m_isPaused = false;
				if( encoder.IsPaused() )
				{
					videoDisplayf("Recording Started OK - On Resume");
					encoder.ResumeCapture();
				}
				

				break;
			}

		case ACTION_STOP_RECORDING:
			{
				// set up metadata saying it's been recorded in the game's video editor
				const int maxStringLength = 255;
				char stringToSet[maxStringLength] = { 0 };

				m_isInInitialPause = false;

				if( encoder.IsCapturing() &&
					encoder.StopCapture( m_Cancelled ) )
				{
					UpdateErrorCode();
				}
				else
				{
					m_errorState = ERROR_STATE_UNKNOWN;
				}

				formatf(stringToSet, maxStringLength, "Created with the Rockstar Editor in %s", m_gameName.c_str());
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_DESCRIPTION, stringToSet);

				formatf(stringToSet, maxStringLength, "%s Exported Video", m_gameNameShort.c_str());
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_SUBTITLE, stringToSet);


				char	fileNameAndPath[256];
				sprintf(fileNameAndPath, "%s%s%s", SCE_VIDEO_RECORDING_PATH_GAME, m_ClipName.c_str(), ".mp4");

				videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_STOP_RECORDING");

				{
					int ret = sceVideoRecordingStop();
					if ( ret != SCE_OK ) 
					{
						Displayf("ERROR: sceVideoRecordingStop() - 0x%x", ret);
						m_errorState = (ret == SCE_VIDEO_RECORDING_ERROR_NO_SPACE) ? ERROR_STATE_OUT_OF_SPACE : ERROR_STATE_UNKNOWN;
					}
					else
					{
						m_RecordState = RECORD_STATE_RECORDING_STOPPED;
					}
				}

				int ret = sceVideoRecordingClose((int)m_Cancelled);
				if (ret == SCE_OK) 
				{
					m_RecordState = HasErrorCode() || m_Cancelled ?  RECORD_STATE_CANCELLED : RECORD_STATE_SUCCESS;
				}
				else
				{
					videoDisplayf("ERROR: sceVideoRecordingClose() - 0x%x", ret);
					m_errorState = (ret == SCE_VIDEO_RECORDING_ERROR_NO_SPACE) ? ERROR_STATE_OUT_OF_SPACE : ERROR_STATE_UNKNOWN;
				}

				if( m_RecordState == RECORD_STATE_SUCCESS )
				{
					videoDisplayf("Video Export: file to export %s", fileNameAndPath);
					if (m_hasThumbnail)
					{
						videoDisplayf("Video Export: Started Video Export with Thumbnail");
						videoDisplayf("Video Export: thumbnail to export %s", m_ThumbnailPath.c_str());

						// hold off exporting until we know the thumbnail is ready, or has failed by setting m_hasThumbnail to false
						while (!m_isThumbnailReady && m_hasThumbnail)
						{
							sysIpcSleep(10);
						}

						// since m_hasThumbnail can become false while in the loop, test again to make sure we have a thumbnail to export
						// if not, fall through to the usual test for export with no thumbnail
						if (m_hasThumbnail)
						{
							videoDisplayf("Video Export: Thumbnail ready");
							CContentExport::DoExportVideo(m_ClipName.c_str(), fileNameAndPath, m_ThumbnailPath.c_str());
							videoDisplayf("Video Export: Exported.");
						}
						else
						{
							videoDisplayf("Video Export: Thumbnail failed");
						}
					}

					if (!m_hasThumbnail)
					{
						videoDisplayf("Video Export: No thumbnail");
						CContentExport::DoExportVideo(m_ClipName.c_str(), fileNameAndPath);
						videoDisplayf("Video Export: Exported.");
					}
				}

				// reset metadata for sharing
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_DESCRIPTION, m_gameName.c_str());
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_SUBTITLE, m_gameNameShort.c_str());

				RecordingTaskOrbis::ProhibitShareRecording(false);
				DecRefCount();
			}

			break;

		default:
			{
				Assertf( false, "RecordingTaskOrbis::RecorderWorkerThread - Unsupported action %d requested", action );
				break;
			}
		}
#else
		switch(action)
		{
		case ACTION_START_RECORDING:
			{
				char	fileNameAndPath[256];
				sprintf(fileNameAndPath, "%s%s%s", SCE_VIDEO_RECORDING_PATH_GAME, m_ClipName.c_str(), ".mp4");

				videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_START_RECORDING");

				SceVideoRecordingParam param;
				sceVideoRecordingParamInit(&param);
#if HOUR_RECORDING
				param.ringSec = (60 * 60);
#endif
				s32 memSize = static_cast<s32>(sceVideoRecordingQueryMemSize(&param));
				videoAssertf(memSize <= VIDEO_RECORDING_MAX_MEM_SIZE, "RecordingTaskOrbis::RecorderWorkerThread - requesting too much memory %d", memSize);

				int ret = sceVideoRecordingOpen(fileNameAndPath, &param, m_RecordBuffer, memSize);

				if(ret == SCE_OK) 
				{
					if( m_isPaused )
					{
						audDriver::GetMixer()->PauseGroup(0, true);
					}
					else
					{
						ret = sceVideoRecordingStart();
						if(ret == SCE_OK) 
						{
							videoDisplayf("Recording Started OK :)");
						}
						else
						{
							// Display the error
							videoDisplayf("ERROR: sceVideoRecordingStart() - 0x%x", ret);
							m_errorState = (ret == SCE_VIDEO_RECORDING_ERROR_NO_SPACE) ? ERROR_STATE_OUT_OF_SPACE : ERROR_STATE_UNKNOWN;
							sceVideoRecordingClose(0);						// Close on start error (see VideoRecording Example)
							DecRefCount();
						}
					}

					/*ret = sceVideoRecordingStart();
					if(ret == SCE_OK) 
					{
						Displayf("Recording Started OK :)");

						pTask->m_isPaused = workerData.userData;
						if( pTask->m_isPaused )
						{
							audDriver::GetMixer()->PauseGroup(0, true);
							sceVideoRecordingStop();
							pTask->m_isPaused = true;
						}
					}
					else
					{
						// Display the error
						Displayf("ERROR: sceVideoRecordingStart() - 0x%x", ret);
						pTask->m_RecordState = RECORD_STATE_ERROR;
						pTask->m_errorState = ERROR_STATE_UNKNOWN;
						sceVideoRecordingClose(0);						// Close on start error (see VideoRecording Example)
						pTask->DecRefCount();
					}*/
				}
				else
				{
					// Display the error
					Displayf("ERROR: sceVideoRecordingOpen() - 0x%x", ret);
					m_errorState = ERROR_STATE_UNKNOWN;
					DecRefCount();
				}
			}
			break;

		case ACTION_STOP_RECORDING:
			{
				// set up metadata saying it's been recorded in the game's video editor
				const int maxStringLength = 255;
				char stringToSet[maxStringLength] = { 0 };

				m_isInInitialPause = false;
				
				formatf(stringToSet, maxStringLength, "Created with the Rockstar Editor in %s", m_gameName.c_str());
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_DESCRIPTION, stringToSet);

				formatf(stringToSet, maxStringLength, "%s Exported Video", m_gameNameShort.c_str());
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_SUBTITLE, stringToSet);

				
				char	fileNameAndPath[256];
				sprintf(fileNameAndPath, "%s%s%s", SCE_VIDEO_RECORDING_PATH_GAME, m_ClipName.c_str(), ".mp4");

				videoDisplayf("RecordingTaskOrbis::RecorderWorkerThread(): ACTION_STOP_RECORDING");

				if (!IsPaused())
				{
					int ret = sceVideoRecordingStop();
					if ( ret != SCE_OK ) 
					{
						videoDisplayf("ERROR: sceVideoRecordingStop() - 0x%x", ret);
						m_errorState = ERROR_STATE_UNKNOWN;
					}
					else
					{
						m_RecordState = RECORD_STATE_RECORDING_STOPPED;
					}
				}

				int ret = sceVideoRecordingClose((int)m_Cancelled);
				if (ret == SCE_OK) 
				{
					if( !HasError() )
					{
						if(m_Cancelled)
						{
							m_RecordState = RECORD_STATE_CANCELLED;
						}
						else
						{
							m_RecordState = RECORD_STATE_SUCCESS;
						}
					}
				}
				else
				{
					videoDisplayf("ERROR: sceVideoRecordingClose() - 0x%x", ret);
					m_errorState = ERROR_STATE_UNKNOWN;
				}

				// If it was canceled, then unlike Durango, the file wasn't created.
				if( !m_Cancelled )
				{

	#if SAVE_VIDEO
					if( m_bSaveToDisc == true && m_RecordState == RECORD_STATE_SUCCESS)
					{
						char	fileNameAndPath[256];
						sprintf(fileNameAndPath, "%s%s%s", SCE_VIDEO_RECORDING_PATH_GAME, m_ClipName.c_str(), ".mp4");

						// Save out to local PC
						fiStream* pSourceStream = ASSET.Open(fileNameAndPath,"");
						if( pSourceStream )
						{
							fiStream *pDestStream = ASSET.Create(m_ClipSaveName.c_str(),"");
							if( pDestStream )
							{
								#define COPY_BUFFER_SIZE	(2048)
								char	copyBuffer[COPY_BUFFER_SIZE];

								int sizeRemaining = pSourceStream->Size();
								while(sizeRemaining)
								{
									int sizeToCopy = rage::Min(sizeRemaining, COPY_BUFFER_SIZE);
									pSourceStream->Read(copyBuffer, sizeToCopy);
									pDestStream->Write(copyBuffer, sizeToCopy);
									sizeRemaining -= sizeToCopy;
								}
								pDestStream->Flush();
								pDestStream->Close();
								videoDisplayf("Recorded Video saved to %s", m_ClipSaveName.c_str());
							}
							pSourceStream->Close();
						}
					}
	#endif	//SAVE_VIDEO

				}

				if( m_RecordState == RECORD_STATE_SUCCESS )
				{
					videoDisplayf("Video Export: file to export %s", fileNameAndPath);
					if (m_hasThumbnail)
					{
						videoDisplayf("Video Export: Started Video Export with Thumbnail");
						videoDisplayf("Video Export: thumbnail to export %s", m_ThumbnailPath.c_str());
						
						// hold off exporting until we know the thumbnail is ready, or has failed by setting m_hasThumbnail to false
						while (!m_isThumbnailReady && m_hasThumbnail)
						{
							sysIpcSleep(10);
						}

						// since m_hasThumbnail can become false while in the loop, test again to make sure we have a thumbnail to export
						// if not, fall through to the usual test for export with no thumbnail
						if (m_hasThumbnail)
						{
							videoDisplayf("Video Export: Thumbnail ready");
							CContentExport::DoExportVideo(m_ClipName.c_str(), fileNameAndPath, m_ThumbnailPath.c_str());
							videoDisplayf("Video Export: Exported.");
						}
						else
						{
							videoDisplayf("Video Export: Thumbnail failed");
						}
					}
					
					if (!m_hasThumbnail)
					{
						videoDisplayf("Video Export: No thumbnail");
						CContentExport::DoExportVideo(m_ClipName.c_str(), fileNameAndPath);
						videoDisplayf("Video Export: Exported.");
					}
				}
				
				// reset metadata for sharing
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_DESCRIPTION, m_gameName.c_str());
				VideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_SUBTITLE, m_gameNameShort.c_str());

				DecRefCount();
			}

			break;
		case ACTION_PAUSE_RECORDING:
			{
				m_isInInitialPause = false;

				audDriver::GetMixer()->PauseGroup(0, true);
				int ret = sceVideoRecordingStop();

				if(ret == SCE_OK) 
				{
					videoDisplayf("Recording Stopped OK - On Pause");
				}
				else
				{
					videoDisplayf("ERROR: sceVideoRecordingStop() - On Pause - 0x%x", ret);
					m_errorState = ERROR_STATE_UNKNOWN;
				}
			}
			break;
		case ACTION_RESUME_RECORDING:
			{
				m_isInInitialPause = false;

				int ret = sceVideoRecordingStart();
				audDriver::GetMixer()->PauseGroup(0, false);

				if(ret == SCE_OK) 
				{
					videoDisplayf("Recording Started OK - On Resume");
				}
				else
				{
					// Display the error
					videoDisplayf("ERROR: sceVideoRecordingStart() - On Resume - 0x%x", ret);
					m_errorState = ERROR_STATE_UNKNOWN;
					sceVideoRecordingClose(0);						// Close on start error (see VideoRecording Example)
					DecRefCount();
				}
			}
			break;
		default:
			{
				videoAssertf( false, "RecordingTaskOrbis - Unsupported action %d requested", action );
				break;
			}
		}
#endif
}


//----------------------------------------------------------------------
//	VideoRecordingInterfaceOrbis
//----------------------------------------------------------------------
VideoRecordingInterfaceOrbis::VideoRecordingInterfaceOrbis() : VideoRecordingInterface()
{
	m_RefCount = 0;
	m_ModuleLoaded = false;
	if(Verifyf(sceSysmoduleLoadModule(SCE_SYSMODULE_VIDEO_RECORDING) == SCE_OK, "ERROR: Load of SCE_SYSMODULE_VIDEO_RECORDING module failed"))
	{
		m_ModuleLoaded = true;

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		// Start our RecordingTask Worker Thread
		RecordingTaskOrbis::InitWorkerThread();
#endif

		m_pTask = rage_new RecordingTaskOrbis();
	}
}

VideoRecordingInterfaceOrbis::~VideoRecordingInterfaceOrbis()
{
	if (m_pTask)
	{
		delete m_pTask;
	}

	// Stop the thread
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	RecordingTaskOrbis::ShutdownWorkerThread();
#endif

	sceSysmoduleUnloadModule(SCE_SYSMODULE_VIDEO_RECORDING);

	// Does the module actually need unloading?
	m_ModuleLoaded = false;
}

void VideoRecordingInterfaceOrbis::EnableRecording()
{
	// Perhaps "prohibit recording"? How to turn back on? Toggle?
// 	uint32_t chapter = SCE_VIDEO_RECORDING_CHAPTER_CHANGE;
// 	sceVideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_CHAPTER, &chapter, sizeof(chapter));

}

void VideoRecordingInterfaceOrbis::DisableRecording()
{
// 	uint32_t chapter = SCE_VIDEO_RECORDING_CHAPTER_PROHIBIT;
// 	sceVideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_CHAPTER, &chapter, sizeof(chapter));
}

}

#endif
