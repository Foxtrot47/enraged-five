#ifndef _GREATESTMOMENT_H_
#define _GREATESTMOMENT_H_

#define GREATEST_MOMENTS_ENABLED (RSG_DURANGO)

#if GREATEST_MOMENTS_ENABLED
#define GREATEST_MOMENTS_ONLY(...)	__VA_ARGS__
#else
#define GREATEST_MOMENTS_ONLY(...)
#endif // GREATEST_MOMENTS_ENABLED

// rage

#include "atl/array.h"
#include "atl/string.h"

namespace rage
{

	// Covers both the Timespan record, and Start/Stop recording
	class fwGreatestMomentTask
	{
	public:

		enum RECORD_STATE
		{
			RECORD_STATE_NOT_RECORDING = 0,
			RECORD_STATE_RECORDING_STARTED,				// Start/Stop functionality
			RECORD_STATE_RECORDING_STOPPED,				// Start/Stop functionality
			RECORD_STATE_RECORDING_TIMESPAN,			// Timespan functionality
			RECORD_STATE_SUCCESS,
			RECORD_STATE_CANCELLED,
			RECORD_STATE_ERROR,
			RECORD_STATE_ERROR_OUT_OF_SPACE
		};

		fwGreatestMomentTask() 
		{
			m_RecordState = RECORD_STATE_NOT_RECORDING;
		}

		virtual ~fwGreatestMomentTask() 
		{
		}

		virtual void	RecordTimespan(u32 userIndex, const char *id, const char* displayName, s32 startOffset, u32 duration) = 0;
		virtual void	StartRecording(u32 userIndex, const char *id) = 0;
		virtual void	PauseRecording() { return; }
		virtual void	ResumeRecording() { return; }
		virtual void	StopRecording() = 0;
		virtual void	CancelRecording() = 0;
		virtual void	Update() = 0;
		virtual bool	IsPaused() const { return false; }

		u32		GetStatus()			{ return m_RecordState; }
		bool	HasIssuedStart()	{ return GetStatus() == RECORD_STATE_RECORDING_STARTED; }
		bool	IsComplete()		{ return (WasSuccessfull() || IsError()); }
		bool	IsError()			{ return	(GetStatus() == RECORD_STATE_ERROR_OUT_OF_SPACE) || 
												(GetStatus() == RECORD_STATE_ERROR); }
		bool	WasSuccessfull()	{ return GetStatus() == RECORD_STATE_SUCCESS; }

	protected:
		RECORD_STATE m_RecordState;
		inline char const * GetClipName() const { return m_ClipName.c_str();  }
		atString	m_ClipName;				// The name of the clip
	};

	class fwGreatestMoment
	{
	public:
		static fwGreatestMoment& GetInstance();

		static void Init(unsigned initMode);
		static void Update();
		static void Shutdown(unsigned shutdownMode);

		virtual void	RecordGreatestMoment(u32 userIndex, const char* id, const char* displayName, s32 startTime, u32 duration) = 0;

		virtual	void	EnableRecording() = 0;
		virtual	void	DisableRecording() = 0;


	protected:
		fwGreatestMoment();

		virtual void	InitInstance(unsigned initMode) = 0;
		virtual	void	UpdateInstance() = 0;
		virtual void	ShutdownInstance(unsigned shutdownMode) = 0;

		virtual ~fwGreatestMoment() {}
	private:

	};

	inline fwGreatestMoment::fwGreatestMoment()
	{
	}

	inline void fwGreatestMoment::Init(unsigned initMode)
	{
		fwGreatestMoment::GetInstance().InitInstance(initMode);
	}

	inline void fwGreatestMoment::Update()
	{
		fwGreatestMoment::GetInstance().UpdateInstance();
	}

	inline void fwGreatestMoment::Shutdown(unsigned shutdownMode)
	{
		fwGreatestMoment::GetInstance().ShutdownInstance(shutdownMode);
	}

}

#endif // _GREATESTMOMENT_H_