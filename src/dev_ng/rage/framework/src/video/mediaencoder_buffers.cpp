/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_buffers.cpp
// PURPOSE : Class for storing buffer data for encoding audio/video
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_buffers.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

#if (RSG_PC || RSG_DURANGO)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <WinSock2.h>
#include <Windows.h>
#include <mfapi.h>
#pragma warning(pop)
#endif // (RSG_PC || RSG_DURANGO)

// rage
#if RSG_ORBIS
#include "grcore/texture_gnm.h"
#endif

#include "grcore/device.h"
#include "grcore/quads.h"
#include "grcore/viewport.h"

#include "profile/profiler.h"
#include "system/timer.h"

// framework
#include "fwsys/timer.h"
#include "media_common.h"
#include "mediaencoder_bufferinterface_rage.h"
#include "mediaencoder_texture_pool.h"
#include "mediaencoder_colourconversion.h"
#include "mediatimer.h"
#include "video_channel.h"

namespace rage
{

bool MediaEncoderBuffers::Initialize( MediaEncoderParams const& captureParameters )
{
	bool success = false;

	Assertf( !IsInitialized(), "MediaEncoderBuffers::Initialize - Double initializing! This is BAD!" );
	if( !IsInitialized() )
	{
		m_bufferPool.Initialize( captureParameters );
		success = IsInitialized();		
	}

	return success;
}

bool MediaEncoderBuffers::IsInitialized() const
{
	return m_bufferPool.IsInitialized();
}

void MediaEncoderBuffers::Shutdown()
{
    SetSynchronized( false );

    {
        SYS_CS_SYNC( m_framePushCsToken );

        DiscardAllBuffers();
        m_bufferPool.Shutdown();
    }
}

// PURPOSE: Takes the incoming render target and converts it into a video frame to be encoded.
// PARAMS:
//		source - grcTexture object we want to turn into a video frame.
//		frameDuration - Frame duration to use in seconds.
bool MediaEncoderBuffers::PushVideoFrame( grcTexture const& source, float const frameDuration )
{
	bool framePushed = false;

	//! Only try and lock here, as if we can't get the CS we are being shut-down anyway
	if( m_framePushCsToken.TryLock() )
	{
		if( IsInitialized() ) 
		{
			WaitForVideoFrame();

			if( IsVideoFrameAvailable() )
			{
				u64 const frameDurationNs = FLOAT_SECONDS_TO_NANOSECONDS( frameDuration );
				{

					grcTexture const * sourceTexture = (grcTexture const *)&source;

#if defined(VIDEO_ENCODE_BUFFERS_AS_BLOBS) && VIDEO_ENCODE_BUFFERS_AS_BLOBS

					grcTextureLock oLock;
					if( sourceTexture->LockRect(0, 0, oLock, grcsRead ) )
					{
						u8* pSrc = (u8*)oLock.Base;

						size_t const c_bytesPerPixel = oLock.BitsPerPixel / 8;
						size_t const c_elementSize = oLock.Width * c_bytesPerPixel;

						framePushed = PushVideoFrameBlob( pSrc, oLock.Pitch - c_elementSize, frameDurationNs );
						sourceTexture->UnlockRect(oLock);
					}

					framePushed = true;

#elif defined(VIDEO_ENCODE_BUFFERS_AS_TEXTURES) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES

					grcTextureObject* textureObject( NULL );
					
					MEDIA_ENCODER_COLOUR_CONVERTER.ConvertRGBToNV12_GPU(textureObject, sourceTexture);

					if( textureObject )
					{
						framePushed = PushVideoFrameRawTexture( textureObject, frameDurationNs );
					}
#else

#error Unknown encoding expectations. Need to buffer either as a binary blob or as a texture.

#endif
				}
			}
			else
			{
				Warningf( "MediaEncoderBuffers::PushVideoFrame - Video buffer starvation. Expect dropped frames!" );
			}
		}

		m_framePushCsToken.Unlock();
	}

	return framePushed;
}

#if defined(VIDEO_ENCODE_BUFFERS_AS_TEXTURES) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES

// PURPOSE: Pushes a frame to be encoded. Frame must already be in the correct surface type and dimensions.
bool MediaEncoderBuffers::PushRawVideoFrame( grcTexture& source, float const frameDuration )
{
	bool framePushed = false;

	if( IsInitialized() )
	{
		WaitForVideoFrame();

		if( IsVideoFrameAvailable() )
		{
			u64 const frameDurationNs = FLOAT_SECONDS_TO_NANOSECONDS( frameDuration ); 

			framePushed = PushVideoFrameRawTexture( source.GetTexturePtr(), frameDurationNs );
		}
		else
		{
			Warningf( "MediaEncoderBuffers::PushRawVideoFrame - Video buffer starvation. Expect dropped frames!" );
		}
	} 

	return framePushed;
}

#endif

void MediaEncoderBuffers::PushAudioBuffer( s16 const* audioBuffer, size_t const sampleCount )
{
	if( IsInitialized() && Verifyf( audioBuffer, "PushAudioBuffer - Null buffer provided!" ) 
		&& Verifyf( sampleCount > 0, "PushAudioBuffer - Invalid sample count!" ) )
	{
		WaitForAudioBuffer();

		if( IsAudioBufferAvailable() )
		{
			//! Try and get the last buffer we were using
			MediaBufferAudioEncode* newBuffer = m_bufferPool.TryGetLastReturnedAudioBuffer( MediaCommon::sc_gameAudioBufferSize );
			if( newBuffer )
			{
				newBuffer->Append( (u8*)audioBuffer, MediaCommon::sc_gameAudioBufferSize );
			}
			else
			{
				// Failed to get an existing buffer, so get a new buffer
				newBuffer = m_bufferPool.TryGetAudioBuffer( MediaCommon::sc_gameAudioBufferSize );
				if( newBuffer )
				{
					newBuffer->SetCreationTimeNs( MediaTimer::GetConstantTimeNs() );
					newBuffer->Set( (u8*)audioBuffer, MediaCommon::sc_gameAudioBufferSize );
				}
			}

			if( newBuffer )
			{
				// If this buffer has no further space, submit it and invalidate the last returned detail
				if( newBuffer->GetSizeRemaining() < MediaCommon::sc_gameAudioBufferSize )
				{
					FlushGivenAudioBuffer( newBuffer );
				}
			}
		}
		else
		{
			Warningf( "MediaEncoderBuffers::PushAudioBuffer - Audio buffer starvation. Expect pops and clicks!" );
		}
	}
}

void MediaEncoderBuffers::FlushPartialAudioBuffer()
{
	if( IsInitialized() )
	{
		//! Try and get the last buffer we were using
		MediaBufferAudioEncode* newBuffer = m_bufferPool.TryGetLastReturnedAudioBuffer( MediaCommon::sc_gameAudioBufferSize );
		FlushGivenAudioBuffer( newBuffer );
	}
}

void MediaEncoderBuffers::PushEOFBuffer()
{
	m_videoBufferQueue.Push( (MediaBufferVideoEncode*)BUFFER_EOF );
	m_audioBufferQueue.Push( (MediaBufferAudioEncode*)BUFFER_EOF );
}

bool MediaEncoderBuffers::IsEOFBuffer( MediaBufferBase const * const buffer ) const
{
	return buffer == (MediaBufferBase*)BUFFER_EOF;
}

bool MediaEncoderBuffers::PopVideoBuffer( MediaBufferBase*& out_buffer )
{
	Assertf( out_buffer == NULL, "MediaEncoderBuffers::PopVideoBuffer - Non-NULL out param. Potential leak?" );
	out_buffer = NULL;

	return m_videoBufferQueue.PopWait( (MediaBufferVideoEncode*&) out_buffer );
}

bool MediaEncoderBuffers::PopAudioBuffer( MediaBufferBase*& out_buffer )
{
	Assertf( out_buffer == NULL, "MediaEncoderBuffers::PopAudioBuffer - Non-NULL out param. Potential leak?" );
	out_buffer = NULL;

	return m_audioBufferQueue.PopWait( (MediaBufferAudioEncode*&) out_buffer );
}

void MediaEncoderBuffers::ReleaseBuffer( MediaBufferBase*& buffer )
{
	if( buffer )
	{
		if( IsEOFBuffer( buffer ) )
		{
			buffer = NULL;
		}
		else
		{
			m_bufferPool.ReleaseUnknownBufferType( buffer );
		}
	}
}

void MediaEncoderBuffers::DiscardAllBuffers()
{
	MediaBufferAudioEncode* audioBuffer = NULL;
	while( m_audioBufferQueue.PopPoll( audioBuffer ) )
	{
		m_bufferPool.ReleaseAudioBuffer( audioBuffer );
	}

	MediaBufferVideoEncode* videoBuffer = NULL;
	while( m_videoBufferQueue.PopPoll( videoBuffer ) )
	{
		m_bufferPool.ReleaseVideoBuffer( videoBuffer );
	}
}

void MediaEncoderBuffers::FlushGivenAudioBuffer( MediaBufferAudioEncode* audioBuffer )
{
	if( audioBuffer )
	{
		m_bufferPool.InvalidateLastReturnedAudioBuffer();

		u64 const c_durationNs = ( audioBuffer->GetUsedSize() / MediaCommon::sc_gameAudioBufferSize ) 
			* MediaCommon::sc_gameAudioBufferDurationNs;

		audioBuffer->SetDurationNs( c_durationNs );

		m_audioBufferQueue.Push( audioBuffer );
	}
}

bool MediaEncoderBuffers::IsVideoFrameAvailable() const
{
	return !m_bufferPool.IsVideoQueueFull();
}

bool MediaEncoderBuffers::IsAudioBufferAvailable() const
{
	return !m_bufferPool.IsAudioQueueFull();
}

bool MediaEncoderBuffers::WaitForVideoFrame() const
{
	while( m_synchronized && !IsVideoFrameAvailable() )
	{
		// Wait for video encoder thread
		sysIpcSleep(1);
	}

	return IsVideoFrameAvailable();
}

bool MediaEncoderBuffers::WaitForAudioBuffer() const
{
	while( m_synchronized && !IsAudioBufferAvailable() )
	{
		// Wait for audio encoder thread
		sysIpcSleep(1);
	}

	return IsAudioBufferAvailable();
}

#if defined( VIDEO_ENCODE_BUFFERS_AS_BLOBS ) && VIDEO_ENCODE_BUFFERS_AS_BLOBS

bool MediaEncoderBuffers::PushVideoFrameBlob( u8 const* source, size_t const padding, u64 const durationNs )
{
	bool framePushed = false;

	MediaBufferVideoEncode* newBuffer = m_bufferPool.TryGetVideoBuffer();
	if( newBuffer )
	{
		newBuffer->SetFromRGBABlob( source, padding );
		newBuffer->SetDurationNs( durationNs );
		newBuffer->SetCreationTimeNs( MediaTimer::GetConstantTimeNs() );
		m_videoBufferQueue.Push( newBuffer );

		framePushed = true;
	}

	return framePushed;
}

#elif defined( VIDEO_ENCODE_BUFFERS_AS_TEXTURES ) && VIDEO_ENCODE_BUFFERS_AS_TEXTURES

bool MediaEncoderBuffers::PushVideoFrameRawTexture( grcTextureObject* texture, u64 const durationNs )
{
	bool framePushed = false;

	MediaBufferVideoEncode* newBuffer = m_bufferPool.TryGetVideoBuffer();
	if( newBuffer )
	{
		newBuffer->Set( texture, GRCDEVICE.AllocFenceAndGpuWrite() );
		newBuffer->SetDurationNs( durationNs );
		newBuffer->SetCreationTimeNs( MediaTimer::GetConstantTimeNs() );
		m_videoBufferQueue.Push( newBuffer );

		framePushed = true;
	}

	return framePushed;
}

#endif

} // namespace rage

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER
