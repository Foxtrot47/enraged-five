#ifndef __CONTENT_EXPORT_ORBIS_H__
#define __CONTENT_EXPORT_ORBIS_H__

#if RSG_ORBIS

// Content Export for Orbis. Very Basic Wrapper, may need better 

class CContentExport
{
public:
	static void Init(unsigned initMode);
	static void Shutdown(unsigned shutdownMode);

	static bool DoExportVideo(const char* title, const char *pMoviePath, const char *pThumbnailPath = NULL);

private:

	static void *Allocate(size_t size, void* user_data);
	static void DeAllocate(void* ptr, void* user_data);

	static bool	m_InitSuccess;
};

#endif	//RSG_ORBIS

#endif //__CONTENT_EXPORT_ORBIS_H__
