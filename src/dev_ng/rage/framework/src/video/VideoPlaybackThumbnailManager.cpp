/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlaybackThumbnailManager.cpp
// PURPOSE : Class for maintaining thumbnails generated from video playback instances
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "VideoPlaybackThumbnailManager.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

// rage
#include "grcore/allocscope.h"
#include "grcore/resourcecache.h"
#include "grcore/texture.h"
#include "grcore/texture_d3d11.h"
#include "grcore/viewport.h"
#include "math/amath.h"
#include "system/interlocked.h"

// framework
#include "fwsys/gameskeleton.h"
#include "video/media_common.h"
#include "video/video_channel.h"

namespace rage
{

#define VIDEO_THUMB_WIDTH ( (u32)256 )
#define VIDEO_THUMB_HEIGHT ( (u32)128 )

grcViewport* VideoPlaybackThumbnailManager::ms_conversionViewport = NULL;
VideoPlaybackThumbnailManager::VideoThumbnailEntries VideoPlaybackThumbnailManager::ms_thumbnailEntries;
VideoPlaybackThumbnailManager::VideoThumbnailEntries VideoPlaybackThumbnailManager::ms_thumbsToRemove;

rage::VideoInstanceHandle VideoPlaybackThumbnailManager::ms_playbackHandle = INDEX_NONE;
VideoPlaybackThumbnailManager::VideoThumbnailBuffer* VideoPlaybackThumbnailManager::ms_stagingEntry = NULL;
rage::sysCriticalSectionToken VideoPlaybackThumbnailManager::ms_stagingCsToken;

void VideoPlaybackThumbnailManager::Init( unsigned initMode )
{
	(void)initMode;
	/*if( initMode == INIT_CORE && !IsInitialized() )
	{
		
	}*/
}

bool VideoPlaybackThumbnailManager::IsInitialized()
{
	return true;
}

void VideoPlaybackThumbnailManager::Shutdown( unsigned shutdownMode )
{
	if( shutdownMode == SHUTDOWN_CORE && IsActive() )
	{
		Deactivate();
	}
}

void VideoPlaybackThumbnailManager::Activate()
{
	if( !IsActive() && IsInitialized() )
	{
		CreateViewport( ms_conversionViewport );
	}
}

bool VideoPlaybackThumbnailManager::IsActive()
{
	return ms_conversionViewport != NULL;
}

void VideoPlaybackThumbnailManager::Deactivate()
{
	if( IsActive() )
	{
		releaseAllImages();
		PerformReleaseImageList();
		DestroyViewport( ms_conversionViewport );
	}
}

void VideoPlaybackThumbnailManager::Update()
{
	// NOP
}

void VideoPlaybackThumbnailManager::UpdateThumbnailFrames()
{
#if !RSG_ORBIS
    SYS_CS_SYNC( ms_stagingCsToken );

	// only remove thumbnails when video isn't loading
	if (!VideoPlayback::IsValidHandle( ms_playbackHandle ) && !VideoPlayback::HasValidFrame( ms_playbackHandle ))
	{
		PerformReleaseImageList();
	}

	if( IsActive() )
	{
		// === Handle releasing the video file if we've captured it
		if( VideoPlayback::IsValidHandle( ms_playbackHandle ) && VideoPlayback::HasValidFrame( ms_playbackHandle ) )
		{
			bool const c_cleanupVideo = ms_stagingEntry ? ( ms_stagingEntry->IsLoaded() || ms_stagingEntry->HasLoadFailed() ) : true;

			if( c_cleanupVideo )
			{
				VideoPlayback::CleanupVideo( ms_playbackHandle );
				ms_stagingEntry = NULL;
			}
		}

        if( ms_stagingEntry == NULL )
        {
            //! Find the next thumbnail pending a load
            VideoThumbnailBuffer** itr = ms_thumbnailEntries.begin();
            while( itr && itr != ms_thumbnailEntries.end() )
            {
                VideoThumbnailBuffer* entry = *itr;
                if( entry && entry->RequiresLoading() && VideoPlayback::IsPlaybackInstanceAvailabile() )	
                {
                    entry->SetLoadAttempted();

                    ms_playbackHandle = VideoPlayback::PrepareVideo( entry->m_file.c_str(), true, true );
                    if( VideoPlayback::IsValidHandle( ms_playbackHandle ) )
                    {
                        ms_stagingEntry = entry;
                        break;
                    }
                    else
                    {
                        entry->SetLoadFailed();
                    }
                }

                ++itr;
            }
        }

		// === Handle capturing the thumbnail from the video file ===
		if( VideoPlayback::IsValidHandle( ms_playbackHandle ) )
		{
			if( VideoPlayback::HasValidFrame( ms_playbackHandle ) )
			{
                if( ms_stagingEntry )
                {
                    if (  ms_stagingEntry->m_refCount > 0 )
                    {
                        GRC_ALLOC_SCOPE_AUTO_PUSH_POP();
                        GRC_VIEWPORT_AUTO_PUSH_POP();

                        if( ms_stagingEntry->RequiresAllocation() )
                        {
                            CreateThumbnailTarget( ms_stagingEntry->m_target WIN32PC_ONLY(, ms_stagingEntry->m_texture) );
                        }

#if RSG_PC
                        grcTexture* entryTexture = (grcTexture*)ms_stagingEntry->m_texture;
#else
                        grcTexture* entryTexture = (grcTexture*)ms_stagingEntry->m_target;
#endif

                        grcTextureFactory::GetInstance().LockRenderTarget( 0, ms_stagingEntry->m_target, NULL );

                        grcViewport::SetCurrent( ms_conversionViewport );
                        ms_conversionViewport->SetWindow( 0, 0, VIDEO_THUMB_WIDTH, VIDEO_THUMB_HEIGHT );
                        ms_conversionViewport->Screen();

                        VideoPlayback::RenderToGivenSize( ms_playbackHandle, VIDEO_THUMB_WIDTH, VIDEO_THUMB_HEIGHT, rage::fwuiScaling::UI_SCALE_CLIP );

                        grcTextureFactory::GetInstance().UnlockRenderTarget( 0 );

                        if( entryTexture )
                        {
#if RSG_PC
                            if (GRCDEVICE.UsingMultipleGPUs())
                            {
                                // CPU round trip to propagate texture to all GPUs
                                grcTextureDX11* pTexture = (grcTextureDX11*)entryTexture;
                                if (!pTexture->HasStagingTexture())
                                    pTexture->InitializeTempStagingTexture();
                                pTexture->UpdateCPUCopy();
                                pTexture->UpdateGPUCopyFromBackingStore();
                            }
#endif
                            atFinalHashString const c_thumbnailHash( ms_stagingEntry->m_id );
                            ms_stagingEntry->m_textureDictionary.addTexture( c_thumbnailHash, entryTexture );
                            ms_stagingEntry->SetLoaded();
                        }
                        else
                        {
                            ms_stagingEntry->SetLoadFailed();
                        }
                    }
                }
            }
		}       
        
        if( ms_stagingEntry && VideoPlayback::HasError( ms_playbackHandle ) )
        {
            ms_stagingEntry->SetLoadFailed();
            ms_stagingEntry->Release();
        }
	}

#endif
}

bool VideoPlaybackThumbnailManager::canRequestImages()
{
    SYS_CS_SYNC( ms_stagingCsToken );
    return ms_stagingEntry == NULL && !fwImposedTextureDictionary::IsAnyPendingCleanup() && ms_thumbsToRemove.GetCount() == 0;    
}

bool VideoPlaybackThumbnailManager::isRequestingImage()
{
    SYS_CS_SYNC( ms_stagingCsToken );

	bool result = ms_stagingEntry ? true : VideoPlayback::IsValidHandle( ms_playbackHandle );

	VideoThumbnailBuffer** itr = ms_thumbnailEntries.begin();
	while( itr && itr != ms_thumbnailEntries.end() && result == false )
	{
		VideoThumbnailBuffer* entry = *itr;
		result = entry && !entry->IsLoaded();
		++itr;
	}

	return result;
}

bool VideoPlaybackThumbnailManager::isImageRequested( atFinalHashString const& textureName )
{
    SYS_CS_SYNC( ms_stagingCsToken );
	VideoThumbnailBuffer const* entry = getThumbnailEntryInternal( textureName );
	return entry != NULL;
}

bool VideoPlaybackThumbnailManager::isImageLoaded( atFinalHashString const& textureName )
{
    SYS_CS_SYNC( ms_stagingCsToken );
	VideoThumbnailBuffer const* entry = getThumbnailEntryInternal( textureName );
	bool const c_loaded = entry && entry->IsLoaded();
	return c_loaded;
}

bool VideoPlaybackThumbnailManager::hasImageRequestFailed( atFinalHashString const& textureName )
{
    SYS_CS_SYNC( ms_stagingCsToken );
	VideoThumbnailBuffer const* entry = getThumbnailEntryInternal( textureName );
	bool const c_loadFailed = entry ? entry->HasLoadFailed() : true;
	return c_loadFailed;
}

bool VideoPlaybackThumbnailManager::requestImage( char const * const filename, atFinalHashString const& textureName )
{
    SYS_CS_SYNC( ms_stagingCsToken );
	bool success = isImageRequested( textureName );

    s32 const c_entryCount = ms_thumbnailEntries.GetCount();
    s32 const c_maxCount = ms_thumbnailEntries.GetMaxCount();

	if( !success && !fwImposedTextureDictionary::IsPendingCleanup( textureName.GetCStr() ) &&
		Verifyf( filename, "VideoPlaybackThumbnailManager::requestImage - NULL filename provided" ) &&
		Verifyf( c_entryCount < c_maxCount, "VideoPlaybackThumbnailManager::requestImage - "
		"maximum thumbnail count of %u already requested", ms_thumbnailEntries.GetMaxCount() ) )
	{
		VideoThumbnailBuffer* newThumbnail = rage_new VideoThumbnailBuffer();
		if( Verifyf( newThumbnail, "VideoPlaybackThumbnailManager::requestImage - Failed to allocate new thumbnail" ) )
		{
			newThumbnail->m_id = textureName;
			newThumbnail->m_file = filename;
			success = newThumbnail->m_textureDictionary.initialize( textureName.GetCStr() );

            if( success )
            {
                ms_thumbnailEntries.Push( newThumbnail );
            }
            else
            {
                delete newThumbnail;
            }
		}
	}

	return success;
}

void VideoPlaybackThumbnailManager::releaseImage( atFinalHashString const& textureName )
{
    SYS_CS_SYNC( ms_stagingCsToken );
	if( textureName.IsNotNull() )
	{
		VideoThumbnailBuffer** itr = ms_thumbnailEntries.begin();
		while( itr && itr != ms_thumbnailEntries.end() )
		{
			VideoThumbnailBuffer* entry = *itr;
            
			if( entry && entry->m_id == textureName )	
			{
                ms_thumbnailEntries.erase( itr );
				ms_thumbsToRemove.Push(entry);

				break;
			}

			++itr;
		}
	}
}

void VideoPlaybackThumbnailManager::releaseAllImages()
{
    SYS_CS_SYNC( ms_stagingCsToken );

	while( ms_thumbnailEntries.GetCount() > 0 )
	{
        VideoThumbnailBuffer* entry = ms_thumbnailEntries.Pop();
        ms_thumbsToRemove.Push(entry);
	}
}

bool VideoPlaybackThumbnailManager::CreateThumbnailTarget( grcRenderTarget*& thumbnailTarget WIN32PC_ONLY(, grcTexture*& thumbnailTexture) )
{
	bool success = false;

	if( videoVerifyf( thumbnailTarget == NULL, "VideoPlaybackThumbnailManager::CreateThumbnailTarget - "
		"Double initializing target" ))
	{
#if RSG_PC
		ASSERT_ONLY(RESOURCE_CACHE_ONLY(grcResourceCache::GetInstance().SetSafeResourceCreate(true)));

		grcTextureFactory::TextureCreateParams TextureParams(grcTextureFactory::TextureCreateParams::SYSTEM, 
			grcTextureFactory::TextureCreateParams::LINEAR,	grcsRead|grcsWrite, NULL, 
			grcTextureFactory::TextureCreateParams::RENDERTARGET, 
			grcTextureFactory::TextureCreateParams::MSAA_NONE);
		thumbnailTexture = grcTextureFactory::GetInstance().Create( VIDEO_THUMB_WIDTH, VIDEO_THUMB_HEIGHT, grctfA8B8G8R8, NULL, 1U, &TextureParams );

		if (thumbnailTexture)
			thumbnailTarget = grcTextureFactory::GetInstance().CreateRenderTarget( "VideoThumbnails", thumbnailTexture->GetTexturePtr());

		ASSERT_ONLY(RESOURCE_CACHE_ONLY(grcResourceCache::GetInstance().SetSafeResourceCreate(false)));
#else	// RSG_PC
		grcTextureFactory::CreateParams params;
		params.Format = grctfA8B8G8R8;
		params.Lockable = true;

		thumbnailTarget = grcTextureFactory::GetInstance().CreateRenderTarget( "VideoThumbnails", grcrtPermanent, 
			VIDEO_THUMB_WIDTH, VIDEO_THUMB_HEIGHT, 32, &params );

#endif	// RSG_PC
		success = videoVerifyf( thumbnailTarget != NULL, "VideoPlaybackThumbnailManager::CreateThumbnailTarget - Failed to create thumbnail target!" );
	}

	if( !success )
	{
		DestroyThumbnailTarget( thumbnailTarget WIN32PC_ONLY(, thumbnailTexture));
	}

	return success;
}

bool VideoPlaybackThumbnailManager::CreateViewport( grcViewport*& conversionViewport )
{
	bool success = false;

	if( videoVerifyf( conversionViewport == NULL, "VideoPlaybackThumbnailManager::CreateThumbnailTarget - "
		"Double initializing viewport" ))
	{
		conversionViewport = rage_new grcViewport();
		success = videoVerifyf( conversionViewport != NULL, "VideoPlaybackThumbnailManager::CreateThumbnailTarget - "
			"Failed to create viewport!" );
	}

	return success;
}

/*bool VideoPlaybackThumbnailManager::CreateThumbnailTexture( grcTexture*& thumbnailTexture )
{
	bool success = false;

	if( videoVerifyf( thumbnailTexture == NULL, "VideoPlaybackThumbnailManager::CreateThumbnailTexture - "
		"Double initializing texture" ) )
	{
		grcTextureFactory::TextureCreateParams oParams( grcTextureFactory::TextureCreateParams::VIDEO,
			grcTextureFactory::TextureCreateParams::LINEAR, grcsRead | grcsWrite );	

		ASSERT_ONLY(RESOURCE_CACHE_ONLY(grcResourceCache::GetInstance().SetSafeResourceCreate(true)));

		thumbnailTexture = grcTextureFactory::GetInstance().Create( VIDEO_THUMB_WIDTH, VIDEO_THUMB_HEIGHT,
			grctfA8B8G8R8, NULL, 1, &oParams );

		ASSERT_ONLY(RESOURCE_CACHE_ONLY(grcResourceCache::GetInstance().SetSafeResourceCreate(false)));

		success = videoVerifyf( thumbnailTexture != NULL, "VideoPlaybackThumbnailManager::CreateThumbnailTexture - "
			"Failed to create thumbnail texture!" );
	}

	return success;
}*/

void VideoPlaybackThumbnailManager::DestroyThumbnailTarget( grcRenderTarget*& thumbnailTarget WIN32PC_ONLY(, grcTexture*& thumbnailTexture) )
{
	if( thumbnailTarget )
	{
		thumbnailTarget->Release();
		thumbnailTarget = NULL;
	}
#if RSG_PC
	if( thumbnailTexture )
	{
		thumbnailTexture->Release();
		thumbnailTexture = NULL;
	}
#endif
}

void VideoPlaybackThumbnailManager::DestroyViewport( grcViewport*& conversionViewport )
{
	if( conversionViewport )
	{
		delete conversionViewport;
		conversionViewport = NULL;
	}
}

VideoPlaybackThumbnailManager::VideoThumbnailBuffer* VideoPlaybackThumbnailManager::getThumbnailEntryInternal( 
	atFinalHashString const& textureName )
{
	return VideoPlaybackThumbnailManager::getThumbnailEntryInternal( textureName.GetHash() );
}

VideoPlaybackThumbnailManager::VideoThumbnailBuffer* VideoPlaybackThumbnailManager::getThumbnailEntryInternal( u32 const textureHash )
{
    SYS_CS_SYNC( ms_stagingCsToken );

	VideoThumbnailBuffer* returnEntry = 0;

	for( int index = 0; index < ms_thumbnailEntries.GetCount(); ++index )
	{
		VideoThumbnailBuffer* entry = ms_thumbnailEntries[ index ];
		if( entry->m_id == textureHash )
		{
			returnEntry = entry;
			break;
		}
	}

	return returnEntry;
}

void VideoPlaybackThumbnailManager::ReleaseThumbnailInternal( VideoThumbnailBuffer*& inout_thumbnailEntry )
{
    SYS_CS_SYNC( ms_stagingCsToken );

	if( inout_thumbnailEntry )
    {
        inout_thumbnailEntry->Release();
		inout_thumbnailEntry = NULL;
	}
}

void VideoPlaybackThumbnailManager::PerformReleaseImageList()
{
	while( ms_thumbsToRemove.GetCount() > 0 )
	{
		VideoThumbnailBuffer* entry = ms_thumbsToRemove.Pop();
		ReleaseThumbnailInternal( entry );
		ms_stagingEntry = NULL;
	}
}

u32 VideoPlaybackThumbnailManager::VideoThumbnailBuffer::Release()
{
	delete this;
	return 0;
}

void VideoPlaybackThumbnailManager::VideoThumbnailBuffer::Cleanup()
{
	m_textureDictionary.shutdown();
	DestroyThumbnailTarget( m_target WIN32PC_ONLY(, m_texture) );
}

} // namespace rage

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
