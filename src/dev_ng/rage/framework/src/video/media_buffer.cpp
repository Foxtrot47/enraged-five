/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : media_buffer.cpp
// PURPOSE : Class representing a single buffer of encoder data
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "media_buffer.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL
#include "assert.h"

// rage
#include <algorithm> // std::reverse_copy
#include "grcore/device.h"
#include "math/amath.h"
#include "system/memops.h"
#include "system/new.h"

// framework

#include "mediatimer.h"

namespace rage
{

/////////////////////////////////////////////////////////////////////////////////
// MediaBufferBase
/////////////////////////////////////////////////////////////////////////////////
MediaBufferBase::MediaBufferBase()
	: m_type( BUFFER_INVALID )
	, m_creationTimeNs( 0 )
	, m_durationNs( 0 )
{

}

MediaBufferBase::~MediaBufferBase()
{
	Shutdown();
}

void MediaBufferBase::Shutdown()
{
	SetType( BUFFER_INVALID );
	m_creationTimeNs = 0;
	m_durationNs = 0;
}

MediaBufferBlob::MediaBufferBlob()
	: MediaBufferBase()
	, m_buffer( NULL )
	, m_size( 0 )
	, m_usedSize( 0 )
{

}

void MediaBufferBlob::Shutdown()
{
	m_buffer = NULL;
	m_size = 0;
	m_usedSize = 0;
	MediaBufferBase::Shutdown();
}

void MediaBufferBlob::Set( u8 const * const source, size_t const sourceSize )
{
	u8* bytes = GetBytes();
	if( Verifyf( bytes, "MediaBufferBase::Set - NULL dest data" ) &&
		Verifyf( source, "MediaBufferBase::Set - NULL source data" ) && 
		Verifyf( sourceSize <= GetSize(), "MediaBufferBase::Set - Buffer size:%" SIZETFMT "d and source size:%" SIZETFMT "d. Not enough capacity!", GetSize(), sourceSize ) )
	{
		sysMemCpy( bytes, source, sourceSize );
		SetUsedSize( sourceSize );
	}
}

void MediaBufferBlob::Set( u8 const * const source, size_t const elementCount, size_t const elementSize, size_t const padding )
{
	size_t const totalSizeNeeded = elementCount * elementSize;
	u8* bytes = GetBytes();

	if( elementCount > 0 &&
		Verifyf( bytes, "MediaBufferBase::Set - NULL dest data" ) &&
		Verifyf( source, "MediaBufferBase::Set - NULL source data" ) && 
		Verifyf( (elementSize > 0), "MediaBufferBase::Set - Invalid element size size:%" SIZETFMT "d", elementSize ) &&
		Verifyf( totalSizeNeeded <= GetSize(), "MediaBufferBase::Set - Buffer size:%" SIZETFMT "d and source size:%" SIZETFMT "d. Not enough capacity!", GetSize(), totalSizeNeeded ) )
	{
		if( elementCount == 1 || padding == 0 )
		{
			sysMemCpy( bytes, source, totalSizeNeeded );
		}
		else
		{
			u8 const * srcLocal = source;
			size_t const srcLineSize = elementSize + padding;

			for( size_t index = 0; index < elementCount; ++index, srcLocal += srcLineSize )
			{
				sysMemCpy( bytes + ( index * elementSize ), srcLocal, elementSize ); 
			}
		}

		SetUsedSize( totalSizeNeeded );
	}
}

void MediaBufferBlob::Append( u8 const * const source, size_t const sourceSize )
{
	u8* bytes = GetBytes();
	if( Verifyf( bytes, "MediaBufferBase::Set - NULL dest data" ) &&
		Verifyf( source, "MediaBufferBase::Append - NULL source data" ) && 
		Verifyf( GetUsedSize() + sourceSize <= GetSize(), "MediaBufferBase::Append - Buffer size:%" SIZETFMT "d and source size:%" SIZETFMT "d. Not enough capacity!", GetSize(), GetUsedSize() + sourceSize ) )
	{
		sysMemCpy( bytes + GetUsedSize(), source, sourceSize );
		SetUsedSize( GetUsedSize() + sourceSize );
	}
}

void MediaBufferBlob::SetSizeUsed( size_t const sizeUsed )
{
	m_usedSize = Clamp<size_t>( sizeUsed, 0, GetSize() );
}

MediaBufferTexture::MediaBufferTexture()
	: m_texture( NULL )
	, m_fence( NULL )
{

}

void MediaBufferTexture::Initialize()
{
	Shutdown();
	SetType( MediaBufferBase::BUFFER_VIDEO );
}

bool MediaBufferTexture::Set( grcTextureObject* texture, grcFenceHandle fence )
{
	if( Verifyf( !IsSet(), "MediaBufferTexture::Set - Double set" ) &&
		Verifyf( texture, "MediaBufferTexture::Set - null texture provided" ) )
	{
		m_texture = texture;
#if RSG_PC || RSG_DURANGO
		m_texture->AddRef();
#endif
		m_fence = fence;
	}

	return MediaBufferTexture::IsSet();
}

void MediaBufferTexture::Shutdown()
{
	if( m_fence )
	{
		GRCDEVICE.CpuFreeFence( m_fence );
		m_fence = NULL;
	}

	if( m_texture )
	{
#if RSG_PC || RSG_DURANGO
		m_texture->Release();
#endif
		m_texture = NULL;
	}

	MediaBufferBase::Shutdown();
}

} // namespace rage

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER
