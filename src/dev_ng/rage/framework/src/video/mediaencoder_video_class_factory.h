/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_video_class_factory.h
// PURPOSE : Class factory for injecting some parameters for video encoding
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_VIDEO_CLASS_FACTORY_H_
#define INC_MEDIAENCODER_VIDEO_CLASS_FACTORY_H_

#if RSG_PC && !__RESOURCECOMPILER

// framework
#include "media_common.h"

// Windows
#pragma warning(push)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <guiddef.h>
#include <mfobjects.h> // THIS FUCKING FILE IS POPPING ONE WARNING STACK TOO MANY, AND SETTING WARNING 4200 to error. HENCE THE DOUBLE TAP.
#include <winerror.h>
#pragma warning(pop)
#pragma warning(pop)

struct IMFTransform;

#define DEFAULT_TRANSFORM_QUALITY_LEVEL (75)
#define DEFAULT_TRANSFORM_SPEED_VS_QUALITY_LEVEL (75)

namespace rage
{
	class VideoEncoderClassFactory : public IClassFactory
	{
	public:
		VideoEncoderClassFactory();
		virtual ~VideoEncoderClassFactory();

		void Initialize( bool const isWmv, u32 const qualityLevel, u32 const speedVsQualityLevel );
		bool IsValid() const { return m_registerImport != NULL && m_unregisterImport != NULL; }
		void Shutdown();

		// IUnknown
		ULONG AddRef();
		HRESULT QueryInterface( REFIID riid, _COM_Outptr_ void **ppvObject );
		ULONG Release();
		
		// IClassFactory
		HRESULT CreateInstance( IUnknown *pUnkOuter, REFIID riid, void **ppvObject );
		HRESULT LockServer( BOOL fLock );

	private: // variables and declarations
		typedef HRESULT (WINAPI *RegisterLocalImport)( IClassFactory*, REFGUID, LPCWSTR, UINT32, UINT32, MFT_REGISTER_TYPE_INFO const*, UINT32, MFT_REGISTER_TYPE_INFO const* );
		typedef HRESULT (WINAPI *UnregisterLocalImport)( IClassFactory* );

		RegisterLocalImport m_registerImport;
		UnregisterLocalImport m_unregisterImport;

		u32 m_refCount;
		u32 m_lockCount;

		u32 m_qualityLevel;
		u32 m_speedVsQualityLevel;
		bool m_isWmv;

	private: // functions

		void InitializeEncoderHooks();
		void CleanupEncoderHooks();

		HRESULT FindEncoder( const GUID& subtype, IMFTransform **ppEncoder );
	};

}

#endif // RSG_PC && !__RESOURCECOMPILER

#endif // INC_MEDIAENCODER_VIDEO_CLASS_FACTORY_H_
