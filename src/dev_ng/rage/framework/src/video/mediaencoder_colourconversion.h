/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_colourconversion.h
// PURPOSE : Class for converting data for encoding video
//
// AUTHOR  : aleksander.netzel
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_COLOURCONVERSION_H_
#define INC_MEDIAENCODER_COLOURCONVERSION_H_

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_params.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "grcore/device.h"
#include "system/messagequeue.h"

#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 

struct ID3D11Texture2D;
struct ID3D11RenderTargetView;

#endif //USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING


namespace rage
{
	// Forward declaration
	class grcRenderTarget;
	class grcViewport;

	class MediaEncoderColorConversion
	{
	public:
		MediaEncoderColorConversion()	: m_outputWidth( 0 )			
										, m_outputHeight( 0 )
#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
										, m_pEffectEncode(NULL)
										, m_tEncodeLuma(grcetNONE)
										, m_tEncodeChroma(grcetNONE)
										, m_RgbTextureId(grcevNONE)
										, m_nv12Texture(NULL)
										, m_nv12LumaRTV(NULL)
										, m_nv12ChromaRTV(NULL)
#endif			
		{}

		~MediaEncoderColorConversion() {
			Shutdown();
		}

		bool Init(u32 const outputWidth, u32 const outputHeight);
		bool IsInitialized() const;
		void Shutdown();

		void ConvertRGBToNV12_GPU(grcTextureObject*& pDst, grcTexture const * pSrc);
		static void ConvertRGBToNV12_CPU( u8* pDst, u8 const* pSrc, s32 width, s32 height, int linePadding );

	private:
		u32						m_outputWidth;
		u32						m_outputHeight;

#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
		grcEffect*				m_pEffectEncode;
		grcEffectTechnique		m_tEncodeLuma;
		grcEffectTechnique		m_tEncodeChroma;
		grcEffectVar			m_RgbTextureId;

		ID3D11Texture2D*			m_nv12Texture;
		ID3D11RenderTargetView*		m_nv12LumaRTV;
		ID3D11RenderTargetView*		m_nv12ChromaRTV;

		u32							m_nv12OutputWidth;
		u32							m_nv12OutputHeight;

		bool InitializeHardwareConversion(u32 const outputWidth, u32 const outputHeight);
		void RGBToNV12Hardware(ID3D11Texture2D* pDst, grcTexture const* pSrc);
		void ShutdownHardwareConversion();
#endif //USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING
	};

	extern MediaEncoderColorConversion MEDIA_ENCODER_COLOUR_CONVERTER;
}

#endif // (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

#endif // INC_MEDIAENCODER_COLOURCONVERSION_H_
