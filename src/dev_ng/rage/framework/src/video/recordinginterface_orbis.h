#if RSG_ORBIS

#include <video_recording.h>

#include "recordinginterface.h"

#include "system/messagequeue.h"

#include "video/mediaencoder_params.h"
#include "video/mediaencoder.h"


#if	USES_MEDIA_ENCODER
#define REC_TASK_USE_WORKER_THREAD 0
#define REC_TASK_DELAY_TO_RENDER_SYNC 1
#define HOUR_RECORDING 1
#else
#define REC_TASK_USE_WORKER_THREAD 0
#define REC_TASK_DELAY_TO_RENDER_SYNC 1
#define HOUR_RECORDING 0
#endif

#if HOUR_RECORDING
#define VIDEO_RECORDING_MAX_MEM_SIZE (2678272)
#else
#define VIDEO_RECORDING_MAX_MEM_SIZE SCE_VIDEO_RECORDING_MAX_MEM_SIZE
#endif

namespace rage
{
	bool VideoRecordingSetInfo(int32_t infoType, const char* stringContent);

class RecordingTaskOrbis : public RecordingTask
{
public:

	RecordingTaskOrbis();
	~RecordingTaskOrbis();

	void	StartRecording( const char *pName, bool const startPaused, bool const synchronized, bool const hasModdedContent );
	void	StopRecording();
	void	CancelRecording();
	void	PauseRecording();
	void	ResumeRecording();
	void	Update();
	bool	IsPaused() const;
	bool	IsReadyForSamples() const;
	bool	IsAudioFrameCaptureAllowed() const;

#if	USES_MEDIA_ENCODER
	void PushVideoFrame( grcRenderTarget const& source, float const frameDuration );
	void UpdateErrorCode();

	static inline void SetRecordingDataProvider( IVideoRecordingDataProvider* provider )
	{
		sm_recordingDataProvider = provider;
	}
#endif

	void	Reset();

	void	AddMetadata(const char* gameName, const char* gameNameShort);
	void	PrepareForExport(const char* pThumbnail = NULL);
	void	ThumbnailIsReady() { m_isThumbnailReady = true; }
	void	SetThumbnailHasFailed() { m_hasThumbnail = false; }
	bool	HasThumbnail() { return m_hasThumbnail; }
	bool	ReadyToCheckForThumbnail() { return m_thumbnailCheckDelay == eValues::ThumbnailCheckFrameDelay; }

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	static void	InitWorkerThread();
	static void	ShutdownWorkerThread();
#endif

	static void	ProhibitShareRecording(bool prohibit);

protected:
	atString	m_ThumbnailPath;
	bool		m_hasThumbnail;
	bool		m_isThumbnailReady;
	char		m_thumbnailCheckDelay;
	bool		m_isPaused;
	bool		m_isInInitialPause;

private:

	bool		m_Cancelled;

#if	USES_MEDIA_ENCODER
	MediaEncoder m_encoder;
	inline MediaEncoder& GetEncoder() { return m_encoder; }
#endif
	// Worker Thread Vars
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	struct sWorkerData
	{
		RecordingTaskOrbis* pTask;
		eWorkerAction		action;
	};
#endif
	enum eValues
	{
		ThumbnailCheckFrameDelay = 3
	};

	atString	m_gameName;
	atString	m_gameNameShort;

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	static sysIpcThreadId	sm_WorkerThread;
	static sysMessageQueue<sWorkerData, 8, true> sm_WorkerCommandQueue;

	static void	RecorderWorkerThread(void *ptr);
#endif

	static IVideoRecordingDataProvider*				sm_recordingDataProvider;

	void ProcessAction( eWorkerAction const action );
	
	char		m_RecordBuffer[VIDEO_RECORDING_MAX_MEM_SIZE];
};


class VideoRecordingInterfaceOrbis: public VideoRecordingInterface
{
public:

	VideoRecordingInterfaceOrbis();
	virtual ~VideoRecordingInterfaceOrbis();


	void	EnableRecording();
	void	DisableRecording();

	// change to not keep dynamically allocating a task
	// might be causing a rare memory bug on deletion B*2547069
	RecordingTask *CreateRecordingTask()
	{
		videoAssertf(m_pTask, "VideoRecordingInterfaceOrbis::CreateRecordingTask - no task created");
		videoAssertf(m_RefCount == 0, "VideoRecordingInterfaceOrbis::CreateRecordingTask - refcount not zero. = %d", m_RefCount);
		
		++m_RefCount;
		return (m_RefCount == 1) ? m_pTask : NULL;
	}

	void DeleteRecordingTask(RecordingTask* /*pTask*/)
	{
		--m_RefCount;
		videoAssertf(m_RefCount == 0, "VideoRecordingInterfaceOrbis::DeleteRecordingTask - refcount not set to zero. = %d", m_RefCount);
		if (m_RefCount == 0)
		{
			m_pTask->Reset();
		}
	}

protected:
private:

	RecordingTaskOrbis* m_pTask;
	s32	m_RefCount;

	bool m_ModuleLoaded;

};

}

#endif
