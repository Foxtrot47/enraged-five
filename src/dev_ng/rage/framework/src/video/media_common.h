/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : media_common.h
// PURPOSE : Container for common functionality between the encode and decode paths
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIA_COMMON_H_
#define INC_MEDIA_COMMON_H_

// rage
#include "audiohardware/mixer.h"
#include "system/mediafoundation.h"

#define SUPPORT_WMV_ENCODING (RSG_PC)

//! PC Encoder uses 100ns units for encoding and decoding...
#define FLOAT_SECONDS_TO_NANOSECONDS( seconds ) ( (u64)(seconds * 1000000000) )
#define MILLISECONDS_TO_ONE_H_NS_UNITS( ms ) ( (u64)ms * 10000 )
#define FLOAT_MILLISECONDS_TO_ONE_H_NS_UNITS( ms ) ( (u64)( ms * 10000.f ) )
#define ONE_H_NS_UNITS_TO_MILLISECONDS( oneHNs ) ( (u64)oneHNs / 10000 )
#define ONE_H_NS_UNITS_TO_FLOAT_MILLISECONDS( oneHNs ) ( oneHNs / 10000.f )

#define NANOSECONDS_TO_ONE_H_NS_UNITS( ns ) ( (u64)ns / 100 )
#define ONE_H_NS_UNITS_TO_NANOSECONDS( oneHNs ) ( (u64)oneHNs * 100 )
#define ONE_H_NS_UNITS_TO_FLOAT_NANOSECONDS( oneHNs ) ( oneHNs * 100.f )

#define NANOSECONDS_TO_MILLISECONDS( ns ) ( (u64)ns / 1000000 )
#define MILLISECONDS_TO_NANOSECONDS( ms ) ( (u64)ms * 1000000 )
#define NANOSECONDS_TO_FLOAT_MILLISECONDS( ns ) ( ns / 1000000.f )
#define FLOAT_MILLISECONDS_TO_NANOSECONDS( ms ) ( (u64)( ms * 1000000.f ) )

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if !__RESOURCECOMPILER && !__RGSC_DLL

#define VIDEO_ENCODE_BUFFERS_AS_BLOBS ( RSG_PC || RSG_ORBIS )
#define VIDEO_ENCODE_BUFFERS_AS_TEXTURES ( RSG_DURANGO )
#define USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING ( RSG_DURANGO )

#endif

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

#include <guiddef.h>

// Windows
struct IMFMediaType;
struct IMFAttributes;
struct IUnknown;

// This interface doesn't exist prior to W8
#if RSG_PC
typedef IUnknown IMFDXGIDeviceManager;
#else
struct IMFDXGIDeviceManager;
#endif

//#define MEDIA_ENABLE_REF_COUNT_DEBUGGING 0

//! COM Helper functions.
template <typename T> void ReleaseMediaObject(T *&ptrRef) 
{
	if (ptrRef) 
	{
	
#if defined(MEDIA_ENABLE_REF_COUNT_DEBUGGING) && MEDIA_ENABLE_REF_COUNT_DEBUGGING
		unsigned const c_refCount = ptrRef->Release();

		Displayf( "ReleaseMediaObject - Object %p of type %s has %u remaining refs", ptrRef, typeid(T).name(), c_refCount );
		static bool spinRefCounts = false;
		while( spinRefCounts && c_refCount > 1 )
		{
			ptrRef = ptrRef;
		}


		ptrRef = NULL;
#else
		ptrRef->Release();
		ptrRef = NULL;
#endif

	}
}

#if defined(MEDIA_ENABLE_REF_COUNT_DEBUGGING) && MEDIA_ENABLE_REF_COUNT_DEBUGGING

template <typename T> void LogRefCount(T *&ptrRef) 
{
	if (ptrRef) 
	{
		ptrRef->AddRef();
		unsigned const c_refCount = ptrRef->Release();
		Displayf( "LogRefCount - Object %p of type %s has %u remaining refs", ptrRef, typeid(T).name(), c_refCount );
	}
}

#endif

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER

namespace rage
{
	class grcTexture;

	class MediaCommon
	{
	public: // declarations and variables
		static u32		const sc_bitsPerGameAudioSample			= 16;
		static u32		const sc_bytesPerAudioSample			= sc_bitsPerGameAudioSample / 8;
		static u32		const sc_audioChannels					= 2;
		static u32		const sc_sampleCountPerGameBuffer		= sc_audioChannels * rage::kMixBufNumSamples;
		static u32		const sc_gameAudioBufferSize			= sc_sampleCountPerGameBuffer * sc_bytesPerAudioSample; 
#if RSG_ORBIS
		static u32		const sc_encoderAudioBufferMultiplier	= 4;
#else
		static u32		const sc_encoderAudioBufferMultiplier	= 1;
#endif
		static u32		const sc_encoderAudioMaxBufferSize		= sc_gameAudioBufferSize * sc_encoderAudioBufferMultiplier;						
		static u32		const sc_encoderAudioBlockAlign			= sc_audioChannels * sc_bytesPerAudioSample;
		static u32		const sc_encoderAudioBytesPerSecond		= sc_encoderAudioBlockAlign * rage::kMixerNativeSampleRate;
		
		static s64		const sc_gameAudioBufferDurationNs		= 5333333;	// Game audio frame in nano-seconds = 187.5hz = (1/187.5)*1000000000
		static u32		const sc_decoderAudioBufferSize			= sc_gameAudioBufferSize * 18;

		enum eErrorState
		{
			MEDIA_ERROR_NONE,

			MEDIA_ERROR_OOM,
			MEDIA_ERROR_UNKNOWN,
			MEDIA_ERROR_ACCESS_DENIED,
			MEDIA_ERROR_SHARING_VIOLATION,

			MEDIA_ERROR_UNSUPPORTED_FILE,
		};

		struct YUY2Frame
		{
			u8 y1;
			u8 u;
			u8 y2;
			u8 v;
		};

		enum ePrepareState
		{
			STATE_NONE = 0,

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER
			STATE_COM,
			STATE_MEDIA_FOUNDATION,
#else
			// TODO - other platforms
#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER

			STATE_PREPARED
		};

		enum eVideoFormat
		{
			VIDEO_FORMAT_INVALID = -1,

			VIDEO_FORMAT_FIRST = 0,
			VIDEO_FORMAT_H264 = VIDEO_FORMAT_FIRST,
			VIDEO_FORMAT_WMV,

			VIDEO_FORMAT_MAX
		};

		enum eAudioFormat
		{
			AUDIO_FORMAT_INVALID = -1,

			AUDIO_FORMAT_FIRST = 0,
			AUDIO_FORMAT_AAC = AUDIO_FORMAT_FIRST,
			AUDIO_FORMAT_WMA,

			AUDIO_FORMAT_MAX
		};

	public: // methods

		static ePrepareState PrepareThread();
		static void CleanupThread( ePrepareState& prepareState );

		static int MediaMulDiv( s64 const number, s64 const numerator, s64 const denominator );

		static bool IsSupportedVideoFormat( eVideoFormat const videoFormat );
		static bool IsSupportedAudioFormat( eAudioFormat const audioFormat );

		static bool IsMp4Supported();
		static bool IsWmvSupported();

		static eVideoFormat GetDefaultVideoFormat();
		static eAudioFormat GetDefaultAudioFormatForVideoFormat( eVideoFormat const videoFormat );

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

		static bool PrepareGameCompatibleOutputVideoMediaType( IMFMediaType*& mediaType, rage::u32 const width, rage::u32 const height, float const fps );
		static bool PrepareGameCompatibleOutputAudioMediaType( IMFMediaType*& mediaType );

		static bool PrepareGameCompatibleInputVideoMediaType( IMFMediaType*& mediaType );
		static bool PrepareGameCompatibleInputAudioMediaType( IMFMediaType*& mediaType );

		static GUID const& GetWmvVersionGUID();

		static GUID const& ConvertVideoFormatToGUID( MediaCommon::eVideoFormat const format );
		static eVideoFormat ConvertGUIDToVideoFormat( GUID const& videoTypeGuid );

		static GUID const& ConvertAudioFormatToGUID( MediaCommon::eAudioFormat const format );
		static eAudioFormat ConvertGUIDToAudioFormat( GUID const& audioTypeGuid );

		static bool GetDxDeviceManager( IMFDXGIDeviceManager*& out_dxDeviceManager );
		static bool PrepareAttributes( IMFAttributes*& out_attributes, IMFDXGIDeviceManager* dxDeviceManager, bool const reading );

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER

		static char const* GetFormatExtension( MediaCommon::eVideoFormat const format );
		static bool GetIsVideoFormatTopDown( MediaCommon::eVideoFormat const format );

		static void ConvertFPSToNumeratorAndDenominator( float const fps, u32& out_numerator, u32& out_denominator );
		static void ConvertNumeratorAndDenominatorToFPS( float& out_fps, u32 const out_numerator, u32 const out_denominator );

		static void ConvertYUY2ToRGB( u8 const* srcYuy2, u8* rgb1, u8* rgb2 );

		static void ConvertYUVToRGB( u8 const y, u8 const u, u8 const v, u8* rgbDest );
		static void ConvertRGBToYUV( u8 const * const rgb, u8& y, u8& u, u8& v );

		static void FillTexture( grcTexture* texture, u8 const fillValue );
		static void InitilizeXXXATexture( grcTexture* texture );

#if RSG_PC || RSG_DURANGO
		static size_t GetExtraBytesRequiredForEncode();
#endif // RSG_PC || RSG_DURANGO

#if RSG_PC

		// Extracted from VersionHelpers.h until we move to the W8.1 SDK or greater
		static bool IsWindowsVersionOrGreater( u16 const wMajorVersion, u16 const wMinorVersion, u16 const wServicePackMajor );
		static bool IsWindows7OrGreater();
		static bool IsWindows8OrGreater();
		static bool IsWindows8Point1OrGreater();

#elif RSG_DURANGO

		static inline bool IsWindows7OrGreater() { return true; }
		static inline bool IsWindows8OrGreater() { return true; }
		static inline bool IsWindows8Point1OrGreater() { return true; }

#else

		static inline bool IsWindows7OrGreater() { return false; }
		static inline bool IsWindows8OrGreater() { return false; }
		static inline bool IsWindows8Point1OrGreater() { return false; }

#endif 

	};

}


#endif // INC_MEDIA_COMMON_H_
