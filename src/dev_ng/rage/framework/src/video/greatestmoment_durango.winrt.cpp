#if RSG_DURANGO

#include "greatestmoment_durango.winrt.h"

#include "math/amath.h"
#include "file/asset.h"
#include "file/winrtbuffer.winrt.h"
#include "string/unicode.h"
#include "string/stringhash.h"
#include "system/ipc.h"
#include "system/userlist_durango.winrt.h"
#include "fwsys/gameskeleton.h"

// WinRT headers
// The following warnings cause issue with the WinRT headers.
#pragma warning(push)
#pragma warning(disable: 4668) // Undefined preprocessor used in #if.
#include <collection.h>

// Reset warnings that WinRT cause.
#pragma warning(pop)

	using namespace Windows::Xbox::Media::Capture;
	using namespace Windows::Foundation;
	using namespace Windows::Foundation::Collections;
	using namespace Windows::Storage;
	using namespace Microsoft::WRL;
	using Windows::Xbox::System::User;

// DON'T COMMIT
//OPTIMISATIONS_OFF()

// Uses the unicode converter
#define UTF8_TO_UTF16(x) reinterpret_cast<const wchar_t* const>(UTF8_TO_WIDE(x))
#define UTF16_TO_UTF8(x) WIDE_TO_UTF8(reinterpret_cast<const char16* const>(x))

namespace rage
{
//----------------------------------------------------------------------
//	fwGreatestMoment
//----------------------------------------------------------------------
	
// fwGreatestMoment creation for Durango
fwGreatestMoment& fwGreatestMoment::GetInstance()
{
	return fwGreatestMomentDurango::GetPlatformInstance();
}
//----------------------------------------------------------------------
//	fwGreatestMomentTaskDurango
//----------------------------------------------------------------------

fwGreatestMomentTaskDurango::fwGreatestMomentTaskDurango() : fwGreatestMomentTask()
{
}

fwGreatestMomentTaskDurango::~fwGreatestMomentTaskDurango()
{
	// Bin any memory we used to capture
	if( m_Clip != nullptr )
	{
		m_Clip = nullptr;
	}
	if( m_ClipInfo != nullptr )
	{
		m_ClipInfo = nullptr;
	}
	if( m_ClipCapture != nullptr )
	{
		m_ClipCapture = nullptr;
	}
	if( m_User != nullptr )
	{
		m_User = nullptr;
	}
}

void fwGreatestMomentTaskDurango::RecordTimespan(u32 userIndex, const char *pName, const char* displayName, s32 startOffset, u32 duration)
{
	try 
	{
		const sysDurangoUserInfo* userInfo = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(userIndex);
		if(userInfo != nullptr)
		{
			m_User = userInfo->GetPlatformUser();
		}
		else
		{
			m_User = nullptr;
		}

		// Verify that there is a logged in user.
		if (m_User == nullptr)
		{
			// Set the state to ERROR
			m_RecordState = RECORD_STATE_ERROR;
			return;
		}

		m_ClipName = pName;

		USES_CONVERSION;
		Platform::String ^greatestMomentID = Platform::StringReference( UTF8_TO_UTF16( m_ClipName.c_str() ) );
		m_ClipInfo = ref new ApplicationClipInfo( greatestMomentID );
		m_ClipInfo->LocalizedClipName = Platform::StringReference( UTF8_TO_UTF16(displayName) );
		
		// Create the ApplicationClipCapture instance
		m_ClipCapture = ref new ApplicationClipCapture();

		// Set up the clip to record for the time specified.
		DateTime start, end;

		const INT64 MILLISECONDS_TO_ONE_H_NS_SCALER = 10000L;

		::GetSystemTimeAsFileTime( reinterpret_cast< FILETIME* >( &start ) );
		start.UniversalTime += (startOffset * MILLISECONDS_TO_ONE_H_NS_SCALER);
		end.UniversalTime = start.UniversalTime + (duration * MILLISECONDS_TO_ONE_H_NS_SCALER);

		m_ClipCapture->RecordLimitationExceeded += ref new TypedEventHandler<ApplicationClipCapture^, Platform::Object^> (
			[this] (ApplicationClipCapture ^appCapture, Platform::Object ^)
		{
			RecordLimitExceeded(appCapture);
		});

		// Failed
		m_ClipCapture->Failed += ref new TypedEventHandler<ApplicationClipCapture^, ApplicationClipCaptureFailedEventArgs^> (
			[this] (ApplicationClipCapture^ UNUSED_PARAM(appCapture), ApplicationClipCaptureFailedEventArgs^ UNUSED_PARAM(args) )
		{
			Failed();
		});

		IAsyncOperationWithProgress<ApplicationClip^, ApplicationClip^> ^asyncOp = m_ClipCapture->RecordTimespanAsync( m_User, m_ClipInfo, start, end );

		m_RecordState = RECORD_STATE_RECORDING_TIMESPAN;

		// Completed
		// This will be executed upon upload completion
		asyncOp->Completed = ref new AsyncOperationWithProgressCompletedHandler< ApplicationClip^, ApplicationClip^ >(
			[this] ( IAsyncOperationWithProgress< ApplicationClip^, ApplicationClip^ >^ operation,
			Windows::Foundation::AsyncStatus status )
		{
			Completed(operation, status);
		});
	}
	catch ( Platform::Exception^ ex )
	{
		m_RecordState = RECORD_STATE_ERROR;
	}

}

void fwGreatestMomentTaskDurango::StartRecording(u32 userIndex, const char *id)
{
	try 
	{
		const sysDurangoUserInfo* userInfo = sysDurangoUserList::GetPlatformInstance().GetPlatformUserInfo(userIndex);
		if(userInfo != nullptr)
		{
			m_User = userInfo->GetPlatformUser();
		}
		else
		{
			m_User = nullptr;
		}

		// Verify that there is a logged in user.
		if (m_User == nullptr)
		{
			// Set the state to ERROR
			m_RecordState = RECORD_STATE_ERROR;
			return;
		}

		m_ClipName = id;

		m_ClipCapture = ref new ApplicationClipCapture();

		this->m_RecordState = RECORD_STATE_RECORDING_STARTED;

		m_ClipCapture->RecordLimitationExceeded += ref new TypedEventHandler<ApplicationClipCapture^, Platform::Object^> (
			[this] (ApplicationClipCapture ^appCapture, Platform::Object ^)
		{
			RecordLimitExceeded(appCapture);
		});

		m_ClipCapture->Failed += ref new TypedEventHandler<ApplicationClipCapture^, ApplicationClipCaptureFailedEventArgs^> (
			[this] (ApplicationClipCapture^ UNUSED_PARAM(appCapture), ApplicationClipCaptureFailedEventArgs^ UNUSED_PARAM(args) )
		{
			Failed();
		});

		Windows::Foundation::IAsyncAction ^asyncAction = m_ClipCapture->StartRecordAsync( m_User );

		asyncAction->Completed = ref new AsyncActionCompletedHandler(    
			[this]( IAsyncAction^ action, Windows::Foundation::AsyncStatus status )
		{
			try
			{
				if( action->ErrorCode.Value == 0 )
				{
					if ( status == Windows::Foundation::AsyncStatus::Completed )
					{
						// Do nothing, we're already in the RECORD_STATE_RECORDING_STARTED state
					}
					else
					{
						this->m_RecordState = RECORD_STATE_ERROR;
					}
				}
				else
				{
					this->m_RecordState = RECORD_STATE_ERROR;
				}
			}
			catch ( Platform::Exception^ ex )
			{
				this->m_RecordState = RECORD_STATE_ERROR;
			}
		});
	}
	catch ( Platform::Exception^ ex )
	{
		this->m_RecordState = RECORD_STATE_ERROR;
	}

}

void fwGreatestMomentTaskDurango::StopRecording()
{
	try
	{
		USES_CONVERSION;
		Platform::String ^greatestMomentID = Platform::StringReference( UTF8_TO_UTF16( m_ClipName.c_str() ) );
		m_ClipInfo = ref new ApplicationClipInfo( greatestMomentID );

		m_RecordState = RECORD_STATE_RECORDING_STOPPED;

		Windows::Foundation::IAsyncOperationWithProgress<ApplicationClip^, ApplicationClip^> ^asyncOp = m_ClipCapture->StopRecordAsync( m_ClipInfo );

		// Executed on Upload Completion
		asyncOp->Completed = ref new AsyncOperationWithProgressCompletedHandler< ApplicationClip^, ApplicationClip^ >(    
			[this]( IAsyncOperationWithProgress< ApplicationClip^, ApplicationClip^ >^ operation,
			Windows::Foundation::AsyncStatus status )
		{
			Completed(operation, status);
		});
	}
	catch ( Platform::Exception^ ex )
	{
		this->m_RecordState = RECORD_STATE_ERROR;
	}

}

void fwGreatestMomentTaskDurango::CancelRecording()
{
	try
	{
		// Check we're recording, otherwise an error has occurred and we can't decref.
		if( m_RecordState == RECORD_STATE_RECORDING_TIMESPAN ||
			m_RecordState == RECORD_STATE_RECORDING_STARTED )
		{
			m_RecordState = RECORD_STATE_CANCELLED;
			m_ClipCapture->CancelRecord();
		}
	}
	catch ( Platform::Exception^ ex )
	{
		this->m_RecordState = RECORD_STATE_ERROR;
	}
}

void fwGreatestMomentTaskDurango::RecordLimitExceeded(Windows::Xbox::Media::Capture::ApplicationClipCapture^ appCapture)
{
	Displayf("Record Limit Exceeded");
	m_RecordState = RECORD_STATE_ERROR_OUT_OF_SPACE;
}

void fwGreatestMomentTaskDurango::Failed()
{
	m_RecordState = RECORD_STATE_ERROR;
}

void fwGreatestMomentTaskDurango::Completed(IAsyncOperationWithProgress< ApplicationClip^, ApplicationClip^ >^ operation, Windows::Foundation::AsyncStatus status)
{
	try
	{
		if( operation->ErrorCode.Value == 0 )
		{
			if ( status == Windows::Foundation::AsyncStatus::Completed )
			{
				m_Clip = operation->GetResults();

				// DEBUG
				//fwGreatestMomentDurango::DisplayClipInfo(-1, clip);
				// DEBUG

				this->m_RecordState = RECORD_STATE_SUCCESS;
			}
			else if ( status == Windows::Foundation::AsyncStatus::Canceled )
			{
				this->m_RecordState = RECORD_STATE_CANCELLED;
			}
			else if ( status == Windows::Foundation::AsyncStatus::Error )
			{
				this->m_RecordState = RECORD_STATE_ERROR;
			}
		}
		else
		{
			this->m_RecordState = RECORD_STATE_ERROR;
		}
	}
	catch ( Platform::Exception^ ex )
	{
		//				exceptionStream << L"Exception thrown. Error code: " << ex->HResult;
		this->m_RecordState = RECORD_STATE_ERROR;
	}

}

void	fwGreatestMomentTaskDurango::Update()
{
}

//----------------------------------------------------------------------
//	fwGreatestMomentDurango
//----------------------------------------------------------------------

// fwGreatestMomentDurango creation for Durango
fwGreatestMomentDurango& fwGreatestMomentDurango::GetPlatformInstance()
{
	static fwGreatestMomentDurango greatestMoment;
	return greatestMoment;
}


fwGreatestMomentDurango::fwGreatestMomentDurango() 
	: fwGreatestMoment()
{
}

fwGreatestMomentDurango::~fwGreatestMomentDurango()
{
}

void fwGreatestMomentDurango::EnableRecording()
{
	try
	{
		auto asyncAction = ApplicationClipCapture::EnableRecordAsync();
		asyncAction->Completed = ref new AsyncActionCompletedHandler(    
			[]( IAsyncAction^ action, Windows::Foundation::AsyncStatus ASSERT_ONLY(status) )
		{
			try
			{
				Assertf( status == Windows::Foundation::AsyncStatus::Completed, "fwGreatestMomentDurango::EnableRecording() - Failed to execute" );
			}
			catch ( Platform::Exception^ ex )
			{
			}
		});
	}
	catch ( Platform::Exception^ ex )
	{
	}
}

void fwGreatestMomentDurango::DisableRecording()
{
	try
	{
		auto asyncAction = ApplicationClipCapture::DisableRecordAsync();
		asyncAction->Completed = ref new AsyncActionCompletedHandler(    
			[]( IAsyncAction^ action, Windows::Foundation::AsyncStatus ASSERT_ONLY(status) )
		{    
			try
			{
				Assertf( status == Windows::Foundation::AsyncStatus::Completed, "fwGreatestMomentDurango::DisableRecording() - Failed to execute" );
			}
			catch ( Platform::Exception^ ex )
			{
			}

		});
	}
	catch ( Platform::Exception^ ex )
	{
	}
}


void fwGreatestMomentDurango::InitInstance(unsigned initMode)
{
	if ( initMode == INIT_CORE )
	{
		::LoadLibraryW(L"applicationclip.dll");
	}
}

void fwGreatestMomentDurango::UpdateInstance()
{
	// We go backwards so we can safely and quickly remove tasks.
	for(s32 i = m_MomentTasks.size() -1; i >= 0; --i)
	{
		m_MomentTasks[i].Update();
		if(m_MomentTasks[i].IsComplete())
		{
			m_MomentTasks.DeleteFast(i);
		}
	}
}

void fwGreatestMomentDurango::ShutdownInstance(unsigned UNUSED_PARAM(shutdownMode))
{
}

void fwGreatestMomentDurango::RecordGreatestMoment(u32 userIndex, const char* id, const char* displayName, s32 startTime, u32 duration)
{
	if(Verifyf(m_MomentTasks.size() < m_MomentTasks.max_size(), "Not enough greatest moment tasks!"))
	{
		fwGreatestMomentTaskDurango& task = m_MomentTasks.Append();
		task = fwGreatestMomentTaskDurango();
		task.RecordTimespan(userIndex, id, displayName, startTime, duration);
	}
}

}	// namespace rage

#undef UTF8_TO_UTF16
#undef UTF16_TO_UTF8

#endif	// RSG_DURANGO

