/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_win.cpp
// PURPOSE : Class for encoding video/audio streams. Windows Specific.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediaencoder.h"

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if RSG_PC && !__RESOURCECOMPILER && !__RGSC_DLL

// Windows
#pragma warning(disable: 4668)
#include <Windows.h>
#include <winerror.h>
#pragma warning(error: 4668)

// rage
#include "grcore/device.h"
#include "grcore/texture.h"

// framework
#include "media_common.h"
#include "video_channel.h"

namespace rage
{

	void MediaEncoder::PrepareRenderThread()
	{
		GenerateDummyVideoData();
	}

	void MediaEncoder::CleanupRenderThread()
	{
		CleanupDummyVideoData();
	}

	bool MediaEncoder::ThreadData::IsVideoFrameCaptureAllowed() const
	{
		return m_buffers.IsInitialized() && !HasError() &&  !m_pauseFlag && !GRCDEVICE.IsPaused() 
			&& ( !m_killFlag || ( m_killFlag && m_drainFlag ) ) &&
			( m_threadMessaging & ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN ) == 0;
	}

	void MediaEncoder::GenerateDummyTexture( grcTexture*& out_texture )
	{
#if defined( USE_DUMMY_VIDEO_DATA )

#define PIX_BUFFER_SIZE ( MEDIA_ENCODER_DEFAULT_WIDTH * MEDIA_ENCODER_DEFAULT_HEIGHT )

		if( out_texture == NULL )
		{
			static DWORD sc_dummyPxBuffer[ PIX_BUFFER_SIZE ];

			// Set all pixels to green
			for ( DWORD i = 0; i < PIX_BUFFER_SIZE; ++i )
			{
				sc_dummyPxBuffer[i] = 0xFF00FF00;
			}

			//! This texture create may assert about threading. We don't care, this is dev only
			grcTextureFactory::TextureCreateParams params( 
				grcTextureFactory::TextureCreateParams::VIDEO,
				grcTextureFactory::TextureCreateParams::LINEAR,
				grcsRead, NULL,
				grcTextureFactory::TextureCreateParams::NORMAL );

			out_texture =
				grcTextureFactory::GetInstance().Create( rage::MediaEncoderParams::sc_defaultWidth, rage::MediaEncoderParams::sc_defaultHeight, 
				grctfA8R8G8B8, sc_dummyPxBuffer, 1, &params);

			videoAssertf( out_texture, "MediaEncoder::GenerateDummyVideoData - Unable to create dummy texture!" );	
		}

		out_texture->AddRef();
#else
		(void)out_texture;
#endif // defined( USE_DUMMY_DATA )
	}

} // namespace rage

#endif // RSG_PC && !__RESOURCECOMPILER
