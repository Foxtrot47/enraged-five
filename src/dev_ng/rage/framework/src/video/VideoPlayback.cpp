/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : VideoPlayback.cpp
// PURPOSE : Controller class for requesting/accessing playback of a video
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "VideoPlayback.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

// rage
#include "bank/bank.h"
#include "bank/button.h"
#include "bank/bkmgr.h"

// framework
#include "fwlocalisation/templateString.h"
#include "fwsys/gameskeleton.h"
#include "video/video_channel.h"

#if RSG_ORBIS
#include "rline/rlsystemui.h"
#endif

namespace rage
{

	rage::VideoPlaybackInstance VideoPlayback::ms_playbackInstances[MAX_CONCURRENT_VIDEOS];
	rage::ServiceDelegate VideoPlayback::ms_serviceDelegate;


#if __BANK

//! This matches the output format for BANK video recording.
#define VIDEO_PLAYBACK_DEBUG_PATH ( "D:\\RAG_Clip.mp4" )

	static bkBank*		s_pBank( NULL );
	static bkButton*	s_pCreateButton( NULL);

	static int			s_bankVideoId(0);
	static PathBuffer	s_debugVideoPath( VIDEO_PLAYBACK_DEBUG_PATH );
	static bool			s_bankVideoRender(false);

#endif // __BANK

	void VideoPlayback::Init( unsigned initMode )
	{
		ms_serviceDelegate.Bind(&sysOnServiceEvent);
		g_SysService.AddDelegate(&ms_serviceDelegate);

		if( initMode == INIT_CORE )
		{
			BANK_ONLY( InitBank() );
		}
	}

	void VideoPlayback::Shutdown( unsigned shutdownMode )
	{
		if( shutdownMode == SHUTDOWN_CORE )
		{
			Deactivate();
			BANK_ONLY( ShutdownBank() );
		}

		g_SysService.RemoveDelegate(&ms_serviceDelegate);
		ms_serviceDelegate.Unbind();
	}

	void VideoPlayback::SetAudioEntityForPlayback( audEntity* entity )
	{
		VideoPlaybackInstance::SetAudioEntityForPlayback( entity );
	}

	void VideoPlayback::SetAudioBucketId( u32 const bucketId )
	{
		VideoPlaybackInstance::SetAudioBucketId( bucketId );
	}

	bool VideoPlayback::Activate()
	{
		return IsActive() ? true : true;
	}

	bool VideoPlayback::IsActive()
	{
		return true;
	}

	void VideoPlayback::Update()
	{
		if( IsActive() )
		{
			int const c_maxInstances = GetMaxInstances();

#if RSG_ORBIS
			if (g_SystemUi.IsUiShowing())
			{
				for( int index = 0; index < c_maxInstances; ++index )
				{
					VideoPlaybackInstance& playbackInstance = GetPlaybackInstance( index );
					if (!playbackInstance.IsPaused())
					{
						playbackInstance.Pause();
					}
				}
			}
#endif

			for( int index = 0; index < c_maxInstances; ++index )
			{
				VideoPlaybackInstance& playbackInstance = GetPlaybackInstance( index );
				playbackInstance.Update();
			}
		}
	}

	void VideoPlayback::UpdatePreRender()
	{
		if( IsActive() )
		{
			int const c_maxInstances = GetMaxInstances();

			for( int index = 0; index < c_maxInstances; ++index )
			{
				VideoPlaybackInstance& playbackInstance = GetPlaybackInstance( index );
				playbackInstance.UpdatePreRender();
			}
		}
	}

	void VideoPlayback::Deactivate()
	{
		if( IsActive() )
		{
			ShutdownAllPlaybackInstances();
		}
	}

	bool VideoPlayback::IsPlaybackInstanceAvailabile()
	{
		bool available = false;

		int const c_maxInstances = GetMaxInstances();
		for( int index = 0; IsActive() && !available && index < c_maxInstances; ++index )
		{
			VideoPlaybackInstance& playbackInstance = GetPlaybackInstance( index );
			available = !playbackInstance.IsPlaybackActive();
		}

		return available;
	}

	bool VideoPlayback::IsPlaybackActive(bool allowPaused)
	{
		bool activePlayback = false;

		int const c_maxInstances = GetMaxInstances();
		for( int index = 0; IsActive() && !activePlayback && index < c_maxInstances; ++index )
		{
			VideoPlaybackInstance& playbackInstance = GetPlaybackInstance( index );
			activePlayback = playbackInstance.IsPlaybackActive() && (allowPaused || !playbackInstance.IsPaused());
		}

		return activePlayback;
	}

	bool VideoPlayback::HasValidFrame( VideoInstanceHandle const handle )
	{
		bool hasFrame = false;

		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		if( instance )
		{
			hasFrame = instance->HasValidFrame();
		}

		return hasFrame;
	}

	bool VideoPlayback::HasError( VideoInstanceHandle const handle )
	{
		bool loadFailed = false;

		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		if( instance )
		{
			loadFailed = instance->HasError();
		}

		return loadFailed;
	}

	void VideoPlayback::RenderToGivenSize( VideoInstanceHandle const handle, int const rtWidth, int const rtHeight, fwuiScaling::eMode const scaleMode )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		if( instance )
		{
			grcDepthStencilStateHandle currentDSS = grcStateBlock::DSS_Active;
			grcStateBlock::SetDepthStencilState( grcStateBlock::DSS_IgnoreDepth );

			instance->RenderToGivenSize( rtWidth, rtHeight, scaleMode );

			grcStateBlock::SetDepthStencilState( currentDSS );
		}
	}

	// PURPOSE: Check if the given video is supported
	// PARAMS:
	//		maxWidth - Maximum video width to support
	//		maxHeight - Maximum video height to support
	//		minDuration - Minimum duration to support
	//		out_width - Optional place to store the width
	//		out_height - Optional place to store the height
	//		out_duration - Optional place to place the duration
	// RETURNS:
	//		True if supported, false otherwise.
	// NOTE: Assumes Media Foundation is initialized to reduce memory fragmentation on enumeration (for Xb1/PC platforms) 
	bool VideoPlayback::IsVideoSupported( const char * const path, u32 const maxWidth, u32 const maxHeight, u32 const minDurationMs, 
		u32 * const out_width, u32 * const out_height, u32 * const out_durationMs )
	{
		bool supported = false;

		u32 width, height, durationMs;
		if( MediaDecoder::PeekVideoDetails( path, width, height, durationMs ) )
		{
			bool const c_widthValid = width <= maxWidth;
			bool const c_heightValid = height <= maxHeight;
			bool const c_durationValid = durationMs >= minDurationMs;

			supported = c_widthValid && c_heightValid && c_durationValid;

			if( out_width )
			{
				*out_width = width;
			}

			if( out_height )
			{
				*out_height = height;
			}

			if( out_durationMs )
			{
				*out_durationMs = durationMs;
			}

			videoCondLogf( !supported, DIAG_SEVERITY_WARNING, "VideoPlayback::IsVideoSupported - File %s is outside the supported resolutions; "
				"width %d / max width %d, height %d / max height %d", path, width, maxWidth, height, maxHeight );
		}
		else
		{
			videoWarningf( "VideoPlayback::IsVideoSupported - Unable to peek file %s!", path );
		}

		return supported;
	}

	// PURPOSE: Prepare a given video for playback. Open it, decode the thumbnail and wait for instructions.
	// PARAMS:
	//		path - Path to the video to prepare
	// RETURNS:
	//		Valid VideoInstanceHandle on success. Check by calling VideoPlayback::IsValidHandle
	rage::VideoInstanceHandle VideoPlayback::PrepareVideo( const char * const path, bool const captureThumbnailThenWait, bool const ignoreAudioTrack )
	{
		VideoInstanceHandle result = INDEX_NONE;

		VideoInstanceHandle const targetHandle = FindUnusedPlaybackInstance();
		if( IsValidHandle( targetHandle ) )
		{
			VideoPlaybackInstance& instance = GetPlaybackInstance( targetHandle );

			//Set the resulting handle based on initialization success!
			result = instance.Initialize( path, captureThumbnailThenWait, ignoreAudioTrack ) ? targetHandle : INDEX_NONE;
		}
		
		return result;
	}

	size_t VideoPlayback::GetFileSize( VideoInstanceHandle const handle )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		size_t const c_fileSize =  instance && instance->GetFileSize();

		return c_fileSize;
	}

	u64 VideoPlayback::GetDurationMs( VideoInstanceHandle const handle )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		u64 const c_durationMs =  instance ? instance->GetDurationMs() : 0;

		return c_durationMs;
	}

	u64 VideoPlayback::GetCurrentTimeMs( VideoInstanceHandle const handle )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		u64 const c_durationMs =  instance ? instance->GetCurrentTimeMs() : 0;

		return c_durationMs;
	}

	bool VideoPlayback::Seek( VideoInstanceHandle const handle, u64 const timeMs )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		bool const c_success =  instance && instance->Seek( timeMs );
		return c_success;
	}

	// PURPOSE: Prepare a given video for playback. Open it and play immediately.
	// PARAMS:
	//		path - Path to the video to prepare
	// RETURNS:
	//		Valid VideoInstanceHandle on success. Check by calling VideoPlayback::IsValidHandle
	rage::VideoInstanceHandle VideoPlayback::PlayVideo( const char * const path, bool const ignoreAudioTrack )
	{
		VideoInstanceHandle result = PrepareVideo( path, false, ignoreAudioTrack );
		PlayVideo( result );

		return result;
	}

	bool VideoPlayback::PlayVideo( VideoInstanceHandle const handle )
	{
		bool success = false;

		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		if( instance && instance->IsInitialized() )
		{
			success = instance->Play();
		}

		return success;
	}

    bool VideoPlayback::IsAtStartOfVideo( VideoInstanceHandle const handle )
    {
        VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
        bool const c_sof =  instance && instance->IsAtStartOfFile();

        return c_sof;
    }

	bool VideoPlayback::IsAtEndOfVideo( VideoInstanceHandle const handle )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		bool const c_eof =  instance && instance->IsAtEndOfFile();

		return c_eof;
	}

	bool VideoPlayback::IsPaused( VideoInstanceHandle const handle )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		bool const c_paused =  instance && instance->IsPaused();

		return c_paused;
	}

	bool VideoPlayback::PauseVideo( VideoInstanceHandle const handle )
	{
		bool success = false;

		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		if( instance && instance->IsInitialized() )
		{
			success = instance->Pause();
		}

		return success;
	}

	void VideoPlayback::CleanupVideo( VideoInstanceHandle& handle )
	{
		VideoPlaybackInstance* instance = TryGetPlaybackInstance( handle );
		if( instance )
		{
			instance->Shutdown();
		}

		handle = INDEX_NONE;
	}

#if __BANK

	void VideoPlayback::RenderVideoOverlayBank()
	{
		VideoInstanceHandle const bankHandle = s_bankVideoId;

		if( s_bankVideoRender && IsValidHandle( bankHandle ) )
		{
			VideoPlaybackInstance &playbackInstance = GetPlaybackInstance( bankHandle );
			if( playbackInstance.IsInitialized() )
			{
				playbackInstance.RenderVideoOverlayBank();
			}
		}
	}

#endif // __BANK

	VideoInstanceHandle VideoPlayback::FindUnusedPlaybackInstance()
	{
		VideoInstanceHandle handle = INDEX_NONE;

		if( IsActive() )
		{
			int const c_maxInstances = GetMaxInstances();

			for( int index = 0; index < c_maxInstances; ++index )
			{
				VideoPlaybackInstance &playbackInstance = GetPlaybackInstance( index );
				if( !playbackInstance.IsInitialized() )
				{
					handle = index;
				}
			}
		}

		return handle;
	}

	void VideoPlayback::ShutdownAllPlaybackInstances()
	{
		int const c_maxInstances = GetMaxInstances();

		for( int index = 0; index < c_maxInstances; ++index )
		{
			VideoInstanceHandle handle = index; // Handle gets wiped on cleanup, so copy index to avoid corruption
			CleanupVideo( handle );
		}
	}

	void VideoPlayback::sysOnServiceEvent( rage::sysServiceEvent* evnt )
	{
		if(evnt != NULL)
		{
			switch(evnt->GetType())
			{
			case sysServiceEvent::SUSPENDED:
			case sysServiceEvent::SUSPEND_IMMEDIATE:
				{
					if( IsActive() )
					{
						int const c_maxInstances = GetMaxInstances();

						for( int index = 0; index < c_maxInstances; ++index )
						{
							VideoPlaybackInstance &playbackInstance = GetPlaybackInstance( index );
							playbackInstance.Pause();
						}
					}

					break;
				}

			default:
				{
					break;
				}
			}
		}
	}

#if __BANK

	void VideoPlayback::InitBank()
	{
		if( s_pBank )
		{
			ShutdownBank();
		}

		// Create the weapons bank
		s_pBank = &BANKMGR.CreateBank("VideoPlayback", 0, 0, false); 
		if( videoVerifyf(s_pBank, "Failed to create VideoPlayback bank") )
		{
			s_pCreateButton = s_pBank->AddButton("Create VideoPlayback widgets", &VideoPlayback::CreateBank );
		}
	}

	void VideoPlayback::CreateBank()
	{
		if( videoVerifyf( s_pBank, "VideoPlayback bank needs to be created first") && 
			videoVerifyf( s_pCreateButton, "VideoPlayback bank needs to be created first") )
		{
			s_pCreateButton->Destroy();
			s_pCreateButton = NULL;

			bkBank* pBank = s_pBank;

			pBank->PushGroup( "Video Instances", false );
			
			static char const * sc_videoIds[ MAX_CONCURRENT_VIDEOS ] =
			{
				"0"//, "1" //, "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"
			};

			pBank->AddCombo( "Target Playback Instance", &s_bankVideoId, NELEM(sc_videoIds), sc_videoIds );
			pBank->AddText( "Debug Video Path", s_debugVideoPath.getWritableBuffer(), (u32)s_debugVideoPath.GetMaxByteCount(), false );
			pBank->AddButton("Prepare Debug Playback", PreparePlaybackCB );
			pBank->AddButton("Pause/Resume Debug Playback", PauseOrResumePlaybackCB );
			pBank->AddButton("Seek to Debug Playback mid-point", SeekMidwayCB );
			pBank->AddButton("Stop Debug Playback", StopPlaybackCB );

			pBank->AddToggle( "Render Debug", &s_bankVideoRender );

			pBank->PopGroup();
		}
	}

	void VideoPlayback::PreparePlaybackCB()
	{
		// Ensure we are active
		if( !IsActive() )
		{
			Activate();
		}

		if( !s_debugVideoPath.IsNullOrEmpty() )
		{
			VideoInstanceHandle const bankHandle = VideoPlayback::PrepareVideo( s_debugVideoPath.getBuffer(), false, false );
			s_bankVideoId = IsValidHandle( bankHandle ) ? bankHandle : s_bankVideoId;
		}
	}
	
	void VideoPlayback::PauseOrResumePlaybackCB()
	{
		VideoInstanceHandle const bankHandle = s_bankVideoId;
		if( VideoPlayback::IsValidHandle( bankHandle ) )
		{
			VideoPlaybackInstance &playbackInstance = GetPlaybackInstance( bankHandle );
			if( playbackInstance.IsInitialized() )
			{
				if( VideoPlayback::IsPaused( bankHandle ) )
				{
					VideoPlayback::PlayVideo( bankHandle );
				}
				else
				{
					VideoPlayback::PauseVideo( bankHandle );
				}
			}
		}
	}

	void VideoPlayback::SeekMidwayCB()
	{
		VideoInstanceHandle const bankHandle = s_bankVideoId;
		if( VideoPlayback::IsValidHandle( bankHandle ) )
		{
			VideoPlaybackInstance &playbackInstance = GetPlaybackInstance( bankHandle );
			if( playbackInstance.IsInitialized() )
			{
				VideoPlayback::Seek( bankHandle, VideoPlayback::GetDurationMs( bankHandle ) / 2 );
			}
		}
	}

	void VideoPlayback::StopPlaybackCB()
	{
		VideoInstanceHandle const bankHandle = s_bankVideoId;
		if( VideoPlayback::IsValidHandle( bankHandle ) )
		{
			VideoPlaybackInstance &playbackInstance = GetPlaybackInstance( bankHandle );
			if( playbackInstance.IsInitialized() )
			{
				//! Reset the id after cleanup, as cleanup invalidates the handle
				int const oldBankId = s_bankVideoId;
				VideoPlayback::CleanupVideo( s_bankVideoId );
				s_bankVideoId = oldBankId;
			}
		}
	}

	void VideoPlayback::ShutdownBank()
	{
		if( s_pCreateButton )
		{
			s_pCreateButton->Destroy();
			s_pCreateButton = NULL;
		}

		if( s_pBank )
		{
			BANKMGR.DestroyBank( *s_pBank );
			s_pBank = NULL;
		}
	}

#endif // __BANK

} // namespace rage

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
