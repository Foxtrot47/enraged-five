#if RSG_ORBIS

#include <scebase_common.h>
#include <_kernel.h>
#include <_fs.h>
#include <libsysmodule.h>
#include <sdk_version.h>

#include <content_export.h>

#pragma comment(lib,"libSceContentExport_stub_weak.a")

#include "contentExport_Orbis.h"
#include "system/new.h"
#include "string/string.h"

using namespace rage;

bool CContentExport::m_InitSuccess = false;

void CContentExport::Init(unsigned)
{
	Assertf(m_InitSuccess == false, "ERROR: CContentExport::Init() - Already Inited");

	if( Verifyf(sceSysmoduleLoadModule(SCE_SYSMODULE_CONTENT_EXPORT) == SCE_OK, "ERROR: Load of SCE_SYSMODULE_CONTENT_EXPORT module failed") )
	{
		// Setup the memory allocations
		// Initialize ContentExport
		SceContentExportInitParam initparam;
		initparam.mallocfunc = Allocate;
		initparam.freefunc = DeAllocate;
		initparam.userdata = NULL;
		int ret = sceContentExportInit(&initparam);
		if( ret == SCE_OK )
		{
			m_InitSuccess = true;
		}
	}
}

// Should be called from a thread as will block!
// May need more params for title and comment in the SceContentExportParam
bool CContentExport::DoExportVideo(const char* title, const char *pMoviePath, const char *pThumbnailPath)
{
	bool	completed = false;

	int id = sceContentExportStart();
	if( id >= 0 )
	{
		char path[SCE_CONTENT_EXPORT_LENGTH_FILEPATH+1];
		SceContentExportParam exportParam;

		if (title)
			safecpy ( exportParam.title, title, SCE_CONTENT_EXPORT_LENGTH_TITLE);
		else
			exportParam.title[0] = '\0';
		exportParam.comment[0] = '\0';		// MAX Length SCE_CONTENT_EXPORT_LENGTH_COMMENT
		safecpy ( exportParam.contenttype, "video/mp4", SCE_CONTENT_EXPORT_LENGTH_CONTENTTYPE);
		
		int ret = SCE_OK;
		if (pThumbnailPath)
			ret = sceContentExportFromFileWithThumbnail(id, &exportParam, pMoviePath, pThumbnailPath, path, SCE_CONTENT_EXPORT_LENGTH_FILEPATH);
		else
			ret = sceContentExportFromFile(id, &exportParam, pMoviePath, path, SCE_CONTENT_EXPORT_LENGTH_FILEPATH);

		if( ret == SCE_OK )
		{
			ret = sceContentExportFinish(id);
			if( ret == SCE_OK )
			{
				Displayf("CContentExport::DoExportVideo() - Exported %s to %s", pMoviePath, path);
				completed = true;
			}
		}
	}
	return completed;
}

void CContentExport::Shutdown(unsigned)
{
	if(m_InitSuccess)
	{
		// Really need to sceContentExportCancel() then sceContentExportFinish() any content handles currently active.
		if( Verifyf( sceContentExportTerm() == SCE_OK,"ERROR:- CContentExport::Shutdown() - sceContentExportTerm() Failed"))
		{
			if( Verifyf(sceSysmoduleUnloadModule(SCE_SYSMODULE_CONTENT_EXPORT) == SCE_OK, "ERROR:- CContentExport::Shutdown() - sceSysmoduleUnloadModule(SCE_SYSMODULE_CONTENT_EXPORT) Failed") )
			{
				m_InitSuccess = false;
			}
		}
	}
}

void *CContentExport::Allocate(size_t size, void* UNUSED_PARAM(user_data))
{
	Displayf("Allocating %d bytes for content export", (int)size);
	void *p = rage_new char[size];
	return p;
}

void CContentExport::DeAllocate(void* ptr, void* UNUSED_PARAM(user_data))
{
	char *p = static_cast<char*>(ptr);
	delete []p;
}

#endif	//RSG_ORBIS
