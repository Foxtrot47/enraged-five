/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediadecoder_win.cpp
// PURPOSE : Class for decoding video/audio streams. Windows implementation
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediadecoder.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

#if ( RSG_DURANGO || RSG_PC ) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "atl/array.h"
#include "audioengine/engine.h"
#include "audioengine/soundmanager.h"
#include "audiohardware/driver.h"
#include "audiohardware/ringbuffer.h"
#include "file/asset.h"
#include "string/unicode.h"
#include "system/alloca.h"

// framework
#include "fwdrawlist/drawlist.h"
#include "fwlocalisation/templateString.h"
#include "media_common.h" 
#include "media_transcoding_allocator.h"
#include "mediadecoder.h"
#include "mediatimer.h"
#include "video/video_channel.h"

// Windows
#pragma warning(push)
#pragma warning(disable: 4668)
#include <Windows.h>
#include <mfapi.h>
#include <Mfidl.h>
#include <Mfreadwrite.h>
#include <propvarutil.h>
#include <winerror.h>
#if RSG_PC
#include <propkey.h>
#endif
#pragma warning(pop)

// Uses the unicode converter
#define UTF8_TO_UTF16(x) reinterpret_cast<const wchar_t* const>(UTF8_TO_WIDE(x))
#define UTF16_TO_UTF8(x) WIDE_TO_UTF8(reinterpret_cast<const char16* const>(x))

#if RSG_PC
PARAM(disableDecodeVerify,"MP4 Decoder: Disable verify key");
#endif

#define USE_SOURCE_RESOLVER 1
//#define ASSERT_ON_BAD_VIDEO_FILES 1

namespace rage
{
	// PURPOSE: Structure to wrap some common data for an audio buffer
	struct AudioBufferContext
	{
		static u32 const	ms_sampleSizeBytes = MediaCommon::sc_bytesPerAudioSample;

		IMFMediaBuffer*		m_audioBuffer;
		LONGLONG			m_timestampOneHNs;
		LONGLONG			m_durationOneHNs;
		u32					m_bufferSize;
		u32					m_readOffset;

		AudioBufferContext()
			: m_audioBuffer( NULL )
			, m_timestampOneHNs( 0 )
			, m_durationOneHNs( 0 )
			, m_bufferSize( 0 )
			, m_readOffset( 0 )
		{

		}

		inline bool CalculateReadOffsetForTimestampOneHNs( u64 const targetTimestampOneHNs, u32& out_result )
		{
			bool const c_containsTimestamp = ContainsAudioForTimestampOneHNs( targetTimestampOneHNs );

			u64 const c_bufferRelativeTimestampOneHns = targetTimestampOneHNs - m_timestampOneHNs;
			float const c_offsetRaw = c_containsTimestamp ? ( ( m_bufferSize / (float)m_durationOneHNs ) * c_bufferRelativeTimestampOneHns ) -0.5f : 0.f;

			out_result = (u32)c_offsetRaw;
			out_result += out_result % ms_sampleSizeBytes;

			return c_containsTimestamp;
		}

		bool ContainsAudioForTimestampOneHNs( u64 const targetTimestampOneHNs )
		{
			u64 const c_endTimeOneHNs = (u64)m_timestampOneHNs + m_durationOneHNs;
			return targetTimestampOneHNs >= (u64)m_timestampOneHNs && targetTimestampOneHNs < c_endTimeOneHNs;
		}

		void JumpReadOffsetToTimestampOneHNs( u64 const targetTimestampOneHNs )
		{
			u32 newReadOffset = 0;
			if( CalculateReadOffsetForTimestampOneHNs( targetTimestampOneHNs, newReadOffset ) )
			{
				m_readOffset = newReadOffset;
			}
		}

		inline bool HasBuffer()
		{
			return m_audioBuffer != NULL;
		}

		inline bool IsValidForReading()
		{
			return HasBuffer() && m_readOffset < m_bufferSize;
		}

		inline void Reset()
		{
			ReleaseMediaObject( m_audioBuffer );
			m_bufferSize		= 0;
			m_readOffset		= 0;
			m_timestampOneHNs	= 0;
			m_durationOneHNs	= 0;
		}
	};

	inline static bool CreateSourceReader( wchar_t const * const uri, IMFAttributes* attributes, IMFSourceReader*& out_reader )
	{
		bool success = false;

#if defined(USE_SOURCE_RESOLVER) && USE_SOURCE_RESOLVER

		IMFSourceResolver* sourceResolver = NULL;
		HRESULT hr = MFCreateSourceResolver( &sourceResolver );

		if( videoVerifyf( SUCCEEDED(hr), "MediaDecoder::CreateSourceReader - MFCreateSourceResolver failed with 0x%08lx, URI: %ls", hr, uri ) )
		{
			MF_OBJECT_TYPE resultingObjectType;
			IUnknown* mediaSourceUnknown = NULL;

			hr = sourceResolver->CreateObjectFromURL( uri, MF_RESOLUTION_MEDIASOURCE | MF_RESOLUTION_READ, NULL, &resultingObjectType, &mediaSourceUnknown );
			bool const c_wasSuccessful = SUCCEEDED(hr);

#if defined(ASSERT_ON_BAD_VIDEO_FILES) && ASSERT_ON_BAD_VIDEO_FILES
			videoAssertf( c_wasSuccessful, "MediaDecoder::CreateSourceReader - CreateObjectFromURL failed with 0x%08lx, URI: %ls.\n"
				"It's valid for this to fail if the named video was not created by the game, or is leftover from an export session that "
				"terminated in a non-standard way.", hr, uri );
#else
			videoCondLogf( !c_wasSuccessful, DIAG_SEVERITY_WARNING, "MediaDecoder::CreateSourceReader - CreateObjectFromURL failed with 0x%08lx, URI: %ls.\n"
				"It's valid for this to fail if the named video was not created by the game, or is leftover from an export session that "
				"terminated in a non-standard way.", hr, uri );
#endif

			if( c_wasSuccessful &&
				resultingObjectType == MF_OBJECT_MEDIASOURCE )
			{
				IMFMediaSource* mediaSource = static_cast<IMFMediaSource*>(mediaSourceUnknown);
				hr = MFCreateSourceReaderFromMediaSource( mediaSource, attributes, &out_reader );

				success = videoVerifyf( SUCCEEDED(hr), "MediaDecoder::CreateSourceReader - MFCreateSourceReaderFromMediaSource failed with 0x%08lx, URI: %ls", 
					hr, uri );
			}

			ReleaseMediaObject( mediaSourceUnknown );
		}

		ReleaseMediaObject( sourceResolver );
#else

		HRESULT hr = MFCreateSourceReaderFromURL( wideUri, readerAttributes, &out_reader );
		success = videoVerifyf( SUCCEEDED(hr), "MediaDecoder::CreateSourceReader - MFCreateSourceReaderFromURL failed with 0x%08lx, URI: %ls", hr, wideUri );

#endif

		if( !success )
		{
			ReleaseMediaObject( out_reader );
		}

		return success;
	}

	// PURPOSE: Prepares the underlying encoding API and returns a number of objects as out parameters that we need for encoding.
	// PARAMS:
	//		uri - URI to the video we are going to decode
	//		out_reader - Out pointer to a IMFSourceReader object, used for reading the video file
	//		out_videoStreamIndex - Index used to identify which stream in our file should contain video data
	//		out_audioStreamIndex - Index used to identify which stream in our file should contain audio data
	// RETURNS:
	//		True if successful, false otherwise.
	inline static bool PrepareForReading( const char * const uri, IMFSourceReader*& out_reader,
										DWORD& out_videoStreamIndex, DWORD& out_audioStreamIndex, bool const noAudio )
	{
		bool success = false;
		HRESULT hr = S_OK;

		if( videoVerifyf( uri, "MediaDecoder::PrepareForReading - No URI proided for reading!" ) &&
			videoVerifyf( out_reader == NULL, "MediaDecoder::PrepareForReading - IMFSourceReader is not NULL!" ) )
		{
			IMFAttributes* readerAttributes( NULL );
			if( MediaCommon::PrepareAttributes( readerAttributes, NULL, true ) )
			{
				USES_CONVERSION;
				wchar_t const * const wideUri = UTF8_TO_UTF16(uri);

                if( CreateSourceReader( wideUri, readerAttributes, out_reader ) )
                {
                    GUID mediaTypeGuid;
                    IMFMediaType* mediaType = NULL;

                    bool hasVideoStream( false );
                    bool hasAudioStream( noAudio ); // No audio? Pretend like we already have the audio stream sorted.

                    // We only support files with two streams
                    for( u32 streamIndex = 0; hr == S_OK; ++streamIndex )
                    {
                        // Get the video stream, to ensure we can use it
                        hr = out_reader->GetNativeMediaType( streamIndex, 0, &mediaType );
                        if( SUCCEEDED(hr) && mediaType )
                        {   
                            hr = mediaType->GetMajorType( &mediaTypeGuid );

                            if( !hasVideoStream && IsEqualGUID( mediaTypeGuid, MFMediaType_Video ) )
                            {
                                out_videoStreamIndex = streamIndex;
                                hasVideoStream = true;
                            }
                            else if( !hasAudioStream && IsEqualGUID( mediaTypeGuid, MFMediaType_Audio ) )
                            {
                                out_audioStreamIndex = streamIndex;
                                hasAudioStream = true;
                            }
                            else
                            {
                                out_reader->SetStreamSelection( streamIndex, FALSE );
                            }

                            ReleaseMediaObject( mediaType );
                        }
                    }

                    success = hasVideoStream && hasAudioStream;
                }
			}

			ReleaseMediaObject( readerAttributes );
		}

		if( !success )
		{
			ReleaseMediaObject( out_reader );
		}

		return success;
	}

	// PURPOSE: Flush the relevant streams on the reader object
	// PARAMS:
	//		reader - IMFSourceReader object doing the decoding we want to close down.
	void FlushReading( IMFSourceReader& reader )
	{
		HRESULT hr;
		hr = reader.Flush( (DWORD)MF_SOURCE_READER_ALL_STREAMS );
		Displayf( "FlushReading - IMFSourceReader::Flush - Completed with 0x%08lx", hr );
	}

	// PURPOSE: Flush the relevant streams on the reader object
	// PARAMS:
	//		reader - IMFSourceReader object doing the decoding we want to close down.
	void FlushAudioReading( IMFSourceReader& reader )
	{
		HRESULT hr;
		hr = reader.Flush( (DWORD)MF_SOURCE_READER_FIRST_AUDIO_STREAM );
		Displayf( "FlushAudioReading - IMFSourceReader::Flush - Completed with 0x%08lx", hr );
	}

	// PURPOSE: Close down and clean-up the given reading elements.
	// PARAMS:
	//		reader - IMFSourceReader object doing the decoding we want to close down.
	void CleanupReading( IMFSourceReader*& reader )
	{
		ReleaseMediaObject( reader );
	}

#if RSG_PC

	static const char* GetStringOfValueAtFirstValidDigit(const char* string, rage::u8 stringLength)
	{
		const char* valueString = string;

		// only check up to last digit, as we need something by the end of it even if it's zero
		for (rage::u8 i = 0; i < stringLength-1; ++i)
		{
			if (valueString[0] == '0')
				++valueString;
			else
				break;
		}

		return valueString;
	}

	static bool VerifyAgainstMetadata( IMFSourceReader& reader, MediaDecoderFormatInfo& formatInfo, const char *path, bool &hasModdedContent )
	{

#if !__FINAL
		if (PARAM_disableDecodeVerify.Get())
		{
			// if we're not checking, then just return true
			return true;
		}
#endif		
		USES_CONVERSION;

		char* keyBuffer = NULL;
		char* titleBuffer = NULL;
		rage::u32 checksumMeta = 0;
		rage::u64 durationMsFromMeta = 0;

		bool extractedMetaData = false;
		bool metaDataValid = false;
		hasModdedContent = false;

		HRESULT hr = S_OK;
		if( MediaCommon::IsMp4Supported() && formatInfo.m_videoFormat == MediaCommon::VIDEO_FORMAT_H264 )
		{
			IPropertyStore* pPropertyStore = NULL;

			hr = reader.GetServiceForStream( static_cast<DWORD>(MF_SOURCE_READER_MEDIASOURCE), MF_PROPERTY_HANDLER_SERVICE, IID_PPV_ARGS(&pPropertyStore));
			if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - IMFSourceReader::GetServiceForStream "
				"failed with 0x%08lx", hr ) )
			{
				// need to read metadata first. 
				// You must release property stores quickly
				// http://blogs.msdn.com/b/benkaras/archive/2006/10/13/gotcha-you-must-release-property-stores-quickly.aspx

				PROPVARIANT variant;

				// get resolvable key from metadata
				pPropertyStore->GetValue(PKEY_Media_UniqueFileIdentifier, &variant);
				LPCWSTR initialKey = PropVariantToStringWithDefault(variant, NULL);

				keyBuffer = UTF16_TO_UTF8(initialKey);
				PropVariantClear( &variant );

				if( keyBuffer )
				{
					// get checksum from metadata
					pPropertyStore->GetValue(PKEY_Identity_UniqueID, &variant);
					LPCWSTR checksumString = PropVariantToStringWithDefault(variant, NULL);

					char const * const checksumBuffer = UTF16_TO_UTF8(checksumString);
					PropVariantClear( &variant );

					if ( checksumBuffer )
					{
						checksumMeta = strtol(checksumBuffer, NULL, 0);

						// get title name from metadata
						pPropertyStore->GetValue(PKEY_Title, &variant);
						LPCWSTR titleString = PropVariantToStringWithDefault(variant, NULL);
						titleBuffer = UTF16_TO_UTF8(titleString);
						PropVariantClear( &variant );

						// check flag for game being tampered in recording
						// only set if tampering has occurred. since this was put in post release
						// consider moving to key in future
						pPropertyStore->GetValue(PKEY_Media_AverageLevel, &variant);
						u32 tamperFlag = 0;
						PropVariantToUInt32(variant, &tamperFlag);
						hasModdedContent = tamperFlag ? true : false;
						PropVariantClear( &variant );

						if( titleBuffer )
						{
							// get duration from metadata
							pPropertyStore->GetValue(PKEY_Media_Duration, &variant);
							PropVariantToUInt64(variant, &durationMsFromMeta);
							PropVariantClear( &variant );

							if (durationMsFromMeta > 0)
							{
								// returns 100ns units, not milliseconds ...convert to ms
								durationMsFromMeta /= 10000;

								// That's the last of it!
								extractedMetaData = true;					
							}
							else
							{
								videoDisplayf("VerifyAgainstMetadata - No duration");
							}
						}
						else
						{
							videoDisplayf("VerifyAgainstMetadata - No title");
						}

					}
					else
					{
						videoDisplayf("VerifyAgainstMetadata - No checksum");
					}
				}
				else
				{
					videoDisplayf("VerifyAgainstMetadata - No Key Buffer");
				}
			}

			// release property store
			ReleaseMediaObject( pPropertyStore );
		}
		else if( MediaCommon::IsWmvSupported() && formatInfo.m_videoFormat == MediaCommon::VIDEO_FORMAT_WMV  )
		{
			IMFMetadataProvider* pMetaDataProvider = NULL; 
			hr = reader.GetServiceForStream( static_cast<DWORD>(MF_SOURCE_READER_MEDIASOURCE), GUID_NULL, IID_PPV_ARGS(&pMetaDataProvider) );
			if( videoVerifyf( SUCCEEDED(hr), "WriteMetadata - IMFSourceReader::GetServiceForStream "
				"failed with 0x%08lx", hr ) )
			{
				IMFMetadata* pMetadata = NULL;
				hr = pMetaDataProvider->GetMFMetadata( NULL, 0, 0, &pMetadata );
				if( videoVerifyf( SUCCEEDED(hr), "WriteMetadata - IMFSourceReader::GetMFMetadata "
					"failed with 0x%08lx", hr ) )
				{
					#define GUID_BUFFER_SIZE (64)
					wchar_t propKeyNameBuffer[GUID_BUFFER_SIZE];
					PROPVARIANT variant;

					// get resolvable key from metadata
					PSStringFromPropertyKey( PKEY_Media_UniqueFileIdentifier, propKeyNameBuffer, GUID_BUFFER_SIZE );
					pMetadata->GetProperty( propKeyNameBuffer, &variant);
					LPCWSTR initialKey = PropVariantToStringWithDefault(variant, NULL);

					keyBuffer = UTF16_TO_UTF8(initialKey);
					PropVariantClear( &variant );

					if( keyBuffer )
					{
						// get checksum from metadata
						PSStringFromPropertyKey( PKEY_Identity_UniqueID, propKeyNameBuffer, GUID_BUFFER_SIZE );
						pMetadata->GetProperty( propKeyNameBuffer, &variant );
						LPCWSTR checksumString = PropVariantToStringWithDefault(variant, NULL);

						char const * const checksumBuffer = UTF16_TO_UTF8(checksumString);
						PropVariantClear( &variant );

						if ( checksumBuffer )
						{
							checksumMeta = strtol(checksumBuffer, NULL, 0);

							// get title name from metadata
							PSStringFromPropertyKey( PKEY_Title, propKeyNameBuffer, GUID_BUFFER_SIZE );
							pMetadata->GetProperty( propKeyNameBuffer, &variant );
							LPCWSTR titleString = PropVariantToStringWithDefault(variant, NULL);
							titleBuffer = UTF16_TO_UTF8(titleString);
							PropVariantClear( &variant );


							// check flag for game being tampered in recording
							// only set if tampering has occurred. since this was put in post release
							// consider moving to key in future
							PSStringFromPropertyKey(PKEY_Media_AverageLevel, propKeyNameBuffer, GUID_BUFFER_SIZE);
							pMetadata->GetProperty( propKeyNameBuffer, &variant );
							u32 tamperFlag = 0;
							PropVariantToUInt32(variant, &tamperFlag);
							hasModdedContent = tamperFlag ? true : false;
							PropVariantClear( &variant );

							if( titleBuffer )
							{
								// get duration from metadata
								PSStringFromPropertyKey( PKEY_Media_Duration, propKeyNameBuffer, GUID_BUFFER_SIZE );
								pMetadata->GetProperty( propKeyNameBuffer, &variant );
								PropVariantToUInt64(variant, &durationMsFromMeta);
								PropVariantClear( &variant );

								if (durationMsFromMeta > 0)
								{
									// returns 100ns units, not milliseconds ...convert to ms
									durationMsFromMeta /= 10000;

									// That's the last of it!
									extractedMetaData = true;					
								}
								else
								{
									videoDisplayf("VerifyAgainstMetadata - No duration");
								}
							}
							else
							{
								videoDisplayf("VerifyAgainstMetadata - No title");
							}

						}
						else
						{
							videoDisplayf("VerifyAgainstMetadata - No checksum");
						}
					}
					else
					{
						videoDisplayf("VerifyAgainstMetadata - No Key Buffer");
					}
				}

				ReleaseMediaObject( pMetadata );
			}

			ReleaseMediaObject( pMetaDataProvider );
		}

		if( extractedMetaData )
		{
			static const rage::u8 keyLength = 100;
			static const rage::u8 digitLength = 10;

			// shuffle index value list same as key was
			rage::u8 shuffle[keyLength] = { 0 };
			rage::u32 w = 4;
			rage::u32 z = 6;

			for (rage::u8 i = 0; i < keyLength; ++i)
				shuffle[i] = i;

			// uses same shuffle routine and random numbers
			rage::u32 random = 0;
			double randomFloat = 0;
			for (rage::u32 i = keyLength - 1; i > 0; i--)
			{
				z = 36969 * (z & 65535) + (z >> 16);
				w = 18000 * (w & 65535) + (w >> 16);

				random = (z << 16) + w;
				randomFloat = (random + 1.0) * 2.328306435454494e-10;

				rage::u8 j = (rage::u8) (randomFloat*(i+1));
				rage::u8 t = shuffle[j];
				shuffle[j] = shuffle[i];
				shuffle[i] = t;
			}

			// find index values in shuffle list, and make a new list that we can rebuild the key from
			rage::u8 indices[keyLength] = { 0 };
			for (rage::u8 i = 0; i < keyLength; ++i)
			{
				for (rage::u8 j = 0; j < keyLength; ++j)
				{
					if (shuffle[j] == i)
					{
						indices[i] = j;
						break;
					}
				}
			}

			// getstring from metadata and rearrange with index list
			char rebuiltKey[keyLength] = { 0 };
			for (rage::u32 i = 0; i < keyLength; ++i)
			{
				rebuiltKey[i] = keyBuffer[indices[i]];
			}

			// checksum
			// use first 80 chars. up to 64 can be valid. half the random characters will get used. 
			wchar_t* checksum = reinterpret_cast<wchar_t*>(rebuiltKey);
			rage::u32  checksumGenerated = 0;
			// pick a weird number to wrap around on, rather than UINT_MAX, to make it tricker
			const rage::u32 wrapAround = (UINT_MAX / 11) * 9;
			for (rage::u32 i = 0; i < 40; ++i)
			{
				checksumGenerated += static_cast<rage::u32>(checksum[i]);

				if (checksumGenerated > wrapAround)
					checksumGenerated -= wrapAround;
			}

			// check against metadata
			if ( checksumMeta == checksumGenerated )
			{
				// get title name length, and then the title name
				rage::u8 titleNameLength = (rebuiltKey[30]- 48)*10 + (rebuiltKey[31] - 48);
				rebuiltKey[32+titleNameLength] = 0;

				// titlename checks
				rage::u8 titleBufferLength = static_cast<rage::u8>(strlen(titleBuffer));
				const rage::u32 maxKeyTitleNameLength = 32;
				if (titleBufferLength > maxKeyTitleNameLength)
					titleBufferLength = maxKeyTitleNameLength;

				if( titleBufferLength == titleNameLength )
				{
					// read remaining values backwards, so we can make strings easily by switcking in 0s to end strings
					rebuiltKey[30] = 0;
					const char* valueString = GetStringOfValueAtFirstValidDigit(&rebuiltKey[20], digitLength);

					// if the duration is zero, then we may as well fail here
					if( valueString[0] != '0' )
					{
						// looks like frame-rates can cause the estimated and final durations to be way out. ignore 
						// ...but this is how it used to be done

						//			rage::u32 durationMsFromKey = static_cast<rage::u32>(strtol(valueString, NULL, 0));

						// this is going to have to be more of a ball park check, as duration is taken from estimate
						// 			if (durationMsFromKey >= durationMsFromMeta + 300
						// 				|| durationMsFromKey <= durationMsFromMeta - 300)
						// 			{
						// 				videoDisplayf("VerifyAgainstMetadata - duration fail (%d - %d)", durationMsFromKey, durationMsFromMeta);
						// 				return false;
						// 			}


						// creation time is another ball-park check as it's taken at the start of encoding, since we can't get an 
						// accurate file time into our metadata key
						HANDLE hFile = CreateFileW(reinterpret_cast<const wchar_t*>(UTF8_TO_WIDE(path)), GENERIC_READ, FILE_SHARE_READ,
							NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );	

						if (hFile != INVALID_HANDLE_VALUE)
						{
							FILETIME modifiedFT;
							bool getTime = ( GetFileTime(hFile, NULL, NULL, &modifiedFT)!=0 );
							CloseHandle(hFile);

							if( getTime )
							{
								videoDisplayf("VerifyAgainstMetadata - Create Time - Key - %s", rebuiltKey);

								FILETIME metaFT;
								rebuiltKey[20] = 0;
								const char* lowString =  GetStringOfValueAtFirstValidDigit(&rebuiltKey[10], digitLength);
								metaFT.dwLowDateTime = strtoul(lowString, NULL, 0);
								videoDisplayf("VerifyAgainstMetadata - rebuiltKey - Low - %s", lowString);

								rebuiltKey[10] = 0;
								const char* highString =  GetStringOfValueAtFirstValidDigit(&rebuiltKey[0], digitLength);
								metaFT.dwHighDateTime = strtoul(highString, NULL, 0);
								videoDisplayf("VerifyAgainstMetadata - rebuiltKey - High - %s", highString);

								rage::u64 fileTime = ( (rage::u64)(modifiedFT.dwHighDateTime) << 32 ) + modifiedFT.dwLowDateTime;
								rage::u64 metaTime = ( (rage::u64)(metaFT.dwHighDateTime) << 32 ) + metaFT.dwLowDateTime;

								videoDisplayf("VerifyAgainstMetadata - modifiedFT - High - %u", modifiedFT.dwHighDateTime);
								videoDisplayf("VerifyAgainstMetadata - modifiedFT - Low. - %u", modifiedFT.dwLowDateTime);

								videoDisplayf("VerifyAgainstMetadata - metaFT - High - %u", metaFT.dwHighDateTime);
								videoDisplayf("VerifyAgainstMetadata - metaFT - Low. - %u", metaFT.dwLowDateTime);

								// 1 minute ball park for creation time
								// we take the modified time just before the file is finalised. 
								// creation time doesn't seem to change, even if we delete a file ?!?!
								if ( fileTime > metaTime + (static_cast<rage::u64>(10000000) * 60) ||
									fileTime < metaTime )
								{
									videoDisplayf("VerifyAgainstMetadata - creation time fail");
								}
								else
								{
									metaDataValid = true;
								}
							}
						}
					}
					else
					{
						videoDisplayf("VerifyAgainstMetadata - key duration is zero");
					}
				}
				else
				{
					videoDisplayf("VerifyAgainstMetadata - title fail");
				}	
			}
			else
			{
				videoDisplayf("VerifyAgainstMetadata - checksum fail");
			}
		}

		videoWarningf( "MediaDecoder_win::VerifyAgainstMetadata - Meta-data for file %s was %s", path, metaDataValid ? "valid" : "invalid" );
		return metaDataValid;
	}

#endif // RSG_PC

	// PURPOSE: Populates a format info structure based on the given IMFSourceReader
	// PARAMS:
	//		reader				- IMFSourceReader to grab format details from
	//		videoStreamIndex	- Index of the video stream to grab video format details from 
	//		audioStreamIndex	- Index of the audio stream to grab audio format details from
	//		out_formatInfo		- FormatInfo structure to populate
	// RETURNS:
	//		True if we successfully populated all of the structure, false otherwise
	static bool PopulateFormatInfo( IMFSourceReader& reader, DWORD const videoStreamIndex, 
		DWORD const audioStreamIndex, MediaDecoderFormatInfo& out_formatInfo, bool const noAudio )
	{
		bool success = false;

		out_formatInfo.Reset();

		HRESULT hr = S_OK;
		IMFMediaType* mediaType = NULL;
		GUID scratchGuid;

		//! Video format info
		hr = reader.GetNativeMediaType( videoStreamIndex, 0, &mediaType );
		if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - IMFSourceReader::GetNativeMediaType (video) failed with 0x%08lx", hr ) )
		{
			hr = mediaType->GetMajorType( &scratchGuid ); 
			if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetMajorType (video) - failed with 0x%08lx", hr ) && 
				videoVerifyf( IsEqualGUID( scratchGuid, MFMediaType_Video ) == TRUE, "PopulateFormatInfo - Video Stream %u is not media type video?", videoStreamIndex ) )
			{
				hr = MFGetAttributeSize( mediaType, MF_MT_FRAME_SIZE, &out_formatInfo.m_displayWidth, &out_formatInfo.m_displayHeight );
				success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - MFGetAttributeSize - MF_MT_FRAME_SIZE failed with 0x%08lx", hr );

				hr = mediaType->GetGUID( MF_MT_SUBTYPE, &scratchGuid );
				success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;
				out_formatInfo.m_videoFormat = MediaCommon::ConvertGUIDToVideoFormat( scratchGuid );

				u32 numerator, denominator;
				hr = MFGetAttributeRatio( mediaType, MF_MT_FRAME_RATE, &numerator, &denominator );   
				success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - MFSetAttributeSize - MF_MT_FRAME_RATE failed with 0x%08lx", hr ) && success;
				MediaCommon::ConvertNumeratorAndDenominatorToFPS( out_formatInfo.m_frameRate, numerator, denominator );
			}

			ReleaseMediaObject( mediaType );
		}

		// Audio format info
        if( !noAudio )
        {
            hr = reader.GetNativeMediaType( audioStreamIndex, 0, &mediaType );
            if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - IMFSourceReader::GetNativeMediaType (audio) failed with 0x%08lx", hr ) )
            {
                hr = mediaType->GetMajorType( &scratchGuid ); 
                if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetMajorType (audio) - failed with 0x%08lx", hr ) && 
                    videoVerifyf( IsEqualGUID( scratchGuid, MFMediaType_Audio ) == TRUE, 
                    "PopulateFormatInfo - Audio Stream %u is not media type audio?", audioStreamIndex ) )
                {
                    hr = mediaType->GetGUID( MF_MT_SUBTYPE, &scratchGuid );
                    success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetGUID - MF_MT_SUBTYPE failed with 0x%08lx", hr ) && success;
                    out_formatInfo.m_audioFormat = MediaCommon::ConvertGUIDToAudioFormat( scratchGuid );

                    hr = mediaType->GetUINT32( MF_MT_AUDIO_NUM_CHANNELS, &out_formatInfo.m_audioChannels );
                    success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetUINT32 - MF_MT_AUDIO_NUM_CHANNELS failed with 0x%08lx", hr ) && success;

                    hr = mediaType->GetUINT32( MF_MT_AUDIO_BITS_PER_SAMPLE, &out_formatInfo.m_audioBitsPerSample );
                    success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetUINT32 - MF_MT_AUDIO_BITS_PER_SAMPLE failed with 0x%08lx", hr ) && success;

                    hr = mediaType->GetUINT32( MF_MT_AUDIO_SAMPLES_PER_SECOND, &out_formatInfo.m_audioSampleRate );
                    success = videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - GetUINT32 - MF_MT_AUDIO_SAMPLES_PER_SECOND failed with 0x%08lx", hr ) && success;

                }

                ReleaseMediaObject( mediaType );
            }
        }

		// Presentation details
		PROPVARIANT var;
		hr = reader.GetPresentationAttribute( (DWORD)MF_SOURCE_READER_MEDIASOURCE, MF_PD_DURATION, &var);
		if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - IMFSourceReader::GetPresentationAttribute MF_PD_DURATION failed with 0x%08lx", hr ) )
		{
			LONGLONG oneHNsDuration(0);
			if( var.vt == VT_UI8 )
			{
				oneHNsDuration = var.hVal.QuadPart;
				success = true;
			}
			PropVariantClear( &var );

			out_formatInfo.m_durationNs = ONE_H_NS_UNITS_TO_NANOSECONDS( oneHNsDuration );
		}

		hr = reader.GetPresentationAttribute( (DWORD)MF_SOURCE_READER_MEDIASOURCE, MF_PD_TOTAL_FILE_SIZE, &var);
		if( videoVerifyf( SUCCEEDED(hr), "PopulateFormatInfo - IMFSourceReader::GetPresentationAttribute "
			"MF_PD_TOTAL_FILE_SIZE failed with 0x%08lx", hr ) )
		{
			if( var.vt == VT_UI8)
			{
				out_formatInfo.m_fileSize = var.uhVal.QuadPart;
				success = true;
			}
			PropVariantClear( &var );
		}

		return success;
	}

	static bool GenerateAudioBuffer( MediaDecoderFormatInfo const& formatInfo, audReferencedRingBuffer*& out_ringBuffer )
	{
		bool success = false;

		if( formatInfo.IsValid() &&
			videoVerifyf( out_ringBuffer == NULL, "GenerateAudioBuffer - Audio Ring buffer already initialized" ) )
		{
			out_ringBuffer = rage_new audReferencedRingBuffer();
			if( videoVerifyf( out_ringBuffer, "GenerateAudioBuffer - Failed to create audReferencedRingBuffer for decoding video" ) )
			{
				size_t const audioBufferSize = formatInfo.GetAudioBufferSize();
				u8* audioBuffer = rage_aligned_new(16) u8[ audioBufferSize ];

				success = videoVerifyf( audioBuffer != NULL, "GenerateAudioBuffer - Unable to allocate audio buffer of size %u"
					SIZETFMT ) && videoVerifyf( out_ringBuffer->Init( audioBuffer, audioBufferSize, true ), "GenerateAudioBuffer - "
					"failed to init ring buffer" );
			}
		}

		return success;
	}

	static void FillBufferWithSilence( audReferencedRingBuffer& out_ringBuffer )
	{
		out_ringBuffer.Lock();

		u32 const c_maxStackBytes = 1024;
		char dummyData[ c_maxStackBytes ];
		sysMemSet( dummyData, 0, c_maxStackBytes );

		u32 bytesInBuffer = out_ringBuffer.GetBytesAvailableToWrite();
		while( bytesInBuffer > 0 )
		{
			u32 const c_bytesToWrite = Min( c_maxStackBytes, bytesInBuffer );
			out_ringBuffer.WriteData( dummyData, c_bytesToWrite );

			bytesInBuffer = out_ringBuffer.GetBytesAvailableToWrite();
		}

		out_ringBuffer.Unlock();
	}

	static void DrainAudioBuffer( audReferencedRingBuffer& out_ringBuffer )
	{
		out_ringBuffer.Lock();

		u32 const c_maxStackBytes = 1024;
		char scratchSpace[ c_maxStackBytes ];

		u32 bytesInBuffer = out_ringBuffer.GetBytesAvailableToRead();
		while( bytesInBuffer > 0 )
		{
			u32 const c_bytesToRead = Min( c_maxStackBytes, bytesInBuffer );
			out_ringBuffer.ReadData( scratchSpace, c_bytesToRead );

			bytesInBuffer = out_ringBuffer.GetBytesAvailableToRead();
		}

		FillBufferWithSilence( out_ringBuffer );

		out_ringBuffer.Unlock();
	}

	static bool ApplyGameCompatibleDecodingParameters( IMFSourceReader& reader, 
							DWORD const videoStreamIndex, DWORD const audioStreamIndex, bool const noAudio )
	{
		bool success = false;
		HRESULT hr = S_OK;

		//! First grab a game compatible video media type and set that...
		IMFMediaType* videoMediaType( NULL );
		hr = MFCreateMediaType( &videoMediaType );

		if( videoVerifyf( SUCCEEDED(hr), "ApplyGameCompatibleDecodingParameters - MFCreateMediaType - failed with 0x%08lx", hr ) )
		{
			if( videoVerifyf( MediaCommon::PrepareGameCompatibleInputVideoMediaType( videoMediaType ), "ApplyGameCompatibleDecodingParameters - Unable to generate game compatible video media type" ) )
			{
				hr = reader.SetCurrentMediaType( videoStreamIndex, NULL, videoMediaType );
				if( videoVerifyf( SUCCEEDED(hr), "ApplyTargetMediaTypes - SetCurrentMediaType (video) - failed with 0x%08lx", hr ) )
				{
					if( noAudio )
					{
						success = true;
					}
					else
					{
						// ... now do the same for audio.
						IMFMediaType* audioMediaType( NULL );
						hr = MFCreateMediaType( &audioMediaType );

						if( videoVerifyf( SUCCEEDED(hr), "ApplyGameCompatibleDecodingParameters - MFCreateMediaType - failed with 0x%08lx", hr ) )
						{
							if( videoVerifyf( MediaCommon::PrepareGameCompatibleInputAudioMediaType( audioMediaType ), 
								"ApplyGameCompatibleDecodingParameters - Unable to generate game compatible audio media type" ) )
							{
								hr = reader.SetCurrentMediaType( audioStreamIndex, NULL, audioMediaType );
								if( videoVerifyf( SUCCEEDED(hr), "ApplyGameCompatibleDecodingParameters - SetCurrentMediaType (audio) - failed with 0x%08lx", hr ) )
								{
									success = true;
								}
							}

							ReleaseMediaObject( audioMediaType );
						}
					}
				}
			}

			ReleaseMediaObject( videoMediaType );
		}
		
		return success;
	}

	static void PopulateDisplayFrame( IMFSample& mediaSample, MediaDecoderFormatInfo const& formatInfo, MediaDecoderDisplayFrame& out_frame )
	{
		if( videoVerifyf( out_frame.IsInitialized(), "PopulateDisplayFrame - Invalid frame given" ) )
		{
			LONGLONG sampleTimestamp(0);
			HRESULT hr = mediaSample.GetSampleTime( &sampleTimestamp );
			videoAssertf( SUCCEEDED(hr), "PopulateDisplayFrame - IMFSample::GetSampleTime - failed with 0x%08lx", hr );

			IMFMediaBuffer* mediaBuffer( NULL );
			hr = mediaSample.ConvertToContiguousBuffer( &mediaBuffer );

			if( videoVerifyf( SUCCEEDED(hr), "PopulateDisplayFrame - IMFSample::ConvertToContiguousBuffer - failed with 0x%08lx", hr ) )
			{
				u8* mediaBufferInternal( NULL );
				DWORD mediaBufferSize( 0 );
				DWORD mediaBufferUsedSize( 0 );


				hr = mediaBuffer->Lock( &mediaBufferInternal, &mediaBufferSize, &mediaBufferUsedSize );
				if( videoVerifyf( SUCCEEDED(hr), "PopulateDisplayFrame - IMFMediaBuffer::Lock - failed with 0x%08lx", hr ) )
				{
					if( mediaBufferInternal && mediaBufferUsedSize > 0 )
					{
						out_frame.CopyNV12IntoFrame( mediaBufferInternal, mediaBufferUsedSize, formatInfo );
						out_frame.SetId( sampleTimestamp );
					}

					mediaBuffer->Unlock();
				}
			}

			ReleaseMediaObject( mediaBuffer );
		}
	}

	static bool ExtractContiguousMediaBuffer( IMFSample& in_mediaSample, IMFMediaBuffer*& out_mediaBuffer, u32& out_sizeBytes,
		bool const invertSamples, u32 const sampleSizeBytes )
	{
		videoAssertf( out_mediaBuffer == NULL, "ExtractContiguousMediaBuffer - Expected NULL media buffer! About to leak memory..." );

		out_mediaBuffer = NULL;
		HRESULT hr = in_mediaSample.ConvertToContiguousBuffer( &out_mediaBuffer );
		videoAssertf( SUCCEEDED(hr), "ExtractContiguousMediaBuffer - IMFSample::ConvertToContiguousBuffer - failed with 0x%08lx", hr );

		if( out_mediaBuffer )
		{
			u8* mediaBufferInternal( NULL );
			DWORD mediaBufferSize( 0 );
			DWORD mediaBufferUsedSize( 0 );

			hr = out_mediaBuffer->Lock( &mediaBufferInternal, &mediaBufferSize, &mediaBufferUsedSize );
			if( videoVerifyf( SUCCEEDED(hr), "ExtractContiguousMediaBuffer - IMFMediaBuffer::Lock - failed with 0x%08lx", hr ) )
			{
				out_sizeBytes = mediaBufferUsedSize;

				//! Inverse functionality, for backwards playback
				if( invertSamples && mediaBufferInternal && mediaBufferUsedSize > 0 && 
					videoVerifyf( mediaBufferUsedSize % sampleSizeBytes == 0, 
					"ExtractContiguousMediaBuffer - Unable to invert sample, as it doesn't match our sample size alignment!" ) )
				{
					u32 const c_elementCount =  ( mediaBufferUsedSize / sampleSizeBytes );

					//! We only support 16-bit samples at this time.
					if( sampleSizeBytes == 2 )
					{
						u16* start	= (u16*)mediaBufferInternal;
						u16* end	= start + c_elementCount;
						std::reverse( start, end );
					}
				}

				out_mediaBuffer->Unlock();
			}
		}

		return out_mediaBuffer != NULL;
	}

	static void PopulateAudioRingBuffer( AudioBufferContext& inout_bufferContext, audReferencedRingBuffer& inout_ringBuffer )
	{
		inout_ringBuffer.Lock();
		u32 const c_totalSpace = inout_ringBuffer.GetBufferSize();
		u32 const c_availableSpace = inout_ringBuffer.GetBytesAvailableToWrite();
		u32 const c_safeSpace = c_availableSpace > c_totalSpace ? 0 : c_availableSpace;

		if( inout_bufferContext.IsValidForReading() && c_safeSpace > 0 )
		{
			u8* mediaBufferInternal( NULL );
			DWORD mediaBufferSize( 0 );
			DWORD mediaBufferUsedSize( 0 );

			HRESULT hr = inout_bufferContext.m_audioBuffer->Lock( &mediaBufferInternal, &mediaBufferSize, &mediaBufferUsedSize );
			if( videoVerifyf( SUCCEEDED(hr), "PopulateAudioRingBuffer - IMFMediaBuffer::Lock - failed with 0x%08lx", hr ) )
			{
				if( mediaBufferInternal && mediaBufferUsedSize > 0 && inout_bufferContext.m_readOffset < mediaBufferUsedSize )
				{
					u32 const c_lockedBufferSizeRemaining = mediaBufferUsedSize - inout_bufferContext.m_readOffset;
					u32 const c_sizeToWrite = min( c_safeSpace, c_lockedBufferSizeRemaining );
					u32 const c_sizeWritten = inout_ringBuffer.WriteData( mediaBufferInternal + inout_bufferContext.m_readOffset, c_sizeToWrite );
					inout_bufferContext.m_readOffset += c_sizeWritten;
				}

				inout_bufferContext.m_audioBuffer->Unlock();
			}
		}
		inout_ringBuffer.Unlock();
	}

	static void UpdateAudioBufferContext( IMFSourceReader& reader, DWORD const audioStreamIndex, AudioBufferContext& inout_bufferContext,
		DWORD& out_statusFlags, LONGLONG& out_timestampOneHNs )
	{
		if( !inout_bufferContext.IsValidForReading() )
		{
			inout_bufferContext.Reset();
		}

		HRESULT hr( S_OK );
		IMFSample* sample( NULL );

		if( !inout_bufferContext.HasBuffer() )
		{
			hr = reader.ReadSample(
				audioStreamIndex,						// Stream index.
				0,										// Flags.
				NULL,									// Receives the actual stream index. 
				&out_statusFlags,						// Receives status flags.
				&inout_bufferContext.m_timestampOneHNs,	// Receives the time stamp.
				&sample );								// Receives the sample or NULL.

			if( videoVerifyf( SUCCEEDED(hr), "UpdateAudioStream - ReadSample failed with 0x%08lx", hr ) && sample )
			{
				hr = sample->GetSampleDuration( &inout_bufferContext.m_durationOneHNs );
				videoAssertf( SUCCEEDED(hr), "UpdateAudioStream - ReadSample failed with 0x%08lx", hr );

				videoVerifyf( ExtractContiguousMediaBuffer( *sample, inout_bufferContext.m_audioBuffer, inout_bufferContext.m_bufferSize, false, 2 ), 
					"UpdateAudioStream - Failed to extract contiguous media buffer!" );

				out_timestampOneHNs = inout_bufferContext.m_timestampOneHNs;
			}

			ReleaseMediaObject( sample );
		}
	}

	static void UpdateVideoStream( IMFSourceReader& reader, DWORD const videoStreamIndex, IMFSample*& out_sample, 
		DWORD& out_statusFlags, LONGLONG& out_timestampOneHNs )
	{
		IMFSample* newSample = NULL;

		HRESULT hr( S_OK );

		hr = reader.ReadSample(
			videoStreamIndex,		// Stream index.
			0,						// Flags.
			NULL,					// Receives the actual stream index. 
			&out_statusFlags,       // Receives status flags.
			&out_timestampOneHNs,	// Receives the time stamp.
			&newSample );			// Receives the sample or NULL.

		videoAssertf( SUCCEEDED(hr), "UpdateVideoStream - ReadSample failed with 0x%08lx", hr );

		// Swap the samples then release the old one if it wasn't consumed
		IMFSample* sample = (IMFSample*)sysInterlockedExchangePointer( (void *volatile *)&out_sample, newSample );
		ReleaseMediaObject( sample );
	}

	static bool SeekOnStreamOneHNs( IMFSourceReader& reader, u64 const seekTimeOneHNs )
	{
		bool success = false;

		PROPVARIANT var;

		var.vt = VT_I8;
		var.hVal.QuadPart = seekTimeOneHNs;

		HRESULT hr = reader.SetCurrentPosition(GUID_NULL, var);
		success = videoVerifyf( SUCCEEDED(hr), "SeekOnStream - SetCurrentPosition failed with 0x%08lx", hr );
		PropVariantClear(&var);

		return success;
	}

	static bool SeekOnStreamNs( IMFSourceReader& reader, u64 const seekTimeNs )
	{
		return SeekOnStreamOneHNs( reader, NANOSECONDS_TO_ONE_H_NS_UNITS( seekTimeNs ) );
	}

	MediaDecoder::MediaDecoder()
		: m_threadData()
		, m_decoderThread( NULL )
	{

	}

	// PURPOSE: Opens a video file and extracts some basic data if it is supported and created by us
	// RETURNS:
	//		Boolean indicating if we support this file
	bool MediaDecoder::PeekVideoDetails( const char * const path, u32& out_width, u32& out_height, u32& out_durationMs, bool* out_hasModdedContent )
	{
		bool supported = false;

		IMFSourceReader* sourceReader( NULL );
		DWORD videoStreamIndex;
		DWORD audioStreamIndex;

		PrepareForReading( path, sourceReader, videoStreamIndex, audioStreamIndex, false );
		MediaDecoderFormatInfo info;

		bool hasModdedContent = false;
		if( sourceReader && PopulateFormatInfo( *sourceReader, videoStreamIndex, audioStreamIndex, info, false ) &&
			info.IsValid() WIN32PC_ONLY( && VerifyAgainstMetadata( *sourceReader, info, path, hasModdedContent )  ) )
		{
			supported = true;
			out_width = info.m_displayWidth;
			out_height = info.m_displayHeight;
			out_durationMs = (u32)NANOSECONDS_TO_MILLISECONDS( info.m_durationNs );
			if (out_hasModdedContent)
				*out_hasModdedContent = hasModdedContent;
		}

		videoCondLogf( !info.IsValid(), DIAG_SEVERITY_WARNING, "MediaDecoder::PeekVideoDetails - File %s does not contain supported "
			"video content.", path );

		ReleaseMediaObject( sourceReader );

		return supported;
	}

	bool MediaDecoder::InitializePlatformSpecific( const char * const path, bool const captureThumbnailThenWait )
	{
		bool success = false;

		if( videoVerifyf( m_decoderThread == NULL, "MediaDecoder::InitializePlatformSpecific - Decoder already initialized" ) &&
			videoVerifyf( path, "MediaDecoder::InitializePlatformSpecific - Null path" ) )
		{
			m_threadData.Initialize( path, captureThumbnailThenWait );

			//! Flag as preparing so when the thread starts it knows what to do...
			SetDecoderStateDirect( DECODER_PREPARING );

			m_decoderThread = sysIpcCreateThread( &DecoderThreadFunc, &m_threadData, 
				65536, PRIO_BELOW_NORMAL, "MediaDecoderThread", 0, "MediaDecoderThread" );

			// ...but catch the case of thread creation failure to abandon ship
			if( m_decoderThread == NULL )
			{
				SetDecoderStateDirect( DECODER_INVALID );
			}

			success = IsInitialized();
		}

		return success;
	}

	void MediaDecoder::ShutdownPlatformSpecific()
	{
		if( m_decoderThread != NULL )
		{
			m_threadData.SetPendingPlayState( PLAYSTATE_CLEANUP );
			sysIpcWaitThreadExit( m_decoderThread );
		}
		else
		{
			videoDisplayf( "MediaDecoder::StopCaptureShutdownPlatformSpecific - "
				"Stopping playback when never started. Check logs for previous failure notifications..." );
		}

		m_decoderThread = NULL;
		SetDecoderStateDirect( DECODER_INVALID );
	}

	void MediaDecoder::UpdatePlatformSpecific()
	{
		eDecodeState const c_decodeState = GetDecoderState();
		if( c_decodeState == DECODER_MIN_DETAIL )
		{
			GenerateDisplayFramesPlatformSpecific();

			if( !m_threadData.m_noAudio )
			{
				GenerateAudioBuffer( GetFormatInfo(), m_threadData.m_audioBuffer );
			}

			// Set state direct, don#t want to wake our thread yet
			SetDecoderStateDirect( DECODER_BUFFERS_CREATED );
		}
		else if( c_decodeState == DECODER_ACTIVE )
		{
			PendingSampleType* sample = (PendingSampleType*)sysInterlockedExchangePointer( (void *volatile *)&m_threadData.m_pendingVideoSample, NULL );
			if( sample )
			{
				PopulateDisplayFrame( *sample, GetFormatInfo(), GetDisplayFrame() );
				ReleaseMediaObject( sample );
			}
		}
	}

	void MediaDecoder::CleanupPlatformSpecific()
	{
		if( m_threadData.m_audioBuffer )
		{
			m_threadData.m_audioBuffer->Release();
			m_threadData.m_audioBuffer = NULL;
		}

		ReleaseMediaObject( m_threadData.m_pendingVideoSample );
		CleanupDisplayFramesPlatformSpecific();

		m_threadData.Cleanup();
	}

	bool MediaDecoder::GenerateDisplayFramesPlatformSpecific()
	{
		return m_threadData.m_displayFrame.Initialize( GetFormatInfo() );
	}

	void MediaDecoder::CleanupDisplayFramesPlatformSpecific()
	{
		m_threadData.m_displayFrame.Shutdown();
	}

	void MediaDecoder::DecoderThreadFunc( void* dataPtr )
	{
		videoDisplayf("Starting MediaDecoder_win::DecoderThreadFunc");

		if( videoVerifyf( dataPtr, "MediaDecoder_win::DecoderThreadFunc - No thread data provided!" ) )
		{
			MediaDecoder::ThreadData& threadData( *static_cast<MediaDecoder::ThreadData*>( dataPtr ) );
			MediaCommon::ePrepareState threadPrepareState( MediaCommon::STATE_NONE );

            DURANGO_ONLY( MEDIA_MEMORY_WRAPPER.setDecoding( true ) );

			threadPrepareState = MediaCommon::PrepareThread();
			videoAssertf( threadPrepareState == MediaCommon::STATE_PREPARED, "MediaDecoder_win::DecoderThreadFunc - Thread preparations failed at level %d", (s32)threadPrepareState );
			
			IMFSourceReader* sourceReader( NULL );
			MediaTimer presentationTimer;
			DWORD videoStreamIndex;
			DWORD audioStreamIndex;

			if( threadPrepareState == MediaCommon::STATE_PREPARED && 
				videoVerifyf( PrepareForReading( threadData.m_targetUri, sourceReader, videoStreamIndex, audioStreamIndex, threadData.m_noAudio ),
					"MediaDecoder::DecoderThreadFunc - Unable to open %s for decoding", threadData.m_targetUri.c_str() ) )
			{
				// Should be valid for the duration of this scope, so get as ref here for helper functions
				IMFSourceReader& reader = *sourceReader;

				//! Now we need to populate our format info so we can correctly generate buffers required
				if( PopulateFormatInfo( reader, videoStreamIndex, audioStreamIndex, threadData.m_formatInfo, threadData.m_noAudio ) &&
					( threadData.m_noAudio ? threadData.m_formatInfo.IsVideoDataValid() : threadData.m_formatInfo.IsValid() ) )
				{
					if( ApplyGameCompatibleDecodingParameters( reader, videoStreamIndex, audioStreamIndex, threadData.m_noAudio ) )
					{
						u64 const c_startingSeekTimeNs = threadData.m_captureThumbnail ? threadData.m_formatInfo.m_durationNs / 2 : 0;
						threadData.m_pendingSeekTimeNs = c_startingSeekTimeNs;
						threadData.SetPlaystateDirect( PLAYSTATE_SEEK_THEN_PAUSE );

						// Flag we have enough detail for the update thread to generate our display frame
						threadData.SetDecodeStateDirect( DECODER_MIN_DETAIL );
						sysIpcWaitEvent( threadData.m_wakeThreadEvent );

						if( threadData.GetDecodeState() == DECODER_ACTIVE )
						{
							//threadData.SetPlaystateDirect( c_startingState );
							threadData.m_pendingSeekTimeNs = 0;

							DWORD videoStatusFlags(0);
							DWORD audioStatusFlags(0);

							MediaTimer presentationTimer;

							MFTIME currentPlaybackTimeOneHNs( 0 );
							MFTIME lastFramePlaybackTimeOneHNs( 0 );
							bool ignoreTimestampAndUpdateVideoFrame = false;
							u64 const c_frameUpdateDiffTimeOneHNs = u64(10000000.f / threadData.m_formatInfo.m_frameRate);

							u64 pausedTime( 0 );

							LONGLONG videoTimestampOneHNs(0);
							LONGLONG audioTimestampOneHNs(0);

							AudioBufferContext audioContext;
							audSoundManager& soundManager = g_AudioEngine.GetSoundManager();

							while( threadData.HasValidPlayStateForPlaybackLoop() )
							{
								if( threadData.m_playState == PLAYSTATE_PAUSED || 
									threadData.m_playState == PLAYSTATE_EOF )
								{
									presentationTimer.Stop();
									g_AudioEngine.GetSoundManager().Pause( MEDIA_DECODER_AUDIO_TIMER_ID );

									videoDisplayf( "MediaDecoder::DecoderThreadFunc - %s, blocking thread until playback resumes", threadData.m_playState == PLAYSTATE_EOF ? 
										"End of file" : "Pausing file" );

									// Block until we have a newly requested play state
									sysIpcWaitEvent( threadData.m_wakeThreadEvent );
								}

								// Apply our state change, if we have one
								if( threadData.m_playState != threadData.m_pendingPlayState &&
									threadData.m_pendingPlayState != PLAYSTATE_INVALID )
								{	
									bool const c_allowStateChange = threadData.m_pendingPlayState == PLAYSTATE_CLEANUP || 
										( threadData.m_playState == PLAYSTATE_EOF && 
										( threadData.m_pendingPlayState == PLAYSTATE_SEEK_THEN_PLAY || threadData.m_pendingPlayState == PLAYSTATE_SEEK_THEN_PAUSE ) ) ||
												( threadData.m_playState != PLAYSTATE_EOF && threadData.m_playState != PLAYSTATE_CLEANUP );
	
									if( c_allowStateChange )
									{
										if( threadData.m_pendingPlayState == PLAYSTATE_PLAYING )
										{
											presentationTimer.Start( pausedTime );
											soundManager.UnPause( MEDIA_DECODER_AUDIO_TIMER_ID );
										}
										else if( threadData.m_pendingPlayState == PLAYSTATE_PAUSED )
										{
											pausedTime = presentationTimer.GetTimeOneHNs();
											presentationTimer.Pause();
											soundManager.Pause( MEDIA_DECODER_AUDIO_TIMER_ID );	

											videoDisplayf( "MediaDecoder::DecoderThreadFunc - Video paused at %u ms, audio paused at %u ms",
												ONE_H_NS_UNITS_TO_MILLISECONDS( videoTimestampOneHNs ), ONE_H_NS_UNITS_TO_MILLISECONDS( audioTimestampOneHNs ) );
										}

										threadData.m_playState = threadData.m_pendingPlayState;
									}

									threadData.m_pendingPlayState = PLAYSTATE_INVALID;
								}

								if( threadData.m_playState == PLAYSTATE_SEEK_THEN_PAUSE || threadData.m_playState == PLAYSTATE_SEEK_THEN_PLAY )
								{
									videoDisplayf( "MediaDecoder::DecoderThreadFunc - Seeking on file" );

									//! Clean-up data from existing playback
									audioStatusFlags = videoStatusFlags = 0;
									audioContext.Reset();
									soundManager.Pause( MEDIA_DECODER_AUDIO_TIMER_ID );

									if( threadData.m_audioBuffer )
									{
										DrainAudioBuffer( *threadData.m_audioBuffer );
									}

									u64 const c_seekTimeOneHNs = NANOSECONDS_TO_ONE_H_NS_UNITS( threadData.m_pendingSeekTimeNs );
									SeekOnStreamOneHNs( reader, c_seekTimeOneHNs );

                                    // B*2341472 - Fix for loss of audio when using seek-then-play. Instead of setting play 
                                    // state directly, set it as the pending play state
                                    if(threadData.m_playState == PLAYSTATE_SEEK_THEN_PLAY)
                                    {
                                        threadData.SetPendingPlayState( PLAYSTATE_PLAYING );
                                    }
                                    else
                                    {
									    threadData.SetPlaystateDirect( PLAYSTATE_PAUSED );
                                    }

									currentPlaybackTimeOneHNs = c_seekTimeOneHNs;
									ignoreTimestampAndUpdateVideoFrame = true;

									if( threadData.m_playState == PLAYSTATE_PLAYING )
									{ 
										presentationTimer.Start( c_seekTimeOneHNs );
										soundManager.UnPause( MEDIA_DECODER_AUDIO_TIMER_ID );

										videoDisplayf( "MediaDecoder::DecoderThreadFunc - Resumed playback on seeking complete" );
									}
									else
									{
										pausedTime = c_seekTimeOneHNs;
										soundManager.Pause( MEDIA_DECODER_AUDIO_TIMER_ID );
									}
								}

								bool const c_atEndOfVideoStream = ( videoStatusFlags & MF_SOURCE_READERF_ENDOFSTREAM ) != 0;
								bool const c_updateVideoFrame = !c_atEndOfVideoStream && ( threadData.m_playState == PLAYSTATE_PLAYING || threadData.m_captureThumbnail || ignoreTimestampAndUpdateVideoFrame );

								if( c_updateVideoFrame )
								{
									currentPlaybackTimeOneHNs = presentationTimer.GetTimeOneHNs();
									u64 const diffTime = currentPlaybackTimeOneHNs - lastFramePlaybackTimeOneHNs;

									if( ignoreTimestampAndUpdateVideoFrame || diffTime >= c_frameUpdateDiffTimeOneHNs )
									{
										UpdateVideoStream( reader, videoStreamIndex, threadData.m_pendingVideoSample,
											videoStatusFlags, videoTimestampOneHNs );

										threadData.m_currentBackbufferPlaybackTimeOneHNs = videoTimestampOneHNs;
										lastFramePlaybackTimeOneHNs = currentPlaybackTimeOneHNs;
										threadData.m_captureThumbnail = false;
										ignoreTimestampAndUpdateVideoFrame = false;
									}
								}

								bool const c_atEndOfAudioStream = ( audioStatusFlags & MF_SOURCE_READERF_ENDOFSTREAM ) != 0;
								bool const c_updateAudio = threadData.m_audioBuffer && !threadData.m_noAudio && threadData.m_playState == PLAYSTATE_PLAYING;
								bool const c_hasAudioBytesToRead =  threadData.m_audioBuffer ? threadData.m_audioBuffer->GetBytesAvailableToRead() != 0 : false;
	
								if( c_updateAudio )
								{
									if( !c_atEndOfAudioStream )
									{
										UpdateAudioBufferContext( reader, audioStreamIndex, audioContext, audioStatusFlags, audioTimestampOneHNs );
									}

									//! Reset the audio stream if we are at the end and all bytes have been read
									if( c_atEndOfAudioStream && !c_hasAudioBytesToRead )
									{
										soundManager.Pause( MEDIA_DECODER_AUDIO_TIMER_ID );
									}
									else
									{
										PopulateAudioRingBuffer( audioContext, *threadData.m_audioBuffer );
									}
								}
								else
								{
									soundManager.Pause( MEDIA_DECODER_AUDIO_TIMER_ID );
								}

								if( c_atEndOfVideoStream && ( threadData.m_noAudio || ( c_atEndOfAudioStream && !c_hasAudioBytesToRead ) ) && 
									threadData.m_playState != PLAYSTATE_EOF )
								{
									threadData.SetPlaystateDirect( PLAYSTATE_EOF );
									videoDisplayf( "MediaDecoder::DecoderThreadFunc - Reached end of file" );
								}
							}

							// Clean up any remaining audio buffers
							audioContext.Reset();
						}

					}
				}
				else
				{
					videoDisplayf( "MediaDecoder::DecoderThreadFunc - File %s is an unsupported format", threadData.m_targetUri.c_str() );
				}

				CleanupReading( sourceReader );
			}

			// If we never reached the point of minimal detail, then we hit a file we don't support.
			// Let's bail out of the thread and let the callee code check the state and respond appropriately
			if( threadData.GetDecodeState() < DECODER_MIN_DETAIL )
			{
				threadData.SetDecodeStateDirect( DECODER_ERROR );
				threadData.SetLastError( MediaCommon::MEDIA_ERROR_UNSUPPORTED_FILE );
			}

			MediaCommon::CleanupThread( threadPrepareState );
			videoAssertf( threadPrepareState == MediaCommon::STATE_NONE, "MediaDecoder_win::DecoderThreadFunc - Failed to clean up thread! State left as %d", (s32)threadPrepareState );

            DURANGO_ONLY( MEDIA_MEMORY_WRAPPER.setDecoding( false ) );
		}

		videoDisplayf("Ending MediaDecoder_win::DecoderThreadFunc");
	}

} // namespace rage

#endif // ( RSG_DURANGO || RSG_PC ) && !__RESOURCECOMPILER && !__RGSC_DLL

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
