/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediadecoder_display_frame.cpp
// PURPOSE : Class for holding a decoded frame of video to be displayed
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2015 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#include "mediadecoder_display_frame.h"

#if defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED

// rage
#include "grcore/texture.h"
#include "system/memops.h"

// framework
#include "video/video_channel.h"

namespace rage
{

	MediaDecoderDisplayFrame::MediaDecoderDisplayFrame()
		: m_yBuffer( NULL )
		, m_uBuffer( NULL )
		, m_vBuffer( NULL )
		, m_id( -1 )
		, m_frameWidth( 0 )
		, m_frameHeight( 0 )
	{

	}

	MediaDecoderDisplayFrame::~MediaDecoderDisplayFrame()
	{
		Shutdown();
	}

	bool MediaDecoderDisplayFrame::Initialize( MediaDecoderFormatInfo const& formatInfo )
	{
		bool success = false;

		if( videoVerifyf( !IsInitialized(), "DisplayFrame::Initialize - Double texture initialization" ) &&
			videoVerifyf( formatInfo.IsVideoDataValid(), "DisplayFrame::Initialize - Invalid video format info!" ) )
		{
			m_frameWidth = formatInfo.m_displayWidth;
			m_frameHeight = formatInfo.m_displayHeight;

			size_t const c_yBufferSize = formatInfo.Get420YBufferSize();
			size_t const c_uBufferSize = formatInfo.Get420UVBufferSize() / 2;
			size_t const c_vBufferSize = c_uBufferSize;

			m_yBuffer = rage_new u8[ c_yBufferSize ];
			videoAssertf( m_yBuffer != NULL, "DisplayFrame::Initialize - Unable allocate display frame of size %lu"
				SIZETFMT "  for Y plane", c_yBufferSize );

			m_uBuffer = rage_new u8[ c_uBufferSize ];
			videoAssertf( m_uBuffer != NULL, "DisplayFrame::Initialize - Unable allocate display frame of size %lu"
				SIZETFMT "  for U plane", c_uBufferSize );

			m_vBuffer = rage_new u8[ c_vBufferSize ];
			videoAssertf( m_vBuffer != NULL, "DisplayFrame::Initialize - Unable allocate display frame of size %lu"
				SIZETFMT "  for V plane", c_vBufferSize );

			// Clear to black
			if( m_yBuffer )
			{
				sysMemSet( m_yBuffer, 16, c_yBufferSize );
			}

			if( m_uBuffer )
			{
				sysMemSet( m_uBuffer, 128, c_uBufferSize );
			}

			if( m_vBuffer )
			{
				sysMemSet( m_vBuffer, 128, c_vBufferSize );
			}

			success = m_initialized = m_yBuffer != NULL && m_uBuffer != NULL && m_vBuffer != NULL;
		}

		return success;
	}

	void MediaDecoderDisplayFrame::Shutdown()
	{
		if( m_yBuffer )
		{
			delete[] m_yBuffer;
			m_yBuffer = NULL;
		}

		if( m_uBuffer )
		{
			delete[] m_uBuffer;
			m_uBuffer = NULL;
		}

		if( m_vBuffer )
		{
			delete[] m_vBuffer;
			m_vBuffer = NULL;
		}

		m_frameWidth = m_frameHeight = 0;
		m_id = -1;
		m_initialized = false;
	}

	void MediaDecoderDisplayFrame::CopyNV12IntoFrame( u8 const * srcNv12, size_t const srcSize, MediaDecoderFormatInfo const& formatInfo )
	{
		CopyNV12IntoFrameInternal( m_yBuffer, m_uBuffer, m_vBuffer, srcNv12, srcSize, formatInfo, formatInfo.IsH264Video() );
	}

	void MediaDecoderDisplayFrame::PopulateTextureFrame( grcTexture* yTexture, grcTexture* uTexture, grcTexture* vTexture ) const
	{
		u32 const c_uvWidth = m_frameWidth / 2;
		u32 const c_uvHeight = m_frameHeight / 2;

		CopyBufferToTexture( yTexture, m_yBuffer, m_frameWidth, m_frameHeight );
		CopyBufferToTexture( uTexture, m_uBuffer, c_uvWidth, c_uvHeight );
		CopyBufferToTexture( vTexture, m_vBuffer, c_uvWidth, c_uvHeight );
	}

	void MediaDecoderDisplayFrame::CopyNV12IntoFrameInternal( u8* destY, u8* destU, u8* destV, u8 const * srcNv12, 
																	size_t const srcSize, MediaDecoderFormatInfo const& formatInfo, bool const useAlignedBuffers )
	{
		size_t const c_decodeAlignedYSize = useAlignedBuffers ? formatInfo.GetDecodeAligned420YBufferSize() : formatInfo.Get420YBufferSize();
		size_t const c_decodeAlignedUVBufferSize = useAlignedBuffers ? formatInfo.GetDecodeAligned420UVBufferSize() : formatInfo.Get420UVBufferSize();

		if( videoVerifyf( destY, "DisplayFrame::CopyNV12IntoFrame - NULL Y Buffer") &&
			videoVerifyf( destU, "DisplayFrame::CopyNV12IntoFrame - NULL U Buffer") &&
			videoVerifyf( destV, "DisplayFrame::CopyNV12IntoFrame - NULL V Buffer") &&
			videoVerifyf( srcNv12, "DisplayFrame::CopyNV12IntoFrame - NULL NV12 source buffer") && 
			videoVerifyf( c_decodeAlignedYSize + c_decodeAlignedUVBufferSize == srcSize, "DisplayFrame::CopyNV12IntoFrame - Mis-matched buffer sizes!"
			"Expected %" SIZETFMT "u but got %" SIZETFMT "u.", srcSize, c_decodeAlignedYSize + c_decodeAlignedUVBufferSize ) )
		{
			u8 const* c_srcYVal = srcNv12;
			u8* destYLocal = destY;

			u8 const* c_srcUVal = srcNv12 + c_decodeAlignedYSize;
			u8 const* c_srcVVal = c_srcUVal + 1;

			size_t const c_yDisplayStride = formatInfo.Get420YRowSize();
			size_t const c_yDecodeStride = useAlignedBuffers ? formatInfo.GetDecodeAligned420YRowSize() : formatInfo.Get420YRowSize();
			size_t const c_yDisplayBufferSize = formatInfo.Get420YBufferSize();

			// If we are aligned with the decode stride, we can do a block copy
			size_t const c_yCopySize = c_yDisplayStride == c_yDecodeStride ? c_yDisplayBufferSize : c_yDisplayStride;
			size_t const c_yUpperLimit = c_yDisplayStride == c_yDecodeStride ? 1 : formatInfo.m_displayHeight;

			for( size_t yIndex = 0; yIndex < c_yUpperLimit; ++yIndex )
			{
				sysMemCpy( destYLocal, c_srcYVal, c_yCopySize );

				destYLocal += c_yDisplayStride;
				c_srcYVal += c_yDecodeStride;
			}

			size_t const c_deinterleavedBufferSize = formatInfo.Get420UVBufferSize() / 2;

			size_t const c_uvDisplayStride = formatInfo.Get420UVRowSize();
			size_t const c_uvDecodeStride = useAlignedBuffers ? formatInfo.GetDecodeAligned420UVRowSize() : formatInfo.Get420UVRowSize();

			size_t const c_uvRowPadding = c_uvDecodeStride - c_uvDisplayStride;

			for( size_t deinterleavedIndex = 0; deinterleavedIndex < c_deinterleavedBufferSize; )
			{
				destU[ deinterleavedIndex ] = *c_srcUVal;
				destV[ deinterleavedIndex ] = *c_srcVVal;

				++deinterleavedIndex;
				size_t const c_padding = ( ( deinterleavedIndex * 2 ) % c_uvDisplayStride == 0 ) ? c_uvRowPadding : 0;

				c_srcUVal += c_padding + 2;
				c_srcVVal += c_padding + 2;
			}
		}
	}

	void MediaDecoderDisplayFrame::CopyBufferToTexture( grcTexture* texture, u8 const * const bufferSource, u32 const bufferWidth, u32 const bufferHeight )
	{
		grcTextureLock oLock;
		if( texture && texture->LockRect( 0, 0, oLock ) )
		{
			u8* dest = (u8*)oLock.Base;
			u32 const c_lineSize = bufferWidth * ( oLock.BitsPerPixel / 8 );

			for( u32 line = 0, srcOffset = 0; line < bufferHeight; ++line, dest += oLock.Pitch, srcOffset += c_lineSize )
			{
				sysMemCpy( dest, bufferSource + srcOffset, c_lineSize );
			}

			texture->UnlockRect( oLock );
		}
	}

} // namespace rage

#endif // defined(VIDEO_PLAYBACK_ENABLED) && VIDEO_PLAYBACK_ENABLED
