/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_buffers.cpp
// PURPOSE : Class for storing buffer data for encoding audio/video
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#include "mediaencoder_colourconversion.h"

#if (USES_MEDIA_ENCODER) && !__RESOURCECOMPILER && !__RGSC_DLL

#if (RSG_PC || RSG_DURANGO)
#pragma warning(push)
#pragma warning(disable: 4668)
#include <WinSock2.h>
#include <Windows.h>
#pragma warning(pop)
#endif // (RSG_PC || RSG_DURANGO)

// rage
#if RSG_ORBIS
#include "grcore/texture_gnm.h"
#elif RSG_DURANGO
#include "grcore/d3dwrapper.h"
#endif

#include "grcore/device.h"
#include "grcore/quads.h"
#include "grcore/viewport.h"

#include "profile/profiler.h"
#include "system/timer.h"

// framework
#include "fwsys/timer.h"
#include "media_common.h"
#include "mediaencoder_texture_pool.h"
#include "mediatimer.h"
#include "video_channel.h"

namespace rage
{
	
	rage::MediaEncoderColorConversion MEDIA_ENCODER_COLOUR_CONVERTER;

#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
	bool MediaEncoderColorConversion::InitializeHardwareConversion(u32 const outputWidth, u32 const outputHeight)
	{
		bool success = false;

		if(m_pEffectEncode == NULL)
		{
			m_pEffectEncode = grcEffect::LookupEffect( atStringHash( "rage_bink" ) );

			if( Verifyf(m_pEffectEncode != NULL, "MediaEncoderColorConversion::InitializeHardwareConversion bink shader not loaded?") )
			{
				m_RgbTextureId	= m_pEffectEncode->LookupVar("RGBColor");
				m_tEncodeLuma	= m_pEffectEncode->LookupTechnique("bink_encode_luma");
				m_tEncodeChroma = m_pEffectEncode->LookupTechnique("bink_encode_chroma");

				success = true;
			}
		}

		ID3D11Device* device = static_cast<ID3D11Device*>( GRCDEVICE.GetCurrent() );
		if(device)
		{
			// encoder required the width and height to be divisible by 2
			m_nv12OutputWidth = ( outputWidth / 2 ) * 2; 
			m_nv12OutputHeight = ( outputHeight / 2 ) * 2; 

			D3D11_TEXTURE2D_DESC nv12Desc = {};
			nv12Desc.Usage = D3D11_USAGE_DEFAULT;
			nv12Desc.Format = DXGI_FORMAT_NV12;
			nv12Desc.BindFlags = D3D11_BIND_VIDEO_ENCODER | D3D11_BIND_RENDER_TARGET;
			nv12Desc.CPUAccessFlags = 0; 
			nv12Desc.MiscFlags =  D3D11_RESOURCE_MISC_SHARED;
			nv12Desc.Width =  m_nv12OutputWidth;  
			nv12Desc.Height =  m_nv12OutputHeight; 
			nv12Desc.ArraySize = 1;
			nv12Desc.MipLevels = 1;
			nv12Desc.SampleDesc.Count = 1;
			nv12Desc.SampleDesc.Quality = 0;

			HRESULT hr;
			if(!m_nv12Texture)
			{
				hr = device->CreateTexture2D( &nv12Desc, NULL, &m_nv12Texture );
				videoAssertf( SUCCEEDED(hr), "MediaEncoderBuffers::InitializeHardwareConversion - "
					"ID3D11Device::CreateTexture2D failed to with 0x%08lx", hr );
			}			

			if(m_nv12Texture)
			{
				D3D11_RENDER_TARGET_VIEW_DESC lumDesc;
				ZeroMemory((void*)&lumDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
				lumDesc.Format = DXGI_FORMAT_R8_UNORM;
				lumDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
				lumDesc.Texture2D.MipSlice = 0;

				D3D11_RENDER_TARGET_VIEW_DESC chromaDesc;
				ZeroMemory((void*)&chromaDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
				chromaDesc.Format = DXGI_FORMAT_R8G8_UINT;
				chromaDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
				chromaDesc.Texture2D.MipSlice = 0;

				if(!m_nv12LumaRTV)
				{
					hr = device->CreateRenderTargetView(m_nv12Texture, &lumDesc, &m_nv12LumaRTV);
					videoAssertf( SUCCEEDED(hr), "MediaEncoderColorConversion::InitializeHardwareConversion - "
						"ID3D11Device::CreateRenderTargetView failed to with 0x%08lx", hr );

					success &= (bool)SUCCEEDED(hr);
				}
				
				if(!m_nv12ChromaRTV)
				{
					hr = device->CreateRenderTargetView(m_nv12Texture, &chromaDesc, &m_nv12ChromaRTV);
					videoAssertf( SUCCEEDED(hr), "MediaEncoderColorConversion::InitializeHardwareConversion - "
						"ID3D11Device::CreateRenderTargetView failed to with 0x%08lx", hr );	

					success &= (bool)SUCCEEDED(hr);
				}				
			}
		}	
		return success;
	}

	void MediaEncoderColorConversion::RGBToNV12Hardware(ID3D11Texture2D* pDst, grcTexture const* pSrc )
	{
		grcViewport *viewport = grcViewport::GetCurrent();
		ID3D11Device* device = static_cast<ID3D11Device*>( GRCDEVICE.GetCurrent() );
		if (device)
		{
			ID3D11DeviceContext* deviceContext( NULL );
			device->GetImmediateContext( &deviceContext );

			if( deviceContext && m_nv12Texture )
			{
				GRC_ALLOC_SCOPE_AUTO_PUSH_POP();
				GRC_VIEWPORT_AUTO_PUSH_POP();

				grcViewport::SetCurrent( viewport );
				viewport->SetWindow( 0, 0, m_nv12OutputWidth, m_nv12OutputHeight );
				viewport->Screen();

				ID3D11RenderTargetView* nvLuma12RT[1] = { m_nv12LumaRTV };
				ID3D11RenderTargetView* nvChroma12RT[1] = { m_nv12ChromaRTV };

				deviceContext->OMSetRenderTargets(1, &nvLuma12RT[0], NULL);					

				if( m_pEffectEncode )
				{						
					m_pEffectEncode->SetVar(m_RgbTextureId, pSrc);

					if (m_pEffectEncode->BeginDraw(m_tEncodeLuma, true)) 
					{
						m_pEffectEncode->BeginPass(0);
					}					
				}

				grcDrawSingleQuadf( 0, 0, m_nv12OutputWidth, m_nv12OutputHeight, 
					0.f, 0.f, 0.f, 1.f, 1.f, Color32(255,255,255,255) );

				if( m_pEffectEncode )
				{
					m_pEffectEncode->EndPass();
					m_pEffectEncode->EndDraw();
				}

				ID3D11RenderTargetView* emptyRT[2] = { NULL };
				deviceContext->OMSetRenderTargets(1, &emptyRT[0], NULL);

				viewport->SetWindow( 0, 0, m_nv12OutputWidth/2, m_nv12OutputHeight/2 );
				viewport->Screen();
				deviceContext->OMSetRenderTargets(1, &nvChroma12RT[0], NULL);


				if( m_pEffectEncode )
				{
					m_pEffectEncode->SetVar(m_RgbTextureId, pSrc);

					if (m_pEffectEncode->BeginDraw(m_tEncodeChroma, true)) 
					{
						m_pEffectEncode->BeginPass(0);
					}					
				}

				grcDrawSingleQuadf( 0, 0, m_nv12OutputWidth/2, m_nv12OutputHeight/2, 
					0.f, 0.f, 0.f, 1.f, 1.f, Color32(255,255,255,255) );

				if( m_pEffectEncode )
				{
					m_pEffectEncode->EndPass();
					m_pEffectEncode->EndDraw();
				}
				deviceContext->OMSetRenderTargets(1, &emptyRT[0], NULL);
				
				if( m_pEffectEncode )
				{
					deviceContext->CopyResource( pDst, m_nv12Texture );					
				}				

				deviceContext->Release();
			}
		}		
	}

	void MediaEncoderColorConversion::ShutdownHardwareConversion()
	{
		SafeRelease(m_nv12LumaRTV);
		SafeRelease(m_nv12ChromaRTV);
		SafeRelease(m_nv12Texture);			

		m_pEffectEncode = NULL;
	}
#endif

	void MediaEncoderColorConversion::ConvertRGBToNV12_CPU( u8 * pDst, u8 const* pSrc, s32 width, s32 height, int linePadding )
	{
		if( videoVerifyf(pDst != NULL, "MediaEncoderColorConversion::ConvertRGBToNV12_CPU - NULL destination data for colour conversion") &&
			videoVerifyf(pSrc != NULL, "MediaEncoderColorConversion::ConvertRGBToNV12_CPU - NULL source data for colour conversion") )
		{
			int i,j;
			unsigned char  y,v,u;
			unsigned char r,g,b;
			
			int uvStart = width * height;
			int uvPos = uvStart;
			int yIndex = 0;

			for( i = 0; i < height; ++i )
			{
				j = 0;
				while(j < width)
				{
					uvPos = uvStart + (width * (i/2)) + (j/2)*2;

					int indexSrc = (width * 4 + linePadding) * i + j * 4;

					r = pSrc[indexSrc+2];
					g = pSrc[indexSrc+1];
					b = pSrc[indexSrc];

					y = ( (  65 * r + 128 * g +  24 * b + 128) >> 8) +  16;
					u = ( ( -37 * r -  74 * g + 112 * b + 128) >> 8) + 128;
					v = ( ( 112 * r -  93 * g -  18 * b + 128) >> 8) + 128;

					pDst[yIndex] = y;

					pDst[uvPos] = u;
					pDst[uvPos+1] = v;

					j+=1;
					yIndex +=1;
				}
			}
		}
	}

	void MediaEncoderColorConversion::ConvertRGBToNV12_GPU(grcTextureObject*& pDst, grcTexture const* pSrc)
	{
#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
		if( videoVerifyf(pSrc != NULL, "MediaEncoderColorConversion::ConvertRGBToNV12_CPU - NULL source data for colour conversion") )
		{
			ID3D11Texture2D* nv12Texture = ENCODER_TEXTURE_POOL.GetNextTexture();
			if( nv12Texture )
			{
				ID3D11Device* device = static_cast<ID3D11Device*>( GRCDEVICE.GetCurrent() );
				if( device )
				{
					RGBToNV12Hardware(nv12Texture, pSrc);

					pDst = nv12Texture;
				}
			}
		}
#else
		(void)pDst;
		(void)pSrc;
		videoAssertf(0, "MediaEncoderColorConversion::ConvertRGBToNV12_GPU not available on this platform!");
#endif
	}

	bool MediaEncoderColorConversion::Init(u32 const outputWidth, u32 const outputHeight)
	{
		bool success = true;

		m_outputWidth = outputWidth;
		m_outputHeight = outputHeight;

#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
		success &= InitializeHardwareConversion(outputWidth, outputHeight);
#endif
		return success;
	}

	void MediaEncoderColorConversion::Shutdown()
	{
#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
		ShutdownHardwareConversion();
#endif
	}

	bool MediaEncoderColorConversion::IsInitialized() const
	{
		return (m_outputWidth > 0) && (m_outputHeight > 0)
#if defined(USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING) && USE_HARDWARE_COLOUR_CONVERSION_FOR_VIDEO_ENCODING 
			&& (m_nv12Texture != NULL) && (m_nv12LumaRTV != NULL) && (m_nv12ChromaRTV != NULL) && (m_pEffectEncode != NULL)
#endif
			;
	}

} // namespace rage

#endif // (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER
