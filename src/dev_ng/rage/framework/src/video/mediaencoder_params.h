/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : mediaencoder_params.h
// PURPOSE : Parameters for encoding video/audio streams.
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef INC_MEDIAENCODER_PARAMS_H_
#define INC_MEDIAENCODER_PARAMS_H_


#define USES_MEDIA_ENCODER (RSG_PC || RSG_DURANGO|| RSG_ORBIS)
#define USE_FLEX_MEMORY_FOR_BUFFERS (RSG_ORBIS)

#if !__RESOURCECOMPILER

// rage
#include "audiohardware/mixer.h"
#include "math/amath.h"

// framework
#include "fwui/Common/fwuiScaling.h"
#include "media_common.h"

#define MEDIA_ENCODER_INPUT_BPP			(4)

#if RSG_DURANGO

#define MEDIA_ENCODER_DEFAULT_WIDTH		(1280)
#define MEDIA_ENCODER_DEFAULT_HEIGHT	(720)

#else

#define MEDIA_ENCODER_DEFAULT_WIDTH		(1280)
#define MEDIA_ENCODER_DEFAULT_HEIGHT	(720)

#endif

namespace rage
{
	struct MediaEncoderParams
	{
		enum eOutputFps
		{
			OUTPUT_FPS_INVALID = -1,

			OUTPUT_FPS_FIRST = 0,
			OUTPUT_FPS_THIRTY = OUTPUT_FPS_FIRST,
			OUTPUT_FPS_SIXTY,

			OUTPUT_FPS_MAX,

			OUTPUT_FPS_DEFAULT = OUTPUT_FPS_THIRTY
		};

		enum eQualityLevel
		{
			QUALITY_INVALID = -1,

			QUALITY_FIRST = 0,

			QUALITY_LOW = QUALITY_FIRST,
			QUALITY_MEDIUM,
			QUALITY_HIGH,
			QUALITY_ULTRA,

			QUALITY_MAX,

			QUALITY_DEFAULT = QUALITY_HIGH
		};

		MediaEncoderParams()
			: m_outputFps( OUTPUT_FPS_DEFAULT )
			, m_videoEncodingFormat( MediaCommon::VIDEO_FORMAT_INVALID )
			, m_audioEncodingFormat( MediaCommon::AUDIO_FORMAT_INVALID )
			, m_qualityLevel( QUALITY_DEFAULT )
			, m_estimatedDurationMs(0)
			, m_gameTitle(NULL)
			, m_copyright(NULL)
			, m_hasModdedContent(false)
		{
			SetOutputDimensions( MEDIA_ENCODER_DEFAULT_WIDTH, MEDIA_ENCODER_DEFAULT_HEIGHT );
		}

		void Reset()
		{
			SetOutputDimensions( MEDIA_ENCODER_DEFAULT_WIDTH, MEDIA_ENCODER_DEFAULT_HEIGHT );
			m_outputFps				= OUTPUT_FPS_DEFAULT;
			m_videoEncodingFormat	= MediaCommon::VIDEO_FORMAT_INVALID;
			m_audioEncodingFormat	= MediaCommon::AUDIO_FORMAT_INVALID;
			m_qualityLevel			= QUALITY_DEFAULT;

			m_estimatedDurationMs	= 0;
			m_gameTitle				= NULL;
			m_copyright				= NULL;
			m_hasModdedContent		= false;

		}

		bool IsValid()
		{
			return MediaCommon::IsSupportedVideoFormat( m_videoEncodingFormat ) && MediaCommon::IsSupportedAudioFormat( m_audioEncodingFormat );
		}

		u32 GetOutputWidth() const { return m_outputWidth; }
		u32 GetOutputHeight() const { return m_outputHeight; }

        //! Takes the size of the current game back-buffer and generates/stores the best supported output video sizes
		void SetOutputDimensions( u32 const gameWidth, u32 const gameHeight )
		{
			int resultingWidth, resultingHeight;
			CalculateOutputDimensions( resultingWidth, resultingHeight, gameWidth, gameHeight );

			m_outputWidth = resultingWidth > 0 ? resultingWidth : MEDIA_ENCODER_DEFAULT_WIDTH;
			m_outputHeight = resultingHeight > 0 ? resultingHeight : MEDIA_ENCODER_DEFAULT_HEIGHT;
		}

		static void CalculateOutputDimensions( int& out_Width, int& out_height, int const requestedWidth, int const requestedHeight )
		{
            int const c_restrictedWidth = Clamp<int>( requestedWidth, GetMinOutputWidth(), GetMaxOutputWidth() );
            int const c_restrictedHeight = Clamp<int>( requestedHeight, GetMinOutputHeight(), GetMaxOutputHeight() );

			fwuiScaling::CalculateBestFitClampDimensions( out_Width, out_height, requestedWidth, requestedHeight, c_restrictedWidth, c_restrictedHeight );
			out_Width = ( out_Width / 2 ) * 2;
			out_height = ( out_height / 2 ) * 2;
		}

		static u32 GetMinOutputWidth()
		{
#if RSG_PC
			return 640;
#else
			return MEDIA_ENCODER_DEFAULT_WIDTH;
#endif
		}

		static u32 GetMaxOutputWidth()
		{
#if RSG_PC
			return 1920;//MediaCommon::IsWindows8OrGreater() ? 4096 : 1920;
#else
			return MEDIA_ENCODER_DEFAULT_WIDTH;
#endif	
		}

		static u32 GetMinOutputHeight()
		{
#if RSG_PC
			return 480;
#else
			return MEDIA_ENCODER_DEFAULT_HEIGHT;
#endif
		}

		static u32 GetMaxOutputHeight()
		{
#if RSG_PC
			return 1080;//MediaCommon::IsWindows8OrGreater() ? 2304 : 1080;
#else
			return MEDIA_ENCODER_DEFAULT_HEIGHT;
#endif
		}

		inline eQualityLevel GetOutputQuality() const { return m_qualityLevel; }
		inline void SetOutputQuality( eQualityLevel const qualityLevel )
		{
			m_qualityLevel = qualityLevel;
		}

		inline eOutputFps GetOutputFps() const { return m_outputFps; }
		inline void SetOutputFps( eOutputFps const outputFps )
		{
			m_outputFps = outputFps;
		}

		inline u32 GetEstimatedDurationMs() const { return m_estimatedDurationMs; }
		inline void SetEstimatedDurationMs( u32 const estimatedDurationMs )
		{
			m_estimatedDurationMs = estimatedDurationMs;
		}

		inline const wchar_t* GetGameTitle() const { return m_gameTitle; }
		inline void SetGameTitle( const wchar_t* gameTitle )
		{
			m_gameTitle = gameTitle;
		}

		inline const wchar_t* GetCopyright() const { return m_copyright; }
		inline void SetCopyright( const wchar_t* copyright )
		{
			m_copyright = copyright;
		}

		inline bool GetModdedContent() const { return m_hasModdedContent; }
		inline void SetModdedContent( const bool hasModdedContent )
		{
			m_hasModdedContent = hasModdedContent;
		}

		inline size_t GetOutputBufferWidthBytes() const { return MEDIA_ENCODER_INPUT_BPP * GetOutputWidth(); }
		inline size_t GetOutputBufferSizeBytes() const { return GetOutputBufferWidthBytes() * GetOutputHeight(); }

		inline float GetOutputFrameDurationMs() const { return GetOutputFrameDurationMs( m_outputFps ); }
		static float GetOutputFrameDurationMs( MediaEncoderParams::eOutputFps const outputFps );

		inline float GetOutputFpsFloat() const { return GetOutputFpsFloat( m_outputFps ); }
		static float GetOutputFpsFloat( MediaEncoderParams::eOutputFps const outputFps );

		inline u32 GetKeyframeInterval() const { return GetKeyframeInterval( m_outputFps ); }
		static u32 GetKeyframeInterval( MediaEncoderParams::eOutputFps const outputFps );

		inline char const * GetOutputFpsDisplayName() const { return GetOutputFpsDisplayName( m_outputFps ); }
		static char const * GetOutputFpsDisplayName( MediaEncoderParams::eOutputFps const outputFps );

		u32 GetDurationQualityTier() const { return GetDurationQualityTier( m_estimatedDurationMs ); }
		static u32 GetDurationQualityTier( u32 const durationMs );

		u32 GetQualityLevelVideoBitRate() const { return GetQualityLevelVideoBitRate( m_videoEncodingFormat, m_qualityLevel, m_estimatedDurationMs ); }
		static u32 GetQualityLevelVideoBitRate( MediaCommon::eVideoFormat const format, MediaEncoderParams::eQualityLevel const qualityLevel, u32 const durationMs );

		u32 GetQualityLevelPercent() const { return GetQualityLevelPercent( m_qualityLevel, m_estimatedDurationMs ); }
		static u32 GetQualityLevelPercent( MediaEncoderParams::eQualityLevel const qualityLevel, u32 const durationMs );

		inline u32 GetSpeedVsQualityPercent() const { return GetSpeedVsQualityPercent( m_qualityLevel, m_estimatedDurationMs ); }
		static u32 GetSpeedVsQualityPercent( MediaEncoderParams::eQualityLevel const qualityLevel, u32 const durationMs );

		inline char const * GetQualityLevelDisplayName() const { return GetQualityLevelDisplayName( m_qualityLevel ); }
		static char const * GetQualityLevelDisplayName( MediaEncoderParams::eQualityLevel const qualityLevel );
		
		void SetOutputFormat( MediaCommon::eVideoFormat const videoFormat );

		inline bool IsOutputtingWMV() const { return IsOutputtingWMV( m_videoEncodingFormat ); }
		static bool IsOutputtingWMV( MediaCommon::eVideoFormat const format );

		inline bool IsOutputtingWMA() const { return IsOutputtingWMA( m_audioEncodingFormat ); }
		static bool IsOutputtingWMA( MediaCommon::eAudioFormat const format );

		inline u32 GetAudioOutputBlockAlign() const { return GetAudioOutputBlockAlign( m_audioEncodingFormat, m_qualityLevel ); }
		static u32 GetAudioOutputBlockAlign( MediaCommon::eAudioFormat const format, MediaEncoderParams::eQualityLevel const qualityLevel );

		inline u32 GetAudioOutputBytesPerSecond() const { return GetAudioOutputBytesPerSecond( m_audioEncodingFormat, m_qualityLevel ); }
		static u32 GetAudioOutputBytesPerSecond( MediaCommon::eAudioFormat const format, MediaEncoderParams::eQualityLevel const qualityLevel );

		inline MediaCommon::eVideoFormat GetVideoEncodingFormat() const { return m_videoEncodingFormat; }
		inline MediaCommon::eAudioFormat GetAudioEncodingFormat() const { return m_audioEncodingFormat; }

		static size_t EstimateSizeOfRecording( u32 const durationMs, MediaEncoderParams::eQualityLevel const qualityLevel,
											MediaCommon::eVideoFormat const videoFormat, MediaCommon::eAudioFormat const audioFormat );

		private:
			u32							m_outputWidth;
			u32							m_outputHeight;
			eOutputFps					m_outputFps;
			MediaCommon::eVideoFormat	m_videoEncodingFormat;
			MediaCommon::eAudioFormat	m_audioEncodingFormat;
			eQualityLevel				m_qualityLevel;

			u32							m_estimatedDurationMs;
			const wchar_t*				m_gameTitle;
			const wchar_t*				m_copyright;
			bool						m_hasModdedContent;
	};

}

#endif // !__RESOURCECOMPILER
#endif // INC_MEDIAENCODER_PARAMS_H_
