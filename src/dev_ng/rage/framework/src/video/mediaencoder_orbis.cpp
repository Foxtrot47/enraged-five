#include "mediaencoder.h"

#if RSG_ORBIS
#if USES_MEDIA_ENCODER && !__RESOURCECOMPILER

#define FRAME_RATE_VARIABLE 0

#include <video_recording.h>

// rage
#include "grcore/texture.h"
#include "system/ipc.h"
#include "audioengine/engineutil.h"
#include "system/alloca.h"
#include "audiohardware/mixing_vmath.inl"

// framework
#include "media_common.h"
#include "mediaencoder_texture_pool.h"
#include "video_channel.h"
#include "mediaencoder_colourconversion.h"

namespace rage
{
	void MediaEncoder::PrepareRenderThread()
	{
		GenerateDummyVideoData();
	}

	void MediaEncoder::CleanupRenderThread()
	{
		CleanupDummyVideoData();
	}

	bool MediaEncoder::ThreadData::IsVideoFrameCaptureAllowed() const
	{
		return m_buffers.IsInitialized() && !HasError() && ( !m_killFlag || ( m_killFlag && m_drainFlag ) ) &&
			( m_threadMessaging & ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN ) == 0;
	}

	void MediaEncoder::GenerateDummyTexture( grcTexture*& out_texture )
	{
#if defined( USE_DUMMY_VIDEO_DATA )

#define PIX_BUFFER_SIZE ( MEDIA_ENCODER_DEFAULT_WIDTH * MEDIA_ENCODER_DEFAULT_HEIGHT )

		if( out_texture == NULL )
		{
			typedef unsigned long DWORD;
			static DWORD sc_dummyPxBuffer[ PIX_BUFFER_SIZE ];

			// Set all pixels to green
			for ( DWORD i = 0; i < PIX_BUFFER_SIZE; ++i )
			{
				sc_dummyPxBuffer[i] = 0xFF00FF00;
			}

			//! This texture create may assert about threading. We don't care, this is dev only
			grcTextureFactory::TextureCreateParams params( 
				grcTextureFactory::TextureCreateParams::VIDEO,
				grcTextureFactory::TextureCreateParams::LINEAR,
				grcsRead, NULL,
				grcTextureFactory::TextureCreateParams::NORMAL );

			out_texture =
				grcTextureFactory::GetInstance().Create( MEDIA_ENCODER_DEFAULT_WIDTH, MEDIA_ENCODER_DEFAULT_HEIGHT, 
				grctfA8R8G8B8, sc_dummyPxBuffer, 1, &params);

			videoAssertf( out_texture, "MediaEncoder::GenerateDummyVideoData - Unable to create dummy texture!" );	
		}

		out_texture->AddRef();
#else
		(void)out_texture;
#endif // defined( USE_DUMMY_DATA )
	}

	inline bool GetNextMediaBuffer( rage::MediaEncoderBuffers& bufferInterface, 
		rage::MediaBufferBase*& out_buffer, bool const isFocusingVideo )
	{
		bool hasBuffer = isFocusingVideo ? 
			bufferInterface.PopVideoBuffer( out_buffer ) : bufferInterface.PopAudioBuffer( out_buffer );

		return hasBuffer;
	}

	void MediaEncoder::OnRecordingCancelled()
	{
		
	}

	void MediaEncoder::EncoderThreadFunc( void* dataPtr )
	{
		videoAssertf( dataPtr, "MediaEncoderWindows::EncoderThreadFunc - No thread data provided!" );
		if( dataPtr )
		{
			ThreadDataWrapper* threadDataWrapper( static_cast<ThreadDataWrapper*>( dataPtr ) );
			bool const isProcessingVideo = threadDataWrapper->m_isVideo;
			ThreadData& threadData = threadDataWrapper->m_threadData;
			MediaCommon::ePrepareState threadPrepareState( MediaCommon::STATE_NONE );

			videoDisplayf("Starting MediaEncoder_orbis::EncoderThreadFunc - Type %s", isProcessingVideo ? "Video" : "Audio" );

			threadPrepareState = MediaCommon::PrepareThread();
			videoAssertf( threadPrepareState == MediaCommon::STATE_PREPARED, "MediaEncoderWindows::EncoderThreadFunc - Thread preparations failed at level %d", (s32)threadPrepareState );
			if( threadPrepareState == MediaCommon::STATE_PREPARED )
			{
				// Video Thread is the main thread for encoding, audio thread waits for it to set things up then starts pumping data like mad
				if( isProcessingVideo )
				{
					VideoThreadFunc( threadData );
				}
				else
				{
					AudioThreadFunc( threadData );
				}
			}
			else
			{
				threadData.m_errorState = MediaCommon::MEDIA_ERROR_UNKNOWN;
			}

			MediaCommon::CleanupThread( threadPrepareState );
			videoAssertf( threadPrepareState == MediaCommon::STATE_NONE, "MediaEncoderWindows::EncoderThreadFunc - Failed to clean up thread! State left as %d", (s32)threadPrepareState );
		}
	}

	static bool PushFrameToAPI( u8 const * buffer, s32 pitch, s32 height)
	{
		bool success = false;

		void *addr;
		int ret = sceVideoRecordingGetInfo(SCE_VIDEO_RECORDING_INFO_VIDEO_INPUT_LOCK, &addr, sizeof(addr));
		if (ret == SCE_OK)
		{
			// size calculation taken from Sony documentation
			memcpy(addr, buffer, pitch * height * 1.5);

			ret = sceVideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_VIDEO_INPUT_UNLOCK, 0, 0);
			if (ret == SCE_OK)
			{
				success = true;
			}
		}

		return success;
	}	

	static bool UpdateVideoCapture(MediaBufferBase*& buffer, MediaEncoderBuffers& bufferOwner)
	{
		bool success = false;

		if( videoVerifyf( buffer, "UpdateVideoCapture - Null frame provided!" ) )
		{
			MediaBufferVideoEncode* videoBuffer = static_cast<MediaBufferVideoEncode*>(buffer);

			MediaBufferBlobInterfaceRage& externalBufferInterface = videoBuffer->GetExternalBufferInterface();
			const u8* externalBuffer = externalBufferInterface.GetBytesReadOnly();
			if (externalBuffer)
			{
				s32 pitch, height = 1;
				sceVideoRecordingGetInfo(SCE_VIDEO_RECORDING_INFO_VIDEO_INPUT_PITCH, &pitch, sizeof(pitch));
				sceVideoRecordingGetInfo(SCE_VIDEO_RECORDING_INFO_VIDEO_INPUT_HEIGHT, &height, sizeof(height));

				if( PushFrameToAPI( externalBuffer, pitch, height ) )
				{
					success = true;
				}
			}

			bufferOwner.ReleaseBuffer( buffer );
		}

		return success;
	}

	void MediaEncoder::VideoThreadFunc( MediaEncoder::ThreadData& threadData )
	{
		MediaEncoderParams& params = threadData.m_encodeParams;

#if FRAME_RATE_VARIABLE
		u64 const c_sceVideoFrameDurationOneHNs = 333333;
		u64 sceOutputDurationOneHns = 0;
		s64 frameDiffOneHns = 0;
#endif

		threadData.m_encoderState = ENCODER_PREPARING;

		if( videoVerifyf( threadData.m_buffers.Initialize( params ), "MediaEncoderWindows::VideoThreadFunc - Unable to initialize buffers!" ) )
		{
			MediaBufferBase* buffer( NULL );
			bool allFrameWritesValid = true;

			threadData.m_encoderState = ENCODER_ACTIVE;

			while( allFrameWritesValid && ( !threadData.m_killFlag || threadData.m_drainFlag ) )
			{
				GetNextMediaBuffer( threadData.m_buffers, buffer, true );

				if( threadData.m_buffers.IsEOFBuffer( buffer ) )
				{
					threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
					threadData.m_buffers.ReleaseBuffer( buffer );
					break;
				}

				u64 const bufferCreationTimeNs = buffer->GetCreationTimeNs();
				u64 const c_bufferDurationNs = buffer->GetDurationNs();

				bool discardBuffer = ( bufferCreationTimeNs < threadData.m_startTimeNs ) ||
					( threadData.m_killFlag && !threadData.m_drainFlag );

				if( !discardBuffer )
				{
					//u64 const c_bufferDurationOneHNs = NANOSECONDS_TO_ONE_H_NS_UNITS( c_bufferDurationNs );
					MediaBufferBase::eBufferType const bufferType = buffer->GetType();

					if( bufferType == MediaBufferBase::BUFFER_VIDEO )
					{
						allFrameWritesValid = UpdateVideoCapture( buffer, threadData.m_buffers );

						if( !allFrameWritesValid )
						{
							threadData.m_errorState = MediaCommon::MEDIA_ERROR_OOM;
						}

						threadData.m_currentVideoFrameNs += c_bufferDurationNs;

#if FRAME_RATE_VARIABLE
						sceOutputDurationOneHns += c_sceVideoFrameDurationOneHNs;

						frameDiffOneHns += static_cast<s32>(c_bufferDurationOneHNs) - static_cast<s32>(c_sceVideoFrameDurationOneHNs);
						if( frameDiffOneHns >= static_cast<s32>(c_sceVideoFrameDurationOneHNs) )
						{
							u8* convertBuffer = threadData.m_buffers.GetYUVBuffer();

							s32 pitch, height = 1;
							sceVideoRecordingGetInfo(SCE_VIDEO_RECORDING_INFO_VIDEO_INPUT_PITCH, &pitch, sizeof(pitch));
							sceVideoRecordingGetInfo(SCE_VIDEO_RECORDING_INFO_VIDEO_INPUT_HEIGHT, &height, sizeof(height));

							PushFrameToAPI(convertBuffer, pitch, height);
							frameDiffOneHns = frameDiffOneHns - static_cast<s32>(c_sceVideoFrameDurationOneHNs);
						}
#endif
					}
					else
					{
						videoAssertf( false, "Unknown buffer type %u!", bufferType );
					}
				}

				threadData.m_buffers.ReleaseBuffer( buffer );		
			}

			threadData.m_encoderState = ENCODER_CLEANUP;

			// Flag audio thread to shut down in-case of failure conditions that mean we never flagged it for shutdown
			// in the above loop
			threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;

			//! Wait until the audio thread has confirmed it is safe to shut down
			while( ( threadData.m_subThreadMessaging & ENC_THREAD_FLAGS_SUB_SHUTDOWN_CONFIRM ) == 0 )
			{
				sysIpcSleep( 1 );
			}

			allFrameWritesValid = allFrameWritesValid && ( ( threadData.m_subThreadMessaging & ENC_THREAD_FLAGS_SUB_HAD_ERRORS ) == 0 );

			bool const c_cleanupSucceeded = true;//CleanupRecording( sinkWriter );	
			threadData.m_errorState = c_cleanupSucceeded ? threadData.m_errorState : MediaCommon::MEDIA_ERROR_UNKNOWN;

#if FRAME_RATE_VARIABLE
			videoDisplayf( "Video Encode %s. Exported Duration %lu milliseconds, raw frame duration %lu milliseconds", 
				c_cleanupSucceeded ? "was successful" : "failed",
				ONE_H_NS_UNITS_TO_MILLISECONDS( sceOutputDurationOneHns ), ONE_H_NS_UNITS_TO_MILLISECONDS( threadData.m_currentVideoFrameOneHNs ) );
#else
			videoDisplayf( "Video Encode %s. Raw frame duration %lu milliseconds", 
				c_cleanupSucceeded ? "was successful" : "failed",
				 NANOSECONDS_TO_MILLISECONDS( threadData.m_currentVideoFrameNs ) );
#endif
		}
		else
		{
			threadData.m_errorState = MediaCommon::MEDIA_ERROR_OOM;
			threadData.m_threadMessaging |= ENC_THREAD_FLAGS_MAIN_SHUTTING_DOWN;
		}

		threadData.m_encoderState = ENCODER_CLEANUP;

		threadData.m_buffers.Shutdown();

		threadData.m_encoderState = ENCODER_PENDING_RESET;
	}

	static bool UpdateAudioFrameCapture( MediaBufferBase*& buffer, MediaEncoderBuffers& bufferOwner )
	{
		bool success = false;

		if( videoVerifyf( buffer, "UpdateAudioFrameCapture - Null audio buffer provided!" ) )
		{
			MediaBufferAudioEncode* audioBuffer = static_cast<MediaBufferAudioEncode*>(buffer);

			MediaBufferBlobInterfaceRage& bufferInterface = audioBuffer->GetExternalBufferInterface();
			u8* externalBuffer( bufferInterface.GetBytes() );
			if (externalBuffer)
			{
				// Sony encoding requires exactly 1024 samples per channel (interleaved) to be submitted at once.
				u32 numSamples = MediaCommon::sc_audioChannels * 1024;

				if( videoVerifyf( numSamples * MediaCommon::sc_bytesPerAudioSample == audioBuffer->GetSize(), "Audio buffer size differs from size of data being submitted" ) )
				{				
					if( videoVerifyf( MediaCommon::sc_bitsPerGameAudioSample == 16, "Expected 16 bit audio data" ) )
					{
						// We (should) have been provided with 16-bit audio data, but Sony encoder requires floating point values.
						f32* floatSampleBuffer = Alloca(f32, numSamples);
						rage::CopyMonoBufferToMonoMixBuf(floatSampleBuffer, (s16*)externalBuffer, numSamples);

						void *addr;
						int ret = sceVideoRecordingGetInfo(SCE_VIDEO_RECORDING_INFO_AUDIO_INPUT_LOCK, &addr, sizeof(addr));
						if (ret == SCE_OK)
						{
							memcpy(addr, floatSampleBuffer, sizeof(float) * numSamples);

							ret = sceVideoRecordingSetInfo(SCE_VIDEO_RECORDING_INFO_AUDIO_INPUT_UNLOCK, 0, 0);
							if (ret == SCE_OK)
							{
								success = true;
							}
						}
					}
				}
			}

			bufferOwner.ReleaseBuffer( buffer );
		}

		return success;
	}

	void MediaEncoder::AudioThreadFunc( MediaEncoder::ThreadData& threadData )
	{
		GenerateDummyAudioData();

 		MediaBufferBase* buffer( NULL );
		bool frameWritesValid = true;

		while( frameWritesValid && ( !threadData.m_killFlag || threadData.m_drainFlag ) )
		{
			GetNextMediaBuffer( threadData.m_buffers, buffer, false );

			if( threadData.m_buffers.IsEOFBuffer( buffer ) )
			{
				threadData.m_buffers.ReleaseBuffer( buffer );
				break;
			}

			u64 const bufferCreationTimeNs = buffer->GetCreationTimeNs();
			u64 const c_bufferDurationNs = buffer->GetDurationNs();

			bool discardBuffer = ( bufferCreationTimeNs < threadData.m_startTimeNs ) || 
				( threadData.m_pauseFlag && bufferCreationTimeNs >= threadData.m_pauseTimeNs ) ||
				( threadData.m_killFlag && !threadData.m_drainFlag ) ||
				( threadData.m_killFlag && threadData.m_drainFlag && bufferCreationTimeNs >= threadData.m_killTimeNs );

			if( !discardBuffer )
			{
				//u64 const c_bufferDurationOneHNs = NANOSECONDS_TO_ONE_H_NS_UNITS( c_bufferDurationNs );
				MediaBufferBase::eBufferType const bufferType = buffer->GetType();

				if( bufferType == MediaBufferBase::BUFFER_AUDIO )
				{
					frameWritesValid = UpdateAudioFrameCapture( buffer, threadData.m_buffers );

					if( !frameWritesValid )
					{
						threadData.m_errorState = MediaCommon::MEDIA_ERROR_OOM;
					}

					threadData.m_currentAudioFrameNs += c_bufferDurationNs;
				}
				else
				{
					videoAssertf( false, "Unknown buffer type %u!", bufferType );
				}
			}

			threadData.m_buffers.ReleaseBuffer( buffer );
		}

		threadData.m_subThreadMessaging |= frameWritesValid ? 0 : ENC_THREAD_FLAGS_SUB_HAD_ERRORS;
		threadData.m_subThreadMessaging |= ENC_THREAD_FLAGS_SUB_SHUTDOWN_CONFIRM;
	}

} // namespace rage

#endif // USE_MEDIA_ENCODER && !__RESOURCECOMPILER
#endif // RSG_ORBIS
