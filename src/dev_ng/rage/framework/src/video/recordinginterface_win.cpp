/////////////////////////////////////////////////////////////////////////////////
//
// FILE    : recordinginterface_win.cpp
// PURPOSE : Windows implementation of the recording interface
//
// AUTHOR  : james.strain
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef __RGSC_DLL
#define __RGSC_DLL 0
#endif

#if (RSG_PC || RSG_DURANGO) && !__RESOURCECOMPILER && !__RGSC_DLL

// rage
#include "file/limits.h"
#include "recordinginterface_win.h"
#include "string/string.h"
#include "system/ipc.h"
#include "system/interlocked.h"
#include "system/pix.h"

namespace rage
{

atFixedString< RAGE_MAX_PATH > VideoRecordingInterfaceWindows::ms_outputPath;

VideoRecordingInterface *VideoRecordingInterface::Create()
{
	return rage_new VideoRecordingInterfaceWindows;
}

//----------------------------------------------------------------------
//	RecordingTaskWindows
//----------------------------------------------------------------------
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
sysIpcThreadId	RecordingTaskWindows::sm_WorkerThread = NULL;
sysMessageQueue<RecordingTaskWindows::sWorkerData, 8, true> RecordingTaskWindows::sm_WorkerCommandQueue;
#endif

IVideoRecordingDataProvider* RecordingTaskWindows::sm_recordingDataProvider = NULL;


RecordingTaskWindows::RecordingTaskWindows( char const * const outputPath ) 
	: RecordingTask()
	, m_encoder()
	, m_cancelled( false )
	, m_outputPath( outputPath )
{
	videoAssertf( m_outputPath, "RecordingTaskWindows Constructor - NULL output path provided!" );
}

RecordingTaskWindows::~RecordingTaskWindows()
{
	
}

void RecordingTaskWindows::StartRecording( const char* pName, bool const startPaused, bool const synchronized, bool const hasModdedContent )
{
	m_ClipName = pName;
	m_startPaused = startPaused;
	m_synchronized = synchronized;
	m_hasModdedContent = hasModdedContent;
	m_cancelled = false;

	// We're starting, add a ref so we can't be cleaned up
	IncRefCount();

	m_RecordState = RECORD_STATE_RECORDING_STARTED;

#if REC_TASK_DELAY_TO_RENDER_SYNC
	m_NextCommand = ACTION_START_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	sWorkerData newData = { this, ACTION_START_RECORDING };
	sm_WorkerCommandQueue.Push(newData);
#else
	ProcessAction( ACTION_START_RECORDING );
#endif

#endif


}

void RecordingTaskWindows::PauseRecording()
{
	if( videoVerifyf( m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to pause a video that wasn't started" ) )
	{
#if REC_TASK_DELAY_TO_RENDER_SYNC
		m_NextCommand = ACTION_PAUSE_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		sWorkerData newData = { this, ACTION_PAUSE_RECORDING };
		sm_WorkerCommandQueue.Push(newData);
#else
		ProcessAction( ACTION_PAUSE_RECORDING );
#endif

#endif
	}
}



void RecordingTaskWindows::ResumeRecording()
{
	if( videoVerifyf( m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to resume a video that wasn't started" ) )
	{
#if REC_TASK_DELAY_TO_RENDER_SYNC
		m_NextCommand = ACTION_RESUME_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		sWorkerData newData = { this, ACTION_RESUME_RECORDING };
		sm_WorkerCommandQueue.Push(newData);
#else
		ProcessAction( ACTION_RESUME_RECORDING );
#endif

#endif
	}
}

void RecordingTaskWindows::StopRecording()
{
	if( videoVerifyf( m_RecordState == RECORD_STATE_RECORDING_STARTED, "Trying to Stop a video that wasn't started" ) )
	{
#if REC_TASK_DELAY_TO_RENDER_SYNC
		m_NextCommand = ACTION_STOP_RECORDING;
#else

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
		sWorkerData newData = { this, ACTION_STOP_RECORDING };
		sm_WorkerCommandQueue.Push(newData);
#else
		ProcessAction( ACTION_STOP_RECORDING );
#endif

#endif
	}
}

void RecordingTaskWindows::CancelRecording()
{
	m_cancelled = true;
	StopRecording();
}

void RecordingTaskWindows::Update()
{
#if REC_TASK_DELAY_TO_RENDER_SYNC
	if (m_NextCommand != ACTION_INVALID)
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD	
	{
		sWorkerData newData = {this, m_NextCommand };
		sm_WorkerCommandQueue.Push(newData);
	}
#else
	{
		ProcessAction( m_NextCommand );
	}
#endif
		m_NextCommand = ACTION_INVALID;
#endif

	UpdateErrorCode();
}

void RecordingTaskWindows::UpdateErrorCode()
{
	MediaEncoder& encoder( GetEncoder() );

	switch( encoder.GetLastError() )
	{
	case MediaCommon::MEDIA_ERROR_OOM:
		{
			m_errorState = ERROR_STATE_OOM;
			break;
		}
	case MediaCommon::MEDIA_ERROR_ACCESS_DENIED:
		{
			m_errorState = ERROR_STATE_ACCESS_DENIED;
			break;
		}
	case MediaCommon::MEDIA_ERROR_SHARING_VIOLATION:
		{
			m_errorState = ERROR_STATE_SHARING_VIOLATION;
			break;
		}
	case MediaCommon::MEDIA_ERROR_NONE:
		{
			m_errorState = ERROR_STATE_NONE;
			break;
		}
	case MediaCommon::MEDIA_ERROR_UNKNOWN:
	default:
		{
			m_errorState = ERROR_STATE_UNKNOWN;
			break;
		}
	}
}

bool RecordingTaskWindows::IsPaused() const
{
	return m_encoder.IsPaused();
}

bool RecordingTaskWindows::IsReadyForSamples() const
{
	return m_encoder.IsReadyToProcessSamples();
}

bool RecordingTaskWindows::IsAudioFrameCaptureAllowed() const
{
	return m_encoder.IsAudioFrameCaptureAllowed();
}

void RecordingTaskWindows::PushVideoFrame( grcRenderTarget const& source, float const frameDuration )
{
	m_encoder.PushVideoFrame( (grcTexture&)source, frameDuration );
}

#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD

void RecordingTaskWindows::InitWorkerThread()
{
	videoAssertf( sm_WorkerThread == NULL, "RecordingTaskWindows::InitWorkerThread - Previous thread still exists!" );
	if( sm_WorkerThread == NULL )
	{
		sm_WorkerThread = sysIpcCreateThread( &RecorderWorkerThread, NULL, 1024, PRIO_BELOW_NORMAL, "RecorderWorkerThread", 0, "RecorderWorkerThread" );
	}
}

void RecordingTaskWindows::ShutdownWorkerThread()
{
	RecordingTaskWindows::sWorkerData newData = { NULL, ACTION_INVALID };
	sm_WorkerCommandQueue.Push(newData);
	if( sm_WorkerThread )
	{
		sysIpcWaitThreadExit( sm_WorkerThread );
		sm_WorkerThread = NULL;
	}
}

void RecordingTaskWindows::RecorderWorkerThread( void *UNUSED_PARAM(ptr) )
{
	RecordingTaskWindows*	ownerTask( NULL );
	sWorkerData				workerData;
	
	videoDisplayf("Starting RecordingTaskWindows::RecorderWorkerThread()");

	while( true )
	{
		workerData				= sm_WorkerCommandQueue.Pop();
		ownerTask				= workerData.m_task;

		//! No owner for this task? Abandon thread!
		if( ownerTask == NULL )
		{
			break;
		} 

		ownerTask->ProcessAction( workerData.m_action );
	}
}

#endif // defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD

void RecordingTaskWindows::ProcessAction( eWorkerAction const action )
{
	MediaEncoder& encoder = GetEncoder();

	switch( action )
	{
	case ACTION_START_RECORDING:
		{
			PF_PUSH_MARKER( "RecordingTaskWindows - ACTION_START_RECORDING" );
			videoDisplayf("RecordingTaskWindows - ACTION_START_RECORDING");

			bool success = false;

			videoAssertf( !encoder.IsCapturing(), "RecordingTaskWindows::RecorderWorkerThread - Encoder already in use." );
			if( !encoder.IsCapturing() )
			{
				char const * clipName = GetClipName();
				char const * path = m_outputPath;
#if __BANK
				path = m_bSaveToDisc ? "D:\\" : path;
#endif
				MediaEncoderParams encodingParams;

				if( sm_recordingDataProvider )
				{
                    u32 gameWidth, gameHeight;
                    sm_recordingDataProvider->GetGameFramebufferDimensions( gameWidth, gameHeight );
					encodingParams.SetOutputDimensions( gameWidth, gameHeight );
					encodingParams.SetOutputQuality( sm_recordingDataProvider->GetRecordingQualityLevel() );
					encodingParams.SetOutputFps( sm_recordingDataProvider->GetRecordingFps() );
					encodingParams.SetOutputFormat( sm_recordingDataProvider->GetFormatRequested() );
					encodingParams.SetEstimatedDurationMs( sm_recordingDataProvider->GetEstimatedDurationMs() );

					encodingParams.SetGameTitle( sm_recordingDataProvider->GetGameTitle() );
					encodingParams.SetCopyright( sm_recordingDataProvider->GetCopyright() );
#if RSG_PC
					encodingParams.SetModdedContent( m_hasModdedContent );
#endif
				}

				success = videoVerifyf( encodingParams.IsValid(), "RecordingTaskWindows::ProcessAction - Invalid encoder parameters specified" ) ? 
					encoder.StartCapture( clipName, path, m_startPaused, m_synchronized, encodingParams ) : false;
			}

			if( !success )
			{
				videoDisplayf( "RecordingTaskWindows - ACTION_START_RECORDING Failed. See above logs for more detail." );
				m_errorState = ERROR_STATE_UNKNOWN;
				DecRefCount();
			}

			PF_POP_MARKER();

			break;
		}

	case ACTION_PAUSE_RECORDING:
		{
			PF_PUSH_MARKER( "RecordingTaskWindows - ACTION_PAUSE_RECORDING" );
			videoDisplayf("RecordingTaskWindows - ACTION_PAUSE_RECORDING");

			if( !encoder.IsPaused() )
			{
				encoder.PauseCapture();
			}

			PF_POP_MARKER();

			break;
		}

	case ACTION_RESUME_RECORDING:
		{
			PF_PUSH_MARKER( "RecordingTaskWindows - ACTION_RESUME_RECORDING" );
			videoDisplayf("RecordingTaskWindows - ACTION_RESUME_RECORDING");

			if( encoder.IsPaused() )
			{
				encoder.ResumeCapture();
			}

			PF_POP_MARKER();

			break;
		}

	case ACTION_STOP_RECORDING:
		{
			PF_PUSH_MARKER( "RecordingTaskWindows - ACTION_STOP_RECORDING" );
			videoDisplayf("RecordingTaskWindows - ACTION_STOP_RECORDING");

			videoAssertf( encoder.IsCapturing(), "RecordingTaskWindows::RecorderWorkerThread - Encoder Stopping encoder without video recording!" );

			UpdateErrorCode();

			if( encoder.IsCapturing() &&
				encoder.StopCapture( m_cancelled ) )
			{
				m_RecordState = HasErrorCode() || m_cancelled ?  RECORD_STATE_CANCELLED : RECORD_STATE_SUCCESS;
			}

			DecRefCount();

			PF_POP_MARKER();

			break;
		}

	default:
		{
			videoAssertf( false, "RecordingTaskWindows - Unsupported action %d requested", action );
			break;
		}
	}
}

//----------------------------------------------------------------------
//	VideoRecordingInterfaceWindows
//----------------------------------------------------------------------
VideoRecordingInterfaceWindows::VideoRecordingInterfaceWindows() 
	: VideoRecordingInterface()
{
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	RecordingTaskWindows::InitWorkerThread();
#endif
}

VideoRecordingInterfaceWindows::~VideoRecordingInterfaceWindows()
{
#if defined(REC_TASK_USE_WORKER_THREAD) && REC_TASK_USE_WORKER_THREAD
	RecordingTaskWindows::ShutdownWorkerThread();
#endif
}

void VideoRecordingInterfaceWindows::EnableRecording()
{
	
}

void VideoRecordingInterfaceWindows::DisableRecording()
{
	
}

RecordingTask * VideoRecordingInterfaceWindows::CreateRecordingTask()
{
	return 	rage_new RecordingTaskWindows( GetOutputPath() );
}

} // namespace rage

#endif // RSG_PC && !__RESOURCECOMPILER
