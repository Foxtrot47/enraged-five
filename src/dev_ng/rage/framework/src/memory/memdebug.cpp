
#include "memdebug.h"

#if RAGE_MEMORY_DEBUG
#include "atl/pool.h"
#include "fragment/manager.h"
#include "streaming/streamingdefs.h"
#include "streaming/streamingengine.h"
#include "system/memory.h"
#include "system/memmanager.h"
#include "system/simpleallocator.h"

#if RSG_ORBIS
#include <kernel.h>
#endif

#if RSG_DURANGO
#include "system/xtl.h"
#endif

namespace rage {

#if __XENON
extern sysMemAllocator* g_pXenonPoolAllocator;
#endif

#if COMMERCE_CONTAINER 
extern sysMemAllocator* g_pResidualAllocator;
#endif

void sysMemDebug::PrintValue(const char* pszTitle, void* const ptr, s32 usedBytes, size_t freeBytes, size_t totalBytes, size_t highBytes, size_t fragPercent) const
{
	Assert(pszTitle && ptr);

	Memoryf("%24s %8d %8" SIZETFMT "d %8" SIZETFMT "d %8" SIZETFMT "d %5" SIZETFMT "d%%  0x%p - 0x%p\n", 
		pszTitle, 
		(usedBytes >> 10), 
		(freeBytes >> 10), 
		(totalBytes >> 10), 
		(highBytes >> 10),
		fragPercent,
		ptr, 
		static_cast<u8*>(ptr) + totalBytes);
}

void sysMemDebug::PrintValue(const char* pszTitle, size_t usedBytes, size_t freeBytes, size_t totalBytes) const
{
	Assert(pszTitle);

	Memoryf("%24s %8" SIZETFMT "d %8" SIZETFMT "d %8" SIZETFMT "d\n", 
		pszTitle, 
		(usedBytes >> 10), 
		(freeBytes >> 10), 
		(totalBytes >> 10));
}

void sysMemDebug::PrintValue(const char* pszTitle, size_t usedBytes) const
{
	Assert(pszTitle);

	Memoryf("%24s %8" SIZETFMT "d\n", 
		pszTitle, 
		(usedBytes >> 10));
}

void sysMemDebug::PrintAllocator(const char* pszTitle, sysMemAllocator* pAllocator)
{
	if (!pAllocator)
		return;

	Memoryf("%24s %8" SIZETFMT "d %8" SIZETFMT "d %8" SIZETFMT "d %8" SIZETFMT "d %5" SIZETFMT "d%%  0x%p - 0x%p\n", 
		pszTitle, 
		(pAllocator->GetMemoryUsed() >> 10), 
		(pAllocator->GetMemoryAvailable() >> 10), 
		(pAllocator->GetHeapSize() >> 10), 
		(pAllocator->GetHighWaterMark(false) >> 10), 
		pAllocator->GetFragmentation(), 
		pAllocator->GetHeapBase(), 		
		static_cast<u8*>(pAllocator->GetHeapBase()) + pAllocator->GetHeapSize());
}

#if MEMORY_TRACKER
void sysMemDebug::PrintTracker(const char* pszTitle, sysMemSystemTracker* pTracker)
{
	if (!pTracker)
		return;

	Memoryf("%24s %8" SIZETFMT "d %8" SIZETFMT "d %8" SIZETFMT "d\n", 
		pszTitle, 
		(pTracker->GetUsed() >> 10), 
		(pTracker->GetAvailable() >> 10), 
		(pTracker->GetSize() >> 10));
}
#endif

#if RESOURCE_POOLS
void sysMemDebug::PrintPool(const char* pszName, sysBuddyPool* pPool)
{
	if (!pPool)
		return;

	Memoryf("%24s %8" SIZETFMT "u %8" SIZETFMT "u %8" SIZETFMT "u %8" NOTFINAL_ONLY(SIZETFMT "u %8") SIZETFMT "u\n", 
		pszName, 
		pPool->GetUsedSize(), 
		pPool->GetSize(), 
		pPool->GetStorageSize(),
		NOTFINAL_ONLY(pPool->GetPeakSize(),)
		(pPool->GetSize() * pPool->GetStorageSize()) / 1024);
}
#endif

#if RAGE_POOL_TRACKING
void sysMemDebug::PrintPool(PoolTracker* pTracker)
{
	if (!pTracker)
		return;

	Memoryf("%48s %8d %8" SIZETFMT "d %8d %8d %8d %8d %8d 0x%p %48s\n", 
		pTracker->GetName(), 
		pTracker->GetActualMemoryUse() >> 10,
		pTracker->GetStorageSize(),		
		pTracker->GetSize(),
		pTracker->GetSpilloverSize(),
		pTracker->GetPeakSlotsUsed(),
		pTracker->GetNoOfUsedSpaces(),
		pTracker->GetNoOfFreeSpaces(),
		pTracker->GetRawPoolPointer(),
		pTracker->GetClassName());
}
#endif

#if ENABLE_STORETRACKING
void sysMemDebug::PrintStore(fwAssetStore<char>* pStore)
{
	if (!pStore)
		return;

	Memoryf("%32s %8" SIZETFMT "d %8" SIZETFMT "d %8d %8d %8d %8d\n", 
		pStore->GetModuleName(), 
		(static_cast<size_t>(pStore->GetMaxSize()) * pStore->GetStorageSize()) >> 10,
		pStore->GetStorageSize(),		
		pStore->GetMaxSize(),
		pStore->GetPeakSlotsUsed(),
		pStore->GetNumUsedSlots(),
		pStore->GetNoOfFreeSpaces());
}
#endif

void sysMemDebug::PrintAllMemory()
{
	PrintAllocators();

#if API_HOOKER
	PrintValue("g_virtualAllocTotal", MEMMANAGER.m_virtualAllocTotal);
	PrintValue("g_heapAllocTotal", MEMMANAGER.m_heapAllocTotal);
#endif

//#if RAGE_POOL_TRACKING
//	PrintPools();
//#endif

//#if ENABLE_STORETRACKING
//	PrintStores();
//#endif

	//PrintSmAllocators();
}

void sysMemDebug::PrintAllocators()
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// ALLOCATORS
	//
	Memoryf("\n%24s %8s %8s %8s %8s %6s  %s\n", "CPU ALLOCATOR", "USED KB", "FREE KB", "TOTAL KB", "PEAK KB", "FRAG %", "ADDRESS RANGE");

	// Game Virtual
	sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
	PrintAllocator("Game Virtual", pAllocator);

	sysMemSimpleAllocator* pSimpleAllocator = reinterpret_cast<sysMemSimpleAllocator*>(pAllocator);
	size_t usedBytes = pSimpleAllocator->GetSmallocatorTotalSize();
	PrintValue("Smallocator", usedBytes);

	// Debug Virtual
#if ENABLE_DEBUG_HEAP
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL);
	PrintAllocator("Debug Virtual", pAllocator);

	PrintAllocator("Recorder", MEMMANAGER.GetRecorderAllocator());
#endif

	// Hemlis Virtual
#if !__FINAL && (RSG_ORBIS || RSG_DURANGO)
	PrintAllocator("Hemlis Virtual", MEMMANAGER.GetHemlisAllocator());
#endif

	// Header Virtual
#if RESOURCE_HEADER
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);
	PrintAllocator("Header Virtual", pAllocator);

	PrintValue("Header Virtual Pool", sysMemManager::GetInstance().GetUsedHeaders() << 10, (sysMemManager::GetInstance().GetTotalHeaders() - sysMemManager::GetInstance().GetUsedHeaders()) << 10, sysMemManager::GetInstance().GetTotalHeaders() << 10);
#endif

	// Resource Virtual
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	PrintAllocator("Resource Virtual", pAllocator);

	// Streaming Virtual
#if ONE_STREAMING_HEAP
	usedBytes = (size_t) strStreamingEngine::GetAllocator().GetPhysicalMemoryUsed();
	size_t totalBytes = (size_t) strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable();
#else
	usedBytes = (size_t) strStreamingEngine::GetAllocator().GetVirtualMemoryUsed();
	size_t totalBytes = (size_t) strStreamingEngine::GetAllocator().GetVirtualMemoryAvailable();
#endif

	WIN32PC_ONLY(usedBytes = strStreamingEngine::GetAllocator().GetMemoryUsed());
	WIN32PC_ONLY(totalBytes = strStreamingEngine::GetAllocator().GetMemoryAvailable());

	size_t freeBytes = totalBytes - usedBytes;
	PrintValue("Streaming Virtual", usedBytes, freeBytes, totalBytes);

	// External Virtual
#if ONE_STREAMING_HEAP
	usedBytes = (s32) strStreamingEngine::GetAllocator().GetExternalPhysicalMemoryUsed();	
#else
	usedBytes = (s32) strStreamingEngine::GetAllocator().GetExternalVirtualMemoryUsed();
#endif

	PrintValue("External Virtual", usedBytes);

	// Slosh Virtual
	PrintValue("Slosh Virtual", (s32) pAllocator->GetMemoryAvailable() - freeBytes);

	// Frag Cache
	pAllocator = (sysMemAllocator*)fragManager::GetFragCacheAllocator();
	PrintAllocator("Frag Cache", pAllocator);

#if RSG_ORBIS
	// Flex 
	PrintAllocator("Flex", MEMMANAGER.GetFlexAllocator());
#endif

#if RSG_ORBIS || RSG_DURANGO || RSG_PC
	// Replay 
	PrintAllocator("Replay", MEMMANAGER.GetReplayAllocator());
#endif

#if __XENON
	// Pool Virtual
	if (g_pXenonPoolAllocator)
		PrintAllocator("Pool Virtual", g_pXenonPoolAllocator);
#endif

#if SCRATCH_ALLOCATOR
	sysMemStack* pStack = sysMemManager::GetInstance().GetScratchAllocator();
	PrintAllocator("Physics Scratch", pStack);
#endif

#if MEMORY_TRACKER
	// System
	sysMemSystemTracker* pTracker = sysMemManager::GetInstance().GetSystemTracker();
	PrintTracker("System", pTracker);
#elif RSG_ORBIS
	size_t used = 0;
	int32_t result = 0;
	void* pAddress = NULL;
	SceKernelVirtualQueryInfo info;

	while ((result = sceKernelVirtualQuery(pAddress, SCE_KERNEL_VQ_FIND_NEXT, &info, sizeof(SceKernelVirtualQueryInfo))) != SCE_KERNEL_ERROR_EACCES)
	{
		pAddress = info.end;
		const size_t size = reinterpret_cast<size_t>(info.end) - reinterpret_cast<size_t>(info.start);				
		used += size;
	}

	PrintValue("System", used, sceKernelGetDirectMemorySize() - used, sceKernelGetDirectMemorySize());
#elif RSG_DURANGO
	TITLEMEMORYSTATUS status;
	status.dwLength =  sizeof(TITLEMEMORYSTATUS);
	TitleMemoryStatus(&status);
	PrintValue("System", status.ullTotalMem - status.ullAvailMem, status.ullAvailMem, status.ullTotalMem);
#endif

#if COMMERCE_CONTAINER
	// Residual
	if (g_pResidualAllocator)
		PrintAllocator("Residual", g_pResidualAllocator);

	// Flex
	pAllocator = sysMemManager::GetInstance().GetFlexAllocator()->GetPrimaryAllocator();
	PrintAllocator("Flex", pAllocator);
	PrintValue("Move Ped", sysMemManager::GetInstance().GetMovePedMemory(), MOVEPED_HEAP_SIZE, 0, MOVEPED_HEAP_SIZE, MOVEPED_HEAP_SIZE, 0);
#endif

#if COMMERCE_CONTAINER
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// VRAM ALLOCATORS
	//
	Memoryf("\n");
	Memoryf("%24s %8s %8s %8s %8s %6s  %s\n", "GPU ALLOCATOR", "USED KB", "FREE KB", "TOTAL KB", "PEAK KB", "FRAG %", "ADDRESS RANGE");

	// Game Physical
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_PHYSICAL);
	PrintAllocator("Game Physical", pAllocator);

	// Resource Physical
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	PrintAllocator("Resource Physical", pAllocator);

#ifdef ONE_STREAMING_HEAP
	// Streaming Physical
	usedBytes = strStreamingEngine::GetAllocator().GetPhysicalMemoryUsed();
	totalBytes = strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable();
	freeBytes = totalBytes - usedBytes;

	PrintValue("Streaming Physical", usedBytes, freeBytes, totalBytes);

	// External Physical
	usedBytes = (s32) strStreamingEngine::GetAllocator().GetExternalPhysicalMemoryUsed();
	PrintValue("External Physical", usedBytes);

	// Slosh Physical
	PrintValue("Slosh Physical", pAllocator->GetMemoryAvailable() - freeBytes);
#endif
#endif

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// XTL ALLOCATOR
	//
#if MEMORY_TRACKER
	Memoryf("\n");
	sysMemSystemTracker* pSystemTracker = sysMemManager::GetInstance().GetSystemTracker();
	pSystemTracker->Print();
#endif

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// POOLS
	//
#if RESOURCE_POOLS
	Memoryf("\n");
	Memoryf("%24s %8s %8s %8s %8s %8s\n", "Pool", "USED", "PEAK", "TOTAL", "SIZE", "TOTAL KB");

	PS3_ONLY(const char* pszTitle[] = {"Resource Virtual 4K Pool", "Resource Virtual 8K Pool"};)
	XENON_ONLY(const char* pszTitle[] = {"Resource Virtual 8K Pool", "Resource Virtual 16K Pool"};)

	sysMemBuddyAllocator* pBuddyAllocator = (sysMemBuddyAllocator*) sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	PrintPool(pszTitle[0], pBuddyAllocator->GetPool(0));
	PrintPool(pszTitle[1], pBuddyAllocator->GetPool(1));
#endif // __PS3 || __XENON
}

void sysMemDebug::PrintSmAllocators()
{
#if !__NO_OUTPUT
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// SMALLOCATORS
	//
	Memoryf("\nSM ALLOCATORS");
	sysMemSimpleAllocator* pAllocator = reinterpret_cast<sysMemSimpleAllocator*>(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL));
	pAllocator->DumpSMAllocators();
#endif
}

#if RAGE_POOL_TRACKING
typedef PoolTracker* PoolTrackerEntry;
s32 OrderPoolByName(const PoolTrackerEntry* ppPool1, const PoolTrackerEntry* ppPool2)
{
	const char* pszName1 = (*ppPool1)->GetName();
	const char* pszName2 = (*ppPool2)->GetName();
	return stricmp(pszName1, pszName2);
};

void sysMemDebug::PrintPools()
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// POOLS
	//
	Memoryf("\n%48s %8s %8s %8s %8s %8s %8s %8s %s %48s\n", "POOL_NAME", "TOTAL_KB", "STORAGE", "TOTAL", "SPILLOVER", "PEAK", "USED", "FREE", "ADDRESS", "CLASS_NAME");

	atArray<PoolTracker*>& trackers = PoolTracker::GetTrackers();
	trackers.QSort(0, -1, OrderPoolByName);
	for (int i = 0; i < trackers.GetCount(); ++i)
	{
		PoolTracker* pTracker = trackers[i];
		if (pTracker && pTracker->GetName())
			PrintPool(pTracker);
	}
}
#endif // RAGE_POOL_TRACKING

#if ENABLE_STORETRACKING
typedef fwAssetStoreBase* StoreEntry;
s32 OrderStoreByName(const StoreEntry* ppStore1, const StoreEntry* ppStore2)
{
	const char* pszName1 = (*ppStore1)->GetModuleName();
	const char* pszName2 = (*ppStore2)->GetModuleName();
	return stricmp(pszName1, pszName2);
};

void sysMemDebug::PrintStores()
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// STORES
	//
	Memoryf("\n%32s %8s %8s %8s %8s %8s %8s\n", "STORE NAME", "TOTAL KB", "STORAGE", "TOTAL", "PEAK", "USED", "FREE");
	
	g_storesTrackingArray.QSort(0, -1, OrderStoreByName);
	for (int i = 0; i < g_storesTrackingArray.GetCount(); ++i)
	{
		fwAssetStore<char>* pStore = (fwAssetStore<char>*) g_storesTrackingArray[i];
		if (pStore && pStore->GetModuleName())
			PrintStore(pStore);
	}
}
#endif // ENABLE_STORETRACKING

} // namespace rage

#endif // RAGE_MEMORY_DEBUG
