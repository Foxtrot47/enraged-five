
#ifndef SYSTEM_MEMDEBUG_H
#define SYSTEM_MEMDEBUG_H

#include "system/memory.h"

#if RAGE_MEMORY_DEBUG
#include "atl/pool.h"
#include "fwtl/assetstore.h"
#include "fwtl/pool.h"
#include "system/poolallocator.h"
#include "system/buddyallocator.h"

#if MEMORY_TRACKER
#include "system/systemallocator_system.h"
#endif

namespace rage {

#define MEMDEBUG sysMemDebug::GetInstance()

class sysMemDebug
{
private:
	sysMemDebug() {}

	void PrintValue(const char* pszTitle, void* const ptr, s32 usedBytes, size_t freeBytes, size_t totalBytes, size_t highBytes, size_t fragPercent) const;
	void PrintValue(const char* pszTitle, size_t usedBytes, size_t freeBytes, size_t totalBytes) const;
	void PrintValue(const char* pszTitle, size_t usedBytes) const;
	
#if RESOURCE_POOLS
	void PrintPool(const char* pszName, sysBuddyPool* pPool);
#endif

#if RAGE_POOL_TRACKING
	void PrintPool(PoolTracker* pTracker);
#endif

#if ENABLE_STORETRACKING
	void PrintStore(fwAssetStore<char>* pStore);
#endif

public:
	inline static sysMemDebug& GetInstance()
	{
		static sysMemDebug s_instance;
		return s_instance;
	}

	void PrintAllMemory();
	void PrintAllocators();
	void PrintSmAllocators();
	void PrintAllocator(const char* pszTitle, sysMemAllocator* pAllocator);

#if MEMORY_TRACKER
	void PrintTracker(const char* pszTitle, sysMemSystemTracker* pTracker);
#endif

#if RAGE_POOL_TRACKING
	void PrintPools();
#endif

#if ENABLE_STORETRACKING
	void PrintStores();
#endif
};
} // namespace rage

#endif // RAGE_MEMORY_DEBUG

#endif // SYSTEM_MEMDEBUG_H