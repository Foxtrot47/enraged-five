//
// vfx/vfxwidget.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/vfxwidget.h"

// includes (rage)

// includes (framework)
#include "vfx/optimisations.h"


// optimisations
VFX_FW_GENERIC_OPTIMISATIONS()



// namespaces 
namespace rage {


#if __BANK
// static member variables
bkBank* vfxWidget::ms_pBank = NULL;
#endif

// code


} // namespace rage




