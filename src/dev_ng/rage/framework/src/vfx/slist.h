#ifndef VFX_SLIST_H
#define VFX_SLIST_H

#include "system/memops.h"

#include "int128.h"


namespace rage {

template<class PTRCONVERTER> class SListItemBase;


////////////////////////////////////////////////////////////////////////////////
//  SListAlignPtr
////////////////////////////////////////////////////////////////////////////////
// Storage for a pointer, but the storage alignment can be less than __alignof(void*)
template<class USERTYPE, unsigned ALIGN>
class SListAlignPtr
{
public:

	// No constructors or destructors, since this type needs to be supported inside a union

	inline operator USERTYPE*() const volatile { USERTYPE *ret; sysMemCpy(&ret, (const void*)m_ptr, sizeof(ret)); return ret; }
	inline operator uptr     () const volatile { uptr      ret; sysMemCpy(&ret, (const void*)m_ptr, sizeof(ret)); return ret; }
	inline void operator=(USERTYPE *ptr) volatile { sysMemCpy((void*)this, &ptr, sizeof(*this)); }
	inline void operator=(uptr      ptr) volatile { sysMemCpy((void*)this, &ptr, sizeof(*this)); }

private:

	CompileTimeAssert(sizeof(void*)%ALIGN == 0);
	typename UIntBits<ALIGN*8>::T m_ptr[sizeof(void*)/ALIGN];
};

// Specialization for the typical case of having the same alignment as a pointer
template<class USERTYPE>
class SListAlignPtr<USERTYPE, __alignof(void*)>
{
public:

	inline operator USERTYPE*() const volatile { return m_ptr; }
	inline operator uptr     () const volatile { return (uptr)m_ptr; }
	inline void operator=(USERTYPE *ptr) volatile { m_ptr=ptr; }
	inline void operator=(uptr      ptr) volatile { m_ptr=(USERTYPE*)ptr; }

private:

	USERTYPE *m_ptr;
};


////////////////////////////////////////////////////////////////////////////////
//  SListCastStorage
////////////////////////////////////////////////////////////////////////////////
// Because SListAlignPtr has no constructors, we need another way to cast a uptr
// to a storage type.

// Helper class since function templates don't allow partial specialization
template<class STORAGE>
struct SListCastStorageHelper
{
	static inline STORAGE Cast(uptr ptr)
	{
		return (STORAGE)ptr;
	}
};

template<class USERTYPE, unsigned ALIGN>
struct SListCastStorageHelper<SListAlignPtr<USERTYPE, ALIGN> >
{
	static inline SListAlignPtr<USERTYPE, ALIGN> Cast(uptr ptr)
	{
		SListAlignPtr<USERTYPE, ALIGN> s;
		s = ptr;
		return s;
	}
};

template<class STORAGE>
inline STORAGE SListCastStorage(uptr ptr)
{
	return SListCastStorageHelper<STORAGE>::Cast(ptr);
}


////////////////////////////////////////////////////////////////////////////////
//  SListDefaultAlignPtrConverter
////////////////////////////////////////////////////////////////////////////////
template<unsigned ALIGN>
class SListDefaultAlignPtrConverter
{
public:

	typedef SListAlignPtr<SListItemBase<SListDefaultAlignPtrConverter>, ALIGN> STORAGE;

	inline SListDefaultAlignPtrConverter() {}
	explicit inline SListDefaultAlignPtrConverter(SListItemBase<SListDefaultAlignPtrConverter> *) {}

	// Microsoft's compilers give warnings about unused parameters when a
	// functor is passed to a function, and the functor type's methods are
	// static
#	ifdef _MSC_VER
#		define NON_MSC_STATIC
#		define MSC_CONST        const
#	else
#		define NON_MSC_STATIC   static
#		define MSC_CONST
#	endif

	NON_MSC_STATIC inline STORAGE FromPtr(SListItemBase<SListDefaultAlignPtrConverter> *ptr, const volatile STORAGE&) MSC_CONST { STORAGE s; s=ptr; return s; }
	NON_MSC_STATIC inline SListItemBase<SListDefaultAlignPtrConverter> *ToPtr(const volatile STORAGE &val) MSC_CONST { return val; }
	NON_MSC_STATIC inline SListItemBase<SListDefaultAlignPtrConverter> *ToPtr(const          STORAGE &val) MSC_CONST { return val; }
	NON_MSC_STATIC inline SListItemBase<SListDefaultAlignPtrConverter> *ToPtr(               STORAGE &val) MSC_CONST { return val; }

#	undef NON_MSC_STATIC
#	undef MSC_CONST
};

typedef SListDefaultAlignPtrConverter<__alignof(void*)> SListDefaultPtrConverter;


////////////////////////////////////////////////////////////////////////////////
//  SListIdxPtrConverter
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class STORAGE_>
class SListIdxPtrConverter
{
public:

	typedef STORAGE_ STORAGE;

	explicit inline SListIdxPtrConverter(const USERTYPE *base) : adjustedBase(base-1) {}
	inline STORAGE FromPtr(const SListItemBase<SListIdxPtrConverter> *ptr, STORAGE) const { return ptr ? (STORAGE)(static_cast<const USERTYPE*>(ptr)-adjustedBase) : 0; }
	inline USERTYPE *ToPtr(STORAGE val) const { return val ? const_cast<USERTYPE*>(adjustedBase)+val : NULL; }

private:

	const USERTYPE *adjustedBase;
};


////////////////////////////////////////////////////////////////////////////////
//  MakeSListIdxPtrConverter
////////////////////////////////////////////////////////////////////////////////
template<class STORAGE, class USERTYPE>
static inline SListIdxPtrConverter<USERTYPE, STORAGE> MakeSListIdxPtrConverter(const USERTYPE *base)
{
	return SListIdxPtrConverter<USERTYPE, STORAGE>(base);
}


////////////////////////////////////////////////////////////////////////////////
//  SListDerivedStorage
////////////////////////////////////////////////////////////////////////////////
// This allows SList<USERTYPE,ALIGN,SListDefaultPtrConverter>::GetPtrPtrHead() and
// SListItem<USERTYPE,ALIGN,SListDefaultPtrConverter>::GetPtrPtrNext() to return
// USERTYPE**, rather than SListItemBase<SListDefaultPtrConverter>*.
template<class USERTYPE, unsigned ALIGN, class PTRCONVERTER_STORAGE>
struct SListDerivedStorage
{
	typedef PTRCONVERTER_STORAGE T;
};

template<class USERTYPE, unsigned ALIGN>
struct SListDerivedStorage<USERTYPE, ALIGN, SListAlignPtr<SListItemBase<SListDefaultAlignPtrConverter<ALIGN> >, ALIGN> >
{
	typedef USERTYPE *T;
};


////////////////////////////////////////////////////////////////////////////////
//  SListItemBase
////////////////////////////////////////////////////////////////////////////////
// Type agnostic singly-linked list item base class
template<class PTRCONVERTER_>
class SListItemBase
{
protected:

	typedef typename PTRCONVERTER_::STORAGE STORAGE;

	inline SListItemBase() {}
	inline ~SListItemBase() {}

public:

	typedef PTRCONVERTER_ PTRCONVERTER;

	inline void SetNext(SListItemBase *n, const PTRCONVERTER &conv=PTRCONVERTER()) { next = conv.FromPtr(n, next); }
	inline SListItemBase *GetNext(const PTRCONVERTER &conv=PTRCONVERTER()) const { return conv.ToPtr(next); }
	inline STORAGE *GetPtrPtrNext() { return &next; }
	inline void SetNextRaw(STORAGE n) { next = n; }
	inline STORAGE GetNextRaw() const { return next; }

private:

	STORAGE next;
};


////////////////////////////////////////////////////////////////////////////////
//  SListItemAlign
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, unsigned ALIGN, class PTRCONVERTER=SListDefaultAlignPtrConverter<ALIGN> >
class SListItemAlign : public SListItemBase<PTRCONVERTER>
{
protected:

	typedef typename SListDerivedStorage<USERTYPE, ALIGN, typename PTRCONVERTER::STORAGE>::T   STORAGE;

public:

	inline void SetNext(USERTYPE *n, const PTRCONVERTER &conv=PTRCONVERTER()) { SListItemBase<PTRCONVERTER>::SetNext(n, conv); }
	inline USERTYPE *GetNext(const PTRCONVERTER &conv=PTRCONVERTER()) const { return static_cast<USERTYPE*>(SListItemBase<PTRCONVERTER>::GetNext(conv)); }
	inline STORAGE *GetPtrPtrNext() { return (STORAGE*)SListItemBase<PTRCONVERTER>::GetPtrPtrNext(); }

protected:

	inline SListItemAlign() {}
	inline ~SListItemAlign() {}
};


////////////////////////////////////////////////////////////////////////////////
//  SListItem
////////////////////////////////////////////////////////////////////////////////
// Templated helper to give nicer interface.
// User type should derive from this type, eg.
//  class Foo : public SListItem<Foo>
template<class USERTYPE, class PTRCONVERTER=SListDefaultPtrConverter>
class SListItem : public SListItemAlign<USERTYPE, __alignof(void*), PTRCONVERTER>
{
};


////////////////////////////////////////////////////////////////////////////////
//  SListItemIndexed
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class STORAGE>
class SListItemIndexed : public SListItem<USERTYPE, SListIdxPtrConverter<USERTYPE, STORAGE> >
{
};


////////////////////////////////////////////////////////////////////////////////
//  SListBase
////////////////////////////////////////////////////////////////////////////////
// Type agnostic singly-linked list item base class
template<class PTRCONVERTER>
class SListBase
{
protected:

	typedef typename PTRCONVERTER::STORAGE STORAGE;
	typedef typename UIntBits<sizeof(STORAGE)*8>::T SIZETYPE;

	inline SListBase() { SetEmpty(); }
	inline ~SListBase() {}

public:

	inline void SetEmpty() { head=(uptr)0; size=0; }
	inline bool IsEmpty() const { return !size; }
	inline const SListItemBase<PTRCONVERTER> *Peek(const PTRCONVERTER &conv=PTRCONVERTER()) const { return conv.ToPtr(head); }
	inline SListItemBase<PTRCONVERTER> *Peek(const PTRCONVERTER &conv=PTRCONVERTER()) { return conv.ToPtr(head); }
	inline STORAGE *GetPtrPtrHead() { return &head; }
	inline SListItemBase<PTRCONVERTER> *Pop(const PTRCONVERTER &conv=PTRCONVERTER());
	inline SListItemBase<PTRCONVERTER> *ScanTail(const PTRCONVERTER &conv=PTRCONVERTER());
	inline void Push(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER());
	inline void Push(SListBase &other, const PTRCONVERTER &conv=PTRCONVERTER());
	inline void Push(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv=PTRCONVERTER());
	inline void AppendTail(SListItemBase<PTRCONVERTER> *currTail, SListItemBase<PTRCONVERTER> *appendFirst, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER());
	inline void AppendTail(SListItemBase<PTRCONVERTER> *currTail, SListBase &appendOther, const PTRCONVERTER &conv=PTRCONVERTER());
	inline void AppendTail(SListItemBase<PTRCONVERTER> *currTail, SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv=PTRCONVERTER());
	inline unsigned GetNumItems() const { return (unsigned)size; }
	inline unsigned CountNumItems(const PTRCONVERTER &conv=PTRCONVERTER()) const;
	inline void SetHead(SListItemBase<PTRCONVERTER> *h, const PTRCONVERTER &conv=PTRCONVERTER()) { head=conv.FromPtr(h, head); }
	inline void AddSize(int n) { size+=n; }
	inline void SetSize(unsigned n) { size=n; }

private:

	STORAGE     head;
	SIZETYPE    size;
};


////////////////////////////////////////////////////////////////////////////////
//  SListBase::Pop
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline SListItemBase<PTRCONVERTER> *SListBase<PTRCONVERTER>::Pop(const PTRCONVERTER &conv)
{
	SListItemBase<PTRCONVERTER> *h=conv.ToPtr(head);
	if(h)
	{
		head = conv.FromPtr(h->GetNext(conv), head);
		--size;
	}
	return h;
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::ScanTail
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline SListItemBase<PTRCONVERTER> *SListBase<PTRCONVERTER>::ScanTail(const PTRCONVERTER &conv)
{
	SListItemBase<PTRCONVERTER> *tail = conv.ToPtr(head);
	SListItemBase<PTRCONVERTER> *next = tail;
	while(next)
	{
		tail = next;
		next = next->GetNext(conv);
	}
	return tail;
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::Push
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void SListBase<PTRCONVERTER>::Push(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv)
{
	Assert(!!first ^ !last);
	Assert(!!first ^ !count);
	if(first)
	{
		last->SetNextRaw(head);
		head = conv.FromPtr(first, head);
		size += (SIZETYPE)count;
	}
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::Push
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void SListBase<PTRCONVERTER>::Push(SListBase &other, const PTRCONVERTER &conv)
{
	SListItemBase<PTRCONVERTER> *first = other.Peek(conv);
	SListItemBase<PTRCONVERTER> *last = other.ScanTail(conv);
	unsigned count = other.GetNumItems();
	Push(first, last, count, conv);
	other.SetEmpty();
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::Push
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void SListBase<PTRCONVERTER>::Push(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv)
{
	Push(n, n, 1, conv);
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::AppendTail
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void SListBase<PTRCONVERTER>::AppendTail(SListItemBase<PTRCONVERTER> *currTail, SListItemBase<PTRCONVERTER> *appendFirst, unsigned count, const PTRCONVERTER &conv)
{
	Assert(!!appendFirst ^ !count);
	if(appendFirst)
	{
		if(currTail)
		{
			Assert(!currTail->GetNext(conv));
			currTail->SetNext(appendFirst, conv);
		}
		else
		{
			head = conv.FromPtr(appendFirst, head);
		}
		size += (SIZETYPE)count;
	}
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::AppendTail
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void SListBase<PTRCONVERTER>::AppendTail(SListItemBase<PTRCONVERTER> *currTail, SListBase &appendOther, const PTRCONVERTER &conv)
{
	SListItemBase<PTRCONVERTER> *appendFirst = appendOther.Peek(conv);
	unsigned count = appendOther.GetNumItems();
	AppendTail(currTail, appendFirst, count, conv);
	appendOther.SetEmpty();
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::AppendTail
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void SListBase<PTRCONVERTER>::AppendTail(SListItemBase<PTRCONVERTER> *currTail, SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv)
{
	Assert(!n->GetNext(conv));
	AppendTail(currTail, n, 1, conv);
}


////////////////////////////////////////////////////////////////////////////////
//  SListBase::CountNumItems
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline unsigned SListBase<PTRCONVERTER>::CountNumItems(const PTRCONVERTER &conv) const
{
	unsigned count = 0;
	const SListItemBase<PTRCONVERTER> *ptr = conv.ToPtr(head);
	while(ptr)
	{
		++count;
		Assert(count <= size);
		ptr = ptr->GetNext(conv);
	}
	Assert(count == size);
	return count;
}


////////////////////////////////////////////////////////////////////////////////
//  SList
////////////////////////////////////////////////////////////////////////////////
// Templated helper to give nicer interface.
// It would be nice to get PTRCONVERTER_ from USERTYPE::PTRCONVERTER, but that then unfortunately means that
// USERTYPE must be defined, it can't be just forward declared.
template<class USERTYPE, class PTRCONVERTER_=SListDefaultPtrConverter>
class SList : public SListBase<PTRCONVERTER_>
{
protected:

	typedef typename SListDerivedStorage<USERTYPE, __alignof(typename PTRCONVERTER_::STORAGE), typename PTRCONVERTER_::STORAGE>::T   STORAGE;

public:

	typedef PTRCONVERTER_ PTRCONVERTER;
	inline const USERTYPE *Peek(const PTRCONVERTER &conv=PTRCONVERTER()) const { return static_cast<const USERTYPE*>(SListBase<PTRCONVERTER>::Peek(conv)); }
	inline USERTYPE *Peek(const PTRCONVERTER &conv=PTRCONVERTER()) { return static_cast<USERTYPE*>(SListBase<PTRCONVERTER>::Peek(conv)); }
	inline STORAGE *GetPtrPtrHead() { return (STORAGE*)SListBase<PTRCONVERTER>::GetPtrPtrHead(); }
	inline USERTYPE *Pop(const PTRCONVERTER &conv=PTRCONVERTER()) { return static_cast<USERTYPE*>(SListBase<PTRCONVERTER>::Pop(conv)); }
	inline USERTYPE *ScanTail(const PTRCONVERTER &conv=PTRCONVERTER()) { return static_cast<USERTYPE*>(SListBase<PTRCONVERTER>::ScanTail(conv)); }
	inline void Push(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::Push(first, last, count, conv); }
	inline void Push(SList &other, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::Push(other, conv); }
	inline void Push(USERTYPE *n, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::Push(n, conv); }
	inline void AppendTail(USERTYPE *currTail, USERTYPE *appendFirst, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::AppendTail(currTail, appendFirst, count, conv); }
	inline void AppendTail(USERTYPE *currTail, SList &appendOther, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::AppendTail(currTail, appendOther, conv); }
	inline void AppendTail(USERTYPE *currTail, USERTYPE *n, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::AppendTail(currTail, n, conv); }
	inline void SetHead(USERTYPE *h, const PTRCONVERTER &conv=PTRCONVERTER()) { SListBase<PTRCONVERTER>::SetHead(h, conv); }
};


////////////////////////////////////////////////////////////////////////////////
//  SListAligned
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, unsigned ALIGN>
class SListAligned : public SList<USERTYPE, SListDefaultAlignPtrConverter<ALIGN> >
{
};


////////////////////////////////////////////////////////////////////////////////
//  SListIndexed
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class STORAGE>
class SListIndexed : public SList<USERTYPE, SListIdxPtrConverter<USERTYPE, STORAGE> >
{
};

} // namespace rage

#endif // VFX_SLIST_H
