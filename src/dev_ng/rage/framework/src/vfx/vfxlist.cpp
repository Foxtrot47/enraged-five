//
// vfx/vfxlist.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

// includes
#include "vfx/vfxlist.h"

// rage
#include "system/cache.h"

// framework
#include "vfx/channel.h"
#include "vfx/optimisations.h"


// optimisations
VFX_FW_GENERIC_OPTIMISATIONS()


// namespaces
using namespace rage;


// code
vfxListItem::vfxListItem()
{
	m_pNext = NULL;

#ifndef USE_BASIC_ONLY
	m_pPrev = NULL;
#endif 
}

vfxList::vfxList()
{
	m_numItems 	= 0;
	m_pHead 	= NULL;

#ifndef USE_BASIC_ONLY
	m_pTail		= NULL;
#endif 
}

int vfxList::AddItem(vfxListItem* pItem)
{	
#ifdef USE_BASIC_ONLY

	pItem->SetNext(m_pHead);
	m_pHead = pItem;

	// one more item in list
	return m_numItems++;

#else // USE_BASIC_ONLY

	// store where the head currently points to
	vfxListItem*	headItem = m_pHead;
	
	// set up the ptrs for the new item
	m_pHead = pItem;
	pItem->SetPrev(NULL);
	pItem->SetNext(headItem);
	
	// set up the ptrs for the old head
	if (headItem)
	{
		headItem->SetPrev(pItem);
	}
	else
	{
		m_pTail = pItem;
	}
	
	// one more item in list
	return m_numItems++;

#endif // USE_BASIC_ONLY
}

int vfxList::AddItemAfter(vfxListItem* pItem, vfxListItem* pItemToAddAfter)
{	
#ifdef USE_BASIC_ONLY

	pItem->SetNext(pItemToAddAfter->GetNext());
	pItemToAddAfter->SetNext(pItem);

	// one more item in list
	return m_numItems++;

#else // USE_BASIC_ONLY

	// insert new item after this one
	vfxListItem* pNextItem = pItemToAddAfter->GetNext();

	// set up ptrs for this item
	pItemToAddAfter->SetNext(pItem);

	// set up ptrs for the new item
	pItem->SetPrev(pItemToAddAfter);
	pItem->SetNext(pNextItem);

	// set up ptrs for the next item
	if (pNextItem)
	{
		pNextItem->SetPrev(pItem);
	}
	else
	{
		m_pTail = pItem;
	}

	// one more item in list
	return m_numItems++;

#endif // USE_BASIC_ONLY
}

int vfxList::RemoveItem(vfxListItem* pItem)
{
#ifdef USE_BASIC_ONLY

	// find the item in the list
	vfxListItem*	pCurrItem = m_pHead;
	vfxListItem*	pPrevItem = NULL;
	
	while (pCurrItem)
	{
		if (pItem == pCurrItem)
		{
			break;
		}	

		pPrevItem = pCurrItem;
		pCurrItem = pCurrItem->GetNext();
	}
	
	// remove if found
	if (pCurrItem)
	{
		if (pPrevItem == NULL)
		{
			m_pHead = pCurrItem->GetNext();
		}
		else
		{
			pPrevItem->SetNext(pCurrItem->GetNext());	
		}

		// one less item in list
		return m_numItems--;
	}
	else
	{
		return m_numItems;
	}

#else // USE_BASIC_ONLY

	// store this items next and prev items
	vfxListItem* nextItem = pItem->GetNext();
	vfxListItem* prevItem = pItem->GetPrev();
	
	// update ptrs 
	if (nextItem)
	{
		nextItem->SetPrev(prevItem);
	}
	else
	{
		m_pTail = pItem->GetPrev();
	}

	if (prevItem)
	{
		prevItem->SetNext(nextItem);
	}
	else
	{
		m_pHead = pItem->GetNext();
	}

	// one less item in list
	return m_numItems--;

#endif // USE_BASIC_ONLY
}

#ifdef USE_BASIC_ONLY

// While using a templated function is not the most obvious implementation, but
// just wrapping RemoveHead() then doing a prefetch on m_pHead is just replacing
// a cache miss with a load-hit-store.
template<bool PREFETCH_NEXT>
vfxListItem* vfxList::TemplatedRemoveHead()
{
	vfxListItem* pReturnItem = NULL;
	
	if (m_pHead)
	{
		pReturnItem = m_pHead;

		vfxListItem *next = m_pHead->GetNext();
		if (PREFETCH_NEXT && next)
		{
			PrefetchDC(next);
		}
		m_pHead = next;
		
		m_numItems--;
	}

	return pReturnItem;
}

vfxListItem* vfxList::RemoveHead()
{
	return TemplatedRemoveHead<false>();
}

vfxListItem* vfxList::RemoveHeadPrefetchNext()
{
	return TemplatedRemoveHead<true>();
}

#else // !USE_BASIC_ONLY

vfxListItem* vfxList::RemoveHead()
{
	vfxListItem* pReturnItem = NULL;
	
	if (m_pHead)
	{
		pReturnItem = m_pHead;
		
		if (m_pHead == m_pTail)
		{
			// it's the only item in the list
			m_pHead = m_pTail = NULL;
		}
		else
		{
			// pt the head to the second item
			m_pHead = m_pHead->GetNext();	
		
			if (m_pHead)
			{
				m_pHead->SetPrev(NULL);
			}
		}
		
		m_numItems--;
		return pReturnItem;
	}
	else
	{
		return pReturnItem;
	}
}

#endif // !USE_BASIC_ONLY

int vfxList::RemoveAll()
{
	m_numItems = 0;
	m_pHead = NULL;

#ifndef USE_BASIC_ONLY
	m_pTail = NULL;
#endif // USE_BASIC_ONLY

	return 0;
}

#ifndef USE_BASIC_ONLY
int vfxList::AppendItem(vfxListItem* pItem)
{
	// store where the tail currently points to
	vfxListItem* tailItem = m_pTail;
	
	// set up ptrs for the new item
	m_pTail = pItem;
	pItem->SetPrev(tailItem);
	pItem->SetNext(NULL);
	
	// set up ptrs for the old tail
	if (tailItem)
	{
		tailItem->SetNext(pItem);
	}
	else
	{
		m_pHead = pItem;
	}

	// one more item in list
	return m_numItems++;
}

int vfxList::AddItemBefore(vfxListItem* pItem, vfxListItem* pItemToAddBefore)
{
	vfxListItem* pPrevItem = pItemToAddBefore->GetPrev();
	
	// set up ptrs for this item
	pItemToAddBefore->SetPrev(pItem);
	
	// set up ptrs for the new item
	pItem->SetPrev(prevItem);
	pItem->SetNext(pItemToAddBefore);
	
	// set up ptrs for the prev item
	if (pPrevItem)
	{
		pPrevItem->SetNext(pItem);
	}
	else
	{
		m_pHead = pItem;
	}
	
	// one more item in list
	return m_numItems++;
}

vfxListItem* vfxList::RemoveTail()
{
	vfxListItem* pReturnItem = NULL;
	
	if (m_pTail)
	{
		pReturnItem = m_pTail;
	
		// pt the tail to the second last item
		m_pTail = m_pTail->GetPrev();
		m_pTail->SetNext(NULL);	
		
		m_numItems--;

		return pReturnItem;
	}
	else
	{
		return pReturnItem;
	}
}

/*
vfxListItem* vfxList::GetItemOffset(bool head, int offset)
{
	if (head)
	{
		// were offset from the head
		vfxListItem* pCurrItem = m_pHead;
	
		for (int i=0; i<offset; i++)
		{
			if (pCurrItem == NULL)
			{
				return NULL;
			}
		
			pCurrItem = pCurrItem->GetNext();
		}
		
		return pCurrItem;
	}
	else
	{
		// were offset from the tail
		vfxListItem* pCurrItem = m_pTail;
	
		for (int i=0; i<offset; i++)
		{
			if (pCurrItem == NULL)
			{
				return NULL;
			}
		
			pCurrItem = pCurrItem->GetPrev();
		}
		
		return pCurrItem;
	}
}
*/
#endif // USE_BASIC_ONLY

#if __ASSERT
void vfxList::CheckListIntegrity(char* txt)
{
	// get the head of the list
	vfxListItem* pCurrItem = GetHead();

	// check how many items there should be
	int numItems = GetNumItems();

	// go through the items in the list checking that they are valid
	if (numItems>0)
	{
		for (int i=0; i<numItems; i++)
		{
			decalAssertf(pCurrItem, "%s list error 1", txt);
			pCurrItem = GetNext(pCurrItem);
		}
	}

	// we should now be at the end of the list
	decalAssertf(pCurrItem==NULL, "%s list error 2", txt);
}
#endif





