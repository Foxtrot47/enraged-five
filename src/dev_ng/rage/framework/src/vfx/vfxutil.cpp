//
// vfx/vfxutil.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "vfx/vfxutil.h"

// framework
#include "file/asset.h"
#include "grcore/debugdraw.h"
#include "fwscene/stores/txdstore.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"


// optimisations
VFX_FW_GENERIC_OPTIMISATIONS()


// defines
#define USE_LOADOBJECT 1															// this controls if you should use the streaming system or hit loadfile on the texture store directly


// namespaces
using namespace rage;


// static variables
#if OUTPUT_FX_MEM_BUCKET_USAGE
s32 vfxUtil::ms_currFxMemory[4];
#endif

int vfxUtil::ms_numBrokenFragInfos = 0;
vfxBrokenFragInfo vfxUtil::ms_brokenFragInfos[VFX_MAX_BROKEN_FRAG_INFOS];


// code
#if OUTPUT_FX_MEM_BUCKET_USAGE
void vfxUtil::OutputFxBucketMemoryCurrent(const char* idString)
{
	// get the memory used since the last call
	bool anyMemDiff = false;		
	int numBytesTotal[4];	
	int numBytesThis[4];
	for (s32 i=0; i<4; i++)
	{	
		numBytesTotal[i] = (int)sysMemAllocator::GetMaster().GetAllocator(i)->GetMemoryUsed(MEMBUCKET_FX);
		numBytesThis[i] = numBytesTotal[i] - ms_currFxMemory[i];
		ms_currFxMemory[i] = numBytesTotal[i];
		if (numBytesThis[i]!=0.0f)
		{
			anyMemDiff = true;
		}
	}

	// add the memory difference to the string
	if (anyMemDiff)
	{
		char txt[256];
		sprintf(txt, "FX Memory In    %32s - ", idString);

		for (s32 i=0; i<4; i++)
		{	
			char memTxt[32];
			sprintf(memTxt, "%8.2fK ", numBytesThis[i]/1024.0f);
			strcat(txt, memTxt);
		}

		vfxDebugf2("%s", txt);
	}
}
#endif

#if OUTPUT_FX_MEM_BUCKET_USAGE
void vfxUtil::OutputFxBucketMemoryTotal(const char* idString)
{
	char txt[256];
	sprintf(txt, "FX Memory Total %32s - ", idString);

	// add the memory total to the string
	for (s32 i=0; i<4; i++)
	{	
		ms_currFxMemory[i] = (s32)sysMemAllocator::GetMaster().GetAllocator(i)->GetMemoryUsed(MEMBUCKET_FX);

		char memTxt[32];
		sprintf(memTxt, "%8.2fK ", ms_currFxMemory[i]/1024.0f);
		strcat(txt, memTxt);
	}

	vfxDebugf2("%s", txt);
}
#endif

#if USE_LOADOBJECT
// if there's a colon (e.g. "platform:/models/textures/bob") use the raw path since it passes the criteria of being 'absolute'
template <typename T, typename U, size_t _Size> void tMakeFilename(T (&dest)[_Size], const U& src)
{
	if(strchr(src,':')) // 
		safecpy(dest,src);
	else
		formatf(dest,"platform:/textures/%s",src);
}
#endif

int vfxUtil::FindTxd(const char* pTxdName)
{
#if USE_LOADOBJECT
	char szTdxFilename[RAGE_MAX_PATH];
	tMakeFilename(szTdxFilename, pTxdName);
	return g_TxdStore.FindSlot(szTdxFilename).Get();
#else
	return g_TxdStore.FindSlot(pTxdName).Get();
#endif
}

int vfxUtil::InitTxd(const char* pTxdName)
{
#if USE_LOADOBJECT
	//Displayf("[vfxUtil::InitTxd] %s",pTxdName);
	// the txd *should* already be registered with the streaming system
	char szTdxFilename[RAGE_MAX_PATH];
	tMakeFilename(szTdxFilename, pTxdName);
	strLocalIndex txdSlot = g_TxdStore.FindSlot(szTdxFilename);
	vfxAssertf(txdSlot!=-1, "Texture dictionary %s not present or not registered with CStreaming",szTdxFilename);
	if(txdSlot==-1)
	{
		return txdSlot.Get();
	}
	else
	{
		if (g_TxdStore.Get(txdSlot) == NULL)
		{
			strIndex index = g_TxdStore.GetStreamingIndex(txdSlot);
			if(strStreamingEngine::GetInfo().RequestObject(index, STRFLAG_DONTDELETE|STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD))
			{
				if (strStreamingEngine::GetInfo().GetStreamingInfoRef(index).GetStatus() != STRINFO_LOADED)
				{
					strStreamingEngine::GetLoader().LoadAllRequestedObjects(true);
				}

				strStreamingEngine::GetInfo().ClearRequiredFlag(index, STRFLAG_DONTDELETE);
			}
		}
	}
#else
	// set up a reference to the texture dictionary
	strLocalIndex txdSlot = g_TxdStore.FindSlot(pTxdName);
	if (txdSlot == -1)
	{
		txdSlot = g_TxdStore.AddSlot(pTxdName);
	}
	vfxAssertf(txdSlot!=-1, "Couldn't add texture dictionary slot");

	// load if not already in memory
	if (g_TxdStore.Get(txdSlot) == NULL)
	{
		ASSET.PushFolder("platform:/textures");
		g_TxdStore.LoadFile(txdSlot, pTxdName);
		ASSET.PopFolder();
	}
#endif

	g_TxdStore.AddRef(txdSlot, REF_OTHER);

	return txdSlot.Get();
}

void vfxUtil::ShutdownTxd(const char* pTxdName)
{
	// remove the reference to the texture dictionary
#if USE_LOADOBJECT
	// Displayf("[vfxUtil::ShutdownTxd] %s",pTxdName);
	char szTdxFilename[RAGE_MAX_PATH];
	tMakeFilename(szTdxFilename, pTxdName);
	strLocalIndex vfxBloodSlot = strLocalIndex(g_TxdStore.FindSlot(szTdxFilename));
#else
	strLocalIndex vfxBloodSlot = strLocalIndex(g_TxdStore.FindSlot(pTxdName));
#endif
	vfxAssertf(vfxBloodSlot != -1, "Couldn't find texture dictionary slot");
	g_TxdStore.RemoveRef(vfxBloodSlot, REF_OTHER);
}

void vfxUtil::ReportBrokenFrag(fwEntity* pParentEntity, fwEntity* pChildEntity)
{
	vfxAssertf(pParentEntity, "Called with a NULL parent entity");
	vfxAssertf(pChildEntity, "Called with a NULL child entity");

	// store the fragment break info so we can update any fx systems with the new info later
	if (ms_numBrokenFragInfos<VFX_MAX_BROKEN_FRAG_INFOS)
	{
		ms_brokenFragInfos[ms_numBrokenFragInfos].pParentEntity = pParentEntity;
		ms_brokenFragInfos[ms_numBrokenFragInfos].pChildEntity = pChildEntity;
		ms_numBrokenFragInfos++;
	}
	else
	{
		vfxDisplayf("No room left in broken frag array");
	}
}

