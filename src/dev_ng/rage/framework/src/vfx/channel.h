//
// vfx/channel.h
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef VFX_CHANNEL_H 
#define VFX_CHANNEL_H 

#include "diag/channel.h"

#define DECAL_ALL_ASSERTS_FATAL     0

namespace rage{

RAGE_DECLARE_CHANNEL(vfx)

#define vfxAssert(cond)							RAGE_ASSERT(vfx,cond)
#define vfxAssertf(cond,fmt,...)				RAGE_ASSERTF(vfx,cond,fmt,##__VA_ARGS__)
#define vfxFatalAssertf(cond,fmt,...)			RAGE_FATALASSERTF(vfx,cond,fmt,##__VA_ARGS__)
#define vfxVerifyf(cond,fmt,...)				RAGE_VERIFYF(vfx,cond,fmt,##__VA_ARGS__)
#define vfxErrorf(fmt,...)						RAGE_ERRORF(vfx,fmt,##__VA_ARGS__)
#define vfxWarningf(fmt,...)					RAGE_WARNINGF(vfx,fmt,##__VA_ARGS__)
#define vfxDisplayf(fmt,...)					RAGE_DISPLAYF(vfx,fmt,##__VA_ARGS__)
#define vfxDebugf1(fmt,...)						RAGE_DEBUGF1(vfx,fmt,##__VA_ARGS__)
#define vfxDebugf2(fmt,...)						RAGE_DEBUGF2(vfx,fmt,##__VA_ARGS__)
#define vfxDebugf3(fmt,...)						RAGE_DEBUGF3(vfx,fmt,##__VA_ARGS__)
#define vfxLogf(severity,fmt,...)				RAGE_LOGF(vfx,severity,fmt,##__VA_ARGS__)

RAGE_DECLARE_SUBCHANNEL(vfx, ptfx)

#define ptfxAssert(cond)						RAGE_ASSERT(vfx_ptfx,cond)
#define ptfxAssertf(cond,fmt,...)				RAGE_ASSERTF(vfx_ptfx,cond,fmt,##__VA_ARGS__)
#define ptfxFatalAssertf(cond,fmt,...)			RAGE_FATALASSERTF(vfx_ptfx,cond,fmt,##__VA_ARGS__)
#define ptfxVerifyf(cond,fmt,...)				RAGE_VERIFYF(vfx_ptfx,cond,fmt,##__VA_ARGS__)
#define ptfxErrorf(fmt,...)						RAGE_ERRORF(vfx_ptfx,fmt,##__VA_ARGS__)
#define ptfxWarningf(fmt,...)					RAGE_WARNINGF(vfx_ptfx,fmt,##__VA_ARGS__)
#define ptfxDisplayf(fmt,...)					RAGE_DISPLAYF(vfx_ptfx,fmt,##__VA_ARGS__)
#define ptfxDebugf1(fmt,...)					RAGE_DEBUGF1(vfx_ptfx,fmt,##__VA_ARGS__)
#define ptfxDebugf2(fmt,...)					RAGE_DEBUGF2(vfx_ptfx,fmt,##__VA_ARGS__)
#define ptfxDebugf3(fmt,...)					RAGE_DEBUGF3(vfx_ptfx,fmt,##__VA_ARGS__)
#define ptfxLogf(severity,fmt,...)				RAGE_LOGF(vfx_ptfx,severity,fmt,##__VA_ARGS__)
#define ptfxCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,vfx_ptfx,severity,fmt,##__VA_ARGS__)

RAGE_DECLARE_SUBCHANNEL(vfx, decal)

#if !DECAL_ALL_ASSERTS_FATAL
#	define decalAssert(cond)                       RAGE_ASSERT(vfx_decal,cond)
#	define decalAssertf(cond,fmt,...)              RAGE_ASSERTF(vfx_decal,cond,fmt,##__VA_ARGS__)
#	define decalFatalAssert(cond)                  RAGE_FATALASSERTF(vfx_decal,cond,"%s",#cond)
#	define decalFatalAssertf(cond,fmt,...)         RAGE_FATALASSERTF(vfx_decal,cond,fmt,##__VA_ARGS__)
#	define decalVerifyf(cond,fmt,...)              RAGE_VERIFYF(vfx_decal,cond,fmt,##__VA_ARGS__)
#else
#	define decalAssert(cond)                       FatalAssert(cond)
#	define decalAssertf(cond,fmt,...)              FatalAssertf(cond,fmt,##__VA_ARGS__)
#	define decalFatalAssert(cond)                  FatalAssert(cond)
#	define decalFatalAssertf(cond,fmt,...)         FatalAssertf(cond,fmt,##__VA_ARGS__)
#	define decalVerifyf(cond,fmt,...)              (RAGE_VERIFYF(vfx_decal,cond,fmt,##__VA_ARGS__) || (__debugbreak(),0))
#endif
#define decalErrorf(fmt,...)                    RAGE_ERRORF(vfx_decal,fmt,##__VA_ARGS__)
#define decalWarningf(fmt,...)                  RAGE_WARNINGF(vfx_decal,fmt,##__VA_ARGS__)
#define decalDisplayf(fmt,...)                  RAGE_DISPLAYF(vfx_decal,fmt,##__VA_ARGS__)
#define decalDebugf1(fmt,...)                   RAGE_DEBUGF1(vfx_decal,fmt,##__VA_ARGS__)
#define decalDebugf2(fmt,...)                   RAGE_DEBUGF2(vfx_decal,fmt,##__VA_ARGS__)
#define decalDebugf3(fmt,...)                   RAGE_DEBUGF3(vfx_decal,fmt,##__VA_ARGS__)
#define decalLogf(severity,fmt,...)             RAGE_LOGF(vfx_decal,severity,fmt,##__VA_ARGS__)
#define decalCondLogf(cond,severity,fmt,...)    RAGE_CONDLOGF(cond,vfx_decal,severity,fmt,##__VA_ARGS__)

#if __BANK
#define decalSpamf(fmt,...)                     do{if(DECALMGRASYNC.GetDebug()->IsEnableSpam())decalWarningf(fmt,##__VA_ARGS__);}while(0)
#else
#define decalSpamf(fmt,...)                     (void)0
#endif



#if __BANK
class vfxChannel
{
public:
	static void Init();
};
#endif


} // namespace rage

#endif // VFX_CHANNEL_H
