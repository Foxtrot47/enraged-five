#ifndef VFX_SLISTITOR_H
#define VFX_SLISTITOR_H

#include "slist.h"

#if !__SPU
#	include "system/cache.h"
#else
#	include "system/dma.h"
#	include "system/memops.h"
#endif


// Microsoft's compilers complain about using typename when specifying a ctor
// for a default arg
#ifdef _MSC_VER
#	define  TYPENAME_DEFAULT_ARG
#else
#	define  TYPENAME_DEFAULT_ARG    typename
#endif

namespace rage {

template<class USERTYPE, unsigned ALIGNMENT> class SListItor;

template<class USERTYPE, unsigned ALIGNMENT=16>
class SListConstItor
{
public:

	inline SListConstItor(const USERTYPE *firstEa, unsigned tag, const typename USERTYPE::PTRCONVERTER &conv=TYPENAME_DEFAULT_ARG USERTYPE::PTRCONVERTER());
	inline const USERTYPE *Ptr();
	inline const USERTYPE *Ea() const;

	// To be called if the list elements being iterated through may have changed (eg, a SListItor was constructed from this SListConstItor)
	inline void Refetch();

#if __SPU
	inline SListConstItor(const SListConstItor &other);
	inline ~SListConstItor();
#endif

private:

	// Copy construction allowed, but not assignment.  (Assignment less
	// efficient on SPU since we first need to construct an SListConstItor,
	// which will do a dma get).
	SListConstItor &operator=(const SListConstItor &other);

	friend class SListItor<USERTYPE,ALIGNMENT>;

#if !__SPU
	const USERTYPE     *curr;
	const USERTYPE     *next;
#else
	// Due to a GCC limitation, __attribute__((__aligned__(__alignof__(T))))
	// fails to compile.  As __alignof__(T) is not considerred a constant inside
	// an __attribute__ tag inside a template.  There is a bit of an
	// explaination of the problem here,
	//      http://gcc.gnu.org/bugzilla/show_bug.cgi?id=10479
	//
	// So for now the "best" (but it still sucks) workaround seems to be to pass
	// in the alignment as a template arg, AND require the caller to align the
	// SListConstItor appropriately.  Even an unsigned template argument cannot
	// be used in __attribute__((__aligned__())).
	//
	// The helper macro SLIST_CONST_ITOR makes this slightly more conveinient.
	//
	CompileTimeAssert(__alignof__(USERTYPE) <= ALIGNMENT);
	char                buf[2][((sizeof(USERTYPE)+ALIGNMENT-1)&-ALIGNMENT) + (__alignof__(USERTYPE)<ALIGNMENT?ALIGNMENT:0)] ;
	const USERTYPE     *ptr[2];
	u32                 ea[2];
	unsigned            idx;
	unsigned            dmaTag;
	bool                done;

	inline void Get(unsigned idx, u32 ea_);
#endif

	const typename USERTYPE::PTRCONVERTER &conv;
};


#if !__SPU

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListConstItor<USERTYPE,ALIGNMENT>::SListConstItor(const USERTYPE *firstEa, unsigned /*tag*/, const typename USERTYPE::PTRCONVERTER &conv_)
		: curr(firstEa)
		, next(firstEa)
		, conv(conv_)
	{
		if(firstEa) PrefetchDC(firstEa);
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline const USERTYPE *SListConstItor<USERTYPE,ALIGNMENT>::Ptr()
	{
		const USERTYPE *ret = curr = next;
		if(ret)
		{
			next = ret->GetNext(conv);
			if(next) PrefetchDC(next);
		}
		return ret;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline const USERTYPE *SListConstItor<USERTYPE,ALIGNMENT>::Ea() const
	{
		return static_cast<const USERTYPE*>(curr);
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline void SListConstItor<USERTYPE,ALIGNMENT>::Refetch()
	{
	}

#else

	template<class USERTYPE, unsigned ALIGNMENT>
	inline void SListConstItor<USERTYPE,ALIGNMENT>::Get(unsigned idx_, u32 ea_)
	{
		const u32 offs = ea_&15;
		sysDmaGet(buf[idx_], ea_&~15, (sizeof(USERTYPE)+offs+15)&~15, dmaTag);
		ptr[idx_] = (USERTYPE*)(buf[idx_] + offs);
		ea[idx_] = ea_;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListConstItor<USERTYPE,ALIGNMENT>::SListConstItor(const USERTYPE *firstEa, unsigned tag, const typename USERTYPE::PTRCONVERTER &conv_)
		: idx(0)
		, dmaTag(tag)
		, done(!firstEa)
		, conv(conv_)
	{
		CompileTimeAssert(OffsetOf(SListConstItor,buf[0]) == 0);
		Assert(((uptr)this&(ALIGNMENT-1)) == 0);
		if(Likely(firstEa))
		{
			Get(0, (u32)firstEa);
		}
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline const USERTYPE *SListConstItor<USERTYPE,ALIGNMENT>::Ptr()
	{
		if(Unlikely(done))
		{
			ea[idx^1] = 0;
			return NULL;
		}
		const USERTYPE *const ret = ptr[idx];
		sysDmaWaitTagStatusAll(1<<dmaTag);
		idx ^= 1;
		const USERTYPE *const next = ret->GetNext(conv);
		if(Likely(next))
		{
			Get(idx, (u32)next);
		}
		else
		{
			done = true;
		}
		return ret;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline const USERTYPE *SListConstItor<USERTYPE,ALIGNMENT>::Ea() const
	{
		return (USERTYPE*)(ea[idx^1]);
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline void SListConstItor<USERTYPE,ALIGNMENT>::Refetch()
	{
		if(Unlikely(done))
		{
			return;
		}
		// Dma gets here need to be fenced, as it is possible that a previous
		// get triggered by Ptr() has not yet completed.
		const unsigned idx0 = idx;
		const unsigned idx1 = idx0^1;
		const u32 ea0 = ea[idx0];
		const u32 ea1 = ea[idx1];
		sysDmaGetf(buf[idx0], ea0&~15, (sizeof(USERTYPE)+(ea0&15)+15)&~15, dmaTag);
		sysDmaGetf(buf[idx1], ea1&~15, (sizeof(USERTYPE)+(ea1&15)+15)&~15, dmaTag);
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListConstItor<USERTYPE, ALIGNMENT>::SListConstItor(const SListConstItor &other)
	{
		sysDmaWaitTagStatusAll(1<<other.dmaTag);
		sysMemCpy(this, &other, sizeof(*this));
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListConstItor<USERTYPE, ALIGNMENT>::~SListConstItor()
	{
		sysDmaWaitTagStatusAll(1<<dmaTag);
	}

#endif


template<class USERTYPE, unsigned ALIGNMENT=16>
class SListItor
{
public:

	inline SListItor(USERTYPE *first, unsigned tag, const typename USERTYPE::PTRCONVERTER &conv=TYPENAME_DEFAULT_ARG USERTYPE::PTRCONVERTER());
	inline SListItor(const SListConstItor<USERTYPE,ALIGNMENT> &constItor, unsigned tag);

	inline USERTYPE *Ptr();

	// WARNING: This is invalid to call after Unchanged()
	inline USERTYPE *Ea() const;

	// Prevent the auto dma put of the currently referenced element when updating the iterator (ie, when Ptr() next called).
	inline void Unchanged();

#if __SPU
	inline ~SListItor();
#endif

private:

	// Neither copy construction, nor assignment currently supported
	SListItor &operator=(const SListItor &other);
	SListItor(const SListItor &other);

#if !__SPU
	USERTYPE           *curr;
	USERTYPE           *next;
	ASSERT_ONLY(bool    unchanged;)
#else
	CompileTimeAssert(__alignof__(USERTYPE) <= ALIGNMENT);
	char                buf[3][((sizeof(USERTYPE)+ALIGNMENT-1)&-ALIGNMENT) + (__alignof__(USERTYPE)<ALIGNMENT?ALIGNMENT:0)] ;
	USERTYPE           *ptr[3];
	u32                 putEa[3];
	unsigned            idx;
	unsigned            dmaTag;
	bool                done;

	inline void Get(unsigned idx, u32 ea);
#endif

	const typename USERTYPE::PTRCONVERTER &conv;
};


#if !__SPU

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListItor<USERTYPE,ALIGNMENT>::SListItor(USERTYPE *first, unsigned /*tag*/, const typename USERTYPE::PTRCONVERTER &conv_)
		: curr(first)
		, next(first)
		ASSERT_ONLY(, unchanged(false))
		, conv(conv_)
	{
		if(first) PrefetchDC(first);
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListItor<USERTYPE,ALIGNMENT>::SListItor(const SListConstItor<USERTYPE,ALIGNMENT> &constItor, unsigned /*tag*/)
		: curr(const_cast<USERTYPE*>(constItor.curr))
		, next(const_cast<USERTYPE*>(constItor.next))
		ASSERT_ONLY(, unchanged(false))
		, conv(constItor.conv)
	{
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline USERTYPE *SListItor<USERTYPE,ALIGNMENT>::Ptr()
	{
		USERTYPE *const ret = next;
		if(ret)
		{
			next = ret->GetNext(conv);
			if(next) PrefetchDC(next);
		}
		ASSERT_ONLY(unchanged = false;)
		return ret;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline USERTYPE *SListItor<USERTYPE,ALIGNMENT>::Ea() const
	{
		Assert(!unchanged);
		return curr;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline void SListItor<USERTYPE,ALIGNMENT>::Unchanged()
	{
		ASSERT_ONLY(unchanged = true;)
	}

#else

	template<class USERTYPE, unsigned ALIGNMENT>
	inline void SListItor<USERTYPE,ALIGNMENT>::Get(unsigned idx_, u32 ea)
	{
		// This get is fenced to ensure any previous put from the same buffer
		// has completed
		const u32 offs = ea&15;
		sysDmaGetf(buf[idx_], ea&~15, (sizeof(USERTYPE)+offs+15)&~15, dmaTag+idx_);
		ptr[idx_] = (USERTYPE*)(buf[idx_] + offs);
		putEa[idx_] = ea;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListItor<USERTYPE,ALIGNMENT>::SListItor(USERTYPE *first, unsigned tag, const typename USERTYPE::PTRCONVERTER &conv_)
		: idx(0)
		, dmaTag(tag)
		, done(!first)
		, conv(conv_)
	{
		CompileTimeAssert(OffsetOf(SListItor,buf[0]) == 0);
		Assert(((uptr)this&(ALIGNMENT-1)) == 0);
		sysMemSet(putEa, 0, sizeof(putEa));
		if(Likely(first))
		{
			Get(0, (u32)first);
		}
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListItor<USERTYPE,ALIGNMENT>::SListItor(const SListConstItor<USERTYPE,ALIGNMENT> &constItor, unsigned tag)
		: idx(0)
		, dmaTag(tag)
		, done(constItor.done)
		, conv(constItor.conv)
	{
		const unsigned idx0 = constItor.idx;
		const unsigned idx1 = idx0^1;
		ptr[0] = (USERTYPE*)((u32)buf[0]+(u32)constItor.ptr[idx0]-(u32)constItor.buf[idx0]);
		ptr[1] = (USERTYPE*)((u32)buf[1]+(u32)constItor.ptr[idx1]-(u32)constItor.buf[idx1]);
		//ptr[2] = 0;
		putEa[0] = constItor.ea[idx0];
		putEa[1] = constItor.ea[idx1];
		putEa[2] = 0;
		sysDmaWaitTagStatusAll(1<<constItor.dmaTag);
		// Note that the special case of calling this ctor, before
		// constItor.Ptr() has been called, means that _both_ sysMemCpy's must be
		// after the dma wait.
		sysMemCpy(buf[0], constItor.buf[idx0], sizeof(buf[0]));
		sysMemCpy(buf[1], constItor.buf[idx1], sizeof(buf[1]));
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline USERTYPE *SListItor<USERTYPE,ALIGNMENT>::Ptr()
	{
		const unsigned prevIdx = idx ? idx-1 : 2;
		if(Likely(putEa[prevIdx]))
		{
			DmaPutAlignedButOddSize(ptr[prevIdx], putEa[prevIdx], dmaTag+prevIdx);
		}
		if(Unlikely(done))
		{
			putEa[prevIdx] = 0;
			return NULL;
		}
		USERTYPE *const ret = ptr[idx];
		const unsigned currIdx = idx;
		idx = (idx + 1) & -(idx < 2);
		sysDmaWaitTagStatusAll(1<<(dmaTag + currIdx));
		USERTYPE *const next = ret->GetNext(conv);
		if(Likely(next))
		{
			Get(idx, (u32)next);
		}
		else
		{
			done = true;
		}
		return ret;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline USERTYPE *SListItor<USERTYPE,ALIGNMENT>::Ea() const
	{
		const unsigned prevIdx = idx ? idx-1 : 2;
		return (USERTYPE*)(putEa[prevIdx]);
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline void SListItor<USERTYPE,ALIGNMENT>::Unchanged()
	{
		const unsigned prevIdx = idx ? idx-1 : 2;
		putEa[prevIdx] = 0;
	}

	template<class USERTYPE, unsigned ALIGNMENT>
	inline SListItor<USERTYPE,ALIGNMENT>::~SListItor()
	{
		sysDmaWaitTagStatusAll(7<<dmaTag);
	}

#endif


#define SLIST_CONST_ITOR_(USERTYPE, ALIGNMENT, NAME, CTOR_ARGS_IN_PARENTHESIS) \
	SListConstItor<USERTYPE, ALIGNMENT> NAME SPU_ONLY(ALIGNED(ALIGNMENT)) CTOR_ARGS_IN_PARENTHESIS

#define SLIST_CONST_ITOR(USERTYPE, NAME, CTOR_ARGS_IN_PARENTHESIS) \
	SLIST_CONST_ITOR_(USERTYPE, (__alignof(USERTYPE)<16?16:__alignof(USERTYPE)), NAME, CTOR_ARGS_IN_PARENTHESIS)

#define SLIST_ITOR_(USERTYPE, ALIGNMENT, NAME, CTOR_ARGS_IN_PARENTHESIS) \
	SListItor<USERTYPE, ALIGNMENT> NAME SPU_ONLY(ALIGNED(ALIGNMENT)) CTOR_ARGS_IN_PARENTHESIS

#define SLIST_ITOR(USERTYPE, NAME, CTOR_ARGS_IN_PARENTHESIS) \
	SLIST_ITOR_(USERTYPE, (__alignof(USERTYPE)<16?16:__alignof(USERTYPE)), NAME, CTOR_ARGS_IN_PARENTHESIS)


} // namespace rage

#undef TYPENAME_DEFAULT_ARG

#endif // VFX_SLISTITOR_H
