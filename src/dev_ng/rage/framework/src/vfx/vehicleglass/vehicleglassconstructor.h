// ==========================================
// vfx/vehicleglass/vehicleglassconstructor.h
// (c) 2012 RockstarNorth
// ==========================================

#ifndef VEHICLEGLASSCONSTRUCTOR_H
#define VEHICLEGLASSCONSTRUCTOR_H

#define VEHICLE_GLASS_CONSTRUCTOR ((1 && __DEV && GTA_VERSION) || (1 && __RESOURCECOMPILER))
#define VEHICLE_GLASS_DEV (1 && VEHICLE_GLASS_CONSTRUCTOR && __DEV && (1 || !__RESOURCECOMPILER))

#if VEHICLE_GLASS_CONSTRUCTOR

#include "vectormath/vec4v.h"

namespace rage {

template <typename,int,class> class atArray;

class fragType;
class bkBank;
class fwVehicleGlassWindowProxy;
class fwVehicleGlassWindowData;

class fwVehicleGlassConstructorTriangleBase
{
public:
	fwVehicleGlassConstructorTriangleBase() {}
	fwVehicleGlassConstructorTriangleBase(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2, Vec4V_In t0, Vec4V_In t1, Vec4V_In t2, Vec4V_In c0, Vec4V_In c1, Vec4V_In c2)
	{
		m_p[0]       = p0;
		m_p[1]       = p1;
		m_p[2]       = p2;
		m_t[0]       = t0;
		m_t[1]       = t1;
		m_t[2]       = t2;
		m_diffuse[0] = c0.Get<Vec::X,Vec::W>();
		m_diffuse[1] = c1.Get<Vec::X,Vec::W>();
		m_diffuse[2] = c2.Get<Vec::X,Vec::W>();
#if __RESOURCECOMPILER
		m_exposed[0] = (c0.GetYf() == 0.0f);
		m_exposed[1] = (c1.GetYf() == 0.0f);
		m_exposed[2] = (c2.GetYf() == 0.0f);
#else
		m_exposed[0] = (c0.GetZf() == 0.0f);
		m_exposed[1] = (c1.GetZf() == 0.0f);
		m_exposed[2] = (c2.GetZf() == 0.0f);
#endif
	}

	Vec3V m_p[3]; // positions
	Vec4V m_t[3]; // texcoords
	Vec2V m_diffuse[3];
	bool  m_exposed[3];
};

class fwVehicleGlassConstructorTriangle : public fwVehicleGlassConstructorTriangleBase
{
public:
	fwVehicleGlassConstructorTriangle() {}
	fwVehicleGlassConstructorTriangle(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2, Vec4V_In t0, Vec4V_In t1, Vec4V_In t2, Vec4V_In c0, Vec4V_In c1, Vec4V_In c2, int shaderIndex, int geomIndex, int origBoneIndex)
		: fwVehicleGlassConstructorTriangleBase(p0, p1, p2, t0, t1, t2, c0, c1, c2)
	{
		m_shaderIndex		= shaderIndex;
		m_geomIndex			= geomIndex;
		m_origBoneIndex		= origBoneIndex;
		m_mappedBoneIndex	= -1;
	}

	int m_shaderIndex;
	int m_geomIndex;
	int m_origBoneIndex;
	int m_mappedBoneIndex;
};

class fwVehicleGlassConstructorInterface
{
public:
	static void SetDebugOutputDirectory(const char* dir);
	static void SetCarGlassMaterialFlag(int materialIndex, const char* name);
	static int  GetCarGlassMaterialFlagCount();
#if __PS3
	static void SetEdgeBuffers(int bufferSizeVerts, void* bufferDataVerts, int bufferSizeIndices, void* bufferDataIndices);
#endif // __PS3

#if VEHICLE_GLASS_DEV && !__RESOURCECOMPILER
	static const fwVehicleGlassWindowProxy* GetOrCreateNewWindowProxy(const char* archetypeName, const fragType* pFragType, u32 lodLevel, u32 modelIndex, int componentId, bool bCreate, bool bOutputDebugFiles, bool bIsFromHD, bool bAllowExposedEdges);
	static const fwVehicleGlassWindowData* GetOrCreateNewWindowData(const char* archetypeName, const fragType* pFragType, u32 lodLevel, u32 modelIndex, bool bCreate, bool bOutputDebugFiles, bool bIsFromHD, bool bAllowExposedEdges);
	static void ReleaseAllWindowProxies();
	static void ReleaseAllWindowData();
	static void AddWidgets(bkBank& bk);
#endif // VEHICLE_GLASS_DEV && !__RESOURCECOMPILER

	static void ConstructWindows(atArray<fwVehicleGlassWindowProxy*,0,unsigned short>* proxies, const char* archetypeName, const fragType* pFragType, u32 lodLevel, const fwVehicleGlassConstructorTriangle* glassTris, int numGlassTris, bool bOutputDebugFiles, bool bIsFromHD, bool bAllowExposedEdges, u32 externalFlags);
#if __RESOURCECOMPILER
	static fwVehicleGlassWindowData* ConstructWindowData(const char* pFilename, const fragType* pFragType, u32 lodLevel, const fwVehicleGlassConstructorTriangle* glassTris, int numGlassTris, bool bIsFromHD, bool bAllowExposedEdges, u32 externalFlags);
#endif // __RESOURCECOMPILER
};

} // namespace rage

#endif // VEHICLE_GLASS_CONSTRUCTOR
#endif // VEHICLEGLASSCONSTRUCTOR_H
