// =====================================
// vfx/vehicleglass/vehicleglasswindow.h
// (c) 2012 RockstarNorth
// =====================================

#ifndef VEHICLEGLASSWINDOW_H
#define VEHICLEGLASSWINDOW_H

#include "math/amath.h"
#include "system/bit.h"
#include "system/floattoint.h"
#include "vectormath/classes.h"

#include "vfx/vehicleglass/vehicleglassconstructor.h"

namespace rage {

class datResource;

namespace fwVehicleGlassUtil
{
	template <typename T> __forceinline T QuantiseData(float value, float valueMin, float valueMax);

	template <> __forceinline u8    QuantiseData<u8   >(float value, float valueMin, float valueMax) { return (u8 )Clamp<float>(floorf(0.5f +   255.0f*(value - valueMin)/(valueMax - valueMin)), 0.0f,   255.0f); }
	template <> __forceinline u16   QuantiseData<u16  >(float value, float valueMin, float valueMax) { return (u16)Clamp<float>(floorf(0.5f + 65535.0f*(value - valueMin)/(valueMax - valueMin)), 0.0f, 65535.0f); }
	template <> __forceinline float QuantiseData<float>(float value, float, float) { return value; }

	template <typename T> __forceinline float UnquantiseData(T value, float valueMin, float valueMax);

	template <> __forceinline float UnquantiseData<u8   >(u8    value, float valueMin, float valueMax) { return valueMin + (valueMax - valueMin)*((float)value/  255.0f); }
	template <> __forceinline float UnquantiseData<u16  >(u16   value, float valueMin, float valueMax) { return valueMin + (valueMax - valueMin)*((float)value/65535.0f); }
	template <> __forceinline float UnquantiseData<float>(float value, float, float) { return value; }

	// Valid data returned in X component only
	template <typename T> __forceinline Vec4V_Out UnquantiseData(const T *pValue, Vec4V_In valueMin, Vec4V_In valueMax);

	template <> __forceinline Vec4V_Out UnquantiseData<u8>(const u8 *pValue, Vec4V_In valueMin, Vec4V_In valueMax)
	{
		using namespace Vec;
#if __BE
		Vector_4V value = V4LoadLeft(pValue);
#else
		Vector_4V value = V4LoadUnalignedSafe<4>(pValue-3);
#endif
		value = V4ShiftRight<24>(value);
		value = V4IntToFloatRaw<0>(value);
		value = V4Scale(value, V4VConstantSplat<FLOAT_TO_INT(1.f/255.f)>());
		const Vector_4V valueRange = V4Subtract(valueMax.GetIntrin128(), valueMin.GetIntrin128());
		return Vec4V(V4AddScaled(valueMin.GetIntrin128(), valueRange, value));
	}
	template <> __forceinline Vec4V_Out UnquantiseData<u16>(const u16 *pValue, Vec4V_In valueMin, Vec4V_In valueMax)
	{
		using namespace Vec;
#if __BE
		Vector_4V value = V4LoadLeft(pValue);
#else
		Vector_4V value = V4LoadUnalignedSafe<4>(pValue-1);
#endif
		value = V4ShiftRight<16>(value);
		value = V4IntToFloatRaw<0>(value);
		value = V4Scale(value, V4VConstantSplat<FLOAT_TO_INT(1.f/65535.f)>());
		const Vector_4V valueRange = V4Subtract(valueMax.GetIntrin128(), valueMin.GetIntrin128());
		return Vec4V(V4AddScaled(valueMin.GetIntrin128(), valueRange, value));
	}
	template <> __forceinline Vec4V_Out UnquantiseData<float>(const float *pValue, Vec4V_In, Vec4V_In)
	{
		return Vec4V(Vec::V4LoadLeft(pValue));
	}
}

class fwVehicleGlassWindowBase
{
public:
	Mat34V m_basis;
	u32    m_tag; // 'VGWC'
	u16    m_componentId;
	u16    m_geomIndex;
	u16    m_dataCols;
	u16    m_dataRows;
	u32    m_dataRLESize;
	u8*    m_dataRLE;
	float  m_dataMin; // 0x00 maps to this value
	float  m_dataMax; // 0xff maps to this value
#if __PS3
	u32    m_pad[1];  // ps3 requires padding this out so that fwVehicleGlassWindow doesn't overlap with base class
#endif
};

#define VGW_DEFAULT_TEXTURE_SCALE (2.0f)

class fwVehicleGlassWindow : public fwVehicleGlassWindowBase
{
public:
	enum eVGWFlags
	{
		VGW_FLAG_HAS_EXPOSED_EDGES = BIT(0),
		VGW_FLAG_IS_FROM_HD_MESH   = BIT(1),

		VGW_FLAG_VERSION_SHIFT   = 16,
		VGW_FLAG_VERSION_CURRENT = 2,
		VGW_FLAG_VERSION_MASK    = 0x00ff0000,

		VGW_FLAG_ERROR_MULTIPLE_MATERIALS  = BIT(24),
		VGW_FLAG_ERROR_MIXED_MATERIALS     = BIT(25),
		VGW_FLAG_ERROR_MULTIPLE_GEOMETRIES = BIT(26),
		VGW_FLAG_ERROR_DAMAGE_SCALE_NOT_01 = BIT(27),
		VGW_FLAG_ERROR_NO_BOUNDARY_EDGES   = BIT(28),
		VGW_FLAG_ERROR_MASK                = 0xff000000,
	};

	fwVehicleGlassWindow();
	~fwVehicleGlassWindow();
#if __RESOURCECOMPILER
	void ByteSwap();
#endif // __RESOURCECOMPILER

	const fwVehicleGlassWindow& GetWindow() const { return *this; }

#if __BANK
	int GetDataValueCode(int i, int j) const;
#endif // __BANK
	float GetDataValue(int i, int j) const;
	float GetDistance(Vec3V_In p, bool) const;
	__forceinline ScalarV_Out GetDistanceLerpI(int i0, int j, ScalarV_In lerpi) const;
	ScalarV_Out GetDistanceLerpIJ(int i0, int j0, ScalarV_In lerpi, ScalarV_In lerpj, bool) const;

	Mat34V_Out GetBasis(bool) const
	{
		return m_basis;
	}

	inline int GetVersion() const
	{
		return (int)((GetFlags() & VGW_FLAG_VERSION_MASK)>>VGW_FLAG_VERSION_SHIFT);
	}

	inline u32 GetFlags() const
	{
		return (m_dataRLE > (const void*)&m_flags) ? m_flags : 0;
	}

	inline float GetTextureScale() const
	{
		return (GetVersion() >= 1) ? m_textureScale : VGW_DEFAULT_TEXTURE_SCALE;
	}

private:
	u32   m_flags;
	float m_textureScale;

	friend class fwVGWComponentConstructor;
};

#if VEHICLE_GLASS_CONSTRUCTOR
class fwVehicleGlassWindowProxy
{
public:
	fwVehicleGlassWindowProxy();
	~fwVehicleGlassWindowProxy();

	const fwVehicleGlassWindow& GetWindow() const { return *m_window; }

	float GetDataValue(int i, int j) const;
	float GetDistance(Vec3V_In p, bool bWindowUseUncompressedData) const;
	ScalarV_Out GetDistanceLerpIJ(int i0, int j0, ScalarV_In lerpi, ScalarV_In lerpj, bool bWindowUseUncompressedData) const;

	Mat34V_Out GetBasis(bool bWindowUseUncompressedData) const
	{
		return bWindowUseUncompressedData ? m_basis : m_window->m_basis;
	}

	Mat34V m_basis;
	int    m_componentId;
	int    m_boneIndex;
	int    m_dataW;
	int    m_dataH;
	float* m_data;

	fwVehicleGlassWindow* m_window;
};
#else
typedef fwVehicleGlassWindow fwVehicleGlassWindowProxy;
#endif

class fwVehicleGlassWindowHeader
{
public:
	fwVehicleGlassWindowHeader(int numWindows, int dataSize) : m_tag('VGWH'), m_windowSize(sizeof(fwVehicleGlassWindow)), m_numWindows((u16)numWindows), m_dataSize(dataSize) {}
#if __RESOURCECOMPILER
	void ByteSwap();
#endif // __RESOURCECOMPILER

	u32 m_tag; // 'VGWH'
	u16 m_windowSize; // zero in older data
	u16 m_numWindows;
	u32 m_dataSize;
};

class fwVehicleGlassWindowRef
{
public:
	fwVehicleGlassWindowRef(int componentId, u32 offset) : m_componentId(componentId), m_offset(offset) {}
#if __RESOURCECOMPILER
	void ByteSwap();
#endif // __RESOURCECOMPILER

	int m_componentId;
	u32 m_offset;
};

class fwVehicleGlassWindowData
{
public:
#if VEHICLE_GLASS_CONSTRUCTOR
	static fwVehicleGlassWindowData* ConstructWindowData(fwVehicleGlassWindowProxy** windows, int numWindows, bool bByteSwap = false);
#endif // VEHICLE_GLASS_CONSTRUCTOR
#if !__RESOURCECOMPILER
	static void PointerFixup(datResource& rsc, fwVehicleGlassWindowData*& data);
#endif // !__RESOURCECOMPILER
	static const fwVehicleGlassWindow* GetWindow(const fwVehicleGlassWindowData* data, int componentId);
	static const fwVehicleGlassWindow* GetWindowByIndex(const fwVehicleGlassWindowData* data, int windowIndex);
};

} // namespace rage

#endif // VEHICLEGLASSWINDOW_H
