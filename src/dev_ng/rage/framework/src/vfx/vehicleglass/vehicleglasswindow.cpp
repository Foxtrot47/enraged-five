// =======================================
// vfx/vehicleglass/vehicleglasswindow.cpp
// (c) 2012 RockstarNorth
// =======================================

#include "data/resource.h"
#include "system/endian.h"
#include "system/memops.h"

#include "fwutil/xmacro.h"
#include "vfx/vehicleglass/vehicleglasswindow.h"

namespace rage {

fwVehicleGlassWindow::fwVehicleGlassWindow()
{
	// make sure we aren't overlapping with the base class
	CompileTimeAssert((OffsetOf(fwVehicleGlassWindow, m_flags) & 15) == 0);

	sysMemSet(this, 0, sizeof(*this));

	m_tag = 'VGWC';
	m_flags = VGW_FLAG_VERSION_CURRENT << VGW_FLAG_VERSION_SHIFT;
}

fwVehicleGlassWindow::~fwVehicleGlassWindow()
{
	if (m_dataRLE)
	{
		delete[] m_dataRLE;
	}
}

#if __RESOURCECOMPILER

void fwVehicleGlassWindow::ByteSwap()
{
	if (m_dataRLE)
	{
		for (int j = 0; j < m_dataRows; j++)
		{
			sysEndian::SwapMe(((u16*)m_dataRLE)[j]);
		}
	}

	sysEndian::SwapMe(m_basis);
	sysEndian::SwapMe(m_tag);
	sysEndian::SwapMe(m_componentId);
	sysEndian::SwapMe(m_geomIndex);
	sysEndian::SwapMe(m_dataCols);
	sysEndian::SwapMe(m_dataRows);
	sysEndian::SwapMe(m_dataRLESize);
	sysEndian::SwapMe(m_dataMin);
	sysEndian::SwapMe(m_dataMax);

	sysEndian::SwapMe(m_flags);
	sysEndian::SwapMe(m_textureScale);

	m_dataRLE = NULL;
}

#endif // __RESOURCECOMPILER

#if __BANK
int fwVehicleGlassWindow::GetDataValueCode(int i, int j) const
{
	Assert(m_dataRLE);

	if (i >= 0 && i < m_dataCols &&
		j >= 0 && j < m_dataRows)
	{
		const u16* rowOffsets = (const u16*)(m_dataRLE);
		const u8* data = m_dataRLE + m_dataRows*sizeof(u16) + (int)rowOffsets[j];

		if (*data != 255)
		{
			const int colA = (int)(*(data++));
			const int colB = (int)(*(data++));

			if (i >= colA)
			{
				if (i <= colB)
				{
					return 1; // inside first RLE span
				}
				else
				{
					data += colB - colA + 1;

					if (*data != 255)
					{
						const int colC = (int)(*(data++));
						const int colD = (int)(*(data++));

						if (i <= colD)
						{
							if (i >= colC)
							{
								return 2; // inside second RLE span
							}
							else
							{
								return 3; // between spans
							}
						}
					}
				}
			}
		}
	}

	return 0; // outside bounds
}
#endif // __BANK

float fwVehicleGlassWindow::GetDataValue(int i, int j) const
{
	Assert(m_dataRLE);

	i = Clamp<int>(i, 0, m_dataCols - 1);
	j = Clamp<int>(j, 0, m_dataRows - 1);

	const u16* rowOffsets = (const u16*)(m_dataRLE);
	const u8* data = m_dataRLE + m_dataRows*sizeof(u16) + (int)rowOffsets[j];

	if (*data != 255)
	{
		const int colA = (int)(*(data++));
		const int colB = (int)(*(data++));

		if (i >= colA)
		{
			if (i <= colB)
			{
				return fwVehicleGlassUtil::UnquantiseData<u8>(data[i - colA], m_dataMin, m_dataMax);
			}
			else
			{
				data += colB - colA + 1;

				if (*data != 255)
				{
					const int colC = (int)(*(data++));
					const int colD = (int)(*(data++));

					if (i <= colD)
					{
						if (i >= colC)
						{
							return fwVehicleGlassUtil::UnquantiseData<u8>(data[i - colC], m_dataMin, m_dataMax);
						}
						else
						{
							return m_dataMax;
						}
					}
				}
			}
		}
	}

	return m_dataMin;
}

float fwVehicleGlassWindow::GetDistance(Vec3V_In p, bool) const
{
	if (m_dataRLE == NULL)
	{
		return FLT_MAX;
	}

	const Vec2V v = Transform(m_basis, p).GetXY();

	const float x = v.GetXf() - 0.5f;
	const float y = v.GetYf() - 0.5f;

	const float xFloor = rage::FloorfFast(x);
	const float yFloor = rage::FloorfFast(y);

	const float xf = x - xFloor;
	const float yf = y - yFloor;

	const int i = (int)xFloor;
	const int j = (int)yFloor;

	const float v00 = GetDataValue(i + 0, j + 0);
	const float v10 = GetDataValue(i + 1, j + 0);
	const float v01 = GetDataValue(i + 0, j + 1);
	const float v11 = GetDataValue(i + 1, j + 1);

	return v00*(1.0f - xf)*(1.0f - yf) + v10*xf*(1.0f - yf) + v01*(1.0f - xf)*yf + v11*xf*yf;
}

__forceinline ScalarV_Out fwVehicleGlassWindow::GetDistanceLerpI(int i0, int j, ScalarV_In lerpi) const
{
	using namespace Vec;

	const int i1 = i0+1;
	const u16* rowOffsets = (const u16*)(m_dataRLE);
	const u8 *data = (u8*)m_dataRLE + m_dataRows*sizeof(u16) + rowOffsets[j];
	const Vec4V dataMin = Vec4V(V4LoadLeft(&m_dataMin));
	const Vec4V dataMax = Vec4V(V4LoadLeft(&m_dataMax));
	Vec4V v0=dataMin, v1=dataMin;
	if (*data != 255)
	{
		const int colA = data[0];
		const int colB = data[1];
		int colC = colB;
		int colD = colB;
		if (data[2+(colB-colA+1)] != 255)
		{
			colC = data[2+(colB-colA+1)+0];
			colD = data[2+(colB-colA+1)+1];
		}

		if (i0 >= colA)
		{
			if (i0 <= colB)
			{
				const u8 *const ptr = data+2+(i0-colA);
				v0 = fwVehicleGlassUtil::UnquantiseData<u8>(ptr+0, dataMin, dataMax);
				if (i1 <= colB)
				{
					v1 = fwVehicleGlassUtil::UnquantiseData<u8>(ptr+1, dataMin, dataMax);
				}
				else if (i1 < colC)
				{
					v1 = dataMax;
				}
			}
			else if (i0 < colC)
			{
				v0 = dataMax;
				if (i1 == colC)
				{
					v1 = fwVehicleGlassUtil::UnquantiseData<u8>(data+2+(colB-colA+1)+2, dataMin, dataMax);
				}
				else
				{
					v1 = dataMax;
				}
			}
			else if (i0 <= colD)
			{
				const u8 *const ptr = data+2+(colB-colA+1)+2+(i0-colC);
				v0 = fwVehicleGlassUtil::UnquantiseData<u8>(ptr+0, dataMin, dataMax);
				if (i1 <= colD)
				{
					v1 = fwVehicleGlassUtil::UnquantiseData<u8>(ptr+1, dataMin, dataMax);
				}
			}
		}
		else if (i1 == colA)
		{
			v1 = fwVehicleGlassUtil::UnquantiseData<u8>(data+2, dataMin, dataMax);
		}
	}
	// return v0*(1.f-lerpi) + v1*lerpi;
	return AddScaled(SubtractScaled(v0,v0,lerpi),v1,lerpi).GetX();
}

ScalarV_Out fwVehicleGlassWindow::GetDistanceLerpIJ(int i0, int j0, ScalarV_In lerpi_, ScalarV_In lerpj_, bool) const
{
	if (m_dataRLE == NULL)
	{
		return ScalarV(FLT_MAX);
	}
	else
	{
#		if __ASSERT
			const float v00 = GetDataValue(i0+0, j0+0);
			const float v10 = GetDataValue(i0+1, j0+0);
			const float v01 = GetDataValue(i0+0, j0+1);
			const float v11 = GetDataValue(i0+1, j0+1);
			const float lerpif = lerpi_.Getf();
			const float lerpjf = lerpj_.Getf();
			const float check = v00*(1.0f - lerpif)*(1.0f - lerpjf) + v10*lerpif*(1.0f - lerpjf) + v01*(1.0f - lerpif)*lerpjf + v11*lerpif*lerpjf;
#		endif

		// Grab a copy of lerpi and lerpj since it may be passed by const ref
		// instead of by value, and we possibly need to change them.
		ScalarV lerpi = lerpi_;
		ScalarV lerpj = lerpj_;

		if (i0 < 0)
		{
			i0 = 0;
			lerpi = ScalarV(V_ZERO);
		}
		else if (i0 >= m_dataCols-1)
		{
			i0 = m_dataCols-2;
			lerpi = ScalarV(V_ONE);
		}

		if (j0 < 0)
		{
			j0 = 0;
			lerpj = ScalarV(V_ZERO);
		}
		else if (j0 >= m_dataRows-1)
		{
			j0 = m_dataRows-2;
			lerpj = ScalarV(V_ONE);
		}

		const ScalarV v0 = GetDistanceLerpI(i0, j0+0, lerpi);
		const ScalarV v1 = GetDistanceLerpI(i0, j0+1, lerpi);
		// ret = v0*(1.f-lerpj) + v1*lerpj;
		const ScalarV ret = AddScaled(SubtractScaled(v0,v0,lerpj),v1,lerpj);

#		if __ASSERT
			const float err = Abs<float>(ret.Getf() - check);
			static float errMax = 0.001f;
			if (errMax < err)
			{
				errMax = err;
				Assertf(0, "distance field check failed: ret=%f, check=%f, err=%f", ret.Getf(), check, err);
			}
#		endif

		return ret;
	}
}

#if VEHICLE_GLASS_CONSTRUCTOR

fwVehicleGlassWindowProxy::fwVehicleGlassWindowProxy()
{
	sysMemSet(this, 0, sizeof(*this));
}

fwVehicleGlassWindowProxy::~fwVehicleGlassWindowProxy()
{
	if (m_data)
	{
		delete[] m_data;
	}

	if (m_window)
	{
		delete m_window;
	}
}

float fwVehicleGlassWindowProxy::GetDataValue(int i, int j) const
{
	Assert(m_data);

	i = Clamp<int>(i, 0, m_dataW - 1);
	j = Clamp<int>(j, 0, m_dataH - 1);

	return m_data[i + j*m_dataW];
}

float fwVehicleGlassWindowProxy::GetDistance(Vec3V_In p, bool bWindowUseUncompressedData) const
{
	if (m_window && (!bWindowUseUncompressedData || m_data == NULL))
	{
		return m_window->GetDistance(p, false);
	}

	if (m_data == NULL)
	{
		return FLT_MAX;
	}

	const Vec2V v = Transform(m_basis, p).GetXY();

	const float x = v.GetXf() - 0.5f;
	const float y = v.GetYf() - 0.5f;

	const float xf = x - floorf(x);
	const float yf = y - floorf(y);

	const int i = (int)floorf(x);
	const int j = (int)floorf(y);

	const float v00 = GetDataValue(i + 0, j + 0);
	const float v10 = GetDataValue(i + 1, j + 0);
	const float v01 = GetDataValue(i + 0, j + 1);
	const float v11 = GetDataValue(i + 1, j + 1);

	return v00*(1.0f - xf)*(1.0f - yf) + v10*xf*(1.0f - yf) + v01*(1.0f - xf)*yf + v11*xf*yf;
}

ScalarV_Out fwVehicleGlassWindowProxy::GetDistanceLerpIJ(int i0, int j0, ScalarV_In lerpi, ScalarV_In lerpj, bool bWindowUseUncompressedData) const
{
	if (m_window && (!bWindowUseUncompressedData || m_data == NULL))
	{
		return m_window->GetDistanceLerpIJ(i0, j0, lerpi, lerpj, false);
	}

	if (m_data == NULL)
	{
		return ScalarV(FLT_MAX);
	}

	const float v00 = GetDataValue(i0 + 0, j0 + 0);
	const float v10 = GetDataValue(i0 + 1, j0 + 0);
	const float v01 = GetDataValue(i0 + 0, j0 + 1);
	const float v11 = GetDataValue(i0 + 1, j0 + 1);
	const float lerpif = lerpi.Getf();
	const float lerpjf = lerpj.Getf();

	return ScalarV(v00*(1.0f - lerpif)*(1.0f - lerpjf) + v10*lerpif*(1.0f - lerpjf) + v01*(1.0f - lerpif)*lerpjf + v11*lerpif*lerpjf);
}

#if __RESOURCECOMPILER

void fwVehicleGlassWindowHeader::ByteSwap()
{
	sysEndian::SwapMe(m_tag);
	sysEndian::SwapMe(m_windowSize);
	sysEndian::SwapMe(m_numWindows);
	sysEndian::SwapMe(m_dataSize);
}

void fwVehicleGlassWindowRef::ByteSwap()
{
	sysEndian::SwapMe(m_componentId);
	sysEndian::SwapMe(m_offset);
}

#endif // __RESOURCECOMPILER

__COMMENT(static) fwVehicleGlassWindowData* fwVehicleGlassWindowData::ConstructWindowData(fwVehicleGlassWindowProxy** windows, int numWindows, bool bByteSwap)
{
	if (numWindows > 0)
	{
		u32 dataSize = 0;
		u32 offset = 0;

		offset += sizeof(fwVehicleGlassWindowHeader);
		offset += numWindows*sizeof(fwVehicleGlassWindowRef);
		offset = (offset + 15)&~15; // align to 16

		dataSize += offset;

		for (int i = 0; i < numWindows; i++)
		{
			dataSize += sizeof(fwVehicleGlassWindow);
			dataSize += (windows[i]->m_window->m_dataRLESize + 15)&~15; // align to 16
		}

		u8* data = rage_new u8[dataSize]; // do we need to align this allocation?

		sysMemSet(data, 0, dataSize);

		fwVehicleGlassWindowHeader* dstHeader = (fwVehicleGlassWindowHeader*)data;
		fwVehicleGlassWindowRef* dstRefs = (fwVehicleGlassWindowRef*)(data + sizeof(fwVehicleGlassWindowHeader));

		*dstHeader = fwVehicleGlassWindowHeader(numWindows, dataSize);

		for (int i = 0; i < numWindows; i++)
		{
			const fwVehicleGlassWindow* srcWindow = windows[i]->m_window;

			dstRefs[i] = fwVehicleGlassWindowRef(windows[i]->m_componentId, offset);
			offset += sizeof(fwVehicleGlassWindow);
			offset += (srcWindow->m_dataRLESize + 15)&~15;
		}

		for (int i = 0; i < numWindows; i++)
		{
			const fwVehicleGlassWindow* srcWindow = windows[i]->m_window;

			Assert(dstRefs[i].m_offset + sizeof(fwVehicleGlassWindow) + ((srcWindow->m_dataRLESize + 15)&~15) <= dataSize);
			//Assert(srcWindow->m_dataRLE);

			fwVehicleGlassWindow* dstWindow = (fwVehicleGlassWindow*)(data + dstRefs[i].m_offset);

			sysMemCpy(dstWindow, srcWindow, sizeof(fwVehicleGlassWindow));

			if (srcWindow->m_dataRLESize > 0)
			{
				u8* dstWindowDataRLE = (u8*)dstWindow + sizeof(fwVehicleGlassWindow);

				sysMemCpy(dstWindowDataRLE, srcWindow->m_dataRLE, srcWindow->m_dataRLESize);
				dstWindow->m_dataRLE = dstWindowDataRLE;
			}
			else
			{
				dstWindow->m_dataRLE = NULL;
			}
		}

#if __RESOURCECOMPILER
		if (bByteSwap)
		{
			dstHeader->ByteSwap();

			for (int i = 0; i < numWindows; i++)
			{
				fwVehicleGlassWindow* dstWindow = (fwVehicleGlassWindow*)(data + dstRefs[i].m_offset);

				dstWindow->ByteSwap();
				dstRefs[i].ByteSwap();
			}
		}
		else // don't byte swap, but we still want to NULL out the internal pointers before serialising
		{
			for (int i = 0; i < numWindows; i++)
			{
				fwVehicleGlassWindow* dstWindow = (fwVehicleGlassWindow*)(data + dstRefs[i].m_offset);

				dstWindow->m_dataRLE = NULL;
			}
		}
#else
		(void)bByteSwap;
#endif
		return (fwVehicleGlassWindowData*)data;
	}

	return NULL;
}

#endif // VEHICLE_GLASS_CONSTRUCTOR

#if !__RESOURCECOMPILER

__COMMENT(static) void fwVehicleGlassWindowData::PointerFixup(datResource& rsc, fwVehicleGlassWindowData*& data)
{
	if (data)
	{
		rsc.PointerFixup(data);

		Assert((((size_t)data)&15) == 0); // must be aligned

		const fwVehicleGlassWindowHeader& header = *(const fwVehicleGlassWindowHeader*)data;
		const fwVehicleGlassWindowRef* windowRefs = (const fwVehicleGlassWindowRef*)((const u8*)data + sizeof(fwVehicleGlassWindowHeader));
		const u32 windowSize = Max<u32>((u32)header.m_windowSize, (u32)sizeof(fwVehicleGlassWindowBase));

		Assert(header.m_tag == 'VGWH');

		for (int i = 0; i < header.m_numWindows; i++)
		{
			fwVehicleGlassWindow* window = (fwVehicleGlassWindow*)((u8*)data + windowRefs[i].m_offset);

			if (window->m_dataRLESize > 0)
			{
				window->m_dataRLE = (u8*)window + windowSize;
			}
			else
			{
				window->m_dataRLE = NULL;
			}
		}
	}
}

#endif // !__RESOURCECOMPILER

__COMMENT(static) const fwVehicleGlassWindow* fwVehicleGlassWindowData::GetWindow(const fwVehicleGlassWindowData* data, int componentId)
{
	const fwVehicleGlassWindowHeader& header = *(const fwVehicleGlassWindowHeader*)data;
	const fwVehicleGlassWindowRef* windowRefs = (const fwVehicleGlassWindowRef*)((const u8*)data + sizeof(fwVehicleGlassWindowHeader));
	const fwVehicleGlassWindow* window = NULL;

	Assert(header.m_tag == 'VGWH');

	for (u32 i = 0; i < header.m_numWindows; i++)
	{
		const int id = windowRefs[i].m_componentId;

		Assert(i == 0 || id > windowRefs[i - 1].m_componentId);

		if (id == componentId)
		{
			window = (const fwVehicleGlassWindow*)((const u8*)data + windowRefs[i].m_offset);
			break;
		}
		else if (id > componentId) // assume windows are stored in order of componentId
		{
			break;
		}
	}

	Assert(window == NULL || (window->m_tag == 'VGWC' && window->m_componentId == componentId)); // if this fails, it's corrupted ..
	return window;
}

__COMMENT(static) const fwVehicleGlassWindow* fwVehicleGlassWindowData::GetWindowByIndex(const fwVehicleGlassWindowData* data, int windowIndex)
{
	const fwVehicleGlassWindowHeader& header = *(const fwVehicleGlassWindowHeader*)data;
	const fwVehicleGlassWindowRef* windowRefs = (const fwVehicleGlassWindowRef*)((const u8*)data + sizeof(fwVehicleGlassWindowHeader));

	Assert(header.m_tag == 'VGWH');

	if (windowIndex >= 0 && windowIndex < header.m_numWindows)
	{
		return (const fwVehicleGlassWindow*)((const u8*)data + windowRefs[windowIndex].m_offset);
	}

	return NULL;
}

} // namespace rage
