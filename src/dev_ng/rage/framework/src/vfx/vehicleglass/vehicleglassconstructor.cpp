// ============================================
// vfx/vehicleglass/vehicleglassconstructor.cpp
// (c) 2012 RockstarNorth
// ============================================

/*
TODO -
- support threshold < 0 by some amount
- span merging in ConstructSpans is not efficient, it would be much better to flip the basis 90 degrees for highly non-convex shapes
- triangle rasterisation checks coverage of tiny squares instead of simply testing points
- noise texture
- widgets to control threshold - automatically re-create and detach triangles when slider is adjusted
- also, try catching how to catch missing vehicle_vehglass_inner or _just_ vehicle_vehglass_inner
- what about windows which have open edges (e.g. not framed the the door) can we automatically adjust the distance field for these?

other stuff
- vehicle_vehglass - do we need tangents (since we use normal maps?)
- when glass first gets hit, it doesn't poke a hole .. but then hole grows too large too fast i think .. can we shrink the sphere maybe?

ninef has exposed edges on        [window_lf,window_rf]
ninef2 has exposed edges on       [window_lf,window_rf]
cheetah has exposed edges on      [window_lf,window_rf]
cogcabrio has exposed edges on    [window_lf,window_rf]
entityxf has exposed edges on     [window_lf,window_rf]
infernus has exposed edges on     [window_lf,window_rf]
stinger has exposed edges on      [window_lf,window_rf]
tornado has exposed edges on      [window_lf,window_rf]
voodoo2 has exposed edges on      [window_lf,window_rf]
f620 has exposed edges on         [window_lf,window_rf]
ruiner has exposed edges on       [window_lf,window_rf]
penumbra has exposed edges on     [window_lf,window_rf]
manana has exposed edges on       [window_lf,window_rf]
sentinel has exposed edges on     [window_lf,window_rf]
sentinel2 has exposed edges on    [window_lf,window_rf]
comet2 has exposed edges on       [window_lf,window_rf]
tampa has exposed edges on        [window_lf,window_rf]
prairie has exposed edges on      [window_lf,window_rf]
tornado3 has exposed edges on     [window_lf,window_rf]
elegy2 has exposed edges on       [window_lf,window_rf]
venem has exposed edges on        [window_lf,window_rf]
veloce has exposed edges on       [window_lf,window_rf]
exemplar has exposed edges on     [window_lf,window_rf]
fusilade has exposed edges on     [window_lf,window_rf]
buccaneer has exposed edges on    [window_lf,window_rf]
seraph has exposed edges on       [window_lf,window_rf]
seraph2 has exposed edges on      [window_lf,window_rf]
schwarzer has exposed edges on    [window_lf,window_rf]
carbonizzare has exposed edges on [window_lf,window_rf]
sabregt has exposed edges on      [window_lf,window_rf]
vigero has exposed edges on       [window_lf,window_rf]
hotknife has exposed edges on     [window_lf,window_rf]
Dominator has exposed edges on    [window_lf,window_rf]
Gauntlet has exposed edges on     [window_lf,window_rf]
Phoenix has exposed edges on      [window_lf,window_rf]
tornado4 has exposed edges on     [window_lf,window_rf]
RapidGT has exposed edges on      [window_lf,window_rf]
RapidGT2 has exposed edges on     [window_lf,window_rf]
Surano has exposed edges on       [window_lf,window_rf]
monroe has exposed edges on       [window_lf,window_rf]

minivan has exposed edges on      [window_lf,window_rf,window_lr,window_rr]
issi2 has exposed edges on        [window_lf,window_rf,window_lr,window_rr]
tornado2 has exposed edges on     [window_lf,window_rf,window_lr,window_rr]
felon2 has exposed edges on       [window_lf,window_rf,window_lr,window_rr]
*/

#include "vfx/vehicleglass/vehicleglassconstructor.h"

#if VEHICLE_GLASS_CONSTRUCTOR

#include "crskeleton/skeletondata.h"
#include "file/default_paths.h"
#include "file/stream.h"
#include "grcore/debugdraw.h"
#include "grcore/edgeExtractgeomspu.h"
#include "grcore/effect.h"
#include "grcore/grcorespu.h"
#include "grcore/image.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "grmodel/geometry.h"
#include "phBound/support.h"
#include "string/stringutil.h"
#include "system/bootmgr.h"
#include "system/memops.h"
#include "system/nelem.h"
#include "system/xtl.h"
#include "vectormath/classes.h"

#include "fragment/drawable.h"
#include "fragment/type.h"

#include "fwmaths/vectorutil.h"
#include "vfx/vehicleglass/vehicleglasswindow.h"

#include <algorithm> // for std::swap

#define VEHICLE_GLASS_MAX_DATA_ROWS 256 // span coords must fit in u8
#define VEHICLE_GLASS_MAX_DATA_COLS 800

namespace rage {

namespace EDT {

// Euclidean Distance Transform (EDT)
// ----------------------------------------------------------------
// Algorithm based on Ricardo Fabbri's implementation of Maurer EDT
// http://www.lems.brown.edu/~rfabbri/stuff/fabbri-EDT-survey-ACMCSurvFeb2008.pdf
// http://tc18.liris.cnrs.fr/subfields/distance_skeletons/DistanceTransform.pdf
// http://www.lems.brown.edu/vision/people/leymarie/Refs/CompVision/DT/DTpaper.pdf
// http://www.comp.nus.edu.sg/~tants/jfa/i3d06.pdf (jump flooding)
// http://www.comp.nus.edu.sg/~tants/jfa/rong-guodong-phd-thesis.pdf
// http://en.wikipedia.org/wiki/Distance_transform

static __forceinline void MaurerEDT_Horizontal(int* img, int w, int /*h*/, int y)
{
	int* row = &img[y*w];

	if (row[0] != 0)
	{
		row[0] = w;
	}

	for (int x = 1; x < w; x++)
	{
		if (row[x] != 0)
		{
			row[x] = row[x - 1] + 1;
		}
	}

	for (int x = w - 2; x >= 0; x--)
	{
		if (row[x] > row[x + 1] + 1)
		{
			row[x] = row[x + 1] + 1;
		}
	}

	for (int x = 0; x < w; x++)
	{
		if (row[x] >= w)
		{
			row[x] = -1; // -1 is "infinity"
		}
		else
		{
			row[x] = row[x]*row[x]; // distance squared
		}
	}
}

static __forceinline bool MaurerEDT_Remove(int du, int dv, int dw, int u, int v, int w)
{
	s64 a = v - u; // these need to be 64-bit ints so the calculation below doesn't overflow
	s64 b = w - v;
	s64 c = w - u;

	return (c*dv - b*du - a*dw) > a*b*c;
}

static void MaurerEDT_Vertical(int* img, int w, int h, int x, int* G, int* H)
{
	int l1 = -1;
	int l2 = 0;

	int* col = img + x;

	for (int y = 0; y < h; y++)
	{
		const int fi = *col; col += w;

		if (fi != -1)
		{
			while (l1 > 0 && MaurerEDT_Remove(G[l1-1], G[l1], fi, H[l1-1], H[l1], y))
			{
				l1--;
			}

			l1++;
			G[l1] = fi;
			H[l1] = y;
		}
	}

	if (l1 == -1)
	{
		return;
	}

	col = img + x;

	for (int y = 0; y < h; y++)
	{
		int tmp0 = H[l2] - y;
		int tmp1 = G[l2] + tmp0*tmp0;

		while (l2 < l1)
		{
			const int tmp2 = H[l2+1] - y;

			if (tmp1 <= G[l2+1] + tmp2*tmp2)
			{
				break;
			}

			l2++;
			tmp0 = H[l2] - y;
			tmp1 = G[l2] + tmp0*tmp0;
		}

		*col = tmp1; col += w;
	}
}

static void MaurerEDT(int* img, int w, int h)
{
	int* G = rage_new int[h];
	int* H = rage_new int[h];

	for (int y = 0; y < h; y++)
	{
		MaurerEDT_Horizontal(img, w, h, y);
	}

	for (int x = 0; x < w; x++)
	{
		MaurerEDT_Vertical(img, w, h, x, G, H);
	}

	delete[] G;
	delete[] H;
}

} // namespace EDT

template <typename T> static __forceinline void AppendBytes(atArray<u8>& bytes, T value);

template <> __forceinline void AppendBytes<u8 >(atArray<u8>& bytes, u8  value) { bytes.PushAndGrow(value); }

#if 0
template <> __forceinline void AppendBytes<u16>(atArray<u8>& bytes, u16 value)
{
#if __BE
	bytes.PushAndGrow((u8)(value >> 8));
	bytes.PushAndGrow((u8)(value & 255));
#else
	bytes.PushAndGrow((u8)(value & 255));
	bytes.PushAndGrow((u8)(value >> 8));
#endif
}
#endif

typedef fwVehicleGlassConstructorTriangleBase fwVGWTriangle;

class fwVGWTriangleRange
{
public:
	int m_componentId;
	int m_glassFirst;
	int m_glassLast;
};

class fwVGWEdge
{
public:
	fwVGWEdge() {}
	fwVGWEdge(Vec3V_In p0, Vec3V_In p1)
	{
		m_p[0] = p0;
		m_p[1] = p1;
	}

	Vec3V m_p[2];
};

class fwVGWComponentConstructor
{
public:
	fwVGWComponentConstructor(const char* modelName, const char* boneName)
	{
		sysMemSet(this, 0, sizeof(*this));

		m_modelName = modelName;
		m_boneName  = boneName;
	}

	~fwVGWComponentConstructor()
	{
		if (m_grid) { delete[] m_grid; }
		if (m_data) { delete[] m_data; }

		if (m_dataMaskGTEMin) { delete[] m_dataMaskGTEMin; }
		if (m_dataMaskLTEMax) { delete[] m_dataMaskLTEMax; }

		if (m_colA) { delete[] m_colA; }
		if (m_colB) { delete[] m_colB; }
		if (m_colC) { delete[] m_colC; }
		if (m_colD) { delete[] m_colD; }

		if (m_mergedSpans) { delete[] m_mergedSpans; }

		if (m_windowProxy)
		{
			delete m_windowProxy;
		}
	}

	Vec2V_Out TransformToBasisXY(Vec3V_In p) const
	{
		return Transform(m_basis, p).GetXY();
	}

	Vec2V_Out TransformToGrid(Vec3V_In p) const
	{
		const Vec2V temp = TransformToBasisXY(p);
		const int offset = m_downsample + m_gridMargin*2;

		const float x = 0.5f*(float)offset + (float)(m_gridCols - offset)*temp.GetXf();
		const float y = 0.5f*(float)offset + (float)(m_gridRows - offset)*temp.GetYf();

		return Vec2V(x, y);
	}

	Vec2V_Out TransformToData(Vec3V_In p) const
	{
		const Vec2V temp = TransformToBasisXY(p);
		const int offset = 1 + m_dataMargin*2;

		const float x = 0.5f*(float)offset + (float)(m_dataW - offset)*temp.GetXf();
		const float y = 0.5f*(float)offset + (float)(m_dataH - offset)*temp.GetYf();

		return Vec2V(x, y);
	}

	bool IsDataGTEMin(int i, int j) const
	{
		Assert(i >= 0 && i < m_dataW);
		Assert(j >= 0 && j < m_dataH);
		const int index = i + j*m_dataW;
		return (m_dataMaskGTEMin[index/8] & BIT(index%8)) != 0;
	}

	bool IsDataLTEMax(int i, int j) const
	{
		Assert(i >= 0 && i < m_dataW);
		Assert(j >= 0 && j < m_dataH);
		const int index = i + j*m_dataW;
		return (m_dataMaskLTEMax[index/8] & BIT(index%8)) != 0;
	}

	void RasteriseTriangle(Vec3V_In p0, Vec3V_In p1, Vec3V_In p2)
	{
		if (m_grid)
		{
			const Vec2V v0 = TransformToGrid(p0);
			const Vec2V v1 = TransformToGrid(p1);
			const Vec2V v2 = TransformToGrid(p2);

			const Vec2V bmin = Min(v0, v1, v2);
			const Vec2V bmax = Max(v0, v1, v2);

			const int i0 = Max<int>(0, (int)floorf(bmin.GetXf()));
			const int j0 = Max<int>(0, (int)floorf(bmin.GetYf()));
			const int i1 = Min<int>((int)ceilf(bmax.GetXf()), m_gridCols) - 1;
			const int j1 = Min<int>((int)ceilf(bmax.GetYf()), m_gridRows) - 1;

			if (i0 == i1 && j0 == j1) // triangle fits into a single grid cell
			{
				const int index = i0 + j0*m_gridCols;

				m_grid[index/8] |= BIT(index%8);
			}
			else // rasterise triangle by clipping to each grid cell (slow!)
			{
				int numCells = 0;

				for (int j = j0; j <= j1; j++)
				{
					for (int i = i0; i <= i1; i++)
					{
						// TODO -- i've shrunk the cell down to a point, really we should just be doing pixel centre tests here (would be much faster)
						const float cell_x0 = (float)(i + 0) + 0.49f;
						const float cell_y0 = (float)(j + 0) + 0.49f;
						const float cell_x1 = (float)(i + 1) - 0.49f;
						const float cell_y1 = (float)(j + 1) - 0.49f;

						Vec3V temp0[3 + 4];
						Vec3V temp1[3 + 4];
						int count = 0;

						temp0[count++] = Vec3V(v0, ScalarV(V_ZERO));
						temp0[count++] = Vec3V(v1, ScalarV(V_ZERO));
						temp0[count++] = Vec3V(v2, ScalarV(V_ZERO));

						// clip to four planes
						count = PolyClip(temp1, NELEM(temp1), temp0, count, BuildPlane(Vec3V(cell_x0, cell_y0, 0.0f), +Vec3V(V_X_AXIS_WZERO)));
						count = PolyClip(temp0, NELEM(temp0), temp1, count, BuildPlane(Vec3V(cell_x0, cell_y0, 0.0f), +Vec3V(V_Y_AXIS_WZERO)));
						count = PolyClip(temp1, NELEM(temp1), temp0, count, BuildPlane(Vec3V(cell_x1, cell_y1, 0.0f), -Vec3V(V_X_AXIS_WZERO)));
						count = PolyClip(temp0, NELEM(temp0), temp1, count, BuildPlane(Vec3V(cell_x1, cell_y1, 0.0f), -Vec3V(V_Y_AXIS_WZERO)));

						if (count > 0)
						{
							const int index = i + j*m_gridCols;

							m_grid[index/8] |= BIT(index%8);
							numCells++;
						}
					}
				}
			}
		}
	}

	void SampleAlongBoundary(float& dmin, float& dmax, float& davg, const atArray<fwVGWEdge>& boundaryEdges) const
	{
		float sampleSum = 0.0f;
		int sampleCount = 0;

		dmin = +FLT_MAX;
		dmax = -FLT_MAX;

		for (int i = 0; i < boundaryEdges.GetCount(); i++)
		{
			const Vec2V p0 = TransformToData(boundaryEdges[i].m_p[0]);
			const Vec2V p1 = TransformToData(boundaryEdges[i].m_p[1]);
			const float len = Mag(p1 - p0).Getf();
			const int steps = (int)ceilf(len*500.0f);

			for (int k = 0; k <= steps; k++)
			{
				const float t = (float)k/(float)steps;
				const Vec2V p = p0 + (p1 - p0)*ScalarV(t);

				float px = p.GetXf();
				float py = p.GetYf();

				const int pi = (int)floorf(px) + 1;
				const int pj = (int)floorf(py);

				px -= floorf(px);
				py -= floorf(py);

				float d00 = m_data[Clamp<int>(pi + 0, 0, m_dataW - 1) + Clamp<int>(pj + 0, 0, m_dataH - 1)*m_dataW];
				float d10 = m_data[Clamp<int>(pi + 1, 0, m_dataW - 1) + Clamp<int>(pj + 0, 0, m_dataH - 1)*m_dataW];
				float d01 = m_data[Clamp<int>(pi + 0, 0, m_dataW - 1) + Clamp<int>(pj + 1, 0, m_dataH - 1)*m_dataW];
				float d11 = m_data[Clamp<int>(pi + 1, 0, m_dataW - 1) + Clamp<int>(pj + 1, 0, m_dataH - 1)*m_dataW];

				d00 += (d10 - d00)*px;
				d01 += (d11 - d01)*px;

				d00 += (d01 - d00)*py;

				dmin = Min<float>(d00, dmin);
				dmax = Max<float>(d00, dmax);

				sampleSum += d00;
				sampleCount++;
			}
		}

		davg = sampleSum/(float)sampleCount;
	}

	void ConstructDistanceField(int blurPasses, bool blurNormalise, const atArray<fwVGWEdge>& boundaryEdges, u32& flags)
	{
		Assert(m_gridCols%m_downsample == 0);
		Assert(m_gridRows%m_downsample == 0);
		Assert(m_gridMargin%m_downsample == 0);

		const int w = m_gridCols/m_downsample;
		const int h = m_gridRows/m_downsample;

		m_dataMargin = m_gridMargin/m_downsample;
		m_dataW = w;
		m_dataH = h;
		m_data = rage_new float[w*h];
		sysMemSet(m_data, 0, w*h*sizeof(float));

		int* temp = rage_new int[m_gridRows*m_gridCols];

		// apply positive range
		{
			for (int index = 0; index < m_gridRows*m_gridCols; index++)
			{
				temp[index] = (m_grid[index/8] & BIT(index%8)) ? 1 : 0;
			}

			EDT::MaurerEDT(temp, m_gridCols, m_gridRows);

			for (int j = 0; j < m_gridRows; j++)
			{
				for (int i = 0; i < m_gridCols; i++)
				{
					m_data[(i/m_downsample) + (j/m_downsample)*w] += sqrtf((float)temp[i + j*m_gridCols]);
				}
			}
		}

		// apply negative range
		{
			for (int index = 0; index < m_gridRows*m_gridCols; index++)
			{
				temp[index] = (m_grid[index/8] & BIT(index%8)) ? 0 : 1;
			}

			EDT::MaurerEDT(temp, m_gridCols, m_gridRows);

			for (int j = 0; j < m_gridRows; j++)
			{
				for (int i = 0; i < m_gridCols; i++)
				{
					m_data[(i/m_downsample) + (j/m_downsample)*w] -= sqrtf((float)temp[i + j*m_gridCols]);
				}
			}
		}

		for (int index = 0; index < w*h; index++)
		{
			m_data[index] *= m_cellSize/(float)(m_downsample*m_downsample*m_downsample);
		}

		if (boundaryEdges.GetCount() == 0)
		{
			flags |= fwVehicleGlassWindow::VGW_FLAG_ERROR_NO_BOUNDARY_EDGES;
		}
		else if (blurPasses > 0)
		{
			float* data2 = rage_new float[w*h];
			float dataMin0 = 0.0f;
			float dataMax0 = 0.0f;

			if (blurNormalise)
			{
				dataMax0 = m_data[0];

				for (int index = 1; index < w*h; index++)
				{
					dataMax0 = Max<float>(m_data[index], dataMax0);
				}

				float dmin = 0.0f;
				float dmax = 0.0f;
				float davg = 0.0f;
				SampleAlongBoundary(dmin, dmax, davg, boundaryEdges);

				dataMin0 = dmax;
			}

			for (int pass = 0; pass < blurPasses; pass++)
			{
				for (int j = 0; j < h; j++)
				{
					for (int i = 0; i < w; i++)
					{
						float sum = 0.0f;

						for (int dj = -1; dj <= 1; dj++)
						{
							for (int di = -1; di <= 1; di++)
							{
								const int ii = Clamp<int>(i + di, 0, w - 1);
								const int jj = Clamp<int>(j + dj, 0, h - 1);

								sum += m_data[ii + jj*w];
							}
						}

						data2[i + j*w] = sum/9.0f;
					}
				}

				std::swap(data2, m_data);
			}

			delete[] data2;

			if (blurNormalise)
			{
				float dataMax1 = m_data[0];

				for (int index = 1; index < w*h; index++)
				{
					dataMax1 = Max<float>(m_data[index], dataMax1);
				}

				if (0)
				{
					// ============================================================================================
					// NOTE -- This isn't really a good way to normalise the distance field, a better way would
					// be to remap it so that the lowest distance along the boundary still maps to distance=0, and
					// the highest distance still maps to the pre-blurred highest distance in the centre of the
					// window. This might be difficult to code analytically, but perhaps we could do it by sampling
					// along the boundary 100x per edge or something like that.
					// 
					// Because we're not normalising properly, we can't set the blur passes very high without
					// causing problems for small windows.
					// ============================================================================================
					for (int index = 0; index < w*h; index++)
					{
						m_data[index] *= dataMax0/dataMax1;
					}
				}
				else // sample along boundary edges
				{
					float dmin = 0.0f;
					float dmax = 0.0f;
					float davg = 0.0f;
					SampleAlongBoundary(dmin, dmax, davg, boundaryEdges);

					const float dataMin1 = dmax;

					for (int index = 0; index < w*h; index++) // map dataMin1 -> dataMin0, and dataMax1 -> dataMax0
					{
						m_data[index] = dataMin0 + (dataMax0 - dataMin0)*((m_data[index] - dataMin1)/(dataMax1 - dataMin1));
					}
				}
			}
		}

		delete[] temp;
	}

	void ConstructSpans(float thresholdMin, float thresholdMax)
	{
		int i0 = m_dataW - 1;
		int j0 = m_dataH - 1;
		int i1 = 0;
		int j1 = 0;

		const int dataMaskSize = (m_dataW*m_dataH + 7)/8;
		m_dataMaskGTEMin = rage_new u8[dataMaskSize];
		m_dataMaskLTEMax = rage_new u8[dataMaskSize];
		sysMemSet(m_dataMaskGTEMin, 0, dataMaskSize*sizeof(u8));
		sysMemSet(m_dataMaskLTEMax, 0, dataMaskSize*sizeof(u8));

		for (int j = 0; j < m_dataH; j++)
		{
			for (int i = 0; i < m_dataW; i++)
			{
				float dataMin = +FLT_MAX;
				float dataMax = -FLT_MAX;

				// Find the local min/max values using a 3x3 filter
				for (int dj = -1; dj <= 1; dj++)
				{
					for (int di = -1; di <= 1; di++)
					{
						const int ii = Clamp<int>(i + di, 0, m_dataW - 1);
						const int jj = Clamp<int>(j + dj, 0, m_dataH - 1);

						dataMin = Min<float>(m_data[ii + jj*m_dataW], dataMin);
						dataMax = Max<float>(m_data[ii + jj*m_dataW], dataMax);
					}
				}

				const int index = i + j*m_dataW;

				// Store the filtered results
				// If the bit is set we know the values around this position are higher/lower equal to the threshold
				if (dataMax >= thresholdMin) { m_dataMaskGTEMin[index/8] |= BIT(index%8); }
				if (dataMin <= thresholdMax) { m_dataMaskLTEMax[index/8] |= BIT(index%8); }

				if (m_dataMaskGTEMin[index/8] & m_dataMaskLTEMax[index/8] & BIT(index%8))
				{
					i0 = Min<int>(i, i0);
					j0 = Min<int>(j, j0);
					i1 = Max<int>(i, i1);
					j1 = Max<int>(j, j1);

					m_dataMin = Min<float>(m_data[index], m_dataMin);
					m_dataMax = Max<float>(m_data[index], m_dataMax);
				}
			}
		}

		// clamp the bounds to the actual geometry of the window (this is especially important for windows with exposed edges)
		{
			const int margin = m_gridMargin/m_downsample - 1;

			i0 = Max<int>(margin, i0);
			j0 = Max<int>(margin, j0);
			i1 = Min<int>(m_dataW - margin - 1, i1);
			j1 = Min<int>(m_dataH - margin - 1, j1);
		}

		if (!Verifyf(i1 >= i0 && j1 >= j0, "### %s: '%s' data has no rows or columns, forcing to cover entire window (this is bad!)", m_modelName, m_boneName))
		{
			i0 = 0;
			j0 = 0;
			i1 = m_dataW - 1;
			j1 = m_dataH - 1;
		}

		if (!Verifyf(i1 - i0 + 1 <= VEHICLE_GLASS_MAX_DATA_COLS, "### %s: '%s' data cols = %d, must be [1..%d], clamping ..", m_modelName, m_boneName, i1 - i0 + 1, VEHICLE_GLASS_MAX_DATA_COLS))
		{
			i1 = i0 + VEHICLE_GLASS_MAX_DATA_COLS - 1;
		}

		if (!Verifyf(j1 - j0 + 1 <= VEHICLE_GLASS_MAX_DATA_ROWS, "### %s: '%s' data rows = %d, must be [1..%d], clamping ..", m_modelName, m_boneName, j1 - j0 + 1, VEHICLE_GLASS_MAX_DATA_ROWS))
		{
			j1 = j0 + VEHICLE_GLASS_MAX_DATA_ROWS - 1;
		}

		m_dataColFirst = i0;
		m_dataColLast  = i1;
		m_dataRowFirst = j0;
		m_dataRowLast  = j1;

		m_colA = rage_new int[m_dataH];
		m_colB = rage_new int[m_dataH];
		m_colC = rage_new int[m_dataH];
		m_colD = rage_new int[m_dataH];

		const int mergedSpansSize = (m_dataH + 7)/8;
		m_mergedSpans = rage_new u8[mergedSpansSize];
		sysMemSet(m_mergedSpans, 0, mergedSpansSize*sizeof(u8));

		for (int j = 0; j < m_dataH; j++)
		{
			int colA = -1;
			int colB = -1;
			int colC = -1;
			int colD = -1;

			if (j >= m_dataRowFirst && j <= m_dataRowLast) // compute spans
			{
				int colOuterFirst = -1;
				int colOuterLast = -1;
				int colInnerFirst = -1;
				int colInnerLast = -1;

				for (int i = m_dataColFirst; i <= m_dataColLast; i++)
				{
					if (IsDataGTEMin(i, j))
					{
						colOuterFirst = i;
						break;
					}
				}

				for (int i = m_dataColLast; i >= m_dataColFirst; i--)
				{
					if (IsDataGTEMin(i, j))
					{
						colOuterLast = i;
						break;
					}
				}

				if (colOuterFirst != -1 &&
					colOuterLast != -1)
				{
					colInnerFirst = colOuterFirst;
					colInnerLast = colOuterLast;

					while (colInnerFirst < colOuterLast && IsDataLTEMax(colInnerFirst, j))
					{
						colInnerFirst++;
					}

					while (colInnerLast > colOuterFirst && IsDataLTEMax(colInnerLast, j))
					{
						colInnerLast--;
					}

					if (colInnerFirst <= colInnerLast)
					{
						colInnerFirst = Max<int>(m_dataColFirst, colInnerFirst - 1);
						colInnerLast = Min<int>(m_dataColLast, colInnerLast + 1);

						if (colInnerFirst + 1 < colInnerLast - 1) // there still might be a span of data between these ..
						{
							int colCentreFirst = colInnerFirst + 1;
							int colCentreLast = colInnerLast - 1;

							while (colCentreFirst < colInnerLast && !IsDataLTEMax(colCentreFirst, j))
							{
								colCentreFirst++;
							}

							while (colCentreLast > colInnerFirst && !IsDataLTEMax(colCentreLast, j))
							{
								colCentreLast--;
							}

							if (colCentreFirst <= colCentreLast) // merge these two spans in the most optimal way
							{
								if ((colCentreFirst - colInnerFirst) < (colInnerLast - colCentreLast))
								{
									colInnerFirst = colCentreLast;
								}
								else
								{
									colInnerLast = colCentreFirst;
								}

								m_mergedSpans[j/8] |= BIT(j%8);
								m_mergedSpanCount++;
							}
						}

						colA = colOuterFirst;
						colB = colInnerFirst;
						colC = colInnerLast;
						colD = colOuterLast;
					}
					else
					{
						colA = colOuterFirst;
						colB = colOuterLast;
					}
				}
			}

			m_colA[j] = colA;
			m_colB[j] = colB;
			m_colC[j] = colC;
			m_colD[j] = colD;
		}
	}

	void ConstructWindow(int componentId, int boneIndex, int geomIndex, u32 flags)
	{
		const int offset = 1 + m_dataMargin*2;
		const Vec3V dataScale((float)(m_dataW - offset), (float)(m_dataH - offset), 1.0f);
		const Vec3V dataOffset(0.5f*(float)offset, 0.5f*(float)offset, 0.0f);

		Mat34V basis = m_basis;
		basis.GetCol0Ref() *= dataScale;
		basis.GetCol1Ref() *= dataScale;
		basis.GetCol2Ref() *= dataScale;
		basis.GetCol3Ref() *= dataScale;
		basis.GetCol3Ref() += dataOffset;

		// uncompressed proxy
		{
			m_windowProxy = rage_new fwVehicleGlassWindowProxy;

			m_windowProxy->m_basis       = basis;
			m_windowProxy->m_componentId = componentId;
			m_windowProxy->m_boneIndex   = boneIndex;
			m_windowProxy->m_dataW       = m_dataW;
			m_windowProxy->m_dataH       = m_dataH;

			if (m_dataW*m_dataH > 0)
			{
				m_windowProxy->m_data = rage_new float[m_dataW*m_dataH];

				sysMemCpy(m_windowProxy->m_data, m_data, m_dataW*m_dataH*sizeof(float));
			}
		}

		// RLE compressed and quantised
		{
			// RLE compressed data is stored relative to first row/column
			basis.GetCol3Ref() -= Vec3V((float)m_dataColFirst, (float)m_dataRowFirst, 0.0f);

			m_windowProxy->m_window = rage_new fwVehicleGlassWindow;

			m_windowProxy->m_window->m_basis        = basis;
			m_windowProxy->m_window->m_componentId  = (u16)componentId;
			m_windowProxy->m_window->m_geomIndex    = (u16)geomIndex;
			m_windowProxy->m_window->m_dataCols     = (u16)(m_dataColLast - m_dataColFirst + 1);
			m_windowProxy->m_window->m_dataRows     = (u16)(m_dataRowLast - m_dataRowFirst + 1);
			m_windowProxy->m_window->m_dataMin      = m_dataMin;
			m_windowProxy->m_window->m_dataMax      = m_dataMax;
			m_windowProxy->m_window->m_flags       |= flags;
			m_windowProxy->m_window->m_textureScale = m_textureScaleAvg;

			if (m_dataW*m_dataH > 0)
			{
				Quantise<u8>(m_windowProxy->m_window->m_dataRLE, m_windowProxy->m_window->m_dataRLESize, true);
			}
		}
	}

	template <typename T> void Quantise(u8*& dataRLE, u32& dataRLESize, bool bUseTwoSpans) const
	{
		float dataMin = +FLT_MAX;
		float dataMax = -FLT_MAX;

		for (int j = m_dataRowFirst; j <= m_dataRowLast; j++)
		{
			for (int i = m_dataColFirst; i <= m_dataColLast; i++)
			{
				if (IsDataGTEMin(i, j) &&
					IsDataLTEMax(i, j))
				{
					dataMin = Min<float>(m_data[i + j*m_dataW], dataMin);
					dataMax = Max<float>(m_data[i + j*m_dataW], dataMax);

					if (bUseTwoSpans)
					{
						Assert((i >= m_colA[j] && i <= m_colB[j]) || (i >= m_colC[j] && i <= m_colD[j]));
					}
					else
					{
						Assert((i >= m_colA[j] && i <= Max<int>(m_colB[j], m_colD[j])));
					}
				}
			}
		}

		u16* rowOffsets = rage_new u16[m_dataRowLast - m_dataRowFirst + 1];
		atArray<u8> cellData;

		for (int j = m_dataRowFirst; j <= m_dataRowLast; j++)
		{
			rowOffsets[j - m_dataRowFirst] = (u16)cellData.GetCount();

			if (m_colA[j] != -1)
			{
				Assert(m_colA[j] - m_dataColFirst <= 255);
				Assert(m_colB[j] - m_dataColFirst <= 255);
				Assert(m_colB[j] >= m_colA[j]);
				Assert(m_colB[j] <= m_dataColLast);

				if (bUseTwoSpans)
				{
					AppendBytes<u8>(cellData, (u8)(m_colA[j] - m_dataColFirst));
					AppendBytes<u8>(cellData, (u8)(m_colB[j] - m_dataColFirst));

					for (int i = m_colA[j]; i <= m_colB[j]; i++)
					{
						AppendBytes<T>(cellData, fwVehicleGlassUtil::QuantiseData<T>(m_data[i + j*m_dataW], dataMin, dataMax));
					}

					if (m_colC[j] != -1)
					{
						Assert(m_colC[j] - m_dataColFirst <= 255);
						Assert(m_colD[j] - m_dataColFirst <= 255);
						Assert(m_colC[j] >  m_colB[j]);
						Assert(m_colD[j] >= m_colC[j]);
						Assert(m_colD[j] <= m_dataColLast);

						AppendBytes<u8>(cellData, (u8)(m_colC[j] - m_dataColFirst));
						AppendBytes<u8>(cellData, (u8)(m_colD[j] - m_dataColFirst));

						for (int i = m_colC[j]; i <= m_colD[j]; i++)
						{
							AppendBytes<T>(cellData, fwVehicleGlassUtil::QuantiseData<T>(m_data[i + j*m_dataW], dataMin, dataMax));
						}
					}
					else
					{
						Assert(m_colC[j] == -1);
						Assert(m_colD[j] == -1);

						AppendBytes<u8>(cellData, 255);
					}
				}
				else
				{
					const int colB = Max<int>(m_colB[j], m_colD[j]);

					AppendBytes<u8>(cellData, (u8)(m_colA[j] - m_dataColFirst));
					AppendBytes<u8>(cellData, (u8)(colB - m_dataColFirst));

					for (int i = m_colA[j]; i <= colB; i++)
					{
						AppendBytes<T>(cellData, fwVehicleGlassUtil::QuantiseData<T>(m_data[i + j*m_dataW], dataMin, dataMax));
					}
				}
			}
			else
			{
				Assert(m_colA[j] == -1);
				Assert(m_colB[j] == -1);
				Assert(m_colC[j] == -1);
				Assert(m_colD[j] == -1);

				AppendBytes<u8>(cellData, 255);
			}
		}

		const int rowOffsetsSize = (m_dataRowLast - m_dataRowFirst + 1)*sizeof(u16);

		dataRLESize = rowOffsetsSize + cellData.GetCount()*sizeof(u8);
		dataRLE = rage_new u8[dataRLESize];

		sysMemCpy(dataRLE, rowOffsets, rowOffsetsSize);
		sysMemCpy(dataRLE + rowOffsetsSize, &cellData[0], cellData.GetCount()*sizeof(u8));

		delete[] rowOffsets;

#if 0 && __ASSERT // TODO -- this check does not work, fix it
		for (int j = 0; j < m_dataH; j++)
		{
			for (int i = 0; i < m_dataW; i++)
			{
				const float RLE = GetData(i, j, true, false);

				if (!IsDataGTEMin(i, j))
				{
					Assert(RLE == m_dataMin);
				}
				else if (!IsDataLTEMax(i, j))
				{
					Assert(RLE == m_dataMax);
				}
				else
				{
					Assert(RLE == GetData(i, j, false, true));
				}
			}
		}
#endif // __ASSERT
	}

#if VEHICLE_GLASS_DEV
	float GetData(int i, int j, bool bUseRLEData, bool bQuantise) const
	{
		if (bUseRLEData && m_windowProxy->m_window)
		{
			m_windowProxy->m_window->GetDataValue(i, j);
		}
		else if (m_data)
		{
			const float raw = m_data[i + j*m_dataW];
			
			if (bQuantise)
			{
				const u8 quant = fwVehicleGlassUtil::QuantiseData<u8>(raw, m_dataMin, m_dataMax);
				return fwVehicleGlassUtil::UnquantiseData<u8>(quant, m_dataMin, m_dataMax);
			}
			else
			{
				return raw;
			}
		}

		return 0.0f;
	}

	void DrawEPSData(fiStream* eps) const
	{
		const float scaleX = 512.0f;
		const float scaleY = 512.0f*(float)m_gridRows/(float)m_gridCols;
		const float offset = 40.0f;

		if (m_data)
		{
			fprintf(eps, "0 setlinewidth\n");

			float valueMin = +FLT_MAX;
			float valueMax = -FLT_MAX;

			for (int index = 0; index < m_dataW*m_dataH; index++)
			{
				valueMin = Min<float>(m_data[index], valueMin);
				valueMax = Max<float>(m_data[index], valueMax);
			}

			if (valueMin < valueMax)
			{
				for (int j = 0; j < m_dataH; j++)
				{
					for (int i = 0; i < m_dataW; i++)
					{
						const float value = (m_data[i + j*m_dataW] - valueMin)/(valueMax - valueMin); // [0..1]

						const float cell_x0 = ((float)i - 0.005f)/(float)m_dataW;
						const float cell_y0 = ((float)j - 0.005f)/(float)m_dataH;
						const float cell_x1 = ((float)i + 1.005f)/(float)m_dataW;
						const float cell_y1 = ((float)j + 1.005f)/(float)m_dataH;

						fprintf(
							eps,
							"%f %f %f setrgbcolor %f %f moveto %f %f lineto %f %f lineto %f %f lineto %f %f lineto fill\n",
							value,
							value,
							value,
							offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y0),
							offset + scaleX*cell_x1, offset + scaleY*(1.0f - cell_y0),
							offset + scaleX*cell_x1, offset + scaleY*(1.0f - cell_y1),
							offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y1),
							offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y0)
						);

						if (m_colA) // has RLE data?
						{
							if ((i >= m_colA[j] && i <= m_colB[j]) ||
								(i >= m_colC[j] && i <= m_colD[j]))
							{
								// valid data
							}
							else
							{
								const float mark_x0 = ((float)i + 0.4f)/(float)m_dataW;
								const float mark_y0 = ((float)j + 0.4f)/(float)m_dataH;
								const float mark_x1 = ((float)i + 0.6f)/(float)m_dataW;
								const float mark_y1 = ((float)j + 0.6f)/(float)m_dataH;

								fprintf(
									eps,
									"0 0 0 setrgbcolor %f %f moveto %f %f lineto %f %f moveto %f %f lineto stroke\n",
									offset + scaleX*mark_x0, offset + scaleY*(1.0f - mark_y0),
									offset + scaleX*mark_x1, offset + scaleY*(1.0f - mark_y1),
									offset + scaleX*mark_x1, offset + scaleY*(1.0f - mark_y0),
									offset + scaleX*mark_x0, offset + scaleY*(1.0f - mark_y1)
								);
							}
						}
					}
				}
			}
		}
	}

	void DrawEPSContours(fiStream* eps, const float* contours, int contourCount, bool bUseRLEData, bool bQuantise, bool bLines, Color32 colour = Color32(0,0,0,255), float linewidth = 0.0f) const
	{
		const float scaleX = 512.0f;
		const float scaleY = 512.0f*(float)m_gridRows/(float)m_gridCols;
	//	const float offset = 40.0f;

		if (m_data)
		{
			for (int contourIndex = 0; contourIndex < contourCount; contourIndex++)
			{
				const float thresh = contours[contourIndex];

				fprintf(eps, "%f %f %f setcolor %f setlinewidth\n", colour.GetRedf(), colour.GetGreenf(), colour.GetBluef(), linewidth);

				for (int j = 0; j < m_dataH - 1; j++)
				{
					for (int i = 0; i < m_dataW - 1; i++)
					{
						const float cell_x0 = ((float)i + 0.5f)/(float)m_dataW;
						const float cell_y0 = ((float)j + 0.5f)/(float)m_dataH;
						const float cell_x1 = ((float)i + 1.5f)/(float)m_dataW;
						const float cell_y1 = ((float)j + 1.5f)/(float)m_dataH;

						const float v00 = GetData(i + 0, j + 0, bUseRLEData, bQuantise);
						const float v10 = GetData(i + 1, j + 0, bUseRLEData, bQuantise);
						const float v01 = GetData(i + 0, j + 1, bUseRLEData, bQuantise);
						const float v11 = GetData(i + 1, j + 1, bUseRLEData, bQuantise);

						const float vmin = Min<float>(v00, v10, v01, v11);
						const float vmax = Max<float>(v00, v10, v01, v11);

						if (vmin <= thresh &&
							vmax >= thresh)
						{
							class DrawContour { public: static void func(fiStream* eps, float scaleX, float scaleY, float thresh, float v00, float v10, float v01, float v11, float cell_x0, float cell_y0, float cell_x1, float cell_y1, float x0, float y0, float x1, float y1, int depth, bool bLines)
							{
								const float z00 = v00*(1.0f - x0)*(1.0f - y0) + v10*x0*(1.0f - y0) + v01*(1.0f - x0)*y0 + v11*x0*y0;
								const float z10 = v00*(1.0f - x1)*(1.0f - y0) + v10*x1*(1.0f - y0) + v01*(1.0f - x1)*y0 + v11*x1*y0;
								const float z01 = v00*(1.0f - x0)*(1.0f - y1) + v10*x0*(1.0f - y1) + v01*(1.0f - x0)*y1 + v11*x0*y1;
								const float z11 = v00*(1.0f - x1)*(1.0f - y1) + v10*x1*(1.0f - y1) + v01*(1.0f - x1)*y1 + v11*x1*y1;

								const float zmin = Min<float>(z00, z10, z01, z11);
								const float zmax = Max<float>(z00, z10, z01, z11);

								if (zmin <= thresh &&
									zmax >= thresh)
								{
									const float xm = (x1 + x0)/2.0f;
									const float ym = (y1 + y0)/2.0f;

									if (depth <= 0)
									{
										const float offset = 40.0f;

										// TODO -- test this codepath ..
										// if this doesn't work, we could try evaluating the contour segment through a
										// unit disc around this cell and then clipping the segment to the cell bounds
										// maxima code:
										// top : z00 + u*(z10 - z00);
										// bot : z01 + u*(z11 - z01);
										// solve([top + v*(bot - top) = thresh, (u*2 - 1)^2 + (v*2 - 1)^2 = 1], [u,v]);

										if (bLines)
										{
											const float u0 = (thresh - z00)/(z10 - z00); // x-coord @ y=0
											const float v0 = (thresh - z00)/(z01 - z00); // y-coord @ x=0
											const float u1 = (thresh - z01)/(z11 - z01); // x-coord @ y=1
											const float v1 = (thresh - z10)/(z11 - z10); // y-coord @ x=1

											float coords[4];
											int n = 0;

											if (Abs<float>(z10 - z00) > 0.00001f && u0 >= 0.0f && u0 <= 1.0f && n < NELEM(coords))
											{
												coords[n++] = u0;
												coords[n++] = 0.0f;
											}

											if (Abs<float>(z01 - z00) > 0.00001f && v0 >= 0.0f && v0 <= 1.0f && n < NELEM(coords))
											{
												coords[n++] = 0.0f;
												coords[n++] = v0;
											}

											if (Abs<float>(z11 - z01) > 0.00001f && u1 >= 0.0f && u1 <= 1.0f && n < NELEM(coords))
											{
												coords[n++] = u1;
												coords[n++] = 1.0f;
											}

											if (Abs<float>(z11 - z10) > 0.00001f && v1 >= 0.0f && v1 <= 1.0f && n < NELEM(coords))
											{
												coords[n++] = 1.0f;
												coords[n++] = v1;
											}

											if (n == 4)
											{
												coords[0] = x0 + (x1 - x0)*coords[0];
												coords[1] = y0 + (y1 - y0)*coords[1];
												coords[2] = x0 + (x1 - x0)*coords[2];
												coords[3] = y0 + (y1 - y0)*coords[3];

												fprintf(
													eps,
													"%f %f moveto %f %f lineto stroke\n",
													offset + scaleX*(cell_x0 + coords[0]*(cell_x1 - cell_x0)), offset + scaleY*(1.0f - (cell_y0 + coords[1]*(cell_y1 - cell_y0))),
													offset + scaleX*(cell_x0 + coords[2]*(cell_x1 - cell_x0)), offset + scaleY*(1.0f - (cell_y0 + coords[3]*(cell_y1 - cell_y0)))
												);
												
											}
										}
										else
										{
											x0 += (xm - x0)*0.1f;
											y0 += (ym - y0)*0.1f;
											x1 += (xm - x1)*0.1f;
											y1 += (ym - y1)*0.1f;

											fprintf(
												eps,
												"%f %f moveto %f %f lineto %f %f moveto %f %f lineto stroke\n",
												offset + scaleX*(cell_x0 + x0*(cell_x1 - cell_x0)), offset + scaleY*(1.0f - (cell_y0 + y0*(cell_y1 - cell_y0))),
												offset + scaleX*(cell_x0 + x1*(cell_x1 - cell_x0)), offset + scaleY*(1.0f - (cell_y0 + y1*(cell_y1 - cell_y0))),
												offset + scaleX*(cell_x0 + x0*(cell_x1 - cell_x0)), offset + scaleY*(1.0f - (cell_y0 + y1*(cell_y1 - cell_y0))),
												offset + scaleX*(cell_x0 + x1*(cell_x1 - cell_x0)), offset + scaleY*(1.0f - (cell_y0 + y0*(cell_y1 - cell_y0)))
											);
										}
									}
									else
									{
										func(eps, scaleX, scaleY, thresh, v00, v10, v01, v11, cell_x0, cell_y0, cell_x1, cell_y1, x0, y0, xm, ym, depth - 1, bLines);
										func(eps, scaleX, scaleY, thresh, v00, v10, v01, v11, cell_x0, cell_y0, cell_x1, cell_y1, xm, y0, x1, ym, depth - 1, bLines);
										func(eps, scaleX, scaleY, thresh, v00, v10, v01, v11, cell_x0, cell_y0, cell_x1, cell_y1, x0, ym, xm, y1, depth - 1, bLines);
										func(eps, scaleX, scaleY, thresh, v00, v10, v01, v11, cell_x0, cell_y0, cell_x1, cell_y1, xm, ym, x1, y1, depth - 1, bLines);
									}
								}
							}};

							DrawContour::func(eps, scaleX, scaleY, thresh, v00, v10, v01, v11, cell_x0, cell_y0, cell_x1, cell_y1, 0.0f, 0.0f, 1.0f, 1.0f, 6, bLines);
						}
					}
				}
			}
		}
	}

	void DrawEPSGrid(fiStream* eps, Color32 colour = Color32(128,128,128,255), float linewidth = 0.0f) const
	{
		const float scaleX = 512.0f;
		const float scaleY = 512.0f*(float)m_gridRows/(float)m_gridCols;
		const float offset = 40.0f;

		if (m_grid)
		{
			fprintf(eps, "%f %f %f setcolor %f setlinewidth\n", colour.GetRedf(), colour.GetGreenf(), colour.GetBluef(), linewidth);

			for (int j = 0; j < m_gridRows; j++)
			{
				for (int i = 0; i < m_gridCols; i++)
				{
					const int index = i + j*m_gridCols;

					if (m_grid[index/8] & BIT(index%8))
					{
						const float cell_x0 = (float)(i + 0)/(float)m_gridCols;
						const float cell_y0 = (float)(j + 0)/(float)m_gridRows;
						const float cell_x1 = (float)(i + 1)/(float)m_gridCols;
						const float cell_y1 = (float)(j + 1)/(float)m_gridRows;

						fprintf(
							eps,
							"%f %f moveto %f %f lineto %f %f lineto %f %f lineto %f %f lineto stroke\n",
							offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y0),
							offset + scaleX*cell_x1, offset + scaleY*(1.0f - cell_y0),
							offset + scaleX*cell_x1, offset + scaleY*(1.0f - cell_y1),
							offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y1),
							offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y0)
						);
					}
				}
			}
		}
	}

	void DrawEPSTriangles(fiStream* eps, const atArray<fwVGWTriangle>& triangles, const fwVGWTriangleRange& range, Color32 colour = Color32(0,0,0,255), float linewidth = 0.5f) const
	{
		const float scaleX = 512.0f;
		const float scaleY = 512.0f*(float)m_gridRows/(float)m_gridCols;
		const float offset = 40.0f;

		if (range.m_glassFirst != -1)
		{
			fprintf(eps, "%f %f %f setcolor %f setlinewidth\n", colour.GetRedf(), colour.GetGreenf(), colour.GetBluef(), linewidth);

			for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
			{
				const Vec2V p0 = TransformToGrid(triangles[i].m_p[0])*Vec2V(1.0f/(float)m_gridCols, 1.0f/(float)m_gridRows);
				const Vec2V p1 = TransformToGrid(triangles[i].m_p[1])*Vec2V(1.0f/(float)m_gridCols, 1.0f/(float)m_gridRows);
				const Vec2V p2 = TransformToGrid(triangles[i].m_p[2])*Vec2V(1.0f/(float)m_gridCols, 1.0f/(float)m_gridRows);

				fprintf(
					eps,
					"%f %f moveto %f %f lineto stroke\n",
					offset + scaleX*p0.GetXf(), offset + scaleY*(1.0f - p0.GetYf()),
					offset + scaleX*p1.GetXf(), offset + scaleY*(1.0f - p1.GetYf())
				);
				fprintf(
					eps,
					"%f %f moveto %f %f lineto stroke\n",
					offset + scaleX*p1.GetXf(), offset + scaleY*(1.0f - p1.GetYf()),
					offset + scaleX*p2.GetXf(), offset + scaleY*(1.0f - p2.GetYf())
				);
				fprintf(
					eps,
					"%f %f moveto %f %f lineto stroke\n",
					offset + scaleX*p2.GetXf(), offset + scaleY*(1.0f - p2.GetYf()),
					offset + scaleX*p0.GetXf(), offset + scaleY*(1.0f - p0.GetYf())
				);

				for (int j = 0; j < 3; j++)
				{
					if (triangles[i].m_exposed[j])
					{
						const Vec2V p = TransformToGrid(triangles[i].m_p[j])*Vec2V(1.0f/(float)m_gridCols, 1.0f/(float)m_gridRows);
						const float x0 = p.GetXf() - 0.5f/(float)m_gridCols;
						const float y0 = p.GetYf() - 0.5f/(float)m_gridRows;
						const float x1 = p.GetXf() + 0.5f/(float)m_gridCols;
						const float y1 = p.GetYf() + 0.5f/(float)m_gridRows;

						fprintf(
							eps,
							"%f %f moveto %f %f lineto %f %f lineto %f %f lineto %f %f lineto fill\n",
							offset + scaleX*x0, offset + scaleY*(1.0f - y0),
							offset + scaleX*x1, offset + scaleY*(1.0f - y0),
							offset + scaleX*x1, offset + scaleY*(1.0f - y1),
							offset + scaleX*x0, offset + scaleY*(1.0f - y1),
							offset + scaleX*x0, offset + scaleY*(1.0f - y0)
						);
					}
				}
			}
		}

		// border
		{
			const float cell_x0 = 0.0f;
			const float cell_y0 = 0.0f;
			const float cell_x1 = 1.0f;
			const float cell_y1 = 1.0f;

			fprintf(eps, "0 0 0 setcolor 0 setlinewidth\n");
			fprintf(
				eps,
				"%f %f moveto %f %f lineto %f %f lineto %f %f lineto %f %f lineto stroke\n",
				offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y0),
				offset + scaleX*cell_x1, offset + scaleY*(1.0f - cell_y0),
				offset + scaleX*cell_x1, offset + scaleY*(1.0f - cell_y1),
				offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y1),
				offset + scaleX*cell_x0, offset + scaleY*(1.0f - cell_y0)
			);
		}
	}

	void Draw(const atArray<fwVGWTriangle>& triangles, const fwVGWTriangleRange& range, int framesToLive = 30) const
	{
		const Vec2V windowScale = m_extent.GetXY()/m_extent.GetX();
		const int screenW = GRCDEVICE.GetWidth();
		const int screenH = GRCDEVICE.GetHeight();
		const float inset = 0.125f;

		for (int pass = 0; pass < 2; pass++)
		{
			for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
			{
				Vec2V points[3];

				for (int j = 0; j < NELEM(points); j++)
				{
					points[j] = TransformToBasisXY(triangles[i].m_p[j])*windowScale;
					points[j] *= Vec2V((float)screenH/(float)screenW, 1.0f);

					if (inset != 0.0f)
					{
						points[j] -= Vec2V(V_HALF);
						points[j] *= ScalarV(1.0f - inset);
						points[j] += Vec2V(V_HALF);
					}
				}

				if (pass == 0)
				{
					grcDebugDraw::Poly(points[0], points[1], points[2], Color32(255,0,0,20), true, framesToLive);
				}
				else if (pass == 1)
				{
					for (int j = 0; j < NELEM(points); j++)
					{
						grcDebugDraw::Line(points[j], points[(j + 1)%NELEM(points)], Color32(255,255,0,255), framesToLive);
					}
				}
			}
		}

		for (int pass = 1; pass < 2; pass++) // border
		{
			Vec2V points[4];

			points[0] = windowScale*Vec2V(V_ZERO);
			points[1] = windowScale*Vec2V(V_X_AXIS_WZERO);
			points[2] = windowScale*Vec2V(V_ONE);
			points[3] = windowScale*Vec2V(V_Y_AXIS_WZERO);

			for (int j = 0; j < NELEM(points); j++)
			{
				points[j] *= Vec2V((float)screenH/(float)screenW, 1.0f);

				if (inset != 0.0f)
				{
					points[j] -= Vec2V(V_HALF);
					points[j] *= ScalarV(1.0f - inset);
					points[j] += Vec2V(V_HALF);
				}
			}

			if (pass == 1)
			{
				for (int j = 0; j < NELEM(points); j++)
				{
					grcDebugDraw::Line(points[j], points[(j + 1)%NELEM(points)], Color32(255,255,255,255), framesToLive);
				}
			}
		}
	}
#endif // VEHICLE_GLASS_DEV

	const char* m_modelName; // helps with debugging
	const char* m_boneName;

	Vec2V  m_diffuse;
	Vec2V  m_diffuseMin;
	Vec2V  m_diffuseMax;

	float  m_textureScale;
	float  m_textureScaleMin;
	float  m_textureScaleMax;
	float  m_textureScaleAvg;
	float  m_textureScaleStd; // standard deviation

	Mat34V m_basis;
	Vec3V  m_extent;
	int    m_numGlassTris;
	int    m_uniquePoints;
	int    m_numExposedEdges;
	int    m_numNonExposedEdges;
	float  m_cellSize;
	int    m_downsample;
	int    m_gridMargin;
	int    m_gridCols;
	int    m_gridRows;
	u8*    m_grid;
	int    m_dataMargin;
	int    m_dataW;
	int    m_dataH;
	float* m_data;
	float  m_dataMin;
	float  m_dataMax;
	u8*    m_dataMaskGTEMin; // data cells for which 3x3 group is all >= min threshold
	u8*    m_dataMaskLTEMax; // data cells for which 3x3 group is all <= max threshold
	int    m_dataColFirst;
	int    m_dataColLast;
	int    m_dataRowFirst;
	int    m_dataRowLast;

	// RLE data
	int*   m_colA;
	int*   m_colB;
	int*   m_colC;
	int*   m_colD;
	u8*    m_mergedSpans;
	int    m_mergedSpanCount;

	fwVehicleGlassWindowProxy* m_windowProxy;
};

class fwVGWControl
{
public:
	fwVGWControl()
	{
		strcpy(m_outputDir, RS_ASSETS "/non_final/vgwindows");
		strcpy(m_outputExt, "");

		m_outputTXTFile         = true;
		m_outputOBJFile         = true;
		m_outputEPSFile         = true;
		m_outputEPSContours     = false;
		m_outputEPSContourMin   = 0.0f;
		m_outputEPSContourStep  = 0.05f;
		m_outputEPSContourCount = 4;
		m_outputEPSContourLines = true;
		m_outputDataMask        = false;
		m_skipBulletproofMtl    = true;
		m_skipSirenBones        = true;
		m_fitUniquePoints       = true;
		m_exposedEdgesEnabled   = true;
		m_cellSize              = 0.025f; // 2.5cm
		m_extraMargin           = 1;
		m_downsample            = 5;
		m_blurPasses            = 20;
		m_blurPassesExposed     = 1; // much less blurring for windows with exposed edges
		m_blurNormalise         = true;
		m_thresholdMin          = -0.025f; // -2.5cm
		m_thresholdMax          = +0.050f; // 5cm
		m_sumDataSize[0]        = 0;
		m_sumDataSize[1]        = 0;
		m_sumDataSize[2]        = 0;
		m_maxDataSize[0]        = 0;
		m_maxDataSize[1]        = 0;
		m_maxDataSize[2]        = 0;
	}

#if VEHICLE_GLASS_DEV
	void AddWidgets(bkBank& bk)
	{
		bk.AddText("Output Dir", &m_outputDir[0], sizeof(m_outputDir), false);
		bk.AddText("Output Ext", &m_outputExt[0], sizeof(m_outputExt), false);
		bk.AddToggle("Output TXT File", &m_outputTXTFile);
		bk.AddToggle("Output OBJ File", &m_outputOBJFile);
		bk.AddToggle("Output EPS File (slow)", &m_outputEPSFile);
		bk.AddToggle("Output EPS Contours", &m_outputEPSContours);
		bk.AddSlider("Output EPS Contour Min", &m_outputEPSContourMin, -1.0f, 1.0f, 0.001f);
		bk.AddSlider("Output EPS Contour Step", &m_outputEPSContourStep, 0.0f, 1.0f, 0.001f);
		bk.AddSlider("Output EPS Contour Count", &m_outputEPSContourCount, 1, 16, 1);
		bk.AddToggle("Output EPS Contour Lines", &m_outputEPSContourLines);
		bk.AddToggle("Output Data Mask", &m_outputDataMask);
		bk.AddToggle("Skip Bulletproof Material", &m_skipBulletproofMtl);
		bk.AddToggle("Skip Siren Bones", &m_skipSirenBones);
		bk.AddToggle("Fit Unique Points", &m_fitUniquePoints);
		bk.AddToggle("Exposed Edges Enabled", &m_exposedEdgesEnabled);
		bk.AddSlider("Cell Size", &m_cellSize, 0.01f, 1.0f, 0.001f);
		bk.AddSlider("Extra Margin", &m_extraMargin, -5, 5, 1);
		bk.AddSlider("Downsample", &m_downsample, 1, 8, 1);
		bk.AddSlider("Blur Passes", &m_blurPasses, 0, 100, 1);
		bk.AddSlider("Blur Passes Exposed", &m_blurPassesExposed, 0, 100, 1);
		bk.AddToggle("Blur Normalise", &m_blurNormalise);
		bk.AddSlider("Threshold Min", &m_thresholdMin, -1.0f, 1.0f, 0.001f);
		bk.AddSlider("Threshold Max", &m_thresholdMax, -1.0f, 1.0f, 0.001f);

		bk.AddSeparator();

		bk.AddSlider("Sum Data Size (uncompressed)", &m_sumDataSize[0], 0, 10*1024*1024, 0);
		bk.AddSlider("Sum Data Size (RLE-1)", &m_sumDataSize[1], 0, 10*1024*1024, 0);
		bk.AddSlider("Sum Data Size (RLE-2)", &m_sumDataSize[2], 0, 10*1024*1024, 0);
		bk.AddSlider("Max Data Size (uncompressed)", &m_maxDataSize[0], 0, 100*1024, 0);
		bk.AddSlider("Max Data Size (RLE-1)", &m_maxDataSize[1], 0, 100*1024, 0);
		bk.AddSlider("Max Data Size (RLE-2)", &m_maxDataSize[2], 0, 100*1024, 0);
	}
#endif // VEHICLE_GLASS_DEV

	char  m_outputDir[RAGE_MAX_PATH];
	char  m_outputExt[64];
	bool  m_outputTXTFile; // text file contains information about all the glass components
	bool  m_outputOBJFile; // obj file contains glass geometry (can be viewed in MeshLab etc.)
	bool  m_outputEPSFile; // eps file contains postscript rendering of each component (rather large)
	bool  m_outputEPSContours;
	float m_outputEPSContourMin;
	float m_outputEPSContourStep;
	int   m_outputEPSContourCount;
	bool  m_outputEPSContourLines;
	bool  m_outputDataMask;
	bool  m_skipBulletproofMtl; // don't create distance fields for bulletproof materials (but do create a window)
	bool  m_skipSirenBones; // don't create distance fields for siren bones (but do create a window)
	bool  m_fitUniquePoints;
	bool  m_exposedEdgesEnabled;
	float m_cellSize;
	int   m_extraMargin;
	int   m_downsample;
	int   m_blurPasses;
	int   m_blurPassesExposed; // blur passes if there are exposed edges
	bool  m_blurNormalise;
	float m_thresholdMin; // affects RLE compression
	float m_thresholdMax;
	int   m_sumDataSize[3];
	int   m_maxDataSize[3];
};

class fwVehicleGlassConstructor
{
public:
	static void SetDebugOutputDirectory(const char* dir)
	{
		strcpy(s_control.m_outputDir, dir);
	}

	static void SetCarGlassMaterialFlag(int materialIndex, const char* name)
	{
		if (AssertVerify(materialIndex >= 0 && materialIndex < 256))
		{
			s_carGlassMaterialFlags[materialIndex/8] |= BIT(materialIndex%8);
			s_carGlassMaterialFlagCount++;

			if (stristr(name, "BULLETPROOF"))
			{
				Assertf(s_carGlassMaterialBulletproofIndex == -1, "bulletproof material index %d being set again to %d (%s)", s_carGlassMaterialBulletproofIndex, materialIndex, name);
				s_carGlassMaterialBulletproofIndex = materialIndex;
			}
		}
	}

	static int GetCarGlassMaterialFlagCount()
	{
		return s_carGlassMaterialFlagCount;
	}

#if __PS3
	static void SetEdgeBuffers(int bufferSizeVerts, void* bufferDataVerts, int bufferSizeIndices, void* bufferDataIndices)
	{
		s_bufferSizeV = bufferSizeVerts;
		s_bufferDataV = bufferDataVerts;
		s_bufferSizeI = bufferSizeIndices;
		s_bufferDataI = bufferDataIndices;
	}
#endif // __PS3

	static void ConstructWindows(
		atArray<fwVehicleGlassWindowProxy*>* proxies,
		int componentToConstruct,
		const char* archetypeName,
		const fragType* pFragType,
		u32 lodLevel,
		const fwVehicleGlassConstructorTriangle* glassTris,
		int numGlassTris,
		bool bOutputDebugFiles,
		bool bIsFromHD,
		bool bAllowExposedEdges,
		u32 externalFlags);

	static int ExtractTriangles(atArray<fwVGWTriangle>& triangles, const char* archetypeName, const fragType* pFragType, grmGeometry& geom, int componentId, int boneIndex, bool bAllowExposedEdges);

	static fwVGWControl s_control;
	static u8           s_carGlassMaterialFlags[32]; // 256 bits
	static int          s_carGlassMaterialFlagCount;
	static int          s_carGlassMaterialBulletproofIndex;
#if __PS3
	static int          s_bufferSizeV;
	static void*        s_bufferDataV;
	static int          s_bufferSizeI;
	static void*        s_bufferDataI;
#endif // __PS3
};

__COMMENT(static) fwVGWControl fwVehicleGlassConstructor::s_control;
__COMMENT(static) u8           fwVehicleGlassConstructor::s_carGlassMaterialFlags[32] = {0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0};
__COMMENT(static) int          fwVehicleGlassConstructor::s_carGlassMaterialFlagCount = 0;
__COMMENT(static) int          fwVehicleGlassConstructor::s_carGlassMaterialBulletproofIndex = -1;
#if __PS3
__COMMENT(static) int          fwVehicleGlassConstructor::s_bufferSizeV = 0;
__COMMENT(static) void*        fwVehicleGlassConstructor::s_bufferDataV = NULL;
__COMMENT(static) int          fwVehicleGlassConstructor::s_bufferSizeI = 0;
__COMMENT(static) void*        fwVehicleGlassConstructor::s_bufferDataI = NULL;
#endif // __PS3

__COMMENT(static) void fwVehicleGlassConstructorInterface::SetDebugOutputDirectory(const char* dir)
{
	fwVehicleGlassConstructor::SetDebugOutputDirectory(dir);
}

__COMMENT(static) void fwVehicleGlassConstructorInterface::SetCarGlassMaterialFlag(int materialIndex, const char* name)
{
	fwVehicleGlassConstructor::SetCarGlassMaterialFlag(materialIndex, name);
}

__COMMENT(static) int fwVehicleGlassConstructorInterface::GetCarGlassMaterialFlagCount()
{
	return fwVehicleGlassConstructor::GetCarGlassMaterialFlagCount();
}

#if __PS3
__COMMENT(static) void fwVehicleGlassConstructorInterface::SetEdgeBuffers(int bufferSizeVerts, void* bufferDataVerts, int bufferSizeIndices, void* bufferDataIndices)
{
	fwVehicleGlassConstructor::SetEdgeBuffers(bufferSizeVerts, bufferDataVerts, bufferSizeIndices, bufferDataIndices);
}
#endif // __PS3

#if VEHICLE_GLASS_DEV && !__RESOURCECOMPILER

__COMMENT(static) const fwVehicleGlassWindowProxy* fwVehicleGlassConstructorInterface::GetOrCreateNewWindowProxy(
	const char* archetypeName,
	const fragType* pFragType,
	u32 lodLevel,
	u32 modelIndex,
	int componentId,
	bool bCreate,
	bool bOutputDebugFiles,
	bool bIsFromHD,
	bool bAllowExposedEdges)
{
	static atMap<u32,fwVehicleGlassWindowProxy*> s_proxies;
	fwVehicleGlassWindowProxy* pProxy = NULL;

	if (archetypeName == NULL && pFragType == NULL && modelIndex == ~0U && componentId == -1 && bCreate) // special case - delete existing proxies
	{
		int numDeleted = 0;

		for (atMap<u32,fwVehicleGlassWindowProxy*>::Iterator iter = s_proxies.CreateIterator(); iter; ++iter)
		{
			delete iter.GetData();
			numDeleted++;
		}

		s_proxies.Kill();
		Displayf("deleted %d proxies", numDeleted);
	}
	else
	{
		const u32 key = modelIndex | (componentId << 18);

		fwVehicleGlassWindowProxy** ppProxy = s_proxies.Access(key);

		if (ppProxy)
		{
			pProxy = *ppProxy;
		}
		else if (bCreate)
		{
			atArray<fwVehicleGlassWindowProxy*> proxies;
			fwVehicleGlassConstructor::ConstructWindows(
				&proxies,
				componentId,
				archetypeName,
				pFragType,
				lodLevel,
				NULL,
				0,
				bOutputDebugFiles,
				bIsFromHD,
				bAllowExposedEdges,
				0 // externalFlags
			);

			if (proxies.GetCount() > 0)
			{
				s_proxies[key] = pProxy = proxies[0];
			}
		}
	}

	return pProxy;
}

__COMMENT(static) const fwVehicleGlassWindowData* fwVehicleGlassConstructorInterface::GetOrCreateNewWindowData(
	const char* archetypeName,
	const fragType* pFragType,
	u32 lodLevel,
	u32 modelIndex,
	bool bCreate,
	bool bOutputDebugFiles,
	bool bIsFromHD,
	bool bAllowExposedEdges)
{
	static atMap<u32,fwVehicleGlassWindowData*> s_datas;
	fwVehicleGlassWindowData* pData = NULL;

	if (archetypeName == NULL && pFragType == NULL && modelIndex == ~0U && bCreate) // special case - delete existing proxies
	{
		int numDeleted = 0;

		for (atMap<u32,fwVehicleGlassWindowData*>::Iterator iter = s_datas.CreateIterator(); iter; ++iter)
		{
			delete[] iter.GetData();
			numDeleted++;
		}

		s_datas.Kill();
		Displayf("deleted %d proxies", numDeleted);
	}
	else
	{
		const u32 key = modelIndex;

		fwVehicleGlassWindowData** ppData = s_datas.Access(key);

		if (ppData)
		{
			pData = *ppData;
		}
		else if (bCreate)
		{
			atArray<fwVehicleGlassWindowProxy*> proxies;
			fwVehicleGlassConstructor::ConstructWindows(
				&proxies,
				-1,
				archetypeName,
				pFragType,
				lodLevel,
				NULL,
				0,
				bOutputDebugFiles,
				bIsFromHD,
				bAllowExposedEdges,
				0 // externalFlags
			);

			if (proxies.GetCount() > 0)
			{
				s_datas[key] = pData = fwVehicleGlassWindowData::ConstructWindowData(proxies.GetElements(), proxies.GetCount());
			}
		}
	}

	return pData;
}

__COMMENT(static) void fwVehicleGlassConstructorInterface::ReleaseAllWindowProxies()
{
	GetOrCreateNewWindowProxy(NULL, NULL, 0, ~0U, -1, true, false, false, false);
}

__COMMENT(static) void fwVehicleGlassConstructorInterface::ReleaseAllWindowData()
{
	GetOrCreateNewWindowData(NULL, NULL, 0, ~0U, true, false, false, false);
}

__COMMENT(static) void fwVehicleGlassConstructorInterface::AddWidgets(bkBank& bk)
{
	bk.PushGroup("Vehicle Glass Constructor", false);
	{
		fwVehicleGlassConstructor::s_control.AddWidgets(bk);
	}
	bk.PopGroup();
}

#endif // VEHICLE_GLASS_DEV && !__RESOURCECOMPILER

__COMMENT(static) void fwVehicleGlassConstructorInterface::ConstructWindows(atArray<fwVehicleGlassWindowProxy*,0,unsigned short>* proxies, const char* archetypeName, const fragType* pFragType, u32 lodLevel, const fwVehicleGlassConstructorTriangle* glassTris, int numGlassTris, bool bOutputDebugFiles, bool bIsFromHD, bool bAllowExposedEdges, u32 externalFlags)
{
	fwVehicleGlassConstructor::ConstructWindows(
		proxies,
		-1, // componentToConstruct (all)
		archetypeName,
		pFragType,
		lodLevel,
		glassTris,
		numGlassTris,
		bOutputDebugFiles,
		bIsFromHD,
		bAllowExposedEdges,
		externalFlags
	);
}

__COMMENT(static) void fwVehicleGlassConstructor::ConstructWindows(
	atArray<fwVehicleGlassWindowProxy*>* proxies,
	int componentToConstruct,
	const char* archetypeName,
	const fragType* pFragType,
	u32 lodLevel,
	const fwVehicleGlassConstructorTriangle* glassTris,
	int numGlassTris,
	bool bOutputDebugFiles,
	bool bIsFromHD,
	bool bAllowExposedEdges,
	u32 externalFlags)
{
	const char* pack = "pack:/";

	if (stristr(archetypeName, pack) == archetypeName)
	{
		archetypeName += strlen(pack);
	}

	char modelName[256] = "";
	strcpy(modelName, archetypeName);
	strlwr(modelName);

	if (strrchr(modelName, '/'))
	{
		strrchr(modelName, '/')[0] = '\0';
	}

	const fragDrawable* pFragDraw = pFragType->GetCommonDrawable();
	const phBound* pBound = pFragType->GetPhysics(0)->GetCompositeBounds();

	if (AssertVerify(pFragDraw && pBound && pBound->GetType() == phBound::COMPOSITE))
	{
		const phBoundComposite* pBoundComposite = static_cast<const phBoundComposite*>(pBound);
		atArray<fwVGWTriangle> triangles;
		atArray<fwVGWTriangleRange> ranges;

#if VEHICLE_GLASS_DEV
		const bool bOutputTXTFile = s_control.m_outputTXTFile && bOutputDebugFiles;
		const bool bOutputOBJFile = s_control.m_outputOBJFile && bOutputDebugFiles;
		const bool bOutputEPSFile = s_control.m_outputEPSFile && bOutputDebugFiles;

		int vehicleDataSize0 = 0;
		int vehicleDataSize1 = 0;
		int vehicleDataSize2 = 0;

		fiStream* txt = NULL;

		int numGlassGeomsOuter = 0;
		int numGlassGeomsInner = 0;

		// count glass geoms
		{
			if (glassTris)
			{
				// we could count glass geoms here, but it would be a pain. and it's not really necessary
			}
			else
			{
				const rmcLodGroup& lodGroup = pFragDraw->GetLodGroup();
				const grmModel& model = lodGroup.GetLodModel0(lodLevel);

				for (int i = 0; i < model.GetGeometryCount(); i++)
				{
					const int shaderIndex = model.GetShaderIndex(i);
					const grmShader& shader = pFragDraw->GetShaderGroup().GetShader(shaderIndex);

					if (strcmp(shader.GetName(), "vehicle_vehglass") == 0)
					{
						numGlassGeomsOuter++;
					}
					else if (strcmp(shader.GetName(), "vehicle_vehglass_inner") == 0)
					{
						numGlassGeomsInner++;
					}
				}
			}
		}
#else
		(void)bOutputDebugFiles;
#endif // VEHICLE_GLASS_DEV

		for (int componentId = 0; componentId < pBoundComposite->GetNumBounds(); componentId++)
		{
			const phBound* pBoundChild = pBoundComposite->GetBound(componentId);
			bool bNoDistanceField = false;
			u32 flags = externalFlags;

			if (pBoundChild == NULL)
			{
				continue;
			}

			const fragTypeChild* pFragChild = pFragType->GetPhysics(0)->GetChild(componentId);
			const int boneIndex = pFragType->GetBoneIndexFromID(pFragChild->GetBoneID());

			if (!AssertVerify(boneIndex != -1))
			{
				continue;
			}

			const char* boneName = pFragType->GetSkeletonData().GetBoneData(boneIndex)->GetName();

			int numVehicleGlassMaterials = 0;
			bool bIsBulletproofMaterial = false;

			for (int i = 0; i < pBoundChild->GetNumMaterials(); i++)
			{
				const phMaterialMgr::Id mtlId = pBoundChild->GetMaterialId(i) & 0xff;//PGTAMATERIALMGR->UnpackMtlId(pBoundChild->GetMaterialId(0));

				if (s_carGlassMaterialFlags[((int)mtlId)/8] & BIT(((int)mtlId)%8))//PGTAMATERIALMGR->GetIsSmashableGlass(mtlId)) // VFXGROUP_CAR_GLASS
				{
					numVehicleGlassMaterials++;

					if (mtlId == s_carGlassMaterialBulletproofIndex)
					{
						bIsBulletproofMaterial = true;
					}
				}
			}

			if (numVehicleGlassMaterials == 0)
			{
				continue;
			}
			else if (numVehicleGlassMaterials > 1)
			{
				Assertf(0, "%s: multiple glass materials used in '%s'", modelName, boneName);
				flags |= fwVehicleGlassWindow::VGW_FLAG_ERROR_MULTIPLE_MATERIALS;
			}
			else if (pBoundChild->GetNumMaterials() > 1)
			{
				Assertf(0, "%s: mixed glass material with non-glass material in '%s'", modelName, boneName);
				flags |= fwVehicleGlassWindow::VGW_FLAG_ERROR_MIXED_MATERIALS;
			}

			if (bIsFromHD)
			{
				flags |= fwVehicleGlassWindow::VGW_FLAG_IS_FROM_HD_MESH;
			}

			if (bIsBulletproofMaterial && s_control.m_skipBulletproofMtl)
			{
				bNoDistanceField = true;
			}

			if (stristr(boneName, "siren") != NULL && s_control.m_skipSirenBones)
			{
				bNoDistanceField = true;
			}

#if __RESOURCECOMPILER
			if (stricmp(modelName, "marquis") == 0) // TODO -- remove this if the marquis windows get fixed
			{
				bNoDistanceField = true;
			}
#endif // __RESOURCECOMPILER

			const int triangleIndexFirst = triangles.GetCount();
			int geomIndex = -1;
			int mappedBoneIndex = -1;

			if (glassTris)
			{
				for (int i = 0; i < numGlassTris; i++)
				{
					if (glassTris[i].m_origBoneIndex == boneIndex)
					{
						triangles.PushAndGrow(glassTris[i]);

						Assert(mappedBoneIndex == -1 || mappedBoneIndex == glassTris[i].m_mappedBoneIndex);
						mappedBoneIndex = glassTris[i].m_mappedBoneIndex;

						if (geomIndex == -1)
						{
							geomIndex = glassTris[i].m_geomIndex;
						}
						else if (geomIndex != glassTris[i].m_geomIndex)
						{
							Assertf(0, "%s: multiple glass geometries attached to '%s'", modelName, boneName);
							flags |= fwVehicleGlassWindow::VGW_FLAG_ERROR_MULTIPLE_GEOMETRIES;
							break;
						}
					}
				}
			}
			else
			{
				const rmcLodGroup& lodGroup = pFragDraw->GetLodGroup();
				const grmModel& model = lodGroup.GetLodModel0(lodLevel);

				for (int i = 0; i < model.GetGeometryCount(); i++)
				{
					const int shaderIndex = model.GetShaderIndex(i);
					const grmShader& shader = pFragDraw->GetShaderGroup().GetShader(shaderIndex);

					if (strcmp(shader.GetName(), "vehicle_vehglass") == 0)
					{
						if (ExtractTriangles(triangles, archetypeName, pFragType, model.GetGeometry(i), componentId, boneIndex, bAllowExposedEdges) > 0)
						{
							if (geomIndex == -1)
							{
								geomIndex = i;
							}
							else
							{
								Assertf(0, "%s: multiple glass geometries attached to '%s'", modelName, boneName);
								flags |= fwVehicleGlassWindow::VGW_FLAG_ERROR_MULTIPLE_GEOMETRIES;
								break;
							}
						}
					}
				}
			}

			if (geomIndex == -1)
			{
				Assertf(0, "%s: no glass geometry attached to '%s'", modelName, boneName);
			}
			else
			{
				fwVGWTriangleRange& range = ranges.Grow();

				range.m_componentId = componentId;
				range.m_glassFirst = triangleIndexFirst;
				range.m_glassLast = triangles.GetCount() - 1;

				if (componentToConstruct == -1 ||
					componentToConstruct == componentId)
				{
					fwVGWComponentConstructor comp(modelName, boneName);
					atArray<fwVGWEdge> boundaryEdges;

					// construct diffuse/alpha (if we saved this in the window data, we could make the glass tessellation cheaper ..)
					{
						Vec2V diffuseSum(V_ZERO);
						Vec2V diffuseMin(V_FLT_MAX);
						Vec2V diffuseMax = -diffuseMin;

						for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								const Vec2V diffuse = triangles[i].m_diffuse[j];

								diffuseSum += diffuse;
								diffuseMin = Min(diffuse, diffuseMin);
								diffuseMax = Max(diffuse, diffuseMax);
							}
						}

						comp.m_diffuse = diffuseSum*ScalarV(1.0f/(float)(3*(range.m_glassLast - range.m_glassFirst + 1)));
						comp.m_diffuseMin = diffuseMin;
						comp.m_diffuseMax = diffuseMax;
					}

					// construct average texture scale (average world-per-texel of triangle edges for secondary texcoords)
					{
						comp.m_textureScale = VGW_DEFAULT_TEXTURE_SCALE;
						comp.m_textureScaleMin = +FLT_MAX;
						comp.m_textureScaleMax = -FLT_MAX;
						comp.m_textureScaleAvg = 0.0f;
						comp.m_textureScaleStd = 0.0f;

						float textureScale_s0 = 0.0f;
						float textureScale_s1 = 0.0f;
						float textureScale_s2 = 0.0f;

						float textureScale_p = 0.0f;
						float textureScale_q = 0.0f;

						for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
						{
							const Vec3V position0 = triangles[i].m_p[0];
							const Vec3V position1 = triangles[i].m_p[1];
							const Vec3V position2 = triangles[i].m_p[2];

							const Vec2V texcoord0 = triangles[i].m_t[0].GetZW(); // secondary texcoords
							const Vec2V texcoord1 = triangles[i].m_t[1].GetZW();
							const Vec2V texcoord2 = triangles[i].m_t[2].GetZW();

							Vec3V s;
							Vec3V t;
							if (CalculateTriangleTangentSpace(s, t, position0, position1, position2, texcoord0, texcoord1, texcoord2))
							{
								const float textureScale = sqrtf((MagSquared(s) + MagSquared(t)).Getf()/2.0f);

								comp.m_textureScaleMin = Min<float>(textureScale, comp.m_textureScaleMin);
								comp.m_textureScaleMax = Max<float>(textureScale, comp.m_textureScaleMax);

								textureScale_s0 += 1.0f;
								textureScale_s1 += textureScale;
								textureScale_s2 += textureScale*textureScale;

								const float weight = CalculateTriangleArea(position0, position1, position2).Getf();

								textureScale_p += weight*textureScale;
								textureScale_q += weight;
							}
						}

						if (textureScale_s0 > 0.0f)
						{
							comp.m_textureScaleAvg = textureScale_s1/textureScale_s0;
							comp.m_textureScaleStd = sqrtf(Max<float>(0.0f, textureScale_s0*textureScale_s2 - textureScale_s1*textureScale_s1))/textureScale_s0;
						}

						if (textureScale_q > 0.0f)
						{
							comp.m_textureScale = textureScale_p/textureScale_q;
						}
					}

					if (!bNoDistanceField) // construct basis/extent
					{
						fwBestFitPlane fit;
						atArray<Vec3V> fitPoints;

						for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								const Vec3V p = triangles[i].m_p[j];
								bool bUnique = true;

								if (s_control.m_fitUniquePoints)
								{
									for (int k = 0; k < fitPoints.GetCount(); k++)
									{
										if (IsLessThanAll(Abs(p - fitPoints[k]), Vec3V(V_FLT_SMALL_3)))
										{
											bUnique = false;
											break;
										}
									}

									if (bUnique)
									{
										fitPoints.PushAndGrow(p);
									}
								}

								fit.AddPoint(p);
							}
						}

						const Vec4V plane = fit.GetPlane();
						const Vec3V basisZ = plane.GetXYZ();

						Vec3V up = Vec3V(V_Z_AXIS_WZERO);

						if (IsLessThanAll(Abs(Abs(basisZ) - up), Vec3V(V_FLT_SMALL_2)))
						{
							up = Vec3V(V_X_AXIS_WZERO); // pick another axis so we don't get a degenerate cross product
						}

						Vec3V basisX = Normalize(Cross(up, basisZ));
						Vec3V basisY = Cross(basisX, basisZ);

						// some vehicles have windscreens which are more efficient to RLE compress vertically ..
						// maybe we would do this dynamically instead of using a hardcoded list?
						if (stricmp(boneName, "windscreen") == 0)
						{
							if (strcmp(modelName, "annihilator") == 0 ||
								strcmp(modelName, "buzzard"    ) == 0 ||
								strcmp(modelName, "buzzard2"   ) == 0 ||
								strcmp(modelName, "cargobob"   ) == 0 ||
								strcmp(modelName, "entityxf"   ) == 0 ||
								strcmp(modelName, "jet"        ) == 0 ||
								strcmp(modelName, "jetmax"     ) == 0 ||
								strcmp(modelName, "luxor"      ) == 0 ||
								strcmp(modelName, "shamal"     ) == 0 ||
								strcmp(modelName, "speeder"    ) == 0 ||
								strcmp(modelName, "squalo"     ) == 0 ||
								strcmp(modelName, "titan"      ) == 0 ||
								strcmp(modelName, "tropic"     ) == 0)
							{
								std::swap(basisX, basisY);
							}
						}
						else if (stricmp(boneName, "misc_a") == 0)
						{
							if (strcmp(modelName, "tropic") == 0)
							{
								std::swap(basisX, basisY);
							}
						}

						Vec3V basisMin(V_FLT_MAX);
						Vec3V basisMax = -basisMin;

						for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								const Vec3V p = triangles[i].m_p[j];
								const Vec3V v(Dot(p, basisX), Dot(p, basisY), Dot(p, basisZ));

								basisMin = Min(v, basisMin);
								basisMax = Max(v, basisMax);
							}
						}

						const Vec3V basisCentre = (basisMax + basisMin)*ScalarV(V_HALF);
						const Vec3V basisExtent = (basisMax - basisMin)*ScalarV(V_HALF);

						Transpose(comp.m_basis.GetMat33Ref(), Mat33V(basisX, basisY, basisZ));
						comp.m_basis.SetCol3(-basisCentre);

						const Vec3V scale = Vec3V(Invert(basisExtent.GetXY())*ScalarV(V_HALF), ScalarV(V_ONE));
						comp.m_basis.GetCol0Ref() *= scale;
						comp.m_basis.GetCol1Ref() *= scale;
						comp.m_basis.GetCol2Ref() *= scale;
						comp.m_basis.GetCol3Ref() *= scale;
						comp.m_basis.GetCol3Ref() += Vec3V(Vec2V(V_HALF), ScalarV(V_ZERO));
						comp.m_extent = basisExtent;
						comp.m_numGlassTris = range.m_glassFirst != -1 ? (range.m_glassLast - range.m_glassFirst + 1) : 0;
						comp.m_uniquePoints = fitPoints.GetCount();
					}

					if (!bNoDistanceField) // construct grid
					{
						// minimum cell size which would yield the max rows and columns (the most we support with RLE)
						const float minCellSizeX = 2.0f*comp.m_extent.GetXf()/(float)(VEHICLE_GLASS_MAX_DATA_COLS - 1) + 0.001f;
						const float minCellSizeY = 2.0f*comp.m_extent.GetYf()/(float)(VEHICLE_GLASS_MAX_DATA_ROWS - 1) + 0.001f;

						comp.m_cellSize   = Max<float>(s_control.m_cellSize, minCellSizeX, minCellSizeY);
						comp.m_downsample = s_control.m_downsample;
						comp.m_gridMargin = s_control.m_downsample*Max<int>(0, s_control.m_extraMargin + s_control.m_blurPasses);
						comp.m_gridCols   = s_control.m_downsample*((int)ceilf(2.0f*comp.m_extent.GetXf()/comp.m_cellSize) + 1) + comp.m_gridMargin*2;
						comp.m_gridRows   = s_control.m_downsample*((int)ceilf(2.0f*comp.m_extent.GetYf()/comp.m_cellSize) + 1) + comp.m_gridMargin*2;
#if __ASSERT
						if (comp.m_cellSize != s_control.m_cellSize) // catch where we've adjusted the cell size
						{
							Assertf(
								0,
								"### %s: '%s' extent is (%f,%f), cell size is %f (minCellSize=%f,%f), data is %dx%d (should be %dx%d adjusted for margin)",
								modelName,
								boneName,
								VEC2V_ARGS(comp.m_extent),
								comp.m_cellSize,
								minCellSizeX,
								minCellSizeY,
								(comp.m_gridCols)/s_control.m_downsample,
								(comp.m_gridRows)/s_control.m_downsample,
								(comp.m_gridCols - comp.m_gridMargin*2)/s_control.m_downsample,
								(comp.m_gridRows - comp.m_gridMargin*2)/s_control.m_downsample
							);
						}
#endif // __ASSERT
						const int gridSize = (comp.m_gridRows*comp.m_gridCols + 7)/8;
						comp.m_grid = rage_new u8[gridSize];
						sysMemSet(comp.m_grid, 0, gridSize*sizeof(u8));

						// temporary hack for manana ..
						/*if (stricmp(modelName, "manana") == 0 && (stricmp(boneName, "window_lf") == 0 || stricmp(boneName, "window_rf") == 0))
						{
							bool bHasExposedVerts = false;

							for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
							{
								if (triangles[i].m_exposed[0] ||
									triangles[i].m_exposed[1] ||
									triangles[i].m_exposed[2])
								{
									bHasExposedVerts = true;
									break;
								}
							}

							if (!bHasExposedVerts)
							{
								// NOTE -- i don't want to stop ragebuilder processing these vehicles, but i do want the string "Error" to occur in the log so i'll see it ..
								Warningf("%s: (Error) using hardcoded exposed vertex paint for %s, vehicle asset needs to be fixed!", modelName, boneName);

								for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
								{
									for (int j = 0; j < 3; j++)
									{
										const Vec2V p = comp.TransformToGrid(triangles[i].m_p[j]);

										if (p.GetXf() > 0.5f*(float)comp.m_gridCols &&
											p.GetYf() < 0.5f*(float)comp.m_gridRows)
										{
											triangles[i].m_exposed[j] = true;
										}
									}
								}
							}
						}*/

						Vec3V bmin(V_FLT_MAX);
						Vec3V bmax = -bmin;

						for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								bmin = Min(triangles[i].m_p[j], bmin);
								bmax = Max(triangles[i].m_p[j], bmax);
							}
						}

						const Vec3V centroid = (bmax + bmin)*ScalarV(V_HALF);

						for (int i_0 = range.m_glassFirst; i_0 <= range.m_glassLast; i_0++)
						{
							for (int side_0 = 0; side_0 < 3; side_0++)
							{
								const Vec3V p0_0 = triangles[i_0].m_p[(side_0 + 0)];
								const Vec3V p1_0 = triangles[i_0].m_p[(side_0 + 1)%3];

								bool bConnected = false;

								for (int i_1 = range.m_glassFirst; i_1 <= range.m_glassLast; i_1++)
								{
									if (i_1 != i_0)
									{
										for (int side_1 = 0; side_1 < 3; side_1++)
										{
											const Vec3V p0_1 = triangles[i_1].m_p[(side_1 + 0)];
											const Vec3V p1_1 = triangles[i_1].m_p[(side_1 + 1)%3];

											if (MaxElement(Abs(p0_0 - p1_1)).Getf() <= 0.001f &&
												MaxElement(Abs(p1_0 - p0_1)).Getf() <= 0.001f)
											{
												bConnected = true;
												break;
											}
										}
									}

									if (bConnected)
									{
										break;
									}
								}

								if (!bConnected)
								{
									bool bExposed = false;

									if (triangles[i_0].m_exposed[(side_0 + 0)] ||
										triangles[i_0].m_exposed[(side_0 + 1)%3])
									{
										if (s_control.m_exposedEdgesEnabled)
										{
											bExposed = true;
										}
									}

									if (bExposed)
									{
										comp.m_numExposedEdges++;
									}
									else
									{
										comp.m_numNonExposedEdges++;
										boundaryEdges.PushAndGrow(fwVGWEdge(p0_0, p1_0));
									}
								}
							}
						}

						if (comp.m_numExposedEdges > 0)
						{
							for (int i = 0; i < boundaryEdges.GetCount(); i++)
							{
								const Vec3V p0 = boundaryEdges[i].m_p[0];
								const Vec3V p1 = boundaryEdges[i].m_p[1];
								const Vec3V p0_extruded = p0 + (p0 - centroid)*ScalarV(20.0f);
								const Vec3V p1_extruded = p1 + (p1 - centroid)*ScalarV(20.0f);

								comp.RasteriseTriangle(p0, p1, p1_extruded);
								comp.RasteriseTriangle(p0, p1_extruded, p0_extruded);
							}

							for (int i = 0; i < (comp.m_gridRows*comp.m_gridCols + 7)/8; i++)
							{
								comp.m_grid[i] ^= 0xff; // invert
							}

							flags |= fwVehicleGlassWindow::VGW_FLAG_HAS_EXPOSED_EDGES;
						}
						else
						{
							for (int i = range.m_glassFirst; i <= range.m_glassLast; i++)
							{
								comp.RasteriseTriangle(triangles[i].m_p[0], triangles[i].m_p[1], triangles[i].m_p[2]);
							}
 
							comp.m_numNonExposedEdges = 0; // clear these just to be sure
						}

#if VEHICLE_GLASS_DEV
						if (bOutputDebugFiles)
						{
							grcImage* gridImage = grcImage::Create(comp.m_gridCols, comp.m_gridRows, 1, grcImage::L8, grcImage::STANDARD, 0, 0);

							if (gridImage)
							{
								sysMemSet(gridImage->GetBits(), 0, comp.m_gridCols*comp.m_gridRows);

								for (int i = 0; i < comp.m_gridRows*comp.m_gridCols; i++)
								{
									gridImage->GetBits()[i] = (comp.m_grid[i/8] & BIT(i%8)) ? 255 : 0;
								}

								gridImage->SaveDDS(atVarString(RS_ASSETS "/non_final/vgwindows/%s_%s.dds", comp.m_modelName, comp.m_boneName).c_str());
								gridImage->Release();
							}
						}
#endif // VEHICLE_GLASS_DEV
					}

#if VEHICLE_GLASS_DEV
					int componentDataSize0 = 0;
					int componentDataSize1 = 0;
					int componentDataSize2 = 0;
#endif // VEHICLE_GLASS_DEV

					if (!bNoDistanceField) // construct data
					{
						const int blurPasses = (comp.m_numExposedEdges > 0) ? s_control.m_blurPassesExposed : s_control.m_blurPasses;

						comp.ConstructDistanceField(blurPasses, s_control.m_blurNormalise, boundaryEdges, flags);
						comp.ConstructSpans(s_control.m_thresholdMin, s_control.m_thresholdMax);
					}

					// construct window
					{
						comp.ConstructWindow(componentId, mappedBoneIndex != -1 ? mappedBoneIndex : boneIndex, geomIndex, flags);
					}

					if (!bNoDistanceField)
					{
						if (s_control.m_outputDataMask)
						{
							for (int j = 0; j < comp.m_dataH; j++)
							{
								char buf[257];

								for (int i = 0; i < comp.m_dataW; i++)
								{
									const bool bGTEMin = comp.IsDataGTEMin(i, j);
									const bool bLTEMax = comp.IsDataLTEMax(i, j);

									if (bGTEMin && bLTEMax)
									{
										buf[i] = 'x';
									}
									else if (bGTEMin)
									{
										buf[i] = 'G';
									}
									else if (bLTEMax)
									{
										buf[i] = 'L';
									}
									else
									{
										buf[i] = '.';
									}
								}

								buf[comp.m_dataW] = '\0';

								Displayf("%s row[%03d/%03d]: %s", boneName, j + 1, comp.m_dataH, buf);
							}
						}
#if VEHICLE_GLASS_DEV
						u8* scratchData = NULL;
						u32 dataRLESizeSingleSpan = 0;
						comp.Quantise<u8>(scratchData, dataRLESizeSingleSpan, false);
						delete[] scratchData;

						// data size if we didn't use RLE compression
						componentDataSize0 = (comp.m_dataRowLast - comp.m_dataRowFirst + 1)*(comp.m_dataColLast - comp.m_dataColFirst + 1)*sizeof(u8);

						// data size using single-span RLE compression
						componentDataSize1 = dataRLESizeSingleSpan;

						// data size using two-span RLE compression
						componentDataSize2 = comp.m_windowProxy->m_window->m_dataRLESize;

						vehicleDataSize0 += componentDataSize0;
						vehicleDataSize1 += componentDataSize1;
						vehicleDataSize2 += componentDataSize2;
#endif // VEHICLE_GLASS_DEV
					}

					if (proxies)
					{
						if (0) // TODO -- how much would we save by sharing RLE data between windows?
						{
							for (int i = 0; i < proxies->GetCount(); i++)
							{
								const char* boneName2 = pFragType->GetSkeletonData().GetBoneData(proxies->operator[](i)->m_boneIndex)->GetName();

								const fwVehicleGlassWindow* pA = proxies->operator[](i)->m_window;
								const fwVehicleGlassWindow* pB = comp.m_windowProxy->m_window;
								bool bNearlyIdentical = false;

								if (pA->m_dataCols == pB->m_dataCols &&
									pA->m_dataRows == pB->m_dataRows &&
									pA->m_dataRLESize == pB->m_dataRLESize &&
									memcmp(pA->m_dataRLE, pB->m_dataRLE, pA->m_dataRLESize) == 0)
								{
									const float minDiff = Abs<float>(pA->m_dataMin - pB->m_dataMin);
									const float maxDiff = Abs<float>(pA->m_dataMax - pB->m_dataMax);

									if (minDiff < 0.01f &&
										maxDiff < 0.01f)
									{
										Displayf("%s: '%s' and '%s' are nearly identical (minDiff=%f, maxDiff=%f)", modelName, boneName, boneName2, minDiff, maxDiff);
										bNearlyIdentical = true;
									}
								}

								if (!bNearlyIdentical)
								{
									if (proxies->operator[](i)->m_dataW == comp.m_windowProxy->m_dataW &&
										proxies->operator[](i)->m_dataH == comp.m_windowProxy->m_dataH)
									{
										const int w = proxies->operator[](i)->m_dataW;
										const int h = proxies->operator[](i)->m_dataH;

										float maxDiff = 0.0f;

										for (int j = 0; j < h; j++)
										{
											for (int i = 0; i < w; i++)
											{
												maxDiff = Max<float>(Abs<float>(proxies->operator[](i)->m_data[i + j*w] - comp.m_windowProxy->m_data[i + j*w]), maxDiff);
											}
										}

										Displayf("%s: '%s' and '%s' are the same dimensions (maxDiff=%f)", modelName, boneName, boneName2, maxDiff);
									}
								}
							}
						}

						proxies->PushAndGrow(comp.m_windowProxy);
						comp.m_windowProxy = NULL;
					}

#if VEHICLE_GLASS_DEV
					if (bOutputEPSFile && !bNoDistanceField)
					{
						fiStream* eps = fiStream::Create(atVarString("%s/%s_%s%s.eps", s_control.m_outputDir, modelName, boneName, s_control.m_outputExt).c_str());

						if (eps)
						{
							fprintf(eps, "%%!PS-Adobe EPSF-3.0\n");
							fprintf(eps, "%%%%HiResBoundingBox: 0 0 700 700\n");
							fprintf(eps, "%%%%BoundingBox: 0 0 700 700\n");
							fprintf(eps, "\n");
							fprintf(eps, "/Times-Roman findfont\n");
							fprintf(eps, "12 scalefont\n");
							fprintf(eps, "setfont\n");
							fprintf(eps, "25 12 moveto\n");
							fprintf(eps, "0 0 0 setrgbcolor\n");
							fprintf(eps, "(%s_%s) show\n", modelName, boneName);
							fprintf(eps, "\n");

							comp.DrawEPSData(eps);

							if (s_control.m_outputEPSContours)
							{
								const int contourCount = s_control.m_outputEPSContourCount;
								const float contourMin = s_control.m_outputEPSContourMin;
								const float contourMax = s_control.m_outputEPSContourMin + s_control.m_outputEPSContourStep*(float)(contourCount - 1);
								float* contours = rage_new float[contourCount];

								for (int i = 0; i < contourCount; i++)
								{
									contours[i] = contourMin + (contourMax - contourMin)*(float)i/(float)(contourCount - 1);
								}

								comp.DrawEPSContours(eps, contours, contourCount, false, false, s_control.m_outputEPSContourLines);

								delete[] contours;
							}

							// draw threshold contours
							{
								int numContours = 0;
								float contours[5];

								contours[numContours++] = s_control.m_thresholdMin;
								contours[numContours++] = s_control.m_thresholdMax;

								if (s_control.m_thresholdMin != 0.0f)
								{
									contours[numContours++] = 0.0f;
								}

								// black = full 32-bit float
								comp.DrawEPSContours(eps, contours, numContours, false, false, s_control.m_outputEPSContourLines);

								// red = quantised
								comp.DrawEPSContours(eps, contours, numContours, false, true, s_control.m_outputEPSContourLines, Color32(255,0,0,255));
							}

							comp.DrawEPSGrid(eps);
							comp.DrawEPSTriangles(eps, triangles, range);

							fprintf(eps, "\n");
							fprintf(eps, "showpage\n");

							eps->Close();
						}
					}

					char report[1024] = "";
					char reportExposed[64] = "";
					char reportDiffuse[64] = "";
					char reportAlpha[64] = "";
					char reportTextureScale[256] = "";

					if (comp.m_numExposedEdges > 0)
					{
						sprintf(reportExposed, ", %d of %d exposed edges", comp.m_numExposedEdges, comp.m_numExposedEdges + comp.m_numNonExposedEdges);
					}

					if ((comp.m_diffuseMax - comp.m_diffuseMin).GetXf() <= 0.001f)
					{
						sprintf(reportDiffuse, "%.3f", comp.m_diffuse.GetXf());
					}
					else
					{
						sprintf(reportDiffuse, "[%.3f..%.3f]", comp.m_diffuseMin.GetXf(), comp.m_diffuseMax.GetXf());
					}

					if ((comp.m_diffuseMax - comp.m_diffuseMin).GetYf() <= 0.001f)
					{
						sprintf(reportAlpha, "%.3f", comp.m_diffuse.GetYf());
					}
					else
					{
						sprintf(reportAlpha, "[%.3f..%.3f]", comp.m_diffuseMin.GetYf(), comp.m_diffuseMax.GetYf());
					}

					if (__DEV)
					{
						sprintf(reportTextureScale, "%.4f [%.4f..%.4f, avg=%f, std=%f]", comp.m_textureScale, comp.m_textureScaleMin, comp.m_textureScaleMax, comp.m_textureScaleAvg, comp.m_textureScaleStd);
					}
					else
					{
						sprintf(reportTextureScale, "%.3f", comp.m_textureScale);
					}

					if (bNoDistanceField)
					{
						sprintf(
							report,
							"'%s' (componentId=%d) has %d triangles, diffuse = %s, alpha = %s, texscale = %s, no distance field",
							boneName,
							componentId,
							range.m_glassFirst != -1 ? (range.m_glassLast - range.m_glassFirst + 1) : 0,
							reportDiffuse,
							reportAlpha,
							reportTextureScale
						);
					}
					else
					{
						sprintf(
							report,
							"'%s' (componentId=%d) has %d triangles, %d points%s, diffuse = %s, alpha = %s, texscale = %s, extent = (%f,%f,%f), data = %dx%d, (%d cols, %d rows), data size = %.2f/%.2f/%.2fKB",
							boneName,
							componentId,
							comp.m_numGlassTris,
							comp.m_uniquePoints,
							reportExposed,
							reportDiffuse,
							reportAlpha,
							reportTextureScale,
							VEC3V_ARGS(comp.m_extent),
							comp.m_dataW,
							comp.m_dataH,
							comp.m_dataColLast - comp.m_dataColFirst + 1,
							comp.m_dataRowLast - comp.m_dataRowFirst + 1,
							(float)componentDataSize0/1024.0f,
							(float)componentDataSize1/1024.0f,
							(float)componentDataSize2/1024.0f
						);

						if (comp.m_mergedSpanCount > 0)
						{
							strcat(report, atVarString(" (%d spans merged)", comp.m_mergedSpanCount).c_str());
						}
					}

					Displayf("%s: %s", modelName, report);

					if (txt == NULL && bOutputTXTFile)
					{
						char txtPath[RAGE_MAX_PATH] = "";

						if (componentToConstruct != -1)
						{
							sprintf(txtPath, "%s/%s_%s%s.txt", s_control.m_outputDir, modelName, boneName, s_control.m_outputExt);
						}
						else
						{
							sprintf(txtPath, "%s/%s%s.txt", s_control.m_outputDir, modelName, s_control.m_outputExt);
						}

						txt = fiStream::Create(txtPath);
					}

					if (txt)
					{
						fprintf(txt, "%s\n", report);
					}
#endif // VEHICLE_GLASS_DEV
				}
			}
		}

#if VEHICLE_GLASS_DEV
		if (ranges.GetCount() > 0)
		{
			if (componentToConstruct == -1)
			{
				s_control.m_sumDataSize[0] += vehicleDataSize0;
				s_control.m_sumDataSize[1] += vehicleDataSize1;
				s_control.m_sumDataSize[2] += vehicleDataSize2;
				s_control.m_maxDataSize[0] = Max<int>(vehicleDataSize0, s_control.m_maxDataSize[0]);
				s_control.m_maxDataSize[1] = Max<int>(vehicleDataSize1, s_control.m_maxDataSize[1]);
				s_control.m_maxDataSize[2] = Max<int>(vehicleDataSize2, s_control.m_maxDataSize[2]);

				char report[256] = "";
				sprintf(
					report,
					"total data size = %.2f/%.2f/%.2fKB",
					(float)vehicleDataSize0/1024.0f,
					(float)vehicleDataSize1/1024.0f,
					(float)vehicleDataSize2/1024.0f
				);

				if (numGlassGeomsOuter > 0 ||
					numGlassGeomsInner > 0)
				{
					strcat(report, atVarString(" (%d/%d glass geometries)", numGlassGeomsOuter, numGlassGeomsInner).c_str());
				}

				Displayf("%s: %s", modelName, report);

				if (txt)
				{
					fprintf(txt, "%s\n", report);
				}
			}

			if (bOutputOBJFile)
			{
				for (int rangeIndex = 0; rangeIndex < ranges.GetCount(); rangeIndex++)
				{
					const int componentId = ranges[rangeIndex].m_componentId;

					if (componentToConstruct == -1 ||
						componentToConstruct == componentId)
					{
						const fragTypeChild* pFragChild = pFragType->GetPhysics(0)->GetChild(componentId);
						const int boneIndex = pFragType->GetBoneIndexFromID(pFragChild->GetBoneID());
						const char* boneName = pFragType->GetSkeletonData().GetBoneData(boneIndex)->GetName();

						char objPath[RAGE_MAX_PATH] = "";
						sprintf(objPath, "%s/%s_%s%s.obj", s_control.m_outputDir, modelName, boneName, s_control.m_outputExt);

						fiStream* obj = fiStream::Create(objPath);

						if (obj)
						{
							int materialGroupId = 1;

							fprintf(obj, "mtllib ../materials.mtl\n");

							for (int k = 0; k < ranges.GetCount(); k++)
							{
								const int first = ranges[k].m_glassFirst;
								const int last  = ranges[k].m_glassLast;

								fprintf(obj, "g %d\n", materialGroupId++);
								fprintf(obj, "usemtl %s\n", k == rangeIndex ? "red" : "white");

								for (int i = first; i <= last; i++)
								{
									for (int j = 0; j < 3; j++)
									{
										fprintf(obj, "v %f %f %f\n", VEC3V_ARGS(triangles[i].m_p[j]));
									}
								}

								for (int i = 0; i <= last - first; i++)
								{
									fprintf(obj, "f %d %d %d\n", -(i*3 + 3), -(i*3 + 2), -(i*3 + 1));
								}
							}

							obj->Close();
						}
					}
				}
			}
		}

		if (txt)
		{
			txt->Close();
		}
#else
		(void)bAllowExposedEdges;
#endif // VEHICLE_GLASS_DEV
	}
}

__COMMENT(static) int fwVehicleGlassConstructor::ExtractTriangles(atArray<fwVGWTriangle>& triangles, const char* archetypeName, const fragType* pFragType, grmGeometry& geom, int componentId, int boneIndex, bool bAllowExposedEdges)
{
	Assert(boneIndex >= 0); // must be skinned

	int numTrianglesExtracted = 0;

#if __PS3
	if (geom.GetType() == grmGeometry::GEOMETRYEDGE && AssertVerify(s_bufferDataI && s_bufferDataV))
	{
		const grmGeometryEdge* pGeomEdge = static_cast<const grmGeometryEdge*>(&geom);

#if VEHICLE_GLASS_DEV
		const fragDrawable* pFragDraw = pFragType->GetCommonDrawable();
		const char* boneName = pFragDraw->GetSkeletonData()->GetBoneData(boneIndex)->GetName();

		// check up front how many verts are in processed geometry and assert if too many
		int numI = 0;
		int numV = 0;

		const EdgeGeomPpuConfigInfo* edgeGeomInfos = pGeomEdge->GetEdgeGeomPpuConfigInfos();
		const int count = pGeomEdge->GetEdgeGeomPpuConfigInfoCount();

		for (int i = 0; i < count; i++)
		{
			numI += edgeGeomInfos[i].spuConfigInfo.numIndexes;
			numV += edgeGeomInfos[i].spuConfigInfo.numVertexes;
		}

		if (numI > s_bufferSizeI)
		{
			Assertf(0, "%s: '%s' index buffer has more indices (%d) than system can handle (%d)", archetypeName, boneName, numI, s_bufferSizeI);
			return false;
		}

		if (numV > s_bufferSizeV)
		{
			Assertf(0, "%s: '%s' vertex buffer has more verts (%d) than system can handle (%d)", archetypeName, boneName, numV, s_bufferSizeV);
			return false;
		}
#endif // VEHICLE_GLASS_DEV

		static Vec4V* edgeVertexStreams[CExtractGeomParams::obvIdxMax] ;
		sysMemSet(&edgeVertexStreams[0], 0, sizeof(edgeVertexStreams));

		u16* indices = (u16*)s_bufferDataI;

		u8  BoneIndexesAndWeights[8192];
		int BoneOffset1     = 0;
		int BoneOffset2     = 0;
		int BoneOffsetPoint = 0;
		int BoneIndexOffset = 0;
		int BoneIndexStride = 1;

#if HACK_GTA4_MODELINFOIDX_ON_SPU && __PS3
		CGta4DbgSpuInfoStruct gtaSpuInfoStruct;
		gtaSpuInfoStruct.gta4RenderPhaseID = 0;
		gtaSpuInfoStruct.gta4ModelInfoIdx  = 0;//pPhysical->GetModelIndex();
		gtaSpuInfoStruct.gta4ModelInfoType = 0;//pPhysical->GetBaseModelInfo()->GetModelType();
#endif // HACK_GTA4_MODELINFOIDX_ON_SPU && __PS3

		int numVerts = 0;
		const int numIndices = pGeomEdge->GetVertexAndIndex(
			(Vector4*)s_bufferDataV,
			s_bufferSizeV,
			(Vector4**)edgeVertexStreams,
			indices,
			s_bufferSizeI,
			BoneIndexesAndWeights,
			sizeof(BoneIndexesAndWeights),
			&BoneIndexOffset,
			&BoneIndexStride,
			&BoneOffset1,
			&BoneOffset2,
			&BoneOffsetPoint,
			(u32*)&numVerts,
#if HACK_GTA4_MODELINFOIDX_ON_SPU
			&gtaSpuInfoStruct,
#endif // HACK_GTA4_MODELINFOIDX_ON_SPU
			NULL,
			CExtractGeomParams::extractPos | CExtractGeomParams::extractUv | CExtractGeomParams::extractCol
		);

		Assertf(!(BoneIndexStride != 1 || BoneIndexOffset != 0), "%s: geometry is not hardskinned", archetypeName);

		const Vec3V* positions = (const Vec3V*)edgeVertexStreams[CExtractGeomParams::obvIdxPositions];
		const Vec4V* texcoords = (const Vec4V*)edgeVertexStreams[CExtractGeomParams::obvIdxUVs];
		const Vec4V* colours   = (const Vec4V*)edgeVertexStreams[CExtractGeomParams::obvIdxColors];

		for (int i = 0; i < numIndices; i += 3)
		{
			const int index0 = (int)indices[i + 0];
			const int index1 = (int)indices[i + 1];
			const int index2 = (int)indices[i + 2];

			if (index0 != index1 &&
				index1 != index2 &&
				index2 != index0)
			{
				int blendBoneIndex = BoneIndexesAndWeights[index0*BoneIndexStride + BoneIndexOffset];

				if (blendBoneIndex >= BoneOffsetPoint)
				{
					blendBoneIndex += BoneOffset2 - BoneOffsetPoint;
				}
				else
				{
					blendBoneIndex += BoneOffset1;
				}

				if (blendBoneIndex == boneIndex)
				{
					triangles.PushAndGrow(fwVGWTriangle(
						positions[index0],
						positions[index1],
						positions[index2],
						texcoords[index0],
						texcoords[index1],
						texcoords[index2],
						colours[index0],
						colours[index1],
						colours[index2]
					));

					if (!bAllowExposedEdges)
					{
						triangles.back().m_exposed[0] = false;
						triangles.back().m_exposed[1] = false;
						triangles.back().m_exposed[2] = false;
					}

					numTrianglesExtracted++;
				}
			}
		}
	}
	else
#endif // __PS3
#if __RESOURCECOMPILER
	{
		(void)triangles;
		(void)archetypeName;
		(void)pFragType;
		(void)geom;
		(void)componentId;
		(void)boneIndex;
		(void)bAllowExposedEdges;
	}
#else // not __RESOURCECOMPILER
	{
		(void)archetypeName;
		(void)pFragType;
		(void)componentId;

		grcVertexBuffer* vb = geom.GetVertexBuffer(true);
		grcIndexBuffer*  ib = geom.GetIndexBuffer (true);

		const u16* pMatrixPalette = geom.GetMatrixPalette();

		Assert(vb && ib && pMatrixPalette);

		PS3_ONLY(++g_AllowVertexBufferVramLocks); // PS3: attempting to edit VBs in VRAM
		grcVertexBufferEditor vertexBufferEditor(vb, true, true); // lock=true, readOnly=true

		Assert(geom.GetPrimitiveType() == drawTris);
		Assert(geom.GetPrimitiveCount()*3 == (u32)ib->GetIndexCount());

		const int  numVerts     = vb->GetVertexCount();
		const int  numTriangles = ib->GetIndexCount()/3;
		const u16* indexData    = ib->LockRO();

		for (int i = 0; i < numTriangles; i++)
		{
			const u16 index0 = indexData[i*3 + 0];
			const u16 index1 = indexData[i*3 + 1];
			const u16 index2 = indexData[i*3 + 2];

			if (!AssertVerify((int)Max<u16>(index0, index1, index2) < numVerts))
			{
				continue;
			}

			if (pMatrixPalette[vertexBufferEditor.GetBlendIndices(index0).GetRed()] == boneIndex)
			{
				const Vector2 uv0 = vertexBufferEditor.GetUV(index0, 0);
				const Vector2 uv1 = vertexBufferEditor.GetUV(index1, 0);
				const Vector2 uv2 = vertexBufferEditor.GetUV(index2, 0);

				const Vector2 uv0_sec = vertexBufferEditor.GetUV(index0, 1);
				const Vector2 uv1_sec = vertexBufferEditor.GetUV(index1, 1);
				const Vector2 uv2_sec = vertexBufferEditor.GetUV(index2, 1);

				triangles.PushAndGrow(fwVGWTriangle(
					VECTOR3_TO_VEC3V(vertexBufferEditor.GetPosition(index0)),
					VECTOR3_TO_VEC3V(vertexBufferEditor.GetPosition(index1)),
					VECTOR3_TO_VEC3V(vertexBufferEditor.GetPosition(index2)),
					Vec4V(uv0.x, 1.0f - uv0.y, uv0_sec.x, 1.0f - uv0_sec.y),
					Vec4V(uv1.x, 1.0f - uv1.y, uv1_sec.x, 1.0f - uv1_sec.y),
					Vec4V(uv2.x, 1.0f - uv2.y, uv2_sec.x, 1.0f - uv2_sec.y),
					vertexBufferEditor.GetCPV(index0).GetRGBA(),
					vertexBufferEditor.GetCPV(index1).GetRGBA(),
					vertexBufferEditor.GetCPV(index2).GetRGBA()
				));

				if (!bAllowExposedEdges)
				{
					triangles.back().m_exposed[0] = false;
					triangles.back().m_exposed[1] = false;
					triangles.back().m_exposed[2] = false;
				}

				numTrianglesExtracted++;
			}
		}

		PS3_ONLY(--g_AllowVertexBufferVramLocks); // PS3: finished with editing VBs in VRAM

		ib->UnlockRO();
	}

	// check if all of the triangles are exposed, this would indicate that cpv.b was zero for all
	// triangles, which would mean the vehicle was not converted recently .. in this case we want
	// to just force all the edges to be non-exposed.
	{
		bool bHasNonExposedEdges = false;

		for (int i = 0; i < triangles.GetCount(); i++)
		{
			if (!triangles[i].m_exposed[0] ||
				!triangles[i].m_exposed[1] ||
				!triangles[i].m_exposed[2])
			{
				bHasNonExposedEdges = true;
				break;
			}
		}

		if (!bHasNonExposedEdges)
		{
			Assertf(0, "%s: all vertices for boneIndex %d have cpv.b = 0, vehicle must be reconverted", archetypeName, boneIndex);

			for (int i = 0; i < triangles.GetCount(); i++)
			{
				triangles[i].m_exposed[0] = false;
				triangles[i].m_exposed[1] = false;
				triangles[i].m_exposed[2] = false;
			}
		}
	}
#endif // not __RESOURCECOMPILER

	return numTrianglesExtracted;
}

#if __RESOURCECOMPILER
PARAM(vgwindowsdebugoutput, "Output debug files for vehicle glass windows into assets:/non_final/vgwindows");

__COMMENT(static) fwVehicleGlassWindowData* fwVehicleGlassConstructorInterface::ConstructWindowData(const char* pFilename, const fragType* pFragType, u32 lodLevel, const fwVehicleGlassConstructorTriangle* glassTris, int numGlassTris, bool bIsFromHD, bool UNUSED_PARAM(bAllowExposedEdges), u32 externalFlags)
{
	fwVehicleGlassWindowData* data = NULL;

	if (numGlassTris > 0)
	{
		sysMemStartTemp();

		atArray<fwVehicleGlassWindowProxy*> proxies;
		bool bOutputDebugFiles = false;

#if VEHICLE_GLASS_DEV
		if (PARAM_vgwindowsdebugoutput.Get())//sysBootManager::IsDebuggerPresent())
		{
			bOutputDebugFiles = true;
		}

		fwVehicleGlassConstructorInterface::SetDebugOutputDirectory(RS_BUILDBRANCH "/common/non_final/vgwindows");
#endif // VEHICLE_GLASS_DEV
		fwVehicleGlassConstructorInterface::ConstructWindows(&proxies, pFilename, pFragType, lodLevel, glassTris, numGlassTris, bOutputDebugFiles, bIsFromHD, true, externalFlags);

		sysMemEndTemp();

		const bool bByteSwap = (g_sysPlatform == platform::XENON || g_sysPlatform == platform::PS3);
		data = fwVehicleGlassWindowData::ConstructWindowData(proxies.GetElements(), proxies.GetCount(), bByteSwap);

		sysMemStartTemp();

		for (int i = 0; i < proxies.GetCount(); i++)
		{
			delete proxies[i];
		}

		proxies.Reset();

		sysMemEndTemp();
	}

	return data;
}
#endif // __RESOURCECOMPILER

} // namespace rage

#endif // VEHICLE_GLASS_CONSTRUCTOR
