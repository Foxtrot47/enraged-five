//
// vfx/optimisations.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef VFX_FW_OPTIMISATIONS_H
#define VFX_FW_OPTIMISATIONS_H

#define VFX_FW_GENERIC_OPTIMISATIONS_OFF	0
#define VFX_FW_PTFX_OPTIMISATIONS_OFF		0
#define VFX_FW_DECAL_OPTIMISATIONS_OFF		0

#if VFX_FW_GENERIC_OPTIMISATIONS_OFF
#define VFX_FW_GENERIC_OPTIMISATIONS()		OPTIMISATIONS_OFF()
#else
#define VFX_FW_GENERIC_OPTIMISATIONS()
#endif

#if VFX_FW_PTFX_OPTIMISATIONS_OFF
#define VFX_FW_PTFX_OPTIMISATIONS()			OPTIMISATIONS_OFF()
#else
#define VFX_FW_PTFX_OPTIMISATIONS()
#endif	

#if VFX_FW_DECAL_OPTIMISATIONS_OFF
#define VFX_FW_DECAL_OPTIMISATIONS()		OPTIMISATIONS_OFF()
#else
#define VFX_FW_DECAL_OPTIMISATIONS()
#endif

#endif // VFX_FW_OPTIMISATIONS_H
