#ifndef VFX_ASYNCSLIST_H
#define VFX_ASYNCSLIST_H

#include "slist.h"
#include "system/membarrier.h"

#if __PPU
#	include "system/llsc.h"
#	include <ppu_intrinsics.h>
#elif __SPU
#	include "system/dma.h"
#	include <spu_intrinsics.h>
#elif RSG_ORBIS
#	include <sce_atomic.h>
#elif __WIN32
#	include "system/xtl.h"
#	if __XENON
#		include "system/llsc.h"
#	endif
#endif


#define ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE         SPU_ONLY(, AsyncSListBase<PTRCONVERTER> *thisEa, u32 dmaTag)
#define ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE             SPU_ONLY(, AsyncSList                   *thisEa, u32 dmaTag)
#define ASYNCSLIST_SPU_COMMA_ARGS_PASS(THISEA, DMATAG)  SPU_ONLY(,                               THISEA,     DMATAG)

#if __SPU
#	define ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER
#else
#	define ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER  =PTRCONVERTER()
#endif

#if __PS3 || __XENON
	// PPC uses L[WD]ARX/ST[WD]CX. on PPU/XENON, and GETLLAR/PUTLLC on SPU, so
	// the ABA problem doesn't exist.
#	define ASYNCSLIST_ABA   0
#else
	// Other platforms do need to protect against ABA.
#	define ASYNCSLIST_ABA   1
#endif


namespace rage {

////////////////////////////////////////////////////////////////////////////////
//  AlignBits
////////////////////////////////////////////////////////////////////////////////
// Very similar to UIntBits, but 128-bit specialization handled differently, as
// here we need a type that has no ctors or assignment operators, so that it can
// be used inside of a union.
template<unsigned NUMBITS> struct AlignBits;
template<> struct AlignBits<8 > { typedef u8   T; };
template<> struct AlignBits<16> { typedef u16  T; };
template<> struct AlignBits<32> { typedef u32  T; };
template<> struct AlignBits<64> { typedef u64  T; };
template<> struct AlignBits<128>{ typedef u128 T; };


////////////////////////////////////////////////////////////////////////////////
//  ASYNCSLISTBASE_ALIGN_WITH_UNION
////////////////////////////////////////////////////////////////////////////////
#ifdef __SNC__
	// SNC440 has become more strict on what is allowed inside a union, but has
	// not implemented C++11 unrestricted unions to allow a way around this.  It
	// is correct, but is also annoying.  See
	// https://ps3.scedev.net/forums/thread/229031.
#	define ASYNCSLISTBASE_ALIGN_WITH_UNION  0
#else
	// Microsoft compilers need to use a union for alignment, since
	// __declspec(aligned(X)) doesn't work on templated classes.  The union hack
	// actually turns also out a bit simpler for platforms that need a sequence
	// counter for ABA handling, so keep the union approach as the default.
#	define ASYNCSLISTBASE_ALIGN_WITH_UNION  1
#endif


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListBase
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER=SListDefaultPtrConverter>
class AsyncSListBase
{
public:

	SListItemBase<PTRCONVERTER> *Pop(const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE);
	void Push(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE);
	inline void Push(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE);

	inline bool IsEmpty() const { return !data.size; }
	inline unsigned GetNumItems() const { return (unsigned)data.size; }

	// Syncrhonous peek.  Higher level code must ensure list is not being
	// updated while this is called.
	inline const SListItemBase<PTRCONVERTER> *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER()) const { return conv.ToPtr(data.head); }
	inline       SListItemBase<PTRCONVERTER> *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER())       { return conv.ToPtr(data.head); }

	// Syncrhonous scan for tail.  Higher level code must ensure list is not
	// being updated while this is called.
	SListItemBase<PTRCONVERTER> *SyncScanTail(const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER SPU_ONLY(, u32 dmaTag));

#if !__SPU
	inline void SetEmpty() { data.head=(uptr)0; data.size=0; }
	inline AsyncSListBase() { SetEmpty(); }

	// Syncrhonous count number of items in list.  This is slow, and only
	// intended for debugging.  Generally you will want to use GetNumItems().
	// Higher level code must ensure list is not being updated while this is
	// called.
	unsigned SyncCountNumItems(const PTRCONVERTER &conv=PTRCONVERTER()) const;

	// Syncrhonous peek.  Higher level code must ensure list is not being
	// updated while this is called.
	inline SListItemBase<PTRCONVERTER> *SyncPop(const PTRCONVERTER &conv=PTRCONVERTER());

	// Synchronous push.  Higher level code must ensure list is not
	// being updated while this is called.
	inline void SyncPush(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER());
	inline void SyncPush(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv=PTRCONVERTER()) { SyncPush(n, n, 1, conv); }
#endif

protected:

	typedef typename PTRCONVERTER::STORAGE STORAGE;
#if !ASYNCSLIST_ABA
	typedef typename UIntBits <sizeof(STORAGE)*8*1>::T  SIZETYPE;
	typedef typename AlignBits<sizeof(STORAGE)*8*2>::T  ALIGNTYPE;
	typedef typename UIntBits <sizeof(ALIGNTYPE)*8>::T  ATOMIC_TYPE;
#else
	// When the storage size is the same as that of a pointer, we halve the
	// number of bit used for the list size, as we don't assume a CAS operation
	// that supports more than twice the size of a pointer.
	typedef typename UIntBits <(sizeof(STORAGE)<sizeof(void*)) ? (sizeof(STORAGE)*8)   : (sizeof(STORAGE)*8/2)>::T  SIZETYPE;
	// The sequence counter uses the remaining bits up to the next power of two multiple.
	// This means we will always get at least 16-bits for the sequence counter.
	typedef typename UIntBits <(sizeof(STORAGE)<sizeof(void*)) ? (sizeof(STORAGE)*8*2) : (sizeof(STORAGE)*8/2)>::T  SEQTYPE;
	CompileTimeAssert(sizeof(SEQTYPE) >= 2);
	typedef typename AlignBits<sizeof(SIZETYPE)*8*4>::T ALIGNTYPE;
	typedef typename UIntBits <sizeof(ALIGNTYPE)*8>::T ATOMIC_TYPE;

	CompileTimeAssert(!(sizeof(STORAGE)==sizeof(void*)) || (
		sizeof(ATOMIC_TYPE) == 2*sizeof(void*) &&
		sizeof(SIZETYPE)    == sizeof(void*)/2 &&
		sizeof(SEQTYPE)     == sizeof(void*)/2 ));

	CompileTimeAssert(!(sizeof(STORAGE)<sizeof(void*)) || (
		sizeof(ATOMIC_TYPE) == 4*sizeof(STORAGE) &&
		sizeof(SIZETYPE)    == sizeof(STORAGE) &&
		sizeof(SEQTYPE)     == 2*sizeof(STORAGE) ));
#endif

#if ASYNCSLISTBASE_ALIGN_WITH_UNION
	union
	{
#endif
		struct
		{
#if __BE
			volatile STORAGE  head;
			volatile SIZETYPE size;
#endif
#if ASYNCSLIST_ABA
			volatile SEQTYPE  seq;
#endif
#if !__BE
			volatile SIZETYPE size;
			volatile STORAGE  head;
#endif
		} data;

#if ASYNCSLISTBASE_ALIGN_WITH_UNION
		ALIGNTYPE forceAlignmentToBeCorrect;
	};
#endif

	enum { HEAD_SHIFT = (sizeof(ATOMIC_TYPE)-sizeof(STORAGE))*8 };
#	if ASYNCSLIST_ABA
		enum { SIZE_SHIFT = sizeof(SEQTYPE)*8 };
#	endif
}
#if !ASYNCSLISTBASE_ALIGN_WITH_UNION
#	if !ASYNCSLIST_ABA
		ALIGNED(sizeof(PTRCONVERTER::STORAGE)*2)
#	else
#		error "ASYNCSLISTBASE_ALIGN_WITH_UNION required with ASYNCSLIST_ABA"
#	endif
#endif
;


#if __WIN32 && !__XENON
	template<class ATOMIC_TYPE> static inline ATOMIC_TYPE CompareAndSwap(volatile ATOMIC_TYPE *ptr, ATOMIC_TYPE newVal, ATOMIC_TYPE cmpVal);
	template<> static inline u32 CompareAndSwap(volatile u32 *ptr, u32 newVal, u32 cmpVal) { return _InterlockedCompareExchange  ((volatile LONG*)  ptr, newVal, cmpVal); }
	template<> static inline u64 CompareAndSwap(volatile u64 *ptr, u64 newVal, u64 cmpVal) { return _InterlockedCompareExchange64((volatile LONG64*)ptr, newVal, cmpVal); }
#	if __64BIT
		template<> static inline UInt128 CompareAndSwap(volatile UInt128 *ptr, UInt128 newVal, UInt128 cmpVal)
		{
			__int64 xchgHi = newVal.hi;
			__int64 xchgLo = newVal.lo;
			__int64 cmp[2] = { cmpVal.lo, cmpVal.hi };
			ASSERT_ONLY(const unsigned char ok =) _InterlockedCompareExchange128((volatile __int64*)ptr, xchgHi, xchgLo, cmp);
			UInt128 ret;
			ret.lo = cmp[0];
			ret.hi = cmp[1];
			Assert(!!ok == (cmpVal==ret));
			return ret;
		}
#	endif

#elif RSG_ORBIS
	// Note: newVal and cmpVal are swapped in Sony functions; system/interlocked.h already deals with this.
	template<class ATOMIC_TYPE> static inline ATOMIC_TYPE CompareAndSwap(volatile ATOMIC_TYPE *ptr, ATOMIC_TYPE newVal, ATOMIC_TYPE cmpVal);
	template<> inline u32 CompareAndSwap(volatile u32 *ptr, u32 newVal, u32 cmpVal) { return sceAtomicCompareAndSwap32((volatile int32_t*)  ptr, cmpVal, newVal); }
	template<> inline u64 CompareAndSwap(volatile u64 *ptr, u64 newVal, u64 cmpVal) { return sceAtomicCompareAndSwap64((volatile int64_t*)ptr, cmpVal, newVal); }
	template<> inline UInt128 CompareAndSwap(volatile UInt128 *ptr, UInt128 newVal, UInt128 cmpVal) { __int128 result = sceAtomicCompareAndSwap128((volatile __int128*)ptr, (__int128&)cmpVal, (__int128&)newVal); return (UInt128&) result; }

#elif __SPU
	template<class T> struct SiFromHelper;
	template<> struct SiFromHelper<u8 >      { qword operator()(u8  t) const { return si_from_uchar (t); } };
	template<> struct SiFromHelper<u16>      { qword operator()(u16 t) const { return si_from_ushort(t); } };
	template<> struct SiFromHelper<u32>      { qword operator()(u32 t) const { return si_from_uint  (t); } };
	template<class U> struct SiFromHelper<U*>{ qword operator()(U  *t) const { return si_from_ptr   (t); } };
	template<class T> static inline qword SiFrom(T t) { return SiFromHelper<T>()(t); }

#endif


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListBase::Push
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
void AsyncSListBase<PTRCONVERTER>::Push(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE)
{
	Assert(!!first ^ !last);
	Assert(!!first ^ !count);
	if(!first)
	{
		return;
	}


#	if __PPU || __XENON
		LLSC_MSR();
		volatile ATOMIC_TYPE *const pHead = (ATOMIC_TYPE*)&data.head;
		ATOMIC_TYPE wr;
		do
		{
			const ATOMIC_TYPE rd = LoadAndReserve(pHead);
			const STORAGE prev = SListCastStorage<STORAGE>((uptr)(rd >> HEAD_SHIFT));
			const STORAGE firstStor = conv.FromPtr(first, prev);
			const ATOMIC_TYPE firstStorShifted = (ATOMIC_TYPE)(uptr)firstStor << HEAD_SHIFT;
			const SIZETYPE sz = (SIZETYPE)rd;
			wr = firstStorShifted | (ATOMIC_TYPE)(sz + count);
			Assert(conv.ToPtr(prev) != last);
			last->SetNextRaw(prev);
			sysMemWriteBarrier();
		}
		while(Unlikely(!StoreConditional(pHead, wr)));

#	elif __WIN32 || RSG_ORBIS
		volatile ATOMIC_TYPE *const p = (ATOMIC_TYPE*)this;
		ATOMIC_TYPE cmp, wr;
		do
		{
			cmp = *p;
			const STORAGE prev = SListCastStorage<STORAGE>((uptr)(cmp >> HEAD_SHIFT));
			const STORAGE firstStor = conv.FromPtr(first, prev);
			const ATOMIC_TYPE firstStorShifted = (ATOMIC_TYPE)(uptr)firstStor << HEAD_SHIFT;
			const SIZETYPE sz = (SIZETYPE)(cmp >> SIZE_SHIFT);
			const SEQTYPE seq = (SEQTYPE)cmp;
			wr = firstStorShifted | (((ATOMIC_TYPE)(sz+count))<<SIZE_SHIFT) | (ATOMIC_TYPE)(SEQTYPE)(seq+1);
			Assert(conv.ToPtr(prev) != last);
			last->SetNextRaw(prev);
			sysMemWriteBarrier();
		}
		while(CompareAndSwap(p, wr, cmp) != cmp);

#	elif __SPU
		char buf[128] __attribute__((__aligned__(128)));
		const u32 ea128 = (u32)thisEa & ~127;
		volatile AsyncSListBase *const ls = (AsyncSListBase*)(buf + ((u32)thisEa & 127));
		CompileTimeAssert(sizeof(STORAGE)==1 || sizeof(STORAGE)==2 || sizeof(STORAGE)==4);
		const qword shufSplat = sizeof(STORAGE)==1 ? si_ilh(0x0303) : sizeof(STORAGE)==2 ? si_ilh(0x0203) : si_ila(0x10203);
		do
		{
			// Get and reserve cache line containing struct
			spu_writech(MFC_LSA, (u32)buf);
			spu_writech(MFC_EAL, ea128);
			spu_writech(MFC_Cmd, MFC_GETLLAR_CMD);
			spu_readch(MFC_RdAtomicStat);

			// Put the current head pointer to the last of the items we are pushing
			// Note, that the put here is fenced, this allows the same tag to be used by the caller
			// for setting up the items to be pushed.
			Assert(conv.ToPtr(ls->data.head) != last);
			const qword headStor = SiFrom((uptr)ls->data.head);
			const qword headStorSplat = si_shufb(headStor, headStor, shufSplat);
			CompileTimeAssert(sizeof(STORAGE) <= 16);
			CompileTimeAssert(__alignof__(STORAGE) >= sizeof(STORAGE));
			sysDmaSmallPutfAndWait((char*)&headStorSplat + ((u32)last & 15), (u32)last, sizeof(STORAGE), dmaTag);

			// Attempt to replace head pointer with first and increase size
			const STORAGE & firstStor = conv.FromPtr(first, ls->data.head);
			sysMemCpy((void*)&ls->data.head, &firstStor, sizeof(ls->data.head));
			ls->data.size += count;
			spu_writech(MFC_LSA, (u32)buf);
			spu_writech(MFC_EAL, ea128);
			spu_writech(MFC_Cmd, MFC_PUTLLC_CMD);
		}
		while(Unlikely(spu_readch(MFC_RdAtomicStat)));

		// Update caller's local store copy
		sysMemCpy((void*)&data.head, (const void*)ls->data.head, sizeof(data.head));
		sysMemCpy((void*)&data.size, (const void*)ls->data.size, sizeof(data.size));

#	else
#		error "TODO: Not yet implemented for this platform"
#	endif
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListBase::Push
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void AsyncSListBase<PTRCONVERTER>::Push(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE)
{
	Push(n, n, 1, conv ASYNCSLIST_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListBase::Pop
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
SListItemBase<PTRCONVERTER> *AsyncSListBase<PTRCONVERTER>::Pop(const PTRCONVERTER &conv ASYNCSLISTBASE_SPU_COMMA_ARGS_SIGNATURE)
{
	SListItemBase<PTRCONVERTER> *popped;

#	if __PPU || __XENON
		LLSC_MSR();
		volatile ATOMIC_TYPE *const pHead = (ATOMIC_TYPE*)&data.head;
		ATOMIC_TYPE wr;
		do
		{
			const ATOMIC_TYPE rd = LoadAndReserve(pHead);
			if(!rd)
			{
				// Clear reservation by storing to a scratch address.
				ATOMIC_TYPE scratch;
				StoreConditional(&scratch, rd);
				return NULL;
			}
			popped = conv.ToPtr(SListCastStorage<STORAGE>((uptr)(rd >> HEAD_SHIFT)));
			const SIZETYPE sz = (SIZETYPE)rd;
			const STORAGE next = SListCastStorage<STORAGE>((uptr)popped->GetNextRaw());
			wr = ((ATOMIC_TYPE)(uptr)next << HEAD_SHIFT) | (ATOMIC_TYPE)(sz - 1);
		}
		while(Unlikely(!StoreConditional(pHead, wr)));

#	elif __WIN32 || RSG_ORBIS
		volatile ATOMIC_TYPE *const p = (ATOMIC_TYPE*)this;
		ATOMIC_TYPE cmp, wr;
		do
		{
			cmp = *p;
			const SIZETYPE sz = (SIZETYPE)(cmp >> SIZE_SHIFT);
			if(!sz) return NULL;
			popped = conv.ToPtr(SListCastStorage<STORAGE>((uptr)(cmp >> HEAD_SHIFT)));
			const SEQTYPE seq  = (SEQTYPE)cmp;
			const STORAGE next = SListCastStorage<STORAGE>((uptr)popped->GetNextRaw());
			wr = ((ATOMIC_TYPE)(uptr)next<<HEAD_SHIFT) | (((ATOMIC_TYPE)(sz-1))<<SIZE_SHIFT) | (ATOMIC_TYPE)(SEQTYPE)(seq+1);
		}
		while(CompareAndSwap(p, wr, cmp) != cmp);

#	elif __SPU
		char buf[128] __attribute__((__aligned__(128)));
		const u32 ea128 = (u32)thisEa & ~127;
		volatile AsyncSListBase *const ls = (AsyncSListBase*)(buf + ((u32)thisEa & 127));
		u32 hEa;
		do
		{
			// Get and reserve cache line containing struct
			spu_writech(MFC_LSA, (u32)buf);
			spu_writech(MFC_EAL, ea128);
			spu_writech(MFC_Cmd, MFC_GETLLAR_CMD);
			spu_readch(MFC_RdAtomicStat);

			// List empty ?
			STORAGE hStor;
			sysMemCpy(&hStor, (const char*)&ls->data.head, sizeof(hStor));
			if(Unlikely(!hStor)) return NULL;
			hEa = (u32)conv.ToPtr(hStor);

			// Get the next pointer out of the current head node
			qword hLs;
			CompileTimeAssert(sizeof(SListItemBase<PTRCONVERTER>) <= 16);
			CompileTimeAssert(__alignof__(SListItemBase<PTRCONVERTER>) >= sizeof(SListItemBase<PTRCONVERTER>));
			sysDmaSmallGetAndWait(&hLs, hEa&~15, 16, dmaTag);

			// Attempt to replace head pointer with next and decrement size
			const STORAGE nextStor = SListCastStorage<STORAGE>((uptr)si_to_uint(si_rotqby(hLs, si_from_uint(hEa - (4-sizeof(STORAGE))))));
			sysMemCpy((char*)&ls->data.head, &nextStor, sizeof(ls->data.head));
			--(ls->data.size);
			spu_writech(MFC_LSA, (u32)buf);
			spu_writech(MFC_EAL, ea128);
			spu_writech(MFC_Cmd, MFC_PUTLLC_CMD);
		}
		while(Unlikely(spu_readch(MFC_RdAtomicStat)));

		// Update caller's local store copy
		sysMemCpy((void*)&data.head, (const void*)&ls->data.head, sizeof(data.head));
		sysMemCpy((void*)&data.size, (const void*)&ls->data.size, sizeof(data.size));

		// Return the item we just popped
		popped = (SListItemBase<PTRCONVERTER>*)hEa;

#	else
#		error "TODO: Not yet implemented for this platform"
#	endif
    return popped;
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListBase::SyncScanTail
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
SListItemBase<PTRCONVERTER> *AsyncSListBase<PTRCONVERTER>::SyncScanTail(const PTRCONVERTER &conv SPU_ONLY(, u32 dmaTag))
{
#if !__SPU
	SListItemBase<PTRCONVERTER> *tail = conv.ToPtr(data.head);
	if(tail)
	{
		SListItemBase<PTRCONVERTER> *const next = tail->GetNext(conv);
		while(next)
		{
			tail = next;
			next = next->GetNext();
		}
	}
	return tail;

#else
	SListItemBase<PTRCONVERTER> *tailEa = conv.ToPtr(data.head);
	if(tailEa)
	{
		SListItemBase<PTRCONVERTER> *const next = tailEa;
		do
		{
			tailEa = next;
			qword tail;
			sysDmaGetAndWait(&tail, (u32)tailEa & ~15, 16, dmaTag);
			next = conv.ToPtr(SListCastStorage<STORAGE>(si_to_uint(si_rotqby(tail, si_from_uint((u32)tailEa-(4-sizeof(STORAGE)))))));
		}
		while(next);
	}
	return tailEa;

#endif
}


#if !__SPU

	////////////////////////////////////////////////////////////////////////////
	//  AsyncSListBase::SyncPop
	////////////////////////////////////////////////////////////////////////////
	template<class PTRCONVERTER>
	inline SListItemBase<PTRCONVERTER> *AsyncSListBase<PTRCONVERTER>::SyncPop(const PTRCONVERTER &conv)
	{
		SListItemBase<PTRCONVERTER> *n = conv.ToPtr(data.head);
		if(n)
		{
			data.head = n->GetNext(conv);
			--data.size;
		}
		return n;
	}


	////////////////////////////////////////////////////////////////////////////
	//  AsyncSListBase::SyncPush
	////////////////////////////////////////////////////////////////////////////
	template<class PTRCONVERTER>
	inline void AsyncSListBase<PTRCONVERTER>::SyncPush(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv)
	{
		last->SetNext(data.head, conv);
		data.head = conv.FromPtr(first, data.head);
		data.size += count;
	}


	////////////////////////////////////////////////////////////////////////////
	//  AsyncSListBase::SyncCountNumItems
	////////////////////////////////////////////////////////////////////////////
	template<class PTRCONVERTER>
	unsigned AsyncSListBase<PTRCONVERTER>::SyncCountNumItems(const PTRCONVERTER &conv) const
	{
		unsigned count = 0;
		const SListItemBase<PTRCONVERTER> *ptr = conv.ToPtr(data.head);
		while(ptr)
		{
			++count;
			ptr = ptr->GetNext(conv);
		}
		Assert(count == data.size);
		return count;
	}

#endif // !__SPU




////////////////////////////////////////////////////////////////////////////////
//  AsyncSList
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER=SListDefaultPtrConverter>
class AsyncSList : public AsyncSListBase<PTRCONVERTER>
{
public:

	inline USERTYPE *Pop(const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE);
	inline void Push(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE);
	inline void Push(USERTYPE *n, const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE);
	inline const USERTYPE *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER()) const { return static_cast<const USERTYPE*>(AsyncSListBase<PTRCONVERTER>::SyncPeek(conv)); }
	inline       USERTYPE *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER())       { return static_cast<      USERTYPE*>(AsyncSListBase<PTRCONVERTER>::SyncPeek(conv)); }
	inline USERTYPE *SyncScanTail(const PTRCONVERTER &conv ASYNCSLIST_NON_SPU_DEFAULT_PTRCONVERTER SPU_ONLY(, u32 dmaTag)) { return static_cast<USERTYPE*>(AsyncSListBase<PTRCONVERTER>::SyncScanTail(conv SPU_ONLY(, dmaTag))); }

#if !__SPU
	inline void SyncPush(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER()) { AsyncSListBase<PTRCONVERTER>::SyncPush(first, last, count, conv); }
	inline void SyncPush(USERTYPE *n, const PTRCONVERTER &conv=PTRCONVERTER()) { AsyncSListBase<PTRCONVERTER>::SyncPush(n, n, 1, conv); }
#endif
};


////////////////////////////////////////////////////////////////////////////////
//  AsyncSList::Push
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER>
inline void AsyncSList<USERTYPE, PTRCONVERTER>::Push(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE)
{
	AsyncSListBase<PTRCONVERTER>::Push(first, last, count, conv ASYNCSLIST_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncSList::Push
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER>
inline void AsyncSList<USERTYPE, PTRCONVERTER>::Push(USERTYPE *n, const PTRCONVERTER &conv ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE)
{
	AsyncSListBase<PTRCONVERTER>::Push(n, conv ASYNCSLIST_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncSList::Pop
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER>
inline USERTYPE *AsyncSList<USERTYPE, PTRCONVERTER>::Pop(const PTRCONVERTER &conv ASYNCSLIST_SPU_COMMA_ARGS_SIGNATURE)
{
	return static_cast<USERTYPE*>(AsyncSListBase<PTRCONVERTER>::Pop(conv ASYNCSLIST_SPU_COMMA_ARGS_PASS(thisEa, dmaTag)));
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListAligned
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, unsigned ALIGN>
class AsyncSListAligned : public AsyncSList<USERTYPE, SListDefaultAlignPtrConverter<ALIGN> >
{
};


////////////////////////////////////////////////////////////////////////////////
//  AsyncSListIndexed
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class STORAGE>
class AsyncSListIndexed : public AsyncSList<USERTYPE, SListIdxPtrConverter<USERTYPE, STORAGE> >
{
};

} // namespace rage

#endif // VFX_ASYNCSLIST_H
