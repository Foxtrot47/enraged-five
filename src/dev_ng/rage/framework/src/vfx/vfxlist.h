//
// vfx/vfxlist.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_LIST_H
#define VFX_LIST_H


// namespaces 
namespace rage {


// defines
#define USE_BASIC_ONLY													// gets rid of prev and tail ptrs and related functions if defined


// classes

// PURPOSE
//  base class for an individual item in a list

class vfxListItem
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		vfxListItem();

		vfxListItem* GetNext() {return m_pNext;}
		const vfxListItem* GetNext() const {return m_pNext;}
		void SetNext(vfxListItem* pItem) {m_pNext = pItem;}

#ifndef USE_BASIC_ONLY
		vfxListItem* GetPrev() {return m_pPrev;}
		const vfxListItem* GetPrev() const {return m_pPrev;}
		void SetPrev(vfxListItem* pItem) {m_pPrev = pItem;}
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		vfxListItem*		m_pNext;
#ifndef USE_BASIC_ONLY
		vfxListItem*		m_pPrev;
#endif 

};


// PURPOSE
//  class representing a linked list of objects that inherit from the vfxListItem base class
class vfxList
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////
	
		vfxList();
		int AddItem(vfxListItem* pItem);
		int AddItemAfter(vfxListItem* pItem, vfxListItem* pItemToAddAfter);
		int RemoveItem(vfxListItem* pItem);
		vfxListItem* RemoveHead();
#ifdef USE_BASIC_ONLY
		vfxListItem* RemoveHeadPrefetchNext();
#else

		// When USE_BASIC_ONLY is not defined, RemoveHead() will end up caching
		// the next entry anyways.
		vfxListItem* RemoveHeadPrefetchNext() { return RemoveHead(); }
#endif
		int RemoveAll();
		vfxListItem* GetHead() {return m_pHead;}
		const vfxListItem* GetHead() const {return m_pHead;}
		vfxListItem* GetNext(vfxListItem* pItem) {return pItem->GetNext();}
		const vfxListItem* GetNext(const vfxListItem* pItem) const {return pItem->GetNext();}
		u32 GetNumItems() const {return m_numItems;}
		
#ifndef USE_BASIC_ONLY
		int AppendItem(vfxListItem* pItem);
		int AddItemBefore(vfxListItem* pItem, vfxListItem* pItemToAddBefore);
		vfxListItem* RemoveTail();
		vfxListItem* GetTail() {return m_pTail;}
		const vfxListItem* GetTail() const {return m_pTail;}
		vfxListItem* GetPrev(vfxListItem* pItem) {return pItem->GetPrev();}
		const vfxListItem* GetPrev(const vfxListItem* pItem) const {return pItem->GetPrev();}
//		vfxListItem* GetItemOffset(bool head, int offset);
#endif

#if __ASSERT
		void CheckListIntegrity(char* txt);
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		u32 m_numItems;
		vfxListItem* m_pHead;
#ifndef USE_BASIC_ONLY
		vfxListItem* m_pTail;
#endif 

	template<bool PREFETCH_NEXT>
	vfxListItem* TemplatedRemoveHead();
};


} // namespace rage

#endif // PTFX_LIST_H


