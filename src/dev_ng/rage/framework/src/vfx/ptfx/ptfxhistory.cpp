//
// vfx/ptfxhistory.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxhistory.h"

// includes (framework)
#include "entity/entity.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxdebug.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// namespaces 
namespace rage {


// static member variables
atArray<ptfxHistoryInfo> ptfxHistory::ms_infos;


// code
void ptfxHistory::Init(int maxSize)
{
	ms_infos.Reset();
	ms_infos.Reserve(maxSize);
	ms_infos.Resize(maxSize);

	for (int i=0; i<ms_infos.GetCapacity(); i++)
	{
		ms_infos[i].isValid = false;
	}
}

void ptfxHistory::Update(float deltaTime)
{	
#if __BANK
	ptfxDebug::ms_numFxHistories = 0;
#endif

	for (int i=0; i<ms_infos.GetCapacity(); i++)
	{
		if (ms_infos[i].isValid)
		{
			// check for deleted entities
			if (ms_infos[i].hasEntity && !ms_infos[i].regdEntity)
			{
				// the entity has been deleted - we're no longer valid
				ms_infos[i].isValid = false;
			}
			else
			{
				// still valid - increment the elapsed time
				ms_infos[i].elapsedTime += deltaTime; 

				// check if we're expired
				if (ms_infos[i].elapsedTime>ms_infos[i].aliveTime)
				{
					// the info has expired - we're no longer valid
					ms_infos[i].isValid = false;
				}
			}

#if __BANK
			if (ms_infos[i].isValid)
			{
				ptfxDebug::ms_numFxHistories++;
			}
#endif
		}

	}
}

void ptfxHistory::Add(const atHashWithStringNotFinal fxHashName, const u32 subType, fwEntity* pEntity, Vec3V_In vPosWld, const float aliveTime)
{	
	for (int i=0; i<ms_infos.GetCapacity(); i++)
	{
		if (!ms_infos[i].isValid)
		{
			ms_infos[i].isValid = true;
			ms_infos[i].fxHashName = fxHashName;
			ms_infos[i].subType = subType;
			ms_infos[i].regdEntity = pEntity;
			ms_infos[i].aliveTime = aliveTime;
			ms_infos[i].elapsedTime = 0.0f;

			if (pEntity)
			{
				ms_infos[i].vPosLcl = UnTransformOrtho(pEntity->GetMatrix(), vPosWld);
				ms_infos[i].hasEntity = true;
			}
			else
			{
				ms_infos[i].vPosLcl = vPosWld;
				ms_infos[i].hasEntity = false;
			}

			return;
		}
	}

	ptfxAssertf(0, "no space left to add history information\n");
}

bool ptfxHistory::Check(const atHashWithStringNotFinal fxHashName, const u32 subType, fwEntity* pEntity, Vec3V_In vPosWld, const float range)
{	
	for (int i=0; i<ms_infos.GetCapacity(); i++)
	{
		// only process if the entity is still valid or we never had one in the first place
		if (ms_infos[i].regdEntity || ms_infos[i].hasEntity==false)
		{
			if (ms_infos[i].isValid && ms_infos[i].fxHashName==fxHashName && ms_infos[i].subType==subType && ms_infos[i].regdEntity==pEntity)
			{

				Vec3V vInfoPosWld;
				if (ms_infos[i].regdEntity)
				{
					vInfoPosWld = Transform(ms_infos[i].regdEntity->GetMatrix(), ms_infos[i].vPosLcl);
				}
				else
				{
					vInfoPosWld = ms_infos[i].vPosLcl;
				}

				Vec3V vDiff = vPosWld - vInfoPosWld;
				if (MagSquared(vDiff).Getf()<=range*range)
				{
					return true;
				}
			}
		}

	}

	return false;
}



} // namespace rage

