//
// vfx/ptfxattach.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxattach.h"

// includes (framework)
#include "entity/entity.h"
#include "vfx/channel.h"
#include "vfx/vfxutil.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxconfig.h"
#include "vfx/ptfx/ptfxdebug.h"
#include "vfx/ptfx/ptfxflags.h"
#include "vfx/ptfx/ptfxwrapper.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// namespaces 
namespace rage {


// static member variables
atArray<ptfxAttachInfo> ptfxAttach::ms_attachInfos;
atMap<ptxEffectInst*, ptfxAttachInfo*> ptfxAttach::ms_attachInfoMap;
vfxList ptfxAttach::ms_attachInfoPool;
vfxList ptfxAttach::ms_attachInfoList;



// code
void ptfxAttach::Init(int maxSize)
{
	ms_attachInfos.Reset();
	ms_attachInfos.Reserve(maxSize);
	ms_attachInfos.Resize(maxSize);

	ms_attachInfoMap.Kill();
}

void ptfxAttach::InitLevel()
{
	// initialise the attach infos and add them to the pool
	ptfxFatalAssertf(ms_attachInfoList.GetNumItems()==0, "attach info list is not empty");
	ptfxFatalAssertf(ms_attachInfoPool.GetNumItems()==0, "attach info pool is not empty");

	for (int i=0; i<ms_attachInfos.GetCapacity(); i++)
	{
		// initialise
		ms_attachInfos[i].m_pFxInst = NULL;
		ms_attachInfos[i].m_pAttachEntity = NULL;
		ms_attachInfos[i].m_attachBoneIndex = -1;
		ms_attachInfos[i].m_attachMode = PTFXATTACHMODE_FULL;
		ms_attachInfos[i].m_useAltSkeleton = false;

		// add to pool
		ms_attachInfoPool.AddItem(&ms_attachInfos[i]);
	}
}

void ptfxAttach::ShutdownLevel()
{		
	// go through the active attach infos
	ptfxAttachInfo* pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetHead());
	while (pAttachInfo)
	{
		// store the next active attach info
		ptfxAttachInfo* pNextAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetNext(pAttachInfo));

		// stop the effect 
		ptfxWrapper::StopEffectInst(pAttachInfo->m_pFxInst);

		// detach the effect
		Detach(pAttachInfo);

		// set the next active attach info
		pAttachInfo = pNextAttachInfo;
	}

	// do some checks
	ptfxFatalAssertf(ms_attachInfoList.GetNumItems()==0, "attach info list is not empty");
	ptfxFatalAssertf(ms_attachInfoPool.GetNumItems()==(u32)ms_attachInfos.GetCapacity(), "attach info pool is not full");

	// remove all the attach infos from the pool
	ms_attachInfoPool.RemoveAll();

	// do some checks
	ptfxFatalAssertf(ms_attachInfoList.GetNumItems()==0, "attach info list is not empty");
	ptfxFatalAssertf(ms_attachInfoPool.GetNumItems()==0, "attach info pool is not empty");

	// remove all the attach infos from the map
	ms_attachInfoMap.Kill();
}

void ptfxAttach::Update()
{
#if __BANK
	ptfxDebug::ms_numFxAttached = 0;
#endif

	ptfxAttachInfo* pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetHead());
	while (pAttachInfo)
	{
		// increment the number of attached effect insts
#if __BANK
		ptfxDebug::ms_numFxAttached++;
#endif

		ptfxAttachInfo* pAttachInfoNext = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetNext(pAttachInfo));

		// deal with effects that have a null user data (the entity they point to has been deleted)
		if (!pAttachInfo->m_pAttachEntity)
		{
			UpdateLostEntity(pAttachInfo);
		}
		// deal with effects that are still attached to an entity
		else 
		{
			// set the pos
			Mat34V boneMtx = SetPtFxBaseMtx(pAttachInfo);

			// this effect still points to an entity - check that the effect is finished with (and still not registered)
			if (pAttachInfo->m_pFxInst->GetIsFinished() && pAttachInfo->m_pFxInst->GetFlag(PTFX_RESERVED_REGISTERED)==0)
			{
				// the effect is finished with - tidy up the reference
				Detach(pAttachInfo->m_pFxInst);
			}
			// check for effects that now point to a zero matrix 
			else if (IsZeroAll(boneMtx.GetCol0()))
			{
				// try to fix up the broken frag data
				if (UpdateBrokenFrag(pAttachInfo))
				{
					// set the new pos
					SetPtFxBaseMtx(pAttachInfo);
				}
				else
				{
					UpdateLostEntity(pAttachInfo);
				}
			}
			// check that the entity has valid info
			else
			{
				fwEntity* pEntity = pAttachInfo->m_pAttachEntity;
				ptfxAssertf(pEntity, "Effect inst has invalid fx user data");

				// check that the drawable still exists
				if (g_pPtfxCallbacks->GetEntityDrawable(pEntity)==NULL)
				{
					UpdateLostEntity(pAttachInfo);
				}
			}
		}

#if __ASSERT
		// check that we don't have an effect with an entity pointer flag but no entity pointer
		if (pAttachInfo->m_pFxInst && pAttachInfo->m_pFxInst->GetFlag(PTFX_RESERVED_ATTACHED))
		{
			ptfxAssertf(pAttachInfo->m_pAttachEntity, "Effect inst has its attached flag set but its fx user data is invalid");
		}
#endif

		pAttachInfo = pAttachInfoNext;
	}

	ptfxAssertf(ms_attachInfoMap.GetNumUsed()==(int)ms_attachInfoList.GetNumItems(), "mismatch between map and list");
}

Mat34V_Out ptfxAttach::SetPtFxBaseMtx(ptfxAttachInfo* pAttachInfo)
{
	Mat34V boneMtx = Mat34V(V_IDENTITY);

	if (pAttachInfo->m_attachMode!=PTFXATTACHMODE_INFO_ONLY)
	{
		Vec3V vBasePosDiff = Vec3V(V_ZERO);

		g_pPtfxCallbacks->GetMatrixFromBoneIndex(boneMtx, pAttachInfo->m_pAttachEntity, pAttachInfo->m_attachBoneIndex, pAttachInfo->m_useAltSkeleton);
		if (pAttachInfo->m_attachMode==PTFXATTACHMODE_FULL)
		{
			vBasePosDiff = pAttachInfo->m_pFxInst->SetBaseMtx(boneMtx);
		}
		else if (pAttachInfo->m_attachMode==PTFXATTACHMODE_POS_ONLY)
		{
			vBasePosDiff = pAttachInfo->m_pFxInst->SetBasePos(boneMtx.GetCol3());
		}

		// relocate the effect if the position has moved more than 10m
		// this is used as an addition to the relocation via the teleport system to catch cases that don't use teleport (e.g. car recordings that jump about)
		if (pAttachInfo->m_pFxInst->GetCanMoveVeryFast()==false && MagSquared(vBasePosDiff).Getf()>100.0f) // && (pAttachInfo->m_pFxInst->GetIsActive() || pAttachInfo->m_pFxInst->GetIsStopped()))
		{
			pAttachInfo->m_pFxInst->SetNeedsRelocated();
		}
	}

	return boneMtx;
}

void ptfxAttach::Attach(ptxEffectInst* pFxInst, fwEntity* pEntity, int boneIndex, ptfxAttachMode attachMode, bool useAltSkeleton)
{
#if __ASSERT
	// check that this fx inst is pointing to a vector or matrix
	ptfxAssertf(pEntity, "Trying to attach an effect instance to a NULL entity");
#endif // __ASSERT

	// try to find this fxinst in the active list
	ptfxAttachInfo* pAttachInfo = Find(pFxInst);
	if (pAttachInfo)
	{
		// already set up - check it's with the same entity
		//ptfxAssertf(pAttachInfo->m_pAttachEntity==pEntity, "Trying to attach an already attached effect instance to a different entity (%p-%s %p-%s)", &pAttachInfo->m_pAttachEntity, pAttachInfo->m_pAttachEntity->GetModelName(), &pEntity, pEntity->GetModelName());
		pAttachInfo->m_useAltSkeleton = useAltSkeleton;
		return;
	}
	else
	{
		pAttachInfo = GetAttachInfoFromPool(pFxInst);
		if (vfxVerifyf(pAttachInfo, "couldn't get an attach info from the pool"))
		{
			pAttachInfo->m_pFxInst = pFxInst;
			pAttachInfo->m_pAttachEntity = pEntity;
			pAttachInfo->m_attachBoneIndex = boneIndex;
			pAttachInfo->m_attachMode = attachMode;
			pAttachInfo->m_useAltSkeleton = useAltSkeleton;

			// set the effect's position instantly - so that pre-update works
			SetPtFxBaseMtx(pAttachInfo);

			// set the attachment flag
			ptfxFlags::SetUserReservedFlag(pFxInst, PTFX_RESERVED_ATTACHED);
		}
	}
}

void ptfxAttach::Detach(ptxEffectInst* pFxInst)
{
	if (pFxInst->GetFlag(PTFX_RESERVED_ATTACHED))
	{	
		ptfxAttachInfo* pAttachInfo = Find(pFxInst);
		if (vfxVerifyf(pAttachInfo, "couldn't find the attach info to detach effect"))
		{
			Detach(pAttachInfo);
		}
	}
#if __ASSERT
	else
	{
		// assert that the effect isn't found in out list of attach infos
		ptfxAttachInfo* pAttachInfo = Find(pFxInst);
		vfxAssertf(pAttachInfo==NULL, "effect has attach info but isn't flagged as being attached");
	}
#endif
}

void ptfxAttach::Detach(ptfxAttachInfo* pAttachInfo)
{
	// remove any attachment reference
	if (pAttachInfo->m_pAttachEntity)
	{
		pAttachInfo->m_pAttachEntity = NULL;
	}

	// clear the attachment flag
	if (pAttachInfo->m_pFxInst->GetFlag(PTFX_RESERVED_ATTACHED))
	{
		ptfxFlags::ClearUserReservedFlag(pAttachInfo->m_pFxInst, PTFX_RESERVED_ATTACHED);
	}

	// return the attach info to the pool
	ReturnAttachInfoToPool(pAttachInfo);
}

void ptfxAttach::UpdateLostEntity(ptfxAttachInfo* pAttachInfo)
{
	// stop the effect instance from emitting any more particles
	ptfxWrapper::StopEffectInst(pAttachInfo->m_pFxInst);

	// clear up attachment settings
	Detach(pAttachInfo->m_pFxInst);
}

void ptfxAttach::UpdateBrokenFrag(fwEntity* pParentEntity, fwEntity* pChildEntity)
{
	ptfxAttachInfo* pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetHead());
	while (pAttachInfo)
	{
		ptfxAttachInfo* pAttachInfoNext = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetNext(pAttachInfo));

		if (pAttachInfo->m_pAttachEntity==pParentEntity)
		{
			pAttachInfo->m_pAttachEntity = pChildEntity;
		}

		pAttachInfo = pAttachInfoNext;
	}
}

void ptfxAttach::UpdateBrokenFrag(fwEntity* pParentEntity, fwEntity* pChildEntity, s32 boneIndex)
{
	ptfxAttachInfo* pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetHead());
	while (pAttachInfo)
	{
		ptfxAttachInfo* pAttachInfoNext = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetNext(pAttachInfo));

		if (pAttachInfo->m_pAttachEntity==pParentEntity && pAttachInfo->m_attachBoneIndex==boneIndex)
		{
			pAttachInfo->m_pAttachEntity = pChildEntity;
		}

		pAttachInfo = pAttachInfoNext;
	}
}

bool ptfxAttach::UpdateBrokenFrag(ptfxAttachInfo* pAttachInfo)
{
	for (int i=0; i<vfxUtil::GetNumBrokenFrags(); i++)
	{
		const vfxBrokenFragInfo& brokenFragInfo = vfxUtil::GetBrokenFragInfo(i);

		if (brokenFragInfo.pParentEntity==pAttachInfo->m_pAttachEntity.Get() && brokenFragInfo.pChildEntity!=NULL)
		{
			pAttachInfo->m_pAttachEntity = brokenFragInfo.pChildEntity;
			return true;
		}
	}

	return false;
}

bool ptfxAttach::GetIsAttachedTo(ptxEffectInst* pFxInst, fwEntity* pEntity)
{
	ptfxAttachInfo* pAttachInfo = Find(pFxInst);
	if (pAttachInfo)
	{
		if (pAttachInfo->m_pAttachEntity==pEntity)
		{
			return true;
		}
	}

	return false;
}

fwEntity* ptfxAttach::GetAttachEntity(ptxEffectInst* pFxInst)
{
	ptfxAttachInfo* pAttachInfo = Find(pFxInst);
	if (pAttachInfo)
	{
		return pAttachInfo->m_pAttachEntity;
	}

	return NULL;
}

ptfxAttachInfo* ptfxAttach::GetAttachInfoFromPool(ptxEffectInst* pFxInst)
{
	// get an attach info from the pool
	ptfxAttachInfo* pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoPool.RemoveHead());

	// add this attach info to the active list
	if (pAttachInfo)
	{
		// add to the list
		ms_attachInfoList.AddItem(pAttachInfo);

		// add to the map
		ms_attachInfoMap.Insert(pFxInst, pAttachInfo);

		// we have a registered effect - check that it is initialised ok
		ptfxAssertf(pAttachInfo->m_pFxInst==NULL, "the new attach info has an uninitialised fx inst");
		ptfxAssertf(!pAttachInfo->m_pAttachEntity, "the new attach info has an uninitialised entity pointer");
		ptfxAssertf(pAttachInfo->m_attachBoneIndex==-1, "the new attach info has an uninitialised bone index");
		ptfxAssertf(pAttachInfo->m_attachMode==PTFXATTACHMODE_FULL, "the new attach info has an uninitialised attach mode");
		ptfxAssertf(pAttachInfo->m_useAltSkeleton==false, "the new attach info has an uninitialised useAltSkeleton flag");
	}

	// return the registered fx
	return pAttachInfo;
}

void ptfxAttach::ReturnAttachInfoToPool(ptfxAttachInfo* pAttachInfo)
{
	// remove from the map
#if __ASSERT
	bool deletedOk = 
#endif
		ms_attachInfoMap.Delete(pAttachInfo->m_pFxInst);

	ptfxAssertf(deletedOk, "failed to delete entry from the map");

	// reset the attach info
	pAttachInfo->m_pFxInst = NULL;
	pAttachInfo->m_pAttachEntity = NULL;
	pAttachInfo->m_attachBoneIndex = -1;
	pAttachInfo->m_attachMode = PTFXATTACHMODE_FULL;
	pAttachInfo->m_useAltSkeleton = false;

	// remove the attach info from the active list
	ms_attachInfoList.RemoveItem(pAttachInfo);

	// add the attach info to the pool
	ms_attachInfoPool.AddItem(pAttachInfo);
}

ptfxAttachInfo* ptfxAttach::Find(ptxEffectInst* pFxInst)
{
// 	ptfxAttachInfo* pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetHead());
// 	while (pAttachInfo)
// 	{
// 		if (pAttachInfo->m_pFxInst==pFxInst)
// 		{
// 			return pAttachInfo;
// 		}
// 
// 		pAttachInfo = static_cast<ptfxAttachInfo*>(ms_attachInfoList.GetNext(pAttachInfo));
// 	}

	ptfxAttachInfo** ppAttachInfo = ms_attachInfoMap.Access(pFxInst);
	if (ppAttachInfo)
	{
		return *ppAttachInfo;
	}

	return NULL;
}



} // namespace rage




