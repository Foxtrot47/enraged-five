//
// vfx/ptfxwrapper.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxwrapper.h"

// includes (rage)
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "string/stringhash.h"
#include "system/param.h"

// includes (framework)
#include "fwsys/timer.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxdebug.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// namespaces 
namespace rage {


// params
XPARAM(noptfx);
XPARAM(novfx);


// code
ptxEffectInst* ptfxWrapper::GetEffectInst(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListHashName)
{
	ptxEffectRule* pEffectRule = NULL;
	if (fxListHashName.GetHash()==0)
	{
		pEffectRule = RMPTFXMGR.GetEffectRule(effectRuleHashName);
	}
	else
	{
		pEffectRule = RMPTFXMGR.GetEffectRule(effectRuleHashName, fxListHashName);
	}

	if (pEffectRule)
	{
		return GetEffectInst(pEffectRule);
	}

	ptfxDebugf2("Couldn't find effect rule %s on fxlist %s, so we couldn't set up a valid effect instance", effectRuleHashName.GetCStr(), fxListHashName.GetCStr());
	return NULL;
}

ptxEffectInst* ptfxWrapper::GetEffectInst(ptxEffectRule* pEffectRule)
{
	// return if the game is paused - network games can request the same effect over and over if a game is paused
	if (fwTimer::IsGamePaused())
	{
		return NULL;
	}

	if (PARAM_noptfx.Get() || PARAM_novfx.Get())
	{
		return NULL;
	}
	else
	{
#if __BANK
		if (ptfxDebug::ms_disablePtFx)
		{
			return NULL;
		}

		if (ptfxDebug::ms_onlyPlayThisFx)
		{
			if (atHashValue(pEffectRule->GetName())!=atHashValue(ptfxDebug::ms_onlyPlayThisFxName))
			{
				return NULL;
			}
		}
#endif

		// try to create an effect instance
#if __BANK
		static int numFailedFx = 0;
#endif
		ptxEffectInst* pFxInst = RMPTFXMGR.GetEffectInst(pEffectRule);
		if (pFxInst)
		{
#if __BANK
			numFailedFx = 0;
#endif
			ptfxAssertf(pFxInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW)==0, "effect instance is reserved for drawing manually");
		}
#if __BANK
		else
		{
			numFailedFx++;
			if (numFailedFx==100)
			{
				// 100 effect inst requests have failed in a row - output debug info
				ptfxDebug::OutputActiveEffectInsts();
				ptfxAssertf(0, "100 effect instance requests have failed in a row (%s)", pEffectRule->GetName());
				numFailedFx=0;
			}
		}
#endif

		return pFxInst;
	}
}

ptxEffectRule* ptfxWrapper::GetEffectRule(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListHashName)
{
	if (fxListHashName.GetHash()==0)
	{
		return RMPTFXMGR.GetEffectRule(effectRuleHashName);
	}
	else
	{
		return RMPTFXMGR.GetEffectRule(effectRuleHashName, fxListHashName);
	}
}

ptxEffectRule* ptfxWrapper::GetEffectRule(const atHashWithStringNotFinal effectRuleHashName)
{
	return GetEffectRule(effectRuleHashName, atHashWithStringNotFinal(u32(0)));
}

void ptfxWrapper::StopEffectInst(ptxEffectInst* pFxInst)
{
	if (pFxInst->GetIsPlaying())
	{
		pFxInst->Stop();
	}
}

void ptfxWrapper::SetAlphaTint(ptxEffectInst* pFxInst, float alphaTint)
{
#if __BANK
	if (ptfxDebug::ms_disableAlphaTint)
	{
		return;
	}
#endif

	//ptxAssertf(pFxInst->GetIsPlaying()==false, "must be called before starting an effect");
	pFxInst->SetAlphaTint(alphaTint);
}

void ptfxWrapper::SetColourTint(ptxEffectInst* pFxInst, Vec3V_In vColourTint)
{
#if __BANK
	if (ptfxDebug::ms_disableColourTint)
	{
		return;
	}
#endif

	//ptxAssertf(pFxInst->GetIsPlaying()==false, "must be called before starting an effect");
	pFxInst->SetColourTint(vColourTint);
}

void ptfxWrapper::SetVelocityAdd(ptxEffectInst* pFxInst, Vec3V_In vVelocity)
{
#if __BANK
	if (ptfxDebug::ms_disableAddVelocity)
	{
		return;
	}
#endif

	// always force off for the moment - will only add in again if really needed
	static bool skipThis = true;
	if (skipThis)
	{
		return;
	}

	pFxInst->SetVelocityAdd(vVelocity);
}


} // namespace rage

