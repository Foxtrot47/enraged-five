//
// vfx/ptfxmanager.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXMANAGER_H
#define VFX_PTFXMANAGER_H


// includes (rage)
#include "atl/binmap.h"
#include "script/thread.h"
#include "vectormath/classes.h"

// includes (framework)
#include "vfx/ptfx/ptfxasset.h"

#include "PtFxIterator.h"


// defines
#define PTFX_MAX_OVERRIDE_INFOS		16


// namespaces 
namespace rage {


// forward declarations
class bkBank;
class ptxEffectInst;


// structures
struct ptfxOverrideInfo_s
{
	atHashWithStringNotFinal ptFxToOverrideHashName;
	atHashWithStringNotFinal ptFxToUseInsteadHashName;
	scrThreadId scriptThreadId;

};



// classes

// PURPOSE
//  
class ptfxManager
{
	friend class ptfxDebug;
	friend class ptfxReg;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		typedef CPtFxIterator iterator;
		typedef CPtFxConstIterator const_iterator;

		static void Init(int maxParticles, int maxEffectInsts, int maxTrails, int maxReg, int maxScript, int maxHistory, const char* pClipRegionFilename);
#if __BANK
		static void InitWidgets();
#endif
		static iterator begin();
		static iterator end();

		static void Shutdown();
		static void InitLevel();
		static void ShutdownLevel();
		static void Update(float deltaTime, Mat34V_ConstRef camMtx);
		static void UpdateThread(float deltaTime, const grcViewport* pViewport);
		static void RemovePtFx(ptxEffectInst* pFxInst);

		static void RemoveScript(scrThreadId scriptThreadId);

		static ptxEffectInst* GetTriggeredInst(atHashWithStringNotFinal effectRuleHashName);
		static ptxEffectInst* GetTriggeredInst(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName);
		static ptxEffectInst* GetRegisteredInst(const void* pOwnerClass, int classFxOffset, bool isOwnerAnEntity, atHashWithStringNotFinal effectRuleHashName, bool& justCreated, bool isOneShotSpecialCase=false, bool bypassReplayPauseCheck = false);
		static ptxEffectInst* GetRegisteredInst(const void* pOwnerClass, int classFxOffset, bool isOwnerAnEntity, atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, bool& justCreated, bool isOneShotSpecialCase=false, bool bypassReplayPauseCheck = false);

		static ptfxAssetStore& GetAssetStore() {return ms_ptfxAssetStore;}

		// particle effect overrides
		static void AddPtFxOverrideInfo(const char* pPtFxToOverride, const char* pPtFxToUseInstead, scrThreadId scriptThreadId);
		static void RemovePtFxOverrideInfo(const char* pPtFxToOverride, scrThreadId scriptThreadId);
		static void RemovePtFxOverrideInfo(scrThreadId scriptThreadId);
		static atHashWithStringNotFinal QueryPtFxOverride(atHashWithStringNotFinal ptFxHashName);
#if __BANK
		static void OutputPtFxOverrideInfos();
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	private: //////////////////////////

		static int ms_maxPoints;
		static int ms_maxInsts;
		static Mat34V ms_cameraMtx;

		static ptfxAssetStore ms_ptfxAssetStore;

		// particle effect overrides
		static atFixedArray<ptfxOverrideInfo_s, PTFX_MAX_OVERRIDE_INFOS> ms_ptfxOverrideInfos;


}; 

#define g_ParticleStore ptfxManager::GetAssetStore()

} // namespace rage


#endif // VFX_PTFXMANAGER_H


