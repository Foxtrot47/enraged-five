//
// vfx/ptfxasset.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxasset.h"

// includes (rage)
#include "grcore/debugdraw.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxinterface.h"
#include "rmptfx/ptxmanager.h"

// includes (framework)
#include "fwsys/fileexts.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxdebug.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// params
#if __DEV
PARAM(ptfxloadxml, "RMPTFX should load xml data");
PARAM(ptfxloadall, "loads all ptfx assets");
#endif

// namespaces 
namespace rage {



ptfxAssetStore::ptfxAssetStore() : fwAssetRscStore<ptxFxList, ptxFxListDef>("PtFxAssetStore", FXLIST_FILE_ID, CONFIGURED_FROM_FILE, 455, true, FXLIST_FILE_VERSION)
{
	m_safeToRemoveFxLists = true;
}

void ptfxAssetStore::InitLevel()
{
	// make sure all non-core assets have a dependency to the core asset
	// they may be sharing textures from the core asset so we don't want to be able to delete the core before the rest
	strLocalIndex coreIndex = FindSlotFromHashKey(atHashValue("core"));
	for (int index=0; index<GetSize(); index++)
	{
		if (IsValidSlot(strLocalIndex(index)) && index!=coreIndex.Get())
		{
			// only set the parent of assets that don't have a parent already
			if (GetParentIndex(strLocalIndex(index))==-1)
			{
				SetParentIndex(strLocalIndex(index), coreIndex.Get());
			}
		}
	}

	// disable particle defrag if we are likely to be editting particle data
#if __DEV
	if (PARAM_ptfxloadall.Get())
#endif
	{
		m_canDefragment = false;
	}
}

#if __BANK
void ptfxAssetStore::ShutdownLevel()
{
	OutputLoadedAssets();
	for (int index=0; index<GetSize(); index++)
	{
		// get the definition at this slot and check if there is data here
		ptxFxListDef* pFxListDef = GetSlot(strLocalIndex(index));
		if (pFxListDef && pFxListDef->m_pObject)
		{
			ptfxAssertf(fwAssetRscStore::GetNumRefs(strLocalIndex(index))==0, "particle asset has a non zero ref count when calling ShutdownLevel");
		}
	}
}
#endif

void ptfxAssetStore::Update(float UNUSED_PARAM(deltaTime))
{
	// remove any assets that have no references
	for (int i=0; i<GetSize(); i++)
	{
		ptxFxListDef* pFxListDef = GetSlot(strLocalIndex(i));
		if (pFxListDef && pFxListDef->m_pObject)
		{	
			if (strStreamingEngine::GetInfo().IsObjectReadyToDelete(GetStreamingIndex(strLocalIndex(i)), STRFLAG_LOADSCENE))
			{
				if (IsObjectRequired(strLocalIndex(i))==false && GetNumRefs(strLocalIndex(i))==0)
				{
					StreamingRemove(strLocalIndex(i));
				}
			}
		}
	}

#if __BANK
	// count the number of loaded particle assets
	ptfxDebug::ms_numFxAssets = 0;
	for (int i=0; i<GetSize(); i++)
	{
		ptxFxListDef* pFxListDef = GetSlot(strLocalIndex(i));
		if (pFxListDef && pFxListDef->m_pObject)
		{	
			ptfxDebug::ms_numFxAssets++;
		}
	}
#endif
}

void ptfxAssetStore::PrepForXmlLoading()
{
#if __DEV
	if (PARAM_ptfxloadxml.Get())
	{
		int indices = GetCount();
		for(int i = 0; i < indices; i++)
		{
			strStreamingInfo* pStreamingInfo = GetStreamingInfo(strLocalIndex(i));
			if (pStreamingInfo)
			{
				// Mark this asset as a dummy object (so that the streaming system calls Load() ) instead of a 
				// resource (which would make the streaming system call PlaceResource)
				Assertf(pStreamingInfo->GetStatus() == STRINFO_NOTLOADED, "Can't convert an already loaded/requested object into a dummy object!");
				pStreamingInfo->ClearFlag(GetStreamingIndex(strLocalIndex(i)), STRFLAG_INTERNAL_RESOURCE);
				pStreamingInfo->SetFlag(GetStreamingIndex(strLocalIndex(i)), STRFLAG_INTERNAL_DUMMY);
			}
		}
	}
#endif
}

bool ptfxAssetStore::Load(strLocalIndex DEV_ONLY(index), void* UNUSED_PARAM(pData), int UNUSED_PARAM(size))
{
#if __DEV
	ptxFxListDef* pFxListDef = GetSlot(index);
	strStreamingObjectName name = pFxListDef->m_name;
	const char* pAssetName = name.GetCStr();
	if (atHashValue(pAssetName)==atHashValue("core"))
	{
		pFxListDef->m_pObject = RMPTFXMGR.LoadFxList(pAssetName, FXLISTFLAG_SHAREABLE);
	}
	else
	{
		pFxListDef->m_pObject = RMPTFXMGR.LoadFxList(pAssetName, 0);
	}
#endif
	return true;
}

int ptfxAssetStore::GetDependencies(strLocalIndex index, strIndex* pIndices, int indexArraySize) const
{
	int count = 0;
	strLocalIndex parentIndex = GetParentIndex(index);
	if (parentIndex != -1)
	{
		AddDependencyOutput(pIndices, count, GetStreamingIndex(parentIndex), indexArraySize, GetStreamingIndex(strLocalIndex(indexArraySize)));
	}

	return count;
}

bool ptfxAssetStore::IsDefragmentCopyBlocked() const 
{
	// This is never blocked because the caller ensures that the particle thread is in a safe spot before calling 
	// the defragmentation system's update
	return false;
}

void ptfxAssetStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header)
{
// #if __DEV
// 	ptxFxListDef* pFxListDefTemp = GetSlot(index);
// 	if (pFxListDefTemp && pFxListDefTemp->m_name.GetHash()==atHashValue(ptfxDebug::ms_debugAssetName))
// 	{	
// 		ptfxAssertf(0, "placing the debug asset");
// 	}
// #endif

#if RMPTFX_BANK
	ptxDebug::sm_debugFxListInfo.Clear();
#endif

	// stream the particle asset into memory
	fwAssetRscStore::PlaceResource(index, map, header);


#if RMPTFX_BANK
	//ptxDebug::sm_debugFxListInfo.Output(pFxListDef->m_pObject);
#endif

	ptfxDebugf1("asset loaded:");
#if __BANK
	OutputAssetInfoLoaded(index);
#endif
}

void ptfxAssetStore::SetResource(strLocalIndex index, datResourceMap& map)
{
	// perform a post load step on the streamed in asset to fix up the data
	ptxFxListDef* pFxListDef = GetSlot(index);
	ptxFxList* pFxList = static_cast<ptxFxList*>(map.GetVirtualBase());

	const char* name = NULL;
#if !__FINAL
	name = GetName(index);
#endif
	datResource rsc(map, name, false); // So child functions can see if they are placing a resource or not

	if (atHashValue(pFxListDef->m_name)==atHashValue("core"))
	{
		RMPTFXMGR.PostLoadFxList(pFxList, pFxListDef->m_name, FXLISTFLAG_RESOURCED | FXLISTFLAG_SHAREABLE);
		strStreamingEngine::GetInfo().SetDoNotDefrag(GetStreamingIndex(index));
	}
	else
	{
		RMPTFXMGR.PostLoadFxList(pFxList, pFxListDef->m_name, FXLISTFLAG_RESOURCED);
	}

	if (!pFxList->AreAllShadersInSync())
	{
		strStreamingEngine::GetInfo().SetDoNotDefrag(GetStreamingIndex(index));
	}

	BaseClass::SetResource(index, map);
}

void ptfxAssetStore::Remove(strLocalIndex index)
{
#if __DEV
	ptxFxListDef* pFxListDefTemp = GetSlot(index);
	if (pFxListDefTemp && pFxListDefTemp->m_name.GetHash()==atHashValue(ptfxDebug::ms_debugAssetName))
	{	
		ptfxAssertf(0, "removing the debug asset");
	}
#endif

	// get the fx list definition at the required slot
	ptxFxListDef* pFxListDef = GetSlot(index);
	if (ptfxVerifyf(pFxListDef, "no definition data at this slot"))
	{
		// get the fx list from the definition
		if (ptfxVerifyf(pFxListDef->m_pObject, "no data in this definition"))
		{
			// remove the streamed in particle asset from memory
			ptxFxList* pFxList = pFxListDef->m_pObject;

			RMPTFXMGR.PreUnloadFxList(pFxList);
			fwAssetRscStore::Remove(index);
			RMPTFXMGR.PostUnloadFxList();

			ptfxDebugf1("asset unloaded:");
			ptfxDebugf1("  %16s", pFxListDef->m_name.TryGetCStr());
		}
	}
}

int ptfxAssetStore::GetNumRefs(strLocalIndex index) const
{
	int numRefs = fwAssetRscStore::GetNumRefs(index);

	if (!IsSafeToRemoveFxLists())
	{
		numRefs += 1; // Effectively boost all ref counts by 1 to make sure we never remove an fxlist while the ptx update thread is running
		// (in could cause deadlocks)
	}

	const ptxFxListDef* pFxListDef = GetSlot(index);
	if (pFxListDef && pFxListDef->m_pObject)
	{
		// the asset is loaded
		ptxFxList* pFxList = pFxListDef->m_pObject;
		numRefs += pFxList->GetRefCount();
	}

	return numRefs;
}

#if __BANK
void ptfxAssetStore::DebugDrawFxListReferences()
{
	// output fx list references
	grcDebugDraw::AddDebugOutputEx(false, "FxList References");	
	grcDebugDraw::AddDebugOutputEx(false, "    Name                      : Store  Ext  Fxl  IsRefed?");

	for(int index = 0; index < GetNumUsedSlots(); index++)
	{
		ptxFxListDef* pFxListDef = GetSlot(strLocalIndex(index));
		if (pFxListDef)
		{	
			ptxFxList* pFxList = pFxListDef->m_pObject;
			if (!pFxList)
			{
				continue;
			}

			// Find this fxlist using the iterator, so we can get the flags
			ptxFxListIterator fxListIterator = RMPTFXMGR.CreateFxListIterator();
			bool foundMatch = false;
			for (fxListIterator.Start((u32)FXLISTFLAG_ALL); !fxListIterator.AtEnd(); fxListIterator.Next())
			{
				if (fxListIterator.GetCurrFxList() == pFxList)
				{
					ptxFxListMap::Iterator& fxListMapIterator = fxListIterator.GetRawIter();
					u32 fxListFlags = fxListMapIterator.GetData().m_flags;

					bool isResourced = (fxListFlags & FXLISTFLAG_RESOURCED) != 0;
					bool isShareable = (fxListFlags & FXLISTFLAG_SHAREABLE) != 0;
					bool isLooseFxList = (fxListFlags & FXLISTFLAG_LOOSE) != 0;

					grcDebugDraw::AddDebugOutputEx(false, "    %20.20s (%s%s%s): %4d  %4d  %4d  %s",
						pFxListDef->m_name.GetCStr(),
						(isResourced ? "r" : " "), (isShareable ? "s" : " "), (isLooseFxList ? "l" : " "),
						GetNumRefs(strLocalIndex(index)),
						pFxList->FindExternalReferenceCount(), 
						pFxList->GetRefCount(),
						(pFxList->IsReferenced() ? "ref" : "noref"));

					foundMatch = true;

					break;
				}
			}
			if (!foundMatch)
			{
				grcDebugDraw::AddDebugOutputEx(false, "    %20.20s (-?-): %4d  %4d  %4d  %s",
					pFxListDef->m_name.GetCStr(),
					GetNumRefs(strLocalIndex(index)),
					pFxList->FindExternalReferenceCount(), 
					pFxList->GetRefCount(),
					(pFxList->IsReferenced() ? "ref" : "noref"));
			}
		}
	}

	// Now look for any non-resourced FXLists
	ptxFxListIterator fxListIterator = RMPTFXMGR.CreateFxListIterator();
	for (fxListIterator.Start((u32)FXLISTFLAG_NONRESOURCED); !fxListIterator.AtEnd(); fxListIterator.Next())
	{
		ptxFxListMap::Iterator& fxListMapIterator = fxListIterator.GetRawIter();
		u32 fxListFlags = fxListMapIterator.GetData().m_flags;
		ptxFxList* pFxList = fxListIterator.GetCurrFxList();
		const rage::atHashString& fxListName = fxListMapIterator.GetKey();
		if (!pFxList)
		{
			continue;
		}

		bool isResourced = (fxListFlags & FXLISTFLAG_RESOURCED) != 0;
		bool isShareable = (fxListFlags & FXLISTFLAG_SHAREABLE) != 0;
		bool isLooseFxList = (fxListFlags & FXLISTFLAG_LOOSE) != 0;

		grcDebugDraw::AddDebugOutputEx(false, "    %20.20s (%s%s%s): %4d  %4d  %4d  %s",
			fxListName.GetCStr(),
			(isResourced ? "r" : " "), (isShareable ? "s" : " "), (isLooseFxList ? "l" : " "),
			0,
			pFxList->FindExternalReferenceCount(), 
			pFxList->GetRefCount(),
			(pFxList->IsReferenced() ? "ref" : "noref"));
	}

	grcDebugDraw::AddDebugOutputEx(false, "");
}
#endif

#if __BANK
void ptfxAssetStore::AddRef(strLocalIndex index, strRefKind refKind)
{
// #if __DEV
// 	ptxFxListDef* pFxListDefTemp = GetSlot(index);
// 	if (pFxListDefTemp && pFxListDefTemp->m_name.GetHash()==atHashValue(ptfxDebug::ms_debugAssetName))
// 	{	
// 		ptfxAssertf(0, "adding a ref to the debug asset");
// 	}
// #endif

#if __DEV
	if (!PARAM_ptfxloadxml.Get())
#endif
	{
		fwAssetRscStore::AddRef(index, refKind);
	}

	ptxFxListDef* pFxListDef = GetSlot(index);
	if (ptfxVerifyf(pFxListDef && pFxListDef->m_pObject, "no definition data at this slot"))
	{	
		ptfxDebugf1("asset gameref added:");
		OutputAssetInfoRefChanged(index);
	}
}
#endif

#if __BANK
void ptfxAssetStore::RemoveRef(strLocalIndex index, strRefKind refKind)
{
// #if __DEV
// 	ptxFxListDef* pFxListDefTemp = GetSlot(index);
// 	if (pFxListDefTemp && pFxListDefTemp->m_name.GetHash()==atHashValue(ptfxDebug::ms_debugAssetName))
// 	{	
// 		ptfxAssertf(0, "removing a ref to the debug asset");
// 	}
// #endif

#if __DEV
	if (!PARAM_ptfxloadxml.Get())
#endif
	{
		fwAssetRscStore::RemoveRef(index, refKind);
	}

	ptxFxListDef* pFxListDef = GetSlot(index);
	if (ptfxVerifyf(pFxListDef && pFxListDef->m_pObject, "no definition data at this slot"))
	{	
		ptfxDebugf1("asset gameref removed:");
		OutputAssetInfoRefChanged(index);
	}
}
#endif

#if __BANK
void ptfxAssetStore::OutputAssetInfoLoaded(strLocalIndex index)
{
	ptxFxListDef* pFxListDef = GetSlot(index);
	if (pFxListDef)
	{	
		ptxFxList* pFxList = pFxListDef->m_pObject;

		if (pFxList)
		{
			int virtSize = 0;
			int physSize = 0;
			strStreamingInfo* pStreamingInfo = GetStreamingInfo(index);
			if (pStreamingInfo)
			{
				strIndex streamIndex = GetStreamingIndex(index);
				virtSize = pStreamingInfo->ComputeVirtualSize(streamIndex, true);
				physSize = pStreamingInfo->ComputePhysicalSize(streamIndex, true);
			}

			ptfxDebugf1("  %32s - gamerefs=%d \tfxrefs=%d \tkeep=%d \tmain=%8.2fk \tvram=%8.2fk",
				pFxListDef->m_name.GetCStr(), 
				fwAssetRscStore::GetNumRefs(index), 
				GetNumRefs(index)-fwAssetRscStore::GetNumRefs(index),
				IsObjectRequired(index), 
				virtSize/1024.0f, 
				physSize/1024.0f);
		}
	}
}
#endif

#if __BANK
void ptfxAssetStore::OutputAssetInfoRefChanged(strLocalIndex index)
{
	ptxFxListDef* pFxListDef = GetSlot(index);
	if (pFxListDef)
	{	
		ptxFxList* pFxList = pFxListDef->m_pObject;

		if (pFxList)
		{
			ptfxDebugf1("  %32s - gamerefs=%d \tfxrefs=%d \tkeep=%d",
				pFxListDef->m_name.GetCStr(), 
				fwAssetRscStore::GetNumRefs(index), 
				GetNumRefs(index)-fwAssetRscStore::GetNumRefs(index), 
				IsObjectRequired(index));
		}
	}
}
#endif

#if __BANK
void ptfxAssetStore::OutputAssetInfoDetailed(strLocalIndex index, ptfxAssetInfoDetailed& accumInfo)
{
	ptxFxListDef* pFxListDef = GetSlot(index);
	if (pFxListDef)
	{	
		ptxFxList* pFxList = pFxListDef->m_pObject;

		if (pFxList)
		{
			ptfxAssetInfoDetailed info;

			strStreamingInfo* pStreamingInfo = GetStreamingInfo(index);
			if (pStreamingInfo)
			{
				strIndex streamIndex = GetStreamingIndex(index);
				info.virtSize = pStreamingInfo->ComputeVirtualSize(streamIndex, true);
				info.physSize = pStreamingInfo->ComputePhysicalSize(streamIndex, true);
			}

#if RMPTFX_BANK
			info.keyframeEntryCount = 0;
			info.keyframeEntryMem = 0;
			info.keyframeEntryMax = 0;
			info.keyframeEntryDefault = 0;

			for (int i=0; i<pFxList->GetEffectRuleDictionary()->GetCount(); i++)
			{
				ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->GetEntry(i);
				if (pEffectRule)
				{
					pEffectRule->GetKeyframeEntryInfo(info.keyframeEntryCount, info.keyframeEntryMem, info.keyframeEntryMax, info.keyframeEntryDefault);
				}
			}

			for (int i=0; i<pFxList->GetEmitterRuleDictionary()->GetCount(); i++)
			{
				ptxEmitterRule* pEmitterRule = pFxList->GetEmitterRuleDictionary()->GetEntry(i);
				if (pEmitterRule)
				{
					pEmitterRule->GetKeyframeEntryInfo(info.keyframeEntryCount, info.keyframeEntryMem, info.keyframeEntryMax, info.keyframeEntryDefault);
				}
			}

			for (int i=0; i<pFxList->GetParticleRuleDictionary()->GetCount(); i++)
			{
				ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->GetEntry(i);
				if (pParticleRule)
				{
					pParticleRule->GetKeyframeEntryInfo(info.keyframeEntryCount, info.keyframeEntryMem, info.keyframeEntryMax, info.keyframeEntryDefault);
				}
			}
#endif

			info.numEffectRules = pFxList->GetEffectRuleDictionary()->GetCount();
			info.numEmitterRules = pFxList->GetEmitterRuleDictionary()->GetCount();
			info.numParticleRules = pFxList->GetParticleRuleDictionary()->GetCount();
			info.numTextures = pFxList->GetTextureDictionary()->GetCount();
			info.numModels = pFxList->GetModelDictionary()->GetCount();

			accumInfo.Accum(info);

			ptfxDebugf1("  %32s - %6d %6d %6d %8.2f %8.2f %6d %6d %6d %6d %6d %6d %8.2f %6d %6d", 
				pFxListDef->m_name.GetCStr(), 
				fwAssetRscStore::GetNumRefs(index), 
				GetNumRefs(index)-fwAssetRscStore::GetNumRefs(index), 
				IsObjectRequired(index), 
				info.virtSize/1024.0f, 
				info.physSize/1024.0f, 
				info.numEffectRules, 
				info.numEmitterRules, 
				info.numParticleRules,
				info.numTextures, 
				info.numModels,
				info.keyframeEntryCount,
				info.keyframeEntryMem/1024.0f,
				info.keyframeEntryMax,
				info.keyframeEntryDefault);
		}
	}
}
#endif

#if __BANK
void ptfxAssetStore::RemoveDefaultKeyframes()
{
	ptfxAssetInfoDetailed accumInfo;
	for (int index=0; index<GetSize(); index++)
	{
		ptxFxListDef* pFxListDef = GetSlot(strLocalIndex(index));
		if (pFxListDef)
		{	
			ptxFxList* pFxList = pFxListDef->m_pObject;

			if (pFxList)
			{
#if RMPTFX_BANK
				for (int i=0; i<pFxList->GetEffectRuleDictionary()->GetCount(); i++)
				{
					ptxEffectRule* pEffectRule = pFxList->GetEffectRuleDictionary()->GetEntry(i);
					if (pEffectRule)
					{
						pEffectRule->RemoveDefaultKeyframes();
					}
				}

				for (int i=0; i<pFxList->GetEmitterRuleDictionary()->GetCount(); i++)
				{
					ptxEmitterRule* pEmitterRule = pFxList->GetEmitterRuleDictionary()->GetEntry(i);
					if (pEmitterRule)
					{
						pEmitterRule->RemoveDefaultKeyframes();
					}
				}

				for (int i=0; i<pFxList->GetParticleRuleDictionary()->GetCount(); i++)
				{
					ptxParticleRule* pParticleRule = pFxList->GetParticleRuleDictionary()->GetEntry(i);
					if (pParticleRule)
					{
						pParticleRule->RemoveDefaultKeyframes();
					}
				}
#endif
			}
		}
	}

#if RMPTFX_BANK
	RMPTFXMGR.RemoveDefaultKeyframes();
#endif
}
#endif

#if __BANK
void ptfxAssetStore::OutputLoadedAssets()
{
	// output info about the loaded particle assets
	ptfxDebugf1("assets loaded:");

	//							   123456 123456 123456 12345678 12345678 123456 123456 123456 123456 123456 123456 12345678 123456 123456
	ptfxDebugf1("\t\t\t\t\t name     game     fx   keep     main     vram effect   emit    ptx    tex  model kf_cnt   kf_mem kf_max kf_def");
	ptfxDebugf1("\t\t\t\t\t          refs   refs             mem");

	ptfxAssetInfoDetailed accumInfo;
	for (int index=0; index<GetSize(); index++)
	{
		OutputAssetInfoDetailed(strLocalIndex(index), accumInfo);
	}

#if RMPTFX_BANK
	RMPTFXMGR.GetKeyframeEntryInfo(accumInfo.keyframeEntryCount, accumInfo.keyframeEntryMem, accumInfo.keyframeEntryMax, accumInfo.keyframeEntryDefault);
#endif

	ptfxDebugf1("  %32s -                      %8.2f %8.2f %6d %6d %6d %6d %6d %6d %8.2f %6d %6d", 
		"total", 
		accumInfo.virtSize/1024.0f, 
		accumInfo.physSize/1024.0f, 
		accumInfo.numEffectRules, 
		accumInfo.numEmitterRules, 
		accumInfo.numParticleRules,
		accumInfo.numTextures, 
		accumInfo.numModels,
		accumInfo.keyframeEntryCount,
		accumInfo.keyframeEntryMem/1024.0f,
		accumInfo.keyframeEntryMax,
		accumInfo.keyframeEntryDefault);
}
#endif



} // namespace rage




