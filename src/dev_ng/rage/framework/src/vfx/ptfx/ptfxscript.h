//
// vfx/ptfxscript.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXSCRIPT_H
#define VFX_PTFXSCRIPT_H


// includes (framework)
#include "entity/extensiblebase.h"
#include "fwutil/rtti.h"

// includes (rage)
#include "atl/array.h"
#include "vectormath/classes.h"


// namespaces 
namespace rage {


// forward declarations
class fwEntity;
class ptfxManager;
class ptfxRegInst;
class ptxEffectInst;

// defines
#define SCRIPT_REG_OWNER_CLASS (0x10000000)


// structures
struct ptfxScriptInfo : public fwExtensibleBase
{
	int id;
	bool isActive;	
	bool removeWhenDestroyed;

	DECLARE_RTTI_DERIVED_CLASS(ptfxScriptInfo, fwExtensibleBase);
};


// classes

// PURPOSE
//  
class ptfxScript
{
	friend class ptfxDebug;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(int maxSize);	
		static void Update();	

		static bool Trigger(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, fwEntity* pParentEntity, int boneIndex, Vec3V_In vOffsetPos, Vec3V_In vOffsetRot, float userScale=1.0f, u8 invertAxes=0);
		static bool Trigger(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, fwEntity* pParentEntity, int boneIndex, Mat34V_In mOffset, float userScale=1.0f, u8 invertAxes=0); 
		static void SetTriggeredColourTint(float r, float g, float b);
		static void SetTriggeredAlphaTint(float a);
		static void SetTriggeredScale(float scale);
		static void SetTriggeredEmitterSize(Vec3V_In pOverrideSize);
		static void SetTriggeredForceVehicleInteriorFX(bool bIsVehicleInterior);

		static void GetTriggeredColourTint(float& r, float& g, float& b); 
		static void GetTriggeredAlphaTint(float& a);

		static int Start(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, fwEntity* pParentEntity, int boneIndex, Vec3V_In vOffsetPos, Vec3V_In vOffsetRot, float userScale=1.0f, u8 invertAxes=0);
		static bool Stop(int ptfxScriptId, bool calledFromRemove=false);
		static bool Remove(int ptfxScriptId);
		static bool Destroy(int ptfxScriptId);
		static void SetRemoveWhenDestroyed(int ptfxScriptId, bool val);
		static bool DoesExist(int ptfxScriptId);
		static bool UpdateOffset(int ptfxScriptId, Vec3V_In vOffsetPos, Vec3V_In vOffsetRot);
		static bool UpdateOffset(int ptfxScriptId, Mat34V_In vMtx);	
		static bool UpdateEvo(int ptfxScriptId, const char* evoName, float evoVal);
		static bool UpdateEvo(int ptfxScriptId, u32 evoHash, float evoVal);
		static bool UpdateEvo(int ptfxScriptId, u16 evoID, float evoVal);
		static bool UpdateColourTint(int ptfxScriptId, Vec3V_In vColourTint);
		static bool UpdateAlphaTint(int ptfxScriptId, float a);
		static bool UpdateScale(int ptfxScriptId, float scale);
		static bool UpdateEmitterSize(int ptfxScriptId, bool isOverridden, Vec3V_In pvOverrideSize);
		static bool UpdateFarClipDist(int ptfxScriptId, float farClipDist);
		static bool UpdateForceVehicleInteriorFX(int ptfxScriptId, bool bIsVehicleInterior);

		static const ptfxScriptInfo* FindInfo(int ptfxScriptId);

	private: //////////////////////////
		static void ResetTriggerEffect(); 
		static void UpdateTriggerEffectSettings(ptxEffectInst* pFxInst); 

		static ptfxScriptInfo* GetId();
		static ptfxScriptInfo* Find(int ptfxScriptId);
		static ptfxRegInst*	FindRegFxInst(int ptfxScriptId);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	private: //////////////////////////

		static atArray<ptfxScriptInfo> ms_infos;

		static bool	 ms_triggeredForceVehicleInterior;
		static float ms_triggeredColourTintR;
		static float ms_triggeredColourTintG;
		static float ms_triggeredColourTintB;
		static float ms_triggeredAlphaTint;
		static float ms_triggeredScale;
		static float ms_triggeredEmitterSizeX;
		static float ms_triggeredEmitterSizeY;
		static float ms_triggeredEmitterSizeZ;
}; 


} // namespace rage

#endif // VFX_PTFXSCRIPT_H


