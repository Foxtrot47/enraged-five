//
// vfx/ptfxscript.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxscript.h"

// includes (rage)
#include "rmptfx/ptxeffectinst.h"
#include "system/param.h"

// includes (framework)
#include "fwscript/scriptguid.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxattach.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxdebug.h"
#include "vfx/ptfx/ptfxmanager.h"
#include "vfx/ptfx/ptfxreg.h"
#include "vfx/ptfx/ptfxwrapper.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// namespaces 
namespace rage {


INSTANTIATE_RTTI_CLASS(ptfxScriptInfo,0x77FDC2C0);


// static member variables
atArray<ptfxScriptInfo> ptfxScript::ms_infos;
float ptfxScript::ms_triggeredColourTintR;
float ptfxScript::ms_triggeredColourTintG;
float ptfxScript::ms_triggeredColourTintB;
float ptfxScript::ms_triggeredAlphaTint;
float ptfxScript::ms_triggeredScale;
float ptfxScript::ms_triggeredEmitterSizeX;
float ptfxScript::ms_triggeredEmitterSizeY;
float ptfxScript::ms_triggeredEmitterSizeZ;
bool ptfxScript::ms_triggeredForceVehicleInterior = false;


// code
void ptfxScript::Init(int maxSize)
{
	ms_infos.Reset();
	ms_infos.Reserve(maxSize);
	ms_infos.Resize(maxSize);

	// initialise the scripted effect infos
	for (int i=0; i<ms_infos.GetCapacity(); i++)	
	{
		ms_infos[i].id = 0;
		ms_infos[i].isActive = false;
		ms_infos[i].removeWhenDestroyed = false;
	}

	// initialise the triggered fx settings
	ms_triggeredColourTintR = -1.0f;
	ms_triggeredColourTintG = -1.0f;
	ms_triggeredColourTintB = -1.0f;
	ms_triggeredAlphaTint = -1.0f;
	ms_triggeredScale = -1.0f;
	ms_triggeredEmitterSizeX = -1.0f;
	ms_triggeredEmitterSizeY = -1.0f;
	ms_triggeredEmitterSizeZ = -1.0f;
}

void ptfxScript::Update()
{
#if __BANK
	ptfxDebug::ms_numFxScripted = 0;
#endif

	for (int i=0; i<ms_infos.GetCapacity(); i++)
	{
		if (ms_infos[i].isActive)
		{
			bool justCreated = false;			
			ptfxReg::Register((void*)SCRIPT_REG_OWNER_CLASS, ms_infos[i].id, false, atHashWithStringNotFinal((u32)0), atHashWithStringNotFinal((u32)0), justCreated);

#if __BANK
			ptfxDebug::ms_numFxScripted++;
#endif
		}
	}
}

bool ptfxScript::Trigger(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, fwEntity* pParentEntity, int boneIndex, Vec3V_In vOffsetPos, Vec3V_In vOffsetRot, float userScale, u8 invertAxes)
{
	// try to create an effect instance
	ptxEffectInst* pFxInst = ptfxManager::GetTriggeredInst(effectRuleHashName, fxListHashName);
	if (pFxInst)
	{
		// check that this effect does not infinitely loop
		if (pFxInst->GetEffectRule()->GetIsInfinitelyLooped())
		{	
			ptfxAssertf(0, "called with an infinitely looping effect");
			ptfxWrapper::StopEffectInst(pFxInst);
			return false;
		}

		// effect instance created ok - set up position and orientation
		if (pParentEntity)
		{
			//Mat34V boneMtx;
			//g_pPtfxCallbacks->GetMatrixFromBoneIndex(boneMtx, pParentEntity, boneIndex);
			//pFxInst->SetBaseMtx(boneMtx);

			ptfxAttach::Attach(pFxInst, pParentEntity, boneIndex);
		}
		else
		{
			pFxInst->SetBaseMtx(Mat34V(V_IDENTITY));
		}

		pFxInst->SetOffsetPos(vOffsetPos);								
		pFxInst->SetOffsetRot(vOffsetRot);

		pFxInst->SetInvertAxes(invertAxes);

		UpdateTriggerEffectSettings(pFxInst); 

		// trigger it
		pFxInst->SetUserZoom(userScale);
		pFxInst->Start();

		ResetTriggerEffect(); 
		return true;
	}

	ResetTriggerEffect(); 
	return false;
}

bool ptfxScript::Trigger(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, fwEntity* pParentEntity, int boneIndex, Mat34V_In mOffset, float userScale, u8 invertAxes)
{
	// try to create an effect instance
	ptxEffectInst* pFxInst = ptfxManager::GetTriggeredInst(effectRuleHashName, fxListHashName);
	if (pFxInst)
	{
		// check that this effect does not infinitely loop
		if (pFxInst->GetEffectRule()->GetIsInfinitelyLooped())
		{	
			ptfxAssertf(0, "called with an infinitely looping effect");
			ptfxWrapper::StopEffectInst(pFxInst);
			return false;
		}

		// effect instance created ok - set up position and orientation
		if (pParentEntity)
		{
			//Mat34V boneMtx;
			//g_pPtfxCallbacks->GetMatrixFromBoneIndex(boneMtx, pParentEntity, boneIndex);
			//pFxInst->SetBaseMtx(boneMtx);

			ptfxAttach::Attach(pFxInst, pParentEntity, boneIndex);
		}
		else
		{
			pFxInst->SetBaseMtx(Mat34V(V_IDENTITY));
		}

		pFxInst->SetOffsetMtx(mOffset);								

		pFxInst->SetInvertAxes(invertAxes);

		UpdateTriggerEffectSettings(pFxInst); 

		// trigger it
		pFxInst->SetUserZoom(userScale);
		pFxInst->Start();

		return true;
	}

	ResetTriggerEffect(); 

	return false;
}


void ptfxScript::ResetTriggerEffect()
{
	// reset the triggered fx settings
	ms_triggeredColourTintR = -1.0f;
	ms_triggeredColourTintG = -1.0f;
	ms_triggeredColourTintB = -1.0f;
	ms_triggeredAlphaTint = -1.0f;
	ms_triggeredScale = -1.0f;
	ms_triggeredEmitterSizeX = -1.0f;
	ms_triggeredEmitterSizeY = -1.0f;
	ms_triggeredEmitterSizeZ = -1.0f;
	ms_triggeredForceVehicleInterior = false;
}

void ptfxScript::UpdateTriggerEffectSettings(ptxEffectInst* pFxInst)
{
	// set the triggered fx settings
	if (ms_triggeredColourTintR>=0.0f)
	{
		ptfxWrapper::SetColourTint(pFxInst, Vec3V(ms_triggeredColourTintR, ms_triggeredColourTintG, ms_triggeredColourTintB));
		ms_triggeredColourTintR = -1.0f;
		ms_triggeredColourTintG = -1.0f;
		ms_triggeredColourTintB = -1.0f;
	}

	if (ms_triggeredAlphaTint>=0.0f)
	{
		pFxInst->SetAlphaTint(ms_triggeredAlphaTint);
		ms_triggeredAlphaTint = -1.0f;
	}

	if (ms_triggeredScale>=0.0f)
	{
		pFxInst->SetUserZoom(ms_triggeredScale);
		ms_triggeredScale = -1.0f;
	}

	if (ms_triggeredEmitterSizeX>=0.0f)
	{
		Vec3V overrideSize(ms_triggeredEmitterSizeX, ms_triggeredEmitterSizeY, ms_triggeredEmitterSizeZ);
		pFxInst->SetOverrideCreationDomainSize(overrideSize);
		ms_triggeredEmitterSizeX = -1.0f;
		ms_triggeredEmitterSizeY = -1.0f;
		ms_triggeredEmitterSizeZ = -1.0f;
	}
	
	if(ms_triggeredForceVehicleInterior)
	{
		pFxInst->SetFlag(PTFX_EFFECTINST_FORCE_VEHICLE_INTERIOR);
	}
	else
	{
		pFxInst->ClearFlag(PTFX_EFFECTINST_FORCE_VEHICLE_INTERIOR);
	}

}

void ptfxScript::SetTriggeredColourTint(float r, float g, float b)
{
	ms_triggeredColourTintR = r;
	ms_triggeredColourTintG = g;
	ms_triggeredColourTintB = b;
}

void ptfxScript::SetTriggeredAlphaTint(float a)
{
	ms_triggeredAlphaTint = a;
}

void ptfxScript::SetTriggeredScale(float scale)
{
	ms_triggeredScale = scale;
}

void ptfxScript::SetTriggeredEmitterSize(Vec3V_In vOverrideSize)
{
	ms_triggeredEmitterSizeX = vOverrideSize.GetXf();
	ms_triggeredEmitterSizeY = vOverrideSize.GetYf();
	ms_triggeredEmitterSizeZ = vOverrideSize.GetZf();
}

void ptfxScript::SetTriggeredForceVehicleInteriorFX(bool bIsVehicleInterior)
{
	ms_triggeredForceVehicleInterior = bIsVehicleInterior;
}

void ptfxScript::GetTriggeredColourTint(float& r, float& g, float& b)
{
	r = ms_triggeredColourTintR;
	g = ms_triggeredColourTintG;
	b = ms_triggeredColourTintB;
}

void ptfxScript::GetTriggeredAlphaTint(float& a)
{
	a = ms_triggeredAlphaTint;
}

int ptfxScript::Start(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, fwEntity* pParentEntity, int boneIndex, Vec3V_In vOffsetPos, Vec3V_In vOffsetRot, float userScale, u8 invertAxes)
{
	// try to get a new scripted effect id
	ptfxScriptInfo* pScriptedPtFxInfo = GetId();
	if (pScriptedPtFxInfo)
	{
		bool justCreated = false;
		ptxEffectInst* pFxInst = ptfxReg::Register((void*)SCRIPT_REG_OWNER_CLASS, pScriptedPtFxInfo->id, false, effectRuleHashName, fxListHashName, justCreated);
		if (pFxInst)
		{
			// check that this is newly created
			if (justCreated==false)
			{
				ptfxAssertf(0, "WARNING: Start has registered an existing effect (%s)", pFxInst->GetEffectRule() ? pFxInst->GetEffectRule()->GetName() : "<effect rule name not available>");
				return 0;
			}

			// effect instance created ok - set up position and orientation
			if (pParentEntity)
			{
				//Mat34V boneMtx;
				//g_pPtfxCallbacks->GetMatrixFromBoneIndex(boneMtx, pParentEntity, boneIndex);
				//pFxInst->SetBaseMtx(boneMtx);

				ptfxAttach::Attach(pFxInst, pParentEntity, boneIndex);
			}
			else
			{
				pFxInst->SetBaseMtx(Mat34V(V_IDENTITY));
			}

			pFxInst->SetOffsetPos(vOffsetPos);								
			pFxInst->SetOffsetRot(vOffsetRot);

			pFxInst->SetInvertAxes(invertAxes);

			// start it
			pFxInst->SetUserZoom(userScale);
			pFxInst->Start();

			ptfxDebugf1("ptfxScript - start effect %d", pScriptedPtFxInfo->id);
			return pScriptedPtFxInfo->id;
		}
		else
		{
			ptfxDebugf1("ptfxScript - start effect failed, couldn't register effect instance [%s] from fxlist [%s]", effectRuleHashName.GetCStr(), fxListHashName.GetCStr());
			Stop(pScriptedPtFxInfo->id);
			return 0;
		}
	}

	ptfxDebugf1("ptfxScript - start effect failed, couldn't retrieve ptfxScriptInfo");
	return 0;
}

bool ptfxScript::Stop(int ptfxScriptId, bool calledFromRemove)
{
	ptfxScriptInfo* pScriptInfo = Find(ptfxScriptId);
	if (pScriptInfo)
	{
		if (calledFromRemove==false)
		{
			ptfxAssertf(pScriptInfo->isActive, "trying to stop an effect that hasn't started yet");
		}
		pScriptInfo->id = 0;
		pScriptInfo->isActive = false;
		pScriptInfo->removeWhenDestroyed = false;
		fwScriptGuid::DeleteGuid(*pScriptInfo);

		if (calledFromRemove==false)
		{
			ptfxDebugf1("ptfxScript - stop effect %d", ptfxScriptId);
		}
		return true;
	}

	if (calledFromRemove==false)
	{
		ptfxAssertf(0, "trying to stop an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	}
	return false;
}

bool ptfxScript::Remove(int ptfxScriptId)
{	
	// stop the effect
	if (Stop(ptfxScriptId, true))
	{
		// remove the effect
		ptfxRegInst* pRegFxInst = ptfxReg::Find((void*)SCRIPT_REG_OWNER_CLASS, ptfxScriptId);
		if (pRegFxInst)
		{
			// remove the effect
			ptfxManager::RemovePtFx(pRegFxInst->m_pFxInst);

			ptfxDebugf1("ptfxScript - remove effect %d", ptfxScriptId);
			return true;
		}
	}

	ptfxAssertf(0, "trying to remove an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::Destroy(int ptfxScriptId)
{
	ptfxScriptInfo* pScriptInfo = Find(ptfxScriptId);
	if (pScriptInfo)
	{
		if (pScriptInfo->removeWhenDestroyed)
		{
			return Remove(ptfxScriptId);
		}
		else
		{
			return Stop(ptfxScriptId);
		}
	}

	ptfxAssertf(0, "trying to set remove when destroyed on an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

void ptfxScript::SetRemoveWhenDestroyed(int ptfxScriptId, bool val)
{
	ptfxScriptInfo* pScriptInfo = Find(ptfxScriptId);
	if (pScriptInfo)
	{
		pScriptInfo->removeWhenDestroyed = val;
		return;
	}

	ptfxAssertf(0, "trying to set remove when destroyed on an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
}

bool ptfxScript::DoesExist(int ptfxScriptId)
{
	return fwScriptGuid::IsType<ptfxScriptInfo>(ptfxScriptId);
}

bool ptfxScript::UpdateOffset(int ptfxScriptId, Vec3V_In vOffsetPos, Vec3V_In vOffsetRot)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		if (pRegFxInst->m_pFxInst)
		{
			// update the position and orientation
			pRegFxInst->m_pFxInst->SetOffsetPos(vOffsetPos);								
			pRegFxInst->m_pFxInst->SetOffsetRot(vOffsetRot);

			ptfxDebugf1("ptfxScript - update effect offset %d", ptfxScriptId);
			return true;
		}
#if __ASSERT
		else
		{
			ptfxAssertf(0, "the registered inst is valid but it's fx inst is not");
		}
#endif
	}

	ptfxAssertf(0, "trying to update the offset of an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateOffset(int ptfxScriptId, Mat34V_In vMtx)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		if (pRegFxInst->m_pFxInst)
		{
			// update the position and orientation
			pRegFxInst->m_pFxInst->SetOffsetMtx(vMtx); 							

			ptfxDebugf1("ptfxScript - update effect offset %d", ptfxScriptId);
			return true;
		}
#if __ASSERT
		else
		{
			ptfxAssertf(0, "the registered inst is valid but it's fx inst is not");
		}
#endif
	}

	ptfxAssertf(0, "trying to update the offset of an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}



bool ptfxScript::UpdateEvo(int ptfxScriptId, const char* evoName, float evoVal)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// evolve the effect
		pRegFxInst->m_pFxInst->SetEvoValueFromHash(atHashValue(evoName), evoVal);

		ptfxDebugf1("ptfxScript - update effect evo %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to evolve an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateEvo(int ptfxScriptId, u32 evoHash, float evoVal)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// evolve the effect
		pRegFxInst->m_pFxInst->SetEvoValueFromHash(atHashValue(evoHash), evoVal);

		ptfxDebugf1("ptfxScript - update effect evo %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to evolve an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateEvo(int ptfxScriptId, u16 evoID, float evoVal)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// evolve the effect
		pRegFxInst->m_pFxInst->SetEvoValueFromHash16(evoID, evoVal);

		ptfxDebugf1("ptfxScript - update effect evo %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to evolve an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}



bool ptfxScript::UpdateColourTint(int ptfxScriptId, Vec3V_In vColourTint)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// evolve the effect
		ptfxWrapper::SetColourTint(pRegFxInst->m_pFxInst, vColourTint);

		ptfxDebugf1("ptfxScript - update effect colour tint %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to set the colour tint an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateAlphaTint(int ptfxScriptId, float a)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// evolve the effect
		ptfxWrapper::SetAlphaTint(pRegFxInst->m_pFxInst, a);

		ptfxDebugf1("ptfxScript - update effect alpha tint %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to set the alpha tint an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateScale(int ptfxScriptId, float scale)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// evolve the effect
		pRegFxInst->m_pFxInst->SetUserZoom(scale);

		ptfxDebugf1("ptfxScript - update effect scale %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to scale an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateEmitterSize(int ptfxScriptId, bool isOverridden, Vec3V_In vOverrideSize)
{
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		// update the emitter size
		if (isOverridden)
		{
			pRegFxInst->m_pFxInst->SetOverrideCreationDomainSize(vOverrideSize);
		}
		else
		{
			pRegFxInst->m_pFxInst->SetOverrideCreationDomainSize(Vec3V(V_ZERO));
		}

		ptfxDebugf1("ptfxScript - update effect emitter size %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to update the emitter size of an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}

bool ptfxScript::UpdateFarClipDist(int ptfxScriptId, float farClipDist)
{	
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		pRegFxInst->m_pFxInst->SetOverrideFarClipDist(farClipDist);

		ptfxDebugf1("ptfxScript - update effect far clip dist %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to set the far clip distance an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;
}


bool ptfxScript::UpdateForceVehicleInteriorFX(int ptfxScriptId, bool bIsVehicleInterior)
{
	ptfxRegInst* pRegFxInst = FindRegFxInst(ptfxScriptId);
	if (pRegFxInst)
	{
		if(bIsVehicleInterior)
		{
			pRegFxInst->m_pFxInst->SetFlag(PTFX_EFFECTINST_FORCE_VEHICLE_INTERIOR);
		}
		else
		{
			pRegFxInst->m_pFxInst->ClearFlag(PTFX_EFFECTINST_FORCE_VEHICLE_INTERIOR);
		}

		ptfxDebugf1("ptfxScript - update Force Vehicle Interior FX %d", ptfxScriptId);
		return true;
	}

	ptfxAssertf(0, "trying to update the force vehicle interior flag for an effect (id=%d) that wasn't found (start may have failed or the effect may have been stopped already)", ptfxScriptId);
	return false;

}

ptfxScriptInfo*	ptfxScript::GetId()
{
	for (int i=0; i<ms_infos.GetCapacity(); i++)
	{
		if (ms_infos[i].isActive==false)
		{
			ms_infos[i].isActive = true;
			ms_infos[i].removeWhenDestroyed = false;
			ms_infos[i].id = fwScriptGuid::CreateGuid(ms_infos[i]);
			return &ms_infos[i];
		}
	}

	ptfxWarningf("cannot get a scripted ptfx");
	return NULL;
}

const ptfxScriptInfo* ptfxScript::FindInfo(int ptfxScriptId)
{
	return Find(ptfxScriptId);
}

ptfxScriptInfo*	ptfxScript::Find(int ptfxScriptId)
{
	return fwScriptGuid::FromGuid<ptfxScriptInfo>(ptfxScriptId);
}

ptfxRegInst* ptfxScript::FindRegFxInst(int ptfxScriptId)
{
	ptfxScriptInfo* pScriptedPtFxInfo = Find(ptfxScriptId);
	if (pScriptedPtFxInfo)
	{
		ptfxAssertf(pScriptedPtFxInfo->isActive, "trying to get the registered fx inst of an inactive script fx");
		return ptfxReg::Find((void*)SCRIPT_REG_OWNER_CLASS, ptfxScriptId);
	}

	return NULL;
}



} // namespace rage

