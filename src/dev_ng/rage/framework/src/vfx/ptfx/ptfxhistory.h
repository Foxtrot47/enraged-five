//
// vfx/ptfxhistory.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXHISTORY_H
#define VFX_PTFXHISTORY_H


// includes (framework)
#include "fwtl/regdrefs.h"

// includes (rage)
#include "atl/array.h"
#include "atl/hashstring.h"
#include "vectormath/classes.h"


// namespaces 
namespace rage {


// forward declarations
class ptfxManager;


// structures
struct ptfxHistoryInfo
{
	Vec3V vPosLcl;
	fwRegdRef<class fwEntity> regdEntity;
	bool isValid;
	bool hasEntity;
	u32 fxHashName;
	u32 subType;
	float aliveTime;
	float elapsedTime;
};


// classes

// PURPOSE
//  
class ptfxHistory
{
	friend class ptfxDebug;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(int maxSize);	
		static void Update(float deltaTime);	
		static void Add(const atHashWithStringNotFinal fxHashName, const u32 subType, fwEntity* pEntity, Vec3V_In vPosWld, const float aliveTime);
		static bool Check(const atHashWithStringNotFinal fxHashName, const u32 subType, fwEntity* pEntity, Vec3V_In vPosWld, const float range);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	private: //////////////////////////

		static atArray<ptfxHistoryInfo> ms_infos;

	
}; 


} // namespace rage

#endif // VFX_PTFXHISTORY_H


