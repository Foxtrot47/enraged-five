//
// vfx/ptfxflags.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXFLAGS_H
#define VFX_PTFXFLAGS_H


// namespaces 
namespace rage {


// forward declarations
class ptxEffectInst;


// classes

// PURPOSE
//  
class ptfxFlags
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// reserved flag interface
		static bool IsAnyUserReservedFlagSet(ptxEffectInst* pFxInst);
		static void SetUserReservedFlag(ptxEffectInst* pFxInst, ptxEffectFlags flag, bool isManualDraw=false);
		static void ClearUserReservedFlag(ptxEffectInst* pFxInst, ptxEffectFlags flag, bool isManualDraw=false);
		static void ClearKeepReservedFlag(ptxEffectInst* pFxInst);
	
}; 


} // namespace rage

#endif // VFX_PTFXFLAGS_H


