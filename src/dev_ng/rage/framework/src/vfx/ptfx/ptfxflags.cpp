//
// vfx/ptfxflags.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//


// includes (rage) - needs to be first include for ps3 compile to work (?)
#include "rmptfx/ptxeffectinst.h"

// includes (us)
#include "vfx/ptfx/ptfxflags.h"

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// namespaces 
namespace rage {


// code

bool ptfxFlags::IsAnyUserReservedFlagSet(ptxEffectInst* pFxInst)
{
	return (pFxInst->GetFlag(PTXEFFECTFLAG_USER1) || 
			pFxInst->GetFlag(PTXEFFECTFLAG_USER2) || 
			pFxInst->GetFlag(PTXEFFECTFLAG_USER3));
}

void ptfxFlags::SetUserReservedFlag(ptxEffectInst* pFxInst, ptxEffectFlags ptfxFlag, bool isManualDraw)
{
	// check this flag is clear initially
	ptfxAssertf(pFxInst->GetFlag(ptfxFlag)==0, "effect inst is already reserved with this flag");
	if (isManualDraw)
	{
		ptfxAssertf(pFxInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW)==0, "effect inst is already set to draw manually");
	}

	// reserve this effect inst
	pFxInst->SetFlag(PTXEFFECTFLAG_KEEP_RESERVED);

	// tell rmptfx that we are manually drawing this effect
	if (isManualDraw)
	{
		pFxInst->SetFlag(PTXEFFECTFLAG_MANUAL_DRAW);
	}

	// set the type of reservation 
	pFxInst->SetFlag(ptfxFlag);

}

void ptfxFlags::ClearUserReservedFlag(ptxEffectInst* pFxInst, ptxEffectFlags ptfxFlag, bool isManualDraw)
{
	// check this flag is set initially
	ptfxAssertf(pFxInst->GetFlag(ptfxFlag), "effect inst is not reserved with this flag");
	ptfxAssertf(pFxInst->GetFlag(PTXEFFECTFLAG_KEEP_RESERVED), "effect inst is already reserved in some way");
#if __ASSERT
	if (isManualDraw)
	{
		ptfxAssertf(pFxInst->GetFlag(PTXEFFECTFLAG_MANUAL_DRAW), "effect inst is already set to draw manually");
	}
#endif

	// clear the reservation type
	pFxInst->ClearFlag(ptfxFlag);

	// tell rmptfx that we are no longer manually drawing this effect
	if (isManualDraw)
	{
		pFxInst->ClearFlag(PTXEFFECTFLAG_MANUAL_DRAW);
	}

	// clear the reserved flag if we aren't still registered
	ClearKeepReservedFlag(pFxInst);
}

void ptfxFlags::ClearKeepReservedFlag(ptxEffectInst* pFxInst)
{
	if (pFxInst->GetFlag(PTXEFFECTFLAG_USER1)==0 && 
		pFxInst->GetFlag(PTXEFFECTFLAG_USER2)==0 && 
		pFxInst->GetFlag(PTXEFFECTFLAG_USER3)==0)
	{
		pFxInst->ClearFlag(PTXEFFECTFLAG_KEEP_RESERVED);
	}
}



} // namespace rage

