// 
// ptfx/PtFxIterator_inline.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FX_PTFXITERATOR_INLINE_H 
#define FX_PTFXITERATOR_INLINE_H 

#ifndef FX_PTFXITERATOR_H
#	error "Must be included after PtFxIterator.h!"
#endif

//**************************************************************************************************************************************
//* Warning 556 explanation:
//* 
//* The particle iterator base is templated to eliminate code duplication between the iterator and const_iterator
//* classes. STL compliant iterators support conversion from iterator to const_iterator but do not necessarily define
//* the inverse conversion. This limits the effectiveness of other versions of iterators, such as const_iterator. (Also
//* reverse_iterator and const_reverse_iterator, but these do not apply here)
//* 
//* This class supports conversion in both directions. Conversion is handled by the following 2 functions:
//* 
//* template<typename FxInstType>
//* class CPtFxIteratorBase {
//*		CPtFxIteratorBase(const CPtFxIteratorBase<const FxInstType> &ci);	//Allows conversion: const_iterator -> iterator
//*		operator CPtFxIteratorBase<const FxInstType>();						//Allows conversion: iterator -> const_iterator
//* };
//* 
//* However, compiling this normally in the PS3SNC configuration produces the following warning:
//* 
//* 1>ProDG Compiling "c:/soft/rdr2/game/src/fx/GTA_Layer/Systems/PtFxFire.cpp"
//* 1>c:/soft/rdr2/game/src/fx/GTA_Layer/Systems/../PtFxIterator.h(42,1): warning 556: "CPtFxIteratorBase<FxInstType>::operator CPtFxIteratorBase<const rage::ptxEffectInst>() [with FxInstType=const rage::ptxEffectInst]" will not be called for implicit or explicit conversions
//* 1>          detected during:
//* 1>            instantiation of class "CPtFxIteratorBase<FxInstType> [with FxInstType=const rage::ptxEffectInst]" at line 909 of "c:/soft/rdr2/game/src/fx/GTA_Layer/Systems/PtFxFire.cpp"
//* 1>            instantiation of "void <unnamed>::AJGDebugTestIterators<iterator>() [with iterator=CPtFxManager::iterator]" at line 943 of "c:/soft/rdr2/game/src/fx/GTA_Layer/Systems/PtFxFire.cpp"
//* 
//* To understand what the warning is complaining about, it is easier to understand if you instantiate this class with
//* some type T and const T. Doing so you get the following functions:
//* 
//* CPtFxIteratorBase<T>::CPtFxIteratorBase(const CPtFxIteratorBase<const T> &)()		//Convert: A<const T> -> A<T>
//* CPtFxIteratorBase<T>::operator CPtFxIteratorBase<const T>()							//Convert: A<T> -> A<const T>
//* 
//* CPtFxIteratorBase<const T>::CPtFxIteratorBase(const CPtFxIteratorBase<const const T> &)()	//Convert: A<const const T> -> A<const T>
//* CPtFxIteratorBase<const T>::operator CPtFxIteratorBase<const const T>()						//Convert: A<const T> -> A<const const T>
//* 
//* Notice that the PS3 compiler complains about the case where FxInstType = const T. It seems that the PS3 compiler collapses const const T
//* down to just const T. This changes the 2 const T functions as follows:
//* 
//* CPtFxIteratorBase<const T>::CPtFxIteratorBase(const CPtFxIteratorBase<const T> &)()	//Convert: A<const T> -> A<const T> (Note: This is copy ctor!)
//* CPtFxIteratorBase<const T>::operator CPtFxIteratorBase<const T>()					//Convert: A<const T> -> A<const T>
//* 
//* Since both functions now convert and output the same types, C++ standard says that conversion will always prefer to use a constructor
//* rather than the cast operator. So the PS3 compiler warns that the cast function will never be used. This is safe to ignore since we are
//* really only interested in the ctor and cast function from the non-const type anyway. And since no one will ever use the const version's
//* cast operator the compiler should optimize out the code. Thus, I am disabling the warning.
//* 
//**************************************************************************************************************************************

template<typename T>
CPtFxIteratorBase<T>::CPtFxIteratorBase()
: mCurrDrawList(-1)
, mEffectInstNode(NULL)
#if PTFX_DETECT_ENDLESS_LOOP
, mEndlessLoopDetector(0)
#endif
{
}

template<typename FxInstType>
CPtFxIteratorBase<FxInstType>::CPtFxIteratorBase(const CPtFxIteratorBase<const FxInstType> &ci)
: mCurrDrawList(ci.mCurrDrawList)
, mEffectInstNode(ci.mEffectInstNode)
#if PTFX_DETECT_ENDLESS_LOOP
, mEndlessLoopDetector(ci.mEndlessLoopDetector)
#endif
{
}

//Operators
template<typename T>
bool CPtFxIteratorBase<T>::operator ==(const this_type &rhs) const
{
	return (mCurrDrawList == rhs.mCurrDrawList) && (mEffectInstNode == rhs.mEffectInstNode);
}

template<typename T>
bool CPtFxIteratorBase<T>::operator !=(const this_type &rhs) const
{
	return !(*this == rhs);
}

template<typename T>
typename CPtFxIteratorBase<T>::pointer CPtFxIteratorBase<T>::operator ->() const
{
	if(IsValid() && mEffectInstNode && mEffectInstNode->GetDataSB())
	{
		return mEffectInstNode->GetDataSB();
	}
	else
	{
		Quitf(ERR_GFX_PFX_1,
			  "Attempt to call a member by pointer (->) using Particle FX Iterator failed! Could not aquire a valid FX pointer!\n"
			  "[IsValid = %s, CurrentList = %d, Node = %p, Node->GetDataSB = %p]", IsValid() ? "True" : "False",
			  mCurrDrawList, mEffectInstNode, mEffectInstNode ? mEffectInstNode->GetDataSB() : NULL);
		return NULL;
	}
}

template<typename T>
typename CPtFxIteratorBase<T>::reference CPtFxIteratorBase<T>::operator *() const
{
	if(IsValid() && mEffectInstNode && mEffectInstNode->GetDataSB())
	{
		return *mEffectInstNode->GetDataSB();
	}
	else
	{
		Quitf(ERR_GFX_PFX_2,
			  "Attempt to dereference a Particle FX Iterator failed! Could not aquire a valid FX reference!\n"
			  "[IsValid = %s, CurrentList = %d, Node = %p, Node->GetDataSB = %p]", IsValid() ? "True" : "False",
			  mCurrDrawList, mEffectInstNode, mEffectInstNode ? mEffectInstNode->GetDataSB() : NULL);
		return *mEffectInstNode->GetDataSB();
	}
}

//Cast to const iterator type
template<typename T>
CPtFxIteratorBase<T>::operator CPtFxIteratorBase<const T>()
{
	CPtFxIteratorBase<const T> casted;
	casted.mCurrDrawList = mCurrDrawList;
	casted.mEffectInstNode = mEffectInstNode;
#if PTFX_DETECT_ENDLESS_LOOP
	casted.mEndlessLoopDetector = mEndlessLoopDetector;
#endif
	return casted;
}

template<typename T>
void CPtFxIteratorBase<T>::Reset()
{
	mCurrDrawList = -1;
	mEffectInstNode = NULL;

#	if PTFX_DETECT_ENDLESS_LOOP
	{
		mEndlessLoopDetector = 0;
	}
#	endif
}

template<typename T>
typename CPtFxIteratorBase<T>::this_type &CPtFxIteratorBase<T>::Increment()
{
	if(Verifyf(IsValid(), "Particle FX iterator is invalid! Can not increment iterator!"))
	{
		bool success = false;

		if(mEffectInstNode) //In initial increment call, this will be NULL. For all others, this should be valid. (Iteration is mid-list)
		{
			mEffectInstNode = static_cast<ptxEffectInstItem *>(mEffectInstNode->GetNext());
			success = IterateList();
			if(!success)
				++mCurrDrawList;
		}

		//Select new list if necessary
		if(!success)
		{
			//We're not in the middle of a list. This should only happen after we were in the middle and failed to get the next item in the current list,
			//or if this is the 1st item we are iterating
			u32 drawListCount = RMPTFXMGR.GetNumDrawLists();
			for(/*mCurrDrawList*/; mCurrDrawList < static_cast<int>(drawListCount) && !success; ++mCurrDrawList)
			{
				mEffectInstNode = static_cast<ptxEffectInstItem *>(RMPTFXMGR.GetDrawList(mCurrDrawList)->m_effectInstList.GetHead());
				success = IterateList();
			}
		}

		//End. If not successful, reset the iterator!
		if(!success)
		{
			Reset();
		}
	}

	return *this;
}

template<typename T>
bool CPtFxIteratorBase<T>::IterateList()
{
	bool success = false;

	while(mEffectInstNode && !success)
	{
#		if PTFX_DETECT_ENDLESS_LOOP
		{
			++mEndlessLoopDetector;
			if(mEndlessLoopDetector > PTFX_MAX_INSTS)
			{
				static bool bDidDump = false;
				if (!bDidDump)
				{
					bDidDump = true;
					int iBreakCount=0;
					Errorf("*********************************************");
					Errorf("Endless loop in particles - dumping list now!");
					Errorf("*********************************************");
					ptxEffectInstItem *pCheckInst = static_cast<ptxEffectInstItem *>(RMPTFXMGR.GetDrawList(mCurrDrawList)->m_effectInstList.GetHead());
					while (pCheckInst)
					{
						ptxEffectInst* pCurFXInst = pCheckInst->GetDataSB();
						if (pCheckInst == mEffectInstNode)
						{
							//At broken point
							if (iBreakCount == 0)
							{
								Errorf("\tBREAK START Effect [wrapper: 0x%p, inst: 0x%p]: %s", pCheckInst, pCurFXInst, pCurFXInst->GetName());
								iBreakCount++;
							}
							else
							{
								Errorf("\tBREAK END Effect [wrapper: 0x%p, inst: 0x%p]: %s", pCheckInst, pCurFXInst, pCurFXInst->GetName());
								return false;
							}
						}
						else
						{
							Errorf("\tEffect [wrapper: 0x%p, inst: 0x%p]: %s", pCheckInst, pCurFXInst, pCurFXInst->GetName());
						}
						pCheckInst = (ptxEffectInstItem*)pCheckInst->GetNextUpdate();
					}
				}

				static bool sStopLoop = true;
				if(sStopLoop)
					break;
			}
		}
#		endif //PTFX_DETECT_ENDLESS_LOOP

		success = mEffectInstNode->GetDataSB() != NULL;
		if(!success)
			mEffectInstNode = static_cast<ptxEffectInstItem *>(mEffectInstNode->GetNext());
	}

	return success;
}

template<typename T>
typename CPtFxIteratorBase<T>::this_type CPtFxIteratorBase<T>::CreateBegin()
{
	this_type iter;
	iter.Reset();
	iter.mCurrDrawList = 0;
	iter.Increment(); //Set iterator to 1st value
	return iter;
}

template<typename T>
typename CPtFxIteratorBase<T>::this_type CPtFxIteratorBase<T>::CreateEnd()
{
	this_type iter;
	iter.Reset();
	return iter;
}

#endif // FX_PTFXITERATOR_INLINE_H 
