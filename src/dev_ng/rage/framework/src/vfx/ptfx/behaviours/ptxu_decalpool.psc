<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_DecalPool" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
<float name="m_velocityThresh"/>
<int name="m_liquidType"/>
<int name="m_decalId"/>
<float name="m_startSize"/>
<float name="m_endSize"/>
<float name="m_growthRate"/>
</structdef>

</ParserSchema>