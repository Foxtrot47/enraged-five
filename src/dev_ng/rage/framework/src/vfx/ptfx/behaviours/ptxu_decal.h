// 
// vfx/ptfx/behaviours/ptxu_decal.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_DECAL_H 
#define PTXU_DECAL_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// classes
class ptxu_Decal : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Decal();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Decal(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Decal);

		// info
		const char* GetName() {return "ptxu_Decal";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_DECAL;}										// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}														// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}												// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);												// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);											// called from InitPoint and UpdatePoint to update the particle accordingly 

		void UpdateFinalize(ptxBehaviourParams& params);

		void SetKeyframeDefns();																// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);							// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Decal";}											// name of the behaviour shown in the editor
		bool IsActive();																		// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_dimensionsKFP;	
		ptxKeyframeProp m_alphaKFP;	

		// non keyframe data
		int	m_decalId;
		float m_velocityThresh;
		float m_totalLife;
		float m_fadeInTime;
		float m_uvMultStart;
		float m_uvMultEnd;
		float m_uvMultTime;
		float m_duplicateRejectDist;
		bool m_flipU;
		bool m_flipV;
		bool m_proportionalSize;
		bool m_useComplexColn;
		float m_projectionDepth;
		float m_distanceScale;
		bool m_isDirectional;

		datPadding<3> m_pad;

		// static data
		static ptxKeyframeDefn sm_dimensionsDefn;
		static ptxKeyframeDefn sm_alphaDefn;
};


} // namespace rage


#endif // PTXU_DECAL_H 


