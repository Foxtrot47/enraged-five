// 
// vfx/ptfx/behaviours/ptxu_fogvolume.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_FOGVOLUME_H
#define PTXU_FOGVOLUME_H


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"
#include "rmptfx/ptxconfig.h"
#include "vfx/ptfx/ptfxcallbacks.h"


// namespaces
namespace rage
{


// enumerations

enum ptxu_FogVolume_Lighting
{
	PTXU_FOGVOLUME_LIGHTING_FOGHDR	= 0,
	PTXU_FOGVOLUME_LIGHTING_DIRECTIONAL,
	PTXU_FOGVOLUME_LIGHTING_NONE,
	PTXU_FOGVOLUME_LIGHTING_NUM
};

// classes
class ptxu_FogVolume : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_FogVolume();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_FogVolume(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_FogVolume);

		// info
		const char* GetName() {return "ptxu_FogVolume";}												// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_FOGVOLUME;}											// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}																// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}																// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}														// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return true;}																// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}															// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);
		void UpdatePoint(ptxBehaviourParams& params);													// called from InitPoint and UpdatePoint to update the particle accordingly 

		ptfxFogVolumeInfo CalculateFogVolumeParams(ptxPointSB* pPointSB, ptxPointDB* pPointDB, ptxEffectInst* pEffectInst, ptxBehaviourParams& params);
		void UpdateFinalize(ptxBehaviourParams& params);

		__forceinline void DrawPoints(ptxEffectInst* RESTRICT pEffectInst,								// called for each active particle to draw them
			ptxEmitterInst* RESTRICT pEmitterInst,
			ptxParticleRule* RESTRICT pParticleRule,
			ptxEvolutionList* RESTRICT pEvoList
			PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface *pMultipleDrawInterface));


		void SetKeyframeDefns();																		// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);									// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Fog Volume";}												// name of the behaviour shown in the editor
		bool IsActive();																				// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);														// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);				// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);	// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data (light specific)
		ptxKeyframeProp m_rgbTintMinKFP;		
		ptxKeyframeProp m_rgbTintMaxKFP;		
		ptxKeyframeProp m_densityRangeKFP;	
		ptxKeyframeProp m_scaleMinKFP;
		ptxKeyframeProp m_scaleMaxKFP;
		ptxKeyframeProp m_rotationMinKFP;
		ptxKeyframeProp m_rotationMaxKFP;

		// non keyframe data (light specific)
		float m_falloff;
		float m_hdrMult;
		int m_lightingType;
		bool m_colourTintFromParticle;
		bool m_sortWithParticles;
		bool m_useGroundFogColour;
		bool m_useEffectEvoValues;

		//datPadding<1> m_pad;

		// static data
		// light specific
		static ptxKeyframeDefn sm_rgbTintMinDefn;		
		static ptxKeyframeDefn sm_rgbTintMaxDefn;		
		static ptxKeyframeDefn sm_densityRangeDefn;	
		static ptxKeyframeDefn sm_scaleMinDefn;		
		static ptxKeyframeDefn sm_scaleMaxDefn;	
		static ptxKeyframeDefn sm_rotationMinDefn;		
		static ptxKeyframeDefn sm_rotationMaxDefn;	


};


} // namespace rage


#endif // PTXU_FOGVOLUME_H 

