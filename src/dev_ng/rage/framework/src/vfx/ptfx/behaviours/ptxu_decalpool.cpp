// 
// vfx/ptfx/behaviours/ptxu_decalpool.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "vfx/ptfx/behaviours/ptxu_decalpool.h"
#include "vfx/ptfx/behaviours/ptxu_decalpool_parser.h"

#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxconfig.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_DecalPool::ptxu_DecalPool()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_DecalPool::SetDefaultData()
{
	// default keyframes

	// default other data
	m_velocityThresh = 0.0f;
	m_liquidType = -1;
	m_decalId = -1;
	m_startSize = 0.0f;
	m_endSize = 0.0f;
	m_growthRate = 0.0f;
}

ptxu_DecalPool::ptxu_DecalPool(datResource& rsc)
	: ptxBehaviour(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_DecalPool::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_DecalPool, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_DecalPool, m_velocityThresh)
		SSTRUCT_FIELD(ptxu_DecalPool, m_liquidType)
		SSTRUCT_FIELD(ptxu_DecalPool, m_decalId)
		SSTRUCT_FIELD(ptxu_DecalPool, m_startSize)
		SSTRUCT_FIELD(ptxu_DecalPool, m_endSize)
		SSTRUCT_FIELD(ptxu_DecalPool, m_growthRate)
		SSTRUCT_IGNORE(ptxu_DecalPool, m_pad)
	SSTRUCT_END(ptxu_DecalPool)
}
#endif

IMPLEMENT_PLACE(ptxu_DecalPool);

void ptxu_DecalPool::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// Get pointers to the actual data.
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// set this particle as collidable
	pPointSB->SetFlag(PTXPOINT_FLAG_COLLIDES);

#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointDecalPool)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_DecalPool::UpdatePoint(ptxBehaviourParams& params)
{
	// don't do decals for effects flagged as only rendering in non-first person views
	// these are likely to be duplicate effects for rendering in mirrors/shadows when in first person
	ptxEffectInst* pEffectInst = params.pEffectInst;
	if (pEffectInst->GetFlag(PTFX_EFFECTINST_RENDER_ONLY_NON_FIRST_PERSON_VIEW))
	{
		return;
	}

	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
#if !__RESOURCECOMPILER
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();
#endif // !__RESOURCECOMPILER

	// return if this emitter isn't active
#if RMPTFX_EDITOR
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	if (pPointSB->GetFlag(PTXPOINT_FLAG_HAS_COLLIDED))
	{
#if !__RESOURCECOMPILER
		if (Mag(pPointDB->GetCurrVel()).Getf()>=m_velocityThresh)
		{
			// add the decal pool
			ptfxDecalPoolInfo decalPoolInfo;
			decalPoolInfo.liquidType = m_liquidType;
			decalPoolInfo.decalRenderSettingId = (u32)m_decalId;
			decalPoolInfo.vPos = pPointSB->GetColnPos();
			decalPoolInfo.vNormal = pPointSB->GetColnNormal();
			decalPoolInfo.startSize = m_startSize;
			decalPoolInfo.endSize = m_endSize;
			decalPoolInfo.growthRate = m_growthRate;
			decalPoolInfo.mtlId = pPointSB->GetColnMtlId();
			decalPoolInfo.colR = pPointDB->GetColour().GetRed();
			decalPoolInfo.colG = pPointDB->GetColour().GetGreen();
			decalPoolInfo.colB = pPointDB->GetColour().GetBlue();
			decalPoolInfo.colA = pPointDB->GetColour().GetAlpha();

			g_pPtfxCallbacks->AddDecalPool(decalPoolInfo);
		}
#endif
	}
}

void ptxu_DecalPool::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_DecalPool::IsActive()
{
	ptxu_DecalPool* pDecalPool = static_cast<ptxu_DecalPool*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_velocityThresh == pDecalPool->m_velocityThresh &&
				   m_liquidType == pDecalPool->m_liquidType &&
				   m_decalId == pDecalPool->m_decalId &&
				   m_startSize == pDecalPool->m_startSize && 
				   m_endSize == pDecalPool->m_endSize && 
				   m_growthRate == pDecalPool->m_growthRate;

	return !isEqual;
}

void ptxu_DecalPool::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 6);

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Velocity Threshold", 0.0f, 10.0f);
	SendIntDefnToEditor(buff, "Liquid Type", -1, 9);
	SendIntDefnToEditor(buff, "Decal Id", -1, 9999);
	SendFloatDefnToEditor(buff, "Start Size", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "End Size", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Growth Rate", 0.0f, 1.0f);
}

void ptxu_DecalPool::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_velocityThresh);
	SendIntToEditor(buff, m_liquidType);
	SendIntToEditor(buff, m_decalId);
	SendFloatToEditor(buff, m_startSize);
	SendFloatToEditor(buff, m_endSize);
	SendFloatToEditor(buff, m_growthRate);
}

void ptxu_DecalPool::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_velocityThresh);
	ReceiveIntFromEditor(buff, m_liquidType);
	ReceiveIntFromEditor(buff, m_decalId);
	ReceiveFloatFromEditor(buff, m_startSize);
	ReceiveFloatFromEditor(buff, m_endSize);
	ReceiveFloatFromEditor(buff, m_growthRate);
}
#endif // RMPTFX_EDITOR



} // namespace rage
