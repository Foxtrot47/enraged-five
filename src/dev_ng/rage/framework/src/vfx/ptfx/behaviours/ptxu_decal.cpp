// 
// vfx/ptfx/behaviours/ptxu_decal.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "vfx/ptfx/behaviours/ptxu_decal.h"
#include "vfx/ptfx/behaviours/ptxu_decal_parser.h"

#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxconfig.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Decal::sm_dimensionsDefn(					"Dimensions",				atHashValue("ptxu_Decal:m_dimensionsKFP"),					PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10.0f,		0.05f),		"Width Min",	"Width Max",	"Height Min",	"Height Max");
ptxKeyframeDefn ptxu_Decal::sm_alphaDefn(						"Alpha (Not Used)",			atHashValue("ptxu_Decal:m_alphaKFP"),						PTXKEYFRAMETYPE_FLOAT,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Alpha Min",	"Alpha Max");


// code
#if RMPTFX_XML_LOADING
ptxu_Decal::ptxu_Decal()
{
	// create keyframes
	m_dimensionsKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Decal:m_dimensionsKFP"));
	m_alphaKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Decal:m_alphaKFP"));

	m_keyframePropList.AddKeyframeProp(&m_dimensionsKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_alphaKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif 

void ptxu_Decal::SetDefaultData()
{
	// default keyframes
	m_dimensionsKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_alphaKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));

	// default other data
	m_decalId = -1;
	m_velocityThresh = 0.0f;
	m_totalLife = 0.0f;
	m_fadeInTime = 0.0f;
	m_uvMultStart = 1.0f;
	m_uvMultEnd = 1.0f;
	m_uvMultTime = 0.0f;
	m_duplicateRejectDist = 1.0f;
	m_flipU = false;
	m_flipV = false;
	m_proportionalSize = true;
	m_useComplexColn = false;
	m_projectionDepth = 0.3f;
	m_distanceScale = 1.0f;
	m_isDirectional = false;
}

ptxu_Decal::ptxu_Decal(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_dimensionsKFP(rsc)
	, m_alphaKFP(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_Decal::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Decal, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Decal, m_dimensionsKFP)
		SSTRUCT_FIELD(ptxu_Decal, m_alphaKFP)
		SSTRUCT_FIELD(ptxu_Decal, m_decalId)
		SSTRUCT_FIELD(ptxu_Decal, m_velocityThresh)
		SSTRUCT_FIELD(ptxu_Decal, m_totalLife)
		SSTRUCT_FIELD(ptxu_Decal, m_fadeInTime)
		SSTRUCT_FIELD(ptxu_Decal, m_uvMultStart)
		SSTRUCT_FIELD(ptxu_Decal, m_uvMultEnd)
		SSTRUCT_FIELD(ptxu_Decal, m_uvMultTime)
		SSTRUCT_FIELD(ptxu_Decal, m_duplicateRejectDist)
		SSTRUCT_FIELD(ptxu_Decal, m_flipU)
		SSTRUCT_FIELD(ptxu_Decal, m_flipV)
		SSTRUCT_FIELD(ptxu_Decal, m_proportionalSize)
		SSTRUCT_FIELD(ptxu_Decal, m_useComplexColn)
		SSTRUCT_FIELD(ptxu_Decal, m_projectionDepth)
		SSTRUCT_FIELD(ptxu_Decal, m_distanceScale)
		SSTRUCT_FIELD(ptxu_Decal, m_isDirectional)
		SSTRUCT_IGNORE(ptxu_Decal, m_pad)
	SSTRUCT_END(ptxu_Decal)
}
#endif

IMPLEMENT_PLACE(ptxu_Decal);

void ptxu_Decal::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// Get pointers to the actual data.
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// set this particle as collidable
	pPointSB->SetFlag(PTXPOINT_FLAG_COLLIDES);

#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointDecal)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Decal::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
#if !__RESOURCECOMPILER
	ptxEffectInst* pEffectInst = params.pEffectInst;

	// don't do decals for effects flagged as only rendering in non-first person views
	// these are likely to be duplicate effects for rendering in mirrors/shadows when in first person
	if (pEffectInst->GetFlag(PTFX_EFFECTINST_RENDER_ONLY_NON_FIRST_PERSON_VIEW))
	{
		return;
	}
#endif

	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle life ratio key time
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	// return if this emitter isn't active
#if RMPTFX_EDITOR
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	if (pPointSB->GetFlag(PTXPOINT_FLAG_HAS_COLLIDED))
	{
#if !__RESOURCECOMPILER
		if (Mag(pPointDB->GetCurrVel()).Getf()>=m_velocityThresh)
		{
			// add the decal 
			u8 randOffset = 1;
			if (m_proportionalSize)
			{
				randOffset = 0;
			}

			ptxEvolutionList* pEvolutionList = params.pEvolutionList;
			Vec4V vDimensionsMinMax = m_dimensionsKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
			ScalarV vWidth = Lerp(GetBiasValue(m_dimensionsKFP, pPointSB->GetRandIndex()), SplatX(vDimensionsMinMax), SplatY(vDimensionsMinMax));
			ScalarV vHeight = Lerp(GetBiasValue(m_dimensionsKFP, pPointSB->GetRandIndex()+randOffset), SplatZ(vDimensionsMinMax), SplatW(vDimensionsMinMax));

			//Vec4V vAlphaMinMax = m_alphaKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
			//ScalarV vAlpha = Lerp(GetBiasValue(m_alphaKFP, pPointSB->GetRandIndex()), SplatX(vAlphaMinMax), SplatY(vAlphaMinMax));

			ptfxDecalInfo decalInfo;
			decalInfo.pEffectInst = pEffectInst;
			decalInfo.decalId = m_decalId;
			decalInfo.vPos = pPointSB->GetColnPos();
			decalInfo.vDir = -pPointSB->GetColnNormal();
			decalInfo.width = vWidth.Getf();
			decalInfo.height = vHeight.Getf();
			decalInfo.totalLife = m_totalLife;
			decalInfo.fadeInTime = m_fadeInTime;
			decalInfo.uvMultStart = m_uvMultStart;
			decalInfo.uvMultEnd = m_uvMultEnd;
			decalInfo.uvMultTime = m_uvMultTime;
			decalInfo.duplicateRejectDist = m_duplicateRejectDist;
			decalInfo.projectionDepth = m_projectionDepth;
			decalInfo.distanceScale = m_distanceScale;
			decalInfo.flipU = m_flipU;
			decalInfo.flipV = m_flipV;
			decalInfo.useComplexColn = m_useComplexColn;
			decalInfo.isDirectional = m_isDirectional;
			decalInfo.colR = pPointDB->GetColour().GetRed();
			decalInfo.colG = pPointDB->GetColour().GetGreen();
			decalInfo.colB = pPointDB->GetColour().GetBlue();
			decalInfo.colA = pPointDB->GetColour().GetAlpha();  // vAlpha.Getf();

			g_pPtfxCallbacks->AddDecal(decalInfo);
		}
#endif
	}
}

void ptxu_Decal::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}

void ptxu_Decal::SetKeyframeDefns()
{
	m_dimensionsKFP.GetKeyframe().SetDefn(&sm_dimensionsDefn);
	m_alphaKFP.GetKeyframe().SetDefn(&sm_alphaDefn);
}

void ptxu_Decal::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_dimensionsKFP, &sm_dimensionsDefn);
	pEvolutionList->SetKeyframeDefn(&m_alphaKFP, &sm_alphaDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Decal::IsActive()
{
	ptxu_Decal* pDecal = static_cast<ptxu_Decal*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_dimensionsKFP == pDecal->m_dimensionsKFP &&
				   m_alphaKFP == pDecal->m_alphaKFP && 
				   m_decalId == pDecal->m_decalId &&
				   m_velocityThresh == pDecal->m_velocityThresh &&
				   m_totalLife == pDecal->m_totalLife &&
				   m_fadeInTime == pDecal->m_fadeInTime &&
				   m_uvMultStart == pDecal->m_uvMultStart &&
				   m_uvMultEnd == pDecal->m_uvMultEnd &&
				   m_uvMultTime == pDecal->m_uvMultTime &&
				   m_flipU == pDecal->m_flipU &&
				   m_flipV == pDecal->m_flipV &&
				   m_proportionalSize == pDecal->m_proportionalSize && 
				   m_useComplexColn == pDecal->m_useComplexColn && 
				   m_projectionDepth == pDecal->m_projectionDepth && 
				   m_distanceScale == pDecal->m_distanceScale &&
				   m_isDirectional == pDecal->m_isDirectional;

	return !isEqual;
}

void ptxu_Decal::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 17);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Dimensions");
	SendKeyframeDefnToEditor(buff, "Alpha (Not Used)");

	// send non-keyframes
	SendIntDefnToEditor(buff, "Decal Id", -1, 9999);
	SendFloatDefnToEditor(buff, "Velocity Threshold", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Total Life", 0.0f, 1000.0f);
	SendFloatDefnToEditor(buff, "Fade In Time", 0.0f, 100.0f);
	SendFloatDefnToEditor(buff, "UV Mult Start", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "UV Mult End", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "UV Mult Time", 0.0f, 100.0f);
	SendFloatDefnToEditor(buff, "Duplicate Reject Distance", 0.0f, 100.0f);
	SendBoolDefnToEditor(buff, "Flip U");
	SendBoolDefnToEditor(buff, "Flip V");
	SendBoolDefnToEditor(buff, "Proportional Size");
	SendBoolDefnToEditor(buff, "Use Complex Collision");
	SendFloatDefnToEditor(buff, "Projection Depth", 0.0f, 5.0f);
	SendFloatDefnToEditor(buff, "Distance Scale", -2.0f, 5.0f);
	SendBoolDefnToEditor(buff, "Is Directional");
}


void ptxu_Decal::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_dimensionsKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_alphaKFP, pParticleRule);

	// send non-keyframes
	SendIntToEditor(buff, m_decalId);
	SendFloatToEditor(buff, m_velocityThresh);
	SendFloatToEditor(buff, m_totalLife);
	SendFloatToEditor(buff, m_fadeInTime);
	SendFloatToEditor(buff, m_uvMultStart);
	SendFloatToEditor(buff, m_uvMultEnd);
	SendFloatToEditor(buff, m_uvMultTime);
	SendFloatToEditor(buff, m_duplicateRejectDist);
	SendBoolToEditor(buff, m_flipU);
	SendBoolToEditor(buff, m_flipV);
	SendBoolToEditor(buff, m_proportionalSize);
	SendBoolToEditor(buff, m_useComplexColn);
	SendFloatToEditor(buff, m_projectionDepth);
	SendFloatToEditor(buff, m_distanceScale);
	SendBoolToEditor(buff, m_isDirectional);
	
}
void ptxu_Decal::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_dimensionsKFP);
	ReceiveKeyframeFromEditor(buff, m_alphaKFP);

	// receive non-keyframes
	ReceiveIntFromEditor(buff, m_decalId);
	ReceiveFloatFromEditor(buff, m_velocityThresh);
	ReceiveFloatFromEditor(buff, m_totalLife);
	ReceiveFloatFromEditor(buff, m_fadeInTime);
	ReceiveFloatFromEditor(buff, m_uvMultStart);
	ReceiveFloatFromEditor(buff, m_uvMultEnd);
	ReceiveFloatFromEditor(buff, m_uvMultTime);
	ReceiveFloatFromEditor(buff, m_duplicateRejectDist);
	ReceiveBoolFromEditor(buff, m_flipU);
	ReceiveBoolFromEditor(buff, m_flipV);
	ReceiveBoolFromEditor(buff, m_proportionalSize);
	ReceiveBoolFromEditor(buff, m_useComplexColn);
	ReceiveFloatFromEditor(buff, m_projectionDepth);
	ReceiveFloatFromEditor(buff, m_distanceScale);
	ReceiveBoolFromEditor(buff, m_isDirectional);
}
#endif // RMPTFX_EDITOR



} // namespace rage
