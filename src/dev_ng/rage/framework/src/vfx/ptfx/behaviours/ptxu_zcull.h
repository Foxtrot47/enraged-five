// 
// vfx/ptfx/behaviours/ptxu_zcull.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_ZCULL_H
#define PTXU_ZCULL_H


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_ZCull_Mode
{
	PTXU_ZCULL_MODE_CULL_UNDER			= 0,
	PTXU_ZCULL_MODE_CULL_OVER,
	PTXU_ZCULL_MODE_CLAMP_UNDER,
	PTXU_ZCULL_MODE_CLAMP_OVER,

	PTXU_ZCULL_MODE_NUM
};

enum ptxu_ZCull_ReferenceSpace
{
	PTXU_ZCULL_REFERENCESPACE_WORLD		= 0,
	PTXU_ZCULL_REFERENCESPACE_EFFECT,
	PTXU_ZCULL_REFERENCESPACE_EMITTER,
	PTXU_ZCULL_REFERENCESPACE_WATER_Z_CAMERA,
	PTXU_ZCULL_REFERENCESPACE_WATER_Z_POINT,

	PTXU_ZCULL_REFERENCESPACE_NUM
};


// classes
class ptxu_ZCull : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_ZCull();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_ZCull(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_ZCull);

		// info
		const char* GetName() {return "ptxu_ZCull";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_ZCULL;}										// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}														// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}												// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		int GetMode() { return m_mode; }
		int GetReferenceSpace() { return m_referenceSpace; }

		void InitPoint(ptxBehaviourParams& params);												// called for each newly created particle if NeedsToInit returns true						
		void UpdatePoint(ptxBehaviourParams& params);											// called from InitPoint and UpdatePoint to update the particle accordingly 						

		void UpdateFinalize(ptxBehaviourParams& params);

		void SetKeyframeDefns();																// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);							// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Z Cull";}											// name of the behaviour shown in the editor
		bool IsActive();																		// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data
		ptxKeyframeProp m_heightKFP;		
		ptxKeyframeProp m_fadeDistKFP;	

		// global data
		float* m_pGlobalData_NOTUSED;

		// non keyframe data
		int m_mode;
		int m_referenceSpace;

		datPadding<4> m_pad;

		// static data
		static ptxKeyframeDefn sm_heightDefn;			
		static ptxKeyframeDefn sm_fadeDistDefn;	

};


} // namespace rage


#endif // PTXU_ZCULL_H 

