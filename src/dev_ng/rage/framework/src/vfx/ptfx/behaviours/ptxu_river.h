// 
// vfx/ptfx/behaviours/ptxu_river.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_RIVER_H 
#define PTXU_RIVER_H 


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// classes
class ptxu_River : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_River();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_River(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_River);

		// info
		const char* GetName() {return "ptxu_River";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_RIVER;}										// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}														// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}												// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);												// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);											// called from InitPoint and UpdatePoint to update the particle accordingly 

		void UpdateFinalize(ptxBehaviourParams& params);

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "River";}											// name of the behaviour shown in the editor
		bool IsActive();																		// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data

		// non keyframe data
		float m_velocityMult;
		float m_influence;

		datPadding<8> m_pad;

		// static data

};


} // namespace rage


#endif // PTXU_RIVER_H 


