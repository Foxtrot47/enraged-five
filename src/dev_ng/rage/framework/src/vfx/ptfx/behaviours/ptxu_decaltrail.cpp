// 
// vfx/ptfx/behaviours/ptxu_decaltrail.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

/*
// includes
#include "vfx/ptfx/behaviours/ptxu_decaltrail.h"
#include "vfx/ptfx/behaviours/ptxu_decaltrail_parser.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{

// code
#if RMPTFX_XML_LOADING
ptxu_DecalTrail::ptxu_DecalTrail()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_DecalTrail::SetDefaultData()
{
	// default keyframes

	// default other data
	m_velocityThresh = 0.0f;
	m_liquidType = -1;
	m_decalId = -1;
	m_widthMin = 0.0f;
	m_widthMax = 0.0f;
}

ptxu_DecalTrail::ptxu_DecalTrail(datResource& rsc)
	: ptxBehaviour(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_DecalTrail::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_DecalTrail, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_DecalTrail, m_velocityThresh)
		SSTRUCT_FIELD(ptxu_DecalTrail, m_liquidType)
		SSTRUCT_FIELD(ptxu_DecalTrail, m_decalId)
		SSTRUCT_FIELD(ptxu_DecalTrail, m_widthMin)
		SSTRUCT_FIELD(ptxu_DecalTrail, m_widthMax)
		SSTRUCT_IGNORE(ptxu_DecalTrail, m_pad)
	SSTRUCT_END(ptxu_DecalTrail)
}
#endif

IMPLEMENT_PLACE(ptxu_DecalTrail);

void ptxu_DecalTrail::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// Get pointers to the actual data.
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// set this particle as collidable
	pPointSB->SetFlag(PTXPOINT_FLAG_COLLIDES);

#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointDecalTrail)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_DecalTrail::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
#if !__RESOURCECOMPILER
	ptxEffectInst* pEffectInst = params.pEffectInst;
#endif // !__RESOURCECOMPILER
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
#if !__RESOURCECOMPILER
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();
#endif // !__RESOURCECOMPILER
	
	// return if this emitter isn't active
#if RMPTFX_EDITOR
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	if (pPointSB->GetFlag(PTXPOINT_FLAG_HAS_COLLIDED))
	{
#if !__RESOURCECOMPILER
		if (Mag(pPointDB->GetCurrVel()).Getf()>=m_velocityThresh)
		{
			// add the decal trail
			ptfxDecalTrailInfo decalTrailInfo;
			decalTrailInfo.pEffectInst = pEffectInst;
			decalTrailInfo.vPos = pPointSB->GetColnPos();
			decalTrailInfo.vNormal = pPointSB->GetColnNormal();
			decalTrailInfo.liquidType = m_liquidType;
			decalTrailInfo.decalRenderSettingId = (u32)m_decalId;
			decalTrailInfo.widthMin = m_widthMin;
			decalTrailInfo.widthMax = m_widthMax;
			decalTrailInfo.mtlId = pPointSB->GetColnMtlId();
			decalTrailInfo.colR = pPointDB->GetColour().GetRed();
			decalTrailInfo.colG = pPointDB->GetColour().GetGreen();
			decalTrailInfo.colB = pPointDB->GetColour().GetBlue();
			decalTrailInfo.colA = pPointDB->GetColour().GetAlpha();

			g_pPtfxCallbacks->AddDecalTrail(decalTrailInfo);
		}
#endif
	}
}

void ptxu_DecalTrail::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_DecalTrail::IsActive()
{
	ptxu_DecalTrail* pDecalTrail = static_cast<ptxu_DecalTrail*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_velocityThresh == pDecalTrail->m_velocityThresh &&
				   m_liquidType == pDecalTrail->m_liquidType &&
				   m_decalId == pDecalTrail->m_decalId && 
				   m_widthMin == pDecalTrail->m_widthMin && 
				   m_widthMax == pDecalTrail->m_widthMax;

	return !isEqual;
}

void ptxu_DecalTrail::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 5);

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Velocity Threshold", 0.0f, 10.0f);
	SendIntDefnToEditor(buff, "Liquid Type", -1, 9);
	SendIntDefnToEditor(buff, "Decal Id", -1, 9999);
	SendFloatDefnToEditor(buff, "Width Min", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Width Max", 0.0f, 10.0f);
}

void ptxu_DecalTrail::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_velocityThresh);
	SendIntToEditor(buff, m_liquidType);
	SendIntToEditor(buff, m_decalId);
	SendFloatToEditor(buff, m_widthMin);
	SendFloatToEditor(buff, m_widthMax);
}

void ptxu_DecalTrail::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_velocityThresh);
	ReceiveIntFromEditor(buff, m_liquidType);
	ReceiveIntFromEditor(buff, m_decalId);
	ReceiveFloatFromEditor(buff, m_widthMin);
	ReceiveFloatFromEditor(buff, m_widthMax);
}
#endif // RMPTFX_EDITOR


} // namespace rage
*/
