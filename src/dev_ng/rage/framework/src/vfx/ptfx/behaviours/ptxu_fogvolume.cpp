// 
// vfx/ptfx/behaviours/ptxu_fogvolume.cpp 
// 
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "vfx/ptfx/behaviours/ptxu_fogvolume.h"
#include "vfx/ptfx/behaviours/ptxu_fogvolume_parser.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_FogVolume::sm_rgbTintMinDefn(				"Fog Volume Tint Min",		atHashValue("ptxu_FogVolume:m_rgbTintMinKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Min",			"Green Min",		"Blue Min");
ptxKeyframeDefn ptxu_FogVolume::sm_rgbTintMaxDefn(				"Fog Volume Tint Max",		atHashValue("ptxu_FogVolume:m_rgbTintMaxKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Max",			"Green Max",		"Blue Max");
ptxKeyframeDefn ptxu_FogVolume::sm_densityRangeDefn(			"Fog Volume Density/Range",	atHashValue("ptxu_FogVolume:m_densityRangeKFP"),				PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10000.0f,	0.1f),		"Density Min",		"Density Max",		"Range Min",		"Range Max");
ptxKeyframeDefn ptxu_FogVolume::sm_scaleMinDefn(				"Fog Volume Scale Min",		atHashValue("ptxu_FogVolume:m_scaleMinKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"X Min",			"Y Min",			"Z Min");
ptxKeyframeDefn ptxu_FogVolume::sm_scaleMaxDefn(				"Fog Volume Scale Max",		atHashValue("ptxu_FogVolume:m_scaleMaxKFP"),					PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"X Max",			"Y Max",			"Z Max");
ptxKeyframeDefn ptxu_FogVolume::sm_rotationMinDefn(				"Fog Volume Rotation Min",	atHashValue("ptxu_FogVolume:m_rotationMinKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X Min",			"Y Min",			"Z Min");
ptxKeyframeDefn ptxu_FogVolume::sm_rotationMaxDefn(				"Fog Volume Rotation Max",	atHashValue("ptxu_FogVolume:m_rotationMaxKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-360.0f,	360.0f,		0.01f),		"X Max",			"Y Max",			"Z Max");


// code
#if RMPTFX_XML_LOADING
ptxu_FogVolume::ptxu_FogVolume()
{
	// create keyframes
	m_rgbTintMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_FogVolume:m_rgbTintMinKFP"));
	m_rgbTintMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_FogVolume:m_rgbTintMaxKFP"));
	m_densityRangeKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_FogVolume:m_densityRangeKFP"));
	m_rotationMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_FogVolume:m_rotationMinKFP"));
	m_rotationMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_FogVolume:m_rotationMaxKFP"));
	m_scaleMinKFP.Init(Vec4V(V_ONE), atHashValue("ptxu_FogVolume:m_scaleMinKFP"));
	m_scaleMaxKFP.Init(Vec4V(V_ONE), atHashValue("ptxu_FogVolume:m_scaleMaxKFP"));

	m_keyframePropList.AddKeyframeProp(&m_rgbTintMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rgbTintMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_densityRangeKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_scaleMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_scaleMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rotationMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rotationMaxKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_FogVolume::SetDefaultData()
{
	// default keyframes
	m_rgbTintMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_rgbTintMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_densityRangeKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(Vec2V(V_ONE), Vec2V(V_THREE)));
	m_scaleMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_scaleMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_rotationMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_rotationMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_falloff = 1.0f;
	m_colourTintFromParticle = true;
	m_sortWithParticles = false;
	m_lightingType = PTXU_FOGVOLUME_LIGHTING_FOGHDR;
	m_hdrMult = 1.0f;
	m_useGroundFogColour = false;
	m_useEffectEvoValues = false;
}

ptxu_FogVolume::ptxu_FogVolume(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_rgbTintMinKFP(rsc)
	, m_rgbTintMaxKFP(rsc)
	, m_densityRangeKFP(rsc)
	, m_scaleMinKFP(rsc)
	, m_scaleMaxKFP(rsc)
	, m_rotationMinKFP(rsc)
	, m_rotationMaxKFP(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_FogVolume::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_FogVolume, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_FogVolume, m_rgbTintMinKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_rgbTintMaxKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_densityRangeKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_scaleMinKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_scaleMaxKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_rotationMinKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_rotationMaxKFP)
		SSTRUCT_FIELD(ptxu_FogVolume, m_falloff)
		SSTRUCT_FIELD(ptxu_FogVolume, m_hdrMult)
		SSTRUCT_FIELD(ptxu_FogVolume, m_lightingType)
		SSTRUCT_FIELD(ptxu_FogVolume, m_colourTintFromParticle)
		SSTRUCT_FIELD(ptxu_FogVolume, m_sortWithParticles)
		SSTRUCT_FIELD(ptxu_FogVolume, m_useGroundFogColour)
		SSTRUCT_FIELD(ptxu_FogVolume, m_useEffectEvoValues)
		//SSTRUCT_IGNORE(ptxu_FogVolume, m_pad)
		SSTRUCT_END(ptxu_FogVolume)
}
#endif

IMPLEMENT_PLACE(ptxu_FogVolume);

void ptxu_FogVolume::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	//if (!ptxDebug::sm_disableInitPointLight)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_FogVolume::UpdatePoint(ptxBehaviourParams& 
#if !__RESOURCECOMPILER
								 params
#endif
								 )
{

	//No need to update if sort with ptfx is true. This will be done on renderthread
	if(m_sortWithParticles)
	{
		return;
	}

	// cache data from params
#if !__RESOURCECOMPILER
	ptxEffectInst* pEffectInst = params.pEffectInst;

	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// return if this emitter isn't active
#if RMPTFX_EDITOR
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	// return if the particle has expired already - unless it's the first frame
	// this allows us to tune fog volumes that last a single frame only by setting the particle life to zero
	if (pPointDB->GetLifeRatio()>1.0f && pPointSB->GetFlag(PTXPOINT_FLAG_IS_BEING_INITIALISED)==false)
	{
		return;
	}

	// initialise the fogVolume info
	ptfxFogVolumeInfo fogVolumeInfo = CalculateFogVolumeParams(pPointSB, pPointDB, pEffectInst, params); 	

	//register the fogball
	if (fogVolumeInfo.range>0.0f)
	{
		//Register the fogball with the callback
		g_pPtfxCallbacks->AddFogVolume(fogVolumeInfo);
	}

#endif // !__RESOURCECOMPILER
}

#if !__RESOURCECOMPILER
ptfxFogVolumeInfo ptxu_FogVolume::CalculateFogVolumeParams(ptxPointSB* pPointSB, ptxPointDB* pPointDB, ptxEffectInst* pEffectInst, ptxBehaviourParams& params)
{
#if !__SPU
	(void)params;
#endif
	ptfxFogVolumeInfo fogVolumeInfo;
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	fogVolumeInfo.vPos = pPointDB->GetCurrPos();
	fogVolumeInfo.range = 0.0f;
	// calculate the shared bias values using the light keyframe data
	ScalarV vBiasDensityRange;
	vBiasDensityRange = GetBiasValue(m_densityRangeKFP, pPointSB->GetRandIndex());

	ScalarV vBiasTint;
	vBiasTint = GetBiasValue(m_rgbTintMinKFP, pPointSB->GetRandIndex());

	ScalarV vBiasScale;
	vBiasScale = GetBiasValue(m_scaleMinKFP, pPointSB->GetRandIndex());

	ScalarV vBiasRotation;
	vBiasRotation = GetBiasValue(m_rotationMinKFP, pPointSB->GetRandIndex());

	// evolution mode
	ptxEvoValueType* pEvoValues = pPointSB->GetEvoValues();
	if (m_useEffectEvoValues)
	{
		pEvoValues = pEffectInst->GetEvoValues();
	}

	// SET UP THE FOG VOLUME
	// calculate the fog Volume density and range
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vFogVolumeDensityRangeMinMax = m_densityRangeKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
	ScalarV vFogVolumeDensity = Lerp(vBiasDensityRange, SplatX(vFogVolumeDensityRangeMinMax), SplatY(vFogVolumeDensityRangeMinMax));
	ScalarV vFogVolumeRange = Lerp(vBiasDensityRange, SplatZ(vFogVolumeDensityRangeMinMax), SplatW(vFogVolumeDensityRangeMinMax));
	if (IsZeroAll(vFogVolumeDensity)==false && IsZeroAll(vFogVolumeRange)==false)
	{	
		// calculate the light colour
		Vec4V vFogVolumeTintMin = m_rgbTintMinKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
		Vec4V vFogVolumeTintMax = m_rgbTintMaxKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
		Vec4V vFogVolumeScaleMin = m_scaleMinKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
		Vec4V vFogVolumeScaleMax = m_scaleMaxKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
		Vec4V vFogVolumeRotationMin = m_rotationMinKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
		Vec4V vFogVolumeRotationMax = m_rotationMaxKFP.Query(vKeyTimePoint, pEvolutionList, pEvoValues);
		Color32 fogVolumeTint = Color32(Lerp(vBiasScale, vFogVolumeTintMin, vFogVolumeTintMax));
		Vec3V fogVolumeScale = Lerp(vBiasScale, vFogVolumeScaleMin, vFogVolumeScaleMax).GetXYZ();
		Vec3V fogVolumeRotation = Lerp(vBiasRotation, vFogVolumeRotationMin, vFogVolumeRotationMax).GetXYZ() * Vec3V(ScalarV(V_TO_RADIANS));
		if (m_colourTintFromParticle)
		{
			//Use the rgb Tint from fog volume as tint to the particle colour
			fogVolumeTint = fogVolumeTint * pPointDB->GetColour();
		}

		if (IsZeroAll(fogVolumeTint.GetARGB())==false && IsZeroAll(fogVolumeScale) == false)
		{
			// the light is valid - set up the light info
			fogVolumeInfo.col = fogVolumeTint;
			fogVolumeInfo.vScale = fogVolumeScale;
			fogVolumeInfo.vRotation = fogVolumeRotation;
			fogVolumeInfo.density = vFogVolumeDensity.Getf()*pEffectInst->GetAlphaTint()*pEffectInst->GetLodAlphaMult(); 
			fogVolumeInfo.falloff = m_falloff;
			fogVolumeInfo.hdrMult = m_hdrMult;
			fogVolumeInfo.range = vFogVolumeRange.Getf();
			fogVolumeInfo.lightingType = m_lightingType;
			fogVolumeInfo.useGroundFogColour = m_useGroundFogColour;
		}
	}

	fogVolumeInfo.interiorLocation.SetAsUint32(pEffectInst->GetInteriorLocation());

	return fogVolumeInfo;

}
#endif // !__RESOURCECOMPILER

void ptxu_FogVolume::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}


void ptxu_FogVolume::DrawPoints(ptxEffectInst* RESTRICT pEffectInst,
	ptxEmitterInst* RESTRICT pEmitterInst,
	ptxParticleRule* RESTRICT pParticleRule,
	ptxEvolutionList* RESTRICT pEvoList
	PTXDRAW_MULTIPLE_DRAWS_PER_BATCH_ONLY_PARAM(ptxDrawInterface::ptxMultipleDrawInterface* UNUSED_PARAM(pMultipleDrawInterface)))
{
#if !__RESOURCECOMPILER
	//Early out if this is set to false as it would be registered through the update function
	if(!m_sortWithParticles)
	{
		return;
	}
	ptxAssertf(pEffectInst, "missing effect instance");
	ptxAssertf(pEmitterInst, "missing emitter instance");
	ptxAssertf(pParticleRule->GetPointPool() , "missing point pool");

	// MN - why are we passing through a particle rule when it can be got from the emitter inst
	//    - if this assert never fires then they're the same and we can stop passing it through
	ptxAssertf(pParticleRule == pEmitterInst->GetParticleRule(), "particle rule mismatch");
	ptxAssertf(pEmitterInst->GetEmitterRule(), "can't get emitter rule from emitter inst");
	ptxAssertf(pEmitterInst->GetEmitterRule()->GetCreationDomainObj().GetDomain(), "can't get emitter creation domain from emitter inst");

#if RMPTFX_EDITOR
	sysTimer timer;
	timer.Reset();
#endif

#if RMPTFX_USE_PARTICLE_SHADOWS
	//Switching fog volumes off when rendering shadows
	if(RMPTFXMGR.IsShadowPass())
	{
		return;
	}
#endif

	// return if there are no points to draw
	const unsigned drawBufferId = RMPTFXTHREADER.GetDrawBufferId();
	if (pEmitterInst->GetPointList().GetNumItemsMT(drawBufferId)==0)
	{
		return;
	}

	// sort the points
	unsigned numPoints = pEmitterInst->GetNumActivePointsMT(drawBufferId);
	ptxPointItem** ppPoints = Alloca(ptxPointItem*, numPoints);
	const unsigned numPoints2 = pEmitterInst->SortPoints(ppPoints);
	Assert(numPoints >= numPoints2);
	numPoints = numPoints2;

	grcViewport::SetCurrentWorldIdentity();

	// render the points
	for (unsigned i=0; i<numPoints; ++i)
	{
		ptxPointItem* pCurrPointItem = ppPoints[i];

		ptxPointDB* pPointDB = pCurrPointItem->GetDataDBMT(drawBufferId);
		ptxPointSB* pPointSB = pCurrPointItem->GetDataSB();

		ptxBehaviourParams params;
		pParticleRule->SetParams(params, pEffectInst, pEmitterInst, pEvoList, pCurrPointItem, ScalarV(V_ZERO), ScalarV(V_ONE), drawBufferId);

		ptfxFogVolumeInfo fogBallInfo = CalculateFogVolumeParams(pPointSB, pPointDB, pEffectInst, params);

		if(fogBallInfo.range > 0.0f && fogBallInfo.density > 0.0f)
		{
			g_pPtfxCallbacks->DrawFogVolume(fogBallInfo);
		}
	}

	// update the editor cpu draw time
#if RMPTFX_EDITOR
	pEmitterInst->GetEmitterRule()->GetUIData().IncAccumCPUDrawTime(timer.GetMsTime()); 
#endif
#else
	(void) pEffectInst;
	(void) pEmitterInst;
	(void) pParticleRule;
	(void) pEvoList;
#endif
}


void ptxu_FogVolume::SetKeyframeDefns()
{
	m_rgbTintMinKFP.GetKeyframe().SetDefn(&sm_rgbTintMinDefn);
	m_rgbTintMaxKFP.GetKeyframe().SetDefn(&sm_rgbTintMaxDefn);
	m_densityRangeKFP.GetKeyframe().SetDefn(&sm_densityRangeDefn);
	m_scaleMinKFP.GetKeyframe().SetDefn(&sm_scaleMinDefn);
	m_scaleMaxKFP.GetKeyframe().SetDefn(&sm_scaleMaxDefn);
	m_rotationMinKFP.GetKeyframe().SetDefn(&sm_rotationMinDefn);
	m_rotationMaxKFP.GetKeyframe().SetDefn(&sm_rotationMaxDefn);
}

void ptxu_FogVolume::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_rgbTintMinKFP, &sm_rgbTintMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_rgbTintMaxKFP, &sm_rgbTintMaxDefn);
	pEvolutionList->SetKeyframeDefn(&m_densityRangeKFP, &sm_densityRangeDefn);
	pEvolutionList->SetKeyframeDefn(&m_scaleMinKFP, &sm_scaleMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_scaleMaxKFP, &sm_scaleMaxDefn);
	pEvolutionList->SetKeyframeDefn(&m_rotationMinKFP, &sm_rotationMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_rotationMaxKFP, &sm_rotationMaxDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_FogVolume::IsActive()
{
	ptxu_FogVolume* pFogVolume = static_cast<ptxu_FogVolume*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_rgbTintMinKFP == pFogVolume->m_rgbTintMinKFP &&
				   m_rgbTintMaxKFP == pFogVolume->m_rgbTintMaxKFP &&
				   m_densityRangeKFP == pFogVolume->m_densityRangeKFP &&
				   m_scaleMinKFP == pFogVolume->m_scaleMinKFP &&
				   m_scaleMaxKFP == pFogVolume->m_scaleMaxKFP &&
				   m_rotationMinKFP == pFogVolume->m_rotationMinKFP &&
				   m_rotationMaxKFP == pFogVolume->m_rotationMaxKFP &&
				   m_falloff == pFogVolume->m_falloff &&
				   m_hdrMult == pFogVolume->m_hdrMult &&
				   m_colourTintFromParticle == pFogVolume->m_colourTintFromParticle &&
				   m_sortWithParticles == pFogVolume->m_sortWithParticles &&
				   m_lightingType == pFogVolume->m_lightingType &&
				   m_useGroundFogColour == pFogVolume->m_useGroundFogColour && 
				   m_useEffectEvoValues == pFogVolume->m_useEffectEvoValues;

	return !isEqual;
}

void ptxu_FogVolume::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 14);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "FogVolume RGB Tint Min");
	SendKeyframeDefnToEditor(buff, "FogVolume RGB Tint Max");
	SendKeyframeDefnToEditor(buff, "FogVolume Density/Range");
	SendKeyframeDefnToEditor(buff, "FogVolume Scale Min");
	SendKeyframeDefnToEditor(buff, "FogVolume Scale Max");
	SendKeyframeDefnToEditor(buff, "FogVolume Rotation Min");
	SendKeyframeDefnToEditor(buff, "FogBall Rotation Max");

	SendFloatDefnToEditor(buff, "Fall Off");
	SendFloatDefnToEditor(buff, "HDR Mult");

	// send non-keyframes
	const char* pComboItemNames[PTXU_FOGVOLUME_LIGHTING_NUM] = {"Fog HDR", "Directional"};
	SendComboDefnToEditor(buff, "Lighting Type", PTXU_FOGVOLUME_LIGHTING_NUM, pComboItemNames);

	SendBoolDefnToEditor(buff, "Colour From Particle");
	SendBoolDefnToEditor(buff, "Sort With Particles");
	SendBoolDefnToEditor(buff, "Use Ground Fog Colour");
	SendBoolDefnToEditor(buff, "Use Effect Evo Values");
}

void ptxu_FogVolume::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes
	SendKeyframeToEditor(buff, m_rgbTintMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_rgbTintMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_densityRangeKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_scaleMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_scaleMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_rotationMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_rotationMaxKFP, pParticleRule);

	// send non-keyframes
	SendFloatToEditor(buff, m_falloff);
	SendFloatToEditor(buff, m_hdrMult);
	SendComboItemToEditor(buff, m_lightingType);
	SendBoolToEditor(buff, m_colourTintFromParticle);
	SendBoolToEditor(buff, m_sortWithParticles);
	SendBoolToEditor(buff, m_useGroundFogColour);
	SendBoolToEditor(buff, m_useEffectEvoValues);
}

void ptxu_FogVolume::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_rgbTintMinKFP);
	ReceiveKeyframeFromEditor(buff, m_rgbTintMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_densityRangeKFP);
	ReceiveKeyframeFromEditor(buff, m_scaleMinKFP);
	ReceiveKeyframeFromEditor(buff, m_scaleMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_rotationMinKFP);
	ReceiveKeyframeFromEditor(buff, m_rotationMaxKFP);

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_falloff);
	ReceiveFloatFromEditor(buff, m_hdrMult);
	ReceiveComboItemFromEditor(buff, m_lightingType);
	ReceiveBoolFromEditor(buff, m_colourTintFromParticle);
	ReceiveBoolFromEditor(buff, m_sortWithParticles);
	ReceiveBoolFromEditor(buff, m_useGroundFogColour);
	ReceiveBoolFromEditor(buff, m_useEffectEvoValues);
}
#endif // RMPTFX_EDITOR


} // namespace rage
