// 
// rmptfx/ptxu_fire.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 
/*
// includes
#include "vfx/ptfx/behaviours/ptxu_fire.h"
#include "vfx/ptfx/behaviours/ptxu_fire_parser.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_Fire::ptxu_Fire()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Fire::SetDefaultData()
{
	// default keyframes

	// default other data
	m_velocityThresh = 0.0f;
	m_burnTime = 0.0f;
	m_burnStrength = 0.0f;
	m_peakTime = 0.0f;
	m_fuelLevel = 0.0f;
	m_fuelBurnRate = 0.0f;
	m_numGenerations = 0;
}

ptxu_Fire::ptxu_Fire(datResource& rsc)
	: ptxBehaviour(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_Fire::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Fire, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Fire, m_velocityThresh)
		SSTRUCT_FIELD(ptxu_Fire, m_burnTime)
		SSTRUCT_FIELD(ptxu_Fire, m_burnStrength)
		SSTRUCT_FIELD(ptxu_Fire, m_peakTime)
		SSTRUCT_FIELD(ptxu_Fire, m_fuelLevel)
		SSTRUCT_FIELD(ptxu_Fire, m_fuelBurnRate)
		SSTRUCT_FIELD(ptxu_Fire, m_numGenerations)
		SSTRUCT_IGNORE(ptxu_Fire, m_pad)
	SSTRUCT_END(ptxu_Fire)
}
#endif

IMPLEMENT_PLACE(ptxu_Fire);

void ptxu_Fire::InitPoint(	ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// Get pointers to the actual data.
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// set this particle as collidable
	pPointSB->SetFlag(PTXPOINT_FLAG_COLLIDES);

	UpdatePoint(params);
}

void ptxu_Fire::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
#if !__RESOURCECOMPILER
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();
#endif // !__RESOURCECOMPILER
	
	// return if this emitter isn't active
#if RMPTFX_EDITOR
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	if (pPointSB->GetFlag(PTXPOINT_FLAG_HAS_COLLIDED))
	{
#if !__RESOURCECOMPILER
		if (Mag(pPointDB->GetCurrVel()).Getf()>=m_velocityThresh)
		{
			// add the fire 
			ptfxFireInfo fireInfo;
			fireInfo.vPos = pPointSB->GetColnPos();
			fireInfo.vNormal = pPointSB->GetColnNormal();
			fireInfo.mtlId = pPointSB->GetColnMtlId();
			fireInfo.burnTime = m_burnTime;
			fireInfo.burnStrength = m_burnStrength;
			fireInfo.peakTime = m_peakTime;
			fireInfo.fuelLevel = m_fuelLevel;
			fireInfo.fuelBurnRate = m_fuelBurnRate;
			fireInfo.numGenerations = m_numGenerations;

			g_pPtfxCallbacks->AddFire(fireInfo);
		}
#endif
	}
}

void ptxu_Fire::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}

// editor code
#if RMPTFX_EDITOR
bool ptxu_Fire::IsActive()
{
	ptxu_Fire* pFire = static_cast<ptxu_Fire*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_velocityThresh == pFire->m_velocityThresh &&
				   m_burnTime == pFire->m_burnTime && 
				   m_burnStrength == pFire->m_burnStrength &&
				   m_peakTime == pFire->m_peakTime && 
				   m_fuelLevel == pFire->m_fuelLevel && 
				   m_fuelBurnRate == pFire->m_fuelBurnRate &&
				   m_numGenerations == pFire->m_numGenerations;

	return !isEqual;
}

void ptxu_Fire::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 7);

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Velocity Threshold", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Burn Time", 0.0f, 20.0f);
	SendFloatDefnToEditor(buff, "Burn Strength", 0.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Peak Time", 0.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Fuel Level", 0.0f, 100.0f);
	SendFloatDefnToEditor(buff, "Fuel Burn Rate", 0.0f, 10.0f);
	SendIntDefnToEditor(buff, "Num Generations", 0, 5);
}

void ptxu_Fire::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_velocityThresh);
	SendFloatToEditor(buff, m_burnTime);
	SendFloatToEditor(buff, m_burnStrength);
	SendFloatToEditor(buff, m_peakTime);
	SendFloatToEditor(buff, m_fuelLevel);
	SendFloatToEditor(buff, m_fuelBurnRate);
	SendIntToEditor(buff, m_numGenerations);
}

void ptxu_Fire::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{	
	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_velocityThresh);
	ReceiveFloatFromEditor(buff, m_burnTime);
	ReceiveFloatFromEditor(buff, m_burnStrength);
	ReceiveFloatFromEditor(buff, m_peakTime);
	ReceiveFloatFromEditor(buff, m_fuelLevel);
	ReceiveFloatFromEditor(buff, m_fuelBurnRate);
	ReceiveIntFromEditor(buff, m_numGenerations);
}
#endif // RMPTFX_EDITOR



} // namespace rage
*/