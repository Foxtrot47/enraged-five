<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Decal" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_dimensionsKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_alphaKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_decalId"/>
	<float name="m_velocityThresh"/>
	<float name="m_totalLife"/>
	<float name="m_fadeInTime"/>
	<float name="m_uvMultStart"/>
	<float name="m_uvMultEnd"/>
	<float name="m_uvMultTime"/>
	<float name="m_duplicateRejectDist"/>
	<bool name="m_flipU"/>
	<bool name="m_flipV"/>
	<bool name="m_proportionalSize"/>
	<bool name="m_useComplexColn"/>
</structdef>

</ParserSchema>