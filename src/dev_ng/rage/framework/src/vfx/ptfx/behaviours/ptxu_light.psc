<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="::rage::ptxu_Light" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
    <struct name="m_rgbMinKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_rgbMaxKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_intensityKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_rangeKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_coronaRgbMinKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_coronaRgbMaxKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_coronaIntensityKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_coronaSizeKFP" type="::rage::ptxKeyframeProp"/>
    <struct name="m_coronaFlareKFP" type="::rage::ptxKeyframeProp"/>
    <float name="m_coronaZBias"/>
    <bool name="m_coronaUseLightCol"/>
    <bool name="m_colourFromParticle"/>
    <bool name="m_colourPerFrame"/>
    <bool name="m_intensityPerFrame"/>
    <bool name="m_rangePerFrame"/>
    <bool name="m_castsShadows"/>
    <bool name="m_coronaNotInReflection"/>
    <bool name="m_coronaOnlyInReflection"/>
    <int name="m_lightType"/>
  </structdef>

</ParserSchema>