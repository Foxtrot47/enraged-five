<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_ZCull" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_heightKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_fadeDistKFP" type="::rage::ptxKeyframeProp"/>
	<int name="m_mode"/>
	<int name="m_referenceSpace"/>
</structdef>

</ParserSchema>