<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_FogVolume" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<struct name="m_rgbTintMinKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_rgbTintMaxKFP" type="::rage::ptxKeyframeProp"/>
	<struct name="m_densityRangeKFP" type="::rage::ptxKeyframeProp"/>
  <struct name="m_rotationMinKFP" type="::rage::ptxKeyframeProp"/>
  <struct name="m_rotationMaxKFP" type="::rage::ptxKeyframeProp"/>
  <struct name="m_scaleMinKFP" type="::rage::ptxKeyframeProp"/>
  <struct name="m_scaleMaxKFP" type="::rage::ptxKeyframeProp"/>
  <float name="m_falloff"/>
  <int name="m_lightingType"/>
  <bool name="m_colourTintFromParticle"/>
  <bool name="m_sortWithParticles"/>
  <bool name="m_useGroundFogColour"/>
</structdef>

</ParserSchema>
