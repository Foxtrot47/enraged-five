// 
// vfx/ptfx/behaviours/ptxu_light.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_LIGHT_H
#define PTXU_LIGHT_H


// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// enumerations
enum ptxu_Light_Type
{
	PTXU_LIGHT_TYPE_OMNI	= 0,
	PTXU_LIGHT_TYPE_SPOT_EFFECT_X,
	PTXU_LIGHT_TYPE_SPOT_EFFECT_Y,
	PTXU_LIGHT_TYPE_SPOT_EFFECT_Z,
	PTXU_LIGHT_TYPE_SPOT_EMITTER_X,
	PTXU_LIGHT_TYPE_SPOT_EMITTER_Y,
	PTXU_LIGHT_TYPE_SPOT_EMITTER_Z,

	PTXU_LIGHT_TYPE_NUM
};


// classes
class ptxu_Light : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Light();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Light(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Light);

		// info
		const char* GetName() {return "ptxu_Light";}											// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_LIGHT;}										// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}														// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}												// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);
		void UpdatePoint(ptxBehaviourParams& params);											// called from InitPoint and UpdatePoint to update the particle accordingly 

		void UpdateFinalize(ptxBehaviourParams& params);

		void SetKeyframeDefns();																// sets the keyframe definitions on the keyframe properties
		void SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList);							// sets the keyframe definitions on the evolved keyframe properties in the evolution list

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Light";}											// name of the behaviour shown in the editor
		bool IsActive();																		// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif


	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data (light specific)
		ptxKeyframeProp m_rgbMinKFP;		
		ptxKeyframeProp m_rgbMaxKFP;		
		ptxKeyframeProp m_intensityKFP;	
		ptxKeyframeProp m_rangeKFP;	

		// keyframe data (corona specific)
		ptxKeyframeProp m_coronaRgbMinKFP;	
		ptxKeyframeProp m_coronaRgbMaxKFP;		
		ptxKeyframeProp m_coronaIntensityKFP;	
		ptxKeyframeProp m_coronaSizeKFP;
		ptxKeyframeProp m_coronaFlareKFP;	

		// non keyframe data (corona specific)
		float m_coronaZBias;
		bool m_coronaUseLightCol;

		// non keyframe data (light specific)
		bool m_colourFromParticle;
		bool m_colourPerFrame;
		bool m_intensityPerFrame;
		bool m_rangePerFrame;
		bool m_castsShadows;
		bool m_coronaNotInReflection;
		bool m_coronaOnlyInReflection;

		//datPadding<1> m_pad;

		int m_lightType;

		// static data
		// light specific
		static ptxKeyframeDefn sm_rgbMinDefn;		
		static ptxKeyframeDefn sm_rgbMaxDefn;		
		static ptxKeyframeDefn sm_intensityDefn;	
		static ptxKeyframeDefn sm_rangeDefn;

		// corona specific
		static ptxKeyframeDefn sm_coronaRgbMinDefn;		
		static ptxKeyframeDefn sm_coronaRgbMaxDefn;		
		static ptxKeyframeDefn sm_coronaIntensityDefn;	
		static ptxKeyframeDefn sm_coronaSizeDefn;
		static ptxKeyframeDefn sm_coronaFlareDefn;

};


} // namespace rage


#endif // PTXU_LIGHT_H 

