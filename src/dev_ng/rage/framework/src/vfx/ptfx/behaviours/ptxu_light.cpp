// 
// vfx/ptfx/behaviours/ptxu_light.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "vfx/ptfx/behaviours/ptxu_light.h"
#include "vfx/ptfx/behaviours/ptxu_light_parser.h"

#include "vfx/ptfx/ptfxcallbacks.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_Light::sm_rgbMinDefn(						"Light Color Min",			atHashValue("ptxu_Light:m_rgbMinKFP"),						PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Min",			"Green Min",		"Blue Min");
ptxKeyframeDefn ptxu_Light::sm_rgbMaxDefn(						"Light Color Max",			atHashValue("ptxu_Light:m_rgbMaxKFP"),						PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Max",			"Green Max",		"Blue Max");
ptxKeyframeDefn ptxu_Light::sm_intensityDefn(					"Light Intensity",			atHashValue("ptxu_Light:m_intensityKFP"),					PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10000.0f,	0.1f),		"Intensity Min",	"Intensity Max",	"Falloff Min",		"Falloff Max");
ptxKeyframeDefn ptxu_Light::sm_rangeDefn(						"Light Range",				atHashValue("ptxu_Light:m_rangeKFP"),						PTXKEYFRAMETYPE_FLOAT4,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		100.0f,		0.1f),		"Range Min",		"Range Max",		"Angle Min",		"Angle Max");
ptxKeyframeDefn ptxu_Light::sm_coronaRgbMinDefn(				"Corona Color Min",			atHashValue("ptxu_Light:m_coronaRgbMinKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Min",			"Green Min",		"Blue Min");
ptxKeyframeDefn ptxu_Light::sm_coronaRgbMaxDefn(				"Corona Color Max",			atHashValue("ptxu_Light:m_coronaRgbMaxKFP"),				PTXKEYFRAMETYPE_FLOAT3,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Red Max",			"Green Max",		"Blue Max");
ptxKeyframeDefn ptxu_Light::sm_coronaIntensityDefn(				"Corona Intensity",			atHashValue("ptxu_Light:m_coronaIntensityKFP"),				PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		10000.0f,	0.1f),		"Intensity Min",	"Intensity Max");
ptxKeyframeDefn ptxu_Light::sm_coronaSizeDefn(					"Corona Size",				atHashValue("ptxu_Light:m_coronaSizeKFP"),					PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		100.0f,		0.1f),		"Size Min",			"Size Max");
ptxKeyframeDefn ptxu_Light::sm_coronaFlareDefn(					"Corona Flare",				atHashValue("ptxu_Light:m_coronaFlareKFP"),					PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO),						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		1.0f,		0.01f),		"Flare Min",		"Flare Max");


// code
#if RMPTFX_XML_LOADING
ptxu_Light::ptxu_Light()
{
	// create keyframes
	m_rgbMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_rgbMinKFP"));
	m_rgbMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_rgbMaxKFP"));
	m_intensityKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_intensityKFP"));
	m_rangeKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_rangeKFP"));
	m_coronaRgbMinKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_coronaRgbMinKFP"));
	m_coronaRgbMaxKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_coronaRgbMaxKFP"));
	m_coronaIntensityKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_coronaIntensityKFP"));
	m_coronaSizeKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_coronaSizeKFP"));
	m_coronaFlareKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_Light:m_coronaFlareKFP"));

	m_keyframePropList.AddKeyframeProp(&m_rgbMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rgbMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_intensityKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_rangeKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_coronaRgbMinKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_coronaRgbMaxKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_coronaIntensityKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_coronaSizeKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_coronaFlareKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Light::SetDefaultData()
{
	// default keyframes
	m_rgbMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_rgbMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ONE));
	m_intensityKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_THREE));
	m_rangeKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_THREE));
	m_coronaRgbMinKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_coronaRgbMaxKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_coronaIntensityKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_coronaSizeKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_coronaFlareKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_coronaZBias = 0.0f;
	m_coronaUseLightCol = true;
	m_colourFromParticle = true;
	m_colourPerFrame = false;
	m_intensityPerFrame = false;
	m_rangePerFrame = false;
	m_castsShadows = false;
	m_coronaNotInReflection = false;
	m_coronaOnlyInReflection = false;
	m_lightType = PTXU_LIGHT_TYPE_OMNI;
}

ptxu_Light::ptxu_Light(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_rgbMinKFP(rsc)
	, m_rgbMaxKFP(rsc)
	, m_intensityKFP(rsc)
	, m_rangeKFP(rsc)
	, m_coronaRgbMinKFP(rsc)
	, m_coronaRgbMaxKFP(rsc)
	, m_coronaIntensityKFP(rsc)
	, m_coronaSizeKFP(rsc)
	, m_coronaFlareKFP(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_Light::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Light, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Light, m_rgbMinKFP)
		SSTRUCT_FIELD(ptxu_Light, m_rgbMaxKFP)
		SSTRUCT_FIELD(ptxu_Light, m_intensityKFP)
		SSTRUCT_FIELD(ptxu_Light, m_rangeKFP)
		SSTRUCT_FIELD(ptxu_Light, m_coronaRgbMinKFP)
		SSTRUCT_FIELD(ptxu_Light, m_coronaRgbMaxKFP)
		SSTRUCT_FIELD(ptxu_Light, m_coronaIntensityKFP)
		SSTRUCT_FIELD(ptxu_Light, m_coronaSizeKFP)
		SSTRUCT_FIELD(ptxu_Light, m_coronaFlareKFP)
		SSTRUCT_FIELD(ptxu_Light, m_coronaZBias)
		SSTRUCT_FIELD(ptxu_Light, m_coronaUseLightCol)
		SSTRUCT_FIELD(ptxu_Light, m_colourFromParticle)
		SSTRUCT_FIELD(ptxu_Light, m_colourPerFrame)
		SSTRUCT_FIELD(ptxu_Light, m_intensityPerFrame)
		SSTRUCT_FIELD(ptxu_Light, m_rangePerFrame)
		SSTRUCT_FIELD(ptxu_Light, m_castsShadows)
		SSTRUCT_FIELD(ptxu_Light, m_coronaNotInReflection)
		SSTRUCT_FIELD(ptxu_Light, m_coronaOnlyInReflection)
		//SSTRUCT_IGNORE(ptxu_Light, m_pad)
		SSTRUCT_FIELD(ptxu_Light, m_lightType)
	SSTRUCT_END(ptxu_Light)
}
#endif

IMPLEMENT_PLACE(ptxu_Light);

void ptxu_Light::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointLight)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Light::UpdatePoint(ptxBehaviourParams& 
#if !__RESOURCECOMPILER
							 params
#endif
							 )
{
	// cache data from params
#if !__RESOURCECOMPILER
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;

	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle life ratio key time
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	// return if this emitter isn't active
#if RMPTFX_EDITOR
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	// return if the particle has expired already - unless it's the first frame
	// this allows us to tune lights that last a single frame only by setting the particle life to zero
	if (pPointDB->GetLifeRatio()>1.0f && pPointSB->GetFlag(PTXPOINT_FLAG_IS_BEING_INITIALISED)==false)
	{
		return;
	}

	// initialise the light info
	ptfxLightInfo lightInfo;
	lightInfo.pEffectInst = pEffectInst;
	lightInfo.pEmitterInst = pEmitterInst;
	lightInfo.pPointItem = pPointItem;
	lightInfo.vPos = pPointDB->GetCurrPos();
	lightInfo.lightRange = 0.0f;
	lightInfo.coronaSize = 0.0f;
	lightInfo.coronaZBias = m_coronaZBias;
	lightInfo.castsShadows = m_castsShadows;
	lightInfo.coronaNotInReflection = m_coronaNotInReflection;
	lightInfo.coronaOnlyInReflection = m_coronaOnlyInReflection;
	lightInfo.coronasAdded = false;
	lightInfo.lightType = m_lightType;

	// calculate the shared bias values using the light keyframe data
	ScalarV vBiasIntensity;
	m_intensityPerFrame ? vBiasIntensity = ptxRandomTable::GetValueV() : vBiasIntensity = GetBiasValue(m_intensityKFP, pPointSB->GetRandIndex());

	ScalarV vBiasRange;
	m_rangePerFrame ? vBiasRange = ptxRandomTable::GetValueV() : vBiasRange = GetBiasValue(m_rangeKFP, pPointSB->GetRandIndex());

	ScalarV vBiasColour;
	m_colourPerFrame ? vBiasColour = ptxRandomTable::GetValueV() : vBiasColour = GetBiasValue(m_rgbMinKFP, pPointSB->GetRandIndex());

	ptxEvolutionList* pEvolutionList = params.pEvolutionList;

	// SET UP THE LIGHT
	// calculate the light intensity
	bool lightColCalculated = false;
	Vec4V vLightIntensityMinMax = m_intensityKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	ScalarV vLightIntensity = Lerp(vBiasIntensity, SplatX(vLightIntensityMinMax), SplatY(vLightIntensityMinMax));
	ScalarV vLightFalloff = Lerp(vBiasIntensity, SplatZ(vLightIntensityMinMax), SplatW(vLightIntensityMinMax));
	if (IsZeroAll(vLightIntensity)==false)
	{	
		// calculate the light range
		Vec4V vLightRangeMinMax = m_rangeKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
		ScalarV vLightRange = Lerp(vBiasRange, SplatX(vLightRangeMinMax), SplatY(vLightRangeMinMax));
		ScalarV vLightAngle = Lerp(vBiasRange, SplatZ(vLightRangeMinMax), SplatW(vLightRangeMinMax));
		if (IsZeroAll(vLightRange)==false)
		{
			// calculate the light colour
			lightColCalculated = true;
			Vec4V vLightColour;
			if (m_colourFromParticle)
			{
				vLightColour = pPointDB->GetColour().GetRGBA();
			}
			else
			{
				Vec4V vLightColourMin = m_rgbMinKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				Vec4V vLightColourMax = m_rgbMaxKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				vLightColour = Lerp(vBiasColour, vLightColourMin, vLightColourMax);
			}

			if (IsZeroAll(vLightColour)==false)
			{
				// the light is valid - set up the light info
				lightInfo.lightCol = Color32(vLightColour);
				lightInfo.lightIntensity = vLightIntensity.Getf()*pEffectInst->GetAlphaTint()*pEffectInst->GetLodAlphaMult(); 
				lightInfo.lightFalloff = vLightFalloff.Getf();
				lightInfo.lightRange = vLightRange.Getf();
				lightInfo.lightAngle = vLightAngle.Getf();
			}
		}
	}

	// SET UP THE CORONA
	// calculate the corona intensity
	Vec4V vCoronaIntensityMinMax = m_coronaIntensityKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	ScalarV vCoronaIntensity = Lerp(vBiasIntensity, SplatX(vCoronaIntensityMinMax), SplatY(vCoronaIntensityMinMax));
	if (IsZeroAll(vCoronaIntensity)==false)
	{	
		// calculate the corona size
		Vec4V vCoronaSizeMinMax = m_coronaSizeKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
		ScalarV vCoronaSize = Lerp(vBiasRange, SplatX(vCoronaSizeMinMax), SplatY(vCoronaSizeMinMax));
		if (IsZeroAll(vCoronaSize)==false)
		{
			// calculate the corona colour
			Vec4V vCoronaColour;
			if (m_coronaUseLightCol)
			{
				if (lightColCalculated)
				{
					vCoronaColour = lightInfo.lightCol.GetRGBA();
				}
				else
				{
					Vec4V vLightColourMin = m_rgbMinKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
					Vec4V vLightColourMax = m_rgbMaxKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
					vCoronaColour = Lerp(vBiasColour, vLightColourMin, vLightColourMax);
				}
			}
			else
			{
				Vec4V vCoronaColourMin = m_coronaRgbMinKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				Vec4V vCoronaColourMax = m_coronaRgbMaxKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				vCoronaColour = Lerp(vBiasColour, vCoronaColourMin, vCoronaColourMax);
			}

			if (IsZeroAll(vCoronaColour)==false)
			{
				// the corona is valid - set up the light info

				// TODO: calculate the corona flare
				ScalarV vBiasFlare = GetBiasValue(m_rgbMinKFP, pPointSB->GetRandIndex());
				Vec4V vCoronaFlareMinMax = m_coronaFlareKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
				ScalarV vCoronaFlare = Lerp(vBiasFlare, SplatX(vCoronaFlareMinMax), SplatY(vCoronaFlareMinMax));

				lightInfo.coronaCol = Color32(vCoronaColour);
				lightInfo.coronaIntensity = vCoronaIntensity.Getf()*pEffectInst->GetAlphaTint()*pEffectInst->GetLodAlphaMult(); 
				lightInfo.coronaSize = vCoronaSize.Getf();
				lightInfo.coronaFlare = vCoronaFlare.Getf();
			}
		}
	}

	// add the light
	if (lightInfo.lightRange>0.0f || lightInfo.coronaSize>0.0f)
	{
		g_pPtfxCallbacks->AddLight(lightInfo);
	}
#endif // !__RESOURCECOMPILER
}

void ptxu_Light::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}

void ptxu_Light::SetKeyframeDefns()
{
	m_rgbMinKFP.GetKeyframe().SetDefn(&sm_rgbMinDefn);
	m_rgbMaxKFP.GetKeyframe().SetDefn(&sm_rgbMaxDefn);
	m_intensityKFP.GetKeyframe().SetDefn(&sm_intensityDefn);
	m_rangeKFP.GetKeyframe().SetDefn(&sm_rangeDefn);
	m_coronaRgbMinKFP.GetKeyframe().SetDefn(&sm_coronaRgbMinDefn);
	m_coronaRgbMaxKFP.GetKeyframe().SetDefn(&sm_coronaRgbMaxDefn);
	m_coronaIntensityKFP.GetKeyframe().SetDefn(&sm_coronaIntensityDefn);
	m_coronaSizeKFP.GetKeyframe().SetDefn(&sm_coronaSizeDefn);
	m_coronaFlareKFP.GetKeyframe().SetDefn(&sm_coronaFlareDefn);
}

void ptxu_Light::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_rgbMinKFP, &sm_rgbMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_rgbMaxKFP, &sm_rgbMaxDefn);
	pEvolutionList->SetKeyframeDefn(&m_intensityKFP, &sm_intensityDefn);
	pEvolutionList->SetKeyframeDefn(&m_rangeKFP, &sm_rangeDefn);
	pEvolutionList->SetKeyframeDefn(&m_coronaRgbMinKFP, &sm_coronaRgbMinDefn);
	pEvolutionList->SetKeyframeDefn(&m_coronaRgbMaxKFP, &sm_coronaRgbMaxDefn);
	pEvolutionList->SetKeyframeDefn(&m_coronaIntensityKFP, &sm_coronaIntensityDefn);
	pEvolutionList->SetKeyframeDefn(&m_coronaSizeKFP, &sm_coronaSizeDefn);
	pEvolutionList->SetKeyframeDefn(&m_coronaFlareKFP, &sm_coronaFlareDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Light::IsActive()
{
	ptxu_Light* pLight = static_cast<ptxu_Light*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_rgbMinKFP == pLight->m_rgbMinKFP &&
				   m_rgbMaxKFP == pLight->m_rgbMaxKFP &&
				   m_intensityKFP == pLight->m_intensityKFP &&
				   m_rangeKFP == pLight->m_rangeKFP &&
				   m_coronaRgbMinKFP == pLight->m_coronaRgbMinKFP &&
				   m_coronaRgbMaxKFP == pLight->m_coronaRgbMaxKFP &&
				   m_coronaIntensityKFP == pLight->m_coronaIntensityKFP &&
				   m_coronaSizeKFP == pLight->m_coronaSizeKFP &&
				   m_coronaFlareKFP == pLight->m_coronaFlareKFP &&
				   m_coronaZBias == pLight->m_coronaZBias &&
				   m_coronaUseLightCol == pLight->m_coronaUseLightCol &&
				   m_colourFromParticle == pLight->m_colourFromParticle &&
				   m_colourPerFrame == pLight->m_colourPerFrame &&
				   m_intensityPerFrame == pLight->m_intensityPerFrame &&
				   m_rangePerFrame == pLight->m_rangePerFrame && 
				   m_castsShadows == pLight->m_castsShadows && 
				   m_coronaNotInReflection == pLight->m_coronaNotInReflection &&
				   m_coronaOnlyInReflection == pLight->m_coronaOnlyInReflection &&
				   m_lightType == pLight->m_lightType;

	return !isEqual;
}

void ptxu_Light::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 19);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Light RGB Min");
	SendKeyframeDefnToEditor(buff, "Light RGB Max");
	SendKeyframeDefnToEditor(buff, "Light Intensity");
	SendKeyframeDefnToEditor(buff, "Light Range");
	SendKeyframeDefnToEditor(buff, "Corona RGB Min");
	SendKeyframeDefnToEditor(buff, "Corona RGB Max");
	SendKeyframeDefnToEditor(buff, "Corona Intensity");
	SendKeyframeDefnToEditor(buff, "Corona Size");
	SendKeyframeDefnToEditor(buff, "Corona Flare");

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Corona Z Bias");
	SendBoolDefnToEditor(buff, "Corona Use Light Colour");
	SendBoolDefnToEditor(buff, "Colour From Particle");
	SendBoolDefnToEditor(buff, "Colour Per Frame");
	SendBoolDefnToEditor(buff, "Intensity Per Frame");
	SendBoolDefnToEditor(buff, "Range Per Frame");
	SendBoolDefnToEditor(buff, "Casts Shadows");
	SendBoolDefnToEditor(buff, "Corona Not In Reflection");
	SendBoolDefnToEditor(buff, "Corona Only In Reflection");

	const char* pComboItemNames[PTXU_LIGHT_TYPE_NUM] = {"Omni", "Spot (Effect X)", "Spot (Effect Y)", "Spot (Effect Z)", "Spot (Emitter X)", "Spot (Emitter Y)", "Spot (Emitter Z)"};
	SendComboDefnToEditor(buff, "Light Type", PTXU_LIGHT_TYPE_NUM, pComboItemNames);
}

void ptxu_Light::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes
	SendKeyframeToEditor(buff, m_rgbMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_rgbMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_intensityKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_rangeKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_coronaRgbMinKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_coronaRgbMaxKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_coronaIntensityKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_coronaSizeKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_coronaFlareKFP, pParticleRule);

	// send non-keyframes
	SendFloatToEditor(buff, m_coronaZBias);
	SendBoolToEditor(buff, m_coronaUseLightCol);
	SendBoolToEditor(buff, m_colourFromParticle);
	SendBoolToEditor(buff, m_colourPerFrame);
	SendBoolToEditor(buff, m_intensityPerFrame);
	SendBoolToEditor(buff, m_rangePerFrame);
	SendBoolToEditor(buff, m_castsShadows);
	SendBoolToEditor(buff, m_coronaNotInReflection);
	SendBoolToEditor(buff, m_coronaOnlyInReflection);
	SendIntToEditor(buff, m_lightType);
}

void ptxu_Light::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_rgbMinKFP);
	ReceiveKeyframeFromEditor(buff, m_rgbMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_intensityKFP);
	ReceiveKeyframeFromEditor(buff, m_rangeKFP);
	ReceiveKeyframeFromEditor(buff, m_coronaRgbMinKFP);
	ReceiveKeyframeFromEditor(buff, m_coronaRgbMaxKFP);
	ReceiveKeyframeFromEditor(buff, m_coronaIntensityKFP);
	ReceiveKeyframeFromEditor(buff, m_coronaSizeKFP);
	ReceiveKeyframeFromEditor(buff, m_coronaFlareKFP);

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_coronaZBias);
	ReceiveBoolFromEditor(buff, m_coronaUseLightCol);
	ReceiveBoolFromEditor(buff, m_colourFromParticle);
	ReceiveBoolFromEditor(buff, m_colourPerFrame);
	ReceiveBoolFromEditor(buff, m_intensityPerFrame);
	ReceiveBoolFromEditor(buff, m_rangePerFrame);
	ReceiveBoolFromEditor(buff, m_castsShadows);
	ReceiveBoolFromEditor(buff, m_coronaNotInReflection);
	ReceiveBoolFromEditor(buff, m_coronaOnlyInReflection);
	ReceiveIntFromEditor(buff, m_lightType);
}
#endif // RMPTFX_EDITOR


} // namespace rage
