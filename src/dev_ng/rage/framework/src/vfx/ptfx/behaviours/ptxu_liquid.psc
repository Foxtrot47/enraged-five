<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::ptxu_Liquid" base="::rage::ptxBehaviour" cond="RMPTFX_XML_LOADING">
	<float name="m_velocityThresh"/>
	<int name="m_liquidType"/>
	<float name="m_poolStartSize"/>
	<float name="m_poolEndSize"/>
	<float name="m_poolGrowthRate"/>
	<float name="m_trailWidthMin"/>
	<float name="m_trailWidthMax"/>
</structdef>

</ParserSchema>