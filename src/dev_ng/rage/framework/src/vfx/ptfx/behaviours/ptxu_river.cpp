// 
// vfx/ptfx/behaviours/ptxu_river.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "vfx/ptfx/behaviours/ptxu_river.h"
#include "vfx/ptfx/behaviours/ptxu_river_parser.h"

#include "vfx/ptfx/ptfxcallbacks.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_River::ptxu_River()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_River::SetDefaultData()
{
	// default keyframes

	// default other data
	m_velocityMult = 100.0f;
	m_influence = 0.0f;
}

ptxu_River::ptxu_River(datResource& rsc)
	: ptxBehaviour(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_River::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_River, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_River, m_velocityMult)
		SSTRUCT_FIELD(ptxu_River, m_influence)
		SSTRUCT_IGNORE(ptxu_River, m_pad)
	SSTRUCT_END(ptxu_River)
}
#endif

IMPLEMENT_PLACE(ptxu_River);

void ptxu_River::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointRiver)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_River::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	
	ScalarV	vDeltaTime = params.vDeltaTime;

	ScalarV vVelocityMult = ScalarVFromF32(m_velocityMult);
	ScalarV vRiverInfluence = ScalarVFromF32(m_influence);

	// get the particle data (single and double buffered)
	//ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	Vec3V vRiverVel;

#if __RESOURCECOMPILER
	vRiverVel = Vec3V(V_ZERO);
#else
	g_pPtfxCallbacks->GetRiverVel(pPointDB->GetCurrPos(), vRiverVel);
#endif

	vRiverVel *= vVelocityMult;

	// calculate the difference between the particle and air velocity
	Vec3V vDeltaVel = vRiverVel - pPointDB->GetCurrVel();

	// determine how quickly the particle velocity should match the wind velocity
	// if running at 30fps when a wind influence of 30 would match it instantly i.e. (delta mult = 1.0f)
	// a wind influence of 1 would mean it would take 1 second to match the wind velocity (delta mult = 1/30)
	ScalarV vDeltaMult = vRiverInfluence * vDeltaTime;
	vDeltaMult = SelectFT(IsGreaterThan(vDeltaMult, ScalarV(V_ONE)), vDeltaMult, ScalarV(V_ONE));

	// apply the scale
	vDeltaVel *= vDeltaMult;

	// update the particle's velocity
	pPointDB->IncCurrVel(vDeltaVel);
}

void ptxu_River::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_River::IsActive()
{
	ptxu_River* pRiver = static_cast<ptxu_River*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_velocityMult == pRiver->m_velocityMult &&
				   m_influence == pRiver->m_influence;

	return !isEqual;
}

void ptxu_River::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 2);

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Velocity Mult", 0.0f, 1000.0f);
	SendFloatDefnToEditor(buff, "Influence", 0.0f, 10.0f);
}

void ptxu_River::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_velocityMult);
	SendFloatToEditor(buff, m_influence);
}

void ptxu_River::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_velocityMult);
	ReceiveFloatFromEditor(buff, m_influence);
}
#endif // RMPTFX_EDITOR



} // namespace rage
