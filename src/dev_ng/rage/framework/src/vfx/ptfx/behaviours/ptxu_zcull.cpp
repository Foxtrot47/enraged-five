// 
// vfx/ptfx/behaviours/ptxu_zcull.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

// includes
#include "vfx/ptfx/behaviours/ptxu_zcull.h"
#include "vfx/ptfx/behaviours/ptxu_zcull_parser.h"

#include "vfx/ptfx/ptfxcallbacks.h"

#include "rmptfx/ptxmanager.h"


// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


//																name						propertyid													type						default									  xmin		xmax		xdelta			  ymin		ymax		ydelta		labels														
ptxKeyframeDefn ptxu_ZCull::sm_heightDefn(						"Height",					atHashValue("ptxu_ZCull:m_heightKFP"),						PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO), 						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(-1000.0f,	10000.0f,	0.5f),		"Height Min",		"Height Max");
ptxKeyframeDefn ptxu_ZCull::sm_fadeDistDefn(					"Fade Dist",				atHashValue("ptxu_ZCull:m_fadeDistKFP"),					PTXKEYFRAMETYPE_FLOAT2,		Vec4V(V_ZERO), 						Vec3V(0.0f,		1.0f,		0.01f),		Vec3V(0.0f,		100.0f,		0.1f),		"Fade Dist Min",	"Fade Dist Max");


// code
#if RMPTFX_XML_LOADING
ptxu_ZCull::ptxu_ZCull()
{
	// create keyframes
	m_heightKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_ZCull:m_heightKFP"));
	m_fadeDistKFP.Init(Vec4V(V_ZERO), atHashValue("ptxu_ZCull:m_fadeDistKFP"));

	m_keyframePropList.AddKeyframeProp(&m_heightKFP, NULL);
	m_keyframePropList.AddKeyframeProp(&m_fadeDistKFP, NULL);

	// set the keyframe definitions
	SetKeyframeDefns();

	// set default data
	SetDefaultData();

	// init global data
	m_pGlobalData_NOTUSED = NULL;
}
#endif

void ptxu_ZCull::SetDefaultData()
{
	// default keyframes
	m_heightKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));
	m_fadeDistKFP.GetKeyframe().Init(ScalarV(V_ZERO), Vec4V(V_ZERO));

	// default other data
	m_mode = PTXU_ZCULL_MODE_CULL_UNDER;
	m_referenceSpace = PTXU_ZCULL_REFERENCESPACE_EFFECT;
}

ptxu_ZCull::ptxu_ZCull(datResource& rsc)
	: ptxBehaviour(rsc)
	, m_heightKFP(rsc)
	, m_fadeDistKFP(rsc)
{
	m_pGlobalData_NOTUSED = NULL;
}

#if __DECLARESTRUCT
void ptxu_ZCull::DeclareStruct(datTypeStruct& s)
{
	m_pGlobalData_NOTUSED = NULL; 

	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_ZCull, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_ZCull, m_heightKFP)
		SSTRUCT_FIELD(ptxu_ZCull, m_fadeDistKFP)
		SSTRUCT_IGNORE(ptxu_ZCull, m_pGlobalData_NOTUSED)
		SSTRUCT_FIELD(ptxu_ZCull, m_mode)
		SSTRUCT_FIELD(ptxu_ZCull, m_referenceSpace)
		SSTRUCT_IGNORE(ptxu_ZCull, m_pad)
	SSTRUCT_END(ptxu_ZCull)
}
#endif

IMPLEMENT_PLACE(ptxu_ZCull);

void ptxu_ZCull::InitPoint(ptxBehaviourParams& params)
{
#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointZCull)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_ZCull::UpdatePoint(ptxBehaviourParams& params)
{	
	// cache data from params
	ptxEffectInst* pEffectInst = params.pEffectInst;
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();

	// get the particle life ratio key time
	ScalarV vKeyTimePoint = pPointDB->GetKeyTime();

	// cache some data that we'll need later
	ScalarV vOne = ScalarV(V_ONE);	
	ScalarV vZero = ScalarV(V_ZERO);	
	BoolV vCompareLessThan = m_mode==PTXU_ZCULL_MODE_CULL_UNDER || m_mode==PTXU_ZCULL_MODE_CLAMP_UNDER ? BoolV(V_T_T_T_T) : BoolV(V_F_F_F_F);
	ScalarV vWaterZ = vZero;
#if	!__RESOURCECOMPILER
	if (m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_WATER_Z_POINT)
	{	
		vWaterZ = ScalarVFromF32(g_pPtfxCallbacks->GetWaterZ(pPointDB->GetCurrPos(), pEffectInst));
	}
	else if (m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_WATER_Z_CAMERA)
	{
		vWaterZ = ScalarVFromF32(g_pPtfxCallbacks->GetGameCamWaterZ());
	}
#endif

	// get the keyframe data
	ptxEvolutionList* pEvolutionList = params.pEvolutionList;
	Vec4V vHeightMinMax = m_heightKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());
	Vec4V vFadeDistMinMax = m_fadeDistKFP.Query(vKeyTimePoint, pEvolutionList, pPointSB->GetEvoValues());

	// calculate the bias values
	ScalarV vBiasHeight = GetBiasValue(m_heightKFP, pPointSB->GetRandIndex());
	ScalarV vBiasFadeDist = GetBiasValue(m_fadeDistKFP, pPointSB->GetRandIndex());

	// calculate the lerped zcull data between the min and max keyframe data
	ScalarV vHeight = Lerp(vBiasHeight, SplatX(vHeightMinMax), SplatY(vHeightMinMax));
	ScalarV vFadeDist = Lerp(vBiasFadeDist, SplatX(vFadeDistMinMax), SplatY(vFadeDistMinMax));

	// get the z value to cull at
	ScalarV vZToCullAt = ScalarV(V_ZERO);
	if (m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_EFFECT)
	{
		vZToCullAt = SplatZ(pEffectInst->GetCurrPos());
	}
	if (m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_EMITTER)
	{
		vZToCullAt = SplatZ(pEmitterInst->GetDataDB()->GetCurrPosWld());
	}
	else if (m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_WATER_Z_CAMERA || m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_WATER_Z_POINT)
	{
		vZToCullAt = vWaterZ;
	}

	// get z cull keyframe info and add on the offset 
	vZToCullAt += vHeight;

	// kill the particle if required and calc its distance from the plane
	ScalarV vZPoint = SplatZ(pPointDB->GetCurrPos());
	ScalarV vIsPointAbove = SelectFT(IsGreaterThanOrEqual(vZPoint, vZToCullAt), vZero, vOne);
	ScalarV vIsPointBelow = SelectFT(IsLessThanOrEqual(vZPoint, vZToCullAt), vZero, vOne);
	ScalarV vIsPointFlagged = SelectFT(vCompareLessThan, vIsPointAbove, vIsPointBelow);

	// fade the particle if required
	ScalarV vDistFromCullZ = SelectFT(vCompareLessThan, vZToCullAt-vZPoint, vZPoint-vZToCullAt);
	vDistFromCullZ = SelectFT(IsLessThanOrEqual(vDistFromCullZ, vZero), vDistFromCullZ, vZero);
	ScalarV vFadeAlpha = SelectFT(IsLessThan(vDistFromCullZ, vFadeDist), vOne, vDistFromCullZ/vFadeDist);

	// set the data on the particle	
	if (vIsPointFlagged.Getf()>0.0f)
	{
		if (m_mode==PTXU_ZCULL_MODE_CLAMP_UNDER || m_mode==PTXU_ZCULL_MODE_CLAMP_OVER)
		{
			Vec3V vCurrPos = pPointDB->GetCurrPos();
			vCurrPos.SetZ(vZToCullAt);
			pPointDB->SetCurrPos(vCurrPos);
		}
		else
		{
			pPointDB->SetIsDead(true);
		}
	}

	pPointDB->SetAlphaMult((u8)(255*vFadeAlpha.Getf()));    	
}

void ptxu_ZCull::UpdateFinalize(ptxBehaviourParams& params)
{
	if (m_referenceSpace==PTXU_ZCULL_REFERENCESPACE_WATER_Z_POINT)
	{
		UpdatePoint(params);
	}
}

void ptxu_ZCull::SetKeyframeDefns()
{
	m_heightKFP.GetKeyframe().SetDefn(&sm_heightDefn);
	m_fadeDistKFP.GetKeyframe().SetDefn(&sm_fadeDistDefn);
}

void ptxu_ZCull::SetEvolvedKeyframeDefns(ptxEvolutionList* pEvolutionList)
{
	pEvolutionList->SetKeyframeDefn(&m_heightKFP, &sm_heightDefn);
	pEvolutionList->SetKeyframeDefn(&m_fadeDistKFP, &sm_fadeDistDefn);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_ZCull::IsActive()
{
	ptxu_ZCull* pZCull = static_cast<ptxu_ZCull*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_heightKFP == pZCull->m_heightKFP && 
				   m_fadeDistKFP == pZCull->m_fadeDistKFP &&
				   m_mode == pZCull->m_mode &&
				   m_referenceSpace == pZCull->m_referenceSpace;

	return !isEqual;
}

void ptxu_ZCull::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 4);

	// send keyframes
	SendKeyframeDefnToEditor(buff, "Height");
	SendKeyframeDefnToEditor(buff, "Fade Dist");

	// send non-keyframes
	const char* pModeComboItemNames[PTXU_ZCULL_MODE_NUM] = {"Cull Under", "Cull Over", "Clamp Under", "Clamp Over"};
	SendComboDefnToEditor(buff, "Mode", PTXU_ZCULL_MODE_NUM, pModeComboItemNames);

	const char* pRefSpaceComboItemNames[PTXU_ZCULL_REFERENCESPACE_NUM] = {"World", "Effect", "Emitter", "WaterZ (Camera)", "WaterZ (Point)"};
	SendComboDefnToEditor(buff, "Reference Space", PTXU_ZCULL_REFERENCESPACE_NUM, pRefSpaceComboItemNames);
}

void ptxu_ZCull::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);

	// send keyframes
	SendKeyframeToEditor(buff, m_heightKFP, pParticleRule);
	SendKeyframeToEditor(buff, m_fadeDistKFP, pParticleRule);

	// send non-keyframes
	SendIntToEditor(buff, m_mode);
	SendIntToEditor(buff, m_referenceSpace);
}

void ptxu_ZCull::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes
	ReceiveKeyframeFromEditor(buff, m_heightKFP);
	ReceiveKeyframeFromEditor(buff, m_fadeDistKFP);

	// receive non-keyframes
	ReceiveIntFromEditor(buff, m_mode);
	ReceiveIntFromEditor(buff, m_referenceSpace);
}
#endif // RMPTFX_EDITOR


} // namespace rage
