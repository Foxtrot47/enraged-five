// 
// vfx/ptfx/behaviours/ptxu_liquid.cpp 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 


// includes
#include "vfx/ptfx/behaviours/ptxu_liquid.h"
#include "vfx/ptfx/behaviours/ptxu_liquid_parser.h"

#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxconfig.h"

#include "rmptfx/ptxmanager.h"



// optimisations
RMPTFX_OPTIMISATIONS()


// namespaces
namespace rage
{


// code
#if RMPTFX_XML_LOADING
ptxu_Liquid::ptxu_Liquid()
{
	// create keyframes

	// set default data
	SetDefaultData();
}
#endif

void ptxu_Liquid::SetDefaultData()
{
	// default keyframes

	// default other data
	m_velocityThresh = 0.0f;
	m_liquidType = -1;
	m_poolStartSize = 0.0f;
	m_poolEndSize = 0.0f;
	m_poolGrowthRate = 0.0f;
	m_trailWidthMin = 0.0f;
	m_trailWidthMax = 0.0f;
}

ptxu_Liquid::ptxu_Liquid(datResource& rsc)
	: ptxBehaviour(rsc)
{
}

#if __DECLARESTRUCT
void ptxu_Liquid::DeclareStruct(datTypeStruct& s)
{
	ptxBehaviour::DeclareStruct(s);
	SSTRUCT_BEGIN_BASE(ptxu_Liquid, ptxBehaviour)
		SSTRUCT_FIELD(ptxu_Liquid, m_velocityThresh)
		SSTRUCT_FIELD(ptxu_Liquid, m_liquidType)
		SSTRUCT_FIELD(ptxu_Liquid, m_poolStartSize)
		SSTRUCT_FIELD(ptxu_Liquid, m_poolEndSize)
		SSTRUCT_FIELD(ptxu_Liquid, m_poolGrowthRate)
		SSTRUCT_FIELD(ptxu_Liquid, m_trailWidthMin)
		SSTRUCT_FIELD(ptxu_Liquid, m_trailWidthMax)
		SSTRUCT_IGNORE(ptxu_Liquid, m_pad)
	SSTRUCT_END(ptxu_Liquid)
}
#endif

IMPLEMENT_PLACE(ptxu_Liquid);

void ptxu_Liquid::InitPoint(ptxBehaviourParams& params)
{
	// cache data from params
	ptxPointItem* pPointItem = params.pPointItem;	

	// Get pointers to the actual data.
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();

	// set this particle as collidable
	pPointSB->SetFlag(PTXPOINT_FLAG_COLLIDES);

#if RMPTFX_BANK
	if (!ptxDebug::sm_disableInitPointLiquid)
#endif
	{
		UpdatePoint(params);
	}
}

void ptxu_Liquid::UpdatePoint(ptxBehaviourParams& params)
{
	// cache data from params
#if !__RESOURCECOMPILER
	ptxEffectInst* pEffectInst = params.pEffectInst;

	// don't do liquids for effects flagged as only rendering in non-first person views
	// these are likely to be duplicate effects for rendering in mirrors/shadows when in first person
	if (pEffectInst->GetFlag(PTFX_EFFECTINST_RENDER_ONLY_NON_FIRST_PERSON_VIEW))
	{
		return;
	}

#endif // !__RESOURCECOMPILER
	ptxPointItem* pPointItem = params.pPointItem;	

	// get the particle data (single and double buffered)
	ptxPointSB* RESTRICT pPointSB = pPointItem->GetDataSB();
#if !__RESOURCECOMPILER
	ptxPointDB* RESTRICT pPointDB = pPointItem->GetDataDB();
#endif // !__RESOURCECOMPILER

	// return if this emitter isn't active
#if RMPTFX_EDITOR
	ptxEmitterInst* pEmitterInst = params.pEmitterInst;
	if (pEmitterInst->GetFlag(PTXEMITINSTFLAG_NOT_ACTIVE))
	{
		return;
	}
#endif

	if (pPointSB->GetFlag(PTXPOINT_FLAG_HAS_COLLIDED))
	{
#if !__RESOURCECOMPILER
		if (Mag(pPointDB->GetCurrVel()).Getf()>=m_velocityThresh)
		{
			// add the liquid
			ptfxLiquidInfo liquidInfo;
			liquidInfo.pEffectInst = pEffectInst;
			liquidInfo.vPos = pPointSB->GetColnPos();
			liquidInfo.vNormal = pPointSB->GetColnNormal();
			liquidInfo.liquidType = m_liquidType;
			liquidInfo.trailWidthMin = m_trailWidthMin;
			liquidInfo.trailWidthMax = m_trailWidthMax;
			liquidInfo.poolStartSize = m_poolStartSize;
			liquidInfo.poolEndSize = m_poolEndSize;
			liquidInfo.poolGrowthRate = m_poolGrowthRate;
			liquidInfo.mtlId = pPointSB->GetColnMtlId();
			liquidInfo.colR = pPointDB->GetColour().GetRed();
			liquidInfo.colG = pPointDB->GetColour().GetGreen();
			liquidInfo.colB = pPointDB->GetColour().GetBlue();
			liquidInfo.colA = pPointDB->GetColour().GetAlpha();

			g_pPtfxCallbacks->AddLiquid(liquidInfo);
		}
#endif
	}
}

void ptxu_Liquid::UpdateFinalize(ptxBehaviourParams& params)
{
	UpdatePoint(params);
}


// editor code
#if RMPTFX_EDITOR
bool ptxu_Liquid::IsActive()
{
	ptxu_Liquid* pLiquid = static_cast<ptxu_Liquid*>(RMPTFXMGR.GetBehaviour(GetName()));

	bool isEqual = m_velocityThresh == pLiquid->m_velocityThresh &&
				   m_liquidType == pLiquid->m_liquidType &&
				   m_poolStartSize == pLiquid->m_poolStartSize && 
				   m_poolEndSize == pLiquid->m_poolEndSize && 
				   m_poolGrowthRate == pLiquid->m_poolGrowthRate &&
				   m_trailWidthMin == pLiquid->m_trailWidthMin &&
				   m_trailWidthMax == pLiquid->m_trailWidthMax;

	return !isEqual;
}

void ptxu_Liquid::SendDefnToEditor(ptxByteBuff& buff)
{
	ptxBehaviour::SendDefnToEditor(buff);

	// send number of definitions
	SendNumDefnsToEditor(buff, 7);

	// send keyframes

	// send non-keyframes
	SendFloatDefnToEditor(buff, "Velocity Threshold", 0.0f, 10.0f);
	SendIntDefnToEditor(buff, "Liquid Type", -1, 9);
	SendFloatDefnToEditor(buff, "Pool Start Size", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Pool End Size", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Pool Growth Rate", 0.0f, 1.0f);
	SendFloatDefnToEditor(buff, "Trail Width Min", 0.0f, 10.0f);
	SendFloatDefnToEditor(buff, "Trail Width Max", 0.0f, 1.0f);
}

void ptxu_Liquid::SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule)
{
	ptxBehaviour::SendTuneDataToEditor(buff, pParticleRule);
	
	// send keyframes

	// send non-keyframes
	SendFloatToEditor(buff, m_velocityThresh);
	SendIntToEditor(buff, m_liquidType);
	SendFloatToEditor(buff, m_poolStartSize);
	SendFloatToEditor(buff, m_poolEndSize);
	SendFloatToEditor(buff, m_poolGrowthRate);
	SendFloatToEditor(buff, m_trailWidthMin);
	SendFloatToEditor(buff, m_trailWidthMax);
}

void ptxu_Liquid::ReceiveTuneDataFromEditor(const ptxByteBuff& buff)
{
	// receive keyframes

	// receive non-keyframes
	ReceiveFloatFromEditor(buff, m_velocityThresh);
	ReceiveIntFromEditor(buff, m_liquidType);
	ReceiveFloatFromEditor(buff, m_poolStartSize);
	ReceiveFloatFromEditor(buff, m_poolEndSize);
	ReceiveFloatFromEditor(buff, m_poolGrowthRate);
	ReceiveFloatFromEditor(buff, m_trailWidthMin);
	ReceiveFloatFromEditor(buff, m_trailWidthMax);
}
#endif // RMPTFX_EDITOR



} // namespace rage
