// 
// vfx/ptfx/behaviours/ptxu_fire.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef PTXU_FIRE_H 
#define PTXU_FIRE_H 

/*
// includes
#include "parser/macros.h"
#include "rmptfx/ptxbehaviour.h"


// namespaces
namespace rage
{


// classes
class ptxu_Fire : public ptxBehaviour
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

#if RMPTFX_XML_LOADING
		ptxu_Fire();
#endif
		void SetDefaultData();

		// resourcing
		explicit ptxu_Fire(datResource& rsc);
#if __DECLARESTRUCT
		void DeclareStruct(datTypeStruct& s);
#endif
		DECLARE_PLACE(ptxu_Fire);

		// info
		const char* GetName() {return "ptxu_Fire";}												// this must exactly match the class name
		float GetOrder() {return RMPTFX_BEHORDER_FIRE;}											// the ordering of the behaviour (see ptxconfig.h)

		bool NeedsToInit() {return true;}														// whether we call init when a particle is created
		bool NeedsToUpdate() {return true;}														// whether we call update on an existing particle
		bool NeedsToUpdateFinalize() {return true;}												// whether we call update finalize on an existing particle
		bool NeedsToDraw() {return false;}														// whether we call draw on an existing particle
		bool NeedsToRelease() {return false;}													// whether we call release when a particle dies

		void InitPoint(ptxBehaviourParams& params);												// called for each newly created particle if NeedsToInit returns true
		void UpdatePoint(ptxBehaviourParams& params);											// called from InitPoint and UpdatePoint to update the particle accordingly 

		void UpdateFinalize(ptxBehaviourParams& params);

#if RMPTFX_EDITOR
		// editor specific
		const char* GetEditorName() {return "Fire";}											// name of the behaviour shown in the editor
		bool IsActive();																		// whether this behaviour is active or not

		void SendDefnToEditor(ptxByteBuff& buff);												// sends the definition of this behaviour's tuning data to the editor
		void SendTuneDataToEditor(ptxByteBuff& buff, const ptxParticleRule* pParticleRule);		// sends tuning data to the editor
		void ReceiveTuneDataFromEditor(const ptxByteBuff& buff);								// receives tuning data from the editor
#endif

		// parser
#if RMPTFX_XML_LOADING
		PAR_PARSABLE;
#endif

	///////////////////////////////////
	// VARIABLES  
	///////////////////////////////////	

	public: ///////////////////////////

		// keyframe data

		// non keyframe data
		float m_velocityThresh;
		float m_burnTime;
		float m_burnStrength;
		float m_peakTime;
		float m_fuelLevel;
		float m_fuelBurnRate;
		int m_numGenerations;

		datPadding<4> m_pad;

		// static data

};


} // namespace rage
*/

#endif // PTXU_FIRE_H 
