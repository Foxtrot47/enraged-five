// 
// ptfx/PtFxIterator.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FX_PTFXITERATOR_H 
#define FX_PTFXITERATOR_H 

//Includes
#include <iterator>
#include "rmptfx/ptxmanager.h"
#include "rmptfx/ptxeffectinst.h"

//Endless loop detection?
#if __FINAL
#	if defined(PTFX_DETECT_ENDLESS_LOOP)	//Always turn endless loop detection off in __FINAL builds
#		undef PTFX_DETECT_ENDLESS_LOOP
#	endif
#	define PTFX_DETECT_ENDLESS_LOOP 0
#else //__FINAL
#	if !defined(PTFX_DETECT_ENDLESS_LOOP)
#		define PTFX_DETECT_ENDLESS_LOOP 0
#	endif
#	if PTFX_DETECT_ENDLESS_LOOP
#		if !defined(PTFX_MAX_INSTS)
#			error "You must include PtFxDefines (or what ever file defines PTFX_MAX_INSTS) before PtFxIterator if you wish to turn on endless loop detection."
#		endif
#	endif
#endif //__FINAL

namespace rage {

template<typename FxInstType>
class CPtFxIteratorBase : public std::iterator<FxInstType, ptrdiff_t>
{
public:
	typedef std::iterator<FxInstType, ptrdiff_t> base_type;
	typedef CPtFxIteratorBase<FxInstType> this_type;

	CPtFxIteratorBase();
	CPtFxIteratorBase(const CPtFxIteratorBase<const FxInstType> &ci);	//Allows conversion: const_iterator -> iterator

	bool IsValid() const	{ return mCurrDrawList >= 0; }

	//Operators
	bool operator ==(const this_type &rhs) const;
	bool operator !=(const this_type &rhs) const;
	typename this_type::pointer operator ->() const;
	typename this_type::reference operator *() const;

	this_type &operator ++() { return Increment(); }	//Pre-Increment operator: ++iter
	this_type operator ++(int)							//Post-Increment operator: iter++
	{ 
		this_type copy(*this);
		Increment();
		return copy;
	}

#if __PS3
#	pragma diag_suppress=556
#endif //__PS3
	//Suppressing PS3 SNC warning 556. (Please see full explanation in PtFxIterator_inline.h if interested.)
	//This warning is generated when CPtFxIteratorBase is instantiated with a const type and complains that
	//the conversion operator will never be used. This is because it will always prefer to use the constructor with the same signature. This
	//is ok and safe. This cast operator was not meant to be used from an already const template parameter, (const_iterator) and there is no
	//way to implicitly call this function.

	//Cast operator
	operator CPtFxIteratorBase<const FxInstType>();	//Allows conversion: iterator -> const_iterator
#if __PS3
#	pragma diag_default=556
#endif //__PS3

private:
	template<typename T> friend class CPtFxIteratorBase;
	void Reset();
	this_type &Increment();
	bool IterateList();

	int mCurrDrawList;
	ptxEffectInstItem *mEffectInstNode;

private:
	//friend class CPtFxManager;
	friend class ptfxManager;
	static this_type CreateBegin(); //Creates the "begin()" iterator. Called from the owner class. (ptfxManager)
	static this_type CreateEnd(); //Creates the "end()" iterator. Called from the owner class. (ptfxManager)

#if PTFX_DETECT_ENDLESS_LOOP
	typename this_type::difference_type mEndlessLoopDetector;
#endif
};

typedef CPtFxIteratorBase<const ptxEffectInst> CPtFxConstIterator;
typedef CPtFxIteratorBase<ptxEffectInst> CPtFxIterator;

//Need to include cpp implementation for templates.
#include "PtFxIterator_inline.h"

} //namespace rage

#endif // FX_PTFXITERATOR_H 
