//
// vfx/ptfxasset.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXASSET_H
#define VFX_PTFXASSET_H


// includes (rage)
#include "rmptfx/ptxfxlist.h"

// includes (framework)
#include "fwtl/assetstore.h"

// includes (game)


// namespaces 
namespace rage {


// structures
struct ptfxAssetInfoDetailed
{
	ptfxAssetInfoDetailed()
	{
		virtSize = 0;
		physSize = 0;
		numEffectRules = 0;
		numEmitterRules = 0;
		numParticleRules = 0;
		numTextures = 0;
		numModels = 0;
		keyframeEntryCount = 0;
		keyframeEntryMem = 0;
		keyframeEntryMax = 0;
		keyframeEntryDefault = 0;
	}

	void Accum(ptfxAssetInfoDetailed& otherInfo)
	{
		virtSize += otherInfo.virtSize;
		physSize += otherInfo.physSize;
		numEffectRules += otherInfo.numEffectRules;
		numEmitterRules += otherInfo.numEmitterRules;
		numParticleRules += otherInfo.numParticleRules;
		numTextures += otherInfo.numTextures;
		numModels += otherInfo.numModels;
		keyframeEntryCount += otherInfo.keyframeEntryCount;
		keyframeEntryMem += otherInfo.keyframeEntryMem;
		keyframeEntryMax = Max(keyframeEntryMax, otherInfo.keyframeEntryMax);
		keyframeEntryDefault += otherInfo.keyframeEntryDefault;
	}

	int virtSize;
	int physSize;
	int numEffectRules;
	int numEmitterRules;
	int numParticleRules;
	int numTextures;
	int numModels;
	int keyframeEntryCount;
	int keyframeEntryMem;
	int keyframeEntryMax;
	int keyframeEntryDefault;
};


// classes
class ptxFxListDef : public fwAssetDef<ptxFxList>
{
public:
	void Init(const strStreamingObjectName name) 
	{
		m_parentIndex = strLocalIndex(-1);
		fwAssetDef<ptxFxList>::Init(name); 
	}

	void Load(const char* pName);
	void Unload();

	strLocalIndex	m_parentIndex;
#if __DEV
	pgRef<ptxFxList> m_pFxListXML;
#endif
};


class ptfxAssetStore : public fwAssetRscStore<ptxFxList, ptxFxListDef>
{
public:
	typedef fwAssetRscStore<ptxFxList, ptxFxListDef> BaseClass;

	ptfxAssetStore();

	void InitLevel();

	//  virtual interface
	virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);
	virtual void SetResource(strLocalIndex index, datResourceMap& map);
	virtual void Remove(strLocalIndex index);
	virtual int GetNumRefs(strLocalIndex index) const;

	virtual bool Load(strLocalIndex index, void* pData, int size);
	virtual int GetDependencies(strLocalIndex index, strIndex* pIndices, int indexArraySize) const;

	virtual bool IsDefragmentCopyBlocked() const;

	void Update(float deltaTime);

	strLocalIndex GetParentIndex(strLocalIndex index) const {return GetSlot(index)->m_parentIndex;}
	void SetParentIndex(strLocalIndex index, int parentIndex) {FastAssert(index!=parentIndex); GetSlot(index)->m_parentIndex=(s16)parentIndex;}

	void PrepForXmlLoading();

	void	SetIsSafeToRemoveFxLists(bool b) { m_safeToRemoveFxLists = b; }
	bool	IsSafeToRemoveFxLists() const { return m_safeToRemoveFxLists ;}

	// debug
#if __BANK
	void ShutdownLevel();

	virtual void AddRef(strLocalIndex index, strRefKind strRefKind);
	virtual void RemoveRef(strLocalIndex index, strRefKind strRefKind);

	void OutputAssetInfoLoaded(strLocalIndex index);
	void OutputAssetInfoRefChanged(strLocalIndex index);
	void OutputAssetInfoDetailed(strLocalIndex index, ptfxAssetInfoDetailed& accumInfo);

	void RemoveDefaultKeyframes();
	void OutputLoadedAssets();

	void DebugDrawFxListReferences();
#endif

private:
	bool m_safeToRemoveFxLists;
};



} // namespace rage

#endif // VFX_PTFXASSET_H



