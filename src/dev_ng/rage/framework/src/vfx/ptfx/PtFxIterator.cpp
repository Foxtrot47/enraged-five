// 
// ptfx/PtFxIterator.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "PtFxIterator.h"

//This file may be used later to add non-template code that would be better abstracted from the header.
//Currently, all iterator code is defined in inline header.
