//
// vfx/ptfxreg.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxreg.h"

// includes (rage)
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "system/param.h"
#include "vectormath/classes.h"

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxdebug.h"
#include "vfx/ptfx/ptfxflags.h"
#include "vfx/ptfx/ptfxreg.h"
#include "vfx/ptfx/ptfxscript.h"
#include "vfx/ptfx/ptfxwrapper.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// namespaces 
namespace rage {


// static member variables
atArray<ptfxRegInst> ptfxReg::ms_regFxInsts;
atMap<const void*, ptfxRegInst*> ptfxReg::ms_regInstMap;
vfxList ptfxReg::ms_regFxInstPool;
vfxList ptfxReg::ms_regFxInstList;


// code
void ptfxReg::Init(int maxSize)
{	
	ms_regFxInsts.Reset();
	ms_regFxInsts.Reserve(maxSize);
	ms_regFxInsts.Resize(maxSize);

	ms_regInstMap.Kill();
}

void ptfxReg::InitLevel()
{	
	// initialise the registered effect insts and add them to the pool
	ptfxFatalAssertf(ms_regFxInstList.GetNumItems()==0, "registered effect inst list is not empty");
	ptfxFatalAssertf(ms_regFxInstPool.GetNumItems()==0, "registered effect inst pool is not empty");

	for (int i=0; i<ms_regFxInsts.GetCapacity(); i++)
	{
		// initialise
		ms_regFxInsts[i].m_pOwnerClass = NULL;
		ms_regFxInsts[i].m_classFxOffset = -1;
		ms_regFxInsts[i].m_isRegReffed = false;
		ms_regFxInsts[i].m_pFxInst = NULL;
		ms_regFxInsts[i].m_regThisFrame = false;
		ms_regFxInsts[i].m_hasAudio = false;

		// add to pool
		ms_regFxInstPool.AddItem(&ms_regFxInsts[i]);
	}
}

void ptfxReg::ShutdownLevel()
{		
	// go through the active registered effects
	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetHead());
	while (pRegFxInst)
	{
		// store the next active registered fx inst
		ptfxRegInst* pNextRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetNext(pRegFxInst));

		// stop the effect 
		ptfxWrapper::StopEffectInst(pRegFxInst->m_pFxInst);

		// un register the effect
		UnRegister(pRegFxInst);

		// set the next active registered fx inst
		pRegFxInst = pNextRegFxInst;
	}

	// do some checks
	ptfxFatalAssertf(ms_regFxInstList.GetNumItems()==0, "registered effect inst list is not empty");
	ptfxFatalAssertf(ms_regFxInstPool.GetNumItems()==(u32)ms_regFxInsts.GetCapacity(), "registered effect inst pool is not full");

	// remove all the registered effects from the pool
	ms_regFxInstPool.RemoveAll();

	// do some checks
	ptfxFatalAssertf(ms_regFxInstList.GetNumItems()==0, "registered effect inst list is not empty");
	ptfxFatalAssertf(ms_regFxInstPool.GetNumItems()==0, "registered effect inst pool is not empty");

	// remove all the registered effects from the map
	ms_regInstMap.Kill();
}

void ptfxReg::Update(float deltaTime)
{
#if __BANK
	ptfxDebug::ms_numFxRegistered = 0;
#endif

	// update the registered effects
	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetHead());
	while (pRegFxInst)
	{
		// store the next active registered fx inst
		ptfxRegInst* pNextRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetNext(pRegFxInst));

		// check if this effect's owner class has been deleted
		bool hasLostOwnerClass = pRegFxInst->m_isRegReffed && pRegFxInst->m_pOwnerClass==NULL;
		if (hasLostOwnerClass)
		{
			ptfxWrapper::StopEffectInst(pRegFxInst->m_pFxInst);
			UnRegister(pRegFxInst);
		}
		else
		{
			// check the registered fx inst is valid
			ptfxAssertf(pRegFxInst->m_pOwnerClass, "the registered effect inst has a null owner class");
			ptfxAssertf(pRegFxInst->m_classFxOffset>=0, "the registered effect inst has an invalid class fx offset");
			ptfxAssertf(pRegFxInst->m_pFxInst, "the registered effect inst has no effect inst pointer");

			// if it hasn't been registered this frame then stop the effect and remove it from the active list
			if (pRegFxInst->m_regThisFrame==false)
			{
				// stop the effect from playing - it hasn't been registered this frame
				if (pRegFxInst->m_pFxInst->GetIsPlaying())
				{
					ptfxWrapper::StopEffectInst(pRegFxInst->m_pFxInst);
				}

				pRegFxInst->m_timeSinceLastRegistered += deltaTime;	

				bool recycleEffect = pRegFxInst->m_pFxInst->GetIsFinished();

				if (recycleEffect)
				{
					// stop the effect and tidy up
					UnRegister(pRegFxInst);
				}
			}
			else
			{
				if (pRegFxInst->m_timeSinceLastRegistered>0.0f && !pRegFxInst->m_isOneShotSpecialCase)
				{
					pRegFxInst->m_pFxInst->Start();
				}

				pRegFxInst->m_timeSinceLastRegistered = 0.0f;

#if __BANK
				ptfxDebug::ms_numFxRegistered++;
#endif
			}

			// reset the registered this frame flag
			pRegFxInst->m_regThisFrame = false;
		}

		// set the next active registered fx inst
		pRegFxInst = pNextRegFxInst;
	}

	ptfxAssertf(ms_regInstMap.GetNumUsed()==(int)ms_regFxInstList.GetNumItems(), "mismatch between map and list");
}

ptxEffectInst* ptfxReg::Register(const void* pOwnerClass, int classFxOffset, bool canRegRef, atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, bool& justCreated, bool isOneShotSpecialCase)
{
	ptxEffectRule* pEffectRule = NULL;
	if (effectRuleHashName.GetHash()!=0)
	{
		if (fxListHashName.GetHash()==0)
		{
			pEffectRule = RMPTFXMGR.GetEffectRule(effectRuleHashName);
		}
		else
		{
			pEffectRule = RMPTFXMGR.GetEffectRule(effectRuleHashName, fxListHashName);
		}
	}

	return Register(pOwnerClass, classFxOffset, canRegRef, pEffectRule, justCreated, isOneShotSpecialCase);
}

ptxEffectInst* ptfxReg::Register(const void* pOwnerClass, int classFxOffset, bool canRegRef, ptxEffectRule* pEffectRule, bool& justCreated, bool isOneShotSpecialCase)
{
	// check and initialise some data
	ptfxAssertf(pOwnerClass, "has been passed an invalid owner class");
	justCreated = false;

	// get the registered effect if it exists already
	ptfxRegInst* pRegFxInst = Find(pOwnerClass, classFxOffset);

 	// check for finding a registered effect with a deleted owner class
	if (pRegFxInst && pRegFxInst->m_pOwnerClass==NULL)
	{
		// make sure this was regreffed (it shouldn't be null otherwise)
		ptfxAssertf(pRegFxInst->m_isRegReffed, "non regreffed owner class is null");

		// we need to unregister now as we'll be creating a new one
		// and can't have 2 entries in the map with the same key
		UnRegister(pRegFxInst);
		pRegFxInst = NULL;
	}

	// if the registered effect already exists then check its validity
	if (pRegFxInst)
	{
		ptfxAssertf(pRegFxInst->m_pOwnerClass, "the registered effect inst has a null owner class");
		ptfxAssertf(pRegFxInst->m_classFxOffset>=0, "the registered effect inst has an invalid class fx offset");
		ptfxAssertf(pRegFxInst->m_pFxInst, "the registered effect inst has no effect inst pointer");
		ptfxAssertf(pRegFxInst->m_pOwnerClass==pOwnerClass, "the registered effect inst has an owner class different to the one passed");

		// check for having a different name (only if a new name is passed in)
		if (pEffectRule && atHashValue(pRegFxInst->m_pFxInst->GetEffectRule()->GetName())!=atHashValue(pEffectRule->GetName()))
		{
			// remove any regref on the owner pointer
			if (pRegFxInst->m_isRegReffed && pRegFxInst->m_pOwnerClass)
			{
				g_pPtfxCallbacks->RemoveEntityRef(pRegFxInst->m_pOwnerClass, const_cast<void**>(&pRegFxInst->m_pOwnerClass));
			}

			// the effect instance on the registered effect is different - stop the current one 
			ptfxWrapper::StopEffectInst(pRegFxInst->m_pFxInst);

			// we can now stop this effect from being reserved
			ptfxFlags::ClearUserReservedFlag(pRegFxInst->m_pFxInst, PTFX_RESERVED_REGISTERED);

			pRegFxInst->m_pFxInst = NULL;
		}
	}

	// if we don't have a registered effect then try to get one from the pool
	if (pRegFxInst==NULL)
	{
		const void* mapKey = GetMapKey(pOwnerClass, classFxOffset);
		pRegFxInst = GetRegFxInstFromPool(mapKey);
		if (pRegFxInst)
		{
			pRegFxInst->m_mapKey = mapKey;
		}
		else
		{
			ptfxDebugf2("ptfxReg - Couldn't get registered fx instance from pool");
		}
	}

	// create the effect instance on the registered effect (if required)
	if (pRegFxInst)
	{
		if (pRegFxInst->m_pFxInst==NULL)
		{
			// now try to create the effect instance
			if (pEffectRule)
			{
				pRegFxInst->m_pFxInst = ptfxWrapper::GetEffectInst(pEffectRule);
			}

			if (pRegFxInst->m_pFxInst)
			{
				// the effect instance was created ok - set up the rest of its data
				ptfxFlags::SetUserReservedFlag(pRegFxInst->m_pFxInst, PTFX_RESERVED_REGISTERED);
				pRegFxInst->m_pOwnerClass = pOwnerClass;
				pRegFxInst->m_isRegReffed = canRegRef;
				pRegFxInst->m_classFxOffset = classFxOffset;
				pRegFxInst->m_timeSinceLastRegistered = 0.0f;
				pRegFxInst->m_isOneShotSpecialCase = isOneShotSpecialCase;
				justCreated = true;

				// reg ref the owner class
				if (canRegRef)
				{
					// reg ref the owner pointer
					g_pPtfxCallbacks->AddEntityRef(pRegFxInst->m_pOwnerClass, const_cast<void**>(&pRegFxInst->m_pOwnerClass));
				}

				// check that the effect infinitely looped 
				if (isOneShotSpecialCase==false)
				{
					ptfxAssertf(pRegFxInst->m_pFxInst->GetEffectRule()->GetIsInfinitelyLooped(), "trying to register a finite time effect (%s)", pRegFxInst->m_pFxInst->GetEffectRule()->GetName());
				}
			}
			else
			{
				// cannot create the effect instance - return the registered effect to the pool
				ptfxDebugf2("ptfxReg - Couldn't create the effect instance");
				ReturnRegFxInstToPool(pRegFxInst);
				pRegFxInst = NULL;
			}
		}
	}

	// check if we have got a registered effect
	if (pRegFxInst)
	{
		// we have a registered effect - check its validity and register it this frame
		ptfxAssertf(pRegFxInst->m_pOwnerClass, "the registered effect inst has a null owner class");
		ptfxAssertf(pRegFxInst->m_classFxOffset>=0, "the registered effect inst has an invalid class fx offset");
		ptfxAssertf(pRegFxInst->m_pFxInst, "the registered effect inst has no effect inst pointer");
		pRegFxInst->m_regThisFrame = true;

		// return the effect instance
		return pRegFxInst->m_pFxInst;
	}
	else
	{
		// we haven't got a registered effect - return NULL
		return NULL;
	}
}

void ptfxReg::UnRegister(const void* pOwnerClass, bool finishPtFx)
{
	ptfxAssertf(pOwnerClass, "invalid owner class passed in");

	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetHead());
	while (pRegFxInst)
	{
		ptfxRegInst* pNextRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetNext(pRegFxInst));

		if (pRegFxInst->m_pOwnerClass==pOwnerClass)
		{
			ptfxWrapper::StopEffectInst(pRegFxInst->m_pFxInst);
			UnRegister(pRegFxInst, finishPtFx);
		}

		pRegFxInst = pNextRegFxInst;
	}
}

void ptfxReg::UnRegister(ptxEffectInst* pFxInst, bool finishPtFx)
{
	if (pFxInst->GetFlag(PTFX_RESERVED_REGISTERED))
	{	
		// search for this registered effect
		ptfxRegInst* pRegFxInst = Find(pFxInst);

		// if the registered effect already exists then check its validity
		if (pRegFxInst)
		{
			UnRegister(pRegFxInst, finishPtFx);
		}
		else
		{
			ptfxFlags::ClearUserReservedFlag(pFxInst, PTFX_RESERVED_REGISTERED);
		}
	}
}

void ptfxReg::UnRegister(ptfxRegInst* pRegFxInst, bool finishPtFx)
{
	ptfxAssertf(pRegFxInst->m_classFxOffset>=0, "the registered effect inst has an invalid class fx offset");
	ptfxAssertf(pRegFxInst->m_pFxInst, "the registered effect inst has no effect inst pointer");
	ptfxAssertf(pRegFxInst->m_pFxInst->GetFlag(PTFX_RESERVED_REGISTERED), "the registered effect inst doesn't have its reserved flag set");

	// remove any regref on the owner pointer
	if (pRegFxInst->m_isRegReffed && pRegFxInst->m_pOwnerClass)
	{
		g_pPtfxCallbacks->RemoveEntityRef(pRegFxInst->m_pOwnerClass, const_cast<void**>(&pRegFxInst->m_pOwnerClass));
	}

	// remove any scripted effect script resources
	if (pRegFxInst->m_pOwnerClass==(void*)SCRIPT_REG_OWNER_CLASS)
	{
		g_pPtfxCallbacks->RemoveScriptResource(pRegFxInst->m_classFxOffset);
	}

	// we can now stop this effect from being reserved
	ptfxFlags::ClearUserReservedFlag(pRegFxInst->m_pFxInst, PTFX_RESERVED_REGISTERED);

	// finish the effect, if requested
	if (finishPtFx)
	{
		pRegFxInst->m_pFxInst->Finish(false);
	}

	// stop the effect and tidy up
	ReturnRegFxInstToPool(pRegFxInst);
}

// numOffsets defines the range to search for - i.e. a fire can have many registered offsets
//   from the base unique id - we may want to search for many fire types in one go
// bool ptfxReg::Exists(void* pOwnerClass, int numClassFxOffsets)
// {
// 	ptfxAssertf(pOwnerClass, "trying to look up a registered effect with a null owner class");
// 	ptfxAssertf(numClassFxOffsets>=0, "trying to look up a registered effect with an invalid number of class fx offsets");
// 
// 	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetHead());
// 	while (pRegFxInst)
// 	{
// 		if (pRegFxInst->m_pOwnerClass==pOwnerClass && pRegFxInst->m_classFxOffset<numClassFxOffsets)
// 		{
// 			return true;
// 		}
// 
// 		pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetNext(pRegFxInst));
// 	}
// 
// 	return false;
// }

// numOffsets defines the range to search for - i.e. a fire can have many registered offsets
//   from the base unique id - we may want to search for many fire types in one go
ptfxRegInst* ptfxReg::Find(const void* pOwnerClass, int classFxOffset)
{
	ptfxAssertf(pOwnerClass, "trying to look up a registered effect with a null owner class");
	ptfxAssertf(classFxOffset>=0, "trying to look up a registered effect with an invalid class fx offset");

// 	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetHead());
// 	while (pRegFxInst)
// 	{
// 		if (pRegFxInst->m_pOwnerClass==pOwnerClass && pRegFxInst->m_classFxOffset==classFxOffset)
// 		{
// 			return pRegFxInst;
// 		}
// 
// 		pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetNext(pRegFxInst));
// 	}

	const void* mapKey = GetMapKey(pOwnerClass, classFxOffset);
	ptfxRegInst** ppRegFxInst = ms_regInstMap.Access(mapKey);
	if (ppRegFxInst)
	{
		ptfxAssertf(mapKey==(*ppRegFxInst)->m_mapKey, "map key mismatch");

		return *ppRegFxInst;
	}

	return NULL;
}

ptfxRegInst* ptfxReg::Find(ptxEffectInst* pFxInst)
{
	ptfxAssertf(pFxInst, "trying to look up a NULL effect inst");

	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetHead());
	while (pRegFxInst)
	{
		if (pRegFxInst->m_pFxInst == pFxInst)
		{
			return pRegFxInst;
		}

		pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstList.GetNext(pRegFxInst));
	}

	return NULL;
}

ptfxRegInst* ptfxReg::GetRegFxInstFromPool(const void* mapKey)
{
	// get an registered fx inst from the pool
	ptfxRegInst* pRegFxInst = static_cast<ptfxRegInst*>(ms_regFxInstPool.RemoveHead());

	// add this registered fx inst to the active list
	if (pRegFxInst)
	{
		// add to the list
		ms_regFxInstList.AddItem(pRegFxInst);

		// add to the map
		ms_regInstMap.Insert(mapKey, pRegFxInst);

		// we have a registered effect - check that it is initialised ok
		ptfxAssertf(pRegFxInst->m_pOwnerClass==NULL, "the newly registered effect inst has a null owner class");
		ptfxAssertf(pRegFxInst->m_classFxOffset>=-1, "the newly registered effect inst has an invalid class fx offset");
		ptfxAssertf(pRegFxInst->m_pFxInst==NULL, "the newly registered effect inst has no effect inst pointer");
	}

	// return the registered fx
	return pRegFxInst;
}

void ptfxReg::ReturnRegFxInstToPool(ptfxRegInst* pRegFxInst)
{
	// remove from the map
#if __ASSERT
	bool deletedOk = 
#endif
		ms_regInstMap.Delete(pRegFxInst->m_mapKey);

	ptfxAssertf(deletedOk, "failed to delete entry from the map");

	// reset the registered fx inst
	pRegFxInst->m_pOwnerClass = NULL;
	pRegFxInst->m_classFxOffset = -1;
	pRegFxInst->m_mapKey = 0;
	pRegFxInst->m_pFxInst = NULL;
	pRegFxInst->m_timeSinceLastRegistered = 0.0f;
	pRegFxInst->m_regThisFrame = false;
	pRegFxInst->m_isOneShotSpecialCase = false;
	pRegFxInst->m_isRegReffed = false;
	pRegFxInst->m_hasAudio = false;

	// remove the registered fx inst from the active list
	ms_regFxInstList.RemoveItem(pRegFxInst);

	// add the registered fx inst to the pool
	ms_regFxInstPool.AddItem(pRegFxInst);
}


} // namespace rage

