//
// vfx/ptfxattach.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXATTACH_H
#define VFX_PTFXATTACH_H


// includes (rage)
#include "rmptfx/ptxeffectinst.h"

// includes (framework)
#include "fwtl/regdrefs.h"
#include "vfx/vfxlist.h"

// includes (game)
#include "vfx/ptfx/ptfxconfig.h"


// namespaces 
namespace rage {


// forward declarations
class ptfxManager;


// enumerations
enum ptfxAttachMode
{
	PTFXATTACHMODE_FULL		= 0,							// attached using a full matrix (position and rotation)
	PTFXATTACHMODE_POS_ONLY,								// attached using position only
	PTFXATTACHMODE_INFO_ONLY								// attached for information purposes only

};


// classes

// PURPOSE
//  
class ptfxAttachInfo : public vfxListItem
{
	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	public: ///////////////////////////

		ptxEffectInst* m_pFxInst;
		fwRegdRef<class fwEntity> m_pAttachEntity;
		int m_attachBoneIndex;
		ptfxAttachMode m_attachMode;
		bool m_useAltSkeleton;

};


// PURPOSE
//  
class ptfxAttach
{
	friend class ptfxDebug;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(int maxSize);	
		static void InitLevel();	
		static void ShutdownLevel();	
		static void Update();

		static void Attach(ptxEffectInst* pFxInst, fwEntity* pEntity, int boneIndex, ptfxAttachMode attachMode=PTFXATTACHMODE_FULL, bool useAltSkeleton=false);
		static void Detach(ptxEffectInst* pFxInst);

		static bool GetIsAttached(ptxEffectInst* pFxInst) {return pFxInst->GetFlag(PTFX_RESERVED_ATTACHED);}
		static bool GetIsAttachedTo(ptxEffectInst* pFxInst, fwEntity* pEntity);
		static fwEntity* GetAttachEntity(ptxEffectInst* pFxInst);

		static void UpdateBrokenFrag(fwEntity* pParentEntity, fwEntity* pChildEntity);
		static void UpdateBrokenFrag(fwEntity* pParentEntity, fwEntity* pChildEntity, s32 boneIndex);

		static ptfxAttachInfo* Find(ptxEffectInst* pFxInst);

	private: //////////////////////////

		static Mat34V_Out SetPtFxBaseMtx(ptfxAttachInfo* pAttachInfo);
		static void Detach(ptfxAttachInfo* pAttachInfo);

		static void UpdateLostEntity(ptfxAttachInfo* pAttachInfo);

		// broken frag info
		static bool UpdateBrokenFrag(ptfxAttachInfo* pAttachInfo);

		static ptfxAttachInfo* GetAttachInfoFromPool(ptxEffectInst* pFxInst);
		static void ReturnAttachInfoToPool(ptfxAttachInfo* pAttachInfo);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		static atArray<ptfxAttachInfo> ms_attachInfos;
		static atMap<ptxEffectInst*, ptfxAttachInfo*> ms_attachInfoMap;
		static vfxList ms_attachInfoPool;
		static vfxList ms_attachInfoList;

}; 


} // namespace rage

#endif // VFX_PTFXATTACH_H



