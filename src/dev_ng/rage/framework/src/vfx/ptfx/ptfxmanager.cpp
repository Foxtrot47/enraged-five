//
// vfx/ptfxmanager.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes (us)
#include "vfx/ptfx/ptfxmanager.h"

// includes (rage)
#include "rmptfx/ptxclipregion.h"
#include "rmptfx/ptxdrawlist.h"
#include "rmptfx/ptxmanager.h"
#include "system/param.h"
#if RMPTFX_EDITOR
#include "rmptfx/ptxinterface.h"
#endif

// includes (framework)
#include "fwsys/timer.h"
#include "streaming/streamingvisualize.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/ptfx/ptfxattach.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxdebug.h"
#include "vfx/ptfx/ptfxflags.h"
#include "vfx/ptfx/ptfxhistory.h"
#include "vfx/ptfx/ptfxreg.h"
#include "vfx/ptfx/ptfxscript.h"
#include "vfx/ptfx/ptfxwrapper.h"
#include "vfx/vfxwidget.h"

// optimisations
VFX_FW_PTFX_OPTIMISATIONS()
	
// namespaces 
namespace rage {


// static member variables
int ptfxManager::ms_maxPoints;
int ptfxManager::ms_maxInsts;

Mat34V ptfxManager::ms_cameraMtx;
ptfxAssetStore ptfxManager::ms_ptfxAssetStore;

atFixedArray<ptfxOverrideInfo_s, PTFX_MAX_OVERRIDE_INFOS> ptfxManager::ms_ptfxOverrideInfos;

ptfxCallbacks* g_pPtfxCallbacks = NULL;


// code
void ptfxManager::Init(int maxParticles, int maxEffectInsts, int UNUSED_PARAM(maxTrails), int maxReg, int maxScript, int maxHistory, 
						const char* pClipRegionFilename)
{
	ms_maxPoints = maxParticles;
	ms_maxInsts = maxEffectInsts;

	ptxManager::InitClass(-1);
	RMPTFXMGR.CreatePointPool(maxParticles);
	RMPTFXMGR.CreateEffectInstPool(maxEffectInsts);
	
	ptfxReg::Init(maxReg);
	ptfxAttach::Init(maxEffectInsts);
	ptfxScript::Init(maxScript);
	ptfxHistory::Init(maxHistory);
	
#if __BANK
	ptfxDebug::Init();
#endif

	ms_cameraMtx = Mat34V(V_IDENTITY);

	ptxClipRegions::Load(pClipRegionFilename);

	// initialise the override infos
	ms_ptfxOverrideInfos.Resize(PTFX_MAX_OVERRIDE_INFOS);
	for (int i=0; i<PTFX_MAX_OVERRIDE_INFOS; i++)
	{
		ms_ptfxOverrideInfos[i].ptFxToOverrideHashName = (u32)0;
		ms_ptfxOverrideInfos[i].ptFxToUseInsteadHashName = (u32)0;
		ms_ptfxOverrideInfos[i].scriptThreadId = THREAD_INVALID;
	}
}

#if __BANK
void ptfxManager::InitWidgets()
{
	ptfxDebug::InitWidgets();
#if RMPTFX_BANK
	RMPTFXMGR.InitDebug(vfxWidget::GetBank(), ptfxDebug::GetRMPTFXGroup());
#endif
}
#endif

ptfxManager::iterator ptfxManager::begin()
{
	return iterator::CreateBegin();
}

ptfxManager::iterator ptfxManager::end()
{
	return iterator::CreateEnd();
}

void ptfxManager::Shutdown()
{
	ptxManager::ShutdownClass();
}

void ptfxManager::InitLevel()
{
	ptfxReg::InitLevel();
	ptfxAttach::InitLevel();

	ms_ptfxAssetStore.InitLevel();
}

void ptfxManager::ShutdownLevel()
{
	ptfxAttach::ShutdownLevel();
	ptfxReg::ShutdownLevel();

	// go through all active effect and make sure their flags are 0
	u32 drawListCount = RMPTFXMGR.GetNumDrawLists();
	for(u32 i=0;i<drawListCount;i++)
	{
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)RMPTFXMGR.GetDrawList(i)->m_effectInstList.GetHead();
		ptxEffectInstItem* pNextEffectInstItem = pCurrEffectInstItem;
		while (pCurrEffectInstItem)
		{
			pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();
			ptxEffectInst* pFxInst = pCurrEffectInstItem->GetDataSB();

			g_pPtfxCallbacks->ShutdownFxInst(pFxInst);

			// check that no reserved flags are set on the effect
			ptfxAssertf(pFxInst->GetFlag(PTXEFFECTFLAG_KEEP_RESERVED)==false, "some reserved flags are still set on an effect instance (%s - %d)", pFxInst->GetEffectRule()->GetName(), pFxInst->GetFlags());

			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

#if __BANK
	ms_ptfxAssetStore.ShutdownLevel();
#endif
}

void ptfxManager::Update(float deltaTime, Mat34V_ConstRef camMtx)
{
	STRVIS_AUTO_CONTEXT(strStreamingVisualize::PTFXMANAGER);

	ms_cameraMtx = camMtx;

	ms_ptfxAssetStore.Update(deltaTime);

	// update attachment data
	ptfxAttach::Update();
	ptfxReg::Update(deltaTime);

	// look for any effects that point to data in deleted entities
#if __BANK
	ptfxDebug::ms_numFxInstsActive = 0;
#endif

	u32 drawListCount = RMPTFXMGR.GetNumDrawLists();
	//Make sure we setup the key frame prop thread ID for the particle update thread
	RMPTFXMGR.SetupPtxKeyFramePropThreadId();
	for(u32 i=0;i<drawListCount;i++)
	{
		ptxEffectInstItem* pCurrEffectInstItem = (ptxEffectInstItem*)RMPTFXMGR.GetDrawList(i)->m_effectInstList.GetHead();
		ptxEffectInstItem* pNextEffectInstItem = pCurrEffectInstItem;
		while (pCurrEffectInstItem)
		{
			pNextEffectInstItem = (ptxEffectInstItem*)pCurrEffectInstItem->GetNext();
			ptxEffectInst* pFxInst = pCurrEffectInstItem->GetDataSB();

			g_pPtfxCallbacks->UpdateFxInst(pFxInst);

#if __ASSERT
			// check that we don't have any reserved effects with neither flag set
			if ((pFxInst->GetFlag(PTXEFFECTFLAG_KEEP_RESERVED)))
			{
				ptfxAssertf(ptfxFlags::IsAnyUserReservedFlagSet(pFxInst), "effect instance has a reservation flag mismatch");
			}
#endif

			// increment the number of active effect insts
#if __BANK
			ptfxDebug::ms_numFxInstsActive++;
#endif

			// move onto the next effect inst
			pCurrEffectInstItem = pNextEffectInstItem;
		}
	}

#if __BANK
	ptfxDebug::ms_numPointsActive = RMPTFXMGR.GetPointPool().GetNumItemsActive();
#endif

#if RMPTFX_BANK
	RMPTFXMGR.UpdateDebug(deltaTime);
#endif

#if __BANK
	ptfxDebug::Update();
#endif 

#if RMPTFX_EDITOR
	static bool wasConnected = false;
	if (!wasConnected && RMPTFXMGR.GetInterface().IsConnected())
	{
		// add a reference so the system knows it's safe to delete resource memory
		// which the particle editor will most likely need to do when adding behaviours, effects, emitters etc
		wasConnected = true;
	}
	else if (wasConnected && !RMPTFXMGR.GetInterface().IsConnected())
	{
		// remove the reference so the system knows it's no longer safe to delete resource memory
		wasConnected = false;
	}
#endif
}

void ptfxManager::UpdateThread(float deltaTime, const grcViewport* pViewport)
{
	RMPTFXMGR.Update(deltaTime, pViewport, PTXUPDATETYPE_ALL_AT_ONCE);
	ptfxScript::Update();
	ptfxHistory::Update(deltaTime);
}

void ptfxManager::RemoveScript(scrThreadId scriptThreadId)
{
	RemovePtFxOverrideInfo(scriptThreadId);
}

void ptfxManager::RemovePtFx(ptxEffectInst* pFxInst)
{
	// stop the effect from emitting any more particles
	ptfxWrapper::StopEffectInst(pFxInst);

	// detach the effect
	ptfxAttach::Detach(pFxInst);

	// stop registering the effect
	ptfxReg::UnRegister(pFxInst);

	// handle any scene sorted effects
	if (g_pPtfxCallbacks->RemoveFxInst(pFxInst))
	{
		return;
	}

	// check that no reserved flags are set on the effect
	ptfxAssertf(pFxInst->GetFlag(PTXEFFECTFLAG_KEEP_RESERVED)==false, "some reserved flags are still set on an effect instance (%s - %d)", pFxInst->GetEffectRule()->GetName(), pFxInst->GetFlags());

	// finish the effect
	pFxInst->Finish(false);
}

ptxEffectInst* ptfxManager::GetTriggeredInst(atHashWithStringNotFinal effectRuleHashName)
{	
	return GetTriggeredInst(effectRuleHashName, atHashWithStringNotFinal(u32(0)));
}

ptxEffectInst* ptfxManager::GetTriggeredInst(atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName)
{	
	ptxAssertf(sysThreadType::IsUpdateThread(), "Function called from non main thread!");

	if (g_pPtfxCallbacks->CanGetTriggeredInst()==false)
	{
		return NULL;
	}

#if __BANK
	if (ptfxDebug::ms_overrideAllTriggered)
	{
		effectRuleHashName = atHashWithStringNotFinal(ptfxDebug::ms_overrideAllTriggeredName);
	}
#endif

	effectRuleHashName = QueryPtFxOverride(effectRuleHashName);

	// check that the effect rule doesn't loop infinitely
	ptxEffectRule* pEffectRule = ptfxWrapper::GetEffectRule(effectRuleHashName, fxListHashName);
	if (pEffectRule)
	{
		if (ptxVerifyf(pEffectRule->GetIsInfinitelyLooped()==false, "trying to trigger an infinitely looped effect (%s)", pEffectRule->GetName()))
		{
			return ptfxWrapper::GetEffectInst(pEffectRule);
		}
	}

	return NULL;
}

ptxEffectInst* ptfxManager::GetRegisteredInst(const void* pOwnerClass, int classFxOffset, bool isOwnerAnEntity, atHashWithStringNotFinal effectRuleHashName, bool& justCreated, bool isOneShotSpecialCase, bool bypassReplayPauseCheck)
{
	return GetRegisteredInst(pOwnerClass, classFxOffset, isOwnerAnEntity, effectRuleHashName, atHashWithStringNotFinal(u32(0)), justCreated, isOneShotSpecialCase, bypassReplayPauseCheck);
}

ptxEffectInst* ptfxManager::GetRegisteredInst(const void* pOwnerClass, int classFxOffset, bool isOwnerAnEntity, atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, bool& justCreated, bool isOneShotSpecialCase, bool bypassReplayPauseCheck)
{	
	ptxAssertf(sysThreadType::IsUpdateThread(), "Function called from non main thread!");

	if (g_pPtfxCallbacks->CanGetRegisteredInst(bypassReplayPauseCheck)==false)
	{
		return NULL;
	}

#if __BANK
	if (ptfxDebug::ms_overrideAllRegistered)
	{
		effectRuleHashName = atHashWithStringNotFinal(ptfxDebug::ms_overrideAllRegisteredName);
	}
#endif

	effectRuleHashName = QueryPtFxOverride(effectRuleHashName);

	// check that the effect rule loops infinitely
	ptxEffectRule* pEffectRule = ptfxWrapper::GetEffectRule(effectRuleHashName, fxListHashName);
	if (pEffectRule)
	{
		if (ptxVerifyf(pEffectRule->GetIsInfinitelyLooped() || isOneShotSpecialCase, "trying to register a non-infinitely looped effect (%s)", pEffectRule->GetName()))
		{	
			return ptfxReg::Register(pOwnerClass, classFxOffset, isOwnerAnEntity, pEffectRule, justCreated, isOneShotSpecialCase);
		}
	}

	return NULL;
}

void ptfxManager::AddPtFxOverrideInfo(const char* pPtFxToOverride, const char* pPtFxToUseInstead, scrThreadId scriptThreadId)
{
	atHashValue ptFxToOverrideHashValue(pPtFxToOverride);

	int firstFreeIdx = -1;

	for (int i=0; i<PTFX_MAX_OVERRIDE_INFOS; i++)
	{
		if (ms_ptfxOverrideInfos[i].scriptThreadId==THREAD_INVALID)
		{	
			if (firstFreeIdx==-1)
			{
				firstFreeIdx = i;
#if !__ASSERT
				break;
#endif
			}
		}
#if __ASSERT
		else
		{
			if (ms_ptfxOverrideInfos[i].ptFxToOverrideHashName.GetHash()==ptFxToOverrideHashValue)
			{
				if (ms_ptfxOverrideInfos[i].scriptThreadId==scriptThreadId)
				{
					ptfxAssertf(ms_ptfxOverrideInfos[i].ptFxToOverrideHashName.GetHash()!=ptFxToOverrideHashValue, "trying to override a particle effect that is already being overridden by this script (%s)", pPtFxToOverride);
				}
				else
				{
					ptfxAssertf(ms_ptfxOverrideInfos[i].ptFxToOverrideHashName.GetHash()!=ptFxToOverrideHashValue, "trying to override a particle effect that is already being overridden by another script (%s)", pPtFxToOverride);
				}
			}
		}
#endif
	}

	if (ptfxVerifyf(firstFreeIdx>-1, "no room available to add ptfx override info"))
	{
		ms_ptfxOverrideInfos[firstFreeIdx].ptFxToOverrideHashName = pPtFxToOverride;
		ms_ptfxOverrideInfos[firstFreeIdx].ptFxToUseInsteadHashName = pPtFxToUseInstead;
		ms_ptfxOverrideInfos[firstFreeIdx].scriptThreadId = scriptThreadId;
	}
}

void ptfxManager::RemovePtFxOverrideInfo(const char* pPtFxToOverride, scrThreadId scriptThreadId)
{
	atHashValue ptFxToOverrideHashValue(pPtFxToOverride);

	for (int i=0; i<PTFX_MAX_OVERRIDE_INFOS; i++)
	{
		if (ms_ptfxOverrideInfos[i].ptFxToOverrideHashName==ptFxToOverrideHashValue)
		{	
			if (ptfxVerifyf(ms_ptfxOverrideInfos[i].scriptThreadId==scriptThreadId, "trying to remove a particle effect override that a different script set up"))
			{
				ms_ptfxOverrideInfos[i].ptFxToOverrideHashName = (u32)0;
				ms_ptfxOverrideInfos[i].ptFxToUseInsteadHashName = (u32)0;
				ms_ptfxOverrideInfos[i].scriptThreadId = THREAD_INVALID;

				return;
			}
		}
	}

	ptfxAssertf(0, "cannot remove particle effect override (%s) - it's not being overridden", pPtFxToOverride);
}

void ptfxManager::RemovePtFxOverrideInfo(scrThreadId scriptThreadId)
{
	for (int i=0; i<PTFX_MAX_OVERRIDE_INFOS; i++)
	{
		if (ms_ptfxOverrideInfos[i].scriptThreadId==scriptThreadId)
		{	
			ms_ptfxOverrideInfos[i].ptFxToOverrideHashName = (u32)0;
			ms_ptfxOverrideInfos[i].ptFxToUseInsteadHashName = (u32)0;
			ms_ptfxOverrideInfos[i].scriptThreadId = THREAD_INVALID;
		}
	}
}

atHashWithStringNotFinal ptfxManager::QueryPtFxOverride(atHashWithStringNotFinal ptFxHashName)
{
	for (int i=0; i<PTFX_MAX_OVERRIDE_INFOS; i++)
	{
		if (ms_ptfxOverrideInfos[i].ptFxToOverrideHashName==ptFxHashName)
		{	
			return ms_ptfxOverrideInfos[i].ptFxToUseInsteadHashName;
		}
	}

	return ptFxHashName;
}

#if __BANK
void ptfxManager::OutputPtFxOverrideInfos()
{
	ptfxDisplayf("Overridden Particle Effect Infos");
	for (int i=0; i<PTFX_MAX_OVERRIDE_INFOS; i++)
	{
		ptfxDisplayf("%d: %s, %s, %d", i, ms_ptfxOverrideInfos[i].ptFxToOverrideHashName.GetCStr(), 
									   ms_ptfxOverrideInfos[i].ptFxToUseInsteadHashName.GetCStr(),
								      (s32)ms_ptfxOverrideInfos[i].scriptThreadId);
	}
}
#endif



} // namespace rage

