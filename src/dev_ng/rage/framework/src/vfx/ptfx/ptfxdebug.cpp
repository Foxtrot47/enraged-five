//
// vfx/ptfxdebug.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#if __BANK

// includes (us)
#include "vfx/ptfx/ptfxdebug.h"

// includes (c)
#include <stdio.h>

// includes (rage)
#include "bank/bkmgr.h"
#include "math/amath.h"
#include "script/thread.h"
#include "rmptfx/ptxdebug.h"
#include "rmptfx/ptxeffectinst.h"
#include "rmptfx/ptxmanager.h"
#include "vfx/vfxwidget.h"

// includes (framework)
#include "fwsys/timer.h"
#include "vfx/channel.h"
#include "vfx/ptfx/ptfxasset.h"
#include "vfx/ptfx/ptfxattach.h"
#include "vfx/ptfx/ptfxcallbacks.h"
#include "vfx/ptfx/ptfxhistory.h"
#include "vfx/ptfx/ptfxmanager.h"
#include "vfx/ptfx/ptfxreg.h"
#include "vfx/ptfx/ptfxscript.h"
#include "vfx/ptfx/ptfxwrapper.h"
#include "vfx/optimisations.h"


// optimisations
VFX_FW_PTFX_OPTIMISATIONS()


// params
PARAM(ptfxdebugdraw, "enables the rmptfx debug drawing by default");
PARAM(ptfxonlyplaythis, "plays only the particle effect specified");
PARAM(ptfxperformancetest, "enables particle performace test case - no particle effects will play apart from those created by a debug button");


// namespaces 
namespace rage {


// static member variables
bkGroup* ptfxDebug::ms_pGameGroup = NULL;
bkGroup* ptfxDebug::ms_pRmptfxGroup = NULL;


int ptfxDebug::ms_numFxAssets;
int ptfxDebug::ms_numPointsActive;
int ptfxDebug::ms_numFxInstsActive;
int ptfxDebug::ms_numFxAttached;
int ptfxDebug::ms_numFxRegistered;
int ptfxDebug::ms_numFxScripted;
int ptfxDebug::ms_numFxHistories;

bool ptfxDebug::ms_disablePtFx;
bool ptfxDebug::ms_disableColourTint;
bool ptfxDebug::ms_disableAlphaTint;
bool ptfxDebug::ms_disableAddVelocity;

bool ptfxDebug::ms_onlyPlayThisFx;
char ptfxDebug::ms_onlyPlayThisFxName[64];

bool ptfxDebug::ms_overrideAllTriggered;
char ptfxDebug::ms_overrideAllTriggeredName[64];
bool ptfxDebug::ms_overrideAllRegistered;
char ptfxDebug::ms_overrideAllRegisteredName[64];
//bool ptfxDebug::ms_overrideThisFx;
char ptfxDebug::ms_overrideThisFxName[64];
char ptfxDebug::ms_overrideWithThisFxName[64];

char ptfxDebug::ms_debugFxName[64];
ptxEffectInst* ptfxDebug::ms_pDebugFxInst;
Mat34V ptfxDebug::ms_debugFxMat;
float ptfxDebug::ms_debugFxRotateSpeedX;
float ptfxDebug::ms_debugFxRotateSpeedY;
float ptfxDebug::ms_debugFxRotateSpeedZ;
bool ptfxDebug::ms_debugFxFlipAxisX;
bool ptfxDebug::ms_debugFxFlipAxisY;
bool ptfxDebug::ms_debugFxFlipAxisZ;
float ptfxDebug::ms_debugFxRadius;
float ptfxDebug::ms_debugFxSpeed; 
float ptfxDebug::ms_debugFxCurrTheta;
Vec3V ptfxDebug::ms_debugFxOffsetPos;
float ptfxDebug::ms_debugFxEvo1;
float ptfxDebug::ms_debugFxEvo2;
float ptfxDebug::ms_debugFxVelocityMult;
bool ptfxDebug::ms_debugFxOverrideCreationDomainSizeOn;
float ptfxDebug::ms_debugFxOverrideCreationDomainSizeX;
float ptfxDebug::ms_debugFxOverrideCreationDomainSizeY;
float ptfxDebug::ms_debugFxOverrideCreationDomainSizeZ;
bool ptfxDebug::ms_debugAttachToCamera;
bool ptfxDebug::ms_animateEvo1;
bool ptfxDebug::ms_animateEvo2;
bool ptfxDebug::ms_applyRGBATint;
u8 ptfxDebug::ms_tintR;
u8 ptfxDebug::ms_tintG;
u8 ptfxDebug::ms_tintB;
u8 ptfxDebug::ms_tintA;

char ptfxDebug::ms_debugAssetName[64];


// code
void ptfxDebug::Init()
{
	ms_pGameGroup = NULL;
	ms_pRmptfxGroup = NULL;

	// set up the initial state and widgets
	ms_numFxAssets = 0;
	ms_numPointsActive = 0;
	ms_numFxInstsActive = 0;
	ms_numFxAttached = 0;
	ms_numFxRegistered = 0;
	ms_numFxScripted = 0;
	ms_numFxHistories = 0;

	ms_disablePtFx = false;
	ms_disableColourTint = false;
	ms_disableAlphaTint = false;
	ms_disableAddVelocity = false;

	ms_onlyPlayThisFx = false;
	sprintf(ms_onlyPlayThisFxName, "%s", "");

	if (PARAM_ptfxonlyplaythis.Get())
	{
		const char* pOnlyPlayThisFxName = NULL;
		PARAM_ptfxonlyplaythis.Get(pOnlyPlayThisFxName);

		ms_onlyPlayThisFx = true;
		sprintf(ms_onlyPlayThisFxName, "%s", pOnlyPlayThisFxName);
	}

	if (PARAM_ptfxperformancetest.Get())
	{
		ms_onlyPlayThisFx = true;
		sprintf(ms_onlyPlayThisFxName, "%s", "xxx");
	}

#if RMPTFX_BANK
 	if (PARAM_ptfxdebugdraw.Get())
 	{
 		ptxDebug::sm_enableDebugDrawing = true;
 	}
#endif // RMPTFX_BANK

	ms_overrideAllTriggered = false;
	sprintf(ms_overrideAllTriggeredName, "%s", "");
	ms_overrideAllRegistered = false;
	sprintf(ms_overrideAllRegisteredName, "%s", "");
	//ms_overrideThisFx = false;
	sprintf(ms_overrideThisFxName, "%s", "");
	sprintf(ms_overrideWithThisFxName, "%s", "");

	sprintf(ms_debugFxName, "%s", "");
	ms_pDebugFxInst = NULL;
	ms_debugFxMat = Mat34V(V_IDENTITY);
	ms_debugFxRotateSpeedX = 0.0f;
	ms_debugFxRotateSpeedY = 0.0f;
	ms_debugFxRotateSpeedZ = 0.0f;
	ms_debugFxFlipAxisX = false;
	ms_debugFxFlipAxisY = false;
	ms_debugFxFlipAxisZ = false;
	ms_debugFxRadius = 0.0f;
	ms_debugFxSpeed = 0.0f;
	ms_debugFxCurrTheta = 0.0f;
	ms_debugFxOffsetPos = Vec3V(V_ZERO);
	ms_debugFxEvo1 = 0.0f;
	ms_debugFxEvo2 = 0.0f;
	ms_debugFxVelocityMult = 1.0f;
	ms_debugFxOverrideCreationDomainSizeOn = false;
	ms_debugFxOverrideCreationDomainSizeX = 1.0f;
	ms_debugFxOverrideCreationDomainSizeY = 1.0f;
	ms_debugFxOverrideCreationDomainSizeZ = 1.0f;
	ms_debugAttachToCamera = false;
	ms_animateEvo1 = false;
	ms_animateEvo2 = false;
	ms_applyRGBATint = false;
	ms_tintR = 255;
	ms_tintG = 255;
	ms_tintB = 255;
	ms_tintA = 255;

	sprintf(ms_debugAssetName, "%s", "");
}

void ptfxDebug::Update()
{
#if RMPTFX_BANK
	if (PARAM_ptfxdebugdraw.Get())
	{
		ptxDebug::sm_enableDebugDrawing = true;
	}
#endif // RMPTFX_BANK

	if (ms_pDebugFxInst)
	{
		if (!ms_pDebugFxInst->GetIsPlaying())
		{
			ms_pDebugFxInst = NULL;
			return;
		}
		
		// set offset position
		float offsetX = Sinf(ms_debugFxCurrTheta);
		float offsetY = Cosf(ms_debugFxCurrTheta);

		Vec3V vPrevOffsetPos = ms_debugFxOffsetPos;
		ms_debugFxOffsetPos.SetX(offsetX * ms_debugFxRadius);
		ms_debugFxOffsetPos.SetY(offsetY * ms_debugFxRadius);
		ms_debugFxOffsetPos.SetZ(ScalarV(V_ZERO));

		ms_debugFxCurrTheta += ms_debugFxSpeed;
		if (ms_debugFxCurrTheta>PI)
		{
			ms_debugFxCurrTheta = -PI;
		}
		else if (ms_debugFxCurrTheta<-PI)
		{
			ms_debugFxCurrTheta = PI;
		}

		ms_pDebugFxInst->SetOffsetPos(ms_debugFxOffsetPos);

		if (ms_debugAttachToCamera)
		{
			ms_debugFxMat = ptfxManager::ms_cameraMtx;
		}
		else
		{
			// rotate the base matrix
			Mat34V rotMat;
			Mat34VFromEulersXYZ(rotMat, Vec3V(ms_debugFxRotateSpeedX, ms_debugFxRotateSpeedY, ms_debugFxRotateSpeedZ), Vec3V(V_ZERO));
			Transform(ms_debugFxMat, rotMat);
		}

		ms_pDebugFxInst->SetBaseMtx(ms_debugFxMat);

		// animate evolutions
		float currTime = fwTimer::GetTimeInMilliseconds()/1000.0f;
		float sinTime = sinf(currTime);
		float cosTime = cosf(currTime);

		if (ms_animateEvo1)
		{
			ms_debugFxEvo1 = 0.5f + (sinTime*0.5f);
		}

		if (ms_animateEvo2)
		{
			ms_debugFxEvo2 = 0.5f + (cosTime*0.5f);
		}

		if (ms_applyRGBATint)
		{
			ms_pDebugFxInst->SetColourTint(Vec3V(ms_tintR/255.0f, ms_tintG/255.0f, ms_tintB/255.0f));
			ms_pDebugFxInst->SetAlphaTint(ms_tintA/255.0f);
		}

		// update evolution variable
		ms_pDebugFxInst->SetEvoValueFromIndex(0, ms_debugFxEvo1);
		ms_pDebugFxInst->SetEvoValueFromIndex(1, ms_debugFxEvo2);

		// set velocity of system
		Vec3V vVel = ms_debugFxOffsetPos - vPrevOffsetPos;
		vVel *= ScalarVFromF32(fwTimer::GetInvTimeStep());
		vVel *= ScalarVFromF32(ms_debugFxVelocityMult);
		ms_pDebugFxInst->SetVelocityAdd(vVel);

		// flip axes
		u8 invertAxes = 0;
		if (ms_debugFxFlipAxisX) invertAxes |= PTXEFFECTINVERTAXIS_X;
		if (ms_debugFxFlipAxisY) invertAxes |= PTXEFFECTINVERTAXIS_Y;
		if (ms_debugFxFlipAxisZ) invertAxes |= PTXEFFECTINVERTAXIS_Z;
		ms_pDebugFxInst->SetInvertAxes(invertAxes);

		// override the emitter domain size
		if (ms_debugFxOverrideCreationDomainSizeOn)
		{
			ms_pDebugFxInst->SetOverrideCreationDomainSize(Vec3V(ms_debugFxOverrideCreationDomainSizeX, ms_debugFxOverrideCreationDomainSizeY, ms_debugFxOverrideCreationDomainSizeZ));
		}
		else
		{
			ms_pDebugFxInst->SetOverrideCreationDomainSize(Vec3V(V_ZERO));
		}
	}
}

void ptfxDebug::StartDebugFx()
{
	StopDebugFx();

	if (ms_pDebugFxInst==NULL)
	{
		ms_pDebugFxInst = ptfxWrapper::GetEffectInst(atHashWithStringNotFinal(ms_debugFxName), atHashWithStringNotFinal(u32(0)));
		if (ms_pDebugFxInst)
		{
			// setup the effect matrix
			ms_debugFxMat.SetCol3(ptfxManager::ms_cameraMtx.GetCol3());
			ms_debugFxMat.SetCol3(ms_debugFxMat.GetCol3() + ptfxManager::ms_cameraMtx.GetCol1()*ScalarV(V_TEN));

			ms_pDebugFxInst->SetBaseMtx(ms_debugFxMat);

			// start the effect
			ms_pDebugFxInst->Start();
		}
	}
}

void ptfxDebug::StopDebugFx()
{
	if (ms_pDebugFxInst)
	{
		ptfxWrapper::StopEffectInst(ms_pDebugFxInst);
		ms_pDebugFxInst = NULL;
	}
}

void ptfxDebug::ResetDebugFx()
{
	if (ms_pDebugFxInst)
	{
		ptfxManager::RemovePtFx(ms_pDebugFxInst);
		ms_pDebugFxInst = NULL;
	}
}

void ptfxDebug::ResetDebugFxRotation()
{
	ms_debugFxMat.SetIdentity3x3();
}

void ptfxDebug::OutputActiveEffectInsts()
{
	ptfxDisplayf("\nRMPTFX INFO: **************************************************\n\n");
#if __DEV
	RMPTFXMGR.OutputActiveEffectInsts();
#endif
	ptfxDisplayf("Num Scripted Fx %d\n", ms_numFxScripted);
	ptfxDisplayf("***************************************************************\n\n");
}

void ptfxDebug::RemoveDefaultKeyframes()
{
	ptfxManager::GetAssetStore().RemoveDefaultKeyframes();
}

void ptfxDebug::PrintAssetList()
{
	ptfxManager::GetAssetStore().OutputLoadedAssets();
}

void ptfxDebug::RequestAsset()
{
	strLocalIndex slot = ptfxManager::GetAssetStore().FindSlotFromHashKey(atHashValue(ms_debugAssetName));
	if (ptfxVerifyf(slot.IsValid(), "cannot find particle asset with this name in any loaded rpf (%s)", ms_debugAssetName))
	{
		ptfxManager::GetAssetStore().StreamingRequest(slot, STRFLAG_DONTDELETE);
	}
}

void ptfxDebug::ReleaseAsset()
{
	strLocalIndex slot = ptfxManager::GetAssetStore().FindSlotFromHashKey(atHashValue(ms_debugAssetName));
	if (ptfxVerifyf(slot.IsValid(), "cannot find particle asset with this name in any loaded rpf (%s)", ms_debugAssetName))
	{
		ptfxManager::GetAssetStore().ClearRequiredFlag(slot.Get(), STRFLAG_DONTDELETE);
	}
}

void ptfxDebug::RemoveAsset()
{
	strLocalIndex slot = ptfxManager::GetAssetStore().FindSlotFromHashKey(atHashValue(ms_debugAssetName));
	if (ptfxVerifyf(slot.IsValid(), "cannot find particle asset with this name in any loaded rpf (%s)", ms_debugAssetName))
	{
		ptfxManager::GetAssetStore().StreamingRemove(slot);
	}
}

void ptfxDebug::AddOverride()
{
	ptfxManager::AddPtFxOverrideInfo(ms_overrideThisFxName, ms_overrideWithThisFxName, (scrThreadId)0xf000000);
}

void ptfxDebug::RemoveOverride()
{
	ptfxManager::RemovePtFxOverrideInfo(ms_overrideThisFxName, (scrThreadId)0xf000000);
}

void ptfxDebug::RemoveAllOverrides()
{
	ptfxManager::RemoveScript((scrThreadId)0xf000000);
}

void ptfxDebug::OutputAllOverrides()
{
	ptfxManager::OutputPtFxOverrideInfos();
}





void ptfxDebug::InitWidgets()
{
	bkBank* pVfxBank = vfxWidget::GetBank();
	if (pVfxBank)
	{
		pVfxBank->PushGroup("Particles", false);	
		{
			//RMPTFX
			ms_pRmptfxGroup = pVfxBank->PushGroup("RMPTFX", false);	
			{
			}
			pVfxBank->PopGroup();

			//Framework
			pVfxBank->PushGroup("Framework", false);	
			{
				pVfxBank->AddTitle("INFO");
				const int numAssets = ptfxManager::GetAssetStore().GetSize();
				pVfxBank->AddSlider("Num Assets", &ms_numFxAssets, 0, numAssets, 0);
				pVfxBank->AddSlider("Num Active Points", &ms_numPointsActive, 0, ptfxManager::ms_maxPoints, 0);
				pVfxBank->AddSlider("Num Active Insts", &ms_numFxInstsActive, 0, ptfxManager::ms_maxInsts, 0);
				pVfxBank->AddSlider("Num Attached", &ms_numFxAttached, 0, ptfxAttach::ms_attachInfos.GetCapacity(), 0);
				pVfxBank->AddSlider("Num Registered", &ms_numFxRegistered, 0, ptfxReg::ms_regFxInsts.GetCapacity(), 0);
				pVfxBank->AddSlider("Num Scripted", &ms_numFxScripted, 0, ptfxScript::ms_infos.GetCapacity(), 0);
				pVfxBank->AddSlider("Num Histories", &ms_numFxHistories, 0, ptfxHistory::ms_infos.GetCapacity(), 0);

				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("DEBUG TOGGLES");
				pVfxBank->AddToggle("Disable All", &ms_disablePtFx);
				pVfxBank->AddToggle("Disable Colour Tint", &ms_disableColourTint);
				pVfxBank->AddToggle("Disable Alpha Tint", &ms_disableAlphaTint);
				pVfxBank->AddToggle("Disable Add Velocity", &ms_disableAddVelocity);

				pVfxBank->AddToggle("Only Play This", &ms_onlyPlayThisFx);
				pVfxBank->AddText("Only Play This Name", ms_onlyPlayThisFxName, 64);

				pVfxBank->AddToggle("Override All Triggered", &ms_overrideAllTriggered);
				pVfxBank->AddText("Override All Triggered Name", ms_overrideAllTriggeredName, 64);
				pVfxBank->AddToggle("Override All Registered", &ms_overrideAllRegistered);
				pVfxBank->AddText("Override All Registered Name", ms_overrideAllRegisteredName, 64);
				//pVfxBank->AddToggle("Override This", &ms_overrideThisFx);
				pVfxBank->AddText("Override This Name", ms_overrideThisFxName, 64);
				pVfxBank->AddText("Override With This Name", ms_overrideWithThisFxName, 64);
				pVfxBank->AddButton("Add Override", AddOverride);
				pVfxBank->AddButton("Remove Override", RemoveOverride);
				pVfxBank->AddButton("Remove All Overrides", RemoveAllOverrides);
				pVfxBank->AddButton("Output All Overrides", OutputAllOverrides);

				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("DEBUG EFFECT INST");
				pVfxBank->AddText("Name", ms_debugFxName,64);
				pVfxBank->AddButton("Start", StartDebugFx);
				pVfxBank->AddButton("Stop", StopDebugFx);
				pVfxBank->AddButton("Reset", ResetDebugFx);
				pVfxBank->AddButton("Reset Rotation", ResetDebugFxRotation);
				pVfxBank->AddSlider("Rotation Speed X", &ms_debugFxRotateSpeedX, -1.0f, 1.0f, 0.01f);
				pVfxBank->AddSlider("Rotation Speed Y", &ms_debugFxRotateSpeedY, -1.0f, 1.0f, 0.01f);
				pVfxBank->AddSlider("Rotation Speed Z", &ms_debugFxRotateSpeedZ, -1.0f, 1.0f, 0.01f);
				pVfxBank->AddToggle("Flip Axis X", &ms_debugFxFlipAxisX);
				pVfxBank->AddToggle("Flip Axis Y", &ms_debugFxFlipAxisY);
				pVfxBank->AddToggle("Flip Axis Z", &ms_debugFxFlipAxisZ);
				pVfxBank->AddSlider("Radius", &ms_debugFxRadius, 0.0f, 10.0f, 0.1f);
				pVfxBank->AddSlider("Speed", &ms_debugFxSpeed, -1.0f, 1.0f, 0.01f);
				pVfxBank->AddSlider("Evo 1", &ms_debugFxEvo1, 0.0f, 1.0f, 0.02f);
				pVfxBank->AddSlider("Evo 2", &ms_debugFxEvo2, 0.0f, 1.0f, 0.02f);
				pVfxBank->AddSlider("Velocity Multiplier", &ms_debugFxVelocityMult, -10.0f, 10.0f, 0.25f);
				pVfxBank->AddToggle("Override Emitter Domain Size", &ms_debugFxOverrideCreationDomainSizeOn);
				pVfxBank->AddSlider("Override Emitter Domain Size X", &ms_debugFxOverrideCreationDomainSizeX, 0.0f, 200.0f, 0.25f);
				pVfxBank->AddSlider("Override Emitter Domain Size Y", &ms_debugFxOverrideCreationDomainSizeY, 0.0f, 200.0f, 0.25f);
				pVfxBank->AddSlider("Override Emitter Domain Size Z", &ms_debugFxOverrideCreationDomainSizeZ, 0.0f, 200.0f, 0.25f);
				pVfxBank->AddToggle("Attach To Camera", &ms_debugAttachToCamera);
				pVfxBank->AddToggle("Animate Evo 1", &ms_animateEvo1);
				pVfxBank->AddToggle("Animate Evo 2", &ms_animateEvo2);
				pVfxBank->AddToggle("Apply RGBA Tint", &ms_applyRGBATint);
				pVfxBank->AddSlider("Tint R", &ms_tintR, 0, 255, 1);
				pVfxBank->AddSlider("Tint G", &ms_tintG, 0, 255, 1);
				pVfxBank->AddSlider("Tint B", &ms_tintB, 0, 255, 1);
				pVfxBank->AddSlider("Tint A", &ms_tintA, 0, 255, 1);

				pVfxBank->AddTitle("DEBUG MISC");
				pVfxBank->AddButton("Remove Default Keyframes", RemoveDefaultKeyframes);
				pVfxBank->AddButton("Print Asset List", PrintAssetList);
				pVfxBank->AddText("Debug Asset Name", ms_debugAssetName,64);
				pVfxBank->AddButton("Request Debug Asset", RequestAsset);
				pVfxBank->AddButton("Release Debug Asset", ReleaseAsset);
				pVfxBank->AddButton("Remove Debug Asset", RemoveAsset);
				pVfxBank->AddButton("Print EffectInst List", OutputActiveEffectInsts);

			}
			pVfxBank->PopGroup();

			//Game
			ms_pGameGroup = pVfxBank->PushGroup("Game", false);	
			{
			}
			pVfxBank->PopGroup();
		}
		pVfxBank->PopGroup();
	}
}


} // namespace rage


#endif // __BANK

