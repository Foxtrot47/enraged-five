//
// vfx/ptfxwrapper.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXWRAPPER_H
#define VFX_PTFXWRAPPER_H


// includes (rage)
#include "atl/hashstring.h"
#include "vectormath/vec3v.h"


// namespaces 
namespace rage {


// forward declarations
class ptxEffectInst;
class ptxEffectRule;


// structures


// classes

// PURPOSE
//  
class ptfxWrapper
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static ptxEffectInst* GetEffectInst(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListHashName);
		static ptxEffectInst* GetEffectInst(ptxEffectRule* pEffectRule);
		static ptxEffectRule* GetEffectRule(const atHashWithStringNotFinal effectRuleHashName, const atHashWithStringNotFinal fxListHashName);
		static ptxEffectRule* GetEffectRule(const atHashWithStringNotFinal effectRuleHashName);
		static void StopEffectInst(ptxEffectInst* pFxInst);
		static void SetAlphaTint(ptxEffectInst* pFxInst, float alphaTint);
		static void SetColourTint(ptxEffectInst* pFxInst, Vec3V_In vColourTint);
		static void SetVelocityAdd(ptxEffectInst* pFxInst, Vec3V_In vVelocity);

}; 


} // namespace rage

#endif // VFX_PTFXWRAPPER_H


