//
// vfx/ptfxreg.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXREG_H
#define VFX_PTFXREG_H


// includes (rage)
#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/map.h"

// includes (framework)
#include "vfx/vfxlist.h"
#include "vfx/ptfx/ptfxconfig.h"


// namespaces 
namespace rage {


// forward declarations
class ptfxManager;
class ptxEffectInst;
class ptxEffectRule;


// classes

// PURPOSE
//  
class ptfxRegInst : public vfxListItem
{
	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	//private: //////////////////////////
	public: ///////////////////////////

		// identification data
		const void* m_pOwnerClass;						// the address of the class that this effect is being registered using (e.g. vehicle, projectile, fire)
		int m_classFxOffset;							// the effect offset for this class type (e.g. veh_overheat, veh_exhaust, veh_propeller)
		const void* m_mapKey;							// key for looking up data in the map (derived from the owner class and class offset)						

		// particle instance data
		ptxEffectInst* m_pFxInst;

		// registration data
		float m_timeSinceLastRegistered;
		bool m_regThisFrame;
		bool m_isOneShotSpecialCase;

		//
		bool m_isRegReffed;								// whether or not our owner class will be automatically nulled when the owner class is deleted

		// audio data
		bool m_hasAudio;

}; 


// PURPOSE
//  
class ptfxReg
{
	friend class ptfxDebug;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(int maxSize);
		static void InitLevel();	
		static void ShutdownLevel();
		static void Update(float deltaTime);	

		static ptxEffectInst* Register(const void* pOwnerClass, int classFxOffset, bool canRegRef, atHashWithStringNotFinal effectRuleHashName, atHashWithStringNotFinal fxListHashName, bool& justCreated, bool isOneShotSpecialCase=false);
		static ptxEffectInst* Register(const void* pOwnerClass, int classFxOffset, bool canRegRef, ptxEffectRule* pEffectRule, bool& justCreated, bool isOneShotSpecialCase=false);
		static void UnRegister(ptxEffectInst* pFxInst, bool finishPtFx=false);
		static void UnRegister(const void* pOwnerClass, bool finishPtFx=false);

//		static bool Exists(void* pOwnerClass, int  numClassFxOffsets=1);
		static ptfxRegInst* Find(const void* pOwnerClass, int classFxOffset);


	private: //////////////////////////

		static void UnRegister(ptfxRegInst* pRegFxInst, bool finishPtFx=false);
		static ptfxRegInst* Find(ptxEffectInst* pFxInst);

		static ptfxRegInst* GetRegFxInstFromPool(const void* mapKey);
		static void ReturnRegFxInstToPool(ptfxRegInst* pRegFxInst);

		static const void* GetMapKey(const void* pOwnerClass, int classFxOffset) {return static_cast<const u8*>(pOwnerClass) + classFxOffset;}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	private: //////////////////////////

		static atArray<ptfxRegInst> ms_regFxInsts;
		static atMap<const void*, ptfxRegInst*> ms_regInstMap;
		static vfxList ms_regFxInstPool;
		static vfxList ms_regFxInstList;

};


} // namespace rage

#endif // VFX_PTFXREG_H


