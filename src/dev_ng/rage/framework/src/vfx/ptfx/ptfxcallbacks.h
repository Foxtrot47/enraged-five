//
// vfx/ptfxcallbacks.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXCALLBACKS_H
#define VFX_PTFXCALLBACKS_H


// includes (rage)
#include "phcore/materialmgr.h"
#include "vectormath/classes.h"

// includes (framework)
#include "fwscene/world/InteriorLocation.h"


// namespaces 
namespace rage {


// forward declarations
class ptxEffectInst;
class ptxEmitterInst;
class ptfxRegInst;


// structures
struct ptfxLightInfo
{
	ptxEffectInst* pEffectInst; 
	ptxEmitterInst* pEmitterInst; 
	ptxPointItem* pPointItem; 
	Vec3V vPos;
	Color32 lightCol;
	float lightIntensity; 
	float lightFalloff; 
	float lightRange;
	float lightAngle;
	Color32 coronaCol;
	float coronaIntensity; 
	float coronaSize;
	float coronaFlare;
	float coronaZBias;
	bool castsShadows;
	bool coronaNotInReflection;
	bool coronaOnlyInReflection;
	bool coronasAdded;
	int lightType;
};

struct ptfxFireInfo
{
	Vec3V vPos;
	Vec3V vNormal;
	phMaterialMgr::Id mtlId;
	float burnTime;
	float burnStrength;
	float peakTime;
	float fuelLevel;
	float fuelBurnRate;
	int numGenerations;
};

struct ptfxDecalInfo
{
	ptxEffectInst* pEffectInst; 
	int decalId;
	Vec3V vPos;
	Vec3V vDir;
	float width;
	float height;
	float totalLife;
	float fadeInTime;
	float uvMultStart;
	float uvMultEnd;
	float uvMultTime;
	float duplicateRejectDist;
	float projectionDepth;
	float distanceScale;
	bool flipU;
	bool flipV;
	bool useComplexColn;
	bool isDirectional;
	u8 colR;
	u8 colG;
	u8 colB;
	u8 colA;
};

struct ptfxDecalPoolInfo
{
	Vec3V vPos;
	Vec3V vNormal;
	int liquidType;
	u32 decalRenderSettingId;
	float startSize;
	float endSize;
	float growthRate;
	u8 mtlId;
	u8 colR;
	u8 colG;
	u8 colB;
	u8 colA;
};

struct ptfxDecalTrailInfo
{
	ptxEffectInst* pEffectInst; 
	Vec3V vPos;
	Vec3V vNormal;
	int liquidType;
	u32 decalRenderSettingId;
	float widthMin;
	float widthMax;
	u8 mtlId;
	u8 colR;
	u8 colG;
	u8 colB;
	u8 colA;
};

struct ptfxLiquidInfo
{
	ptxEffectInst* pEffectInst; 
	Vec3V vPos;
	Vec3V vNormal;
	int liquidType;
	float trailWidthMin;
	float trailWidthMax;
	float poolStartSize;
	float poolEndSize;
	float poolGrowthRate;
	u8 mtlId;
	u8 colR;
	u8 colG;
	u8 colB;
	u8 colA;
};

struct ptfxFogVolumeInfo
{
public:
	ptfxFogVolumeInfo()
		: vPos(V_ZERO)
		, vScale(V_ONE)
		, vRotation(V_ZERO)
		, col(0.0f,0.0f,0.0f,0.0f)
		, density(0.0f)
		, falloff(0.0f)
		, range(0.0f)
		, lightingType(0)
		, hdrMult(1.0f)
		, useGroundFogColour(false)
	{
		interiorLocation.MakeInvalid();
	}
	Vec3V vPos;
	Vec3V vScale;
	Vec3V vRotation;
	Color32 col;
	float density; 
	float falloff; 
	float range;
	float hdrMult;
	int lightingType;
	bool useGroundFogColour;
	fwInteriorLocation interiorLocation;
};

// classes
class ptfxCallbacks
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		virtual ~ptfxCallbacks() {}

		// general
		virtual	void GetMatrixFromBoneIndex(Mat34V_InOut boneMtx, const void* pEntity, int boneIndex, bool useAltSkeleton=false) = 0;

		// ptfxmanager
		virtual void UpdateFxInst(ptxEffectInst* pFxInst) = 0;
		virtual bool RemoveFxInst(ptxEffectInst* pFxInst) = 0;
		virtual void ShutdownFxInst(ptxEffectInst* pFxInst) = 0;

		// ptfxreg
		virtual void RemoveScriptResource(int ptfxScriptId) = 0;

		// ptfxattach - entity referencing
		virtual void AddEntityRef(const void* pEntity, void** ppReference) = 0;
		virtual void RemoveEntityRef(const void* pEntity, void** ppReference) = 0;

		// ptfxattach - entity queries
		virtual void* GetEntityDrawable(const void* pEntity) = 0;

		// behaviours
		virtual Vec3V_Out GetGameCamPos() = 0;
		virtual float GetGameCamWaterZ() = 0;
		virtual float GetGravitationalAcceleration() = 0;
		virtual float GetAirResistance() = 0;
		virtual float GetWaterZ(Vec3V_In vPos, ptxEffectInst* pEffectInst) = 0;
		virtual void GetRiverVel(Vec3V_In vPos, Vec3V_InOut vVel) = 0;

		virtual void AddLight(ptfxLightInfo& lightInfo) = 0;
		virtual void AddFire(ptfxFireInfo& fireInfo) = 0;
		virtual void AddDecal(ptfxDecalInfo& decalInfo) = 0;
		virtual void AddDecalPool(ptfxDecalPoolInfo& decalPoolInfo) = 0;
		virtual void AddDecalTrail(ptfxDecalTrailInfo& decalTrailInfo) = 0;
		virtual void AddLiquid(ptfxLiquidInfo& liquidInfo) = 0;
		virtual void AddFogVolume(ptfxFogVolumeInfo& fogVolumeInfo) = 0;

		// render behaviors
		virtual void DrawFogVolume(ptfxFogVolumeInfo& fogVolumeInfo) = 0;

		// replay
		virtual bool CanGetTriggeredInst() = 0;
		virtual bool CanGetRegisteredInst(bool) = 0;

};

extern rage::ptfxCallbacks* g_pPtfxCallbacks;

} // namespace rage

#endif // VFX_PTFXCALLBACKS_H
