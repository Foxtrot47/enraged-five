//
// vfx/ptfxdebug.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_PTFXDEBUG_H
#define VFX_PTFXDEBUG_H


#if __BANK


// includes (rage)
#include "vectormath/classes.h"


// namespaces 
namespace rage {


// forward declarations
class bkBank;
class bkGroup;
class ptfxManager;
class ptxEffectInst;


// classes

// PURPOSE
//  
class ptfxDebug
{
	friend class ptfxManager;
	friend class ptfxAssetStore;
	friend class ptfxAttach;
	friend class ptfxReg;
	friend class ptfxScript;
	friend class ptfxHistory;
	friend class ptfxWrapper;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////		

	public: ///////////////////////////

		static void Init();
		static void Update();

		static void SetDisablePtFx(const bool value) {ms_disablePtFx = value;}
		static bool GetDisablePtFx() {return ms_disablePtFx;}

		static void	SetOnlyPlayThisFx(bool val) {ms_onlyPlayThisFx = val;}

		//
		static bkGroup* GetGameGroup() {return ms_pGameGroup;}
		static bkGroup* GetRMPTFXGroup() {return ms_pRmptfxGroup;}


	private: //////////////////////////

		static void InitWidgets();

		static void StartDebugFx();
		static void StopDebugFx();
		static void ResetDebugFx();
		static void UpdateDebugFx();
		static void ResetDebugFxRotation();	

		static void OutputActiveEffectInsts();
		static void RemoveDefaultKeyframes();
		static void PrintAssetList();
		static void RequestAsset();
		static void ReleaseAsset();
		static void RemoveAsset();

		static void AddOverride();
		static void RemoveOverride();
		static void RemoveAllOverrides();
		static void OutputAllOverrides();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	private: //////////////////////////

		static bkGroup* ms_pGameGroup;
		static bkGroup* ms_pRmptfxGroup;

		static int ms_numFxAssets;
		static int ms_numPointsActive;
		static int ms_numFxInstsActive;
		static int ms_numFxAttached;
		static int ms_numFxRegistered;
		static int ms_numFxScripted;
		static int ms_numFxHistories;

		static bool ms_disablePtFx;
		static bool ms_disableColourTint;
		static bool ms_disableAlphaTint;
		static bool ms_disableAddVelocity;

		static bool ms_onlyPlayThisFx;
		static char ms_onlyPlayThisFxName[64];

		static bool ms_overrideAllTriggered;
		static char ms_overrideAllTriggeredName[64];
		static bool ms_overrideAllRegistered;
		static char ms_overrideAllRegisteredName[64];
		//static bool ms_overrideThisFx;
		static char ms_overrideThisFxName[64];
		static char ms_overrideWithThisFxName[64];

		static char ms_debugFxName[64];
		static ptxEffectInst* ms_pDebugFxInst;
		static Mat34V ms_debugFxMat;
		static float ms_debugFxRotateSpeedX;
		static float ms_debugFxRotateSpeedY;
		static float ms_debugFxRotateSpeedZ;
		static bool ms_debugFxFlipAxisX;
		static bool ms_debugFxFlipAxisY;
		static bool ms_debugFxFlipAxisZ;
		static float ms_debugFxRadius;
		static float ms_debugFxSpeed; 
		static float ms_debugFxCurrTheta;
		static Vec3V ms_debugFxOffsetPos;
		static float ms_debugFxEvo1;
		static float ms_debugFxEvo2;
		static float ms_debugFxVelocityMult;
		static bool ms_debugFxOverrideCreationDomainSizeOn;
		static float ms_debugFxOverrideCreationDomainSizeX;
		static float ms_debugFxOverrideCreationDomainSizeY;
		static float ms_debugFxOverrideCreationDomainSizeZ;
		static bool	ms_debugAttachToCamera;
		static bool ms_animateEvo1;
		static bool ms_animateEvo2;
		static bool ms_applyRGBATint;
		static u8 ms_tintR;
		static u8 ms_tintG;
		static u8 ms_tintB;
		static u8 ms_tintA;

		static char ms_debugAssetName[64];

}; 


} // namespace rage


#endif // __BANK

#endif // VFX_PTFXDEBUG_H


