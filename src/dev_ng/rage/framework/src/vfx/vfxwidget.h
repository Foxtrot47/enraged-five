//
// vfx/vfxwidget.h
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved. 
//

#ifndef VFX_WIDGET_H
#define VFX_WIDGET_H

#if __SPU
// use #include instead of #error, as then gcc gives the include stack
#include "error, vfxwidget.h not supported in spu code"
#endif

// includes (rage)
#include "bank/bkmgr.h"

// includes (framework)

// includes (game)


// namespaces 
namespace rage {


// forward declarations


// classes


// PURPOSE
//  
#if __BANK
class vfxWidget
{
	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		static void Init() {ms_pBank = &BANKMGR.CreateBank("Visual Effects", 0, 0, false);}
		static bkBank* GetBank() {return ms_pBank;}


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

		static bkBank* ms_pBank;

};
#endif


} // namespace rage

#endif // VFX_WIDGET_H



