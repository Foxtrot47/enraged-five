//
// vfx/decal/decalcallbacks.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved.
//

#ifndef VFX_DECALCALLBACKS_H
#define VFX_DECALCALLBACKS_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "grcore/effect_config.h"
#include "phcore/materialmgr.h"
#include "physics/leveldefs.h"
#include "vector/color32.h"
#include "vectormath/classes.h"

// includes (framework)
#include "vfx/decal/decalasynctasktype.h"
#include "vfx/decal/decalbones.h"
#include "vfx/decal/decalbucket.h"
#include "vfx/decal/decalmanagerenables.h"
#include "vfx/decal/decalsettings.h"

// namespaces
namespace rage {


// forward declarations
class  crSkeleton;
struct decalAsyncTaskDescBase;
class  decalSettings;
class  fragInst;
class  fragType;
class  fwEntity;
class  fwTexturePayload;
class  grcRenderTarget;
class  grcTexture;
class  grcViewport;
class  grmShader;
class  phBoundComposite;
class  phInst;
class  rmcDrawable;
class  rmcLod;


// classes
class decalCallbacks
{
	protected:

		///////////////////////////////////////////////////////////////////////
		//  IMPORTANT                                                        //
		//-------------------------------------------------------------------//
		//                                                                   //
		// When adding or removing virtual functions that are compiled into  //
		// SPU code, the enum NUM_SPU_VIRTUAL_FUNCTIONS must be kept up to   //
		// date.                                                             //
		//                                                                   //
		// This is number is used to relocate the functions pointers inside  //
		// the vtable.  This is required since the SPU code is compiled as   //
		// position independent code (PIC).                                  //
		//                                                                   //
		///////////////////////////////////////////////////////////////////////

		enum { NUM_SPU_VIRTUAL_FUNCTIONS = 9 };

	public:

		enum { MAX_SMASH_GROUP_VERTS_PER_BATCH = 510 };     // pow 2 rounded down to multiple of 3

		virtual bool IsGlassShader(const grmShader* pShader) = 0;
		virtual bool IsGlassMaterial(phMaterialMgr::Id mtlId) = 0;
		virtual bool ShouldProcessCompositeBoundFlags(u32 flags) = 0;
		virtual bool ShouldProcessCompositeChildBoundFlags(u32 flags) = 0;
		virtual bool ShouldProcessDrawableBone(const fwEntity* pEntity, decalBoneHierarchyIdx boneIndex) = 0;
		virtual bool ShouldProcessShader(const grmShader* pShader, const fwEntity* pEntity) = 0;
		virtual bool ShouldProcessMaterial(u16 typeSettingsIndex, phMaterialMgr::Id mtlId) = 0;
		virtual bool ShouldBucketBeRendered(const decalBucket* pDecalBucket, decalRenderPass rp) = 0;

		virtual const rmcLod* GetLod(const fwEntity* pEntity, const rmcDrawable* pDrawable) = 0;
		virtual unsigned DecompressSmashGroup(decalAsyncTaskDescBase *taskDesc, Vec3V *pos, Vec3V *nrm) = 0;

		virtual decalAsyncTaskDescBase *AllocAsyncTaskDesk(decalAsyncTaskType type, const decalSettings& settings, const fwEntity* pEnt, const phInst* pInst) = 0;
		virtual void FreeAsyncTaskDesc(decalAsyncTaskDescBase *desc) = 0;
		virtual size_t GetAsyncTaskDescSize() = 0;

		virtual u32 GetIncludeFlags() = 0;
		virtual phLevelNew* GetPhysicsLevel() = 0;
		virtual fwEntity* GetEntityFromInst(phInst* pInst) = 0;
		virtual phInst* GetPhysInstFromEntity(const fwEntity* pEntity) = 0;
		virtual ScalarV_Out GetEntityBoundRadius(const fwEntity* pEntity) = 0;
		virtual size_t GetEntitySize(const fwEntity* pEntity) = 0;
		virtual size_t GetPhysInstSize(const fwEntity* pEntity, const phInst* pPhInst) = 0;
		virtual const rmcDrawable* GetDrawable(const fwEntity* pEntity, const phInst* pInst, bool onlyApplyToGlass, bool dontApplyToGlass) = 0;
		virtual	void GetMatrixFromBoneIndex(Mat34V_InOut boneMtx, bool* isDamaged, const fwEntity* pEntity, decalBoneHierarchyIdx boneIndex) = 0;
		virtual	void GetMatrixFromBoneIndex_Skinned(Mat34V_InOut boneMtx, bool* isDamaged, const fwEntity* pEntity, decalBoneHierarchyIdx boneIndex) = 0;
		virtual	void GetMatrixFromBoneIndex_SmashGroup(Mat34V_InOut boneMtx, const fwEntity* pEntity, decalBoneHierarchyIdx boneIndex) = 0;
		virtual bool ShouldProcessInst(const phInst* pInst, const bool bApplyToBreakable) = 0;
		virtual void PatchModdedDrawables(unsigned* numDrawables, unsigned* damagedDrawableMask, const rmcDrawable** pDrawables, unsigned numSkelMtxs, const Vec4V* skelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, u8* skelDrawables, const fwEntity* pEntity) = 0;

		virtual void ApplyVehicleSuspensionToMtx(Mat34V_InOut vMtx, const fwEntity* pEntity) = 0;

		virtual void BeginRenderDynamic(bool bIsForwardPass) = 0;
		virtual void EndRenderDynamic(bool bIsForwardPass) = 0;
		virtual const grcTexture* GetDepthTexture() = 0;
		virtual const grcTexture* GetNormalTexture() = 0;

		virtual void InitGlobalRender() = 0;
		virtual void ShutdownGlobalRender() = 0;
		virtual void InitBucketForwardRender(Vec4V_In vCentreAndRadius, fwInteriorLocation interiorLocation) = 0;
		virtual void SetGlobals(float natural, float artificial, bool isInInterior) = 0;
		virtual void GetAmbientFromEntity(const fwEntity* pEntity, float &natural, float &artificial, bool &inInterior) = 0;


		virtual Vector4 CalculateProjectionParams(const grcViewport* pViewport = NULL) = 0;

		virtual Vec4V_Out GetCameraPosAndFOVScale() = 0;
		virtual float GetDistSqrToViewport(Vec3V_In vPosition) = 0;
		virtual ScalarV_Out GetDistSqrToViewportV(Vec3V_In vPosition) = 0;
		virtual const grcViewport* GetCurrentActiveGameViewport()= 0;

		virtual bool IsEntityVisible(const fwEntity* pEntity) = 0;
		virtual void GetEntityInfo(const fwEntity* pEntity, u16 typeSettingsIndex, s16 roomId, bool& isPersistent, fwInteriorLocation& interiorLocation, bool& shouldProcess) = 0;

		virtual void GetDamageData(const fwEntity* pEntity, fwTexturePayload** ppDamageMap, float& damageRadius, float& damageMult) = 0;

		virtual bool IsParticleUpdateThread() = 0;

		virtual bool IsUnderwater(Vec3V_In vTestPos) = 0;
		virtual bool PerformUnderwaterCheck(Vec3V_In vTestPos) = 0;

		virtual bool IsAllowedToRecycle(u16 typeSettingsIndex) = 0;

#			if HACK_GTA4_MODELINFOIDX_ON_SPU && USE_EDGE
		virtual int GetModelInfoType(u32 modelIndex) = 0;
#			endif

		virtual float GetDistanceMult(u16 typeSettingsIndex) = 0;

		virtual void DecalMerged(int oldDecalId, int newDecalId) = 0;
		virtual void LiquidUVMultChanged(int decalId, float uvMult, Vec3V_In pos) = 0;

	protected:

		// Could safetly be made non-virtual, but not worth
		// the hassle of disabling all the warnings for each compiler.
		virtual	~decalCallbacks() {}
};


} // namespace rage


extern rage::decalCallbacks* g_pDecalCallbacks;

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALCALLBACKS_H
