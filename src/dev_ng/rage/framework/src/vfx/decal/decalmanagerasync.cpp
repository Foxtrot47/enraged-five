//
// vfx/decal/decalmanagerasync.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalmanagerasync.h"

// includes (rage)
#include "crskeleton/skeletondata.h"
#include "diag/art_channel.h"
#include "fragment/cache.h"
#include "fragment/drawable.h"
#include "fragment/instance.h"
#include "fragment/typechild.h"
#include "fragment/typegroup.h"
#include "fragment/typephysicslod.h"
#include "grcore/indexbuffer.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/geometry.h"
#include "phbound/boundbvh.h"
#include "phbound/boundcomposite.h"
#include "phbound/OptimizedBvh.h"
#include "phglass/glassinstance.h"
#include "physics/archetype.h"
#include "physics/levelnew.h"
#include "system/alloca.h"
#include "system/interlocked.h"
#include "system/memops.h"
#include "system/param.h"
#include "vector/geometry.h"
#include "vectormath/classfreefuncsv.h"
#include "vectormath/v4vector4v.h"
#if !__SPU
#	include "breakableglass/breakable.h"
#	include "breakableglass/glassmanager.h"
#	include "grcore/debugdraw.h"
#endif

// includes (framework)
#include "entity/drawdata.h"
#include "entity/entity.h"
#include "fwnet/netinterface.h"
#include "fwscene/world/InteriorLocation.h"
#include "vfx/optimisations.h"
#include "vfx/slistitor.h"
#include "vfx/decal/decalasynctaskdescbase.h"
#include "vfx/decal/decalbounds.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decaldmahelpers.h"
#include "vfx/decal/decaldmatags.h"
#include "vfx/decal/decalmanagerenables.h"
#include "vfx/decal/decalminiidxbufwriter.h"
#include "vfx/decal/decalpools.h"
#include "vfx/decal/decalsettings.h"
#include "vfx/decal/decalvertexcache.h"

// includes (other)
#include <string.h>
#if RSG_DURANGO
#	include "grcore/wrapper_d3d.h"
#	include <xdk.h>
#	include "system/d3d11.h"
#elif __PS3
#	include <sys/sys_fixed_addr.h>
#endif


// defines
#define DECAL_MANAGER_ALLOW_PACKED_NORMALS 0

#if __ASSERT && USE_DEFRAGMENTATION
#	define DECAL_ASSERT_ALREADY_LOCKED(PTR)     decalAssert(m_defragLocker.AssertOnlyIsAlreadyLocked(PTR))
#else
#	define DECAL_ASSERT_ALREADY_LOCKED(PTR)     (void)0
#endif

#if __SPU
#	define DECALASYNC_EDGEIOSIZE        0x0a000
#	define DECALASYNC_EDGESCRATCHSIZE   0x12000
#	define DECALASYNC_EDGETEMPSIZE      (DECALASYNC_EDGEIOSIZE+DECALASYNC_EDGESCRATCHSIZE)
#endif

// Largest and least aligned grmGeometry derived type we need to use.  Note that
// this is different to the behaviour of LARGEST_PHBOUND in that we want the
// least aligned type.  Being least aligned is important for ensuring
// PipelinedDmaGetter allocates a large enough buffer.  The dmaed in object will
// still have a correctly aligned local store address, as the local store
// address is congruent to the effective address mod 16.
#if USE_EDGE
#	define LARGEST_GEOMETRY_TYPE    rage::grmGeometryQB
	CompileTimeAssert(sizeof(LARGEST_GEOMETRY_TYPE) >= sizeof(rage::grmGeometryEdge));
	CompileTimeAssert(sizeof(LARGEST_GEOMETRY_TYPE) >= sizeof(rage::grmGeometryQB));
	CompileTimeAssert(__alignof(LARGEST_GEOMETRY_TYPE) <= __alignof(rage::grmGeometryEdge));
	CompileTimeAssert(__alignof(LARGEST_GEOMETRY_TYPE) <= __alignof(rage::grmGeometryQB));
#else
#	define LARGEST_GEOMETRY_TYPE    rage::grmGeometryQB
	CompileTimeAssert(sizeof(LARGEST_GEOMETRY_TYPE) >= sizeof(rage::grmGeometryQB));
	CompileTimeAssert(__alignof(LARGEST_GEOMETRY_TYPE) <= __alignof(rage::grmGeometryQB));
#endif

// SNC with -Xfastmath will cause alignment issues reading scalar floats from
// non-cacheable memory (eg. RSX local).  The offical workaround is to mark
// pointers as volatile (https://ps3.scedev.net/forums/thread/115910/)
#ifdef __SNC__
#	define SNC_VOLATILE     volatile
#else
#	define SNC_VOLATILE
#endif

// Maximum number of bones that ProcessSkinnedDrawable can handle in one loop.
#if __PPU
 	// For the PPU implementation, it runs on the main thread, so we can do all
 	// bones in one loop.  Also the code to kick the Edge job currently doesn't
 	// work when set to less than DECAL_MAX_BONES.
 	enum { MAX_BONES_PER_LOOP = DECAL_MAX_BONES };
#else
 	// Other platforms run on sysTaskManager worker threads, so we cannot set to
 	// DECAL_MAX_BONES, else we'll blow the stack.
#	if !__SPU
 		enum { MAX_BONES_PER_LOOP = 16 };
#	else
 		enum { MAX_BONES_PER_LOOP = 4 };
#	endif
#endif


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// params
#if !__SPU
	XPARAM(mapMoverOnlyInMultiplayer);
#endif


// namespaces
namespace rage {


// static variables
#if USE_EDGE && !__SPU
	decalEdgeClipInfo decalManagerAsync::ms_edgeClipInfo;
#endif
decalManagerAsync *decalManagerAsync::ms_instance/*=NULL*/;


///////////////////////////////////////////////////////////////////////////////
//  CODE
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//  GetVBInteger
///////////////////////////////////////////////////////////////////////////////

static inline u32 GetVBInteger(const void *ptr)
{
	using namespace Vec;
#	if __SPU
		return si_to_uint((qword)V4LoadUnaligned(ptr));
#	elif __XENON
		// 360 should always be using cachable memory (if the vertex buffer is
		// stored in write-combined memory, then we should be reading from the
		// cachable read-only ea alias).
		return *(const u32*)ptr;
#	else
		// Assume non-aligned read from non-cachable memory.
		u32 ret;
		sysMemCpy(&ret, ptr, 4);
		return ret;
#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  GetVBPosition
///////////////////////////////////////////////////////////////////////////////

static inline Vec3V_Out GetVBPosition(const void *pPos)
{
	return Vec3V(Vec::V4LoadUnalignedSafe<12>(pPos));
}


///////////////////////////////////////////////////////////////////////////////
//  GetVBNormalCompressed
///////////////////////////////////////////////////////////////////////////////

static inline Vec3V_Out GetVBNormalCompressed(const void *pNrm)
{
	using namespace Vec;
	Vector_4V cmp = V4LoadUnalignedSafe<4>(pNrm);
	Vec3V ret;
#	if __PS3
		ret = Vec3V(V4UnpackNormFloats_11_11_10(cmp));
#	else
		ret = Vec3V(V4UnpackNormFloats_10_10_10_X(cmp));
#	endif
	return ret;
}


#if (RSG_PC || RSG_DURANGO || RSG_ORBIS)

	///////////////////////////////////////////////////////////////////////////
	//  GetVBNormalUncompressed
	///////////////////////////////////////////////////////////////////////////

	static inline Vec3V_Out GetVBNormalUncompressed(const void *pNrm)
	{
		return Vec3V(Vec::V4LoadUnalignedSafe<12>(pNrm));
	}

#endif // (RSG_PC || RSG_DURANGO || RSG_ORBIS)


///////////////////////////////////////////////////////////////////////////////
//  GetVBDamageScale
///////////////////////////////////////////////////////////////////////////////

static inline ScalarV_Out GetVBDamageScale(const void *pCpv)
{
	using namespace Vec;
	Vector_4V packed = V4LoadUnalignedSafe<4>(pCpv);
#	if RSG_XENON
		return ScalarV(V4SplatZ(V4Uint8ToFloat32(packed)));
#	else
		return ScalarV(V4SplatY(V4Uint8ToFloat32(packed)));
#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  GetVBBlendIndex
///////////////////////////////////////////////////////////////////////////////

static inline decalBoneVertexIdx GetVBBlendIndex(const void *pIndices)
{
	return (decalBoneVertexIdx)(Color32(GetVBInteger(pIndices)).GetRed());
}


///////////////////////////////////////////////////////////////////////////////
//  CTOR
///////////////////////////////////////////////////////////////////////////////

decalManagerAsync::decalManagerAsync()
{
	Assert(!ms_instance);
#	if __PPU
		m_thisEa = this;
#	endif
	ms_instance = this;

#	if __BANK
		m_modelSpaceOffsetUnits = DECAL_MODEL_SPACE_OFFSET_UNITS_DEFAULT;
#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  DTOR
///////////////////////////////////////////////////////////////////////////////

decalManagerAsync::~decalManagerAsync()
{
	Assert(ms_instance == this);
	ms_instance = NULL;
}


///////////////////////////////////////////////////////////////////////////////
//  GetInsts
///////////////////////////////////////////////////////////////////////////////

bool decalManagerAsync::GetInsts(decalInst** ppInstGlass, decalInst** ppInstNonGlass)
{
	*ppInstGlass = m_pools.GetInst(SPU_ONLY(&m_thisEa->m_pools, DMATAG_BLOCKING));
	if (*ppInstGlass)
	{
		*ppInstNonGlass = m_pools.GetInst(SPU_ONLY(&m_thisEa->m_pools, DMATAG_BLOCKING));
		if (*ppInstNonGlass)
		{
			return true;
		}
		else
		{
			m_pools.ReturnInst(*ppInstGlass SPU_ONLY(, &m_thisEa->m_pools, DMATAG_BLOCKING));
			*ppInstGlass = NULL;
		}
	}

	decalSpamf("Ran out of decal instances");
	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  GetBucket
///////////////////////////////////////////////////////////////////////////////

decalBucket* decalManagerAsync::GetBucket(unsigned bucketLists, decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, ScalarV_In posScale, Vec3V_In msOffsetInt, Vec4V_In boundSphere, decalRenderPass renderPass, decalMethod method, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, decalAttachType attachType, Mat34V_In wldMtx, s16 attachMatrixId, void* pSmashGroup, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted)
{
	decalBucket* pNewBucketEa = m_pools.GetBucket(SPU_ONLY(&m_thisEa->m_pools, DMATAG_BLOCKING));
	if (pNewBucketEa)
	{
#		if !__SPU
			decalBucket* const pNewBucketLs = pNewBucketEa;
#		else
			char bucketBuf[sizeof(decalBucket)] ALIGNED(__alignof__(decalBucket));
			decalBucket* const pNewBucketLs = rage_placement_new(bucketBuf) decalBucket;
#		endif
		pNewBucketLs->Init(taskDescEa, pDecalInstEa, posScale, msOffsetInt, boundSphere, wldMtx, attachMatrixId, attachType, typeSettingsIndex, renderSettingsIndex, subType, isGlass, isDamaged, isPersistent, interiorLocation, isScripted, pSmashGroup);

#		if __SPU
			sysDmaPut(pNewBucketLs, (u32)pNewBucketEa, sizeof(*pNewBucketEa), DMATAG_BLOCKING);
#		endif

		// add to the pending list
		// must be done after the spu sysDmaPut (no need to wait first though, as Push has a fenced put and wait)
		CompileTimeAssert(NELEM(m_pendingBucketLists)    == DECAL_NUM_RENDER_PASSES);
		CompileTimeAssert(NELEM(m_pendingBucketLists[0]) == DECAL_NUM_METHODS);
		const unsigned offset = renderPass*DECAL_NUM_METHODS + method;
		((AsyncSList<decalBucket>*)((uptr)this+bucketLists)+offset)->Push(pNewBucketEa
			SPU_ONLY(, SListDefaultPtrConverter(), (AsyncSList<decalBucket>*)((uptr)m_thisEa+bucketLists)+offset, DMATAG_BLOCKING));
	}
	else
	{
		decalWarningf("Out of buckets, decals will be missing");
	}

	return pNewBucketEa;
}


///////////////////////////////////////////////////////////////////////////////
//  AllocDecalId
///////////////////////////////////////////////////////////////////////////////

int decalManagerAsync::AllocDecalId()
{
	u32 cmp = m_decalIdSeqFree;
	u32 idx;
	for (;;)
	{
		idx = cmp&0xffff;
		if (idx != 0xffff)
		{
			const u32 seq = cmp>>16;
			decalAssert(idx < NELEM(m_decalIdCountFree));
			const u32 idx2 = m_decalIdCountFree[idx];
			decalAssert(idx2 < NELEM(m_decalIdCountFree) || idx2 == 0xffff);
			const u32 swap = ((seq+1)<<16)|idx2;
			const u32 mem = sysInterlockedCompareExchange(&m_decalIdSeqFree, swap, cmp);
			if (cmp == mem)
			{
				decalAssert(idx < NELEM(m_decalIdCountFree));
				CompileTimeAssert(NELEM(m_decalIdCountFree) == NELEM(m_decalIdAlive));
				m_decalIdCountFree[idx] = 0;
				return (int)(((idx+1)<<16) | (u32)(m_decalIdAlive[idx]));
			}
			cmp = mem;
		}
		else
		{
			break;
		}
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
//  FreeDecalId
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::FreeDecalId(int decalId)
{
	// Decrement the alive counter
	decalAssert(decalId);
	const u32 idx = ((u32)decalId-0x00010000)>>16;
	const u32 seq = (u32)decalId&0xffff;
	decalAssert(idx < NELEM(m_decalIdAlive));
	CompileTimeAssert(NELEM(m_decalIdAlive) == NELEM(m_decalIdCountFree));
	decalAssert(m_decalIdAlive[idx] == seq);

	// Increment sequence number so that decalId is no longer considerred alive
	m_decalIdAlive[idx] = (u16)(seq+1);

	// Add to free list
	u32 cmp = m_decalIdSeqFree;
	for (;;)
	{
		m_decalIdCountFree[idx] = (u16)cmp;
		const u32 swap = ((cmp+0x00010000)&0xffff0000)|idx;
		const u32 mem = sysInterlockedCompareExchange(&m_decalIdSeqFree, swap, cmp);
		if (cmp == mem)
		{
			break;
		}
		cmp = mem;
	}
}


///////////////////////////////////////////////////////////////////////////////
//  AddDecalIdInstance
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::AddDecalIdInstance(int decalId)
{
	// Decrement the alive counter
	decalAssert(decalId);
	const u32 idx = ((u32)decalId-0x00010000)>>16;
	ASSERT_ONLY(const u32 seq = (u32)decalId&0xffff;)
	decalAssert(idx < NELEM(m_decalIdAlive));
	CompileTimeAssert(NELEM(m_decalIdAlive) == NELEM(m_decalIdCountFree));
	decalAssert(m_decalIdAlive[idx] == seq);
	sysInterlockedIncrement_NoWrapping(m_decalIdCountFree+idx);
}


///////////////////////////////////////////////////////////////////////////////
//  RemoveDecalIdInstance
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::RemoveDecalIdInstance(int decalId)
{
	// Decrement the alive counter
	decalAssert(decalId);
	const u32 idx = ((u32)decalId-0x00010000)>>16;
	ASSERT_ONLY(const u32 seq = (u32)decalId&0xffff;)
	decalAssert(idx < NELEM(m_decalIdAlive));
	CompileTimeAssert(NELEM(m_decalIdAlive) == NELEM(m_decalIdCountFree));
	decalAssert(m_decalIdAlive[idx] == seq);
	const u16 deced = sysInterlockedDecrement_NoWrapping(m_decalIdCountFree+idx);

	// Alive counter decremented to zero ?
	if (!deced)
	{
		FreeDecalId(decalId);

		// Tell higher level code that the decal is dead
		m_decalRemovedCallback(decalId);
	}
}


///////////////////////////////////////////////////////////////////////////////
//  AddInstToBucket
///////////////////////////////////////////////////////////////////////////////

decalBucket* decalManagerAsync::AddInstToBucket(const decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, unsigned bucketLists, const decalSettings* settings, ScalarV_In posScale, Vec3V_In msOffsetInt, decalInst* pDecalInstLs, decalInst* pDecalInstEa, Mat34V_In wldMtx, s16 attachMatrixId, bool isGlassInst, bool isDamaged SPU_ONLY(, decalMiniIdxBufWriter* miniIdxBufWriter))
{
	// static insts only want added if there are active triangles in them
	if (settings->user.pipelineSettings.method==DECAL_METHOD_STATIC)
	{
		if (pDecalInstLs->GetNumTris()==0)
		{
			m_pools.ReturnInst(pDecalInstEa SPU_ONLY(, &m_thisEa->m_pools, DMATAG_BLOCKING));
			return NULL;
		}
	}

	// get a bucket to add the inst to
	decalBucket* pDecalBucket = GetBucket(bucketLists, 
										  taskDescEa, 
										  pDecalInstEa, 
										  posScale, 
										  msOffsetInt, 
										  pDecalInstLs->GetBoundCentreAndRadius(), 
										  settings->user.pipelineSettings.renderPass,	
										  settings->user.pipelineSettings.method,	
										  settings->user.bucketSettings.typeSettingsIndex, 
										  settings->user.bucketSettings.renderSettingsIndex,	
										  settings->user.bucketSettings.subType, 
										  settings->internal.attachType, 
										  wldMtx, 
										  attachMatrixId, 
										  settings->user.bucketSettings.pSmashGroup,
										  isGlassInst, 
										  isDamaged,
										  settings->internal.isPersistent,
										  settings->internal.interiorLocation,
										  settings->user.bucketSettings.isScripted);
	if (pDecalBucket)
	{
#		if __BANK
			m_debug.m_numInstsCreatedThisProjection++;
#		endif

		bool primary = settings->user.lodSettings.primaryInstFlag;
		if (!settings->user.lodSettings.copyPrimaryInstFlag)
		{
			// Note that taskDescLs can be NULL when directly creating a dynamic
			// decal via the ProcessAddInfos code path (not when doing a lod
			// reduction, that will not call this function).
			primary = taskDescLs ? (sysInterlockedIncrement_NoWrapping(taskDescLs->taskInstCounter)==1) : true;
		}
		pDecalInstLs->SetIsPrimaryInst(primary);
	}
	else
	{
		// No buckets available - return this inst to the pool.
		//
		// Note that we must flush and invalidate the miniIdxBufWriter when
		// running on the spu so that decalInst::ShutdownImmediate() doesn't
		// fetch a mib that is undefined.  decalMiniIdxBufWriter::Flush() does
		// not block on dma completion, but decalInst::ShutdownImmediate() does.
		//
#		if __SPU
			miniIdxBufWriter->Flush();
			miniIdxBufWriter->Invalidate();
#		endif
		ASSERT_ONLY(pDecalInstLs->MarkForDelete();)
		pDecalInstLs->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_DONT_RELEASE_DECAL_ID);
		m_pools.ReturnInst(pDecalInstEa SPU_ONLY(, &m_thisEa->m_pools, DMATAG_BLOCKING));
	}
	return pDecalBucket;
}


#if !__PS3

	///////////////////////////////////////////////////////////////////////////
	//  ProcessInstTask
	///////////////////////////////////////////////////////////////////////////

	/*static*/ void decalManagerAsync::ProcessInstTask(sysTaskParameters& p)
	{
		decalManagerAsync *const self = ms_instance;
		decalAsyncTaskDescBase *const td = (decalAsyncTaskDescBase*) p.Input.Data;
		self->ProcessInst(DECAL_DEBUG_NAME_ONLY(td->debugName,)
			td, td, td->typeSettings, td->renderSettings, td->pEntity, td->pPhInst,
			td->skelObjMtxs, td->skelBones, td->skelDrawableIdxs, td->skelInvJoints,
			td->renderToHierarchyLut);
	}

#endif // !__PS3


///////////////////////////////////////////////////////////////////////////////
//  CalcPosScale
///////////////////////////////////////////////////////////////////////////////

ScalarV_Out decalManagerAsync::CalcPosScale(ScalarV_In entityRadius) const
{
	using namespace Vec;

	// Apply a fudge factor to increase the radius a bit.  Should not really be
	// necissary if the radius is very accurate, but adds a safety margin in
	// case it is not.
	Vector_4V posScale;
	posScale = V4Scale(entityRadius.GetIntrin128(), V4VConstantSplat<FLOAT_TO_INT(1.125f)>());

	// Clamp position scale to the max position scale value.
	posScale = V4Min(posScale, GetMaxPositionScale().GetIntrin128());

	return ScalarV(posScale);
}


///////////////////////////////////////////////////////////////////////////////
//  CalcModelSpaceOffsetInt
///////////////////////////////////////////////////////////////////////////////

Vec3V_Out decalManagerAsync::CalcModelSpaceOffsetInt(const decalSettings *settings) const
{
	Vec3V msPos = settings->internal.vLclPosition;
	msPos *= GetModelSpaceOffsetUnitsInverse();
	return RoundToNearestInt(msPos);
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessInst
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::ProcessInst(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, const fwEntity* pEntity, phInst* pPhInst, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8 *skelDrawableIdxs, const Mat34V *skelInvJoints, DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut)
{
	using namespace Vec;

	decalMiniIdxBufWriter miniIdxBufWriter;

	// SPU code is compiled into multiple jobs each which support only one of
	// the following switch cases.  The reason for this is to save on local
	// store code space.
#if !__SPU
#	define SWITCH       switch (taskDescLs->taskType)
#	define CASE(TYPE)       case TYPE:
#	define BREAK            break;
#else
#	define SWITCH
#	define CASE(TYPE)
#	define BREAK
#endif

	// Load the entity radius from the task descriptor, then convert to next
	// largest power of two integer.
	ScalarV entityRadius = ScalarV(V4SplatX(V4LoadLeft(&taskDescLs->entityRadius)));
	const ScalarV posScale = CalcPosScale(entityRadius);
	const ScalarV invPosScale(V4Invert(posScale.GetIntrin128()));

	SWITCH
	{
#if DECAL_MANAGER_ENABLE_PROCESSBOUND
		CASE(DECAL_DO_PROCESSBOUND)
		{
			DECAL_ASSERT_ALREADY_LOCKED(pPhInst);
			const s16 childId = 0;
			const bool isBreakable = false;
			const bool isBrokenBreakable = false;
			ProcessBound(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pPhInst, pPhInst->GetArchetype()->GetBound(), pPhInst->GetMatrix(), childId, isBreakable, isBrokenBreakable, &miniIdxBufWriter);
			BREAK
		}
#endif

#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED
		CASE(DECAL_DO_PROCESSDRAWABLESNONSKINNED)
		{
			unsigned damagedDrawableMask = taskDescLs->damagedDrawableMask;
			const unsigned numDrawables = taskDescLs->numDrawables;
			decalAssert((numDrawables==1) ^ !!taskDescLs->skelDrawableIdxs);
			for (unsigned drawableIdx=0; drawableIdx<numDrawables; ++drawableIdx)
			{
				const rmcDrawable *const pDrawableEa = taskDescLs->drawablePtrs[drawableIdx];
				DECAL_ASSERT_ALREADY_LOCKED(pDrawableEa);
				BLOCKING_DMA_GET(rmcDrawable, pDrawable, pDrawableEa);
				ProcessDrawable(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pDrawable, drawableIdx, (bool)(damagedDrawableMask&1), skelObjMtxs, skelBones, skelDrawableIdxs, renderToHierarchyLut, &miniIdxBufWriter);
				damagedDrawableMask >>= 1;
			}
			BREAK
		}
#endif

#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED
		CASE(DECAL_DO_PROCESSDRAWABLESSKINNED)
		{
#			if USE_EDGE && __PPU
				// Doing a dynamic memory allocation here is a bit lame, but
				// this code path should only be used rarely, so we don't want
				// to waste the memory for a static allocation.  Cannot use the
				// stack, because the SPUs need to dma this in.  No need to
				// bother calling the ctors.
				sysMemAllocator *const sma = &sysMemAllocator::GetCurrent();
				decalSettings *const boneSettings = (decalSettings*)sma->TryAllocate(DECAL_MAX_BONES*sizeof(decalSettings), __alignof(decalSettings));
				if (Unlikely(!boneSettings))
				{
					decalSpamf("out of space for boneSettings temp allocation");
					return;
				}
#			endif

			unsigned damagedDrawableMask = taskDescLs->damagedDrawableMask;
			const unsigned numDrawables = taskDescLs->numDrawables;
			decalAssert((numDrawables==1) ^ !!taskDescLs->skelDrawableIdxs);
			for (unsigned drawableIdx=0; drawableIdx<numDrawables; ++drawableIdx)
			{
				const rmcDrawable *const pDrawableEa = taskDescLs->drawablePtrs[drawableIdx];
				DECAL_ASSERT_ALREADY_LOCKED(pDrawableEa);
				BLOCKING_DMA_GET(rmcDrawable, pDrawable, pDrawableEa);
				ProcessSkinnedDrawable(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pDrawable, drawableIdx, (bool)(damagedDrawableMask&1), skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, renderToHierarchyLut, &miniIdxBufWriter PPU_ONLY(, boneSettings));
				damagedDrawableMask >>= 1;
			}

#			if USE_EDGE && __PPU
				sma->Free(boneSettings);
#			endif

			BREAK
		}
#endif

#if DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS
		CASE(DECAL_DO_PROCESSBREAKABLEGLASS)
		{
			DECAL_ASSERT_ALREADY_LOCKED(pPhInst);
			fragInst *const pFragInst = static_cast<fragInst*>(pPhInst);
			ProcessBreakableGlass(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pFragInst, &miniIdxBufWriter);
			BREAK
		}
#endif

#if DECAL_MANAGER_ENABLE_PROCESSSMASHGROUP
		CASE(DECAL_DO_PROCESSSMASHGROUP)
		{
			ProcessSmashGroup(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, &miniIdxBufWriter);
			BREAK
		}
#endif

#if !__SPU
		CASE(NUM_DECAL_ASYNC_TASK_TYPES)
		{
			decalAssert(0);
			BREAK;
		}
#endif
	}

#undef BREAK
#undef CASE
#undef SWITCH
}


#if DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS

	///////////////////////////////////////////////////////////////////////////
	//  ProcessBreakableGlass
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessBreakableGlass(DECAL_DEBUG_NAME_ONLY(const char* debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, fragInst* pFragInst, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		const fragType* const pFragTypeEa = pFragInst->GetType();
		DECAL_ASSERT_ALREADY_LOCKED(pFragTypeEa);
		BLOCKING_DMA_GET(fragType, pFragType, pFragTypeEa);

		const fragInst::ePhysicsLOD lod = pFragInst->GetCurrentPhysicsLOD();
		const fragPhysicsLODGroup* const pLodGroupEa = pFragType->GetPhysicsLODGroup();
		DECAL_ASSERT_ALREADY_LOCKED(pLodGroupEa);
		BLOCKING_DMA_GET(fragPhysicsLODGroup, pLodGroup, pLodGroupEa);
		const fragPhysicsLOD* const pFragPhysicsEa = pLodGroup->GetLODByIndex(lod);
		DECAL_ASSERT_ALREADY_LOCKED(pFragPhysicsEa);
		BLOCKING_DMA_GET(fragPhysicsLOD, pFragPhysics, pFragPhysicsEa);

		const unsigned numChildren = pFragPhysics->GetNumChildren();
		if (Unlikely(!numChildren))
		{
			return;
		}

#		if !__SPU
			const fragCacheEntry *pFragCacheEntry = pFragInst->GetCacheEntry();
			DECAL_ASSERT_ALREADY_LOCKED(pFragCacheEntry);
#		else
			const fragCacheEntry *pFragCacheEntry = NULL;
			CompileTimeAssert(__alignof(fragCacheEntry) >= 16);
			char fragCacheEntryBuf[sizeof(fragCacheEntry)] ALIGNED(__alignof(fragCacheEntry)); // use char array here, as we don't want to call the ctor or dtor
			const fragCacheEntry *const pFragCacheEntryEa = pFragInst->GetCacheEntry();
			if (Likely(pFragCacheEntryEa))
			{
				sysDmaGet(fragCacheEntryBuf, (u32)pFragCacheEntryEa, sizeof(fragCacheEntryBuf), DMATAG_BLOCKING);
				pFragCacheEntry = (fragCacheEntry*)fragCacheEntryBuf;
			}
#		endif

		fragTypeChild** const ppChildrenEa = pFragPhysics->GetAllChildren();
		DECAL_ASSERT_ALREADY_LOCKED(ppChildrenEa);
		DMA_GET_ARRAY(fragTypeChild*, ppChildren, numChildren, ppChildrenEa, DMATAG_BLOCKING);

		const unsigned numGroups = pFragPhysics->GetNumChildGroups();
		const datOwner<fragTypeGroup>* const ppAllGroupsEa = pFragPhysics->GetAllGroups();
		DECAL_ASSERT_ALREADY_LOCKED(ppAllGroupsEa);
		BLOCKING_DMA_GET_ARRAY(datOwner<fragTypeGroup>, ppAllGroups, numGroups, ppAllGroupsEa);

		// If we have a fragCacheEntry, then get the associated
		// fragHierarchyInst ptr (struct is embedded in the fragCacheEntry).
		const fragHierarchyInst *pHierInst = NULL;
		if (Likely(pFragCacheEntry))
		{
			pHierInst = pFragCacheEntry->GetHierInst();
			decalAssertf((uptr)pFragCacheEntry<=(uptr)pHierInst && (uptr)pHierInst<(uptr)(pFragCacheEntry+1),
				"spu code expects the fragHierarchyInst to be embedded in the fragCacheEntry, and not require another dma get");
		}

		const phArchetype* const pArchetype = pFragInst->GetArchetype();
		DECAL_ASSERT_ALREADY_LOCKED(pArchetype);

		// If a single drawable is associated with a group of children then only the first child in
		// the group has a drawable with valid lod groups
		// therefore we need to get the first child in the group to get a valid drawable to process

		// Prime prefetchers before looping
		PipelinedDmaGetter<fragTypeChild, 4> childGetter(DMATAG_GET_FRAGTYPECHILD0);
		for (unsigned i=0; i<Min(3u,numChildren); ++i)
		{
			const fragTypeChild* const pChildEa = ppChildren[i];
			DECAL_ASSERT_ALREADY_LOCKED(pChildEa);
			childGetter.Prefetch(pChildEa);
		}
		PipelinedDmaGetter<fragTypeGroup, 3> typeGroupGetter(DMATAG_GET_FRAGTYPEGROUP0);
		for (unsigned i=0; i<Min(2u,numChildren); ++i)
		{
			const unsigned grpIdx = childGetter.GetFuture()->GetOwnerGroupPointerIndex();
			const fragTypeGroup* const pTypeGroupEa = ppAllGroups[grpIdx];
			DECAL_ASSERT_ALREADY_LOCKED(pTypeGroupEa);
			typeGroupGetter.Prefetch(pTypeGroupEa);
		}

		// Loop over children
		//
		// ppChildren is an array of fragTypeChild, indexed by child number.
		// For each of these fragTypeChild's, we grab the fragTypeGroup
		// (with typeGroupGetter), and the fragGroupInst if we have a
		// fragCacheEntry (with groupInstGetter).
		//
		// From the fragTypeGroup, we get the index of the first sibling of
		// the current child, then fetch its fragTypeChild.  The damaged or
		// undamaged drawable is then gotten from this sibling
		// fragTypeChild.
		//
		for (unsigned i=0; i<numChildren; ++i)
		{
			// Prefetch ahead
			if (Likely(i+1<numChildren))
			{
				if (Likely(i+2<numChildren))
				{
					const unsigned grpIdx = childGetter.GetFuture()->GetOwnerGroupPointerIndex();
					const fragTypeGroup* const pTypeGroupEa = ppAllGroups[grpIdx];
					DECAL_ASSERT_ALREADY_LOCKED(pTypeGroupEa);
					typeGroupGetter.Prefetch(pTypeGroupEa);
					if (Likely(i+3<numChildren))
					{
						const fragTypeChild* const pChildEa = ppChildren[i+3];
						DECAL_ASSERT_ALREADY_LOCKED(pChildEa);
						childGetter.Prefetch(pChildEa);
					}
				}
			}

			const fragTypeGroup *const pTypeGroup = typeGroupGetter.GetCurrent();
			if (pTypeGroup->GetMadeOfGlass())
			{
				if (taskDescLs->settings.user.pipelineSettings.applyToBreakables)
				{
					if (taskDescLs->settings.user.lodSettings.lodAttachMatrixId>-1 && taskDescLs->settings.user.lodSettings.lodAttachMatrixId!=(int)i)
					{
						continue;
					}

					// this is breakable glass
					phBoundComposite* pBoundComposite = static_cast<phBoundComposite*>(pArchetype->GetBound());
					DECAL_ASSERT_ALREADY_LOCKED(pBoundComposite);
					phBound* pChildBound = pBoundComposite->GetBound(i);
					if (pChildBound)
					{
						// it's unbroken
						DECAL_ASSERT_ALREADY_LOCKED(pChildBound);
						Mat34V childBoundMtx = pBoundComposite->GetCurrentMatrix(i);
						Transform(childBoundMtx, pFragInst->GetMatrix(), childBoundMtx);
						ProcessBound(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pFragInst, pChildBound, childBoundMtx, (s16)i, true, false, miniIdxBufWriter);
					}
				}
			}
		}

		// handle breakable glass that is broken
		if (taskDescLs->settings.user.pipelineSettings.applyToBreakables)
		{
			if (pFragCacheEntry)
			{
				if (pHierInst)
				{
					DECAL_ASSERT_ALREADY_LOCKED(pHierInst->glassInfos);
					for (int glassIndex=0; glassIndex!=pHierInst->numGlassInfos; glassIndex++)
					{
						bgGlassHandle handle = pHierInst->glassInfos[glassIndex].handle;
						if (handle != bgGlassHandle_Invalid)
						{
							if (taskDescLs->settings.user.lodSettings.lodAttachMatrixId>-1 && taskDescLs->settings.user.lodSettings.lodAttachMatrixId!=handle)
							{
								continue;
							}

							taskDescLs->settings.user.pipelineSettings.colnComponentId = handle;

							static bool useProcessBreakable = true;
							if (useProcessBreakable)
							{
								ProcessBreakable(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, &bgGlassManager::GetGlassBreakable(handle), miniIdxBufWriter);
							}
							else
							{
								// RSNE - DF:
								// Place the decal as though the glass is not broken (later remove unneeded parts with stencil)
								// This is better for performance as generating the decal from a box bound is faster and generates less triangles.
								// Another benefit is that we don't have to regenerate the decals when the glass breaks or when it re-breaks again
								phInst* pInst = bgGlassManager::GetGlassPhysicsInst(handle);
								DECAL_ASSERT_ALREADY_LOCKED(pInst);
								const phBoundComposite* pBoundComposite = static_cast<const phBoundComposite*>(pArchetype->GetBound());
								decalAssertf(pBoundComposite->GetNumBounds() == 2, "Breakable glass should have two bounds (a BVH and a Box");
								phBound* pChildBound = pBoundComposite->GetBound(1); // Get the box bound
								if (pChildBound)
								{
									DECAL_ASSERT_ALREADY_LOCKED(pChildBound);
									decalAssertf(pChildBound->GetType()==phBound::BOX, "We expect the second child to be a box bound (%s)", pInst->GetArchetype()->GetFilename());
									Mat34V newBoundWldMtx;
									Transform(newBoundWldMtx, pInst->GetMatrix(), pBoundComposite->GetCurrentMatrix(1));
									ProcessBound(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pInst, pChildBound, newBoundWldMtx, handle, true, true, miniIdxBufWriter);
								}
							}
						}
					}
				}
			}
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS


#if !__SPU

 	///////////////////////////////////////////////////////////////////////////
 	//  IndexBufferItor (non-SPU)
 	///////////////////////////////////////////////////////////////////////////

 	class IndexBufferItor
 	{
 	public:

 		inline IndexBufferItor(const u16 *ea, u32 count, u32 firstDmaTag)
 			: ptr(ea)
 		{
 			(void) count;
 			(void) firstDmaTag;
 		}

 		inline void FinishPrefetch() {}

 		inline u16 Get() { return *ptr++; }

 	private:

 		const u16 *ptr;
 	};


#else // __SPU


 	///////////////////////////////////////////////////////////////////////////
 	//  IndexBufferItor (SPU)
 	///////////////////////////////////////////////////////////////////////////

 	class IndexBufferItor
 	{
 	public:

 		inline IndexBufferItor(const u16 *ea, u32 count, u32 firstDmaTag)
 		    : bufIdx(0)
 			, bufPos(((u32)ea&127)>>1)
 			, currEa((u32)ea&~127)
 			, endEa((((u32)ea+(count<<1))+127)&~127)
 			, dmaTag(firstDmaTag)
 		{
 			sysDmaGet(buf[0], currEa, 128, firstDmaTag);
 			currEa += 128;
 			if (Likely(currEa<endEa))
 			{
 				sysDmaGet(buf[1], currEa, 128, firstDmaTag+1);
 				currEa += 128;
 			}
 		}

 		inline ~IndexBufferItor()
 		{
 			sysDmaWaitTagStatusAll(3<<dmaTag);
 		}

 		inline void FinishPrefetch()
 		{
 			sysDmaWaitTagStatusAll(1<<dmaTag);
 		}

 		inline u16 Get()
 		{
 			const u16 ret = buf[bufIdx][bufPos];
 			if (Unlikely(++bufPos == 64))
 			{
 				if (Likely(currEa<endEa))
 				{
 					sysDmaGet(buf[bufIdx], currEa, 128, dmaTag+bufIdx);
 					currEa += 128;
 				}
 				bufIdx ^= 1;
 				bufPos = 0;
 				sysDmaWaitTagStatusAll(1<<(dmaTag+bufIdx));
 			}
 			return ret;
 		}


 	private:

 		u16 buf[2][64] ALIGNED(128);
 		u32 bufIdx;
 		u32 bufPos;
 		u32 currEa;
 		u32 endEa;
 		u32 dmaTag;
 	};


#endif // !__SPU


#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED || DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

	///////////////////////////////////////////////////////////////////////////
	//  UnpackSkelObjMtx
	///////////////////////////////////////////////////////////////////////////

	static Mat34V_Out UnpackSkelObjMtx(const Vec4V *skelObjMtxs, decalBoneTaskIdx boneIdx)
	{
		const Vector4 *const ptr = (Vector4*)skelObjMtxs + boneIdx*3;
		Matrix44 boneObjMtxT;
		boneObjMtxT.a = ptr[0];
		boneObjMtxT.b = ptr[1];
		boneObjMtxT.c = ptr[2];
		boneObjMtxT.d = VEC4V_TO_VECTOR4(Vec4V(V_ZERO_WONE));

		Mat34V boneObjMtx;
		boneObjMtxT.Transpose();
		boneObjMtx.SetCol0(RCC_VEC3V(boneObjMtxT.a));
		boneObjMtx.SetCol1(RCC_VEC3V(boneObjMtxT.b));
		boneObjMtx.SetCol2(RCC_VEC3V(boneObjMtxT.c));
		boneObjMtx.SetCol3(RCC_VEC3V(boneObjMtxT.d));
		return boneObjMtx;
	}


	///////////////////////////////////////////////////////////////////////////
	//  AllowVertexBufferVramLocks
	///////////////////////////////////////////////////////////////////////////

	class AllowVertexBufferVramLocks
	{
	public:

		inline AllowVertexBufferVramLocks()
			PS3_ONLY(BANK_ONLY(: m_Prev(g_AllowVertexBufferVramLocks)))
		{
			PS3_ONLY(BANK_ONLY(g_AllowVertexBufferVramLocks = 1;))
		}

		inline ~AllowVertexBufferVramLocks()
		{
			PS3_ONLY(BANK_ONLY(g_AllowVertexBufferVramLocks = m_Prev;))
		}

	private:

		PS3_ONLY(BANK_ONLY(u32 m_Prev;))
	};

#endif // DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED || DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED


#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED

	///////////////////////////////////////////////////////////////////////////
	//  ProcessGeometry
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessGeometry(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, const u16 *pIndexPtrEa, unsigned idxCount, const void *pVertexPtrEa, unsigned vertexStride, const grcFvf *pVertexFormatLs, bool isGlassShader, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		DECAL_DEBUG_NAME_ONLY((void)debugName;)

#		if __PS3
			// Hammering on RSX local memory can cause audio failures (MP3
			// B*425905).  For now, just disable decals unless we can come up
			// with a better solution.
			if (((u32)pIndexPtrEa-RSX_FB_BASE_ADDR<0x10000000 || (u32)pVertexPtrEa-RSX_FB_BASE_ADDR<0x10000000) BANK_ONLY(&& !m_debug.m_allowRsxLocalReads))
			{
				decalSpamf("decalManagerAsync::ProcessGeometry ignoring RSX local buffer \"%s\"\n", debugName);
				return;
			}
#		endif

		// get the index buffer
		decalAssert(idxCount);
		decalAssert(pIndexPtrEa);
		IndexBufferItor idxItor(pIndexPtrEa, idxCount, DMATAG_GET_IDX0);

		// get the vertex buffer
		const s32  vbOffsetNormal = pVertexFormatLs->GetOffsetNoAssert(grcFvf::grcfcNormal);
		const bool vbHasNormals = pVertexFormatLs->IsChannelActive(grcFvf::grcfcNormal);
#		if (RSG_PC || RSG_DURANGO || RSG_ORBIS)
			grcFvf::grcDataSize NormalSize = grcFvf::grcdsFloat3;
#			if DECAL_MANAGER_ALLOW_PACKED_NORMALS
				if (vbHasNormals)
				{
					NormalSize = pVertexFormatLs->GetDataSizeType(grcFvf::grcfcNormal);
				}
#			endif
#		endif

		const bool initialPrefetch = true;
		decalVertexCache vtxCache(pVertexPtrEa, vertexStride, DMATAG_GET_VTXBUF0, initialPrefetch);
		const unsigned numTris = idxCount/3;

		// vehicle damage should only be applied to skinned geometry
		const ScalarV vehDmgScale(V_ZERO);

		idxItor.FinishPrefetch();
		for (unsigned tri=0; tri<numTris; ++tri)
		{
			const unsigned index1 = idxItor.Get();
			const unsigned index2 = idxItor.Get();
			const unsigned index3 = idxItor.Get();
			vtxCache.Prefetch(index1);
			vtxCache.Prefetch(index2);
			vtxCache.Prefetch(index3);

			// Skip degenerate triangles
			if (Unlikely(index1==index2 || index2==index3 || index1==index3))
			{
				continue;
			}

			const char *const ptr1 = (char*)vtxCache.Get(index1);
			const char *const ptr2 = (char*)vtxCache.Get(index2);
			const char *const ptr3 = (char*)vtxCache.Get(index3);

			const Vec3V vPos1 = GetVBPosition(ptr1);
			const Vec3V vPos2 = GetVBPosition(ptr2);
			const Vec3V vPos3 = GetVBPosition(ptr3);

			Vec3V vNrm1, vNrm2, vNrm3;
			if (vbHasNormals)
			{
#			  if (RSG_PC || RSG_DURANGO || RSG_ORBIS)
				if (NormalSize != grcFvf::grcdsPackedNormal)
				{
					vNrm1 = GetVBNormalUncompressed(ptr1+vbOffsetNormal);
					vNrm2 = GetVBNormalUncompressed(ptr2+vbOffsetNormal);
					vNrm3 = GetVBNormalUncompressed(ptr3+vbOffsetNormal);
				}
				else
#			  endif
				{
					vNrm1 = GetVBNormalCompressed(ptr1+vbOffsetNormal);
					vNrm2 = GetVBNormalCompressed(ptr2+vbOffsetNormal);
					vNrm3 = GetVBNormalCompressed(ptr3+vbOffsetNormal);
				}
			}
			else
			{
				vNrm1 = Normalize(Cross(vPos3-vPos1, vPos2-vPos1));
				vNrm2 = vNrm1;
				vNrm3 = vNrm1;
			}

			ProcessTri(&taskDescLs->settings, typeSettings, renderSettings,
				posScale, invPosScale, msOffsetFlt, vPos1, vPos2, vPos3, vNrm1, vNrm2, vNrm3,
				vehDmgScale, vehDmgScale, vehDmgScale, isGlassShader, miniIdxBufWriter);
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED

#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

	///////////////////////////////////////////////////////////////////////////
	//  ProcessSkinnedGeometry
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessSkinnedGeometry(DECAL_DEBUG_NAME_ONLY(const char *debugName,) const decalSettings* boneSettings, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const Vec3V *msOffsetFlt, const u16* pIndexPtrEa, unsigned idxCount, const void* pVertexPtrEa, unsigned vertexStride, const grcFvf* pVertexFormatLs, DECAL_BONEREMAPLUT(Vertex,Loop) vertexToLoopLut, bool isGlassShader, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		DECAL_DEBUG_NAME_ONLY((void) debugName;)

#		if __PS3
			// Hammering on RSX local memory can cause audio failures (MP3
			// B*425905).  For now, just disable decals unless we can come up
			// with a better solution.
			if (((u32)pIndexPtrEa-RSX_FB_BASE_ADDR<0x10000000 || (u32)pVertexPtrEa-RSX_FB_BASE_ADDR<0x10000000) BANK_ONLY(&& !m_debug.m_allowRsxLocalReads))
			{
				decalSpamf("decalManagerAsync::ProcessSkinnedGeometry ignoring RSX local buffer \"%s\"\n", debugName);
				return;
			}
#		endif

		// get the index buffer
		decalAssert(idxCount);
		decalAssert(pIndexPtrEa);
		IndexBufferItor idxItor(pIndexPtrEa, idxCount, DMATAG_GET_IDX0);

		// get the vertex buffer
		const Vec3V vecNormalUp(V_Z_AXIS_WZERO);
		const ScalarV onef(V_ONE);
		const unsigned vbOffsetNormal = pVertexFormatLs->GetOffsetNoAssert(grcFvf::grcfcNormal);
#		if (RSG_PC || RSG_DURANGO || RSG_ORBIS)
			grcFvf::grcDataSize NormalSize = grcFvf::grcdsFloat3;
#		endif
		const unsigned vbOffsetBinding = pVertexFormatLs->IsChannelActive(grcFvf::grcfcBinding) ? pVertexFormatLs->GetOffset(grcFvf::grcfcBinding) : 0;
		const bool vbHasNormals = pVertexFormatLs->IsChannelActive(grcFvf::grcfcNormal);
		if (Likely(vbHasNormals))
		{
#		if !(RSG_PC || RSG_DURANGO || RSG_ORBIS)
			if (pVertexFormatLs->GetDataSizeType(grcFvf::grcfcNormal) != grcFvf::grcdsPackedNormal)
#		elif !DECAL_MANAGER_ALLOW_PACKED_NORMALS
			decalAssertf(pVertexFormatLs->GetDataSizeType(grcFvf::grcfcNormal) != grcFvf::grcdsPackedNormal, "decalManagerAsync::ProcessSkinnedGeometry()...Allow packed normals on WIN32PC!");
			if (pVertexFormatLs->GetDataSizeType(grcFvf::grcfcNormal) != grcFvf::grcdsFloat3)
#		else
			NormalSize = pVertexFormatLs->GetDataSizeType(grcFvf::grcfcNormal);
			if ((NormalSize != grcFvf::grcdsFloat3) && (NormalSize != grcFvf::grcdsPackedNormal))
#		endif
			{
				return;
			}
		}
		const bool vbHasCpv = pVertexFormatLs->IsChannelActive(grcFvf::grcfcDiffuse);
		const unsigned vbOffsetCpv = vbHasCpv ? pVertexFormatLs->GetOffset(grcFvf::grcfcDiffuse) : 0;

		const bool initialPrefetch = true;
		decalVertexCache vtxCache(pVertexPtrEa, vertexStride, DMATAG_GET_VTXBUF0, initialPrefetch);
		const unsigned numTris = idxCount/3;

		idxItor.FinishPrefetch();
		for (unsigned tri=0; tri<numTris; ++tri)
		{
			const unsigned index1 = idxItor.Get();
			const unsigned index2 = idxItor.Get();
			const unsigned index3 = idxItor.Get();
			vtxCache.Prefetch(index1);
			vtxCache.Prefetch(index2);
			vtxCache.Prefetch(index3);

			// Skip degenerate triangles
			if (Unlikely(index1==index2 || index2==index3 || index1==index3))
			{
				continue;
			}

			const char *const ptr1 = (char*)vtxCache.Get(index1);
			const char *const ptr2 = (char*)vtxCache.Get(index2);
			const char *const ptr3 = (char*)vtxCache.Get(index3);

			// It is unlikely but possible that the bone for each vertex is
			// different.  Decals cannot work correctly with this case, so skip
			// the triangle.
			const decalBoneVertexIdx boneVertexIdx = GetVBBlendIndex(ptr1+vbOffsetBinding);
			if (Unlikely(boneVertexIdx!=GetVBBlendIndex(ptr2+vbOffsetBinding)
				|| boneVertexIdx!=GetVBBlendIndex(ptr3+vbOffsetBinding)))
			{
				continue;
			}

			artAssertf(boneVertexIdx < DECAL_MAX_BONES, "\"%s\" bad bone index %d", debugName, boneVertexIdx.Val());
			const decalBoneLoopIdx boneLoopIdx = vertexToLoopLut[boneVertexIdx];

			// Skip triangle if attached to a bone we are not processing
			if (boneLoopIdx == 0xff)
			{
				continue;
			}

			const Vec3V vPos1 = GetVBPosition(ptr1);
			const Vec3V vPos2 = GetVBPosition(ptr2);
			const Vec3V vPos3 = GetVBPosition(ptr3);

			Vec3V vNrm1, vNrm2, vNrm3;
			if (vbHasNormals)
			{
#			  if (RSG_PC || RSG_DURANGO || RSG_ORBIS)
				if (NormalSize != grcFvf::grcdsPackedNormal)
				{
					vNrm1 = GetVBNormalUncompressed(ptr1+vbOffsetNormal);
					vNrm2 = GetVBNormalUncompressed(ptr2+vbOffsetNormal);
					vNrm3 = GetVBNormalUncompressed(ptr3+vbOffsetNormal);
				}
				else
#			  endif
				{
					vNrm1 = GetVBNormalCompressed(ptr1+vbOffsetNormal);
					vNrm2 = GetVBNormalCompressed(ptr2+vbOffsetNormal);
					vNrm3 = GetVBNormalCompressed(ptr3+vbOffsetNormal);
				}
			}
			else
			{
				vNrm1 = Normalize(Cross(vPos3-vPos1, vPos2-vPos1));
				vNrm2 = vNrm1;
				vNrm3 = vNrm1;
			}

			ScalarV vehDmgScale1 = onef;
			ScalarV vehDmgScale2 = onef;
			ScalarV vehDmgScale3 = onef;
			if (vbHasCpv)
			{
				vehDmgScale1 = GetVBDamageScale(ptr1+vbOffsetCpv);
				vehDmgScale2 = GetVBDamageScale(ptr2+vbOffsetCpv);
				vehDmgScale3 = GetVBDamageScale(ptr3+vbOffsetCpv);
			}

			ProcessTri(boneSettings+boneLoopIdx.Val(), typeSettings, renderSettings,
				posScale, invPosScale, msOffsetFlt[boneLoopIdx.Val()],
				vPos1, vPos2, vPos3, vNrm1, vNrm2, vNrm3,
				vehDmgScale1, vehDmgScale2, vehDmgScale3, isGlassShader, miniIdxBufWriter);
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

#if __SPU && (DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED || DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED)

	///////////////////////////////////////////////////////////////////////
	//  FilterVertexAttributes
	///////////////////////////////////////////////////////////////////////

	static void FilterVertexAttributes(EdgeGeomVertexStreamDescription *inputStreamDesc)
	{
		const u32 wantedAttribMask =
			  (1<<EDGE_GEOM_ATTRIBUTE_ID_POSITION)
			| (1<<EDGE_GEOM_ATTRIBUTE_ID_NORMAL)
			| (1<<EDGE_GEOM_ATTRIBUTE_ID_COLOR);
		const unsigned numAttributes = inputStreamDesc->numAttributes;
		unsigned keepAttribIdx = 0;
		for (unsigned attribIdx=0; attribIdx<numAttributes; ++attribIdx)
		{
			if (wantedAttribMask & (1<<inputStreamDesc->blocks[attribIdx].attributeBlock.edgeAttributeId))
			{
				sysMemCpy(&inputStreamDesc->blocks[keepAttribIdx].attributeBlock, &inputStreamDesc->blocks[attribIdx].attributeBlock, sizeof(inputStreamDesc->blocks[keepAttribIdx].attributeBlock));
				++keepAttribIdx;
			}
		}
		inputStreamDesc->numAttributes = keepAttribIdx;
	}


	///////////////////////////////////////////////////////////////////////
	//  ProcessEdgeSegment
	///////////////////////////////////////////////////////////////////////

	// __alignof__(void) is invalid, so use these helpers instead
#	define ALIGNOF(TYPE) (AlignOfHelper<TYPE>::value)
	template<class T> struct AlignOfHelper
	{
		enum { value = __alignof__(T) };
	};
	template<> struct AlignOfHelper<void>
	{
		enum { value = 1 };
	};

	template<bool SKINNED>
	__attribute__((__noinline__))
	void decalManagerAsync::ProcessEdgeSegment(DECAL_DEBUG_NAME_ONLY(const char *debugName,) const decalSettings *settings, unsigned ASSERT_ONLY(numBoneSettings), const decalTypeSettings *typeSettings, const decalRenderSettings *renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const Vec3V *msOffsetFlt, EdgeGeomPpuConfigInfo *ppuConf, DECAL_BONEREMAPLUT(Edge,Loop) edgeToLoopLut, bool isGlass, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		using namespace Vec;

		decalAssert(numBoneSettings <= MAX_BONES_PER_LOOP);

		EdgeGeomSpuContext geomCtx;

		const sysScratchScope scopedResetScratch;
		char *const ioBuf = (char*)sysScratchTryAlloc(DECALASYNC_EDGETEMPSIZE);
		if (Unlikely(!ioBuf))
		{
			decalSpamf("Cannot allocate edge buffer space in decalManagerAsync::ProcessEdgeSegment, decals will be missing");
			return;
		}
		CompileTimeAssert((DECALASYNC_EDGEIOSIZE & 15) == 0);
		char *ioBufEnd = ioBuf + DECALASYNC_EDGEIOSIZE;
		char *const scratchBuf = ioBufEnd;
		char *ioPtr = ioBuf;

#		define CHECK_IO_SPACE()                                                    \
			do                                                                     \
			{                                                                      \
				if (Unlikely(ioPtr > ioBufEnd))                                    \
				{                                                                  \
					decalWarningf("Out of io buffer space in "                     \
						"decalManagerAsync::ProcessEdgeSegment(), need at least "  \
						"0x%08x more, decals will be missing",                     \
						(u32)ptrdiff_t_to_int(ioPtr - ioBufEnd));                  \
					return;                                                        \
				}                                                                  \
			}                                                                      \
			while (0)

#		define DMAGET_EDGEDATA(TYPE, NAME, EA, SIZE)                               \
			TYPE *NAME = NULL;                                                     \
			do                                                                     \
			{                                                                      \
				const u32 dmaSize = (SIZE);                                        \
				if (Likely(dmaSize))                                               \
				{                                                                  \
					NAME = (TYPE*)(                                                \
						((u32)ioPtr + ALIGNOF(TYPE) - 1) & -ALIGNOF(TYPE));        \
					ioPtr += dmaSize;                                              \
					CHECK_IO_SPACE();                                              \
					sysDmaLargeGet(NAME, (u64)(u32)static_cast<const void*>(EA),   \
						dmaSize, DMATAG_BLOCKING);                                 \
				}                                                                  \
			}                                                                      \
			while (0)

		DMAGET_EDGEDATA(EdgeGeomVertexStreamDescription, outputStreamDesc, ppuConf->spuOutputStreamDesc, ppuConf->spuOutputStreamDescSize);
		DMAGET_EDGEDATA(void, cmpIndices, ppuConf->indexes, ppuConf->indexesSizes[0] + ppuConf->indexesSizes[1]);
		const unsigned skinIndicesAndWeightsSize = ppuConf->skinIndexesAndWeightsSizes[0] + ppuConf->skinIndexesAndWeightsSizes[1];
		DMAGET_EDGEDATA(void, skinIndicesAndWeights, ppuConf->skinIndexesAndWeights, skinIndicesAndWeightsSize);
		DMAGET_EDGEDATA(void, verticesA, ppuConf->spuVertexes[0], ppuConf->spuVertexesSizes[0] + ppuConf->spuVertexesSizes[1] + ppuConf->spuVertexesSizes[2]);
		DMAGET_EDGEDATA(void, verticesB, ppuConf->spuVertexes[1], ppuConf->spuVertexesSizes[3] + ppuConf->spuVertexesSizes[4] + ppuConf->spuVertexesSizes[5]);
		DMAGET_EDGEDATA(void, fixedOffsetsA, ppuConf->fixedOffsets[0], ppuConf->fixedOffsetsSize[0]);
		DMAGET_EDGEDATA(void, fixedOffsetsB, ppuConf->fixedOffsets[1], ppuConf->fixedOffsetsSize[1]);
		DMAGET_EDGEDATA(EdgeGeomVertexStreamDescription, inputStreamDescA, ppuConf->spuInputStreamDescs[0], ppuConf->spuInputStreamDescSizes[0]);
		DMAGET_EDGEDATA(EdgeGeomVertexStreamDescription, inputStreamDescB, ppuConf->spuInputStreamDescs[1], ppuConf->spuInputStreamDescSizes[1]);

#		undef DMAGET_EDGEDATA

		sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);

		// Store another copy of the skin related data before the index
		// decompression overwrites it.  Put it at the end of the io buffer, and
		// reduce the io buffer size that is passed to Edge.
		const unsigned skinIndicesAndWeightsSize16 = (skinIndicesAndWeightsSize + 15) & ~15;
		ioBufEnd = (char*)(ioBufEnd - skinIndicesAndWeightsSize16);
		CHECK_IO_SPACE();
		u8 *skinIndicesAndWeightsCopy = (u8*)ioBufEnd;
		memcpy(skinIndicesAndWeightsCopy, skinIndicesAndWeights, skinIndicesAndWeightsSize16);

#       undef CHECK_IO_SPACE

		// Commented out, as we really don't have code space for this error checking.
// #		if __ASSERT
// 			const u32 errCode = edgeGeomValidateBufferOrder(
// 				outputStreamDesc,
// 				cmpIndices,
// 				NULL,   // pSkinMatrices
// 				skinIndicesAndWeights,
// 				verticesA,
// 				verticesB,
// 				NULL,   // pViewportInfo
// 				NULL,   // pLocalToWorld
// 				&ppuConf->spuConfigInfo,
// 				fixedOffsetsA,
// 				fixedOffsetsB,
// 				inputStreamDescA,
// 				inputStreamDescB);
// 			// We ignore EDGE_GEOM_VALIDATE_ERROR_INDEXES here since it will
// 			// fire for skinned geometry as we don't bother dmaing in the
// 			// matrices.  This is not a real issue in this case, since we
// 			// don't actually want to do the skinning.  Also don't really
// 			// care about EDGE_GEOM_VALIDATE_WARNING_SPU_CONFIG_INFO.
// 			if (Unlikely(errCode&~(EDGE_GEOM_VALIDATE_ERROR_INDEXES|EDGE_GEOM_VALIDATE_WARNING_SPU_CONFIG_INFO)))
// 			{
// 				decalDisplayf("Edge buffer order incorrect; errCode = 0x%08X\n", errCode);
// 				__debugbreak();
// 			}
// #		endif

		// Filter out vertex attributes we don't care about.  This saves some local store space.
		EdgeGeomSpuConfigInfo *const spuConf = &ppuConf->spuConfigInfo;
		decalAssert(inputStreamDescA);
		const unsigned prevInputTableCount = inputStreamDescA->numAttributes + (inputStreamDescB?inputStreamDescB->numAttributes:0);
		decalAssert((spuConf->flagsAndUniformTableCount&15)+1u >= prevInputTableCount);
		FilterVertexAttributes(inputStreamDescA);
		if (inputStreamDescB)
		{
			FilterVertexAttributes(inputStreamDescB);
		}
		const unsigned updatedInputTableCount = inputStreamDescA->numAttributes + (inputStreamDescB?inputStreamDescB->numAttributes:0);
		decalAssert(prevInputTableCount >= updatedInputTableCount);
		spuConf->flagsAndUniformTableCount -= prevInputTableCount - updatedInputTableCount;

		EdgeGeomCustomVertexFormatInfo customFormatInfo;
		sysMemSet(&customFormatInfo, 0, sizeof(customFormatInfo));
		customFormatInfo.inputStreamDescA = inputStreamDescA;
		customFormatInfo.inputStreamDescB = inputStreamDescB;
// 		customFormatInfo.outputStreamDesc = NULL;

		// Using a custom modification to Edge that will return false if it
		// runs out of local store, rather than asserting/crashing
		if (Unlikely(!edgeGeomInitialize(
			&geomCtx,
			spuConf,
			scratchBuf,
			DECALASYNC_EDGESCRATCHSIZE,
			ioBuf,
			DECALASYNC_EDGEIOSIZE-skinIndicesAndWeightsSize16,
			DMATAG_EDGE,
			NULL,           // inViewportInfo
			NULL,           // inLocalToWorldMatrix
			&customFormatInfo,
			NULL,           // customTransformInfo
			0)))            // gcmControlEa
		{
			decalWarningf("Edge out of space, decals will be missing");
			return;
		}

		edgeGeomDecompressVertexes(&geomCtx, verticesA, fixedOffsetsA, verticesB, fixedOffsetsB);

		edgeGeomDecompressIndexes(&geomCtx, cmpIndices); // trashes the boneIndexes and weights, and maybe even verticesA

		const u16 *const indices = edgeGeomGetIndexTable(&geomCtx);
		const Vector_4V *const positions = (Vector_4V*)edgeGeomGetPositionUniformTable(&geomCtx);
		const Vector_4V *const normals   = (Vector_4V*)edgeGeomGetNormalUniformTable(&geomCtx);
		const Vector_4V *const diffuse   = (Vector_4V*)edgeGeomGetUniformTableByAttribute(&geomCtx, EDGE_GEOM_ATTRIBUTE_ID_COLOR);
		const unsigned numIndices = ppuConf->spuConfigInfo.numIndexes;

		// Check if we have normal and diffuse vertex arrays from Edge.
		// There may still be in RSX local, but we don't have access to
		// them.
		const qword shufAAAA = si_ila(0x10203);
		qword hasNormals = si_from_ptr(normals);
		hasNormals = si_shufb(hasNormals, hasNormals, shufAAAA);
		hasNormals = si_clgti(hasNormals, 0);
		qword hasDiffuse = si_from_ptr(diffuse);
		hasDiffuse = si_shufb(hasDiffuse, hasDiffuse, shufAAAA);
		hasDiffuse = si_clgti(hasDiffuse, 0);

		const Vector_4V onef = V4VConstant(V_ONE);

		if (SKINNED)
		{
			// Check for single bone skinning
			const unsigned skinningFlavor = ppuConf->spuConfigInfo.indexesFlavorAndSkinningFlavor & 0x0F;
			decalAssert(skinningFlavor != EDGE_GEOM_SKIN_NONE);
			const bool useSingleBoneSkinning =
				skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_NO_SCALING ||
				skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_UNIFORM_SCALING ||
				skinningFlavor == EDGE_GEOM_SKIN_SINGLE_BONE_NON_UNIFORM_SCALING;
			const unsigned indexShl = useSingleBoneSkinning ? 0 : 3;

			// For single bone skinning, skinIndicesAndWeightsCopy is an array
			// of byte indices.  For multiple bone skinning, the format is
			//      byte weight0, index0, weight1, index1, weight2, index2, weight3, index3
			// For multiple bone skinning we want to grab index1, so offset the
			// array pointer by one.
			skinIndicesAndWeightsCopy += !useSingleBoneSkinning;

			for (unsigned ii=0; ii<numIndices; ii+=3)
			{
				const unsigned index1 = indices[ii+0];
				const unsigned index2 = indices[ii+1];
				const unsigned index3 = indices[ii+2];
				if (Likely(index1!=index2 && index2!=index3 && index1!=index3))
				{
					// Ignore cloth.  Cloth has an index of 0xff stored in the vertex.
					const decalBoneEdgeIdx edgeBoneIdx(skinIndicesAndWeightsCopy[index1<<indexShl]);
					if (Unlikely(edgeBoneIdx == 0xff))
					{
						continue;
					}

					// Ignore triangles that have different bones for each
					// vertex, as decals cannot handle this.
					if (Unlikely(edgeBoneIdx!=skinIndicesAndWeightsCopy[index2<<indexShl]
						|| edgeBoneIdx!=skinIndicesAndWeightsCopy[index3<<indexShl]))
					{
						continue;
					}

					// Convert to an index in the bone settings array.  If it
					// maps to 0xff, then ignore, we are not processing this
					// bone this loop iteration.
					const decalBoneLoopIdx boneSettingsIdx = edgeToLoopLut[edgeBoneIdx];
					if (Unlikely(boneSettingsIdx == 0xff))
					{
						continue;
					}

					const Vector_4V pos1 = positions[index1];
					const Vector_4V pos2 = positions[index2];
					const Vector_4V pos3 = positions[index3];
					const Vector_4V cross(V3Cross(V4Subtract(pos2,pos1), V4Subtract(pos3,pos1)));
					const Vector_4V faceNrm(V3Normalize(cross));
					const Vec3V nrm1(V4SelectFT((Vector_4V)hasNormals, faceNrm, normals[index1]));
					const Vec3V nrm2(V4SelectFT((Vector_4V)hasNormals, faceNrm, normals[index2]));
					const Vec3V nrm3(V4SelectFT((Vector_4V)hasNormals, faceNrm, normals[index3]));
					const Vector_4V vds1 = V4SelectFT((Vector_4V)hasDiffuse, onef, V4SplatY(diffuse[index1]));
					const Vector_4V vds2 = V4SelectFT((Vector_4V)hasDiffuse, onef, V4SplatY(diffuse[index2]));
					const Vector_4V vds3 = V4SelectFT((Vector_4V)hasDiffuse, onef, V4SplatY(diffuse[index3]));
					decalAssert(boneSettingsIdx < numBoneSettings);
					ProcessTri(settings+boneSettingsIdx.Val(), typeSettings, renderSettings,
					   posScale, invPosScale, msOffsetFlt[boneSettingsIdx.Val()],
					   Vec3V(pos1), Vec3V(pos2), Vec3V(pos3), nrm1, nrm2, nrm3,
					   ScalarV(vds1), ScalarV(vds2), ScalarV(vds3), isGlass, miniIdxBufWriter);
				}
			}
		}
		else
		{
			for(unsigned ii=0; ii<numIndices; ii+=3)
			{
				const unsigned index1 = indices[ii+0];
				const unsigned index2 = indices[ii+1];
				const unsigned index3 = indices[ii+2];
				if(Likely(index1!=index2 && index2!=index3 && index1!=index3))
				{
					const Vector_4V pos1 = positions[index1];
					const Vector_4V pos2 = positions[index2];
					const Vector_4V pos3 = positions[index3];
					const Vector_4V cross(V3Cross(V4Subtract(pos2,pos1), V4Subtract(pos3,pos1)));
					const Vector_4V faceNrm(V3Normalize(cross));
					const Vec3V nrm1(V4SelectFT((Vector_4V)hasNormals, faceNrm, normals[index1]));
					const Vec3V nrm2(V4SelectFT((Vector_4V)hasNormals, faceNrm, normals[index2]));
					const Vec3V nrm3(V4SelectFT((Vector_4V)hasNormals, faceNrm, normals[index3]));
					const Vector_4V vds1 = V4SelectFT((Vector_4V)hasDiffuse, onef, V4SplatY(diffuse[index1]));
					const Vector_4V vds2 = V4SelectFT((Vector_4V)hasDiffuse, onef, V4SplatY(diffuse[index2]));
					const Vector_4V vds3 = V4SelectFT((Vector_4V)hasDiffuse, onef, V4SplatY(diffuse[index3]));
					ProcessTri(settings, typeSettings, renderSettings,
						posScale, invPosScale, *msOffsetFlt,
						Vec3V(pos1), Vec3V(pos2), Vec3V(pos3), nrm1, nrm2, nrm3,
						ScalarV(vds1), ScalarV(vds2), ScalarV(vds3), isGlass, miniIdxBufWriter);
				}
			}
		}

		edgeGeomFinalize(&geomCtx);
	}

#	undef ALIGNOF

#endif // __SPU && (DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED || DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED)


#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED

	///////////////////////////////////////////////////////////////////////////
	//  ProcessDrawable
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessDrawable(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, const rmcDrawable* pDrawable, unsigned drawableIdx, bool isDamaged, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8 *skelDrawableIdxs, DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		// ask the game for a lod to apply the decal onto
		DECAL_ASSERT_ALREADY_LOCKED(pEntity);
		DECAL_ASSERT_ALREADY_LOCKED(pDrawable);
		const rmcLod* const pLodEa = g_pDecalCallbacks->GetLod(pEntity, pDrawable);
		if (Unlikely(!pLodEa))
		{
			// no lod or game doesn't want to process this
			return;
		}
		DECAL_ASSERT_ALREADY_LOCKED(pLodEa);

		// get the lod
		BLOCKING_DMA_GET(rmcLod, pLod, pLodEa);

		// get the array of models pointers from the lod
		const unsigned modelCount = pLod->GetCount();
		if (Unlikely(!modelCount))
		{
			return;
		}

		AllowVertexBufferVramLocks allow;

		BLOCKING_DMA_GET_ARRAY(datOwner<grmModel>, modelPtrArray, modelCount, pLod->GetModelPtrArray());

		const grmShaderGroup* const shaderGroupEa = &pDrawable->GetShaderGroup();
		DECAL_ASSERT_ALREADY_LOCKED(shaderGroupEa);
		BLOCKING_DMA_GET(grmShaderGroup, shaderGroup, shaderGroupEa);

		BLOCKING_DMA_GET_ARRAY(datOwner<grmShader>, shaderPtrArray, shaderGroup->GetCount(), shaderGroup->GetShaderPtrArray());

#		if HACK_GTA4_MODELINFOIDX_ON_SPU && USE_EDGE && __PPU
			CGta4DbgSpuInfoStruct gtaSpuInfoStruct;
			gtaSpuInfoStruct.gta4ModelInfoIdx = pEntity->GetModelIndex();
			gtaSpuInfoStruct.gta4RenderPhaseID = 0x01; // called by decalManager
			gtaSpuInfoStruct.gta4ModelInfoType = g_pDecalCallbacks->GetModelInfoType(gtaSpuInfoStruct.gta4ModelInfoIdx);
#		endif

		PipelinedDmaGetter<grmModel,2> modelGetter(DMATAG_GET_MODEL0);
		DECAL_ASSERT_ALREADY_LOCKED(modelPtrArray[0]);
		modelGetter.Prefetch(modelPtrArray[0]);

		// Build lookup array to convert from a render bone index to a task bone index
		DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Render,Task, skelMtxIdxLut, DECAL_MAX_BONES);
		if (skelObjMtxs)
		{
			// First build a temporary hierarchy to task lut
			DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Hierarchy,Task, htotLut, DECAL_MAX_BONES);
			sysMemSet(htotLut.GetPtr(), 0xff, DECAL_MAX_BONES);
			const unsigned numSkelMtxs = taskDescLs->numSkelMtxs;
			decalBoneTaskIdx taskIdx(0);
			for (decalBoneHierarchyIdx hierarchyIdx(0); hierarchyIdx<DECAL_MAX_BONES; ++hierarchyIdx)
			{
				if (taskIdx<numSkelMtxs && skelBones[taskIdx]==hierarchyIdx)
				{
					if (!skelDrawableIdxs || skelDrawableIdxs[taskIdx.Val()]==drawableIdx)
					{
						htotLut[hierarchyIdx] = taskIdx;
					}
					++taskIdx;
				}
			}

			// Now create the actual render to task lut
			const unsigned numRenderBones = taskDescLs->numRenderBones;
			if (!numRenderBones)
			{
				sysMemCpy(skelMtxIdxLut.GetPtr(), htotLut.GetPtr(), DECAL_MAX_BONES);
			}
			else
			{
				sysMemSet(skelMtxIdxLut.GetPtr(), 0xff, DECAL_MAX_BONES);
				decalAssert(!!renderToHierarchyLut);
				for (decalBoneRenderIdx renderIdx(0); renderIdx<numRenderBones; ++renderIdx)
				{
					skelMtxIdxLut[renderIdx] = htotLut[renderToHierarchyLut[renderIdx]];
				}
			}
		}

		for (unsigned modelIdx=0; modelIdx<modelCount; ++modelIdx)
		{
			if (Likely(modelIdx+1<modelCount))
			{
				DECAL_ASSERT_ALREADY_LOCKED(modelPtrArray[modelIdx+1]);
				modelGetter.Prefetch(modelPtrArray[modelIdx+1]);
			}
			const grmModel *const pLodModel = modelGetter.Get();

			// Don't process skinned models.
			// Seperate DECAL_DO_PROCESSDRAWABLESSKINNED task will handle it.
			if (pLodModel->GetSkinFlag())
			{
				continue;
			}

			// go through the models geometry groups
			const unsigned geomCount = pLodModel->GetGeometryCount();
			if (Unlikely(!geomCount))
			{
				continue;
			}

			const decalBoneRenderIdx boneRenderIdx(pLodModel->GetMatrixIndex());
			Mat34V boneWldMtx;
			if (boneRenderIdx == 0xff)
			{
				boneWldMtx = taskDescLs->entityMtx;
			}
			// It is not really clear if this is valid art or not, but there is
			// a few rare things that have a boneRenderIdx of 0, and no skeleton
			// or skeleton data.  Seems odd, but we will support it for now.
			else if (Unlikely(!skelObjMtxs))
			{
				// But a non-zero boneRenderIdx definitely doesn't make sense
				// though, so assert about that.
				artAssertf(boneRenderIdx==0,
					"\"%s\" has a matrix index (%u) specified for a grmModel, but has no skeleton",
					debugName, boneRenderIdx.Val());
				boneWldMtx = taskDescLs->entityMtx;
			}
			else
			{
				decalAssert(boneRenderIdx < DECAL_MAX_BONES);
				const decalBoneTaskIdx boneTaskIdx = skelMtxIdxLut[boneRenderIdx];
				if (boneTaskIdx == 0xff)
				{
					continue;
				}
				const Mat34V boneObjMtx = UnpackSkelObjMtx(skelObjMtxs, boneTaskIdx);
				Transform(boneWldMtx, taskDescLs->entityMtx, boneObjMtx);
			}

			// get some insts to store the clipped triangle in
			decalInst* pInstGlassEa = NULL;
			decalInst* pInstNonGlassEa = NULL;
			if (GetInsts(&pInstGlassEa, &pInstNonGlassEa))
			{
				// initialise the insts
#				if !__SPU
					decalInst* const pInstGlassLs    = pInstGlassEa;
					decalInst* const pInstNonGlassLs = pInstNonGlassEa;
#				else
					char pInstGlassBuf   [DMA_BUF_SIZE_TYPE(decalInst, 16)] ;
					char pInstNonGlassBuf[DMA_BUF_SIZE_TYPE(decalInst, 16)] ;
					decalInst* const pInstGlassLs    = rage_placement_new (pInstGlassBuf   +((u32)pInstGlassEa   &15)) decalInst;
					decalInst* const pInstNonGlassLs = rage_placement_new (pInstNonGlassBuf+((u32)pInstNonGlassEa&15)) decalInst;
#				endif
				taskDescLs->settings.CalcLocalInternalSettings(boneWldMtx, pInstGlassLs, pInstNonGlassLs, DECAL_ATTACH_TYPE_BONE);
				pInstGlassLs->Init(taskDescLs->settings);
				pInstNonGlassLs->Init(taskDescLs->settings);
				const Vec3V msOffsetInt = CalcModelSpaceOffsetInt(&taskDescLs->settings);
				const Vec3V msOffsetFlt = msOffsetInt * GetModelSpaceOffsetUnits();

				DMA_GET_ARRAY(u16, shaderIndices, geomCount, pLodModel->GetShaderIndexPtr(), DMATAG_BLOCKING);
				BLOCKING_DMA_GET_ARRAY(datOwner<grmGeometry>, geomPtrArry, geomCount, pLodModel->GetGeometryPtrArray());

				PipelinedDmaGetter<LARGEST_GEOMETRY_TYPE,2> geomGetter(DMATAG_GET_GEOM0);
				decalAssert(geomCount >= 1);
				geomGetter.Prefetch(static_cast<LARGEST_GEOMETRY_TYPE*>(geomPtrArry[0].ptr));

				for (unsigned geomIdx=0; geomIdx<geomCount; ++geomIdx)
				{
					if (Likely(geomIdx+1<geomCount))
					{
						const grmGeometry* const geomEa = geomPtrArry[geomIdx+1].ptr;
						DECAL_ASSERT_ALREADY_LOCKED(geomEa);
						geomGetter.Prefetch(static_cast<const LARGEST_GEOMETRY_TYPE*>(geomEa));
					}

					// get information about this shader
					const unsigned shaderIndex = shaderIndices[geomIdx];
					decalAssert(shaderIndex < (unsigned)shaderGroup->GetCount());
					const grmShader* const shaderEa = shaderPtrArray[shaderIndex].ptr;
					DECAL_ASSERT_ALREADY_LOCKED(shaderEa);
					BLOCKING_DMA_GET(grmShader, shader, shaderEa);
					const bool isGlassShader = g_pDecalCallbacks->IsGlassShader(shader);
					if (!ShouldProcessShader(shader, pEntity, isGlassShader, taskDescLs->settings.user.pipelineSettings.onlyApplyToGlass, taskDescLs->settings.user.pipelineSettings.dontApplyToGlass))
					{
						geomGetter.Skip();
						continue;
					}

					const grmGeometry* const geom = geomGetter.Get();
					const int geomType = geom->GetType();

#				if USE_EDGE
					if (geomType == grmGeometry::GEOMETRYEDGE)
					{
#						if __PPU
							ms_edgeClipInfo.pVertsSpuCurr = &ms_edgeClipInfo.vertsSpu[0];

							ms_edgeClipInfo.pClipPlanes[0] = taskDescLs->settings.internal.lclClipper.GetPlaneEquationsPtr();
							ms_edgeClipInfo.decalNormal[0] = -taskDescLs->settings.internal.vLclDirection;

							float polyRejectThreshold = typeSettings->polyRejectThreshold;
#							if __BANK
								if (m_debug.m_disablePolyAngleTest)
								{
									polyRejectThreshold = -1.0f;
								}
#							endif

							const grmGeometryEdge* const pGeomEdge = static_cast<const grmGeometryEdge*>(geom);
							BANK_ONLY(bool res=) pGeomEdge->ClipGeomWithPlanes((const Vector4**)ms_edgeClipInfo.pClipPlanes,
								(u32)1 | u32(/*isHardSkinned*/1)<<16,	// numBones is s16
								NULL, 0,
								(Vector4**)&ms_edgeClipInfo.pVertsSpuCurr,
								(Vector4*)&ms_edgeClipInfo.vertsSpu[DECAL_MAX_SPU_PT_VERTS],
								NULL,
								0.0f,
								(const Vector3*)&ms_edgeClipInfo.decalNormal[0],
								polyRejectThreshold
#							if HACK_GTA4_MODELINFOIDX_ON_SPU
								, &gtaSpuInfoStruct
#							endif
								);
#							if __BANK
								if (Unlikely(!res))
								{
									decalSpamf("ClipGeomWithPlanes out of space, some decals may be missing");
								}
#							endif

							Vec4V *pPolys = &ms_edgeClipInfo.vertsSpu[0];
							decalAssertf(ms_edgeClipInfo.pVertsSpuCurr <= &ms_edgeClipInfo.vertsSpu[DECAL_MAX_SPU_PT_VERTS], "vertsSpu array overflow!");

							// we found at least 1 polygon
							while (pPolys != ms_edgeClipInfo.pVertsSpuCurr)
							{
								const unsigned numVerts = (unsigned)pPolys[0].GetWf();
								Assert(numVerts > 1);
								Vec3V *pNormals = (Vec3V*)pPolys + numVerts;
								Vec4V *pCpvs    = (Vec4V*)pPolys + numVerts*2;

								pPolys[0].SetW(1.0f);
								pPolys[1].SetW(1.0f);

								// Need to splat to green component of the cpv to get the vehicle damage scale value as a scalar
								for (unsigned k=0; k<numVerts; ++k)
								{
									pCpvs[k] = Vec4V(pCpvs[k].GetY());
								}

								ProcessClippedPolygon(&taskDescLs->settings, renderSettings, posScale, invPosScale, msOffsetFlt, numVerts, (Vec3V*)pPolys, pNormals, (ScalarV*)pCpvs, isGlassShader, miniIdxBufWriter);

								pPolys += numVerts*3;
							}
#						else // __SPU
							const grmGeometryEdge* const geomEdge = static_cast<const grmGeometryEdge*>(geom);
							const unsigned numSegments = geomEdge->GetBlendHeaderCount();
							CompileTimeAssert(__alignof__(EdgeGeomPpuConfigInfo) >= 16);
							const sysScratchScope dmaGetScopedResetScratch;
							EdgeGeomPpuConfigInfo *const ppuConf = sysScratchAllocObj<EdgeGeomPpuConfigInfo>(numSegments);
							sysDmaGetAndWait(ppuConf, (u32)(geomEdge->GetEdgeGeomPpuConfigInfos()), numSegments*sizeof(*ppuConf), DMATAG_BLOCKING);
							for (unsigned k=0; k<numSegments; ++k)
							{
								const unsigned numBoneSettings = 1;
								const DECAL_BONEREMAPLUT_CTOR(Edge,Loop) edgeToLoopLut(NULL);
								ProcessEdgeSegment<false>(DECAL_DEBUG_NAME_ONLY(debugName,) &taskDescLs->settings, numBoneSettings, typeSettings, renderSettings, posScale, invPosScale, &msOffsetFlt, ppuConf+k, edgeToLoopLut, isGlassShader, miniIdxBufWriter);
							}
#						endif // __SPU
					}
					else
#				endif // USE_EDGE
					if (geomType == grmGeometry::GEOMETRYQB)
					{
						const grmGeometryQB* const geomQB = static_cast<const grmGeometryQB*>(geom);
						decalAssert(geomQB->GetPrimitiveType() == drawTris);
						if (Likely(geomQB->GetPrimitiveType() == drawTris))
						{
							// There's no sensible way to determine which index
							// and vertex buffer to use, since we are not
							// synchronized with the main thread calls to
							// grmGeometryQB::SwapBuffers().  But that doesn't
							// really matter, since decals don't make sense to
							// be applied to dynamic geometry, since the decal
							// will not move with the vertices.
							const grcIndexBuffer  *const pIdxBufEa = geomQB->GetIndexBufferByIndex(0);
							const grcVertexBuffer *const pVtxBufEa = geomQB->GetVertexBufferByIndex(0);
							DECAL_ASSERT_ALREADY_LOCKED(pIdxBufEa);
							DECAL_ASSERT_ALREADY_LOCKED(pVtxBufEa);
#							if !__SPU
								const grcIndexBuffer  *const pIdxBufLs = pIdxBufEa;
								const grcVertexBuffer *const pVtxBufLs = pVtxBufEa;
								const grcFvf *const fvf = pVtxBufEa->GetFvf();
								DECAL_ASSERT_ALREADY_LOCKED(fvf);
#							else
								char idxBufBuf[DMA_BUF_SIZE_TYPE(grcIndexBufferGCM, 16)] ;
								char vtxBufBuf[DMA_BUF_SIZE_TYPE(grcVertexBufferGCM,16)] ;
								const grcVertexBuffer *const pVtxBufLs = (grcVertexBuffer*)DmaGetAligned(vtxBufBuf, (u32)pVtxBufEa, sizeof(grcVertexBufferGCM), 16, DMATAG_GET_VTXBUF);
								const grcIndexBuffer  *const pIdxBufLs = (grcIndexBuffer*) DmaGetAligned(idxBufBuf, (u32)pIdxBufEa, sizeof(grcIndexBufferGCM),  16, DMATAG_BLOCKING);
								sysDmaWaitTagStatusAll(1<<DMATAG_GET_VTXBUF);
								BLOCKING_DMA_GET(grcFvf, fvf, pVtxBufLs->GetFvf());
#							endif
							const unsigned numIndices = pIdxBufLs->GetIndexCount();
							if (Likely(numIndices))
							{
								const u16 *const pIndexPtrEa = pIdxBufLs->LockRO();
								if (Likely(pIndexPtrEa))
								{
									DECAL_ASSERT_ALREADY_LOCKED(pIndexPtrEa);
									const void *const pVertexPtrEa = pVtxBufLs->LockRO();
									if (Likely(pVertexPtrEa))
									{
										DECAL_ASSERT_ALREADY_LOCKED(pVertexPtrEa);
										const unsigned vertexStride = pVtxBufLs->GetVertexStride();
										ProcessGeometry(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, pIndexPtrEa, numIndices, pVertexPtrEa, vertexStride, fvf, isGlassShader, miniIdxBufWriter);
										pVtxBufLs->UnlockRO();
									}
									pIdxBufLs->UnlockRO();
								}
							}
						}
					}
				}

				// add the inst to a bucket
				decalBoneHierarchyIdx boneHierarchyIdx;
				if (!renderToHierarchyLut)
				{
					boneHierarchyIdx = decalBoneHierarchyIdx(boneRenderIdx.Val());
				}
				else
				{
					boneHierarchyIdx = renderToHierarchyLut[boneRenderIdx];
				}
				decalAssert(boneHierarchyIdx.Val()+1u <= 0x10000u);     // +1 here, since boneHierarchyIdx could be -1
				if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, &taskDescLs->settings, posScale, msOffsetInt, pInstGlassLs,    pInstGlassEa,    boneWldMtx, boneHierarchyIdx, true,  isDamaged SPU_ONLY(, miniIdxBufWriter)))
				{
					SPU_ONLY(DmaPutAlignedButOddSize(pInstGlassLs,    (u32)pInstGlassEa,    DMATAG_BLOCKING);)
				}
				if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, &taskDescLs->settings, posScale, msOffsetInt, pInstNonGlassLs, pInstNonGlassEa, boneWldMtx, boneHierarchyIdx, false, isDamaged SPU_ONLY(, miniIdxBufWriter)))
				{
					SPU_ONLY(DmaPutAlignedButOddSize(pInstNonGlassLs, (u32)pInstNonGlassEa, DMATAG_BLOCKING);)
				}
				SPU_ONLY(sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);)
			}
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED


#if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

	///////////////////////////////////////////////////////////////////////////
	//  ProcessSkinnedDrawable
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessSkinnedDrawable(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, const rmcDrawable* pDrawable, unsigned drawableIdx, bool isDamaged, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8 *skelDrawableIdxs, const Mat34V *skelInvJoints, DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut, decalMiniIdxBufWriter* miniIdxBufWriter
#		if USE_EDGE && __PPU
			, decalSettings *boneSettings
#		endif
		)
	{
		// ask the game for a lod to apply the decal onto
		DECAL_ASSERT_ALREADY_LOCKED(pEntity);
		DECAL_ASSERT_ALREADY_LOCKED(pDrawable);
		const rmcLod* const pLodEa = g_pDecalCallbacks->GetLod(pEntity, pDrawable);
		if (Unlikely(!pLodEa))
		{
			// no lod or game doesn't want to process this
			return;
		}
		DECAL_ASSERT_ALREADY_LOCKED(pLodEa);

		// get the lod
		BLOCKING_DMA_GET(rmcLod, pLod, pLodEa);

		// get the array of models pointers from the lod
		const unsigned modelCount = pLod->GetCount();
		if (Unlikely(!modelCount))
		{
			return;
		}

		AllowVertexBufferVramLocks allow;

		BLOCKING_DMA_GET_ARRAY(datOwner<grmModel>, modelPtrArray, modelCount, pLod->GetModelPtrArray());

		const grmShaderGroup* const shaderGroupEa = &pDrawable->GetShaderGroup();
		DECAL_ASSERT_ALREADY_LOCKED(shaderGroupEa);
		BLOCKING_DMA_GET(grmShaderGroup, shaderGroup, shaderGroupEa);

		BLOCKING_DMA_GET_ARRAY(datOwner<grmShader>, shaderPtrArray, shaderGroup->GetCount(), shaderGroup->GetShaderPtrArray());

#		if HACK_GTA4_MODELINFOIDX_ON_SPU && USE_EDGE && __PPU
			CGta4DbgSpuInfoStruct gtaSpuInfoStruct;
			gtaSpuInfoStruct.gta4ModelInfoIdx = pEntity->GetModelIndex();
			gtaSpuInfoStruct.gta4RenderPhaseID = 0x01;
			gtaSpuInfoStruct.gta4ModelInfoType = g_pDecalCallbacks->GetModelInfoType(gtaSpuInfoStruct.gta4ModelInfoIdx);
#		endif

		const unsigned numTaskSkelMtxs = taskDescLs->numSkelMtxs;
		decalBoneTaskIdx nextTaskIdx(0);
		for (decalBoneTaskIdx taskIdx(0); taskIdx<numTaskSkelMtxs; taskIdx=nextTaskIdx)
		{
			// Move forward to next task bone index, if this one does not
			// correspond to the current drawable we are processing.
			if (skelDrawableIdxs && skelDrawableIdxs[taskIdx.Val()]!=drawableIdx)
			{
				++nextTaskIdx;
				continue;
			}

			// Select up to MAX_BONES_PER_LOOP to process.
			DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Loop,Hierarchy, origBoneIdx,        MAX_BONES_PER_LOOP);
			DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Hierarchy,Loop, hierarchyToLoopLut, DECAL_MAX_BONES);
			sysMemSet(hierarchyToLoopLut.GetPtr(), 0xff, DECAL_MAX_BONES);

			// Setup instances for each of the bones we are going to process.
#			if !USE_EDGE || !__PPU
#				if !__SPU
					decalSettings boneSettings[MAX_BONES_PER_LOOP];
#				else
					// Avoid calling the ctor in SPU code since it wastes a lot
					// of local store space and is completely unnecissary.
					char boneSettingsBuf[MAX_BONES_PER_LOOP*sizeof(decalSettings)] ALIGNED(__alignof(decalSettings));
					decalSettings *const boneSettings = (decalSettings*)boneSettingsBuf;
#				endif
#			endif
			decalInst* pInstGlassEa   [MAX_BONES_PER_LOOP];
			decalInst* pInstNonGlassEa[MAX_BONES_PER_LOOP];
#			if !__SPU
				decalInst*const*const pInstGlassLs    = pInstGlassEa;
				decalInst*const*const pInstNonGlassLs = pInstNonGlassEa;
#			else
				char pInstGlassBuf   [MAX_BONES_PER_LOOP][DMA_BUF_SIZE_TYPE(decalInst,16)] ;
				char pInstNonGlassBuf[MAX_BONES_PER_LOOP][DMA_BUF_SIZE_TYPE(decalInst,16)] ;
				decalInst* pInstGlassLs   [MAX_BONES_PER_LOOP];
				decalInst* pInstNonGlassLs[MAX_BONES_PER_LOOP];
#			endif
			Vec3V msOffsetInt[MAX_BONES_PER_LOOP];
			Vec3V msOffsetFlt[MAX_BONES_PER_LOOP];
			decalAssert(taskIdx == nextTaskIdx);
			decalBoneLoopIdx boneRemapIdx(0);
			do
			{
				if (nextTaskIdx >= numTaskSkelMtxs)
				{
					break;
				}

				if (!skelDrawableIdxs || skelDrawableIdxs[nextTaskIdx.Val()]==drawableIdx)
				{
					// get some insts to store the clipped triangle in
					if (GetInsts(pInstGlassEa+boneRemapIdx.Val(), pInstNonGlassEa+boneRemapIdx.Val()))
					{
						BANK_ONLY(++(m_debug.m_numSkinnedBoneMatricesThisProjection);)

						// initialise the insts
#						if __SPU
							pInstGlassLs[boneRemapIdx.Val()]    = rage_placement_new (pInstGlassBuf[boneRemapIdx.Val()]   +((u32)pInstGlassEa[boneRemapIdx.Val()]   &15)) decalInst;
							pInstNonGlassLs[boneRemapIdx.Val()] = rage_placement_new (pInstNonGlassBuf[boneRemapIdx.Val()]+((u32)pInstNonGlassEa[boneRemapIdx.Val()]&15)) decalInst;
#						endif

						const decalBoneHierarchyIdx hierarchyIdx = skelBones[nextTaskIdx];
						const Mat34V boneObjMtx = UnpackSkelObjMtx(skelObjMtxs, nextTaskIdx);
						Mat34V skinnedMtx, boneWldMtx;
#						if __SPU
							Transform(skinnedMtx, boneObjMtx, skelInvJoints[nextTaskIdx.Val()]);
#						else
							Transform(skinnedMtx, boneObjMtx, skelInvJoints[hierarchyIdx.Val()]);
#						endif
						Transform(boneWldMtx, taskDescLs->entityMtx, skinnedMtx);

						taskDescLs->settings.CalcLocalInternalSettings(boneWldMtx, pInstGlassLs[boneRemapIdx.Val()], pInstNonGlassLs[boneRemapIdx.Val()], DECAL_ATTACH_TYPE_BONE_SKINNED);
						pInstGlassLs[boneRemapIdx.Val()]->Init(taskDescLs->settings);
						pInstNonGlassLs[boneRemapIdx.Val()]->Init(taskDescLs->settings);
						const Vec3V msoi = CalcModelSpaceOffsetInt(&taskDescLs->settings);
						msOffsetInt[boneRemapIdx.Val()] = msoi;
						msOffsetFlt[boneRemapIdx.Val()] = msoi * GetModelSpaceOffsetUnits();

						// we want to process this bone - init the settings
						boneSettings[boneRemapIdx.Val()] = taskDescLs->settings;
#						if USE_EDGE && __PPU
							ms_edgeClipInfo.pClipPlanes[boneRemapIdx.Val()] =  boneSettings[boneRemapIdx.Val()].internal.lclClipper.GetPlaneEquationsPtr();
							ms_edgeClipInfo.decalNormal[boneRemapIdx.Val()] = -boneSettings[boneRemapIdx.Val()].internal.vLclDirection;
#						endif
						origBoneIdx[boneRemapIdx] = hierarchyIdx;
						hierarchyToLoopLut[hierarchyIdx] = boneRemapIdx++;
					}
				}

				++nextTaskIdx;
			}
			while (boneRemapIdx < MAX_BONES_PER_LOOP);
			const unsigned numBoneSettings = boneRemapIdx.Val();


			// Iterate over the models in the drawable
			PipelinedDmaGetter<grmModel,2> modelGetter(DMATAG_GET_MODEL0);
			DECAL_ASSERT_ALREADY_LOCKED(modelPtrArray[0]);
			modelGetter.Prefetch(modelPtrArray[0]);
			for (unsigned j=0; j<modelCount; j++)
			{
				if (Likely(j+1<modelCount))
				{
					DECAL_ASSERT_ALREADY_LOCKED(modelPtrArray[j+1]);
					modelGetter.Prefetch(modelPtrArray[j+1]);
				}
				const grmModel *const pLodModel = modelGetter.Get();

				// Don't process non-skinned models.
				// Seperate DECAL_DO_PROCESSDRAWABLESNONSKINNED task will handle it.
				if (!pLodModel->GetSkinFlag())
				{
					continue;
				}

				// go through the models geometry groups
				const unsigned geomCount = pLodModel->GetGeometryCount();
				if (Unlikely(!geomCount))
				{
					continue;
				}

				DMA_GET_ARRAY(u16, shaderIndices, geomCount, pLodModel->GetShaderIndexPtr(), DMATAG_BLOCKING);
				BLOCKING_DMA_GET_ARRAY(datOwner<grmGeometry>, geomPtrArry, geomCount, pLodModel->GetGeometryPtrArray());

				PipelinedDmaGetter<LARGEST_GEOMETRY_TYPE,2> geomGetter(DMATAG_GET_GEOM0);
				decalAssert(geomCount >= 1);
				geomGetter.Prefetch(static_cast<LARGEST_GEOMETRY_TYPE*>(geomPtrArry[0].ptr));
				for (unsigned i=0; i<geomCount; i++)
				{
					if (Likely(i+1<geomCount))
					{
						const grmGeometry* const geomEa = geomPtrArry[i+1].ptr;
						DECAL_ASSERT_ALREADY_LOCKED(geomEa);
						geomGetter.Prefetch(static_cast<const LARGEST_GEOMETRY_TYPE*>(geomEa));
					}

					// get information about this shader
					const unsigned shaderIndex = shaderIndices[i];
					decalAssert(shaderIndex < (unsigned)shaderGroup->GetCount());
					const grmShader* const shaderEa = shaderPtrArray[shaderIndex].ptr;
					DECAL_ASSERT_ALREADY_LOCKED(shaderEa);
					BLOCKING_DMA_GET(grmShader, shader, shaderEa);
					const bool isGlassShader = g_pDecalCallbacks->IsGlassShader(shader);
					if (!ShouldProcessShader(shader, pEntity, isGlassShader, taskDescLs->settings.user.pipelineSettings.onlyApplyToGlass, taskDescLs->settings.user.pipelineSettings.dontApplyToGlass))
					{
						geomGetter.Skip();
						continue;
					}

					const grmGeometry* const geom = geomGetter.Get();

#				if USE_EDGE
					const int geomType = geom->GetType();
					if (geomType == grmGeometry::GEOMETRYEDGE)
					{
#						if __PPU
							ms_edgeClipInfo.pVertsSpuCurr = &ms_edgeClipInfo.vertsSpu[0];

							float polyRejectThreshold = typeSettings->polyRejectThreshold;
#							if __BANK
								if (m_debug.m_disablePolyAngleTest)
								{
									polyRejectThreshold = -1.0f;
								}
#							endif

							// renderToLoopLut is passed to the edgeExtractgeomspu job, so cannot be on the ppu stack.
							DECAL_BONEREMAPLUT_MUTABLE_ARRAY(static , Render,Loop, renderToLoopLut, DECAL_MAX_BONES);
							const unsigned numRenderBones = taskDescLs->numRenderBones;
							if (!numRenderBones)
							{
								for (decalBoneHierarchyIdx hierarchyIdx(0); hierarchyIdx<DECAL_MAX_BONES; ++hierarchyIdx)
								{
									const decalBoneRenderIdx renderIdx(hierarchyIdx.Val());
									const decalBoneLoopIdx loopIdx(hierarchyToLoopLut[hierarchyIdx]);
									renderToLoopLut[renderIdx] = loopIdx;
								}
							}
							else
							{
								for (decalBoneRenderIdx renderIdx(0); renderIdx<numRenderBones; ++renderIdx)
								{
									const decalBoneHierarchyIdx hierarchyIdx(renderToHierarchyLut[renderIdx]);
									const decalBoneLoopIdx loopIdx(hierarchyToLoopLut[hierarchyIdx]);
									renderToLoopLut[renderIdx] = loopIdx;
								}
							}

							const grmGeometryEdge* pGeomEdge = static_cast<const grmGeometryEdge*>(geom);
							Assert(numBoneSettings <= 0xffff);
							BANK_ONLY(bool res=) pGeomEdge->ClipGeomWithPlanes((const Vector4**)ms_edgeClipInfo.pClipPlanes,
								numBoneSettings | u32(/*isHardSkinned*/1)<<16,
								(const u8*)renderToLoopLut.GetPtr(), DECAL_MAX_BONES,
								(Vector4**)&ms_edgeClipInfo.pVertsSpuCurr,
								(Vector4*)&ms_edgeClipInfo.vertsSpu[DECAL_MAX_SPU_PT_VERTS],
								NULL,
								0.0f,
								(const Vector3*)&ms_edgeClipInfo.decalNormal[0],
								polyRejectThreshold
#								if HACK_GTA4_MODELINFOIDX_ON_SPU
									,&gtaSpuInfoStruct
#								endif
								);
#							if __BANK
								if (Unlikely(!res))
								{
									decalSpamf("ClipGeomWithPlanes out of space, some decals may be missing");
								}
#							endif

							Vec4V *pPolys = &ms_edgeClipInfo.vertsSpu[0];
							decalAssertf(ms_edgeClipInfo.pVertsSpuCurr <= &ms_edgeClipInfo.vertsSpu[DECAL_MAX_SPU_PT_VERTS], "vertsSpu array overflow (%s)", debugName);

							// we found at least 1 polygon
							while (pPolys != ms_edgeClipInfo.pVertsSpuCurr)
							{
								const unsigned numVerts = (unsigned)pPolys[0].GetWf();
								decalAssertf(numVerts > 1, "invalid number of verts found (%s)", debugName);

								Vec3V *pNormals = (Vec3V*)pPolys + numVerts;
								Vec4V *pCpvs    = (Vec4V*)pPolys + numVerts*2;

								const decalBoneLoopIdx packedBoneIdx((unsigned)pPolys[1].GetWf());
								decalAssert(packedBoneIdx < numBoneSettings);
								decalAssert(origBoneIdx[packedBoneIdx] < DECAL_MAX_BONES);

								pPolys[0].SetW(1.0f);
								pPolys[1].SetW(1.0f);

								// Need to splat to green component of the cpv to get the vehicle damage scale value as a scalar
								for (unsigned k=0; k<numVerts; ++k)
								{
									pCpvs[k] = Vec4V(pCpvs[k].GetY());
								}

								ProcessClippedPolygon(boneSettings+packedBoneIdx.Val(), renderSettings, posScale, invPosScale, msOffsetFlt[packedBoneIdx.Val()], numVerts, (Vec3V*)pPolys, pNormals, (ScalarV*)pCpvs, isGlassShader, miniIdxBufWriter);

								pPolys += numVerts*3;
							}
#						else // __SPU
							const grmGeometryEdge* const geomEdge = static_cast<const grmGeometryEdge*>(geom);
							const unsigned numSegments = geomEdge->GetBlendHeaderCount();
							CompileTimeAssert(__alignof__(EdgeGeomPpuConfigInfo) >= 16);
							const sysScratchScope dmaGetScopedResetScratch;
							EdgeGeomPpuConfigInfo *const ppuConf = sysScratchAllocObj<EdgeGeomPpuConfigInfo>(numSegments);
							sysDmaGetAndWait(ppuConf, (u32)(geomEdge->GetEdgeGeomPpuConfigInfos()), numSegments*sizeof(*ppuConf), DMATAG_BLOCKING);
							for (unsigned k=0; k<numSegments; ++k)
							{
								DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Edge,Loop, edgeToLoopLut, DECAL_MAX_BONES);
								sysMemSet(edgeToLoopLut.GetPtr(), 0xff, DECAL_MAX_BONES);
								EdgeGeomPpuConfigInfo *const conf = ppuConf+k;
								const u32 range0 = conf->skinMatricesSizes[0]/48;
								const u32 offset0 = conf->skinMatricesByteOffsets[0]/48;
								decalAssert(range0 + offset0 < DECAL_MAX_BONES);
								for (decalBoneEdgeIdx L(0); L<range0; ++L)
								{
									const decalBoneRenderIdx renderIdx(L+offset0);
									const decalBoneHierarchyIdx hierarchyIdx(!!renderToHierarchyLut ? renderToHierarchyLut[renderIdx] : decalBoneHierarchyIdx(renderIdx.Val()));
									const decalBoneLoopIdx loopIdx(hierarchyToLoopLut[hierarchyIdx]);
									edgeToLoopLut[L] = loopIdx;
								}
								const u32 range1 = conf->skinMatricesSizes[1]/48;
								const u32 offset1 = conf->skinMatricesByteOffsets[1]/48;
								decalAssert(range1 + offset1 < DECAL_MAX_BONES);
								for (decalBoneEdgeIdx L(0); L<range1; ++L)
								{
									const decalBoneRenderIdx renderIdx(L+offset1);
									const decalBoneHierarchyIdx hierarchyIdx(!!renderToHierarchyLut ? renderToHierarchyLut[renderIdx] : decalBoneHierarchyIdx(renderIdx.Val()));
									const decalBoneLoopIdx loopIdx(hierarchyToLoopLut[hierarchyIdx]);
									edgeToLoopLut[(decalBoneEdgeIdx)(L.Val()+range0)] = loopIdx;
								}
								ProcessEdgeSegment<true>(DECAL_DEBUG_NAME_ONLY(debugName,) boneSettings, numBoneSettings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, conf, edgeToLoopLut, isGlassShader, miniIdxBufWriter);
							}
#						endif // __SPU
					}
					else
#				endif // USE_EDGE
					{
						const grmGeometryQB* const geomQB = static_cast<const grmGeometryQB*>(geom);
						decalAssert(geomQB->GetPrimitiveType() == drawTris);
						if (Likely(geomQB->GetPrimitiveType() == drawTris))
						{
							// There's no sensible way to determine which index
							// and vertex buffer to use, since we are not
							// synchronized with the main thread calls to
							// grmGeometryQB::SwapBuffers().  But that doesn't
							// really matter, since decals don't make sense to
							// be applied to dynamic geometry, since the decal
							// will not move with the vertices.
							const grcIndexBuffer  *const pIdxBufEa = geomQB->GetIndexBufferByIndex(0);
							const grcVertexBuffer *const pVtxBufEa = geomQB->GetVertexBufferByIndex(0);
							DECAL_ASSERT_ALREADY_LOCKED(pIdxBufEa);
							DECAL_ASSERT_ALREADY_LOCKED(pVtxBufEa);
#							if !__SPU
								const grcIndexBuffer  *const pIdxBufLs = pIdxBufEa;
								const grcVertexBuffer *const pVtxBufLs = pVtxBufEa;
								const grcFvf *const fvf = pVtxBufEa->GetFvf();
								DECAL_ASSERT_ALREADY_LOCKED(fvf);
#							else
								char idxBufBuf[DMA_BUF_SIZE_TYPE(grcIndexBufferGCM, 16)] ;
								char vtxBufBuf[DMA_BUF_SIZE_TYPE(grcVertexBufferGCM,16)] ;
								const grcVertexBuffer *const pVtxBufLs = (grcVertexBuffer*)DmaGetAligned(vtxBufBuf, (u32)pVtxBufEa, sizeof(grcVertexBufferGCM), 16, DMATAG_GET_VTXBUF);
								const grcIndexBuffer  *const pIdxBufLs = (grcIndexBuffer*) DmaGetAligned(idxBufBuf, (u32)pIdxBufEa, sizeof(grcIndexBufferGCM),  16, DMATAG_BLOCKING);
								sysDmaWaitTagStatusAll(1<<DMATAG_GET_VTXBUF);
								BLOCKING_DMA_GET(grcFvf, fvf, pVtxBufLs->GetFvf());
#							endif
							const unsigned numIndices = pIdxBufLs->GetIndexCount();
							if (Likely(numIndices))
							{
								const u16 *const pIndexPtrEa = pIdxBufLs->LockRO();
								if (Likely(pIndexPtrEa))
								{
									DECAL_ASSERT_ALREADY_LOCKED(pIndexPtrEa);
									const void *const pVertexPtrEa = pVtxBufLs->LockRO();
									if (Likely(pVertexPtrEa))
									{
										DECAL_ASSERT_ALREADY_LOCKED(pVertexPtrEa);
										const unsigned vertexStride = pVtxBufLs->GetVertexStride();
#										if !__SPU
											const unsigned matrixPaletteCount = geomQB->GetMatrixPaletteCount();
											const u16* const pMatrixPalette = geomQB->GetMatrixPalette();
											DECAL_ASSERT_ALREADY_LOCKED(pMatrixPalette);
#										else
											const unsigned matrixPaletteCount = geomQB->grmGeometryQB::GetMatrixPaletteCount();
											const u16* const pMatrixPaletteEa = geomQB->grmGeometryQB::GetMatrixPalette();
											DECAL_ASSERT_ALREADY_LOCKED(pMatrixPaletteEa);
											BLOCKING_DMA_GET_ARRAY(u16, pMatrixPalette, matrixPaletteCount, pMatrixPaletteEa);
#										endif
										DECAL_BONEREMAPLUT_CTOR(Vertex,Render,u16) vertexToRenderLut(pMatrixPalette);
										DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Vertex,Loop, vertexToLoopLut, DECAL_MAX_BONES);
										sysMemSet(vertexToLoopLut.GetPtr(), 0xff, DECAL_MAX_BONES);
										for (decalBoneVertexIdx vtxIdx(0); vtxIdx<matrixPaletteCount; ++vtxIdx)
										{
											const decalBoneRenderIdx renderIdx(vertexToRenderLut[vtxIdx]);
											const decalBoneHierarchyIdx hierarchyIdx(!!renderToHierarchyLut ? renderToHierarchyLut[renderIdx] : decalBoneHierarchyIdx(renderIdx.Val()));
											const decalBoneLoopIdx loopIdx(hierarchyToLoopLut[hierarchyIdx]);
											vertexToLoopLut[vtxIdx] = loopIdx;
										}
										ProcessSkinnedGeometry(DECAL_DEBUG_NAME_ONLY(debugName,) boneSettings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, pIndexPtrEa, numIndices, pVertexPtrEa, vertexStride, fvf, vertexToLoopLut, isGlassShader, miniIdxBufWriter);
										pVtxBufLs->UnlockRO();
									}
									pIdxBufLs->UnlockRO();
								}
							}
						}
					}
				}
			}

			// add the active insts to buckets
			const Mat34V wldMtx(V_IDENTITY);
			for (decalBoneLoopIdx boneLoopIndex(0); boneLoopIndex<numBoneSettings; ++boneLoopIndex)
			{
				// add the inst to a bucket
				const decalBoneHierarchyIdx boneIdx = origBoneIdx[boneLoopIndex];
				decalInst* const instGlassEa = pInstGlassEa[boneLoopIndex.Val()];
				if (instGlassEa)
				{
					decalInst* const instGlassLs = pInstGlassLs[boneLoopIndex.Val()];
					if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, boneSettings+boneLoopIndex.Val(), posScale, msOffsetInt[boneLoopIndex.Val()], instGlassLs,    instGlassEa,    wldMtx, boneIdx, true,  isDamaged SPU_ONLY(, miniIdxBufWriter)))
					{
						SPU_ONLY(DmaPutAlignedButOddSize(instGlassLs,    (u32)instGlassEa,    DMATAG_BLOCKING);)
					}
				}
				decalInst* const instNonGlassEa = pInstNonGlassEa[boneLoopIndex.Val()];
				if (instNonGlassEa)
				{
					decalInst* const instNonGlassLs = pInstNonGlassLs[boneLoopIndex.Val()];
					if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, boneSettings+boneLoopIndex.Val(), posScale, msOffsetInt[boneLoopIndex.Val()], instNonGlassLs, instNonGlassEa, wldMtx, boneIdx, false, isDamaged SPU_ONLY(, miniIdxBufWriter)))
					{
						SPU_ONLY(DmaPutAlignedButOddSize(instNonGlassLs, (u32)instNonGlassEa, DMATAG_BLOCKING);)
					}
				}
			}
			SPU_ONLY(sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);)
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED


///////////////////////////////////////////////////////////////////////////////
//  CheckCullBox
///////////////////////////////////////////////////////////////////////////////

bool decalManagerAsync::CheckCullBox(const decalSettings* settings, const phBound* bound, Mat34V_In wldMtx) const
{
	DECAL_ASSERT_ALREADY_LOCKED(bound);

	// decal cull box
	Vec3V cullBoxHalfWidths = Scale(settings->user.instSettings.vDimensions, ScalarV(V_HALF));
	cullBoxHalfWidths.SetW(ScalarV(V_ZERO));
	Mat34V cullBoxMatInv;
	InvertTransformOrtho(cullBoxMatInv, settings->GetDecalMatrix());

	// get the center and half width of both bounds
	Vec3V vHalfWidths;
	Vec3V vCenter;
	bound->GetBoundingBoxHalfWidthAndCenter(vHalfWidths, vCenter);
	vHalfWidths.SetW(ScalarV(V_ZERO));

	// adjust the matrices so that they are positioned in the center of the bounding box
	Mat34V mtx = wldMtx;
	Vec3V offset = Transform3x3(mtx, vCenter);
	mtx.SetCol3(mtx.GetCol3() + offset);

#if __BANK && !__SPU
	if (m_debug.m_renderBoundsConsidered)
	{
		grcDebugDraw::BoxOriented(-vHalfWidths, vHalfWidths, mtx, Color32(g_DrawRand.GetRanged(0.0f, 1.0f), g_DrawRand.GetRanged(0.0f, 1.0f), g_DrawRand.GetRanged(0.0f, 1.0f), 1.0f), false, m_debug.m_renderTimer);
	}
#endif

	// calculate bound matrix relative to cull box matrix
	Mat34V boundRelativeToCullBoxMtx;
	Transform(boundRelativeToCullBoxMtx, cullBoxMatInv, mtx);

	// test if the two bounds intersect
	return geomBoxes::TestBoxToBoxOBB(RCC_VECTOR3(vHalfWidths), RCC_VECTOR3(cullBoxHalfWidths), RCC_MATRIX34(boundRelativeToCullBoxMtx));
}


#if DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS

	///////////////////////////////////////////////////////////////////////////
	//  ProcessBreakable
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessBreakable(DECAL_DEBUG_NAME_ONLY(const char *UNUSED_PARAM(debugName),) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, bgBreakable* pBreakable, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		decalInst* pInstGlassEa = m_pools.GetInst(SPU_ONLY(&m_thisEa->m_pools, DMATAG_BLOCKING));
		decalInst* const pInstNonGlassLs = NULL;
		if (Likely(pInstGlassEa))
		{
			// get the smash group matrix
			Mat34V breakableWldMtx = RCC_MAT34V(pBreakable->GetTransform());

			// initialise the insts
#			if !__SPU
				decalInst* const pInstGlassLs = pInstGlassEa;
#			else
				char pInstGlassBuf[DMA_BUF_SIZE_TYPE(decalInst, 16)] ;
				decalInst* const pInstGlassLs = rage_placement_new (pInstGlassBuf+((u32)pInstGlassEa&15)) decalInst;
#			endif
			taskDescLs->settings.CalcLocalInternalSettings(breakableWldMtx, pInstGlassLs, pInstNonGlassLs, DECAL_ATTACH_TYPE_BREAKABLE_BROKEN);
			pInstGlassLs->Init(taskDescLs->settings);
			const Vec3V msOffsetInt = CalcModelSpaceOffsetInt(&taskDescLs->settings);
			const Vec3V msOffsetFlt = msOffsetInt * GetModelSpaceOffsetUnits();

			ScalarV vehDmgScale(V_ZERO);

			// go through this breakable's polys
			bgBreakable::TriangleId breakTriId = bgBreakable::kInvalidTriangle;
			breakTriId = pBreakable->GetNextPaneTriangle(breakTriId);
			while (breakTriId != bgBreakable::kInvalidTriangle)
			{
				Vec3V vPosA, vPosB, vPosC, vNormal;

				if (pBreakable->GetPaneTriangleInfo(breakTriId, RC_VECTOR3(vPosA), RC_VECTOR3(vPosB), RC_VECTOR3(vPosC), RC_VECTOR3(vNormal)))
				{
					const bool isGlassShader = true;
					ProcessTri(&taskDescLs->settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vPosA, vPosB, vPosC,  vNormal,  vNormal,  vNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassShader, miniIdxBufWriter);
					ProcessTri(&taskDescLs->settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vPosA, vPosC, vPosB, -vNormal, -vNormal, -vNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassShader, miniIdxBufWriter);
				}

				breakTriId = pBreakable->GetNextPaneTriangle(breakTriId);
			}

			// add the inst to a bucket
			Mat34V wldMtx(V_IDENTITY);
			if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, &taskDescLs->settings, posScale, msOffsetInt, pInstGlassLs, pInstGlassEa, wldMtx, taskDescLs->settings.user.pipelineSettings.colnComponentId, true, false SPU_ONLY(, miniIdxBufWriter)))
			{
#				if __SPU
					DmaPutAlignedButOddSize(pInstGlassLs, (u32)pInstGlassEa, DMATAG_BLOCKING);
					sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);
#				endif
			}
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS


#if DECAL_MANAGER_ENABLE_PROCESSBOUND || DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS

	///////////////////////////////////////////////////////////////////////////
	//  ProcessBound
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessBound(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, phInst* pPhInst, const phBound* pBound, Mat34V_In boundWldMtx, s16 childId, bool isBreakable, bool isBrokenBreakable, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		const int boundType = pBound->GetType();

		// if this is a composite bound then recurse with the child bounds
		if (boundType == phBound::COMPOSITE)
		{
			const phBoundComposite* pBoundComposite = static_cast<const phBoundComposite*>(pBound);
			const unsigned numBounds = pBoundComposite->GetNumBounds();
			if (Unlikely(!numBounds))
			{
				return;
			}

			// fetch the per child flags
#			if !__SPU
				const u32 *const typeAndIncludeFlags = pBoundComposite->GetTypeAndIncludeFlags();
#			else
				u32 *typeAndIncludeFlags = NULL;
				const u32 typeAndIncludeFlagsEa = (u32)pBoundComposite->GetTypeAndIncludeFlags();
				if (typeAndIncludeFlagsEa)
				{
				    typeAndIncludeFlags = (u32*)(((u32)RageAlloca(
						DMA_BUF_SIZE(numBounds*2*sizeof(u32), __alignof__(u32), 128) + 128) + 127) & ~127);
					typeAndIncludeFlags = (u32*)DmaGetAligned(
						typeAndIncludeFlags, typeAndIncludeFlagsEa, numBounds*2*sizeof(u32), 128, DMATAG_BLOCKING);
				}
#			endif

			// if there isn't flags per child, then do a high level check on the composite bound
			if (!typeAndIncludeFlags)
			{
#if __BANK
				if (m_debug.m_disableBoundTypeTest==false)
#endif
				{
					if (!g_pDecalCallbacks->ShouldProcessCompositeBoundFlags(taskDescLs->includeFlags))
					{
						return;
					}
				}
			}

			// cannot grab all the child bounds and matrices in one go, as sometimes there is too many to fit in spu local store
			enum { NUM_CHILDREN_PER_LOOP = 16 };
			unsigned numBoundsNextLoop = Min(numBounds, (unsigned)NUM_CHILDREN_PER_LOOP);
#			if __SPU
				const phBound** childBoundsEa = pBoundComposite->GetBoundArray();
				const Mat34V* matricesEa = (Mat34V*)pBoundComposite->GetCurrentMatrices();
				const phBound* childBoundsDblBuf[2][(NUM_CHILDREN_PER_LOOP+8+3)&~3] ; // +8 for 32 bytes dma padding, rounded up to 16 byte multiple
				Mat34V matricesDblBuf[2][NUM_CHILDREN_PER_LOOP];
				sysDmaGet(childBoundsDblBuf[0], (u32)childBoundsEa&~15, (((u32)childBoundsEa&15)+numBoundsNextLoop*sizeof(void*)+15)&~15, DMATAG_BLOCKING);
				sysDmaGet(matricesDblBuf[0], (u32)matricesEa, numBoundsNextLoop*sizeof(Mat34V), DMATAG_GET_PHBOUNDPTRS);
				unsigned bufIdx = 0;
				const phBound** childBounds = (const phBound**)((u32)childBoundsDblBuf[0] + ((u32)childBoundsEa&15));
				const Mat34V*   matrices = matricesDblBuf[0];
				childBoundsEa += numBoundsNextLoop;
				matricesEa    += numBoundsNextLoop;
				sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);
#			else
				const phBound** childBounds = pBoundComposite->GetBoundArray();
				const Mat34V* matrices = (Mat34V*)pBoundComposite->GetCurrentMatrices();
#			endif
			PipelinedDmaGetter<DECAL_LARGEST_BOUNDS_TYPE,2> childBoundGetter(DMATAG_GET_PHBOUND0);
			childBoundGetter.Prefetch(static_cast<const DECAL_LARGEST_BOUNDS_TYPE*>(childBounds[0]));
			unsigned remainingBounds = numBounds;
			SPU_ONLY(sysDmaWaitTagStatusAll(1<<DMATAG_GET_PHBOUNDPTRS));
			while (remainingBounds)
			{
				const unsigned numBoundsThisLoop = numBoundsNextLoop;
				decalAssert(remainingBounds >= numBoundsThisLoop);
				remainingBounds -= numBoundsThisLoop;
				if (Likely(remainingBounds))
				{
					numBoundsNextLoop = Min(remainingBounds, (unsigned)NUM_CHILDREN_PER_LOOP);
#					if __SPU
						const unsigned nextBufIdx = bufIdx^1;
						sysDmaGet(childBoundsDblBuf[nextBufIdx], (u32)childBoundsEa&~15, (((u32)childBoundsEa&15)+numBoundsNextLoop*sizeof(void*)+15)&~15, DMATAG_GET_PHBOUNDPTRS);
						sysDmaGet(matricesDblBuf[nextBufIdx], (u32)matricesEa, numBoundsNextLoop*sizeof(Mat34V), DMATAG_GET_PHBOUNDPTRS);
#					endif
				}

				for (unsigned i=0; i<numBoundsThisLoop; ++i)
				{
					int cid = numBounds-remainingBounds-numBoundsThisLoop+i;

					// prefetch child bound for next loop
#					if !__SPU
						if (Likely(i+1<numBoundsThisLoop+remainingBounds))
						{
							childBoundGetter.Prefetch(static_cast<const DECAL_LARGEST_BOUNDS_TYPE*>(childBounds[i+1]));
						}
#					else
						if (Likely(i+1<numBoundsThisLoop))
						{
							childBoundGetter.Prefetch(static_cast<const DECAL_LARGEST_BOUNDS_TYPE*>(childBounds[i+1]));
						}
						else
						{
							decalAssert(i+1 == numBoundsThisLoop);
							if (Likely(remainingBounds))
							{
								sysDmaWaitTagStatusAll(1<<DMATAG_GET_PHBOUNDPTRS);
								childBoundGetter.Prefetch(static_cast<const DECAL_LARGEST_BOUNDS_TYPE*>(childBoundsDblBuf[bufIdx^1][((u32)childBoundsEa&15)>>2]));
							}
						}
#					endif

					// reject any bounds based on the lod system
					if (taskDescLs->settings.user.lodSettings.lodAttachMatrixId>-1 && taskDescLs->settings.user.lodSettings.lodAttachMatrixId!=(int)cid)
					{
						childBoundGetter.Skip();
						continue;
					}

					// reject any child bounds that are low lod (game 'mover' collision)
#if __BANK
					if (m_debug.m_disableBoundTypeTest==false)
#endif
					{
						if (typeAndIncludeFlags && !g_pDecalCallbacks->ShouldProcessCompositeChildBoundFlags(phBoundComposite::GetTypeFlags(typeAndIncludeFlags, cid)))
						{
							childBoundGetter.Skip();
							continue;
						}
					}

					const phBound *const pChildBound = childBoundGetter.Get();
					if (pChildBound)
					{
						decalAssertf(pChildBound->GetType()!=phBound::COMPOSITE, "composite child bound found (%s)", debugName);
						Mat34V newBoundWldMtx;
						Transform(newBoundWldMtx, boundWldMtx, matrices[i]);

						ProcessBound(DECAL_DEBUG_NAME_ONLY(debugName,) taskDescLs, taskDescEa, typeSettings, renderSettings, posScale, invPosScale, pEntity, pPhInst, pChildBound, newBoundWldMtx, (s16)cid, isBreakable, isBrokenBreakable, miniIdxBufWriter);
					}
				}

#				if !__SPU
					childBounds += numBoundsThisLoop;
					matrices    += numBoundsThisLoop;
#				else
					bufIdx ^= 1;
					childBounds = (const phBound**)((u32)childBoundsDblBuf[bufIdx] + ((u32)childBoundsEa&15));
					matrices = matricesDblBuf[bufIdx];
					childBoundsEa += numBoundsNextLoop;
					matricesEa    += numBoundsNextLoop;
#				endif
			}
		}

		// non-composite bound
		else
		{
			decalAttachType attachType = DECAL_ATTACH_TYPE_COMPONENT;
#			if DECAL_PROCESS_BREAKABLES
				if (isBreakable)
				{
					if (isBrokenBreakable)
					{
						attachType = DECAL_ATTACH_TYPE_BREAKABLE_BROKEN;
					}
					else
					{
						attachType = DECAL_ATTACH_TYPE_BREAKABLE_UNBROKEN;
					}
				}
#			endif

			// check if this bound intersects with the decal box
			if (CheckCullBox(&taskDescLs->settings, pBound, boundWldMtx))
			{
				// get some insts to store the clipped triangle in
				decalInst* pInstGlassEa = NULL;
				decalInst* pInstNonGlassEa = NULL;
				if (GetInsts(&pInstGlassEa, &pInstNonGlassEa))
				{
					Mat34V boundLocMtx = boundWldMtx;

					Vec3V offset(V_ZERO);
					if (boundType == phBound::BVH || boundType == phBound::GEOMETRY)
					{
#						if COMPRESSED_VERTEX_METHOD == 0
							offset = pBound->GetCentroidOffset();
#						else
#							if !__SPU
								decalAssert(dynamic_cast<const phBoundPolyhedron*>(pBound));
#							endif
							offset = static_cast<const phBoundPolyhedron*>(pBound)->GetLocalSpaceOffset();
#						endif
					}
					else if (boundType == phBound::BOX)
					{
						// leave offset as the default of zero
					}

					decalAssert(IsFiniteAll(offset));
					boundLocMtx.GetCol3Ref() += Transform3x3(boundWldMtx, offset);

					// initialise the insts
#					if !__SPU
						decalInst* const pInstGlassLs    = pInstGlassEa;
						decalInst* const pInstNonGlassLs = pInstNonGlassEa;
#					else
						char pInstGlassBuf   [DMA_BUF_SIZE_TYPE(decalInst, 16)] ;
						char pInstNonGlassBuf[DMA_BUF_SIZE_TYPE(decalInst, 16)] ;
						decalInst* const pInstGlassLs    = rage_placement_new (pInstGlassBuf   +((u32)pInstGlassEa   &15)) decalInst;
						decalInst* const pInstNonGlassLs = rage_placement_new (pInstNonGlassBuf+((u32)pInstNonGlassEa&15)) decalInst;
#					endif
					taskDescLs->settings.CalcLocalInternalSettings(boundLocMtx, pInstGlassLs, pInstNonGlassLs, attachType);
					pInstGlassLs->Init(taskDescLs->settings);
					pInstNonGlassLs->Init(taskDescLs->settings);
					const Vec3V msOffsetInt = CalcModelSpaceOffsetInt(&taskDescLs->settings);
					const Vec3V msOffsetFlt = msOffsetInt * GetModelSpaceOffsetUnits();

					if (boundType == phBound::BVH)
					{
						const phBoundBVH* pBoundBvh = static_cast<const phBoundBVH*>(pBound);
#						if __SPU
							DMA_GET(phOptimizedBvh, bvh, pBoundBvh->GetBVH(), DMATAG_GET_PHBOUND0);
							const_cast<phBoundBVH*>(pBoundBvh)->SetBVH(const_cast<phOptimizedBvh*>(bvh));
#						endif

						// cull the bound with a sphere around the decal box
						phBoundCuller culler;
						phPolygon::Index culledPolys[1024];
						culler.SetArrays(culledPolys, NELEM(culledPolys));
						Mat34V invWldMtx;
						InvertTransformOrtho(invWldMtx, boundWldMtx);
						Vec3V cullPos = Transform(invWldMtx, taskDescLs->settings.user.instSettings.vPosition);
						SPU_ONLY(sysDmaWaitTagStatusAll(1<<DMATAG_GET_PHBOUND0);)
						pBoundBvh->CullSpherePolys(culler, cullPos, decalHelper::GetRadius(taskDescLs->settings.user.instSettings.vDimensions));

						// go through this bound's culled polys
						const unsigned numActivePolys = culler.GetNumCulledPolygons();
#if __ASSERT
						if (numActivePolys == NELEM(culledPolys))
						{
							decalWarningf("Limit of culled polygons per bound reached (%d), some triangles might be missing!", int(numActivePolys));
						}
#endif
						if (Likely(numActivePolys))
						{
							const phPolygon *const polygonEa = pBoundBvh->GetPolygonPointer();
							PipelinedDmaGetter<phPolygon, 2> polygonGetter(DMATAG_GET_PHPOLY0);
							polygonGetter.Prefetch(polygonEa+culler.GetCulledPolygonIndex(0));

#							if COMPRESSED_VERTEX_METHOD == 0
								const Vec3V *const pVertices = pBoundBvh->GetVertexPointer();
								const unsigned stride = 16;
#							else // COMPRESSED_VERTEX_METHOD > 0
								const CompressedVertexType *const pVertices = pBoundBvh->GetCompressedVertexPointer();
								const unsigned stride = sizeof(*pVertices)*3;
#							endif
							const bool initialPrefetch = false;
							decalVertexCache vertexCache(pVertices, stride, DMATAG_GET_VTXBUF0, initialPrefetch);

							for (unsigned i=0; i<numActivePolys; ++i)
							{
								if(Likely(i+1<numActivePolys))
								{
									polygonGetter.Prefetch(polygonEa+culler.GetCulledPolygonIndex(i+1));
								}

								const phPolygon* const currPolygon = polygonGetter.Get();
								const phPolygon::Index polygonIndex = culler.GetCulledPolygonIndex(i);

#								if DECAL_PROCESS_BREAKABLES
									// if this is a broken breakable then only process the attached polys
									if (isBrokenBreakable)
									{
#									if __SPU
										// Atm, the task descriptor won't be
										// reporting a large enough phInst size
										// to grab all of the phGlassInst.  Not
										// really a problem yet while only
										// DECAL_DO_PROCESSBOUND tasks are
										// supported.
										decalAssertf(0,"TODO: GLASS NOT YET SUPPORTED");
										continue;
#									else
#if BREAKABLE_GLASS_USE_BVH
										decalAssert(dynamic_cast<const phGlassInst*>(pPhInst));
										if (!static_cast<const phGlassInst*>(pPhInst)->GetIsElementActive(polygonIndex))
										{
											continue;
										}
#endif // BREAKABLE_GLASS_USE_BVH
#									endif
									}
#								endif

								ProcessBoundPoly(DECAL_DEBUG_NAME_ONLY(debugName,) &taskDescLs->settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, pBoundBvh, &vertexCache, currPolygon, polygonIndex, isBrokenBreakable, miniIdxBufWriter);
							}
						}
					}
					else if (boundType == phBound::GEOMETRY)
					{
						// go through this bound's polys
						const phBoundGeometry* pBoundPolyhedron = static_cast<const phBoundGeometry*>(pBound);
						const phPolygon *const polygonEa = pBoundPolyhedron->GetPolygonPointer();
						PipelinedDmaGetter<phPolygon, 2> polygonGetter(DMATAG_GET_PHPOLY0);
						polygonGetter.Prefetch(polygonEa);
#						if COMPRESSED_VERTEX_METHOD == 0
							const Vec3V *const pVertices = pBoundPolyhedron->GetVertexPointer();
							const unsigned stride = 16;
#						else // COMPRESSED_VERTEX_METHOD > 0
							const CompressedVertexType *const pVertices = pBoundPolyhedron->GetCompressedVertexPointer();
							const unsigned stride = sizeof(*pVertices)*3;
#						endif
						const bool initialPrefetch = false;
						decalVertexCache vertexCache(pVertices, stride, DMATAG_GET_VTXBUF0, initialPrefetch);
						const unsigned numPolygons = pBoundPolyhedron->GetNumPolygons();
						for (unsigned i=0; i<numPolygons; ++i)
						{
							if(Likely(i+1<numPolygons))
							{
								polygonGetter.Prefetch(polygonEa+i+1);
							}
							const phPolygon* const currPolygon = polygonGetter.Get();
							ProcessBoundPoly(DECAL_DEBUG_NAME_ONLY(debugName,) &taskDescLs->settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, pBoundPolyhedron, &vertexCache, currPolygon, i, isBrokenBreakable, miniIdxBufWriter);
						}
					}
					else if (boundType == phBound::BOX)
					{
						const phBoundBox* pBoundBox = static_cast<const phBoundBox*>(pBound);

						// check if the poly material should have decals
						const phMaterialMgr::Id packedMtlId = pBoundBox->GetMaterialId(0);

						// check if the poly material is glass and respect the settings passed in
						const bool isGlassMtl = g_pDecalCallbacks->IsGlassMaterial(packedMtlId);
						const bool processMtl
							 = (!taskDescLs->settings.user.pipelineSettings.onlyApplyToGlass ||  isGlassMtl)
							&& (!taskDescLs->settings.user.pipelineSettings.dontApplyToGlass || !isGlassMtl);

						if (processMtl && (BANK_ONLY(m_debug.m_disableMaterialTest ||) g_pDecalCallbacks->ShouldProcessMaterial(taskDescLs->settings.user.bucketSettings.typeSettingsIndex, packedMtlId)))
						{
							Vec3V vBoxMax = pBoundBox->GetBoundingBoxMax();
							Vec3V vBoxMin = pBoundBox->GetBoundingBoxMin();
							Vec3V vCentrePos = Vec3V(V_ZERO);//pBoundBox->GetCentroidOffset();

							ScalarV vMinX = SplatX(vBoxMin);
							ScalarV vMinY = SplatY(vBoxMin);
							ScalarV vMinZ = SplatZ(vBoxMin);
							ScalarV vMaxX = SplatX(vBoxMax);
							ScalarV vMaxY = SplatY(vBoxMax);
							ScalarV vMaxZ = SplatZ(vBoxMax);

							Vec3V vRight   = Vec3V(V_X_AXIS_WZERO);
							Vec3V vForward = Vec3V(V_Y_AXIS_WZERO);
							Vec3V vUp      = Vec3V(V_Z_AXIS_WZERO);

							Vec3V vBoxVerts[8];
							vBoxVerts[0] = vCentrePos + vRight*vMinX + vForward*vMinY + vUp*vMinZ;
							vBoxVerts[1] = vCentrePos + vRight*vMaxX + vForward*vMinY + vUp*vMinZ;
							vBoxVerts[2] = vCentrePos + vRight*vMaxX + vForward*vMaxY + vUp*vMinZ;
							vBoxVerts[3] = vCentrePos + vRight*vMinX + vForward*vMaxY + vUp*vMinZ;
							vBoxVerts[4] = vCentrePos + vRight*vMinX + vForward*vMinY + vUp*vMaxZ;
							vBoxVerts[5] = vCentrePos + vRight*vMaxX + vForward*vMinY + vUp*vMaxZ;
							vBoxVerts[6] = vCentrePos + vRight*vMaxX + vForward*vMaxY + vUp*vMaxZ;
							vBoxVerts[7] = vCentrePos + vRight*vMinX + vForward*vMaxY + vUp*vMaxZ;

							static const int boxTris[12][3] = {{0, 2, 1},
							{0, 3, 2},
							{1, 6, 5},
							{1, 2, 6},
							{4, 3, 0},
							{4, 7, 3},
							{4, 1, 5},
							{4, 0, 1},
							{3, 6, 2},
							{3, 7, 6},
							{7, 5, 6},
							{7, 4, 5}};

							ScalarV vehDmgScale(V_ZERO);

							for (int i=0; i<12; i++)
							{
								// calc the normal of this poly
								Vec3V vVtx0 = vBoxVerts[boxTris[i][0]];
								Vec3V vVtx1 = vBoxVerts[boxTris[i][1]];
								Vec3V vVtx2 = vBoxVerts[boxTris[i][2]];

								Vec3V vPolyNormal = Cross(vVtx1-vVtx0, vVtx2-vVtx0);
								vPolyNormal = Normalize(vPolyNormal);

								ProcessTri(&taskDescLs->settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVtx0, vVtx1, vVtx2, vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
							}
						}
					}

#					if __BANK && !__SPU
						// add the processed bound box (red)
						m_debug.StoreLastDecalBox(boundWldMtx, pBound->GetBoundingBoxMin(), pBound->GetBoundingBoxMax(), Color32(1.0f, 0.0f, 0.0f, 1.0f));
#					endif

					// add the instances to buckets
					if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, &taskDescLs->settings, posScale, msOffsetInt, pInstGlassLs,    pInstGlassEa,    boundLocMtx, childId, true,  false SPU_ONLY(, miniIdxBufWriter)))
					{
						SPU_ONLY(DmaPutAlignedButOddSize(pInstGlassLs,    (u32)pInstGlassEa,    DMATAG_BLOCKING);)
					}
					if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, &taskDescLs->settings, posScale, msOffsetInt, pInstNonGlassLs, pInstNonGlassEa, boundLocMtx, childId, false, false SPU_ONLY(, miniIdxBufWriter)))
					{
						SPU_ONLY(DmaPutAlignedButOddSize(pInstNonGlassLs, (u32)pInstNonGlassEa, DMATAG_BLOCKING);)
					}
					SPU_ONLY(sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);)
				}
			}
#		if __BANK && !__SPU
			else
			{
				// add the non-processed bound box (red)
				m_debug.StoreLastDecalBox(boundWldMtx, pBound->GetBoundingBoxMin(), pBound->GetBoundingBoxMax(), Color32(1.0f, 0.0f, 0.0f, 0.1f));
			}
#		endif
		}
	}


	///////////////////////////////////////////////////////////////////////////
	//  ProcessBoundPoly
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessBoundPoly(DECAL_DEBUG_NAME_ONLY(const char *debugName,) const decalSettings* settings, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, const phBoundGeometry* polyhedron, decalVertexCache* vertexCache, const phPolygon* polygon, unsigned polygonIndex, bool processDoubleSided, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		// should we reject this poly?
#		if POLY_MAX_VERTICES==3
#			if !__SPU
				const phMaterialIndex boundMtlId = polyhedron->GetPolygonMaterialIndex(polygonIndex);
#			else
				CompileTimeAssert(sizeof(__typeof__(*polyhedron->GetPolygonMaterialIndexArray())) == 1);
				const phMaterialIndex boundMtlId = sysDmaGetUInt8((uptr)(polyhedron->GetPolygonMaterialIndexArray()+polygonIndex), DMATAG_BLOCKING);
#			endif
#		else
			const phMaterialIndex boundMtlId = polygon->GetMaterialIndex();
#		endif

#		if !__SPU
			const phMaterialMgr::Id packedMtlId = polyhedron->GetMaterialId(boundMtlId);
#		elif PH_MATERIAL_ID_64BIT
			CompileTimeAssert(sizeof(__typeof__(*polyhedron->GetMaterialIdArray())) == 8);
			const phMaterialMgr::Id packedMtlId = sysDmaGetUInt64((uptr)(polyhedron->GetMaterialIdArray()+boundMtlId), DMATAG_BLOCKING);
#		else
			CompileTimeAssert(sizeof(__typeof__(*polyhedron->GetMaterialIdArray())) == 4);
			const phMaterialMgr::Id packedMtlId = sysDmaGetUInt32((uptr)(polyhedron->GetMaterialIdArray()+boundMtlId), DMATAG_BLOCKING);
#		endif

		// if an exclusive material is specified then return if this is any other material
		phMaterialMgr::Id unpackedMtlId = packedMtlId&0xff;
		if (settings->user.pipelineSettings.exclusiveMtlId!=(phMaterialMgr::Id)(-1) && (settings->user.pipelineSettings.exclusiveMtlId!=unpackedMtlId))
		{
			return;
		}

		// check if the poly material should have decals
		if (BANK_ONLY(!m_debug.m_disableMaterialTest &&) !g_pDecalCallbacks->ShouldProcessMaterial(settings->user.bucketSettings.typeSettingsIndex, packedMtlId))
		{
			return;
		}

		// check if the poly material is glass and respect the settings passed in
		bool isGlassMtl = g_pDecalCallbacks->IsGlassMaterial(packedMtlId);
		if ((settings->user.pipelineSettings.onlyApplyToGlass && !isGlassMtl) ||
			(settings->user.pipelineSettings.dontApplyToGlass &&  isGlassMtl))
		{
			return;
		}

		// check the type of polygon we have
		if (polyhedron->GetType()==phBound::BVH)
		{
			const phPrimitive& bvhPrimitive = polygon->GetPrimitive();
			if (bvhPrimitive.GetType() == PRIM_TYPE_POLYGON)
			{
				// get the verts of the poly
				const unsigned idx0 = polygon->GetVertexIndex(0);
				const unsigned idx1 = polygon->GetVertexIndex(1);
				const unsigned idx2 = polygon->GetVertexIndex(2);
				vertexCache->Prefetch(idx0);
				vertexCache->Prefetch(idx1);
				vertexCache->Prefetch(idx2);
				const void *const pVtx0 = vertexCache->Get(idx0);
				const void *const pVtx1 = vertexCache->Get(idx1);
				const void *const pVtx2 = vertexCache->Get(idx2);
#				if COMPRESSED_VERTEX_METHOD == 0
					const Vec3V vVtx0 = *(Vec3V*)pVtx0 - polyhedron->GetCentroidOffset();
					const Vec3V vVtx1 = *(Vec3V*)pVtx1 - polyhedron->GetCentroidOffset();
					const Vec3V vVtx2 = *(Vec3V*)pVtx2 - polyhedron->GetCentroidOffset();
#				else // COMPRESSED_VERTEX_METHOD > 0
					const Vec3V vVtx0 = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx0);
					const Vec3V vVtx1 = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx1);
					const Vec3V vVtx2 = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx2);
#				endif

				// calc the poly normal
				Vec3V vPolyNormal = Cross(vVtx1-vVtx0, vVtx2-vVtx0);
				vPolyNormal = Normalize(vPolyNormal);

				ScalarV vehDmgScale(V_ZERO);

				// process the triangle
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVtx0, vVtx1, vVtx2, vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				if (processDoubleSided)
				{
					ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVtx0, vVtx2, vVtx1, -vPolyNormal, -vPolyNormal, -vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				}
			}
			else if (bvhPrimitive.GetType() == PRIM_TYPE_BOX)
			{
				const phPrimBox& bvhBoxPrim = bvhPrimitive.GetBox();

				//			2______6
				//		   /.	  /|
				//		  /	.	 / |
				//		4/_____0/  |
				//		|	.	|  |
				//		|   7...|..3
				//		|  .	| /
				//		| .		|/
				//		1-------5

				// store vertex info (transforming into world space)
				Vec3V vVerts[4];
				const unsigned idx0 = bvhBoxPrim.GetVertexIndex(0);
				const unsigned idx1 = bvhBoxPrim.GetVertexIndex(1);
				const unsigned idx2 = bvhBoxPrim.GetVertexIndex(2);
				const unsigned idx3 = bvhBoxPrim.GetVertexIndex(3);
				vertexCache->Prefetch(idx0);
				vertexCache->Prefetch(idx1);
				vertexCache->Prefetch(idx2);
				vertexCache->Prefetch(idx3);
				const void *const pVtx0 = vertexCache->Get(idx0);
				const void *const pVtx1 = vertexCache->Get(idx1);
				const void *const pVtx2 = vertexCache->Get(idx2);
				const void *const pVtx3 = vertexCache->Get(idx3);
#				if COMPRESSED_VERTEX_METHOD == 0
					vVerts[0] = *(Vec3V*)pVtx0 - polyhedron->GetCentroidOffset();
					vVerts[1] = *(Vec3V*)pVtx1 - polyhedron->GetCentroidOffset();
					vVerts[2] = *(Vec3V*)pVtx2 - polyhedron->GetCentroidOffset();
					vVerts[3] = *(Vec3V*)pVtx3 - polyhedron->GetCentroidOffset();
#				else // COMPRESSED_VERTEX_METHOD > 0
					vVerts[0] = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx0);
					vVerts[1] = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx1);
					vVerts[2] = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx2);
					vVerts[3] = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx3);
#				endif

				Mat34V vBoxMtxLcl;
				Vec3V vBoxSize;
				ScalarV vBoxMaxMargin;
				geomBoxes::ComputeBoxDataFromOppositeDiagonals(vVerts[0], vVerts[1], vVerts[2], vVerts[3], vBoxMtxLcl, vBoxSize, vBoxMaxMargin);

				Vec3V vRight = vBoxMtxLcl.GetCol0() * vBoxSize.GetX() * ScalarV(V_HALF);
				Vec3V vForward = vBoxMtxLcl.GetCol1() * vBoxSize.GetY() * ScalarV(V_HALF);
				Vec3V vUp = vBoxMtxLcl.GetCol2() * vBoxSize.GetZ() * ScalarV(V_HALF);
				Vec3V vCentre = vBoxMtxLcl.GetCol3();

				Vec3V vVertsLcl[8];
				vVertsLcl[0] = vCentre + vRight - vForward + vUp;
				vVertsLcl[1] = vCentre - vRight - vForward - vUp;
				vVertsLcl[2] = vCentre - vRight + vForward + vUp;
				vVertsLcl[3] = vCentre + vRight + vForward - vUp;
				vVertsLcl[4] = vCentre - vRight - vForward + vUp;
				vVertsLcl[5] = vCentre + vRight - vForward - vUp;
				vVertsLcl[6] = vCentre + vRight + vForward + vUp;
				vVertsLcl[7] = vCentre - vRight + vForward - vUp;

				// process the triangles
				Vec3V vPolyNormal = Cross(vVertsLcl[7]-vVertsLcl[3], vVertsLcl[6]-vVertsLcl[3]);
				vPolyNormal = Normalize(vPolyNormal);
				ScalarV vehDmgScale(V_ZERO);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[2], vVertsLcl[3], vVertsLcl[7], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[2], vVertsLcl[6], vVertsLcl[3], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);

				vPolyNormal = Cross(vVertsLcl[2]-vVertsLcl[6], vVertsLcl[0]-vVertsLcl[6]);
				vPolyNormal = Normalize(vPolyNormal);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[4], vVertsLcl[6], vVertsLcl[2], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[4], vVertsLcl[0], vVertsLcl[6], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);

				vPolyNormal = Cross(vVertsLcl[4]-vVertsLcl[0], vVertsLcl[5]-vVertsLcl[0]);
				vPolyNormal = Normalize(vPolyNormal);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[1], vVertsLcl[0], vVertsLcl[4], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[1], vVertsLcl[5], vVertsLcl[0], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);

				vPolyNormal = Cross(vVertsLcl[1]-vVertsLcl[5], vVertsLcl[3]-vVertsLcl[5]);
				vPolyNormal = Normalize(vPolyNormal);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[7], vVertsLcl[5], vVertsLcl[1], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[7], vVertsLcl[3], vVertsLcl[5], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);

				vPolyNormal = Cross(vVertsLcl[7]-vVertsLcl[2], vVertsLcl[4]-vVertsLcl[2]);
				vPolyNormal = Normalize(vPolyNormal);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[1], vVertsLcl[2], vVertsLcl[7], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[1], vVertsLcl[4], vVertsLcl[2], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);

				vPolyNormal = Cross(vVertsLcl[6]-vVertsLcl[3], vVertsLcl[5]-vVertsLcl[3]);
				vPolyNormal = Normalize(vPolyNormal);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[0], vVertsLcl[3], vVertsLcl[6], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVertsLcl[0], vVertsLcl[5], vVertsLcl[3], vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
			}
#if DECAL_DEBUG_NAME
			else if (bvhPrimitive.GetType() == PRIM_TYPE_SPHERE)
			{
				decalDebugf1("decalManager::ProcessBoundPoly - %s has sphere primitive - decals will not be applied", debugName);
			}
			else if (bvhPrimitive.GetType() == PRIM_TYPE_CAPSULE)
			{
				decalDebugf1("decalManager::ProcessBoundPoly - %s has capsule primitive - decals will not be applied", debugName);
			}
			else if (bvhPrimitive.GetType() == PRIM_TYPE_CYLINDER)
			{
				decalDebugf1("decalManager::ProcessBoundPoly - %s has cylinder primitive - decals will not be applied", debugName);
			}
			else
			{
				decalDebugf1("decalManager::ProcessBoundPoly - %s has unknown primitive (%d) - decals will not be applied", debugName, bvhPrimitive.GetType());
			}
#endif // DECAL_DEBUG_NAME
		}
		else if (polyhedron->GetType()==phBound::GEOMETRY)
		{
			// get the verts of the poly
			const unsigned idx0 = polygon->GetVertexIndex(0);
			const unsigned idx1 = polygon->GetVertexIndex(1);
			const unsigned idx2 = polygon->GetVertexIndex(2);
			vertexCache->Prefetch(idx0);
			vertexCache->Prefetch(idx1);
			vertexCache->Prefetch(idx2);
			const void *const pVtx0 = vertexCache->Get(idx0);
			const void *const pVtx1 = vertexCache->Get(idx1);
			const void *const pVtx2 = vertexCache->Get(idx2);
#			if COMPRESSED_VERTEX_METHOD == 0
				const Vec3V vVtx0 = *(Vec3V*)pVtx0 - polyhedron->GetCentroidOffset();
				const Vec3V vVtx1 = *(Vec3V*)pVtx1 - polyhedron->GetCentroidOffset();
				const Vec3V vVtx2 = *(Vec3V*)pVtx2 - polyhedron->GetCentroidOffset();
#			else // COMPRESSED_VERTEX_METHOD > 0
				const Vec3V vVtx0 = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx0);
				const Vec3V vVtx1 = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx1);
				const Vec3V vVtx2 = polyhedron->DecompressVertexLocalSpace((const CompressedVertexType*)pVtx2);
#			endif

			// calc the poly normal
			Vec3V vPolyNormal = Cross(vVtx1-vVtx0, vVtx2-vVtx0);
			vPolyNormal = Normalize(vPolyNormal);

			ScalarV vehDmgScale(V_ZERO);

			// process the triangle
			ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVtx0, vVtx1, vVtx2, vPolyNormal, vPolyNormal, vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
			if (processDoubleSided)
			{
				ProcessTri(settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, vVtx0, vVtx2, vVtx1, -vPolyNormal, -vPolyNormal, -vPolyNormal, vehDmgScale, vehDmgScale, vehDmgScale, isGlassMtl, miniIdxBufWriter);
			}
		}
		else
		{
			decalAssertf(0, "decalManager::ProcessBoundPoly encountered an unsupported bound (%s has type %d)", debugName, polyhedron->GetType());
		}
	}

#endif // DECAL_MANAGER_ENABLE_PROCESSBOUND || DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS


#if DECAL_MANAGER_ENABLE_PROCESSSMASHGROUP

	///////////////////////////////////////////////////////////////////////////
	//  ProcessSmashGroup
	///////////////////////////////////////////////////////////////////////////

	void decalManagerAsync::ProcessSmashGroup(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, decalMiniIdxBufWriter* miniIdxBufWriter)
	{
		DECAL_DEBUG_NAME_ONLY((void)debugName;)

		decalInst *const pInstGlassEa = m_pools.GetInst(SPU_ONLY(&m_thisEa->m_pools, DMATAG_BLOCKING));
		decalInst *const pInstNonGlass = NULL;
		if (!pInstGlassEa)
		{
			decalSpamf("Ran out of decal instances");
			return;
		}

		// initialise the insts
#		if !__SPU
			decalInst* const pInstGlassLs = pInstGlassEa;
#		else
			char pInstGlassBuf[DMA_BUF_SIZE_TYPE(decalInst, 16)] ;
			decalInst* const pInstGlassLs = rage_placement_new (pInstGlassBuf+((u32)pInstGlassEa&15)) decalInst;
#		endif
		const Mat34V wldMtx = taskDescLs->entityMtx;
		taskDescLs->settings.CalcLocalInternalSettings(wldMtx, pInstGlassLs, pInstNonGlass, DECAL_ATTACH_TYPE_SMASHGROUP);
		taskDescLs->settings.internal.isPersistent = false;
		pInstGlassLs->Init(taskDescLs->settings);

		const Vec3V msOffsetInt = CalcModelSpaceOffsetInt(&taskDescLs->settings);
		const Vec3V msOffsetFlt = msOffsetInt * GetModelSpaceOffsetUnits();

		const ScalarV vehDmgScale(V_ZERO);
		const bool isGlass = true;

		Vec3V pos[decalCallbacks::MAX_SMASH_GROUP_VERTS_PER_BATCH];
		Vec3V nrm[decalCallbacks::MAX_SMASH_GROUP_VERTS_PER_BATCH];
		unsigned batchNumVtxs;
		while ((batchNumVtxs = g_pDecalCallbacks->DecompressSmashGroup(taskDescLs, pos, nrm)) != 0)
		{
			for (unsigned i=0; i<batchNumVtxs; i+=3)
			{
				ProcessTri(&taskDescLs->settings, typeSettings, renderSettings, posScale, invPosScale, msOffsetFlt, pos[i], pos[i+1], pos[i+2], nrm[i], nrm[i+1], nrm[i+2], vehDmgScale, vehDmgScale, vehDmgScale, isGlass, miniIdxBufWriter);
			}
		}

		// add the inst to a bucket
		const bool isDamaged = false;
		if (AddInstToBucket(taskDescLs, taskDescEa, taskDescLs->bucketLists, &taskDescLs->settings, posScale, msOffsetInt, pInstGlassLs, pInstGlassEa, wldMtx, taskDescLs->settings.user.pipelineSettings.colnComponentId, isGlass, isDamaged SPU_ONLY(, miniIdxBufWriter)))
		{
#			if __SPU
				DmaPutAlignedButOddSize(pInstGlassLs, (u32)pInstGlassEa, DMATAG_BLOCKING);
				sysDmaWaitTagStatusAll(1<<DMATAG_BLOCKING);
#			endif
		}
	}


#endif // DECAL_MANAGER_ENABLE_PROCESSSMASHGROUP


///////////////////////////////////////////////////////////////////////////////
//  ProcessTri
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::ProcessTri(const decalSettings* settings, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, Vec3V_In vtx1, Vec3V_In vtx2, Vec3V_In vtx3, Vec3V_In nrm1, Vec3V_In nrm2, Vec3V_In nrm3, ScalarV_In vehDmgScale1, ScalarV_In vehDmgScale2, ScalarV_In vehDmgScale3, bool isGlass, decalMiniIdxBufWriter* miniIdxBufWriter)
{
#	if __BANK && !__SPU
		// increment the number of considered polys
		m_debug.m_numPolysConsideredThisProjection++;

		// render the considered polys (yellow)
		if (m_debug.m_renderPolysConsidered)
		{
			Vec3V vDebugVtx1 = Transform(settings->internal.parentMtx, vtx1);
			Vec3V vDebugVtx2 = Transform(settings->internal.parentMtx, vtx2);
			Vec3V vDebugVtx3 = Transform(settings->internal.parentMtx, vtx3);
			grcDebugDraw::Poly(vDebugVtx1, vDebugVtx2, vDebugVtx3, Color32(1.0f, 1.0f, 0.0f, 1.0f), true, false, m_debug.m_renderTimer);
		}
#	endif

	// store the verts
	Vec3V vVerts[DECAL_MAX_CLIPPED_VERTS];
	vVerts[0] = vtx1;
	vVerts[1] = vtx2;
	vVerts[2] = vtx3;

	// calc this triangles normal
	Vec3V vFaceNormal = Cross(vVerts[1]-vVerts[0], vVerts[2]-vVerts[0]);
	vFaceNormal = Normalize(vFaceNormal);

	// check if this tri needs rejected by checking its face normal
#if __BANK
	if (!m_debug.m_disablePolyAngleTest)
#endif
	{
		float dot = Dot(-vFaceNormal, settings->internal.vLclDirection).Getf();
		if (dot<typeSettings->polyRejectThreshold)
		{
			return;
		}
	}

#	if __BANK && !__SPU
		// increment the number of sent to clip polys
		m_debug.m_numPolysSentToClipThisProjection++;

		// render the sent to clip polys (orange)
		if (m_debug.m_renderPolysSentToClip)
		{
			Vec3V vDebugVtx1 = Transform(settings->internal.parentMtx, vtx1);
			Vec3V vDebugVtx2 = Transform(settings->internal.parentMtx, vtx2);
			Vec3V vDebugVtx3 = Transform(settings->internal.parentMtx, vtx3);
			grcDebugDraw::Poly(vDebugVtx1, vDebugVtx2, vDebugVtx3, Color32(1.0f, 0.5f, 0.0f, 1.0f), true, false, m_debug.m_renderTimer);
		}
#	endif

	// store the normals and vehicle damage scales
	Vec3V vNorms[DECAL_MAX_CLIPPED_VERTS];
	vNorms[0] = nrm1;
	vNorms[1] = nrm2;
	vNorms[2] = nrm3;
	ScalarV vVehDmgScales[DECAL_MAX_CLIPPED_VERTS];
	vVehDmgScales[0] = vehDmgScale1;
	vVehDmgScales[1] = vehDmgScale2;
	vVehDmgScales[2] = vehDmgScale3;

	// clip against the planes
	int numVerts = 3;
	settings->internal.lclClipper.ClipPoly(numVerts, &vVerts[0], &vNorms[0], &vVehDmgScales[0]);
	if (numVerts==0)
	{
		return;
	}

#	if __BANK
		if (m_debug.m_disableTriClipping)
		{
			numVerts = 3;
			vVerts[0] = vtx1;
			vVerts[1] = vtx2;
			vVerts[2] = vtx3;
		}
#	endif

	ProcessClippedPolygon(settings, renderSettings, posScale, invPosScale, msOffsetFlt, numVerts, vVerts, vNorms, vVehDmgScales, isGlass, miniIdxBufWriter);
}


///////////////////////////////////////////////////////////////////////////////
//  OutputVertex
///////////////////////////////////////////////////////////////////////////////

inline void decalManagerAsync::OutputVertex(decalVtx *pVtxLs SPU_ONLY(, decalVtx *pVtxEa, u32 dmaTag))
{
#	if RSG_DURANGO
#		if _XDK_VER < 9516	// fixed in July 2013 XDK
			// There is an XDK bug with D3DFlushCpuCache() not doing an mfence
			// before the clflush's.  The #error here is so that the
			// D3DFlushCpuCache() can be checked when we update XDK.  The
			// implementation lives in "Find file: c:/Program Files
			// (x86)/Microsoft Durango XDK/xdk/Include/um/d3d11_1.h".  If the
			// mfence is still missing, then bump the version number here so we
			// can check again next XDK.  If it has been added, then this work
			// around can be removed.  Note that the current XDK version can be
			// found in "c:/Program Files (x86)/Microsoft Durango
			// XDK/xdk/Include/shared/xdk.h" See this article for reference:
			// https://forums.xboxlive.com/AnswerPage.aspx?qid=f2fac028-89cc-40d0-aa95-4aad7f1e322b&tgt=1
			_mm_mfence();
#		endif
		D3DFlushCpuCache(pVtxLs, sizeof(*pVtxLs));

#	elif RSG_PC
		// Only update the buffer when postDec is non-negative (ie, the buffer
		// wasn't full).  While it initially looks tempting to just remove the
		// branch, that would be wrong.  Removing the branch would create a race
		// condition that could lead to a one frame glitch... the render thread
		// for the current frame may miss updating a vertex that is about to be
		// renderred because we just wrote over it.  Would all sort it self out
		// next frame since the buffer is full and the render thread would
		// update everything.  But it is still wrong for one frame.
		const s32 postDec = (s32)sysInterlockedDecrement((volatile u32*)&m_gpuUploadWorkerThreadsSpaceRemaining);
		if (postDec >= 0)
		{
			CompileTimeAssert((NELEM(m_gpuUploadIndices) & (NELEM(m_gpuUploadIndices)-1)) == 0);
			const u32 ii = (m_gpuUploadWorkerThreadsSubFrom - postDec) & (NELEM(m_gpuUploadIndices)-1);
			m_gpuUploadIndices[ii] = (u16)(pVtxLs - m_pools.GetVtxPoolBase());
		}

#	elif __SPU
	 	// Dma out the vertex to xdr.
	 	DmaPutAlignedButOddSize(pVtxLs, (u32)pVtxEa, dmaTag);

#	elif RSG_XENON
	 	// The decalVtx's are writen directly in the vertex buffer that the GPU
	 	// renders from.  This memory is cacheable, and the 360 GPU does not
	 	// snoop the CPU cache, so we need to manually store the line out to
	 	// memory.
	 	CompileTimeAssert(sizeof(decalVtx) <= 128);
	 	__dcbst(0,                  pVtxLs);
	 	__dcbst(sizeof(decalVtx)-1, pVtxLs);

#	else
		(void)pVtxLs;
#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessClippedPolygon
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::ProcessClippedPolygon(const decalSettings* settings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, unsigned numVerts, const Vec3V* pVerts, const Vec3V* pNorms, const ScalarV* pVehDmgScales, bool isGlass, decalMiniIdxBufWriter* miniIdxBufWriter)
{
	const ScalarV DOUBLE_SIDED_OFFSET(0.02f);
	const ScalarV DOUBLE_SIDED_OFFSET_VEH(0.005f);

	const bool doubleSided = isGlass;

	// try to get the verts and tris from the pools
	decalAssert(numVerts <= DECAL_MAX_CLIPPED_VERTS);
	decalAssert(numVerts >= 3);
	const unsigned numVertsDS = numVerts << (unsigned)doubleSided;
	const unsigned numTris = numVerts - 2;
	const unsigned numTrisDS = numTris << (unsigned)doubleSided;
	bool decalVtxInListAlready[DECAL_MAX_CLIPPED_VERTS*2];
	decalVtx* pDecalVtxEas[DECAL_MAX_CLIPPED_VERTS*2];

	// initialise the data
	sysMemSet(pDecalVtxEas, 0, sizeof(pDecalVtxEas));
	sysMemSet(decalVtxInListAlready, 0, numVerts);
	sysMemSet(decalVtxInListAlready+numVerts, !doubleSided, numVerts);

	// get the correct inst to store the data in
	decalInst *const pDecalInst = isGlass ? settings->internal.pInstGlass : settings->internal.pInstNonGlass;
	decalAssert(pDecalInst);

	SPU_ONLY(decalPools *const pPoolsEa = &m_thisEa->m_pools;)
	decalMiniIdxBuf *const pMibBase = m_pools.GetMibPoolBase();
	decalVtx        *const pVtxBase = m_pools.GetVtxPoolBase();
	const decalMiniIdxBufSListPtrConverter mibConv(pMibBase);
	// const SListIdxPtrConverter<decalVtx,u16> vtxConv = MakeSListIdxPtrConverter<u16>(pVtxBase);

	// Try to match the input vertices to those already existing in the
	// instance.
	//
	// Note that we cap the number of mibs we search to keep the algorithm
	// linear (otherwise it becomes quadratic complexity and can occasionally
	// become extremely expensive (eg. GTAV B*1479777)).  New mibs are added to
	// the front of the list, so if the input mesh hash been reasonably well
	// vertex cache optimized, limiting the search should miss very few
	// duplicate vertices.
	//
	const Vec3V vPosTolerance(0.001f, 0.001f, 0.001f);
	const ScalarV vNrmTolerance(0.92387950f); // cos(pi/8)
	enum { MIB_SEARCH_LIMIT = 5 };
#	define DEBUG_MEASURE_MIP_SEARCH_LIMIT   0
	int mibSearchLimit = MIB_SEARCH_LIMIT;
#	if DEBUG_MEASURE_MIP_SEARCH_LIMIT
		// Filter output tty through
		//   sed -n 's/.*DEBUG_MEASURE.*=\([0-9]\+\).*=\([0-9]\+\).*=\([0-9]\+\).*/\1 \2 \3/p'
		unsigned mibSearchLimitMissedResults = 0;
		unsigned mibSearchLimitToFindAll = 1;
#	endif
#	if __SPU
		// Ensure all previous indices and vertices for this instance have
		// actually been put to xdr
		sysDmaWaitTagStatusAll((3<<DMATAG_PUT_MINIIDXBUF0) | (7<<DMATAG_PUT_VTX0));
#	endif
	// When running on an spu, the first mib in the list may not actually be in
	// xdr, but cached in local store by the mib writer.  Load the first mib
	// through the writer so that we can get access to the cached copy.  From
	// then on, use a SListConstItor to iterate through the mibs.
	decalMiniIdxBuf* const pHeadMibEa = decalMiniIdxBuf::GetPtr(pMibBase, pDecalInst->m_idxBufList);
	if (pHeadMibEa)
	{
		const decalMiniIdxBuf* pMib = miniIdxBufWriter->LocalPtrLoad(pHeadMibEa);
		SLIST_CONST_ITOR(decalMiniIdxBuf, mibItor, (pMib->GetNext(mibConv), DMATAG_BLOCKING, mibConv));

		decalAssert(pMib->GetNumTris());
		PipelinedDmaAccessor<decalVtx, 2> vtxGetter(DMATAG_GET_VTX0);
		vtxGetter.Prefetch(pVtxBase + pMib->m_idx[0]);
		const decalMiniIdxBuf *pNextMib = pMib;
		do
		{
			pMib = pNextMib;
			const unsigned numIndices = pMib->GetNumTris()*3;
			for (unsigned vi=0; vi<numIndices; ++vi)
			{
				if (vi < numIndices-1)
				{
					vtxGetter.Prefetch(pVtxBase + pMib->m_idx[vi+1]);
					if (vi == numIndices-2)
					{
						// Don't start prefetching ahead until we have read the
						// last of the vertex indices out of the current mini
						// index buffer.  PipelinedDmaGetterEa<>::Ptr() will
						// overwrite the memory pointed to by pMib when running
						// on an SPU.
						pNextMib = mibItor.Ptr();

						// Limit mibs searched to keep cost linear.  Notice that
						// we don't skip the mibItor.Ptr() call since we still
						// should block on the mfc get.
						if (!--mibSearchLimit)
						{
#							if !DEBUG_MEASURE_MIP_SEARCH_LIMIT
								pNextMib = NULL;
#							endif
						}
					}
				}
				else if (pNextMib)
				{
					decalAssert(pNextMib->GetNumTris());
					vtxGetter.Prefetch(pVtxBase + pNextMib->m_idx[0]);
				}

				decalVtx *const pVtx = vtxGetter.Get();
				const Vec3V pos = pVtx->GetPos(posScale, msOffsetFlt);
				const Vec3V nrm = pVtx->GetNormal();
				decalVtx *const ea = (decalVtx*)(vtxGetter.GetEa());
				for (unsigned i=0; i<numVerts; ++i)
				{
					if (IsCloseAll(pos, pVerts[i], vPosTolerance) && IsGreaterThanAll(Dot(nrm, pNorms[i]), vNrmTolerance))
					{
						pDecalVtxEas[i] = ea;
#						if DEBUG_MEASURE_MIP_SEARCH_LIMIT
							if (!decalVtxInListAlready[i])
							{
								mibSearchLimitToFindAll = MIB_SEARCH_LIMIT-mibSearchLimit+1;
								if (mibSearchLimit<0)
								{
									++mibSearchLimitMissedResults;
								}
							}
#						endif
						decalVtxInListAlready[i] = true;
						pVtx->SetNormal(Normalize(Add(nrm, pNorms[i])));
						vtxGetter.Put();
						break;
					}
				}
				if (doubleSided)
				{
					for (unsigned i=0; i<numVerts; ++i)
					{
						const Vec3V n = -pNorms[i];
						const Vec3V p = pVerts[i] + n * (settings->user.pipelineSettings.isVehicleGlass?DOUBLE_SIDED_OFFSET_VEH:DOUBLE_SIDED_OFFSET);
						if (IsCloseAll(pos, p, vPosTolerance) && IsGreaterThanAll(Dot(nrm, n), vNrmTolerance))
						{
							pDecalVtxEas[numVerts+i] = ea;
#							if DEBUG_MEASURE_MIP_SEARCH_LIMIT
								if (!decalVtxInListAlready[numVerts+i])
								{
									mibSearchLimitToFindAll = MIB_SEARCH_LIMIT-mibSearchLimit+1;
									if (mibSearchLimit<0)
									{
										++mibSearchLimitMissedResults;
									}
								}
#							endif
							decalVtxInListAlready[numVerts+i] = true;
							pVtx->SetNormal(Normalize(Add(nrm, n)));
							vtxGetter.Put();
							break;
						}
					}
				}
			}
		}
		while (pNextMib);
	}
#	if DEBUG_MEASURE_MIP_SEARCH_LIMIT
		Displayf("DEBUG_MEASURE_MIP_SEARCH_LIMIT: curr=%u, required=%u, missed=%u\n", MIB_SEARCH_LIMIT, mibSearchLimitToFindAll, mibSearchLimitMissedResults);
#	endif

	bool problemsFound = false;
	for (unsigned i=0; i<numVertsDS; ++i)
	{
		if (!decalVtxInListAlready[i])
		{
			pDecalVtxEas[i] = m_pools.GetVtx(SPU_ONLY(pPoolsEa, DMATAG_BLOCKING));
			if (!pDecalVtxEas[i])
			{
				problemsFound = true;
				break;
			}
		}
	}

	if (!problemsFound)
	{
		problemsFound = !pDecalInst->ReserveTris(numTrisDS, miniIdxBufWriter);
	}

	if (problemsFound)
	{
		// return these to their pools
		for (unsigned i=0; i<numVertsDS; ++i)
		{
			if (pDecalVtxEas[i] && !decalVtxInListAlready[i])
			{
				m_pools.ReturnVtx(pDecalVtxEas[i] SPU_ONLY(, pPoolsEa, DMATAG_BLOCKING));
			}
		}
	}
	else
	{
		// All allocations have succeeded, so record add the newly allocated
		// vertices to the instances linked list.
		u16 *const allocatedVertices = m_allocatedVertices;
		u16 allocatedVerticesHead = pDecalInst->m_vtxList;
		for (unsigned i=0; i<numVertsDS; ++i)
		{
			if (!decalVtxInListAlready[i])
			{
				const u16 idx = (u16)(pDecalVtxEas[i]-pVtxBase);
				decalAssert(allocatedVertices[idx] == 0xffff);
				allocatedVertices[idx] = allocatedVerticesHead;
				allocatedVerticesHead = idx;
			}
		}
		pDecalInst->m_vtxList = allocatedVerticesHead;

#		if __SPU
			// Double buffer output vertices and triangles.  Note that we use
			// static buffers here so that we can leave dmas in flight after
			// leaving function.  Note that while we have four buffers here, we
			// are only double buffering, its just that we can output two
			// vertices at a time when generating double sided decals.
			static char decalVtxBufLs[4][DMA_BUF_SIZE_TYPE(decalVtx,16)] ;
			static unsigned vtxBufIdx/*=0*/;
			decalAssert(vtxBufIdx <= 1); // expecting .bss to be zeroed
#		endif

		unsigned numNewVerts = 0;

		// In order to calculate the tangents per vertex without refetching
		// previous vertices from memory, these variables work as a pipeline to
		// shuffle the previous values through.  Triangles form a fan so the
		// next.*A variables handle forcing vert0 values to the end of the
		// pipeline once the first two verts have been processed.
		decalAssert(numVerts >= 3);
		Vec3V posA = pVerts[1], posB = pVerts[2];
		Vec3V nextPosA = posB;
		Vec2V texCoords0;
		Vec2V texCoordsA, texCoordsB;
		ScalarV frontToBackRatio;
		CalcTexCoords(settings, renderSettings, pVerts[0], texCoords0, frontToBackRatio);
		CalcTexCoords(settings, renderSettings, posA, texCoordsA, frontToBackRatio);
		CalcTexCoords(settings, renderSettings, posB, texCoordsB, frontToBackRatio);
		Vec2V nextTexCoordsA = texCoordsB;

		// Go ahead and set up the data
		for (unsigned i=0; i<numVerts; i++)
		{
			decalAssertf(pDecalVtxEas[i], "ProcessClippedPolygon - invalid index found");

			Vec3V posC = pVerts[i];
			Vec2V texCoordsC;
			CalcTexCoords(settings, renderSettings, posC, texCoordsC, frontToBackRatio);

			const bool notInList0 = !decalVtxInListAlready[i];
			const bool notInList1 = !decalVtxInListAlready[numVerts+i];
			if (notInList0 || notInList1)
			{
				numNewVerts += notInList0 + notInList1;

#				if __SPU
					// Wait for any previous dma on this double buffered tag to
					// have completed, this makes sure we don't overwrite
					// anything while it is still being put to memory.
					const unsigned dmaTag = DMATAG_PUT_VTX0 + vtxBufIdx;
					decalVtx *const pDecalVtxLs = (decalVtx*)(decalVtxBufLs[vtxBufIdx] + ((u32)pDecalVtxEas[i]&15));
					vtxBufIdx ^= 1;
					sysDmaWaitTagStatusAll(1<<dmaTag);
					rage_placement_new(pDecalVtxLs) decalVtx;
#				else
					decalVtx *const pDecalVtxLs = pDecalVtxEas[i];
#				endif

				Vec3V n = pNorms[i];
				if (__SPU || notInList0)
				{
					pDecalVtxLs->SetPos(posC, invPosScale, msOffsetFlt);
					pDecalVtxLs->SetNormal(n);
					pDecalVtxLs->SetTexCoord(texCoordsC);
					pDecalVtxLs->SetFrontToBackRatio(frontToBackRatio);
					pDecalVtxLs->SetVehicleDamageScale(pVehDmgScales[i]);
					pDecalVtxLs->SetTangent(CalcTangent(posA, posB, posC, n, texCoordsA, texCoordsB, texCoordsC));
					SPU_ONLY(if(notInList0))
					{
						OutputVertex(pDecalVtxLs SPU_ONLY(, pDecalVtxEas[i], dmaTag));
					}
				}

				if (notInList1)
				{
#					if __SPU
						decalVtx *const ea2 = pDecalVtxEas[numVerts+i];
						decalVtx *const pDecalVtx2Ls = (decalVtx*)(decalVtxBufLs[vtxBufIdx+2] + ((u32)ea2&15));
#					else
						decalVtx *const pDecalVtx2Ls = pDecalVtxEas[numVerts+i];
#					endif
					sysMemCpy(pDecalVtx2Ls, pDecalVtxLs, sizeof(*pDecalVtx2Ls));
					const Vec3V n2 = -n;
					const Vec3V p2 = posC + n2 * (settings->user.pipelineSettings.isVehicleGlass?DOUBLE_SIDED_OFFSET_VEH:DOUBLE_SIDED_OFFSET);
					pDecalVtx2Ls->SetPos(p2, invPosScale, msOffsetFlt);
					pDecalVtx2Ls->SetNormal(n2);
					pDecalVtx2Ls->FlipBitangent();
					OutputVertex(pDecalVtx2Ls SPU_ONLY(, ea2, dmaTag));
				}
			}

			// Shuffle vertex values through the pipeline for tangent calculations
			posA = nextPosA;
			posB = posC;
			nextPosA = pVerts[0];
			texCoordsA = nextTexCoordsA;
			texCoordsB = texCoordsC;
			nextTexCoordsA = texCoords0;
		}
		pDecalInst->m_numVtxs += (u16)numNewVerts;


		for (unsigned i=0; i<numTris; ++i)
		{
			u16 *const pIndices = pDecalInst->AddTri(miniIdxBufWriter);
			pIndices[0] = (u16)(pDecalVtxEas[0]   - pVtxBase);
			pIndices[1] = (u16)(pDecalVtxEas[i+1] - pVtxBase);
			pIndices[2] = (u16)(pDecalVtxEas[i+2] - pVtxBase);

#			if __BANK && !__SPU

				// increment the number of clipped polys
				m_debug.m_numPolysClippedThisProjection++;

				// render the clipped polys (red)
				if (m_debug.m_renderPolysClipped)
				{
					Vec3V vDebugVtx1 = Transform(settings->internal.parentMtx, pVerts[0]);
					Vec3V vDebugVtx2 = Transform(settings->internal.parentMtx, pVerts[i+1]);
					Vec3V vDebugVtx3 = Transform(settings->internal.parentMtx, pVerts[i+2]);
					grcDebugDraw::Poly(vDebugVtx1, vDebugVtx2, vDebugVtx3, Color32(1.0f, 0.0f, 0.0f, 1.0f), true, false, m_debug.m_renderTimer);
				}
#			endif
		}
		if (doubleSided)
		{
			for (unsigned i=0; i<numTris; ++i)
			{
				u16 *const pIndices = pDecalInst->AddTri(miniIdxBufWriter);
				pIndices[0] = (u16)(pDecalVtxEas[numVerts+0]   - pVtxBase);
				pIndices[1] = (u16)(pDecalVtxEas[numVerts+i+2] - pVtxBase);
				pIndices[2] = (u16)(pDecalVtxEas[numVerts+i+1] - pVtxBase);
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  CalcTexCoords
///////////////////////////////////////////////////////////////////////////////

void decalManagerAsync::CalcTexCoords(const decalSettings* settings, const decalRenderSettings* renderSettings, Vec3V_In vPos, Vec2V_InOut vTexCoords, ScalarV_InOut vFrontToBackRatio)
{
	// calc the texture coords at this position
	Vec3V vDiff;
	vDiff = vPos - settings->internal.vLclPosition;
	float dot1 = Dot(settings->internal.vLclSide, vDiff).Getf();
	float dot2 = Dot(-settings->internal.vLclForward, vDiff).Getf();

	// do any user requested flips
	if (settings->user.instSettings.flipU)
	{
		dot1 = -dot1;
	}

	if (settings->user.instSettings.flipV)
	{
		dot2 = -dot2;
	}

	// put u and v in the range (-0.5f, 0.5f)
	float u = dot1/settings->user.instSettings.vDimensions.GetXf();
	float v = dot2/settings->user.instSettings.vDimensions.GetYf();

	// put u and v in the range (0.0f, 1.0f)
	u += 0.5f;
	v += 0.5f;

	// clamp to 0.0 and 1.0 incase of slight inaccuracies
	u = Max(u, 0.0f);
	u = Min(u, 1.0f);
	v = Max(v, 0.0f);
	v = Min(v, 1.0f);

	// set the back to front ratio
	vFrontToBackRatio = ScalarVFromF32(v);

	// apply wrap length
	const float texWrapLength = renderSettings->wrapLength;
	if (texWrapLength>0.0f)
	{
		const float currWrapLength = settings->user.instSettings.currWrapLength;
		const float finalWrapLength = currWrapLength + settings->user.instSettings.vDimensions.GetYf();
		const float v0 = 1.0f - (finalWrapLength/texWrapLength);
		const float v1 = 1.0f - (currWrapLength/texWrapLength);
		v = v0 + (v*(v1-v0));
	}

	// set
	vTexCoords.SetX(u);
	vTexCoords.SetY(v);
}


///////////////////////////////////////////////////////////////////////////////
//  CalcTangent
///////////////////////////////////////////////////////////////////////////////

// TODO -- vectorise
Vec4V_Out decalManagerAsync::CalcTangent(Vec3V_In posA, Vec3V_In posB, Vec3V_In posC, Vec3V_In n, Vec2V_In texCoordsA, Vec2V_In texCoordsB, Vec2V_In texCoordsC)
{
	float x1 = posB.GetXf() - posA.GetXf();
	float x2 = posC.GetXf() - posA.GetXf();
	float y1 = posB.GetYf() - posA.GetYf();
	float y2 = posC.GetYf() - posA.GetYf();
	float z1 = posB.GetZf() - posA.GetZf();
	float z2 = posC.GetZf() - posA.GetZf();

	float s1 = texCoordsB.GetXf() - texCoordsA.GetXf();
	float s2 = texCoordsC.GetXf() - texCoordsA.GetXf();
	float t1 = texCoordsB.GetYf() - texCoordsA.GetYf();
	float t2 = texCoordsC.GetYf() - texCoordsA.GetYf();

	float p = (s1 * t2) - (s2 * t1);
	if (p==0.0f)
	{
		p = 0.001f;
	}

	float r = 1.0f / p;
	Vec3V sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
	Vec3V tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

	Vec3V tangent = (sdir - n * Dot(n, sdir));

	// When normalizing, check for zero tangents (seems to happen if all 3 verts have a zero u or v component)
	tangent = NormalizeSafe(tangent, Vec3V(V_ONE));

	Vec4V finalTangent;
	finalTangent.SetXYZ(tangent);
	finalTangent.SetWf((Dot(Cross(n, sdir), tdir).Getf() > 0.0f) ? -1.0f : 1.0f);	// left or right handed calculation

	return finalTangent;
}


///////////////////////////////////////////////////////////////////////////////
//  ShouldProcessShader
///////////////////////////////////////////////////////////////////////////////

bool decalManagerAsync::ShouldProcessShader(const grmShader* pShader, const fwEntity* pEntity, bool isGlassShader, bool onlyApplyToGlass, bool dontApplyToGlass)
{
#	if __BANK
		if (m_debug.m_disableShaderTest)
		{
			return true;
		}
#	endif

	// check if the shader is glass and respect the settings passed in
	if ((onlyApplyToGlass && !isGlassShader) ||
		(dontApplyToGlass && isGlassShader))
	{
		return false;
	}

	return g_pDecalCallbacks->ShouldProcessShader(pShader, pEntity);
}


///////////////////////////////////////////////////////////////////////////////
//  ShouldProcessDrawableBone
///////////////////////////////////////////////////////////////////////////////

bool decalManagerAsync::ShouldProcessDrawableBone(decalBoneHierarchyIdx lodAttachMatrixId, const fwEntity* pEntity, decalBoneHierarchyIdx boneIdx)
{
	// reject any bones based on the lod system
	if (lodAttachMatrixId>-1 && lodAttachMatrixId!=boneIdx)
	{
		return false;
	}

	// reject any bones that the game doesn't want to process
#if __BANK
	if (!m_debug.m_disableDrawableBoneGameTest)
#endif
	{
		if (!g_pDecalCallbacks->ShouldProcessDrawableBone(pEntity, boneIdx))
		{
			return false;
		}
	}

	return true;
}


///////////////////////////////////////////////////////////////////////////////
//  ShouldProcessDrawableBone
///////////////////////////////////////////////////////////////////////////////

bool decalManagerAsync::ShouldProcessDrawableBone(const decalAsyncTaskDescBase* taskDescLs, const fwEntity* pEntity, decalBoneHierarchyIdx boneIdx)
{
	return ShouldProcessDrawableBone(decalBoneHierarchyIdx(taskDescLs->settings.user.lodSettings.lodAttachMatrixId), pEntity, boneIdx);
}


} // namespace rage


// undefines
#undef DECAL_ASSERT_ALREADY_LOCKED

#endif // !__TOOL && !__RESOURCECOMPILER
