//
// vfx/decal/decalpools.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalpools.h"

// includes (rage)
#include "system/memory.h"

// includes (framework)
#include "vfx/decal/decalmanager.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"

#if RSG_DURANGO
#	include "grcore/wrapper_d3d.h"
#	include "system/d3d11.h"
#endif


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


// code
void decalPools::Init()
{
	m_buckets   = m_bucketPool  .InitAlloc(DECAL_MAX_BUCKETS);
	m_insts     = m_instPool    .InitAlloc(DECAL_MAX_INSTS);
	m_mibs      = m_mibPool     .InitAlloc(DECAL_MAX_MINI_IDX_BUFS);
#	if !RSG_DURANGO
		m_verts = m_vtxPool.InitAlloc(DECAL_MAX_VERTS);
#	else
		// This code is intentionally allocating
		// D3D11_GRAPHICS_MEMORY_ACCESS_CPU_CACHE_NONCOHERENT, DO NOT switch to
		// D3D11_GRAPHICS_MEMORY_ACCESS_CPU_WRITECOMBINE_NONCOHERENT when we get
		// final hardware that fixes writecombining.  The CPU needs to be able
		// to cache the vertex buffer for reads.
		const SIZE_T sizeBytes = DECAL_MAX_VERTS * sizeof(decalVtx);
		m_verts = (decalVtxFree*) sysMemPhysicalAllocate(sizeBytes, PhysicalMemoryType::PHYS_MEM_GARLIC_WRITEBACK);
		decalAssert(m_verts);
		m_vtxPool.Init(m_verts, DECAL_MAX_VERTS, SListDefaultAlignPtrConverter<__alignof(decalVtxFree)>());
#	endif
}


void decalPools::Shutdown()
{
	decalAssertf(m_bucketPool   .GetNumItems() == DECAL_MAX_BUCKETS,                            "bucket list isn't empty");
	decalAssertf(m_instPool     .GetNumItems() == DECAL_MAX_INSTS,                              "inst list isn't empty");
	decalAssertf(m_mibPool      .GetNumItems() == DECAL_MAX_TRIS/decalMiniIdxBuf::MAX_NUM_TRIS, "mini index buffer list isn't empty");
	decalAssertf(m_vtxPool      .GetNumItems() == DECAL_MAX_VERTS,                              "vtx list isn't empty");

	sysMemAllocator *const mem = &sysMemAllocator::GetCurrent();
	RAGE_LOG_DELETE(m_buckets); mem->Free(m_buckets);
	RAGE_LOG_DELETE(m_insts);   mem->Free(m_insts);
	RAGE_LOG_DELETE(m_mibs);    mem->Free(m_mibs);
#	if !RSG_DURANGO
		RAGE_LOG_DELETE(m_verts);   mem->Free(m_verts);
#	else
	sysMemPhysicalFree(m_verts);
#	endif
}



} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
