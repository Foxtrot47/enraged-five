// 
// vfx/decal/decalhelper.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalhelper.h"

// includes (rage)
#if DECAL_PROCESS_BREAKABLES && !__SPU
#include "breakableglass/breakable.h"
#include "breakableglass/glassmanager.h"
#endif
#include "entity/entity.h"
#include "fragment/drawable.h"
#include "fragment/instance.h"
#include "fragment/type.h"
#include "fragment/typechild.h"
#include "phbound/boundcomposite.h"
#include "physics/inst.h"
#include "physics/levelnew.h"

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalbones.h"
#include "vfx/decal/decalcallbacks.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces 
namespace rage {



decalOverlayResult decalHelper::PreProcessOverlays(Vec3V_In posA, float radiusA, Vec3V_In posB, float& radiusB, float maxOverlayRadius)
{
	// A is the existing decal
	// B is the new decal

	// get the distance from the new decal to this inst
	float distBetween = Mag(posA-posB).Getf();

	// calc the radius of this inst

	// check if the new texture is totally enclosed within the existing one
	if (distBetween+radiusB < radiusA)
	{
		// it is - we don't want to project the new texture at all
		return DECAL_OVERLAY_ENCLOSED;
	}
	// check if the new texture overlaps the existing texture
	else if (distBetween-(0.5f*radiusB) < radiusA)
	{
		// check we haven't got too big
		if (distBetween+radiusA<maxOverlayRadius)
		{
			// they overlap - update the radius of the new decal
			float thisRadius = (distBetween+radiusA) * 1.25f;		// add on a little extra to make sure!
			if (thisRadius>radiusB)
			{
				radiusB = thisRadius;
			}

			return DECAL_OVERLAY_OVERLAPPING;
		}
	}

	return DECAL_OVERLAY_SEPARATE;
}

#if !__SPU
bool decalHelper::CalcMatrix(Mat34V_InOut mtx, bool* isDamaged, const fwEntity* pAttachEntity, const phInst* pInst, s32 attachMatrixId, decalAttachType attachType)
{
	*isDamaged = false;

	// get current world matrix of this bucket
	if (attachType==DECAL_ATTACH_TYPE_NONE)
	{
		// don't modify current matrix
	}
#if DECAL_PROCESS_BREAKABLES
	else if (attachType==DECAL_ATTACH_TYPE_BREAKABLE_BROKEN)
	{
		// get the component matrix
		const bgBreakable& breakable = bgGlassManager::GetGlassBreakable((bgGlassHandle)attachMatrixId);
		mtx = RCC_MAT34V(breakable.GetTransform());
	}
#endif
	else
	{
		if (attachType==DECAL_ATTACH_TYPE_BONE)
		{
			g_pDecalCallbacks->GetMatrixFromBoneIndex(mtx, isDamaged, pAttachEntity, (decalBoneHierarchyIdx)attachMatrixId);
		}
		else if (attachType==DECAL_ATTACH_TYPE_BONE_SKINNED)
		{
			g_pDecalCallbacks->GetMatrixFromBoneIndex_Skinned(mtx, isDamaged, pAttachEntity, (decalBoneHierarchyIdx)attachMatrixId);
		}
		else if (attachType==DECAL_ATTACH_TYPE_COMPONENT 
#if DECAL_PROCESS_BREAKABLES
			     || attachType==DECAL_ATTACH_TYPE_BREAKABLE_UNBROKEN
#endif
				 )
		{
			// attachMatrixId is a component id
			int componentId = attachMatrixId;

			// get the component matrix

#			if DECAL_PROCESS_BREAKABLES
				decalAssertf(pInst, "must have a phInst for DECAL_ATTACH_TYPE_COMPONENT or DECAL_ATTACH_TYPE_BREAKABLE_UNBROKEN");
#			else
				decalAssertf(pInst, "must have a phInst for DECAL_ATTACH_TYPE_COMPONENT");
#			endif
			const phArchetype* pArchetype = pInst->GetArchetype();
			decalAssertf(pArchetype, "cannot get archetype from inst (%s)", pAttachEntity->GetModelName());
			phBound* pBound = pArchetype->GetBound();
			decalAssertf(pBound, "cannot get bound from archetype (%s)", pAttachEntity->GetModelName());

			Mat34V boundMtx;
			const phBound* pLocalBound;
			if ((pBound)->GetType() == phBound::COMPOSITE)
			{
				const phBoundComposite* pBoundComposite = static_cast<const phBoundComposite*>(pBound);
				if (decalVerifyf(componentId >= 0 && componentId < pBoundComposite->GetNumBounds(), "component id out of range (%s)", pAttachEntity->GetModelName()))
				{
					Mat34V childMtx = pBoundComposite->GetCurrentMatrix(componentId);
					g_pDecalCallbacks->ApplyVehicleSuspensionToMtx(childMtx, pAttachEntity);
					Transform(boundMtx, pInst->GetMatrix(), childMtx);
					pLocalBound = pBoundComposite->GetBound(componentId);

					// The bound may validly be NULL if it has been broken off
					if (Unlikely(!pLocalBound))
					{
						return false;
					}
				}
				else
				{
					// Same as above, the bound may be invalid in some situations
					return false;
				}
			}
			else
			{
				decalAssertf(componentId==0, "non zero component id in non composite bound (%s)", pAttachEntity->GetModelName());
				boundMtx = pInst->GetMatrix();
				pLocalBound = pBound;
			}

			Vec3V offset(V_ZERO);
			if (pLocalBound->GetType() == phBound::BVH || pLocalBound->GetType() == phBound::GEOMETRY)
			{
#				if COMPRESSED_VERTEX_METHOD == 0
					offset = pLocalBound->GetCentroidOffset();
#				else
					decalAssert(dynamic_cast<const phBoundPolyhedron*>(pLocalBound));
					offset = static_cast<const phBoundPolyhedron*>(pLocalBound)->GetLocalSpaceOffset();
#				endif
			}

			boundMtx.GetCol3Ref() += Transform3x3(boundMtx, offset);
			mtx = boundMtx;
		}
		else if (attachType==DECAL_ATTACH_TYPE_SMASHGROUP)
		{
			g_pDecalCallbacks->GetMatrixFromBoneIndex_SmashGroup(mtx, pAttachEntity, (decalBoneHierarchyIdx)attachMatrixId);
		}
	}

	return true;
}
#endif // !__SPU

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
