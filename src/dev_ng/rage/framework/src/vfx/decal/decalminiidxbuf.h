//
// vfx/decal/decalminiidxbuf.h
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALMINIIDXBUF_H
#define	VFX_DECALMINIIDXBUF_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)


// includes (framework)
#include "vfx/channel.h"
#include "vfx/slist.h"


// namespaces
namespace rage {


// classes
class decalMiniIdxBuf;
class decalMiniIdxBufFree_;


// Custom SList pointer converter.  We derive decalMiniIdxBuf from
// SListItemBase<> so that we can make use of the SListItors.  The 16-bit
// storage holds not just the index of the next mini index buffer, but also the
// count of the number of triangles in the mib.  See decalMiniIdxBuf for the
// enums controlling the bit allocations.
class decalMiniIdxBufSListPtrConverter
{
public:
	typedef u16 STORAGE;
	explicit inline decalMiniIdxBufSListPtrConverter(const decalMiniIdxBuf *base_);
	inline u16 FromPtr(const SListItemBase<decalMiniIdxBufSListPtrConverter> *ptr, u16 current) const;
	inline decalMiniIdxBuf *ToPtr(u16 val) const;
private:
	const decalMiniIdxBuf *base;
};


// The minimum valid alignment is sizeof(void*) (due to the aliased class
// decalMiniIdxBufFree_).  For performance reasons this has been increased to
// prevent an instance from crossing a cache line boundary.
#define DECAL_MINI_IDX_BUF_ALIGN     32

class ALIGNAS(DECAL_MINI_IDX_BUF_ALIGN) decalMiniIdxBuf : public SListItemBase<decalMiniIdxBufSListPtrConverter>
{
	friend class decalMiniIdxBufFree_;
	friend class decalMiniIdxBufSListPtrConverter;

	enum { COUNT_MASK  = 0xe000 };
	enum { NEXT_MASK   = 0x1fff };
	enum { COUNT_SHIFT = 13 };
	enum { NEXT_SHIFT  = 0 };

public:

	enum { MAX_NUM_TRIS = 5 };
	enum { NULL_IDX = NEXT_MASK >> NEXT_SHIFT };
	enum { INVALID_VTX_IDX = 0xffff };

	CompileTimeAssert(MAX_NUM_TRIS <= (COUNT_MASK >> COUNT_SHIFT));

	u16     m_idx[MAX_NUM_TRIS*3];

	static inline const decalMiniIdxBuf *GetPtr(const decalMiniIdxBuf *base, u16 idx);
	static inline       decalMiniIdxBuf *GetPtr(      decalMiniIdxBuf *base, u16 idx);

	inline void Init();
	inline unsigned GetFirstIdxIdx(const decalMiniIdxBuf *base) const;
	inline unsigned GetNumTris() const;
	inline bool IsFull() const;
	inline u16 *AddTri();
	inline void SetNumTris(unsigned count);
	inline u16 GetNextIdx() const;
	inline const decalMiniIdxBuf *GetNext(const decalMiniIdxBuf *base) const;
	inline       decalMiniIdxBuf *GetNext(      decalMiniIdxBuf *base);
	inline const decalMiniIdxBuf *GetNext(const decalMiniIdxBufSListPtrConverter &conv) const;
	inline       decalMiniIdxBuf *GetNext(const decalMiniIdxBufSListPtrConverter &conv);
	inline void SetNext(u16 idx);
	inline void SetNext(const decalMiniIdxBuf *base, const decalMiniIdxBuf *next);
};


class ALIGNAS(DECAL_MINI_IDX_BUF_ALIGN) decalMiniIdxBufFree_ : public SListItem<decalMiniIdxBufFree_>
{
	ATTR_UNUSED u16     pad[1 + decalMiniIdxBuf::MAX_NUM_TRIS*3 - sizeof(void*)/2];
};

typedef decalMiniIdxBufFree_ MAY_ALIAS decalMiniIdxBufFree;

CompileTimeAssert(__alignof(decalMiniIdxBufFree) == __alignof(decalMiniIdxBuf));
CompileTimeAssert(sizeof(decalMiniIdxBufFree)    == sizeof(decalMiniIdxBuf));


inline decalMiniIdxBufSListPtrConverter::decalMiniIdxBufSListPtrConverter(const decalMiniIdxBuf *base_)
	: base(base_)
{
}


inline u16 decalMiniIdxBufSListPtrConverter::FromPtr(const SListItemBase<decalMiniIdxBufSListPtrConverter> *ptr, u16 current) const
{
	const u16 val = (ptr ? (u16)(static_cast<const decalMiniIdxBuf*>(ptr)-base) : decalMiniIdxBuf::NULL_IDX) << decalMiniIdxBuf::NEXT_SHIFT;
	decalAssert((val & ~decalMiniIdxBuf::NEXT_MASK) == 0);
	return val | (current & ~decalMiniIdxBuf::NEXT_MASK);
}


inline decalMiniIdxBuf *decalMiniIdxBufSListPtrConverter::ToPtr(u16 val) const
{
	const u16 v = (val & decalMiniIdxBuf::NEXT_MASK) >> decalMiniIdxBuf::NEXT_SHIFT;
	return v!=decalMiniIdxBuf::NULL_IDX ? const_cast<decalMiniIdxBuf*>(base)+v : NULL;
}


/*static*/ inline const decalMiniIdxBuf *decalMiniIdxBuf::GetPtr(const decalMiniIdxBuf *base, u16 idx)
{
	return (idx==NULL_IDX) ? NULL : (base+idx);
}


/*static*/ inline decalMiniIdxBuf *decalMiniIdxBuf::GetPtr(decalMiniIdxBuf *base, u16 idx)
{
	return const_cast<decalMiniIdxBuf*>(GetPtr(const_cast<const decalMiniIdxBuf*>(base), idx));
}


inline void decalMiniIdxBuf::Init()
{
	SetNextRaw(NEXT_MASK);
}


inline unsigned decalMiniIdxBuf::GetFirstIdxIdx(const decalMiniIdxBuf *base) const
{
	return (unsigned)((u16*)this - (u16*)base) + 1;
}


inline unsigned decalMiniIdxBuf::GetNumTris() const
{
	return (GetNextRaw() & COUNT_MASK) >> COUNT_SHIFT;
}


inline bool decalMiniIdxBuf::IsFull() const
{
	return (GetNextRaw() & COUNT_MASK) == (MAX_NUM_TRIS << COUNT_SHIFT);
}


inline u16 *decalMiniIdxBuf::AddTri()
{
	decalAssert(!IsFull());
	const unsigned nac = GetNextRaw();
	const unsigned c = (nac & COUNT_MASK) >> COUNT_SHIFT;
	SetNextRaw((u16)(nac + (1 << COUNT_SHIFT)));
	return m_idx+(c*3);
}


inline void decalMiniIdxBuf::SetNumTris(unsigned count)
{
	decalAssert(count <= MAX_NUM_TRIS);
	unsigned nac = GetNextRaw();
	nac &= ~COUNT_MASK;
	nac |= count << COUNT_SHIFT;
	SetNextRaw((u16)nac);
}


inline u16 decalMiniIdxBuf::GetNextIdx() const
{
	return (GetNextRaw() & NEXT_MASK) >> NEXT_SHIFT;
}


inline const decalMiniIdxBuf *decalMiniIdxBuf::GetNext(const decalMiniIdxBuf *base) const
{
	return GetPtr(base, GetNextIdx());
}


inline decalMiniIdxBuf *decalMiniIdxBuf::GetNext(decalMiniIdxBuf *base)
{
	return const_cast<decalMiniIdxBuf*>(const_cast<const decalMiniIdxBuf*>(this)->GetNext(base));
}


inline const decalMiniIdxBuf *decalMiniIdxBuf::GetNext(const decalMiniIdxBufSListPtrConverter &conv) const
{
	return conv.ToPtr(GetNextRaw());
}


inline decalMiniIdxBuf *decalMiniIdxBuf::GetNext(const decalMiniIdxBufSListPtrConverter &conv)
{
	return const_cast<decalMiniIdxBuf*>(const_cast<const decalMiniIdxBuf*>(this)->GetNext(conv));
}


inline void decalMiniIdxBuf::SetNext(u16 idx)
{
	decalAssert(idx <= (NEXT_MASK>>NEXT_SHIFT));
	SetNextRaw((GetNextRaw() & ~NEXT_MASK) | (idx << NEXT_SHIFT));
}


inline void decalMiniIdxBuf::SetNext(const decalMiniIdxBuf *base, const decalMiniIdxBuf *next)
{
	SetNext(next ? (u16)(next - base) : (u16)NULL_IDX);
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALMINIIDXBUF_H
