//
// vfx/decal/decallod.h
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALLOD_H
#define	VFX_DECALLOD_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)

// includes (framework)
#include "vfx/decal/decalsettings.h"


// namespaces 
namespace rage {


// forward declarations
class decalBucket;
class decalInst;


// classes
class decalLodSystem
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Update();

	private: //////////////////////////

		static bool ConvertToDynamicDecal(decalInst* pInst, decalBucket* pBucket, decalRenderPass renderPass);
		static bool ConvertToStaticDecal(decalInst* pInst, decalBucket* pBucket, decalRenderPass renderPass, bool onlyApplyToGlass=false, bool dontApplyToGlass=true, bool applyToBreakables=false);
		static bool CanLodBucket(const decalBucket* pBucket);

};


// externs
#if __DEV
extern int DECAL_MAX_INSTS_TO_LOD_PER_FRAME;
extern int DECAL_MAX_INSTS_TO_UNLOD_PER_FRAME;
#endif


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALLOD_H
