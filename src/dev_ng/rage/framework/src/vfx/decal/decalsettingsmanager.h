//
// vfx/decal/decalsettingsmanager.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALSETTINGSMANAGER_H
#define	VFX_DECALSETTINGSMANAGER_H

#if !__TOOL && !__RESOURCECOMPILER

#include "vfx/channel.h"
#include "vfx/decal/decalsettings.h"

// namespaces
namespace rage {


// forward declarations	(rage)
class grcTexture;


// forward declarations	(framework)
struct decalTypeSettings;
struct decalRenderSettings;
struct decalRenderSettingsInfo;


// defines
#define	DECAL_MAX_TYPE_SETTINGS			(16)
#define	DECAL_MAX_RENDER_SETTINGS		(260)


// classes
class decalSettingsManager
{
	friend class decalBucket;
	friend class decalDebug;
	friend class decalInst;
	friend class decalManager;

	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		static void Init(u32 numTypeSettings, decalTypeSettings* pTypeSettings, u32 numRenderSettingsFiles, decalRenderSettingsInfo* pRenderSettingsInfos);
		static void Shutdown();
		static void Update();

		// type settings
		static __forceinline const decalTypeSettings* GetTypeSettings(u32 typeSettingsIndex)
		{
			if (decalVerifyf(typeSettingsIndex<ms_numTypeSettings, "invalid type settings index"))
			{
				return &ms_pTypeSettings[typeSettingsIndex];
			}

			return NULL;
		}


		// render settings
		static bool	FindRenderSettingInfo(u32 renderSettingsId, u32& renderSettingsIndex, u32& renderSettingCount);
		static int GetRenderSettingsIndex(u32 renderSettingsIndex, u32 renderSettingsCount);
		static __forceinline const decalRenderSettings* GetRenderSettings(u32 renderSettingsIndex)
		{
			if (decalVerifyf(renderSettingsIndex<ms_numRenderSettings, "invalid render settings index"))
			{
				return &ms_renderSettings[renderSettingsIndex];
			}

			return NULL;
		}

		// misc
		static void PatchDiffuseMap(u32 renderSettingsId, grcTexture* pDiffuseMap);


#if __BANK
		static void InitWidgets();
#endif


	private: //////////////////////////

		static void LoadRenderSettings(const char* pFilename);


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

		static u32 ms_numTypeSettings;
		static decalTypeSettings* ms_pTypeSettings;

		static u32 ms_numRenderSettings;
		static decalRenderSettings ms_renderSettings[DECAL_MAX_RENDER_SETTINGS];

		struct PatchRequest
		{
			u32         renderSettingsIndex;
			grcTexture *pDiffuseMap;
		};
		static u32 ms_numPatchRequests;
		static PatchRequest ms_patchRequests[DECAL_MAX_RENDER_SETTINGS];
};


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALSETTINGSMANAGER_H
