//
// vfx/decal/decalrecycler.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALRECYCLER_H
#define	VFX_DECALRECYCLER_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "vectormath/classes.h"


// namespaces 
namespace rage {


// forward declarations
class decalBucket;
class decalInst;


// structures
struct decalSortEntry
{
	decalBucket* pDecalBucket;
	decalInst* pDecalInst;
};


// classes
class decalRecycler
{	
	friend class decalDebug;

	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		static void Update();
#if __BANK
		static void RenderDebug(Vec3V_In vCamPos);
#endif

	private: //////////////////////////

//		static bool ArePoolsUnderThresh(float thresh);
		static float GetMinPoolRatio();
		static void RecycleUsingRangeRatio();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////		

	private: //////////////////////////

		static float ms_currRecycleRangeRatio;

};

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALRECYCLER_H
