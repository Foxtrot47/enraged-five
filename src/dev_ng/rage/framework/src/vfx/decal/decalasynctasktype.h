//
// vfx/decal/decalasynctasktype.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALASYNCTASKTYPE_H
#define VFX_DECALASYNCTASKTYPE_H

#if !__TOOL && !__RESOURCECOMPILER

// namespaces
namespace rage {


// enums
enum decalAsyncTaskType
{
	DECAL_DO_PROCESSBOUND,
	DECAL_DO_PROCESSBREAKABLEGLASS,
	DECAL_DO_PROCESSDRAWABLESNONSKINNED,
	DECAL_DO_PROCESSDRAWABLESSKINNED,
	DECAL_DO_PROCESSSMASHGROUP,

	NUM_DECAL_ASYNC_TASK_TYPES
};


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALASYNCTASKTYPE_H
