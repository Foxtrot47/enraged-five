//
// vfx/decal/decaldefraglocker.cpp
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decaldefraglocker.h"

#if USE_DEFRAGMENTATION

// includes (rage)
#include "data/resource.h"
#include "paging/base.h"
#include "paging/rscbuilder.h"
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/threadtype.h"

// includes (framework)
#include "streaming/defragmentation.h"
#include "streaming/streamingengine.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"

// includes (other)
#include <algorithm>
#include <string.h>


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// defines
#if __ASSERT
#	define DEFRAG_LOCKER_OVERFLOWED    (~0u)
#endif


// namespaces
namespace rage {


#if __ASSERT

	// Mutex to prevent worker threads from searching for a pointer in the array
	// while the main thread is updating the array.
	static sysCriticalSectionToken s_defragLockerCritSecTok;

#endif


__forceinline bool decalDefragLocker::LockCmp::operator()(const pgBase *lhs, const Lock &rhs) const
{
	return lhs < rhs.m_resource;
}


__forceinline bool decalDefragLocker::LockCmp::operator()(const Lock &lhs, const pgBase *rhs) const
{
	return lhs.m_resource < rhs;
}


__forceinline bool decalDefragLocker::LockCmp::operator()(const Lock &lhs, const Lock &rhs) const
{
	return lhs.m_resource < rhs.m_resource;
}


void decalDefragLocker::Init(unsigned poolSize)
{
	m_locks = rage_new Lock[poolSize];
	m_maxNumLocks = poolSize;
	m_numLocks = 0;
}


void decalDefragLocker::Shutdown()
{
	decalAssert(!m_numLocks);
	delete[] m_locks;
	ASSERT_ONLY(m_locks = NULL);
}


bool decalDefragLocker::LockRawPtr(const void *ptr)
{
	sysMemAllocator* pAllocator;

#if RESOURCE_HEADER
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
	if (pAllocator->IsValidPointer(ptr))
		return true;
#endif

	pAllocator = &sysMemAllocator::GetCurrent();
	const void *const block = pAllocator->GetCanonicalBlockPtr(ptr);
	if (!block)
	{
		// Not from a streaming allocator, so won't be defragged, and .: nothing to be locked
		return true;
	}

	// The strIndex stored in the allocation user data will be a valid streaming
	// index, iff the allocation was made by a streaming module
	const strIndex idx = strStreamingInfoManager::GetStreamingIndexFromMemBlock(block);
	if (idx.IsInvalid())
	{
		// Memory doesn't need to be locked, so return true
		return true;
	}

	// For all defragmentable resources, the pointer returned here must be a pgBase pointer
	pgBase *const resource = (pgBase*)strStreamingEngine::GetInfo().GetObjectPtr(idx);
	decalAssert(resource);

	return LockInternal(resource, idx);
}


bool decalDefragLocker::LockPgBase(const pgBase *resource)
{
	const strIndex idx = strStreamingInfoManager::GetStreamingIndexFromMemBlock(resource);
	decalAssert(idx.IsValid());
	decalAssert(resource == (pgBase*)strStreamingEngine::GetInfo().GetObjectPtr(idx));
	return LockInternal(const_cast<pgBase*>(resource), idx);
}


#if __ASSERT && !__SPU
	bool decalDefragLocker::AssertOnlyIsAlreadyLocked(const void *ptr) const
	{
		// If we overflowed the pointer array, then always return true to prevent false asserts
		if (m_numLocks == DEFRAG_LOCKER_OVERFLOWED)
		{
			return true;
		}
		else
		{
			sysMemAllocator* pAllocator;

#if RESOURCE_HEADER
			pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
			if (pAllocator->IsValidPointer(ptr))
				return true;
#endif
			pAllocator = &sysMemAllocator::GetCurrent();
			const void *const block = pAllocator->GetCanonicalBlockPtr(ptr);
			if (!block)
			{
				return true;
			}
			const strIndex idx = strStreamingInfoManager::GetStreamingIndexFromMemBlock(block);
			if (idx.IsInvalid())
			{
				return true;
			}
			pgBase *const resource = (pgBase*)strStreamingEngine::GetInfo().GetObjectPtr(idx);
			decalAssert(resource);
			SYS_CS_SYNC(s_defragLockerCritSecTok);
			return std::binary_search(m_locks, m_locks+m_numLocks, resource, LockCmp());
		}
	}
#endif // __ASSERT


void decalDefragLocker::UnlockAll()
{
	strDefragmentation *const defrag = strStreamingEngine::GetDefragmentation();
	Lock *p = m_locks;
	const unsigned numLocks = m_numLocks ASSERT_ONLY(== DEFRAG_LOCKER_OVERFLOWED ? m_maxNumLocks : m_numLocks);
	decalAssert(numLocks <= m_maxNumLocks);
	Lock *const end = p+numLocks;
	while (Likely(p < end))
	{
		defrag->Unlock(p->m_resource, p->m_index);
		++p;
	}
	m_numLocks = 0;
}


bool decalDefragLocker::LockInternal(pgBase *resource, strIndex idx)
{
	// If the pointer is already recorded in the array, then we're done.  Note
	// that this does not need to be protected with s_defragLockerCritSecTok in
	// assert enabled builds, since only the main thread can modify the array.
	decalAssert(sysThreadType::IsUpdateThread());
	Lock *const end = m_locks+m_numLocks;
	Lock *const pos = std::lower_bound(m_locks, end, resource, LockCmp());
	if (pos < end)
	{
		if (pos->m_resource == resource)
		{
			decalAssert(pos->m_index == idx);
			return true;
		}
		decalAssert(pos->m_index != idx);
	}

	// Not already locked.  Fail if we don't have space to record a new lock.
	decalAssert(DEFRAG_LOCKER_OVERFLOWED > m_maxNumLocks);
	if (Unlikely(m_numLocks >= m_maxNumLocks))
	{
		ASSERT_ONLY(m_numLocks = DEFRAG_LOCKER_OVERFLOWED;)
		return false;
	}

	// Validate he streaming index matches everything in the resource map.
#	if __ASSERT
		datResourceMap map;
		resource->RegenerateMap(map);
		const unsigned chunkCount =
#		  if !FREE_PHYSICAL_RESOURCES
			map.PhysicalCount +
#		  endif // !FREE_PHYSICAL_RESOURCES
			map.VirtualCount;
		sysMemAllocator *const mem = &sysMemAllocator::GetCurrent();
		for (unsigned i=0; i<chunkCount; ++i)
		{
			const void *const ptr = map.Chunks[i].DestAddr;
			decalAssert(ptr);
			decalAssert(ptr == mem->GetCanonicalBlockPtr(ptr));
			decalAssert(strStreamingInfoManager::GetStreamingIndexFromMemBlock(ptr) == idx);
		}
#	endif

	// Perform the lock.
	strStreamingEngine::GetDefragmentation()->Lock(resource, idx);

	// Store the lock.  For assert enabled builds, lock the critsec to ensure no
	// worker threads/spus are currently accessing the array.
	ASSERT_ONLY(SYS_CS_SYNC(s_defragLockerCritSecTok);)
	memmove(pos+1, pos, (uptr)end-(uptr)pos);
	pos->m_resource = resource;
	pos->m_index = idx;
	++m_numLocks;

	return true;
}


} // namespace rage


// undefines
#if __ASSERT
#	undef DEFRAG_LOCKER_OVERFLOWED
#endif


#endif // USE_DEFRAGMENTATION

#endif // !__TOOL && !__RESOURCECOMPILER
