//
// vfx/decal/decalmanager.cpp
//
// Copyright (C) 1999-2015 Rockstar North.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "fwnet/netinterface.h"
#include "vfx/decal/decalmanager.h"

// includes (libc)
#include <string.h>

// includes (rage)
#if DECAL_PROCESS_BREAKABLES
#include "breakableglass/breakable.h"
#include "breakableglass/glassmanager.h"
#endif
#include "crskeleton/skeleton.h"
#include "crskeleton/skeletondata.h"
#include "diag/art_channel.h"
#include "fragment/cache.h"
#include "fragment/drawable.h"
#include "fragment/type.h"
#include "fragment/typechild.h"
#include "fragment/typegroup.h"
#include "grcore/debugdraw.h"
#include "grcore/device.h"
#include "grcore/gfxcontext.h"
#include "grcore/im.h"
#include "grcore/indexbuffer.h"
#include "grcore/light.h"
#include "grcore/matrix43.h"
#include "grcore/vertexdecl.h"
#include "grmodel/geometry.h"
#include "grmodel/matrixset.h"
#include "phbound/boundbox.h"
#include "phbound/boundbvh.h"
#include "phbound/boundcomposite.h"
#include "phbound/primitives.h"
#include "phglass/glassinstance.h"
#include "physics/iterator.h"
#include "physics/shapetest.h"
#include "system/cache.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/threadtype.h"
#include "system/timer.h"
#include "system/taskheader.h"
#if USE_EDGE && HACK_GTA4_MODELINFOIDX_ON_SPU
	#include "grcore/grcorespu.h"
#endif

// includes (framework)
#include "entity/archetype.h"
#include "entity/drawdata.h"
#include "entity/entity.h"
#include "fwdrawlist/drawlistmgr.h"
#include "fwrenderer/renderthread.h"
#include "fwscene/world/InteriorLocation.h"
#include "fwsys/gameskeleton.h"
#include "fwsys/timer.h"
#include "streaming/streamingengine.h"
#include "vfx/channel.h"
#include "vfx/vfxutil.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalasynctaskdescbase.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decaldamagetaskdesc.h"
#include "vfx/decal/decalhelper.h"
#include "vfx/decal/decallod.h"
#include "vfx/decal/decalpools.h"
#include "vfx/decal/decalrecycler.h"
#include "vfx/decal/decalsettings.h"
#include "vfx/decal/decalsettingsmanager.h"
#if RSG_DURANGO
#	include "grcore/wrapper_d3d.h"
#	include "system/d3d11.h"
#elif RSG_PS3
#	include "grcore/wrapper_gcm.h"		// for vram base addr
#	if USE_EDGE
#		include "entity/archetypemanager.h"
#		include "entity/entity.h"
#	endif
#elif RSG_PC && __D3D11
#	include "grcore/wrapper_d3d.h"
#	include <D3D11.h>
#endif

#define  VFX_DECAL_IS_COMPILING_SHADER 0
#include "vfx/decal/decalperinstcbuf.h"

#if __XENON
#	if __DEV && !__OPTIMIZED && !HACK_GTA4
#       define DBG 1
#       include <xgraphics.h>
#       undef DBG
#       pragma comment(lib,"xgraphicsd.lib")
#   else
#       define DBG 0
#       include <xgraphics.h>
#       undef DBG
#       pragma comment(lib,"xgraphics.lib")
#   endif
#endif

// includes (debug)
#if 0
#include "system/findsize.h"			// 12/05/12		13/05/12	22/10/13
FindSize(decalBucket);					// 128 bytes	128			176
FindSize(decalInst);					// 240 bytes	176			96
FindSize(decalVtx);						//  48 bytes	 48			20
//FindSize(decalInstanceSettings);		//							288
#endif


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// defines
#define DECAL_DEFRAG_LOCKER_SIZE    (256)
#define DECAL_DATA_STORE_BYTES      (8*1024)

// Number of concurrent sub render threads supported.
#define DECAL_NUMBER_OF_RENDER_THREADS  NUMBER_OF_RENDER_THREADS


// params
PARAM(nodecallods, "never use decal lods (i.e. dynamic decals)");


// externs
#if __PS3
	DECLARE_TASK_INTERFACE(decaldamage);
#else
	DECLARE_TASK_INTERFACE(ProcessInstTask);
#endif


// namespaces
namespace rage {


// params
XPARAM(nodecals);
XPARAM(novfx);


// static variables
decalManager *decalManager::ms_instance/*=NULL*/;

// global variables
PS3_ONLY(extern u32 g_AllowVertexBufferVramLocks);

//Used for storing the context Index for rendering static decals within a sub render thread
DECAL_MULTI_THREADED_RENDERING_ONLY(__THREAD s8 renderCtxIndex= -1;)

///////////////////////////////////////////////////////////////////////////////
//  CODE
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  CTOR
///////////////////////////////////////////////////////////////////////////////

decalManager::decalManager()
{
	decalAssert(!ms_instance);

#	if RSG_PC && __D3D11
		m_staticIdxBufHeader	  = rage_new ID3D11Buffer*[NUM_PER_CONTEXT_BUFFERS];
		m_staticConstBufIdxHeader = rage_new ID3D11Buffer*[NUM_PER_CONTEXT_BUFFERS];
		m_staticIdxBufLastAdded	  = rage_new unsigned[NUM_PER_CONTEXT_BUFFERS];

		m_staticVtxBuf = NULL;
		sysMemSet(m_staticIdxBufHeader,		0, sizeof(ID3D11Buffer*) * NUM_PER_CONTEXT_BUFFERS);
		sysMemSet(m_staticConstBufIdxHeader,0, sizeof(ID3D11Buffer*) * NUM_PER_CONTEXT_BUFFERS);
		sysMemSet(m_staticIdxBufLastAdded,	0, sizeof(unsigned) * NUM_PER_CONTEXT_BUFFERS);
		m_staticConstBuf = NULL;
#	endif

	m_staticDecalRasterizerState									=  grcStateBlock::RS_Invalid;
	m_staticDecalBlendState											=  grcStateBlock::BS_Invalid;
	m_staticDecalDepthStencilState[DECAL_RENDER_PASS_DEFERRED]		=  grcStateBlock::DSS_Invalid;
	m_staticDecalDepthStencilState[DECAL_RENDER_PASS_FORWARD]		=  grcStateBlock::DSS_Invalid;
	m_staticDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_1]		=  grcStateBlock::DSS_Invalid;
	m_staticDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_2]		=  grcStateBlock::DSS_Invalid;
	m_staticDecalExitRasterizerState								=  grcStateBlock::RS_Invalid;
	m_staticDecalExitBlendState										=  grcStateBlock::BS_Invalid;
	m_staticDecalExitDepthStencilState								=  grcStateBlock::DSS_Invalid;

	m_dynamicDecalRasterizerState									=  grcStateBlock::RS_Invalid;
	m_dynamicDecalBlendState										=  grcStateBlock::BS_Invalid;
	m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_DEFERRED]		=  grcStateBlock::DSS_Invalid;
	m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_FORWARD]		=  grcStateBlock::DSS_Invalid;
	m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_1]		=  grcStateBlock::DSS_Invalid;
	m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_2]		=  grcStateBlock::DSS_Invalid;
	m_dynamicDecalExitRasterizerState								=  grcStateBlock::RS_Invalid;
	m_dynamicDecalExitBlendState									=  grcStateBlock::BS_Invalid;
	m_dynamicDecalExitDepthStencilState								=  grcStateBlock::DSS_Invalid;

	m_staticDecalStencilRef[DECAL_RENDER_PASS_DEFERRED]				= 0xFF;
	m_staticDecalStencilRef[DECAL_RENDER_PASS_FORWARD]				= 0xFF;
	m_staticDecalStencilRef[DECAL_RENDER_PASS_EXTRA_1]				= 0xFF;
	m_staticDecalStencilRef[DECAL_RENDER_PASS_EXTRA_2]				= 0xFF;
	m_dynamicDecalStencilRef[DECAL_RENDER_PASS_DEFERRED]			= 0xFF;
	m_dynamicDecalStencilRef[DECAL_RENDER_PASS_FORWARD]				= 0xFF;
	m_dynamicDecalStencilRef[DECAL_RENDER_PASS_EXTRA_1]				= 0xFF;
	m_dynamicDecalStencilRef[DECAL_RENDER_PASS_EXTRA_2]				= 0xFF;

	m_overrideStaticBlendStateFunctor = NULL;
#if RSG_PC
	m_delayedDeletionQueues = rage_new SList<decalInst>[NUM_RENDER_LATENCY_BUFFERS + 1];

#if __D3D11
	CreateDX11Buffers(UNINITIALIZED_VERTICES);
#endif
#endif // RSG_PC

	m_bInitialized = false;

	ms_instance = this;
}


///////////////////////////////////////////////////////////////////////////////
//  DTOR
///////////////////////////////////////////////////////////////////////////////

decalManager::~decalManager()
{
#if RSG_PC && __D3D11
	DestroyDX11Buffers();
#endif

#	define SAFE_DELETE(RES)    if(RES) { delete [] RES; (RES)=NULL; }
	decalAssert(ms_instance == this);

#	if RSG_PC
		SAFE_DELETE(m_staticIdxBufHeader);
		SAFE_DELETE(m_staticConstBufIdxHeader);
		SAFE_DELETE(m_staticIdxBufLastAdded);
		SAFE_DELETE(m_delayedDeletionQueues);
#	endif
#	undef SAFE_DELETE

	ms_instance = NULL;
}


///////////////////////////////////////////////////////////////////////////////
//  InitSettings
///////////////////////////////////////////////////////////////////////////////

void decalManager::InitSettings(int numTypeSettings, decalTypeSettings* pTypeSettings, int numRenderSettingsFiles, decalRenderSettingsInfo* pRenderSettingsInfos)
{
	// init the settings manager
	decalSettingsManager::Init(numTypeSettings, pTypeSettings, numRenderSettingsFiles, pRenderSettingsInfos);
}


#if __ASSERT

	///////////////////////////////////////////////////////////////////////////
	//  CountBits
	///////////////////////////////////////////////////////////////////////////

	static inline unsigned CountBits(u8 m)
	{
		const unsigned count2 = (m&0x55)      + ((m>>1)&0x55);
		const unsigned count4 = (count2&0x33) + ((count2>>2)&0x33);
		const unsigned count8 = (count4&0x0f) + (count4>>4);
		return count8;
	}


	///////////////////////////////////////////////////////////////////////////
	//  InvariantInst
	///////////////////////////////////////////////////////////////////////////

	void decalManager::InvariantInst(const decalInst *pDecalInst, u8 *allUsedMibs, u8 *allUsedVtxs, unsigned *inUseMibs, unsigned *inUseVtxs, decalMethod method, u16 *decalIdCounts) const
	{
		const decalMiniIdxBuf *const pMibBase = m_pools.GetMibPoolBase();
		unsigned numTris = 0;
		unsigned numVtxs = 0;
		u8 instUsedVtxs[(DECAL_MAX_VERTS+7)>>3] = {0};

		if (decalIdCounts)
		{
			const int decalId = pDecalInst->m_id;
			const u32 decalIdIdx = (u32)(decalId-0x00010000)>>16;
			const u32 decalIdSeq = (u32)decalId&0xffff;
			decalAssert(decalIdIdx < NELEM(m_decalIdAlive));
			decalAssert(m_decalIdAlive[decalIdIdx] == decalIdSeq);
			++(decalIdCounts[decalIdIdx]);
		}

		// Iterate over the instance's mini index buffers.
		u16 mibIdx = pDecalInst->m_idxBufList;
		bool firstMib = true;
		decalAssert((method==DECAL_METHOD_DYNAMIC) ^ (mibIdx!=decalMiniIdxBuf::NULL_IDX));
		while (mibIdx != decalMiniIdxBuf::NULL_IDX)
		{
			// Ensure that the mini index buffer isn't already in use by another instance.
			const unsigned mibByte = mibIdx>>3;
			const u8 mibMask = (0x80>>(mibIdx&7));
			decalAssert((allUsedMibs[mibByte] & mibMask) == 0);
			allUsedMibs[mibByte] |= mibMask;
			++*inUseMibs;

			// Iterate over the indices in the mini index buffer.  Validate that
			// no vertex is being used by another instance.
			const decalMiniIdxBuf *const mib = decalMiniIdxBuf::GetPtr(pMibBase, mibIdx);
			const unsigned mibNumTris = mib->GetNumTris();
			decalAssert(0<mibNumTris && mibNumTris<=(unsigned)decalMiniIdxBuf::MAX_NUM_TRIS);
			decalAssert(firstMib || mibNumTris==decalMiniIdxBuf::MAX_NUM_TRIS);
			firstMib = false;
			const unsigned mibNumIdxs = mibNumTris*3;
			numTris += mibNumTris;
			for (unsigned idxIdx=0; idxIdx<mibNumIdxs; ++idxIdx)
			{
				const u16 vtxIdx = mib->m_idx[idxIdx];
				decalAssert(vtxIdx != decalMiniIdxBuf::INVALID_VTX_IDX);
				const unsigned vtxByte = vtxIdx>>3;
				const u8 vtxMask = (0x80>>(vtxIdx&7)) & ~instUsedVtxs[vtxByte];
				decalAssert((allUsedVtxs[vtxByte] & vtxMask) == 0);
				numVtxs += CountBits(vtxMask);
				instUsedVtxs[vtxByte] |= vtxMask;
				allUsedVtxs [vtxByte] |= vtxMask;
			}

			mibIdx = mib->GetNextIdx();
		}

		// Now loop over the list of vertices stored in the instance.  This
		// should agree with the vertices found by iterating over the mini index
		// buffers.
		unsigned numVtxs2 = 0;
		u16 vtxIdx = pDecalInst->m_vtxList;
		const u16 *const allocatedVertices = m_allocatedVertices;
		while (vtxIdx != 0xffff)
		{
			++numVtxs2;
			const unsigned vtxByte = vtxIdx>>3;
			const u8 vtxMask = 0x80>>(vtxIdx&7);
			decalAssert((instUsedVtxs[vtxByte] & vtxMask) != 0);
			decalAssert((allUsedVtxs [vtxByte] & vtxMask) != 0);
			vtxIdx = allocatedVertices[vtxIdx];
		}
		decalAssert(numVtxs == numVtxs2);
		decalAssert(numVtxs == pDecalInst->m_numVtxs);
		*inUseVtxs += numVtxs;
	}


	///////////////////////////////////////////////////////////////////////////
	//  Invariant
	///////////////////////////////////////////////////////////////////////////

	bool decalManager::Invariant() const
	{
		unsigned inUseBuckets = 0;
		unsigned inUseInsts   = 0;
		unsigned inUseMibs    = 0;
		unsigned inUseVtxs    = 0;

		u8 allUsedMibs[(DECAL_MAX_MINI_IDX_BUFS+7)>>3] = {0};
		u8 allUsedVtxs[(DECAL_MAX_VERTS+7)>>3]         = {0};
		u16 decalIdCounts[DECAL_MAX_INSTS] = {0};

		// Loop through all active buckets and process their instances
		for (unsigned pass=0; pass<(unsigned)DECAL_NUM_RENDER_PASSES; ++pass)
		{
			for (unsigned method=0; method<(unsigned)DECAL_NUM_METHODS; ++method)
			{
				decalAssert(m_pendingBucketLists[pass][method].IsEmpty());
				decalAssert(m_synchronousBucketLists[pass][method].IsEmpty());

				const SList<decalBucket> *const buckets = &m_runningBucketLists[pass][method];
				const unsigned numBuckets = buckets->GetNumItems();
				decalAssert(numBuckets == buckets->CountNumItems());
				inUseBuckets += numBuckets;
				for (const decalBucket *bucket=buckets->Peek(); bucket; bucket=bucket->GetNext())
				{
					const SList<decalInst> *const instances = &bucket->m_instList;
					const unsigned numInsts = instances->GetNumItems();
					decalAssert(numInsts == instances->CountNumItems());
					inUseInsts += numInsts;
					for (const decalInst *instance=instances->Peek(); instance; instance=instance->GetNext())
					{
						InvariantInst(instance, allUsedMibs, allUsedVtxs, &inUseMibs, &inUseVtxs, (decalMethod)method, decalIdCounts);
					}
				}
			}
		}

		// Process all instances that are queued for delayed deletion
		for (unsigned frame=0; frame<(NUM_RENDER_LATENCY_BUFFERS + 1); ++frame)
		{
			for (const decalInst *instance=m_delayedDeletionQueues[frame].Peek(); instance; instance=instance->GetNext())
			{
				// Only static decals should ever be queued for delayed deletion
				InvariantInst(instance, allUsedMibs, allUsedVtxs, &inUseMibs, &inUseVtxs, DECAL_METHOD_STATIC, NULL);
				++inUseInsts;
			}
		}

		// Check that the mini index buffer and vertex free lists don't reference anything that is in use
		const decalMiniIdxBufFree *const pMibBase = m_pools.m_mibs;
		for (const decalMiniIdxBufFree *mib=m_pools.m_mibPool.SyncPeek(); mib; mib=mib->GetNext())
		{
			const unsigned mibIdx  = (unsigned)(mib-pMibBase);
			const unsigned mibByte = mibIdx>>3;
			const u8 mibMask = (0x80>>(mibIdx&7));
			decalAssert((allUsedMibs[mibByte] & mibMask) == 0);
		}
		const decalVtxFree *const pVtxBase = m_pools.m_verts;
		for (const decalVtxFree *vtx=m_pools.m_vtxPool.SyncPeek(); vtx; vtx=vtx->GetNext())
		{
			const unsigned vtxIdx  = (unsigned)(vtx-pVtxBase);
			const unsigned vtxByte = vtxIdx>>3;
			const u8 vtxMask = (0x80>>(vtxIdx&7));
			decalAssert((allUsedVtxs[vtxByte] & vtxMask) == 0);
		}

		// Further increment the decal id counters for each pending add info struct
		for (unsigned i=0; i<m_numAddInfos; ++i)
		{
			const int decalId = m_addInfos[i].id;
			const u32 decalIdIdx = (u32)(decalId-0x00010000)>>16;
			const u32 decalIdSeq = (u32)decalId&0xffff;
			decalAssert(decalIdIdx < NELEM(m_decalIdAlive));
			decalAssert(m_decalIdAlive[decalIdIdx] == decalIdSeq);
			++(decalIdCounts[decalIdIdx]);
		}
		// Check that any decal id counts are correct, and count the number of ids in use
		unsigned inUseDecalIds = 0;
		for (unsigned i=0; i<NELEM(decalIdCounts); ++i)
		{
			if (decalIdCounts[i])
			{
				decalAssert(m_decalIdCountFree[i] == decalIdCounts[i]);
				++inUseDecalIds;
			}
		}
		// Count the number of free decal ids
		unsigned freeDecalIds = 0;
		for (unsigned i=m_decalIdSeqFree&0xffff; i!=0xffff; i=m_decalIdCountFree[i])
		{
			++freeDecalIds;
		}

		// Check that everything is accounted for and nothing has gotten lost
		const unsigned freeBuckets = m_pools.m_bucketPool .GetNumItems();
		const unsigned freeInsts   = m_pools.m_instPool   .GetNumItems();
		const unsigned freeMibs    = m_pools.m_mibPool    .GetNumItems();
		const unsigned freeVtxs    = m_pools.m_vtxPool    .GetNumItems();
		decalAssert(freeBuckets == m_pools.m_bucketPool .SyncCountNumItems());
		decalAssert(freeInsts   == m_pools.m_instPool   .SyncCountNumItems());
		decalAssert(freeMibs    == m_pools.m_mibPool    .SyncCountNumItems());
		decalAssert(freeVtxs    == m_pools.m_vtxPool    .SyncCountNumItems());
		decalAssert(inUseBuckets  + freeBuckets  == DECAL_MAX_BUCKETS);
		decalAssert(inUseInsts    + freeInsts    == DECAL_MAX_INSTS);
		decalAssert(inUseMibs     + freeMibs     == DECAL_MAX_MINI_IDX_BUFS);
		decalAssert(inUseVtxs     + freeVtxs     == DECAL_MAX_VERTS);
		decalAssert(inUseDecalIds + freeDecalIds == DECAL_MAX_INSTS);

		return true;
	}

#endif // __ASSERT


///////////////////////////////////////////////////////////////////////////////
//  decalPhInstDtorCallback
///////////////////////////////////////////////////////////////////////////////

static void decalPhInstDtorCallback(phInst *inst)
{
	DECALMGR.OnDelete(inst);
}


///////////////////////////////////////////////////////////////////////////////
//  decalPhInstArchetypeChangedCallback
///////////////////////////////////////////////////////////////////////////////

static void decalPhInstArchetypeChangedCallback(phInst *inst, phArchetype * /*newArchetype*/)
{
	DECALMGR.OnDelete(inst);
}


#if RSG_PC && __D3D11

	///////////////////////////////////////////////////////////////////////////
	//  CreateDX11Buffers
	///////////////////////////////////////////////////////////////////////////

	void decalManager::CreateDX11Buffers(ReinitArg reinitArg)
	{
		DestroyDX11Buffers();

		ID3D11Device *const dev = GRCDEVICE.GetCurrent();

		const D3D11_SUBRESOURCE_DATA *const noInitialData = NULL;
		D3D11_SUBRESOURCE_DATA initFromPool;
		initFromPool.pSysMem = m_pools.GetVtxPoolBase();

		// Create the primary vertex buffer
		D3D11_BUFFER_DESC vtxBufDesc;
		vtxBufDesc.ByteWidth                = DECAL_MAX_VERTS * sizeof(decalVtx);
		vtxBufDesc.Usage                    = D3D11_USAGE_DYNAMIC;
		vtxBufDesc.BindFlags                = D3D11_BIND_VERTEX_BUFFER;
		vtxBufDesc.CPUAccessFlags           = D3D11_CPU_ACCESS_WRITE;
		vtxBufDesc.MiscFlags                = 0;
		vtxBufDesc.StructureByteStride      = sizeof(decalVtx);
		CHECK_HRESULT(dev->CreateBuffer(&vtxBufDesc, reinitArg==UNINITIALIZED_VERTICES?noInitialData:&initFromPool, &m_staticVtxBuf));

		for (unsigned i=0; i<NUM_PER_CONTEXT_BUFFERS; ++i)
		{
			// Create the per frame index buffers
			D3D11_BUFFER_DESC idxBufDesc;
			idxBufDesc.ByteWidth                = DECAL_MAX_TRIS * 3 * sizeof(u16);
			idxBufDesc.Usage                    = D3D11_USAGE_DYNAMIC;
			idxBufDesc.BindFlags                = D3D11_BIND_INDEX_BUFFER;
			idxBufDesc.CPUAccessFlags           = D3D11_CPU_ACCESS_WRITE;
			idxBufDesc.MiscFlags                = 0;
			idxBufDesc.StructureByteStride      = sizeof(u16);
			CHECK_HRESULT(dev->CreateBuffer(&idxBufDesc, noInitialData, &m_staticIdxBufHeader[i]));

			// Create the per frame vertex buffer that is an index into the constant buffer
			D3D11_BUFFER_DESC cbufIdxBufDesc;
			cbufIdxBufDesc.ByteWidth            = DECAL_MAX_VERTS * sizeof(decalCbufIdx);
			cbufIdxBufDesc.Usage                = D3D11_USAGE_DYNAMIC;
			cbufIdxBufDesc.BindFlags            = D3D11_BIND_VERTEX_BUFFER;
			cbufIdxBufDesc.CPUAccessFlags       = D3D11_CPU_ACCESS_WRITE;
			cbufIdxBufDesc.MiscFlags            = 0;
			cbufIdxBufDesc.StructureByteStride  = sizeof(decalCbufIdx);
			CHECK_HRESULT(dev->CreateBuffer(&cbufIdxBufDesc, noInitialData, &m_staticConstBufIdxHeader[i]));
		}

		m_mostRecentlyUsedPerContextBuffer = 0;
		sysMemSet(m_staticIdxBufLastAdded, 0, sizeof(unsigned) * NUM_PER_CONTEXT_BUFFERS);

		// Create the constant buffer
		D3D11_BUFFER_DESC cbufDesc;
		cbufDesc.ByteWidth                  = 256 * sizeof(decalPerInstCBuf);
		cbufDesc.Usage                      = D3D11_USAGE_DYNAMIC;
		cbufDesc.BindFlags                  = D3D11_BIND_CONSTANT_BUFFER;
		cbufDesc.CPUAccessFlags             = D3D11_CPU_ACCESS_WRITE;
		cbufDesc.MiscFlags                  = 0;
		cbufDesc.StructureByteStride        = sizeof(decalPerInstCBuf);
		CHECK_HRESULT(dev->CreateBuffer(&cbufDesc, noInitialData, &m_staticConstBuf));
	}


	///////////////////////////////////////////////////////////////////////////
	//  DestroyDX11Buffers
	///////////////////////////////////////////////////////////////////////////

	void decalManager::DestroyDX11Buffers()
	{
		SAFE_RELEASE_RESOURCE(m_staticVtxBuf);
		for (unsigned i=0; i<NUM_PER_CONTEXT_BUFFERS; ++i)
		{
			SAFE_RELEASE_RESOURCE(m_staticIdxBufHeader[i]);
			SAFE_RELEASE_RESOURCE(m_staticConstBufIdxHeader[i]);
		}
		SAFE_RELEASE_RESOURCE(m_staticConstBuf);
	}


	///////////////////////////////////////////////////////////////////////////
	//  DeviceLostCallback
	///////////////////////////////////////////////////////////////////////////

	/*static*/ void decalManager::DeviceLostCallback()
	{
	}


	///////////////////////////////////////////////////////////////////////////
	//  DeviceResetCallback
	///////////////////////////////////////////////////////////////////////////

	/*static*/ void decalManager::DeviceResetCallback()
	{
		DECALMGR.CreateDX11Buffers(VERTICES_INITIALIZED_FROM_POOL);
	}


	///////////////////////////////////////////////////////////////////////////
	//  PrimaryRenderThreadBeginFrameCallback
	///////////////////////////////////////////////////////////////////////////

	/*static*/ void decalManager::PrimaryRenderThreadBeginFrameCallback()
	{
		decalManager *const self = ms_instance;
		if (!self || !self->m_bInitialized)
		{
			return;
		}

		// Copy any newly generated vertices into the GPU's copy of the vertex
		// buffer.
		const bool copyAll = self->m_gpuUploadRenderThreadCopyAll;
		const u32 begin    = self->m_gpuUploadRenderThreadBegin;
		const u32 end      = self->m_gpuUploadRenderThreadEnd;

		// Note that while it is tempting to early out here if there is nothing
		// to be copied, that is actually slower!  Stupid drivers seem to be
		// moving the vertex buffer to slower memory if not accessed all the
		// time, causing very long stalls the first time it is accessed again.

		ID3D11DeviceContext *const ctxD3D11 = g_grcCurrentContext;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		const UINT subresource = 0;
		const D3D11_MAP mapType = D3D11_MAP_WRITE_NO_OVERWRITE;
		const UINT mapFlags = 0;
		Assert(ctxD3D11 != NULL);
		Assert(self->m_staticVtxBuf != NULL);
		if (ctxD3D11 && self->m_staticVtxBuf)
		{
			PF_PUSH_TIMEBAR_BUDGETED("Decal Vertex Bufffer Map", 0.2f);
			HRESULT hResult = ctxD3D11->Map(self->m_staticVtxBuf, subresource, mapType, mapFlags, &mappedResource); 
			CHECK_HRESULT(hResult);
			if (SUCCEEDED(hResult))
			{
				decalVtx *const gpuPtr = (decalVtx*)mappedResource.pData;
				const decalVtx *const cpuPtr = self->m_pools.GetVtxPoolBase();
				PF_POP_TIMEBAR();

				PF_PUSH_TIMEBAR_BUDGETED("Decal Vertex Buffer Update", 0.2f);
				if (copyAll)
				{
					sysMemCpy(gpuPtr, cpuPtr, DECAL_MAX_VERTS*sizeof(decalVtx));
					self->m_gpuUploadRenderThreadCopyAll = false;
				}
				else
				{
					CompileTimeAssert((NELEM(self->m_gpuUploadIndices) & (NELEM(self->m_gpuUploadIndices)-1)) == 0);
					for (unsigned ii=begin; ii!=end; ii=((ii+1)&(NELEM(self->m_gpuUploadIndices)-1)))
					{
						const unsigned i = self->m_gpuUploadIndices[ii];
						gpuPtr[i] = cpuPtr[i];
					}
				}
				self->m_gpuUploadRenderThreadBegin = end;
				PF_POP_TIMEBAR();

				PF_PUSH_TIMEBAR_BUDGETED("Decal Vertex Buffer Unmap", 0.01f);
				ctxD3D11->Unmap(self->m_staticVtxBuf, subresource);
				PF_POP_TIMEBAR();
			}
			else
			{
				Assertf(false, "Failed to Map decalManager static vertex buffers in PrimaryRenderThreadBeginFrameCallback");
			}
		}
	}

#endif // RSG_PC && __D3D11


///////////////////////////////////////////////////////////////////////////////
//  InitShaders
///////////////////////////////////////////////////////////////////////////////

void decalManager::InitShaders()
{
	ASSET.PushFolder("common:/shaders");

	// init the shaders
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX].Init("vfx_decal");
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX_STEEP].Init("vfx_decal_steep");
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX_GLASS].Init("vfx_decal_glass");
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP].Init("vfx_decal_glass_steep");
	m_shaderStatic[DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA].Init("vfx_decal_rotate_towards_camera");
	for (unsigned i=0; i<NELEM(m_shaderStatic); ++i)
	{
		m_shaderStatic[i].LookUpVars();
	}

	m_shaderDynamic[DECAL_DYNAMIC_SHADER_PARALLAX].Init(MSAA_ONLY(GRCDEVICE.GetMSAA() > 1 ? "vfx_decaldynMS": ) "vfx_decaldyn");
	m_shaderDynamic[DECAL_DYNAMIC_SHADER_PARALLAX_STEEP].Init(MSAA_ONLY(GRCDEVICE.GetMSAA() > 1 ? "vfx_decaldyn_steepMS": ) "vfx_decaldyn_steep");

	m_shaderDynamic[DECAL_DYNAMIC_SHADER_PARALLAX].LookUpVars();
	m_shaderDynamic[DECAL_DYNAMIC_SHADER_PARALLAX_STEEP].LookUpVars();

	ASSET.PopFolder();
}

///////////////////////////////////////////////////////////////////////////////
//  DeleteShaders
///////////////////////////////////////////////////////////////////////////////
#if RSG_PC
void decalManager::DeleteShaders()
{
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX].Shutdown();
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX_STEEP].Shutdown();
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX_GLASS].Shutdown();
	m_shaderStatic[DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP].Shutdown();
	m_shaderStatic[DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA].Shutdown();

	m_shaderDynamic[DECAL_DYNAMIC_SHADER_PARALLAX].Shutdown();
	m_shaderDynamic[DECAL_DYNAMIC_SHADER_PARALLAX_STEEP].Shutdown();
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  ResetShaders
///////////////////////////////////////////////////////////////////////////////
#if RSG_PC
void decalManager::ResetShaders()
{
	DeleteShaders();
	InitShaders();
}
#endif


///////////////////////////////////////////////////////////////////////////////
//  DecalRemovedCallbackNop
///////////////////////////////////////////////////////////////////////////////

static void DecalRemovedCallbackNop(int)
{
}


///////////////////////////////////////////////////////////////////////////////
//  Init
///////////////////////////////////////////////////////////////////////////////

void decalManager::Init(unsigned initMode)
{
	if (initMode==INIT_CORE)
	{
		// task management
		m_runningAsyncTaskDesc = NULL;
		m_runningSyncTaskDesc  = NULL;

		m_renderSema = sysIpcCreateSema(DECAL_NUMBER_OF_RENDER_THREADS);

#		if RSG_PS3
			sysMemSet(m_spuTasks, 0, sizeof(m_spuTasks));
#		endif

		m_taskDataStoreBase = (u8*)sysMemAllocator::GetCurrent().RAGE_LOG_ALLOCATE(DECAL_DATA_STORE_BYTES, 16);
		m_taskDataStoreFreeLower = m_taskDataStoreBase;
		m_taskDataStoreFreeUpper = m_taskDataStoreBase + DECAL_DATA_STORE_BYTES;

#		if RSG_PC && __D3D11
			m_gpuUploadRenderThreadCopyAll          = false;
			m_gpuUploadRenderThreadBegin            = 0;
			m_gpuUploadRenderThreadEnd              = 0;
			m_gpuUploadWorkerThreadsSubFrom         = NELEM(m_gpuUploadIndices)-1;
			m_gpuUploadWorkerThreadsSpaceRemaining  = NELEM(m_gpuUploadIndices);
#		endif

		// set the wrap sampler state (wrap/linear is default)	
		grcSamplerStateDesc descWrap;
		m_samplerStateWrap = grcStateBlock::CreateSamplerState(descWrap);

		// set the clamp sampler state
		grcSamplerStateDesc descClamp;
		descClamp.AddressU = grcSSV::TADDRESS_CLAMP;
		descClamp.AddressV = grcSSV::TADDRESS_CLAMP;
		m_samplerStateClamp = grcStateBlock::CreateSamplerState(descClamp);

		// set the aniso sampler state (clamp)
		grcSamplerStateDesc descAniso;		
		descAniso.AddressU = grcSSV::TADDRESS_CLAMP;
		descAniso.AddressV = grcSSV::TADDRESS_CLAMP;
		descAniso.Filter = grcSSV::FILTER_ANISOTROPIC;
		descAniso.MinLod = 0.0f;
		descAniso.MaxLod = 2.0f;
		descAniso.MaxAnisotropy = 6;
		m_samplerStateAnisoClamp = grcStateBlock::CreateSamplerState(descAniso);

		// set the aniso sampler state (wrap)
		descAniso.AddressU = grcSSV::TADDRESS_WRAP;
		descAniso.AddressV = grcSSV::TADDRESS_WRAP;
		descAniso.Filter = grcSSV::FILTER_ANISOTROPIC;
		descAniso.MinLod = 0.0f;
		descAniso.MaxLod = 2.0f;
		descAniso.MaxAnisotropy = 6;
		m_samplerStateAnisoWrap = grcStateBlock::CreateSamplerState(descAniso);

		// set the sampler state for vehicle damage
#		if !USE_EDGE
			grcSamplerStateDesc descDamage;
			descDamage.Filter = grcSSV::FILTER_MIN_MAG_MIP_POINT;
			m_samplerStateDamage = grcStateBlock::CreateSamplerState(descDamage);
#		endif

		InitShaders();

		InitRenderStateBlocks();

		m_vtxBufferDynamic.Init();

		m_allocatedVertices = rage_new u16[DECAL_MAX_VERTS];
		sysMemSet(m_allocatedVertices, 0xff, DECAL_MAX_VERTS*sizeof(u16));

		// We are creating somewhat odd vertex declarations here, where the
		// texcoord stream is overlapping the position stream (only texcoord y,
		// z and w components are used on non-PS3 platforms).  Because of this,
		// the usual grcDevice::CreateVertexDeclaration() interface cannot be
		// used.
#		if __XENON
			static const D3DVERTEXELEMENT9 s_vtxElems[] =
			{
				{0, 0,  D3DDECLTYPE_SHORT4N,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
				{0, 4,  D3DDECLTYPE_UBYTE4N,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
				{0, 8,  D3DDECLTYPE_FLOAT16_2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
				{0, 12, D3DDECLTYPE_HEND3N,    D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0},
				{0, 16, D3DDECLTYPE_DEC4N,     D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,  0},
				D3DDECL_END()
			};
			sysMemStartTemp();
			m_staticVtxDecl = rage_new grcVertexDeclaration;
			sysMemEndTemp();
			GRCDEVICE.GetCurrent()->CreateVertexDeclaration(s_vtxElems, &m_staticVtxDecl->D3dDecl);
			sysMemSet(m_staticVtxDecl->Divider, 0, sizeof(m_staticVtxDecl->Divider));
			m_staticVtxDecl->Stream0Size = sizeof(decalVtx);

#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				static const D3DVERTEXELEMENT9 s_debugVtxElems[] =
				{
					{0, 20, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
					{0, 32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
					{0, 40, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
					{0, 48, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL,   0},
					{0, 60, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT,  0},
					D3DDECL_END()
				};
				sysMemStartTemp();
				m_debugStaticVtxDecl = rage_new grcVertexDeclaration;
				sysMemEndTemp();
				GRCDEVICE.GetCurrent()->CreateVertexDeclaration(s_debugVtxElems, &m_debugStaticVtxDecl->D3dDecl);
				sysMemSet(m_debugStaticVtxDecl->Divider, 0, sizeof(m_debugStaticVtxDecl->Divider));
				m_debugStaticVtxDecl->Stream0Size = sizeof(decalVtx);
#			endif

#		elif RSG_DURANGO || (RSG_PC && __D3D11)
			const unsigned elementCount = 6;
			m_staticVtxDecl = (grcVertexDeclaration*)(rage_new char[sizeof(grcVertexDeclaration) + elementCount * sizeof(D3D11_INPUT_ELEMENT_DESC)]);
			m_staticVtxDecl->elementCount = elementCount;
			m_staticVtxDecl->refCount = 1;
			m_staticVtxDecl->Stream0Size = sizeof(decalVtx);

			m_staticVtxDecl->desc[0].SemanticName           = "POSITION";
			m_staticVtxDecl->desc[0].SemanticIndex          = 0;
			m_staticVtxDecl->desc[0].Format                 = DXGI_FORMAT_R16G16B16A16_SNORM;
			m_staticVtxDecl->desc[0].InputSlot              = 0;
			m_staticVtxDecl->desc[0].AlignedByteOffset      = 0;
			m_staticVtxDecl->desc[0].InputSlotClass         = D3D11_INPUT_PER_VERTEX_DATA;
			m_staticVtxDecl->desc[0].InstanceDataStepRate   = 0;

			m_staticVtxDecl->desc[1].SemanticName           = "TEXCOORD";
			m_staticVtxDecl->desc[1].SemanticIndex          = 0;
			m_staticVtxDecl->desc[1].Format                 = DXGI_FORMAT_R8G8B8A8_UNORM;
			m_staticVtxDecl->desc[1].InputSlot              = 0;
			m_staticVtxDecl->desc[1].AlignedByteOffset      = 4;
			m_staticVtxDecl->desc[1].InputSlotClass         = D3D11_INPUT_PER_VERTEX_DATA;
			m_staticVtxDecl->desc[1].InstanceDataStepRate   = 0;

			m_staticVtxDecl->desc[2].SemanticName           = "TEXCOORD";
			m_staticVtxDecl->desc[2].SemanticIndex          = 1;
			m_staticVtxDecl->desc[2].Format                 = DXGI_FORMAT_R16G16_FLOAT;
			m_staticVtxDecl->desc[2].InputSlot              = 0;
			m_staticVtxDecl->desc[2].AlignedByteOffset      = 8;
			m_staticVtxDecl->desc[2].InputSlotClass         = D3D11_INPUT_PER_VERTEX_DATA;
			m_staticVtxDecl->desc[2].InstanceDataStepRate   = 0;

			m_staticVtxDecl->desc[3].SemanticName           = "NORMAL";
			m_staticVtxDecl->desc[3].SemanticIndex          = 0;
			m_staticVtxDecl->desc[3].Format                 = DXGI_FORMAT_R10G10B10A2_UNORM;
			m_staticVtxDecl->desc[3].InputSlot              = 0;
			m_staticVtxDecl->desc[3].AlignedByteOffset      = 12;
			m_staticVtxDecl->desc[3].InputSlotClass         = D3D11_INPUT_PER_VERTEX_DATA;
			m_staticVtxDecl->desc[3].InstanceDataStepRate   = 0;

			m_staticVtxDecl->desc[4].SemanticName           = "TANGENT";
			m_staticVtxDecl->desc[4].SemanticIndex          = 0;
			m_staticVtxDecl->desc[4].Format                 = DXGI_FORMAT_R10G10B10A2_UNORM;
			m_staticVtxDecl->desc[4].InputSlot              = 0;
			m_staticVtxDecl->desc[4].AlignedByteOffset      = 16;
			m_staticVtxDecl->desc[4].InputSlotClass         = D3D11_INPUT_PER_VERTEX_DATA;
			m_staticVtxDecl->desc[4].InstanceDataStepRate   = 0;

			m_staticVtxDecl->desc[5].SemanticName           = "TEXCOORD";
			m_staticVtxDecl->desc[5].SemanticIndex          = 2;
			CompileTimeAssert(sizeof(decalCbufIdx)==1 || sizeof(decalCbufIdx)==4);
			m_staticVtxDecl->desc[5].Format                 = sizeof(decalCbufIdx)==1 ? DXGI_FORMAT_R8_UINT : DXGI_FORMAT_R32_UINT;
			m_staticVtxDecl->desc[5].InputSlot              = 1;
			m_staticVtxDecl->desc[5].AlignedByteOffset      = 0;
			m_staticVtxDecl->desc[5].InputSlotClass         = D3D11_INPUT_PER_VERTEX_DATA;
			m_staticVtxDecl->desc[5].InstanceDataStepRate   = 0;

#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				m_debugStaticVtxDecl = (grcVertexDeclaration*)(rage_new char[sizeof(grcVertexDeclaration) + elementCount * sizeof(D3D11_INPUT_ELEMENT_DESC)]);
				m_debugStaticVtxDecl->elementCount = elementCount;
				m_debugStaticVtxDecl->refCount = 1;
				m_debugStaticVtxDecl->Stream0Size = sizeof(decalVtx);

				m_debugStaticVtxDecl->desc[0].SemanticName          = "POSITION";
				m_debugStaticVtxDecl->desc[0].SemanticIndex         = 0;
				m_debugStaticVtxDecl->desc[0].Format                = DXGI_FORMAT_R32G32B32_FLOAT;
				m_debugStaticVtxDecl->desc[0].InputSlot             = 0;
				m_debugStaticVtxDecl->desc[0].AlignedByteOffset     = 20;
				m_debugStaticVtxDecl->desc[0].InputSlotClass        = D3D11_INPUT_PER_VERTEX_DATA;
				m_debugStaticVtxDecl->desc[0].InstanceDataStepRate  = 0;

				m_debugStaticVtxDecl->desc[1].SemanticName          = "TEXCOORD";
				m_debugStaticVtxDecl->desc[1].SemanticIndex         = 0;
				m_debugStaticVtxDecl->desc[1].Format                = DXGI_FORMAT_R32G32B32A32_FLOAT;
				m_debugStaticVtxDecl->desc[1].InputSlot             = 0;
				m_debugStaticVtxDecl->desc[1].AlignedByteOffset     = 24;
				m_debugStaticVtxDecl->desc[1].InputSlotClass        = D3D11_INPUT_PER_VERTEX_DATA;
				m_debugStaticVtxDecl->desc[1].InstanceDataStepRate  = 0;

				m_debugStaticVtxDecl->desc[2].SemanticName          = "TEXCOORD";
				m_debugStaticVtxDecl->desc[2].SemanticIndex         = 1;
				m_debugStaticVtxDecl->desc[2].Format                = DXGI_FORMAT_R32G32_FLOAT;
				m_debugStaticVtxDecl->desc[2].InputSlot             = 0;
				m_debugStaticVtxDecl->desc[2].AlignedByteOffset     = 40;
				m_debugStaticVtxDecl->desc[2].InputSlotClass        = D3D11_INPUT_PER_VERTEX_DATA;
				m_debugStaticVtxDecl->desc[2].InstanceDataStepRate  = 0;

				m_debugStaticVtxDecl->desc[3].SemanticName          = "NORMAL";
				m_debugStaticVtxDecl->desc[3].SemanticIndex         = 0;
				m_debugStaticVtxDecl->desc[3].Format                = DXGI_FORMAT_R32G32B32_FLOAT;
				m_debugStaticVtxDecl->desc[3].InputSlot             = 0;
				m_debugStaticVtxDecl->desc[3].AlignedByteOffset     = 48;
				m_debugStaticVtxDecl->desc[3].InputSlotClass        = D3D11_INPUT_PER_VERTEX_DATA;
				m_debugStaticVtxDecl->desc[3].InstanceDataStepRate  = 0;

				m_debugStaticVtxDecl->desc[4].SemanticName          = "TANGENT";
				m_debugStaticVtxDecl->desc[4].SemanticIndex         = 0;
				m_debugStaticVtxDecl->desc[4].Format                = DXGI_FORMAT_R32G32B32A32_FLOAT;
				m_debugStaticVtxDecl->desc[4].InputSlot             = 0;
				m_debugStaticVtxDecl->desc[4].AlignedByteOffset     = 60;
				m_debugStaticVtxDecl->desc[4].InputSlotClass        = D3D11_INPUT_PER_VERTEX_DATA;
				m_debugStaticVtxDecl->desc[4].InstanceDataStepRate  = 0;

				m_debugStaticVtxDecl->desc[5].SemanticName          = "TEXCOORD";
				m_debugStaticVtxDecl->desc[5].SemanticIndex         = 2;
				CompileTimeAssert(sizeof(decalCbufIdx)==1 || sizeof(decalCbufIdx)==4);
				m_debugStaticVtxDecl->desc[5].Format                = sizeof(decalCbufIdx)==1 ? DXGI_FORMAT_R8_UINT : DXGI_FORMAT_R32_UINT;
				m_debugStaticVtxDecl->desc[5].InputSlot             = 1;
				m_debugStaticVtxDecl->desc[5].AlignedByteOffset     = 0;
				m_debugStaticVtxDecl->desc[5].InputSlotClass        = D3D11_INPUT_PER_VERTEX_DATA;
				m_debugStaticVtxDecl->desc[5].InstanceDataStepRate  = 0;
#			endif

#		elif RSG_ORBIS
			// This vertex declaration is only used for generating fetch
			// shaders.  So all that is required is the existence and order of
			// the array semantics.
			const grcVertexElement dummyVtxElems[] =
			{
				grcVertexElement(0, grcVertexElement::grcvetPosition, 0, 12, grcFvf::grcdsFloat3),
				grcVertexElement(0, grcVertexElement::grcvetTexture,  0, 8,  grcFvf::grcdsFloat2),
				grcVertexElement(0, grcVertexElement::grcvetTexture,  1, 8,  grcFvf::grcdsFloat2),
				grcVertexElement(0, grcVertexElement::grcvetNormal,   0, 12, grcFvf::grcdsFloat3),
				grcVertexElement(0, grcVertexElement::grcvetTangent,  0, 16, grcFvf::grcdsFloat4),
				grcVertexElement(1, grcVertexElement::grcvetTexture,  2, 4,  grcFvf::grcdsFloat ),
			};
			m_staticDummyVtxDecl = GRCDEVICE.CreateVertexDeclaration(dummyVtxElems, NELEM(dummyVtxElems));

#		endif


		m_delayedDeletionFrame = 0;
#		if RSG_PC && __D3D11 && __ASSERT
			IDXGIDevice1 *pDXGIDevice1;
			HRESULT hr = GRCDEVICE.GetCurrent()->QueryInterface(__uuidof(IDXGIDevice1), (void**)&pDXGIDevice1);
			decalAssert(SUCCEEDED(hr));
			UINT maxLatency;
			hr = pDXGIDevice1->GetMaximumFrameLatency(&maxLatency);
			decalAssert(SUCCEEDED(hr));
			decalAssert(maxLatency <= (NUM_RENDER_LATENCY_BUFFERS + 1));
#		endif

		phInst::SetDtorCallback(decalPhInstDtorCallback);
		phInst::SetArchetypeChangeCallback(decalPhInstArchetypeChangedCallback);

#		if DECAL_PROCESS_BREAKABLES
			// set the breakable glass callback
			bgContactFunctor breakableGlassFunctor;
			breakableGlassFunctor.Bind(&BreakableStateChangedCB);
			bgGlassManager::SetContactFunctor(breakableGlassFunctor);
#		endif

		m_decalRemovedCallback = DecalRemovedCallbackNop;

		m_pools.Init();

#		if USE_DEFRAGMENTATION
			m_defragLocker.Init(DECAL_DEFRAG_LOCKER_SIZE);
#		endif

		// Due to having odd vertex formats, we cannot specify a grcFvf
		// structure, so we cannot use a grcVertexBuffer.  Instead we need to
		// bypass grcDevice and do things manually.
#		if RSG_DURANGO
			// Allocate the per context index buffers and cbuf idx vertex buffers.
			const unsigned indicesPerBuf = DECAL_MAX_TRIS*3;
			const SIZE_T sizeBytes = indicesPerBuf*sizeof(u16)*NELEM(m_staticIdxBufData)
				+ DECAL_MAX_VERTS*sizeof(decalCbufIdx)*NELEM(m_staticConstBufIdxData);
			m_staticIdxBufData[0] = (u16*) sysMemPhysicalAllocate(sizeBytes, RSG_CACHE_LINE_SIZE, PhysicalMemoryType::PHYS_MEM_GARLIC_WRITECOMBINE);
			decalAssert(m_staticIdxBufData[0]);
			for (unsigned i=1; i<NELEM(m_staticIdxBufData); ++i)
			{
				m_staticIdxBufData[i] = m_staticIdxBufData[i-1] + indicesPerBuf;
			}
			m_staticConstBufIdxData[0] = (decalCbufIdx*)(m_staticIdxBufData[NELEM(m_staticIdxBufData)-1] + indicesPerBuf);
			for (unsigned i=1; i<NELEM(m_staticConstBufIdxData); ++i)
			{
				m_staticConstBufIdxData[i] = m_staticConstBufIdxData[i-1] + DECAL_MAX_VERTS;
			}

			m_mostRecentlyUsedPerContextBuffer = 0;

#		elif RSG_PC && __D3D11

			// This is safe, it doesn't actually register multiple times, even
			// if we are called with INIT_SESSION multiple times.
			GRCDEVICE.RegisterDeviceLostCallbacks(MakeFunctor(DeviceLostCallback), MakeFunctor(DeviceResetCallback));

#		elif RSG_XENON
			m_staticIdxBufHeader = rage_new D3DIndexBuffer[NUM_RENDER_LATENCY_BUFFERS];
			for (unsigned i=0; i<NUM_RENDER_LATENCY_BUFFERS; ++i)
			{
				XGSetIndexBufferHeader(
					DECAL_MAX_MINI_IDX_BUFS*sizeof(decalMiniIdxBuf),
					0,
					D3DFMT_INDEX16,
					0,
					0,
					m_staticIdxBufHeader+i);
			}

			// Allocate the index buffers in write combining memory.  There are
			// two benefits to this.  Firstly it is a little bit faster to be
			// writing to.  And secondly, it's simpler, since we don't need to
			// worry about explicitly flushing the CPU cache to ensure that the
			// values are visible to the GPU.
			const unsigned bytesPerBuf = (DECAL_MAX_TRIS*3*sizeof(u16)+15)&~15;
			m_staticIdxBufData[0] = (u16*)XPhysicalAlloc(bytesPerBuf*NELEM(m_staticIdxBufData), MAXULONG_PTR, 0, PAGE_READWRITE|PAGE_WRITECOMBINE);
			XGOffsetResourceAddress(m_staticIdxBufHeader+0, m_staticIdxBufData[0]);
			for (unsigned i=1; i<NELEM(m_staticIdxBufData); ++i)
			{
				m_staticIdxBufData[i] = (u16*)((char*)m_staticIdxBufData[i-1] + bytesPerBuf);
				XGOffsetResourceAddress(m_staticIdxBufHeader+i, m_staticIdxBufData[i]);
			}

			m_staticIdxBufFrame = 0;

			m_staticVtxBuf = rage_new D3DVertexBuffer;
			XGSetVertexBufferHeader(
				DECAL_MAX_VERTS * sizeof(decalVtx),
				D3DUSAGE_CPU_CACHED_MEMORY,
				0,
				0,
				m_staticVtxBuf);
			XGOffsetResourceAddress(m_staticVtxBuf, m_pools.GetVtxPoolBase());

#		elif RSG_ORBIS
			// Currently allocating the buffers in WB/ONION, possibly would be
			// better in WC/GARLIC.
			const unsigned indicesPerBuf = DECAL_MAX_TRIS*3;
			const size_t sizeBytes = indicesPerBuf*sizeof(u16)*NELEM(m_staticIdxBufData)
				+ DECAL_MAX_VERTS*sizeof(decalCbufIdx)*NELEM(m_staticConstBufIdxData);
			m_staticIdxBufData[0] = (u16*) rage_aligned_new(64) u8[sizeBytes];
			for (unsigned i=1; i<NELEM(m_staticIdxBufData); ++i)
			{
				m_staticIdxBufData[i] = m_staticIdxBufData[i-1] + indicesPerBuf;
			}
			m_staticConstBufIdxData[0] = (decalCbufIdx*)(m_staticIdxBufData[NELEM(m_staticIdxBufData)-1] + indicesPerBuf);
			for (unsigned i=1; i<NELEM(m_staticConstBufIdxData); ++i)
			{
				m_staticConstBufIdxData[i] = m_staticConstBufIdxData[i-1] + DECAL_MAX_VERTS;
			}
			m_mostRecentlyUsedPerContextBuffer = 0;

#		elif RSG_PS3
			const unsigned vehDamageOffsetBufSize = DECAL_MAX_VERTS * sizeof(u32);
			m_vehDamageOffset[0] = (u32*)physical_new(vehDamageOffsetBufSize * NELEM(m_vehDamageOffset), 4);
			decalAssertf(m_vehDamageOffset[0],"failed to allocate vehicle damage buffers");
			m_vehDamageOffset[1] = (u32*)((char*)m_vehDamageOffset[0] + vehDamageOffsetBufSize);
			CompileTimeAssert(NELEM(m_vehDamageOffset) == 2);
			m_vehDamageFrame = 0;
			m_vehDamageRsxLabels = gcm::RsxSemaphoreRegistrar::Allocate(NELEM(m_vehDamageOffset));
			m_vehDamageSeqNum = 0;
			m_vehDamageRunningTask = NULL;
#		endif

			// init the debug
#		if __BANK
			m_debug.Init();
#		endif

		m_bInitialized = true;
	}


	// Not all tools explicitly call with INIT_SESSION, so do all session
	// initialization in the core initialization as well.
	if (initMode==INIT_SESSION || initMode==INIT_CORE)
	{
		m_numAddInfos = 0;

		sysMemSet(m_decalIdAlive, 0x00, sizeof(m_decalIdAlive));
		for (unsigned i=0; i<NELEM(m_decalIdCountFree)-1; ++i)
		{
			m_decalIdCountFree[i] = (u16)(i+1);
		}
		m_decalIdCountFree[NELEM(m_decalIdCountFree)-1] = 0xffff;
		m_decalIdSeqFree = 0;

#		if __ASSERT
			// bucket list
			for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
			{
				for (int m=0; m<DECAL_NUM_METHODS; m++)
				{
					decalAssertf(m_synchronousBucketLists[p][m].IsEmpty(), "synchronous bucket list [%d][%d] isn't empty", p, m);
					decalAssertf(m_pendingBucketLists[p][m].IsEmpty(), "pending bucket list [%d][%d] isn't empty", p, m);
					decalAssertf(m_runningBucketLists[p][m].IsEmpty(), "running bucket list [%d][%d] isn't empty", p, m);
				}
			}
#		endif

		decalAssert(Invariant());
	}

// 	// debug size info
// 	int sizeBucket = sizeof(decalBucket);
// 	int sizeInst = sizeof(decalInst);
// 	int sizeVtx = sizeof(decalVtx);
// 	
// 	int totalBucket = sizeBucket*DECAL_MAX_BUCKETS;
// 	int totalInst = sizeInst*DECAL_MAX_INSTS;
// 	int totalVtx = sizeVtx*DECAL_MAX_VERTS;
// 
// 	int totalMem = totalBucket + totalInst + totalVtx;
// 	totalMem++;
// 	totalMem--;
}


#if __BANK

	///////////////////////////////////////////////////////////////////////////
	//  InitWidgets
	///////////////////////////////////////////////////////////////////////////

	void decalManager::InitWidgets()
	{
		m_debug.InitWidgets();
	}

#endif // __BANK


///////////////////////////////////////////////////////////////////////////////
//  Shutdown
///////////////////////////////////////////////////////////////////////////////

void decalManager::Shutdown(unsigned shutdownMode)
{
	Synchronize();

	if (shutdownMode==SHUTDOWN_CORE)
	{
#		if USE_DEFRAGMENTATION
			m_defragLocker.Shutdown();
#		endif

#		if RSG_DURANGO
			sysMemPhysicalFree(m_staticIdxBufData[0]);
			sysMemSet(m_staticIdxBufData,        0, sizeof(m_staticIdxBufData));
			sysMemSet(m_staticConstBufIdxData,   0, sizeof(m_staticConstBufIdxData));

#		elif RSG_ORBIS
			delete[] m_staticIdxBufData[0];
			sysMemSet(m_staticIdxBufData,      0, sizeof(m_staticIdxBufData));
			sysMemSet(m_staticConstBufIdxData, 0, sizeof(m_staticConstBufIdxData));

#		elif RSG_PC && __D3D11

#		elif RSG_PS3
			physical_delete(m_vehDamageOffset[0]);
			sysMemSet(m_vehDamageOffset, 0, sizeof(m_vehDamageOffset));
			gcm::RsxSemaphoreRegistrar::Free(m_vehDamageRsxLabels, NELEM(m_vehDamageOffset));

#		elif RSG_XENON
			XPhysicalFree(m_staticIdxBufData[0]);
			sysMemSet(m_staticIdxBufData, 0, sizeof(m_staticIdxBufData));
			delete[] m_staticIdxBufHeader;
			m_staticIdxBufHeader = NULL;

			delete m_staticVtxBuf;

#		endif

		m_pools.Shutdown();

		phInst::SetDtorCallback(NULL);
		phInst::SetArchetypeChangeCallback(NULL);

#		if RSG_DURANGO || (RSG_PC && __D3D11) || RSG_XENON
			m_staticVtxDecl->Release();
			m_staticVtxDecl = NULL;
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				m_debugStaticVtxDecl->Release();
				m_debugStaticVtxDecl = NULL;
#			endif
#		elif RSG_ORBIS
			m_staticDummyVtxDecl->Release();
			m_staticDummyVtxDecl = NULL;
#		endif

		m_vtxBufferDynamic.Shutdown();
		for (unsigned i=0; i<NELEM(m_shaderDynamic); ++i)
		{
			m_shaderDynamic[i].Shutdown();
		}

		for (unsigned i=0; i<NELEM(m_shaderStatic); ++i)
		{
			m_shaderStatic[i].Shutdown();
		}

		sysIpcDeleteSema(m_renderSema);

		RAGE_LOG_DELETE(m_taskDataStoreBase);
		sysMemAllocator::GetCurrent().Free(m_taskDataStoreBase);
	}
	else if (shutdownMode==SHUTDOWN_SESSION)
	{
		decalAssert(Invariant());

		// shutdown bucket lists
		for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
		{
			for (int m=0; m<DECAL_NUM_METHODS; m++)
			{
				decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
				if (pCurrBucket)
				{
					decalBucket* const pHeadBucket = pCurrBucket;
					decalBucket* pTailBucket;
					unsigned count = 0;
					do
					{
						pCurrBucket->MarkForDelete();
						pCurrBucket->ShutdownImmediate();

						++count;
						pTailBucket = pCurrBucket;
						pCurrBucket = pCurrBucket->GetNext();
					}
					while (pCurrBucket);
					m_pools.ReturnBuckets(pHeadBucket, pTailBucket, count);
					m_runningBucketLists[p][m].SetEmpty();
				}

				decalAssertf(m_synchronousBucketLists[p][m].IsEmpty(), "synchronous bucket list [%d][%d] isn't empty", p, m);
				decalAssertf(m_pendingBucketLists[p][m].IsEmpty(), "pending bucket list [%d][%d] isn't empty", p, m);
				decalAssertf(m_runningBucketLists[p][m].IsEmpty(), "running bucket list [%d][%d] isn't empty", p, m);
			}
		}

		for (unsigned f=0; f<(NUM_RENDER_LATENCY_BUFFERS + 1); ++f)
		{
			SList<decalInst> *const ddq = m_delayedDeletionQueues+f;
			decalInst *const pInstHead = ddq->Peek();
			decalInst *pInstTail = pInstHead;
			for (decalInst *pInst=pInstHead; pInst; pInst=pInst->GetNext())
			{
				pInst->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_DONT_RELEASE_DECAL_ID);
				pInstTail = pInst;
			}
			m_pools.ReturnInsts(pInstHead, pInstTail, ddq->GetNumItems());
			ddq->SetEmpty();
		}

		decalAssert(Invariant());
	}
}


///////////////////////////////////////////////////////////////////////////////
//  GetMergableBucket
///////////////////////////////////////////////////////////////////////////////

decalBucket* decalManager::GetMergableBucket(decalRenderPass renderPass, decalMethod method, Vec3V_In vPosition, u16 typeSettingsIndex, u16 renderSettingsIndex, u32 msOffsetInt, u16 subType, decalAttachType attachType, fwEntity* pAttachEntity, s16 attachMatrixId, void* pSmashGroup, bool isGlass, bool isDamaged, bool forceRegen, fwInteriorLocation interiorLocation, bool isScripted)
{
	// Disable bucket merging when the camera rotation shader is used.
	// Otherwise we would need to set the rotation center per instance rather
	// than per bucket.  For such a special case, not worth adding this overhead
	// to every instance render.
	const decalRenderSettings *const pRenderSettings = decalSettingsManager::GetRenderSettings(renderSettingsIndex);
	decalAssert(pRenderSettings);
	if (Unlikely(pRenderSettings->shaderId == DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA))
	{
		return NULL;
	}

	// Look through the active buckets for a suitable one.
	decalBucket* pCurrBucket = m_runningBucketLists[renderPass][method].Peek();
	while (pCurrBucket)
	{
		if (pCurrBucket->m_attachEntity == pAttachEntity &&
			pCurrBucket->m_attachMatrixId == attachMatrixId &&
			pCurrBucket->GetModelSpaceOffsetIntU32() == msOffsetInt &&
			pCurrBucket->m_attachType == attachType &&
			pCurrBucket->m_pSmashGroup == pSmashGroup &&
			pCurrBucket->IsGlass() == isGlass &&
			pCurrBucket->m_isDamaged == isDamaged &&
			pCurrBucket->m_forceRegen == forceRegen &&
			pCurrBucket->m_typeSettingsIndex == typeSettingsIndex &&
			pCurrBucket->m_renderSettingsIndex == renderSettingsIndex &&
			pCurrBucket->m_subType == subType &&
			pCurrBucket->GetInteriorLocation().GetAsUint32() == interiorLocation.GetAsUint32() &&
			pCurrBucket->IsScripted() == isScripted)
		{
			if (isGlass)
			{
				// only use this bucket on glass if it's close enough
				// we don't want glass buckets to get too large as the forward renderer needs to set up lighting
				Vec3V vDist = vPosition - pCurrBucket->GetBoundSphere().GetXYZ();
				if (IsLessThanOrEqualAll(MagSquared(vDist), ScalarV(V_FOUR)))
				{
					return pCurrBucket;
				}
			}
			else
			{
				// it is - use this bucket
				return pCurrBucket;
			}
		}

		pCurrBucket = pCurrBucket->GetNext();
	}

	return NULL;
}


///////////////////////////////////////////////////////////////////////////////
//  GetMergableBucket
///////////////////////////////////////////////////////////////////////////////

decalBucket* decalManager::GetMergableBucket(decalRenderPass renderPass, decalMethod method, const decalBucket *pBucket)
{
	const bool forceRegen = false;
	return GetMergableBucket(
		renderPass,
		method,
		pBucket->GetBoundSphere().GetXYZ(),
		pBucket->m_typeSettingsIndex,
		pBucket->m_renderSettingsIndex,
		pBucket->GetModelSpaceOffsetIntU32(),
		pBucket->m_subType,
		pBucket->m_attachType,
		pBucket->m_attachEntity,
		pBucket->m_attachMatrixId,
		pBucket->m_pSmashGroup,
		pBucket->IsGlass(),
		pBucket->m_isDamaged,
		forceRegen,
		pBucket->GetInteriorLocation(),
		pBucket->IsScripted());
}


///////////////////////////////////////////////////////////////////////////////
//  OrderredInsertBucket
///////////////////////////////////////////////////////////////////////////////

void decalManager::OrderredInsertBucket(decalRenderPass renderPass, decalMethod method, decalBucket *pNewBucket)
{
#if __BANK
	if (m_debug.m_disableBucketSorting)
	{
		m_runningBucketLists[renderPass][method].Push(pNewBucket);
	}
	else
#endif
	{
		// insert new bucket into bucket list at correct position to keep the list sorted on ascending z-shift
		const float newBucketZShiftOffset = decalSettingsManager::GetTypeSettings(pNewBucket->m_typeSettingsIndex)->zShiftOffset;

		decalBucket **ppCurrBucket = m_runningBucketLists[renderPass][method].GetPtrPtrHead();
		decalBucket *pCurrBucket = *ppCurrBucket;
		while (pCurrBucket)
		{
			const float currBucketZShiftOffset = decalSettingsManager::GetTypeSettings(pCurrBucket->m_typeSettingsIndex)->zShiftOffset;
			if (currBucketZShiftOffset>=newBucketZShiftOffset)
			{
				break;
			}
			ppCurrBucket = pCurrBucket->GetPtrPtrNext();
			pCurrBucket = pCurrBucket->GetNext();
		}
		pNewBucket->SetNext(pCurrBucket);
		*ppCurrBucket = pNewBucket;
		m_runningBucketLists[renderPass][method].AddSize(1);
	}
}


///////////////////////////////////////////////////////////////////////////////
//  SyncGetBucket
///////////////////////////////////////////////////////////////////////////////

decalBucket* decalManager::SyncGetBucket(decalInst* pDecalInst, decalRenderPass renderPass, decalMethod method, Vec3V_In vPosition, u16 typeSettingsIndex, u16 renderSettingsIndex, float posScale, u32 msOffsetInt, u16 subType, decalAttachType attachType, fwEntity* pAttachEntity, Mat34V_In wldMtx, s16 attachMatrixId, void* pSmashGroup, bool isGlass, bool isDamaged, bool forceRegen, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted)
{
	// look for a bucket to merge with
	decalBucket* pNewBucket = GetMergableBucket(renderPass, method, vPosition, typeSettingsIndex, renderSettingsIndex, msOffsetInt, subType, attachType, pAttachEntity, attachMatrixId, pSmashGroup, isGlass, isDamaged, forceRegen, interiorLocation, isScripted);
	if (pNewBucket)
	{
		pNewBucket->AddInst(pDecalInst);
		return pNewBucket;
	}

	// no suitable active bucket was found
	pNewBucket = m_pools.GetBucket();
	if (pNewBucket)
	{
		decalAsyncTaskDescBase *const taskDesc = NULL;
		pNewBucket->Init(taskDesc, pDecalInst, posScale, msOffsetInt, pDecalInst->GetBoundCentreAndRadius(), wldMtx, attachMatrixId, attachType, typeSettingsIndex, renderSettingsIndex, subType, isGlass, isDamaged, isPersistent, interiorLocation, isScripted, pSmashGroup);
		pNewBucket->SetAttachEntity(pAttachEntity);
		pNewBucket->SetForceRegen(forceRegen);
		OrderredInsertBucket(renderPass, method, pNewBucket);
	}

	return pNewBucket;
}


#if __PS3

	///////////////////////////////////////////////////////////////////////////
	//  KickVehDamageTask
	///////////////////////////////////////////////////////////////////////////

	void decalManager::KickVehDamageTask()
	{
		decalAssert(!m_vehDamageRunningTask);

		static decalDamageTaskDesc desc;
		for (unsigned i=0; i<NELEM(desc.buckets); ++i)
		{
			desc.buckets[i] = m_runningBucketLists[i][DECAL_METHOD_STATIC].Peek();
		}
		const unsigned frame = m_vehDamageFrame^1;
		desc.mibs               = m_pools.GetMibPoolBase();
		desc.inputVertexArray   = m_pools.GetVtxPoolBase();
		desc.outputVertexArray  = m_vehDamageOffset[frame];
		desc.rsxWaitLabel       = cellGcmGetLabelAddress((u8)(m_vehDamageRsxLabels+frame));
		desc.seqNum             = m_vehDamageSeqNum;
		Vec::V4StoreScalar32FromSplatted(desc.modelSpaceOffsetUnits, GetModelSpaceOffsetUnits().GetIntrin128());

		sysTaskParameters params;
		params.Input.Size = sizeof(desc);
		params.Input.Data = &desc;
		params.Scratch.Size = 0;
		params.SpuStackSize = 0x10000;

		m_vehDamageRunningTask = sysTaskManager::Create(TASK_INTERFACE(decaldamage), params);
		decalAssert(m_vehDamageRunningTask);
	}


	///////////////////////////////////////////////////////////////////////////
	//  RecycleVehDamageTask
	///////////////////////////////////////////////////////////////////////////

	void decalManager::RecycleVehDamageTask()
	{
		// Free up the task handle for any previous damage task.
		const sysTaskHandle damageHandle = m_vehDamageRunningTask;
		if (Likely(damageHandle))
		{
			sysTaskManager::Wait(damageHandle);
			m_vehDamageRunningTask = NULL;
			++m_vehDamageSeqNum;
		}
	}


	///////////////////////////////////////////////////////////////////////////
	//  SyncRsxToVehDamageTask
	///////////////////////////////////////////////////////////////////////////

	void decalManager::SyncRsxToVehDamageTask()
	{
		// Wait labels are not the most efficient way of blocking the RSX, but
		// seriously, this is once a frame, so lets just go with the simplest
		// implementation.
		cellGcmSetWaitLabel(GCM_CONTEXT, m_vehDamageRsxLabels+m_vehDamageFrame, m_vehDamageSeqNum);
	}

#endif // __PS3


///////////////////////////////////////////////////////////////////////////////
//  MergeBuckets
///////////////////////////////////////////////////////////////////////////////

void decalManager::MergeBuckets(decalRenderPass renderPass, decalMethod method, decalBucket *firstNewBucket)
{
	decalBucket *pBucket = firstNewBucket;
	while (pBucket)
	{
		decalBucket *const pNext = pBucket->GetNext();
		decalBucket *const pMergeBucket = GetMergableBucket(renderPass, method, pBucket);
		if (pMergeBucket)
		{
			pMergeBucket->MergeInsts(method, &pBucket->m_instList, pBucket->m_boundSphere);
			ASSERT_ONLY(pBucket->MarkForDelete());
			pBucket->ShutdownImmediate();
			m_pools.ReturnBucket(pBucket);
		}
		else
		{
			OrderredInsertBucket(renderPass, method, pBucket);
		}
		pBucket = pNext;
	}
}


///////////////////////////////////////////////////////////////////////////////
//  decalAddRefs
///////////////////////////////////////////////////////////////////////////////

static void decalAddRefs(decalAsyncTaskDescBase *td)
{
	const decalAsyncTaskType type = td->taskType;
	if (type == DECAL_DO_PROCESSBOUND || type == DECAL_DO_PROCESSBREAKABLEGLASS)
	{
		// Physics bounds can be either loaded by one of several asset stores, or
		// they can be created at runtime by code.  We can determine which is the
		// case by extracting some meta data out of the memory allocation that the
		// phBound belongs to.
		phArchetype *const pPhArchetype = td->pPhInst->GetArchetype();
		phBound *const pPhBound = pPhArchetype->GetBound();
#		if __ASSERT
			td->mainThreadData.debugPhArchetype = pPhArchetype;
			td->mainThreadData.debugPhBound     = pPhBound;
#		endif
		// Is a streamed bound ?
		if (strStreamingEngine::AddRefByPtr(pPhBound, REF_OTHER))
		{
#			if __ASSERT
				// Adding another ref on the bound itself doesn't really do anything
				// particuarly useful, except in assert enabled builds, it will
				// cause an assert to fire if a dictionary is destroyed while there
				// is a ref on a bound in the dictionary.
				pPhBound->AddRef();
#			endif
		}
		// Code generated bound ?
		else
		{
			pPhBound->AddRef();
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  decalRemoveRefs
///////////////////////////////////////////////////////////////////////////////

static void decalRemoveRefs(const decalAsyncTaskDescBase *td)
{
	const decalAsyncTaskType type = td->taskType;
	if (type == DECAL_DO_PROCESSBOUND || type == DECAL_DO_PROCESSBREAKABLEGLASS)
	{
		phArchetype *const pPhArchetype = td->pPhInst->GetArchetype();
		phBound *const pPhBound = pPhArchetype->GetBound();
		decalAssert(td->mainThreadData.debugPhArchetype == pPhArchetype);
		decalAssert(td->mainThreadData.debugPhBound == pPhBound);
		if (strStreamingEngine::RemoveRefByPtr(pPhBound, REF_OTHER))
		{
			ASSERT_ONLY(pPhBound->Release();)
		}
		else
		{
			pPhBound->Release();
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  Synchronize
///////////////////////////////////////////////////////////////////////////////

void decalManager::Synchronize()
{
	// Free up any task descriptors used for synchronous tasks on the previous frame
	{
		decalAsyncTaskDescBase *td = m_runningSyncTaskDesc;
		while (td)
		{
			decalAsyncTaskDescBase *const next = td->mainThreadData.next;
			g_pDecalCallbacks->FreeAsyncTaskDesc(td);
			td = next;
		}
		m_runningSyncTaskDesc = NULL;
	}

	// Block until all asynchronous tasks kicked last frame have completed
	decalAsyncTaskDescBase *td = m_runningAsyncTaskDesc;
 	while (td)
	{
		// The handle may have been already waited on, so need to ensure we
		// don't wait on it a second time
		const sysTaskHandle h = td->mainThreadData.handle;
		if (Likely(h))
		{
			sysTaskManager::Wait(h);
		}

		td = td->mainThreadData.next;
	}

#	if USE_DEFRAGMENTATION
		// Release defrag locks on memory allocations that were used asynchronously
		m_defragLocker.UnlockAll();
#	endif

	// Move all the pending buckets to the running list and register entity
	// references now that we are back on the main thread.
	for (unsigned p=0; p<(unsigned)DECAL_NUM_RENDER_PASSES; ++p)
	{
		for (unsigned m=0; m<(unsigned)DECAL_NUM_METHODS; ++m)
		{
			AsyncSList<decalBucket> *const bucketList = &m_pendingBucketLists[p][m];
			decalAssert(bucketList->SyncCountNumItems() == bucketList->GetNumItems());
			decalBucket *pHead = bucketList->SyncPeek();
			if (pHead)
			{
				decalBucket **ppNext = &pHead;
				decalBucket *pNext = pHead;
				do
				{
					const decalAsyncTaskDescBase *const tdc = pNext->GetTaskDesc();
					decalAssert(tdc);
					fwEntity *const ent = tdc->mainThreadData.regdEnt.Get();
					pNext->SetAttachEntity(ent);

					// Check if the entity has already been deleted.  OnDelete()
					// will have previously blocked on the asynchronous work
					// finishing, but we could not clean up properly until here.
					if (Likely(ent))
					{
						// Since the entity has not been deleted, we advance the
						// tail pointer to include this list item
						ppNext = pNext->GetPtrPtrNext();
						pNext = *ppNext;
					}
					else
					{
						// The entity has already been deleted, so unlink the
						// bucket from the pending list, and free it up.  Don't
						// advance the tail pointer.
						decalBucket *const pNextNext = pNext->GetNext();
						ASSERT_ONLY(pNext->MarkForDelete());
						pNext->ShutdownImmediate();
						m_pools.m_bucketPool.Free(pNext);
						*ppNext = pNextNext;
						pNext = pNextNext;
					}
				}
				while (pNext);

				// Due to the asynchronous nature of the bucket allocation, we
				// cannot merge at that point, so do it here once we have
				// synchronized
		 		MergeBuckets(static_cast<decalRenderPass>(p), static_cast<decalMethod>(m), pHead);
				bucketList->SetEmpty();
			}
		}
	}

	// Free up all the async task descriptors, and release the additional
	// reference they hold
	td = m_runningAsyncTaskDesc;
	while (td)
	{
		// If we haven't already blocked in OnDeleteBlock()
		if (Likely(td->mainThreadData.handle))
		{
			decalRemoveRefs(td);
		}

		// Release reference on entity.  Note that this is a "passive"
		// reference, in that the entity may have already been destroyed.  So
		// this is handled differently to the reference on the physics which may
		// have needed to be done earlier in OnDeleteBlock().
		td->mainThreadData.regdEnt = NULL;

		RemoveDecalIdInstance(td->settings.internal.id);

		decalAsyncTaskDescBase *next = td->mainThreadData.next;
		g_pDecalCallbacks->FreeAsyncTaskDesc(td);
		td = next;
	}
	m_runningAsyncTaskDesc = NULL;

	// Free up the data allocated last frame
	m_taskDataStoreFreeLower = m_taskDataStoreBase;
	m_taskDataStoreFreeUpper = m_taskDataStoreBase + DECAL_DATA_STORE_BYTES;
}


///////////////////////////////////////////////////////////////////////////////
//  UpdateCompleteAsync
///////////////////////////////////////////////////////////////////////////////

void decalManager::UpdateCompleteAsync()
{
	decalSettingsManager::Update();

	// free up any instances queued for delayed deletion
	const unsigned ddf = m_delayedDeletionFrame;
	decalAssert(ddf < (NUM_RENDER_LATENCY_BUFFERS + 1));
	SList<decalInst> *const ddq = m_delayedDeletionQueues+ddf;
	decalInst *const pDDInstHead = ddq->Peek();
	decalInst *pDDInstTail = pDDInstHead;
	for (decalInst *i=pDDInstHead; i; i=i->GetNext())
	{
		ASSERT_ONLY(i->MarkForDelete();)
		i->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_DONT_RELEASE_DECAL_ID);
		pDDInstTail = i;
	}
	m_pools.ReturnInsts(pDDInstHead, pDDInstTail, ddq->GetNumItems());
	ddq->SetEmpty();

	// run any pending buckets created by asynchronous work
	Synchronize();

#	if RSG_PC && __D3D11
		// Update the range of indices for the render thread to upload next time
		// it runs.  Notice that the copy all flag is set when space remaining
		// is <= not < 0.  The reason is that we will end up with begin == end
		// when space remaining is 0.
		CompileTimeAssert((NELEM(m_gpuUploadIndices) & (NELEM(m_gpuUploadIndices)-1)) == 0);
		m_gpuUploadRenderThreadEnd = (m_gpuUploadWorkerThreadsSubFrom - Max(m_gpuUploadWorkerThreadsSpaceRemaining,0) + 1) & (NELEM(m_gpuUploadIndices)-1);
		m_gpuUploadRenderThreadCopyAll |= m_gpuUploadWorkerThreadsSpaceRemaining<=0;

		// Update the range of indices that the worker thread can fill.  This
		// must be done before the worker threads start running again (ie.
		// ProcessAddInfos), otherwise they can write to the wrong ring buffer
		// entries.  It also must be done before we leave the render thread safe
		// zone (to ensure PrimaryRenderThreadBeginFrameCallback picks up the
		// correct values).
		m_gpuUploadWorkerThreadsSpaceRemaining = ((m_gpuUploadRenderThreadBegin-m_gpuUploadRenderThreadEnd-1) & (NELEM(m_gpuUploadIndices)-1)) + 1;
		m_gpuUploadWorkerThreadsSubFrom = (m_gpuUploadRenderThreadBegin - 1) & (NELEM(m_gpuUploadIndices)-1);
#	endif

	decalAssert(Invariant());

	// While still in the render thread safe zone, reduce semaphore count to
	// zero so that decal rendering will be blocked until UpdateStartAsync has
	// completed.  Since we are in the safe zone, this call should never block.
	sysIpcWaitSema(m_renderSema, DECAL_NUMBER_OF_RENDER_THREADS);
}


///////////////////////////////////////////////////////////////////////////////
//  UpdateStartAsync
///////////////////////////////////////////////////////////////////////////////

void decalManager::UpdateStartAsync(float deltaTime)
{
	// update the buckets matrices (needed when processing the add infos below)
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				pCurrBucket->Update_BeforeAdd();
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// reset the persistent info and other stats
	m_persistentLimitReached = false;
	m_statsPersistent.Reset();
	m_statsFading.Reset();
#	if __BANK
		m_debug.m_statsRendered.Reset();
#	endif

	const grcViewport *pActiveVP = g_pDecalCallbacks->GetCurrentActiveGameViewport();
	const Vec4V gameCamPosFOVScale = g_pDecalCallbacks->GetCameraPosAndFOVScale();
	// update the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->Update_AfterAdd(deltaTime, (decalRenderPass)p, (decalMethod)m, pActiveVP, gameCamPosFOVScale))
				{
					pCurrBucket->MarkForDelete();
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// update the persistent info
	int numPersistentBuckets = 0;
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			numPersistentBuckets += m_statsPersistent.numBuckets[p][m];
		}
	}
	if ((numPersistentBuckets > DECAL_MAX_PERSISTENT_BUCKETS)||
		(m_statsPersistent.numInsts > DECAL_MAX_PERSISTENT_INSTS) ||
		(m_statsPersistent.numTris > DECAL_MAX_PERSISTENT_TRIS) ||
		(m_statsPersistent.numVerts > DECAL_MAX_PERSISTENT_VERTS))
	{
		m_persistentLimitReached = true;
	}
	else
	{
		m_persistentLimitReached = false;
	}

	// update the recycler
	decalRecycler::Update();

#	if __BANK
		static bool s_clearForceDynamicLods;
		if (m_modelSpaceOffsetUnits != m_debug.m_modelSpaceOffsetUnits)
		{
			m_modelSpaceOffsetUnits = m_debug.m_modelSpaceOffsetUnits;

			// Force all decals to drop lod to dynamic decal so that they will
			// get regenerated with the new setting.
			m_debug.m_forceDynamicLods = true;
			m_debug.m_forceStaticLods = false;
			s_clearForceDynamicLods = true;
		}
		else if (s_clearForceDynamicLods)
		{
			// Allow the decals to be converted back to static lods using the
			// new model space offset unit value.
			m_debug.m_forceDynamicLods = false;
			s_clearForceDynamicLods = false;
		}
#	endif

	// update the lod system
	if (PARAM_nodecallods.Get()==false)
	{
		decalLodSystem::Update();
	}

	// update debug
#	if __BANK
		// reset all?
		if (m_debug.m_resetAll)
		{
			Remove(NULL, -1, NULL, 0);
			m_debug.m_resetAll = false;
		}

		// update debug info
		unsigned numTris = 0;
		for (unsigned p=0; p<(unsigned)DECAL_NUM_RENDER_PASSES; ++p)
		{
			for (unsigned m=0; m<(unsigned)DECAL_NUM_METHODS; ++m)
			{
				m_debug.m_statsGlobal.numBuckets[p][m] = m_runningBucketLists[p][m].GetNumItems();
				for (decalBucket *pBucket=m_synchronousBucketLists[p][m].SyncPeek(); pBucket; pBucket=pBucket->GetNext())
				{
					for (const decalInst *pInst=pBucket->m_instList.Peek(); pInst; pInst=pInst->GetNext())
					{
						numTris += pInst->GetNumTris();
					}
				}
			}
		}
		m_debug.m_statsGlobal.numInsts = DECAL_MAX_INSTS - m_pools.m_instPool.GetNumItems();
		m_debug.m_statsGlobal.numTris  = numTris;
		m_debug.m_statsGlobal.numVerts = DECAL_MAX_VERTS - m_pools.m_vtxPool.GetNumItems();
#	endif

	// remove any buckets or insts marked for delete
	ProcessMarkedForDelete();

	decalAssert(Invariant());

	// kick off asynchronous work
	ProcessAddInfos();

	// merge any buckets that ended up being done synchronously into the running bucket lists
	for (unsigned p=0; p<(unsigned)DECAL_NUM_RENDER_PASSES; ++p)
	{
		for (unsigned m=0; m<(unsigned)DECAL_NUM_METHODS; ++m)
		{
			decalBucket *const pHead = m_synchronousBucketLists[p][m].SyncPeek();
			for (decalBucket *pBucket=pHead; pBucket; pBucket=pBucket->GetNext())
			{
				// Some buckets created on the main thread will not have a task
				// descriptor (eg. dynamic decals).  These should already have
				// the attached entity setup (may be NULL).
				const decalAsyncTaskDescBase *const td = pBucket->GetTaskDesc();
				if (td)
				{
					pBucket->SetAttachEntity(td->mainThreadData.regdEnt.Get());
				}
			}
	 		MergeBuckets(static_cast<decalRenderPass>(p), static_cast<decalMethod>(m), pHead);
			m_synchronousBucketLists[p][m].SetEmpty();
		}
	}
	decalAsyncTaskDescBase *td = m_runningSyncTaskDesc;
	while (td)
	{
		decalAsyncTaskDescBase *const next = td->mainThreadData.next;
		decalRemoveRefs(td);
		td = next;
	}
	// Because we have potentially locked some memory blocks against defragging,
	// from tasks in the synchronous list, we can't clear out this list yet.  It
	// is still required by OnDelete().

#	if DECAL_MULTI_THREADED_RENDERING
		// Wrap the context buffer index back into range so that we don't get an
		// issue when the value overflows, multiple render threads are enabled,
		// and NUM_PER_CONTEXT_BUFFERS is not a power of two.
		m_mostRecentlyUsedPerContextBuffer %= (unsigned)NUM_PER_CONTEXT_BUFFERS;
#	endif

	// Now that any delayed deletions for this frame have been added to the
	// queue that was emptied at the top of this function, we can now update the
	// frame index.
	const unsigned ddf = m_delayedDeletionFrame;
	m_delayedDeletionFrame = (ddf<(NUM_RENDER_LATENCY_BUFFERS)) ? (ddf+1) : 0;

#	if __PS3
		// Start new vehicle damage spu task.
		RecycleVehDamageTask();
		KickVehDamageTask();
#	endif

	// Once update is completed, allow the render thread(s) to run.
#	if DECAL_MULTI_THREADED_RENDERING && __ASSERT
		m_numRenderStaticCalls = 0;
#	endif
	sysIpcSignalSema(m_renderSema, DECAL_NUMBER_OF_RENDER_THREADS);
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessMarkedForDelete
///////////////////////////////////////////////////////////////////////////////

void decalManager::ProcessMarkedForDelete()
{
	// update the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			int adjust = 0;
			decalBucket** ppCurrBucket = m_runningBucketLists[p][m].GetPtrPtrHead();
			decalBucket* pCurrBucket = *ppCurrBucket;
			while (pCurrBucket)
			{
				decalBucket* pNextBucket = pCurrBucket->GetNext();

				if (pCurrBucket->IsMarkedForDelete())
				{
					--adjust;
					*ppCurrBucket = pNextBucket;
					if (m == DECAL_METHOD_STATIC)
					{
						pCurrBucket->ShutdownDelayed();
					}
					else
					{
						pCurrBucket->ShutdownImmediate();
					}
					m_pools.ReturnBucket(pCurrBucket);
				}
				else
				{
					pCurrBucket->ProcessMarkedForDelete((decalMethod)m);
					ppCurrBucket = pCurrBucket->GetPtrPtrNext();
				}

				pCurrBucket = pNextBucket;
			}
			m_runningBucketLists[p][m].AddSize(adjust);
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  DelayedDeletion
///////////////////////////////////////////////////////////////////////////////

void decalManager::DelayedDeletion(decalInst* pDecalInst)
{
	// Only static instances should have delayed deletion.  Dynamic instances
	// can be immedately deleted.
	decalAssert(pDecalInst->m_idxBufList != decalMiniIdxBuf::NULL_IDX);

	m_delayedDeletionQueues[m_delayedDeletionFrame].Push(pDecalInst);

	RemoveDecalIdInstance(pDecalInst->m_id);
}


///////////////////////////////////////////////////////////////////////////////
//  DelayedDeletion
///////////////////////////////////////////////////////////////////////////////

void decalManager::DelayedDeletion(decalInst* pDecalInstHead, decalInst* pDecalInstTail, unsigned count)
{
#	if __ASSERT
		if (pDecalInstHead)
		{
			decalInst *pDecalInstNext = pDecalInstHead;
			decalInst *pDecalInst;
			do
			{
				decalAssert(pDecalInstNext);
				pDecalInst = pDecalInstNext;
				pDecalInstNext = pDecalInst->GetNext();
				decalAssert(pDecalInst->m_idxBufList != decalMiniIdxBuf::NULL_IDX);
			}
			while (pDecalInst != pDecalInstTail);
		}
#	endif

	decalInst* pDecalInst = pDecalInstHead;
	if (pDecalInst)
	{
		for (;;)
		{
			RemoveDecalIdInstance(pDecalInst->m_id);
			if (pDecalInst == pDecalInstTail)
			{
				break;
			}
			pDecalInst = pDecalInst->GetNext();
		}
	}

	m_delayedDeletionQueues[m_delayedDeletionFrame].Push(pDecalInstHead, pDecalInstTail, count);
}


///////////////////////////////////////////////////////////////////////////////
//  Render
///////////////////////////////////////////////////////////////////////////////

void decalManager::Render(decalRenderPass firstRenderPass, decalRenderPass lastRenderPass, u32 exceptionTypeSettingsFlags, bool bIsForwardPass, bool bResetCtxIndex, bool dynamicOnly)
{
#	if __BANK
		if (m_debug.m_disableRendering)
		{
			return;
		}
#	endif

	sysIpcWaitSema(m_renderSema);

	if(dynamicOnly)
	{
#	if GRCGFXCONTEXT_CONTROL_SUPPORTED
		// Must disable automatic partial submits, since that could lead to the
		// GPU executing a draw call that caues m_staticConstBufIdxData to be
		// cached.  Subsequent draw calls can then use the stale cached data and
		// miss updates from the CPU.  But first do a kick if partial submits
		// are allowed, to avoid an emergency kick later (since that will be
		// significantly more expensive due to cache invalidations).
		grcGfxContext::kickContextIfPartialSubmitsAllowed();
		grcGfxContext::disablePartialSubmit(true);
#	endif

		for (decalRenderPass rp=firstRenderPass; rp<=lastRenderPass; rp=(decalRenderPass)(rp+1))
		{
			Render_Dynamic(rp, exceptionTypeSettingsFlags, bIsForwardPass);
		}

#	if GRCGFXCONTEXT_CONTROL_SUPPORTED
		grcGfxContext::disablePartialSubmit(false);
#	endif
		
		sysIpcSignalSema(m_renderSema);
		return;
	}

#	if DECAL_MULTI_THREADED_RENDERING
		if(renderCtxIndex == -1)
		{
#			if __ASSERT
				// This assert is very important, it doesn't just indicate that D3D
				// may block when mapping a buffer using discard, but also a
				// potential threading issue when m_renderSema is (hopefully)
				// incremented by more than 1.  The threading issue is that if a
				// previous context is still using a buffer, then we can mess up its
				// m_staticIdxBufLastAdded value.  If the assert fires, simply
				// increase MAX_RENDER_STATIC_CALLS_PER_FRAME.
				const u32 acutalNumRenderStaticCalls = sysInterlockedIncrement(&m_numRenderStaticCalls);
				decalAssertf(acutalNumRenderStaticCalls <= MAX_RENDER_STATIC_CALLS_PER_FRAME,
					"Number of Render calls, %u, exceeds expect value of %u.",
					acutalNumRenderStaticCalls, MAX_RENDER_STATIC_CALLS_PER_FRAME);
#			endif

			renderCtxIndex = (s8)sysInterlockedIncrement(&m_mostRecentlyUsedPerContextBuffer) % (unsigned)NUM_PER_CONTEXT_BUFFERS;
			m_staticIdxBufLastAdded[renderCtxIndex] = 0;
		}
#	endif

#	if RSG_DURANGO

#	elif RSG_ORBIS

#	elif RSG_PC && __D3D11

#	elif RSG_XENON
		// Must be doing tiled rendering on 360.  The reason for this is that we
		// are writing to an index buffer multiple times (but not to overlapping
		// parts of it), then calling IDirect3DDevice9::DrawIndexedVertices
		// between each batch of writes.  We are not locking and unlocking the
		// index buffer between the calls.  With tiled rendering, the draw
		// commands are not actually submitted to the GPU until the end of the
		// tiling bracket.  Which means that the GPU won't be prefetching ahead
		// in the index buffer and getting indices that have not yet been
		// writen.
		//
		// Unfortunately, this assert to check we are in a tiling bracket,
		// doesn't work properly.
	  //decalAssert(GRCDEVICE.GetTileCount() != -1);

		if (firstRenderPass == (decalRenderPass)0)
		{
			m_staticIdxBufLastAdded = 0;
			m_staticIdxBufFrame = (m_staticIdxBufFrame + 1) % NUM_RENDER_LATENCY_BUFFERS;
		}

#	elif RSG_PS3
		if (firstRenderPass == (decalRenderPass)0)
		{
			m_vehDamageFrame ^= 1;
			SyncRsxToVehDamageTask();
		}

#	endif

#	if GRCGFXCONTEXT_CONTROL_SUPPORTED
		// Must disable automatic partial submits, since that could lead to the
		// GPU executing a draw call that caues m_staticConstBufIdxData to be
		// cached.  Subsequent draw calls can then use the stale cached data and
		// miss updates from the CPU.  But first do a kick if partial submits
		// are allowed, to avoid an emergency kick later (since that will be
		// significantly more expensive due to cache invalidations).
		grcGfxContext::kickContextIfPartialSubmitsAllowed();
		grcGfxContext::disablePartialSubmit(true);
#	endif

	for (decalRenderPass rp=firstRenderPass; rp<=lastRenderPass; rp=(decalRenderPass)(rp+1))
	{
		Render_Static(rp, exceptionTypeSettingsFlags, bIsForwardPass DECAL_MULTI_THREADED_RENDERING_ONLY(, renderCtxIndex));
		Render_Dynamic(rp, exceptionTypeSettingsFlags, bIsForwardPass);
	}

#	if GRCGFXCONTEXT_CONTROL_SUPPORTED
		grcGfxContext::disablePartialSubmit(false);
#	endif

	if (bResetCtxIndex)
	{
		renderCtxIndex = -1;
	}
	sysIpcSignalSema(m_renderSema);
}


///////////////////////////////////////////////////////////////////////////////
//  ResetContext
///////////////////////////////////////////////////////////////////////////////

void decalManager::ResetContext()
{
	renderCtxIndex = -1;
}


///////////////////////////////////////////////////////////////////////////////
//  PollForEmergencyPartialSubmit
///////////////////////////////////////////////////////////////////////////////

static inline void PollForEmergencyPartialSubmit()
{
#	if RSG_DURANGO || RSG_ORBIS
		// Perform an emergency partial submit if required.
		if (Unlikely(grcGfxContext::emergencyPartialSubmit()))
		{
			// A submit was required, which means that due to cacheline
			// alignment, the GPU may now have fetched data that has not been
			// writen yet, but will be used by future decals.  This can affect
			// the index buffer, and the vertex buffer that contains an index
			// into a constant buffer.
			//
			// Since index buffers are affected, the cache invalidation needs to
			// stall the PFP, not just the ME.
			//
			grcContextHandle *const ctx = g_grcCurrentContext;
#			if RSG_DURANGO
				// This wait until idle shouldn't really be necissary, but there
				// has been some strange GPU hangs on XB1 when invalidating the
				// cache without waiting for idle.  Have been discussing this
				// for quite a while with Microsoft, but we still don't know why
				// (see url:bugstar:1902996 and possibly url:bugstar:1942358).
				// This code is definitely non-performance critical, so just do
				// the stall.
				ctx->InsertWaitUntilIdle(0);
				ctx->FlushGpuCacheRange(
					(UINT)(D3D11_FLUSH_TEXTURE_L1_INVALIDATE | D3D11_FLUSH_TEXTURE_L2_INVALIDATE | D3D11_FLUSH_ENGINE_PFP),
					NULL, ~0uLL);
#			elif RSG_ORBIS
				ctx->flushShaderCachesAndWait(
					sce::Gnm::kCacheActionWriteBackAndInvalidateL1andL2, 0,
					sce::Gnm::kStallCommandBufferParserEnable);
#			endif
		}
#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  Render_Static
///////////////////////////////////////////////////////////////////////////////

void decalManager::Render_Static(decalRenderPass renderPass, u32 exceptionTypeSettingsFlags, bool bIsForwardPass
	DECAL_MULTI_THREADED_RENDERING_ONLY(, unsigned ctxIdx))
{
	// early out to avoid setting render state and shader constants if there's nothing to render
	if (m_runningBucketLists[renderPass][DECAL_METHOD_STATIC].IsEmpty())
	{
		return;
	}

	// set the global emissive scale
	g_pDecalCallbacks->InitGlobalRender();

	grcLightState::SetEnabled(true);

	// set render state
	PrepareStaticRenderPass(renderPass);

	// render the buckets
	const decalBucket* pDecalBucket = m_runningBucketLists[renderPass][DECAL_METHOD_STATIC].Peek();
	while (pDecalBucket)
	{
		if ((!pDecalBucket->GetIsCulled() && g_pDecalCallbacks->ShouldBucketBeRendered(pDecalBucket, renderPass)) BANK_ONLY(|| m_debug.m_disableRenderCulling))
		{
			if (decalManager::IsFlaggedType(pDecalBucket->m_typeSettingsIndex, exceptionTypeSettingsFlags)==false)
			{
				pDecalBucket->Render_Static(bIsForwardPass DECAL_MULTI_THREADED_RENDERING_ONLY(, ctxIdx));
				PollForEmergencyPartialSubmit();
			}
		}
		pDecalBucket = pDecalBucket->GetNext();
	}

	// restore render state
	FinishStaticRenderPass();

	// restore the global emissive scale
	g_pDecalCallbacks->ShutdownGlobalRender();
}


///////////////////////////////////////////////////////////////////////////////
//  Render_Dynamic
///////////////////////////////////////////////////////////////////////////////

void decalManager::Render_Dynamic(decalRenderPass renderPass, u32 exceptionTypeSettingsFlags, bool bIsForwardPass)
{
	// early out to avoid setting render state and shader constants if there's nothing to render
	if (m_runningBucketLists[renderPass][DECAL_METHOD_DYNAMIC].IsEmpty())
	{
		return;
	}

	// begin rendering dynamic decals.
	g_pDecalCallbacks->BeginRenderDynamic(bIsForwardPass);

	// set up the global shader variables
	for (int i=0; i<DECAL_NUM_DYNAMIC_SHADERS; i++)
	{
		m_shaderDynamic[i].SetVarsGlobal();
	}

	// set the global emissive scale
	g_pDecalCallbacks->InitGlobalRender();

	// set the rendering state
	if (bIsForwardPass)
	{
		grcLightState::SetEnabled(true);
	}

	PrepareDynamicRenderPass(renderPass);

	// render the buckets
	const decalBucket* pDecalBucket = m_runningBucketLists[renderPass][DECAL_METHOD_DYNAMIC].Peek();
	while (pDecalBucket)
	{
		if (g_pDecalCallbacks->ShouldBucketBeRendered(pDecalBucket, renderPass) && decalManager::IsFlaggedType(pDecalBucket->m_typeSettingsIndex, exceptionTypeSettingsFlags)==false)
		{
			pDecalBucket->Render_Dynamic(bIsForwardPass);
			PollForEmergencyPartialSubmit();
		}
		pDecalBucket = pDecalBucket->GetNext();
	}

	FinishDynamicRenderPass();

	// restore the global emissive scale
	g_pDecalCallbacks->ShutdownGlobalRender();
	// end rendering of dynamic decals.
	g_pDecalCallbacks->EndRenderDynamic(bIsForwardPass);
}


#if __BANK

	///////////////////////////////////////////////////////////////////////////
	//  RenderDebug
	///////////////////////////////////////////////////////////////////////////

	void decalManager::RenderDebug()
	{
		// render the bucket debug
		for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
		{
			for (int m=0; m<DECAL_NUM_METHODS; m++)
			{
				const decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
				while (pCurrBucket)
				{
					pCurrBucket->RenderDebug();
					pCurrBucket = pCurrBucket->GetNext();
				}
			}
		}

		// render debug info
		m_debug.Render();
	}

#endif // __BANK


#if __PS3

	///////////////////////////////////////////////////////////////////////////
	//  RegisterSpuTask
	///////////////////////////////////////////////////////////////////////////

	void decalManager::RegisterSpuTask(decalAsyncTaskType type, const char *name, const char *binStart, const char *binSize)
	{
		decalAssert(type < NUM_DECAL_ASYNC_TASK_TYPES);
		m_spuTasks[type].m_name     = name;
		m_spuTasks[type].m_binStart = binStart;
		m_spuTasks[type].m_binSize  = binSize;
	}

#endif // __PS3


///////////////////////////////////////////////////////////////////////////////
//  OnDeleteBlock
///////////////////////////////////////////////////////////////////////////////

void decalManager::OnDeleteBlock()
{
	// Wait on completion of all asynchronous tasks.
	decalAsyncTaskDescBase *td = m_runningAsyncTaskDesc;
	while (td)
	{
		// Unlikely, but possible to call OnDelete() for two different
		// entities in the same frame that are both being used for
		// decals.  The first call will wait on then clear out all task
		// descriptor handles, the second call will see NULL handles.
		if (Likely(td->mainThreadData.handle))
		{
			sysTaskManager::Wait(td->mainThreadData.handle);
			decalRemoveRefs(td);

			// Clear the handle so that Synchronize() won't wait on it a second time
			td->mainThreadData.handle = NULL;
		}

		// The actual cleaning up of decal resources is not done until Synchronize()

		td = td->mainThreadData.next;
	}

#	if USE_DEFRAGMENTATION
		// Unlock all memory blocks
		m_defragLocker.UnlockAll();
#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  OnDeleteCmp
///////////////////////////////////////////////////////////////////////////////

template<class CMP> bool decalManager::OnDeleteCmp(CMP &cmp, const decalAsyncTaskDescBase *td)
{
	while (td)
	{
		if (Unlikely(cmp(td)))
		{
			OnDeleteBlock();
			return true;
		}

		td = td->mainThreadData.next;
	}
	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  OnDeleteCmp
///////////////////////////////////////////////////////////////////////////////

template<class CMP> bool decalManager::OnDeleteCmp(CMP &cmp)
{
	// Check if there is currently a task in flight that is referencing this the
	// data being deleted.  If there is, then we need to spin and wait for all
	// tasks to complete.  We can't simply wait for tasks that reference this
	// entity, as we need to unlock all defraggable memory regions in one go.
	//
	// This is ugly and it sucks, but it should be very rare that the code
	// actually needs to block here.
	//
	if (Unlikely(OnDeleteCmp(cmp, m_runningAsyncTaskDesc)))
	{
		return true;
	}

	// Need to also check the synchronous tasks, since if any of them match,
	// then it is unsafe to release the defrag locks after we return from this
	// function.
	return OnDeleteCmp(cmp, m_runningSyncTaskDesc);
}


#define DECAL_ONDELETECMP(TYPE, TDFIELD)                                       \
	struct OnDeleteCmp##TYPE                                                   \
	{                                                                          \
		const TYPE *m_p;                                                       \
                                                                               \
		explicit inline OnDeleteCmp##TYPE(const TYPE *p)                       \
			: m_p(p)                                                           \
		{                                                                      \
		}                                                                      \
                                                                               \
		inline bool operator()(const decalAsyncTaskDescBase *td) const         \
		{                                                                      \
			return td->TDFIELD == m_p;                                         \
		}                                                                      \
	};                                                                         \
	bool decalManager::OnDelete(const TYPE *p)                                 \
	{                                                                          \
		OnDeleteCmp##TYPE cmp(p);                                              \
		return OnDeleteCmp(cmp);                                               \
	}

DECAL_ONDELETECMP(fwEntity, pEntity)
DECAL_ONDELETECMP(phInst,   pPhInst)

#undef DECAL_ONDELETECMP


///////////////////////////////////////////////////////////////////////////////
//  AddSettings
///////////////////////////////////////////////////////////////////////////////

int decalManager::AddSettings(fwEntity* entity, decalUserSettings& settings, int forceDecalId, bool overridePauseCheck)
{
	decalAssert(forceDecalId);
	decalAssertf(fwInteriorLocation::IsValidRoomIndex(settings.bucketSettings.roomId), "Invalid room id set for decal = %d (max is %d)", settings.bucketSettings.roomId, INTLOC_MAX_INTERIOR_ROOMS_COUNT);

#if __DEV
	if (PARAM_nodecals.Get() || PARAM_novfx.Get() || m_debug.m_disableAdding)
	{
		return 0;
	}
#endif

	// return if the game is paused
 	if (fwTimer::IsGamePaused() && !overridePauseCheck)
 	{
 		return 0;
 	}

#if __ASSERT
	decalAssertf((sysThreadType::GetCurrentThreadType()&sysThreadType::THREAD_TYPE_UPDATE), "a decal is being added by a thread other than the main update thread");
#endif

	// add the decal settings to a list that gets processed during the update loop
	if (m_numAddInfos<(unsigned)DECAL_MAX_ADD_INFOS)
	{
		// override the decal settings if required
#if __BANK
		if (m_debug.m_forceMethodDynamic)
		{
			settings.pipelineSettings.method = DECAL_METHOD_DYNAMIC;
		}
		else if (m_debug.m_forceMethodStatic)
		{
			settings.pipelineSettings.method = DECAL_METHOD_STATIC;
		}

		if (m_debug.m_forceRenderPassExtra1)
		{
			settings.pipelineSettings.renderPass = DECAL_RENDER_PASS_EXTRA_1;
		}
		else if (m_debug.m_forceRenderPassExtra2)
		{
			settings.pipelineSettings.renderPass = DECAL_RENDER_PASS_EXTRA_2;
		}
		else if (m_debug.m_forceRenderPassDeferred)
		{
			settings.pipelineSettings.renderPass = DECAL_RENDER_PASS_DEFERRED;
		}
		else if (m_debug.m_forceRenderPassForward)
		{
			settings.pipelineSettings.renderPass = DECAL_RENDER_PASS_FORWARD;
		}

		if (m_debug.m_overrideBoxDepth>=0.0f)
		{
			settings.instSettings.vDimensions.SetZf(m_debug.m_overrideBoxDepth);
		}

		if (m_debug.m_overrideMaxOverlayRadius>=0.0f)
		{
			settings.pipelineSettings.maxOverlayRadius = m_debug.m_overrideMaxOverlayRadius;
		}
#endif
	
		decalAssertf(IsZeroAll(settings.instSettings.vDimensions)==false, "trying to add a decal with zero size");

		m_addInfos[m_numAddInfos].settings.user = settings;
		m_addInfos[m_numAddInfos].entity = entity;
		m_addInfos[m_numAddInfos].shouldProcess = true;
		m_addInfos[m_numAddInfos].id = forceDecalId;

		// apply any size multiplier
		const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(m_addInfos[m_numAddInfos].settings.user.bucketSettings.renderSettingsIndex);
		m_addInfos[m_numAddInfos].settings.user.instSettings.vDimensions *= Vec3V(pRenderSettings->sizeMult, pRenderSettings->sizeMult, 1.0f);

		// if join verts are passed we need to calculate the internal settings straight away so that the new join verts can be returned
		if (m_addInfos[m_numAddInfos].settings.user.pipelineSettings.hasJoinVerts)
		{
			// check that the settings don't want this to be pre processed later on
			// pre processing could alter the settings (i.e. make the decal bigger or reject it) so our internal settings would no longer be valid
			decalAssertf(settings.pipelineSettings.maxOverlayRadius==0.0f, "decals with join verts can't specify an overlay radius");
			decalAssertf(settings.pipelineSettings.duplicateRejectDist==0.0f, "decals with join verts can't specify a duplicate reject distance");
			decalAssertf(settings.instSettings.isLiquidPool==false, "decals with join verts can't be liquid pool");

			// calc the global internal settings here so that any join plane info can be returned instantly
			m_addInfos[m_numAddInfos].settings.CalcGlobalInternalSettings(m_addInfos[m_numAddInfos].id);

			// return any join plane info
			settings.pipelineSettings.vReturnedJoinVerts[0] = m_addInfos[m_numAddInfos].settings.user.pipelineSettings.vReturnedJoinVerts[0];
			settings.pipelineSettings.vReturnedJoinVerts[1] = m_addInfos[m_numAddInfos].settings.user.pipelineSettings.vReturnedJoinVerts[1];
			settings.pipelineSettings.vReturnedJoinVerts[2] = m_addInfos[m_numAddInfos].settings.user.pipelineSettings.vReturnedJoinVerts[2];
			settings.pipelineSettings.vReturnedJoinVerts[3] = m_addInfos[m_numAddInfos].settings.user.pipelineSettings.vReturnedJoinVerts[3];
		}

		// deal with requests from script
		if (m_addInfos[m_numAddInfos].settings.user.bucketSettings.isScripted)
		{
			// this is a scripted decal - we should prioritise it by putting it at the start of the array (if it isn't already)
			if (m_numAddInfos>0)
			{	
				// this isn't the start of the array - make sure we don't have a script request at the start already
				if (decalVerifyf(m_addInfos[0].settings.user.bucketSettings.isScripted==false, "more than one scripted decal request this frame detected"))
				{
					// swap the add info at the start with this one
					decalAddInfo tempAddInfo = m_addInfos[0];
					m_addInfos[0] = m_addInfos[m_numAddInfos];
					m_addInfos[m_numAddInfos] = tempAddInfo;
				}
			}
		}

		m_numAddInfos++;

		AddDecalIdInstance(forceDecalId);
		decalAssert(IsAlive(forceDecalId));
		return forceDecalId;
	}
	else
	{
		decalWarningf("maximum number of decals that can be added per frame has been exceeded - skipping decal");
	}

	return 0;
}


///////////////////////////////////////////////////////////////////////////////
//  AddSettings
///////////////////////////////////////////////////////////////////////////////

int decalManager::AddSettings(fwEntity* entity, decalUserSettings& settings, bool overridePauseCheck)
{
	int decalId = AllocDecalId();
	if (decalId)
	{
		int ret = AddSettings(entity, settings, decalId, overridePauseCheck);
		if (ret)
		{
			decalAssert(ret == decalId);
			return ret;
		}
		else
		{
			FreeDecalId(decalId);
		}
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
//  PreProcessSetting
///////////////////////////////////////////////////////////////////////////////

bool decalManager::PreProcessSetting(fwEntity* entity, decalUserSettings& settings, s32 currAddInfoIndex)
{
	// check for overlays
	if (settings.pipelineSettings.maxOverlayRadius>0.0f)
	{
		if (PreProcessOverlays(entity, settings)==false)
		{
			// the new texture has been rejected 
			return false;
		}
	}
	// check for liquid pools
	else if (settings.instSettings.isLiquidPool)
	{
		if (PreProcessLiquidPools(settings)==false)
		{
			return false;
		}
	}
	// check for being too close
	else if (settings.pipelineSettings.duplicateRejectDist>0.0f)
	{
		float closestDistSqr = FindClosestDistSqr(settings.instSettings.vPosition, settings.bucketSettings.typeSettingsIndex, entity);
		if (closestDistSqr<settings.pipelineSettings.duplicateRejectDist*settings.pipelineSettings.duplicateRejectDist)
		{
			return false;
		}

		// go through the add infos
		for (unsigned i=currAddInfoIndex+1; i<m_numAddInfos; i++)
		{
			// don't look at add infos we've already flagged as not to process
			if (m_addInfos[i].shouldProcess==false)
			{
				continue;
			}

			if (m_addInfos[i].entity==entity)
			{
				if (m_addInfos[i].settings.user.bucketSettings.typeSettingsIndex==settings.bucketSettings.typeSettingsIndex)
				{
					float distSqr = MagSquared(m_addInfos[i].settings.user.instSettings.vPosition-settings.instSettings.vPosition).Getf();
					if (distSqr<settings.pipelineSettings.duplicateRejectDist*settings.pipelineSettings.duplicateRejectDist)
					{
						return false;
					}
				}
			}
		}
	}

	return true;
}


///////////////////////////////////////////////////////////////////////////////
//  PreProcessOverlays
///////////////////////////////////////////////////////////////////////////////

bool decalManager::PreProcessOverlays(const fwEntity* entity, decalUserSettings& settings)
{
	// TODO -- if this gets called a bunch, change these floats to ScalarV's
	float oldRadius = decalHelper::GetRadiusXY(settings.instSettings.vDimensions).Getf();
	float newRadius = decalHelper::GetRadiusXY(settings.instSettings.vDimensions).Getf();

	// go through the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->PreProcessOverlays(entity, settings, newRadius)==false)
				{
					// the new texture has been rejected 
					return false;
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

//	// go through the add infos
// 	for (unsigned i=currAddInfoIndex+1; i<m_numAddInfos; i++)
// 	{
// 		// don't look at add infos we've already flagged as not to process
// 		if (m_addInfos[i].shouldProcess==false)
// 		{
// 			continue;
// 		}
// 
// 		if (m_addInfos[i].entity==entity)
// 		{
// 			if (m_addInfos[i].settings.user.typeSettingsIndex==settings.typeSettingsIndex)
// 			{
// 				Vec3V vCurrPos = m_addInfos[i].settings.user.instSettings.vPosition;
// 				float currRadius = decalHelper::GetRadiusXY(m_addInfos[i].settings.user.instSettings.vDimensions);
// 
// 				int returnCode = decalHelper::PreProcessOverlays(vCurrPos, currRadius, settings.instSettings.vPosition, newRadius, settings.instSettings.maxOverlayRadius);
// 				if (returnCode==-1)
// 				{
// 					// the new texture has been rejected
// 					return false;
// 				}
// 				else if (returnCode==1)
// 				{
// 					// flag this as not needing processed anymore
// 					m_addInfos[i].shouldProcess = false;
// 				}
// 			}
// 		}
// 	}

	// update the details of this projection if it needs to grow
	if (newRadius>oldRadius)
	{
		float oldWidth = settings.instSettings.vDimensions.GetXf();
		float oldHeight = settings.instSettings.vDimensions.GetYf();
		float scale = newRadius/oldRadius;

		settings.instSettings.vDimensions.SetXf(oldWidth * scale);
		settings.instSettings.vDimensions.SetYf(oldHeight * scale);
	}

	decalAssertf(IsZeroAll(settings.instSettings.vDimensions)==false, "pre process has created a decal with zero size");

	return true;
}


///////////////////////////////////////////////////////////////////////////////
//  PreProcessLiquidPools
///////////////////////////////////////////////////////////////////////////////

bool decalManager::PreProcessLiquidPools(decalUserSettings& settings)
{
	// go through the buckets
	bool foundLiquidPool = false;
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->PreProcessLiquidPools(settings))
				{
					foundLiquidPool = true;
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	return !foundLiquidPool;
}


///////////////////////////////////////////////////////////////////////////////
//  FindClosestDistSqr
///////////////////////////////////////////////////////////////////////////////

float decalManager::FindClosestDistSqr(Vec3V_In vPos, u32 typeSettingsIndex, const fwEntity* pEntity)
{
	ScalarV vClosestDistSqr = ScalarVFromF32(FLT_MAX);

	// go through the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			const decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->m_typeSettingsIndex==typeSettingsIndex && (pEntity==NULL || pCurrBucket->m_attachEntity==pEntity))
				{
					ScalarV vDistSqr = pCurrBucket->FindClosestDistSqr(vPos);
					vClosestDistSqr = SelectFT(IsLessThan(vDistSqr, vClosestDistSqr), vClosestDistSqr, vDistSqr);
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	return vClosestDistSqr.Getf();
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessAddInfos
///////////////////////////////////////////////////////////////////////////////

void decalManager::ProcessAddInfos()
{
	phLevelNew* pPhysicsLevel = g_pDecalCallbacks->GetPhysicsLevel();
	const u32 physicsIncludeFlags = g_pDecalCallbacks->GetIncludeFlags();

	m_taskInstCountersUsed = 0;

	// process the stored up decal settings
	for (unsigned i=0; i<m_numAddInfos; ++i)
	{
		if (m_addInfos[i].shouldProcess)
		{
			// settings with join verts have already had their internal settings calculated when their add info was created
			bool process = true;
			if (m_addInfos[i].settings.user.pipelineSettings.hasJoinVerts==false)
			{
				if (PreProcessSetting(m_addInfos[i].entity, m_addInfos[i].settings.user, i))
				{
					// settings without join verts could have their settings altered in the pre processed so we need to calculate their internal settings now
					m_addInfos[i].settings.CalcGlobalInternalSettings(m_addInfos[i].id);
				}
				else
				{
					process = false;
				}
			}

			if (process)
			{
				fwEntity *const entity = m_addInfos[i].entity;
#				if __BANK
					// reset the debug rendering every time we process a decal
					m_debug.ResetLastDecalData();

					// reset the debug counts
					m_debug.m_numInstsCreatedThisProjection = 0;
					m_debug.m_numSkinnedBoneMatricesThisProjection = 0;
					m_debug.m_numPolysConsideredThisProjection = 0;
					m_debug.m_numPolysSentToClipThisProjection = 0;
					m_debug.m_numPolysClippedThisProjection = 0;

					// start a timer
					sysTimer timer;
					timer.Reset();
#				endif

				//Following conditions are used for enabling the underwater check
				//1. if parent entity is dynamic, enable underwater check
				//2. if decal is close to any water body, enable underwater check
				m_addInfos[i].settings.internal.doUnderwaterCheck = (entity && entity->IsBaseFlagSet( fwEntity::IS_DYNAMIC )) ||
					g_pDecalCallbacks->PerformUnderwaterCheck(m_addInfos[i].settings.user.instSettings.vPosition);

				// process the settings using the correct method
				if (m_addInfos[i].settings.user.pipelineSettings.method == DECAL_METHOD_DYNAMIC)
				{
					ProcessSetting_Dynamic(entity, m_addInfos[i].settings);
				}
				else if (m_addInfos[i].settings.user.pipelineSettings.method == DECAL_METHOD_STATIC)
				{
					ProcessSetting_Static(entity, m_addInfos[i].settings, pPhysicsLevel, physicsIncludeFlags);
				}

				// output any required debug info
#				if __BANK
					float timeToProcessThisProjection = timer.GetMsTime();

					if (m_debug.m_outputNumInstsCreatedThisProjection && m_debug.m_numInstsCreatedThisProjection>1)
					{
						decalDebugf1("%d decal insts added this projection", m_debug.m_numInstsCreatedThisProjection);
					}

					if (m_debug.m_outputNumSkinnedBoneMatricesThisProjection)
					{
						decalDebugf1("%d skinned bone matrices this projection", m_debug.m_numSkinnedBoneMatricesThisProjection);
					}

					if (m_debug.m_outputNumPolysConsideredThisProjection)
					{
						decalDebugf1("%d polys considered this projection", m_debug.m_numPolysConsideredThisProjection);
					}

					if (m_debug.m_outputNumPolysSentToClipThisProjection)
					{
						decalDebugf1("%d polys sent to clip this projection", m_debug.m_numPolysSentToClipThisProjection);
					}

					if (m_debug.m_outputNumPolysClippedThisProjection)
					{
						decalDebugf1("%d polys clipped this projection", m_debug.m_numPolysClippedThisProjection);
					}

					if (m_debug.m_outputTimeToProcessThisProjection)
					{
						decalDebugf1("%fms to process this projection", timeToProcessThisProjection);
					}
#				endif
			}
		}

		RemoveDecalIdInstance(m_addInfos[i].id);
	}

	// everything is processed now - reset
	m_numAddInfos = 0;
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessSetting_Dynamic
///////////////////////////////////////////////////////////////////////////////

void decalManager::ProcessSetting_Dynamic(fwEntity* entity, decalSettings& settings)
{
	// get the inst matrix and matrix id
	decalAttachType attachType = DECAL_ATTACH_TYPE_NONE;
	s16 attachMtxId = -1;
	Mat34V instWldMtx = Mat34V(V_IDENTITY);
	const phInst *pPhInst = NULL;
	bool isDamaged = false;
	if (entity)
	{
		attachType = DECAL_ATTACH_TYPE_COMPONENT;
		attachMtxId = settings.user.pipelineSettings.colnComponentId;
		pPhInst = g_pDecalCallbacks->GetPhysInstFromEntity(entity);
		decalHelper::CalcMatrix(instWldMtx, &isDamaged, entity, pPhInst, settings.user.pipelineSettings.colnComponentId, DECAL_ATTACH_TYPE_COMPONENT);
		decalAssertf(IsFiniteAll(instWldMtx), "decal bucket matrix isn't finite 4 (pEntity:%p, attachType:%d)", entity, attachType);
	}

	// get some insts to store the clipped triangle in
	decalInst* pInst = m_pools.GetInst();
	if (pInst)
	{
		// calc the local internal settings
		settings.CalcLocalInternalSettings(instWldMtx, NULL, pInst, attachType);
		settings.internal.isPersistent = decalSettingsManager::GetTypeSettings(settings.user.bucketSettings.typeSettingsIndex)->isPersistent;
		const Vec3V msOffsetInt = CalcModelSpaceOffsetInt(&settings);

		// initialise the insts
		pInst->Init(settings);

		// add the inst to a bucket
		decalAsyncTaskDescBase* taskDesc = NULL;
		const ScalarV posScale(V_ZERO);
		const bool isGlass = false;
		decalBucket *const pBucket = AddInstToBucket(taskDesc, taskDesc, OffsetOf(decalManager, m_synchronousBucketLists), &settings, posScale, msOffsetInt, pInst, pInst, instWldMtx, attachMtxId, isGlass, isDamaged);
		if (Likely(pBucket))
		{
			pBucket->SetAttachEntity(entity);
		}
	}

#if __BANK
	// add the clipper
	m_debug.StoreLastDecalClipper(settings.internal.clipper, settings.user.instSettings.vPosition, decalHelper::GetRadius(settings.user.instSettings.vDimensions).Getf());

	// add the decal box (green)
	Vec3V halfDimensions = settings.user.instSettings.vDimensions * ScalarV(V_HALF);
	Mat34V boxMat = settings.GetDecalMatrix();
	m_debug.StoreLastDecalBox(boxMat, -halfDimensions, halfDimensions, Color32(0.0f, 1.0f, 0.0f, 1.0f), 0.5f);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessSetting_Static
///////////////////////////////////////////////////////////////////////////////

void decalManager::ProcessSetting_Static(fwEntity* entity, decalSettings& settings, phLevelNew* pPhysicsLevel, u32 physicsIncludeFlags)
{
	m_taskInstCounters[m_taskInstCountersUsed] = 0;

	if (settings.user.pipelineSettings.useColnEntityOnly)
	{
		if (Likely(entity))
		{
			phInst* pInst = g_pDecalCallbacks->GetPhysInstFromEntity(entity);
			if (pInst)
			{
				ProcessInstAsync(settings, pInst, pPhysicsLevel);
			}
		}
	}
	else
	{
		const int numResults = 16;
		//using sphere shape test as it gives us the exact entities instead of huge static bounds
		phShapeTest<phShapeSphere> sphereTester;
		phIntersection pIntersections[numResults];
		const ScalarV radius = decalHelper::GetRadius(settings.user.instSettings.vDimensions);
		const u32 collisionFlags = decalSettingsManager::GetTypeSettings(settings.user.bucketSettings.typeSettingsIndex)->collisionFlags;
		sphereTester.InitSphere( settings.user.instSettings.vPosition, radius, pIntersections ,numResults);
		sphereTester.SetIncludeFlags(physicsIncludeFlags);
		sphereTester.GetShape().SetTestBackFacingPolygons(true);
		sphereTester.SetTypeFlags(TYPE_FLAGS_ALL);
		//sphereTester.SetLevel(pPhysicsLevel);
		int numHits = sphereTester.TestInLevel();
		phInst* detectedEntities[numResults];
		int numDetectedEntities = 0;
		for(int i=0; i<numHits; i++)
		{
			phInst* pHitInst = pIntersections[i].GetInstance();
			u32 instTypeFlag = (1<<pHitInst->GetClassType());
			if ((instTypeFlag & collisionFlags))
			{
				//only enter unique entities
				bool bDuplicateEntity = false;
				for(int i=0; i<numDetectedEntities; i++)
				{
					if(pHitInst == detectedEntities[i])
					{
						bDuplicateEntity = true;
						break;								
					}
				}
				//reject if duplicate entity is found
				if(!bDuplicateEntity)
				{
					detectedEntities[numDetectedEntities++] = pHitInst;
					ProcessInstAsync(settings, pHitInst, pPhysicsLevel);
				}
			}

		}
	}

	++m_taskInstCountersUsed;

#if __BANK
	// add the clipper
	m_debug.StoreLastDecalClipper(settings.internal.clipper, settings.user.instSettings.vPosition, decalHelper::GetRadius(settings.user.instSettings.vDimensions).Getf());

	// add the decal box (green)
	Vec3V halfDimensions = settings.user.instSettings.vDimensions * ScalarV(V_HALF);
	Mat34V boxMat = settings.GetDecalMatrix();
	m_debug.StoreLastDecalBox(boxMat, -halfDimensions, halfDimensions, Color32(0.0f, 1.0f, 0.0f, 1.0f), 0.5f);

	//grcDebugDraw::BoxOriented(-halfDimensions, halfDimensions, boxMat, Color32(0.0f, 1.0f, 0.0f, 1.0f), false, 1000);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//  CheckAlreadyPackedSkeleton
///////////////////////////////////////////////////////////////////////////////

bool decalManager::CheckAlreadyPackedSkeleton(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId) const
{
	// Check if we've already created another task with this same entity, as
	// that means we don't need to copy all the matrices again (meaning we can
	// keep DECAL_DATA_STORE_BYTES at a vaugely reasonable size).  Also need to
	// check that the task type is something that does actually store the
	// skeletons.
	const decalAsyncTaskDescBase *td;
	for (td=m_runningAsyncTaskDesc; td; td=td->mainThreadData.next)
	{
		if (td->pEntity == pEntity
			&& td->settings.user.lodSettings.lodAttachMatrixId == lodAttachMatrixId
			&& (td->taskType == DECAL_DO_PROCESSDRAWABLESNONSKINNED
				|| td->taskType == DECAL_DO_PROCESSDRAWABLESSKINNED))
		{
			goto found;
		}
	}
	for (td=m_runningSyncTaskDesc; td; td=td->mainThreadData.next)
	{
		if (td->pEntity == pEntity
			&& td->settings.user.lodSettings.lodAttachMatrixId == lodAttachMatrixId
			&& (td->taskType == DECAL_DO_PROCESSDRAWABLESNONSKINNED
				|| td->taskType == DECAL_DO_PROCESSDRAWABLESSKINNED))
		{
			goto found;
		}
	}
	return false;

found:
	*outNumSkelMtxs         = td->numSkelMtxs;
	*outSkelMtxs            = td->skelObjMtxs;
	*outSkelBones           = td->skelBones;
	*outNumDrawables        = td->numDrawables;
	*outDamagedDrawableMask = td->damagedDrawableMask;
	*outSkelDrawableIdxs    = const_cast<u8*>(td->skelDrawableIdxs); // the const_cast here for skelDrawableIdxs is nasty, but it will not actually get modified.
	sysMemCpy(outDrawablePtrs, td->drawablePtrs, sizeof(td->drawablePtrs));

	return true;
}


#if !__NO_OUTPUT

	///////////////////////////////////////////////////////////////////////////
	//  GetEntityName
	///////////////////////////////////////////////////////////////////////////

	static inline const char *GetEntityName(const fwEntity *pEntity)
	{
#		if __ASSERT
			// GetDebugName can return an empty string which is kinda useless.
			// Only use it if it is a non-empty string.
			const char *const debugName = pEntity->GetDebugName();
			if (debugName && *debugName)
			{
				return debugName;
			}
#		endif
		return pEntity->GetModelName();
	}

#endif // !__NO_OUTPUT


///////////////////////////////////////////////////////////////////////////////
//  PackSkeletons
///////////////////////////////////////////////////////////////////////////////

decalManager::PackSkelRet decalManager::PackSkeletons(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const crSkeleton* pUndamagedSkel, const crSkeleton* pDamagedSkel)
{
	if (CheckAlreadyPackedSkeleton(outNumSkelMtxs, outSkelMtxs, outSkelBones, outNumDrawables, outDamagedDrawableMask, outSkelDrawableIdxs, outDrawablePtrs, pEntity, lodAttachMatrixId))
	{
		return PACKSKEL_REUSE;
	}

	// Validate bone counts
	const unsigned numBoneHierarchyIdxs = pUndamagedSkel->GetBoneCount();
	artAssertf(numBoneHierarchyIdxs <= DECAL_MAX_BONES,
		"\"%s\" has more bones (%u) in skeleton that supported (%u)",
		GetEntityName(pEntity), numBoneHierarchyIdxs, DECAL_MAX_BONES);
	artAssertf(numBoneHierarchyIdxs == (unsigned)pUndamagedSkel->GetSkeletonData().GetNumBones(),
		"\"%s\" has a different bone count in the undamaged skeleton (%u), and undamaged skeleton data (%u)",
		GetEntityName(pEntity), numBoneHierarchyIdxs, pUndamagedSkel->GetSkeletonData().GetNumBones());
	if (pDamagedSkel)
	{
		artAssertf(&pUndamagedSkel->GetSkeletonData() == &pDamagedSkel->GetSkeletonData(),
			"\"%s\" has different skeleton data for damaged and undamaged skeletons",
			GetEntityName(pEntity));
		artAssertf(numBoneHierarchyIdxs == pDamagedSkel->GetBoneCount(),
			"\"%s\" has an undamaged and a damaged skeleton with different bone counts (undamaged %u, damaged %u)",
			GetEntityName(pEntity), numBoneHierarchyIdxs, pDamagedSkel->GetBoneCount());
	}

	// Build a temporary array of flags to specify if each bone in the hierarchy
	// will be used in the lod skeleton.  If no lod skeleton, then the hierarchy
	// bone index is the same as the render bone index, so it is used.
	bool hierarchyBoneUsedInRender[DECAL_MAX_BONES];
 	const fwArchetype *const archetype = pEntity->GetArchetype();
 	decalAssert(archetype);
 	const u16 *const renderToHierarchyLut = archetype->GetLodSkeletonMap();
 	if (!renderToHierarchyLut)
	{
		sysMemSet(hierarchyBoneUsedInRender, true, sizeof(hierarchyBoneUsedInRender));
	}
 	else
 	{
 		sysMemSet(hierarchyBoneUsedInRender, false, sizeof(hierarchyBoneUsedInRender));
 		const unsigned numRenderBones = archetype->GetLodSkeletonBoneNum();
 		for (unsigned renderIdx=0; renderIdx<numRenderBones; ++renderIdx)
 		{
 			hierarchyBoneUsedInRender[renderToHierarchyLut[renderIdx]] = true;
 		}
 	}

	// Create temporary copies of skelBones and skelDrawableIdxs on the stack
	// first, till we know how many matrices need to be stored.
	Vec4V *const skelObjMtxs = (Vec4V*)(((uptr)m_taskDataStoreFreeLower+15)&~15);
	Vec4V *currSkelMtxs = skelObjMtxs;
	unsigned tmpNumDrawables = 1;
	DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Task,Hierarchy, tmpSkelBones, DECAL_MAX_BONES);
	u8 tmpSkelDrawableIdxs[DECAL_MAX_BONES];
	decalBoneTaskIdx boneTaskIdx(0);
	for (decalBoneHierarchyIdx boneHierarchyIdx(0); boneHierarchyIdx<numBoneHierarchyIdxs; ++boneHierarchyIdx)
	{
		if (!hierarchyBoneUsedInRender[boneHierarchyIdx.Val()] BANK_ONLY(&& !m_debug.m_disableLodSkeletonMapping))
		{
			continue;
		}

		if (!ShouldProcessDrawableBone(lodAttachMatrixId, pEntity, boneHierarchyIdx))
		{
			continue;
		}

		unsigned drawableIdx = 0;
		Mat34V boneObjMtx = pUndamagedSkel->GetObjectMtx(boneHierarchyIdx.Val());
		if (IsZeroAll(boneObjMtx.GetCol0()))
		{
			if (pDamagedSkel)
			{
				drawableIdx = 1;
				boneObjMtx = pDamagedSkel->GetObjectMtx(boneHierarchyIdx.Val());
				if (IsZeroAll(boneObjMtx.GetCol0()))
				{
					continue;
				}
			}
			else
			{
				continue;
			}
		}

		decalSettings::ValidateMatrix(boneObjMtx);

		if ((u8*)(currSkelMtxs+3) > m_taskDataStoreFreeUpper)
		{
			decalSpamf("Out of space trying to store skeleton(s) for \"%s\"", GetEntityName(pEntity));
			return PACKSKEL_FAILED;
		}

		Matrix44 boneObjMtxT;
		boneObjMtxT.a = VEC4V_TO_VECTOR4(boneObjMtx.GetCol0());
		boneObjMtxT.b = VEC4V_TO_VECTOR4(boneObjMtx.GetCol1());
		boneObjMtxT.c = VEC4V_TO_VECTOR4(boneObjMtx.GetCol2());
		boneObjMtxT.d = VEC4V_TO_VECTOR4(boneObjMtx.GetCol3());
		boneObjMtxT.Transpose();
		*currSkelMtxs++ = VECTOR4_TO_VEC4V(boneObjMtxT.a);
		*currSkelMtxs++ = VECTOR4_TO_VEC4V(boneObjMtxT.b);
		*currSkelMtxs++ = VECTOR4_TO_VEC4V(boneObjMtxT.c);
		tmpSkelBones[boneTaskIdx] = boneHierarchyIdx;
		tmpSkelDrawableIdxs[boneTaskIdx.Val()] = (u8)drawableIdx;
		++boneTaskIdx;

		tmpNumDrawables = Max(tmpNumDrawables, drawableIdx+1u);
	}

	// If there were no relevant bones to process, then just return false.  This
	// is not an error though, we simply want to bail out of processing the
	// decals.
	const unsigned numTaskBones = boneTaskIdx.Val();
	if (!numTaskBones)
	{
		return PACKSKEL_EMPTYSKEL;
	}

	// Copy the bone and drawable lookup tables to the data store.
	u8 *const skelBones = m_taskDataStoreFreeUpper - numTaskBones;
	u8 *const skelDrawableIdxs = skelBones - numTaskBones;
	if (skelDrawableIdxs < (u8*)currSkelMtxs)
	{
		decalSpamf("Out of space trying to store skeleton(s) for \"%s\"", GetEntityName(pEntity));
		return PACKSKEL_FAILED;
	}
	sysMemCpy(skelBones,        tmpSkelBones.GetPtr(), numTaskBones);
	sysMemCpy(skelDrawableIdxs, tmpSkelDrawableIdxs,   numTaskBones);

	// Save the allocations
	m_taskDataStoreFreeLower = (u8*)currSkelMtxs;
	m_taskDataStoreFreeUpper = skelDrawableIdxs;

	*outNumSkelMtxs         = numTaskBones;
	*outSkelMtxs            = skelObjMtxs;
	outSkelBones->SetPtr((const decalBoneHierarchyIdx*)skelBones);
	*outNumDrawables        = tmpNumDrawables;
	*outSkelDrawableIdxs    = skelDrawableIdxs;
	*outDamagedDrawableMask = tmpNumDrawables>1 ? 2 : 0;
	// Note that outDrawablePtrs is intentially uninitialized here, caller must set this up
	return PACKSKEL_NEW;
}


///////////////////////////////////////////////////////////////////////////////
//  PackSkeletonData
///////////////////////////////////////////////////////////////////////////////

decalManager::PackSkelRet decalManager::PackSkeletonData(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const crSkeletonData* pSkelData)
{
	unsigned discardNumDrawables;
	unsigned discardDamagedDrawableMask;
	u8 *discardSkelDrawableIdxs;
	const rmcDrawable *discardDrawablePtrs[DECAL_MAX_DRAWABLES];
	if (CheckAlreadyPackedSkeleton(outNumSkelMtxs, outSkelMtxs, outSkelBones, &discardNumDrawables, &discardDamagedDrawableMask, &discardSkelDrawableIdxs, discardDrawablePtrs, pEntity, lodAttachMatrixId))
	{
		// PackSkeletonData is never called for frags, so there can be only one drawable
		decalAssert(discardNumDrawables == 1);
		decalAssert(!discardSkelDrawableIdxs);
		return PACKSKEL_REUSE;
	}

	const unsigned numBoneHierarchyIdxs = pSkelData->GetNumBones();
	artAssertf(numBoneHierarchyIdxs <= DECAL_MAX_BONES,
		"\"%s\" has more bones (%u) in skeleton that supported (%u)",
		GetEntityName(pEntity), numBoneHierarchyIdxs, DECAL_MAX_BONES);

	// Basically a simplified version of the loop in PackSkeletons.  Here we
	// only have one skeleton, so there is no need to maintain a drawable index
	// array.
	Vec4V *const skelObjMtxs = (Vec4V*)(((uptr)m_taskDataStoreFreeLower+15)&~15);
	Vec4V *currSkelMtxs = skelObjMtxs;
	DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Task,Hierarchy, tmpSkelBones, DECAL_MAX_BONES);
	decalBoneTaskIdx boneTaskIdx(0);
	for (decalBoneHierarchyIdx boneHierarchyIdx(0); boneHierarchyIdx<numBoneHierarchyIdxs; ++boneHierarchyIdx)
	{
		if (!ShouldProcessDrawableBone(lodAttachMatrixId, pEntity, boneHierarchyIdx))
		{
			continue;
		}

		Mat34V boneObjMtx;
		pSkelData->ComputeObjectTransform(boneHierarchyIdx.Val(), boneObjMtx);

		if (IsZeroAll(boneObjMtx.GetCol0()))
		{
			continue;
		}

		decalSettings::ValidateMatrix(boneObjMtx);

		if ((u8*)(currSkelMtxs+3) > m_taskDataStoreFreeUpper)
		{
			decalSpamf("Out of space trying to store skeleton(s) for \"%s\"", GetEntityName(pEntity));
			return PACKSKEL_FAILED;
		}

		Matrix44 boneObjMtxT;
		boneObjMtxT.a = VEC4V_TO_VECTOR4(boneObjMtx.GetCol0());
		boneObjMtxT.b = VEC4V_TO_VECTOR4(boneObjMtx.GetCol1());
		boneObjMtxT.c = VEC4V_TO_VECTOR4(boneObjMtx.GetCol2());
		boneObjMtxT.d = VEC4V_TO_VECTOR4(boneObjMtx.GetCol3());
		boneObjMtxT.Transpose();
		*currSkelMtxs++ = VECTOR4_TO_VEC4V(boneObjMtxT.a);
		*currSkelMtxs++ = VECTOR4_TO_VEC4V(boneObjMtxT.b);
		*currSkelMtxs++ = VECTOR4_TO_VEC4V(boneObjMtxT.c);
		tmpSkelBones[boneTaskIdx] = boneHierarchyIdx;
		++boneTaskIdx;
	}

	// If there were no relevant bones to process, then just return false.  This
	// is not an error though, we simply want to bail out of processing the
	// decals.
	const unsigned numTaskBones = boneTaskIdx.Val();
	if (!numTaskBones)
	{
		return PACKSKEL_EMPTYSKEL;
	}

	// Copy the bone lookup table in the data store
	u8 *const skelBones = m_taskDataStoreFreeUpper - numTaskBones;
	if (skelBones < (u8*)currSkelMtxs)
	{
		decalSpamf("Out of space trying to store skeleton for \"%s\"", GetEntityName(pEntity));
		return PACKSKEL_FAILED;
	}
	sysMemCpy(skelBones, tmpSkelBones.GetPtr(), numTaskBones);

	// Save the allocations
	m_taskDataStoreFreeLower = (u8*)currSkelMtxs;
	m_taskDataStoreFreeUpper = skelBones;

	*outNumSkelMtxs   = numTaskBones;
	*outSkelMtxs      = skelObjMtxs;
	outSkelBones->SetPtr((const decalBoneHierarchyIdx*)skelBones);
	return PACKSKEL_NEW;
}


///////////////////////////////////////////////////////////////////////////////
//  PackMatrixSet
///////////////////////////////////////////////////////////////////////////////

decalManager::PackSkelRet decalManager::PackMatrixSet(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const crSkeletonData* pSkelData, const grmMatrixSet* pMatrixSet)
{
	if (CheckAlreadyPackedSkeleton(outNumSkelMtxs, outSkelMtxs, outSkelBones, outNumDrawables, outDamagedDrawableMask, outSkelDrawableIdxs, outDrawablePtrs, pEntity, lodAttachMatrixId))
	{
		// There can possibly be multiple drawables returned here if we have an
		// modded frag.
		return PACKSKEL_REUSE;
	}

	const Matrix43 *objMtxs43 = pMatrixSet->GetMatrices();
	decalAssert(objMtxs43);
	const unsigned numBoneHierarchyIdxs = pSkelData->GetNumBones();
	artAssertf(numBoneHierarchyIdxs <= DECAL_MAX_BONES,
		"\"%s\" has more bones (%u) in skeleton that supported (%u)",
		GetEntityName(pEntity), numBoneHierarchyIdxs, DECAL_MAX_BONES);

	// Basically a simplified version of the loop in PackSkeletons.  Here we
	// only have one skeleton, so there is no need to maintain a drawable index
	// array.
	Vec4V *const skelObjMtxs = (Vec4V*)(((uptr)m_taskDataStoreFreeLower+15)&~15);
	Vec4V *currSkelMtxs = skelObjMtxs;
	DECAL_BONEREMAPLUT_MUTABLE_ARRAY(,Task,Hierarchy, tmpSkelBones, DECAL_MAX_BONES);
	decalBoneTaskIdx boneTaskIdx(0);
	for (decalBoneHierarchyIdx boneHierarchyIdx(0); boneHierarchyIdx<numBoneHierarchyIdxs; ++boneHierarchyIdx)
	{
		const Matrix43 *const src = objMtxs43++;

		if (!ShouldProcessDrawableBone(lodAttachMatrixId, pEntity, boneHierarchyIdx))
		{
			continue;
		}

		if (IsZeroAll(Vec4V(src->x)))
		{
			continue;
		}

#		if __ASSERT
			Matrix44 boneObjMtxT;
			boneObjMtxT.a = Vector4(src->x);
			boneObjMtxT.b = Vector4(src->y);
			boneObjMtxT.c = Vector4(src->z);
			boneObjMtxT.d = VEC4V_TO_VECTOR4(Vec4V(V_ZERO_WONE));
			boneObjMtxT.Transpose();
			Mat34V boneObjMtx;
			boneObjMtx.SetCol0(RCC_VEC3V(boneObjMtxT.a));
			boneObjMtx.SetCol1(RCC_VEC3V(boneObjMtxT.b));
			boneObjMtx.SetCol2(RCC_VEC3V(boneObjMtxT.c));
			boneObjMtx.SetCol3(RCC_VEC3V(boneObjMtxT.d));
			decalSettings::ValidateMatrix(boneObjMtx);
#		endif

		if ((u8*)(currSkelMtxs+3) > m_taskDataStoreFreeUpper)
		{
			decalSpamf("Out of space trying to store skeleton for \"%s\"", GetEntityName(pEntity));
			return PACKSKEL_FAILED;
		}

		*currSkelMtxs++ = Vec4V(src->x);
		*currSkelMtxs++ = Vec4V(src->y);
		*currSkelMtxs++ = Vec4V(src->z);
		tmpSkelBones[boneTaskIdx] = boneHierarchyIdx;
		++boneTaskIdx;
	}

	// If there were no relevant bones to process, then just return false.  This
	// is not an error though, we simply want to bail out of processing the
	// decals.
	const unsigned numTaskBones = boneTaskIdx.Val();
	if (!numTaskBones)
	{
		return PACKSKEL_EMPTYSKEL;
	}

	// Copy the bone and create the drawable lookup tables in the data store
	u8 *const skelBones = m_taskDataStoreFreeUpper - numTaskBones;
	u8 *const skelDrawableIdxs = skelBones - numTaskBones;
	if (skelDrawableIdxs < (u8*)currSkelMtxs)
	{
		decalSpamf("Out of space trying to store skeleton for \"%s\"", GetEntityName(pEntity));
		return PACKSKEL_FAILED;
	}
	sysMemCpy(skelBones,        tmpSkelBones.GetPtr(), numTaskBones);
	sysMemSet(skelDrawableIdxs, 0,                     numTaskBones);

	// Save the allocations
	m_taskDataStoreFreeLower = (u8*)currSkelMtxs;
	m_taskDataStoreFreeUpper = skelDrawableIdxs;

	*outNumSkelMtxs         = numTaskBones;
	*outSkelMtxs            = skelObjMtxs;
	outSkelBones->SetPtr((const decalBoneHierarchyIdx*)skelBones);
	*outNumDrawables        = 1;
	*outDamagedDrawableMask = 0;
	*outSkelDrawableIdxs    = skelDrawableIdxs;
	// Note that outDrawablePtrs is intentially uninitialized here, caller must set this up
	return PACKSKEL_NEW;
}


///////////////////////////////////////////////////////////////////////////////
//  PackFragSkeletons
///////////////////////////////////////////////////////////////////////////////

decalManager::PackSkelRet decalManager::PackFragSkeletons(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const Mat34V **outSkelInvJoints, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const fragInst* pFragInst, const fragType* pFragType)
{
	const rmcDrawable *const pUndamagedDrawable = pFragType->GetCommonDrawable();
	decalAssertf(pUndamagedDrawable, "frag \"%s\" has no common drawable", GetEntityName(pEntity));
	outDrawablePtrs[0] = pUndamagedDrawable;

	// Frag inst has a cache entry?
	const fragCacheEntry *const pFragCacheEntry = pFragInst->GetCacheEntry();
	if (pFragCacheEntry)
	{
		// All frag cache entries should have an undamaged skeleton.
		const fragHierarchyInst *const pFragHierarchyInst = pFragCacheEntry->GetHierInst();
		decalAssert(pFragHierarchyInst);
		const crSkeleton *const pUndamagedSkel = pFragHierarchyInst->skeleton;
		decalAssert(pUndamagedSkel);

		// A damaged skeleton is optional
		const crSkeleton *const pDamagedSkel = pFragHierarchyInst->damagedSkeleton;
		const rmcDrawable *const pDamagedDrawable = pFragType->GetDamagedDrawable();
		decalAssertf(!pDamagedSkel ^ !!pDamagedDrawable,
			"frag \"%s\" has a damaged skeleton, but no damaged drawable, or vice versa",
			GetEntityName(pEntity));

		// If we have a damaged skeleton/drawable, then assume it will be used,
		// and update pDrawables/numDrawables accordingly.  If it turns out to
		// not be used, then RemoveUnusedDrawables will clean this up for us
		// later.  If there was a previous task that we copy from, then this
		// will be overwritten.
		outDrawablePtrs[1] = pDamagedDrawable;

		const PackSkelRet ret = PackSkeletons(outNumSkelMtxs, outSkelMtxs, outSkelBones, outNumDrawables, outDamagedDrawableMask, outSkelDrawableIdxs, outDrawablePtrs, pEntity, lodAttachMatrixId, pUndamagedSkel, pDamagedSkel);

		*outSkelInvJoints = pUndamagedSkel->GetSkeletonData().GetCumulativeInverseJoints();

		return ret;
	}

	// Even without a frag cache entry, there can still be a shared skeleton
	// between all frag insts of the same type.
	const grmMatrixSet *const pMatrixSet = pFragType->GetSharedMatrixSet();
	const crSkeletonData *const pSkelData = &pFragType->GetSkeletonData();
	decalAssertf(!pMatrixSet ^ !!pSkelData,
		"frag \"%s\" has a matrix set but no skeleton data, or vice versa",
		GetEntityName(pEntity));
	if (pMatrixSet)
	{
		*outSkelInvJoints = pSkelData->GetCumulativeInverseJoints();
		return PackMatrixSet(outNumSkelMtxs, outSkelMtxs, outSkelBones, outNumDrawables, outDamagedDrawableMask, outSkelDrawableIdxs, outDrawablePtrs, pEntity, lodAttachMatrixId, pSkelData, pMatrixSet);
	}

	*outNumSkelMtxs         = 0;
	*outSkelMtxs            = NULL;
	outSkelBones->SetPtr(NULL);
	*outNumDrawables        = 1;
	*outDamagedDrawableMask = 0;
	*outSkelDrawableIdxs    = NULL;
	// outDrawablePtrs[0] already setup
	*outSkelInvJoints       = NULL;
	return PACKSKEL_NOSKEL;
}


///////////////////////////////////////////////////////////////////////////////
//  RemoveUnusedDrawables
///////////////////////////////////////////////////////////////////////////////

void decalManager::RemoveUnusedDrawables(unsigned *inOutNumDrawables, unsigned* inOutDamagedDrawableMask, const rmcDrawable** pDrawables, unsigned numSkelMtxs, u8** inOutSkelDrawableIdxs)
{
	// Check that skelDrawableIdxs was the last allocation so that we can free it
	// up if not required
	u8 *skelDrawableIdxs = *inOutSkelDrawableIdxs;
	decalAssert(skelDrawableIdxs == m_taskDataStoreFreeUpper);

	// Remove unused drawables from the array (main case where this occurs is
	// with a damaged frag, where the undamaged drawable is no longer used).

	// First build an array of flags to indicate which drawables are used.
	bool drawableUsedFlags[DECAL_MAX_DRAWABLES];
	sysMemSet(drawableUsedFlags, 0, sizeof(drawableUsedFlags));
	for (unsigned i=0; i<numSkelMtxs; ++i)
	{
		drawableUsedFlags[skelDrawableIdxs[i]] = true;
	}

	// Build a table to map the previous drawable indices to the new indices
	// where unused drawables have been removed.  Also compact the array of
	// drawable pointers, and the mask of which drawables come from the damaged
	// skeleton.
	unsigned numActuallyUsedDrawables = 0;
	u8 drawableRenumberring[DECAL_MAX_DRAWABLES];
	unsigned origDamagedDrawableMask = *inOutDamagedDrawableMask;
	unsigned newDamagedDrawableMask = 0;
	unsigned newDamagedDrawableMaskBit = 1;
	const unsigned numDrawables = *inOutNumDrawables;
	for (unsigned i=0,j=0; i<numDrawables; ++i)
	{
		if (drawableUsedFlags[i])
		{
			drawableRenumberring[i] = (u8)numActuallyUsedDrawables;
			++numActuallyUsedDrawables;

			pDrawables[j++] = pDrawables[i];
			newDamagedDrawableMask |= newDamagedDrawableMaskBit & -(int)(origDamagedDrawableMask&1);
			newDamagedDrawableMaskBit <<= 1;
		}
		origDamagedDrawableMask >>= 1;
	}

	// If we are left with just one drawable, then we can simply remove
	// the array of drawable indices.
	if (numActuallyUsedDrawables == 1)
	{
		m_taskDataStoreFreeUpper = skelDrawableIdxs + numSkelMtxs;
		*inOutSkelDrawableIdxs = NULL;
	}

	// Multiple drawables still in use, .: need to renumber the drawable
	// index array.
	else
	{
		decalAssert(numActuallyUsedDrawables);
		for (unsigned i=0; i<numSkelMtxs; ++i)
		{
			skelDrawableIdxs[i] = drawableRenumberring[skelDrawableIdxs[i]];
		}
	}

	*inOutNumDrawables = numActuallyUsedDrawables;
	*inOutDamagedDrawableMask = newDamagedDrawableMask;
}


///////////////////////////////////////////////////////////////////////////////
//  KickProcessDrawables
///////////////////////////////////////////////////////////////////////////////

void decalManager::KickProcessDrawables(decalSettings& settings, fwEntity* pEntity, phInst* pPhInst, unsigned numDrawables, unsigned damagedDrawableMask, const rmcDrawable*const* pDrawables, Mat34V_In entityMtx, unsigned numSkelMtxs, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8* skelDrawableIdxs, const Mat34V* skelInvJoints, phLevelNew* pPhysicsLevel)
{
	decalAssert(pDrawables);
	decalAssert(numDrawables);

	bool kickedNonSkinned = false;
	bool kickedSkinned = false;

	for (unsigned drawableIdx=0; drawableIdx<numDrawables; ++drawableIdx)
	{
		const rmcDrawable *const pDrawable = pDrawables[drawableIdx];
		decalAssert(pDrawable);

		const rmcLod* const pLod = g_pDecalCallbacks->GetLod(pEntity, pDrawable);
		decalAssert(pLod);
		const unsigned numModels = pLod->GetCount();
		for (unsigned i=0; i<numModels; ++i)
		{
			const grmModel *const pModel = pLod->GetModel(i);
			if (pModel->GetSkinFlag())
			{
				if (!kickedSkinned)
				{
					KickProcessInstAsync(DECAL_DO_PROCESSDRAWABLESSKINNED, settings, pEntity, pPhInst, numDrawables, damagedDrawableMask, pDrawables, entityMtx, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, pPhysicsLevel);

					// If we have also already kicked the non-skinned task, then we're done
					if (kickedNonSkinned)
					{
						return;
					}

					kickedSkinned = true;
				}
			}
			else
			{
				if (!kickedNonSkinned)
				{
					KickProcessInstAsync(DECAL_DO_PROCESSDRAWABLESNONSKINNED, settings, pEntity, pPhInst, numDrawables, damagedDrawableMask, pDrawables, entityMtx, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, pPhysicsLevel);

					if (kickedSkinned)
					{
						return;
					}

					kickedNonSkinned = true;
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  ProcessInstAsync
///////////////////////////////////////////////////////////////////////////////

void decalManager::ProcessInstAsync(decalSettings& settings, phInst* pPhInst, phLevelNew* pPhysicsLevel)
{
	// check if we should be processing this inst
	if (!g_pDecalCallbacks->ShouldProcessInst(pPhInst, settings.user.pipelineSettings.applyToBreakables))
	{
		return;
	}

	fwEntity* const pEntity = g_pDecalCallbacks->GetEntityFromInst(pPhInst);
	Assert(pEntity);

	decalAssertf(pPhysicsLevel, "Physics Level is NULL");

	bool shouldProcess = true;
	g_pDecalCallbacks->GetEntityInfo(pEntity, settings.user.bucketSettings.typeSettingsIndex, settings.user.bucketSettings.roomId, settings.internal.isPersistent, settings.internal.interiorLocation, shouldProcess);

	// check if we should be processing this entity
	if (!shouldProcess)
	{
		return;
	}

	// don't project if the persistent limit has been reached and this is a persistent entity
	if (m_persistentLimitReached)
	{
		if (settings.internal.isPersistent)
		{
			return;
		}
	}

	if (settings.user.bucketSettings.pSmashGroup)
	{
		// calculate the smash group matrix
		Mat34V smashGroupWldMtx = Mat34V(V_IDENTITY);
		bool ignoreIsDamaged;
		decalHelper::CalcMatrix(smashGroupWldMtx, &ignoreIsDamaged, pEntity, pPhInst, settings.user.pipelineSettings.colnComponentId, DECAL_ATTACH_TYPE_SMASHGROUP);
		decalAssertf(IsFiniteAll(smashGroupWldMtx), "decal bucket matrix isn't finite 5 (pEntity:%p, attachType:%d)", pEntity, DECAL_ATTACH_TYPE_SMASHGROUP);

		// Kick physics bounds task
		const rmcDrawable *const pDrawable = NULL;
		KickProcessInstAsync(DECAL_DO_PROCESSSMASHGROUP, settings, pEntity, pPhInst, pDrawable, smashGroupWldMtx, pPhysicsLevel);
	}
	else
	{
		Mat34V entityMtx = pEntity->GetMatrix();

		fragInst *const pFragInst = pEntity->GetFragInst();
		if (pFragInst)
		{
			const fragType *const pFragType = pFragInst->GetType();
			decalAssert(pFragType);

			// Validate that deprecated old frag setups are not being used
			artAssertf(pFragType->GetSkinned(),
				"frag \"%s\" is not \"skinned\", this is unsupported for decals",
				GetEntityName(pEntity));
			artAssertf(!pFragType->GetRootChild() || pFragType->GetRootChild()->GetUndamagedEntity()==pFragType->GetCommonDrawable(),
				"frag \"%s\" has a different root drawable than the common drawable, this is unsupported for decals",
				GetEntityName(pEntity));

			unsigned numDrawables;
			unsigned damagedDrawableMask;
			const rmcDrawable *pDrawables[DECAL_MAX_DRAWABLES];
			unsigned numSkelMtxs;
			const Vec4V *skelObjMtxs;
			DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones;
			u8 *skelDrawableIdxs;
			const Mat34V *skelInvJoints;
			switch (PackFragSkeletons(&numSkelMtxs, &skelObjMtxs, &skelBones, &numDrawables, &damagedDrawableMask, &skelDrawableIdxs, pDrawables, &skelInvJoints, pEntity, decalBoneHierarchyIdx(settings.user.lodSettings.lodAttachMatrixId), pFragInst, pFragType))
			{
				case PACKSKEL_FAILED:
				case PACKSKEL_EMPTYSKEL:
					return;

				case PACKSKEL_NEW:
					decalAssert(skelDrawableIdxs);
					g_pDecalCallbacks->PatchModdedDrawables(&numDrawables, &damagedDrawableMask, pDrawables, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, pEntity);
					RemoveUnusedDrawables(&numDrawables, &damagedDrawableMask, pDrawables, numSkelMtxs, &skelDrawableIdxs);
					break;

				default:;
			}

			KickProcessDrawables(settings, pEntity, pFragInst, numDrawables, damagedDrawableMask, pDrawables, entityMtx, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, pPhysicsLevel);

			// kick an additional task for any breakable glass
			// do this after initial kick so that defrag locking on fragment is already done
			KickProcessInstAsync(DECAL_DO_PROCESSBREAKABLEGLASS, settings, pEntity, pFragInst, *pDrawables, entityMtx, pPhysicsLevel);
		}

		else
		{
			// try to get a drawable from the entity
			const rmcDrawable* pDrawable = g_pDecalCallbacks->GetDrawable(
				pEntity, pPhInst, settings.user.pipelineSettings.onlyApplyToGlass, settings.user.pipelineSettings.dontApplyToGlass);
#			if __BANK
				if (m_debug.m_forceStaticDecalsOnBounds)
				{
					pDrawable = NULL;
				}
#			endif

			if (pDrawable)
			{
				const crSkeleton *const pUndamagedSkel = pEntity->GetEntitySkeleton();
				if (pUndamagedSkel)
				{
					unsigned numSkelMtxs;
					const Vec4V *skelObjMtxs;
					DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones;
					unsigned numDrawables;
					unsigned damagedDrawableMask;
					u8 *skelDrawableIdxs;
					const rmcDrawable *pDrawables[DECAL_MAX_DRAWABLES];
					pDrawables[0] = pDrawable; // PackSkeletons cannot always initialize this for us
					const crSkeleton *const pDamagedSkel = NULL;
					switch (PackSkeletons(&numSkelMtxs, &skelObjMtxs, &skelBones, &numDrawables, &damagedDrawableMask, &skelDrawableIdxs, pDrawables, pEntity, decalBoneHierarchyIdx(settings.user.lodSettings.lodAttachMatrixId), pUndamagedSkel, pDamagedSkel))
					{
						case PACKSKEL_FAILED:
						case PACKSKEL_EMPTYSKEL:
							return;

						case PACKSKEL_NEW:
							// Free up the skelDrawableIdxs array.  There can only be
							// one drawable, so this is unneeded.
							decalAssert(skelDrawableIdxs == m_taskDataStoreFreeUpper);
							m_taskDataStoreFreeUpper = skelDrawableIdxs + numSkelMtxs;
							skelDrawableIdxs = NULL;
							break;

						case PACKSKEL_NOSKEL:
							decalAssertf(0, "Should never have gotten here");
							break;

						case PACKSKEL_REUSE:
							decalAssert(!skelDrawableIdxs);
							break;
					}

					decalAssert(numDrawables == 1);
					decalAssert(damagedDrawableMask == 0);
					decalAssert(pDrawables[0] == pDrawable);
					const Mat34V *const skelInvJoints = pUndamagedSkel->GetSkeletonData().GetCumulativeInverseJoints();
					KickProcessDrawables(settings, pEntity, pPhInst, numDrawables, damagedDrawableMask, pDrawables, entityMtx, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, pPhysicsLevel);
				}
				else
				{
					const crSkeletonData *const skelData = pDrawable->GetSkeletonData();
					if (skelData)
					{
						unsigned numSkelMtxs;
						const Vec4V *skelObjMtxs;
						DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones;
						switch (PackSkeletonData(&numSkelMtxs, &skelObjMtxs, &skelBones, pEntity, decalBoneHierarchyIdx(settings.user.lodSettings.lodAttachMatrixId), skelData))
						{
							case PACKSKEL_FAILED:
							case PACKSKEL_EMPTYSKEL:
								return;
							default:;
						}

						const u8 *const skelDrawableIdxs = NULL;
						const unsigned numDrawables = 1;
						const unsigned damagedDrawableMask = 0;
						const Mat34V *const skelInvJoints = skelData->GetCumulativeInverseJoints();
						KickProcessDrawables(settings, pEntity, pPhInst, numDrawables, damagedDrawableMask, &pDrawable, entityMtx, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, pPhysicsLevel);
					}
					else
					{
						// Entity has no skeleton, so it cannot be skinned.  .: we
						// can directly kick a non-skinned drawable task.
						KickProcessInstAsync(DECAL_DO_PROCESSDRAWABLESNONSKINNED, settings, pEntity, pPhInst, pDrawable, entityMtx, pPhysicsLevel);
					}
				}
			}
			else
			{
				// Kick physics bounds task
				KickProcessInstAsync(DECAL_DO_PROCESSBOUND, settings, pEntity, pPhInst, pDrawable, entityMtx, pPhysicsLevel);
			}
		}
	}
}


#if USE_DEFRAGMENTATION

	///////////////////////////////////////////////////////////////////////////
	//  DefragLock
	///////////////////////////////////////////////////////////////////////////

	bool decalManager::DefragLock(decalAsyncTaskDescBase* taskDesc)
	{
#		if !__ASSERT
#			define DECAL_DEFRAG_LOCK_RAW_PTR(PTR) do{if(Unlikely(!m_defragLocker.LockRawPtr(PTR)))return false;}while(0)
#			define DECAL_DEFRAG_LOCK_PGBASE(PTR)  do{if(Unlikely(!m_defragLocker.LockPgBase(PTR)))return false;}while(0)
#		else
			// For assert enabled builds, we keep trying to lock all pointers.
			// This is so that if a lock fails due to defragging, then we won't
			// get a spurious assert on the main thread about a pointer not
			// being locked.
			bool ret = true;
#			define DECAL_DEFRAG_LOCK_RAW_PTR(PTR) do{if(Unlikely(!m_defragLocker.LockRawPtr(PTR)))ret=false;}while(0)
#			define DECAL_DEFRAG_LOCK_PGBASE(PTR)  do{if(Unlikely(!m_defragLocker.LockPgBase(PTR)))ret=false;}while(0)
#		endif

		// Macro to assert that the locks performed with DECAL_DEFRAG_LOCK* are
		// sufficent to lock all data that asynchronous code will touch
#		define DECAL_DEFRAG_ASSERT_LOCKED(PTR) decalAssert(m_defragLocker.AssertOnlyIsAlreadyLocked(PTR))


		// Entities shouldn't need locking (ie, they should not be in a
		// defraggable resource heap).
		const fwEntity *const pEntity = taskDesc->pEntity;
		DECAL_DEFRAG_ASSERT_LOCKED(pEntity);

		switch (taskDesc->taskType)
		{
			case DECAL_DO_PROCESSBREAKABLEGLASS:
			{
				const phInst *const pPhInst = taskDesc->pPhInst;
				decalAssert(dynamic_cast<const fragInst*>(pPhInst));
				const fragInst *const pFragInst = static_cast<const fragInst*>(pPhInst);
				// No need to lock pPhInst/pFragInst here, it will be locked in
				// the DECAL_DO_PROCESSBOUND case below (which we fall through
				// to).
				const fragType *const pFragType = pFragInst->GetType();
				DECAL_DEFRAG_LOCK_PGBASE(pFragType);

				const fragPhysicsLOD *const pPhysLod = pFragInst->GetTypePhysics();
				DECAL_DEFRAG_LOCK_RAW_PTR(pPhysLod);

				fragTypeChild **const ppChildren = pPhysLod->GetAllChildren();
				const unsigned numChildren = pPhysLod->GetNumChildren();
				for (unsigned i=0; i<numChildren; i++)
				{
					const unsigned groupIndex = ppChildren[i]->GetOwnerGroupPointerIndex();
					if (!pPhysLod->GetAllGroups()[groupIndex]->GetMadeOfGlass())
					{
						const unsigned firstChildInGroupIndex = pFragInst->GetTypePhysics()->GetAllGroups()[groupIndex]->GetChildFragmentIndex();
						if (pFragInst->GetCacheEntry() && pFragInst->GetChildBroken(i) && pFragInst->GetCacheEntry()->GetHierInst()->groupInsts[ppChildren[i]->GetOwnerGroupPointerIndex()].IsDamaged())
						{
							DECAL_DEFRAG_LOCK_RAW_PTR(ppChildren[firstChildInGroupIndex]->GetDamagedEntity());
						}
						else
						{
							DECAL_DEFRAG_LOCK_RAW_PTR(ppChildren[firstChildInGroupIndex]->GetUndamagedEntity());
						}
					}
				}

				// Fall through to DECAL_DO_PROCESSBOUND case
			}

			case DECAL_DO_PROCESSBOUND:
			{
				// phInst is derived from pgBase, but pPhInst doesn't
				// necessarily point to a "toplevel resource", so can't use
				// DECAL_DEFRAG_LOCK_PGBASE here.  Same deal with phArchetype.
				const phInst *const pPhInst = taskDesc->pPhInst;
				DECAL_DEFRAG_LOCK_RAW_PTR(pPhInst);
				const phArchetype *const pArchetype = pPhInst->GetArchetype();
				DECAL_DEFRAG_LOCK_RAW_PTR(pArchetype);
				const phBound *const pBound = pArchetype->GetBound();
				DECAL_DEFRAG_LOCK_RAW_PTR(pBound);

				if (pBound->GetType() == phBound::COMPOSITE)
				{
					const phBoundComposite* pBoundComposite = static_cast<const phBoundComposite*>(pBound);
					const unsigned numBounds = pBoundComposite->GetNumBounds();
					for (unsigned i=0; i<numBounds; ++i)
					{
						DECAL_DEFRAG_LOCK_RAW_PTR(pBoundComposite->GetBound(i));
					}
				}
				break;
			}

			case DECAL_DO_PROCESSDRAWABLESSKINNED:
			{
				DECAL_DEFRAG_LOCK_RAW_PTR(taskDesc->skelInvJoints);

				// Fall through to DECAL_DO_PROCESSDRAWABLESNONSKINNED case
			}

			case DECAL_DO_PROCESSDRAWABLESNONSKINNED:
			{
				DECAL_DEFRAG_LOCK_RAW_PTR(taskDesc->skelInvJoints);
				const unsigned numDrawables = taskDesc->numDrawables;
				for (unsigned drawableIdx=0; drawableIdx<numDrawables; ++drawableIdx)
				{
					const rmcDrawable *const pDrawable = taskDesc->drawablePtrs[drawableIdx];
					DECAL_DEFRAG_LOCK_RAW_PTR(pDrawable);
					const grmShaderGroup *const pShaderGroup = pDrawable->GetShaderGroupPtr();
					DECAL_DEFRAG_LOCK_RAW_PTR(pShaderGroup);
					const rmcLod *const pLod = g_pDecalCallbacks->GetLod(pEntity, pDrawable);
					DECAL_DEFRAG_LOCK_RAW_PTR(pLod);
					const unsigned modelCount = pLod->GetCount();
					for (unsigned i=0; i<modelCount; ++i)
					{
						const grmModel *const pModel = pLod->GetModel(i);
						const unsigned geomCount = pModel->GetGeometryCount();
						for (unsigned j=0; j<geomCount; ++j)
						{
							DECAL_DEFRAG_ASSERT_LOCKED(&pShaderGroup->GetShader(pModel->GetShaderIndex(j)));
							const grmGeometry *const geom = &pModel->GetGeometry(j);
							DECAL_DEFRAG_ASSERT_LOCKED(geom);
#						if USE_EDGE
							if (geom->GetType() == grmGeometry::GEOMETRYEDGE)
							{
								// Nothing more needed
							}
							else
#						endif
							{
								const grcIndexBuffer *const pIB = geom->GetIndexBuffer();
								DECAL_DEFRAG_LOCK_RAW_PTR(pIB);
								DECAL_DEFRAG_LOCK_RAW_PTR(pIB->GetUnsafeReadPtr());

								const grcVertexBuffer *const pVB = geom->GetVertexBuffer();
								DECAL_DEFRAG_LOCK_RAW_PTR(pVB);
								DECAL_DEFRAG_LOCK_RAW_PTR(pVB->GetUnsafeReadPtr());

								const grcFvf *const pFvf = pVB->GetFvf();
								DECAL_DEFRAG_LOCK_RAW_PTR(pFvf);
							}
						}
					}
				}
				DECAL_DEFRAG_ASSERT_LOCKED(taskDesc->renderToHierarchyLut.GetPtr());
				break;
			}

			case DECAL_DO_PROCESSSMASHGROUP:
			{
				break;
			}

			// This should be an impossible code path to reach, but we need this
			// case to prevent compiler warnings.
			case NUM_DECAL_ASYNC_TASK_TYPES:
			{
				decalAssert(0);
			}
		}

#		if !__ASSERT
			return true;
#		else
			return ret;
#		endif

#		undef DECAL_DEFRAG_ASSERT_LOCKED
#		undef DECAL_DEFRAG_LOCK_PGBASE
#		undef DECAL_DEFRAG_LOCK_RAW_PTR
	}

#endif // USE_DEFRAGMENTATION


///////////////////////////////////////////////////////////////////////////////
//  KickProcessInstAsync
///////////////////////////////////////////////////////////////////////////////

void decalManager::KickProcessInstAsync(decalAsyncTaskType type, decalSettings& settings, fwEntity* pEntity, phInst *pPhInst, const rmcDrawable* pDrawable, Mat34V_In entityMtx, phLevelNew* pPhysicsLevel)
{
	const unsigned numDrawables = 1;
	const unsigned damagedDrawableMask = 0;
	const unsigned numSkelMtxs = 0;
	const Vec4V *const skelObjMtxs = NULL;
	const DECAL_BONEREMAPLUT_CTOR(Task,Hierarchy) skelBones(NULL);
	const u8 *const skelDrawableIdxs = NULL;
	const Mat34V *const skelInvJoints = NULL;
	KickProcessInstAsync(type, settings, pEntity, pPhInst, numDrawables, damagedDrawableMask, &pDrawable, entityMtx, numSkelMtxs, skelObjMtxs, skelBones, skelDrawableIdxs, skelInvJoints, pPhysicsLevel);
}


///////////////////////////////////////////////////////////////////////////////
//  KickProcessInstAsync
///////////////////////////////////////////////////////////////////////////////

void decalManager::KickProcessInstAsync(decalAsyncTaskType type, decalSettings& settings, fwEntity* pEntity, phInst *pPhInst, unsigned numDrawables, unsigned damagedDrawableMask, const rmcDrawable*const* pDrawables, Mat34V_In entityMtx, unsigned numSkelMtxs, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8* skelDrawableIdxs, const Mat34V* skelInvJoints, phLevelNew* pPhysicsLevel)
{
	// Attempt to allocate a task descriptor structure
	decalAsyncTaskDescBase *td = g_pDecalCallbacks->AllocAsyncTaskDesk(type, settings, pEntity, pPhInst);
	if (Unlikely(!td))
	{
		// This warning is going off so often that it isn't really worth warning
		// about.
		//
		// TODO: Need to optimize the decalSettings structure so that the task
		// descriptors aren't so gigantic, and then we can increase the number
		// of them.
		//
		decalSpamf("decalManager::KickProcessInstAsync out of task descriptors, decals not generated");
		return;
	}

	// Fill in the basics

	decalSettings::ValidateMatrix(entityMtx);
	td->entityMtx               = entityMtx;
	td->settings                = settings;
	td->typeSettings            = decalSettingsManager::GetTypeSettings(settings.user.bucketSettings.typeSettingsIndex);
	td->renderSettings          = decalSettingsManager::GetRenderSettings(settings.user.bucketSettings.renderSettingsIndex);
	td->pEntity                 = pEntity;
	td->taskType                = type;
	StoreScalar32FromScalarV(td->entityRadius, g_pDecalCallbacks->GetEntityBoundRadius(pEntity));
	td->taskInstCounter         = m_taskInstCounters+m_taskInstCountersUsed;

#	if DECAL_DEBUG_NAME
		strncpy(td->debugName, GetEntityName(pEntity), NELEM(td->debugName));
		td->debugName[NELEM(td->debugName)-1] = '\0';
#	endif

	switch (type)
	{
		case DECAL_DO_PROCESSBOUND:
		case DECAL_DO_PROCESSBREAKABLEGLASS:
		{
			td->pPhInst                 = pPhInst;
			td->includeFlags            = pPhysicsLevel->GetObjectDataArray()[pPhInst->GetLevelIndex()].m_CachedArchIncludeFlags;
#			if __PS3
				td->phInstSize              = (unsigned)g_pDecalCallbacks->GetPhysInstSize(pEntity, pPhInst);
#			endif
			break;
		}

		case DECAL_DO_PROCESSDRAWABLESNONSKINNED:
		case DECAL_DO_PROCESSDRAWABLESSKINNED:
		{
			td->skelObjMtxs             = skelObjMtxs;
			td->skelBones               = skelBones;
			td->skelDrawableIdxs        = skelDrawableIdxs;
			td->skelInvJoints           = skelInvJoints;
			sysMemCpy(td->drawablePtrs, pDrawables, numDrawables*sizeof(*pDrawables));
 			const fwArchetype *const archetype = pEntity->GetArchetype();
 			decalAssert(archetype);
			td->renderToHierarchyLut.SetPtr(archetype->GetLodSkeletonMap());
			td->numSkelMtxs             = (u8)numSkelMtxs;
			td->numRenderBones          = (u8)archetype->GetLodSkeletonBoneNum();
			td->numDrawables            = (u8)numDrawables;
			td->damagedDrawableMask     = (u8)damagedDrawableMask;
			decalAssert(!td->renderToHierarchyLut ^ !!td->numRenderBones);
			break;
		}

		case DECAL_DO_PROCESSSMASHGROUP:
		{
			break;
		}

		// Other cases just to stop compiler warning
		case NUM_DECAL_ASYNC_TASK_TYPES:
		{
			break;
		}
	}

	decalAssert(g_pDecalCallbacks->GetEntityFromInst(pPhInst) == pEntity);
	decalAssert(g_pDecalCallbacks->GetPhysInstFromEntity(pEntity) == pPhInst);

#	if __BANK
		bool async = !m_debug.m_disableAsync;
#	else
		bool async = true;
#	endif

#	if __PS3
		td->decalMan    = this;
		td->entitySize  = (unsigned)g_pDecalCallbacks->GetEntitySize(pEntity);
#	endif // __PS3

	td->mainThreadData.regdEnt = pEntity;

	decalAddRefs(td);


#	if USE_DEFRAGMENTATION
		// Lock all memory allocations we are going to touch so that they don't get
		// moved by the defragger while they are being used.
		if (Unlikely(!DefragLock(td)))
		{
			async = false;
			decalSpamf("decalManager::KickProcessInstAsync failed to lock all memory allocations, using slow path");
		}
#	endif


	// Attempt to kick the asynchronous task
	sysTaskHandle h = NULL;
	if (Likely(async))
	{
		td->bucketLists = OffsetOf(decalManager, m_pendingBucketLists);

		sysTaskParameters params;
		params.Input.Size = g_pDecalCallbacks->GetAsyncTaskDescSize();
		params.Input.Data = td;

#		if !__PS3
			// DECAL_DO_PROCESSBREAKABLEGLASS is not currently thread safe
			if (type != DECAL_DO_PROCESSBREAKABLEGLASS)
			{
				h = sysTaskManager::Create(TASK_INTERFACE(ProcessInstTask), params);
			}
#		else
#			define LOAD_TASK_INTERFACE(TYPE)    m_spuTasks[TYPE].m_name, m_spuTasks[TYPE].m_binStart, m_spuTasks[TYPE].m_binSize
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				const u32 scratchSizeAdjust = 0x800;
#			else
				const u32 scratchSizeAdjust = 0;
#			endif
			switch (type)
			{
				case DECAL_DO_PROCESSBOUND:
					params.Scratch.Size = 0x10000 - scratchSizeAdjust;
					params.SpuStackSize = 0xa000;
					h = sysTaskManager::Create(LOAD_TASK_INTERFACE(DECAL_DO_PROCESSBOUND), params);
					break;
				case DECAL_DO_PROCESSBREAKABLEGLASS:
					// No SPU support currently
					break;
				case DECAL_DO_PROCESSDRAWABLESNONSKINNED:
					params.Scratch.Size = 0x1f800 - scratchSizeAdjust;
					params.SpuStackSize = 0x6600;
					h = sysTaskManager::Create(LOAD_TASK_INTERFACE(DECAL_DO_PROCESSDRAWABLESNONSKINNED), params);
					break;
				case DECAL_DO_PROCESSDRAWABLESSKINNED:
					params.Scratch.Size = 0x1f000 - scratchSizeAdjust;
					params.SpuStackSize = 0x6600;
					h = sysTaskManager::Create(LOAD_TASK_INTERFACE(DECAL_DO_PROCESSDRAWABLESSKINNED), params);
					break;
				case DECAL_DO_PROCESSSMASHGROUP:
					params.Scratch.Size = 0x10000 - scratchSizeAdjust;
					params.SpuStackSize = 0xa000;
					h = sysTaskManager::Create(LOAD_TASK_INTERFACE(DECAL_DO_PROCESSSMASHGROUP), params);
					break;
			}
#			undef LOAD_TASK_INTERFACE
#		endif
#if !__PS3 // TODO: this warning should be enabled on all platforms once PS3 is properly implemented
		if (Unlikely(!h))
		{
			decalSpamf("decalManager::KickProcessInstAsync failed to create task, using slow path");
		}
#endif
	}

	if (Likely(h))
	{
		// Succesfully kicked asynchronous task, store required data for Synchronize()
		td->mainThreadData.handle = h;
		td->mainThreadData.next = m_runningAsyncTaskDesc;
		m_runningAsyncTaskDesc = td;
		AddDecalIdInstance(td->settings.internal.id);
	}
	else BANK_ONLY(if(!m_debug.m_disableSync))
	{
		// Run serialy on main thread if any allocation failed (or if forced by
		// debug toggle)
		td->bucketLists = OffsetOf(decalManager, m_synchronousBucketLists);

		ProcessInst(DECAL_DEBUG_NAME_ONLY(td->pEntity->GetModelName(),)
			td, td, td->typeSettings, td->renderSettings, td->pEntity, td->pPhInst,
			td->skelObjMtxs, td->skelBones, td->skelDrawableIdxs, td->skelInvJoints,
			td->renderToHierarchyLut);
		td->mainThreadData.next = m_runningSyncTaskDesc;
		m_runningSyncTaskDesc = td;
	}
#if __BANK
	else
	{
		g_pDecalCallbacks->FreeAsyncTaskDesc(td);
	}
#endif
}


///////////////////////////////////////////////////////////////////////////////
//  Wash
///////////////////////////////////////////////////////////////////////////////

void decalManager::Wash(float amount, const fwEntity* pEntity, bool dueToRain)
{
	// wash the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pEntity==NULL || pEntity==pCurrBucket->m_attachEntity)
				{
					// make sure we don't wash interior decals due to rain
					if (pCurrBucket->GetInteriorLocation().IsValid() || dueToRain==false)
					{
						pCurrBucket->Wash(amount);
					}
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  WashInRange
///////////////////////////////////////////////////////////////////////////////

void decalManager::WashInRange(float amount, Vec3V_In vPos, ScalarV_In range, const fwEntity* pEntity)
{
	// wash the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pEntity==NULL || pEntity==pCurrBucket->m_attachEntity)
				{
					pCurrBucket->WashInRange(amount, vPos, range);
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  IsDecalPending
///////////////////////////////////////////////////////////////////////////////

bool decalManager::IsDecalPending(int decalId) const
{
	// Check the add infos
	for (unsigned i=0; i<m_numAddInfos; ++i)
	{
		if (Unlikely(m_addInfos[i].id == decalId && m_addInfos[i].shouldProcess))
		{
			return true;
		}
	}

	// Check the running asynchronous tasks
	const decalAsyncTaskDescBase *td = m_runningAsyncTaskDesc;
	while (td)
	{
		if (Unlikely(td->settings.internal.id == decalId))
		{
			return true;
		}
		td = td->mainThreadData.next;
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  GetWashLevel
///////////////////////////////////////////////////////////////////////////////

float decalManager::GetWashLevel(int decalId)
{
	// process the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			const decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				float washLevel = pCurrBucket->GetWashLevel(decalId);
				if (washLevel>-1.0f)
				{
					return washLevel;
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// Check the pending decals.  The decal could temporarily be here due to a
	// low -> high lod transition.
	if (Unlikely(IsDecalPending(decalId)))
	{
		// Decal instances will be created with a random wash level >= 1.f, so
		// returning 1.f here is "close enough".
		return 1.f;
	}

	return -1.0f;
}


///////////////////////////////////////////////////////////////////////////////
//  IsActive
///////////////////////////////////////////////////////////////////////////////

bool decalManager::IsAlive(int decalId)
{
	if (decalId)
	{
		const u32 idx = (u32)(decalId-0x00010000)>>16;
		const u32 seq = (u32)decalId&0x0000ffff;
		decalAssert(idx < NELEM(m_decalIdAlive));
		return m_decalIdAlive[idx] == seq;
	}
	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  Remove
///////////////////////////////////////////////////////////////////////////////

void decalManager::Remove(int decalId)
{
	// process the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				pCurrBucket->Remove(decalId);
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  Remove
///////////////////////////////////////////////////////////////////////////////

void decalManager::Remove(const fwEntity* pEntity, const int boneIndex, const void* pSmashGroup, u32 exceptionTypeSettingsFlags)
{
	// process the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				// check if only decals from a specific bone should be removed
				bool passedBoneTest = true;
				if (boneIndex>-1)
				{
					// check that the decal is attached to a bone
					if (pCurrBucket->m_attachType==DECAL_ATTACH_TYPE_BONE)
					{
						// if the attach matrix and bone index don't match then we don't want to remove decals from this bone
						if (pCurrBucket->m_attachMatrixId!=boneIndex)
						{
							passedBoneTest = false;
						}
					}
				}

				if (pEntity==NULL || (pEntity==pCurrBucket->m_attachEntity && passedBoneTest))
				{
					if (pSmashGroup==NULL || pSmashGroup==pCurrBucket->m_pSmashGroup)
					{
						if (decalManager::IsFlaggedType(pCurrBucket->m_typeSettingsIndex, exceptionTypeSettingsFlags)==false)
						{
							pCurrBucket->MarkForDelete();
						}
					}
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// process the add infos
	for (unsigned i=0; i<m_numAddInfos; i++)
	{
		if (pEntity==NULL || pEntity==m_addInfos[i].entity)
		{
			if (pSmashGroup==NULL || pSmashGroup==m_addInfos[i].settings.user.bucketSettings.pSmashGroup)
			{
				if (decalManager::IsFlaggedType(m_addInfos[i].settings.user.bucketSettings.typeSettingsIndex, exceptionTypeSettingsFlags)==false)
				{
					m_addInfos[i].shouldProcess = false;
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  RemoveFacing
///////////////////////////////////////////////////////////////////////////////

void decalManager::RemoveFacing(Vec3V_In vDir, const fwEntity* pEntity, const void* pSmashGroup)
{
	// process the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->IsPersistent()==false)
				{
					if (pEntity==NULL || pEntity==pCurrBucket->m_attachEntity)
					{
						if (pSmashGroup==NULL || pSmashGroup==pCurrBucket->m_pSmashGroup)
						{
							pCurrBucket->RemoveFacing(vDir);
						}
					}
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// process the add infos
	for (unsigned i=0; i<m_numAddInfos; i++)
	{
		if (pEntity==NULL || pEntity==m_addInfos[i].entity)
		{
			if (pSmashGroup==NULL || pSmashGroup==m_addInfos[i].settings.user.bucketSettings.pSmashGroup)
			{
				Vec3V vInstDirWld = m_addInfos[i].settings.user.instSettings.vDirection;
				if (Dot(vInstDirWld, vDir).Getf()>0.0f)
				{
					m_addInfos[i].shouldProcess = false;
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  RemoveInRange
///////////////////////////////////////////////////////////////////////////////

int decalManager::RemoveInRange(Vec3V_In vPos, ScalarV_In range, const fwEntity* pEntity, const void* pSmashGroup)
{
	int iTotalRemoved = 0;

	// process the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->IsPersistent()==false)
				{
					if (pEntity==NULL || pEntity==pCurrBucket->m_attachEntity)
					{
						if (pSmashGroup==NULL || pSmashGroup==pCurrBucket->m_pSmashGroup)
						{
							iTotalRemoved += pCurrBucket->RemoveInRange(vPos, range);
						}
					}
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// process the add infos
	for (unsigned i=0; i<m_numAddInfos; i++)
	{
		if (pEntity==NULL || pEntity==m_addInfos[i].entity)
		{
			if (pSmashGroup==NULL || pSmashGroup==m_addInfos[i].settings.user.bucketSettings.pSmashGroup)
			{
				const ScalarV distSqr = MagSquared(m_addInfos[i].settings.user.instSettings.vPosition-vPos);
				if (IsLessThanAll(distSqr, range*range))
				{
					m_addInfos[i].shouldProcess = false;
					iTotalRemoved--;
				}
			}
		}
	}

	return iTotalRemoved;
}


///////////////////////////////////////////////////////////////////////////////
//  FadeInRange
///////////////////////////////////////////////////////////////////////////////

void decalManager::FadeInRange(Vec3V_In vPos, ScalarV_In range, float fadeTime, const fwEntity* pEntity, const void* pSmashGroup)
{
	// process the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->IsPersistent()==false)
				{
					if (pEntity==NULL || pEntity==pCurrBucket->m_attachEntity)
					{
						if (pSmashGroup==NULL || pSmashGroup==pCurrBucket->m_pSmashGroup)
						{
							pCurrBucket->FadeInRange(vPos, range, fadeTime);
						}
					}
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// process the add infos
	for (unsigned i=0; i<m_numAddInfos; i++)
	{
		if (pEntity==NULL || pEntity==m_addInfos[i].entity)
		{
			if (pSmashGroup==NULL || pSmashGroup==m_addInfos[i].settings.user.bucketSettings.pSmashGroup)
			{
				const ScalarV distSqr = MagSquared(m_addInfos[i].settings.user.instSettings.vPosition-vPos);
				if (IsLessThanAll(distSqr, range*range))
				{
					m_addInfos[i].shouldProcess = false;
				}
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  UpdateAttachEntity
///////////////////////////////////////////////////////////////////////////////

void decalManager::UpdateAttachEntity(const fwEntity* pEntityFrom, fwEntity* pEntityTo)
{
	// Check the buckets that have already been completed.
	for (unsigned p=0; p<(unsigned)DECAL_NUM_RENDER_PASSES; ++p)
	{
		for (unsigned m=0; m<(unsigned)DECAL_NUM_METHODS; ++m)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->GetAttachEntity() == pEntityFrom)
				{
					pCurrBucket->SetAttachEntity(pEntityTo);
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// If there is any currently running decal generation tasks that reference
	// the entity that is being replaced, then block on their completion.
	if (Unlikely(OnDelete(pEntityFrom)))
	{
		for (unsigned p=0; p<(unsigned)DECAL_NUM_RENDER_PASSES; ++p)
		{
			for (unsigned m=0; m<(unsigned)DECAL_NUM_METHODS; ++m)
			{
				decalBucket* pCurrBucket = m_pendingBucketLists[p][m].SyncPeek();
				while (pCurrBucket)
				{
					decalAsyncTaskDescBase *const td = pCurrBucket->GetTaskDesc();
					if (td->mainThreadData.regdEnt == pEntityFrom)
					{
						td->mainThreadData.regdEnt = pEntityTo;
					}
					pCurrBucket = pCurrBucket->GetNext();
				}

				decalAssert(m_synchronousBucketLists[p][m].IsEmpty());
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
//  IsFlaggedType
///////////////////////////////////////////////////////////////////////////////

bool decalManager::IsFlaggedType(u32 typeSettingsIndex, u32 typeSettingsFlags)
{
	return ((1<<typeSettingsIndex & typeSettingsFlags) > 0);
}


///////////////////////////////////////////////////////////////////////////////
//  IsWaitingToAdd
///////////////////////////////////////////////////////////////////////////////

bool decalManager::IsWaitingToAdd(u32 typeSettingsFlags, const fwEntity* pEntity)
{
	for (unsigned i=0; i<m_numAddInfos; i++)
	{
		if (decalManager::IsFlaggedType(m_addInfos[i].settings.user.bucketSettings.typeSettingsIndex, typeSettingsFlags) &&
			m_addInfos[i].entity==pEntity)
		{
			return true;
		}
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  IsPointInLiquid
///////////////////////////////////////////////////////////////////////////////

bool decalManager::IsPointInLiquid(u32 typeSettingsFlags, s8& liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, u32& foundDecalTypeFlag, float distThresh)
{
	// check the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->IsMarkedForDelete()==false && decalManager::IsFlaggedType(pCurrBucket->m_typeSettingsIndex, typeSettingsFlags))
				{
					if (pCurrBucket->IsPointInLiquid(liquidType, vPos, minDecalSize, removeDecal, fadeDecal, fadeTime, m==DECAL_METHOD_DYNAMIC, distThresh))
					{
						foundDecalTypeFlag = 1<<pCurrBucket->m_typeSettingsIndex;
						return true;
					}
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  IsPointWithin
///////////////////////////////////////////////////////////////////////////////

bool decalManager::IsPointWithin(u32 typeSettingsFlags, s8 liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, u32& foundDecalTypeFlag, float distThresh)
{
	// check the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->IsMarkedForDelete()==false && decalManager::IsFlaggedType(pCurrBucket->m_typeSettingsIndex, typeSettingsFlags))
				{
					if (pCurrBucket->IsPointWithin(liquidType, vPos, minDecalSize, removeDecal, fadeDecal, fadeTime, m==DECAL_METHOD_DYNAMIC, distThresh))
					{
						foundDecalTypeFlag = 1<<pCurrBucket->m_typeSettingsIndex;
						return true;
					}
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  FindClosest
///////////////////////////////////////////////////////////////////////////////

bool decalManager::FindClosest(u32 typeSettingsFlags, s8 liquidType, Vec3V_In vPos, float range, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, Vec3V_InOut vClosestPosWld, fwEntity** ppClosestEntity, u32& foundDecalTypeFlag)
{
	const ScalarV minDecalSizeV(minDecalSize);
	ScalarV closestDistSqr = ScalarV(range*range);
	decalBucket* pClosestBucket = NULL;
	decalInst* pClosestInst = NULL;

	// check the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				// check the type is valid
				if (pCurrBucket->IsMarkedForDelete()==false && decalManager::IsFlaggedType(pCurrBucket->m_typeSettingsIndex, typeSettingsFlags))
				{
					decalInst* pCurrInst = pCurrBucket->m_instList.Peek();
					while (pCurrInst)
					{
						if (pCurrInst->IsMarkedForDelete()==false && (liquidType==-1 || liquidType==pCurrInst->m_liquidType)) 
						{
							// check the size is valid
							const ScalarV decalSize = pCurrInst->GetBoundRadius()*ScalarV(pCurrInst->CalcUVMult()*0.707f);
							if (IsGreaterThanOrEqualAll(decalSize, minDecalSizeV) && !pCurrInst->IsFadingOut())
							{
								// get the decal position
								Vec3V vDecalPosition = Transform(pCurrBucket->m_wldMtx, pCurrInst->GetPosition());

								// get distance to centre of decal
								ScalarV distSqr = MagSquared(vDecalPosition - vPos);

								// adjust to get distance to edge of decal
								distSqr = Max(ScalarV(V_ZERO), distSqr-(decalSize*decalSize));

								if (IsLessThanAll(distSqr, closestDistSqr))
								{
									closestDistSqr = distSqr;
									pClosestBucket = pCurrBucket;
									pClosestInst = pCurrInst;
									vClosestPosWld = vDecalPosition;
									if (pCurrBucket->GetAttachEntity())
									{
										*ppClosestEntity = pCurrBucket->GetAttachEntity();
									}
									foundDecalTypeFlag = 1<<pCurrBucket->m_typeSettingsIndex;
								}
							}
						}

						pCurrInst = pCurrInst->GetNext();
					}
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// check if an inst in range has been found
	if (pClosestInst)
	{
		if (removeDecal)
		{
			pClosestBucket->RemoveInstSafe(pClosestInst);
		}
		else if (fadeDecal)
		{
			pClosestInst->StartFadingOut(fadeTime);
		}

		return true;
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  FindFarthest
///////////////////////////////////////////////////////////////////////////////

bool decalManager::FindFarthest(u32 typeSettingsFlags, s8 liquidType, Vec3V_In vPos, float range, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, Vec3V_InOut vFarthestPosWld, fwEntity** ppFarthestEntity, u32& foundDecalTypeFlag)
{
	const ScalarV minDecalSizeV(minDecalSize);
	ScalarV farthestDistSqr = ScalarV(V_ZERO);
	ScalarV rangeSqr = ScalarV(range*range);
	decalBucket* pFarthestBucket = NULL;
	decalInst* pFarthestInst = NULL;

	// check the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				// check the type is valid
				if (pCurrBucket->IsMarkedForDelete()==false && decalManager::IsFlaggedType(pCurrBucket->m_typeSettingsIndex, typeSettingsFlags))
				{
					decalInst* pCurrInst = pCurrBucket->m_instList.Peek();
					while (pCurrInst)
					{
						if (pCurrInst->IsMarkedForDelete()==false && (liquidType==-1 || liquidType==pCurrInst->m_liquidType)) 
						{
							// check the size is valid
							const ScalarV decalSize = pCurrInst->GetBoundRadius()*ScalarV(pCurrInst->CalcUVMult()*0.707f);
							if (IsGreaterThanOrEqualAll(decalSize, minDecalSizeV) && !pCurrInst->IsFadingOut())
							{
								// get the decal position
								Vec3V vDecalPosition = Transform(pCurrBucket->m_wldMtx, pCurrInst->GetPosition());

								// get distance to centre of decal
								ScalarV distSqr = MagSquared(vDecalPosition - vPos);

								// adjust to get distance to edge of decal
								distSqr = Max(ScalarV(V_ZERO), distSqr-(decalSize*decalSize));

								if (IsLessThanAll(distSqr, rangeSqr) && IsGreaterThanAll(distSqr, farthestDistSqr))
								{
									farthestDistSqr = distSqr;
									pFarthestBucket = pCurrBucket;
									pFarthestInst = pCurrInst;
									vFarthestPosWld = vDecalPosition;
									if (pCurrBucket->GetAttachEntity())
									{
										*ppFarthestEntity = pCurrBucket->GetAttachEntity();
									}

									foundDecalTypeFlag = 1<<pCurrBucket->m_typeSettingsIndex;
								}
							}
						}

						pCurrInst = pCurrInst->GetNext();
					}
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	// check if an inst in range has been found
	if (pFarthestInst)
	{
		if (removeDecal)
		{
			pFarthestBucket->RemoveInstSafe(pFarthestInst);
		}
		else if (fadeDecal)
		{
			pFarthestInst->StartFadingOut(fadeTime);
		}

		return true;
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////
//  FindFirstBucket
///////////////////////////////////////////////////////////////////////////////

decalBucket* decalManager::FindFirstBucket(u32 typeSettingsFlags, const fwEntity* pEntity)
{
	// check the buckets
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if (pCurrBucket->m_attachEntity==pEntity && 
					decalManager::IsFlaggedType(pCurrBucket->m_typeSettingsIndex, typeSettingsFlags) &&
					pCurrBucket->IsMarkedForDelete()==false)
				{
					return pCurrBucket;
				}

				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	return NULL;
}


///////////////////////////////////////////////////////////////////////////////
//  RegisterDecalRemovedCallback
///////////////////////////////////////////////////////////////////////////////

void decalManager::RegisterDecalRemovedCallback(DecalRemovedCallback *callback)
{
	m_decalRemovedCallback = callback;
}


#if DECAL_PROCESS_BREAKABLES

	///////////////////////////////////////////////////////////////////////////
	//  BreakableStateChangedCB
	///////////////////////////////////////////////////////////////////////////

	/*static*/ void decalManager::BreakableStateChangedCB(bgContactReport* pReport)
	{
	 	decalAssert(pReport);

		decalManager *const self = ms_instance;

		// set up the old and new matrix ids
		s16 oldMtxId = pReport->handle;
		s16 newMtxId = pReport->handle;
		if (pReport->firstContact)
		{
			oldMtxId = pReport->childIdx;
		}

		fwEntity* pBrokenEntity = static_cast<fwEntity*>(pReport->pFragInst->GetUserData());

		for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
		{
			for (int m=0; m<DECAL_NUM_METHODS; m++)
			{
				// buckets
				decalBucket* pCurrBucket = self->m_runningBucketLists[p][m].Peek();
				while (pCurrBucket)
				{
					pCurrBucket->ReApplyBreakable(pBrokenEntity, oldMtxId, newMtxId, (decalRenderPass)p);
					pCurrBucket = pCurrBucket->GetNext();
				}
			}
		}
	}

#endif // DECAL_PROCESS_BREAKABLES


///////////////////////////////////////////////////////////////////////////////
//  VehicleGlassStateChanged
///////////////////////////////////////////////////////////////////////////////

bool decalManager::VehicleGlassStateChanged(const fwEntity* pAttachEntity, void* pSmashGroup)
{
	bool bDecalsUpdated = false;
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			// buckets
			decalBucket* pCurrBucket = m_runningBucketLists[p][m].Peek();
			while (pCurrBucket)
			{
				if(pCurrBucket->ReApplyVehicleGlass(pAttachEntity, pSmashGroup, (decalRenderPass)p))
				{
					bDecalsUpdated = true;
				}
				pCurrBucket = pCurrBucket->GetNext();
			}
		}
	}

	return bDecalsUpdated;
}


///////////////////////////////////////////////////////////////////////////////
//  InitRenderStateBlocks
///////////////////////////////////////////////////////////////////////////////

void decalManager::InitRenderStateBlocks()
{
	// exit state blocks

	// rasterizer
	if( m_staticDecalExitRasterizerState ==  grcStateBlock::RS_Invalid )
	{
		m_staticDecalExitRasterizerState = grcStateBlock::RS_NoBackfaceCull;
	}
	
	if( m_dynamicDecalExitRasterizerState ==  grcStateBlock::RS_Invalid )
	{
		m_dynamicDecalExitRasterizerState = grcStateBlock::RS_NoBackfaceCull;
	}

	// blend
	grcBlendStateDesc defaultExitStateB;
	defaultExitStateB.BlendRTDesc[0].BlendEnable = 1;
	defaultExitStateB.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	defaultExitStateB.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;

	if( m_dynamicDecalExitBlendState ==  grcStateBlock::BS_Invalid )
	{
		m_dynamicDecalExitBlendState = grcStateBlock::CreateBlendState(defaultExitStateB);
	}
	
	if( m_staticDecalExitBlendState ==  grcStateBlock::BS_Invalid )
	{
		m_staticDecalExitBlendState = grcStateBlock::CreateBlendState(defaultExitStateB);
	}

	// depth/stencil
	grcDepthStencilStateDesc defaultExitStateDS;
	defaultExitStateDS.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	defaultExitStateDS.DepthWriteMask = 0;
	if( m_staticDecalExitDepthStencilState ==  grcStateBlock::DSS_Invalid )
	{
		m_staticDecalExitDepthStencilState = grcStateBlock::CreateDepthStencilState(defaultExitStateDS);
	}
	
	if( m_dynamicDecalExitDepthStencilState ==  grcStateBlock::DSS_Invalid )
	{
		m_dynamicDecalExitDepthStencilState = grcStateBlock::CreateDepthStencilState(defaultExitStateDS);
	}

	// render state blocks
	// rasterizer
#if __XENON
	// fix for z-fighting issues on 360 due to difference in depth buffer format 
	// and near clip values between consoles
	grcRasterizerStateDesc decalPassR;
	decalPassR.DepthBiasDX9 = 0.00001f;
	if( m_staticDecalRasterizerState ==  grcStateBlock::RS_Invalid )
	{
		m_staticDecalRasterizerState = grcStateBlock::CreateRasterizerState(decalPassR);
	}
#else
	if( m_staticDecalRasterizerState ==  grcStateBlock::RS_Invalid )
	{
		m_staticDecalRasterizerState = grcStateBlock::RS_Default;
	}
#endif

	if( m_dynamicDecalRasterizerState ==  grcStateBlock::RS_Invalid )
	{
		m_dynamicDecalRasterizerState = grcStateBlock::RS_NoBackfaceCull;
	}

	// blend
	grcBlendStateDesc renderStateB;
	renderStateB.BlendRTDesc[0].BlendEnable = 1;
	renderStateB.BlendRTDesc[0].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	renderStateB.BlendRTDesc[0].SrcBlend = grcRSV::BLEND_SRCALPHA;
	renderStateB.BlendRTDesc[0].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_RGB;
	renderStateB.BlendRTDesc[1].BlendEnable = 1;
	renderStateB.BlendRTDesc[1].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	renderStateB.BlendRTDesc[1].SrcBlend = grcRSV::BLEND_SRCALPHA;
	renderStateB.BlendRTDesc[1].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_RGB;
	renderStateB.BlendRTDesc[2].BlendEnable = 1;
	renderStateB.BlendRTDesc[2].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	renderStateB.BlendRTDesc[2].SrcBlend = grcRSV::BLEND_SRCALPHA;
	renderStateB.BlendRTDesc[2].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_RGB;
	renderStateB.BlendRTDesc[3].BlendEnable = 1;
	renderStateB.BlendRTDesc[3].DestBlend = grcRSV::BLEND_INVSRCALPHA;
	renderStateB.BlendRTDesc[3].SrcBlend = grcRSV::BLEND_SRCALPHA;
	renderStateB.BlendRTDesc[3].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_NONE;
	renderStateB.IndependentBlendEnable = 1;

	m_staticBlendStateDesc = renderStateB;

	if( m_staticDecalBlendState	==  grcStateBlock::BS_Invalid )
	{	
		m_staticDecalBlendState = grcStateBlock::CreateBlendState(renderStateB);
	}

	renderStateB.BlendRTDesc[3].RenderTargetWriteMask = grcRSV::COLORWRITEENABLE_BLUE;

	if( m_dynamicDecalBlendState ==  grcStateBlock::BS_Invalid )
	{
		m_dynamicDecalBlendState = grcStateBlock::CreateBlendState(renderStateB);
	}

	// depth/stencil
	grcDepthStencilStateDesc depthStencilBlockDesc;
	depthStencilBlockDesc.DepthFunc = rage::FixupDepthDirection(grcRSV::CMP_LESSEQUAL);
	depthStencilBlockDesc.DepthWriteMask = 0;

	if( m_staticDecalDepthStencilState[DECAL_RENDER_PASS_DEFERRED] ==  grcStateBlock::DSS_Invalid )
	{
		m_staticDecalDepthStencilState[DECAL_RENDER_PASS_DEFERRED] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	if( m_staticDecalDepthStencilState[DECAL_RENDER_PASS_FORWARD] ==  grcStateBlock::DSS_Invalid )
	{
		m_staticDecalDepthStencilState[DECAL_RENDER_PASS_FORWARD] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	if( m_staticDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_1] ==  grcStateBlock::DSS_Invalid )
	{
		m_staticDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_1] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	if( m_staticDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_2] ==  grcStateBlock::DSS_Invalid )
	{
		m_staticDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_2] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	
	if( m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_DEFERRED] ==  grcStateBlock::DSS_Invalid )
	{
		m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_DEFERRED] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	if( m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_FORWARD] ==  grcStateBlock::DSS_Invalid )
	{
		m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_FORWARD] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	if( m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_1] ==  grcStateBlock::DSS_Invalid )
	{
		m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_1] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
	if( m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_2] ==  grcStateBlock::DSS_Invalid )
	{
		m_dynamicDecalDepthStencilState[DECAL_RENDER_PASS_EXTRA_2] = grcStateBlock::CreateDepthStencilState(depthStencilBlockDesc);
	}
}


///////////////////////////////////////////////////////////////////////////////
//  PrepareStaticRenderPass
///////////////////////////////////////////////////////////////////////////////

void decalManager::PrepareStaticRenderPass(decalRenderPass renderPass)
{
	grcStateBlock::SetRasterizerState(m_staticDecalRasterizerState);
	grcBlendStateHandle staticDecalBlendState = grcStateBlock::BS_Invalid;
	if( m_overrideStaticBlendStateFunctor )
	{
		staticDecalBlendState = GetOverrideBlendStateFunctor() (renderPass);
	}

	if(staticDecalBlendState == grcStateBlock::BS_Invalid)
	{
		staticDecalBlendState = m_staticDecalBlendState;
	}

	grcStateBlock::SetBlendState(staticDecalBlendState);
	
	grcStateBlock::SetDepthStencilState(m_staticDecalDepthStencilState[renderPass], m_staticDecalStencilRef[renderPass]);
}


///////////////////////////////////////////////////////////////////////////////
//  FinishStaticRenderPass
///////////////////////////////////////////////////////////////////////////////

void decalManager::FinishStaticRenderPass()
{
	grcStateBlock::SetRasterizerState(m_staticDecalExitRasterizerState);
	grcStateBlock::SetBlendState(m_staticDecalExitBlendState);
	grcStateBlock::SetDepthStencilState(m_staticDecalExitDepthStencilState);
}


///////////////////////////////////////////////////////////////////////////////
//  PrepareDynamicRenderPass
///////////////////////////////////////////////////////////////////////////////

void decalManager::PrepareDynamicRenderPass(decalRenderPass renderPass)
{
	grcStateBlock::SetRasterizerState(m_dynamicDecalRasterizerState);
	grcStateBlock::SetBlendState(m_dynamicDecalBlendState);
	grcStateBlock::SetDepthStencilState(m_dynamicDecalDepthStencilState[renderPass], m_dynamicDecalStencilRef[renderPass]);
}


///////////////////////////////////////////////////////////////////////////////
//  FinishDynamicRenderPass
///////////////////////////////////////////////////////////////////////////////

void decalManager::FinishDynamicRenderPass()
{
	grcStateBlock::SetRasterizerState(m_dynamicDecalExitRasterizerState);
	grcStateBlock::SetBlendState(m_dynamicDecalExitBlendState);
	grcStateBlock::SetDepthStencilState(m_dynamicDecalExitDepthStencilState);
}

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
