//
// vfx/decal/decalrecycler.cpp
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalrecycler.h"

// includes (rage)
#include "grcore/im.h"

// includes (framework)
#include "fwsys/timer.h"
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decalmanager.h"
#include "vfx/decal/decalpools.h"
#include "vfx/decal/decalsettingsmanager.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces 
namespace rage {


// global variables
const float DECAL_RECYCLE_POOL_THRESH = 0.25f;
const float DECAL_RECYCLE_RANGE_RATIO_MAX = 4.0f;
const float DECAL_RECYCLE_RANGE_RATIO_MIN = 0.1f;
const float DECAL_RECYCLE_RANGE_RATIO_SPEED_MIN = 0.01f;
const float DECAL_RECYCLE_RANGE_RATIO_SPEED_MAX = 0.1f;
const float DECAL_RECYCLE_FADE_OUT_TIME_MIN = 0.0f;
const float DECAL_RECYCLE_FADE_OUT_TIME_MAX = 2.5f;

// static variables
float decalRecycler::ms_currRecycleRangeRatio = DECAL_RECYCLE_RANGE_RATIO_MAX;

// code
void decalRecycler::Update()
{
	RecycleUsingRangeRatio();
}

#if __BANK
void decalRecycler::RenderDebug(Vec3V_In vCamPos)
{
	// render the curr recycle range ratio
	grcColor(Color32(1.0f, 0.5f, 0.5f, 0.5f));
	grcDrawSphere(ms_currRecycleRangeRatio*30.0f, RCC_VECTOR3(vCamPos), 15, true, false);
}
#endif

// bool decalRecycler::ArePoolsUnderThresh(float thresh)
// {
// 	decalManager *const dm = &DECALMGR;
// 	int numFadingBuckets = 0;
// 	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
// 	{
// 		for (int m=0; m<DECAL_NUM_METHODS; m++)
// 		{
// 			numFadingBuckets += dm->m_statsFading.numBuckets[p][m];
// 		}
// 	}

// 	return (((dm->m_pools.m_bucketPool.GetNumItems() + numFadingBuckets) < (DECAL_MAX_BUCKETS * thresh)) || 
// 			((dm->m_pools.m_instPool.GetNumItems() + dm->m_statsFading.numInsts) < (DECAL_MAX_INSTS * thresh)) || 
// 			((dm->m_pools.m_triPool.GetNumItems() + dm->m_statsFading.numTris) < (DECAL_MAX_TRIS * thresh)) ||
// 			((dm->m_pools.m_vtxPool.GetNumItems() + dm->m_statsFading.numVerts) < (DECAL_MAX_VERTS * thresh)));
// }

float decalRecycler::GetMinPoolRatio()
{
	decalManager *const dm = &DECALMGR;

	int numFadingBuckets = 0;
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		for (int m=0; m<DECAL_NUM_METHODS; m++)
		{
			numFadingBuckets += dm->m_statsFading.numBuckets[p][m];
		}
	}

	float bucketPoolRatio = (dm->m_pools.m_bucketPool.GetNumItems() + numFadingBuckets) / (float)DECAL_MAX_BUCKETS;
	float instPoolRatio = (dm->m_pools.m_instPool.GetNumItems() + dm->m_statsFading.numInsts) / (float)DECAL_MAX_INSTS;
	float mibPoolRatio = (dm->m_pools.m_mibPool.GetNumItems()*decalMiniIdxBuf::MAX_NUM_TRIS + dm->m_statsFading.numTris) / (float)DECAL_MAX_TRIS;
	float vtxPoolRatio = (dm->m_pools.m_vtxPool.GetNumItems() + dm->m_statsFading.numVerts) / (float)DECAL_MAX_VERTS;

	return Min(bucketPoolRatio, instPoolRatio, mibPoolRatio, vtxPoolRatio);
}

void decalRecycler::RecycleUsingRangeRatio()
{
	// update the current recycle range ratio
	float minPoolsRatio = GetMinPoolRatio();
	if (minPoolsRatio<DECAL_RECYCLE_POOL_THRESH)
	{
		float poolsAlertLevel = 1.0f - (minPoolsRatio/DECAL_RECYCLE_POOL_THRESH);
		float recycleFadeOutTime = DECAL_RECYCLE_FADE_OUT_TIME_MAX + (poolsAlertLevel*(DECAL_RECYCLE_FADE_OUT_TIME_MIN-DECAL_RECYCLE_FADE_OUT_TIME_MAX));
		bool poolsUnderThresh = false;

		decalManager *const dm = &DECALMGR;

		// go through the buckets
		for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
		{
			for (int m=0; m<DECAL_NUM_METHODS; m++)
			{
				decalBucket* pDecalBucket = dm->m_runningBucketLists[p][m].Peek();
				while (pDecalBucket)
				{
					// only try to recycle non persistent buckets
					if (!pDecalBucket->IsPersistent() && pDecalBucket->IsMarkedForDelete() == false)
					{
						if (g_pDecalCallbacks->IsAllowedToRecycle(pDecalBucket->m_typeSettingsIndex))
						{
							// go through the insts in this bucket
							const float cullDistance = decalSettingsManager::GetTypeSettings(pDecalBucket->m_typeSettingsIndex)->cullDistance * g_pDecalCallbacks->GetDistanceMult(pDecalBucket->m_typeSettingsIndex);
							const float invCullDistance = 1.f / cullDistance;
							decalInst* pDecalInst = pDecalBucket->m_instList.Peek();
							while (pDecalInst)
							{
								// remove the inst if it is culled and over the current recycle range ratio
								const float rangeRatio = pDecalInst->m_distToViewport.GetFloat32_FromFloat16() * invCullDistance;
								if ((pDecalInst->IsMarkedForDelete() == false) && (rangeRatio>ms_currRecycleRangeRatio))
								{
									if (pDecalInst->IsCulled())
									{
										// this inst needs recycled 
										pDecalInst->MarkForDelete();
									}
									else
									{
										// this inst needs to start fading
										pDecalInst->StartFadingOut(recycleFadeOutTime);
									}

									// check if we can stop recycling
									if (GetMinPoolRatio()>DECAL_RECYCLE_POOL_THRESH)
									{
										poolsUnderThresh = true;
										break;
									}
								}

								pDecalInst = pDecalInst->GetNext();
							}
						}
					}

					pDecalBucket = pDecalBucket->GetNext();
				}
			}
		}
	
		// lower the recycle range ratio for the next update if we haven't succeeded in getting the pools under the threshold
		if (!poolsUnderThresh)
		{
			float recycleRangeRatioSpeed = DECAL_RECYCLE_RANGE_RATIO_SPEED_MIN + (poolsAlertLevel*(DECAL_RECYCLE_RANGE_RATIO_SPEED_MAX-DECAL_RECYCLE_RANGE_RATIO_SPEED_MIN));
			ms_currRecycleRangeRatio = Max(ms_currRecycleRangeRatio-recycleRangeRatioSpeed, DECAL_RECYCLE_RANGE_RATIO_MIN);
		}
	}
	else
	{
		// no recycling needed - increase the recycle range ratio for the next update
		ms_currRecycleRangeRatio = Min(ms_currRecycleRangeRatio+DECAL_RECYCLE_RANGE_RATIO_SPEED_MAX, DECAL_RECYCLE_RANGE_RATIO_MAX);
	}
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
