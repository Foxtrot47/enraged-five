//
// vfx/decal/decaldamagetaskdesc.h
//
// Copyright (C) 2013-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef VFX_DECALDAMAGETASKDESC_H
#define VFX_DECALDAMAGETASKDESC_H

#if !__TOOL && !__RESOURCECOMPILER

// includes
#include "vfx/decal/decalsettings.h"


namespace rage
{

class decalBucket;
class decalMiniIdxBuf;
class decalVtx;


struct decalDamageTaskDesc
{
	decalBucket            *buckets[DECAL_NUM_RENDER_PASSES];
	const decalMiniIdxBuf  *mibs;
	const decalVtx         *inputVertexArray;
	u32                    *outputVertexArray;
	u32                    *rsxWaitLabel;
	u32                     seqNum;
	float                   modelSpaceOffsetUnits;
}
;


}
// namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALDAMAGETASKDESC_H
