//
// vfx/decal/decalperinstcbuf.h
//
// Copyright (C) 2013-2013 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALPERINSTCBUF_H
#define	VFX_DECALPERINSTCBUF_H


#ifndef VFX_DECAL_IS_COMPILING_SHADER
#	error "VFX_DECAL_IS_COMPILING_SHADER must be defined to either 0 or 1 before including this header file"
#endif


#if !VFX_DECAL_IS_COMPILING_SHADER
#	include "vectormath/vectortypes.h"  // TODO: vectortypes.h currently MUST be included before v4vector4v.h.  This needs to be fixed.
#	include "vectormath/v4vector4v.h"
#	include "vectormath/vec2v.h"
#	include "vectormath/vec3v.h"
#	include "vectormath/vec4v.h"
#endif



#if !VFX_DECAL_IS_COMPILING_SHADER
namespace rage
{
#endif


#define VFX_DECAL_PER_INST_CBUF_SLOT            6


#if VFX_DECAL_IS_COMPILING_SHADER
#	define VFX_DECAL_FLOAT4                     float4
#else
#	define VFX_DECAL_FLOAT4                     Vec::Vector_4V
#endif

// These constants change very frequently (ie, once per decal instance), so keep
// as much other cruft out of this cbuffer as it reasonably possible to.
struct decalPerInstCBuf
{
	// Combine gAlphaConst into a single float4 to improve CPU side.
	//  (x,y)   gAlphaConst     constants used for calculating the output alpha (decoded.diffuse.a = gAlphaConst.x*fbRatio + gAlphaConst.y)
	//  (z,w)   unused
	VFX_DECAL_FLOAT4 constSet0;

	// Output color
	//  (x,y,z) gVertexColor    (decoded.diffuse.rgb = gVertexColor)
	//  (w)     unused
	VFX_DECAL_FLOAT4 vertexColor;

	// Two float2 constants used for calculating texcoord animation.  Similar to
	// gAlphaConst, these are combined into a single float4 for performance
	// reasons.
	VFX_DECAL_FLOAT4 texcoordConst;

#	if !VFX_DECAL_IS_COMPILING_SHADER
		inline decalPerInstCBuf() {}
		inline decalPerInstCBuf(Vec2V_In constSet0_, Vec3V_In vertexColor_, Vec4V_In texcoordConst_);
		inline void Invalidate();
		inline bool operator==(const decalPerInstCBuf &other) const;
		inline bool operator!=(const decalPerInstCBuf &other) const;
#	endif
};

#undef VFX_DECAL_FLOAT4



#if !VFX_DECAL_IS_COMPILING_SHADER

inline decalPerInstCBuf::decalPerInstCBuf(Vec2V_In constSet0_, Vec3V_In vertexColor_, Vec4V_In texcoordConst_)
{
	constSet0     = constSet0_.GetIntrin128();
	vertexColor   = vertexColor_.GetIntrin128();
	texcoordConst = texcoordConst_.GetIntrin128();
}

inline void decalPerInstCBuf::Invalidate()
{
	using namespace Vec;
	Vector_4V allset = V4VConstant(V_TRUE);
	constSet0     = allset;
	vertexColor   = allset;
	texcoordConst = allset;
}

inline bool decalPerInstCBuf::operator==(const decalPerInstCBuf &other) const
{
	using namespace Vec;
	return V2IsEqualIntAll(constSet0,     other.constSet0)
		&& V3IsEqualIntAll(vertexColor,   other.vertexColor)
		&& V4IsEqualIntAll(texcoordConst, other.texcoordConst);
}

inline bool decalPerInstCBuf::operator!=(const decalPerInstCBuf &other) const
{
	return !operator==(other);
}

} // namespace rage

#endif


#endif // VFX_DECALPERINSTCBUF_H
