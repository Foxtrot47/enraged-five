//
// vfx/decal/decalsettings.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALSETTINGS_H
#define	VFX_DECALSETTINGS_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "grcore/effect.h"
#include "phcore/materialmgr.h"
#include "system/floattoint.h"
#include "system/memops.h"
#include "vectormath/classes.h"

// includes (framework)
#include "fwscene/world/InteriorLocation.h"
#include "vfx/channel.h"
#include "vfx/decal/decalclipper.h"


// namespaces 
namespace rage {


// forward declarations	(rage)									
class grcTexture;

// forward declarations	(framework)	
class decalInst;	


// defines											
#define DECAL_USE_INDEXED_VERTS			1 && (__XENON)

#define DECAL_MAX_DRAWABLES             (2)

#if __FINAL || __PROFILE
#	define DECAL_DEBUG_NAME				0
#else
#	define DECAL_DEBUG_NAME				1
#endif

#if DECAL_DEBUG_NAME
#	define DECAL_DEBUG_NAME_ONLY(...)	__VA_ARGS__
#else
#	define DECAL_DEBUG_NAME_ONLY(...)
#endif

#if DECAL_USE_INDEXED_VERTS
#define	DECAL_NUM_DYNAMIC_INST_VERTS	(8)										// number of verts to render on a dynamic decal box
#define DECAL_NUM_DYNAMIC_INST_IDX		(36)
#define DECAL_NUM_DYNAMIC_INST_IDX_HALF	(DECAL_NUM_DYNAMIC_INST_IDX/2)
#else
#define	DECAL_NUM_DYNAMIC_INST_VERTS	(20)									// number of verts to render on a dynamic decal box
#endif	

#define DECAL_MAX_LIFE_TIME				(60.0f*60.0f*24.0f)
#define DECAL_FADE_OUT_TIME				(5.0f)

#define DECAL_PROCESS_BREAKABLES		(1)
#if DECAL_PROCESS_BREAKABLES
#define DECAL_BREAKABLE_ONLY(x)			x
#else
#define DECAL_BREAKABLE_ONLY(x)
#endif


// enumerations
enum decalMethod
{
	DECAL_METHOD_DYNAMIC				= 0,
	DECAL_METHOD_STATIC,

	DECAL_NUM_METHODS
};

enum decalRenderPass
{
	DECAL_RENDER_PASS_EXTRA_1 = 0,
	DECAL_RENDER_PASS_EXTRA_2,

	DECAL_RENDER_PASS_DEFERRED,

	DECAL_RENDER_PASS_FORWARD,

	DECAL_NUM_RENDER_PASSES
};

enum decalAttachType
{
	// no attach (world space)
	DECAL_ATTACH_TYPE_NONE				= 0,

	// components
	DECAL_ATTACH_TYPE_COMPONENT,

	// bones
	DECAL_ATTACH_TYPE_BONE,
	DECAL_ATTACH_TYPE_BONE_SKINNED,

	// smash groups
	DECAL_ATTACH_TYPE_SMASHGROUP

#if DECAL_PROCESS_BREAKABLES
	// breakable
	,DECAL_ATTACH_TYPE_BREAKABLE_BROKEN,
	DECAL_ATTACH_TYPE_BREAKABLE_UNBROKEN
#endif
};


// structures
struct decalTypeSettings
{	
	float						depth;
	float						zShiftOffset;
	float						polyRejectThreshold;
	float						lodDistance;
	float						fadeDistance;
	float						cullDistance;
	bool						canMergeInsts;
	bool						wrapUV;
	bool						useAniso;
	bool						isPersistent;
	u32							collisionFlags;
};

struct decalRenderSettingsInfo
{
	const char*					pFilename;
};

struct decalRenderSettings
{	
	u32							id;
	grcTextureHandle			pDiffuseMap;
	grcTextureHandle			pNormalMap;
	grcTextureHandle			pSpecularMap;										
	float						parallaxScale;	
	float						specFalloff;										
	float						specIntensity;									
	float						specFresnel;
	float						emissiveness;
	float						wrapLength;
	float						animTime;
	float						sizeMult;
	u8							texRows;
	u8							texColumns;
	u8							texIdA;
	u8							texIdB;
	u8							shaderId;	
	bool						isWashable;
	bool						createUnderwater;
	bool						fadeUnderwater;
	bool						useAniso;
};

struct decalBucketSettings
{
	// Disabling the ctor in spu builds since it is unnecessary and uses a lot of
	// local store.
	SPU_ONLY(private:)
		decalBucketSettings();
	SPU_ONLY(public:)

		inline decalBucketSettings &operator=(const decalBucketSettings &rhs)
	{
		sysMemCpy(this, &rhs, sizeof(*this));
		return *this;
	}

	void*						pSmashGroup;						// pointer to a smash group that the decal should be applied to
	u16							typeSettingsIndex;					// index into the type settings array
	u16							renderSettingsIndex;				// index into the render settings array
	u16							subType;							// sub type of the main type - used for different things depending on the type
	s16							roomId;								// room id in which the decal is applied, comes from collision

	u8							isScripted:1;						// whether this decal has been created by script

};

struct decalInstSettings
{
// Disabling the ctor in spu builds since it is unnecessary and uses a lot of
// local store.
SPU_ONLY(private:)
	decalInstSettings();
SPU_ONLY(public:)

	inline decalInstSettings &operator=(const decalInstSettings &rhs)
	{
		sysMemCpy(this, &rhs, sizeof(*this));
		return *this;
	}

	Vec3V						vPosition;							// world space position of the decal - a local space version of this is calculated and stored on the inst
	Vec3V						vDirection;							// world space direction vector of the decal - a local space version of this is calculated and stored on the inst
	Vec3V						vSide;								// world space side vector of the decal - a local space version of this is calculated and stored on the inst
	Vec3V						vDimensions;						// the dimensions of the decal projection box

	float						currWrapLength;						// the current wrap length of the decal - used on trails etc where the decal texture coords are spaced over a set distance 
	float						totalLife;							// the total life that the decal lives
	float						fadeInTime;							// time that the decal takes to fade in
	float						fadeOutTime;						// time that the decal takes to fade out

	float						uvMultStart;						// the uv multiplier at the start of the decal life
	float						uvMultEnd;							// the uv multiplier at the end of the uv animation time
	float						uvMultTime;							// the time it takes for the uv mutliplier to animate from start to end

	u8							colR;								// the red colour component of the decal
	u8							colG;								// the green colour component of the decal
	u8							colB;								// the blue colour component of the decal
	u8							alphaFront;							// the alpha at the front of the decal
	u8							alphaBack;							// the alpha at the back of the decal
	s8							liquidType;							// specifies if this decal is a certain liquid type

	u8							isLiquidPool:1;						// specifies if this decal is a liquid pool
	u8							flipU:1;							// whether to flip the u tex coord
	u8							flipV:1;							// whether to flip the v tex coord
	u8							singleFrame:1;						// whether this decal only lasts for a single frame

};

struct decalPipelineSettings
{
	// Disabling the ctor in spu builds since it is unnecessary and uses a lot of
	// local store.
SPU_ONLY(private:)
	decalPipelineSettings();
SPU_ONLY(public:)

	inline decalPipelineSettings &operator=(const decalPipelineSettings &rhs)
	{
		sysMemCpy(this, &rhs, sizeof(*this));
		return *this;
	}

	Vec3V						vPassedJoinVerts[4];				// the verts to join this decal onto
	Vec3V						vReturnedJoinVerts[4];				// the verts that the next decal in the trail needs to join to 

	decalRenderPass				renderPass;							// the pass that this decal gets rendered (deferred, forward etc) 
	decalMethod					method;								// the method used to apply the decal (static or dynamic)

	float						duplicateRejectDist;				// the distance where this decal gets rejected if a similar one is found within range
	float						maxOverlayRadius;					// the maximum size that a decal can overlay to
	phMaterialMgr::Id			exclusiveMtlId;						// material id that this decal should only be applied to 
	s16							colnComponentId;					// the component id of the collision that caused the decal

	u8							hasJoinVerts:1;						// whether this decal has join verts
	u8							useColnEntityOnly:1;				// whether we should only be applying this decal to the collision entity
	u8							onlyApplyToGlass:1;					// whether this decal should only be applied to glass polys (using either the material of collision polys or the shader type of drawable geometry)
	bool						dontApplyToGlass:1;					// whether this decal shouldn't be applied to glass polys
	bool						isVehicleGlass:1;					// whether this is a glass decal on a vehicle
#if DECAL_PROCESS_BREAKABLES
	bool						applyToBreakables:1;				// whether this decal gets applied to breakables
#endif

};

struct decalLodSettings
{
	// Disabling the ctor in spu builds since it is unnecessary and uses a lot of
	// local store.
	SPU_ONLY(private:)
		decalLodSettings();
	SPU_ONLY(public:)

	inline decalLodSettings &operator=(const decalLodSettings &rhs)
	{
		sysMemCpy(this, &rhs, sizeof(*this));
		return *this;
	}

	float						currLife;							// the current life of the decal inst that is lodding
	int							lodAttachMatrixId;					// the matrix id that the lodded decal is attached to (so when it turn back to a static decal it only gets applied to this component/bone)
	u8                          copyPrimaryInstFlag:1;				// whether the primary inst needs copied
	u8                          primaryInstFlag:1;					// whether this decal inst is the primary one (so when it turns into a dynamic decal only this one will be used)

};


struct decalUserSettings
{
	decalInstSettings			instSettings;
	decalBucketSettings			bucketSettings;
	decalPipelineSettings		pipelineSettings;
	decalLodSettings			lodSettings;

};

struct decalInternalSettings
{
	// global data
	Vec3V						vForward;
	decalClipper				clipper;

	// local data
	Mat34V						parentMtx;
	Mat34V						invParentMtx;
	decalClipper				lclClipper;
	Vec3V						vLclPosition;
	Vec3V						vLclDirection;
	Vec3V						vLclSide;
	Vec3V						vLclForward;

	// other data
	decalInst*					pInstGlass;
	decalInst*					pInstNonGlass;
	decalAttachType				attachType;
	decalJoinType				joinType;

	//global data
	int							id;

	// other data
	fwInteriorLocation			interiorLocation;
	bool						isPersistent;
	bool						doUnderwaterCheck;
};

struct decalStats
{
	void Reset() {for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++) for (int m=0; m<DECAL_NUM_METHODS; m++) numBuckets[p][m] = 0; numInsts = 0; numTris = 0; numVerts = 0;}
	int numBuckets[DECAL_NUM_RENDER_PASSES][DECAL_NUM_METHODS];
	int numInsts;
	int numTris;
	int numVerts;
};


// classes
class decalSettings
{
#	if !__SPU
		friend class decalManager;
		friend class decalManagerAsync;
		friend class decalBucket;
		friend class decalInst;
#	endif

	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		static inline void ValidateMatrix(Mat34V_In ASSERT_ONLY(mat))
		{
#			if __ASSERT
				const ScalarV angTolerance(Vec::V4VConstantSplat<FLOAT_TO_INT(0.2588f)>()); // cos(75 deg) = 0.2588
				decalAssert(mat.IsOrthogonal3x3(angTolerance));
				decalAssert(IsFiniteAll(mat.GetCol3()));
#			endif
		}

		// internal settings calcs
		void CalcGlobalInternalSettings(int id);
		void CalcLocalInternalSettings(Mat34V_In wldMtx, decalInst* pInstGlass, decalInst* pInstNonGlass, decalAttachType attachType);

		Mat34V_Out GetDecalMatrix() const;


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

		decalUserSettings user;

#if !__SPU
	private:
#endif

		decalInternalSettings internal;

#if __SPU
	// We don't really need the constructor in SPU code, and getting rid of it
	// saves over 1KiB of local store.  So make it private so that it isn't
	// accidentally used.
	private:
		decalSettings();
#endif
};


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALSETTINGS_H
