//
// vfx/decal/decalsettings.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

// includes
#include "decalsettings.h"

// includes (rage)

// includes (framework)
#include "vfx/channel.h"
#include "vfx/vfxutil.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decalhelper.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


// code

#if !__SPU
	decalInstSettings::decalInstSettings()
	{
		vPosition = Vec3V(V_ZERO);
		vDirection = Vec3V(V_ZERO);
		vSide = Vec3V(V_ZERO);
		vDimensions = Vec3V(V_ZERO);

		currWrapLength = -1.0f;
		totalLife = -1.0f;
		fadeInTime = 0.0f;
		fadeOutTime = DECAL_FADE_OUT_TIME;

		uvMultStart = 1.0f;
		uvMultEnd = 1.0f;
		uvMultTime = 0.0f;

		colR = 255;
		colG = 255;
		colB = 255;
		alphaFront = 255;
		alphaBack = 255;
		liquidType = -1;

		isLiquidPool = false;
		flipU = false;
		flipV = false;
		singleFrame = false;
	}
#endif // !__SPU

#if !__SPU
	decalBucketSettings::decalBucketSettings()
	{
		pSmashGroup = NULL;
		typeSettingsIndex = 0;
		renderSettingsIndex = 0;
		subType = 0;
		roomId = INTLOC_INVALID_INDEX;

		isScripted = false;
	}
#endif // !__SPU

#if !__SPU
	decalPipelineSettings::decalPipelineSettings()
	{
		vPassedJoinVerts[0] = Vec3V(V_ZERO);
		vReturnedJoinVerts[0] = Vec3V(V_ZERO);

		renderPass = DECAL_RENDER_PASS_DEFERRED;
		method = DECAL_METHOD_DYNAMIC;

		duplicateRejectDist = 0.0f;
		maxOverlayRadius = 0.0f;
		exclusiveMtlId = (phMaterialMgr::Id)(-1);
		colnComponentId = -1;

		hasJoinVerts = false;
		useColnEntityOnly = false;
		onlyApplyToGlass = false;
		dontApplyToGlass = false;
		isVehicleGlass = false;
#	if DECAL_PROCESS_BREAKABLES
		applyToBreakables = false;
#	endif
	}
#endif // !__SPU

#if !__SPU
	decalLodSettings::decalLodSettings()
	{
		currLife = 0.0f;
		lodAttachMatrixId = -1;
		copyPrimaryInstFlag = false;
		primaryInstFlag = false;
	}
#endif // !__SPU

void decalSettings::CalcGlobalInternalSettings(int id)
{
	// set the id
	internal.id = id;

	// calc the forward vector (the cross product of the two provided vectors)
	internal.vForward = Cross(user.instSettings.vSide, user.instSettings.vDirection);

	// static decals need to calculate clip planes
	if (user.pipelineSettings.method==DECAL_METHOD_STATIC)
	{
		// init the clipper
		internal.clipper.Init(user.instSettings.vPosition, user.instSettings.vDirection, user.instSettings.vSide, internal.vForward, user.instSettings.vDimensions, user.pipelineSettings.vPassedJoinVerts, user.pipelineSettings.vReturnedJoinVerts, internal.joinType);
	}
}

void decalSettings::CalcLocalInternalSettings(Mat34V_In wldMtx, decalInst* pInstGlass, decalInst* pInstNonGlass, decalAttachType attachType)
{
	ValidateMatrix(wldMtx);

	internal.parentMtx = wldMtx;
	InvertTransformOrtho(internal.invParentMtx, wldMtx);

	internal.vLclPosition = Transform(internal.invParentMtx, user.instSettings.vPosition);
	internal.vLclDirection = Transform3x3(internal.invParentMtx, user.instSettings.vDirection);
	internal.vLclSide = Transform3x3(internal.invParentMtx, user.instSettings.vSide);
	internal.vLclForward = Transform3x3(internal.invParentMtx, internal.vForward);

	// static decals need to calculate local clip planes
	if (user.pipelineSettings.method==DECAL_METHOD_STATIC)
	{
		for (int i=0; i<DECAL_NUM_PLANES; i++)
		{
			decalPlaneId planeId = (decalPlaneId)i;

			// store the old clip normal
			Vec4V_ConstRef vOldPlaneEq = internal.clipper.GetPlaneEquation(planeId);
			Vec3V vOldPlaneNormal = vOldPlaneEq.GetXYZ();
			float oldPlaneD = vOldPlaneEq.GetWf();

			// calc the new clip normal
			Vec3V vNewPlaneNormal = Transform3x3(internal.invParentMtx, vOldPlaneNormal);

			float dot = Dot(vNewPlaneNormal, internal.invParentMtx.GetCol3()).Getf();

			internal.lclClipper.SetPlaneEquation(planeId, vNewPlaneNormal, oldPlaneD+dot);
		}
	}

	internal.pInstGlass = pInstGlass;
	internal.pInstNonGlass = pInstNonGlass;
	internal.attachType = attachType;
}

Mat34V_Out decalSettings::GetDecalMatrix() const
{
	Mat34V decalMtx;
	decalMtx.SetCol0(user.instSettings.vSide);
	decalMtx.SetCol1(internal.vForward);
	decalMtx.SetCol2(user.instSettings.vDirection);
	decalMtx.SetCol3(user.instSettings.vPosition);

	return decalMtx;
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
