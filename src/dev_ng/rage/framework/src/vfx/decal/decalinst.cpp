//
// vfx/decal/decalinst.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalinst.h"

// includes (rage)
#include "grcore/im.h"
#include "grcore/debugdraw.h"
#include "grcore/viewport.h"
#include "grcore/wrapper_d3d.h"
#include "math/amath.h"
#include "math/random.h"
#include "system/floattoint.h"
#include "system/nelem.h"
#include "system/typeinfo.h"
#include "vector/color32.h"
#include "vectormath/mathops.h"

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decalclipper.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decaldmatags.h"
#include "vfx/decal/decalmanagerasync.h"
#include "vfx/decal/decalminiidxbuf.h"
#include "vfx/decal/decalminiidxbufwriter.h"
#include "vfx/decal/decalpools.h"
#if !__SPU
#	include "vfx/vfxwidget.h"
#	include "vfx/decal/decalmanager.h"
#	include "vfx/decal/decalmanagerasync.h"
#	include "vfx/decal/decalrenderstaticbatcher.h"
#	include "vfx/decal/decalsettingsmanager.h"
#	include "vfx/decal/decalshader.h"
#endif

#if __PPU
#	include <cell/gcm/gcm_method_data.h>
#elif RSG_ORBIS
#	include "grcore/wrapper_gnm.h"
#	include "grcore/gnmx.h"
#endif


#include <algorithm>


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


///////////////////////////////////////////////////////////////////////////////
//  CODE
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  Init
///////////////////////////////////////////////////////////////////////////////

void decalInst::Init(const decalSettings& settings)
{
	using namespace Vec;

	const decalInstSettings& instSettings = settings.user.instSettings;
	const decalLodSettings& lodSettings = settings.user.lodSettings;
	const decalInternalSettings& internalSettings = settings.internal;

	// init the settings
	m_id = internalSettings.id;

	Vec3V position = Transform(internalSettings.invParentMtx, instSettings.vPosition);
	
	CompileTimeAssert(OffsetOf(decalInst,m_directionY) == OffsetOf(decalInst,m_directionX)+2);
	CompileTimeAssert(OffsetOf(decalInst,m_directionZ) == OffsetOf(decalInst,m_directionX)+4);
	CompileTimeAssert(OffsetOf(decalInst,m_sideY) == OffsetOf(decalInst,m_sideX)+2);
	CompileTimeAssert(OffsetOf(decalInst,m_sideZ) == OffsetOf(decalInst,m_sideX)+4);
	CompileTimeAssert(OffsetOf(decalInst,m_sideX) > OffsetOf(decalInst,m_directionX));
	Vector_4V vdir = V4LoadUnaligned(&m_directionX);
	Vector_4V vsid = V4LoadUnaligned(&m_sideX);
#	if __BE
		Vector_4V mask = V4VConstant<0xffffffff,0xffff0000,0x00000000,0x00000000>();
#	else
		Vector_4V mask = V4VConstant<0xffffffff,0x0000ffff,0x00000000,0x00000000>();
#	endif
	Vector_4V d = Transform3x3(internalSettings.invParentMtx, instSettings.vDirection).GetIntrin128();
	d = V4FloatToIntRaw<15>(d);
	d = V4PackSignedIntToSignedShort(d, d);
	vdir = V4SelectFT(mask, vdir, d);
	V4StoreUnaligned(&m_directionX, vdir);
	Vector_4V s = Transform3x3(internalSettings.invParentMtx, instSettings.vSide).GetIntrin128();
	s = V4FloatToIntRaw<15>(s);
	s = V4PackSignedIntToSignedShort(s, s);
	vsid = V4SelectFT(mask, vsid, s);
	V4StoreUnaligned(&m_sideX, vsid);

	Vector_4V vDimensionsF32 = instSettings.vDimensions.GetIntrin128();
	Vector_4V vDimensionsF16 = V4Float16Vec8Pack(vDimensionsF32, vDimensionsF32);
	CompileTimeAssert((OffsetOf(decalInst, m_dimensionsX) & 3) == 0);
	V4Store32<0>(&m_dimensionsX, vDimensionsF16);
	V4Store16<2>(&m_dimensionsZ, vDimensionsF16);

	m_currWrapLength = instSettings.currWrapLength;

	// only required if trail decals no longer merge insts causing them to go through the lod system
//	m_vPassedJoinVerts[0] = instSettings.vPassedJoinVerts[0];
//	m_vPassedJoinVerts[1] = instSettings.vPassedJoinVerts[1];
//	m_vPassedJoinVerts[2] = instSettings.vPassedJoinVerts[2];
//	m_vPassedJoinVerts[3] = instSettings.vPassedJoinVerts[3];

	SetBoundCentre(position);
	SetBoundRadius(decalHelper::GetRadius(instSettings.vDimensions));
	m_totalLife = instSettings.totalLife;
	SetFadeInTime(instSettings.fadeInTime);
	SetFadeOutTime(instSettings.fadeOutTime);

	CompileTimeAssert((OffsetOf(decalInstSettings, uvMultStart) & 15) == 0);
	CompileTimeAssert(OffsetOf(decalInstSettings, uvMultEnd)  == OffsetOf(decalInstSettings, uvMultStart) + 4);
	CompileTimeAssert(OffsetOf(decalInstSettings, uvMultTime) == OffsetOf(decalInstSettings, uvMultStart) + 8);
// 	typedef Vector_4V MAY_ALIAS Vector_4V_Alias;
	Vector_4V uvMultF32 = *(Vector_4V/*_Alias*/*)&instSettings.uvMultStart;
	Vector_4V uvMultF16 = V4Float16Vec8Pack(uvMultF32, uvMultF32);
	CompileTimeAssert((OffsetOf(decalInst, m_uvMultStart) & 3) == 0);
	CompileTimeAssert(OffsetOf(decalInst, m_uvMultEnd)  == OffsetOf(decalInst, m_uvMultStart) + 2);
	CompileTimeAssert(OffsetOf(decalInst, m_uvMultTime) == OffsetOf(decalInst, m_uvMultStart) + 4);
	V4Store32<0>(&m_uvMultStart, uvMultF16);
	V4Store16<2>(&m_uvMultTime,  uvMultF16);

	m_liquidType = instSettings.liquidType;
	m_isLiquidPool = instSettings.isLiquidPool;
	m_flipU = instSettings.flipU;
	m_flipV = instSettings.flipV;
	m_singleFrame = instSettings.singleFrame;
	m_colR = instSettings.colR;
	m_colG = instSettings.colG;
	m_colB = instSettings.colB;
	m_alphaFront = instSettings.alphaFront;
	m_alphaBack = instSettings.alphaBack;

	m_positionOffsetX.SetFloat16_Zero();
	m_positionOffsetY.SetFloat16_Zero();
	m_positionOffsetZ.SetFloat16_Zero();

	if (m_totalLife<0.0f)
	{
		m_totalLife = DECAL_MAX_LIFE_TIME;
	}

	// init the state
	m_currLife = lodSettings.currLife;
	m_distToViewport.SetFloat16_Zero();
	SetWashLevel(g_DrawRand.GetRanged(1.f, DECAL_INST_WASH_MAX_VALUE));
	m_isDistanceCulled = false;
	m_isViewportCulled = false;
	m_doUnderwaterCheck = internalSettings.doUnderwaterCheck;
	m_isLod = false;

#	if __BANK
		m_joinType = internalSettings.joinType;
#	endif

	m_markedForDelete = 0;

	m_idxBufList = decalMiniIdxBuf::NULL_IDX;
	m_vtxList = 0xffff;
	m_numTris = 0;
	m_numVtxs = 0;

	SetNext(NULL);
}


#if !__SPU

	///////////////////////////////////////////////////////////////////////////
	//  InitForLodReduction
	///////////////////////////////////////////////////////////////////////////

	void decalInst::InitForLodReduction(const decalInst *pOther)
	{
		sysMemCpy(this, pOther, sizeof(*this));
		SetNext(NULL);
		m_idxBufList = decalMiniIdxBuf::NULL_IDX;
		m_vtxList = 0xffff;
		m_numTris = 0;
		m_numVtxs = 0;
		m_isLod = true;
	}

#endif // !__SPU


///////////////////////////////////////////////////////////////////////////////
//  ShutdownImmediate
///////////////////////////////////////////////////////////////////////////////

void decalInst::ShutdownImmediate(ShutdownImmediateReleaseDecalId releaseDecalId)
{
	decalManagerAsync *const dm = &DECALMGRASYNC;
	decalAssertf(IsMarkedForDelete(), "decalInst has not been previously marked for delete");
	m_markedForDelete = 0;

	// Shutdown the vertices.
	u16 *const allocatedVertices = dm->m_allocatedVertices;
	u16 vtxIdx = m_vtxList;
	ASSERT_ONLY(unsigned totalNumFreedVtxs = 0;)
	while (vtxIdx != 0xffff)
	{
		dm->m_pools.ReturnVtx(vtxIdx);
		const u16 nextVtxIdx = allocatedVertices[vtxIdx];
		allocatedVertices[vtxIdx] = 0xffff;
		vtxIdx = nextVtxIdx;
		ASSERT_ONLY(++totalNumFreedVtxs;)
	}
	decalAssert(totalNumFreedVtxs == m_numVtxs);
	m_numVtxs = 0;

	// Shutdown the mini index buffers.
	decalMiniIdxBuf *const pMibBase = dm->m_pools.GetMibPoolBase();
	decalMiniIdxBuf *pMib = decalMiniIdxBuf::GetPtr(pMibBase, m_idxBufList);
	ASSERT_ONLY(unsigned totalNumFreedTris = 0;)
	while (pMib)
	{
		decalMiniIdxBuf *const pMibNext = pMib->GetNext(pMibBase);
		ASSERT_ONLY(totalNumFreedTris += pMib->GetNumTris());
		dm->m_pools.ReturnMib(pMib);
		pMib = pMibNext;
	}
	decalAssert(totalNumFreedTris == m_numTris);
	m_numTris = 0;

	if (releaseDecalId == SHUTDOWN_IMMEDIATE_RELEASE_DECAL_ID)
	{
		dm->RemoveDecalIdInstance(m_id);
	}
}


///////////////////////////////////////////////////////////////////////////////
//  ReserveTris
///////////////////////////////////////////////////////////////////////////////

bool decalInst::ReserveTris(unsigned count, decalMiniIdxBufWriter *miniIdxBufWriter)
{
	decalAssert(count-1 < (DECAL_MAX_CLIPPED_VERTS-2)*2);

	// Find how much space is left in the current buffer ?
	unsigned spaceInCurr = 0;
	const u16 headIdx = m_idxBufList;
	decalMiniIdxBuf *const mibBase = DECALMGRASYNC.m_pools.GetMibPoolBase();
	decalMiniIdxBuf *const headEa = decalMiniIdxBuf::GetPtr(mibBase, headIdx);
	if (Likely(headEa))
	{
		decalMiniIdxBuf *const headLs = miniIdxBufWriter->LocalPtrLoad(headEa);
		spaceInCurr = decalMiniIdxBuf::MAX_NUM_TRIS - headLs->GetNumTris();
		if (Likely(spaceInCurr >= count))
		{
			return true;
		}
	}

	// Need to reserve some more buffers
	const unsigned numBufs = (count - spaceInCurr + decalMiniIdxBuf::MAX_NUM_TRIS - 1) / decalMiniIdxBuf::MAX_NUM_TRIS;
	return miniIdxBufWriter->ReserveBufs(numBufs);
}


///////////////////////////////////////////////////////////////////////////////
//  AddTri
///////////////////////////////////////////////////////////////////////////////

u16 *decalInst::AddTri(decalMiniIdxBufWriter *miniIdxBufWriter)
{
	++m_numTris;

	// Space in current buffer ?
	const u16 headIdx = m_idxBufList;
	decalMiniIdxBuf *const mibBase = DECALMGRASYNC.m_pools.GetMibPoolBase();
	decalMiniIdxBuf *const headEa = decalMiniIdxBuf::GetPtr(mibBase, headIdx);
	if (Likely(headEa))
	{
		decalMiniIdxBuf *const headLs = miniIdxBufWriter->LocalPtrLoad(headEa);
		if (Likely(!headLs->IsFull()))
		{
			return headLs->AddTri();
		}
	}

	// Allocate a new buffer and link it into the list
	decalMiniIdxBuf *const newHeadEa = miniIdxBufWriter->AllocBuf();
	decalMiniIdxBuf *const newHeadLs = miniIdxBufWriter->LocalPtrInit(newHeadEa);
	newHeadLs->SetNext(headIdx);
	m_idxBufList = (u16)(newHeadEa - mibBase);
	return newHeadLs->AddTri();
}


#if !__SPU

	///////////////////////////////////////////////////////////////////////////
	//  Update
	///////////////////////////////////////////////////////////////////////////

	decalInst::decalState decalInst::Update(float deltaTime, const decalBucket* pBucket, float animTime, const grcViewport &pBucketVP, 
		Vec3V_In camPosBucketSpace, ScalarV_In fovScaleSqr, ScalarV_In cullDistance, bool bPerformUnderwaterTest, bool bAllowedToRecycle, bool bIsWashable, bool bValidVP)
	{
		//keeping local copy to avoid LHS
		float currLife = m_currLife;
		const float totalLife = m_totalLife;
		const float fadeOutTime = GetFadeOutTime();

		if (bPerformUnderwaterTest && IsPerformUnderWaterCheck() && !IsFadingOut(fadeOutTime) && !IsMarkedForDelete())
		{
			Vec3V vInstPosWld = Transform(pBucket->m_wldMtx, GetPosition());
			if (g_pDecalCallbacks->IsUnderwater(vInstPosWld))
			{
				//equivalent to calling StartFadeOutTime without the condition check
				currLife = m_totalLife - fadeOutTime;
			}
		}

		if (m_singleFrame && currLife>0.0f)
		{
			return DECAL_STATE_DEAD;
		}


		// update the life of this inst
		currLife += deltaTime;

		// pause the time of persistent insts once they've faded in or after having completed their animation
		if (pBucket->IsPersistent() && !IsFadingOut(fadeOutTime, currLife, totalLife))
		{
			const float persistentInstMaxTime = Max(GetFadeInTime(), animTime);
			if(currLife>persistentInstMaxTime)
			{
				currLife = persistentInstMaxTime;
			}

		}

		// return if expired
		//TODO: convert m_totalLife>-1.0f to integer comparison
		if (totalLife>-1.0f && currLife>totalLife)
		{
			return DECAL_STATE_DEAD;
		}

		// return if washed off
		if (!m_washLevel)
		{
			return DECAL_STATE_DEAD;
		}

		// calc the distance to the viewport
		decalAssertf(IsFiniteAll(GetBoundCentre()), "decal inst bound centre isn't finite");
		
		// get the vector from the camera to the test position
		Vec3V vCamToPos = GetBoundCentre() - camPosBucketSpace;

		// return the sqr distance
		const ScalarV distToViewportSqr = MagSquared(vCamToPos) * fovScaleSqr;		
		
		const ScalarV distToViewport = SqrtFast(distToViewportSqr);
		m_distToViewport.SetFloat16_FromFloat32(distToViewport);

		// set the culled states
		if (IsLessThanAll(distToViewport, cullDistance))
		{
			m_isDistanceCulled = false;
			m_isViewportCulled = !bValidVP || !pBucketVP.IsSphereVisible(GetBoundCentreAndRadius());
		}
		else
		{
			m_isDistanceCulled = true;
			if (bAllowedToRecycle)
			{
				// remove any distance culled washable decals that have been around for more than 60 seconds 
				if (bIsWashable && currLife>60.0f)
				{
					return DECAL_STATE_DEAD;
				}
			}
		}

		m_currLife = currLife;
		decalManager *const dm = &DECALMGR;

		if (IsFadingOut(fadeOutTime, currLife, totalLife))
		{
			dm->m_statsFading.numInsts++;
			dm->m_statsFading.numTris += GetNumTris();
			dm->m_statsFading.numVerts += GetNumVerts();
			return DECAL_STATE_FADEOUT;
		}

#		if __BANK
			if (!IsCulled())
			{
				dm->m_debug.m_statsRendered.numInsts++;
				dm->m_debug.m_statsRendered.numTris += GetNumTris();
				dm->m_debug.m_statsRendered.numVerts += GetNumVerts();
			}
#		endif

		return DECAL_STATE_ALIVE;
	}


	///////////////////////////////////////////////////////////////////////////
	//  Render_Static
	///////////////////////////////////////////////////////////////////////////

	void decalInst::Render_Static(decalRenderStaticBatcher* batcher, u8 texRows, u8 texCols, u8 texIdA, u8 texIdB, float animTime, float fadeDistance, float cullDistance) const
	{
		using namespace Vec;

		decalManager *const dm = &DECALMGR;

		// calc per vertex alpha
		const float alphaFront = ((float)m_alphaFront) * (1.f/255.f);
		const float alphaBack  = ((float)m_alphaBack)  * (1.f/255.f);
		const float alphaBackMinusFront = alphaBack-alphaFront;

		// calc the alpha based on time
		const float fadeInTime  = GetFadeInTime();
		const float fadeOutTime = GetFadeOutTime();
		const float currLife  = m_currLife;
		const float totalLife = m_totalLife;
		float alphaTimeMult = 1.f;
		if (!m_singleFrame)
		{
			alphaTimeMult = Selectf(currLife-fadeInTime, alphaTimeMult, currLife/fadeInTime);
			alphaTimeMult = Selectf(currLife-totalLife+fadeOutTime, (totalLife-currLife)/fadeOutTime, alphaTimeMult);
			decalAssertf(FPIsFinite(alphaTimeMult),
				"alphaTimeMult=0x%08x, currLife=%f, totalLife=%f, fadeInTime=%f, fadeOutTime=%f",
				union_cast<u32>(alphaTimeMult), currLife, totalLife, fadeInTime, fadeOutTime);
		}

		// calc the alpha based on washing
		const float alphaWashMult = Min(GetWashLevel(), 1.f);

		// calc the alpha based on distance
		float alphaDistMult = 1.f;
		const float distToViewport = m_distToViewport.GetFloat32_FromFloat16();
		alphaDistMult = Selectf(distToViewport-cullDistance, 0.f, alphaDistMult);
		alphaDistMult = Selectf(distToViewport-fadeDistance, (cullDistance-distToViewport)/(cullDistance-fadeDistance), alphaDistMult);
		decalAssertf(FPIsFinite(alphaDistMult),
			"alphaDistMult=0x%08x, distToViewport=%f, cullDistance=%f, fadeDistance=%f",
			union_cast<u32>(alphaTimeMult), distToViewport, cullDistance, fadeDistance);
		const float alphaMults = alphaDistMult*alphaTimeMult*alphaWashMult;
		const float alphaMul(alphaBackMinusFront*alphaMults);
		const float alphaAdd(alphaFront*alphaMults);
		const Vec2V alphaConst(alphaMul, alphaAdd);

		// calculate the texture coordinate mutliplier
		const float ooUVMult = 1.f/CalcUVMult();
		decalAssertf(FPIsFinite(ooUVMult),
			"ooUVMult=0x%08x, m_currLife=%f, m_uvMultTime=0x%04x",
			union_cast<u32>(ooUVMult), m_currLife, m_uvMultTime.GetBinaryData());

		// calculate current animation frame
		u8 texId = texIdA;
		if (animTime > 0.0f)
		{
			const float a = (float)texIdA;
			const float b = (float)texIdB;
			const float t = Clamp(m_currLife/animTime, 0.f, 1.f);
			texId = (u8)(a + t*(b-a));
		}

		// calculate texture pages and offsets
		const float xStep = 1.f/texCols;
		const float yStep = 1.f/texRows;
		const int xIndex = texId % texCols;
		const int yIndex = texId / texCols;
		const float texcoordMulX = xStep*ooUVMult;
		const float texcoordMulY = yStep*ooUVMult;
		const float texcoordAddX = xStep*(xIndex+0.5f-0.5f*ooUVMult);
		const float texcoordAddY = yStep*(yIndex+0.5f-0.5f*ooUVMult);
		const Vec4V texcoordConst(texcoordMulX, texcoordMulY, texcoordAddX, texcoordAddY);

		const Vec3V vertexColor(m_colR*(1.f/255.f), m_colG*(1.f/255.f), m_colB*(1.f/255.f));

		const decalPerInstCBuf vars(alphaConst, vertexColor, texcoordConst);
		batcher->SetPerInstanceVars(&vars);

		// Go through the mib list and render them
		const decalMiniIdxBuf *const pMibBase = dm->m_pools.GetMibPoolBase();
		const decalMiniIdxBuf *pMibNext = decalMiniIdxBuf::GetPtr(pMibBase, m_idxBufList);
		decalAssert(pMibNext);
		do
		{
			// This branch, the branch inside GetNumTris, and the branch at the
			// end of the loop, are all closely correlated.  For PowerPC
			// platforms at least, do not add branch hints, as that will prevent
			// the branch predictor from learning the correlation.
			const decalMiniIdxBuf *const pMib = pMibNext;
			if ((pMibNext = pMib->GetNext(pMibBase)) != NULL)
			{
				PrefetchDC(pMibNext);
			}

			batcher->RenderMib(pMib);
		}
		while (pMibNext);
	}


	///////////////////////////////////////////////////////////////////////////
	//  Render_Dynamic
	///////////////////////////////////////////////////////////////////////////

	void decalInst::Render_Dynamic(Mat34V_In bucketWldMtx, u8 texRows, u8 texCols, u8 texIdA, u8 texIdB, float animTime, float texWrapLength, float fadeDistance, float cullDistance, float alphaMult) const
	{
		const ScalarV vHalf = ScalarV(V_HALF);
		const ScalarV vTwo = ScalarV(V_TWO);

#		if DECAL_USE_INDEXED_VERTS == 0
			static const int vertIndices[DECAL_NUM_DYNAMIC_INST_VERTS] = {0, 0, 1, 3, 2, 7, 6, 4, 5, 5, 2, 2, 6, 1, 5, 0, 4, 3, 7, 7};
#		endif

		Vec3V vPosition = Transform(bucketWldMtx, GetPosition());
		Vec3V vDirection = Transform3x3(bucketWldMtx, GetDirection());
		Vec3V vSide = Transform3x3(bucketWldMtx, GetSide());
		Vec3V vForward = Cross(vSide, vDirection);

		Vec3V halfDimensions = GetDimensions() * vHalf;
		vSide *= SplatX(halfDimensions);
		vForward *= SplatY(halfDimensions);
		vDirection *= SplatZ(halfDimensions);

		// apply any uv mults by altering the render size
		float uvMult = CalcUVMult();
		ScalarV vUvMult = ScalarVFromF32(uvMult);
		vSide *= vUvMult;
		vForward *= vUvMult;

		Vec3V vVerts[8];
		vVerts[0] = vPosition + vForward - vSide + vDirection;
		vVerts[1] = vPosition + vForward + vSide + vDirection;
		vVerts[2] = vPosition - vForward + vSide + vDirection;
		vVerts[3] = vPosition - vForward - vSide + vDirection;
		vVerts[4] = vVerts[0] - (vDirection*vTwo);
		vVerts[5] = vVerts[1] - (vDirection*vTwo);
		vVerts[6] = vVerts[2] - (vDirection*vTwo);
		vVerts[7] = vVerts[3] - (vDirection*vTwo);

		// pack colour red component in w
		const float fColR = (float)m_colR/255.0f;
		vVerts[0].SetWf(fColR);
		vVerts[1].SetWf(fColR);
		vVerts[2].SetWf(fColR);
		vVerts[3].SetWf(fColR);
		vVerts[4].SetWf(fColR);
		vVerts[5].SetWf(fColR);
		vVerts[6].SetWf(fColR);
		vVerts[7].SetWf(fColR);


		// calc the alpha based on time
		float alphaTimeMult = 1.0f;
		if (IsFadingIn())
		{
			alphaTimeMult = m_currLife/GetFadeInTime();
		}
		else if (IsFadingOut())
		{
			alphaTimeMult = (m_totalLife-m_currLife)/GetFadeOutTime();
		}

		// calc the alpha based on washing
		float alphaWashMult = Min(GetWashLevel(), 1.f);

		// calc the alpha based on distance
		float alphaDistMult = 1.0f;
		const float distToViewport = m_distToViewport.GetFloat32_FromFloat16();
		if (distToViewport>cullDistance)
		{
			alphaDistMult = 0.0f;
		}
		else if (distToViewport>fadeDistance)
		{
			alphaDistMult = (cullDistance-distToViewport)/(cullDistance-fadeDistance);
		}

		// calc the final alpha
		float vertexAlpha = alphaTimeMult * alphaDistMult * alphaWashMult * alphaMult;

		// calculate current animation frame
		u8 texId = texIdA;
		if (animTime > 0.0f)
		{
			float a = (float)texIdA;
			float b = (float)texIdB;
			float t = Clamp(m_currLife/animTime, 0.0f, 1.0f);
			texId = (u8)(a + t*(b-a));
		}

		// add the verts to the vertex buffer
		Vec4V vForward4(vForward);
		Vec4V vSide4(vSide);
		Vec4V vTex4((float)texRows, (float)texCols, (float)texId, (float)(m_flipU*2+m_flipV));

		float fColG = (float)m_colG/255.0f;
		Vec4V vAlpha4(vertexAlpha, m_alphaFront/255.0f, m_alphaBack/255.0f, fColG);

		// pack texture wrap length and current wrap length in spare w component
		Vec4V vPosition4 = Vec4V(vPosition);
		vForward4.SetWf(texWrapLength);
		vPosition4.SetWf(m_currWrapLength);

		// pack blue colour component in w
		float fColB = (float)m_colB/255.0f;
		vSide4.SetWf(fColB);


#		if DECAL_USE_INDEXED_VERTS
			decalDrawInterface::CopyDynamicDecalVertices(vVerts, vPosition4, vForward4, vSide4, vTex4, vAlpha4);
#		else
			decalManager *const dm = &DECALMGR;
			for (int v=0; v<DECAL_NUM_DYNAMIC_INST_VERTS; v++)
			{
				dm->m_vtxBufferDynamic.AddVertex(vVerts[vertIndices[v]], vPosition4, vForward4, vSide4, vTex4, vAlpha4);
			}
#		endif
	}


#	if __BANK

		///////////////////////////////////////////////////////////////////////
		//  RenderDebug
		///////////////////////////////////////////////////////////////////////

		void decalInst::RenderDebug(Mat34V_In bucketWldMtx, ScalarV_In posScale, Vec3V_In msOffsetFlt, Color32 debugCol) const
		{
			float alpha = 1.0f;
			if (IsCulled())
			{
				alpha = 0.1f;
			}

			decalManager *const dm = &DECALMGR;

			// render the decal box
			if (dm->m_debug.m_renderInstBox)
			{
				Vec3V vPosition = Transform(bucketWldMtx, GetPosition());
				Vec3V vDirection = Transform3x3(bucketWldMtx, GetDirection());
				Vec3V vSide = Transform3x3(bucketWldMtx, GetSide());
				Vec3V vForward = Cross(vSide, vDirection);

				Mat34V mtx;
				mtx.SetCol0(vSide);
				mtx.SetCol1(vForward);
				mtx.SetCol2(vDirection);
				mtx.SetCol3(vPosition);

				Color32 col(1.0f, 1.0f, 1.0f, alpha);
				if (m_joinType==DECAL_JOINTYPE_BACK) col = Color32(1.0f, 1.0f, 0.0f, alpha);
				else if (m_joinType==DECAL_JOINTYPE_BACK_CROSS) col = Color32(1.0f, 0.0f, 0.0f, alpha);
				else if (m_joinType==DECAL_JOINTYPE_FRONT_CROSS) col = Color32(1.0f, 0.5f, 0.0f, alpha);

				if (DECALMGRASYNC.m_debug.m_bucketColourMode)
				{
					col = debugCol;
					col.SetAlpha(static_cast<u8>(alpha*255));
				}

				Vec3V halfDimensions = GetDimensions() * ScalarV(V_HALF);
				grcDebugDraw::BoxOriented(-halfDimensions, halfDimensions, mtx, col, false);
			}

			// render the bounding sphere
			if (dm->m_debug.m_renderInstSphere)
			{
				float boundRadius = GetBoundRadius().Getf();
				Vec3V vBoundCentre = Transform(bucketWldMtx, GetBoundCentre());

				Color32 col(1.0f, 1.0f, 1.0f, alpha);
				if (DECALMGRASYNC.m_debug.m_bucketColourMode)
				{
					col = debugCol;
					col.SetAlpha(static_cast<u8>(alpha*255));
				}

				grcDebugDraw::Sphere(vBoundCentre, boundRadius, col, false);
			}

			// render the tris
			if (dm->m_debug.m_renderInstTris)
			{
				const decalMiniIdxBuf* const pMibBase = dm->m_pools.GetMibPoolBase();
				const decalVtx*        const pVtxBase = dm->m_pools.GetVtxPoolBase();

				Color32 col(0.5f, 0.5f, 0.5f, alpha);
				if (DECALMGRASYNC.m_debug.m_bucketColourMode)
				{
					col = debugCol;
					col.SetAlpha(static_cast<u8>(alpha*255));
				}

				const decalMiniIdxBuf *pMib = decalMiniIdxBuf::GetPtr(pMibBase, m_idxBufList);
				while (pMib)
				{
					const unsigned numIndices = pMib->GetNumTris()*3;
					for (unsigned ii=0; ii<numIndices; ii+=3)
					{
						// check if the point is within the triangle
						Vec3V vtx0 = pVtxBase[pMib->m_idx[ii+0]].GetPos(posScale, msOffsetFlt);
						Vec3V vtx1 = pVtxBase[pMib->m_idx[ii+1]].GetPos(posScale, msOffsetFlt);
						Vec3V vtx2 = pVtxBase[pMib->m_idx[ii+2]].GetPos(posScale, msOffsetFlt);

						vtx0 = Transform(bucketWldMtx, vtx0);
						vtx1 = Transform(bucketWldMtx, vtx1);
						vtx2 = Transform(bucketWldMtx, vtx2);

						grcDebugDraw::Poly(vtx0, vtx1, vtx2, col, false, false);
					}

					pMib = pMib->GetNext(pMibBase);
				}
			}
		}

#	endif // __BANK


	///////////////////////////////////////////////////////////////////////////
	//  CalcUVMult
	///////////////////////////////////////////////////////////////////////////

	float decalInst::CalcUVMult() const
	{
		if (m_isLiquidPool)
		{
			// use the start mult as the uv mult (this gets updated as more liquid drips into the pool)
			return m_uvMultStart.GetFloat32_FromFloat16();
		}
		else
		{
			// use current life to work out the uv mult
			float uvMultRatio = 1.0f;
			const float uvMultTime = m_uvMultTime.GetFloat32_FromFloat16();
			if (m_currLife<uvMultTime)
			{
				uvMultRatio = m_currLife / uvMultTime;
			}
			const float uvMultStart = m_uvMultStart.GetFloat32_FromFloat16();
			const float uvMultEnd = m_uvMultEnd.GetFloat32_FromFloat16();
			return uvMultStart + ((uvMultEnd-uvMultStart)*uvMultRatio);
		}
	}

#endif // !__SPU

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
