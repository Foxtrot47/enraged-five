//
// vfx/decal/decaldefraglocker.h
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALDEFRAGLOCKER_H
#define VFX_DECALDEFRAGLOCKER_H

#if !__TOOL && !__RESOURCECOMPILER

#include "streaming/streamingdefs.h"


#if USE_DEFRAGMENTATION


// namespaces
namespace rage {


// forward declarations
class pgBase;
class strIndex;


class decalDefragLocker
{
public:

	void Init(unsigned poolSize);

	void Shutdown();

	bool LockRawPtr(const void *ptr);

	bool LockPgBase(const pgBase *resource);

#	if __ASSERT
#		if !__SPU
			bool AssertOnlyIsAlreadyLocked(const void *ptr) const;
#		else
			// SPU code doesn't have access to the necissary memory managers to
			// implement this
			inline bool AssertOnlyIsAlreadyLocked(const void*) const {return true;}
#		endif
#	endif

	void UnlockAll();


private:

	struct Lock
	{
		pgBase     *m_resource;
		strIndex    m_index;
#		if __ASSERT
			u32 m_userData;
#		endif
	};

	struct LockCmp
	{
		inline bool operator()(const pgBase *lhs, const Lock   &rhs) const;
		inline bool operator()(const Lock   &lhs, const pgBase *rhs) const;
		inline bool operator()(const Lock   &lhs, const Lock   &rhs) const;
	};

	Lock *m_locks;

	unsigned m_maxNumLocks;

	// If we ever fail to lock (return false from either LockRawPtr or
	// LockPgBase), then m_numLocks will get set to ~0 until the next UnlockAll.
	// This is to allow AssertOnlyIsAlreadyLocked to always return true in this
	// case.
	unsigned m_numLocks;


	bool LockInternal(pgBase *resource, strIndex idx);
};


} // namespace rage


#endif // USE_DEFRAGMENTATION

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALDEFRAGLOCKER_H
