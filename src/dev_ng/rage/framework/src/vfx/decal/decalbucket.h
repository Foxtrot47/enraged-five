//
// vfx/decal/decalbucket.h
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALBUCKET_H
#define	VFX_DECALBUCKET_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "vectormath/vectormath.h"


// include (framework)
#include "fwtl/regdrefs.h"
#include "fwscene/world/InteriorLocation.h"
#include "vfx/slist.h"
#include "vfx/decal/decalhelper.h"
#include "vfx/decal/decalinst.h"
#if !__SPU
#include "vfx/decal/decalsettingsmanager.h"
#endif


// namespaces
namespace rage {

// Not using the MULTIPLE_RENDER_THREADS define here, since it can be switched
// off, instead DECAL_MULTI_THREADED_RENDERING is true for platforms that will
// generally support MULTIPLE_RENDER_THREADS.
#define DECAL_MULTI_THREADED_RENDERING          (RSG_DURANGO || RSG_ORBIS || (RSG_PC && __D3D11))

#if DECAL_MULTI_THREADED_RENDERING
#	define DECAL_MULTI_THREADED_RENDERING_ONLY(...)     __VA_ARGS__
#else
#	define DECAL_MULTI_THREADED_RENDERING_ONLY(...)
#endif

// forward declarations
struct decalAsyncTaskDescBase;
class  decalInst;
struct decalInstSettings;
class  fwEntity;
class  fwTexturePayload;
class  grcViewport;
class  phInst;

// classes
class decalBucket : public SListItem<decalBucket>
{
	friend class decalInst;
	friend class decalManager;
	friend class decalManagerAsync;
	friend class decalLodSystem;
	friend class decalRecycler;


	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		enum BucketFlags
		{
			BUCKETFLAG_IS_GLASS = (1<<0),
			BUCKETFLAG_IS_CULLED = (1<<1),
			BUCKETFLAG_IS_PERSISTENT = (1<<2),
			BUCKETFLAG_UNUSED = (1<<3),
			BUCKETFLAG_IS_SCRIPTED = (1<<4),
			BUCKETFLAG_IS_FADINGOUT = (1<<5),
			BUCKETFLAG_IS_MARKEDFORDELETE = (1<<6),
			BUCKETFLAG_IS_ENTITYVISIBLETHISFRAME = (1<<7),
		};

		void Init(decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, ScalarV_In posScale, Vec3V_In msOffsetInt, Vec4V_In boundSphere, Mat34V_In wldMtx, s16 attachMatrixId, decalAttachType attachType, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted, void* pSmashGroup);

		void Init(decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, float posScale, u32 msOffsetInt, Vec4V_In boundSphere, Mat34V_In wldMtx, s16 attachMatrixId, decalAttachType attachType, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted, void* pSmashGroup);

		ScalarV_Out GetPosScale() const
		{
			using namespace Vec;
			Vector_4V v;
			v = V4LoadLeft(&m_posScale);
			v = V4SplatX(v);
			return ScalarV(v);
		}

		Vec3V_Out GetModelSpaceOffsetInt() const
		{
			using namespace Vec;
			CompileTimeAssert((OffsetOf(decalBucket, m_modelSpaceOffsetX) & 3) == 0);
			CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetY) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 1);
			CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetZ) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 2);
			Vector_4V v = V4LoadLeft(&m_modelSpaceOffsetX);
			v = V4UnpackLowSignedByte(v);
			v = V4UnpackLowSignedShort(v);
			v = V4IntToFloatRaw<0>(v);
			return Vec3V(v);
		}

		// Return a u32 representation of the model space offset.  Format is
		// unspecified.  Only for use in comparing buckets.
		u32 GetModelSpaceOffsetIntU32() const
		{
			CompileTimeAssert((OffsetOf(decalBucket, m_modelSpaceOffsetX) & 3) == 0);
			CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetY) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 1);
			CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetZ) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 2);
			typedef u32 MAY_ALIAS u32_alias;
			u32 val = *(u32_alias*)&m_modelSpaceOffsetX;
#			if __BE
				return val >> 8;
#			else
				return val << 8;
#			endif
		}

		const decalInst* GetInstListHead() const {return m_instList.Peek();}

		const fwTexturePayload* GetVehDamage() const {return m_pDamageMapData;}
		float GetDamageRadius() const {return m_damageRadius;}
		float GetDamageMult() const {return m_damageMult;}

		bool GetIsDamaged() const {return m_isDamaged;}
		void SetIsDamaged(bool isDamaged) {m_isDamaged=isDamaged;}

		bool GetForceRegen() const {return m_forceRegen;}
		void SetForceRegen(bool forceRegen) {m_forceRegen=forceRegen;}

		void ShutdownImmediate();
		void ShutdownDelayed();
		bool Update_BeforeAdd();
		bool Update_AfterAdd(float deltaTime, decalRenderPass renderPass, decalMethod method, const grcViewport* pActiveGameVP, Vec4V_In gameCamPosFOVScale);

		void Render_Static(bool bIsForwardPass  DECAL_MULTI_THREADED_RENDERING_ONLY(, unsigned ctxIdx)) const;

		void Render_Dynamic(bool bIsForwardPass) const;

#if __BANK
		void RenderDebug() const;
#endif


		bool GetIsCulled() const {return (m_bucketFlags & BUCKETFLAG_IS_CULLED) != 0;}

		// utility functions
		bool PreProcessOverlays(const fwEntity* entity, decalUserSettings& settings, float& newRadius);
		bool PreProcessLiquidPools(decalUserSettings& settings);

		float GetWashLevel(int decalId) const;
		void Remove(int decalId);
		fwInteriorLocation GetInteriorLocation()		const { return m_interiorLocation; }

		void Wash(float amount);
		void WashInRange(float amount, Vec3V_In vPos, ScalarV_In range);

		void RemoveFacing(Vec3V_In vDir);
		int RemoveInRange(Vec3V_In vPos, ScalarV_In range);
		void FadeInRange(Vec3V_In vPos, ScalarV_In range, float fadeTime);

		bool IsPointInLiquid(s8& liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, bool isDynamic, float distThresh);
		bool IsPointWithin(s8 liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, bool isDynamic, float distThresh);

		void ReApplyBreakable(const fwEntity* pAttachEntity, s16 oldMatrixId, s16 newMatrixId, decalRenderPass renderPass);
		bool ReApplyVehicleGlass(const fwEntity* pAttachEntity, void* pSmashGroup, decalRenderPass renderPass);
		bool ReApplyAsStaticDecal(decalInst* pInst, void* pSmashGroup, decalRenderPass renderPass, s16 newMatrixId, bool onlyApplyToGlass, bool dontApplyToGlass, bool applyToBreakables);

		// access functions
		decalAsyncTaskDescBase *GetTaskDesc() {return m_taskDesc;}
		const decalAsyncTaskDescBase *GetTaskDesc() const {return m_taskDesc;}

		void SetAttachEntity(fwEntity* ent) {m_attachEntity = ent;}
		fwEntity* GetAttachEntity() {return m_attachEntity.Get();}
		const fwEntity* GetAttachEntity() const {return m_attachEntity.Get();}

		u16 GetRenderSettingsIndex() const {return m_renderSettingsIndex;}

		float GetPosScaleFloat() const {return m_posScale;}


	private: //////////////////////////

		void InitCommon(decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, Vec4V_In boundSphere, Mat34V_In wldMtx, s16 attachMatrixId, decalAttachType attachType, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted, void* pSmashGroup);

		void ExpandBoundSphere(const decalInst* pInstLs);
		void ExpandBoundSphere(Vec4V_In boundSphere);

#		if !__SPU
			bool UpdateMatrix(bool* isDamaged, const fwEntity* pEnt, const phInst* pInst);
			void AddInst(decalInst* pInstNew);
			void MergeInsts(decalMethod method, SList<decalInst>* pList, Vec4V_In boundSphere);
			void RemoveInstSafe(decalInst* pInst);

			void MarkForDelete() { m_bucketFlags |= BUCKETFLAG_IS_MARKEDFORDELETE; MarkInstsForDelete();}
			bool IsMarkedForDelete() const {return (m_bucketFlags & BUCKETFLAG_IS_MARKEDFORDELETE) != 0;}
			void ProcessMarkedForDelete(decalMethod method);
			void MarkInstsForDelete();

			bool IsGlass()					const { return (m_bucketFlags & BUCKETFLAG_IS_GLASS) != 0;}
			bool IsPersistent()				const { return (m_bucketFlags & BUCKETFLAG_IS_PERSISTENT) != 0; }
			bool IsFadingOut()				const { return (m_bucketFlags & BUCKETFLAG_IS_FADINGOUT) != 0; }
			bool IsScripted()				const { return (m_bucketFlags & BUCKETFLAG_IS_SCRIPTED) != 0; }
			bool IsEntityVisibleThisFrame() const { return (m_bucketFlags & BUCKETFLAG_IS_ENTITYVISIBLETHISFRAME) != 0; }

			void RecalcBoundSphere();
			int GetNumTris() const;
			int GetNumDynamicInstsToRender() const;
			int GetNumVerts() const;

			__forceinline bool IsWashable() const
			{
				// check if this bucket is washable
				bool isWashable;
				const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex);
				isWashable = pRenderSettings->isWashable;

#if __BANK
				if(CheckForceWashable())
				{
					isWashable = true;
				}
#endif
				return isWashable;
			}

			__forceinline bool FadesUnderwater() const
			{
				// check if this bucket fades underwater
				bool fadesUnderwater;
				const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex);
				fadesUnderwater = pRenderSettings->fadeUnderwater;
				return fadesUnderwater;
			}

			inline Vec4V_Out GetBoundSphere() const {return Vec4V(Transform(m_wldMtx, m_boundSphere.GetXYZ()), m_boundSphere.GetW()); }
			//inline float GetBoundRadius() {return m_boundSphere.GetWf();}
			//inline Vec3V_Out GetBoundCentre() {return Transform(m_wldMtx, m_boundSphere.GetXYZ());}

			ScalarV_Out FindClosestDistSqr(Vec3V_In vTestPos) const;

#if __BANK
			bool CheckForceWashable() const;
#endif

#		endif // !__SPU


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

		SList<decalInst> m_instList;
		void* m_pSmashGroup;

		Mat34V m_wldMtx;
		Vec4V m_boundSphere;

		decalAsyncTaskDescBase* m_taskDesc;

#		if !__SPU
			fwRegdRef<fwEntity> m_attachEntity;
#		else
			fwEntity* m_attachEntity;
#		endif
		decalAttachType m_attachType;				// how the matrix is attached (component id, bone index, skinned bone index)
		s16 m_attachMatrixId;						// either a component id (child of a composite), bone hierarchy index or bgGlassHandle

		u16	m_typeSettingsIndex;
		u16 m_renderSettingsIndex;
		u16 m_subType;
		
		fwTexturePayload* m_pDamageMapData;
		float m_damageRadius;
		float m_damageMult;

		float m_posScale;

		float m_ambient[2];

		fwInteriorLocation m_interiorLocation;

		// Offset from the model space origin in units of
		// decalManagerAsync::GetModelSpaceOffsetUnits().
		s8 m_modelSpaceOffsetX;
		s8 m_modelSpaceOffsetY;
		s8 m_modelSpaceOffsetZ;

		// WARNING: The next byte will get stomped on in Init().  If re-ordering
		// the member variables here, be sure that Init() still initializes
		// variables in the correct order.

		// Flags
		u8 m_bucketFlags; //uses BucketFlags enum to set values

		u8 m_entityAlpha;

		bool m_isDamaged : 1;
		bool m_forceRegen : 1;
		bool m_isInInterior : 1;
		bool m_isForceAlpha: 1;
};

// externs
extern dev_float DECAL_ZSHIFT_BASE;
extern dev_bool DECAL_WASH_UNDERWATER_INSTS;


} // namespace rage


#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALBUCKET_H
