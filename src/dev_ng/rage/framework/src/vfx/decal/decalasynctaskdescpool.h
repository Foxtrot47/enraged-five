//
// vfx/decal/decalasynctaskdescpool.h
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALASYNCTASKDESCPOOL_H
#define VFX_DECALASYNCTASKDESCPOOL_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "system/debugmemoryfill.h"
#include "system/memory.h"
#include "system/new.h"

#include "vfx/decal/decalasynctaskdescbase.h"

#include <string.h>


// namespaces
namespace rage {


// Because the actual task descriptor type needs to live in game play code,
// template this pool type to allow the logic to live in framework code.
// USER_DERIVED_TYPE must publicly derive from decalAsyncTaskDescBase.
template<class USER_DERIVED_TYPE>
class decalAsyncTaskDescPool
{
public:

	void Init(unsigned count);
	void Shutdown();
	decalAsyncTaskDescBase *AllocAsyncTaskDesk();
	void FreeAsyncTaskDesc(decalAsyncTaskDescBase *desc);


private:

	USER_DERIVED_TYPE *allNodes;
	USER_DERIVED_TYPE *freeNodes;
#if __ASSERT || SUPPORT_DEBUG_MEMORY_FILL
	unsigned numNodes;
#endif
};


template<class USER_DERIVED_TYPE>
void decalAsyncTaskDescPool<USER_DERIVED_TYPE>::Init(unsigned count)
{
	// Cannot allow the derived type to be more aligned than the base type,
	// otherwise this will cause issues for spu code.
	CompileTimeAssert(__alignof(USER_DERIVED_TYPE) == __alignof(decalAsyncTaskDescBase));

	USER_DERIVED_TYPE *const nodes = static_cast<USER_DERIVED_TYPE*>(
		sysMemAllocator::GetCurrent().RAGE_LOG_ALLOCATE(count*sizeof(USER_DERIVED_TYPE), __alignof(decalAsyncTaskDescBase)));
	Assert(nodes);

	IF_DEBUG_MEMORY_FILL_N(memset(nodes, 0xcc, count*sizeof(USER_DERIVED_TYPE)),DMF_ASYNCPOOL);

	Assert(count > 0);
	for (unsigned i=0; i<count-1; ++i)
	{
		nodes[i].mainThreadData.next = nodes+i+1;
	}
	nodes[count-1].mainThreadData.next = NULL;
	allNodes = freeNodes = nodes;

#if __ASSERT || SUPPORT_DEBUG_MEMORY_FILL
	numNodes = count;
#endif
}


template<class USER_DERIVED_TYPE>
void decalAsyncTaskDescPool<USER_DERIVED_TYPE>::Shutdown()
{
#if __ASSERT
	unsigned numFree = 0;
	for (const USER_DERIVED_TYPE *p=freeNodes; p; p=static_cast<const USER_DERIVED_TYPE*>(p->mainThreadData.next))
	{
		++numFree;
	}
	Assert(numFree == numNodes);
#endif

	IF_DEBUG_MEMORY_FILL_N(memset(allNodes, 0xfe, numNodes*sizeof(USER_DERIVED_TYPE)),DMF_ASYNCPOOL);

	RAGE_LOG_DELETE(allNodes);
	sysMemAllocator::GetCurrent().Free(allNodes);
	allNodes = freeNodes = NULL;
}


template<class USER_DERIVED_TYPE>
decalAsyncTaskDescBase *decalAsyncTaskDescPool<USER_DERIVED_TYPE>::AllocAsyncTaskDesk()
{
	USER_DERIVED_TYPE *const desc = freeNodes;
	if (desc)
	{
		freeNodes = static_cast<USER_DERIVED_TYPE*>(desc->mainThreadData.next);
		IF_DEBUG_MEMORY_FILL_N(memset(desc, 0xcd, sizeof(*desc)),DMF_ASYNCPOOL);
		rage_placement_new(desc) USER_DERIVED_TYPE;
	}
	return desc;
}


template<class USER_DERIVED_TYPE>
void decalAsyncTaskDescPool<USER_DERIVED_TYPE>::FreeAsyncTaskDesc(decalAsyncTaskDescBase *desc)
{
	Assert(desc);
	USER_DERIVED_TYPE *const udt = static_cast<USER_DERIVED_TYPE*>(desc);
	udt->~USER_DERIVED_TYPE();
	IF_DEBUG_MEMORY_FILL_N(memset(udt, 0xdd, sizeof(*udt)),DMF_ASYNCPOOL);
	udt->mainThreadData.next = freeNodes;
	freeNodes = udt;
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALASYNCTASKDESCPOOL_H
