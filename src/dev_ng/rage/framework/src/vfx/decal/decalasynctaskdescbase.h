//
// vfx/decal/decalasynctaskdescbase.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALASYNCTASKDESCBASE_H
#define VFX_DECALASYNCTASKDESCBASE_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "system/taskheader.h"
#include "vector/matrix34.h"

// includes (framework)
#include "vfx/asyncslist.h"
#include "vfx/int128.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalasynctasktype.h"
#include "vfx/decal/decalbones.h"
#include "vfx/decal/decalsettings.h"
#if !__SPU
#include "fwtl/regdrefs.h"
#endif


// namespaces
namespace rage {


// forward declarations
class decalBucket;
class decalManagerAsync;
class fragInst;
class fwEntity;
class Mat34V;
class phArchetype;
class phBound;
class phInst;
class phObjectData;
class rmcDrawable;
class Vec4V;


#ifdef _MSC_VER
#	pragma warning(push)
#	pragma warning(disable: 4201)  // nonstandard extension used : nameless struct/union
#endif


// classes
struct decalAsyncTaskDescBase
{
	// stuff needed by the task
	Mat34V                      entityMtx;
	decalSettings               settings;  // TODO: double check exactly what settings are actually required
	const decalTypeSettings    *typeSettings;
	const decalRenderSettings  *renderSettings;
	fwEntity                   *pEntity;
	unsigned                    bucketLists;
	decalAsyncTaskType          taskType;
	float                       entityRadius;
	u8                         *taskInstCounter;

	// Having this in a union saves space, but can be a pain for debugging (eg,
	// ProDG only shows the first struct).  For debugging purposes, the union
	// can be commented out so that both structs are present at separate
	// offsets.
#if !VFX_FW_DECAL_OPTIMISATIONS_OFF
	union
	{
#endif

		// DECAL_DO_PROCESSBOUND / DECAL_DO_PROCESSBREAKABLEGLASS
		struct
		{
			phInst                     *pPhInst;
			u32                         includeFlags;
#			if __PS3
				unsigned                    phInstSize;
#			endif
		};

		// DECAL_DO_PROCESSDRAWABLESNONSKINNED / DECAL_DO_PROCESSDRAWABLESSKINNED
		struct
		{
			// Packed object space matrices transposed to be 48-bytes each.
			// Indexed [0..numSkelMtxs-1].
			const Vec4V                *skelObjMtxs;

			// Parallel array of bone hierarchy indices for each of the packed
			// skeleton matrices.  Indexed by bone task index,
			// [0..numSkelMtxs-1].
			DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones;

			// Parallel array of drawable indices for each of the packed
			// skeleton matrices.  Indexed [0..numSkelMtxs-1].  Will be NULL if
			// there is only a single drawable.
			const u8                   *skelDrawableIdxs;

			// The object space matrices are converted to world space
			// differently, depending on wether or not the drawable is skinned.
			// For skinned drawables, we need the inverse of the joint transform
			// to calculate the world space matrix.  These joint inverses are
			// static and do not change (stored in the crSkeletonData).  This
			// array is indexed by the skeleton's bone index.
			const Mat34V               *skelInvJoints;

			// Pointer to each of the drawables used.  Indexed by
			// skelDrawableIdxs[i] (which should be in the range
			// [0..numDrawables-1]), or if skelDrawableIdxs is NULL, then
			// numDrawables should be 1, and only drawablePtrs[0] is valid.
			const rmcDrawable          *drawablePtrs[DECAL_MAX_DRAWABLES];

			// Optional remapping from bone render index to bone hierarchy
			// index.  If no remapping is specified, then the two index spaces
			// are the same.
			DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut;

			u8                          numSkelMtxs;            // number of packed skeleton matrices that have been stored
			u8                          numRenderBones;         // number of elements in renderToHierarchyLut
			u8                          numDrawables;
			u8                          damagedDrawableMask;
		};

		// DECAL_DO_PROCESSSMASHGROUP
// 		struct
// 		{
// 		};

#if !VFX_FW_DECAL_OPTIMISATIONS_OFF
	};
#endif

	void CompileTimeAssertCheck() { CompileTimeAssert(sizeof(damagedDrawableMask)*8 >= DECAL_MAX_DRAWABLES); }

#	if __PS3
		decalManagerAsync          *decalMan;
		unsigned                    entitySize;
#	endif

#	if DECAL_DEBUG_NAME
		char                        debugName[32];
#	endif

	// Main thread book keeping.
	// This data is opaque on the SPUs, but must still be present since game code
	// derives from this struct.
#	if __64BIT
#		define DECALASYNCTASKDESCBASE_MAINTHREADDATASIZE    (24 ASSERT_ONLY(+16))
#		define DECALASYNCTASKDESCBASE_MAINTHREADDATAALIGN   (8)
#	else
#		define DECALASYNCTASKDESCBASE_MAINTHREADDATASIZE    (12 ASSERT_ONLY(+8))
#		define DECALASYNCTASKDESCBASE_MAINTHREADDATAALIGN   (4)
#	endif
#	if !__SPU
		struct MainThreadData
		{
			decalAsyncTaskDescBase     *next;
			fwRegdRef<fwEntity>         regdEnt;
			sysTaskHandle               handle;
#			if __ASSERT
				phArchetype                *debugPhArchetype;
				phBound                    *debugPhBound;
#			endif
		};
		MainThreadData          mainThreadData;
		CompileTimeAssert(   sizeof(MainThreadData) == DECALASYNCTASKDESCBASE_MAINTHREADDATASIZE );
		CompileTimeAssert(__alignof(MainThreadData) == DECALASYNCTASKDESCBASE_MAINTHREADDATAALIGN);
#	else
		ALIGNAS(DECALASYNCTASKDESCBASE_MAINTHREADDATAALIGN) char mainThreadData[DECALASYNCTASKDESCBASE_MAINTHREADDATASIZE];
#	endif
};


#ifdef _MSC_VER
#	pragma warning(pop)
#endif


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALASYNCTASKDESCBASE_H
