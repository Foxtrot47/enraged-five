//
// vfx/decal/decalminiidxbufwriter.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALMINIIDXBUFWRITER_H
#define	VFX_DECALMINIIDXBUFWRITER_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "system/dma.h"

// includes (framework)
#include "vfx/channel.h"
#include "vfx/decal/decalclipper.h"
#include "vfx/decal/decaldmatags.h"
#include "vfx/decal/decalmanagerasync.h"
#include "vfx/decal/decalminiidxbuf.h"


// namespaces
namespace rage {


// classes
class decalMiniIdxBufWriter
{
public:

	inline decalMiniIdxBufWriter();
	inline ~decalMiniIdxBufWriter();

	inline bool ReserveBufs(unsigned count);
	inline decalMiniIdxBuf *AllocBuf();

	inline decalMiniIdxBuf *LocalPtrInit(decalMiniIdxBuf *ea);
	inline decalMiniIdxBuf *LocalPtrLoad(decalMiniIdxBuf *ea);
	inline void Flush();
	inline void Invalidate();


private:

	enum { MAX_RESERVED_BUFS = ((DECAL_MAX_CLIPPED_VERTS-2)*2+(decalMiniIdxBuf::MAX_NUM_TRIS-1)) / decalMiniIdxBuf::MAX_NUM_TRIS + 1 };

	decalMiniIdxBuf    *m_reservedEa[MAX_RESERVED_BUFS];
	unsigned            m_numReserved;

#	if __SPU
		CompileTimeAssert(__alignof(decalMiniIdxBuf) >= 16);
		decalMiniIdxBuf     m_buf[2];
		u32                 m_bufIdx;
		u32                 m_putEa;
#	endif
}
SPU_ONLY();


inline decalMiniIdxBufWriter::decalMiniIdxBufWriter()
	: m_numReserved(0)
#	if __SPU
		, m_bufIdx(0)
		, m_putEa(0)
#	endif
{
}


inline decalMiniIdxBufWriter::~decalMiniIdxBufWriter()
{
	decalAssert(!m_numReserved);
	Flush();
#	if __SPU
		sysDmaWaitTagStatusAll(3<<DMATAG_PUT_MINIIDXBUF0);
#	endif
}


inline bool decalMiniIdxBufWriter::ReserveBufs(unsigned count)
{
	decalAssert(!m_numReserved);
	decalManagerAsync *const dm = &DECALMGRASYNC;
	decalPools *const poolsLs = &dm->m_pools;
	SPU_ONLY(decalPools *const poolsEa = &dm->m_thisEa->m_pools;)
	for (unsigned i=0; i<count; ++i)
	{
		decalMiniIdxBuf *const ea = poolsLs->GetMib(SPU_ONLY(poolsEa, DMATAG_BLOCKING));
		if (Unlikely(!ea))
		{
			for (unsigned j=0; j<i; ++j)
			{
				poolsLs->ReturnMib(m_reservedEa[j] SPU_ONLY(, poolsEa, DMATAG_BLOCKING));
			}
			return false;
		}
		m_reservedEa[i] = ea;
	}
	m_numReserved = count;
	return true;
}


inline decalMiniIdxBuf *decalMiniIdxBufWriter::AllocBuf()
{
	decalAssert(m_numReserved);
	return m_reservedEa[--m_numReserved];
}


inline decalMiniIdxBuf *decalMiniIdxBufWriter::LocalPtrInit(decalMiniIdxBuf *ea)
{
#	if !__SPU
		ea->Init();
		return ea;
#	else
		// Put any old cached buffer back to xdr.
		const u32 oldBufIdx = m_bufIdx;
		const u32 oldPutEa = m_putEa;
		if (Likely(oldPutEa))
		{
			// See comments in Flush as to why there is a fence on this put.
			sysDmaPutf(m_buf+oldBufIdx, oldPutEa, sizeof(*m_buf), DMATAG_PUT_MINIIDXBUF0+oldBufIdx);
		}

		// Flip buffers.
		const u32 newBufIdx = oldBufIdx^1;
		m_bufIdx = newBufIdx;
		m_putEa = (u32)ea;

		// After ensuring any old put from the new buffer has completed, reinitialise the buffer.
		decalMiniIdxBuf *const ls = m_buf+newBufIdx;
		sysDmaWaitTagStatusAll(1<<(DMATAG_PUT_MINIIDXBUF0+newBufIdx));
		ls->Init();
		return ls;
#	endif
}


inline decalMiniIdxBuf *decalMiniIdxBufWriter::LocalPtrLoad(decalMiniIdxBuf *ea)
{
#	if !__SPU
		return ea;
#	else
		// If the ea is the same as what we currently have cached, then just
		// return the local store pointer.
		const u32 oldBufIdx = m_bufIdx;
		const u32 oldPutEa = m_putEa;
		if (Likely(oldPutEa == (u32)ea))
		{
			return m_buf+oldBufIdx;
		}

		// Fetch from xdr.  The get is fenced, as the previous put from this
		// buffer may still be inflight.
		const u32 newBufIdx = oldBufIdx^1;
		decalMiniIdxBuf *const ls = m_buf+newBufIdx;
		sysDmaGetf(ls, (u32)ea, sizeof(*ls), DMATAG_PUT_MINIIDXBUF0+newBufIdx);
		m_bufIdx = newBufIdx;
		m_putEa = (u32)ea;

		// If we previously had something cached, then put it back to xdr.
		if (Likely(oldPutEa))
		{
			// See comments in Flush as to why there is a fence on this put.
			sysDmaPutf(m_buf+oldBufIdx, oldPutEa, sizeof(*m_buf), DMATAG_PUT_MINIIDXBUF0+oldBufIdx);
		}

		// Wait till the buffer we just fetched has arrived in ls.
		sysDmaWaitTagStatusAll(1<<(DMATAG_PUT_MINIIDXBUF0+newBufIdx));
		return ls;
#	endif
}


inline void decalMiniIdxBufWriter::Flush()
{
#	if __SPU
		// Not positive, but think if Flush is called, local store modified, and
		// Flush called again, then it is possible for the mfc to execute a put,
		// read the buffer in local store issue a write, then the spu modifies
		// the local store address after the data has been sent to the bus, but
		// before it has been commited to xdr.  Then the second mfc put from the
		// same address can be commited to xdr first.  So this put here has a
		// fence on it.  Shouldn't hurt anyways.  If you are absolutely sure
		// that this case is NOT possible, then the fence can be removed.
		// Please add a reference to the appropriate docs in the changelist
		// description.
		const u32 putEa = m_putEa;
		if (Likely(putEa))
		{
			const u32 bufIdx = m_bufIdx;
			sysDmaPutf(m_buf+bufIdx, m_putEa, sizeof(*m_buf), DMATAG_PUT_MINIIDXBUF0+bufIdx);
		}
#	endif
}


inline void decalMiniIdxBufWriter::Invalidate()
{
#	if __SPU
		// Clear m_putEa so that we will not dma out anything when flushed again
		// by the dtor.
		m_putEa = 0;
#	endif
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALMINIIDXBUFWRITER_H
