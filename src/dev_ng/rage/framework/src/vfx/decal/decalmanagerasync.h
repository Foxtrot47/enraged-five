//
// vfx/decal/decalmanagerasync.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECALMANAGERASYNC_H
#define	VFX_DECALMANAGERASYNC_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "grcore/edgeExtractgeomspu.h"	// DECAL_MAX_BONES
#include "grcore/effect_config.h"
#include "system/floattoint.h"
#include "vectormath/classes.h"

// includes (framework)
#include "vfx/asyncslist.h"
#include "vfx/slist.h"
#include "vfx/decal/decalbones.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decaldefraglocker.h"
#include "vfx/decal/decalpools.h"
#include "vfx/decal/decalsettings.h"


struct EdgeGeomPpuConfigInfo;


// namespaces
namespace rage {


// defines
#define DECAL_MAX_FRAG_DRAWABLES                (128)
#if USE_EDGE
#	define DECAL_MAX_SPU_PT_VERTS				(2000)
#endif

#if RSG_DURANGO || RSG_ORBIS || RSG_PC
#	define DECAL_MAX_BUCKETS						(256 * 3)
#	define DECAL_MAX_INSTS							(1*1024 * 3)
#	define DECAL_MAX_TRIS							(6*1024 * 3)
#	define DECAL_MAX_VERTS							(10*1024 * 3)
#else
#	define DECAL_MAX_BUCKETS						(256)
#	define DECAL_MAX_INSTS							(1*1024)
#	define DECAL_MAX_TRIS							(6*1024)
#	define DECAL_MAX_VERTS							(10*1024)
#endif

#define DECAL_MAX_MINI_IDX_BUFS                 (DECAL_MAX_TRIS/decalMiniIdxBuf::MAX_NUM_TRIS)

#define DECAL_MODEL_SPACE_OFFSET_UNITS_DEFAULT  (64.f)


// forward declarations
class  bkGroup;
class  bgBreakable;
class  crSkeletonData;
struct decalAsyncTaskDescBase;
class  decalBucket;
class  decalInst;
class  decalMiniIdxBufWriter;
struct decalRenderSettings;
class  decalRenderStaticBatcher;
class  decalSettings;
struct decalTypeSettings;
class  decalVertexCache;
class  fragInst;
class  fwEntity;
class  grmShader;
class  phArchetype;
class  phBound;
class  phBoundGeometry;
class  phInst;
class  phObjectData;
class  phPolygon;
class  rmcDrawable;
struct sysTaskParameters;


typedef void DecalRemovedCallback(int id);


// structures
#if USE_EDGE
struct decalEdgeClipInfo
{
	// ps3 specific variables to deal with edge geometry clipping
	Vec3V decalNormal[DECAL_MAX_BONES] ;				// only need one of these - not per bone - can this be changed Andrzej?
	Vec4V vertsSpu[DECAL_MAX_SPU_PT_VERTS] ;
	Vec4V_Ptr pVertsSpuCurr ;
	Vec4V_Ptr pClipPlanes[DECAL_MAX_BONES] ;
};
#endif


// classes
class decalManagerAsync
{
	friend class decalBucket;
	friend class decalInst;
	friend class decalLodSystem;
	friend class decalMiniIdxBufWriter;
	friend class decalRenderStaticBatcher;

	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		static inline decalManagerAsync &GetInstance() { return *ms_instance; }

#if __SPU
		static inline void SetInstance(decalManagerAsync *self) { ms_instance = self; }
#endif

#if __BANK
		inline const decalDebug *GetDebug() const { return &m_debug; }
		inline bkGroup *DebugGetGameGroup() { return m_debug.GetGameGroup(); }
#endif

		void ProcessInst(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, const fwEntity* pEntity, phInst* pPhInst, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8 *skelDrawableIdxs, const Mat34V *skelInvJoints, DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut);

		inline ScalarV_Out GetModelSpaceOffsetUnits() const
		{
#			if __BANK
				return ScalarV(m_modelSpaceOffsetUnits);
#			else
				return ScalarV(Vec::V4VConstantSplat<FLOAT_TO_INT(DECAL_MODEL_SPACE_OFFSET_UNITS_DEFAULT)>());
#			endif
		}

		inline ScalarV_Out GetModelSpaceOffsetUnitsInverse() const
		{
#			if __BANK
				return Invert(ScalarV(m_modelSpaceOffsetUnits));
#			else
				return ScalarV(Vec::V4VConstantSplat<FLOAT_TO_INT(1.f/DECAL_MODEL_SPACE_OFFSET_UNITS_DEFAULT)>());
#			endif
		}

		inline ScalarV_Out GetMaxPositionScale() const
		{
#			if __BANK
				return ScalarV(m_modelSpaceOffsetUnits) * ScalarV(V_TWO);
#			else
				return ScalarV(Vec::V4VConstantSplat<FLOAT_TO_INT(DECAL_MODEL_SPACE_OFFSET_UNITS_DEFAULT*2.f)>());
#			endif
		}


	protected: ////////////////////////

		decalManagerAsync();
		~decalManagerAsync();

		// processing pipeline
#if !__PS3
		static void ProcessInstTask(sysTaskParameters& p);
#endif
		ScalarV_Out CalcPosScale(ScalarV_In entityRadius) const;
		Vec3V_Out CalcModelSpaceOffsetInt(const decalSettings *settings) const;

		bool CheckCullBox(const decalSettings* settings, const phBound* bound, Mat34V_In wldMtx) const;
		void ProcessBreakableGlass(DECAL_DEBUG_NAME_ONLY(const char* debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, fragInst* pFragInst, decalMiniIdxBufWriter* miniIdxBufWriter);
#if __SPU
		template<bool SKINNED>
		void ProcessEdgeSegment(DECAL_DEBUG_NAME_ONLY(const char *debugName,) const decalSettings *settings, unsigned numBoneSettings, const decalTypeSettings *typeSettings, const decalRenderSettings *renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const Vec3V *msOffsetFlt, EdgeGeomPpuConfigInfo *ppuConf, DECAL_BONEREMAPLUT(Edge,Loop) edgeToLoopLut, bool isGlass, decalMiniIdxBufWriter* miniIdxBufWriter);
#endif
		void ProcessGeometry(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, const u16 *pIndexPtrEa, unsigned idxCount, const void *pVertexPtrEa, unsigned vertexStride, const grcFvf *pVertexFormatLs, bool isGlassShader, decalMiniIdxBufWriter* miniIdxBufWriter);
		void ProcessSkinnedGeometry(DECAL_DEBUG_NAME_ONLY(const char *debugName,) const decalSettings* boneSettings, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const Vec3V *msOffsetFlt, const u16* pIndexPtrEa, unsigned idxCount, const void* pVertexPtrEa, unsigned vertexStride, const grcFvf* pVertexFormatLs, DECAL_BONEREMAPLUT(Vertex,Loop) vertexToLoopLut, bool isGlassShader, decalMiniIdxBufWriter* miniIdxBufWriter);
		void ProcessDrawable(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, const rmcDrawable* pDrawable, unsigned drawableIdx, bool isDamaged, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8 *skelDrawableIdxs, DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut, decalMiniIdxBufWriter* miniIdxBufWriter);
		void ProcessSkinnedDrawable(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, const rmcDrawable* pDrawable, unsigned drawableIdx, bool isDamaged, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8 *skelDrawableIdxs, const Mat34V *skelInvJoints, DECAL_BONEREMAPLUT(Render,Hierarchy,u16) renderToHierarchyLut, decalMiniIdxBufWriter* miniIdxBufWriter
#		if USE_EDGE && __PPU
			, decalSettings *boneSettings
#		endif
			);
		void ProcessBound(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, const fwEntity* pEntity, phInst* pPhInst, const phBound* pBound, Mat34V_In boundWldMtx, s16 childId, bool isBreakable, bool isBrokenBreakable, decalMiniIdxBufWriter* miniIdxBufWriter);
#if DECAL_PROCESS_BREAKABLES
		void ProcessBreakable(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, bgBreakable* pBreakable, decalMiniIdxBufWriter* miniIdxBufWriter);
#endif
		void ProcessBoundPoly(DECAL_DEBUG_NAME_ONLY(const char *debugName,) const decalSettings* settings, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, const phBoundGeometry* polyhedron, decalVertexCache* vertexCache, const phPolygon* polygon, unsigned polygonIndex, bool processDoubleSided, decalMiniIdxBufWriter* miniIdxBufWriter);

		void ProcessSmashGroup(DECAL_DEBUG_NAME_ONLY(const char *debugName,) decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, decalMiniIdxBufWriter* miniIdxBufWriter);

		void OutputVertex(decalVtx *pVtxLs SPU_ONLY(, decalVtx *pVtxEa, u32 dmaTag));
		void ProcessTri(const decalSettings* settings, const decalTypeSettings* typeSettings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, Vec3V_In vtx1, Vec3V_In vtx2, Vec3V_In vtx3, Vec3V_In nrm1, Vec3V_In nrm2, Vec3V_In nrm3, ScalarV_In vehDmgScale1, ScalarV_In vehDmgScale2, ScalarV_In vehDmgScale3, bool isGlass, decalMiniIdxBufWriter* miniIdxBufWriter);
		void ProcessClippedPolygon(const decalSettings* settings, const decalRenderSettings* renderSettings, ScalarV_In posScale, ScalarV_In invPosScale, Vec3V_In msOffsetFlt, unsigned numVerts, const Vec3V* pVerts, const Vec3V* pNorms, const ScalarV* pVehDmgScales, bool isGlass, decalMiniIdxBufWriter* miniIdxBufWriter);

		void CalcTexCoords(const decalSettings* settings, const decalRenderSettings* renderSettings, Vec3V_In vPos, Vec2V_InOut vTexCoords, ScalarV_InOut vFrontToBackRatio);
		Vec4V_Out CalcTangent(Vec3V_In posA, Vec3V_In posB, Vec3V_In posC, Vec3V_In n, Vec2V_In texCoordsA, Vec2V_In texCoordsB, Vec2V_In texCoordsC);
		bool ShouldProcessDrawableBone(decalBoneHierarchyIdx lodAttachMatrixId, const fwEntity* pEntity, decalBoneHierarchyIdx boneIdx);
		bool ShouldProcessDrawableBone(const decalAsyncTaskDescBase* taskDescLs, const fwEntity* pEntity, decalBoneHierarchyIdx boneIdx);
		bool ShouldProcessShader(const grmShader* pShader, const fwEntity* pEntity, bool isGlassShader, bool onlyApplyToGlass, bool dontApplyToGlass);

		// pool access
		bool GetInsts(decalInst** ppInstGlass, decalInst** ppInstNonGlass);
		decalBucket* GetBucket(unsigned bucketLists, decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, ScalarV_In posScale, Vec3V_In msOffsetInt, Vec4V_In boundSphere, decalRenderPass renderPass, decalMethod method, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, decalAttachType attachType, Mat34V_In wldMtx, s16 attachMatrixId, void* pSmashGroup, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted);

		int AllocDecalId();
		void FreeDecalId(int decalId);
		void AddDecalIdInstance(int decalId);
		void RemoveDecalIdInstance(int decalId);

		decalBucket* AddInstToBucket(const decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, unsigned bucketLists, const decalSettings* settings, ScalarV_In posScale, Vec3V_In msOffsetInt, decalInst* pDecalInstLs, decalInst* pDecalInstEa, Mat34V_In wldMtx, s16 attachMatrixId, bool isGlassInst, bool isDamaged SPU_ONLY(, decalMiniIdxBufWriter* miniIdxBufWriter));

		decalBucket* AddInstToBucket(const decalAsyncTaskDescBase* taskDescLs, decalAsyncTaskDescBase* taskDescEa, unsigned bucketLists, const decalSettings* settings, ScalarV_In posScale, Vec3V_In msOffsetInt, decalInst* pDecalInstLs, decalInst* pDecalInstEa, Mat34V_In wldMtx, decalBoneHierarchyIdx attachMatrixId, bool isGlassInst, bool isDamaged SPU_ONLY(, decalMiniIdxBufWriter* miniIdxBufWriter))
		{
			CompileTimeAssert(sizeof(attachMatrixId) == 1);
			const s16 mtxId = (s16)(s8)(attachMatrixId.Val());
			return AddInstToBucket(taskDescLs, taskDescEa, bucketLists, settings, posScale, msOffsetInt, pDecalInstLs, pDecalInstEa, wldMtx, mtxId, isGlassInst, isDamaged SPU_ONLY(, miniIdxBufWriter));
		}


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	protected: ////////////////////////

#		if __BANK
			decalDebug                  m_debug;
#		endif

		decalPools                  m_pools;

		// To speed up decalInst shutdown, each decalInst has a linked list of
		// vertices associated with it (rather than having to sort and uniqify
		// the index buffer).  Linked list node storage is external to the
		// decalVtx since we don't want this to be polluting the GPU caches.
		// m_allocatedVertices is a pointer to an array of DECAL_MAX_VERTS
		// 16-bit indices.  This array is indexed using the vertex index.  The
		// value in an array element is the next vertex index in the linked
		// list.  Linked lists are terminated with a 0xffff.
		u16*                        m_allocatedVertices;

#		if __ASSERT && USE_DEFRAGMENTATION
			// The asynchronous code needs access to this in assert enabled
			// builds.  In assert disabled builds, it can live here instead.
			decalDefragLocker           m_defragLocker;
#		endif

		// active bucket lists
		AsyncSList<decalBucket>     m_pendingBucketLists[DECAL_NUM_RENDER_PASSES][DECAL_NUM_METHODS];

#		if USE_EDGE
			// static, as this is pointless to dma up to the spus
			static decalEdgeClipInfo    ms_edgeClipInfo;
#		endif

#		if __PS3
			// so that spu code can access the main memory copy
			decalManagerAsync*          m_thisEa;
#		endif

#		if RSG_PC
			// Ring buffer of indices for vertices that need to be uploaded to
			// the GPU's copy of the vertex buffer.  But not implemented using
			// the typical put/get style indices.  Instead it is implemented
			// with a space remaining counter.  Because we are allowing the ring
			// buffer to become full, the space remaining counter simplifies
			// thing a lot.
			u16                         m_gpuUploadIndices[0x400];
			u32                         m_gpuUploadWorkerThreadsSubFrom;
			s32                         m_gpuUploadWorkerThreadsSpaceRemaining;
#		endif

#		if __BANK
			float                       m_modelSpaceOffsetUnits;
#		endif

		// Callback for higher level code to be informed when a decal id is removed.  Used by Replay.
		DecalRemovedCallback *m_decalRemovedCallback;

		// O(1) check to see if a decal is "alive".  There can never be more
		// than DECAL_MAX_INSTS decal ids alive at one time.  So we can use this
		// fact to create a fast and reasonably sized data structure.  A decal
		// id is a 32-bit value, the most significant 16-bits store an index
		// (plus 1) into these arrays, and the least significant 16-bits stores
		// a sequence number.  Zero is a reserved value to indicate "no decal
		// id", hence the +1 for the index to prevent zero ever being generated.
		//
		// To test if a decal id is still alive, the sequence number embedded in
		// the decal id is checked against the sequence number stored in the
		// corresponding index of m_decalIdAlive.
		//
		// When decal instances are created, they increment the counter in the
		// m_decalIdCountFree array.  When instances are destroyed, they
		// decrement it.  When the counter is decremented to zero, the id is no
		// longer alive, so the sequence number is incremented.
		//
		// The m_decalIdCountFree array also doubles as a linked list of free
		// decal ids.
		//
		u16                 m_decalIdAlive[DECAL_MAX_INSTS];
		u16                 m_decalIdCountFree[DECAL_MAX_INSTS];
		volatile u32        m_decalIdSeqFree;    // 16-msb, ABA sequence number, 16-lsb linked list head of free entries in m_decalIdNext


	private: //////////////////////////

		static decalManagerAsync   *ms_instance;
};


#define DECALMGRASYNC (::rage::decalManagerAsync::GetInstance())


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALMANAGERASYNC_H
