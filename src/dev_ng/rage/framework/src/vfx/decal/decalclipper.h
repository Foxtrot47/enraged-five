//
// vfx/decal/decalclipper.h
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALCLIPPER_H
#define	VFX_DECALCLIPPER_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "vectormath/classes.h"


// includes(framework)


// namespaces 
namespace rage {


// forward declarations	


// defines											
#define DECAL_MAX_CLIPPED_VERTS		(3+DECAL_NUM_PLANES)


// enumerations
enum decalPlaneId
{
	DECAL_PLANE_RIGHT				= 0,
	DECAL_PLANE_LEFT,
	DECAL_PLANE_BOTTOM,
	DECAL_PLANE_TOP,
	DECAL_PLANE_FRONT,
	DECAL_PLANE_BACK,

	DECAL_NUM_PLANES
};

enum decalJoinType
{
	DECAL_JOINTYPE_NONE		= 0,
	DECAL_JOINTYPE_FRONT,
	DECAL_JOINTYPE_BACK,
	DECAL_JOINTYPE_FRONT_CROSS,
	DECAL_JOINTYPE_BACK_CROSS
};


// classes
class decalClipper
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void Init(Vec3V_In vPosition, Vec3V_In vDirection, Vec3V_In vSide, Vec3V_In vForward, Vec3V_In vDimensions, Vec3V_Ptr pPassedJoinVerts, Vec3V_Ptr pReturnedJoinVerts, decalJoinType& joinType);
		
		void SetPlaneEquation(decalPlaneId planeId, Vec4V_In vPlaneEq);
		void SetPlaneEquation(decalPlaneId planeId, Vec3V_In vPlaneNormal, float planeD);

		Vec4V_ConstRef GetPlaneEquation(decalPlaneId planeId) const;
		Vec4V_Ptr GetPlaneEquationsPtr();

		void ClipPoly(int& numVerts, Vec3V* pVerts, Vec3V* pNorms, ScalarV* pVehDmgScales) const;

#if __BANK && !__SPU
		void RenderDebug(Vec3V_In vPosition, float range) const;
#endif

	private: //////////////////////////

		bool CalcJoinPlanes(Vec3V_In vPosition, /*Vec3V_In vDirection, Vec3V_In vSide, Vec3V_In vForward,*/ Vec3V_In vDimensions, Vec3V_Ptr pPassedJoinVerts, Vec3V_Ptr pReturnedJoinVerts, decalJoinType& joinType);
		void CalcPlaneEquation(Vec3V_InOut vPlaneNormal, float& planeD, Vec3V_In vSideABottom, Vec3V_In vSideATop, Vec3V_In vSideBBottom, Vec3V_In vSideBTop, int id);
		void CalcPlaneEquation(Vec4V_InOut vPlaneEq, Vec3V_In vSideABottom, Vec3V_In vSideATop, Vec3V_In vSideBBottom, Vec3V_In vSideBTop, int id);
		void AlignVertsToPlane(Vec4V_InOut vPlaneEq, Vec3V_InOut vSideABottom, Vec3V_InOut vSideATop, Vec3V_InOut vSideBBottom, Vec3V_InOut vSideBTop);

#if __BANK && !__SPU
		void RenderDebugPlane(decalPlaneId planeId, Vec3V_In vPosition, ScalarV_In vSize) const;
#endif


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		Vec4V m_planeEquations[DECAL_NUM_PLANES];

};


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALCLIPPER_H
