//
// vfx/decal/decalrenderstaticbatcher.h
//
// Copyright (C) 2013-2013 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALRENDERSTATICBATCHER_H
#define	VFX_DECALRENDERSTATICBATCHER_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "grcore/config.h"

// includes (framework)
#include "vfx/decal/decalmanager.h"
#define  VFX_DECAL_IS_COMPILING_SHADER 0
#include "vfx/decal/decalperinstcbuf.h"


// namespaces
namespace rage {


// forward declarations
class  decalMiniIdxBuf;
class  decalShader_Static;


// classes
class decalRenderStaticBatcher
{
public:

	explicit decalRenderStaticBatcher(decalShader_Static *shader DECAL_MULTI_THREADED_RENDERING_ONLY(, unsigned ctxIdx));

	~decalRenderStaticBatcher();

	void SetPerInstanceVars(const decalPerInstCBuf *vars);

	void RenderMib(const decalMiniIdxBuf *mib);


private:

	void PerBucketState(DECAL_MULTI_THREADED_RENDERING_ONLY(unsigned ctxIdx));

	void StartNewBatch();

	void FinishCurrentBatch();


	decalShader_Static *m_shader;

#	if DECAL_MULTI_THREADED_RENDERING
		u16                *m_idxBuf;
		decalCbufIdx       *m_cbufIdxBuf;
		unsigned            m_drawStartIdxIdx;
		unsigned            m_drawEndIdxIdx;
		decalPerInstCBuf    m_constSets[256];
		u8                  m_constHashUsageIdx[256];
		unsigned            m_numUsedConstSets;
		unsigned            m_ctxIdx;
		u8                  m_currConstUsageIdx;

#	elif RSG_PS3
		decalPerInstCBuf m_currConsts;
		bool m_batchStarted;
		u32 *m_drawIndexArrayHeader;
		u32 *m_drawIndexArrayData;
		u32 *m_drawIndexArrayEnd;

#	elif RSG_XENON
		decalPerInstCBuf m_currConsts;
		unsigned m_drawStartIdxIdx;
		unsigned m_drawEndIdxIdx;

#	endif
};


} // namespace rage


#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALRENDERSTATICBATCHER_H
