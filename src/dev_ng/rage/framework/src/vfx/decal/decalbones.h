//
// vfx/decal/decalbones.h
//
// Copyright (C) 2013-2013 Rockstar North.  All Rights Reserved.
//

#ifndef VFX_DECALBONES_H
#define VFX_DECALBONES_H

namespace rage
{

// These dummy classes here (before the actual definitions) are just to allow
// Emacs/Vi/etc tags to find these types (which are "hidden" in the macros
// below).  Will probably help VisualAssist people too.
#ifdef NEVER_DEFINED
class decalBoneHierarchyIdx{};
class decalBoneRenderIdx{};
class decalBoneVertexIdx{};
class decalBoneTaskIdx{};
class decalBoneLoopIdx{};
class decalBoneEdgeIdx{};
#endif


#define DECAL_TYPE_SAFE_BONE_IDX(NAME)                                         \
	class decalBone##NAME##Idx                                                 \
	{                                                                          \
	public:                                                                    \
                                                                               \
		__forceinline decalBone##NAME##Idx()                                   \
		{                                                                      \
			/* Default ctor just leaves m_idx undefined */                     \
		}                                                                      \
                                                                               \
		explicit __forceinline decalBone##NAME##Idx(int idx)                   \
			: m_idx((u8)idx)                                                   \
		{                                                                      \
		}                                                                      \
                                                                               \
		__forceinline decalBone##NAME##Idx &operator=(int idx)                 \
		{                                                                      \
			m_idx = (u8)idx;                                                   \
			return *this;                                                      \
		}                                                                      \
                                                                               \
		__forceinline bool operator!() const                                   \
		{                                                                      \
			return !m_idx;                                                     \
		}                                                                      \
                                                                               \
		__forceinline bool operator==(const decalBone##NAME##Idx &rhs) const   \
		{                                                                      \
			return m_idx == rhs.m_idx;                                         \
		}                                                                      \
                                                                               \
		__forceinline bool operator!=(const decalBone##NAME##Idx &idx) const   \
		{                                                                      \
			return !operator==(idx);                                           \
		}                                                                      \
                                                                               \
		__forceinline bool operator==(int idx) const                           \
		{                                                                      \
			return m_idx == (u8)idx;                                           \
		}                                                                      \
                                                                               \
		__forceinline bool operator!=(int idx) const                           \
		{                                                                      \
			return !operator==(idx);                                           \
		}                                                                      \
                                                                               \
		__forceinline bool operator<(unsigned x) const                         \
		{                                                                      \
			return m_idx < x;                                                  \
		}                                                                      \
                                                                               \
		__forceinline bool operator>=(unsigned x) const                        \
		{                                                                      \
			return m_idx >= x;                                                 \
		}                                                                      \
                                                                               \
		__forceinline bool operator>(int x) const                              \
		{                                                                      \
			return (int)(s8)m_idx > x;                                         \
		}                                                                      \
                                                                               \
		__forceinline unsigned operator*(unsigned x) const                     \
		{                                                                      \
			return m_idx * x;                                                  \
		}                                                                      \
                                                                               \
		__forceinline unsigned operator+(unsigned x) const                     \
		{                                                                      \
			return m_idx + x;                                                  \
		}                                                                      \
                                                                               \
		__forceinline unsigned operator-(unsigned x) const                     \
		{                                                                      \
			return m_idx - x;                                                  \
		}                                                                      \
                                                                               \
		__forceinline decalBone##NAME##Idx &operator++()                       \
		{                                                                      \
			++m_idx;                                                           \
			return *this;                                                      \
		}                                                                      \
                                                                               \
		__forceinline decalBone##NAME##Idx operator++(int)                     \
		{                                                                      \
			const u8 prev = m_idx++;                                           \
			return decalBone##NAME##Idx(prev);                                 \
		}                                                                      \
                                                                               \
		__forceinline u8 Val() const                                           \
		{                                                                      \
			return m_idx;                                                      \
		}                                                                      \
                                                                               \
                                                                               \
	private:                                                                   \
                                                                               \
		u8  m_idx;                                                             \
	};                                                                         \
                                                                               \
	__forceinline bool operator==(int lhs, const decalBone##NAME##Idx &rhs)    \
	{                                                                          \
		return rhs == lhs;                                                     \
	}                                                                          \
                                                                               \
	__forceinline bool operator!=(int lhs, const decalBone##NAME##Idx &rhs)    \
	{                                                                          \
		return rhs != lhs;                                                     \
	}                                                                          \
                                                                               \
	struct DummyStructToEatSemiColon##NAME {}


// Bone index as specified in the skeleton hierarchy (crSkeleton, etc).
DECAL_TYPE_SAFE_BONE_IDX(Hierarchy);

// Index into the compacted matrix set passed to rendering.  This is not the
// actual index stored per vertex though.  For grmGeometryQB see
// decalBoneVertexIdx, and for PS3 grmGeometryEdge, see decalBoneEdgeIdx.
DECAL_TYPE_SAFE_BONE_IDX(Render);

// For grmGeometryQB, the value stored per vertex is a decalBoneVertexIdx.  This
// is mapped via a "matrix palette" (stored with the geometry), into a
// decalBoneRenderIdx.
DECAL_TYPE_SAFE_BONE_IDX(Vertex);

// When skeletons are passed from the main thread to the async decal tasks,
// unused bones are skipped.  This is a further reduced sub space of the render
// bone index space.
DECAL_TYPE_SAFE_BONE_IDX(Task);

// The asynchronous code for skinned geometry, cannot process all the task bone
// indices in one go, so it loops over groups of these.  The bones being
// processed in the current loop are in the loop bone index space, a sub space
// of the task bone index space.
DECAL_TYPE_SAFE_BONE_IDX(Loop);

#if __PS3
	// Edge compresses the render bone index space by removing the largest gap
	// of unused indices in the middle.  This mapping is specific to each
	// compressed segment.
	DECAL_TYPE_SAFE_BONE_IDX(Edge);
#endif

#undef DECAL_TYPE_SAFE_BONE_IDX



// The types above solve only half of the type safety problem.  Since we need to
// map between index spaces all the time, the following class adds type safety
// for the index space being mapped from.
template<class FROM, class TO, class STORAGE=TO> class decalBoneRemapLut
{
public:

	// This class needs to be able to be used inside of a union, so cannot have
	// constructors or assignment operators.  The derived class
	// decalBoneRemapLutCtor provides a constructor.

	__forceinline void SetPtr(const STORAGE *lut)
	{
		CompileTimeAssert(sizeof(STORAGE) >= sizeof(TO));
		m_lut = lut;
	}

	__forceinline const STORAGE *GetPtr() const
	{
		return m_lut;
	}

	__forceinline bool operator!() const
	{
		return !m_lut;
	}

	__forceinline TO operator[](const FROM &from) const
	{
		return (TO)m_lut[from.Val()];
	}


protected:

	const STORAGE *m_lut;
};


template<class FROM, class TO, class STORAGE=TO> class decalBoneRemapLutCtor : public decalBoneRemapLut<FROM,TO,STORAGE>
{
public:

	explicit decalBoneRemapLutCtor(const STORAGE *to)
	{
		decalBoneRemapLut<FROM,TO,STORAGE>::m_lut = to;
	}
};


template<class FROM, class TO> class decalBoneRemapLutMutable : public decalBoneRemapLutCtor<FROM,TO,TO>
{
public:

	explicit decalBoneRemapLutMutable(TO *to)
		: decalBoneRemapLutCtor<FROM,TO,TO>(to)
	{
	}

	// Hide base class SetPtr, so that arg must be non-const
	__forceinline void SetPtr(TO *lut)
	{
		decalBoneRemapLut<FROM,TO,TO>::m_lut = lut;
	}

	__forceinline const TO *GetPtr() const
	{
		return decalBoneRemapLut<FROM,TO,TO>::GetPtr();
	}

	__forceinline TO *GetPtr()
	{
		return const_cast<TO*>(const_cast<const decalBoneRemapLutMutable*>(this)->GetPtr());
	}

	__forceinline TO operator[](const FROM &from) const
	{
		return decalBoneRemapLut<FROM,TO,TO>::m_lut[from.Val()];
	}

	__forceinline TO &operator[](const FROM &from)
	{
		return const_cast<TO*>(decalBoneRemapLut<FROM,TO,TO>::m_lut)[from.Val()];
	}
};


// Helper macro to define the type of a lookup table from one bone indexing
// space to another.  Simply makes things more terse and a bit easier to read.
#define DECAL_BONEREMAPLUT(FROM, TO, ...)                                      \
	decalBoneRemapLut<decalBone##FROM##Idx, decalBone##TO##Idx, ##__VA_ARGS__>

// Same thing, but type with constructor.
#define DECAL_BONEREMAPLUT_CTOR(FROM, TO, ...)                                 \
	decalBoneRemapLutCtor<decalBone##FROM##Idx, decalBone##TO##Idx, ##__VA_ARGS__>

// Same thing, but mutable type.
#define DECAL_BONEREMAPLUT_MUTABLE(FROM, TO)                                   \
	decalBoneRemapLutMutable<decalBone##FROM##Idx, decalBone##TO##Idx>

// Helper macro to define an array at local scope that acts as a lookup table
// from one bone index space to another.
#define DECAL_BONEREMAPLUT_MUTABLE_ARRAY(STATIC, FROM, TO, NAME, SIZE)         \
	STATIC decalBone##TO##Idx NAME##Array[SIZE];                               \
	DECAL_BONEREMAPLUT_MUTABLE(FROM,TO) NAME(NAME##Array)


}
// namespace rage

#endif // VFX_DECALBONES_H
