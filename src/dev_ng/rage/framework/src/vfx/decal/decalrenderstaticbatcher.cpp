//
// vfx/decal/decalrenderstaticbatcher.cpp
//
// Copyright (C) 2013-2014 Rockstar North.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalrenderstaticbatcher.h"

// includes (rage)
#include "grcore/effect.h"
#include "grcore/device.h"
#include "grcore/gfxcontext.h"
#include "grcore/stateblock.h"
#include "grcore/vertexdecl.h"
#include "system/memops.h"
#if RSG_DURANGO
#	include "grcore/wrapper_d3d.h"
#	include <xdk.h>
#	include "system/d3d11.h"
#elif RSG_ORBIS
#elif RSG_PC && __D3D11
#	include "grcore/wrapper_d3d.h"
#	include <D3D11.h>
#elif RSG_PS3
#	include "grcore/wrapper_gcm.h"
#	include <cell/gcm/gcm_method_data.h>
#elif RSG_XENON
#	include "grcore/wrapper_d3d.h"
#endif

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalmanager.h"
#include "vfx/decal/decalminiidxbuf.h"
#include "vfx/decal/decalshader.h"
#include "vfx/decal/decalvertex.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


///////////////////////////////////////////////////////////////////////////////
//  CTOR
///////////////////////////////////////////////////////////////////////////////

decalRenderStaticBatcher::decalRenderStaticBatcher(decalShader_Static *shader DECAL_MULTI_THREADED_RENDERING_ONLY(, unsigned ctxIdx))
{
	PerBucketState(DECAL_MULTI_THREADED_RENDERING_ONLY(ctxIdx));

	m_shader = shader;

#	if RSG_DURANGO || RSG_ORBIS
		decalManager *const dm = &DECALMGR;
		m_drawStartIdxIdx = m_drawEndIdxIdx = dm->m_staticIdxBufLastAdded[ctxIdx];
		m_numUsedConstSets = 0;
		m_idxBuf = dm->m_staticIdxBufData[ctxIdx] + m_drawStartIdxIdx;
		m_cbufIdxBuf = dm->m_staticConstBufIdxData[ctxIdx];
		m_ctxIdx = ctxIdx;

#	elif RSG_PC && __D3D11
		m_drawStartIdxIdx = m_drawEndIdxIdx = DECALMGR.m_staticIdxBufLastAdded[ctxIdx];
		m_numUsedConstSets = 0;
		m_ctxIdx = ctxIdx;
		StartNewBatch();

#	elif RSG_PS3
		m_currConsts.Invalidate();
		m_batchStarted = false;

#	elif RSG_XENON
		m_currConsts.Invalidate();
		m_drawStartIdxIdx = m_drawEndIdxIdx = DECALMGR.m_staticIdxBufLastAdded;

#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  DTOR
///////////////////////////////////////////////////////////////////////////////

decalRenderStaticBatcher::~decalRenderStaticBatcher()
{
	FinishCurrentBatch();

#	if RSG_PS3
		InvalidateSpuGcmState(CachedStates.VertexFormats, ~0);
#	elif RSG_XENON
		DECALMGR.m_staticIdxBufLastAdded = m_drawEndIdxIdx;
#	elif RSG_DURANGO || RSG_ORBIS || (RSG_PC && __D3D11)
		DECALMGR.m_staticIdxBufLastAdded[m_ctxIdx] = m_drawEndIdxIdx;
#	endif
}


#if RSG_DURANGO || RSG_ORBIS || (RSG_PC && __D3D11)

	///////////////////////////////////////////////////////////////////////////
	//  HashConsts
	///////////////////////////////////////////////////////////////////////////

	static u8 HashConsts(const decalPerInstCBuf *vars)
	{
		const u8 *p = (u8*)vars;
		const u8 *const end = p + sizeof(*vars);
		u8 sum = 0;
		do
		{
			sum += *p++;
		}
		while (p < end);
		return sum;
	}

#endif


///////////////////////////////////////////////////////////////////////////////
//  SetPerInstanceVars
///////////////////////////////////////////////////////////////////////////////

void decalRenderStaticBatcher::SetPerInstanceVars(const decalPerInstCBuf *vars)
{
#	if RSG_DURANGO || RSG_ORBIS || (RSG_PC && __D3D11)
		// Here we use a hash table like structure to attempt to reuse
		// previously set constants.  Hash collisions are handled by simply
		// allocating a new entry in the constant buffer.  If we were unlucky
		// enough to continously switch back and fowrard between two different
		// constant sets that had a hash collision, then each usage would get a
		// new index.  But this seems so unlikely, that this scheme is a good
		// tradeoff, as the lookup is constant time, we never need to search
		// through consecutive table entries.
		const unsigned hash = HashConsts(vars);
		unsigned usageIdx = m_constHashUsageIdx[hash];
		if (usageIdx>=m_numUsedConstSets || memcmp(m_constSets+usageIdx, vars, sizeof(*vars))!=0)
		{
			usageIdx = m_numUsedConstSets++;
#			define MAX_USED_CONSTANST_SETS 0x100
#			if __D3D11
				CompileTimeAssert(MAX_USED_CONSTANST_SETS*sizeof(decalPerInstCBuf)>>4 <= D3D11_REQ_CONSTANT_BUFFER_ELEMENT_COUNT);
#			endif
			if (Unlikely(m_numUsedConstSets > MAX_USED_CONSTANST_SETS))
			{
				// Notice that m_numUsedConstSets has counted 1 too many at this
				// point.  Set back to MAX_USED_CONSTANST_SETS, since it is used
				// by FinishCurrentBatch, then after we start a new batch, we
				// reset it to 1 and usageIdx to 0.
				decalAssert(m_numUsedConstSets == MAX_USED_CONSTANST_SETS+1);
				m_numUsedConstSets = MAX_USED_CONSTANST_SETS;
				FinishCurrentBatch();
				StartNewBatch();
				usageIdx = 0;
				m_numUsedConstSets = 1;
			}
#			undef MAX_USED_CONSTANST_SETS
			sysMemCpy(m_constSets+usageIdx, vars, sizeof(*vars));
			m_constHashUsageIdx[hash] = (u8)usageIdx;
		}
		m_currConstUsageIdx = (u8)usageIdx;

#	elif RSG_PC && !__D3D11
		(void) vars;

#	elif RSG_PS3
		if (m_currConsts != *vars)
		{
			FinishCurrentBatch();

			// Set the per instance vertex constants
			CellGcmContextData *const ctx = GCM_CONTEXT;
			u32 *cmd = ctx->current;
			if (Unlikely(cmd+0xf > ctx->end))
			{
				ctx->callback(ctx, 0);
				cmd = ctx->current;
			}
			const unsigned reg = m_shader->GetPerInstVtxConstReg();
			cmd[0x0] = CELL_GCM_METHOD_HEADER_VERTEX_CONSTANT_LOAD(3);
			cmd[0x1] = CELL_GCM_METHOD_DATA_VERTEX_CONSTANT_LOAD(reg);
			vec_stvlx((vec_uint4)vars->constSet0,     0,  cmd+0x2);
			vec_stvrx((vec_uint4)vars->constSet0,     16, cmd+0x2);
			cmd[0x4] = CELL_GCM_METHOD_HEADER_VERTEX_CONSTANT_LOAD(4);
			cmd[0x5] = CELL_GCM_METHOD_DATA_VERTEX_CONSTANT_LOAD(reg+1);
			vec_stvlx((vec_uint4)vars->vertexColor,   0,  cmd+0x6);
			vec_stvrx((vec_uint4)vars->vertexColor,   16, cmd+0x6);
			cmd[0x9] = CELL_GCM_METHOD_HEADER_VERTEX_CONSTANT_LOAD(5);
			cmd[0xa] = CELL_GCM_METHOD_DATA_VERTEX_CONSTANT_LOAD(reg+2);
			vec_stvlx((vec_uint4)vars->texcoordConst, 0,  cmd+0xb);
			vec_stvrx((vec_uint4)vars->texcoordConst, 16, cmd+0xb);
			ctx->current = cmd + 0xf;

			m_currConsts = *vars;

			StartNewBatch();
		}

#	elif RSG_XENON
		if (m_currConsts != *vars)
		{
			FinishCurrentBatch();

			// Set the per instance vertex constants
			grcDeviceHandle *const devD3D9 = GRCDEVICE.GetCurrent();
			CHECK_HRESULT(devD3D9->SetVertexShaderConstantF(m_shader->GetPerInstVtxConstReg(), (const float*)vars, 3));

			m_currConsts = *vars;

			StartNewBatch();
		}

#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  RenderMib
///////////////////////////////////////////////////////////////////////////////

void decalRenderStaticBatcher::RenderMib(const decalMiniIdxBuf *mib)
{
#	if RSG_DURANGO || RSG_ORBIS || (RSG_PC && __D3D11)
		// As well as copying each index into the index buffer we will render
		// with, need to set the constant usage index for each vertex that is
		// indexed by the mib.
		u16 *idxBuf = m_idxBuf;
		decalCbufIdx *const cbufIdxBuf = m_cbufIdxBuf;
		const u8 constUsageIdx = m_currConstUsageIdx;
		const unsigned numIndices = mib->GetNumTris()*3;
		for (unsigned ii=0; ii<numIndices; ++ii)
		{
			const unsigned i = mib->m_idx[ii];
			cbufIdxBuf[i] = constUsageIdx;
			*idxBuf++ = (u16)i;
		}
		m_drawEndIdxIdx += numIndices;
		m_idxBuf = idxBuf;

#	elif RSG_PC && !__D3D11
		(void) mib;

#	elif RSG_PS3
		decalManager *const dm = &DECALMGR;
		const decalMiniIdxBuf *const mibBase = dm->m_pools.GetMibPoolBase();
		const unsigned first = mib->GetFirstIdxIdx(mibBase);
		const unsigned numIndices = mib->GetNumTris()*3;
		u32 *cmd = m_drawIndexArrayData;
		*cmd++ = CELL_GCM_METHOD_DATA_DRAW_INDEX_ARRAY(first, numIndices-1);

		// If we have reached the end of the method, patch up the header.
		CellGcmContextData *const ctx = GCM_CONTEXT;
		if (Unlikely(cmd >= m_drawIndexArrayEnd))
		{
			decalAssert(cmd == m_drawIndexArrayEnd);
			decalAssert(cmd > m_drawIndexArrayHeader+1);
			const unsigned count = cmd - m_drawIndexArrayHeader - 1;
			*m_drawIndexArrayHeader = CELL_GCM_METHOD_HEADER_DRAW_INDEX_ARRAY_NI(count);

			// Did the method end because the fifo segment ended.  Note that we
			// are adding one here in the condition to handle the case where we
			// actually finished the method because of hitting the max method
			// size, but there is not enough space to start a new method in this
			// fifo segment.
			if (Likely(cmd+1 >= ctx->end))
			{
				decalAssert(cmd <= ctx->end);
				ctx->current = cmd;
				ctx->callback(ctx, 0);
				m_drawIndexArrayHeader = ctx->current;
				cmd = ctx->current+1;
			}

			// If fifo segments are large enough, then the method could have
			// ended because it reached the max size.
			else
			{
				decalAssert(count == CELL_GCM_MAX_METHOD_COUNT);
				m_drawIndexArrayHeader = cmd;
				++cmd;
				decalAssert(cmd < ctx->end);
			}

			m_drawIndexArrayEnd = Min(ctx->end, cmd+CELL_GCM_MAX_METHOD_COUNT);
		}

		m_drawIndexArrayData = cmd;

#	elif RSG_XENON
		// While it may be tempting to always write
		// decalMiniIdxBuf::MAX_NUM_TRIS*3 indices (to get the compiler to
		// optimize for the compile time size memcpy), don't do this, as then
		// the write order is not ideal for the write combiners (index buffer is
		// allocated in WC memory).
		//
		// Note that because the index buffer is in WC memory, we cannot use
		// XMemCpy() and .: cannot use sysMemCpy().
		//
		decalManager *const dm = &DECALMGR;
		const unsigned frame = dm->m_staticIdxBufFrame;
		u16 *const idxBuf = dm->m_staticIdxBufData[frame] + m_drawEndIdxIdx;
		const unsigned numIndices = mib->GetNumTris()*3;
		memcpy(idxBuf, mib->m_idx, numIndices*2);
		m_drawEndIdxIdx += numIndices;

#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  PerBucketState
///////////////////////////////////////////////////////////////////////////////

void decalRenderStaticBatcher::PerBucketState(DECAL_MULTI_THREADED_RENDERING_ONLY(unsigned ctxIdx))
{
	decalManager *const dm = &DECALMGR;

	grcStateBlock::Flush();

#	if RSG_DURANGO
		grcVertexDeclaration *vtxDecl = dm->m_staticVtxDecl;
#	    if DECAL_STORE_UNCOMPRESSED_VERTICES
			if (decalVtx::UseUncompressedVertices())
			{
				vtxDecl = dm->m_debugStaticVtxDecl;
			}
#		endif
		GRCDEVICE.SetVertexDeclaration(vtxDecl);
		grcContextHandle *const ctxD3D11 = g_grcCurrentContext;
		GRCDEVICE.SetInputLayout();
		GRCDEVICE.SetProgramResources();
		ctxD3D11->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// Set the index buffer.
		GRCDEVICE.SetIndices(dm->m_staticIdxBufData[ctxIdx]);

		// Set the vertex buffers.
		GRCDEVICE.SetStreamSource(0, dm->m_pools.GetVtxPoolBase(),        sizeof(decalVtx));
		GRCDEVICE.SetStreamSource(1, dm->m_staticConstBufIdxData[ctxIdx], sizeof(decalCbufIdx));

#	elif RSG_ORBIS
		char *const baseAddr = (char*)dm->m_pools.GetVtxPoolBase();
		const unsigned stride = sizeof(decalVtx);
		sce::Gnm::Buffer vtxBufs[6];
#       define INIT_AS_VERTEX_BUFFER(SLOT, OFFSET, BUFFER_FORMAT, CHANNEL_TYPE) \
			initAsVertexBufferOptimized(                                        \
				vtxBufs[SLOT],                                                  \
				baseAddr+(OFFSET),                                              \
				stride,                                                         \
				DECAL_MAX_VERTS,                                                \
				AMDSISLANDS_VERTEX_BUFFER_RESOURCE_U32_3(CHANNEL_TYPE, BUFFER_FORMAT))
#		if DECAL_STORE_UNCOMPRESSED_VERTICES
			if (decalVtx::UseUncompressedVertices())
			{
				INIT_AS_VERTEX_BUFFER(0, 20, 32_32_32,    FLOAT);       // pos                          : position
				INIT_AS_VERTEX_BUFFER(1, 32, 32_32,       FLOAT);       // fbRatioAndVehDamageScale     : texcoord0
				INIT_AS_VERTEX_BUFFER(2, 40, 32_32,       FLOAT);       // texcoords                    : texcoord1
				INIT_AS_VERTEX_BUFFER(3, 48, 32_32_32,    FLOAT);       // normal                       : normal
				INIT_AS_VERTEX_BUFFER(4, 60, 32_32_32_32, FLOAT);       // tangent                      : tangent0
			}
			else
#		endif
			{
				INIT_AS_VERTEX_BUFFER(0, 0,  16_16_16_16, SNORM);       // pos                          : position
				INIT_AS_VERTEX_BUFFER(1, 6,  8_8,         UNORM);       // fbRatioAndVehDamageScale     : texcoord0
				INIT_AS_VERTEX_BUFFER(2, 8,  16_16,       FLOAT);       // texcoords                    : texcoord1
				INIT_AS_VERTEX_BUFFER(3, 12, 2_10_10_10,  UNORM);       // normal                       : normal
				INIT_AS_VERTEX_BUFFER(4, 16, 2_10_10_10,  UNORM);       // tangent                      : tangent0
			}
#		undef INIT_AS_VERTEX_BUFFER
		initAsVertexBufferOptimized(vtxBufs[5], dm->m_staticConstBufIdxData[ctxIdx],
			sizeof(decalCbufIdx), DECAL_MAX_VERTS, AMDSISLANDS_VERTEX_BUFFER_RESOURCE_U32_3(UINT, 8));
		const unsigned startSlot = 0;
		Assertf(grcVertexProgram::GetCurrent(), "no currently bound vs.\n");
		gfxc.setVertexBuffers(grcVertexProgram::GetCurrent()->GetGnmStage(), startSlot, NELEM(vtxBufs), vtxBufs);

		// On Orbis, grmShader::Bind doesn't actually do the final setting of
		// the vertex program like on other platforms, because the vertex
		// declaration is required to match up with the correct fetch shader.
		grcVertexProgram::GetCurrent()->Bind(dm->m_staticDummyVtxDecl);

		gfxc.setPrimitiveType(sce::Gnm::kPrimitiveTypeTriList);
		gfxc.setIndexBuffer(dm->m_staticIdxBufData[ctxIdx]);
		gfxc.setIndexSize(sce::Gnm::kIndexSize16);

#	elif RSG_PC && __D3D11
		(void) ctxIdx;
		grcVertexDeclaration *vtxDecl = dm->m_staticVtxDecl;
#	    if DECAL_STORE_UNCOMPRESSED_VERTICES
			if (decalVtx::UseUncompressedVertices())
			{
				vtxDecl = dm->m_debugStaticVtxDecl;
			}
#		endif
		GRCDEVICE.SetVertexDeclaration(vtxDecl);
		ID3D11DeviceContext *const ctxD3D11 = g_grcCurrentContext;
		GRCDEVICE.SetInputLayout();
		GRCDEVICE.SetProgramResources();
		ctxD3D11->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

#	elif RSG_PC && !__D3D11
		(void) dm;

#	elif RSG_PS3
		const unsigned freq = 0;
		const unsigned stride = sizeof(decalVtx);
		const u32 disabled = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(0, 0, 0, CELL_GCM_VERTEX_F);
		u32 *cmd = gcm::AllocateFifo_Secret(24);
		cmd[0]  = CELL_GCM_METHOD_HEADER_VERTEX_DATA_ARRAY_FORMAT(0, 16);
#		if DECAL_STORE_UNCOMPRESSED_VERTICES
			if (decalVtx::UseUncompressedVertices())
			{
				cmd[1] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 3, CELL_GCM_VERTEX_F);
				cmd[2] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 2, CELL_GCM_VERTEX_F);
				cmd[3] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 2, CELL_GCM_VERTEX_F);
				cmd[4] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 3, CELL_GCM_VERTEX_F);
				cmd[5] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 4, CELL_GCM_VERTEX_F);
			}
			else
#		endif
			{
				cmd[1] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 3, CELL_GCM_VERTEX_S1);
				cmd[2] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 2, CELL_GCM_VERTEX_UB);
				cmd[3] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 2, CELL_GCM_VERTEX_SF);
				cmd[4] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 1, CELL_GCM_VERTEX_CMP);
				cmd[5] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, stride, 1, CELL_GCM_VERTEX_CMP);
			}
		cmd[6]  = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_FORMAT(freq, 4, 1, CELL_GCM_VERTEX_CMP);
		cmd[7]  = disabled;
		cmd[8]  = disabled;
		cmd[9]  = disabled;
		cmd[10] = disabled;
		cmd[11] = disabled;
		cmd[12] = disabled;
		cmd[13] = disabled;
		cmd[14] = disabled;
		cmd[15] = disabled;
		cmd[16] = disabled;
		const u32 vbOffset = gcm::MainOffset(dm->m_pools.GetVtxPoolBase());
		cmd[17] = CELL_GCM_METHOD_HEADER_VERTEX_DATA_ARRAY_OFFSET(0, 6);
#		if DECAL_STORE_UNCOMPRESSED_VERTICES
 			if (decalVtx::UseUncompressedVertices())
 			{
 				cmd[18] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+20);
 				cmd[19] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+32);
 				cmd[20] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+40);
 				cmd[21] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+48);
 				cmd[22] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+60);
 			}
 			else
#		endif
 			{
 				cmd[18] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset);
 				cmd[19] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+6);
 				cmd[20] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+8);
 				cmd[21] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+12);
 				cmd[22] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_MAIN, vbOffset+16);
 			}
    	cmd[23] = CELL_GCM_METHOD_DATA_VERTEX_DATA_ARRAY_OFFSET((u32)CELL_GCM_LOCATION_LOCAL, gcm::LocalOffset(dm->m_vehDamageOffset[dm->m_vehDamageFrame]));

#	elif RSG_XENON
		grcVertexDeclaration *vtxDecl = dm->m_staticVtxDecl;
#	    if DECAL_STORE_UNCOMPRESSED_VERTICES
			if (decalVtx::UseUncompressedVertices())
			{
				vtxDecl = dm->m_debugStaticVtxDecl;
			}
#		endif
		GRCDEVICE.SetVertexDeclaration(vtxDecl);
		grcDeviceHandle *const devD3D9 = GRCDEVICE.GetCurrent();
		devD3D9->SetIndices(dm->m_staticIdxBufHeader+dm->m_staticIdxBufFrame);
		devD3D9->SetStreamSource(0, dm->m_staticVtxBuf, 0, sizeof(decalVtx));

#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  StartNewBatch
///////////////////////////////////////////////////////////////////////////////

void decalRenderStaticBatcher::StartNewBatch()
{
#	if RSG_PC && __D3D11
		PF_AUTO_PUSH_TIMEBAR_BUDGETED("StartNewBatch", 0.4f);
		decalManager *const dm = &DECALMGR;
		ID3D11DeviceContext *const ctxD3D11 = g_grcCurrentContext;

		// If this is the first time we are locking the index and vertex buffers
		// this context (ie, m_drawEndIdxIdx==0), then map with "discard", else
		// map with "no overwrite".  Assuming the driver is implemented
		// sensibly, this map operation "should" be non-blocking and should not
		// involve any new memory allocations.
		//
		// Note that this stupid D3D11 requirement only applies to deferred
		// contexts.  Since we currently have MTR disabled on PC, that means we
		// only ever render to the immediate context, and .: can always use
		// no-overwrite.  Test MULTIPLE_RENDER_THREADS rather than
		// DECAL_MULTI_THREADED_RENDERING, since we want this optimization on
		// whenever possible (helps a lot with AMD drivers, see
		// url:bugstar:2158654).
		//
#		if MULTIPLE_RENDER_THREADS
			const D3D11_MAP mapType = m_drawEndIdxIdx ? D3D11_MAP_WRITE_NO_OVERWRITE : D3D11_MAP_WRITE_DISCARD;
#		else
			const D3D11_MAP mapType = D3D11_MAP_WRITE_NO_OVERWRITE;
#		endif
		const UINT subresource = 0;
		const UINT mapFlags = 0;
		const unsigned ctxIdx = m_ctxIdx;

		// Map the index buffer.
		ID3D11Buffer *const idxBuf = dm->m_staticIdxBufHeader[ctxIdx];
		D3D11_MAPPED_SUBRESOURCE mappedIdxBuf;
		CHECK_HRESULT(ctxD3D11->Map(idxBuf, subresource, mapType, mapFlags, &mappedIdxBuf));
		decalAssert(mappedIdxBuf.pData);
		m_idxBuf = (u16*)(mappedIdxBuf.pData) + m_drawEndIdxIdx;

		// Map the vertex buffer that indexes into the constant buffer.
		ID3D11Buffer *const cbufIdxBuf = dm->m_staticConstBufIdxHeader[ctxIdx];
		D3D11_MAPPED_SUBRESOURCE mappedCbufIdxBuf;
		CHECK_HRESULT(ctxD3D11->Map(cbufIdxBuf, subresource, mapType, mapFlags, &mappedCbufIdxBuf));
		decalAssert(mappedCbufIdxBuf.pData);
		m_cbufIdxBuf = (decalCbufIdx*)(mappedCbufIdxBuf.pData);

#	elif RSG_PS3
		// Make sure we have enough command buffer space to start the draw call,
		// and have at least one draw index array.
		CellGcmContextData *const ctx = GCM_CONTEXT;
		u32 *cmd = ctx->current;
		u32 *end = ctx->end;
		if (Unlikely(cmd+9 > end))
		{
			ctx->callback(ctx, 0);
			cmd = ctx->current;
			end = ctx->end;
		}

		decalManager *const dm = &DECALMGR;
		const decalMiniIdxBuf *const mibBase = dm->m_pools.GetMibPoolBase();
		const u32 ibOffset = gcm::MainOffset(mibBase);
		cmd[0] = CELL_GCM_METHOD_HEADER_INVALIDATE_VERTEX_FILE(1);
		cmd[1] = CELL_GCM_METHOD_DATA_INVALIDATE_VERTEX_FILE;
		cmd[2] = CELL_GCM_METHOD_HEADER_INDEX_ARRAY_OFFSET(2);
		cmd[3] = CELL_GCM_METHOD_DATA_INDEX_ARRAY_OFFSET(ibOffset);
		cmd[4] = CELL_GCM_METHOD_DATA_INDEX_ARRAY_FORMAT(CELL_GCM_LOCATION_MAIN, CELL_GCM_DRAW_INDEX_ARRAY_TYPE_16);
		cmd[5] = CELL_GCM_METHOD_HEADER_BEGIN_END(1);
		cmd[6] = CELL_GCM_METHOD_DATA_BEGIN_END(CELL_GCM_PRIMITIVE_TRIANGLES);
		m_drawIndexArrayHeader = cmd+7;
		m_drawIndexArrayData   = cmd+8;
		m_drawIndexArrayEnd    = Min(ctx->end, cmd+8+CELL_GCM_MAX_METHOD_COUNT);

		m_batchStarted = true;

#	endif
}


///////////////////////////////////////////////////////////////////////////////
//  FinishCurrentBatch
///////////////////////////////////////////////////////////////////////////////

void decalRenderStaticBatcher::FinishCurrentBatch()
{
#	if RSG_DURANGO
		const unsigned drawStartIdxIdx = m_drawStartIdxIdx;
		const unsigned drawEndIdxIdx = m_drawEndIdxIdx;
		if (drawStartIdxIdx != drawEndIdxIdx)
		{
			grcContextHandle *const ctxD3D11 = g_grcCurrentContext;

			decalAssert(m_numUsedConstSets);
			const unsigned size = m_numUsedConstSets*sizeof(decalPerInstCBuf);
			void *const ctmp = grcGfxContext::current()->allocateTemp(size, 16);
			sysMemCpy(ctmp, m_constSets, size);
			grcDevice::SetConstantBuffer<VS_TYPE>(ctxD3D11, VFX_DECAL_PER_INST_CBUF_SLOT, ctmp);

			const unsigned numIndices = drawEndIdxIdx - drawStartIdxIdx;

			// Draw the newly added indices.
			const INT baseVtx = 0;
			ctxD3D11->DrawIndexed(numIndices, drawStartIdxIdx, baseVtx);

			m_drawStartIdxIdx = drawEndIdxIdx;
		}

#	elif RSG_ORBIS
		const unsigned drawStartIdxIdx = m_drawStartIdxIdx;
		const unsigned drawEndIdxIdx = m_drawEndIdxIdx;
		if (drawStartIdxIdx != drawEndIdxIdx)
		{
			decalAssert(m_numUsedConstSets);
			const unsigned size = m_numUsedConstSets*sizeof(decalPerInstCBuf);
			void *const ctmp = gfxc.allocateFromCommandBuffer(size, sce::Gnm::kEmbeddedDataAlignment16);
			sysMemCpy(ctmp, m_constSets, size);
			sce::Gnm::Buffer buffer;
			grcGfxContext::initAsConstantBuffer(buffer, ctmp, size);

			const u32 numSlots = 1;
			Assertf(grcVertexProgram::GetCurrent(), "no currently bound vs.\n");
			gfxc.setConstantBuffers(grcVertexProgram::GetCurrent()->GetGnmStage(), VFX_DECAL_PER_INST_CBUF_SLOT, numSlots, &buffer);

			if (GRCDEVICE.NotifyDrawCall(0))
				gfxc.drawIndexOffset(drawStartIdxIdx, drawEndIdxIdx-drawStartIdxIdx);

			m_drawStartIdxIdx = drawEndIdxIdx;
		}

#	elif RSG_PC && __D3D11
		// Unmap the index buffer and constant buffer index vertex buffer.
		grcContextHandle *const ctxD3D11 = g_grcCurrentContext;
		decalManager *const dm = &DECALMGR;
		const unsigned ctxIdx = m_ctxIdx;
		ID3D11Buffer *const idxBuf = dm->m_staticIdxBufHeader[ctxIdx];
		ID3D11Buffer *const cbufIdxBuf = dm->m_staticConstBufIdxHeader[ctxIdx];
		const UINT subresource = 0;
		ctxD3D11->Unmap(idxBuf,     subresource);
		ctxD3D11->Unmap(cbufIdxBuf, subresource);
#		if __ASSERT
			m_idxBuf = NULL;
			m_cbufIdxBuf = NULL;
#		endif

		const unsigned drawStartIdxIdx = m_drawStartIdxIdx;
		const unsigned drawEndIdxIdx = m_drawEndIdxIdx;
		if (drawStartIdxIdx != drawEndIdxIdx)
		{
			PF_AUTO_PUSH_TIMEBAR_BUDGETED("FinishCurrentBatch", 0.4f);
			// Set the index buffer.
			const DXGI_FORMAT format = DXGI_FORMAT_R16_UINT;
			const UINT offset = 0;
			ctxD3D11->IASetIndexBuffer(idxBuf, format, offset);

			// Set the vertex buffers.
			ID3D11Buffer *const vbufs[2] = {dm->m_staticVtxBuf, cbufIdxBuf};
			const UINT startSlot = 0;
			const UINT numVBuffers = 2;
			static const UINT strides[2] = {sizeof(decalVtx), sizeof(decalCbufIdx)};
			static const UINT offsets[2] = {0, 0};
			ctxD3D11->IASetVertexBuffers(startSlot, numVBuffers, vbufs, strides, offsets);

			// Map the constant buffer with "discard" and copy our temp version
			// there.  The reason for the temp copy (ie, not simply mapping
			// during StartNewBatch()) is that there is no garuntees what kind
			// of memory the driver will return, so reading from it in
			// SetPerInstanceVars() could be disasterously slow.
			ID3D11Buffer *const cbuf = dm->m_staticConstBuf;
			D3D11_MAPPED_SUBRESOURCE mappedCbuf;
			const UINT mapFlags = 0;
			const HRESULT hr = ctxD3D11->Map(cbuf, subresource, D3D11_MAP_WRITE_DISCARD, mapFlags, &mappedCbuf);
			// Need to safely handle E_OUTOFMEMORY, but don't expect any other reasons for Map to fail.
			decalAssert(SUCCEEDED(hr) || hr==0x8007000e/*E_OUTOFMEMORY*/);
			if (SUCCEEDED(hr))
			{
				sysMemCpy(mappedCbuf.pData, m_constSets, m_numUsedConstSets*sizeof(decalPerInstCBuf));
				ctxD3D11->Unmap(cbuf, subresource);
				const UINT numCBuffers = 1;
					ctxD3D11->VSSetConstantBuffers(VFX_DECAL_PER_INST_CBUF_SLOT, numCBuffers, &cbuf);

				// Draw the newly added indices.
				const UINT startVtx = 0;
				ctxD3D11->DrawIndexed(drawEndIdxIdx-drawStartIdxIdx, drawStartIdxIdx, startVtx);
			}

			m_drawStartIdxIdx = drawEndIdxIdx;
		}

#	elif RSG_PS3
		if (m_batchStarted)
		{
			CellGcmContextData *const ctx = GCM_CONTEXT;
			decalAssert(m_drawIndexArrayData > m_drawIndexArrayHeader);
			const unsigned count = m_drawIndexArrayData - m_drawIndexArrayHeader - 1;
			u32 *cmd;
			if (Likely(count))
			{
				// Patch up the method header
				*m_drawIndexArrayHeader = CELL_GCM_METHOD_HEADER_DRAW_INDEX_ARRAY_NI(count);
				cmd = m_drawIndexArrayData;

				// Make sure we have enough command buffer space to finish the
				// draw.
				if (Unlikely(cmd+2 > ctx->end))
				{
					ctx->current = cmd;
					ctx->callback(ctx, 0);
					cmd = ctx->current;
				}
			}
			else
			{
				// Final method has no data, so lets reuse the method header.
				// This then garuntees we have enough space to finish the draw.
				cmd = m_drawIndexArrayHeader;
				decalAssert(cmd+2 <= ctx->end);
			}

			cmd[0] = CELL_GCM_METHOD_HEADER_BEGIN_END(1);
			cmd[1] = CELL_GCM_METHOD_DATA_BEGIN_END(0);
			ctx->current = cmd+2;
		}

#	elif RSG_XENON
		const unsigned drawStartIdxIdx = m_drawStartIdxIdx;
		const unsigned drawEndIdxIdx = m_drawEndIdxIdx;
		if (drawStartIdxIdx != drawEndIdxIdx)
		{
			grcDeviceHandle *const devD3D9 = GRCDEVICE.GetCurrent();
			devD3D9->DrawIndexedVertices(D3DPT_TRIANGLELIST, 0, drawStartIdxIdx, drawEndIdxIdx-drawStartIdxIdx);
			m_drawStartIdxIdx = drawEndIdxIdx;
		}

#	endif
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
