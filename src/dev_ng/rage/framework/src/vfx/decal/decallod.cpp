//
// vfx/decal/decallod.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decallod.h"

// includes (rage)

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decalmanager.h"
#include "vfx/decal/decalpools.h"
#include "vfx/decal/decalsettingsmanager.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces 
namespace rage {


// global variables
#if __DEV
int DECAL_MAX_INSTS_TO_LOD_PER_FRAME = 10;
int DECAL_MAX_INSTS_TO_UNLOD_PER_FRAME = 5;
#else
const int DECAL_MAX_INSTS_TO_LOD_PER_FRAME = 10;
const int DECAL_MAX_INSTS_TO_UNLOD_PER_FRAME = 5;
#endif


// code
void decalLodSystem::Update()
{
	// temporarily disable the lod system
// 	static bool disableUpdate = true;
// 	if (disableUpdate)
// 	{
// 		return;
// 	}

	decalManager *const dm = &DECALMGR;

#if __BANK
	if (dm->m_debug.m_disableLods)
	{
		return;
	}
#endif

	// look for any static decals that should become dynamic decals
	int numConversions = 0;
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		decalBucket** ppCurrBucket = dm->m_runningBucketLists[p][DECAL_METHOD_STATIC].GetPtrPtrHead();
		decalBucket* pCurrBucket = *ppCurrBucket;
		while (pCurrBucket)
		{
			decalBucket *const pNextBucket = pCurrBucket->GetNext();
			bool bucketRemoved = false;
			const bool forceRegen = pCurrBucket->GetForceRegen();
			if (!pCurrBucket->IsMarkedForDelete() && (forceRegen || CanLodBucket(pCurrBucket)))
			{
				const decalTypeSettings* pTypeSettings = decalSettingsManager::GetTypeSettings(pCurrBucket->m_typeSettingsIndex);

				// go through the insts in this bucket
				unsigned numInstInBucketConverted = 0;
				decalInst** ppDecalInst = pCurrBucket->m_instList.GetPtrPtrHead();
				decalInst* pDecalInst = *ppDecalInst;
				while (pDecalInst)
				{
					decalInst* pNextDecalInst = pDecalInst->GetNext();

					bool converted = false;
					if (!pDecalInst->IsMarkedForDelete() && !pDecalInst->m_isLod)
					{
#if __BANK
						if (dm->m_debug.m_forceDynamicLods)
						{
							if (ConvertToDynamicDecal(pDecalInst, pCurrBucket, (decalRenderPass)p))
							{
								*ppDecalInst = pNextDecalInst;
								converted = true;
								++numInstInBucketConverted;
							}
						}
						else if (dm->m_debug.m_forceStaticLods)
						{
							// do nothing
						}
						else
#endif
						{
							// look for an inst out of lod range
							if (forceRegen || pDecalInst->GetDistToViewport()>pTypeSettings->lodDistance)
							{
								if (ConvertToDynamicDecal(pDecalInst, pCurrBucket, (decalRenderPass)p))
								{
									*ppDecalInst = pNextDecalInst;
									converted = true;
									++numInstInBucketConverted;

									// check if we've done enough this frame
									numConversions++;
									if (numConversions>=DECAL_MAX_INSTS_TO_LOD_PER_FRAME)
									{
										break;
									}
								}
							}
						}
					}

					if (!converted)
					{
						ppDecalInst = pDecalInst->GetPtrPtrNext();
					}

					pDecalInst = pNextDecalInst;
				}

				if (numInstInBucketConverted)
				{
					pCurrBucket->m_instList.AddSize(-(int)numInstInBucketConverted);
					if (pCurrBucket->m_instList.IsEmpty())
					{
						*ppCurrBucket = pNextBucket;
						bucketRemoved = true;
						dm->m_pools.ReturnBucket(pCurrBucket);
						dm->m_runningBucketLists[p][DECAL_METHOD_STATIC].AddSize(-1);
					}
					else
					{
						pCurrBucket->RecalcBoundSphere();
					}
					if (numInstInBucketConverted>=(unsigned)DECAL_MAX_INSTS_TO_LOD_PER_FRAME)
					{
						goto done_convert_to_dynamic;
					}
				}
			}

			if (!bucketRemoved)
			{
				ppCurrBucket = pCurrBucket->GetPtrPtrNext();
			}
			pCurrBucket = pNextBucket;
		}
	}
done_convert_to_dynamic:

	// look for any dynamic decals that should become static decals
	numConversions = 0;
	for (int p=0; p<DECAL_NUM_RENDER_PASSES; p++)
	{
		decalBucket* pCurrBucket = dm->m_runningBucketLists[p][DECAL_METHOD_DYNAMIC].Peek();
		while (pCurrBucket)
		{
			if (!pCurrBucket->IsMarkedForDelete())
			{
				const decalTypeSettings* pTypeSettings = decalSettingsManager::GetTypeSettings(pCurrBucket->m_typeSettingsIndex);

				// go through the insts in this bucket
				unsigned numInstInBucketConverted = 0;
				decalInst* pDecalInst = pCurrBucket->m_instList.Peek();;
				while (pDecalInst)
				{
					if (!pDecalInst->IsPendingMarkedForDelete() && pDecalInst->m_isLod)
					{
						// Because there is a one frame delay in creating new
						// static decals, add a one frame delay before deleting
						// the current dynamic instance.
						const unsigned delayedDeleteNumFrames = 1;

#if __BANK
						if (dm->m_debug.m_forceStaticLods)
						{
							if (ConvertToStaticDecal(pDecalInst, pCurrBucket, (decalRenderPass)p))
							{
								pDecalInst->MarkForDelete(delayedDeleteNumFrames);
								++numInstInBucketConverted;
							}
						}
						else if (dm->m_debug.m_forceDynamicLods)
						{
							// do nothing
						}
						else
#endif
						{
							// look for an inst out of lod range
							if (pDecalInst->m_isLod && pDecalInst->GetDistToViewport()<pTypeSettings->lodDistance)
							{
								if (ConvertToStaticDecal(pDecalInst, pCurrBucket, (decalRenderPass)p))
								{
									pDecalInst->MarkForDelete(delayedDeleteNumFrames);
									++numInstInBucketConverted;
									++numConversions;
								}

								// check if we've done enough this frame
								if (numConversions>=DECAL_MAX_INSTS_TO_UNLOD_PER_FRAME)
								{
									break;
								}
							}
						}
					}

					pDecalInst = pDecalInst->GetNext();
				}

				if (numInstInBucketConverted)
				{
					pCurrBucket->RecalcBoundSphere();
					if (numConversions>=DECAL_MAX_INSTS_TO_UNLOD_PER_FRAME)
					{
						return;
					}
				}
			}

			pCurrBucket = pCurrBucket->GetNext();
		}
	}
}

bool decalLodSystem::ConvertToDynamicDecal(decalInst* pInst, decalBucket* pBucket, decalRenderPass renderPass)
{
	decalManager *const dm = &DECALMGR;

	// allocate a new instance
	decalInst *const pNewInst = dm->m_pools.GetInst();
	if (Unlikely(!pNewInst))
	{
		decalSpamf("decalLodSystem::ConvertToDynamicDecal ran out of instances\n");
		return false;
	}
	pNewInst->InitForLodReduction(pInst);

	// get a new bucket to add the inst to
	decalBucket *const pNewBucket = dm->SyncGetBucket(
		pNewInst,
		renderPass,
		DECAL_METHOD_DYNAMIC,
		pInst->GetPosition(),
		pBucket->m_typeSettingsIndex,
		pBucket->m_renderSettingsIndex,
		pBucket->GetPosScaleFloat(),
		pBucket->GetModelSpaceOffsetIntU32(),
		pBucket->m_subType,
		pBucket->m_attachType,
		pBucket->m_attachEntity,
		pBucket->m_wldMtx,
		pBucket->m_attachMatrixId,
		pBucket->m_pSmashGroup,
		pBucket->IsGlass(),
		pBucket->m_isDamaged,
		pBucket->m_forceRegen,
		pBucket->IsPersistent(),
		pBucket->GetInteriorLocation(),
		pBucket->IsScripted());
	if (Unlikely(!pNewBucket))
	{
		decalSpamf("decalLodSystem::ConvertToDynamicDecal ran out of buckets\n");
		ASSERT_ONLY(pNewInst->MarkForDelete();)
		pNewInst->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_DONT_RELEASE_DECAL_ID);
		dm->m_pools.ReturnInst(pNewInst);
		return false;
	}

	// clean up
	pInst->MarkForDelete();
	dm->DelayedDeletion(pInst);
	pBucket->RecalcBoundSphere();

	return true;
}

bool decalLodSystem::ConvertToStaticDecal(decalInst* pInst, decalBucket* pBucket, decalRenderPass renderPass, bool onlyApplyToGlass, bool dontApplyToGlass, bool applyToBreakables)
{
	return pBucket->ReApplyAsStaticDecal(pInst, pBucket->m_pSmashGroup, renderPass, pBucket->m_attachMatrixId, onlyApplyToGlass, dontApplyToGlass, applyToBreakables);
}

bool decalLodSystem::CanLodBucket(const decalBucket* pBucket)
{
	// Lodding/unlodding decals on skinned entities does not work correctly,
	// since multiple layers of decals build up if the decal is applied to
	// multiple bones.  Simplest solution, just disable when we are attached to
	// a skinned entity.
	if (pBucket->m_attachType==DECAL_ATTACH_TYPE_BONE_SKINNED)
	{
		return false;
	}

	// Do not allow lodding of rotate towards camera decals.
	const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(pBucket->m_renderSettingsIndex);
	if (pRenderSettings->shaderId==DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA)
	{
		return false;
	}

	const decalTypeSettings* pTypeSettings = decalSettingsManager::GetTypeSettings(pBucket->m_typeSettingsIndex);
	if (pTypeSettings->canMergeInsts==false && pBucket->IsGlass()==false && pBucket->m_pSmashGroup==NULL)
	{
		return true;
	}

	return false;
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
