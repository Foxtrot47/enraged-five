//
// vfx/decal/decaldebug.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALDEBUG_H
#define	VFX_DECALDEBUG_H

#if !__TOOL && !__RESOURCECOMPILER

// WARNING: This takes a lot of memory when enabled, should always be checked in disabled.
#define DECAL_STORE_UNCOMPRESSED_VERTICES   0//__BANK


#if __BANK


// includes (rage)
#include "vectormath/classes.h"
#include "vector/color32.h"

// includes (framework)
#include "vfx/decal/decalclipper.h"
#include "vfx/decal/decalsettings.h"


// namespaces 
namespace rage {


// forward declarations	
class bkGroup;


// defines
#define	DECAL_MAX_LAST_DECAL_BOXES			(128)


// structures
struct decalDebugBox
{
	Mat34V mtx;
	Vec3V vDimensions;
	Color32 colour;
	float axisScale;
};


// classes
class decalDebug
{
	friend class decalBucket;
	friend class decalInst;
	friend class decalLodSystem;
	friend class decalManager;
	friend class decalManagerAsync;
	friend class decalVtx;


	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void Init();
		void InitWidgets();
		void Render();

		// debug rendering
		void ResetLastDecalData() {m_numLastDecalBoxes = 0; m_lastDecalClipperRange = 0.0f;}
		void StoreLastDecalBox(Mat34V_In mtx, Vec3V_In bBoxMin, Vec3V_In bBoxMax, const Color32& colour, float axisScale=0.0f);
		void StoreLastDecalClipper(const decalClipper& clipper, Vec3V_In vPosition, float range);

		// debug toggles
		static void ForceRenderPassDeferredChanged();
		static void ForceRenderPassForwardChanged();
		static void ForceRenderPassExtra1Changed();
		static void ForceRenderPassExtra2Changed();
		static void ForceMethodStaticChanged();
		static void ForceMethodDynamicChanged();

		// lod toggles
		static void ForceStaticLodsChanged();
		static void ForceDynamicLodsChanged();

		// debug utils
		static void Wash();

		//
		bkGroup* GetGameGroup() {return m_pGameGroup;}

		bool IsEnableSpam() const {return m_enableSpam;}


	private: //////////////////////////

		void AddStatsWidgets(bkBank* pVfxBank, const char* pName, decalStats& stats);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		bkGroup* m_pGameGroup;

		// debug rendering
		int m_numLastDecalBoxes;
		decalDebugBox m_lastDecalBoxes[DECAL_MAX_LAST_DECAL_BOXES];
		decalClipper m_lastDecalClipper;
		float m_lastDecalClipperRange;
		Vec3V m_lastDecalClipperPosition;

		// debug info
		decalStats m_statsGlobal;
		decalStats m_statsRendered;
		bool m_enableSpam;

		// pipeline toggles
		bool m_forceRenderPassDeferred;
		bool m_forceRenderPassForward;
		bool m_forceRenderPassExtra1;
		bool m_forceRenderPassExtra2;
		bool m_forceMethodStatic;
		bool m_forceMethodDynamic;
		bool m_forceStaticDecalsOnBounds;
		bool m_disablePolyAngleTest;
		bool m_disableTriClipping;
		bool m_disableDrawableBoneGameTest;
		bool m_disableDrawableBoneBoundTest;
		bool m_disableMaterialTest;
		bool m_disableShaderTest;
		bool m_disableBoundTypeTest;
		bool m_disableLodSkeletonMapping;
		float m_overrideBoxDepth;
		float m_overrideMaxOverlayRadius;

		// debug
		bool m_disableAdding;
		bool m_disableAsync;
		bool m_disableSync;
		bool m_disableRendering;
		bool m_disableRenderCulling;
		bool m_disableLods;
		bool m_disableBucketSorting;
		bool m_disableMergingInsts;
		bool m_disableMergingVerts;
		bool m_forceStaticLods;
		bool m_forceDynamicLods;
		bool m_forceWashable;
#		if DECAL_STORE_UNCOMPRESSED_VERTICES
			bool m_useUncompressedVertices;
#		endif
#		if __PS3
			bool m_allowRsxLocalReads;
#		endif
		float m_modelSpaceOffsetUnits;
		float m_washAmount;

		// debug rendering
		int m_renderTimer;
		bool m_renderBoundsConsidered;
		bool m_renderPolysConsidered;
		bool m_renderPolysSentToClip;
		bool m_renderPolysClipped;
		bool m_renderLastDecal;
		bool m_renderBucketInfo;
		bool m_renderBucketSphere;
		bool m_renderInstBox;
		bool m_renderInstSphere;
		bool m_renderInstTris;
		bool m_bucketColourMode;

		int m_numInstsCreatedThisProjection;
		bool m_outputNumInstsCreatedThisProjection;

		int m_numSkinnedBoneMatricesThisProjection;
		bool m_outputNumSkinnedBoneMatricesThisProjection;

		int m_numPolysConsideredThisProjection;
		bool m_outputNumPolysConsideredThisProjection;

		int m_numPolysSentToClipThisProjection;
		bool m_outputNumPolysSentToClipThisProjection;

		int m_numPolysClippedThisProjection;
		bool m_outputNumPolysClippedThisProjection;

		bool m_outputTimeToProcessThisProjection;

		bool m_resetAll;

};


} // namespace rage


#endif // __BANK

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALDEBUG_H
