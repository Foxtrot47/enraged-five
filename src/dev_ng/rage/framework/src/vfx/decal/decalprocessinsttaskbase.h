//
// vfx/decal/decalprocessinsttaskbase.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// This file provides the basic template for the decal spu jobs.
// Game play side code just needs to implement
//
//  void DecalProcessInstGameInit(decalAsyncTaskDescBase *tdBase, const fwEntity *pEntity);
//

// Instead of usual include guards, just error out if we are included more than
// once, this file is not like a normal header than can be included anywhere.
#ifdef VFX_DECAL_DECALPROCESSINSTTASKBASE_H
#	error "vfx/decal/decalprocessinsttaskbase.h included multiple times"
#endif
#define VFX_DECAL_DECALPROCESSINSTTASKBASE_H

#if !__SPU
#	error "vfx/decal/decalprocessinsttaskbase.h only for use in spu code"
#endif

#define ENTRYPOINT                  ProcessInstTask
#define PASS_SPURS_ARGS             1

// Save as much code space as possible
#define SYS_DMA_NOTHING

// Unless we re-partition all the data some stuff is just never going to fit.
// Handle this gracefully rather than dying.
#define CUSTOM_EDGE_SAFE_MEMORY

#if !__FINAL
#	define SAFE_ALLOCA              1
#	define EDGE_GEOM_DEBUG
#endif

#if __ASSERT
#	undef  Assert
#	undef  Assertf
#	undef  AssertMsg
#	undef  AssertMsgf
#	undef  AssertVerify
#	undef  FastAssert
#	define STOP_WITH_LINE_NUMBER()          __asm__ __volatile__("stop %0"::"i"(__LINE__<=0x3fff?__LINE__:0x3fff))
	static inline __attribute__((/*__always_inline__,*/__format__(__printf__,1,2))) void EatVarArgs(const char *fmt,...){(void)fmt;}
	// Branch hint Likely to save code space
	//      we want
	//          brnz    register,.+8
	//          stop    __LINE__
	//
	//      not
	//          brz     register, assertion_failure
	//        return_from_assert:
	//          ...
	//        assertion_failure:
	//          stop    __LINE__
	//          br      return_from_assert
	//
#	define Assert(EXP)                      do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();}while(0)
#	define Assertf(EXP,FMT,...)             do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();EatVarArgs(FMT,##__VA_ARGS__);}while(0)
#	define AssertMsg(EXP,FMT,...)           do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();EatVarArgs(FMT,##__VA_ARGS__);}while(0)
#	define AssertMsgf(EXP,FMT,...)          do{if(Likely(!(EXP))) STOP_WITH_LINE_NUMBER();EatVarArgs(FMT,##__VA_ARGS__);}while(0)
#	define AssertVerify(EXP)                (EXP)
#	define FastAssert(EXP)                  Assert(EXP)

#	include "vfx/channel.h"
#	undef  decalAssert
#	undef  decalAssertf
#	undef  decalFatalAssert
#	undef  decalFatalAssertf
#	define decalAssert(EXP)                 Assert(EXP)
#	define decalAssertf(EXP,FMT,...)        Assertf(EXP,FMT,##__VA_ARGS__)
#	define decalFatalAssert(EXP)            FatalAssert(EXP)
#	define decalFatalAssertf(EXP,FMT,...)   FatalAssertf(EXP,FMT,##__VA_ARGS__)

	// We do want to keep artAssertf working properly though, so we need to
	// redefine it to something not based on Assertf.
#	include "diag/art_channel.h"
#	undef  artAssertf
#	define artAssertf(EXP,FMT,...)          do{if(Unlikely(!(EXP))){Errorf(FMT,##__VA_ARGS__);rage::AssertFailed("[art] "#EXP,__FILE__,__LINE__);}}while(0)
#endif

// includes (rage)
#include "math/random.h"
#include "phbound/bound.h"
#include "system/dma.h"
#include "system/memory.h"
#include "system/taskheaderspu.h"
#include "system/task_spu.h"
#include "system/spuscratch.h"

// includes (framework)
#include "vfx/decal/decalasynctaskdescbase.h"
#include "vfx/decal/decalbounds.h"
#include "vfx/decal/decaldmatags.h"

// Include all code we need (no seperate compile and link steps)
#include "edge/geom/edgegeom.cpp"
#include "edge/geom/edgegeom_decompress.cpp"
#include "edge/geom/edgegeom_masks.cpp"
#include "edge/geom/edgegeom_skin.cpp"
#include "fragment/typephysicslod.cpp"
#include "grcore/fvf.cpp"
#include "grcore/vertexbuffer_gcm.cpp"
#include "math/random.cpp"
#include "phbound/boundbvh.cpp"
#include "phbound/boundbox.cpp"
#include "system/cellsyncmutex.cpp"
#include "vfx/decal/decalbucket.cpp"
#include "vfx/decal/decalclipper.cpp"
#include "vfx/decal/decalhelper.cpp"
#include "vfx/decal/decalinst.cpp"
#include "vfx/decal/decalmanagerasync.cpp"
#include "vfx/decal/decalsettings.cpp"


// forward declare callbacks that need to be provided by game level code
void DecalProcessInstGameInit(decalAsyncTaskDescBase *tdBase, const fwEntity *pEntity);


namespace rage
{
	mthRandom g_DrawRand;
	u32 g_AllowVertexBufferVramLocks/*=0*/;
}


// _exit() required to prevent link errors.
extern "C" void _exit()
{
	ASSERT_ONLY(__debugbreak();)
}

// free() is getting linked in by what looks to be the pure virtual handlers.
// Create a stub version here to save some code space.
extern "C" void free(void*)
{
	ASSERT_ONLY(__debugbreak();)
}


void ProcessInstTask(sysTaskParameters &params, CellSpursJobContext2 *jobContext, CellSpursJob256 *job)
{
 	decalAsyncTaskDescBase *const descLs = (decalAsyncTaskDescBase*)params.Input.Data;
	decalAsyncTaskDescBase *const descEa = (decalAsyncTaskDescBase*)job->workArea.userData[0];
	sysScratchInit(params.Scratch.Data, params.Scratch.Size);

	Vec4V *skelObjMtxs=NULL;
	u8 *skelBones=NULL, *skelDrawableIdxs=NULL;
	phInst *pPhInst=NULL;
#	if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED || DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

		const unsigned numSkelMtxs = descLs->numSkelMtxs;
		if (numSkelMtxs)
		{
			// skelBones and skelDrawableIdxs
			const u32 skelBonesEa        = (u32)descLs->skelBones.GetPtr();
			const u32 skelDrawableIdxsEa = (u32)descLs->skelDrawableIdxs;
			if (skelDrawableIdxsEa)
			{
				// The drawables index array and the bone index array should
				// have been allocated consecutively in memory, so grab them
				// with a single dma get.
				decalAssert(skelDrawableIdxsEa+numSkelMtxs == skelBonesEa);
				const u32 size = ((skelBonesEa+numSkelMtxs+15)&~15) - (skelDrawableIdxsEa&~15);
				u8 *const alloc = (u8*)sysScratchAlloc(size);
				sysDmaGet(alloc, skelDrawableIdxsEa&~15, size, DMATAG_GET_SKELBONESDRAWABLES);
				skelDrawableIdxs = (u8*)((u32)alloc+(skelDrawableIdxsEa&15));
				skelBones = skelDrawableIdxs+numSkelMtxs;
			}
			else
			{
				const u32 size = ((skelBonesEa+numSkelMtxs+15)&~15) - (skelBonesEa&~15);
				u8 *const alloc = (u8*)sysScratchAlloc(size);
				sysDmaGet(alloc, skelBonesEa&~15, size, DMATAG_GET_SKELBONESDRAWABLES);
				skelDrawableIdxs = NULL;
				skelBones = (u8*)((u32)alloc+(skelBonesEa&15));
			}

			// skelObjMtxs
			const u32 skelObjMtxsEa = (u32)descLs->skelObjMtxs;
			const u32 size = numSkelMtxs*3*sizeof(Vec4V);
			skelObjMtxs = (Vec4V*)sysScratchAlloc(size);
			sysDmaGet(skelObjMtxs, skelObjMtxsEa, size, DMATAG_GET_SKELOBJMTXS);
		}

#	elif DECAL_MANAGER_ENABLE_PROCESSBOUND || DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS

		// phInst
		CompileTimeAssert(__alignof__(phInst) >= 16);
		pPhInst = (phInst*)sysScratchAlloc(descLs->phInstSize);
		sysDmaGet(pPhInst, (u32)descLs->pPhInst, descLs->phInstSize, DMATAG_GET_PHINST);

#	endif

	// fwEntity
	char *const pEntityBuf = (char*)sysScratchAlloc((descLs->entitySize+31)&~15);
	sysDmaGet(pEntityBuf, (u32)descLs->pEntity&~15, (descLs->entitySize+31)&~15, DMATAG_GET_ENTITY);
	const fwEntity *const pEntity = (fwEntity*)(pEntityBuf + ((u32)descLs->pEntity&15));

	// decalManagerAsync
	DMA_GET(decalManagerAsync, dm, descLs->decalMan, DMATAG_GET_DECALMAN);
	decalManagerAsync::SetInstance(const_cast<decalManagerAsync*>(dm));

	// Initialize game play level code
	sysDmaWaitTagStatusAll(1<<DMATAG_GET_ENTITY);
	DecalProcessInstGameInit(descLs, pEntity);

	// decalTypeSettings
	DMA_GET(decalTypeSettings, typeSettings, descLs->typeSettings, DMATAG_GET_TYPESETTINGS);

	// decalRenderSettings
	DMA_GET(decalRenderSettings, renderSettings, descLs->renderSettings, DMATAG_GET_RENDERSETTINGS);

	Mat34V *skelInvJoints=NULL;
	DECAL_BONEREMAPLUT_CTOR(Render,Hierarchy,u16) renderToHierarchyLut(NULL);
#	if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESNONSKINNED || DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

		const unsigned numRenderBones = descLs->numRenderBones;
		if (numRenderBones)
		{
			const u32 rtohEa = (u32)descLs->renderToHierarchyLut.GetPtr();
			const u32 begin = rtohEa & ~15;
			const u32 end = (rtohEa + numRenderBones*sizeof(u16) + 15) & ~15;
			const u32 size = end - begin;
			u16 *ls = (u16*)sysScratchAlloc(size);
			sysDmaGet(ls, begin, size, DMATAG_GET_RENDERTOHIERARCHYLUT);
			renderToHierarchyLut.SetPtr((const u16*)((u32)ls+(rtohEa&15)));
		}

#		if DECAL_MANAGER_ENABLE_PROCESSDRAWABLESSKINNED

			if (numSkelMtxs)
			{
				sysDmaWaitTagStatusAll(1<<DMATAG_GET_SKELBONESDRAWABLES);
				skelInvJoints = (Mat34V*)sysScratchAlloc(numSkelMtxs*sizeof(Mat34V));
				Mat34V *dst = skelInvJoints;
				const Mat34V *src = descLs->skelInvJoints;
				for (unsigned i=0; i<numSkelMtxs; ++i)
				{
					sysDmaGet(dst, (u32)(src+skelBones[i]), sizeof(Mat34V), DMATAG_GET_SKELINVJOINTS);
					++dst;
				}
			}

#		endif

#	elif DECAL_MANAGER_ENABLE_PROCESSBOUND || DECAL_MANAGER_ENABLE_PROCESSBREAKABLEGLASS

		// phArchetype
		sysDmaWaitTagStatusAll(1<<DMATAG_GET_PHINST);
		DMA_GET(phArchetype, archetype, pPhInst->GetArchetype(), DMATAG_GET_ARCHETYPE);
		pPhInst->SetUserData(const_cast<fwEntity*>(pEntity));
		pPhInst->SetLSArchetype(const_cast<phArchetype*>(archetype));

		// phBound
		sysDmaWaitTagStatusAll(1<<DMATAG_GET_ARCHETYPE);
		CompileTimeAssert(__alignof__(phBound) >= 16);
		char boundBuf[sizeof(DECAL_LARGEST_BOUNDS_TYPE)] __attribute__((__aligned__(16)));
		sysDmaGet(boundBuf, (u32)archetype->GetBound(), sizeof(boundBuf), DMATAG_GET_BOUND);
		*(const_cast<phArchetype*>(archetype)->GetBoundPtr()) = (phBound*)boundBuf;

#	endif

	// Wait for all remaining dma
	sysDmaWaitTagStatusAll(~(1<<CELL_SPURS_KERNEL_DMA_TAG_ID));

	// Go
	DECALMGRASYNC.ProcessInst(DECAL_DEBUG_NAME_ONLY(descLs->debugName,) descLs, descEa, typeSettings, renderSettings, pEntity, pPhInst, skelObjMtxs, DECAL_BONEREMAPLUT_CTOR(Task,Hierarchy)((const decalBoneHierarchyIdx*)skelBones), skelDrawableIdxs, skelInvJoints, renderToHierarchyLut);

	// Make sure we haven't still got any dma puts in flight
	sysDmaWaitTagStatusAll(~(1<<CELL_SPURS_KERNEL_DMA_TAG_ID));
}

#endif // !__TOOL && !__RESOURCECOMPILER
