//
// vfx/decal/decalvertex.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALVERTEX_H
#define	VFX_DECALVERTEX_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "math/amath.h"
#include "math/float16.h"
#include "system/floattoint.h"
#include "system/memops.h"
#include "vectormath/classes.h"
#include "vectormath/scalarv.h"
#include "vectormath/vec3v.h"
#include "vectormath/vec4v.h"
#include "vectormath/vectormath.h"

// includes (framework)
#include "vfx/slist.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decalsettings.h"


// namespaces
namespace rage {


// forward declarations


// classes
class decalVtx
{
	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

#		if !__SPU

			// In a seperate compilation unit to prevent circular header file
			// includes.  Debug only code, so performance is not that important.
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				static bool UseUncompressedVertices();
#			else
				static __forceinline bool UseUncompressedVertices() { return false; }
#			endif

#		endif // !__SPU


 		inline Vec3V_Out GetPos(ScalarV_In posScale, Vec3V_In msOffsetFlt) const
		{
			using namespace Vec;

#			if DECAL_STORE_UNCOMPRESSED_VERTICES && !__SPU
				if (UseUncompressedVertices())
				{
					return Vec3V(V4LoadUnalignedSafe<12>(uncompPos)) + msOffsetFlt;
				}
#			endif

			Vector_4V xyz = V4LoadUnalignedSafe<6>(&posX);
			xyz = V4UnpackLowGpuSignedShortToFloatAccurate(xyz);
			xyz = V4AddScaled(msOffsetFlt.GetIntrin128(), xyz, posScale.GetIntrin128());
			return Vec3V(xyz);
		}

		inline void SetPos(Vec3V_In val, ScalarV_In invPosScale, Vec3V_In msOffsetFlt)
		{
			using namespace Vec;

			Vector_4V xyz = val.GetIntrin128();
			xyz = V4Subtract(xyz, msOffsetFlt.GetIntrin128());

#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				sysMemCpy(uncompPos, &xyz, sizeof(uncompPos));
#			endif

			CompileTimeAssert(__alignof(decalVtx) >= 4);
			CompileTimeAssert((OffsetOf(decalVtx, posX) & 3) == 0);
			CompileTimeAssert(OffsetOf(decalVtx, posY) == OffsetOf(decalVtx, posX) + 2);
			CompileTimeAssert(OffsetOf(decalVtx, posZ) == OffsetOf(decalVtx, posX) + 4);

			xyz = V4Scale(xyz, invPosScale.GetIntrin128());
			xyz = V4PackFloatToGpuSignedShortAccurate(xyz);
			V4Store32<0>(&posX, xyz);
			V4Store16<2>(&posZ, xyz);

// 			Vector_4V posScale = V4Invert(invPosScale.GetIntrin128());
// 			Vector_4V test = GetPos(ScalarV_In(posScale), msOffsetFlt).GetIntrin128();
// 			Vector_4V diff = V4Abs(V4Subtract(test, val.GetIntrin128()));
// 			Vector_4V tolerance = V4Scale(posScale, V4VConstantSplat<FLOAT_TO_INT(0.5f/0x7fff)>());
// 			if (!V3IsLessThanOrEqualAll(diff, tolerance))
// 			{
// #				if !__SPU
// 					Printf(
// 						"ERROR POSITION 0x%08x 0x%08x 0x%08x\n"
// 						"this %p\n"
// 						"in  (%.3f, %.3f, %.3f)\n"
// 						"out (%.3f, %.3f, %.3f)\n"
// 						, ((u32*)&val)[0], ((u32*)&val)[1], ((u32*)&val)[2]
// 						, this
// 						, ((float*)&val)[0],  ((float*)&val)[1],  ((float*)&val)[2]
// 						, ((float*)&test)[0], ((float*)&test)[1], ((float*)&test)[2]);
// #				endif
// 				__debugbreak();
// 			}
		}

 		inline Vec3V_Out GetNormal() const
		{
			using namespace Vec;

			Vector_4V n = V4LoadLeft(&normal);
#			if __PS3 || __XENON
				n = V4UnpackNormFloats_11_11_10(n);
#			else
				n = V4UnpackNormFloats_10_10_10_X(n);
#			endif
			return Vec3V(n);
		}

		inline void SetNormal(Vec3V_In val)
		{
			using namespace Vec;

#			if DECAL_STORE_UNCOMPRESSED_VERTICES
#				if __WIN32PC || RSG_DURANGO
					const Vec3V half(V_HALF);
					const Vec3V uval = val * half + half;
#				else
					const Vec3V uval = val;
#				endif
				sysMemCpy(uncompNrm, &uval, sizeof(uncompNrm));
#			endif

#			if __PS3 || __XENON
				Vector_4V n = V4PackNormFloats_11_11_10(val.GetIntrin128());
#			else
				Vector_4V n = V4PackNormFloats_10_10_10_X(val.GetIntrin128());
#			endif
			V4Store32<0>(&normal, n);

// 			Vector_4V test = GetNormal().GetIntrin128();
// 			Vector_4V diff = V4Abs(V4Subtract(test, V4Clamp(val.GetIntrin128(), V4VConstant(V_NEGONE), V4VConstant(V_ONE))));
// 			Vector_4V tolerance = V4VConstantSplat<FLOAT_TO_INT(3e-3f)>();
// 			if (!V3IsLessThanAll(diff, tolerance))
// 			{
// #				if !__SPU
// 					Printf(
// 						"ERROR NORMAL 0x%08x 0x%08x 0x%08x\n"
// 						"this %p\n"
// 						"in  (%.3f, %.3f, %.3f)\n"
// 						"out (%.3f, %.3f, %.3f)\n"
// 						, ((u32*)&val)[0], ((u32*)&val)[1], ((u32*)&val)[2]
// 						, this
// 						, ((float*)&val)[0], ((float*)&val)[1], ((float*)&val)[2]
// 						, ((float*)&test)[0], ((float*)&test)[1], ((float*)&test)[2]);
// #				endif
// 				__debugbreak();
// 			}
		}

		inline ScalarV_Out GetFrontToBackRatio() const
		{
			using namespace Vec;

#			if __PS3
				Vector_4V signBit = V4VConstant(V_80000000);
				Vector_4V v = V4LoadLeft(&fbRatioAndBitangentFlip);
				v = V4Andc(v, signBit);
				v = V4UnpackLowUnsignedByte(v);
				v = V4UnpackLowUnsignedShort(v);
				v = V4IntToFloatRaw<0>(v);
				v = V4Scale(v, V4VConstantSplat<FLOAT_TO_INT(1.f/127.f)>());
				v = V4SplatX(v);
				return ScalarV(v);
#			else
				Vector_4V v = V4LoadLeft(&fbRatio);
				v = V4UnpackLowUnsignedByte(v);
				v = V4UnpackLowUnsignedShort(v);
				v = V4IntToFloatRaw<0>(v);
				v = V4Scale(v, V4VConstantSplat<FLOAT_TO_INT(1.f/255.f)>());
				v = V4SplatX(v);
				return ScalarV(v);
#			endif
		}

		inline void SetFrontToBackRatio(ScalarV_In val)
		{
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				sysMemCpy(&uncompFbr, &val, sizeof(uncompFbr));
#			endif

			using namespace Vec;
			Vector_4V v = val.GetIntrin128();
#			if !__PS3
				v = V4Scale(v, V4VConstantSplat<FLOAT_TO_INT(255.f)>());
				v = V4FloatToIntRaw<0>(v);
#				if __BE
					V4Store8<3>(&fbRatio, v);
#				else
					V4Store8<0>(&fbRatio, v);
#				endif
#			else
				Vector_4V signBit = V4VConstant(V_80000000);
				Vector_4V bitangentFlip = V4LoadLeft(&fbRatioAndBitangentFlip);
				bitangentFlip = V4InvertBits(bitangentFlip);
				v = V4Scale(v, V4VConstantSplat<FLOAT_TO_INT(127.f)>());
				v = V4FloatToIntRaw<24>(v);
				v = V4SelectFT(signBit, v, bitangentFlip);
				V4Store8<0>(&fbRatioAndBitangentFlip, v);
#			endif
		}

		inline ScalarV_Out GetVehicleDamageScale() const
		{
			using namespace Vec;
			Vector_4V v = V4LoadLeft(&vehicleDamageScale);
			v = V4UnpackLowUnsignedByte(v);
			v = V4UnpackLowUnsignedShort(v);
			v = V4IntToFloatRaw<0>(v);
			v = V4Scale(v, V4VConstantSplat<FLOAT_TO_INT(1.f/255.f)>());
			v = V4SplatX(v);
			return ScalarV(v);
		}

		inline void SetVehicleDamageScale(ScalarV_In val)
		{
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				sysMemCpy(&uncompVds, &val, sizeof(uncompVds));
#			endif

			using namespace Vec;
			Vector_4V v = val.GetIntrin128();
			v = V4Scale(v, V4VConstantSplat<FLOAT_TO_INT(255.f)>());
			v = V4FloatToIntRaw<0>(v);
#			if __BE
				V4Store8<3>(&vehicleDamageScale, v);
#			else
				V4Store8<0>(&vehicleDamageScale, v);
#			endif
		}

 		inline Vec2V_Out GetTexCoord() const
		{
			using namespace Vec;
			CompileTimeAssert(__alignof(decalVtx) >= 4);
			CompileTimeAssert((OffsetOf(decalVtx,texcoordU)&3) == 0);
			Vector_4V v = V4LoadLeft(&texcoordU);
			return Vec2V(V4Float16Vec2UnpackFromX(v));
		}

		inline void SetTexCoord(Vec2V_In val)
		{
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				sysMemCpy(uncompTex, &val, sizeof(uncompTex));
#			endif

			using namespace Vec;
			Vector_4V t = V4Float16Vec8Pack(val.GetIntrin128(),val.GetIntrin128());
			V4Store32<0>(&texcoordU, t);
		}

   		inline Vec4V_Out GetTangent() const
		{
			using namespace Vec;

#			if __PS3
				Vector_4V xyz = V4LoadLeft(&tangentXYZ);
				xyz = V4UnpackNormFloats_11_11_10(xyz);
				Vector_4V one = V4VConstant(V_ONE);
				Vector_4V signBit = V4VConstant(V_80000000);
				Vector_4V w = V4LoadLeft(&fbRatioAndBitangentFlip);
				w = V4SelectFT(signBit, one, w);
				w = V4Xor(w, signBit);
				Vector_4V xyzw = V4PermuteTwo<X1,Y1,Z1,X2>(xyz, w);
				return Vec4V(xyzw);
#			else
				Vector_4V xyzw = V4LoadLeft(&tangent);
				xyzw = V4UnpackNormFloats_10_10_10_2(xyzw);
				return Vec4V(xyzw);
#			endif
		}

		inline void SetTangent(Vec4V_In val)
		{
			using namespace Vec;

#			if DECAL_STORE_UNCOMPRESSED_VERTICES
#				if RSG_ORBIS
					static const float oooh[4]  = {1.f, 1.f, 1.f, 0.5f};
					static const float zzzh[4]  = {0.f, 0.f, 0.f, 0.5f};
					const Vec4V uval = val * (const Vec4V&)oooh + (const Vec4V&)zzzh;
#				elif __WIN32PC || RSG_DURANGO
					const Vec4V half(V_HALF);
					const Vec4V uval = val * half + half;
#				else
					const Vec4V uval = val;
#				endif
				sysMemCpy(uncompTan, &uval, sizeof(uncompTan));
#			endif

#			if __PS3
				Vector_4V v = val.GetIntrin128();
				Vector_4V xyz = V4PackNormFloats_11_11_10(v);
				Vector_4V w = V4SplatW(v);
				Vector_4V fbr = V4LoadLeft(&fbRatioAndBitangentFlip);
				Vector_4V signBit = V4VConstant(V_80000000);
				w = V4Xor(w, signBit);
				Vector_4V combined = V4SelectFT(signBit, fbr, w);
				V4Store8<0>(&fbRatioAndBitangentFlip, combined);
				V4Store32<0>(&tangentXYZ, xyz);
#			else
				Vector_4V xyzw = V4PackNormFloats_10_10_10_2(val.GetIntrin128());
				V4Store32<0>(&tangent, xyzw);
#			endif

// 			Vector_4V test = GetTangent().GetIntrin128();
// 			Vector_4V diff = V4Abs(V4Subtract(test, V4Clamp(val.GetIntrin128(), V4VConstant(V_NEGONE), V4VConstant(V_ONE))));
// 			Vector_4V tolerance = V4VConstant<FLOAT_TO_INT(3e-3f), FLOAT_TO_INT(3e-3f), FLOAT_TO_INT(3e-3f), FLOAT_TO_INT(1e-3f)>();
// 			if (!V4IsLessThanAll(diff, tolerance))
// 			{
// #				if !__SPU
// 					Printf(
// 						"ERROR TANGENT 0x%08x 0x%08x 0x%08x 0x%08x\n"
// 						"this %p\n"
// 						"in  (%.3f, %.3f, %.3f, %.3f)\n"
// 						"out (%.3f, %.3f, %.3f, %.3f)\n"
// 						, ((u32*)&val)[0], ((u32*)&val)[1], ((u32*)&val)[2], ((u32*)&val)[3]
// 						, this
// 						, ((float*)&val)[0], ((float*)&val)[1], ((float*)&val)[2], ((float*)&val)[3]
// 						, ((float*)&test)[0], ((float*)&test)[1], ((float*)&test)[2], ((float*)&test)[3]);
// #				endif
// 				__debugbreak();
// 			}
		}

		inline void FlipBitangent()
		{
#			if __PS3
				fbRatioAndBitangentFlip = -fbRatioAndBitangentFlip;
#			else
				tangent ^= 0xc0000000;
#			endif
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				uncompTan[3] = -uncompTan[3];
#			endif
		}

		static int CanMerge(const decalVtx* pDecalVtxA, const decalVtx* pDecalVtxB);


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

#		if __PS3

			s16         posX;
			s16         posY;
			s16         posZ;

			// The bitangent flip bit is stored in the sign bit, as the front to
			// back ratio is always positive
			u8          fbRatioAndBitangentFlip;    // CELL_GCM_VERTEX_UB * 2 - 1

			u8          vehicleDamageScale;         // CELL_GCM_VERTEX_UB

			Float16     texcoordU;
			Float16     texcoordV;

			u32         normal;                     // CELL_GCM_VERTEX_CMP
			u32         tangentXYZ;                 // CELL_GCM_VERTEX_CMP (X,Y,Z)

#		else

			s16         posX;
			s16         posY;
			s16         posZ;

			u8          fbRatio;                    // D3DDECLTYPE_UBYTE4N / DXGI_FORMAT_R8G8B8A8_UNORM / kBufferFormat8_8,kBufferChannelTypeUNorm

			u8          vehicleDamageScale;         // D3DDECLTYPE_UBYTE4N / DXGI_FORMAT_R8G8B8A8_UNORM / kBufferFormat8_8,kBufferChannelTypeUNorm

			Float16     texcoordU;
			Float16     texcoordV;

			u32         normal;                     // D3DDECLTYPE_HEND3N  / DXGI_FORMAT_R10G10B10A2_UNORM / kBufferFormat10_11_11,kBufferChannelTypeSNorm
			u32         tangent;                    // D3DDECLTYPE_DEC4N   / DXGI_FORMAT_R10G10B10A2_UNORM / kBufferFormat2_10_10_10,kBufferChannelTypeSNorm

#		endif


#		if DECAL_STORE_UNCOMPRESSED_VERTICES
			float       uncompPos[3];
#			if __XENON
				// Note that uncompFbr and uncompVds are in the opposite order
				// to the compressed versions, because D3DDECLTYPE_UBYTE4N
				// treats the vertex attribute as a single big endian u32, with
				// X in the least significant byte, Y the next, etc.
				// Effectively, this reverses the order in the compressed
				// version, so we need to do the same here.
				float       uncompVds;
				float       uncompFbr;
#			else
				// Other platforms can store these values in the same order as
				// the compressed versions.
				float       uncompFbr;
				float       uncompVds;
#			endif
			float       uncompTex[2];
			float       uncompNrm[3];
			float       uncompTan[4];
#		endif
};


// For all other platforms, pointers are a bit more efficient so use them.  Note
// that regardless of the size of a pointer, we can only align to 4 here,
// otherwise the structure cannot alias over the top of decalVtx.
class decalVtxFree_ : public SListItemAlign<decalVtxFree_, 4>
{
	ATTR_UNUSED u32     pad1[5-sizeof(void*)/4];
#	if DECAL_STORE_UNCOMPRESSED_VERTICES
		ATTR_UNUSED u32     pad2[14];
#	endif
};

typedef decalVtxFree_ MAY_ALIAS decalVtxFree;

CompileTimeAssert(__alignof(decalVtx) == __alignof(decalVtxFree));
CompileTimeAssert(sizeof(decalVtx)    == sizeof(decalVtxFree));


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALVERTEX_H
