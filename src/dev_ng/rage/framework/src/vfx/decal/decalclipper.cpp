//
// vfx/decal/decalclipper.cpp
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

// includes
#include "decalclipper.h"

// includes (rage)
#include "math/random.h"
#include "vectormath/legacyconvert.h"
#if !__SPU
#	include "grcore/im.h"
#endif

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decaldebug.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


// code
void decalClipper::Init(Vec3V_In vPosition, Vec3V_In vDirection, Vec3V_In vSide, Vec3V_In vForward, Vec3V_In vDimensions, Vec3V_Ptr pPassedJoinVerts, Vec3V_Ptr pReturnedJoinVerts, decalJoinType& joinType)
{
	// calculate the returned join verts (the verts on the front plane)
	Vec3V halfDimensions = vDimensions * ScalarV(V_HALF);
	ScalarV halfWidth	= SplatX(halfDimensions);
	ScalarV halfHeight	= SplatY(halfDimensions);
	ScalarV halfDepth	= SplatZ(halfDimensions);

	Vec3V vRight = vSide*halfWidth;
	Vec3V vFront = vForward*halfHeight;
	Vec3V vUp = -vDirection*halfDepth;
	Vec3V vCentreFrontPos = vPosition + vFront;
	pReturnedJoinVerts[0] = vCentreFrontPos - vUp + vRight;
	pReturnedJoinVerts[1] = vCentreFrontPos + vUp + vRight;
	pReturnedJoinVerts[2] = vCentreFrontPos + vUp - vRight;
	pReturnedJoinVerts[3] = vCentreFrontPos - vUp - vRight;

	// calc the perpendicular distances from the origin to the position of the box along each plane normal
	ScalarV dotSide			= Dot(vSide,		vPosition);
	ScalarV dotForward		= Dot(vForward,		vPosition);
	ScalarV dotDirection	= Dot(vDirection,	vPosition);

	// set up the clip planes (x, y, z represent the normal of the plane - w represents the perpendicular distance from the origin to the plane)
	m_planeEquations[DECAL_PLANE_BOTTOM]	= Vec4V( vDirection,	halfDepth + dotDirection);
	m_planeEquations[DECAL_PLANE_TOP]		= Vec4V(-vDirection,	halfDepth - dotDirection);

	joinType = DECAL_JOINTYPE_NONE;

	if (IsZeroAll(pPassedJoinVerts[0]) || 
		(IsEqualAll(pPassedJoinVerts[0], pReturnedJoinVerts[0]) && 
		 IsEqualAll(pPassedJoinVerts[1], pReturnedJoinVerts[1]) && 
		 IsEqualAll(pPassedJoinVerts[2], pReturnedJoinVerts[2]) && 
		 IsEqualAll(pPassedJoinVerts[3], pReturnedJoinVerts[3])) ||
		CalcJoinPlanes(vPosition, /*vDirection, vSide, vForward,*/ vDimensions, pPassedJoinVerts, pReturnedJoinVerts, joinType)==false)
	{
		m_planeEquations[DECAL_PLANE_RIGHT]	= Vec4V( vSide,			halfWidth	+ dotSide);
		m_planeEquations[DECAL_PLANE_LEFT]	= Vec4V(-vSide,			halfWidth	- dotSide);
		m_planeEquations[DECAL_PLANE_FRONT]	= Vec4V( vForward,		halfHeight	+ dotForward);
		m_planeEquations[DECAL_PLANE_BACK]	= Vec4V(-vForward,		halfHeight	- dotForward);
	}
}

static inline void CalcMidPoint(Vec3V_InOut vMidPt, Vec3V_In vVec1, Vec3V_In vVec2, Vec3V_In vVec3, Vec3V_In vVec4)
{
	vMidPt = (vVec1+vVec2+vVec3+vVec4) * ScalarV(V_QUARTER);
}

bool decalClipper::CalcJoinPlanes(Vec3V_In vPosition, /*Vec3V_In vDirection, Vec3V_In vSide, Vec3V_In vForward,*/ Vec3V_In vDimensions, Vec3V_Ptr pPassedJoinVerts, Vec3V_Ptr pReturnedJoinVerts, decalJoinType& joinType)
{
	// calc new rear plane equation from the passed in verts (which are the front verts of the previous projection)
	Vec3V vRearPlaneNormal;
	float rearPlaneD;
	CalcPlaneEquation(vRearPlaneNormal, rearPlaneD, pPassedJoinVerts[3], pPassedJoinVerts[2], pPassedJoinVerts[0], pPassedJoinVerts[1], 0);

	// calc the mid points of the front and rear plane
	Vec3V vRearMidPt, vFrontMidPt;
	CalcMidPoint(vRearMidPt, pPassedJoinVerts[0], pPassedJoinVerts[1], pPassedJoinVerts[2], pPassedJoinVerts[3]);
	CalcMidPoint(vFrontMidPt, pReturnedJoinVerts[0], pReturnedJoinVerts[1], pReturnedJoinVerts[2], pReturnedJoinVerts[3]);

	// calc the direction from the rear to front mid points
	Vec3V vForwardDirNotNormalised = vFrontMidPt - vRearMidPt;
	//vForwardDir = Normalize(vForwardDir); // don't need to normalise (until later), we're just testing the dot product against zero

	// calculate which side of the rear plane we are traveling
	if (IsGreaterThanAll(Dot(vRearPlaneNormal, vForwardDirNotNormalised), ScalarV(V_ZERO)))
	{
		// we're traveling backwards - behind the rear plane
		joinType = DECAL_JOINTYPE_BACK;

		// always reject these for now
		static bool disableJoinPlanesBehind = false;
		if (disableJoinPlanesBehind)
		{
			return false;
		}

		// negate the plane normal 
		vRearPlaneNormal = -vRearPlaneNormal;
		rearPlaneD = -rearPlaneD;

		// swap the 2 passed sides around that we'll be joining up to
		// this means we can treat front and back identically from this point on
		Vec3V tempA = pPassedJoinVerts[0];
		Vec3V tempB = pPassedJoinVerts[1];
		pPassedJoinVerts[0] = pPassedJoinVerts[3];
		pPassedJoinVerts[1] = pPassedJoinVerts[2];
		pPassedJoinVerts[3] = tempA;
		pPassedJoinVerts[2] = tempB;
	}
//#if __BANK
	else
	{
		// we're traveling forwards - in front of the rear plane
		joinType = DECAL_JOINTYPE_FRONT;	
	}
//#endif

	// set the new rear plane normal
	m_planeEquations[DECAL_PLANE_BACK].SetXYZ(vRearPlaneNormal);
	m_planeEquations[DECAL_PLANE_BACK].SetWf(rearPlaneD);

	//
	// STEP 2 - check if the new forward points are on the same side of the rear plane
	//
	// check if all new points are on the same side of the rear plane
	float frontVertDots[4];
	for (int i=0; i<4; i++)
	{
		frontVertDots[i] = Dot(vRearPlaneNormal, pReturnedJoinVerts[i]).Getf();
	}

	if ((frontVertDots[0]<=rearPlaneD) && 
		(frontVertDots[1]<=rearPlaneD) && 
		(frontVertDots[2]<=rearPlaneD) && 
		(frontVertDots[3]<=rearPlaneD))
	{
		Vec3V vForwardDir = Normalize(vForwardDirNotNormalised);

		// all on the same side
		// the front plane is just the direction 
		ScalarV halfHeight = vDimensions.GetY() * ScalarV(V_HALF);
		ScalarV dotForward = Dot(vForwardDir, vPosition);
		m_planeEquations[DECAL_PLANE_FRONT].SetXYZ(vForwardDir);
		m_planeEquations[DECAL_PLANE_FRONT].SetW(halfHeight + dotForward);

		// calc the new right plane
		CalcPlaneEquation(m_planeEquations[DECAL_PLANE_RIGHT], pPassedJoinVerts[0], pPassedJoinVerts[1], pReturnedJoinVerts[0], pReturnedJoinVerts[1], 1);

		// calc the new left plane
		CalcPlaneEquation(m_planeEquations[DECAL_PLANE_LEFT], pReturnedJoinVerts[3], pReturnedJoinVerts[2], pPassedJoinVerts[3], pPassedJoinVerts[2], 2);
	}
	else
	{
		// different sides
		if (joinType==DECAL_JOINTYPE_FRONT)
		{
			joinType = DECAL_JOINTYPE_FRONT_CROSS;
		}
		else if (joinType==DECAL_JOINTYPE_BACK)
		{
			joinType = DECAL_JOINTYPE_BACK_CROSS;
		}

		// always reject these for now
		static bool disableJoinPlanesCrossed = true;
		if (disableJoinPlanesCrossed)
		{
			return false;
		}

		// clip any new front verts that are behind the rear plane to the rear passed in verts
		int numClipped = 0;
		for (int i=0; i<4; i++)
		{
			if (frontVertDots[i]>rearPlaneD)
			{
				pReturnedJoinVerts[i] = pPassedJoinVerts[i];
				numClipped++;
			}
		}

		// check if all 4 new verts have been clipped to the passed verts 
		// therefore errors have crept in and the direction is therefore incorrect
		if (numClipped==4)
		{
			decalAssertf(0, "All 4 new verts have been clipped to the previous verts - plane calcs will go wrong!");
			return false;
		}

		// recalc the front mid point 
		CalcMidPoint(vFrontMidPt, pReturnedJoinVerts[0], pReturnedJoinVerts[1], pReturnedJoinVerts[2], pReturnedJoinVerts[3]);

		// recalc the forward direction
		vForwardDirNotNormalised = vFrontMidPt - vRearMidPt;
		//vForwardDir = Normalize(vForwardDir); // don't need to normalise, we're just testing the dot product against zero

		// calc new front plane equation from the clipped new front verts
		CalcPlaneEquation(m_planeEquations[DECAL_PLANE_FRONT], pReturnedJoinVerts[0], pReturnedJoinVerts[1], pReturnedJoinVerts[3], pReturnedJoinVerts[2], 3);
	
		// some of the new verts have been clipped so they are no longer coplanar - adjust them so they are again
		AlignVertsToPlane(m_planeEquations[DECAL_PLANE_FRONT], pReturnedJoinVerts[0], pReturnedJoinVerts[1], pReturnedJoinVerts[3], pReturnedJoinVerts[2]);

		// the new points cross the plane - choose a side of the passed in verts to join up to
		Vec3V vJoinPtTop;
		Vec3V vJoinPtBottom;
		if (IsGreaterThanAll(Dot(vForwardDirNotNormalised, pPassedJoinVerts[2] - pPassedJoinVerts[1]), ScalarV(V_ZERO)))
		{
			vJoinPtTop = pPassedJoinVerts[1];
			vJoinPtBottom = pPassedJoinVerts[0];
		}
		else
		{
			vJoinPtTop = pPassedJoinVerts[2];
			vJoinPtBottom = pPassedJoinVerts[3];
		}

		// calc the new right plane
		CalcPlaneEquation(m_planeEquations[DECAL_PLANE_RIGHT], vJoinPtBottom, vJoinPtTop, pReturnedJoinVerts[0], pReturnedJoinVerts[1], 4);

		// calc the new left plane
		CalcPlaneEquation(m_planeEquations[DECAL_PLANE_LEFT], pReturnedJoinVerts[3], pReturnedJoinVerts[2], vJoinPtBottom, vJoinPtTop, 5);
	}

	// calc the new box data
// 	vForward = vForwardDir;
// 	vSide = Cross(vForward, vDirection);
// 	vSide = Normalize(vSide);
// 	vDirection = Cross(vSide, vForward);
// 	vDirection = Normalize(vDirection);
// 	vPosition = (vFrontMidPt+vRearMidPt)*ScalarV(V_HALF);

	// pass back the new front mid point
//	m_newFrontMidPt = vFrontMidPt;

	return true;
}

void decalClipper::AlignVertsToPlane(Vec4V_InOut vPlaneEq, Vec3V_InOut vSideABottom, Vec3V_InOut vSideATop, Vec3V_InOut vSideBBottom, Vec3V_InOut vSideBTop)
{
	const Vec3V vPlaneNormal = vPlaneEq.GetXYZ();

#if __XENON // V4VECTORV_HAS_NATIVE_DOT_PRODUCT 

	const ScalarV neg1(V_NEGONE);

	vSideABottom = AddScaled(vSideABottom, vPlaneNormal, Dot(Vec4V(vSideABottom, neg1), vPlaneEq));
	vSideATop    = AddScaled(vSideATop,    vPlaneNormal, Dot(Vec4V(vSideATop,    neg1), vPlaneEq));
	vSideBBottom = AddScaled(vSideBBottom, vPlaneNormal, Dot(Vec4V(vSideBBottom, neg1), vPlaneEq));
	vSideBTop    = AddScaled(vSideBTop,    vPlaneNormal, Dot(Vec4V(vSideBTop,    neg1), vPlaneEq));

#else // transposed to avoid dot products

	Vec4V a,b,c;
	Transpose3x4to4x3(a,b,c, vSideABottom, vSideATop, vSideBBottom, vSideBTop);

	Vec4V negd(vPlaneEq.GetW());

	negd = SubtractScaled(negd, a, vPlaneEq.GetX());
	negd = SubtractScaled(negd, b, vPlaneEq.GetY());
	negd = SubtractScaled(negd, c, vPlaneEq.GetZ());

	vSideABottom = SubtractScaled(vSideABottom, vPlaneNormal, negd.GetX());
	vSideATop    = SubtractScaled(vSideATop,    vPlaneNormal, negd.GetY());
	vSideBBottom = SubtractScaled(vSideBBottom, vPlaneNormal, negd.GetZ());
	vSideBTop    = SubtractScaled(vSideBTop,    vPlaneNormal, negd.GetW());

#endif

#if __ASSERT
	ScalarV vDot;
	float error;

	vDot = Dot(vSideABottom, vPlaneNormal);
	error = Abs((vDot-vPlaneEq.GetW()).Getf());
	decalAssertf(error<0.02f, "Aligned vertex is not on the plane");

	vDot = Dot(vSideATop, vPlaneNormal);
	error = Abs((vDot-vPlaneEq.GetW()).Getf());
	decalAssertf(error<0.02f, "Aligned vertex is not on the plane");

	vDot = Dot(vSideBBottom, vPlaneNormal);
	error = Abs((vDot-vPlaneEq.GetW()).Getf());
	decalAssertf(error<0.02f, "Aligned vertex is not on the plane");

	vDot = Dot(vSideBTop, vPlaneNormal);
	error = Abs((vDot-vPlaneEq.GetW()).Getf());
	decalAssertf(error<0.02f, "Aligned vertex is not on the plane");
#endif
}



void decalClipper::CalcPlaneEquation(Vec3V_InOut vPlaneNormal, float& planeD, Vec3V_In vSideABottom, Vec3V_In vSideATop, Vec3V_In vSideBBottom, Vec3V_In vSideBTop, int ASSERT_ONLY(id))
{
	// the passed points may not be coplanar so we need to approximate 
	// the plane equation across the surface
	//
	// the plane normal will come out of the screen
	//
	//		ATop x-----------------x BTop
	//			 |                 |
	//			 |                 |
	//			 |      MidPt      |
	//	  AMidPt x        x ------ x BMidPt
	//			 |        |        |
	//			 |        |        |
	//			 |        |        |
	//	 ABottom x--------x--------x BBottom
	//		         BottomMidPt

	decalAssertf(!IsEqualAll(vSideABottom, vSideBBottom) || !IsEqualAll(vSideATop, vSideBTop), "Cannot calculate a plane equation as the 2 sides are equal - MarkN needs to see this in the debugger (bug #723374) (%d) (%.3f, %.3f, %.3f - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f - %.3f, %.3f, %.3f",
		id,
		VEC3V_ARGS(vSideABottom),
		VEC3V_ARGS(vSideBBottom),
		VEC3V_ARGS(vSideATop),
		VEC3V_ARGS(vSideBTop));

#if 0 // slightly faster normal calculation - might try this sometime ..
	vPlaneNormal = Cross(vSideBBottom - vSideATop, vSideBTop - vSideABottom);
	vPlaneNormal = Normalize(vPlaneNormal);
	Vec3V vMidPt;
	CalcMidPoint(vMidPt, vSideABottom, vSideATop, vSideBBottom, vSideBTop);
	planeD = Dot(vMidPt, vPlaneNormal).Getf();
#else // old calculation
	Vec3V vSideAMidPt = vSideABottom + vSideATop; // no reason to scale by 0.5 here, since we're going to normalise
	Vec3V vSideBMidPt = vSideBBottom + vSideBTop;
	Vec3V vBottomMidPt = vSideABottom + vSideBBottom;
	Vec3V vMidPt = (vSideAMidPt + vSideBMidPt) * ScalarV(V_HALF);

	vPlaneNormal = Cross(vBottomMidPt - vMidPt, vSideBMidPt - vMidPt);
	vPlaneNormal = Normalize(vPlaneNormal);
	planeD = Dot(vMidPt * ScalarV(V_HALF), vPlaneNormal).Getf();
#endif
}

void decalClipper::CalcPlaneEquation(Vec4V_InOut vPlaneEq, Vec3V_In vSideABottom, Vec3V_In vSideATop, Vec3V_In vSideBBottom, Vec3V_In vSideBTop, int id)
{
	Vec3V vPlaneNormal;
	float planeD;
	CalcPlaneEquation(vPlaneNormal, planeD, vSideABottom, vSideATop, vSideBBottom, vSideBTop, id);

	vPlaneEq.SetXYZ(vPlaneNormal);
	vPlaneEq.SetWf(planeD);
}

void decalClipper::SetPlaneEquation(decalPlaneId planeId, Vec4V_In vPlaneEq) 
{
	if (decalVerifyf(planeId>=0 && planeId<DECAL_NUM_PLANES, "invalid plane id"))
	{
		m_planeEquations[planeId] = vPlaneEq;
	}
}

void decalClipper::SetPlaneEquation(decalPlaneId planeId, Vec3V_In vPlaneNormal, float planeD)
{
	if (decalVerifyf(planeId>=0 && planeId<DECAL_NUM_PLANES, "invalid plane id"))
	{
		m_planeEquations[planeId].SetXYZ(vPlaneNormal);
		m_planeEquations[planeId].SetWf(planeD);
	}
}

Vec4V_ConstRef decalClipper::GetPlaneEquation(decalPlaneId planeId) const
{
	decalAssertf(planeId>=0 && planeId<DECAL_NUM_PLANES, "invalid plane id");
	return m_planeEquations[planeId];
}

Vec4V_Ptr decalClipper::GetPlaneEquationsPtr()
{
	return &m_planeEquations[0];
}

void decalClipper::ClipPoly(int& numVerts, Vec3V* pVerts, Vec3V* pNorms, ScalarV* pVehDmgScales) const
{
	// clip against each plane in turn
	for (int planeId=0; planeId<DECAL_NUM_PLANES; planeId++)
	{
		// calc the distances to the plane of each vertex
		float dists[DECAL_MAX_CLIPPED_VERTS];
		const Vec3V* pV = pVerts;
		for (int i=0; i<numVerts; i++)
		{
			dists[i] = Dot(Vec4V(pV[i], ScalarV(V_NEGONE)), m_planeEquations[planeId]).Getf();
		}

		// go through the dists outputting the clipped verts
		int numClippedVerts = 0;
		Vec3V   vClippedVerts[DECAL_MAX_CLIPPED_VERTS+1];
		Vec3V   vClippedNorms[DECAL_MAX_CLIPPED_VERTS+1];
		ScalarV vClippedVdss [DECAL_MAX_CLIPPED_VERTS+1];

		for (int i=0; i<numVerts; i++)
		{
			int id1 = i;
			int id2 = i+1;

			if (i==numVerts-1)
			{
				id2 = 0;
			}

			if (dists[id1]<=0.0f)
			{
				if (dists[id2]<=0.0f)
				{
					// in, in - add the second point 
					vClippedVerts[numClippedVerts] = pVerts[id2];
					vClippedNorms[numClippedVerts] = pNorms[id2];
					vClippedVdss [numClippedVerts] = pVehDmgScales[id2];
					numClippedVerts++;
				}
				else
				{
					// in, out  - add the intersection
					ScalarV t = ScalarVFromF32(dists[id1]/(dists[id1]-dists[id2]));
					vClippedVerts[numClippedVerts] = pVerts[id1]        + ((pVerts[id2]       -pVerts       [id1]) * t);
					vClippedNorms[numClippedVerts] = pNorms[id1]        + ((pNorms[id2]       -pNorms       [id1]) * t);
					vClippedVdss [numClippedVerts] = pVehDmgScales[id1] + ((pVehDmgScales[id2]-pVehDmgScales[id1]) * t);
					numClippedVerts++;
				}
			}
			else if (dists[id2]<=0.0f)
			{
				// out, in - add the intersection and the 2nd point
				ScalarV t = ScalarVFromF32(dists[id1]/(dists[id1]-dists[id2]));
				vClippedVerts[numClippedVerts] = pVerts[id1]        + ((pVerts[id2]       -pVerts       [id1]) * t);
				vClippedNorms[numClippedVerts] = pNorms[id1]        + ((pNorms[id2]       -pNorms       [id1]) * t);
				vClippedVdss [numClippedVerts] = pVehDmgScales[id1] + ((pVehDmgScales[id2]-pVehDmgScales[id1]) * t);
				numClippedVerts++;
				vClippedVerts[numClippedVerts] = pVerts[id2];
				vClippedNorms[numClippedVerts] = pNorms[id2];
				vClippedVdss [numClippedVerts] = pVehDmgScales[id2];
				numClippedVerts++;
			}
		}

		if (numClippedVerts>numVerts+1)
		{
#if __ASSERT
			// something has gone wrong - a max of 1 extra vert should have been added only
			decalDisplayf("plane is %.2f %.2f %.2f %.2f\n", VEC4V_ARGS(m_planeEquations[planeId]));
			for (int i=0; i<numVerts; i++)
			{
				decalDisplayf("verts are %.2f %.2f %.2f\n", VEC3V_ARGS(pVerts[i]));
			}
			decalDisplayf("clipping started with %d verts and ended with %d", numVerts, numClippedVerts);
#endif

			numVerts = 0;
			return;
		}

		// copy over the clipped verts
		numVerts = numClippedVerts;
		for (int i=0; i<numVerts; i++)
		{
			pVerts[i]        = vClippedVerts[i];
			pNorms[i]        = vClippedNorms[i];
			pVehDmgScales[i] = vClippedVdss [i];
		}

		// return early if there are no verts
		if (numVerts==0)
		{
			return;
		}
	}
}

#if __BANK && !__SPU
void decalClipper::RenderDebug(Vec3V_In vPosition, float range) const
{
	ScalarV vSize = ScalarVFromF32(range);
	for (int i=0; i<DECAL_NUM_PLANES; i++)
	{
		RenderDebugPlane((decalPlaneId)i, vPosition, vSize);
	}
}
#endif

#if __BANK && !__SPU
void decalClipper::RenderDebugPlane(decalPlaneId planeId, Vec3V_In vPosition, ScalarV_In vSize) const
{
	grcWorldIdentity();

	// set the colour
	Color32 col;
	if		(planeId==DECAL_PLANE_RIGHT)	col = Color32(1.0f, 0.0f, 0.5f, 1.0f);
	else if	(planeId==DECAL_PLANE_LEFT)		col = Color32(1.0f, 0.0f, 1.0f, 1.0f);
	else if (planeId==DECAL_PLANE_BOTTOM)	col = Color32(0.0f, 0.5f, 1.0f, 1.0f);
	else if (planeId==DECAL_PLANE_TOP)		col = Color32(0.0f, 1.0f, 1.0f, 1.0f);
	else if (planeId==DECAL_PLANE_FRONT)	col = Color32(0.5f, 1.0f, 0.0f, 1.0f);
	else if (planeId==DECAL_PLANE_BACK)		col = Color32(1.0f, 1.0f, 0.0f, 1.0f);

	// cache some values
	Vec3V vPlaneNormal = m_planeEquations[planeId].GetXYZ();
	float planeD = m_planeEquations[planeId].GetWf();

	// calculate a point on the plane
	float dotPN = Dot(vPosition, vPlaneNormal).Getf();
	float distToPlane = planeD - dotPN;
	Vec3V vPtOnPlane = vPosition + (vPlaneNormal*ScalarVFromF32(distToPlane));

	// calculate 2 orthogonal vectors along the plane
	Vec3V vWorldUp(V_Z_AXIS_WZERO);
	Vec3V vWorldSide(V_X_AXIS_WZERO);
	Vec3V vVecSide;
	if (Abs(vPlaneNormal.GetZf())>0.99f)
	{
		vVecSide = Cross(vPlaneNormal, vWorldSide);
	}
	else
	{
		vVecSide = Cross(vPlaneNormal, vWorldUp);
	}
	vVecSide = NormalizeSafe(vVecSide, Vec3V(V_X_AXIS_WZERO));
	Vec3V vVecUp = Cross(vVecSide, vPlaneNormal);
	vVecUp = NormalizeSafe(vVecUp, Vec3V(V_X_AXIS_WZERO));

	// calculate the 5 verts of the plane visualisation
	/// Mat34V mtx = Mat34V(V_IDENTITY);
	Vec3V vVecUpScaled = vVecUp*vSize;
	Vec3V vVecSideScaled = vVecSide*vSize;
	Vec3V vVtx1 = vPtOnPlane + vVecUpScaled - vVecSideScaled;
	Vec3V vVtx2 = vPtOnPlane + vVecUpScaled + vVecSideScaled;
	Vec3V vVtx3 = vPtOnPlane - vVecUpScaled + vVecSideScaled;
	Vec3V vVtx4 = vPtOnPlane - vVecUpScaled - vVecSideScaled;

	// store the plane debug
	grcDrawLine(RCC_VECTOR3(vVtx1), RCC_VECTOR3(vVtx2), col);
	grcDrawLine(RCC_VECTOR3(vVtx2), RCC_VECTOR3(vVtx3), col);
	grcDrawLine(RCC_VECTOR3(vVtx3), RCC_VECTOR3(vVtx4), col);
	grcDrawLine(RCC_VECTOR3(vVtx4), RCC_VECTOR3(vVtx1), col);

	// add plane normals
	Vec3V vNormAdd = vPlaneNormal*ScalarVFromF32(0.05f);
	Vec3V vVtx1N = vVtx1 + vNormAdd;
	Vec3V vVtx2N = vVtx2 + vNormAdd;
	Vec3V vVtx3N = vVtx3 + vNormAdd;
	Vec3V vVtx4N = vVtx4 + vNormAdd;

	// store the plane normal debug
	grcDrawLine(RCC_VECTOR3(vVtx1), RCC_VECTOR3(vVtx1N), col);
	grcDrawLine(RCC_VECTOR3(vVtx2), RCC_VECTOR3(vVtx2N), col);
	grcDrawLine(RCC_VECTOR3(vVtx3), RCC_VECTOR3(vVtx3N), col);
	grcDrawLine(RCC_VECTOR3(vVtx4), RCC_VECTOR3(vVtx4N), col);
}
#endif



} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
