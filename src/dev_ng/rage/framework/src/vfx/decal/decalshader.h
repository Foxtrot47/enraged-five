//
// vfx/decal/decalshader.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALSHADER_H
#define	VFX_DECALSHADER_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "grcore/effect_typedefs.h"
#include "math/float16.h"
#include "system/codecheck.h"
#include "vectormath/classes.h"
#include "vectormath/legacyconvert.h"

// includes (framework)
#include "fwtl/customvertexpushbuffer.h"
#include "vfx/channel.h"
#include "decalsettings.h"

#define DECAL_VTX_CNT (50176U)

// namespaces 
namespace rage {


// forward declarations		
class grcTexture;
class grmShader;
#if __WIN32PC || RSG_DURANGO || RSG_ORBIS
	class grcCBuffer;
#endif

// type	
typedef VtxPushBuffer<VtxPB_PoNoTe4Half_trait> decalVertexBuffer_Dynamic;

// structs
// helper structure used to alias dynamic decals 
// vertex data when using indexed vertices
struct dynDecalVtx 
{
	Vec4V	pos;			// grcFvf::grcdsFloat4;	// POSITION
	Vec4V	normal;			// grcFvf::grcdsFloat4;	// NORMAL
	Vec4V	uv01;			// grcFvf::grcdsHalf4;	// TEXCOORD0 TEXCOORD1
	Vec4V	uv23;			// grcFvf::grcdsHalf4;	// TEXCOORD2 TEXCOORD3
};

// classes
class decalShader
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		virtual ~decalShader() {}
		decalShader() : m_pShader(NULL) {}

		void Init(const char* pName);
		void Shutdown();

		RETURNCHECK(bool) Start(int pass);
		void End();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	protected: ////////////////////////

		grmShader* m_pShader;	

};


class decalShader_Static : public decalShader
{
	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		void LookUpVars();

		void SetPerBucketVarsLocal(grcTexture* pDiffuseMap, grcTexture* pNormalMap, grcTexture* pSpecularMap, float specFalloff, float specIntensity, float specFresnel, float parallaxScale, float zShift, bool damageOn, grcTexture* pDamageMap, float damageBoundRadius, float damageMult, float emissiveMultiplier, ScalarV_In posScale, Vec3V_In rotationCenter, float alignToCamRot PS3_ONLY(, float vehDamageOffsetScale));

		void UnbindTextures();

		void PushSamplerState(grcSamplerStateHandle samplerStateHandle
#			if !USE_EDGE
				, grcSamplerStateHandle damageSamplerStateHandle
#			endif
			);
		void PopSamplerState();

#		if RSG_PS3 || RSG_XENON
			unsigned GetPerInstVtxConstReg() const { return m_perInstVtxConstReg; }
#		endif

	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		grcEffectVar m_shaderVarIdDiffuseMap;
		grcEffectVar m_shaderVarIdBumpMap;
		grcEffectVar m_shaderVarIdSpecularMap;
		grcEffectVar m_shaderVarIdSpecular;
		grcEffectVar m_shaderVarIdSpecularColor;
		grcEffectVar m_shaderVarIdSpecularFresnel;
		grcEffectVar m_shaderVarIdParallaxScale;
		grcEffectVar m_shaderVarIdZShiftScale;
		grcEffectVar m_shaderVarIdPositionScale;
		grcEffectVar m_shaderVarIdRotationCenter;
		grcEffectVar m_shaderVarIdAlignToCamRot;
#		if RSG_PS3
			grcEffectVar m_shaderVarIdVehDamageOffsetScale;
#		endif

		grcEffectGlobalVar m_shaderVarIdDamageOn;
		grcEffectVar m_shaderVarIdDamageMap;
		grcEffectVar m_shaderVarIdDamageBoundRadius;
		grcEffectVar m_shaderVarIdDamageMult;
		
#		if RSG_PS3 || RSG_XENON
			unsigned m_perInstVtxConstReg;
#		endif
};


class decalShader_Dynamic : public decalShader
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		void LookUpVars();
		void SetVarsGlobal();
		void SetVarsLocal(grcTexture* pTexture, grcTexture* pNormal, float specFalloff, float specIntensity, float specFresnel, float parallaxScale, float emissiveMultiplier, float polyRejectThreshold);
		void UnbindTextures();

		void PushSamplerState(grcSamplerStateHandle samplerStateHandle);
		void PopSamplerState();


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// shader variables
		grcEffectVar		m_shaderVarIdDecalTexture;
		grcEffectVar		m_shaderVarIdDecalNormal;
		grcEffectGlobalVar	m_shaderVarIdDepthTexture;
		grcEffectGlobalVar	m_shaderVarIdNormalTexture;
		grcEffectVar		m_shaderVarIdScreenSize;
		grcEffectVar		m_shaderVarIdProjParams;
		grcEffectVar		m_shaderVarIdSpecular;
		grcEffectVar		m_shaderVarIdSpecularColor;
		grcEffectVar		m_shaderVarIdSpecularFresnel;
		grcEffectVar		m_shaderVarIdParallaxScale;
		grcEffectVar		m_shaderVarIdEmissiveMultiplier;
		grcEffectVar		m_shaderVarIdPolyRejectThreshold;
};

class decalVB_Dynamic
{	
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// main interface
		inline void Init() {m_vtxBuffer.Init(drawTriStrip, DECAL_VTX_CNT); }
		inline void Shutdown() {m_vtxBuffer.Shutdown();}
		inline void Start() {m_vtxBuffer.Begin();}
		inline void End() {m_vtxBuffer.End();}

		// vertex buffer interface
		inline int GetBatchSize() {return m_vtxBuffer.GetVtxBatchSize();}
		inline void Lock(int vtxCount) {m_vtxBuffer.Lock(vtxCount);}
		inline void UnLock() {m_vtxBuffer.UnLock();}

		inline void AddVertex(Vec3V_In vVert, Vec4V_In vPos, Vec4V_In vForward, Vec4V_In vSide, Vec4V_In vTex, Vec4V_In vAlpha)
		{
			dynDecalVtx vertex;
			vertex.pos		= Vec4V(vVert);
			vertex.normal	= vPos;
			vertex.uv01 = Float16Vec8Pack( vForward, vSide );
			vertex.uv23 = Float16Vec8Pack( vTex, vAlpha );

			m_vtxBuffer.BeginVertex();
			m_vtxBuffer.SetPosition(RCC_VECTOR3(vertex.pos));
			m_vtxBuffer.SetNormal(RCC_VECTOR3(vertex.normal));
			m_vtxBuffer.SetUV(RCC_VECTOR4(vertex.uv01));
			m_vtxBuffer.SetUV1(RCC_VECTOR4(vertex.uv23));
			m_vtxBuffer.EndVertex();
		}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// vertex buffer variables
		decalVertexBuffer_Dynamic m_vtxBuffer;
			
}; 

#if DECAL_USE_INDEXED_VERTS
class decalDrawInterface
{	
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// wrapper functions for dynamic decals rendering
		static void BeginDynamicDecals();
		static void EndDynamicDecals();
		static void BeginIndexedVertices(u32 vertexCount, u32 indexCount, u32 vertexSize);
		static void EndIndexedVertices();

		static void CopyDynamicDecalIndexedData(const u32 numDynDecals);
		static void CopyDynamicDecalVertices(Vec3V_Ptr pvVerts, Vec4V_In vPosition4, Vec4V_In vForward4, Vec4V_In vSide4, Vec4V_In vTex4, Vec4V_In vAlpha4);



	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

		// decalDrawInterface variables
		static const grcVertexElement	ms_dynDecalVtxElements[];
		static grcVertexDeclaration*	ms_pDynDecalVtxDecl; 

		static const u32				ms_dynDecalBoxIndices[DECAL_NUM_DYNAMIC_INST_IDX_HALF]	;
		static u32*						ms_pDynDecalIndices										;
		static dynDecalVtx*				ms_pDynDecalVerts										;
}; 
#endif

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALSHADER_H
