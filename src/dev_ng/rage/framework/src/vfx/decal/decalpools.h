//
// vfx/decal/decalpools.h
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALPOOLS_H
#define	VFX_DECALPOOLS_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)

// includes (framework)
#include "vfx/asyncpool.h"
#include "vfx/slistitor.h"
#include "vfx/decal/decalbucket.h"
#include "vfx/decal/decaldmatags.h"
#include "vfx/decal/decalinst.h"
#include "vfx/decal/decalminiidxbuf.h"
#include "vfx/decal/decalvertex.h"


// namespaces
namespace rage {


// forward declarations

// classes
class decalPools
{
	friend class decalManager;
	friend class decalManagerAsync;
	friend class decalRecycler;

	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////
		decalPools() : m_buckets(NULL), m_insts(NULL), m_mibs(NULL), m_verts(NULL) { }

		// pool access
		void Init();
		void Shutdown();

		inline const decalBucket     *GetBucketPoolBase () const;
		inline const decalInst       *GetInstPoolBase   () const;
		inline const decalMiniIdxBuf *GetMibPoolBase    () const;
		inline const decalVtx        *GetVtxPoolBase    () const;
		inline decalBucket     *GetBucketPoolBase   ();
		inline decalInst       *GetInstPoolBase     ();
		inline decalMiniIdxBuf *GetMibPoolBase      ();
		inline decalVtx        *GetVtxPoolBase      ();

		inline decalBucket     *GetBucket   (SPU_ONLY(decalPools *thisEa, u32 dmaTag));
		inline decalInst       *GetInst     (SPU_ONLY(decalPools *thisEa, u32 dmaTag));
		inline decalMiniIdxBuf *GetMib      (SPU_ONLY(decalPools *thisEa, u32 dmaTag));
		inline decalVtx        *GetVtx      (SPU_ONLY(decalPools *thisEa, u32 dmaTag));

		inline void ReturnBucket    (decalBucket     *pBucket SPU_ONLY(, decalPools *thisEa, u32 dmaTag));
		inline void ReturnInst      (decalInst       *pInst   SPU_ONLY(, decalPools *thisEa, u32 dmaTag));
		inline void ReturnMib       (decalMiniIdxBuf *pMib    SPU_ONLY(, decalPools *thisEa, u32 dmaTag));
		inline void ReturnVtx       (decalVtx        *pVtx    SPU_ONLY(, decalPools *thisEa, u32 dmaTag));
		inline void ReturnVtx       (u16              vtxIdx  SPU_ONLY(, decalPools *thisEa, u32 dmaTag));

		inline void ReturnBuckets(decalBucket *pHead, decalBucket *pTail, unsigned count SPU_ONLY(, decalPools *thisEa, u32 dmaTag));
		inline void ReturnInsts  (decalInst   *pHead, decalInst   *pTail, unsigned count SPU_ONLY(, decalPools *thisEa, u32 dmaTag));


	private: //////////////////////////


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

		AsyncPool<decalBucket>              m_bucketPool;
		AsyncPool<decalInst>                m_instPool;
		AsyncPool<decalMiniIdxBufFree>      m_mibPool;
		AsyncPoolAligned<decalVtxFree, 4>   m_vtxPool;

		decalBucket         *m_buckets;
		decalInst           *m_insts;
		decalMiniIdxBufFree *m_mibs;
		decalVtxFree        *m_verts;

		template<class TYPE, class CONV> static void ValidateCount(const TYPE *pHead, const TYPE *pTail, unsigned count, const CONV &conv);
};


inline const decalBucket *decalPools::GetBucketPoolBase() const
{
	return m_buckets;
}

inline const decalInst *decalPools::GetInstPoolBase() const
{
	return m_insts;
}

inline const decalMiniIdxBuf *decalPools::GetMibPoolBase() const
{
	return (decalMiniIdxBuf*)m_mibs;
}

inline const decalVtx *decalPools::GetVtxPoolBase() const
{
	return (decalVtx*)m_verts;
}

inline decalBucket *decalPools::GetBucketPoolBase()
{
	return m_buckets;
}

inline decalInst *decalPools::GetInstPoolBase()
{
	return m_insts;
}

inline decalMiniIdxBuf *decalPools::GetMibPoolBase()
{
	return (decalMiniIdxBuf*)m_mibs;
}

inline decalVtx *decalPools::GetVtxPoolBase()
{
	return (decalVtx*)m_verts;
}

inline decalBucket *decalPools::GetBucket(SPU_ONLY(decalPools *thisEa, u32 dmaTag))
{
	return m_bucketPool.Alloc(SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_bucketPool, dmaTag));
}

inline decalInst *decalPools::GetInst(SPU_ONLY(decalPools *thisEa, u32 dmaTag))
{
	return m_instPool.Alloc(SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_instPool, dmaTag));
}

inline decalMiniIdxBuf *decalPools::GetMib(SPU_ONLY(decalPools *thisEa, u32 dmaTag))
{
	return (decalMiniIdxBuf*)m_mibPool.Alloc(SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_mibPool, dmaTag));
}

inline decalVtx *decalPools::GetVtx(SPU_ONLY(decalPools *thisEa, u32 dmaTag))
{
	return (decalVtx*)m_vtxPool.Alloc(SListDefaultAlignPtrConverter<4>() SPU_ONLY(, &thisEa->m_vtxPool, dmaTag));
}

inline void decalPools::ReturnBucket(decalBucket *pBucket SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	m_bucketPool.Free(pBucket, SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_bucketPool, dmaTag));
}

inline void decalPools::ReturnInst(decalInst *pInst SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	m_instPool.Free(pInst, SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_instPool, dmaTag));
}

inline void decalPools::ReturnMib(decalMiniIdxBuf *pMib SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	m_mibPool.Free((decalMiniIdxBufFree*)pMib, SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_mibPool, dmaTag));
}

inline void decalPools::ReturnVtx(decalVtx *pVtx SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	m_vtxPool.Free((decalVtxFree*)pVtx, SListDefaultAlignPtrConverter<4>() SPU_ONLY(, &thisEa->m_vtxPool, dmaTag));
}

inline void decalPools::ReturnVtx(u16 vtxIdx SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	m_vtxPool.Free(m_verts+vtxIdx, SListDefaultAlignPtrConverter<4>() SPU_ONLY(, &thisEa->m_vtxPool, dmaTag));
}

template<class TYPE, class CONV> void decalPools::ValidateCount(const TYPE *pHead, const TYPE *pTail, unsigned count, const CONV &conv)
{
#	if !__ASSERT
		(void) pHead;
		(void) pTail;
		(void) count;
		(void) conv;
#	else
		SListConstItor<TYPE> it(pHead, DMATAG_BLOCKING, conv);
		unsigned validateCount = 0;
		while (it.Ptr())
		{
			++validateCount;
			if (it.Ea() == pTail)
			{
				break;
			}
		}
		decalAssertf(validateCount == count, "validateCount=%u, count=%u", validateCount, count);
#	endif
}

inline void decalPools::ReturnBuckets(decalBucket *pHead, decalBucket *pTail, unsigned count SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	ValidateCount(pHead, pTail, count, SListDefaultPtrConverter());
	m_bucketPool.Free(pHead, pTail, count, SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_bucketPool, dmaTag));
}

inline void decalPools::ReturnInsts(decalInst *pHead, decalInst *pTail, unsigned count SPU_ONLY(, decalPools *thisEa, u32 dmaTag))
{
	ValidateCount(pHead, pTail, count, SListDefaultPtrConverter());
	m_instPool.Free(pHead, pTail, count, SListDefaultPtrConverter() SPU_ONLY(, &thisEa->m_instPool, dmaTag));
}

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALPOOLS_H
