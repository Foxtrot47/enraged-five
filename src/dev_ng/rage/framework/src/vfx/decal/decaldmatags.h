//
// vfx/decal/decaldmatags.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECAL_DECALDMATAGS_H
#define VFX_DECAL_DECALDMATAGS_H

#if !__TOOL && !__RESOURCECOMPILER

// 0 is hardcoded in quite a few places where dma results are blocked on
// immediately, so keep DMATAG_BLOCKING set to 0
#define DMATAG_BLOCKING                 0

// These other tags can be reassigned as necissary
#define DMATAG_GET_DECALMAN             1
#define DMATAG_GET_SKELBONESDRAWABLES   2
#define DMATAG_GET_SKELINVJOINTS        2
#define DMATAG_GET_RENDERTOHIERARCHYLUT 2
#define DMATAG_GET_PHINST               2
#define DMATAG_GET_ENTITY               3
#define DMATAG_GET_ARCHETYPE            3
#define DMATAG_GET_GTADRAWABLE          4
#define DMATAG_GET_SKELOBJMTXS          5
#define DMATAG_GET_BOUND                5
#define DMATAG_GET_TYPESETTINGS         7
#define DMATAG_GET_RENDERSETTINGS       8
#define DMATAG_GET_BUCKET0              7
#define DMATAG_GET_BUCKET1              8
#define DMATAG_PUT_BUCKET               9
#define DMATAG_GET_VTX0                 10
#define DMATAG_GET_VTX1                 11
#define DMATAG_PUT_VTX0                 13
#define DMATAG_PUT_VTX1                 14
#define DMATAG_PUT_VTX2                 15
#define DMATAG_GET_VTXBUF0              10
#define DMATAG_GET_VTXBUF1              11
#define DMATAG_GET_VTXBUF2              12
#define DMATAG_PUT_MINIIDXBUF0          16
#define DMATAG_PUT_MINIIDXBUF1          17
#define DMATAG_GET_BONEINDEX0           18
#define DMATAG_GET_BONEINDEX1           19
#define DMATAG_GET_MODEL0               20
#define DMATAG_GET_MODEL1               21
#define DMATAG_GET_MODEL2               22

#define DMATAG_INIT_GAME_BEGIN          23
#define DMATAG_INIT_GAME_END            31

#define DMATAG_GET_GEOM0                21
#define DMATAG_GET_GEOM1                22
#define DMATAG_GET_IDX0                 23
#define DMATAG_GET_IDX1                 24
#define DMATAG_GET_VTXBUF               25
#define DMATAG_PUT_DECALINST            26
#define DMATAG_EDGE                     27
#define DMATAG_GET_FRAGTYPEGROUP0       28
#define DMATAG_GET_FRAGTYPEGROUP1       29
#define DMATAG_GET_FRAGTYPEGROUP2       30
#define DMATAG_GET_FRAGTYPECHILD0       1
#define DMATAG_GET_FRAGTYPECHILD1       2
#define DMATAG_GET_FRAGTYPECHILD2       3
#define DMATAG_GET_FRAGTYPECHILD3       4
#define DMATAG_GET_FRAGGROUPSIBLING0    5
#define DMATAG_GET_FRAGGROUPSIBLING1    6
#define DMATAG_GET_PHBOUNDPTRS          4
#define DMATAG_GET_PHBOUND0             5
#define DMATAG_GET_PHBOUND1             6
#define DMATAG_GET_DRAWABLE0            5
#define DMATAG_GET_DRAWABLE1            6
#define DMATAG_GET_SHADER0              7
#define DMATAG_GET_SHADER1              8
#define DMATAG_GET_PHPOLY0              7
#define DMATAG_GET_PHPOLY1              8

// But tag id 31 is reserved for SPURS, don't use that
#if __PS3
#	include <cell/spurs.h>
	CompileTimeAssert(CELL_SPURS_KERNEL_DMA_TAG_ID == 31);
#endif

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECAL_DECALDMATAGS_H
