//
// vfx/decal/decaldebug.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

#if __BANK

// includes (us)
#include "vfx/decal/decaldebug.h"

// __D3D11_MONO_DRIVER_HACK needs to come from here. Remove most quickly
#if RSG_DURANGO
#include "system/d3d11.h"
#endif // RSG_DURANGO

// includes (rage)
#include "bank/bank.h"
#include "grcore/im.h"

// includes (framework)
#include "vfx/channel.h"
#include "vfx/optimisations.h"
#include "vfx/vfxwidget.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decalmanager.h"
#include "vfx/decal/decallod.h"
#include "vfx/decal/decalrecycler.h"
#include "vfx/decal/decalsettingsmanager.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces 
namespace rage {


// code
void decalDebug::Init()
{
	m_pGameGroup = NULL;

	m_numLastDecalBoxes = 0;

	m_statsGlobal.Reset();
	m_statsRendered.Reset();
	m_enableSpam = false;

	m_forceMethodStatic = false;
	m_forceMethodDynamic = false;
	m_forceRenderPassDeferred = false;
	m_forceRenderPassForward = false;
	m_forceRenderPassExtra1 = false;
	m_forceRenderPassExtra2 = false;
	m_forceStaticDecalsOnBounds = false;
	m_disablePolyAngleTest = false;
	m_disableTriClipping = false;
	m_disableDrawableBoneGameTest = false;
	m_disableDrawableBoneBoundTest = false;
	m_disableMaterialTest = false;
	m_disableShaderTest = false;
	m_disableBoundTypeTest = false;
	m_disableLodSkeletonMapping = false;
	m_overrideBoxDepth = -1.0f;
	m_overrideMaxOverlayRadius = -1.0f;

	m_disableAdding = false;
	m_disableAsync = false;
	m_disableSync = false;
	m_disableRendering = false;
	m_disableRenderCulling = false;
	m_disableLods = false;
	m_disableBucketSorting = false;
	m_disableMergingInsts = false;
	m_disableMergingVerts = false;
	m_forceStaticLods = false;
	m_forceDynamicLods = false;
	m_forceWashable = false;
#	if DECAL_STORE_UNCOMPRESSED_VERTICES
		m_useUncompressedVertices = false;
#	endif
#	if __PS3
		m_allowRsxLocalReads = false;
#	endif
	m_modelSpaceOffsetUnits = DECAL_MODEL_SPACE_OFFSET_UNITS_DEFAULT;
	m_washAmount = 0.01f;

	m_renderTimer = 1;
	m_renderBoundsConsidered = false;
	m_renderPolysConsidered = false;
	m_renderPolysSentToClip = false;
	m_renderPolysClipped = false;
	m_renderLastDecal = false;
	m_renderBucketInfo = false;
	m_renderBucketSphere = false;
	m_renderInstBox = false;
	m_renderInstSphere = false;
	m_renderInstTris = false;
	m_bucketColourMode = false;

	m_numInstsCreatedThisProjection = 0;
	m_outputNumInstsCreatedThisProjection = false;

	m_numSkinnedBoneMatricesThisProjection = 0;
	m_outputNumSkinnedBoneMatricesThisProjection = false;

	m_numPolysConsideredThisProjection = 0;
	m_outputNumPolysConsideredThisProjection = false;

	m_numPolysSentToClipThisProjection = 0;
	m_outputNumPolysSentToClipThisProjection = false;
	
	m_numPolysClippedThisProjection = 0;
	m_outputNumPolysClippedThisProjection = false;

	m_outputTimeToProcessThisProjection = false;

	m_resetAll = false;
}

void decalDebug::Render()
{
	if (m_renderLastDecal)
	{
		// stored boxes
		for (int i=0; i<m_numLastDecalBoxes; i++)
		{
			grcDrawBox(RCC_VECTOR3(m_lastDecalBoxes[i].vDimensions), RCC_MATRIX34(m_lastDecalBoxes[i].mtx), m_lastDecalBoxes[i].colour);

			if (m_lastDecalBoxes[i].axisScale>0.0f)
			{
				grcDrawAxis(m_lastDecalBoxes[i].axisScale, RCC_MATRIX34(m_lastDecalBoxes[i].mtx), true);
			}
		}

		// clipper
		if (m_lastDecalClipperRange>0.0f)
		{
			m_lastDecalClipper.RenderDebug(m_lastDecalClipperPosition, m_lastDecalClipperRange);
		}
	}
}

void decalDebug::StoreLastDecalBox(Mat34V_In mtx, Vec3V_In bBoxMin, Vec3V_In bBoxMax, const Color32& colour, float axisScale)
{
	if (m_numLastDecalBoxes<DECAL_MAX_LAST_DECAL_BOXES)
	{
		// correct the matrix so it's in the centre of the box
		Mat34V mtxNew = mtx;
		Vec3V vDimensions = bBoxMax - bBoxMin;
		Vec3V vCentre = bBoxMin + (vDimensions*ScalarV(V_HALF));
		Vec3V vOffset = Transform3x3(mtxNew, vCentre);
		mtxNew.SetCol3(mtxNew.GetCol3() + vOffset);

		m_lastDecalBoxes[m_numLastDecalBoxes].mtx = mtxNew;
		m_lastDecalBoxes[m_numLastDecalBoxes].vDimensions = vDimensions;
		m_lastDecalBoxes[m_numLastDecalBoxes].colour = colour;
		m_lastDecalBoxes[m_numLastDecalBoxes].axisScale = axisScale;

		m_numLastDecalBoxes++;
	}
}

void decalDebug::StoreLastDecalClipper(const decalClipper& clipper, Vec3V_In vPosition, float range) 
{
	m_lastDecalClipper = clipper; 
	m_lastDecalClipperPosition = vPosition; 
	m_lastDecalClipperRange = range;
}

void decalDebug::ForceRenderPassDeferredChanged()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceRenderPassDeferred)
	{
		self->m_forceRenderPassForward = false;
		self->m_forceRenderPassExtra1 = false;
		self->m_forceRenderPassExtra2 = false;
	}
}

void decalDebug::ForceRenderPassForwardChanged()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceRenderPassForward)
	{
		self->m_forceRenderPassDeferred = false;
		self->m_forceRenderPassExtra1 = false;
		self->m_forceRenderPassExtra2 = false;
	}
}

void decalDebug::ForceRenderPassExtra1Changed()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceRenderPassExtra1)
	{
		self->m_forceRenderPassDeferred = false;
		self->m_forceRenderPassForward = false;
		self->m_forceRenderPassExtra2 = false;
	}
}

void decalDebug::ForceRenderPassExtra2Changed()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceRenderPassExtra2)
	{
		self->m_forceRenderPassDeferred = false;
		self->m_forceRenderPassForward = false;
		self->m_forceRenderPassExtra1 = false;
	}
}

void decalDebug::ForceMethodStaticChanged()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceMethodStatic)
	{
		self->m_forceMethodDynamic = false;
	}
}

void decalDebug::ForceMethodDynamicChanged()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceMethodDynamic)
	{
		self->m_forceMethodStatic = false;
	}
}

void decalDebug::ForceStaticLodsChanged()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceStaticLods)
	{
		self->m_forceDynamicLods = false;
	}
}

void decalDebug::ForceDynamicLodsChanged()
{
	decalDebug *const self = &DECALMGR.m_debug;
	if (self->m_forceDynamicLods)
	{
		self->m_forceStaticLods = false;
	}
}

void decalDebug::Wash()
{
	decalManager *const dm = &DECALMGR;
	decalDebug *const self = &dm->m_debug;
	dm->Wash(self->m_washAmount, NULL, false);
}

void decalDebug::AddStatsWidgets(bkBank* pVfxBank, const char* pName, decalStats& stats)
{
	char txt[256];

	pVfxBank->AddTitle("");
	sprintf(txt, "Num %s Buckets [Deferred][Static]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_DEFERRED][DECAL_METHOD_STATIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Deferred][Dynamic]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_DEFERRED][DECAL_METHOD_DYNAMIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Forward][Static]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_FORWARD][DECAL_METHOD_STATIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Forward][Dynamic]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_FORWARD][DECAL_METHOD_DYNAMIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Extra1][Static]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_EXTRA_1][DECAL_METHOD_STATIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Extra1][Dynamic]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_EXTRA_1][DECAL_METHOD_DYNAMIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Extra2][Static]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_EXTRA_2][DECAL_METHOD_STATIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Buckets [Extra2][Dynamic]", pName);
	pVfxBank->AddSlider(txt, &stats.numBuckets[DECAL_RENDER_PASS_EXTRA_2][DECAL_METHOD_DYNAMIC], 0, DECAL_MAX_BUCKETS, 0);
	sprintf(txt, "Num %s Insts", pName);
	pVfxBank->AddSlider(txt, &stats.numInsts, 0, DECAL_MAX_INSTS, 0);
	sprintf(txt, "Num %s Tris", pName);
	pVfxBank->AddSlider(txt, &stats.numTris, 0, DECAL_MAX_TRIS, 0);
	sprintf(txt, "Num %s Verts", pName);
	pVfxBank->AddSlider(txt, &stats.numVerts, 0, DECAL_MAX_VERTS, 0);
}

void decalDebug::InitWidgets()
{
	bkBank* pVfxBank = vfxWidget::GetBank();
	if (pVfxBank)
	{
		pVfxBank->PushGroup("Decals", false);	
		{
			pVfxBank->PushGroup("Framework", false);	
			{
				pVfxBank->AddTitle("INFO");
				pVfxBank->AddSlider("Num Type Settings", &decalSettingsManager::ms_numTypeSettings, 0, DECAL_MAX_TYPE_SETTINGS, 0);
				pVfxBank->AddSlider("Num Render Settings", &decalSettingsManager::ms_numRenderSettings, 0, DECAL_MAX_RENDER_SETTINGS, 0);

				AddStatsWidgets(pVfxBank, "", m_statsGlobal);
				AddStatsWidgets(pVfxBank, "Fading", DECALMGR.m_statsFading);
				AddStatsWidgets(pVfxBank, "Rendered", m_statsRendered);
				AddStatsWidgets(pVfxBank, "Persistent", DECALMGR.m_statsPersistent);
				pVfxBank->AddToggle("Enable Spammy Warnings", &m_enableSpam);

				pVfxBank->AddTitle("");
				pVfxBank->AddSlider("Curr Recycle Range Ratio", &decalRecycler::ms_currRecycleRangeRatio, 0.0f, 100.0f, 0.0f);

#if __DEV
				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("SETTINGS");
				pVfxBank->AddSlider("Z Shift (Base)", &DECAL_ZSHIFT_BASE, -2.0f, 2.0f, 0.001f);
				pVfxBank->AddSlider("Max Insts To LOD Per Frame", &DECAL_MAX_INSTS_TO_LOD_PER_FRAME, 1, 1000, 1);
				pVfxBank->AddSlider("Max Insts To Un-LOD Per Frame", &DECAL_MAX_INSTS_TO_UNLOD_PER_FRAME, 1, 1000, 1);
				pVfxBank->AddToggle("Wash Underwater Insts", &DECAL_WASH_UNDERWATER_INSTS);
#endif

				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("DEBUG PIPELINE TOGGLES");
				pVfxBank->AddToggle("Force Render Pass Deferred", &m_forceRenderPassDeferred, ForceRenderPassDeferredChanged);
				pVfxBank->AddToggle("Force Render Pass Forward", &m_forceRenderPassForward, ForceRenderPassForwardChanged);
				pVfxBank->AddToggle("Force Render Pass Extra1", &m_forceRenderPassExtra1, ForceRenderPassExtra1Changed);
				pVfxBank->AddToggle("Force Render Pass Extra2", &m_forceRenderPassExtra2, ForceRenderPassExtra2Changed);
				pVfxBank->AddToggle("Force Method Static", &m_forceMethodStatic, ForceMethodStaticChanged);
				pVfxBank->AddToggle("Force Method Dynamic", &m_forceMethodDynamic, ForceMethodDynamicChanged);
				pVfxBank->AddToggle("Force Static Decals On Bounds", &m_forceStaticDecalsOnBounds);
				pVfxBank->AddToggle("Disable Poly Angle Test", &m_disablePolyAngleTest);
				pVfxBank->AddToggle("Disable Tri Clipping", &m_disableTriClipping);
				pVfxBank->AddToggle("Disable Drawable Bone Game Test", &m_disableDrawableBoneGameTest);
				pVfxBank->AddToggle("Disable Drawable Bone Bound Test", &m_disableDrawableBoneBoundTest);
				pVfxBank->AddToggle("Disable Material Test", &m_disableMaterialTest);
				pVfxBank->AddToggle("Disable Shader Test", &m_disableShaderTest);
				pVfxBank->AddToggle("Disable Bound Type Test", &m_disableBoundTypeTest);
				pVfxBank->AddToggle("Disable Lod Skeleton Mapping", &m_disableLodSkeletonMapping);
				
				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("DEBUG PIPELINE OVERRIDES");
				pVfxBank->AddSlider("Override Box Depth", &m_overrideBoxDepth, -1.0f, 2.0f, 0.01f);
				pVfxBank->AddSlider("Override Overlay Dist", &m_overrideMaxOverlayRadius, -1.0f, 10.0f, 0.01f);

				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("DEBUG RENDERING");
								
				pVfxBank->AddSlider("Render Timer", &m_renderTimer, -10000, 10000, 1);
				pVfxBank->AddToggle("Render Bounds Considered", &m_renderBoundsConsidered);
				pVfxBank->AddToggle("Render Polys Considered", &m_renderPolysConsidered);
				pVfxBank->AddToggle("Render Polys Sent To Clip", &m_renderPolysSentToClip);
				pVfxBank->AddToggle("Render Polys Clipped", &m_renderPolysClipped);
				pVfxBank->AddToggle("Render Last Decal", &m_renderLastDecal);				
				pVfxBank->AddToggle("Render Bucket Info", &m_renderBucketInfo);
				pVfxBank->AddToggle("Render Bucket Sphere", &m_renderBucketSphere);
				pVfxBank->AddToggle("Render Inst Box", &m_renderInstBox);
				pVfxBank->AddToggle("Render Inst Sphere", &m_renderInstSphere);
				pVfxBank->AddToggle("Render Inst Tris", &m_renderInstTris);
				pVfxBank->AddToggle("Bucket Colour Mode", &m_bucketColourMode);

				pVfxBank->AddTitle("");
				pVfxBank->AddTitle("DEBUG MISC");
				pVfxBank->AddToggle("Disable Adding", &m_disableAdding);
				pVfxBank->AddToggle("Disable Async Calculations", &m_disableAsync);
				pVfxBank->AddToggle("Disable Sync Calculations", &m_disableSync);
				pVfxBank->AddToggle("Disable Rendering", &m_disableRendering);
				pVfxBank->AddToggle("Disable Render Culling", &m_disableRenderCulling);
				pVfxBank->AddToggle("Disable Lods", &m_disableLods);
				pVfxBank->AddToggle("Disable Bucket Sorting", &m_disableBucketSorting);
				pVfxBank->AddToggle("Disable Merging Insts", &m_disableMergingInsts);
				pVfxBank->AddToggle("Disable Merging Verts", &m_disableMergingVerts);
				pVfxBank->AddToggle("Force Static Lods", &m_forceStaticLods, ForceStaticLodsChanged);
				pVfxBank->AddToggle("Force Dynamic Lods", &m_forceDynamicLods, ForceDynamicLodsChanged);
				pVfxBank->AddToggle("Force Washable", &m_forceWashable);
#				if DECAL_STORE_UNCOMPRESSED_VERTICES
					pVfxBank->AddToggle("Use Uncompressed Verts", &m_useUncompressedVertices);
#				endif
#				if __PS3
					pVfxBank->AddToggle("Allow RSX Local Reads", &m_allowRsxLocalReads);
#				endif
				pVfxBank->AddSlider("Model Space Offset Units", &m_modelSpaceOffsetUnits, 1.f, 65536.f, 1.f);
				pVfxBank->AddSlider("Wash Amount", &m_washAmount, 0.0f, 2.0f, 0.001f);
				pVfxBank->AddButton("Wash", datCallback(CFA1(Wash)),"");

				pVfxBank->AddToggle("Output Info When More Than One Inst Is Created Per Projection", &m_outputNumInstsCreatedThisProjection);
				pVfxBank->AddToggle("Output Num Skinned Bone Matrices Per Projection", &m_outputNumSkinnedBoneMatricesThisProjection);
				pVfxBank->AddToggle("Output Num Polys Considered Per Projection", &m_outputNumPolysConsideredThisProjection);
				pVfxBank->AddToggle("Output Num Polys Sent To Clip Per Projection", &m_outputNumPolysSentToClipThisProjection);
				pVfxBank->AddToggle("Output Num Polys Clipped Per Projection", &m_outputNumPolysClippedThisProjection);
				pVfxBank->AddToggle("Output Time To Process Per Projection", &m_outputTimeToProcessThisProjection);

				pVfxBank->AddToggle("Reset All", &m_resetAll);
			}
			pVfxBank->PopGroup();

			m_pGameGroup = pVfxBank->PushGroup("Game", false);	
			{
			}
			pVfxBank->PopGroup();
		}
		pVfxBank->PopGroup();
	}
}


} // namespace rage

#endif // __BANK

#endif // !__TOOL && !__RESOURCECOMPILER
