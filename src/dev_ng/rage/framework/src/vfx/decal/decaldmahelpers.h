//
// vfx/decal/decaldmahelpers.h
//
// Copyright (C) 2012-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECAL_DECALDMAHELPERS_H
#define VFX_DECAL_DECALDMAHELPERS_H

#if !__TOOL && !__RESOURCECOMPILER

namespace rage
{

// PURPOSE: Calculate the buffer size required for using DmaAligned()
// PARAMS:  SIZE        - minimum number of bytes to get
//          SRC_ALIGN   - garunteed effective address alignment
//          LS_EA_ALIGN - local store and effective addresses for get will be congruent mod this value
#define DMA_BUF_SIZE(SIZE, SRC_ALIGN, LS_EA_ALIGN)                             \
	(((SIZE) + 2 * ((LS_EA_ALIGN) - ((SRC_ALIGN) < (LS_EA_ALIGN) ? (SRC_ALIGN) : (LS_EA_ALIGN))) + (LS_EA_ALIGN) - 1) & ~((LS_EA_ALIGN) - 1))

// PURPOSE: Calculate the buffer size required for using DmaAligned()
// PARAMS:  TYPE        - type that will be fetched by the dma get
//          LS_EA_ALIGN - local store and effective addresses for get will be congruent mod this value
#define DMA_BUF_SIZE_TYPE(TYPE, LS_EA_ALIGN)                                   \
	DMA_BUF_SIZE(sizeof(TYPE), __alignof(TYPE), (LS_EA_ALIGN))

// This may seem completely pointless at first, but this makes the const
// associate with the first dereference, of a pointer to pointer [to pointer...]
// type, where as using macros, the const associates with the last dereference.
template<class TYPE> struct AddConstToFirstDeref;
template<class TYPE> struct AddConstToFirstDeref<TYPE*>
{
	typedef const TYPE *T;
};


////////////////////////////////////////////////////////////////////////////////
#if __SPU

#include "decaldmatags.h"
#include "system/spuscratch.h"
#include "system/dma.h"

// PURPOSE: Dma get from main into a local store buffer.
//          The local store will be aligned so that it is congruent to the effective address mod 'align'.
// PARAMS:  cmd   - (SYS_DMA_GET, SYS_DMA_GETF or SYS_DMA_GETB)
//          ls    - pointer to local store buffer.
//                  buffer size needs to be at least DMA_BUF_SIZE(size, garunteed alignment of ea, align)
//                  buffer needs to be aligned to 'align' on local store
//          ea    - effective address
//          size  - number of bytes to get
//          align - see above.  must be atleast 16.
//          tag   - dma tag
// RETURN:  pointer to start of valid data within buffer
static inline void *DmaAligned(eSysDmaCmd cmd, void *ls, uint32_t ea, uint32_t size, uint32_t align, uint32_t tag)
{
	const uint32_t getEa = ea & -align;
	const uint32_t getSize = ((ea + size + align - 1) & -align) - getEa;
	sysDma(cmd, ls, getEa, getSize, tag);
	return (void*)((uint32_t)ls + (ea & (align - 1)));
}

// PURPOSE: Dma get from main into a local store buffer.
//          The local store will be aligned so that it is congruent to the effective address mod 'align'.
// PARAMS:  ls    - pointer to local store buffer.
//                  buffer size needs to be at least DMA_BUF_SIZE(size, garunteed alignment of ea, align)
//                  buffer needs to be aligned to 'align' on local store
//          ea    - effective address
//          size  - number of bytes to get
//          align - see above.  must be atleast 16.
//          tag   - dma tag
// RETURN:  pointer to start of valid data within buffer
static inline void *DmaGetAligned(void *ls, uint32_t ea, uint32_t size, uint32_t align, uint32_t tag)
{
	return DmaAligned(SYS_DMA_GET, ls, ea, size, align, tag);
}

// PURPOSE: Fenced dma get from main into a local store buffer.
//          The local store will be aligned so that it is congruent to the effective address mod 'align'.
// PARAMS:  ls    - pointer to local store buffer.
//                  buffer size needs to be at least DMA_BUF_SIZE(size, garunteed alignment of ea, align)
//                  buffer needs to be aligned to 'align' on local store
//          ea    - effective address
//          size  - number of bytes to get
//          align - see above.  must be atleast 16.
//          tag   - dma tag
// RETURN:  pointer to start of valid data within buffer
static inline void *DmaGetFencedAligned(void *ls, uint32_t ea, uint32_t size, uint32_t align, uint32_t tag)
{
	return DmaAligned(SYS_DMA_GETF, ls, ea, size, align, tag);
}

// PURPOSE: Dma put to main where the size is not garunteed to be a multiple of 16,
//          but the alignment on local store is congruent to the effective address mod 16.
// PARAMS:  MIN_ALIGN   - minimum garunteed alignment
//          POW_2_SIZE  - largest power of 2 that always divides size
//          ls          - pointer to local store buffer.
//          ea          - effective address
//          size        - number of bytes to get
//          tag         - dma tag
template<unsigned MIN_ALIGN, unsigned POW_2_SIZE>
static void DmaPutAlignedButOddSize(const void *ls, uint32_t ea, uint32_t size, uint32_t tag)
{
	Assert(((uint32_t)ls&15) == (ea&15));

	if(MIN_ALIGN>=16 && POW_2_SIZE>=16)
	{
		sysDmaPut(ls, ea, size, tag);
	}
	else
	{
		// Align to the first 16-byte boundary
		if((ea&1) && MIN_ALIGN==1)
		{
			sysDmaSmallPut(ls, ea, 1, tag);
			ls = (char*)ls + 1;
			++ea;
			--size;
		}
		if((ea&2) && MIN_ALIGN<=2)
		{
			sysDmaSmallPut(ls, ea, 2, tag);
			ls = (char*)ls + 2;
			ea += 2;
			size -= 2;
		}
		if((ea&4) && MIN_ALIGN<=4)
		{
			sysDmaSmallPut(ls, ea, 4, tag);
			ls = (char*)ls + 4;
			ea += 4;
			size -= 4;
		}
		if((ea&8) && MIN_ALIGN<=8)
		{
			sysDmaSmallPut(ls, ea, 8, tag);
			ls = (char*)ls + 8;
			ea += 8;
			size -= 8;
		}

		// Put multiples of 16-bytes
		if(size>=16)
		{
			const uint32_t size16 = size & ~15;
		 	sysDmaPut(ls, ea, size16, tag);
			ls = (char*)ls + size16;
			ea += size16;
			size -= size16;
		}

		// Put remainder
		if(size>=8 && ( MIN_ALIGN<=8 || POW_2_SIZE<=8 ))
		{
			sysDmaSmallPut(ls, ea, 8, tag);
			ls = (char*)ls + 8;
			ea += 8;
			size -= 8;
		}
		if(size>=4 && ( MIN_ALIGN<=4 || POW_2_SIZE<=4 ))
		{
			sysDmaSmallPut(ls, ea, 4, tag);
			ls = (char*)ls + 4;
			ea += 4;
			size -= 4;
		}
		if(size>=2 && ( MIN_ALIGN<=2 || POW_2_SIZE<=2 ))
		{
			sysDmaSmallPut(ls, ea, 2, tag);
			ls = (char*)ls + 2;
			ea += 2;
			size -= 2;
		}
		if(size && ( MIN_ALIGN==1 || POW_2_SIZE==1 ))
		{
			sysDmaSmallPut(ls, ea, 1, tag);
			ASSERT_ONLY(--size);
		}
		Assert(!size);
	}
}

// PURPOSE: Dma put to main where the size is not garunteed to be a multiple of 16,
//          but the alignment on local store is congruent to the effective address mod 16.
// PARAMS:  ls   - pointer to local store buffer.
//          ea   - effective address
//          tag  - dma tag
template<class T>
static inline void DmaPutAlignedButOddSize(const T *ls, uint32_t ea, uint32_t tag)
{
	// Note that we clamp the MIN_ALIGN and POW_2_SIZE template arguments to 16
	// to prevent template bload when not inlined
	DmaPutAlignedButOddSize<
		__alignof__(T)<=16       ? __alignof__(T)       : 16,
		sizeof(T)&-sizeof(T)<=16 ? sizeof(T)&-sizeof(T) : 16
		>(ls, ea, sizeof(T), tag);
}

#define DMA_GET(TYPE, NAME, EA, TAG)                                           \
	DMA_GET_(TYPE, NAME, (EA), (TAG), sizeof(TYPE)<128?16:128, __LINE__)
#define DMA_GET_(TYPE, NAME, EA, TAG, ALIGN, UNIQUE)                           \
	DMA_GET__(TYPE, NAME, (EA), (TAG), (ALIGN), UNIQUE)
#define DMA_GET__(TYPE, NAME, EA, TAG, ALIGN, UNIQUE)                          \
	char dmaGetBuf_##UNIQUE[ DMA_BUF_SIZE_TYPE(TYPE, (ALIGN)) ] ALIGNED(ALIGN);\
	/* the const_cast here is to check we have a sensible pointer type */      \
	/* before we C-cast it to a uint32_t. */                                   \
	AddConstToFirstDeref<TYPE*>::T const NAME                                  \
		= (AddConstToFirstDeref<TYPE*>::T)DmaGetAligned(                       \
			dmaGetBuf_##UNIQUE, (uint32_t)const_cast<TYPE*>(EA), sizeof(TYPE), \
			(ALIGN), (TAG))

#define BLOCKING_DMA_GET(TYPE, NAME, EA)                                       \
	DMA_GET(TYPE, NAME, (EA), DMATAG_BLOCKING);                                \
	sysDmaWaitTagStatusAll(1 << DMATAG_BLOCKING)


#define DMA_GET_ARRAY(TYPE, NAME, COUNT, EA, TAG)                              \
	DMA_GET_ARRAY_(TYPE, NAME, (COUNT), (EA), (TAG), __LINE__)
#define DMA_GET_ARRAY_(TYPE, NAME, COUNT, EA, TAG, UNIQUE)                     \
	DMA_GET_ARRAY__(TYPE, NAME, (COUNT), (EA), (TAG), UNIQUE)
#define DMA_GET_ARRAY__(TYPE, NAME, COUNT, EA, TAG, UNIQUE)                    \
	const rage::sysScratchScope dmaGetScopedResetScratch_##UNIQUE;             \
	void *const dmaGetBuf_##UNIQUE                                             \
		= sysScratchAllocAligned(                                              \
			DMA_BUF_SIZE((COUNT)*sizeof(TYPE), __alignof__(TYPE), 128), 128);  \
	AddConstToFirstDeref<TYPE*>::T const NAME                                  \
		= (AddConstToFirstDeref<TYPE*>::T)DmaGetAligned(                       \
			dmaGetBuf_##UNIQUE, (uint32_t)const_cast<TYPE*>(EA),               \
			sizeof(TYPE)*(COUNT), 128, (TAG))

#define BLOCKING_DMA_GET_ARRAY(TYPE, NAME, COUNT, EA)                          \
	checkSpursMarkers("before get array");                                     \
	DMA_GET_ARRAY(TYPE, NAME, (COUNT), (EA), DMATAG_BLOCKING);                 \
	sysDmaWaitTagStatusAll(1 << DMATAG_BLOCKING);                              \
	checkSpursMarkers("after get array")



// PURPOSE: Helper class to make pipelined dma loops simpler
template<class T, unsigned DEPTH>
class PipelinedDmaGetter
{
public:

	// PARAMS:  firstTag_   - use dma tags from [firstTag_, firstTag_+DEPTH-1]
	explicit PipelinedDmaGetter(unsigned firstTag_)
		: prefetchIdx(0)
		, futureIdx(0)
		, currentIdx(0)
		, firstTag(firstTag_)
	{
	}

	~PipelinedDmaGetter()
	{
		sysDmaWaitTagStatusAll(((1<<DEPTH)-1)<<firstTag);
	}

	// PURPOSE: Prefetch to back of the queue
	// PARAMS:  ea  - effective address
	void Prefetch(const T *ea)
	{
		PrefetchInternal(ea, SYS_DMA_GET);
	}

	// PURPOSE: Access the second most recent prefetched item
	//          Name comes from fact that this is generally called to assist in
	//          prefetching dependant data for a future loop iteration
	const T *GetFuture()
	{
		const T *const ret = ptr[futureIdx];
		sysDmaWaitTagStatusAll(1<<(firstTag+futureIdx));
		futureIdx = (futureIdx+1) & -(futureIdx<DEPTH-1);
		return ret;
	}

	// PURPOSE: Access the oldest prefetched item
	//          Name comes from fact that this is generally called to access the
	//          item for the current loop iteration
	const T *GetCurrent()
	{
		const T *const ret = ptr[currentIdx];
		currentIdx = (currentIdx+1) & -(currentIdx<DEPTH-1);
		return ret;
	}


protected:

	void PrefetchInternal(const T *ea, eSysDmaCmd mfcCmd)
	{
		if(Likely(ea))
		{
			ptr[prefetchIdx] = (T*)DmaGetAligned(mfcCmd, buf[prefetchIdx], (uint32_t)ea, sizeof(T), 16, firstTag+prefetchIdx);
		}
		else
		{
			ptr[prefetchIdx] = NULL;
		}
		prefetchIdx = (prefetchIdx+1) & -(prefetchIdx<DEPTH-1);
	}

	unsigned GetPrefetchIdx() const
	{
		return prefetchIdx;
	}

	unsigned GetPrevGetIdx() const
	{
		return currentIdx ? (currentIdx-1) : (DEPTH-1);
	}

	const T *PeekCurrent() const
	{
		return ptr[GetPrevGetIdx()];
	}

	unsigned GetFirstTag() const
	{
		return firstTag;
	}


private:

	char buf[DEPTH][DMA_BUF_SIZE_TYPE(T,16)] ;
	const T *ptr[DEPTH];
	unsigned prefetchIdx;
	unsigned futureIdx;
	unsigned currentIdx;
	unsigned firstTag;
};


// Specialization for DEPTH=2 (ie, double-bufferred).
// Has a slightly different interface to reflect the different usage pattern.
template<class T>
class PipelinedDmaGetter<T,2>
{
public:

	explicit PipelinedDmaGetter(unsigned firstTag_)
		: prefetchIdx(0)
		, getIdx(0)
		, firstTag(firstTag_)
	{
	}

	~PipelinedDmaGetter()
	{
		sysDmaWaitTagStatusAll(3<<firstTag);
	}

	void Prefetch(const T *ea)
	{
		if(Likely(ea))
		{
			// Because Skip() may be called instead of Get(), this get needs to be fenced
			ptr[prefetchIdx] = (T*)DmaGetFencedAligned(buf[prefetchIdx], (uint32_t)ea, sizeof(T), 16, firstTag+prefetchIdx);
		}
		else
		{
			ptr[prefetchIdx] = NULL;
		}
		prefetchIdx ^= 1;
	}

	const T *Get()
	{
		const T *const ret = ptr[getIdx];
		sysDmaWaitTagStatusAll(1<<(firstTag+getIdx));
		getIdx ^= 1;
		return ret;
	}

	void Skip()
	{
		getIdx ^= 1;
	}


protected:

	unsigned GetPrefetchIdx() const
	{
		return prefetchIdx;
	}

	unsigned GetPrevGetIdx() const
	{
		return getIdx^1;
	}

	const T *Peek() const
	{
		return ptr[GetPrevGetIdx()];
	}

	unsigned GetFirstTag() const
	{
		return firstTag;
	}


private:

	char buf[2][DMA_BUF_SIZE_TYPE(T,16)] ;
	const T *ptr[2];
	unsigned prefetchIdx;
	unsigned getIdx;
	unsigned firstTag;
};


// PURPOSE: Extended version of PipelinedDmaGetter that remembers the effective addresses.
template<class T, unsigned DEPTH>
class PipelinedDmaGetterEa : public PipelinedDmaGetter<T, DEPTH>
{
public:

	explicit PipelinedDmaGetterEa(unsigned firstTag_)
		: PipelinedDmaGetter<T, DEPTH>(firstTag_)
	{
	}

	void Prefetch(const T *ea)
	{
		eas[PipelinedDmaGetter<T, DEPTH>::GetPrefetchIdx()] = (u32)ea;
		PipelinedDmaGetter<T, DEPTH>::Prefetch(ea);
	}

	// PURPOSE: Get the effective address that corresponds to the most recent
	//          GetCurrent()/Get() or Skip() call.
	uptr GetEa() const
	{
		return eas[PipelinedDmaGetter<T, DEPTH>::GetPrevGetIdx()];
	}


private:

	u32 eas[DEPTH];
};


// PURPOSE: Extended version of PipelinedDmaGetterEa that allows modification to the data being iterated over.
template<class T, unsigned DEPTH>
class PipelinedDmaAccessor : public PipelinedDmaGetterEa<T, DEPTH>
{
public:

	explicit PipelinedDmaAccessor(unsigned firstTag_)
		: PipelinedDmaGetterEa<T, DEPTH>(firstTag_)
	{
	}

	void Prefetch(T *ea)
	{
		// Need to use a fenced get to make sure we don't overwrite the buffer
		// before the dma from any previous Put() call completes.
		PipelinedDmaGetterEa<T, DEPTH>::PrefetchInternal(ea, SYS_DMA_GETF);
	}

	T *GetCurrent()
	{
		return const_cast<T*>(PipelinedDmaGetterEa<T, DEPTH>::GetCurrent());
	}

	void Put()
	{
		const T *const ls = PipelinedDmaGetterEa<T, DEPTH>::PeekCurrent();
		const u32 ea = PipelinedDmaGetterEa<T, DEPTH>::GetEa();
		const u32 tag = PipelinedDmaGetterEa<T, DEPTH>::GetFirstTag() + PipelinedDmaGetterEa<T, DEPTH>::GetPrevGetIdx();
		DmaPutAlignedButOddSize(ls, ea, tag);
	}
};

template<class T>
class PipelinedDmaAccessor<T,2> : public PipelinedDmaGetterEa<T,2>
{
public:

	explicit PipelinedDmaAccessor(unsigned firstTag_)
		: PipelinedDmaGetterEa<T,2>(firstTag_)
	{
	}

	void Prefetch(T *ea)
	{
		// PipelinedDmaGetterEa<T,2>::Prefetch is already fenced unlike DEPTH!=2
		PipelinedDmaGetterEa<T,2>::Prefetch(ea);
	}

	T *Get()
	{
		return const_cast<T*>(PipelinedDmaGetterEa<T,2>::Get());
	}

	void Put()
	{
		const T *const ls = PipelinedDmaGetterEa<T,2>::Peek();
		const u32 ea = PipelinedDmaGetterEa<T,2>::GetEa();
		const u32 tag = PipelinedDmaGetterEa<T,2>::GetFirstTag() + PipelinedDmaGetterEa<T,2>::GetPrevGetIdx();
		DmaPutAlignedButOddSize(ls, ea, tag);
	}
};


////////////////////////////////////////////////////////////////////////////////
#else // !__SPU

#define DMA_GET(TYPE, NAME, EA, unused_TAG)                                    \
	(void)(unused_TAG);                                                        \
	AddConstToFirstDeref<TYPE*>::T const NAME = (EA)

#define BLOCKING_DMA_GET(TYPE, NAME, EA)                                       \
	DMA_GET(TYPE, NAME, EA, 0/*unused_TAG*/)

#define DMA_GET_ARRAY(TYPE, NAME, unused_COUNT, EA, unused_TAG)                \
	(void)(unused_COUNT);                                                      \
	(void)(unused_TAG);                                                        \
	AddConstToFirstDeref<TYPE*>::T const NAME = (EA)

#define BLOCKING_DMA_GET_ARRAY(TYPE, NAME, unused_COUNT, EA)                   \
	(void)(unused_COUNT);                                                      \
	DMA_GET_ARRAY(TYPE, NAME, unused_COUNT, EA, 0/*unused_TAG*/)


template<class T, unsigned DEPTH>
class PipelinedDmaGetter
{
public:

	explicit PipelinedDmaGetter(unsigned)
		: prefetchIdx(0)
		, futureIdx(0)
		, currentIdx(0)
	{
	}

	void Prefetch(const T *ea)
	{
		ptr[prefetchIdx] = ea;
		prefetchIdx = (prefetchIdx+1) & -(prefetchIdx<DEPTH-1);
	}

	const T *GetFuture()
	{
		const T *const ret = ptr[futureIdx];
		futureIdx = (futureIdx+1) & -(futureIdx<DEPTH-1);
		return ret;
	}

	const T *GetCurrent()
	{
		const T *const ret = ptr[currentIdx];
		currentIdx = (currentIdx+1) & -(currentIdx<DEPTH-1);
		return ret;
	}


protected:

	const T *GetEa() const
	{
		return ptr[currentIdx ? (currentIdx-1) : (DEPTH-1)];
	}


private:

	const T *ptr[DEPTH];
	unsigned prefetchIdx;
	unsigned futureIdx;
	unsigned currentIdx;
};


template<class T>
class PipelinedDmaGetter<T,2>
{
public:

	explicit PipelinedDmaGetter(unsigned)
		: prefetchIdx(0)
		, getIdx(0)
	{
	}

	void Prefetch(const T *ea)
	{
		ptr[prefetchIdx] = ea;
		prefetchIdx ^= 1;
	}

	const T *Get()
	{
		const T *const ret = ptr[getIdx];
		getIdx ^= 1;
		return ret;
	}

	void Skip()
	{
		getIdx ^= 1;
	}


protected:

	const T *GetEa() const
	{
		return ptr[getIdx^1];
	}


private:

	const T *ptr[2];
	unsigned prefetchIdx;
	unsigned getIdx;
};


template<class T, unsigned DEPTH>
class PipelinedDmaGetterEa : public PipelinedDmaGetter<T, DEPTH>
{
public:

	explicit PipelinedDmaGetterEa(unsigned firstTag_)
		: PipelinedDmaGetter<T, DEPTH>(firstTag_)
	{
	}

	uptr GetEa() const
	{
		return (uptr)(PipelinedDmaGetter<T, DEPTH>::GetEa());
	}
};


template<class T, unsigned DEPTH>
class PipelinedDmaAccessor : public PipelinedDmaGetterEa<T, DEPTH>
{
public:

	explicit PipelinedDmaAccessor(unsigned firstTag_)
		: PipelinedDmaGetterEa<T, DEPTH>(firstTag_)
	{
	}

	void Prefetch(T *ea)
	{
		PipelinedDmaGetterEa<T, DEPTH>::Prefetch(ea);
	}

	T *GetCurrent()
	{
		return const_cast<T*>(PipelinedDmaGetterEa<T, DEPTH>::GetCurrent());
	}

	void Put()
	{
	}
};

template<class T>
class PipelinedDmaAccessor<T,2> : public PipelinedDmaGetterEa<T,2>
{
public:

	explicit PipelinedDmaAccessor(unsigned firstTag_)
		: PipelinedDmaGetterEa<T,2>(firstTag_)
	{
	}

	void Prefetch(T *ea)
	{
		PipelinedDmaGetterEa<T,2>::Prefetch(ea);
	}

	T *Get()
	{
		return const_cast<T*>(PipelinedDmaGetterEa<T,2>::Get());
	}

	void Put()
	{
	}
};


#endif // !__SPU

}
// namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECAL_DECALDMAHELPERS_H
