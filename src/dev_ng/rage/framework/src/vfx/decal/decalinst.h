//
// vfx/decal/decalinst.h
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALINST_H
#define	VFX_DECALINST_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "math/float16.h"
#include "vectormath/classes.h"


// includes (framework)
#include "vfx/channel.h"
#include "vfx/slist.h"
#include "vfx/decal/decalhelper.h"
#include "vfx/decal/decalsettings.h"
#include "vfx/decal/decalvertex.h"
#if __BANK
#	include "vfx/decal/decalclipper.h"
#endif


// namespaces
namespace rage {


// forward declarations
class decalBucket;
class decalMiniIdxBufWriter;
class decalRenderStaticBatcher;
class decalSettings;
class fwTexturePayload;
class grcViewport;

// defines
#define DECAL_INST_FADE_TIME_UNITS_HZ           (16.f)
#define DECAL_INST_INV_FADE_TIME_UNITS_HZ       (1.f/DECAL_INST_FADE_TIME_UNITS_HZ)

#define DECAL_INST_WASH_MAX_VALUE               (1.2f)


// classes
class decalInst : public SListItem<decalInst>
{
	friend class decalBucket;
	friend class decalManager;
	friend class decalManagerAsync;
	friend class decalLodSystem;
	friend class decalRecycler;


	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		enum decalState
		{
			DECAL_STATE_ALIVE = 0,
			DECAL_STATE_DEAD,
			DECAL_STATE_FADEOUT
		};
		void Init(const decalSettings& settings);

		enum ShutdownImmediateReleaseDecalId
		{
			SHUTDOWN_IMMEDIATE_DONT_RELEASE_DECAL_ID,
			SHUTDOWN_IMMEDIATE_RELEASE_DECAL_ID,
		};
		void ShutdownImmediate(ShutdownImmediateReleaseDecalId releaseDecalId);

#		if !__SPU
			void InitForLodReduction(const decalInst *pOther);
#		endif

		inline int GetNumTris()  const {return m_numTris;}
		inline int GetNumVerts() const {return m_numVtxs;}

		inline u16 GetMibs() const {return m_idxBufList;}

		bool ReserveTris(unsigned count, decalMiniIdxBufWriter *miniIdxBufWriter);
		u16 *AddTri(decalMiniIdxBufWriter *miniIdxBufWriter);

		inline ScalarV_Out GetBoundRadius() const {return m_boundRadius.GetFloat32_FromFloat16_ScalarV();}

		inline Vec3V_Out GetBoundCentre() const
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreY) == OffsetOf(decalInst, m_boundCentreX)+4);
			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreZ) == OffsetOf(decalInst, m_boundCentreX)+8);
			Vector_4V centre = V4LoadUnaligned(&m_boundCentreX);
			return Vec3V(centre);
		}

		inline Vec4V_Out GetBoundCentreAndRadius() const
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreY) == OffsetOf(decalInst, m_boundCentreX)+4);
			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreZ) == OffsetOf(decalInst, m_boundCentreX)+8);
			Vector_4V centre = V4LoadUnaligned((const char*)&m_boundCentreX-4);
			CompileTimeAssert(OffsetOf(decalInst, m_boundRadius)+16 < sizeof(decalInst));
			Vector_4V rad = V4LoadLeftUnsafe(&m_boundRadius);
			rad = V4Float16Vec4UnpackFromXY(rad);
			Vector_4V sphere = V4PermuteTwo<Y1,Z1,W1,X2>(centre,rad);
			return Vec4V(sphere);
		}

		inline Vec3V_Out GetDimensions() const
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst, m_dimensionsY) == OffsetOf(decalInst, m_dimensionsX)+2);
			CompileTimeAssert(OffsetOf(decalInst, m_dimensionsZ) == OffsetOf(decalInst, m_dimensionsX)+4);
			Vector_4V dimensions = V4LoadUnaligned(&m_dimensionsX);
			return Vec3V(V4Float16Vec4UnpackFromXY(dimensions));
		}

		inline void SetBoundRadius(ScalarV_In val)
		{
			m_boundRadius.SetFloat16_FromFloat32(val);
		}

		inline void SetBoundCentre(Vec3V_In val)
		{
			using namespace Vec;
			decalAssert(V3IsFiniteAll(val.GetIntrin128()));
			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreY) == OffsetOf(decalInst, m_boundCentreX)+4);
			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreZ) == OffsetOf(decalInst, m_boundCentreX)+8);
			Vector_4V oldCentre = V4LoadUnaligned(&m_boundCentreX);
			Vector_4V v = V4PermuteTwo<X1,Y1,Z1,W2>(val.GetIntrin128(), oldCentre);
			V4StoreUnaligned(&m_boundCentreX, v);
			Vector_4V diff = V4Subtract(val.GetIntrin128(), oldCentre);
			Vector_4V posOffs = GetPositionOffset().GetIntrin128();
			posOffs = V4Add(posOffs, diff);
			SetPositionOffset(Vec3V(posOffs));
		}

		inline void SetBoundCentreAndRadius(Vec4V_In centreAndRadius)
		{
// MN - this is commented out as it doesn't work as expected - the radius is getting messed up
// 			using namespace Vec;
// 			decalAssert(V4IsFiniteAll(centreAndRadius.GetIntrin128()));
// 			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreY) == OffsetOf(decalInst, m_boundCentreX)+4);
// 			CompileTimeAssert(OffsetOf(decalInst, m_boundCentreZ) == OffsetOf(decalInst, m_boundCentreX)+8);
// 			CompileTimeAssert(OffsetOf(decalInst, m_boundRadius) == OffsetOf(decalInst, m_boundCentreX)+12);
// 			Vector_4V oldCentre = V4LoadUnaligned(&m_boundCentreX);
// 			Vector_4V sphere = centreAndRadius.GetIntrin128();
// 			Vector_4V radF32 = V4SplatZ(sphere);
// 			Vector_4V radF16 = V4Float16Vec8Pack(radF32, radF32);
// 			sphere = V4PermuteTwo<X1,Y1,Z1,W2>(sphere, radF16);
// # if __BE
// 			Vector_4V v = V4SelectFT(V4VConstant<0,0,0,0x0000ffff>(), sphere, oldCentre);
// # else
// 			Vector_4V v = V4SelectFT(V4VConstant<0,0,0,0xffff0000>(), sphere, oldCentre);
// # endif
// 			V4StoreUnaligned(&m_boundCentreX, v);
// 			Vector_4V diff = V4Subtract(centreAndRadius.GetIntrin128(), oldCentre);
// 			Vector_4V posOffs = GetPositionOffset().GetIntrin128();
// 			posOffs = V4Add(posOffs, diff);
// 			SetPositionOffset(Vec3V(posOffs));
			SetBoundCentre(centreAndRadius.GetXYZ());
			SetBoundRadius(centreAndRadius.GetW());
		}

		inline float GetFadeInTime()  const {return ((float)m_fadeInTime +1.f)*DECAL_INST_INV_FADE_TIME_UNITS_HZ;}
		inline float GetFadeOutTime() const {return ((float)m_fadeOutTime+1.f)*DECAL_INST_INV_FADE_TIME_UNITS_HZ;}
		inline void SetFadeInTime (float t) {u32 u=(u32)Max((t-DECAL_INST_INV_FADE_TIME_UNITS_HZ)*DECAL_INST_FADE_TIME_UNITS_HZ,0.f); m_fadeInTime  = (u8)Min(u,0xffu);}
		inline void SetFadeOutTime(float t) {u32 u=(u32)Max((t-DECAL_INST_INV_FADE_TIME_UNITS_HZ)*DECAL_INST_FADE_TIME_UNITS_HZ,0.f); m_fadeOutTime = (u8)Min(u,0xffu);}

		inline void StartFadingOut() {if (!IsFadingOut()) m_currLife=m_totalLife-GetFadeOutTime();}
		inline void StartFadingOut(float fadeOutTime) {SetFadeOutTime(fadeOutTime); StartFadingOut();}

		inline float GetDistToViewport() const {return m_distToViewport.GetFloat32_FromFloat16();}
		inline bool IsDistanceCulled() const {return m_isDistanceCulled;}
		inline bool IsViewportCulled() const {return m_isViewportCulled;}
		inline bool IsCulled() const {return IsDistanceCulled() || IsViewportCulled() || IsExpired();}

		inline bool IsPerformUnderWaterCheck() const {return m_doUnderwaterCheck; }

		// utility functions
		inline void Wash(float amount) {SetWashLevel(Max(GetWashLevel()-amount, 0.f));}
		inline bool IsInRange(Vec3V_In vPos, ScalarV_In rangeSqr, Mat34V_In bucketWldMtx) {return IsLessThanAll(MagSquared(Transform(bucketWldMtx, GetBoundCentre())-vPos), rangeSqr) != 0;}

		inline bool IsMarkedForDelete() const {return m_markedForDelete==1;}
		inline bool IsPendingMarkedForDelete() const {return m_markedForDelete!=0;}
		inline void	MarkForDelete(unsigned delayedNumFrames=0)	{m_markedForDelete=(u8)(delayedNumFrames+1);}
		inline bool ProcessMarkedForDelete() {if(m_markedForDelete)return !--m_markedForDelete;return false;}
		inline bool GetIsPrimaryInst() const {return m_isPrimaryInst;}
		inline void SetIsPrimaryInst(bool primary) {m_isPrimaryInst=primary;}

#		if !__SPU
			decalState Update(float deltaTime, const decalBucket* pBucket, float animTime, const grcViewport &pBucketVP, 
				Vec3V_In camPosBucketSpace, ScalarV_In fovScaleSqr, ScalarV_In cullDistance, bool bPerformUnderwaterTest, bool bAllowedToRecycle, bool isWashable, bool bValidVP);

			void Render_Static(decalRenderStaticBatcher* batcher, u8 texRows, u8 texCols, u8 texIdA, u8 texIdB, float animTime, float fadeDistance, float cullDistance) const;
			void Render_Dynamic(Mat34V_In bucketWldMtx, u8 texRows, u8 texCols, u8 texIdA, u8 texIdB, float animTime, float texWrapLength, float fadeDistance, float cullDistance, float alphaMult) const;
#			if __BANK
				void RenderDebug(Mat34V_In boundWldMtx, ScalarV_In posScale, Vec3V_In msOffsetFlt, Color32 debugCol) const;
#			endif

			float CalcUVMult() const;
#		endif // !__SPU


	private: //////////////////////////

		inline bool IsFadingIn() const {return m_currLife<GetFadeInTime();}
		inline bool IsFadingOut() const {return m_currLife>m_totalLife-GetFadeOutTime();}
		inline bool IsFadingOut(float fadeOutTime) const {return m_currLife>m_totalLife-fadeOutTime;}
		inline bool IsFadingOut(float fadeOutTime, float currLife, float totalLife) const {return currLife>totalLife-fadeOutTime;}

		inline bool IsExpired() const
		{
			typedef u32 MAY_ALIAS u32_alias;
			const u32 currLife  = *(const u32_alias*)&m_currLife;
			const u32 totalLife = *(const u32_alias*)&m_totalLife;
			// Ensure sign bits of the floats are clear, otherwise comparing as integers won't work correctly
			decalAssert((s32)currLife  >= 0);
			decalAssert((s32)totalLife >= 0);
			return currLife > totalLife;
		}

		inline float GetWashLevel() const {return (float)m_washLevel*(DECAL_INST_WASH_MAX_VALUE/(float)0xffff);}
		inline void SetWashLevel(float f) {m_washLevel = (u16)(f*((float)0xffff/DECAL_INST_WASH_MAX_VALUE));}

		inline Vec3V_Out GetPositionOffset() const
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst,m_positionOffsetY) == OffsetOf(decalInst,m_positionOffsetX)+2);
			CompileTimeAssert(OffsetOf(decalInst,m_positionOffsetZ) == OffsetOf(decalInst,m_positionOffsetX)+4);
			Vector_4V po = V4LoadUnaligned(&m_positionOffsetX);
			po = V4UnpackLowSignedShort(po);
			po = V4IntToFloatRaw<15>(po);
			return Vec3V(po);
		}

		inline void SetPositionOffset(Vec3V_In posOffs)
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst,m_positionOffsetY) == OffsetOf(decalInst,m_positionOffsetX)+2);
			CompileTimeAssert(OffsetOf(decalInst,m_positionOffsetZ) == OffsetOf(decalInst,m_positionOffsetX)+4);
			Vector_4V v = V4LoadUnaligned(&m_positionOffsetX);
			Vector_4V po = V4FloatToIntRaw<15>(posOffs.GetIntrin128());
			po = V4PackSignedIntToSignedShort(po, po);
		#if __BE
			v = V4SelectFT(V4VConstant<0x00000000,0x0000ffff,0xffffffff,0xffffffff>(), po, v);
		#else
			v = V4SelectFT(V4VConstant<0x00000000,0xffff0000,0xffffffff,0xffffffff>(), po, v);
		#endif
			V4StoreUnaligned(&m_positionOffsetX, v);
		}

		inline Vec3V_Out GetPosition() const
		{
			return GetBoundCentre() + GetPositionOffset();
		}

		inline Vec3V_Out GetDirection() const
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst,m_directionY) == OffsetOf(decalInst,m_directionX)+2);
			CompileTimeAssert(OffsetOf(decalInst,m_directionZ) == OffsetOf(decalInst,m_directionX)+4);
			Vector_4V dir = V4LoadUnaligned(&m_directionX);
			dir = V4UnpackLowSignedShort(dir);
			dir = V4IntToFloatRaw<15>(dir);
			return Vec3V(dir);
		}

		inline Vec3V_Out GetSide() const
		{
			using namespace Vec;
			CompileTimeAssert(OffsetOf(decalInst,m_sideY) == OffsetOf(decalInst,m_sideX)+2);
			CompileTimeAssert(OffsetOf(decalInst,m_sideZ) == OffsetOf(decalInst,m_sideX)+4);
			Vector_4V side = V4LoadUnaligned(&m_sideX);
			side = V4UnpackLowSignedShort(side);
			side = V4IntToFloatRaw<15>(side);
			return Vec3V(side);
		}


	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

		int m_id;

		float m_totalLife;
		float m_currLife;

//		Vec3V m_vPassedJoinVerts[4];										// only required if trail decals no longer merge insts causing them to go through the lod system

		float m_currWrapLength;

		//
		float m_boundCentreX;
		float m_boundCentreY;
		float m_boundCentreZ;
		Float16 m_boundRadius;

		// dynamic decal specific
		Float16 m_positionOffsetX;
		Float16 m_positionOffsetY;
		Float16 m_positionOffsetZ;
		s16 m_directionX;
		s16 m_directionY;
		s16 m_directionZ;
		s16 m_sideX;
		s16 m_sideY;
		s16 m_sideZ;
		Float16 m_dimensionsX;
		Float16 m_dimensionsY;
		Float16 m_dimensionsZ;

		Float16 m_distToViewport;

		Float16 m_uvMultStart;
		Float16 m_uvMultEnd;
		Float16 m_uvMultTime;

		// lists
		u16 m_idxBufList;
		u16 m_vtxList;
		u16 m_numTris;
		u16 m_numVtxs;

		u16 m_washLevel;

		u8 m_fadeInTime;
		u8 m_fadeOutTime;
		s8 m_liquidType;
		u8 m_colR;
		u8 m_colG;
		u8 m_colB;
		u8 m_alphaFront;
		u8 m_alphaBack;
		unsigned m_markedForDelete : 3;
		bool m_isDistanceCulled : 1;
		bool m_isViewportCulled : 1;
		bool m_isLod : 1;
		bool m_isPrimaryInst : 1;
		bool m_isLiquidPool : 1;
		bool m_flipU : 1;
		bool m_flipV : 1;
		bool m_singleFrame : 1;
		bool m_doUnderwaterCheck : 1;

#if __BANK
		decalJoinType m_joinType;
#endif
};


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALINST_H
