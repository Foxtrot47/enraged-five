//
// vfx/decal/decalbucket.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalbucket.h"

// includes (rage)
#if DECAL_PROCESS_BREAKABLES
#	include "breakableglass/glassmanager.h"
#endif
#include "grcore/allocscope.h"
#include "grcore/debugdraw.h"
#include "grcore/effect.h"
#include "grcore/effect_values.h"
#include "math/random.h"
#include "phbound/boundcomposite.h"
#include "physics/inst.h"
#include "system/nelem.h"

// includes (framework)
#include "fwsys/glue.h"
#include "entity/entity.h"
#include "vfx/channel.h"
#include "vfx/vfxutil.h"
#include "vfx/optimisations.h"
#include "vfx/decal/decalasynctaskdescbase.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decalhelper.h"
#include "vfx/decal/decalinst.h"
#include "vfx/decal/decallod.h"
#include "vfx/decal/decalpools.h"
#if !__SPU
#	include "vfx/vfxwidget.h"
#	include "vfx/decal/decalmanager.h"
#	include "vfx/decal/decalmanagerasync.h"
#	include "vfx/decal/decalrenderstaticbatcher.h"
#endif


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


#define DECAL_MERGE_INST_RANGE (1.0f)


// global variables
dev_float DECAL_ZSHIFT_BASE	= 0.040f;
dev_bool DECAL_WASH_UNDERWATER_INSTS = true;


// code
void decalBucket::Init(decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, ScalarV_In posScale, Vec3V_In msOffsetInt, Vec4V_In boundSphere, Mat34V_In wldMtx, s16 attachMatrixId, decalAttachType attachType, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted, void* pSmashGroup)
{
	using namespace Vec;

	// Store the model space offset before any of the flags since we are going
	// to stomp on the following byte.
	CompileTimeAssert((OffsetOf(decalBucket, m_modelSpaceOffsetX) & 3) == 0);
	CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetY) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 1);
	CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetZ) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 2);
	Vector_4V msoi = V4FloatToIntRaw<0>(msOffsetInt.GetIntrin128());
#	if RSG_PS3 || RSG_XENON
		msoi = V4BytePermute<3,7,11,15,3,7,11,15,3,7,11,15,3,7,11,15>(msoi);
		typedef u32 MAY_ALIAS u32_alias;
		V4StoreScalar32FromSplatted(*(u32_alias*)&m_modelSpaceOffsetX, msoi);
#	else
		msoi = V4PackSignedIntToSignedShort(msoi, msoi);
		msoi = V4PackSignedShortToSignedByte(msoi, msoi);
		V4Store32<0>(&m_modelSpaceOffsetX, msoi);
#	endif

	InitCommon(taskDescEa, pDecalInstEa, boundSphere, wldMtx, attachMatrixId, attachType, typeSettingsIndex, renderSettingsIndex, subType, isGlass, isDamaged, isPersistent, interiorLocation, isScripted, pSmashGroup);

	Vec::V4StoreScalar32FromSplatted(m_posScale, posScale.GetIntrin128());
}

void decalBucket::Init(decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, float posScale, u32 msOffsetInt, Vec4V_In boundSphere, Mat34V_In wldMtx, s16 attachMatrixId, decalAttachType attachType, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted, void* pSmashGroup)
{
	// Store the model space offset before any of the flags since we are going
	// to stomp on the following byte.
	CompileTimeAssert((OffsetOf(decalBucket, m_modelSpaceOffsetX) & 3) == 0);
	CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetY) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 1);
	CompileTimeAssert(OffsetOf(decalBucket, m_modelSpaceOffsetZ) == OffsetOf(decalBucket, m_modelSpaceOffsetX) + 2);
	typedef u32 MAY_ALIAS u32_alias;
#	if __BE
		*(u32_alias*)&m_modelSpaceOffsetX = msOffsetInt << 8;
#	else
		*(u32_alias*)&m_modelSpaceOffsetX = msOffsetInt >> 8;
#	endif

	InitCommon(taskDescEa, pDecalInstEa, boundSphere, wldMtx, attachMatrixId, attachType, typeSettingsIndex, renderSettingsIndex, subType, isGlass, isDamaged, isPersistent, interiorLocation, isScripted, pSmashGroup);

	m_posScale = posScale;
}

void decalBucket::InitCommon(decalAsyncTaskDescBase* taskDescEa, decalInst* pDecalInstEa, Vec4V_In boundSphere, Mat34V_In wldMtx, s16 attachMatrixId, decalAttachType attachType, u16 typeSettingsIndex, u16 renderSettingsIndex, u16 subType, bool isGlass, bool isDamaged, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted, void* pSmashGroup)
{
	m_wldMtx = wldMtx;
	m_boundSphere = boundSphere;

	// taskDesc may validly be NULL on input
	m_taskDesc = taskDescEa;

#	if __SPU
		// SPU code has a fwEntity*, not a fwRegdRef<fwEntity>, so need to manually initialize to NULL.
		m_attachEntity = NULL;
#	else
		// For non-SPU code, the fwRegdRef<fwEntity> ctor should have taken care of this.
		decalAssert(!m_attachEntity);
#	endif

	m_attachMatrixId = attachMatrixId;
	m_attachType = attachType;
	m_pSmashGroup = pSmashGroup;
	m_pDamageMapData = NULL;
	u8 localBucketFlags = 0U;
	localBucketFlags |= isGlass?BUCKETFLAG_IS_GLASS:0;
	localBucketFlags |= isPersistent?BUCKETFLAG_IS_PERSISTENT:0;
	localBucketFlags |= isScripted?BUCKETFLAG_IS_SCRIPTED:0;
	localBucketFlags |= BUCKETFLAG_IS_ENTITYVISIBLETHISFRAME;
	m_isDamaged = isDamaged;
	m_forceRegen = false;
	m_typeSettingsIndex = typeSettingsIndex;
	m_renderSettingsIndex = renderSettingsIndex;
	m_subType = subType;
	m_bucketFlags = localBucketFlags;
	m_ambient[0] = 1.0f;
	m_ambient[1] = 1.0f;
	m_interiorLocation = interiorLocation;

	decalAssertf(m_instList.GetNumItems()==0, "inst list not empty");
	decalAssert(pDecalInstEa);
	m_instList.SetHead(pDecalInstEa);
	m_instList.SetSize(1);

	DECALMGRASYNC.AddDecalIdInstance(pDecalInstEa->m_id);
}

void decalBucket::ExpandBoundSphere(const decalInst* pInstLs)
{
	ExpandBoundSphere(pInstLs->GetBoundCentreAndRadius());
}

void decalBucket::ExpandBoundSphere(Vec4V_In boundSphere)
{
	spdSphere sphere(m_boundSphere);
	sphere.GrowSphere(spdSphere(boundSphere));
	m_boundSphere = sphere.GetV4();
}

#if !__SPU

	bool decalBucket::UpdateMatrix(bool* isDamaged, const fwEntity* pEnt, const phInst* pInst)
	{
		return decalHelper::CalcMatrix(m_wldMtx, isDamaged, pEnt, pInst, m_attachMatrixId, m_attachType);
	}

	void decalBucket::AddInst(decalInst* pInstNew)
	{
		m_instList.Push(pInstNew);
		DECALMGRASYNC.AddDecalIdInstance(pInstNew->m_id);
		ExpandBoundSphere(pInstNew);
	}

	void decalBucket::ShutdownImmediate()
	{
		decalManager *const dm = &DECALMGR;
		Assertf(IsMarkedForDelete(), "decalBucket has not been previously marked for delete");

		// reset delete flag
		m_bucketFlags &= ~BUCKETFLAG_IS_MARKEDFORDELETE;

		// shutdown any active insts
		decalInst *const pHead = m_instList.Peek();
		if(pHead)
		{
			decalInst *pTail;
			decalInst *pTailNext = pHead;
			unsigned count = 0;
			do
			{
				pTail = pTailNext;
				ASSERT_ONLY(pTail->MarkForDelete();)
				pTail->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_RELEASE_DECAL_ID);
				++count;
				pTailNext = pTailNext->GetNext();
			}
			while(pTailNext);
			dm->m_pools.ReturnInsts(pHead, pTail, count);
			m_instList.SetEmpty();
		}
	}

	void decalBucket::ShutdownDelayed()
	{
		decalManager *const dm = &DECALMGR;
		Assertf(IsMarkedForDelete(), "decalBucket has not been previously marked for delete");

		// reset delete flag
		m_bucketFlags &= ~BUCKETFLAG_IS_MARKEDFORDELETE;

		// shutdown any active insts
		dm->DelayedDeletion(m_instList.Peek(), m_instList.ScanTail(), m_instList.GetNumItems());
		m_instList.SetEmpty();
	}

	bool decalBucket::Update_BeforeAdd()
	{
		// check if we should be attached to an entity
		if (m_attachType!=DECAL_ATTACH_TYPE_NONE)
		{
			// check if the entity we're attached to still exists
			if (!m_attachEntity)
			{
				// entity no longer exists - this bucket is finished with
				MarkForDelete();
				return true;
			}
			// check if the physics instance is still valid
			else if (g_pDecalCallbacks->GetPhysInstFromEntity(m_attachEntity)==NULL
				&& (m_attachType==DECAL_ATTACH_TYPE_COMPONENT
#if DECAL_PROCESS_BREAKABLES
				    || m_attachType==DECAL_ATTACH_TYPE_BREAKABLE_UNBROKEN
#endif
				))
			{
				// physics instance no longer valid - this bucket is finished with
				MarkForDelete();
				return true;
			}
#if DECAL_PROCESS_BREAKABLES
			// check if broken breakable is still valid
			else if (m_attachType==DECAL_ATTACH_TYPE_BREAKABLE_BROKEN)
			{
				if (bgGlassManager::IsHandleValid((bgGlassHandle)m_attachMatrixId)==false)
				{
					// broken breakable no longer valid - this bucket is finished with
					MarkForDelete();
					return true;
				}
			}
#endif
// 			// check if the drawable is still valid
// 			else if (m_attachEntity->GetDrawable()==NULL && (m_attachType==DECAL_ATTACH_TYPE_BONE || m_attachType==DECAL_ATTACH_TYPE_BONE_SKINNED))
// 			{
// 				// drawable no longer valid - this bucket is finished with
// 				Shutdown();
// 				return true;
// 			}
// 			// check if a dynamic entity has had its matrices updated - otherwise the texture could be floating in mid air
// 			else if (m_attachEntity->GetIsDynamic() && m_matrixIndex>0)
// 			{
// 				CDynamicEntity* pDynEntity = static_cast<CDynamicEntity*>(m_attachEntity.Get());
// 				if (pDynEntity->GetFragInst()==NULL && pDynEntity->GetGlobalMatricesUpdatedThisFrame()==false)
// 				{
// 					// dynamic entity hasn't had its matrices updated - this bucket is finished with
// 					Shutdown();
// 					return true;
// 				}
// 			}

			g_pDecalCallbacks->GetDamageData(m_attachEntity, &m_pDamageMapData, m_damageRadius, m_damageMult);
			bool isInInterior = false;
			g_pDecalCallbacks->GetAmbientFromEntity(m_attachEntity, m_ambient[0], m_ambient[1], isInInterior);
			m_isInInterior = isInInterior;
			m_isForceAlpha = m_attachEntity ? m_attachEntity->IsBaseFlagSet(fwEntity::FORCE_ALPHA) : false;
			m_entityAlpha = m_attachEntity ? m_attachEntity->GetAlpha() : 255;
		}

		// update the current matrix
		// entity pointer is allowed to be null when DECAL_ATTACH_TYPE_NONE
		fwEntity* const pEntity = m_attachEntity.Get();
		decalAssert(pEntity || m_attachType==DECAL_ATTACH_TYPE_NONE);
		if (pEntity)
		{
			bool isDamaged;
			if (Unlikely(!UpdateMatrix(&isDamaged, pEntity, g_pDecalCallbacks->GetPhysInstFromEntity(pEntity))))
			{
				MarkForDelete();

				decalAssertf(IsFiniteAll(m_wldMtx), "decal bucket matrix isn't finite 1");
				return true;
			}
#if __ASSERT
			else
			{
				decalAssertf(IsFiniteAll(m_wldMtx), "decal bucket matrix isn't finite 3 (pEntity:%p, attachType:%d)", pEntity, m_attachType);
			}
#endif

			// check for a frag instance being broken off
			if (m_attachEntity)
			{
				if (IsZeroAll(m_wldMtx.GetCol0()))
				{
					// try to find in our stored array
					bool updatedEntityPtr = false;
					for (int i=0; i<vfxUtil::GetNumBrokenFrags(); i++)
					{
						vfxBrokenFragInfo& brokenFragInfo = vfxUtil::GetBrokenFragInfo(i);
						if (brokenFragInfo.pParentEntity==m_attachEntity && brokenFragInfo.pChildEntity!=NULL)
						{
							// get the child matrix
							bool ignoredIsDamaged;
							Mat34V childMat = Mat34V(V_IDENTITY);
							decalHelper::CalcMatrix(childMat, &ignoredIsDamaged, brokenFragInfo.pChildEntity, g_pDecalCallbacks->GetPhysInstFromEntity(brokenFragInfo.pChildEntity), m_attachMatrixId, m_attachType);
							if (!IsZeroAll(childMat.GetCol0()))
							{
								// replace with the child
								m_attachEntity = brokenFragInfo.pChildEntity;

								updatedEntityPtr = true;
							}
						}
					}

					if (!updatedEntityPtr)
					{
						MarkForDelete();
					}
				}
			}

			if (m_isDamaged != isDamaged)
			{
				m_isDamaged = isDamaged;
				m_forceRegen = true;
			}
		}

		decalAssertf(IsFiniteAll(m_wldMtx), "decal bucket matrix isn't finite 2 (pEntity:%p, attachType:%d)", pEntity, m_attachType);
		return false;
	}

	bool decalBucket::Update_AfterAdd(float deltaTime, decalRenderPass renderPass, decalMethod method, const grcViewport* pActiveGameVP, Vec4V_In gameCamPosFOVScale)
	{
		u8 localbucketFlags = m_bucketFlags;
		// init the cull state
		localbucketFlags |= (BUCKETFLAG_IS_FADINGOUT | BUCKETFLAG_IS_CULLED);

		// return if the bucket is empty
		if (m_instList.IsEmpty())
		{
			return true;
		}

		const bool isEntityVisibleThisFrame = m_attachEntity && g_pDecalCallbacks->IsEntityVisible(m_attachEntity);
		if (isEntityVisibleThisFrame)
		{
			localbucketFlags |= BUCKETFLAG_IS_ENTITYVISIBLETHISFRAME;
		}
		else
		{
			localbucketFlags &= ~BUCKETFLAG_IS_ENTITYVISIBLETHISFRAME;
		}
		Mat34V invWldMtx;
		InvertTransformOrtho(invWldMtx, m_wldMtx);

		grcViewport bucketViewport;

		decalAssertf(IsFiniteAll(m_wldMtx), "decal bucket matrix isn't finite");
		const bool bValidVP = pActiveGameVP?true:false;
		if(pActiveGameVP)
		{
			bucketViewport = *pActiveGameVP;
			Mat34V bucketCamMatrix;
			Transform(bucketCamMatrix, invWldMtx, pActiveGameVP->GetCameraMtx());
			bucketViewport.SetCameraMtx(bucketCamMatrix); 
		}
		const Vec3V camPosBucketSpace = Transform(invWldMtx, gameCamPosFOVScale.GetXYZ());
		const ScalarV fovScaleSqr = gameCamPosFOVScale.GetW() * gameCamPosFOVScale.GetW();
		bool isInInterior = false;
		g_pDecalCallbacks->GetAmbientFromEntity(m_attachEntity, m_ambient[0], m_ambient[1], isInInterior);
		m_isInInterior = isInInterior;
		m_isForceAlpha = m_attachEntity ? m_attachEntity->IsBaseFlagSet(fwEntity::FORCE_ALPHA) : false;
		m_entityAlpha = m_attachEntity ? m_attachEntity->GetAlpha() : 255;

		//get information about bucket to pass to decalInst::Update
		const ScalarV cullDistance = ScalarVFromF32(decalSettingsManager::GetTypeSettings(m_typeSettingsIndex)->cullDistance*g_pDecalCallbacks->GetDistanceMult(m_typeSettingsIndex));
		const bool bPerformUnderWaterTestForBucket = DECAL_WASH_UNDERWATER_INSTS && FadesUnderwater();
		const bool bAllowedToRecycle = g_pDecalCallbacks->IsAllowedToRecycle(m_typeSettingsIndex);
		const bool bIsWashable = IsWashable();
		const float animTime = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex)->animTime;
		// update the insts
		decalInst *pInst = m_instList.Peek();
		while (pInst)
		{
			//Adding a condition check here as prefecthing NULL can slow things down to 
			//due to an ERAT miss on PPC. Adding Likely so that it would always take this
			//branch and mispredict only on the last step
			if(Likely(pInst->GetNext()))
			{
				PrefetchDC(pInst->GetNext());
			}
			// fade if underwater
			const decalInst::decalState decalInstState = pInst->Update(deltaTime, this, animTime, bucketViewport, 
				camPosBucketSpace, fovScaleSqr, cullDistance, bPerformUnderWaterTestForBucket, bAllowedToRecycle, bIsWashable, bValidVP);

			if(decalInstState == decalInst::DECAL_STATE_DEAD)
			{
				// the inst is finished with
				pInst->MarkForDelete();
			}
			else
			{
				//IsCulled call contains IsViewportCulled, IsDistanceCulled and IsExpired
				//IsExpired is a little expensive, and at this point we already know 
				//the it is not expired. So splitting up the call
				//if (!pInst->IsCulled())
				if(!(pInst->IsViewportCulled() || pInst->IsDistanceCulled()))
				{
					localbucketFlags &= ~(BUCKETFLAG_IS_CULLED);

				}

				if (decalInstState == decalInst::DECAL_STATE_ALIVE)
				{
					localbucketFlags &= ~(BUCKETFLAG_IS_FADINGOUT);
				}
			}

			pInst = pInst->GetNext();
		}

		//set flags back to member variable before checking them
		m_bucketFlags = localbucketFlags;

		decalManager *const dm = &DECALMGR;

		// update stats for non empty buckets
		if (IsPersistent())
		{
			dm->m_statsPersistent.numBuckets[renderPass][method]++;
			dm->m_statsPersistent.numInsts += m_instList.GetNumItems();
			dm->m_statsPersistent.numTris  += GetNumTris();
			dm->m_statsPersistent.numVerts += GetNumVerts();
		}

		if (IsFadingOut())
		{
			dm->m_statsFading.numBuckets[renderPass][method]++;
		}

#if __BANK
		if (!GetIsCulled())
		{
			dm->m_debug.m_statsRendered.numBuckets[renderPass][method]++;
		}
#endif

		return false;
	}

	int decalBucket::GetNumTris() const
	{
		int numTris = 0;
		const decalInst *pInst = m_instList.Peek();
		while (pInst)
		{
			numTris += pInst->GetNumTris();
			pInst = pInst->GetNext();
		}
		return numTris;
	}

	int decalBucket::GetNumDynamicInstsToRender() const
	{
		int numInsts = 0;
		const decalInst *pInst = m_instList.Peek();
		while (pInst)
		{
			numInsts += pInst->GetIsPrimaryInst() && !pInst->IsCulled();
			pInst = pInst->GetNext();
		}
		return numInsts;
	}

	int decalBucket::GetNumVerts() const
	{
		int numVerts = 0;
		const decalInst *pInst = m_instList.Peek();
		while (pInst)
		{
			numVerts += pInst->GetNumVerts();
			pInst = pInst->GetNext();
		}
		return numVerts;
	}

	void decalBucket::Render_Static(bool bIsForwardPass DECAL_MULTI_THREADED_RENDERING_ONLY(, unsigned ctxIdx)) const
	{
		using namespace Vec;

		// return if the attach entity is no longer valid
		if (!m_attachEntity)
		{
			return;
		}

		if (!IsEntityVisibleThisFrame())
		{
			return;
		}

		int numInsts = m_instList.GetNumItems();
		if (numInsts>0)
		{
			GRC_ALLOC_SCOPE_AUTO_PUSH_POP()

			decalManager *const dm = &DECALMGR;

			Mat34V wldMtx = m_wldMtx;
			Vec3V msOffsetFlt = GetModelSpaceOffsetInt() * dm->GetModelSpaceOffsetUnits();
			wldMtx[3] += Transform3x3(wldMtx, msOffsetFlt);
			grcWorldMtx(RCC_MATRIX34(wldMtx));

			// setup the lighting - if required
			if (bIsForwardPass)
			{
				g_pDecalCallbacks->InitBucketForwardRender(GetBoundSphere(), GetInteriorLocation());
			}
			g_pDecalCallbacks->SetGlobals(m_ambient[0], m_ambient[1], m_isInInterior);

			const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex);
			const decalTypeSettings* pTypeSettings = decalSettingsManager::GetTypeSettings(m_typeSettingsIndex);

			if (pRenderSettings->pDiffuseMap==const_cast<grcTexture*>(grcTexture::None))
			{
				return;
			}

			// Remap from the shader id stored in the render settings to the
			// shader id we want to use at runtime.  The value can change when
			// the decal is projected onto glass.
			CompileTimeAssert(DECAL_STATIC_SHADER_PARALLAX              == 0);
			CompileTimeAssert(DECAL_STATIC_SHADER_PARALLAX_STEEP        == 1);
			CompileTimeAssert(DECAL_STATIC_SHADER_PARALLAX_GLASS        == 2);
			CompileTimeAssert(DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP  == 3);
			CompileTimeAssert(DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA == 4);
			CompileTimeAssert(DECAL_NUM_STATIC_SHADERS                  == 5);
			static const u8 s_shaderIdRemap[2][5] =
			{
				// non-glass
				{
					DECAL_STATIC_SHADER_PARALLAX,               // DECAL_STATIC_SHADER_PARALLAX
					DECAL_STATIC_SHADER_PARALLAX_STEEP,         // DECAL_STATIC_SHADER_PARALLAX_STEEP
					DECAL_NUM_STATIC_SHADERS,                   // DECAL_STATIC_SHADER_PARALLAX_GLASS       (should never be stored in decalRenderSettings)
					DECAL_NUM_STATIC_SHADERS,                   // DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP (should never be stored in decalRenderSettings)
					DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA,  // DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA
				},

				// glass
				{
					DECAL_STATIC_SHADER_PARALLAX_GLASS,         // DECAL_STATIC_SHADER_PARALLAX
					DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP,   // DECAL_STATIC_SHADER_PARALLAX_STEEP
					DECAL_NUM_STATIC_SHADERS,                   // DECAL_STATIC_SHADER_PARALLAX_GLASS       (should never be stored in decalRenderSettings)
					DECAL_NUM_STATIC_SHADERS,                   // DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP (should never be stored in decalRenderSettings)
					DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA,  // DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA
				},
			};
			const unsigned shaderId = s_shaderIdRemap[IsGlass()][pRenderSettings->shaderId];
			decalAssert(shaderId < (unsigned)DECAL_NUM_STATIC_SHADERS);
			decalShader_Static *const shader = dm->m_shaderStatic + shaderId;

			// push the texture sampler state
			grcSamplerStateHandle currSamplerState = dm->m_samplerStateClamp;
			bool bUseAniso = (pTypeSettings->useAniso || pRenderSettings->useAniso);
			if (pTypeSettings->wrapUV)
			{
				currSamplerState = (bUseAniso ? dm->m_samplerStateAnisoWrap : dm->m_samplerStateWrap);
			}
			else
			{
				currSamplerState = (bUseAniso ? dm->m_samplerStateAnisoClamp : dm->m_samplerStateClamp);
			}
			shader->PushSamplerState(currSamplerState
#				if !USE_EDGE
					, dm->m_samplerStateDamage
#				endif
				);


			// set the local vars
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				const ScalarV posScale = ScalarV(V4SelectFT(V4IsTrueV(decalVtx::UseUncompressedVertices()), GetPosScale().GetIntrin128(), V4VConstant(V_ONE)));
#			else
				const ScalarV posScale = GetPosScale();
#			endif
#			if !__FINAL && !__PROFILE
				// Check we are not attempting to render with an dangling
				// texture handle.  The -1 in the comparison here is to allow
				// for NULLs.
				if (Unlikely(
					(uptr)static_cast<grcTexture*>(pRenderSettings->pDiffuseMap)  -1 < 0x10000u ||
					(uptr)static_cast<grcTexture*>(pRenderSettings->pNormalMap)   -1 < 0x10000u ||
					(uptr)static_cast<grcTexture*>(pRenderSettings->pSpecularMap) -1 < 0x10000u ))
				{
					// In the past when this has been seen, it has been a script
					// bug.  Displaying the id "should" be enough to track it
					// down.  Quitf here, as the bug MUST be fixed, and it
					// is going to crash anyways.
					Quitf("Attempted to render a decal with a dangling texture handle.  ID=%u.", pRenderSettings->id);
				}
#			endif

			// We avoid merging buckets with
			// DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA so that we only need to
			// set the rotation center once per bucket (see
			// decalManager::GetMergableBucket).
			const decalInst *pInst = m_instList.Peek();
			decalAssert(pInst);
			const Vec3V rotationCenter = pInst->GetPosition() - msOffsetFlt;
			decalAssert(pRenderSettings->shaderId!=DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA || numInsts==1);

			bool alignToCamRot = false;
			if (pRenderSettings->shaderId==DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA && m_subType==1)
			{
				alignToCamRot = true;
			}

			if (m_pDamageMapData)
			{
				shader->SetPerBucketVarsLocal(pRenderSettings->pDiffuseMap, pRenderSettings->pNormalMap, pRenderSettings->pSpecularMap, pRenderSettings->specFalloff, pRenderSettings->specIntensity, pRenderSettings->specFresnel, pRenderSettings->parallaxScale, DECAL_ZSHIFT_BASE+pTypeSettings->zShiftOffset, true, m_pDamageMapData->GetTexture(), m_damageRadius, m_damageMult, pRenderSettings->emissiveness, posScale, rotationCenter, alignToCamRot PS3_ONLY(, 1.f));
			}
			else
			{
				shader->SetPerBucketVarsLocal(pRenderSettings->pDiffuseMap, pRenderSettings->pNormalMap, pRenderSettings->pSpecularMap, pRenderSettings->specFalloff, pRenderSettings->specIntensity, pRenderSettings->specFresnel, pRenderSettings->parallaxScale, DECAL_ZSHIFT_BASE+pTypeSettings->zShiftOffset, false, NULL, 0.0f, 1.0f, pRenderSettings->emissiveness, posScale, rotationCenter, alignToCamRot PS3_ONLY(, 0.f));
			}


			const int shaderPass = 0;
			if (Likely(shader->Start(shaderPass)))
			{
				decalRenderStaticBatcher batcher(shader DECAL_MULTI_THREADED_RENDERING_ONLY(, ctxIdx));

				while (pInst)
				{
					const decalInst *const pNext = pInst->GetNext();
					if (pNext)
					{
						PrefetchDC(pNext);
					}
					if (!pInst->IsCulled())
					{
						float cullDistance = pTypeSettings->cullDistance * g_pDecalCallbacks->GetDistanceMult(m_typeSettingsIndex);
						float fadeDistance = pTypeSettings->fadeDistance * g_pDecalCallbacks->GetDistanceMult(m_typeSettingsIndex);
						pInst->Render_Static(&batcher, pRenderSettings->texRows, pRenderSettings->texColumns, pRenderSettings->texIdA, pRenderSettings->texIdB, pRenderSettings->animTime, fadeDistance, cullDistance);
					}
					pInst = pNext;
				}
			}

			shader->PopSamplerState();
			shader->End();
			shader->UnbindTextures();
		}
	}

	void decalBucket::Render_Dynamic(bool bIsForwardPass) const
	{
		decalManager *const dm = &DECALMGR;

#if DECAL_USE_INDEXED_VERTS
		const u32 vertexSize = sizeof(dynDecalVtx);
		const u32 maxVtxBatchSize = 1023;
#else
		// set up the tri strip index info
		const u32 maxVtxBatchSize = dm->m_vtxBufferDynamic.GetBatchSize();
#endif

		// return if the attach entity is no longer valid
		if (!m_attachEntity)
		{
			return;
		}

		if (!IsEntityVisibleThisFrame())
		{
			return;
		}

		// only for Dynamic forward do we check this.
		if( m_isForceAlpha != bIsForwardPass )
		{
			return;
		}

		int numInsts = m_instList.GetNumItems();
		if (numInsts>0)
		{
			GRC_ALLOC_SCOPE_AUTO_PUSH_POP()

			const decalTypeSettings* pTypeSettings = decalSettingsManager::GetTypeSettings(m_typeSettingsIndex);
			float cullDistance = pTypeSettings->cullDistance * g_pDecalCallbacks->GetDistanceMult(m_typeSettingsIndex);
			float fadeDistance = pTypeSettings->fadeDistance * g_pDecalCallbacks->GetDistanceMult(m_typeSettingsIndex);
			float alphaMult = 1.0f;

			if (bIsForwardPass)
			{
				g_pDecalCallbacks->InitBucketForwardRender(GetBoundSphere(), GetInteriorLocation());
				if( m_isForceAlpha )
				{
					alphaMult = (float)(m_entityAlpha)/255.0f;		
				}
			}
			g_pDecalCallbacks->SetGlobals(m_ambient[0], m_ambient[1], m_isInInterior);
			
			const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex);
			int shaderId = pRenderSettings->shaderId;
			Assert(shaderId < DECAL_NUM_DYNAMIC_SHADERS);

			if (pRenderSettings->pDiffuseMap==const_cast<grcTexture*>(grcTexture::None))
			{
				return;
			}

			grcViewport *const vp = grcViewport::GetCurrent();
			const Mat34V origViewInv = vp->GetCameraMtx();
			Mat34V viewInv;
			viewInv.SetCol0(origViewInv.GetCol0());
			viewInv.SetCol1(origViewInv.GetCol1());
			viewInv.SetCol2(origViewInv.GetCol2());
			viewInv.SetCol3(Vec3V(V_ZERO));
			vp->SetCameraMtx(viewInv);
			vp->SetWorldIdentity();

			Mat34V world;
			world.SetCol0(m_wldMtx.GetCol0());
			world.SetCol1(m_wldMtx.GetCol1());
			world.SetCol2(m_wldMtx.GetCol2());
			world.SetCol3(m_wldMtx.GetCol3() - origViewInv.GetCol3());

			// push the texture sampler state
			grcSamplerStateHandle currSamplerState = dm->m_samplerStateClamp;
			bool bUseAniso = (pTypeSettings->useAniso || pRenderSettings->useAniso);
			if (pTypeSettings->wrapUV)
			{
				currSamplerState = (bUseAniso ? dm->m_samplerStateAnisoWrap : dm->m_samplerStateWrap);
			}
			else
			{
				currSamplerState = (bUseAniso ? dm->m_samplerStateAnisoClamp : dm->m_samplerStateClamp);
			}


			// set the local vars
			dm->m_shaderDynamic[shaderId].SetVarsLocal(pRenderSettings->pDiffuseMap, pRenderSettings->pNormalMap, pRenderSettings->specFalloff, pRenderSettings->specIntensity, pRenderSettings->specFresnel, pRenderSettings->parallaxScale, pRenderSettings->emissiveness, pTypeSettings->polyRejectThreshold);
			dm->m_shaderDynamic[shaderId].PushSamplerState(currSamplerState);

			if (Likely(dm->m_shaderDynamic[shaderId].Start(1)))
			{
				grcStateBlock::Flush();

#if DECAL_USE_INDEXED_VERTS
				decalDrawInterface::BeginDynamicDecals();
#else
				dm->m_vtxBufferDynamic.Start();
#endif
				u32 numInstsLeftToRender = GetNumDynamicInstsToRender();
				if (numInstsLeftToRender>0)
				{
					const decalInst *pInst = m_instList.Peek();

					while (numInstsLeftToRender>0)
					{
						decalAssert(pInst);

						u32 numInstsToRenderThisPass = numInstsLeftToRender;
						if ((numInstsToRenderThisPass*DECAL_NUM_DYNAMIC_INST_VERTS)>maxVtxBatchSize)
						{
							numInstsToRenderThisPass = maxVtxBatchSize/DECAL_NUM_DYNAMIC_INST_VERTS;
						}

#if DECAL_USE_INDEXED_VERTS
						const u32 numVertices = numInstsToRenderThisPass*DECAL_NUM_DYNAMIC_INST_VERTS;
						const u32 numIndices = numInstsToRenderThisPass*DECAL_NUM_DYNAMIC_INST_IDX;
						decalDrawInterface::BeginIndexedVertices(numVertices, numIndices, vertexSize);
#else
						// render the decal insts
						dm->m_vtxBufferDynamic.Lock(numInstsToRenderThisPass*DECAL_NUM_DYNAMIC_INST_VERTS);
#endif
						u32 numInstsRenderedThisPass = 0;
						while (numInstsRenderedThisPass<numInstsToRenderThisPass)
						{
							if (pInst->GetIsPrimaryInst() && !pInst->IsCulled())
							{

#if DECAL_USE_INDEXED_VERTS
								decalDrawInterface::CopyDynamicDecalIndexedData(numInstsRenderedThisPass);
#endif
								pInst->Render_Dynamic(world, pRenderSettings->texRows, pRenderSettings->texColumns, pRenderSettings->texIdA, pRenderSettings->texIdB, pRenderSettings->animTime, pRenderSettings->wrapLength, fadeDistance, cullDistance, alphaMult);
								numInstsRenderedThisPass++;
							}
							pInst = pInst->GetNext();
						}

#if DECAL_USE_INDEXED_VERTS
						decalDrawInterface::EndIndexedVertices();
#else
						dm->m_vtxBufferDynamic.UnLock();
#endif
						numInstsLeftToRender -= numInstsToRenderThisPass;
					}
				}

#if DECAL_USE_INDEXED_VERTS
				decalDrawInterface::EndDynamicDecals();
#else
				dm->m_vtxBufferDynamic.End();
#endif
			}

			vp->SetCameraMtx(origViewInv);

			dm->m_shaderDynamic[shaderId].PopSamplerState();
			dm->m_shaderDynamic[shaderId].End();
			dm->m_shaderDynamic[shaderId].UnbindTextures();
		}
	}


#if __BANK
	void decalBucket::RenderDebug() const
	{
		GRC_ALLOC_SCOPE_AUTO_PUSH_POP()

		Color32 debugCol(1.0f, 1.0f, 0.5f, 1.0f);
		if (DECALMGRASYNC.m_debug.m_bucketColourMode)
		{
			g_DrawRand.Reset((u32)(size_t)this);
			u8 colR = (u8)g_DrawRand.GetRanged(0, 255);
			u8 colG = (u8)g_DrawRand.GetRanged(0, 255);
			u8 colB = (u8)g_DrawRand.GetRanged(0, 255);

			debugCol = Color32(colR, colG, colB, 255);
		}

		// render the bucket info
		if (DECALMGRASYNC.m_debug.m_renderBucketInfo)
		{
			Vec4V sphere = GetBoundSphere();
			Vec3V vCentre = sphere.GetXYZ();

			const decalRenderSettings* pRenderSettings = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex);

			char displayTxt[1024];
			char tempTxt[1024];

			displayTxt[0] = '\0';

			formatf(tempTxt, 256, "decalid:  %d\n", pRenderSettings->id);
			formatf(displayTxt, 256, "%s", tempTxt);

			formatf(tempTxt, 256, "%sdiffuse:  %s\n", displayTxt, pRenderSettings->pDiffuseMap->GetName());
			formatf(displayTxt, 256, "%s", tempTxt);

			formatf(tempTxt, 256, "%snormal:   %s\n", displayTxt, pRenderSettings->pNormalMap->GetName());
			formatf(displayTxt, 256, "%s", tempTxt);

			formatf(tempTxt, 256, "%sspecular: %s\n", displayTxt, pRenderSettings->pSpecularMap->GetName());
			formatf(displayTxt, 256, "%s", tempTxt);

			formatf(tempTxt, 256, "%srow:      %d\n", displayTxt, pRenderSettings->texRows);
			formatf(displayTxt, 256, "%s", tempTxt);

			formatf(tempTxt, 256, "%scolumn:   %d\n", displayTxt, pRenderSettings->texColumns);
			formatf(displayTxt, 256, "%s", tempTxt);

			grcDebugDraw::Text(vCentre, debugCol, 0, 10, displayTxt, false);

			const decalInst *pInst = m_instList.Peek();
			while (pInst)
			{
				Vec3V vInstPos = pInst->GetBoundCentre();
				vInstPos = Transform(m_wldMtx, vInstPos);
				grcDebugDraw::Line(vCentre, vInstPos, debugCol);
				pInst = pInst->GetNext();
			}
		}

		// render the bucket bound sphere
		if (DECALMGRASYNC.m_debug.m_renderBucketSphere)
		{
			Vec4V sphere = GetBoundSphere();
			Vec3V vCentre = sphere.GetXYZ();
			float radius = sphere.GetWf();

			grcDebugDraw::Sphere(vCentre, radius, debugCol, false);
		}

		// render the insts
		const ScalarV posScale = GetPosScale();
		const Vec3V msOffsetFlt = GetModelSpaceOffsetInt() * DECALMGRASYNC.GetModelSpaceOffsetUnits();
		const decalInst *pInst = m_instList.Peek();
		while (pInst)
		{
			pInst->RenderDebug(m_wldMtx, posScale, msOffsetFlt, debugCol);
			pInst = pInst->GetNext();
		}
	}
#endif

	void decalBucket::MergeInsts(decalMethod method, SList<decalInst>* pList, Vec4V_In boundSphere)
	{
		// check if this bucket accepts merged insts
		bool tryToMergeInsts = false;
		if (method==DECAL_METHOD_STATIC)
		{
			const decalTypeSettings* pTypeSettings = decalSettingsManager::GetTypeSettings(m_typeSettingsIndex);
			if (pTypeSettings)
			{
				tryToMergeInsts = pTypeSettings->canMergeInsts;
			}
		}

		decalManager *const dm = &DECALMGR;

#if __BANK
		if (dm->m_debug.m_disableMergingInsts)
		{
			tryToMergeInsts = false;
		}
#endif

		// Keep track of the current last instance in the list so that we can
		// append new ones to the end.  While not perfect, this does generally
		// help to keep the expected draw order of new instances rendering on
		// top of old ones.
		//
		// This is not perfect though, since
		//  - There is no garunteed ordering between buckets that may overlap
		//  - When instances are merged, the new triangles are added to the end,
		//    not the start.  This one could theoretically be fixed with a fair
		//    bit of work.
		decalInst *currLastInst = m_instList.ScanTail();

		if (!tryToMergeInsts)
		{
			m_instList.AppendTail(currLastInst, *pList);
		}
		else
		{
			decalMiniIdxBuf *const pMibBase = dm->m_pools.GetMibPoolBase();
			// decalVtx        *const pVtxBase = dm->m_pools.GetVtxPoolBase();
			const decalMiniIdxBufSListPtrConverter mibConv(pMibBase);
			// const SListIdxPtrConverter<decalVtx,u16> vtxConv = MakeSListIdxPtrConverter<u16>(pVtxBase);

			decalInst *pInstNew = pList->Peek();
			while (pInstNew)
			{
				decalInst *const pInstNewNext = pInstNew->GetNext();

				// go through the current insts looking for the closest compatible one
				float closestDistSqr = DECAL_MERGE_INST_RANGE*DECAL_MERGE_INST_RANGE;
				decalInst *pInstToMergeInto = NULL;

				decalInst *pDecalInst = m_instList.Peek();
				while (pDecalInst)
				{
					// we must be able to merge with this inst and it must have the correct colour properties
					if (pDecalInst->m_colR == pInstNew->m_colR &&
						pDecalInst->m_colG == pInstNew->m_colG &&
						pDecalInst->m_colB == pInstNew->m_colB &&
						pDecalInst->m_alphaBack	== pDecalInst->m_alphaFront &&
						pInstNew->m_alphaBack == pInstNew->m_alphaFront &&
						pDecalInst->m_alphaBack	== pInstNew->m_alphaBack)
					{
						// check distance from bound centre to the new data
						Vec3V vDist = pDecalInst->GetBoundCentre() - pInstNew->GetBoundCentre();
						float distSqr = MagSquared(vDist).Getf();

						// update the closest info if this one is closer
						if (distSqr<closestDistSqr)
						{
							closestDistSqr = distSqr;
							pInstToMergeInto = pDecalInst;
						}
					}

					// set the next inst
					pDecalInst = pDecalInst->GetNext();
				}

				// nothing to merge with ?
				if (!pInstToMergeInto)
				{
					m_instList.AppendTail(currLastInst, pInstNew);
					currLastInst = pInstNew;
				}
				else
				{
					unsigned numNewVtxs = pInstNew->m_numVtxs;

// Vertices are never getting merged, so lets remove this additional but pointless complication
#if 0
					BANK_ONLY(if (!dm->m_debug.m_disableMergingVerts))
					{
						for (decalTri *pFromTri=pInstNew->m_triList.Peek(triConv); pFromTri; pFromTri=pFromTri->GetNext(triConv))
						{
							for (decalTri *pToTri=pInstToMergeInto->m_triList.Peek(triConv); pToTri; pToTri=pToTri->GetNext(triConv))
							{
								for (unsigned i=0; i<3; ++i)
								{
									decalVtx *const pFromVtx = pFromTri->GetVertex(i, pVtxBase);
									decalVtx *const pToVtx = pToTri->GetVertex(i, pVtxBase);
									// Check the pointers don't match, as this vertex may have already been merged
									if (pFromTri != pToTri)
									{
										if (decalVtx::CanMerge(pFromVtx, pToVtx))
										{
											// Found a merge.  Replace all other references to pFromVtx with pToVtx.
											pFromTri->SetVertex(i, pVtxBase, pToVtx);
											for (decalTri *pFromTri2=pFromTri->GetNext(triConv); pFromTri2; pFromTri2=pFromTri2->GetNext(triConv))
											{
												for (unsigned j=0; j<3; ++j)
												{
													if (pFromTri2->GetVertex(j, pVtxBase) == pFromVtx)
													{
														pFromTri2->SetVertex(j, pVtxBase, pToVtx);
													}
												}
											}

											// Free up the merged vertex.
											decalAssert(numNewVtxs);
											--numNewVtxs;
											dm->m_pools.ReturnVtx(pFromVtx);
										}
									}
								}
							}
						}
					}
#endif // 0

					// copy the tris over

					// if there is space in the mini index buffer in the destination, move some tris from the new instance
					decalMiniIdxBuf *pMibToMergeInto = decalMiniIdxBuf::GetPtr(pMibBase, pInstToMergeInto->m_idxBufList);
					decalMiniIdxBuf *pMibNew         = decalMiniIdxBuf::GetPtr(pMibBase, pInstNew->m_idxBufList);
					while (!pMibToMergeInto->IsFull())
					{
						const unsigned numTrisDst = pMibToMergeInto->GetNumTris();
						const unsigned numTrisSrc = pMibNew->GetNumTris();

						// enough space to take all the triangles from the src mini index buffer ?
						if (numTrisDst+numTrisSrc <= decalMiniIdxBuf::MAX_NUM_TRIS)
						{
							sysMemCpy(pMibToMergeInto->m_idx+numTrisDst*3, pMibNew->m_idx, numTrisSrc*6);
							pMibToMergeInto->SetNumTris(numTrisDst+numTrisSrc);
							decalMiniIdxBuf *const pMibNewNext = pMibNew->GetNext(pMibBase);
							dm->m_pools.ReturnMib(pMibNew);
							pMibNew = pMibNewNext;
							if (!pMibNew)
							{
								break;
							}
							// keep looping, may be able to move some more triangles from the next mini index buffer
						}

						// fill the destination without emptying the source ?
						else
						{
							const unsigned spaceTrisDst = decalMiniIdxBuf::MAX_NUM_TRIS - numTrisDst;
							memcpy(pMibToMergeInto->m_idx+numTrisDst*3, pMibNew->m_idx+(numTrisSrc-spaceTrisDst)*3, spaceTrisDst*6);
							pMibToMergeInto->SetNumTris(decalMiniIdxBuf::MAX_NUM_TRIS);
							pMibNew->SetNumTris(numTrisSrc - spaceTrisDst);
							break;
						}
					}

					// if there are still remaining mini index buffers, then link them across
					if (pMibNew)
					{
						decalMiniIdxBuf *pMibNewLast = pMibNew;
						decalMiniIdxBuf *pMibNewNext = pMibNew->GetNext(pMibBase);
						while (pMibNewNext)
						{
							pMibNewLast = pMibNewNext;
							pMibNewNext = pMibNewNext->GetNext(pMibBase);
						}
						pMibNewLast->SetNext(pMibBase, pMibToMergeInto);
						pInstToMergeInto->m_idxBufList = (u16)(pMibNew - pMibBase);
					}
					pInstToMergeInto->m_numTris += pInstNew->m_numTris;
					pInstNew->m_numTris = 0;
					pInstNew->m_idxBufList = decalMiniIdxBuf::NULL_IDX;

					// The Cruise Control worker machine RSGEDICC11, dies with a compiler ICE on this code.
					// Not positive, but looks like it may be running out of memory.
#					if RSG_DURANGO
#						define DISABLE_ON_BAD_BUILD_SERVERS(...)
#					else
#						define DISABLE_ON_BAD_BUILD_SERVERS(...) __VA_ARGS__
#					endif

					const u16 pVtxNewFirst = pInstNew->m_vtxList;
					decalAssert(pVtxNewFirst != 0xffff);
					u16 pVtxNewLast;
					u16 pVtxNewNext = pVtxNewFirst;
					DISABLE_ON_BAD_BUILD_SERVERS(ASSERT_ONLY(unsigned numNewVtxsCheck = 0;))
					u16 *const allocatedVertices = dm->m_allocatedVertices;
					do
					{
						pVtxNewLast = pVtxNewNext;
						pVtxNewNext = allocatedVertices[pVtxNewLast];
						DISABLE_ON_BAD_BUILD_SERVERS(ASSERT_ONLY(++numNewVtxsCheck;))
					}
					while (pVtxNewNext != 0xffff);
					allocatedVertices[pVtxNewLast] = pInstToMergeInto->m_vtxList;
					pInstToMergeInto->m_vtxList = pVtxNewFirst;
					DISABLE_ON_BAD_BUILD_SERVERS(decalAssert(numNewVtxsCheck == numNewVtxs);)
					pInstToMergeInto->m_numVtxs += (u16)numNewVtxs;
					pInstNew->m_vtxList = 0xffff;
					pInstNew->m_numVtxs = 0;

#					undef DISABLE_ON_BAD_BUILD_SERVERS

					// calc the new bounding sphere
					const Vec4V sphere0 = pInstNew->GetBoundCentreAndRadius();
					const Vec4V sphere1 = pInstToMergeInto->GetBoundCentreAndRadius();
					Vec4V sphereMerged = spdSphere::MergeSpheres(sphere0, sphere1);
					pInstToMergeInto->SetBoundCentreAndRadius(sphereMerged);

					//Notify higher level code that a decal has been merged.
					g_pDecalCallbacks->DecalMerged(pInstNew->m_id, pInstToMergeInto->m_id);

					// return this inst to the pool
					ASSERT_ONLY(pInstNew->MarkForDelete();)
					pInstNew->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_RELEASE_DECAL_ID);
					dm->m_pools.ReturnInst(pInstNew);
				}

				pInstNew = pInstNewNext;
			}

			pList->SetEmpty();
		}

		ExpandBoundSphere(boundSphere);
	}

	void decalBucket::RemoveInstSafe(decalInst* pInst)
	{
		pInst->MarkForDelete();
		RecalcBoundSphere();
	}

	void decalBucket::RecalcBoundSphere()
	{
		spdSphere sphere = spdSphere(Vec4V(V_ZERO));

		// initialise the bound sphere to that of the first inst
		const decalInst* pCurrInst = m_instList.Peek();
		if (pCurrInst)
		{
			sphere = spdSphere(pCurrInst->GetBoundCentreAndRadius());

			// go through the remaining insts growing the bound sphere if necessary
			pCurrInst = pCurrInst->GetNext();
			while (pCurrInst)
			{
				if (pCurrInst->IsMarkedForDelete()==false)
				{
					sphere.GrowSphere(spdSphere(pCurrInst->GetBoundCentreAndRadius()));
				}

				pCurrInst = pCurrInst->GetNext();
			}
		}

		// set the final bound sphere
		m_boundSphere = sphere.GetV4();
	}

	ScalarV_Out decalBucket::FindClosestDistSqr(Vec3V_In vTestPos) const
	{
		ScalarV vClosestDistSqr = ScalarVFromF32(FLT_MAX);

		const decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			if (pCurrInst->IsMarkedForDelete()==false)
			{
				Vec3V vPosInst = Transform(m_wldMtx, pCurrInst->GetBoundCentre());
				ScalarV vDistSqr = MagSquared(vPosInst-vTestPos);
				vClosestDistSqr = SelectFT(IsLessThan(vDistSqr, vClosestDistSqr), vClosestDistSqr, vDistSqr);
			}

			pCurrInst = pCurrInst->GetNext();
		}

		return vClosestDistSqr;
	}

	bool decalBucket::PreProcessOverlays(const fwEntity* entity, decalUserSettings& settings, float& newRadius)
	{
		if (m_attachEntity==entity)
		{
			if (m_typeSettingsIndex==settings.bucketSettings.typeSettingsIndex && m_subType==settings.bucketSettings.subType)
			{
				// don't allow glass decals to obstruct adding new non-glass decals close to glass (and vice versa)
				if ((IsGlass() && settings.pipelineSettings.dontApplyToGlass) ||
					(!IsGlass() && settings.pipelineSettings.onlyApplyToGlass))
				{
					return true;
				}

				// go through the active insts in this bucket
				decalInst* pCurrInst = m_instList.Peek();
				while (pCurrInst)
				{
					if (pCurrInst->IsMarkedForDelete()==false)
					{
						Vec3V vInstPos = Transform(m_wldMtx, pCurrInst->GetBoundCentre());
						const ScalarV instRadius = pCurrInst->GetBoundRadius();

						decalOverlayResult overlayResult = decalHelper::PreProcessOverlays(vInstPos, instRadius.Getf(), settings.instSettings.vPosition, newRadius, settings.pipelineSettings.maxOverlayRadius);
						if (overlayResult==DECAL_OVERLAY_ENCLOSED)
						{
							// the new texture has been rejected
							return false;
						}
						else if (overlayResult==DECAL_OVERLAY_OVERLAPPING)
						{
							// remove this inst from this bucket
							RemoveInstSafe(pCurrInst);
						}
					}
					pCurrInst = pCurrInst->GetNext();
				}
			}
		}

		return true;
	}

	bool decalBucket::PreProcessLiquidPools(decalUserSettings& settings)
	{
		bool foundLiquidPool = false;
		if (settings.instSettings.isLiquidPool && (m_typeSettingsIndex==settings.bucketSettings.typeSettingsIndex && m_subType==settings.bucketSettings.subType))
		{
			// go through the active insts in this bucket
			decalInst* pCurrInst = m_instList.Peek();
			while (pCurrInst)
			{
				// check if the new decal is in range (radius of inst * current uv growth * 1/root2)
				// 1/root2 converts the bound radius from being outside of the square to inside it - i.e. where the pixels are on a round texture
				if ((pCurrInst->IsMarkedForDelete()==false) && pCurrInst->IsInRange(settings.instSettings.vPosition, pCurrInst->GetBoundRadius()*ScalarV(pCurrInst->CalcUVMult()*0.707f), m_wldMtx))
				{
					if (settings.instSettings.uvMultEnd==0.0f)
					{
						// flagged to grown instantly to the max size
						pCurrInst->m_uvMultStart.SetFloat16_FromFloat32(1.0f);
					}
					else
					{
						// grow the existing inst
						const float uvMultStart = pCurrInst->m_uvMultStart.GetFloat32_FromFloat16();
						if (uvMultStart<1.0f)
						{
							const float uvGrowRate = pCurrInst->m_uvMultEnd.GetFloat32_FromFloat16();
							pCurrInst->m_uvMultStart.SetFloat16_FromFloat32(Min(1.0f, uvMultStart+(uvGrowRate*Cosf(uvMultStart*0.5f*PI))));
						}
					}

					//Notify higher level code that a the UV mult changed for replay
					g_pDecalCallbacks->LiquidUVMultChanged(pCurrInst->m_id, pCurrInst->m_uvMultStart.GetFloat32_FromFloat16(), settings.instSettings.vPosition);

					foundLiquidPool = true;
				}

				pCurrInst = pCurrInst->GetNext();
			}
		}

		return foundLiquidPool;
	}

	float decalBucket::GetWashLevel(int decalId) const
	{
		const decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			if ((pCurrInst->IsMarkedForDelete()==false) && pCurrInst->m_id==decalId)
			{
				return pCurrInst->GetWashLevel();
			}

			pCurrInst = pCurrInst->GetNext();
		}

		return -1.0f;
	}

	void decalBucket::Remove(int decalId)
	{
		bool anyRemoved = false;
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			if (pCurrInst->IsMarkedForDelete()==false && pCurrInst->m_id==decalId)
			{
				pCurrInst->MarkForDelete();
				anyRemoved = true;
			}

			pCurrInst = pCurrInst->GetNext();
		}

		if (anyRemoved)
		{
			RecalcBoundSphere();
		}
	}
		
	void decalBucket::Wash(float amount)
	{
		if (IsWashable())
		{
			// wash the insts in this bucket
			decalInst* pCurrInst = m_instList.Peek();
			while (pCurrInst)
			{
				pCurrInst->Wash(amount);

				pCurrInst = pCurrInst->GetNext();
			}
		}
	}

	void decalBucket::WashInRange(float amount, Vec3V_In vPos, ScalarV_In range)
	{
		// get the square of the range as a vector scalar
		const ScalarV rangeSqr = range*range;

		if (IsWashable())
		{
			// wash the insts in this bucket
			decalInst* pCurrInst = m_instList.Peek();
			while (pCurrInst)
			{
				if ((pCurrInst->IsMarkedForDelete()==false) && pCurrInst->IsInRange(vPos, rangeSqr, m_wldMtx))
				{
					pCurrInst->Wash(amount);
				}

				pCurrInst = pCurrInst->GetNext();
			}
		}
	}

	void decalBucket::RemoveFacing(Vec3V_In vDir)
	{
		// remove any insts in this bucket that are facing the direction passed
		bool anyRemoved = false;
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			if (pCurrInst->IsMarkedForDelete()==false)
			{
				Vec3V vInstDirWld = Transform3x3(m_wldMtx, pCurrInst->GetDirection());
				if (IsGreaterThanAll(Dot(vInstDirWld, vDir), ScalarV(V_ZERO)))
				{
					pCurrInst->MarkForDelete();
					anyRemoved = true;
				}
			}

			pCurrInst = pCurrInst->GetNext();
		}

		if (anyRemoved)
		{
			RecalcBoundSphere();
		}
	}

	int decalBucket::RemoveInRange(Vec3V_In vPos, ScalarV_In range)
	{
		// remove any in range insts in this bucket
		int iTotalRemoved = 0;
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			ScalarV r = range + pCurrInst->GetBoundRadius();
			if ((pCurrInst->IsMarkedForDelete()==false) && pCurrInst->IsInRange(vPos, r * r, m_wldMtx))
			{
				pCurrInst->MarkForDelete();
				iTotalRemoved++;
			}

			pCurrInst = pCurrInst->GetNext();
		}

		if (iTotalRemoved > 0)
		{
			RecalcBoundSphere();
		}

		return iTotalRemoved;
	}

	void decalBucket::FadeInRange(Vec3V_In vPos, ScalarV_In range, float fadeTime)
	{
		// get the square of the range as a vector scalar
		const ScalarV rangeSqr = range*range;

		// remove any in range insts in this bucket
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			if ((pCurrInst->IsMarkedForDelete()==false) && pCurrInst->IsInRange(vPos, rangeSqr, m_wldMtx))
			{
				pCurrInst->StartFadingOut(fadeTime);
			}

			pCurrInst = pCurrInst->GetNext();
		}
	}

	static bool PointInTriangle(Vec3V_In vPointToTest, Vec3V_In vVertex0, Vec3V_In vVertex1, Vec3V_In vVertex2, Vec3V_In vTriangleNormal, ScalarV vDistThresh)
	{
		// calc the edge vectors of the triangle we're testing against
		const Vec3V vEdge01 = Subtract(vVertex1, vVertex0);
		const Vec3V vEdge12 = Subtract(vVertex2, vVertex1);
		const Vec3V vEdge20 = Subtract(vVertex0, vVertex2);

		// calc the vectors from the point we're testing to the triangle verts we're testing against
		const Vec3V vPointToV0 = Subtract(vVertex0, vPointToTest);
		const Vec3V vPointToV1 = Subtract(vVertex1, vPointToTest);
		const Vec3V vPointToV2 = Subtract(vVertex2, vPointToTest);

		// calc the outward normals of the faces that are generated from the test point to the triangle verts
		const Vec3V vFaceNormal0 = Cross(vPointToV0, vEdge01);
		const Vec3V vFaceNormal1 = Cross(vPointToV1, vEdge12);
		const Vec3V vFaceNormal2 = Cross(vPointToV2, vEdge20);

		// calc the dot products of the face normals with the triangle normal
		const ScalarV vsDot0 = Dot(vFaceNormal0, vTriangleNormal);
		const ScalarV vsDot1 = Dot(vFaceNormal1, vTriangleNormal);
		const ScalarV vsDot2 = Dot(vFaceNormal2, vTriangleNormal);

		// test if all of the dot products are zero or greater
		// i.e. all face normals point in the same direction as the triangle normal
		// this means that the test point is within the infinite volume of the triangle in the direction of its normal
		const Vec3V vZero(V_ZERO);
		const Vec3V vCombinedDots(vsDot0, vsDot1, vsDot2);
		if (IsGreaterThanOrEqualAll(vCombinedDots, vZero) != 0)
		{
			// calc the distance of the test point to the plane defined by the triangle
			const ScalarV vDistToPlane = Dot(vPointToTest-vVertex0, vTriangleNormal);

			// if the test point is close enough to the triangle plane then we're inside
			return (IsLessThanAll(vDistToPlane, vDistThresh) != 0);
		}

		return false;
	}

	bool decalBucket::IsPointInLiquid(s8& liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, bool isDynamic, float distThresh)
	{
		const ScalarV minDecalSizeV(minDecalSize);
		const ScalarV posScale = GetPosScale();

		const decalManager* const dm = &DECALMGR;
		const Vec3V msOffsetFlt = GetModelSpaceOffsetInt() * dm->GetModelSpaceOffsetUnits();

		// go through the decals insts
		const decalMiniIdxBuf* const pMibBase = dm->m_pools.GetMibPoolBase();
		const decalVtx*        const pVtxBase = dm->m_pools.GetVtxPoolBase();
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			// check if this is the correct liquid type
			if (pCurrInst->m_liquidType!=-1)
			{
				// check if the test point is within the inst bound radius
				const ScalarV decalRadius = pCurrInst->GetBoundRadius()*ScalarV(pCurrInst->CalcUVMult());
				if ((pCurrInst->IsMarkedForDelete()==false) && IsGreaterThanOrEqualAll(decalRadius, minDecalSizeV) && !pCurrInst->IsFadingOut() && pCurrInst->IsInRange(vPos, decalRadius, m_wldMtx))
				{
					// it is - check if it's dynamic
					if (isDynamic)
					{
						// dynamic decals have no triangles to check - return true
						liquidType = pCurrInst->m_liquidType;
						return true;
					}
					else
					{
						// static decals have triangles - check if we're within one
						const decalMiniIdxBuf *pMib = decalMiniIdxBuf::GetPtr(pMibBase, pCurrInst->m_idxBufList);
						while (pMib)
						{
							const unsigned numIndices = pMib->GetNumTris()*3;
							for (unsigned ii=0; ii<numIndices; ii+=3)
							{
								// check if the point is within the triangle
								Vec3V vVerts[3];
								vVerts[0] = Transform(m_wldMtx, pVtxBase[pMib->m_idx[ii+0]].GetPos(posScale, msOffsetFlt));
								vVerts[1] = Transform(m_wldMtx, pVtxBase[pMib->m_idx[ii+1]].GetPos(posScale, msOffsetFlt));
								vVerts[2] = Transform(m_wldMtx, pVtxBase[pMib->m_idx[ii+2]].GetPos(posScale, msOffsetFlt));
								Vec3V vNormal = Cross(vVerts[1]-vVerts[0], vVerts[2]-vVerts[0]);
								vNormal = Normalize(vNormal);

								if (PointInTriangle(vPos, vVerts[0], vVerts[1], vVerts[2], vNormal, ScalarV(distThresh)))
								{
									// it is
									if (removeDecal)
									{
										pCurrInst->MarkForDelete();
										RecalcBoundSphere();
									}
									else if (fadeDecal)
									{
										pCurrInst->StartFadingOut(fadeTime);
									}

									liquidType = pCurrInst->m_liquidType;
									return true;
								}
							}

							pMib = pMib->GetNext(pMibBase);
						}
					}
				}
			}

			pCurrInst = pCurrInst->GetNext();
		}

		return false;
	}

	bool decalBucket::IsPointWithin(s8 liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, bool isDynamic, float distThresh)
	{
		const ScalarV minDecalSizeV(minDecalSize);
		const ScalarV posScale = GetPosScale();

		const decalManager* const dm = &DECALMGR;
		const Vec3V msOffsetFlt = GetModelSpaceOffsetInt() * dm->GetModelSpaceOffsetUnits();

		// go through the decals insts
		const decalMiniIdxBuf* const pMibBase = dm->m_pools.GetMibPoolBase();
		const decalVtx*        const pVtxBase = dm->m_pools.GetVtxPoolBase();
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			// check if this is the correct liquid type
			if (liquidType==-1 || liquidType==pCurrInst->m_liquidType)
			{
				// check if the test point is within the inst bound radius
				const ScalarV decalRadius = pCurrInst->GetBoundRadius()*ScalarV(pCurrInst->CalcUVMult());
				if ((pCurrInst->IsMarkedForDelete()==false) && IsGreaterThanOrEqualAll(decalRadius, minDecalSizeV) && !pCurrInst->IsFadingOut() && pCurrInst->IsInRange(vPos, decalRadius, m_wldMtx))
				{
					// it is - check if it's dynamic
					if (isDynamic)
					{
						// dynamic decals have no triangles to check - return true
						return true;
					}
					else
					{
						// static decals have triangles - check if we're within one
						const decalMiniIdxBuf *pMib = decalMiniIdxBuf::GetPtr(pMibBase, pCurrInst->m_idxBufList);
						while (pMib)
						{
							const unsigned numIndices = pMib->GetNumTris()*3;
							for (unsigned ii=0; ii<numIndices; ii+=3)
							{
								// check if the point is within the triangle
								Vec3V vVerts[3];
								vVerts[0] = Transform(m_wldMtx, pVtxBase[pMib->m_idx[ii+0]].GetPos(posScale, msOffsetFlt));
								vVerts[1] = Transform(m_wldMtx, pVtxBase[pMib->m_idx[ii+1]].GetPos(posScale, msOffsetFlt));
								vVerts[2] = Transform(m_wldMtx, pVtxBase[pMib->m_idx[ii+2]].GetPos(posScale, msOffsetFlt));
								Vec3V vNormal = Cross(vVerts[1]-vVerts[0], vVerts[2]-vVerts[0]);
								vNormal = Normalize(vNormal);

								if (PointInTriangle(vPos, vVerts[0], vVerts[1], vVerts[2], vNormal, ScalarV(distThresh)))
								{
									// it is
									if (removeDecal)
									{
										pCurrInst->MarkForDelete();
										RecalcBoundSphere();
									}
									else if (fadeDecal)
									{
										pCurrInst->StartFadingOut(fadeTime);
									}

									return true;
								}
							}

							pMib = pMib->GetNext(pMibBase);
						}
					}
				}
			}

			pCurrInst = pCurrInst->GetNext();
		}

		return false;
	}

#if DECAL_PROCESS_BREAKABLES
	void decalBucket::ReApplyBreakable(const fwEntity* pAttachEntity, s16 oldMatrixId, s16 newMatrixId, decalRenderPass renderPass)
	{
		if (m_attachEntity==pAttachEntity && m_attachMatrixId==oldMatrixId)
		{
			if (m_attachType==DECAL_ATTACH_TYPE_BREAKABLE_UNBROKEN || m_attachType==DECAL_ATTACH_TYPE_BREAKABLE_BROKEN)
			{
				bool anyRemoved = false;
				decalInst* pDecalInst = m_instList.Peek();
				while (pDecalInst)
				{
					if (pDecalInst->IsMarkedForDelete() == false)
					{
						if (ReApplyAsStaticDecal(pDecalInst, NULL, renderPass, newMatrixId, true, false, false))
						{
							// Because there is a one frame delay in creating new
							// static decals, add a one frame delay before deleting
							// the existing ones.
							const unsigned delayedDeleteNumFrames = 1;
							pDecalInst->MarkForDelete(delayedDeleteNumFrames);
							anyRemoved = true;
						}
					}
					pDecalInst = pDecalInst->GetNext();
				}
				if (anyRemoved)
				{
					RecalcBoundSphere();
				}
			}
		}
	}
#endif

	bool decalBucket::ReApplyVehicleGlass(const fwEntity* pAttachEntity, void* pSmashGroup, decalRenderPass renderPass)
	{
		bool anyRemoved = false;
		if (m_attachEntity==pAttachEntity && m_pSmashGroup==pSmashGroup)
		{
			decalInst* pDecalInst = m_instList.Peek();
			while (pDecalInst)
			{
				if (pDecalInst->IsMarkedForDelete() == false)
				{
					if (ReApplyAsStaticDecal(pDecalInst, m_pSmashGroup, renderPass, m_attachMatrixId, false, true, false))
					{
						const unsigned delayedDeleteNumFrames = 1;
						pDecalInst->MarkForDelete(delayedDeleteNumFrames);
						anyRemoved = true;
					}
				}
				pDecalInst = pDecalInst->GetNext();
			}
			if (anyRemoved)
			{
				RecalcBoundSphere();
			}
		}

		return anyRemoved;
	}

	bool decalBucket::ReApplyAsStaticDecal(decalInst* pInst, void* pSmashGroup, decalRenderPass renderPass, s16 newMatrixId, bool onlyApplyToGlass, bool dontApplyToGlass, bool DECAL_BREAKABLE_ONLY(applyToBreakables))
	{
		const decalRenderSettings* const pRenderSettings = decalSettingsManager::GetRenderSettings(m_renderSettingsIndex);

		// generate settings for a new static decal
		decalUserSettings settings;

		settings.bucketSettings.pSmashGroup = pSmashGroup;
		settings.bucketSettings.typeSettingsIndex = m_typeSettingsIndex;
		settings.bucketSettings.renderSettingsIndex = m_renderSettingsIndex;
		settings.bucketSettings.subType = m_subType;

		settings.instSettings.vPosition = Transform(m_wldMtx, pInst->GetPosition());
		settings.instSettings.vDirection = Transform3x3(m_wldMtx, pInst->GetDirection());
		settings.instSettings.vSide = Transform3x3(m_wldMtx, pInst->GetSide());
		settings.instSettings.vDimensions = pInst->GetDimensions() / Vec3V(pRenderSettings->sizeMult,pRenderSettings->sizeMult,1.f);
		settings.instSettings.currWrapLength = pInst->m_currWrapLength;
		settings.instSettings.totalLife = pInst->m_totalLife;
		settings.instSettings.fadeInTime = pInst->GetFadeInTime();
		settings.instSettings.fadeOutTime = pInst->GetFadeOutTime();
		settings.instSettings.uvMultStart = pInst->m_uvMultStart.GetFloat32_FromFloat16();
		settings.instSettings.uvMultEnd = pInst->m_uvMultEnd.GetFloat32_FromFloat16();
		settings.instSettings.uvMultTime = pInst->m_uvMultTime.GetFloat32_FromFloat16();
		settings.instSettings.colR = pInst->m_colR;
		settings.instSettings.colG = pInst->m_colG;
		settings.instSettings.colB = pInst->m_colB;
		settings.instSettings.alphaFront = pInst->m_alphaFront;
		settings.instSettings.alphaBack = pInst->m_alphaBack;
		settings.instSettings.liquidType = pInst->m_liquidType;
		settings.instSettings.isLiquidPool = pInst->m_isLiquidPool;
		settings.instSettings.flipU = pInst->m_flipU;
		settings.instSettings.flipV = pInst->m_flipV;
		settings.instSettings.singleFrame = pInst->m_singleFrame;

		settings.pipelineSettings.vPassedJoinVerts[0] = Vec3V(V_ZERO);
		settings.pipelineSettings.vReturnedJoinVerts[0] = Vec3V(V_ZERO);
		settings.pipelineSettings.renderPass = renderPass;
		settings.pipelineSettings.method = DECAL_METHOD_STATIC;
		settings.pipelineSettings.duplicateRejectDist = 0.0f;
		settings.pipelineSettings.maxOverlayRadius = 0.0f;
		settings.pipelineSettings.exclusiveMtlId = (phMaterialMgr::Id)(-1);
		settings.pipelineSettings.colnComponentId = newMatrixId;			// not really valid - it could be a bone index now
		settings.pipelineSettings.hasJoinVerts = false;	
		settings.pipelineSettings.useColnEntityOnly = true;
		settings.pipelineSettings.onlyApplyToGlass = onlyApplyToGlass;
		settings.pipelineSettings.dontApplyToGlass = dontApplyToGlass;	
#if DECAL_PROCESS_BREAKABLES
		settings.pipelineSettings.applyToBreakables = applyToBreakables;
#endif

		// When forcing decal regeneration, allow it to reattach to any
		// bone.  This is required for damaged frags, since the new drawable can
		// be skinned to a different bone.
		settings.lodSettings.currLife = pInst->m_currLife;
		settings.lodSettings.lodAttachMatrixId = m_forceRegen ? -1 : newMatrixId;
		settings.lodSettings.copyPrimaryInstFlag = true;
		settings.lodSettings.primaryInstFlag = pInst->GetIsPrimaryInst();

		return !!DECALMGR.AddSettings(m_attachEntity, settings, pInst->m_id, false);
	}

	void decalBucket::MarkInstsForDelete()
	{
		decalInst* pCurrInst = m_instList.Peek();
		while (pCurrInst)
		{
			pCurrInst->MarkForDelete();
			pCurrInst = pCurrInst->GetNext();
		}
	}

	void decalBucket::ProcessMarkedForDelete(decalMethod method)
	{
		// go through the active insts in this bucket
		decalManager *const dm = &DECALMGR;
		decalInst** ppCurrInst = m_instList.GetPtrPtrHead();
		decalInst* pCurrInst = *ppCurrInst;
		int adjust = 0;
		while (pCurrInst)
		{
			decalInst* pNextInst = pCurrInst->GetNext();
			if (pCurrInst->ProcessMarkedForDelete())
			{
#				if __ASSERT
					// decalInst::ProcessMarkedForDelete() will decrement the
					// deletion counter to zero, which will then annoyingly
					// trigger an assert when shutting down the instance.  So
					// put the counter back to 1.  Serves no other purpose other
					// than supressing the assert.
					pCurrInst->MarkForDelete();
#				endif
				if (method == DECAL_METHOD_STATIC)
				{
					dm->DelayedDeletion(pCurrInst);
				}
				else
				{
					pCurrInst->ShutdownImmediate(decalInst::SHUTDOWN_IMMEDIATE_RELEASE_DECAL_ID);
					dm->m_pools.ReturnInst(pCurrInst);
				}
				*ppCurrInst = pNextInst;
				--adjust;
			}
			else
			{
				ppCurrInst = pCurrInst->GetPtrPtrNext();
			}
			pCurrInst = pNextInst;
		}

		if (adjust)
		{
			m_instList.AddSize(adjust);
			RecalcBoundSphere();
		}
	}

#if __BANK
	bool decalBucket::CheckForceWashable() const
	{
		return (DECALMGRASYNC.m_debug.m_forceWashable);
	}
#endif

#endif // !__SPU

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
