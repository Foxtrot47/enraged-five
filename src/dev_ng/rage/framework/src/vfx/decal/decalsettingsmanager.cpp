//
// vfx/decal/decalsettingsmanager.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes
#include "decalsettingsmanager.h"

// includes (rage)
#include "file/asset.h"
#include "grcore/effect_config.h"
#include "grcore/texture.h"
#include "math/random.h"
#include "system/nelem.h"
#include "vfx/decal/decalmanager.h"

// includes (framework)
#include "fwdrawlist/drawlistmgr.h"
#include "fwscene/stores/txdstore.h"
#include "streaming/streamingengine.h"
#include "streaming/streaminginfo.h"
#include "vfx/optimisations.h"
#include "vfx/vfxutil.h"
#include "vfx/decal/decalsettings.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


// static variables
u32 decalSettingsManager::ms_numTypeSettings;
decalTypeSettings* decalSettingsManager::ms_pTypeSettings;

u32 decalSettingsManager::ms_numRenderSettings;
decalRenderSettings decalSettingsManager::ms_renderSettings[DECAL_MAX_RENDER_SETTINGS];

u32 decalSettingsManager::ms_numPatchRequests;
decalSettingsManager::PatchRequest decalSettingsManager::ms_patchRequests[DECAL_MAX_RENDER_SETTINGS];


// code
void decalSettingsManager::Init(u32 numTypeSettings, decalTypeSettings* pTypeSettings, u32 numRenderSettingsFiles, decalRenderSettingsInfo* pRenderSettingsInfos)
{
	// init the type settings
	ms_numTypeSettings = numTypeSettings;
	ms_pTypeSettings = pTypeSettings;

	// init the render settings
	ms_numRenderSettings = 0;
	for (u32 i=0; i<numRenderSettingsFiles; i++)
	{
		LoadRenderSettings(pRenderSettingsInfos->pFilename);
	}

	// clear the patch requests
	ms_numPatchRequests = 0;
}

void decalSettingsManager::Shutdown()
{
	Update();
	for (unsigned i=0; i<ms_numRenderSettings; ++i)
	{
		ms_patchRequests[i].renderSettingsIndex = i;
		ms_patchRequests[i].pDiffuseMap = NULL;
	}
	ms_numPatchRequests = ms_numRenderSettings;
	Update();
}

void decalSettingsManager::Update()
{
	const unsigned numPatchRequests = ms_numPatchRequests;
	ms_numPatchRequests = 0;
	for (unsigned reqIdx=0; reqIdx<numPatchRequests; ++reqIdx)
	{
		const unsigned renderSettingsIndex = ms_patchRequests[reqIdx].renderSettingsIndex;
		grcTexture *const pDiffuseMap = ms_patchRequests[reqIdx].pDiffuseMap;

		// Add ref to new patch first, just in case it is the same as the
		// previous.
		if (pDiffuseMap)
		{
			strStreamingEngine::AddRefByPtr(pDiffuseMap, REF_OTHER);
		}

		// Add a temporary 3-frame ref to the previous patch via the draw list
		// manager, then remove the permanent ref we had added.
		grcTexture *const prev = ms_renderSettings[renderSettingsIndex].pDiffuseMap;
		if (prev && prev != grcTexture::None)
		{
			// Because we need to add the txd to the draw list manager, we need
			// to know the streaming module object index.  So we can't simply
			// call
			//      strStreamingEngine::RemoveRefByPtr(prev, REF_OTHER);
			//
			// though that is basically what the code below is doing.
			//
			sysMemAllocator *const alloc = strStreamingEngine::GetAllocator().GetPointerOwner(prev);
			if (alloc)
			{
				// Get the start of the memory allocation
				void *const block = alloc->GetCanonicalBlockPtr(prev);
				decalAssert(block); // if ptr is in one of the streaming allocators, we expect it to return a memory block

				// The strIndex stored in the allocation user data will be a valid
				// streaming index, iff the allocation was made by a streaming
				// module
				const strIndex strIdx = strStreamingInfoManager::GetStreamingIndexFromMemBlock(block);
				decalAssert(strIdx.IsValid());
				decalAssert(strStreamingEngine::GetInfo().GetModule(strIdx) == &g_TxdStore);
				const strLocalIndex objIdx = strLocalIndex(g_TxdStore.GetObjectIndex(strIdx));
				gDrawListMgr->AddTxdReference(objIdx.Get());
				g_TxdStore.RemoveRef(objIdx, REF_OTHER);
			}
		}
		ms_renderSettings[renderSettingsIndex].pDiffuseMap = pDiffuseMap;
	}
}

void decalSettingsManager::LoadRenderSettings(const char* pFilename)
{
	fiStream* stream = ASSET.Open(pFilename, "dat", true);
	decalAssertf(stream, "cannot open data file");

	if (stream)
	{	
		// set up a reference to the texture dictionary
		strLocalIndex txdSlot = strLocalIndex(vfxUtil::InitTxd("fxdecal"));

		// initialise the tokens
		fiAsciiTokenizer token;
		token.Init("decal", stream);

		//
		int phase = -1;	
		char charBuff[128];

		// read in the version
		float version = token.GetFloat();
		(void)version;

		while (1)
		{
			token.GetToken(charBuff, 128);

			// check for commented lines
			if (charBuff[0]=='#')
			{
				token.SkipToEndOfLine();
				continue;
			}

			// check for change of phase
			if (stricmp(charBuff, "DECAL_DEF_START")==0)
			{
				phase = 0;
				continue;
			}
			if (stricmp(charBuff, "DECAL_DEF_END")==0)
			{
				break;
			}

			// phase 0
			if (phase==0)
			{
				if (decalVerifyf(ms_numRenderSettings<DECAL_MAX_RENDER_SETTINGS, "no room to add new decal render setting"))
				{
					// id
					ms_renderSettings[ms_numRenderSettings].id = atoi(&charBuff[0]);

					// diffuse map
					token.GetToken(charBuff, 128);		
					ms_renderSettings[ms_numRenderSettings].pDiffuseMap = const_cast<grcTexture*>(grcTexture::None);
					if (stricmp(charBuff, "-"))
					{
						grcTexture *const tex = g_TxdStore.Get(txdSlot)->Lookup(charBuff);;
						ms_renderSettings[ms_numRenderSettings].pDiffuseMap = tex;
						strStreamingEngine::AddRefByPtr(tex, REF_OTHER);
						decalAssertf(tex, "cannot find texture - %s", charBuff);
					}

					// normal map
					token.GetToken(charBuff, 128);	
					ms_renderSettings[ms_numRenderSettings].pNormalMap = g_TxdStore.Get(txdSlot)->Lookup(charBuff);
					decalAssertf(ms_renderSettings[ms_numRenderSettings].pNormalMap, "cannot find texture - %s", charBuff);

					// specular map
					token.GetToken(charBuff, 128);	
					ms_renderSettings[ms_numRenderSettings].pSpecularMap = const_cast<grcTexture*>(grcTexture::None);
					if (stricmp(charBuff, "-"))
					{	
						ms_renderSettings[ms_numRenderSettings].pSpecularMap = g_TxdStore.Get(txdSlot)->Lookup(charBuff);
						decalAssertf(ms_renderSettings[ms_numRenderSettings].pSpecularMap, "cannot find texture - %s", charBuff);
					}

					// texture page info
					int texRows = token.GetInt();
					int texCols = token.GetInt();
					int texIdA = token.GetInt();
					int texIdB = texIdA;

					// keep compatibility with previous version, where only one texId was defined
					if (version>=2.0f)
					{
						texIdB = token.GetInt();
					}

					if (version>=4.0f)
					{
						ms_renderSettings[ms_numRenderSettings].animTime = token.GetFloat();
					}
					else
					{
						ms_renderSettings[ms_numRenderSettings].animTime = 0.0f;
					}
		
					ms_renderSettings[ms_numRenderSettings].texRows = (u8)texRows;
					ms_renderSettings[ms_numRenderSettings].texColumns = (u8)texCols;
					ms_renderSettings[ms_numRenderSettings].texIdA = (u8)texIdA;
					ms_renderSettings[ms_numRenderSettings].texIdB = (u8)texIdB;

					// size mult
					if (version>=6.0f)
					{
						ms_renderSettings[ms_numRenderSettings].sizeMult = token.GetFloat();
					}
					else
					{

						ms_renderSettings[ms_numRenderSettings].sizeMult = 1.0f;
					}

					// specular settings
					ms_renderSettings[ms_numRenderSettings].specFalloff = token.GetFloat();
					ms_renderSettings[ms_numRenderSettings].specIntensity = token.GetFloat();
					if (version>=5.0)
					{
						ms_renderSettings[ms_numRenderSettings].specFresnel = token.GetFloat();
					}
					else
					{
						ms_renderSettings[ms_numRenderSettings].specFresnel = 0.0f;
					}

					// parallax settings
					int parallaxUseSteep = token.GetInt();
					ms_renderSettings[ms_numRenderSettings].shaderId = parallaxUseSteep ? (u8)DECAL_STATIC_SHADER_PARALLAX_STEEP : (u8)DECAL_STATIC_SHADER_PARALLAX;
					ms_renderSettings[ms_numRenderSettings].parallaxScale = token.GetFloat();

					// emissive settings
					if (version>=3.0f)
					{
						ms_renderSettings[ms_numRenderSettings].emissiveness = token.GetFloat();
					}
					else
					{
						ms_renderSettings[ms_numRenderSettings].emissiveness = 0.0f;
					}

					// misc settings
					ms_renderSettings[ms_numRenderSettings].wrapLength = token.GetFloat();

					if (version>=9.0f)
					{
						int useAnisoFiltering = token.GetInt();
						ms_renderSettings[ms_numRenderSettings].useAniso = useAnisoFiltering>0;
					}
					else
					{
						ms_renderSettings[ms_numRenderSettings].useAniso = false; 
					}

					int isWashable = token.GetInt();
					ms_renderSettings[ms_numRenderSettings].isWashable = isWashable>0;

					if (version>=7.0f)
					{
						int createUnderwater = token.GetInt();
						ms_renderSettings[ms_numRenderSettings].createUnderwater = createUnderwater>0;

						int fadeUnderwater = token.GetInt();
						ms_renderSettings[ms_numRenderSettings].fadeUnderwater = fadeUnderwater>0;
					}
					else
					{
						ms_renderSettings[ms_numRenderSettings].createUnderwater = true;
						ms_renderSettings[ms_numRenderSettings].fadeUnderwater = false;
					}

					// rotate towards camera
					// note that this is mutually exclusive with the parallaxUseSteep option
					if (version>=8.0f)
					{
						int rotateTowardsCamera = token.GetInt();
						if (rotateTowardsCamera)
						{
							ms_renderSettings[ms_numRenderSettings].shaderId = DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA;
						}
					}

					ms_numRenderSettings++;
				}
			}
		}

		// remove the reference to the texture dictionary
		vfxUtil::ShutdownTxd("fxdecal");

		stream->Close();
	}
}

bool decalSettingsManager::FindRenderSettingInfo(u32 renderSettingsId, u32& renderSettingsIndex, u32& renderSettingsCount)
{
	// check for no infos
	if (decalVerifyf(renderSettingsId>0, "Invalid render setting id passed (%d)", renderSettingsId))
	{
		// search for an entry with this id
		renderSettingsIndex = DECAL_MAX_RENDER_SETTINGS;
		renderSettingsCount = 0;
		for (u32 i=0; i<ms_numRenderSettings; i++)
		{
			if (ms_renderSettings[i].id == renderSettingsId)
			{
				if (renderSettingsIndex==DECAL_MAX_RENDER_SETTINGS)
				{
					renderSettingsIndex = i;
				}

				// check that the entries are next to each other in the data file
				decalAssert(i==renderSettingsIndex+renderSettingsCount);

				renderSettingsCount++;
			}
		}

		decalAssertf(renderSettingsIndex<DECAL_MAX_RENDER_SETTINGS, "cannot find render setting id in data (%d)", renderSettingsId);
		return renderSettingsIndex<DECAL_MAX_RENDER_SETTINGS;
	}

	return false;
}

int decalSettingsManager::GetRenderSettingsIndex(u32 renderSettingsIndex, u32 renderSettingsCount)
{
	int rand = DECAL_MAX_RENDER_SETTINGS;
	if (renderSettingsIndex<DECAL_MAX_RENDER_SETTINGS)
	{
		rand = g_DrawRand.GetRanged((int)renderSettingsIndex, (int)(renderSettingsIndex+renderSettingsCount)-1);
	}

	return rand;
}

void decalSettingsManager::PatchDiffuseMap(u32 renderSettingsId, grcTexture *pDiffuseMap)
{
	u32 renderSettingsIndex;
	u32 renderSettingsCount;
	FindRenderSettingInfo(renderSettingsId, renderSettingsIndex, renderSettingsCount);
	if (decalVerifyf(renderSettingsCount==1, "more than one entry for decal render setting id - not supported when patching textures"))
	{
		if (decalVerifyf(renderSettingsIndex<ms_numRenderSettings, "invalid render settings index"))
		{
			// We do not directly apply the patch here, since the render thread
			// may still be executing the previous frame.  Instead, we buffer up
			// these requests until Update() is called in the render thread safe
			// zone.
			const unsigned reqIdx = ms_numPatchRequests++;
			decalAssert(reqIdx < NELEM(ms_patchRequests));
			ms_patchRequests[reqIdx].renderSettingsIndex = renderSettingsIndex;
			ms_patchRequests[reqIdx].pDiffuseMap = pDiffuseMap;
		}
	}
}


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
