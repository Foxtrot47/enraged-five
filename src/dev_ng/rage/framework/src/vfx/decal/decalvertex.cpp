//
// vfx/decal/decalvertex.cpp
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved.
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalvertex.h"

// includes (rage)

// includes (framework)
#include "vfx/optimisations.h"
#include "vfx/decal/decalmanagerasync.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces
namespace rage {


// code
int decalVtx::CanMerge(const decalVtx* pDecalVtxA, const decalVtx* pDecalVtxB)
{
	return memcmp(&pDecalVtxA->posX, &pDecalVtxB->posX, 20) == 0;
}


#if DECAL_STORE_UNCOMPRESSED_VERTICES

	/*static*/ bool decalVtx::UseUncompressedVertices()
	{
		return DECALMGRASYNC.GetDebug()->m_useUncompressedVertices;
	}

#endif

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
