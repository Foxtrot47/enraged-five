//
// vfx/decal/decalvertexcache.h
//
// Copyright (C) 2013-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef VFX_DECALVERTEXCACHE_H
#define VFX_DECALVERTEXCACHE_H

// includes (libc)
#include <string.h>

// includes (rage)
#include "system/dma.h"
#include "system/memops.h"

// includes (framework)
#include "vfx/channel.h"


namespace rage
{

#if !__SPU

 	///////////////////////////////////////////////////////////////////////////
 	//  decalVertexCache (non-SPU)
 	///////////////////////////////////////////////////////////////////////////

 	class decalVertexCache
 	{
 	public:

 		inline decalVertexCache(const void *ea, u32 stride_, u32 UNUSED_PARAM(firstDmaTag), bool UNUSED_PARAM(initialPrefetch))
 			: array((char*)ea)
 			, stride(stride_)
 		{
 		}

 		inline void Prefetch(unsigned UNUSED_PARAM(idx))
 		{
 		}

 		inline const void *Get(unsigned idx)
 		{
 			return array + idx*stride;
 		}


 	private:

 		const char *array;
 		unsigned stride;
 	};


#else // __SPU

 	///////////////////////////////////////////////////////////////////////////
 	//  decalVertexCache (SPU)
 	///////////////////////////////////////////////////////////////////////////

#	define VERTEX_CACHE_STATS 0

 	// Actually a reasonably generic read-only cache.  Warning!  this cannot be
 	// simply turned into a writeable cache, since it is possible for some bytes
 	// of memory to be mapped to more than one buffer.
 	class decalVertexCache
 	{
 	public:

 		inline decalVertexCache(const void *ea, u32 stride_, u32 firstDmaTag, bool initialPrefetch)
 			: dmaTag(firstDmaTag)
 			, accessCount(0)
 			, vertZeroEa((u32)ea)
 			, stride(stride_)
#			if VERTEX_CACHE_STATS
 				, hits(0)
 				, misses(0)
#			endif
 		{
 			decalAssert(stride_ + 128 <= BUFFER_SIZE);
 			if (initialPrefetch)
 			{
 				u32 vtxIdx = 0;
 				for (u32 bufIdx=0; bufIdx<NUM_BUFFERS; ++bufIdx)
 				{
 					vtxIdx = DmaGet(bufIdx, vtxIdx) + 1;
 				}
 			}
 			else
 			{
 				// invalidate caches
 				sysMemSet(firstVert, 0x00, sizeof(firstVert));
 				sysMemSet(lastVert,  0xff, sizeof(lastVert) );
 			}
 		}

 		inline ~decalVertexCache()
 		{
#			if VERTEX_CACHE_STATS
 				const float hundredDivTot = 100.f/(float)(hits + misses);
 				decalDisplayf("Decal vtx cache hits %u (%.1f%%), misses %u (%.1f%%)\n",
 					hits, (float)hits*hundredDivTot, misses, (float)misses*hundredDivTot);
#			endif
 			sysDmaWaitTagStatusAll(((1<<NUM_BUFFERS)-1) << dmaTag);
 		}

 		inline void Prefetch(unsigned idx)
 		{
 			if (Find(idx)==NOT_FOUND)
 			{
#				if VERTEX_CACHE_STATS
 					++misses;
#				endif
 				DmaGet(idx);
 			}
#		if VERTEX_CACHE_STATS
 			else
 			{
 				++hits;
 			}
#		endif
 		}

 		// Use noinline for two reasons, the obvious one is to save code space
 		// and not inline it 6 times in ProcessGeometry.  The other reason is to
 		// save stack space in assert enabled builds.  GCC is not cleaning up
 		// the stack after each alloca in an inlined function, it is only
 		// cleaned up in the prologue of the non-inlined caller.
 		__attribute__((__noinline__)) const void *Get(unsigned idx)
 		{
 			for (;;)
 			{
 				const int found = Find(idx);
 				if (Likely(found!=NOT_FOUND))
 				{
#					if VERTEX_CACHE_STATS
 						++hits;
#					endif

 					if (Unlikely(needWait[found]))
 					{
 						needWait[found] = false;
 						sysDmaWaitTagStatusAll(1<<(dmaTag+found));
 					}
 					const void *const ptr = buf[found] + offset[found] + stride*(idx - firstVert[found]);

#					if __ASSERT
 						// Verify that the memory we are returning to the caller contains the correct vertex
 						char *const checkBuf = (char*)(((u32)RageAlloca(DMA_BUF_SIZE(stride,1,16)+16)+15)&~15);
 						const char *const check = (char*)DmaGetAligned(checkBuf, vertZeroEa+idx*stride, stride, 16, DMATAG_BLOCKING);
 						sysDmaWaitTagStatusAll(1 << DMATAG_BLOCKING);
#						if 1
 							decalAssert(memcmp(check, ptr, stride) == 0);
#						else
 							if (memcmp(check, ptr, stride) != 0)
 							{
 								decalDisplayf("check 0x%p, ptr 0x%p, ea 0x%08x, stride 0x%02x, this 0x%p, idx 0x%08x, found 0x%08x\n",
 									check, ptr, vertZeroEa+idx*stride, stride, this, idx, found);
 								__debugbreak();
 							}
#						endif
#					endif
 					return ptr;
 				}

#				if VERTEX_CACHE_STATS
 					--hits;
 					++misses;
#				endif
 				DmaGet(idx);
 			}
 		}


 	private:

 		enum { BUFFER_SIZE = 512 };
 		enum { NUM_BUFFERS = 3 };   // at least 3 to handle worst case of all three verts of a tri in a different buffer
 		enum { NOT_FOUND = -1 };

 		CompileTimeAssert(!(BUFFER_SIZE&127));

 		char buf[NUM_BUFFERS][BUFFER_SIZE] ALIGNED(128);
 		s32  firstVert[NUM_BUFFERS];    // note that this can go negative
 		s32  lastVert[NUM_BUFFERS];
 		s32  offset[NUM_BUFFERS];
 		u32  lastUsed[NUM_BUFFERS];
 		bool needWait[NUM_BUFFERS];
 		u32  dmaTag;
 		u32  accessCount;
 		u32  vertZeroEa;
 		u32  stride;

#		if VERTEX_CACHE_STATS
 			u32  hits;
 			u32  misses;
#		endif

 		int Find(unsigned idx)
 		{
 			for (unsigned i=0; i<NUM_BUFFERS; ++i)
 			{
 				const s32 first = firstVert[i];
 				const s32 last  = lastVert[i];
 				if (first <= (s32)idx && (s32)idx <= last)
 				{
 					lastUsed[i] = accessCount++;
 					return i;
 				}
 			}
 			return NOT_FOUND;
 		}

 		unsigned Oldest() const
 		{
 			unsigned oldestIdx = 0;
 			unsigned oldestAge = accessCount-lastUsed[0];
 			for (unsigned i=1; i<NUM_BUFFERS; ++i)
 			{
 				const unsigned age = accessCount-lastUsed[i];
 				if (age>oldestAge)
 				{
 					oldestIdx = i;
 					oldestAge = age;
 				}
 			}
 			return oldestIdx;
 		}

 		unsigned DmaGet(unsigned bufIdx, unsigned vtxIdx)
 		{
 			// Note that we need a fenced dma here, as we may not have ever waited
 			// on the previous get to this buffer to have completed
 			const u32 vertEa = vertZeroEa + vtxIdx*stride;
 			const u32 getEa = vertEa & ~127;
 			sysDmaGetf(buf[bufIdx], getEa, BUFFER_SIZE, dmaTag+bufIdx);

 			const s32 first = vtxIdx - (vertEa&127)/stride;
 			const s32 offs = (vertZeroEa + first*stride) & 127;
 			const u32 last = first + (BUFFER_SIZE-offs)/stride - 1;
 			firstVert[bufIdx] = first;
 			lastVert [bufIdx] = last;
 			offset   [bufIdx] = offs;
 			lastUsed [bufIdx] = accessCount++;
 			needWait [bufIdx] = true;

 			return last;
 		}

 		void DmaGet(unsigned idx)
 		{
 			DmaGet(Oldest(), idx);
 		}
 	};

#	undef VERTEX_CACHE_STATS


#endif // __SPU

}
// namespace rage

#endif // VFX_DECALVERTEXCACHE_H
