//
// vfx/decal/decalhelper.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//


#ifndef VFX_DECALHELPER_H
#define	VFX_DECALHELPER_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "grcore/vertexbuffer.h"
#include "vector/color32.h"
#include "vectormath/classes.h"

// includes (framework)
#include "vfx/decal/decalsettings.h"


// namespaces 
namespace rage {


// forward declarations
class fragInst;
class fragType;
class fwEntity;
class grcVertexBuffer;
class phBound;
class phInst;


// enumerations
enum decalOverlayResult
{
	DECAL_OVERLAY_ENCLOSED			= 0,
	DECAL_OVERLAY_SEPARATE,
	DECAL_OVERLAY_OVERLAPPING,
};


// classes
class decalHelper
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// [SPHERE-OPTIMISE]
		static decalOverlayResult PreProcessOverlays(Vec3V_In posA, float radiusA, Vec3V_In posB, float& radiusB, float maxOverlayRadius);

		static ScalarV_Out GetRadiusXY(Vec3V_In vDimensions) {return Mag(vDimensions.GetXY())*ScalarV(V_HALF);}
		static ScalarV_Out GetRadius(Vec3V_In vDimensions) {return Mag(vDimensions)*ScalarV(V_HALF);}

		static bool CalcMatrix(Mat34V_InOut mtx, bool* isDamaged, const fwEntity* pAttachEntity, const phInst* pInst, int attachMatrixId, decalAttachType attachType);


	private: //////////////////////////

		// vertex buffer access
		static float UnpackNormalComponent(u32 value, u32 size, u32 shift);
		static Vec3V_Out UnpackNormalForPlatform(u32 packedNormal);
		static void UnpackNormal_11_11_10(Vec3V& vRet, u32 packedNormal);


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////	

	private: //////////////////////////

};



} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALHELPER_H
