//
// vfx/decal/decalmanager.h
//
// Copyright (C) 1999-2015 Rockstar North.  All Rights Reserved.
//


#ifndef VFX_DECALMANAGER_H
#define	VFX_DECALMANAGER_H

#if !__TOOL && !__RESOURCECOMPILER

#if __SPU
// use #include instead of #error, as then gcc gives the include stack
#include "error, decalmanager.h not supported in spu code"
#endif


// includes (rage)
#include "grcore/config.h"
#include "grcore/effect.h"
#include "grcore/stateblock.h"
#include "physics/levelnew.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/nelem.h"
#include "system/taskheader.h"
#include "vectormath/classes.h"

// includes (framework)
#include "vfx/slist.h"
#include "vfx/decal/decalasynctasktype.h"
#include "vfx/decal/decalbucket.h"
#include "vfx/decal/decaldebug.h"
#include "vfx/decal/decalinst.h"
#include "vfx/decal/decalmanagerasync.h"
#include "vfx/decal/decalminiidxbuf.h"
#include "vfx/decal/decalshader.h"

#if RSG_PC && __D3D11
struct ID3D11Buffer;
#elif __XENON
struct D3DIndexBuffer;
struct D3DVertexBuffer;
#endif


// namespaces
namespace rage {


// defines
#define DECAL_MAX_ADD_INFOS						(32)
#define DECAL_MAX_CULLED_COMPONENTS				(64)

#define	DECAL_POOL_PERSIST_RATIO				(0.5f)
#define	DECAL_MAX_PERSISTENT_BUCKETS			(DECAL_MAX_BUCKETS*DECAL_POOL_PERSIST_RATIO)
#define	DECAL_MAX_PERSISTENT_INSTS				(DECAL_MAX_INSTS*DECAL_POOL_PERSIST_RATIO)
#define	DECAL_MAX_PERSISTENT_TRIS				(DECAL_MAX_TRIS*DECAL_POOL_PERSIST_RATIO)
#define	DECAL_MAX_PERSISTENT_VERTS				(DECAL_MAX_VERTS*DECAL_POOL_PERSIST_RATIO)

// WARNING: This value cannot be increased too high, or else we can fail to
// allocate a task from the sysTaskManager.  The decal code handles this, but
// not all other code that uses sysTaskManager does.
#define DECAL_MAX_ASYNC_PROCESS_INST_TASKS      (32)


// The PC DX11 debug runtime complains unless vertex buffers have a stride that
// is a multiple of 4.  That seems reasonable given all the different hardware
// it needs to run on.
//
// PC gives the (non-fatal) error
//     D3D11: ERROR: ID3D11DeviceContext::DrawIndexed: Vertex Buffer Stride (1) at the input vertex slot 1 is not aligned properly. The current Input Layout imposes an alignment of (4) because of the Formats used with this slot. [ EXECUTION ERROR #367: DEVICE_DRAW_VERTEX_STRIDE_UNALIGNED ]
//
#if RSG_DURANGO || RSG_ORBIS
	typedef u8      decalCbufIdx;
#elif RSG_PC
	typedef u32     decalCbufIdx;
#endif

typedef Functor1Ret<grcBlendStateHandle, decalRenderPass>	decalBlendStateFunctor;

enum decalStaticShaderId
{
	DECAL_STATIC_SHADER_PARALLAX				= 0,
	DECAL_STATIC_SHADER_PARALLAX_STEEP,
	DECAL_STATIC_SHADER_PARALLAX_GLASS,
	DECAL_STATIC_SHADER_PARALLAX_GLASS_STEEP,
	DECAL_STATIC_SHADER_ROTATE_TOWARDS_CAMERA,

	DECAL_NUM_STATIC_SHADERS
};

enum decalDynamicShaderId
{
	DECAL_DYNAMIC_SHADER_PARALLAX				= 0,
	DECAL_DYNAMIC_SHADER_PARALLAX_STEEP,
	DECAL_NUM_DYNAMIC_SHADERS
};



// forward declarations
#if DECAL_PROCESS_BREAKABLES
class  bgBreakable;
struct bgContactReport;
#endif
class  crSkeleton;
class  crSkeletonData;
struct decalAsyncTaskDescBase;
struct decalRenderSettingsInfo;
class  decalRenderStaticBatcher;
class  decalSettings;
struct decalTypeSettings;
class  fragInst;
class  fragType;
struct grcVertexDeclaration;
class  grmMatrixSet;
class  phBound;
class  phBoundPolyhedron;
class  phInst;
class  phPolygon;
class  rmcDrawable;


// structures
struct decalAddInfo
{
	decalSettings           settings;
	fwRegdRef<fwEntity>     entity;
	bool                    shouldProcess;
	int                     id;
};

struct decalRemoveInfo
{
	decalInst* pInst;
	decalBucket* pBucket;
	SList<decalBucket>* pBucketList;
};

// classes
class decalManager : public decalManagerAsync
{
	friend class decalBucket;
	friend class decalDebug;
	friend class decalInst;
	friend class decalLodSystem;
	friend class decalRecycler;
	friend class decalRenderStaticBatcher;


	///////////////////////////////////
	// FUNCTIONS
	///////////////////////////////////

	public: ///////////////////////////

		decalManager();
		~decalManager();
		static inline decalManager &GetInstance() { return *ms_instance; }

		void InitShaders();
#if RSG_PC
		void DeleteShaders();
		void ResetShaders();
#endif

		void InitSettings(int numTypeSettings, decalTypeSettings* pTypeSettings, int numRenderSettingsFiles, decalRenderSettingsInfo* pRenderSettingsInfos);
		void Init(unsigned initMode);
		void Shutdown(unsigned shutdownMode);

		int AddSettings(fwEntity* entity, decalUserSettings& settings, bool overridePauseCheck);
		int AddSettings(fwEntity* entity, decalUserSettings& settings, int forceDecalId, bool overridePauseCheck);

#if __PS3
		// This function signature intentionally matches
		//      void RegisterSpuTask(decalAsyncTaskType type, TASK_INTERFACE(taskName));
		void RegisterSpuTask(decalAsyncTaskType type, const char *name, const char *binStart, const char *binSize);
#endif

		bool OnDelete(const fwEntity* pEntity);
		bool OnDelete(const phInst* pPhInst);

		// Block on any async tasks kicked last frame, and do minimal house
		// keeping chores.  Must be called even when game paused.  Must be
		// called during the render thread safe zone.
		void UpdateCompleteAsync();

		// Main part of update.  Starts new async work, and does remained of
		// house keeping.  Must be called even when game paused.  Should be
		// called just after the render thread safe zone has finished.
		void UpdateStartAsync(float deltaTime);

		void Render(decalRenderPass firstRenderPass, decalRenderPass lastRenderPass, u32 exceptionTypeSettingsFlags, bool bIsForwardPass, bool bResetCtxIndex, bool dynamicOnly);
		void ResetContext();


#if __BANK
		void InitWidgets();
		void RenderDebug();
#endif

#if DECAL_PROCESS_BREAKABLES
		static void BreakableStateChangedCB(bgContactReport* pReport);
#endif

		// Renderstate handling
		void SetStaticDecalRasterizerState(grcRasterizerStateHandle state)									{ m_staticDecalRasterizerState = state; }
		void SetStaticDecalBlendState(grcBlendStateHandle state)											{ m_staticDecalBlendState = state; }
		void SetStaticDecalDepthStencilState(decalRenderPass renderPass, grcDepthStencilStateHandle state, unsigned int stencilRef)		{ m_staticDecalDepthStencilState[renderPass] = state; m_staticDecalStencilRef[renderPass] = stencilRef; }
		void SetStaticDecalExitRasterizerState(grcRasterizerStateHandle state)								{ m_staticDecalExitRasterizerState = state; }
		void SetStaticDecalExitBlendState(grcBlendStateHandle state)										{ m_staticDecalExitBlendState = state; }
		void SetStaticDecalExitDepthStencilState(grcDepthStencilStateHandle state)							{ m_staticDecalExitDepthStencilState = state; }

		void SetDynamicDecalRasterizerState(grcRasterizerStateHandle state)									{ m_dynamicDecalRasterizerState = state; }
		void SetDynamicDecalBlendState(grcBlendStateHandle state)											{ m_dynamicDecalBlendState = state; }
		void SetDynamicDecalDepthStencilState(decalRenderPass renderPass, grcDepthStencilStateHandle state, unsigned int stencilRef)	{ m_dynamicDecalDepthStencilState[renderPass] = state; m_dynamicDecalStencilRef[renderPass] = stencilRef; }
		void SetDynamicDecalExitRasterizerState(grcRasterizerStateHandle state)								{ m_dynamicDecalExitRasterizerState = state; }
		void SetDynamicDecalExitBlendState(grcBlendStateHandle state)										{ m_dynamicDecalExitBlendState = state; }
		void SetDynamicDecalExitDepthStencilState(grcDepthStencilStateHandle state)							{ m_dynamicDecalExitDepthStencilState = state; }

		// Notify the decal manager that the vehicle glass mesh has changed so decals can be updated
		bool VehicleGlassStateChanged(const fwEntity* pAttachEntity, void* pSmashGroup);

		// utility functions
		void Wash(float amount, const fwEntity* pEntity, bool dueToRain);
		void WashInRange(float amount, Vec3V_In pos, ScalarV_In range, const fwEntity* pEntity);

		float GetWashLevel(int decalId);
		bool IsAlive(int decalId);
		void Remove(int decalId);
		void Remove(const fwEntity* pEntity, const int boneIndex, const void* pSmashGroup, u32 exceptionTypeSettingsFlags);
		void RemoveFacing(Vec3V_In vDir, const fwEntity* pEntity, const void* pSmashGroup);
		int RemoveInRange(Vec3V_In vPos, ScalarV_In range, const fwEntity* pEntity, const void* pSmashGroup);
		void FadeInRange(Vec3V_In vPos, ScalarV_In range, float fadeTime, const fwEntity* pEntity, const void* pSmashGroup);

		void UpdateAttachEntity(const fwEntity* pEntityFrom, fwEntity* pEntityTo);

		// query functions
		bool IsWaitingToAdd(u32 typeSettingsFlags, const fwEntity* pEntity);
		bool IsPointInLiquid(u32 typeSettingsFlags, s8& liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, u32& foundDecalTypeFlag, float distThresh);
		bool IsPointWithin(u32 typeSettingsFlags, s8 liquidType, Vec3V_In vPos, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, u32& foundDecalTypeFlag, float distThresh);
		bool FindClosest(u32 typeSettingsFlags, s8 liquidType, Vec3V_In vPos, float range, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, Vec3V_InOut vClosestPosWld, fwEntity** ppClosestEntity, u32& foundDecalTypeFlag);
		bool FindFarthest(u32 typeSettingsFlags, s8 liquidType, Vec3V_In vPos, float range, float minDecalSize, bool removeDecal, bool fadeDecal, float fadeTime, Vec3V_InOut vFarthestPosWld, fwEntity** ppFarthestEntity, u32& foundDecalTypeFlag);
		decalBucket* FindFirstBucket(u32 typeSettingsFlags, const fwEntity* pEntity);

		void RegisterDecalRemovedCallback(DecalRemovedCallback *callback);

		bool IsDecalPending(int decalId) const;

		void SetOverrideBlendStateFunctor(decalBlendStateFunctor functor) {m_overrideStaticBlendStateFunctor = functor;}
		decalBlendStateFunctor GetOverrideBlendStateFunctor() {return m_overrideStaticBlendStateFunctor;}

		grcBlendStateDesc GetStaticBlendStateDesc() {return m_staticBlendStateDesc;}

		u32 GetNumAddInfos() { return m_numAddInfos; }

#		if RSG_PC && __D3D11
			static void PrimaryRenderThreadBeginFrameCallback();
#		endif


	private: //////////////////////////

		void OnDeleteBlock();
		template<class CMP> bool OnDeleteCmp(CMP &cmp, const decalAsyncTaskDescBase *td);
		template<class CMP> bool OnDeleteCmp(CMP &cmp);

		decalBucket* GetMergableBucket(decalRenderPass renderPass, decalMethod method, Vec3V_In vPosition, u16 typeSettingsIndex, u16 renderSettingsIndex, u32 msOffsetInt, u16 subType, decalAttachType attachType, fwEntity* pAttachEntity, s16 attachMatrixId, void* pSmashGroup, bool isGlass, bool isDamaged, bool forceRegen, fwInteriorLocation interiorLocation, bool isScripted);
		decalBucket* GetMergableBucket(decalRenderPass renderPass, decalMethod method, const decalBucket *pBucket);
		void OrderredInsertBucket(decalRenderPass renderPass, decalMethod method, decalBucket *pNewBucket);
		decalBucket* SyncGetBucket(decalInst* pDecalInst, decalRenderPass renderPass, decalMethod method, Vec3V_In vPosition, u16 typeSettingsIndex, u16 renderSettingsIndex, float posScale, u32 msOffsetInt, u16 subType, decalAttachType attachType, fwEntity* pAttachEntity, Mat34V_In wldMtx, s16 attachMatrixId, void* pSmashGroup, bool isGlass, bool isDamaged, bool forceRegen, bool isPersistent, fwInteriorLocation interiorLocation, bool isScripted);

#		if __PS3
			void KickVehDamageTask();
			void RecycleVehDamageTask();
			void SyncRsxToVehDamageTask();
#		endif

#		if RSG_PC && __D3D11
			enum ReinitArg { UNINITIALIZED_VERTICES, VERTICES_INITIALIZED_FROM_POOL };
			void CreateDX11Buffers(ReinitArg reinitArg);
			void DestroyDX11Buffers();
			static void DeviceLostCallback();
			static void DeviceResetCallback();
#		endif

		void MergeBuckets(decalRenderPass renderPass, decalMethod method, decalBucket *firstNewBucket);
		void Synchronize();

		void Render_Static(decalRenderPass renderPass, u32 exceptionTypeSettingsFlags, bool bIsForwardPass DECAL_MULTI_THREADED_RENDERING_ONLY(, unsigned ctxIdx));
		void Render_Dynamic(decalRenderPass renderPass, u32 exceptionTypeSettingsFlags, bool bIsForwardPass);

		void ProcessAddInfos();

		// pre processing pipeline
		bool PreProcessSetting(fwEntity* entity, decalUserSettings& settings, s32 currAddInfoIndex);
		bool PreProcessOverlays(const fwEntity* entity, decalUserSettings& settings);
		bool PreProcessLiquidPools(decalUserSettings& settings);
		float FindClosestDistSqr(Vec3V_In vPos, u32 typeSettingsIndex, const fwEntity* pEntity);

		// processing pipeline
		void ProcessSetting_Dynamic(fwEntity* entity, decalSettings& settings);
		void ProcessSetting_Static(fwEntity* entity, decalSettings& settings, phLevelNew* pPhysicsLevel, u32 physicsIncludeFlags);

		bool CheckAlreadyPackedSkeleton(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId) const;
		enum PackSkelRet { PACKSKEL_FAILED, PACKSKEL_REUSE, PACKSKEL_NEW, PACKSKEL_EMPTYSKEL, PACKSKEL_NOSKEL };
		PackSkelRet PackSkeletons(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned* outDamagedDrawableMask, unsigned *outNumDrawables, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const crSkeleton* pUndamagedSkel, const crSkeleton* pDamagedSkel);
		PackSkelRet PackSkeletonData(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const crSkeletonData* pSkelData);
		PackSkelRet PackMatrixSet(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const crSkeletonData* pSkelData, const grmMatrixSet* pMatrixSet);
		PackSkelRet PackFragSkeletons(unsigned *outNumSkelMtxs, const Vec4V** outSkelMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy)* outSkelBones, unsigned *outNumDrawables, unsigned* outDamagedDrawableMask, u8** outSkelDrawableIdxs, const rmcDrawable** outDrawablePtrs, const Mat34V **outSkelInvJoints, const fwEntity *pEntity, decalBoneHierarchyIdx lodAttachMatrixId, const fragInst* pFragInst, const fragType* pFragType);
		void RemoveUnusedDrawables(unsigned *inOutNumDrawables, unsigned* outDamagedDrawableMask, const rmcDrawable** inOutDrawablePtrs, unsigned numSkelMtxs, u8** inOutSkelDrawableIdxs);

		void KickProcessDrawables(decalSettings& settings, fwEntity* pEntity, phInst* pPhInst, unsigned numDrawables, unsigned damagedDrawableMask, const rmcDrawable*const* pDrawables, Mat34V_In entityMtx, unsigned numSkelMtxs, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8* skelDrawableIdxs, const Mat34V* skelInvJoints, phLevelNew* pPhysicsLevel);
		void KickProcessInstAsync(decalAsyncTaskType type, decalSettings& settings, fwEntity* pEntity, phInst *pPhInst, const rmcDrawable* pDrawable, Mat34V_In entityMtx, phLevelNew* pPhysicsLevel);
		void KickProcessInstAsync(decalAsyncTaskType type, decalSettings& settings, fwEntity* pEntity, phInst *pPhInst, unsigned numDrawables, unsigned damagedDrawableMask, const rmcDrawable*const* pDrawables, Mat34V_In entityMtx, unsigned numSkelMtxs, const Vec4V* skelObjMtxs, DECAL_BONEREMAPLUT(Task,Hierarchy) skelBones, const u8* skelDrawableIdxs, const Mat34V* skelInvJoints, phLevelNew* pPhysicsLevel);

		void ProcessInstAsync(decalSettings& settings, phInst* pInst, phLevelNew* pPhysicsLevel);
#if USE_DEFRAGMENTATION
		bool DefragLock(decalAsyncTaskDescBase* taskDesc);
#endif

		// house keeping
#if __ASSERT
		void InvariantInst(const decalInst *pDecalInst, u8 *allUsedMibs, u8 *allUsedVtxs, unsigned *inUseMibs, unsigned *inUseVtxs, decalMethod method, u16 *decalIdCounts) const;
		bool Invariant() const;
#endif
		void ProcessMarkedForDelete();
		void DelayedDeletion(decalInst* pDecalInst);
		void DelayedDeletion(decalInst* pDecalInstHead, decalInst* pDecalInstTail, unsigned count);

		// misc
		bool IsFlaggedType(u32 typeSettingsIndex, u32 typeSettingsFlags);

		// render state
		void InitRenderStateBlocks();
		void PrepareStaticRenderPass(decalRenderPass renderPass);
		void FinishStaticRenderPass();
		void PrepareDynamicRenderPass(decalRenderPass renderPass);
		void FinishDynamicRenderPass();

	///////////////////////////////////
	// VARIABLES
	///////////////////////////////////

	private: //////////////////////////

		static decalManager *ms_instance;

		// task management
		decalAsyncTaskDescBase *m_runningAsyncTaskDesc;
		decalAsyncTaskDescBase *m_runningSyncTaskDesc;

#		if !__ASSERT && USE_DEFRAGMENTATION
			// The asynchronous code needs access to this in assert enabled
			// builds.  In assert disabled builds, it can live here instead.
			decalDefragLocker       m_defragLocker;
#		endif

#		if __PS3
			struct SpuTask
			{
				const char *m_name;
				const char *m_binStart;
				const char *m_binSize;
			};
			SpuTask                 m_spuTasks[NUM_DECAL_ASYNC_TASK_TYPES];
#		endif

		// active bucket lists
		SList<decalBucket>      m_runningBucketLists    [DECAL_NUM_RENDER_PASSES][DECAL_NUM_METHODS];
		AsyncSList<decalBucket> m_synchronousBucketLists[DECAL_NUM_RENDER_PASSES][DECAL_NUM_METHODS];
		void CompileTimeAssertChecker()
		{
			CompileTimeAssert(NELEM(m_synchronousBucketLists)    == NELEM(m_pendingBucketLists));
			CompileTimeAssert(NELEM(m_synchronousBucketLists[0]) == NELEM(m_pendingBucketLists[0]));
		}

		// Counters for determining the primary decal instance for lodding.
		u8 m_taskInstCounters[DECAL_MAX_ASYNC_PROCESS_INST_TASKS];
		unsigned m_taskInstCountersUsed;

		// Additional data for tasks
		// Allocated from both ends, out of memory when the two free pointers meet each other
		u8 *m_taskDataStoreBase;
		u8 *m_taskDataStoreFreeLower;
		u8 *m_taskDataStoreFreeUpper;

		// stored settings to process
		unsigned m_numAddInfos;
		decalAddInfo m_addInfos[DECAL_MAX_ADD_INFOS];

		// Delay render thread execution until update has been completed.  This
		// allow moving the update out of the render thread safe zone.  The
		// semaphore count can also be limited to 1 if multiple render threads
		// are enabled, and the code needs to synchronize between them.
		sysIpcSema m_renderSema;

		// sampler states
		grcSamplerStateHandle m_samplerStateWrap;
		grcSamplerStateHandle m_samplerStateClamp;
		grcSamplerStateHandle m_samplerStateAnisoClamp;
		grcSamplerStateHandle m_samplerStateAnisoWrap;

#		if !USE_EDGE
			grcSamplerStateHandle m_samplerStateDamage;
#		endif

		// render state blocks
		grcRasterizerStateHandle	m_staticDecalRasterizerState;
		grcBlendStateHandle			m_staticDecalBlendState;
		grcDepthStencilStateHandle	m_staticDecalDepthStencilState[DECAL_NUM_RENDER_PASSES];
		grcRasterizerStateHandle	m_staticDecalExitRasterizerState;
		grcBlendStateHandle			m_staticDecalExitBlendState;
		grcDepthStencilStateHandle	m_staticDecalExitDepthStencilState;

		grcBlendStateDesc			m_staticBlendStateDesc;

		grcRasterizerStateHandle	m_dynamicDecalRasterizerState;
		grcBlendStateHandle			m_dynamicDecalBlendState;
		grcDepthStencilStateHandle	m_dynamicDecalDepthStencilState[DECAL_NUM_RENDER_PASSES];
		grcRasterizerStateHandle	m_dynamicDecalExitRasterizerState;
		grcBlendStateHandle			m_dynamicDecalExitBlendState;
		grcDepthStencilStateHandle	m_dynamicDecalExitDepthStencilState;

		decalBlendStateFunctor		m_overrideStaticBlendStateFunctor;

		unsigned int				m_staticDecalStencilRef[DECAL_NUM_RENDER_PASSES];
		unsigned int				m_dynamicDecalStencilRef[DECAL_NUM_RENDER_PASSES];

		// shaders
		decalShader_Static          m_shaderStatic[DECAL_NUM_STATIC_SHADERS];
		decalShader_Dynamic         m_shaderDynamic[DECAL_NUM_DYNAMIC_SHADERS];
		decalVB_Dynamic             m_vtxBufferDynamic;


		// This is the number of frames that the render thread may be ahead of
		// the GPU plus 1.  eg. if the render thread can be one frame ahead of
		// the GPU, then double buffering is required.
#if RSG_PC
		#define NUM_RENDER_LATENCY_BUFFERS (MAX_FRAMES_RENDER_THREAD_AHEAD_OF_GPU + 1)
#else
		enum { NUM_RENDER_LATENCY_BUFFERS = MAX_FRAMES_RENDER_THREAD_AHEAD_OF_GPU + 1 };
#endif 

#		if DECAL_MULTI_THREADED_RENDERING
			enum { MAX_RENDER_STATIC_CALLS_PER_FRAME = 2 };
#			if __ASSERT
				volatile u32                m_numRenderStaticCalls;
#			endif

			// We assume that each call to Render_Static per frame, can happen
			// concurrently.  We need to create enough "contexts" for these
			// threads to be able to execute without needing further
			// synchronization.
#if RSG_PC
			#define NUM_PER_CONTEXT_BUFFERS (NUM_RENDER_LATENCY_BUFFERS * MAX_RENDER_STATIC_CALLS_PER_FRAME)
#else
			enum
			{
				NUM_PER_CONTEXT_BUFFERS
					= NUM_RENDER_LATENCY_BUFFERS
					* MAX_RENDER_STATIC_CALLS_PER_FRAME
			};
#endif // RSG_PC

			// Atomically incremented to allocate a context.
			volatile u32                m_mostRecentlyUsedPerContextBuffer;

#		endif


#		if RSG_DURANGO
			// Vertex declarations
			grcVertexDeclaration        *m_staticVtxDecl;
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				grcVertexDeclaration        *m_debugStaticVtxDecl;
#			endif

			u16                         *m_staticIdxBufData[NUM_PER_CONTEXT_BUFFERS];
			unsigned                     m_staticIdxBufLastAdded[NUM_PER_CONTEXT_BUFFERS];
			decalCbufIdx                *m_staticConstBufIdxData[NUM_PER_CONTEXT_BUFFERS];

#		elif RSG_ORBIS
			grcVertexDeclaration        *m_staticDummyVtxDecl;

			u16                         *m_staticIdxBufData[NUM_PER_CONTEXT_BUFFERS];
			unsigned                     m_staticIdxBufLastAdded[NUM_PER_CONTEXT_BUFFERS];
			decalCbufIdx                *m_staticConstBufIdxData[NUM_PER_CONTEXT_BUFFERS];

#		elif RSG_PC && __D3D11
			// Vertex declarations
			grcVertexDeclaration        *m_staticVtxDecl;
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				grcVertexDeclaration        *m_debugStaticVtxDecl;
#			endif

			// Unlike other platforms, the memory held in
			// decalManagerAsync::m_pools cannot be directly used by the GPU due
			// to DX11 API limitations.  So every frame, the buffer is locked
			// with D3D11_MAP_WRITE_NO_OVERWRITE, and any new vertices that were
			// created are updated.  The list of vertices needing an update are
			// maintained in decalManagerAsync base class, since it is added to
			// by the worker threads that generate the decals.
			ID3D11Buffer                *m_staticVtxBuf;

			ID3D11Buffer               **m_staticIdxBufHeader; //[NUM_PER_CONTEXT_BUFFERS];
			ID3D11Buffer               **m_staticConstBufIdxHeader; //[NUM_PER_CONTEXT_BUFFERS];
			unsigned                    *m_staticIdxBufLastAdded; //[NUM_PER_CONTEXT_BUFFERS];

			ID3D11Buffer                *m_staticConstBuf;

			bool                         m_gpuUploadRenderThreadCopyAll;
			u32                          m_gpuUploadRenderThreadBegin;
			u32                          m_gpuUploadRenderThreadEnd;

#		elif RSG_PS3
			u32                         *m_vehDamageOffset[NUM_RENDER_LATENCY_BUFFERS];
			u32                          m_vehDamageFrame;
			u32                          m_vehDamageRsxLabels;
			u32                          m_vehDamageSeqNum;
			sysTaskHandle                m_vehDamageRunningTask;

#		elif RSG_XENON
			// Vertex declarations
			grcVertexDeclaration        *m_staticVtxDecl;
#			if DECAL_STORE_UNCOMPRESSED_VERTICES
				grcVertexDeclaration        *m_debugStaticVtxDecl;
#			endif

			u16                         *m_staticIdxBufData[NUM_RENDER_LATENCY_BUFFERS];
			D3DIndexBuffer              *m_staticIdxBufHeader;
			unsigned                     m_staticIdxBufFrame;
			unsigned                     m_staticIdxBufLastAdded;

			// On 360, the vertex buffer is just a wrapper structure around the
			// pool.  We never lock the vertex buffer, we just ensure that we
			// modify it only when safe to do so (ie, only modify vertices that
			// cannot be currently being used by the GPU).
			D3DVertexBuffer             *m_staticVtxBuf;

#		endif

		// Delayed deletion.  This is done on the main thread, so one extra
		// buffer is needed over NUM_RENDER_LATENCY_BUFFERS.
#if RSG_PC
		SList<decalInst> *m_delayedDeletionQueues; //[NUM_RENDER_LATENCY_BUFFERS+1];
#else
		SList<decalInst> m_delayedDeletionQueues[NUM_RENDER_LATENCY_BUFFERS+1];
#endif // RSG_PC

		unsigned m_delayedDeletionFrame;

		// misc
		bool m_persistentLimitReached;
		bool m_bInitialized;
		decalStats m_statsPersistent;
		decalStats m_statsFading;
};


#define DECALMGR (::rage::decalManager::GetInstance())


} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECALMANAGER_H
