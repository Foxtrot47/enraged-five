//
// vfx/decal/decalshader.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

#if !__TOOL && !__RESOURCECOMPILER

// includes (us)
#include "vfx/decal/decalshader.h"

// includes (rage)
#include "bank/bank.h"
#include "fwtl/customvertexpushbuffer.h"
#include "grcore/viewport.h"
#include "grmodel/shader.h"
#if __D3D11
#	include "grcore/wrapper_d3d.h"
#elif RSG_ORBIS
#	include "grcore/wrapper_gnm.h"
#	include "grcore/gnmx.h"
#endif

// includes (framework)
#include "vfx/optimisations.h"
#include "vfx/vfxwidget.h"
#include "vfx/channel.h"
#include "vfx/decal/decalcallbacks.h"
#include "vfx/decal/decalsettings.h"


// optimisations
VFX_FW_DECAL_OPTIMISATIONS()


// namespaces 
namespace rage {


// code
void decalShader::Init(const char* pName)
{
	m_pShader = grmShaderFactory::GetInstance().Create();
	decalAssertf(m_pShader, "cannot create '%s' shader", pName);
	if (!m_pShader->Load(pName))
	{
		decalAssertf(0, "error loading '%s' shader", pName);
	}
}


void decalShader::Shutdown()
{
	if (m_pShader) delete m_pShader;
	m_pShader = NULL;
}


RETURNCHECK(bool) decalShader::Start(int pass)
{
	const bool ok = !!m_pShader->BeginDraw(grmShader::RMC_DRAW, false);
	decalAssertf(ok, "decalShader::Start failed for shader %s pass %d (RMC_DRAW)", m_pShader->GetName(), pass);
	if (Unlikely(!ok))
	{
		return false;
	}
	m_pShader->Bind(pass);
	return true;
}


void decalShader::End()
{
	m_pShader->UnBind();
	m_pShader->EndDraw();
}


void decalShader_Static::LookUpVars()
{
	m_shaderVarIdDiffuseMap = m_pShader->LookupVar("DiffuseTex");
	m_shaderVarIdBumpMap = m_pShader->LookupVar("BumpTex");

	// tbr: SpecularTex is looked up with mustExist = false because not all versions 
	// of the shader use specular map - does this require a stronger check?
	m_shaderVarIdSpecularMap= m_pShader->LookupVar("SpecularTex", false);	
	m_shaderVarIdSpecular = m_pShader->LookupVar("Specular");
	m_shaderVarIdSpecularColor = m_pShader->LookupVar("SpecularColor");
	m_shaderVarIdSpecularFresnel = m_pShader->LookupVar("Fresnel");
	m_shaderVarIdParallaxScale = m_pShader->LookupVar("ParallaxScaleBias");
	m_shaderVarIdZShiftScale = m_pShader->LookupVar("zShiftScale");
	m_shaderVarIdPositionScale = m_pShader->LookupVar("gPositionScale");
	m_shaderVarIdRotationCenter = m_pShader->LookupVar("gRotationCenter", false);
	m_shaderVarIdAlignToCamRot = m_pShader->LookupVar("gAlignToCamRot", false);
#	if RSG_PS3
		m_shaderVarIdVehDamageOffsetScale = m_pShader->LookupVar("gVehDamageOffsetScale");
#	endif

#	if !USE_EDGE
		m_shaderVarIdDamageOn = grcEffect::LookupGlobalVar("switchOn");
		m_shaderVarIdDamageMap = m_pShader->LookupVar("damagetexture");
		m_shaderVarIdDamageBoundRadius = m_pShader->LookupVar("BoundRadius");
		m_shaderVarIdDamageMult = m_pShader->LookupVar("DamageMultiplier");
#	endif


#	if RSG_PS3 || RSG_XENON
		const grcEffect *const effect = &m_pShader->GetInstanceData().GetBasis();
		const bool required = true;
		grcEffectVar v;
		if ((v = m_pShader->LookupVar("gConstSet0", required)) != grcevNONE)
		{
			m_perInstVtxConstReg = effect->GetVarRegister(v);
		}
#		if __ASSERT
			if ((v = m_pShader->LookupVar("gVertexColor", required)) != grcevNONE)
			{
				decalAssert(effect->GetVarRegister(v) == m_perInstVtxConstReg+1);
			}
			if ((v = m_pShader->LookupVar("gTexcoordConst", required)) != grcevNONE)
			{
				decalAssert(effect->GetVarRegister(v) == m_perInstVtxConstReg+2);
			}
#		endif
#	endif
}


void decalShader_Static::SetPerBucketVarsLocal(grcTexture* pDiffuseMap, grcTexture* pNormalMap, grcTexture* pSpecularMap, float specFalloff, float specIntensity, float specFresnel, float parallaxScale, float zShift, bool damageOn, grcTexture* pDamageMap, float damageBoundRadius, float damageMult, float UNUSED_PARAM(emissiveMultiplier), ScalarV_In posScale, Vec3V_In rotationCenter, float alignToCamRot PS3_ONLY(, float vehDamageOffsetScale))
{
	// setup the shader local variables
	m_pShader->SetVar(m_shaderVarIdDiffuseMap, pDiffuseMap);
	m_pShader->SetVar(m_shaderVarIdBumpMap, pNormalMap);
	m_pShader->SetVar(m_shaderVarIdSpecularMap, pSpecularMap);
	m_pShader->SetVar(m_shaderVarIdSpecular, specFalloff);
	m_pShader->SetVar(m_shaderVarIdSpecularColor, specIntensity);
	m_pShader->SetVar(m_shaderVarIdSpecularFresnel, specFresnel);
	m_pShader->SetVar(m_shaderVarIdParallaxScale, parallaxScale);
	m_pShader->SetVar(m_shaderVarIdZShiftScale, zShift);
	m_pShader->SetVar(m_shaderVarIdPositionScale, Vec3V(posScale));
	m_pShader->SetVar(m_shaderVarIdRotationCenter, rotationCenter);
	m_pShader->SetVar(m_shaderVarIdAlignToCamRot, alignToCamRot);
#	if __PS3
		m_pShader->SetVar(m_shaderVarIdVehDamageOffsetScale, vehDamageOffsetScale);
#	endif

#	if !USE_EDGE
		grcEffect::SetGlobalVar(m_shaderVarIdDamageOn, damageOn);

		Assert( (damageOn && pDamageMap) || !damageOn);
		m_pShader->SetVar(m_shaderVarIdDamageMap, damageOn && pDamageMap ? pDamageMap : const_cast<grcTexture*>(grcTexture::NoneBlack));
		m_pShader->SetVar(m_shaderVarIdDamageBoundRadius, damageBoundRadius);
		m_pShader->SetVar(m_shaderVarIdDamageMult, damageMult);
#	else
		damageOn = damageOn;
		pDamageMap = pDamageMap;
		damageBoundRadius = damageBoundRadius;
		damageMult = damageMult;
#	endif
}


void decalShader_Static::UnbindTextures() 
{
	const grcTexture* pNullTexture = NULL;
	m_pShader->SetVar(m_shaderVarIdDiffuseMap, pNullTexture);
	m_pShader->SetVar(m_shaderVarIdBumpMap, pNullTexture);
	m_pShader->SetVar(m_shaderVarIdSpecularMap, pNullTexture);
#if !USE_EDGE
	m_pShader->SetVar(m_shaderVarIdDamageMap, pNullTexture);
#endif
}


void decalShader_Static::PushSamplerState(grcSamplerStateHandle samplerStateHandle
#	if !USE_EDGE
		, grcSamplerStateHandle damageSamplerStateHandle
#	endif
)
{
	m_pShader->PushSamplerState(m_shaderVarIdDiffuseMap, samplerStateHandle);
	m_pShader->PushSamplerState(m_shaderVarIdBumpMap, samplerStateHandle);
	if (m_shaderVarIdSpecularMap!=grcevNONE)
	{
		m_pShader->PushSamplerState(m_shaderVarIdSpecularMap, samplerStateHandle);
	}
#if !USE_EDGE
	if (m_shaderVarIdDamageMap!=grcevNONE)
	{
		m_pShader->PushSamplerState(m_shaderVarIdDamageMap, damageSamplerStateHandle);
	}
#endif
}


void decalShader_Static::PopSamplerState()
{
	m_pShader->PopSamplerState(m_shaderVarIdDiffuseMap);
	m_pShader->PopSamplerState(m_shaderVarIdBumpMap);
	if (m_shaderVarIdSpecularMap!=grcevNONE)
	{
		m_pShader->PopSamplerState(m_shaderVarIdSpecularMap);
	}
#if !USE_EDGE
	if (m_shaderVarIdDamageMap!=grcevNONE)
	{
		m_pShader->PopSamplerState(m_shaderVarIdDamageMap);
	}
#endif
}


void decalShader_Dynamic::LookUpVars()
{
	m_shaderVarIdDecalTexture = m_pShader->LookupVar("decalTexture");
	m_shaderVarIdDecalNormal = m_pShader->LookupVar("decalNormal");
	//these samplers are global
	m_shaderVarIdDepthTexture = grcEffect::LookupGlobalVar("gbufferTextureDepthGlobal");
	m_shaderVarIdNormalTexture = grcEffect::LookupGlobalVar("gbufferTexture1Global");

	m_shaderVarIdScreenSize = m_pShader->LookupVar("deferredLightScreenSize");
	m_shaderVarIdProjParams = m_pShader->LookupVar("deferredProjectionParams");
	m_shaderVarIdSpecular = m_pShader->LookupVar("Specular");
	m_shaderVarIdSpecularColor = m_pShader->LookupVar("SpecularColor");
	m_shaderVarIdSpecularFresnel = m_pShader->LookupVar("Fresnel");
	m_shaderVarIdParallaxScale = m_pShader->LookupVar("ParallaxScaleBias");
	m_shaderVarIdEmissiveMultiplier = m_pShader->LookupVar("EmissiveMultiplier");
	m_shaderVarIdPolyRejectThreshold = m_pShader->LookupVar("PolyRejectThreshold");
}


void decalShader_Dynamic::SetVarsGlobal()
{
	// set the depth texture in global sampler
	grcEffect::SetGlobalVar(m_shaderVarIdDepthTexture, g_pDecalCallbacks->GetDepthTexture());

	// set the normal texture in global sampler
	grcEffect::SetGlobalVar(m_shaderVarIdNormalTexture, g_pDecalCallbacks->GetNormalTexture());

	
	// calc and set the one over screen size shader var
	const grcViewport *vp = grcViewport::GetCurrent();
	const float width = static_cast<float>(vp->GetWidth());
	const float height = static_cast<float>(vp->GetHeight());

	Vec4V vScreenSize = Vec4V( width, height, 1.0f/width, 1.0f/height);
	m_pShader->SetVar(m_shaderVarIdScreenSize, RCC_VECTOR4(vScreenSize));

	const Vector4 projParams = g_pDecalCallbacks->CalculateProjectionParams();
	m_pShader->SetVar(m_shaderVarIdProjParams, projParams);
}


void decalShader_Dynamic::SetVarsLocal(grcTexture* pTexture, grcTexture* pNormal, float specFalloff, float specIntensity, float specFresnel, float parallaxScale, float emissiveMultiplier, float polyRejectThreshold)
{				
	m_pShader->SetVar(m_shaderVarIdDecalTexture, pTexture);
	m_pShader->SetVar(m_shaderVarIdDecalNormal, pNormal);
	m_pShader->SetVar(m_shaderVarIdSpecular, specFalloff);
	m_pShader->SetVar(m_shaderVarIdSpecularColor, specIntensity);
	m_pShader->SetVar(m_shaderVarIdSpecularFresnel, specFresnel);
	m_pShader->SetVar(m_shaderVarIdParallaxScale, parallaxScale);
	m_pShader->SetVar(m_shaderVarIdEmissiveMultiplier, emissiveMultiplier);
	m_pShader->SetVar(m_shaderVarIdPolyRejectThreshold, polyRejectThreshold);
}


void decalShader_Dynamic::UnbindTextures() 
{
	const grcTexture* pNullTexture = NULL;
	m_pShader->SetVar(m_shaderVarIdDecalTexture, pNullTexture);
	m_pShader->SetVar(m_shaderVarIdDecalNormal, pNullTexture);
}


void decalShader_Dynamic::PushSamplerState(grcSamplerStateHandle samplerStateHandle)
{
	m_pShader->PushSamplerState(m_shaderVarIdDecalTexture, samplerStateHandle);
	m_pShader->PushSamplerState(m_shaderVarIdDecalNormal, samplerStateHandle);
}


void decalShader_Dynamic::PopSamplerState()
{
	m_pShader->PopSamplerState(m_shaderVarIdDecalTexture);
	m_pShader->PopSamplerState(m_shaderVarIdDecalNormal);
}


//////////////////////////////////////////////////////////////////////////
// Indexed Dynamic Decals 

#if DECAL_USE_INDEXED_VERTS

// we use 16-bit indices to draw dynamic decals; 
// however, to avoid the performance hit of writing to 
// write-combined (non-cached) memory in chunks of less than 4 bytes, 
// we pack 2 indices into a u32.
//
// the original layout of indices is commented out below for reference:
//
// {	0,3,2, 2,1,0, 
//		0,1,5, 5,4,0,
//		0,4,7, 7,3,0,
//		2,6,5, 5,1,2,
//		4,5,6, 6,7,4,
//		6,2,3, 3,7,6 
//						};
// CopyDynamicDecalIndexedData can be used to copy dynamic decals indices
// with no hassle
const u32 decalDrawInterface::ms_dynDecalBoxIndices[DECAL_NUM_DYNAMIC_INST_IDX_HALF] = {	3 | (0<<16),	2 | (2<<16), 0 | (1<<16),
																							1 | (0<<16),	5 | (5<<16), 0 | (4<<16),
																							4 | (0<<16),	7 | (7<<16), 0 | (3<<16),
																							6 | (2<<16),	5 | (5<<16), 2 | (1<<16),
																							5 | (4<<16),	6 | (6<<16), 4 | (7<<16),
																							2 | (6<<16),	3 | (3<<16), 6 | (7<<16),
																							};

u32* decalDrawInterface::ms_pDynDecalIndices = NULL;
dynDecalVtx* decalDrawInterface::ms_pDynDecalVerts = NULL;

grcVertexDeclaration* decalDrawInterface::ms_pDynDecalVtxDecl = NULL; 

const grcVertexElement decalDrawInterface::ms_dynDecalVtxElements[] =
{
	grcVertexElement(0, grcVertexElement::grcvetPosition, 0, 16,  grcFvf::grcdsFloat4 ), // pos[3];		
	grcVertexElement(0, grcVertexElement::grcvetNormal,   0, 16,  grcFvf::grcdsFloat4 ), //	normal[3];	
	grcVertexElement(0, grcVertexElement::grcvetTexture,  0, 8,  grcFvf::grcdsHalf4 ),	 //	uv0[4];	
	grcVertexElement(0, grcVertexElement::grcvetTexture,  1, 8,  grcFvf::grcdsHalf4 ),	 //	uv1[4];	
	grcVertexElement(0, grcVertexElement::grcvetTexture,  2, 8,  grcFvf::grcdsHalf4 ),	 //	uv2[4];	
	grcVertexElement(0, grcVertexElement::grcvetTexture,  3, 8,  grcFvf::grcdsHalf4 ),	 //	uv3[4];	
};

void decalDrawInterface::BeginDynamicDecals()
{
	if (ms_pDynDecalVtxDecl == NULL)
	{
		ms_pDynDecalVtxDecl = GRCDEVICE.CreateVertexDeclaration(ms_dynDecalVtxElements, NELEM(ms_dynDecalVtxElements));
		Assert(ms_pDynDecalVtxDecl != NULL);
	}

	GRCDEVICE.SetVertexDeclaration(ms_pDynDecalVtxDecl);

}

void decalDrawInterface::EndDynamicDecals()
{
	GRCDEVICE.SetVertexDeclaration(NULL);
}

void decalDrawInterface::BeginIndexedVertices( u32 vertexCount, u32 indexCount, u32 vertexSize )
{
	GRCDEVICE.ClearStreamSource(0);
	Assert(vertexCount * vertexSize <= GPU_VERTICES_MAX_SIZE);

	GRCDEVICE.GetCurrent()->BeginIndexedVertices(	D3DPT_TRIANGLELIST, 0, vertexCount, indexCount, 
													D3DFMT_INDEX16, vertexSize, 
													(void**)&decalDrawInterface::ms_pDynDecalIndices,
													(void**)&decalDrawInterface::ms_pDynDecalVerts);

	Assert(decalDrawInterface::ms_pDynDecalVerts != NULL);
	Assert(decalDrawInterface::ms_pDynDecalIndices != NULL);
}

void decalDrawInterface::EndIndexedVertices()
{
	GRCDEVICE.GetCurrent()->EndIndexedVertices();

	decalDrawInterface::ms_pDynDecalIndices = NULL;
	decalDrawInterface::ms_pDynDecalVerts = NULL;
}

void decalDrawInterface::CopyDynamicDecalIndexedData(const u32 numDynDecals)
{
	// we use 16-bit indices to draw dynamic decals; 
	// however, to avoid the performance hit of writing to 
	// write-combined memory in chunks of less than 4 bytes, 
	// we combine pairs of indices into u32 blocks.
	const u32 curOffset = (8*numDynDecals) | ((8*numDynDecals) << 16);

	for (u32 k =0; k<DECAL_NUM_DYNAMIC_INST_IDX_HALF;k++)
	{
		ms_pDynDecalIndices[k] = decalDrawInterface::ms_dynDecalBoxIndices[k]+curOffset;
	}

	ms_pDynDecalIndices += DECAL_NUM_DYNAMIC_INST_IDX_HALF;

}

void decalDrawInterface::CopyDynamicDecalVertices(Vec3V_Ptr pvVerts, Vec4V_In vPosition4, Vec4V_In vForward4, Vec4V_In vSide4, Vec4V_In vTex4, Vec4V_In vAlpha4)
{
	Vec4V vUV01, vUV23;
	vUV01 = Float16Vec8Pack( vForward4, vSide4 );
	vUV23 = Float16Vec8Pack( vTex4, vAlpha4 );

	for (u32 i = 0; i < DECAL_NUM_DYNAMIC_INST_VERTS; i++)
	{
		ms_pDynDecalVerts[i].pos = Vec4V(pvVerts[i]);
		ms_pDynDecalVerts[i].normal = vPosition4;
		ms_pDynDecalVerts[i].uv01 = vUV01;
		ms_pDynDecalVerts[i].uv23 = vUV23;
	}
	ms_pDynDecalVerts += DECAL_NUM_DYNAMIC_INST_VERTS;
}
#endif // DECAL_USE_INDEXED_VERTS

} // namespace rage

#endif // !__TOOL && !__RESOURCECOMPILER
