//
// vfx/decal/decalbounds.h
//
// Copyright (C) 2012-2012 Rockstar Games.  All Rights Reserved.
//


#ifndef VFX_DECAL_DECALBOUNDS_H
#define VFX_DECAL_DECALBOUNDS_H

#if !__TOOL && !__RESOURCECOMPILER

// includes (rage)
#include "phbound/bound.h"
#include "phbound/boundbvh.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundgeom.h"

// includes (framework)


// defines
#define DECAL_LARGEST_BOUNDS_TYPE     rage::phBoundBVH


// compile time asserts
CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) >= sizeof(rage::phBound));
CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) >= sizeof(rage::phBoundBVH));
CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) >= sizeof(rage::phBoundComposite));
CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) >= sizeof(rage::phBoundGeometry));
CompileTimeAssert(__alignof(DECAL_LARGEST_BOUNDS_TYPE) >= __alignof(rage::phBound));
CompileTimeAssert(__alignof(DECAL_LARGEST_BOUNDS_TYPE) >= __alignof(rage::phBoundBVH));
CompileTimeAssert(__alignof(DECAL_LARGEST_BOUNDS_TYPE) >= __alignof(rage::phBoundComposite));
CompileTimeAssert(__alignof(DECAL_LARGEST_BOUNDS_TYPE) >= __alignof(rage::phBoundGeometry));
CompileTimeAssert(__alignof(DECAL_LARGEST_BOUNDS_TYPE) >= 16);

// TODO: GTAV doesn't currently have a MEMORY_SAFE_OVERREAD_BYTES define
// CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) - sizeof(rage::phBound)          <= MEMORY_SAFE_OVERREAD_BYTES);
// CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) - sizeof(rage::phBoundBVH)       <= MEMORY_SAFE_OVERREAD_BYTES);
// CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) - sizeof(rage::phBoundComposite) <= MEMORY_SAFE_OVERREAD_BYTES);
// CompileTimeAssert(sizeof(DECAL_LARGEST_BOUNDS_TYPE) - sizeof(rage::phBoundGeometry)  <= MEMORY_SAFE_OVERREAD_BYTES);

#endif // !__TOOL && !__RESOURCECOMPILER

#endif // VFX_DECAL_DECALBOUNDS_H
