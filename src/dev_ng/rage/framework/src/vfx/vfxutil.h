//
// vfx/vfxutil.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef _VFX_UTIL_H
#define _VFX_UTIL_H


// includes (rage)
#include "vector/color32.h"
#include "vectormath/classes.h"

// includes (framework)
#include "fwtl/regdrefs.h"


// namespaces 
namespace rage {


// defines
#define VFX_MAX_BROKEN_FRAG_INFOS			(128)

#define OUTPUT_FX_MEM_BUCKET_USAGE			1
#if OUTPUT_FX_MEM_BUCKET_USAGE
#define FX_MEM_RESET()						vfxUtil::ResetFxBucketMemory()
#define FX_MEM_CURR(name)					vfxUtil::OutputFxBucketMemoryCurrent(name)
#define FX_MEM_TOTAL(name)					vfxUtil::OutputFxBucketMemoryTotal(name)
#else
#define FX_MEM_RESET()
#define FX_MEM_CURR(name)
#define FX_MEM_TOTAL(name)	
#endif


// structures
struct vfxBrokenFragInfo
{
	fwRegdRef<class fwEntity> pParentEntity;
	fwRegdRef<class fwEntity> pChildEntity;
};


// classes
class vfxUtil
{
	///////////////////////////////////
	// FUNCTIONS 
	///////////////////////////////////

	public: ///////////////////////////

		// memory 
#if OUTPUT_FX_MEM_BUCKET_USAGE
		static void OutputFxBucketMemoryCurrent(const char* idString);
		static void OutputFxBucketMemoryTotal(const char* idString);
		static void ResetFxBucketMemory() {for (s32 i=0; i<4; i++) ms_currFxMemory[i] = 0;}
#endif

		// texture dictionary
		static int InitTxd(const char* pTxdName);
		static int FindTxd(const char* pTxdName);
		static void ShutdownTxd(const char* pTxdName);

		// broken fragment info
		static void ReportBrokenFrag(fwEntity* pParentEntity, fwEntity* pChildEntity);
		static void ResetBrokenFrags() {ms_numBrokenFragInfos = 0;}
		static int GetNumBrokenFrags() {return ms_numBrokenFragInfos;}
		static vfxBrokenFragInfo& GetBrokenFragInfo(int id) {return ms_brokenFragInfos[id];}


	///////////////////////////////////
	// VARIABLES 
	///////////////////////////////////

	private: //////////////////////////

		// memory
#if OUTPUT_FX_MEM_BUCKET_USAGE
		static s32 ms_currFxMemory[4];
#endif

		// broken fragment info
		static int ms_numBrokenFragInfos;
		static vfxBrokenFragInfo ms_brokenFragInfos[VFX_MAX_BROKEN_FRAG_INFOS];

};

} // namespace rage

#endif // VFX_UTIL_H


