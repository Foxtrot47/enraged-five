// Retarded amount of code to "trivialy" support 128-bit integers.
// If you can think of a better way to do it, please change this.

#ifndef VFX_INT128_H
#define VFX_INT128_H

#include "math/float16.h"                   // GTAV hack, used to get RSG_SSE_VERSION.  Once integrated to RDR3 this include should be unnecissary.
#include "system/bit.h"
#include "vectormath/vectortypes.h"

namespace rage {


// Microsoft compilers warn about "multiple copy constructors specified" and
// "multiple assignment operators specified", due to having volatile and
// non-volatile const references for the argument.  But this is required,
// otherwise operator selection can be ambiguous.
#ifdef _MSC_VER
#	pragma warning(push)
#	pragma warning(disable:4521)
#	pragma warning(disable:4522)
#endif

// C++11 finally added support for explicit type conversion operators.
// Though the Microsoft compilers (XB1: July 2014 XDK, PC: 2012), do not support it yet.
#if __cplusplus >= 201103L
#    define EXPLICIT_CONVERSION_OPERATOR        explicit
#else
#    define EXPLICIT_CONVERSION_OPERATOR
#endif


////////////////////////////////////////////////////////////////////////////////
//  Int128Base
////////////////////////////////////////////////////////////////////////////////
// Base class containing sign agnostic functions.
class ALIGNAS(16) Int128Base
{
public:

#	if __BE
		u64     hi, lo;
#	else
		u64     lo, hi;
#	endif

	__forceinline EXPLICIT_CONVERSION_OPERATOR operator u8() const
	{
		return (u8)lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator s8() const
	{
		return (s8)lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator u16() const
	{
		return (u16)lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator s16() const
	{
		return (s16)lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator u32() const
	{
		return (u32)lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator s32() const
	{
		return (s32)lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator u64() const
	{
		return lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator s64() const
	{
		return lo;
	}
	__forceinline EXPLICIT_CONVERSION_OPERATOR operator bool() const
	{
		return hi || lo;
	}
	__forceinline bool operator!() const
	{
		return !hi && !lo;
	}
	__forceinline bool operator==(const Int128Base &rhs) const
	{
		return hi==rhs.hi && lo==rhs.lo;
	}
	__forceinline bool operator!=(const Int128Base &rhs) const
	{
		return hi!=rhs.hi || lo!=rhs.lo;
	}


protected:

	__forceinline Int128Base()
	{
	}
	__forceinline Int128Base &operator=(u64 rhs)
	{
		lo = rhs;
		hi = 0;
		return *this;
	}
	__forceinline Int128Base &operator=(s64 rhs)
	{
		lo = rhs;
		hi = rhs>>63;
		Assert(-((rhs>>63)&1) == ~((rhs>>63)&1)+1);     // assert that negation is two's complement
		Assert(rhs>>63 == -((rhs>>63)&1));              // assert that right shift is arithmetic
		return *this;
	}
	__forceinline Int128Base &operator=(u8 rhs)
	{
		return operator=((u64)rhs);
	}
	__forceinline Int128Base &operator=(s8 rhs)
	{
		return operator=((s64)rhs);
	}
	__forceinline Int128Base &operator=(u16 rhs)
	{
		return operator=((u64)rhs);
	}
	__forceinline Int128Base &operator=(s16 rhs)
	{
		return operator=((s64)rhs);
	}
	__forceinline Int128Base &operator=(u32 rhs)
	{
		return operator=((u64)rhs);
	}
	__forceinline Int128Base &operator=(s32 rhs)
	{
		return operator=((s64)rhs);
	}
	__forceinline /*implicit*/ Int128Base(u8  rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(s8  rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(u16 rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(s16 rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(u32 rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(s32 rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(u64 rhs)
	{
		operator=(rhs);
	}
	__forceinline /*implicit*/ Int128Base(s64 rhs)
	{
		operator=(rhs);
	}
	__forceinline Int128Base operator-() const
	{
		Int128Base ret;
		const s64 tlo = lo;
		ret.lo = -tlo;
		ret.hi = ~hi + !tlo;
		return ret;
	}
	__forceinline Int128Base operator~() const
	{
		Int128Base ret;
		ret.lo = ~lo;
		ret.hi = ~hi;
		return ret;
	}
	__forceinline Int128Base operator<<(unsigned shift) const
	{
		Int128Base ret;
		const u64 tlo = lo;
		const u64 thi = hi;
		ret.lo = (tlo<<shift) & -(shift<64);
		ret.hi = ((thi<<shift | tlo>>(64-shift)) & -(shift< 64))
			   | ((tlo<<(shift-64))              & -(shift>=64));
		return ret;
	}
	__forceinline Int128Base &operator<<=(unsigned shift)
	{
		return *this = operator<<(shift);
	}
	__forceinline Int128Base operator<<(signed shift) const
	{
		return operator<<((unsigned)shift);
	}
	__forceinline Int128Base &operator<<=(signed shift)
	{
		return operator<<=((unsigned)shift);
	}
	__forceinline Int128Base &operator++()
	{
		const u64 tlo = lo+1;
		lo = tlo;
		hi += !tlo;
		return *this;
	}
	__forceinline Int128Base &operator--()
	{
		const u64 tlo = lo;
		hi -= !tlo;
		lo = tlo-1;
		return *this;
	}
	__forceinline Int128Base operator++(int)
	{
		const Int128Base ret = *this;
		Int128Base update = ret;
		++update;
		*this = update;
		return ret;
	}
	__forceinline Int128Base operator--(int)
	{
		const Int128Base ret = *this;
		Int128Base update = ret;
		--update;
		*this = update;
		return ret;
	}
	__forceinline Int128Base operator+(const Int128Base &rhs) const
	{
		Int128Base ret;
		const u64 tlo = lo;
		ret.lo = tlo + rhs.lo;
		ret.hi = hi + rhs.hi + (ret.lo < tlo);
		return ret;
	}
	__forceinline Int128Base operator-(const Int128Base &rhs) const
	{
		return operator+(-rhs);
	}
	__forceinline Int128Base operator&(const Int128Base &rhs) const
	{
		Int128Base ret;
		ret.lo = lo & rhs.lo;
		ret.hi = hi & rhs.hi;
		return ret;
	}
	__forceinline Int128Base operator|(const Int128Base &rhs) const
	{
		Int128Base ret;
		ret.lo = lo | rhs.lo;
		ret.hi = hi | rhs.hi;
		return ret;
	}
	__forceinline Int128Base operator^(const Int128Base &rhs) const
	{
		Int128Base ret;
		ret.lo = lo ^ rhs.lo;
		ret.hi = hi ^ rhs.hi;
		return ret;
	}
	__forceinline Int128Base &operator+=(const Int128Base &rhs)
	{
		return *this = operator+(rhs);
	}
	__forceinline Int128Base &operator-=(const Int128Base &rhs)
	{
		return *this = operator-(rhs);
	}
	__forceinline Int128Base &operator&=(const Int128Base &rhs)
	{
		lo &= rhs.lo;
		hi &= rhs.hi;
		return *this;
	}
	__forceinline Int128Base &operator|=(const Int128Base &rhs)
	{
		lo |= rhs.lo;
		hi |= rhs.hi;
		return *this;
	}
	__forceinline Int128Base &operator^=(const Int128Base &rhs)
	{
		lo ^= rhs.lo;
		hi ^= rhs.hi;
		return *this;
	}
	void Load(const volatile Int128Base *src)
	{
#		if RSG_CPU_INTEL
#			if RSG_SSE_VERSION >= 40
				__m128i v = _mm_load_si128((__m128i*)src);
				lo = _mm_extract_epi64(v, 0);
				hi = _mm_extract_epi64(v, 1);
#			else
				_mm_store_si128((__m128i*)this, _mm_load_si128((__m128i*)src));
#			endif
#		else
#			error "not yet implemented for this platform"
#		endif
	}
	void Store(volatile Int128Base *dst) const
	{
#		if RSG_CPU_INTEL
#			if RSG_SSE_VERSION >= 40
				__m128i v = _mm_setzero_si128();
				v = _mm_insert_epi64(v, lo, 0);
				v = _mm_insert_epi64(v, hi, 1);
				_mm_store_si128((__m128i*)dst, v);
#			else
				_mm_store_si128((__m128i*)dst, _mm_load_si128((__m128i*)this));
#			endif
#		else
#			error "not yet implemented for this platform"
#		endif
	}
};


////////////////////////////////////////////////////////////////////////////////
//  Functions shared between signed and unsigned implementations, but that which
//  cannot be implemented in the base class.  While templating Int128Base on the
//  derived class does allow it to cast return values to the correct type, that
//  breaks inlining on XB1.
////////////////////////////////////////////////////////////////////////////////
#define INT128_COMMON_FUNCTIONS(TYPE)                                          \
	__forceinline TYPE()                                                       \
	{                                                                          \
	}                                                                          \
	__forceinline TYPE(const Int128Base &rhs)                                  \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline TYPE(const TYPE &rhs)                                        \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline TYPE(const volatile TYPE &rhs)                               \
	{                                                                          \
		Load(&rhs);                                                            \
	}                                                                          \
	__forceinline TYPE &operator=(const TYPE &rhs)                             \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(volatile const TYPE &rhs)                    \
	{                                                                          \
		Load(&rhs);                                                            \
		return *this;                                                          \
	}                                                                          \
	__forceinline volatile TYPE &operator=(const TYPE &rhs) volatile           \
	{                                                                          \
		rhs.Store(this);                                                       \
		return *this;                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(u8  rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(s8  rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(u16 rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(s16 rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(u32 rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(s32 rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(u64 rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline /*implicit*/ TYPE(s64 rhs)                                   \
		: Int128Base(rhs)                                                      \
	{                                                                          \
	}                                                                          \
	__forceinline TYPE &operator=(u8  rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(s8  rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(u16 rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(s16 rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(u32 rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(s32 rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(u64 rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE &operator=(s64 rhs)                                     \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator=(rhs));                 \
	}                                                                          \
	__forceinline TYPE operator-() const                                       \
	{                                                                          \
		return TYPE(Int128Base::operator-());                                  \
	}                                                                          \
	__forceinline TYPE operator~() const                                       \
	{                                                                          \
		return TYPE(Int128Base::operator~());                                  \
	}                                                                          \
	__forceinline TYPE operator<<(unsigned shift) const                        \
	{                                                                          \
		return TYPE(Int128Base::operator<<(shift));                            \
	}                                                                          \
	__forceinline TYPE &operator<<=(unsigned shift)                            \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator<<=(shift));             \
	}                                                                          \
	__forceinline TYPE operator<<(signed shift) const                          \
	{                                                                          \
		return TYPE(Int128Base::operator<<(shift));                            \
	}                                                                          \
	__forceinline TYPE &operator<<=(signed shift)                              \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator<<=(shift));             \
	}                                                                          \
	__forceinline TYPE &operator++()                                           \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator++());                   \
	}                                                                          \
	__forceinline TYPE &operator--()                                           \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator--());                   \
	}                                                                          \
	__forceinline TYPE operator++(int)                                         \
	{                                                                          \
		return TYPE(Int128Base::operator++(int()));                            \
	}                                                                          \
	__forceinline TYPE operator--(int)                                         \
	{                                                                          \
		return TYPE(Int128Base::operator--(int()));                            \
	}                                                                          \
	__forceinline TYPE operator+(const Int128Base &rhs) const                  \
	{                                                                          \
		return TYPE(Int128Base::operator+(rhs));                               \
	}                                                                          \
	__forceinline TYPE operator-(const Int128Base &rhs) const                  \
	{                                                                          \
		return operator+(-TYPE(rhs));                                          \
	}                                                                          \
	__forceinline TYPE operator&(const Int128Base &rhs) const                  \
	{                                                                          \
		return TYPE(Int128Base::operator&(rhs));                               \
	}                                                                          \
	__forceinline TYPE operator|(const Int128Base &rhs) const                  \
	{                                                                          \
		return TYPE(Int128Base::operator|(rhs));                               \
	}                                                                          \
	__forceinline TYPE operator^(const Int128Base &rhs) const                  \
	{                                                                          \
		return TYPE(Int128Base::operator^(rhs));                               \
	}                                                                          \
	__forceinline TYPE &operator+=(const Int128Base &rhs)                      \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator+=(rhs));                \
	}                                                                          \
	__forceinline TYPE &operator-=(const Int128Base &rhs)                      \
	{                                                                          \
		return static_cast<TYPE&>(operator-=(rhs));                            \
	}                                                                          \
	__forceinline TYPE &operator&=(const Int128Base &rhs)                      \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator&=(rhs));                \
	}                                                                          \
	__forceinline TYPE &operator|=(const Int128Base &rhs)                      \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator|=(rhs));                \
	}                                                                          \
	__forceinline TYPE &operator^=(const Int128Base &rhs)                      \
	{                                                                          \
		return static_cast<TYPE&>(Int128Base::operator^=(rhs));                \
	}                                                                          \
	__forceinline TYPE &operator>>=(unsigned shift)                            \
	{                                                                          \
		TYPE tmp;                                                              \
		tmp = operator>>(shift);                                               \
		lo = tmp.lo;                                                           \
		hi = tmp.hi;                                                           \
		return *this;                                                          \
	}                                                                          \
	__forceinline TYPE operator>>(signed shift) const                          \
	{                                                                          \
		return operator>>((unsigned)shift);                                    \
	}                                                                          \
	__forceinline TYPE &operator>>=(signed shift)                              \
	{                                                                          \
		return operator>>=((unsigned)shift);                                   \
	}                                                                          \
	__forceinline bool operator>(const TYPE &rhs) const                        \
	{                                                                          \
		return rhs.operator<(*this);                                           \
	}                                                                          \
	__forceinline bool operator>=(const TYPE &rhs) const                       \
	{                                                                          \
		return rhs.operator<=(*this);                                          \
	}


////////////////////////////////////////////////////////////////////////////////
//  UInt128
////////////////////////////////////////////////////////////////////////////////
// Two's complement 128-bit unsigned integer.
// u128 is already taken, and it is NOT a 128-bit unsigned integer :/
class UInt128: public Int128Base
{
public:

	__forceinline UInt128 operator>>(unsigned shift) const
	{
		const u64 tlo = lo;
		const u64 thi = hi;
		UInt128 ret;
		ret.hi = (thi>>shift) & -(shift<64);
		ret.lo = ((tlo>>shift | thi<<(64-shift)) & -(shift< 64))
			   | ((thi>>(shift-64))              & -(shift>=64));
		return ret;
	}
	__forceinline bool operator<(const UInt128 &rhs) const
	{
		const u64 thi = hi;
		const u64 rhi = rhs.hi;
		return thi<rhi || (thi==rhi && lo<rhs.lo);
	}
	__forceinline bool operator<=(const UInt128 &rhs) const
	{
		const u64 thi = hi;
		const u64 rhi = rhs.hi;
		return thi<rhi || (thi==rhi && lo<=rhs.lo);
	}

	INT128_COMMON_FUNCTIONS(UInt128)
};


// A SInt128 class could be added here if required.


#undef INT128_COMMON_FUNCTIONS
#undef EXPLICIT_CONVERSION_OPERATOR


#ifdef _MSC_VER
#	pragma warning(pop)
#endif


////////////////////////////////////////////////////////////////////////////////
//  Basic integer helper template specializations
////////////////////////////////////////////////////////////////////////////////
template<> struct UnsignedType<UInt128>{ typedef UInt128 T; };
template<> struct UIntBits<128>{ typedef UInt128 T; };


} // namespace rage


#endif // VFX_INT128_H
