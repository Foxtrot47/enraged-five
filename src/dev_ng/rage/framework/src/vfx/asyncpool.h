#ifndef VFX_ASYNCPOOL_H
#define VFX_ASYNCPOOL_H

#include "asyncslist.h"

#include "system/debugmemoryfill.h"
#include "system/memory.h"
#include "system/new.h"

#define ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE          SPU_ONLY(, AsyncPoolBase<PTRCONVERTER> *thisEa, u32 dmaTag)
#define ASYNCPOOL_SPU_COMMA_ARGS_SIGNATURE              SPU_ONLY(, AsyncPool                   *thisEa, u32 dmaTag)
#define ASYNCPOOL_SPU_COMMA_ARGS_PASS(THISEA, DMATAG)   SPU_ONLY(,                              THISEA,     DMATAG)

#if __SPU
#	define ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER
#else
#	define ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER  =PTRCONVERTER()
#endif


namespace rage {

////////////////////////////////////////////////////////////////////////////////
//  AsyncPoolBase
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER=SListDefaultPtrConverter>
class AsyncPoolBase : private AsyncSListBase<PTRCONVERTER>
{
public:

	inline SListItemBase<PTRCONVERTER> *Alloc(const PTRCONVERTER &conv ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE);
	inline void Free(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE);
	inline void Free(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE);
	inline unsigned GetNumItems() const { return AsyncSListBase<PTRCONVERTER>::GetNumItems(); }

#if !__SPU
	void Init(SListItemBase<PTRCONVERTER> *nodes, unsigned count, unsigned nodeSize, const PTRCONVERTER &conv=PTRCONVERTER());
	inline SListItemBase<PTRCONVERTER> *SyncAlloc(const PTRCONVERTER &conv=PTRCONVERTER()) { return SyncPop(conv); }
	inline void SyncFree(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER()) { SyncPush(first, last, count, conv); }
	inline void SyncFree(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv=PTRCONVERTER()) { SyncPush(n, conv); }
	inline const SListItemBase<PTRCONVERTER> *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER()) const { return AsyncSListBase<PTRCONVERTER>::SyncPeek(conv); }
	inline       SListItemBase<PTRCONVERTER> *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER())       { return AsyncSListBase<PTRCONVERTER>::SyncPeek(conv); }
	inline unsigned SyncCountNumItems(const PTRCONVERTER &conv=PTRCONVERTER()) const { return AsyncSListBase<PTRCONVERTER>::SyncCountNumItems(conv); }
#endif // !__SPU
};


////////////////////////////////////////////////////////////////////////////////
//  AsyncPoolBase::Alloc
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline SListItemBase<PTRCONVERTER> *AsyncPoolBase<PTRCONVERTER>::Alloc(const PTRCONVERTER &conv ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE)
{
	return AsyncSListBase<PTRCONVERTER>::Pop(conv ASYNCPOOL_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncPoolBase::Free
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void AsyncPoolBase<PTRCONVERTER>::Free(SListItemBase<PTRCONVERTER> *first, SListItemBase<PTRCONVERTER> *last, unsigned count, const PTRCONVERTER &conv ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE)
{
	AsyncSListBase<PTRCONVERTER>::Push(first, last, count, conv ASYNCPOOL_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncPoolBase::Free
////////////////////////////////////////////////////////////////////////////////
template<class PTRCONVERTER>
inline void AsyncPoolBase<PTRCONVERTER>::Free(SListItemBase<PTRCONVERTER> *n, const PTRCONVERTER &conv ASYNCPOOLBASE_SPU_COMMA_ARGS_SIGNATURE)
{
	Free(n, n, 1, conv ASYNCPOOL_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
}


#if !__SPU

	////////////////////////////////////////////////////////////////////////////
	//  AsyncPoolBase::Init
	////////////////////////////////////////////////////////////////////////////
	template<class PTRCONVERTER>
	void AsyncPoolBase<PTRCONVERTER>::Init(SListItemBase<PTRCONVERTER> *nodes, unsigned count, unsigned nodeSize, const PTRCONVERTER &conv)
	{
		IF_DEBUG_MEMORY_FILL_N(memset(nodes, 0xcc, count*nodeSize), DMF_ASYNCPOOL);

		SListItemBase<PTRCONVERTER> *curr=nodes;
		SListItemBase<PTRCONVERTER> *last=(SListItemBase<PTRCONVERTER>*)((uptr)curr+(count-1)*nodeSize);
		while(curr<last)
		{
			SListItemBase<PTRCONVERTER> *next=(SListItemBase<PTRCONVERTER>*)((uptr)curr+nodeSize);
			curr->SetNext(next, conv);
			curr=next;
		}
		curr->SetNext(NULL, conv);
		typename PTRCONVERTER::STORAGE h;
		sysMemCpy(&h, (const void*)&AsyncSListBase<PTRCONVERTER>::data.head, sizeof(h));
		h = conv.FromPtr(nodes, h);
		sysMemCpy((void*)&AsyncSListBase<PTRCONVERTER>::data.head, &h, sizeof(h));
		AsyncSListBase<PTRCONVERTER>::data.size = (typename AsyncSListBase<PTRCONVERTER>::SIZETYPE)count;
	}

#endif // !__SPU


////////////////////////////////////////////////////////////////////////////////
//  AsyncPool
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER=SListDefaultPtrConverter>
class AsyncPool : public AsyncPoolBase<PTRCONVERTER>
{
public:

	inline USERTYPE *Alloc(const PTRCONVERTER &conv ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER ASYNCPOOL_SPU_COMMA_ARGS_SIGNATURE);
	inline void Free(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER ASYNCPOOL_SPU_COMMA_ARGS_SIGNATURE);
	inline void Free(USERTYPE *n, const PTRCONVERTER &conv ASYNCPOOL_NON_SPU_DEFAULT_PTRCONVERTER ASYNCPOOL_SPU_COMMA_ARGS_SIGNATURE);

#if !__SPU
	inline void Init(USERTYPE *nodes, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER()) { AsyncPoolBase<PTRCONVERTER>::Init(nodes, count, sizeof(USERTYPE), conv); }
	inline USERTYPE *InitAlloc(unsigned count);
	inline USERTYPE *SyncAlloc(const PTRCONVERTER &conv=PTRCONVERTER()) { return static_cast<USERTYPE*>(AsyncPoolBase<PTRCONVERTER>::SyncAlloc(conv)); }
	inline void SyncFree(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv=PTRCONVERTER()) { AsyncPoolBase<PTRCONVERTER>::SyncFree(first, last, count, conv); }
	inline void SyncFree(USERTYPE *n, const PTRCONVERTER &conv=PTRCONVERTER()) { AsyncPoolBase<PTRCONVERTER>::SyncFree(n, conv); }
	inline const USERTYPE *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER()) const { return static_cast<const USERTYPE*>(AsyncPoolBase<PTRCONVERTER>::SyncPeek(conv)); }
	inline       USERTYPE *SyncPeek(const PTRCONVERTER &conv=PTRCONVERTER())       { return static_cast<      USERTYPE*>(AsyncPoolBase<PTRCONVERTER>::SyncPeek(conv)); }
#endif // !__SPU
};


////////////////////////////////////////////////////////////////////////////////
//  AsyncPool::Alloc
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER>
inline USERTYPE *AsyncPool<USERTYPE, PTRCONVERTER>::Alloc(const PTRCONVERTER &conv ASYNCPOOL_SPU_COMMA_ARGS_SIGNATURE)
{
	USERTYPE *const ptr = static_cast<USERTYPE*>(AsyncPoolBase<PTRCONVERTER>::Alloc(conv ASYNCPOOL_SPU_COMMA_ARGS_PASS(thisEa, dmaTag)));
#if !__SPU
	// SPU code cannot call ctor since ptr is an effective address
	if(!ptr) return NULL;
	IF_DEBUG_MEMORY_FILL_N(memset(ptr, 0xcd, sizeof(USERTYPE)),DMF_ASYNCPOOL);
	rage_placement_new(ptr) USERTYPE;
#endif
	return ptr;
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncPool::Free
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER>
inline void AsyncPool<USERTYPE, PTRCONVERTER>::Free(USERTYPE *first, USERTYPE *last, unsigned count, const PTRCONVERTER &conv ASYNCPOOL_SPU_COMMA_ARGS_SIGNATURE)
{
	if(count)
	{
#if !__SPU
		// SPU code cannot call dtors since first and last are effective addresses
		USERTYPE *next = first;
		USERTYPE *ptr;
		do
		{
			ptr = next;
			next = ptr->GetNext(conv);
			ptr->~USERTYPE();
			IF_DEBUG_MEMORY_FILL_N(memset(ptr, 0xdd, sizeof(USERTYPE)),DMF_ASYNCPOOL);
			ptr->SetNext(next, conv);
		}
		while(ptr != last);
#endif
		AsyncPoolBase<PTRCONVERTER>::Free(first, last, count, conv ASYNCPOOL_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
	}
}


////////////////////////////////////////////////////////////////////////////////
//  AsyncPool::Free
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class PTRCONVERTER>
inline void AsyncPool<USERTYPE, PTRCONVERTER>::Free(USERTYPE *n, const PTRCONVERTER &conv SPU_ONLY(, AsyncPool *thisEa, u32 dmaTag))
{
	if(n)
	{
#if !__SPU
		// SPU code cannot call dtor since n is an effective address
		n->~USERTYPE();
		IF_DEBUG_MEMORY_FILL_N(memset(n, 0xdd, sizeof(USERTYPE)),DMF_ASYNCPOOL);
#endif
		AsyncPoolBase<PTRCONVERTER>::Free(n, conv ASYNCPOOL_SPU_COMMA_ARGS_PASS(thisEa, dmaTag));
	}
}


#if !__SPU

	////////////////////////////////////////////////////////////////////////////
	//  AsyncPool::InitAlloc
	////////////////////////////////////////////////////////////////////////////
	template<class USERTYPE, class PTRCONVERTER>
	inline USERTYPE *AsyncPool<USERTYPE, PTRCONVERTER>::InitAlloc(unsigned count)
	{
		USERTYPE *const nodes = static_cast<USERTYPE*>(sysMemAllocator::GetCurrent().RAGE_LOG_ALLOCATE(count*sizeof(USERTYPE), __alignof(USERTYPE)));
		Assert(nodes);
		Init(nodes, count, PTRCONVERTER(nodes));
		return nodes;
	}

#endif // !__SPU


////////////////////////////////////////////////////////////////////////////////
//  AsyncPoolAligned
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, unsigned ALIGN>
class AsyncPoolAligned : public AsyncPool<USERTYPE, SListDefaultAlignPtrConverter<ALIGN> >
{
};


////////////////////////////////////////////////////////////////////////////////
//  AsyncPoolIndexed
////////////////////////////////////////////////////////////////////////////////
template<class USERTYPE, class STORAGE>
class AsyncPoolIndexed : public AsyncPool<USERTYPE, SListIdxPtrConverter<USERTYPE, STORAGE> >
{
};

} // namespace rage

#endif // VFX_ASYNCPOOL_H
