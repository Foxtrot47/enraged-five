//
// vfx/channel.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

// includes (us)
#include "vfx/channel.h"

// includes (rage)
#include "bank/bkmgr.h"
#include "diag/channel.h"
#include "system/param.h"

namespace rage
{

// defines
RAGE_DEFINE_CHANNEL(vfx)
RAGE_DEFINE_SUBCHANNEL(vfx, ptfx)
RAGE_DEFINE_SUBCHANNEL(vfx, decal)
RAGE_DEFINE_CHANNEL(timecycle)


// params
PARAM(vfx_tty_windows, "Create RAG windows for vfx related TTY");
PARAM(timecycle_tty_windows, "Create RAG windows for timecycle related TTY");

PARAM(nodecals, "disables decals");
PARAM(noptfx, "disables particle effects");
PARAM(novfx, "disables all visual effects");


#if __BANK
void vfxChannel::Init()
{
	if (PARAM_vfx_tty_windows.Get())
	{
		BANKMGR.CreateOutputWindow("vfx", "NamedColor:Green"); 
		BANKMGR.CreateOutputWindow("vfx_ptfx", "NamedColor:Green"); 
		BANKMGR.CreateOutputWindow("vfx_decal", "NamedColor:Green"); 
	}

	if (PARAM_timecycle_tty_windows.Get())
	{
		BANKMGR.CreateOutputWindow("timecycle", "NamedColor:Green"); 
	}
};
#endif

} // namespace rage