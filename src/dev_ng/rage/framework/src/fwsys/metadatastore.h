//
// fwsys/metadatastore.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef _FWSYS_METADATASTORE_H_
#define _FWSYS_METADATASTORE_H_

#include "fwtl/assetstore.h"
#include "fwscene/stores/psostore.h"
#include "fwscene/stores/boxstreamer.h"

namespace rage {

class parStructure;

class fwMetaDataFile 
{
public:
	fwMetaDataFile() {}
	~fwMetaDataFile() { Assertf(!m_fileData.IsLoaded(), "Call Unload before deleting this"); }

	fwPsoStoreLoadInstance m_fileData;
	parStructure* m_structure;
	
	template <class T> T* GetObject() const {return reinterpret_cast<T*>(m_fileData.GetInstance());}
	template <class T> T* GetObject() {return reinterpret_cast<T*>(m_fileData.GetInstance());}
};

class fwMetaDataStore : public fwAssetStore<fwMetaDataFile>
{
public:
	fwMetaDataStore();

	static void Init(u32 initMode);
	virtual void Shutdown();

	void Update();

	virtual bool Load(strLocalIndex index, void* pData, int size);
	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void Remove(strLocalIndex index);
	virtual bool RequiresTempMemory(strLocalIndex index) const;
	virtual void* GetDataPtr(strLocalIndex index);

	virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);
	virtual void SetResource(strLocalIndex index, datResourceMap& map);

	virtual bool CanPlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex)) const { return true; }
	virtual void PlaceAsynchronously(strLocalIndex objIndex, strStreamingLoader::StreamingFile& file, datResourceInfo& rsc);

	fwBoxStreamer& GetBoxStreamer() { return m_boxStreamer; }

	virtual strLocalIndex AddSlot(const fwAssetDef<fwMetaDataFile>::strObjectNameType name);
	virtual strLocalIndex AddSlot(strLocalIndex index, const fwAssetDef<fwMetaDataFile>::strObjectNameType name);
	int MakeSlotStreamable(const fwAssetDef<fwMetaDataFile>::strObjectNameType name, spdAABB& aabb);
	virtual void RemoveSlot(strLocalIndex index);
	void RemoveUnrequired();
	void RemoveLoaded(strLocalIndex slotIndex);
	bool SafeRemove(strLocalIndex slotIndex);
	void RemoveAll(bool bIgnoreRefCount = false);

#if __BANK
	void SetAllowLoadInResourceMem(bool b) { m_AllowLoadInResourceMem = b; }
#endif

	static void ShutdownSystem(unsigned shutdownMode);

#if __BANK
	void InitWidgets(bkBank& bank);
#endif

private:
	void ConfigureLoaderForSlot(fwPsoStoreLoader& loader, strLocalIndex index) const;
	bool ShouldLoadInResourceMem(strLocalIndex index) const;

	fwPsoStoreLoader m_loader;
	fwBoxStreamer	m_boxStreamer;
#if __BANK
	bool			m_AllowLoadInResourceMem;
#endif
};

extern fwMetaDataStore g_fwMetaDataStore;

} // namespace rage

#endif // _FWSYS_METADATASTORE_H_
