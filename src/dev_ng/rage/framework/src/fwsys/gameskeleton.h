//
// gameskeleton.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef GAME_SKELETON_H
#define GAME_SKELETON_H

#include "atl/array.h"
#include "profile/timebars.h"
#include "profile/rocky.h"
#include "script/thread.h"
#include "system/memory.h"

namespace rage
{
	const unsigned INIT_CORE                	= BIT(0);
	const unsigned SHUTDOWN_CORE            	= BIT(0);
	const unsigned INIT_BEFORE_MAP_LOADED   	= BIT(1);
	const unsigned INIT_AFTER_MAP_LOADED    	= BIT(2);
	const unsigned SHUTDOWN_WITH_MAP_LOADED 	= BIT(2);
	const unsigned INIT_SESSION            	    = BIT(3);
	const unsigned SHUTDOWN_SESSION        	    = BIT(3);

	const unsigned COMMON_MAIN_UPDATE       	= BIT(0);
	const unsigned FULL_MAIN_UPDATE         	= BIT(1);
	const unsigned SIMPLE_MAIN_UPDATE       	= BIT(2);
	
#if COMMERCE_CONTAINER
	const unsigned COMMERCE_MAIN_UPDATE       	= BIT(3);
#endif

	const unsigned LANDING_PAGE_PRE_INIT_UPDATE = BIT(4);
	const unsigned LANDING_PAGE_UPDATE          = BIT(5);

    //PURPOSE
    // The game skeleton class provides an application with a basic framework for a game's management of
    // initialising, shutting down and updating of the various systems the game consists of. It provides
    // a way of specifying and testing dependencies between systems at init/shutdown time, captures timing
    // information.
    class gameSkeleton
    {
    public:

        typedef void (*fnInitFunction) (unsigned);
        typedef void (*fnShutdownFunction) (unsigned);
        typedef void (*fnUpdateFunction) ();

        gameSkeleton();
        virtual ~gameSkeleton();

        void SetCurrentInitType       (unsigned initType)        { m_CurrentInitType        = initType; }
        void SetCurrentShutdownType   (unsigned shutdownType)    { m_CurrentShutdownType    = shutdownType; }
        void SetCurrentUpdateType     (unsigned updateType);
        void SetCurrentDependencyLevel(unsigned dependencyLevel) { m_CurrentDependencyLevel = dependencyLevel; }

#if __DEV
        void SetTestDependencies(bool test) { m_RunDependenciesTest = test; }
#endif // __DEV

        //PURPOSE
        // Registers an initialisation function with the game skeleton. This function will be registered
        // at the current init type (previously set by a call to SetCurrentInitType), and the current dependency
        // level (previous set by a call to SetCurrentDependencyLevel)
        //PARAMS
        // initFunction - The initialisation function
        // systemName   - Description of the initialisation function
        void RegisterInitSystem(fnInitFunction initFunction,
                                atHashString systemName
								PROFILER_ONLY(,Profiler::EventDescription* desc));

        //PURPOSE
        // Registers a shutdown function with the game skeleton. This function will be registered
        // at the current shutdown type (previously set by a call to SetCurrentShutdownType), and the current dependency
        // level (previous set by a call to SetCurrentDependencyLevel)
        //PARAMS
        // shutdownFunction - The shutdown function
        // systemName       - Description of the shutdown function
        void RegisterShutdownSystem(fnShutdownFunction shutdownFunction,
                                    const atHashString& systemName);

        //PURPOSE
        // Registers an update function with the game skeleton. This function will be registered
        // at the current update type (previously set by a call to SetCurrentShutdownType). Dependency levels
        // are not used for update functions.
        //PARAMS
        // updateFunction - The update function
        // systemName     - Description of the update function
        // addTimebar     - Indicates whether a timebar should be associated with this update system
        // timebarBudget  - Indicates whether the system has a budget for how long it is allowed to take
        void RegisterUpdateSystem(fnUpdateFunction updateFunction,
                                  atHashString systemName,
								  PROFILER_ONLY(Profiler::EventDescription* profilerDescription,)
                                  bool             addTimebar = false,
                                  float            timebarBudget = 0.0f);

        //PURPOSE
        // Pushes a new update group on to the update group stack. This allows a collection of
        // update systems to be timed collectively. Groups can be nested.
        //PARAMS
        // groupName     - Name of the update group
        // addTimebar    - Indicates whether a timebar should be associated with this update group
        // timebarBudget - Indicates whether the group has a budget for how long it is allowed to take
        void PushUpdateGroup(atHashString groupName,
							 PROFILER_ONLY(Profiler::EventDescription* profilerDescription,)
                             bool        addTimebar = false,
                             float       timebarBudget = 0.0f);

        //PURPOSE
        // Pops an update group from the update group stack.
        void PopUpdateGroup();

        //PURPOSE
        // Calls the initialisation functions registered with the specified initialisation mode.
        // These functions will be called in the order they were registered, grouped by the dependency
        // levels they were registered at. Dependency levels are processed lowest to highest.
        //PARAMS
        // initMode - The initialisation mode
        virtual void Init(unsigned initMode);

        //PURPOSE
        // Calls the shutdown functions registered with the specified shutdown mode.
        // These functions will be called in the order they were registered, grouped by the dependency
        // levels they were registered at. Dependency levels are processed lowest to highest.
        //PARAMS
        // shutdownMode - The shutdown mode
        virtual void Shutdown(unsigned shutdownMode);

        //PURPOSE
        // Calls the update functions registered with the specified update mode.
        //PARAMS
        // updateMode - The update mode
        virtual void Update(unsigned updateMode);

    private:

        static const unsigned DEFAULT_MAX_SYSTEMS_REGISTERED = 128;
        static const unsigned MAX_DEPENDENCY_LEVELS          = 32;
        static const unsigned MAX_NESTED_UPDATE_GROUPS       = 32;

        //PURPOSE
        // The system data structure encapsulates data about initialisation and shutdown functions
        // related to a system registered with the game skeleton. A system may be registered to different
        // types of initialisation/shutdown
        struct systemData
        {
            systemData() {}

            systemData(fnInitFunction      initFunction,
                       fnShutdownFunction  shutdownFunction,
                       unsigned            initDependencyLevel,
                       unsigned            shutdownDependencyLevel,
                       unsigned            initTypes,
                       unsigned            shutdownTypes,
                       const atHashString& systemName) :
            m_InitFunction(initFunction)
            ,m_ShutdownFunction(shutdownFunction)
            ,m_InitDependencyLevel(initDependencyLevel)
            ,m_ShutdownDependencyLevel(shutdownDependencyLevel)
            ,m_InitTypes(initTypes)
            ,m_ShutdownTypes(shutdownTypes)
            ,m_SystemName(systemName)
			PROFILER_ONLY(, m_EventDescription(NULL))
            {
            }

            systemData(const systemData &sysData) :
            m_InitFunction(sysData.m_InitFunction)
            ,m_ShutdownFunction(sysData.m_ShutdownFunction)
            ,m_InitDependencyLevel(sysData.m_InitDependencyLevel)
            ,m_ShutdownDependencyLevel(sysData.m_ShutdownDependencyLevel)
            ,m_InitTypes(sysData.m_InitTypes)
            ,m_ShutdownTypes(sysData.m_ShutdownTypes)
            ,m_SystemName(sysData.m_SystemName)
			PROFILER_ONLY(, m_EventDescription(NULL))
            {
            }

            bool InitialisesThisMode(unsigned initMode) const { return ((m_InitTypes & initMode)!=0); }
            bool ShutdownsThisMode(unsigned shutdownMode) const { return ((m_ShutdownTypes & shutdownMode)!=0); }

            fnInitFunction     m_InitFunction;
            fnShutdownFunction m_ShutdownFunction;
            unsigned           m_InitDependencyLevel;
            unsigned           m_ShutdownDependencyLevel;
            unsigned           m_InitTypes;
            unsigned           m_ShutdownTypes;
			atHashString       m_SystemName;
#if USE_PROFILER
			const Profiler::EventDescription* m_EventDescription;
#endif
        };

        //PURPOSE
        // This structure contains all of the data for init/shutdown calls
        // at a specific dependency level. These structures are stored in a linked list.
        struct dependency
        {
            dependency(unsigned level) :
            dependencyLevel(level)
            , next(0)
            {
            }

            unsigned              dependencyLevel;
            atArray<unsigned>     sysData;
            dependency           *next;
        };

        //PURPOSE
        // This structure contains all of the data for an init/shutdown mode.
        // The different modes are stored in a linked list, and the mode contains
        // a pointer to a linked list of structures containing data for the different
        // dependency level of the mode
        struct mode
        {
            mode(unsigned type) :
            modeType(type)
            , head(0)
            , next(0)
            {
            }

            unsigned    modeType;
            dependency *head;
            mode       *next;
        };

        // PURPOSE
        // This structure is a base class for the different types of update
        // elements used by the system.
        struct updateBase
        {
            updateBase(const atHashString& name,
                       bool        addTimebar,
                       float       timebarBudget) :
            m_AddTimebar(addTimebar)
            , m_TimebarBudget(timebarBudget)
            , m_Name(name)
            , m_Next(0)
			#if USE_PROFILER
			, m_ProfilerDescription(nullptr)
			#endif
            {
            }

            virtual ~updateBase() {}

            virtual void Update() = 0;

            bool             m_AddTimebar;
            float            m_TimebarBudget;
            atHashString     m_Name;
            updateBase      *m_Next;

			#if USE_PROFILER
			Profiler::EventDescription* m_ProfilerDescription;
			#endif
        };

        //PURPOSE
        // This class wraps an update function related to an
        // update mode
        struct updateElement : public updateBase
        {
            updateElement() :
            updateBase(atHashString(), false, 0.0f)
            {
            }

            updateElement(const atHashString& name,
                          bool             addTimebar,
                          float            timebarBudget,
                          fnUpdateFunction updateFunction) :
            updateBase(name, addTimebar, timebarBudget)
            , m_UpdateFunction(updateFunction)
            {
            }

            void Update()
            {
				PROFILER_CUSTOM_EVENT(m_ProfilerDescription);

                if(m_AddTimebar)
                {
                    if(m_TimebarBudget > 0)
                    {
                        PF_PUSH_TIMEBAR_BUDGETED(m_Name.GetCStr(), m_TimebarBudget);
                    }
                    else
                    {
                        PF_PUSH_TIMEBAR_DETAIL(m_Name.GetCStr());
                    }
                }
                (*m_UpdateFunction)();
#if !__FINAL
				scrThread::CheckIfMonitoredScriptArraySizeHasChanged(m_Name.GetCStr(), 0);
#endif	//	!__FINAL
				if(m_AddTimebar)
				{
					if(m_TimebarBudget > 0)
					{
						PF_POP_TIMEBAR();
					}
					else
					{
						PF_POP_TIMEBAR_DETAIL();
					}
				}
			}

            fnUpdateFunction m_UpdateFunction;
        };

        //PURPOSE
        // This class contains a group of update elements,
        // which may include another group of elements, related
        // to an update mode
        struct updateGroup : public updateBase
        {
            updateGroup(const atHashString&name,
                        bool        addTimebar,
                        float       timebarBudget) :
            updateBase(name, addTimebar, timebarBudget)
            , m_Head(0)
            {
            }

            ~updateGroup()
            {
                updateBase *element = m_Head;

                while(element)
                {
                    updateBase *next = element->m_Next;
                    delete element;
                    element = next;
                }
            }

            void Update()
            {
				PROFILER_CUSTOM_EVENT(m_ProfilerDescription);

                if(m_AddTimebar)
                {
                    if(m_TimebarBudget > 0)
                    {
                        PF_PUSH_TIMEBAR_BUDGETED(m_Name.GetCStr(), m_TimebarBudget);
                    }
                    else
                    {
                        PF_PUSH_TIMEBAR(m_Name.GetCStr());
                    }
                }

                updateBase *element = m_Head;

                while(element)
                {
                    element->Update();
                    element = element->m_Next;
                }

                if(m_AddTimebar)
                {
                    PF_POP_TIMEBAR();
                }
            }

            updateBase *m_Head;
        };

        //PURPOSE
        // This class contains the data relating to an individual update mode.
        // Different update modes can be used to let different systems be updated
        // depending on the game state, as an example there may be an update mode for
        // the main game, one for being on the frontend/pause screen.
        // The different update modes are stored in a linked list, and contain a pointer
        // to a linked list of update elements associated with the update mode.
        struct updateMode
        {
            updateMode(unsigned type) :
            m_ModeType(type)
            , m_Head(0)
            , m_Next(0)
            {
            }

            ~updateMode()
            {
                updateBase *element = m_Head;

                while(element)
                {
                    updateBase *next = element->m_Next;
                    delete element;
                    element = next;
                }
            }

            void Update()
            {
                updateBase *element = m_Head;

                while(element)
                {
                    element->Update();
                    element = element->m_Next;
                }
            }

            unsigned    m_ModeType;
            updateBase *m_Head;
            updateMode *m_Next;
        };

        //PURPOSE
        // Registers the specified system data with the specified mode type. The system data will be
        // registered at the dependency level last specified by a call to SetCurrentDependencyLevel()
        //PARAMS
        // modeRoot      - The root node of the mode to add the system data to (init or shutdown root)
        // modeType      - The mode type to add the system data to
        // sysDataIndex  - Index of the registered system data to associate with the mode
        void AddSysDataForMode(mode *&modeRoot, unsigned modeType, unsigned sysDataIndex);

        //PURPOSE
        // Returns a mode from the specified mode root, with the specified type, if it exists
        //PARAMS
        // modeRoot - The root node of the type of mode to get the mode from (init or shutdown root)
        // modeType - The mode type to retrieve
        const mode *GetMode(const mode *modeRoot, unsigned modeType) const;

        //PURPOSE
        // Releases any allocated resources used by the specified mode linked list
        //PARAMS
        // modeRoot - The root node of the type of mode to cleanup
        void CleanupModeFromRoot(mode *&modeRoot);

        //PURPOSE
        // Releases any allocated resources used by the different update modes registered
        // with the system
        void CleanupUpdateModes();

        //PURPOSE
        // Adds an update element to the linked list for the specified update mode
        //PARAMS
        // updateType - The update mode type to add the element to
        // element    - Element to add
        void AddElementForUpdateMode(unsigned updateType, updateBase *element);

        //PURPOSE
        // Returns the system data with the specified name, if it has already been registered
        // with the system
        //PARAMS
        // name  - Name of the system data to retrieve
        // index - The index of the system data in the registered array
        systemData *GetSystemDataByName(const atHashString& name, unsigned &index);

        unsigned m_CurrentDependencyLevel;          // The current dependency level, used when building an init/shutdown mode
        unsigned m_CurrentInitType;                 // The current init mode type, used when building an init mode
        unsigned m_CurrentShutdownType;             // The current shutdown mode type, used when building an shutdown mode
        unsigned m_CurrentUpdateType;               // The current update mode type, used when building an update mode

        atArray<systemData> m_RegisteredSystemData; // Array of system data, used as storage for data that can be shared between init/shutdown modes

        unsigned m_UpdateGroupStackPointer;                        // The update group stack pointer, used when building an update mode
        updateGroup *m_UpdateGroupStack[MAX_NESTED_UPDATE_GROUPS]; // The update group stack, used when building an update mode

        mode       *m_InitModes;                    // Linked list of different registered initialisation modes
        mode       *m_ShutdownModes;                // Linked list of different registered shutdown modes
        updateMode *m_UpdateModes;                  // Linked list of different registered update modes

#if __DEV
        bool        m_RunDependenciesTest;          // Debug flag to initialise all systems at the same dependency level in reverse order
#endif // __DEV
    };
} // namespace rage

#endif  // GAME_SKELETON_H
