//
// fwsys/config.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "config.h"

#include "atl/map.h"
#include "diag/errorcodes.h"
#include "math/amath.h"
#include "parser/manager.h"
#include "system/nelem.h"

using namespace rage;

#include "config_parser.h"

bool fwConfigManager::sm_UseLargerPoolFromCodeOrData = false;
fwConfigManager* fwConfigManager::sm_Instance = NULL;

void fwConfigManager::ShutdownClass()
{
	// ShutdownClass() should be matched with InitClass() calls, so if this
	// fails, something may have gone wrong.
	Assert(sm_Instance);

	delete sm_Instance;
	sm_Instance = NULL;
}


int fwConfigManager::GetSizeOfPool(u32 poolNameHash, int defaultSize) const
{
	// Look up the pool using the name hash, and return the override
	// if it's present in the map.
	const int* pNewPoolSize = m_PoolSizeMap.Access(poolNameHash);
	if(pNewPoolSize && *pNewPoolSize != kLockedPoolSize)
	{
		if(sm_UseLargerPoolFromCodeOrData)
		{
			return Max(defaultSize, *pNewPoolSize);
		}
		else
		{
			Assertf(*pNewPoolSize >= defaultSize, "Pool has default size %d greater than configured size %d.", defaultSize, *pNewPoolSize);
			return* pNewPoolSize;
		}
	}

	// Otherwise, return the default size.
	return defaultSize;
}


void fwConfigManager::LockPoolSize(const atHashString& poolNameHash)
{
	int* poolSize = m_PoolSizeMap.Access(poolNameHash);
	if(poolSize && *poolSize != kLockedPoolSize)
	{
#if __ASSERT
		Assertf(0, "Pool \"%s\" doesn't support resizing through the configuration file, but is listed there (size %d).", poolNameHash.GetCStr(), *poolSize);
#elif !__FINAL // atHashString doesn't have GetCStr() in final
		Errorf("Pool \"%s\" doesn't support resizing through the configuration file, but is listed there (size %d).", poolNameHash.GetCStr(), *poolSize);
#endif
		*poolSize = kLockedPoolSize;
	}
	else
	{
		m_PoolSizeMap.Insert(poolNameHash, kLockedPoolSize);
	}
}


void fwConfigManager::Load(const char* pConfigFileName)
{
	// Delete any previous configuration object, and create a new one.
	delete m_Config;
	m_Config = CreateConfigData();

	// Try to load the configuration file.
	fwAllConfigs* allConfigs = NULL;
	if(PARSER.LoadObjectPtr(pConfigFileName, "", allConfigs))
	{
		if (!allConfigs)
		{
			Quitf(ERR_GAMECONFIG_1, "The game config file could not be loaded: %s", pConfigFileName);
		}

		// Loop over all configurations in the file.
		for(int j = 0; j < allConfigs->m_ConfigArray.GetCount(); j++)
		{
			// Check to see if the filter matches the current platform and build.
			if(!MatchFilter(allConfigs->m_ConfigArray[j]))
			{
				continue;
			}

			// Merge in any overrides into the main m_Config object.
			const fwConfig &d = *allConfigs->m_ConfigArray[j].m_Config;
			m_Config->MergeNonPoolData(d);

			// Loop over the pool size array and update the map.
			for(int i = 0; i < d.m_PoolSizes.m_Entries.GetCount(); i++)
			{
				const fwPoolSizeEntry &e = d.m_PoolSizes.m_Entries[i];
				const u32 poolNameHash = atStringHash((const char*)e.m_PoolName);
				const int poolSize = e.m_PoolSize;

				// Check if it already exists.
				int* existing = m_PoolSizeMap.Access(poolNameHash);
				if(existing)
				{
					// It does exist, override the previous value.
					*existing = poolSize;
				}
				else
				{
					// Doesn't exist already, add it.
					m_PoolSizeMap.Insert(poolNameHash, poolSize);
				}
			}

		}

		// We no longer need the object we used temporarily for loading.
		delete allConfigs;
		allConfigs = NULL;
	}
}


fwConfigManager::fwConfigManager()
		: m_Config(NULL)
{
}


fwConfigManager::~fwConfigManager()
{
	delete m_Config;
}

// PURPOSE:	Used by sParseSeparatedString() for holding name/value pairs.
struct fwConfigNameValuePair { const char* m_Name; bool m_Value; };

// PURPOSE:	Helper function for parsing a string containing a series of words,
//			separated by spaces or commas, where each word is a known option.
// PARAMS:	str				- The string to parse.
//			pairArray		- Array of name/value pairs.
//			numPairs		- Number of elements in the array.
//			valueIfEmpty	- Fallback value to use if the string is empty.
// RETURNS:	True if any of the words in the input string mapped to a true value.
static bool sParseSeparatedString(const char* str, const fwConfigNameValuePair* pairArray,
		int numPairs, bool valueIfEmpty)
{
	bool value = false;
	bool wordFound = false;
	const char* ptr = str;
	if(ptr)
	{
		// Loop over the string.
		while(1)
		{
#if !__NO_OUTPUT
			bool thisFound = false;
#endif
			// Skip any whitespace or separators.
			while(*ptr && (*ptr == ' ' || *ptr == '\t' || *ptr == ',')) ptr++;
			if(!*ptr)
			{
				// Done, found the null terminator.
				break;
			}

			wordFound = true;

			// Store the pointer to the start of the word, and look ahead until
			// encountering a separator or the end of the string.
			const char* wordStartPtr = ptr;
			while(*ptr && (*ptr != ' ' && *ptr != '\t' && *ptr != ',')) ptr++;
			const char* wordEndPtr = ptr;

			// Compute the length of the word we found.
			int wordLen = ptrdiff_t_to_int(wordEndPtr - wordStartPtr);

			// Look over the array of name/value pairs passed in by the user.
			for(int i = 0; i < numPairs; i++)
			{
				if(strnicmp(pairArray[i].m_Name, wordStartPtr, wordLen) == 0)
				{
					value = value | pairArray[i].m_Value;
#if !__NO_OUTPUT
					thisFound = true;
#endif
					break;
				}
			}

			// If we didn't recognize this word at all, generate an error message.
#if !__NO_OUTPUT
			if(!thisFound)
			{
				char buff[256];
				int charsToWrite = Min((int)(sizeof(buff) - 1), wordLen);
				strncpy(buff, wordStartPtr, charsToWrite);
				buff[charsToWrite] = '\0';
				Errorf("Unrecognized choice '%s' in string '%s'.", buff, str);
			}
#endif
		}
	}

	if(!wordFound)
	{
		value = valueIfEmpty;
	}
	return value;
}


bool fwConfigManager::MatchFilter(const fwConfigWithFilter& filter) const
{
	// Array of build names, and the corresponding values.
	static fwConfigNameValuePair s_BuildData[] =
	{
		{	"any",		1								},
		{	"bank",		__BANK							},
		{	"bankrelease",	!__DEV && __BANK			},
		{	"beta",		__DEV && __OPTIMIZED			},
		{	"debug",	__DEV && !__OPTIMIZED			},
		{	"dev",		__DEV							},
		{	"final",	__FINAL							},
		{	"profile",	__PROFILE						},
		{	"release",	!__DEV && !__BANK && !__FINAL	},
		{	"nonfinal",	!__FINAL						}
	};

	// Array of platform names, and the corresponding values.
	static fwConfigNameValuePair s_PlatformData[] =
	{
		{	"any",		1			},
		{	"ps3",		__PS3		},
		{	"psn",		__PS3		},
		{	"psp2",		__PSP2		},
		{	"x64",	__WIN32PC	},
		{	"xbox360",	__XENON		},
		{	"xenon",	__XENON		},
		{   "xboxone",  RSG_DURANGO },
		{	"ps4",	RSG_ORBIS	}
	};

	// Parse the Build and Platform strings.
	const bool buildValue = sParseSeparatedString(filter.m_Build.c_str(), s_BuildData, NELEM(s_BuildData), true);
	const bool platformValue = sParseSeparatedString(filter.m_Platforms.c_str(), s_PlatformData, NELEM(s_PlatformData), true);

#if !__FINAL
	const sysParam * pParam = filter.m_Param.c_str() ? sysParam::GetParamByName(filter.m_Param.c_str()) : NULL;
	const bool paramValue = !filter.m_Param.c_str() || (pParam && pParam->Get());
#else
	const bool paramValue = true;
#endif

	// To consider this a match, both the build and the platform must match.
	return buildValue && platformValue && paramValue;
}

//-----------------------------------------------------------------------------

fwConfig::fwConfig()
		: m_ArchMaxModelInfos(32200)
{
}


void fwConfig::MergeNonPoolData(const fwConfig &mergeIn)
{
	// Merge in values that have been set in the other configuration
	if(mergeIn.m_ArchMaxModelInfos >= 0)
	{
		m_ArchMaxModelInfos = mergeIn.m_ArchMaxModelInfos;
	}
}

//-----------------------------------------------------------------------------

fwConfigWithFilterBase::fwConfigWithFilterBase()
		: m_Config(NULL)
{
}


fwConfigWithFilterBase::fwConfigWithFilterBase(const fwConfigWithFilterBase& src)
		: m_Build(src.m_Build)
		, m_Platforms(src.m_Platforms)
		, m_Config(NULL)
{
	// Clone the fwConfig object.
	if(src.m_Config)
	{
		m_Config = src.m_Config->CreateClone();
	}
}


fwConfigWithFilterBase::~fwConfigWithFilterBase()
{
	// We own the configuration, so we need to delete it here.
	delete m_Config;
}


const fwConfigWithFilterBase& fwConfigWithFilterBase::operator=(const fwConfigWithFilterBase& src)
{
	// Delete any previous configuration.
	delete m_Config;
	m_Config = NULL;

	// Create a new fwConfig clone, if the source contains one.
	if(src.m_Config)
	{
		m_Config = src.m_Config->CreateClone();
	}

	// Copy the other data.
	m_Build = src.m_Build;
	m_Platforms = src.m_Platforms;

	return *this;
}

fwConfigWithFilter::fwConfigWithFilter(const fwConfigWithFilter& src)
	: fwConfigWithFilterBase(src)
#if !__FINAL
	, m_Param(src.m_Param)
#endif
{
}

const fwConfigWithFilter& fwConfigWithFilter::operator=(const fwConfigWithFilter& src)
{
	fwConfigWithFilterBase::operator=(src);

#if !__FINAL
	m_Param = src.m_Param;
#endif

	return *this;
}

//-----------------------------------------------------------------------------

// End of file fwsys/config.cpp
