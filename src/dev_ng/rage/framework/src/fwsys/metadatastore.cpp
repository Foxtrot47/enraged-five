//
// fwsys/metadatastore.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

//Framework headers
#include "fwsys/metadatastore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"
#include "streaming/streamingvisualize.h"

//Rage headers
#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/restparserservices.h"
#include "parser/visitorutils.h"

#define IMETA_PRESTREAM_DIST					(50)

namespace rage {

fwMetaDataStore g_fwMetaDataStore;

fwMetaDataStore::fwMetaDataStore()
		: fwAssetStore<fwMetaDataFile>("MetaDataStore", META_FILE_ID, CONFIGURED_FROM_FILE, 585, true)
#if __BANK
		, m_AllowLoadInResourceMem(true)
#endif
{
	atFixedBitSet32 loadFlags;
	// loadFlags.Set(fwPsoStoreLoader::RUN_POSTLOAD_CALLBACKS); Because of MT loading, do this in SetResource
	loadFlags.Set(fwPsoStoreLoader::LOAD_XML_FILES);
	loadFlags.Set(fwPsoStoreLoader::LOAD_IN_PLACE);
	loadFlags.Set(fwPsoStoreLoader::WARN_ON_LAYOUT_MISMATCH);

	m_loader.SetFlags(loadFlags);
	m_loader.SetInPlaceHeap(PSO_HEAP_CURRENT);
	m_loader.SetNotInPlaceHeap(PSO_HEAP_DEBUG);
	m_loader.SetRootType(NULL); // Load *any* type
}

void fwMetaDataStore::Init(u32 initMode)
{
	if(initMode == INIT_CORE)
	{
		fwBoxStreamer& streamer = g_fwMetaDataStore.GetBoxStreamer();

		// initialise the box streamer quadtree
		streamer.Init(&g_fwMetaDataStore, 30.0f);
		streamer.AddSupportedAsset(fwBoxStreamerAsset::ASSET_METADATA, IMETA_PRESTREAM_DIST, false, false, false);

		// assume not streamable by default use MakeSlotStreamable to switch it.
		for (s32 i=0; i<g_fwMetaDataStore.GetMaxSize(); i++)
		{
			streamer.SetIsIgnored(i, true);
		}
	}
}

void fwMetaDataStore::Shutdown()
{
	fwAssetStore<fwMetaDataFile>::Shutdown();
	m_boxStreamer.Shutdown();
}

void fwMetaDataStore::Update()
{
	RemoveUnrequired();
}

bool fwMetaDataStore::Load(strLocalIndex index, void* pData, int size)
{
	Assert(pData);

	fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);
	Assertf(pDef, "No entry at this slot");
	Assertf(!pDef->m_pObject, "%s is already in memory", pDef->m_name.GetCStr());

	pDef->m_pObject = rage_new fwMetaDataFile();

	fwPsoStoreLoader localLoader = m_loader;
	ConfigureLoaderForSlot(localLoader, index);
	const char *debugName = "";
#if !__NO_OUTPUT															
	char debugNameBuffer[RAGE_MAX_PATH];
	debugName = strStreamingEngine::GetInfo().GetObjectName(GetStreamingIndex(index), debugNameBuffer, NELEM(debugNameBuffer));
#endif

	parStructure::sm_SkipPostloadCallbacks = true; // Prevent postloads if loading from XML
	
	fwPsoStoreLoadResult loadResult = localLoader.Load(pData, size, debugName, pDef->m_pObject->m_fileData);
	pDef->m_pObject->m_structure = loadResult.m_Structure;
	
	streamAssertf(pDef->m_pObject->m_fileData.IsPso(), "Don't stream in any XML files: %s", debugName);

	parStructure::sm_SkipPostloadCallbacks = false; 
	
	fwPsoStoreLoader::RunPostloadCallbacks(loadResult.m_Structure, pDef->m_pObject->m_fileData);
	
	if (!m_boxStreamer.GetIsIgnored(index.Get()))
		AddRef(index, REF_OTHER);	// protect against streaming cleanup removal

#if __BANK
	char storeEntryName[256];
	formatf(storeEntryName, "MetadataStore/%s", pDef->m_name.GetCStr());
	parRestRegisterSingleton(storeEntryName, pDef->m_pObject->m_fileData.GetInstance(), pDef->m_pObject->m_structure, pDef->m_name.GetCStr());
#endif

	return true;
}

void fwMetaDataStore::PlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex), strStreamingLoader::StreamingFile& file, datResourceInfo& resInfo)
{
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
	streamAssertf(pInfo->GetStatus() == STRINFO_LOADING, "Info status is %d", pInfo->GetStatus());
	pInfo->SetFlag(file.m_Index, STRFLAG_INTERNAL_PLACING);
	strStreamingEngine::GetLoader().RequestAsyncPlacement(file, file.m_AllocationMap, resInfo);
}

void fwMetaDataStore::PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header)
{
#if !__FINAL
	fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);
	Assertf(pDef, "No entry at this slot");
	Assertf(!pDef->m_pObject, "%s is already in memory", pDef->m_name.GetCStr());
#endif

	fwPsoStoreLoader localLoader = m_loader; // Make a copy so we can change some of the flags
	ConfigureLoaderForSlot(localLoader, index);

	fwPsoStorePlacementCookies::Cookie cookie;

	const char *debugName = "";
#if !__NO_OUTPUT															
	char debugNameBuffer[RAGE_MAX_PATH];
	debugName = strStreamingEngine::GetInfo().GetObjectName(GetStreamingIndex(index), debugNameBuffer, NELEM(debugNameBuffer));
#endif

	if (header.GetVirtualSize() > 0)
	{
		// It's a resource PSO
		psoResourceData* resource;
		pgRscBuilder::PlaceStream(resource, header, map, 
#if !__FINAL
			pDef->m_name.GetCStr()
#else
			""
#endif
			);

		fwPsoStoreLoadResult loadResult = localLoader.LoadResource(resource, debugName, cookie.m_Instance);
		cookie.m_OtherData = loadResult.m_Structure;
	}
	else
	{
		parStructure::sm_SkipPostloadCallbacks = true; // Prevent postloads if loading from XML

		// It's not a resource, so chunk 0 points to the memory
		fwPsoStoreLoadResult loadResult = localLoader.Load(map.GetVirtualBase(), (int)map.GetVirtualSize(), debugName, cookie.m_Instance);
		cookie.m_OtherData = loadResult.m_Structure;

		parStructure::sm_SkipPostloadCallbacks = false;
	}

	g_PsoPlacementCookies.Add(GetStreamingIndex(index), cookie);
}

void fwMetaDataStore::SetResource(strLocalIndex index, datResourceMap& /*map*/)
{
	fwPsoStorePlacementCookies::Cookie cookie = g_PsoPlacementCookies.Get(GetStreamingIndex(index));

	fwMetaDataFile* file = rage_new fwMetaDataFile;
	file->m_fileData = cookie.m_Instance;
	file->m_structure = reinterpret_cast<parStructure*>(cookie.m_OtherData);

	fwPsoStoreLoader::RunPostloadCallbacks(file->m_structure, cookie.m_Instance);

	Set(index, file);

	if (!m_boxStreamer.GetIsIgnored(index.Get()))
		AddRef(index, REF_OTHER);	// protect against streaming cleanup removal

#if __BANK
	fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);
	char storeEntryName[256];
	formatf(storeEntryName, "MetadataStore/%s", pDef->m_name.GetCStr());
	parRestRegisterSingleton(storeEntryName, pDef->m_pObject->m_fileData.GetInstance(), pDef->m_pObject->m_structure, pDef->m_name.GetCStr());
#endif

	g_PsoPlacementCookies.Remove(GetStreamingIndex(index));
}

bool fwMetaDataStore::LoadFile(strLocalIndex index, const char* filename)
{
	fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);

	Assertf(pDef, "No object at this slot");
	Assertf(pDef->m_pObject == NULL, "%s:Object is in memory already", pDef->m_name.GetCStr());

	pDef->m_pObject = rage_new fwMetaDataFile();

	fwPsoStoreLoader localLoader = m_loader;
	ConfigureLoaderForSlot(localLoader, index);

	fwPsoStoreLoadResult loadResult = localLoader.Load(filename, pDef->m_pObject->m_fileData);
	pDef->m_pObject->m_structure = loadResult.m_Structure;


	if(!pDef->m_pObject->m_fileData.IsLoaded())
	{
		delete pDef->m_pObject; 
		pDef->m_pObject = NULL;
		return false;
	}

#if __BANK
	char storeEntryName[256];
	formatf(storeEntryName, "MetadataStore/%s", pDef->m_name.GetCStr());
	parRestRegisterSingleton(storeEntryName, pDef->m_pObject->m_fileData.GetInstance(), pDef->m_pObject->m_structure, pDef->m_name.GetCStr());
#endif

	return true;
}

bool fwMetaDataStore::SafeRemove(strLocalIndex slotIndex)
{
	bool bRemoved = false;
	if (HasObjectLoaded(slotIndex))
	{
		Assertf(GetNumRefs(slotIndex)==1, "Removing metadata %s with ref count %d", GetName(slotIndex), GetNumRefs(slotIndex));
		RemoveRef(slotIndex, REF_OTHER);
		bRemoved = StreamingRemove(slotIndex);
		if (!bRemoved)
		{
			AddRef(slotIndex, REF_OTHER);
		}
	}
	return bRemoved;
}

void fwMetaDataStore::Remove(strLocalIndex index)
{
	fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);
	Assertf(pDef, "No metadatefile in slot");
	Assertf(pDef->m_pObject!=NULL, "Metadata not in memory");

	fwPsoStoreLoader localLoader = m_loader;
	ConfigureLoaderForSlot(localLoader, index);

	localLoader.Unload(pDef->m_pObject->m_fileData, *pDef->m_pObject->m_structure, true);

#if __BANK
	char storeEntryName[256];
	formatf(storeEntryName, "MetadataStore/%s", pDef->m_name.GetCStr());
	parRestUnregisterSingleton(storeEntryName, pDef->m_pObject->m_fileData.GetInstance());
#endif

	fwAssetStore<fwMetaDataFile>::Remove(index);
}

bool fwMetaDataStore::RequiresTempMemory(strLocalIndex index) const
{
	// Adapted from fwMapDataStore::RequiresTempMemory(), to support unloading of
	// files loaded into the debug heap, in case of PSO mismatch:
	//	If the LOADER owns the storage, that means the STREAMER does not. Therefore the streamed
	//	memory is considered "temp". If the loader thinks the file is not loaded, the streamer
	//	should clean up any memory it holds too.
	const fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);
	if(pDef)
	{
		fwMetaDataFile* pFile = pDef->m_pObject;
		if(pFile && (!pFile->m_fileData.IsLoaded() || pFile->m_fileData.OwnsStorage()))
		{
			return true;
		}
	}

	return !ShouldLoadInResourceMem(index);
}


void* fwMetaDataStore::GetDataPtr(strLocalIndex index)	
{
	Assert(index.Get() >= 0);
	fwAssetDef<fwMetaDataFile>* pDef = GetSlot(index);
	if(pDef && pDef->m_pObject)
	{
		return pDef->m_pObject->m_fileData.GetStorage();
	}
	return NULL;
}

void fwMetaDataStore::RemoveUnrequired()
{
	STRVIS_AUTO_CONTEXT(strStreamingVisualize::METADATASTORE);

	PF_PUSH_TIMEBAR_DETAIL("RemoveUnrequired");
	for (int i=0; i < GetSize(); i++)
	{
		if (!m_boxStreamer.GetIsIgnored(i) &&
			m_boxStreamer.CanRemoveAsset(i) && 
			!(GetStreamingFlags(strLocalIndex(i))&STR_DONTDELETE_MASK) &&
			!m_boxStreamer.GetIsPinned(i)
			)
		{
			ClearRequiredFlag(i, STRFLAG_DONTDELETE);

			if (HasObjectLoaded(strLocalIndex(i)))
			{
				Verifyf(SafeRemove(strLocalIndex(i)),"Object in slot (%d) filed to be removed.", i);
			}
		}
	}
	PF_POP_TIMEBAR_DETAIL();
}

void fwMetaDataStore::RemoveLoaded(strLocalIndex slotIndex)
{
	PF_PUSH_TIMEBAR_DETAIL("RemoveLoaded");
	ClearRequiredFlag(slotIndex.Get(), STRFLAG_DONTDELETE);

	if (HasObjectLoaded(slotIndex))
	{
		Verifyf(SafeRemove(slotIndex),"Object in slot (%d) filed to be removed.", slotIndex.Get());
	}
	PF_POP_TIMEBAR_DETAIL();
}

strLocalIndex fwMetaDataStore::AddSlot(const fwAssetDef<fwMetaDataFile>::strObjectNameType name)
{
	strLocalIndex index = strLocalIndex(fwAssetStore<fwMetaDataFile>::AddSlot(name));
	m_boxStreamer.InitSlot(index.Get(), fwBoxStreamerAsset::ASSET_METADATA);
	m_boxStreamer.SetIsIgnored(index.Get(), true); // assume not streamable by default
	return index;
}

strLocalIndex fwMetaDataStore::AddSlot(strLocalIndex index, const fwAssetDef<fwMetaDataFile>::strObjectNameType name)
{
	fwAssetStore<fwMetaDataFile>::AddSlot(index, name);
	m_boxStreamer.InitSlot(index.Get(), fwBoxStreamerAsset::ASSET_METADATA);
	m_boxStreamer.SetIsIgnored(index.Get(), true); // assume not streamable by default
	return index;
}

int fwMetaDataStore::MakeSlotStreamable(const fwAssetDef<fwMetaDataFile>::strObjectNameType name, spdAABB& aabb)
{
	int id = fwAssetStore<fwMetaDataFile>::FindSlot(name).Get();
	if (id == -1) //just to be sure in case the slot was not setup yet... 
		id = fwAssetStore<fwMetaDataFile>::AddSlot(name).Get();

	m_boxStreamer.InitSlot(id, fwBoxStreamerAsset::ASSET_METADATA);
	m_boxStreamer.GetBounds(id) = aabb;
	m_boxStreamer.SetIsIgnored(id, false); //no longer ignored by box streamer
	return id;
}

void fwMetaDataStore::RemoveSlot(strLocalIndex index)
{
	fwAssetStore<fwMetaDataFile>::RemoveSlot(index);
	m_boxStreamer.RemoveSlot(index.Get());//just incase it was setup with bounds...
}

void fwMetaDataStore::ShutdownSystem(unsigned shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		g_fwMetaDataStore.RemoveAll(true);
		g_fwMetaDataStore.Shutdown();
	}
}

#if __BANK
void fwMetaDataStore::InitWidgets(bkBank& bank)
{
	bank.PushGroup("Meta Data");
	m_boxStreamer.InitWidgets(bank, "fwMetaDataStore");
	//todo ... other widgets go here ... 
	bank.PopGroup();
}
#endif

void fwMetaDataStore::RemoveAll(bool bIgnoreRefCount)
{
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(strLocalIndex(i)) && Get(strLocalIndex(i)))
		{
			if(bIgnoreRefCount || 
				(GetNumRefs(strLocalIndex(i)) <= 1 && !(GetStreamingFlags(strLocalIndex(i)) & STR_DONTDELETE_MASK) && !m_boxStreamer.GetIsIgnored(i)))
			{
				SafeRemove(strLocalIndex(i));
			}
#if __DEV
			else
			{

				Displayf("Failed to remove %s.%s asidx [%d] stridx {%d} : refCount %d : D%d M%d C%d I%d",
					GetName(strLocalIndex(i)),
					fwMetaDataStore::m_fileExt.c_str(),
					i,
					GetStreamingIndex(strLocalIndex(i)).Get(),
					GetNumRefs(strLocalIndex(i)),
					(GetStreamingFlags(strLocalIndex(i)) & STRFLAG_DONTDELETE) != 0,
					(GetStreamingFlags(strLocalIndex(i)) & STRFLAG_MISSION_REQUIRED) != 0,
					(GetStreamingFlags(strLocalIndex(i)) & STRFLAG_CUTSCENE_REQUIRED) != 0,
					(GetStreamingFlags(strLocalIndex(i)) & STRFLAG_INTERIOR_REQUIRED) != 0);
				strStreamingEngine::GetInfo().PrintAllDependents(GetStreamingIndex(strLocalIndex(i)));

			}
#endif // __DEV			
		}
	}	
}

void fwMetaDataStore::ConfigureLoaderForSlot(fwPsoStoreLoader& loader, strLocalIndex index) const
{
	if(ShouldLoadInResourceMem(index))
	{
		loader.SetInPlaceHeap(PSO_DONT_ALLOCATE);
		loader.SetFlags( loader.GetFlags().Set(fwPsoStoreLoader::ASSERT_ON_LAYOUT_MISMATCH) );
	}
	else
	{
		loader.SetInPlaceHeap(PSO_HEAP_CURRENT);
		loader.SetFlags( loader.GetFlags().Clear(fwPsoStoreLoader::ASSERT_ON_LAYOUT_MISMATCH) );
	}
}

bool fwMetaDataStore::ShouldLoadInResourceMem(strLocalIndex index) const
{
	// Currently, the only files that use the box streamer are the scenario files,
	// which are the ones we want to resource load. If the box streamer ends up being
	// used for some other type of file in the future, and those should not use resource
	// memory, then we may need some other flag or something to keep track of that.
	// m_AllowLoadInResourceMem here is needed to support the scenario editor, since
	// in that case, we can't in-place load and thus shouldn't try to use resource memory. 

	const strStreamingInfo& info = *GetStreamingInfo(index);
	if (info.IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
	{
		return true;
	}

	return BANK_ONLY(m_AllowLoadInResourceMem &&) !m_boxStreamer.GetIsIgnored(index.Get());
}

} // namespace rage
