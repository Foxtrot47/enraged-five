<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

	<structdef type="fwConfigManager::fwPoolSizeEntry" >
		<string name="m_PoolName" type="ConstString"/>
		<int name="m_PoolSize"/>
	</structdef>

	<structdef type="fwConfigManager::fwPoolSizeEntryArray" >
		<array name="m_Entries" type="atArray">
			<struct type="fwConfigManager::fwPoolSizeEntry"/>
		</array>
	</structdef>

	<structdef type="fwConfig" >
		<struct name="m_PoolSizes" type="fwConfigManager::fwPoolSizeEntryArray"/>
	</structdef>

	<structdef type="fwConfigWithFilterBase" constructible="false" >
		<string name="m_Build" type="ConstString"/>
		<string name="m_Platforms" type="ConstString"/>
		<pointer name="m_Config" type="fwConfig" policy="owner" />
	</structdef>

  	<structdef type="fwConfigWithFilter" base="fwConfigWithFilterBase" cond="!__FINAL" >
    	<string name="m_Param" type="ConstString"/>
  	</structdef>  

  	<structdef type="fwConfigWithFilter" base="fwConfigWithFilterBase" cond="__FINAL" >
  	</structdef>

	<structdef type="fwAllConfigs" >
		<array name="m_ConfigArray" type="atArray">
			<struct type="fwConfigWithFilter"/>
		</array>
	</structdef>
</ParserSchema>
