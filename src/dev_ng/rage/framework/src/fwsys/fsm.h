//
// fwsys/fsm.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef FWSYS_FSM_H__
#define FWSYS_FSM_H__

#include "atl/bitset.h"

namespace rage {

#if 1
#define fwFsmBegin		if(state<0){if(0){
#define fwFsmEnd		}}else{Assert(0);return(Quit);} return(Unhandled);
#define fwFsmState(a)	}}else if(a == state) { if(0){
#define fwFsmOnEvent(a)	}else if(a == event) {
#define fwFsmOnEnter	fwFsmOnEvent(OnEnter)
#define fwFsmOnUpdate	fwFsmOnEvent(OnUpdate)
#define fwFsmOnExit		fwFsmOnEvent(OnExit)
#define fwFsmOnMsg(a)	}else if (OnMessage==event && a==message){
#else
#define fwFsmBegin		if(state<0){char statename[] = "STATE_Global"; if(0){ Warningf(statename);
#define fwFsmEnd		}}else{Assert(0);return(Quit);} return(Unhandled);				
#define fwFsmState(a)	}}else if(a == state) {char statename[] = #a; if(0){ Warningf(statename);
#define fwFsmOnEvent(a)	}else if(a == event) { Warningf("FSM State %s %s", statename, #a);
#define fwFsmOnEnter	fwFsmOnEvent(OnEnter)
#define fwFsmOnUpdate	fwFsmOnEvent(OnUpdate)
#define fwFsmOnExit		fwFsmOnEvent(OnExit)
#define fwFsmOnMsg(a)	}else if (OnMessage==event && a==message){Warningf("FSM State %s Message %s", statename, #a);
#endif

class fwFsm
{
public:
	enum
	{
		CurrentStateNotYetEntered = BIT(0)
	};

	// PURPOSE: State return values, continue or quit
	enum Return
	{
		Unhandled = 0,
		Continue,
		Quit,
	};

	// PURPOSE: Basic FSM operation events
	enum Event
	{
		OnEnter = 0,
		OnUpdate,
		OnExit,
		OnMessage,
	};

	fwFsm();
	virtual ~fwFsm() {}

	void Reset();

	fwFsm::Return Update();

	void Run() 
	{
		while (Update() != fwFsm::Quit)
		{
			/* Run like the wind */
		}
	}

	bool RunOneIteration()
	{
		if(Update() == fwFsm::Quit)
		{
			return false;
		}
		return true;
	}

	s16  GetState() const {return m_State;}
	void SetState(s16 state);
	void SetMessage(s16 message);

protected:
	void SetTimeInState(float timeInState) {m_TimeInState = timeInState;}
	virtual fwFsm::Return UpdateState(const s32 state, const s32 iMessage, const fwFsm::Event event) = 0;
	
	float				  GetTimeInState() const {return m_TimeInState;}

private:
	float					m_TimeInState;
	float					m_TimeRunning;
	s16						m_State; 
	s16						m_NextState; 
	s16						m_Message; 
	atFixedBitSet16			m_Flags;
};

} // namespace rage

#endif // FWSYS_FSM_H__