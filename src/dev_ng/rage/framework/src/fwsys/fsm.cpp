//
// fwsys/fsm.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "fsm.h"

#include "timer.h"

namespace rage {

fwFsm::fwFsm()
{
	Reset();
}

void fwFsm::Reset()
{
	m_State = 0; 
	m_NextState = 0;
	m_Message = -1;
	m_TimeInState = .0f;
	m_TimeRunning= .0f;
	m_Flags.Reset();
	m_Flags.Set(fwFsm::CurrentStateNotYetEntered);
}

void fwFsm::SetState(s16 state)
{
	Assertf(m_NextState == m_State, "Changing state again before previous transition has been completed");
	m_NextState = state;
}

void fwFsm::SetMessage(s16 message)
{
	Assertf(m_Message == -1, "Changing message again before previous message has been handled");
	m_Message = message;
}

fwFsm::Return fwFsm::Update()
{
	m_TimeRunning += fwTimer::GetTimeStep();
	m_TimeInState += fwTimer::GetTimeStep();

	fwFsm::Return ret = fwFsm::Unhandled;
	while (ret == Unhandled)
	{	
		if (m_State != m_NextState)
		{
			s16 state = m_State;

			m_State = m_NextState;

			ret = UpdateState(state, -1, OnExit);

			m_Flags.Set(fwFsm::CurrentStateNotYetEntered);
			m_TimeInState = 0.0f;
		}
		else if (m_Message != -1)
		{
			ret = UpdateState(m_State, m_Message, OnMessage);
			if (ret == Unhandled)
			{
				ret = UpdateState(-1, m_Message, OnMessage); // global message handler
			}

			m_Message = -1;
		}
		else
		{
			if (m_Flags.GetAndClear(fwFsm::CurrentStateNotYetEntered))
			{
				ret = UpdateState(m_State, -1, OnEnter);
			}
			else if (m_State == m_NextState)
			{
				ret = UpdateState(m_State, -1, OnUpdate);
			}
		}
	}

	return ret;
}

} // namespace rage