//
// fwsys/game.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "game.h"

#include "metadatastore.h"
#include "fwscene/stores/drawablestore.h"
#include "fwscene/stores/dwdstore.h"
#include "fwscene/stores/fragmentstore.h"
#include "fwscene/stores/txdstore.h"
#include "fwscene/stores/clothstore.h"
#include "fwscene/stores/maptypesstore.h"
#include "fwscene/stores/mapdatastore.h"
#include "fwscene/scan/scan.h"
#include "fwscene/search/search.h"
#include "profile/timebars.h"
#include "streaming/packfilemanager.h"

namespace rage
{

fwGameInterface* g_fwGameInterface;

void fwGame::RegisterStreamingModules()
{
	USE_MEMBUCKET(MEMBUCKET_STREAMING);
	g_DwdStore.RegisterStreamingModule();
	g_DrawableStore.RegisterStreamingModule();
	g_TxdStore.RegisterStreamingModule();
	g_FragmentStore.RegisterStreamingModule();
	g_ClothStore.RegisterStreamingModule();
	g_fwMetaDataStore.RegisterStreamingModule();

	g_MapTypesStore.RegisterStreamingModule();
	fwMapDataStore::GetStore().RegisterStreamingModule();

	g_fwGameInterface->RegisterStreamingModules();
}


void fwGame::InitClass(fwGameInterface* gameInterface)
{
	g_fwGameInterface = gameInterface;
}

void fwGame::ShutdownClass()
{
	delete g_fwGameInterface;
	g_fwGameInterface = NULL;
}

void fwGame::Init()
{
	RegisterStreamingModules();
}

void fwGame::Shutdown()
{
}

void fwGame::Update()
{
#if __BANK && !__TOOL
	fwScan::UpdateDebug();
	fwSearch::UpdateDebug();
#endif
}

} // namespace rage
