//
// fwsys/config.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWSYS_CONFIG_H
#define FWSYS_CONFIG_H

#include "atl/array.h"			// atArray
#include "atl/map.h"			// atMap
#include "parser/macros.h"		// PAR_SIMPLE_PARSABLE
#include "string/string.h"		// ConstString

//-----------------------------------------------------------------------------

namespace rage
{

class fwBasePool;
struct fwConfig;
struct fwConfigWithFilter;

//-----------------------------------------------------------------------------

/*
PURPOSE
	fwConfigManager handles game configuration files which specify the size of
	pools and similar settings and limits. Pools deriving from fwBasePool will
	all be possible to override from the configuration file, while other uses
	may involve adding variables to fwConfig or a subclass of it.

	fwConfigManager is meant to be initialized as early as possible during
	startup, probably right after parParserManager. fwPool objects can be
	created before that, but may not be used until the configuration file
	has loaded and the proper size is known.
*/
class fwConfigManager
{
public:
	// PURPOSE:	During loading of the configuration files, this specifies
	//			the size of a particular named pool.
	struct fwPoolSizeEntry
	{
		// PURPOSE:	The name of the fwPool, as passed in as the pName parameter
		//			in the constructor.
		ConstString		m_PoolName;

		// PURPOSE:	The desired maximum size of the pool.
		int				m_PoolSize;

		PAR_SIMPLE_PARSABLE;
	};

	// PURPOSE:	An array of fwPoolSizeEntry objects, this is also only used
	//			internally while loading the configuration files.
	struct fwPoolSizeEntryArray
	{
		// PURPOSE:	Array of pool size entries.
		atArray<fwPoolSizeEntry>	m_Entries;

		PAR_SIMPLE_PARSABLE;
	};

	// PURPOSE:	Shut down the class, destroying the instance.
	// NOTES:	Should match a previous call to fwConfigManagerImpl<>::InitClass().
	static void ShutdownClass();

	// PURPOSE:	Get a pointer to the current instance, if it exists.
	// RETURNS:	Pointer to the current instance, or NULL.
	static fwConfigManager* GetInstancePtr()
	{	return sm_Instance;	}

	// PURPOSE:	Get a reference to the current instance, if it's known that it should exist.
	// RETURNS:	Reference to the current instance.
	static fwConfigManager& GetInstance()
	{	Assertf(sm_Instance, "Expected configuration manager object");
		return *sm_Instance;
	}

	// PURPOSE:	Get the desired size of a pool with a given name. Used internally by fwBasePool
	//			when allocating storage space.
	// PARAMS:	poolNameHash	- The hash (atStringHash) of the name of the pool to get the size of.
	//			defaultSize		- The size that will be used if there is no override.
	// RETURNS:	defaultSize, or whatever override was set in the configuration file.
	int GetSizeOfPool(u32 poolNameHash, int defaultSize) const;

	// PURPOSE:	Indicate that a given pool is not intended to have its size set through the
	//			configuration file.
	// PARAMS:	poolName		- The name of the pool.
	void LockPoolSize(const atHashString& poolNameHash);

	// PURPOSE:	Load a configuration file with a given name. Normally called internally
	//			by fwConfigManagerImpl<>::InitClass().
	// PARAMS:	pConfigFilename		- Name of the configuration file to load.
	void Load(const char* pConfigFilename);

	// PURPOSE:	Get the configuration.
	// RETURNS:	Read-only reference to the configuration data.
	const fwConfig& GetConfig() const
	{	Assert(m_Config);
		return *m_Config;
	}

	// PURPOSE:	Set the UseLargetPoolFromCodeOrData option.
	static void SetUseLargerPoolFromCodeOrData(bool b)
	{	sm_UseLargerPoolFromCodeOrData = b;	}

protected:
	// PURPOSE:	Default constructor. These objects should normally be constructed by
	//			fwConfigManagerImpl<>::InitClass().
	fwConfigManager();

	// PURPOSE:	Destructor. These objects are normally destroyed by
	//			fwConfigManager::ShutdownClass().
	virtual ~fwConfigManager();

	// PURPOSE:	Factory function for configuration objects, allowing games to use
	//			different subclasses, to hold data not needed at the framework level.
	//			Subclasses of fwConfigManager should override this, but this is normally
	//			taken care of by using fwConfigManagerImpl.
	// RETURNS:	Pointer to the newly created fwConfig subclass.
	virtual fwConfig *CreateConfigData() const = 0;

	// PURPOSE:	Used internally when loading configuration files, to see if values in
	//			a given configuration should apply to the current build and platform.
	// PARAMS:	filter		- Configuration with filter that shold be checked.
	// RETURNS:	True if this configuration should apply.
	bool MatchFilter(const fwConfigWithFilter& filter) const;

	enum
	{
		// PURPOSE:	Used as a marker in m_PoolSizeMap to indicate that a pool has a locked size.
		kLockedPoolSize = -1
	};

	// PURPOSE:	Mapping from pool name hash to pool size. This gets set up by
	//			loading a configuration file, and is used by GetSizeOfPool().
	atMap<u32, int>			m_PoolSizeMap;

	// PURPOSE:	Pointer to the currently loaded configuration, merged together
	//			from all sections of the configuration file that apply for the
	//			current build and platform.
	// NOTES:	This object is owned.
	fwConfig*				m_Config;

	// PURPOSE:	If true, silently use the default pool size if it's larger than
	//			what's found in the configuration file.
	// NOTES:	May be a temporary solution.
	static bool				sm_UseLargerPoolFromCodeOrData;

	// PURPOSE:	Pointer to the manager instance, or NULL.
	static fwConfigManager*	sm_Instance;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	This subclass template simply makes sure that it's easy to use fwConfigManager
	with configuration classes specialized for the game.
*/
template<class _CfgData> class fwConfigManagerImpl : public fwConfigManager
{
public:
	// PURPOSE:	Initialize fwConfigManager, by loading a given configuration file
	//			and resizing all fwPool objects that have been registered.
	// PARAMS:	configFilename		- File name of the configuration file to load,
	//								  if it exists.
	static void InitClass(const char* configFilename)
	{
		// Should only have one instance.
		Assert(!sm_Instance);

		// Create the instance.
		sm_Instance = rage_new fwConfigManagerImpl<_CfgData>;

		// Load the configuration file. Note that this couldn't just be done
		// by constructor of fwConfigManager, as we need to finish the construction
		// before the call to CreateConfigData().
		sm_Instance->Load(configFilename);
	}

protected:
	virtual fwConfig *CreateConfigData() const
	{	return rage_new _CfgData;	}
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	fwConfig contains the actual configuration variables of the game engine.
	It can be loaded from an XML file and knows how to create a clone of itself
	and how to merge in variables specified in another fwConfig object.
	A game would typically override this above the framework level, adding
	whatever settings the game needs.

EXAMPLES
	This block of XML would set the size of the fwPool named "FragmentStore" to 1000:
		<PoolSizes>
			<Entries>
				<Item>
					<PoolName>FragmentStore</PoolName>
					<PoolSize value="1000"/>
				</Item>
			</Entries>
		</PoolSizes>

*/
struct fwConfig
{
	// PURPOSE:	Constructor.
	fwConfig();

	// PURPOSE:	Destructor.
	virtual ~fwConfig() {}

	// PURPOSE:	Merge in data from another fwConfig object, for the variables
	//			not set to their default values. Subclasses will need to override this.
	// PARAMS:	mergeIn		- Other fwConfig object to merge.
	// NOTES:	m_PoolSizes is not expected to be merged by this function, it's handled
	//			differently.
	virtual void MergeNonPoolData(const fwConfig &mergeIn);

	// PURPOSE:	Create a clone of this object.
	// RETURNS:	A pointer to a copy of this object, to be freed by the user with 'delete' when no longer needed.
	// NOTES:	The need for this mostly comes from fwConfig being used in an array of
	//			fwConfigWithFilter objects, and when that array gets resized, temporary copies may
	//			have to be made.
	virtual fwConfig *CreateClone() const
	{
		return rage_new fwConfig(*this);
	}

	// PURPOSE:	Array of pool size overrides.
	fwConfigManager::fwPoolSizeEntryArray		m_PoolSizes;

	// PURPOSE:	Max number of fwArchetype pointers in fwArchetypeManager.
	// NOTES:	Used to be NUM_MODEL_INFOS in 'entity/archetypemanager.h'.
	int				m_ArchMaxModelInfos;

	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	fwConfigWithFilterBase consists of an fwConfig object, together with a filter
	which determines on what platforms and builds the values in that configuration
	should be used. This class is only used while loading, after that, everything
	gets merged together into a single fwConfig object.
*/
struct fwConfigWithFilterBase
{
	// PURPOSE:	Default constructor.
	explicit fwConfigWithFilterBase();

	// PURPOSE:	Copy constructor.
	// PARAMS:	src		 - Object to copy.
	explicit fwConfigWithFilterBase(const fwConfigWithFilterBase& src);

	// PURPOSE:	Destructor.
	~fwConfigWithFilterBase();

	// PURPOSE:	Assignment operator.
	// PARAMS:	src		- Object to copy.
	// RETURNS:	Reference to this object.
	const fwConfigWithFilterBase& operator=(const fwConfigWithFilterBase& src);

	// PURPOSE:	List of builds (beta, release, etc) in which these configuration
	//			value should be used.
	ConstString			m_Build;

	// PURPOSE:	List of platforms (PS3, Xbox360, etc) in which these configuration
	//			value should be used.
	ConstString			m_Platforms;

	// PURPOSE:	Pointer to an owned object containing the actual configuration values.
	fwConfig*			m_Config;

	PAR_SIMPLE_PARSABLE;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	fwConfigWithFilter derives fwConfigWithFilterBase so we can add extra dev filters
*/
struct fwConfigWithFilter : fwConfigWithFilterBase
{
	// PURPOSE:	Default constructor.
	explicit fwConfigWithFilter() {};

	// PURPOSE:	Copy constructor.
	// PARAMS:	src		 - Object to copy.
	explicit fwConfigWithFilter(const fwConfigWithFilter& src);

	// PURPOSE:	Assignment operator.
	// PARAMS:	src		- Object to copy.
	// RETURNS:	Reference to this object.
	const fwConfigWithFilter& operator=(const fwConfigWithFilter& src);

#if !__FINAL
	// PURPOSE:	Param in which these configuration values should be used.
	ConstString			m_Param;
#endif

	PAR_SIMPLE_PARSABLE;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	During loading, a configuration file gets loaded by PARSER using this type.
	The array of conditional configurations gets collapsed into a single fwConfig
	object by fwConfigManager.
*/
struct fwAllConfigs
{
	// PURPOSE:	Array of conditional configurations.
	atArray<fwConfigWithFilter>		m_ConfigArray;

	PAR_SIMPLE_PARSABLE;
};

//-----------------------------------------------------------------------------

// PURPOSE:	This is meant to be used to make it clear when a value, such as
//			a pool size, is expected to always get overridden by a value from
//			the configuration file. That way, we don't get programmers wasting
//			time trying to change something in code that won't work anyway.
#define CONFIGURED_FROM_FILE (1)

//-----------------------------------------------------------------------------

}	// namespace rage

//-----------------------------------------------------------------------------

#endif	// FWSYS_CONFIG_H

// End of file fwsys/config.h
