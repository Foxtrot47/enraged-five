//
// filename:	fileExts.h
// description:	
//

#ifndef FRAMEWORK_FILEEXTS_H_
#define FRAMEWORK_FILEEXTS_H_

#include "grcore/config_switches.h"		// yuck

// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
// Game headers

// --- Defines ------------------------------------------------------------------

/* 
	If you need to add a new resource type, make sure you add it in all the appropriate places:
	1. NEWTYPE_FILE_ID
	2. NEWTYPE_FILE_EXT, for each of the *three* supported platforms (assuming it's resourced)
	3. NEWTYPE_FILE_EXT_PATTERN
	4. PI_NEWTYPE_FILE_EXT
	5. The FILEEXT_CASES macro definition.  Note that you usually don't want the PI version 
	   in the macro unless that's what actually ends up in the game-side rpf file.
*/


#define DRAWABLE_FILE_VERSION				(rmcDrawable::RORC_VERSION + 18)
#define DWD_FILE_VERSION					(rmcDrawable::RORC_VERSION + 18)
#define FRAGMENT_FILE_VERSION				rvFragObject
#define ANIM_FILE_VERSION					crClipDictionary::RORC_VERSION
#define BOUND_FILE_VERSION					phBound::RORC_VERSION
#define BOUNDSDICTIONARY_FILE_VERSION		phBound::RORC_VERSION
#define BLENDSHAPE_FILE_VERSION				grbTargetManager::RORC_VERSION
#define PMDICTIONARY_FILE_VERSION			crpmParameterizedMotionDictionary::RORC_VERSION
#define EXPRESSIONSDICTIONARY_FILE_VERSION	crExpressions::RORC_VERSION
#define FXLIST_FILE_VERSION					ptxFxList::RORC_VERSION
#define CLOTH_FILE_VERSION					phVerletCloth::RORC_VERSION
#define NETWORK_DEF_FILE_VERSION			mvNetworkDef::RORC_VERSION
#define POSEMATCHER_FILE_VERSION			crPoseMatcher::RORC_VERSION
#define FRAMEFILTERDICTIONARY_FILE_VERSION	crFrameFilter::RORC_VERSION

#define PI_ARCHIVE_FILE_ID						0
#define DRAWABLE_FILE_ID						1
#define FRAGMENT_FILE_ID						2
#define DWD_FILE_ID								3
#define TXD_FILE_ID								4
#define ANIM_FILE_ID							5
#define BOUND_FILE_ID							6
#define BOUNDSDICTIONARY_FILE_ID				7
#define BLENDSHAPE_FILE_ID						8
#define CLOTH_FILE_ID							9
#define PMDICTIONARY_FILE_ID					10
#define EXPRESSIONSDICTIONARY_FILE_ID			11
#define FXLIST_FILE_ID							12
#define META_FILE_ID							13
#define MAPDATA_FILE_ID							14
#define POSEMATCHER_FILE_ID						15
#define PI_NETWORK_DEF_FILE_ID					16			// mrf
#define NAVMESH_FILE_ID							17
#define NAVNODES_FILE_ID						18
#define SCRIPT_FILE_ID							19			// NOTE, depending on the value of RESOURCED_SCRIPTS this needs to expand to SCRIPT_FILE_EXT or PI_SCRIPT_FILE_EXT
#define PI_CUTSCENE_FILE_ID						20			// cut
#define PI_SCALEFORM_FILE_ID					21			// gfx
#define PLACEMENTS_FILE_ID						22
#define AUDMESH_FILE_ID							23
#define PATHS_FILE_ID							24			// nod or #nd?
#define PI_PATHS_FILE_ID						25			// nod or #nd?
#define VEHICLERECORDING_FILE_ID				26
#define WAYPOINTRECORDING_FILE_ID				27
#define MANIFEST_FILE_ID						28
#define MAPTYPES_FILE_ID						29
#define HEIGHTMESH_FILE_ID						30
#define FRAMEFILTERDICTIONARY_FILE_ID			31
#define TEXT_DATABASE_FILE_ID					32

#if __WIN32PC

#if __64BIT

#define DRAWABLE_FILE_EXT						"ydr"
#define FRAGMENT_FILE_EXT						"yft"
#define DWD_FILE_EXT							"ydd"
#define TXD_FILE_EXT							"ytd"
#define ANIM_FILE_EXT							"ycd"
#define BOUND_FILE_EXT							"ybn"
#define BOUNDSDICTIONARY_FILE_EXT				"ybd"
#define BLENDSHAPE_FILE_EXT						"ybs"
#define CLOTH_FILE_EXT							"yld"
#define PMDICTIONARY_FILE_EXT					"ypm"
#define EXPRESSIONSDICTIONARY_FILE_EXT			"yed"
#define FXLIST_FILE_EXT							"ypt"
#define META_FILE_EXT							"ymt"
#define MAPDATA_FILE_EXT						"ymap"
#define MAPTYPES_FILE_EXT						"ytyp"
#define NETWORK_DEF_FILE_EXT					"yrf"
#define POSEMATCHER_FILE_EXT					"ypdb"
#define NAVMESH_FILE_EXT						"ynv"
#define NAVNODES_FILE_EXT						"yhn"
#define SCRIPT_FILE_EXT							"ysc"
#define AUDMESH_FILE_EXT						"yam"
#define PLACEMENTS_FILE_EXT						"ypl"
#define AUDMESH_FILE_EXT						"yam"
#define PATHS_FILE_EXT							"ynd"
#define VEHICLERECORDING_FILE_EXT				"yvr"
#define WAYPOINTRECORDING_FILE_EXT				"ywr"
#define MANIFEST_FILE_EXT						"ymf"
#define HEIGHTMESH_FILE_EXT						"ynh"
#define FRAMEFILTERDICTIONARY_FILE_EXT			"yfd"
#define TEXT_DATABASE_FILE_EXT					"yldb"

#else

#define DRAWABLE_FILE_EXT						"wdr"
#define FRAGMENT_FILE_EXT						"wft"
#define DWD_FILE_EXT							"wdd"
#define TXD_FILE_EXT							"wtd"
#define ANIM_FILE_EXT							"wcd"
#define BOUND_FILE_EXT							"wbn"
#define BOUNDSDICTIONARY_FILE_EXT				"wbd"
#define BLENDSHAPE_FILE_EXT						"wbs"
#define CLOTH_FILE_EXT							"wld"
#define PMDICTIONARY_FILE_EXT					"wpm"
#define EXPRESSIONSDICTIONARY_FILE_EXT			"wed"
#define FXLIST_FILE_EXT							"wpt"
#define META_FILE_EXT							"wmt"
#define MAPDATA_FILE_EXT						"wmap"
#define MAPTYPES_FILE_EXT						"wtyp"
#define NETWORK_DEF_FILE_EXT					"wrf"
#define POSEMATCHER_FILE_EXT					"wpdb"
#define NAVMESH_FILE_EXT						"wnv"
#define NAVNODES_FILE_EXT						"whn"
#define SCRIPT_FILE_EXT							"wsc"
#define AUDMESH_FILE_EXT						"wam"
#define PLACEMENTS_FILE_EXT						"wpl"
#define AUDMESH_FILE_EXT						"wam"
#define PATHS_FILE_EXT							"wnd"
#define VEHICLERECORDING_FILE_EXT				"wvr"
#define WAYPOINTRECORDING_FILE_EXT				"wwr"
#define MANIFEST_FILE_EXT						"wmf"
#define HEIGHTMESH_FILE_EXT						"wnh"
#define FRAMEFILTERDICTIONARY_FILE_EXT			"wfd"
#define TEXT_DATABASE_FILE_EXT					"wldb"

#endif

#if RSG_ORBIS
#define TXD_FILE_VERSION		grcTextureGNM::RORC_VERSION
#else
#define TXD_FILE_VERSION		grcTexturePC::RORC_VERSION
#endif

#elif RSG_DURANGO

#define DRAWABLE_FILE_EXT						"ddr"
#define FRAGMENT_FILE_EXT						"dft"
#define DWD_FILE_EXT							"ddd"
#define TXD_FILE_EXT							"dtd"
#define ANIM_FILE_EXT							"dcd"
#define BOUND_FILE_EXT							"dbn"
#define BOUNDSDICTIONARY_FILE_EXT				"dbd"
#define BLENDSHAPE_FILE_EXT						"dbs"
#define CLOTH_FILE_EXT							"dld"
#define PMDICTIONARY_FILE_EXT					"dpm"
#define EXPRESSIONSDICTIONARY_FILE_EXT			"ded"
#define FXLIST_FILE_EXT							"dpt"
#define META_FILE_EXT							"dmt"
#define MAPDATA_FILE_EXT						"dmap"
#define MAPTYPES_FILE_EXT						"dtyp"
#define NETWORK_DEF_FILE_EXT					"drf"
#define POSEMATCHER_FILE_EXT					"dpdb"
#define NAVMESH_FILE_EXT						"dnv"
#define NAVNODES_FILE_EXT						"dhn"
#define SCRIPT_FILE_EXT							"dsc"
#define AUDMESH_FILE_EXT						"dam"
#define PLACEMENTS_FILE_EXT						"dpl"
#define PATHS_FILE_EXT							"dnd"
#define VEHICLERECORDING_FILE_EXT				"dvr"
#define WAYPOINTRECORDING_FILE_EXT				"dwr"
#define MANIFEST_FILE_EXT						"dmf"
#define HEIGHTMESH_FILE_EXT						"dnh"
#define FRAMEFILTERDICTIONARY_FILE_EXT			"dfd"
#define TEXT_DATABASE_FILE_EXT					"dldb"

#define TXD_FILE_VERSION		grcTextureDurango::RORC_VERSION

#elif RSG_ORBIS

#define DRAWABLE_FILE_EXT						"odr"
#define FRAGMENT_FILE_EXT						"oft"
#define DWD_FILE_EXT							"odd"
#define TXD_FILE_EXT							"otd"
#define ANIM_FILE_EXT							"ocd"
#define BOUND_FILE_EXT							"obn"
#define BOUNDSDICTIONARY_FILE_EXT				"obd"
#define BLENDSHAPE_FILE_EXT						"obs"
#define CLOTH_FILE_EXT							"old"
#define PMDICTIONARY_FILE_EXT					"opm"
#define EXPRESSIONSDICTIONARY_FILE_EXT			"oed"
#define FXLIST_FILE_EXT							"opt"
#define META_FILE_EXT							"omt"
#define MAPDATA_FILE_EXT						"omap"
#define MAPTYPES_FILE_EXT						"otyp"
#define NETWORK_DEF_FILE_EXT					"orf"
#define POSEMATCHER_FILE_EXT					"opdb"
#define NAVMESH_FILE_EXT						"onv"
#define NAVNODES_FILE_EXT						"ohn"
#define SCRIPT_FILE_EXT							"osc"
#define AUDMESH_FILE_EXT						"oam"
#define PLACEMENTS_FILE_EXT						"opl"
#define PATHS_FILE_EXT							"ond"
#define VEHICLERECORDING_FILE_EXT				"ovr"
#define WAYPOINTRECORDING_FILE_EXT				"owr"
#define MANIFEST_FILE_EXT						"omf"
#define HEIGHTMESH_FILE_EXT						"onh"
#define FRAMEFILTERDICTIONARY_FILE_EXT			"ofd"
#define TEXT_DATABASE_FILE_EXT					"oldb"

#define TXD_FILE_VERSION		grcTextureGNM::RORC_VERSION

#elif __XENON

#define DRAWABLE_FILE_EXT						"xdr"
#define FRAGMENT_FILE_EXT						"xft"
#define DWD_FILE_EXT							"xdd"
#define TXD_FILE_EXT							"xtd"
#define ANIM_FILE_EXT							"xcd"
#define BOUND_FILE_EXT							"xbn"
#define BOUNDSDICTIONARY_FILE_EXT				"xbd"
#define BLENDSHAPE_FILE_EXT						"xbs"
#define CLOTH_FILE_EXT							"xld"
#define PMDICTIONARY_FILE_EXT					"xpm"
#define EXPRESSIONSDICTIONARY_FILE_EXT			"xed"
#define FXLIST_FILE_EXT							"xpt"
#define META_FILE_EXT							"xmt"
#define MAPDATA_FILE_EXT						"xmap"
#define MAPTYPES_FILE_EXT						"xtyp"
#define NETWORK_DEF_FILE_EXT					"xrf"
#define POSEMATCHER_FILE_EXT					"xpdb"
#define NAVMESH_FILE_EXT						"xnv"
#define NAVNODES_FILE_EXT						"xhn"
#define SCRIPT_FILE_EXT							"xsc"
#define AUDMESH_FILE_EXT						"xam"
#define PLACEMENTS_FILE_EXT						"xpl"
#define PATHS_FILE_EXT							"xnd"
#define VEHICLERECORDING_FILE_EXT				"xvr"
#define WAYPOINTRECORDING_FILE_EXT				"xwr"
#define MANIFEST_FILE_EXT						"xmf"
#define HEIGHTMESH_FILE_EXT						"xnh"
#define FRAMEFILTERDICTIONARY_FILE_EXT			"xfd"
#define TEXT_DATABASE_FILE_EXT					"xldb"

#define TXD_FILE_VERSION		grcTextureXenon::RORC_VERSION

#elif __PPU

#define DRAWABLE_FILE_EXT						"cdr"
#define FRAGMENT_FILE_EXT						"cft"
#define DWD_FILE_EXT							"cdd"
#define TXD_FILE_EXT							"ctd"
#define ANIM_FILE_EXT							"ccd"
#define BOUND_FILE_EXT							"cbn"
#define BOUNDSDICTIONARY_FILE_EXT				"cbd"
#define BLENDSHAPE_FILE_EXT						"cbs"
#define CLOTH_FILE_EXT							"cld"
#define PMDICTIONARY_FILE_EXT					"cpm"
#define EXPRESSIONSDICTIONARY_FILE_EXT			"ced"
#define FXLIST_FILE_EXT							"cpt"
#define META_FILE_EXT							"cmt"
#define MAPDATA_FILE_EXT						"cmap"
#define MAPTYPES_FILE_EXT						"ctyp"
#define NETWORK_DEF_FILE_EXT					"crf"
#define POSEMATCHER_FILE_EXT					"cpdb"
#define NAVMESH_FILE_EXT						"cnv"
#define NAVNODES_FILE_EXT						"chn"
#define SCRIPT_FILE_EXT							"csc"
#define AUDMESH_FILE_EXT						"cam"
#define PLACEMENTS_FILE_EXT						"cpl"
#define PATHS_FILE_EXT							"cnd"
#define VEHICLERECORDING_FILE_EXT				"cvr"
#define WAYPOINTRECORDING_FILE_EXT				"cwr"
#define MANIFEST_FILE_EXT						"cmf"
#define HEIGHTMESH_FILE_EXT						"cnh"
#define FRAMEFILTERDICTIONARY_FILE_EXT			"cfd"
#define TEXT_DATABASE_FILE_EXT					"cldb"

#define TXD_FILE_VERSION						grcTextureGCM::RORC_VERSION

#endif

// Platform independent patterns for file extensions
#define DRAWABLE_FILE_EXT_PATTERN				"#dr"
#define FRAGMENT_FILE_EXT_PATTERN				"#ft"
#define DWD_FILE_EXT_PATTERN					"#dd"
#define TXD_FILE_EXT_PATTERN					"#td"
#define ANIM_FILE_EXT_PATTERN					"#cd"
#define BOUND_FILE_EXT_PATTERN					"#bn"
#define BOUNDSDICTIONARY_FILE_EXT_PATTERN		"#bd"
#define BLENDSHAPE_FILE_EXT_PATTERN				"#bs"
#define CLOTH_FILE_EXT_PATTERN					"#ld"
#define PMDICTIONARY_FILE_EXT_PATTERN			"#pm"
#define EXPRESSIONSDICTIONARY_FILE_EXT_PATTERN	"#ed"
#define FXLIST_FILE_EXT_PATTERN					"#pt"
#define META_FILE_EXT_PATTERN					"#mt"
#define MAPDATA_FILE_EXT_PATTERN				"#map"
#define MAPTYPES_FILE_EXT_PATTERN				"#typ"
#define NETWORK_DEF_FILE_EXT_PATTERN			"#rf"
#define POSEMATCHER_FILE_EXT_PATTERN			"#pdb"
#define NAVMESH_FILE_EXT_PATTERN				"#nv"
#define NAVNODES_FILE_EXT_PATTERN				"#hn"
#define SCRIPT_FILE_EXT_PATTERN					"#sc"
#define AUDMESH_FILE_EXT_PATTERN				"#am"
#define PLACEMENTS_FILE_EXT_PATTERN				"#pl"
#define PATHS_FILE_EXT_PATTERN					"#nd"
#define VEHICLERECORDING_FILE_EXT_PATTERN		"#vr"
#define WAYPOINTRECORDING_FILE_EXT_PATTERN		"#wr"
#define MANIFEST_FILE_EXT_PATTERN				"#mf"
#define HEIGHTMESH_FILE_EXT_PATTERN				"#nh"
#define FRAMEFILTERDICTIONARY_FILE_EXT_PATTERN	"#fd"
#define TEXT_DATABASE_FILE_EXT_PATTERN			"#ldb"

// Platform independent file extensions
#define PI_DRAWABLE_FILE_EXT					"idr"
#define PI_DWD_FILE_EXT							"idd"
#define PI_FRAGMENT_FILE_EXT					"ift"
#define PI_TXD_FILE_EXT							"itd"
#define PI_BOUND_FILE_EXT						"ibn"
#define PI_BOUNDSDICTIONARY_FILE_EXT			"ibd"
#define PI_ANIM_FILE_EXT						"icd"
#define PI_BLENDSHAPE_FILE_EXT					"ibs"
#define PI_CLOTH_FILE_EXT						"ild"
#define PI_PMDICTIONARY_FILE_EXT				"ipm"
#define PI_EXPRESSIONSDICTIONARY_FILE_EXT		"ied"
#define PI_FXLIST_FILE_EXT						"ipt"
#define PI_MAPDATA_FILE_EXT						"imap"
#define PI_MAPTYPES_FILE_EXT					"ityp"
#define PI_NETWORK_DEF_FILE_EXT					"mrf"
#define PI_POSEMATCHER_FILE_EXT					"ipdb"
#define PI_AUDMESH_FILE_EXT						"iam"
#define PI_PLACEMENTS_FILE_EXT					"ipl"
#define PI_VEHICLERECORDING_FILE_EXT			"ivr"
#define PI_WAYPOINTRECORDING_FILE_EXT			"iwr"
#define PI_MANIFEST_FILE_EXT					"imf"
#define PI_HEIGHTMESH_FILE_EXT					"inh"
#define PI_FRAMEFILTERDICTIONARY_FILE_EXT		"ifd"
#define PI_TEXT_DATABASE_FILE_EXT				"ildb"
#define PI_CUTSCENE_FILE_EXT					"cut"
#define PI_SCALEFORM_FILE_EXT					"gfx"
#define PI_SCRIPT_FILE_EXT						"sco"
#define PI_PATHS_FILE_EXT						"nod"
#define PI_ARCHIVE_FILE_EXT						"rpf"

// Use this macro any time you need to iterate over all known extensions.  Define the CASE macro before
// invoking this, and #undef it when you're done.
#define FILEEXT_CASES \
	CASE(PI_ARCHIVE); \
	CASE(DRAWABLE); \
	CASE(FRAGMENT); \
	CASE(DWD); \
	CASE(TXD); \
	CASE(ANIM); \
	CASE(BOUND); \
	CASE(BOUNDSDICTIONARY); \
	CASE(BLENDSHAPE); \
	CASE(CLOTH); \
	CASE(PMDICTIONARY); \
	CASE(EXPRESSIONSDICTIONARY); \
	CASE(FXLIST); \
	CASE(META); \
	CASE(MAPDATA); \
	CASE(POSEMATCHER); \
	CASE(PI_NETWORK_DEF); \
	CASE(NAVMESH); \
	CASE(NAVNODES); \
	CASE(SCRIPT); \
	CASE(PI_CUTSCENE); \
	CASE(PI_SCALEFORM); \
	CASE(PLACEMENTS); \
	CASE(AUDMESH); \
	CASE(PATHS); \
	CASE(PI_PATHS);	\
	CASE(VEHICLERECORDING); \
	CASE(WAYPOINTRECORDING); \
	CASE(MAPTYPES); \
	CASE(MANIFEST); \
	CASE(HEIGHTMESH); \
	CASE(FRAMEFILTERDICTIONARY); \
	CASE(TEXT_DATABASE);
	

#endif // !INC_FILEEXTS_H_
