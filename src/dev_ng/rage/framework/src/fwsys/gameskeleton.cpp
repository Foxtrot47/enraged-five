//
// gameskeleton.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwsys/gameskeleton.h"

#include "fwsys/syschannel.h"
#include "streaming/streamingvisualize.h"
#include "streaming/streamingengine.h"
#include "system/timemgr.h"

namespace rage
{

gameSkeleton::gameSkeleton() :
m_InitModes(0)
, m_ShutdownModes(0)
, m_UpdateModes(0)
, m_UpdateGroupStackPointer(0)
#if __DEV
, m_RunDependenciesTest(false)
#endif // __DEV
{
    m_RegisteredSystemData.Reserve(DEFAULT_MAX_SYSTEMS_REGISTERED);
}

gameSkeleton::~gameSkeleton()
{
	if(m_InitModes)			// Check for NULL to avoid assert if we never got initialized.
	{
	    CleanupModeFromRoot(m_InitModes);
	}
	if(m_ShutdownModes)		// Check for NULL to avoid assert if we never got initialized.
	{
	    CleanupModeFromRoot(m_ShutdownModes);
	}
    CleanupUpdateModes();
}

void gameSkeleton::SetCurrentUpdateType(unsigned updateType)
{ 
    // Shouldn't be changing the current update type while update groups are
    // still on the stack, this indicates a PushUpdateGroup without a corresponsing PopUpdateGroup
    Assert(m_UpdateGroupStackPointer == 0);

    m_CurrentUpdateType = updateType; 
}

void gameSkeleton::RegisterInitSystem(fnInitFunction initFunction,
                                      atHashString systemName
									  PROFILER_ONLY(,Profiler::EventDescription* desc))
{
    if(AssertVerify(initFunction) &&
       AssertVerify(systemName.IsNotNull()) &&
       AssertVerify(m_CurrentDependencyLevel < MAX_DEPENDENCY_LEVELS))
    {
        unsigned    sysDataIndex = 0;
        systemData *sysData      = GetSystemDataByName(systemName, sysDataIndex);

        if(sysData)
        {
            Assert((initFunction == sysData->m_InitFunction) || (sysData->m_InitFunction == 0));
            Assertf((sysData->m_InitTypes & m_CurrentInitType) == 0, "This function has already been registered for this init type!");

            sysData->m_InitFunction = initFunction;
            sysData->m_InitTypes   |= m_CurrentInitType;
        }
        else
        {
            systemData newData(initFunction, 0, m_CurrentDependencyLevel, 0, m_CurrentInitType, 0, systemName);
            m_RegisteredSystemData.PushAndGrow(newData);

            sysData = GetSystemDataByName(systemName, sysDataIndex);
            Assert(sysData);
        }

		PROFILER_ONLY(sysData->m_EventDescription = desc);

        AddSysDataForMode(m_InitModes, m_CurrentInitType, sysDataIndex);
    }
}

void gameSkeleton::RegisterShutdownSystem(fnShutdownFunction shutdownFunction,
                                          const atHashString& systemName)
{
    if(AssertVerify(shutdownFunction) &&
       AssertVerify(systemName.IsNotNull()) &&
       AssertVerify(m_CurrentDependencyLevel < MAX_DEPENDENCY_LEVELS))
    {
        unsigned    sysDataIndex = 0;
        systemData *sysData      = GetSystemDataByName(systemName, sysDataIndex);

        if(sysData)
        {
            Assert((shutdownFunction == sysData->m_ShutdownFunction) || (sysData->m_ShutdownFunction == 0));
            Assertf((sysData->m_ShutdownTypes & m_CurrentShutdownType) == 0, "This function has already been registered for this shutdown type!");

            sysData->m_ShutdownFunction = shutdownFunction;
            sysData->m_ShutdownTypes   |= m_CurrentShutdownType;
        }
        else
        {
            systemData newData(0, shutdownFunction, 0, m_CurrentDependencyLevel, 0, m_CurrentShutdownType, systemName);
            m_RegisteredSystemData.PushAndGrow(newData);

            sysData = GetSystemDataByName(systemName, sysDataIndex);
            Assert(sysData);
        }

        AddSysDataForMode(m_ShutdownModes, m_CurrentShutdownType, sysDataIndex);
    }
}

void gameSkeleton::RegisterUpdateSystem(fnUpdateFunction updateFunction,
                                        atHashString systemName,
										PROFILER_ONLY(Profiler::EventDescription* profilerDescription,)
                                        bool             addTimebar,
                                        float            timebarBudget)
{
    if(AssertVerify(updateFunction) &&
       AssertVerify(systemName.IsNotNull()))
    {
        updateElement *element = rage_new updateElement(systemName, addTimebar, timebarBudget, updateFunction);

		#if USE_PROFILER
		element->m_ProfilerDescription = profilerDescription;
		#endif

        Assert(element);

        AddElementForUpdateMode(m_CurrentUpdateType, element);
    }
}

void gameSkeleton::PushUpdateGroup(atHashString groupName,
								   PROFILER_ONLY(Profiler::EventDescription* profilerDescription,)
                                   bool        addTimebar,
                                   float       timebarBudget)
{
    updateGroup *newGroup = rage_new updateGroup(groupName, addTimebar, timebarBudget);

#if USE_PROFILER
	newGroup->m_ProfilerDescription = profilerDescription;
#endif

    AddElementForUpdateMode(m_CurrentUpdateType, newGroup);

    // push the new group on the update group stack
    if(AssertVerify(m_UpdateGroupStackPointer < MAX_NESTED_UPDATE_GROUPS))
    {
        m_UpdateGroupStack[m_UpdateGroupStackPointer] = newGroup;
        m_UpdateGroupStackPointer++;
    }
}

void gameSkeleton::PopUpdateGroup()
{
    if(AssertVerify(m_UpdateGroupStackPointer > 0))
    {
        m_UpdateGroupStackPointer--;
        m_UpdateGroupStack[m_UpdateGroupStackPointer] = 0;
    }
}

void gameSkeleton::AddSysDataForMode(mode *&modeRoot, unsigned modeType, unsigned sysDataIndex)
{
    // find the mode of the specified type, or add it if it doesn't exist
    mode *prevMode = 0;
    mode *currMode = modeRoot;

    while(currMode && currMode->modeType != modeType)
    {
        prevMode = currMode;
        currMode = currMode->next;
    }

    if(!currMode)
    {
        if(prevMode == 0)
        {
            Assert(modeRoot == 0);
            currMode = modeRoot = rage_new mode(modeType);
        }
        else
        {
            Assert(prevMode->next == 0);
            currMode = prevMode->next = rage_new mode(modeType);
        }

        Assert(currMode);
    }

    // find the specified dependency level for the specified mode, or add it if it doesn't exist
    dependency *prevDep = 0;
    dependency *currDep = currMode->head;

    while(currDep && currDep->dependencyLevel < m_CurrentDependencyLevel)
    {
        prevDep = currDep;
        currDep = currDep->next;
    }

    if(!currDep)
    {
        if(prevDep == 0)
        {
            Assert(currMode->head == 0);
            currDep = currMode->head = rage_new dependency(m_CurrentDependencyLevel);
        }
        else
        {
            Assert(prevDep->next == 0);
            currDep = prevDep->next = rage_new dependency(m_CurrentDependencyLevel);
        }

        Assert(currDep);
    }
    else if(currDep->dependencyLevel != m_CurrentDependencyLevel)
    {
        dependency *newDep = rage_new dependency(m_CurrentDependencyLevel);

        if(prevDep == 0)
        {
            Assert(currDep == currMode->head);
            newDep->next = currDep;
            currMode->head = newDep;
        }
        else
        {
            newDep->next = currDep;
            prevDep->next = newDep;
        }

        currDep = newDep;
    }

    // add a reference to the system for the specified mode at the specified dependency level
    currDep->sysData.PushAndGrow(sysDataIndex);
}

const gameSkeleton::mode *gameSkeleton::GetMode(const mode *modeRoot, unsigned modeType) const
{
    const mode *currMode = modeRoot;

    while(currMode && currMode->modeType != modeType)
    {
        currMode = currMode->next;
    }

    return currMode;
}

void gameSkeleton::CleanupModeFromRoot(mode *&modeRoot)
{
    if(AssertVerify(modeRoot))
    {
        mode *currMode = modeRoot;

        while(currMode)
        {
            dependency *currDep = currMode->head;

            while(currDep)
            {
                dependency *prevDep = currDep;
                currDep = currDep->next;
                delete prevDep;
            }

            mode *prevMode = currMode;
            currMode = currMode->next;
            delete prevMode;
        }
    }
}

void gameSkeleton::CleanupUpdateModes()
{
    updateMode *currMode = m_UpdateModes;

    while(currMode)
    {
        updateMode *next = currMode->m_Next;
        delete currMode;
        currMode = next;
    }
}

void gameSkeleton::AddElementForUpdateMode(unsigned updateType, gameSkeleton::updateBase *element)
{
    // check if there are any update groups on the stack, if so we add the element to
    // the update group on top of the stack, otherwise it gets added to the end of the
    // current update modes list
    if(m_UpdateGroupStackPointer > 0)
    {
        updateGroup *group = m_UpdateGroupStack[m_UpdateGroupStackPointer -1];

        if(AssertVerify(group))
        {
            if(group->m_Head == 0)
            {
                group->m_Head = element;
            }
            else
            {
                updateBase *base = group->m_Head;

                while(base->m_Next)
                {
                    base = base->m_Next;
                }

                base->m_Next = element;
            }
        }
    }
    else
    {
        // find the update mode of the specified type, or add it if it doesn't exist
        updateMode *prevMode = 0;
        updateMode *currMode = m_UpdateModes;

        while(currMode && currMode->m_ModeType != updateType)
        {
            prevMode = currMode;
            currMode = currMode->m_Next;
        }

        if(!currMode)
        {
            if(prevMode == 0)
            {
                Assert(m_UpdateModes == 0);
                currMode = m_UpdateModes = rage_new updateMode(updateType);
            }
            else
            {
                Assert(prevMode->m_Next == 0);
                currMode = prevMode->m_Next = rage_new updateMode(updateType);
            }

            Assert(currMode);
        }

        // add the new update element to the end of the update mode's update list
        updateBase *prevElement = 0;
        updateBase *currElement = currMode->m_Head;

        while(currElement)
        {
            prevElement = currElement;
            currElement = currElement->m_Next;
        }

        if(prevElement == 0)
        {
            Assert(currElement == currMode->m_Head);
            currMode->m_Head = element;
        }
        else
        {
            Assert(prevElement->m_Next == 0);
            prevElement->m_Next = element;
        }
    }
}

void gameSkeleton::Init(unsigned initMode)
{
	//@@: location GAMESKELETON_INIT_GETMODE
    const mode *theMode = GetMode(m_InitModes, initMode);

    if(theMode)
    {
        const dependency *currDep = theMode->head;

        while(currDep)
        {
            int numSystems = currDep->sysData.GetCount();

            if(numSystems > 0)
            {
                for(int systemIndex = 0; systemIndex < numSystems; systemIndex++)
                {
                    int index = systemIndex;
#if __DEV
                    if(m_RunDependenciesTest)
                    {
                        index = numSystems - systemIndex - 1;
                    }
#endif // __DEV

                    if(AssertVerify(currDep->sysData[index] < static_cast<unsigned>(m_RegisteredSystemData.GetCount())))
                    {
                        const systemData &sysData = m_RegisteredSystemData[currDep->sysData[index]];

                        gsysDebug1("Initialising %s", sysData.m_SystemName.GetCStr());
						PF_START_STARTUPBAR(sysData.m_SystemName.GetCStr());
						PROFILER_CUSTOM_EVENT(sysData.m_EventDescription);

#if !__NO_OUTPUT
                        unsigned startTime = sysTimer::GetSystemMsTime();
#endif
						(*sysData.m_InitFunction)(initMode);
						strStreamingEngine::GetLoader().CallKeepAliveCallbackIfNecessary();
#if !__NO_OUTPUT
						unsigned initTime = sysTimer::GetSystemMsTime() - startTime;
#endif

                        gsysDebug1("Initialised %s : %dms", sysData.m_SystemName.GetCStr(), initTime);
                    }
                }
            }

            currDep = currDep->next;
        }
    }
}

void gameSkeleton::Shutdown(unsigned shutdownMode)
{
    const mode *theMode = GetMode(m_ShutdownModes, shutdownMode);

    if(theMode)
    {
        const dependency *currDep = theMode->head;

        while(currDep)
        {
            int numSystems = currDep->sysData.GetCount();

            if(numSystems > 0)
            {
                for(int systemIndex = 0; systemIndex < numSystems; systemIndex++)
                {
                    int index = systemIndex;
#if __DEV
                    if(m_RunDependenciesTest)
                    {
                        index = numSystems - systemIndex - 1;
                    }
#endif // __DEV
                    
                    if(AssertVerify(currDep->sysData[index] < static_cast<unsigned>(m_RegisteredSystemData.GetCount())))
                    {
                        const systemData &sysData = m_RegisteredSystemData[currDep->sysData[index]];

                        gsysDebug1("Shutting down %s", sysData.m_SystemName.GetCStr());

#if !__NO_OUTPUT
                        unsigned startTime = sysTimer::GetSystemMsTime();
#endif
						(*sysData.m_ShutdownFunction)(shutdownMode);
#if !__NO_OUTPUT
                        unsigned initTime = sysTimer::GetSystemMsTime() - startTime;
#endif

                        gsysDebug1("Shutting down %s : %dms", sysData.m_SystemName.GetCStr(), initTime);
                    }
                }
            }

            currDep = currDep->next;
        }
    }
}

void gameSkeleton::Update(unsigned updateModeType)
{
    updateMode *theUpdateMode = m_UpdateModes;

    while(theUpdateMode && theUpdateMode->m_ModeType != updateModeType)
    {
        theUpdateMode = theUpdateMode->m_Next;
    }

    if(theUpdateMode)
    {
        theUpdateMode->Update();
    }
}

gameSkeleton::systemData *gameSkeleton::GetSystemDataByName(const atHashString& name, unsigned &index)
{
    int numSystems = m_RegisteredSystemData.GetCount();

    systemData *data = 0;

    if(numSystems > 0)
    {
        for(int systemIndex = 0; systemIndex < numSystems && !data; systemIndex++)
        {
            systemData &sysData = m_RegisteredSystemData[systemIndex];

            if(name.GetHash() == sysData.m_SystemName.GetHash())
            {
                data  = &sysData;
                index = systemIndex;
            }
        }
    }

    return data;
}

} // namespace rage
