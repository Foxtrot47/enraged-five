// 
// fwsys/syschannel.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef INC_SYSTEM_CHANNEL_H
#define INC_SYSTEM_CHANNEL_H

#include "diag/channel.h"
#include "system/param.h"

RAGE_DECLARE_CHANNEL(sys)

#define gsysDebug1(fmt, ...)       RAGE_DEBUGF1(sys, fmt, ##__VA_ARGS__)
#define gsysDebug2(fmt, ...)       RAGE_DEBUGF2(sys, fmt, ##__VA_ARGS__)
#define gsysDebug3(fmt, ...)       RAGE_DEBUGF3(sys, fmt, ##__VA_ARGS__)
#define gsysError(fmt, ...)        RAGE_ERRORF(sys, fmt, ##__VA_ARGS__)
#define gsysWarning(fmt, ...)      RAGE_WARNINGF(sys, fmt, ##__VA_ARGS__)
#define gsysAssertf(cond,fmt,...)  RAGE_ASSERTF(sys, cond, fmt, ##__VA_ARGS__)
#define gsysAssert(cond)           RAGE_ASSERT(sys, cond)
#define gsysVerifyf(cond,fmt,...)  RAGE_VERIFYF(sys, cond, fmt, ##__VA_ARGS__)
#define gsysVerify(cond)           RAGE_VERIFY(sys,cond)

#endif // INC_SYSTEM_CHANNEL_H 

