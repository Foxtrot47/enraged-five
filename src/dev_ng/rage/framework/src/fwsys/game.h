//
// fwsys/game.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef FWSYS_GAME_H__
#define FWSYS_GAME_H__

namespace rage
{

class fwGameInterface
{
public:
	virtual ~fwGameInterface() {}
	
	virtual void RegisterStreamingModules() {}
};

class fwGame 
{
public:
	static void InitClass(fwGameInterface* gameInterface);
	static void ShutdownClass();

	static void RegisterStreamingModules();
	
	static void Init();
	static void Update();
	static void Shutdown();
	
private:
	bool WantToExit();
};

} // namespace rage

#endif // FWSYS_GAME_H__