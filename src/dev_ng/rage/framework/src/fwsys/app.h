//
// fwsys/app.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef FWSYS_APP_H__
#define FWSYS_APP_H__

#include "fsm.h"

namespace rage
{

class fwApp : public fwFsm
{
public:

protected:
	
};

} // namespace rage

#endif // FWSYS_APP_H__