//
// fwsys/timer.h 
//
// Copyright (C) 1999-2011 Rockstar North.  All Rights Reserved. 
//

#ifndef _FWSYS_TIMER_H_
#define _FWSYS_TIMER_H_

// includes
#include "file/remote.h"
#include "system/timemgr.h"

#define TIMER_PRINTSTACKTRACE  (!__NO_OUTPUT)

#define FRAME_LIMITER_MIN_FRAME_TIME (1.0f/30.0f)		// The game will never run faster than this. CSystem::EndRender() waits until this much time has expired.

#if __ASSERT
#define CHECKOUT()	{\
						if( !(sm_IsNetworkGame == false || GetTimeWarpActive() == 1.0f) && sm_TimescaleIsBeingModifiedByScript == false )\
						{\
							Displayf("fwTimer::sm_fTimeScaleSpecialAbiltity = %f", sm_aTimeScales[eTST_SpecialAbility]);\
							Displayf("fwTimer::sm_fTimeScaleUI = %f", sm_aTimeScales[eTST_UI]);\
							Displayf("fwTimer::sm_fTimeScaleScript = %f", sm_aTimeScales[eTST_Script]);\
							Displayf("fwTimer::sm_fTimeScaleCamera = %f", sm_aTimeScales[eTST_Camera]);\
							Displayf("fwTimer::sm_fTimeScale = %f", sm_fTimeScale);\
							Assertf(0,"fwTimer:: Timescale should be 1.0f in MP!");\
						}\
					}
#else	//__ASSERT
#define CHECKOUT()
#endif	//__ASSERT

#if defined(__RGSC_DLL) && __RGSC_DLL
#define RSG_LOST_FOCUS_SUPPORT 0
#else
#define RSG_LOST_FOCUS_SUPPORT (RSG_PC)
#endif

namespace rage {

/////////////////////////////////////////////////////////////////////////////////
// This class handles the tracking of time for a specific time domain, for
// example system (un-pausable) time vs. game (pausable) time.
/////////////////////////////////////////////////////////////////////////////////
class fwTimeSet
{
	 friend class fwTimer;
public:
	void	Init() { m_Time = m_frameCount = m_TimePrevious = 0;
					 m_StepInSeconds = 0.05f; m_StepInMs = (u32)(m_StepInSeconds * 1000.0f);
					 m_InvStep = 20.0f; m_TimeRemainderMs = 0; }
	void	SetTimeInMilliseconds(u32 time) { m_Time = time; m_TimePrevious = m_Time-1 < m_TimePrevious? m_Time-1 : m_TimePrevious; }
	u32		GetTimeInMilliseconds() { return m_Time; }
	void	SetFrameCount(u32 frameCount) { m_frameCount = frameCount; }
	u32		GetFrameCount() { return m_frameCount; }
	u32		GetPrevElapsedTimeInMilliseconds() { return m_TimePrevious; }
	float	GetTimeStep() { return m_StepInSeconds; }
	void	SetTimeStepInSeconds(float time)	{	m_StepInSeconds = time; m_StepInMs = (u32)(m_StepInSeconds * 1000.0f); }
	void	SetTimeStepInMilliseconds(u32 time)	{	m_StepInSeconds = (float)time / 1000.0f; m_StepInMs = (u32)(m_StepInSeconds * 1000.0f); }
	u32		GetTimeStepInMilliseconds() { return m_StepInMs; }  // The old version could return negative values in network games. { return (m_Time-m_TimePrevious); }
	float	GetInvTimeStep() { return m_InvStep; }

	// HACK - talk to klaas before changing this
	void	Knock(size_t valueThatIsAtLeast64); 
	// HACK - talk to klaas before changing this

	void	Update(float stepInSeconds, float scale, bool clip);

protected:
	u32		m_Time;				// Elapsed time since the start of the game in milliseconds.
	u32		m_frameCount;		// Elapsed frames since the start of the game. The frame counter. Increases by 1 every frame.
	u32		m_TimePrevious;		// Elapsed time of the previous frame.
	float	m_StepInSeconds;	// The step in seconds from the last frame to the current one
	u32		m_StepInMs;			// The step in ms from the last frame to the current one
	float	m_InvStep;			// Inverse time step (in 1 / seconds)
	float	m_TimeRemainderMs;	// Fractional amount of milliseconds
};


/////////////////////////////////////////////////////////////////////////////////
// This class handles the tracking of time for the various in game systems and
// is used to handle network time synchronization.
/////////////////////////////////////////////////////////////////////////////////
class fwTimer
{
public:
	// functions
	static void Init();
	static void InitLevel();

	static void Shutdown()												{}
	static void Update();

	static void Stop()										/* TO DO */	{}
	static void Suspend()									/* TO DO */	{}
	static void Resume()									/* TO DO */	{}

	static const sysTimer& GetFrameTimer() { return sm_sysTimer; }

	// Access functions for the regular game timer
	inline static void SetTimeInMilliseconds(u32 time) { sm_gameTime.SetTimeInMilliseconds(time); }
	inline static u32 GetTimeInMilliseconds() { return sm_gameTime.GetTimeInMilliseconds(); }		
	inline static void SetFrameCount(u32 frameCount) { sm_gameTime.SetFrameCount(frameCount); }
	inline static u32 GetFrameCount() { return sm_gameTime.GetFrameCount(); }
	inline static u32 GetPrevElapsedTimeInMilliseconds() { return sm_gameTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetTimeStep() { return sm_gameTime.GetTimeStep(); }
	inline static u32 GetTimeStepInMilliseconds() { return sm_gameTime.GetTimeStepInMilliseconds(); }
	inline static float GetTimeStepInMillsecondsHighRes() { return ( GetTimeStep() * 1000.0f ); }
	inline static float GetInvTimeStep() { return sm_gameTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetGameTimer() { return sm_gameTime; }

	// Access functions for the regular game timer, that is not paused
	inline static void SetNonPausedTimeInMilliseconds(u32 time) { sm_nonPausedGameTime.SetTimeInMilliseconds(time); }
	inline static u32 GetNonPausedTimeInMilliseconds() { return sm_nonPausedGameTime.GetTimeInMilliseconds(); }		
	inline static void SetNonPausedFrameCount(u32 frameCount) { sm_nonPausedGameTime.SetFrameCount(frameCount); }
	inline static u32 GetNonPausedFrameCount() { return sm_nonPausedGameTime.GetFrameCount(); }
	inline static u32 GetNonPausedPrevElapsedTimeInMilliseconds() { return sm_nonPausedGameTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetNonPausedTimeStep() { return sm_nonPausedGameTime.GetTimeStep(); }
	inline static u32 GetNonPausedTimeStepInMilliseconds() { return sm_nonPausedGameTime.GetTimeStepInMilliseconds(); }
	inline static float GetNonPausedInvTimeStep() { return sm_nonPausedGameTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetNonPausedGameTimer() { return sm_nonPausedGameTime; }

	// Access functions for the system time (non scaled non clipped)
	inline static void SetSystemTimeInMilliseconds(u32 time) { sm_systemTime.SetTimeInMilliseconds(time); }
	inline static u32 GetSystemTimeInMilliseconds() { return sm_systemTime.GetTimeInMilliseconds(); }	
	inline static void SetSystemFrameCount(u32 frameCount) { sm_systemTime.SetFrameCount(frameCount); }
	inline static u32 GetSystemFrameCount() { return sm_systemTime.GetFrameCount(); }
	inline static u32 GetSystemPrevElapsedTimeInMilliseconds() { return sm_systemTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetSystemTimeStep() { return sm_systemTime.GetTimeStep(); }
	inline static u32 GetSystemTimeStepInMilliseconds() { return sm_systemTime.GetTimeStepInMilliseconds(); }
	inline static float GetSystemInvTimeStep() { return sm_systemTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetSystemTimer() { return sm_systemTime; }

#ifdef GTA_REPLAY_RAGE
	// Access functions for the replay pausable time (non scaled non clipped)
	inline static void SetReplayTimeInMilliseconds(u32 time) { sm_replayTimeScaledNonClippedPausable.SetTimeInMilliseconds(time); }
	inline static u32 GetReplayTimeInMilliseconds() { return sm_replayTimeScaledNonClippedPausable.GetTimeInMilliseconds(); }			
	inline static u32 GetReplayPrevElapsedTimeInMilliseconds() { return sm_replayTimeScaledNonClippedPausable.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetReplayTimeStep() { return sm_replayTimeScaledNonClippedPausable.GetTimeStep(); }
	inline static u32 GetReplayTimeStepInMilliseconds() { return sm_replayTimeScaledNonClippedPausable.GetTimeStepInMilliseconds(); }
	inline static float GetReplayInvTimeStep() { return sm_replayTimeScaledNonClippedPausable.GetInvTimeStep(); }
	inline static fwTimeSet &GetReplayTime()	{ return sm_replayTimeScaledNonClippedPausable; }
#endif // GTA_REPLAY

	// Access functions for the scaled but non clipped timer
	inline static void SetTimeInMilliseconds_ScaledNonClipped(u32 time) { sm_scaledNonClippedTime.SetTimeInMilliseconds(time); }
	inline static u32 GetTimeInMilliseconds_ScaledNonClipped() { return sm_scaledNonClippedTime.GetTimeInMilliseconds(); }			
	inline static void SetFrameCount_ScaledNonClipped(u32 frameCount) { sm_scaledNonClippedTime.SetFrameCount(frameCount); }
	inline static u32 GetFrameCount_ScaledNonClipped() { return sm_scaledNonClippedTime.GetFrameCount(); }
	inline static u32 GetPrevElapsedTimeInMilliseconds_ScaledNonClipped() { return sm_scaledNonClippedTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetTimeStep_ScaledNonClipped() { return sm_scaledNonClippedTime.GetTimeStep(); }
	inline static u32 GetTimeStepInMilliseconds_ScaledNonClipped() { return sm_scaledNonClippedTime.GetTimeStepInMilliseconds(); }
	inline static float GetInvTimeStep_ScaledNonClipped() { return sm_scaledNonClippedTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetScaledNonClippedTimer() { return sm_scaledNonClippedTime; }

	// Access functions for the non scaled but clipped timer
	inline static void SetTimeInMilliseconds_NonScaledClipped(u32 time) { sm_nonScaledClippedTime.SetTimeInMilliseconds(time); }
	inline static u32 GetTimeInMilliseconds_NonScaledClipped() { return sm_nonScaledClippedTime.GetTimeInMilliseconds(); }			
	inline static void SetFrameCount_NonScaledClipped(u32 frameCount) { sm_nonScaledClippedTime.SetFrameCount(frameCount); }
	inline static u32 GetFrameCount_NonScaledClipped() { return sm_nonScaledClippedTime.GetFrameCount(); }
	inline static u32 GetPrevElapsedTimeInMilliseconds_NonScaledClipped() { return sm_nonScaledClippedTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetTimeStep_NonScaledClipped() { return sm_nonScaledClippedTime.GetTimeStep(); }
	inline static u32 GetTimeStepInMilliseconds_NonScaledClipped() { return sm_nonScaledClippedTime.GetTimeStepInMilliseconds(); }
	inline static float GetInvTimeStep_NonScaledClipped() { return sm_nonScaledClippedTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetNonScaledClippedTimer() { return sm_nonScaledClippedTime; }

	// Access functions for the cam timer
	inline static void SetCamTimeInMilliseconds(u32 time) { sm_camTime.SetTimeInMilliseconds(time); }
	inline static u32 GetCamTimeInMilliseconds() { return sm_camTime.GetTimeInMilliseconds(); }		
	inline static void SetCamFrameCount(u32 frameCount) { sm_camTime.SetFrameCount(frameCount); }
	inline static u32 GetCamFrameCount() { return sm_camTime.GetFrameCount(); }
	inline static u32 GetCamPrevElapsedTimeInMilliseconds() { return sm_camTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float GetCamTimeStep() { return sm_camTime.GetTimeStep(); }
	inline static u32 GetCamTimeStepInMilliseconds() { return sm_camTime.GetTimeStepInMilliseconds(); }
	inline static float GetCamInvTimeStep() { return sm_camTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetCamTimer() { return sm_camTime; }

	// Access functions for the NonPausable cam timer
	inline static void SetNonPausableCamTimeInMilliseconds(u32 time) { sm_nonPausableCamTime.SetTimeInMilliseconds(time); }
	inline static u32 GetNonPausableCamTimeInMilliseconds() { return sm_nonPausableCamTime.GetTimeInMilliseconds(); }			
	inline static void SetNonPausableCamFrameCount(u32 frameCount) { sm_nonPausableCamTime.SetFrameCount(frameCount); }
	inline static u32 GetNonPausableCamFrameCount() { return sm_nonPausableCamTime.GetFrameCount(); }	// NB: This is used by anti-tamper, not for general consumption!
	inline static u32 GetNonPausableCamPrevElapsedTimeInMilliseconds() { return sm_nonPausableCamTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float	GetNonPausableCamTimeStep() { return sm_nonPausableCamTime.GetTimeStep(); }
	inline static u32 GetNonPausableCamTimeStepInMilliseconds() { return sm_nonPausableCamTime.GetTimeStepInMilliseconds(); }
	inline static float GetNonPausableCamInvTimeStep() { return sm_nonPausableCamTime.GetInvTimeStep(); }
	inline static fwTimeSet& GetNonPausableCamTimer() { return sm_nonPausableCamTime; }

	// Access functions for the non pausable, non scaled and clipped timer
	inline static void SetTimeInMilliseconds_NonPausedNonScaledClipped(u32 time) { sm_nonPausedNonScaledClippedTime.SetTimeInMilliseconds(time); }
	inline static u32 GetTimeInMilliseconds_NonPausedNonScaledClipped() { return sm_nonPausedNonScaledClippedTime.GetTimeInMilliseconds(); }			
	inline static void SetFrameCount_NonPausedNonScaledClipped(u32 frameCount) { sm_nonPausedNonScaledClippedTime.SetFrameCount(frameCount); }
	inline static u32 GetFrameCount_NonPausedNonScaledClipped() { return sm_nonPausedNonScaledClippedTime.GetFrameCount(); }
	inline static u32 GetPrevElapsedTimeInMilliseconds_NonPausedNonScaledClipped() { return sm_nonPausedNonScaledClippedTime.GetPrevElapsedTimeInMilliseconds(); }
	inline static float	GetTimeStep_NonPausedNonScaledClipped() { return sm_nonPausedNonScaledClippedTime.GetTimeStep(); }
	inline static u32 GetTimeStepInMilliseconds_NonPausedNonScaledClipped() { return sm_nonPausedNonScaledClippedTime.GetTimeStepInMilliseconds(); }
	inline static float GetInvTimeStep_NonPausedNonScaledClipped() { return sm_nonPausedNonScaledClippedTime.GetInvTimeStep(); }

	// Access functions for timestep used by rage physics.
	inline static void SetRagePhysicsUpdateTimeStep(const float fRagePhysicsUpdateTimeStep) {sm_fRagePhysicsUpdateTimeStep=fRagePhysicsUpdateTimeStep;}
	inline static float GetRagePhysicsUpdateTimeStep() {return sm_fRagePhysicsUpdateTimeStep;}

	inline static float GetTimeWarpScript()										{ return sm_aTimeScales[eTST_Script]; }
	inline static void SetTimeWarpScript(float fTimeScale)						{ sm_aTimeScales[eTST_Script] = fTimeScale; sm_fTimeScale = GetLowestTimeWarp(); CHECKOUT() }

	inline static float GetTimeWarpCamera()										{ return sm_aTimeScales[eTST_Camera]; }
	inline static void SetTimeWarpCamera(float fTimeScale)						{ sm_aTimeScales[eTST_Camera] = fTimeScale; sm_fTimeScale = GetLowestTimeWarp(); CHECKOUT() }

	inline static float GetTimeWarpUI()											{ return sm_aTimeScales[eTST_UI]; }
	inline static void SetTimeWarpUI(float fTimeScale)							{ sm_aTimeScales[eTST_UI] = fTimeScale; sm_fTimeScale = GetLowestTimeWarp(); CHECKOUT() }

	inline static float GetTimeWarpSpecialAbility()								{ return sm_aTimeScales[eTST_SpecialAbility]; }
	inline static void SetTimeWarpSpecialAbility(float fTimeScale)				{ sm_aTimeScales[eTST_SpecialAbility] = fTimeScale; sm_fTimeScale = GetLowestTimeWarp(); CHECKOUT() }

	inline static float GetTimeWarp()											{ return sm_fTimeScale; }
	inline static void SetTimeWarp(float fTimeScale)							{ sm_fTimeScale = fTimeScale; CHECKOUT() } // will be used as long as neither the camera, ui, or script are tinkering with it

	inline static void SetTimeWarpAll(float fTimeScale)							
	{
		sm_fTimeScale = fTimeScale;
		for(int i = 0; i < eTST_Max; ++i)
		{
			sm_aTimeScales[i] = fTimeScale;
		}
		CHECKOUT()
	}

	inline static float GetLowestTimeWarp();
	inline static float GetTimeWarpActive();

	static bool IsGamePaused(void);
	static bool ShouldScriptsBePaused(void);
	static bool AreScriptPausedToo(void);

	static void StartUserPause(bool bScriptsToo = true);
	static void EndUserPause(void);
	static bool IsUserPaused(void);

	static void StartScriptPause(void);
	static void EndScriptPause(void);
	static bool IsScriptPaused(void);

	static void StartSystemPause(void);
	static void EndSystemPause(void);
	static bool IsSystemPaused(void);

#if RSG_LOST_FOCUS_SUPPORT
	static void StartLostFocusPause(void);
	static void EndLostFocusPause(void);
	static bool IsLostFocusPaused(void);
#endif // RSG_LOST_FOCUS_SUPPORT

	static void StoreCurrentTime();
	static void RestoreCurrentTime();

	static void SetSingleStepThisFrame()		{ sm_bSingleStepThisFrame = true; }
	static void SetWantsToSingleStepNextFrame()	{ sm_bWantsToSingleStepNextFrame = true; }
	static bool GetSingleStepThisFrame()		{ return sm_bSingleStepThisFrame; }
	static bool GetWantsToSingleStepNextFrame()	{ return sm_bWantsToSingleStepNextFrame; }

	static float GetMinimumFrameTime() {return sm_fMinimumFrameTime;}
	static float GetMaximumFrameTime() {return sm_fMaximumFrameTime;}

#if !__FINAL
	static bool GetForceMinimumFrameRate() {return sm_bForceMinimumFrameRate;}
	static bool GetForceMaximumFrameRate() {return sm_bForceMaximumFrameRate;}
	static bool GetForceUnevenFrameRate() {return sm_bForceUnevenFrameRate;}
	static float GetDebugTimeScale() {return sm_fDebugTimeScale;}
	static void SetDebugTimeScale(float fTimeScale) {sm_fDebugTimeScale = fTimeScale;}
	static void SetFixedFrameTime(float fFrameTime) { sm_fFixedFrameTime = fFrameTime; }
	static float GetFixedFrameTimeScale() { return sm_fFixedFrameTimeScale; }

	static void SetForcedFps(float fps);
	static void UnsetForcedFps();
#endif
#if !__FINAL
	// Remap internal callbacks to game-specific debug keyboard.
	static void SetKeyboardCallbacks(bool (*keyJustDown)(int,const char*),bool (*keyDown)(int,const char*));
	// Override the default keys used to control the game's time from the debug keyboard. Set as zero to disable.
	static void SetKey_Pause(const int iKey);				// default = KEY_PAUSE
	static void SetKey_SingleStep(const int iKey);			// default = KEY_RIGHT (when paused)
	static void SetKey_TimeScaleModifier(const int iKey);	// default = KEY_RCONTROL
	static void SetKey_TimeScaleInc(const int iKey);		// default = KEY_RIGHT (with modifier key)
	static void SetKey_TimeScaleDec(const int iKey);		// default = KEY_LEFT (with modifier key)

	static void SetDebugPause(const bool isDebugPause) { sm_DebugPause = isDebugPause; }
	static void SetDeferredDebugPause(const bool isDeferredDebugPause) { sm_DeferredDebugPause = isDeferredDebugPause ? DDP_PendingOn : DDP_PendingOff; }

	// PURPOSE
	//	Returns true if the game was paused via the debug keyboard, rather than a normal user pause
	static bool IsDebugPause() { return sm_DebugPause; }
#endif	//!__FINAL

#if __ASSERT
	static void SetIsNetworkGame(bool bVal) { sm_IsNetworkGame = bVal; }
	static void SetTimescaleIsBeingModifiedByScript(bool bVal) { sm_TimescaleIsBeingModifiedByScript = bVal; }
#endif//__ASSERT

private:

#	if !TIMER_PRINTSTACKTRACE
	static __forceinline void PrintStackTrace(const char *) {}
#	else
	static void PrintStackTrace(const char *pMessageToDisplay);
#	endif

	static sysTimer	sm_sysTimer;		// Low level timer from rage system/timer.h

	static float sm_fMinimumFrameTime;
	static float sm_fMaximumFrameTime;
	static float sm_fFixedFrameTime;
	static float sm_fFixedFrameTimeScale;
	static bool sm_bForceMinimumFrameRate;
	static bool sm_bForceMaximumFrameRate;
	static bool sm_bForceUnevenFrameRate;

	static bool sm_bUserPause;
	static bool sm_bScriptPause;
	static bool sm_bSystemPause;
#if RSG_LOST_FOCUS_SUPPORT
	static bool sm_bLostFocusPause;
#endif

	static bool sm_bScriptToBePaused;
	static bool sm_bScriptToBeUnpaused;

#if __BANK
	static bool sm_bPauseOnAsserts;
	static bool sm_bPauseScriptsToo;
	static bool sm_bReadOnlyUserPauseWidgetValue;
	static bool sm_bReadOnlyScriptPauseWidgetValue;
#endif
#if !__FINAL
	enum DeferredDebugPause
	{
		DDP_None,
		DDP_PendingOn,
		DDP_PendingOff
	};

	static int sm_iKey_Pause;
	static int sm_iKey_SingleStep;
	static int sm_iKey_TimeScaleModifier;
	static int sm_iKey_TimeScaleInc;
	static int sm_iKey_TimeScaleDec;
	static DeferredDebugPause sm_DeferredDebugPause;
	static bool sm_DebugPause;
#endif

#if __ASSERT
	static bool sm_IsNetworkGame;
	static bool sm_TimescaleIsBeingModifiedByScript;
#endif

	static bool sm_bWantsToSingleStepNextFrame;
	static bool sm_bSingleStepThisFrame;

	// There are 4 different sets of timers:
	// The time can be scaled (slow motion) as determined by the game or script.
	// The time can be clipped to avoid very quick and very slow frames.
	// Clipping always happens after scaling otherwise the scaling wouldn't work.

	// System time: This is unscaled / unclipped
	static fwTimeSet	sm_systemTime;

#ifdef GTA_REPLAY_RAGE
	// Replay time: This is scaled / unclipped / pausable
	static	fwTimeSet	sm_replayTimeScaledNonClippedPausable;
#endif // GTA_REPLAY

	// Game time: This is scaled and clipped 
	static fwTimeSet	sm_gameTime;

	// Game Non Paused time: This is scaled and clipped, but does not pause when sm_gameTime does
	static fwTimeSet	sm_nonPausedGameTime;

	// ... time: This is scaled but not clipped
	static fwTimeSet	sm_scaledNonClippedTime;

	// ... time: This is not scaled but it is clipped
	static fwTimeSet	sm_nonScaledClippedTime;

	// Cam time: Identical to game time except that the network doesn't screw with it!
	static fwTimeSet	sm_camTime;

	// Cam time: Identical to game time except that the network doesn't screw with it! and it is never paused due to a user pause.
	static fwTimeSet	sm_nonPausableCamTime;

	// ... time: This is non-pausable, not scaled and it is clipped
	static fwTimeSet	sm_nonPausedNonScaledClippedTime;

	// The scale used for slow motion (1.0f = normal run of time)
	static float	sm_fTimeScale;			// the timewarp being used

	enum eTimeScaleType 
	{
		eTST_Camera = 0,
		eTST_Script,
		eTST_UI,
		eTST_SpecialAbility,
		eTST_Max
	};

	static float sm_aTimeScales[eTST_Max];
	
	// If true, the game time scale is applied to the camera times
	static bool		sm_bShouldApplyGameTimeScaleToCameras;

	// additional time scale for debug purposes
	static float	sm_fDebugTimeScale;

	// Synced time in milliseconds - this will be a timer synced over the network in network games, yet still available in single player games
    static u32   sm_syncedTimeInMilliseconds;

	// The following are the values that get stored going into frontend and restored coming out.
	static bool		sm_bTimeStored;
	static fwTimeSet	sm_systemTime_Stored;
#ifdef GTA_REPLAY_RAGE
	static	fwTimeSet	sm_replayTimeScaledNonClippedPausable_Stored;
#endif // GTA_REPLAY
	static fwTimeSet	sm_gameTime_Stored;
	static fwTimeSet	sm_camTime_Stored;
	static fwTimeSet	sm_nonPausableCamTime_Stored;
	static fwTimeSet	sm_scaledNonClippedTime_Stored;
	static fwTimeSet	sm_nonScaledClippedTime_Stored;

	//Timestep passed to rage physics update.
	static float sm_fRagePhysicsUpdateTimeStep;
};


inline bool fwTimer::ShouldScriptsBePaused(void)
{
	if (sm_bUserPause || sm_bSystemPause
#if RSG_LOST_FOCUS_SUPPORT
		|| sm_bLostFocusPause
#endif
		)
	{
#if __BANK
		if (sm_bPauseScriptsToo)
#endif
		{
			return true;
		}
	}
	return false;
};

inline bool fwTimer::AreScriptPausedToo(void)
{
#if __BANK
	return sm_bPauseScriptsToo;
#else
	return false;
#endif
}

inline bool fwTimer::IsGamePaused(void)
{
#if !__BANK
	return sm_bScriptPause|sm_bUserPause|sm_bSystemPause
#if RSG_LOST_FOCUS_SUPPORT
	|sm_bLostFocusPause
#endif
	;
#else

	if(sm_bSystemPause)
	{
		return true;
	}

	if (sm_bScriptPause)
	{
		return true;
	}
#if RSG_LOST_FOCUS_SUPPORT
	if (sm_bLostFocusPause)
	{
		return true;
	}
#endif
	if(sm_bUserPause)	//	 || ms_bGamePause)
	{
		return !sm_bSingleStepThisFrame;
	}

	if (fiIsShowingMessageBox && sm_bPauseOnAsserts)
	{
		return true;
	}

	return false;
#endif // __BANK
}

float fwTimer::GetLowestTimeWarp()
{
	float fLowest = sm_aTimeScales[0];
	for(int i = 1; i < eTST_Max; ++i)
	{
		if(sm_aTimeScales[i] < fLowest)
			fLowest = sm_aTimeScales[i];
	}

	return fLowest;
}

float fwTimer::GetTimeWarpActive()
{
	float fLowest = GetLowestTimeWarp();
	if(fLowest < 1.0f)
	{
		return fLowest;
	}

	return sm_fTimeScale;
}

}	// namespace rage

#endif // _FWSYS_TIMER_H_
