//
// fwsys/timer.hcpp
//
// Copyright (C) 1999-2011 Rockstar North.  All Rights Reserved. 
//

#include "timer.h"

// Rage includes
#include "math/amath.h"

#if TIMER_PRINTSTACKTRACE
#include "script/thread.h"
#include "system/stack.h"
#endif	//	TIMER_PRINTSTACKTRACE

#if !__FINAL
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "cutscene/cutsmanager.h"
#include "grcore/debugdraw.h"
#include "input/keyboard.h"
#include "input/keys.h"
#include "input/mapper.h"
#include "system/param.h"
#include "system/threadtype.h"
#endif
#include "grcore/setup.h"
#include "streaming/streamingvisualize.h"
#include "system/hangdetect.h"

//Framework includes
#if !__RESOURCECOMPILER
#include "fwnet/netinterface.h"
#endif

#if !__FINAL
PARAM(timescale, "time step scale value");
PARAM(minframetime, "Maximum size of one frame in seconds");
PARAM(maxframetime, "Maximum size of one frame in seconds");
PARAM(maxfps, "Maximum frames per second");
PARAM(forcedfps, "Force simulated fps to the number of frames, will dilute time ");
PARAM(fixedframetime, "Force each frame to advance by a fixed number of milliseconds (specified in fps)");
PARAM(camsusegametimescale, "Apply the game time scale to the camera times");
PARAM(pauseonassert, "Pauses the game when an assert happens");
PARAM(stallThreshold, "Delay we have to wait until POSSIBLE STALL message is displayed");
#endif

namespace rage {

bank_bool	s_newUpdate = __WIN32PC;
bank_bool	s_retainMsRemainder = true;

sysTimer	fwTimer::sm_sysTimer;		// Low level timer from rage system/timer.h

			// The actual timer info.
fwTimeSet	fwTimer::sm_systemTime;
#ifdef GTA_REPLAY_RAGE
fwTimeSet	fwTimer::sm_replayTimeScaledNonClippedPausable;
#endif // GTA_REPLAY
fwTimeSet	fwTimer::sm_gameTime;
fwTimeSet	fwTimer::sm_nonPausedGameTime;
fwTimeSet	fwTimer::sm_camTime;
fwTimeSet	fwTimer::sm_nonPausableCamTime;
fwTimeSet	fwTimer::sm_scaledNonClippedTime;
fwTimeSet	fwTimer::sm_nonScaledClippedTime;
fwTimeSet	fwTimer::sm_nonPausedNonScaledClippedTime;
float		fwTimer::sm_fTimeScale		= 1.0f;

float		fwTimer::sm_aTimeScales[eTST_Max] = { 1.0f, 1.0f, 1.0f, 1.0f };

bool		fwTimer::sm_bShouldApplyGameTimeScaleToCameras = false;

#if !__FINAL
float						fwTimer::sm_fDebugTimeScale = 1.0f;
bool						fwTimer::sm_DebugPause = false;
fwTimer::DeferredDebugPause	fwTimer::sm_DeferredDebugPause = DDP_None;
#endif

#if __ASSERT
bool		fwTimer::sm_IsNetworkGame = false;
bool		fwTimer::sm_TimescaleIsBeingModifiedByScript = false;
#endif

// Stored values (for when the game is in frontend)
fwTimeSet	fwTimer::sm_systemTime_Stored;
#ifdef GTA_REPLAY_RAGE
fwTimeSet	fwTimer::sm_replayTimeScaledNonClippedPausable_Stored;
#endif // GTA_REPLAY

fwTimeSet	fwTimer::sm_gameTime_Stored;
fwTimeSet	fwTimer::sm_camTime_Stored;
fwTimeSet	fwTimer::sm_nonPausableCamTime_Stored;
fwTimeSet	fwTimer::sm_scaledNonClippedTime_Stored;
fwTimeSet	fwTimer::sm_nonScaledClippedTime_Stored;
bool		fwTimer::sm_bTimeStored = false;

float		fwTimer::sm_fMinimumFrameTime = (1.0f / 30.0f);
float		fwTimer::sm_fMaximumFrameTime = (1.0f / 15.0f);
float		fwTimer::sm_fFixedFrameTime = 0.0f;
float		fwTimer::sm_fFixedFrameTimeScale = 1.0f;

bool		fwTimer::sm_bUserPause = false;
bool		fwTimer::sm_bScriptPause = false;
bool		fwTimer::sm_bSystemPause = false;
#if RSG_LOST_FOCUS_SUPPORT
bool		fwTimer::sm_bLostFocusPause = false;
#endif

//	bool		fwTimer::ms_bGamePause = false;
bool		fwTimer::sm_bForceMinimumFrameRate = false;
bool		fwTimer::sm_bForceMaximumFrameRate = false;
#if __BANK
bool		fwTimer::sm_bPauseOnAsserts = false;
bool		fwTimer::sm_bPauseScriptsToo = true;

bool		fwTimer::sm_bReadOnlyUserPauseWidgetValue = false;
bool		fwTimer::sm_bReadOnlyScriptPauseWidgetValue = false;
#endif
bool		fwTimer::sm_bSingleStepThisFrame = false;
bool		fwTimer::sm_bWantsToSingleStepNextFrame = false;

bool		fwTimer::sm_bForceUnevenFrameRate = false;

float		fwTimer::sm_fRagePhysicsUpdateTimeStep=0;

bool fwTimer::sm_bScriptToBePaused = false;
bool fwTimer::sm_bScriptToBeUnpaused = false;


//////////////////////////////////////////////////////////////
//
//	Debug functions for pausing, single-stepping, etc..
//
//////////////////////////////////////////////////////////////
#if !__FINAL
void Timer_TogglePauseMode(void)
{
	if(fwTimer::IsGamePaused())
	{
		fwTimer::SetDebugPause(false);
		fwTimer::EndUserPause();
	}
	else
	{
		fwTimer::SetDebugPause(true);
		fwTimer::StartUserPause(fwTimer::AreScriptPausedToo());
	}
}

void Time_SingleStep(void)
{
	fwTimer::SetWantsToSingleStepNextFrame();
}

#endif // !__FINAL

void fwTimeSet::Update(float stepInSeconds, float scale, bool clip)
{
	if (!s_retainMsRemainder)
		m_TimeRemainderMs = 0.0f;

	float stepInSecondsScaled = (stepInSeconds * scale);
	float correctedStepInSecondsScaled = (stepInSecondsScaled) + m_TimeRemainderMs/1000.0f;
	if (clip)
	{
		stepInSecondsScaled = Clamp(stepInSecondsScaled, fwTimer::GetMinimumFrameTime(), fwTimer::GetMaximumFrameTime());
		correctedStepInSecondsScaled = Clamp(correctedStepInSecondsScaled, fwTimer::GetMinimumFrameTime(), fwTimer::GetMaximumFrameTime());
	}

	// Apply whole to time
	float correctedStepInMillisScaled = correctedStepInSecondsScaled * 1000.0f;
	u32 wholeCorrectedStepInMillisScaled = (u32) correctedStepInMillisScaled;
	m_TimeRemainderMs = correctedStepInMillisScaled - wholeCorrectedStepInMillisScaled;

	m_TimePrevious = m_Time;
	m_Time += (u32)(wholeCorrectedStepInMillisScaled);

	// Apply float to timestep
	SetTimeStepInSeconds(stepInSecondsScaled);
	m_InvStep = 1.0f / stepInSecondsScaled;
	m_frameCount++;
}



#if TIMER_PRINTSTACKTRACE
void fwTimer::PrintStackTrace(const char *pMessageToDisplay)
{
	if (pMessageToDisplay)
	{
		Displayf("%s", pMessageToDisplay);
	}

#if !__FINAL
	sysStack::PrintStackTrace();
#endif
	scrThread *const vm = scrThread::GetActiveThread();
	if (vm)
	{
		Displayf("Currently running script thread \"%s\"\n", vm->GetScriptName());
#if !__FINAL
		vm->PrintStackTrace();
#endif // !__FINAL
	}
	else
	{
		Displayf("No currently running script thread\n");
	}
}
#endif



void fwTimer::Init(void)
{
#if __BANK

	bkBank & bank = BANKMGR.CreateBank("Time");

	bank.AddButton("Pause", datCallback(CFA(Timer_TogglePauseMode)));
	bank.AddToggle("Pause Scripts Too", &fwTimer::sm_bPauseScriptsToo);
	bank.AddToggle("Pause on Asserts", &fwTimer::sm_bPauseOnAsserts);
	bank.AddButton("Single Step", datCallback(CFA(Time_SingleStep)));
	bank.AddSlider("TimeScale:", &sm_fDebugTimeScale, 0.0001f, 1.0f, 0.01f);
	bank.AddToggle("Apply Game TimeScale To Cameras", &sm_bShouldApplyGameTimeScaleToCameras);

	bank.AddSlider("Min Frame Time", &fwTimer::sm_fMinimumFrameTime, 0.0001f, 1.0f, 0.01f);
	bank.AddSlider("Max Frame Time", &fwTimer::sm_fMaximumFrameTime, 0.0001f, 1.0f, 0.01f);
	bank.AddSlider("Fixed Frame Time Step", &fwTimer::sm_fFixedFrameTime, 0.0f, 1.0f, 0.01f);
	bank.AddSlider("Fixed Frame Time Step Scale", &fwTimer::sm_fFixedFrameTimeScale, 0.0f, 10.0f, 0.01f);

	bank.AddToggle("Force Min Framerate", &fwTimer::sm_bForceMinimumFrameRate);
	bank.AddToggle("Force Uneven Framerate", &fwTimer::sm_bForceUnevenFrameRate);

	bank.AddToggle("User Pause (read-only)", &fwTimer::sm_bReadOnlyUserPauseWidgetValue);
	bank.AddToggle("Script Pause (read-only)", &fwTimer::sm_bReadOnlyScriptPauseWidgetValue);

	bank.AddToggle("Switch to new update", &s_newUpdate);
	bank.AddToggle("Retain ms remainder", &s_retainMsRemainder);

	PARAM_stallThreshold.Get(sysTimer::GetStallThreshold());

	bank.AddSlider("POSSIBLE STALL delay", &sysTimer::GetStallThreshold(), 200, 10000, 10);

	if (PARAM_pauseonassert.Get())
	{
		sm_bPauseOnAsserts = true;
	}
#endif

	sm_fMinimumFrameTime = (1.0f / 3000.0f);		// Allows for 100x slow motion
#if !__FINAL
	if(PARAM_maxframetime.Get())
		 PARAM_maxframetime.Get(sm_fMaximumFrameTime);
	else
#endif
		sm_fMaximumFrameTime = (1.0f / 15.0f);

#if !__FINAL
	sm_fFixedFrameTimeScale = 1.0f;
	
	if (PARAM_forcedfps.Get())
	{
		float forcedFps = 30.0f;
		PARAM_forcedfps.Get(forcedFps);
		sm_fMaximumFrameTime = sm_fMinimumFrameTime =  1/forcedFps;
		sm_bForceMinimumFrameRate = true;

		sm_fFixedFrameTimeScale = forcedFps/30.0f;
	}

	if (PARAM_fixedframetime.Get())
	{
		float fixedFrameTime = 30.0f;
		PARAM_fixedframetime.Get(fixedFrameTime);

		sm_fFixedFrameTime = 1.0f / fixedFrameTime;
	}

	if (PARAM_maxframetime.Get())
	{
		float maxframetime = 1000/60.0f;
		PARAM_maxframetime.Get(maxframetime);
		sm_fMaximumFrameTime = 1.0f / maxframetime;
		sm_bForceMinimumFrameRate = true;
	}


	if (PARAM_minframetime.Get())
	{
		float minframetime = 1000/15.0f;
		PARAM_minframetime.Get(minframetime);
		sm_fMinimumFrameTime = 1.0f / minframetime;
		sm_bForceMaximumFrameRate = true;
	}

	if (PARAM_maxfps.Get())
	{
		float maxfps = 60.0f;
		PARAM_maxfps.Get(maxfps);
		sm_fMinimumFrameTime = 1/maxfps;
		sm_bForceMaximumFrameRate = true;
	}
#endif // !__FINAL

	// Set some sensible values at the start of the game.
	sm_systemTime.Init();
#ifdef GTA_REPLAY_RAGE
	sm_replayTimeScaledNonClippedPausable.Init();
#endif // GTA_REPLAY
	sm_gameTime.Init();
	sm_camTime.Init();
	sm_nonPausableCamTime.Init();
	sm_scaledNonClippedTime.Init();
	sm_nonScaledClippedTime.Init();

	for(int i = 0; i < eTST_Max; ++i)
	{
		sm_aTimeScales[i] = 1.0f;
	}

#if !__FINAL
	PARAM_timescale.Get(sm_fDebugTimeScale);

	sm_bShouldApplyGameTimeScaleToCameras = PARAM_camsusegametimescale.Get();
#endif
}

#if !__FINAL

struct
{
	float maxFrameTime, minFrameTime;
	bool forceMin;
	float fixedTimeScale;
	bool pushed;
} g_previousForceFrameRate = { 100, 1, false, 1.0f, false };

void fwTimer::SetForcedFps(float fps)
{
	// This will be set if the user has tried to set their own
	// forced framerate on the commandline.
	if (sm_bForceMinimumFrameRate)
		return;

	if (!g_previousForceFrameRate.pushed)
	{
		g_previousForceFrameRate.maxFrameTime = sm_fMaximumFrameTime;
		g_previousForceFrameRate.minFrameTime = sm_fMinimumFrameTime;
		g_previousForceFrameRate.forceMin = sm_bForceMinimumFrameRate;
		g_previousForceFrameRate.fixedTimeScale = sm_fFixedFrameTimeScale;
		g_previousForceFrameRate.pushed = true;
	}

	sm_fMaximumFrameTime = sm_fMinimumFrameTime =  1.0f/fps;
	sm_bForceMinimumFrameRate = true;
	sm_fFixedFrameTimeScale = fps/30.0f;
}

void fwTimer::UnsetForcedFps()
{
	if (g_previousForceFrameRate.pushed)
	{
		sm_fMaximumFrameTime = g_previousForceFrameRate.maxFrameTime;
		sm_fMinimumFrameTime = g_previousForceFrameRate.minFrameTime;
		sm_bForceMinimumFrameRate = g_previousForceFrameRate.forceMin;
		sm_fFixedFrameTimeScale = g_previousForceFrameRate.fixedTimeScale;
		g_previousForceFrameRate.pushed = false;
	}
}

// default keys to control timer
int fwTimer::sm_iKey_Pause				= KEY_PAUSE;
int fwTimer::sm_iKey_SingleStep			= KEY_RIGHT;
int fwTimer::sm_iKey_TimeScaleModifier	= KEY_RCONTROL;
int fwTimer::sm_iKey_TimeScaleInc		= KEY_RIGHT;
int fwTimer::sm_iKey_TimeScaleDec		= KEY_LEFT;

// name:		GetKeyDown_RAGE
// description:	Default version of key down test
static bool GetKeyDown_RAGE(int key,const char *)
{
	// ioMapper::GetEnableKeyboard() is true in game mode and false in other (including debug) modes.
	return ioMapper::DebugKeyDown(key) != 0;
}

// name:		GetKeyJustDown_RAGE
// description:	Default version of key just down test
static bool GetKeyJustDown_RAGE(int key,const char *)
{
	// ioMapper::GetEnableKeyboard() is true in game mode and false in other (including debug) modes.
	return ioMapper::DebugKeyPressed(key) != 0;
}

// default key input functions
static bool (*s_GetKeyDown)(int,const char*) = GetKeyDown_RAGE;
static bool (*s_GetKeyJustDown)(int,const char*) = GetKeyJustDown_RAGE;

// name:		pfTimeBarMgr::SetKeyboardCallbacks
// description:	Set functions for key down and key just down
void fwTimer::SetKeyboardCallbacks(bool (*keyJustDown)(int,const char*),bool (*keyDown)(int,const char*))
{
	s_GetKeyJustDown = keyJustDown;
	s_GetKeyDown = keyDown;
}
// functions to override default keys
void fwTimer::SetKey_Pause(const int iKey) { sm_iKey_Pause = iKey; }
void fwTimer::SetKey_SingleStep(const int iKey) { sm_iKey_SingleStep = iKey; }
void fwTimer::SetKey_TimeScaleModifier(const int iKey) { sm_iKey_TimeScaleModifier = iKey; }
void fwTimer::SetKey_TimeScaleInc(const int iKey) { sm_iKey_TimeScaleInc = iKey; }
void fwTimer::SetKey_TimeScaleDec(const int iKey) { sm_iKey_TimeScaleDec = iKey; }

#endif // !__FINAL


void fwTimer::Update(void)
{
	if (sm_bScriptToBePaused)
	{
		sm_bScriptPause = true;
		sm_bScriptToBePaused = false;
	}

	if (sm_bScriptToBeUnpaused)
	{
		sm_bScriptPause = false;
		sm_bScriptToBeUnpaused = false;
	}
	//********************
	// Debug key controls

#if !__FINAL
	// Deferred debug pause
	if(sm_DeferredDebugPause == DDP_PendingOn)
	{
		SetDebugPause(true);
		StartUserPause(AreScriptPausedToo());
		sm_DeferredDebugPause = DDP_None;
	}
	else if(sm_DeferredDebugPause == DDP_PendingOff)
	{
		SetDebugPause(false);
		EndUserPause();
		sm_DeferredDebugPause = DDP_None;
	}

	// Toggle pause mode
	// Pause need to work in all modes.
	// NOTE: We also use KEY_NUMLOCK on ORBIS as Sony have reserved the Pause/Break key and this is the only key I could find not
	// in use (for url:bugstar:1906554 and url:bugstar:1905982).
	if(ioKeyboard::KeyPressed(sm_iKey_Pause) != 0 ORBIS_ONLY(|| ioKeyboard::KeyPressed(KEY_NUMLOCK) != 0)) //, "Toggle pause"))
	{
		Timer_TogglePauseMode();
	}

	// Allow game to be single-stepped
	if(IsGamePaused() && s_GetKeyJustDown(sm_iKey_SingleStep, "Single step when paused"))
	{
		Time_SingleStep();
	}
	// Have an optional modifier key (eg. KEY_RCONTROL) for altering the game's timescale (slowmo)
	if(sm_iKey_TimeScaleModifier==0 || s_GetKeyDown(sm_iKey_TimeScaleModifier, "Hold to modify timescale"))
	{
		// Increase the timescale
		if(s_GetKeyDown(sm_iKey_TimeScaleInc, "Increment timescale"))
		{
#if __BANK
			if(!cutsManager::IsBankOpen()) // This is temporary until we get a dedicated animation keyboard mapping (coming soon).
#endif // __BANK
			{
				sm_fDebugTimeScale = Min(sm_fDebugTimeScale + 0.01f, 1.0f);
			}
		}
		// Decrease the timescale
		else if(s_GetKeyDown(sm_iKey_TimeScaleDec, "Decrement timescale"))
		{
#if __BANK
			if(!cutsManager::IsBankOpen()) // This is temporary until we get a dedicated animation keyboard mapping (coming soon).
#endif // __BANK
			{
				sm_fDebugTimeScale = Max(sm_fDebugTimeScale - 0.01f, 0.0001f);
			}
		}
	}
#endif	// !__FINAL


	sm_bSingleStepThisFrame = false;

	if(sm_bWantsToSingleStepNextFrame)
	{
#if __BANK
		StartUserPause(sm_bPauseScriptsToo);
#else
		StartUserPause();
#endif
		sm_bSingleStepThisFrame = true;
		sm_bWantsToSingleStepNextFrame = false;
	}

	CHECKOUT()

	float timeScale = sm_fTimeScale = GetTimeWarpActive();

#if !__FINAL
	timeScale *= sm_fDebugTimeScale;
#endif
	float TimeStepInSeconds = sm_sysTimer.GetTime();

	sm_sysTimer.Reset();

    // inform the network code the timestep has just been calculated, this is
    // required so an accurate timestamp can be associated with game object position updates
#if !__RESOURCECOMPILER
    netInterface::StartOfFrame();
#endif

	// Update the 4 different timers.
	float stepInSeconds = Clamp(TimeStepInSeconds * timeScale, sm_fMinimumFrameTime, sm_fMaximumFrameTime);
#if !__FINAL
	if (sm_fFixedFrameTime != 0.0f)
	{
		stepInSeconds = sm_fFixedFrameTime;
	}
#endif // !__FINAL

	// The non Pause Game timer.
	sm_nonPausedGameTime.m_TimePrevious = sm_nonPausedGameTime.m_Time;
	if (s_newUpdate) 
	{
		sm_nonPausedGameTime.Update(TimeStepInSeconds, timeScale, true);
	}
	else
	{
		sm_nonPausedGameTime.SetTimeStepInSeconds(stepInSeconds);
		sm_nonPausedGameTime.m_Time += (s32)(1000.0f * stepInSeconds);
		sm_nonPausedGameTime.m_InvStep = 1.0f / stepInSeconds;
		sm_nonPausedGameTime.m_frameCount++;
	}

	// Stuff in here does NOT happen when the game is paused.
	if(!IsGamePaused() || sm_bSingleStepThisFrame)
	{
#if !__FINAL && DEBUG_DRAW
		grcDebugDraw::SetFrozenMode(false);
#endif // !__FINAL && DEBUG_DRAW

		grcSetup::SetPaused(false);

		// Game time: This is scaled and clipped
		sm_gameTime.m_TimePrevious = sm_gameTime.m_Time;
		if (s_newUpdate) 
		{
			sm_gameTime.Update(TimeStepInSeconds, timeScale, true);
		}
		else
		{
			sm_gameTime.SetTimeStepInSeconds(stepInSeconds);
			sm_gameTime.m_Time += (s32)(1000.0f * stepInSeconds);
			sm_gameTime.m_InvStep = 1.0f / stepInSeconds;
			sm_gameTime.m_frameCount++;
		}

		STRVIS_FRAME();

		
		// ... time: This is scaled but not clipped
		if (s_newUpdate) 
		{
			sm_scaledNonClippedTime.Update(TimeStepInSeconds, timeScale, false);
		}
		else
 		{
			sm_scaledNonClippedTime.m_TimePrevious = sm_scaledNonClippedTime.m_Time;
			sm_scaledNonClippedTime.SetTimeStepInSeconds(TimeStepInSeconds * timeScale);
			sm_scaledNonClippedTime.m_Time += (s32)(1000.0f * sm_scaledNonClippedTime.m_StepInSeconds);
			sm_scaledNonClippedTime.m_InvStep = 1.0f / sm_scaledNonClippedTime.m_StepInSeconds;
			sm_scaledNonClippedTime.m_frameCount++;
		}

		// ... time: This is non scaled and clipped
		if (s_newUpdate) 
		{
			sm_nonScaledClippedTime.Update(TimeStepInSeconds, 1.0f, false);
		}
		else
		{
			sm_nonScaledClippedTime.m_TimePrevious = sm_nonScaledClippedTime.m_Time;
			sm_nonScaledClippedTime.SetTimeStepInSeconds(Clamp(TimeStepInSeconds, sm_fMinimumFrameTime, sm_fMaximumFrameTime));
			sm_nonScaledClippedTime.m_Time += (s32)(1000.0f * sm_nonScaledClippedTime.m_StepInSeconds);
			sm_nonScaledClippedTime.m_InvStep = 1.0f / sm_nonScaledClippedTime.m_StepInSeconds;
			sm_nonScaledClippedTime.m_frameCount++;
		}

#ifdef GTA_REPLAY_RAGE
		// Replay time: This one is unscaled clipped
		if (s_newUpdate) 
		{
			sm_replayTimeScaledNonClippedPausable.Update(TimeStepInSeconds, timeScale, true);
		}
		else
		{
			sm_replayTimeScaledNonClippedPausable.m_TimePrevious = sm_replayTimeScaledNonClippedPausable.m_Time;
			sm_replayTimeScaledNonClippedPausable.SetTimeStepInSeconds(Clamp(TimeStepInSeconds * timeScale, sm_fMinimumFrameTime, sm_fMaximumFrameTime));
			sm_replayTimeScaledNonClippedPausable.m_Time += (s32)(1000.0f * sm_replayTimeScaledNonClippedPausable.m_StepInSeconds);				// real elapsed time, may differ from normal elapsed time during a network game
			sm_replayTimeScaledNonClippedPausable.m_InvStep = 1.0f / sm_replayTimeScaledNonClippedPausable.m_StepInSeconds;
		}
#endif // GTA_REPLAY
	}
	else
	{
#if !__FINAL && DEBUG_DRAW
		grcDebugDraw::SetFrozenMode(true);
#endif // !__FINAL && DEBUG_DRAW

		grcSetup::SetPaused(true);
	}

	// System time: This is unscaled / unclipped
	if (s_newUpdate) 
	{
		sm_systemTime.Update(TimeStepInSeconds, 1.0f, false);
	}
	else
	{
		sm_systemTime.m_TimePrevious = sm_systemTime.m_Time;
		sm_systemTime.SetTimeStepInSeconds(TimeStepInSeconds);
		sm_systemTime.m_Time += (s32)(1000.0f * sm_systemTime.m_StepInSeconds);				// real elapsed time, may differ from normal elapsed time during a network game
		sm_systemTime.m_InvStep = 1.0f / sm_systemTime.m_StepInSeconds;
		sm_systemTime.m_frameCount++;// don't pause frame count as this was breaking some script code
		#if RSG_PC
			sm_systemTime.m_frameCount = fwTimer::GetNonPausableCamFrameCount();
		#endif
	}


	// Cam time: This is scaled and clipped ( and not affected by the network!)
	const float camTimeScale = sm_bShouldApplyGameTimeScaleToCameras ? timeScale : 1.0f;
	if (s_newUpdate) 
	{
		if (!IsGamePaused() || sm_bSingleStepThisFrame) 
		{
			sm_camTime.Update(TimeStepInSeconds, camTimeScale, true);
		}
		else
		{
			sm_camTime.m_TimePrevious = sm_camTime.m_Time;
			sm_camTime.SetTimeStepInSeconds(Clamp(TimeStepInSeconds * camTimeScale, sm_fMinimumFrameTime, sm_fMaximumFrameTime));
			sm_camTime.m_InvStep = 1.0f / sm_camTime.m_StepInSeconds;
		}
	}
	else
	{
		sm_camTime.m_TimePrevious = sm_camTime.m_Time;
		sm_camTime.SetTimeStepInSeconds(Clamp(TimeStepInSeconds * camTimeScale, sm_fMinimumFrameTime, sm_fMaximumFrameTime));
		if (!IsGamePaused() || sm_bSingleStepThisFrame)
		{
			sm_camTime.m_Time += (s32)(1000.0f * sm_camTime.m_StepInSeconds);
			sm_camTime.m_frameCount++;
		}
		sm_camTime.m_InvStep = 1.0f / sm_camTime.m_StepInSeconds;
	}
	

	// Non Pausable Cam time: This is scaled and clipped ( and not affected by the network!)... and cant be paused when you are in frontend etc so that screen fades work and viewports move etc.
	// DW - sorry about this ( I know its not ideal )  but its convienient for me to do this this way in order to allow me not to have to refactor a LOT of code which would be dangerous at this time.
	if (s_newUpdate) 
	{
		sm_nonPausableCamTime.Update(TimeStepInSeconds, camTimeScale, true);
	}
	else
	{
		sm_nonPausableCamTime.m_TimePrevious = sm_nonPausableCamTime.m_Time;
		sm_nonPausableCamTime.SetTimeStepInSeconds(Clamp(TimeStepInSeconds * camTimeScale, sm_fMinimumFrameTime, sm_fMaximumFrameTime));
		sm_nonPausableCamTime.m_Time += (s32)(1000.0f * sm_nonPausableCamTime.m_StepInSeconds);
		sm_nonPausableCamTime.m_InvStep = 1.0f / sm_nonPausableCamTime.m_StepInSeconds;
		sm_nonPausableCamTime.m_frameCount++;
	}

	// Non pausable, non scaled, clipped time
	if (s_newUpdate)
	{
		sm_nonPausedNonScaledClippedTime.Update(TimeStepInSeconds, 1.0f, true);
	}
	else
	{
		sm_nonPausedNonScaledClippedTime.m_TimePrevious = sm_nonPausedNonScaledClippedTime.m_Time;
		sm_nonPausedNonScaledClippedTime.SetTimeStepInSeconds(Clamp(TimeStepInSeconds, sm_fMinimumFrameTime, sm_fMaximumFrameTime));
		sm_nonPausedNonScaledClippedTime.m_Time += (s32)(1000.0f * sm_nonPausedNonScaledClippedTime.m_StepInSeconds);
		sm_nonPausedNonScaledClippedTime.m_InvStep = 1.0f / sm_nonPausedNonScaledClippedTime.m_StepInSeconds;
		sm_nonPausedNonScaledClippedTime.m_frameCount++;
	}

		// The following bit can be removed once Rage have removed their references to TIME.
		// Also deal with the one case in ped.cpp in ApplyExtractedVelocity()
	TIME.SetSeconds(sm_gameTime.GetTimeStep());
	TIME.SetFrameCount(sm_systemTime.GetFrameCount());
	TIME.SetUnwarpedSeconds(sm_systemTime.GetTimeStep());
#ifdef GTA_REPLAY_RAGE
	TIME.SetUnwarpedSeconds(sm_replayTimeScaledNonClippedPausable.GetTimeStep());
#endif // GTA_REPLAY
	TIME.SetUnwarpedSeconds(sm_nonScaledClippedTime.GetTimeStep());
	TIME.SetElapsedTime(0.001f*sm_gameTime.GetTimeInMilliseconds());

#if __BANK
	sm_bReadOnlyUserPauseWidgetValue = sm_bUserPause;
	sm_bReadOnlyScriptPauseWidgetValue = sm_bScriptPause;
#endif
}

void fwTimer::StartUserPause(bool BANK_ONLY(bScriptsToo))
{
	Assert(sysThreadType::IsUpdateThread());
	PrintStackTrace("StartUserPause is setting sm_bUserPause");

	sm_bUserPause = true;
#if __BANK
	sm_bPauseScriptsToo = bScriptsToo;
#endif
	grcSetup::SetPaused(sm_bUserPause);
}

void fwTimer::EndUserPause(void)
{
	Assert(sysThreadType::IsUpdateThread());
	PrintStackTrace("EndUserPause is clearing sm_bUserPause");

	sm_bUserPause = false;
	grcSetup::SetPaused(sm_bUserPause);
}

bool fwTimer::IsUserPaused(void)
{
	return sm_bUserPause;
}

void fwTimer::StartScriptPause(void)
{
	Assert(sysThreadType::IsUpdateThread());
	sm_bScriptToBePaused = true;
	sm_bScriptToBeUnpaused = false;

	PrintStackTrace("StartScriptPause");
}

void fwTimer::EndScriptPause(void)
{
	sm_bScriptToBePaused = false;
	sm_bScriptToBeUnpaused = true;

	PrintStackTrace("EndScriptPause");
}

bool fwTimer::IsScriptPaused(void)
{
	return sm_bScriptPause;
}


void fwTimer::StartSystemPause(void)
{
	Assert(sysThreadType::IsUpdateThread());

	PrintStackTrace("StartSystemPause is setting sm_bSystemPause");

	sm_bSystemPause = true;
	grcSetup::SetPaused(sm_bSystemPause);
}

void fwTimer::EndSystemPause(void)
{
	Assert(sysThreadType::IsUpdateThread());

	PrintStackTrace("EndSystemPause is clearing sm_bSystemPause");

	sm_bSystemPause = false;
	grcSetup::SetPaused(sm_bSystemPause);
}

bool fwTimer::IsSystemPaused(void)
{
	return sm_bSystemPause;
}

#if RSG_LOST_FOCUS_SUPPORT
void fwTimer::StartLostFocusPause(void)
{
	Assert(sysThreadType::IsRenderThread());
	sm_bLostFocusPause = true;
	grcSetup::SetPaused(sm_bLostFocusPause);
}

void fwTimer::EndLostFocusPause(void)
{
	Assert(sysThreadType::IsRenderThread());
	sm_bLostFocusPause = false;
	grcSetup::SetPaused(sm_bLostFocusPause);
}

bool fwTimer::IsLostFocusPaused(void)
{
	return sm_bLostFocusPause;
}
#endif//RSG_LOST_FOCUS_SUPPORT

void fwTimer::InitLevel()
{
	sm_fTimeScale		= 1.0f;

	sm_aTimeScales[eTST_Script] = 1.0f;
	sm_aTimeScales[eTST_Camera] = 1.0f;
	sm_aTimeScales[eTST_SpecialAbility] = 1.0f;

	sm_bTimeStored = false;
	sm_bUserPause = false;
	sm_bScriptPause = false;

	PrintStackTrace("fwTimer::InitLevel is clearing sm_bUserPause, sm_bScriptPause and sm_bSystemPause");

	sm_bSystemPause = false;
#if RSG_PC && RSG_LOST_FOCUS_SUPPORT
	sm_bLostFocusPause = false;
#endif
}

void fwTimer::StoreCurrentTime()
{
	sm_bTimeStored = true;

	sm_systemTime_Stored = sm_systemTime;
#ifdef GTA_REPLAY_RAGE
	sm_replayTimeScaledNonClippedPausable_Stored = sm_replayTimeScaledNonClippedPausable;
#endif // GTA_REPLAY
	sm_gameTime_Stored = sm_gameTime;
	sm_camTime_Stored = sm_camTime;
	sm_nonPausableCamTime_Stored = sm_nonPausableCamTime;
	sm_scaledNonClippedTime_Stored = sm_scaledNonClippedTime;
	sm_nonScaledClippedTime_Stored = sm_nonScaledClippedTime;

    // Note: network time is not stored as this should always continue running
	// Note: nonPausedGameTime not stored as rumble relies on it going forward.
}

void fwTimer::RestoreCurrentTime()
{
	if (sm_bTimeStored)
	{
		sm_systemTime = sm_systemTime_Stored;
#ifdef GTA_REPLAY_RAGE
		sm_replayTimeScaledNonClippedPausable = sm_replayTimeScaledNonClippedPausable_Stored;
#endif // GTA_REPLAY
		sm_gameTime = sm_gameTime_Stored;
		sm_camTime =  sm_camTime_Stored;
		sm_nonPausableCamTime = sm_nonPausableCamTime_Stored;
		sm_scaledNonClippedTime = sm_scaledNonClippedTime_Stored;
		sm_nonScaledClippedTime = sm_nonScaledClippedTime_Stored;
	}
	sm_bTimeStored = false;

    // Note: network time is not restored as this should always continue running
	// Note: nonPausedGameTime not stored as rumble relies on it going forward.
}


// HACK - talk to Klaas before changing this
void fwTimeSet::Knock(size_t valueThatIsAtLeast64)
{
	Assertf(this == &fwTimer::GetSystemTimer(), "Only call Knock() on the system timer!");
	Assert(valueThatIsAtLeast64 >= 64);

	// Combine all the bits in valueThatIsAtLeast64 that are over the 0x40 bit.
	// Pass 1: Compress down to a 16-bit value
	size_t pattern = (m_Time << 16) | ((m_Time & 0xff00) << 14);	// Put garbage in top 16 bits of pattern
	pattern |= (valueThatIsAtLeast64 & 0x00000000003fffc0) >> 6;	// pattern contains bits 6..21 of value in bits 0..15, and garbage in top bits
	pattern |= (valueThatIsAtLeast64 & 0x00000003fffc0000) >> 18;	// bitwise OR in bits 18..33 of value over bits 0..15
	pattern |= (valueThatIsAtLeast64 & 0x0000fffc00000000) >> 34;	// bitwise OR in bits 34..47 of value over bits 0..15
	pattern |= (valueThatIsAtLeast64 & 0xffff000000000000) >> 48;	// bitwise OR in bits 48..63 of value over bits 0..15

	// Pass 2: Compress down to a 4-bit value
	pattern = (pattern & 0x000f) | ((pattern & 0x00f0) >> 4) | ((pattern & 0x0f00) >> 8) | ((pattern & 0xf000) >> 12);

	// Pass 3: Compress down to a 1-bit value and invert
	pattern = (pattern & 0x1) | ((pattern & 0x2) >> 1) | ((pattern & 0x4) >> 2) | ((pattern & 0x8) >> 3);
	pattern = pattern ^ 1;

	// Since pattern should be 0 at this point, this code should be equivalent to m_frameCount = m_frameCount, UNLESS the
	// two timers are off by one already, in which case the difference will be retained.
	m_frameCount = m_frameCount + (!(m_frameCount ^ fwTimer::GetNonPausableCamFrameCount())) * (u32)pattern;
}
// HACK - talk to Klaas before changing this

} // namespace rage
