//
// fwsys/glue.h
//
// Copyright (C) 2011-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef FWSYS_GLUE_H__
#define FWSYS_GLUE_H__

#include "grcore\texture.h"
#include "grcore\locktypes.h"

namespace rage
{
class fwTexturePayload 
{
/*--------------------------------------------------------------------------------------------------*/
/* Constructors/Destructor.																			*/
/*--------------------------------------------------------------------------------------------------*/

public:
	fwTexturePayload()
	{
		m_pTexture = NULL;
#if !__D3D11
		m_pBasePtr = NULL;
#endif //!__D3D11
	}
	~fwTexturePayload()
	{
		// Attempt to ensure code using these payloads clean up after themselves.
		AssertMsg(m_pTexture == NULL, "fwTexturePayload::~fwTexturePayload()...Texture pointer not NULLed!");
	}

	/*--------------------------------------------------------------------------------------------------*/
	/* General access functions.																		*/
	/*--------------------------------------------------------------------------------------------------*/

public:
	// Set the texture.
	void SetTexture(grcTexture *pTexture)
	{
		AssertMsg(pTexture, "fwTexturePayload::SetPayloadTexture()...Invalid texture pointer.");
		m_pTexture = pTexture;

#if !__D3D11
		grcTextureLock texLock;

		// Obtain the address of the texture data.
		ASSERT_ONLY(bool LockResult = )m_pTexture->LockRect(0, 0, texLock);
		AssertMsg(LockResult, "fwTexturePayload::SetPayloadTexture()...Failed to lock the texture.");
		m_pBasePtr = texLock.Base;
		m_pTexture->UnlockRect(texLock);
#endif //!__D3D11
	}

	// Returns the payload texture.
	grcTexture *GetTexture()
	{
		return m_pTexture;
	}

	// Clears the payload texture.
	void ClearTexture()
	{
		m_pTexture = NULL;
#if !__D3D11
		m_pBasePtr = NULL;
#endif //!__D3D11
	}


	// Returns the texture data base pointer.
	void *AcquireBasePtr(u32 LockFlags)
	{
	#if !__D3D11
		(void)LockFlags;
		return m_pBasePtr;
	#else //!__D3D11
		m_TexLock.Base = NULL;
		ASSERT_ONLY(bool LockResult = )m_pTexture->LockRect(0, 0, m_TexLock, LockFlags);
		AssertMsg(LockResult, "fwTexturePayload::AcquireBasePtr()...Failed to lock the texture.");
		return m_TexLock.Base;
	#endif //!__D3D11
	}


	// Returns the texture data base pointer.
	const void *AcquireBasePtr(u32 LockFlags) const
	{
		return const_cast<fwTexturePayload*>(this)->AcquireBasePtr(LockFlags);
	}


	// Releases the base pointer.
	void ReleaseBasePtr() const
	{
	#if __D3D11
		m_pTexture->UnlockRect(m_TexLock);
	#endif //__D3D11
	}

/*--------------------------------------------------------------------------------------------------*/
/* Data members.																					*/
/*--------------------------------------------------------------------------------------------------*/

private:
	grcTexture *m_pTexture;
#if !__D3D11
	void *m_pBasePtr;
#else //!__D3D11	
	grcTextureLock m_TexLock;
#endif //!__D3D11
};

} // namespace rage

#endif // FWSYS_GLUE_H__
