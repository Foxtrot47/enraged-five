Project RageFramework

RootDirectory ..\..

ParserIgnore

IncludePath ..\..\..\..\base\src
IncludePath ..\..\..\..\suite\src
IncludePath ..\..\..\..\script\src
IncludePath ..\..\..\..\framework\src
IncludePath ..\..\..\..\scaleform\include
IncludePath[Platform=='x64'] $(RAGE_DIR)\3rdparty\Arxan\TransformIT\x64\include\

Include vcproj\RageFramework\makefile.txt

Libraries {
	%RAGE_DIR%\base\src\rage_lib_psc\rage_lib_psc
	%RAGE_DIR%\framework\src\framework_lib_psc\framework_lib_psc
}
