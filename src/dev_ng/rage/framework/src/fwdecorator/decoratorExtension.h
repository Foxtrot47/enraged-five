/////////////////////////////////////////////////////////////////////////////////
// FILE :		decoratorExtension.h
// PURPOSE :	an extension given to any extensiblebase that allows for the 
//                  application of fwDecorators to that object through the fwDecoratorExtension.
// AUTHOR :		Jason Jurecka.
// CREATED :	2/6/2011
/////////////////////////////////////////////////////////////////////////////////
#ifndef FW_DECORATOR_EXTENSION_H_
#define FW_DECORATOR_EXTENSION_H_

// Rage Includes
#include "atl/string.h"

// Framework Includes
#include "entity/extensiblebase.h"
#include "fwtl/pool.h"
#include "fwdecorator/decorator_channel.h"
#include "fwdecorator/decorator.h"

#if DECORATOR_DEBUG
#include "vectormath/vec3v.h"
#endif

namespace rage {

class fwDecoratorExtension : public fwExtension
{
public:
    //////////////////////////////////////////////////////////////////////////
    template <class T> static bool Get (fwExtensibleBase& exBase, const atHashWithStringBank& key, T &out)
    {
        fwDecoratorExtension *extension = exBase.GetExtension<fwDecoratorExtension>();
        if (extension)
        {
            return extension->Get(key, out);
        }

        return false;
    }

    template <class T> static void Set (fwExtensibleBase& exBase, const atHashWithStringBank& key, T &val)
    {
        fwDecoratorExtension *extension = exBase.GetExtension<fwDecoratorExtension>();
        if (!extension)
        {
            // Nope - need to create one first.
            extension = rage_new fwDecoratorExtension(exBase);
            exBase.GetExtensionList().Add(*extension);
        }

        extension->Update(key, val);
    }

	template <class T> static bool Get (fwExtensibleBase* exBase, const atHashWithStringBank& key, T &out)
	{
		fwDecoratorExtension *extension = exBase ? exBase->GetExtension<fwDecoratorExtension>() : NULL;
		if (extension)
		{
			return extension->Get(key, out);
		}

		return false;
	}

    static bool ExistsOn      (fwExtensibleBase& exBase, const atHashWithStringBank& key);
    static bool RemoveFrom    (fwExtensibleBase& exBase, const atHashWithStringBank& key);
    static bool RemoveAllFrom (fwExtensibleBase& exBase);
    //////////////////////////////////////////////////////////////////////////

    fwDecoratorExtension (fwExtensibleBase& base);
    ~fwDecoratorExtension();

    EXTENSIONID_DECL(fwDecoratorExtension, fwExtension);

    FW_REGISTER_CLASS_POOL(fwDecoratorExtension); 

    void Update     (const atHashWithStringBank& key, bool val);
    void Update     (const atHashWithStringBank& key, float val);
    void Update     (const atHashWithStringBank& key, int val);
    void Update     (const atHashWithStringBank& key, atHashWithStringBank &val);
	void Update     (const atHashWithStringBank& key, fwDecorator::Time val);

    template <class T> bool Get (const atHashWithStringBank& key, T &out)
    {
        bool valid = false;
        fwDecorator* dec = Find(key);
        if (dec)
        {
            dec->Get(out);
            valid = true;
        }    
        return valid;
    }

    bool            Exists     (const atHashWithStringBank& key);
    void            Remove     (const atHashWithStringBank& key);
    void            RemoveAll  ();
    int             GetCount   () const { return m_Count; }
	fwDecorator*    GetRoot    () const { return m_Root; }

#if DECORATOR_DEBUG
	void                SpewDecorators      ();
	void                SpewOneDecorator     (void* pItem);
    void                RenderDecorators    (Vec3V_In location);
    fwExtensibleBase*   GetBase             () const { return m_pBase; }
#endif

private:
    fwDecorator* Find   (const atHashWithStringBank& key);
    void         Add    (fwDecorator* newDecorator);

    fwDecorator* m_Root; //LinkedList of decorators ... 
    u32          m_Count;

#if DECORATOR_DEBUG
    fwExtensibleBase*	m_pBase;
#endif
};

} // namespace rage

#endif //FW_DECORATOR_EXTENSION_H_