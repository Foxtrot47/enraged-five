/////////////////////////////////////////////////////////////////////////////////
// FILE :		DecoratorInterface.h
// PURPOSE :	Interface for debug drawing and rag so it can be easily separated from core code
//               also includes a registry for all decorator names so we can enforce type usage
// AUTHOR :		Jason Jurecka.
// CREATED :	24/6/2011
/////////////////////////////////////////////////////////////////////////////////
#ifndef FW_DECORATOR_INTERFACE_H_
#define FW_DECORATOR_INTERFACE_H_

// Rage Includes
#include "atl/map.h"
#include "atl/string.h"
#include "vector/color32.h"

// Framework Includes
#include "entity/extensiblebase.h"
#include "fwdecorator/decorator_channel.h"
#include "fwdecorator/decorator.h"

namespace rage {

class fwDecoratorInterface
{
public:
            fwDecoratorInterface  ();
    virtual	~fwDecoratorInterface ();

    //////////////////////////////////////////////////////////////////////////
    // STATIC
    static void Init            (unsigned initMode); //SKELETON INTERFACE
    static void Shutdown        (unsigned shutdownMode); //SKELETON INTERFACE
    static void InitClass       (fwDecoratorInterface* decoratorInterface);
    static void ShutdownClass   ();
    //////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////
    // Name Type registry
    void                Register           (const atHashWithStringBank& key, fwDecorator::Type type, const char* whoIsRegistering);
    bool                IsRegisteredAsType (const atHashWithStringBank& key, fwDecorator::Type type);
    bool                IsRegistered       (const atHashWithStringBank& key);
    fwDecorator::Type   GetRegisteredType  (const atHashWithStringBank& key);
    //////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////
    // Debug Draw
#if DECORATOR_DEBUG
            void SpewDecorator      (const fwDecorator &dec);
            void RenderDecorator    (Vec3V_In position, const fwDecorator &dec);
    virtual bool GetOffset          (fwExtensibleBase* pBase, Vec3V_InOut position);
            void RenderDebug        ();
            void InitWidgets        ();
    static  void SpewDecorators     ();
	static  void SpewOneDecorator   (void* pItem);

    static float sm_renderOffset;
#endif
    //////////////////////////////////////////////////////////////////////////

private:

    //Used for debug Name vs Type checks so it is known 
    //  if a name is being used as two different types
    //  in the code or scripts ... 
    atMap<u32, fwDecorator::Type> m_Registered;

#if DECORATOR_DEBUG
    atMap<u32, atHashWithStringBank> m_RegisteredOwners; //who did the registering
    bool m_renderDebug;
    Color32 m_renderColor;
#endif
};

///////////////////////////////////////////////////////////////////////////////
//	EXTERNS
///////////////////////////////////////////////////////////////////////////////
extern fwDecoratorInterface* gp_DecoratorInterface;

} // namespace rage

#endif // FW_DECORATOR_INTERFACE_H_
