/////////////////////////////////////////////////////////////////////////////////
// FILE :		decoratorExtension.h
// PURPOSE :	an extension given to any extensiblebase that allows for the 
//                  application of fwDecorators to that object through the fwDecoratorExtension.
// AUTHOR :		Jason Jurecka.
// CREATED :	2/6/2011
/////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  INCLUDES
///////////////////////////////////////////////////////////////////////////////

//rage
#include "bank\bkmgr.h"
#include "bank\bank.h"

//Framework
#include "fwsys\gameskeleton.h"
#include "fwdecorator\decoratorInterface.h"
#include "fwdecorator\decoratorExtension.h"
#include "fwdecorator\decorator_channel.h"

DECORATOR_OPTIMISATIONS();

namespace rage {

///////////////////////////////////////////////////////////////////////////////
//  STATIC CODE
///////////////////////////////////////////////////////////////////////////////
bool fwDecoratorExtension::ExistsOn (fwExtensibleBase& exBase, const atHashWithStringBank& key)
{
    fwDecoratorExtension *extension = exBase.GetExtension<fwDecoratorExtension>();
    if (extension)
    {
        return extension->Exists(key);
    }

    return false;
}

bool fwDecoratorExtension::RemoveFrom (fwExtensibleBase& exBase, const atHashWithStringBank& key)
{
    fwDecoratorExtension *extension = exBase.GetExtension<fwDecoratorExtension>();
    if (extension)
    {
        extension->Remove(key);

        if (!extension->GetCount())
        {
            //remove the extension cause we have no more decorators
            exBase.DestroyExtension<fwDecoratorExtension>();
        }

        return true;
    }

    return false;
}

bool fwDecoratorExtension::RemoveAllFrom (fwExtensibleBase& exBase)
{
    fwDecoratorExtension *extension = exBase.GetExtension<fwDecoratorExtension>();
    if (extension)
    {
        extension->RemoveAll();
        //remove the extension cause we have no more decorators
        exBase.DestroyExtension<fwDecoratorExtension>();
        return true;
    }

    return false;
}


///////////////////////////////////////////////////////////////////////////////
//  CODE
///////////////////////////////////////////////////////////////////////////////
AUTOID_IMPL(fwDecoratorExtension);

#if DECORATOR_DEBUG
fwDecoratorExtension::fwDecoratorExtension(fwExtensibleBase& base) :
#else
fwDecoratorExtension::fwDecoratorExtension(fwExtensibleBase& /*base*/) :
#endif
m_Root(NULL),
m_Count(0)
{
#if DECORATOR_DEBUG
    m_pBase = &base;
#endif
}

fwDecoratorExtension::~fwDecoratorExtension()
{
    RemoveAll();
#if DECORATOR_DEBUG
    m_pBase = NULL;
#endif
}

#if COMMERCE_CONTAINER
FW_INSTANTIATE_CLASS_POOL_NO_FLEX(fwDecoratorExtension, CONFIGURED_FROM_FILE, atHashString("DecoratorExtension",0xe0a96c19));
#else
FW_INSTANTIATE_CLASS_POOL(fwDecoratorExtension, CONFIGURED_FROM_FILE, atHashString("DecoratorExtension",0xe0a96c19));
#endif

void fwDecoratorExtension::Update (const atHashWithStringBank& key, bool val)
{
    fwDecorator* dec = Find(key);
    if (!dec)
    {
        //add it 
        dec = rage_new fwDecorator(key, fwDecorator::TYPE_BOOL);
        Add(dec);
    }
    dec->Set(val);
}

void fwDecoratorExtension::Update (const atHashWithStringBank& key, float val)
{
    fwDecorator* dec = Find(key);
    if (!dec)
    {
        //add it 
        dec = rage_new fwDecorator(key, fwDecorator::TYPE_FLOAT);
        Add(dec);
    }
    dec->Set(val);
}

void fwDecoratorExtension::Update (const atHashWithStringBank& key, int val)
{
    fwDecorator* dec = Find(key);
    if (!dec)
    {
        //add it 
		dec = rage_new fwDecorator(key, fwDecorator::TYPE_INT);
        Add(dec);
    }
    dec->Set(val);
}

void fwDecoratorExtension::Update (const atHashWithStringBank& key, atHashWithStringBank &val)
{
    fwDecorator* dec = Find(key);
    if (!dec)
    {
        //add it 
        dec = rage_new fwDecorator(key, fwDecorator::TYPE_STRING);
        Add(dec);
    }
    dec->Set(val);
}

void fwDecoratorExtension::Update (const atHashWithStringBank& key, fwDecorator::Time val)
{
	fwDecorator* dec = Find(key);
	if (!dec)
	{
		//add it 
		dec = rage_new fwDecorator(key, fwDecorator::TYPE_TIME);
		Add(dec);
	}
	dec->Set(val);
}

void fwDecoratorExtension::Add (fwDecorator* newDecorator)
{
    Assertf(newDecorator, "Passed in invalid decorator to fwDecoratorExtension::Add");

    newDecorator->m_Next = m_Root;
    m_Root = newDecorator;
    m_Count++;
}

fwDecorator* fwDecoratorExtension::Find(const atHashWithStringBank& key)
{
    gp_DecoratorInterface->IsRegistered(key);

    fwDecorator* found = NULL;
    fwDecorator* current = m_Root;
    while (current)
    {
        if (current->GetKey() == key)
        {
            found = current;
            break;
        }
        current = current->m_Next;
    }

    return found;
}

bool fwDecoratorExtension::Exists (const atHashWithStringBank& key)
{
    bool found = false;
    if (Find(key))
    {
        found = true;
    }

    return found;
}

void fwDecoratorExtension::Remove (const atHashWithStringBank& key)
{
    gp_DecoratorInterface->IsRegistered(key);

    fwDecorator* current = m_Root;
    fwDecorator* prev = NULL;
    while (current)
    {
        if (current->GetKey() == key)
        {
            //isolate it 
            if (m_Root == current)
            {
                m_Root = current->m_Next;
            }

            if (prev)
            {
                prev->m_Next = current->m_Next;
            }

            //delete it
            delete current;
            m_Count--;
            break;
        }
        prev = current;
        current = current->m_Next;
    }
}

void fwDecoratorExtension::RemoveAll ()
{
    fwDecorator* current = m_Root;
    while (current)
    {
        //isolate it 
        fwDecorator* todelete = current;
        current = current->m_Next;

        //delete it
        delete todelete;
        m_Count--;
    }
    m_Root = NULL;

    decAssertf(!m_Count, "Somehow our count is not 0 after a RemoveAll");
}

#if DECORATOR_DEBUG
void fwDecoratorExtension::SpewDecorators()
{
    //NOTE: expects you to have already dumped some sort of identifying marker so you know what object this is
    decDisplayf("Applied Decorators");
    fwDecorator* current = m_Root;
    while (current)
    {
        gp_DecoratorInterface->SpewDecorator((*current));
        current = current->m_Next;
    }
}

void fwDecoratorExtension::SpewOneDecorator(void* pItem)
{
	//NOTE: expects you to have already dumped some sort of identifying marker so you know what object this is
	decDisplayf("Applied Decorators");
	if(pItem)
		reinterpret_cast<fwDecoratorExtension*>(pItem)->SpewDecorators();
}

void fwDecoratorExtension::RenderDecorators (Vec3V_In location)
{
    Vec3V local = location;
    fwDecorator* current = m_Root;
    while (current)
    {
        gp_DecoratorInterface->RenderDecorator(local, (*current));
        local += Vec3V(0.0f,0.0f, fwDecoratorInterface::sm_renderOffset); //shift the position up a bit before we draw the next.
        current = current->m_Next;
    }
}
#endif

} // namespace rage
