/////////////////////////////////////////////////////////////////////////////////
// FILE :		decorator_channel.h
// PURPOSE :	spew channel for decorators
// AUTHOR :		Jason Jurecka.
// CREATED :	30/6/2011
/////////////////////////////////////////////////////////////////////////////////

#ifndef FW_DECORATOR_DECORATOR_CHANNEL_H 
#define FW_DECORATOR_DECORATOR_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(decorator)

#define decAssert(cond)						RAGE_ASSERT(decorator,cond)
#define decAssertf(cond,fmt,...)			RAGE_ASSERTF(decorator,cond,fmt,##__VA_ARGS__)
#define decFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(decorator,cond,fmt,##__VA_ARGS__)
#define decVerifyf(cond,fmt,...)			RAGE_VERIFYF(decorator,cond,fmt,##__VA_ARGS__)
#define decErrorf(fmt,...)					RAGE_ERRORF(decorator,fmt,##__VA_ARGS__)
#define decWarningf(fmt,...)				RAGE_WARNINGF(decorator,fmt,##__VA_ARGS__)
#define decDisplayf(fmt,...)				RAGE_DISPLAYF(decorator,fmt,##__VA_ARGS__)
#define decDebugf1(fmt,...)					RAGE_DEBUGF1(decorator,fmt,##__VA_ARGS__)
#define decDebugf2(fmt,...)					RAGE_DEBUGF2(decorator,fmt,##__VA_ARGS__)
#define decDebugf3(fmt,...)					RAGE_DEBUGF3(decorator,fmt,##__VA_ARGS__)
#define decLogf(severity,fmt,...)			RAGE_LOGF(decorator,severity,fmt,##__VA_ARGS__)

#define DECORATOR_OPTIMISATIONS_OFF	0

#if DECORATOR_OPTIMISATIONS_OFF
#define DECORATOR_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define DECORATOR_OPTIMISATIONS()
#endif	//DECORATOR_OPTIMISATIONS_OFF

#define DECORATOR_DEBUG __BANK

#endif // FW_DECORATOR_DECORATOR_CHANNEL_H
