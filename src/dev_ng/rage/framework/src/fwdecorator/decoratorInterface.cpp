/////////////////////////////////////////////////////////////////////////////////
// FILE :		DecoratorInterface.cpp
// PURPOSE :	Interface for debug drawing and rag so it can be easily separated from core code
//               also includes a registry for all decorator names so we can enforce type usage
// AUTHOR :		Jason Jurecka.
// CREATED :	24/6/2011
/////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  INCLUDES
///////////////////////////////////////////////////////////////////////////////

// Rage includes
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "grcore/debugdraw.h"
#include "file/limits.h"

// Framework includes
#include "entity/entity.h"
#include "fwdecorator/decoratorExtension.h"
#include "fwdecorator/decoratorInterface.h"
#include "fwsys/gameskeleton.h"

DECORATOR_OPTIMISATIONS();

namespace rage {
///////////////////////////////////////////////////////////////////////////////
//  GLOBAL VARIABLES
///////////////////////////////////////////////////////////////////////////////
fwDecoratorInterface* gp_DecoratorInterface = NULL;

///////////////////////////////////////////////////////////////////////////////
//  STATIC CODE
///////////////////////////////////////////////////////////////////////////////
#if DECORATOR_DEBUG
float fwDecoratorInterface::sm_renderOffset = 0.09f;
#endif

void fwDecoratorInterface::Init (unsigned initMode)
{
    //INIT_CORE
    //INIT_BEFORE_MAP_LOADED
    //INIT_AFTER_MAP_LOADED
    //INIT_SESSION

    if (initMode == INIT_SESSION)
    {
        fwDecoratorExtension::InitPool( MEMBUCKET_GAMEPLAY );
        fwDecorator::InitPool( MEMBUCKET_GAMEPLAY );

#if __DEV
		fwDecoratorExtension::GetPool()->RegisterPoolCallback(fwDecoratorInterface::SpewOneDecorator);
#endif // __DEV
    }
}

void fwDecoratorInterface::Shutdown (unsigned shutdownMode)
{
    //SHUTDOWN_CORE
    //SHUTDOWN_WITH_MAP_LOADED
    //SHUTDOWN_SESSION

    if (shutdownMode == SHUTDOWN_SESSION)
    {
        fwDecoratorExtension::ShutdownPool();
        fwDecorator::ShutdownPool();
    }
}

void fwDecoratorInterface::InitClass (fwDecoratorInterface* decoratorInterface)
{
    decAssertf(!gp_DecoratorInterface, "Possible multiple calls to fwDecoratorInterface::InitClass gp_DecoratorInterface is already initialized.");
    
    gp_DecoratorInterface = decoratorInterface;
}

void fwDecoratorInterface::ShutdownClass ()
{
    if (gp_DecoratorInterface)
    {
        delete gp_DecoratorInterface;
        gp_DecoratorInterface = NULL;
    }
}

#if DECORATOR_DEBUG
void fwDecoratorInterface::SpewDecorators ()
{
    fwDecoratorExtension::Pool* p = fwDecoratorExtension::GetPool();
    s32 size = p->GetSize();
    while(size--)
    {
        fwDecoratorExtension* pItem = p->GetSlot(size);
        if(pItem)
        {
            pItem->SpewDecorators();
        }
    }
}

void fwDecoratorInterface::SpewOneDecorator (void* pItem)
{
	if(pItem)
		reinterpret_cast<fwDecoratorExtension*>(pItem)->SpewOneDecorator(pItem);
}
#endif

///////////////////////////////////////////////////////////////////////////////
//  CODE
///////////////////////////////////////////////////////////////////////////////
fwDecoratorInterface::fwDecoratorInterface ()
{
#if DECORATOR_DEBUG
    m_renderColor.Set(0,0,0);
    m_renderDebug = false;
#endif
}

fwDecoratorInterface::~fwDecoratorInterface ()
{
    m_Registered.Kill();
}

#if DECORATOR_DEBUG
void fwDecoratorInterface::Register (const atHashWithStringBank& key, fwDecorator::Type type, const char* whoIsRegistering)
#else
void fwDecoratorInterface::Register (const atHashWithStringBank& key, fwDecorator::Type type, const char* /*whoIsRegistering*/)
#endif
{
    fwDecorator::Type* pData = m_Registered.Access(key.GetHash());
    if (!pData)
    {
        m_Registered.Insert(key.GetHash(),type);
#if DECORATOR_DEBUG
        atHashWithStringBank owner(whoIsRegistering);
        m_RegisteredOwners.Insert(key.GetHash(),owner);
#endif
    }
    else
    {
#if DECORATOR_DEBUG
        if ((*pData) != type)
        {

            atHashWithStringBank* original = m_RegisteredOwners.Access(key.GetHash());
            Quitf("{%s} is trying to register Decorator [%s] as type (%s) but it has already been registered as type (%s) by {%s}", whoIsRegistering, key.GetCStr(), fwDecorator::TypeAsString(type), fwDecorator::TypeAsString((*pData)), original->GetCStr());
        }
#endif
    }
}

bool fwDecoratorInterface::IsRegisteredAsType (const atHashWithStringBank& key, fwDecorator::Type type)
{
    const fwDecorator::Type* pData = m_Registered.Access(key.GetHash());
    if (pData)
    {
        if ((*pData) == type)
        {
            return true;
        }
    }
    return false;
}

bool fwDecoratorInterface::IsRegistered (const atHashWithStringBank& key)
{
    if (m_Registered.Access(key.GetHash()))
    {
        return true;
    }
    decAssertf(0, "Decorator {%s}[%d] is unregistered", key.GetCStr(), key.GetHash());
    return false;
}

fwDecorator::Type fwDecoratorInterface::GetRegisteredType (const atHashWithStringBank& key)
{
    const fwDecorator::Type* pData = m_Registered.Access(key.GetHash());
    if (!pData)
    {
        decAssertf(0, "Decorator with name %s and key %d is not registered.", key.GetCStr(), key.GetHash());
        return fwDecorator::TYPE_UNKNOWN;
    }

    return (*pData);
}

#if DECORATOR_DEBUG
void fwDecoratorInterface::SpewDecorator (const fwDecorator &dec)
{
    const char* who = "NO REGISTERED OWNER";
    atHashWithStringBank* own = m_RegisteredOwners.Access(dec.GetKey().GetHash());
    if (own)
    {
        who = own->GetCStr();
    }

    switch(dec.GetType())
    {
    case fwDecorator::TYPE_BOOL:
        {
            bool val = false;
            dec.Get(val);
            decDisplayf("  Hash 0x%x,Name '%s',Type %s,Value %d,Register '%s'", dec.GetKey().GetHash(), dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val, who);
        }
        break;
    case fwDecorator::TYPE_INT:
        {
            int val = 0;
            dec.Get(val);
            decDisplayf("  Hash 0x%x,Name '%s',Type %s,Value %d,Register '%s'", dec.GetKey().GetHash(), dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val, who);
        }
        break;
    case fwDecorator::TYPE_FLOAT:
        {
            float val = 0.0f;
            dec.Get(val);
            decDisplayf("  Hash 0x%x,Name '%s',Type %s,Value %f,Register '%s'", dec.GetKey().GetHash(), dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val, who);
        }
        break;
    case fwDecorator::TYPE_STRING:
        {
            atHashWithStringBank val;
            dec.Get(val);
            decDisplayf("  Hash 0x%x,Name '%s',Type %s,Value %d,Register '%s'", dec.GetKey().GetHash(), dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val.GetHash(), who);
        }
        break;
	case fwDecorator::TYPE_TIME:
		{
			fwDecorator::Time val = fwDecorator::UNDEF_TIME;
			dec.Get(val);
			decDisplayf("  Hash 0x%x,Name '%s',Type %s,Value %d,Register '%s'", dec.GetKey().GetHash(), dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val, who);
		}
		break;
    case fwDecorator::TYPE_UNKNOWN:default:
        {
            decDisplayf("  Hash 0x%x,Name '%s',Type %s,Register '%s'", dec.GetKey().GetHash(), dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), who);
        }
        break;
    };
}

void fwDecoratorInterface::RenderDecorator (Vec3V_In position, const fwDecorator &dec)
{
    char line[RAGE_MAX_PATH];

    switch(dec.GetType())
    {
    case fwDecorator::TYPE_BOOL:
        {
            bool val = false;
            dec.Get(val);
            sprintf(line,"%s,%s,%d", dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val);
        }
        break;
    case fwDecorator::TYPE_INT:
        {
            int val = 0;
            dec.Get(val);
            sprintf(line,"%s,%s,%d", dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val);
        }
        break;
    case fwDecorator::TYPE_FLOAT:
        {
            float val = 0.0f;
            dec.Get(val);
            sprintf(line,"%s,%s,%f", dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val);
        }
        break;
    case fwDecorator::TYPE_STRING:
        {
            atHashWithStringBank val;
            dec.Get(val);
            sprintf(line,"%s,%s,%d", dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val.GetHash());
        }
        break;
	case fwDecorator::TYPE_TIME:
		{
			fwDecorator::Time val = fwDecorator::UNDEF_TIME;
			dec.Get(val);
			sprintf(line,"%s,%s,%d", dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()), val);
		}
		break;
    case fwDecorator::TYPE_UNKNOWN:default:
        {
            sprintf(line,"%s,%s", dec.GetKey().GetCStr(), fwDecorator::TypeAsString(dec.GetType()));
        }
        break;
    };

    grcDebugDraw::Text(position, m_renderColor, line);
}

bool fwDecoratorInterface::GetOffset(fwExtensibleBase* pBase, Vec3V_InOut position)
{
    bool typefound = false;
   
    fwEntity* entity = dynamic_cast<fwEntity*>(pBase);
    if (decVerifyf(entity, "Extension found on 0X%p - dynamic cast failed so its either NULL or deleted", pBase))
    {
        const fwTransform &trans = entity->GetTransform();
        position = trans.GetPosition();
        typefound = true;
    }

    return typefound;
}

void fwDecoratorInterface::RenderDebug()
{
    if (m_renderDebug)
    {
        fwDecoratorExtension::Pool* p = fwDecoratorExtension::GetPool();
        s32 size = p->GetSize();
        while(size--)
        {
            fwDecoratorExtension* pItem = p->GetSlot(size);
            if(pItem)
            {
                fwExtensibleBase* pBase = pItem->GetBase();
                Vec3V objPos(V_ZERO);
                if (!GetOffset(pBase, objPos))
                {
                    decAssertf(0, "Could not get the position for the fwDecoratorExtension* drawing debug info at 0,0,0");
                }
                pItem->RenderDecorators(objPos);
            }
        }
    }
}

void fwDecoratorInterface::InitWidgets()
{
    bkBank* pBank = &BANKMGR.CreateBank("Decorators", 0, 0, false);
    pBank->AddToggle("Render All", &m_renderDebug);
    pBank->AddColor("Text Color", &m_renderColor);
    pBank->AddSlider("Text Render Offset", &sm_renderOffset, 0.01f, 10.0f, 0.01f);
    pBank->AddButton("Spew Decorators", SpewDecorators);
}
#endif

} // namespace rage
