/////////////////////////////////////////////////////////////////////////////////
// FILE :		Decorator.h
// PURPOSE :	The decorator with type and raw data.
// AUTHOR :		Jason Jurecka.
// CREATED :	2/6/2011
/////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  INCLUDES
///////////////////////////////////////////////////////////////////////////////
#include "fwdecorator/decorator.h"
#include "fwdecorator/decorator_channel.h"
#include "fwdecorator/decoratorInterface.h"

DECORATOR_OPTIMISATIONS();

namespace rage {

///////////////////////////////////////////////////////////////////////////////
//  STATIC CODE
///////////////////////////////////////////////////////////////////////////////

#if DECORATOR_DEBUG
const char* fwDecorator::TypeAsString(Type type)
{
    switch(type)
    {
    case fwDecorator::TYPE_UNKNOWN:default:return "unknown";
    case fwDecorator::TYPE_FLOAT:          return "float";
    case fwDecorator::TYPE_BOOL:           return "bool";
    case fwDecorator::TYPE_INT:            return "int";
    case fwDecorator::TYPE_STRING:         return "string";
	case fwDecorator::TYPE_TIME:		   return "time";
    }
}
#endif // DECORATOR_DEBUG

///////////////////////////////////////////////////////////////////////////////
//  CODE
///////////////////////////////////////////////////////////////////////////////
FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwDecorator, 600, 0.56f, atHashString("Decorator",0xe54ea79d));

fwDecorator::fwDecorator () :
m_Next(NULL),
m_Type(TYPE_UNKNOWN),
m_Unsigned(0)
{

}

fwDecorator::fwDecorator (const atHashWithStringBank &key, Type type) :
m_Next(NULL),
m_Type(TYPE_UNKNOWN),
m_Unsigned(0)
{
    Init(key,type);
}

fwDecorator::fwDecorator (fwDecorator &dec) :
m_Next(NULL),
m_Type(TYPE_UNKNOWN),
m_Unsigned(0)
{
    Set(dec);
}

fwDecorator::~fwDecorator()
{
    m_Next = NULL;
    m_Type = TYPE_UNKNOWN;
    m_Unsigned = 0;
}

void fwDecorator::Init(const atHashWithStringBank &key, Type type)
{
    if (RegisteredCheck(key))
    {
        m_Key = key;
        m_Type = type;
        m_Unsigned = 0; //reset the data
    }
}

bool fwDecorator::Get (bool &out) const
{
    bool outValid = false;
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_BOOL))
    {
        out = m_Bool;
        outValid = true;
    }
    return outValid;
}

bool fwDecorator::Get (float &out) const
{
    bool outValid = false;
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_FLOAT))
    {
        out = m_Float;
        outValid = true;
    }
    return outValid;
}

bool fwDecorator::Get (int &out) const
{
    bool outValid = false;
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_INT))
    {
        out = m_Signed;
        outValid = true;
    }
    return outValid;
}

bool fwDecorator::Get (atHashWithStringBank &out) const
{
    bool outValid = false;
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_STRING))
    {
        out.SetHash(m_Unsigned);
        outValid = true;
    }
    return outValid;
}

bool fwDecorator::Get (fwDecorator &out) const
{
    bool outValid = false;
    if (RegisteredCheck(m_Key))
    {
        out.Set((*this));
        outValid = true;
    }
    return outValid;
}

bool fwDecorator::Get (Time &out) const
{
	bool outValid = false;
	if (RegisteredCheck(m_Key) && TypeCheck(TYPE_TIME))
	{
		out = (Time)m_Signed;
		outValid = true;
	}
	return outValid;
}

void fwDecorator::Set (const bool val)
{
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_BOOL))
    {
        m_Bool = val;
    }
}

void fwDecorator::Set (const float val)
{
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_FLOAT))
    {
        m_Float = val;
    }
}

void fwDecorator::Set (const int val)
{
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_INT))
    {
        m_Signed = val;
    }
}

void fwDecorator::Set (const atHashWithStringBank &val)
{
    if (RegisteredCheck(m_Key) && TypeCheck(TYPE_STRING))
    {
        m_Unsigned = val.GetHash();
    }
}

void fwDecorator::Set (const fwDecorator &val)
{
    Init(val.m_Key, val.m_Type);
    m_Unsigned = val.m_Unsigned;
}

void fwDecorator::Set (const Time val)
{
	if (RegisteredCheck(m_Key) && TypeCheck(TYPE_TIME))
	{
		m_Signed = val;
	}
}

bool fwDecorator::TypeCheck(Type beType) const
{
    bool istype = true;
    if (m_Type != beType)
    {
		istype = false;
		decAssertf(istype, "Trying to use Decorator {%s}[%d] of type %s as type %s", m_Key.GetCStr(), m_Key.GetHash(), fwDecorator::TypeAsString(m_Type), fwDecorator::TypeAsString(beType));
    }
    return istype;
}

bool fwDecorator::RegisteredCheck(const atHashWithStringBank &beKey) const
{
    bool isRegistered = gp_DecoratorInterface->IsRegistered(beKey);
    decAssertf(isRegistered, "Trying to create a decorator with unregistered key {%s}[%d]", beKey.GetCStr(), beKey.GetHash());
    return isRegistered;
}

} // namespace rage