/////////////////////////////////////////////////////////////////////////////////
// FILE :		Decorator.h
// PURPOSE :	The decorator with type and raw data.
// AUTHOR :		Jason Jurecka.
// CREATED :	2/6/2011
/////////////////////////////////////////////////////////////////////////////////
#ifndef FW_DECORATOR_H_
#define FW_DECORATOR_H_

#include "atl/hashstring.h"
#include "fwtl/Pool.h"
#include "fwdecorator/decorator_channel.h"

namespace rage {

class fwDecorator
{
public:

	enum Time {UNDEF_TIME = 0,};

    typedef enum DECORATOR_TYPES
    {
        TYPE_UNKNOWN=0,
        TYPE_FLOAT,
        TYPE_BOOL,
        TYPE_INT,
        TYPE_STRING,
		TYPE_TIME,
    } Type;

            fwDecorator ();
            fwDecorator (const atHashWithStringBank &key, Type type);
            fwDecorator (fwDecorator &dec);
    virtual ~fwDecorator();

    FW_REGISTER_CLASS_POOL(fwDecorator);

    //////////////////////////////////////////////////////////////////////////
    bool                    Get     (bool &out) const;
    bool                    Get     (float &out) const;
    bool                    Get     (int &out) const;
    bool                    Get     (atHashWithStringBank &out) const;
    bool                    Get     (fwDecorator &out) const;
	bool                    Get     (Time &out) const;
    Type                    GetType () const {return m_Type;}
    atHashWithStringBank    GetKey  () const {return m_Key;}

    //////////////////////////////////////////////////////////////////////////
    void Set (const bool val);
    void Set (const float val);
    void Set (const int val);
    void Set (const atHashWithStringBank &val);
    void Set (const fwDecorator &val);
	void Set (const Time val);

    void Init (const atHashWithStringBank &key, Type type);

    fwDecorator*         m_Next;    //the next decorator in the linked list stored in fwDecoratorExtension

#if DECORATOR_DEBUG
	static const char* TypeAsString(Type type);
#endif // DECORATOR_DEBUG

private:

    bool TypeCheck      (Type beType) const;
    bool RegisteredCheck(const atHashWithStringBank &beKey) const;

    Type                 m_Type;    //type flags stored here
    atHashWithStringBank m_Key;     //the string hash of the decorator name value
    union
    {
        bool    m_Bool;
        s32     m_Signed;
        u32     m_Unsigned;
        float   m_Float;
    };
};


} //namespace rage

#endif //FW_DECORATOR_H_
