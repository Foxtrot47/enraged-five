/////////////////////////////////////////////////////////////////////////////////
// FILE :		decorator_channel.cpp
// PURPOSE :	spew channel for decorators
// AUTHOR :		Jason Jurecka.
// CREATED :	30/6/2011
/////////////////////////////////////////////////////////////////////////////////

// Rage Includes
#include "system/param.h"

// Framework Includes
#include "fwdecorator/decorator_channel.h"

DECORATOR_OPTIMISATIONS();

RAGE_DEFINE_CHANNEL(decorator)
