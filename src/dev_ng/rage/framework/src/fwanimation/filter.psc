<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="fwFrameFilter_TrackIdIndex" autoregister="true">
	<u8 name="m_Track"/>
	<enum name="m_Id" type="eAnimBoneTag"/>
	<u8 name="m_WeightIndex"/>
</structdef>

<structdef type="fwFrameFilter" autoregister="true">
	<string name="m_Name" type="atHashString"/>
	<array name="m_TrackIdIndices" type="atArray">
		<struct type="fwFrameFilter_TrackIdIndex"/>
	</array>
	<array name="m_Weights" type="atArray">
		<float/>
	</array>
</structdef>

<structdef type="fwFrameFilterDictionary" autoregister="true">
	<string name="m_Name" type="atHashString"/>
	<array name="m_FrameFilters" type="atArray">
		<struct type="fwFrameFilter"/>
	</array>
</structdef>

<structdef type="fwFrameFilterDictionaryStore" autoregister="true">
	<array name="m_FrameFilterDictionaries" type="atArray">
		<struct type="fwFrameFilterDictionary"/>
	</array>
</structdef>

</ParserSchema>