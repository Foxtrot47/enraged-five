#include "fwanimation/movedump.h"

#if __BANK

#include "cranimation/frameiterators.h"
#include "creature/componentextradofs.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tagiterators.h"
#include "entity/entity.h"
#include "fwanimation/animdirector.h"
#include "fwanimation/directorcomponentmove.h"
#include "fwanimation/filter.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "fwsys/timer.h"
#include "grcore/debugdraw.h"
#include "move/move_config.h"
#include "move/move_network.h"
#include "move/move_state.h"
#include "move/move_statedef.h"
#include "parser/psofile.h"
#include "parser/psoparserbuilder.h"
#include "parser/visitorxml.h"
#include "parsercore/streamxml.h"
#include "vector/colors.h"

using namespace rage;

#include "movedump_parser.h"

#define FLATTEN_CREATE_MOVE_DUMP_NODE (1)
#define FLATTEN_WALK_AND_RENDER (1)

namespace rage {

///////////////////////////////////////////////////////////////////////////////

PARAM(capturemotiontree, "Enable motion tree capture from startup");
PARAM(capturemotiontreecount, "Number of motion tree frames to capture");

///////////////////////////////////////////////////////////////////////////////

const char *g_szMotionTreeDumpDirectory = "X:/gta5";
const char *g_szMoveDumpExtension = "xml";

///////////////////////////////////////////////////////////////////////////////

void fwMoveDumpPrinterFile::PrintLine(int iIndent, const char *szText)
{
	if(szText && szText[0] != '\0')
	{
		fwMoveDumpString textString;

		for(int i = 0; i < iIndent; i ++)
		{
			textString += " ";
		}

		textString += szText;

		int num = textString.GetLength();
		if(num > 0)
		{
			mp_File->WriteByte(textString.c_str(), num);
			mp_File->WriteByte("\n", 1);
		}
	}
}

void fwMoveDumpPrinterTTY::PrintLine(int iIndent, const char *szText)
{
	if(szText && szText[0] != '\0')
	{
		fwMoveDumpString textString;

		for(int i = 0; i < iIndent; i ++)
		{
			textString += " ";
		}

		textString += szText;

		int num = textString.GetLength();
		if(num > 0)
		{
			Displayf("%s", textString.c_str());
		}
	}
}


///////////////////////////////////////////////////////////////////////////////

fwMoveDumpNode *CreateMoveDumpNode(const fwEntity *pEntity, const crmtNode &node)
{
	//	crmtNode
	//		crmtNodeAnimation
	//		crmtNodeClip
	//		crmtNodeFrame
	//		crmtNodeIdentity
	//		crmtNodeInvalid
	//		crmtNodeParent
	//			crmtNodeCapture
	//			crmtNodeExpression
	//			crmtNodeExtrapolate
	//			crmtNodeFilter
	//			crmtNodeIk
	//			crmtNodeJointLimit
	//			crmtNodeMerge
	//			crmtNodeMirror
	//			crmtNodePair
	//				crmtNodeAddSubtract
	//				crmtNodeBlend
	//			crmtNodeN
	//				crmtNodeAddN
	//				crmtNodeBlendN
	//				crmtNodeMergeN
	//		crmtNodePm
	//		crmtNodePose
	//		crmtNodeProxy

	USE_DEBUG_ANIM_MEMORY();

	fwMoveDumpNode *pNode = NULL;

	switch(node.GetNodeType())
	{
	case crmtNode::kNodeMergeN:
		{
			pNode = rage_new fwMoveDumpNodeMergeN(pEntity, static_cast< const crmtNodeMergeN & >(node));
		} break;
	case crmtNode::kNodeBlendN:
		{
			pNode = rage_new fwMoveDumpNodeBlendN(pEntity, static_cast< const crmtNodeBlendN & >(node));
		} break;
	case crmtNode::kNodeAddN:
		{
			pNode = rage_new fwMoveDumpNodeAddN(pEntity, static_cast< const crmtNodeAddN & >(node));
		} break;
	case crmtNode::kNodeN:
		{
			pNode = rage_new fwMoveDumpNodeN(pEntity, static_cast< const crmtNodeN & >(node));
		} break;
	case crmtNode::kNodeBlend:
		{
			pNode = rage_new fwMoveDumpNodeBlend(pEntity, static_cast< const crmtNodeBlend & >(node));
		} break;
	case crmtNode::kNodeAddSubtract:
		{
			pNode = rage_new fwMoveDumpNodeAddSubtract(pEntity, static_cast< const crmtNodeAddSubtract & >(node));
		} break;
		//case crmtNode::kNodePair:
		//{
		//	pNode = rage_new fwMoveDumpNodePair(pEntity, static_cast< const crmtNodePair & >(node));
		//} break;
	case crmtNode::kNodeStateMachine:
	case crmtNode::kNodeStateRoot:
	case crmtNode::kNodeSubNetwork:
		{
			pNode = rage_new fwMoveDumpNodeState(pEntity, static_cast< const crmtNodeParent & >(node));
		} break;
	case crmtNode::kNodeMirror:
		{
			pNode = rage_new fwMoveDumpNodeMirror(pEntity, static_cast< const crmtNodeMirror & >(node));
		} break;
	case crmtNode::kNodeMerge:
		{
			pNode = rage_new fwMoveDumpNodeMerge(pEntity, static_cast< const crmtNodeMerge & >(node));
		} break;
	case crmtNode::kNodeJointLimit:
		{
			pNode = rage_new fwMoveDumpNodeJointLimit(pEntity, static_cast< const crmtNodeJointLimit & >(node));
		} break;
	case crmtNode::kNodeIk:
		{
			pNode = rage_new fwMoveDumpNodeIk(pEntity, static_cast< const crmtNodeIk & >(node));
		} break;
	case crmtNode::kNodeFilter:
		{
			pNode = rage_new fwMoveDumpNodeFilter(pEntity, static_cast< const crmtNodeFilter & >(node));
		} break;
	case crmtNode::kNodeExtrapolate:
		{
			pNode = rage_new fwMoveDumpNodeExtrapolate(pEntity, static_cast< const crmtNodeExtrapolate & >(node));
		} break;
	case crmtNode::kNodeExpression:
		{
			pNode = rage_new fwMoveDumpNodeExpression(pEntity, static_cast< const crmtNodeExpression & >(node));
		} break;
	case crmtNode::kNodeCapture:
		{
			pNode = rage_new fwMoveDumpNodeCapture(pEntity, static_cast< const crmtNodeCapture & >(node));
		} break;
	case crmtNode::kNodeParent:
		{
			pNode = rage_new fwMoveDumpNodeParent(pEntity, static_cast< const crmtNodeParent & >(node));
		} break;
	case crmtNode::kNodeProxy:
		{
			pNode = rage_new fwMoveDumpNodeProxy(pEntity, static_cast< const crmtNodeProxy & >(node));
		} break;
	case crmtNode::kNodePose:
		{
			pNode = rage_new fwMoveDumpNodePose(pEntity, static_cast< const crmtNodePose & >(node));
		} break;
	case crmtNode::kNodePm:
		{
			pNode = rage_new fwMoveDumpNodePm(pEntity, static_cast< const crmtNodePm & >(node));
		} break;
	case crmtNode::kNodeInvalid:
		{
			pNode = rage_new fwMoveDumpNodeInvalid(pEntity, static_cast< const crmtNodeInvalid & >(node));
		} break;
	case crmtNode::kNodeIdentity:
		{
			pNode = rage_new fwMoveDumpNodeIdentity(pEntity, static_cast< const crmtNodeIdentity & >(node));
		} break;
	case crmtNode::kNodeFrame:
		{
			pNode = rage_new fwMoveDumpNodeFrame(pEntity, static_cast< const crmtNodeFrame & >(node));
		} break;
	case crmtNode::kNodeClip:
		{
			pNode = rage_new fwMoveDumpNodeClip(pEntity, static_cast< const crmtNodeClip & >(node));
		} break;
	case crmtNode::kNodeAnimation:
		{
			pNode = rage_new fwMoveDumpNodeAnimation(pEntity, static_cast< const crmtNodeAnimation & >(node));
		} break;
	case crmtNode::kNodeBase:
		{
			pNode = rage_new fwMoveDumpNode(pEntity, node);
		} break;
	case crmtNode::kNodeRagdoll:
	case crmtNode::kNodeMover:
	case crmtNode::kNodePonyTail:
	default:
		{
			Assert(false);
		} break;
	}

	return pNode;
}

///////////////////////////////////////////////////////////////////////////////

/* Destructor */

fwMoveDumpNode::~fwMoveDumpNode()
{
	USE_DEBUG_ANIM_MEMORY();

	for(int i = 0; i < m_Children.GetCount(); i ++)
	{
		delete m_Children[i]; m_Children[i] = NULL;
	}
	m_Children.Reset();
}

/* Constructors */

#if !FLATTEN_CREATE_MOVE_DUMP_NODE
fwMoveDumpNode::fwMoveDumpNode(const fwEntity *pEntity, const crmtNode &node)
#else // !FLATTEN_CREATE_MOVE_DUMP_NODE
	fwMoveDumpNode::fwMoveDumpNode(const fwEntity*, const crmtNode &node)
#endif // !FLATTEN_CREATE_MOVE_DUMP_NODE
{
#if CRMT_NODE_2_DEF_MAP
	if(PARAM_movedebug.Get())
	{
		const void **ppNodeDef = g_crmtNode2DefMap.Access(&node);
		Assert(ppNodeDef);
		if(ppNodeDef)
		{
			const mvNodeDef *pNodeDef = reinterpret_cast< const mvNodeDef * >(*ppNodeDef);
			if(pNodeDef)
			{
				const char *szNodeName = atHashString(pNodeDef->m_id).TryGetCStr();
				if(szNodeName)
				{
					m_Name = szNodeName;
				}
			}
		}
	}
#endif //CRMT_NODE_2_DEF_MAP

	char buffer[512];
	formatf(buffer, "%p", &node);
	m_Address = buffer;

#if !FLATTEN_CREATE_MOVE_DUMP_NODE
	Assert(pEntity);
	if(node.GetNumChildren() > 0)
	{
		m_Children.Reserve(node.GetNumChildren());

		for(crmtNode *pChildNode = node.GetFirstChild(); pChildNode; pChildNode = pChildNode->GetNextSibling())
		{
			m_Children.PushAndGrow(CreateMoveDumpNode(pEntity, *pChildNode));
		}
	}
#endif // !FLATTEN_CREATE_MOVE_DUMP_NODE
}

/* Properties */

fwMoveDumpNode::String fwMoveDumpNode::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	String description;

	if(m_Name.GetLength() > 0)
	{
		description += VarString("'%s'", m_Name.c_str());
	}

	if(renderFlags.m_bRenderAddress)
	{
		description += VarString(" (%s)", m_Address.c_str());
	}

	if(renderFlags.m_bRenderClass)
	{
		description += VarString(" %s", GetFriendlyClassName());
	}

	if(renderFlags.m_bRenderWeight && bIsParentBlend)
	{
		description += VarString(" %.3f", fWeight);
	}

	if(renderFlags.m_bRenderLocalWeight && bIsParentBlend)
	{
		description += VarString(" %.3f", fLocalWeight);
	}

	if(!description.c_str())
	{
		description = "";
	}

	return description;
}

/* Operations */

struct RenderNode
{
	RenderNode() {}
	RenderNode(const fwMoveDumpNode* node, float weight) 
		: m_Node(node), m_Index(0), m_Weight(weight), m_Start(FLT_MAX) {}

	const fwMoveDumpNode* m_Node;
	int m_Index;
	float m_Weight;
	float m_Start;
};

void fwMoveDumpNode::WalkAndRender(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend, unsigned& numNodes) const
{
#if FLATTEN_WALK_AND_RENDER
	atArray<RenderNode> nodes;
	nodes.PushAndGrow(RenderNode(this, fWeight*fLocalWeight));

	numNodes++;
	Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, fWeight*fLocalWeight, fLocalWeight, bIsParentBlend);

	do
	{
		const fwMoveDumpNode& node = *nodes.Top().m_Node;

		int index = nodes.Top().m_Index;
		if(index < node.m_Children.GetCount())
		{
			const fwMoveDumpNode& child = *node.m_Children[index];
			float childWeight = node.GetChildWeight(index);

			float start = fOriginY;
			bool line = (!child.IsInvalidNode() && !child.IsStateNode()) ||
				(child.IsStateNode() && (renderFlags.m_bRenderInvalidNodes || static_cast<const fwMoveDumpNodeState&>(child).IsInterestingState()));

			if(line)
			{
				float x = fOriginX;
				float y = start;

				grcDebugDraw::Line(Vector2(x, y), Vector2(x + (fHorizontalIndent / 2.0f), y), Color32(1.0f, 1.0f, 1.0f, 1.0f), Color32(1.0f, 1.0f, 1.0f, 1.0f));

				nodes.Top().m_Start = Min(nodes.Top().m_Start, start - (fVerticalIndent / 2.0f));
			}

			fOriginX += fHorizontalIndent;

			numNodes++;
			child.Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, nodes.Top().m_Weight*childWeight, childWeight, node.m_Children.GetCount() > 1);

			if(line)
			{
				float x = fOriginX - fHorizontalIndent;
				float y = start;

				grcDebugDraw::Line(Vector2(x, nodes.Top().m_Start), Vector2(x, y), Color32(1.0f, 1.0f, 1.0f, 1.0f), Color32(1.0f, 1.0f, 1.0f, 1.0f));
			}

			nodes.Top().m_Index++;
			nodes.PushAndGrow(RenderNode(node.m_Children[index], nodes.Top().m_Weight*childWeight));

			continue;
		}

		fOriginX -= fHorizontalIndent;

		nodes.Pop();
	}
	while(nodes.GetCount() > 0);	
#else // FLATTEN_WALK_AND_RENDER

	numNodes++;
	Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, fWeight, fLocalWeight, bIsParentBlend);

	float fStartX = fOriginX + (fHorizontalIndent / 2.0f), fStartY = fOriginY;
	float fCurrentY = 0.0f;

	// Indent
	fOriginX += fHorizontalIndent;

	int iChildCount = 0;

	// WalkAndRender each child recursively
	for(int i = 0; i < m_Children.GetCount(); i ++)
	{
		if((!m_Children[i]->IsInvalidNode() && !m_Children[i]->IsStateNode()) ||
			(m_Children[i]->IsStateNode() && (renderFlags.m_bRenderInvalidNodes || static_cast< fwMoveDumpNodeState * >(m_Children[i])->IsInterestingState())))
		{
			fCurrentY = fOriginY + (fVerticalIndent / 2.0f);

			iChildCount ++;
		}

		m_Children[i]->WalkAndRender(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, fWeight * GetChildWeight(i), GetChildWeight(i), m_Children.GetCount() > 1, numNodes);

		if((!m_Children[i]->IsInvalidNode() && !m_Children[i]->IsStateNode()) ||
			(m_Children[i]->IsStateNode() && (renderFlags.m_bRenderInvalidNodes || static_cast< fwMoveDumpNodeState * >(m_Children[i])->IsInterestingState())))
		{
			grcDebugDraw::Line(Vector2(fStartX, fCurrentY), Vector2(fStartX + (fHorizontalIndent / 2.0f), fCurrentY), Color32(1.0f, 1.0f, 1.0f, 1.0f), Color32(1.0f, 1.0f, 1.0f, 1.0f));
		}
	}

	// Outdent
	fOriginX -= fHorizontalIndent;

	if(iChildCount > 0)
	{
		grcDebugDraw::Line(Vector2(fStartX, fStartY), Vector2(fStartX, fCurrentY), Color32(1.0f, 1.0f, 1.0f, 1.0f), Color32(1.0f, 1.0f, 1.0f, 1.0f));
	}
#endif // FLATTEN_WALK_AND_RENDER
}

void fwMoveDumpNode::Render(float &fOriginX, float &fOriginY, float /*fHorizontalIndent*/, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	Vector2 position(fOriginX, fOriginY);

	Color32 color;
	bool background;
	if(fWeight < 0.001f)
	{
		color.Setf(1.0f, 1.0f, 1.0f, 0.5f);
		background = false;
	}
	else
	{
		color.Setf(1.0f, 1.0f, 0.0f, 0.5f + (fWeight * (1.0f - 0.5f)));
		background = true;
	}

	grcDebugDraw::Text(position, color, GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), background);

	fOriginY += fVerticalIndent;
}

void fwMoveDumpNode::WalkAndPrint(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend, unsigned& numNodes) const
{
	numNodes++;
	Print(rPrinter, iHorizontalIndent, renderFlags, fWeight, fLocalWeight, bIsParentBlend);

	// Indent
	iHorizontalIndent ++;

	int iChildCount = 0;

	// WalkAndPrint each child recursively
	for(int i = 0; i < m_Children.GetCount(); i ++)
	{
		if((!m_Children[i]->IsInvalidNode() && !m_Children[i]->IsStateNode()) ||
			(m_Children[i]->IsStateNode() && (renderFlags.m_bRenderInvalidNodes || static_cast< fwMoveDumpNodeState * >(m_Children[i])->IsInterestingState())))
		{
			iChildCount ++;
		}

		m_Children[i]->WalkAndPrint(rPrinter, iHorizontalIndent, renderFlags, fWeight * GetChildWeight(i), GetChildWeight(i), m_Children.GetCount() > 1, numNodes);
	}

	// Outdent
	iHorizontalIndent --;
}

void fwMoveDumpNode::Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	rPrinter.PrintLine(iHorizontalIndent, GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str());
}

///////////////////////////////////////////////////////////////////////////////

fwMoveDumpNodeClip::fwMoveDumpNodeClip()
: m_Time(0.0f), m_Phase(0.0f), m_Duration(0.0f), m_Rate(0.0f), m_Looped(false)
{
}

bool Filter(const crTag& tag, float startPhase, float endPhase)
{
	// only filtering a phase range, if end phase >= 0.0
	if(endPhase >= 0.f)
	{
		// check for special case of start == 0.0, end == 1.0
		if(endPhase == 1.f && startPhase == 0.f)
		{
			return true;
		}

		float tagStart = tag.GetStart();
		float tagEnd = tag.GetEnd();

		float phaseStart = startPhase;
		float phaseEnd = endPhase;

		// unwind tag and/or phase wraps
		if(tagEnd < tagStart)
		{
			tagEnd += 1.f;

			if(phaseEnd < tagStart)
			{
				phaseStart += 1.f;
				phaseEnd += 1.f;
			}
		}
		if(phaseEnd < phaseStart)
		{
			phaseEnd += 1.f;

			if(tagEnd < phaseStart)
			{
				tagStart += 1.f;
				tagEnd += 1.f;
			}
		}

		// check for exclusion
		if(((tagStart < phaseStart) || (tagStart > phaseEnd)) &&
			((tagEnd < phaseStart) || (tagEnd > phaseEnd)) && 
			((phaseStart < tagStart) || (phaseStart > tagEnd)) &&
			((phaseEnd < tagStart) || (phaseEnd > tagEnd)))
		{
			return false;
		}
	}

	return true;
}

fwMoveDumpNodeClip::fwMoveDumpNodeClip(const fwEntity *pEntity, const crmtNodeClip &node)
: base(pEntity, node), m_Time(0.0f), m_Duration(0.0f), m_Phase(0.0f), m_Rate(0.0f), m_Looped(false)
{
	const crClipPlayer &player = node.GetClipPlayer();

	const crClip *pClip = player.GetClip();
	if(pClip)
	{
		String clipName(pClip->GetName());
		if(clipName.StartsWith("pack:/"))
		{
			clipName.Replace("pack:/", "");
			clipName.Replace(".clip", "");

			const char *szClipDictionaryName = NULL;
			int dictSize = g_ClipDictionaryStore.GetSize();
			for(int clipDictionaryIndex = 0; clipDictionaryIndex < dictSize ; clipDictionaryIndex ++)
			{
				fwClipDictionaryDef* pSlot = g_ClipDictionaryStore.GetSlot(strLocalIndex(clipDictionaryIndex));
				if(pSlot)
				{
					if(pSlot->m_pObject == pClip->GetDictionary())
					{
						szClipDictionaryName = pSlot->m_name.GetCStr();
						break;
					}
				}
			}

			if(szClipDictionaryName)
			{
				char buffer[512];
				formatf(buffer, "%s/%s", szClipDictionaryName, clipName.c_str());
				m_Clip = buffer;
			}
			else
			{
				m_Clip = clipName.c_str();
			}
		}
		else
		{
			m_Clip = clipName.c_str();
		}
	}
	else
	{
		m_Clip = "(null clip)";
	}

	m_Time = player.GetTime();
	if(pClip)
	{
		m_Duration = pClip->GetDuration();
		m_Phase = Clamp(m_Time / m_Duration, 0.0f, 1.0f);
	}
	m_Rate = player.GetRate();
	m_Looped = player.IsLooped();

	if(pClip)
	{
		const crTags *pTags = pClip->GetTags();
		if(pTags)
		{
			static u32 footId = ATSTRINGHASH("Foot", 0xB82E430B);
			static u32 heelId = ATSTRINGHASH("Heel", 0x2BC03824);
			static u32 rightId = ATSTRINGHASH("Right", 0xB8CCC339);
			static u32 moveEventId = ATSTRINGHASH("MoveEvent", 0x7EA9DFC4);

			float fLastPhase = Clamp(m_Phase - (player.GetDelta() / m_Duration), 0.0f, 1.0f);

			for(crTagIterator it(*pTags, 0.0f, 1.0f); *it; ++it)
			{
				const crTag *pTag = *it;

				const crPropertyAttributeBool *heelAttr = static_cast< const crPropertyAttributeBool * >(pTag->GetProperty().GetAttribute(heelId));
				const crPropertyAttributeBool *rightAttr = static_cast< const crPropertyAttributeBool * >(pTag->GetProperty().GetAttribute(rightId));

				atHashString key = pTag->GetKey();
				String name;
				if(key.TryGetCStr())
				{
					name = key.TryGetCStr();
				}
				else
				{
					name = VarString("%u", key.GetHash());
				}

				float fMid = pTag->GetMid();
				bool bActive = Filter(*pTag, fLastPhase, m_Phase);
				bool bFoot = pTag->GetKey() == footId;
				bool bHeel = heelAttr ? heelAttr->GetValue() : false;
				bool bRight = rightAttr ? rightAttr->GetValue() : false;

				const crPropertyAttributeHashString *moveEventAttr = static_cast< const crPropertyAttributeHashString * >(pTag->GetProperty().GetAttribute(moveEventId));

				atHashString moveEvent;
				if(moveEventAttr)
				{
					moveEvent = moveEventAttr->GetValue();
					if(moveEvent.IsNotNull() && !moveEvent.TryGetCStr())
					{
						char buffer[512];
						formatf(buffer, "%u", moveEvent.GetHash());
						moveEvent.SetFromString(buffer);
					}
				}

				m_Tags.PushAndGrow(fwMoveDumpNodeClipTag(name.c_str(), fMid, bActive, bFoot, bHeel, bRight, moveEvent));
			}
		}
	}

	static const u32 messageId = ATSTRINGHASH("MotionTreeVisualise", 0xB6EA596);

	atString synchronizers;
	crmtMessage msg(messageId, (void *)(&synchronizers));
	const_cast< crmtNodeClip & >(node).MessageObservers(msg);

	if(synchronizers.GetLength() > 0)
	{
		synchronizers.Split(m_Synchronizers, "\n", true);
	}
}

/* Properties */

fwMoveDumpNodeClip::String fwMoveDumpNodeClip::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	String description = base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str();

	const char *szClip = m_Clip.c_str() ? m_Clip.c_str() : "No Clip";
	description += VarString(" '%s' Time:%.3f/%.3f Phase:%.3f Rate:%.3f Looped:%s", szClip, m_Time, m_Duration, m_Phase, m_Rate, m_Looped ? "true" : "false");

	int iActiveCount = 0;
	for(int i = 0; i < m_Tags.GetCount(); i ++)
	{
		if(m_Tags[i].GetActive())
		{
			iActiveCount ++;
		}
	}

	if(iActiveCount > 0)
	{
		description += " Tags[";

		int iActiveIndex = 0;
		for(int i = 0; i < m_Tags.GetCount(); i ++)
		{
			if(m_Tags[i].GetActive())
			{
				description += m_Tags[i].GetName().c_str();
				if(m_Tags[i].GetFoot())
				{
					description += VarString("(%s)", m_Tags[i].GetRight() ? "Right" : "Left");
				}
				else if(m_Tags[i].GetMoveEvent().IsNotNull())
				{
					description += VarString("(%s)", m_Tags[i].GetMoveEvent().TryGetCStr());
				}

				iActiveIndex ++;

				if(iActiveIndex < iActiveCount)
				{
					description += " ";
				}
			}
		}

		description += "]";
	}

	return description;
}

/* Operations */

void fwMoveDumpNodeClip::Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	base::Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, fWeight, fLocalWeight, bIsParentBlend);

	Color32 color;
	bool background;
	if(fWeight < 0.001f)
	{
		color.Setf(1.0f, 1.0f, 1.0f, 0.5f);
		background = false;
	}
	else
	{
		color.Setf(1.0f, 1.0f, 0.0f, 0.5f + (fWeight * (1.0f - 0.5f)));
		background = true;
	}

	fOriginX += fHorizontalIndent;

	if(renderFlags.m_bRenderSyncWeights)
	{
		for(int i = 0; i < m_Synchronizers.GetCount(); i ++)
		{
			Vector2 position(fOriginX, fOriginY);

			grcDebugDraw::Text(position, color, m_Synchronizers[i].c_str(), background);

			fOriginY += fVerticalIndent;
		}
	}

	if(renderFlags.m_bRenderSyncGraph && m_Tags.GetCount() > 0)
	{
		//draw the tag graph for this clipName:
		float fTagSyncGraphXOffset = 0.3f;
		float fTagSyncGraphWidth = 0.3f;
		float fTagSyncGraphHeight = 0.007f;
		float fTagSyncGraphReferenceYOffset = 0.014f;

		float fPhase = m_Phase;
		float fGraphCentre = fOriginX + fTagSyncGraphXOffset;
		float fGraphCentreY = fOriginY + fTagSyncGraphHeight / 2.0f;
		int maxObserverCount = 0;

		Vector2 vRangeStart(fGraphCentre - (fTagSyncGraphWidth*fPhase) ,fGraphCentreY);
		Vector2 vRangeEnd(fGraphCentre + ((1.0f - fPhase)*fTagSyncGraphWidth) ,fGraphCentreY);

		// draw the current phase marker
		grcDebugDraw::Line(Vector2(fGraphCentre, fGraphCentreY), Vector2(fGraphCentre, fGraphCentreY - 0.01f), Color_white);
		// draw the range
		grcDebugDraw::Line(vRangeStart, vRangeEnd, Color_white);

		// draw the tag positions
		for(int i = 0; i < m_Tags.GetCount(); i ++)
		{
			const fwMoveDumpNodeClipTag &tag = m_Tags[i];
			if(tag.GetFoot())
			{
				float fTagMid = tag.GetMid();
				float fTagCentre = vRangeStart.x + (fTagMid * fTagSyncGraphWidth);
				Color32 col;

				if (tag.GetHeel() == true)
				{
					if (tag.GetRight() == true)
					{
						col = Color_red;
					}
					else
					{
						col = Color_green;
					}
				}
				else
				{
					if (tag.GetRight() == true)
					{
						col = Color_yellow;
					}
					else
					{
						col = Color_cyan;
					}
				}

				Vector2 min = Vector2(fTagCentre - 0.001f, fGraphCentreY - 0.01f);
				Vector2 max = Vector2(fTagCentre + 0.001f, fGraphCentreY);
				grcDebugDraw::RectAxisAligned(min,max,col);
			}
		}

		fOriginY += (fTagSyncGraphHeight + fTagSyncGraphReferenceYOffset*maxObserverCount);
	}

	fOriginX -= fHorizontalIndent;
}

void fwMoveDumpNodeClip::Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	base::Print(rPrinter, iHorizontalIndent, renderFlags, fWeight, fLocalWeight, bIsParentBlend);

	iHorizontalIndent ++;

	if(renderFlags.m_bRenderSyncWeights)
	{
		for(int i = 0; i < m_Synchronizers.GetCount(); i ++)
		{
			rPrinter.PrintLine(iHorizontalIndent, m_Synchronizers[i].c_str());
		}
	}

	iHorizontalIndent --;
}

///////////////////////////////////////////////////////////////////////////////

/* Operations */

void fwMoveDumpNodeInvalid::Render(float &/*fOriginX*/, float &/*fOriginY*/, float /*fHorizontalIndent*/, float /*fVerticalIndent*/, const fwMoveDumpRenderFlags &/*renderFlags*/, float /*fWeight*/, float /*fLocalWeight*/, bool /*bIsParentBlend*/) const
{
}

void fwMoveDumpNodeInvalid::Print(fwMoveDumpPrinterBase & /*pFile*/, int &/*iHorizontalIndent*/, const fwMoveDumpRenderFlags &/*renderFlags*/, float /*fWeight*/, float /*fLocalWeight*/, bool /*bIsParentBlend*/) const
{
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeExpression::fwMoveDumpNodeExpression()
: m_Time(0.0f), m_Weight(0.0f)
{
}

fwMoveDumpNodeExpression::fwMoveDumpNodeExpression(const fwEntity *pEntity, const crmtNodeExpression &node)
: base(pEntity, node), m_Time(0.0f), m_Weight(0.0f)
{
	const crExpressions *pExpressions = node.GetExpressionPlayer().GetExpressions();
	if(pExpressions)
	{
		m_Expression = pExpressions->GetName();
		m_Expression.Replace("pack:/", "");
		m_Expression.Replace(".expr", "");
	}

	m_Time = node.GetExpressionPlayer().GetTime();
	m_Weight = node.GetWeight();
}

/* Properties */

fwMoveDumpNodeExpression::String fwMoveDumpNodeExpression::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	const char *szExpression = m_Expression.c_str() ? m_Expression.c_str() : "No Expression";

	return VarString("%s Expression:'%s' Time:%.3f Weight:%.3f", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), szExpression, m_Time, m_Weight);
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeFilter::fwMoveDumpNodeFilter(const fwEntity *pEntity, const crmtNodeFilter &node)
: base(pEntity, node)
{
	crFrameFilter *pFilter = node.GetFilter();
	if(pFilter)
	{
		const char *szFilterName = g_FrameFilterDictionaryStore.FindFrameFilterNameSlow(pFilter);
		if(szFilterName)
		{
			m_Filter = szFilterName;
		}
		else
		{
			m_Filter = "Name Missing";
		}
	}
}

/* Properties */

fwMoveDumpNodeFilter::String fwMoveDumpNodeFilter::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	const char *szFilter = m_Filter.c_str() ? m_Filter.c_str() : "No Filter";

	return VarString("%s Filter:'%s'", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), szFilter);
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeMerge::fwMoveDumpNodeMerge(const fwEntity *pEntity, const crmtNodeMerge &node)
: base(pEntity, node)
{
	crFrameFilter *pFilter = node.GetFilter();
	if(pFilter)
	{
		const char *szFilterName = g_FrameFilterDictionaryStore.FindFrameFilterNameSlow(pFilter);
		if(szFilterName)
		{
			m_Filter = szFilterName;
		}
		else
		{
			m_Filter = "Name Missing";
		}
	}
}

/* Properties */

fwMoveDumpNodeMerge::String fwMoveDumpNodeMerge::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	const char *szFilter = m_Filter.c_str() ? m_Filter.c_str() : "No Filter";

	return VarString("%s Filter:'%s'", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), szFilter);
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeStateInputParam::fwMoveDumpNodeStateInputParam(const mvKeyedParameter &keyedParameter)
{
	const char *szName = atHashString(keyedParameter.Key).TryGetCStr();
	if(szName)
	{
		m_Name = szName;
	}
	else
	{
		m_Name = atVarString("%u", keyedParameter.Key);
	}

	const mvParameter &param = keyedParameter.Parameter;

	switch(param.GetType())
	{
	case mvParameter::kParameterAnimation:
		{
			m_Type = "Animation";
			m_Value = param.GetAnimation() ? param.GetAnimation()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterClip:
		{
			m_Type = "Clip";
			m_Value = param.GetClip() ? param.GetClip()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterExpressions:
		{
			m_Type = "Expressions";
			m_Value = param.GetExpressions() ? param.GetExpressions()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterFilter:
		{
			m_Type = "Filter";
			m_Value = atVarString("%p", param.GetFilter());
		} break;
	case mvParameter::kParameterFilterN:
		{
			m_Type = "FilterN";
			m_Value = "FilterN";
		} break;
	case mvParameter::kParameterFrame:
		{
			m_Type = "Frame";
			m_Value = atVarString("%p", param.GetFrame());
		} break;
	case mvParameter::kParameterPM:
		{
			m_Type = "PM";
			m_Value = atVarString("%p", param.GetPM());
		} break;
	case mvParameter::kParameterBoolean:
		{
			m_Type = "Bool";
			m_Value = param.GetBool() ? "TRUE" : "FALSE";
		} break;
	case mvParameter::kParameterReal:
		{
			m_Type = "Real";
			m_Value = atVarString("%.3f", param.GetReal());
		} break;
	case mvParameter::kParameterInt:
		{
			m_Type = "Int";
			m_Value = atVarString("%i", param.GetInt());
		} break;
	case mvParameter::kParameterData:
		{
			m_Type = "Data";
			m_Value = atVarString("%p", param.GetData());
		} break;
	case mvParameter::kParameterNode:
		{
			m_Type = "Node";
			m_Value = atVarString("%p", param.GetNode());
		} break;
	case mvParameter::kParameterObserver:
		{
			m_Type = "Observer";
			m_Value = atVarString("%p", param.GetObserver());
		} break;
	case mvParameter::kParameterNetwork:
		{
			m_Type = "Network";
			m_Value = param.GetNetwork() ? param.GetNetwork()->GetDefinition()->GetDebugName(NULL, 0) : "NULL";
		} break;
	case mvParameter::kParameterReference:
		{
			m_Type = "Reference";
			m_Value = atVarString("%p", param.GetData());
		} break;
	case mvParameter::kParameterProperty:
		{
			m_Type = "Property";
			m_Value = param.GetProperty() ? param.GetProperty()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterNodeId:
		{
			m_Type = "NodeId";
			const char *szNodeName = atHashString(param.GetNodeId()).TryGetCStr();
			if(szNodeName)
			{
				m_Value = szNodeName;
			}
			else
			{
				m_Value = atVarString("%u", param.GetNodeId());
			}
		} break;
	case mvParameter::kParameterNodeName:
		{
			m_Type = "NodeName";
			const char *szNodeName = atHashString(param.GetNodeId()).TryGetCStr();
			if(szNodeName)
			{
				m_Value = szNodeName;
			}
			else
			{
				m_Value = atVarString("%u", param.GetNodeId());
			}
		} break;
	case mvParameter::kParameterNone:
		break;
	case mvParameter::kParameterRequest:
		{
			m_Type = "Request";
			m_Value = "Request";
		} break;
	case mvParameter::kParameterFlag:
		{
			m_Type = "Flag";
			m_Value = param.GetFlag() ? "TRUE" : "FALSE";
		} break;
	default:
		{
			Assert(false);
		} break;
	}
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeStateOutputParam::fwMoveDumpNodeStateOutputParam(const mvKeyedParameter &keyedParameter)
{
	const char *szName = atHashString(keyedParameter.Key).TryGetCStr();
	if(szName)
	{
		m_Name = szName;
	}
	else
	{
		m_Name = atVarString("%u", keyedParameter.Key);
	}

	const mvParameter &param = keyedParameter.Parameter;

	switch(param.GetType())
	{
	case mvParameter::kParameterAnimation:
		{
			m_Type = "Animation";
			m_Value = param.GetAnimation() ? param.GetAnimation()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterClip:
		{
			m_Type = "Clip";
			m_Value = param.GetClip() ? param.GetClip()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterExpressions:
		{
			m_Type = "Expressions";
			m_Value = param.GetExpressions() ? param.GetExpressions()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterFilter:
		{
			m_Type = "Filter";
			m_Value = atVarString("%p", param.GetFilter());
		} break;
	case mvParameter::kParameterFilterN:
		{
			m_Type = "FilterN";
			m_Value = "FilterN";
		} break;
	case mvParameter::kParameterFrame:
		{
			m_Type = "Frame";
			m_Value = atVarString("%p", param.GetFrame());
		} break;
	case mvParameter::kParameterPM:
		{
			m_Type = "PM";
			m_Value = atVarString("%p", param.GetPM());
		} break;
	case mvParameter::kParameterBoolean:
		{
			m_Type = "Bool";
			m_Value = param.GetBool() ? "TRUE" : "FALSE";
		} break;
	case mvParameter::kParameterReal:
		{
			m_Type = "Real";
			m_Value = atVarString("%.3f", param.GetReal());
		} break;
	case mvParameter::kParameterInt:
		{
			m_Type = "Int";
			m_Value = atVarString("%i", param.GetInt());
		} break;
	case mvParameter::kParameterData:
		{
			m_Type = "Data";
			m_Value = atVarString("%p", param.GetData());
		} break;
	case mvParameter::kParameterNode:
		{
			m_Type = "Node";
			m_Value = atVarString("%p", param.GetNode());
		} break;
	case mvParameter::kParameterObserver:
		{
			m_Type = "Observer";
			m_Value = atVarString("%p", param.GetObserver());
		} break;
	case mvParameter::kParameterNetwork:
		{
			m_Type = "Network";
			m_Value = param.GetNetwork() ? param.GetNetwork()->GetDefinition()->GetDebugName(NULL, 0) : "NULL";
		} break;
	case mvParameter::kParameterReference:
		{
			m_Type = "Reference";
			m_Value = atVarString("%p", param.GetData());
		} break;
	case mvParameter::kParameterProperty:
		{
			m_Type = "Property";
			m_Value = param.GetProperty() ? param.GetProperty()->GetName() : "NULL";
		} break;
	case mvParameter::kParameterNodeId:
		{
			m_Type = "NodeId";
			const char *szNodeName = atHashString(param.GetNodeId()).TryGetCStr();
			if(szNodeName)
			{
				m_Value = szNodeName;
			}
			else
			{
				m_Value = atVarString("%u", param.GetNodeId());
			}
		} break;
	case mvParameter::kParameterNodeName:
		{
			m_Type = "NodeName";
			const char *szNodeName = atHashString(param.GetNodeId()).TryGetCStr();
			if(szNodeName)
			{
				m_Value = szNodeName;
			}
			else
			{
				m_Value = atVarString("%u", param.GetNodeId());
			}
		} break;
	case mvParameter::kParameterNone:
		break;
	case mvParameter::kParameterRequest:
		{
			m_Type = "Request";
			m_Value = "Request";
		} break;
	case mvParameter::kParameterFlag:
		{
			m_Type = "Flag";
			m_Value = param.GetFlag() ? "TRUE" : "FALSE";
		} break;
	default:
		{
			Assert(false);
		} break;
	}
}

///////////////////////////////////////////////////////////////////////////////

/* Destructor */

fwMoveDumpNodeState::~fwMoveDumpNodeState()
{
	USE_DEBUG_ANIM_MEMORY();

	for(int i = 0; i < m_Requests.GetCount(); i ++)
	{
		delete m_Requests[i]; m_Requests[i] = NULL;
	}
	m_Requests.Reset();

	for(int i = 0; i < m_Flags.GetCount(); i ++)
	{
		delete m_Flags[i]; m_Flags[i] = NULL;
	}
	m_Flags.Reset();

	for(int i = 0; i < m_InputParams.GetCount(); i ++)
	{
		delete m_InputParams[i]; m_InputParams[i] = NULL;
	}
	m_InputParams.Reset();

	for(int i = 0; i < m_OutputParams.GetCount(); i ++)
	{
		delete m_OutputParams[i]; m_OutputParams[i] = NULL;
	}
	m_OutputParams.Reset();
}

/* Constructors */

fwMoveDumpNodeState::fwMoveDumpNodeState()
: m_IsInterestingState(false), m_Transitions(0)
{
}

fwMoveDumpNodeState::fwMoveDumpNodeState(const fwEntity *pEntity, const crmtNodeParent &node)
: base(pEntity, node), m_IsInterestingState(false), m_Transitions(0)
{
	USE_DEBUG_ANIM_MEMORY();

	for(u32 i = 0; i < node.GetNumChildren(); i ++)
	{
		crmtNode *pChildNode = node.GetChild(i);

		if(pChildNode)
		{
			if(pChildNode->GetNodeType() != crmtNode::kNodeInvalid)
			{
				m_IsInterestingState = true;
			}
		}
	}

#if __DEV
	fwAnimDirector* pAnimDirector = pEntity->GetAnimDirector();
	if(!pAnimDirector)
		return;

	crmtNode::eNodes nodeType = node.GetNodeTypeInfo().GetNodeType();
	switch(nodeType)
	{
	case crmtNode::kNodeSubNetwork:
		{
			const mvNodeSubNetwork* pInterface = static_cast<const mvNodeSubNetwork*>(&node);

			m_StateInterfaceType = "SubNetwork";

			const mvSubNetwork *pSubNetwork = pInterface->DebugGetSubNetwork();
			if(pSubNetwork)
			{
				const mvNetworkDef *pNetworkDef = pSubNetwork->GetDefinition();
				Assert(pNetworkDef);
				if(pNetworkDef)
				{
					String networkNameString(pNetworkDef->GetName());
					if(networkNameString.StartsWith("memory:$"))
					{
						int index = networkNameString.LastIndexOf(":");
						if(index != -1)
						{
							String tempString(&networkNameString.c_str()[index + 1]);
							networkNameString = tempString;
						}
					}
					if(networkNameString.StartsWith("assets:"))
					{
						int index = networkNameString.LastIndexOf("/");
						if(index != -1)
						{
							String tempString(&networkNameString.c_str()[index + 1]);
							index = tempString.LastIndexOf(".");
							if(index != -1)
							{
								tempString.Truncate(index);
							}
							networkNameString = tempString;
						}
					}
					if(networkNameString.GetLength() > 0)
					{
						m_StateInterfaceName = atVarString("%s(%p)", networkNameString.c_str(), pInterface);
					}

					const mvSignalData &signalData = pSubNetwork->GetSignalData();

					m_Requests.Reserve(pNetworkDef->GetRequestCount());
					const request_t *pRequests = pNetworkDef->GetRequests();
					for(int i = 0, count = pNetworkDef->GetRequestCount(); i < count; i ++)
					{
						const request_t &request = pRequests[i];
						if(request.key != (u32)-1)
						{
							atHashString name(request.key);
							bool bValue = signalData.GetRequest(request.index);
							m_Requests.PushAndGrow(rage_new fwMoveDumpNetworkRequest(name, bValue));
						}
					}

					m_Flags.Reserve(pNetworkDef->GetFlagCount());
					const flag_t *pFlags = pNetworkDef->GetFlags();
					for(int i = 0, count = pNetworkDef->GetFlagCount(); i < count; i ++)
					{
						const flag_t &flag = pFlags[i];
						if(flag.key != (u32)-1)
						{
							atHashString name(flag.key);
							bool bValue = signalData.GetFlag(flag.index);
							m_Flags.PushAndGrow(rage_new fwMoveDumpNetworkFlag(name, bValue));
						}
					}
				}
			}
			else
			{
				atHashString id = pInterface->DebugGetNetworkId();

				if(id.TryGetCStr())
				{
					m_StateInterfaceName = id.TryGetCStr();
				}
				else
				{
					m_StateInterfaceName = atVarString("%u", id.GetHash());
				}
			}

			fwAnimDirectorComponentMove *pAnimDirectorComponentMove = static_cast< fwAnimDirectorComponentMove * >(pAnimDirector->GetComponentByType(fwAnimDirectorComponent::kComponentTypeMove));
			Assert(pAnimDirectorComponentMove);
			if(pAnimDirectorComponentMove)
			{
				fwMoveNetworkPlayer *pMoveNetworkPlayer = pAnimDirectorComponentMove->FindMoveNetworkPlayer(pInterface->DebugGetSubNetwork());
				if(pMoveNetworkPlayer)
				{
					bool bFirst = true;
					const atMap< fwMvClipSetVarId, fwMvClipSetId > &clipSetIdMap = pMoveNetworkPlayer->GetClipSetIdMap();
					if(clipSetIdMap.GetNumUsed() > 0)
					{
						for(atMap< fwMvClipSetVarId, fwMvClipSetId >::ConstIterator it = clipSetIdMap.CreateIterator(); !it.AtEnd(); it.Next())
						{
							if(bFirst)
							{
								bFirst = false;
							}
							else
							{
								m_ClipSet += ", ";
							}

							fwMvClipSetVarId clipSetVarId = it.GetKey();
							if(clipSetVarId.TryGetCStr())
							{
								m_ClipSet += clipSetVarId.TryGetCStr();
							}
							else
							{
								m_ClipSet += atVarString("%u", clipSetVarId.GetHash());
							}

							m_ClipSet += "=";

							fwMvClipSetId clipSetId = it.GetData();
							if(clipSetId.TryGetCStr())
							{
								m_ClipSet += clipSetId.TryGetCStr();
							}
							else
							{
								m_ClipSet += atVarString("%u", clipSetId.GetHash());
							}
						}
					}
					else
					{
						fwMvClipSetId clipSetId = pMoveNetworkPlayer->GetClipSetId();
						if(clipSetId.TryGetCStr())
						{
							m_ClipSet += clipSetId.TryGetCStr();
						}
						else
						{
							m_ClipSet += atVarString("%u", clipSetId.GetHash());
						}
					}

					mvParameterBuffer::Iterator it;
					for(mvKeyedParameter* kp = it.Begin(pMoveNetworkPlayer->GetInputBuffer().GetInternalBuffer()); kp != NULL; kp = it.Next())
					{
						m_InputParams.PushAndGrow(rage_new fwMoveDumpNodeStateInputParam(*kp));
					}

					for(mvKeyedParameter* kp = it.Begin(pMoveNetworkPlayer->GetOutputBuffer()); kp != NULL; kp = it.Next())
					{
						m_OutputParams.PushAndGrow(rage_new fwMoveDumpNodeStateOutputParam(*kp));
					}
				}
			}
			break;
		}

		case crmtNode::kNodeStateMachine:
		{
			const mvNodeStateMachine* pInterface = static_cast<const mvNodeStateMachine*>(&node);

			m_StateInterfaceType = "StateMachine";

			atHashString id = pInterface->GetDefinition()->m_id;

			if(id.TryGetCStr())
			{
				m_StateInterfaceName = id.TryGetCStr();
			}
			else
			{
				m_StateInterfaceName = atVarString("%u", id.GetHash());
			}

			const mvNodeStateBaseDef* activeDef = pInterface->GetActiveDef();
			if(activeDef)
			{
				atHashString activeId = activeDef->m_id;
				if(activeId.TryGetCStr())
				{
					m_ActiveName = activeId.TryGetCStr();
				}
				else
				{
					m_ActiveName = atVarString("%u", activeId.GetHash());
				}
				m_Transitions = activeDef->m_TransitionCount;
			}
			break;	
		}

	case crmtNode::kNodeStateRoot:
		{
			const mvNodeStateRoot* pInterface = static_cast<const mvNodeStateRoot*>(&node);

			m_StateInterfaceType = "RootMotionTree";

			atHashString id = pInterface->GetDefinition()->m_id;

			if(id.TryGetCStr())
			{
				m_StateInterfaceName = id.TryGetCStr();
			}
			else
			{
				m_StateInterfaceName = atVarString("%u", id.GetHash());
			}

			bool bFirst = true;
			const atMap< fwMvClipSetVarId, fwMvClipSetId > &clipSetIdMap = pAnimDirector->GetMove().GetClipSetIdMap();
			if(clipSetIdMap.GetNumUsed() > 0)
			{
				for(atMap< fwMvClipSetVarId, fwMvClipSetId >::ConstIterator it = clipSetIdMap.CreateIterator(); !it.AtEnd(); it.Next())
				{
					if(bFirst)
					{
						bFirst = false;
					}
					else
					{
						m_ClipSet += ", ";
					}

					fwMvClipSetVarId clipSetVarId = it.GetKey();
					if(clipSetVarId.TryGetCStr())
					{
						m_ClipSet += clipSetVarId.TryGetCStr();
					}
					else
					{
						m_ClipSet += atVarString("%u", clipSetVarId.GetHash());
					}

					m_ClipSet += " = ";

					fwMvClipSetId clipSetId = it.GetData();
					if(clipSetId.TryGetCStr())
					{
						m_ClipSet += clipSetId.TryGetCStr();
					}
					else
					{
						m_ClipSet += atVarString("%u", clipSetId.GetHash());
					}
				}
			}
			else
			{
				fwMvClipSetId clipSetId = pAnimDirector->GetMove().GetClipSetId();
				if(clipSetId.TryGetCStr())
				{
					m_ClipSet += clipSetId.TryGetCStr();
				}
				else
				{
					m_ClipSet += atVarString("%u", clipSetId.GetHash());
				}
			}

			mvParameterBuffer::Iterator it;
			for(mvKeyedParameter* kp = it.Begin(pAnimDirector->GetMove().GetInputBuffer().GetInternalBuffer()); kp != NULL; kp = it.Next())
			{
				m_InputParams.PushAndGrow(rage_new fwMoveDumpNodeStateInputParam(*kp));
			}

			for(mvKeyedParameter* kp = it.Begin(pAnimDirector->GetMove().GetOutputBuffer()); kp != NULL; kp = it.Next())
			{
				m_OutputParams.PushAndGrow(rage_new fwMoveDumpNodeStateOutputParam(*kp));
			}
			break;
		}

	default: break;
	}
#endif // __DEV
}

/* Properties */

fwMoveDumpNodeState::String fwMoveDumpNodeState::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	String description = base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend);

#if __DEV
	description += VarString(" %s '%s'", m_StateInterfaceType.c_str(), m_StateInterfaceName.c_str());

	if(renderFlags.m_bRenderStateActiveName && m_ActiveName.GetLength() > 0)
	{
		description += VarString(" (%s)", m_ActiveName.c_str());
	}

	if(m_ClipSet.GetLength() > 0)
	{
		description += VarString(" ClipSet:'%s'", m_ClipSet.c_str());
	}

	if(m_Transitions > 0)
	{
		description += VarString(" Transitions:%i", m_Transitions);
	}
#endif // __DEV

	return description;
}

/* Operations */

void fwMoveDumpNodeState::Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	if(renderFlags.m_bRenderInvalidNodes || m_IsInterestingState)
	{
		base::Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, fWeight, fLocalWeight, bIsParentBlend);

		Color32 color;
		bool background;
		if(fWeight < 0.001f)
		{
			color.Setf(1.0f, 1.0f, 1.0f, 0.5f);
			background = false;
		}
		else
		{
			color.Setf(1.0f, 1.0f, 0.0f, 0.5f + (fWeight * (1.0f - 0.5f)));
			background = true;
		}

		if(renderFlags.m_bRenderRequests && m_Requests.GetCount() > 0)
		{
			Vector2 requestPos(fOriginX, fOriginY);

			String description(VarString("[Requests(%i): ", m_Requests.GetCount()));
			for(int i = 0, count = m_Requests.GetCount(); i < count; i ++)
			{
				const fwMoveDumpNetworkRequest *pRequest = m_Requests[i];
				const char *szName = pRequest->GetName().TryGetCStr();
				if(szName)
				{
					description += VarString("%s:%s", szName, pRequest->GetValue() ? "TRUE" : "FALSE");
				}
				else
				{
					description += VarString("%u:%s", pRequest->GetName().GetHash(), pRequest->GetValue() ? "TRUE" : "FALSE");
				}

				if(i + 1 < count)
				{
					description += " ";
				}
			}
			description += "]";

			grcDebugDraw::Text(requestPos, color, description.c_str(), background);

			fOriginY += fVerticalIndent;
		}

		if(renderFlags.m_bRenderFlags && m_Flags.GetCount() > 0)
		{
			Vector2 flagPos(fOriginX, fOriginY);

			String description(atVarString("[Flags(%i): ", m_Flags.GetCount()));
			for(int i = 0, count = m_Flags.GetCount(); i < count; i ++)
			{
				const fwMoveDumpNetworkFlag *pFlag = m_Flags[i];
				const char *szName = pFlag->GetName().TryGetCStr();
				if(szName)
				{
					description += VarString("%s:%s", szName, pFlag->GetValue() ? "TRUE" : "FALSE");
				}
				else
				{
					description += VarString("%u:%s", pFlag->GetName().GetHash(), pFlag->GetValue() ? "TRUE" : "FALSE");
				}

				if(i + 1 < count)
				{
					description += " ";
				}
			}
			description += "]";

			grcDebugDraw::Text(flagPos, color, description.c_str(), background);

			fOriginY += fVerticalIndent;
		}

		if(renderFlags.m_bRenderInputParams && m_InputParams.GetCount() > 0)
		{
			String inputParams = VarString("[Input Params(%i)", m_InputParams.GetCount());

			for(int i = 0; i < m_InputParams.GetCount(); i ++)
			{
				const fwMoveDumpNodeStateInputParam *pInputParam = m_InputParams[i];

				if(pInputParam->GetValue().GetLength() > 0)
				{
					inputParams += VarString(" %i: %s %s %s", i, pInputParam->GetName().c_str(), pInputParam->GetType().c_str(), pInputParam->GetValue().c_str());
				}
				else
				{
					inputParams += VarString(" %i: %s %s", i, pInputParam->GetName().c_str(), pInputParam->GetType().c_str());
				}
			}

			inputParams += "]";

			Vector2 position(fOriginX, fOriginY);

			grcDebugDraw::Text(position, color, inputParams.c_str(), background);

			fOriginY += fVerticalIndent;
		}

		if(renderFlags.m_bRenderOutputParams && m_OutputParams.GetCount() > 0)
		{
			String outputParams = VarString("[Output Params(%i)", m_OutputParams.GetCount());

			for(int i = 0; i < m_OutputParams.GetCount(); i ++)
			{
				const fwMoveDumpNodeStateOutputParam *pOutputParam = m_OutputParams[i];

				outputParams += VarString(" %i: %s %s %s", i, pOutputParam->GetName().c_str(), pOutputParam->GetType().c_str(), pOutputParam->GetValue().c_str());
			}

			outputParams += "]";

			Vector2 position(fOriginX, fOriginY);

			Color32 color;
			bool background;
			if(fWeight < 0.001f)
			{
				color.Setf(1.0f, 1.0f, 1.0f, 0.5f);
				background = false;
			}
			else
			{
				color.Setf(1.0f, 1.0f, 0.0f, 0.5f + (fWeight * (1.0f - 0.5f)));
				background = true;
			}

			grcDebugDraw::Text(position, color, outputParams.c_str(), background);

			fOriginY += fVerticalIndent;
		}
	}
}

void fwMoveDumpNodeState::Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	if(renderFlags.m_bRenderInvalidNodes || m_IsInterestingState)
	{
		base::Print(rPrinter, iHorizontalIndent, renderFlags, fWeight, fLocalWeight, bIsParentBlend);

		if(renderFlags.m_bRenderRequests && m_Requests.GetCount() > 0)
		{
			fwMoveDumpString description(atVarString("[Requests(%i): ", m_Requests.GetCount()));
			for(int i = 0, count = m_Requests.GetCount(); i < count; i ++)
			{
				const fwMoveDumpNetworkRequest *pRequest = m_Requests[i];
				const char *szName = pRequest->GetName().TryGetCStr();
				if(szName)
				{
					description += atVarString("%s:%s", szName, pRequest->GetValue() ? "TRUE" : "FALSE");
				}
				else
				{
					description += atVarString("%u:%s", pRequest->GetName().GetHash(), pRequest->GetValue() ? "TRUE" : "FALSE");
				}

				if(i + 1 < count)
				{
					description += " ";
				}
			}
			description += "]";

			rPrinter.PrintLine(iHorizontalIndent, description.c_str());
		}

		if(renderFlags.m_bRenderFlags && m_Flags.GetCount() > 0)
		{
			fwMoveDumpString description(atVarString("[Flags(%i): ", m_Flags.GetCount()));
			for(int i = 0, count = m_Flags.GetCount(); i < count; i ++)
			{
				const fwMoveDumpNetworkFlag *pFlag = m_Flags[i];
				const char *szName = pFlag->GetName().TryGetCStr();
				if(szName)
				{
					description += atVarString("%s:%s", szName, pFlag->GetValue() ? "TRUE" : "FALSE");
				}
				else
				{
					description += atVarString("%u:%s", pFlag->GetName().GetHash(), pFlag->GetValue() ? "TRUE" : "FALSE");
				}

				if(i + 1 < count)
				{
					description += " ";
				}
			}
			description += "]";

			rPrinter.PrintLine(iHorizontalIndent, description.c_str());
		}

		if(renderFlags.m_bRenderInputParams && m_InputParams.GetCount() > 0)
		{
			char buffer[512];
			formatf(buffer, "[Input Params(%i)", m_InputParams.GetCount());
			fwMoveDumpString inputParams(buffer);

			for(int i = 0; i < m_InputParams.GetCount(); i ++)
			{
				const fwMoveDumpNodeStateInputParam *pInputParam = m_InputParams[i];

				if(pInputParam->GetValue().GetLength() > 0)
				{
					inputParams += atVarString(" %i: %s %s %s", i, pInputParam->GetName().c_str(), pInputParam->GetType().c_str(), pInputParam->GetValue().c_str());
				}
				else
				{
					inputParams += atVarString(" %i: %s %s", i, pInputParam->GetName().c_str(), pInputParam->GetType().c_str());
				}
			}

			inputParams += "]";

			rPrinter.PrintLine(iHorizontalIndent, inputParams.c_str());
		}

		if(renderFlags.m_bRenderOutputParams && m_OutputParams.GetCount() > 0)
		{
			char buffer[512];
			formatf(buffer, "[Output Params(%i)", m_OutputParams.GetCount());
			fwMoveDumpString outputParams(buffer);

			for(int i = 0; i < m_OutputParams.GetCount(); i ++)
			{
				const fwMoveDumpNodeStateOutputParam *pOutputParam = m_OutputParams[i];

				outputParams += atVarString(" %i: %s %s %s", i, pOutputParam->GetName().c_str(), pOutputParam->GetType().c_str(), pOutputParam->GetValue().c_str());
			}

			outputParams += "]";

			rPrinter.PrintLine(iHorizontalIndent, outputParams.c_str());
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeProxy::fwMoveDumpNodeProxy(const fwEntity *pEntity, const crmtNodeProxy &node) : base(pEntity, node)
{
	crmtNode *pProxyNode = node.GetProxyNode();
	if(pProxyNode)
	{
		String name("No name");

#if __DEV
		crmtNode::eNodes nodeType = pProxyNode->GetNodeTypeInfo().GetNodeType();
		switch(nodeType)
		{
		case crmtNode::kNodeStateMachine:
			{
				const mvNodeStateMachine *pInterface = static_cast< const mvNodeStateMachine * >(pProxyNode);
				atHashString id = pInterface->GetDefinition()->m_id;
				if(id.TryGetCStr())
				{
					name = id.TryGetCStr();
				}
				else
				{
					name = VarString("%u", id.GetHash());
				}
			} break;
		case crmtNode::kNodeStateRoot:
			{
				const mvNodeStateRoot *pInterface = static_cast< const mvNodeStateRoot * >(pProxyNode);
				atHashString id = pInterface->GetDefinition()->m_id;
				if(id.TryGetCStr())
				{
					name = id.TryGetCStr();
				}
				else
				{
					name = VarString("%u", id.GetHash());
				}
			} break;
		default:
			{
			} break;
		}
#endif // __DEV

		m_Target = atVarString("%s '%s' (%p)", pProxyNode->GetNodeTypeName(), name.c_str(), pProxyNode);
	}
	else
	{
		m_Target = "No target node";
	}
}

fwMoveDumpNodeProxy::String fwMoveDumpNodeProxy::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	String description = VarString("%s -> %s", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), m_Target.c_str());

	return description;
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodeExtrapolate::fwMoveDumpNodeExtrapolate(const fwEntity *pEntity, const crmtNodeExtrapolate &node)
: base(pEntity, node)
{
	m_Damping = node.GetDamping();
}

/* Properties */

fwMoveDumpNodeExtrapolate::String fwMoveDumpNodeExtrapolate::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	return VarString("%s Damping:%.3f", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), m_Damping);
}

fwMoveDumpNodeCapture::String fwMoveDumpNodeCapture::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	return VarString("%s HasFrame:%s", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), m_HasFrame ? "true" : "false");
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpNodePair::fwMoveDumpNodePair()
: m_Weight(0.0f), m_WeightRate(0.0f)
{
}

fwMoveDumpNodePair::fwMoveDumpNodePair(const fwEntity *pEntity, const crmtNodePairWeighted &node)
: base(pEntity, node), m_Weight(node.GetWeight()), m_WeightRate(node.GetWeightRate())
{
	crFrameFilter *pFilter = node.GetFilter();
	if(pFilter)
	{
		const char *szFilterName = g_FrameFilterDictionaryStore.FindFrameFilterNameSlow(pFilter);
		if(szFilterName)
		{
			m_Filter = szFilterName;
		}
		else
		{
			m_Filter = "Name Missing";
		}
	}
}

/* Properties */

fwMoveDumpNodePair::String fwMoveDumpNodePair::GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const
{
	const char *szFilter = m_Filter.c_str() ? m_Filter.c_str() : "No Filter";

	return VarString("%s Weight:%.3f WeightRate:%.3f Filter:'%s'", base::GetDescription(renderFlags, fWeight, fLocalWeight, bIsParentBlend).c_str(), m_Weight, m_WeightRate, szFilter);
}

///////////////////////////////////////////////////////////////////////////////

float fwMoveDumpNodeBlend::GetChildWeight(int iChildIndex) const
{
	float fWeight = 1.0f;

	switch(iChildIndex)
	{
	case 0:
		{
			fWeight = 1.0f - m_Weight;
		} break;
	case 1:
		{
			fWeight = m_Weight;
		} break;
	default:
		{
			Assert(iChildIndex >= 0 && iChildIndex < 2);
		} break;
	}

	return fWeight;
}

///////////////////////////////////////////////////////////////////////////////

fwMoveDumpNodeN::fwMoveDumpNodeN(const fwEntity *pEntity, const crmtNodeN &node)
: base(pEntity, node)
{
	for(u32 uChildIndex = 0; uChildIndex < node.GetNumChildren(); uChildIndex ++)
	{
		m_Weights.PushAndGrow(node.GetWeight(uChildIndex));
	}
}

///////////////////////////////////////////////////////////////////////////////

float fwMoveDumpNodeAddN::GetChildWeight(int iChildIndex) const
{
	float fWeight = 1.0f;

	Assert(iChildIndex >= 0 && iChildIndex < m_Weights.GetCount());
	if(iChildIndex >= 0 && iChildIndex < m_Weights.GetCount())
	{
		fWeight = m_Weights[iChildIndex];
	}

	return fWeight;
}

///////////////////////////////////////////////////////////////////////////////

float fwMoveDumpNodeBlendN::GetChildWeight(int iChildIndex) const
{
	float fWeight = 1.0f;

	Assert(iChildIndex >= 0 && iChildIndex < m_Weights.GetCount());
	if(iChildIndex >= 0 && iChildIndex < m_Weights.GetCount())
	{
		fWeight = m_Weights[iChildIndex];
	}

	return fWeight;
}

///////////////////////////////////////////////////////////////////////////////

/* Destructor */

fwMoveDumpNetwork::~fwMoveDumpNetwork()
{
	USE_DEBUG_ANIM_MEMORY();

	for(int i = 0; i < m_Requests.GetCount(); i ++)
	{
		delete m_Requests[i]; m_Requests[i] = NULL;
	}
	m_Requests.Reset();

	for(int i = 0; i < m_Flags.GetCount(); i ++)
	{
		delete m_Flags[i]; m_Flags[i] = NULL;
	}
	m_Flags.Reset();

	for(int i = 0; i < m_Dofs.GetCount(); i ++)
	{
		delete m_Dofs[i]; m_Dofs[i] = NULL;
	}
	m_Dofs.Reset();

	if(m_Root)
	{
		delete m_Root; m_Root = NULL;
	}
}

/* Constructors */

fwMoveDumpNetwork::fwMoveDumpNetwork()
: m_FrameIndex(0), m_FrameTime(0), m_FrameTimeStep(0), m_Root(NULL), m_Matrix(Matrix34::IdentityType), m_CaptureTimeMs(0), m_NumNodes(0)
{
}

class FrameDofIterator : public crFrameIterator< FrameDofIterator >
{
public:

	FrameDofIterator(const crFrame &frame, atArray< fwMoveDumpDof * > &dofs, const crSkeletonData &skelData)
		: crFrameIterator< FrameDofIterator >(frame)
		, m_Dofs(dofs)
		, m_SkelData(skelData)
	{
	}

	__forceinline void IterateDof(const crFrameData::Dof &dof, const crFrame::Dof &dest, float /*weight*/)
	{
		USE_DEBUG_ANIM_MEMORY();

		u8 track = dof.m_Track;
		u16 id = dof.m_Id;
		u8 type = dof.m_Type;
		bool invalid = dest.IsInvalid();

		switch(type)
		{
		case kFormatTypeVector3:
			{
				Vector3 value = VEC3V_TO_VECTOR3(dest.GetUnsafe< Vec3V >());
				m_Dofs.PushAndGrow(rage_new fwMoveDumpDofVector3(track, id, type, invalid, m_SkelData, value));
			} break;
		case kFormatTypeQuaternion:
			{
				Quaternion value = QUATV_TO_QUATERNION(dest.GetUnsafe< QuatV >());
				m_Dofs.PushAndGrow(rage_new fwMoveDumpDofQuaternion(track, id, type, invalid, m_SkelData, value));
			} break;
		case kFormatTypeFloat:
			{
				float value = dest.GetUnsafe< float >();
				m_Dofs.PushAndGrow(rage_new fwMoveDumpDofFloat(track, id, type, invalid, m_SkelData, value));
			} break;
		default:
			{
				m_Dofs.PushAndGrow(rage_new fwMoveDumpDof(track, id, type, invalid, m_SkelData));
			} break;
		}
	}

	const crSkeletonData &m_SkelData;
	atArray< fwMoveDumpDof * > &m_Dofs;
};

struct CreateNode
{
	CreateNode() {}
	CreateNode(crmtNode& node)
		: m_Node(&node), m_DumpNode(NULL) { }

	crmtNode* m_Node;
	fwMoveDumpNode* m_DumpNode;
};

fwMoveDumpNetwork::fwMoveDumpNetwork(const fwEntity *pEntity, bool bCaptureDof, const char *szFilter, bool bRelativeToParent)
: m_FrameIndex(fwTimer::GetFrameCount()), m_FrameTime(fwTimer::GetTimeInMilliseconds()), m_FrameTimeStep(fwTimer::GetTimeStepInMilliseconds()), m_Root(NULL), m_Matrix(Matrix34::IdentityType), m_Entity(pEntity->GetModelName()), m_Filter(szFilter), m_CaptureTimeMs(0), m_NumNodes(0)
{
	u32 startCaptureTimeMs = sysTimer::GetSystemMsTime();

	Assert(pEntity);
	if(pEntity)
	{
		fwAnimDirector *pAnimDirector = pEntity->GetAnimDirector();
		Assert(pAnimDirector);
		if(pAnimDirector)
		{
#if __DEV
			const mvNetwork *pNetwork = pAnimDirector->GetMove().GetNetwork();
			if(pNetwork)
			{
				const mvNetworkDef *pNetworkDef = pNetwork->GetDefinition();
				if(pNetworkDef)
				{
					const mvSignalData &signalData = pNetwork->GetSignalData();

					m_Requests.Reserve(pNetworkDef->GetRequestCount());
					const request_t *pRequests = pNetworkDef->GetRequests();
					for(int i = 0, count = pNetworkDef->GetRequestCount(); i < count; i ++)
					{
						const request_t &request = pRequests[i];
						if(request.key != (u32)-1)
						{
							atHashString name(request.key);
							bool bValue = signalData.GetRequest(request.index);
							m_Requests.PushAndGrow(rage_new fwMoveDumpNetworkRequest(name, bValue));
						}
					}

					m_Flags.Reserve(pNetworkDef->GetFlagCount());
					const flag_t *pFlags = pNetworkDef->GetFlags();
					for(int i = 0, count = pNetworkDef->GetFlagCount(); i < count; i ++)
					{
						const flag_t &flag = pFlags[i];
						if(flag.key != (u32)-1)
						{
							atHashString name(flag.key);
							bool bValue = signalData.GetFlag(flag.index);
							m_Flags.PushAndGrow(rage_new fwMoveDumpNetworkFlag(name, bValue));
						}
					}
				}
			}
#endif // __DEV

			crmtNode *pRootNode = pAnimDirector->GetMove().GetMotionTree().GetRootNode();
			if(pRootNode)
			{
#if !FLATTEN_CREATE_MOVE_DUMP_NODE
				m_Root = CreateMoveDumpNode(pEntity, *pRootNode);
#else // !FLATTEN_CREATE_MOVE_DUMP_NODE
				atArray<CreateNode> nodes;
				nodes.PushAndGrow(CreateNode(*pRootNode));

				do 
				{					
					while(1)
					{
						crmtNode* node = nodes.Top().m_Node;
						nodes.Top().m_DumpNode = CreateMoveDumpNode(pEntity, *node);

						if(nodes.Top().m_Node->HasChildren())
						{
							nodes.Top().m_DumpNode->GetChildren().Reserve(nodes.Top().m_Node->GetNumChildren());
							nodes.PushAndGrow(CreateNode(*nodes.Top().m_Node->GetFirstChild()));
							continue;
						}
						break;
					}
					
					do
					{
						CreateNode top = nodes.Pop();

						if(nodes.GetCount()>0)
						{
							nodes.Top().m_DumpNode->GetChildren().Push(top.m_DumpNode);
						}
						else
						{
							m_Root = top.m_DumpNode;
							break;
						}
					
						if(top.m_Node->GetNextSibling())
						{
							nodes.Push(CreateNode(*top.m_Node->GetNextSibling()));
							break;
						}
					}
					while(1);
				} 
				while(nodes.GetCount() > 0);
#endif // !FLATTEN_CREATE_MOVE_DUMP_NODE
			}
		}

		if(bCaptureDof)
		{
			fwAnimDirector *pAnimDirector = pEntity->GetAnimDirector();
			Assert(pAnimDirector);
			if(pAnimDirector)
			{
				m_Matrix = MAT34V_TO_MATRIX34(pEntity->GetMatrix());

				if (bRelativeToParent && pEntity->GetSkeleton() && pEntity->GetSkeleton()->GetParentMtx())
				{
					m_Matrix.DotTranspose(RCC_MATRIX34(*pEntity->GetSkeleton()->GetParentMtx()));
				}

				crFrameFilter *filter = NULL;
				if(szFilter && strlen(szFilter) > 0)
				{
					filter = g_FrameFilterDictionaryStore.FindFrameFilter(fwMvFilterId(szFilter));
				}
				const crSkeletonData &skelData = pEntity->GetSkeletonData();
				crSkeleton &skel = *pEntity->GetSkeleton();

				crFrame* pFrame = pAnimDirector->GetFrameBuffer().AllocateFrame();
				pFrame->InversePose(skel, false, filter);

				crCreature *pCreature = pEntity->GetCreature();
				if(pCreature)
				{
					const crCreatureComponentExtraDofs *pExtraDofs = pCreature->FindComponent<crCreatureComponentExtraDofs>();
					if(pExtraDofs)
					{
						pFrame->Set(pExtraDofs->GetPoseFrame(), filter);
					}

					FrameDofIterator it(*pFrame, m_Dofs, skelData);
					it.Iterate(filter, 1.0f, false);
				}

				pAnimDirector->GetFrameBuffer().ReleaseFrame(pFrame);
			}
		}
	}

	m_CaptureTimeMs = sysTimer::GetSystemMsTime() - startCaptureTimeMs;
}

/* Properties */

fwMoveDumpNetwork::String fwMoveDumpNetwork::GetDescription(const fwMoveDumpRenderFlags &/*renderFlags*/) const
{
	return VarString("%s FrameIndex:%u FrameTime:%u FrameTimeStep:%u Entity:%s Filter:%s CaptureTimeMs:%u NumNodes:%u", GetFriendlyClassName(), m_FrameIndex, m_FrameTime, m_FrameTimeStep, m_Entity.c_str(), m_Filter.c_str(), m_CaptureTimeMs, m_NumNodes);
}

/* Operations */

void fwMoveDumpNetwork::Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags)
{
	Vector2 position(fOriginX, fOriginY);
	Color32 color(1.0f, 1.0f, 0.0f, 1.0f);
	bool background = true;

	fOriginY += fVerticalIndent;

	if(renderFlags.m_bRenderRequests && m_Requests.GetCount() > 0)
	{
		Vector2 requestPos(fOriginX, fOriginY);

		String description(VarString("[Requests(%i): ", m_Requests.GetCount()));
		for(int i = 0, count = m_Requests.GetCount(); i < count; i ++)
		{
			fwMoveDumpNetworkRequest *pRequest = m_Requests[i];
			const char *szName = pRequest->GetName().TryGetCStr();
			if(szName)
			{
				description += VarString("%s:%s", szName, pRequest->GetValue() ? "TRUE" : "FALSE");
			}
			else
			{
				description += VarString("%u:%s", pRequest->GetName().GetHash(), pRequest->GetValue() ? "TRUE" : "FALSE");
			}

			if(i + 1 < count)
			{
				description += " ";
			}
		}
		description += "]";

		grcDebugDraw::Text(requestPos, color, description.c_str(), background);

		fOriginY += fVerticalIndent;
	}

	if(renderFlags.m_bRenderFlags && m_Flags.GetCount() > 0)
	{
		Vector2 flagPos(fOriginX, fOriginY);

		String description(VarString("[Flags(%i): ", m_Flags.GetCount()));
		for(int i = 0, count = m_Flags.GetCount(); i < count; i ++)
		{
			fwMoveDumpNetworkFlag *pFlag = m_Flags[i];
			const char *szName = pFlag->GetName().TryGetCStr();
			if(szName)
			{
				description += VarString("%s:%s", szName, pFlag->GetValue() ? "TRUE" : "FALSE");
			}
			else
			{
				description += VarString("%u:%s", pFlag->GetName().GetHash(), pFlag->GetValue() ? "TRUE" : "FALSE");
			}

			if(i + 1 < count)
			{
				description += " ";
			}
		}
		description += "]";

		grcDebugDraw::Text(flagPos, color, description.c_str(), background);

		fOriginY += fVerticalIndent;
	}

	if(renderFlags.m_bRenderTree && m_Root)
	{
		m_NumNodes = 0;
		m_Root->WalkAndRender(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, 1.0f, 1.0f, false, m_NumNodes);
	}

	if(renderFlags.m_bRenderDofs)
	{
		Vector2 dofPos(fOriginX, fOriginY);

		const Vector3 &a = m_Matrix.a;
		const Vector3 &b = m_Matrix.b;
		const Vector3 &c = m_Matrix.c;
		const Vector3 &d = m_Matrix.d;

		String description;

		description += VarString("Matrix: a(%.3f %.3f %.3f) b(%.3f %.3f %.3f) c(%.3f %.3f %.3f) d(%.3f %.3f %.3f)", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z);

		grcDebugDraw::Text(dofPos, color, description.c_str(), background);

		fOriginY += fVerticalIndent;

		for(int i = 0; i < m_Dofs.GetCount(); i ++)
		{
			m_Dofs[i]->Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags, 1.0f);
		}
	}

	grcDebugDraw::Text(position, color, GetDescription(renderFlags).c_str(), background);
}

void fwMoveDumpNetwork::Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags)
{
	rPrinter.PrintLine(iHorizontalIndent, GetDescription(renderFlags).c_str());

	if(renderFlags.m_bRenderRequests && m_Requests.GetCount() > 0)
	{
		fwMoveDumpString description(atVarString("[Requests(%i): ", m_Requests.GetCount()));
		for(int i = 0, count = m_Requests.GetCount(); i < count; i ++)
		{
			fwMoveDumpNetworkRequest *pRequest = m_Requests[i];
			const char *szName = pRequest->GetName().TryGetCStr();
			if(szName)
			{
				description += atVarString("%s:%s", szName, pRequest->GetValue() ? "TRUE" : "FALSE");
			}
			else
			{
				description += atVarString("%u:%s", pRequest->GetName().GetHash(), pRequest->GetValue() ? "TRUE" : "FALSE");
			}

			if(i + 1 < count)
			{
				description += " ";
			}
		}
		description += "]";

		rPrinter.PrintLine(iHorizontalIndent, description.c_str());
	}

	if(renderFlags.m_bRenderFlags && m_Flags.GetCount() > 0)
	{
		fwMoveDumpString description(atVarString("[Flags(%i): ", m_Flags.GetCount()));
		for(int i = 0, count = m_Flags.GetCount(); i < count; i ++)
		{
			fwMoveDumpNetworkFlag *pFlag = m_Flags[i];
			const char *szName = pFlag->GetName().TryGetCStr();
			if(szName)
			{
				description += atVarString("%s:%s", szName, pFlag->GetValue() ? "TRUE" : "FALSE");
			}
			else
			{
				description += atVarString("%u:%s", pFlag->GetName().GetHash(), pFlag->GetValue() ? "TRUE" : "FALSE");
			}

			if(i + 1 < count)
			{
				description += " ";
			}
		}
		description += "]";

		rPrinter.PrintLine(iHorizontalIndent, description.c_str());
	}

	if(renderFlags.m_bRenderTree && m_Root)
	{
		m_NumNodes = 0;
		m_Root->WalkAndPrint(rPrinter, iHorizontalIndent, renderFlags, 1.0f, 1.0f, false, m_NumNodes);
	}

	if(renderFlags.m_bRenderDofs)
	{
		const Vector3 &a = m_Matrix.a;
		const Vector3 &b = m_Matrix.b;
		const Vector3 &c = m_Matrix.c;
		const Vector3 &d = m_Matrix.d;

		fwMoveDumpString description;

		description += atVarString("Matrix: a(%.3f %.3f %.3f) b(%.3f %.3f %.3f) c(%.3f %.3f %.3f) d(%.3f %.3f %.3f)", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z);

		rPrinter.PrintLine(iHorizontalIndent, description.c_str());

		for(int i = 0; i < m_Dofs.GetCount(); i ++)
		{
			m_Dofs[i]->Print(rPrinter, iHorizontalIndent, renderFlags, 1.0f);
		}
	}
}

void fwMoveDumpNetwork::Apply(fwEntity *pEntity, const char *szFilter, bool bRelativeToParent) const
{
	Assert(pEntity);
	if(pEntity)
	{
		if(m_Dofs.GetCount() > 0)
		{
			Matrix34 newMatrix(m_Matrix);
			if (bRelativeToParent && pEntity->GetSkeleton() && pEntity->GetSkeleton()->GetParentMtx())
			{
				newMatrix.Dot(RCC_MATRIX34(*pEntity->GetSkeleton()->GetParentMtx()));
			}

			pEntity->SetMatrix(newMatrix);

			crFrameFilter *filter = NULL;
			if(szFilter && strlen(szFilter) > 0)
			{
				filter = g_FrameFilterDictionaryStore.FindFrameFilter(fwMvFilterId(szFilter));
			}
			const crSkeletonData &skelData = pEntity->GetSkeletonData();
			crSkeleton &skel = *pEntity->GetSkeleton();

			crFrame frame;
			frame.InitCreateBoneAndMoverDofs(skelData, true, 0, true, filter);
			frame.IdentityFromSkel(skelData, filter);

			for(int i = 0; i < m_Dofs.GetCount(); i ++)
			{
				const fwMoveDumpDof *dof = m_Dofs[i];
				if(dof->GetTrack() != kTrackMoverTranslation && dof->GetTrack() != kTrackMoverRotation)
				{
					switch(dof->GetType())
					{
					case kFormatTypeVector3:
						{
							const fwMoveDumpDofVector3 *dofVector3 = static_cast< const fwMoveDumpDofVector3 * >(dof);
							frame.SetVector3(dof->GetTrack(), dof->GetId(), VECTOR3_TO_VEC3V(dofVector3->GetValue()));
						} break;
					case kFormatTypeQuaternion:
						{
							const fwMoveDumpDofQuaternion *dofQuaternion = static_cast< const fwMoveDumpDofQuaternion * >(dof);
							frame.SetQuaternion(dof->GetTrack(), dof->GetId(), QUATERNION_TO_QUATV(dofQuaternion->GetValue()));
						} break;
					case kFormatTypeFloat:
						{
							const fwMoveDumpDofFloat *dofFloat = static_cast< const fwMoveDumpDofFloat * >(dof);
							frame.SetFloat(dof->GetTrack(), dof->GetId(), dofFloat->GetValue());
						} break;
					default:
						{
							Assert(false);
						} break;
					}
				}
			}

			frame.Pose(skel, false, filter);
			skel.Update();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

/* Constructors */

fwMoveDumpDof::fwMoveDumpDof(u8 track, u16 id, u8 type, bool invalid, const crSkeletonData &skelData)
: m_Track(track), m_Id(id), m_Type(type), m_Invalid(invalid), m_IdName(NULL)
{
	int boneIdx = -1;
	skelData.ConvertBoneIdToIndex(m_Id, boneIdx);
	if(boneIdx >= 0)
	{
		const crBoneData *pBoneData = skelData.GetBoneData(boneIdx);
		if(pBoneData)
		{
			m_IdName = pBoneData->GetName();
		}
	}
	if(!m_IdName)
	{
		m_IdName = GetBoneTagName(m_Id);
	}
#if !__FINAL
	if(!m_IdName)
{
		m_IdName = fwAnimDirector::GetDOFName(m_Id);
	}
#endif // !__FINAL
}

/* Properties */

const char *fwMoveDumpDof::GetTrackName() const
{
	const char *szTrack = crAnimTrack::ConvertTrackIndexToName(m_Track);
	if(!szTrack)
	{
		szTrack = eTrackType_ToString(static_cast<eTrackType>(m_Track));
	}
	return szTrack;
}

fwMoveDumpDof::String fwMoveDumpDof::GetDescription(const fwMoveDumpRenderFlags &/*renderFlags*/) const
{
	return VarString("Track:%3u %-28s Id:%5u %-34s %-7s", m_Track, GetTrackName(), m_Id, GetIdName(), m_Invalid ? "Invalid" : "Valid");
}

/* Operations */

void fwMoveDumpDof::Render(float &fOriginX, float &fOriginY, float /*fHorizontalIndent*/, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight) const
{
	Vector2 position(fOriginX, fOriginY);

	Color32 color;
	bool background;
	if(fWeight < 0.001f)
	{
		color.Setf(1.0f, 1.0f, 1.0f, 0.5f);
		background = false;
	}
	else
	{
		color.Setf(1.0f, 1.0f, 0.0f, 0.5f + (fWeight * (1.0f - 0.5f)));
		background = true;
	}

	grcDebugDraw::Text(position, color, GetDescription(renderFlags).c_str(), background);

	fOriginY += fVerticalIndent;
}

void fwMoveDumpDof::Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float /*fWeight*/) const
{
	rPrinter.PrintLine(iHorizontalIndent, GetDescription(renderFlags).c_str());
}

///////////////////////////////////////////////////////////////////////////////

/* Properties */

fwMoveDumpDofVector3::String fwMoveDumpDofVector3::GetDescription(const fwMoveDumpRenderFlags &renderFlags) const
{
	return VarString("%s Vector3:%.3f,%.3f,%.3f", base::GetDescription(renderFlags).c_str(), m_Value.GetX(), m_Value.GetY(), m_Value.GetZ());
}

///////////////////////////////////////////////////////////////////////////////

/* Properties */

fwMoveDumpDofQuaternion::String fwMoveDumpDofQuaternion::GetDescription(const fwMoveDumpRenderFlags &renderFlags) const
{
	if(!m_Invalid)
	{
		Vector3 eulers; m_Value.ToEulers(eulers);
		return VarString("%s Quat:%6.3f %6.3f %6.3f %6.3f (%7.2f %7.2f %7.2f)", base::GetDescription(renderFlags).c_str(), m_Value.x, m_Value.y, m_Value.z, m_Value.w, eulers.x * RtoD, eulers.y * RtoD, eulers.z * RtoD);
	}
	else
	{
		return VarString("%s Quat:%6.3f %6.3f %6.3f %6.3f", base::GetDescription(renderFlags).c_str(), m_Value.x, m_Value.y, m_Value.z, m_Value.w);
	}
}

///////////////////////////////////////////////////////////////////////////////

/* Properties */

fwMoveDumpDofFloat::String fwMoveDumpDofFloat::GetDescription(const fwMoveDumpRenderFlags &renderFlags) const
{
	return VarString("%s Float:%6.3f", base::GetDescription(renderFlags).c_str(), m_Value);
}

///////////////////////////////////////////////////////////////////////////////

/* Destructor */

fwMoveDumpFrameSequence::~fwMoveDumpFrameSequence()
{
	USE_DEBUG_ANIM_MEMORY();

	for(int i = 0; i < m_Frames.GetCount(); i ++)
	{
		delete m_Frames[i]; m_Frames[i] = NULL;
	}
	m_Frames.Reset();
}

/* Constructors */

fwMoveDumpFrameSequence::fwMoveDumpFrameSequence()
: m_MaxFrameCount(0), m_StartFrame(0), m_CurrentFrame(0)
{
}

fwMoveDumpFrameSequence::fwMoveDumpFrameSequence(u32 uMaxFrames)
: m_MaxFrameCount(uMaxFrames), m_StartFrame(0), m_CurrentFrame(0)
{
	USE_DEBUG_ANIM_MEMORY();

	if(m_MaxFrameCount > 0)
	{
		m_Frames.Reserve(m_MaxFrameCount);
	}
}

/* Operations */

void fwMoveDumpFrameSequence::Capture(const fwEntity *pEntity, bool bCaptureDof, const char *szFilter, bool bRelativeToParent)
{
	USE_DEBUG_ANIM_MEMORY();

	if(m_MaxFrameCount == 0)
	{
		m_Frames.PushAndGrow(rage_new fwMoveDumpNetwork(pEntity, bCaptureDof, szFilter, bRelativeToParent));
	}
	else
	{
		m_CurrentFrame = m_StartFrame;

		m_StartFrame = (m_StartFrame + 1) % m_MaxFrameCount;

		if(static_cast< u32 >(m_Frames.GetCount()) == m_MaxFrameCount)
		{
			delete m_Frames[m_CurrentFrame]; m_Frames[m_CurrentFrame] = NULL;
		}
		else
		{
			m_Frames.Resize(m_Frames.GetCount() + 1);
		}

		m_Frames[m_CurrentFrame] = rage_new fwMoveDumpNetwork(pEntity, bCaptureDof, szFilter, bRelativeToParent);
	}
}

void fwMoveDumpFrameSequence::Render(u32 uFrameIndex, float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags) const
{
	USE_DEBUG_ANIM_MEMORY();

	if (!m_Frames.GetCount())
		return; //Avoid % 0

	Vector2 position(fOriginX, fOriginY);
	Color32 color(1.0f, 1.0f, 0.0f, 1.0f);
	bool background = true;

	u32 uOffsetFrameIndex = (m_StartFrame + uFrameIndex) % m_Frames.GetCount();

//#if USE_MOVE_DUMP_OBJECT_POOL
//
//	atString memPool = atVarString("%u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u",
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNode),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeAnimation),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeClip),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeFrame),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeIdentity),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeInvalid),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodePm),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodePose),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeProxy),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeParent),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeCapture),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeExpression),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeExtrapolate),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeFilter),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeIk),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeJointLimit),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeMerge),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeMirror),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeState),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodePair),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeAddSubtract),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeBlend),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeN),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeAddN),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeBlendN),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpNodeMergeN),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpDof),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpDofVector3),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpDofQuaternion),
//		GET_MOVE_DUMP_OBJECT_POOL_ALLOCATED_COUNT(fwMoveDumpDofFloat));
//
//	grcDebugDraw::Text(position, color, memPool.c_str(), background);
//
//	fOriginY += fVerticalIndent; position.y = fOriginY;
//
//#endif // USE_MOVE_DUMP_OBJECT_POOL

	String description = VarString("FrameIndex:%u FrameCount:%i, MaxFrameCount:%u", uFrameIndex, m_Frames.GetCount(), m_MaxFrameCount);

	grcDebugDraw::Text(position, color, description.c_str(), background);

	fOriginY += fVerticalIndent; position.y = fOriginY;

	if(uOffsetFrameIndex < static_cast< u32 >(m_Frames.GetCount()))
	{
		m_Frames[uOffsetFrameIndex]->Render(fOriginX, fOriginY, fHorizontalIndent, fVerticalIndent, renderFlags);
	}
}

void fwMoveDumpFrameSequence::Print(fwMoveDumpPrinterBase &rPrinter, u32 uFrameIndex, const fwMoveDumpRenderFlags &renderFlags) const
{
	USE_DEBUG_ANIM_MEMORY();

	if (!m_Frames.GetCount())
		return; //Avoid % 0

	u32 uOffsetFrameIndex = (m_StartFrame + uFrameIndex) % m_Frames.GetCount();

	char buffer[512];
	formatf(buffer, "FrameIndex:%u FrameCount:%i, MaxFrameCount:%u", uFrameIndex, m_Frames.GetCount(), m_MaxFrameCount);
	rPrinter.PrintLine(0, buffer);

	if(uOffsetFrameIndex < static_cast< u32 >(m_Frames.GetCount()))
	{
		int iHorizontalIndex = 0;

		m_Frames[uOffsetFrameIndex]->Print(rPrinter, iHorizontalIndex, renderFlags);
	}
}

void fwMoveDumpFrameSequence::Reset()
{
	USE_DEBUG_ANIM_MEMORY();

	for(int i = 0; i < m_Frames.GetCount(); i ++)
	{
		delete m_Frames[i]; m_Frames[i] = NULL;
	}
	m_Frames.Reset();

	m_Frames.Reserve(m_MaxFrameCount);

	m_StartFrame = 0;
}

void fwMoveDumpFrameSequence::Apply(fwEntity *pEntity, u32 uFrameIndex, const char *szFilter, bool bRelativeToParent) const
{
	USE_DEBUG_ANIM_MEMORY();

	u32 uOffsetFrameIndex = (m_StartFrame + uFrameIndex) % m_Frames.GetCount();

	if(uOffsetFrameIndex < static_cast< u32 >(m_Frames.GetCount()))
	{
		m_Frames[uOffsetFrameIndex]->Apply(pEntity, szFilter, bRelativeToParent);
	}
}

bool fwMoveDumpFrameSequence::Load(const char *szFilename)
{
	USE_DEBUG_ANIM_MEMORY();

	bool loaded = false;

	Reset();

	ASSET.PushFolder(g_szMotionTreeDumpDirectory);

	atString filenameString(szFilename);
	filenameString += ".pso";

	psoFile *pPsoFile = psoLoadFile(filenameString.c_str());
	Assert(pPsoFile);
	if(pPsoFile)
	{
		loaded = psoLoadObject(*pPsoFile, *this);
		Assert(loaded);

		delete pPsoFile; pPsoFile = NULL;
	}

	ASSET.PopFolder();

	return loaded;
}

bool fwMoveDumpFrameSequence::Save(const char *szFilename)
{
	USE_DEBUG_ANIM_MEMORY();

	bool saved = false;

	ASSET.PushFolder(g_szMotionTreeDumpDirectory);

	atString filenameString(szFilename);
	filenameString += ".pso";

	saved = psoSaveObject(filenameString.c_str(), this);

	ASSET.PopFolder();

	return saved;
}

bool fwMoveDumpFrameSequence::Print(const char *szFilename, u32 uFrameIndex, const fwMoveDumpRenderFlags &renderFlags) const
{
	USE_DEBUG_ANIM_MEMORY();

	if (!m_Frames.GetCount())
		return false; //Avoid % 0

	bool printed = false;

	ASSET.PushFolder(g_szMotionTreeDumpDirectory);

	fiStream *pFile = ASSET.Create(szFilename, "txt");
	if(pFile)
	{
		fwMoveDumpPrinterFile printer(pFile);
		Print(printer, uFrameIndex, renderFlags);

		pFile->Close(); pFile = NULL;

		printed = true;
	}

	ASSET.PopFolder();

	return printed;
}

bool fwMoveDumpFrameSequence::Print(const char *szFilename, const fwMoveDumpRenderFlags &renderFlags) const
{
	USE_DEBUG_ANIM_MEMORY();

	if (!m_Frames.GetCount())
		return false; //Avoid % 0

	bool printed = false;

	ASSET.PushFolder(g_szMotionTreeDumpDirectory);

	fiStream *pFile = ASSET.Create(szFilename, "txt");
	if(pFile)
	{
		fwMoveDumpPrinterFile printer(pFile);
		for(int uFrameIndex = 0; uFrameIndex < m_Frames.GetCount(); uFrameIndex ++)
		{
			Print(printer, uFrameIndex, renderFlags);
		}

		pFile->Close(); pFile = NULL;

		printed = true;
	}

	ASSET.PopFolder();

	return printed;
}

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif //__BANK
