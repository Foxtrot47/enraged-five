//
// fwanimation/directorcomponentsyncedscene.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "directorcomponentsyncedscene.h"

#include "animdirector.h"
#include "animhelpers.h"

#include "directorcomponentfacialrig.h"

#include "creature/creature.h"
#include "creature/componentextradofs.h"
#include "crmetadata/tagiterators.h"
#include "fwaudio/audioentity.h"
#include "system/ipc.h"

#include "boneids.h"

namespace rage
{

fwSyncedSceneAudioInterface *fwSyncedSceneAudioInterface::sm_pInstance = NULL;

fwSyncedSceneAudioInterface::~fwSyncedSceneAudioInterface()
{
	if(sm_pInstance == this)
	{
		sm_pInstance = NULL;
	}
}

fwSyncedSceneAudioInterface::fwSyncedSceneAudioInterface()
{
	if(animVerifyf(sm_pInstance == NULL, "fwSyncedSceneAudioInterface is a singleton, only one instance can exist at a time!"))
	{
		sm_pInstance = this;
	}
}

fwSyncedSceneAudioInterface *fwSyncedSceneAudioInterface::GetInstance()
{
	return sm_pInstance;
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentSyncedScene::fwAnimDirectorComponentSyncedScene()
: fwAnimDirectorComponent(kComponentTypeSyncedScene)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentSyncedScene::~fwAnimDirectorComponentSyncedScene()
{
	fwAnimDirectorComponentSyncedScene::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentSyncedScene, 0x16a315a2, kComponentTypeSyncedScene, ANIMDIRECTOR_POOL_PED_MAX, 0.71f);

////////////////////////////////////////////////////////////////////////////////

int fwAnimDirectorComponentSyncedScene::sm_iInitRefCount = 0;

#if __ASSERT
sysIpcCurrentThreadId g_SyncedSceneMainThreadId;
#endif // __ASSERT

void fwAnimDirectorComponentSyncedScene::StaticInit()
{
	animAssert(sm_iInitRefCount >= 0);

	if(sm_iInitRefCount == 0)
	{
		// Init!
		int syncedScenes = fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("SyncedScenes", 0x7BA86481), 1);
		ResizeSyncedScenePool(syncedScenes);
	}

	sm_iInitRefCount ++;

#if __ASSERT
	g_SyncedSceneMainThreadId = sysIpcGetCurrentThreadId();
#endif // __ASSERT
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::StaticShutdown()
{
	animAssert(sm_iInitRefCount > 0);

	sm_iInitRefCount --;

	if(sm_iInitRefCount == 0)
	{
		// Shutdown!

		if(sm_SynchronizedSceneIdMap.GetNumUsed() > 0)
		{
#if __BANK
			atString debugInfo; fwAnimDirectorComponentSyncedScene::DumpSynchronizedSceneDebugInfo(debugInfo);
			atArray< atString > splitString; debugInfo.Split(splitString, "\n", true);
			for(int i = 0; i < splitString.GetCount(); i ++)
			{
				animDisplayf("%s", splitString[i].c_str());
			}
#endif //__BANK

			animAssertf(sm_SynchronizedSceneIdMap.GetNumUsed() == 0, "Not all synced scenes have been shutdown!");
		}

		sm_SynchronizedScenes.Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::Init(fwAnimDirector& director)
{
	InitBase(director);

}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::Shutdown()
{
	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::ReceiveMessage(eMessageId msgId, ePhase phase, void* payload)
{
	switch(msgId)
	{
	case kMessageUpdate:
		if(phase == kPhasePrePhysics)
		{
			const float& deltaTime = *(float*)(payload);
			UpdateSyncedSceneParameters(deltaTime);
		}
		break;

	case kMessageShuttingDown:
		{
			if (IsPlayingSyncedScene())
			{
				StopSyncedScenePlayback(INSTANT_BLEND_OUT_DELTA, false);
			}
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::InitDerivedClass()
{
	// clear the synchronized scenes
	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		sm_SynchronizedScenes[i].Reset();
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::StartSyncedScenePlayback(int dictionaryIndex, const crClip* pClip, const crClip* pFacialClip, fwSyncedSceneId sceneId, const fwMvNetworkDefId &networkDefId, float blendInDelta /*= INSTANT_BLEND_IN_DELTA*/, bool exitOnSceneEnd /*= true*/, bool loopWithinScene /*= false */, bool blockMoverUpdate /*= false */)
{
	animAssert(pClip);

	StopSyncedScenePlayback(INSTANT_BLEND_OUT_DELTA);

	SyncedScene *pScene = GetSyncedScene(sceneId);
	if (animVerifyf(pScene, "Invalid sceneId %d in StartSynchronizedScenePlayback. The anim could not be played.", (s32)sceneId))
	{
		m_SyncedInstance.m_Clip = pClip;
		m_SyncedInstance.m_FacialClip = pFacialClip;
		m_SyncedInstance.m_DictionaryIndex = dictionaryIndex;
		m_SyncedInstance.m_ExitOnSceneEnd = exitOnSceneEnd;
		m_SyncedInstance.m_LoopWithinScene = loopWithinScene;
		m_SyncedInstance.m_BlockMoverUpdate = blockMoverUpdate;

		fwAnimManager::AddRef(strLocalIndex(dictionaryIndex));

		//blend in the network here
		const mvNetworkDef& pDef = GetDirector().GetNetworkDef(networkDefId);

		m_SyncedInstance.m_Network = GetDirector().CreateNetworkPlayer(pDef);
		m_SyncedInstance.m_Network->AddRef();

		float blendDuration = fwAnimHelpers::CalcBlendDuration(blendInDelta);

		bool successful = GetDirector().GetMove().SetSynchronizedSceneNetwork(m_SyncedInstance.m_Network, blendDuration);

		if (!successful)
		{
			animAssertf(0, "Synchronized scenes not currently supported on this entity!");
			m_SyncedInstance.m_Network->Release();
			return;
		}

		// set the clip pointer for the network to use and start the phase at 0.0f
		m_SyncedInstance.m_Network->SetClip(sm_ClipId, pClip);
		m_SyncedInstance.m_Network->SetFloat(sm_PhaseId, 0.0f);
		m_SyncedInstance.m_Network->SetFloat(sm_RateId, 0.0f);

		if(pFacialClip)
		{
			GetDirector().GetMove().SetClip(sm_FacialClipId, pFacialClip);
			GetDirector().GetMove().SetFloat(sm_FacialPhaseId, 0.0f);
			GetDirector().GetMove().SetFloat(sm_FacialRateId, 0.0f);
			GetDirector().GetMove().SetFloat(sm_FacialBlendInId, blendDuration);
		}

		AddSyncedSceneRef(sceneId);
		m_SyncedInstance.m_SceneId = sceneId;	

	#if __BANK
		RegisterWithSyncedScene(sceneId, &GetDirector().GetEntity());
	#endif // __BANK

		// if we have a valid clip and the sceneId duration has not been intialized, set it here
		if(pClip)
		{
			if(pClip->GetDuration() > GetSyncedSceneDuration(sceneId))
			{
				SetSyncedSceneDuration(sceneId, pClip->GetDuration());
			}
		}

		// Do this once when we first start the sceneId to eliminate single frame pops
		// When starting the sceneId from script.
		UpdateSyncedSceneMover();
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::StopSyncedScenePlayback(float blendOutDelta, bool tagSyncOut /*= false*/)
{
	if(IsPlayingSyncedScene())
	{
		float blendDuration = fwAnimHelpers::CalcBlendDuration(blendOutDelta);
		if(blendDuration > 0.0f)
		{
			// If we're going to cross blend out the anim, set the rate to 1.0f so it still progresses
			m_SyncedInstance.m_Network->SetFloat(sm_RateId, 1.0f);
			UpdateSyncedSceneMover();
		}

		if (GetDirector().HasMoveInstance())
		{
			GetDirector().GetMove().ClearSynchronizedSceneNetwork(m_SyncedInstance.m_Network, blendDuration, tagSyncOut);

			// Clear code-driven, scripted facial
			fwAnimDirectorComponentFacialRig* pFacialRigComp = GetDirector().GetComponentByPhase<fwAnimDirectorComponentFacialRig>(fwAnimDirectorComponent::kPhaseAll);
			if (pFacialRigComp)
			{
				pFacialRigComp->ClearSyncedSceneFacialAnim(blendDuration);
			}
		}

		// Synced scene may have already been removed.
		if (fwAnimDirectorComponentSyncedScene::IsValidSceneId(m_SyncedInstance.m_SceneId))
		{
#if __BANK
			RemoveFromSyncedScene(m_SyncedInstance.m_SceneId, &GetDirector().GetEntity());
#endif //__BANK
			DecSyncedSceneRef(m_SyncedInstance.m_SceneId);
		}

		m_SyncedInstance.m_SceneId = -1;

		fwAnimManager::RemoveRefWithoutDelete(strLocalIndex(m_SyncedInstance.m_DictionaryIndex));
		// null the clip and network handle
		m_SyncedInstance.m_Clip = NULL;
		m_SyncedInstance.m_FacialClip = NULL;
		m_SyncedInstance.m_DictionaryIndex = -1;
		m_SyncedInstance.m_ExitOnSceneEnd = true;
		m_SyncedInstance.m_BlockMoverUpdate = false;

		m_SyncedInstance.m_Network->Release();
		m_SyncedInstance.m_Network = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::UpdateSyncedSceneParameters(float /*deltaTime*/)
{
	if (IsPlayingSyncedScene())
	{
		if (!fwAnimDirectorComponentSyncedScene::IsValidSceneId(m_SyncedInstance.m_SceneId))
		{
			// Exit synced scene
			StopSyncedScenePlayback();
		}
		else
		{
			SyncedScene *pScene = GetSyncedScene(m_SyncedInstance.m_SceneId);
			animAssertf(pScene, "Invalid synchronised sceneId!");
			if(pScene)
			{
				float fDuration = m_SyncedInstance.m_Clip ? m_SyncedInstance.m_Clip->GetDuration() : 0.0f;

				if(pScene->m_Time >= fDuration && !pScene->m_Looped && !pScene->m_HoldLastFrame && m_SyncedInstance.m_ExitOnSceneEnd)
				{
					// Exit synced sceneId
					StopSyncedScenePlayback();
				}
				else
				{
					// get the current phase of the sceneId and pass it to MoVE
					float newPhase = m_SyncedInstance.m_Clip ? m_SyncedInstance.m_Clip->ConvertTimeToPhase(pScene->m_Time) : 0.0f;
					float prevPhase = m_SyncedInstance.m_Clip ? m_SyncedInstance.m_Clip->ConvertTimeToPhase(pScene->m_PrevTime) : 0.0f;
					float moverDelta = m_SyncedInstance.m_Clip ? pScene->m_Time : 0.0f;
					if(m_SyncedInstance.m_LoopWithinScene)
					{
						if(newPhase > 1.0f) { newPhase -= floorf(newPhase); }
						else if(newPhase < 0.0f) { newPhase -= floorf(newPhase); }

						if(prevPhase > 1.0f) { prevPhase -= floorf(prevPhase); }
						else if(prevPhase < 0.0f) { prevPhase -= floorf(prevPhase); }
					}
					else
					{
						if(newPhase > 1.0f)
						{
							newPhase = 1.0f;
							moverDelta = 0.0f;
						}
						else if(newPhase < 0.0f)
						{
							newPhase = 0.0f;
							moverDelta = 0.0f;
						}

						if(prevPhase > 1.0f)
						{
							prevPhase = 1.0f;
						}
						else if(prevPhase < 0.0f)
						{
							prevPhase = 0.0f;
						}
					}

					float newAudioPhase = m_SyncedInstance.m_FacialClip ? m_SyncedInstance.m_FacialClip->ConvertTimeToPhase(pScene->m_Time) : 0.0f;
					if(newAudioPhase > 1.0f)
					{
						newAudioPhase = 1.0f;
					}
					else if(newAudioPhase < 0.0f)
					{
						newAudioPhase = 0.0f;
					}

					bool bLoopClip = pScene->m_Looped || m_SyncedInstance.m_LoopWithinScene;

					m_SyncedInstance.m_Network->SetFloat(sm_PhaseId, newPhase);
					m_SyncedInstance.m_Network->SetFloat(sm_MoverDeltaId, moverDelta);
					m_SyncedInstance.m_Network->SetClip	(sm_ClipId, m_SyncedInstance.m_Clip);
					m_SyncedInstance.m_Network->SetBoolean(sm_LoopedId, bLoopClip);

					GetDirector().GetMove().SetFloat(sm_FacialPhaseId, newAudioPhase);
					GetDirector().GetMove().SetFloat(sm_FacialRateId, 0.0f);
					GetDirector().GetMove().SetBoolean(sm_FacialLoopedId, bLoopClip);

					// If the clip is playing, add the delta (since we haven't updated the motion tree yet)
					if (!pScene->m_Paused)
					{
						//	pass the time step to the MoVE network (assuming we're doing this before the motion tree update
						//	the change should be reflected in the network this frame)
						m_SyncedInstance.m_Network->SetFloat(sm_DeltaId, pScene->m_Time - pScene->m_PrevTime);

						// Because rate is zero, we need to process tags manually
						const crTags *pTags = m_SyncedInstance.m_Clip->GetTags();
						if(pTags)
						{
							// Iterate through tags between previous phase and current phase
							for(crTagIterator it(*pTags, prevPhase, newPhase); *it; ++it)
							{
								const crTag *pTag = *it;
								const crProperty *pProperty = &pTag->GetProperty();

								// Audio
								static const u32 audioTag = ATSTRINGHASH("Audio", 0xB80C6AE8);
								static const u32 audioEventPropAttr = ATSTRINGHASH("Id", 0x1B60404D);

								// Looping Audio
								static const u32 loopingAudioTag = ATSTRINGHASH("LoopingAudio", 0xA31D8F23);
								static const u32 startStopKey = ATSTRINGHASH("Start", 0x84DC271F);

								// Facial
								static const u32 facialTag = ATSTRINGHASH("Facial",0x511E56C2);

								// Handle audio tags
								if(pTag->GetKey() == audioTag)
								{
									// Get audio event attribute
									const crPropertyAttribute *pPropertyAttribute = pProperty->GetAttribute(audioEventPropAttr);
									if(pPropertyAttribute && pPropertyAttribute->GetType() == crPropertyAttribute::kTypeHashString)
									{
										// Get audio event attribute value
										const crPropertyAttributeHashString *pPropertyAttributeHashString = static_cast< const crPropertyAttributeHashString * >(pPropertyAttribute);
										u32 audioEvent = pPropertyAttributeHashString->GetHashString().GetHash();

										// Handle audio event
										fwEntity *pEntity = &GetDirector().GetEntity();
										fwAudioEntity *pAudioEntity = pEntity->GetAudioEntity();
										if(pAudioEntity)
										{
											pAudioEntity->AddAnimEvent(audioEvent);
										}
										else
										{
											fwAudioEntity::HandleAnimEvent(audioEvent, pEntity);
										}
									}
								}
								// Handle looping audio tags
								else if(pTag->GetKey() == loopingAudioTag)
								{
									// Get audio event attribute
									crPropertyAttributeHashStringAccessor eventIdAccessor(pProperty->GetAttribute(audioEventPropAttr));
									crPropertyAttributeBoolAccessor startStopAccessor(pProperty->GetAttribute(startStopKey));

									if(eventIdAccessor.Valid())
									{
										u32 audioEvent = eventIdAccessor.GetPtr()->GetHash();

										bool startStop;
										startStopAccessor.Get(startStop);

										// Handle looping audio event
										fwEntity *pEntity = &GetDirector().GetEntity();
										fwAudioEntity *pAudioEntity = pEntity->GetAudioEntity();
										if(pAudioEntity)
										{
											pAudioEntity->HandleLoopingAnimEvent(audAnimEvent(audioEvent, startStop));
										}
									}
								}
								// Handle facial tags
								else if(pTag->GetKey() == facialTag)
								{
									const crProperty* prop = &pTag->GetProperty();
									if (prop)
									{
										GetDirector().GetEntity().HandleFacialAnimEvent(prop);
									}
								}
							}
						}
					}
					else
					{
						m_SyncedInstance.m_Network->SetFloat(sm_DeltaId, 0.0f);
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
void fwAnimDirectorComponentSyncedScene::GetSceneVelocity(Vec3V_InOut sceneVel) const
{
	if (IsValidSceneId(m_SyncedInstance.m_SceneId))
	{
		SyncedScene* pScene = GetSyncedScene(m_SyncedInstance.m_SceneId);

		if (pScene && pScene->m_AttachParent)
		{
			phCollider* pCol = pScene->m_AttachParent->GetCurrentCollider();
			
			if (pCol)
			{
				sceneVel = pCol->GetVelocity();
				return;
			}
		}
	}
	sceneVel.ZeroComponents();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::UpdateSyncedSceneMover()
{
	// early out if the entity is in ragdoll. The physics should be controlling its mover at this point
	if (GetDirector().HasMoveInstance() && GetDirector().GetMove().IsUsingRagDoll(GetDirector().GetEntity()))
	{
		return;
	}

	if (IsPlayingSyncedScene() && !m_SyncedInstance.m_BlockMoverUpdate && fwAnimDirectorComponentSyncedScene::IsValidSceneId(m_SyncedInstance.m_SceneId))
	{
		Matrix34 newMatrix(M34_IDENTITY);

		CalcEntityMatrix(m_SyncedInstance.m_SceneId, m_SyncedInstance.m_Clip, m_SyncedInstance.m_LoopWithinScene, newMatrix);

		fwAttachmentEntityExtension* pExtension = GetDirector().GetEntity().GetAttachmentExtension();
		if (pExtension)
		{
			pExtension->SetAttachFlag(ATTACH_FLAG_DONT_ASSERT_ON_MATRIX_CHANGE, true);
		}

		// Set the entity matrix
		GetDirector().GetEntity().SetMatrix(newMatrix);

		if (pExtension) 
		{
			pExtension->SetAttachFlag(ATTACH_FLAG_DONT_ASSERT_ON_MATRIX_CHANGE, false);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsPlayingSyncedScene() const
{
	return (m_SyncedInstance.m_Clip && m_SyncedInstance.m_Network);
}

////////////////////////////////////////////////////////////////////////////////

fwSyncedSceneId fwAnimDirectorComponentSyncedScene::GetSyncedSceneId() const
{
	return m_SyncedInstance.m_SceneId;
}

////////////////////////////////////////////////////////////////////////////////

const crClip* fwAnimDirectorComponentSyncedScene::GetSyncedSceneClip() const
{
	return m_SyncedInstance.m_Clip;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetBlockMoverUpdate(bool block)
{
	m_SyncedInstance.m_BlockMoverUpdate = block;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedScenePhase(fwSyncedSceneId sceneId, float newPhase)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedScenePhase", (s32)sceneId))
	{
		pScene->m_Time = newPhase * pScene->m_Duration;
		pScene->m_PrevTime = pScene->m_Time;
	}
}

////////////////////////////////////////////////////////////////////////////////

float fwAnimDirectorComponentSyncedScene::GetSyncedScenePhase(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedScenePhase", (s32)sceneId))
	{
		if(pScene->m_Duration > 0.0f)
		{
			return pScene->m_Time / pScene->m_Duration;
		}
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneRate(fwSyncedSceneId sceneId, float newRate)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneRate", (s32)sceneId))
	{
		pScene->m_Rate = newRate;
	}
}

////////////////////////////////////////////////////////////////////////////////

float fwAnimDirectorComponentSyncedScene::GetSyncedSceneRate(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedSceneRate", (s32)sceneId))
	{
		return pScene->m_Rate;
	}

	return 1.0f;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneTime(fwSyncedSceneId sceneId, float newTime)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneTime", (s32)sceneId))
	{
		pScene->m_Time = newTime;
		pScene->m_PrevTime = pScene->m_Time;
	}
}

////////////////////////////////////////////////////////////////////////////////

float fwAnimDirectorComponentSyncedScene::GetSyncedSceneTime(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedSceneTime", (s32)sceneId))
	{
		return pScene->m_Time;
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////////////////////////

float fwAnimDirectorComponentSyncedScene::GetSyncedScenePreviousTime(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedScenePreviousTime", (s32)sceneId))
	{
		return pScene->m_PrevTime;
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneDuration(fwSyncedSceneId sceneId, float duration)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneDuration", (s32)sceneId))
	{
		pScene->m_Duration = duration;
	}
}

#if __BANK

void fwAnimDirectorComponentSyncedScene::UpdateSyncedSceneDuration(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to UpdateSyncedSceneDuration", (s32)sceneId))
	{
		float duration = 0.0f;

		for(int i = 0; i < pScene->m_AttachedCameras.GetCount(); i ++)
		{
			const crClip *pClip = pScene->m_AttachedCameras[i].GetCamClip();
			if(pClip)
			{
				if(pClip->GetDuration() > duration)
				{
					duration = pClip->GetDuration();
				}
			}
		}

		for(int i = 0; i < pScene->m_AttachedDynamicEntities.GetCount(); i ++)
		{
			fwEntity *pEntity = pScene->m_AttachedDynamicEntities[i];
			if(pEntity)
			{
				fwAnimDirector *pAnimDirector = pEntity->GetAnimDirector();
				if(pAnimDirector)
				{
					fwAnimDirectorComponentSyncedScene *pAnimDirectorComponentSyncedScene = pAnimDirector->GetComponent< fwAnimDirectorComponentSyncedScene >();
					if(pAnimDirectorComponentSyncedScene)
					{
						const crClip *pClip = pAnimDirectorComponentSyncedScene->m_SyncedInstance.m_Clip;
						if(pClip)
						{
							if(pClip->GetDuration() > duration)
							{
								duration = pClip->GetDuration();
							}
						}
					}
				}
			}
		}

		pScene->m_Duration = duration;
	}
}

#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

float fwAnimDirectorComponentSyncedScene::GetSyncedSceneDuration(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedSceneDuration", (s32)sceneId))
	{
		return pScene->m_Duration;
	}

	return 0.0f;
}

////////////////////////////////////////////////////////////////////////////////

atHashString fwAnimDirectorComponentSyncedScene::GetSyncedSceneAudioEvent(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedSceneAudioEvent", (s32)sceneId))
	{
		return pScene->m_AudioHash;
	}

	return atHashString::Null();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneOrigin(fwSyncedSceneId sceneId, Vector3& scenePosition, Quaternion& sceneOrientation)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneOrigin", (s32)sceneId))
	{
		pScene->m_ScenePosition = scenePosition;
		pScene->m_SceneOrientation = sceneOrientation;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneOrigin(fwSyncedSceneId sceneId, Matrix34& sceneMatrix)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneOrigin", (s32)sceneId))
	{
		pScene->m_ScenePosition = sceneMatrix.d;
		sceneMatrix.ToQuaternion(pScene->m_SceneOrientation);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::GetSyncedSceneOrigin(fwSyncedSceneId sceneId, Matrix34& outMatrix)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to GetSyncedSceneOrigin", (s32)sceneId))
	{
		pScene->GetRootMatrix(outMatrix);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsSyncedScenePaused(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to IsSyncedScenePaused", (s32)sceneId))
	{
		return pScene->m_Paused;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedScenePaused(fwSyncedSceneId sceneId, bool pause)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedScenePaused", (s32)sceneId))
	{
		pScene->m_Paused = pause;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsSyncedSceneAbsolute(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to IsSyncedSceneAbsolute", (s32)sceneId))
	{
		return pScene->m_Absolute;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneAbsolute(fwSyncedSceneId sceneId, bool absolute)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneAbsolute", (s32)sceneId))
	{
		pScene->m_Absolute = absolute;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsSyncedSceneLooped(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to IsSyncedSceneLooped", (s32)sceneId))
	{
		return pScene->m_Looped;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneLooped(fwSyncedSceneId sceneId, bool looped)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneLooped", (s32)sceneId))
	{
		pScene->m_Looped = looped;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsSyncedSceneHoldLastFrame(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to IsSyncedSceneHoldLastFrame", (s32)sceneId))
	{
		return pScene->m_HoldLastFrame;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneHoldLastFrame(fwSyncedSceneId sceneId, bool holdLastFrame)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneHoldLastFrame", (s32)sceneId))
	{
		pScene->m_HoldLastFrame = holdLastFrame;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsSyncedSceneAbort(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to IsSyncedSceneAbort", (s32)sceneId))
	{
		return pScene->m_Abort;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetSyncedSceneAbort(fwSyncedSceneId sceneId, bool abort)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetSyncedSceneAbort", (s32)sceneId))
	{
		pScene->m_Abort = abort;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SetDestroySceneCallback(fwSyncedSceneId sceneId, void (*pDestroySceneCallback)(fwSyncedSceneId, void *), void *pDestroySceneCallbackParam)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to SetDestroySceneCallback", (s32)sceneId))
	{
		pScene->m_pDestroySceneCallback = pDestroySceneCallback;
		pScene->m_pDestroySceneCallbackParam = pDestroySceneCallbackParam;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::AttachSyncedScene(fwSyncedSceneId sceneId, fwEntity* parentEntity, int parentEntityBoneIndex)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to AttachSyncedScene", (s32)sceneId))
	{
		animAssert(parentEntity);

#if __BANK
		if(parentEntityBoneIndex > -1)
		{
			animAssertf(parentEntity->GetSkeleton(), "parentEntity %s does not have a skeleton", parentEntity->GetModelName());
			if(parentEntity->GetSkeleton())
			{
				animAssertf(parentEntityBoneIndex < (int)parentEntity->GetSkeleton()->GetBoneCount(), "Invalid parentEntityBoneIndex %d entity %s max index is %d", parentEntityBoneIndex, parentEntity->GetModelName(), parentEntity->GetSkeleton()->GetBoneCount());
			}
		}
#endif // __BANK

		pScene->m_AttachBoneIndex = (s16)parentEntityBoneIndex;
		pScene->m_AttachParent = parentEntity;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::DetachSyncedScene(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to DetachSyncedScene", (s32)sceneId))
	{
		pScene->m_AttachBoneIndex = -1;
		pScene->m_AttachParent = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::TakeOwnershipOfSyncedScene(fwSyncedSceneId sceneId, scrThreadId newScriptOwnerThreadId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if (animVerifyf(pScene, "Invalid sceneId %i, passed to TakeOwnershipOfSyncedScene", (s32)sceneId))
	{
		if (animVerifyf(newScriptOwnerThreadId != THREAD_INVALID, "THREAD_INVALID passed to TakeOwnershipOfSyncedScene for sceneId %i", (s32)sceneId))
		{
			pScene->m_ScriptThreadId = newScriptOwnerThreadId;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

fwEntity* fwAnimDirectorComponentSyncedScene::GetSyncedSceneAttachEntity(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to DetachSyncedScene", (s32)sceneId))
	{
		return pScene->m_AttachParent;
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::AddSyncedSceneRef(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to AddSyncedSceneRef", (s32)sceneId))
	{
		pScene->m_Refs ++;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::DecSyncedSceneRef(fwSyncedSceneId sceneId)
{
	Assert(g_SyncedSceneMainThreadId == sysIpcGetCurrentThreadId());

	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to DecSyncedSceneRef", (s32)sceneId))
	{
		pScene->m_Refs --;

		if(pScene->m_Refs <= 0)
		{
			//there are no more entities on this sceneId - make it inactive

			void (*pDestroySceneCallback)(fwSyncedSceneId, void *) = pScene->m_pDestroySceneCallback;
			void *pDestroySceneCallbackParam = pScene->m_pDestroySceneCallbackParam;

			pScene->Reset(); // pScene members are now reset

			ReleaseSyncedSceneId(sceneId); // sceneId is now invalid

			if(pDestroySceneCallback)
			{
				pDestroySceneCallback(sceneId, pDestroySceneCallbackParam);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

s32 fwAnimDirectorComponentSyncedScene::GetSyncedSceneRefCount(fwSyncedSceneId sceneId)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(pScene)
	{
		return pScene->m_Refs;
	}

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::UpdateSyncedScenes(float deltaTime)
{
	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		sm_SynchronizedScenes[i].Update(deltaTime);
	}

	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		// Check scene exists but is unreferenced
		SyncedScene &syncedScene = sm_SynchronizedScenes[i];
		if(syncedScene.m_Active && syncedScene.m_Refs == 0)
		{
			// Get scene id
			fwSyncedSceneId sceneId = INVALID_SYNCED_SCENE_ID;
			for(atMap<fwSyncedSceneId, int>::Iterator It = sm_SynchronizedSceneIdMap.CreateIterator(); !It.AtEnd(); It.Next())
			{
				if(It.GetData() == i)
				{
					sceneId = It.GetKey();

					break;
				}
			}
			if(animVerifyf(sceneId != INVALID_SYNCED_SCENE_ID, "Could not find synced scene id!"))
			{
#if __BANK
				atString debugInfo; fwAnimDirectorComponentSyncedScene::DumpSynchronizedSceneDebugInfo(debugInfo);
				atArray< atString > splitString; debugInfo.Split(splitString, "\n", true);
				for(int i = 0; i < splitString.GetCount(); i ++)
				{
					animDisplayf("%s", splitString[i].c_str());
				}
#endif //__BANK
				animAssertf(syncedScene.m_Active == false || syncedScene.m_Refs == 0, "Synced scene %i has been created but nothing has been added to it in the same frame so we're going to delete it, see log for more info!", sceneId);

				// Release scene
				AddSyncedSceneRef(sceneId);
				DecSyncedSceneRef(sceneId);
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

fwSyncedSceneId fwAnimDirectorComponentSyncedScene::StartSynchronizedScene(scrThreadId ScriptId)
{
	// If the synced scene pool is full, then try and free one of the existing, but "unowned" ones.
	if (IsSyncedScenePoolFull())
	{
		FreeAnUnownedSyncedScene();
	}

	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		if(!sm_SynchronizedScenes[i].m_Active)
		{
			sm_SynchronizedScenes[i].Reset();
			sm_SynchronizedScenes[i].m_Active = true;
			sm_SynchronizedScenes[i].m_ScriptThreadId = ScriptId;

			return CreateSyncedSceneId(i);
		}
	}

#if __BANK
	atString debugInfo; fwAnimDirectorComponentSyncedScene::DumpSynchronizedSceneDebugInfo(debugInfo);
	atArray< atString > splitString; debugInfo.Split(splitString, "\n", true);
	for(int i = 0; i < splitString.GetCount(); i ++)
	{
		animDisplayf("%s", splitString[i].c_str());
	}
#endif //__BANK

	animAssertf(0, "Unable to create new synchronized sceneId, see log for more info!");
	return INVALID_SYNCED_SCENE_ID;
}

////////////////////////////////////////////////////////////////////////////////

fwSyncedSceneId fwAnimDirectorComponentSyncedScene::FindSynchronizedScene(Vector3& position, Quaternion& orientation)
{
	float epsilon = 0.05f;

	for(atMap<fwSyncedSceneId, int>::Iterator It = sm_SynchronizedSceneIdMap.CreateIterator(); !It.AtEnd(); It.Next())
	{
		SyncedScene *pScene = &sm_SynchronizedScenes[It.GetData()];

		if(pScene->m_ScenePosition.IsClose(position, epsilon))
		{
			if((fabs(pScene->m_SceneOrientation.x - orientation.x) <= epsilon) &&
				(fabs(pScene->m_SceneOrientation.y - orientation.y) <= epsilon) &&
				(fabs(pScene->m_SceneOrientation.z - orientation.z) <= epsilon) &&
				(fabs(pScene->m_SceneOrientation.w - orientation.w) <= epsilon))
			{
				return It.GetKey();
			}
		}
	}
	return INVALID_SYNCED_SCENE_ID;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::FreeAnUnownedSyncedScene()
{
	animAssertf(IsSyncedScenePoolFull(), "fwAnimDirectorComponentSyncedScene::FreeAnUnownedSyncedScene() should only be called if the pool is actually full");
	
	int					iOldestSyncedSceneIndex		= -1;
	fwSyncedSceneId		iOldestSyncedSceneId		= INVALID_SYNCED_SCENE_ID;

	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		if(sm_SynchronizedScenes[i].m_Active && sm_SynchronizedScenes[i].m_ScriptThreadId != THREAD_INVALID)
		{
			scrThread* pThread = scrThread::GetThread(sm_SynchronizedScenes[i].m_ScriptThreadId);

			// Thread no longer exists.  This synced scene could be removed to make room.
			if(!pThread)
			{
				// Get the sceneId of this synced scene
				fwSyncedSceneId sceneId = INVALID_SYNCED_SCENE_ID;
				for(atMap<fwSyncedSceneId, int>::Iterator It = sm_SynchronizedSceneIdMap.CreateIterator(); !It.AtEnd(); It.Next())
				{
					if(It.GetData() == i)
					{
						sceneId = It.GetKey();
						break;
					}
				}

				// Is this scene older?
				if (iOldestSyncedSceneIndex == -1 || sceneId < iOldestSyncedSceneId)
				{
					iOldestSyncedSceneIndex = i;
					iOldestSyncedSceneId = sceneId;
				}
			}
		}
	}

	if (iOldestSyncedSceneIndex != -1 && iOldestSyncedSceneId != INVALID_SYNCED_SCENE_ID)
	{
		SyncedScene *pScene = GetSyncedScene(iOldestSyncedSceneId);
		if(animVerifyf(pScene, "Invalid sceneId %i, in FreeAnUnownedSyncedScene", (s32)iOldestSyncedSceneId))
		{
			void (*pDestroySceneCallback)(fwSyncedSceneId, void *) = pScene->m_pDestroySceneCallback;
			void *pDestroySceneCallbackParam = pScene->m_pDestroySceneCallbackParam;

			pScene->Reset(); // pScene members are now reset

			ReleaseSyncedSceneId(iOldestSyncedSceneId); // sceneId is now invalid

			if(pDestroySceneCallback)
			{
				pDestroySceneCallback(iOldestSyncedSceneId, pDestroySceneCallbackParam);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::IsValidSceneId(fwSyncedSceneId sceneId)
{
	int *pSceneIndex = sm_SynchronizedSceneIdMap.Access(sceneId);
	if(pSceneIndex)
	{
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

int fwAnimDirectorComponentSyncedScene::GetSyncedScenePoolSize()
{
	animAssertf(sm_SynchronizedScenes.GetCount() > 0, "Synced scene pool size retrieved before it has been init'd!");

	return sm_SynchronizedScenes.GetCount();
}

bool fwAnimDirectorComponentSyncedScene::IsSyncedScenePoolFull()
{
	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		if(sm_SynchronizedScenes[i].m_Active == false)
		{
			return false;
		}
	}

	return true;
}

void fwAnimDirectorComponentSyncedScene::ResizeSyncedScenePool(int poolSize)
{
	animAssertf(poolSize >= 1, "SyncedScenes pool size should be at least 1!");
	poolSize = Max(1, poolSize);

	sm_SynchronizedScenes.Reset(true);
	sm_SynchronizedScenes.Reserve(poolSize);
	sm_SynchronizedScenes.Resize(poolSize);
}

void fwAnimDirectorComponentSyncedScene::StartNetworkSession()
{
	// resize the synced scene pool based on the mp pool settings.
	int syncedScenes = fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("SyncedScenesMp", 0x6DA32494), 1);
	fwAnimDirectorComponentSyncedScene::ResizeSyncedScenePool(syncedScenes);
}

void fwAnimDirectorComponentSyncedScene::EndNetworkSession()
{
	// resize the synced scene pool based on the sp pool settings.
	int syncedScenes = fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("SyncedScenes", 0x7BA86481), 1);
	fwAnimDirectorComponentSyncedScene::ResizeSyncedScenePool(syncedScenes);
}

int fwAnimDirectorComponentSyncedScene::CountActiveSyncedScenes()
{
	int activeScenes = 0;
	for (int i = 0; i < sm_SynchronizedScenes.GetCount(); i++)
	{
		// Check scene exists but is unreferenced
		SyncedScene &syncedScene = sm_SynchronizedScenes[i];
		if (syncedScene.m_Active)
		{
			activeScenes++;
		}
	}
	return activeScenes;
}


void fwAnimDirectorComponentSyncedScene::CalcEntityMatrix(fwSyncedSceneId sceneId, const crClip* pClip, bool loopWithinScene, Matrix34& outMat, float time/* = -1.0f*/)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to CalcEntityMatrix", (s32)sceneId))
	{
		if (time<0.0f)
		{
			time = pScene->m_Time;
		}

		// Get the root matrix for the sceneId (this will take into account any attachments / etc)
		pScene->GetRootMatrix(outMat);

		// apply the initial offset from the clip
		fwAnimHelpers::ApplyInitialOffsetFromClip(*pClip, outMat);

		// THIS IS NOT GOOD!
		// have to calculate the mover on the ppu, which requires a decompress.
		float phase = pClip->ConvertTimeToPhase(time);
		if(!loopWithinScene)
		{
			if(phase > 1.0f)
			{
				phase = 1.0f;
			}
			else if(phase < 0.0f)
			{
				phase = 0.0f;
			}
		}

		// If the phase is 0.0 then the character should be at their initial offset only, so no need to do this work...
		if (phase != 0.0f)
		{
			//	fix up the mover position on the dynamic entity
			Matrix34 deltaMatrix(M34_IDENTITY);

			if(pScene->m_Absolute)
			{
				//Apply the fix up as absolute and not a delta
				deltaMatrix.d = fwAnimHelpers::GetMoverTrackTranslation(*pClip, phase);

				Quaternion qRot(Quaternion::sm_I);
				qRot = fwAnimHelpers::GetMoverTrackRotation(*pClip, phase);

				deltaMatrix.FromQuaternion(qRot);
			}
			else
			{
				// Get the calculated mover track offset from phase 0.0 to the new phase
				fwAnimHelpers::GetMoverTrackMatrixDelta(*pClip, 0.0f, phase, deltaMatrix);
			}

			// Transform by the mover offset
			outMat.DotFromLeft(deltaMatrix);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void fwAnimDirectorComponentSyncedScene::RegisterWithSyncedScene(fwSyncedSceneId sceneId, fwEntity* pEntity)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to RegisterWithSyncedScene", (s32)sceneId))
	{
		// don't want duplicates
		fwRegdRef<fwEntity> newEnt(pEntity);

		pScene->m_AttachedDynamicEntities.DeleteMatches(newEnt);
		pScene->m_AttachedDynamicEntities.PushAndGrow(newEnt);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::RemoveFromSyncedScene(fwSyncedSceneId sceneId, fwEntity* pEntity)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to RemoveFromSyncedScene", (s32)sceneId))
	{
		fwRegdRef<fwEntity> newEnt(pEntity);

		pScene->m_AttachedDynamicEntities.DeleteMatches(newEnt);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::RegisterCameraWithSyncedScene(fwSyncedSceneId sceneId, void* pCam, const crClip* pCamClip)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to RegisterCameraWithSyncedScene", (s32)sceneId))
	{
		pScene->m_AttachedCameras.PushAndGrow(SyncedScene::CamData(pCam, pCamClip));
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::RemoveCameraFromSyncedScene(fwSyncedSceneId sceneId, void* pCam)
{
	SyncedScene *pScene = GetSyncedScene(sceneId);
	if(animVerifyf(pScene, "Invalid sceneId %i, passed to RemoveCameraFromSyncedScene", (s32)sceneId))
	{
		for(int i = 0; i < pScene->m_AttachedCameras.GetCount(); i ++)
		{
			if(pScene->m_AttachedCameras[i].GetCam() == pCam)
			{
				pScene->m_AttachedCameras[i].Clear();

				pScene->m_AttachedCameras.Delete(i);

				i --;
			}
		}
	}
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

#if __BANK
void AppendInt(atString& string, s32 value)
{
	char buffer[16];
	sprintf(buffer, "%d", value);
	string+= buffer;
}

////////////////////////////////////////////////////////////////////////////////

void AppendInt(atString& string, u32 value)
{
	char buffer[16];
	sprintf(buffer, "%u", value);
	string+= buffer;
}

////////////////////////////////////////////////////////////////////////////////

void AppendFloat(atString& string, float value)
{
	char buffer[16];
	sprintf(buffer, "%.4f", value);
	string+= buffer;
}

////////////////////////////////////////////////////////////////////////////////

void AppendVector(atString& string, Vector3& value)
{
	char buffer[64];
	sprintf(buffer, "x: %.4f, y:%.4f, z:%.4f", value.x, value.y, value.z);
	string+= buffer;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::DumpSynchronizedSceneDebugInfo(atString& string)
{
	// Number of active scenes
	int iNumActiveScenes = 0;
	for(int k = 0; k < sm_SynchronizedScenes.GetCount(); k++)
	{
		SyncedScene& syncedSceneInst = sm_SynchronizedScenes[k];
		if (syncedSceneInst.m_Active)
		{
			iNumActiveScenes++;
		}
	}
	AppendInt(string, iNumActiveScenes);
	string += " Active Synchronized Scenes (Max ";

	// Synced Scene pool size
	AppendInt(string, sm_SynchronizedScenes.GetCount());
	string += "):\n";
	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
		fwSyncedSceneId sceneId = INVALID_SYNCED_SCENE_ID;
		for(atMap<fwSyncedSceneId, int>::Iterator It = sm_SynchronizedSceneIdMap.CreateIterator(); !It.AtEnd(); It.Next())
		{
			if(It.GetData() == i)
			{
				sceneId = It.GetKey();

				break;
			}
		}

		SyncedScene& syncedScene = sm_SynchronizedScenes[i];
		if(sceneId != INVALID_SYNCED_SCENE_ID || syncedScene.m_Refs > 0)
		{
			string += "Scene index="; AppendInt(string, i); string += " id="; AppendInt(string, sceneId); string += ": ";
			string += syncedScene.m_Active ? "Active" : "Inactive"; string +=", ";
			string += syncedScene.m_Paused ? "Paused" : "Playing"; string +=", ";
			string += syncedScene.m_Looped ? "Looped" : "Not looped"; string +=", ";
			string += syncedScene.m_HoldLastFrame ? "HoldLastFrame" : "Not HoldLastFrame"; string +=", ";
			string += syncedScene.m_Abort ? "Abort" : "Not Abort"; string +=", ";
			string += syncedScene.m_Absolute ? "Absolute" : "Not Absolute"; string +=", ";
			string += "time: "; AppendFloat(string, syncedScene.m_Time); string +=", ";
			string += "rate: "; AppendFloat(string, syncedScene.m_Rate); string +=", ";
			string += "duration: "; AppendFloat(string, syncedScene.m_Duration); string +=", ";
			string += "script thread: "; AppendInt(string, syncedScene.m_ScriptThreadId); string +=", \n";
			if (syncedScene.m_AttachParent)
			{
				string += "Attached to ";
				string += syncedScene.m_AttachParent->GetModelName();
				string += ", boneIdx: "; AppendInt(string, syncedScene.m_AttachBoneIndex);
				string += "\n";	

				Matrix34 globalMatrix;
				globalMatrix.FromQuaternion(syncedScene.m_SceneOrientation);
				globalMatrix.d = syncedScene.m_ScenePosition;

				if(syncedScene.m_AttachBoneIndex==-1)
				{
					Matrix34 parentMatrix(MAT34V_TO_MATRIX34(syncedScene.m_AttachParent->GetMatrix()));
					globalMatrix.Dot(parentMatrix);
				}
				else
				{
					Matrix34 parentMatrix(M34_IDENTITY);
					syncedScene.m_AttachParent->GetGlobalMtx(syncedScene.m_AttachBoneIndex, parentMatrix);
					globalMatrix.Dot(parentMatrix);
				}
				
				string += "origin pos: "; AppendVector(string, globalMatrix.d); string +=", ";

				Vector3 eulers;
				globalMatrix.ToEulersXYZ(eulers);
				string += "origin rot: "; AppendVector(string, eulers); string +=", ";
			}
			else
			{
				string += "Not attached\n";
				string += "origin pos: "; AppendVector(string, syncedScene.m_ScenePosition); string +=", ";
				Vector3 eulers;
				syncedScene.m_SceneOrientation.ToEulers(eulers);
				string += "origin rot: "; AppendVector(string, eulers); string +=", ";
			}

			AppendInt (string, syncedScene.m_Refs); string += " references\n";

			if(g_pSyncedSceneAudioInterface)
			{
				if(syncedScene.m_AudioHash.IsNotNull())
				{
					string += "\tAudioEvent: "; string += syncedScene.m_AudioHash.GetCStr();
					u32 uDurationMs = g_pSyncedSceneAudioInterface->GetDurationMs(syncedScene.m_AudioHash);
					if(uDurationMs != (u32)-1)
					{
						string += " Duration: "; AppendFloat(string, uDurationMs / 1000.0f);
					}
					u32 uElapsedTimeMs = g_pSyncedSceneAudioInterface->GetElapsedTimeMs(syncedScene.m_AudioHash);
					if(uElapsedTimeMs != (u32)-1)
					{
						string += " ElapsedTime: "; AppendFloat(string, uElapsedTimeMs / 1000.0f);
					}
					string += "\n";
				}
			}
#if __BANK
			string += "	"; AppendInt(string, syncedScene.m_AttachedDynamicEntities.GetCount()); string += " entities:\n";
			for (u32 j=0; j<((u32)syncedScene.m_AttachedDynamicEntities.GetCount()); j++)
			{
				fwEntity* pEntity = syncedScene.m_AttachedDynamicEntities[j];

				if (pEntity)
				{
					string +="	"; string += pEntity->GetModelName(); string += ": ";

					fwAnimDirector *pAnimDirector = pEntity->GetAnimDirector();
					if (pAnimDirector)
					{
						fwAnimDirectorComponentSyncedScene *pAnimDirectorComponentSyncedScene = pAnimDirector->GetComponent<fwAnimDirectorComponentSyncedScene>();
						if (pAnimDirectorComponentSyncedScene)
						{
							const crClip *pClip = pAnimDirectorComponentSyncedScene->m_SyncedInstance.m_Clip;
							if(pClip)
							{
								const char *szClipDictionaryName = NULL;
								for(int clipDictionaryIndex = 0; clipDictionaryIndex < g_ClipDictionaryStore.GetSize(); clipDictionaryIndex ++)
								{
									if(g_ClipDictionaryStore.IsValidSlot(strLocalIndex(clipDictionaryIndex)))
									{
										crClipDictionary *pClipDictionary = g_ClipDictionaryStore.Get(strLocalIndex(clipDictionaryIndex));
										if(pClipDictionary == pClip->GetDictionary())
										{
											szClipDictionaryName = g_ClipDictionaryStore.GetName(strLocalIndex(clipDictionaryIndex));

											break;
										}
									}
								}

								string += szClipDictionaryName ? szClipDictionaryName : "(null)";

								string += " ";

								string += pClip->GetName();

								float phase = pClip->ConvertTimeToPhase(syncedScene.m_Time);
								string += " phase: "; AppendFloat(string, phase);

								string += " duration: "; AppendFloat(string, pClip->GetDuration());

								string += " loopWithinScene: "; string += pAnimDirectorComponentSyncedScene->m_SyncedInstance.m_LoopWithinScene ? "true" : "false";
							}
						}
					}

					if (pEntity->IsBaseFlagSet(fwEntity::IS_FIXED))
					{
						string += ", FIXED";
					}
					if (pEntity->IsBaseFlagSet(fwEntity::IS_FIXED_BY_NETWORK))
					{
						string += ", FIXED BY NETWORK";
					}
					if (pEntity->IsProtectedBaseFlagSet(fwEntity::IS_FIXED_UNTIL_COLLISION))
					{
						string += ", FIXED UNTIL COLLISION";
					}

					Vector3 entityPos = VEC3V_TO_VECTOR3(pEntity->GetMatrix().d());
					string += ", pos: "; AppendVector(string, entityPos);

				string +="\n";
			}
				else
				{
					string += "NULL ENTITY!\n";
				}
				
			}
			string += "	"; AppendInt(string, syncedScene.m_AttachedCameras.GetCount()); string += " cameras:\n";
			for (int i = 0; i < syncedScene.m_AttachedCameras.GetCount(); i++)
			{
				const crClip *pClip = syncedScene.m_AttachedCameras[i].GetCamClip();
				if (pClip)
				{
					string += "	";

					const char *szClipDictionaryName = NULL;
					for (int clipDictionaryIndex = 0; clipDictionaryIndex < g_ClipDictionaryStore.GetSize(); clipDictionaryIndex++)
					{
						if (g_ClipDictionaryStore.IsValidSlot(strLocalIndex(clipDictionaryIndex)))
						{
							crClipDictionary *pClipDictionary = g_ClipDictionaryStore.Get(strLocalIndex(clipDictionaryIndex));
							if (pClipDictionary == pClip->GetDictionary())
							{
								szClipDictionaryName = g_ClipDictionaryStore.GetName(strLocalIndex(clipDictionaryIndex));

								break;
							}
						}
					}

					string += szClipDictionaryName ? szClipDictionaryName : "(null)";

					string += " ";

					string += pClip->GetName();

					float phase = pClip->ConvertTimeToPhase(syncedScene.m_Time);
					string += " phase: "; AppendFloat(string, phase);

					string += " duration: "; AppendFloat(string, pClip->GetDuration());

					string += "\n";
				}
			}
#endif // __BANK

			string += "\n";
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::RenderSynchronizedSceneDebug()
{
	for(int i = 0; i < sm_SynchronizedScenes.GetCount(); i ++)
	{
	}
}
#endif //__BANK

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::PrepareSynchronizedSceneAudioEvent(const char *audioEvent)
{
	if(animVerifyf(g_pSyncedSceneAudioInterface, "Synced scene audio interface does not exist!"))
	{
		if(g_pSyncedSceneAudioInterface->Prepare(audioEvent))
		{
			atHashString audioHash(audioEvent);

			audioHash.Clear();

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::PrepareSynchronizedSceneAudioEvent(fwSyncedSceneId sceneId, const char *audioEvent)
{
	if(animVerifyf(IsValidSceneId(sceneId), "Synced scene id (%u) is invalid!", sceneId))
	{
		SyncedScene *pSyncedScene = GetSyncedScene(sceneId);
		if(animVerifyf(pSyncedScene, "Synced scene (%u) does not exist!", sceneId))
		{
			if(animVerifyf(g_pSyncedSceneAudioInterface, "Synced scene audio interface does not exist!"))
			{
				if(g_pSyncedSceneAudioInterface->Prepare(audioEvent))
				{
					pSyncedScene->m_AudioHash = audioEvent;

					return true;
				}
			}
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::PlaySynchronizedSceneAudioEvent(fwSyncedSceneId sceneId)
{
	if(animVerifyf(IsValidSceneId(sceneId), "Synced scene id (%u) is invalid!", sceneId))
	{
		SyncedScene *pSyncedScene = GetSyncedScene(sceneId);
		if(animVerifyf(pSyncedScene, "Synced scene (%u) does not exist!", sceneId))
		{
			return pSyncedScene->PlayAudioEvent();
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::StopSynchronizedSceneAudioEvent(fwSyncedSceneId sceneId)
{
	if(animVerifyf(IsValidSceneId(sceneId), "Synced scene id (%u) is invalid!", sceneId))
	{
		SyncedScene *pSyncedScene = GetSyncedScene(sceneId);
		if(animVerifyf(pSyncedScene, "Synced scene (%u) does not exist!", sceneId))
		{
			return pSyncedScene->StopAudioEvent();
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentSyncedScene::SyncedInstance::SyncedInstance()
: m_Clip(NULL)
, m_Network(NULL)
, m_SceneId(-1)
, m_DictionaryIndex(-1)
, m_BlockMoverUpdate(false)
, m_ExitOnSceneEnd(true)
, m_LoopWithinScene(false)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentSyncedScene::SyncedScene::SyncedScene()
: m_SceneOrientation(Quaternion::IdentityType)
, m_ScenePosition(Vector3::ZeroType)
, m_Time(0.0f)
, m_PrevTime(0.0f)
, m_Duration(0.0f)
, m_Rate(1.0f)
, m_Paused(false)
, m_Absolute(false)
, m_Active(false)
, m_Looped(false)
, m_HoldLastFrame(true)
, m_Abort(false)
, m_pDestroySceneCallback(NULL)
, m_pDestroySceneCallbackParam(NULL)
, m_Refs(0)
, m_AttachParent(NULL)
, m_AttachBoneIndex(-1)
, m_ScriptThreadId(THREAD_INVALID)
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SyncedScene::Update(float deltaTime)
{
	if (m_Active)
	{
		m_PrevTime = m_Time;

		if (!m_Paused)
		{
			if (m_Duration > 0.0f)
			{
				if(g_pSyncedSceneAudioInterface && m_AudioHash.IsNotNull())
				{
					u32 elapsedTimeMs = g_pSyncedSceneAudioInterface->GetElapsedTimeMs(m_AudioHash.GetHash());
					if(elapsedTimeMs != (u32)-1)
					{
						m_Time = elapsedTimeMs / 1000.0f;

						m_Time = Clamp<float>(m_Time, 0.0f, m_Duration);
					}
				}
				else
				{
					m_Time += (deltaTime * m_Rate);

					if (m_Looped)
					{
						while (m_Time > m_Duration)
						{
							m_Time -= m_Duration;
						}
						while (m_Time < 0.0f)
						{
							m_Time += m_Duration;
						}
					}
					else
					{
						m_Time = Clamp<float>(m_Time, 0.0f, m_Duration);
					}
				}

				if(m_Time >= m_Duration || m_Abort)
				{
					StopAudioEvent();
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SyncedScene::Reset()
{
	m_SceneOrientation = Quaternion::sm_I;
	m_ScenePosition = VEC3_ZERO;
	m_Time = 0.0f;
	m_PrevTime = 0.0f;
	m_Duration = 0.0f;
	m_Rate = 1.0f;
	if(m_AudioHash.IsNotNull())
	{
		StopAudioEvent();
	}
	m_Paused = false;
	m_Absolute = false;
	m_Active = false;
	m_Looped = false;
	m_HoldLastFrame = true;
	m_Abort = false;
	m_pDestroySceneCallback = NULL;
	m_pDestroySceneCallbackParam = NULL;
	m_Refs = 0;
	m_AttachParent = NULL;
	m_AttachBoneIndex = -1;
	m_ScriptThreadId = THREAD_INVALID;

#if __BANK
	m_AttachedDynamicEntities.clear();
	m_AttachedCameras.clear();
#endif //__BANK
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SyncedScene::GetRootPosition(Vector3& outPosition)
{
	outPosition = m_ScenePosition;

	if (m_AttachParent)
	{
		Matrix34 parentMatrix(M34_IDENTITY);

		if (m_AttachBoneIndex>-1)
		{
			m_AttachParent->GetGlobalMtx(m_AttachBoneIndex, parentMatrix);
		}
		else
		{
			parentMatrix = MAT34V_TO_MATRIX34(m_AttachParent->GetMatrix());
		}

		parentMatrix.Transform(outPosition);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SyncedScene::GetRootOrientation(Quaternion& outRotation)
{
	outRotation = m_SceneOrientation;

	if (m_AttachParent)
	{
		Matrix34 parentMatrix(M34_IDENTITY);

		if (m_AttachBoneIndex>-1)
		{
			m_AttachParent->GetGlobalMtx(m_AttachBoneIndex, parentMatrix);
		}
		else
		{
			parentMatrix = MAT34V_TO_MATRIX34(m_AttachParent->GetMatrix());
		}

		Quaternion parentRot;
		parentMatrix.ToQuaternion(parentRot);

		outRotation.Multiply(parentRot);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentSyncedScene::SyncedScene::GetRootMatrix(Matrix34& outMatrix)
{
	outMatrix.d = m_ScenePosition;
	outMatrix.FromQuaternion(m_SceneOrientation);

	if (m_AttachParent)
	{
		Matrix34 parentMatrix(M34_IDENTITY);

		if (m_AttachBoneIndex>-1 && m_AttachParent->GetSkeleton())
		{
			if (m_AttachBoneIndex < (int)m_AttachParent->GetSkeleton()->GetBoneCount())
			{
				m_AttachParent->GetGlobalMtx(m_AttachBoneIndex, parentMatrix);
			}
			else
			{
				animAssertf(0, "A synchronized sceneId is attached with an invalid bone index %d!", m_AttachBoneIndex);
			}
		}
		else
		{
			parentMatrix = MAT34V_TO_MATRIX34(m_AttachParent->GetMatrix());
		}

		outMatrix.Dot(parentMatrix);
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::SyncedScene::PlayAudioEvent()
{
	if(g_pSyncedSceneAudioInterface)
	{
		u32 audioHash = g_pSyncedSceneAudioInterface->Play(m_AudioHash.GetHash());
		if(audioHash != 0)
		{
			if(m_AudioHash != audioHash)
			{
				m_AudioHash = audioHash;
			}

			u32 audioDurationMs = g_pSyncedSceneAudioInterface->GetDurationMs(m_AudioHash.GetHash());
			if(animVerifyf(audioDurationMs != (u32)-1, ""))
			{
				float audioDuration = audioDurationMs / 1000.0f;
				//animAssertf(IsClose(m_Duration, audioDuration, 0.1f), "Synced scene audio duration (%f) and animation duration (%f) do not match!", audioDuration, m_Duration);
				if(!IsClose(m_Duration, audioDuration, 0.001f))
				{
					animWarningf("Synced scene audio duration (%f) and animation duration (%f) do not match!", audioDuration, m_Duration);
				}
			}

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentSyncedScene::SyncedScene::StopAudioEvent()
{
	if(g_pSyncedSceneAudioInterface)
	{
		if(m_AudioHash.IsNotNull())
		{
			g_pSyncedSceneAudioInterface->Stop(m_AudioHash.GetHash());

			m_AudioHash.Clear();

			return true;
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

fwSyncedSceneId fwAnimDirectorComponentSyncedScene::CreateSyncedSceneId(int sceneIndex)
{
	Assert(g_SyncedSceneMainThreadId == sysIpcGetCurrentThreadId());

	static fwSyncedSceneId sceneId = INVALID_SYNCED_SCENE_ID;

	sceneId ++;

	if(sceneId == INVALID_SYNCED_SCENE_ID)
	{
		sceneId ++;
	}

	int *pSceneIndex = sm_SynchronizedSceneIdMap.Access(sceneId);
	if(animVerifyf(!pSceneIndex, "Invalid sceneId %i, passed to CreateSyncedSceneId", (s32)sceneId))
	{
		sm_SynchronizedSceneIdMap.Insert(sceneId, sceneIndex);
	}

	return sceneId;
}

////////////////////////////////////////////////////////////////////////////////


void fwAnimDirectorComponentSyncedScene::ReleaseSyncedSceneId(fwSyncedSceneId sceneId)
{
	Assert(g_SyncedSceneMainThreadId == sysIpcGetCurrentThreadId());

	int *pSceneIndex = sm_SynchronizedSceneIdMap.Access(sceneId);
	if(animVerifyf(pSceneIndex, "Invalid sceneId %i, passed to ReleaseSyncedSceneId", (s32)sceneId))
	{
		sm_SynchronizedSceneIdMap.Delete(sceneId);
	}
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentSyncedScene::SyncedScene *fwAnimDirectorComponentSyncedScene::GetSyncedScene(fwSyncedSceneId sceneId)
{
	int *pSceneIndex = sm_SynchronizedSceneIdMap.Access(sceneId);
	if(pSceneIndex)
	{
		return &sm_SynchronizedScenes[*pSceneIndex];
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

const fwMvClipId fwAnimDirectorComponentSyncedScene::sm_ClipId("clip",0xE71BD32A);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_DeltaId("delta",0x429BDA1A);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_MoverDeltaId("moverDelta",0x7DD8A9EC);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_PhaseId("phase",0xA27F482B);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_RateId("rate",0x7E68C088);
const fwMvBooleanId fwAnimDirectorComponentSyncedScene::sm_LoopedId("looped",0x204169A7);

const fwMvClipId fwAnimDirectorComponentSyncedScene::sm_FacialClipId("FacialClip",0x2416E454);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_FacialPhaseId("FacialPhase",0x8158623E);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_FacialRateId("FacialRate",0x33019CC7);
const fwMvBooleanId fwAnimDirectorComponentSyncedScene::sm_FacialLoopedId("FacialLooped",0xE4D8E6D9);
const fwMvFloatId fwAnimDirectorComponentSyncedScene::sm_FacialBlendInId("SyncedSceneFacialBlendIn",0x41c6d43f);

atArray<fwAnimDirectorComponentSyncedScene::SyncedScene> fwAnimDirectorComponentSyncedScene::sm_SynchronizedScenes;
atMap<fwSyncedSceneId, int> fwAnimDirectorComponentSyncedScene::sm_SynchronizedSceneIdMap;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage




