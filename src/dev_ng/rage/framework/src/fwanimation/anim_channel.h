// 
// animation/anim_channel.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef INC_ANIM_CHANNEL_H 
#define INC_ANIM_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(anim)

#define animAssert(cond)						RAGE_ASSERT(anim,cond)
#define animAssertf(cond,fmt,...)				RAGE_ASSERTF(anim,cond,fmt,##__VA_ARGS__)
#define animFatalAssertf(cond,fmt,...)			RAGE_FATALASSERTF(anim,cond,fmt,##__VA_ARGS__)
#define animVerifyf(cond,fmt,...)				RAGE_VERIFYF(anim,cond,fmt,##__VA_ARGS__)
#define animErrorf(fmt,...)						RAGE_ERRORF(anim,fmt,##__VA_ARGS__)
#define animWarningf(fmt,...)					RAGE_WARNINGF(anim,fmt,##__VA_ARGS__)
#define animDisplayf(fmt,...)					RAGE_DISPLAYF(anim,fmt,##__VA_ARGS__)
#define animDebugf1(fmt,...)					RAGE_DEBUGF1(anim,fmt,##__VA_ARGS__)
#define animDebugf2(fmt,...)					RAGE_DEBUGF2(anim,fmt,##__VA_ARGS__)
#define animDebugf3(fmt,...)					RAGE_DEBUGF3(anim,fmt,##__VA_ARGS__)
#define animLogf(severity,fmt,...)				RAGE_LOGF(anim,severity,fmt,##__VA_ARGS__)
#define animCondLogf(cond,severity,fmt,...)		RAGE_CONDLOGF(cond,anim,severity,fmt,##__VA_ARGS__)

#define ENABLE_SEMAPHORE_CHECK (0)
#if ENABLE_SEMAPHORE_CHECK
#define animFastAssert(cond)					FastAssert(cond)
#else // ENABLE_SEMAPHORE_CHECK
#define animFastAssert(cond)					RAGE_ASSERT(anim,cond)
#endif // ENABLE_SEMAPHORE_CHECK

#endif // INC_ANIM_CHANNEL_H 
