#include "AnimManager.h"

// Rage headers
#include "cranimation/animation.h"
#include "creature/creature.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"
#include "creature/componentextradofs.h"
#include "crmotiontree/nodeexpression.h"
#include "crmotiontree/nodepm.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tagiterators.h"
#include "crparameterizedmotion/parameterizedmotiondictionary.h"
#include "file/asset.h"
#include "fwanimation/anim_channel.h"
#include "fwanimation/animdefines.h"
#include "fwanimation/clipsets.h"
#include "fwanimation/directorcomponentcreature.h"
#include "fwscene/stores/blendshapestore.h"
#include "fwscene/stores/expressionsdictionarystore.h"
#include "fwsys/fileexts.h"
#include "fwsys/gameskeleton.h"
#include "paging/rscbuilder.h"
#include "vector/quaternion.h"
#include "vectormath/legacyconvert.h"


RAGE_DEFINE_CHANNEL(anim)

namespace rage {


	CompileTimeAssert(sizeof(fwMvClipSetId) == 4);
	CompileTimeAssert(sizeof(fwMvClipId) == 4);


fwAnimManager *g_AnimManager;

	
// --- fwAnimManager --------------------------------------------------------------------------------------

void fwAnimManager::InitClass()
{
	// Note hat any override of this function will most likely not call this
	// base implementation, so don't put anything into this function other than
	// the instantiation of the class.
	Assert(!g_AnimManager);
	g_AnimManager = rage_new fwAnimManager();
}

void fwAnimManager::ShutdownClass()
{
	delete g_AnimManager;
	g_AnimManager = NULL;
}

//
// name:		fwAnimManager::Init
// description:	Initialise anim manager
void fwAnimManager::InitCore()
{
}

void fwAnimManager::InitBeforeMap()
{
	g_ClipDictionaryStore.Shutdown();
}

void fwAnimManager::ShutdownCore()
{
    // Worth setting to true occasionally to see if other processes are 
    // tidying up references correctly
    bool bIgnoreRefCount = true;
    g_ClipDictionaryStore.RemoveAll(bIgnoreRefCount);
    g_ClipDictionaryStore.Shutdown();
}

void fwAnimManager::ShutdownSession()
{
    g_ClipDictionaryStore.RemoveAll();
}

//////////////////////////////////////////////////////////////////////////

const crClip* fwAnimManager::GetClipIfExistsBySetId(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId)
{
	const crClip* pClip = NULL;

	if((clipSetId == CLIP_SET_ID_INVALID) || (clipId == CLIP_ID_INVALID))
	{
		return NULL;
	}

	fwClipSet* pClipSet = fwClipSetManager::GetClipSet(clipSetId);
	if(pClipSet)
	{
		pClip = pClipSet->GetClip(clipId);
	}

	return pClip; 
}

//////////////////////////////////////////////////////////////////////////

const crClip* fwAnimManager::GetClipIfExistsByName(const char* clipDictName, const char* pClipName)
{
	if ((pClipName==NULL) || (clipDictName == NULL))
	{
		return NULL;
	}
	
	strLocalIndex clipDictIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictName));
	if(clipDictIndex.Get() < 0)
	{
		return NULL;
	}

	fwClipDictionaryDef* pClipDict = GetSlot(clipDictIndex);
	if(!(pClipDict && pClipDict->m_pObject))
	{
		return NULL;
	}

	u32 clipHashKey = atStringHash(pClipName);
	const crClip* pClip = pClipDict->FindClipIfExists(clipHashKey);
	return pClip;
}

//////////////////////////////////////////////////////////////////////////

#if !__FINAL
fwClipDictionaryDef* g_pLastFoundDef = NULL;
#endif // !__FINAL

const crClip* fwAnimManager::GetClipIfExistsByDictIndex(s32 clipDictIndex, u32 clipHashKey)
{
	if(clipDictIndex < 0)
	{
		return NULL;
	}

	fwClipDictionaryDef* pClipDict = GetSlot(strLocalIndex(clipDictIndex));
#if !__FINAL
	g_pLastFoundDef = pClipDict;
#endif //!__FINAL

	if(!(pClipDict && pClipDict->m_pObject))
	{
		return NULL;
	}

	const crClip* pClip = pClipDict->FindClipIfExists(clipHashKey);
	return pClip;
}

//////////////////////////////////////////////////////////////////////////

const crClip* fwAnimManager::GetClipInDict(const s32 clipDictIndex, u32 clipIndex)
{
	const crClip* pClip = NULL;
	animAssert(clipDictIndex >= 0);
	crClipDictionary* pClipDict = g_ClipDictionaryStore.Get(strLocalIndex(clipDictIndex));
	animAssertf(pClipDict, "%s:clip dict not loaded", g_ClipDictionaryStore.GetName(strLocalIndex(clipDictIndex)));
	animAssert( clipIndex < pClipDict->GetNumClips() );
	pClip = pClipDict->FindClipByIndex(clipIndex);
	return pClip;
}

//////////////////////////////////////////////////////////////////////////

u32 fwAnimManager::GetAnimHashInDict(const s32 clipDictIndex, u32 clipIndex)
{
	animAssert(clipDictIndex >= 0);
	crClipDictionary* pClipDict = g_ClipDictionaryStore.Get(strLocalIndex(clipDictIndex));
	animAssertf(pClipDict, "%s:clip dict not loaded", g_ClipDictionaryStore.GetName(strLocalIndex(clipDictIndex)));

	animAssert( clipIndex < pClipDict->GetNumClips() );
	u32 clipHashKey = pClipDict->FindKeyByIndex(clipIndex);
	return clipHashKey;
}

//////////////////////////////////////////////////////////////////////////

s32 fwAnimManager::CountAnimsInDict(const s32 animDictIndex )
{
	animAssert(animDictIndex >= 0);
	crClipDictionary* pAnimDict = g_ClipDictionaryStore.Get(strLocalIndex(animDictIndex));
	animAssertf(pAnimDict, "%s:clip dict not loaded", g_ClipDictionaryStore.GetName(strLocalIndex(animDictIndex)));

	s32 animCount = pAnimDict->GetNumClips();
	return animCount;
}

//////////////////////////////////////////////////////////////////////////

s32 fwAnimManager::GetClipDictIndex(const fwMvClipSetId &clipSetId)
{
	return fwClipSetManager::GetClipDictionaryIndex(clipSetId);
}

//////////////////////////////////////////////////////////////////////////

void fwAnimManager::GetLocalTranslation(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Vector3 &translation)
{
	translation.Set(VEC3_ZERO);
	if (pClip && pSkeletonData)
	{
		int iBoneIdx = 0;
		bool validBoneId = pSkeletonData->ConvertBoneIdToIndex(iBoneId, iBoneIdx);
		animAssertf(validBoneId, "Invalid bone id : %d\n", iBoneId);
		if (validBoneId)
		{
			const crBoneData *pBoneData = pSkeletonData->GetBoneData(iBoneIdx);
			animAssertf(pBoneData, "Invalid bone data\n");
			if (pBoneData)
			{
				GetLocalTranslation(pBoneData, pClip, fPhase, translation);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwAnimManager::GetLocalRotation(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Quaternion &rotation)
{
	rotation.Set(Quaternion::sm_I);
	if (pClip && pSkeletonData)
	{
		int iBoneIdx = 0;
		bool validBoneId = pSkeletonData->ConvertBoneIdToIndex(iBoneId, iBoneIdx);
		animAssertf(validBoneId, "Invalid bone id : %d\n", iBoneId);
		if (validBoneId)
		{
			const crBoneData *pBoneData = pSkeletonData->GetBoneData(iBoneIdx);
			animAssertf(pBoneData, "Invalid bone data\n");
			if (pBoneData)
			{
				GetLocalRotation(pBoneData, pClip, fPhase, rotation);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwAnimManager::GetLocalMatrix(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Matrix34 &matrix)
{
	matrix.Set(M34_IDENTITY);
	if (pSkeletonData && pClip)
	{
		int iBoneIdx = 0;
		bool validBoneId = pSkeletonData->ConvertBoneIdToIndex(iBoneId, iBoneIdx);
		animAssertf(validBoneId, "Invalid bone id : %d\n", iBoneId);
		if (validBoneId)
		{
			const crBoneData *pBoneData = pSkeletonData->GetBoneData(iBoneIdx);
			animAssertf(pBoneData, "Invalid bone data\n");
			if (pBoneData)
			{
				Vector3 translation(VEC3_ZERO);
				if (pBoneData->GetBoneId() == 0)
				{
					GetLocalTranslation(pBoneData, pClip, fPhase, translation);
				}
				else
				{
					translation = RCC_VECTOR3(pBoneData->GetDefaultTranslation());
				}
				matrix.d = translation;

				Quaternion rotation(Quaternion::sm_I);
				GetLocalRotation(pBoneData, pClip, fPhase, rotation);
				matrix.FromQuaternion(rotation);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwAnimManager::GetObjectMatrix(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Matrix34& matrix)
{
	matrix.Set(M34_IDENTITY);
	if (pSkeletonData && pClip)
	{
		int iBoneIdx = 0;
		bool validBoneId = pSkeletonData->ConvertBoneIdToIndex(iBoneId, iBoneIdx);
		animAssertf(validBoneId, "Invalid bone id : %d\n", iBoneId);
		if (validBoneId)
		{
			const crBoneData *parentBoneData = pSkeletonData->GetBoneData(iBoneIdx);
			animAssertf(parentBoneData, "Invalid bone data\n");
			if (parentBoneData)
			{
				Vector3 translation(VEC3_ZERO);
				if (parentBoneData->GetBoneId() == 0)
				{
					GetLocalTranslation(parentBoneData, pClip, fPhase, translation);
				}
				else
				{
					translation = RCC_VECTOR3(parentBoneData->GetDefaultTranslation());
				}
				matrix.d = translation;

				Quaternion rotation(Quaternion::sm_I);
				GetLocalRotation(parentBoneData, pClip, fPhase, rotation);
				matrix.FromQuaternion(rotation);


				parentBoneData = parentBoneData->GetParent();
				while(parentBoneData)
				{
					Matrix34 localBoneMat(M34_IDENTITY);

					Vector3 translation(VEC3_ZERO);
					if (parentBoneData->GetBoneId() == 0)
					{
						GetLocalTranslation(parentBoneData, pClip, fPhase, translation);
					}
					else
					{
						translation = RCC_VECTOR3(parentBoneData->GetDefaultTranslation());
					}
					localBoneMat.d = translation;

					Quaternion rotation(Quaternion::sm_I);
					GetLocalRotation(parentBoneData, pClip, fPhase, rotation);
					localBoneMat.FromQuaternion(rotation);

					matrix.Dot(localBoneMat);
					parentBoneData = parentBoneData->GetParent();
				}
			}
		}
	}

}

//////////////////////////////////////////////////////////////////////////

void fwAnimManager::GetLocalTranslation(const crBoneData *pBoneData, const crClip *pClip, float fPhase, Vector3& translation)
{
	translation.Set(VEC3_ZERO);

	animAssertf(pBoneData, "Invalid bone data\n");
	if (pBoneData && pBoneData->HasDofs(crBoneData::TRANSLATION))
	{
		crFrameDataSingleDof frameData(kTrackBoneTranslation, pBoneData->GetBoneId(), kFormatTypeVector3);
		crFrameSingleDof frame(frameData);
		frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());
		pClip->Composite(frame, pClip->ConvertPhaseToTime(fPhase));
		if(!frame.GetValue<Vec3V>(RC_VEC3V(translation)))
		{
			translation = RCC_VECTOR3(pBoneData->GetDefaultTranslation());
		}
	}
	else if (pBoneData)
	{
		translation = RCC_VECTOR3(pBoneData->GetDefaultTranslation());
	}
}

//////////////////////////////////////////////////////////////////////////

void fwAnimManager::GetLocalRotation(const crBoneData *pBoneData, const crClip *pClip, float fPhase, Quaternion& rotation)
{
	rotation.Set(Quaternion::sm_I);
	animAssertf(pBoneData, "Invalid bone data\n");
	if (pBoneData && pBoneData->HasDofs(crBoneData::ROTATION))
	{
		crFrameDataSingleDof frameData(kTrackBoneRotation, pBoneData->GetBoneId(), kFormatTypeQuaternion);
		crFrameSingleDof frame(frameData);
		frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());
		pClip->Composite(frame, pClip->ConvertPhaseToTime(fPhase));
		if(!frame.GetValue<QuatV>(RC_QUATV(rotation)))
		{
			rotation = RCC_QUATERNION(pBoneData->GetDefaultRotation());
		}
	}
	else if (pBoneData)
	{
		rotation = RCC_QUATERNION(pBoneData->GetDefaultRotation());
	}
}

//////////////////////////////////////////////////////////////////////////

int fwAnimManager::GetStreamingModuleId()
{
	return g_ClipDictionaryStore.GetStreamingModuleId();
}

strLocalIndex fwAnimManager::FindSlot(const strStreamingObjectName name)
{
	return g_ClipDictionaryStore.FindSlot(name);
}

strLocalIndex fwAnimManager::FindSlotFromHashKey(const u32 hashKey)
{
	return g_ClipDictionaryStore.FindSlotFromHashKey(hashKey);
}

s32 fwAnimManager::FindHashKeyFromSlot(strLocalIndex index)
{
	return g_ClipDictionaryStore.GetHash(index);
}

bool fwAnimManager::IsValidSlot(strLocalIndex index)
{
	return g_ClipDictionaryStore.IsValidSlot(index);
}

void fwAnimManager::RemoveRefWithoutDelete(strLocalIndex index)
{
	g_ClipDictionaryStore.RemoveRefWithoutDelete(strLocalIndex(index), REF_OTHER);
}

#if !__FINAL
const char* fwAnimManager::GetName(strLocalIndex index)
{
	return g_ClipDictionaryStore.GetName(index);
}
#else
const char* fwAnimManager::GetName(strLocalIndex UNUSED_PARAM(index))
{
	return NULL;
}
#endif // !__FINAL

void fwAnimManager::StreamingRequest(strLocalIndex index, s32 flags)
{
	g_ClipDictionaryStore.StreamingRequest(index, flags);
}

void fwAnimManager::AddRef(strLocalIndex index)
{
	g_ClipDictionaryStore.AddRef(strLocalIndex(index), REF_OTHER);
}

void fwAnimManager::RemoveRef(strLocalIndex index)
{
	g_ClipDictionaryStore.RemoveRef(strLocalIndex(index), REF_OTHER);
}

s32 fwAnimManager::GetNumRefs(strLocalIndex index)
{
	return g_ClipDictionaryStore.GetNumRefs(strLocalIndex(index));
}

s32 fwAnimManager::GetMaxSize()
{
	return g_ClipDictionaryStore.GetMaxSize();
}

s32 fwAnimManager::GetSize()
{
	return g_ClipDictionaryStore.GetSize();
}

s32 fwAnimManager::GetNumUsedSlots()
{
	return g_ClipDictionaryStore.GetNumUsedSlots();
}

crClipDictionary* fwAnimManager::Get(strLocalIndex index)
{
	return g_ClipDictionaryStore.Get(index);
}

fwClipDictionaryDef* fwAnimManager::GetSlot(strLocalIndex index)
{
	return g_ClipDictionaryStore.GetSlot(index);
}

class strStreamingModule* fwAnimManager::GetStreamingModule()
{
	return &g_ClipDictionaryStore;
};

strLocalIndex fwAnimManager::Register(const char* name)
{
	return g_ClipDictionaryStore.Register(name);
}

} // namespace rage
