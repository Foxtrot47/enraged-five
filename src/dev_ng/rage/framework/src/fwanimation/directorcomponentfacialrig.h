// 
// fwanimation/directorcomponentfacialrig.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_FACIALRIG_H
#define FWANIMATION_DIRECTOR_COMPONENT_FACIALRIG_H

#include "directorcomponent.h"

#include "cranimation/frame.h"
#include "cranimation/framebuffers.h"
#include "cranimation/framedatafactory.h"
#include "cranimation/framefilters.h"
#include "crmotiontree/nodeblend.h"
#include "crmotiontree/nodecapture.h"
#include "crmotiontree/nodeclip.h"
#include "crmotiontree/nodeframe.h"
#include "crmotiontree/observer.h"
#include "paging/ref.h"

#define USE_FACIAL_FRAME_DATA (1)

namespace rage
{

class mvParameterBuffer;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that manages facial rigs
class fwAnimDirectorComponentFacialRig : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentFacialRig();

	// PURPOSE: Initializing constructor
	fwAnimDirectorComponentFacialRig(fwAnimDirector& director);

	// PURPOSE: Destructor
	~fwAnimDirectorComponentFacialRig();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentFacialRig);

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);

	// PURPOSE: Implement in derived class to perform class specific initialization
	static void InitDerivedClass();

	// PURPOSE: Get facial frame
	const crFrame& GetFrame() const;
	crFrame& GetFrame();

	// PURPOSE: Get facial filter
	const crFrameFilter& GetFilter() const;
	crFrameFilter& GetFilter();

	// PURPOSE: Get expression root
	const crmtObserver& GetExpressionRoot() const;

#if __BANK
	// PURPOSE: Enable / disable the facial rig
	void SetEnable(bool bEnable) { m_bEnable = bEnable; }

	// PURPOSE: Get whether the facial rig is enabled
	bool GetEnable() const { return m_bEnable; }
#endif


private:

#if USE_FACIAL_FRAME_DATA
	crFrameData* m_FrameData;
	crFrameBufferFrameData m_FrameBuffer;
#endif // USE_FACIAL_FRAME_DATA
	crFrame* m_Frame;

	// PURPOSE: Custom frame filter - blocks all except face bones, ANMs and control values
	class FacialFilter : public crFrameFilter
	{
	public:

		// PURPOSE: Constructor
		FacialFilter();

		// PURPOSE: Stubbed resource constructor
		FacialFilter(datResource&);

		// PURPOSE: Filter type registration
		CR_DECLARE_FRAME_FILTER_TYPE(FacialFilter);

		// PURPOSE: Initializer
		// PARAMS: skelData - provide skeleton data for the character
		void Init(fwAnimDirector& director);

		// PURPOSE: Stubbed serialize call
		virtual void Serialize(datSerialize&); 

		// PURPOSE: Filter DOF override
		virtual bool FilterDof(u8 track, u16 id, float& inoutWeight);

	protected:

		// PURPOSE: Signature calculation
		virtual u32 CalcSignature() const;

	private:

		pgRef<const crSkeletonData> m_SkeletonData;
	};

	FacialFilter m_Filter;


	// PURPOSE: Facial clip and blend node controller
	class FacialNodeController
	{
	public:

		// PURPOSE: Constructor
		FacialNodeController();

		// PURPOSE: Destructor
		~FacialNodeController();

		// PURPOSE: Shutdown
		void Shutdown();

		// PURPOSE: Initializer
		void Init(const crmtObserver& clipObsever, const crmtObserver& blendObserver, u32 clipKey, u32 phaseKey=0, u32 weightKey=0, u32 rateKey=0, u32 loopKey=0, u32 blendInDurationKey=0, u32 blendOutDurationKey=0, u32 blendOutTransitionKey=0, bool bAlwaysSetNewClip = false);

		// PURPOSE: Update facial node
		bool Update(mvParameterBuffer& buffer);


		// PURPOSE: Set clip on node
		bool SetClip(const crClip* clip);

		// PURPOSE: Set clip phase on node
		bool SetPhase(float phase);

		// PURPOSE: Set blend weight on node
		bool SetWeight(float weight);

		// PURPOSE: Set clip rate on node
		bool SetRate(float rate);

		// PURPOSE: Set looping on node
		bool SetLooping(bool looping);

		// PURPOSE: Set blend in duration on node
		void SetBlendInDuration(float duration);

		// PURPOSE: Set blend out duration on node
		void SetBlendOutDuration(float duration);

		// PURPOSE: Get blend weight on node
		bool GetWeight(float& weightOut);

	private:

		crmtObserverTyped<crmtNodeClip> m_ClipObserver;
		crmtObserverTyped<crmtNodeBlend> m_BlendObserver;

		u32 m_ClipKey;
		u32 m_PhaseKey;
		u32 m_WeightKey;
		u32 m_RateKey;
		u32 m_LoopKey;
		u32 m_BlendInDurationKey;
		u32 m_BlendOutDurationKey;
		u32 m_BlendOutTransitionKey;

		bool m_ManageWeight;
		bool m_DoBlendOutTransition;

		bool m_AlwaysSetNewClip;

		float m_BlendInRate;
		float m_BlendOutRate;

		static const float sm_DefaultBlendInOutRate;
	};

	FacialNodeController m_IdleNodeCtrl;
	FacialNodeController m_OneShotNodeCtrl;
	FacialNodeController m_VisemeNodeCtrl;
	FacialNodeController m_SyncedSceneNodeCtrl;
	FacialNodeController m_MouthNodeCtrl;

	crmtObserver m_ExpressionRoot;

	crmtObserverTyped<crmtNodeFrame> m_MidPhysicsFrame;
	crmtObserverTyped<crmtNodeCapture> m_MidPhysicsCapture;
	crmtObserverTyped<crmtNodeFrame> m_PreRenderFrame;

	atArray<crFrame*> m_GarbageFrames;

	bool m_MouthClipInit;

	// Voice driven mouth movement variables
	float m_fDesiredVoiceDrivenMouthMovementWeight;
	float m_fVoiceDrivenMouthMovementBlendRate;
	float m_fVoiceLoudness;
	s32 m_iVoiceStartTimeMs;

#if __BANK
	bool m_bEnable;
#endif

public:

	// PURPOSE: Clear off the synced scene facial anim
	void ClearSyncedSceneFacialAnim(float fBlendOutTime);

	// PURPOSE: Start the voice driven mouth movement.
	void SetVoiceDrivenMouthMovementBlend(float fDesiredWeight, float fBlendDuration);

	// PURPOSE: Sets the voice loudness
	void SetVoiceLoudness(float fVoiceLoudness) { m_fVoiceLoudness = fVoiceLoudness; }

	// PURPOSE: Gets the voice loudness
	float GetVoiceLoudness() const { return m_fVoiceLoudness; }

	// PURPOSE: Is the voice currently active?
	bool IsVoiceActive() const { return m_iVoiceStartTimeMs != -1; }

	// PURPOSE: Sets the voice start time
	void SetVoiceStartTimeMs(s32 iVoiceStartTimeMs) { m_iVoiceStartTimeMs = iVoiceStartTimeMs; }

	// PURPOSE: Gets the voice start time
	s32 GetVoiceStartTimeMs() const { return m_iVoiceStartTimeMs; }

	// PURPOSE: Immediately blend out all facial animations.
	void RemoveAllNonIdleFacialAnims();

private:

	// PURPOSE: Updates the voice driven mouth movement
	void UpdateVoiceDrivenMouthMovement();

	// PURPOSE: Collect any garbage frames
	void RemoveGarbageFrames();

	static const fwMvFrameId sm_CaptureFrameId;
};

////////////////////////////////////////////////////////////////////////////////

inline const crFrame& fwAnimDirectorComponentFacialRig::GetFrame() const
{
	return *m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame& fwAnimDirectorComponentFacialRig::GetFrame()
{
	return *m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameFilter& fwAnimDirectorComponentFacialRig::GetFilter() const
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameFilter& fwAnimDirectorComponentFacialRig::GetFilter()
{
	return m_Filter;
}

////////////////////////////////////////////////////////////////////////////////

inline const crmtObserver& fwAnimDirectorComponentFacialRig::GetExpressionRoot() const
{
	return m_ExpressionRoot;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_FACIALRIG_H

