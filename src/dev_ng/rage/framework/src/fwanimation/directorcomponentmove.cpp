// 
// fwanimation/directorcomponentmove.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponentmove.h"

#include "animdirector.h"
#include "directorcomponentmotiontree.h"
#include "move.h"

#include "entity/entity.h"

#if __ASSERT
#include "fwsys/timer.h"
#endif // __ASSERT

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentMove::fwAnimDirectorComponentMove()
: fwAnimDirectorComponent(kComponentTypeMove)
, m_Move(NULL)
, m_NetworkPlayers(NULL)
, m_NumNetworkPlayersAddedSinceLastDelete(0)
, m_OutputBuffersSwapped(false)
, m_PostInit(false)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentMove::~fwAnimDirectorComponentMove()
{
	fwAnimDirectorComponentMove::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentMove, 0xc79c63d2, kComponentTypeMove, ANIMDIRECTOR_POOL_MAX, 0.35f);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::Init(fwAnimDirector& director)
{
	InitBase(director);

	fwEntity& entity = GetDirector().GetEntity();

	// create the appropriate move type for the entity type
	m_Move = entity.CreateMoveObject();

	const fwMvNetworkDefId &defId = entity.GetAnimNetworkMoveInfo();
	fwAnimDirector::ForceLoadNetworkDef(defId);

	const mvNetworkDef& def = fwAnimDirector::GetNetworkDef(defId);

	m_Move->Init(entity, def, GetDirector());  

	m_PostInit = true;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::Shutdown()
{
//	Assert(!m_NetworkPlayers && !m_Move);
	// release any network players that might still be hanging around
	fwMoveNetworkPlayer* pPlayer = m_NetworkPlayers;
	while(pPlayer)
	{
		fwMoveNetworkPlayer* pRemove = pPlayer;
		pPlayer = pPlayer->GetNextPlayer();
		pRemove->Release();		
	}
	m_NetworkPlayers = NULL;

	if(m_Move)
	{
		delete m_Move;
		m_Move = NULL;
	}

	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

u32 fwAnimDirectorComponentMove::GetPriority() const 
{
	return 50;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::ReceiveMessage(eMessageId msgId, ePhase, void* payload)
{
	switch(msgId)
	{
	case kMessageUpdate:
		{
			// This is used to ensure we don't swap our move output parameter buffer more than once per frame
			// (since individual tasks can ask to block on the clip update early and flip the buffer)
			m_OutputBuffersSwapped = false;

			// update move
			const float& deltaTime = *(float*)(payload);
			GetMove().Update(deltaTime);

			// swap input buffers, all signals after this call go to subsequent update
			GetMove().SwapInputBuffers();

			// do the same for the network players
			fwMoveNetworkPlayer* player = GetNetworkPlayers();
			while(player)
			{
				player->SwapInputBuffers();
				player = player->GetNextPlayer();
			}

			// post update move, following input buffer swap
			GetMove().PostUpdate();
		}
		break;

	case kMessageMotionTreePreDelegate:
		{
			if(m_PostInit)
			{
				// temporarily transfer contents of command buffer, so buffer is empty for PostInit() call
				mvParameterBuffer parameterBuf;
				parameterBuf.Append(GetMove().GetInputBuffers().GetNextBuffer().GetInternalBuffer());

				GetMove().PostInit();

				// transfer command buffer contents back, so buffer contains correct signals for update
				GetMove().GetInputBuffers().GetNextBuffer().Append(parameterBuf);

				m_PostInit = false;
			}

			GetMove().PreMotionTreeUpdate();

			fwMoveNetworkPlayer* player = GetNetworkPlayers();
			while(player)
			{
				player->PreMotionTreeUpdate();
				player = player->GetNextPlayer();
			}
		}
		break;

	case kMessageMotionTreeMidDelegate:
		{
			GetMove().PostMotionTreeUpdate();

			fwMoveNetworkPlayer* player = GetNetworkPlayers();
			while(player)
			{
				player->PostMotionTreeUpdate();
				player = player->GetNextPlayer();
			}
		}
		break;

	case kMessageShuttingDown:
		{
			if(m_Move)
			{
				delete m_Move;
				m_Move = NULL;
			}

			// Free subnetworks first. This takes into account subnetworks in subnetworks
			fwMoveNetworkPlayer* pPlayer = m_NetworkPlayers;
			while (pPlayer)
			{
				pPlayer->m_SubNetwork->Release();
				pPlayer->m_SubNetwork = NULL;
				pPlayer = pPlayer->GetNextPlayer();
			}

			pPlayer = m_NetworkPlayers;
			while(pPlayer)
			{
				fwMoveNetworkPlayer* pRemove = pPlayer;
				pPlayer = pPlayer->GetNextPlayer();
				pRemove->Release();		
			}
			m_NetworkPlayers = NULL;
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentMove::HasNetworkPlayer(const fwMoveNetworkPlayer* player) const
{
	const fwMoveNetworkPlayer* curr = m_NetworkPlayers;
	while(curr != NULL)
	{
		if(player == curr)
		{
			return true;
		}
		curr = curr->GetNextPlayer();
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

fwMoveNetworkPlayer* fwAnimDirectorComponentMove::CreateNetworkPlayer(const mvNetworkDef& def)
{
	fwMoveNetworkPlayer* player = rage_new fwMoveNetworkPlayer(GetDirector(), def);
	player->AddRef();

	// registers this network player with the update list
	player->SetNextPlayer(m_NetworkPlayers);
	m_NetworkPlayers = player;

	m_NumNetworkPlayersAddedSinceLastDelete++;

#if __ASSERT
	static const u32 s_MaxPlayersSinceRemove = 20; 
	static const u32 s_MaxPlayersCreatedThisFrame = 6;
	static bool s_PrintOnce = true;

	if (m_NumNetworkPlayersAddedSinceLastDelete >= s_MaxPlayersSinceRemove)
	{
		// Keep track of how many of our current players were created on this frame.
		// If there's a handful, then some AI code has likely gone wrong and created too many at once.
		u32 uNumPlayersCreatedThisFrame = 0;

		if (s_PrintOnce)
		{
			animDisplayf("Entity %s (Model: %s) creating new network: %s", GetDirector().GetEntity().GetDebugName(), GetDirector().GetEntity().GetModelName(), def.GetDebugName(NULL, 0));
			animDisplayf("Current frame: %u, Last removed network players on frame: %u", fwTimer::GetFrameCount(), GetDirector().GetLastDeleteUnusedNetworkPlayersFrame());
		}

		s32 i = 0;
		fwMoveNetworkPlayer* player = GetNetworkPlayers();
		while(player)
		{
			// Created on this frame?
			if (player->GetCreationFrame() == fwTimer::GetFrameCount())
			{
				uNumPlayersCreatedThisFrame++;
			}

			if (s_PrintOnce && player->GetSubNetwork() && player->GetSubNetwork()->GetDefinition())
			{
				animDisplayf("Network %d: %s. Creation frame: %u, Refs: %u", i, player->GetSubNetwork()->GetDefinition()->GetDebugName(NULL, 0), player->GetCreationFrame(), player->GetNumRefs());
			}

			i++;
			player = player->GetNextPlayer();
		}
		s_PrintOnce = false;

		// Decide what went wrong - Excessive players created in the same frame (likely AI code issue), or too many players without a cleanup (aka without an anim update).
		if (uNumPlayersCreatedThisFrame >= s_MaxPlayersCreatedThisFrame)
		{
			animAssertf(false, "%d MoVE network players created since last clean up! %u of them this frame, likely AI code issue causing too many network players to be created at the same time.  Assign bug to Default Code AI and attach logs.", m_NumNetworkPlayersAddedSinceLastDelete, uNumPlayersCreatedThisFrame);
		}
		else
		{
			animAssertf(false, "%d MoVE network players created since last clean up! Last clean up/anim update was on frame %u, current frame: %u", m_NumNetworkPlayersAddedSinceLastDelete, GetDirector().GetLastDeleteUnusedNetworkPlayersFrame(), fwTimer::GetFrameCount());
		}
	}
#endif //__DEV

	return player;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::DeleteUnusedNetworkPlayers()
{
	// don't ever do this when the motion tree thread is updating!
	animFastAssert(!GetDirector().IsMotionTreeLockedByPhase(GetPhase()));

	// Search the list of network players and release any players
	// with no refs on them or their mvSubNetwork.
	fwMoveNetworkPlayer** ppsn = &m_NetworkPlayers;
	while(*ppsn)
	{
		fwMoveNetworkPlayer* sn = *ppsn;
		if(sn->GetNumRefs()==1 && sn->m_SubNetwork->GetNumRefs()==1)
		{
			*ppsn = sn->GetNextPlayer();
			sn->Release();
			continue;
		}
		ppsn = &((*ppsn)->m_NextPlayer);
	}

	m_NumNetworkPlayersAddedSinceLastDelete = 0;
}

////////////////////////////////////////////////////////////////////////////////

#if __BANK
fwMoveNetworkPlayer* fwAnimDirectorComponentMove::FindMoveNetworkPlayer(const mvMotionWeb* pWeb) const
{
	// don't ever do this when the motion tree thread is updating!
	animAssert(!GetDirector().GetComponentByPhase<fwAnimDirectorComponentMotionTree>(GetPhase())->IsLocked());

	// Search the list of network players and return the one with the  
	// subnetwork pointer matching pWeb
	fwMoveNetworkPlayer* pPlayer = m_NetworkPlayers;
	while(pPlayer)
	{
		if(pPlayer->GetSubNetwork()==pWeb)
		{
			return pPlayer;
		}
		pPlayer = pPlayer->GetNextPlayer();
	}

	return NULL;
}
#endif // __BANK

////////////////////////////////////////////////////////////////////////////////

u32 fwAnimDirectorComponentMove::GetNumNetworkPlayersAddedSinceLastDelete() const
{
	return m_NumNetworkPlayersAddedSinceLastDelete;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::UpdateAudioAndTriggeredFacialEvents()
{
	fwEntity& entity = GetDirector().GetEntity();

	m_Move->UpdateAudioAndTriggeredFacialEvents(entity);

	fwMoveNetworkPlayer* player = GetNetworkPlayers();
	while(player)
	{
		player->UpdateAudioAndTriggeredFacialEvents(entity);
		player = player->GetNextPlayer();
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::SwapOutputBuffers()
{
	if(!m_OutputBuffersSwapped)
	{
		GetMove().FinishUpdate();

		fwMoveNetworkPlayer* player = GetNetworkPlayers();
		while(player)
		{
			player->SwapOutputBuffers();
			player->UpdateEventWatch();

			player = player->GetNextPlayer();
		}

		GetMove().SwapOutputBuffers();
		UpdateAudioAndTriggeredFacialEvents();

		GetMove().PostFinishUpdate();
	}
	m_OutputBuffersSwapped = true;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::InitDerivedClass()
{
	const u32 peakNumCommandBuffers = ANIMDIRECTOR_POOL_MAX*8;
	sm_CommandBufferManager.Init(peakNumCommandBuffers);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::ShutdownDerivedClass()
{
	sm_CommandBufferManager.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMove::UpdateDerivedClass()
{
	sm_CommandBufferManager.Update();
}

////////////////////////////////////////////////////////////////////////////////

mvCommandBuffer::Manager fwAnimDirectorComponentMove::sm_CommandBufferManager;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage




