// 
// fwanimation/posematcher.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_POSEMATCHER_H
#define FWANIMATION_POSEMATCHER_H

#include "atl/binmap.h"
#include "crextra/posematcher.h"
#include "fwanimation/animdefines.h"
#if __DEV
#include "vector/color32.h"
#endif // __DEV


namespace rage {

class fwEntity;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Frame work wrapper for the crPoseMatcher class
// Adds project level filtering, sets/ids etc
class fwPoseMatcher
{
public:
	
	struct BaseFilter : public crPoseMatcher::MatchFilter
	{
		// PURPOSE: Default constructor
		BaseFilter();

		virtual bool PostFilter(u64 UNUSED_PARAM(key), float UNUSED_PARAM(time), Mat34V_In UNUSED_PARAM(transform), float distance);

		float GetBestDistance() { return m_BestDistance; }
		void ResetBestDistance() { m_BestDistance = FLT_MAX; }

#if DEBUG_POSE_MATCHER
		virtual const char* DebugName(u64 key);
#endif // DEBUG_POSE_MATCHER

	private:

		float m_BestDistance;
	};
	
	// PURPOSE:
	struct Filter : public BaseFilter
	{
		// PURPOSE: Default constructor
		Filter();

		// PURPOSE:
		void AddKey(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, float bias=1.0f);

		// PURPOSE:
		u32 GetKeyCount() const;

		// PURPOSE:
		virtual float PreFilter(u64 key, float);

		// PURPOSE:
		void Reset()
		{ 
			m_Keys.Reset();
		}

		// PURPOSE:
		void Clear()
		{ 
			m_Keys.ResetCount();
		}

	private:

		// Only because we might want to put a check in to make sure the data is there
		atBinaryMap<f32, u64> m_Keys;
	};

	// PURPOSE: Default constructor
	fwPoseMatcher();

	// PURPOSE: Destructor
	~fwPoseMatcher();

	// PURPOSE: Initialize
	void Init();

	// PURPOSE: Shutdown
	void Shutdown();

	// PURPOSE: Add a pose matcher
	// PARAMS: poseMatcher - underlying crPoseMatcher
	void AddPoseMatcher(crPoseMatcher& poseMatcher);

	// PURPOSE: 
	bool FindBestMatch(const crSkeleton& skel, fwMvClipSetId &clipSetId, fwMvClipId &clipId, float& time, Matrix34& transform, fwPoseMatcher::BaseFilter* filter);

	// PURPOSE:
	bool FindBestMatch(const crSkeleton& skel, fwMvClipSetId &clipSetId, fwMvClipId &clipId, Matrix34& transform, fwPoseMatcher::BaseFilter* filter);

	// PURPOSE:
	static u64 CalcKey(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId);

	// PURPOSE:
	const crPoseMatcher::MatchSample* FindSample(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, float time = -1.0f);

#if __DEV
	// PURPOSE:
	void DebugRenderPointCloud(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const Matrix34& renderTransform, float boneSize);

	// PURPOSE:
	void Dump();

	// PURPOSE:
	void CheckForDuplicateSamples();

	// PURPOSE:
	static void DebugRenderPointCloud(crpmPointCloud* cloud, const Matrix34& renderTransform, float boneSize, rage::Color32 color);

#endif // __DEV

private:

	atArray<crPoseMatcher*> m_PoseMatcher;

#if __DEV
public:
	static crpmPointCloud s_LastMatchedPose; // used to save the last pose found by FindBestMatch
	static const char* s_LastMatchedPoseName;
#endif // __DEV
};

////////////////////////////////////////////////////////////////////////////////

inline bool fwPoseMatcher::FindBestMatch(const crSkeleton& skel, fwMvClipSetId &clipSetId, fwMvClipId &clipId, Matrix34& transform, fwPoseMatcher::BaseFilter* filter)
{
	float time;
	return FindBestMatch(skel, clipSetId, clipId, time, transform, filter);
}

////////////////////////////////////////////////////////////////////////////////

inline u64 fwPoseMatcher::CalcKey(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId)
{
	return (u64(clipSetId.GetHash())<<32)|clipId;
}

////////////////////////////////////////////////////////////////////////////////

inline fwPoseMatcher::BaseFilter::BaseFilter() :
crPoseMatcher::MatchFilter(),
m_BestDistance(FLT_MAX)
{
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwPoseMatcher::BaseFilter::PostFilter(u64 UNUSED_PARAM(key), float UNUSED_PARAM(time), Mat34V_In UNUSED_PARAM(transform), float distance)
{
	if (distance <= m_BestDistance)
	{
		m_BestDistance = distance;

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

#if DEBUG_POSE_MATCHER
inline const char* fwPoseMatcher::BaseFilter::DebugName(u64 key)
{
	static char s_DebugName[128];

	fwMvClipSetId clipSetId = fwMvClipSetId(key >> 32);
	fwMvClipId clipId = fwMvClipId(key & 0xFFFFFFFF);
	
	formatf(s_DebugName, sizeof(s_DebugName), "%s:%s", clipSetId.TryGetCStr(), clipId.TryGetCStr());

	return s_DebugName;
}
#endif // DEBUG_POSE_MATCHER

////////////////////////////////////////////////////////////////////////////////

inline fwPoseMatcher::Filter::Filter() :
fwPoseMatcher::BaseFilter()
{
}

////////////////////////////////////////////////////////////////////////////////

inline void fwPoseMatcher::Filter::AddKey(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, f32 bias)
{
	m_Keys.Insert(fwPoseMatcher::CalcKey(clipSetId, clipId), bias);
}

////////////////////////////////////////////////////////////////////////////////

inline u32 fwPoseMatcher::Filter::GetKeyCount() const
{
	return m_Keys.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline float fwPoseMatcher::Filter::PreFilter(u64 key, float)
{
	m_Keys.FinishInsertion();
	f32* bias = m_Keys.SafeGet(key);
	if(bias != NULL)
	{
		return *bias;
	}
	return 0.f;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_POSEMATCHER_H

