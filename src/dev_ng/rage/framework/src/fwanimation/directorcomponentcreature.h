// 
// fwanimation/directorcomponentcreature.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_CREATURE_H
#define FWANIMATION_DIRECTOR_COMPONENT_CREATURE_H

#include "directorcomponent.h"

#include "creature/component.h"
#include "cranimation/commonpool.h"
#include "cranimation/frameaccelerator.h"
#include "cranimation/framebuffers.h"
#include "cranimation/framedatafactory.h"

namespace rage
{

class crCreature;
class crCreatureComponentPhysical;
class fwDynamicEntityComponent;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that manages a creature
class fwAnimDirectorComponentCreature : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentCreature();

	// PURPOSE: Destructor
	~fwAnimDirectorComponentCreature();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentCreature);

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);

	// PURPOSE: 
	void ChangeSkeleton(crSkeleton& skeleton);

	// PURPOSE: Get creature, guaranteed to work after initialization/setup
	crCreature& GetCreature() const;


	// PURPOSE: 
	void ApplyVelocity(float deltaTime, fwDynamicEntityComponent* dynComp);

	// PURPOSE: Get the shared creature component factory
	static crCreatureComponent::Factory* GetComponentFactory();


	// PURPOSE: Get frame data
	const crFrameData& GetFrameData() const;

	// PURPOSE: Get frame buffer
	crFrameBuffer& GetFrameBuffer();

	// PURPOSE: Get PPU frame accelerator
	// NOTE: For use by PPU main thread only - not thread safe to use anywhere else
	static crFrameAccelerator& GetAccelerator();

	// PURPOSE: Get frame pool
	static crCommonPool& GetCommonPool();

	// PURPOSE: Get frame pool
	static crFrameDataFactory& GetFrameDataFactory();

protected:

	// PURPOSE: Implement in derived class to perform class specific initialization
	static void InitDerivedClass();

	// PURPOSE: Implement in derived class to perform class specific initialization
	static void ShutdownDerivedClass();

	// PURPOSE: Override derived class update
	static void UpdateDerivedClass();

private:

	crCreature* m_Creature;
	crCreatureComponentPhysical* m_CreaturePhysicalComponent;
	u32 m_CreatureSignature;
	u32 m_SkeletonSignatureComprehensive;

	crFrame::Accessor m_MoverTranslation;
	crFrame::Accessor m_MoverRotation;

	crFrameBufferFrameData m_FrameBuffer;

	static crFrameAccelerator sm_Accelerator;
	static crCommonPool sm_CommonPool;
	static crFrameDataFactory sm_FrameDataFactory;
	static crCreatureComponent::Factory* sm_Factory;
};

////////////////////////////////////////////////////////////////////////////////

inline crCreature& fwAnimDirectorComponentCreature::GetCreature() const
{
	animFastAssert(m_Creature);
	return *m_Creature;
}

////////////////////////////////////////////////////////////////////////////////

inline crCreatureComponent::Factory* fwAnimDirectorComponentCreature::GetComponentFactory()
{
	return sm_Factory;
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrameData& fwAnimDirectorComponentCreature::GetFrameData() const
{
	return m_Creature->GetFrameData();
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameBuffer& fwAnimDirectorComponentCreature::GetFrameBuffer()
{
	return m_FrameBuffer;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameAccelerator& fwAnimDirectorComponentCreature::GetAccelerator()
{
	return sm_Accelerator;
}

////////////////////////////////////////////////////////////////////////////////

inline crCommonPool& fwAnimDirectorComponentCreature::GetCommonPool()
{
	return sm_CommonPool;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrameDataFactory& fwAnimDirectorComponentCreature::GetFrameDataFactory()
{
	return sm_FrameDataFactory;
}
////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_CREATURE_H

