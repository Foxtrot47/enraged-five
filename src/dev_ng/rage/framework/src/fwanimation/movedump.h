#ifndef MOVEDUMP_H_
#define MOVEDUMP_H_

#if __BANK

// rage includes

#include "crmotiontree/node.h"
#include "crmotiontree/nodeaddn.h"
#include "crmotiontree/nodeaddsubtract.h"
#include "crmotiontree/nodeanimation.h"
#include "crmotiontree/nodeblend.h"
#include "crmotiontree/nodeblendn.h"
#include "crmotiontree/nodecapture.h"
#include "crmotiontree/nodeclip.h"
#include "crmotiontree/nodeexpression.h"
#include "crmotiontree/nodeextrapolate.h"
#include "crmotiontree/nodefilter.h"
#include "crmotiontree/nodeframe.h"
#include "crmotiontree/nodeidentity.h"
#include "crmotiontree/nodeik.h"
#include "crmotiontree/nodeinvalid.h"
#include "crmotiontree/nodejointlimit.h"
#include "crmotiontree/nodemerge.h"
#include "crmotiontree/nodemergen.h"
#include "crmotiontree/nodemirror.h"
#include "crmotiontree/nodepm.h"
#include "crmotiontree/nodepose.h"
#include "crmotiontree/nodeproxy.h"
#include "move/move_command.h"
#include "parser/macros.h"
#include "parser/manager.h"
#include "vector/quaternion.h"

// game includes

namespace rage {

// forward declarations

class fwEntity;
class fwMoveDumpNetworkRequest;
class fwMoveDumpNetworkFlag;
class fwMoveDumpNetwork;
class fwMoveDumpNode;
struct mvKeyedParameter;
class mvNodeState;
class mvObserver;
class mvSubNetwork;

///////////////////////////////////////////////////////////////////////////////

XPARAM(capturemotiontree);
XPARAM(capturemotiontreecount);

///////////////////////////////////////////////////////////////////////////////

template <u32 maxLen>
class FixedString
{
public:

	FixedString()
		: m_Length(0)
	{
		m_Buffer[0] = '\0';
	}

	FixedString(const char* str)
		: m_Length(0)
	{
		m_Buffer[0] = '\0';
		_Append(str);
	}

	FixedString(const FixedString& other)
		: m_Length(0)
	{
		m_Buffer[0] = '\0';
		_Append(other);
	}

	void operator+=(const char* str)
	{
		_Append(str);
	}

	void operator+=(const FixedString& other)
	{
		_Append(other);
	}

	void operator=(const char* str)
	{
		m_Length = 0;
		m_Buffer[0] = '\0';

		_Append(str);
	}

	void operator=(const FixedString& other)
	{
		m_Length = 0;
		m_Buffer[0] = '\0';

		_Append(other);
	}

	const char* c_str() const
	{
		return m_Buffer;
	}

	u32 GetLength() const
	{
		return m_Length;
	}

	int IndexOf(char ch, u32 startIdx=0) const
	{
		for(u32 i=0; i<m_Length; ++i)
		{
			if(m_Buffer[i] == ch)
			{
				return int(i);
			}
		}
		return -1;
	}

	int IndexOf(const char* str, u32 startIdx=0) const
	{
		if(startIdx >= m_Length)
		{
			return -1;
		}

		const char* p = strstr(m_Buffer + startIdx, str);
		if(p)
		{
			return int(p - m_Buffer);
		}

		return -1;
	}

	int LastIndexOf(char ch) const
	{
		for(u32 i=m_Length; i>0; --i)
		{
			if(m_Buffer[i-1] == ch)
			{
				return int(i-1);
			}
		}
		return -1;
	}
	
	int LastIndexOf(const char* str) const
	{
		const char* p = strstr(m_Buffer, str);
		if(p)
		{
			const char* pLast = p;
			while((pLast+1) < (m_Buffer + m_Length))
			{
				p = strstr(pLast+1, str);
				if(!p)
				{
					break;
				}
				pLast = p;
			} 

			return int(pLast - m_Buffer);
		}

		return -1;
	}

	bool StartsWith(const char* str) const
	{
		u32 strLen = u32(strlen(str));
		if(strLen > m_Length)
		{
			return false;
		}

		for(u32 i=0; i<strLen; i++)
		{
			if (m_Buffer[i] != str[i])
			{
				return false;
			}
		}

		return true;
	}

	void Replace(const char* search, const char* replace)
	{
		int pos = IndexOf(search);
		if(pos >= 0)
		{
			int searchLen = int(strlen(search));
			int replaceLen = int(strlen(replace));

			while(pos >= 0)
			{
				int copyLen = m_Length;
				char copy[maxLen];
				sysMemCpy(copy, m_Buffer, m_Length+1);
				m_Buffer[pos] = '\0';

				m_Length = 0;
				m_Buffer[0] = '\0';

				_Append(copy, pos);
				_Append(replace, replaceLen);
				_Append(copy+pos+searchLen, copyLen-pos-searchLen);

				pos = IndexOf(search, pos + replaceLen);
			}
		}
	}

	void Truncate(u32 idx)
	{
		if(idx < m_Length)
		{
			m_Buffer[idx] = '\0';
			m_Length = idx;
		}
	}

protected:

	void _Append(const char* str)
	{
		if(!str)
		{
			return;
		}

		const u32 newLen = Min(m_Length + u32(strlen(str)), maxLen-1);
		if(newLen > m_Length)
		{
			sysMemCpy(m_Buffer + m_Length, str, newLen - m_Length);
			m_Buffer[newLen] = '\0';
			m_Length = newLen;
		}
	}

	void _Append(const char* str, u32 strLen)
	{
		if(!str)
		{
			return;
		}

		const u32 newLen = Min(m_Length + strLen, maxLen-1);
		if(newLen > m_Length)
		{
			sysMemCpy(m_Buffer + m_Length, str, newLen - m_Length);
			m_Buffer[newLen] = '\0';
			m_Length = newLen;
		}
	}

	void _Append(const FixedString& other)
	{
		const u32 newLen = Min(m_Length + other.m_Length, maxLen-1);
		if(newLen > m_Length)
		{
			sysMemCpy(m_Buffer + m_Length, other.m_Buffer, newLen - m_Length);
			m_Buffer[newLen] = '\0';
			m_Length = newLen;
		}
	}

protected:

	char m_Buffer[maxLen];
	u32 m_Length;
};

///////////////////////////////////////////////////////////////////////////////

template <u32 maxLen>
class VarFixedString : public FixedString<maxLen>
{
	typedef FixedString<maxLen> _Base;

public:

	VarFixedString(const char* format, ...)
		: FixedString<maxLen>()
	{
		va_list args;
		va_start(args, format);
		vsnprintf(_Base::m_Buffer, sizeof(_Base::m_Buffer), format, args);
		va_end(args);

		_Base::m_Length = u32(strlen(_Base::m_Buffer));
	}
};

///////////////////////////////////////////////////////////////////////////////

typedef FixedString<512> fwMoveDumpString;
typedef VarFixedString<512> fwMoveDumpVarString;

///////////////////////////////////////////////////////////////////////////////

fwMoveDumpNode *CreateMoveDumpNode(const fwEntity *pEntity, const crmtNode &node);

///////////////////////////////////////////////////////////////////////////////

struct fwMoveDumpRenderFlags
{
	fwMoveDumpRenderFlags(bool bRenderTree,
		bool bRenderClass,
		bool bRenderWeight,
		bool bRenderLocalWeight,
		bool bRenderStateActiveName,
		bool bRenderRequests,
		bool bRenderFlags,
		bool bRenderInputParams,
		bool bRenderOutputParams,
		bool bRenderInvalidNodes,
		bool bRenderSyncWeights,
		bool bRenderSyncGraph,
		bool bRenderDofs,
		bool bRenderAddress)
		: m_bRenderTree(bRenderTree)
		, m_bRenderClass(bRenderClass)
		, m_bRenderWeight(bRenderWeight)
		, m_bRenderLocalWeight(bRenderLocalWeight)
		, m_bRenderStateActiveName(bRenderStateActiveName)
		, m_bRenderRequests(bRenderRequests)
		, m_bRenderFlags(bRenderFlags)
		, m_bRenderInputParams(bRenderInputParams)
		, m_bRenderOutputParams(bRenderOutputParams)
		, m_bRenderInvalidNodes(bRenderInvalidNodes)
		, m_bRenderSyncWeights(bRenderSyncWeights)
		, m_bRenderSyncGraph(bRenderSyncGraph)
		, m_bRenderDofs(bRenderDofs)
		, m_bRenderAddress(bRenderAddress)
	{
	}

	bool m_bRenderTree : 1;
	bool m_bRenderClass : 1;
	bool m_bRenderWeight : 1;
	bool m_bRenderLocalWeight : 1;
	bool m_bRenderStateActiveName : 1;
	bool m_bRenderRequests : 1;
	bool m_bRenderFlags : 1;
	bool m_bRenderInputParams : 1;
	bool m_bRenderOutputParams : 1;
	bool m_bRenderInvalidNodes : 1;
	bool m_bRenderSyncWeights : 1;
	bool m_bRenderSyncGraph : 1;
	bool m_bRenderDofs : 1;
	bool m_bRenderAddress : 1;
};


//Print structures
///////////////////////////////////////////////////////////////////////////////
struct fwMoveDumpPrinterBase
{
	virtual void PrintLine(int iIndent, const char *szText) = 0;
	virtual ~fwMoveDumpPrinterBase(){};
};

struct fwMoveDumpPrinterFile : public fwMoveDumpPrinterBase
{
	fiStream *mp_File;
	fwMoveDumpPrinterFile(fiStream *pFile)
		:mp_File(pFile)
	{	}
	virtual void PrintLine(int iIndent, const char *szText);
};

struct fwMoveDumpPrinterTTY : public fwMoveDumpPrinterBase
{
	virtual void PrintLine(int iIndent, const char *szText);
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNode
{
public:

	/* Types */

	typedef fwMoveDumpString String;
	typedef fwMoveDumpVarString VarString;

	/* Destructor */

	virtual ~fwMoveDumpNode();

	/* Constructors */

	fwMoveDumpNode() { }
	fwMoveDumpNode(const fwEntity *pEntity, const crmtNode &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Node"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
	virtual float GetChildWeight(int /*iChildIndex*/) const { return 1.0f; }
	virtual bool IsInvalidNode() const { return false; }
	virtual bool IsStateNode() const { return false; }

	/* Serializable properties */

	const atString &GetName() const { return m_Name; }
	const atString &GetAddress() const { return m_Name; }
	const atArray< fwMoveDumpNode * > &GetChildren() const { return m_Children; }
	atArray< fwMoveDumpNode * > &GetChildren() { return m_Children; }

	/* Operations */

	virtual void WalkAndRender(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend, unsigned& numNodes) const;
	virtual void Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
	virtual void WalkAndPrint(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend, unsigned& numNodes) const;
	virtual void Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

protected:

	/* Serialized data */

	atString m_Name;
	atString m_Address;
	atArray< fwMoveDumpNode * > m_Children;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeAnimation : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeAnimation() { }

	/* Constructors */

	fwMoveDumpNodeAnimation() { }
	fwMoveDumpNodeAnimation(const fwEntity *pEntity, const crmtNodeAnimation &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Animation"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeClipTag
{
public:

	/* Destructor */

	virtual ~fwMoveDumpNodeClipTag() { }

	/* Constructors */

	fwMoveDumpNodeClipTag() : m_Mid(0.0f), m_Active(false), m_Foot(false),  m_Heel(false), m_Right(false) { }
	fwMoveDumpNodeClipTag(const char *szName, float fMid, bool bActive, bool bFoot, bool bHeel, bool bRight, const atHashString &moveEvent)
		: m_Name(szName), m_Mid(fMid), m_Active(bActive), m_Foot(bFoot), m_Heel(bHeel), m_Right(bRight), m_MoveEvent(moveEvent)
	{
		Assert(m_Name);
		Assert(m_MoveEvent.IsNull() || m_MoveEvent.GetCStr());
	}

	/* Serialized properties */

	const atString &GetName() const { return m_Name; }
	float GetMid() const { return m_Mid; }
	bool GetActive() const { return m_Active; }
	bool GetFoot() const { return m_Foot; }
	bool GetRight() const { return m_Right; }
	bool GetHeel() const { return m_Heel; }
	const atHashString &GetMoveEvent() const { return m_MoveEvent; }

protected:

	/* Serialized data */

	atString m_Name;
	float m_Mid;
	bool m_Active;
	bool m_Foot;
	bool m_Heel;
	bool m_Right;
	atHashString m_MoveEvent;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeClip : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeClip() { }

	/* Constructors */

	fwMoveDumpNodeClip();
	fwMoveDumpNodeClip(const fwEntity *pEntity, const crmtNodeClip &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Clip"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	/* Serialized properties */

	const atString &GetClip() const { return m_Clip; }
	float GetTime() const { return m_Time; }
	float GetDuration() const { return m_Duration; }
	float GetPhase() const { return m_Phase; }
	float GetRate() const { return m_Rate; }
	bool GetLooped() const { return m_Looped; }
	const atArray< fwMoveDumpNodeClipTag > &GetTags() const { return m_Tags; }

	/* Operations */

	virtual void Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
	virtual void Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

protected:

	/* Serialized data */

	atString m_Clip;
	float m_Time;
	float m_Duration;
	float m_Phase;
	float m_Rate;
	bool m_Looped;
	atArray< fwMoveDumpNodeClipTag > m_Tags;
	atArray< atString > m_Synchronizers;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeFrame : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeFrame() { }

	/* Constructors */

	fwMoveDumpNodeFrame() { }
	fwMoveDumpNodeFrame(const fwEntity *pEntity, const crmtNodeFrame &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Frame"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeIdentity : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeIdentity() { }

	/* Constructors */

	fwMoveDumpNodeIdentity() { }
	fwMoveDumpNodeIdentity(const fwEntity *pEntity, const crmtNodeIdentity &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Identity"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeInvalid : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeInvalid() { }

	/* Constructors */

	fwMoveDumpNodeInvalid() { }
	fwMoveDumpNodeInvalid(const fwEntity *pEntity, const crmtNodeInvalid &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Invalid"; }
	virtual bool IsInvalidNode() const { return true; }

	/* Operations */

	virtual void Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
	virtual void Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodePm : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodePm() { }

	/* Constructors */

	fwMoveDumpNodePm() { }
	fwMoveDumpNodePm(const fwEntity *pEntity, const crmtNodePm &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Pm"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodePose : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodePose() { }

	/* Constructors */

	fwMoveDumpNodePose() { }
	fwMoveDumpNodePose(const fwEntity *pEntity, const crmtNodePose &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Pose"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeProxy : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

	atString m_Target;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeProxy() { }

	/* Constructors */

	fwMoveDumpNodeProxy() { }
	fwMoveDumpNodeProxy(const fwEntity *pEntity, const crmtNodeProxy &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Proxy"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeParent : public fwMoveDumpNode
{
private:

	typedef fwMoveDumpNode base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeParent() { }

	/* Constructors */

	fwMoveDumpNodeParent() { }
	fwMoveDumpNodeParent(const fwEntity *pEntity, const crmtNodeParent &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Parent"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeCapture : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeCapture() { }

	/* Constructors */

	fwMoveDumpNodeCapture() { m_HasFrame = false; }
	fwMoveDumpNodeCapture(const fwEntity *pEntity, const crmtNodeCapture &node) : base(pEntity, node) { 
		if (node.GetFrame())
			m_HasFrame = true;
		else
			m_HasFrame = false;
	}

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Capture"; }

	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	bool m_HasFrame;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeExpression : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeExpression() { }

	/* Constructors */

	fwMoveDumpNodeExpression();
	fwMoveDumpNodeExpression(const fwEntity *pEntity, const crmtNodeExpression &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Expression"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	/* Serializable properties */

	const atString &GetExpression() const { return m_Expression; }
	float GetTime() const { return m_Time; }
	float GetWeight() const { return m_Weight; }

protected:

	/* Serializable data */

	atString m_Expression;
	float m_Time;
	float m_Weight;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeExtrapolate : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeExtrapolate() { }

	/* Constructors */

	fwMoveDumpNodeExtrapolate() { }
	fwMoveDumpNodeExtrapolate(const fwEntity *pEntity, const crmtNodeExtrapolate &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Extrapolate"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
   
	float m_Damping;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeFilter : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeFilter() { }

	/* Constructors */

	fwMoveDumpNodeFilter() { }
	fwMoveDumpNodeFilter(const fwEntity *pEntity, const crmtNodeFilter &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Filter"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	/* Serializable properties */

	const atString &GetFilter() const { return m_Filter; }

protected:

	/* Serializable data */

	atString m_Filter;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeIk : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeIk() { }

	/* Constructors */

	fwMoveDumpNodeIk() { }
	fwMoveDumpNodeIk(const fwEntity *pEntity, const crmtNodeIk &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "IK"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeJointLimit : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeJointLimit() { }

	/* Constructors */

	fwMoveDumpNodeJointLimit() { }
	fwMoveDumpNodeJointLimit(const fwEntity *pEntity, const crmtNodeJointLimit &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "JointLimit"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeMerge : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeMerge() { }

	/* Constructors */

	fwMoveDumpNodeMerge() { }
	fwMoveDumpNodeMerge(const fwEntity *pEntity, const crmtNodeMerge &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Merge"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	/* Serializable properties */

	const atString &GetFilter() const { return m_Filter; }

protected:

	/* Serializable data */

	atString m_Filter;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeMirror : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeMirror() { }

	/* Constructors */

	fwMoveDumpNodeMirror() { }
	fwMoveDumpNodeMirror(const fwEntity *pEntity, const crmtNodeMirror &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Mirror"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeStateInputParam
{
public:

	/* Destructor */

	virtual ~fwMoveDumpNodeStateInputParam() { }

	/* Constructors */

	fwMoveDumpNodeStateInputParam() { }
	fwMoveDumpNodeStateInputParam(const mvKeyedParameter &keyedParameter);

	/* Serialized properties */

	const atString &GetName() const { return m_Name; }
	const atString &GetType() const { return m_Type; }
	const atString &GetValue() const { return m_Value; }

protected:

	/* Serialized data */

	atString m_Name;
	atString m_Type;
	atString m_Value;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeStateOutputParam
{
public:

	/* Destructor */

	virtual ~fwMoveDumpNodeStateOutputParam() { }

	/* Constructors */

	fwMoveDumpNodeStateOutputParam() { }
	fwMoveDumpNodeStateOutputParam(const mvKeyedParameter &keyedParameter);

	/* Serialized properties */

	const atString &GetName() const { return m_Name; }
	const atString &GetType() const { return m_Type; }
	const atString &GetValue() const { return m_Value; }

protected:

	/* Serialized data */

	atString m_Name;
	atString m_Type;
	atString m_Value;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeState : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeState();

	/* Constructors */

	fwMoveDumpNodeState();
	fwMoveDumpNodeState(const fwEntity *pEntity, const crmtNodeParent &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "State"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
	virtual bool IsStateNode() const { return true; }

	/* Serialized properties */

	bool IsInterestingState() const { return m_IsInterestingState; }
	const atArray< fwMoveDumpNetworkRequest * > &GetRequests() const { return m_Requests; }
	const atArray< fwMoveDumpNetworkFlag * > &GetFlags() const { return m_Flags; }

	/* Operations */

	virtual void Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;
	virtual void Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

protected:

	/* Serialized data */

	bool m_IsInterestingState;
	atString m_StateInterfaceType;
	atString m_StateInterfaceName;
	atString m_ActiveName;
	atString m_ClipSet;
	int m_Transitions;
	atArray< fwMoveDumpNetworkRequest * > m_Requests;
	atArray< fwMoveDumpNetworkFlag * > m_Flags;
	atArray< fwMoveDumpNodeStateInputParam * > m_InputParams;
	atArray< fwMoveDumpNodeStateOutputParam * > m_OutputParams;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodePair : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodePair() { }

	/* Constructors */

	fwMoveDumpNodePair();
	fwMoveDumpNodePair(const fwEntity *pEntity, const crmtNodePairWeighted &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Pair"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags, float fWeight, float fLocalWeight, bool bIsParentBlend) const;

	/* Serializable properties */

	float GetWeight() const { return m_Weight; }
	float GetWeightRate() const { return m_WeightRate; }
	const atString &GetFilter() const { return m_Filter; }

protected:

	/* Serialized data */

	float m_Weight;
	float m_WeightRate;
	atString m_Filter;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeAddSubtract : public fwMoveDumpNodePair
{
private:

	typedef fwMoveDumpNodePair base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeAddSubtract() { }

	/* Constructors */

	fwMoveDumpNodeAddSubtract() { }
	fwMoveDumpNodeAddSubtract(const fwEntity *pEntity, const crmtNodeAddSubtract &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "AddSubtract"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeBlend : public fwMoveDumpNodePair
{
private:

	typedef fwMoveDumpNodePair base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeBlend() { }

	/* Constructors */

	fwMoveDumpNodeBlend() { }
	fwMoveDumpNodeBlend(const fwEntity *pEntity, const crmtNodeBlend &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Blend"; }
	virtual float GetChildWeight(int iChildIndex) const;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeN : public fwMoveDumpNodeParent
{
private:

	typedef fwMoveDumpNodeParent base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeN() { }

	/* Constructors */

	fwMoveDumpNodeN() { }
	fwMoveDumpNodeN(const fwEntity *pEntity, const crmtNodeN &node);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "NodeN"; }

	/* Serializable properties */

	const atArray< float > &GetWeights() const { return m_Weights; }

protected:

	/* Serializable data */

	atArray< float > m_Weights;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeAddN : public fwMoveDumpNodeN
{
private:

	typedef fwMoveDumpNodeN base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeAddN() { }

	/* Constructors */

	fwMoveDumpNodeAddN() { }
	fwMoveDumpNodeAddN(const fwEntity *pEntity, const crmtNodeAddN &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "AddN"; }
	virtual float GetChildWeight(int iChildIndex) const;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeBlendN : public fwMoveDumpNodeN
{
private:

	typedef fwMoveDumpNodeN base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeBlendN() { }

	/* Constructors */

	fwMoveDumpNodeBlendN() { }
	fwMoveDumpNodeBlendN(const fwEntity *pEntity, const crmtNodeBlendN &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "BlendN"; }
	virtual float GetChildWeight(int iChildIndex) const;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNodeMergeN : public fwMoveDumpNodeN
{
private:

	typedef fwMoveDumpNodeN base;

public:

	/* Destructor */

	virtual ~fwMoveDumpNodeMergeN() { }

	/* Constructors */

	fwMoveDumpNodeMergeN() { }
	fwMoveDumpNodeMergeN(const fwEntity *pEntity, const crmtNodeMergeN &node) : base(pEntity, node) { }

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "MergeN"; }

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

enum eMoveDumpDofType
{
	eDofUnknown,
	eDofVector3,
	eDofQuaternion,
	eDofFloat,
};

class fwMoveDumpDof
{
public:

	/* Types */

	typedef fwMoveDumpString String;
	typedef fwMoveDumpVarString VarString;

	/* Destructor */

	virtual ~fwMoveDumpDof() { }

	/* Constructors */

	fwMoveDumpDof() : m_Track(0), m_Id(0), m_Type(0), m_Invalid(false) { }
	fwMoveDumpDof(u8 track, u16 id, u8 type, bool invalid, const crSkeletonData &skelData);

	/* Properties */

	virtual eMoveDumpDofType GetDofType() const { return eDofUnknown; }
	const char *GetTrackName() const;
	const char *GetIdName() const { return m_IdName; }
	virtual const char *GetTypeName() const { return "Dof"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags) const;

	/* Operations */

	virtual void Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight) const;
	virtual void Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags, float fWeight) const;

	/* Serializable properties */

	u8 GetTrack() const { return m_Track; }
	u16 GetId() const { return m_Id; }
	u8 GetType() const { return m_Type; }
	bool GetInvalid() const { return m_Invalid; }

protected:

	/* Data */

	const char *m_IdName;

	/* Serializable data */

	u8 m_Track;
	u16 m_Id;
	u8 m_Type;
	bool m_Invalid;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpDofVector3 : public fwMoveDumpDof
{
private:

	typedef fwMoveDumpDof base;

public:

	/* Destructor */

	virtual ~fwMoveDumpDofVector3() { }

	/* Constructors */

	fwMoveDumpDofVector3() { }
	fwMoveDumpDofVector3(u8 track, u16 id, u8 type, bool invalid, const crSkeletonData &skelData, const Vector3 &value) : base(track, id, type, invalid, skelData), m_Value(value) { }

	/* Properties */

	virtual eMoveDumpDofType GetDofType() const { return eDofVector3; }
	virtual const char *GetTypeName() const { return "Vec3"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags) const;

	/* Serializable properties */

	const Vector3 &GetValue() const { return m_Value; }

protected:

	/* Serializable data */

	Vector3 m_Value;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpDofQuaternion : public fwMoveDumpDof
{
private:

	typedef fwMoveDumpDof base;

public:

	/* Destructor */

	virtual ~fwMoveDumpDofQuaternion() { }

	/* Constructors */

	fwMoveDumpDofQuaternion() { }
	fwMoveDumpDofQuaternion(u8 track, u16 id, u8 type, bool invalid, const crSkeletonData &skelData, const Quaternion &value) : base(track, id, type, invalid, skelData), m_Value(value) { }

	/* Properties */

	virtual eMoveDumpDofType GetDofType() const { return eDofQuaternion; }
	virtual const char *GetTypeName() const { return "Quat"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags) const;

	/* Serializable properties */

	const Quaternion &GetValue() const { return m_Value; }

protected:

	/* Serializable data */

	Quaternion m_Value;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpDofFloat : public fwMoveDumpDof
{
private:

	typedef fwMoveDumpDof base;

public:

	/* Destructor */

	virtual ~fwMoveDumpDofFloat() { }

	/* Constructors */

	fwMoveDumpDofFloat() { }
	fwMoveDumpDofFloat(u8 track, u16 id, u8 type, bool invalid, const crSkeletonData &skelData, float value) : base(track, id, type, invalid, skelData), m_Value(value) { }

	/* Properties */

	virtual eMoveDumpDofType GetDofType() const { return eDofFloat; }
	virtual const char *GetTypeName() const { return "Float"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags) const;

	/* Serializable properties */

	float GetValue() const { return m_Value; }

protected:

	/* Serializable data */

	float m_Value;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNetworkRequest
{
public:

	/* Destructor */

	virtual ~fwMoveDumpNetworkRequest() { }

	/* Constructors */

	fwMoveDumpNetworkRequest() { }
	fwMoveDumpNetworkRequest(const atHashString &name, bool bValue)
		: m_Name(name), m_bValue(bValue)
	{
		Assert(m_Name.IsNull() || m_Name.GetCStr());
	}

	/* Serializable properties */

	const atHashString &GetName() const { return m_Name; }
	bool GetValue() const { return m_bValue; }

protected:

	/* Serializable data */

	atHashString m_Name;
	bool m_bValue;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNetworkFlag
{
public:

	/* Destructor */

	virtual ~fwMoveDumpNetworkFlag() { }

	/* Constructors */

	fwMoveDumpNetworkFlag() { }
	fwMoveDumpNetworkFlag(const atHashString &name, bool bValue)
		: m_Name(name), m_bValue(bValue)
	{
		Assert(m_Name.IsNull() || m_Name.GetCStr());
	}

	/* Serializable properties */

	const atHashString &GetName() const { return m_Name; }
	bool GetValue() const { return m_bValue; }

protected:

	/* Serializable data */

	atHashString m_Name;
	bool m_bValue;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpNetwork
{
public:

	/* Types */

	typedef fwMoveDumpString String;
	typedef fwMoveDumpVarString VarString;

	/* Destructor */

	virtual ~fwMoveDumpNetwork();

	/* Constructors */

	fwMoveDumpNetwork();
	fwMoveDumpNetwork(const fwEntity *pEntity, bool bCaptureDof, const char *szFilter, bool bRelativeToParent);

	/* Properties */

	virtual const char *GetFriendlyClassName() const { return "Network"; }
	virtual String GetDescription(const fwMoveDumpRenderFlags &renderFlags) const;

	/* Serializable properties */

	u32 GetFrameIndex() const { return m_FrameIndex; }
	u32 GetFrameTime() const { return m_FrameTime; }
	u32 GetFrameTimeStep() const { return m_FrameTimeStep; }
	const fwMoveDumpNode *GetRoot() const { return m_Root; }
	const Matrix34 &GetMatrix() const { return m_Matrix; }
	const atArray< fwMoveDumpNetworkRequest * > &GetRequests() const { return m_Requests; }
	const atArray< fwMoveDumpNetworkFlag * > &GetFlags() const { return m_Flags; }
	const atArray< fwMoveDumpDof * > &GetDofs() const { return m_Dofs; }

	/* Operations */

	virtual void Render(float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags);
	virtual void Print(fwMoveDumpPrinterBase &rPrinter, int &iHorizontalIndent, const fwMoveDumpRenderFlags &renderFlags);
	void Apply(fwEntity *pEntity, const char *szFilter, bool bRelativeToParent) const;

protected:

	/* Serialized data */

	u32 m_NumNodes;
	u32 m_FrameIndex;
	u32 m_FrameTime;
	u32 m_FrameTimeStep;
	fwMoveDumpNode *m_Root;
	Matrix34 m_Matrix;
	atString m_Entity;
	atString m_Filter;
	atArray< fwMoveDumpNetworkRequest * > m_Requests;
	atArray< fwMoveDumpNetworkFlag * > m_Flags;
	atArray< fwMoveDumpDof * > m_Dofs;

	u32 m_CaptureTimeMs;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

class fwMoveDumpFrameSequence
{
public:

	/* Types */

	typedef fwMoveDumpString String;
	typedef fwMoveDumpVarString VarString;

	/* Destructor */

	virtual ~fwMoveDumpFrameSequence();

	/* Constructors */

	fwMoveDumpFrameSequence();
	fwMoveDumpFrameSequence(u32 uMaxFrames);

	/* Properties */

	u32 GetFrameCount() const { return m_Frames.GetCount(); }

	/* Serializable properties */

	u32 GetMaxFrameCount() const { return m_MaxFrameCount; }
	u32 GetStartFrame() const { return m_StartFrame; }
	u32 GetCurrentFrame() const { return m_CurrentFrame; }
	const atArray< fwMoveDumpNetwork * > &GetFrames() const { return m_Frames; }

	/* Operations */

	void Capture(const fwEntity *pEntity, bool bCaptureDof, const char *szFilter, bool bRelativeToParent);
	void Render(u32 uFrameIndex, float &fOriginX, float &fOriginY, float fHorizontalIndent, float fVerticalIndent, const fwMoveDumpRenderFlags &renderFlags) const;
	void Print(fwMoveDumpPrinterBase &rPrinter, u32 uFrameIndex, const fwMoveDumpRenderFlags &renderFlags) const;
	void Reset();
	void Apply(fwEntity *pEntity, u32 uFrameIndex, const char *szFilter, bool bRelativeToParent) const;

	bool Load(const char *szFilename);
	bool Save(const char *szFilename);
	bool Print(const char *szFilename, u32 uFrameIndex, const fwMoveDumpRenderFlags &renderFlags) const;
	bool Print(const char *szFilename, const fwMoveDumpRenderFlags &renderFlags) const;

protected:

	/* Serializable data */

	u32 m_MaxFrameCount;
	u32 m_StartFrame;
	u32 m_CurrentFrame;
	atArray< fwMoveDumpNetwork * > m_Frames;

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // __BANK

#endif // MOVEDUMP_H_
