// 
// fwanimation/directorcomponent.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponent.h"

#include "directorcomponentcreature.h"
#include "directorcomponentexpressions.h"
#include "directorcomponentfacialrig.h"
#include "directorcomponentmotiontree.h"
#include "directorcomponentmove.h"
#include "directorcomponentragdoll.h"
#include "directorcomponentsyncedscene.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponent::fwAnimDirectorComponent(eComponentType componentType)
: m_ComponentType(componentType)
, m_Director(NULL)
, m_Phase(kPhaseAll)
{
	Assert(componentType != kComponentTypeNone);
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponent::~fwAnimDirectorComponent()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::Shutdown()
{
	m_ComponentType = kComponentTypeNone;
	m_Director = NULL;
	m_Phase = kPhaseAll;
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponent* fwAnimDirectorComponent::Allocate(eComponentType componentType)
{
	const ComponentTypeInfo* info = FindComponentTypeInfo(componentType);
	Assert(info);
	if(info)
	{
		ComponentAllocateFn* allocateFn = info->GetAllocateFn();
		fwAnimDirectorComponent* component = allocateFn();
		Assert(component);
		return component;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if CR_DEV
const char* fwAnimDirectorComponent::GetComponentTypeName() const
{
	const ComponentTypeInfo* info = FindComponentTypeInfo(GetComponentType());
	if(info)
	{
		return info->GetName();
	}

	return "fwAnimDirectorComponent_UNKNOWN_TYPE";
}
#endif // CR_DEV

////////////////////////////////////////////////////////////////////////////////

u32 fwAnimDirectorComponent::GetPriority() const 
{
	return 100;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::ReceiveMessage(eMessageId, ePhase, void*)
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::PreUpdate(ePhase)
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::Update(ePhase)
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::PostUpdate(ePhase)
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::InitClass()
{
	if(!sm_InitClassCalled)
	{
		sm_InitClassCalled = true;

		sm_ComponentTypeInfos.Resize(kComponentTypeNum);
		for(int i=0; i<kComponentTypeNum; ++i)
		{
			sm_ComponentTypeInfos[i] = NULL;
		}

		// register all built in component types
		fwAnimDirectorComponentCreature::RegisterClass();
		fwAnimDirectorComponentMotionTree::RegisterClass();
		fwAnimDirectorComponentMove::RegisterClass();
		fwAnimDirectorComponentExpressions::RegisterClass();
		fwAnimDirectorComponentRagDoll::RegisterClass();
		fwAnimDirectorComponentSyncedScene::RegisterClass();
		fwAnimDirectorComponentFacialRig::RegisterClass();

		// invoke init class on all the derived classes
		for(int i=0; i<kComponentTypeNum; ++i)
		{
			if(sm_ComponentTypeInfos[i])
			{
				ComponentFn* initFn = sm_ComponentTypeInfos[i]->GetInitFn();
				initFn();
				ComponentFn* initPoolFn = sm_ComponentTypeInfos[i]->GetInitPoolFn();
				initPoolFn();
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::ShutdownClass()
{
	// invoke shutdown class on all the derived classes
	for(int i=0; i<kComponentTypeNum; ++i)
	{
		if(sm_ComponentTypeInfos[i])
		{
			ComponentFn* shutdownPoolFn = sm_ComponentTypeInfos[i]->GetShutdownPoolFn();
			shutdownPoolFn();
			ComponentFn* shutdownFn = sm_ComponentTypeInfos[i]->GetShutdownFn();
			shutdownFn();
		}
	}

	sm_ComponentTypeInfos.Reset();
	sm_InitClassCalled = false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::UpdateClass()
{
	// invoke update class on all the derived classes
	for(int i=0; i<kComponentTypeNum; ++i)
	{
		if(sm_ComponentTypeInfos[i])
		{
			ComponentFn* updateFn = sm_ComponentTypeInfos[i]->GetUpdateFn();
			updateFn();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponent::ComponentTypeInfo::ComponentTypeInfo(eComponentType componentType, const char* name, size_t size, ComponentAllocateFn* allocateFn, ComponentFn* initFn, ComponentFn* updateFn, ComponentFn* shutdownFn, ComponentFn* initPoolFn, ComponentFn* shutdownPoolFn)
: m_ComponentType(componentType)
, m_Name(name)
, m_Size(size)
, m_AllocateFn(allocateFn)
, m_InitFn(initFn)
, m_UpdateFn(updateFn)
, m_ShutdownFn(shutdownFn)
, m_InitPoolFn(initPoolFn)
, m_ShutdownPoolFn(shutdownPoolFn)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponent::ComponentTypeInfo::~ComponentTypeInfo()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::ComponentTypeInfo::Register() const
{
	fwAnimDirectorComponent::RegisterComponentTypeInfo(*this);
}

////////////////////////////////////////////////////////////////////////////////

const fwAnimDirectorComponent::ComponentTypeInfo* fwAnimDirectorComponent::FindComponentTypeInfo(eComponentType componentType)
{
	Assert(componentType < kComponentTypeNum);
	if(componentType < kComponentTypeNum)
	{
		Assert(componentType > kComponentTypeNone);
		Assert(sm_ComponentTypeInfos.GetCount() > 0);
		Assert(componentType < sm_ComponentTypeInfos.GetCount());

		const ComponentTypeInfo* typeInfo = sm_ComponentTypeInfos[componentType];
		Assert(typeInfo);

		return typeInfo;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::InitBase(fwAnimDirector& director)
{
	Assert(!m_Director);
	m_Director = &director;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::RegisterComponentTypeInfo(const ComponentTypeInfo& info)
{
	Assert(sm_ComponentTypeInfos.GetCount() > 0);
	Assert(info.GetComponentType() < kComponentTypeNum);

	Assert(info.GetComponentType() < sm_ComponentTypeInfos.GetCount());
	Assert(sm_ComponentTypeInfos[info.GetComponentType()] == NULL);

	sm_ComponentTypeInfos[info.GetComponentType()] = &info;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::InitDerivedClass()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::UpdateDerivedClass()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponent::ShutdownDerivedClass()
{
}

////////////////////////////////////////////////////////////////////////////////

atArray<const fwAnimDirectorComponent::ComponentTypeInfo*> fwAnimDirectorComponent::sm_ComponentTypeInfos;
bool fwAnimDirectorComponent::sm_InitClassCalled = false;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage



