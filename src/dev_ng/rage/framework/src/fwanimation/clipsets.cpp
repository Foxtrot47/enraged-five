
#include "fwanimation/clipsets.h"
#include "fwanimation/anim_channel.h"

#include "atl/creator.h"
#include "bank/combo.h"
#include "file/default_paths.h"
#include "fwanimation/animmanager.h"
#include "fwanimation/animhelpers.h"
#include "fwmaths/random.h"
#include "fwnet/netinterface.h"
#include "fwscene/stores/clipdictionarystore.h"
#include "fwscene/stores/psostore.h"
#include "fwsys/fileExts.h"
#include "fwsys/gameskeleton.h"
#include "parser/codegenutils.h"
#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/psoparserbuilder.h"
#include "streaming/streamingvisualize.h"

using namespace rage;

#include "clipsets_parser.h"

PARAM(validateclipsets, "Set to validate ClipSets on fwClipSetManager init");
PARAM(loadclipsetsfromxmlandsavepso, "Load the ClipSets from clipsets.xml and save to clipsets.pso");

#if __BANK
#include "bank/msgbox.h"
#endif // __BANK

namespace rage {

#if	__BANK

int SortAlphabeticalNoCase(const char* const* pp_A,const char* const* pp_B)
{
	return stricmp(*pp_A,*pp_B);
}

#endif //__BANK

//////////////////////////////////////////////////////////////////////////

const char *g_clipSetPlatformDirectory = "platform:/data/anim/clip_sets";

#if __BANK
const char *g_clipSetAssetDirectory = RS_ASSETS "/export/data/anim/clip_sets";
#endif

//////////////////////////////////////////////////////////////////////////

#if __ASSERT

const char *PrintClipInfo(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId)
{
	static const int bufferLength = 1024;
	static char buffer[bufferLength];

	atHashString clipDictionaryHashString;
	atHashString clipHashString;

	if(fwClipSetManager::GetClipDictionaryNameAndClipName(clipSetId, clipId, clipDictionaryHashString, clipHashString))
	{
		const char * pClipSetName = clipSetId.TryGetCStr();
		const char * pClipName = clipId.TryGetCStr();
		sprintf(buffer, "ClipSet: %s %u Clip: %s %u ClipDictionary: %s %u Clip: %s %u",
			pClipSetName ? pClipSetName : "UNKNOWN", clipSetId.GetHash(),
			pClipName ? pClipName : "UNKNOWN", clipId.GetHash(),
			clipDictionaryHashString.GetCStr(), clipDictionaryHashString.GetHash(),
			clipHashString.GetCStr(), clipHashString.GetHash());
	}
	else
	{
		sprintf(buffer, "ClipSetId: %s %u ClipId: %s %u NO CLIP FOUND",
			clipSetId.GetCStr(), clipSetId.GetHash(),
			clipId.GetCStr(), clipId.GetHash());
	}

	return buffer;
}

#endif // __ASSERT

//////////////////////////////////////////////////////////////////////////

fwClipSet::fwClipSet()
: m_fallbackId(CLIP_SET_ID_INVALID)
, m_clipDictionaryName(atHashString::Null())
, m_clipDictionaryIdx(-1)
, m_fallbackClipSet(NULL)
{
}

fwClipSet::fwClipSet(const fwMvClipSetId &fallbackId, const atHashString &clipDictionaryName)
: m_fallbackId(fallbackId)
, m_clipDictionaryName(clipDictionaryName)
, m_clipDictionaryIdx(-1)
, m_fallbackClipSet(NULL)
{
}

void fwClipSet::SetFallbackId(const fwMvClipSetId &fallbackId) 
{ 
	m_fallbackId = fallbackId;
	if(m_fallbackId != CLIP_SET_ID_INVALID)
	{
		m_fallbackClipSet = fwClipSetManager::GetInstance().GetClipSet(m_fallbackId);
	}
    else
    {
        m_fallbackClipSet = NULL;
    }
}

fwClipItem *fwClipSet::GetClipItemByIndex(int clipItemIndex) const
{
	animAssert(clipItemIndex >= 0 && clipItemIndex < m_clipItems.GetCount());
	if(clipItemIndex >= 0 && clipItemIndex < m_clipItems.GetCount())
	{
		atBinaryMap< fwClipItem *, fwMvClipId > &clipItems = const_cast< atBinaryMap< fwClipItem *, fwMvClipId > & >(m_clipItems);

		fwClipItem **ppClipItem = clipItems.GetItem(clipItemIndex);
		if(ppClipItem)
		{
			return *ppClipItem;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwClipItem *fwClipSet::GetClipItem(const fwMvClipId &clipId, bool orFallbackIfExists /* = true */) const
{
	animAssert(clipId != CLIP_ID_INVALID);
	if(clipId != CLIP_ID_INVALID)
	{
		atBinaryMap< fwClipItem *, fwMvClipId > &clipItems = const_cast< atBinaryMap< fwClipItem *, fwMvClipId > & >(m_clipItems);

		fwClipItem **ppClipItem = clipItems.SafeGet(clipId);
		if(ppClipItem)
		{
			return *ppClipItem;
		}

		if(orFallbackIfExists)
		{
			fwClipSet *pFallbackSet = GetFallbackSet();
			if(pFallbackSet)
			{
				return pFallbackSet->GetClipItem(clipId, orFallbackIfExists);
			}
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

strLocalIndex fwClipSet::GetClipDictionaryIndex() const
{
	if(m_clipDictionaryIdx != -1) // used cached index if available
	{
		animAssert(g_ClipDictionaryStore.FindSlotFromHashKey(m_clipDictionaryName.GetHash()) == m_clipDictionaryIdx);
		return strLocalIndex(m_clipDictionaryIdx);
	}
	m_clipDictionaryIdx = g_ClipDictionaryStore.FindSlotFromHashKey(m_clipDictionaryName.GetHash()).Get();
	animAssertf(m_clipDictionaryIdx != -1, "Clip dictionary %s %u has an invalid slot -1!", m_clipDictionaryName.GetCStr() ? m_clipDictionaryName.GetCStr() : "(null)", m_clipDictionaryName.GetHash());
	return strLocalIndex(m_clipDictionaryIdx);
}

//////////////////////////////////////////////////////////////////////////

crClipDictionary *fwClipSet::GetClipDictionary(bool ASSERT_ONLY(assertIfNotStreamed)) const
{
	int clipDictionaryIndex = GetClipDictionaryIndex().Get();
	animAssertf(clipDictionaryIndex != -1, "Clip dictionary %s %u has an invalid slot -1!", m_clipDictionaryName.GetCStr() ? m_clipDictionaryName.GetCStr() : "(null)", m_clipDictionaryName.GetHash());

	fwClipDictionaryLoader loader(clipDictionaryIndex, false);

#if __ASSERT
	if(clipDictionaryIndex != -1 && !loader.IsLoaded())
	{
		animAssertf(loader.IsValid(), "Clip dictionary %s %u has an invalid slot %i!", m_clipDictionaryName.GetCStr() ? m_clipDictionaryName.GetCStr() : "(null)", m_clipDictionaryName.GetHash(), clipDictionaryIndex);
		animAssertf(!assertIfNotStreamed || loader.IsValid(), "Clip dictionary %s %u at index %i is not loaded!", m_clipDictionaryName.GetCStr() ? m_clipDictionaryName.GetCStr() : "(null)", m_clipDictionaryName.GetHash(), clipDictionaryIndex);
	}
#endif // __ASSERT

	return loader.GetDictionary();
}

//////////////////////////////////////////////////////////////////////////

fwClipDictionaryMetadata *fwClipSet::GetClipDictionaryMetadata() const
{
	if(m_clipDictionaryName.IsNotNull())
	{
		return fwClipSetManager::GetClipDictionaryMetadata(m_clipDictionaryName);
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

u32 fwClipSet::GetClipDictionaryStreamingPolicy() const
{
	fwClipDictionaryMetadata *pClipDictionaryMetadata = GetClipDictionaryMetadata();
	if(pClipDictionaryMetadata)
	{
		return pClipDictionaryMetadata->GetStreamingPolicy();
	}

	return SP_STREAMING;
}

//////////////////////////////////////////////////////////////////////////

fwMvClipId fwClipSet::GetClipItemIdByIndex(int clipItemIndex) const
{
	animAssert(clipItemIndex >= 0 && clipItemIndex < m_clipItems.GetCount());
	if(clipItemIndex >= 0 && clipItemIndex < m_clipItems.GetCount())
	{
		atBinaryMap< fwClipItem *, fwMvClipId > &clipItems = const_cast< atBinaryMap< fwClipItem *, fwMvClipId > & >(m_clipItems);

		return clipItems.GetRawDataArray()[clipItemIndex].key;
	}

	return CLIP_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

crClip *fwClipSet::GetClip(const fwMvClipId &clipId, u32 requestOptions /* = CRO_FALLBACK */) const
{
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipId != CLIP_ID_INVALID)
	{
		if(m_clipItems.GetCount() == 0)
		{
			crClipDictionary *pClipDictionary = GetClipDictionary(!(requestOptions&CRO_FALLBACK_IF_NOT_STREAMED));
			if(pClipDictionary)
			{
				crClip *pClip = pClipDictionary->GetClip(clipId);
				if(pClip)
				{
					return pClip;
				}
			}
		}
		else
		{
			atBinaryMap< fwClipItem *, fwMvClipId > &clipItems = const_cast< atBinaryMap< fwClipItem *, fwMvClipId > & >(m_clipItems);

			fwClipItem **ppClipItem = clipItems.SafeGet(clipId);
			if(ppClipItem)
			{
				crClipDictionary *pClipDictionary = GetClipDictionary(!(requestOptions&CRO_FALLBACK_IF_NOT_STREAMED));
				if(pClipDictionary)
				{
					crClip *pClip = pClipDictionary->GetClip(clipId);
					if(pClip)
					{
						return pClip;
					}
				}
			}
		}

		if(requestOptions & CRO_FALLBACK)
		{
			fwClipSet *pFallbackSet = GetFallbackSet();
			if(pFallbackSet)
			{
				return pFallbackSet->GetClip(clipId, requestOptions);
			}
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSet::PickRandomClip(fwMvClipId& clipIdOut)
{
	atBinaryMap<fwClipItem*, fwMvClipId > allItems;

	BuildAvailableClipItemMap(allItems);

	allItems.FinishInsertion();

	s32 numItems = allItems.GetCount();

	if (numItems>0)
	{
		s32 item = fwRandom::GetRandomNumberInRange(0, numItems);
		item = Clamp(item, 0, numItems-1);
		fwMvClipId* pSelected = allItems.GetKey(item);
		if (pSelected)
		{
			clipIdOut = *pSelected;
			return true;
		}
	}
	else
	{
		crClipDictionary* pDict = GetClipDictionary();

		const u32 uNumClips = pDict->GetNumClips();
		s32 item = fwRandom::GetRandomNumberInRange(0, uNumClips);
		clipIdOut = fwMvClipId(pDict->FindKeyByIndex(item));
		return true;
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::BuildAvailableClipItemMap(atBinaryMap<fwClipItem*, fwMvClipId >& map)
{
	bool isDictionaryStreamed = false;
	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
		if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
		{
			isDictionaryStreamed = true;
		}
	}

	if(isDictionaryStreamed)
	{
		for (s32 i=0; i<m_clipItems.GetCount(); i++)
		{
			fwMvClipId* pKey = m_clipItems.GetKey(i);
			fwClipItem** ppItem = m_clipItems.GetItem(i);

			if (pKey && ppItem)
			{
				map.SafeInsert(*pKey, *ppItem);
			}
		}
	}

	// add any items from the fallback set (And its fallbacks)
	fwClipSet* pFallbackSet = GetFallbackSet();
	if (pFallbackSet)
	{
		pFallbackSet->BuildAvailableClipItemMap(map);
	}
}

//////////////////////////////////////////////////////////////////////////

const fwClipSet *fwClipSet::GetClipClipSet(const fwMvClipId &clipId, u32 requestOptions /* = CRO_FALLBACK */) const
{
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipId != CLIP_ID_INVALID)
	{
		if(m_clipItems.GetCount() == 0)
		{
			crClipDictionary *pClipDictionary = GetClipDictionary(!(requestOptions&CRO_FALLBACK_IF_NOT_STREAMED));
			if(pClipDictionary)
			{
				crClip *pClip = pClipDictionary->GetClip(clipId);
				if(pClip)
				{
					return this;
				}
			}
		}
		else
		{
			atBinaryMap< fwClipItem *, fwMvClipId > &clipItems = const_cast< atBinaryMap< fwClipItem *, fwMvClipId > & >(m_clipItems);

			fwClipItem **ppClipItem = clipItems.SafeGet(clipId);
			if(ppClipItem)
			{
				crClipDictionary *pClipDictionary = GetClipDictionary(!(requestOptions&CRO_FALLBACK_IF_NOT_STREAMED));
				if(pClipDictionary)
				{
					crClip *pClip = pClipDictionary->GetClip(clipId);
					if(pClip)
					{
						return this;
					}
				}
			}
		}

		if(requestOptions & CRO_FALLBACK)
		{
			fwClipSet *pFallbackSet = GetFallbackSet();
			if(pFallbackSet)
			{
				return pFallbackSet->GetClipClipSet(clipId, requestOptions);
			}
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::Shutdown()
{
	m_fallbackId.Clear();

	for(int i = 0; i < m_clipItems.GetCount(); i ++)
	{
		delete m_clipItems.GetRawDataArray()[i].data; m_clipItems.GetRawDataArray()[i].data = NULL;
	}
	m_clipItems.Reset();

	m_properties.Reset();
}

//////////////////////////////////////////////////////////////////////////

#if __BANK

bool fwClipSet::StreamIn_DEPRECATED()
{
	bool allDictionariesLoaded = true;

	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
		if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
		{
		}
		else
		{
			if(strStreamingEngine::GetInfo().IsObjectInImage(streamingIndex))
			{
				strStreamingEngine::GetInfo().RequestObject(streamingIndex, STRFLAG_FORCE_LOAD);
				strStreamingEngine::GetLoader().LoadAllRequestedObjects(false);
			}

			if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() != STRINFO_LOADED)
			{
				allDictionariesLoaded = false;
			}
		}
	}
	else
	{
		allDictionariesLoaded = false;
	}

	if(allDictionariesLoaded)
	{
		if(m_fallbackId.IsNotNull())
		{
			fwClipSet *pFallbackSet = GetFallbackSet();
			if(pFallbackSet)
			{
				allDictionariesLoaded = pFallbackSet->StreamIn_DEPRECATED();
			}
		}
	}

	return allDictionariesLoaded;
}

#endif //__BANK

//////////////////////////////////////////////////////////////////////////

bool fwClipSet::Request_DEPRECATED(s32 iFlags)
{
	bool allDictionariesLoaded = true;

	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
		if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
		{
		}
		else
		{
			if(strStreamingEngine::GetInfo().IsObjectInImage(streamingIndex))
			{
				strStreamingEngine::GetInfo().RequestObject(streamingIndex, iFlags);
			}

			if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() != STRINFO_LOADED)
			{
				allDictionariesLoaded = false;
			}
		}
	}
	else
	{
		allDictionariesLoaded = false;
	}

	if(m_fallbackId.IsNotNull())
	{
		fwClipSet *pFallbackSet = GetFallbackSet();
		if(pFallbackSet)
		{
			allDictionariesLoaded &= pFallbackSet->Request_DEPRECATED(iFlags);
		}
	}

	return allDictionariesLoaded;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::ClearRequiredFlag_DEPRECATED(s32 iFlags)
{
	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		//Grab the streaming module id.
		s32 iModuleId = fwAnimManager::GetStreamingModuleId();

		//Grab the streaming index.
		strStreamingModule* pStreamingModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(iModuleId);
		pStreamingModule->ClearRequiredFlag(clipDictionaryIndex.Get(), STRFLAG_DONTDELETE);
	}

	if(m_fallbackId.IsNotNull())
	{
		fwClipSet *pFallbackSet = GetFallbackSet();
		if(pFallbackSet)
		{
			pFallbackSet->ClearRequiredFlag_DEPRECATED(iFlags);
		}
	}
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSet::IsStreamedIn_DEPRECATED() const
{
	bool allDictionariesLoaded = true;

	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
		if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() != STRINFO_LOADED)
		{
			allDictionariesLoaded = false;
		}
	}
	else
	{
		allDictionariesLoaded = false;
	}

	if(allDictionariesLoaded)
	{
		if(m_fallbackId.IsNotNull())
		{
			fwClipSet *pFallbackSet = GetFallbackSet();
			if(pFallbackSet)
			{
				allDictionariesLoaded = pFallbackSet->IsStreamedIn_DEPRECATED();
			}
		}
	}

	return allDictionariesLoaded;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::AddRef_DEPRECATED()
{
	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		fwAnimManager::AddRef(clipDictionaryIndex);
	}

	if(m_fallbackId.IsNotNull())
	{
		fwClipSet *pFallbackSet = GetFallbackSet();
		if(pFallbackSet)
		{
			pFallbackSet->AddRef_DEPRECATED();
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::Release_DEPRECATED()
{
	strLocalIndex clipDictionaryIndex = strLocalIndex(GetClipDictionaryIndex());
	if(clipDictionaryIndex != -1)
	{
		fwAnimManager::RemoveRefWithoutDelete(clipDictionaryIndex);
	}

	if(m_fallbackId.IsNotNull())
	{
		fwClipSet *pFallbackSet = GetFallbackSet();
		if(pFallbackSet)
		{
			pFallbackSet->Release_DEPRECATED();
		}
	}
}

//////////////////////////////////////////////////////////////////////////


fwClipItem *fwClipSet::CreateClipItem(const fwMvClipId &clipId)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipId != CLIP_ID_INVALID)
	{
		fwClipItem *pClipItem = GetClipItem(clipId, false);
		animAssertf(!pClipItem, "clipId %s %u already exists!", clipId.GetCStr(), clipId.GetHash());
		if(!pClipItem)
		{
			pClipItem = rage_new fwClipItem;

			m_clipItems.Insert(clipId, pClipItem);

			m_clipItems.FinishInsertion();

			fwClipItem **ppClipItem = m_clipItems.SafeGet(clipId);
			if(ppClipItem && *ppClipItem == pClipItem)
			{
				return *ppClipItem;
			}

			animAssert(false);
			delete pClipItem; pClipItem = NULL;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////


fwClipItemWithProps *fwClipSet::CreateClipItemWithProps(const fwMvClipId &clipId, u32 animFlags /* = 0 */, eAnimPriority priority /* = AP_MEDIUM */, const atHashString &boneMask /* = atHashString::Null() */)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipId != CLIP_ID_INVALID)
	{
		fwClipItem *pClipItem = GetClipItem(clipId, false);
		animAssertf(!pClipItem, "clipId %s %u already exists!", clipId.GetCStr(), clipId.GetHash());
		if(!pClipItem)
		{
			fwClipItemWithProps *pClipItemWithProps = rage_new fwClipItemWithProps(animFlags, priority, boneMask);

			m_clipItems.Insert(clipId, pClipItemWithProps);

			m_clipItems.FinishInsertion();

			fwClipItem **ppClipItem = m_clipItems.SafeGet(clipId);
			if(ppClipItem && *ppClipItem == pClipItemWithProps)
			{
				return pClipItemWithProps;
			}

			animAssert(false);
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSet::DeleteClipItem(const fwMvClipId &clipId)
{
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipId != CLIP_ID_INVALID)
	{
		for(int clipItemIndex = 0, clipItemCount = m_clipItems.GetCount(); clipItemIndex < clipItemCount; clipItemIndex ++)
		{
			if(m_clipItems.GetRawDataArray()[clipItemIndex].key == clipId)
			{
				delete m_clipItems.GetRawDataArray()[clipItemIndex].data; m_clipItems.GetRawDataArray()[clipItemIndex].data = NULL;

				m_clipItems.Remove(clipItemIndex);

				return true;
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::DeleteAllClipItems()
{
	for(int i = 0; i < m_clipItems.GetCount(); i ++)
	{
		delete m_clipItems.GetRawDataArray()[i].data; m_clipItems.GetRawDataArray()[i].data = NULL;
	}
	m_clipItems.Reset();
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSet::CreateMoveNetworkFlag(const atHashString &moveNetworkFlag)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	animAssert(moveNetworkFlag.IsNotNull());
	if(moveNetworkFlag.IsNotNull())
	{
		int index = m_moveNetworkFlags.Find(moveNetworkFlag);
		animAssert(index == -1);
		if(index == -1)
		{
			m_moveNetworkFlags.PushAndGrow(moveNetworkFlag);

			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSet::DeleteMoveNetworkFlag(const atHashString &moveNetworkFlag)
{
	animAssertf(moveNetworkFlag.IsNotNull(), "moveNetworkFlag is null!");
	if(moveNetworkFlag.IsNotNull())
	{
		int index = m_moveNetworkFlags.Find(moveNetworkFlag);
		animAssert(index != -1);
		if(index != -1)
		{
			m_moveNetworkFlags.Delete(index);

			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSet::DeleteAllMoveNetworkFlags()
{
	m_moveNetworkFlags.Reset();
}

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSet::AddWidgets(bkBank *pBank)
{
	pBank->AddText("Dictionary", const_cast< char * >(m_clipDictionaryName.GetCStr()), istrlen(m_clipDictionaryName.GetCStr()) + 1, true);

	atArray< atBinaryMap< fwClipItem *, fwMvClipId >::DataPair > &rawDataArray = m_clipItems.GetRawDataArray();

	for(int clipItemIndex = 0, clipItemCount = m_clipItems.GetCount(); clipItemIndex < clipItemCount; clipItemIndex ++)
	{
		fwClipItem *pClipItem = rawDataArray[clipItemIndex].data;
		if(pClipItem->IsClipItemWithProps())
		{
			fwClipItemWithProps *pClipItemWithProps = static_cast< fwClipItemWithProps * >(pClipItem);

			fwMvClipId clipId = rawDataArray[clipItemIndex].key;
			pBank->PushGroup(clipId.GetCStr(), false);
			{
				pBank->AddText("Anim flags", (int *)&pClipItemWithProps->GetFlags(), true);
			}
			pBank->PopGroup();
		}
	}
}

#endif //__BANK

//////////////////////////////////////////////////////////////////////////

bool fwClipVariationSet::PickRandomClip(fwMvClipId& clipIdOut) const
{
	const u32 uNumClips = m_clips.GetCount();
	if (uNumClips == 0)
	{
		return false;
	}

	s32 item = fwRandom::GetRandomNumberInRange(0, uNumClips);
	clipIdOut = m_clips[item];
	return true;
}

//////////////////////////////////////////////////////////////////////////
fwClipSetManager fwClipSetManager::ms_instance;

bool fwClipSetManager::ms_bIsPatchingClipSets = false;

#if __ASSERT
bool fwClipSetManager::m_bStartNetworkSessionCalled = false;
#endif // __ASSERT

#if __BANK

atArray< const char * > fwClipSetManager::ms_clipSetNames;
atArray< const char * > fwClipSetManager::ms_clipDictionaryNames;
atArray< const char * > fwClipSetManager::ms_clipNames;
atArray< const char * > fwClipSetManager::ms_memoryGroupNames;

fwDebugBank *fwClipSetManager::m_pBank;
bkGroup *fwClipSetManager::m_pClipGroup;
bkGroup *fwClipSetManager::m_pClipSetGroup;
bkCombo *fwClipSetManager::m_pClipSetList;

int fwClipSetManager::ms_selectedClipSet;
int fwClipSetManager::ms_selectedClipDictionary;
int fwClipSetManager::ms_selectedClip;

char fwClipSetManager::m_deleteClipName[MAX_CLIP_SET_NAME_STRING_LENGTH];
char fwClipSetManager::m_addDeleteClipSetName[MAX_CLIP_SET_NAME_STRING_LENGTH];

int fwClipSetManager::ms_selectedFallbackClipSet;
bkCombo *fwClipSetManager::m_pFallbackSetList;

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

fwClipSet *fwClipSetManager::GetClipSetByIndex(int clipSetIndex)
{
	animAssertf(clipSetIndex >= 0 && clipSetIndex < ms_instance.m_clipSets.GetCount(), "clipSetIndex %i is invalid!", clipSetIndex);
	if(clipSetIndex >= 0 && clipSetIndex < ms_instance.m_clipSets.GetCount())
	{
		fwClipSet **ppClipSet = ms_instance.m_clipSets.GetItem(clipSetIndex);
		if(ppClipSet)
		{
			return *ppClipSet;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwClipSet *fwClipSetManager::GetClipSet(const fwMvClipSetId &clipSetId)
{
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet **ppClipSet = ms_instance.m_clipSets.SafeGet(clipSetId);
		if(ppClipSet)
		{
			return *ppClipSet;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwClipVariationSet* fwClipSetManager::GetClipVariationSetByIndex(int clipVariationSetIndex)
{
	animAssertf(clipVariationSetIndex >= 0 && clipVariationSetIndex < ms_instance.m_clipVariationSets.GetCount(), "clipVariationSetIndex %i is invalid!", clipVariationSetIndex);
	if(clipVariationSetIndex >= 0 && clipVariationSetIndex < ms_instance.m_clipVariationSets.GetCount())
	{
		fwClipVariationSet **ppClipVariationSet = ms_instance.m_clipVariationSets.GetItem(clipVariationSetIndex);
		if(ppClipVariationSet)
		{
			return *ppClipVariationSet;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwClipVariationSet* fwClipSetManager::GetClipVariationSet(const fwMvClipVariationSetId &clipVariationSetId)
{
	if(clipVariationSetId != CLIP_VARIATION_SET_ID_INVALID)
	{
		fwClipVariationSet **ppClipVariationSet = ms_instance.m_clipVariationSets.SafeGet(clipVariationSetId);
		if(ppClipVariationSet)
		{
			return *ppClipVariationSet;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwClipDictionaryMetadata *fwClipSetManager::GetClipDictionaryMetadata(const atHashString &clipDictionaryName)
{
	animAssertf(clipDictionaryName.IsNotNull(), "clipDictionaryName is invalid!");
	if(clipDictionaryName.IsNotNull())
	{
		fwClipDictionaryMetadata *pClipDictionaryMetadata = ms_instance.m_clipDictionaryMetadatas.SafeGet(clipDictionaryName);
		if(pClipDictionaryMetadata)
		{
			return pClipDictionaryMetadata;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwMemoryGroupMetadata *fwClipSetManager::GetMemoryGroupMetadata(const atHashString &memoryGroupName)
{
	animAssertf(memoryGroupName.IsNotNull(), "memoryGroupName is invalid!");
	if(memoryGroupName.IsNotNull())
	{
		fwMemoryGroupMetadata *pMemoryGroupMetadata = ms_instance.m_memoryGroupMetadatas.SafeGet(memoryGroupName);
		if(pMemoryGroupMetadata)
		{
			return pMemoryGroupMetadata;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwMemoryGroupMetadata *fwClipSetManager::GetMemoryGroupMetadataByIndex(s32 index)
{
	atBinaryMap< fwMemoryGroupMetadata, atHashString > &memoryGroupMetadatas = const_cast< atBinaryMap< fwMemoryGroupMetadata, atHashString > & >(ms_instance.m_memoryGroupMetadatas);
	animAssert(index >= 0 && index < memoryGroupMetadatas.GetCount()); 
	if(index >= 0 && index <  memoryGroupMetadatas.GetCount())
	{
		atBinaryMap< fwMemoryGroupMetadata, atHashString >::DataPair &pair = memoryGroupMetadatas.GetRawDataArray()[index];
		//const atHashString &clipDictionaryMetadataName = pair.key;
		fwMemoryGroupMetadata &memoryGroupMetadata = const_cast<fwMemoryGroupMetadata&>(pair.data);
		return &memoryGroupMetadata;
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

atHashString *fwClipSetManager::GetMemoryGroupNameByIndex(s32 index)
{
	atBinaryMap< fwMemoryGroupMetadata, atHashString > &memoryGroupMetadatas = const_cast< atBinaryMap< fwMemoryGroupMetadata, atHashString > & >(ms_instance.m_memoryGroupMetadatas);
	animAssert(index >= 0 && index < memoryGroupMetadatas.GetCount()); 
	if(index >= 0 && index <  memoryGroupMetadatas.GetCount())
	{
		atBinaryMap< fwMemoryGroupMetadata, atHashString >::DataPair &pair = memoryGroupMetadatas.GetRawDataArray()[index];
		//const atHashString &memoryGroupMetadataName = pair.key;
		atHashString &memoryGroupMetadataName = const_cast<atHashString&>(pair.key);
		return &memoryGroupMetadataName;
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

fwMvClipSetId fwClipSetManager::GetClipSetIdByIndex(int clipSetIndex)
{
	animAssert(clipSetIndex >= 0 && clipSetIndex < ms_instance.m_clipSets.GetCount());
	if(clipSetIndex >= 0 && clipSetIndex < ms_instance.m_clipSets.GetCount())
	{
		return ms_instance.m_clipSets.GetRawDataArray()[clipSetIndex].key;
	}

	return CLIP_SET_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

fwMvClipSetId fwClipSetManager::GetClipSetId(const char *clipSetName)
{
	fwMvClipSetId clipSetId(clipSetName);

	if(GetClipSet(clipSetId))
	{
		return clipSetId;
	}

	return CLIP_SET_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

fwMvClipSetId fwClipSetManager::GetClipSetId(const atHashString& clipSetName)
{
	fwMvClipSetId clipSetId(clipSetName);

	if(GetClipSet(clipSetId))
	{
		return clipSetId;
	}

	return CLIP_SET_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

fwClipItem *fwClipSetManager::GetClipItem(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, bool orFallbackIfExists /* = true */)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID && clipId != CLIP_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		if(pClipSet)
		{
			fwClipItem *pClipItem = pClipSet->GetClipItem(clipId, orFallbackIfExists);
			if(pClipItem)
			{
				return pClipItem;
			}
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

crClip *fwClipSetManager::GetClip(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, bool orFallbackIfExists /* = true */)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID && clipId != CLIP_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		if(pClipSet)
		{
			crClip *pClip = pClipSet->GetClip(clipId, orFallbackIfExists);
			if(pClip)
			{
				return pClip;
			}
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::PickRandomClip(const fwMvClipSetId& clipSetId, fwMvClipId& clipIdOut)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		if (pClipSet)
		{
			return pClipSet->PickRandomClip(clipIdOut);
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::GetClipDictionaryNameAndClipName( const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, atHashString &clipDictName, atHashString &clipName, bool orFallbackIfExists /*= true*/ )
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID && clipId != CLIP_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			fwClipItem *pClipItem = NULL;
			if(pClipSet->GetClipItemCount() > 0)
			{
				pClipItem = pClipSet->GetClipItem(clipId, false);
			}

			if(pClipSet->GetClipItemCount() == 0 || pClipItem)
			{
				crClipDictionary *pClipDictionary = pClipSet->GetClipDictionary();
				animAssertf(pClipDictionary, "Could not get clip dictionary!");
				if(pClipDictionary)
				{
					crClip *pClip = pClipDictionary->GetClip(clipId);
					if(pClip)
					{
						clipDictName = pClipSet->GetClipDictionaryName();

						clipName = clipId;

						return true;
					}
				}
			}

			if(orFallbackIfExists)
			{
				fwMvClipSetId fallbackClipSetId = pClipSet->GetFallbackId();
				if(fallbackClipSetId)
				{
					return GetClipDictionaryNameAndClipName(fallbackClipSetId, clipId, clipDictName, clipName, orFallbackIfExists);
				}
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

s32 fwClipSetManager::GetClipDictionaryIndex(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			return pClipSet->GetClipDictionaryIndex().Get();
		}
	}

	return -1;
}

//////////////////////////////////////////////////////////////////////////

s32 fwClipSetManager::GetClipDictionaryIndex(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	animAssertf(clipId != CLIP_ID_INVALID, "clipId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID && clipId != CLIP_ID_INVALID)
	{
		const fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			pClipSet = pClipSet->GetClipClipSet(clipId);
			if(pClipSet)
			{
				return pClipSet->GetClipDictionaryIndex().Get();
			}
		}
	}

	return -1;
}

//////////////////////////////////////////////////////////////////////////

u32 fwClipSetManager::GetClipDictionaryStreamingPolicy(const atHashString &clipDictionaryName)
{
	animAssertf(clipDictionaryName.IsNotNull(), "clipDictionaryName is invalid!");
	if(clipDictionaryName.IsNotNull())
	{
		fwClipDictionaryMetadata *pClipDictionaryMetadata = GetClipDictionaryMetadata(clipDictionaryName);
		animAssertf(pClipDictionaryMetadata, "Could not get clip dictionary metadata!");
		if(pClipDictionaryMetadata)
		{
			return pClipDictionaryMetadata->GetStreamingPolicy();
		}
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::Init(u32 initMode)
{
	if(initMode == INIT_CORE)
	{
		GetInstance().Load();
#if __BANK
		RebuildClipSetNames();
#endif // __BANK

		// RESIDENCY STATE - Nothing resident
		GetInstance().m_eResidencyState = CSM_RESIDENCYSTATE_NONE;
	}
	else if(initMode == INIT_AFTER_MAP_LOADED)
	{
		PF_START_STARTUPBAR("ClipSetMgr: Request metadata");
		// Iterate over each clip dictionary metadata
		for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
		{
			atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
			const atHashString &clipDictionaryMetadataName = pair.key;
			const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;

			if(clipDictionaryMetadata.GetStreamingPolicy() & SP_SINGLEPLAYER_RESIDENT)
			{
				strLocalIndex clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName);
				animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
				if(clipDictionaryIndex != -1)
				{
					g_ClipDictionaryStore.StreamingRequest(clipDictionaryIndex, STRFLAG_DONTDELETE | STRFLAG_PRIORITY_LOAD);
					//Printf("SinglePlayer Init - StreamingRequest (%s)\n", clipDictionaryMetadataName.TryGetCStr());
				}
			}
		}

		PF_START_STARTUPBAR("ClipSetMgr: Load all requests");
		// Load all the requested clip dictionaries
		strStreamingEngine::GetLoader().LoadAllRequestedObjects(false);

		PF_START_STARTUPBAR("ClipSetMgr: Parse metadata");
		// Iterate over each clip dictionary metadata
		for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
		{
			atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
			const atHashString &clipDictionaryMetadataName = pair.key;
			const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;

			// Check clip dictionary is supposed to be resident
			if(clipDictionaryMetadata.GetStreamingPolicy() & SP_SINGLEPLAYER_RESIDENT)
			{
				// Find clip dictionary slot streamingIndex
				strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
				animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
				if(clipDictionaryIndex != -1)
				{
					// Check clip dictionary has loaded
					strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
					if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
					{
						fwAnimManager::AddRef(clipDictionaryIndex);
						strStreamingEngine::GetInfo().SetObjectIsDeletable(streamingIndex);
						//Printf("SinglePlayer Init - AddRef_DEPRECATED (%s)\n", clipDictionaryMetadataName.TryGetCStr());
						g_strStreamingInterface->AddResidentObject(streamingIndex);
					}
				}
			}
		}

#if __BANK
		if(PARAM_validateclipsets.Get())
		{
			ValidateClipSets();
		}
#endif

		// RESIDENCY STATE - Singleplayer-resident clipsets are now resident
		GetInstance().m_eResidencyState = CSM_RESIDENCYSTATE_SINGLEPLAYER;
	}
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::Shutdown(u32 shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		GetInstance().DeleteAllClipSets();
		GetInstance().DeleteAllClipDictionaryMetadatas();

#if __BANK
		ms_clipSetNames.clear();
		ms_memoryGroupNames.clear();
#endif //__BANK

	}
	else if(shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
	{		
		// Iterate over each clip dictionary metadata
		for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
		{
			atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
			const atHashString &clipDictionaryMetadataName = pair.key;
			const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;

			// Check clip dictionary is supposed to be resident
			if(clipDictionaryMetadata.GetStreamingPolicy() & SP_SINGLEPLAYER_RESIDENT)
			{
				// Find clip dictionary slot streamingIndex
				strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
				animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
				if(clipDictionaryIndex != -1)
				{
 					strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
 					g_strStreamingInterface->RemoveResidentObject(streamingIndex);

					animAssert(fwAnimManager::GetNumRefs(clipDictionaryIndex)>0);
					fwAnimManager::RemoveRef(clipDictionaryIndex);
					//Printf("SinglePlayer Shutdown - RemoveRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
				}
			}
		}

		// Clear *ALL* clip dictionary STRFLAG_DONTDELETE streaming flags
		for(int iClipSetIndex = 0; iClipSetIndex < ms_instance.m_clipSets.GetCount(); iClipSetIndex ++)
		{
			fwClipSet *pClipSet = ms_instance.m_clipSets.GetRawDataArray()[iClipSetIndex].data;
			if(pClipSet)
			{
				u32 uClipDictionaryHashKey = pClipSet->GetClipDictionaryName().GetHash();
				if(uClipDictionaryHashKey != 0)
				{
					strLocalIndex iClipDictionarySlot = strLocalIndex(g_ClipDictionaryStore.FindSlotFromHashKey(uClipDictionaryHashKey));
					if(g_ClipDictionaryStore.IsValidSlot(iClipDictionarySlot))
					{
						g_ClipDictionaryStore.ClearRequiredFlag(iClipDictionarySlot.Get(), STRFLAG_DONTDELETE);
					}
				}
			}
		}

		// RESIDENCY STATE - Nothing resident
		GetInstance().m_eResidencyState = CSM_RESIDENCYSTATE_NONE;
	}
	else if(shutdownMode == SHUTDOWN_SESSION)
	{
		// Clear *ALL* clip dictionary STRFLAG_DONTDELETE streaming flags
		for(int iClipSetIndex = 0; iClipSetIndex < ms_instance.m_clipSets.GetCount(); iClipSetIndex ++)
		{
			fwClipSet *pClipSet = ms_instance.m_clipSets.GetRawDataArray()[iClipSetIndex].data;
			if(pClipSet)
			{
				u32 uClipDictionaryHashKey = pClipSet->GetClipDictionaryName().GetHash();
				if(uClipDictionaryHashKey != 0)
				{
					strLocalIndex iClipDictionarySlot = strLocalIndex(g_ClipDictionaryStore.FindSlotFromHashKey(uClipDictionaryHashKey));
					if(g_ClipDictionaryStore.IsValidSlot(iClipDictionarySlot))
					{
						g_ClipDictionaryStore.ClearRequiredFlag(iClipDictionarySlot.Get(), STRFLAG_DONTDELETE);
					}
				}
			}
		}

		// Remove patch/DLC added clipsets here.  They will be added again by the Extra Content Manager for each session.
		RemovePatchClipSets();
	}
}

//////////////////////////////////////////////////////////////////////////

#if __BANK

bool fwClipSetManager::StreamIn_DEPRECATED(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			return pClipSet->StreamIn_DEPRECATED();
		}
	}

	return false;
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::Request_DEPRECATED(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			return pClipSet->Request_DEPRECATED();
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::IsStreamedIn_DEPRECATED(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			return pClipSet->IsStreamedIn_DEPRECATED();
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::AddRef_DEPRECATED(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			pClipSet->AddRef_DEPRECATED();
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::Release_DEPRECATED(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(pClipSet, "Could not get clip set %s %u!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(pClipSet)
		{
			pClipSet->Release_DEPRECATED();
		}
	}
}

//////////////////////////////////////////////////////////////////////////

fwClipSet *fwClipSetManager::CreateClipSet(const fwMvClipSetId &clipSetId, const fwMvClipSetId &fallbackSetName, const atHashString &clipDictionaryName)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		fwClipSet *pClipSet = GetClipSet(clipSetId);
		animAssertf(!pClipSet, "Clip set %s %u already exists!", clipSetId.GetCStr(), clipSetId.GetHash());
		if(!pClipSet)
		{
			pClipSet = rage_new fwClipSet(fallbackSetName, clipDictionaryName);

			ms_instance.m_clipSets.Insert(clipSetId, pClipSet);

			ms_instance.m_clipSets.FinishInsertion();

			fwClipSet **ppClipSet = ms_instance.m_clipSets.SafeGet(clipSetId);
			if(ppClipSet)
			{
				return *ppClipSet;
			}

			animAssert(false);
			delete pClipSet; pClipSet = NULL;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::DeleteClipSet(const fwMvClipSetId &clipSetId)
{
	animAssertf(clipSetId != CLIP_SET_ID_INVALID, "clipSetId is invalid!");
	if(clipSetId != CLIP_SET_ID_INVALID)
	{
		for(int clipSetIndex = 0, clipSetCount = ms_instance.m_clipSets.GetCount(); clipSetIndex < clipSetCount; clipSetIndex ++)
		{
			if(ms_instance.m_clipSets.GetRawDataArray()[clipSetIndex].key == clipSetId)
			{
				delete ms_instance.m_clipSets.GetRawDataArray()[clipSetIndex].data; ms_instance.m_clipSets.GetRawDataArray()[clipSetIndex].data = NULL;

				ms_instance.m_clipSets.Remove(clipSetIndex);

				return true;
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::DeleteAllClipSets()
{
	for(int i = 0; i < ms_instance.m_clipSets.GetCount(); i ++)
	{
		delete ms_instance.m_clipSets.GetRawDataArray()[i].data; ms_instance.m_clipSets.GetRawDataArray()[i].data = NULL;
	}
	ms_instance.m_clipSets.Reset();
}

//////////////////////////////////////////////////////////////////////////

fwClipDictionaryMetadata *fwClipSetManager::CreateClipDictionaryMetadata(const atHashString &clipDictionaryName, u32 streamingPolicy /* = MG_NONE */, fwMemoryGroup memoryGroup /* = SP_STREAMING */, eStreamingPriority streamingPriority /* = SP_Medium */)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	animAssertf(clipDictionaryName.IsNotNull(), "clipDictionaryName is invalid!");
	if(clipDictionaryName.IsNotNull())
	{
		fwClipDictionaryMetadata *pClipDictionaryMetadata = GetClipDictionaryMetadata(clipDictionaryName);
		animAssertf(!pClipDictionaryMetadata, "Clip dictionary name already exists!");
		if(!pClipDictionaryMetadata)
		{
			ms_instance.m_clipDictionaryMetadatas.Insert(clipDictionaryName, fwClipDictionaryMetadata(streamingPolicy, memoryGroup, streamingPriority));

			ms_instance.m_clipDictionaryMetadatas.FinishInsertion();

			pClipDictionaryMetadata = ms_instance.m_clipDictionaryMetadatas.SafeGet(clipDictionaryName);

			return pClipDictionaryMetadata;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::DeleteClipDictionaryMetadata(const atHashString &clipDictionaryName)
{
	animAssertf(clipDictionaryName.IsNotNull(), "clipDictionaryName is invalid!");
	if(clipDictionaryName.IsNotNull())
	{
		for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
		{
			if(ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex].key == clipDictionaryName)
			{
				ms_instance.m_clipDictionaryMetadatas.Remove(clipDictionaryMetadataIndex);

				return true;
			}
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::DeleteAllClipDictionaryMetadatas()
{
	ms_instance.m_clipDictionaryMetadatas.Reset();
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::DeleteAllMemoryGroupMetadatas()
{
	ms_instance.m_memoryGroupMetadatas.Reset();
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::DeleteAllMemorySituations()
{
	ms_instance.m_memorySituations.Reset();
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::StartNetworkSession()
{
#if __ASSERT
	animAssertf(!fwClipSetManager::m_bStartNetworkSessionCalled, "Calling StartNetworkSession called twice in a row, without calling EndNetworkSession between them");
	fwClipSetManager::m_bStartNetworkSessionCalled = true;
#endif // __ASSERT

	// Iterate over each clip dictionary metadata
	for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
	{
		atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
		const atHashString &clipDictionaryMetadataName = pair.key;
		const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;
		u32 streamingPolicy = clipDictionaryMetadata.GetStreamingPolicy();

		// Stream out dictionaries that are single-player resident only
		if((streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && !(streamingPolicy & SP_MULTIPLAYER_RESIDENT))
		{
			strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
			animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
			if(clipDictionaryIndex != -1)
			{
				animAssert(fwAnimManager::GetNumRefs(clipDictionaryIndex)>0);
				fwAnimManager::RemoveRef(clipDictionaryIndex);
				//Printf("MultiPlayer Init - RemoveRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
			}
		}
		// Stream in dictionaries that are multi-player resident only
		else if(!(streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && (streamingPolicy & SP_MULTIPLAYER_RESIDENT))
		{
			strLocalIndex clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName);
			animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
			if(clipDictionaryIndex != -1)
			{
				g_ClipDictionaryStore.StreamingRequest(clipDictionaryIndex, STRFLAG_DONTDELETE | STRFLAG_PRIORITY_LOAD);
				//Printf("MultiPlayer Init - StreamingRequest (%s)\n", clipDictionaryMetadataName.TryGetCStr());
			}
		}
	}

	// Load all the requested clip dictionaries
	strStreamingEngine::GetLoader().LoadAllRequestedObjects(false);

	// Iterate over each clip dictionary metadata
	for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
	{
		atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
		const atHashString &clipDictionaryMetadataName = pair.key;
		const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;

		// If its multi-player resident and it hasn't already streamed in for single-player resident
		u32 streamingPolicy = clipDictionaryMetadata.GetStreamingPolicy();
		if(!(streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && (streamingPolicy & SP_MULTIPLAYER_RESIDENT))
		{
			strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
			animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
			if(clipDictionaryIndex != -1)
			{
				strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
				if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
				{
					fwAnimManager::AddRef(clipDictionaryIndex);
					strStreamingEngine::GetInfo().SetObjectIsDeletable(streamingIndex);
					//Printf("MultiPlayer Init - AddRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
				}
			}
		}
	}

	// RESIDENCY STATE - Multiplayer-resident clipsets are now resident
	GetInstance().m_eResidencyState = CSM_RESIDENCYSTATE_MULTIPLAYER;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::EndNetworkSession()
{
#if __ASSERT
	animAssertf(fwClipSetManager::m_bStartNetworkSessionCalled, "Calling EndNetworkSession without calling StartNetworkSession");
	fwClipSetManager::m_bStartNetworkSessionCalled = false; // Reset
#endif // __ASSERT

	// Iterate over each clip dictionary metadata
	for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
	{
		atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
		const atHashString &clipDictionaryMetadataName = pair.key;
		const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;
		u32 streamingPolicy = clipDictionaryMetadata.GetStreamingPolicy();

		// Stream out dictionaries that are multi-player resident only
		if(!(streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && (streamingPolicy & SP_MULTIPLAYER_RESIDENT))
		{
			strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
			animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
			if(clipDictionaryIndex != -1)
			{
				animAssert(fwAnimManager::GetNumRefs(clipDictionaryIndex)>0);
				fwAnimManager::RemoveRef(clipDictionaryIndex);
				Printf("MultiPlayer Shutdown - RemoveRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
			}
		}
		// Stream in dictionaries that are single-player resident only
		else if((streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && !(streamingPolicy & SP_MULTIPLAYER_RESIDENT))
		{
			strLocalIndex clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName);
			animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
			if(clipDictionaryIndex != -1)
			{
				g_ClipDictionaryStore.StreamingRequest(clipDictionaryIndex, STRFLAG_DONTDELETE | STRFLAG_PRIORITY_LOAD);
				//Printf("MultiPlayer Shutdown - StreamingRequest (%s)\n", clipDictionaryMetadataName.TryGetCStr());
			}
		}
	}

	// Load all the requested clip dictionaries
	strStreamingEngine::GetLoader().LoadAllRequestedObjects(false);

	// Iterate over each clip dictionary metadata
	for(int clipDictionaryMetadataIndex = 0, clipDictionaryMetadataCount = ms_instance.m_clipDictionaryMetadatas.GetCount(); clipDictionaryMetadataIndex < clipDictionaryMetadataCount; clipDictionaryMetadataIndex ++)
	{
		atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = ms_instance.m_clipDictionaryMetadatas.GetRawDataArray()[clipDictionaryMetadataIndex];
		const atHashString &clipDictionaryMetadataName = pair.key;
		const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;

		// If its multi-player resident and it hasn't already streamed in for single-player resident
		u32 streamingPolicy = clipDictionaryMetadata.GetStreamingPolicy();
		if((streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && !(streamingPolicy & SP_MULTIPLAYER_RESIDENT))
		{
			strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
			animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
			if(clipDictionaryIndex != -1)
			{
				strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
				if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
				{
					fwAnimManager::AddRef(clipDictionaryIndex);
					strStreamingEngine::GetInfo().SetObjectIsDeletable(streamingIndex);
					//Printf("MultiPlayer Shutdown - AddRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
				}
			}
		}
	}

	// RESIDENCY STATE - Singleplayer-resident clipsets are now resident
	GetInstance().m_eResidencyState = CSM_RESIDENCYSTATE_SINGLEPLAYER;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::Load()
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	bool bResult = true;

	GetInstance().DeleteAllClipSets();
	GetInstance().DeleteAllClipDictionaryMetadatas();
	GetInstance().DeleteAllMemoryGroupMetadatas();
	GetInstance().DeleteAllMemorySituations();

	Displayf("Loading clip_sets." META_FILE_EXT "...\n");

	fwPsoStoreLoader loader = fwPsoStoreLoader::MakeSimpleFileLoader<fwClipSetManager>();
		
	atVarString fileName("%s/%s", g_clipSetPlatformDirectory, "clip_sets." META_FILE_EXT);
	fwPsoStoreLoadInstance inst(&GetInstance());
	loader.Load(fileName.c_str(), inst);

	bResult = inst.IsLoaded();

	animAssertf(bResult, "Load clip_sets." META_FILE_EXT " failed!\n");

	Displayf("Loaded clip_sets." META_FILE_EXT ".\n");
	Displayf("ClipSets:%i ClipDictionaryMetadatas:%i MemoryGroupMetadatas:%i MemorySituations:%i",
		GetInstance().GetClipSetCount(),
		GetInstance().GetClipDictionaryMetadataCount(),
		GetInstance().GetMemoryGroupMetadataCount(),
		GetInstance().GetMemorySituationCount());

	// set pointer to fall back clip set
	for(int iClipSetIndex = 0; iClipSetIndex < GetInstance().m_clipSets.GetCount(); iClipSetIndex++)
	{
		fwClipSet *pClipSet = GetInstance().m_clipSets.GetRawDataArray()[iClipSetIndex].data;
		pClipSet->SetFallbackId(pClipSet->GetFallbackId());
	}

	return bResult;
}

#if __BANK
void fwClipSetManager::RebuildClipSetNames()
{
	ms_clipSetNames.Reset();
	for (s32 i=0 ; i<GetInstance().GetClipSetCount(); i++)
	{
		atHashString string( GetInstance().GetClipSetIdByIndex(i) );
		ms_clipSetNames.PushAndGrow(string.TryGetCStr());
	}
	ms_clipSetNames.QSort(0, -1, SortAlphabeticalNoCase);

	ms_memoryGroupNames.Reset();
	for (s32 i=0 ; i<GetInstance().GetMemoryGroupMetadataCount(); i++)
	{
		atHashString* pMemoryGroupName = fwClipSetManager::GetMemoryGroupNameByIndex(i);
		if (pMemoryGroupName)
		{				
			ms_memoryGroupNames.PushAndGrow(pMemoryGroupName->TryGetCStr());
		}
	}
	ms_memoryGroupNames.QSort(0, -1, SortAlphabeticalNoCase);
}
#endif //__BANK

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::PatchClipSets(const char *fname)
{ 
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);
	Displayf("Patch ClipSets...");
	Displayf("Loading %s...\n", fname);

	ms_bIsPatchingClipSets = true;

#if __BANK
	atString name(fname), dummy, patch;
	name.Split(patch, dummy, ':');
#endif

	fwPsoStoreLoader loader = fwPsoStoreLoader::MakeSimpleFileLoader<fwClipSetManager>();
	fwPsoStoreLoadInstance inst;
	loader.Load(fname, inst);

	bool bResult = inst.IsLoaded();
	animAssertf(bResult, "Load %s failed!\n", fname);

	fwClipSetManager& temp_inst = *reinterpret_cast<fwClipSetManager*>(inst.GetInstance());

	Displayf("Loaded %s.\n", fname);
	Displayf("ClipSets:%i ClipDictionaryMetadatas:%i MemoryGroupMetadatas:%i MemorySituations:%i",
		temp_inst.m_clipSets.GetCount(),
		temp_inst.m_clipDictionaryMetadatas.GetCount(),
		temp_inst.m_memoryGroupMetadatas.GetCount(), 
		temp_inst.m_memorySituations.GetCount());

	// We now overwrite or append on this new data.  In the case of memory group metadatas, memory situations
	// and clipsets we can just do this in a straightforward manner first.  Then for clip dictionary metadatas,
	// we must detect changes to existing clipsets (that are being overriden) and then make resident or unload
	// these and any new clipsets according to the current residency state of the clipsetmanager.

	PatchBinMap(GetInstance().m_clipSets, temp_inst.m_clipSets, AppendBinMapFreeObjectPtr<fwClipSet>); 
	PatchBinMap(GetInstance().m_memoryGroupMetadatas, temp_inst.m_memoryGroupMetadatas, AppendBinMapFreeNone<fwMemoryGroupMetadata>);
	PatchBinMap(GetInstance().m_memorySituations, temp_inst.m_memorySituations, AppendBinMapFreeNone<fwMemorySituation>);

	// We only need to do residency checks if the clipset manager has actually made items resident
	if (GetInstance().m_eResidencyState != CSM_RESIDENCYSTATE_NONE)
	{
		atArray<strLocalIndex> clipDictsToStreamIn;

		// For each clip dictionary metadata being added, is it new or existing (and being overriden)
		for(int i = 0; i < temp_inst.m_clipDictionaryMetadatas.GetCount(); i++)
		{
			atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = temp_inst.m_clipDictionaryMetadatas.GetRawDataArray()[i];
			const atHashString &clipDictionaryMetadataName = pair.key;
			const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;
			u32 streamingPolicy = clipDictionaryMetadata.GetStreamingPolicy();

			// Find it
			int iExistingIdx = GetInstance().m_clipDictionaryMetadatas.GetIndex(clipDictionaryMetadataName);

			if (iExistingIdx >= 0)
			{
				// EXISTING - Does it need streaming in or out
				atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &existingPair = GetInstance().m_clipDictionaryMetadatas.GetRawDataArray()[iExistingIdx];
				const fwClipDictionaryMetadata &existingClipDictionaryMetadata = existingPair.data;
				u32 prevStreamingPolicy = existingClipDictionaryMetadata.GetStreamingPolicy();

				bool bWasResident = ((prevStreamingPolicy & SP_SINGLEPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_SINGLEPLAYER) ||
					((prevStreamingPolicy & SP_MULTIPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_MULTIPLAYER);

				bool bNeedsResidency = ((streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_SINGLEPLAYER) ||
					((streamingPolicy & SP_MULTIPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_MULTIPLAYER);

				if (!bWasResident && bNeedsResidency)
				{
					// Needs to made resident
					strLocalIndex clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName);
					animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
					if(clipDictionaryIndex != -1)
					{
						//Printf("PatchClipSets - Making resident (%s)\n", clipDictionaryMetadataName.TryGetCStr());
						clipDictsToStreamIn.PushAndGrow(clipDictionaryIndex);
					}
				}
				else if (bWasResident && !bNeedsResidency)
				{
					// Needs to be unloaded
					strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
					animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
					if(clipDictionaryIndex != -1)
					{
						animAssert(fwAnimManager::GetNumRefs(clipDictionaryIndex)>0);
						fwAnimManager::RemoveRef(clipDictionaryIndex);
						//Printf("PatchClipSets (unloading previously resident clip dictionary) - RemoveRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
					}
				}
			}
			else
			{
				// NEW - Does it need streaming in?
				if ( ((streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_SINGLEPLAYER) ||
					((streamingPolicy & SP_MULTIPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_MULTIPLAYER) )
				{
					strLocalIndex clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName);
					animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
					if(clipDictionaryIndex != -1)
					{
						//Printf("PatchClipSets - Making resident (%s)\n", clipDictionaryMetadataName.TryGetCStr());
						clipDictsToStreamIn.PushAndGrow(clipDictionaryIndex);
					}
				}
			}
		}

		// Load new resident clipsets.
		if (clipDictsToStreamIn.GetCount() > 0)
		{
			Printf("Making %d patched clipsets resident\n", clipDictsToStreamIn.GetCount());
			for (int j = 0; j < clipDictsToStreamIn.GetCount(); j++)
			{
				strLocalIndex clipDictionaryIndex = clipDictsToStreamIn[j];
				g_ClipDictionaryStore.StreamingRequest(clipDictionaryIndex, STRFLAG_DONTDELETE | STRFLAG_PRIORITY_LOAD);
			}

			// Load all the requested clip dictionaries
			strStreamingEngine::GetLoader().LoadAllRequestedObjects(false);

			for (int k = 0; k < clipDictsToStreamIn.GetCount(); k++)
			{
				strLocalIndex clipDictionaryIndex = clipDictsToStreamIn[k];

				// Check clip dictionary has loaded
				strIndex streamingIndex = g_ClipDictionaryStore.GetStreamingIndex(clipDictionaryIndex);
				if(strStreamingEngine::GetInfo().GetStreamingInfo(streamingIndex)->GetStatus() == STRINFO_LOADED)
				{
					fwAnimManager::AddRef(clipDictionaryIndex);
					strStreamingEngine::GetInfo().SetObjectIsDeletable(streamingIndex);
					g_strStreamingInterface->AddResidentObject(streamingIndex);
				}
			}
		}
	}

#if __BANK
	for(int j = 0; j < temp_inst.m_clipDictionaryMetadatas.GetCount(); j++)
	{
		atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = temp_inst.m_clipDictionaryMetadatas.GetRawDataArray()[j];
		strLocalIndex clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(pair.key);

		g_ClipDictionarySource.Delete(clipDictionaryIndex.Get());
		g_ClipDictionarySource[clipDictionaryIndex.Get()] = patch;
	}
#endif

	// We've done our residency work, so now just patch the metadata map
	PatchBinMap(GetInstance().m_clipDictionaryMetadatas, temp_inst.m_clipDictionaryMetadatas, AppendBinMapFreeNone<fwClipDictionaryMetadata>);

	// Set pointer to fall back clip set
	for(int iClipSetIndex = 0; iClipSetIndex < GetInstance().m_clipSets.GetCount(); iClipSetIndex++)
	{
		fwClipSet *pClipSet = GetInstance().m_clipSets.GetRawDataArray()[iClipSetIndex].data;
		pClipSet->SetFallbackId(pClipSet->GetFallbackId());
	}
		 
#if __BANK 
	BuildClipDictionaryNameArray();
	RebuildClipSetNames();
#endif // __BANK

	loader.Unload(inst);

	ms_bIsPatchingClipSets = false;

	Displayf("Patch ClipSets... Done.");

	return bResult;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::RemovePatchClipSets()
{
	// This function removes any clipsets and metadatas that were added to the manager via fwClipSetManager::AddClipSets(...).
	// This effectively reverts the ClipSetManager to a pre-patched/DLC state.  However, to make things more complicated, we
	// also have to unload any resident clipsets that should no longer be (i.e. all new/added clipsets and any clipsets that
	// had their residency changed during the fwClipSetManager::AddClipSets patching.

	USE_MEMBUCKET(MEMBUCKET_ANIMATION);
	Displayf("Removing Patch ClipSets...");

#if __BANK
	g_ClipDictionarySource.Kill();
#endif

	if (GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_NONE)
	{
		// Nothing is resident at the moment.  Simply reset.
		GetInstance().Load();
	}
	else
	{
		Displayf("Loading clip_sets." META_FILE_EXT "...\n");
		atVarString fileName("%s/%s", g_clipSetPlatformDirectory, "clip_sets." META_FILE_EXT);

		fwPsoStoreLoader loader = fwPsoStoreLoader::MakeSimpleFileLoader<fwClipSetManager>();
		fwPsoStoreLoadInstance inst;

		loader.Load(fileName.c_str(), inst);
		animAssertf(inst.IsLoaded(), "Load clip_sets." META_FILE_EXT " failed!\n");

		fwClipSetManager& temp_inst = *reinterpret_cast<fwClipSetManager*>(inst.GetInstance());

		Displayf("Loaded clip_sets." META_FILE_EXT ".\n");
		Displayf("ClipSets:%i ClipDictionaryMetadatas:%i MemoryGroupMetadatas:%i MemorySituations:%i",
			temp_inst.m_clipSets.GetCount(),
			temp_inst.m_clipDictionaryMetadatas.GetCount(),
			temp_inst.m_memoryGroupMetadatas.GetCount(), 
			temp_inst.m_memorySituations.GetCount());

		// Go through all the current clipsets and find ones that aren't in the base clip_sets file.
		for(int i = 0; i < GetInstance().m_clipDictionaryMetadatas.GetCount(); i++)
		{
			atBinaryMap< fwClipDictionaryMetadata, atHashString >::DataPair &pair = GetInstance().m_clipDictionaryMetadatas.GetRawDataArray()[i];
			const atHashString &clipDictionaryMetadataName = pair.key;
			const fwClipDictionaryMetadata &clipDictionaryMetadata = pair.data;
			u32 streamingPolicy = clipDictionaryMetadata.GetStreamingPolicy();

			if (!temp_inst.m_clipDictionaryMetadatas.Has(clipDictionaryMetadataName))
			{
				// This clip dictionary metadata has been added.  Does it need unloading?
				if ( ((streamingPolicy & SP_SINGLEPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_SINGLEPLAYER) ||
				     ((streamingPolicy & SP_MULTIPLAYER_RESIDENT) && GetInstance().m_eResidencyState == CSM_RESIDENCYSTATE_MULTIPLAYER) )
				{
					strLocalIndex clipDictionaryIndex = strLocalIndex(g_ClipDictionaryStore.FindSlot(clipDictionaryMetadataName));
					animAssertf(clipDictionaryIndex != -1, "Could not find slot for clip dictionary %s!", clipDictionaryMetadataName.GetCStr());
					if(clipDictionaryIndex != -1)
					{
						animAssert(fwAnimManager::GetNumRefs(clipDictionaryIndex)>0);
						fwAnimManager::RemoveRef(clipDictionaryIndex);
						//Printf("RemovePatchClipSets - RemoveRef (%s)\n", clipDictionaryMetadataName.TryGetCStr());
					}
				}
			}
		}

		// Delete existing arrays first
		GetInstance().DeleteAllClipSets();
		GetInstance().DeleteAllClipDictionaryMetadatas();
		GetInstance().DeleteAllMemoryGroupMetadatas();
		GetInstance().DeleteAllMemorySituations();

		// Replace all binary maps
		AppendBinMap(GetInstance().m_clipSets, temp_inst.m_clipSets, AppendBinMapFreeObjectPtr<fwClipSet>); 
		AppendBinMap(GetInstance().m_clipDictionaryMetadatas, temp_inst.m_clipDictionaryMetadatas, AppendBinMapFreeNone<fwClipDictionaryMetadata>);
		AppendBinMap(GetInstance().m_memoryGroupMetadatas, temp_inst.m_memoryGroupMetadatas, AppendBinMapFreeNone<fwMemoryGroupMetadata>);
		AppendBinMap(GetInstance().m_memorySituations, temp_inst.m_memorySituations, AppendBinMapFreeNone<fwMemorySituation>);

		// Set pointer to fall back clip set
		for(int iClipSetIndex = 0; iClipSetIndex < GetInstance().m_clipSets.GetCount(); iClipSetIndex++)
		{
			fwClipSet *pClipSet = GetInstance().m_clipSets.GetRawDataArray()[iClipSetIndex].data;
			pClipSet->SetFallbackId(pClipSet->GetFallbackId());
		}

		loader.Unload(inst);
	}

#if __BANK 
	BuildClipDictionaryNameArray();
	RebuildClipSetNames();
#endif // __BANK

	Displayf("Removing Patch ClipSets... Done.");

	return true;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::Script_AddAndRequestClipSet(fwMvClipSetId clipSetId)
{
#if __ASSERT
	static const int MAX_EXPECTED_CONCURRENT_SCRIPT_CLIPSET_REQUESTS = 16;
#endif // __ASSERT

	animAssert(clipSetId != CLIP_SET_ID_INVALID);

	for (int i = GetInstance().m_scriptResourceRequestHelpers.GetCount() - 1; i >= 0; --i)
	{
		if (GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.GetClipSetId() == clipSetId)
		{
			GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.Request(clipSetId);
			animAssert(GetInstance().m_scriptResourceRequestHelpers[i].m_refCount > 0); // If it already exists, it should have at least one ref on it
			GetInstance().m_scriptResourceRequestHelpers[i].m_refCount++; // Already exists, bump existing ref count
			return;
		}
	}

	// Doesn't exist, add it
	ScriptResourceRequestHelper newHelper;
	newHelper.m_requestHelper.Request(clipSetId);
	newHelper.m_refCount++;
	animAssert(newHelper.m_refCount == 1);
	GetInstance().m_scriptResourceRequestHelpers.PushAndGrow(newHelper);

	// If this fires, then script are either requesting a lot of unique clipsets at the same time, or requests are being leaked
	animAssert(GetInstance().m_scriptResourceRequestHelpers.GetCount() <= MAX_EXPECTED_CONCURRENT_SCRIPT_CLIPSET_REQUESTS);
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::Script_RequestClipSet(fwMvClipSetId clipSetId)
{
	animAssert(clipSetId != CLIP_SET_ID_INVALID);

	for (int i = GetInstance().m_scriptResourceRequestHelpers.GetCount() - 1; i >= 0; --i)
	{
		if (GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.GetClipSetId() == clipSetId)
		{
			animAssert(GetInstance().m_scriptResourceRequestHelpers[i].m_refCount > 0); // If it already exists, it should have at least one ref on it
			GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.Request(clipSetId);
			return;
		}
	}
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetManager::Script_HasClipSetLoaded(fwMvClipSetId clipSetId)
{
	animAssert(clipSetId != CLIP_SET_ID_INVALID);

	for (int i = GetInstance().m_scriptResourceRequestHelpers.GetCount() - 1; i >= 0; --i)
	{
		if (GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.GetClipSetId() == clipSetId)
		{
			animAssert(GetInstance().m_scriptResourceRequestHelpers[i].m_refCount > 0); // If it already exists, it should have at least one ref on it
			return GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.Request(clipSetId);
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetManager::Script_RemoveClipSet(fwMvClipSetId clipSetId)
{
	animAssert(clipSetId != CLIP_SET_ID_INVALID);

	for (int i = GetInstance().m_scriptResourceRequestHelpers.GetCount() - 1; i >= 0; --i)
	{
		if (GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.GetClipSetId() == clipSetId)
		{
			animAssert(GetInstance().m_scriptResourceRequestHelpers[i].m_refCount > 0); // If it already exists, it should have at least one ref on it
			GetInstance().m_scriptResourceRequestHelpers[i].m_refCount--; // Decrement ref count
			if (GetInstance().m_scriptResourceRequestHelpers[i].m_refCount == 0)
			{
				GetInstance().m_scriptResourceRequestHelpers[i].m_requestHelper.Release();
				GetInstance().m_scriptResourceRequestHelpers.DeleteFast(i);
			}
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////

#if __BANK

bool fwClipSetManager::Save()
{
	bool bResult = true;

	ASSET.PushFolder(g_clipSetAssetDirectory);

	Displayf("Saving clip_sets.pso.meta...\n");

	bResult = PARSER.SaveObject("clip_sets.pso.meta", "", &GetInstance());

	Displayf("Saved clip_sets.pso.meta.\n");

	ASSET.PopFolder();

	return bResult;
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::AddWidgets(fwDebugBank * /*pBank*/)
{
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::AddClipSetWidgets(bkBank *pBank, int *pSelection, datCallback pSelectionCallback /*= NullCB*/)
{
	pBank->AddCombo("ClipSet", pSelection, ms_clipSetNames.GetCount(), &ms_clipSetNames[0], pSelectionCallback);
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::InitWidgets()
{
	if(m_pBank)
	{
		m_pBank->Shutdown();
	}

	m_pBank = fwDebugBank::CreateBank("ClipSet editor", MakeFunctor(fwClipSetManager::ActivateBank), MakeFunctor(fwClipSetManager::DeactivateBank));
	ms_instance.ms_selectedClipSet = 0;
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::ActivateBank()
{
	BuildClipDictionaryNameArray();
	BuildClipNameArray();

	// set up the selectors / menu structure
	m_pBank->PushGroup("Clip sets");
	{
		m_pBank->PushGroup("Add / remove a clip set");
		{
			m_pBank->AddText("Clip set name:", &m_addDeleteClipSetName[0], MAX_CLIP_SET_NAME_STRING_LENGTH);
			m_pBank->AddCombo("Clip dictionary:", &ms_selectedClipDictionary, ms_clipDictionaryNames.GetCount(), &ms_clipDictionaryNames[0], fwClipSetManager::SelectClipDictionary);
			m_pBank->AddButton("Add this clip set", fwClipSetManager::AddClipSetByName);
			m_pBank->AddButton("Remove this clip set", fwClipSetManager::RemoveClipSetByName);
		}
		m_pBank->PopGroup();

		m_pClipSetList = m_pBank->AddCombo("Edit clip set:", &ms_selectedClipSet, ms_clipSetNames.GetCount(), &ms_clipSetNames[0], fwClipSetManager::SelectClipSet);

		m_pClipGroup = m_pBank->PushGroup("Add pClip");
		{
			m_pBank->AddCombo("Clip:", &ms_selectedClip, ms_clipNames.GetCount(), &ms_clipNames[0], datCallback::NULL_CALLBACK);
			m_pBank->AddButton("Add clip to clip set", fwClipSetManager::AddClipToCurrentClipSet);
			m_pBank->AddButton("Add all clips to clip set", fwClipSetManager::AddAllClipsToCurrentClipSet);
		}
		m_pBank->PopGroup();

		m_pBank->PushGroup("Remove pClip");
		{
			m_pBank->AddText("Enter name:", &m_deleteClipName[0], MAX_CLIP_SET_NAME_STRING_LENGTH);
			m_pBank->AddButton("Remove clip from clip set", fwClipSetManager::DeleteClipFromCurrentClipSet);
			m_pBank->AddButton("Remove all clips from clip set", fwClipSetManager::DeleteAllClipsFromCurrentClipSet);
		}
		m_pBank->PopGroup();

		m_pClipSetGroup = m_pBank->PushGroup("Clips");
		{
			//we'll fill this in a bit later (see SelectClipSet())
		}
		m_pBank->PopGroup();

		m_pFallbackSetList = m_pBank->AddCombo("Fallback clip set:", &ms_selectedFallbackClipSet, ms_clipSetNames.GetCount(), &ms_clipSetNames[0], fwClipSetManager::UpdateFallbackClipSet);

	}
	m_pBank->PopGroup();
	m_pBank->PushGroup("Dictionary metadata");
	{
	}
	m_pBank->PopGroup();
	m_pBank->AddButton("Save to xml", CFA1(fwClipSetManager::Save));
	m_pBank->AddButton("Load from xml", CFA1(fwClipSetManager::LoadFromXML));

	// populate the anim set information
	SelectClipSet();
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::DeactivateBank()
{
	m_pBank->RemoveAllMainWidgets();
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::ShutdownWidgets()
{
	m_pBank->Shutdown();
	m_pBank = NULL;
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::BuildClipDictionaryNameArray()
{
	ms_selectedClipDictionary = 0;
	ms_clipDictionaryNames.Reset();

	atArray< const char * > sortedClipDictionaryNames;

	for(int i = 0; i < g_ClipDictionaryStore.GetCount(); i++)
	{
		if(g_ClipDictionaryStore.IsValidSlot(strLocalIndex(i)))
		{
			sortedClipDictionaryNames.PushAndGrow(g_ClipDictionaryStore.GetName(strLocalIndex(i)));
		}
	}

	sortedClipDictionaryNames.QSort(0, -1, SortAlphabeticalNoCase);

	for(int i = 0; i < sortedClipDictionaryNames.GetCount(); i++)
	{
		ms_clipDictionaryNames.PushAndGrow(sortedClipDictionaryNames[i]);
	}
}

void fwClipSetManager::BuildClipNameArray()
{
	ms_selectedClip = 0;
	ms_clipNames.Reset();

	int clipDictionaryIndex = g_ClipDictionaryStore.FindSlot(ms_clipDictionaryNames[ms_selectedClipDictionary]).Get();
	fwClipDictionaryLoader loader(clipDictionaryIndex);
	if(loader.IsLoaded())
	{
		crClipDictionary *pClipDictionary = loader.GetDictionary();
		if(pClipDictionary)
		{
			atArray< const char * > sortedClipNames;

			for(u32 i = 0; i < pClipDictionary->GetNumClips(); i ++)
			{
				crClip *pClip = pClipDictionary->FindClipByIndex(i);
				if(pClip)
				{
					atString clipName(pClip->GetName());
					atArray< atString > clipNameSplit;
					clipName.Split(clipNameSplit, "/");
					clipName = clipNameSplit[1];
					clipName.Truncate(clipName.GetLength() - 5);

					atHashValue clipNameHash = atHashString(clipName).GetHash();

					const char *hashString = atHashString(clipNameHash).GetCStr();

					sortedClipNames.PushAndGrow(hashString);
				}
			}

			sortedClipNames.QSort(0, -1, SortAlphabeticalNoCase);

			for(int i = 0; i < sortedClipNames.GetCount(); i ++)
			{
				ms_clipNames.PushAndGrow(sortedClipNames[i]);
			}
		}
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////


#if __BANK

void fwClipSetManager::SelectClipSet()
{
	if(m_pClipSetGroup)
	{
		m_pBank->SetCurrentGroup(*m_pClipSetGroup);

		while(m_pClipSetGroup->GetChild())
		{
			m_pClipSetGroup->GetChild()->Destroy();
		}

		ms_selectedFallbackClipSet = 0;

		if(ms_selectedClipSet > 0)
		{
			fwMvClipSetId clipSetId(ms_clipSetNames[ms_selectedClipSet]);
			fwClipSet *pClipSet = GetClipSet(clipSetId);

			if(pClipSet)
			{
				pClipSet->AddWidgets(m_pBank);
			}

			fwMvClipSetId fallbackClipSetId = pClipSet->GetFallbackId();
			for(int i = 0; i < ms_clipSetNames.GetCount(); i ++)
			{
				if(ms_clipSetNames[i] == fallbackClipSetId.GetCStr())
				{
					ms_selectedFallbackClipSet = i;
				}
			}
		}

		m_pBank->UnSetCurrentGroup(*m_pClipSetGroup);
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::SelectClipDictionary()
{
	BuildClipNameArray();

	while(m_pClipGroup->GetChild())
	{
		bkWidget *pWidget = m_pClipGroup->GetChild();

		m_pClipGroup->Remove(*pWidget);
	}

	m_pBank->SetCurrentGroup(*m_pClipGroup);
	{
		m_pBank->AddCombo("Clip:", &ms_selectedClip, ms_clipNames.GetCount(), &ms_clipNames[0], datCallback::NULL_CALLBACK);
		m_pBank->AddButton("Add clip to clip set", fwClipSetManager::AddClipToCurrentClipSet);
		m_pBank->AddButton("Add all clips to clip set", fwClipSetManager::AddAllClipsToCurrentClipSet);
	}
	m_pBank->UnSetCurrentGroup(*m_pClipGroup);
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::AddClipToCurrentClipSet()
{
	if(ms_selectedClipSet > 0)
	{
		fwClipSet *pClipSet = GetClipSet(GetClipSetIdByIndex(ms_selectedClipSet));

		if(pClipSet)
		{
			if(pClipSet->CreateClipItem(fwMvClipId(ms_clipNames[ms_selectedClip])))
			{
				//reload the animation list
				SelectClipSet();
			}
		}
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::AddAllClipsToCurrentClipSet()
{
	if(ms_selectedClipSet > 0)
	{
		fwClipSet *pClipSet = GetClipSet(GetClipSetIdByIndex(ms_selectedClipSet));

		// TODO: Re-implement this!
		if(pClipSet)
		{
			for(int i = 0; i < ms_clipNames.GetCount(); i ++)
			{
				if(pClipSet->CreateClipItem(fwMvClipId(ms_clipNames[i])))
				{
					//reload the animation list
					SelectClipSet();
				}
			}
		}
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::DeleteClipFromCurrentClipSet()
{
	if(ms_selectedClipSet > 0)
	{
		fwClipSet *pClipSet = GetClipSet(GetClipSetIdByIndex(ms_selectedClipSet));

		if(pClipSet)
		{
			if(pClipSet->DeleteClipItem(fwMvClipId(m_deleteClipName[0])))
			{
				//reload the animation list
				SelectClipSet();
			}
		}
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::DeleteAllClipsFromCurrentClipSet()
{
	if(ms_selectedClipSet > 0)
	{
		fwClipSet *pClipSet = GetClipSet(GetClipSetIdByIndex(ms_selectedClipSet));

		if(pClipSet)
		{
			pClipSet->DeleteAllClipItems();

			//reload the animation list
			SelectClipSet();
		}
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::AddClipSetByName()
{
	CreateClipSet(fwMvClipSetId(&m_addDeleteClipSetName[0]), CLIP_SET_ID_INVALID, ms_clipDictionaryNames[ms_selectedClipDictionary]);
	UpdateClipSetComboBox();
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::RemoveClipSetByName()
{
	DeleteClipSet(fwMvClipSetId(&m_addDeleteClipSetName[0]));
	UpdateClipSetComboBox();
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::UpdateClipSetComboBox()
{
	if(ms_selectedClipSet > ms_clipSetNames.GetCount())
	{
		ms_selectedClipSet = ms_clipSetNames.GetCount();
	}

	m_pClipSetList->UpdateCombo("Edit clip set:", &ms_selectedClipSet, ms_clipSetNames.GetCount(), &ms_clipSetNames[0], fwClipSetManager::SelectClipSet);
	m_pFallbackSetList->UpdateCombo("Fallback clip set:", &ms_selectedFallbackClipSet, ms_clipSetNames.GetCount(), &ms_clipSetNames[0], fwClipSetManager::UpdateFallbackClipSet);
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::UpdateFallbackClipSet()
{
	fwClipSet *pClipSet = GetClipSet(GetClipSetIdByIndex(ms_selectedClipSet));
	if(pClipSet)
	{
		if(ms_selectedFallbackClipSet == 0)
		{
			pClipSet->SetFallbackId(CLIP_SET_ID_INVALID);
		}
		else
		{
			pClipSet->SetFallbackId(GetClipSetIdByIndex(ms_selectedFallbackClipSet));
		}
	}
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

void fwClipSetManager::ValidateClipSets()
{
	Printf("Validating ClipSets...\n");

	ASSET.PushFolder(g_clipSetAssetDirectory);

	int badClipItems = 0;
	int badClipSets = 0;
	int badFallbacks = 0;

	char szBuffer[256];

	// Open CSV file
	fiStream *pCsvFile = ASSET.Create("ValidateClipSets", "csv");
	animAssert(pCsvFile);

	// Write header
	sprintf(szBuffer, "Clip Set,Fallback Set,Reason Invalid\n");
	pCsvFile->WriteByte(szBuffer, istrlen(szBuffer));

	// Iterate through clip sets
	for(int clipSetIndex = 0; clipSetIndex < fwClipSetManager::GetClipSetCount(); clipSetIndex ++)
	{
		fwMvClipSetId clipSetId = fwClipSetManager::GetClipSetIdByIndex(clipSetIndex);
		const fwClipSet *pClipSet = fwClipSetManager::GetClipSetByIndex(clipSetIndex);
		fwMvClipSetId fallbackId = pClipSet->GetFallbackId();
		if(fallbackId != CLIP_SET_ID_INVALID)
		{
			fwClipSet *pFallback = GetClipSet(fallbackId);
			if(!pFallback)
			{
				// Write row
				sprintf(szBuffer, "%s,%s,%s\n",
					clipSetId.TryGetCStr(),
					fallbackId.TryGetCStr(),
					"ClipSet has an invalid fallback");
				pCsvFile->WriteByte(szBuffer, istrlen(szBuffer));

				badFallbacks ++;
			}
		}
	}

	// Write header
	sprintf(szBuffer, "\nClip Set,Fallback Set,Clip Dictionary,Clip,Reason Invalid\n");
	pCsvFile->WriteByte(szBuffer, istrlen(szBuffer));

	// Iterate through clip sets
	for(int clipSetIndex = 0; clipSetIndex < fwClipSetManager::GetClipSetCount(); clipSetIndex ++)
	{
		fwMvClipSetId clipSetId = fwClipSetManager::GetClipSetIdByIndex(clipSetIndex);
		const fwClipSet *pClipSet = fwClipSetManager::GetClipSetByIndex(clipSetIndex);

		int badClipItemsBefore = badClipItems;

		// Iterate through clip items
		for(int clipItemIndex = 0; clipItemIndex < pClipSet->GetClipItemCount(); clipItemIndex ++)
		{
			fwMvClipId clipId = pClipSet->GetClipItemIdByIndex(clipItemIndex);

			const char *clipItemInvalidReason = NULL;
			if(pClipSet->GetClipDictionaryIndex() != -1)
			{
				if(g_ClipDictionaryStore.IsValidSlot(strLocalIndex(pClipSet->GetClipDictionaryIndex())))
				{
					if(fwClipSetManager::StreamIn_DEPRECATED(clipSetId))
					{
						if(!pClipSet->GetClip(clipId))
						{
							clipItemInvalidReason = "Could not find clip in clip dictionary!";
						}
					}
					else
					{
						clipItemInvalidReason = "Could not stream in clip set or one of it's fallback clip sets!";
					}
				}
				else
				{
					clipItemInvalidReason = "Could not find clip dictionary!";
				}
			}
			else
			{
				clipItemInvalidReason = "Could not find clip dictionary!";
			}

			if(clipItemInvalidReason)
			{
				badClipItems ++;
			}

			// Check pClipItem points to a invalid pClip
			if(clipItemInvalidReason != NULL)
			{
				atString streamingPolicy;
				eStreamingPolicy_ToString((eStreamingPolicy)pClipSet->GetClipDictionaryStreamingPolicy(), streamingPolicy);

				// Write row
				sprintf(szBuffer, "%s,%s,%s,%s,%s\n",
					clipSetId.GetCStr(),
					pClipSet->GetFallbackId().GetCStr() ? pClipSet->GetFallbackId().GetCStr() : "",
					pClipSet->GetClipDictionaryName().GetCStr(),
					clipId.GetCStr(),
					clipItemInvalidReason);
				pCsvFile->WriteByte(szBuffer, istrlen(szBuffer));
			}
		}

		if(badClipItems > badClipItemsBefore)
		{
			badClipSets ++;
		}
	}

	// Close CSV file
	pCsvFile->Close();

	ASSET.PopFolder();

	Printf("Validated ClipSets, found %i bad ClipSets, %i bad Fallbacks and %i bad ClipItems. See \\export\\data\\anim\\clip_sets\\ValidateClipSets.csv\n", badClipSets, badFallbacks, badClipItems);

	animAssertf(badClipSets == 0 && badFallbacks == 0 && badClipItems == 0, "Validated ClipSets, found %i bad ClipSets, %i bad Fallbacks and %i bad ClipItems. See \\export\\data\\anim\\clip_sets\\ValidateClipSets.csv\\n", badClipSets, badFallbacks, badClipItems);
}

void fwClipSetManager::LoadFromXML()
{
	// UGH.  Due to optimisations we can't just call fwClipSetManager::Load().
	// So we have to start a new game to do this, but......
	// We can't do that automatically as we're at the framework level of code.
	// So... we'll prompt them to do it.  Horrible.
	char tempString[128];
	sprintf(tempString, "Unfortunately, to reload the XML data you will need to start a new game. Sorry!");
	bkMessageBox("Sorry...", tempString, bkMsgOk, bkMsgInfo);
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

#if __BANK

atArray< const char * > &fwClipSetManager::GetClipSetNames()
{
	return ms_clipSetNames;
}


atArray< const char * > &fwClipSetManager::GetMemoryGroupNames()
{
	return ms_memoryGroupNames;
}

atString fwClipSetManager::GetRequestFailReason(const fwMvClipSetId& clipSet, const fwMvClipId& clip)
{
	atString failReason;

	failReason+=atVarString("Clip set '%s', clip'%s' :", clipSet.TryGetCStr() ? clipSet.GetCStr(): "NULL", clip.TryGetCStr() ? clip.GetCStr(): "NULL");

	fwClipSet* pSet = GetClipSet(clipSet);

	if (pSet)
	{
		if (pSet->IsStreamedIn_DEPRECATED())
		{
			const crClip * pClip = pSet->GetClip(clip);
			if (pClip)
			{
				pClip = pSet->GetClip(clip, CRO_NONE);

				if (pClip)
				{
					failReason += " Unknown clip request failure.";
				}
				else
				{
					failReason += " The clip only exists in the fallback set. Did you mean to request with CRO_FALLBACK?";
				}
			}
			else
			{
				failReason+=" The clip does not exist on the clip set or its fallback sets.";
			}
		}
		else
		{
			failReason+=" The clip set is not streamed in.";
		}
	}
	else
	{
		failReason+=atVarString(" The clip set '%s' does not exist", clipSet.GetCStr());
	}

	return failReason;
}


#endif // __BANK


//////////////////////////////////////////////////////////////////////////

fwClipSetRequestHelper::~fwClipSetRequestHelper()
{
	Release();
}

//////////////////////////////////////////////////////////////////////////

fwClipSetRequestHelper::fwClipSetRequestHelper()
: m_clipSetId(CLIP_SET_ID_INVALID)
, m_clipDictionaryRefs(false)
{
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetRequestHelper::Request(const fwMvClipSetId &clipSetId BANK_ONLY(, bool bForce /* = false */))
{
	// Is clip set changing?
	if(m_clipSetId != clipSetId)
	{
		// Does clip set exist?
		if(m_clipSetId != CLIP_SET_ID_INVALID)
		{
			Release();
		}

		// Assign clip set
		m_clipSetId = clipSetId;
	}
	else if(m_clipDictionaryRefs.IsSet(kSpecialBitLoaded))
	{
		// If this bit was set, it means that we must have previously passed through the request
		// and found everything to be resident. Since we keep a ref-count on them, they should
		// still be resident. The GetClipSet() call below is somewhat expensive, and there are also
		// variable bit shifts and various memory access which we want to avoid the cost of.
		return true;
	}

	// Does clip set exist?
	fwClipSet *pClipSet = fwClipSetManager::GetClipSet(m_clipSetId);

	animAssertf(pClipSet || clipSetId == CLIP_SET_ID_INVALID, "ClipSet %s did not exist, check clip_sets.pso.meta to make sure it was named correctly.", clipSetId.GetCStr());
	
	if(pClipSet)
	{
		STRVIS_AUTO_CONTEXT(strStreamingVisualize::CLIPREQUEST);
		bool allClipDictionariesLoaded = true;

		// Iterate through clip sets
		u32 uClipDictionaryIndex = 0;
		for(; pClipSet; pClipSet = pClipSet->GetFallbackSet(), uClipDictionaryIndex ++)
		{
			// Make sure we don't have so many fallback sets that we are about to stomp on the special bit.
			animAssert(uClipDictionaryIndex != kSpecialBitLoaded);

			// Is clip dictionary not loaded?
			if(m_clipDictionaryRefs.IsClear(uClipDictionaryIndex))
			{
				strLocalIndex iClipDictionarySlot = strLocalIndex(pClipSet->GetClipDictionaryIndex());
				if(Verifyf(g_ClipDictionaryStore.IsValidSlot(iClipDictionarySlot), "Clip set clip dictionary slot is invalid!"))
				{
					if(Verifyf(g_ClipDictionaryStore.IsObjectInImage(iClipDictionarySlot), "Clip set clip dictionary is not in image!"))
					{
						if(!g_ClipDictionaryStore.HasObjectLoaded(iClipDictionarySlot))
						{
							g_ClipDictionaryStore.StreamingRequest(iClipDictionarySlot, STRFLAG_DONTDELETE | STRFLAG_PRIORITY_LOAD);
						}

#if __BANK
						if(bForce)
						{
							strStreamingEngine::GetLoader().LoadAllRequestedObjects(true);
						}
#endif // __BANK

						if(g_ClipDictionaryStore.HasObjectLoaded(iClipDictionarySlot))
						{
							crClipDictionary *pClipDictionary = g_ClipDictionaryStore.Get(iClipDictionarySlot);
							if(Verifyf(pClipDictionary, "Clip set clip dictionary is loaded but is NULL!"))
							{
								pClipDictionary->AddRef();

								g_ClipDictionaryStore.ClearRequiredFlag(iClipDictionarySlot.Get(), STRFLAG_DONTDELETE);

								m_clipDictionaryRefs.Set(uClipDictionaryIndex);
							}
						}
					}
				}
			}

			allClipDictionariesLoaded &= m_clipDictionaryRefs.IsSet(uClipDictionaryIndex);
		}

		if(allClipDictionariesLoaded)
		{
			m_clipDictionaryRefs.Set(kSpecialBitLoaded);
		}
		return allClipDictionariesLoaded;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetRequestHelper::Request(BANK_ONLY(bool bForce /* = false */))
{
	return Request(m_clipSetId BANK_ONLY(, bForce));
}

//////////////////////////////////////////////////////////////////////////

void fwClipSetRequestHelper::Release()
{
	// Does clip set exist?
	fwClipSet *pClipSet = fwClipSetManager::GetClipSet(m_clipSetId);
	if(pClipSet)
	{
		// Iterate through clip sets
		u32 uClipDictionaryIndex = 0;
		for(; pClipSet; pClipSet = pClipSet->GetFallbackSet(), uClipDictionaryIndex ++)
		{
			if(m_clipDictionaryRefs.IsSet(uClipDictionaryIndex))
			{
				crClipDictionary *pClipDictionary = pClipSet->GetClipDictionary();
				if(Verifyf(pClipDictionary, "Clip set clip dictionary is ref'd but is not loaded!"))
				{
					pClipDictionary->Release();

					m_clipDictionaryRefs.Clear(uClipDictionaryIndex);
				}
			}
		}
	}

	m_clipDictionaryRefs.Clear(kSpecialBitLoaded);

	m_clipSetId = CLIP_SET_ID_INVALID;

	animAssertf(!m_clipDictionaryRefs.AreAnySet(), "Not all clip dictionary refs have been released!");
	m_clipDictionaryRefs.Reset();
}

//////////////////////////////////////////////////////////////////////////

fwClipSet* fwClipSetRequestHelper::GetClipSet() const
{
	return IsLoaded() ? fwClipSetManager::GetClipSet(m_clipSetId) : NULL;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetRequestHelper::IsLoaded() const
{
	if(m_clipDictionaryRefs.IsSet(kSpecialBitLoaded))
	{
		return true;
	}

// No need to do all this, if we set all the relevant bits in m_clipDictionaryRefs,
// we should have also set kSpecialBitLoaded.
#if 0
	// Does clip set exist?
	fwClipSet *pClipSet = fwClipSetManager::GetClipSet(m_clipSetId);
	if(pClipSet)
	{
		bool allClipDictionariesLoaded = true;

		// Iterate through clip sets
		u32 uClipDictionaryIndex = 0;
		for(; allClipDictionariesLoaded && pClipSet; pClipSet = pClipSet->GetFallbackSet(), uClipDictionaryIndex ++)
		{
			allClipDictionariesLoaded &= m_clipDictionaryRefs.IsSet(uClipDictionaryIndex);
		}

		return allClipDictionariesLoaded;
	}
#endif

	return false;
}

//////////////////////////////////////////////////////////////////////////

bool fwClipSetRequestHelper::IsInvalid() const
{
	return m_clipSetId == CLIP_SET_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

/* Properties */

u32 fwClipRpfBuildMetadata::GetClipDictionaryBuildMetadataCount() const
{
	return m_dictionaries.GetCount();
}

atHashString fwClipRpfBuildMetadata::GetClipDictionaryBuildMetadataName(u32 index) const
{
	if(Verifyf(index < (u32)m_dictionaries.GetCount(), ""))
	{
		const atHashString *pName = m_dictionaries.GetKey(index);
		if(pName)
		{
			return *pName;
		}
	}

	return atHashString::Null();
}

const fwClipDictionaryBuildMetadata *fwClipRpfBuildMetadata::GetClipDictionaryBuildMetadata(u32 index) const
{
	if(Verifyf(index < (u32)m_dictionaries.GetCount(), ""))
	{
		return m_dictionaries.GetItem(index);
	}

	return NULL;
}

fwClipDictionaryBuildMetadata *fwClipRpfBuildMetadata::GetClipDictionaryBuildMetadata(u32 index)
{
	if(Verifyf(index < (u32)m_dictionaries.GetCount(), ""))
	{
		return m_dictionaries.GetItem(index);
	}

	return NULL;
}

/* Operations */

bool fwClipRpfBuildMetadata::CreateClipDictionaryBuildMetadata(const atHashString &name, const fwClipDictionaryBuildMetadata &clipBuildMetadata)
{
	if(Verifyf(m_dictionaries.SafeInsert(name, clipBuildMetadata), ""))
	{
		m_dictionaries.FinishInsertion();

		return true;
	}

	return false;
}

bool fwClipRpfBuildMetadata::DestroyClipDictionaryBuildMetadata(const atHashString &name)
{
	fwClipDictionaryBuildMetadata *pClipDictionaryBuildMetadata = m_dictionaries.SafeGet(name);
	if(Verifyf(pClipDictionaryBuildMetadata, ""))
	{
		u32 index = m_dictionaries.GetIndexFromDataPtr(pClipDictionaryBuildMetadata);
		if(Verifyf(index < (u32)m_dictionaries.GetCount(), ""))
		{
			m_dictionaries.Remove(index);

			return true;
		}
	}

	return false;
}

const fwClipDictionaryBuildMetadata *fwClipRpfBuildMetadata::FindClipDictionaryBuildMetadata(const atHashString &name) const
{
	return m_dictionaries.SafeGet(name);
}

fwClipDictionaryBuildMetadata *fwClipRpfBuildMetadata::FindClipDictionaryBuildMetadata(const atHashString &name)
{
	return m_dictionaries.SafeGet(name);
}

#if __BANK

void fwClipRpfBuildMetadata::Merge(const fwClipRpfBuildMetadata &clipRpfBuildMetadata)
{
	for(u32 i = 0; i < clipRpfBuildMetadata.GetClipDictionaryBuildMetadataCount(); i ++)
	{
		atHashString name = clipRpfBuildMetadata.GetClipDictionaryBuildMetadataName(i);
		atString temp(name.GetCStr());
		temp.Replace(".icd.zip", "");
		name.SetFromString(temp);
		const fwClipDictionaryBuildMetadata *pClipDictionaryBuildMetadata = clipRpfBuildMetadata.GetClipDictionaryBuildMetadata(i);

		if(Verifyf(!FindClipDictionaryBuildMetadata(name), ""))
		{
			CreateClipDictionaryBuildMetadata(name, *pClipDictionaryBuildMetadata);
		}
	}
}

bool fwClipRpfBuildMetadata::Load(const char *file)
{
	bool bResult = true;

	m_dictionaries.Reset();

	ASSET.PushFolder("common://non_final/anim/clip_dictionary_metadata");

	Displayf("Loading fwClipRpfBuildMetadata %s.xml...\n", file);

	bResult = PARSER.LoadObject(file, "xml", *this);
	animAssertf(bResult, "Load %s.xml failed!\n", file);

	Displayf("Loaded fwClipRpfBuildMetadata %s.xml.\n", file);

	ASSET.PopFolder();

	return bResult;
}

bool fwClipRpfBuildMetadata::Save(const char *file)
{
	bool bResult = true;

	ASSET.PushFolder("common://non_final/anim/clip_dictionary_metadata");

	Displayf("Saving fwClipRpfBuildMetadata %s.xml...\n", file);

	bResult = PARSER.SaveObject(file, "xml", this);

	Displayf("Saved fwClipRpfBuildMetadata %s.xml.\n", file);

	ASSET.PopFolder();

	return bResult;
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

} // namespace rage
