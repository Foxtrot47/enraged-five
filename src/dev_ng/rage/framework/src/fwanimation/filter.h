// 
// animation/Filter.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_FILTER_H
#define FWANIMATION_FILTER_H

#include "parser/macros.h"
#include "data/base.h"
#include "atl/map.h" 
#include "atl/array.h"
#include "atl/string.h"
#include "atl/hashstring.h"

#include "fwanimation/boneids.h"

// rage forward declarations
namespace rage {

const char *GetBoneTagName(u16 boneTag);

class fwFrameFilter_TrackIdIndex
{
public:

	u8 m_Track;
	eAnimBoneTag m_Id;
	u8 m_WeightIndex;

	PAR_SIMPLE_PARSABLE;
};

class fwFrameFilter
{
public:

	atHashString m_Name;
	atArray< fwFrameFilter_TrackIdIndex > m_TrackIdIndices;
	atArray< float > m_Weights;

	PAR_SIMPLE_PARSABLE;
};

class fwFrameFilterDictionary
{
public:

	atHashString m_Name;
	atArray< fwFrameFilter > m_FrameFilters;

	PAR_SIMPLE_PARSABLE;
};

class fwFrameFilterDictionaryStore
{
public:

	atArray< fwFrameFilterDictionary > m_FrameFilterDictionaries;

#if !__FINAL
	static void Save();
	static void Load();
#endif // !__FINAL

	PAR_SIMPLE_PARSABLE;
};

} // namespace rage

#endif // FWANIMATION_FILTER_H