#ifndef FACIALCLIPSETGROUPS_H_
#define FACIALCLIPSETGROUPS_H_

#include "atl/binmap.h"
#include "fwanimation/animdefines.h"
#include "fwanimation/clipsets.h"
#include "fwdebug/debugbank.h"
#include "parser/macros.h"

namespace rage {

// TODO: Specify this per-platform.  More on PC?
#define MAX_LOADED_FACIAL_VARIATION_CLIP_SETS (2)

//////////////////////////////////////////////////////////////////////////

// Enum for specifying the mood variations.
typedef enum
{
	MoodAiming,
	MoodAngry,
	MoodHappy,
	MoodInjured,
	MoodNormal,
	MoodStressed,
	MoodExcited,
	MoodFrustrated,
	MoodTalking,
	MoodDancingLow,
	MoodDancingMed,
	MoodDancingHigh,
	MoodDancingTrance
} VariationMood;


//////////////////////////////////////////////////////////////////////////

class fwFacialClipSetVariationInfo
{
public:

	// Constructor/Destructor
	fwFacialClipSetVariationInfo() {};
	~fwFacialClipSetVariationInfo() {};

	// Accessors
	inline VariationMood GetMood() const			{ return m_mood; }
	inline const atHashString& GetClipSet() const	{ return m_clipSet; }
	inline float GetMinimumResidentTime() const		{ return m_minimumResidentTime; }

	inline float GetMinimumBasePlaybackTime() const { return m_baseMinimumPlaybackTime; }
	inline float GetMaximumBasePlaybackTime() const { return m_baseMaximumPlaybackTime; }

	inline float GetMinimumPlaybackTime() const		{ return m_minimumPlaybackTime; }
	inline float GetMaximumPlaybackTime() const		{ return m_maximumPlaybackTime; }

	inline float GetBlendInTime() const				{ return m_blendInTime; }
	inline float GetBlendOutTime() const			{ return m_blendOutTime; }

private:

	VariationMood		m_mood;
	atHashString		m_clipSet;
	float				m_minimumResidentTime;
	float				m_baseMinimumPlaybackTime;
	float				m_baseMaximumPlaybackTime;
	float				m_minimumPlaybackTime;
	float				m_maximumPlaybackTime;
	float				m_blendInTime;
	float				m_blendOutTime;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwFacialClipSetGroup
{
public:

	// Constructor/Destructor
	fwFacialClipSetGroup() {};
	~fwFacialClipSetGroup() {};

	// Get clipset names...
	inline const atHashString& GetBaseClipSetName() const			{ return m_baseClipSet; }

	// Get variation count
	inline const int GetVariationCount() const						{ return m_Variations.GetCount(); }

	// Get variation by index
	inline const fwFacialClipSetVariationInfo& GetVariationByIndex(int idx) { return m_Variations[idx]; }

	// Get variation by mood
	const fwFacialClipSetVariationInfo* GetVariationByMood(VariationMood mood);

protected:

	atHashString m_baseClipSet;
	atArray<fwFacialClipSetVariationInfo>	m_Variations;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwFacialClipSetGroupManager
{
public:

	// Static properties

	static inline fwFacialClipSetGroupManager &GetInstance() { return ms_instance; }

	static void Init(u32 initMode);
	static void Shutdown(u32 shutdownMode);

	static void Reset();

	static void Update();

	static bool Load();
	static bool Append(const char *fname);

#if __BANK
	static bool Save();
#endif // __BANK

	static inline int GetFacialClipSetGroupCount() { return ms_instance.m_facialClipSetGroups.GetCount(); }

	static fwMvFacialClipSetGroupId GetFacialClipSetGroupId(const atHashString& facialClipSetGroupName);
	static fwFacialClipSetGroup *GetFacialClipSetGroup(const fwMvFacialClipSetGroupId &facialClipSetGroupId);

	static void DeleteAllFacialClipSetGroups();

	static const fwFacialClipSetVariationInfo* RequestVariationsAndGetVariationInfo(const fwMvFacialClipSetGroupId &facialClipSetGroupId, VariationMood mood, float fImportance);

	static void GetVariationClipSetAndClip(const fwMvFacialClipSetGroupId &facialClipSetGroupId, VariationMood mood, fwMvClipSetId& varClipSetIdOut, fwMvClipId& varClipIdOut);

#if __BANK
	// PURPOSE:
	static void DumpTrackerDebugInfo(atString& string, const int MaxTrackersToDisplay);
#endif // __BANK

protected:

	// Enum for tracking a variation clipset's state
	enum VariationClipSetState
	{
		VariationClipSetState_UnLoaded,
		VariationClipSetState_StreamingIn,
		VariationClipSetState_Loaded,
		VariationClipSetState_StreamingOut,
	};

	// Class for keeping track of a variation's scoring and clipset slot
	class fwVariationTracker
	{
	public:

		// Constructor

		fwVariationTracker(fwMvFacialClipSetGroupId groupId, const fwFacialClipSetVariationInfo* pVariationInfo);

		// Reset

		void Reset();

		// Sort function

		static bool GreaterThanEqual(const fwVariationTracker* a, const fwVariationTracker* b);

		// Set state function

		void SetState(VariationClipSetState newState);

		// Properties

		fwClipSetRequestHelper				m_clipSetRequestHelper;
		VariationClipSetState				m_eClipSetState;
		fwMvFacialClipSetGroupId			m_groupId;
		const fwFacialClipSetVariationInfo*	m_pVariationInfo;
		float								m_fTimer;
		float								m_fImportance;
	};

	// Static data

	static fwFacialClipSetGroupManager								ms_instance;

	// Serialised Data

	atBinaryMap< fwFacialClipSetGroup*, fwMvFacialClipSetGroupId >	m_facialClipSetGroups;

	// Runtime Data

	atArray<fwVariationTracker*>									m_variationTrackers;

	bool															m_bClipsetUnloadRequired;

	PAR_SIMPLE_PARSABLE;
};

} // rage

#endif // FACIALCLIPSETGROUPS_H_