// 
// fwanimation/directorcomponentcreature.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponentcreature.h"

#include "animdirector.h"

#include "creature/componentextradofs.h"
#include "creature/componentskeleton.h"
#include "creature/creature.h"
#include "entity/entity.h"

// For dumping motion tree on errors
#include "cranimation/animation_config.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentCreature::fwAnimDirectorComponentCreature()
: fwAnimDirectorComponent(kComponentTypeCreature)
, m_Creature(NULL)
, m_CreaturePhysicalComponent(NULL)
, m_CreatureSignature(0)
, m_SkeletonSignatureComprehensive(0)
, m_MoverRotation(kTrackMoverRotation, 0)
, m_MoverTranslation(kTrackMoverTranslation, 0)
, m_FrameBuffer(sm_CommonPool)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentCreature::~fwAnimDirectorComponentCreature()
{
	fwAnimDirectorComponentCreature::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentCreature, 0x36776cca, kComponentTypeCreature, ANIMDIRECTOR_POOL_MAX, 0.35f);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::Init(fwAnimDirector& director)
{
	InitBase(director);

	// creature is mandatory
	const fwEntity& entity = GetDirector().GetEntity();
	m_Creature = entity.GetCreature();
	m_CreaturePhysicalComponent = m_Creature->FindComponent<crCreatureComponentPhysical>();
	m_CreatureSignature = m_Creature->GetSignature();
	m_SkeletonSignatureComprehensive = m_Creature->GetSkeleton()->GetSkeletonData().GetSignatureComprehensive();

	// construct a frame buffer
	m_FrameBuffer.SetFrameData(m_Creature->GetFrameData());
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::Shutdown()
{
	m_Creature = NULL;
	m_CreaturePhysicalComponent = NULL;

	m_FrameBuffer.Shutdown();

	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::ReceiveMessage(eMessageId msgId, ePhase, void*)
{
	switch(msgId)
	{
	case kMessageUpdate:
		{
			fwAnimDirector& director = GetDirector();

			// check skeleton matches
			crSkeleton* entitySkeleton = director.GetEntity().GetSkeleton();
			const crSkeleton* creatureSkeleton = m_Creature->GetSkeleton();
			// Ensure that the matrices haven't changed as well since we cache off the pointers. 
			if(Unlikely(entitySkeleton != creatureSkeleton || entitySkeleton->GetLocalMtxs() != creatureSkeleton->GetLocalMtxs() || entitySkeleton->GetObjectMtxs() != creatureSkeleton->GetObjectMtxs()))
			{
				Warningf("entity <-> creature skeleton pointer mismatch, attempting to correct");

				// update skeleton pointer in creature
				ChangeSkeleton(*entitySkeleton);
			}

			// check for signature change
			u32 creatureSig = m_Creature->GetSignature();
			if(Unlikely(m_CreatureSignature != creatureSig))
			{
				director.MessageAllComponents(kMessageCreatureChanged);

				m_CreatureSignature = creatureSig;
			}

			// check for skeleton data change
			u32 skeletonSigComp = m_Creature->GetSkeleton()->GetSkeletonData().GetSignatureComprehensive();
			if(Unlikely(m_SkeletonSignatureComprehensive != skeletonSigComp))
			{
				if(m_CreaturePhysicalComponent)
				{
					m_CreaturePhysicalComponent->ResetMotions();
				}
				m_SkeletonSignatureComprehensive = skeletonSigComp;
			}
		}
		break;

	case kMessageCreatureChanged:
		{
			m_Creature->Refresh();

			m_CreatureSignature = m_Creature->GetSignature();

			m_CreaturePhysicalComponent = m_Creature->FindComponent<crCreatureComponentPhysical>();

			m_FrameBuffer.SetFrameData(m_Creature->GetFrameData());

			GetDirector().MessageAllComponents(kMessageFrameDataChanged);
		}
		break;
	default:
		break;
	}
}

void fwAnimDirectorComponentCreature::ChangeSkeleton(crSkeleton& skeleton)
{
	fwAnimDirector& director = GetDirector();
	crCreatureComponentSkeleton* componentSkeleton = m_Creature->FindComponent<crCreatureComponentSkeleton>(); 
	if(componentSkeleton)
	{
		componentSkeleton->SetSkeleton(skeleton);
	}
	director.MessageAllComponents(kMessageSkeletonChanged);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::ApplyVelocity(float deltaTime, fwDynamicEntityComponent* dynComp) 
{
	const crCreatureComponentExtraDofs* extraDofs = m_Creature->FindComponent<crCreatureComponentExtraDofs>();
	if(extraDofs && !IsNearZero(deltaTime))
	{
		const crFrame& poseFrame = extraDofs->GetPoseFrame();
		ScalarV invDeltaTime = Invert(ScalarVFromF32(deltaTime));

		Vec3V v(m_MoverTranslation.Read(poseFrame, Vec::V4VConstant(V_ZERO)));
#if __ASSERT || CR_DEV
		bool bIsFiniteAll = IsFiniteAll(v);
#endif

#if CR_DEV
		static bool bFiniteAllHasDumped = false;
		if (!bIsFiniteAll && !bFiniteAllHasDumped)
		{
			GetDirector().GetMove().GetMotionTree().Dump();
			bFiniteAllHasDumped = true;
		}
#endif // __DEV

		animAssertf(bIsFiniteAll, "ApplyVelocity: <%f, %f, %f> is valid translation", v.GetXf(), v.GetYf(), v.GetZf());
		dynComp->SetAnimatedVelocity(VEC3V_TO_INTRIN(Scale(invDeltaTime, v)));

		QuatV q(m_MoverRotation.Read(poseFrame, Vec::V4VConstant(V_ZERO_WONE)));
#if __ASSERT || CR_DEV
		unsigned int iIsCloseAll = IsCloseAll(MagSquared(q), ScalarV(V_ONE), ScalarV(V_FLT_SMALL_2));
#endif

#if CR_DEV
		static bool bCloseAllHasDumped = false;
		if (!iIsCloseAll && !bCloseAllHasDumped)
		{
			GetDirector().GetMove().GetMotionTree().Dump();
			bCloseAllHasDumped = true;
		}
#endif //__DEV

		animAssertf(iIsCloseAll, "ApplyVelocity: <%f, %f, %f, %f> is not a pure rotation", q.GetXf(), q.GetYf(), q.GetZf(), q.GetWf());
		Vec3V e = QuatVToEulersXYZ(q);
		dynComp->SetAnimatedAngularVelocity(Scale(invDeltaTime, e).GetZf());
	}
	else
	{
		dynComp->SetAnimatedVelocity(Vec::V4VConstant(V_ZERO));
		dynComp->SetAnimatedAngularVelocity(0.f);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::InitDerivedClass()
{
	// create creature components factory
	crCreatureComponent::FactoryPooled* factory = rage_new crCreatureComponent::FactoryPooled;
	factory->CreatePool(crCreatureComponent::kCreatureComponentTypeSkeleton, ANIMDIRECTOR_POOL_MAX);
	factory->CreatePool(crCreatureComponent::kCreatureComponentTypeExtraDofs, ANIMDIRECTOR_POOL_MAX);
	factory->CreatePool(crCreatureComponent::kCreatureComponentTypeShaderVars, ANIMDIRECTOR_POOL_PED_MAX/2);
	factory->CreatePool(crCreatureComponent::kCreatureComponentTypePhysical, ANIMDIRECTOR_POOL_PED_MAX*3/4);
	sm_Factory = factory;

	// create common pool for frame allocation
	const u16 peakNumFrames = (RSG_PS3||RSG_XENON)?1280:3840;
	const u32 peakFrameMemUse = (RSG_PS3||RSG_XENON)?(640*1024):(1920*1024);
	sm_CommonPool.Init(peakNumFrames, peakFrameMemUse);
	sm_FrameDataFactory.Init(sm_CommonPool);

	// setup frame accelerator
	static const u32 counts[] = { 32, 128, 64, 128, 64, 64 };
	static const u32 sizes[] = { 80*1024, 96*1024, 48*1024, 64*1024, 32*1024, 32*1024 };
	sm_Accelerator.Init(counts, sizes);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::ShutdownDerivedClass()
{
	delete sm_Factory;
	sm_Factory = NULL;

	sm_Accelerator.Shutdown();
	sm_FrameDataFactory.Shutdown();
	sm_CommonPool.Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentCreature::UpdateDerivedClass()
{
	sm_FrameDataFactory.GarbageCollect();
	sm_CommonPool.Compact(16*1024);
}

////////////////////////////////////////////////////////////////////////////////

crFrameAccelerator fwAnimDirectorComponentCreature::sm_Accelerator;
crFrameDataFactory fwAnimDirectorComponentCreature::sm_FrameDataFactory;
crCommonPool fwAnimDirectorComponentCreature::sm_CommonPool;
crCreatureComponent::Factory* fwAnimDirectorComponentCreature::sm_Factory = NULL;

////////////////////////////////////////////////////////////////////////////////

} // namespace rage
