// 
// fwanimation/directorcomponentmotiontree.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_MOTION_TREE_H
#define FWANIMATION_DIRECTOR_COMPONENT_MOTION_TREE_H

#include "directorcomponent.h"

#include "cranimation/animcache.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/motiontreescheduler.h"
#include "crmotiontree/nodefactory.h"

#if CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE
#include "system/stack_collector.h" 
#endif //CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE

#define ENABLE_ANIM_CACHE (0)

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that manages a motion tree
class fwAnimDirectorComponentMotionTree : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentMotionTree();

	// PURPOSE: Destructor
	~fwAnimDirectorComponentMotionTree();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentMotionTree);

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director, fragInst* frag=NULL, bool creatureFinalizePose=true);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Override to receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);
	

	// PURPOSE: Direct access to underlying motion tree
	crmtMotionTree& GetMotionTree();


	// PURPOSE: Is motion tree locked for update/compose
	bool IsLocked() const;

	// PURPOSE: Are all motion trees globally locked for update/compose
	static bool IsLockedGlobal(ePhase phase);
	static bool IsLockedGlobal();

	// PURPOSE: Set global locked state
	static void SetLockedGlobal(bool locked, ePhase phase);


	// PURPOSE: Has motion tree processing completed
	// NOTE: This is different to IsLocked
	// IsLocked returns true after the the post delegate is completed
	// IsComplete returns true after the tree has been fully recycled by the scheduler
	// (which is necessary in order to re-schedule the same tree)
	bool IsComplete(bool updateOnly=false) const;

	// PURPOSE: Wait on motion tree to complete processing
	void WaitOnComplete(bool updateOnly=false);

	// PURPOSE: Wait on *ALL* motion trees to complete processing
	static void WaitOnAllComplete();


	// PURPOSE: Schedule motion tree for update and compose
	void Schedule(float deltaTime, u8 priority=0, crmtMotionTree* parent=NULL, bool update=true, bool compose=true);


	// PURPOSE: Override derived class shutdown
	static void InitDerivedClass();

	// PURPOSE: Override derived class shutdown
	static void ShutdownDerivedClass();

#if CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE
	static sysStackCollector sm_LockedGlobalCallStacks;
#endif //CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE

private:

	// PURPOSE: Pre delegate - call after motion tree update, but before compose starts
	static void PreDelegate(void*);

	// PURPOSE: Mid delegate - call after motion tree update, but before compose starts
	static void MidDelegate(void*);

	// PURPOSE: Post delegate - called after motion tree update/compose completed
	static void PostDelegate(void*);

	crmtMotionTree m_MotionTree;

	bool m_Locked;

	static bool sm_LockedGlobal;
	static int sm_LockedGlobalPhase;

	static crmtMotionTreeScheduler* sm_Scheduler;
	static crmtNodeFactory* sm_NodeFactory;
#if ENABLE_ANIM_CACHE
	static crAnimCache sm_AnimCache;
#endif // ENABLE_ANIM_CACHE
};

////////////////////////////////////////////////////////////////////////////////

inline crmtMotionTree& fwAnimDirectorComponentMotionTree::GetMotionTree()
{
	return m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirectorComponentMotionTree::IsLocked() const
{
	return m_Locked;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirectorComponentMotionTree::IsLockedGlobal(ePhase phase)
{
	return sm_LockedGlobal && (sm_LockedGlobalPhase == phase);
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirectorComponentMotionTree::IsLockedGlobal()
{
	return sm_LockedGlobal;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirectorComponentMotionTree::SetLockedGlobal(bool locked, ePhase phase)
{
	sm_LockedGlobal = locked;
	sm_LockedGlobalPhase = phase;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirectorComponentMotionTree::IsComplete(bool updateOnly) const
{
	return sm_Scheduler->IsComplete(m_MotionTree, updateOnly);
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirectorComponentMotionTree::WaitOnComplete(bool updateOnly)
{
	const bool elevatePriority = true;
	sm_Scheduler->WaitOnComplete(m_MotionTree, elevatePriority, updateOnly);
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirectorComponentMotionTree::WaitOnAllComplete()
{
	sm_Scheduler->WaitOnAllComplete();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_MOTION_TREE_H

