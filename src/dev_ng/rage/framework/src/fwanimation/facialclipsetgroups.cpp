#include "fwanimation/facialclipsetgroups.h"
#include "fwanimation/anim_channel.h"
#include "fwscene/stores/clipdictionarystore.h"
#include "fwsys/gameskeleton.h"
#include "fwsys/timer.h"
#include "parser/manager.h"

using namespace rage;

#include "facialclipsetgroups_parser.h"

namespace rage {

	//////////////////////////////////////////////////////////////////////////

	const char *g_facialClipSetGroupsDirectory = "common:/data/anim/facial_clipset_groups";

	//////////////////////////////////////////////////////////////////////////

	fwFacialClipSetGroupManager fwFacialClipSetGroupManager::ms_instance;

	//////////////////////////////////////////////////////////////////////////

	const fwFacialClipSetVariationInfo* fwFacialClipSetGroup::GetVariationByMood(VariationMood mood)
	{
		for (int i=0; i<m_Variations.GetCount(); i++)
		{
			if (m_Variations[i].GetMood() == mood)
			{
				return &m_Variations[i];
			}
		}

		return NULL;
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::Init(u32 initMode)
	{
		if(initMode == INIT_CORE)
		{
			GetInstance().Load();

			// Create the score info array
			for (int i=0; i<ms_instance.m_facialClipSetGroups.GetCount(); i++)
			{
				fwFacialClipSetGroup** ppGroup = ms_instance.m_facialClipSetGroups.GetItem(i);
				if (ppGroup && *ppGroup)
				{
					fwFacialClipSetGroup* pGroup = *ppGroup;
					fwMvFacialClipSetGroupId groupId = *ms_instance.m_facialClipSetGroups.GetKey(i);

					for (int j=0; j<pGroup->GetVariationCount(); j++)
					{
						const fwFacialClipSetVariationInfo& rVarInfo = pGroup->GetVariationByIndex(j);

						fwVariationTracker* pNewScoreInfo = rage_new fwVariationTracker(groupId, &rVarInfo);
						animAssert(pNewScoreInfo);

						ms_instance.m_variationTrackers.PushAndGrow(pNewScoreInfo);
					}
				}
			}
		}
		else if(initMode == INIT_SESSION)
		{
			// Currently not required
		}

		ms_instance.m_bClipsetUnloadRequired = false;
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::Shutdown(u32 shutdownMode)
	{
		if(shutdownMode == SHUTDOWN_CORE)
		{
			// Reset
			Reset();
			
			// Delete loaded data
			GetInstance().DeleteAllFacialClipSetGroups();

			// Delete trackers
			for (int i=0; i<ms_instance.m_variationTrackers.GetCount(); i++)
			{
				fwVariationTracker* pTracker = ms_instance.m_variationTrackers[i];
				if (pTracker)
				{
					delete pTracker;
				}
			}
			ms_instance.m_variationTrackers.clear();
		}
		else if(shutdownMode == SHUTDOWN_SESSION)
		{		
			Reset();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::Reset()
	{
		for (int i=0; i<ms_instance.m_variationTrackers.GetCount(); i++)
		{
			fwVariationTracker* pTracker = ms_instance.m_variationTrackers[i];
			if (pTracker)
			{
				pTracker->Reset();
			}
		}
		ms_instance.m_bClipsetUnloadRequired = false;
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::Update()
	{
		if (fwTimer::IsGamePaused())
		{
			return;
		}

		// Get Delta Time
		const float fDeltaTime = fwTimer::GetTimeStep();

		// Number of variation clipset's in use
		int iNumClipSetsInUse = 0;

		// Sort the trackers based on score and clipset state
		std::sort(&ms_instance.m_variationTrackers[0], &ms_instance.m_variationTrackers[0] + ms_instance.m_variationTrackers.GetCount(), fwFacialClipSetGroupManager::fwVariationTracker::GreaterThanEqual);

		// Flag to indicate we are finished processing as many trackers as we want.
		bool bFinishedTrackerProcessing = false;
		float fLowestImportanceForLoadedClipset = 99999999.9f;

		// Update the variation trackers
		for (int i=0; i< ms_instance.m_variationTrackers.GetCount(); i++)
		{
			// These are sorted so that ones that are using clipsets are in the array first
			fwVariationTracker* pVarTracker = ms_instance.m_variationTrackers[i];
			animAssert(pVarTracker);

			// Increment time
			pVarTracker->m_fTimer += fDeltaTime;

			// Still need to process trackers fully
			if (!bFinishedTrackerProcessing)
			{
				// Clipset loading and scoring behaviour
				switch(pVarTracker->m_eClipSetState)
				{
					case VariationClipSetState_StreamingIn:
					{
						if (pVarTracker->m_clipSetRequestHelper.Request() && pVarTracker->m_clipSetRequestHelper.IsLoaded())
						{
							// Now fully loaded, set next state
							pVarTracker->SetState(VariationClipSetState_Loaded);
						}

						iNumClipSetsInUse++;
					}
					break;

					case VariationClipSetState_Loaded:
					{
						animAssert(pVarTracker->m_pVariationInfo);
						if (ms_instance.m_bClipsetUnloadRequired && pVarTracker->m_fTimer >= pVarTracker->m_pVariationInfo->GetMinimumResidentTime())
						{
							// Unloaded something
							ms_instance.m_bClipsetUnloadRequired = false;

							// Set next state
							pVarTracker->SetState(VariationClipSetState_StreamingOut);
						}
						else
						{
							if (pVarTracker->m_fImportance < fLowestImportanceForLoadedClipset)
							{
								fLowestImportanceForLoadedClipset = pVarTracker->m_fImportance;
							}
						}
						iNumClipSetsInUse++;
					}
					break;

					case VariationClipSetState_StreamingOut:
					{
						// Wait for reference count to reach 1 (then we know our clipset request helper is the last reference) before unloading
						bool bStillInUse = false;
						if (pVarTracker->m_clipSetRequestHelper.IsLoaded())
						{
							fwClipSet* pClipSet = pVarTracker->m_clipSetRequestHelper.GetClipSet();
							if (pClipSet)
							{
								int iDictIndex = pClipSet->GetClipDictionaryIndex().Get();
								if (iDictIndex != -1)
								{
									int iRefCount = g_ClipDictionaryStore.GetNumRefs(strLocalIndex(iDictIndex));
									if (iRefCount > 1)
									{
										bStillInUse = true;
									}
								}
							}
						}

						if (bStillInUse)
						{
							iNumClipSetsInUse++;
						}
						else
						{
							// Release/Unload the clipset
							pVarTracker->m_clipSetRequestHelper.Release();

							// Set next state
							pVarTracker->SetState(VariationClipSetState_UnLoaded);
						}
					}
					break;

					case VariationClipSetState_UnLoaded:
					{
						// Got to a tracker that is not loaded.  Check if:
						// 1. There is room to load it
						// 2. That it has been requested
						if (pVarTracker->m_fImportance > 0.0f)
						{
							if (iNumClipSetsInUse < MAX_LOADED_FACIAL_VARIATION_CLIP_SETS)
							{
								animAssert(pVarTracker->m_pVariationInfo);
								fwMvClipSetId clipSetId = fwClipSetManager::GetClipSetId(pVarTracker->m_pVariationInfo->GetClipSet());

								// Begin loading in the required clipset
								pVarTracker->m_clipSetRequestHelper.Request(clipSetId);

								// Set next state
								pVarTracker->SetState(VariationClipSetState_StreamingIn);

								// Now using a clipset
								iNumClipSetsInUse++;
							}
							else if (iNumClipSetsInUse > 0 && pVarTracker->m_fImportance >= fLowestImportanceForLoadedClipset)
							{
								ms_instance.m_bClipsetUnloadRequired = true;
							}								
						}
						else
						{
							bFinishedTrackerProcessing = true;
						}
					}
					break;

					default:
					{
						// Do nothing
					}
					break;
				}
			}

			// Reset importance each frame
			pVarTracker->m_fImportance = 0.0f;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	bool fwFacialClipSetGroupManager::Load()
	{
		USE_MEMBUCKET(MEMBUCKET_ANIMATION);

		bool bResult = true;

		GetInstance().DeleteAllFacialClipSetGroups();

		ASSET.PushFolder(g_facialClipSetGroupsDirectory);

		Displayf("Loading facial_clipset_groups.xml...\n");

		bResult = PARSER.LoadObject("facial_clipset_groups", "xml", GetInstance());
		Assertf(bResult, "Load facial_clipset_groups.xml failed!\n");

		Displayf("Loaded facial_clipset_groups.xml.\n");
		Displayf("FacialClipSetGroups: %i", GetInstance().GetFacialClipSetGroupCount());

		ASSET.PopFolder();

		return bResult;
	}

	bool fwFacialClipSetGroupManager::Append(const char *fname)
	{
		USE_MEMBUCKET(MEMBUCKET_ANIMATION);

		bool bResult = true;

		Displayf("Appending facial_clipset_groups.xml...\n");

		fwFacialClipSetGroupManager temp_inst;
		bResult = PARSER.LoadObject(fname, NULL, temp_inst);
		Assertf(bResult, "Load %s failed!\n", fname);

		Displayf("Loaded %s.\n", fname);
		Displayf("FacialClipsetGroups: %i", temp_inst.GetFacialClipSetGroupCount());
		
		AppendBinMap(GetInstance().m_facialClipSetGroups, temp_inst.m_facialClipSetGroups, AppendBinMapFreeObjectPtr<fwFacialClipSetGroup>);
		
		return bResult;
	}

	//////////////////////////////////////////////////////////////////////////

#if __BANK

	bool fwFacialClipSetGroupManager::Save()
	{
		bool bResult = true;

		ASSET.PushFolder(g_facialClipSetGroupsDirectory);

		Displayf("Saving facial_clipset_groups.xml...\n");

		bResult = PARSER.SaveObject("facial_clipset_groups", "xml", &GetInstance());

		Displayf("Saved facial_clipset_groups.xml.\n");

		ASSET.PopFolder();

		return bResult;
	}

#endif // __BANK

	//////////////////////////////////////////////////////////////////////////

	fwMvFacialClipSetGroupId fwFacialClipSetGroupManager::GetFacialClipSetGroupId(const atHashString& facialClipSetGroupName)
	{
		fwMvFacialClipSetGroupId facialClipSetGroupId(facialClipSetGroupName);

		if(GetFacialClipSetGroup(facialClipSetGroupId))
		{
			return facialClipSetGroupId;
		}

		return FACIAL_CLIP_SET_GROUP_ID_INVALID;
	}

	//////////////////////////////////////////////////////////////////////////

	fwFacialClipSetGroup *fwFacialClipSetGroupManager::GetFacialClipSetGroup(const fwMvFacialClipSetGroupId &facialClipSetGroupId)
	{
		if(facialClipSetGroupId != FACIAL_CLIP_SET_GROUP_ID_INVALID)
		{
			fwFacialClipSetGroup **ppFacialClipSetGroup = ms_instance.m_facialClipSetGroups.SafeGet(facialClipSetGroupId);
			if(ppFacialClipSetGroup)
			{
				return *ppFacialClipSetGroup;
			}
		}

		return NULL;
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::DeleteAllFacialClipSetGroups()
	{
		for(int i = 0; i < ms_instance.m_facialClipSetGroups.GetCount(); i ++)
		{
			delete ms_instance.m_facialClipSetGroups.GetRawDataArray()[i].data;
			ms_instance.m_facialClipSetGroups.GetRawDataArray()[i].data = NULL;
		}
		ms_instance.m_facialClipSetGroups.Reset();
	}

	//////////////////////////////////////////////////////////////////////////

	const fwFacialClipSetVariationInfo* fwFacialClipSetGroupManager::RequestVariationsAndGetVariationInfo(const fwMvFacialClipSetGroupId& facialClipSetGroupId, VariationMood mood, float fImportance)
	{
		if (facialClipSetGroupId != FACIAL_CLIP_SET_GROUP_ID_INVALID && fImportance > 0.0f)
		{
			for (int i=0; i<ms_instance.m_variationTrackers.GetCount(); i++)
			{
				fwVariationTracker* pVarTracker = ms_instance.m_variationTrackers[i];
				if (pVarTracker && pVarTracker->m_groupId == facialClipSetGroupId && pVarTracker->m_pVariationInfo->GetMood() == mood)
				{
					pVarTracker->m_fImportance += fImportance;
					return pVarTracker->m_pVariationInfo;
				}
			}
		}

		return NULL;
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::GetVariationClipSetAndClip(const fwMvFacialClipSetGroupId &facialClipSetGroupId, VariationMood mood, fwMvClipSetId& varClipSetIdOut, fwMvClipId& varClipIdOut)
	{
		varClipSetIdOut = CLIP_SET_ID_INVALID;
		varClipIdOut = CLIP_ID_INVALID;

		if (facialClipSetGroupId != FACIAL_CLIP_SET_GROUP_ID_INVALID)
		{
			for (int i=0; i<ms_instance.m_variationTrackers.GetCount(); i++)
			{
				fwVariationTracker* pVarTracker = ms_instance.m_variationTrackers[i];
				if (pVarTracker && pVarTracker->m_eClipSetState == VariationClipSetState_Loaded && pVarTracker->m_groupId == facialClipSetGroupId && pVarTracker->m_pVariationInfo->GetMood() == mood)
				{
					fwMvClipSetId loadedClipSetId = pVarTracker->m_clipSetRequestHelper.GetClipSetId();
					if (loadedClipSetId != CLIP_SET_ID_INVALID)
					{
						fwClipSet* pClipSet = pVarTracker->m_clipSetRequestHelper.GetClipSet();
						if (pClipSet)
						{
							fwMvClipId loadedRandomClipId;
							if (pClipSet->PickRandomClip(loadedRandomClipId))
							{
								varClipSetIdOut = loadedClipSetId;
								varClipIdOut = loadedRandomClipId;
								return;
							}
						}
					}
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

#if __BANK

	void fwFacialClipSetGroupManager::DumpTrackerDebugInfo(atString& string, const int MaxTrackersToDisplay)
	{
		char buffer[512];

		// Number of trackers total
		sprintf(buffer, "%d Facial Variation Trackers:\n", ms_instance.m_variationTrackers.GetCount());
		string += buffer;

		const int numTrackersToDisplay = Min(ms_instance.m_variationTrackers.GetCount(), MaxTrackersToDisplay);
		for (int i=0; i<numTrackersToDisplay; i++)
		{
			string += "Tracker: ";

			fwVariationTracker* pVarTracker = ms_instance.m_variationTrackers[i];
			if (pVarTracker)
			{
				// Group
				if (pVarTracker->m_groupId.TryGetCStr())
				{
					sprintf(buffer, "%s - ", pVarTracker->m_groupId.TryGetCStr());
				}
				else
				{
					sprintf(buffer, "%u - ", pVarTracker->m_groupId.GetHash());
				}
				string += buffer;

				// Mood
				if (pVarTracker->m_pVariationInfo)
				{
					if (pVarTracker->m_pVariationInfo->GetMood() == MoodAiming)
					{
						string += "Aiming: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodAngry)
					{
						string += "Angry: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodInjured)
					{
						string += "Injured: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodHappy)
					{
						string += "Happy: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodNormal)
					{
						string += "Normal: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodStressed)
					{
						string += "Stressed: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodExcited)
					{
						string += "Excited: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodFrustrated)
					{
						string += "Frustrated: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodTalking)
					{
						string += "Talking: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodDancingLow)
					{
						string += "DancingLow: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodDancingMed)
					{
						string += "DancingMed: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodDancingHigh)
					{
						string += "DancingHigh: ";
					}
					else if (pVarTracker->m_pVariationInfo->GetMood() == MoodDancingTrance)
					{
						string += "DancingTrance: ";
					}
				}
				
				// Load State
				if (pVarTracker->m_eClipSetState == VariationClipSetState_UnLoaded)
				{
					string += "Unloaded, ";
				}
				else if (pVarTracker->m_eClipSetState == VariationClipSetState_StreamingIn)
				{
					string += "Streaming In, ";
				}
				else if (pVarTracker->m_eClipSetState == VariationClipSetState_Loaded)
				{
					string += "Loaded, ";
				}
				else if (pVarTracker->m_eClipSetState == VariationClipSetState_StreamingOut)
				{
					string += "Streaming Out, ";
				}

				// Importance
				sprintf(buffer, "Importance: %.2f", pVarTracker->m_fImportance);
				string += buffer;

				// end of line
				string += "\n";
			}
		}
	}
#endif // __BANK

	//////////////////////////////////////////////////////////////////////////

	fwFacialClipSetGroupManager::fwVariationTracker::fwVariationTracker(fwMvFacialClipSetGroupId groupId, const fwFacialClipSetVariationInfo* pVariationInfo)
	{
		animAssert(pVariationInfo);
		m_groupId = groupId;
		m_pVariationInfo = pVariationInfo;
		m_eClipSetState = fwFacialClipSetGroupManager::VariationClipSetState_UnLoaded;
		m_fImportance = 0.0f;
		m_fTimer = 0.0f;
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::fwVariationTracker::Reset()
	{
		m_clipSetRequestHelper.Release();
		m_eClipSetState = fwFacialClipSetGroupManager::VariationClipSetState_UnLoaded;
		m_fImportance = 0.0f;
		m_fTimer = 0.0f;
	}

	//////////////////////////////////////////////////////////////////////////

	bool fwFacialClipSetGroupManager::fwVariationTracker::GreaterThanEqual(const fwVariationTracker* a, const fwVariationTracker* b)
	{
		animAssert(a);
		animAssert(b);

		// This number must be higher than any attainable value of importance, so that any clipset that is not "unloaded" is sorted to the front of the array.
		const float STATE_SORTING_BOOST = 65536.0f;

		// This will order everything based on importance, but most importantly clipsets currently in use will be at the front of the array.
		const float valueA = a->m_fImportance + ((float)a->m_eClipSetState * STATE_SORTING_BOOST);
		const float valueB = b->m_fImportance + ((float)b->m_eClipSetState * STATE_SORTING_BOOST);

		return (valueA > valueB);
	}

	//////////////////////////////////////////////////////////////////////////

	void fwFacialClipSetGroupManager::fwVariationTracker::SetState(VariationClipSetState newState)
	{
		// Should never set to the same state
		animAssert(m_eClipSetState != newState);

		m_eClipSetState = newState;
		m_fTimer = 0.0f;
	}

} // namespace rage