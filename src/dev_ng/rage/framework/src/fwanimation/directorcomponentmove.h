// 
// fwanimation/directorcomponentmove.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_MOVE_H
#define FWANIMATION_DIRECTOR_COMPONENT_MOVE_H

#include "directorcomponent.h"

#include "move/move_command.h"
#include "move/move_config.h"

namespace rage
{

class fwMove;
class fwMoveNetworkPlayer;
class mvMotionWeb;
class mvNetworkDef;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that manages MoVE
class fwAnimDirectorComponentMove : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentMove();

	// PURPOSE: Destructor
	~fwAnimDirectorComponentMove();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentMove);

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Component priority
	virtual u32 GetPriority() const;

	// PURPOSE: Receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);


	// PURPOSE: Access to underlying fwMove object
	fwMove& GetMove() const;

	// PURPOSE: Is there a valid move instance
	bool HasMoveInstance() const;

	// PURPOSE: Get the head of the network player chain
	fwMoveNetworkPlayer* GetNetworkPlayers() const;

	// PURPOSE: Is this player is attached to this component?
	bool HasNetworkPlayer(const fwMoveNetworkPlayer* player) const;

	// PURPOSE: Creates a new move network player to be inserted into another network.
	//  The player will continue to exist until it becomes unused
	fwMoveNetworkPlayer* CreateNetworkPlayer(const mvNetworkDef& def);

	// PURPOSE: Deletes any network players that have no refs from tasks
	void DeleteUnusedNetworkPlayers();

#if __BANK
	// PURPOSE: Search for the network player that owns the given motion web
	// Useful for rendering the contents of parameter buffers etc in __DEV builds.
	fwMoveNetworkPlayer* FindMoveNetworkPlayer(const mvMotionWeb* pWeb) const;
#endif //__BANK

	// PURPOSE: Get the number of network players added since last call to DeleteUnusedNetworkPlayers
	u32 GetNumNetworkPlayersAddedSinceLastDelete() const;

	// PURPOSE: Update audio and triggered facial events
	void UpdateAudioAndTriggeredFacialEvents();

	// PURPOSE: Swap output parameter buffers
	void SwapOutputBuffers();
	

	// PURPOSE: Override derived class initialization
	static void InitDerivedClass();

	// PURPOSE: Override derived class shutdown
	static void ShutdownDerivedClass();

	// PURPOSE: Override derived class update
	static void UpdateDerivedClass();


	// PURPOSE: Get command buffer manager
	static mvCommandBuffer::Manager& GetCommandBufferManager();

private:

	fwMove* m_Move;

	fwMoveNetworkPlayer* m_NetworkPlayers;

	bool m_OutputBuffersSwapped;
	bool m_PostInit;

	u32 m_NumNetworkPlayersAddedSinceLastDelete;

	static mvCommandBuffer::Manager sm_CommandBufferManager;
};

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirectorComponentMove::HasMoveInstance() const
{
	return m_Move!=NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline fwMove& fwAnimDirectorComponentMove::GetMove() const
{
	animFastAssert(m_Move);
	return *m_Move;
}

////////////////////////////////////////////////////////////////////////////////

inline fwMoveNetworkPlayer* fwAnimDirectorComponentMove::GetNetworkPlayers() const
{
	return m_NetworkPlayers;
}

////////////////////////////////////////////////////////////////////////////////

inline mvCommandBuffer::Manager& fwAnimDirectorComponentMove::GetCommandBufferManager()
{
	return sm_CommandBufferManager;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_MOVE_H

