// 
// fwanimation/directorcomponentexpressions.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_EXPRESSIONS_H
#define FWANIMATION_DIRECTOR_COMPONENT_EXPRESSIONS_H

#include "animdefines.h"
#include "directorcomponent.h"

#include "cranimation/frame.h"
#include "crmotiontree/observer.h"

namespace rage
{

class crExpressions;
class crmtNodeExpression;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that manages expressions
class fwAnimDirectorComponentExpressions : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentExpressions();

	// PURPOSE: Destructor
	~fwAnimDirectorComponentExpressions();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentExpressions);

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director, const crmtObserver* rootObserver=NULL);

	// PURPOSE: Shutdown
	virtual void Shutdown();


	// PURPOSE: Receive messages
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);


	// PURPOSE: Append an expression in the motion tree
	bool AppendExpression(const crExpressions& expr);

	// PURPOSE: Remove an individual expression
	// RETURNS: true - expression successfully removed, false - expression not found
	bool RemoveExpression(const crExpressions& expr);

	// PURPOSE: Remove all expressions
	void RemoveAllExpressions();


	// PURPOSE: Has an individual expression
	// RETURNS: true - expression found, false - expression not found
	bool HasExpression(const crExpressions& expr) const;

	// PURPOSE: Have any expressions been added
	bool HasExpressions() const;


	// PURPOSE: Starts tracking expression use 
	// NOTE: Call RemoveUnusedExpressions() to remove unused expressions when finished adding/testing expressions
	void ClearUnused();

	// PURPOSE: End tracking expression use, removing all unused expressions
	// NOTE: Must call ClearUnused() first, 
	// and then AddExpression/HasExpression on all currently used expressions
	void RemoveUnusedExpressions();


	// PURPOSE: Override derived class initialization
	static void InitDerivedClass();

	// PURPOSE: Override derived class shutdown
	static void InitShutdownClass();

#if DEBUG_EXPRESSIONS
	// PURPOSE: Get debug input frame
	crFrame& GetDebugFrameIn();

	// PURPOSE: Get debug output frame
	crFrame& GetDebugFrameOut();

	// PURPOSE: Get debug override frame
	crFrame& GetDebugFrameOverride();

	// PURPOSE: Get array of expression observers
	atArray<crmtObserverTyped<crmtNodeExpression>* >& GetDebugObservers();
#endif // DEBUG_EXPRESSIONS

private:

	crmtObserver m_RootObserver;

#if DEBUG_EXPRESSIONS
	crFrame m_DebugFrameIn;
	crFrame m_DebugFrameOut;
	crFrame m_DebugFrameOverride;
	atArray<crmtObserverTyped<crmtNodeExpression>* > m_DebugObservers;
#endif // DEBUG_EXPRESSIONS

	mutable u32 m_IsUsed;
};

////////////////////////////////////////////////////////////////////////////////

#if DEBUG_EXPRESSIONS
inline crFrame& fwAnimDirectorComponentExpressions::GetDebugFrameIn()
{
	return m_DebugFrameIn;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame& fwAnimDirectorComponentExpressions::GetDebugFrameOut()
{
	return m_DebugFrameOut;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame& fwAnimDirectorComponentExpressions::GetDebugFrameOverride()
{
	return m_DebugFrameOverride;
}

////////////////////////////////////////////////////////////////////////////////

inline atArray<crmtObserverTyped<crmtNodeExpression>* >& fwAnimDirectorComponentExpressions::GetDebugObservers()
{
	return m_DebugObservers;
}
#endif // DEBUG_EXPRESSIONS

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_EXPRESSIONS_H

