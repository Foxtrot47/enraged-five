<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="fwExpressionSet">
    <string name="m_dictionaryName" type="atHashString" />
    <array name="m_expressions" type="atArray">
      <string type="atHashString" />
    </array>
  </structdef>

  <structdef type="fwExpressionSetManager">
    <map name="m_expressionSets" type="atBinaryMap" key="atHashString">
      <pointer type="fwExpressionSet" policy="owner" />
    </map>
  </structdef>
  
</ParserSchema>
