// 
// animation/AnimDefines.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

// Rage headers
#include "atl/string.h"

// Game headers
#include "anim_channel.h"
#include "animdefines.h"

namespace rage {

const fwMvFloatId FLOAT_ID_INVALID(fwMvFloatId::Null());
const fwMvBooleanId BOOLEAN_ID_INVALID(fwMvBooleanId::Null());

const fwMvAnimId ANIM_ID_INVALID(fwMvAnimId::Null());

const fwMvClipId CLIP_ID_INVALID(fwMvClipId::Null());
const fwMvClipSetId CLIP_SET_ID_INVALID(fwMvClipSetId::Null());
const fwMvClipSetVarId CLIP_SET_VAR_ID_INVALID(fwMvClipSetVarId::Null());
const fwMvClipSetVarId CLIP_SET_VAR_ID_DEFAULT("default",0xE4DF46D5);
const fwMvFilterId FILTER_ID_INVALID(fwMvFilterId::Null());

const fwMvExpressionSetId EXPRESSION_SET_ID_INVALID(fwMvExpressionSetId::Null());
const fwMvExpressionsId EXPRESSIONS_ID_INVALID(fwMvExpressionsId::Null());

const fwMvFacialClipSetGroupId FACIAL_CLIP_SET_GROUP_ID_INVALID(fwMvFacialClipSetGroupId::Null());

const fwMvClipVariationSetId CLIP_VARIATION_SET_ID_INVALID(fwMvClipVariationSetId::Null());

fwMvStateId::operator fwMvRequestId() const
{
	return fwMvRequestId(GetHash());
}

fwMvRequestId::operator fwMvStateId() const
{
	return fwMvStateId(GetHash());
}

void eStreamingPriority_ToString(const eStreamingPriority val, class atString &string)
{
	string = "";
	if		(val == SP_Invalid	) { string += "SP_Invalid"; }
	else if	(val == SP_Variation) { string += "SP_Variation"; }
	else if	(val == SP_Low		) { string += "SP_Low"; }
	else if	(val == SP_Medium	) { string += "SP_Medium"; }
	else if	(val == SP_High		) { string += "SP_High"; }
}

void eStreamingPolicy_ToString(const eStreamingPolicy val, atString &string)
{
	string = "";
	if(val & SP_STREAMING			  ) {                                                 string += "SP_STREAMING"; }
	if(val & SP_SINGLEPLAYER_RESIDENT ) { if(string.GetLength() > 0) { string += " | "; } string += "SP_SINGLEPLAYER_RESIDENT"; }
	if(val & SP_MULTIPLAYER_RESIDENT  ) { if(string.GetLength() > 0) { string += " | "; } string += "SP_MULTIPLEY_RESIDENT"; }
}

const char *eAnimPriority_ToString(const eAnimPriority val)
{
	switch(val)
	{
		case AP_LOW: return "AP_LOW";
		case AP_MEDIUM: return "AP_MEDIUM";
		case AP_HIGH: return "AP_HIGH";
		case NUM_EANIMPRIORITY: return "NUM_EANIMPRIORITY";
	}
	return NULL;
}

const char *eTrackType_ToString(const eTrackType val)
{
	switch(val)
	{
	case kTrackParticleEffect0: return "kTrackParticleEffect0";
	case kTrackParticleEffect1: return "kTrackParticleEffect1";
	case kTrackParticleEffect2: return "kTrackParticleEffect2";
	case kTrackParticleEffect3: return "kTrackParticleEffect3";
	case kTrackParticleEffect4: return "kTrackParticleEffect4";
	case kTrackUpperBodyFixupWeight: return "kTrackUpperBodyFixupWeight";
	case kTrackIndependentMover: return "kTrackIndependentMover";
	case kTrackRootHeight: return "kTrackRootHeight";
	case kTrackIndependentMoverWeight: return "kTrackIndependentMoverWeight";
	case kTrackFirstPersonCameraWeight: return "kTrackFirstPersonCameraWeight";
	case kTrackConstraintHelperLeftHandWeight: return "kTrackConstraintHelperLeftHandWeight";
	case kTrackConstraintHelperRightHandWeight: return "kTrackConstraintHelperRightHandWeight";
	case kTrackUpperBodyFixupShadow: return "kTrackUpperBodyFixupShadow";
	case kTrackUpperBodyFixupScratch: return "kTrackUpperBodyFixupScratch";
	}
	return NULL;
}

const fwSyncedSceneId INVALID_SYNCED_SCENE_ID = -1;

} // namespace rage

//////////////////////////////////////////////////////////////////////////
