#ifndef EXPRESSIONSETS_H_
#define EXPRESSIONSETS_H_

#include "atl/binmap.h"
#include "fwanimation/animdefines.h"
#include "fwdebug/debugbank.h"
#include "parser/macros.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

class fwExpressionSet
{
public:

	fwExpressionSet() {};
	~fwExpressionSet() {};

	inline const atHashString& GetDictionaryName() const { return m_dictionaryName; }
	inline const atArray< atHashString > &GetExpressions() const { return m_expressions; }

	bool ContainsExpression(const atHashString& hashToCompare);

protected:

	atHashString m_dictionaryName;
	atArray< atHashString > m_expressions;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwExpressionSetManager
{
public:

	// Static properties

	static inline fwExpressionSetManager &GetInstance() { return ms_instance; }

	static void Init(u32 initMode);
	static void Shutdown(u32 shutdownMode);

	static bool Load();
	static bool Append(const char *fname);

#if __BANK
	static bool Save();
#endif // __BANK

	static inline int GetExpressionSetCount() { return ms_instance.m_expressionSets.GetCount(); }

	static fwMvExpressionSetId GetExpressionSetId(const atHashString& expressionSetName);
	static fwExpressionSet *GetExpressionSet(const fwMvExpressionSetId &expressionSetId);

	static void DeleteAllExpressionSets();

protected:

	// Static data

	static fwExpressionSetManager ms_instance;

	// Data

	atBinaryMap< fwExpressionSet *, fwMvExpressionSetId > m_expressionSets;

	PAR_SIMPLE_PARSABLE;
};

} // rage

#endif // EXPRESSIONSETS_H_