#ifndef FWANIMATION_MOVE_H
#define FWANIMATION_MOVE_H

// rage includes
#include "atl/functor.h"
#include "fwtl/pool.h"
#include "atl/array.h"
#include "atl/hashstring.h"
#include "crmetadata/property.h"
#include "crmotiontree/motiontree.h"
#include "fwanimation/anim_channel.h"
#include "fwanimation/animmanager.h"
#include "fwanimation/clipsets.h"
#include "fwutil/Flags.h"
#include "move/move.h"
#include "move/move_command.h"
#include "move/move_fixedheap.h"
#include "move/move_parameterbuffer.h"
#include "move/move_subnetwork.h"
#include "paging/ref.h"
#include "physics/debugPlayback.h"	//Code in here needs to go somewhere else!

#define MOVE_DR_ENABLED 1

namespace rage {

class fwEntity;
class fwMove;
class fwAnimDirector;

#if __DEV
#define CHECK_MOVE_BUFFER CheckParameterBuffer(3)
#else 
#define CHECK_MOVE_BUFFER
#endif


// PURPOSE: Interface to Motion tree Visual Editor (MoVE)
class fwMoveNetworkInterface
{

public:

	// PURPOSE: Id, hash to identify signals/flags/events
	typedef s32 Id;

	fwMoveNetworkInterface();

	virtual ~fwMoveNetworkInterface();

	// PURPOSE: Set the value of a float signal parameter
	void SetFloat( const fwMvFloatId &floatId, float value );

	// PURPOSE: Get the value of a float signal parameter
	float GetFloat( const fwMvFloatId &floatId );

	// PURPOSE: Get the value of a float signal parameter, with existence test
	bool GetFloat( const fwMvFloatId &floatId, float& value );

	// PURPOSE: Set the value of a boolean signal parameter / event
	void SetBoolean( const fwMvBooleanId &booleanId, bool value );

	// PURPOSE: Get the value of a boolean signal parameter / event
	bool GetBoolean( const fwMvBooleanId &booleanId );

	// PURPOSE: Get the value of a boolean signal parameter / event, with existence test
	bool GetBoolean( const fwMvBooleanId &booleanId, bool& value );

	// PURPOSE: Set the value of a flag in the network
	void SetFlag( const fwMvFlagId &flagId, bool value );

	// PURPOSE: Get the value of a flag in the network
	//void GetFlag( Id id, bool value );

	// PURPOSE: Set an animation pointer in the network
	void SetAnimation( const fwMvAnimId &animId, const crAnimation* anim );

	// PURPOSE: Return a pointer to the animation currently assigned to the given parameter id
	const crAnimation* GetAnimation( const fwMvAnimId &animId );

	// PURPOSE:
	void SetClip( const fwMvClipId &clipId, const crClip* clip);

	// PURPOSE: Return a pointer to the clip currently assigned to the given parameter id
	const crClip* GetClip( const fwMvClipId &clipId);

	// PURPOSE: 
	void SetParameterizedMotion( const fwMvParameterizedMotionId &pmId, crpmParameterizedMotion* pm);

	// PURPOSE: Return a pointer to the PM currently assigned to the given parameter id
	const crpmParameterizedMotion* GetParameterizedMotion( const fwMvParameterizedMotionId &pmId);

	// PURPOSE:
	void SetExpressions( const fwMvExpressionsId &exprId, const crExpressions* expressions);

	// PURPOSE: Return a pointer to the Expressions currently assigned to the given parameter id
	const crExpressions* GetExpressions( const fwMvExpressionsId &exprId);

	// PURPOSE:
	void SetFilter( const fwMvFilterId &filterId, crFrameFilter* filter);

	// PURPOSE: Return a pointer to the filter currently assigned to the given parameter id
	const crFrameFilter* GetFilter( const fwMvFilterId &filterId);

	// PURPOSE: Passes an observer to the network
	void SetObserver( const fwMvObserverId &observerId, mvObserver* observer);

	// PURPOSE:
	void SetFrame( const fwMvFrameId &frameId, crFrame* frame);

	// PURPOSE: Return a pointer to the frame currently assigned to the given parameter id
	const crFrame* GetFrame( const fwMvFrameId &frameId);

	// PURPOSE: return a pointer to the clip property returned by the output param with name hash id
	const crProperty* GetProperty( const fwMvPropertyId &propId);

	// PURPOSE: Returns true if an output control parameter with the given id exists in the specified network / subnetwork
	bool HasOutputParameter( const fwMvId &id);

	// PURPOSE: Returns true if an output control parameter with the given id and type exists in the specified network / subnetwork
	bool HasOutputParameter( const fwMvId &id, mvParameter::ParamType type);

	// PURPOSE: Use this to attach a subnetwork to the network
	void SetSubNetwork(const fwMvNetworkId &networkId, mvSubNetwork* value);

	// PURPOSE: Use this to set the cross blend duration when THIS NETWORK is inserted as a subnetwork.
	// NOTE:	This differs from the SetSubNetworkBlendOutDuration below for technical reasons. Hopefully
	//			we can get rid of this inconsistency with proper tool support for cross blend durations on subnetworks.
	void SetSubNetworkBlendDuration( float duration );

	// PURPOSE: Use this to set the cross blend duration when ANOTHER NETWORK is inserted into this one as a subnetwork.
	void SetSubNetworkBlendOutDuration( float duration );

	// PURPOSE: Sends a request to the network
	void BroadcastRequest(const fwMvRequestId &requestId);

	// PURPOSE: Forces a state transition within the state with Id parent, to the state with Id id
	void ForceState(const fwMvStateId &parentId, const fwMvStateId &id);

	// PURPOSE: Set the override clip set for this network
	//			This means that any new nodes created by this network
	//			(for  example when transitioning to a new state) will
	//			use this clip set instead of the one specified in the
	//			network.
	void SetClipSet(const fwMvClipSetId &clipSetId, const fwMvClipSetVarId &clipSetVarId = CLIP_SET_VAR_ID_DEFAULT);

	// PURPOSE: Clear all clip sets in the currently in the clip set map
	void ClearClipSets();

	// PURPOSE: Get the override clip set currently specified for this network
	fwClipSet* GetClipSet(const fwMvClipSetVarId &clipSetVarId = CLIP_SET_VAR_ID_DEFAULT) const;
	const fwMvClipSetId GetClipSetId(const fwMvClipSetVarId &clipSetVarId = CLIP_SET_VAR_ID_DEFAULT) const;
	const atMap< fwMvClipSetVarId, fwMvClipSetId > &GetClipSetIdMap() const { return m_clipSetIdMap; }

	// PURPOSE: Useful helper method for finding the most recently generated of a list of
	//			events in the last frame. Useful for example when using a group of state
	//			enter events as event markers.
	fwMvId GetLastEventThisFrame(const fwMvId eventsToSearch[], u32 eventCount);

	// PURPOSE: Clears any parameters in the output buffer with ids matching the 
	//			members of idsToMatch.
	bool ClearOutputParameters(atArray<fwMvId>& idsToMatch);

#if __BANK

	// PURPOSE: Locate the name of the network definition this network
	//			was initialized with
	const char * FindNetworkDefName() const;

	// PURPOSE: Adds string representations of the contents of the 
	//			output parameter buffer to the atString string.
	void DumpOutputParamBuffer(atString& string);
	void DumpInputParamBuffer(atString& );

#endif //__BANK

	virtual bool IsUsingRagDoll(fwEntity & /*entity*/) const			{ return false; }

	virtual bool GetShouldBeLowLOD(fwEntity & /*entity*/) const			{ return false; }

#if __DEV
	bool CheckParameterChainLength(int bufferLimit);
	void CheckParameterBuffer(int bufferLimit);
#endif

	mvCommandBuffer& GetInputBuffer() { return m_InputBuffers.GetCurrentBuffer(); }
	mvCommandBuffers& GetInputBuffers() { return m_InputBuffers; }

	mvParameterBuffer& GetOutputBuffer() { return m_OutputBuffers.GetCurrentBuffer(); }
	mvParameterBuffers& GetOutputBuffers() { return m_OutputBuffers; }

	DR_DEV_ONLY(static bool smb_RecordAnimationCalls);

	// PURPOSE: Will be called before the motion tree updates
	void PreMotionTreeUpdate();

	// PURPOSE: Will be called after the motion tree finishes updating
	void PostMotionTreeUpdate();

	mvMotionWeb* GetNetwork() const { return m_pNetwork; }

	void UpdateAudioAndTriggeredFacialEvents(fwEntity& entity);

	void SwapInputBuffers();

	void SwapOutputBuffers();

protected:

	// Find the first output parameter with the given id
	const mvParameter* FindOutputParameter(const fwMvId &id);
	// Find the first output parameter with the given name and type
	const mvParameter* FindOutputParameter(const fwMvId &id, mvParameter::ParamType type);

	// PURPOSE: Registers the relevant network / subnetwork pointer with the interface
	void RegisterNetwork(mvNetwork* pNetwork) {m_pNetwork = pNetwork;}
	void RegisterSubNetwork(mvSubNetwork* pNetwork) {m_pNetwork = pNetwork;}

#if __DEV
	virtual const fwAnimDirector& AnimDirector() const = 0;
#endif

#if DR_ENABLED && __DEV
	void RecordSetAnimValue(const char *pFilterName, const crFrameFilter *pFrameFilter) const;
	void RecordSetExpressions(const char *pExpressionsName) const;
	void RecordSetAnimValue(const char *pStringName, 
							const char *pString1id, const char *pString1, 
							const char *pString2id=0, const char *pString2=0) const;
	void RecordSetAnimValue(const char *pClipName, const crClip *clip) const;
	void RecordGetAnimValue(const char *pValueName, bool bValue) const;
	void RecordSetAnimValue(const char *pValueName, bool bValue) const;
	void RecordGetAnimValue(const char *pValueName, float fValue) const;
	void RecordSetAnimValue(const char *pValueName, float fValue) const;
	void SetShuttingDown(){m_bShuttingDown = true;}
#endif //DR_ENABLED

	mvCommandBuffers m_InputBuffers;

	mvParameterBuffers m_OutputBuffers;
	bool m_OutputBuffersSwapped;

	atMap< fwMvClipSetVarId, fwMvClipSetId > m_clipSetIdMap; // The current override clip set specified for this network

	mvMotionWeb* m_pNetwork;					// A pointer to the MoVE network or subnetwork this interface talks to

	DR_DEV_ONLY(bool m_bShuttingDown;)
};

//////////////////////////////////////////////////////////////////////////




// PURPOSE: 
class fwMoveNetworkPlayer : public fwMoveNetworkInterface
{
	friend class fwAnimDirector;
	friend class fwAnimDirectorComponentMove;

protected:	// constructor is protected so that only fwAnimDirector can create them

	// PURPOSE:
	fwMoveNetworkPlayer(fwAnimDirector& director, const mvNetworkDef& networkDef);

	// PURPOSE:
	virtual ~fwMoveNetworkPlayer();

	enum flags
	{
		Flag_WatchForEvent = BIT0,
		Flag_EventFound = BIT1,
		Flag_EventFoundThisFrame = BIT2,
		Flag_ReadyForInsert = BIT3,
		Flag_Inserted = BIT4,
		Flag_BlendingOut = BIT5,
		Flag_Expired = BIT6
	};

public:

	// PURPOSE:
	rage::mvSubNetwork* GetSubNetwork() {return m_SubNetwork;}

	// PURPOSE:
	fwAnimDirector& GetAnimDirector() { animAssert(m_Director); return *m_Director; }

	inline void AddRef() { m_NumRefs++; }

	inline void Release()
	{ 
		animAssert(m_NumRefs);
		m_NumRefs--;

		if (m_NumRefs<1)
		{
			delete this;
		}
	}

	inline u32 GetNumRefs()
	{
		return m_NumRefs;
	}

	inline fwMoveNetworkPlayer* GetNextPlayer() const		{ return m_NextPlayer; }
	inline void SetNextPlayer(fwMoveNetworkPlayer* player)	{ m_NextPlayer = player; }


	//////////////////////////////////////////////////////////////////////////
	// Event watching mechanism
	//////////////////////////////////////////////////////////////////////////

	inline void WatchForEvent(const fwMvBooleanId &eventId) 
	{
		m_EventWatch = eventId;
		m_flags.SetFlag(Flag_WatchForEvent);
		m_flags.ClearFlag(Flag_EventFound);
		m_flags.ClearFlag(Flag_EventFoundThisFrame);
		if (GetBoolean(m_EventWatch))
		{
			SetEventFound();
		}
	}

	inline bool EventFound() const { return m_flags.IsFlagSet(Flag_EventFound); }

	inline bool EventFoundThisFrame() const { return m_flags.IsFlagSet(Flag_EventFoundThisFrame); }

	inline void SetEventFound() 
	{ 
		m_flags.SetFlag(Flag_EventFound);
		m_flags.SetFlag(Flag_EventFoundThisFrame);
		m_flags.ClearFlag(Flag_WatchForEvent);
	}

	inline bool IsWatchingForEvent() const { return m_flags.IsFlagSet(Flag_WatchForEvent) && m_EventWatch.GetHash(); }

	inline fwMvBooleanId GetWatchedEvent()	{ return m_EventWatch; }


	// PURPOSE: Returns true if this nework player has not yet been inseterd into a move network
	inline bool IsReadyForInsert() { return m_flags.IsFlagSet(Flag_ReadyForInsert); }

	// PURPOSE: Returns true if this nework player is currently inserted into a move network
	inline bool IsInserted() { return m_flags.IsFlagSet(Flag_Inserted); }

	// PURPOSE: Should return true if another move network player has taken over this players network slot
	inline bool IsBlendingOut() { return m_flags.IsFlagSet(Flag_BlendingOut); }

	// PURPOSE: Returns true if the subnetwork was, but is no longer inserted into a move network
	inline bool HasExpired() { return m_flags.IsFlagSet(Flag_Expired); }

	// PURPOSE: Should be called by the network slot (or task) if the move network player is replaced with a new one
	inline void SetBlendingOut() { m_flags.SetFlag(Flag_BlendingOut); }

	// Sets a custom observer on the network
	void SetNetworkObserver(mvObserver* pObserver);

#if __ASSERT
	u32 GetCreationFrame() const { return m_uCreationFrame; }
#endif // __ASSERT

protected:
	// PURPOSE:
	void Init();

	// PURPOSE: 
	void Shutdown();

	// PURPOSE: Will be called before the motion tree updates
	void PreMotionTreeUpdate();


	void UpdateEventWatch()
	{
		if (m_flags.IsFlagSet(Flag_ReadyForInsert) && m_SubNetwork->IsAttached())
		{
			// The sub network has now been inserted into move and is being referenced
			m_flags.ClearFlag(Flag_ReadyForInsert);
			m_flags.SetFlag(Flag_Inserted);
		}
		else if (m_flags.IsFlagSet(Flag_Inserted) && !m_SubNetwork->IsAttached())
		{
			// The sub network has no longer referenced in move
			// Set a flag so any game side systems referencing the network player
			// know that MoVE is finished with it
			m_flags.ClearFlag(Flag_Inserted);
			m_flags.ClearFlag(Flag_BlendingOut);
			m_flags.SetFlag(Flag_Expired);
		}

		if (m_flags.IsFlagSet(Flag_WatchForEvent))
		{
			if (GetBoolean(m_EventWatch))
			{
				SetEventFound();
			}
		}
		else if (m_flags.IsFlagSet(Flag_EventFoundThisFrame))
		{
			m_flags.ClearFlag(Flag_EventFoundThisFrame);
		}
	}
#if __DEV
	virtual const fwAnimDirector& AnimDirector() const { return *m_Director; }
#endif
protected:

	rage::mvSubNetwork* m_SubNetwork;		// The move subnetwork this player owns

	fwAnimDirector* m_Director;

	fwMoveNetworkPlayer* m_NextPlayer;		// Link to the next network player in the AnimDirector list

	u32 m_NumRefs;	// Network players are reference counted

	fwFlags16 m_flags;			// Stores some useful flags

	fwMvBooleanId m_EventWatch;	// Helper functionality for tasks waiting on an important event from the network
												// the network helper can take charge of asking for the event every frame and 
												// storing the result.

#if __ASSERT
	u32 m_uCreationFrame;	// Frame this network player was created on.
#endif // __ASSERT

public:

	MOVE_DECLARE_ALLOCATOR(); //allocates from the MoVE pool
};

//////////////////////////////////////////////////////////////////////////
//	Common base class for main network interfaces.
//	fwAnimDirector talks to this directly.
//	Override this to create entity specific main network classes
//	(e.g. fwMovePed, fwMoveVehicle, fwMoveAnimatedBuilding, etc)
//////////////////////////////////////////////////////////////////////////

class fwMove : public fwMoveNetworkInterface
{
public:

	typedef  Functor3Ret<bool, fwMoveNetworkPlayer*, float, u32> InsertNetworkCB;

	// PURPOSE: 
	fwMove(fwEntity& dynamicEntity);

	// PURPOSE:
	virtual ~fwMove();

	// PURPOSE:
	virtual void Init(fwEntity& dynamicEntity, const mvNetworkDef& definition, const fwAnimDirector& owner);

	// PURPOSE:
	virtual void PostInit();

	// PURPOSE:
	virtual void Shutdown();

	// PURPOSE: Main thread update
	virtual void Update(float deltaTime);

	// PURPOSE: Main thread post update
	virtual void PostUpdate();

	// PURPOSE: Called immediately prior to motion tree update
	void PreMotionTreeUpdate();

	// PURPOSE: Main thread finish update, (called prior to output buffer swap, after motion tree finished)
	virtual void FinishUpdate();

	// PURPOSE: Main thread finish post update, (called after output buffer swap, after motion tree finished)
	virtual void PostFinishUpdate();

	// PURPOSE: Get access to underlying motion tree (const and non-const versions)
	const crmtMotionTree& GetMotionTree() const;
	crmtMotionTree& GetMotionTree();

	// PURPOSE: Should insert the synchrononized scene playback MoVE network on
	//			the entity's main MoVE network.
	// RETURNS: True if the insert was successful, false if the entity does not support playback of synchronized scenes
	virtual bool SetSynchronizedSceneNetwork( fwMoveNetworkPlayer* pPlayer, float blendDuration);
	virtual void ClearSynchronizedSceneNetwork( fwMoveNetworkPlayer* pPlayer, float blendDuration, bool tagSyncOut = false );

	// PURPOSE: 
	void Dump();
#if CR_DEV
	//	Dev only accessor for debug functionality
	mvNetwork* GetNetwork()
	{
		return m_MainNetwork;
	}
#endif //CR_DEV

protected:
#if __DEV
	virtual const fwAnimDirector& AnimDirector() const { return *m_Director; }
#endif

	crmtMotionTree* m_MotionTree;
	mvNetwork* m_MainNetwork;

private:
	const fwAnimDirector* m_Director;
};

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetFloat( const fwMvFloatId &floatId, float value)
{
	animAssert(m_pNetwork);

	DR_DEV_ONLY(RecordSetAnimValue(floatId.TryGetCStr(), value));

	GetInputBuffer().AppendSetFloat(floatId.GetHash(), value);
}

//////////////////////////////////////////////////////////////////////////

inline float fwMoveNetworkInterface::GetFloat( const fwMvFloatId &floatId)
{
	animAssert(m_pNetwork);

	const mvParameter* pParam = NULL;

	pParam = FindOutputParameter(floatId, mvParameter::kParameterReal);

	DR_DEV_ONLY(RecordGetAnimValue(floatId.TryGetCStr(), pParam ? pParam->GetReal() : -1.0f));

	return pParam ? pParam->GetReal(): -1.0f;
}

//////////////////////////////////////////////////////////////////////////

inline bool fwMoveNetworkInterface::GetFloat( const fwMvFloatId &floatId, float& value)
{
	animAssert(m_pNetwork);

	const mvParameter* pParam = NULL;

	pParam = FindOutputParameter(floatId, mvParameter::kParameterReal);

	if (pParam)
	{
		value = pParam->GetReal();
		DR_DEV_ONLY(RecordGetAnimValue(floatId.TryGetCStr(),pParam->GetReal()));
	}
	
	return pParam!=NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetBoolean( const fwMvBooleanId &booleanId, bool value)
{
	animAssert(m_pNetwork);

	DR_DEV_ONLY(RecordSetAnimValue(booleanId.TryGetCStr(), value));

	GetInputBuffer().AppendSetBoolean(booleanId.GetHash(), value);
}

//////////////////////////////////////////////////////////////////////////

inline bool fwMoveNetworkInterface::GetBoolean( const fwMvBooleanId &booleanId)
{
	animAssert(m_pNetwork);

	const mvParameter* pParam = NULL;

	pParam = FindOutputParameter(booleanId, mvParameter::kParameterBoolean);

	DR_DEV_ONLY(RecordGetAnimValue(booleanId.TryGetCStr(), pParam ? pParam->GetBool() : false));

	return pParam ? pParam->GetBool(): false;
}

//////////////////////////////////////////////////////////////////////////

inline bool fwMoveNetworkInterface::GetBoolean( const fwMvBooleanId &booleanId, bool& value)
{
	animAssert(m_pNetwork);

	const mvParameter* pParam = NULL;

	pParam = FindOutputParameter(booleanId, mvParameter::kParameterBoolean);

	if (pParam)
	{
		value = pParam->GetBool();
		DR_DEV_ONLY(RecordGetAnimValue(booleanId.TryGetCStr(),pParam->GetBool()));
	}

	return pParam!=NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetFlag( const fwMvFlagId &flagId, bool value)
{
	animAssert(m_pNetwork);

	if (value)
	{
		GetInputBuffer().AppendSetFlag(flagId.GetHash());
	}
	else
	{
		GetInputBuffer().AppendResetFlag(flagId.GetHash());
	}

	DR_DEV_ONLY(RecordSetAnimValue(flagId.TryGetCStr(), value));	//Just record as a bool for now
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetAnimation( const fwMvAnimId &animId, const crAnimation* anim)
{
	animAssert(m_pNetwork);

	GetInputBuffer().AppendSetAnimation(animId.GetHash(), anim);

	DR_DEV_ONLY(RecordSetAnimValue("MV:SETANIM", animId.TryGetCStr(), anim->GetName()));	//Just record as a bool for now
}

////////////////////////////////////////////////////////////////////////////////

inline const crAnimation* fwMoveNetworkInterface::GetAnimation( const fwMvAnimId &animId)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);

	pParam = FindOutputParameter(animId, mvParameter::kParameterAnimation);

	return pParam ? pParam->GetAnimation() : NULL;

}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetClip( const fwMvClipId &clipId, const crClip* clip)
{
	CHECK_MOVE_BUFFER;

	animAssert(m_pNetwork);

	GetInputBuffer().AppendSetClip(clipId.GetHash(), clip);

	DR_DEV_ONLY(RecordSetAnimValue(clipId.TryGetCStr(), clip));	//Just record as a bool for now
}

////////////////////////////////////////////////////////////////////////////////

inline const crClip* fwMoveNetworkInterface::GetClip( const fwMvClipId &clipId)
{
	const mvParameter* clipParam = NULL;

	animAssert(m_pNetwork);

	clipParam = FindOutputParameter(clipId, mvParameter::kParameterClip);

	return clipParam ? clipParam->GetClip() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetParameterizedMotion( const fwMvParameterizedMotionId &pmId, crpmParameterizedMotion* pm)
{
	animAssert(m_pNetwork);

	m_InputBuffers.GetCurrentBuffer().AppendSetParameterizedMotion(pmId.GetHash(), pm);

	DR_DEV_ONLY(RecordSetAnimValue("Move::SetPM", "id", pmId.TryGetCStr()));	//Just record as a tag with a string value for now
}

////////////////////////////////////////////////////////////////////////////////

inline const crpmParameterizedMotion* fwMoveNetworkInterface::GetParameterizedMotion( const fwMvParameterizedMotionId &pmId)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);
	
	pParam = FindOutputParameter(pmId, mvParameter::kParameterPM);

	return pParam ? pParam->GetPM() : NULL;

}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetExpressions( const fwMvExpressionsId &exprId, const crExpressions* expressions)
{
	animAssert(m_pNetwork);
	
	GetInputBuffer().AppendSetExpressions(exprId.GetHash(), expressions);

	DR_DEV_ONLY(RecordSetExpressions(exprId.TryGetCStr()));
}

////////////////////////////////////////////////////////////////////////////////

inline const crExpressions* fwMoveNetworkInterface::GetExpressions( const fwMvExpressionsId &exprId)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);
	
	pParam = FindOutputParameter(exprId, mvParameter::kParameterExpressions);

	return pParam ? pParam->GetExpressions() : NULL;

}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetFilter( const fwMvFilterId &filterId, crFrameFilter* filter)
{
	animAssert(m_pNetwork);

	GetInputBuffer().AppendSetFilter(filterId.GetHash(), filter);

	DR_DEV_ONLY(RecordSetAnimValue(filterId.TryGetCStr(), filter));
}
////////////////////////////////////////////////////////////////////////////////

inline const crFrameFilter* fwMoveNetworkInterface::GetFilter( const fwMvFilterId &filterId)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);
	
	pParam = FindOutputParameter(filterId, mvParameter::kParameterFilter);

	return pParam ? pParam->GetFilter() : NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetObserver( const fwMvObserverId &observerId, mvObserver* observer)
{
	animAssert(m_pNetwork);

	GetInputBuffer().AppendSetObserver(observerId.GetHash(), observer);
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetFrame( const fwMvFrameId &frameId, crFrame* frame)
{
	animAssert(m_pNetwork);
	
	GetInputBuffer().AppendSetFrame(frameId.GetHash(), frame);
}

////////////////////////////////////////////////////////////////////////////////

inline const crFrame* fwMoveNetworkInterface::GetFrame(const fwMvFrameId &frameId)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);

	pParam = FindOutputParameter(frameId, mvParameter::kParameterFrame);

	if (pParam)
	{
		if (pParam->GetType() == mvParameter::kParameterFrame)
		{
			return pParam->GetFrame();
		}
		else
		{
			Assertf(0, "frameId (%d) %s : is not param type kParameterFrame it is param type (%d) %s", 
				frameId.GetHash(), frameId.TryGetCStr(), pParam->GetType(), pParam->GetParamTypeName(pParam->GetType()));
		}
	}

	return NULL;

}

////////////////////////////////////////////////////////////////////////////////

inline const crProperty* fwMoveNetworkInterface::GetProperty( const fwMvPropertyId &propId)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);
	
	pParam = FindOutputParameter(propId, mvParameter::kParameterProperty);

	if (pParam)
	{
		if (pParam->GetType() == mvParameter::kParameterProperty)
		{
			return pParam->GetProperty();
		}
		else
		{
			Assertf(0, "propId (%d) %s : is not param type kParameterProperty it is param type (%d) %s", 
				propId.GetHash(), propId.TryGetCStr(), pParam->GetType(), pParam->GetParamTypeName(pParam->GetType()));
		}
		
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwMoveNetworkInterface::HasOutputParameter( const fwMvId &id)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);

	pParam = FindOutputParameter(id);

	return pParam!=NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwMoveNetworkInterface::HasOutputParameter( const fwMvId &id, mvParameter::ParamType type)
{
	const mvParameter* pParam = NULL;

	animAssert(m_pNetwork);

	pParam = FindOutputParameter(id, type);

	return pParam!=NULL;
}

//////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::BroadcastRequest( const fwMvRequestId &requestId)
{
	animAssert(m_pNetwork);

	GetInputBuffer().AppendBroadcastRequest(requestId.GetHash());
}

//////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SetSubNetwork(const fwMvNetworkId &networkId, mvSubNetwork* network)
{
	animAssert(m_pNetwork);

	GetInputBuffer().AppendSetNetwork(networkId.GetHash(), network);

	DR_DEV_ONLY(RecordSetAnimValue("Move::SubNet", networkId.TryGetCStr(), network ? atVarString("0x%p", network) : "<null>"));
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::ForceState( const fwMvStateId &parentId, const fwMvStateId &id)
{
	animAssert(m_pNetwork);

	GetInputBuffer().AppendForceSelectState(parentId.GetHash(), id.GetHash());

	DR_DEV_ONLY(RecordSetAnimValue("Move::ForceState", parentId.TryGetCStr(), id.TryGetCStr()));
}

//////////////////////////////////////////////////////////////////////////

inline const mvParameter* fwMoveNetworkInterface::FindOutputParameter(const fwMvId &id)
{
	animAssert(id.GetHash()!=0);
	animAssert(m_pNetwork);

	mvParameterBuffer& buf = GetOutputBuffer();
	return buf.FindFirst(id.GetHash());
}

//////////////////////////////////////////////////////////////////////////

inline const mvParameter* fwMoveNetworkInterface::FindOutputParameter(const fwMvId &id, mvParameter::ParamType type)
{
	animAssert(id.GetHash()!=0);
	animAssert(m_pNetwork);

	mvParameterBuffer& buf = GetOutputBuffer();
	return buf.FindFirstOfType(id.GetHash(), type);
}

//////////////////////////////////////////////////////////////////////////

inline fwClipSet* fwMoveNetworkInterface::GetClipSet(const fwMvClipSetVarId &clipSetVarId /* = CLIP_SET_VAR_ID_DEFAULT */) const
{
	return fwClipSetManager::GetClipSet(GetClipSetId(clipSetVarId));
}

//////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::PreMotionTreeUpdate()
{
	animAssert(m_pNetwork);
	if(m_OutputBuffersSwapped)
	{
		m_pNetwork->GetOutputBuffer()->GetNextBuffer().Clear();
	}
	else
	{
		m_pNetwork->GetOutputBuffer()->GetNextBuffer().UnFinalize();
	}
	m_OutputBuffersSwapped = false;
	m_pNetwork->GetInternalParameters()->Clear();
}

/////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::PostMotionTreeUpdate()
{
	animAssert(m_pNetwork);
	m_pNetwork->GetOutputBuffer()->GetNextBuffer().Finalize();
}

/////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SwapInputBuffers()
{ 
	m_InputBuffers.SwapBuffers(); 
}

/////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkInterface::SwapOutputBuffers()
{
	m_OutputBuffers.SwapBuffers();
	m_OutputBuffersSwapped = true;
}

//////////////////////////////////////////////////////////////////////////

//	fwMove inlines
////////////////////////////////////////////////////////////////////////////////

inline const crmtMotionTree& fwMove::GetMotionTree() const
{
	return *m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////

inline crmtMotionTree& fwMove::GetMotionTree()
{
	return *m_MotionTree;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwMove::PreMotionTreeUpdate()
{
	animAssert(m_MainNetwork);
	m_MainNetwork->PrepareBuffers(m_InputBuffers);

	fwMoveNetworkInterface::PreMotionTreeUpdate();
}

////////////////////////////////////////////////////////////////////////////////

// Move network player
//////////////////////////////////////////////////////////////////////////

inline void fwMoveNetworkPlayer::PreMotionTreeUpdate()
{
	m_SubNetwork->PrepareBuffers(m_InputBuffers);

	fwMoveNetworkInterface::PreMotionTreeUpdate();
}

////////////////////////////////////////////////////////////////////////////////

#if __DEV
#undef CHECK_MOVE_BUFFER
#endif

} // namespace rage


#endif // FWANIMATION_MOVE_H
