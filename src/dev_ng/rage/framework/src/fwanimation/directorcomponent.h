// 
// fwanimation/directorcomponent.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_H
#define FWANIMATION_DIRECTOR_COMPONENT_H

#include "anim_channel.h"
#include "animdefines.h"

#include "atl/array.h"

namespace rage
{

class fwAnimDirector;


////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Abstract base class for all animation director components
class fwAnimDirectorComponent
{
public:

	// PURPOSE: Director component types
	enum eComponentType
	{
		kComponentTypeNone = 0,
		kComponentTypeMotionTree,
		kComponentTypeCreature,
		kComponentTypeMove,
		kComponentTypeExpressions,
		kComponentTypeRagDoll,
		kComponentTypeSyncedScene,
		kComponentTypeFacialRig,

		kComponentTypeNum,
	};

	// PURPOSE: Director component messages
	enum eMessageId
	{
		kMessageNone = 0,

		kMessageUpdate,
		kMessageShuttingDown,
		kMessageMotionTreePreDelegate,
		kMessageMotionTreeMidDelegate,
		kMessageMotionTreePostDelegate,

		kMessageCreatureChanged,
		kMessageFrameDataChanged,
		kMessageSkeletonChanged,
	};


	// PURPOSE: Constructor
	fwAnimDirectorComponent(eComponentType componentType);

	// PURPOSE: Destructor
	virtual ~fwAnimDirectorComponent();

	// PURPOSE: Shutdown, frees/releases dynamic resources
	virtual void Shutdown();

	// PURPOSE: Allocate a component (factory)
	// PARAMS: componentType - type of component to allocate
	static fwAnimDirectorComponent* Allocate(eComponentType componentType);

	// PURPOSE: Clone a component (automatically implemented in derived classes)
	virtual fwAnimDirectorComponent* Clone() const = 0;

	// PURPOSE: Get component type
	eComponentType GetComponentType() const;

#if CR_DEV
	// PURPOSE: Get component type name
	const char* GetComponentTypeName() const;
#endif

	// PURPOSE: Get parent animation director
	fwAnimDirector& GetDirector() const;


	// PURPOSE: Update passes
	enum ePhase
	{
		kPhasePrePhysics = 0,
		kPhaseMidPhysics,
		kPhasePreRender,
		kPhaseNum,

		kPhaseAll = -1,
		kPhaseNone = -2,
	};

	// PURPOSE: Set phase(s) which component belongs to (and will receive update calls for)
	void SetPhase(ePhase phase);

	// PURPOSE: Get phase(s) which component belongs to (and will receive update calls for)
	ePhase GetPhase() const;


	// PURPOSE: Get component priority 
	// NOTES: Changes message receiving order, relative to other components
	// Higher priority == earlier receipt of message
	// WARNING: Checked during component insertion only - varying at other times will produce undefined results
	virtual u32 GetPriority() const;


	// PURPOSE: Override to receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);

	// PURPOSE: Override to receive call prior to update
	virtual void PreUpdate(ePhase phase);

	// PURPOSE: Override to receive call during update
	virtual void Update(ePhase phase);

	// PURPOSE: Override to receive call after update
	virtual void PostUpdate(ePhase phase);


	// PURPOSE: Init class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Update class
	static void UpdateClass();


	// PURPOSE: Definition of director component allocation call
	typedef fwAnimDirectorComponent* ComponentAllocateFn();

	// PURPSOE: Definition of director component initUpdateShutdown call
	typedef void ComponentFn();

	// PURPOSE: Director component type information
	struct ComponentTypeInfo
	{
	public:

		// PURPOSE: Constructor
		// PARAMS: componentType - component type identifier 
		// (see fwAnimDirectorComponent::eComponentType enumeration for built in component types)
		// name - component name string
		// size - size of component (in bytes)
		// allocateFn - component allocation call
		// initFn - component init call
		// updateFn - component update call
		// shutdownFn - component shutdown call
		// initPoolFn - component init pool call
		// shutdownPoolFn - component shutdown pool call
		ComponentTypeInfo(eComponentType componentType, const char* name, size_t size, ComponentAllocateFn* allocateFn, ComponentFn* initFn, ComponentFn* updateFn, ComponentFn* shutdownFn, ComponentFn* initPoolFn, ComponentFn* shutdownPoolFn);

		// PURPOSE: Destructor
		~ComponentTypeInfo();

		// PURPOSE: Registration call
		void Register() const;

		// PURPOSE: Get component type identifier
		// RETURNS: component type identifier 
		// (see fwAnimDirectorComponent::eComponentType enumeration for built in component types)
		eComponentType GetComponentType() const;

		// PURPOSE: Get component name string
		// RETURNS: string of component's name
		const char* GetName() const;

		// PURPOSE: Get component size
		// RETURNS: component size (in bytes)
		size_t GetSize() const;

		// PURPOSE: Get component allocation function
		// RETURNS: component allocation function pointer
		ComponentAllocateFn* GetAllocateFn() const;

		// PURPOSE: Get component init function
		// RETURNS: component init function pointer
		ComponentFn* GetInitFn() const;

		// PURPOSE: Get component update function
		// RETURNS: component update function pointer
		ComponentFn* GetUpdateFn() const;

		// PURPOSE: Get component shutdown function
		// RETURNS: component shutdown function pointer
		ComponentFn* GetShutdownFn() const;

		// PURPOSE: Get component int pool function
		// RETURNS: component init pool function pointer
		ComponentFn* GetInitPoolFn() const;

		// PURPOSE: Get component shutdown pool function
		// RETURNS: component shutdown function pointer
		ComponentFn* GetShutdownPoolFn() const;

	private:
		eComponentType m_ComponentType;
		const char* m_Name;
		size_t m_Size;
		ComponentAllocateFn* m_AllocateFn;
		ComponentFn* m_InitFn;
		ComponentFn* m_UpdateFn;
		ComponentFn* m_ShutdownFn;
		ComponentFn* m_InitPoolFn;
		ComponentFn* m_ShutdownPoolFn;
	};

	// PURPOSE: Get info about a component type
	// PARAMS: componentType - component type identifier 
	// (see fwAnimDirectorComponent::eComponentType enumeration for built in component types)
	// RETURNS: const pointer to component type info structure (may be NULL if component type unknown)
	static const ComponentTypeInfo* FindComponentTypeInfo(eComponentType componentType);

	// PURPOSE: Get info about this component
	// RETURNS: const reference to component type info structure about this component
	virtual const ComponentTypeInfo& GetComponentTypeInfo() const = 0;

	// PURPOSE: Declare functions necessary to register a new component type
	// NOTES: Must be placed in header file, in public section of derived class declaration
	#define FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(__typename) \
		static fwAnimDirectorComponent* AllocateComponent(); \
		virtual fwAnimDirectorComponent* Clone() const; \
		static void RegisterClass(); \
		static void InitPoolClass(); \
		static void ShutdownPoolClass(); \
		virtual const ComponentTypeInfo& GetComponentTypeInfo() const; \
		static const fwAnimDirectorComponent::ComponentTypeInfo sm_ComponentTypeInfo;

	// PURPOSE: Implement functions necessary to register a new component type
	// NOTES: Must be placed in derived class source file, in rage namespace
#if COMMERCE_CONTAINER
#define FW_INSTANTIATE_CLASS_POOL_FLEX_OR_NO_FLEX FW_INSTANTIATE_CLASS_POOL_NO_FLEX_SPILLOVER
#else // COMMERCE_CONTAINER
#define FW_INSTANTIATE_CLASS_POOL_FLEX_OR_NO_FLEX FW_INSTANTIATE_CLASS_POOL_SPILLOVER
#endif // COMMERCE_CONTAINER

	#define FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(__typename, __typenamehash, __typeid, __poolMax, __spillover) \
		class __typename##PooledObject : public __typename \
		{ \
		public: \
			__typename##PooledObject() : __typename() {} \
			FW_REGISTER_CLASS_POOL(__typename##PooledObject); \
		}; \
		FW_INSTANTIATE_CLASS_POOL_FLEX_OR_NO_FLEX(__typename##PooledObject, (__poolMax), __spillover, atHashString(#__typename, __typenamehash)); \
		fwAnimDirectorComponent* __typename::AllocateComponent() \
		{ \
			__typename* component = (((__poolMax) > 0) && (__typename##PooledObject::GetPool()->GetNoOfFreeSpaces() > 0))?(rage_new __typename##PooledObject):(rage_new __typename); \
			return component; \
		} \
		fwAnimDirectorComponent* __typename::Clone() const \
		{ \
			return rage_new __typename(*this); \
		} \
		void __typename::RegisterClass() \
		{ \
			sm_ComponentTypeInfo.Register(); \
		} \
		void __typename::InitPoolClass() \
		{ \
			if((__poolMax) > 0) \
				__typename##PooledObject::InitPool(MEMBUCKET_ANIMATION); \
		} \
		void __typename::ShutdownPoolClass() \
		{ \
			Assert(__typename##PooledObject::GetPool()->GetNoOfUsedSpaces() == 0); \
			if((__poolMax) > 0) \
				__typename##PooledObject::ShutdownPool(); \
		} \
		const fwAnimDirectorComponent::ComponentTypeInfo& __typename::GetComponentTypeInfo() const \
		{ \
			return sm_ComponentTypeInfo; \
		} \
		const fwAnimDirectorComponent::ComponentTypeInfo __typename::sm_ComponentTypeInfo((__typeid), CR_DEV ? #__typename :  NULL, sizeof(__typename), AllocateComponent, InitDerivedClass, UpdateDerivedClass, ShutdownDerivedClass, InitPoolClass, ShutdownPoolClass); \

protected:

	// PURPOSE: Base initialization, *must* be called from derived initialization function
	void InitBase(fwAnimDirector& director);

	// PURPOSE: Register a new component type (only call from ComponentTypeInfo::Register)
	// PARAMS: component type info (must be global/class static, persist for entire execution)
	static void RegisterComponentTypeInfo(const ComponentTypeInfo& info);

	// PURPOSE: Implement in derived class to perform class specific initialization
	// NOTE: This is static, it will be called once per *class* type not per *instance*
	static void InitDerivedClass();

	// PURPOSE: Implement in derived class to perform class specific initialization
	// NOTE: This is static, it will be called once per *class* type not per *instance*
	static void UpdateDerivedClass();

	// PURPOSE: Implement in derived class to perform class specific initialization
	// NOTE: This is static, it will be called once per *class* type not per *instance*
	static void ShutdownDerivedClass();

protected:

	eComponentType m_ComponentType;
	fwAnimDirector* m_Director;

	ePhase m_Phase;

	static atArray<const ComponentTypeInfo*> sm_ComponentTypeInfos;
	static bool sm_InitClassCalled;
};

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::eComponentType fwAnimDirectorComponent::GetComponentType() const
{
	return m_ComponentType;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirector& fwAnimDirectorComponent::GetDirector() const
{
	animFastAssert(m_Director);
	return *m_Director;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirectorComponent::SetPhase(ePhase phase)
{
	m_Phase = phase;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ePhase fwAnimDirectorComponent::GetPhase() const
{
	return m_Phase;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::eComponentType fwAnimDirectorComponent::ComponentTypeInfo::GetComponentType() const
{
	return m_ComponentType;
}

////////////////////////////////////////////////////////////////////////////////

inline const char* fwAnimDirectorComponent::ComponentTypeInfo::GetName() const
{
	return m_Name;
}

////////////////////////////////////////////////////////////////////////////////

inline size_t fwAnimDirectorComponent::ComponentTypeInfo::GetSize() const
{
	return m_Size;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ComponentAllocateFn* fwAnimDirectorComponent::ComponentTypeInfo::GetAllocateFn() const
{
	return m_AllocateFn;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ComponentFn* fwAnimDirectorComponent::ComponentTypeInfo::GetInitFn() const
{
	return m_InitFn;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ComponentFn* fwAnimDirectorComponent::ComponentTypeInfo::GetUpdateFn() const
{
	return m_UpdateFn;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ComponentFn* fwAnimDirectorComponent::ComponentTypeInfo::GetShutdownFn() const
{
	return m_ShutdownFn;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ComponentFn* fwAnimDirectorComponent::ComponentTypeInfo::GetInitPoolFn() const
{
	return m_InitPoolFn;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent::ComponentFn* fwAnimDirectorComponent::ComponentTypeInfo::GetShutdownPoolFn() const
{
	return m_ShutdownPoolFn;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_H

