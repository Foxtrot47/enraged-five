<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="fwSyncedSceneEntityAttachmentInfo">
    <Vector3 name="m_Offset"/>
    <Vector3 name="m_Rot"/>
    <s32 name="m_ParentBone"/>
    <s32 name="m_ChildBone"/>
    <s32 name="m_flags"/>
  </structdef>

  <structdef type="fwSyncedSceneEntity">
    <u32 name="m_Type"/>
    <string name="m_ModelName" type="atHashString"/>
    <string name="m_AnimDict" type="atHashString"/>
    <string name="m_AnimName" type="atHashString"/>
    <array name="m_AttachedEntities" type="atArray">
      <struct type="fwSyncedSceneEntity"/>
    </array>
    <struct name="m_AttachedInfo" type="fwSyncedSceneEntityAttachmentInfo"/>
  </structdef>

  <structdef type="CSyncedSceneLocation">
    <string name="m_LocationName" type="atString"/>
    <Vector3 name="m_LocationRotation"/>
    <Vector3 name="m_LocationOrigin"/>
  </structdef>

  <structdef type="CSyncedSceneInfo">
    <string name="m_SceneName" type="atString"/>
    <string name="m_Audio" type="atHashString"/>
    <array name="m_Locations" type="atArray">
      <struct type="CSyncedSceneLocation"/>
    </array>
  </structdef>

  <structdef type="fwSyncedSceneEntityManager">
    <struct	name="m_SceneInfo" type="CSyncedSceneInfo"/>
    <array name="m_SyncedSceneEntites" type="atArray">
      <struct type="fwSyncedSceneEntity"/>
    </array>
  </structdef>

</ParserSchema>
