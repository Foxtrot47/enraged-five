#ifndef CLIPSET_H_
#define CLIPSET_H_

#include "atl/binmap.h"
#include "fwanimation/animdefines.h"
#include "fwdebug/debugbank.h"
#include "script/thread.h"
#include "streaming/streamingmodule.h"

namespace rage {

class crClip;
class crClipDictionary;
class fwClipDictionaryMetadata;

//////////////////////////////////////////////////////////////////////////

// These flags control various aspects of playback of animations 
// When playing back animations via generic task
enum eAnimPlayerFlag
{
	APF_USE_SECONDARY_SLOT = (1<<0), // Use the secondary slot in the ped network (rather than the task slot)
	APF_TAG_SYNC_WITH_MOTION_TASK = (1<<1), // Sync with anims being played by the motion task
	APF_UNUSED_3 = (1<<2), // UNUSED
	APF_UNUSED_4 = (1<<3), // UNUSED
	APF_ISPLAYING = (1<<4),	// The animation is playing (set to false to pause)
	APF_ISLOOPED = (1<<5), // The animation should loop externally
	APF_UNUSED_10 = (1<<6), // DEPRECATED
	APF_UNUSED_11 = (1<<7), // DEPRECATED
	APF_UNUSED_18 = (1<<8), // DEPRECATED
	APF_SCRIPT = (1<<9), // The animation was started by script
	APF_UNUSED_6 = (1<<10), // UNUSED
	APF_UNUSED_7 = (1<<11), // UNUSED
	APF_UNUSED_8 = (1<<12), // UNUSED
	APF_UNUSED_12 = (1<<13), // DEPRECATED
	APF_ISBLENDAUTOREMOVE = (1<<14), // DEPRECATED
	APF_ISFINISHAUTOREMOVE = (1<<15), // The animation will be removed when the animation finishes (e.g the phase = 0.999 and not looping)
	APF_UNUSED_17 = (1<<16), // DEPRECATED
	APF_ADDITIVE = (1<<17), // The animation will be composited additively
	APF_FACIAL = (1<<18), // The animation is a facial animation
	APF_UNUSED_20 = (1<<19), // DEPRECATED
	APF_UNUSED_19 = (1<<20), // DEPRECATED
	APF_UPPERBODYONLY = (1<<21), // The animation is an upper body only animation (note it should still contain a root bone dof (it will be masked out at runtime))
	APF_SKIP_NEXT_UPDATE_BLEND = (1<<22), 	// Delay blendout by 1 frame
	APF_UNUSED_16 = (1<<23), 	// DEPRECATED
	APF_UNUSED_15 = (1<<24),	// DEPRECATED
	APF_UNUSED_14 = (1<<25), // DEPRECATED
	APF_BLOCK_IK = (1<<26), // Block all IK during this animation
	APF_BLOCK_LEG_IK = (1<<27), // Block leg IK during this animation
	APF_BLOCK_HEAD_IK = (1<<28), // Block head IK during this animation
	APF_FOLLOW_PED_ROOT_BONE = (1<<29), // DEPRECATED 
	APF_UNUSED_9 = (1<<30), // UNUSED 
	APF_USE_DEFAULT_RCB = (1<<31), // DEPRECATED
};

//////////////////////////////////////////////////////////////////////////

enum eClipRequestOptions
{
	CRO_NONE,
	CRO_FALLBACK,
	CRO_FALLBACK_IF_NOT_STREAMED
};

//////////////////////////////////////////////////////////////////////////

#if __ASSERT
const char *PrintClipInfo(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId);
#endif // __ASSERT

//////////////////////////////////////////////////////////////////////////

class fwClipItem
{
public:

	// Destructor

	virtual ~fwClipItem() { }

	// Constructors

	fwClipItem() { }

	// Properties

	virtual bool IsClipItemWithProps() const { return false; }

	virtual bool IsDirectedClipItemWithProps() const { return false; }

	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipItemWithProps : public fwClipItem
{
public:

	// Destructor

	virtual ~fwClipItemWithProps() { }

	// Constructors

	fwClipItemWithProps()
		: m_flags(0)
		, m_priority(AP_LOW)
		, m_boneMask(atHashString::Null())
	{
	}

	fwClipItemWithProps(u32 flags, eAnimPriority priority, const atHashString &boneMask)
		: m_flags(flags)
		, m_priority(priority)
		, m_boneMask(boneMask)
	{
	}

	// Properties

	virtual bool IsClipItemWithProps() const { return true; }

	void SetFlags(u32 flags) { m_flags = flags; }
	u32 GetFlags() const { return m_flags; }
	u32 &GetFlags() { return m_flags; }

	void SetPriority(eAnimPriority priority) { m_priority = priority; }
	eAnimPriority GetPriority() const { return m_priority; }

	void SetBoneMask(const atHashString &boneMask) { m_boneMask = boneMask; }
	const atHashString &GetBoneMask() const { return m_boneMask; }

protected:

	// Data

	u32 m_flags; /* 0 */
	eAnimPriority m_priority; /* AP_LOW */
	atHashString m_boneMask; /* atHashString::Null */

	PAR_PARSABLE;
};

///////////////////////////////////////////////////////////////////////////////////

class fwDirectedClipItemWithProps : public fwClipItemWithProps
{

public:

	// Destructor

	virtual ~fwDirectedClipItemWithProps() { }

	// Constructors

	fwDirectedClipItemWithProps()
		: fwClipItemWithProps(0, AP_LOW, atHashString::Null())
		, m_direction(0.0f)
	{
	}

	// Properties

	virtual bool IsDirectedClipItemWithProps() const { return true; }

	virtual float GetDirection() const { return m_direction; }

protected:

	// Data

	float m_direction;

	PAR_PARSABLE;
};

inline u32 GetClipItemFlags(const fwClipItem *pClipItem)
{
	return pClipItem->IsClipItemWithProps() ? static_cast< const fwClipItemWithProps * >(pClipItem)->GetFlags() : 0;
}

inline eAnimPriority GetClipItemPriority(const fwClipItem *pClipItem)
{
	return pClipItem->IsClipItemWithProps() ? static_cast< const fwClipItemWithProps * >(pClipItem)->GetPriority() : AP_LOW;
}

inline atHashString GetClipItemBoneMask(const fwClipItem *pClipItem)
{
	return pClipItem->IsClipItemWithProps() ? static_cast< const fwClipItemWithProps * >(pClipItem)->GetBoneMask() : atHashString::Null();
}

inline float GetClipItemDirection(const fwClipItem *pClipItem)
{
	return pClipItem->IsDirectedClipItemWithProps() ? static_cast< const fwDirectedClipItemWithProps * >(pClipItem)->GetDirection() : 0.0f;
}

//////////////////////////////////////////////////////////////////////////

class fwClipSet
{
public:

	// Types

	enum eClipSetType
	{
		kTypeClipSet = 0,
		kTypeClipSetWithGetUp
	};

	typedef atMap< u32, float > Properties;

	// Constructors

	fwClipSet();
	fwClipSet(const fwMvClipSetId &fallbackId, const atHashString &clipDictionaryName);

	virtual ~fwClipSet(){ Shutdown(); }
	// Properties

	virtual eClipSetType GetClipSetType() const { return kTypeClipSet; }

	inline void SetFallbackId(const fwMvClipSetId &fallbackId);
	inline const fwMvClipSetId &GetFallbackId() const { return m_fallbackId; }
	fwClipSet *GetFallbackSet() const { return m_fallbackClipSet; }

	inline const atHashString &GetClipDictionaryName() const { return m_clipDictionaryName; }

	inline int GetClipItemCount() const { return m_clipItems.GetCount(); }
	fwClipItem *GetClipItemByIndex(int clipItemIndex) const;
	fwClipItem *GetClipItem(const fwMvClipId &clipId, bool orFallbackIfExists = true) const;

	inline const atArray< atHashString > &GetMoveNetworkFlags() const { return m_moveNetworkFlags; }

	Properties &GetProperties() { return m_properties; }

	// Helper properties

	strLocalIndex GetClipDictionaryIndex() const;
	crClipDictionary *GetClipDictionary(bool assertIfNotStreamed = true) const;

	fwClipDictionaryMetadata *GetClipDictionaryMetadata() const;
	u32 GetClipDictionaryStreamingPolicy() const;

	fwMvClipId GetClipItemIdByIndex(int clipIndex) const;

	crClip *GetClip(const fwMvClipId &clipId, u32 clipRequestOptions = CRO_FALLBACK) const;

	// PURPOSE: Picks a random clip id from the clips currently available in this clip set.
	//			Note: Any dictionaries not streamed in (either in this clip set or its 
	//			fallbacks) will be excluded from the list.
	bool PickRandomClip(fwMvClipId& clipIdOut);

	const fwClipSet *GetClipClipSet(const fwMvClipId &clipId, u32 clipRequestOptions = CRO_FALLBACK) const;

	// Operations

	void Shutdown();

#if __BANK
	bool StreamIn_DEPRECATED(); // fwClipSet::StreamIn is deprecated and should be replaced with fwClipSetRequestHelper
#endif //__BANK
	bool Request_DEPRECATED(s32 iFlags = STRFLAG_FORCE_LOAD); // fwClipSet::Request is deprecated and should be replaced with fwClipSetRequestHelper
	void ClearRequiredFlag_DEPRECATED(s32 iFlags = STRFLAG_DONTDELETE); // fwClipSet::ClearRequiredFlag_DEPRECATED was added deprecated for fun
	bool IsStreamedIn_DEPRECATED() const; // fwClipSet::IsStreamedIn is deprecated and should be replaced with fwClipSetRequestHelper
	void AddRef_DEPRECATED(); // fwClipSet::AddRef is deprecated and should be replaced with fwClipSetRequestHelper
	void Release_DEPRECATED(); // fwClipSet::Release is deprecated and should be replaced with fwClipSetRequestHelper

	fwClipItem *CreateClipItem(const fwMvClipId &clipId);
	fwClipItemWithProps *CreateClipItemWithProps(const fwMvClipId &clipId, u32 animFlags = 0, eAnimPriority priority = AP_MEDIUM, const atHashString &boneMask = atHashString::Null());
	bool DeleteClipItem(const fwMvClipId &clipId);
	void DeleteAllClipItems();

	bool CreateMoveNetworkFlag(const atHashString &moveNetworkFlag);
	bool DeleteMoveNetworkFlag(const atHashString &moveNetworkFlag);
	void DeleteAllMoveNetworkFlags();

#if __BANK
	void AddWidgets(bkBank *pBank);
#endif //__BANK

protected:

	// PURPOSE: Build a map of all of the clip items available to play from this 
	//			clip set at this moment in time (including recursive fallbacks). Any items
	//			in dictionaries that are not currently streamed in will be excluded.
	void BuildAvailableClipItemMap(atBinaryMap<fwClipItem*, fwMvClipId >& map);

	// Data

	fwClipSet* m_fallbackClipSet;
	fwMvClipSetId m_fallbackId;
	atHashString m_clipDictionaryName;
	atBinaryMap< fwClipItem *, fwMvClipId > m_clipItems;
	atArray< atHashString > m_moveNetworkFlags;
	Properties m_properties;
	mutable s32 m_clipDictionaryIdx;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipSetWithGetup : public fwClipSet
{
public:
	fwClipSetWithGetup()
		: fwClipSet()
		, m_GetupSet((u32)0)
		, m_DeadBlendOutSet((u32)0)
		, m_AnimatedDeadFallSet((u32)0)
		, m_AllowAnimatedDeadFall(false)
	{

	}

	virtual ~fwClipSetWithGetup() { fwClipSet::Shutdown(); }
	
	virtual eClipSetType GetClipSetType() const { return kTypeClipSetWithGetUp; }

	atHashString GetGetupSet() const { return m_GetupSet; }
	atHashString GetDeadBlendOutSet() const { return m_DeadBlendOutSet; }
	bool		 CanAnimatedDeadFall() const { return m_AllowAnimatedDeadFall; }
	atHashString GetAnimatedDeadFallSet() const { return m_AnimatedDeadFallSet; }

protected:
	atHashString m_GetupSet;
	atHashString m_DeadBlendOutSet;
	bool m_AllowAnimatedDeadFall;
	atHashString m_AnimatedDeadFallSet;
	
	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipDictionaryMetadata
{
public:

	// Constructors

	fwClipDictionaryMetadata()
		: m_streamingPolicy(SP_STREAMING)
		, m_memoryGroup(atHashString::Null())
		, m_streamingPriority(SP_Invalid)
	{
	}

	fwClipDictionaryMetadata(u32 streamingPolicy, const atHashString &memoryGroup, eStreamingPriority streamingPriority)
		: m_streamingPolicy(streamingPolicy)
		, m_memoryGroup(memoryGroup)
		, m_streamingPriority(streamingPriority)
	{
	}

	// Properties

	inline void SetStreamingPolicy(u32 streamingPolicy) { m_streamingPolicy = streamingPolicy; }
	inline u32 GetStreamingPolicy() const { return m_streamingPolicy; }

	inline void SetMemoryGroup(atHashString& memoryGroup) { m_memoryGroup = memoryGroup; }
	inline const atHashString &GetMemoryGroup() const { return m_memoryGroup; }
	
	inline void SetStreamingPriority(eStreamingPriority streamingPriority) { m_streamingPriority = streamingPriority; }
	inline eStreamingPriority GetStreamingPriority() const { return m_streamingPriority; }

	// Helper properties

	crClipDictionary *GetClipDictionary() const;

protected:

	// Data

	u32 m_streamingPolicy;
	atHashString m_memoryGroup;
	eStreamingPriority m_streamingPriority;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwMemoryGroupMetadata
{
public:

	// Constructors

	fwMemoryGroupMetadata()
		: m_memoryBudget(0), m_memorySelected(0)
	{
	}

	// Properties
	inline void SetMemoryBudget(u32 budget) { m_memoryBudget = budget; }
	inline u32 GetMemoryBudget() const { return m_memoryBudget; }

	inline void SetMemorySelected(u32 selected) { m_memorySelected = selected; }
	inline u32 GetMemorySelected() const { return m_memorySelected; }

protected:

	// Data

	u32 m_memoryBudget;
	u32 m_memorySelected;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwMemorySituation
{

public:

	struct Adjustment
	{
		Adjustment()
		{}

		atHashWithStringNotFinal	m_MemoryGroup;
		int							m_Amount;

		PAR_SIMPLE_PARSABLE;
	};

	fwMemorySituation()
	{}

	atArray<Adjustment> m_Adjustments;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipVariationSet
{
public:

	// Accessors

	inline int GetNumClips() const { return m_clips.GetCount(); }

	bool PickRandomClip(fwMvClipId& clipIdOut) const; 

protected:

	// Properties

	atArray<fwMvClipId> m_clips;

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipSetRequestHelper
{
public:

	// Destructor

	~fwClipSetRequestHelper();

	// Constructors

	fwClipSetRequestHelper();

	// Operations

	bool Request(const fwMvClipSetId &clipSetId BANK_ONLY(, bool bForce = false));
	bool Request(BANK_ONLY(bool bForce = false));
	void Release();

	// Properties

	fwMvClipSetId GetClipSetId() const { return m_clipSetId; }
	fwClipSet *GetClipSet() const;
	bool IsLoaded() const;
	bool IsInvalid() const;
	bool IsLoadedOrInvalid() const { return IsLoaded() || IsInvalid(); }

	// Copy Constructor

	fwClipSetRequestHelper(const fwClipSetRequestHelper& that)
		: m_clipSetId(CLIP_SET_ID_INVALID)
		, m_clipDictionaryRefs(false)
	{
		if (that.m_clipSetId != CLIP_SET_ID_INVALID)
		{
			Request(m_clipSetId);
		}
	}

	// Assignment Operator

	fwClipSetRequestHelper& operator=(const fwClipSetRequestHelper& that) 
	{
		if (that.m_clipSetId != CLIP_SET_ID_INVALID)
		{
			Request(that.m_clipSetId);
		}
		else
		{
			if (m_clipSetId != CLIP_SET_ID_INVALID)
			{
				Release();
			}
		}
		return *this;
	}

private:
	// PURPOSE:	Special bit in m_clipDictionaryRefs, which if set indicates that everything is loaded.
	static const int kSpecialBitLoaded = 31;

	fwMvClipSetId m_clipSetId;
	atFixedBitSet32 m_clipDictionaryRefs;
};

//////////////////////////////////////////////////////////////////////////

class fwClipSetManager
{
public:

	// Static properties

	static inline fwClipSetManager &GetInstance() { return ms_instance; }

	static inline int GetClipSetCount() { return ms_instance.m_clipSets.GetCount(); }
	static fwClipSet *GetClipSetByIndex(int clipSetIndex);
	static fwClipSet *GetClipSet(const fwMvClipSetId &clipSetId);

	static inline int GetClipDictionaryMetadataCount() { return ms_instance.m_clipDictionaryMetadatas.GetCount(); }
	static fwClipDictionaryMetadata *GetClipDictionaryMetadata(const atHashString &clipDictionaryName);

	static inline int GetMemoryGroupMetadataCount() { return ms_instance.m_memoryGroupMetadatas.GetCount(); }
	static fwMemoryGroupMetadata *GetMemoryGroupMetadata(const atHashString &memoryGroupName);
	static fwMemoryGroupMetadata *GetMemoryGroupMetadataByIndex(s32 index);
	static atHashString *GetMemoryGroupNameByIndex(s32 index);

	static inline int GetMemorySituationCount() { return ms_instance.m_memorySituations.GetCount(); }
	static const fwMemorySituation* GetMemorySituation(atHashString hName) { return ms_instance.m_memorySituations.SafeGet(hName); }

	static inline int GetClipVariationSetCount() { return ms_instance.m_clipVariationSets.GetCount(); }
	static fwClipVariationSet* GetClipVariationSetByIndex(int clipVariationSetIndex);
	static fwClipVariationSet* GetClipVariationSet(const fwMvClipVariationSetId &clipVariationSetId);

	static bool IsPatchingClipSets() { return ms_bIsPatchingClipSets; }

	// Static helper properties

	static fwMvClipSetId GetClipSetIdByIndex(int clipSetIndex);
	static fwMvClipSetId GetClipSetId(const char *clipSetName);
	static fwMvClipSetId GetClipSetId(const atHashString& clipSetName);

	static fwClipItem *GetClipItem(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, bool orFallbackIfExists = true);

	static crClip *GetClip(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, bool orFallbackIfExists = true);

	static bool GetClipDictionaryNameAndClipName(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, atHashString &clipDictName, atHashString &clipName, bool orFallbackIfExists = true);

	static int GetClipDictionaryIndex(const fwMvClipSetId &clipSetId);
	static int GetClipDictionaryIndex(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId);

	static u32 GetClipDictionaryStreamingPolicy(const atHashString &clipDictionaryName);

	static bool PickRandomClip (const fwMvClipSetId& clipSetId, fwMvClipId& clipIdOut);
	
	static void Init(u32 initMode);
	static void Shutdown(u32 shutdownMode);

#if __BANK
	static bool StreamIn_DEPRECATED(const fwMvClipSetId &clipSetId); // fwClipSetManager::StreamIn is deprecated and should be replaced with fwClipSetRequestHelper
#endif // __BANK
	static bool Request_DEPRECATED(const fwMvClipSetId &clipSetId); // fwClipSetManager::Request is deprecated and should be replaced with fwClipSetRequestHelper
	static bool IsStreamedIn_DEPRECATED(const fwMvClipSetId &clipSetId); // fwClipSetManager::IsStreamedIn is deprecated and should be replaced with fwClipSetRequestHelper
	static void AddRef_DEPRECATED(const fwMvClipSetId &clipSetId); // fwClipSetManager::AddRef is deprecated and should be replaced with fwClipSetRequestHelper
	static void Release_DEPRECATED(const fwMvClipSetId &clipSetId); // fwClipSetManager::Release is deprecated and should be replaced with fwClipSetRequestHelper

	static bool Load();

	// Patching
	static bool PatchClipSets(const char *fname);
	static bool RemovePatchClipSets();

	// Script Resources

	static void Script_AddAndRequestClipSet(fwMvClipSetId clipSetId);
	static void Script_RequestClipSet(fwMvClipSetId clipSetId);
	static bool Script_HasClipSetLoaded(fwMvClipSetId clipSetId);
	static void Script_RemoveClipSet(fwMvClipSetId clipSetId);
	
#if __BANK
	static bool Save();
#endif // __BANK

	static fwClipSet *CreateClipSet(const fwMvClipSetId &clipSetId, const fwMvClipSetId &fallbackId, const atHashString &clipDictionaryName);
	static bool DeleteClipSet(const fwMvClipSetId &clipSetId);
	static void DeleteAllClipSets();

	static fwClipDictionaryMetadata *CreateClipDictionaryMetadata(const atHashString &clipDictionaryName, u32 streamingPolicy = MG_NONE, fwMemoryGroup memoryGroup = SP_STREAMING, eStreamingPriority streamingPriority = SP_Medium);
	static bool DeleteClipDictionaryMetadata(const atHashString &clipDictionaryMetadata);
	static void DeleteAllClipDictionaryMetadatas();
	static void DeleteAllMemoryGroupMetadatas();
	static void DeleteAllMemorySituations();

	static void StartNetworkSession();
	static void EndNetworkSession();

#if __BANK
	static void AddWidgets(fwDebugBank *pBank);
	static void AddClipSetWidgets(bkBank *pBank, int *pSelection, datCallback pSelectionCallback = NullCB);

	static void InitWidgets();
	static void ActivateBank();
	static void DeactivateBank();
	static void ShutdownWidgets();

	static void BuildClipDictionaryNameArray();
	static void BuildClipNameArray();
	static void SelectClipSet();
	static void SelectClipDictionary();
	static void AddClipToCurrentClipSet();
	static void AddAllClipsToCurrentClipSet();
	static void DeleteClipFromCurrentClipSet();
	static void DeleteAllClipsFromCurrentClipSet();
	static void AddClipSetByName();
	static void RemoveClipSetByName();
	static void UpdateClipSetComboBox();
	static void UpdateFallbackClipSet();
	static void ValidateClipSets();

	static void LoadFromXML();

	static void RebuildClipSetNames();

	static atArray< const char * > &GetClipSetNames();
	static atArray< const char * > &GetMemoryGroupNames();

	static atString GetRequestFailReason(const fwMvClipSetId& clipSet, const fwMvClipId& clip);
#endif // __BANK

protected:

	// Static data

	static fwClipSetManager ms_instance;

	// Data

	atBinaryMap< fwClipSet *, fwMvClipSetId > m_clipSets;
	atBinaryMap< fwClipDictionaryMetadata, atHashString > m_clipDictionaryMetadatas;
	atBinaryMap< fwMemoryGroupMetadata, atHashString > m_memoryGroupMetadatas;
	atBinaryMap< fwMemorySituation, atHashString > m_memorySituations;
	atBinaryMap< fwClipVariationSet*, fwMvClipVariationSetId > m_clipVariationSets;

	// Script Resources

	// CScriptResource_ClipSet can't have a fwClipSetRequestHelper as a member variable due to class size restrictions,
	// so store a map owned by this manager to look up/get the correct request helper
	struct ScriptResourceRequestHelper
	{
		u32						m_refCount;			// Ref count, multiple script resources may request the same clipset
		fwClipSetRequestHelper	m_requestHelper;	// The request helper (has the clipset id)

		// Constructor

		ScriptResourceRequestHelper()
		{
			m_refCount = 0;
		}
	};

	atArray<ScriptResourceRequestHelper>	m_scriptResourceRequestHelpers;

	// Residency state
	// We track what type of residency we are currently applying to clipsets.  So that patches/DLC that add clipsets
	// can be loaded, ref'd and made resident accordingly when they are applied.  The state is updated from:
	// Init, Shutdown, StartNetworkSession and EndNetworkSession.

	enum eClipSetManagerResidencyState
	{
		CSM_RESIDENCYSTATE_NONE,			// No clipsets have been made resident.
		CSM_RESIDENCYSTATE_SINGLEPLAYER,	// Clipsets marked as singleplayer-resident should now be resident.
		CSM_RESIDENCYSTATE_MULTIPLAYER,		// Clipsets marked as multiplayer-resident should now be resident.
	};
	eClipSetManagerResidencyState m_eResidencyState;

	static bool ms_bIsPatchingClipSets;

#if __ASSERT
	static bool m_bStartNetworkSessionCalled;
#endif //__ASSERT

#if __BANK
	static atArray< const char * > ms_clipSetNames;
	static atArray< const char * > ms_clipDictionaryNames;
	static atArray< const char * > ms_clipNames;
	static atArray< const char * > ms_memoryGroupNames;

	static fwDebugBank *m_pBank;
	static bkGroup *m_pClipGroup;
	static bkGroup *m_pClipSetGroup;
	static bkCombo *m_pClipSetList;

	static int ms_selectedClipSet;
	static int ms_selectedClipDictionary;
	static int ms_selectedClip;

	static const u32 MAX_CLIP_SET_NAME_STRING_LENGTH = 128;
	static char m_deleteClipName[MAX_CLIP_SET_NAME_STRING_LENGTH];
	static char m_addDeleteClipSetName[MAX_CLIP_SET_NAME_STRING_LENGTH];

	static int ms_selectedFallbackClipSet;
	static bkCombo *m_pFallbackSetList;
#endif // __BANK

	PAR_SIMPLE_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipDictionaryBuildMetadata
{
public:

	/* Destructor */

	virtual ~fwClipDictionaryBuildMetadata() { }

	/* Constructors */

	fwClipDictionaryBuildMetadata() : m_sizeBefore(0), m_sizeAfter(0) { }
	fwClipDictionaryBuildMetadata(u32 sizeBefore, u32 sizeAfter) : m_sizeBefore(sizeBefore), m_sizeAfter(sizeAfter) { }
	fwClipDictionaryBuildMetadata(const fwClipDictionaryBuildMetadata &that) : m_sizeBefore(that.m_sizeBefore), m_sizeAfter(that.m_sizeAfter) { }

	/* Operators */

	fwClipDictionaryBuildMetadata &operator =(const fwClipDictionaryBuildMetadata &that)
	{
		m_sizeBefore = that.m_sizeBefore;
		m_sizeAfter = that.m_sizeAfter;
		return *this;
	}

	/* Properties */

	void SetSizeBefore(u32 sizeBefore) { m_sizeBefore = sizeBefore; }
	u32 GetSizeBefore() const { return m_sizeBefore; }
	void SetSizeAfter(u32 sizeAfter) { m_sizeAfter = sizeAfter; }
	u32 GetSizeAfter() const { return m_sizeAfter; }

protected:

	/* Data */

	u32 m_sizeBefore;
	u32 m_sizeAfter;

	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

class fwClipRpfBuildMetadata
{
public:

	/* Destructor */

	virtual ~fwClipRpfBuildMetadata() { }

	/* Constructors */

	fwClipRpfBuildMetadata() { }

	/* Properties */

	void SetName(const atHashString &name) { m_name = name; }
	const atHashString &GetName() const { return m_name; }
	u32 GetClipDictionaryBuildMetadataCount() const;
	atHashString GetClipDictionaryBuildMetadataName(u32 index) const;
	const fwClipDictionaryBuildMetadata *GetClipDictionaryBuildMetadata(u32 index) const;
	fwClipDictionaryBuildMetadata *GetClipDictionaryBuildMetadata(u32 index);

	/* Operations */

	bool CreateClipDictionaryBuildMetadata(const atHashString &name, const fwClipDictionaryBuildMetadata &clipBuildMetadata);
	bool DestroyClipDictionaryBuildMetadata(const atHashString &name);
	const fwClipDictionaryBuildMetadata *FindClipDictionaryBuildMetadata(const atHashString &name) const;
	fwClipDictionaryBuildMetadata *FindClipDictionaryBuildMetadata(const atHashString &name);

#if __BANK
	void Merge(const fwClipRpfBuildMetadata &clipRpfBuildMetadata);
	bool Load(const char *file);
	bool Save(const char *file);
#endif // __BANK

protected:

	/* Data */

	atHashString m_name;
	atBinaryMap< fwClipDictionaryBuildMetadata, atHashString > m_dictionaries;

	/* Constructors */

	fwClipRpfBuildMetadata(const fwClipRpfBuildMetadata &);

	/* Operators */

	fwClipRpfBuildMetadata &operator =(const fwClipRpfBuildMetadata &);

	PAR_PARSABLE;
};

//////////////////////////////////////////////////////////////////////////

} // namespace rage


#endif // CLIPSET_H_
