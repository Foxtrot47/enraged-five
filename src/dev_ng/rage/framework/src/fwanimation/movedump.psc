<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="fwMoveDumpNode" >
    <string name="m_Name" type="atString" />
    <string name="m_Address" type="atString" />
    <array name="m_Children" type="atArray">
      <pointer type="fwMoveDumpNode" policy="owner" />
    </array>
  </structdef>

  <structdef type="fwMoveDumpNodeAnimation" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodeClipTag" >
    <string name="m_Name" type="atString" />
    <float name="m_Mid" />
    <bool name="m_Active" />
    <bool name="m_Foot" />
    <bool name="m_Heel" />    
    <bool name="m_Right" />
    <string name="m_MoveEvent" type="atHashString" />
  </structdef>

  <structdef type="fwMoveDumpNodeClip" base="fwMoveDumpNode" >
    <string name="m_Clip" type="atString" />
    <float name="m_Time" />
    <float name="m_Duration" />
    <float name="m_Phase" />
    <float name="m_Rate" />
    <bool name="m_Looped" />
    <array name="m_Tags" type="atArray">
      <struct type="fwMoveDumpNodeClipTag" />
    </array>
    <array name="m_Synchronizers" type="atArray">
      <string type="atString" />
    </array>
  </structdef>

  <structdef type="fwMoveDumpNodeFrame" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodeIdentity" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodeInvalid" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodePm" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodePose" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodeProxy" base="fwMoveDumpNode" >
    <string name="m_Target" type="atString" />
  </structdef>

  <structdef type="fwMoveDumpNodeParent" base="fwMoveDumpNode" >
  </structdef>

  <structdef type="fwMoveDumpNodeCapture" base="fwMoveDumpNodeParent" >
    <bool name="m_HasFrame"/>
  </structdef>

  <structdef type="fwMoveDumpNodeExpression" base="fwMoveDumpNodeParent" >
    <string name="m_Expression" type="atString" />
    <float name="m_Time" />
    <float name="m_Weight" />
  </structdef>

  <structdef type="fwMoveDumpNodeExtrapolate" base="fwMoveDumpNodeParent" >
    <float name="m_Damping" />
  </structdef>

  <structdef type="fwMoveDumpNodeFilter" base="fwMoveDumpNodeParent" >
    <string name="m_Filter" type="atString" />
  </structdef>

  <structdef type="fwMoveDumpNodeIk" base="fwMoveDumpNodeParent" >
  </structdef>

  <structdef type="fwMoveDumpNodeJointLimit" base="fwMoveDumpNodeParent" >
  </structdef>

  <structdef type="fwMoveDumpNodeMerge" base="fwMoveDumpNodeParent" >
    <string name="m_Filter" type="atString" />
  </structdef>

  <structdef type="fwMoveDumpNodeMirror" base="fwMoveDumpNodeParent" >
  </structdef>

  <structdef type="fwMoveDumpNodeStateInputParam" >
    <string name="m_Name" type="atString" />
    <string name="m_Type" type="atString" />
    <string name="m_Value" type="atString" />
  </structdef>

  <structdef type="fwMoveDumpNodeStateOutputParam" >
    <string name="m_Name" type="atString" />
    <string name="m_Type" type="atString" />
    <string name="m_Value" type="atString" />
  </structdef>

  <structdef type="fwMoveDumpNodeState" base="fwMoveDumpNodeParent" >
    <bool name="m_IsInterestingState" />
    <string name="m_StateInterfaceType" type="atString" />
    <string name="m_StateInterfaceName" type="atString" />
    <string name="m_ActiveName" type="atString" />
    <string name="m_ClipSet" type="atString" />
    <int name="m_Transitions" />
    <array name="m_Requests" type="atArray">
      <pointer type="fwMoveDumpNetworkRequest" policy="owner" />
    </array>
    <array name="m_Flags" type="atArray">
      <pointer type="fwMoveDumpNetworkFlag" policy="owner" />
    </array>
    <array name="m_InputParams" type="atArray">
      <pointer type="fwMoveDumpNodeStateInputParam" policy="owner" />
    </array>
    <array name="m_OutputParams" type="atArray">
      <pointer type="fwMoveDumpNodeStateOutputParam" policy="owner" />
    </array>
  </structdef>

  <structdef type="fwMoveDumpNodePair" base="fwMoveDumpNodeParent" >
    <float name="m_Weight" />
    <float name="m_WeightRate" />
    <string name="m_Filter" type="atString" />
  </structdef>

  <structdef type="fwMoveDumpNodeAddSubtract" base="fwMoveDumpNodePair" >
  </structdef>

  <structdef type="fwMoveDumpNodeBlend" base="fwMoveDumpNodePair" >
  </structdef>

  <structdef type="fwMoveDumpNodeN" base="fwMoveDumpNodeParent" >
    <array name="m_Weights" type="atArray">
      <float />
    </array>
  </structdef>

  <structdef type="fwMoveDumpNodeAddN" base="fwMoveDumpNodeN" >
  </structdef>

  <structdef type="fwMoveDumpNodeBlendN" base="fwMoveDumpNodeN" >
  </structdef>

  <structdef type="fwMoveDumpNodeMergeN" base="fwMoveDumpNodeN" >
  </structdef>

  <structdef type="fwMoveDumpDof" >
    <u8 name="m_Track" />
    <u16 name="m_Id" />
    <u8 name="m_Type" />
    <bool name="m_Invalid" />
  </structdef>

  <structdef type="fwMoveDumpDofVector3" base="fwMoveDumpDof" >
    <Vector3 name="m_Value" />
  </structdef>

  <structdef type="fwMoveDumpDofQuaternion" base="fwMoveDumpDof" >
    <Vector4 name="m_Value" />
  </structdef>

  <structdef type="fwMoveDumpDofFloat" base="fwMoveDumpDof" >
    <float name="m_Value" />
  </structdef>

  <structdef type="fwMoveDumpNetworkRequest" >
    <string name="m_Name" type="atHashString" />
    <bool name="m_bValue" />
  </structdef>

  <structdef type="fwMoveDumpNetworkFlag" >
    <string name="m_Name" type="atHashString" />
    <bool name="m_bValue" />
  </structdef>

  <structdef type="fwMoveDumpNetwork" >
    <u32 name="m_FrameIndex" />
    <u32 name="m_FrameTime" />
    <pointer name="m_Root" type="fwMoveDumpNode" policy="owner" />
    <Matrix34 name="m_Matrix" />
    <string name="m_Entity" type="atString" />
    <string name="m_Filter" type="atString" />
    <array name="m_Requests" type="atArray">
      <pointer type="fwMoveDumpNetworkRequest" policy="owner" />
    </array>
    <array name="m_Flags" type="atArray">
      <pointer type="fwMoveDumpNetworkFlag" policy="owner" />
    </array>
    <array name="m_Dofs" type="atArray">
      <pointer type="fwMoveDumpDof" policy="owner" />
    </array>
  </structdef>

  <structdef type="fwMoveDumpFrameSequence" >
    <u32 name="m_MaxFrameCount" />
    <u32 name="m_StartFrame" />
    <u32 name="m_CurrentFrame" />
    <array name="m_Frames" type="atArray">
      <pointer type="fwMoveDumpNetwork" policy="owner" />
    </array>
  </structdef>

</ParserSchema>
