#ifndef FWANIMATION_ANIMMANAGER_H
#define FWANIMATION_ANIMMANAGER_H

// Rage headers
#include "paging/dictionary.h"

#include "fwanimation/animdefines.h"


#include "fwscene/stores/clipdictionarystore.h"
#include "fwtl/pool.h"

#include "crclip/clipdictionary.h"
#include "crclip/clip.h"
#include "crclip/clipanimation.h"

// Rage forward declaration
namespace rage
{
	class crAnimation;
	class crBoneData;
	class crClip;
	class crSkeletonData;
	class fwAnimDirector;
	class mvMotionWeb;
	class Quaternion;
	class strStreamingModule;
	class crPropertyAttributeInt;

// Defines
#define ANIM_DICT_INDEX_INVALID (-1)
#define ANIM_HASH_INVALID (0)

//
// fwAnimManager
// Storage and retrieval of anim files
//
class fwAnimManager // : public CStore<crClipDictionary, CAnimDef>
{
public:
	virtual ~fwAnimManager() {};

	// If an animation debugger is part of the system:
	virtual fwAnimDirector *GetAnimDirectorBeingViewed() const		{ return NULL; }

	// Called on initialisation in game.cpp
	static void InitClass();
	static void InitCore();
	static void InitBeforeMap();
	static void ShutdownClass();
    static void ShutdownCore();
	static void ShutdownSession();

	// Clip retrieval

	// Get a clip using the clip set id and the anim id
	// or return NULL if it doesn't exist (instead of asserting)
	static const crClip* GetClipIfExistsBySetId(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId);

	// Get a clip using the the anim dict name and the anim name
	// or return NULL if it doesn't exist (instead of asserting)
	static const crClip* GetClipIfExistsByName(const char* clipDictName, const char* pAnimName);	

	// Get a clip using the anim dict index and the anim hash key
	// or return NULL if it doesn't exist (instead of asserting)
	static const crClip* GetClipIfExistsByDictIndex(s32 animDictIndex, u32 animHashKey);	

	// Get a clip using the anim dict index and the anim index 
	static const crClip* GetClipInDict(const s32 animDictIndex, u32 iAnimIndex);	

	// Anim retrieval

	// Get an anim hash using the anim dict index and the anim index 
	static u32 GetAnimHashInDict(const s32 animDictIndex, u32 iAnimIndex);	

	// Get the number of anims in the anim dict using the anim dict index
	static s32 CountAnimsInDict(const s32 animDictIndex);

	// Get the anim dict index using the clip set id
	static s32 GetClipDictIndex(const fwMvClipSetId &clipSetId);

	// Gets the local translation of the given bone id
	// Asserts and/or returns 0.0f, 0.0f, 0.0f if the anim or bone is invalid
	// Returns the bind pose translation if no translation dof exists
	static void GetLocalTranslation(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Vector3 &translation);

	// Gets the local rotation of the given bone id
	// Asserts and/or returns identity if the anim or bone is invalid
	// Returns the bind pose rotation if no rotation dof exists
	static void GetLocalRotation(const crSkeletonData *pSkeletonData, const crClip *pClip,  float fPhase, u16 iBoneId, Quaternion &rotation);

	// Gets the local matrix of the given bone id
	// Asserts and/or returns identity if the anim or bone is invalid
	// Uses the bind pose translation if no translation dof exists
	// Uses the bind pose rotation if no rotation dof exists
	static void GetLocalMatrix(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Matrix34 &matrix);

	// Gets the global matrix of the given bone id
	// Asserts and/or returns identity if the anim or bone is invalid
	// Uses the bind pose translation if no translation dof exists
	// Uses the bind pose rotation if no rotation dof exists
	static void GetObjectMatrix(const crSkeletonData *pSkeletonData, const crClip *pClip, float fPhase, u16 iBoneId, Matrix34 &matrix);

	// Temporary functions to allow us to breakup animmanager 
	// into a manager class and a clipdictionary store 
	static int GetStreamingModuleId();
	static strLocalIndex FindSlot(const strStreamingObjectName name);
	static strLocalIndex FindSlotFromHashKey(const u32 hashKey);
	static s32 FindHashKeyFromSlot(strLocalIndex index);
	static bool IsValidSlot(strLocalIndex index);
	static void RemoveRefWithoutDelete(strLocalIndex index);
	static const char* GetName(strLocalIndex index);
	static void StreamingRequest(strLocalIndex index, s32 flags=0);
	static void AddRef(strLocalIndex index);
	static void RemoveRef(strLocalIndex index);
	static s32 GetNumRefs(strLocalIndex index);
	static s32 GetMaxSize();
	static s32 GetSize();
	static s32 GetNumUsedSlots();
	static crClipDictionary* Get(strLocalIndex index);
	static fwClipDictionaryDef* GetSlot(strLocalIndex index);
	static strStreamingModule* GetStreamingModule();
	static strLocalIndex Register(const char* name);

private:

	// Gets the local translation of the given bone at a given phase for a given anim
	// Asserts and/or returns 0.0f, 0.0f, 0.0f if the anim or bone is invalid
	// Returns the bind pose translation if no translation dof exists
	static void GetLocalTranslation(const crBoneData *pBoneData, const crClip *crClip, float fPhase, Vector3& translation);

	// Gets the local rotation of a given bone at a given phase for a given anim
	// Asserts and/or returns identity if the anim or bone is invalid
	// Returns the bind pose rotation if no rotation dof exists
	static void GetLocalRotation(const crBoneData *pBoneData, const crClip *crClip, float fPhase, Quaternion& rotation);
};

// Global instance to access all the virtual functions
extern fwAnimManager *g_AnimManager;

} // namespace rage

#endif // FWANIMATION_ANIMMANAGER_H
