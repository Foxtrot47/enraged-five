// 
// animation/AnimDefines.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DEFINES_H
#define FWANIMATION_DEFINES_H

// Rage headers
#include "vector/vector3.h"
#include "vector/vector4.h"
#include "cranimation/framedata.h"
#include "atl/hashstring.h"
#include "atl/binmap.h"

// Game headers

#define DEBUG_EXPRESSIONS (__BANK)

#define USE_DEBUG_ANIM_MEMORY() sysMemUseMemoryBucket mem_debug(MEMBUCKET_ANIMATION); sysMemAutoUseDebugMemory auto_debug

namespace rage {

class fwMvId : public atHashWithStringNotFinal
{
public:

	fwMvId() { }

	explicit fwMvId(u32 hash)
		: atHashWithStringNotFinal(hash)
	{
	}

	explicit fwMvId(const char *str)
		: atHashWithStringNotFinal(str)
	{
	}

	explicit fwMvId(const char *str, u32 hash)
		: atHashWithStringNotFinal(str, hash)
	{
	}

	bool operator !=(const fwMvId &Id) const
	{
		return GetHash() != Id.GetHash();
	}
};

template < typename T >
class fwMvTemplateId : public fwMvId
{
public:

	fwMvTemplateId() { }

	explicit fwMvTemplateId(u32 hash)
		: fwMvId(hash)
	{
	}

	explicit fwMvTemplateId(const char *str)
		: fwMvId(str)
	{
	}

	explicit fwMvTemplateId(const char *str, u32 hash)
		: fwMvId(str, hash)
	{
	}

	bool operator !=(const fwMvTemplateId &Id) const { return fwMvId::operator !=(Id); }
};

class fwMvFloatIdType { }; typedef fwMvTemplateId< fwMvFloatIdType > fwMvFloatId;
class fwMvBooleanIdType { }; typedef fwMvTemplateId< fwMvBooleanIdType > fwMvBooleanId;
class fwMvFlagIdType { }; typedef fwMvTemplateId< fwMvFlagIdType > fwMvFlagId;
class fwMvAnimIdType { }; typedef fwMvTemplateId< fwMvAnimIdType > fwMvAnimId;
class fwMvClipIdType { }; typedef fwMvTemplateId< fwMvClipIdType > fwMvClipId;
class fwMvParameterizedMotionIdType { }; typedef fwMvTemplateId< fwMvParameterizedMotionIdType > fwMvParameterizedMotionId;
class fwMvExpressionsIdType { }; typedef fwMvTemplateId< fwMvExpressionsIdType > fwMvExpressionsId;
class fwMvExpressionSetIdType { }; typedef fwMvTemplateId< fwMvExpressionSetIdType > fwMvExpressionSetId;
class fwMvFacialClipSetGroupIdType { }; typedef fwMvTemplateId< fwMvFacialClipSetGroupIdType > fwMvFacialClipSetGroupId;
class fwMvFilterSetIdType { }; typedef fwMvTemplateId< fwMvFilterSetIdType  > fwMvFilterSetId;
class fwMvFilterIdType { }; typedef fwMvTemplateId< fwMvFilterIdType > fwMvFilterId;
class fwMvObserverIdType { }; typedef fwMvTemplateId< fwMvObserverIdType > fwMvObserverId;
class fwMvFrameIdType { }; typedef fwMvTemplateId< fwMvFrameIdType > fwMvFrameId;
class fwMvPropertyIdType { }; typedef fwMvTemplateId< fwMvPropertyIdType > fwMvPropertyId;
class fwMvNetworkIdType { }; typedef fwMvTemplateId< fwMvNetworkIdType > fwMvNetworkId;
class fwMvNetworkDefIdType { }; typedef fwMvTemplateId< fwMvNetworkDefIdType > fwMvNetworkDefId;
class fwMvClipSetIdType { }; typedef fwMvTemplateId< fwMvClipSetIdType > fwMvClipSetId;
class fwMvClipSetVarIdType { }; typedef fwMvTemplateId< fwMvClipSetVarIdType > fwMvClipSetVarId;
class fwMvClipVariationSetIdType { }; typedef fwMvTemplateId< fwMvClipVariationSetIdType > fwMvClipVariationSetId;

class fwMvRequestId;
class fwMvStateId;

class fwMvStateId : public fwMvId
{
public:

	fwMvStateId() { }

	explicit fwMvStateId(u32 hash)
		: fwMvId(hash)
	{
	}

	explicit fwMvStateId(const char *str)
		: fwMvId(str)
	{
	}

	explicit fwMvStateId(const char *str, u32 hash)
		: fwMvId(str, hash)
	{
	}

	bool operator !=(const fwMvStateId &Id) const { return fwMvId::operator !=(Id); }

	operator fwMvRequestId() const;
};

class fwMvRequestId : public fwMvId
{
public:

	fwMvRequestId() { }

	explicit fwMvRequestId(u32 hash)
		: fwMvId(hash)
	{
	}

	explicit fwMvRequestId(const char *str)
		: fwMvId(str)
	{
	}

	explicit fwMvRequestId(const char *str, u32 hash)
		: fwMvId(str, hash)
	{
	}

	bool operator !=(const fwMvRequestId &Id) const { return fwMvId::operator !=(Id); }

	operator fwMvStateId() const;
};

typedef int fwAnimEventFlag;
typedef u32 fwAnimFlag;
typedef int fwAnimTrack;
typedef int fwMemoryGroup;

extern const fwMvFloatId FLOAT_ID_INVALID;
extern const fwMvBooleanId BOOLEAN_ID_INVALID;

extern const fwMvAnimId ANIM_ID_INVALID;

extern const fwMvClipId CLIP_ID_INVALID;
extern const fwMvClipSetId CLIP_SET_ID_INVALID;
extern const fwMvClipSetVarId CLIP_SET_VAR_ID_INVALID;
extern const fwMvClipSetVarId CLIP_SET_VAR_ID_DEFAULT;
extern const fwMvFilterId FILTER_ID_INVALID;

extern const fwMvExpressionSetId EXPRESSION_SET_ID_INVALID;
extern const fwMvExpressionsId EXPRESSIONS_ID_INVALID;

extern const fwMvFacialClipSetGroupId FACIAL_CLIP_SET_GROUP_ID_INVALID;

extern const fwMvClipVariationSetId CLIP_VARIATION_SET_ID_INVALID;

enum
{
	TP_NONE = 0,
	MG_NONE = 0,
};

#define INSTANT_BLEND_IN_DELTA (1000.0f)
#define INSTANT_BLEND_OUT_DELTA (-1000.0f)
#define FAST_BLEND_IN_DELTA (16.0f)
#define FAST_BLEND_OUT_DELTA (-16.0f)
#define NORMAL_BLEND_IN_DELTA (8.0f)
#define NORMAL_BLEND_OUT_DELTA (-8.0f)
#define SLOW_BLEND_IN_DELTA (4.0f)
#define SLOW_BLEND_OUT_DELTA (-4.0f)
#define REALLY_SLOW_BLEND_IN_DELTA (2.0f)
#define REALLY_SLOW_BLEND_OUT_DELTA (-2.0f)
#define MIGRATE_SLOW_BLEND_OUT_DELTA (-0.1f)

#define INSTANT_BLEND_DURATION (0.0f)
#define FAST_BLEND_DURATION (0.0625f)
#define NORMAL_BLEND_DURATION (0.125f)
#define SLOW_BLEND_DURATION (0.25f)
#define REALLY_SLOW_BLEND_DURATION (0.5f)
#define SUPER_SLOW_BLEND_DURATION (1.0f)
#define MIGRATE_SLOW_BLEND_DURATION (10.0f)

enum eStreamingPolicy
{
	SP_STREAMING = (1<<0),
	SP_SINGLEPLAYER_RESIDENT = (1<<1),
	SP_MULTIPLAYER_RESIDENT = (1<<2),
};

enum eStreamingPriority
{
	SP_Invalid = -1,
	SP_Variation,		//Won't make things work better, but will give some variation.
	SP_Low,				//Won't prevent things from working, but things might work better.
	SP_Medium,			//Not game breaking, but prevents things from working.
	SP_High,			//Game breaking if not loaded immediately.
	SP_NumPriorities,
};

enum eAnimPriority
{
	AP_LOW = 0,
	AP_MEDIUM,
	AP_HIGH,
	NUM_EANIMPRIORITY,
	EANIMPRIORITY_MAX = NUM_EANIMPRIORITY,
};

void eStreamingPriority_ToString(const eStreamingPriority val, class atString &string);
void eStreamingPolicy_ToString(const eStreamingPolicy val, class atString &string);
const char *eAnimPriority_ToString(const eAnimPriority val);

// Flags used in tracks of type AT_FIX_UP_EVENTS
// You used to need to update TagDefinations.xml
enum eAnimFixUpEventFlag
{
	AEF_START_TRANS_X_FIX_UP	= (1<<0),
	AEF_STOP_TRANS_X_FIX_UP		= (1<<1),
	AEF_START_TRANS_Y_FIX_UP	= (1<<2),
	AEF_STOP_TRANS_Y_FIX_UP		= (1<<3),
	AEF_START_TRANS_Z_FIX_UP	= (1<<4),
	AEF_STOP_TRANS_Z_FIX_UP		= (1<<5),
	AEF_START_ROT_FIX_UP		= (1<<6),
	AEF_STOP_ROT_FIX_UP			= (1<<7),
};

//Flag to define the exporter type
enum eExporterTypeFlag
{
	ET_MB_CUTSCENE = 0,
	ET_MB_IN_GAME,
	ET_MAX_VEHICLES,
	ET_MAX_RAYFIRE,
	ET_MAX_MAP,
	NUM_EXPORTERTYPE,
	EXPORTERTYPE_MAX = NUM_EXPORTERTYPE,
};

// framework level track types
enum eTrackType
{
	// 129 - 133 Used by crCreatureComponentParticleEffect
	kTrackParticleEffect0 = kTrackFirstReservedForProjectUse + 1,	// 129
	kTrackParticleEffect1,											// 130
	kTrackParticleEffect2,											// 131
	kTrackParticleEffect3,											// 132
	kTrackParticleEffect4,											// 133

	// 134 Used by the root stabilization expression
	kTrackUpperBodyFixupWeight,	

	// 135 Used by the independent mover expression
	kTrackIndependentMover,

	// 136 Used by the skeleton re-targeting expression
	kTrackRootHeight,			

	// 137 Used by the independent mover expression to track the weight of the required mover fixup
	kTrackIndependentMoverWeight,			

	// 138 Used by the first person camera system to track the weight of first person camera animations
	kTrackFirstPersonCameraWeight,

	// 139 Used by the first person camera system to track the weight of the left hand constraint helper data
	kTrackConstraintHelperLeftHandWeight,

	// 140 Used by the first person camera system to track the weight of the right hand constraint helper data
	kTrackConstraintHelperRightHandWeight,

	// 161 Used by the root stabilization expression
	kTrackUpperBodyFixupShadow = kTrackBoneRotation + kTrackFirstReservedForProjectUse + 32,

	// 177 Used by the root stabilization expression
	kTrackUpperBodyFixupScratch = kTrackBoneRotation + kTrackFirstReservedForProjectUse + 48
};

const char *eTrackType_ToString(const eTrackType val);

//typedef int fwAnimBoneTag;

struct fwShaderVarComponentData
{
	union
	{
		struct
		{
			u8 m_PedCompId;
			u8 m_MaskId;
			u8 m_Component;
			u8 m_Padding;  // unused
			u32 m_Hash;
		} unpack;
		u64 m_Packed;
	};
};

typedef short fwSyncedSceneId;
extern const fwSyncedSceneId INVALID_SYNCED_SCENE_ID;

#if !__SPU

template <class _T>
static void AppendBinMapFreeObjectPtr(_T *p)
{
	delete p;
}

template <class _T>
static void AppendBinMapFreeNone(_T)
{
}

template <typename _T, typename KT, typename FreeFunc >
inline
bool AppendBinMap(rage::atBinaryMap<_T, KT> &to, 
                  const rage::atBinaryMap<_T, KT> &from,
				  FreeFunc free_func = AppendBinMapFreeNone<_T>) 
{
	bool result = true;

	for(int i = 0; i < from.GetCount(); i++)
	{
		const _T *val = from.GetItem(i);
		const KT *key = from.GetKey(i);

		if(!to.SafeInsert(*key, *val))
		{
			free_func(*val);
			result = false;
		}
	}

	to.FinishInsertion();

	return result;
}

template <typename _T, typename KT, typename FreeFunc >
inline
	bool PatchBinMap(rage::atBinaryMap<_T, KT> &to, 
	const rage::atBinaryMap<_T, KT> &from,
	FreeFunc free_func = AppendBinMapFreeNone<_T>) 
{
	bool result = true;

	for(int i = 0; i < from.GetCount(); i++)
	{
		const _T *val = from.GetItem(i);
		const KT *key = from.GetKey(i);

		// Remove it if it already exists in the map
		to.RemoveKey(*key);

		// Now insert the new value
		if(!to.SafeInsert(*key, *val))
		{
			free_func(*val);
			result = false;
		}
	}

	to.FinishInsertion();

	return result;
}

#endif // !__SPU

} // namespace rage

#endif // FWANIMATION_DEFINES_H
