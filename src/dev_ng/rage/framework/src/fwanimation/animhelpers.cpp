// 
// fwanimation/animhelpers.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "animhelpers.h"

#include "animmanager.h"

#include "cranimation/frame.h"
#include "crclip/clip.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "fwanimation/directorcomponentcreature.h"
#include "vectormath/legacyconvert.h"

#include "atl/string.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

bool fwAnimHelpers::HasMoverTranslation(const crClip& clip)
{
	crFrameDataSingleDof frameData(kTrackMoverTranslation, 0, kFormatTypeVector3);
	crFrameSingleDof frame(frameData);
	frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());

	clip.Composite(frame, 0.f);

	return frame.Valid();
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimHelpers::HasCameraTranslation(const crClip& clip)
{
	crFrameDataSingleDof frameData(kTrackCameraTranslation, 0, kFormatTypeVector3);
	crFrameSingleDof frame(frameData);
	frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());

	clip.Composite(frame, 0.f);

	return frame.Valid();
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackTranslation(const crClip& clip, const float phase)
{
	crFrameDataSingleDof frameData(kTrackMoverTranslation, 0, kFormatTypeVector3);
	crFrameSingleDof frame(frameData);
	frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());

	clip.Composite(frame, clip.ConvertPhaseToTime(phase));

	Vector3 v(Vector3::ZeroType);
	frame.GetValue<Vec3V>(RC_VEC3V(v));
	return v;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackTranslation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetMoverTrackTranslation(*pClip, phase);
	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackTranslation(const crClip& clip, const float phase)
{
	crFrameDataSingleDof frameData(kTrackCameraTranslation, 0, kFormatTypeVector3);
	crFrameSingleDof frame(frameData);
	frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());

	clip.Composite(frame, clip.ConvertPhaseToTime(phase));

	Vector3 v(Vector3::ZeroType);
	frame.GetValue<Vec3V>(RC_VEC3V(v));
	return v;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackTranslation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetCameraTrackTranslation(*pClip, phase);
	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackTranslationDiff(const crClip& clip, const float startPhase, const float endPhase)
{
	if(!IsClose(startPhase, endPhase))
	{
		crFrameDataSingleDof frameData(kTrackMoverTranslation, 0, kFormatTypeVector3);
		crFrameSingleDof frame(frameData);

		Vec3V start(V_ZERO), end(V_ZERO);

		float duration = clip.GetDuration();

		clip.Composite(frame, startPhase * duration);
		frame.GetValue<Vec3V>(start);

		clip.Composite(frame, endPhase * duration);
		frame.GetValue<Vec3V>(end);

		Vec3V diff = end - start;

		return RCC_VECTOR3(diff);

	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackTranslationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetMoverTrackTranslationDiff(*pClip, startPhase, endPhase);
	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackTranslationDiff(const crClip& clip, const float startPhase, const float endPhase)
{
	if(!IsClose(startPhase, endPhase))
	{
		return GetCameraTrackTranslation(clip, endPhase) - GetCameraTrackTranslation(clip, startPhase);
	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackTranslationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetCameraTrackTranslationDiff(*pClip, startPhase, endPhase);
	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetMoverTrackRotation(const crClip& clip, const float phase)
{
	Quaternion q(Quaternion::IdentityType);

	crFrameDataSingleDof frameData(kTrackMoverRotation, 0, kFormatTypeQuaternion);
	crFrameSingleDof frame(frameData);
	frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());
	clip.Composite(frame, clip.ConvertPhaseToTime(phase));
	frame.GetValue<QuatV>(RC_QUATV(q));

	return q;
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetMoverTrackRotation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if (pClip)
	{
		return GetMoverTrackRotation(*pClip, phase);
	}

	return Quaternion(Quaternion::IdentityType);
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetCameraTrackRotation(const crClip& clip, const float phase)
{
	Quaternion q(Quaternion::IdentityType);

	crFrameDataSingleDof frameData(kTrackCameraRotation, 0, kFormatTypeQuaternion);
	crFrameSingleDof frame(frameData);
	frame.SetAccelerator(fwAnimDirectorComponentCreature::GetAccelerator());
	clip.Composite(frame, clip.ConvertPhaseToTime(phase));
	frame.GetValue<QuatV>(RC_QUATV(q));

	return q;
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetCameraTrackRotation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if (pClip)
	{
		return GetCameraTrackRotation(*pClip, phase);
	}

	return Quaternion(Quaternion::IdentityType);
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetMoverTrackRotationDiff(const crClip& clip, const float startPhase, const float endPhase)
{
	if(!IsClose(startPhase, endPhase))
	{
		crFrameDataSingleDof frameData(kTrackMoverRotation, 0, kFormatTypeQuaternion);
		crFrameSingleDof frame(frameData);

		QuatV start(V_IDENTITY), end(V_IDENTITY);

		float duration = clip.GetDuration();

		clip.Composite(frame, startPhase * duration);
		frame.GetValue<QuatV>(start);

		clip.Composite(frame, endPhase * duration);
		frame.GetValue<QuatV>(end);

		QuatV diff = Multiply(InvertNormInput(start), end);

		return RCC_QUATERNION(diff);
	}

	return Quaternion(Quaternion::IdentityType);
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetMoverTrackRotationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetMoverTrackRotationDiff(*pClip, startPhase, endPhase);
	}
	return Quaternion(Quaternion::IdentityType);
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetCameraTrackRotationDiff(const crClip& clip, const float startPhase, const float endPhase)
{
	if(!IsClose(startPhase, endPhase))
	{
		Quaternion qEnd = GetCameraTrackRotation(clip, endPhase);
		Quaternion qStart = GetCameraTrackRotation(clip, startPhase);

		qStart.Inverse();
		qStart.Multiply(qEnd);

		return qStart;
	}

	return Quaternion(Quaternion::IdentityType);
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetCameraTrackRotationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetCameraTrackRotationDiff(*pClip, startPhase, endPhase);
	}
	return Quaternion(Quaternion::IdentityType);
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetMoverTrackRotationDelta(const crClip& clip, float startPhase, float endPhase )
{
	if(startPhase > endPhase)
	{
		Quaternion deltaRot = GetMoverTrackRotationDiff(clip, startPhase, 1.f);
		deltaRot.Multiply(GetMoverTrackRotationDiff(clip, 0.f, endPhase));
		return deltaRot;
	}
	else
	{
		Quaternion finalRotation; finalRotation.Identity();
		Quaternion rotationQuat; rotationQuat.Identity();

		if(startPhase > 1.0f)
		{
			float loopCount = floorf(startPhase);

			startPhase -= loopCount;
			endPhase -= loopCount;
		}

		// In

		rotationQuat = GetMoverTrackRotationDiff(clip, startPhase, Min(endPhase, 1.0f));

		finalRotation.Multiply(rotationQuat);

		startPhase = 0.0f;
		endPhase -= Min(endPhase, 1.0f);

		// Loop

		if(endPhase > 1.0f)
		{
			rotationQuat = GetMoverTrackRotationDiff(clip, 0.0f, 1.0f);
			while(endPhase > 1.0f)
			{
				finalRotation.Multiply(rotationQuat);

				endPhase -= 1.0f;
			}
		}

		// Out

		if(endPhase > 0.0f)
		{
			rotationQuat = GetMoverTrackRotationDiff(clip, 0.0f, endPhase);

			finalRotation.Multiply(rotationQuat);

			endPhase -= endPhase;
		}

		return finalRotation;
	}
}

////////////////////////////////////////////////////////////////////////////////

Quaternion fwAnimHelpers::GetCameraTrackRotationDelta(const crClip& clip, float startPhase, float endPhase )
{
	if(startPhase > endPhase)
	{
		Quaternion deltaRot = GetCameraTrackRotationDiff(clip, startPhase, 1.f);
		deltaRot.Multiply(GetCameraTrackRotationDiff(clip, 0.f, endPhase));
		return deltaRot;
	}
	else
	{
		return GetCameraTrackRotationDiff(clip, startPhase, endPhase);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimHelpers::GetMoverTrackMatrixDelta(const crClip& clip, float startPhase, float endPhase, Matrix34& outMatrix)
{		
	outMatrix.d = GetMoverTrackDisplacement(clip, startPhase, endPhase);
	outMatrix.FromQuaternion(GetMoverTrackRotationDelta(clip, startPhase, endPhase));
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimHelpers::GetCameraTrackMatrixDelta(const crClip& clip, float startPhase, float endPhase, Matrix34& outMatrix)
{		
	outMatrix.d = GetCameraTrackDisplacement(clip, startPhase, endPhase);
	outMatrix.FromQuaternion(GetCameraTrackRotationDelta(clip, startPhase, endPhase));
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackVelocity(const crClip& clip)
{
	Vector3 v(VEC3_ZERO);
	Vector3 diff = GetMoverTrackTranslationDiff(clip, 0.f, 1.f);
	const float time = clip.GetDuration();
	animAssertf(time > 0.f, "Clip has duration <= 0.0 : %s\n", clip.GetName());
	if(time > 0.f)
	{
		v = diff;
		v /= time;
	}
	return v;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackVelocity(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId)
{
	Vector3 v(VEC3_ZERO);
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetMoverTrackVelocity(*pClip);
	}
	return v;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackVelocity(const crClip& clip)
{
	Vector3 v(VEC3_ZERO);
	Vector3 diff = GetCameraTrackTranslationDiff(clip, 0.f, 1.f);
	const float time = clip.GetDuration();
	animAssertf(time > 0.f, "Clip has duration <= 0.0 : %s\n", clip.GetName());
	if(time > 0.f)
	{
		v = diff;
		v /= time;
	}
	return v;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackVelocity(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId)
{
	Vector3 v(VEC3_ZERO);
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetCameraTrackVelocity(*pClip);
	}
	return v;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackTranslationDiffRotated(const crClip& clip, float startPhase, float endPhase, bool asDelta)
{
	if(!IsClose(startPhase, endPhase))
	{
		Quaternion startAdjustment(Quaternion::IdentityType);
		if(asDelta)
		{
			startAdjustment = GetMoverTrackRotation(clip, startPhase);
		}
		else if(startPhase > 0.f)
		{
			startAdjustment = GetMoverTrackRotationDelta(clip, 0.f, startPhase);
		}

		startAdjustment.Inverse();

		Matrix34 rotationAdjustment(M34_IDENTITY);
		rotationAdjustment.FromQuaternion(startAdjustment);

		Vector3 relativeOffset = GetMoverTrackTranslationDiff(clip, startPhase, endPhase);

		// get the correct offset for the current rotation in the mover track
		relativeOffset.Dot(rotationAdjustment); 

		return relativeOffset;
	}

	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackTranslationDiffRotated(const crClip& clip, float startPhase, float endPhase, bool asDelta)
{
	if(!IsClose(startPhase, endPhase))
	{
		Quaternion startAdjustment(Quaternion::IdentityType);
		if(asDelta)
		{
			startAdjustment = GetCameraTrackRotation(clip, startPhase);
		}
		else if(startPhase > 0.f)
		{
			startAdjustment = GetCameraTrackRotationDelta(clip, 0.f, startPhase);
		}

		startAdjustment.Inverse();

		Matrix34 rotationAdjustment(M34_IDENTITY);
		rotationAdjustment.FromQuaternion(startAdjustment);

		Vector3 relativeOffset = GetCameraTrackTranslationDiff(clip, startPhase, endPhase);

		// get the correct offset for the current rotation in the camera track
		relativeOffset.Dot(rotationAdjustment); 

		return relativeOffset;
	}

	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackDisplacement(const crClip& clip, float startPhase, float endPhase)
{
	Vector3 finalOffset;

	if (startPhase > endPhase)
	{
		finalOffset = GetMoverTrackTranslationDiffRotated(clip, startPhase, 1.f);
		Vector3 loopOffset = GetMoverTrackTranslationDiffRotated(clip, 0.f, endPhase);

		// need to rotate this second vector by the complete change in orientation from the start phase to 1.0f
		Quaternion startAdjustment = GetMoverTrackRotationDelta(clip, startPhase, 1.f);
		Matrix34 rotationAdjustment(M34_IDENTITY);
		rotationAdjustment.FromQuaternion(startAdjustment);

		loopOffset.Dot(rotationAdjustment);

		finalOffset += loopOffset;
	}
	else
	{
		finalOffset.Zero();
		Quaternion rotationQuat; rotationQuat.Identity();
		Matrix34 rotationMat; rotationMat.Identity();

		if(startPhase > 1.0f)
		{
			float loopCount = floorf(startPhase);

			startPhase -= loopCount;
			endPhase -= loopCount;
		}

		// In

		Vector3 offset = GetMoverTrackTranslationDiffRotated(clip, startPhase, Min(endPhase, 1.0f));
		Quaternion rotationOffset = GetMoverTrackRotationDelta(clip, startPhase, Min(endPhase, 1.0f));
		offset.Dot(rotationMat);

		finalOffset += offset;

		rotationQuat.Multiply(rotationOffset);
		rotationMat.Identity(); rotationMat.FromQuaternion(rotationQuat);

		startPhase = 0.0f;
		endPhase -= Min(endPhase, 1.0f);

		// Loop

		if(endPhase > 1.0f)
		{
			offset = GetMoverTrackTranslationDiffRotated(clip, 0.0f, 1.0f);
			rotationOffset = GetMoverTrackRotationDelta(clip, 0.0f, 1.0f);
			while(endPhase > 1.0f)
			{
				Vector3 tempOffset = offset; tempOffset.Dot(rotationMat);

				finalOffset += tempOffset;

				rotationQuat.Multiply(rotationOffset);
				rotationMat.Identity(); rotationMat.FromQuaternion(rotationQuat);

				endPhase -= 1.0f;
			}
		}

		// Out

		if(endPhase > 0.0f)
		{
			offset = GetMoverTrackTranslationDiffRotated(clip, 0.0f, endPhase);
			rotationOffset = GetMoverTrackRotationDelta(clip, 0.0f, endPhase);
			offset.Dot(rotationMat);

			finalOffset += offset;

			rotationQuat.Multiply(rotationOffset);
			rotationMat.Identity(); rotationMat.FromQuaternion(rotationQuat);

			endPhase -= endPhase;
		}
	}

	// need to transform the position offset by the inverse of the initial orientation on the mover (since the in game clip system does this implicitly using deltas)
	Quaternion initialRotation;
	initialRotation = GetMoverTrackRotation(clip, 0.f);
	initialRotation.Inverse();
	initialRotation.Transform(finalOffset);

	return finalOffset;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetMoverTrackDisplacement(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase)
{
	const crClip* pClip = fwAnimManager::GetClipIfExistsBySetId(clipSetId, clipId);
	animAssertf(pClip, "Couldn't find clip : %s\n", PrintClipInfo(clipSetId, clipId));
	if(pClip)
	{
		return GetMoverTrackDisplacement(*pClip, startPhase, endPhase);
	}
	return VEC3_ZERO;
}

////////////////////////////////////////////////////////////////////////////////

Vector3 fwAnimHelpers::GetCameraTrackDisplacement(const crClip& clip, float startPhase, float endPhase)
{
	Vector3 start, end;
	Vector3 finalOffset;

	if (startPhase > endPhase)
	{
		finalOffset = GetCameraTrackTranslationDiffRotated(clip, startPhase, 1.f);
		Vector3 loopOffset = GetCameraTrackTranslationDiffRotated(clip, 0.f, endPhase);

		// need to rotate this second vector by the complete change in orientation from the start phase to 1.0f
		Quaternion startAdjustment = GetCameraTrackRotationDelta(clip, startPhase, 1.f);
		Matrix34 rotationAdjustment(M34_IDENTITY);
		rotationAdjustment.FromQuaternion(startAdjustment);

		loopOffset.Dot(rotationAdjustment);

		finalOffset += loopOffset;
	}
	else
	{
		finalOffset = GetCameraTrackTranslationDiffRotated(clip, startPhase, endPhase);
	}

	// need to transform the position offset by the inverse of the initial orientation on the camera (since the in game clip system does this implicitly using deltas)
	Quaternion initialRotation;
	initialRotation = GetCameraTrackRotation(clip, 0.f);
	initialRotation.Inverse();
	initialRotation.Transform(finalOffset);

	return finalOffset;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimHelpers::ContributesToLinearMovement(const crClip& clip)
{
	Vector3 start = GetMoverTrackTranslation(clip, 0.f);
	Vector3 end = GetMoverTrackTranslation(clip, 1.f);
	float epsilon = 0.001f;

	return !start.IsClose(end, epsilon);
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimHelpers::ContributesToAngularMovement(const crClip& clip)
{
	Quaternion start = GetMoverTrackRotation(clip, 0.f);
	Quaternion end = GetMoverTrackRotation(clip, 1.f);
	float epsilon = 0.001f;

	//compare the two quaternions based on a tolerance value (strange there's no quaternion method for this...)
	if (!IsClose(start.x, end.x, epsilon))
		return true;
	if (!IsClose(start.y, end.y, epsilon))
		return true;
	if (!IsClose(start.z, end.z, epsilon))
		return true;
	if (!IsClose(start.w, end.w, epsilon))
		return true;

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimHelpers::ApplyInitialOffsetFromClip(const crClip& clip, Vector3& outPos, Quaternion& outRotation)
{
	if(clip.HasProperties())
	{
		// find the property from the clip
		const crProperty* positionOffsetProperty = clip.GetProperties()->FindProperty(sm_PositionPropertyId);
		const crProperty* rotationOffsetProperty = clip.GetProperties()->FindProperty(sm_RotationPropertyId);

		if(positionOffsetProperty)
		{
			const crPropertyAttribute* pAttrib = positionOffsetProperty->GetAttribute(sm_PositionPropertyId);
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeVector3)
			{
				const crPropertyAttributeVector3* pAttribVector3 = static_cast<const crPropertyAttributeVector3*>(pAttrib);

				Vector3 offsetPosition = RCC_VECTOR3(pAttribVector3->GetVector3());	// get the stored vector offset

				outRotation.Transform(offsetPosition);			// transform the offset by the original orientation
				outPos.Add(offsetPosition);						// add the offset to the original position
			}
		}

		if(rotationOffsetProperty)
		{
			const crPropertyAttribute* pAttrib = rotationOffsetProperty->GetAttribute(sm_RotationPropertyId);
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeQuaternion)
			{
				const crPropertyAttributeQuaternion* pAttribQuat = static_cast<const crPropertyAttributeQuaternion*>(pAttrib);
				outRotation.Multiply(RCC_QUATERNION(pAttribQuat->GetQuaternion()));
			}
		}

		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimHelpers::ApplyInitialOffsetFromClip(const crClip& clip, Matrix34& outMtx)
{
	if(clip.HasProperties())
	{
		// find the property from the clip
		const crProperty* positionOffsetProperty = clip.GetProperties()->FindProperty(sm_PositionPropertyId);
		const crProperty* rotationOffsetProperty = clip.GetProperties()->FindProperty(sm_RotationPropertyId);

		Matrix34 offsetMatrix(M34_IDENTITY);

		if(positionOffsetProperty)
		{
			const crPropertyAttribute* pAttrib = positionOffsetProperty->GetAttribute(sm_PositionPropertyId);
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeVector3)
			{
				const crPropertyAttributeVector3* pAttribVector3 = static_cast<const crPropertyAttributeVector3*>(pAttrib);
				offsetMatrix.d.Set(RCC_VECTOR3(pAttribVector3->GetVector3()));
			}
		}

		if (rotationOffsetProperty)
		{
			const crPropertyAttribute* pAttrib = rotationOffsetProperty->GetAttribute(sm_RotationPropertyId);
			if(pAttrib && pAttrib->GetType() == crPropertyAttribute::kTypeQuaternion)
			{
				const crPropertyAttributeQuaternion* pAttribQuat = static_cast<const crPropertyAttributeQuaternion*>(pAttrib);
				offsetMatrix.FromQuaternion(RCC_QUATERNION(pAttribQuat->GetQuaternion()));
			}
		}

		outMtx.DotFromLeft(offsetMatrix);
		return true;
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimHelpers::GetDebugClipName(const crClip& clip, atString& outDebugClipName)
{
	atString clipName(clip.GetName());

	clipName.Replace("pack:/", "");
	clipName.Replace(".clip", "");

	// Remove any leading absolute path
	int iLastSlashIndex = clipName.LastIndexOf("/");
	if (iLastSlashIndex != -1 && (iLastSlashIndex+1) < (int)clipName.length())
	{
		outDebugClipName.Set(clipName, iLastSlashIndex+1);
	}
	else
	{
		outDebugClipName = clipName;
	}
}

////////////////////////////////////////////////////////////////////////////////

crProperty::Key fwAnimHelpers::sm_PositionPropertyId = crProperty::CalcKey("InitialOffsetPosition", 0x320B8B63);
crProperty::Key fwAnimHelpers::sm_RotationPropertyId = crProperty::CalcKey("InitialOffsetRotation", 0x443D96E7);

////////////////////////////////////////////////////////////////////////////////

} // namespace rage



