#include "fwanimation/expressionsets.h"
#include "fwsys/gameskeleton.h"
#include "parser/manager.h"

using namespace rage;

#include "expressionsets_parser.h"

namespace rage {

//////////////////////////////////////////////////////////////////////////

const char *g_expressionSetDirectory = "common:/data/anim/expression_sets";

static const u32 s_HashOfNullString = atHashString("null",0x3ADB3357);

//////////////////////////////////////////////////////////////////////////

fwExpressionSetManager fwExpressionSetManager::ms_instance;

//////////////////////////////////////////////////////////////////////////

bool fwExpressionSet::ContainsExpression(const atHashString& hashToCompare)
{
	if (hashToCompare && hashToCompare != s_HashOfNullString)
	{
		int iNumExpressions = m_expressions.GetCount();
		for (int i = 0; i<iNumExpressions; i++)
		{
			if (m_expressions[i] == hashToCompare)
			{
				return true;
			}
		}
	}

	// Could not be found in the expression set
	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwExpressionSetManager::Init(u32 initMode)
{
	if(initMode == INIT_CORE)
	{
		GetInstance().Load();
	}
	else if(initMode == INIT_SESSION)
	{
		// Currently not required
	}
}

//////////////////////////////////////////////////////////////////////////

void fwExpressionSetManager::Shutdown(u32 shutdownMode)
{
	if(shutdownMode == SHUTDOWN_CORE)
	{
		GetInstance().DeleteAllExpressionSets();
	}
	else if(shutdownMode == SHUTDOWN_SESSION)
	{		
		// Currently not required
	}
}

//////////////////////////////////////////////////////////////////////////

bool fwExpressionSetManager::Load()
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	bool bResult = true;

	GetInstance().DeleteAllExpressionSets();
	
	ASSET.PushFolder(g_expressionSetDirectory);

	Displayf("Loading expression_sets.xml...\n");

	bResult = PARSER.LoadObject("expression_sets", "xml", GetInstance());
	Assertf(bResult, "Load expression_sets.xml failed!\n");

	Displayf("Loaded expression_sets.xml.\n");
	Displayf("ExpressionSets:%i", GetInstance().GetExpressionSetCount());

	ASSET.PopFolder();

	return bResult;
}

bool fwExpressionSetManager::Append(const char *fname)
{
	fwExpressionSetManager temp_inst;
	bool bResult = PARSER.LoadObject(fname, NULL, temp_inst);
	Assertf(bResult, "Load %s failed!\n", fname);
	Displayf("Loaded %s.\n", fname);
	AppendBinMap(GetInstance().m_expressionSets, temp_inst.m_expressionSets, AppendBinMapFreeObjectPtr<fwExpressionSet> );
	return bResult;
}

//////////////////////////////////////////////////////////////////////////

#if __BANK

bool fwExpressionSetManager::Save()
{
	bool bResult = true;

	ASSET.PushFolder(g_expressionSetDirectory);

	Displayf("Saving expression_sets.xml...\n");

	bResult = PARSER.SaveObject("expression_sets", "xml", &GetInstance());

	Displayf("Saved expression_sets.xml.\n");

	ASSET.PopFolder();

	return bResult;
}

#endif // __BANK

//////////////////////////////////////////////////////////////////////////

fwMvExpressionSetId fwExpressionSetManager::GetExpressionSetId(const atHashString& expressionSetName)
{
	fwMvExpressionSetId expressionSetId(expressionSetName);

	if(GetExpressionSet(expressionSetId))
	{
		return expressionSetId;
	}

	return EXPRESSION_SET_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

fwExpressionSet *fwExpressionSetManager::GetExpressionSet(const fwMvExpressionSetId &expressionSetId)
{
	if(expressionSetId != EXPRESSION_SET_ID_INVALID)
	{
		fwExpressionSet **ppExpressionSet = ms_instance.m_expressionSets.SafeGet(expressionSetId);
		if(ppExpressionSet)
		{
			return *ppExpressionSet;
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

void fwExpressionSetManager::DeleteAllExpressionSets()
{
	for(int i = 0; i < ms_instance.m_expressionSets.GetCount(); i ++)
	{
		delete ms_instance.m_expressionSets.GetRawDataArray()[i].data;
		ms_instance.m_expressionSets.GetRawDataArray()[i].data = NULL;
	}
	ms_instance.m_expressionSets.Reset();
}

//////////////////////////////////////////////////////////////////////////

} // namespace rage