// 
// fwanimation/AnimDirector.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#include "AnimDirector.h"

// rage headers
#include "creature/componentextradofs.h"
#include "cranimation/animtolerance.h"
#include "crmetadata/properties.h"
#include "crmetadata/property.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/tag.h"
#include "crmetadata/tagiterators.h"
#include "crmotiontree/motiontree.h"
#include "crmotiontree/nodeblend.h"
#include "crmotiontree/nodeblendn.h"
#include "crmotiontree/nodeexpression.h"
#include "crmotiontree/nodeik.h"
#include "crmotiontree/nodepm.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestfilter.h"
#include "crmotiontree/requestframe.h"
#include "crmotiontree/requestik.h"
#include "crmotiontree/requestmerge.h"
#include "crmotiontree/requestpose.h"

#include "entity/archetype.h"
#include "fwanimation/animhelpers.h"
#include "fwanimation/posematcher.h"
#include "fwanimation/move.h"
#include "fwanimation/expressionsets.h"
#include "fwanimation/networkdefs.h"
#include "fwaudio/audioentity.h"
#include "fwscene/stores/expressionsdictionarystore.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "fwscene/stores/networkdefstore.h"
#include "fwscene/stores/posematcherstore.h"
#include "fwsys/timer.h"
#include "math/simplemath.h"
#include "move/move.h"
#include "move/move_clip.h"
#include "move/move_network.h"
#include "parser/tree.h"
#include "parser/manager.h"

#include "directorcomponent.h"
#include "directorcomponentcreature.h"
#include "directorcomponentexpressions.h"
#include "directorcomponentfacialrig.h"
#include "directorcomponentmotiontree.h"
#include "directorcomponentmove.h"
#include "directorcomponentragdoll.h"
#include "directorcomponentsyncedscene.h"


namespace rage {

const u32 BONEMASK_ALL = 88128354u;

const fwMvPropertyId fwAnimDirector::ms_VisibleToScriptKey("VisibleToScript",0xF301E135);
u32 fwAnimDirector::ms_EventKey = ATSTRINGHASH("Event", 2915749078);

////////////////////////////////////////////////////////////////////////////////

fwAnimDirector::fwAnimDirector(fwEntity& entity)
: m_pEntity(&entity)
, m_iExpressionsDictIndex(-1)
, m_PoseUpdated(false)
, m_IncrementedPrePhysicsQueue(false)
, m_Updated(false)
, m_UpdatePostCameraRequested(false)
, m_bInitialised(false)
{
	GenerateComponentIndices();
}

void fwAnimDirector::Init(bool withRagDollComponent, bool withFacialRigComponent)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);
	
	if (!m_bInitialised)
	{
		animAssert(m_pEntity);

		fwAnimDirectorComponentCreature* componentCreature = static_cast<fwAnimDirectorComponentCreature*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeCreature));
		componentCreature->Init(*this);
		AddComponent(*componentCreature);

		InitPrePhysicsMotionTree();
		InitPreRenderMotionTree();

		if(withFacialRigComponent)
		{
			InitMidPhysicsMotionTree();
		}

		InitExpressions();

		if(withRagDollComponent)
		{
			fwAnimDirectorComponentRagDoll* componentRagDoll = static_cast<fwAnimDirectorComponentRagDoll*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeRagDoll));
			componentRagDoll->SetPhase(fwAnimDirectorComponent::kPhasePrePhysics);
			componentRagDoll->Init(*this);
			AddComponent(*componentRagDoll);
		}

		m_bActivePoseFromSkel = false;
		m_bInitialised = true;

#if __ASSERT
		m_uLastDeleteUnusedNetworkPlayersFrame = 0;
#endif // __ASSERT
	}
}

void fwAnimDirector::Shutdown()
{
	if (m_bInitialised)
	{
		// Taken from destructor (but we keep the entity pointer).
		MessageAllComponents(fwAnimDirectorComponent::kMessageShuttingDown);

		ShutdownExpressions();

		RemoveAllComponents();

		// Taken from Constructor
		m_iExpressionsDictIndex = -1;
		m_PoseUpdated = false;
		m_IncrementedPrePhysicsQueue = false;
		m_Updated = false;
		m_UpdatePostCameraRequested = false;

		m_bInitialised = false;
	}

	ClearMotionTreePreDelegateCallback();
}

void fwAnimDirector::WaitForAnyActiveUpdateToComplete(bool excludeMidPhysics)
{
#if CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE
	if(CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE)
	{
		fwAnimDirectorComponentMotionTree* componentMotionTreePrePhysics = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
		if(componentMotionTreePrePhysics && componentMotionTreePrePhysics->IsLocked())
		{
			fwAnimDirectorComponentMotionTree::sm_LockedGlobalCallStacks.CollectStack(0);
		}
	}
#endif // CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE

	for(int i=0; i<fwAnimDirectorComponent::kPhaseNum; ++i)
	{
		if(!excludeMidPhysics || i != fwAnimDirectorComponent::kPhaseMidPhysics)
		{
			fwAnimDirectorComponentMotionTree* component = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::ePhase(i));
			if(component)
			{
#if ENABLE_PRERENDER_BLOCKING_WARNINGS || ENABLE_PREPHYSICS_BLOCKING_WARNINGS
				Warningf("Verify motion tree update complete called on AnimDirector:%p", this);
#endif
				component->WaitOnComplete();
				//Assert(!component->IsLocked());
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirector::~fwAnimDirector()
{
	if (m_bInitialised)
	{
		MessageAllComponents(fwAnimDirectorComponent::kMessageShuttingDown);

		ShutdownExpressions();

		RemoveAllComponents();
	}

	m_pEntity = NULL;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::PreUpdate(float UNUSED_PARAM(deltaTime))
{
	// If we called Queue.Inc(), we should have already called Queue.Push() at this point,
	// otherwise we may have stalled the consumer.
	animAssert(!m_IncrementedPrePhysicsQueue);
	m_IncrementedPrePhysicsQueue = false;
	m_Updated = false;
	m_PoseUpdated = false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::StartUpdatePrePhysics(float deltaTime, bool ignoreVisibilityTest, fwAnimDirector* parent)
{
	animAssert(m_pEntity);

	UpdateMove(deltaTime, ignoreVisibilityTest, parent);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::EndUpdatePrePhysics(float deltaTime)
{
	UpdateVelocityAndAngularVelocity(deltaTime);

	fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	Assert(componentMove);  // mandatory component? - for now, at least while converting
	componentMove->DeleteUnusedNetworkPlayers();

#if __ASSERT
	m_uLastDeleteUnusedNetworkPlayersFrame = fwTimer::GetFrameCount();
#endif // __ASSERT

	SwapMoveOutputBuffers();

	StartUpdateMidPhysics(deltaTime);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::UpdateVelocityAndAngularVelocity(float deltaTime)
{
	fwAnimDirectorComponentCreature* componentCreature = GetComponent<fwAnimDirectorComponentCreature>();
	if(componentCreature)
	{
		componentCreature->ApplyVelocity(deltaTime, m_pEntity->CreateDynamicComponentIfMissing());
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::StartUpdatePostCamera(float deltaTime)
{
	animAssert(m_pEntity);

	// if not updated before, or updated, but UpdateMove not called
	if(!m_Updated || m_UpdatePostCameraRequested)
	{
		WaitForAnyActiveUpdateToComplete(false);

		// zero out delta time if already updated
		if(m_Updated)
		{
			deltaTime = 0.f;
		}

		bool updateAll = true;
		UpdateMove(deltaTime, updateAll);
	}
	else if(!m_PoseUpdated) // if was updated, but mover only, now visible so compose with full body 
	{
		WaitForAnyActiveUpdateToComplete(false);

		// Probably don't want to do this here, don't want something in this queue
		// from these post-camera updates:
		//	sm_PrePhysicsQueue.Inc();
		// Just assert that we've already updated, that should have pushed it into
		// the queue already.
		animAssert(m_Updated);

		// schedule motion tree for late compose	
		u8 motionTreePriority = GetEntity().GetMotionTreePriority(fwAnimDirectorComponent::kPhasePrePhysics);
		fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
		if(componentMotionTree)
		{
			componentMotionTree->Schedule(0.f, motionTreePriority, NULL, false, true);
		}
		m_PoseUpdated = true;
	}

	m_UpdatePostCameraRequested = false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::StartUpdateMidPhysics(float deltaTime)
{
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhaseMidPhysics);
	if(componentMotionTree)
	{
		MessageAllComponentsByPhase(fwAnimDirectorComponent::kPhaseMidPhysics, fwAnimDirectorComponent::kMessageUpdate, &deltaTime);

		componentMotionTree->Schedule(deltaTime, 0); 
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::StartUpdatePreRender(float deltaTime, crmtMotionTree* parent, bool copyLastBoundMatrices)
{
	animAssert(m_pEntity);

	if(m_pEntity->GetFragInst())
	{
		m_pEntity->GetFragInst()->SetBoundsPrerenderCopyLastMatrices(copyLastBoundMatrices);
	}

	MessageAllComponentsByPhase(fwAnimDirectorComponent::kPhasePreRender, fwAnimDirectorComponent::kMessageUpdate, &deltaTime);

	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePreRender);
	if(componentMotionTree)
	{
		u8 motionTreePriority = GetEntity().GetMotionTreePriority(fwAnimDirectorComponent::kPhasePreRender);
		componentMotionTree->Schedule(deltaTime, motionTreePriority, parent);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::SetPosed(bool posed)
{
	m_PoseUpdated = posed;

	if(!m_PoseUpdated)
	{
		// check if we can schedule immediately
		if(fwAnimDirectorComponentMotionTree::IsLockedGlobal())
		{
			fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
			if(componentMotionTree && componentMotionTree->IsComplete())
			{
				StartUpdatePostCamera(fwTimer::GetTimeStep());
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::InitClass()
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	crAnimation::InitClass();
	crClip::InitClass();
	crpmParameterizedMotion::InitClass();
	crExpressions::InitClass();
	crCreature::InitClass();	
	crmtMotionTree::InitClass();
	crmtNodeExpression::InitClass();
	crmtNodePm::InitClass();
	crmtNodeClip::InitClass();
	crmtNodeIk::InitClass();

	fwAnimDirectorComponent::InitClass();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::ShutdownClass()
{
	fwAnimDirectorComponent::ShutdownClass();

	crmtMotionTree::ShutdownClass();
	crCreature::ShutdownClass();
	crExpressions::ShutdownClass();
	crpmParameterizedMotion::ShutdownClass();
	crClip::ShutdownClass();
	crAnimation::ShutdownClass();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::UpdateClass()
{
	fwAnimDirectorComponent::UpdateClass();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::UpdateMove(float deltaTime, bool UNUSED_PARAM(ignoreVisibilityTest), fwAnimDirector* parent)
{
	animAssert(m_pEntity);

	// schedule a pre-physics motion tree update
	MessageAllComponentsByPhase(fwAnimDirectorComponent::kPhasePrePhysics, fwAnimDirectorComponent::kMessageUpdate, &deltaTime);

	// The first time we get here, we want to register that we will push something
	// onto the queue, and remember that we did that so we can actually push it later.
	if(!m_Updated)
	{
		animAssert(!m_IncrementedPrePhysicsQueue);
		m_IncrementedPrePhysicsQueue = true;
		sm_PrePhysicsQueue.Inc();
	}

	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
	if(componentMotionTree)
	{
		// If a parent fwAnimDirector was passed in, find its motion tree.
		crmtMotionTree* parentMotionTree = NULL;
		if(parent)
		{
			fwAnimDirectorComponentMotionTree* parentComponentMotionTree = parent->GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
			if(parentComponentMotionTree)
			{
				parentMotionTree = &parentComponentMotionTree->GetMotionTree();
			}
		}

		u8 motionTreePriority = GetEntity().GetMotionTreePriority(fwAnimDirectorComponent::kPhasePrePhysics);
		componentMotionTree->Schedule(deltaTime, motionTreePriority, parentMotionTree);
	}

	m_Updated = true;
	m_PoseUpdated = true;
}


void fwAnimDirector::SwapMoveOutputBuffers()
{
	fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	if(componentMove)
	{
		componentMove->SwapOutputBuffers();
	}
}

const crProperty *fwAnimDirector::FindPropertyInMoveNetworks(u32 uPropertyKey)
{
	const crProperty *pProp = NULL;
	fwMvPropertyId propId(uPropertyKey);

	// loop through the network players and read last frame's output parameters into their buffer
	for(fwMoveNetworkPlayer *pPlayer = GetNetworkPlayers(); pPlayer; pPlayer = pPlayer->GetNextPlayer())
	{
		pProp = pPlayer->GetProperty(propId);
		if(pProp)
		{
			return pProp;
		}
	}

	// Synchronized scenes don't trigger events through the normal clip pPlayer update 
	// (because their rate in the move network, and therefore their delta, is 0.0f)
	// Do a search of the synced scene clip to pick up any script visible events.
	fwAnimDirectorComponentSyncedScene *pComponentSyncedScene = GetComponent< fwAnimDirectorComponentSyncedScene >();
	if(pComponentSyncedScene && pComponentSyncedScene->IsPlayingSyncedScene() && fwAnimDirectorComponentSyncedScene::IsValidSceneId(pComponentSyncedScene->GetSyncedSceneId()))
	{
		const crClip *pClip = pComponentSyncedScene->GetSyncedSceneClip();
		if(pClip)
		{
			float currentSceneTime = pComponentSyncedScene->GetSyncedSceneTime(pComponentSyncedScene->GetSyncedSceneId());
			float sceneRate = pComponentSyncedScene->GetSyncedSceneRate(pComponentSyncedScene->GetSyncedSceneId());
			float startClipPhase = pClip->ConvertTimeToPhase(currentSceneTime - (fwTimer::GetTimeStep()*sceneRate));
			float endClipPhase = pClip->ConvertTimeToPhase(currentSceneTime);

			if (startClipPhase<0.0f)
			{
				if (pComponentSyncedScene->IsSyncedSceneLooped(pComponentSyncedScene->GetSyncedSceneId()))
				{
					startClipPhase = 1.0f+startClipPhase;
				}
				else
				{
					startClipPhase = 0.0f;
				}
			}

			if (sceneRate<0.0f)
			{
				float tmp = startClipPhase;
				startClipPhase = endClipPhase;
				endClipPhase = tmp;
			}

			// Synced scene duration could potentially be longer than the clip duration so clamp (mimic holding at the end)
			startClipPhase = Clamp(startClipPhase, 0.0f, 1.0f);
			endClipPhase = Clamp(endClipPhase, 0.0f, 1.0f);
			
			crTagIterator it(*pClip->GetTags(), startClipPhase, endClipPhase, uPropertyKey);
			if (*it)
			{
				return &(*it)->GetProperty();
			}
		}
	}

	// also search the entities root move network
	pProp = GetMove().GetProperty(propId);
	if(pProp)
	{
		return pProp;
	}

	return NULL;
}

const crProperty *fwAnimDirector::FindScriptVisiblePropertyInMoveNetworks(u32 uPropertyKey)
{
	const crProperty *pProp = NULL;

	// loop through the network players and read last frame's output parameters into their buffer
	for(fwMoveNetworkPlayer *pPlayer = GetNetworkPlayers(); pPlayer; pPlayer = pPlayer->GetNextPlayer())
	{
		pProp = pPlayer->GetProperty(ms_VisibleToScriptKey);
		if(pProp)
		{
			const crPropertyAttribute *pAttr = pProp->GetAttribute(ms_EventKey);
			if(animVerifyf(pAttr && pAttr->GetType() == crPropertyAttribute::kTypeHashString, ""))
			{
				const crPropertyAttributeHashString *pAttrHashString = static_cast< const crPropertyAttributeHashString * >(pAttr);
				if(pAttrHashString->GetHashString().GetHash() == uPropertyKey)
				{
					return pProp;
				}
			}
		}
	}

	// Synchronized scenes don't trigger events through the normal clip pPlayer update 
	// (because their rate in the move network, and therefore their delta, is 0.0f)
	// Do a search of the synced scene clip to pick up any script visible events.
	fwAnimDirectorComponentSyncedScene *pComponentSyncedScene = GetComponent< fwAnimDirectorComponentSyncedScene >();
	if(pComponentSyncedScene && pComponentSyncedScene->IsPlayingSyncedScene() && fwAnimDirectorComponentSyncedScene::IsValidSceneId(pComponentSyncedScene->GetSyncedSceneId()))
	{
		const crClip *pClip = pComponentSyncedScene->GetSyncedSceneClip();
		if(pClip)
		{
			float rate = pComponentSyncedScene->GetSyncedSceneRate(pComponentSyncedScene->GetSyncedSceneId());
			float currentSceneTime = pComponentSyncedScene->GetSyncedSceneTime(pComponentSyncedScene->GetSyncedSceneId());
			float startClipPhase = pClip->ConvertTimeToPhase(currentSceneTime - (fwTimer::GetTimeStep() * rate));
			float endClipPhase = pClip->ConvertTimeToPhase(currentSceneTime);

			// Synced scene duration could potentially be longer than the clip duration so clamp (mimic holding at the end)
			startClipPhase = Clamp(startClipPhase, 0.0f, 1.0f);
			endClipPhase = Clamp(endClipPhase, 0.0f, 1.0f);

			if(rate >= 0.0f)
			{
				pProp = FindScriptVisiblePropertyInPhaseRange(pClip, startClipPhase, endClipPhase, uPropertyKey);
			}
			else
			{
				pProp = FindScriptVisiblePropertyInPhaseRange(pClip, endClipPhase, startClipPhase, uPropertyKey);
			}
			if (pProp)
			{
				return pProp;
			}
		}
	}

	pProp = GetMove().GetProperty(ms_VisibleToScriptKey);
	if(pProp)
	{
		const crPropertyAttribute *pAttr = pProp->GetAttribute(ms_EventKey);
		if(animVerifyf(pAttr && pAttr->GetType() == crPropertyAttribute::kTypeHashString, ""))
		{
			const crPropertyAttributeHashString *pAttrHashString = static_cast< const crPropertyAttributeHashString * >(pAttr);
			if(pAttrHashString->GetHashString().GetHash() == uPropertyKey)
			{
				return pProp;
			}
		}
	}

	return NULL;
}

const crProperty *fwAnimDirector::FindScriptVisiblePropertyInPhaseRange( const crClip* pClip, float startPhase, float endPhase, u32 uPropertyKey)
{
	const crTags *pTags = pClip->GetTags();
	if(pTags)
	{
		float prevPhase = startPhase;

		int currentLoop = static_cast< int >(floorf(endPhase));
		int prevLoop = static_cast< int >(floorf(prevPhase));

		Assert(prevLoop <= currentLoop);

		while(prevLoop < currentLoop)
		{
			float shiftedPrevPhase = prevPhase;
			while(shiftedPrevPhase < 0.0f)
			{
				shiftedPrevPhase += 1.0f;
			}

			Assertf(InRange(shiftedPrevPhase, 0.0f, 1.0f), "shiftedPrevPhase %f is not in range 0.0 <-> 1.0! pClip:%s uPropertyKey:%u %s", shiftedPrevPhase, pClip->GetName(), uPropertyKey, atHashString(uPropertyKey).TryGetCStr());
			for(crTagIterator it(*pTags, shiftedPrevPhase, 1.0f, ms_VisibleToScriptKey); *it; ++it)
			{
				const crTag *pTag = (*it);
				if(pTag)
				{
					const crProperty *pProp = &pTag->GetProperty();
					if(pProp->GetKey() == ms_VisibleToScriptKey)
					{
						const crPropertyAttribute *pAttr = pProp->GetAttribute(ms_EventKey);
						if(animVerifyf(pAttr && pAttr->GetType() == crPropertyAttribute::kTypeHashString, ""))
						{
							const crPropertyAttributeHashString *pAttrHashString = static_cast< const crPropertyAttributeHashString * >(pAttr);
							if(pAttrHashString->GetHashString().GetHash() == uPropertyKey)
							{
								return pProp;
							}
						}
					}
				}
			}

			prevLoop ++;
			prevPhase = static_cast< float >(prevLoop);
		}

		Assertf(InRange(prevPhase, 0.0f, 1.0f), "prevPhase %f is not in range 0.0 <-> 1.0! pClip:%s uPropertyKey:%u %s", prevPhase, pClip->GetName(), uPropertyKey, atHashString(uPropertyKey).TryGetCStr());
		Assertf(InRange(endPhase, 0.0f, 1.0f), "endPhase %f is not in range 0.0 <-> 1.0! pClip:%s uPropertyKey:%u %s", endPhase, pClip->GetName(), uPropertyKey, atHashString(uPropertyKey).TryGetCStr());
		for(crTagIterator it(*pTags, prevPhase, endPhase, ms_VisibleToScriptKey); (*it); ++it)
		{
			const crTag *pTag = (*it);
			if(pTag)
			{
				const crProperty *pProp = &pTag->GetProperty();
				if(pProp->GetKey() == ms_VisibleToScriptKey)
				{
					const crPropertyAttribute *pAttr = pProp->GetAttribute(ms_EventKey);
					if(animVerifyf(pAttr && pAttr->GetType() == crPropertyAttribute::kTypeHashString, ""))
					{
						const crPropertyAttributeHashString *pAttrHashString = static_cast< const crPropertyAttributeHashString * >(pAttr);
						if(pAttrHashString->GetHashString().GetHash() == uPropertyKey)
						{
							return pProp;
						}
					}
				}
			}
		}
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////////////
//	Static asset loading implementation
//////////////////////////////////////////////////////////////////////////

const crClip *fwAnimDirector::RetrieveClip(u32 clipContext, u32 clipParamA, u32 clipParamB, const rage::mvMotionWeb *network)
{
	switch(clipContext)
	{
	case mvNodeClipDef::kClipContextVariableClipSet:
		{
			fwMvClipSetVarId clipSetVarId(clipParamA);
			fwMvClipId clipId(clipParamB);

			// Check network exists
			Assertf(network, "MoVE network must not be null! "
				"(clipSetVarId: %s %u, clipId: %s %u)",
				clipSetVarId.TryGetCStr(), clipSetVarId.GetHash(), clipId.TryGetCStr(), clipId.GetHash());
			if(network)
			{
				// Get move network interface
				fwMoveNetworkInterface *moveNetworkInterface = reinterpret_cast< fwMoveNetworkInterface * >(network->GetMoveNetworkInterface());
				Assertf(moveNetworkInterface, "Could not get MoVE network interface from MoVE network! "
					"(clipSetVarId: %s %u, clipId: %s %u, network: %p %s)",
					clipSetVarId.TryGetCStr(), clipSetVarId.GetHash(), clipId.TryGetCStr(), clipId.GetHash(), network, FindNetworkDefName(network->GetDefinition()));
				if(moveNetworkInterface)
				{
					// Get clip set
					fwMvClipSetId clipSetId = moveNetworkInterface->GetClipSetId(clipSetVarId);
					Assertf(clipSetId.IsNotNull(), "Could not get clip set id from MoVE network clip set variable id! Has it been set? "
						"(clipSetVarId: %s %u, clipSetId: %s %u, clipId: %s %u, network: %p %s)",
						clipSetVarId.TryGetCStr(), clipSetVarId.GetHash(), clipSetId.TryGetCStr(), clipSetId.GetHash(), clipId.TryGetCStr(), clipId.GetHash(), network, FindNetworkDefName(network->GetDefinition()));
					if (clipSetId.IsNotNull())
					{
						fwClipSet *clipSet = fwClipSetManager::GetClipSet(clipSetId);
						Assertf(clipSet, "Could not get clip set! Does it exist? "
							"(clipSetVarId: %s %u, clipSetId: %s %u, clipId: %s %u, network: %p %s)",
							clipSetVarId.TryGetCStr(), clipSetVarId.GetHash(), clipSetId.TryGetCStr(), clipSetId.GetHash(), clipId.TryGetCStr(), clipId.GetHash(), network, FindNetworkDefName(network->GetDefinition()));
						if (clipSet)
						{
							// Get clip
							const crClip *clip = clipSet->GetClip(clipId);
							Assertf(clip, "Could not get clip from clip set or its fallbacks! Does it exist? "
								"(clipSetVarId: %s %u, clipSetId: %s %u, clipId: %s %u, network: %p %s)",
								clipSetVarId.TryGetCStr(), clipSetVarId.GetHash(), clipSetId.TryGetCStr(), clipSetId.GetHash(), clipId.TryGetCStr(), clipId.GetHash(), network, FindNetworkDefName(network->GetDefinition()));
							if (clip)
							{
								clip->AddRef();
							}
							return clip;
						}
					}
				}
			}
		} break;
	case mvNodeClipDef::kClipContextClipDictionary:
		{
			atHashString clipDictionaryName(clipParamA);
			atHashString clipName(clipParamB);

			// Get clip dictionary slot index
			int clipDictionaryIndex = g_ClipDictionaryStore.FindSlotFromHashKey(clipDictionaryName).Get();
			Assertf(clipDictionaryIndex != -1, "Could not find clip dictionary slot index! Does the clip dictionary exist? "
				"(clipDictionaryNameHash: %s %u, clipNameHash: %s %u, network: %p %s)",
				clipDictionaryName.TryGetCStr(), clipDictionaryName.GetHash(), clipName.TryGetCStr(), clipName.GetHash(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
			if(clipDictionaryIndex != -1)
			{
				bool forced = false;  
				DEV_ONLY(forced = sm_ForceLoadAnims);
				fwClipDictionaryLoader loader(clipDictionaryIndex, forced);

				// Get clip dictionary
				crClipDictionary *clipDictionary = loader.GetDictionary();
				Assertf(clipDictionary, "Clip dictionary is not loaded! "
					"(clipDictionaryIndex:%i, clipDictionaryNameHash: %s %u, clipNameHash: %s %u, network: %p %s)",
					clipDictionaryIndex, clipDictionaryName.TryGetCStr(), clipDictionaryName.GetHash(), clipName.TryGetCStr(), clipName.GetHash(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
				if(clipDictionary)
				{
					// Get clip
					crClip *clip = clipDictionary->GetClip(clipName);
					Assertf(clip, "Could not get clip from clip dictionary! Does it exist? "
						"(clipDictionaryNameHash: %s %u, clipNameHash: %s %u, network: %p %s)",
						clipDictionaryName.TryGetCStr(), clipDictionaryName.GetHash(), clipName.TryGetCStr(), clipName.GetHash(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
					if(clip)
					{
						clip->AddRef();
					}
					return clip;
				}
			}
		} break;
	case mvNodeClipDef::kClipContextAbsoluteClipSet:
		{
			fwMvClipSetId clipSetId(clipParamA);
			fwMvClipId clipId(clipParamB);

			// Get clip set
			fwClipSet *clipSet = fwClipSetManager::GetClipSet(clipSetId);
			Assertf(clipSet, "Could not get clip set! Does it exist? "
				"(clipSetId: %s %u, clipId: %s %u, network: %p %s)",
				clipSetId.TryGetCStr(), clipSetId.GetHash(), clipId.TryGetCStr(), clipId.GetHash(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
			if(clipSet)
			{
				// Get clip
				const crClip *clip = clipSet->GetClip(clipId);
				Assertf(clipSet, "Could not get clip from clip set or its fallbacks! Does it exist? "
					"(clipSetId: %s %u, clipId: %s %u, network: %p %s)",
					clipSetId.TryGetCStr(), clipSetId.GetHash(), clipId.TryGetCStr(), clipId.GetHash(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
				if(clip)
				{
					clip->AddRef();
				}
				return clip;
			}
		} break;
#if __DEV && CR_DEV
	case mvNodeClipDef::kClipContextLocalFile:
		{
			const char *szFilePath = atHashString(clipParamA).TryGetCStr();
			Assertf(szFilePath, "Could not retrieve clip file path from hash %u! Has the MoVE network got a *_nodemap.xml file in common:/non_final?",
				clipParamA);
			if(szFilePath)
			{
				atString filePath(szFilePath);
				filePath.Lowercase();
			
				// Is clip name a filename ending in .clip?
				bool endsWithDotClip = filePath.EndsWith(".clip");
				if(endsWithDotClip)
				{
					// Tokenize into filename + extension
					atArray< atString > clipNameTokens;
					filePath.Split(clipNameTokens, ".");
					Assertf(clipNameTokens.GetCount() == 2, "Could not split file path into filename + extension! "
						"(filePath: %s, network: %p %s)",
						filePath.c_str(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
					if(clipNameTokens.GetCount() == 2)
					{
						bool assetExists = ASSET.Exists(clipNameTokens[0], clipNameTokens[1]);
						Assertf(clipNameTokens.GetCount() == 2, "Could not find local file! Does it exist? "
							"(filePath: %s, network: %p %s)",
							filePath.c_str(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
						if (assetExists)
						{
							// lossless compression on hot-loaded animations - keep compression techniques cheap
							crAnimToleranceSimple tolerance(SMALL_FLOAT, SMALL_FLOAT, SMALL_FLOAT, SMALL_FLOAT, crAnimTolerance::kDecompressionCostDefault, crAnimTolerance::kCompressionCostLow);
			
							// load animation directly, compress on load
							crAnimLoaderCompress loader;
							loader.Init(tolerance);
			
							// WARNING: This is known to be slow and dynamic memory intense.
							// Do NOT profile or bug performance of this call, it is DEV only.
							u32 startTime = sysTimer::GetSystemMsTime();
							const crClip *clip = crClip::AllocateAndLoad(filePath.c_str(), &loader);
							Assertf(clipNameTokens.GetCount() == 2, "Could not load local file! "
								"(filePath: %s, network: %p %s)",
								filePath.c_str(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
							u32 endTime = sysTimer::GetSystemMsTime();
							if(endTime - startTime > 100)
							{
								Warningf("Loading %s took %u milliseconds, loading clips using local file paths is always slow and can cause the game to stutter!",
									szFilePath, endTime - startTime);
							}
							return clip;
						}
					}
				}
#if __ASSERT
				else
				{
					Assertf(endsWithDotClip, "File path does not end with .clip! "
						"(filePath: %s, network: %p %s)",
						filePath.c_str(), network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
				}
#endif // __ASSERT
			}
		} break;
#endif // __DEV && CR_DEV
	default:
		{
			Assertf(false, "Clip context is not valid! "
				"(clipContext: %u, network: %p %s)",
				clipContext, network, network ? FindNetworkDefName(network->GetDefinition()) : "Unknown");
		} break;
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
//	Static asset loading implementation
//////////////////////////////////////////////////////////////////////////
const crExpressions* fwAnimDirector::RetrieveExpression(u32 expressionDict, u32 expression, const mvMotionWeb * BANK_ONLY(network))
{
	const crExpressions* pExpressions = NULL;

	if (expressionDict != 0 && expression != 0)
	{
		// Find expression dictionary
		s32 expressionDictSlot = g_ExpressionDictionaryStore.FindSlotFromHashKey(expressionDict).Get();
		animAssertf(expressionDictSlot != -1, "Could not find expression dictionary %u '%s' in MoVE network %s!", expressionDict, atHashString(expressionDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
		if (expressionDictSlot != -1)
		{
			bool forced = false;  DEV_ONLY(forced = sm_ForceLoadAnims);
			fwExpressionsDictionaryLoader loader(expressionDictSlot, forced);

			// Get expression dictionary
			crExpressionsDictionary *pExpressionDict = loader.GetDictionary();
			animAssertf(pExpressionDict, "Could not get expression dictionary %u '%s' in MoVE network %s!", expressionDict, atHashString(expressionDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
			if (pExpressionDict)
			{
				// Get expression from expression dictionary
				pExpressions = pExpressionDict->Lookup(expression);
				animAssertf(pExpressions, "Could not get expression %u '%s' from expression dictionary %u '%s' in MoVE network %s!", expression, atHashString(expression).TryGetCStr(), expressionDict, atHashString(expressionDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
				if (pExpressions)
				{
					pExpressions->AddRef(); // Move does the release ref internally
				}
			}
		}
	}
	else
	{
#if __BANK
		animDisplayf("Unable to find expression dictionary %u '%s' / expression %u '%s' from MoVE network %s", expressionDict, atHashString(expressionDict).TryGetCStr(), expression, atHashString(expression).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN" );
#endif // __BANK
	}

	return pExpressions;
}

//////////////////////////////////////////////////////////////////////////

const mvNetworkDef* fwAnimDirector::RetrieveNetworkDef(u32 networkDefHash, const mvMotionWeb* UNUSED_PARAM(network))
{
	const mvNetworkDef* def = NULL;

	atHashString networkHash(networkDefHash);

	strLocalIndex slot = strLocalIndex(g_NetworkDefStore.FindSlotFromHashKey(networkDefHash));
	if (slot.Get()>-1)
	{
		if (g_NetworkDefStore.HasObjectLoaded(slot))
		{
			def = g_NetworkDefStore.Get(slot);
		}
		else
		{
			animErrorf("Referenced network def '%s' is not streamed in!", networkHash.TryGetCStr());
		}
	}

	return def;
}

void fwAnimDirector::UnloadNetworkDefinitions(const char *filename)
{
	fwMoveNetworkDefs *networkDefs = NULL;

	if (!PARSER.LoadObjectPtr(filename, "meta", networkDefs))
	{
		animErrorf("UnloadNetworkDefinitions - Could not load Move Network Definitions from file '%s'!", filename);
		return;
	}

	for(int i = 0; i < networkDefs->m_moveNetworkDefs.GetCount(); i ++)
	{
		atHashString networkDefHash(networkDefs->m_moveNetworkDefs[i]);

		strLocalIndex slot = strLocalIndex(g_NetworkDefStore.FindSlotFromHashKey(networkDefHash));

		if (slot.Get()>-1)
		{
			g_NetworkDefStore.ClearRequiredFlag(slot.Get(), STRFLAG_DONTDELETE);
			g_NetworkDefStore.RemoveRef(slot, REF_OTHER);

			Assert(g_NetworkDefStore.GetNumRefs(slot)==0);
			g_NetworkDefStore.StreamingRemove(slot);
		}
		else
		{
			animErrorf("Could not find network def %s referenced from file '%s'!", networkDefHash.TryGetCStr(), filename);
		}
	}
}

void fwAnimDirector::LoadNetworkDefinitions(const char *filename)
{
	fwMoveNetworkDefs *networkDefs = NULL;

	if (!PARSER.LoadObjectPtr(filename, "meta", networkDefs))
	{
		animErrorf("Could not load Move Network Definitions from file '%s'!", filename);
		return;
	}

	for(int i = 0; i < networkDefs->m_moveNetworkDefs.GetCount(); i ++)
	{
		atHashString networkDefHash(networkDefs->m_moveNetworkDefs[i]);

		strLocalIndex slot = strLocalIndex(g_NetworkDefStore.FindSlotFromHashKey(networkDefHash));

		if (slot.Get()>-1)
		{
			fwNetworkDefLoader loader(slot.Get(), false);

			if(!loader.IsLoaded() && loader.Load(true))
			{
				g_NetworkDefStore.AddRef(slot, REF_OTHER);
			}

			animAssertf(loader.IsValid(), "Network Def '%s' has failed to load\n", g_NetworkDefStore.GetName(slot));
		}
		else
		{
			animErrorf("Could not find network def %s referenced from file '%s'!", networkDefHash.TryGetCStr(), filename);
		}
	}
}

//////////////////////////////////////////////////////////////////////////

crFrameFilterMultiWeight *fwAnimDirector::RetrieveFilter(u32 filterDict, u32 filter, const mvMotionWeb * BANK_ONLY(network))
{
	if(filterDict != 0 && filter != 0 && filter != BONEMASK_ALL)
	{
		strLocalIndex filterDictSlot = strLocalIndex(g_FrameFilterDictionaryStore.FindSlotFromHashKey(filterDict));
		animAssertf(filterDictSlot != -1, "Could not find filter dictionary %u '%s' in MoVE network %s!", filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
		if(filterDictSlot != -1)
		{
			crFrameFilterDictionary *pFilterDict = g_FrameFilterDictionaryStore.Get(filterDictSlot);
			animAssertf(pFilterDict, "Could not get filter dictionary %u '%s' in MoVE network %s!", filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
			if(pFilterDict)
			{
				crFrameFilter *pFilter = pFilterDict->Lookup(filter);
				animAssertf(pFilter, "Could not get filter %u '%s' from filter dictionary %u '%s' in MoVE network %s!", filter, atHashString(filter).TryGetCStr(), filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
				if(pFilter)
				{
					crFrameFilterMultiWeight *pFilterMultiWeight = pFilter->DynamicCast< crFrameFilterMultiWeight >();
					animAssertf(pFilterMultiWeight, "Filter %u '%s' is not a crFrameFilterMultiWeight from filter dictionary %u '%s' in MoVE network %s!", filter, atHashString(filter).TryGetCStr(), filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
					if(pFilterMultiWeight)
					{
						return pFilterMultiWeight;
					}
				}
			}
		}
	}
	else
	{
		if(filter != BONEMASK_ALL)
		{
#if __BANK
			animDisplayf("Could not find filter dictionary %u '%s' \\ filter %u '%s' in MoVE network %s!", filterDict, atHashString(filterDict).TryGetCStr(), filter, atHashString(filter).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
#endif // __BANK
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////

mvTransitionFrameFilter *fwAnimDirector::RetrieveTransitionFilter(u32 filterDict, u32 filter, const mvMotionWeb * BANK_ONLY(network))
{
	if(filterDict != 0 && filter != 0 && filter != BONEMASK_ALL)
	{
		strLocalIndex filterDictSlot = strLocalIndex(g_FrameFilterDictionaryStore.FindSlotFromHashKey(filterDict));
		animAssertf(filterDictSlot != -1, "Could not find filter dictionary %u '%s' in MoVE network %s!", filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
		if(filterDictSlot != -1)
		{
			crFrameFilterDictionary *pFilterDict = g_FrameFilterDictionaryStore.Get(filterDictSlot);
			animAssertf(pFilterDict, "Could not get filter dictionary %u '%s' in MoVE network %s!", filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
			if(pFilterDict)
			{
				crFrameFilter *pFilter = pFilterDict->Lookup(filter);
				animAssertf(pFilter, "Could not get filter %u '%s' from filter dictionary %u '%s' in MoVE network %s!", filter, atHashString(filter).TryGetCStr(), filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
				if(pFilter)
				{
					mvTransitionFrameFilter *pTransitionFilter = pFilter->DynamicCast< mvTransitionFrameFilter >();
					animAssertf(pTransitionFilter, "Filter %u '%s' is not a mvTransitionFrameFilter from filter dictionary %u '%s' in MoVE network %s!", filter, atHashString(filter).TryGetCStr(), filterDict, atHashString(filterDict).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
					if(pTransitionFilter)
					{
						return pTransitionFilter;
					}
				}
			}
		}
	}
	else
	{
		if(filter != BONEMASK_ALL)
		{
#if __BANK
			animDisplayf("Could not find filter dictionary %u '%s' \\ filter %u '%s' in MoVE network %s!", filterDict, atHashString(filterDict).TryGetCStr(), filter, atHashString(filter).TryGetCStr(), network ? FindNetworkDefName(network->GetDefinition()) : "UNKNOWN");
#endif // __BANK
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////
//	static network def streaming methods
///////////////////////////////////////////////////////////////////////////

bool fwAnimDirector::RequestNetworkDef( const fwMvNetworkDefId &/*networkDefId*/ )
{
	return true;
}

void fwAnimDirector::ForceLoadNetworkDef( const fwMvNetworkDefId &/*networkDefId*/ )
{
	// do nothing at the moment
}

bool fwAnimDirector::IsNetworkDefLoaded( const fwMvNetworkDefId &networkDefId )
{
	// Is the current slot index valid
	s32 iNetworkDefIndex = g_NetworkDefStore.FindSlotFromHashKey(networkDefId.GetHash()).Get();
	fwNetworkDefLoader loader(iNetworkDefIndex, false);
	return loader.IsLoaded();
}

const mvNetworkDef& fwAnimDirector::GetNetworkDef( const fwMvNetworkDefId &networkDefId )
{
	animAssertf(IsNetworkDefLoaded(networkDefId), "Network Def '%s' is not loaded", networkDefId.GetCStr());
	return *g_NetworkDefStore.Get(strLocalIndex(g_NetworkDefStore.FindSlotFromHashKey(networkDefId.GetHash())));
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::InitPrePhysicsMotionTree()
{
	animAssert(!GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics));
	CreateComponentMotionTree(fwAnimDirectorComponent::kPhasePrePhysics);

	animAssert(!GetComponentByPhase<fwAnimDirectorComponentMove>(fwAnimDirectorComponent::kPhasePrePhysics));
	fwAnimDirectorComponentMove* componentMove = static_cast<fwAnimDirectorComponentMove*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeMove));
	componentMove->SetPhase(fwAnimDirectorComponent::kPhasePrePhysics);
	componentMove->Init(*this);
	AddComponent(*componentMove);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::InitMidPhysicsMotionTree()
{
	animAssert(!GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhaseMidPhysics));
	CreateComponentMotionTree(fwAnimDirectorComponent::kPhaseMidPhysics, false);

	animAssert(!GetComponentByPhase<fwAnimDirectorComponentFacialRig>(fwAnimDirectorComponent::kPhaseMidPhysics));
	fwAnimDirectorComponentFacialRig* componentFacialRig = static_cast<fwAnimDirectorComponentFacialRig*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeFacialRig));
	componentFacialRig->SetPhase(fwAnimDirectorComponent::kPhaseAll);
	componentFacialRig->Init(*this);
	AddComponent(*componentFacialRig);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::InitPreRenderMotionTree()
{
	animAssert(!GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePreRender));
	fwAnimDirectorComponentMotionTree* componentMotionTree = CreateComponentMotionTree(fwAnimDirectorComponent::kPhasePreRender);

	crmtRequestPose reqPose;
	crmtRequest* req = &reqPose;

	// TODO --- add face rig and merge node

#if ENABLE_MIRROR_TEST
	crmtRequestMirror reqMirror;
	if (m_pEntity->GetIsTypePed())
	{
		reqMirror.Init(*req);
		req = &reqMirror;
	}
#else

	crmtRequestIk reqIk;

	if (m_pEntity->InitIkRequest(*req, reqIk))
	{
		req = &reqIk;
	}
#endif

	componentMotionTree->GetMotionTree().Request(*req);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::InitExpressions(bool reset)
{
	animAssert(m_pEntity);

	WaitForAnyActiveUpdateToComplete(false);

	if(reset)
	{
		ShutdownExpressions();
	}
	else 
	{
		for(int i=0; i<fwAnimDirectorComponent::kPhaseNum; ++i)
		{
			fwAnimDirectorComponentExpressions* componentExpr = GetComponentByPhase<fwAnimDirectorComponentExpressions>(fwAnimDirectorComponent::ePhase(i));
			if(componentExpr)
			{
				componentExpr->ClearUnused();
			}
		}

		if (m_iExpressionsDictIndex != -1)
		{
			g_ExpressionDictionaryStore.RemoveRefWithoutDelete(strLocalIndex(m_iExpressionsDictIndex), REF_OTHER);
		}
		m_iExpressionsDictIndex = -1;
	}

	crCreature& creature = *GetCreature(); 	// NOTE: GetCreature cannot fail (even though it returns a pointer).
	//	if (creature)
	{
		// TODO - Make this data driven. At the moment the upperbody fixup expression is added to all peds
		if (m_pEntity->GetType() == 4) // ENTITY_TYPE_PED = 4
		{
			// Initialise the creature using the upperbody fixup expression from the default expression dictionary
			static atHashString defaultExpressions("default",0xE4DF46D5);
			strLocalIndex slot = strLocalIndex(g_ExpressionDictionaryStore.FindSlotFromHashKey(defaultExpressions));

			fwExpressionsDictionaryLoader loader(slot.Get(), false);
			if(loader.IsLoaded())
			{
				crFrameDataFactory& frameDataFactory = fwAnimDirectorComponentCreature::GetFrameDataFactory();
				crCommonPool& commonPool = fwAnimDirectorComponentCreature::GetCommonPool();

				// We are not appending the upperbody fixup expressions here
				// We are just using them to add the extra dofs we need to the skeleton
				atHashString upperBodyFixupExpression("upperbody_fixup",0xC2C52064);
				crExpressions *pFixupExpressions = g_ExpressionDictionaryStore.Get(slot)->Lookup(upperBodyFixupExpression);
				if (pFixupExpressions)
				{
					pFixupExpressions->InitializeCreature(creature, &frameDataFactory, &commonPool);
				}
				else
				{
					animAssertf(0, "Failed to find expression : %s in expression dictionary %s\n", upperBodyFixupExpression.TryGetCStr(), defaultExpressions.TryGetCStr());
				}

				atHashString upperBodyShadowExpression("upperbody_shadow",0x4EA05219);
				crExpressions *pShadowExpressions = g_ExpressionDictionaryStore.Get(slot)->Lookup(upperBodyShadowExpression);
				if (pShadowExpressions)
				{
					pShadowExpressions->InitializeCreature(creature, &frameDataFactory, &commonPool);
				}
				else
				{
					animAssertf(0, "Failed to find expression : %s in expression dictionary %s\n", upperBodyFixupExpression.TryGetCStr(), defaultExpressions.TryGetCStr());
				}

				atHashString independentMoverExpression("independent_mover",0x63391E19);
				crExpressions *pIndependentMoverExpressions = g_ExpressionDictionaryStore.Get(slot)->Lookup(independentMoverExpression);
				if (pIndependentMoverExpressions)
				{
					pIndependentMoverExpressions->InitializeCreature(creature, &frameDataFactory, &commonPool);
				}
				else
				{
					animAssertf(0, "Failed to find expression : %s in expression dictionary %s\n", independentMoverExpression.TryGetCStr(), defaultExpressions.TryGetCStr());
				}

				atHashString independentMoverShadowExpression("independent_mover_shadow",0xc8263961);
				crExpressions *pIndependentMoverShadowExpressions = g_ExpressionDictionaryStore.Get(slot)->Lookup(independentMoverShadowExpression);
				if (pIndependentMoverShadowExpressions)
				{
					pIndependentMoverShadowExpressions->InitializeCreature(creature, &frameDataFactory, &commonPool);
				}
				else
				{
					animAssertf(0, "Failed to find expression : %s in expression dictionary %s\n", independentMoverShadowExpression.TryGetCStr(), defaultExpressions.TryGetCStr());
				}

				atHashString rootHeightFixupExpression("rootheight_fixup",0xA9A5431);
				crExpressions *pRootHeightFixupExpression = g_ExpressionDictionaryStore.Get(slot)->Lookup(rootHeightFixupExpression);
				if (pRootHeightFixupExpression)
				{
					pRootHeightFixupExpression->InitializeCreature(creature, &frameDataFactory, &commonPool);
				}
				else
				{
					animAssertf(0, "Failed to find expression : %s in expression dictionary %s\n", rootHeightFixupExpression.TryGetCStr(), defaultExpressions.TryGetCStr());
				}

				atHashString firstPersonCamExpression("FirstPersonCam",0x6c856aeb);
				crExpressions *pFirstPersonCamExpression = g_ExpressionDictionaryStore.Get(slot)->Lookup(firstPersonCamExpression);
				if (pFirstPersonCamExpression)
				{
					pFirstPersonCamExpression->InitializeCreature(creature, &frameDataFactory, &commonPool);
				}
				else
				{
					animAssertf(0, "Failed to find expression : %s in expression dictionary %s\n", firstPersonCamExpression.TryGetCStr(), defaultExpressions.TryGetCStr());
				}
			}
			else
			{
				animAssertf(0, "Expression dictionary : %s has failed to load\n", defaultExpressions.TryGetCStr());
			}
		} // m_pEntity->GetType() == ENTITY_TYPE_PED

		// Is this a ped?  Does it have an expression set?  If so, use those over the archetype's expression dictionary and name.
		bool bIgnoreArchetypeExpressions = false;
		fwExpressionSet* pExprSet = m_pEntity->GetExpressionSet();
		if (pExprSet)
		{
			// We're using the expression set's expressions so we don't care about the archetypes now.
			bIgnoreArchetypeExpressions = true;

			strLocalIndex iExpressionDictionaryIndex = strLocalIndex(g_ExpressionDictionaryStore.FindSlotFromHashKey(pExprSet->GetDictionaryName()));
			Assertf(iExpressionDictionaryIndex != -1, "Could not find expression dictionary slot for %s", pExprSet->GetDictionaryName().GetCStr());

			fwExpressionsDictionaryLoader loader(iExpressionDictionaryIndex.Get(), false);
			if(loader.IsLoaded())
			{
				// Remember the dictionary index for reference counting
				g_ExpressionDictionaryStore.AddRef(iExpressionDictionaryIndex, REF_OTHER);
				m_iExpressionsDictIndex = iExpressionDictionaryIndex.Get();

				crExpressionsDictionary* exprDict = loader.GetDictionary();
				if(!GetEntity().InitExpressions(*exprDict))
				{
					//Go through each expression in the set
					for (int i=0; i<pExprSet->GetExpressions().GetCount(); i++)
					{
						const crExpressions* pExpression = NULL;
						atHashString exprHash = pExprSet->GetExpressions()[i];

						pExpression = loader.GetDictionary()->Lookup(exprHash.GetHash());
						if (!pExpression)
						{
#if __DEV
							char expressionDictionaryName[STORE_NAME_LENGTH];
							if (g_ExpressionDictionaryStore.IsValidSlot(iExpressionDictionaryIndex))
							{
								sprintf(expressionDictionaryName, "dictionary name = %s", g_ExpressionDictionaryStore.GetName(iExpressionDictionaryIndex));
							}
							else
							{
								sprintf(expressionDictionaryName, "dictionary index = %d", iExpressionDictionaryIndex.Get());
							}
							char expressionName[STORE_NAME_LENGTH];
							if (exprHash.TryGetCStr())
							{
								sprintf(expressionName, "expression name = %s", exprHash.TryGetCStr());
							}
							else
							{
								sprintf(expressionName, "expression hash = %d", exprHash.GetHash());
							}
							animAssertf(pExpression, "Failed to find (%s) in (%s)\n", expressionName, expressionDictionaryName);
#else //__DEV
							animAssertf(pExpression, "Failed to find expression hash = (%d) in dictionary index = (%d)\n", iExpressionHash, iExpressionDictionaryIndex);
#endif //__DEV
						}

						if (pExpression)
						{
							bool facial = !stricmp(pExpression->GetName(), "pack:/facial.expr");
							AddExpression(*pExpression, facial);
						}
					}
				}
			}
			else
			{
				animAssertf(m_iExpressionsDictIndex == -1, "Expression dictionary has failed to load %d\n", iExpressionDictionaryIndex.Get());
			}
		}

		// This models expression dictionary should be setup as a dependency
		const fwArchetype* pArchetype = m_pEntity->GetArchetype();
		animAssert(pArchetype);
		if (!bIgnoreArchetypeExpressions && pArchetype)
		{
			strLocalIndex iExpressionDictionaryIndex = strLocalIndex(pArchetype->GetExpressionDictionaryIndex());
			u32 iExpressionHash = pArchetype->GetExpressionHash().GetHash();

			const crExpressions* pExpressions = NULL;
			fwExpressionsDictionaryLoader loader(iExpressionDictionaryIndex.Get(), false);
			if(loader.IsLoaded())
			{
				g_ExpressionDictionaryStore.AddRef(iExpressionDictionaryIndex, REF_OTHER);
				m_iExpressionsDictIndex = iExpressionDictionaryIndex.Get();

				crExpressionsDictionary* exprDict = loader.GetDictionary();
				if(!GetEntity().InitExpressions(*exprDict))
				{
					pExpressions = loader.GetDictionary()->Lookup(iExpressionHash);
					if (!pExpressions)
					{
#if __DEV
						char expressionDictionaryName[STORE_NAME_LENGTH];
						if (g_ExpressionDictionaryStore.IsValidSlot(iExpressionDictionaryIndex))
						{
							sprintf(expressionDictionaryName, "dictionary name = %s", g_ExpressionDictionaryStore.GetName(iExpressionDictionaryIndex));
						}
						else
						{
							sprintf(expressionDictionaryName, "dictionary index = %d", iExpressionDictionaryIndex.Get());
						}
						char expressionName[STORE_NAME_LENGTH];
						if (pArchetype->GetExpressionHash().TryGetCStr())
						{
							sprintf(expressionName, "expression name = %s", pArchetype->GetExpressionHash().TryGetCStr());
						}
						else
						{
							sprintf(expressionName, "expression hash = %d", pArchetype->GetExpressionHash().GetHash());
						}
						animAssertf(pExpressions, "Failed to find (%s) in (%s)\n", expressionName, expressionDictionaryName);
#else //__DEV
						animAssertf(pExpressions, "Failed to find expression hash = (%d) in dictionary index = (%d)\n", iExpressionHash, iExpressionDictionaryIndex);
#endif //__DEV
					}
				}
			}
			else
			{
				animAssertf(m_iExpressionsDictIndex == -1, "Expression dictionary has failed to load %d\n", iExpressionDictionaryIndex.Get());
			}
			if (pExpressions)
			{
				AddExpression(*pExpressions);
			}

		} // if (pArchetype)

	} // if (m_pDynamicEntity->GetCreature())

	if(!reset)
	{
		for(int i=0; i<fwAnimDirectorComponent::kPhaseNum; ++i)
		{
			fwAnimDirectorComponentExpressions* componentExpr = GetComponentByPhase<fwAnimDirectorComponentExpressions>(fwAnimDirectorComponent::ePhase(i));
			if(componentExpr)
			{
				componentExpr->RemoveUnusedExpressions();
			}
		}
	}

	MessageAllComponents(fwAnimDirectorComponent::kMessageCreatureChanged);
}

//////////////////////////////////////////////////////////////////////////

bool fwAnimDirector::AddExpression(const crExpressions& expr, bool midPhase)
{
	if(!expr.GetSignature())
	{
		return false;
	}

	fwAnimDirectorComponent::ePhase phase = midPhase?fwAnimDirectorComponent::kPhaseMidPhysics:fwAnimDirectorComponent::kPhasePreRender;

	fwAnimDirectorComponentExpressions* componentExpressions = GetComponentByPhase<fwAnimDirectorComponentExpressions>(phase);
	if(!componentExpressions)
	{
		if(midPhase)
		{
			fwAnimDirectorComponentFacialRig* componentFacialRig = GetComponent<fwAnimDirectorComponentFacialRig>();
			if(!componentFacialRig)
			{
				InitMidPhysicsMotionTree();	
				componentFacialRig = GetComponent<fwAnimDirectorComponentFacialRig>();
			}
			
			if(componentFacialRig)
			{
				componentExpressions = CreateComponentExpressions(phase, &componentFacialRig->GetExpressionRoot());
			}
		}
		else
		{
			componentExpressions = CreateComponentExpressions(phase);
		}
	}
	
	return componentExpressions && (componentExpressions->HasExpression(expr) || componentExpressions->AppendExpression(expr));
}

//////////////////////////////////////////////////////////////////////////

fwMoveNetworkPlayer* fwAnimDirector::CreateNetworkPlayer(const mvNetworkDef& def)
{
	fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	Assert(componentMove);  // mandatory component? - for now, at least while converting

	return componentMove->CreateNetworkPlayer(def);
}

#if __BANK
//////////////////////////////////////////////////////////////////////////

const char* fwAnimDirector::FindNetworkDefName(const mvNetworkDef* pDef)
{
	if (pDef)
	{
		// Load all the network definitions
		for(int iNetworkDefIndex = 0; iNetworkDefIndex < g_NetworkDefStore.GetSize(); iNetworkDefIndex ++)
		{
			fwNetworkDefLoader loader(iNetworkDefIndex, false);
			if(loader.IsLoaded())
			{
				if (loader.GetDictionary() == pDef)
				{
					return g_NetworkDefStore.GetName(strLocalIndex(iNetworkDefIndex));
				}
			}
		}
	}

	return NULL;
}

#endif //__BANK

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::ShutdownExpressions()
{
	animAssert(m_pEntity);

	crCreatureComponentPhysical* physical = GetCreature()->FindComponent<crCreatureComponentPhysical>();
	if(physical)
	{
		physical->RemoveMotions();
	}

	for(u32 i=0; i < fwAnimDirectorComponent::kPhaseNum; i++)
	{
		fwAnimDirectorComponentExpressions* componentExpressions = GetComponentByPhase<fwAnimDirectorComponentExpressions>(fwAnimDirectorComponent::ePhase(i));
		if(componentExpressions)
		{
			componentExpressions->RemoveAllExpressions();
		}
	}

	if (m_iExpressionsDictIndex != -1)
	{
		g_ExpressionDictionaryStore.RemoveRefWithoutDelete(strLocalIndex(m_iExpressionsDictIndex), REF_OTHER);
	}
	m_iExpressionsDictIndex = -1;
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentMotionTree* fwAnimDirector::CreateComponentMotionTree(fwAnimDirectorComponent::ePhase phase, bool creatureFinalizePose)
{
	fwAnimDirectorComponentMotionTree* componentMotionTree = static_cast<fwAnimDirectorComponentMotionTree*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeMotionTree));
	componentMotionTree->SetPhase(phase);
	componentMotionTree->Init(*this, 
		(m_pEntity->GetFragInst() && m_pEntity->GetFragInst()->IsBoundsPrerenderUpdateEnabled()) ? m_pEntity->GetFragInst() : NULL, 
		creatureFinalizePose);
	AddComponent(*componentMotionTree);

	return componentMotionTree;
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentExpressions* fwAnimDirector::CreateComponentExpressions(fwAnimDirectorComponent::ePhase phase, const crmtObserver* rootObserver)
{
	fwAnimDirectorComponentExpressions* componentExpressions = static_cast<fwAnimDirectorComponentExpressions*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeExpressions));
	componentExpressions->SetPhase(phase);
	componentExpressions->Init(*this, rootObserver);
	AddComponent(*componentExpressions);

	return componentExpressions;
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentSyncedScene* fwAnimDirector::CreateComponentSyncedScene()
{
	WaitForAnyActiveUpdateToComplete(false);

	fwAnimDirectorComponentSyncedScene* componentSyncedScene = static_cast<fwAnimDirectorComponentSyncedScene*>(fwAnimDirectorComponent::Allocate(fwAnimDirectorComponent::kComponentTypeSyncedScene));
	componentSyncedScene->SetPhase(fwAnimDirectorComponent::kPhasePrePhysics);
	componentSyncedScene->Init(*this);
	AddComponent(*componentSyncedScene);

	return componentSyncedScene;
}

////////////////////////////////////////////////////////////////////////////////

// Causes the animdirector to be updated if on screen post camera, even if it 
// was updated in the first pass.
void fwAnimDirector::RequestUpdatePostCamera()
{	
	m_UpdatePostCameraRequested = true;

	// check if we can schedule immediately
	if(fwAnimDirectorComponentMotionTree::IsLockedGlobal())
	{
		fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
		if(componentMotionTree && componentMotionTree->IsComplete())
		{
			StartUpdatePostCamera(fwTimer::GetTimeStep());
		}
	}
}

//////////////////////////////////////////////////////////////////////////

crCreature* fwAnimDirector::GetCreature() const
{
	const fwAnimDirectorComponentCreature* componentCreature = GetComponent<fwAnimDirectorComponentCreature>();
	Assert(componentCreature);  // mandatory component

	return &componentCreature->GetCreature();
}

//////////////////////////////////////////////////////////////////////////

const crFrameData& fwAnimDirector::GetFrameData() const
{
	const fwAnimDirectorComponentCreature* componentCreature = GetComponent<fwAnimDirectorComponentCreature>();
	Assert(componentCreature);  // mandatory component

	return componentCreature->GetFrameData();
}

//////////////////////////////////////////////////////////////////////////

crFrameBuffer& fwAnimDirector::GetFrameBuffer()
{
	fwAnimDirectorComponentCreature* componentCreature = GetComponent<fwAnimDirectorComponentCreature>();
	Assert(componentCreature);  // mandatory component

	return componentCreature->GetFrameBuffer();
}

//////////////////////////////////////////////////////////////////////////

crmtMotionTree& fwAnimDirector::GetPrePhysicsMotionTree()
{
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting

	return componentMotionTree->GetMotionTree();
}

//////////////////////////////////////////////////////////////////////////

crmtMotionTree& fwAnimDirector::GetPreRenderMotionTree()
{
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePreRender);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting

	return componentMotionTree->GetMotionTree();
}

//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::WaitForPrePhysicsUpdateToComplete()
{
#if CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE
	if(CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE)
	{
		fwAnimDirectorComponentMotionTree* componentMotionTreePrePhysics = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
		if(componentMotionTreePrePhysics && componentMotionTreePrePhysics->IsLocked())
		{
			fwAnimDirectorComponentMotionTree::sm_LockedGlobalCallStacks.CollectStack(0);
		}
	}
#endif // CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE

	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting
	componentMotionTree->WaitOnComplete();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::WaitForPrePhysicsStateUpdateToComplete()
{
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePrePhysics);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting
	componentMotionTree->WaitOnComplete(true);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::WaitForPreRenderUpdateToComplete()
{
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePreRender);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting
	componentMotionTree->WaitOnComplete();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::WaitForAllUpdatesToComplete()
{
	fwAnimDirectorComponentMotionTree::WaitOnAllComplete();
}

////////////////////////////////////////////////////////////////////////////////

crmtMotionTree& fwAnimDirector::GetMotionTreeByPhase(fwAnimDirectorComponent::ePhase phase)
{
	Assert(phase != fwAnimDirectorComponent::kPhaseAll);
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(phase);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting
	return componentMotionTree->GetMotionTree();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::WaitForMotionTreeByPhase(fwAnimDirectorComponent::ePhase phase, bool updateOnly)
{
	Assert(phase != fwAnimDirectorComponent::kPhaseAll);
	fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(phase);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting
	componentMotionTree->WaitOnComplete(updateOnly);
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirector::IsMotionTreeLockedByPhase(fwAnimDirectorComponent::ePhase phase) const
{
	const fwAnimDirectorComponentMotionTree* componentMotionTree = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(phase);
	Assert(componentMotionTree);  // mandatory component? - for now, at least while converting
	return componentMotionTree->IsLocked();
}

//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::RunPreDelegateCallback()
{
	if (m_PreDelegateCallbackFn)
		m_PreDelegateCallbackFn(m_pEntity);
}

////////////////////////////////////////////////////////////////////////////////

const bool fwAnimDirector::HasMoveInstance() const
{
	const fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	return componentMove && componentMove->HasMoveInstance();
}
////////////////////////////////////////////////////////////////////////////////

const fwMove& fwAnimDirector::GetMove() const
{
	const fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	Assert(componentMove);  // mandatory component? - for now, at least while converting

	return componentMove->GetMove();
}

////////////////////////////////////////////////////////////////////////////////

fwMove& fwAnimDirector::GetMove()
{
	fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	Assert(componentMove);  // mandatory component? - for now, at least while converting

	return componentMove->GetMove();
}

////////////////////////////////////////////////////////////////////////////////

fwMoveNetworkPlayer* fwAnimDirector::GetNetworkPlayers() const
{ 
	const fwAnimDirectorComponentMove* componentMove = GetComponent<fwAnimDirectorComponentMove>();
	Assert(componentMove);  // mandatory component? - for now, at least while converting

	return componentMove->GetNetworkPlayers();
}

////////////////////////////////////////////////////////////////////////////////

#if __ASSERT
bool fwAnimDirector::IsAnyMotionTreeLocked() const
{	
	for(int i=0; i<fwAnimDirectorComponent::kPhaseNum; ++i)
	{
		const fwAnimDirectorComponentMotionTree* component = GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::ePhase(i));
		if(component && component->IsLocked())
		{
			return true;
		}
	}
	return false;
}
#endif //__ASSERT

//////////////////////////////////////////////////////////////////////////
// Ragdoll frames
//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::PoseRagdollFrameFromCreature(const crCreature& creature)
{
	fwAnimDirectorComponentRagDoll* componentRagDoll = GetComponent<fwAnimDirectorComponentRagDoll>();
	animAssert(componentRagDoll);

	componentRagDoll->PoseFromRagDoll(creature);
}

void fwAnimDirector::PoseSkeletonUsingRagdollFrame(crSkeleton& skeleton, const crClip* pClip, float fPhase)
{
	fwAnimDirectorComponentRagDoll* componentRagDoll = GetComponent<fwAnimDirectorComponentRagDoll>();
	animAssert(componentRagDoll);

	componentRagDoll->PoseSkeletonUsingRagdollFrame(skeleton, pClip, fPhase);
}


//////////////////////////////////////////////////////////////////////////
// Init
//////////////////////////////////////////////////////////////////////////

#if !__FINAL

atBinaryMap< atString, u16 > fwAnimDirector::sm_DOFMap;

void fwAnimDirector::EnumDOFMapFilesCallback(const fiFindData &data, void * /*userArg*/)
{
	atString filename(data.m_Name);
	filename.Lowercase();
	if(filename.EndsWith(".xml"))
	{
		filename.Replace(".xml", "");

		if(ASSET.Exists(filename.c_str(), "xml"))
		{
			sysMemStartTemp();
			parTree *pTree = PARSER.LoadTree(filename, "xml");
			sysMemEndTemp();

			if(animVerifyf(pTree, ""))
			{
				parTreeNode *pRoot = pTree->GetRoot();

				if(animVerifyf(parUtils::StringEquals(pRoot->GetElement().GetName(), "DOFMap"), ""))
				{
					for(parTreeNode *pDOF = pRoot->GetChild(); pDOF; pDOF = pDOF->GetSibling())
					{
						u16 id = static_cast< u16 >(pDOF->GetElement().FindAttributeIntValue("id", 0, false));

						char szName[256];
						pDOF->GetElement().FindAttributeStringValue("name", "", szName, 256, true, false);

						if(id != 0 && szName && szName[0] != '\0')
						{
							sm_DOFMap.SafeInsert(id, atString(szName));
						}
					}
				}
			}

			sysMemStartTemp();
			delete pTree;
			sysMemEndTemp();
		}
	}
}

const char *fwAnimDirector::GetDOFName(u16 boneId)
{
	atString *pName = sm_DOFMap.SafeGet(boneId);
	return pName ? pName->c_str() : NULL;
}

#endif

void fwAnimDirector::InitStatic(u32 initMode)
{
	if (initMode == INIT_AFTER_MAP_LOADED)
	{
		// Load all the network definitions
		for(int iNetworkDefIndex = 0; iNetworkDefIndex < g_NetworkDefStore.GetNumUsedSlots(); iNetworkDefIndex ++)
		{
			fwNetworkDefLoader loader(iNetworkDefIndex, false);
			if(loader.IsLoaded() || loader.Load(true))
			{
				g_NetworkDefStore.AddRef(strLocalIndex(iNetworkDefIndex), REF_OTHER);
			}
			animAssertf(loader.IsValid(), "Network Def '%s' has failed to load\n", g_NetworkDefStore.GetName(strLocalIndex(iNetworkDefIndex)));
		}

		// Load all the posematcher
		for(int iPoseMatcherIndex = 0; iPoseMatcherIndex < g_PoseMatcherStore.GetNumUsedSlots(); iPoseMatcherIndex ++)
		{
			fwPoseMatcherLoader loader(iPoseMatcherIndex, false);
			if(loader.IsLoaded() || loader.Load(true))
			{
				g_PoseMatcherStore.AddRef(strLocalIndex(iPoseMatcherIndex), REF_OTHER);
			}
			animAssertf(loader.IsValid(), "Posematcher '%s' has failed to load\n", g_PoseMatcherStore.GetName(strLocalIndex(iPoseMatcherIndex)));
		}

		// Load the "default" expression dictionary 
		atHashString defaultExpressions("default",0xE4DF46D5);
		strLocalIndex slot = strLocalIndex(g_ExpressionDictionaryStore.FindSlotFromHashKey(defaultExpressions));
		if (slot!=-1)
		{
			fwExpressionsDictionaryLoader loader(slot.Get(), false);
			if(loader.IsLoaded() || loader.Load(true))
			{
				g_ExpressionDictionaryStore.AddRef(slot, REF_OTHER);
			}
			animAssertf(loader.IsValid(), "Expression dictionary : %s has failed to load\n", defaultExpressions.TryGetCStr());
		}
		else
		{
			animAssertf(0, "Expression dictionary : %s has an invalid index\n", defaultExpressions.TryGetCStr());
		}

		// Load all the frame filter dictionaries
		for(int iFrameFilterDictionaryIndex = 0; iFrameFilterDictionaryIndex < g_FrameFilterDictionaryStore.GetNumUsedSlots(); iFrameFilterDictionaryIndex ++)
		{
			fwFrameFilterDictionaryLoader loader(iFrameFilterDictionaryIndex, false);
			if(loader.IsLoaded() || loader.Load(true))
			{
				g_FrameFilterDictionaryStore.AddRef(strLocalIndex(iFrameFilterDictionaryIndex), REF_OTHER);
			}

	#if __BANK
			if(loader.IsLoaded())
			{
				// Load frame filter dictionary map
				atString pathString("assets:/anim/filters/");
				pathString += g_FrameFilterDictionaryStore.GetName(strLocalIndex(iFrameFilterDictionaryIndex));
				pathString += "_filtermap";

				if(ASSET.Exists(pathString.c_str(), "xml"))
				{
					sysMemStartTemp();
					parTree *tree = PARSER.LoadTree(pathString.c_str(), "xml");
					sysMemEndTemp();

					if(tree != NULL)
					{
						parTreeNode* root = tree->GetRoot();

						for(parTreeNode::ChildNodeIterator iter = root->BeginChildren(); iter != root->EndChildren(); ++iter)
						{
							parElement& element = (*iter)->GetElement();

							const parAttribute* nameAttribute = element.FindAttribute("name");

							const char *name = nameAttribute->GetStringValue();

							atHashString temp; temp.SetFromString(name);
						}

						sysMemStartTemp();
						delete tree;
						sysMemEndTemp();
					}
				}
			}
	#endif
			animAssertf(loader.IsValid(), "Frame Filter Dictionary '%s' has failed to load\n", g_FrameFilterDictionaryStore.GetName(strLocalIndex(iFrameFilterDictionaryIndex)));
		}

#if !__FINAL
		{
			USE_DEBUG_MEMORY();

			ASSET.PushFolder("common:/non_final/anim/move/dofmaps");

			sm_DOFMap.Reset();

			ASSET.EnumFiles("common:/non_final/anim/move/dofmaps/", EnumDOFMapFilesCallback, NULL);

			sm_DOFMap.FinishInsertion();

			ASSET.PopFolder();
		}
#endif

		// Initialize synced scene component
		fwAnimDirectorComponentSyncedScene::StaticInit();
	}
}


//////////////////////////////////////////////////////////////////////////
// Shutdown
//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::ShutdownStatic(u32 shutdownMode)
{
	if (shutdownMode == SHUTDOWN_WITH_MAP_LOADED)
	{
		// Shutdown synced scene component
		fwAnimDirectorComponentSyncedScene::StaticShutdown();

		Assert(fwAnimDirectorPooledObject::GetPool()->GetNoOfUsedSpaces() == 0);

		// Unload all the network definitions
		for(int iNetworkDefIndex = 0; iNetworkDefIndex < g_NetworkDefStore.GetSize(); iNetworkDefIndex ++)
		{
			fwNetworkDefLoader loader(iNetworkDefIndex, false);
			if(loader.IsLoaded())
			{
				// Remove the reference that was added when the network def was loaded (and clean up possible stray don't delete flag)
				g_NetworkDefStore.ClearRequiredFlag(iNetworkDefIndex, STRFLAG_DONTDELETE);
				g_NetworkDefStore.RemoveRef(strLocalIndex(iNetworkDefIndex), REF_OTHER);

				Assert(g_NetworkDefStore.GetNumRefs(strLocalIndex(iNetworkDefIndex))==0);
				g_NetworkDefStore.StreamingRemove(strLocalIndex(iNetworkDefIndex));
			}
		}

		// Unload all the posematchers
		for(int iPoseMatcherIndex = 0; iPoseMatcherIndex < g_PoseMatcherStore.GetSize(); iPoseMatcherIndex ++)
		{
			fwPoseMatcherLoader loader(iPoseMatcherIndex, false);
			if(loader.IsLoaded())
			{
				// Remove the reference that was added when the posematchers was loaded (and clean up possible stray don't delete flag)
				g_PoseMatcherStore.ClearRequiredFlag(iPoseMatcherIndex, STRFLAG_DONTDELETE);
				g_PoseMatcherStore.RemoveRef(strLocalIndex(iPoseMatcherIndex), REF_OTHER);
				
				Assert(g_PoseMatcherStore.GetNumRefs(strLocalIndex(iPoseMatcherIndex))==0);
				g_PoseMatcherStore.StreamingRemove(strLocalIndex(iPoseMatcherIndex));
			}
		}

		// Unload the "default" expression dictionary 
		atHashString defaultExpressions("default",0xE4DF46D5);
		strLocalIndex slot = strLocalIndex(g_ExpressionDictionaryStore.FindSlotFromHashKey(defaultExpressions));
		if (slot!=-1)
		{
			fwExpressionsDictionaryLoader loader(slot.Get(), false);
			if(loader.IsLoaded())
			{
				// Remove the reference that was added when the expression dictionary was loaded
				g_ExpressionDictionaryStore.RemoveRef(slot, REF_OTHER);
			}
		}

		// Unload all the frame filter dictionaries
		for(int iFrameFilterDictionaryIndex = 0; iFrameFilterDictionaryIndex < g_FrameFilterDictionaryStore.GetSize(); iFrameFilterDictionaryIndex ++)
		{
			fwFrameFilterDictionaryLoader loader(iFrameFilterDictionaryIndex, false);
			if(loader.IsLoaded())
			{
				// Remove the reference that was added when the frame filter dictionary was loaded (and clean up possible stray don't delete flag)
				g_FrameFilterDictionaryStore.ClearRequiredFlag(iFrameFilterDictionaryIndex, STRFLAG_DONTDELETE);
				g_FrameFilterDictionaryStore.RemoveRef(strLocalIndex(iFrameFilterDictionaryIndex), REF_OTHER);

				Assert(g_FrameFilterDictionaryStore.GetNumRefs(strLocalIndex(iFrameFilterDictionaryIndex))==0);
				g_FrameFilterDictionaryStore.StreamingRemove(strLocalIndex(iFrameFilterDictionaryIndex));
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::GenerateComponentIndices()
{
	memset(&m_ComponentIndices[0], sm_MaxNumComponents, sm_MaxNumComponentIndices);
	for(int i=0; i<m_Components.GetCount(); ++i)
	{
		Assert(m_ComponentIndices[ComponentTypeAndPhaseToIndex(m_Components[i]->GetComponentType(), m_Components[i]->GetPhase())] == sm_MaxNumComponents);
		m_ComponentIndices[ComponentTypeAndPhaseToIndex(m_Components[i]->GetComponentType(), m_Components[i]->GetPhase())] = u8(i);
	}
}

//////////////////////////////////////////////////////////////////////////

fwAnimDirector::Queue::Queue()
: m_Num(0)
, m_State(kStateIdle)
{
	m_Semaphore = sysIpcCreateSema(0);
}

//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::Queue::Inc()
{
	// interlock not required (for now)
	m_Num++;
}

//////////////////////////////////////////////////////////////////////////

void fwAnimDirector::Queue::Push(fwEntity& entity)
{
	AssertVerify(m_Queue.Push(&entity));

	u32 state = sysInterlockedRead(&m_State);
	if(state == kStateWait)
	{
		if(sysInterlockedCompareExchange(&m_State, kStateIdle, kStateWait) == kStateWait)
		{
			sysIpcSignalSema(m_Semaphore);
		}
	}
}

//////////////////////////////////////////////////////////////////////////

fwEntity* fwAnimDirector::Queue::Pop()
{
	u32 num = m_Num;
	if(num > 0)
	{
		fwEntity* entity = m_Queue.Pop();
		while(!entity)
		{
			(void)AssertVerify(sysInterlockedExchange(&m_State, kStateWait) == kStateIdle);

			entity = m_Queue.Pop();
			if(!entity)
			{
				sysIpcWaitSema(m_Semaphore);

				entity = m_Queue.Pop();
			}
			else 
			{
				// queue no longer empty - try and reverse the wait
				if(sysInterlockedCompareExchange(&m_State, kStateIdle, kStateWait) != kStateWait)
				{
					// failed to reverse, must wait on semaphore as a producer has already signaled
					sysIpcWaitSema(m_Semaphore);
				}
			}
		}

		m_Num--;
		return entity;
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

fwEntity* fwAnimDirector::Queue::PopIfAvailable(bool &queueIsEmptyOut)
{
	if(m_Num > 0)
	{
		queueIsEmptyOut = false;

		fwEntity* entity = m_Queue.Pop();
		if(entity)
		{
			m_Num--;
		}
		return entity;
	}
	else
	{
		queueIsEmptyOut = true;
		return NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirector::Queue::Clear()
{
	while(Pop());
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirector::sm_DebugEnableMotionTreeLogging = false;
bool fwAnimDirector::sm_DebugEnablePoseUpdate = true;

#if __DEV
bool fwAnimDirector::sm_ForceLoadAnims = false;
#endif // __DEV

fwAnimDirector::Queue fwAnimDirector::sm_PrePhysicsQueue;

////////////////////////////////////////////////////////////////////////////////

FW_INSTANTIATE_CLASS_POOL_SPILLOVER(fwAnimDirectorPooledObject, ANIMDIRECTOR_POOL_MAX, 0.35f, atHashString("fwAnimDirector",0xca5ed810));

////////////////////////////////////////////////////////////////////////////////

#if __DEV

void fwAnimDirector::DebugDump()	
{
	Printf("fwAnimDirector::DebugDump()\n");
	CClipIterator it(NULL);
	GetMove().GetMotionTree().Iterate(it); 
};

void CClipIterator::Visit(crmtNode& node)
{
	crmtIterator::Visit(node);

	if (node.GetNodeType()==crmtNode::kNodeClip)
	{
		crmtNodeClip* pClipNode = static_cast<crmtNodeClip*>(&node);
		if (pClipNode 
			&& pClipNode->GetClipPlayer().HasClip())
		{
			//get the weight from the parent node (if it exists)
			float weight = CalcVisibleWeight(node);
			//if (weight > 0.0f)
			{
#if __BANK
				const crClip* clip = pClipNode->GetClipPlayer().GetClip();
				/* To display the clip dictionary name we need to find the clip dictionary index in the clip dictionary store */
				const char *clipDictionaryName = NULL;

				if (clip->GetDictionary())
				{
					/* Iterate through the clip dictionaries in the clip dictionary store */
					const crClipDictionary *clipDictionary = NULL;
					int clipDictionaryIndex = 0, clipDictionaryCount = g_ClipDictionaryStore.GetSize();
					for(; clipDictionaryIndex < clipDictionaryCount && clipDictionaryName==NULL; clipDictionaryIndex ++)
					{
						if(g_ClipDictionaryStore.IsValidSlot(strLocalIndex(clipDictionaryIndex)))
						{
							clipDictionary = g_ClipDictionaryStore.Get(strLocalIndex(strLocalIndex(clipDictionaryIndex)));
							if(clipDictionary == clip->GetDictionary())
							{
								clipDictionaryName = g_ClipDictionaryStore.GetName(strLocalIndex(clipDictionaryIndex));
							}
						}
					}
				}
#endif // __BANK

				char outText[256] = "";
				sprintf(outText, "W:%.3f", weight );
				const crClip* pClip = pClipNode->GetClipPlayer().GetClip();
				Assert(pClip);
				atString clipName(pClip->GetName());
				clipName.Replace("pack:/", "");
				clipName.Replace(".clip", "");

#if __BANK
				sprintf(outText, "%s, %16s/%s, T:%.3f", outText, clipDictionaryName ? clipDictionaryName : "UNKNOWN", clipName.c_str(), pClipNode->GetClipPlayer().GetTime());
#else
				sprintf(outText, "%s, %16s, T:%.3f", outText, clipName.c_str(), pClipNode->GetClipPlayer().GetTime());
#endif // __BANK
				if (pClip->GetDuration()>0.0f)
				{
					sprintf(outText, "%s, P:%.3f", outText, pClipNode->GetClipPlayer().GetTime() / pClip->GetDuration());
				}
				Printf("%s\n", outText);
				m_noOfTexts++;
			}
		}
	}
}

float CClipIterator::CalcVisibleWeight(crmtNode& node)
{
	crmtNode * pNode = &node;

	float weight = 1.0f;

	crmtNode* pParent = pNode->GetParent();

	while (pParent)
	{
		float outWeight = 1.0f;

		switch(pParent->GetNodeType())
		{
		case crmtNode::kNodeBlend:
			{
				crmtNodePairWeighted* pPair = static_cast<crmtNodePairWeighted*>(pParent);

				if (pNode==pPair->GetFirstChild())
				{
					outWeight = 1.0f - pPair->GetWeight();
				}
				else
				{
					outWeight = pPair->GetWeight();
				}
			}
			break;
		case crmtNode::kNodeBlendN:
			{
				crmtNodeN* pN = static_cast<crmtNodeN*>(pParent);
				u32 index = pN->GetChildIndex(*pNode);
				outWeight = pN->GetWeight(index);
			}
			break;
		default:
			outWeight = 1.0f;
			break;
		}

		weight*=outWeight;

		pNode = pParent;
		pParent = pNode->GetParent();
	}

	return weight;
}
#endif //__DEV
} // namespace rage
