// 
// fwanimation/directorcomponentmotiontree.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponentmotiontree.h"

#include "animdirector.h"
#include "directorcomponentcreature.h"

#include "move/move_fixedheap.h"
#include "system/nelem.h"


namespace rage
{

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentMotionTree::fwAnimDirectorComponentMotionTree()
: fwAnimDirectorComponent(kComponentTypeMotionTree)
, m_Locked(false)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentMotionTree::~fwAnimDirectorComponentMotionTree()
{
	fwAnimDirectorComponentMotionTree::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentMotionTree, 0xcfaeee7a, kComponentTypeMotionTree, ANIMDIRECTOR_POOL_MAX*2 + ANIMDIRECTOR_POOL_PED_MAX, 0.34f);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::Init(fwAnimDirector& director, fragInst* fragment, bool creatureFinalizePose)
{
	InitBase(director);

	m_MotionTree.Init(*director.GetCreature(), fragment, creatureFinalizePose, GetPhase()==kPhasePreRender);
	m_MotionTree.SetNodeFactory(sm_NodeFactory);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::Shutdown()
{
	m_MotionTree.SetNodeFactory(NULL);

	m_MotionTree.Shutdown();

	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::ReceiveMessage(eMessageId msgId, ePhase phase, void*)
{
	switch(msgId)
	{
	case kMessageCreatureChanged:
	case kMessageSkeletonChanged:
		m_MotionTree.Refresh();
		break;

	case kMessageShuttingDown:
		animAssertf(!IsLocked(), "fwAnimDirectorComponentMotionTree::Shutdown - cannot destroy while locked!");
		//	animAssertf(!IsLockedGlobal(), "fwAnimDirectorComponentMotionTree::Shutdown - cannot destroy while global locked!");
		if(IsLocked())
		{
			WaitOnComplete();
		}

		// if pre-physics, use a free list to batch all mvFixedHeap free requests together
		if(GetPhase() == kPhasePrePhysics)
		{		
			mvFixedHeap::FreeList freeList;
			m_MotionTree.Shutdown();
		}
		else
		{
			m_MotionTree.Shutdown();
		}
		break;

	case kMessageMotionTreePostDelegate:
		{
			fwAnimDirector& director = GetDirector();

			switch(phase)
			{
			case fwAnimDirectorComponent::kPhasePrePhysics:
				{
					if(director.m_bActivePoseFromSkel)
					{
						director.m_pEntity->SetActivePoseFromSkel();

						// reset the flag
						director.m_bActivePoseFromSkel = false;
					}

					// If we called Queue.Inc(), call Queue.Push() now, and reset m_IncrementedPrePhysicsQueue.
					// During post-camera updates, we might get here without wanting to do this.
					if(director.m_IncrementedPrePhysicsQueue)
					{
						fwAnimDirector::sm_PrePhysicsQueue.Push(director.GetEntity());
						director.m_IncrementedPrePhysicsQueue = false;
					}
				}
				// break;  // break omitted

#if __DEV
			case fwAnimDirectorComponent::kPhaseMidPhysics:
			case fwAnimDirectorComponent::kPhasePreRender:
				{
					// debug dump of motion tree
					if(fwAnimDirector::sm_DebugEnableMotionTreeLogging)
					{
						if(g_AnimManager->GetAnimDirectorBeingViewed() == &director)
						{
							m_MotionTree.Dump();
							if(GetPhase() == fwAnimDirectorComponent::kPhasePreRender)
							{
								fwAnimDirector::sm_DebugEnableMotionTreeLogging = false;
							}
						}
					}
				}
#endif //__DEV
				break;

			default:
				break;
			}
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::Schedule(float deltaTime, u8 priority, crmtMotionTree* parent, bool update, bool compose)
{
	// detect double schedule
	Assert(!GetDirector().IsAnyMotionTreeLocked());
	Assert(!m_Locked);
	Assert(sm_LockedGlobal);

	Assert(update || compose);

	m_Locked = true;

	sm_Scheduler->Schedule(m_MotionTree, deltaTime, priority, update?&fwAnimDirectorComponentMotionTree::PreDelegate:NULL, update?&fwAnimDirectorComponentMotionTree::MidDelegate:NULL, fwAnimDirectorComponentMotionTree::PostDelegate, this, update, compose, parent);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::InitDerivedClass()
{
	sm_Scheduler = rage_new crmtMotionTreeScheduler();

	// initialize the node factory
	const int nodePoolPageSizes[] = { 64, 96, 136, 208, 264 };
	const int nodePoolNumPages = (RSG_PS3||RSG_XENON)?92:276;
	sm_NodeFactory = rage_new crmtNodeFactory(nodePoolPageSizes, NELEM(nodePoolPageSizes));
	sm_NodeFactory->CreatePagePool(nodePoolNumPages);

	// initialize PPU animation decompression cache (PS3 only)
#if ENABLE_ANIM_CACHE
	const u32 maxBlockSize = CR_DEFAULT_MAX_BLOCK_SIZE;
	const u32 maxNumBlocks = 4;
	sm_AnimCache.Init(maxBlockSize, maxNumBlocks);
	crAnimCache::SetInstance(sm_AnimCache);
#endif // ENABLE_ANIM_CACHE
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::ShutdownDerivedClass()
{
	delete sm_Scheduler;

	if(sm_NodeFactory)
	{
		sm_NodeFactory->Shutdown();
		delete sm_NodeFactory;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::PreDelegate(void* payload)
{
	fwAnimDirectorComponentMotionTree* component = static_cast<fwAnimDirectorComponentMotionTree*>(payload);
	Assert(component);

	// check locks
	Assert(component->m_Locked);
	Assert(component->sm_LockedGlobal || component->GetPhase() == kPhaseMidPhysics);

	// message components
	fwAnimDirector& director = component->GetDirector();
	if (component->GetPhase() == kPhasePrePhysics)
		director.RunPreDelegateCallback();

	director.MessageAllComponentsByPhase(component->GetPhase(), kMessageMotionTreePreDelegate, NULL);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::MidDelegate(void* payload)
{
	fwAnimDirectorComponentMotionTree* component = static_cast<fwAnimDirectorComponentMotionTree*>(payload);
	Assert(component);

	// check locks
	Assert(component->m_Locked);
	Assert(component->sm_LockedGlobal || component->GetPhase() == kPhaseMidPhysics);

	// message components
	fwAnimDirector& director = component->GetDirector();
	director.MessageAllComponentsByPhase(component->GetPhase(), kMessageMotionTreeMidDelegate, NULL);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentMotionTree::PostDelegate(void* payload)
{
	fwAnimDirectorComponentMotionTree* component = static_cast<fwAnimDirectorComponentMotionTree*>(payload);
	Assert(component);

	// check locks
	Assert(component->m_Locked);
	Assert(component->sm_LockedGlobal || component->GetPhase() == kPhaseMidPhysics);

	// message components
	fwAnimDirector& director = component->GetDirector();
	director.MessageAllComponentsByPhase(component->GetPhase(), kMessageMotionTreePostDelegate, NULL);

	// unlock 
	component->m_Locked = false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentMotionTree::sm_LockedGlobal = false;
int fwAnimDirectorComponentMotionTree::sm_LockedGlobalPhase = kPhaseAll;

#if CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE
sysStackCollector fwAnimDirectorComponentMotionTree::sm_LockedGlobalCallStacks;
#endif // CALLSTACK_ON_BLOCKING_ANIM_PREPHYSICS_UPDATE

crmtMotionTreeScheduler* fwAnimDirectorComponentMotionTree::sm_Scheduler = NULL;

crmtNodeFactory* fwAnimDirectorComponentMotionTree::sm_NodeFactory = NULL;

#if ENABLE_ANIM_CACHE
crAnimCache fwAnimDirectorComponentMotionTree::sm_AnimCache;
#endif // ENABLE_ANIM_CACHE

///////////////////////////////////////////////////////////////////////////////

} // namespace rage




