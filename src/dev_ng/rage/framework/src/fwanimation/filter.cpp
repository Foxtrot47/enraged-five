//
// animation/Filter.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "filter.h"
#include "parser/manager.h"

#include "fwanimation/animhelpers.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "fwscene/stores/framefilterdictionarystore.h"

using namespace rage;

#include "filter_parser.h"

// These are defined in boneids_parser.h
EXTERN_PARSER_ENUM(eAnimBoneTag);

namespace rage {

const char *GetBoneTagName(u16 boneTag)
{
	return PARSER_ENUM(eAnimBoneTag).NameFromValueUnsafe(boneTag);
}

#if !__FINAL

const char *g_FrameFilterDictionaryStoreFolder = "assets:/anim/filters";

void fwFrameFilterDictionaryStore::Save()
{
	fwFrameFilterDictionaryStore ffds;

	int frameFilterDictionaryCount = 0;

	// Iterate through frame filter dictionaries
	for (int frameFilterDictionaryIndex = 0; frameFilterDictionaryIndex < g_FrameFilterDictionaryStore.GetCount(); frameFilterDictionaryIndex ++)
	{
		fwFrameFilterDictionaryLoader loader(frameFilterDictionaryIndex);
		if(loader.IsLoaded())
		{
			// Get frame filter dictionary name
			const char *frameFilterDictionaryName = g_FrameFilterDictionaryStore.GetName(strLocalIndex(frameFilterDictionaryIndex));

			// Get frame filter dictionary
			crFrameFilterDictionary *frameFilterDictionary = loader.GetDictionary();

			if (frameFilterDictionaryName && frameFilterDictionary)
			{
				frameFilterDictionaryCount ++;
			}
		}
	}

	if(frameFilterDictionaryCount > 0)
	{
		ffds.m_FrameFilterDictionaries.ResizeGrow(frameFilterDictionaryCount);

		// Iterate through frame filter dictionaries
		for (int frameFilterDictionaryIndex = 0; frameFilterDictionaryIndex < g_FrameFilterDictionaryStore.GetCount(); frameFilterDictionaryIndex ++)
		{
			// Is the frame filter dictionary loaded?
			if (g_FrameFilterDictionaryStore.HasObjectLoaded(strLocalIndex(frameFilterDictionaryIndex)))
			{
				// Get frame filter dictionary name
				const char *frameFilterDictionaryName = g_FrameFilterDictionaryStore.GetName(strLocalIndex(frameFilterDictionaryIndex));

				// Get frame filter dictionary
				crFrameFilterDictionary *frameFilterDictionary = g_FrameFilterDictionaryStore.Get(strLocalIndex(frameFilterDictionaryIndex));

				if (frameFilterDictionaryName && frameFilterDictionary)
				{
					fwFrameFilterDictionary &ffd = ffds.m_FrameFilterDictionaries[frameFilterDictionaryIndex];

					// Init name
					ffd.m_Name = frameFilterDictionaryName;

					// Init frame filter array
					ffd.m_FrameFilters.ResizeGrow(frameFilterDictionary->GetCount());

					// Iterate through frame filters
					for (int frameFilterIndex = 0; frameFilterIndex < frameFilterDictionary->GetCount(); frameFilterIndex ++)
					{
						// Get frame filter name
						const char *frameFilterName = atHashString(frameFilterDictionary->GetCode(frameFilterIndex)).GetCStr();

						// Get frame filter
						crFrameFilter *frameFilter = frameFilterDictionary->GetEntry(frameFilterIndex);

						if (frameFilterName && frameFilter)
						{
							// Check frame filter is a frame filter multi weight
							crFrameFilterMultiWeight *frameFilterMultiWeight = frameFilter->DynamicCast<crFrameFilterMultiWeight>();
							if (frameFilterMultiWeight)
							{

								fwFrameFilter &ff = ffd.m_FrameFilters[frameFilterIndex];

								// Init name
								ff.m_Name = frameFilterName;

								// Init dof array
								ff.m_TrackIdIndices.ResizeGrow(frameFilterMultiWeight->GetEntryCount());

								// Populate dofs
								for(int i = 0; i < frameFilterMultiWeight->GetEntryCount(); i ++)
								{
									u8 track;
									u16 id;
									int index;
									float weight;

									frameFilterMultiWeight->GetEntry(i, track, id, index, weight);

									fwFrameFilter_TrackIdIndex &trackIdIndex = ff.m_TrackIdIndices[i];

									trackIdIndex.m_Track = track;
									trackIdIndex.m_Id = (eAnimBoneTag)id;
									trackIdIndex.m_WeightIndex = (u8)index;
								}

								// Init weight array
								ff.m_Weights.ResizeGrow(frameFilterMultiWeight->GetNumWeightGroups());

								// Populate weights
								for(int i = 0; i < frameFilterMultiWeight->GetNumWeightGroups(); i ++)
								{
									float weight = frameFilterMultiWeight->GetWeight(i);

									ff.m_Weights[i] = weight;
								}
							}
						}
					}
				}
			}
		}

		ASSET.PushFolder(g_FrameFilterDictionaryStoreFolder);

		// Save frame filter dictionary store
		if (!PARSER.SaveObject("filters", "xml", &ffds))
		{
			Assertf(0, "Failed to save FrameFilterDictionaryStore to %s/%s", g_FrameFilterDictionaryStoreFolder, "filters.xml" );
		}

		ASSET.PopFolder();
	}

	Load();
}

// Load the 
void fwFrameFilterDictionaryStore::Load()
{
	fwFrameFilterDictionaryStore ffds;

	ASSET.PushFolder(g_FrameFilterDictionaryStoreFolder);

	if (PARSER.LoadObject("filters", "xml", ffds))
	{
		// Iterate through frame filter dictionaries
		for (int frameFilterDictionaryIndex = 0; frameFilterDictionaryIndex < ffds.m_FrameFilterDictionaries.GetCount(); frameFilterDictionaryIndex ++)
		{
			fwFrameFilterDictionary &ffd = ffds.m_FrameFilterDictionaries[frameFilterDictionaryIndex];

			crFrameFilterDictionary *frameFilterDictionary = NULL;

			// Get frame filter dictionary
			strLocalIndex frameFilterDictionarySlot = strLocalIndex(g_FrameFilterDictionaryStore.FindSlot(ffd.m_Name));
			if (g_FrameFilterDictionaryStore.IsValidSlot(frameFilterDictionarySlot))
			{
				frameFilterDictionary = g_FrameFilterDictionaryStore.Get(frameFilterDictionarySlot);
			}

			if (!frameFilterDictionary)
			{
				// Create frame filter dictionary
				frameFilterDictionarySlot = g_FrameFilterDictionaryStore.AddSlot(ffd.m_Name);
				if (g_FrameFilterDictionaryStore.IsValidSlot(frameFilterDictionarySlot))
				{
					frameFilterDictionary = g_FrameFilterDictionaryStore.Get(frameFilterDictionarySlot);
				}
			}

			if (frameFilterDictionary)
			{
				// Iterate through frame filters
				for (int frameFilterIndex = 0; frameFilterIndex < ffd.m_FrameFilters.GetCount(); frameFilterIndex ++)
				{
					fwFrameFilter &ff = ffd.m_FrameFilters[frameFilterIndex];
					if (!ff.m_Name==0)
					{
					crFrameFilter* frameFilter = NULL;

					// Get frame filter
					frameFilter = frameFilterDictionary->Lookup(ff.m_Name);
					if (!frameFilter)
					{
						// Create frame filter
						crFrameFilterMultiWeight *frameFilterMultiWeight = new crFrameFilterMultiWeight;
							frameFilterDictionary->AddNewEntry(ff.m_Name.GetCStr(), frameFilterMultiWeight);
						frameFilter = frameFilterDictionary->Lookup(ff.m_Name);
					}

					if (frameFilter)
					{
						// Check frame filter is a frame filter multi weight
						crFrameFilterMultiWeight *frameFilterMultiWeight = frameFilter->DynamicCast<crFrameFilterMultiWeight>();
						if(frameFilterMultiWeight)
						{
								/*bool frameFilterChanged = false;

							// Check array sizes have changed
							frameFilterChanged |= (frameFilterMultiWeight->GetEntryCount() != ff.m_TrackIdIndices.GetCount());
							frameFilterChanged |= (frameFilterMultiWeight->GetNumWeightGroups() != ff.m_Weights.GetCount());

							if(!frameFilterChanged)
							{
								// Check dofs have changed
								for(int dofIndex = 0; !frameFilterChanged && dofIndex < ff.m_TrackIdIndices.GetCount(); dofIndex ++)
								{
									fwFrameFilter_TrackIdIndex &trackIdIndex = ff.m_TrackIdIndices[dofIndex];

									u8 track;
									u16 id;
									int index;
									float weight;
									frameFilterMultiWeight->GetEntry(dofIndex, track, id, index, weight);

									frameFilterChanged |= (track != trackIdIndex.m_Track);
									frameFilterChanged |= (id != trackIdIndex.m_Id);
									frameFilterChanged |= (index != trackIdIndex.m_WeightIndex);
								}
								static bool forceFilterChanged = true;
								if (forceFilterChanged)
								{
									frameFilterChanged = true;
								}
							}

							if(!frameFilterChanged)
							{
								// Check weights have changed
								for(int weightIndex = 0; !frameFilterChanged && weightIndex < ff.m_Weights.GetCount(); weightIndex ++)
								{
									float weight = frameFilterMultiWeight->GetWeight(weightIndex);

									frameFilterChanged |= (!IsClose(weight, ff.m_Weights[weightIndex]));
								}
								}*/

								//if(frameFilterChanged)
								{
									if (ff.m_Weights.GetCount()>0)
							{
								// Reset frame filter
								frameFilterMultiWeight->Shutdown();
								frameFilterMultiWeight->Init(ff.m_Weights.GetCount());

								// Populate dofs
								for(int i = 0; i < ff.m_TrackIdIndices.GetCount(); i ++)
								{
									fwFrameFilter_TrackIdIndex &trackIdIndex = ff.m_TrackIdIndices[i];

									frameFilterMultiWeight->Add(trackIdIndex.m_Track, (u16)trackIdIndex.m_Id, trackIdIndex.m_WeightIndex);
								}

								// Populate weights
								for(int i = 0; i < ff.m_Weights.GetCount(); i ++)
								{
									frameFilterMultiWeight->SetWeight(i, ff.m_Weights[i]);
								}

								// Build path
								atString path(g_FrameFilterDictionaryStoreFolder);
								path += "/";
								path += ffd.m_Name.GetCStr();
								path += "/";
								path += ff.m_Name.GetCStr();
								path += ".";
								path += crFrameFilter::s_FileExtension;

								// Save frame filter
								frameFilterMultiWeight->Save(path.c_str());
							}

								}
						}
						else
						{
							Assertf(false, "FrameFilter is not a FrameFilterMultiWeight!");
						}
					}
					else
					{
						Assertf(false, "Could not find or create FrameFilter!");
					}
				}
					else
					{
						Assertf(false, "FrameFilter has no name!");
					}					
				}
			}
			else
			{
				Assertf(false, "Could not find or create FrameFilterDictionary!");
			}
		}
	}
	else
	{
		Assertf(false, "Failed to load FrameFilterDictionaryStore from %s/%s", g_FrameFilterDictionaryStoreFolder, "filters.xml" );
	}

	ASSET.PopFolder();
}

#endif // !__FINAL

} // namespace rage
