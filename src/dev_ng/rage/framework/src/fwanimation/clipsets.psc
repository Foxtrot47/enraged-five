<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <enumdef type="eAnimPlayerFlag">
    <enumval name="APF_USE_SECONDARY_SLOT" />
    <enumval name="APF_TAG_SYNC_WITH_MOTION_TASK" />
    <enumval name="APF_UNUSED_3" />
    <enumval name="APF_UNUSED_4" />
    <enumval name="APF_ISPLAYING" />
    <enumval name="APF_ISLOOPED" />
    <enumval name="APF_UNUSED_10" />
    <enumval name="APF_UNUSED_11" />
    <enumval name="APF_UNUSED_18" />
    <enumval name="APF_SCRIPT" />
    <enumval name="APF_UNUSED_6" />
    <enumval name="APF_UNUSED_7" />
    <enumval name="APF_UNUSED_8" />
    <enumval name="APF_UNUSED_12" />
    <enumval name="APF_ISBLENDAUTOREMOVE" />
    <enumval name="APF_ISFINISHAUTOREMOVE" />
    <enumval name="APF_UNUSED_17" />
    <enumval name="APF_ADDITIVE" />
    <enumval name="APF_FACIAL" />
    <enumval name="APF_UNUSED_20" />
    <enumval name="APF_UNUSED_19" />
    <enumval name="APF_UPPERBODYONLY" />
    <enumval name="APF_SKIP_NEXT_UPDATE_BLEND" />
    <enumval name="APF_UNUSED_16" />
    <enumval name="APF_UNUSED_15" />
    <enumval name="APF_UNUSED_14" />
    <enumval name="APF_BLOCK_IK" />
    <enumval name="APF_BLOCK_LEG_IK" />
    <enumval name="APF_BLOCK_HEAD_IK" />
    <enumval name="APF_FOLLOW_PED_ROOT_BONE" />
    <enumval name="APF_UNUSED_9" />
    <enumval name="APF_USE_DEFAULT_RCB" />
  </enumdef>

  <enumdef type="eAnimPriority">
    <enumval name="AP_LOW" />
    <enumval name="AP_MEDIUM" />
    <enumval name="AP_HIGH" />
    <enumval name="AP_FACE_LOW" />
    <enumval name="AP_FACE_MEDIUM" />
    <enumval name="AP_FACE_HIGH" />
  </enumdef>

  <enumdef type="eStreamingPolicy">
    <enumval name="SP_STREAMING" />
    <enumval name="SP_SINGLEPLAYER_RESIDENT" />
    <enumval name="SP_MULTIPLAYER_RESIDENT" />
  </enumdef>

  <enumdef type="eStreamingPriority">
    <enumval name="SP_Variation" />
    <enumval name="SP_Low" />
    <enumval name="SP_Medium" />
    <enumval name="SP_High" />
  </enumdef>

  <structdef type="fwClipItem">
  </structdef>

  <structdef type="fwClipItemWithProps" base="fwClipItem">
    <bitset name="m_flags" type="fixed32" values="eAnimPlayerFlag">0</bitset>
    <enum name="m_priority" type="eAnimPriority">AP_LOW</enum>
    <string name="m_boneMask" type="atHashString"></string>
  </structdef>

  <structdef type="fwDirectedClipItemWithProps" base="fwClipItemWithProps">
    <float name="m_direction" min="-3.14159265f" max="3.14159265f" init="0.0f"/>
  </structdef>

  <structdef type="fwClipSet">
    <string name="m_fallbackId" type="atHashString" />
    <string name="m_clipDictionaryName" type="atHashString" />
    <map name="m_clipItems" type="atBinaryMap" key="atHashString">
      <pointer type="fwClipItem" policy="owner" />
    </map>
    <array name="m_moveNetworkFlags" type="atArray">
      <string type="atHashString" />
    </array>
  </structdef>

  <structdef type="fwClipSetWithGetup" base="fwClipSet">
    <string name="m_GetupSet" type="atHashString" />
    <string name="m_DeadBlendOutSet" type="atHashString" />
    <bool name="m_AllowAnimatedDeadFall" init="false" />
    <string name="m_AnimatedDeadFallSet" type="atHashString" />
  </structdef>

  <structdef type="fwClipDictionaryMetadata">
    <bitset name="m_streamingPolicy" type="fixed32" values="eStreamingPolicy" />
    <string name="m_memoryGroup" type="atHashString" />
    <enum name="m_streamingPriority" type="eStreamingPriority" init="SP_Medium" />
  </structdef>

  <structdef type="fwMemoryGroupMetadata">
    <u32 name="m_memoryBudget" init="0"/>
    <u32 name="m_memorySelected" init="0"/>
  </structdef>

  <structdef type="fwMemorySituation::Adjustment">
    <string name="m_MemoryGroup" type="atHashString" />
    <int name="m_Amount" init="0" />
  </structdef>

  <structdef type="fwMemorySituation">
    <array name="m_Adjustments" type="atArray">
      <struct type="fwMemorySituation::Adjustment" />
    </array>
  </structdef>

  <structdef type="fwClipVariationSet">
    <array name="m_clips" type="atArray">
      <string type="atHashString" />
    </array>
  </structdef>
  
  <structdef type="fwClipSetManager">
    <map name="m_clipSets" type="atBinaryMap" key="atHashString">
      <pointer type="fwClipSet" policy="owner" />
    </map>
    <map name="m_clipDictionaryMetadatas" type="atBinaryMap" key="atHashString">
      <struct type="fwClipDictionaryMetadata" />
    </map>
    <map name="m_memoryGroupMetadatas" type="atBinaryMap" key="atHashString">
      <struct type="fwMemoryGroupMetadata" />
    </map>
    <map name="m_memorySituations" type="atBinaryMap" key="atHashString">
      <struct type="fwMemorySituation" />
    </map>
    <map name="m_clipVariationSets" type="atBinaryMap" key="atHashString">
      <pointer type="fwClipVariationSet" policy="owner" />
    </map>
  </structdef>

  <structdef type="fwClipDictionaryBuildMetadata">
    <u32 name="m_sizeBefore" />
    <u32 name="m_sizeAfter" />
  </structdef>

  <structdef type="fwClipRpfBuildMetadata">
    <string name="m_name" type="atHashString" />
    <map name="m_dictionaries" type="atBinaryMap" key="atHashString">
      <struct type="fwClipDictionaryBuildMetadata" />
    </map>
  </structdef>

</ParserSchema>
