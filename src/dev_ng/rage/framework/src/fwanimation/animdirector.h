// 
// fwanimation/animdirector.h
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_ANIMDIRECTOR_H
#define FWANIMATION_ANIMDIRECTOR_H

//motion tree update blocking and warning defines
#define ENABLE_PREPHYSICS_BLOCKING_WARNINGS (0)
#define ENABLE_PRERENDER_BLOCKING_WARNINGS (0)

#include "animdefines.h"
#include "directorcomponent.h"
#include "move.h"

// rage headers
#include "cranimation/animation_config.h"
#include "cranimation/framebuffers.h"
#include "cranimation/framedatafactory.h"
#include "cranimation/framefilters.h"
#include "entity/entity.h"
#include "fwsys/gameskeleton.h"
#include "fwtl/pool.h"
#include "paging/ref.h"
#include "system/ipc.h"
#include "system/lockfreering.h"
#include "vector/quaternion.h"


// rage forward declarations
namespace rage
{

class crExpressions;
class crmtObserver;
class fwAnimDirectorComponentExpressions;
class fwAnimDirectorComponentMotionTree;
class fwAnimDirectorComponentSyncedScene;
class fwEntity;
class mvNetworkDef;
class mvMotionWeb;

// game forward declarations

// ====================== Platform Dependent =========================
#define ANIMDIRECTOR_POOL_MAX ((RSG_PC || RSG_DURANGO || RSG_ORBIS) ? 1024 : 310)
#define ANIMDIRECTOR_POOL_PED_MAX ((RSG_PC || RSG_DURANGO || RSG_ORBIS)?(ANIMDIRECTOR_POOL_MAX / 3): 110)
// ====================== Platform Dependent =========================

#define ANIM_NAMELEN (32) //this is no longer correct - should it be removed?

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation Director 
// Deals with animation system initialization, MoVE and scheduler management,
// higher level animation features and update
class fwAnimDirector
{
public:

	// PURPOSE: Construct an animation director - requires parent dynamic entity
	fwAnimDirector(fwEntity& entity);

	// PURPOSE: Destructor
	virtual ~fwAnimDirector();

	// PURPOSE: Initialization
	void Init(bool withRagDollComponent, bool withFacialRigComponent);

	// PURPOSE: Shutdown
	void Shutdown();

public:

	typedef Functor1<fwEntity*> DelegateCallbackFn;

	// PURPOSE: Pre update function
	void PreUpdate(float deltaTime);

	// PURPOSE: Update pre-physics. 
	// move state update, mover update, move compose (updates local matrices)
	void StartUpdatePrePhysics(float deltaTime, bool ignoreVisibilityTest = false, fwAnimDirector* parent = NULL);

	// PURPOSE: End update pre-physics
	void EndUpdatePrePhysics(float deltaTime);

	// PURPOSE: Update post camera. 
	// move compose (updates local matrices)
	void StartUpdatePostCamera(float deltaTime);

	inline bool RequiresUpdatePostCamera() { return !m_Updated || !m_PoseUpdated || m_UpdatePostCameraRequested; }

	inline bool HasBeenUpdatedThisFrame() { return m_Updated || m_PoseUpdated; }

	// PURPOSE: Update mid-physics
	// updates facial
	void StartUpdateMidPhysics(float deltaTime);

	// PURPOSE: Update pre-render
	// updates expressions, ik solvers, skeleton update (for certain entities)
	void StartUpdatePreRender(float deltaTime, crmtMotionTree* parent=NULL, bool copyLastBoundMatrices = true);

	// PURPOSE: Force update of velocities 
	void UpdateVelocityAndAngularVelocity(float deltaTime);

	// PURPOSE: Initialize class
	static void InitClass();

	// PURPOSE: Shutdown class
	static void ShutdownClass();

	// PURPOSE: Update class
	static void UpdateClass();

	// PURPOSE: Get Move interface
	const fwMove& GetMove() const;
	fwMove& GetMove();

	const bool HasMoveInstance() const;

	// PURPOSE: Has the pre-physics update been run this frame
	bool IsUpdated() const;

	// PURPOSE: Have the local matrices have been updated
	bool IsPosed() const;

	// PURPOSE: Set whether or not the local matrices have been updated
	void SetPosed(bool posed);

	// PURPOSE: Wait for the pre-physics update of this director to complete
	void WaitForPrePhysicsUpdateToComplete();

	// PURPOSE: Wait for the pre-physics state update of this director to complete
	void WaitForPrePhysicsStateUpdateToComplete();

	// PURPOSE: Wait for pre-render update of this director to complete
	void WaitForPreRenderUpdateToComplete();

	// PURPOSE: Wait for all motion trees to complete (any phase)
	void WaitForAnyActiveUpdateToComplete(bool excludeMidPhysics=true);

	// PURPOSE: Causes the animdirector to be updated if on screen post camera, even if it 
	// was updated in the first pass. Should not be called while the main anim update is happening.
	void RequestUpdatePostCamera();

	// PURPOSE: Search the output buffers of all the MoVE networks running on this entity
	//			and return the first property who's key matches the provided hash
	const crProperty* FindPropertyInMoveNetworks(u32 uPropertyKey);

	// PURPOSE: Search the output buffers of all the MoVE networks running on this entity
	//			and return the first script visible property matching the provided hash
	const crProperty* FindScriptVisiblePropertyInMoveNetworks(u32 propertyKey);

	// PURPOSE: Search the provided phase reange in the clip for a script visible property with the specified event name
	const crProperty* FindScriptVisiblePropertyInPhaseRange( const crClip* pClip, float startPhase, float endPhase, u32 uPropertyKey);

	// PURPOSE: Type definitions used by callbacks in ForAllComponent calls
	typedef bool (*ConstComponentCallback)(const fwAnimDirectorComponent& component, void* data);
	typedef bool (*ComponentCallback)(fwAnimDirectorComponent& component, void* data);

	// PURPOSE: Iterate all the animation director components
	bool ForAllComponents(ConstComponentCallback cb, void* data) const;
	bool ForAllComponents(ComponentCallback cb, void* data);

	// PURPOSE: Message all the animation director components
	void MessageAllComponents(fwAnimDirectorComponent::eMessageId msgId, void* payload=NULL);

	// PURPOSE: Message all the animation director components (for selected phase only)
	void MessageAllComponentsByPhase(fwAnimDirectorComponent::ePhase phase, fwAnimDirectorComponent::eMessageId msgId, void* payload=NULL);

	// PURPOSE: Run any callbacks assigned to pre delegate phase of this update
	void RunPreDelegateCallback();

	// PURPOSE: Get animation director component by component type
	// PARAMS: componentType - type of component (see fwAnimDirectorComponent::eComponentType for list)
	// NOTES: Can return NULL if type not found
	const fwAnimDirectorComponent* GetComponentByType(fwAnimDirectorComponent::eComponentType componentType) const;
	fwAnimDirectorComponent* GetComponentByType(fwAnimDirectorComponent::eComponentType componentType);

	// PURPOSE: Get animation director component by component type and phase
	// PARAMS: componentType - type of component (see fwAnimDirectorComponent::eComponentType for list)
	// phase - phase of component to to retrieve
	// NOTES: Can return NULL if type not found
	const fwAnimDirectorComponent* GetComponentByTypeAndPhase(fwAnimDirectorComponent::eComponentType componentType, fwAnimDirectorComponent::ePhase phase=fwAnimDirectorComponent::kPhaseAll) const;
	fwAnimDirectorComponent* GetComponentByTypeAndPhase(fwAnimDirectorComponent::eComponentType componentType, fwAnimDirectorComponent::ePhase phase=fwAnimDirectorComponent::kPhaseAll);

	// PURPOSE: Get animation director component by with template casting
	// PARAMS: _ComponentType - derived type of component
	template<typename _ComponentType> const _ComponentType* GetComponent() const;
	template<typename _ComponentType> _ComponentType* GetComponent();

	// PURPOSE: Get animation director component by phase with template casting
	// PARAMS: _ComponentType - derived type of component
	// phase - phase of component to to retrieve
	template<typename _ComponentType> const _ComponentType* GetComponentByPhase(fwAnimDirectorComponent::ePhase phase=fwAnimDirectorComponent::kPhaseAll) const;
	template<typename _ComponentType> _ComponentType* GetComponentByPhase(fwAnimDirectorComponent::ePhase phase=fwAnimDirectorComponent::kPhaseAll);

	// PURPOSE: Finds the total number of components of a particular type
	// PARAMS: componentType - type of component (see fwAnimDirectorComponent::eComponentType for list)
	u32 GetNumComponentsByType(fwAnimDirectorComponent::eComponentType componentType) const;

	// PURPOSE: Gets the total number of components attached to the director
	u32 GetNumComponents() const;

	// PURPOSE: Gets a component by index
	// PARAMS: idx - index of component (see GetNumComponents for count)
	// NOTE: *** Prefer ForAllComponents over enumerating the component list by index ***
	const fwAnimDirectorComponent* GetComponentByIndex(u32) const;
	fwAnimDirectorComponent* GetComponentByIndex(u32);

	// PURPOSE: Add new animation director component
	// NOTES: Automatically assumes responsibility for destruction of component later on
	void AddComponent(fwAnimDirectorComponent& component);

	// PURPOSE: Remove animation director component
	// PARAM: component to remove (WARNING - component passed in will be *destroyed* by this call)
	// NOTES: Will impact index values used in GetComponent calls
	bool RemoveComponent(fwAnimDirectorComponent& component);

	// PURPOSE: Remove animation director components by type
	// PARAMS: componentType - type of component (see fwAnimDirectorComponent::eComponentType for list)
	// NOTES: Will impact index values used in GetComponent calls
	bool RemoveAllComponentsByType(fwAnimDirectorComponent::eComponentType componentType);

	// PURPOSE: Remove all components
	void RemoveAllComponents();


	// PUPOSE: Allocate and initialize motion tree component on demand
	fwAnimDirectorComponentMotionTree* CreateComponentMotionTree(fwAnimDirectorComponent::ePhase phase, bool creatureFinalizePose=true);

	// PUPOSE: Allocate and initialize expression component on demand
	fwAnimDirectorComponentExpressions* CreateComponentExpressions(fwAnimDirectorComponent::ePhase phase, const crmtObserver* rootObserver=NULL);

	// PUPOSE: Allocate and initialize synchronized scene component on demand
	fwAnimDirectorComponentSyncedScene* CreateComponentSyncedScene();

#if __DEV
	static void SetForceLoadAnims(bool enableForceLoad)							{ sm_ForceLoadAnims = enableForceLoad; }
	void DebugDump();
#endif //__DEV

#if __ASSERT
	u32 GetLastDeleteUnusedNetworkPlayersFrame() const { return m_uLastDeleteUnusedNetworkPlayersFrame; }
#endif // __ASSERT

	// PURPOSE: Wait for update of all directors to complete move state update
	static void WaitForAllUpdatePrePhysicsStateToComplete();

	// PURPOSE: Wait for update of all directors to complete move state update and compose
	static void WaitForAllUpdatesToComplete();

	// PURPOSE: Reads the output parameters from all subnetworks into their buffers for consumption by the AI system next frame
	void SwapMoveOutputBuffers();

	// Set each frame to allow NM to grab the results from the move blender to pose the skeleton
	void SetUseCurrentSkeletonForActivePose(bool bUseCurrentSkeleton) { m_bActivePoseFromSkel = bUseCurrentSkeleton; }

	fwMoveNetworkPlayer* GetNetworkPlayers() const;

	const fwEntity& GetEntity() const { animAssert(m_pEntity); return *m_pEntity; }
	fwEntity& GetEntity() { animAssert(m_pEntity); return *m_pEntity; }
	
	
	// TODO --- CONVERT TO RETURN REFERENCE
	// CREATURE ALWAYS PRESENT WITH DIRECTOR COMPONENTS
	crCreature* GetCreature() const;

	// PURPOSE:
	const crFrameData& GetFrameData() const;

	// PURPOSE:
	crFrameBuffer& GetFrameBuffer();

	// PURPOSE:
	crmtMotionTree& GetPrePhysicsMotionTree();

	// PURPOSE:
	crmtMotionTree& GetPreRenderMotionTree();

	// PURPOSE: Get motion tree by component phase
	crmtMotionTree& GetMotionTreeByPhase(fwAnimDirectorComponent::ePhase phase);

	// PURPOSE: Wait for motion tree by component phase
	void WaitForMotionTreeByPhase(fwAnimDirectorComponent::ePhase phase, bool updateOnly=false);

	// PURPOSE: Wait for motion tree by component phase
	bool IsMotionTreeLockedByPhase(fwAnimDirectorComponent::ePhase phase) const;

#if __ASSERT
	// PURPOSE: Check if any motion tree is locked
	bool IsAnyMotionTreeLocked() const;
#endif //__ASSERT

protected:

	// PURPOSE: 
	void UpdateMove(float deltaTime, bool updateAll=false, fwAnimDirector* parent=NULL);

	// PURPOSE:
	void UpdateAudioAndTriggeredFacialEvents();

	// PURPOSE:
	void UpdateNetworkAudioEvents(fwMoveNetworkInterface *pNetwork);

public:

	// PURPOSE: Creates a new move network player to be inserted into another network.
	fwMoveNetworkPlayer* CreateNetworkPlayer(const mvNetworkDef& def);

#if __BANK

	// PURPOSE: Search the network def store for the given def and get its name
	//			from the static string table.
	static const char * FindNetworkDefName(const mvNetworkDef* pDef);
#endif //__BANK


	//////////////////////////////////////////////////////////////////////////
	//	Static methods for loading assets in the MoVE runtime
	//////////////////////////////////////////////////////////////////////////
	
	// PURPOSE: 
	static crFrameFilterMultiWeight* RetrieveFilter(u32 filterDict, u32 filter, const rage::mvMotionWeb *network);

	// PURPOSE: 
	static mvTransitionFrameFilter* RetrieveTransitionFilter(u32 filterDict, u32 filter, const rage::mvMotionWeb *network);

	// PURPOSE: Clip loading function to allow the MoVE runtime to retrieve clips from in game asset stores
	static const crClip *RetrieveClip(u32 clipContext, u32 clipParamA, u32 clipParamB, const rage::mvMotionWeb *network);
	
	// PURPOSE: Expression loading function to allow the MoVE runtime to retrieve expression from in game asset stores
	static const crExpressions* RetrieveExpression(u32 expressionDict, u32 expression, const mvMotionWeb *network);

	// PURPOSE: Network def loading function to allow the MoVE runtime to retrieve network defs from in game asset store
	static const mvNetworkDef* RetrieveNetworkDef(u32 networkDefHash, const rage::mvMotionWeb* requestingNetwork);

	static void LoadNetworkDefinitions(const char *filename);

	static void UnloadNetworkDefinitions(const char *filename);

	//////////////////////////////////////////////////////////////////////////
	// Ragdoll frame
	//////////////////////////////////////////////////////////////////////////

	// Just use the current creature pose as anim, and optionally blend that frame out again.
	// Create an anim frame from a ragdoll pose.  When the ped switches from ragdoll physics to
	// anim the recorded anim frame will be used to help smoothly blend from the ragdoll pose.
	void PoseRagdollFrameFromCreature(const crCreature& creature);

	// want to pose a given skeleton (usually a temporary skeleton)
	// with the first frame of a a specific anim
	// using the ragdoll frame as an intermediary
	void PoseSkeletonUsingRagdollFrame(crSkeleton& skeleton, const crClip* pClip, float fPhase=0.0f);

	// PURPOSE:
	void InitExpressions(bool reset=true);

public:
	// Static interface for streaming and getting network definitions

	// PURPOSE:	Requests a network definition from the streaming system
	// RETURNS: TRUE if the networkdef is already in memory
	static bool RequestNetworkDef( const fwMvNetworkDefId &networkDefId );

	// PURPOSE:	Forces the load of a network definition from the streaming system
	static void ForceLoadNetworkDef( const fwMvNetworkDefId &networkDefId );

	// PURPOSE: Returns true if the requested network definition is in memory
	static bool IsNetworkDefLoaded( const fwMvNetworkDefId &networkDefId );

	// PURPOSE: Get a reference to a loaded network definition (remember to check the network def is in memory before getting)
	static const mvNetworkDef& GetNetworkDef( const fwMvNetworkDefId &networkDefId );


	// PURPOSE: Init the anim director
	// Loads the move network definitions into the store
	// Loads the posematchers into the store
	static void InitStatic(u32 initMode = INIT_AFTER_MAP_LOADED);

	// PURPOSE: 
	static void ShutdownStatic(u32 shutdownMode = SHUTDOWN_SESSION);

#if !__FINAL
	static const char *GetDOFName(u16 boneId);
#endif

private:

	// PURPOSE: Set up post physics motion tree
	void InitPrePhysicsMotionTree();

	// PURPOSE: Set up post physics motion tree
	void InitMidPhysicsMotionTree();

	// PURPOSE: Set up post physics motion tree
	void InitPreRenderMotionTree();

	// PURPOSE:
	void ShutdownExpressions();

public:

	// PURPOSE: 
	bool AddExpression(const crExpressions& expr, bool midPhase=false);

private:

	// PURPOSE:
	void GenerateComponentIndices();

	// PURPOSE:
	static u32 ComponentTypeAndPhaseToIndex(fwAnimDirectorComponent::eComponentType componentType, fwAnimDirectorComponent::ePhase phase);

protected:
	// 
	fwEntity* m_pEntity;

	// true if we want natural motion to use to use the current skeleton this frame
	bool m_bActivePoseFromSkel;

	// Expression dictionary index 
	s32 m_iExpressionsDictIndex;

	// PURPOSE:	True if we have called Queue.Inc(), but not Queue.Push() yet.
	bool m_IncrementedPrePhysicsQueue;

	// 
	bool m_PoseUpdated;

	// 
	bool m_Updated;

	//
	DelegateCallbackFn m_PreDelegateCallbackFn;

	//
	bool m_UpdatePostCameraRequested;

	//
	bool m_bInitialised;

	static const u32 sm_MaxNumComponents = 12;
	atFixedArray<fwAnimDirectorComponent*, sm_MaxNumComponents> m_Components;
	atFixedArray<fwAnimDirectorComponent::ePhase, sm_MaxNumComponents> m_ComponentPhases;

	static const u32 sm_MaxNumComponentIndices = (fwAnimDirectorComponent::kComponentTypeNum-1)*(fwAnimDirectorComponent::kPhaseNum+1);
	atRangeArray<u8, sm_MaxNumComponentIndices> m_ComponentIndices;

	static const fwMvPropertyId ms_VisibleToScriptKey;
	static u32 ms_EventKey;

#if __DEV
	// Stream animations immediately on retrieval
	static bool sm_ForceLoadAnims;
#endif // __DEV

#if __ASSERT
	u32 m_uLastDeleteUnusedNetworkPlayersFrame; // The frame on which we last called DeleteUnusedNetworkPlayers() on the move component.
#endif // __ASSERT

#if !__FINAL
	static void EnumDOFMapFilesCallback(const fiFindData &data, void * /*userArg*/);
	static atBinaryMap< atString, u16 > sm_DOFMap;
#endif

public:

	// PURPOSE: Single consumer, multiple producer threads
	// Lock less FIFO with wait on semaphore for temporary queue exhaustion
	struct Queue
	{
		// PURPOSE: Constructor
		Queue();

		// PURPOSE: Increment number of pending entities
		// NOTE: *** ONLY CALL FROM SINGLE CONSUMER THREAD ***
		void Inc();

		// PURPOSE: Push entity into queue
		// NOTE: *** ONLY CALL PRODUCER THREAD(S) ***
		void Push(fwEntity&);

		// PURPOSE: Pop entity from queue (will wait if queue temporarily exhausted) 
		// RETURNS: pointer to entity, or NULL if no more entities left to process
		// NOTE: *** ONLY CALL FROM SINGLE CONSUMER THREAD ***
		fwEntity* Pop();

		// PURPOSE:	Pop entity from queue, if one is available
		// PARAMS:	queueEmptyOut - if the queue is truly empty true this will be set to true, if nothing is available yet, it will be false
		// RETURNS:	pointer to entity, or NULL if the queue is empty or no entity is ready to be popped yet
		// NOTE: *** ONLY CALL FROM SINGLE CONSUMER THREAD ***
		fwEntity* PopIfAvailable(bool& queueIsEmptyOut);

		// PURPOSE: Clear all entries in the queue
		// NOTE: *** ONLY CALL FROM SINGLE CONSUMER THREAD ***
		void Clear();

	private:

		u32 m_Num;
		u32 m_State;
		sysLockFreeRing<fwEntity,512> m_Queue;
		sysIpcSema m_Semaphore;

		static const u32 kStateIdle = 0;
		static const u32 kStateWait = 1;
	};

	// PURPOSE: Sets a callback that will be called during the PrePhysics phase's PreDelegate step of the motion tree update
	void SetMotionTreePreDelegateCallback(DelegateCallbackFn callback)
	{
		m_PreDelegateCallbackFn = callback;
	}

	void ClearMotionTreePreDelegateCallback()
	{
		m_PreDelegateCallbackFn = DelegateCallbackFn::NullFunctor();
	}

	static Queue& GetPrePhysicsQueue() { return sm_PrePhysicsQueue; }

private:
	static Queue sm_PrePhysicsQueue;

public:

	// Debug functionality - toggle motion tree debugging on or off
	static bool sm_DebugEnableMotionTreeLogging;

	// Debug functionality - if true allow the skeleton to be animated
	// mover is always animated
	static bool sm_DebugEnablePoseUpdate;

	friend class fwAnimDirectorComponentMotionTree;
};

////////////////////////////////////////////////////////////////////////////////

class fwAnimDirectorPooledObject : public fwAnimDirector
{
public:

	fwAnimDirectorPooledObject(fwEntity& dynamicEntity)
		: fwAnimDirector(dynamicEntity)
	{
	}

	FW_REGISTER_CLASS_POOL(fwAnimDirectorPooledObject);
};

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirector::IsUpdated() const
{
	return m_Updated;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirector::IsPosed() const
{
	return m_PoseUpdated;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirector::ForAllComponents(ConstComponentCallback cb, void* data) const
{
	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		Assert(m_Components[i]);
		if(!cb(*m_Components[i], data))
		{
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirector::ForAllComponents(ComponentCallback cb, void* data)
{	
	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		Assert(m_Components[i]);
		if(!cb(*m_Components[i], data))
		{
			return false;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirector::MessageAllComponents(fwAnimDirectorComponent::eMessageId msgId, void* payload)
{
	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		Assert(m_Components[i]);
		m_Components[i]->ReceiveMessage(msgId, fwAnimDirectorComponent::kPhaseAll, payload);
	}
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirector::MessageAllComponentsByPhase(fwAnimDirectorComponent::ePhase phase, fwAnimDirectorComponent::eMessageId msgId, void* payload)
{
	if(phase == fwAnimDirectorComponent::kPhaseAll)
	{
		MessageAllComponents(msgId, payload);
	}
	else
	{
		const u32 numComponents = m_Components.GetCount();
		for(u32 i=0; i<numComponents; ++i)
		{
			Assert(m_Components[i]);
			const fwAnimDirectorComponent::ePhase componentPhase = m_ComponentPhases[i];
			if(componentPhase == phase || componentPhase == fwAnimDirectorComponent::kPhaseAll)
			{
				m_Components[i]->ReceiveMessage(msgId, phase, payload);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

inline const fwAnimDirectorComponent* fwAnimDirector::GetComponentByType(fwAnimDirectorComponent::eComponentType componentType) const
{
	for(int i=-1; i<fwAnimDirectorComponent::kPhaseNum; ++i)
	{
		u8 idx = m_ComponentIndices[ComponentTypeAndPhaseToIndex(componentType, fwAnimDirectorComponent::ePhase(i))];
		if(idx != sm_MaxNumComponents)
		{
			Assert(m_Components[idx] && m_Components[idx]->GetComponentType() == componentType);
			return m_Components[idx];
		}
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent* fwAnimDirector::GetComponentByType(fwAnimDirectorComponent::eComponentType componentType)
{
	for(int i=-1; i<fwAnimDirectorComponent::kPhaseNum; ++i)
	{
		u8 idx = m_ComponentIndices[ComponentTypeAndPhaseToIndex(componentType, fwAnimDirectorComponent::ePhase(i))];
		if(idx != sm_MaxNumComponents)
		{
			Assert(m_Components[idx] && m_Components[idx]->GetComponentType() == componentType);
			return m_Components[idx];
		}
	}
	return NULL;
}


////////////////////////////////////////////////////////////////////////////////

inline const fwAnimDirectorComponent* fwAnimDirector::GetComponentByTypeAndPhase(fwAnimDirectorComponent::eComponentType componentType, fwAnimDirectorComponent::ePhase phase) const
{
	u8 idx = m_ComponentIndices[ComponentTypeAndPhaseToIndex(componentType, phase)];
	if(idx != sm_MaxNumComponents)
	{
		Assert(m_Components[idx] && m_Components[idx]->GetComponentType() == componentType && m_Components[idx]->GetPhase() == phase);
		return m_Components[idx];
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent* fwAnimDirector::GetComponentByTypeAndPhase(fwAnimDirectorComponent::eComponentType componentType, fwAnimDirectorComponent::ePhase phase)
{
	u8 idx = m_ComponentIndices[ComponentTypeAndPhaseToIndex(componentType, phase)];
	if(idx != sm_MaxNumComponents)
	{
		Assert(m_Components[idx] && m_Components[idx]->GetComponentType() == componentType && m_Components[idx]->GetPhase() == phase);
		return m_Components[idx];
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _ComponentType> inline const _ComponentType* fwAnimDirector::GetComponent() const
{
	return static_cast<const _ComponentType*>(GetComponentByType(_ComponentType::sm_ComponentTypeInfo.GetComponentType()));
}

////////////////////////////////////////////////////////////////////////////////

template<typename _ComponentType> inline _ComponentType* fwAnimDirector::GetComponent()
{
	return static_cast<_ComponentType*>(GetComponentByType(_ComponentType::sm_ComponentTypeInfo.GetComponentType()));
}

////////////////////////////////////////////////////////////////////////////////

template<typename _ComponentType> inline const _ComponentType* fwAnimDirector::GetComponentByPhase(fwAnimDirectorComponent::ePhase phase) const
{
	return static_cast<const _ComponentType*>(GetComponentByTypeAndPhase(_ComponentType::sm_ComponentTypeInfo.GetComponentType(), phase));
}

////////////////////////////////////////////////////////////////////////////////

template<typename _ComponentType> inline _ComponentType* fwAnimDirector::GetComponentByPhase(fwAnimDirectorComponent::ePhase phase)
{
	return static_cast<_ComponentType*>(GetComponentByTypeAndPhase(_ComponentType::sm_ComponentTypeInfo.GetComponentType(), phase));
}

////////////////////////////////////////////////////////////////////////////////

inline u32 fwAnimDirector::GetNumComponentsByType(fwAnimDirectorComponent::eComponentType componentType) const
{
	const u32 numComponents = m_Components.GetCount();
	u32 n=0;
	for(u32 i=0; i<numComponents; ++i)
	{
		if(m_Components[i]->GetComponentType() == componentType)
		{
			++n;
		}
	}
	return n;
}

////////////////////////////////////////////////////////////////////////////////

inline u32 fwAnimDirector::GetNumComponents() const
{
	return m_Components.GetCount();
}

////////////////////////////////////////////////////////////////////////////////

inline const fwAnimDirectorComponent* fwAnimDirector::GetComponentByIndex(u32 idx) const
{
	return m_Components[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline fwAnimDirectorComponent* fwAnimDirector::GetComponentByIndex(u32 idx)
{
	return m_Components[idx];
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirector::AddComponent(fwAnimDirectorComponent& component)
{
	Assert(!IsAnyMotionTreeLocked());
	u32 priority = component.GetPriority();

	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		if(m_Components[i]->GetPriority() < priority)
		{
			m_Components.Insert(i) = &component;
			m_ComponentPhases.Insert(i) = component.GetPhase();

			GenerateComponentIndices();
			return;
		}
	}

	m_Components.Append() = &component;
	m_ComponentPhases.Append() = component.GetPhase();

	Assert(m_ComponentIndices[ComponentTypeAndPhaseToIndex(component.GetComponentType(), component.GetPhase())] == sm_MaxNumComponents);
	m_ComponentIndices[ComponentTypeAndPhaseToIndex(component.GetComponentType(), component.GetPhase())] = u8(m_Components.GetCount()-1);
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirector::RemoveComponent(fwAnimDirectorComponent& component)
{
	WaitForAnyActiveUpdateToComplete();

	const u32 numComponents = m_Components.GetCount();
	for(u32 i=0; i<numComponents; ++i)
	{
		if(m_Components[i] == &component)
		{
			delete m_Components[i];
			m_Components.Delete(i);
			m_ComponentPhases.Delete(i);

			GenerateComponentIndices();

			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

inline bool fwAnimDirector::RemoveAllComponentsByType(fwAnimDirectorComponent::eComponentType componentType)
{
	WaitForAnyActiveUpdateToComplete();

	u32 n=0;
	for(int i=m_Components.GetCount()-1; i>=0; --i)
	{
		if(m_Components[i]->GetComponentType() == componentType)
		{
			delete m_Components[i];
			m_Components.Delete(i);
			m_ComponentPhases.Delete(i);

			GenerateComponentIndices();

			n++;
		}
	}
	return n>0;
}

////////////////////////////////////////////////////////////////////////////////

inline void fwAnimDirector::RemoveAllComponents()
{
	WaitForAnyActiveUpdateToComplete();

	const u32 numComponents = m_Components.GetCount();
	for(int i=numComponents-1; i>=0; --i)
	{
		delete m_Components[i];
	}
	m_Components.Reset();
	m_ComponentPhases.Reset();

	GenerateComponentIndices();
}

////////////////////////////////////////////////////////////////////////////////

__forceinline u32 fwAnimDirector::ComponentTypeAndPhaseToIndex(fwAnimDirectorComponent::eComponentType componentType, fwAnimDirectorComponent::ePhase phase)
{
	return u32(int(componentType)-1)*u32(fwAnimDirectorComponent::kPhaseNum+1) + u32(int(phase)+1);
}

////////////////////////////////////////////////////////////////////////////////

#if __DEV
class CClipIterator : public crmtIterator
{	
public:
	CClipIterator(crmtNode* referenceNode = NULL)
	{ 
		(void)referenceNode;
		m_noOfTexts = 0;
	};

	~CClipIterator() {};

	// PURPOSE: Iterator visit override
	virtual void Visit(crmtNode&);

protected:

	float CalcVisibleWeight(crmtNode& node);

	int m_noOfTexts;
};
#endif //__DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_ANIMDIRECTOR_H

