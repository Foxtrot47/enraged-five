// 
// fwanimation/directorcomponentsyncedscene.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_SYNCED_SCENE_H
#define FWANIMATION_DIRECTOR_COMPONENT_SYNCED_SCENE_H

// Rage
#include "script/thread.h"

#include "directorcomponent.h"

#include "entity/entity.h"
#include "fwtl/regdrefs.h"
#include "paging/ref.h"


namespace rage
{

class fwSyncedSceneAudioInterface
{
public:

	// Destructor

	virtual ~fwSyncedSceneAudioInterface();

	// Constructors

	fwSyncedSceneAudioInterface();

	// Operations

	virtual bool Prepare(const char *audioEvent) = 0;
	virtual u32 Play(u32 audioHash) = 0;
	virtual void Stop(u32 audioHash) = 0;
	virtual void Skip(u32 audioHash, u32 skipTimeMs) = 0;

	// Properties

	virtual u32 GetElapsedTimeMs(u32 audioHash) const = 0;
	virtual u32 GetDurationMs(u32 audioHash) const = 0;

	// Static properties

	static fwSyncedSceneAudioInterface *GetInstance();

protected:

	// Static data

	static fwSyncedSceneAudioInterface *sm_pInstance;

private:

	// Constructors

	fwSyncedSceneAudioInterface(const fwSyncedSceneAudioInterface &);

	// Operators

	fwSyncedSceneAudioInterface &operator =(const fwSyncedSceneAudioInterface &);
};

#define g_pSyncedSceneAudioInterface fwSyncedSceneAudioInterface::GetInstance()

class crClip;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that synced scenes
class fwAnimDirectorComponentSyncedScene : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentSyncedScene();

	// PURPOSE: Destructor
	~fwAnimDirectorComponentSyncedScene();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentSyncedScene);

	// PURPOSE:
	static void StaticInit();

	// PURPOSE:
	static void StaticShutdown();

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);


	// PURPOSE: 
	static void InitDerivedClass();


	// PURPOSE: Instructs the anim director to start playing the specified clip as a synced scene anim
	//			from the scene position provided.
	// PARAMS:	dictionaryIndex - The index of the dictionary being used
	//			pClip - pointer of the clip to play
	//			sceneIndex - the index of the scene to attach to
	//			blendInDelta - time to blend in over, defaults to INSTANT_BLEND_IN_DELTA
	void StartSyncedScenePlayback(int dictionaryIndex, const crClip* pClip, const crClip* pFacialClip, fwSyncedSceneId sceneId, const fwMvNetworkDefId &networkDefId, float blendInDelta=INSTANT_BLEND_IN_DELTA, bool exitOnSceneEnd = true, bool loopWithinScene = false, bool blockMoverUpdate = false);

	// PURPOSE: Instructs the anim director to stop playing any synced scene anim
	//			from the scene position provided.
	void StopSyncedScenePlayback(float blendOutDelta=INSTANT_BLEND_OUT_DELTA, bool tagSyncOut=false);

	// PURPOSE:	Per frame update for synchronized in-game anim scenes
	//			Sends the appropriate signal parameters to MoVE
	void UpdateSyncedSceneParameters(float deltaTime);

	// PURPOSE: Get the root velocity of the synced scene this component belongs to.
	void GetSceneVelocity(Vec3V_InOut sceneVel) const;

	// PURPOSE: Per frame update for synchronized in-game anim scenes
	//			Updates the entity position from the calculated absolute mover position
	void UpdateSyncedSceneMover();

	// PURPOSE: Returns true if the anim director is playing back a synced scene animation
	bool IsPlayingSyncedScene() const;

	// PURPOSE:
	fwSyncedSceneId GetSyncedSceneId() const;

	// PURPOSE: Get the clip currently being used to in the synced scene
	const crClip* GetSyncedSceneClip() const;

	// PURPOSE:
	void SetBlockMoverUpdate(bool block);

	// PURPOSE: Sets the phase of the synced scene anim running on this object
	static void SetSyncedScenePhase(fwSyncedSceneId sceneId, float newPhase);

	// PURPOSE:
	static float GetSyncedScenePhase(fwSyncedSceneId sceneId);

	// PURPOSE: Sets the playback rate of the synced scene
	static void SetSyncedSceneRate(fwSyncedSceneId sceneId, float newRate);

	// PURPOSE:
	static float GetSyncedSceneRate(fwSyncedSceneId sceneId);

	// PURPOSE:
	static void SetSyncedSceneTime(fwSyncedSceneId sceneId, float newTime);

	// PURPOSE:
	static float GetSyncedSceneTime(fwSyncedSceneId sceneId);

	// PURPOSE:
	static float GetSyncedScenePreviousTime(fwSyncedSceneId sceneId);

	// PURPOSE:
	static void SetSyncedSceneDuration(fwSyncedSceneId sceneId, float duration);
#if __BANK
	static void UpdateSyncedSceneDuration(fwSyncedSceneId sceneId);
#endif // __BANK

	// PURPOSE:
	static float GetSyncedSceneDuration(fwSyncedSceneId sceneId);

	// PURPOSE:
	static atHashString GetSyncedSceneAudioEvent(fwSyncedSceneId sceneId);

	// PURPOSE: Sets the origin position and orientation of the synced scene
	static void SetSyncedSceneOrigin(fwSyncedSceneId sceneId, Vector3& scenePosition, Quaternion& sceneOrientation);

	// PURPOSE:
	static void SetSyncedSceneOrigin(fwSyncedSceneId sceneId, Matrix34& sceneMatrix);

	// PURPOSE: 
	static void GetSyncedSceneOrigin(fwSyncedSceneId sceneId, Matrix34& outMatrix);


	// PURPOSE: 
	static bool IsSyncedScenePaused(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static void SetSyncedScenePaused(fwSyncedSceneId sceneId, bool pause);

	// PURPOSE: 
	static bool IsSyncedSceneAbsolute(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static void SetSyncedSceneAbsolute(fwSyncedSceneId sceneId, bool absolute);

	// PURPOSE: 
	static bool IsSyncedSceneLooped(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static void SetSyncedSceneLooped(fwSyncedSceneId sceneId, bool looped);

	// PURPOSE: 
	static bool IsSyncedSceneHoldLastFrame(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static void SetSyncedSceneHoldLastFrame(fwSyncedSceneId sceneId, bool holdLastFrame);

	// PURPOSE: 
	static bool IsSyncedSceneAbort(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static void SetSyncedSceneAbort(fwSyncedSceneId sceneId, bool abort);

	// PURPOSE:
	static void SetDestroySceneCallback(fwSyncedSceneId sceneId, void (*pDestroySceneCallback)(fwSyncedSceneId, void *), void *pParam);

	// PURPOSE: 
	static void AttachSyncedScene(fwSyncedSceneId sceneId, fwEntity* parentEntity, int parentEntityBoneIndex);

	// PURPOSE:
	static void DetachSyncedScene(fwSyncedSceneId sceneId);

	// PURPOSE:
	static void TakeOwnershipOfSyncedScene(fwSyncedSceneId sceneId, scrThreadId newScriptOwnerThreadId);

	// PURPOSE:
	static fwEntity* GetSyncedSceneAttachEntity(fwSyncedSceneId sceneId);

	// PURPOSE:
	static void AddSyncedSceneRef(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static void DecSyncedSceneRef(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static s32 GetSyncedSceneRefCount(fwSyncedSceneId sceneId);

	// PURPOSE: progress the phases of any synchronized scenes that are playing
	static void UpdateSyncedScenes(float deltaTime);

	// PURPOSE: Finds an unused synchronized scene and initializes it for use
	static fwSyncedSceneId StartSynchronizedScene(scrThreadId ScriptId = THREAD_INVALID);

	// PURPOSE: Finds an active synchronized scene at the position and orientation specified (approximate)
	static fwSyncedSceneId FindSynchronizedScene(Vector3& position, Quaternion& orientation);

	// PURPOSE: Tries to free a synced scene slot, by stopping the oldest "unowned" synced scenes.  Unowned means the thread it belongs to has exited.
	static void FreeAnUnownedSyncedScene();

	// PURPOSE:
	static bool IsValidSceneId(fwSyncedSceneId sceneId);

	// PURPOSE: 
	static int GetSyncedScenePoolSize();
	static bool IsSyncedScenePoolFull();
	static void CalcEntityMatrix(fwSyncedSceneId sceneId, const crClip* pClip, bool loopWithinScene, Matrix34& outMat, float time = -1.0f);

	// PURPOSE: Resize the sync scene pool. Will assert if any active scenes remain when this is called.
	static void ResizeSyncedScenePool(int newSize);
	static int CountActiveSyncedScenes();

	// PURPOSE: Setup on the way into network games
	static void StartNetworkSession();

	// PURPOSE: Make any changes for the return to singleplayer.
	static void EndNetworkSession();

#if __BANK
	// PURPOSE:
	static void RegisterWithSyncedScene(fwSyncedSceneId sceneId, fwEntity* pEntity);

	// PURPOSE:
	static void RemoveFromSyncedScene(fwSyncedSceneId sceneId, fwEntity* pEntity);

	// PURPOSE:
	static void RegisterCameraWithSyncedScene(fwSyncedSceneId sceneId, void* pCam, const crClip *pCamClip);

	// PURPOSE:
	static void RemoveCameraFromSyncedScene(fwSyncedSceneId sceneId, void* pCam);
#endif //__BANK

#if __BANK
	// PURPOSE:
	static void DumpSynchronizedSceneDebugInfo(atString& string);

	// PURPOSE:
	static void RenderSynchronizedSceneDebug();
#endif //__BANK

	// PURPOSE:
	static bool PrepareSynchronizedSceneAudioEvent(const char *audioEvent);

	// PURPOSE:
	static bool PrepareSynchronizedSceneAudioEvent(fwSyncedSceneId sceneId, const char *audioEvent);

	// PURPOSE:
	static bool PlaySynchronizedSceneAudioEvent(fwSyncedSceneId sceneId);

	// PURPOSE:
	static bool StopSynchronizedSceneAudioEvent(fwSyncedSceneId sceneId);

private:

	// PURPOSE: An instance of a synced scene 
	struct SyncedInstance
	{
		// PURPOSE:
		SyncedInstance();

	public:
		pgRef<const crClip>	m_Clip; // The clip to playback on this entity. If this and m_network are non null, synced playback is considered to be active
		pgRef<const crClip> m_FacialClip;
		fwMoveNetworkPlayer* m_Network;	 // The MoVE network we're playing this back on
		fwSyncedSceneId m_SceneId; // The index of the synchronized scene we're attached to
		int m_DictionaryIndex;
		bool m_BlockMoverUpdate : 1;		// Can be used to block update of the mover by the synced scene system. This can be usefull if a task wants to take control of the mover temporarily
		bool m_ExitOnSceneEnd : 1;
		bool m_LoopWithinScene : 1;
	};

	SyncedInstance m_SyncedInstance;

	// PURPOSE: Global class for managing a synced scene (aka in-game 'pseudo cut-scene')
	class SyncedScene
	{
	public:

		// PURPOSE:
		SyncedScene();

		// PURPOSE:
		void Update(float deltaTime);

		// PURPOSE:
		void Reset();

		// PURPOSE:
		void GetRootPosition(Vector3& outPosition);

		// PURPOSE:
		void GetRootOrientation(Quaternion& outRotation);

		// PURPOSE:
		void GetRootMatrix(Matrix34& outMatrix);

		// PURPOSE:
		bool PlayAudioEvent();

		// PURPOSE:
		bool StopAudioEvent();

	public:
		Quaternion m_SceneOrientation; // The orientation of the scene 'root'
		Vector3 m_ScenePosition; // The position of the scene 'root'
		float m_Time; // The current time of the synchronized scene
		float m_PrevTime; // The previous time of the synchronized scene
		float m_Duration; // The duration of the longest entity in the synced scene
		float m_Rate;
		atHashString m_AudioHash;
		bool m_Paused : 1; // If true, the scene will be paused
		bool m_Absolute : 1; // If true, the fixup will be applied as an absolute offset (i.e. the position and orientation at phase 0.0 in the clip will not be subtracted)
		bool m_Active : 1;
		bool m_Looped : 1; // Should the animated scene loop
		bool m_HoldLastFrame : 1; // Should the animated scene hold last frame
		bool m_Abort : 1; // If true, the entities currently attached to the scene should abort
		void (*m_pDestroySceneCallback)(fwSyncedSceneId, void *);
		void *m_pDestroySceneCallbackParam;
		s32 m_Refs;	// The number of entities currently attached to the scene. the scene will only be destroyed once all of the entities are removed from it
		fwRegdRef<class fwEntity> m_AttachParent; // The entity this scene is attached to. If this is non-null, the scene orientation and position will be relative to this object.
		s16 m_AttachBoneIndex; // The bone no the parent this entity it attached to. -1 corresponds to the entity matrix.

		// The script that owns this synced scene.  The synced scene will be aborted if the script exits.
		scrThreadId m_ScriptThreadId;

#if __BANK
		atArray< fwRegdRef<fwEntity> > m_AttachedDynamicEntities; // can use this in dev builds to track the objects attached to this scene
		class CamData
		{
		public:

			/* Destructor */

			~CamData()
			{
				Clear();
			}

			/* Constructors */

			CamData()
				: pCam(NULL), pCamClip(NULL)
			{
			}

			CamData(void *pCam, const crClip *pCamClip)
				: pCam(pCam), pCamClip(pCamClip)
			{
			}

			CamData(const CamData &camData)
				: pCam(camData.pCam), pCamClip(camData.pCamClip)
			{
			}

			/* Operators */

			CamData &operator =(const CamData &camData)
			{
				pCam = camData.pCam;
				pCamClip = camData.pCamClip;

				return *this;
			}

			/* Properties */

			void *GetCam() const { return pCam; }
			const crClip *GetCamClip() const { return pCamClip; }

			/* Operations */

			void Clear() { pCam = NULL; pCamClip = NULL; }

		protected:

			/* Data */

			void *pCam;
			pgRef< const crClip > pCamClip;
		};
		atArray< CamData > m_AttachedCameras; // can use this in dev builds to track the objects attached to this scene
#endif //__BANK
	};

	static fwSyncedSceneId CreateSyncedSceneId(int sceneIndex);
	static void ReleaseSyncedSceneId(fwSyncedSceneId sceneId);
	static SyncedScene *GetSyncedScene(fwSyncedSceneId sceneId);

	static atArray<SyncedScene> sm_SynchronizedScenes;
	static atMap<fwSyncedSceneId, int> sm_SynchronizedSceneIdMap;
	static int sm_iInitRefCount;

	static const fwMvClipId sm_ClipId;
	static const fwMvFloatId sm_DeltaId;
	static const fwMvFloatId sm_MoverDeltaId;
	static const fwMvFloatId sm_PhaseId;
	static const fwMvFloatId sm_RateId;
	static const fwMvBooleanId sm_LoopedId;

	static const fwMvClipId sm_FacialClipId;
	static const fwMvFloatId sm_FacialPhaseId;
	static const fwMvFloatId sm_FacialRateId;
	static const fwMvBooleanId sm_FacialLoopedId;
	static const fwMvFloatId sm_FacialBlendInId;
};

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_SYNCED_SCENE_H

