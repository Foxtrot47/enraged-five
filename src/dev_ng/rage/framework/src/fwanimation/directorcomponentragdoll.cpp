// 
// fwanimation/directorcomponentragdoll.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponentragdoll.h"

#include "animdirector.h"
#include "boneids.h"
#include "directorcomponentmove.h"

#include "creature/componentextradofs.h"
#include "creature/creature.h"
#include "entity/archetype.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "fwscene/stores/posematcherstore.h"
#include "fwsys/timer.h"

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentRagDoll::fwAnimDirectorComponentRagDoll()
: fwAnimDirectorComponent(kComponentTypeRagDoll)
, m_ExtractedVelocity(V_ZERO)
, m_Frame(NULL)
, m_RootFrame(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentRagDoll::~fwAnimDirectorComponentRagDoll()
{
	fwAnimDirectorComponentRagDoll::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentRagDoll, 0xbf69bb29, kComponentTypeRagDoll, ANIMDIRECTOR_POOL_PED_MAX, 0.26f);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::Init(fwAnimDirector& director)
{
	InitBase(director);

	// initialize the root frame data if we haven't already done so
	if (!sm_pRootFrameData)
	{
		sm_pRootFrameData = rage_new crFrameDataFixedDofs<2>();
		sm_pRootFrameData->AddDof(kTrackBoneTranslation, 0, kFormatTypeVector3);
		sm_pRootFrameData->AddDof(kTrackBoneRotation, 0, kFormatTypeQuaternion);
	}

	m_RootFrame = rage_new crFrameFixedDofs<2>(*sm_pRootFrameData);

	const fwArchetype* archetype = director.GetEntity().GetArchetype();
	animAssert(archetype);
	if(archetype)
	{
		strLocalIndex idx = strLocalIndex(archetype->GetPoseMatcherFileIndex());
		if(idx != -1)
		{
			crPoseMatcher* poseMatcher = g_PoseMatcherStore.Get(idx);
			if(poseMatcher)
			{
				m_PoseMatcher.AddPoseMatcher(*poseMatcher);
			}
		}

		idx = archetype->GetPoseMatcherProneFileIndex();
		if(idx != -1)
		{
			crPoseMatcher* poseMatcher = g_PoseMatcherStore.Get(idx);
			if(poseMatcher)
			{
				m_PoseMatcher.AddPoseMatcher(*poseMatcher);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::Shutdown()
{
	m_PoseMatcher.Shutdown();

	if(m_Frame)
	{
		m_Director->GetFrameBuffer().ReleaseFrame(m_Frame);
		m_Frame = NULL;
	}

	m_RootFrame->Release();
	m_RootFrame = NULL;

	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::ReceiveMessage(eMessageId msgId, ePhase, void* UNUSED_PARAM(payload))
{
	switch(msgId)
	{
	case kMessageUpdate:
		{
			fwAnimDirector& director = GetDirector();
			fwAnimDirectorComponentMove* componentMove = director.GetComponent<fwAnimDirectorComponentMove>();
			if(componentMove)
			{
				componentMove->GetMove().SetFrame(sm_RagdollFrameId, m_Frame);
				componentMove->GetMove().SetFrame(sm_RagdollRootFrameId, m_RootFrame);
			}
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::PoseFromRagDoll(const crCreature& creature)
{
	const crSkeleton* skeleton = creature.GetSkeleton();
	if(Verifyf(skeleton, "Creature has no skeleton!"))
	{
		if(!m_Frame)
		{
			m_Frame = m_Director->GetFrameBuffer().AllocateFrame();
		}

		m_Frame->InversePose(*skeleton, false, NULL);

		const crCreatureComponentExtraDofs *pExtraDofs = creature.FindComponent<crCreatureComponentExtraDofs>();
		if(pExtraDofs)
		{
			m_Frame->Set(pExtraDofs->GetPoseFrame());
		}

		// The following dofs are generated procedurally within the tree, and 
		// as such we don't want to store them in the ragdoll frame.
		m_Frame->InvalidateDof(kTrackUpperBodyFixupWeight, BONETAG_ROOT);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupShadow, BONETAG_ROOT);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupShadow, BONETAG_SPINE0);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupShadow, BONETAG_SPINE1);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupShadow, BONETAG_SPINE2);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupShadow, BONETAG_SPINE3);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupShadow, BONETAG_SPINE_ROOT);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupScratch, BONETAG_ROOT);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupScratch, BONETAG_SPINE0);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupScratch, BONETAG_SPINE1);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupScratch, BONETAG_SPINE2);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupScratch, BONETAG_SPINE3);
		m_Frame->InvalidateDof(kTrackUpperBodyFixupScratch, BONETAG_SPINE_ROOT);

		m_Frame->SetMoverSituation(TransformV(V_IDENTITY));

		m_Frame->Normalize();

		m_ExtractedVelocity.ZeroComponents();
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::PoseRagdollRootFrame(const crCreature& creature)
{
	const crSkeleton* skeleton = creature.GetSkeleton();
	if(Verifyf(skeleton, "Creature has no skeleton!"))
	{
		m_RootFrame->InversePose(*skeleton);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::PoseSkelRootFromRagdollRootFrame(crCreature& creature)
{
	crSkeleton* skeleton = creature.GetSkeleton();
	if(Verifyf(skeleton, "Creature has no skeleton!"))
	{
		m_RootFrame->Pose(*skeleton);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentRagDoll::PoseSkeletonUsingRagdollFrame(crSkeleton& skeleton, const crClip* pClip, float fPhase)
{
	if(pClip)
	{
		if(!m_Frame)
		{
			m_Frame = m_Director->GetFrameBuffer().AllocateFrame();
		}

		float time = pClip->ConvertPhaseToTime(fPhase);
		pClip->Composite(*m_Frame, time, 0.0f);
		m_Frame->Pose(skeleton);
	}
}

void fwAnimDirectorComponentRagDoll::SetFromFrame(const crFrame& frame, crFrameFilter* filter)
{
	if(!m_Frame)
	{
		m_Frame = m_Director->GetFrameBuffer().AllocateFrame();
	}

	m_Frame->Set(frame, filter);
}

////////////////////////////////////////////////////////////////////////////////

const fwMvFrameId fwAnimDirectorComponentRagDoll::sm_RagdollFrameId("RagdollFrame",0x19C78D61);
const fwMvFrameId fwAnimDirectorComponentRagDoll::sm_RagdollRootFrameId("RagdollRootFrame",0x9C5ECFB3);
crFrameDataFixedDofs<2>* fwAnimDirectorComponentRagDoll::sm_pRootFrameData(NULL);

////////////////////////////////////////////////////////////////////////////////

} // namespace rage


