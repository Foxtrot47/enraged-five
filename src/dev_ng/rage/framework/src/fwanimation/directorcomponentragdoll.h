// 
// fwanimation/directorcomponentragdoll.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_DIRECTOR_COMPONENT_RAG_DOLL_H
#define FWANIMATION_DIRECTOR_COMPONENT_RAG_DOLL_H

#include "directorcomponent.h"
#include "posematcher.h"

#include "cranimation/frame.h"
#include "creature/creature.h"
#include "vectormath/vectormath.h"

namespace rage
{

class crClip;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Animation director component that manages expressions
class fwAnimDirectorComponentRagDoll : public fwAnimDirectorComponent
{
public:

	// PURPOSE: Default constructor
	fwAnimDirectorComponentRagDoll();

	// PURPOSE: Destructor
	~fwAnimDirectorComponentRagDoll();

	// PURPOSE: Register component type
	FW_ANIM_DIRECTOR_DECLARE_COMPONENT_TYPE(fwAnimDirectorComponentRagDoll);

	// PURPOSE: Initializer
	void Init(fwAnimDirector& director);

	// PURPOSE: Shutdown
	virtual void Shutdown();

	// PURPOSE: Receive message
	virtual void ReceiveMessage(eMessageId msgId, ePhase phase, void* payload);

	// PURPOSE:
	void PoseFromRagDoll(const crCreature& creature);

	// PURPOSE: Poses the ragdoll root frame. Should be called whenever the bounds are synced to the animated skeleton
	void PoseRagdollRootFrame(const crCreature& creature);

	// PURPOSE: Poses the root of the passed in creature's skeleton from the ragdoll root frame. Call when switching
	// to ragdoll to ensure the animated skeleton stays correctly in sync with the ragdoll bounds.
	void PoseSkelRootFromRagdollRootFrame(crCreature& creature);

	// PURPOSE: Pose skeleton using ragdoll frame
	void PoseSkeletonUsingRagdollFrame(crSkeleton& skeleton, const crClip* pClip, float fPhase);

	// PURPOSE: Set ragdoll frame using given frame
	void SetFromFrame(const crFrame& frame, crFrameFilter* filter=NULL);

	// PURPOSE:
	const crFrame* GetFrame() const;
	crFrame* GetFrame();

	// PURPOSE:
	Vec3V_ConstRef GetExtractedVelocity() const; 

	// PURPOSE: 
	fwPoseMatcher& GetPoseMatcher() { return m_PoseMatcher; }

private:

	Vec3V m_ExtractedVelocity;

	fwPoseMatcher m_PoseMatcher;

	crFrame* m_Frame;
	crFrame* m_RootFrame;
	
	static crFrameDataFixedDofs<2>* sm_pRootFrameData;

	static const fwMvFrameId sm_RagdollFrameId;
	static const fwMvFrameId sm_RagdollRootFrameId;
};

////////////////////////////////////////////////////////////////////////////////

inline const crFrame* fwAnimDirectorComponentRagDoll::GetFrame() const
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline crFrame* fwAnimDirectorComponentRagDoll::GetFrame()
{
	return m_Frame;
}

////////////////////////////////////////////////////////////////////////////////

inline Vec3V_ConstRef fwAnimDirectorComponentRagDoll::GetExtractedVelocity() const
{
	return m_ExtractedVelocity;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_DIRECTOR_COMPONENT_RAG_DOLL_H

