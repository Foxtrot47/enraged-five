// 
// fwanimation/posematcher.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "posematcher.h"

#include "crparameterizedmotion/pointcloud.h"
#include "entity/entity.h"
#include "fwanimation/anim_channel.h"
#include "fwanimation/animdirector.h"
#include "grcore/debugdraw.h"
#include "vector/colors.h"
#include "vector/vector3.h"

namespace rage 
{

////////////////////////////////////////////////////////////////////////////////

fwPoseMatcher::fwPoseMatcher()
{
	Init();
}

////////////////////////////////////////////////////////////////////////////////

fwPoseMatcher::~fwPoseMatcher()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwPoseMatcher::Init()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwPoseMatcher::Shutdown()
{
	m_PoseMatcher.Reset();
}

////////////////////////////////////////////////////////////////////////////////

void fwPoseMatcher::AddPoseMatcher(crPoseMatcher& poseMatcher)
{
	m_PoseMatcher.PushAndGrow(&poseMatcher, 1);

#if __DEV
	CheckForDuplicateSamples();
#endif
}

////////////////////////////////////////////////////////////////////////////////

bool fwPoseMatcher::FindBestMatch(const crSkeleton& skel, fwMvClipSetId& clipSetId, fwMvClipId& clipId, float& time, Matrix34& transform, fwPoseMatcher::BaseFilter* filter)
{
	u64 key = 0;
	Mat34V temp = RCC_MAT34V(transform);

	bool ret = false;

	BaseFilter baseFilter;
	if(filter)
	{
		filter->ResetBestDistance();
	}
	else
	{
		filter = &baseFilter;
	}

	for(int i=0; i<m_PoseMatcher.GetCount(); i++)
	{
		if(m_PoseMatcher[i] && m_PoseMatcher[i]->FindBestMatch(skel, key, time, temp, filter))
		{
			ret = true;

#if __DEV
			u64 calckey = CalcKey(clipSetId, clipId);
			const int numSamples = m_PoseMatcher[i]->m_Samples.GetCount();
			for(int j=0; j<numSamples; ++j)
			{
				if(calckey == m_PoseMatcher[i]->m_Samples[j]->m_Key) 
				{
					s_LastMatchedPose = *m_PoseMatcher[i]->m_Samples[j]->m_PointCloud;
					s_LastMatchedPoseName = clipId.GetCStr();
					break;
				}
			}
#endif

			clipSetId = fwMvClipSetId(key>>32);
			clipId = fwMvClipId(key & 0xFFFFFFFF);

			transform = RCC_MATRIX34(temp);
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

const crPoseMatcher::MatchSample* fwPoseMatcher::FindSample(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, float time)
{
	u64 key = CalcKey(clipSetId, clipId);
	for(int i=0; i<m_PoseMatcher.GetCount(); i++)
	{
		if(m_PoseMatcher[i])
		{
			const int numSamples = m_PoseMatcher[i]->m_Samples.GetCount();
			for(int j=0; j<numSamples; ++j)
			{
				if(key == m_PoseMatcher[i]->m_Samples[j]->m_Key && (time < 0.0f || time == m_PoseMatcher[i]->m_Samples[j]->m_Time))
				{
					return m_PoseMatcher[i]->m_Samples[j];
				}
			}
		}
	}

	return NULL;
}

////////////////////////////////////////////////////////////////////////////////

#if __DEV
void fwPoseMatcher::DebugRenderPointCloud(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const Matrix34& renderTransform, float boneSize)
{
	const crPoseMatcher::MatchSample* sample = FindSample(clipSetId, clipId);

	if(sample)
	{
		DebugRenderPointCloud(sample->m_PointCloud, renderTransform, boneSize, Color_white);
	}
	else
	{
		animAssertf(0,"No point cloud found for %s", PrintClipInfo(clipSetId, clipId));
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwPoseMatcher::Dump()
{
	for(int i=0; i<m_PoseMatcher.GetCount(); i++)
	{
		if(m_PoseMatcher[i])
		{
			int numSamples = m_PoseMatcher[i]->m_Samples.GetCount();

			Printf("fwPoseMatcher::Dump. %d samples.\n", numSamples);
			Printf("key, fwMvClipSetId (hash), fwMvClipId (hash)\n");

			for(int j=0; j<numSamples; ++j)
			{
				u64 key = m_PoseMatcher[i]->m_Samples[j]->m_Key;
				fwMvClipSetId clipSetId = fwMvClipSetId(key>>32);
				fwMvClipId clipId = fwMvClipId(key & 0xFFFFFFFF);

				Printf("%" I64FMT "u, %s (%u), %s (%u)\n", key, clipSetId.GetCStr(), clipSetId.GetHash(), clipId.GetCStr(), clipId.GetHash());
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwPoseMatcher::CheckForDuplicateSamples()
{
	const int numPoseMatcher = m_PoseMatcher.GetCount();
	for(int x=0; x<numPoseMatcher; x++)
	{
		if(m_PoseMatcher[x])
		{
			for(int y=0; y<numPoseMatcher; y++)
			{
				if(x != y && m_PoseMatcher[y])
				{
					const int numSamplesX = m_PoseMatcher[x]->m_Samples.GetCount();
					const int numSamplesY = m_PoseMatcher[y]->m_Samples.GetCount();
					for(int i=0; i<numSamplesX; i++)
					{
						for(int j=0; j<numSamplesY; j++)
						{
							if(m_PoseMatcher[x]->m_Samples[i]->m_Key == m_PoseMatcher[y]->m_Samples[j]->m_Key)
							{
								fwMvClipSetId clipSetId = fwMvClipSetId(m_PoseMatcher[x]->m_Samples[i]->m_Key >> 32);
								fwMvClipId clipId = fwMvClipId(m_PoseMatcher[x]->m_Samples[i]->m_Key & 0xFFFFFFFF);
								animWarningf("Pose match sample %s:%s found in more than one database", clipSetId.GetCStr(), clipId.GetCStr());
							}
						}
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwPoseMatcher::DebugRenderPointCloud(crpmPointCloud* cloud, const Matrix34& renderTransform, float boneSize, rage::Color32 color)
{
	char index[3];

	for(crpmPointCloud::size_type i=0; i<cloud->size(); i++)
	{
		Vec3V pointEntry(cloud->GetPoint(i));
		pointEntry = Transform(RCC_MAT34V(renderTransform), pointEntry);

		grcDebugDraw::Sphere(pointEntry, boneSize, color);
		formatf(index, "%d", i);
		grcDebugDraw::Text(pointEntry, color, 0,0, index);
	}
}

#endif // __DEV

////////////////////////////////////////////////////////////////////////////////

#if __DEV
crpmPointCloud fwPoseMatcher::s_LastMatchedPose;
const char* fwPoseMatcher::s_LastMatchedPoseName = NULL;
#endif // __DEV

////////////////////////////////////////////////////////////////////////////////

} // namespace rage



