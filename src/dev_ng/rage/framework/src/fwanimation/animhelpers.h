// 
// fwanimation/animhelpers.h
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#ifndef FWANIMATION_ANIM_HELPERS_H
#define FWANIMATION_ANIM_HELPERS_H

#include "anim_channel.h"
#include "animdefines.h"
#include "clipsets.h"

#include "crmetadata/properties.h"
#include "fwscene/stores/clipdictionarystore.h"
#include "fwscene/stores/expressionsdictionarystore.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "fwscene/stores/networkdefstore.h"
#include "fwscene/stores/posematcherstore.h"
#include "vector/quaternion.h"

namespace rage
{

class crClip;
class atString;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Static animation helper functions
class fwAnimHelpers
{
public:

	// PURPOSE: Check if clip has mover translation track
	static bool HasMoverTranslation(const crClip& clip);	

	// PURPOSE: Check if clip has camera translation track
	static bool HasCameraTranslation(const crClip& clip);

	// PURPOSE: Retrieve mover translation at specific clip phase
	static Vector3 GetMoverTrackTranslation(const crClip& clip, const float phase);
	static Vector3 GetMoverTrackTranslation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase);

	// PURPOSE: Retrieve camera translation at specific clip phase
	static Vector3 GetCameraTrackTranslation(const crClip& clip, const float phase);
	static Vector3 GetCameraTrackTranslation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase);

	// PURPOSE: Retrieve difference in mover translation between two clip phases
	// NOTES: Does NOT handle looping (i.e. startPhase > endPhase), use Delta calls instead
	static Vector3 GetMoverTrackTranslationDiff(const crClip& clip, const float startPhase, const float endPhase);
	static Vector3 GetMoverTrackTranslationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase);

	// PURPOSE: Retrieve difference in camera translation between two clip phases
	// NOTES: Does NOT handle looping (i.e. startPhase > endPhase), use Delta calls instead
	static Vector3 GetCameraTrackTranslationDiff(const crClip& clip, const float startPhase, const float endPhase);
	static Vector3 GetCameraTrackTranslationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase);

	// PURPOSE: Retrieve mover rotation at specific clip phase
	static Quaternion GetMoverTrackRotation(const crClip& clip, const float phase);
	static Quaternion GetMoverTrackRotation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase);

	// PURPOSE: Retrieve camera rotation at specific clip phase
	static Quaternion GetCameraTrackRotation(const crClip& clip, const float phase);
	static Quaternion GetCameraTrackRotation(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float phase);

	// PURPOSE: Retrieve difference in mover rotation between two clip phases
	// NOTES: Does NOT handle looping (i.e. startPhase > endPhase), use Delta calls instead
	static Quaternion GetMoverTrackRotationDiff(const crClip& clip, const float startPhase, const float endPhase);
	static Quaternion GetMoverTrackRotationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase);

	// PURPOSE: Retrieve difference in camera rotation between two clip phases
	// NOTES: Does NOT handle looping (i.e. startPhase > endPhase), use Delta calls instead
	static Quaternion GetCameraTrackRotationDiff(const crClip& clip, const float startPhase, const float endPhase);
	static Quaternion GetCameraTrackRotationDiff(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase);

	// PURPOSE: Retrieve difference in mover rotation between two clip phases
	// NOTES: If phase loops (i.e. startPhase > endPhase), rotation will be calculated based on looping around 1.0
	static Quaternion GetMoverTrackRotationDelta(const crClip& clip, float startPhase, float endPhase);

	// PURPOSE: Retrieve difference in camera rotation between two clip phases
	// NOTES: If phase loops (i.e. startPhase > endPhase), rotation will be calculated based on looping around 1.0
	static Quaternion GetCameraTrackRotationDelta(const crClip& clip, float startPhase, float endPhase);

	// PURPOSE: Retrieve difference in mover translation and rotation between two clip phases as matrix
	// NOTES: If phase loops (i.e. startPhase > endPhase), rotation will be calculated based on looping around 1.0
	// Translation will also be rotated to correct for effects of initial orientation 
	static void GetMoverTrackMatrixDelta(const crClip& pClip, float startPhase, float endPhase, Matrix34& outMatrix);

	// PURPOSE: Retrieve difference in camera translation and rotation between two clip phases as matrix
	// NOTES: If phase loops (i.e. startPhase > endPhase), rotation will be calculated based on looping around 1.0
	// Translation will also be rotated to correct for effects of initial orientation 
	static void GetCameraTrackMatrixDelta(const crClip& pClip, float startPhase, float endPhase, Matrix34& outMatrix);

	// PURPOSE: Retrieve mover velocity over entire clip
	static Vector3 GetMoverTrackVelocity(const crClip& clip);
	static Vector3 GetMoverTrackVelocity(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId);

	// PURPOSE: Retrieve camera velocity over entire clip
	static Vector3 GetCameraTrackVelocity(const crClip& clip);
	static Vector3 GetCameraTrackVelocity(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId);

	// PURPOSE: Retrieve mover translation between two clip phases, rotated by start phase rotation
	// NOTES: Does NOT handle looping (i.e. startPhase > endPhase), use Displacement call instead
	static Vector3 GetMoverTrackTranslationDiffRotated(const crClip& clip, float startPhase, float endPhase, bool asDelta = false);

	// PURPOSE: Retrieve camera translation between two clip phases, rotated by start phase rotation
	// NOTES: Does NOT handle looping (i.e. startPhase > endPhase), use Displacement call instead
	static Vector3 GetCameraTrackTranslationDiffRotated(const crClip& clip, float startPhase, float endPhase, bool asDelta = false);


	// PURPOSE: Retrieve mover translation between two clip phases, rotated to correct for various effects
	// NOTES: If phase loops (i.e. startPhase > endPhase), rotation will be calculated based on looping around 1.0
	// Translation will also be rotated to correct for effects of initial orientation 
	static Vector3 GetMoverTrackDisplacement(const crClip& clip, float startPhase, float endPhase);
	static Vector3 GetMoverTrackDisplacement(const fwMvClipSetId &clipSetId, const fwMvClipId &clipId, const float startPhase, const float endPhase);

	// PURPOSE: Retrieve camera translation between two clip phases, rotated to correct for various effects
	// NOTES: If phase loops (i.e. startPhase > endPhase), rotation will be calculated based on looping around 1.0
	// Translation will also be rotated to correct for effects of initial orientation 
	static Vector3 GetCameraTrackDisplacement(const crClip& clip, float startPhase, float endPhase);


	// PURPOSE: Does clip alter angular or linear position of mover track
	// NOTES: This is based solely on the position and orientation at the start and end of the clip. 
	// Therefore, animations which start and end in the same position/orientation will return false,
	// even if they contain movement
	static bool ContributesToAngularMovement(const crClip& clip);
	static bool ContributesToLinearMovement(const crClip& clip);


	// PURPOSE: Converts a GTA blend delta (e.g. NORMAL_BLEND_IN_DELTA) into a duration for passing to MoVE
	static float CalcBlendDuration(float blendDelta); 


	// PURPOSE: Apply the the authored initial offset in the clip file to the provided matrix (if it exists)
	// PARAMS:	clip - source clip
	// outMtx, outPosition, outRotation - reference to the matrix/vector/quaternion
	//   that will be transformed with the authored initial offset.  If no authored offset
	//   is specified in the clip, this will be left unchanged
	// RETURNS:	true if the clip contains an authored offset, otherwise false
	static bool ApplyInitialOffsetFromClip(const crClip& clip, Matrix34& outMtx);
	static bool ApplyInitialOffsetFromClip(const crClip& clip, Vector3& outPosition, Quaternion& outRotation );

	// PURPOSE: Generates a debug clip name for a clip (strips off "pack:/", ".clip" and any absolute path from the clip name.
	// PARAMS:	clip, out atString
	// RETURNS:	nothing.
	static void GetDebugClipName(const crClip& clip, atString& outDebugClipName);

private:
	
	static crProperty::Key sm_PositionPropertyId;
	static crProperty::Key sm_RotationPropertyId;
};

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Simplifies force loading of dictionaries
template<typename _DictionaryType, typename _StoreType>
class fwDictionaryLoader
{
public:

	// PURPOSE: Constructor
	fwDictionaryLoader(int slot, bool force=true);

	// PURPOSE: Destructor
	// NOTE: Automatically unloads, if required
	~fwDictionaryLoader();

	// PURPOSE: Has dictionary been loaded?
	bool IsLoaded() const;

	// PURPOSE: Was the dictionary valid
	// NOTE: May not have loaded (but slot index was valid and object was in image)
	bool IsValid() const;

	// PURPOSE: Retrieve dictionary pointer (can be NULL)
	_DictionaryType* GetDictionary() const;

	// PURPOSE: Explicitly attempt to load
	bool Load(bool force=true);

	// PURPOSE: Explicitly unload
	void Unload();

	// PURPOSE: Get dictionary store
	static _StoreType& GetStore();

private:
	strLocalIndex m_Slot;
	bool m_Loaded;
	bool m_Unload;
	bool m_Valid;
};

////////////////////////////////////////////////////////////////////////////////

typedef fwDictionaryLoader<crClipDictionary, fwClipDictionaryStore> fwClipDictionaryLoader;
typedef fwDictionaryLoader<crExpressionsDictionary, fwAssetRscStore<crExpressionsDictionary> > fwExpressionsDictionaryLoader;
typedef fwDictionaryLoader<crFrameFilterDictionary, crFrameFilterDictionaryStore> fwFrameFilterDictionaryLoader;
typedef fwDictionaryLoader<pgDictionary<crPoseMatcher>, fwPoseMatcherStore> fwPoseMatcherLoader;
typedef fwDictionaryLoader<mvNetworkDef, fwNetworkDefStore> fwNetworkDefLoader;

////////////////////////////////////////////////////////////////////////////////

inline float fwAnimHelpers::CalcBlendDuration(float blendDelta) 
{ 	
	blendDelta = fabs(blendDelta);
	return blendDelta==INSTANT_BLEND_IN_DELTA ? 0.f : 1.f / blendDelta;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
inline fwDictionaryLoader<_DictionaryType, _StoreType>::fwDictionaryLoader(int slot, bool force) 
: m_Slot(slot)
, m_Loaded(false)
, m_Unload(false)
, m_Valid(false)
{
	Load(force);
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
inline fwDictionaryLoader<_DictionaryType, _StoreType>::~fwDictionaryLoader()
{
	Unload();
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
inline bool fwDictionaryLoader<_DictionaryType, _StoreType>::IsLoaded() const
{
	return m_Loaded;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
inline bool fwDictionaryLoader<_DictionaryType, _StoreType>::IsValid() const
{
	return m_Valid;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
inline _DictionaryType* fwDictionaryLoader<_DictionaryType, _StoreType>::GetDictionary() const
{
	return IsLoaded()?GetStore().Get(m_Slot):NULL;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
bool fwDictionaryLoader<_DictionaryType, _StoreType>::Load(bool force)
{
	if(m_Slot != -1)
	{
		// Is the current slot valid?
		if(GetStore().IsValidSlot(m_Slot))
		{
			// Is the clip dictionary in the image?
			if(GetStore().IsObjectInImage(m_Slot))
			{
				// Is the clip dictionary already loaded?
				if(GetStore().HasObjectLoaded(m_Slot))
				{
					m_Loaded = true;
				}
				else if(force)
				{
					// Try and stream in the clip dictionary
					GetStore().StreamingRequest(m_Slot, STRFLAG_FORCE_LOAD | STRFLAG_DONTDELETE | STRFLAG_PRIORITY_LOAD);
					strStreamingEngine::GetLoader().LoadAllRequestedObjects();

					if(GetStore().HasObjectLoaded(m_Slot))
					{
						m_Loaded = true;
						m_Unload = true;
					}
				}
				m_Valid = true;
			}
		}
	}

	return m_Loaded;
}

////////////////////////////////////////////////////////////////////////////////

template<typename _DictionaryType, typename _StoreType>
void fwDictionaryLoader<_DictionaryType, _StoreType>::Unload()
{
	if (m_Unload)
	{
		GetStore().ClearRequiredFlag(m_Slot.Get(), STRFLAG_DONTDELETE);
		strIndex index = GetStore().GetStreamingIndex(m_Slot);
		strStreamingEngine::GetInfo().GetStreamingInfoRef(index).ClearFlag(index, STRFLAG_FORCE_LOAD);
		m_Unload = false;
	}
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline fwClipDictionaryStore& fwClipDictionaryLoader::GetStore()
{
	return g_ClipDictionaryStore;
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline fwAssetRscStore<crExpressionsDictionary>& fwExpressionsDictionaryLoader::GetStore()
{
	return g_ExpressionDictionaryStore;
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline crFrameFilterDictionaryStore& fwFrameFilterDictionaryLoader::GetStore()
{
	return g_FrameFilterDictionaryStore;
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline fwPoseMatcherStore& fwPoseMatcherLoader::GetStore()
{
	return g_PoseMatcherStore;
}

////////////////////////////////////////////////////////////////////////////////

template<>
inline fwNetworkDefStore& fwNetworkDefLoader::GetStore()
{
	return g_NetworkDefStore;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage

#endif // FWANIMATION_ANIM_HELPERS_H

