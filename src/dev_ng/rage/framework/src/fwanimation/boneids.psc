<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
							generate="class">
 
  <enumdef type="eAnimBoneTag">    
    <enumval name="BONETAG_INVALID"         value="-1"/>
    <enumval name="BONETAG_ROOT"            value="0"/>
    <enumval name="BONETAG_PELVISROOT"      value="17916"/>
    <enumval name="BONETAG_PELVIS"          value="11816"/>
    <enumval name="BONETAG_PELVIS1"         value="53251"/>
    <enumval name="BONETAG_SPINE_ROOT"      value="57597"/>
    <enumval name="BONETAG_SPINE0"          value="23553"/>
    <enumval name="BONETAG_SPINE1"          value="24816"/>
    <enumval name="BONETAG_SPINE2"          value="24817"/>
    <enumval name="BONETAG_SPINE3"          value="24818"/>
    <enumval name="BONETAG_NECK"            value="39317"/>
    <enumval name="BONETAG_NECK2"           value="24532"/>
    <enumval name="BONETAG_HEAD"            value="31086"/>
    <enumval name="BONETAG_R_CLAVICLE"      value="10706"/>
    <enumval name="BONETAG_R_UPPERARM"      value="40269"/>
    <enumval name="BONETAG_R_FOREARM"       value="28252"/>
    <enumval name="BONETAG_R_HAND"          value="57005"/>
    <enumval name="BONETAG_R_FINGER0"       value="58866"/>
    <enumval name="BONETAG_R_FINGER01"      value="64016"/>
    <enumval name="BONETAG_R_FINGER02"      value="64017"/>
    <enumval name="BONETAG_R_FINGER1"       value="58867"/>
    <enumval name="BONETAG_R_FINGER11"      value="64096"/>
    <enumval name="BONETAG_R_FINGER12"      value="64097"/>
    <enumval name="BONETAG_R_FINGER2"       value="58868"/>
    <enumval name="BONETAG_R_FINGER21"      value="64112"/>
    <enumval name="BONETAG_R_FINGER22"      value="64113"/>
    <enumval name="BONETAG_R_FINGER3"       value="58869"/>
    <enumval name="BONETAG_R_FINGER31"      value="64064"/>
    <enumval name="BONETAG_R_FINGER32"      value="64065"/>
    <enumval name="BONETAG_R_FINGER4"       value="58870"/>
    <enumval name="BONETAG_R_FINGER41"      value="64080"/>
    <enumval name="BONETAG_R_FINGER42"      value="64081"/>
    <enumval name="BONETAG_L_CLAVICLE"      value="64729"/>
    <enumval name="BONETAG_L_UPPERARM"      value="45509"/>
    <enumval name="BONETAG_L_FOREARM"       value="61163"/>
    <enumval name="BONETAG_L_HAND"          value="18905"/>
    <enumval name="BONETAG_L_FINGER0"       value="26610"/>
    <enumval name="BONETAG_L_FINGER01"      value="4089"/>
    <enumval name="BONETAG_L_FINGER02"      value="4090"/>
    <enumval name="BONETAG_L_FINGER1"       value="26611"/>
    <enumval name="BONETAG_L_FINGER11"      value="4169"/>
    <enumval name="BONETAG_L_FINGER12"      value="4170"/>
    <enumval name="BONETAG_L_FINGER2"       value="26612"/>
    <enumval name="BONETAG_L_FINGER21"      value="4185"/>
    <enumval name="BONETAG_L_FINGER22"      value="4186"/>
    <enumval name="BONETAG_L_FINGER3"       value="26613"/>
    <enumval name="BONETAG_L_FINGER31"      value="4137"/>
    <enumval name="BONETAG_L_FINGER32"      value="4138"/>
    <enumval name="BONETAG_L_FINGER4"       value="26614"/>
    <enumval name="BONETAG_L_FINGER41"      value="4153"/>
    <enumval name="BONETAG_L_FINGER42"      value="4154"/>
    <enumval name="BONETAG_L_THIGH"         value="58271"/>
    <enumval name="BONETAG_L_CALF"          value="63931"/>
    <enumval name="BONETAG_L_FOOT"          value="14201"/>
    <enumval name="BONETAG_L_TOE"           value="2108"/>
    <enumval name="BONETAG_L_TOE1"          value="7531"/>
    <enumval name="BONETAG_R_THIGH"         value="51826"/>
    <enumval name="BONETAG_R_CALF"          value="36864"/>
    <enumval name="BONETAG_R_FOOT"          value="52301"/>
    <enumval name="BONETAG_R_TOE"           value="20781"/>
    <enumval name="BONETAG_R_TOE1"          value="45631"/>
    <enumval name="BONETAG_NECKROLL"        value="35731"/>
    <enumval name="BONETAG_L_ARMROLL"       value="5232"/>
    <enumval name="BONETAG_R_ARMROLL"       value="37119"/>
    <enumval name="BONETAG_L_FOREARMROLL"   value="61007"/>
    <enumval name="BONETAG_R_FOREARMROLL"   value="43810"/>
    <enumval name="BONETAG_L_THIGHROLL"     value="23639"/>
    <enumval name="BONETAG_R_THIGHROLL"     value="6442"/>
    <enumval name="BONETAG_L_PH_HAND"       value="60309"/>
    <enumval name="BONETAG_R_PH_HAND"       value="28422"/>
    <enumval name="BONETAG_WEAPON_GRIP"     value="41922"/>
    <enumval name="BONETAG_WEAPON_GRIP2"    value="18212"/>
    <enumval name="BONETAG_CAMERA"          value="33399"/>
    <enumval name="BONETAG_SKEL_SADDLE"     value="38180"/>
    <enumval name="BONETAG_L_STIRRUP"       value="39126"/>
    <enumval name="BONETAG_R_STIRRUP"       value="39638"/>
    <enumval name="BONETAG_L_IK_HAND"       value="36029"/>
    <enumval name="BONETAG_R_IK_HAND"       value="6286"/>
    <enumval name="BONETAG_TAIL0"           value="838"/>
    <enumval name="BONETAG_TAIL1"           value="839"/>
    <enumval name="BONETAG_TAIL2"           value="840"/>
    <enumval name="BONETAG_TAIL3"           value="841"/>
    <enumval name="BONETAG_TAIL4"           value="842"/>
    <enumval name="BONETAG_TAIL5"           value="843"/>    
    <enumval name="BONETAG_SKEL_TAIL0"      value="30992"/>
    <enumval name="BONETAG_SKEL_TAIL1"      value="30993"/>
    <enumval name="BONETAG_SKEL_TAIL2"      value="30994"/>
    <enumval name="BONETAG_SKEL_TAIL3"      value="30995"/>
    <enumval name="BONETAG_SKEL_TAIL4"      value="30996"/>
    <enumval name="BONETAG_SKEL_TAIL5"      value="30997"/>    
    <enumval name="BONETAG_TAILM1"          value="6986"/>
    <enumval name="BONETAG_TAILM2"          value="6987"/>
    <enumval name="BONETAG_TAILM3"          value="6988"/>
    <enumval name="BONETAG_JAW"             value="16351"/>
    <enumval name="BONETAG_L_CLAW"          value="1805"/>
    <enumval name="BONETAG_R_CLAW"          value="39905"/>
    <enumval name="BONETAG_L_EYE"           value="5956"/>
    <enumval name="BONETAG_R_EYE"           value="6468"/>
    <enumval name="BONETAG_L_EYE2"          value="25260"/>
    <enumval name="BONETAG_R_EYE2"          value="27474"/>
    <enumval name="BONETAG_FACING_DIR"      value="56604"/>
    <enumval name="BONETAG_LOOK_DIR"        value="12844"/>
    <enumval name="BONETAG_L_PH_FOOT"       value="57717"/>
    <enumval name="BONETAG_R_PH_FOOT"       value="24806"/>
    <enumval name="BONETAG_L_IK_FOOT"       value="65245"/>
    <enumval name="BONETAG_R_IK_FOOT"       value="35502"/>
    <enumval name="BONETAG_HIGH_HEELS"      value="15570"/>
    <enumval name="BONETAG_HIGH_HEELS_META" value="28462"/>
    <enumval name="BONETAG_HAIR_SCALE"      value="13201"/>
    <enumval name="BONETAG_HAIR_HEIGHT"     value="48472"/>
    <enumval name="BONETAG_GUN_STOCK"       value="5930"/>
    <enumval name="BONETAG_BAGROOT"         value="44297"/>
    <enumval name="BONETAG_BAGPIVOTROOT"    value="47158"/>
    <enumval name="BONETAG_BAGPIVOT"        value="19729"/>
    <enumval name="BONETAG_BAGBODY"         value="43885"/>
    <enumval name="BONETAG_BAGBONE_R"       value="2359"/>
    <enumval name="BONETAG_BAGBONE_L"       value="2449"/>
    <enumval name="BONETAG_HEAD_X"          value="19336"/>
    <enumval name="BONETAG_NECK_X"          value="64744"/>
    <enumval name="BONETAG_NECK_Y"          value="64745"/>
    <enumval name="BONETAG_NECK_Z"          value="64746"/>
    <enumval name="BONETAG_FIRSTPERSONCAM"      value="56194"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_MINX" value="45733"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_MINY" value="45734"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_MINZ" value="45735"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_MAXX" value="48389"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_MAXY" value="48390"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_MAXZ" value="48391"/>
    <enumval name="BONETAG_FIRSTPERSONCAM_FOV" value="30929"/>
    <enumval name="BONETAG_CH_L_HAND" value="33869"/>
    <enumval name="BONETAG_CH_R_HAND" value="4126"/>
  </enumdef>
</ParserSchema>
