// 
// fwanimation/directorcomponentfacialrig.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponentfacialrig.h"

#include "animdirector.h"
#include "directorcomponentcreature.h"
#include "directorcomponentexpressions.h"
#include "directorcomponentmotiontree.h"
#include "directorcomponentmove.h"

#include "crmotiontree/requestblend.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestclip.h"
#include "crmotiontree/requestfilter.h"
#include "crmotiontree/requestframe.h"
#include "crmotiontree/requestmerge.h"
#include "entity/entity.h"
#include "fwmaths/random.h"
#include "fwsys/timer.h"
#include "string/stringhash.h"
#include "move/move_clip.h"


#define PREVENT_NO_PROGRESS_BLENDS (1)

namespace rage
{

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::fwAnimDirectorComponentFacialRig()
: fwAnimDirectorComponent(kComponentTypeFacialRig)
#if USE_FACIAL_FRAME_DATA
, m_FrameData(NULL)
#endif // USE_FACIAL_FRAME_DATA
, m_Frame(NULL)
, m_MouthClipInit(false)
, m_fDesiredVoiceDrivenMouthMovementWeight(0.0f)
, m_fVoiceDrivenMouthMovementBlendRate(4.0f)
, m_fVoiceLoudness(0.0f)
, m_iVoiceStartTimeMs(-1)
#if __BANK
, m_bEnable(true)
#endif
{
	m_Filter.AddRef();

	m_MidPhysicsFrame.AddRef();
	m_MidPhysicsCapture.AddRef();
	m_PreRenderFrame.AddRef();
	m_ExpressionRoot.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::fwAnimDirectorComponentFacialRig(fwAnimDirector& director)
: fwAnimDirectorComponent(kComponentTypeFacialRig)
, m_Frame(NULL)
, m_MouthClipInit(false)
, m_fDesiredVoiceDrivenMouthMovementWeight(0.0f)
, m_fVoiceDrivenMouthMovementBlendRate(4.0f)
, m_fVoiceLoudness(0.0f)
, m_iVoiceStartTimeMs(-1)
#if __BANK
, m_bEnable(true)
#endif
{
	m_Filter.AddRef();

	m_MidPhysicsFrame.AddRef();
	m_MidPhysicsCapture.AddRef();
	m_PreRenderFrame.AddRef();
	m_ExpressionRoot.AddRef();

	Init(director);
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::~fwAnimDirectorComponentFacialRig()
{
	fwAnimDirectorComponentFacialRig::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentFacialRig, 0x210e10be, kComponentTypeFacialRig, ANIMDIRECTOR_POOL_PED_MAX, 0.26f);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::Init(fwAnimDirector& director)
{
	InitBase(director);

	Assert(director.GetCreature());

	m_Filter.Init(director);

#if USE_FACIAL_FRAME_DATA
	fwAnimDirectorComponentCreature& componentCreature = *director.GetComponent<fwAnimDirectorComponentCreature>();
	m_FrameData = componentCreature.GetFrameDataFactory().AllocateFrameData(componentCreature.GetCreature(), &m_Filter);
	m_FrameBuffer.Init(componentCreature.GetCommonPool());
	m_FrameBuffer.SetFrameData(*m_FrameData);
	m_Frame = m_FrameBuffer.AllocateFrame();
#else // USE_FACIAL_FRAME_DATA
	fwAnimDirectorComponentCreature& componentCreature = *director.GetComponent<fwAnimDirectorComponentCreature>();
	m_Frame = componentCreature.GetFrameBuffer().AllocateFrame();
#endif // USE_FACIAL_FRAME_DATA

	crmtRequestFrame reqFrameMidPhysics(m_Frame); 

	// Facial nodes, in lowest to highest priority order
	crmtRequestClip reqIdleClip(NULL);
	reqIdleClip.SetLooping(true, true);
	crmtRequestBlend reqIdleBlend(reqIdleClip, reqFrameMidPhysics, 1.f);
	reqIdleBlend.SetMergeBlend(true);

	crmtRequestClip reqOneShotClip(NULL);
	reqOneShotClip.SetLooping(true, false);
	crmtRequestBlend reqOneShotBlend(reqIdleBlend, reqOneShotClip, 0.f);
	reqOneShotBlend.SetMergeBlend(true);

	crmtRequestClip reqSyncedSceneClip(NULL);
	reqSyncedSceneClip.SetLooping(true, false);
	crmtRequestBlend reqSyncedSceneBlend(reqOneShotBlend, reqSyncedSceneClip, 0.f);
	reqSyncedSceneBlend.SetMergeBlend(true);

	crmtRequestClip reqVisemeClip(NULL);
	reqVisemeClip.SetLooping(true, false);
	crmtRequestBlend reqVisemeBlend(reqSyncedSceneBlend, reqVisemeClip, 0.f);
	reqVisemeBlend.SetMergeBlend(true);

	crmtRequestClip reqMouthClip(NULL);
	reqMouthClip.SetLooping(true, true);
	crmtRequestBlend reqMouthBlend(reqVisemeBlend, reqMouthClip, 0.f);
	reqMouthBlend.SetMergeBlend(true);

	crmtRequestCapture reqCapture(reqMouthBlend, m_Frame);

	fwAnimDirectorComponentMotionTree* componentMotionTreeMidPhysics = director.GetComponentByPhase<fwAnimDirectorComponentMotionTree>(kPhaseMidPhysics);
	Assert(componentMotionTreeMidPhysics);

	componentMotionTreeMidPhysics->GetMotionTree().Request(reqCapture);

	m_MidPhysicsFrame.Attach(reqFrameMidPhysics.GetObserver());
	m_MidPhysicsCapture.Attach(reqCapture.GetObserver());

	m_IdleNodeCtrl.Init(reqIdleClip.GetObserver(), reqIdleBlend.GetObserver(), fwMvClipId("FacialIdleClip",0x5DCCE343), 0, 0, 0, 0, fwMvFloatId("FacialIdleClipBlendDuration",0x46C8BCEB), 0, 0);
	m_MouthNodeCtrl.Init(reqMouthClip.GetObserver(), reqMouthBlend.GetObserver(), 0, fwMvFloatId("VoiceDrivenMouthMovementClipPhase",0x144F8E7F));
	m_OneShotNodeCtrl.Init(reqOneShotClip.GetObserver(), reqOneShotBlend.GetObserver(), fwMvClipId("FacialOneShotClip",0x493C9BB8), 0, 0, 0, 0, fwMvFloatId("FacialOneShotClipBlendDuration",0x9FF1248C), 0, 0, true);
	m_SyncedSceneNodeCtrl.Init(reqSyncedSceneClip.GetObserver(), reqSyncedSceneBlend.GetObserver(), fwMvClipId("FacialClip", 0x2416E454), fwMvFloatId("FacialPhase",0x8158623E), 0, fwMvFloatId("FacialRate",0x33019CC7), 0, fwMvFloatId("SyncedSceneFacialBlendIn",0x41c6d43f), 0, 0);
	m_VisemeNodeCtrl.Init(reqVisemeClip.GetObserver(), reqVisemeBlend.GetObserver(), fwMvClipId("FacialVisemeClip",0x7839CEA), fwMvFloatId("FacialVisemePhase",0x380D8D8A), 0, fwMvFloatId("FacialVisemeRate",0x35F5F7AA), 0, fwMvFloatId("FacialVisemeClipBlendDuration",0x730B1F19), 0, fwMvBooleanId("Viseme_ClipEnded",0x89360EF2));

	m_ExpressionRoot.Attach(reqMouthBlend.GetObserver());

	fwAnimDirectorComponentMotionTree* componentMotionTreePreRender = director.GetComponentByPhase<fwAnimDirectorComponentMotionTree>(fwAnimDirectorComponent::kPhasePreRender);
	Assert(componentMotionTreePreRender);

	crmtRequestFrame reqFramePreRender(m_Frame);
	crmtRequestFilter reqFilter(reqFramePreRender, &m_Filter);
	crmtRequestMerge reqMergePreRender;
	reqMergePreRender.SetSource(&reqFilter, 0);

	componentMotionTreePreRender->GetMotionTree().Request(reqMergePreRender);

	m_PreRenderFrame.Attach(reqFramePreRender.GetObserver());
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::Shutdown()
{
	if(m_Frame)
	{
		m_GarbageFrames.Grow() = m_Frame;
		m_Frame = NULL;
	}

	RemoveGarbageFrames();
	Assert(m_GarbageFrames.GetCount() == 0);

#if USE_FACIAL_FRAME_DATA
	m_FrameBuffer.Shutdown();

	if(m_FrameData)
	{
		fwAnimDirectorComponentCreature::GetFrameDataFactory().ReleaseFrameData(*m_FrameData);
		m_FrameData = NULL;
	}
#endif // USE_FACIAL_FRAME_DATA

	m_IdleNodeCtrl.Shutdown();
	m_MouthNodeCtrl.Shutdown();
	m_OneShotNodeCtrl.Shutdown();
	m_SyncedSceneNodeCtrl.Shutdown();
	m_VisemeNodeCtrl.Shutdown();
	
	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::ReceiveMessage(eMessageId msgId, ePhase phase, void*)
{
	switch(msgId)
	{
	case kMessageUpdate:
		{
			fwAnimDirector& director = GetDirector();
			switch(phase)
			{
			case kPhasePrePhysics:
				{
					fwAnimDirectorComponentMove* componentMove = director.GetComponent<fwAnimDirectorComponentMove>();
					if(componentMove)
					{
						componentMove->GetMove().SetFrame(sm_CaptureFrameId, m_Frame);
					}
					if(m_GarbageFrames.GetCount())
					{
						RemoveGarbageFrames();
					}
				}
				break;

			case kPhasePreRender:
				{
					fwAnimDirectorComponentMotionTree* componentMotionTree = director.GetComponentByPhase<fwAnimDirectorComponentMotionTree>(kPhaseMidPhysics);
					if(componentMotionTree)
					{
						componentMotionTree->WaitOnComplete();
					}
				}
				break;

			default:
				break;
			}
		}
		break;

	case kMessageMotionTreePreDelegate:
		{
			switch(phase)
			{
			case kPhasePrePhysics:
				{
					fwAnimDirector& director = GetDirector();
					fwAnimDirectorComponentMove* componentMove = director.GetComponent<fwAnimDirectorComponentMove>();
					if(componentMove)
					{
						mvParameterBuffer& buffer = componentMove->GetMove().GetInputBuffers().GetNextBuffer().GetInternalBuffer();
					
#if __BANK
						if(!m_bEnable)
						{
							m_IdleNodeCtrl.SetClip(NULL);
							m_MouthNodeCtrl.SetClip(NULL);
							m_OneShotNodeCtrl.SetClip(NULL);
							m_SyncedSceneNodeCtrl.SetClip(NULL);
							m_VisemeNodeCtrl.SetClip(NULL);
						}
						else
#endif // __BANK
						{
							m_IdleNodeCtrl.Update(buffer);

							// Voice Driven Mouth Movement
							UpdateVoiceDrivenMouthMovement();
						
							m_OneShotNodeCtrl.Update(buffer);
							m_SyncedSceneNodeCtrl.Update(buffer);
							m_VisemeNodeCtrl.Update(buffer);
						}
					}
				}
				break;

			default:
				break;
			}
		}
		break;

	case kMessageFrameDataChanged:
		{
			fwAnimDirector& director = GetDirector();
#if USE_FACIAL_FRAME_DATA
			fwAnimDirectorComponentCreature& componentCreature = *director.GetComponent<fwAnimDirectorComponentCreature>();
			crFrameData* newFrameData = componentCreature.GetFrameDataFactory().AllocateFrameData(componentCreature.GetCreature(), &m_Filter);
			if(newFrameData != m_FrameData)
			{
				crFrameData* oldFrameData = m_FrameData;
				m_FrameData = newFrameData;

				m_FrameBuffer.SetFrameData(*m_FrameData);

				m_GarbageFrames.Grow() = m_Frame;
				m_Frame = m_FrameBuffer.AllocateFrame();

				if(m_MidPhysicsFrame.IsAttached())
				{
					m_MidPhysicsFrame.GetNodeSafe()->SetFrame(m_Frame);
				}
				if(m_MidPhysicsCapture.IsAttached())
				{
					m_MidPhysicsCapture.GetNodeSafe()->SetFrame(m_Frame);
				}
				if(m_PreRenderFrame.IsAttached())
				{
					m_PreRenderFrame.GetNodeSafe()->SetFrame(m_Frame);
				}

				RemoveGarbageFrames();

				if(oldFrameData)
				{
					fwAnimDirectorComponentCreature::GetFrameDataFactory().ReleaseFrameData(*oldFrameData);
				}
			}
#else // USE_FACIAL_FRAME_DATA
			const crFrameData& frameData = GetDirector().GetFrameData();
			if(&frameData != m_Frame->GetFrameData())
			{
				fwAnimDirectorComponentCreature& componentCreature = *director.GetComponent<fwAnimDirectorComponentCreature>();
			
				m_GarbageFrames.Grow() = m_Frame;
				m_Frame = componentCreature.GetFrameBuffer().AllocateFrame();

				if(m_MidPhysicsFrame.IsAttached())
				{
					m_MidPhysicsFrame.GetNodeSafe()->SetFrame(m_Frame);
				}
				if(m_MidPhysicsCapture.IsAttached())
				{
					m_MidPhysicsCapture.GetNodeSafe()->SetFrame(m_Frame);
				}
				if(m_PreRenderFrame.IsAttached())
				{
					m_PreRenderFrame.GetNodeSafe()->SetFrame(m_Frame);
				}

				RemoveGarbageFrames();
			}
#endif // USE_FACIAL_FRAME_DATA
			m_Filter.Init(director);
		}
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::InitDerivedClass()
{
	fwAnimDirectorComponentFacialRig::FacialFilter::InitClass();
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::FacialFilter::FacialFilter()
: crFrameFilter(eFrameFilterType(crFrameFilter::kFrameFilterTypeFrameworkFilters+1))
, m_SkeletonData(NULL)
{
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::FacialFilter::FacialFilter(datResource& rsc)
: crFrameFilter(rsc)
{
	// does not support resourcing
	Assert(0);
}

////////////////////////////////////////////////////////////////////////////////

CR_IMPLEMENT_FRAME_FILTER_TYPE(fwAnimDirectorComponentFacialRig::FacialFilter, crFrameFilter::eFrameFilterType(crFrameFilter::kFrameFilterTypeFrameworkFilters+1), crFrameFilter);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::FacialFilter::Init(fwAnimDirector& director)
{
	m_SkeletonData = &director.GetCreature()->GetSkeleton()->GetSkeletonData();

#if __ASSERT
	fwAnimDirectorComponentExpressions* exprComponent = director.GetComponentByPhase<fwAnimDirectorComponentExpressions>(kPhaseMidPhysics);
	if(exprComponent && exprComponent->HasExpressions())
	{
		bool foundByName = m_SkeletonData->FindBoneData("FACIAL_facialRoot") != NULL;
		if(!foundByName)
		{
			animWarningf("fwAnimDirectorComponentFacialRig::FacialFilter failed to find REQUIRED bone FACIAL_facialRoot by name. Modelname = %s. The bone may be missing, or the model may have been assigned incorrect expressions in peds.meta.", director.GetEntity().GetModelName() ? director.GetEntity().GetModelName() : "Unknown");
		}
	
		int junk; 
		bool foundById = m_SkeletonData->ConvertBoneIdToIndex(crSkeletonData::ConvertBoneNameToId("FACIAL_facialRoot"), junk);
		if(!foundById)
		{
			animWarningf("fwAnimDirectorComponentFacialRig::FacialFilter failed to find REQUIRED bone FACIAL_facialRoot by ID (65068). Modelname = %s. The bone may be missing, or the model may have been assigned incorrect expressions in peds.meta.", director.GetEntity().GetModelName() ? director.GetEntity().GetModelName() : "Unknown");
		}
	}
#endif // __ASSERT

	SetSignature(CalcSignature());
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::FacialFilter::Serialize(datSerialize& s)
{
	crFrameFilter::Serialize(s);

	// does not support serialization
	Assert(0);
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialFilter::FilterDof(u8 track, u16 id, float& inoutWeight)
{
	static u16 facialRootBoneId = crSkeletonData::ConvertBoneNameToId("FACIAL_facialRoot");

	switch(track)
	{
	case kTrackAnimatedNormalMaps:
	case kTrackBlendShape:
	case kTrackVisemes:
	case kTrackFacialControl:
	case kTrackFacialTranslation:
	case kTrackFacialRotation:
	case kTrackFacialScale:
		return (inoutWeight > 0.f);

	case kTrackBoneTranslation:
	case kTrackBoneRotation:
	case kTrackBoneScale:
		{
			int idx;
			if(m_SkeletonData->ConvertBoneIdToIndex(id, idx))
			{
				const crBoneData* bd = m_SkeletonData->GetBoneData(idx);
				while(bd)
				{
					if(bd->GetBoneId() == facialRootBoneId)
					{
						return (inoutWeight > 0.f);
					} 
					bd = bd->GetParent();
				}
			}
		}
		break;

	default:
		break;
	}

	inoutWeight = 0.f;
	return false;
}

////////////////////////////////////////////////////////////////////////////////

u32 fwAnimDirectorComponentFacialRig::FacialFilter::CalcSignature() const
{
	return m_SkeletonData->GetSignature() ^ ATSTRINGHASH("FacialFilter", 0x5D1FD161);
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::FacialNodeController::FacialNodeController()
: m_ClipKey(0)
, m_PhaseKey(0)
, m_WeightKey(0)
, m_RateKey(0)
, m_LoopKey(0)
, m_BlendInDurationKey(0)
, m_BlendOutDurationKey(0)
, m_BlendOutTransitionKey(0)
, m_ManageWeight(false)
, m_DoBlendOutTransition(false)
, m_AlwaysSetNewClip(false)
, m_BlendInRate(sm_DefaultBlendInOutRate)
, m_BlendOutRate(sm_DefaultBlendInOutRate)
{
	m_ClipObserver.AddRef();
	m_BlendObserver.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentFacialRig::FacialNodeController::~FacialNodeController()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::FacialNodeController::Shutdown()
{
	m_ClipObserver.Detach();
	m_BlendObserver.Detach();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::FacialNodeController::Init(const crmtObserver& clipObsever, const crmtObserver& blendObserver, u32 clipKey, u32 phaseKey, u32 weightKey, u32 rateKey, u32 loopKey, u32 blendInDurationKey, u32 blendOutDurationKey, u32 blendOutTransitionKey, bool bAlwaysSetNewClip)
{
	m_ClipObserver.Attach(clipObsever);
	m_BlendObserver.Attach(blendObserver);

	m_ClipKey = clipKey;
	m_PhaseKey = phaseKey;
	m_WeightKey = weightKey;
	m_RateKey = rateKey;
	m_LoopKey = loopKey;
	m_BlendInDurationKey = blendInDurationKey;
	m_BlendOutDurationKey = blendOutDurationKey;
	m_BlendOutTransitionKey = blendOutTransitionKey;

	m_ManageWeight = !m_WeightKey && !m_ClipObserver.GetNodeSafe()->GetClipPlayer().IsLooped();
	m_DoBlendOutTransition = false;

	m_AlwaysSetNewClip = bAlwaysSetNewClip;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::Update(mvParameterBuffer& buffer)
{
	bool weighted = false;

	if(Unlikely(m_LoopKey))
	{
		const mvParameter* loopParameter = buffer.FindFirstOfType(m_LoopKey, mvParameter::kParameterBoolean);
		if(Unlikely(loopParameter))
		{
			SetLooping(loopParameter->GetBool());
			buffer.Insert(m_LoopKey, false);
		}
	}
	if(m_BlendInDurationKey)
	{
		const mvParameter* blendInDurationParameter = buffer.FindFirstOfType(m_BlendInDurationKey, mvParameter::kParameterReal);
		if(Unlikely(blendInDurationParameter))
		{
			SetBlendInDuration(blendInDurationParameter->GetReal());
			buffer.Insert(m_BlendInDurationKey, 0.f);
		}
	}
	if(Unlikely(m_BlendOutDurationKey))
	{
		const mvParameter* blendOutDurationParameter = buffer.FindFirstOfType(m_BlendOutDurationKey, mvParameter::kParameterReal);
		if(Unlikely(blendOutDurationParameter))
		{
			SetBlendOutDuration(blendOutDurationParameter->GetReal());
			buffer.Insert(m_BlendOutDurationKey, 0.f);
		}
	}
	if(m_ClipKey)
	{
		const mvParameter* clipParameter = buffer.FindFirstOfType(m_ClipKey, mvParameter::kParameterClip);
		if(Unlikely(clipParameter))
		{
			SetClip(clipParameter->GetClip());
			buffer.Insert(m_ClipKey, (const crClip*)NULL);
		}
	}
	if(m_PhaseKey)
	{
		const mvParameter* phaseParameter = buffer.FindFirstOfType(m_PhaseKey, mvParameter::kParameterReal);
		if(Unlikely(phaseParameter))
		{
			SetPhase(phaseParameter->GetReal());
			buffer.Insert(m_PhaseKey, 0.f);
		}
	}
	if(Unlikely(m_WeightKey))
	{
		const mvParameter* weightParameter = buffer.FindFirstOfType(m_WeightKey, mvParameter::kParameterReal);
		if(Unlikely(weightParameter))
		{
			SetWeight(weightParameter->GetReal());
			buffer.Insert(m_WeightKey, 0.f);
			weighted = (weightParameter->GetReal())>0.f;		
		}
	}
	if(m_RateKey)
	{
		const mvParameter* rateParameter = buffer.FindFirstOfType(m_RateKey, mvParameter::kParameterReal);
		if(Unlikely(rateParameter))
		{
			SetRate(rateParameter->GetReal());
			buffer.Insert(m_RateKey, 0.f);
		}
	}

	// Blending out behaviors - forced or managed.
	if(m_BlendOutTransitionKey)
	{
		const mvParameter* forceBlendOutParameter = buffer.FindFirstOfType(m_BlendOutTransitionKey, mvParameter::kParameterBoolean);
		if(Unlikely(forceBlendOutParameter))
		{
			m_DoBlendOutTransition = true;
			buffer.Insert(m_BlendOutTransitionKey, forceBlendOutParameter->GetBool());
		}
	}

	if(m_ManageWeight || m_DoBlendOutTransition)
	{
		crmtNodeClip* nodeClip = m_ClipObserver.GetNodeSafe();
		if(nodeClip)
		{
			const crClip* clip = nodeClip->GetClipPlayer().GetClip();
			if(clip)
			{
				crmtNodeBlend* nodeBlend = m_BlendObserver.GetNodeSafe();
				if(nodeBlend)
				{
					if(m_DoBlendOutTransition || (IsClose(clip->GetDuration(), nodeClip->GetClipPlayer().GetTime()) && !nodeClip->GetClipPlayer().IsLooped()))
					{
						nodeBlend->SetWeightRate(-m_BlendOutRate);
						if(IsNearZero(nodeBlend->GetWeight()))
						{
							nodeClip->GetClipPlayer().SetClip(NULL);
							m_DoBlendOutTransition = false;
						}
					}
				}
			}
		}
	}

	return weighted;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::SetClip(const crClip* clip)
{
	crmtNodeClip* node = m_ClipObserver.GetNodeSafe();
	if(node)
	{
		if(node->GetClipPlayer().GetClip() != clip || m_AlwaysSetNewClip)
		{
#if PREVENT_NO_PROGRESS_BLENDS
			bool noProgress = node->GetParent() && node->GetParent() != m_BlendObserver.GetNode() && node->GetParent()->GetNodeType() == crmtNode::kNodeBlend && IsNearZero(static_cast<crmtNodeBlend*>(node->GetParent())->GetWeight());
			if(node->GetClipPlayer().GetClip() && !noProgress)
#else // PREVENT_NO_PROGRESS_BLENDS
			if(node->GetClipPlayer().GetClip())
#endif // PREVENT_NO_PROGRESS_BLENDS
			{
				crmtRequestClip reqClip(clip);
				reqClip.SetLooping(true, node->GetClipPlayer().IsLooped());

				crmtRequestBlend reqBlend(reqClip, 0.f, m_BlendInRate);

				node->GetMotionTree().Request(reqBlend, &m_ClipObserver);

				m_ClipObserver.Attach(reqClip.GetObserver());
			}
			else
			{
				node->GetClipPlayer().SetClip(clip);
				node->GetClipPlayer().SetTime(0.0f);
			}
		}

		if(m_ManageWeight)
		{
			if(clip)
			{
				crmtNodeBlend* nodeBlend = m_BlendObserver.GetNodeSafe();
				if(nodeBlend)
				{
					nodeBlend->SetWeightRate(m_BlendInRate);
				}
			}
		}

		// Set a new clip, so don't force it to blend out
		m_DoBlendOutTransition = false;

		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::SetPhase(float phase)
{
	crmtNodeClip* node = m_ClipObserver.GetNodeSafe();
	if(node)
	{
		const crClip* clip = node->GetClipPlayer().GetClip();
		if(clip)
		{
			node->GetClipPlayer().SetTime(clip->ConvertPhaseToTime(phase));
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::SetWeight(float weight)
{
	crmtNodeBlend* node = m_BlendObserver.GetNodeSafe();
	if(node)
	{
		node->SetWeight(weight);
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::SetRate(float rate)
{
	crmtNodeClip* node = m_ClipObserver.GetNodeSafe();
	if(node)
	{
		node->GetClipPlayer().SetRate(rate);
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::SetLooping(bool looping)
{
	bool bResult = false;
	crmtNodeClip* node = m_ClipObserver.GetNodeSafe();
	if(node)
	{
		node->GetClipPlayer().SetLooped(true, looping);
		bResult = true;

		m_ManageWeight = !m_WeightKey && !node->GetClipPlayer().IsLooped();

		// If we're not being managed and we're looping then make sure we stay at full weight by setting the weight rate.
		if (!m_ManageWeight && !m_WeightKey && node->GetClipPlayer().IsLooped())
		{
			crmtNodeBlend* nodeBlend = m_BlendObserver.GetNodeSafe();
			if(nodeBlend)
			{
				nodeBlend->SetWeightRate(m_BlendInRate);
			}
		}
	}

	return bResult;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::FacialNodeController::SetBlendInDuration(float duration)
{
	if (duration >= 0.01f)
	{
		m_BlendInRate = 1.0f / duration;
	}
	else
	{
		m_BlendInRate = 1000.0f;
	}	
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::FacialNodeController::SetBlendOutDuration(float duration)
{
	if (duration >= 0.01f)
	{
		m_BlendOutRate = 1.0f / duration;
	}
	else
	{
		m_BlendOutRate = 1000.0f;
	}	
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentFacialRig::FacialNodeController::GetWeight(float& weightOut)
{
	crmtNodeBlend* node = m_BlendObserver.GetNodeSafe();
	if(node)
	{
		weightOut = node->GetWeight();
		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::ClearSyncedSceneFacialAnim(float fBlendOutTime)
{
	m_SyncedSceneNodeCtrl.SetLooping(false);
	m_SyncedSceneNodeCtrl.SetRate(1.0f);
	m_SyncedSceneNodeCtrl.SetBlendInDuration(fBlendOutTime); // Blend out in this case, is the blend in of a null clip.
	m_SyncedSceneNodeCtrl.SetClip(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::SetVoiceDrivenMouthMovementBlend(float fDesiredWeight, float fBlendDuration)
{
	m_fDesiredVoiceDrivenMouthMovementWeight = fDesiredWeight;

	if (fBlendDuration > 0.01f)
	{
		m_fVoiceDrivenMouthMovementBlendRate = 1.0f / fBlendDuration;
	}
	else
	{
		m_fVoiceDrivenMouthMovementBlendRate = 0.0f;
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::RemoveAllNonIdleFacialAnims()
{
	m_OneShotNodeCtrl.SetClip(NULL);
	m_VisemeNodeCtrl.SetClip(NULL);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::UpdateVoiceDrivenMouthMovement()
{
	// Get the weight
	float fPrevWeight;
	if (m_MouthNodeCtrl.GetWeight(fPrevWeight))
	{
		if (fPrevWeight != m_fDesiredVoiceDrivenMouthMovementWeight)
		{
			// If animation is about to become active, then choose a random clip phase.
			if (fPrevWeight <= 0.0f)
			{
				m_MouthNodeCtrl.SetPhase(fwRandom::GetRandomNumberInRange(0.0f, 1.0f));
			}

			// Blend to desired weight
			if (m_fVoiceDrivenMouthMovementBlendRate <= 0.0f)
			{
				// Instant blend
				m_MouthNodeCtrl.SetWeight(m_fDesiredVoiceDrivenMouthMovementWeight);
				m_MouthNodeCtrl.SetRate(m_fDesiredVoiceDrivenMouthMovementWeight > 0.0f ? 1.0f : 0.0f);
			}
			else
			{
				// Blend in or out?
				if (m_fDesiredVoiceDrivenMouthMovementWeight < fPrevWeight)
				{
					// Out
					float fNewWeight = fPrevWeight - (m_fVoiceDrivenMouthMovementBlendRate * fwTimer::GetTimeStep());
					fNewWeight = Clamp(fNewWeight, m_fDesiredVoiceDrivenMouthMovementWeight, 1.0f);
					m_MouthNodeCtrl.SetWeight(fNewWeight);
					m_MouthNodeCtrl.SetRate(fNewWeight > 0.0f ? 1.0f : 0.0f);
				}
				else
				{
					// In
					float fNewWeight = fPrevWeight + (m_fVoiceDrivenMouthMovementBlendRate * fwTimer::GetTimeStep());
					fNewWeight = Clamp(fNewWeight, 0.0f, m_fDesiredVoiceDrivenMouthMovementWeight);
					m_MouthNodeCtrl.SetWeight(fNewWeight);
					m_MouthNodeCtrl.SetRate(fNewWeight > 0.0f ? 1.0f : 0.0f);
				}
			}
		}
	}

	float fMouthWeight;
	if (m_MouthNodeCtrl.GetWeight(fMouthWeight))
	{
		if(fMouthWeight > 0.0f && !m_MouthClipInit)
		{
			const crClip* mouthClip = fwAnimDirector::RetrieveClip(mvNodeClipDef::kClipContextClipDictionary, ATSTRINGHASH("mp_facial", 0x4503F6B9), ATSTRINGHASH("MIC_CHATTER", 0xED9FFE5C), NULL);
			m_MouthNodeCtrl.SetClip(mouthClip);
			m_MouthNodeCtrl.SetPhase(fwRandom::GetRandomNumberInRange(0.0f, 1.0f));
			if(mouthClip)
			{
				mouthClip->Release();
			}
			m_MouthClipInit = true;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentFacialRig::RemoveGarbageFrames()
{
	for(int i=m_GarbageFrames.GetCount()-1; i>=0; --i)
	{
		if(m_GarbageFrames[i]->GetRef() == 1)
		{
#if USE_FACIAL_FRAME_DATA
			m_FrameBuffer.ReleaseFrame(m_GarbageFrames[i]);
#else // USE_FACIAL_FRAME_DATA
			GetDirector().GetComponent<fwAnimDirectorComponentCreature>()->GetFrameBuffer().ReleaseFrame(m_GarbageFrames[i]);
#endif // USE_FACIAL_FRAME_DATA
			m_GarbageFrames.DeleteFast(i);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

const float fwAnimDirectorComponentFacialRig::FacialNodeController::sm_DefaultBlendInOutRate = 1.f/0.25f;

////////////////////////////////////////////////////////////////////////////////

const fwMvFrameId fwAnimDirectorComponentFacialRig::sm_CaptureFrameId("FacialCaptureFrame",0xFE0CB107);

////////////////////////////////////////////////////////////////////////////////

} // namespace rage




