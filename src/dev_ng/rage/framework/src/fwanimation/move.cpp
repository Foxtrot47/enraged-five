#include "fwanimation/move.h"

#include "crmetadata/propertyattributes.h"
#include "data/resource.h"
#include "fwanimation/animdirector.h"
#include "fwanimation/directorcomponentmove.h"
#include "fwanimation/clipsets.h"
#include "fwaudio/audioentity.h"
#include "move/move_state.h"
#include "parser/manager.h"

#if __ASSERT
#include "fwsys/timer.h"
#endif // __ASSERT

#define DEFAULT_INPUT_BUFFER_SIZE (8)

namespace rage {

////////////////////////////////////////////////////////////////////////////////

fwMoveNetworkPlayer::fwMoveNetworkPlayer(fwAnimDirector& director, const mvNetworkDef& networkDef)
: fwMoveNetworkInterface()
, m_Director(&director)
, m_SubNetwork(NULL)
, m_NextPlayer(NULL)
, m_NumRefs(0)
, m_EventWatch(u32(0))
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	m_flags.ClearAllFlags();

	m_SubNetwork = rage_new rage::mvSubNetwork();  // allocates from move pool

	m_SubNetwork->Init(&networkDef, m_OutputBuffers);
	m_SubNetwork->AddRef();

	m_SubNetwork->SetMoveNetworkInterface((void*)this);

	// this registers the sub network with the parameter interface
	RegisterSubNetwork(m_SubNetwork);

	m_flags.SetFlag(Flag_ReadyForInsert);

#if __ASSERT
	m_uCreationFrame = fwTimer::GetFrameCount();
#endif // __ASSERT
}

////////////////////////////////////////////////////////////////////////////////

fwMoveNetworkPlayer::~fwMoveNetworkPlayer()
{
	Shutdown();
}


////////////////////////////////////////////////////////////////////////////////

void fwMoveNetworkPlayer::Shutdown()
{
	DR_DEV_ONLY(SetShuttingDown());
	if (m_Director)
	{
		m_Director = NULL;
	}

	if (m_SubNetwork)
	{
		m_SubNetwork->Release();
		m_SubNetwork = NULL;
	}

	ClearClipSets();
}

void fwMoveNetworkPlayer::SetNetworkObserver(mvObserver* pObserver)
{
	static const fwMvObserverId observerId ("move_network_observer",0x7DD50177);
	SetObserver(observerId, pObserver);
}

//////////////////////////////////////////////////////////////////////////
// Move network interface
//////////////////////////////////////////////////////////////////////////

fwMoveNetworkInterface::fwMoveNetworkInterface()
: m_pNetwork(NULL)
#if DR_ENABLED && __DEV
, m_bShuttingDown(false)
#endif
, m_OutputBuffersSwapped(false)
{
	m_InputBuffers.GetCurrentBuffer().SetManager(&fwAnimDirectorComponentMove::GetCommandBufferManager());
	m_InputBuffers.GetNextBuffer().SetManager(&fwAnimDirectorComponentMove::GetCommandBufferManager());
}

//////////////////////////////////////////////////////////////////////////

fwMoveNetworkInterface::~fwMoveNetworkInterface()
{
	m_InputBuffers.Shutdown();
	m_OutputBuffers.Shutdown();

	ClearClipSets();

	m_pNetwork = NULL; // This isn't owned by the network interface...
}

//////////////////////////////////////////////////////////////////////////

bool fwMoveNetworkInterface::ClearOutputParameters(atArray<fwMvId>& idsToMatch)
{
	bool keyFound = false;
	for(int i=0; i<idsToMatch.GetCount(); ++i)
	{
		mvParameterBuffer::Iterator it;
		for(mvKeyedParameter* kp = it.Begin(m_OutputBuffers.GetCurrentBuffer(), mvParameter::kParameterNone, idsToMatch[i].GetHash()); kp != NULL; kp = it.Next())
		{
			if(!keyFound)
			{
				// output buffers are typically finalized for performance, must unfinalize before invalidating anything
				m_OutputBuffers.GetCurrentBuffer().UnFinalize();
			}
			kp->ResetRelease();
			keyFound = true;
		}
	}
	return keyFound;
}

#if __BANK

//////////////////////////////////////////////////////////////////////////

const char * fwMoveNetworkInterface::FindNetworkDefName() const
{
#if __DEV
	if (m_pNetwork && m_pNetwork->GetDefinition())
	{
		return AnimDirector().FindNetworkDefName(m_pNetwork->GetDefinition());
	}
#endif //__DEV
	return "NULL";
}


//////////////////////////////////////////////////////////////////////////

void fwMoveNetworkInterface::DumpOutputParamBuffer(atString& string)
{		
	char numParams[64];
	sprintf(numParams, ": %d output params", m_OutputBuffers.GetCurrentBuffer().CalcCount());

	string +=numParams;

	m_OutputBuffers.GetCurrentBuffer().Dump();
}


void fwMoveNetworkInterface::DumpInputParamBuffer(atString& output)
{	
	GetInputBuffer().Dump(output);
}

#endif //__BANK


//////////////////////////////////////////////////////////////////////////
#if __DEV
bool fwMoveNetworkInterface::CheckParameterChainLength(int)
{
	return true;
}

void fwMoveNetworkInterface::CheckParameterBuffer(int ASSERT_ONLY(bufferLimit))
{
	animAssertf(CheckParameterChainLength(bufferLimit), 
		"fwMoveNetworkInterface::m_ParameterChain fwEntity '%p' buffers are not being consumed!", 
		&AnimDirector().GetEntity());
}
#endif
//////////////////////////////////////////////////////////////////////////

fwMvId fwMoveNetworkInterface::GetLastEventThisFrame(const fwMvId eventsToSearch[], u32 eventCount)
{
	mvParameterBuffer::Iterator it;
	for(mvKeyedParameter* kp = it.End(m_OutputBuffers.GetCurrentBuffer(), mvParameter::kParameterBoolean); kp != NULL; kp = it.Previous())
	{
		u32 key = kp->Key;
		for(u32 i=0; i<eventCount; ++i)
		{
			if(key == eventsToSearch[i].GetHash())
			{
				return eventsToSearch[i];
			}
		}
	}
	return fwMvId();
}

//////////////////////////////////////////////////////////////////////////

void fwMoveNetworkInterface::SetClipSet(const fwMvClipSetId &clipSetId, const fwMvClipSetVarId &clipSetVarId /* = CLIP_SET_VAR_ID_DEFAULT */)
{
	if (clipSetVarId != CLIP_SET_VAR_ID_INVALID)
	{
		// Unset any move network flags from the clip set on the network
		fwMvClipSetId oldClipSetId = GetClipSetId(clipSetVarId);
		if (oldClipSetId != CLIP_SET_ID_INVALID)
		{
			fwClipSet *clipSet = fwClipSetManager::GetClipSet(oldClipSetId);
			if (clipSet)
			{
				for (int i = 0; i < clipSet->GetMoveNetworkFlags().GetCount(); i ++)
				{
					fwMvFlagId flagId;
					flagId.SetHash(clipSet->GetMoveNetworkFlags()[i]);
					SetFlag(flagId, false);
				}

				fwClipSetManager::Release_DEPRECATED(oldClipSetId);
			}
		}

		m_clipSetIdMap.Delete(clipSetVarId);

		if(clipSetId != CLIP_SET_ID_INVALID)
		{
			m_clipSetIdMap.Insert(clipSetVarId, clipSetId);
		}

		// Set any move network flags from the clip set on the network
		fwMvClipSetId newClipSetId = GetClipSetId(clipSetVarId);
		if (newClipSetId != CLIP_SET_ID_INVALID)
		{
			fwClipSet *clipSet = fwClipSetManager::GetClipSet(newClipSetId);
			if (animVerifyf(clipSet, "ClipSet %s %u does not exist!", newClipSetId.TryGetCStr(), newClipSetId.GetHash()))
			{
				for (int i = 0; i < clipSet->GetMoveNetworkFlags().GetCount(); i ++)
				{
					fwMvFlagId flagId;
					flagId.SetHash(clipSet->GetMoveNetworkFlags()[i]);
					SetFlag(flagId, true);
				}

				fwClipSetManager::AddRef_DEPRECATED(newClipSetId);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////

void fwMoveNetworkInterface::ClearClipSets()
{

	atMap< fwMvClipSetVarId, fwMvClipSetId >::Iterator it = m_clipSetIdMap.CreateIterator();

	while (it)
	{
		// Unset any move network flags from the clip set on the network
		const fwMvClipSetId oldClipSetId = *it;
		if (oldClipSetId != CLIP_SET_ID_INVALID)
		{
			fwClipSet *clipSet = fwClipSetManager::GetClipSet(oldClipSetId);
			if (clipSet)
			{
				for (int i = 0; i < clipSet->GetMoveNetworkFlags().GetCount(); i ++)
				{
					fwMvFlagId flagId;
					flagId.SetHash(clipSet->GetMoveNetworkFlags()[i]);
					SetFlag(flagId, false);
				}

				fwClipSetManager::Release_DEPRECATED(oldClipSetId);
			}
		}
		++it;
	}

	m_clipSetIdMap.Reset();
}

////////////////////////////////////////////////////////////////////////////////

const fwMvClipSetId fwMoveNetworkInterface::GetClipSetId(const fwMvClipSetVarId &clipSetVarId /* = CLIP_SET_VAR_ID_DEFAULT */) const
{
	const fwMvClipSetId *clipSetId = m_clipSetIdMap.Access(clipSetVarId);
	return clipSetId ? *clipSetId : CLIP_SET_ID_INVALID;
}

//////////////////////////////////////////////////////////////////////////

void fwMoveNetworkInterface::SetSubNetworkBlendDuration( float duration)
{
	fwMvFloatId subnetworkBlendDurationId;
	subnetworkBlendDurationId.SetHash(mvSubNetwork::ms_BlendDurationId);
	SetFloat(subnetworkBlendDurationId, duration);
}

//////////////////////////////////////////////////////////////////////////

void fwMoveNetworkInterface::SetSubNetworkBlendOutDuration( float duration)
{
	fwMvFloatId subnetworkBlendOutDurationId;
	subnetworkBlendOutDurationId.SetHash(mvSubNetwork::ms_BlendOutDurationId);
	SetFloat(subnetworkBlendOutDurationId, duration);
}


//////////////////////////////////////////////////////////////////////////

void fwMoveNetworkInterface::UpdateAudioAndTriggeredFacialEvents(fwEntity& entity)
{
	static const crProperty::Key audioKey = crProperty::CalcKey("Audio", 0xB80C6AE8);
	static const crProperty::Key LoopingAudioKey = crProperty::CalcKey("LoopingAudio", 0xA31D8F23);
	static const crProperty::Key idKey = crProperty::CalcKey("Id", 0x1B60404D);
	static const crProperty::Key startStopKey = crProperty::CalcKey("Start", 0x84DC271F);
	static const crProperty::Key facialKey = crProperty::CalcKey("Facial",0x511E56C2);

	mvParameterBuffer::Iterator it;

	// Audio
	for(mvKeyedParameter* kp = it.Begin(GetOutputBuffer(), mvParameter::kParameterProperty, audioKey); kp != NULL; kp = it.Next())
	{
		const crProperty* prop = kp->Parameter.GetProperty();
		crPropertyAttributeHashStringAccessor accessor(prop->GetAttribute(idKey));

		if(accessor.Valid())
		{
			int hashKey = accessor.GetPtr()->GetHash();
			fwAudioEntity *aud = entity.GetAudioEntity();
			if(aud)
			{
				aud->AddAnimEvent(audAnimEvent(hashKey));
			}
			else
			{
				// for those entities that haven't got an audio entity, let the fwAudioAnimHandlerInterface manage it. 
				fwAudioEntity::HandleAnimEvent(hashKey, &entity);
			}
		}
	}

	// Looping Audio
	for(mvKeyedParameter* kp = it.Begin(GetOutputBuffer(), mvParameter::kParameterProperty, LoopingAudioKey); kp != NULL; kp = it.Next())
	{
		const crProperty* prop = kp->Parameter.GetProperty();
		crPropertyAttributeHashStringAccessor accessor(prop->GetAttribute(idKey));
		crPropertyAttributeBoolAccessor startStopAccessor(prop->GetAttribute(startStopKey));

		if(accessor.Valid())
		{
			int hashKey = accessor.GetPtr()->GetHash();
			bool startStop;
			startStopAccessor.Get(startStop);
			fwAudioEntity *aud = entity.GetAudioEntity();
			if(aud)
			{
				aud->HandleLoopingAnimEvent(audAnimEvent(hashKey, startStop));
			}
		}
	}

	// Facial
	for(mvKeyedParameter* kp = it.Begin(GetOutputBuffer(), mvParameter::kParameterProperty, facialKey); kp != NULL; kp = it.Next())
	{
		const crProperty* prop = kp->Parameter.GetProperty();
		entity.HandleFacialAnimEvent(prop);
	}
}

//////////////////////////////////////////////////////////////////////////
//	Generic fwMove
//////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

fwMove::fwMove(fwEntity& dynamicEntity)
: fwMoveNetworkInterface()
, m_MainNetwork(NULL)
, m_Director(NULL)
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	m_MotionTree = &dynamicEntity.GetAnimDirector()->GetPrePhysicsMotionTree();
//	m_MotionTree->AddRef_DEPRECATED();  // TEMP - until multi pass component destruction
}

////////////////////////////////////////////////////////////////////////////////

fwMove::~fwMove()
{
	Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::Init(fwEntity&, const mvNetworkDef& definition, const fwAnimDirector& director )
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	m_Director = &director;

	m_MainNetwork = definition.Create(m_OutputBuffers);

	m_MainNetwork->SetMoveNetworkInterface((void*)this);

	//register the main network with the parameter interface
	RegisterNetwork(m_MainNetwork);
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::PostInit()
{
	USE_MEMBUCKET(MEMBUCKET_ANIMATION);

	m_MainNetwork->Init(*m_MotionTree, m_InputBuffers);
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::Shutdown()
{
	DR_DEV_ONLY(SetShuttingDown());

	// need to shutdown the motion tree before we delete stuff that it might be referencing...
	if(m_MotionTree)
	{
//		m_MotionTree->Release();  // TEMP - until multi pass component destruction
		m_MotionTree = NULL;
	}

	if(m_MainNetwork)
	{
		m_MainNetwork->Release();
	}
	m_MainNetwork = NULL;

}

////////////////////////////////////////////////////////////////////////////////

void fwMove::Update(float /*deltaTime*/)
{
	// Does nothing right now...
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::PostUpdate()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::FinishUpdate()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::PostFinishUpdate()
{
}

////////////////////////////////////////////////////////////////////////////////

void fwMove::Dump()
{
	animAssert(m_MainNetwork);
	m_MainNetwork->Dump();
}

//////////////////////////////////////////////////////////////////////////

bool fwMove::SetSynchronizedSceneNetwork( fwMoveNetworkPlayer* UNUSED_PARAM(pPlayer), float UNUSED_PARAM(blendDuration))
{
	return false;
}

//////////////////////////////////////////////////////////////////////////

void fwMove::ClearSynchronizedSceneNetwork( fwMoveNetworkPlayer* UNUSED_PARAM(pPlayer), float UNUSED_PARAM(blendDuration), bool UNUSED_PARAM(tagSyncOut))
{

}

//////////////////////////////////////////////////////////////////////////
//DEBUG RECORDER FUNCTIONALITY
#if DR_ENABLED && __DEV
phInst* ValidateEntitySelection(phInst *pAnimInstance)
{
	if (pAnimInstance)
	{
		if (debugPlayback::PhysicsEvent::IsSelected(*pAnimInstance))
			return pAnimInstance;
	}
	return 0;
}

void fwMoveNetworkInterface::RecordSetAnimValue(const char *pValueName, float fValue) const
{
	if (mvDRTrackInterface::sm_bEnabled && mvDRTrackInterface::sm_Instance && !m_bShuttingDown)
	{
		phInst *pInst = ValidateEntitySelection(AnimDirector().GetEntity().GetCurrentPhysicsInst());
		if (pInst)
		{
			mvDRTrackInterface::sm_Instance->RecordSetAnimValue(pInst, pValueName, fValue);
		}
	}
}

void fwMoveNetworkInterface::RecordGetAnimValue(const char *pValueName, float fValue) const
{
	if (mvDRTrackInterface::sm_bEnabled && mvDRTrackInterface::sm_Instance && !m_bShuttingDown)
	{
		phInst *pInst = ValidateEntitySelection(AnimDirector().GetEntity().GetCurrentPhysicsInst());
		if (pInst)
		{
			mvDRTrackInterface::sm_Instance->RecordGetAnimValue(pInst, pValueName, fValue);
		}
	}
}

void fwMoveNetworkInterface::RecordSetAnimValue(const char *pStringName, const char *pString1id, const char *pString1, const char *pString2id, const char *pString2) const
{
	if (mvDRTrackInterface::sm_bEnabled && mvDRTrackInterface::sm_Instance && !m_bShuttingDown)
	{
		phInst *pInst = ValidateEntitySelection(AnimDirector().GetEntity().GetCurrentPhysicsInst());
		if (pInst)
		{
			mvDRTrackInterface::sm_Instance->RecordSetAnimValue(pInst, pStringName, pString1id, pString1, pString2id, pString2);
		}
	}
}

void fwMoveNetworkInterface::RecordSetAnimValue(const char *pValueName, bool bValue) const
{
	RecordSetAnimValue(pValueName, bValue ? 1.0f : 0.0f);
}

void fwMoveNetworkInterface::RecordGetAnimValue(const char *pValueName, bool bValue) const
{
	RecordGetAnimValue(pValueName, bValue ? 1.0f : 0.0f);
}

void fwMoveNetworkInterface::RecordSetAnimValue(const char *pClipName, const crClip *clip) const
{
	RecordSetAnimValue("MV:SETCLIP", "id", pClipName, "name", clip ? clip->GetName() : "<null>");
}

void fwMoveNetworkInterface::RecordSetExpressions(const char *pExpressionsName) const
{
	RecordSetAnimValue("MV:SETEXPR", "id", pExpressionsName);
}

void fwMoveNetworkInterface::RecordSetAnimValue(const char *pFilterName, const crFrameFilter *pFrameFilter) const
{
	RecordSetAnimValue("MV:SETFILTER", "id", pFilterName, "type", pFrameFilter ? pFrameFilter->GetTypeName() ? pFrameFilter->GetTypeName() : "Unknown type" : "<null>");
}
#endif //#if DR_ENABLED && __DEV

}	//namespace rage