<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="fwFacialClipSetVariationInfo">
    <enum name="m_mood" type="VariationMood" init="MoodAiming"/>
    <string name="m_clipSet" type="atHashString" />
    <float name="m_minimumResidentTime" init="5.0" min="1.0" max="20.0"/>
    <float name="m_baseMinimumPlaybackTime" init="4.0" min="1.0" max="20.0"/>
    <float name="m_baseMaximumPlaybackTime" init="8.0" min="1.0" max="20.0"/>
    <float name="m_minimumPlaybackTime" init="4.0" min="1.0" max="20.0"/>
    <float name="m_maximumPlaybackTime" init="8.0" min="1.0" max="20.0"/>
    <float name="m_blendInTime" init="0.25" min="0.0625" max="1.0"/>
    <float name="m_blendOutTime" init="0.25" min="0.0625" max="1.0"/>
  </structdef>

  <structdef type="fwFacialClipSetGroup">
    <string name="m_baseClipSet" type="atHashString" />
    <array name="m_Variations" type="atArray">
      <struct type="fwFacialClipSetVariationInfo"/>
    </array>
  </structdef>

  <structdef type="fwFacialClipSetGroupManager">
    <map name="m_facialClipSetGroups" type="atBinaryMap" key="atHashString">
      <pointer type="fwFacialClipSetGroup" policy="owner" />
    </map>
  </structdef>

  <enumdef type="VariationMood">
    <enumval name="MoodAiming"/>
    <enumval name="MoodAngry"/>
    <enumval name="MoodHappy"/>
    <enumval name="MoodInjured"/>
    <enumval name="MoodNormal"/>
    <enumval name="MoodStressed"/>
    <enumval name="MoodExcited"/>
    <enumval name="MoodFrustrated"/>
    <enumval name="MoodTalking"/>
    <enumval name="MoodDancingLow"/>
    <enumval name="MoodDancingMed"/>
    <enumval name="MoodDancingHigh"/>
    <enumval name="MoodDancingTrance"/>
  </enumdef>
  
</ParserSchema>