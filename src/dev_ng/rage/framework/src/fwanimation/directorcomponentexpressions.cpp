// 
// fwanimation/directorcomponentexpressions.cpp
// 
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved. 
// 

#include "directorcomponentexpressions.h"

#include "animdirector.h"
#include "directorcomponentcreature.h"
#include "directorcomponentmotiontree.h"

#include "crmotiontree/nodeexpression.h"
#include "crmotiontree/requestcapture.h"
#include "crmotiontree/requestexpression.h"
#include "crmotiontree/requestframe.h"
#include "crmotiontree/requestmerge.h"

namespace rage
{

#if DEBUG_EXPRESSIONS
PARAM(debugexpr, "Enable expression debugging");
#endif // DEBUG_EXPRESSIONS

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentExpressions::fwAnimDirectorComponentExpressions()
: fwAnimDirectorComponent(kComponentTypeExpressions)
, m_IsUsed(0)
{
	m_RootObserver.AddRef();
}

////////////////////////////////////////////////////////////////////////////////

fwAnimDirectorComponentExpressions::~fwAnimDirectorComponentExpressions()
{
	fwAnimDirectorComponentExpressions::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

FW_ANIM_DIRECTOR_IMPLEMENT_COMPONENT_TYPE(fwAnimDirectorComponentExpressions, 0x3cc59946, kComponentTypeExpressions, (ANIMDIRECTOR_POOL_MAX/3)+ANIMDIRECTOR_POOL_PED_MAX, 0.24f);

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::Init(fwAnimDirector& director, const crmtObserver* rootObserver)
{
	InitBase(director);

	crmtMotionTree& motionTree = director.GetMotionTreeByPhase(GetPhase());

#if DEBUG_EXPRESSIONS
	const bool debugExpressions = PARAM_debugexpr.Get();
	if(debugExpressions)
	{
		const crFrameData& frameData = director.GetFrameData();
		m_DebugFrameIn.Init(frameData);
		m_DebugFrameOut.Init(frameData);
		m_DebugFrameOverride.Init(frameData);

		crmtRequestFrame reqFrameOverride;
		crmtRequestCapture reqCaptureOut;
		crmtRequestCapture reqCaptureIn;
		crmtRequestMerge reqMerge;

		reqFrameOverride.Init(&m_DebugFrameOverride);
		reqMerge.SetSource(&reqFrameOverride, 0);
		motionTree.Request(reqMerge, rootObserver);

		reqCaptureIn.SetFrame(&m_DebugFrameIn);
		motionTree.Request(reqCaptureIn, &reqMerge.GetObserver());

		reqCaptureOut.SetFrame(&m_DebugFrameOut);
		motionTree.Request(reqCaptureOut, &reqCaptureIn.GetObserver());

		m_RootObserver.Attach(reqCaptureIn.GetObserver());
	}
	else
#endif // DEBUG_EXPRESSIONS
	if(rootObserver)
	{
		m_RootObserver.Attach(*rootObserver);
	}
	else
	{
		motionTree.AttachToRoot(m_RootObserver);
	}
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::Shutdown()
{
#if DEBUG_EXPRESSIONS
	for(int i=0; i<m_DebugObservers.GetCount(); ++i)
	{
		delete m_DebugObservers[i];
	}
	m_DebugObservers.Reset();
#endif // DEBUG_EXPRESSIONS
	m_RootObserver.Detach();

	fwAnimDirectorComponent::Shutdown();
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::ReceiveMessage(eMessageId msgId, ePhase, void*)
{
	switch(msgId)
	{
	case kMessageFrameDataChanged:
#if DEBUG_EXPRESSIONS
		if(PARAM_debugexpr.Get())
		{
			const crFrameData& frameData = GetDirector().GetFrameData();
			if(&frameData != m_DebugFrameIn.GetFrameData())
			{
				m_DebugFrameIn.ExchangeFrameData(frameData);
			}
			if(&frameData != m_DebugFrameOut.GetFrameData())
			{
				m_DebugFrameOut.ExchangeFrameData(frameData);
			}
			if(&frameData != m_DebugFrameOverride.GetFrameData())
			{
				m_DebugFrameOverride.ExchangeFrameData(frameData);
			}
		}
#endif // DEBUG_EXPRESSIONS
		break;

	default:
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentExpressions::AppendExpression(const crExpressions& expr)
{
	if(!expr.GetSignature())
	{
		return false;
	}

	crFrameDataFactory& frameDataFactory = fwAnimDirectorComponentCreature::GetFrameDataFactory();
	crCommonPool& framePool = fwAnimDirectorComponentCreature::GetCommonPool();

	fwAnimDirector& director = GetDirector();
	crmtMotionTree& motionTree = director.GetMotionTreeByPhase(GetPhase());

	crmtRequestExpression reqExpr(&expr);
	motionTree.Request(reqExpr, &m_RootObserver);
	m_RootObserver.Attach(reqExpr.GetObserver());

	m_IsUsed = (m_IsUsed << 1) | 1;

	crCreature& creature = *director.GetCreature();
	expr.InitializeCreature(creature, &frameDataFactory, &framePool);

#if DEBUG_EXPRESSIONS
	if(PARAM_debugexpr.Get())
	{
		crmtObserverTyped<crmtNodeExpression>* observerExpr = rage_new crmtObserverTyped<crmtNodeExpression>;
		observerExpr->AddRef();
		observerExpr->Attach(reqExpr.GetObserver());

		m_DebugObservers.Grow() = observerExpr;
	}
#endif // DEBUG_EXPRESSIONS

	return true;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentExpressions::RemoveExpression(const crExpressions& expr)
{
	// check motion tree isn't locked for update/compose, before potentially modifying it
	Assert(!GetDirector().GetComponentByPhase<fwAnimDirectorComponentMotionTree>(GetPhase())->IsLocked());

	crmtNode* node = m_RootObserver.GetNode();
	if(node)
	{
		bool isRoot = true;
		u32 idx = 0;
		while(node && node->GetNodeType() == crmtNode::kNodeExpression)
		{
			crmtNodeExpression* nodeExpression = static_cast<crmtNodeExpression*>(node);
			if(nodeExpression->GetExpressionPlayer().GetExpressions() == &expr)
			{
				crmtNode* nodeChild = nodeExpression->GetFirstChild();

				if(isRoot)
				{
					m_RootObserver.Attach(*nodeChild);
				}

#if DEBUG_EXPRESSIONS
				for(int i=0; i<m_DebugObservers.GetCount(); ++i)
				{
					if(m_DebugObservers[i]->GetNode() == nodeExpression)
					{
						delete m_DebugObservers[i];
						m_DebugObservers.Delete(i);
						break;
					}
				}
#endif // DEBUG_EXPRESSIONS

				nodeExpression->PromoteChild(*nodeChild);

				u32 mask = ((1<<idx)-1);
				m_IsUsed = (mask&m_IsUsed) | ((~mask)&(m_IsUsed>>1));

				return true;
			}

			isRoot = false;
			idx++;
			node = node->GetFirstChild();
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::RemoveAllExpressions()
{
	// check motion tree isn't locked for update/compose, before potentially modifying it
	crmtNode* node = m_RootObserver.GetNode();
	if(node)
	{
		Assert(!GetDirector().GetComponentByPhase<fwAnimDirectorComponentMotionTree>(GetPhase())->IsLocked());

		while(node && node->GetNodeType() == crmtNode::kNodeExpression)
		{
			crmtNodeExpression* nodeExpression = static_cast<crmtNodeExpression*>(node);
			crmtNode* nodeChild = nodeExpression->GetFirstChild();

			nodeExpression->PromoteChild(*nodeChild);

			node = nodeChild;
		}

		if(node)
		{
			m_RootObserver.Attach(*node);
		}
		else
		{
			m_RootObserver.Detach();
		}

#if DEBUG_EXPRESSIONS
		for(int i=0; i<m_DebugObservers.GetCount(); ++i)
		{
			delete m_DebugObservers[i];
		}
		m_DebugObservers.Reset();
#endif // DEBUG_EXPRESSIONS

		m_IsUsed = 0;
	}
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentExpressions::HasExpression(const crExpressions& expr) const
{
	if(m_RootObserver.IsAttached())
	{
		u32 used = 1;
		crmtNode* node = m_RootObserver.GetNode();
		while(node && node->GetNodeType() == crmtNode::kNodeExpression)
		{
			crmtNodeExpression* nodeExpression = static_cast<crmtNodeExpression*>(node);
			if(nodeExpression->GetExpressionPlayer().GetExpressions() == &expr)
			{
				m_IsUsed |= used;
				return true;
			}

			used <<= 1;
			node = node->GetFirstChild();
		}
	}

	return false;
}

////////////////////////////////////////////////////////////////////////////////

bool fwAnimDirectorComponentExpressions::HasExpressions() const
{
	return (m_RootObserver.IsAttached() && m_RootObserver.GetNode()->GetNodeType() == crmtNode::kNodeExpression);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::ClearUnused()
{
	m_IsUsed = 0;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::RemoveUnusedExpressions()
{
	crmtNode* node = m_RootObserver.GetNode();
	if(node)
	{
		u32 used = 1;
		while(node && node->GetNodeType() == crmtNode::kNodeExpression)
		{
			crmtNodeExpression* nodeExpression = static_cast<crmtNodeExpression*>(node);
			node = node->GetFirstChild();

			if(!(m_IsUsed&used))
			{
				if(nodeExpression->GetExpressionPlayer().GetExpressions())
				{
					if(RemoveExpression(*nodeExpression->GetExpressionPlayer().GetExpressions()))
					{
						continue;
					}
				}
			}
			used <<= 1;
		}
	}

	m_IsUsed = 0;
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::InitDerivedClass()
{
	static const u32 elemSize = 16;
	static const u32 numElem = 20;
	crCreatureComponentPhysical::InitPool(elemSize, numElem);
}

////////////////////////////////////////////////////////////////////////////////

void fwAnimDirectorComponentExpressions::InitShutdownClass()
{
	crCreatureComponentPhysical::ShutdownPool();
}

////////////////////////////////////////////////////////////////////////////////

} // namespace rage




