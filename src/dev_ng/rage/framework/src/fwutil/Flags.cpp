//
// fwutil/flags.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

// Class header
#include "fwutil/Flags.h"

//-------------------------------------------------------------------------
// Flag container
//-------------------------------------------------------------------------

#if __FLAG_DEBUGGING

namespace rage
{

template <typename T>
void fwFlags<T>::SetFlag(T flag)
{
	m_flags |= flag;
	static T FLAG_TO_BREAK_ON = 0;
	static void* FLAG_ADDRESS = NULL;
	if(FLAG_TO_BREAK_ON == flag && FLAG_ADDRESS == this)
	{
		Assert(0);
	}
	
}

template <typename T>
void fwFlags<T>::SetAllFlags(T flag)
{
	m_flags = flag;
	static T FLAG_TO_BREAK_ON = 0;
	static void* FLAG_ADDRESS = NULL;
	if(FLAG_TO_BREAK_ON == flag && FLAG_ADDRESS == this)
	{
		Assert(0);
	}
}

template <typename T>
bool fwFlags<T>::ClearFlag(T flag)
{
	static T FLAG_TO_BREAK_ON = 0;
	static void* FLAG_ADDRESS = NULL;
	if(FLAG_TO_BREAK_ON == flag && FLAG_ADDRESS == this)
	{
		Assert(0);
	}

	return (m_flags &= ~flag)!=0;
}

template <typename T>
void fwFlags<T>::ClearAllFlags()
{
	m_flags = 0;
}

template <typename T>
void fwFlags<T>::ChangeFlag( T flag, bool bVal )
{
	if( bVal ) 
	{ 
		SetFlag(flag); 
	} 
	else 
	{ 
		ClearFlag(flag); 
	}
}

template <typename T>
void fwFlags<T>::ToggleFlag(T flag)
{
	m_flags ^= flag;
	static T FLAG_TO_BREAK_ON = 0;
	static void* FLAG_ADDRESS = NULL;
	if(FLAG_TO_BREAK_ON == flag && FLAG_ADDRESS == this)
	{
		Assert(0);
	}
}

// Explicit instantiation of template members
template class fwFlags<s8>;
template class fwFlags<s16>;
template class fwFlags<s32>;
template class fwFlags<s64>;

} // namespace rage

#endif // __FLAG_DEBUGGING
