//
// fwutil/idgen.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef FWUTIL_IDGEN_H
#define	FWUTIL_IDGEN_H

// includes
#include "atl/map.h"


// namespaces 
namespace rage {


///////////////////////////////////////////////////////////////////////////////
// Generate a unique id from a pointer and an offset as simply as possible.
// This is just and encapsulation of what was already being done in the code;
// it is now platform independant and hopefully more readable where used.
// The size of the unique ID is not restricted to 32 bit and will adjust
// itself to the platforms native size.
//
// NOTE:
// Do not use the ID to get back to the original pointer; this may be
// promoted to a stored ID system and the ID generation scheme may change
// significantly from the one immediately below.
//
// TO USE:
// fwUniqueObjId skidUniqueID = fwIdKeyGenerator::Get( static_cast<void*>(pCar), 3 );// For tyre 3 (the right rear tyre).
//
typedef size_t fwUniqueObjId;
class fwIdKeyGenerator
{
public:
      static fwUniqueObjId Get(const void* pObjPtr, size_t ownedItemNum = 0) {return reinterpret_cast<fwUniqueObjId>(pObjPtr) + ownedItemNum;}
};


///////////////////////////////////////////////////////////////////////////////
// Helper class for fwUniqueObjectIDGenerator below.
struct fwIDInstanceInfo 
{
public:
	fwIDInstanceInfo();
	~fwIDInstanceInfo();

public:
	bool m_isValid;
	void* m_uniqueBase;
};


///////////////////////////////////////////////////////////////////////////////
// Generate a unique id from a pointer but no offset.
//
// The fwUniqueObjectIDGenerator keeps track of previously uniquely identified
// objects.
// When you ask for the ID of an object, the fwUniqueObjectIDGenerator knows
// whether to return the existing ID, or generate and remember a new ID.
// The fwUniqueObjectIDGenerator retains what ID is assigned to what object and
// vice versa.
//
// Object IDs are 32-bit numbers. Allocation starts from one, so zero is
// never a valid object ID.
// You can get back the original pointer from the ID by using the
// GetIDInstanceInfoFromID function.
// The IDs may be used as handles to pass back to script.
//
// The best thing to do is to use the THIS pointer once and then store that
// value as the location of the object could change and you don't want to lose
// your unique ID.  Don't assume use the current THIS pointer is the unique
// base.
class fwUniqueObjectIDGenerator
{
public:
	fwUniqueObjectIDGenerator();
	~fwUniqueObjectIDGenerator();

	// Use this to get or generate a unique ID.
	u32 GetID(void* uniqueBase);

	// Call this when you are done with a given ID.
	void FreeID(u32 idValue);

	// Call to free all IDs
	void Reset();

	// This is useful for debugging purposes.
	// Make sure to check the returned info for validity (m_isValid).
	fwIDInstanceInfo GetIDInstanceInfoFromID(u32 idValue);

protected:
	u32 m_lastUsedIdValue;
	atMap<void*,u32> m_baseToIdStore;
	atMap<u32,fwIDInstanceInfo> m_IdToInfoStore;
};


} // namespace rage


extern rage::fwUniqueObjectIDGenerator g_uniqueObjectIDGenerator;


#endif // FWUTIL_IDGEN_H
