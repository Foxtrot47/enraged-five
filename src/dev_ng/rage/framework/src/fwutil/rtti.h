//
// fwutil/rtti.h
//
// Copyright (C) 1999-2010 Rockstar North.  All Rights Reserved. 
//
#ifndef FWUTIL_RTTI_H_
#define FWUTIL_RTTI_H_

// Rage headers
#include "atl/hashstring.h"

////////////////////////////////////////////////////////////////////////////////

#define RTTI_CLASS_STRING(_ClassId) #_ClassId
#define RTTI_CLASS_TO_STRING(_ClassId) RTTI_CLASS_STRING(_ClassId)

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare a base class - the class string is used to generate a unique hash identifier
// NOTE: If you get a linker error on RTTIExistsCheck not existing then ensure the type has correct RTTI defined
#define DECLARE_RTTI_BASE_CLASS_WITH_HASH_TYPE(_ClassId, _HashType) \
    public: \
        virtual bool GetIsClassId(const _HashType ClassId) const { return _ClassId::GetStaticClassId() == ClassId ? true : false; } \
        template<typename _ClassType> bool GetIsClass() const { return GetIsClassId(_ClassType::GetStaticClassId()); } \
		template <typename _ClassType> _ClassType* AsClass() { return GetIsClass<_ClassType>() ? static_cast<_ClassType*>(this) : nullptr; } \
		template <typename _ClassType> _ClassType const* AsClass() const { return GetIsClass<_ClassType>() ? static_cast<_ClassType const*>(this) : nullptr; } \
        virtual const _HashType GetClassId() const { return GetStaticClassId(); } \
		virtual const _HashType GetBaseClassId() const { return _HashType::Null(); } \
        static const _HashType GetStaticClassId() { return ms_ClassId_##_ClassId; } \
		static const _HashType GetStaticBaseClassId() { return _HashType::Null(); } \
        typedef _ClassId thisclass; \
		template<typename T__> \
		static bool RTTIExistsCheck(); \
    private: \
        static _HashType ms_ClassId_##_ClassId;

#define DECLARE_RTTI_BASE_CLASS(_ClassId) DECLARE_RTTI_BASE_CLASS_WITH_HASH_TYPE(_ClassId, rage::atHashWithStringNotFinal)

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare a derived class - the class string is used to generate a unique hash identifier
// NOTE: If you get a linker error on RTTIExistsCheck not existing then ensure the type has correct RTTI defined
#define DECLARE_RTTI_DERIVED_CLASS_WITH_HASH_TYPE(_ClassId, _BaseClassId, _HashType) \
	public: \
		virtual bool GetIsClassId(const _HashType ClassId) const { return _ClassId::GetStaticClassId() == ClassId ? true : _BaseClassId::GetIsClassId(ClassId); } \
		virtual const _HashType GetClassId() const { return GetStaticClassId(); } \
		virtual const _HashType GetBaseClassId() const { return _BaseClassId::GetClassId(); } \
        static const _HashType GetStaticClassId() { return ms_ClassId_##_ClassId; } \
		static const _HashType GetStaticBaseClassId() { return _BaseClassId::GetStaticClassId(); } \
		typedef _ClassId thisclass; \
		typedef _BaseClassId superclass; \
		template<typename T__> \
		static bool RTTIExistsCheck(); \
	private: \
		static _HashType ms_ClassId_##_ClassId;

#define DECLARE_RTTI_DERIVED_CLASS(_ClassId, _BaseClassId) DECLARE_RTTI_DERIVED_CLASS_WITH_HASH_TYPE(_ClassId, _BaseClassId, rage::atHashWithStringNotFinal)

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare the class hash identifier
#define INSTANTIATE_RTTI_CLASS_WITH_HASH_TYPE(_ClassId, _HashId, _HashType) \
    _HashType _ClassId::ms_ClassId_##_ClassId(#_ClassId,_HashId); \
	template<>\
	bool _ClassId::RTTIExistsCheck<_ClassId>() {return true;}

#define INSTANTIATE_RTTI_CLASS(_ClassId, _HashId) INSTANTIATE_RTTI_CLASS_WITH_HASH_TYPE(_ClassId, _HashId, rage::atHashWithStringNotFinal)

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare the class hash identifier with namespace
#define INSTANTIATE_RTTI_NAMESPACE_CLASS_WITH_HASH_TYPE(_NamespaceId, _ClassId, _HashId, _HashType) \
    _HashType _NamespaceId::_ClassId::ms_ClassId_##_ClassId(RTTI_CLASS_TO_STRING(_NamespaceId::_ClassId), _HashId);\
 	template<>\
 	bool _NamespaceId::_ClassId::RTTIExistsCheck<_NamespaceId::_ClassId>() { return true; }

#define INSTANTIATE_RTTI_NAMESPACE_CLASS(_NamespaceId, _ClassId, _HashId) INSTANTIATE_RTTI_NAMESPACE_CLASS_WITH_HASH_TYPE(_NamespaceId, _ClassId, _HashId, rage::atHashWithStringNotFinal)

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare a base class with a specific Id
// NOTE: It's up to the user to ensure Id's are unique
// NOTE: If you get a linker error on RTTIExistsCheck not existing then ensure the type has correct RTTI defined
#define DECLARE_RTTI_BASE_CLASS_WITH_ID(_ClassId, _Id) \
	public: \
		virtual bool GetIsClassId(const u32 ClassId) const { return _ClassId::GetStaticClassId() == ClassId ? true : false; } \
		template<typename _ClassType> bool GetIsClass() const { return GetIsClassId(_ClassType::GetStaticClassId()); } \
		template <typename _ClassType> _ClassType* AsClass() { return GetIsClass<_ClassType>() ? static_cast<_ClassType*>(this) : nullptr; } \
		template <typename _ClassType> _ClassType const* AsClass() const { return GetIsClass<_ClassType>() ? static_cast<_ClassType const*>(this) : nullptr; } \
		virtual const u32 GetClassId() const { return GetStaticClassId(); } \
		static const u32 GetStaticClassId() { return ms_ClassId_##_ClassId; } \
		typedef _ClassId thisclass; \
		template<typename T__> \
		static bool RTTIExistsCheck(); \
	private: \
		static const u32 ms_ClassId_##_ClassId = _Id;

////////////////////////////////////////////////////////////////////////////////

// PURPOSE: Declare a derived class with a specific Id
// NOTE: It's up to the user to ensure Id's are unique
// NOTE: If you get a linker error on RTTIExistsCheck not existing then ensure the type has correct RTTI defined
#define DECLARE_RTTI_DERIVED_CLASS_WITH_ID(_ClassId, _BaseClassId, _Id) \
	public: \
		virtual bool GetIsClassId(const u32 ClassId) const { return _ClassId::GetStaticClassId() == ClassId ? true : _BaseClassId::GetIsClassId(ClassId); } \
		virtual const u32 GetClassId() const { return GetStaticClassId(); } \
		static const u32 GetStaticClassId() { return ms_ClassId_##_ClassId; } \
		typedef _ClassId thisclass; \
		typedef _BaseClassId superclass; \
		template<typename T__> \
		static bool RTTIExistsCheck(); \
	private: \
		static const u32 ms_ClassId_##_ClassId = _Id;

////////////////////////////////////////////////////////////////////////////////

#endif // FWUTIL_RTTI_H_
