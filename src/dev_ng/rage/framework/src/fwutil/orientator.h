// ======================
// fwutil/orientator.h
// (c) 2013 RockstarNorth
// ======================

#ifndef _FWUTIL_ORIENTATOR_H_
#define _FWUTIL_ORIENTATOR_H_

#if __BANK || __RESOURCECOMPILER

#include "file/stream.h"
#include "vector/color32.h"
#include "vectormath/classes.h"

namespace rage {

class Vec2
{
public:
	Vec2() {}
	Vec2(float x_, float y_) : x(x_), y(y_) {}
	Vec2 operator +(const Vec2& rhs) const { return Vec2(x + rhs.x, y + rhs.y); }
	Vec2 operator -(const Vec2& rhs) const { return Vec2(x - rhs.x, y - rhs.y); }
	bool operator <(const Vec2& rhs) const { return x < rhs.x || (x == rhs.x && y < rhs.y); }
	float x, y;
};

class fwOrientator
{
public:
	enum eCriteria
	{
		CRITERIA_A, // area = w*h
		CRITERIA_P, // perimeter = 2*(w + h)
		CRITERIA_M, // max side = max(w, h)
		CRITERIA_COUNT,
		CRITERIA_NONE = -1,
	};

	enum
	{
		ERROR_FLAG_TOO_FEW_POINTS       = BIT(0),
		ERROR_FLAG_TOO_MANY_POINTS      = BIT(1),
		ERROR_FLAG_TOO_MANY_HULL_POINTS = BIT(2),
	};

	fwOrientator();

	static void Init(int pointCountMax = 32678, int hullPointCountMax = 4096);
	static void Release();

	void Begin();
	void AddPoint(Vec3V_In point);
	void Process();
#if __BANK
//	void ConstructFromDrawable(const rmcDrawable* drawable, const char* path);
	void WriteToCSV(fiStream* csv, const char* path) const;
	void DebugDraw(Mat34V_In matrix, Color32 colour, eCriteria criteria) const;
#endif // __BANK

	u32      m_errorFlags;
	Vec3V    m_boundsMin;
	Vec3V    m_boundsMax;
	int      m_pointCount;
	int      m_hullPointCount;

	static Vec2* sm_pointData;
	static int   sm_pointCountMax;
	static Vec2* sm_hullPointData;
	static int   sm_hullPointCountMax;

	class fwBestFit
	{
	public:
		void Update(float value, Vec3V_In basisX, Vec3V_In basisY, Vec3V_In boundsMin, Vec3V_In boundsMax);

		float m_valueBase;
		float m_value;
		Vec3V m_basisX;
		Vec3V m_basisY;
		Vec3V m_boundsMin;
		Vec3V m_boundsMax;
	};

	fwBestFit m_bestFit[CRITERIA_COUNT];
};

} // namespace rage

#endif // __BANK || __RESOURCECOMPILER
#endif // _FWUTIL_ORIENTATOR_H_
