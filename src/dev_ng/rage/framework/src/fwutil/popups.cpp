// ===========================
// fwutil/popups.cpp
// (c) 2014 Rockstar San Diego
// ===========================

#if !__FINAL

#include "system/param.h"

#include "fwutil/popups.h"

namespace rage {

XPARAM(nopopups);

FASTRETURNCHECK(bool) ForceNoPopupsBegin(bool nopopups)
{
	const bool prev = PARAM_nopopups.Get();
	PARAM_nopopups.Set(nopopups ? "" : NULL);
	return prev;
}

void ForceNoPopupsEnd(bool prev)
{
	PARAM_nopopups.Set(prev ? "" : NULL);
}

ForceNoPopupsAuto::ForceNoPopupsAuto(bool nopopups)
{
	m_nopopups = ForceNoPopupsBegin(nopopups);
}

ForceNoPopupsAuto::~ForceNoPopupsAuto()
{
	ForceNoPopupsEnd(m_nopopups);
}

} // namespace rage

#endif // !__FINAL
