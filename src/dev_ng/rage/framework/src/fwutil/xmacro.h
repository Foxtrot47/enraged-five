// ======================
// fwutil/xmacro.h
// (c) 2011 RockstarNorth
// ======================

#ifndef _FWUTIL_XMACRO_H_
#define _FWUTIL_XMACRO_H_

// =============================================================================================== //
// MACROS
// =============================================================================================== //

#if GTA_VERSION
	#define GTA_ONLY(...) __VA_ARGS__
	#define RDR_ONLY(...)
#elif RDR_VERSION
	#define GTA_ONLY(...)
	#define RDR_ONLY(...) __VA_ARGS__
#else
	#define GTA_ONLY(...)
	#define RDR_ONLY(...)
#endif

#if __GAMETOOL
	#define GAMETOOL_ONLY(...) __VA_ARGS__
#else
	#define GAMETOOL_ONLY(...)
#endif

// http://en.wikipedia.org/wiki/C_preprocessor#X-Macros
// http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1124.pdf  (page 154)

#define FOREACH(DEF) FOREACH_##DEF(DEF)
#define DEF_TERMINATOR // terminator for multi-line FOREACH defines
#define COMMA ,
#define STRING(tok) #tok
#define __COMMENT(x)
#define INDEX_NONE (-1)
#if 1 || __XENON
	#define UNUSED_VAR(var) (void)var
#else
	#define UNUSED_VAR(var) (void)sizeof(var) // new XDK has issues with this apparently .. could use this on PS3 perhaps
#endif

#if __BANK
	#define RELEASE_CONST
#else
	#define RELEASE_CONST const
#endif

// Conditional output
#define BANK_SWITCH_ASSERT (0 && !__SPU)
#define DEV_SWITCH_ASSERT (0 && !__SPU)

#if __BANK
#if BANK_SWITCH_ASSERT 
	#include "vectormath/classes.h"

	template <typename T, typename TT>
	__forceinline T bankSwitchTest(const T &a, const TT &b, const char* astr, const char* bstr)
	{
		Assertf(a == b, "bankSwitchTest: a=[%s],b=[%s]", astr, bstr);
		return a;
	}

	__forceinline rage::u32 bankSwitchTest(rage::u32 a, rage::u32 b, const char* astr, const char* bstr)
	{
		Assertf(a == b, "bankSwitchTest: a=[%s](%d),b=[%s](%d)", astr, bstr, a, b);
		return a;
	}

	__forceinline int bankSwitchTest(int a, int b, const char* astr, const char* bstr)
	{
		Assertf(a == b, "bankSwitchTest: a=[%s](%d),b=[%s](%d)", astr, bstr, a, b);
		return a;
	}

	__forceinline rage::ScalarV_Out bankSwitchTest(rage::ScalarV_In a, rage::ScalarV_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "bankSwitchTest: a=[%s](%f),b=[%s](%f)", astr, bstr, a.Getf(), b.Getf());
		return a;
	}

	__forceinline rage::Vec2V_Out bankSwitchTest(rage::Vec2V_In a, rage::Vec2V_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "bankSwitchTest: a=[%s](%f,%f),b=[%s](%f,%f)", astr, bstr, VEC2V_ARGS(a), VEC2V_ARGS(b));
		return a;
	}

	__forceinline rage::Vec3V_Out bankSwitchTest(rage::Vec3V_In a, rage::Vec3V_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "bankSwitchTest: a=[%s](%f,%f,%f),b=[%s](%f,%f,%f)", astr, bstr, VEC3V_ARGS(a), VEC3V_ARGS(b));
		return a;
	}

	__forceinline rage::Vec4V_Out bankSwitchTest(rage::Vec4V_In a, rage::Vec4V_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "bankSwitchTest: a=[%s](%f,%f,%f,%f),b=[%s](%f,%f,%f,%f)", astr, bstr, VEC4V_ARGS(a), VEC4V_ARGS(b));
		return a;
	}

	#define BANK_SWITCH_NT(_if_BANK_,_else_) _if_BANK_
	#define BANK_SWITCH(_if_BANK_,_else_) bankSwitchTest((_if_BANK_),(_else_),#_if_BANK_,#_else_)

#else
	#define BANK_SWITCH_NT(_if_BANK_,_else_) _if_BANK_
	#define BANK_SWITCH(_if_BANK_,_else_) _if_BANK_
#endif
#else
	#define BANK_SWITCH_NT(_if_BANK_,_else_) _else_
	#define BANK_SWITCH(_if_BANK_,_else_) _else_
#endif
#if __BANK
	#define BANK_PARAM(x) ,x
#else
	#define BANK_PARAM(x)
#endif

#if __DEV
#if DEV_SWITCH_ASSERT
	#include "vectormath/classes.h"
#if __WIN32	
#  pragma warning (push)
#  pragma warning (disable: 4389)
#  pragma warning (disable: 4805)
#endif

	template <typename T, typename TT>
	__forceinline T devSwitchTest(const T &a, const TT &b, const char* astr, const char* bstr)
	{
		Assertf(a == b, "devSwitchTest: a=[%s],b=[%s]", astr, bstr);
		return a;
	}

	__forceinline rage::u32 devSwitchTest(rage::u32 a, rage::u32 b, const char* astr, const char* bstr)
	{
		Assertf(a == b, "devSwitchTest: a=[%s](%d),b=[%s](%d)", astr, bstr, a, b);
		return a;
	}

	__forceinline int devSwitchTest(int a, int b, const char* astr, const char* bstr)
	{
		Assertf(a == b, "devSwitchTest: a=[%s](%d),b=[%s](%d)", astr, bstr, a, b);
		return a;
	}

	__forceinline rage::ScalarV_Out devSwitchTest(rage::ScalarV_In a, rage::ScalarV_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "devSwitchTest: a=[%s](%f),b=[%s](%f)", astr, bstr, a.Getf(), b.Getf());
		return a;
	}

	__forceinline rage::Vec2V_Out devSwitchTest(rage::Vec2V_In a, rage::Vec2V_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "devSwitchTest: a=[%s](%f,%f),b=[%s](%f,%f)", astr, bstr, VEC2V_ARGS(a), VEC2V_ARGS(b));
		return a;
	}

	__forceinline rage::Vec3V_Out devSwitchTest(rage::Vec3V_In a, rage::Vec3V_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "devSwitchTest: a=[%s](%f,%f,%f),b=[%s](%f,%f,%f)", astr, bstr, VEC3V_ARGS(a), VEC3V_ARGS(b));
		return a;
	}

	__forceinline rage::Vec4V_Out devSwitchTest(rage::Vec4V_In a, rage::Vec4V_In b, const char* astr, const char* bstr)
	{
		Assertf(IsEqualAll(a,b), "devSwitchTest: a=[%s](%f,%f,%f,%f),b=[%s](%f,%f,%f,%f)", astr, bstr, VEC4V_ARGS(a), VEC4V_ARGS(b));
		return a;
	}

#if __WIN32	
#  pragma warning (pop)	
#endif

	#define DEV_SWITCH_NT(_if_DEV_,_else_) _if_DEV_
	#define DEV_SWITCH(_if_DEV_,_else_) devSwitchTest((_if_DEV_),(_else_),#_if_DEV_,#_else_)
#else
	#define DEV_SWITCH_NT(_if_DEV_,_else_) _if_DEV_
	#define DEV_SWITCH(_if_DEV_,_else_) _if_DEV_
#endif
#else
	#define DEV_SWITCH_NT(_if_DEV_,_else_) _else_
	#define DEV_SWITCH(_if_DEV_,_else_) _else_
#endif

#if __XENON
	#define XENON_SWITCH(_if_XENON_,_else_) _if_XENON_
#else
	#define XENON_SWITCH(_if_XENON_,_else_) _else_
#endif

#if __PS3
	#define PS3_SWITCH(_if_PS3_,_else_) _if_PS3_
#else
	#define PS3_SWITCH(_if_PS3_,_else_) _else_
#endif

#if __WIN32PC
	#define WIN32PC_SWITCH(_if_WIN32PC_,_else_) _if_WIN32PC_
#else
	#define WIN32PC_SWITCH(_if_WIN32PC_,_else_) _else_
#endif

#if __XENON
	#define XENON_PS3_SWITCH(_if_XENON_,_if_PS3_,_else_) _if_XENON_
#elif __PS3
	#define XENON_PS3_SWITCH(_if_XENON_,_if_PS3_,_else_) _if_PS3_
#else
	#define XENON_PS3_SWITCH(_if_XENON_,_if_PS3_,_else_) _else_
#endif

// ================================================================================================

// e.g. REP1_SEPARATOR(float3 dither,;); ShadowRecvDither(REP1_COMMA(dither), tex, aniso, vPos);

#define REP1_SEPARATOR(DEF,separator)                                         DEF##0
#define REP2_SEPARATOR(DEF,separator) REP1_SEPARATOR(DEF,separator) separator DEF##1
#define REP3_SEPARATOR(DEF,separator) REP2_SEPARATOR(DEF,separator) separator DEF##2
#define REP4_SEPARATOR(DEF,separator) REP3_SEPARATOR(DEF,separator) separator DEF##3
#define REP5_SEPARATOR(DEF,separator) REP4_SEPARATOR(DEF,separator) separator DEF##4
#define REP6_SEPARATOR(DEF,separator) REP5_SEPARATOR(DEF,separator) separator DEF##5
#define REP7_SEPARATOR(DEF,separator) REP6_SEPARATOR(DEF,separator) separator DEF##6
#define REP8_SEPARATOR(DEF,separator) REP7_SEPARATOR(DEF,separator) separator DEF##7
#define REP9_SEPARATOR(DEF,separator) REP8_SEPARATOR(DEF,separator) separator DEF##8
#define REP10_SEPARATOR(DEF,separator) REP9_SEPARATOR(DEF,separator) separator DEF##9
#define REP11_SEPARATOR(DEF,separator) REP10_SEPARATOR(DEF,separator) separator DEF##10
#define REP12_SEPARATOR(DEF,separator) REP11_SEPARATOR(DEF,separator) separator DEF##11
#define REP13_SEPARATOR(DEF,separator) REP12_SEPARATOR(DEF,separator) separator DEF##12
#define REP14_SEPARATOR(DEF,separator) REP13_SEPARATOR(DEF,separator) separator DEF##13
#define REP15_SEPARATOR(DEF,separator) REP14_SEPARATOR(DEF,separator) separator DEF##14
#define REP16_SEPARATOR(DEF,separator) REP15_SEPARATOR(DEF,separator) separator DEF##15

#define REP1_COMMA(DEF)                  DEF##0
#define REP2_COMMA(DEF) REP1_COMMA(DEF), DEF##1
#define REP3_COMMA(DEF) REP2_COMMA(DEF), DEF##2
#define REP4_COMMA(DEF) REP3_COMMA(DEF), DEF##3
#define REP5_COMMA(DEF) REP4_COMMA(DEF), DEF##4
#define REP6_COMMA(DEF) REP5_COMMA(DEF), DEF##5
#define REP7_COMMA(DEF) REP6_COMMA(DEF), DEF##6
#define REP8_COMMA(DEF) REP7_COMMA(DEF), DEF##7
#define REP9_COMMA(DEF) REP8_COMMA(DEF), DEF##8
#define REP10_COMMA(DEF) REP9_COMMA(DEF), DEF##9
#define REP11_COMMA(DEF) REP10_COMMA(DEF), DEF##10
#define REP12_COMMA(DEF) REP11_COMMA(DEF), DEF##11
#define REP13_COMMA(DEF) REP12_COMMA(DEF), DEF##12
#define REP14_COMMA(DEF) REP13_COMMA(DEF), DEF##13
#define REP15_COMMA(DEF) REP14_COMMA(DEF), DEF##14
#define REP16_COMMA(DEF) REP15_COMMA(DEF), DEF##15

#define REP1_SEPARATOR_FOREACH(def,separator)                                                 DEF_##def(0)
#define REP2_SEPARATOR_FOREACH(def,separator) REP1_SEPARATOR_FOREACH(def,separator) separator DEF_##def(1)
#define REP3_SEPARATOR_FOREACH(def,separator) REP2_SEPARATOR_FOREACH(def,separator) separator DEF_##def(2)
#define REP4_SEPARATOR_FOREACH(def,separator) REP3_SEPARATOR_FOREACH(def,separator) separator DEF_##def(3)
#define REP5_SEPARATOR_FOREACH(def,separator) REP4_SEPARATOR_FOREACH(def,separator) separator DEF_##def(4)
#define REP6_SEPARATOR_FOREACH(def,separator) REP5_SEPARATOR_FOREACH(def,separator) separator DEF_##def(5)
#define REP7_SEPARATOR_FOREACH(def,separator) REP6_SEPARATOR_FOREACH(def,separator) separator DEF_##def(6)
#define REP8_SEPARATOR_FOREACH(def,separator) REP7_SEPARATOR_FOREACH(def,separator) separator DEF_##def(7)
#define REP9_SEPARATOR_FOREACH(def,separator) REP8_SEPARATOR_FOREACH(def,separator) separator DEF_##def(8)
#define REP10_SEPARATOR_FOREACH(def,separator) REP9_SEPARATOR_FOREACH(def,separator) separator DEF_##def(9)
#define REP11_SEPARATOR_FOREACH(def,separator) REP10_SEPARATOR_FOREACH(def,separator) separator DEF_##def(10)
#define REP12_SEPARATOR_FOREACH(def,separator) REP11_SEPARATOR_FOREACH(def,separator) separator DEF_##def(11)
#define REP13_SEPARATOR_FOREACH(def,separator) REP12_SEPARATOR_FOREACH(def,separator) separator DEF_##def(12)
#define REP14_SEPARATOR_FOREACH(def,separator) REP13_SEPARATOR_FOREACH(def,separator) separator DEF_##def(13)
#define REP15_SEPARATOR_FOREACH(def,separator) REP14_SEPARATOR_FOREACH(def,separator) separator DEF_##def(14)
#define REP16_SEPARATOR_FOREACH(def,separator) REP15_SEPARATOR_FOREACH(def,separator) separator DEF_##def(15)

#define REP1_COMMA_FOREACH(def)                            DEF_##def(0)
#define REP2_COMMA_FOREACH(def) REP1_COMMA_DEF_##def(def), DEF_##def(1)
#define REP3_COMMA_FOREACH(def) REP2_COMMA_DEF_##def(def), DEF_##def(2)
#define REP4_COMMA_FOREACH(def) REP3_COMMA_DEF_##def(def), DEF_##def(3)
#define REP5_COMMA_FOREACH(def) REP4_COMMA_DEF_##def(def), DEF_##def(4)
#define REP6_COMMA_FOREACH(def) REP5_COMMA_DEF_##def(def), DEF_##def(5)
#define REP7_COMMA_FOREACH(def) REP6_COMMA_DEF_##def(def), DEF_##def(6)
#define REP8_COMMA_FOREACH(def) REP7_COMMA_DEF_##def(def), DEF_##def(7)
#define REP9_COMMA_FOREACH(def) REP8_COMMA_DEF_##def(def), DEF_##def(8)
#define REP10_COMMA_FOREACH(def) REP9_COMMA_DEF_##def(def), DEF_##def(9)
#define REP11_COMMA_FOREACH(def) REP10_COMMA_DEF_##def(def), DEF_##def(10)
#define REP12_COMMA_FOREACH(def) REP11_COMMA_DEF_##def(def), DEF_##def(11)
#define REP13_COMMA_FOREACH(def) REP12_COMMA_DEF_##def(def), DEF_##def(12)
#define REP14_COMMA_FOREACH(def) REP13_COMMA_DEF_##def(def), DEF_##def(13)
#define REP15_COMMA_FOREACH(def) REP14_COMMA_DEF_##def(def), DEF_##def(14)
#define REP16_COMMA_FOREACH(def) REP15_COMMA_DEF_##def(def), DEF_##def(15)

#define REP1_SEPARATOR_FOREACH_ARGS(def,separator,args)                                                           DEF_##def(0,args)
#define REP2_SEPARATOR_FOREACH_ARGS(def,separator,args) REP1_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(1,args)
#define REP3_SEPARATOR_FOREACH_ARGS(def,separator,args) REP2_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(2,args)
#define REP4_SEPARATOR_FOREACH_ARGS(def,separator,args) REP3_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(3,args)
#define REP5_SEPARATOR_FOREACH_ARGS(def,separator,args) REP4_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(4,args)
#define REP6_SEPARATOR_FOREACH_ARGS(def,separator,args) REP5_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(5,args)
#define REP7_SEPARATOR_FOREACH_ARGS(def,separator,args) REP6_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(6,args)
#define REP8_SEPARATOR_FOREACH_ARGS(def,separator,args) REP7_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(7,args)
#define REP9_SEPARATOR_FOREACH_ARGS(def,separator,args) REP8_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(8,args)
#define REP10_SEPARATOR_FOREACH_ARGS(def,separator,args) REP9_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(9,args)
#define REP11_SEPARATOR_FOREACH_ARGS(def,separator,args) REP10_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(10,args)
#define REP12_SEPARATOR_FOREACH_ARGS(def,separator,args) REP11_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(11,args)
#define REP13_SEPARATOR_FOREACH_ARGS(def,separator,args) REP12_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(12,args)
#define REP14_SEPARATOR_FOREACH_ARGS(def,separator,args) REP13_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(13,args)
#define REP15_SEPARATOR_FOREACH_ARGS(def,separator,args) REP14_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(14,args)
#define REP16_SEPARATOR_FOREACH_ARGS(def,separator,args) REP15_SEPARATOR_FOREACH_ARGS(def,separator,args) separator DEF_##def(15,args)

#define _ARG0(a0,               ...) a0
#define _ARG1(a0,a1,            ...) a1
#define _ARG2(a0,a1,a2,         ...) a2
#define _ARG3(a0,a1,a2,a3,      ...) a3
#define _ARG4(a0,a1,a2,a3,a4,   ...) a4
#define _ARG5(a0,a1,a2,a3,a4,a5,...) a5

#define JOIN(a,b) a##b

#define ARG(i,args) _ARG##i args

#endif // _FWUTIL_XMACRO_H_
