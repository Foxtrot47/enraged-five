//
// fwutil/flags.h
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#ifndef FWUTIL_FLAGS_H
#define FWUTIL_FLAGS_H

#define __FLAG_DEBUGGING 0

namespace rage
{

//-------------------------------------------------------------------------
// Flag container
//-------------------------------------------------------------------------
template<typename T>
class fwFlags
{
public:

	fwFlags() : m_flags(0) {}
	fwFlags(T flags) : m_flags(flags) {}

#if !__FLAG_DEBUGGING
	void SetFlag(T flag) { m_flags |= flag; }
	void SetAllFlags(T flag = ~(T())) { m_flags = flag; }
	bool ClearFlag(T flag) { return (m_flags &= ~flag) != 0; }
	void ClearAllFlags() { m_flags = 0; }
	void ChangeFlag(T flag, bool bVal) { if( bVal ) { SetFlag(flag); } else { ClearFlag(flag); } }
	void ToggleFlag(T flag) { m_flags ^= flag; }
#else
	void SetFlag(T flag);
	void SetAllFlags(T flag = ~(T()));
	bool ClearFlag(T flag);
	void ClearAllFlags();
	void ChangeFlag(T flag, bool bVal);
	void ToggleFlag(T flag);
#endif // !__FLAG_DEBUGGING

	bool IsFlagSet(T flag) const { return (m_flags & flag)!=0; }
	bool AreAllFlagsSet(T flags) const { return (m_flags & flags)==flags; }
	T GetAllFlags() const { return m_flags; }

	// Operators
	operator T() const { return m_flags; }

private:
	T m_flags;
};

typedef fwFlags<u8>  fwFlags8;
typedef fwFlags<u16> fwFlags16;
typedef fwFlags<s32> fwFlags32;
typedef fwFlags<u64> fwFlags64;

}  // namespace rage

#endif // FWUTIL_FLAGS_H
