//
// fwutil/idgen.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

// includes
#include "fwutil/idgen.h"


// namespaces
using namespace rage;


// global variables
fwUniqueObjectIDGenerator g_uniqueObjectIDGenerator;


///////////////////////////////////////////////////////////////////////////////
// fwIDInstanceInfo
fwIDInstanceInfo::fwIDInstanceInfo()
	:
	m_isValid(false),
	m_uniqueBase(0)
{
	;
}

fwIDInstanceInfo::~fwIDInstanceInfo()
{
	m_isValid=false;
	m_uniqueBase=0;
}


///////////////////////////////////////////////////////////////////////////////
// fwUniqueObjectIDGenerator
fwUniqueObjectIDGenerator::fwUniqueObjectIDGenerator()
	:
	m_lastUsedIdValue(0)
{
	// Make sure the stores are empty.
	m_baseToIdStore.Kill();
	m_IdToInfoStore.Kill();
}

fwUniqueObjectIDGenerator::~fwUniqueObjectIDGenerator()
{
	m_baseToIdStore.Kill();
	m_IdToInfoStore.Kill();
}

// Reset id generator, remove all IDs
void fwUniqueObjectIDGenerator::Reset()
{
	m_baseToIdStore.Kill();
	m_IdToInfoStore.Kill();
}


// Use this to get or generate a unique ID.
u32 fwUniqueObjectIDGenerator::GetID (void* uniqueBase)
{
	// See if the base already has an ID.  We do this by
	// trying to find the unique base in the base store.
	u32* pEntryId = m_baseToIdStore.Access(uniqueBase);
	if( pEntryId )
	{
		// The base already has a matching ID so it has already been
		// assigned an ID; return the existing ID to the user.
		return *pEntryId;
	}
	else
	{
		// The base does not seem to have a matching ID yet, so we
		// need to generate one for it and store the results for next time.

		// Generate a new entry
		u32 newId = 0;
		bool freeIdFound = false;
		while(!freeIdFound)
		{
			// Go to the next possible ID value, but
			// don't allow zero to ever be used.
			++m_lastUsedIdValue;
			if( m_lastUsedIdValue == 0 ){ ++m_lastUsedIdValue; }

			// Check if the ID is actually free.
			fwIDInstanceInfo* pEntry = m_IdToInfoStore.Access(m_lastUsedIdValue);
			if(!pEntry)
			{
				// This current candidate ID is free, so use it.
				freeIdFound = true;
				newId = m_lastUsedIdValue;

				// Create the new ID info.
				fwIDInstanceInfo			newEntry;
				newEntry.m_isValid		= true;
				newEntry.m_uniqueBase	= uniqueBase;

				// Insert the new entry into the stores.
				m_baseToIdStore.Insert(uniqueBase, newId);
				m_IdToInfoStore.Insert(newId, newEntry);
			}
		}

		// Return the new ID (this will return 0 if none could be found).
		return newId;
	}
}

// Call this when you are done with a given ID.
void fwUniqueObjectIDGenerator::FreeID (u32 idValue)
{
	// Try to get the base, so we can delete the base store entry.
	fwIDInstanceInfo* pEntry = m_IdToInfoStore.Access(idValue);
	if( pEntry )
	{
		void* base = pEntry->m_uniqueBase;

		// Delete the entries in the stores.
		m_baseToIdStore.Delete(base);
	}

	m_IdToInfoStore.Delete(idValue);
}


// This is useful for debugging purposes.
// Make sure to check the returned info for validity (m_isValid).
fwIDInstanceInfo fwUniqueObjectIDGenerator::GetIDInstanceInfoFromID(u32 idValue)
{
	fwIDInstanceInfo* pFoundInfo = m_IdToInfoStore.Access(idValue);
	if( pFoundInfo )
	{
        return *pFoundInfo;
	}
	else
	{
		fwIDInstanceInfo badInfo;
		badInfo.m_isValid = false;
		badInfo.m_uniqueBase = NULL;

		return badInfo;
	}
}
