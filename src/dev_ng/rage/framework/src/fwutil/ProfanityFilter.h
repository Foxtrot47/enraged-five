//
// ProfanityFilter.h 
//
// Checks text for the existence of reserved words
//
#ifndef INC_PROFANITY_FILTER_H_
#define INC_PROFANITY_FILTER_H_

// Rage headers
#include "atl/array.h"
#include "atl/string.h"
#include "parser/macros.h"
#include "string/string.h"

namespace rage {

//! TODO - Not really just a profanity filter. Either re-name to fwTextValidation or refactor into seperate systems
class fwProfanityFilter
{
public:
	enum eRESULT
	{
		RESULT_NOT_READY,

		RESULT_VALID,
		RESULT_PROFANE,
		RESULT_RESERVED_WORD,
        RESULT_UNSUPPORTED_CHARACTER,

		RESULT_COUNT
	};

	fwProfanityFilter();

	static fwProfanityFilter& GetInstance() {return sm_Instance;}

	void Init();
    eRESULT CheckForReservedTermsOnly( const char* pString ) const;
    eRESULT CheckForUnsupportedFileSystemCharactersOnly( char const * pString, atString* const out_charactersFound = NULL ) const;
	eRESULT CheckForProfanityAndReservedTerms( const char* pString, bool const checkPlatformReservedWords = false, const char** profaneWord = NULL) const;
	char* AsteriskProfanity(char* pString) const;
	bool IsAvailable() const { return !m_profaneTerms.empty(); }

private:
    bool CheckIsReservedTerm( const char* pString ) const;
    bool CheckContainsInvalidFileSystemCharacters( const char* pString, atString* const out_charactersFound ) const;
	void SubstituteCharacters(char* pDest, const char* pSrc) const;
	void AsteriskTerm(char* pString, int offset, int length) const;
	int CheckForProfaneTermsRaw(const char* pString, const char** ppTerm = NULL) const;
	void AddReservedTerm(const char* pTerm);
	void Finalise();

	atArray<ConstString> m_profaneTerms;
    atArray<ConstString> m_reservedTerms;
    atArray<ConstString> m_reservedFSCharacters;

	static fwProfanityFilter sm_Instance;

	PAR_SIMPLE_PARSABLE;
};

} //namespace rage

#endif //INC_PROFANITY_FILTER_H_