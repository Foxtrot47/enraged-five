<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="fwProfanityFilter">
  <array name="m_profaneTerms" type="atArray">
    <string type="ConstString"/> 
  </array>
  <array name="m_reservedTerms" type="atArray">
    <string type="ConstString"/>
  </array>
  <array name="m_reservedFSCharacters" type="atArray">
    <string type="ConstString"/>
  </array>
</structdef>

</ParserSchema>