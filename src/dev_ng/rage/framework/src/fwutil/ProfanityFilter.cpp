//
// ProfanityFilter.cpp
//
// Checks text for the existence of reserved words
//
using namespace rage;

#include "ProfanityFilter.h"
#include "ProfanityFilter_parser.h"

#include "file/limits.h"
#include "fwsys/fileExts.h"
#include "parser/psofile.h"

namespace rage {

	fwProfanityFilter fwProfanityFilter::sm_Instance;

fwProfanityFilter::fwProfanityFilter() {}

void fwProfanityFilter::Init()
{
	// Load PSO
	char metadataFilename[RAGE_MAX_PATH];
	formatf(metadataFilename, RAGE_MAX_PATH, "platform:/data/ui/dictionary.%s", META_FILE_EXT);

	psoFile* pPsoFile = psoLoadFile(metadataFilename, PSOLOAD_PREP_FOR_PARSER_LOADING, atFixedBitSet32().Set(psoFile::REQUIRE_CHECKSUM));
	if(Verifyf(pPsoFile, "Failed to load the metadata PSO file (%s)", metadataFilename))
	{
		ASSERT_ONLY(const bool bHasSucceeded = ) psoInitAndLoadObject(*pPsoFile, *this);
		Assertf(bHasSucceeded, "Failed to parse the metadata PSO file (%s)", metadataFilename);
	}

	delete pPsoFile;

	Finalise();
}

fwProfanityFilter::eRESULT fwProfanityFilter::CheckForReservedTermsOnly( const char* pString ) const
{
	fwProfanityFilter::eRESULT const c_result = CheckIsReservedTerm( pString ) ? RESULT_RESERVED_WORD : RESULT_VALID;
	return c_result;
}

fwProfanityFilter::eRESULT fwProfanityFilter::CheckForUnsupportedFileSystemCharactersOnly( char const * pString, atString* const out_charactersFound ) const
{
    fwProfanityFilter::eRESULT const c_result = CheckContainsInvalidFileSystemCharacters( pString, out_charactersFound ) ? RESULT_UNSUPPORTED_CHARACTER : RESULT_VALID;
    return c_result;
}

fwProfanityFilter::eRESULT fwProfanityFilter::CheckForProfanityAndReservedTerms( const char* pString, bool const checkPlatformReservedWords, const char** profaneWord) const
{
	int count = StringLength(pString);
	char* pString2 = Alloca(char, count+1);
	char* pCopy = pString2;
	int i;

	// if reserved terms array is empty then the dictionary never got loaded assume text contains reserved text
	if(m_profaneTerms.empty())
		return RESULT_NOT_READY;

	if( checkPlatformReservedWords && CheckIsReservedTerm( pString ) )
		return RESULT_RESERVED_WORD;

	//remove spaces, non alpha chars, and replace characters
	SubstituteCharacters(pString2, pString);

	if(  CheckForProfaneTermsRaw(pString2, profaneWord) != -1)
	    return RESULT_PROFANE;

	// check for situation where spaces are put in between every letter
	count = StringLength(pString2);
	char* pString3 = Alloca(char, count+1);
	pCopy = pString3;

	//remove spaces
	for(i=0; i<count; i++)
	{
		char c = *pString2;
		char c2 = *(pString2+2);
		pString2++;

		if(i < count-1 && c == ' ' && (c2 == ' ' || c2 == '\0'))
			continue;
		*pCopy++ = c;
	}
	*pCopy = '\0';

	return (CheckForProfaneTermsRaw(pString3, profaneWord) != -1) ? RESULT_PROFANE : RESULT_VALID;
}

// PURPOSE: takes a string and writes asterisks over any profane terms
char* fwProfanityFilter::AsteriskProfanity(char* pString) const
{
	int count = StringLength(pString);
	char* pString2 = Alloca(char, count+1);

	// if reserved terms array is empty then the dictionary never got loaded assume text contains reserved text
	if(m_profaneTerms.empty())
		return pString;

	//non alpha chars, and replace characters
	SubstituteCharacters(pString2, pString);

	int offset = 0;
	while(true)
	{
		const char* pProfanity;
		int offset2 = CheckForProfaneTermsRaw(pString2+offset, &pProfanity);
		if(offset2 == -1)
			break;

		AsteriskTerm(pString, offset2+offset, StringLength(pProfanity));

		offset += offset2 + 1;
	}
	return pString;
}


// PURPOSE: Swap out numbers for characters to catch words that may have been hidden
void fwProfanityFilter::SubstituteCharacters(char* pDest, const char* pSrc) const
{
	int count = StringLength(pSrc);
	//remove non alpha chars, and replace characters
	for(int i=0; i<count; i++)
	{
		char c = *pSrc;
		pSrc++;
		// Can treat utf8 the same as 7 bit ASCII as any continuing byte has the top bit set
		switch(c)
		{
		case '1':
		case '!':
		case '|':
			c = 'i';
			break;
		case '@':
		case '2':
		case '4':
			c = 'a';
			break;
		case '3':
			c = 'e';
			break;
		case '0':
			c = 'o';
			break;
		case '5':
			c = 's';
			break;
		case '6':
		case '9':
			c = 'g';
			break;
		case '8':
			c = 'b';
			break;
		case '*':
			c = ' ';
			break;
		default:
			break;
		}

		if (c < '0' && c != ' ')
			continue;
		else if (c >= 'A' && c <= 'Z')
			c = c + 32;
		*pDest++ = c;
	}
	*pDest = '\0';
}

//PURPOSE: write asterisks over profane terms
void fwProfanityFilter::AsteriskTerm(char* pString, int offset, int length) const
{
	int count = StringLength(pString);
	int offset2 = 0;

	for(int i=0; i < count; i++)
	{
		if (*pString >= '0' || *pString == ' ' || *pString == '*')
		{
			offset2++;
			if(offset2 > offset)
			{
				*pString = '*';
				length--;
				if(length == 0)
					break;
			}
		}
		pString++;
	}
}

// PURPOSE: check if given string is a case-insensitive complete match for a given string
bool fwProfanityFilter::CheckIsReservedTerm( const char* pString ) const
{
	bool result = false;

	int const c_characterCountToCheck = StringLength( pString );
	int wordIndex = 0;

	int const c_wordCount = m_reservedTerms.GetCount();
	while( !result && pString && wordIndex < c_wordCount )
	{
		ConstString const& currentWord = m_reservedTerms[ wordIndex ];
		char const * const currentWordString = currentWord.c_str();

		int const c_characterCount = StringLength( currentWordString );
		result = c_characterCountToCheck == c_characterCount && strnicmp( pString, currentWordString, c_characterCount ) == 0;

		++wordIndex;
	}

	return result;
}

bool fwProfanityFilter::CheckContainsInvalidFileSystemCharacters( const char* pString, atString* const out_charactersFound ) const
{
    bool result = false;

    if( out_charactersFound )
    {
        out_charactersFound->Reset();
    }

    int const c_reservedCharacterCount = m_reservedFSCharacters.GetCount();
    for( int reservedCharacterIndex = 0; pString && reservedCharacterIndex < c_reservedCharacterCount; ++reservedCharacterIndex )
    {
        ConstString const& currentCharacter = m_reservedFSCharacters[ reservedCharacterIndex ];
        char const * const currentCharacterString = currentCharacter.c_str();

        bool const c_containsThisCharacter = strstr( pString, currentCharacterString ) != 0;
        if( c_containsThisCharacter && out_charactersFound )
        {
            *out_charactersFound += currentCharacterString;
        }

        result = c_containsThisCharacter || result;
    }

    return result;
}

// PURPOSE: check for profanity in a string without editing it
int fwProfanityFilter::CheckForProfaneTermsRaw(const char* pString, const char** ppTerm) const
{
	int count = StringLength(pString);

	// for every starting letter
	for(int i=0; i<count; i++)
	{
		int wordIndex = 0;
		// for every possible word length
		for(int wordLength=1; wordLength<count-i+1; wordLength++)
		{
			int result = 1;
			// for each profane word
			while(wordIndex < m_profaneTerms.GetCount() )
			{
				const char* profaneTerm = m_profaneTerms[wordIndex];
				// if this is the first character remove spaces at the beginning of the term
				bool bLeadingSpaceHasBeenIgnored = false;
				if(i == 0 && profaneTerm[0] == ' ')
				{
					bLeadingSpaceHasBeenIgnored = true;
					profaneTerm++;
				}

				result = strncmp(&pString[i], profaneTerm, wordLength);
				if(result == 0)
				{
					if(profaneTerm[wordLength] == '\0' ||
						(i + wordLength == count && profaneTerm[wordLength] == ' '))
					{
						if(ppTerm)
							*ppTerm = profaneTerm;
						return i;
					}
					else 
						break;
				}
				else if(result < 0)
				{
					if (!bLeadingSpaceHasBeenIgnored)
					{	//	This break relies on the m_profaneTerms being in ascending ASCII order.
						//	Ignoring the leading space breaks that ordering so we can't early out in that case.
						break;
					}
				}
				wordIndex++;
			}
			if(result != 0)
				break;
		}
	}

	return -1;
}

void fwProfanityFilter::AddReservedTerm(const char* pTerm)
{
	m_profaneTerms.PushAndGrow(ConstString(pTerm));
}

static int CompareAtStrings(const ConstString* a, const ConstString* b)
{
	return strcmp(*a,*b);
}

void fwProfanityFilter::Finalise()
{
	m_profaneTerms.QSort(0, m_profaneTerms.GetCount(), CompareAtStrings);
}

} //namespace rage
