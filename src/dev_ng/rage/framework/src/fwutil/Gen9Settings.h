#ifndef GTA_GEN9_SETTINGS
#define GTA_GEN9_SETTINGS

// rage
#include "system/param.h"

#ifndef RSG_GEN9
#define RSG_GEN9 0
#endif

#if RSG_GEN9
#define GET_OBJECT(x) x->GetObject()
#else
#define GET_OBJECT(x) x->m_pObject 
#endif

#define IS_GEN9_PLATFORM		(1 && defined( RSG_GEN9 ) && RSG_GEN9)

// Only use the simulation for pre-Gen9
#define GEN9_UI_SIMULATION_ENABLED  (0 && !IS_GEN9_PLATFORM && __BANK && !RSG_DURANGO)

#if GEN9_UI_SIMULATION_ENABLED
#define GEN9_UI_SIMULATION_ONLY(...)    __VA_ARGS__
#else // GEN9_UI_SIMULATION_ENABLED
#define GEN9_UI_SIMULATION_ONLY(...)
#endif // GEN9_UI_SIMULATION_ENABLED

#define GEN9_STANDALONE_ENABLED  ((1 && GEN9_UI_SIMULATION_ENABLED) || (1 && IS_GEN9_PLATFORM))

#if GEN9_STANDALONE_ENABLED
#define GEN9_STANDALONE_ONLY(...)    __VA_ARGS__
#else // GEN9_STANDALONE_ENABLED
#define GEN9_STANDALONE_ONLY(...)
#endif // GEN9_STANDALONE_ENABLED

#endif // GTA_GEN9_SETTINGS
