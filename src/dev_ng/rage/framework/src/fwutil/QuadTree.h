// Title	:	QuadTree.h
// Author	:	Greg Smith
// Started	:	18/11/03

#ifndef _QUADTREE_H_
#define _QUADTREE_H_

// Rage headers
#include "vector/vector3.h"

// Framework headers
#include "fwtl/pool.h"
#include "fwmaths/rect.h"
#include "fwutil/PtrList.h"

namespace rage {

class fwQuadTreeNode
{
public:
	FW_REGISTER_CLASS_POOL(fwQuadTreeNode);

	enum
	{
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT,
		NUM_CHILDREN
	};

	fwQuadTreeNode(const fwRect& bb,int Depth = 0);
	~fwQuadTreeNode();
    void DestroyTree();

	void AddItem(void* p_vData, const fwRect& bb);
	void AddItemNoDupes(void* p_vData, const fwRect& bb);
	void AddItemRestrictDupes(void* p_vData, const fwRect& bb);
	void DeleteItem(void* p_vData);
	void DeleteItem(void* p_vData, const fwRect& bb);
	void DeleteAllItems(void);
	void GetAllMatching(const fwRect& bb,fwPtrListSingleLink& matches);
	void GetAllMatching(const Vector3& position,fwPtrListSingleLink& matches);
	void GetAllMatchingNoDupes(const fwRect& bb,fwPtrListSingleLink& Matches);
	void GetAllMatchingNoDupes(const Vector3& position,fwPtrListSingleLink& matches);
	void GetAll(fwPtrListSingleLink& Matches);
	void GetAllNoDupes(fwPtrListSingleLink& Matches);

	// class used in for all matching functions
	typedef void (*RectCB)(const fwRect& bb, void* data);
	typedef void (*PosnCB)(const Vector3& bb, void* data);

	class fwQuadTreeFn {
	public:
		virtual ~fwQuadTreeFn() {};
		virtual void operator()(const fwRect&, void*) {}
		virtual void operator()(const Vector3&, void*) {}
	};
	void ForAllMatching(const fwRect& bb, fwQuadTreeFn& fn);
	void ForAllMatching(const Vector3& posn, fwQuadTreeFn& fn);
	// old style functions 
	void ForAllMatching(const fwRect& bb, RectCB fn);
	void ForAllMatching(const Vector3& posn, PosnCB fn);

#if __BANK
	void GetEntryCount(u32* aCounts);
	void GetNodeCount(u32* aCounts);
#endif

    fwRect m_rect;
	fwPtrListSingleLink m_DataItems;
	fwQuadTreeNode* mp_Children[NUM_CHILDREN];
	int m_Depth;
	
protected:
	bool InSector(const fwRect& bb, int sectorIndex);
	int FindSector(const fwRect& bb);
	int FindSector(const Vector3& Position);
};

}	// namespace rage

#endif //_QUADTREE_H_
