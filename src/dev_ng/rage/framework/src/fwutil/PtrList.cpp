// Title	:	PtrList.cpp
// Author	:	Richard Jobling
// Started	:	15/11/99

#include "PtrList.h"

namespace rage {

//FW_INSTANTIATE_CLASS_POOL(fwPtrNodeSingleLink, 12000, atHashString("PtrNode Single",0xba4368d9));	// was 3200, but caused problems with physics world search
//FW_INSTANTIATE_CLASS_POOL(fwPtrNodeDoubleLink, 800, atHashString("PtrNode Double",0x794dbcdb));

#if __DEV
FullDebugCB fwPtrList::sm_FullDebugCB=NULL; // a global function  which is called when we're full
#endif

void fwPtrListSingleLink::Flush()
{
	fwPtrNodeSingleLink* pNode = (fwPtrNodeSingleLink*)m_pHead;
	while (pNode != NULL)
	{
		fwPtrNodeSingleLink* pOldNode = pNode;
		pNode = (fwPtrNodeSingleLink*)pNode->GetNextPtr();

		pOldNode->Remove(*this);
		delete pOldNode;
	}
}

void fwPtrListDoubleLink::Flush()
{
	fwPtrNodeDoubleLink* pNode = (fwPtrNodeDoubleLink*)m_pHead;
	while (pNode != NULL)
	{
		fwPtrNodeDoubleLink* pOldNode = pNode;
		pNode = (fwPtrNodeDoubleLink*)pNode->GetNextPtr();

		pOldNode->Remove(*this);
		delete pOldNode;
	}
}

int fwPtrList::CountElements() const
{
	int Result = 0;

	fwPtrNode const * pNode = GetHeadPtr();
	
	while (pNode)
	{
		Result++;
		pNode = pNode->GetNextPtr();
	}
	return Result;
}

bool fwPtrList::IsMemberOfList(void *pMember)
{
	fwPtrNode *pNode = GetHeadPtr();
	
	while (pNode)
	{
		if (pMember == pNode->GetPtr())
		{
			return true;
		}
		pNode = pNode->GetNextPtr();
	}
	return false;
}

#if __DEV
template<> void rage::fwPool<fwPtrNodeSingleLink>::PoolFullCallback() 
{
	if(fwPtrList::GetFullDebugCB())
		fwPtrList::GetFullDebugCB()();
}
template<> void rage::fwPool<fwPtrNodeDoubleLink>::PoolFullCallback() 
{
	if(fwPtrList::GetFullDebugCB())
		fwPtrList::GetFullDebugCB()();
}

#endif

} // namespace rage
