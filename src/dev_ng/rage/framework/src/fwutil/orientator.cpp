// ======================
// fwutil/orientator.cpp
// (c) 2013 RockstarNorth
// ======================

#if __BANK || __RESOURCECOMPILER

#include "grcore/debugdraw.h"
#include "math/amath.h"
#include "system/memory.h"

#include "fwutil/orientator.h"
#include "fwutil/xmacro.h"

#include <float.h>
#include <algorithm>

namespace rage {

__COMMENT(static) Vec2* fwOrientator::sm_pointData         = NULL;
__COMMENT(static) int   fwOrientator::sm_pointCountMax     = 0;
__COMMENT(static) Vec2* fwOrientator::sm_hullPointData     = NULL;
__COMMENT(static) int   fwOrientator::sm_hullPointCountMax = 0;

static inline float dot(const Vec2& A, const Vec2& B)
{
	return A.x*B.x + A.y*B.y;
}

// 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
// Returns a positive value, if OAB makes a counter-clockwise turn,
// negative for clockwise turn, and zero if the points are collinear.
static inline float cross(const Vec2 &O, const Vec2 &A, const Vec2 &B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

fwOrientator::fwOrientator()
{
	sysMemSet(this, 0, sizeof(*this));
}

template <typename T> 
static T* fwOrientatorAllocate(int count)
{
#if ENABLE_DEBUG_HEAP
	// Debug Heap
	sysMemAutoUseDebugMemory debug;
	return rage_new T[count];
#else
	return (T*)sysMemHeapAllocate(count*sizeof(T));
#endif
}

template <typename T> 
static void fwOrientatorFree(T* ptr)
{
#if ENABLE_DEBUG_HEAP
	// Debug Heap
	sysMemAutoUseDebugMemory debug;
	delete [] ptr;
#else
	sysMemHeapFree(ptr);
#endif
}

__COMMENT(static) void fwOrientator::Init(int pointCountMax, int hullPointCountMax)
{
	if (sm_pointData == NULL)
	{
		sm_pointData     = fwOrientatorAllocate<Vec2>(pointCountMax);
		sm_pointCountMax = pointCountMax;
	}
	
	if (sm_hullPointData == NULL)
	{
		sm_hullPointData     = fwOrientatorAllocate<Vec2>(hullPointCountMax);
		sm_hullPointCountMax = hullPointCountMax;
	}
}

__COMMENT(static) void fwOrientator::Release()
{
	if (sm_pointData)
	{
		fwOrientatorFree<Vec2>(sm_pointData);
		sm_pointData = NULL;
	}

	if (sm_hullPointData)
	{
		fwOrientatorFree<Vec2>(sm_hullPointData);
		sm_hullPointData = NULL;
	}
}

void fwOrientator::Begin()
{
	Init();

	m_errorFlags     = 0;
	m_boundsMin      = Vec3V(V_FLT_MAX);
	m_boundsMax      = -m_boundsMin;
	m_pointCount     = 0;
	m_hullPointCount = 0;

	for (int i = 0; i < CRITERIA_COUNT; i++)
	{
		sysMemSet(&m_bestFit[i], 0, sizeof(m_bestFit[i]));
		m_bestFit[i].m_value = FLT_MAX;
	}
}

void fwOrientator::AddPoint(Vec3V_In point)
{
	m_boundsMin = Min(point, m_boundsMin);
	m_boundsMax = Max(point, m_boundsMax);

	if (m_pointCount < sm_pointCountMax)
	{
		sm_pointData[m_pointCount++] = Vec2(point.GetXf(), point.GetYf());
	}
	else
	{
		m_errorFlags |= ERROR_FLAG_TOO_MANY_POINTS;
	}
}

void fwOrientator::Process()
{
	if (m_errorFlags)
	{
		return;
	}
	else if (m_pointCount < 2)
	{
		m_errorFlags |= ERROR_FLAG_TOO_FEW_POINTS;
		return;
	}

	Vec2* P = sm_pointData;
	Vec2* H = sm_hullPointData;
	int N = 0;

	// build convex hull
	{
		// Sort points lexicographically
		std::sort(P, P + m_pointCount);

		// Build lower hull
		for (int i = 0; i < m_pointCount; i++)
		{
			while (N >= 2 && cross(H[N-2], H[N-1], P[i]) <= 0) N--;
			if (N >= sm_hullPointCountMax) { m_errorFlags |= ERROR_FLAG_TOO_MANY_HULL_POINTS; return; }
			H[N++] = P[i];
		}

		// Build upper hull
		for (int i = m_pointCount-2, t = N+1; i >= 0; i--)
		{
			while (N >= t && cross(H[N-2], H[N-1], P[i]) <= 0) N--;
			if (N >= sm_hullPointCountMax) { m_errorFlags |= ERROR_FLAG_TOO_MANY_HULL_POINTS; return; }
			H[N++] = P[i];
		}
	}

	Vec2 basisXPrev(0.0f, 0.0f);

	// TODO -- use rotating calipers algorithm to find best fit
	for (int i = -1; i < N; i++)
	{
		Vec2 basisX = (i >= 0) ? (H[(i + 1)%N] - H[i]) : Vec2(1.0f, 0.0f);
		float q = dot(basisX, basisX);

		if (q <= 0.001f)
		{
			continue; // edge is too short to consider
		}

		q = 1.0f/sqrtf(q);
		basisX.x *= q;
		basisX.y *= q;

		const Vec2 basisY(-basisX.y, basisX.x);

		if (i >= 0 && Abs<float>(dot(basisXPrev, basisY)) < 0.0001f)
		{
			continue; // edge is too similar to previous edge
		}

		float minX = +FLT_MAX;
		float minY = +FLT_MAX;
		float maxX = -FLT_MAX;
		float maxY = -FLT_MAX;

		for (int j = 0; j < N; j++)
		{
			const float x = dot(H[j], basisX);
			const float y = dot(H[j], basisY);

			minX = Min<float>(x, minX);
			minY = Min<float>(y, minY);
			maxX = Max<float>(x, maxX);
			maxY = Max<float>(y, maxY);
		}

		const float w = maxX - minX;
		const float h = maxY - minY;

		const float a = w*h;
		const float p = 2.0f*(w + h);
		const float m = Max<float>(w, h);

		if (i == -1)
		{
			m_bestFit[CRITERIA_A].m_valueBase = a;
			m_bestFit[CRITERIA_P].m_valueBase = p;
			m_bestFit[CRITERIA_M].m_valueBase = m;
		}
		else
		{
			m_hullPointCount++;
		}

		const Vec3V vbx(basisX.x, basisX.y, 0.0f);
		const Vec3V vby(basisY.x, basisY.y, 0.0f);

		const Vec3V boundsMin(minX, minY, m_boundsMin.GetZf());
		const Vec3V boundsMax(maxX, maxY, m_boundsMax.GetZf());

		m_bestFit[CRITERIA_A].Update(a, vbx, vby, boundsMin, boundsMax);
		m_bestFit[CRITERIA_P].Update(p, vbx, vby, boundsMin, boundsMax);
		m_bestFit[CRITERIA_M].Update(m, vbx, vby, boundsMin, boundsMax);

		basisXPrev = basisX;
	}
}

#if __BANK
/*void fwOrientator::ConstructFromDrawable(const rmcDrawable* drawable, const char* path)
{
	static fwOrientator& _this = *this;
	class AddTriangle { public: static void func(Vec3V_In v0, Vec3V_In v1, Vec3V_In v2, int, int, int, void*)
	{
		_this.AddPoint(v0);
		_this.AddPoint(v1);
		_this.AddPoint(v2);
	}};

	Begin();
	GeometryUtil::AddTrianglesForDrawable(drawable, LOD_HIGH, path, NULL, AddTriangle::func);
	Process();
}*/

void fwOrientator::WriteToCSV(fiStream* csv, const char* path) const
{
	if (m_errorFlags == 0)
	{
		const fwOrientator::fwBestFit& a = m_bestFit[fwOrientator::CRITERIA_A];
		const fwOrientator::fwBestFit& p = m_bestFit[fwOrientator::CRITERIA_P];
		const fwOrientator::fwBestFit& m = m_bestFit[fwOrientator::CRITERIA_M];

		fprintf(csv, "%s,%d,%d", path, m_pointCount, m_hullPointCount);
		fprintf(csv, ",a=,%f,%f,%f,%f", a.m_valueBase, a.m_value, a.m_valueBase/a.m_value, RtoD*atan2f(a.m_basisX.GetYf(), a.m_basisX.GetXf()));
		fprintf(csv, ",p=,%f,%f,%f,%f", p.m_valueBase, p.m_value, p.m_valueBase/p.m_value, RtoD*atan2f(p.m_basisX.GetYf(), p.m_basisX.GetXf()));
		fprintf(csv, ",m=,%f,%f,%f,%f", m.m_valueBase, m.m_value, m.m_valueBase/m.m_value, RtoD*atan2f(m.m_basisX.GetYf(), m.m_basisX.GetXf()));
		fprintf(csv, "\n");
	}
	else
	{
		fprintf(csv, "%s,errorflags=,%d\n", path, m_errorFlags);
	}

	csv->Flush();
}

void fwOrientator::DebugDraw(Mat34V_In matrix, Color32 colour, eCriteria criteria) const
{
	Vec3V basisX(V_X_AXIS_WZERO);
	Vec3V basisY(V_Y_AXIS_WZERO);
	Vec3V basisZ(V_Z_AXIS_WZERO);
	Vec3V boundsMin = m_boundsMin;
	Vec3V boundsMax = m_boundsMax;

	if (criteria != CRITERIA_NONE)
	{
		basisX    = m_bestFit[criteria].m_basisX;
		basisY    = m_bestFit[criteria].m_basisY;
		boundsMin = m_bestFit[criteria].m_boundsMin;
		boundsMax = m_bestFit[criteria].m_boundsMax;
	}

	class TransformLocal { public: static Vec3V_Out func(Mat34V_In m, Vec3V_In bx, Vec3V_In by, Vec3V_In bz, Vec3V_In p)
	{
		return Transform(m, bx*p.GetX() + by*p.GetY() + bz*p.GetZ());
	}};

	const Vec3V p000 = TransformLocal::func(matrix, basisX, basisY, basisZ, m_boundsMin);
	const Vec3V p100 = TransformLocal::func(matrix, basisX, basisY, basisZ, GetFromTwo<Vec::X2,Vec::Y1,Vec::Z1>(m_boundsMin, m_boundsMax));
	const Vec3V p010 = TransformLocal::func(matrix, basisX, basisY, basisZ, GetFromTwo<Vec::X1,Vec::Y2,Vec::Z1>(m_boundsMin, m_boundsMax));
	const Vec3V p110 = TransformLocal::func(matrix, basisX, basisY, basisZ, GetFromTwo<Vec::X2,Vec::Y2,Vec::Z1>(m_boundsMin, m_boundsMax));
	const Vec3V p001 = TransformLocal::func(matrix, basisX, basisY, basisZ, GetFromTwo<Vec::X1,Vec::Y1,Vec::Z2>(m_boundsMin, m_boundsMax));
	const Vec3V p101 = TransformLocal::func(matrix, basisX, basisY, basisZ, GetFromTwo<Vec::X2,Vec::Y1,Vec::Z2>(m_boundsMin, m_boundsMax));
	const Vec3V p011 = TransformLocal::func(matrix, basisX, basisY, basisZ, GetFromTwo<Vec::X1,Vec::Y2,Vec::Z2>(m_boundsMin, m_boundsMax));
	const Vec3V p111 = TransformLocal::func(matrix, basisX, basisY, basisZ, m_boundsMax);

	grcDebugDraw::Line(p000, p001, colour);
	grcDebugDraw::Line(p100, p101, colour);
	grcDebugDraw::Line(p010, p011, colour);
	grcDebugDraw::Line(p110, p111, colour);

	grcDebugDraw::Line(p000, p100, colour);
	grcDebugDraw::Line(p010, p110, colour);
	grcDebugDraw::Line(p001, p101, colour);
	grcDebugDraw::Line(p011, p111, colour);

	grcDebugDraw::Line(p000, p010, colour);
	grcDebugDraw::Line(p001, p011, colour);
	grcDebugDraw::Line(p100, p110, colour);
	grcDebugDraw::Line(p101, p111, colour);
}
#endif // __BANK

void fwOrientator::fwBestFit::Update(float value, Vec3V_In basisX, Vec3V_In basisY, Vec3V_In boundsMin, Vec3V_In boundsMax)
{
	if (m_value > value)
	{
		m_value     = value;
		m_basisX    = basisX;
		m_basisY    = basisY;
		m_boundsMin = boundsMin;
		m_boundsMax = boundsMax;
	}
}

} // namespace rage

#endif // __BANK || __RESOURCECOMPILER
