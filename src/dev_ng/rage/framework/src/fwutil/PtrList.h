#ifndef PCH_PRECOMPILED

// Title	:	PtrList.h
// Author	:	Richard Jobling
// Started	:	15/11/99

#ifndef _PTRLIST_H_
#define _PTRLIST_H_

#include "fwtl/pool.h"
#include "diag/tracker.h"		// RAGE_TRACK()
//#include "debug\debug.h"

namespace rage {

class fwPtrListSingleLink;
class fwPtrListDoubleLink;

//
// name:		fwPtrNode
// description:	One way link list 

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4201)
#endif

class fwPtrNode
{
protected:
#if __SPU
public:
#endif
	union
	{
		u64 memForSPUDma; 
		struct 
		{ 
			void* m_pVoid;
			fwPtrNode* m_pNext;
		};
	};
	fwPtrNode() {}
	fwPtrNode(void* pVoid) { m_pVoid = pVoid; }
public:

	void* GetPtr() const { return m_pVoid; }
	fwPtrNode* GetNextPtr() const { return m_pNext; }
};
#if !__SPU
CompileTimeAssertSize(fwPtrNode,8,16);
#endif

#if __WIN32
#pragma warning(pop)
#endif

//
// name:		fwPtrNodeSingleLink
// description:	Singly linked PtrNode
class fwPtrNodeSingleLink : public fwPtrNode
{
public:
	//FW_REGISTER_CLASS_POOL(fwPtrNodeSingleLink)	// was 70000

	fwPtrNodeSingleLink(void* pVoid) : fwPtrNode(pVoid) {}
	
	void Add(fwPtrListSingleLink& list);
	void Remove(fwPtrListSingleLink& list);
	void Remove(fwPtrListSingleLink& list, fwPtrNodeSingleLink* pPrev);
};

//
// name:		fwPtrNodeSingleLink
// description:	Doubly linked PtrNode
class fwPtrNodeDoubleLink : public fwPtrNode
{
public:	
	//FW_REGISTER_CLASS_POOL(fwPtrNodeDoubleLink)

	fwPtrNodeDoubleLink(void* pVoid) : fwPtrNode(pVoid) {}

	const fwPtrNode* GetPreviousPtr() const { return m_pPrevious; }

	void Add(fwPtrListDoubleLink& list);
	void Remove(fwPtrListDoubleLink& list);

protected:
	fwPtrNodeDoubleLink* m_pPrevious;
	
};

#if __DEV
typedef void (*FullDebugCB)(void);
#endif

//
// name:		fwPtrList
class fwPtrList
{
protected:
	class fwPtrNode* m_pHead;

	fwPtrList() : m_pHead(NULL) {}
public:
    class fwPtrNode* GetHeadPtr() { return m_pHead; }
    class fwPtrNode const* GetHeadPtr() const { return m_pHead; }
	int		CountElements() const;
	bool	IsMemberOfList(void *pMember);

#if __DEV
	static void SetFullDebugCB(FullDebugCB cb)	{sm_FullDebugCB=cb;}	// set the global call back for full lists (it used to call g_IplStore.PrintLoadedObjs(), but we don't knwo about g_iplStore and thw fw level)
	static FullDebugCB GetFullDebugCB()			{return sm_FullDebugCB;}	
#endif

private:

#if __DEV
	static FullDebugCB sm_FullDebugCB;
#endif
};

//
// name:		CPtrListSimple
// description:	One way link list 
class fwPtrListSingleLink : public fwPtrList
{
	friend class fwPtrNodeSingleLink;

public:
	~fwPtrListSingleLink() { Flush(); }

	void Flush();

	class fwPtrNodeSingleLink* GetHeadPtr() const { return (fwPtrNodeSingleLink*)m_pHead; }
	void SetHeadPtr(class fwPtrNodeSingleLink *pNode) { m_pHead = pNode; }
	fwPtrNodeSingleLink* Add(void* pPtr);
	void Remove(void* pPtr, bool checkAserts = true);
	void RemoveAll();
};

//
// name:		CPtrListSimple
// description:	One way link list 
class fwPtrListDoubleLink : public fwPtrList
{
	friend class fwPtrNodeDoubleLink;

public:
	~fwPtrListDoubleLink() { Flush(); }

	void Flush();

	class fwPtrNodeDoubleLink* GetHeadPtr() const { return (fwPtrNodeDoubleLink*)m_pHead; }
	void SetHeadPtr(class fwPtrNodeDoubleLink *pNode) { m_pHead = pNode; }
	fwPtrNodeDoubleLink* Add(void* pPtr);
	void Remove(void* pPtr);
	void RemoveAll();
};

// --- fwPtrListSingleLink -------------------------------------------------

//
// name:		Add
// description:	Add one way link list node to list
inline fwPtrNodeSingleLink* fwPtrListSingleLink::Add(void* pPtr)
{
	fwPtrNodeSingleLink* pNode = rage_new fwPtrNodeSingleLink(pPtr);
	pNode->Add(*this);
	return pNode;
}

//
// name:		Remove
// description:	Remove one way link list node from list
inline void fwPtrListSingleLink::Remove(void* pPtr, bool ASSERT_ONLY(checkAsserts))
{
	fwPtrNodeSingleLink* pNode = GetHeadPtr();
	fwPtrNodeSingleLink* pPrev = NULL;
#if	__ASSERT	
	bool deleted = false;
#endif	
	while (pNode != NULL)
	{
		if (pNode->GetPtr() == pPtr)
		{
			Assert(!checkAsserts || deleted == false);

			fwPtrNodeSingleLink* pOldNode = pNode;
			pNode = (fwPtrNodeSingleLink*)pNode->GetNextPtr();

			pOldNode->Remove(*this, pPrev);
			delete pOldNode;
			
#if	__ASSERT	
			deleted = true;
#else
			break;
#endif						
		}
		else
		{
			pPrev = pNode;
			pNode = (fwPtrNodeSingleLink*)pNode->GetNextPtr();
		}	
	}
	Assert(!checkAsserts || deleted == true);
}

// name:		RemoveAll
// description:	Remove all nodes
inline void fwPtrListSingleLink::RemoveAll()
{
	fwPtrNodeSingleLink* pNode = GetHeadPtr();
	fwPtrNodeSingleLink* pNextNode;

	while (pNode)
	{
		pNextNode = static_cast< fwPtrNodeSingleLink* >( pNode->GetNextPtr() );
		delete pNode;
		pNode = pNextNode;
	}

	m_pHead = NULL;
}

// --- fwPtrListDoubleLink -------------------------------------------------

//
// name:		Add
// description:	Add two way link list node to list
inline fwPtrNodeDoubleLink* fwPtrListDoubleLink::Add(void* pPtr)
{
	fwPtrNodeDoubleLink* pNode = rage_new fwPtrNodeDoubleLink(pPtr);
	pNode->Add(*this);
	return pNode;
}

//
// name:		Remove
// description:	Remove two way link list node from list
inline void fwPtrListDoubleLink::Remove(void* pPtr)
{
	fwPtrNodeDoubleLink* pNode = GetHeadPtr();
#if	__ASSERT	
	bool deleted = false;
#endif	
	while (pNode != NULL)
	{
		if (pNode->GetPtr() == pPtr)
		{
			Assert(deleted == false);

			fwPtrNodeDoubleLink* pOldNode = pNode;
			pNode = (fwPtrNodeDoubleLink*)pNode->GetNextPtr();

			pOldNode->Remove(*this);
			delete pOldNode;
			
#if	__ASSERT	
			deleted = true;
#else
			break;
#endif						
		}
		else
			pNode = (fwPtrNodeDoubleLink*)pNode->GetNextPtr();
	}
	Assert(deleted == true);
}

// name:		RemoveAll
// description:	Remove all nodes
inline void fwPtrListDoubleLink::RemoveAll()
{
	fwPtrNodeDoubleLink* pNode = GetHeadPtr();
	fwPtrNodeDoubleLink* pNextNode;

	while (pNode)
	{
		pNextNode = static_cast< fwPtrNodeDoubleLink* >( pNode->GetNextPtr() );
		delete pNode;
		pNode = pNextNode;
	}

	m_pHead = NULL;
}

// --- fwPtrNodeSingleLink -------------------------------------------------

//
// name:		Add
// description:	Add one way link list node to list
inline void fwPtrNodeSingleLink::Add(fwPtrListSingleLink& list)
{
	m_pNext = list.m_pHead;
	list.m_pHead = this;
}

//
// name:		Remove
// description:	Remove one way link list node from list
inline void fwPtrNodeSingleLink::Remove(fwPtrListSingleLink& list, fwPtrNodeSingleLink* pPrev)
{
	if (list.m_pHead == this)
	{
		Assert(pPrev == NULL);
		list.m_pHead = m_pNext;
	}
	else
	{
		Assert(pPrev != NULL);
		pPrev->m_pNext = m_pNext;
	}
}

//
// name:		Remove
// description:	Remove one way link list node from list
inline void fwPtrNodeSingleLink::Remove(fwPtrListSingleLink& list)
{
	if (list.m_pHead == this)
	{
		list.m_pHead = m_pNext;
		return;
	}	

	fwPtrNodeSingleLink* pPrev = (fwPtrNodeSingleLink*)list.m_pHead;

	while(pPrev != NULL && pPrev->m_pNext != this)
		pPrev = (fwPtrNodeSingleLink*)pPrev->m_pNext;
	
	if (pPrev != NULL)
		pPrev->m_pNext = m_pNext;
}

// --- fwPtrNodeDoubleLink -------------------------------------------------

//
// name:		Add
// description:	Add two way link list node to list
inline void fwPtrNodeDoubleLink::Add(fwPtrListDoubleLink& list)
{
	m_pPrevious = NULL;
	m_pNext = list.m_pHead;

	if (list.m_pHead != NULL)
	{
		((fwPtrNodeDoubleLink*)list.m_pHead)->m_pPrevious = this;
	}

	list.m_pHead = this;
}

//
// name:		Remove
// description:	Remove two way link list node from list
inline void fwPtrNodeDoubleLink::Remove(fwPtrListDoubleLink& list)
{
	if (list.m_pHead == this)
	{
		list.m_pHead = m_pNext;
	}

	if (m_pPrevious != NULL)
	{
		m_pPrevious->m_pNext = m_pNext;
	}

	if (m_pNext != NULL)
	{
		((fwPtrNodeDoubleLink*)m_pNext)->m_pPrevious = m_pPrevious;
	}
}

} // namespace rage 

#endif

#if __DEV && !__SPU
// forward declare so we don't get multiple definitions

namespace rage {
	template<> void fwPool<fwPtrNodeSingleLink>::PoolFullCallback();
	template<> void fwPool<fwPtrNodeDoubleLink>::PoolFullCallback();
} // namespace rage

#endif


#endif
