// ===========================
// fwutil/popups.h
// (c) 2014 Rockstar San Diego
// ===========================

#ifndef _FWUTIL_POPUPS_H_
#define _FWUTIL_POPUPS_H_

#if !__FINAL

#include "system/codecheck.h"

namespace rage {

FASTRETURNCHECK(bool) ForceNoPopupsBegin(bool nopopups);
void ForceNoPopupsEnd(bool prev);

class ForceNoPopupsAuto
{
public:
	ForceNoPopupsAuto(bool nopopups);
	~ForceNoPopupsAuto();
private:
	bool m_nopopups;
};

} // namespace rage

#endif // !__FINAL
#endif // _FWUTIL_POPUPS_H_
