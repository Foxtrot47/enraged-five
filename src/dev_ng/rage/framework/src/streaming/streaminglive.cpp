//
// streaming/streaminglive.cpp
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "streaminglive.h"

#include "streamingdefs.h"

#include "string/stringhash.h"

#if LIVE_STREAMING

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/packfile.h"
#include "system/criticalsection.h"
#include "system/timer.h"
#include "system/param.h"
#include "system/threadtype.h"

#include "packfilemanager.h"
#include "streamingengine.h"
#include "streaminginfo.h"
#include "streamingmodule.h"

#include "vector/color32.h"

namespace rage {

#define SEPARATOR			'/'
#define SEPARATOR_STRING	"/"
#define OTHER_SEPARATOR		'\\'

// Number of ms after which the critical section needs to be temporarily unlocked.
const float CRITSEC_THROTTLE_TIMER = 1.0f;

PARAM(previewdebug, "[streaming] Spew debug info to analyze the preview folder");
PARAM(previewcoords, "Where to put the preview tag (=x,y)");
PARAM(dontaddunlinkedfiles, "Don't add a streamable index for new files");
#if __BANK
PARAM(previewmode, "Default mode for Preview display (summary/patched/all/nada");
#endif

Vector2 g_previewCoords = Vector2(0.7f, 0.8f);

static sysCriticalSectionToken s_LiveCriticalSection;

struct UnlinkedFile
{
	UnlinkedFile() {};
	UnlinkedFile(strStreamingLiveFile *file, const char *path, const char *relativePath)
	{
		m_File = file;
		safecpy(m_Path, path);
		safecpy(m_RelativePath, relativePath);
	}

	strStreamingLiveFile *m_File;
	char m_Path[RAGE_MAX_PATH];
	char m_RelativePath[RAGE_MAX_PATH];
};

// This is a list of live files that haven't been linked to a streamable yet. This
// list is accessed and maintained by the live thread only.
static atArray<UnlinkedFile> s_TempUnlinkedLiveFiles;

// This is a list of live files that haven't been linked to a streamable yet. This
// list is accessed by the update thread; access needs to be synchronized with the
// s_LiveCriticalSection.
static atArray<UnlinkedFile> s_UnlinkedLiveFiles;



// Fix a path to a normalised form - from rage::file/asset.h
static void FixPath(char *dest, int destSize, const char *src) 
{
	safecpy(dest, src, destSize);
	if (dest[0] == 0)
	{
		return;
	}

	int sl = StringLength(dest);
	char *path = dest;

	// Convert to expected slash type
	while (*path) 
	{
		if (*path == OTHER_SEPARATOR)
		{
			*path = SEPARATOR;
		}
		path++;
	}

	// Tack on trailing appropriate slash if necessary
	if (sl && dest[--sl] != SEPARATOR) 
	{
		dest[++sl] = SEPARATOR;
		dest[++sl] = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////

// Unlink a file from the streaming system
bool strStreamingLiveFile::UnlinkFile()
{
	if (m_flags & strStreamingLiveFile::LIVE_STREAMING_FILE)
	{
		strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(m_index);

		// Close the live file handle we opened.
		pgStreamer::Close(pInfo->GetHandle());

		// Repoint streaming info back to original handle
		pInfo->SetHandle(m_oldhandle);
		
		if (m_flags & LIVE_ADDED_NEW_STRINDEX)
		{
			strStreamingEngine::GetInfo().UnregisterObject(m_index);
		}

		pInfo->ClearFlag(m_index, STRFLAG_INTERNAL_LIVE);

		m_flags &= ~(strStreamingLiveFile::LIVE_STREAMING_FILE | strStreamingLiveFile::LIVE_ADDED_NEW_STRINDEX);
		m_index = strIndex(strIndex::INVALID_INDEX);

		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////

// Constructor.
void strStreamingLiveFolder::Init(const char* folder, const char* relativePath)
{
	m_folder = folder;
	m_relativePath = relativePath;
	m_subfolders.Init(10);
}


// Destructor.
void strStreamingLiveFolder::Shutdown()
{
	for (int i=0; i<m_files.GetCount(); i++)
	{
		DeleteLiveFile(&m_files[i]);
	}
	m_files.Reset();
	m_subfolders.Reset();
}


// Find a file by index.
strStreamingLiveFile* strStreamingLiveFolder::FindFileByIndex(strIndex index)
{
	for (int i=0; i<m_files.GetCount(); i++)
	{
		strStreamingLiveFile* pFile = &m_files[i];
		if (pFile->m_index == index)
		{
			return pFile;
		}
	}

	return NULL;
}


// Delete a file.
void strStreamingLiveFolder::DeleteLiveFile(strStreamingLiveFile* pFile)
{
	if (pFile->m_flags & strStreamingLiveFile::LIVE_STREAMING_FILE)
	{
		streamVerifyf(pFile->UnlinkFile(), "Failed to unlink");
	}
}


// Callback to send the information back to the calling object.
void strStreamingLiveFolder::UpdateFileCB(const fiFindData& findData, void* pUserData)
{
	strStreamingLiveFolder* pThis = (strStreamingLiveFolder*)pUserData;
	pThis->UpdateFile(findData);
}


// Update a file.
void strStreamingLiveFolder::UpdateFile(const fiFindData& findData)
{
	// Don't hold the crit sec for too long.
	strStreamingEngine::GetLive().ThrottleCritsec();

	u32 thisHash = atStringHash(findData.m_Name);
	strStreamingLiveFile* pFile = NULL;

	char path[256];
	formatf(path, "%s%s", GetFolder().GetCStr(), findData.m_Name);

	if (findData.m_Attributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		atHashValue nameHash(findData.m_Name);
		if (m_subfolders.Lookup(nameHash.GetHash()) == -1)
		{
			atString relativePath = atVarString("%s%s/", m_relativePath.IsNotNull()?m_relativePath.GetCStr():"", findData.m_Name);

			int index = strStreamingEngine::GetLive().AddLiveFolder(path, relativePath);
			m_subfolders.Insert(nameHash.GetHash(), index);
		}
	}
	else
	{
		for (int i=0; i<m_files.GetCount(); i++)
		{
			if (m_files[i].m_namehash == thisHash)
			{
				pFile = &m_files[i];
				break;
			}
		}
				
		if (!pFile)
		{
			pFile = &m_files.Grow(16);
			pFile->m_filetime = findData.m_LastWriteTime;
			if (PARAM_previewdebug.Get())
			{
				streamDisplayf("Found new file %s, setting timestamp to %" I64FMT "d", findData.m_Name, pFile->m_filetime);
			}
			pFile->m_oldhandle = pgStreamer::Open(path, NULL, 0);
			streamAssertf(pFile->m_oldhandle != pgStreamer::Error,"Unable to open live streaming file '%s'",path);
			pFile->m_filetime = findData.m_LastWriteTime;
			if (PARAM_previewdebug.Get())
			{
				streamDisplayf("Setting timestamp again to %" I64FMT "d", pFile->m_filetime);
			}
			pFile->m_namehash = thisHash;

			pFile->m_flags = strStreamingLiveFile::LIVE_STILL_EXISTS;
			pFile->m_index = strIndex(strIndex::INVALID_INDEX);
			const char* extension = strrchr(findData.m_Name, '.');
			strStreamingModule* pModule = extension? strStreamingEngine::GetInfo().GetModuleMgr().GetModuleFromFileExtension(extension + 1) : 0;
			pFile->m_module = static_cast<s16>(extension != NULL && pModule != NULL ? pModule->GetStreamingModuleId() : strStreamingInfo::INVALID_LOADER);
		}

		if (pFile->m_module != strStreamingInfo::INVALID_LOADER)
		{
			if (pFile->m_index == strIndex(strIndex::INVALID_INDEX))
			{
				atString relativePath = atVarString("%s%s", m_relativePath.IsNotNull()?m_relativePath.GetCStr():"", findData.m_Name);

				strIndex index = strStreamingEngine::GetInfo().GetStreamingIndexByFilename(relativePath, pFile->m_module);

				if (index == strIndex())
				{
					s_TempUnlinkedLiveFiles.Grow(16) = UnlinkedFile(pFile, path, relativePath);
				}
				else
				{
					// streamVerifyf(pFile->LinkFile(path, index), "Failed to link %s", findData.m_Name);
					strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(index);

					// remember previous handle
					u32 temp = pFile->m_oldhandle;
					pFile->m_oldhandle = pInfo->GetHandle();
					pInfo->SetHandle(temp); 

					if (PARAM_previewdebug.Get())
					{
						streamDisplayf("Marking for LIVE, redirecting folder: %s. Status=%s. Old handle=%x, New handle=%x", findData.m_Name, strStreamingInfo::GetFriendlyStatusName(pInfo->GetStatus()), pFile->m_oldhandle, temp);
					}

					pInfo->SetFlag(index, STRFLAG_INTERNAL_LIVE);
					pFile->m_flags |= strStreamingLiveFile::LIVE_STREAMING_FILE;
					pFile->m_index = index;

					strStreamingModule *module = strStreamingEngine::GetInfo().GetModule(index);
					module->MarkDirty(module->GetObjectIndex(index));

					pFile->m_filetime = 0;	// Use an invalid timestamp to refresh
				}
			}

			if (pFile->m_index != strIndex(strIndex::INVALID_INDEX) && pFile->m_filetime != findData.m_LastWriteTime)
			{
				if (PARAM_previewdebug.Get())
				{
					streamDisplayf("Invalidating %s (timestamp is %" I64FMT "d, stored was %" I64FMT "d)", findData.m_Name, findData.m_LastWriteTime, pFile->m_filetime);
				}
				strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(pFile->m_index);
				pgRawStreamerInvalidateEntry(pInfo->GetHandle());
				pFile->m_filetime = findData.m_LastWriteTime;

				if (pInfo->GetStatus() != STRINFO_NOTLOADED)
				{
					if (PARAM_previewdebug.Get())
					{
						streamDisplayf("Not unloaded - needs refresh");
					}
					pFile->m_flags |= strStreamingLiveFile::LIVE_NEEDS_REFRESH;
				}
				else
				{
					UpdateDirtyFile(*pFile);
				}
			}
			else
			{
				if (PARAM_previewdebug.Get())
				{
					streamDisplayf("Timestamp doesn't mismatch on %s (%" I64FMT "d)", findData.m_Name, pFile->m_filetime);
				}
			}
		}
		

		// reset the check, so this file won't get deleted.
		pFile->m_flags |= strStreamingLiveFile::LIVE_STILL_EXISTS;

		// update stats
		if (pFile->m_flags & strStreamingLiveFile::LIVE_IS_PATCHED)
		{
			if (findData.m_LastWriteTime != pFile->m_filetime)
			{
				if (PARAM_previewdebug.Get())
				{
					streamDisplayf("Live file changed (%s) from %" I64FMT "d to %" I64FMT "d", findData.m_Name, pFile->m_filetime, findData.m_LastWriteTime);
				}

				strIndex index = pFile->m_index;
				if (index.IsValid())
				{
					strStreamingModule *module = strStreamingEngine::GetInfo().GetModule(index);
					module->MarkDirty(module->GetObjectIndex(index));
				}

				pFile->m_flags |= strStreamingLiveFile::LIVE_NEEDS_REFRESH;
				pFile->m_filetime = findData.m_LastWriteTime;
			}
		}

		if (pFile->m_index.IsValid() && pFile->m_flags & strStreamingLiveFile::LIVE_NEEDS_REFRESH)
		{
			strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(pFile->m_index);

			if (pInfo->GetStatus() == STRINFO_NOTLOADED)
			{
				// Now is a good time to update the internal status.
				UpdateDirtyFile(*pFile);
			}
		}
	}
}

void strStreamingLiveFolder::UpdateDirtyFile(strStreamingLiveFile &file)
{
	if (file.m_index.IsValid())
	{
		strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_index);

		if (pInfo->GetStatus() == STRINFO_NOTLOADED || pInfo->GetStatus() == STRINFO_LOADREQUESTED)
		{
			u32 handle = pInfo->GetHandle();
			
			const fiPackEntry &entry = fiCollection::GetCollection(handle)->GetEntryInfo(handle);

			if (entry.IsResource())
			{
				pInfo->SetFlag(file.m_index, STRFLAG_INTERNAL_RESOURCE);
#if STORE_FILE_SIZES
				pInfo->SetVirtualSize((u32) entry.u.resource.m_Info.GetVirtualSize());
				pInfo->SetPhysicalSize((u32) entry.u.resource.m_Info.GetPhysicalSize());
#endif // STORE_FILE_SIZES

				if (PARAM_previewdebug.Get())
				{
					Displayf("Updating live file %s - is a resource, virtual size %" SIZETFMT "u, physical size %" SIZETFMT "u", strStreamingEngine::GetObjectName(file.m_index),
						entry.u.resource.m_Info.GetVirtualSize(), entry.u.resource.m_Info.GetPhysicalSize());
				}
			}
			else
			{
				pInfo->ClearFlag(file.m_index, STRFLAG_INTERNAL_RESOURCE);

#if STORE_FILE_SIZES
				pInfo->SetVirtualSize(entry.u.file.m_UncompressedSize);
#endif // STORE_FILE_SIZES

				Displayf("Updating live file %s - not a resource", strStreamingEngine::GetObjectName(file.m_index));
			}
		}
	}
}

void strStreamingLiveFolder::UpdateAllDirtyFiles()
{
	for (int x=0; x<m_files.GetCount(); x++)
	{
		UpdateDirtyFile(m_files[x]);
	}
}

void strStreamingLive::UpdateAllDirtyFiles()
{
	for (int x=0; x<LIVE_NUMFOLDERS; x++)
	{
		m_folders[x].UpdateAllDirtyFiles();
	}
}

void strStreamingLive::UpdateUnlinkedFiles()
{
	streamAssert(sysThreadType::IsUpdateThread());

	SYS_CS_SYNC(s_LiveCriticalSection);

	int count = s_UnlinkedLiveFiles.GetCount();

	for (int x=0; x<count; x++)
	{
		UnlinkedFile &unlinkedFile = s_UnlinkedLiveFiles[x];
		strStreamingLiveFile *pFile = unlinkedFile.m_File;

		// Double-check to see if it's still unlinked.
		if (pFile->m_index == strIndex())
		{
			strIndex index;

			// Let's see if we can create a new entry for this file.
			if (!PARAM_dontaddunlinkedfiles.Get())
			{
				index = strPackfileManager::RegisterIndividualFile( unlinkedFile.m_Path, false, unlinkedFile.m_RelativePath);
			}

			if (index != strIndex())
			{
				pFile->m_flags |= strStreamingLiveFile::LIVE_ADDED_NEW_STRINDEX;
				if (PARAM_previewdebug.Get())
				{
					streamDisplayf("Discovered new live file '%s', assigned to streaming index %d", unlinkedFile.m_Path, index.Get());
				}
			}

			if (index != strIndex())
			{
				// streamVerifyf(pFile->LinkFile(path, index), "Failed to link %s", findData.m_Name);
				strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(index);

				// remember previous handle
				u32 temp = pFile->m_oldhandle;
				pFile->m_oldhandle = pInfo->GetHandle();
				pInfo->SetHandle(temp); 

				pInfo->SetFlag(index, STRFLAG_INTERNAL_LIVE);
				pFile->m_flags |= strStreamingLiveFile::LIVE_STREAMING_FILE;
				pFile->m_index = index;

				if (pInfo->GetStatus() != STRINFO_NOTLOADED)
				{
					pFile->m_flags |= strStreamingLiveFile::LIVE_NEEDS_REFRESH;
				}
			}
		}
	}

	s_UnlinkedLiveFiles.Resize(0);
}

// Update the folder contents.
bool strStreamingLiveFolder::Update(bool enabled)
{
	bool patched = false;

	// clear state
	for (int i=0; i<m_files.GetCount(); i++)
		m_files[i].m_flags &= ~strStreamingLiveFile::LIVE_STILL_EXISTS;

	// update state
	if (enabled)
	{
		static_cast<const fiDeviceRemote &>(fiDeviceRemote::GetInstance()).InvalidateDirectoryCache(m_folder.GetCStr());
		ASSET.EnumFiles( m_folder.GetCStr(), UpdateFileCB, (void*) this );
	}

	for (int i=0; i<m_files.GetCount(); i++)
	{
		strStreamingLiveFile* pFile = &m_files[i];

		bool bPatchedStreamingFile = (pFile->m_flags & strStreamingLiveFile::LIVE_IS_PATCHED) != 0;

		if (!bPatchedStreamingFile)
		{
			streamDebugf3("Automatic update");
			pFile->m_flags &= ~strStreamingLiveFile::LIVE_NEEDS_REFRESH;
			if (!(pFile->m_flags & strStreamingLiveFile::LIVE_STILL_EXISTS))
			{
				pFile->UnlinkFile();
				m_files.Delete(i);
				--i;
			}
		}
		else
		{
			patched = true;
		}
	}

	return patched;
}


///////////////////////////////////////////////////////////////////////////////

#if __BANK
enum {
	eNone,
	eSummary,
	ePatched,
	eAll
};



static bool s_loadScene = true;
static bool s_enablePreview;
static int s_display = eSummary;
#endif

// Initialise.
void strStreamingLive::Init()
{
	m_numFolders = 0;
	m_running = true;
	m_updateThreadId = sysIpcCreateThread(strStreamingLive::UpdateThreadFunc, this, sysIpcMinThreadStackSize, PRIO_BELOW_NORMAL, "[StreamingLive]UpdateThreadFunc", 1);

#if __BANK
	const char* pMode = NULL;
	if( PARAM_previewmode.Get(pMode) && pMode && pMode[0] != '\0' )
	{
		// why bother with strcmps when the first characters are all different!
		switch (pMode[0])
		{
		case 'n':
		case 'N':
			s_display = eNone;
			break;

		case 's':
		case 'S':
			s_display = eSummary;
			break;

		case 'p':
		case 'P':
			s_display = ePatched;
			break;

		case 'a':
		case 'A':
			s_display = eAll;
			break;		
		}
	}
#endif
}


// Shutdown.
void strStreamingLive::Shutdown()
{
	m_running = false;
	sysIpcWaitThreadExit(m_updateThreadId);

	for (int i = 0; i < m_numFolders; ++i)
	{
		m_folders[i].Shutdown();
	}
	m_numFolders = 0;
	m_enabled = false;
}

void strStreamingLive::SetEnabled(bool enabled)
{
	m_enabled = enabled;
}

void strStreamingLive::UpdateThreadFunc(void* data)
{
	strStreamingLive* live = (strStreamingLive*) data;

	while (live->m_running)
	{
		live->Update();
		sysIpcSleep(1000);
	}
}

#if __BANK
void Flush()
{
	g_strStreamingInterface->RequestFlush(s_loadScene);
	// CStreaming::GetCleanup().RequestFlush(s_loadScene);
}

void UpdateEnable()
{
	strStreamingEngine::GetLive().SetEnabled(s_enablePreview);
}

void strStreamingLive::AddBankWidgets()
{
	bkBank& bank = BANKMGR.CreateBank("Preview Folder");
	
	s_enablePreview = strStreamingEngine::GetLive().GetEnabled();
	bank.AddToggle("Enable", &s_enablePreview, datCallback(UpdateEnable), 
		"Enable the preview folder to allow files to be loaded from the preview folder instead the normal images. Run with -previewfolder to enable from start.");
	
	static const char* displayType[] = {"nada","summary","patched files","all files"};
	
	bank.AddCombo("Display", &s_display,4,displayType,NullCB,
		"Display information on previewed files, whether they are patched or not.");
	
	bank.AddButton("Flush scene", datCallback(Flush),
		"Flush the scene out of memory. Some files can not be flushed, to see reload them please start a new game. Keyboard shortcut is DELETE (not the one on the Numpad).");
	
	bank.AddToggle("Load scene after flush", &s_loadScene, NullCB, 
		"Perform a scene load after every flush.");

	float coords[2];
	if (PARAM_previewcoords.GetArray(coords,2))
	{
		g_previewCoords = Vector2(rage::Clamp(coords[0], 0.2f, 0.8f), rage::Clamp(coords[1], 0.2f, 0.8f));
	}

	bank.AddVector("Preview text location", &g_previewCoords, 0.2f, 0.8f, 0.1f);

	bank.AddText("Path", &m_folders[0].GetFolder());
}

void strStreamingLive::DrawDebugInfo(void (*cb)(Color32 c,const char *fmt,...))
{
	sysCriticalSection c(m_csToken);

	if (s_display != eNone && cb)
	{
		s32 dirty = 0;
		s32 clean = 0;
		s32 notLoaded = 0;

		for (int i=0; i<m_numFolders; i++)
		{
			for (int j=0; j<m_folders[i].m_files.GetCount(); j++)
			{
				strStreamingLiveFile *pFile = &m_folders[i].m_files[j];

				if (pFile->m_flags & strStreamingLiveFile::LIVE_STREAMING_FILE) {
					strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(pFile->m_index);

					Color32 col(0,0,255);
					const char* message = NULL;
					if (pInfo)
					{
						if (pInfo->GetStatus() == STRINFO_NOTLOADED)
						{
							if (s_display == eAll)
							{
								col.Set(128,128,128);
								message = "- not loaded";	
							}
							notLoaded++;
						}
						else if (pFile->m_flags & strStreamingLiveFile::LIVE_IS_PATCHED)
						{
							if (!(pFile->m_flags & strStreamingLiveFile::LIVE_STILL_EXISTS))
							{
								col.Set(255,0,0);
								message = "- patched - patch was deleted";
								dirty++;
							}
							else if (pFile->m_flags & strStreamingLiveFile::LIVE_NEEDS_REFRESH)
							{
								col.Set(255,255,0);
								message = "- patched - patch is different";
								dirty++;
							}
							else 
							{
								col.Set(0,255,0);
								message = "- patched";
								clean++;
							}
						}
						else 
						{
							if (pFile->m_flags & strStreamingLiveFile::LIVE_IS_LOADING)
							{
								col.Set(0,128,0);
								message = "- patching - patch available";
							}
							else if (pInfo->GetStatus() == STRINFO_LOADREQUESTED)
							{
								col.Set(0,128,0);
								message = "- requested - patch available";
							}
							else
							{
								col.Set(255,140,0);
								message = "- not patched - patch available";
								dirty++;
							}	
						}
					}

					if (message && s_display > eSummary)
						cb(col, "%s %s", strStreamingEngine::GetInfo().GetObjectName(pFile->m_index), message);
				}
				else if (s_display == eAll)
				{
					Color32 col(0,255,255);
					cb(col, "%s (non streaming)", pgStreamer::GetFullName(pFile->m_oldhandle));
				}
			}
		}

		if (s_display == eSummary)
		{
			if (dirty)
			{
				if(notLoaded)
				{
					Color32 col(255,0,0);
					cb(col, "Preview clean (%d) DIRTY (%d) not loaded (%d)", clean, dirty, notLoaded);
				}
				else
				{
					Color32 col(255,0,0);
					cb(col, "Preview clean (%d) DIRTY (%d)", clean, dirty);
				}
			}
			else if (clean)
			{
				if(notLoaded)
				{
					Color32 col(0,255,0);
					cb(col, "Preview clean (%d) not loaded (%d)", clean, notLoaded);
				}
				else
				{
					Color32 col(0,255,0);
					cb(col, "Preview clean (%d)", clean);
				}
			}
			else if (m_enabled)
			{
				if(notLoaded)
				{
					Color32 col(255,255,255);
					cb(col, "Preview not loaded (%d)", notLoaded);
				}
				else
				{
					Color32 col(255,255,255);
					cb(col, "Preview empty");
				}
			}
		}
	}
}

#endif // __BANK




// Add a live folder.
int strStreamingLive::AddLiveFolder(const char* livefolder, const char* relativePath)
{
	char fixedfolder[256];

	FixPath(fixedfolder, 256, livefolder);

	atHashValue fixedFolderHash(fixedfolder);

	if( m_numFolders < LIVE_NUMFOLDERS )
	{
		for (int i = 0; i < m_numFolders; ++i)
		{
			if (fixedFolderHash == m_folders[i].GetFolder())
			{
				// already existed
				return i;
			}
		}

		strStreamingLiveFolder* folder = &m_folders[m_numFolders];
		folder->Init(fixedfolder, relativePath);

		return m_numFolders++;
	}

	streamWarningf("Exceeded maximum number of live folders.");
	return -1;
}


// Update the folders.
void strStreamingLive::Update()
{
	bool patched = false;
	{
		m_csToken.Lock();
		m_lockTimer.Reset();
		s32 i;

	
		for (i = 0; i < m_numFolders; ++i)
		{
			patched |= m_folders[i].Update(m_enabled);
		}
		m_csToken.Unlock();
	}
	m_patched = patched;

	// After we're done, update the list of files that we couldn't link.
	SYS_CS_SYNC(s_LiveCriticalSection);
	s_UnlinkedLiveFiles.Swap(s_TempUnlinkedLiveFiles);
	s_TempUnlinkedLiveFiles.Resize(0);
}

void strStreamingLive::ThrottleCritsec()
{
	if (m_lockTimer.GetMsTime() > CRITSEC_THROTTLE_TIMER)
	{
		m_csToken.Unlock();
		sysIpcSleep(1);
		m_csToken.Lock();
		m_lockTimer.Reset();
	}
}


// Find out if the system is patched at all
bool strStreamingLive::GetPatched()
{
	return m_patched;
}


// Find a file using the streaming index.
strStreamingLiveFile* strStreamingLive::FindFileWithIndex(strIndex index)
{
	for (int i=0; i<m_numFolders; i++)
		for (int j=0; j<m_folders[i].m_files.GetCount(); j++)
			if (m_folders[i].m_files[j].m_index == index)
				return &m_folders[i].m_files[j];
	return NULL;
}


// Find a folder and a file combination that for streaming index.
bool strStreamingLive::FindFolderAndFileWithIndex(strIndex index, strStreamingLiveFolder*& pFolder, strStreamingLiveFile*& pFile )
{
	for (int i = 0; i < m_numFolders; ++i)
	{
		pFile = m_folders[i].FindFileByIndex(index);
		if (pFile)
		{
			pFolder = &m_folders[i];
			return true;
		}
	}
	return false;
}

void strStreamingLive::OpenLiveFile(strIndex index)
{
	sysCriticalSection c(m_csToken);

	strStreamingLiveFolder* pFolder = NULL; 
	strStreamingLiveFile* pFile = NULL; 

	if (m_enabled && FindFolderAndFileWithIndex(index, pFolder, pFile))
	{
		pFile->m_flags |= strStreamingLiveFile::LIVE_IS_LOADING;
	}
}


void strStreamingLive::CloseLiveFile(strIndex index)
{
	sysCriticalSection c(m_csToken);

	strStreamingLiveFolder* pFolder = NULL; 
	strStreamingLiveFile* pFile = NULL; 

	if (m_enabled && FindFolderAndFileWithIndex(index, pFolder, pFile))
	{
		pFile->m_flags &= ~strStreamingLiveFile::LIVE_IS_LOADING;
		pFile->m_flags |= strStreamingLiveFile::LIVE_IS_PATCHED;
	}
}



// A live file was removed from streaming memory.
void strStreamingLive::RemovedLiveFile(strIndex index)
{
	sysCriticalSection c(m_csToken);
	strStreamingLiveFile* pFile;
	strStreamingLiveFolder* pFolder;

	if (streamVerifyf(FindFolderAndFileWithIndex(index, pFolder, pFile), "Removing a live file that doesn't have counterpart"))
	{
		if (!(pFile->m_flags & strStreamingLiveFile::LIVE_STILL_EXISTS))
			pFolder->DeleteLiveFile(pFile);
		else if (pFile->m_flags & strStreamingLiveFile::LIVE_NEEDS_REFRESH)
			pFile->m_flags &= ~strStreamingLiveFile::LIVE_NEEDS_REFRESH;

		pFile->m_flags &= ~strStreamingLiveFile::LIVE_IS_PATCHED;
	}
}

bool strStreamingLive::FileStillExists(strIndex index)
{
	sysCriticalSection c(m_csToken);
	strStreamingLiveFile* pFile = FindFileWithIndex(index);

	return (pFile?(pFile->m_flags & strStreamingLiveFile::LIVE_STILL_EXISTS) != 0: false);
}

}		// namespace rage

#endif // LIVE_STREAMING
