//
// streaming/streaminginfo.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_STREAMINGINFO_H
#define STREAMING_STREAMINGINFO_H

#include "atl/pagedpool.h"
#include "atl/pool.h"
#include "atl/binmap.h"
#include "atl/map.h"
#include "atl/dlistsimple.h"
#include "atl/string.h"
#include "data/resourceheader.h"
#include "fwtl/pool.h"
#include "paging/streamer.h"

#include "streaming/streaming_channel.h"
#include "streamingdefs.h"
#include "streamingmodulemgr.h"
// #include "fwtl/pool.h"

namespace rage {

struct fiPackEntry;

enum eHierarchyModType
{
	HMT_ENABLE = 0,
	HMT_DISABLE,
	HMT_FLUSH
};

// Current status of object
// NOTE that you need to change strStreamingInfo::GetFriendlyStatusName if
// you change this!
enum strResidentStatus { 
	STRINFO_NOTLOADED=0, 
	STRINFO_LOADED, 
	STRINFO_LOADREQUESTED, 
	STRINFO_LOADING, 

	STRINFO_COUNT
};

#define STORE_NAME_LENGTH				(256)
#define STORE_FILE_SIZES				(__DEV || __BANK)
#define CHURN_TRACKING_LENGTH			(3)

struct strStreamingFile;
class strStreamingInfoManager;
class sysMemAllocator;
typedef u32 strFileHandle;

inline bool strIsValidHandle(strFileHandle handle)
{
	return handle != 0;
}

/** PURPOSE: An instance of this class defines an object that can be streamed.
 */
class strStreamingInfo
{
public:
	friend class strStreamingInfoManager;

	enum 
	{
		INVALID_LOADER=0xff,
		INVALID_MEMORY_INDEX=0x0fff,

		HANDLE_TYPE_MASK = 0xc0000000,
		HANDLE_TYPE_PACKFILE = 0x80000000,
		HANDLE_PACKFILE_MASK = 0x3fffffff,
	};

	void Init();

	__forceinline strResidentStatus GetStatus() const {return (strResidentStatus) m_status;}
	
#if __ASSERT
	void ClearFlags();
#else // __ASSERT
	__forceinline void ClearFlags() {m_flags = 0;}
#endif // __ASSERT

	__forceinline u16 GetFlags() const {return m_flags;}

	void VerifyFlagChange(u32 ASSERT_ONLY(oldFlags), u32 ASSERT_ONLY(newFlags), bool ASSERT_ONLY(checkLockedFlags))
	{
		streamAssertf(!checkLockedFlags || GetStatus() != STRINFO_LOADREQUESTED || 
			(((oldFlags ^ newFlags) & (STRFLAG_INTERNAL_DUMMY | STRFLAG_PRIORITY_LOAD)) == 0),
			"Don't change STRFLAG_INTERNAL_DUMMY or STRFLAG_PRIORITY_LOAD while a file is requested. (info=%p)", this);

		streamAssertf(!checkLockedFlags || GetStatus() != STRINFO_LOADED || 
			(((oldFlags ^ newFlags) & (STR_DONTDELETE_MASK)) == 0),
			"Don't change any of the STR_DONTDELETE_MASK flags while a file is loaded. (info=%p)", this);
	}

#if !__BANK	|| __RESOURCECOMPILER // This is really for non-StreamingVisualize, but I don't want to introduce this #include dependency
	void ClearFlag(strIndex /*index*/, u32 flag, bool checkLockedFlags = true)	{ VerifyFlagChange(m_flags, m_flags & ~flag, checkLockedFlags); m_flags &= ~flag;}
	void SetFlag(strIndex /*index*/, u32 flag, bool checkLockedFlags = true)	{ VerifyFlagChange(m_flags, m_flags | flag, checkLockedFlags); m_flags |= flag;}
#else // !__BANK
	void ClearFlag(strIndex index, u32 flag, bool checkLockedFlags = true);
	void SetFlag(strIndex index, u32 flag, bool checkLockedFlags = true);
#endif // !__BANK
	__forceinline bool IsFlagSet(u32 flag) const			{ return (m_flags & flag) != 0;}

	__forceinline void AddDependent() {++m_dependentCount;}
	__forceinline void RemoveDependent() {--m_dependentCount;}
	__forceinline u16 GetDependentCount() const {return m_dependentCount;}

	datResourceInfo GetResourceInfo();
	void SetResourceInfo(datResourceInfo& value);

#if STORE_FILE_SIZES
	void SetVirtualSize(u32 size)			{ m_VirtualSize = size; } //{ Assign(m_VirtualSize, (size + 1023) >> 10); }
	void SetPhysicalSize(u32 size)			{ m_PhysicalSize = size; } //{ Assign(m_PhysicalSize, (size + 1023) >> 10); }
#endif // STORE_FILE_SIZES

	/* PURPOSE: Get the number of bytes this file requires in virtual memory. Note that this could be less
	 * than its actual memory requirement if the file is smaller than the size of a buddy heap page.
	 * Use ComputeOccupiedVirtualSize() if you need the actual size it takes up in memory.
	 *
	 * Note that this information isn't always available, for example if this file is inside an RPF that's not resident,
	 * and if the file itself isn't resident either.
	 *
	 * PARAMS:
	 *  mayFail - As noted, the information isn't always available. Passing false here means
	 *  that we really need the size, and the function will assert if we can't get it. Passing
	 *  true will not care. (Note that in __DEV builds, we can get the size through other
	 *  nefarious means, so we'll use those if mayFail is true). This should only be set to true
	 *  in debug code.
	 */
	u32 ComputeVirtualSize(strIndex index, bool mayFail = false, bool dontUseLoadedInfo = false) const;

	/** Like ComputeVirtualSize(), but this function actually returns the amount of memory this file will
	 *  take up, including padding to fill up its buddy heap pages.
	 */
	u32 ComputeOccupiedVirtualSize(strIndex index, bool mayFail = false, bool dontUseLoadedInfo = false) const;
	u32 ComputePhysicalSize(strIndex index, bool mayFail = false) const;

	bool IsCompressed();
	__forceinline bool IsInImage() const {return strIsValidHandle(m_handle) || IsFlagSet(STRFLAG_INTERNAL_DUMMY);}
	__forceinline bool IsValid() const { return strIsValidHandle(m_handle); }

	u32 GetCdPosn(bool forceOptical = true) const;
	u32 GetCdByteSize() const;

	// PURPOSE: Get a bitmask of devices (pgStreamer::Device) that this file is available on.
	u32 GetDevices(strIndex index) const;

	pgStreamer::Device GetPreferredDevice(strIndex index) const;

	bool GetCdPosnAndSectorCount(u32& posn, u32& sectors) const;
	void SetCdPosnAndByteSize(u32 posn, u32 size);

	__forceinline bool IsPackfileHandle() const			{ return IsPackfileHandle(m_handle); }
	__forceinline int GetPackfileIndex() const			{ return GetPackfileIndex(m_handle); }

	strFileHandle GetHandle() const;
	__forceinline void SetHandle(strFileHandle h) { m_handle = h; }		// only for use by live streaming!

#if STREAMING_DETAILED_STATS
	int GetPriority() const;

	void MarkUnloadedTimestamp();
	u32 GetUnloadedTimestamp() const		{ return m_LastUnload; }
#endif // STREAMING_DETAILED_STATS


#if USE_PAGED_POOLS_FOR_STREAMING
#else // USE_PAGED_POOLS_FOR_STREAMING
	// Deprecated - don't use this anymore.
	strIndex GetIndex() const {return strIndex(ptrdiff_t_to_int(this - strStreamingInfo::m_pArrayBase));}
#endif // USE_PAGED_POOLS_FOR_STREAMING

#if !__NO_OUTPUT
	const char *GetFriendlyStatusName() const	{ return GetFriendlyStatusName(GetStatus()); }
#endif // !__NO_OUTPUT


	static void SetArrayBase(strStreamingInfo* pBase) {m_pArrayBase = pBase;}

	static __forceinline u32 MakePackfileHandle(int index)	{ return ((u32) index | HANDLE_TYPE_PACKFILE); }
	static __forceinline bool IsPackfileHandle(u32 handle)	{ return (handle & HANDLE_TYPE_MASK) == HANDLE_TYPE_PACKFILE; }
	static int GetPackfileIndex(u32 handle) 	{ streamAssert(IsPackfileHandle(handle)); return (int) (handle & HANDLE_PACKFILE_MASK); }
	static strFileHandle GetHandle(u32 handle);

#if !__NO_OUTPUT
	static const char *GetFriendlyStatusName(int status);
#endif // !___NO_OUTPUT

#if __ASSERT
	static void DumpLoadingStateCb();
#endif // __ASSERT

private:
	u32			m_handle;					// Low-level streaming handle

	u32			m_status:2,					// STRINFO_...
				m_dependentCount:14,		// Number of people depending on us
				m_flags:16;					// STRFLAG_...; currently nearly all are used.

#if STORE_FILE_SIZES
	u32			m_VirtualSize;				
	u32			m_PhysicalSize;				
#endif // STORE_FILE_SIZES

#if STREAMING_DETAILED_STATS
	// This is the frame count at which this asset was last removed via RemoveObject when
	// it was loaded.
	u32			m_LastUnload;
#endif // STREAMING_DETAILED_STATS


	static strStreamingInfo*		m_pArrayBase;
};

#if USE_PAGED_POOLS_FOR_STREAMING
struct StreamingInfoPageAllocator;

typedef atPagedPoolHeader<strStreamingInfo, true> strStreamingInfoPageHeader;
typedef atPagedPool<strStreamingInfo, true, StreamingInfoPageAllocator> strStreamingInfoPool;

struct StreamingInfoPageAllocator
{
	static strStreamingInfo *Allocate(u32 page, strStreamingInfoPageHeader &header, u32 elementCount, u32 alignment, size_t *memUsage);
	static void Free(u32 page, u32 elementCount, strStreamingInfo *data, strStreamingInfoPageHeader &header, size_t *memUsage);
	static size_t ComputePageMemUsage(u32 subType, u32 elementCount);
};

#endif // USE_PAGED_POOLS_FOR_STREAMING

class strRequestInfo
{
public:
	DLinkSimple<strRequestInfo>	m_Link;
	strIndex					m_Index;
	u32							m_PhysicalSortKey;

#if __DEV
	// If not 0: If not resident yet, timestamp at which we started the request,
	// if resident, the delay until the request was started.
	float						m_StartRequestTime;
#endif // __DEV

#if STREAMING_DETAILED_STATS
	// Timestamp at which the file was initially requested
	u32							m_RequestTime;

	// Timestamp at which the streaming request was issued

	// If not 0: If not resident yet, timestamp at which we started the request,
	// if resident, the delay until the request was started.
	u32							m_StartIssueTime;
#endif // STREAMING_DETAILED_STATS
};

// This is a meta-entry for the sorted list of requests.
struct strRequestSortEntry
{
	enum
	{
		// m_SortKey is set to this in deleted entries.
		// Must be high so the entry gets pushed to the back of the list.
		DELETED = 0x7fffffff,
	};

	bool IsDeleted() const				{ return m_SortKey == DELETED; }
	bool IsPriority() const				{ return m_Priority; }

	u32				m_SortKey : 31;
	u32				m_Priority : 1;
	strIndex		m_Index;
};

CompileTimeAssert(sizeof(strRequestSortEntry) == 8);

class strLoadingInfo
{
public:
#if STREAMING_DETAILED_STATS
	u32 m_StartTime;
	u32 m_LoadTime;
	bool m_IsPriority;
#endif // STREAMING_DETAILED_STATS
};

class strLoadedInfo
{
public:
	DLinkSimple<strLoadedInfo>	m_Link;
	strIndex					m_Index;

	void SetVirtualSize(u32 size)			{ Assign(m_VirtualSize, (size + g_rscVirtualLeafSize - 1) / g_rscVirtualLeafSize); }
	void SetPhysicalSize(u32 size)			{ Assign(m_PhysicalSize, (size + g_rscPhysicalLeafSize - 1) / g_rscPhysicalLeafSize); }

	u32 GetVirtualSize() const				{ return ((u32) m_VirtualSize) * g_rscVirtualLeafSize; }
	u32 GetPhysicalSize()const				{ return ((u32) m_PhysicalSize) * g_rscPhysicalLeafSize; }

	strStreamingModule *GetModule() const	{ return m_Module; }
	void SetModule(strStreamingModule *module)	{ m_Module = module; }

private:
	u16							m_VirtualSize;			// In multiples of g_rscVirtualLeafSize
	u16							m_PhysicalSize;			// In multiples of g_rscPhysicalLeafSize
	strStreamingModule *		m_Module;
};

typedef atMapMemoryPool<strIndex, strRequestInfo, 4> RequestInfoMemoryPool;
typedef atMap<strIndex, strRequestInfo, atMapHashFn<strIndex>, atMapEquals<strIndex>, RequestInfoMemoryPool::MapFunctor > RequestInfoMap;
typedef RequestInfoMap::Entry RequestInfoMapEntry;
typedef DLListSimple<DLIST_SIMPLE_INIT(strRequestInfo, m_Link)> RequestInfoList;

typedef atMapMemoryPool<strIndex, strLoadingInfo, 4> LoadingInfoMemoryPool;
typedef atMap<strIndex, strLoadingInfo, atMapHashFn<strIndex>, atMapEquals<strIndex>, LoadingInfoMemoryPool::MapFunctor > LoadingInfoMap;
typedef LoadingInfoMap::Entry LoadingInfoMapEntry;

typedef atMapMemoryPool<strIndex, strLoadedInfo, 4> LoadedInfoMemoryPool;
typedef atMap<strIndex, strLoadedInfo, atMapHashFn<strIndex>, atMapEquals<strIndex>, LoadedInfoMemoryPool::MapFunctor > LoadedInfoMap;
typedef LoadedInfoMap::Entry LoadedInfoMapEntry;
typedef DLListSimple<DLIST_SIMPLE_INIT(strLoadedInfo, m_Link)> LoadedInfoList;

#if ENABLE_DEBUG_HEAP
// Custom atMap and Memory pool for speeding up GetObjectAndDependenciesSizes
#if RSG_PC
#define OBJECT_MAP_MEMORY_POOL_DEFAULT_SIZE (13297) // Prime size for map, see map.cpp atHashNextSize
#else
#define OBJECT_MAP_MEMORY_POOL_DEFAULT_SIZE (7841) // Prime size for map, see map.cpp atHashNextSize
#endif
typedef atMapMemoryPool<u32, strIndex,4> ObjectDependenciesMapMemoryPool;
typedef atSimplePooledMapType<u32,strIndex, 16, atMapPoolAllocFailPolicy, atMapHashFn<u32>, atMapEquals<u32> > ObjectDependenciesMap;
#endif


class strStreamingInfoIterator
{
public:
	strStreamingInfoIterator();
	void operator++();
	strIndex operator *() const;
	bool IsAtEnd() const;
	void Reset();

private:
#if USE_PAGED_POOLS_FOR_STREAMING
	atPagedPoolIterator<strStreamingInfo, StreamingInfoPageAllocator> m_Iterator;
#else // USE_PAGED_POOLS_FOR_STREAMING
	u32 m_Index;
#endif // !USE_PAGED_POOLS_FOR_STREAMING
};



// Manages the array of modelinfo's and their state.
class strStreamingInfoManager
{
public:
	typedef atArray<strRequestSortEntry> SortRequestArray;
	BANK_ONLY(static bool		ms_bValidDlcOverlay;)

#if __DEV
	enum {
		REQUEST_LOG_SIZE = 32,
	};
#endif // __DEV

	struct FileInfo
	{
		strIndex m_Index;
		atString m_Filename;
	};

	typedef atArray<FileInfo> FileInfoList;



	strStreamingInfoManager();
	~strStreamingInfoManager();

	void Init();
	void InitLevelWithMapUnloaded();
	void ResetStreamingStats();

	void AllocateStreamingInfoArray();

	void Update();

//	This is currently not being called but I think it should be when the map data is unloaded.
//	It's called by strStreamingEngine::ShutdownLevelWithMapLoaded() which is currently commented out.
//	Maybe the call to it was lost when the new init/shutdown system went in.
//	void ShutdownLevelWithMapUnloaded();

	void Shutdown();

	// init models

	/** PURPOSE: Register one streamable file with the streaming system.
	 *  This function will identify the streaming module based on the file's extension and
	 *  register it via strStreamingModule::Register().
	 *
	 *  PARAMS:
	 *   filename - Name of the file to be registered.
	 *   handle - Handle of the file (as used by fiDevice/fiCollection).
	 *   imgIndex - Image index.
	 *   prevIndex - Streaming index of the previous file that was just registered, or
	 *               strIndex::INVALID_INDEX if this is the first file being added to any given archive.
	 *
	 *  RETURNS:
	 *   The streaming index for the object that was just registered, or strIndex::INVALID_INDEX if
	 *   something went wrong.
	 */
	strIndex RegisterObject(const char* filename, strFileHandle handle, s32 imgIndex, bool& bEncrypted, bool quitOnBadVersion = true, fiPackEntry *peOverride = NULL,bool overlayLooseFile=false);
	strIndex RegisterDummyObject(const char* name, strLocalIndex objectIndex, s32 module);

	// PURPOSE: Make the streaming system forget a specific streaming object exists.
	bool UnregisterObject(strIndex index);

	void SetDoNotDefrag(strIndex index);

	strStreamingModuleMgr& GetModuleMgr() {return ms_moduleMgr;}
	const strStreamingModuleMgr& GetModuleMgr() const {return ms_moduleMgr;}

	// state changes.
	strRequestInfo& AddRequest(strIndex index);
	strRequestInfo* GetRequest(strIndex index);

	strLoadingInfo& AddLoading(strIndex index);
	void RemoveLoading(strIndex index);
	strLoadingInfo* GetLoading(strIndex index);

	strLoadedInfo& AddLoaded(strIndex index, u32 virtSize = 0, u32 physSize = 0);
	void RemoveLoaded(strIndex index);
	strLoadedInfo* GetLoaded(strIndex index);

	strLoadedInfo& AddPersistentLoaded(strIndex index, u32 virtSize = 0, u32 physSize = 0);
	void RemovePersistentLoaded(strIndex index);
	strLoadedInfo* GetPersistentLoaded(strIndex index);

	/** Update our cached information of the total memory usage (including extra memory) of a resource.
	 *  The streamable is expected to be resident at this point.
	 */
	void UpdateLoadedInfo(strIndex index, size_t virtSize, size_t physSize);

	void DestroyDependentGraphs();
	void CreateDependentsGraph(atArray<s32>& archives);

	void GetStrIndiciesForArchive(s32 archiveIndex, atBinaryMap<bool, u32>& strIndicies);

	void FindAllDependents(atArray<strIndex>& dependents, strIndex objectStrIndex);

	bool RequestObject(strIndex index, s32 flags);
	bool RemoveObject(strIndex index, bool skipCompleteRpfCheck = false);
	bool RemoveObjectAndDependencies(strIndex index, u32 flags);
	void RemoveObjectAndDependents(strIndex index, eHierarchyModType modType);
	void ReRequestObject(strIndex index);
	void SetObjectToLoading(strIndex index);
	void SetObjectToLoaded(strIndex index);
	void SetObjectToNotLoadedFromLoading(strIndex index);

	void ForceLoadDummyRequest(strIndex index);

	const atArray<s32>* GetPackfileRelationships(s32 imgIndex);
#if __BANK
	void PrintPackfileRelationships();
	void ValidateRelationships();
#endif

	void GetObjectAndDependenciesSizes(strIndex index, u32& virtualSize, u32& physicalSize, const strIndex* ignoreList=NULL, s32 numIgnores=0, bool mayFail = false);
	void GetObjectAndDependenciesSizes(const atArray<strIndex>& indices, u32& virtualSize, u32& physicalSize, const strIndex* ignoreList=NULL, s32 numIgnores=0, bool mayFail = false);
	void GetObjectAndDependencies(strIndex index, atArray<strIndex>& allDeps, const strIndex* ignoreList=NULL, s32 numIgnores=0);
#if ENABLE_DEBUG_HEAP
	void GetObjectAndDependencies(strIndex index, ObjectDependenciesMap& allDepsMap, atArray<strIndex>& allDeps);
#endif

	// PURPOSE: Invalidate all files used by any specific archive. The files will continue to exist, but their
	// handle will be invalid.
	// PARAMS:
	//    archiveIndex - Archive whose files are to be invalidated.
	//    unregister - If true, the files will be unregistered, not just invalidated. It's like they never existed.
	//                 Think the Romans laying waste to the Carthaginians. Note that files that were overlaid will
	//                 continue to exist as what they were before.
	void InvalidateFilesForArchive(s32 archiveIndex, bool unregister);

	void ModifyHierarchyStatusForArchive(s32 archiveIndex, eHierarchyModType modType);

	void RemountFileList(int packfileIndex, const FileInfoList &fileList);

	void GetFileList(int packfileIndex, FileInfoList &outFileList);

#if __ASSERT
	void PrintRecursiveLoadState(strIndex index, int indent = 0) const;
#endif // __ASSERT


	// object access
#if __BANK
	strIndex GetStreamingIndexByName(const char* name) const;
	void PrintInformation(strIndex index);
#endif

#if USE_PAGED_POOLS_FOR_STREAMING
	strStreamingInfoPool &GetStreamingInfoPool()								{ return m_StreamingInfos; }

	strStreamingInfoPageHeader &GetStreamingInfoHeader(strIndex index)			{ return m_StreamingInfos.GetPageHeader(index); }

	inline strStreamingInfo* GetStreamingInfo(strIndex index)					{ return m_StreamingInfos.Get(index); }
	inline const strStreamingInfo* GetStreamingInfo(strIndex index) const		{ return m_StreamingInfos.Get(index); }
	inline const strStreamingInfo& GetStreamingInfoRef(strIndex index) const	{ return *m_StreamingInfos.Get(index); }
	inline strStreamingInfo& GetStreamingInfoRef(strIndex index)				{ return *m_StreamingInfos.Get(index); }

	inline void SanityCheckStrIndex(strIndex /*index*/) const {}

#else // USE_PAGED_POOLS_FOR_STREAMING

	inline strStreamingInfo* GetStreamingInfo(strIndex index) {return &GetStreamingInfoRef(index);}
	inline const strStreamingInfo* GetStreamingInfo(strIndex index) const {return &GetStreamingInfoRef(index);}
	inline const strStreamingInfo& GetStreamingInfoRef(strIndex index) const {return m_aInfoForModel[index.Get()];}
	inline strStreamingInfo& GetStreamingInfoRef(strIndex index) {return m_aInfoForModel[index.Get()];}
	bool IsStreamingInfoInitialized() const					{ return m_aInfoForModel.GetCount() != 0; }

	void SanityCheckStrIndex(strIndex ASSERT_ONLY(index)) const			{ streamAssertf((u32) index.Get() < (u32) m_aInfoForModel.GetCount(), "Invalid strIndex - %d, limit is %d", index.Get(), m_aInfoForModel.GetCount()); }

#endif // USE_PAGED_POOLS_FOR_STREAMING


	u32 GetStreamingInfoSize() const {return m_size;}

	// requests & loaded list access
	s32 GetNumberOfObjectsLoaded() { return m_loadedInfoMap.GetNumUsed() + m_loadedPersistentInfoMap.GetNumUsed(); }
	s32 GetNumberObjectsRequested() {return m_numRequests;}
	s32 GetNumberRealObjectsRequested() {return m_numRealRequests;}
	s32 GetNumberRealObjectsRequested_Cached() {return sm_numRealRequests_Cached;}
	s32 GetNumberPriorityObjectsRequested() {return m_numPriorityRequests;}

	const RequestInfoMap& GetRequestInfoMap() const { return m_requestInfoMap; }
	RequestInfoMap* GetRequestInfoMap() {return &m_requestInfoMap; }
	RequestInfoList* GetRequestInfoList() {return &m_requestInfoList; }

	const LoadingInfoMap& GetLoadingInfoMap() const { return m_loadingInfoMap; }
	LoadingInfoMap& GetLoadingInfoMap() { return m_loadingInfoMap; }

	atArray<strIndex>& GetDummyRequests() { return m_dummyRequestList; }

	const LoadedInfoMap& GetLoadedInfoMap() const { return m_loadedInfoMap; }
	LoadedInfoMap* GetLoadedInfoMap() {return &m_loadedInfoMap; }
	LoadedInfoList* GetLoadedInfoList() {return &m_loadedInfoList; }
	const LoadedInfoMap& GetLoadedPeristentInfoMap() const { return m_loadedPersistentInfoMap; }
	LoadedInfoMap* GetLoadedPersistentInfoMap() { return &m_loadedPersistentInfoMap; }

	const LoadedInfoMemoryPool &GetLoadedInfoPool() const	{ return m_loadedInfoPool; }

	s32 GetRequestInfoPoolCount() const { return m_requestInfoPool.GetUsedCount(); }
	s32 GetRequestInfoPoolCapacity() const;

#if SANITY_CHECK_REQUESTS
	void SanityCheckRequests();
#endif
#if SANITY_CHECK_LIST
	void SanityCheckInfo(strIndex index);
	void SanityCheckList();
#endif

#if __BANK
	void PrintRequestList();
	void PrintLoadedList(bool includeDummies, bool includeDependencies = true, bool asCSV = false, bool requiredOnly = false, strResidentStatus status = STRINFO_LOADED);
	void PrintLoadedListByModule(bool includeDummies, bool requiredOnly, strResidentStatus status);
    void PrintDependenciesRecurse(strIndex index, int indent = 0, bool asCSV = false);
	void PrintLoadingDependents(strIndex index);
	void PrintAllDependents(strIndex index);
	void PrintIfDependent(strIndex index, strIndex dependency);
	void DebugShowImageContents(strStreamingFile &file);
	void DisplayRequestStats();
#if __DEV
	void DisplayStreamingOverview(int requestLines, int completedLines, int rpfCacheMissLines, int oomLines, int deletedObjectsLines);
#endif // __DEV
#endif // __BANK

#if __BANK
	void LogCompletedObject(const char *name, int virtSize, int physSize, bool dummy);
	void LogDeletedObject(const char *name, int virtSize, int physSize);
	void LogFailedAllocation(const char *name, int virtSize, int physSize, bool fragmented, bool wasLastAllocPhysical, size_t failedAllocChunkSize);
	bool *GetLogActivityToTTYPtr()			{ return &m_LogActivityToTTY; }
	void DumpResidentObjects(u32 flagCheck);
#endif // __BANK

	void SetBGScriptIdx(strIndex idx){m_bgScriptIdx=idx;}

	bool PurgeRequestList(u32 ignoreFlags=STR_DONTDELETE_MASK|STRFLAG_FORCE_LOAD, bool noDependencies = false);
	bool FlushLoadedList(u32 ignoreFlags=STR_DONTDELETE_MASK);

	void ResetInfoIteratorForDeletingNextLeastUsed();
	bool DeleteNextLeastUsedObject(u32 ignoreFlags, strStreamingModule *ignoreModule = NULL, bool useTerminator = true);

	// cd layout
	int GetNextRequestsOnCd(u32 cdPosn, bool bUsePriority, strIndex* indexArray, int arraySize, u32 deviceMask, u32 seekTolerance);
	int GetNextFilesOnCdNew(u32 cdPosn, bool bUsePriority, strIndex* indexArray, int arraySize, u32 deviceMask, u32 seekTolerance);

	// flags
	void SetObjectIsDeletable(strIndex index) {ClearRequiredFlag(index, STRFLAG_DONTDELETE);}
	void SetMissionDoesntRequireObject(strIndex index) {ClearRequiredFlag(index, STRFLAG_MISSION_REQUIRED);}
	void SetCutsceneDoesntRequireObject(strIndex index) {ClearRequiredFlag(index, STRFLAG_CUTSCENE_REQUIRED);}
	void SetInteriorDoesntRequireObject(strIndex index) {ClearRequiredFlag(index, STRFLAG_INTERIOR_REQUIRED);}
	void SetObjectIsNotDeletable(strIndex index) {SetRequiredFlag(index, STRFLAG_DONTDELETE);}

	void SetRequiredFlag(strIndex index, u32 flag);
	void ClearRequiredFlag(strIndex index, u32 flag);
	bool IsObjectRequired(strIndex index) const {return (GetStreamingInfoRef(index).IsFlagSet(STR_DONTDELETE_MASK));}

	void SetObjectFlag(strIndex index, u32 flag) {GetStreamingInfoRef(index).SetFlag(index, flag);}
	u32 GetObjectFlags(strIndex index) {return GetStreamingInfoRef(index).GetFlags();}
	bool CheckObjectFlag(strIndex index, u32 uiFlag) {return (GetStreamingInfoRef(index).GetFlags() & uiFlag) != 0;}
	void ClearFlagForAll(u32 flag);

	void AddDependentCounts(strIndex dependent);
	void RemoveDependentCountsAndUnrequest(strIndex dependent);

	// queries
	strLocalIndex GetObjectIndex(strIndex index) const;
#if !__NO_OUTPUT
	// PURPOSE: Get the filename of this streamable. NOT THREAD SAFE, AND WILL ERASE THE RESULT OF THE PREVIOUS CALL.
	const char* GetObjectName(strIndex index) const;
	const char* GetObjectPath(strIndex index) const;

	// PURPOSE: Get the filename of this streamable. Thread-safe.
	FASTRETURNCHECK(const char*) GetObjectName(strIndex index, char *buffer, size_t bufferSize) const;
	FASTRETURNCHECK(const char*) GetObjectPath(strIndex index, char *buffer, size_t bufferSize) const;
#endif

	/** PURPOSE:  Create a bit mask of devices that contain the asset and all of its dependencies.
	 *  Returns a bit mask with 1 << pgStreamer::Device for each asset that's involved.
	 *  Dummies don't count as anything.
	 */
	u32 GetDependencyDeviceMask(strIndex index) const;

	void* GetObjectPtr(strIndex index);
	s32 GetObjectVirtualSize(strIndex index, bool mayFail = false) {return GetStreamingInfoRef(index).ComputeVirtualSize(index, mayFail);}
	s32 GetObjectPhysicalSize(strIndex index, bool mayFail = false) {return GetStreamingInfoRef(index).ComputePhysicalSize(index, mayFail);}
	bool IsObjectRequested(strIndex index) {return GetStreamingInfoRef(index).GetStatus() == STRINFO_LOADREQUESTED;}
	bool IsObjectLoading(strIndex index) {return GetStreamingInfoRef(index).GetStatus() == STRINFO_LOADING;}
	bool HasObjectLoaded(strIndex index) {return GetStreamingInfoRef(index).GetStatus() == STRINFO_LOADED;}
	bool IsObjectInImage(strIndex index);
	bool IsObjectInUse(strIndex index);
	bool IsObjectDeletable(strIndex index, u32 flags = STR_DONTDELETE_MASK);
	bool IsObjectReadyToDelete(strIndex index, u32 flags);
	bool IsObjectNew(strIndex index);
	strIndex GetStreamingIndexByFilename(const char* filename, s32 module);
	u8 GetModuleId(strIndex index) const;
	strStreamingModule* GetModule(strIndex index);
	const strStreamingModule* GetModule(strIndex index) const;
	strStreamingModule* GetModuleForDummy(strIndex index);

	void SnapshotLastLoaded();
	void ClearLastLoaded()						{ m_LastLoadedPrevFrame = NULL; }

#if STREAM_STATS
	u32 GetDeleted() { return m_Deleted; }
	u32 GetPurged() { return m_Purged; }
	u32 GetDeletedSize() { return m_DeletedSize; }
#endif // STREAM_STATS

	static bool *GetSortOnHDDPtr()			{ return &sm_SortOnHDD; }
#if __BANK
	static bool *GetFairHDDSchedulerPtr()	{ return &sm_FairHDDScheduler; }
#endif // __BANK

	static strIndex GetStreamingIndexFromMemBlock(sysMemAllocator *allocator, const void *memPtr);
	static strIndex GetStreamingIndexFromMemBlock(const void *memPtr);
	static u32 CreateUserDataFromStreamingIndex(strIndex index);
	static strIndex GetStreamingIndexFromUserData(u32 userData);

private:
	void InitLoaded(strLoadedInfo& loaded, strIndex index, u32 virtSize = 0, u32 physSize = 0);

	void SetStatus(strIndex index, strResidentStatus status);

	void CompleteDummyRequest(strIndex index);
	void RemoveRequest(strIndex index);

	void MoveToPersistentLoaded(strIndex index);
	void MoveToLoaded(strIndex index);

	void EnsureRequestListSorted(pgStreamer::Device device);
	void AddRealRequestToSortList(pgStreamer::Device device, strStreamingInfo &info, strRequestInfo &request);
	void RemoveRealRequestFromSortList(pgStreamer::Device device, strRequestInfo &request);

	void AddPackfileRelation(s32 oldFile,s32 newFile);

#if __BANK
    void PrintInformation(int objIndex, strStreamingModule* pModule, bool hasPackfile, strStreamingInfo& info, int indent = 0);
	void PrintInformationAsCSV(int objIndex, strStreamingModule* pModule, bool hasPackfile, strStreamingInfo& info, int indent = 0);
#endif

	static int RequestListFunc(const strRequestSortEntry* a, const strRequestSortEntry* b);

	// If true, this device needs to sort its requests by LSN to minimize seeking.
	static bool NeedsSorting(pgStreamer::Device device)	{ return device == pgStreamer::OPTICAL || sm_SortOnHDD; }

#if __BANK
	static const char *RemoveObjectDiagCb(void *a, void *b);
#endif // __BANK

#if USE_PAGED_POOLS_FOR_STREAMING
	strStreamingInfoPool m_StreamingInfos;
#else // USE_PAGED_POOLS_FOR_STREAMING
	atArray<strStreamingInfo, 16, u32>	m_aInfoForModel;			// array of info about every model on the disk
#endif // USE_PAGED_POOLS_FOR_STREAMING
	u32						m_size;

	RequestInfoMemoryPool	m_requestInfoPool;
	RequestInfoMap			m_requestInfoMap;
	RequestInfoList			m_requestInfoList;
	LoadingInfoMemoryPool	m_loadingInfoPool;
	LoadingInfoMap			m_loadingInfoMap;
	LoadedInfoMemoryPool	m_loadedInfoPool;
	LoadedInfoMap			m_loadedInfoMap;
	LoadedInfoList			m_loadedInfoList;
	LoadedInfoMap			m_loadedPersistentInfoMap;
	atArray<strIndex>		m_dummyRequestList;
	SortRequestArray		m_SortedPhysicalRequests[pgStreamer::DEVICE_COUNT];
	bool					m_SortedRequestListDirty[pgStreamer::DEVICE_COUNT];
	int						m_LastGetNextFilesIndex[pgStreamer::DEVICE_COUNT];
	strLoadedInfo			*m_LastLoadedPrevFrame;

	// Master table of all files that have been overlaid by an RPF. Key is the file in question,
	// value is the handle of where the file originally was. (The handle contains the original packfile.)
	atMap<strIndex, u32>	m_OverlayTable;
	atMap<s32,atArray<s32>*> m_packFileRelationshipMap;


	atArray<atArray<strIndex> > m_dependentGraphs;
	atMap<s32, atArray<strIndex>*> m_dependentGraphsLookup;

#if STORE_FILE_SIZES
	// Companion map for m_OverlayTable - this holds the file sizes of each file that is in an overlay.
	// This map is in debug memory.
	// Note that I'd love to use size_t here, but everybody else in the world is using u32 for file sizes.
	atMap<strIndex, std::pair<u32, u32> >	m_OverlayFileSizeTable;
#endif // STORE_FILE_SIZES

#if ENABLE_DEBUG_HEAP
	// Pool and Map for for speeding up object dependencies checking
	ObjectDependenciesMapMemoryPool m_ObjectDependenciesMapMemoryPool;
	ObjectDependenciesMap m_ObjectDependenciesMap;
#endif

	strStreamingModuleMgr ms_moduleMgr;

	s32				m_numRequests;						// The number of models in requested list.
	s32				m_numRealRequests;					// The number of models in requested list that aren't dummy requests.
	s32				m_numPriorityRequests;

	LoadedInfoList::Iterator m_InfoIteratorForDeleteNextLeastUsed;

#if __BANK
	int				m_numMissingPackfileDpendencies;	// Number of times a request was delayed due to a missing RPF
	int				m_numTotalRequests;					// Total number of requests done since last reset
	int				m_numTotalDeletedFiles;				// Total number of objects that were deleted to make room
	int				m_numTotalCompletedFiles;			// Total number of objects that were completed
	int				m_numTotalOOMFailedLoads;			// Total number of objects that couldn't be loaded due to lack of memory
	int				m_numRealRequestAdds;				// Total number of RemoveRequests
	int				m_numRealRequestRemoves;			// Total number of AddRequests
	int				m_numLastRealRequestAdds;			// Total number of RemoveRequests in the last frame
	int				m_numLastRealRequestRemoves;		// Total number of AddRequests in the last frame
	int				m_numListSorts;						// Number of times the request list was sorted this frame
	float			m_numAverageListSorts;				// Average number of times the request list was sorted this frame
	float			m_numAverageRealRequestAdds;		// Average number of RemoveRequests per frame
	float			m_numAverageRealRequestRemoves;		// Average number of AddRequests per frame
	bool			m_LogActivityToTTY;					// Mirror all logs to the TTY
#endif // __BANK
	strIndex		m_bgScriptIdx;						//background script has it's rpf deleted after it is registered, causing cleanup asserts. we keep track of it for this reason

#if __ASSERT
	bool			m_UnrequestsLocked;					// If true, nothing is supposed to be unrequested.
#endif // __ASSERT

#if STREAMING_DETAILED_STATS
	u32				m_ChurnRequests[CHURN_TRACKING_LENGTH];
	u32				m_TotalReqToLoadTime[STREAMING_SCHEDULER_COUNT][STREAMING_PRIORITY_COUNT];				// Sum of all "request to load" times
	u32				m_ReqToLoads[STREAMING_SCHEDULER_COUNT][STREAMING_PRIORITY_COUNT];						// Number of times a request was turned to a load
	u32				m_TotalLoadToResidentTime[STREAMING_SCHEDULER_COUNT][STREAMING_PRIORITY_COUNT];			// Sum of all "load to resident" times
	u32				m_ReqToLoadeds[STREAMING_SCHEDULER_COUNT][STREAMING_PRIORITY_COUNT];					// Number of times a request was fully finished
	u32				m_TotalReqToResidentTime[STREAMING_SCHEDULER_COUNT][STREAMING_PRIORITY_COUNT];			// Sum of all "request to resident" times
#endif // STREAMING_DETAILED_STATS

#if STREAM_STATS
	int				m_Purged;
	int				m_Deleted;
	int				m_DeletedSize;
#endif

	static bool		sm_SortOnHDD;						// Sort by LSN even on HDD
	static bool		sm_FairHDDScheduler;

	static s32 sm_numRealRequests_Cached;
};

inline unsigned atHash(const strIndex index)
{
	return index.Get();
}

class strLoadedInfoIterator{
public:
	strLoadedInfoIterator(s32 streamingModuleId = -1);
	~strLoadedInfoIterator() {}

	strStreamingInfo* GetNext();
	strIndex GetNextIndex();

private:
	int hashpos;
	LoadedInfoMap::Entry* cn;
	LoadedInfoMap* loadedMap;
	LoadedInfoMap* persistentMap;

	s32 m_streamingModule;
};

} // namespace rage

#endif // STREAMING_STREAMINGINFO_H
