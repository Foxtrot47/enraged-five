//
// streaming/streaminginstall.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "streaminginstall.h"

namespace rage {

fiHandle fiInstallerDevice::Open(const char *filename,bool readOnly) const
{
	return const_cast<fiInstallerDevice*>(this)->InstallFile(filename, readOnly);
}
fiHandle fiInstallerDevice::OpenBulk(const char *filename, u64& outBias) const
{
	outBias = 0;
	return const_cast<fiInstallerDevice*>(this)->InstallFile(filename, true);
}
int	fiInstallerDevice::Read(fiHandle handle, void *outBuffer,int bufferSize) const
{
	return fiDeviceLocal::GetInstance().Read(LocalHandle(handle), outBuffer, bufferSize);
}
int	fiInstallerDevice::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const
{
	return fiDeviceLocal::GetInstance().ReadBulk(LocalHandle(handle), offset, outBuffer, bufferSize);
}
u64	fiInstallerDevice::GetFileSize(const char *filename) const
{
	char destname[2048];
	GetInstalledPath(destname, 2048, filename);
	return fiDeviceLocal::GetInstance().GetFileSize(destname);
}
u64	fiInstallerDevice::GetFileTime(const char *filename) const
{
	char destname[2048];
	GetInstalledPath(destname, 2048, filename);
	return fiDeviceLocal::GetInstance().GetFileSize(destname);
}
bool fiInstallerDevice::SetFileTime(const char *filename,u64 timestamp) const
{
	char destname[2048];
	GetInstalledPath(destname, 2048, filename);
	return fiDeviceLocal::GetInstance().SetFileTime(destname, timestamp);
}
int	fiInstallerDevice::Seek(fiHandle handle, int offset, fiSeekWhence whence) const
{
	return fiDeviceLocal::GetInstance().Seek(LocalHandle(handle), offset, whence);
}
fiHandle fiInstallerDevice::Create(const char* /*filename*/) const
{
	return fiHandleInvalid;
}
fiHandle fiInstallerDevice::CreateBulk(const char* /*filename*/) const
{
	return fiHandleInvalid;
}
int	fiInstallerDevice::Write(fiHandle handle, const void* buf, int bufsize) const
{
	return fiDeviceLocal::GetInstance().Write(LocalHandle(handle), buf, bufsize);
}
int	fiInstallerDevice::WriteBulk(fiHandle handle, u64 offset, const void* buf, int bufsize) const
{
	return fiDeviceLocal::GetInstance().WriteBulk(LocalHandle(handle), offset, buf, bufsize);
}
int fiInstallerDevice::CloseBulk(fiHandle handle) const
{
	return Close(handle);	
}

}	// namespace rage
