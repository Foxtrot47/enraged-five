//
// filename:	streamingmodulemgr.h
// description:	
//

#ifndef INC_STREAMINGMODULEMGR_H_
#define INC_STREAMINGMODULEMGR_H_

// --- Include Files ------------------------------------------------------------

// C headers
// Rage headers
#include "atl/array.h"
#include "data/base.h"
#include "streaming/streamingdefs.h"

// Game headers

namespace rage {

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

class bkGroup;
class strStreamingModule;

class strStreamingModuleMgr : public datBase
{
	struct ModuleIndexInfo
	{
		strStreamingModule *m_Module;
		strIndex			m_LastIndex;
		int					m_ModuleIndex;
	};

public:
	void Init();
	void Shutdown();

#if __ASSERT
	static void DisallowModuleAdd();
	static bool IsModuleAddDisallowed();
#endif

	int AddModule(strStreamingModule* pModule);

	// name:		GetStreamingModule
	// description:	Given a streaming index return the related module and the index into this module
	strStreamingModule* GetModule(s32 module) {return m_moduleArray[module];}
	const strStreamingModule* GetModule(s32 module) const {return m_moduleArray[module];}
	int GetNumberOfModules() const {return m_moduleArray.GetCount();}

	// name:		GetStreamingModule
	// description:	Given a streaming index return the related module and the index into this module
	strStreamingModule* GetModuleFromIndex(strIndex streamingIndex) const;
	u8 GetModuleIdFromIndex(strIndex streamingIndex) const;

	const char *GetFileExtensionFromModule(const strStreamingModule *module) const;

	// name:		GetModuleIndexFromFileExtension
	// description:	Return a module index given a file extension
	strStreamingModule* GetModuleFromFileExtension(const char* extension) const;
	// name:		GetTotalStreamingEntries
	// description:	Return the number of objects that can be registered with the streaming modules. 
	//				Basically the entry count of all the modules added up
	int GetTotalStreamingEntries();

	// name:		IsModuleRequiredForFileExtension
	// description:	Return true if a streaming module is required for this extension
	//				i.e. #mf - rpf manifest file does not required a module
#if !__FINAL
	bool IsModuleRequiredForFileExtension(const char* extension) const;
#endif

	void PrintModules();

#if !USE_PAGED_POOLS_FOR_STREAMING
	// PURPOSE: Cache the index ranges of each module. This cache is used to
	// make GetModuleFromIndex faster.
	void CacheModuleIndexRanges();
#endif // !USE_PAGED_POOLS_FOR_STREAMING

#if __BANK
	void ResetLoadedCounts();
	void DumpPagedPoolStats();
#endif

	void DumpPlaceStats();
	void ResetPlaceStats();

	const char* getPlatExtFromExtPattern(const char* extPattern);

#if __BANK
	void AddWidgets(bkGroup &group);
#endif // __BANK

private:
#if !USE_PAGED_POOLS_FOR_STREAMING
	const ModuleIndexInfo *GetModuleInfoFromIndex(strIndex index) const;

	static int ModuleIndexInfoSortFunc(const ModuleIndexInfo *a, const ModuleIndexInfo *b);

	atArray<ModuleIndexInfo> m_ModuleIndexRanges;
#endif // !USE_PAGED_POOLS_FOR_STREAMING

	atArray<strStreamingModule*> m_moduleArray;
};

// --- Globals ------------------------------------------------------------------

}	// namespace rage

#endif // !INC_STREAMINGMODULEMGR_H_
