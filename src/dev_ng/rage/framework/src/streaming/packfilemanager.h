
#ifndef STREAMING_PACKFILEMANAGER_H
#define STREAMING_PACKFILEMANAGER_H

#include "streaminginfo.h"

#include "data/base.h"
#include "data/resourceheader.h"
#include "file/handle.h"
#include "paging/streamer.h"


namespace rage {

class bkBank;
class strDefragmentation;
class fiPackfile;
class fiCollection;
class fiDevice;

/** PURPOSE: This class represents an archive with streamable data.
 *  It is typically identified by an "image index" and can be retrieved via
 *  strPackfileManager::GetImageFile().
 */
struct strStreamingFile
{ 
	u64					m_timestamp;
	u64					m_offset;

	// Name of this file
	atFinalHashString	m_name;
	atFinalHashString	m_mappedName;

	// The underlying device for this file
	const fiDevice*		m_device;
	u32					m_physicalSectorAddr;
	fiHandle			m_handle;

	// The header data, as it came in from the streaming system.
	void *				m_RawHeaderData;

	// The packfile for this file. Note that this file may be currently uninitialized
	// and requires a call to ReInit() before it can be used - check m_bInitialized.
	rage::fiCollection*	m_packfile;

	// Number of pending streaming requests for streamables within this file.
	u16					m_requestCount;

	// If true, the fiPackFile has been initialized and is ready for use.
	bool				m_bInitialized;

	// If true, this file is currently being queued up for asynchronous streaming.
	bool				m_IsQueued;

	bool				m_bRegister;
	s8					m_cachePartition;
	u8					m_extraDataIndex;
	u8					m_contentsType;
	bool				m_bNew : 1;

	// If true, we currently have this packfile locked to prevent the system from
	// constantly evicting and reloading files. m_requestCount had been incremented by 1.
	bool				m_Locked : 1;

	// This archive has been locked by the user and should not be evicted, ever, unless
	// we reset the game.
	bool				m_UserLocked : 1;

	// Set to true if this archive is user-locked, but has been temporarily unlocked
	// to allow for hot-reloading the RPF inbetween resets.
	bool				m_UserLockedAndUnlocked : 1;

	// If true, we have to unload this RPF immediately since something has changed (like
	// it may have been remounted on the HDD). Do not use it until it has been reloaded,
	// its data may not be reliable.
	bool				m_bNeedsReset : 1;

#if __DEV
	// If true, then we want to make sure that all files inside this archive are sorted
	// to reduce seeks.
	bool				m_EnforceLsnSorting : 1;
#endif // __DEV

	// If not enabled, do not open this packfile.
	bool				m_Enabled : 1;

	// If true, this packfile will overlay existing files, i.e. it will keep track of everything
	// it replaced and undo it when closed.
	bool				m_IsOverlay : 1;

	// If true, this packfile will overlay existing files
	bool				m_IsPatchfile : 1;

	bool				m_bLoadCompletely : 1;

	// flag to say if pack file contains encrypted data. If this is true we need to keep the name heap around
	bool				m_containsEncryptedData : 1;

	// Size of the header
	size_t				m_CachedHeaderSize;
	
	// The actual low-level handle to the file.
	u32					m_StreamerHandle;

	// Last time this archive was used.
	u32					m_LastUse;

	bool				m_IsOnHdd;
	bool				m_IsOnOdd;

#if __BANK
	u32					m_memoryUsage;

	// Number of times this packfile has been streamed in
	int					m_StreamInCount;
#endif // __BANK
};

class strPackfileManager : public datBase
{
public:
	static bool IsInitialised() {return ms_bIsInitialised;}

	static void Init(int archiveCount);
	static void Shutdown();
	static void Reset();

	static void Update();

	static void InitImageList();
	static void OpenImageList();
	static void CloseImageList();
	static void ShutdownImageList();
	static void SetImageInstalledFlag(const char* filename);
	static void CleanUpInstalledFiles();

	/** PURPOSE: 
	 *  Register a new image file. This function will add it to the list of images, add a reference
	 *  to it, and scan its contents. If this image had already been registered, it will simply
	 *  increment the reference count.
	 *
	 * PARAMS:
	 *  pImageName - full path of the image file, like "platform:/data/myarchive.rpf"
	 *  bRegisterFiles - If true, the system will scan all files within the archive when loading
	 *                   and register them within the streaming system.
	 *  locked - If true, this archive is locked and won't be unloaded if when its reference count hits 0.
	 *  cachePartition - specify the cache partition index for this archive.
	 *  contentsType - Type of contents, used by high-level code. (See CDataFileMgr::DataFileContents as an example)
	 *  loadCompletely - If true, this RPF will be loaded into memory in its entirety, and all its contents will be immediately loaded.
	 *                   Likewise, a file will only be unloaded if all files in this RPF are removable, which will then result in the
	 *                   RPF being removed from memory completely.
	 *  overlay - If true, this RPF is expected to replace some existing files. These files will be put back to their original state
	 *            if this RPF file is closed.
	 *
	 * RETURNS:
	 *  The image index - either a newly created one, or the existing one if the image had been added
	 *  previously already.
	 */
	static s32 AddImageToList(const char* pImageName, bool bRegisterFiles, s32 cachePartition, bool locked, u8 contentsType, bool loadCompletely, bool overlay, bool patchFile=false);

	// PURPOSE: Shut down an archive entirely. This will go as far as closing the file handle on the archive
	// itself. After this call, we have no longer an interest on the RPF, and it's free to be modified.
	// This function will remove locks and ref counts automatically, but it will fail if anything still needs
	// this archive, like a file that is being streamed in.
	static void CloseArchive(s32 archiveIndex);

	// PURPOSE: Unregister an archive, remove all traces of it, including all the files it contained.
	static void UnregisterArchive(s32 archiveIndex);

	// PURPOSE: Enable or disable an archive. This will not do anything immediately, but upon the next call to
	// OpenImageList, disabled packfiles will be skipped.
	static void SetEnabled(s32 archiveIndex, bool enabled)		{ ms_files[archiveIndex].m_Enabled = enabled; }

	// PURPOSE: Returns the image index as identified by string that was passed into AddImageToList.
	// Returns -1 if not found.
	static s32 FindImage(const char *pImageName);

	// PURPOSE: Return TRUE if the packfile with the specified image is managed by the packfile
	// manager, FALSE if it's just a dummy entry.
	static bool IsArchiveManaged(s32 imageIndex)			{ return ms_ManagedCollections.IsSet(imageIndex); }

	// PURPOSE: Remove the user lock on an archive, if there is one. This will make the archive eligible
	// for eviction if all other conditions are met (i.e. its ref count is 0). This will remove the user lock
	// for good.
	static void RemoveUserLock(s32 imageIndex);

	/** PURPOSE: Go through every registered image and load it up if it hasn't been loaded already.
	 */
	static void LoadImageDirectories();

	/** PURPOSE: If this image hasn't be loaded already, it will be loaded now.
	 *  If it had been added with bRegisterFiles, all its contents will be registered with the
	 *  streaming system.
	 *
	 *  PARAMS:
	 *   index - The image index.
	 */
	static void LoadImageIfUnloaded(s32 index, bool checkTimestamp = true);
	static u64 GetImageTime(u32 index);
	static strStreamingFile* GetImageFile(u32 index) {return &ms_files[index];}	
	static s32 GetImageFileCount() { return ms_files.GetCount(); }
	static strLocalIndex GetImageFileIndexFromHandle(u32 handle);
	static strStreamingFile* GetImageFileFromHandle(u32 handle);
	static const char* GetImageFileNameFromHandle(u32 handle);

	static void RemountDependentPackfiles(fiHandle oldHandle, fiHandle newHandle);

	/* PURPOSE: Call this function before deleting a device - this will make sure that any references
	 * to this device will be removed. This is normally not necessary (devices are typically managed
	 * by the packfile manager), but may be required if this is a device that belongs to someone else -
	 * most likely a DLC device.
	 *
	 * PARAMS:
	 *  device - Device to be removed.
	 */
	static void UnregisterDevice(fiDevice *device);

	static bool IsImageForHandleResident(u32 handle);

	static bool IsImageResident(s32 imageIndex);
	static bool IsImageRequested(s32 imageIndex);
	static void RequestImage(s32 imageIndex);

	static void LockArchive(s32 archiveIndex);
	static void UnlockArchive(s32 archiveIndex);

	// Remove a packfile from the LRU list, if it currently is on it. That means that it will be deleted
	// immediately if there are no more references to it.
	static void RemoveFromLRUList(s32 archiveIndex);

	/** PURPOSE: This function will - if there are at least a certain number of packfiles resident -
	 *  find the least recently used packfile and remove its ref count, marking it eligible
	 *  for recycling by the streaming system.
	 *
	 *  PARAMS:
	 *    forceUnlock - If there's at least one packfile, it will be deleted, even if that brings
	 *                  us under the threshold of minimum number of packfiles expected to be resident.
	 *
	 *  RETURNS:
	 *   Packfile index of the file that was marked eligible for unlocking, or -1 if none could be found.
	 */
	static s32 UnlockLRUPackfile(bool forceUnlock);

	/** PURPOSE: This function will call UnlockLRUPackfile to unlock the least recently used packfile (even
	 *  if that brings the number of resident RPFs files below the expected threshold), and
	 *  then immediately frees up the packfile.
	 *
	 *  PARAMS:
	 *    True if a packfile was found and deleted, false otherwise.
	 *
	 *  RETURNS:
	 *    True if we deleted a packfile, false otherwise. True by itself doesn't mean that we saved memory,
	 *    the packfile may not have been resident yet.
	 */
	static bool DeleteLRUPackfile();

	static int GetImageCount()					{ return ms_files.GetCount(); }

	static void RegisterImages();


	/* PURPOSE: Tell the system that a certain image just got one more file that is waiting
	 * to be streamed in.
	 *
	 * PARAMS:
	 *   imageIndex - Index of the image that just got one more reference.
	 */
	static void AddStreamableReference(s32 imageIndex);

	/* PURPOSE: Tell the system that a certain image has one less file that is waiting to be
	 * streamed in. Note that the reference count hitting zero will not automatically
	 * uninitialize the image, it will just make it eligible for garbage collection.
	 *
	 * PARAMS:
	 *   imageIndex - The image index.
	 */
	static void RemoveStreamableReference(s32 imageIndex);

	/** PURPOSE: Return the streaming module used to load archives.
	 */
	static strStreamingModule* GetStreamingModule();
	static s32 GetStreamingModuleId()				{ return ms_StreamingModuleId; }
	
	// Hail-mary memory RPF clean-up
	static void EvictUnusedImages();

#if __BANK
	static void AddWidgets(bkBank &bank);
	static void DebugDraw();
	static void DumpPackfiles();
	static void ShowPackfileCache();
#endif // __BANK

#if __DEV
	static void SetEnforceLsnSorting(s32 imgIndex, bool enforceLsnSorting)	{ ms_files[imgIndex].m_EnforceLsnSorting = enforceLsnSorting; }
	static bool IsEnforceLsnSorting(s32 imgIndex)							{ return ms_files[imgIndex].m_EnforceLsnSorting; }
#endif // __DEV

	static void SetExtraContentIndex(s32 extraData);

	static void UnloadRpf(s32 imgIndex);

	static void OnStreamIn(int imageIndex, void *headerData);
	static int GetRawStreamerImageIndex();

	static void MarkImageQueued(int imageIndex, bool queued = true);

#if __DEV
	static void IncrementNumLoads();
	static void RegisterStreamingDelay(float delay);
#endif // __DEV

	static strIndex RegisterIndividualFile(const char* file, bool quitOnBadVersion = true, const char *relativePath = NULL, bool quitIfMissing = true,bool overlayIfExists = false);
	static void InvalidateIndividualFile(const char* file);

	static int GetFileArrayCapacity() { return ms_files.GetCapacity(); }
	static int GetFileArraySize() { return ms_files.size(); }

protected:
	/** PURPOSE: Load a particular image and register all of its contents
	 *  with the streaming system.
	 *
	 *  PARAMS:
	 *   imageName - Filename of the archive.
	 *   imgIndex - The image index.
	 */
	static bool LoadRpf(const char* name, s32 imgIndex);

	static bool IsFileInstalled(const char* filename);


private:

//	static bool						ms_bDisableStreaming;		
	static bool						ms_bIsInitialised;				
//	static bool						ms_bLoadingPriorityObjects;		

	static atArray<strStreamingFile> ms_files;

	// All packfiles that are currently initialized and have their headers in memory.
	static atArray<strStreamingFile *> ms_LockedFiles;

#if __BANK
	// Sorted list of files that were locked, but aren't anymore
	static atArray<strStreamingFile *> ms_UnlockedFiles;
#endif // __BANK

	// all packfile names that are installed and not yet registered with the packfilemanager
	static atArray<atHashString> ms_installedFiles;

	// This bitset mirrors the collections (fiCollection) and indicates which ones
	// are managed by the packfile manager.
	static atBitSet				ms_ManagedCollections;

	// Number of files that are either locked or queued up
	static int					ms_QueuedCount;

	// ID of the archive streaming module
	static s32					ms_StreamingModuleId;
};

inline bool strPackfileManager::IsImageRequested(s32 imageIndex)
{
	return ms_files[imageIndex].m_IsQueued;
}


}	// namespace rage

#endif // STREAMING_PACKFILEMANAGER_H
