//
// streaming/defragmentation.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_DEFRAGMENTATION_H
#define STREAMING_DEFRAGMENTATION_H

#include "data/resource.h"
#include "diag/channel.h"
#include "diag/trap.h"
#include "streaming/streamingdefs.h"
#include "system/criticalsection.h"
#include "system/buddyallocator.h"

namespace rage {

struct strDefragDebugInfo;
class strStreamingModule;
struct sysMemDefragmentation;
class pgBase;

#define DEFRAG_TIME (0 && !__FINAL)

#define DEBUG_DEFRAG_REFCNT	(__ASSERT)


#if USE_DEFRAGMENTATION

class strDefragmentation
{
public:
	typedef enum 
	{
		DEFRAG_NORMAL,
		DEFRAG_PAUSE,
		DEFRAG_CUTSCENE_INIT,
		DEFRAG_CUTSCENE_RUN
	} eDefragMode;

	void Init();
	void Shutdown();

	void Enable();
	void FlushAndDisable();
	void FlushAndDisable_NoRenderThreadFlush();
	bool& GetEnabled() {return m_Enabled;}

	eDefragMode& GetDefragMode() {return m_DefragMode;}
	void SetDefragMode(eDefragMode mode) {m_DefragMode = mode;}

	void Frack(u32 frack) {FastAssert(!frack); m_Frack |= !!frack;}

	void Update();

	void AddResource(pgBase* resource, datResourceMap& map, strIndex index);
	void RemoveResource(pgBase* resource, strIndex index);

	// Permanently make a resource undefragmentable
	void MakeUndefragmentable(pgBase* resource, strIndex index) {LockMemAllocs(resource, index, 1);}

	// Temporarily prevent defragmentation of a resource
	void Lock(pgBase* resource, strIndex index);
	void Unlock(pgBase* resource, strIndex index);

	void Cancel(strIndex index, bool bBlockOnGpuMemCpyCompletion);
	void Abort();

	// PURPOSE: Call this from a worker thread to temporarily disable defrag. This function will block if the main thread
	// is currently defragmenting, and will resume once the defrag heartbeat has finished.
	void LockDefragCs()						{ m_DefragLock.Lock(); }
	void UnlockDefragCs()					{ m_DefragLock.Unlock(); }

#if DEBUG_DEFRAG_REFCNT
	void DumpDefragInfo();
	strDefragDebugInfo *FindDefragDebugInfo(strStreamingModule *module, s32 objectIndex, bool createIfMissing, int state);
#endif // DEBUG_DEFRAG_REFCNT

#if __BANK
	void DisplayStatus();
	bool *GetDefragoramaBoolPtr();
	bool *GetDontUpdateWhenPausedPtr()		{ return &m_DontUpdateWhenPaused; }
#endif // __BANK

#if ENABLE_DEFRAG_CALLBACK && ENABLE_DEFRAG_DEBUG
	static int s_defragRemoveCurrent;
	static int s_defragRemoveTotal;
	static int s_defragRemovePeak;
#endif

protected:
	u32 CountSiblingSubjects(strIndex index);

	void DefragmentFree(sysMemBuddyAllocator& alloc, void* ptr);
	void DefragmentAllocators();
	void Process(int subjectIndex);
	void ProcessSubjectsTimed(float fTime);
	void ProcessSubjectsIncremental();
	void ProcessFirstSubject();
	bool AddDefragmentableHeapPage(sysMemDefragmentation& defrag, int index, int heap);

#if __DEV 
	void SanityCheck();
#endif

private:
	// State of a subject - adjust s_StateNames if you change those!!
	enum State
	{
		eStateIdle,
		eStateCopyPhysical,
		eStateCopy,
		eStatePlace,
		eStateCheckCopyPhysical,
		eStateWaitOneFrame,
		eStateFinalise
	};

	typedef struct {
		datResourceMap	m_Map;
		State			m_State;
		State			m_ChunkState[datResourceChunk::MAX_CHUNKS];
		PPU_ONLY(u32	m_ChunkCompleted[datResourceChunk::MAX_CHUNKS]);
		strIndex		m_Index;
		u16             m_CopyLockCount;
		s16				m_Next;
		bool			m_bPhysicalOnly;

#if DEFRAG_TIME
		float m_fCopyTime;
		float m_fPlaceTime;
		float m_fExchangeTime;
#endif
	} Subject;

	enum
	{
		NUM_HEAPS = (2 - ONE_STREAMING_HEAP),
		MAX_SUBJECTS = __PPU?24:16
	};

	Subject	m_Subjects[MAX_SUBJECTS];
	int m_Frack:2;
	int m_PendingCopies:30;

	// Critical section - this is only used for other threads to temporarily disable defrag.
	sysCriticalSectionToken m_DefragLock;

	eDefragMode m_DefragMode;

	s16 m_FirstFreeSubject;
	s16 m_FirstUsedSubject;
	bool m_Enabled;

#if __BANK
	float m_AverageProcessTime;
	float m_AveragePageSize[2];
	bool m_DontUpdateWhenPaused;
	u32 m_AllocDefragCalls;		// Number of times we called alloc.Defragment()
	u32 m_DefragStarts;
	u32 m_Heartbeats;
	u32 m_CockblockedByMtPlacement;
#endif // __BANK


	void LockMemAllocs(pgBase* resource, strIndex index, unsigned lockCount);
	void UnlockMemAllocs(pgBase* resource, strIndex index, unsigned unlockCount);
};

#endif // USE_DEFRAGMENTATION

}		// namespace rage

RAGE_DECLARE_CHANNEL(Defrag)

#define defragErrorf(fmt,...)				RAGE_ERRORF(Defrag,fmt,##__VA_ARGS__)
#define defragWarningf(fmt,...)				RAGE_WARNINGF(Defrag,fmt,##__VA_ARGS__)
#define defragDisplayf(fmt,...)				RAGE_DISPLAYF(Defrag,fmt,##__VA_ARGS__)
#define defragDebugf1(fmt,...)				RAGE_DEBUGF1(Defrag,fmt,##__VA_ARGS__)
#define defragDebugf2(fmt,...)				RAGE_DEBUGF2(Defrag,fmt,##__VA_ARGS__)

#endif // STREAMING_DEFRAGMENTATION_H
