<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

  <structdef type="rage::strResourceReference" name="strResourceReference" simple="true">
    <string name="m_AssetName" type="atString" />
    <string name="m_Extension" type="member" size="8" />
  </structdef>

  <structdef type="rage::strRecordedRequest" name="strRecordedRequest" simple="true">
    <struct name="m_Resource" type="rage::strResourceReference" />
    <int name="m_Flags" type="s32" />
  </structdef>

  <structdef type="rage::strRequestRecording" name="strRequestRecording" simple="true">
    <array name="m_Requests" type="atArray">
      <struct type="rage::strRecordedRequest" />
    </array>
  </structdef>


</ParserSchema>
