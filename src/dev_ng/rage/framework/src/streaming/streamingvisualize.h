#ifndef STREAMING_STREAMINGVISUALIZE_H
#define STREAMING_STREAMINGVISUALIZE_H

#include "atl/array.h"
#include "data/base.h"
#include "file/handle.h"
#include "paging/streamer.h"
#include "streaming/streamingdefs.h"
#include "system/criticalsection.h"
#include "system/messagequeue.h"
#include "vectormath/vec3v.h"
#include "vector/color32.h"
#include "system/timer.h"

#define STREAMING_VISUALIZE	((PGSTREAMER_DEBUG && !__FINAL) && !__RESOURCECOMPILER)	//Don't want STREAMING_VISUALIZE on __FINAL builds at all, not everything uses the correct define to check against

#if STREAMING_VISUALIZE

#define STRVIS_ONLY(_x)	_x

namespace rage 
{
class bkGroup;
class fwEntity;
class fwMapData;
class fwMapDataDef;
class spdAABB;
class Vector3;


class strStreamingVisualize : public datBase
{
	typedef s32 EntityID;

public:
	enum
	{
		PROTOCOL_VERSION = 22,
	};

	enum strPacketType
	{
		SV_INIT = 1,
		SV_REGISTEROBJECT = 2,
		SV_REGISTERMODULE = 3,
		SV_REQUEST = 4,
		SV_UNREQUEST = 5,
		SV_SET_STATUS = 6,
		SV_FRAME = 7,
		SV_REGISTERDEVICE = 8,
		SV_ISSUEREQUEST = 9,
		SV_PROCESSREQUEST = 10,
		SV_FINISHREQUEST = 11,
		SV_SETCONTEXT = 12,
		SV_CANCEL = 13,
		SV_ADD_MARKER = 14,
		SV_ADD_ENTITY = 15,
		SV_DELETE_ENTITY = 16,
		SV_UPDATE_ENTITY = 17,
		SV_REGISTER_ENTITY_TYPE = 18,
		SV_STREAMER_READ = 19,
		SV_FAILED_ALLOCATION = 20,
		SV_ADD_STREAMING_VOLUME = 21,
		SV_LOWLEVEL_OPEN = 22,
		SV_LOWLEVEL_CLOSE = 23,
		SV_LOWLEVEL_READ = 24,
		SV_LOWLEVEL_WRITE = 25,
		SV_LOWLEVEL_SEEK = 26,
		SV_MISSING_LOD = 27,
		SV_DEPENDENCY_INFO = 28,
		SV_UNREGISTER_OBJECT = 29,
		SV_REGISTER_MAPDATA = 30,
		SV_INSTANTIATE_MAPDATA = 31,
		SV_ADD_MAPDATA_ENTITY = 32,
		SV_SET_MARKER_TEXT = 33,
		//SV_ADD_VISIBLE_ENTITIES = 34,
		//SV_REMOVE_VISIBLE_ENTITIES = 35,
		SV_REGISTER_LOD_TYPE = 36,
		SV_ADD_TO_NEEDED_LIST = 37,
		SV_SET_EXTRA_CLIENT_INFO = 38,
		SV_DEVICE_IDLE = 39,
		SV_LOWLEVEL_READEND = 40,
		SV_STREAMER_DECOMPRESS = 41,
		SV_UPDATE_VISFLAGS = 42,
		SV_REGISTER_VISFLAG = 43,
		SV_SET_LOD_PARENT = 44,
		SV_ADD_BOX_SEARCH = 45,
		SV_REGISTER_NAMED_FLAG = 46,
		SV_SET_FLAGS = 47,
		SV_OPEN_RAW_FILE = 48,
		SV_CHANGE_GAME_STATE = 49,
		SV_REGISTER_STATE_FLAG = 50,
		SV_ADD_CULLBOX = 51,
	};

	enum NamedFlagType
	{
		FLAGTYPE_SEARCH = 0,
	};

	enum strVisibilityFlags
	{
		// This object is in the PVS
		VISIBLE_PVS			= (1 << 0),

		// This object is in a renderphase
		VISIBLE_RENDERPHASE	= (1 << 1),

		// This object is in the GBUF
		VISIBLE_GBUF		= (1 << 2),
	};

	enum strFrameFlags
	{
		// This frame has a user marker.
		MARKER = (1 << 0),

		// There were OOMs due to fragmented memory.
		OOM_FRAGMENTED = (1 << 1),

		// There were OOMs due to insufficient memory.
		OOM_NO_MEMORY = (1 << 2),

		// There were OOMs due to churn protection.
		OOM_CHURN_PROTECTION = (1 << 3),

		// An assert occurred.
		ASSERT_OCCURRED = (1 << 5),

		// The game blocked because crucial assets were missing
		BLOCKED_FOR_STREAMING = (1 << 6),

		// A bug has been reported
		BUG_REPORTED = (1 << 7),

		// An entity is using a lower LOD than it should.
		LOW_LOD = (1 << 8),

		// An entity is not rendered entirely because both its LOD and its parent LOD are not resident.
		MISSING_LOD = (1 << 9),

		// An iteration in an automated test has begun.
		BEGIN_TEST = (1 << 10),

		// An iteration in an automated test has ended.
		END_TEST = (1 << 11),

		// The configuration in an automated test has changed.
		TEST_NEW_CONFIG = (1 << 12),

	};

	enum Context
	{
		NONE,
		SCENESTREAMER,
		REQUESTPURGE,
		DELETENEXTUSED,
		MEMORYFREE,
		BOXSTREAMER,
		MAPSTREAMER,
		MAP,
		PHYSICS,
		SRL,
		POPSTREAMER,
		CLIPREQUEST,
		PROPMGMT,
		PATHSERVER,
		SCRIPTBRAIN,
		INSTANTFULFILL,
		INVENTORY,
		VEHICLEPOP,
		CLIPSETSTREAMER,
		MAPDATASTORE,
		MAPTYPESTORE,
		HDVEHICLES,
		TEXLOD,
		TASKTREE,
		METADATASTORE,
		WEAPONITEM,
		PTFXMANAGER,
		PED_HD,
		INTERIORPROXY,
		LOADALLREQS,
		PRIO_UPGRADE,
		SCALEFORM,
		PEDPOPULATION,
		SCRIPTMENU,
		GARBAGECOLLECT,
		COMPENTITIY,
		FAILMASTERCUTOFF,
		FAILTHROTTLE,
		FAILLIMIT,
		RPFPURGE,
		SCENARIOMGR,
		MINIMAP,
		PATHFIND,
		CARGEN,
		PLAYERSWITCH,
		DEPENDENCY,

		MAX_CONTEXT,
	};

	struct NeededListEntry
	{
		enum
		{
			ON_ODD = (1 << 0),

		};
		const fwEntity *m_Entity;
		float m_Score;
		u32 m_Flags;
	};

	struct VisibilityEntry
	{
		const fwEntity *m_Entity;
		u32 m_PhaseFlags;
		u32 m_OptionFlags;
	};

	enum FailedAllocReason
	{
		FRAGMENTED,
		OOM,
		CHURN_PROTECTION,
	};

	enum StreamingVolumeType
	{
		SVT_NONE,
		SVT_SPHERE,
		SVT_FRUSTUM,
		SVT_LINE,
	};

	struct StreamingVolumeInfo
	{
		Vec3V				m_Pos;
		Vec3V				m_Dir;
		StreamingVolumeType m_Type;
		float				m_Radius;
		u32					m_Flags;

		StreamingVolumeInfo()
		{
			m_Flags = SVT_NONE;
		}
	};

	typedef const char *(*GetStreamingContextCb)();
	typedef int (*EntityGuidGetterCb)(const fwEntity *entity);
	typedef float (*EntityBoundSphereGetterCb)(const fwEntity *entity, Vector3 &outCenter);
	typedef void (*AddStackContextCb)();

	struct ClientInfo
	{
		enum ClientFlags
		{

		};

		u32 m_ProtocolVersion;
		char m_SymbolFile[256];
		char m_Platform[32];			// Platform string, such as Xbox 360, PS3, etc
		char m_Configuration[32];		// Configuration type, such as Debug, BankRelease, etc
		char m_RestIP[32];				// IP Address for REST server
		u16 m_PointerSize;				// sizeof(void *) - 4 or 8
		char m_PlatformIdentifier;		// This is g_sysPlatform
		char m_Flags;
	};

    strStreamingVisualize(const bool startWorker);
    ~strStreamingVisualize();

	void RegisterObject(strIndex index, s32 moduleIndex, const char *name, int virtSize, int physSize, int compressedSize, strIndex parentObj);
	void RegisterModule(s32 moduleIndex, const char *name);
	void RegisterDevice(int deviceIndex, const char *name);
	void Request(strIndex index, u32 flags);
	void Unrequest(strIndex index);
	void SetStatus(strIndex index, int status);
	void IssueRequest(strIndex index, int deviceIndex, u32 readId, u32 lsn, u32 flags);
	void OpenRawFile(const char *name, u32 readId);
	void ProcessRequest(int deviceIndex, u32 readId);
	void FinishRequest(int deviceIndex, u32 readId);
	void Cancel(strIndex index);
	void Frame();
	void SetCamera(Vec3V_In camPos, Vec3V_In camDir, Vec3V_In camUp, float farClip, float fovH, float fovV)	{ m_CamPos = camPos; m_CamDir = camDir; m_CamUp = camUp; m_FarClip = farClip; m_FovH = fovH; m_FovV = fovV; }
	void SetStreamCamera(Vec3V_In camPos, Vec3V_In camDir)													{ m_StreamCamPos = camPos; m_StreamCamDir = camDir; }
	void SetContext(Context context)					{ m_CurrentContext = context; }
	Context GetContext() const							{ return m_CurrentContext; }
	void AddMarker(u32 markerBits);
	void AddEntity(const fwEntity *entity);
	void UpdateEntity(const fwEntity *entity);
	void DeleteEntity(const fwEntity *entity);
	void SetScore(float score)							{ m_CurrentScore = score; }
	void RegisterEntityType(int entityType, const char *name, u32 color, bool visible);
	void RegisterLodType(int entityType, const char *name);
	void SetMasterCutoff(float masterCutoff)			{ m_CurrentMasterCutoff = masterCutoff; }
	void StreamerRead(int deviceIndex, u32 readId, u32 lsn, const char *filename);
	void StreamerDecompress(int deviceIndex, u32 readId, bool decompressStart);
	void StreamerIdle(int deviceIndex);
	void SetTrackedStreamerRead(bool isTracked);
	void SetExtraClientInfo(const char *version, const char *assetType);
	void FailedAllocation(strIndex index, FailedAllocReason reason);
	void MarkAssert(const char *message);
	void SetStreamableNeedingMemory(strIndex index)		{ m_CurrentStreamableNeedingMemory = index; }
	void SetStreamingVolume(int volIndex, StreamingVolumeType type, Vec3V_In pos, Vec3V_In dir, float farClipOrRadius, u32 flags);
	void LowLevelOpen(fiHandle handle, const char *filename);
	void LowLevelRead(fiHandle handle, size_t size, u64 offset);
	void LowLevelSeek(fiHandle handle, u64 offset);
	void LowLevelWrite(fiHandle handle, size_t size);
	void LowLevelClose(fiHandle handle);
	void LowLevelReadEnd(fiHandle handle);
	void SetStreamerHandle(u32 handle);
	void SetQueue(u32 queue);
	void MarkMissingLod(const fwEntity *entity, bool parentLodMissing);
	void SendDependencies(strIndex index);
	void SetTotalStreamableCount(int indexCount);
	void UnregisterObject(strIndex index);
	void RegisterMapData(strIndex index, const fwMapData &mapData);
	void RegisterMapData(strIndex index, const fwMapDataDef &mapData);
	void InstantiateMapData(const fwMapData &mapData);
	void AddMapDataEntity(strIndex index, fwEntity *entity, int guid);
	void SetMarkerText(const char *markerText, ...);
	//void UpdateVisibility(const fwEntity **nowVisible, int nowVisibleCount, const fwEntity **nowInvisible, int nowInvisibleCount);
	void AddToNeededList(const NeededListEntry *neededList, int neededListCount);
	void AddCallstackToMarker();
	void UpdateVisFlags(VisibilityEntry *entries, int entryCount);
	void RegisterVisFlag(u32 visFlag, bool isOptionFlag, const char *name);
	void SetLodParent(const fwEntity *entity, const fwEntity *lodParent);
	void RegisterNamedFlag(NamedFlagType namedFlagType, int flagValue, const char *typeName);
	void AddBoxSearch(const spdAABB &aabb, int searchTypeId);
	void SetFlags(strIndex index, u32 flags);
	void ChangeGameState(u32 addFlags, u32 remFlags);
	void RegisterStateFlag(u32 flagMask, const char *name);
	void AddCullBox(const char *name, const spdAABB &aabb, const u32 *containerHash, int containerCount);


    void Update();  // compresses and sends buffered messages
    void RequestDump();

	void StartNetworkWorker();
	void SetStreamingContextCb(GetStreamingContextCb streamingContextCb)	{ m_GetStreamingContextCb = streamingContextCb; }
	void SetEntityGuidGetterCb(EntityGuidGetterCb entityGuidGetterCb)		{ m_EntityGuidGetterCb = entityGuidGetterCb; }
	void SetEntityBoundSphereGetterCb(EntityBoundSphereGetterCb entityBoundSphereGetterCb)		{ m_EntityBoundSphereGetterCb = entityBoundSphereGetterCb; }
	void SetAddStackContextCb(AddStackContextCb addStackContextCb)			{ m_AddStackContextCb = addStackContextCb; }

#if __BANK
	void AddWidgets(bkGroup &group);
#endif // __BANK
		

	static strStreamingVisualize &GetInstance()			{ FastAssert(sm_Instance); return *sm_Instance; }
	static bool IsInstantiated()						{ return (sm_IsEnabled); }
	static void InitClass();
	static void ShutdownClass();

	static void StreamerReadCallback(const char *filename,u32 lsn,int device,u32 handle, u32 readId);
	static void StreamerIdleCallback(int device);
	static void StreamerReadBulkCallback(u64 offset,int count,int device,u32 readId);
	static void StreamerDecompressCallback(const char *filename,int device,u32 readId, bool isStarting);
	static bool StreamOpenCallback(const char *filename,bool readonly, fiHandle fd);
	static bool StreamCreateCallback(const char *filename, fiHandle fd);
	static void StreamReadCallback(fiHandle fd, int len);
	static void StreamCloseCallback(const char *filename, fiStream &stream);


	static void OnFlagsChange(strIndex index, u32 oldFlags, u32 newFlags);


private:
	static void WorkerStub(void* param);
	void AddMarkerWithText();
	void Worker();
	void SendEntityList(int command, const fwEntity **entities, int entityCount);
	void Send(int packetType, const void* data, int dataSize, bool dynamic = false);
	void Flush();
	const char *GetContextName() const;
	void StackTraceCaptureInternal(size_t addr,const char* symbol, size_t offset);

	static int DummyEntityGuidGetter(const fwEntity *)	{ return 0; }
	static float DummyEntityBoundSphereGetter(const fwEntity *, Vector3 &)	{ return 0.0f; }
	static void StackTraceCapture(size_t addr,const char* symbol, size_t offset);
	static void DummyAddStackContext();

	enum 
	{
		NUM_BUFFERS = 8,
		BUF_SIZE = 8192,
		MAX_STREAMING_VOLUMES = 2,
	};
	typedef atFixedArray<u32, BUF_SIZE>	Buffer;
	char									m_CallstackMarker[256];
	Buffer									m_Buffer[NUM_BUFFERS];
	Buffer*									m_CurBuf;
	sysCriticalSectionToken					m_Cs;
	sysMessageQueue<Buffer*, NUM_BUFFERS>	m_Empty;
	sysMessageQueue<Buffer*, NUM_BUFFERS>	m_Full;
	Vec3V									m_CamPos;
	Vec3V									m_CamDir;
	Vec3V									m_CamUp;
	Vec3V									m_StreamCamPos;
	Vec3V									m_StreamCamDir;
	GetStreamingContextCb					m_GetStreamingContextCb;
	EntityGuidGetterCb						m_EntityGuidGetterCb;
	EntityBoundSphereGetterCb				m_EntityBoundSphereGetterCb;
	AddStackContextCb						m_AddStackContextCb;

	Context									m_CurrentContext;
	const char *							m_CurrentScript;
	float									m_CurrentScore;
	float									m_CurrentMasterCutoff;
	float									m_FovH;
	float									m_FovV;
	float									m_FarClip;
	u32										m_CurrentFrameFlags;
	strIndex								m_CurrentStreamableNeedingMemory;

	atRangeArray<StreamingVolumeInfo, MAX_STREAMING_VOLUMES> m_StreamingVolumes;

	bool									m_UpdateEntities;
	bool									m_UpdateNeededList;	

	// Master switch - will be turned on once SV is enabled, will be turned off again once the connection has been severed so we stop doing
	// any more SV-related processing.
	static bool								sm_IsEnabled;

	static strStreamingVisualize *sm_Instance;
};
			   


struct strStreamingVisualizeContext
{
	strStreamingVisualizeContext(strStreamingVisualize::Context context) {
		if (strStreamingVisualize::IsInstantiated()) { m_PrevContext = strStreamingVisualize::GetInstance().GetContext(); strStreamingVisualize::GetInstance().SetContext(context); }
	}

	~strStreamingVisualizeContext() {
		if (strStreamingVisualize::IsInstantiated()) { strStreamingVisualize::GetInstance().SetContext(m_PrevContext); }
	}

	strStreamingVisualize::Context m_PrevContext;
};

#if !__FINAL
//copied from the grcsetup.cpp ticker, will remerge them at somepoint
#define STREAM_TICKER_ONLY(x) x

class StrTicker 
{
public:
	enum EMarker
	{
		HDD_KB_LIMIT		= 0x01,
		HDD_MS_LIMIT		= 0x02,
		HDD_PENDING_LIMIT	= 0x04,
		ODD_KB_LIMIT		= 0x08,
		ODD_MS_LIMIT		= 0x10,
		ODD_PENDING_LIMIT	= 0x20,
	};

	static const float	ScaleY;
	static const float	ScaleX;
	static const u32	History=128;
	static const u32	BufferSize=128+4;

	StrTicker(const char *label,const float r,const float g,const float b, const bool active = true);

	void Add(const u32 value);			//for each individual ticker

	static void AddMarker(const EMarker marker);
	static void Draw(float x,float y);
	static void StepPut();
	static void StepGet();
	static void Reset();	
#if __BANK
	static void AddWidgets(bkGroup &group);
#endif
	static void AddBytes(const u32 bytes);
	static void ClearBytes(void);
	static void SetMaxPendingKB(const u32 kb);
	static void SetMaxConvertMS(const u32 ms);

private:
	bool		Active;
	int			BytesLastSecond;
	int			BytesCurrentSecond;	
	const char	*Label;
	StrTicker	*Next;
	Color32		Color;
	int			Data[StrTicker::BufferSize];

	static bool			initialised;
	static volatile u32 read_size;		
	static int			Put;
	static int			Get;	
	static u32			MaxPendingKB;
	static u32			MaxConvertMS;	
	static u8			Marker[StrTicker::BufferSize];
	static sysTimer		timer;

	static StrTicker	*First;				
	
	static void Init();	
};

#else
#define STREAM_TICKER_ONLY(x) x
#endif




} // namespace rage

#define STREAMINGVIS	strStreamingVisualize::GetInstance()

#define STRVIS_REGISTER_OBJECT(index, moduleIndex, name, virtSize, physSize, compressedSize, parentObj)			do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterObject(index, moduleIndex, name, virtSize, physSize, compressedSize, parentObj); } } while (false)
#define STRVIS_REGISTER_MODULE(moduleIndex, name)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterModule(moduleIndex, name); } } while (false)
#define STRVIS_REGISTER_DEVICE(deviceIndex, name)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterDevice(deviceIndex, name); } } while (false)
#define STRVIS_REQUEST(index, flags)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.Request(index, flags); } } while (false)
#define STRVIS_UNREQUEST(index)										do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.Unrequest(index); } } while (false)
#define STRVIS_SET_STATUS(index, newStatus)							do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetStatus(index, newStatus); } } while (false)
#define STRVIS_ISSUE_REQUEST(index, deviceIndex, handle, lsn, flags)		do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.IssueRequest(index, deviceIndex, handle, lsn, flags); } } while (false)
#define STRVIS_PROCESS_REQUEST(deviceIndex, handle)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.ProcessRequest(deviceIndex, handle); } } while (false)
#define STRVIS_FINISH_REQUEST(deviceIndex, handle)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.FinishRequest(deviceIndex, handle); } } while (false)
#define STRVIS_CANCEL(index)										do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.Cancel(index); } } while (false)
#define STRVIS_FRAME()												do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.Frame(); } } while (false)
#define STRVIS_SET_CONTEXT(context)									do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetContext(context); } } while (false)
#define STRVIS_SET_SCRIPT_NAME(name)
#define STRVIS_ADD_ENTITY(entity)									do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddEntity(entity); } } while (false)
#define STRVIS_UPDATE_ENTITY(entity)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.UpdateEntity(entity); } } while (false)
#define STRVIS_DELETE_ENTITY(entity)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.DeleteEntity(entity); } } while (false)
#define STRVIS_SET_CAMERA(camPos, camDir, camUp, farClip, fovH, fovV)		do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetCamera(camPos, camDir, camUp, farClip, fovH, fovV); } } while (false)
#define STRVIS_SET_STREAM_CAMERA(camPos, camDir)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetStreamCamera(camPos, camDir); } } while (false)
#define STRVIS_SET_SCORE(score)										do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetScore(score); } } while (false)
#define STRVIS_RESET_SCORE()										do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetScore(-1.0f); } } while (false)
#define STRVIS_REGISTER_ENTITY_TYPE(type, name, color, visible)		do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterEntityType(type, name, color, visible); } } while (false)
#define STRVIS_SET_MASTER_CUTOFF(masterCutoff)						do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetMasterCutoff(masterCutoff); } } while (false)
#define STRVIS_MARK_OOM_FRAGMENTED()								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetFlag(OOM_FRAGMENTED); } } while (false)
#define STRVIS_MARK_OOM_NO_MEMORY()									do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetFlag(OOM_NO_MEMORY); } } while (false)
#define STRVIS_MARK_OOM_CHURN_PROTECTION()							do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetFlag(OOM_CHURN_PROTECTION); } } while (false)
#define STRVIS_SET_TRACKED_STREAMEREAD(isTracked)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetTrackedStreamerRead(isTracked); } } while (false)
#define STRVIS_SET_STREAMABLE_NEEDING_MEMORY(index)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetStreamableNeedingMemory(index); } } while (false)
#define STRVIS_ADD_FAILED_ALLOCATION(index, reason)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.FailedAllocation(index, reason); } } while (false)
#define STRVIS_SET_STREAMING_VOLUME(slot, volumeType, pos, dir, farClipOrRadius, flags)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetStreamingVolume(slot, volumeType, pos, dir, farClipOrRadius, flags); } } while (false)
#define STRVIS_MARK_ASSERT(message)									do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.MarkAssert(message); } } while (false)
#define STRVIS_ADD_CALLSTACK_TO_MARKER()							do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddCallstackToMarker(); } } while (false)
#define STRVIS_MARK_BLOCKED_FOR_STREAMING()							do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddMarker(strStreamingVisualize::BLOCKED_FOR_STREAMING); STRVIS_SET_MARKER_TEXT("BLOCKED FOR STREAMING"); STRVIS_ADD_CALLSTACK_TO_MARKER(); } } while (false)
#define STRVIS_MARK_BUG_REPORTED()									do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddMarker(strStreamingVisualize::BUG_REPORTED); } } while (false)
#define STRVIS_MARK_MISSING_LOD(entity, parentLodMissing)			do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.MarkMissingLod(entity, parentLodMissing); } } while (false)
#define STRVIS_SEND_DEPENDENCIES(index)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SendDependencies(index); } } while (false)
#define STRVIS_SET_TOTAL_STREAMABLE_COUNT(indexCount)				do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetTotalStreamableCount(indexCount); } } while (false)
#define STRVIS_UNREGISTER_OBJECT(index)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.UnregisterObject(index); } } while (false)
#define STRVIS_REGISTER_MAPDATA(index, mapData)						do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterMapData(index, mapData); } } while (false)
#define STRVIS_INSTANTIATE_MAPDATA(mapData)							do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.InstantiateMapData(mapData); } } while (false)
#define STRVIS_ADD_MAPDATA_ENTITY(index, entity, guid)				do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddMapDataEntity(index, entity, guid); } } while (false)
#define STRVIS_SET_MARKER_TEXT(markerText, ...)						do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetMarkerText(markerText, ##__VA_ARGS__); } } while (false)
#define STRVIS_ADD_MARKER(markerBits)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddMarker(markerBits); } } while (false)
#define STRVIS_UPDATE_VISIBILITY(nowVisible, nowVisibleCount, nowInvisible, nowInvisibleCount)			do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.UpdateVisibility(nowVisible, nowVisibleCount, nowInvisible, nowInvisibleCount); } } while (false)
#define STRVIS_REGISTER_LOD_TYPE(type, name)						do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterLodType(type, name); } } while (false)
#define STRVIS_ADD_TO_NEEDED_LIST(neededList, neededCount)			do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddToNeededList(neededList, neededCount); } } while (false)
#define STRVIS_SET_EXTRA_CLIENT_INFO(version, assetType)			do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetExtraClientInfo(version, assetType); } } while (false)
#define STRVIS_UPDATE_VISFLAGS(visFlagChanges, count)				do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.UpdateVisFlags(visFlagChanges, count); } } while (false)
#define STRVIS_REGISTER_VISFLAG(visFlag, isOptionFlag, name)		do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterVisFlag(visFlag, isOptionFlag, name); } } while (false)
#define STRVIS_SET_LOD_PARENT(entity, lodParent)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetLodParent(entity, lodParent); } } while (false)
#define STRVIS_REGISTER_NAMED_FLAG(namedFlagType, flagValue, typeName)	do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterNamedFlag(namedFlagType, flagValue, typeName); } } while (false)
#define STRVIS_ADD_BOX_SEARCH(aabb, searchTypeId)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddBoxSearch(aabb, searchTypeId); } } while (false)
#define STRVIS_SET_FLAGS(index, flags)								do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.SetFlags(index, flags); } } while (false)
#define STRVIS_ON_FLAGS_CHANGE(index, oldFlags, newFlags)			do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.OnFlagsChange(index, oldFlags, newFlags); } } while (false)
#define STRVIS_OPEN_RAW_FILE(name, handle)							do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.OpenRawFile(name, handle); } } while (false)
#define STRVIS_CHANGE_GAME_STATE(addFlags, remFlags)				do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.ChangeGameState(addFlags, remFlags); } } while (false)
#define STRVIS_REGISTER_STATE_FLAG(flagMask, name)					do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.RegisterStateFlag(flagMask, name); } } while (false)
#define STRVIS_ADD_CULLBOX(name, box, maps, mapCount)				do { if (strStreamingVisualize::IsInstantiated()) { STREAMINGVIS.AddCullBox(name, box, maps, mapCount); } } while (false)
#define STRVIS_AUTO_CONTEXT(_context)								strStreamingVisualizeContext _strVisContext(_context);


#else // STREAMING_VISUALIZE

#define STRVIS_REGISTER_OBJECT(index, moduleIndex, name, virtSize, physSize, compressedSize, parentObj)
#define STRVIS_REGISTER_MODULE(moduleIndex, name)
#define STRVIS_REGISTER_DEVICE(moduleIndex, name)
#define STRVIS_REQUEST(index, flags)
#define STRVIS_UNREQUEST(index)
#define STRVIS_SET_STATUS(index, newStatus)
#define STRVIS_ISSUE_REQUEST(index, deviceIndex, handle, lsn, flags)
#define STRVIS_FRAME()
#define STRVIS_PROCESS_REQUEST(deviceIndex, handle)
#define STRVIS_FINISH_REQUEST(deviceIndex, handle)
#define STRVIS_CANCEL(index)
#define STRVIS_SET_CONTEXT(context)
#define STRVIS_ADD_ENTITY(entity)
#define STRVIS_UPDATE_ENTITY(entity)
#define STRVIS_DELETE_ENTITY(entity)
#define STRVIS_SET_SCRIPT_NAME(name)
#define STRVIS_SET_CAMERA(camPos, camDir, camUp, farClip, fovH, fovV)
#define STRVIS_SET_STREAM_CAMERA(camPos, camDir)
#define STRVIS_SET_SCORE(score)
#define STRVIS_RESET_SCORE()
#define STRVIS_REGISTER_ENTITY_TYPE(type, name, color, visible)
#define STRVIS_SET_MASTER_CUTOFF(masterCutoff)
#define STRVIS_SET_TRACKED_STREAMEREAD(isTracked)
#define STRVIS_SET_STREAMABLE_NEEDING_MEMORY(index)
#define STRVIS_ADD_FAILED_ALLOCATION(index, reason)
#define STRVIS_SET_STREAMING_VOLUME(slot, volumeType, pos, dir, farClipOrRadius, flags)
#define STRVIS_MARK_ASSERT(message)
#define STRVIS_MARK_BLOCKED_FOR_STREAMING()
#define STRVIS_MARK_BUG_REPORTED()
#define STRVIS_AUTO_CONTEXT(_context)
#define STRVIS_MARK_MISSING_LOD(entity, parentLodMissing)
#define STRVIS_SEND_DEPENDENCIES(index)
#define STRVIS_SET_TOTAL_STREAMABLE_COUNT(indexCount)
#define STRVIS_UNREGISTER_OBJECT(index)
#define STRVIS_REGISTER_MAPDATA(index, mapData)
#define STRVIS_INSTANTIATE_MAPDATA(mapData)
#define STRVIS_ADD_MAPDATA_ENTITY(index, entity, guid)
#define STRVIS_SET_MARKER_TEXT(markerText, ...)
#define STRVIS_ADD_MARKER(markerBits)
#define STRVIS_UPDATE_VISIBILITY(nowVisible, nowVisibleCount, nowInvisible, nowInvisibleCount)
#define STRVIS_REGISTER_LOD_TYPE(type, name)
#define STRVIS_ADD_TO_NEEDED_LIST(neededList, neededCount)
#define STRVIS_ADD_CALLSTACK_TO_MARKER()
#define STRVIS_SET_EXTRA_CLIENT_INFO(version, assetType)
#define STRVIS_UPDATE_VISFLAGS(visflagChanges, count)
#define STRVIS_REGISTER_VISFLAG(visFlag, isOptionFlag, name)
#define STRVIS_SET_LOD_PARENT(entity, lodParent)
#define STRVIS_REGISTER_NAMED_FLAG(namedFlagType, flagValue, typeName)
#define STRVIS_ADD_BOX_SEARCH(aabb, searchTypeId)
#define STRVIS_SET_FLAGS(index, flags)
#define STRVIS_ON_FLAGS_CHANGE(index, oldFlags, newFlags)
#define STRVIS_OPEN_RAW_FILE(name, handle)
#define STRVIS_CHANGE_GAME_STATE(addFlags, remFlags)
#define STRVIS_ADD_CULLBOX(name, box, maps, mapCount)

#define STRVIS_ONLY(_x)

#endif	// STREAMING_VISUALIZE


#endif // STREAMING_STREAMINGVISUALIZE_H
