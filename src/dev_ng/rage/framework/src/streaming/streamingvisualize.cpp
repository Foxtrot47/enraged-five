#include "streamingvisualize.h"

#if STREAMING_VISUALIZE
#include "entity/archetypemanager.h"
#include "entity/entity.h"
#include "file/remote.h"
#include "file/stream.h"
#include "file/tcpip.h"
#include "file/winsock.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/stores/mapdatastore.h"
#include "grcore/setup.h"
#include "grprofile/timebars.h"
#include "math/amath.h"
#include "net/netaddress.h"
#include "net/nethardware.h"
#include "paging/streamer.h"
#include "profile/element.h"
#include "profile/group.h"
#include "profile/page.h"
#include "spatialdata/aabb.h"
#include "streaming/streamingmodule.h"
#include "string/string.h"
#include "system/endian.h"
#include "system/hangdetect.h"
#include "system/memops.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/platform.h"
#include "system/stack.h"
#include "system/timemgr.h"
#include "system/timer.h"
#include <string.h>
#if __PS3
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#elif __XENON
#include "system/xtl.h"
#include <xbdm.h>
#else
#include "system/xtl.h"
#endif

#define USE_DEJA_STYLE_BROADCAST (0) 

XPARAM(symfilepath);



namespace rage {

#if PGSTREAMER_DEBUG
PARAM(streamingvisualize, "[streaming] Connect to the StreamingVisualize client on startup");
PARAM(streamingekg, "[streaming] hook in the streaming graph");
#else // PGSTREAMER_DEBUG
PARAM(streamingvisualize, "[streaming] Connect to the StreamingVisualize client on startup");
PARAM(streamingekg, "[streaming] hook in the streaming graph");
#endif // PGSTREAMER_DEBUG

const int ENTITYTYPE_BITS = 4;
const int LODTYPE_BITS = 4;


#define MONITORED_STREAMING_FLAGS (STR_DONTDELETE_MASK|STRFLAG_LOADSCENE)


// If true, don't track calls to pgStreamer::Read since they're already tracked on a higher level.
// pgStreamer::Read tracking calls contain strings, so we don't want to send them if we don't have to.
__THREAD bool st_IsTrackedRead;

__THREAD u32 st_StreamerHandle;
__THREAD u32 st_Queue;

static char s_MarkerText[128];

/* PURPOSE: A NetVector3 is a simple 3-component vector to be send across the network to StreamingVisualize. */
struct NetVector3
{
	float x, y, z;

	NetVector3 &operator=(Vec3V_In v)
	{
		x = v.GetXf();
		y = v.GetYf();
		z = v.GetZf();
		sysEndian::NtoLMe(x);
		sysEndian::NtoLMe(y);
		sysEndian::NtoLMe(z);

		return *this;
	}
};

const float NETVECTOR16_FACTOR = 4.0f;


struct NetVector16
{
	short x, y, z;

	NetVector16 &operator=(Vec3V_In v)
	{
		x = (short)(v.GetXf()*NETVECTOR16_FACTOR);
		y = (short)(v.GetYf()*NETVECTOR16_FACTOR);
		z = (short)(v.GetZf()*NETVECTOR16_FACTOR);
		sysEndian::NtoLMe(x);
		sysEndian::NtoLMe(y);
		sysEndian::NtoLMe(z);

		return *this;
	}
};

struct NetBox
{
	NetVector3 boxMin, boxMax;

	NetBox &operator=(const spdAABB &aabb)
	{
		boxMin = aabb.GetMin();
		boxMax = aabb.GetMax();

		return *this;
	}
};

struct NetBox16
{
	NetVector16 boxMin, boxMax;

	NetBox16 &operator=(const spdAABB &aabb)
	{
		boxMin = aabb.GetMin();
		boxMax = aabb.GetMax();

		return *this;
	}
};

CompileTimeAssert(sizeof(NetBox16) == 12);

#if __DEVPROF
struct strStreamingVisualizeDeviceProfiler : public fiDeviceProfiler
{
	virtual void OnOpen(fiHandle handle, const char *file);
	virtual void OnRead(fiHandle handle, size_t size, u64 offset);
	virtual void OnReadEnd(fiHandle handle);
	virtual void OnWrite(fiHandle handle, size_t size);
	virtual void OnSeek(fiHandle handle, u64 offset);
	virtual void OnClose(fiHandle handle);
} s_StrVizDeviceProfiler;
#endif // __DEVPROF

// We're doing lots of strIndex->s32 casts here
CompileTimeAssert(sizeof(strIndex) == sizeof(s32));	// We're doing a cast here

strStreamingVisualize *strStreamingVisualize::sm_Instance;
bool strStreamingVisualize::sm_IsEnabled;

static sysTimer s_Timer;

// A simple sparse bitset that has a bit set for each strIndex for which we've already sent the dependencies to SV to.
static atMap<strIndex, bool> s_DependenciesSent;

static const char *s_ContextNames[] = {
	NULL,
	"SCENESTRM",
	"REQUESTPURGE",
	"DELNEXTUSED",
	"MEMORYFREE",
	"BOXSTREAMER",
	"MAPSTREAMER",
	"MAP",
	"PHYSICS",
	"SRL",
	"POPSTREAMER",
	"CLIPREQUEST",
	"PROPMGMT",
	"PATHSERVER",
	"SCRIPTBRAIN",
	"INSTANTFULFILL",
	"INVENTORY",
	"VEHICLEPOP",
	"CLIPSETSTREAMER",
	"MAPDATASTORE",
	"MAPTYPESTORE",
	"HDVEHICLES",
	"TEXLOD",
	"TASKTREE",
	"METADATASTORE",
	"WEAPONITEM",
	"PTFXMANAGER",
	"PED_HD",
	"INTERIORPROXY",
	"LOADALLREQS",
	"PRIO_UPGRADE",
	"SCALEFORM",
	"PEDPOPULATION",
	"SCRIPTMENU",
	"GARBAGECOLLECT",
	"COMPENTITIY",
	"FAILMASTERCUTOFF",
	"FAILTHROTTLE",
	"FAILLIMIT",
	"RPFPURGE",
	"SCENARIOMGR",
	"MINIMAP",
	"PATHFIND",
	"CARGEN",
	"PLAYERSWITCH",
	"DEPENDENCY",
};

CompileTimeAssert(NELEM(s_ContextNames) == strStreamingVisualize::MAX_CONTEXT);

// Here's a map of all open file handles that we couldn't send because the connection to the StreamingVisualize
// client hasn't been established yet.
static atMap<u64, atString> s_fiHandleToName;
static bool	 s_ShowBandwidthGraph = false;

#if RAGE_TIMEBARS
static void strStreamingVisualize_StartupCallback(const char* s)
{
	STRVIS_SET_MARKER_TEXT("STARTUP: %s",s);
}
#endif

strStreamingVisualize::strStreamingVisualize(const bool startWorker)
{
	m_CurrentContext = NONE;
	m_CurrentScript = "";
	m_CamPos = Vec3V(V_ZERO);
	m_CamDir = Vec3V(V_Y_AXIS_WZERO);
	m_CamUp = Vec3V(V_UP_AXIS_WZERO);
	m_StreamCamPos = Vec3V(V_ZERO);
	m_StreamCamDir = Vec3V(V_Y_AXIS_WZERO);
	m_GetStreamingContextCb = NULL;
	m_UpdateEntities = true;
	m_UpdateNeededList = true;
	m_CurrentScore = -1.0f;
	m_CurrentMasterCutoff = 0.0f;
	m_CurrentFrameFlags = 0;
	m_EntityGuidGetterCb = DummyEntityGuidGetter;
	m_EntityBoundSphereGetterCb = DummyEntityBoundSphereGetter;
	m_AddStackContextCb = DummyAddStackContext;

	// TODO: Consider having the game set the callback and call strStreamingVisualize instead.
	pgStreamer::SetReadCallback(StreamerReadCallback);
	pgStreamer::SetIdleCallback(StreamerIdleCallback);
	pgStreamer::SetReadBulkCallback(StreamerReadBulkCallback);
	pgStreamer::SetDecompressCallback(StreamerDecompressCallback);

	fiStream::SetOpenCallback(StreamOpenCallback);
	fiStream::SetCreateCallback(StreamCreateCallback);
	fiStream::SetReadCallback(StreamReadCallback);
	fiStream::SetCloseCallback(StreamCloseCallback);

#if RAGE_TIMEBARS
	g_pfStartupCallback = strStreamingVisualize_StartupCallback;
#endif
	
#if __DEVPROF
	fiDevice::SetDeviceProfiler(&s_StrVizDeviceProfiler);
#endif // __DEVPROF

	if ( startWorker )
		StartNetworkWorker();

	ClientInfo clientInfo;

	m_CurBuf = 0;
	for(int i=0; i<NELEM(m_Buffer); ++i)
		m_Empty.Push(&m_Buffer[i]);

	clientInfo.m_ProtocolVersion = sysEndian::NtoL((u32) PROTOCOL_VERSION);

#if __PS3 || RSG_ORBIS
	char *ext;
	safecpy(clientInfo.m_SymbolFile,*getargv());

	ext = strrchr(clientInfo.m_SymbolFile,'.');
	if (ext)
		strcpy(ext,".cmp");
#elif __XENON
	DM_XBE xbe;
	DmGetXbeInfo(NULL, &xbe);
	safecpy(clientInfo.m_SymbolFile, xbe.LaunchPath);
	char* ext = strrchr(clientInfo.m_SymbolFile,'.');
	if (ext)
		strcpy(ext,".cmp");
#elif RSG_DURANGO
	char *cmdLine = GetCommandLine();

	if (cmdLine)
	{
		if (*cmdLine == '\"')
			cmdLine++;

		if (*cmdLine == 'G')
			cmdLine += 3;

		// HACK: Until we upload the Durango *.cmp file to the devkit
		sprintf(clientInfo.m_SymbolFile, "%%RS_CODEBRANCH%%\\game\\VS_Project\\Durango\\Layout\\Image\\Loose\\%s", cmdLine);

		// Get only the first argument, i.e. the exe name.
		if (strchr(clientInfo.m_SymbolFile, ' '))
			*strchr(clientInfo.m_SymbolFile, ' ') = 0;

		if (strrchr(clientInfo.m_SymbolFile,'.'))
			strcpy(strrchr(clientInfo.m_SymbolFile,'.'),".cmp");
		else
			strcat(clientInfo.m_SymbolFile,".cmp");
	}

#elif __WIN32PC // __XENON
	// sysStack::InitClass() hasn't been called yet, we'll have to figure out the
	// location of the symbol file ourselves.
	//	safecpy(clientInfo.m_SymbolFile, sysStack::GetSymbolFilename());
	const char *cmdLine = GetCommandLine();

	if (cmdLine)
	{
		if (*cmdLine == '\"')
			cmdLine++;

		safecpy(clientInfo.m_SymbolFile, cmdLine);

		// Get only the first argument, i.e. the exe name.
		if (strchr(clientInfo.m_SymbolFile, ' '))
		{
			*strchr(clientInfo.m_SymbolFile, ' ') = 0;
		}

		if (strrchr(clientInfo.m_SymbolFile,'.'))
			strcpy(strrchr(clientInfo.m_SymbolFile,'.'),".cmp");
		else
			strcat(clientInfo.m_SymbolFile,".cmp");
	}
	else
	{
		clientInfo.m_SymbolFile[0] = 0;
	}
#endif // __WIN32PC

	// -symfilepath overrides the path, in all cases, on all platforms.
	const char *pCustomSymFilePath = 0;
	if (PARAM_symfilepath.Get(pCustomSymFilePath))
	{
		safecpy(clientInfo.m_SymbolFile, pCustomSymFilePath);
	}
	else
	{
		Assertf(clientInfo.m_SymbolFile[0] && strcmp(clientInfo.m_SymbolFile, ".cmp"), "This platform doesn't provide the executable name. You'll have to run with -symfilepath=<path to your .cmp file> if you want MVZ to work.");
	}

	safecpy(clientInfo.m_Configuration, RSG_CONFIGURATION);
	safecpy(clientInfo.m_Platform, RSG_PLATFORM);

	netIpAddress ip;
	netHardware::GetLocalIpAddress(&ip);
	char ipLocalStr[netIpAddress::MAX_STRING_BUF_SIZE];
	ip.Format(ipLocalStr);

	safecpy(clientInfo.m_RestIP, ipLocalStr);

	clientInfo.m_PointerSize = sysEndian::NtoL((u16) sizeof(void *));
	clientInfo.m_PlatformIdentifier = g_sysPlatform;
	clientInfo.m_Flags = 0;

	sm_IsEnabled = true; //this should be the first time we send data to strviz, so let's enable it just before.
	Send(SV_INIT, &clientInfo, sizeof(clientInfo));
}

void strStreamingVisualize::StartNetworkWorker()
{
	sysIpcCreateThread(WorkerStub, this, 4096, PRIO_LOWEST, "[RAGE] StreamingVisualize");
}

void strStreamingVisualize::WorkerStub(void* param)
{
	static_cast<strStreamingVisualize*>(param)->Worker();
}

void strStreamingVisualize::Worker()
{
	int port = 8799;
	fiHandle handle = fiHandleInvalid;
	bool isGoodConnection = true;

	streamDisplayf("Streaming Visualize thread is alive and well");

	while (isGoodConnection)
	{
		Buffer& buf = *m_Full.Pop();	
		if (!fiIsValidHandle(handle))
		{
			streamDisplayf("Connecting to Streaming Visualize port %d...", port);
			InitWinSock();

#if USE_DEJA_STYLE_BROADCAST
			// was playing around with connecting the same way as deja, i.e. braodcast
			// a UDP packet to the whole subnet and wait for the tracker to connect to us.
			// it works but it's a bit messy
			fiHandle listener = fiDeviceTcpIp::Listen(port, 1);
			struct sockaddr_in si_other;
			int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			memset(&si_other, 0, sizeof(si_other));
			si_other.sin_family = AF_INET;
			si_other.sin_port = htons((u16)port);
			si_other.sin_addr.s_addr = htonl(INADDR_BROADCAST);
			int val = 1;
			setsockopt(s, SOL_SOCKET, SO_BROADCAST, (char*)&val, sizeof(val));
#if __XENON
			u_long nonblocking = 1;
			XSocketIOCTLSocket((SOCKET)listener, FIONBIO, &nonblocking);
#elif __WIN32
			u_long nonblocking = 1;
			ioctlsocket((SOCKET)listener, FIONBIO, &nonblocking);
#else // !__WIN32
			setsockopt((int)listener, SOL_SOCKET, SO_NBIO, (char*)&val, sizeof(val));
#endif // !__WIN32
			char* msg = "streamingvisualize";
			for(int retry = 0; retry < 10; ++retry)
			{
				sendto(s, msg, strlen(msg)+1, 0, (sockaddr*)&si_other, sizeof(si_other));
				handle = fiDeviceTcpIp::Pickup(listener);
				if (fiIsValidHandle(handle))
					break;
				sysIpcSleep(100);
			} 
#else // USE_DEJA_STYLE_BROADCAST

#if __WIN32PC
			// PC currently has inconsistent behavior - it needs NULL to get the socket
			// MemVisualize is using; GetLocalHost() would return the loopback device, which
			// is not the one MemVisualize binds to.
			const char *localHost = NULL;
#else // __WIN32PC
			const char *localHost = fiDeviceTcpIp::GetLocalHost();
#endif // __WIN32PC

			for(int retry = 0; retry < 10; ++retry)
			{
				handle = fiDeviceTcpIp::Connect(localHost, port);
				if (fiIsValidHandle(handle))
					break;
				sysIpcSleep(100);
			} 
#endif // USE_DEJA_STYLE_BROADCAST
			if (fiIsValidHandle(handle))
			{
				streamDisplayf("Connected to StreamingVisualize.");
			}
			else
			{
				streamErrorf("Unable to connect to StreamingVisualize.");
				sm_IsEnabled = false;
				sm_Instance = NULL;
				//break;
			}
		}
		if (fiIsValidHandle(handle))
			isGoodConnection = fiDeviceTcpIp::GetInstance().SafeWrite(handle, buf.GetElements(), buf.GetCount() * 4);
		buf.Reset();
		m_Empty.Push(&buf);
	}

	streamErrorf("Aborted connection to streaming visualize - discarding all subsequent packages");

	// if the connection is still good and we aborted because of some other problems (like OOM), let's cut the
	// connection so the client doesn't stall forever waiting for us.
	if (fiIsValidHandle(handle))
	{
		fiDeviceTcpIp::GetInstance().Close(handle);
	}

	// Turn SV off so the game believes we're no longer connected to SV. That way, we want waste any more time.
	sm_IsEnabled = false;

	// Keep flushing the buffers so the main thread doesn't stall.
	while (true)
	{
		Buffer& buf = *m_Full.Pop();	
		buf.Reset();
		m_Empty.Push(&buf);
	}
}
  
strStreamingVisualize::~strStreamingVisualize()
{
	ShutdownWinSock();
}

struct sStackEntry
{
	char m_Name[63];
	u8   m_CallstackLevel;
};

/* static __THREAD int sm_SentStackPos = 0;
static __THREAD int sm_StackPos = 0;
static __THREAD sStackEntry sm_Stack[64];

static int CaptureStack(size_t * buf, u32 count)
{
	size_t stack[128];
	sysStack::CaptureStackTrace(stack, 128, 2);
	int i;
	for(i=0; i<128 && stack[i]; ++i) {}
	u32 level = i;
	if (level > count)
		level = count;
	for(--i; i >= 0 && count; --i, --count)
		*buf++ = sysEndian::NtoL(stack[i]);
	for(; count; --count)
		*buf++ = 0;
	return level;
} */

void strStreamingVisualize::Update()
{
	Frame();
	Flush();
}

void strStreamingVisualize::Flush()
{
	SYS_CS_SYNC(m_Cs);
	if (m_CurBuf)
	{
		m_Full.Push(m_CurBuf);
		m_CurBuf = 0;
	}
}

void strStreamingVisualize::Send(int packetType, const void* data, int dataSize, bool dynamic)
{
	if (!sm_IsEnabled)
		return;

	FastAssert(!(dataSize&3));
	dataSize >>= 2;
	u32 header = sysEndian::NtoL(packetType|(((u32)(size_t)sysIpcGetCurrentThreadId())<<8));
	SYS_CS_SYNC(m_Cs);
	if (m_CurBuf && m_CurBuf->GetCount() + 1 + int(dynamic) + dataSize > m_CurBuf->GetMaxCount())
		Flush();
#if 0
	if (!m_CurBuf && !m_Empty.PopWait(m_CurBuf, 0))
	{
		sysTimer w;
		m_CurBuf = m_Empty.Pop();
		streamWarningf("Blocked for %ius waiting for strStreamingVisualize worker", (int)w.GetUsTime());
	}
#else
	if (!m_CurBuf)
	{
		//streamAssertf(m_Empty.GetTail(m_CurBuf), "Thread blocked because all StreamingVisualize buffers are in use. Increase the buffer count to reduce the performance impact. If this thread is now blocked for good, the network communication with the client may be down.");
		HANG_DETECT_SAVEZONE_ENTER();
		unsigned spamMs = 1000;
		unsigned msgBoxCountdown = 3;
		for (;;)
		{
			if (m_Empty.PopWait(m_CurBuf, spamMs))
				break;
			streamErrorf("Stalled waiting for strStreamingVisualize worker");
			spamMs = 5000;
			if (msgBoxCountdown && --msgBoxCountdown == 0)
			{
				if (fiRemoteShowMessageBox("Stalled waiting for Streaming Visualize.\nSever connection?", RSG_PLATFORM " " RSG_CONFIGURATION, MB_YESNO, IDNO) == IDYES)
				{
					sm_IsEnabled = false;
					streamErrorf("Aborted connection to streaming visualize");
					HANG_DETECT_SAVEZONE_EXIT("strStreamingVisualize::Send");
					return;
				}
			}
		}
		HANG_DETECT_SAVEZONE_EXIT("strStreamingVisualize::Send");
	}
#endif
	m_CurBuf->Push(header);
	const u32* src = static_cast<const u32*>(data);
	const u32 size = sysEndian::NtoL((u32)dataSize);
	if (dynamic) m_CurBuf->Push(size);
	while (dataSize--)
		m_CurBuf->Push(*src++);
}

void strStreamingVisualize::RegisterObject(strIndex index, s32 moduleIndex, const char *name, int virtSize, int physSize, int compressedSize, strIndex parentObj)
{
	struct DataBlock
	{
		s32 index;
		s32 moduleIndex;
		s32 virtSize;
		s32 physSize;
		s32 compressedSize;
		s32 parentIndex;
		u16 nameLen;
		char name[256];
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.moduleIndex = sysEndian::NtoL((s32) moduleIndex);
	data.virtSize = sysEndian::NtoL((s32) virtSize);
	data.physSize = sysEndian::NtoL((s32) physSize);
	data.compressedSize = sysEndian::NtoL((s32) compressedSize);
	if (parentObj.IsValid())
	{
		data.parentIndex = sysEndian::NtoL((s32) parentObj.Get());
	}
	else
	{
		data.parentIndex = -1;
	}
	safecpy(data.name, name);
	int nameLen = istrlen(data.name) + 1;
	data.nameLen = sysEndian::NtoL((u16) nameLen);

	int packetSize = sizeof(data) - 256 + nameLen;
	packetSize += 3;
	packetSize &= 0xfffffffc;

	Send(SV_REGISTEROBJECT, &data, packetSize, true);
}

void strStreamingVisualize::RegisterModule(s32 moduleIndex, const char *name)
{
	struct DataBlock
	{
		s32 moduleIndex;
		u16 nameLen;
		char name[256];
	} data;

	data.moduleIndex = sysEndian::NtoL((s32) moduleIndex);
	safecpy(data.name, name);
	int nameLen = istrlen(data.name) + 1;
	data.nameLen = sysEndian::NtoL((u16) nameLen);

	int packetSize = sizeof(data) - 256 + nameLen;
	packetSize += 3;
	packetSize &= 0xfffffffc;

	Send(SV_REGISTERMODULE, &data, packetSize, true);
}

void strStreamingVisualize::RegisterDevice(s32 deviceIndex, const char *name)
{
	struct DataBlock
	{
		s32 deviceIndex;
		u16 nameLen;
		char name[256];
	} data;

	data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
	safecpy(data.name, name);
	int nameLen = istrlen(data.name) + 1;
	data.nameLen = sysEndian::NtoL((u16) nameLen);

	int packetSize = sizeof(data) - 256 + nameLen;
	packetSize += 3;
	packetSize &= 0xfffffffc;

	Send(SV_REGISTERDEVICE, &data, packetSize, true);
}

void strStreamingVisualize::Request(strIndex index, u32 flags)
{
	struct DataBlock
	{
		s32 index;
		u32 flags;
		float time;
		float score;
		char context[16];
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.flags = sysEndian::NtoL((s32) flags);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.score = m_CurrentScore;
	sysEndian::NtoLMe(data.score);

	const char *context = GetContextName();

	safecpy(data.context, context);

	Send(SV_REQUEST, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::Unrequest(strIndex index)
{
	struct DataBlock
	{
		s32 index;
		s32 indexNeedingMemory;
		float score;
		char context[16];
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());

	if (m_CurrentStreamableNeedingMemory.IsValid())
	{
		data.indexNeedingMemory = sysEndian::NtoL((s32) m_CurrentStreamableNeedingMemory.Get());
	}
	else
	{
		data.indexNeedingMemory = -1;
	}

	data.score = m_CurrentScore;
	sysEndian::NtoLMe(data.score);

	const char *context = GetContextName();

	safecpy(data.context, context);

	Send(SV_UNREQUEST, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::SetStatus(strIndex index, int status)
{
	struct DataBlock
	{
		s32 index;
		s32 status;
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.status = sysEndian::NtoL((s32) status);

	Send(SV_SET_STATUS, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::IssueRequest(strIndex index, int deviceIndex, u32 readId, u32 lsn, u32 flags)
{
	struct DataBlock
	{
		s32 index;
		s32 deviceIndex;
		u32 readId;
		float time;
		u32 lsn;
		u32 flags;
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.readId = sysEndian::NtoL(readId);
	data.lsn = sysEndian::NtoL((u32) lsn);
	data.flags = sysEndian::NtoL((u32) flags);

	Send(SV_ISSUEREQUEST, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::OpenRawFile(const char *name, u32 readId)
{
	struct DataBlock
	{
		u32 readId;
		float time;
		char name[256];
	} data;

	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.readId = sysEndian::NtoL(readId);
	safecpy(data.name, name);

	Send(SV_OPEN_RAW_FILE, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::ProcessRequest(int deviceIndex, u32 readId)
{
	struct DataBlock
	{
		s32 deviceIndex;
		u32 readId;
		float time;
	} data;

	data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.readId = sysEndian::NtoL(readId);

	Send(SV_PROCESSREQUEST, &data, sizeof(DataBlock), false);

	SetStreamerHandle(readId);
}

void strStreamingVisualize::FinishRequest(int deviceIndex, u32 readId)
{
	struct
	{
		s32 deviceIndex;
		u32 readId;
		float time;
	} data;

	data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.readId = sysEndian::NtoL(readId);

	Send(SV_FINISHREQUEST, &data, sizeof(data), false);

	SetStreamerHandle(0);
}

void strStreamingVisualize::AddEntity(const fwEntity *entity)
{
	if (m_UpdateEntities)
	{
		struct
		{
			s32 entityId;
			s32 modelStrIndex;
//			NetVector3 position;
			NetBox16 box;
		} data;

		// NOTE - casting a pointer to 32 bit. We're pushing our luck on x64, but this should work.
		data.entityId = sysEndian::NtoL((s32)(uptr) entity);

		strStreamingModule* streamingModule = fwArchetypeManager::GetStreamingModule();
		strLocalIndex objIndex = strLocalIndex(entity->GetModelIndex());
		strIndex index = streamingModule->GetStreamingIndex(objIndex);
		data.modelStrIndex = sysEndian::NtoL((s32) index.Get());

		spdAABB aabb;
		entity->GetAABB(aabb);
		//data.position = entity->GetTransform().GetPosition();
		data.box.boxMin = aabb.GetMin();
		data.box.boxMax = aabb.GetMax();

		Send(SV_ADD_ENTITY, &data, sizeof(data), false);
	}
}

void strStreamingVisualize::UpdateEntity(const fwEntity *entity)
{
	if (m_UpdateEntities)
	{
		if (!entity->GetArchetype() || entity->GetArchetype()->GetDrawableType() == fwArchetype::DT_ASSETLESS)
		{
			return;
		}

		struct
		{
			s32 entityId;
			s32 modelStrIndexLodTypeEntityType;		// LOD type in the upper 4 bits, entity type in the next 4 bits
			s32 entityFlags;						// All entity flags. TODO: This is stupid, we shouldn't be sending this on every update.
//			s32 lodType;
//			NetVector3 position;
			NetVector16 boxMin;
			NetVector16 boxMax;
//			int entityType;
			NetVector16 sphereCenter;
			s16 unused;
			float sphereRadius;
			float lodDistance;
			int parentEntityId;
		} data;

		// NOTE - casting a pointer to 32 bit. We're pushing our luck on x64, but this should work.
		data.entityId = sysEndian::NtoL((s32)(uptr) entity);

		strStreamingModule* streamingModule = fwArchetypeManager::GetStreamingModule();
		strLocalIndex objIndex = strLocalIndex(entity->GetModelIndex());
		strIndex index = streamingModule->GetStreamingIndex(objIndex);

		u32 packedValue = entity->GetType() << 24;
		packedValue |= entity->GetLodData().GetLodType() << 28;
		packedValue |= index.Get();
		Assert(!(index.Get() & ~0xffffff));

		data.modelStrIndexLodTypeEntityType = sysEndian::NtoL(packedValue);
		data.entityFlags = sysEndian::NtoL(entity->GetBaseFlags());

		spdAABB aabb;
		entity->GetAABB(aabb);
		//data.position = entity->GetTransform().GetPosition();
		data.boxMin = aabb.GetMin();
		data.boxMax = aabb.GetMax();

		Vector3 vCentre;
		data.sphereRadius = m_EntityBoundSphereGetterCb(entity, vCentre);
		data.sphereCenter = RCC_VEC3V(vCentre);
		data.lodDistance = (float) entity->GetLodDistance();
		data.parentEntityId =  sysEndian::NtoL((s32)(uptr) entity->GetLod());
		sysEndian::NtoLMe(data.lodDistance);
		sysEndian::NtoLMe(data.sphereRadius);


		Send(SV_UPDATE_ENTITY, &data, sizeof(data), false);
	}
}

void strStreamingVisualize::DeleteEntity(const fwEntity *entity)
{
	if (m_UpdateEntities)
	{
		struct
		{
			s32 entityId;
		} data;

		// NOTE - casting a pointer to 32 bit. We're pushing our luck on x64, but this should work.
		data.entityId = sysEndian::NtoL((s32)(uptr) entity);

		Send(SV_DELETE_ENTITY, &data, sizeof(data), false);
	}
}

void strStreamingVisualize::MarkMissingLod(const fwEntity *entity, bool parentLodMissing)
{
	struct
	{
		s32 entityId;
		int parentLodMissing;
	} data;

	// NOTE - casting a pointer to 32 bit. We're pushing our luck on x64, but this should work.
	data.entityId = sysEndian::NtoL((s32)(uptr) entity);
	data.parentLodMissing = (int) parentLodMissing;

	Send(SV_MISSING_LOD, &data, sizeof(data), false);
	
}


void strStreamingVisualize::RegisterEntityType(int entityType, const char *name, u32 color, bool visible)
{
	struct
	{
		int entityType;
		u32 color;
		char name[16];
		int visible;
	} data;

	data.entityType = sysEndian::NtoL((s32)(uptr) entityType);
	data.color = sysEndian::NtoL((u32) color);
	safecpy(data.name, name);
	data.visible = sysEndian::NtoL((int) visible);

	// If this triggers, we need to juggle LODTYPE_BITS.
	Assert(entityType < (1 << ENTITYTYPE_BITS));

	Send(SV_REGISTER_ENTITY_TYPE, &data, sizeof(data), false);
}

void strStreamingVisualize::RegisterLodType(int lodType, const char *name)
{
	struct
	{
		int lodType;
		char name[16];
	} data;

	data.lodType = sysEndian::NtoL((s32)(uptr) lodType);
	safecpy(data.name, name);

	// If this triggers, we need to juggle LODTYPE_BITS.
	Assert(lodType < (1 << LODTYPE_BITS));

	Send(SV_REGISTER_LOD_TYPE, &data, sizeof(data), false);
}

void strStreamingVisualize::Cancel(strIndex index)
{
	struct DataBlock
	{
		s32 index;
		float time;
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);

	Send(SV_CANCEL, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::SetExtraClientInfo(const char *version, const char *assetType)
{
	struct
	{
		char version[32];
		char assetType[32];
	} data;

	safecpy(data.version, version);
	safecpy(data.assetType, assetType);

	Send(SV_SET_EXTRA_CLIENT_INFO, &data, sizeof(data), false);
}

void strStreamingVisualize::Frame()
{
//	grcDebugDraw::AddDebugOutputEx(false, false, "SV Wall clock: %.3f (Frame %d)", s_Timer.GetTime(), TIME.GetFrameCount());

	struct DataBlock
	{
		float time;
		NetVector3 camPos;
		NetVector3 camDir;
		NetVector3 camUp;
		NetVector3 streamCamPos;
		NetVector3 streamCamDir;
		float masterCutoff;
		u32 flags;
		float fovH;
		float fovV;
		float farClip;
	} data;

	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.camPos = m_CamPos;
	data.camDir = -m_CamDir;
	data.camUp = m_CamUp;
	data.streamCamPos = m_StreamCamPos;
	data.streamCamDir = m_StreamCamDir;
	data.masterCutoff = m_CurrentMasterCutoff;
	sysEndian::NtoLMe(data.masterCutoff);
	data.flags = sysEndian::NtoL(m_CurrentFrameFlags);
	data.fovH = m_FovH;
	data.fovV = m_FovV;
	data.farClip = m_FarClip;
	sysEndian::NtoLMe(data.fovH);
	sysEndian::NtoLMe(data.fovV);
	sysEndian::NtoLMe(data.farClip);

	m_CurrentFrameFlags = 0;

	Send(SV_FRAME, &data, sizeof(DataBlock), false);

	// Also send the streaming volume, if we have one.
	for (int x=0; x<MAX_STREAMING_VOLUMES; x++)
	{
		StreamingVolumeInfo &vol = m_StreamingVolumes[x];

		if (vol.m_Type != SVT_NONE)
		{
			struct
			{
				s32 index;
				s32 type;
				NetVector3 pos;
				NetVector3 dir;
				float radius;
				u32 flags;
			} svdata;

			svdata.index = sysEndian::NtoL((s32) x);
			svdata.type = sysEndian::NtoL((s32) vol.m_Type);
			svdata.pos = vol.m_Pos;
			svdata.dir = vol.m_Dir;
			svdata.radius = vol.m_Radius;
			sysEndian::NtoLMe(svdata.radius);
			svdata.flags = sysEndian::NtoL(vol.m_Flags);

			Send(SV_ADD_STREAMING_VOLUME, &svdata, sizeof(svdata), false);
		}
	}
}

void strStreamingVisualize::AddMarker(u32 markerBits)
{
	struct DataBlock
	{
		s32 flags;
	} data;

	data.flags = sysEndian::NtoL((u32) markerBits);

	Send(SV_ADD_MARKER, &data, sizeof(DataBlock), false);
}

void strStreamingVisualize::AddMarkerWithText()
{
	AddMarker(MARKER);
	SetMarkerText(s_MarkerText);
}


void strStreamingVisualize::StreamerRead(int deviceIndex, u32 readId, u32 lsn, const char *filename)
{
	if (!st_IsTrackedRead)
	{
		struct
		{
			s32 deviceIndex;
			u32 readId;
			float time;
			u32 lsn;
			short nameLen;
			char name[254];
		} data;

		data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
		data.readId = sysEndian::NtoL((s32) readId);
		data.time = s_Timer.GetTime();
		sysEndian::NtoLMe(data.time);
		data.lsn = sysEndian::NtoL((u32) lsn);

		safecpy(data.name, filename);
		int nameLen = istrlen(data.name) + 1;
		data.nameLen = sysEndian::NtoL((u16) nameLen);

		int packetSize = sizeof(data) - 254 + nameLen;
		packetSize += 3;
		packetSize &= 0xfffffffc;

		Send(SV_STREAMER_READ, &data, packetSize, true);
	}
}

void strStreamingVisualize::StreamerDecompress(int deviceIndex, u32 readId, bool decompressStart)
{
	struct
	{
		s32 deviceIndex;
		u32 readId;
		float time;
		u32 decompressInfo;
	} data;

	data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
	data.readId = sysEndian::NtoL((s32) readId);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.decompressInfo = (u32) decompressStart;

	Send(SV_STREAMER_DECOMPRESS, &data, sizeof(data), false);
}

void strStreamingVisualize::StreamerIdle(int deviceIndex)
{
	struct
	{
		s32 deviceIndex;
		float time;
	} data;

	data.deviceIndex = sysEndian::NtoL((s32) deviceIndex);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);

	Send(SV_DEVICE_IDLE, &data, sizeof(data), false);
}

void strStreamingVisualize::LowLevelOpen(fiHandle handle, const char *filename)
{
	struct
	{
		u64 handle;
		float time;
		u32 pad;
		short nameLen;
		char name[254];
	} data;

	data.handle = sysEndian::NtoL((u64) (uptr) handle);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);

	safecpy(data.name, filename);
	int nameLen = istrlen(data.name) + 1;
	data.nameLen = sysEndian::NtoL((u16) nameLen);

	int packetSize = sizeof(data) - 254 + nameLen;
	packetSize += 3;
	packetSize &= 0xfffffffc;

	Send(SV_LOWLEVEL_OPEN, &data, packetSize, true);
}

void strStreamingVisualize::LowLevelRead(fiHandle handle, size_t size, u64 offset)
{
	struct
	{
		u64 handle;
		float time;
		u32 size;
		u64 offset;
		u32 streamerHandle;
		u32 queue;
	} data;

	data.handle = sysEndian::NtoL((u64) (uptr) handle);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.size = sysEndian::NtoL((u32) size);
	data.offset = sysEndian::NtoL((u64) offset);
	data.streamerHandle = sysEndian::NtoL((u32) st_StreamerHandle);
	data.queue = sysEndian::NtoL((u32) st_Queue);

	Send(SV_LOWLEVEL_READ, &data, sizeof(data), false);
}

void strStreamingVisualize::LowLevelReadEnd(fiHandle handle)
{
	struct
	{
		u64 handle;
		float time;
		u32 streamerHandle;
		u32 queue;
		u32 unused;
	} data;

	data.handle = sysEndian::NtoL((u64) (uptr) handle);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.streamerHandle = sysEndian::NtoL((u32) st_StreamerHandle);
	data.queue = sysEndian::NtoL((u32) st_Queue);

	Send(SV_LOWLEVEL_READEND, &data, sizeof(data), false);
}

void strStreamingVisualize::LowLevelSeek(fiHandle handle, u64 offset)
{
	struct
	{
		u64 handle;
		float time;
		u32 streamerHandle;
		u64 offset;
		u32 queue;
		u32 pad;
	} data;

	data.handle = sysEndian::NtoL((u64) (uptr) handle);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.streamerHandle = sysEndian::NtoL((u32) st_StreamerHandle);
	data.offset = sysEndian::NtoL((u64) offset);
	data.queue = sysEndian::NtoL((u32) st_Queue);

	Send(SV_LOWLEVEL_SEEK, &data, sizeof(data), false);
}

void strStreamingVisualize::LowLevelWrite(fiHandle handle, size_t size)
{
	struct
	{
		u64 handle;
		float time;
		u32 size;
		u32 streamerHandle;
		u32 queue;
	} data;

	data.handle = sysEndian::NtoL((u64) (uptr) handle);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);
	data.size = sysEndian::NtoL((u32) size);
	data.streamerHandle = sysEndian::NtoL((u32) st_StreamerHandle);
	data.queue = sysEndian::NtoL((u32) st_Queue);

	Send(SV_LOWLEVEL_WRITE, &data, sizeof(data), false);
}

void strStreamingVisualize::LowLevelClose(fiHandle handle)
{
	struct
	{
		u64 handle;
		float time;
		u32 pad;
	} data;

	data.handle = sysEndian::NtoL((u64) (uptr) handle);
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);

	Send(SV_LOWLEVEL_CLOSE, &data, sizeof(data), false);
}

#if __DEVPROF
void strStreamingVisualizeDeviceProfiler::OnOpen(fiHandle handle, const char *file)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.LowLevelOpen(handle, file);
	}
}

PF_PAGE(StreamingBandwidthPage, "Streaming BandWidth");
PF_GROUP(StreamingBandwidth);
PF_LINK(StreamingBandwidthPage, StreamingBandwidth);
PF_VALUE_FLOAT(StreamingKB, StreamingBandwidth);
//PF_VALUE_FLOAT(AverageStreamingKB, StreamingBandwidth);
static volatile u32 pending_read_size = 0;
//static volatile u32 num_reads = 0;
static StrTicker StreamHDD("HDD",0.9f,0.1f,0.1f);
static StrTicker StreamAvg("Avg",0.1f,0.9f,0.1f);

static StrTicker ReadHDD("Read HDD",0.9f,0.0f,0.9f,false);
static StrTicker ReadODD("Read ODD",0.1f,0.9f,0.9f);

void strStreamingVisualizeDeviceProfiler::OnRead(fiHandle handle, size_t size, u64 offset)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.LowLevelRead(handle, size, offset);		
	}
	sysInterlockedExchange(&pending_read_size, (u32)size);	
}


void strStreamingVisualizeDeviceProfiler::OnReadEnd(fiHandle handle)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.LowLevelReadEnd(handle);
	}
	//bandwidth graph	
	{
		StrTicker::AddBytes(pending_read_size);		
		StreamHDD.Add( pending_read_size );
		sysInterlockedExchange(&pending_read_size, 0);
		//sysInterlockedIncrement(&num_reads);
	}
}

void strStreamingVisualizeDeviceProfiler::OnSeek(fiHandle handle, u64 offset)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.LowLevelSeek(handle, offset);
	}
}

void strStreamingVisualizeDeviceProfiler::OnWrite(fiHandle handle, size_t size)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.LowLevelWrite(handle, size);
	}
}

void strStreamingVisualizeDeviceProfiler::OnClose(fiHandle handle)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.LowLevelClose(handle);
	}
}
#endif // __DEVPROF


void strStreamingVisualize::SetTotalStreamableCount(int /*indexCount*/)
{
	//USE_DEBUG_MEMORY();
	//s_DependenciesSent.Init(indexCount);
}

void strStreamingVisualize::SendDependencies(strIndex index)
{
	// If we already sent them, don't worry about it.
	if (s_DependenciesSent.Access(index) == NULL)
	{
		{
			USE_DEBUG_MEMORY();
			s_DependenciesSent.Insert(index, true);
		}

		struct
		{
			s32 index;
			s32 depCount;
			s32 dependencies[STREAMING_MAX_DEPENDENCIES];
		} data;

		// Retrieve dependencies.
		strStreamingModule *pModule = strStreamingEngine::GetInfo().GetModule(index);
		strLocalIndex objIndex = pModule->GetObjectIndex(index);
		CompileTimeAssert(sizeof(strIndex) == sizeof(s32));
		s32 numDeps = pModule->GetDependencies(objIndex, (strIndex *) data.dependencies, STREAMING_MAX_DEPENDENCIES);

		data.index = sysEndian::NtoL((s32) index.Get());
		data.depCount = sysEndian::NtoL((s32) numDeps);

		for (int x=0; x<numDeps; x++)
		{
			data.dependencies[x] = sysEndian::NtoL(data.dependencies[x]);
		}

		Send(SV_DEPENDENCY_INFO, &data, sizeof(data) - sizeof(s32) * STREAMING_MAX_DEPENDENCIES + sizeof(s32) * numDeps, true);
	}
}

void strStreamingVisualize::UnregisterObject(strIndex index)
{
	struct
	{
		s32 index;
	} data;

	{
		USE_DEBUG_MEMORY();
		s_DependenciesSent.Delete(index);
	}

	data.index = sysEndian::NtoL((s32) index.Get());

	Send(SV_UNREGISTER_OBJECT, &data, sizeof(data), false);
}

void strStreamingVisualize::RegisterMapData(strIndex index, const fwMapData &mapData)
{
	struct
	{
		s32 index;
		s32 mapDataId;
		s32 parentStrIndex;
		NetVector3 physicsBoxMin;
		NetVector3 physicsBoxMax;
		NetVector3 streamingBoxMin;
		NetVector3 streamingBoxMax;
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.mapDataId = sysEndian::NtoL((s32)(uptr) &mapData);

	strLocalIndex parent = g_MapDataStore.GetParentSlot(strLocalIndex(g_MapDataStore.GetObjectIndex(index)));
	if (parent != -1)
	{
		parent = (s32) g_MapDataStore.GetStreamingIndex(parent).Get();
	}

	data.parentStrIndex = sysEndian::NtoL(parent.Get());

	spdAABB aabb;
	mapData.GetPhysicalExtents(aabb);
	data.physicsBoxMin = aabb.GetMin();
	data.physicsBoxMax = aabb.GetMax();
	mapData.GetStreamingExtents(aabb);
	data.streamingBoxMin = aabb.GetMin();
	data.streamingBoxMax = aabb.GetMax();

	Send(SV_REGISTER_MAPDATA, &data, sizeof(data), false);
}

void strStreamingVisualize::RegisterMapData(strIndex index, const fwMapDataDef &mapData)
{
	struct
	{
		s32 index;
		s32 mapDataId;
		s32 parentStrIndex;
		NetVector3 physicsBoxMin;
		NetVector3 physicsBoxMax;
		NetVector3 streamingBoxMin;
		NetVector3 streamingBoxMax;
	} data;

	strLocalIndex objIndex = strLocalIndex(g_MapDataStore.GetObjectIndex(index));
	data.index = sysEndian::NtoL((s32) index.Get());
	data.mapDataId = sysEndian::NtoL((s32)(uptr) &mapData);

	strLocalIndex parent = g_MapDataStore.GetParentSlot(objIndex);
	if (parent != -1)
	{
		parent = (s32) g_MapDataStore.GetStreamingIndex(parent).Get();
	}

	data.parentStrIndex = sysEndian::NtoL(parent.Get());

	spdAABB aabb = g_MapDataStore.GetPhysicalBounds(objIndex);
	data.physicsBoxMin = aabb.GetMin();
	data.physicsBoxMax = aabb.GetMax();
	g_MapDataStore.GetStreamingBounds(objIndex);
	data.streamingBoxMin = aabb.GetMin();
	data.streamingBoxMax = aabb.GetMax();

	Send(SV_REGISTER_MAPDATA, &data, sizeof(data), false);
}

void strStreamingVisualize::AddCullBox(const char *name, const spdAABB &aabb, const u32 *containerHashes, int containerCount)
{
	struct
	{
		char name[32];
		NetBox aabb;
		int count;
		int entries[512];
	} data;

	int realCount = Min(containerCount, NELEM(data.entries));
	safecpy(data.name, name);
	data.aabb = aabb;
	int copyCount = 0;

	for (int x=0; x<realCount; x++)
	{
		strLocalIndex objIndex = g_MapDataStore.FindSlotFromHashKey(containerHashes[x]);

		if (objIndex != -1)
		{
			strIndex index = g_MapDataStore.GetStreamingIndex(objIndex);
			data.entries[copyCount++] = sysEndian::NtoL((s32) index.Get());
		}
	}

	data.count = sysEndian::NtoL((s32) copyCount);

	Send(SV_ADD_CULLBOX, &data, sizeof(data) - sizeof(int) * (NELEM(data.entries) - copyCount), true);
}

void strStreamingVisualize::InstantiateMapData(const fwMapData &mapData)
{
	struct
	{
//		s32 index;
		s32 mapDataId;
	} data;

	//data.index = sysEndian::NtoL((s32) index.Get());
	data.mapDataId = sysEndian::NtoL((s32)(uptr) &mapData);

	Send(SV_INSTANTIATE_MAPDATA, &data, sizeof(data), false);
}

void strStreamingVisualize::AddMapDataEntity(strIndex index, fwEntity *entity, int guid)
{
	if (!m_UpdateEntities || !entity->GetArchetype() || entity->GetArchetype()->GetDrawableType() == fwArchetype::DT_ASSETLESS)
	{
		return;
	}

	struct
	{
		s32 index;
		s32 guid;
		s32 entityId;
		s32 entityStrIndexLodTypeEntityType;
		s32 entityFlags;
		NetBox16 entity;
		NetVector16 sphereCenter;
		s16 unused;
		float sphereRadius;
		float lodDistance;
		int parentEntityId;
	} data;

	strStreamingModule *streamingModule = fwArchetypeManager::GetStreamingModule();
	strLocalIndex objIndex = strLocalIndex(entity->GetModelIndex());
	strIndex archIndex = streamingModule->GetStreamingIndex(objIndex);

	data.index = sysEndian::NtoL((s32) index.Get());
	data.guid = sysEndian::NtoL(guid);
	data.entityId = sysEndian::NtoL((s32)(uptr) entity);
	u32 entityStrIndexLodTypeEntityType = ((u32)entity->GetLodData().GetLodType()) << 28;
	entityStrIndexLodTypeEntityType |= ((u32)entity->GetType()) << 24;
	entityStrIndexLodTypeEntityType |= (u32) archIndex.Get();

	data.entityStrIndexLodTypeEntityType = sysEndian::NtoL((u32) entityStrIndexLodTypeEntityType);
	data.entityFlags = sysEndian::NtoL(entity->GetBaseFlags());

	spdAABB aabb;
	entity->GetAABB(aabb);
	data.entity.boxMin = aabb.GetMin();
	data.entity.boxMax = aabb.GetMax();

	Vector3 vCentre;
	data.sphereRadius = m_EntityBoundSphereGetterCb(entity, vCentre);
	data.sphereCenter = RCC_VEC3V(vCentre);
	data.lodDistance = (float) entity->GetLodDistance();
	data.parentEntityId =  sysEndian::NtoL((s32)(uptr) entity->GetLod());
	sysEndian::NtoLMe(data.lodDistance);
	sysEndian::NtoLMe(data.sphereRadius);

	Send(SV_ADD_MAPDATA_ENTITY, &data, sizeof(data), false);
}

void strStreamingVisualize::SetLodParent(const fwEntity *entity, const fwEntity *lodParent)
{
	if (!m_UpdateEntities || !entity->GetArchetype() || entity->GetArchetype()->GetDrawableType() == fwArchetype::DT_ASSETLESS)
	{
		return;
	}

	struct
	{
		s32 entityId;
		s32 parentEntityId;
	} data;

	data.entityId = sysEndian::NtoL((s32)(uptr) entity);
	data.parentEntityId = (lodParent) ? sysEndian::NtoL((s32)(uptr) lodParent) : 0;

	Send(SV_SET_LOD_PARENT, &data, sizeof(data), false);
}

void strStreamingVisualize::SetMarkerText(const char *markerText, ...)
{
	struct
	{
		float time;
		u16 futureUse;
		u16 strLen;
		char text[128];
	} data;

	va_list args;
	va_start(args, markerText);
	vformatf(data.text, markerText, args);
	va_end(args);
		
	int len = istrlen(data.text);
	data.strLen = sysEndian::NtoL((u16) (len + 1));
	data.time = s_Timer.GetTime();
	sysEndian::NtoLMe(data.time);

	int packetSize = sizeof(data) - sizeof(data.text) + len + 1;
	packetSize += 3;
	packetSize &= 0xfffffffc;

	Send(SV_SET_MARKER_TEXT, &data, packetSize, true);
}

void strStreamingVisualize::SendEntityList(int command, const fwEntity **entities, int entityCount)
{
	struct
	{
		u32 entityCount;
		s32 entityIds[256];
	} data;

	while (entityCount > 0)
	{
		int count = Min(entityCount, NELEM(data.entityIds));

		for (int x=0; x<count; x++)
		{
			data.entityIds[x] = sysEndian::NtoL((s32) (uptr) *(entities++));
		}

		data.entityCount = sysEndian::NtoL(count);
		int packetSize = sizeof(data) - sizeof(data.entityIds) + count * sizeof(s32);
		Send(command, &data, packetSize, true);

		entityCount -= count;
	}
}
/*
void strStreamingVisualize::UpdateVisibility(const fwEntity **nowVisible, int nowVisibleCount, const fwEntity **nowInvisible, int nowInvisibleCount)
{
	SendEntityList(SV_ADD_VISIBLE_ENTITIES, nowVisible, nowVisibleCount);
	SendEntityList(SV_REMOVE_VISIBLE_ENTITIES, nowInvisible, nowInvisibleCount);
}
*/
void strStreamingVisualize::UpdateVisFlags(VisibilityEntry *entries, int entryCount)
{
	struct EntityVisFlagChange
	{
		s32 entityId;
		u32 phaseFlags;
		u32 optionFlags;
	};

	const int maxCount = 256;

	struct
	{
		s32 count;
		EntityVisFlagChange entry[maxCount];
	} data;

	while (entryCount)
	{
		int batchSize = Min(entryCount, maxCount);
		entryCount -= batchSize;

		data.count = sysEndian::NtoL(batchSize);

		for (int x=0; x<batchSize; x++)
		{
			data.entry[x].entityId = sysEndian::NtoL((s32) (uptr) entries[x].m_Entity);
			data.entry[x].phaseFlags = sysEndian::NtoL(entries[x].m_PhaseFlags);
			data.entry[x].optionFlags = sysEndian::NtoL(entries[x].m_OptionFlags);
		}

		entries += batchSize;

		Send(SV_UPDATE_VISFLAGS, &data, sizeof(data) - (maxCount - batchSize) * sizeof(EntityVisFlagChange), true);
	}
}

void strStreamingVisualize::RegisterVisFlag(u32 visFlag, bool isOptionFlag, const char *name)
{
	struct
	{
		int visFlag;
		char name[16];
	} data;

	data.visFlag = sysEndian::NtoL(visFlag | (isOptionFlag ? 0x80000000 : 0));
	safecpy(data.name, name);

	Send(SV_REGISTER_VISFLAG, &data, sizeof(data), false);
}

void strStreamingVisualize::RegisterNamedFlag(NamedFlagType namedFlagType, int flagValue, const char *typeName)
{
	struct
	{
		int namedFlagType;
		int flagValue;
		char name[16];
	} data;

	data.namedFlagType = sysEndian::NtoL(namedFlagType);
	data.flagValue = sysEndian::NtoL(flagValue);
	safecpy(data.name, typeName);

	Send(SV_REGISTER_NAMED_FLAG, &data, sizeof(data), false);
}

void strStreamingVisualize::AddBoxSearch(const spdAABB &aabb, int searchTypeId)
{
	struct
	{
		int searchTypeId;
		NetBox16 aabb;
	} data;

	data.aabb = aabb;
	data.searchTypeId = sysEndian::NtoL(searchTypeId);

	Send(SV_ADD_BOX_SEARCH, &data, sizeof(data), false);
}

void strStreamingVisualize::SetFlags(strIndex index, u32 flags)
{
	struct
	{
		s32 index;
		u32 flags;
		char context[16];
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.flags = sysEndian::NtoL(flags);
	const char *context = GetContextName();

	safecpy(data.context, context);

	Send(SV_SET_FLAGS, &data, sizeof(data), false);
}

void strStreamingVisualize::ChangeGameState(u32 addFlags, u32 remFlags)
{
	struct
	{
		u32 addFlags;
		u32 remFlags;
	} data;

	data.addFlags = sysEndian::NtoL(addFlags);
	data.remFlags = sysEndian::NtoL(remFlags);

	Send(SV_CHANGE_GAME_STATE, &data, sizeof(data), false);
}

void strStreamingVisualize::RegisterStateFlag(u32 flagMask, const char *name)
{
	struct
	{
		u32 flagMask;
		char stateName[32];
	} data;

	data.flagMask = sysEndian::NtoL(flagMask);
	safecpy(data.stateName, name);

	Send(SV_REGISTER_STATE_FLAG, &data, sizeof(data), false);
}


void strStreamingVisualize::AddCallstackToMarker()
{
#if !__FINAL
	static sysCriticalSectionToken cs;

	SYS_CS_SYNC(cs);

	m_CallstackMarker[0] = 0;
	sysStack::PrintStackTrace(StackTraceCapture, 1);

	if (m_CallstackMarker[0])
	{
		SetMarkerText(m_CallstackMarker);
	}
#endif // !__FINAL
}

void strStreamingVisualize::StackTraceCapture(size_t addr, const char *symbol, size_t offset)
{
	GetInstance().StackTraceCaptureInternal(addr, symbol, offset);
}

void strStreamingVisualize::StackTraceCaptureInternal(size_t /*addr*/, const char *symbol, size_t /*offset*/)
{
	int symlen = istrlen(symbol);
	int existing = istrlen(m_CallstackMarker);

	if (existing > 0 && existing + symlen > 128)
	{
		// Won't fit in one line. Let's send what we have.
		SetMarkerText(m_CallstackMarker);
		m_CallstackMarker[0] = 0;
	}

	safecat(m_CallstackMarker, symbol);
	safecat(m_CallstackMarker, " %% ");
}

void strStreamingVisualize::DummyAddStackContext()
{
	if (IsInstantiated())
	{
		GetInstance().AddCallstackToMarker();
	}
}

void strStreamingVisualize::AddToNeededList(const NeededListEntry *neededList, int neededListCount)
{
	struct NeededListEntryInternal
	{
		s32 m_EntityId;
		float m_Score;
		//int m_Flags;	// TODO: This could be a char.
	};

	struct
	{
		int count;
		NeededListEntryInternal entry[256];
	} data;

	while (neededListCount > 0)
	{
		int toCopy = Min(neededListCount, 256);

		for (int x=0; x<toCopy; x++)
		{
			data.entry[x].m_EntityId = sysEndian::NtoL((s32)(uptr) neededList[x].m_Entity);
			data.entry[x].m_Score = neededList[x].m_Score;
			sysEndian::NtoLMe(data.entry[x].m_Score);
			//data.entry[x].m_Flags = sysEndian::NtoL((u32) neededList[x].m_Flags);
		}

		data.count = sysEndian::NtoL((s32)(uptr) toCopy);

		Send(SV_ADD_TO_NEEDED_LIST, &data, sizeof(data) - sizeof(data.entry) + sizeof(NeededListEntryInternal) * toCopy, true);
		neededList += toCopy;
		neededListCount -= toCopy;
	}
}


void strStreamingVisualize::FailedAllocation(strIndex index, FailedAllocReason reason)
{
	struct
	{
		s32 index;
		int reason;
	} data;

	data.index = sysEndian::NtoL((s32) index.Get());
	data.reason = sysEndian::NtoL((s32) reason);

	Send(SV_FAILED_ALLOCATION, &data, sizeof(data), false);
}

void strStreamingVisualize::MarkAssert(const char *message)
{
	AddMarker(ASSERT_OCCURRED);
	SetMarkerText(message);
}

void strStreamingVisualize::SetTrackedStreamerRead(bool isTracked)
{
	st_IsTrackedRead = isTracked;
}

void strStreamingVisualize::StreamerReadCallback(const char *filename,u32 lsn,int device,u32 /*handle*/, u32 readId)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.StreamerRead(device, readId, lsn, filename);
	}	
}

bool strStreamingVisualize::StreamOpenCallback(const char *filename,bool /*readonly*/, fiHandle fd)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		// We don't have an accessor for this?!
		if (strncmp(filename,"memory:",7))
		{
			u32 handle = (u32) (uintptr_t) fd;
			STREAMINGVIS.OpenRawFile(filename, handle);
			STRVIS_PROCESS_REQUEST(0, handle);
		}
	}

	return true;
}

bool strStreamingVisualize::StreamCreateCallback(const char *filename, fiHandle fd)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		// We don't have an accessor for this?!
		if (strncmp(filename,"memory:",7))
		{
			u32 handle = (u32) (uintptr_t) fd;
			STREAMINGVIS.OpenRawFile(filename, handle);
			STRVIS_PROCESS_REQUEST(0, handle);
		}
	}

	return true;
}

void strStreamingVisualize::StreamReadCallback(fiHandle fd, int /*count*/)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.SetStreamerHandle((u32) (uintptr_t) fd);
	}
}

void strStreamingVisualize::StreamCloseCallback(const char * /*name*/, fiStream &stream)
{
	u32 handle = (u32) (uintptr_t) stream.GetLocalHandle();
	if (stream.GetDevice() != &fiDeviceMemory::GetInstance())
	{
		STRVIS_FINISH_REQUEST(0, handle);
	}
}

void strStreamingVisualize::StreamerIdleCallback(int device)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.StreamerIdle(device);
	}
}

void strStreamingVisualize::StreamerReadBulkCallback(u64 /*offset*/,int count,int device,u32 readId)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.SetStreamerHandle(readId);
		STREAMINGVIS.SetQueue(device);
	}
	if(device == pgStreamer::HARDDRIVE)
	{
		ReadHDD.Add(count);
	}
	if(device == pgStreamer::OPTICAL)
	{
		ReadODD.Add(count);
	}
}

void strStreamingVisualize::StreamerDecompressCallback(const char * /*filename*/,int device,u32 readId, bool isStarting)
{
	if (strStreamingVisualize::IsInstantiated())
	{
		STREAMINGVIS.StreamerDecompress(device, readId, isStarting);
	}
}

void strStreamingVisualize::OnFlagsChange(strIndex index, u32 oldFlags, u32 newFlags)
{
	// Is it a flag we care about?
	if ((oldFlags ^ newFlags) & MONITORED_STREAMING_FLAGS)
	{
		if (strStreamingVisualize::IsInstantiated())
		{
			strStreamingVisualize::GetInstance().SetFlags(index, newFlags);
		}
	}
}

const char *strStreamingVisualize::GetContextName() const
{
	if (m_CurrentContext >= 0 && m_CurrentContext < NELEM(s_ContextNames) && s_ContextNames[m_CurrentContext])
	{
		return s_ContextNames[m_CurrentContext];
	}

	if (m_GetStreamingContextCb)
	{
		return m_GetStreamingContextCb();
	}

	return "";
}

void strStreamingVisualize::SetStreamerHandle(u32 handle)
{
	st_StreamerHandle = handle;
}

void strStreamingVisualize::SetQueue(u32 queue)
{
	st_Queue = queue;
}


void strStreamingVisualize::SetStreamingVolume(int volIndex, StreamingVolumeType type, Vec3V_In pos, Vec3V_In dir, float farClipOrRadius, u32 flags)
{
	StreamingVolumeInfo &volume = m_StreamingVolumes[volIndex];

	volume.m_Type = type;
	volume.m_Pos = pos;
	volume.m_Dir = dir;
	volume.m_Flags = flags;
	volume.m_Radius = farClipOrRadius;
}


void strStreamingVisualize::InitClass()
{
	streamAssert(!sm_Instance);

	if (PARAM_streamingvisualize.Get())
	{
#if RSG_ORBIS
		netHardware::Init();
#endif // RSG_ORBIS

		USE_DEBUG_MEMORY();
		sm_Instance = rage_new strStreamingVisualize(false);		

		fiDevice::SetDeviceProfiler(&s_StrVizDeviceProfiler);
	}
#if __DEVPROF

	if(PARAM_streamingekg.Get())
	{
		s_ShowBandwidthGraph = true;
		fiDevice::SetDeviceProfiler(&s_StrVizDeviceProfiler);
		pgStreamer::SetReadBulkCallback(StreamerReadBulkCallback); //set this so we can track what the two reade thread are up to
	}
#endif // __DEVPROF
}

void strStreamingVisualize::ShutdownClass()
{
	delete sm_Instance;
	sm_Instance = NULL;
}


#if !__FINAL

int StrTicker::Put = 0;
int StrTicker::Get = 3;
StrTicker* StrTicker::First = 0;
sysTimer StrTicker::timer;
bool StrTicker::initialised = false;
volatile u32 StrTicker::read_size = 0;

u32	StrTicker::MaxPendingKB = 512;	 //this values are set externally
u32	StrTicker::MaxConvertMS = 8;	 //this values are set externally
u8	StrTicker::Marker[StrTicker::BufferSize];

const float StrTicker::ScaleX = 4.0f;
const float StrTicker::ScaleY = 0.125f;

//copied from the grcsetup.cpp ticker, will remerge them at somepoint
StrTicker::StrTicker(const char *label,const float r,const float g,const float b, const bool active) 
{ 
	memset(Data,0,sizeof(Data)); 
	Label = label;
	Color.Setf(r,g,b);
	Next = First;
	First = this;
	Active = active;
	BytesLastSecond = 0;
	BytesCurrentSecond = 0;

}

void StrTicker::Add(const u32 value) 
{	
	Data[Put] += value;//int((value / 1024.0f) * ScaleY); 
	BytesCurrentSecond += int(value);
}	

void StrTicker::AddMarker(const EMarker marker)
{
	Marker[Put] |= marker;
}

void StrTicker::AddBytes(const u32 bytes)
{
	sysInterlockedAdd(&read_size, bytes);
}

void StrTicker::ClearBytes(void)
{
	sysInterlockedExchange(&read_size, 0);
}

void StrTicker::SetMaxPendingKB(const u32 kb)
{
	MaxPendingKB = kb;
}

void StrTicker::SetMaxConvertMS(const u32 ms)
{
	MaxConvertMS = ms;
}

void StrTicker::Init() 
{
	initialised = true;
	timer.Reset();
	for (StrTicker *t = First; t; t=t->Next) {
		t->BytesLastSecond = 0;
		t->BytesCurrentSecond = 0;
	}
}

void StrTicker::Draw(float x,float y) 
{
	static const short steps[] = {2028,1536,1024,512,256,128};
	static const short marker_step = 128;
	if(!initialised)
		Init();

	//get the MB/S reading	
	if(timer.GetMsTime() > 1000){		
		timer.Reset();
		for (StrTicker *t = First; t; t=t->Next) {
			t->BytesLastSecond = t->BytesCurrentSecond;
			t->BytesCurrentSecond = 0;			
		}
	}
	if(!s_ShowBandwidthGraph)
		return;		

	grcBindTexture(NULL);
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	//draws the guide lines so we can scan how much it is doing
	grcBegin(drawLines,14);	
	//normal guide lines	
	grcColor4f(0.7f,0.7f,0.7f,1.0f);	
	grcVertex2f(x,y - steps[0] * ScaleY); grcVertex2f(x + History * ScaleX,y - steps[0] * ScaleY);
	grcVertex2f(x,y - steps[1] * ScaleY); grcVertex2f(x + History * ScaleX,y - steps[1] * ScaleY);
	grcVertex2f(x,y - steps[2] * ScaleY); grcVertex2f(x + History * ScaleX,y - steps[2] * ScaleY);
	grcVertex2f(x,y - steps[3] * ScaleY); grcVertex2f(x + History * ScaleX,y - steps[3] * ScaleY);
	grcVertex2f(x,y - steps[4] * ScaleY); grcVertex2f(x + History * ScaleX,y - steps[4] * ScaleY);
	grcVertex2f(x,y - steps[5] * ScaleY); grcVertex2f(x + History * ScaleX,y - steps[5] * ScaleY);	
	//show the max KB/Frame Limit
	grcColor4f(0.7f,0.3f,0.3f,1.0f);
	grcVertex2f(x,y - MaxPendingKB * ScaleY); grcVertex2f(x + History * ScaleX,y - MaxPendingKB * ScaleY);	

	grcEnd();

	float lx = x - 30.0f, ly = y - 10.0f;
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	//draws the labels for each guide line
	grcColor4f(0.7f,0.7f,0.7f,1.0f);
	grcDraw2dText(lx - 8.0f,y - steps[0] * ScaleY - 6.0f, "2MB");
	grcDraw2dText(lx - 8.0f,y - steps[1] * ScaleY - 6.0f, "1.5MB");
	grcDraw2dText(lx - 8.0f,y - steps[2] * ScaleY - 6.0f, "1MB");
	grcDraw2dText(lx - 8.0f,y - steps[3] * ScaleY - 6.0f, "0.5MB");	
	grcDraw2dText(lx - 8.0f,y - steps[4] * ScaleY - 6.0f, "0.25MB");	
	grcDraw2dText(lx - 8.0f,y - steps[5] * ScaleY - 6.0f, "0.125MB");		

	//marker types
	grcColor4f(1.0f,0.0f,0.0f,1.0f);
	grcDraw2dText(x + History * ScaleX,y + marker_step*1 * ScaleY - 12.0f, "Pending Limit (HDD)");
	grcColor4f(0.0f,1.0f,0.0f,1.0f);
	grcDraw2dText(x + History * ScaleX,y + marker_step*2 * ScaleY - 12.0f, "KB Limit (HDD)");
	grcColor4f(0.0f,0.0f,1.0f,1.0f);
	grcDraw2dText(x + History * ScaleX,y + marker_step*3 * ScaleY - 12.0f, "MS Limit (HDD)");

	grcColor4f(1.0f,0.0f,0.0f,1.0f);
	grcDraw2dText(x + History * ScaleX,y + marker_step*4 * ScaleY - 12.0f, "Pending Limit (ODD)");
	grcColor4f(0.0f,1.0f,0.0f,1.0f);
	grcDraw2dText(x + History * ScaleX,y + marker_step*5 * ScaleY - 12.0f, "KB Limit (ODD)");
	grcColor4f(0.0f,0.0f,1.0f,1.0f);
	grcDraw2dText(x + History * ScaleX,y + marker_step*6 * ScaleY - 12.0f, "MS Limit (ODD)");
	
	grcColor4f(0.7f,0.3f,0.3f,1.0f);
	grcDraw2dText(x + History * ScaleX,y - MaxPendingKB * ScaleY - 6.0f, "MaxPKB");	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
	//draws the labels for each feed	
	char str[64];
	float mbs = 0;
	if(StreamHDD.Active){
		grcColor(StreamHDD.Color);
		mbs = float(StreamHDD.BytesLastSecond) / float(1048576);
		sprintf(str, "%2.4f MB/s (HDD I/O)", mbs);
		grcDraw2dText(lx,ly,str);
		ly += 10.0f;
	}
	if(StreamAvg.Active){
		grcColor(StreamAvg.Color);
		mbs = float(StreamAvg.Data[Put]) / float(1048576);
		sprintf(str, "%2.4f MB/frames", mbs);
		grcDraw2dText(lx,ly,str);
		ly += 10.0f;
	}
	if(ReadHDD.Active){
		grcColor(ReadHDD.Color);
		grcDraw2dText(lx,ly,ReadHDD.Label);
		ly += 10.0f;
	}
	if(ReadODD.Active){
		grcColor(ReadODD.Color);
		grcDraw2dText(lx,ly,ReadODD.Label);
	}

	int ix = (int) x, iy = (int) y;
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//draws vertical lines for each ticker feed	
	for (int i=0, j=Get; i<History; i++,j++) {
		u32 nLines = 0;
		if (j==BufferSize) j = 0;

		if(Marker[j] & HDD_PENDING_LIMIT){nLines++;}
		if(Marker[j] & HDD_KB_LIMIT){nLines++;}
		if(Marker[j] & HDD_MS_LIMIT){nLines++;}
		if(Marker[j] & ODD_PENDING_LIMIT){nLines++;}
		if(Marker[j] & ODD_KB_LIMIT){nLines++;}
		if(Marker[j] & ODD_MS_LIMIT){nLines++;}

		if(nLines)
		{
			grcBegin(drawLines,2*nLines);			
			if(Marker[j] & HDD_PENDING_LIMIT)
			{
				grcColor4f(1.0f,0.0f,0.0f,1.0f);
				grcVertex2f((float)(ix + i * ScaleX), y);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step * ScaleY);
			}
			if(Marker[j] & HDD_KB_LIMIT)
			{
				grcColor4f(0.0f,1.0f,0.0f,1.0f);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step * ScaleY);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*2 * ScaleY);
			}
			if(Marker[j] & HDD_MS_LIMIT)
			{
				grcColor4f(0.0f,0.0f,1.0f,1.0f);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*2 * ScaleY);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*3 * ScaleY);
			}
			if(Marker[j] & ODD_PENDING_LIMIT)
			{
				grcColor4f(1.0f,0.0f,0.0f,1.0f);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*3 * ScaleY);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*4 * ScaleY);
			}
			if(Marker[j] & ODD_KB_LIMIT)
			{
				grcColor4f(0.0f,1.0f,0.0f,1.0f);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*4 * ScaleY);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*5 * ScaleY);
			}
			if(Marker[j] & ODD_MS_LIMIT)
			{
				grcColor4f(0.0f,0.0f,1.0f,1.0f);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*5 * ScaleY);
				grcVertex2f((float)(ix + i * ScaleX), y + marker_step*6 * ScaleY);
			}
			grcEnd();
		}
	};	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	//draws horizontal lines for each ticker feed	
	float value;
	for (StrTicker *t = First; t; t=t->Next) {
		if(!t->Active)continue;
		grcBegin(drawLineStrip,History);
		grcColor(t->Color);
		for (int i=0, j=Get; i<History; i++,j++) {
			if (j==BufferSize) j = 0;
			value = (float(t->Data[j]) / 1024.0f) * ScaleY; 
			grcVertex2f((float)(ix + i * ScaleX), (float)(iy - value));
		};
		grcEnd();
	}
}

void StrTicker::StepPut() 
{
	// in some cases we step on update thread but not on render, clamp here so we don't go too far
	if (Put == (Get - 1))
		return;
	if (++Put == BufferSize)
		Put = 0;

	for (StrTicker *t = First; t; t=t->Next) {
		t->Data[Put]=0;
	}
	//set the marker for this frame to 0;
	Marker[Put] = 0;

	//cheap hack here, average the last 30 values from HDD 
	int total = 0;
	for(int i=0;i<30;i++)
	{
		int idx = (Put - i)%History;
		total += StreamHDD.Data[idx];
	}	
	int avg = total / 30;
	StreamAvg.Add(avg);

	PF_SET( StreamingKB, float(StrTicker::read_size)/1024.0f );	
	ClearBytes();
}
void StrTicker::StepGet() 
{
	// in some cases (e.g. loading) we do the render calls but not update,
	// need to make sure Get is always in sync with Put or the graphs will be rendered odd.
	if (Get == (Put + 3))
		return;

	if (++Get == BufferSize)
		Get = 0;
}

void StrTicker::Reset() 
{
	Put = 0;
	Get = 3;
}

#if __BANK
void StrTicker::AddWidgets(bkGroup &group)
{
	bkGroup *sbgGroup = group.AddGroup("Streaming Bandwidth Graph");
	sbgGroup->AddToggle("Show Bandwidth Graph", &s_ShowBandwidthGraph);
	for (StrTicker *t = First; t; t=t->Next) {
		sbgGroup->AddToggle(t->Label, &t->Active);
	}
}
#endif

#endif


#if __BANK
void strStreamingVisualize::AddWidgets(bkGroup &group)
{
	bkGroup *svGroup = group.AddGroup("Streaming Visualize");
	svGroup->AddButton("Add Marker", datCallback(MFA(strStreamingVisualize::AddMarkerWithText), this));
	svGroup->AddText("Marker Text", s_MarkerText, sizeof(s_MarkerText));
	svGroup->AddToggle("Update Entities", &m_UpdateEntities);			
}
#endif // __BANK


} // namespace rage

#endif // STREAMING_VISUALIZE
