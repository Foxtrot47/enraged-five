//
// streaming/streaminginstall_psn.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#if __PPU
#include <sys/memory.h>

#include "streaminginstall_psn.h"
#include "streaming_channel.h"
#include "file/savegame.h"
#include "file/device.h"
#include "file/asset.h"
#include "math/amath.h"
#include "system/criticalsection.h"
#include "system/timer.h"
#include "system/nelem.h"
#include "system/param.h"
#include "file/device_installer.h"

#include <sysutil/sysutil_msgdialog.h>

#if !__FINAL
XPARAM(update);
#endif
// :TODO: use correct SFO info..
static const char* SAMPLE_PARAMSFO_VERSION = "01.00";

using namespace rage;

//	The PS3 can only have 31 game data files open at the same time so use a queue to ensure
//	that we don't go over this limit. This queue is shared between the installed GTA4 files and
//	the files for the currently active episode
atQueue<fiFileDetails*, fiFileDetails::MAX_OPEN_FILES> fiFileDetails::m_OpenFiles;

fiPsnUpdateDevice fiPsnUpdateDevice::m_device;
fiPsnExtraContentDevice fiPsnExtraContentDevice::m_device;
char fiPsnExtraContentDevice::ms_GameDataPath[CELL_GAME_PATH_MAX];	//	will store the path to the game data directory on the PS3's HDD

/////////////////////////////////////
//start of psn update device
/////////////////////////////////////

fiPsnUpdateDevice& fiPsnUpdateDevice::Instance()
{
	static bool instanced = false;

	if(!instanced)
	{
		instanced = true;

#if !__FINAL
		const char* pUpdateFolder;
		if(PARAM_update.Get(pUpdateFolder))
		{
			static fiDeviceRelative gUpdateDevice;
			gUpdateDevice.Init(pUpdateFolder, false);
			gUpdateDevice.MountAs("update:/");
			Printf("Mount %s as update:", pUpdateFolder);
		}
		else
#endif
		{
			char contentInfoPath[CELL_GAME_PATH_MAX] = { 0 }, gameDataPath[CELL_GAME_PATH_MAX]  = { 0 };

			if(!fiDeviceInstaller::GetIsBootedFromHdd())
			{
				CellGameContentSize contentSize;
				u32 result = cellGameDataCheck ( CELL_GAME_GAMETYPE_GAMEDATA, XEX_TITLE_ID, &contentSize );

				if(result != CELL_OK)
				{
					streamWarningf("fiPsnUpdateDevice::Failed to retrieve the gamedata directory (%d)", result);
				}

				result = cellGameContentPermit ( contentInfoPath, gameDataPath );

				if(result != CELL_OK)
					streamWarningf("fiPsnUpdateDevice::Failed to retrieve the gamedata directory (%d)", result);
			}
			else
			{
				safecpy(gameDataPath, fiDeviceInstaller::GetHddBootPath());
			}

			if(gameDataPath[0])
			{
				Displayf("fiPsnUpdateDevice::Instance mounting udpate to %s", gameDataPath);
				m_device.Init(gameDataPath, false);
				m_device.MountAs("update:/");
			}
		}
	}

	return m_device;
}


/////////////////////////////////////
//end of psn update device
/////////////////////////////////////



/////////////////////////////////////
//start of psn extra content device
/////////////////////////////////////

fiPsnExtraContentDevice& fiPsnExtraContentDevice::Instance()
{
	static bool instanced = false;

	if(!instanced)
	{
		instanced = true;

		ms_GameDataPath[0] = '\0';

	//	g_EndEnumerateSema = sysIpcCreateSema(0);
//		g_StartEnumerateSema = sysIpcCreateSema(0);

	#if !__FINAL
/*		const char* pExtraContentFolder;
		if(PARAM_extracontent.Get(pExtraContentFolder))
		{
			static fiDeviceRelative gDLCDevice;
			gDLCDevice.Init(pExtraContentFolder, false);
			gDLCDevice.MountAs("dlc:/");
			Printf("Mount %s as dlc:", pExtraContentFolder);
			m_DLC = false;
		}
		else*/
	#endif
		{
		//	m_DLC = true; // using EDATA files
			//if(!CGame::IsHDDGame())

			if (1)
			{
				bool installIcon = false;
				CellGameContentSize contentSize;
				u32 result = cellGameDataCheck(CELL_GAME_GAMETYPE_GAMEDATA, XEX_TITLE_ID, &contentSize);

				if (result == CELL_GAME_RET_NONE)
				{
					char tmp_contentInfoPath[CELL_GAME_PATH_MAX];
					char tmp_usrdirPath[CELL_GAME_PATH_MAX];
					CellGameSetInitParams init;
					memset(&init, 0x00, sizeof(init));
					strncpy(init.title,   "Grand Theft Auto V - Content", CELL_GAME_SYSP_TITLE_SIZE - 1) ;
					strncpy(init.titleId, XEX_TITLE_ID, CELL_GAME_SYSP_TITLEID_SIZE - 1) ;
					strncpy(init.version, "01.00", CELL_GAME_SYSP_VERSION_SIZE - 1) ;
					result = cellGameCreateGameData( &init, tmp_contentInfoPath, tmp_usrdirPath ) ;

					if(result != CELL_OK)
					{
						Errorf("InitInstall: Failed to create the gamedata directory (%x)", result);

						// Re-enable calls to cellGameDataCheck()
						char contentInfoPath[CELL_GAME_PATH_MAX];
						cellGameContentPermit(contentInfoPath, ms_GameDataPath);
					}
					else
						installIcon = true;
				}

				char contentInfoPath[CELL_GAME_PATH_MAX];
				result = cellGameContentPermit ( contentInfoPath, ms_GameDataPath );

				if(result != CELL_OK)
					streamErrorf("fiPsnExtraContentDevice::Failed to retrieve the gamedata directory (%d)", result);

				if (installIcon)
					CopyContentInfoFile("ICON0.PNG", contentInfoPath);
			}
		}
	}

	return m_device;
}

bool fiPsnExtraContentDevice::CopyContentInfoFile(const char* filename, const char* contentInfoPath)
{
	const fiDevice& localDevice = fiDeviceLocal::GetInstance();

	char srcFile[CELL_GAME_PATH_MAX] = { 0 };
	char dstFile[CELL_GAME_PATH_MAX] = { 0 };

	if (fiDeviceInstaller::GetIsBootedFromHdd())
	{
		safecpy(srcFile, fiDeviceInstaller::GetHddBootPath(), sizeof(srcFile));

		char* tempSrcFile = strstr(srcFile, "/USRDIR");

		if (tempSrcFile)
		{
			tempSrcFile++;
			*tempSrcFile = 0;
		}

		strncat(srcFile, filename, CELL_GAME_PATH_MAX);
	}
	else
		formatf(srcFile, sizeof(srcFile), "/dev_bdvd/PS3_GAME/%s", filename);

	formatf(dstFile, "%s/%s", contentInfoPath, filename);
	fiHandle h = localDevice.Open(srcFile, true);
#if !__FINAL
	if (!fiIsValidHandle(h))
	{
		formatf(srcFile, "/app_home/%s", filename);
		h = localDevice.Open(srcFile, true);
	}
#endif

	if (!fiIsValidHandle(h))
		return false;

	u32 copySize = (u32)localDevice.GetFileSize(srcFile);
	char* buffer = Alloca(char, copySize);
	if (localDevice.Read(h, buffer, copySize) != copySize)
	{
		localDevice.Close(h);
		return false;
	}

	localDevice.Close(h);
	h = localDevice.Create(dstFile);
	if (!fiIsValidHandle(h))
		return false;

	if (localDevice.Write(h, buffer, copySize) != copySize)
	{
		localDevice.Close(h);
		localDevice.Delete(dstFile);
		return false;
	}

	localDevice.Close(h);
	return true;
}
/////////////////////////////////////
//end of psn extra content device
/////////////////////////////////////


//	It seems like I will have to put critical sections around all open, close and access functions
//	for files in fiPsnInstallerDevice and fiDeviceRelativePsn since opening one file could
//	force another one to close
static sysCriticalSectionToken	FileAccessCriticalSectionToken;			// Create the shared token


fiPsnInstallerDevice::fiPsnInstallerDevice(bool forceReinstall)
:	m_StartedInstalling(false)
,	m_FinishedInstalling(false)
,   m_InstallFailed(false)
,   m_NotEnoughSpace(false)
,	m_ForceReinstall(forceReinstall)
,	m_BytesCopied(0)
,	m_InstallSize(0)
{	
}

fiHandle fiPsnInstallerDevice::InstallFile(const char* path, bool readOnly, const char* installedname, bool isContentInfo)
{
	streamAssertf(!m_StartedInstalling, "Can't add an installable file after game installation");
	fiHandle handle = (fiHandle)m_Files.GetCount();
	fiInstalledFile& f = m_Files.Grow();	
	safecpy(f.m_SrcFilename, path, NELEM(f.m_SrcFilename));
	if (!installedname || !installedname[0])
		installedname = ASSET.FileName(path);
	safecpy(f.m_InstallName, installedname, NELEM(f.m_InstallName));	
	f.m_Handle = fiHandleInvalid;
	f.m_ReadOnly = readOnly;
	f.m_IsContentInfo = isContentInfo;
	f.m_SeekPos = 0;
	f.m_Installed = false;
	return handle;
}

fiHandle fiPsnInstallerDevice::LocalHandle(fiHandle handle) const
{
	fiInstalledFile& f = m_Files[(int)handle];
	streamAssertf(f.m_Installed, "Trying to access a file that hasn't finished installing");
	if (f.m_Handle == fiHandleInvalid)
	{
		const fiDevice& localdev = fiDeviceLocal::GetInstance();
		if (m_OpenFiles.IsFull())
		{
			// out of file handles, close the oldest one
			fiInstalledFile* fileToClose = m_OpenFiles.Pop();
			streamAssert(fileToClose->m_Handle != fiHandleInvalid);
			fileToClose->m_SeekPos = localdev.Seek(fileToClose->m_Handle, 0, seekCur);
			localdev.Close(fileToClose->m_Handle);
			fileToClose->m_Handle = fiHandleInvalid;
		}
		char destname[2048];
		GetInstalledPath(destname, NELEM(destname), f);
		const_cast<fiInstalledFile&>(f).m_Handle = localdev.Open(destname, f.m_ReadOnly);
		streamAssertf(f.m_Handle != fiHandleInvalid, 
			"Error opening installed file %s (installed from %s)", destname, f.m_SrcFilename);
		localdev.Seek(f.m_Handle, f.m_SeekPos, seekSet);		
		m_OpenFiles.Push(&f);
	}
	return f.m_Handle;
}

int fiPsnInstallerDevice::Close(fiHandle handle) const
{
	fiInstalledFile& f = m_Files[(int)handle];
	if (f.m_Handle != fiHandleInvalid)
	{
		fiDeviceLocal::GetInstance().Close(f.m_Handle);
		f.m_Handle = fiHandleInvalid;
		int i = 0;
		streamVerify(m_OpenFiles.Find(&f, &i));
		m_OpenFiles.Delete(i);
	}
	return 0;
}


bool fiPsnInstallerDevice::InitInstall()
{
	m_StartedInstalling = true;
	// s_InstallerDevice = this;
	// create gamedata directory or find existing one
	CellGameContentSize contentSize;
	u32 result = cellGameDataCheck ( CELL_GAME_GAMETYPE_GAMEDATA, XEX_TITLE_ID, &contentSize );

	int requiredSpace = CalculateSizeToInstall();

#if HACK_GTA4
	// KS - Check for required trophy HDD space.
	requiredSpace += 50 * 1024; // reserve space for trophies TRC 30.0 [R154]
#endif	//	HACK_GTA4

	int need_size = contentSize.hddFreeSizeKB - contentSize.sysSizeKB - requiredSpace;

	if(need_size < 0)
	{
		streamDisplayf("Not enough space to install. free (%i), sysSize (%i), sizeToInstall (%i)", contentSize.hddFreeSizeKB, contentSize.sysSizeKB, requiredSpace);
		// if we have a dialog already open (such as progress dialog) then we need to close it
		m_NotEnoughSpace = true;
		cellMsgDialogAbort();
		cellGameContentErrorDialog( CELL_GAME_ERRDIALOG_NOSPACE_EXIT, -need_size, NULL);
		m_InstallFailed = true;

		//if (m_progressCallback)
		//	m_progressCallback(InstallStatus(), InstallProgress());
		return false;
	}
	else
	{
		// we have enough space
		if(result == CELL_GAME_RET_NONE)
		{
			char tmp_contentInfoPath[CELL_GAME_PATH_MAX];
			char tmp_usrdirPath[CELL_GAME_PATH_MAX];
			CellGameSetInitParams init;
			memset(&init, 0x00, sizeof(init));
			strncpy(init.title,   "Grand Theft Auto 5", CELL_GAME_SYSP_TITLE_SIZE - 1) ;
			strncpy(init.titleId, XEX_TITLE_ID, CELL_GAME_SYSP_TITLEID_SIZE - 1) ;
			strncpy(init.version, SAMPLE_PARAMSFO_VERSION, CELL_GAME_SYSP_VERSION_SIZE - 1) ;
			result = cellGameCreateGameData( &init, tmp_contentInfoPath, tmp_usrdirPath ) ;
		}
	}

	if(result != CELL_OK)
	{
		streamErrorf("fiPsnInstallerDevice::InstallOpenedFiles Failed to create the gamedata directory (%x)", result);
		m_InstallFailed = true;
		if (m_progressCallback)
			m_progressCallback(InstallStatus(), InstallProgress());
		return false;
	}	

	result = cellGameContentPermit ( m_ContentInfoDir, m_GameDataDir );
	
	if(result != CELL_OK)
	{
		streamErrorf("fiPsnInstallerDevice::InstallOpenedFiles Failed to permit the gamedata directory (%x)", result);
		m_InstallFailed = true;
		if (m_progressCallback)
			m_progressCallback(InstallStatus(), InstallProgress());
		return false;
	}	

	return true;
}

void fiPsnInstallerDevice::InstallOpenedFiles()
{
	// start the installer thread
	if(InstallStatus() == INSTALL_OK)
		m_Thread = sysIpcCreateThread(&fiPsnInstallerDevice::InstallerThreadStub, this, sysIpcMinThreadStackSize, PRIO_BELOW_NORMAL, "Installer");
}

bool fiPsnInstallerDevice::IsInstalled(fiHandle handle)
{
	return m_Files[(int)handle].m_Installed;
}

bool fiPsnInstallerDevice::IsEverythingInstalled()
{
	// go through list of files and check if they are all installed
	for(atArray<fiInstalledFile>::iterator it = m_Files.begin(); it != m_Files.end(); ++it)
	{
		fiInstalledFile& f = *it;
		if (!f.m_Installed)
			return false;
	}
	return true;
}

bool fiPsnInstallerDevice::FinishInstallation()
{
	streamAssert(m_StartedInstalling);
	sysIpcWaitThreadExit(m_Thread);
	return !m_InstallFailed;
}

void fiPsnInstallerDevice::InstallerThreadStub(void* ptr)
{
	static_cast<fiPsnInstallerDevice*>(ptr)->InstallerThread();
}

namespace rage
{
	extern bool g_RequestSysutilExit;
}

void fiPsnInstallerDevice::InstallerThread()
{
	// install any missing files
	streamDisplayf("INSTALLING..");
	sysTimer timer;
    float nextProgressReport = 0.0f;
	m_InstallFailed = false;
	const fiDevice& localdev = fiDeviceLocal::GetInstance();
	for(atArray<fiInstalledFile>::iterator it = m_Files.begin(); it != m_Files.end(); ++it)
	{
		fiInstalledFile& f = *it;
		if (!f.m_Installed)
		{
			char destname[2048];
			GetInstalledPath(destname, NELEM(destname), f);

			streamDisplayf("INSTALLING %s to %s", f.m_SrcFilename, destname);

			const fiDevice* srcdevice = fiDevice::GetDevice(f.m_SrcFilename, true);
			fiHandle src = srcdevice->Open(f.m_SrcFilename, true);
			streamAssertf(src != fiHandleInvalid, "Error opening %s for read", f.m_SrcFilename);
			localdev.Delete(destname);
			fiHandle dst = localdev.Create(destname);
			streamAssertf(dst != fiHandleInvalid, "Error opening %s for write", destname);

			u32 size = srcdevice->GetFileSize(f.m_SrcFilename);
			u32 copied = 0;
			while (copied < size)
			{
				char buffer[32768];
				int amtRead = Min(sizeof(buffer), size - copied);
				srcdevice->Read(src, buffer, amtRead);
				if (localdev.Write(dst, buffer, amtRead) != amtRead) 
					break;
				copied += amtRead;
				m_BytesCopied += amtRead;

                if (timer.GetTime() > nextProgressReport)
                {
                    nextProgressReport += 1.0f;
                    /*streamDisplayf("Copied %llu/%lluMB (%llu%%)", 
                        m_BytesCopied/(1024*1024), m_InstallSize/(1024*1024), 
                        m_BytesCopied * 100 / m_InstallSize);*/

                    if (m_progressCallback)
					    m_progressCallback(InstallStatus(), InstallProgress());
                }

				if (g_RequestSysutilExit)
					break;
			}
			srcdevice->Close(src);
			localdev.Close(dst);

			if (copied == size)
			{
				// copy file timestamp
				u64 timestamp = srcdevice->GetFileTime(f.m_SrcFilename);
				localdev.SetFileTime(destname, timestamp);
				f.m_Installed = true;
			}
			else
			{
				m_InstallFailed = true;
				break;
			}
		}
	}	
	m_progressCallback(InstallStatus(), 1.0f);
	streamDisplayf("..INSTALLED in %i seconds!", (int)timer.GetTime());

	if (m_InstallFailed)
	{
		streamErrorf("Game Data installation failed!");
	}
}

#if __DEV && 0
static void DumpCellGameDataStatGet(CellGameDataStatGet *get)
{
	streamDisplayf("Dump CellGameDataStatGet in CellGameDataStatCallback--------------------" );
	streamDisplayf("\tget->hddFreeSizeKB 0x%x", get->hddFreeSizeKB);
	streamDisplayf("\tget->isNewData: %d", get->isNewData );
	streamDisplayf("\tget->contentInfoPath : %s", get->contentInfoPath );
	streamDisplayf("\tget->gameDataPath    : %s", get->gameDataPath );
	streamDisplayf("\tget->sizeKB : %d", get->sizeKB);
	streamDisplayf("\tget->sysSizeKB : 0x%x", get->sysSizeKB);
	streamDisplayf("\tget->%lld  %lld  %lld", get->st_atime, get->st_ctime, get->st_mtime );

	streamDisplayf("\n\tPARAM.SFO:TITLE = [%s]", get->getParam.title );

	// titleLang
	for(int i=0 ; i < 16 ; i++)
	{
		streamDisplayf("\tPARAM.SFO:TITLE%02d = [%s]", i, get->getParam.titleLang[i]);
	}

	streamDisplayf("\tPARAM.SFO:TITLEID        = [%s]", get->getParam.titleId );
	streamDisplayf("\tPARAM.SFO:VERSION        = [%s]", get->getParam.dataVersion );
	//streamDisplayf("\tPARAM.SFO:ATTRIBUTE      = [%d]", get->getParam.attribute ); // no longer used with 210 SDK - KS
	//streamDisplayf("\tPARAM.SFO:PARENTAL_LEVEL = [%d]\n", get->getParam.parentalLevel );// no longer used with 210 SDK - KS
}
#endif

void fiPsnInstallerDevice::GetInstalledPath(char* buf, u32 bufsize, const fiInstalledFile& f) const
{
	if (f.m_IsContentInfo)
		safecpy(buf, m_ContentInfoDir, bufsize);
	else
		safecpy(buf, m_GameDataDir, bufsize);

	safecat(buf, f.m_InstallName, bufsize);
}

void fiPsnInstallerDevice::GetInstalledPath(char* buf, u32 bufsize, const char* path) const
{
	safecpy(buf, m_GameDataDir, bufsize);
	safecat(buf, ASSET.FileName(path), bufsize);
}

static bool TimeIsClose(u64 timeA,u64 timeB) 
{
	// Time is in 100ns units.  FAT filesystem is only accurate to two seconds.
	const u64 minTime = 3 * 10000000;
	if (timeA > timeB)
		return timeA - timeB < minTime;
	else
		return timeB - timeA < minTime;
}

u32 fiPsnInstallerDevice::CalculateSizeToInstall()
{
	u32 requiredSpace = 0;
	m_InstallSize = 0;
	const fiDevice& localdev = fiDeviceLocal::GetInstance();
	for(atArray<fiInstalledFile>::iterator it = m_Files.begin(); it != m_Files.end(); ++it)
	{
		fiInstalledFile& f = *it;
		char destname[2048];
		GetInstalledPath(destname, NELEM(destname), f);
		u64 dstsize = localdev.GetFileSize(destname);
		u64 dsttime = localdev.GetFileTime(destname);
		const fiDevice* srcdevice = fiDevice::GetDevice(f.m_SrcFilename, true);
		u64 srcsize = srcdevice->GetFileSize(f.m_SrcFilename);
		u64 srctime = srcdevice->GetFileTime(f.m_SrcFilename);
		if (dstsize != srcsize || !TimeIsClose(dsttime, srctime) || m_ForceReinstall)
		{
			requiredSpace -= (dstsize + 1023) / 1024; // space reclaimed by deleting old file
			requiredSpace += (srcsize + 1023) / 1024;
			m_InstallSize += srcsize;
		}
		else
		{
			streamDisplayf("%s already installed at %s", f.m_SrcFilename, destname);
			f.m_Installed = true;
		}
	}
	return requiredSpace;
}

fiHandle fiPsnInstallerDevice::InstallIconFile(const char* path)
{
	return InstallFile(path, true, "ICON0.PNG", true);
}

void fiPsnInstallerDevice::SetProgressCallback(Functor2<eInstallStatus, float> fn) 
{
    m_progressCallback = fn;
}

void fiPsnInstallerDevice::SetProgress(eInstallStatus status, float progress)
{
	if(m_progressCallback)
		m_progressCallback(status, progress);
}

float fiPsnInstallerDevice::InstallProgress()
{
	if (m_InstallSize == 0)
		return 1.0f;

	return float(m_BytesCopied) / m_InstallSize;
}

eInstallStatus fiPsnInstallerDevice::InstallStatus()
{
	if(m_StartedInstalling == false)
		return INSTALL_NOT_STARTED;
    else if (m_NotEnoughSpace)
        return INSTALL_ERROR_NOT_ENOUGH_SPACE;
    else if (m_InstallFailed)
        return INSTALL_ERROR_INSTALLING;
    return INSTALL_OK;
}


/////////////////////////////////////
//end of psn installer device
/////////////////////////////////////


/////////////////////////////////////
//start of file details
/////////////////////////////////////

//	Is this critical section token still needed since everything that accesses the queue should be guarded 
//	by critical sections at a higher level - FileAccessCriticalSectionToken
static sysCriticalSectionToken	QueueCriticalSectionToken;			// Create the shared token

fiFileDetails::fiFileDetails()
{
	m_SrcFilename[0] = '\0';
	m_InstallName[0] = '\0';
	m_Installed = false;
	m_FileOpenFunctionToUse = FILE_OPEN;
	m_ReadOnly = false;
	m_IsContentInfo = false;
	m_pDevice = NULL;
	m_SeekPos = 0;
	m_Handle = fiHandleInvalid;
}

void fiFileDetails::MakeSpaceForOneNewEntryIfNecessary()
{
	sysCriticalSection cs(QueueCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	if (m_OpenFiles.IsFull())
	{	// out of file handles, close the oldest one
		fiFileDetails* fileToClose = m_OpenFiles.Pop();
		streamAssert(fileToClose->m_Handle != fiHandleInvalid);
//		DEBUGLOG1("fiFileDetails::MakeSpaceForOneNewEntryIfNecessary - popping %s from the queue of open files to make space for a new file", fileToClose->m_SrcFilename);
		fileToClose->m_SeekPos = fileToClose->m_pDevice->Seek(fileToClose->m_Handle, 0, seekCur);
		fileToClose->m_pDevice->Close(fileToClose->m_Handle);
		fileToClose->m_Handle = fiHandleInvalid;
	}
}

void fiFileDetails::PushOnToQueue(fiFileDetails *pFileDetails)
{
	sysCriticalSection cs(QueueCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

//	DEBUGLOG1("fiFileDetails::PushOnToQueue - pushing %s on to the queue of open files", pFileDetails->m_SrcFilename);
	m_OpenFiles.Push(pFileDetails);
}

void fiFileDetails::RemoveFromQueue(fiFileDetails *pFileDetails)
{
	sysCriticalSection cs(QueueCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	int i = 0;
	if (m_OpenFiles.Find(pFileDetails, &i))
	{
//		DEBUGLOG1("fiFileDetails::RemoveFromQueue - deleting %s from the queue of open files", pFileDetails->m_SrcFilename);
		m_OpenFiles.Delete(i);
	}
	else
	{
#if __DEV
		int nOpenFileCount = m_OpenFiles.GetCount();
		streamDebugf1("fiFileDetails::RemoveFromQueue - number of open files = %d\n", nOpenFileCount);
		for (int OpenFileLoop = 0; OpenFileLoop < nOpenFileCount; OpenFileLoop++)
		{
			fiFileDetails *pFileToDisplay = m_OpenFiles[OpenFileLoop];
			streamDebugf3("Open File %d is %s %s\n", OpenFileLoop, pFileToDisplay->m_SrcFilename, pFileToDisplay->m_InstallName);
		}
#endif // __DEV

		streamAssertf(false, "fiFileDetails::RemoveFromQueue - Can't find %s in open file queue", pFileDetails->m_SrcFilename);
	}
}

/////////////////////////////////////
//end of file details
/////////////////////////////////////


/////////////////////////////////////
//start of psn relative device
/////////////////////////////////////

// fiDeviceRelativePsn - device to read from the episodic data folder on the PS3's HDD
fiDeviceRelativePsn::fiDeviceRelativePsn() : fiDeviceRelative()
{
//	DEBUGLOG("Constructor of fiDeviceRelativePsn\n");
	m_nNumberOfFiles = 0;

	for (u32 char_loop = 0; char_loop < NELEM(m_DrmKey.keydata); char_loop++)
	{
		m_DrmKey.keydata[char_loop] = 0;
	}

	//	Is it better to not have this line at all and just let the map re-allocate as it needs to grow?
	m_FilenameMap.Create(SIZE_OF_FILENAME_MAP, false);
}

fiDeviceRelativePsn::~fiDeviceRelativePsn()
{
	m_FilenameMap.Kill();
}

fiHandle fiDeviceRelativePsn::Open(const char *filename,bool readOnly) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	return const_cast<fiDeviceRelativePsn*>(this)->AddToFilesArray(filename, readOnly, FILE_OPEN);
}

fiHandle fiDeviceRelativePsn::OpenBulk(const char *filename,u64&bias) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	bias = 0;
	return const_cast<fiDeviceRelativePsn*>(this)->AddToFilesArray(filename, true, FILE_OPEN_BULK);
}

fiHandle fiDeviceRelativePsn::OpenBulkDrm(const char *filename,u64 &bias,const void *pDrmKey) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	bias = 0;
	eFileOpenFunctionToUse FileOpenFunction = FILE_OPEN_BULK;
	if (pDrmKey)
	{
		const u8 *pCopyFrom = (const u8 *) pDrmKey;
		u8 *pCopyTo = (u8 *) m_DrmKey.keydata;

		u32 number_of_bytes = sizeof(m_DrmKey.keydata);

		for (u32 char_loop = 0; char_loop < number_of_bytes; char_loop++)
		{
			streamAssertf( (pCopyTo[char_loop] == 0) || (pCopyTo[char_loop] == pCopyFrom[char_loop]), "fiDeviceRelativePsn::OpenBulkDrm - DrmKey already has a different value");
			pCopyTo[char_loop] = pCopyFrom[char_loop];
		}
		FileOpenFunction = FILE_OPEN_BULK_DRM;
	}
	return const_cast<fiDeviceRelativePsn*>(this)->AddToFilesArray(filename, true, FileOpenFunction);
}

int fiDeviceRelativePsn::Close(fiHandle handle) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	fiFileDetails& f = m_FileArray[(int)handle];

	if (f.m_Handle != fiHandleInvalid)
	{
		m_pDevice->Close(f.m_Handle);
		f.m_Handle = fiHandleInvalid;

		fiFileDetails::RemoveFromQueue(&f);
	}
	return 0;
}

int fiDeviceRelativePsn::CloseBulk(fiHandle handle) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	fiFileDetails& f = m_FileArray[(int)handle];

	if (f.m_Handle != fiHandleInvalid)
	{
		m_pDevice->CloseBulk(f.m_Handle);
		f.m_Handle = fiHandleInvalid;

		fiFileDetails::RemoveFromQueue(&f);
	}
	return 0;
}

void fiDeviceRelativePsn::ResetForMapDataShutdown()
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	for (u32 file_loop = 0; file_loop < MAX_FILES_ON_RELATIVE_DEVICE; file_loop++)
	{
		fiFileDetails& f = m_FileArray[file_loop];

		if (f.m_Handle != fiHandleInvalid)
		{
			if (f.m_FileOpenFunctionToUse == FILE_OPEN)
			{
				m_pDevice->Close(f.m_Handle);
			}
			else
			{
				m_pDevice->CloseBulk(f.m_Handle);
			}

			f.m_Handle = fiHandleInvalid;

			fiFileDetails::RemoveFromQueue(&f);
		}

		f.m_SeekPos = 0;
	}
}

int fiDeviceRelativePsn::Read(fiHandle handle,void *outBuffer,int bufferSize) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	return m_pDevice->Read(LocalHandle(handle), outBuffer, bufferSize);
}

int fiDeviceRelativePsn::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	return m_pDevice->ReadBulk(LocalHandle(handle), offset, outBuffer, bufferSize);
}

int fiDeviceRelativePsn::Write(fiHandle handle,const void *buffer,int bufferSize) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	return m_pDevice->Write(LocalHandle(handle), buffer, bufferSize);
}

//	WriteBulk doesn't appear to exist in fiDeviceRelative so maybe I don't need to implement it for fiDeviceRelativePsn

int fiDeviceRelativePsn::Seek(fiHandle handle,int offset,fiSeekWhence whence) const
{
	sysCriticalSection cs(FileAccessCriticalSectionToken);		// Create a critical section to control the mutex //will release on destruct

	return m_pDevice->Seek(LocalHandle(handle), offset, whence);
}

// fiDeviceRelativePsn::LocalHandle - every time an episodic file is read, written or seeked for this device, 
//		this function must be called to convert from a local handle to a proper
//		file handle. If the file is not already open then we will have to open it
//		to get a proper file handle. If the queue of open files is full then
//		we will have to close the oldest file on the queue before we can open the new one.
// Parameters - a local file handle which is an integer in the range 0 to m_nNumberOfFiles (the size of m_FileArray)
// Returns - a proper file handle which can be used by the PS3's cellFs... functions
fiHandle fiDeviceRelativePsn::LocalHandle(fiHandle handle) const
{
	if ( ((int)handle < 0) || ((u32)handle >= m_nNumberOfFiles) )
	{
		streamAssertf(false, "fiDeviceRelativePsn::LocalHandle - local handle is out of range");
		return fiHandleInvalid;
	}

	fiFileDetails& f = m_FileArray[(int)handle];

	if (f.m_Handle == fiHandleInvalid)
	{
		fiFileDetails::MakeSpaceForOneNewEntryIfNecessary();

		char path[RAGE_MAX_PATH];
		FixName(path, RAGE_MAX_PATH, f.m_SrcFilename);

		streamAssertf(f.m_pDevice == this->m_pDevice, "fiDeviceRelativePsn::LocalHandle - relative Device should match the previously stored device pointer for this file %s", path);

		if (FILE_OPEN == f.m_FileOpenFunctionToUse)
		{
			f.m_Handle = m_pDevice->Open(path, f.m_ReadOnly);
		}
		else
		{
			const void *pDrmKey = NULL;
			if (FILE_OPEN_BULK_DRM == f.m_FileOpenFunctionToUse)
			{
				pDrmKey = &m_DrmKey;
			}

			u64 bias = 0;	//	What is this for?
			f.m_Handle = m_pDevice->OpenBulkDrm(path, bias, pDrmKey);
		}

		if (f.m_Handle != fiHandleInvalid)
		{
			m_pDevice->Seek(f.m_Handle, f.m_SeekPos, seekSet);
//			DEBUGLOG1("fiDeviceRelativePsn::LocalHandle - pushing %s on to the queue of open files", f.m_SrcFilename);
			fiFileDetails::PushOnToQueue(&f);	//	the queue of open files
		}
		else
		{
			streamAssertf(false, "fiDeviceRelativePsn::LocalHandle - Error opening file %s (%s)", path, f.m_SrcFilename);
		}
	}
	return f.m_Handle;
}

// fiDeviceRelativePsn::AddToFilesArray - called by the Open file functions. Add the details of the file to 
//		the array if it is not already there. A map is used to speed up the checks for the file already being 
//		present in the array. There is also the chance that the file does not exist on the HDD (e.g. 
//		some .srl files for mo-cap cutscenes) so we check if the file exists before adding it to the array.
//		Unfortunately, this requires opening the file so we might have to force a file in the fiFileDetails::m_OpenFiles
//		queue to close so that we don't temporarily exceed the MAX_OPEN_FILES limit.
// Returns - a local file handle which is an integer in the range 0 to m_nNumberOfFiles (the size of m_FileArray)
fiHandle fiDeviceRelativePsn::AddToFilesArray(const char* path, bool readOnly, eFileOpenFunctionToUse OpenFunctionToUse)
{
	fiHandle handle = fiHandleInvalid;
	
	u32 *pFileIndexFromMap = m_FilenameMap.Access(path);
	if (pFileIndexFromMap)
	{	//	The file is already in the array. Just return its array index as its local file handle
		handle = (fiHandle)(*pFileIndexFromMap);
	}
	else
	{	//	The file hasn't already been added to the array

		//	Check that the file actually exists on the HDD

		//	We are about to open a file to check that it exists.
		//	Even though the file is immediately closed again, we'll make sure that there is at least one 
		//	free file handle in the m_OpenFiles queue so that this one extra file is accounted for by MAX_OPEN_FILES.
		//	Note - testHandle is never actually added to the m_OpenFiles queue
		fiFileDetails::MakeSpaceForOneNewEntryIfNecessary();

		char full_path[RAGE_MAX_PATH];
		FixName(full_path, RAGE_MAX_PATH, path);
		fiHandle testHandle = m_pDevice->Open(full_path, readOnly);
		if (testHandle == fiHandleInvalid)
		{	//	Don't add the filename to the array if the file doesn't exist
			streamDebugf1("fiDeviceRelativePsn::AddToFilesArray - %s not found on HDD so not adding to the array\n", full_path);
			return fiHandleInvalid;
		}
		else
		{
			this->m_pDevice->Close(testHandle);
		}

		if (m_nNumberOfFiles < MAX_FILES_ON_RELATIVE_DEVICE)
		{
			handle = (fiHandle)m_nNumberOfFiles;

			const char *installedname = ASSET.FileName(path);

			fiFileDetails& f = m_FileArray[m_nNumberOfFiles];
			safecpy(f.m_SrcFilename, path, NELEM(f.m_SrcFilename));
			safecpy(f.m_InstallName, installedname, NELEM(f.m_InstallName));
			f.m_Handle = fiHandleInvalid;
			f.m_ReadOnly = readOnly;
			f.m_IsContentInfo = false;
			f.m_pDevice = m_pDevice;
			f.m_SeekPos = 0;
			f.m_Installed = false;
			f.m_FileOpenFunctionToUse = OpenFunctionToUse;

			m_FilenameMap.Insert(f.m_SrcFilename, m_nNumberOfFiles);

			m_nNumberOfFiles++;
		}
		else
		{
			streamAssertf(false, "fiDeviceRelativePsn::AddToFilesArray - array is full - MAX_FILES_ON_RELATIVE_DEVICE needs increased");
		}
	}

	return handle;
}


#endif // __PPU
