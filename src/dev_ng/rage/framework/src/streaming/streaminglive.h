//
// streaming/streaminglive.h
//
// Copyright (C) 1999-2009 Rockstar North.  All Rights Reserved. 
//

#ifndef STREAMING_STREAMINGLIVE_H
#define STREAMING_STREAMINGLIVE_H

#include "streamingdefs.h"

#if LIVE_STREAMING

#include "atl/array.h"
#include "data/base.h"
#include "system/bit.h"
#include "system/criticalsection.h"
#include "paging/streamer.h"
#include "system/timer.h"
#include "vector/color32.h"

#include "fwtl/nameregistrar.h"
#include "fwtl/pool.h"
#include "streaming/streamingdefs.h"

namespace rage
{
	class sysIpcThread;
	class sysCriticalSectionToken;

class strStreamingLiveFile
{
public:
	enum
	{
		LIVE_STREAMING_FILE		= BIT(0),
		LIVE_NEEDS_REFRESH		= BIT(1),
		LIVE_STILL_EXISTS		= BIT(2),
		LIVE_IS_LOADING			= BIT(3),
		LIVE_IS_PATCHED			= BIT(4),
		LIVE_ADDED_NEW_STRINDEX	= BIT(5),
	};

	enum
	{
		LIVE_NUMFILES = 250
	};

	bool LinkFile(const char *filename,u32 index);
	bool UnlinkFile();
	
	u64			m_filetime;		

	u32			m_oldhandle;
	strIndex	m_index;

	u32			m_namehash;
	s16			m_module;		
	u16			m_flags;
};


class strStreamingLiveFolder
{
	friend class strStreamingLive;
public:
	void Init(const char* folder, const char* relativePath);
	void Shutdown();

	bool Update(bool enabled);

	void UpdateFile(const fiFindData& findData);
	void DeleteLiveFile(strStreamingLiveFile* pFile);

	void UpdateAllDirtyFiles();

	atHashString& GetFolder(){return m_folder;}
	atHashString GetRelativePath(){return m_folder;}

	strStreamingLiveFile* FindFile(const char* filename);
	strStreamingLiveFile* FindFileByIndex(strIndex index);

private:
	void UpdateDirtyFile(strStreamingLiveFile &file);

	static void	UpdateFileCB(const fiFindData& findData, void* pUserData);

	atArray<strStreamingLiveFile>		m_files;
	fwNameRegistrar						m_subfolders;
	atHashString						m_folder;
	atHashString						m_relativePath;
	s32									m_numlinkedfiles;
};


class strStreamingLive : public datBase
{
public:
	void Init();
	void Shutdown();
	int AddLiveFolder(const char* livefolder, const char* relativePath = "");
	void Update();

	bool GetEnabled() {return m_enabled;}
	void SetEnabled(bool enabled);

	bool GetPatched();

	bool GetIsRunning() const					{ return m_running; }

	int GetFolderCount() const					{ return m_numFolders; }

	const char *GetFolderPath(int folder) const	{ TrapGE((u32) folder, (u32) m_numFolders); return m_folders[folder].m_folder.GetCStr(); }

	void UpdateAllDirtyFiles();

#if __BANK
	void AddBankWidgets();
	void DrawDebugInfo(void (*cb)(Color32 c,const char *fmt,...));
#endif // __BANK

	void OpenLiveFile(strIndex index);
	void CloseLiveFile(strIndex index);
	void RemovedLiveFile(strIndex index);
	bool FileStillExists(strIndex index);

	void ThrottleCritsec();

	void UpdateUnlinkedFiles();

protected:
	strStreamingLiveFile* FindFileWithIndex(strIndex index);
	bool FindFolderAndFileWithIndex(strIndex index, strStreamingLiveFolder*& pFolder, strStreamingLiveFile*& pFile);

	enum
	{
		LIVE_NUMFOLDERS		= 20,
	};

	static void UpdateThreadFunc(void* data);

	strStreamingLiveFolder			m_folders[LIVE_NUMFOLDERS];	
	sysIpcThreadId					m_updateThreadId;
	sysCriticalSectionToken			m_csToken;
	sysTimer						m_lockTimer;
	int								m_numFolders;
	bool							m_enabled;
	bool							m_running;
	bool							m_patched;
};

}	// namespace rage

#endif // LIVE_STREAMING

#endif // INC_STREAMINGLIVE_H

