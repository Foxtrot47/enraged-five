//
// streaming/streamingdefs.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

//#define STR_LOCAL_IDX


#ifndef STREAMING_STREAMINGDEFS_H
#define STREAMING_STREAMINGDEFS_H

#include "atl/pagedpool.h"
#include "data/resource.h"
#include "system/buddyallocator_config.h"
#include "system/endian.h"
#include "system/memory.h"
#include "grprofile/stats.h"
#include "profile/telemetry.h"

namespace rage {

// configuration options 
#define STREAM_STATS					(__STATS && 1)	// Additional stats
#define DEBUG_NAMES						0	// Maintain the names of the objects inside the CModelInfo elements.
#define USE_DUALLAYER					0	// Dual layer cd/dvd
#define TIME_LOADING					0	// print time it takes to convert streamed object
#define TIME_LOADING_MIN				(1.0f)	// min time to report streaming times
#define SANITY_CHECK_REQUESTS			0
#define SANITY_CHECK_LIST				0
#define ONE_STREAMING_HEAP				(!((__PPU || RSG_PC) && FREE_PHYSICAL_RESOURCES))


#define USE_PAGED_POOLS_FOR_STREAMING	0
#define USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS	1

#if USE_PAGED_POOLS_FOR_STREAMING
	#define STREAMING_INFO_MAX_PAGE_COUNT		256			// Total max number of pages that can be allocated
	#define STREAMING_INFO_ELEMENTS_PER_PAGE	8192		// Max number of elements per page - actual number varies per module

	//CompileTimeAssert((STREAMING_INFO_ELEMENTS_PER_PAGE & (STREAMING_INFO_ELEMENTS_PER_PAGE - 1)) == 0);		// This value needs to be a power of 2

	#define STRINDEX_FMT	"x"

#else // USE_PAGED_POOLS_FOR_STREAMING

	#define STRINDEX_FMT	"d"

#endif // USE_PAGED_POOLS_FOR_STREAMING

#define LIVE_STREAMING					(!__FINAL)
#define USE_DEFRAGMENTATION				(ENABLE_KNOWN_REFERENCES && (__CONSOLE || (RSG_PC && !FREE_PHYSICAL_RESOURCES)))

#define STREAMING_TELEMETRY_INTERFACE	(0 && __BANK && USE_TELEMETRY)

// Tally up some more detailed stats - this will cost some memory,
// so DON'T COMMIT with this enabled.
#define STREAMING_DETAILED_STATS		(0 && __BANK)

// request flags
enum { 
	STRFLAG_NONE					=	(0),
	STRFLAG_DONTDELETE 				=	(1<<0),	// Never delete the object, until CanDelete is called
	STRFLAG_FORCE_LOAD				=	(1<<1),	// Keep the request alive until the object has been loaded
	STRFLAG_PRIORITY_LOAD			=	(1<<2),	// Put the request on the priority list, which get loaded before any other objects.
	STRFLAG_LOADSCENE				= 	(1<<3),	// Mark the request as part of a load scene. During a LoadScene call, it doesn't exit until all these have been loaded in a blocking fashion.
	STRFLAG_MISSION_REQUIRED		= 	(1<<4), // Mark the request as being required for a mission - special handling involved
	STRFLAG_CUTSCENE_REQUIRED		=	(1<<5), // Mark the request as being required for a cutscene - special handling involved
	STRFLAG_INTERIOR_REQUIRED		=	(1<<6), // Mark the request as being required for an interior - special handling involved 
	STRFLAG_ZONEDASSET_REQUIRED		=	(1<<7), // Mark the request as being required by the zoned asset manager - special handling involved 
};

// state flags
enum {
	STRFLAG_INTERNAL_LIVE			=	(1<<8),
	STRFLAG_INTERNAL_DUMMY			=	(1<<9), 
	STRFLAG_INTERNAL_COMPRESSED		=	(1<<10),
	STRFLAG_INTERNAL_RESOURCE		=	(1<<11),
	STRFLAG_INTERNAL_DEFRAGGING		=	(1<<12),
	STRFLAG_INTERNAL_DONT_DEFRAG	=	(1<<13),
	STRFLAG_INTERNAL_PLACING		=	(1<<14),
	STRFLAG_INTERNAL_UNUSED_2		=	(1<<15)
};

// Reference kinds
enum strRefKind {
	REF_RENDER		= 0,
	REF_SCRIPT		= 1,
	REF_DEFRAG		= REF_RENDER,
	REF_OTHER		= 2,

	MAX_REF_KINDS
};

enum {
	REF_RENDER_BITS = 12,
	REF_SCRIPT_BITS = 9,
	//REF_DEFRAG_BITS = 2,
	REF_OTHER_BITS = 43,
};

#if !__NO_OUTPUT
extern const char *strRefKindNames[MAX_REF_KINDS];
extern const u8 strRefKindBits[MAX_REF_KINDS];
#endif // !__NO_OUTPUT

// masks
#define STR_DONTDELETE_MASK		(STRFLAG_DONTDELETE|STRFLAG_MISSION_REQUIRED|STRFLAG_CUTSCENE_REQUIRED|STRFLAG_INTERIOR_REQUIRED|STRFLAG_ZONEDASSET_REQUIRED)
#define STR_REQUEST_FLAGS_MASK	(STRFLAG_DONTDELETE|STRFLAG_FORCE_LOAD|STRFLAG_PRIORITY_LOAD|STRFLAG_LOADSCENE|STRFLAG_MISSION_REQUIRED|STRFLAG_CUTSCENE_REQUIRED|STRFLAG_INTERIOR_REQUIRED|STRFLAG_ZONEDASSET_REQUIRED)
#define STR_FORCELOAD_MASK		(STR_DONTDELETE_MASK|STRFLAG_FORCE_LOAD)
#define STR_PERSISTENT_MASK		(STRFLAG_INTERNAL_LIVE|STRFLAG_INTERNAL_DUMMY|STRFLAG_INTERNAL_COMPRESSED|STRFLAG_INTERNAL_RESOURCE|STRFLAG_INTERNAL_DEFRAGGING|STRFLAG_INTERNAL_DONT_DEFRAG)
#define STR_DONTDELETEARCHETYPE_MASK		(STR_DONTDELETE_MASK & ~(STRFLAG_ZONEDASSET_REQUIRED))

// CD sector size
#define SECTOR_SIZE						(2048)

// ====================== Platform Dependent =========================
#define MAX_CONVERT_MS					(8)
#if RSG_ORBIS
#define MAX_PENDING_KB					(32768)
#else
#define MAX_PENDING_KB					(8192)
#endif

// ====================== Platform Dependent =========================

#define MAX_LOAD_PER_FRAME				(4)
#define LOAD_SLOTS_PER_DEVICE			(32)
#define MAX_STREAMING_MODULES			(30)

#define STREAMING_NUMCDIMAGES			10

//Define the maximum number of archives/packs that can be mounted by the game.
#define MAX_ARCHIVES_MOUNTED			(100)

// Description of code module that uses streaming code. 
// Exposed since it contains definitions and defines used on 
// the client side.
static const int STREAMING_MAX_DEPENDENCIES = 100;
static const int STREAMING_INVALID_MODULE = 0xFF;

#define BASE_RAGE_VERSION		191			// accept only files exported with this rage version or above.

struct sysMemDistribution;

#define STRINDEX_TYPE		u32
#define STRINDEX_INVALID	0xffffffff
#define STRLOCALINDEX_INVALID	0xffffffff

CompileTimeAssert((STRINDEX_TYPE)STRINDEX_INVALID == (STRINDEX_TYPE)sysMemAllocator::INVALID_USER_DATA);

// Number of independent schedulers (typically hard disk and optical drive)
#define STREAMING_SCHEDULER_COUNT		(2)

// Number of distinct priority types for streaming (currently "normal" and "priority")
#define STREAMING_PRIORITY_COUNT		(2)

#if USE_PAGED_POOLS_FOR_STREAMING
class strIndex : public atPagedPoolIndex
{
public:
	enum eInvalidIndex { INVALID_INDEX = STRINDEX_INVALID };

	strIndex() : atPagedPoolIndex(atPagedPoolIndex::INVALID_INDEX) {}
	strIndex(const strIndex &o)			{ m_PageAndIndex = o.m_PageAndIndex; }
	strIndex(const atPagedPoolIndex &o)	{ m_PageAndIndex = o.GetRawValue(); }
	strIndex(u32 page, u32 element) : atPagedPoolIndex(page, element) {}
private:
	strIndex(const u32 &value)			{ m_PageAndIndex = value; }
public:

	explicit strIndex(eInvalidIndex) : atPagedPoolIndex(atPagedPoolIndex::INVALID_INDEX) {}

	inline u32 Get() const				{ return m_PageAndIndex; }
	inline bool IsValid() const			{ return !IsInvalid(); }
};

#else // USE_PAGED_POOLS_FOR_STREAMING
class strIndex
{
public:
	enum eInvalidIndex { INVALID_INDEX = STRINDEX_INVALID };

	strIndex() : m_Index(STRINDEX_INVALID) {}
	explicit strIndex(eInvalidIndex) : m_Index(STRINDEX_INVALID) {}

	// strIndex(const strIndex& other)					{ m_Index = other.m_Index; }
	
	
	explicit strIndex(const u32& value) 			{ Assign(m_Index, value); }

	inline STRINDEX_TYPE Get() const { return m_Index; }
	inline void Set(STRINDEX_TYPE i) { m_Index = i; }

	inline bool IsValid() const { return m_Index != STRINDEX_INVALID; }
	inline bool IsInvalid() const { return m_Index == STRINDEX_INVALID; }

	inline void operator =(const STRINDEX_TYPE other)		{ Set(other); }
	inline void operator =(const strIndex& other)			{ m_Index = other.m_Index; }
	inline bool operator ==(const strIndex& other) const	{ return m_Index == other.m_Index; }
	inline bool operator !=(const strIndex& other) const	{ return m_Index != other.m_Index; }

	inline bool operator >(const strIndex& other) const	{ return m_Index > other.m_Index; }
	inline bool operator >=(const strIndex& other) const	{ return m_Index >= other.m_Index; }
	inline bool operator <=(const strIndex& other) const	{ return m_Index <= other.m_Index; }
	inline bool operator <(const strIndex& other) const	{ return m_Index < other.m_Index; }

	// operator u32() const				{ return (u32)m_Index;		}

	// static const strIndex INVALID_INDEX;

private:
	STRINDEX_TYPE m_Index;
};
#endif // USE_PAGED_POOLS_FOR_STREAMING


class strLocalIndex
{
public:
	enum eInvalidIndex { INVALID_INDEX = STRLOCALINDEX_INVALID };

	strLocalIndex() : m_Index((s32)STRLOCALINDEX_INVALID) {}
	explicit strLocalIndex(eInvalidIndex) : m_Index((s32)STRLOCALINDEX_INVALID) {}

	explicit strLocalIndex(const s32& value) 			{ Assign(m_Index, value); }

	inline int Get() const { return m_Index; }
	inline void Set(s32 i) { m_Index = i; }

	inline void Invalidate() { Set(STRLOCALINDEX_INVALID); }
	inline bool IsValid() const { return m_Index != STRLOCALINDEX_INVALID; }
	inline bool IsInvalid() const { return m_Index == STRLOCALINDEX_INVALID; } 

	inline void operator =(const s32 other)		{ Set(other); }
	inline void operator =(const strLocalIndex& other)			{ m_Index = other.m_Index; }
	inline bool operator ==(const strLocalIndex& other) const	{ return m_Index == other.m_Index; }
	inline bool operator ==(const s32& other) const	{ return m_Index == other; }
	inline bool operator !=(const strLocalIndex& other) const	{ return m_Index != other.m_Index; }
	inline bool operator !=(const s32& other) const	{ return m_Index != other; }

	inline bool operator >(const strLocalIndex& other) const	{ return m_Index > other.m_Index; }
	inline bool operator >=(const strLocalIndex& other) const	{ return m_Index >= other.m_Index; }
	inline bool operator <=(const strLocalIndex& other) const	{ return m_Index <= other.m_Index; }
	inline bool operator <(const strLocalIndex& other) const	{ return m_Index < other.m_Index; }

private:
	s32 m_Index;
};

}	// namespace rage


#if STREAMING_TELEMETRY_INTERFACE

#define STR_TELEMETRY_MESSAGE_LOG(...)	do { if (strStreamingEngine::IsTelemetryLogEnabled()) { TELEMETRY_MESSAGE_LOG(__VA_ARGS__); } } while (false)
#define STR_TELEMETRY_MESSAGE_WARNING(...)	do { if (strStreamingEngine::IsTelemetryLogEnabled()) { TELEMETRY_MESSAGE_LOG(__VA_ARGS__); } } while (false)
#define STR_TELEMETRY_MESSAGE_ERROR(...)	do { if (strStreamingEngine::IsTelemetryLogEnabled()) { TELEMETRY_MESSAGE_LOG(__VA_ARGS__); } } while (false)

#else // STREAMING_TELEMETRY_INTERFACE

#define STR_TELEMETRY_MESSAGE_LOG(...)
#define STR_TELEMETRY_MESSAGE_WARNING(...)
#define STR_TELEMETRY_MESSAGE_ERROR(...)

#endif // STREAMING_TELEMETRY_INTERFACE


#endif // STREAMING_STREAMINGDEFS_H
