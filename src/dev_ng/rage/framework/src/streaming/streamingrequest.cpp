//
// streaming/streamingrequest.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "streaming/streamingrequest.h"

#include "atl/string.h"

#include "fwdrawlist/drawlistmgr.h"
#include "fwscene/stores/maptypesstore.h"

#include "entity/archetypemanager.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingmodule.h"


#define ASSIGN(a,b)	a=b;FastAssert(a==(b)) // damn bitfields

namespace rage {

bool strRequest::Request(strLocalIndex requestId, u32 moduleId, u32 flags)
{
	if (m_requestId != requestId || moduleId != m_moduleId)
	{
		Release();

		Assign(m_requestId,requestId);
		ASSIGN(m_moduleId,moduleId);
		ASSIGN(m_flags, flags|STRFLAG_FORCE_LOAD); 
	}

	return HasLoaded();
}

void strRequest::ClearRequiredFlags(u32 flags)
{
	if (m_moduleId != INVALID_MODULE)
	{
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId);
		pModule->ClearRequiredFlag(m_requestId.Get(), flags); 
	}
}

void strRequest::ClearMyRequestFlags(u32 flags)
{
	m_flags &= ~(flags & STR_DONTDELETE_MASK);
}

void strRequest::SetRequiredFlags(u32 flags)
{
	if (m_moduleId != INVALID_MODULE)
	{
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId);
		pModule->SetRequiredFlag(m_requestId.Get(), flags); 
	}
}

void strRequest::Release()
{
	if (m_loaded)
	{
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId);
		pModule->RemoveRef(m_requestId, REF_SCRIPT);
	}
	m_loaded = false;
	m_requestId.Invalidate();
	m_moduleId = INVALID_MODULE;
}

void strRequest::DelayedRelease()
{
	if (HasLoaded())
	{
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId);
		pModule->AddRef(GetRequestId(), REF_RENDER); // Make sure the reference sticks around for a few more frames
		gDrawListMgr->AddRefCountedModuleIndex(GetRequestId().Get(), pModule); 
	}
	Release();
}

bool strRequest::StreamingRemove()
{
	if(m_loaded)
	{
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId);
		pModule->RemoveRef(m_requestId, REF_SCRIPT);
		if(!streamVerifyf(pModule->StreamingRemove(m_requestId),"Failed to remove the streaming object, it is still being used") )
		{
			return false; 
		}
	}
	m_loaded = false;
	m_requestId.Invalidate();
	m_moduleId = INVALID_MODULE;
	return true; 
}

bool strRequest::HasLoaded()
{
	if (m_loaded)
	{
		return true;
	}
	else if (m_requestId.IsValid())
	{
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId);
		if (pModule)
		{
			if (!pModule->HasObjectLoaded(m_requestId))
			{
				pModule->StreamingRequest(m_requestId, m_flags);				
			}
			// The above call to StreamingRequest can set a dummy's state to loaded
			// so we need to check twice
			if (pModule->HasObjectLoaded(m_requestId))
			{
				//if already loaded make sure it respects the flags that have been set
				u8 flags = m_flags & STR_DONTDELETE_MASK; 
				if(flags)
				{
					pModule->SetRequiredFlag(m_requestId.Get(), flags); 
				}

				pModule->AddRef(m_requestId, REF_SCRIPT);
				m_loaded = true;				
			}
		}
	}
	return m_loaded;
}     

strIndex strRequest::GetIndex() const
{ 
	return strStreamingEngine::GetInfo().GetModuleMgr().GetModule(m_moduleId)->GetStreamingIndex(m_requestId); 
}

#if __BANK
const char* strRequest::GetName()
{
	if (IsValid())
	{
		return strStreamingEngine::GetInfo().GetObjectName( GetIndex() );
	}
	return NULL;
}
#endif	//__BANK

// take a single string request and try and break it into type file and model name
void strModelRequest::Request(const char* qualifiedModelName, u32 flags)
{
	if (qualifiedModelName)
	{
		atString qualName(qualifiedModelName);

		s32 splitIndex = qualName.IndexOf(':');

		if (splitIndex > 0)
		{
			atString itypeFile;
			itypeFile.Set(qualName, 0, splitIndex);

			//u32 qualNameSize = qualName.length();
			atString modelName;
			modelName.Set(qualName, splitIndex + 1);

			Request(itypeFile.c_str(), modelName.c_str(), flags);
		}
		else
		{
			Request(NULL, qualName.c_str(), flags);
		}
	}
}

// take a type file name and a model name and convert it into a low level request
void strModelRequest::Request(const char* pTypeFile, const char* pModelName, u32 flags)
{
	streamAssert(pModelName);

	strLocalIndex itypSlotIdx;

	if (pTypeFile)
	{
		u32 typeFileHash = atStringHash(pTypeFile);
		itypSlotIdx = g_MapTypesStore.FindSlotFromHashKey(typeFileHash);
		streamAssertf( itypSlotIdx.IsValid(), "strModelRequest for (%s:%s) contains an unrecognised .ityp file", pTypeFile, pModelName);
	}

	streamAssert(pModelName);

	if (pModelName)
	{
		atHashString modelName(pModelName);
		Request(itypSlotIdx, modelName, flags);
	}
}

// issue request for a given ityp file and request assets for the specified model
void strModelRequest::Request(strLocalIndex itypSlotIdx, atHashString modelName, u32 flags)
{
	m_itypSlotIdx = itypSlotIdx;
	m_modelName = modelName;
	m_flags = flags;

	if ( m_itypSlotIdx.IsValid() )
	{
		m_typeFileRequest.Request(itypSlotIdx, g_MapTypesStore.GetStreamingModuleId(), flags);
	} else 
	{
		u32 archetypeStreamSlotIdx = fwArchetypeManager::GetArchetypeIndexFromHashKey(modelName.GetHash());
		streamAssertf(archetypeStreamSlotIdx < fwModelId::MI_INVALID, "Invalid strModelRequest - model :%s is not found", modelName.GetCStr());
		if (archetypeStreamSlotIdx < fwModelId::MI_INVALID)
		{
			m_archetypeAssetsRequest.Request(strLocalIndex(archetypeStreamSlotIdx), fwArchetypeManager::GetStreamingModuleId(), m_flags);
		}
	}
}

bool strModelRequest::HasLoaded()
{
	// has the model loaded yet?
	if (m_archetypeAssetsRequest.IsValid())
	{
		if (m_archetypeAssetsRequest.HasLoaded())
		{
			return(true);
		} else 
		{
			return(false);
		}
	}

	// if not, it might be because we still need to loaded the .ityp file
	if (m_typeFileRequest.IsValid() && m_typeFileRequest.HasLoaded())
	{
		u32 archetypeStreamSlotIdx = fwArchetypeManager::GetArchetypeIndexFromHashKey(m_modelName.GetHash());
		streamAssertf(archetypeStreamSlotIdx < fwModelId::MI_INVALID, "Invalid strModelRequest: model (%s) is not found in file (%s)",  m_modelName.GetCStr(), g_MapTypesStore.GetName(strLocalIndex(m_itypSlotIdx)));

		if (archetypeStreamSlotIdx == fwModelId::MI_INVALID)
		{
			Release();
			return(false);
		} else
		{
			streamAssertf(!m_bItypReffed, "m_bItypeReffed should not be true already?");
			g_MapTypesStore.AddRef(m_itypSlotIdx, REF_OTHER);
			m_bItypReffed = true;
			m_archetypeAssetsRequest.Request(strLocalIndex(archetypeStreamSlotIdx), fwArchetypeManager::GetStreamingModuleId(), m_flags);
			return m_archetypeAssetsRequest.IsValid() && m_archetypeAssetsRequest.HasLoaded();
		}
	}

	// waiting...
	return(false);

}

bool strModelRequest::IsValid()
{
	if (m_typeFileRequest.IsValid() || m_archetypeAssetsRequest.IsValid())
	{
		return(true);
	}

	return(false);
}

void strModelRequest::Release()
{
	// free the type request
	if (m_typeFileRequest.IsValid())
	{
		m_typeFileRequest.Release();
	}
	
	// free the model request
	if (m_archetypeAssetsRequest.IsValid())
	{
		m_archetypeAssetsRequest.Release();
	}

	if (m_bItypReffed)
	{
		gDrawListMgr->AddTypeFileReference( (u32) m_itypSlotIdx.Get() );		// add a temp reference until all uses are flushed through GFX
		g_MapTypesStore.RemoveRef(m_itypSlotIdx, REF_OTHER);
		m_bItypReffed = false;
	}

	m_itypSlotIdx.Invalidate();
}

void strModelRequest::ClearRequiredFlags(u32 flags)
{
	m_typeFileRequest.ClearRequiredFlags(flags); 
	m_archetypeAssetsRequest.ClearRequiredFlags(flags);
}

void strModelRequest::SetRequiredFlags(u32 flags)
{
	m_typeFileRequest.SetRequiredFlags(flags); 
	m_archetypeAssetsRequest.SetRequiredFlags(flags);
}

strModelRequest::strModelRequest(const strModelRequest& that)
: m_itypSlotIdx(that.m_itypSlotIdx)
, m_modelName(that.m_modelName)
, m_flags(that.m_flags)
, m_bItypReffed(that.m_bItypReffed)
, m_typeFileRequest(that.m_typeFileRequest)
, m_archetypeAssetsRequest(that.m_archetypeAssetsRequest)
{
	if(m_bItypReffed)
	{
		g_MapTypesStore.AddRef(m_itypSlotIdx, REF_OTHER);
	}
}

strModelRequest& strModelRequest::operator=(const strModelRequest& that)
{
	// Store any old stuff temporarily
	strLocalIndex oldSlotIdx = m_itypSlotIdx;
	bool oldTypeReffed = m_bItypReffed;

	// Copy across...
	m_itypSlotIdx = that.m_itypSlotIdx;
	m_modelName = that.m_modelName;
	m_flags = that.m_flags;
	m_bItypReffed = that.m_bItypReffed;
	m_typeFileRequest = that.m_typeFileRequest;
	m_archetypeAssetsRequest = that.m_archetypeAssetsRequest;

	if(m_bItypReffed)
	{
		g_MapTypesStore.AddRef(m_itypSlotIdx, REF_OTHER);
	}

	// Remove ref on the old one if we need to
	if(oldTypeReffed)
	{
		g_MapTypesStore.RemoveRef(oldSlotIdx, REF_OTHER);
	}

	return *this;
}


}	// namespace rage
