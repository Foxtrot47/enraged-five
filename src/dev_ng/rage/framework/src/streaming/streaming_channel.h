// 
// streaming/streaming_channel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

#ifndef STREAMING_STREAMING_CHANNEL_H 
#define STREAMING_STREAMING_CHANNEL_H 

#include "diag/channel.h"

RAGE_DECLARE_CHANNEL(streaming)

#define streamAssert(cond)						RAGE_ASSERT(streaming,cond)
#define streamAssertf(cond,fmt,...)				RAGE_ASSERTF(streaming,cond,fmt,##__VA_ARGS__)
#define streamFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(streaming,cond,fmt,##__VA_ARGS__)
#define streamVerify(cond)						RAGE_VERIFY(streaming,cond)
#define streamVerifyf(cond,fmt,...)				RAGE_VERIFYF(streaming,cond,fmt,##__VA_ARGS__)
#define streamErrorf(fmt,...)					RAGE_ERRORF(streaming,fmt,##__VA_ARGS__)
#define streamWarningf(fmt,...)					RAGE_WARNINGF(streaming,fmt,##__VA_ARGS__)
#define streamDisplayf(fmt,...)					RAGE_DISPLAYF(streaming,fmt,##__VA_ARGS__)
#define streamDebugf1(fmt,...)					RAGE_DEBUGF1(streaming,fmt,##__VA_ARGS__)
#define streamDebugf2(fmt,...)					RAGE_DEBUGF2(streaming,fmt,##__VA_ARGS__)
#define streamDebugf3(fmt,...)					RAGE_DEBUGF3(streaming,fmt,##__VA_ARGS__)
#define streamLogf(severity,fmt,...)			RAGE_LOGF(streaming,severity,fmt,##__VA_ARGS__)


RAGE_DECLARE_SUBCHANNEL(streaming, asset)

#define streamAssetAssert(cond)						RAGE_ASSERT(streaming_asset,cond)
#define streamAssetAssertf(cond,fmt,...)			RAGE_ASSERTF(streaming_asset,cond,fmt,##__VA_ARGS__)
#define streamAssetFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(streaming_asset,cond,fmt,##__VA_ARGS__)
#define streamAssetVerify(cond)						RAGE_VERIFY(streaming_asset,cond)
#define streamAssetVerifyf(cond,fmt,...)			RAGE_VERIFYF(streaming_asset,cond,fmt,##__VA_ARGS__)
#define streamAssetErrorf(fmt,...)					RAGE_ERRORF(streaming_asset,fmt,##__VA_ARGS__)
#define streamAssetWarningf(fmt,...)				RAGE_WARNINGF(streaming_asset,fmt,##__VA_ARGS__)
#define streamAssetDisplayf(fmt,...)				RAGE_DISPLAYF(streaming_asset,fmt,##__VA_ARGS__)
#define streamAssetDebugf1(fmt,...)					RAGE_DEBUGF1(streaming_asset,fmt,##__VA_ARGS__)
#define streamAssetDebugf2(fmt,...)					RAGE_DEBUGF2(streaming_asset,fmt,##__VA_ARGS__)
#define streamAssetDebugf3(fmt,...)					RAGE_DEBUGF3(streaming_asset,fmt,##__VA_ARGS__)
#define streamAssetLogf(severity,fmt,...)			RAGE_LOGF(streaming_asset,severity,fmt,##__VA_ARGS__)

RAGE_DECLARE_SUBCHANNEL(streaming, srl)

#define streamSRLAssert(cond)					RAGE_ASSERT(streaming_srl,cond)
#define streamSRLAssertf(cond,fmt,...)			RAGE_ASSERTF(streaming_srl,cond,fmt,##__VA_ARGS__)
#define streamSRLFatalAssertf(cond,fmt,...)		RAGE_FATALASSERTF(streaming_srl,cond,fmt,##__VA_ARGS__)
#define streamSRLVerify(cond)					RAGE_VERIFY(streaming_srl,cond)
#define streamSRLVerifyf(cond,fmt,...)			RAGE_VERIFYF(streaming_srl,cond,fmt,##__VA_ARGS__)
#define streamSRLErrorf(fmt,...)				RAGE_ERRORF(streaming_srl,fmt,##__VA_ARGS__)
#define streamSRLWarningf(fmt,...)				RAGE_WARNINGF(streaming_srl,fmt,##__VA_ARGS__)
#define streamSRLDisplayf(fmt,...)				RAGE_DISPLAYF(streaming_srl,fmt,##__VA_ARGS__)
#define streamSRLDebugf1(fmt,...)				RAGE_DEBUGF1(streaming_srl,fmt,##__VA_ARGS__)
#define streamSRLDebugf2(fmt,...)				RAGE_DEBUGF2(streaming_srl,fmt,##__VA_ARGS__)
#define streamSRLDebugf3(fmt,...)				RAGE_DEBUGF3(streaming_srl,fmt,##__VA_ARGS__)
#define streamSRLLogf(severity,fmt,...)			RAGE_LOGF(streaming_srl,severity,fmt,##__VA_ARGS__)

#define STREAMING_OPTIMISATIONS_OFF	0

#if STREAMING_OPTIMISATIONS_OFF
#define STREAMING_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define STREAMING_OPTIMISATIONS()
#endif	//STREAMING_OPTIMISATIONS_OFF

#endif // STREAMING_STREAMING_CHANNEL_H
