//
// streaming/streamingmodule.h
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

#ifndef STREAMING_STREAMINGMODULE_H_
#define STREAMING_STREAMINGMODULE_H_

// --- Include Files ------------------------------------------------------------

#include "atl/array.h"
#include "atl/string.h"
#include "atl/hashstring.h"
#include "data/base.h"
#include "streaming/streamingengine.h"
#include "streaming/streaminginfo.h"

namespace rage {

// --- Defines ------------------------------------------------------------------
#define TRACK_PLACE_STATS	(__BANK)
#if TRACK_PLACE_STATS	
#define TRACK_PLACE_STATS_ONLY(x)	x
#else
#define TRACK_PLACE_STATS_ONLY(x)	
#endif // TRACK_PLACE_STATS	


// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

#define USE_HASH_ONLY_FOR_OBJECT_NAMES		(__FINAL || __NO_OUTPUT)

#if USE_HASH_ONLY_FOR_OBJECT_NAMES
typedef atHashValue		strStreamingObjectName;
#else // USE_HASH_ONLY_FOR_OBJECT_NAMES
typedef atHashString	strStreamingObjectName;
#endif // USE_HASH_ONLY_FOR_OBJECT_NAMES

typedef atFinalHashString	strStreamingObjectNameString;

class pgBase;

//
// PURPOSE
//	This is the base class for defining how a module interacts with the streaming system
// NOTES
//	To use the streaming system a module has to create a class that inherits from this strStreamingModule. The module should
//	override the relevant functions. Basic integration should override the Register, Remove and Load or 
//	PlaceResource depending on if the objects being streamed are resources. To add memory defragmenting support you need to  
//	override GetPtr, Defragment, Exchange. To add support for referencing you need to override AddRef, RemoveRef, GetNumRefs,
//	and GetDependencies. 
class strStreamingModule: public datBase
{
	friend class strStreamingModuleMgr;

public:
	// PURPOSE:	Constructor
	// PARAMS:	pModuleName - name of module
	//			size - number of streaming info entries required by this module
	//			requiresTempMemory - whether this system requires temporary memory when loading objects. In general resources
	//				don't and all other files do
	//			canDefragment - can we defragment objects in this module
	//			rscVersion - version of resources loaded by this module
	strStreamingModule(
		const char *pModuleName,
		int moduleTypeIndex,
		int size,
		bool requiresTempMemory,
		bool canDefragment,
		int rscVersion,
		u32 elementsPerPage /*= STREAMING_INFO_ELEMENTS_PER_PAGE*/
		) : m_size(size),
		m_moduleTypeIndex(moduleTypeIndex),
		m_ModuleName(pModuleName),
		m_RequiresTempMemory(requiresTempMemory),
		m_canDefragment(canDefragment),
		m_usesExtraMemory(false),
		m_expectedRscVersionNumber(rscVersion)
#if __BANK
		, m_expectedVirtualSize(0)
		, m_expectedPhysicalSize(0)
		, m_loadedCount(0)
#endif 
#if STREAM_STATS
		, m_DataStreamed(0)
		, m_DataCanceled(0)
		, m_FilesLoaded(0)
		, m_FilesCanceled(0)
#endif // STREAM_STATS

#if USE_PAGED_POOLS_FOR_STREAMING
		, m_used(0)
		, m_IndexWatermark(0)
#else // USE_PAGED_POOLS_FOR_STREAMING
		, m_indexOffset(strIndex::INVALID_INDEX)
#endif // USE_PAGED_POOLS_FOR_STREAMING
	{
#if TRACK_PLACE_STATS
			ResetPlaceStats();
#endif // TRACK_PLACE_STATS

#if USE_PAGED_POOLS_FOR_STREAMING
			Assert(elementsPerPage >= 128);						// Not efficient to use a smaller size, they're using the buddy heap
			m_ElementsPerPage = elementsPerPage;
			m_MaxLimit = ~0U;
#else // USE_PAGED_POOLS_FOR_STREAMING
			(void)elementsPerPage;
#endif // USE_PAGED_POOLS_FOR_STREAMING
	}

	virtual ~strStreamingModule() {}

	// PURPOSE:	return name of module
	// RETURNS:	name of module
	const char* GetModuleName() const {return m_ModuleName;}
#if !__NO_OUTPUT
	// PURPOSE:	return name of object at this slot
	// PARAMS:	index - index of object
	// RETURNS: name of object

	virtual const char* GetName(strLocalIndex UNUSED_PARAM(index)) const { return m_ModuleName;}
#endif
	// PURPOSE:	register an object of this name with the module.
	// PARAMS:	pName - name of object
	// RETURNS:	index of object. -1 if registration is unsuccessful
	// NOTES:	When a packfile is registered with the streaming system it goes through every file in the packfile, works 
	//			out which streaming module it belongs to and then calls Register() on that 
	//			streaming module with the filename minus the extension
	virtual strLocalIndex Register(const char* name) = 0;

	// PURPOSE:	find an object of this name within the module (if that name has already been registered).
	// PARAMS:	pName - name of object
	// RETURNS:	index of object initially returned by Register. -1 if object is not found
	// NOTES:	This is required because ::Register (should, but) doesn't always check for duplicates before registering a named object
	virtual strLocalIndex FindSlot(const char* name) const = 0;

	// PURPOSE:	Remove object from memory
	// PARAMS:	index - index of object to remove
	virtual void Remove(strLocalIndex UNUSED_PARAM(index)) {}

	// PURPOSE:	Free up a slot entirely. This slot will be considered invalid and can be re-registered with a new object.
	// PARAMS:	index - index of object to unregister
	virtual void RemoveSlot(strLocalIndex UNUSED_PARAM(index)) { Assert(false); }

	// PURPOSE:	Load object from memory and store in module. This is called for non resources
	// PARAMS:	index - index of object being streamed
	//			object - pointer to buffer in memory
	//			size - size of buffer in memory
	// RETURNS:	Was load successful
	virtual bool Load(strLocalIndex UNUSED_PARAM(index), void* UNUSED_PARAM(object), int UNUSED_PARAM(size)) {return false;}

	// PURPOSE:	Place resource using resource system and store in module
	// PARAMS:	index - index of object being streamed
	//			map - info about memory used by resource. This is used in Place functions
	//			header - more info used in Place functions
	virtual void PlaceResource(strLocalIndex UNUSED_PARAM(index), datResourceMap& UNUSED_PARAM(map), datResourceInfo& UNUSED_PARAM(header)) {}

	// PURPOSE: Set the resource and finalize the initialization.
	virtual void SetResource(strLocalIndex UNUSED_PARAM(index), datResourceMap& UNUSED_PARAM(map)) {}

	// PURPOSE:	return void pointer to object in memory. This is used by defrag
	// PARAMS:	index - index of object 
	// RETURNS:	pointer to object
	virtual void* GetPtr(strLocalIndex UNUSED_PARAM(index)) {return NULL;}

	// PURPOSE:	return void pointer to data used by object. This is used by defrag
	// PARAMS:	index - index of object 
	// RETURNS:	pointer to loaded data (if any)
	virtual void* GetDataPtr(strLocalIndex index) {return GetPtr(index);}

	// PURPOSE:	Function called to repoint all the pointers in a resource once it has been moved by defrag
	// PARAMS:	index - index of object 
	//			map - info about resource once memory has moved
	//			flush - return whether render needs to be completed before executing Exchange()
	// RETURNS:	pointer to new object
	virtual void* Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& UNUSED_PARAM(map), bool& UNUSED_PARAM(flush)) {return NULL;}

	// PURPOSE:	Callback when defragment completes on the object
	// PARAMS:	index - index of object 
	virtual void DefragmentComplete(strLocalIndex UNUSED_PARAM(index)) {}

	// PURPOSE:	Callback when defragment begins on the object
	// PARAMS:	index - index of object 
	virtual void DefragmentPreprocess(strLocalIndex UNUSED_PARAM(index)) {}

	// PURPOSE:	Return the pgBase that represents this object, if it is a resource.
	// PARAMS:	index - index of object 
	// RETURNS:	The pgBase pointer to the object if it is streamed in, and if it is a resource, NULL otherwise.
	virtual pgBase *GetResource(strLocalIndex UNUSED_PARAM(index)) const { return NULL; }

	virtual void GetModelMapTypeIndex(strLocalIndex UNUSED_PARAM(slotIndex), strIndex& UNUSED_PARAM(retIndex)) const { }

	virtual bool ModifyHierarchyStatus(strLocalIndex UNUSED_PARAM(index), eHierarchyModType UNUSED_PARAM(modType)) { return false; }

	s32 GetModuleTypeIndex() const { return m_moduleTypeIndex; }

#if USE_PAGED_POOLS_FOR_STREAMING
	// PURPOSE: Called whenever the streaming system allocates a new page of streaming infos for this module
	virtual void RegisterPagedPool(void *data, u32 page, u32 elementCount, strStreamingInfoPageHeader &header, size_t *memUsage);

	// PURPOSE: Called whenever the streaming system frees a page of streaming infos for this module
	virtual void FreePagedPool(u32 page, u32 elementCount, strStreamingInfoPageHeader &header, size_t *memUsage);

	// PURPOSE: Figure out how much memory a page costs us for module-specific data (not counting the strStreamingInfo)
	virtual size_t ComputePageMemUsage(u32 /*elementCount*/) const	{ return 0; }

	virtual size_t GetModuleMemoryRequirement(u32 /*elementCount*/) const	{ return 0; }

	// PURPOSE: Reserve a new streamable in this module, and create a strStreamableInfo for it in the master table.
	s32 AllocateNewStreamable();

	// PURPOSE: Reserve a new streamable in this module with a specific object index, and create a strStreamableInfo
	// for it in the master table.
	s32 AllocateSlot(s32 objIndex);

	// PURPOSE: Specify a soft limit for elements - we'll assert if we exceed it.
	void SetMaxLimit(u32 maxLimit)							{ m_MaxLimit = maxLimit; }

	// PURPOSE: Return the number of elements per page for this module
	u32 GetElementsPerPage() const							{ return m_ElementsPerPage; }
	void SetElementsPerPage(u32 elementsPerPage)			{ m_ElementsPerPage = elementsPerPage; }

	// PURPOSE: Return true if we have an element with this index. This is typically used for iterating, but not very efficient.
	// In the future, we'll have an iterator.
	bool IsAllocated(s32 objIndex) const;
#else // USE_PAGED_POOLS_FOR_STREAMING
	bool IsAllocated(s32 objIndex) const					{ return (u32) objIndex < (u32) m_size; }
#endif // USE_PAGED_POOLS_FOR_STREAMING


	// PURPOSE:	Add reference to object so it cannot be deleted by streaming code
	// PARAMS:	index - index of object
	virtual void AddRef(strLocalIndex UNUSED_PARAM(index), strRefKind UNUSED_PARAM(strRefKind)) {}
	// PURPOSE:	Remove reference from object
	// PARAMS:	index - index of object
	virtual void RemoveRef(strLocalIndex UNUSED_PARAM(index), strRefKind UNUSED_PARAM(strRefKind)) {}
	// PURPOSE:	Reset all references from object
	// PARAMS:	index - index of object
	virtual void ResetAllRefs(strLocalIndex UNUSED_PARAM(index)) {}
	// PURPOSE:	Get number of references to an object
	// PARAMS:	index - index of object
	virtual int GetNumRefs(strLocalIndex UNUSED_PARAM(index)) const {return 1;}
	// PURPOSE: Create a debug string with the ref count (including extra information if available)
	virtual const char *GetRefCountString(strLocalIndex index, char *buffer, size_t bufferLen) const;

	// PURPOSE:	Return the streaming indices of all the objects that are required to be resident in memory before I can
	//			stream this object in. eg Drawables need textures resident
	// PARAMS:	index - index of object
	//			pIndices - array to put indices into
	//			indexArraySize - size of the array specified in pIndices, in counts of strIndex
	// RETURNS:	Number of indices in array
	virtual int GetDependencies(strLocalIndex UNUSED_PARAM(index), strIndex *UNUSED_PARAM(pIndices), int UNUSED_PARAM(indexArraySize)) const {return 0;} 

	// PURPOSE: Describe some additional information about the object. 
	virtual void PrintExtraInfo(strLocalIndex UNUSED_PARAM(index), char* UNUSED_PARAM(extraInfo), size_t UNUSED_PARAM(maxSize)) const {} 

	// PURPOSE: Returns the amount of virtual memory this object actually uses when resident, which
	// is the normal virtual size (UNLESS this object is set to use temp memory), plus its extra virtual
	// memory.
	// PARAMS:	index - index of object
	//          includeTempMemory - include the temporary memory (which is freed up after placement)
	size_t GetVirtualMemoryOfLoadedObj(strLocalIndex index, bool includeTempMemory) const;

	// PURPOSE: Returns the amount of physical memory this object actually uses when resident, which
	// is the normal physical size (UNLESS this object is set to use temp memory), plus its extra physical
	// memory.
	// PARAMS:	index - index of object
	//          includeTempMemory - include the temporary memory (which is freed up after placement)
	size_t GetPhysicalMemoryOfLoadedObj(strLocalIndex index, bool includeTempMemory) const;

	// PURPOSE: Add some extra allocations to the streaming request, beyond what we normally need for streaming the file
	// PARAMS:	index - index of object
	//			map - the resource map to modify to include additional memory.
	//			maxAllocs - the maximum number of allocations (virtual and physical combined) that can be made
	virtual void RequestExtraMemory(strLocalIndex UNUSED_PARAM(index), datResourceMap& UNUSED_PARAM(map), int UNUSED_PARAM(maxAllocs)) {}

	// PURPOSE: Called once the streaming request is complete, and contains all the extra allocations that were made
	// PARAMS:	index - index of object
	//			map - the resource map to modify to include additional memory.
	// NOTES: This is called _after_ Load or PlaceObject, but before any memory would get deleted
	virtual void ReceiveExtraMemory(strLocalIndex UNUSED_PARAM(index), const datResourceMap& UNUSED_PARAM(map)) {}

	// PURPOSE: Return the amount of virtual memory that was given to an object via ReceiveExtraMemory().
	// PARAMS: index - index of object
	virtual size_t GetExtraVirtualMemory(strLocalIndex UNUSED_PARAM(index)) const	{ return 0; }

	// PURPOSE: Return the amount of physical memory that was given to an object via ReceiveExtraMemory().
	// PARAMS: index - index of object
	virtual size_t GetExtraPhysicalMemory(strLocalIndex UNUSED_PARAM(index)) const { return 0; }

	// PURPOSE: Indicate that this resource is dirty (usually because it has been
	// replaced by the preview folder), and any cached information about it
	// will need to be refetched.
	// PARAMS: index - index of object
#if !__FINAL
	virtual void MarkDirty(strLocalIndex UNUSED_PARAM(index)) {}
#endif // !__FINAL

	// --- access functions ---------------------------------------------------------------------

	// PURPOSE:	Return number of streaming entries required by this module
#if USE_PAGED_POOLS_FOR_STREAMING
	int GetCount() const {return m_used;}
	int GetMaxSize() const {return m_size;}
#else // USE_PAGED_POOLS_FOR_STREAMING
	int GetCount() const {return m_size;}
	int GetMaxSize() const {return m_size;}
#endif // USE_PAGED_POOLS_FOR_STREAMING

	// PURPOSE:	Return if we can defrag this module
	bool CanDefragment() const { return m_canDefragment;}

	// PURPOSE: Return if defrag copy is blocked
	virtual bool IsDefragmentCopyBlocked() const { return false; }

	// PURPOSE:	Return if we require a temp memory buffer while loading objects for this module
	virtual bool RequiresTempMemory(strLocalIndex UNUSED_PARAM(objIndex)) const { return m_RequiresTempMemory;}

	// PURPOSE: Return if we use extra memory (i.e. if the streaming system should call RequestExtraMemory and GetExtraMemory)
	bool UsesExtraMemory() const { return m_usesExtraMemory; }

	// PURPOSE: Return the expected resource version for objects loaded by this module
	int GetExpectedRscVersion() const { return m_expectedRscVersionNumber;}

	// PURPOSE: Get an id for this module that can be used by other streaming api's
	int GetStreamingModuleId() const { return m_moduleId;}

	// PURPOSE: Get a list of all dependencies of an object and all of their dependencies. The object itself is in the list as well.
	void GetObjectAndDependencies(strIndex index, atArray<strIndex>& allDeps, const strIndex* ignoreList, s32 numIgnores) const;

#if ENABLE_DEBUG_HEAP
	// PURPOSE: Get a custom hash map of all dependencies of an object and all of their dependencies. The object itself is in the map as well.
	void GetObjectAndDependencies(strIndex index, ObjectDependenciesMap& allDepsMap, atArray<strIndex>& allDeps) const;
#endif

	// PURPOSE: Indicate whether or not a resource in this module can be placed asynchronously in a separate thread.
	virtual bool CanPlaceAsynchronously( strLocalIndex UNUSED_PARAM(objIndex) ) const { return false; }

	// PURPOSE: If CanPlaceAsynchronously returns true, this function will get called to begin placement. 
	// The function should then queue up work on another thread to begin the placement, and that thread needs to set
	// file.IsPlaced to true to signal that the load is finished
	virtual void PlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex), strStreamingLoader::StreamingFile& UNUSED_PARAM(file), datResourceInfo& UNUSED_PARAM(info)) {}

	// --- interface functions to streaming -----------------------------------------------------

	bool StreamingFull() const;
	// PURPOSE:	Request streaming loads this object.
	// PARAMS:	index - index of object 
	//			flags - flags sent to streaming system. 
	//				STRFLAG_DONTDELETE - streaming can't delete this when looking for memory			
	//				STRFLAG_FORCE_LOAD - every frame streaming requests are purged, ensure this one isn't.
	//				STRFLAG_PRIORITY_LOAD - this is a priority. stream before everything else			
	//				STRFLAG_LOADSCENE - this request came from a load scene. This is used to ensure this isn't deleted in the same loadscene it was loaded in
	//				STRFLAG_MISSION_REQUIRED - This is required by a mission don't delete it
	//				STRFLAG_CUTSCENE_REQUIRED - This is requried by a cutscene don't delete it		
	//				STRFLAG_INTERIOR_REQUIRED - This is required by the interior you are in don't delete it
	//				STRFLAG_DEPENDENCY - This is in memory because another streamed required it
	// RETURNS:	whether request was a success
	bool StreamingRequest(strLocalIndex index, s32 flags=0);

	// PURPOSE:	Request streaming loads this object, then wait for streaming to load it.
	// PARAMS:	index - index of object 
	//			flags - same as above
	// RETURNS:	whether request was a success
	bool StreamingBlockingLoad(strLocalIndex index, s32 flags);

	// PURPOSE:	Remove an object from memory
	// PARAMS:	index - index of object
	// RETURNS: whether remove was a success. You cannot remove objects that aren't in memory or objects that are required by an object about to be loaded
	bool StreamingRemove(strLocalIndex index);

	// PURPOSE: Return if object is in the request list
	// PARAMS:	index - index of object
	// RETURNS:	is object in request list
	bool IsObjectRequested(strLocalIndex index) const;
	// PURPOSE: Return if object is in the process of being streamed in
	// PARAMS:	index - index of object
	// RETURNS:	is object in process of streaming
	bool IsObjectLoading(strLocalIndex index) const;
	// PURPOSE: Return if object is loaded
	// PARAMS:	index - index of object
	// RETURNS:	is object in loaded
	bool HasObjectLoaded(strLocalIndex index) const;

	// PURPOSE: Return if object has been registered as existing in a packfile
	// PARAMS: index - index of object
	// RETURNS: is in pack file
	bool IsObjectInImage(strLocalIndex index) const;

	// PURPOSE: Returns if object has been flagged as new ie it is has changed since the last time you loaded the game. To
	//			use this you need to be saying whether pack files are new using the m_bNew flag on strStreamingFile
	// PARAMS:	index - index of object
	// RETURNS: is object new
	bool IsObjectNew(strLocalIndex index) const;

	// PURPOSE: Returns all the streaming flags set on an object.
	// PARAMS:	index - index of object
	// RETURNS: streaming flags
	// NOTES:	These flags are defined in streamingdefs.h
	int GetStreamingFlags(strLocalIndex index) const;

	// PURPOSE: Return the handle the streaming system uses to recognise this object
	// PARAMS:	index - index of object
	// RETURNS: streaming handle
	strIndex GetStreamingIndex(strLocalIndex index) const;

	// PURPOSE: Return the index the module uses to recognise this object
	// PARAMS:	index - streaming handle of object
	// RETURNS: index of object
	strLocalIndex GetObjectIndex(strIndex index) const;

	// PURPOSE:	Return streaming info class associated with this object
	// PARAMS:	index - index of object
	// RETURNS:	streaming info for object
	strStreamingInfo* GetStreamingInfo(strLocalIndex index);
	const strStreamingInfo* GetStreamingInfo(strLocalIndex index) const;

	// PURPOSE:	Set flags that require an object to be resident
	// PARAMS:	index - index of object
	//			flags - required flags
	// NOTES:	Current required flags are STRFLAG_DONTDELETE, STRFLAG_MISSION_REQUIRED, STRFLAG_CUTSCENE_REQUIRED and
	//			STRFLAG_INTERIOR_REQUIRED
	void SetRequiredFlag(int index, int flag);

	// PURPOSE:	Clear flags that require an object to be resident
	// PARAMS:	index - index of object
	//			flags - required flags
	// NOTES:	Current required flags are STRFLAG_DONTDELETE, STRFLAG_MISSION_REQUIRED, STRFLAG_CUTSCENE_REQUIRED and
	//			STRFLAG_INTERIOR_REQUIRED
	void ClearRequiredFlag(int index, int flag);

	// PURPOSE:	Set one or more flags
	// PARAMS:	index - index of object
	//			flags - bitmask of flags to set
	void SetFlag(strLocalIndex index, u32 flag);

	// PURPOSE:	Clear one or more flags
	// PARAMS:	index - index of object
	//			flags - bitmask of flags to clear
	void ClearFlag(strLocalIndex index, u32 flag);

	// PURPOSE:	Return if an object is required to be resident
	// PARAMS:	index - index of object
	// RETURNS: return if object is required
	bool IsObjectRequired(strLocalIndex index) const;

	// PURPOSE: Return any special flags this module should pass to pgStreamer::Read
	virtual u32 GetStreamerReadFlags() const ;

#if TRACK_PLACE_STATS
	void TrackRequest(size_t fileSize);
	void StartTrackingPlace();
	void StopTrackingPlace(const char *name, size_t fileSize);
	void UpdatePlaceStats();
	void ResetPlaceStats();

	void PrintPlaceStats();

	size_t GetRequestedSize() { return m_RequestedSize; }
	size_t GetLoadedSize(){ return m_LoadedSize; }

#else // TRACK_PLACE_STATS
	inline void StartTrackingPlace() {}
	inline void StopTrackingPlace(const char * /*name*/, size_t /*fileSize*/) {}
#endif // TRACK_PLACE_STATS

#if __STATS
	void TrackCompletedLoad(size_t virtSize, size_t physSize)				{ m_DataStreamed += virtSize + physSize; m_FilesLoaded++; }
	void TrackCanceledLoad(size_t virtSize, size_t physSize)				{ m_DataCanceled += virtSize + physSize; m_FilesCanceled++; }
	size_t GetDataStreamed() const											{ return m_DataStreamed; }
	int GetFilesLoaded() const												{ return m_FilesLoaded; }
	size_t GetDataCanceled() const											{ return m_DataCanceled; }
	int GetFilesCanceled() const											{ return m_FilesCanceled; }

	void ResetStreamingStats()												{ m_DataStreamed = m_DataCanceled = 0; m_FilesLoaded = m_FilesCanceled = 0; }
#else // __STATS
	inline void TrackCompletedLoad(size_t /*virtSize*/, size_t /*physSize*/)	{ }
	inline void TrackCanceledLoad(size_t /*virtSize*/, size_t /*physSize*/)		{ }
#endif // __STATS

#if __BANK
	void SetExpectedVirtualSize(size_t size) { m_expectedVirtualSize = size; }
	void SetExpectedPhysicalSize(size_t size) { m_expectedPhysicalSize = size; }
	size_t GetExpectedVirtualSize() { return m_expectedVirtualSize; }
	size_t GetExpectedPhysicalSize() { return m_expectedPhysicalSize; }

	void IncrementLoadedCount() {m_loadedCount++;}
	int GetLoadedCount() {return m_loadedCount;}

#endif // __BANK


#if TRACK_PLACE_STATS
	static bool *GetDumpPlaceStatsPtr()			{ return &sm_DumpPlaceStats; }
#endif // TRACK_PLACE_STATS


protected:
#if USE_PAGED_POOLS_FOR_STREAMING
	// Return the page from an object index
	u32 GetPageFromObjIndex(int index) const;

	// Return the element within the page from an object index. Page must already be known (via GetPageFromObjIndex).
	u32 GetElementFromObjIndex(int index) const;

	// Allocate a single new element.
	strIndex AllocateObject();

	// Free up a single element.
	void FreeObject(int index);

#endif // USE_PAGED_POOLS_FOR_STREAMING

	// PURPOSE: Internal helper function to add a streamable to a list of dependencies
	//          that is being prepared for GetDependencies(). It will assert and not
	//          add the dependency if it would end up out of bounds (although it will
	//          still increment iCount).
	// PARAMS:  indexArray - The beginning of the array to write the result in.
	//          iCount - Current number of entries in the array. Will be incremented.
	//                   This value will not exceed indexArraySize.
	//          dependency - Dependency to add to the array.
	//          indexArraySize - Total size of the array in indexArray, specified in units of strIndex.
	//			resourceIndex - Index of the streamable whose dependencies we're tallying up.
	//                          For debugging purposes.
	static void AddDependencyOutput(strIndex *indexArray, int &iCount, strIndex dependency, int indexArraySize, strIndex resourceIndex);

#if USE_PAGED_POOLS_FOR_STREAMING
	atArray<u16> m_PoolHeaders;	// Pool headers for (objectIndex / ELEMENTS_PER_PAGE)
	u32			m_used;			// Number of elements actually used in this module
	u32			m_ElementsPerPage;
	u32			m_MaxLimit;		// Hardlimit for this pool - will assert (but continue) if this limit is breached, but things will break
	u32			m_IndexWatermark;	// Current highest object index plus 1
#else //  USE_PAGED_POOLS_FOR_STREAMING
	strIndex	m_indexOffset;
#endif //  USE_PAGED_POOLS_FOR_STREAMING

	s32			m_moduleId;
	u32			m_size;
	atString	m_ModuleName;
	bool		m_RequiresTempMemory;
	bool		m_canDefragment;
	bool		m_usesExtraMemory;
	s32			m_expectedRscVersionNumber;
	s32			m_moduleTypeIndex;
#if __BANK
	size_t		m_expectedVirtualSize;
	size_t		m_expectedPhysicalSize;
	int			m_loadedCount;
#endif // __BANK

#if STREAM_STATS
	size_t		m_DataStreamed;
	size_t		m_DataCanceled;
	int			m_FilesLoaded;
	int			m_FilesCanceled;
#endif // STREAM_STATS

#if TRACK_PLACE_STATS
	float m_TotalPlaceTime;		// Total number of milliseconds spent on placing data
	float m_MinPlaceTime;		// Shortest time it took to place a file in ms
	float m_MaxPlaceTime;		// Longest time it took to place a file in ms
	int m_FilesPlaced;			// Number of files that have been placed by this module
	float m_TotalPlaceTimePerKb;// Total number of milliseconds spent on placing data, per KB placed
	float m_MinPlaceTimePerKb;	// Shortest time it took to place a file in ms, per KB placed
	float m_MaxPlaceTimePerKb;	// Longest time it took to place a file in ms, per KB placed
	size_t m_LoadedSize;		// Loaded size this frame.
	size_t m_RequestedSize;		// Requested size this frame.
	char m_SlowestFile[64];		// The file that took the longest to load
#endif // TRACK_PLACE_STATS




#if TRACK_PLACE_STATS
	static sysTimer sm_LastPlaceStart;	// Time when the last place operation started
	static bool sm_DumpPlaceStats;	// Dump place stats to the TTY as they occur
#endif // TRAC_PLACE_STATS
};

// --- Globals ------------------------------------------------------------------

inline bool strStreamingModule::StreamingFull() const
{
	return strStreamingEngine::GetInfo().GetRequestInfoPoolCount() == strStreamingEngine::GetInfo().GetRequestInfoPoolCapacity();
}

inline bool strStreamingModule::StreamingRequest(strLocalIndex index, s32 flags)
{
	return strStreamingEngine::GetInfo().RequestObject(GetStreamingIndex(strLocalIndex(index)), flags);
}

inline bool strStreamingModule::StreamingRemove(strLocalIndex index)
{
	return strStreamingEngine::GetInfo().RemoveObject(GetStreamingIndex(strLocalIndex(index)));
}

inline bool strStreamingModule::IsObjectRequested(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().IsObjectRequested(GetStreamingIndex(strLocalIndex(index)));
}

inline bool strStreamingModule::IsObjectLoading(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().IsObjectLoading(GetStreamingIndex(strLocalIndex(index)));
}

inline bool strStreamingModule::HasObjectLoaded(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().HasObjectLoaded(GetStreamingIndex(strLocalIndex(index)));
}

inline bool strStreamingModule::IsObjectInImage(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().IsObjectInImage(GetStreamingIndex(strLocalIndex(index)));
}

inline bool strStreamingModule::IsObjectNew(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().IsObjectNew(GetStreamingIndex(strLocalIndex(index)));
}

inline int strStreamingModule::GetStreamingFlags(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().GetObjectFlags(GetStreamingIndex(strLocalIndex(index)));
}

inline strStreamingInfo* strStreamingModule::GetStreamingInfo(strLocalIndex index)
{
	return strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(index));
}

inline const strStreamingInfo* strStreamingModule::GetStreamingInfo(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().GetStreamingInfo(GetStreamingIndex(index));
}

inline strIndex strStreamingModule::GetStreamingIndex(strLocalIndex index) const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	u32 page = GetPageFromObjIndex(index);
	return strIndex(page, GetElementFromObjIndex(index));
#else // USE_PAGED_POOLS_FOR_STREAMING
	FastAssert(index.Get() >= 0);
	return strIndex(m_indexOffset.Get()+index.Get());
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

#if USE_PAGED_POOLS_FOR_STREAMING
__forceinline u32 strStreamingModule::GetPageFromObjIndex(strLocalIndex index) const
{
	u32 page = m_PoolHeaders[index / strStreamingEngine::GetInfo().GetStreamingInfoPool().GetElementsPerPage((u32) GetStreamingModuleId())];			// If this is an out-of-array read, the index is invalid
	FastAssert(strStreamingEngine::GetInfo().GetStreamingInfoPool().GetPageHeader(page).GetSubType() == (u32) GetStreamingModuleId());	// Make sure this page belongs to us - if not, this index is invalid

	return page;
}

__forceinline u32 strStreamingModule::GetElementFromObjIndex(strLocalIndex index) const
{
	return index % strStreamingEngine::GetInfo().GetStreamingInfoPool().GetElementsPerPage((u32) GetStreamingModuleId());
}
#endif // USE_PAGED_POOLS_FOR_STREAMING


inline void strStreamingModule::SetRequiredFlag(int index, int flag)
{
	strStreamingEngine::GetInfo().SetRequiredFlag(GetStreamingIndex(strLocalIndex(index)), flag);
}

inline void strStreamingModule::ClearRequiredFlag(int index, int flag)
{
	strStreamingEngine::GetInfo().ClearRequiredFlag(GetStreamingIndex(strLocalIndex(index)), flag);
}

inline bool strStreamingModule::IsObjectRequired(strLocalIndex index) const
{
	return strStreamingEngine::GetInfo().IsObjectRequired(GetStreamingIndex(index));
}

inline void strStreamingModule::SetFlag(strLocalIndex index, u32 flag)
{
	strIndex streamingIndex = GetStreamingIndex(index);
	strStreamingEngine::GetInfo().GetStreamingInfoRef(streamingIndex).SetFlag(streamingIndex, flag);
}

inline void strStreamingModule::ClearFlag(strLocalIndex index, u32 flag)
{
	strIndex streamingIndex = GetStreamingIndex(index);
	strStreamingEngine::GetInfo().GetStreamingInfoRef(streamingIndex).ClearFlag(streamingIndex, flag);
}

inline strLocalIndex strStreamingModule::GetObjectIndex(strIndex index) const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	strStreamingInfoPageHeader &header = strStreamingEngine::GetInfo().GetStreamingInfoHeader(index);
	return index.GetElement() + (u32) header.GetUserData();
#else // USE_PAGED_POOLS_FOR_STREAMING
	FastAssert(index >= m_indexOffset);
	FastAssert(index.Get() - m_indexOffset.Get() < m_size);
	return strLocalIndex(index.Get() - m_indexOffset.Get());
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

inline void strStreamingModule::AddDependencyOutput(strIndex *indexArray, int &iCount, strIndex dependency, int indexArraySize, strIndex ASSERT_ONLY(resourceIndex))
{
#if !__FINAL
	static int currentPeakCount, overallPeakCount;
#endif // !__FINAL

	if (iCount < indexArraySize)
	{
		indexArray[iCount++] = dependency;

#if !__FINAL
		currentPeakCount = iCount;
#endif // !__FINAL
	}
	else
	{
#if !__FINAL
		if (++currentPeakCount > overallPeakCount)
		{
			streamErrorf("Peak streamable dependency count: %d", iCount);
			overallPeakCount = iCount;
		}
#endif // !__FINAL

		streamAssertf(false, "Too many dependencies for streamable %s. Check the TTY for subsequent messages about 'peak streamable dependency count' to see how large the array needs to be.",
			strStreamingEngine::GetInfo().GetObjectName(resourceIndex));
	}
}


}	// namespace rage

#endif // !STREAMING_STREAMINGMODULE_H_
