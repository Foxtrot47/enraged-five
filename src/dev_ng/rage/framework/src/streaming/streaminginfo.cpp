//
// streaming/streaminginfo.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "streaminginfo.h"
// C headers
#include <algorithm>
#include <string.h>
#include <stdio.h>

// Rage headers
#include "entity/archetypemanager.h"
#include "paging/base.h"
#include "diag/art_channel.h"
#include "diag/output.h"
#include "file/packfile.h"
#include "grcore/debugdraw.h"
#include "paging/rscbuilder.h"
#include "profile/element.h"
#include "profile/telemetry.h"
#include "profile/timebars.h"
#include "system/memmanager.h"
#include "system/param.h"
#include "system/tasklog.h"
#include "system/threadtype.h"
#include "system/timemgr.h"

#include "defragmentation.h"
#include "packfilemanager.h"
#include "requestrecorder.h"
// #include "streamingdebug.h"
#include "streamingengine.h"
#include "streaminglive.h"
#include "streamingmodule.h"
// #include "streamingrequestlist.h"
#include "streamingloader.h"
#include "streamingvisualize.h"
#include "strindexdebugmanager.h"

// #include "debug/debug.h"
#include "fwsys/timer.h"
#include "system/cache.h"

#include "fwscene/stores/txdstore.h"
#include "fwscene/stores/maptypesstore.h"

#include "file/asset.h"
#include "fwrenderer/renderthread.h"

namespace rage {

PARAM(dontwarnonmissingmodule, "[streaming] Don't print a warning if a streaming module is missing");
PARAM(sortonhdd, "[streaming] Enable LSN sort on HDD");
PARAM(fairhddscheduler, "[streaming] Improve scheduling on HDD-based files");
PARAM(noarea51, "[streaming] Disable AREA 51");

XPARAM(nomtplacement);

ASSERT_ONLY(static bool s_StreamingCheckSortKeyOrder;)

bool g_AREA51 = true;
int g_AREA51_FRAME = 1;
int g_AREA51_HAS_SORTED[pgStreamer::DEVICE_COUNT][2];

#if __STATS
EXT_PF_TIMER(GetNextRequest);
EXT_PF_TIMER(RemoveRequest);
#if __DEV
EXT_PF_VALUE_FLOAT(AvgListSorts);
EXT_PF_VALUE_INT(ListSorts);
EXT_PF_VALUE_INT(GetNextCalls);
EXT_PF_VALUE_INT(RealReqRemoves);
#endif // __DEV
#endif // __STATS

#if !STREAMING_DETAILED_STATS
#if STORE_FILE_SIZES
	CompileTimeAssert(sizeof(strStreamingInfo) == 16);
#else // STORE_FILE_SIZES
CompileTimeAssert(sizeof(strStreamingInfo) == 8);
#endif // STORE_FILE_SIZES
#endif // !STREAMING_DETAILED_STATS

#define MAX_OBJECT_MEMORY_SIZE		(15*1024*1024)

#if __BANK
// This is the streamable currently being unloaded. We're not allowed to
// re-request it during the unloading process.
static atFixedArray<strIndex, 10>  s_StreamablesBeingUnloaded;
#endif // __BANK

#if !__NO_OUTPUT
const char *strRefKindNames[MAX_REF_KINDS] = {
	"REF_RENDER",
	"REF_SCRIPT",
//	"REF_DEFRAG",
	"REF_OTHER",
};

const u8 strRefKindBits[MAX_REF_KINDS] = {
	REF_RENDER_BITS,
	REF_SCRIPT_BITS,
//	REF_DEFRAG_BITS,
	REF_OTHER_BITS,
};

#endif // !__NO_OUTPUT


bool strStreamingInfoManager::sm_SortOnHDD = false;						// Sort by LSN even on HDD
bool strStreamingInfoManager::sm_FairHDDScheduler = false;
s32  strStreamingInfoManager::sm_numRealRequests_Cached = 0;


#if __DEV
struct StreamingLogEntry
{
	char m_RscName[64];
	char m_SecondaryName[64];
	int m_VirtSize;
	int m_PhysSize;
	bool m_Special;

	StreamingLogEntry()
		: m_VirtSize(0)
		, m_PhysSize(0)
		, m_Special(false)
	{
		m_RscName[0] = 0;
		m_SecondaryName[0] = 0;
	}

	void Set(bool special, const char *rscName, int virtSize, int physSize, const char *secondaryName = NULL)
	{
		m_Special = special;
		m_VirtSize = virtSize;
		m_PhysSize = physSize;
		FastAssert(rscName);
		safecpy(m_RscName, rscName);

		if (secondaryName)
		{
			safecpy(m_SecondaryName, secondaryName);
		}
		else
		{
			m_SecondaryName[0] = 0;
		}
	}
};

typedef atQueue<StreamingLogEntry, strStreamingInfoManager::REQUEST_LOG_SIZE> strDebugStreamingLog;

strDebugStreamingLog s_RequestLog;
strDebugStreamingLog s_RPFCacheMissLog;
strDebugStreamingLog s_OutOfMemoryLog;
strDebugStreamingLog s_DeletedObjectsLog;
strDebugStreamingLog s_CompletedLog;

void AddLogEntry(const char *logString, bool special, strDebugStreamingLog &log, const char *rscName, int virtSize, int physSize, const char *secondaryName = NULL)
{
	// Kill the oldest entry if the log is full.
	if (log.IsFull())
	{
		log.Drop();
	}

	StreamingLogEntry &entry = log.Append();
	entry.Set(special, rscName, virtSize, physSize, secondaryName);

	if (*strStreamingEngine::GetInfo().GetLogActivityToTTYPtr())
	{
		streamDisplayf("%s: %s, virt=%dK, phys=%dK %s", logString, rscName, virtSize / 1024, physSize / 1024,
			(secondaryName) ? secondaryName : "");
	}
}


#endif // __DEV

#if __STATS && __DEV
static int s_GetNextFilesCalls;
static int s_RealRequestRemoves;
#endif // __STATS && __DEV

#if !__FINAL
// Disabled for now - you can use s_StreamableBeingUnloaded for this for now.
//static char gRemovedObjName[255];
#endif

///////////////////////////////////////////////////////////////////////////////
// strStreamingInfo
///////////////////////////////////////////////////////////////////////////////
strStreamingInfo*		strStreamingInfo::m_pArrayBase;

// Initialise streaming info structure.
void strStreamingInfo::Init()
{
	m_handle = 0;

	m_flags = 0;
	m_dependentCount = 0;

	m_status = STRINFO_NOTLOADED;

#if STORE_FILE_SIZES
	m_VirtualSize = 0;
	m_PhysicalSize = 0;
#endif // STORE_FILE_SIZES
}


#if USE_PAGED_POOLS_FOR_STREAMING
strStreamingInfoIterator::strStreamingInfoIterator()
: m_Iterator(strStreamingEngine::GetInfo().GetStreamingInfoPool())
{
	Reset();
}

void strStreamingInfoIterator::operator++()
{
	++m_Iterator;
}

strIndex strStreamingInfoIterator::operator *() const
{
	return m_Iterator.GetPagedPoolIndex();
}

bool strStreamingInfoIterator::IsAtEnd() const
{
	return m_Iterator.IsAtEnd();
}

void strStreamingInfoIterator::Reset()
{
	m_Iterator.Reset();
}

#else // USE_PAGED_POOLS_FOR_STREAMING

strStreamingInfoIterator::strStreamingInfoIterator()
{
	Reset();
}

void strStreamingInfoIterator::operator++()
{
	++m_Index;
}

strIndex strStreamingInfoIterator::operator *() const
{
	return strIndex(m_Index);
}

bool strStreamingInfoIterator::IsAtEnd() const
{
	return m_Index == strStreamingEngine::GetInfo().GetStreamingInfoSize();
}

void strStreamingInfoIterator::Reset()
{
	m_Index = 0;
}
#endif // USE_PAGED_POOLS_FOR_STREAMING




///////////////////////////////////////////////////////////////////////////////
// strStreamingInfoManager
///////////////////////////////////////////////////////////////////////////////

#define DEFAULT_MAX_ACTIVE_REQUEST_INFO		(3000)
#define ACTIVE_REQUEST_INFO			200

BANK_ONLY(bool strStreamingInfoManager::ms_bValidDlcOverlay =false;)

strStreamingInfoManager::strStreamingInfoManager() :
	m_requestInfoMap(atMapHashFn<strIndex>(), atMapEquals<strIndex>(), m_requestInfoPool.m_Functor),
	m_loadingInfoMap(atMapHashFn<strIndex>(), atMapEquals<strIndex>(), m_loadingInfoPool.m_Functor),
	m_loadedInfoMap(atMapHashFn<strIndex>(), atMapEquals<strIndex>(), m_loadedInfoPool.m_Functor),
	m_loadedPersistentInfoMap(atMapHashFn<strIndex>(), atMapEquals<strIndex>(), m_loadedInfoPool.m_Functor)
#if ENABLE_DEBUG_HEAP
	, m_ObjectDependenciesMap()
#endif
{
	USE_MEMBUCKET(MEMBUCKET_STREAMING);

	m_loadingInfoPool.Create<LoadingInfoMapEntry>(LOAD_SLOTS_PER_DEVICE * pgStreamer::DEVICE_COUNT);	
	m_loadingInfoMap.Create(LOAD_SLOTS_PER_DEVICE * pgStreamer::DEVICE_COUNT, false);
	m_LastLoadedPrevFrame = NULL;

	ASSERT_ONLY(m_UnrequestsLocked = false;)
}

strStreamingInfoManager::~strStreamingInfoManager()
{
	m_loadedInfoMap.Kill();
	m_loadedPersistentInfoMap.Kill();
	m_loadingInfoMap.Kill();
	m_requestInfoMap.Kill();
	m_requestInfoPool.Destroy();
	m_loadingInfoPool.Destroy();
	m_loadedInfoPool.Destroy();

	STRINDEX_DEBUG_MANAGER( Shutdown() );
}

// Initialise the manager.
void strStreamingInfoManager::Init()
{
	m_loadedInfoPool.Create<LoadedInfoMapEntry>(fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("MaxLoadedInfo",0xF2E37BE0), CONFIGURED_FROM_FILE));	
	m_loadedInfoMap.Create((u16) fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("ActiveLoadedInfo",0xB758AEC3), CONFIGURED_FROM_FILE), false);
	//note this one must pass a prime value in for num slots
	m_loadedPersistentInfoMap.CreatePrimed((u16) fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("ActivePersistentLoadedInfo",0xC732FB32), CONFIGURED_FROM_FILE));

	{
		USE_MEMBUCKET(MEMBUCKET_STREAMING);
		int maxLoadRequestedInfo = fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("MaxLoadRequestedInfo",0x41663795), DEFAULT_MAX_ACTIVE_REQUEST_INFO);
		m_requestInfoPool.Create<RequestInfoMapEntry>(maxLoadRequestedInfo);	
		m_requestInfoMap.Create((u16) maxLoadRequestedInfo, false);

		for (int x=0; x<pgStreamer::DEVICE_COUNT; x++)
		{
			m_SortedPhysicalRequests[x].Reserve(maxLoadRequestedInfo);
		}
	}

	ms_moduleMgr.Init();

	g_AREA51 = !PARAM_noarea51.Get();

	sm_SortOnHDD = PARAM_sortonhdd.Get();
	sm_FairHDDScheduler = PARAM_fairhddscheduler.Get();

	sysMemSet(m_LastGetNextFilesIndex, 0, sizeof(m_LastGetNextFilesIndex));

#if USE_PAGED_POOLS_FOR_STREAMING
	m_StreamingInfos.Init(STREAMING_INFO_MAX_PAGE_COUNT, STREAMING_INFO_ELEMENTS_PER_PAGE, MAX_STREAMING_MODULES, 16);
#endif // USE_PAGED_POOLS_FOR_STREAMING

#if ENABLE_DEBUG_HEAP
	// Allocate our map from the debug heap - it's only used in BANK, so this should be fine
	sysMemStartDebug();	
	//m_ObjectDependenciesMapMemoryPool.Create<ObjectDependenciesMap::Entry>(OBJECT_MAP_MEMORY_POOL_DEFAULT_SIZE);
	m_ObjectDependenciesMap.Init(OBJECT_MAP_MEMORY_POOL_DEFAULT_SIZE); // will be rounded up to the next highest prime, and will quitf if the pool is full
	//Assert(OBJECT_MAP_MEMORY_POOL_DEFAULT_SIZE == m_ObjectDependenciesMap.GetNumSlots());
	sysMemEndDebug();
#endif
}

void strStreamingInfoManager::AllocateStreamingInfoArray()
{
	USE_MEMBUCKET(MEMBUCKET_STREAMING);
#if __ASSERT
	ms_moduleMgr.DisallowModuleAdd();
#endif // !__FINAL

	int numIndices = ms_moduleMgr.GetTotalStreamingEntries();
	STRVIS_SET_TOTAL_STREAMABLE_COUNT(numIndices);
	STRINDEX_DEBUG_MANAGER( Init( numIndices ) );

#if USE_PAGED_POOLS_FOR_STREAMING
	if (!m_size)
	{
		m_size = numIndices;
	}
#else // USE_PAGED_POOLS_FOR_STREAMING
	if (m_aInfoForModel.GetCount() == 0)
	{
		m_size = numIndices;
		{
			sysMemAutoUseFlexMemory mem;
			m_aInfoForModel.Resize(m_size);
		}		
		strStreamingInfo::SetArrayBase(m_aInfoForModel.GetElements());
	}

	// initialise all streaming infos but the start and end pointers
	for (u32 i = 0; i < m_size; i++)
	{
		m_aInfoForModel[i].Init();
	}

	ms_moduleMgr.CacheModuleIndexRanges();
#endif // !USE_PAGED_POOLS_FOR_STREAMING
}

void strStreamingInfoManager::InitLevelWithMapUnloaded()
{
	m_numRequests = 0;
	m_numRealRequests = 0;
	m_numPriorityRequests = 0;
#if __DEV
	m_LogActivityToTTY = false;
#endif // __DEV

	ResetStreamingStats();
}

void strStreamingInfoManager::ResetStreamingStats()
{
#if __BANK
	m_numMissingPackfileDpendencies = 0;
	m_numTotalRequests = 0;
	m_numTotalOOMFailedLoads = 0;
	m_numTotalDeletedFiles = 0;
	m_numRealRequestRemoves = 0;
	m_numRealRequestAdds = 0;
	m_numLastRealRequestRemoves = 0;
	m_numLastRealRequestAdds = 0;
	m_numAverageRealRequestAdds = 0.0f;
	m_numAverageRealRequestRemoves = 0.0f;
	m_numListSorts = 0;
	m_numAverageListSorts = 0.0f;
#endif // __BANK

#if STREAMING_DETAILED_STATS
	sysMemSet(m_ChurnRequests, 0, sizeof(m_ChurnRequests));
	sysMemSet(m_TotalReqToLoadTime, 0, sizeof(m_TotalReqToLoadTime));
	sysMemSet(m_ReqToLoads, 0, sizeof(m_ReqToLoads));
	sysMemSet(m_TotalLoadToResidentTime, 0, sizeof(m_TotalLoadToResidentTime));
	sysMemSet(m_ReqToLoadeds, 0, sizeof(m_ReqToLoadeds));
	sysMemSet(m_TotalReqToResidentTime, 0, sizeof(m_TotalReqToResidentTime));
#endif // STREAMING_DETAILED_STATS
}

void strStreamingInfoManager::Update()
{
#if STREAM_STATS
	m_Purged = 0;
	m_Deleted = 0;
	m_DeletedSize = 0;

#if __DEV
#if __STATS
	PF_SET(AvgListSorts, m_numAverageListSorts);
	PF_SET(ListSorts, m_numListSorts);
	PF_SET(GetNextCalls, s_GetNextFilesCalls);
	PF_SET(RealReqRemoves, s_RealRequestRemoves);
	s_GetNextFilesCalls = 0;
#endif // __STATS

	m_numAverageRealRequestAdds = m_numAverageRealRequestAdds * 0.95f + float(m_numRealRequestAdds - m_numLastRealRequestAdds) * 0.05f;
	m_numAverageRealRequestRemoves = m_numAverageRealRequestRemoves * 0.95f + float(m_numRealRequestRemoves - m_numLastRealRequestRemoves) * 0.05f;

	m_numLastRealRequestRemoves = m_numRealRequestRemoves;
	m_numLastRealRequestAdds = m_numRealRequestAdds;

	m_numAverageListSorts = (m_numAverageListSorts * 0.95f) + (m_numListSorts * 0.05f);
	m_numListSorts = 0;
#endif // __DEV

#endif // STREAMING_STATS

	// caching the value to be accessed from the outside
	sm_numRealRequests_Cached = GetNumberRealObjectsRequested();
}


void strStreamingInfoManager::Shutdown()
{
	DestroyDependentGraphs();

#if USE_PAGED_POOLS_FOR_STREAMING
	m_StreamingInfos.Shutdown();
#else // USE_PAGED_POOLS_FOR_STREAMING
	{
		sysMemAutoUseFlexMemory mem;
		m_aInfoForModel.Reset();
	}	
#endif // USE_PAGED_POOLS_FOR_STREAMING
	ms_moduleMgr.Shutdown();

	m_loadedPersistentInfoMap.Kill();
	m_loadedInfoMap.Kill();
	m_loadedInfoPool.Destroy();

#if ENABLE_DEBUG_HEAP
	// Shut down our object dependencies map
	sysMemStartDebug();		
	m_ObjectDependenciesMap.Reset();
	m_ObjectDependenciesMapMemoryPool.Destroy();
	sysMemEndDebug();
#endif
}


void strStreamingInfoManager::AddDependentCounts(strIndex dependent)
{
	strStreamingModule* pModule = GetModule(dependent);
	strLocalIndex objIndex = pModule->GetObjectIndex(dependent);
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	s32 numDeps = pModule->GetDependencies(objIndex, deps, STREAMING_MAX_DEPENDENCIES);

	if(numDeps > 0)
	{
		MoveToPersistentLoaded(dependent);
	}

	for (int i = 0; i != numDeps;++i)
	{
		Assertf(GetStreamingInfo(deps[i])->GetDependentCount() < 10000, "Dependent count for %s is %d - that seems awfully high. Something leaking?", GetObjectName(deps[i]), GetStreamingInfo(deps[i])->GetDependentCount());
		GetStreamingInfo(deps[i])->AddDependent();

		MoveToPersistentLoaded(deps[i]);
	}
}

void strStreamingInfoManager::RemoveDependentCountsAndUnrequest(strIndex dependent)
{
#if !__FINAL
	static int recursionCheck;
	bool dumpDependencies = false;

	BANK_ONLY(char name[256];) // Compile fix - This is only needed/used for the DIAG_CONTEXT_MESSAGE below, which is only defined in __BANK
	DIAG_CONTEXT_MESSAGE("RemoveDependentCountsAndUnrequest %s", GetObjectName(dependent, name, 256));

	if (++recursionCheck > 8)
	{
#if __ASSERT
		diagContextMessage::DisplayContext("RemoveDependentCountsAndUnrequest");
		Assertf(false, "RemoveDependentCountsAndUnrequest - recursion level %d for %s", recursionCheck, GetObjectName(dependent));
#else
		Warningf("RemoveDependentCountsAndUnrequest - recursion level %d for %s", recursionCheck, GetObjectName(dependent));
#endif
		dumpDependencies =  true;
	}
#endif // !__FINAL

	strStreamingModule* pModule = GetModule(dependent);
	strLocalIndex objIndex = pModule->GetObjectIndex(dependent);
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	s32 numDeps = pModule->GetDependencies(objIndex, deps, STREAMING_MAX_DEPENDENCIES);
	for (int i = 0; i != numDeps;++i)
	{
		strStreamingInfo &info = GetStreamingInfoRef(deps[i]);

#if !__FINAL
		if (dumpDependencies)
		{
			Errorf("Dependency %d: %s, dependent count %d", i, GetObjectName(deps[i]), info.GetDependentCount());
		}
#endif // !__FINAL

		Assertf(info.GetDependentCount() > 0, "%s has a negative dependent count", GetObjectName(deps[i]));
		info.RemoveDependent();

		if (info.GetDependentCount() == 0)
		{
			if(info.GetStatus() == STRINFO_LOADREQUESTED && (!(info.GetFlags() & STR_DONTDELETE_MASK)))
			{
				streamDebugf3("Removing %s (%" STRINDEX_FMT ") as the last dependency for %" STRINDEX_FMT, GetObjectName(deps[i]), deps[i].Get(), dependent.Get());
				RemoveObject(deps[i]);
			}
			// no dependents and not dont delete - remove from m_loadedPersistentInfoMap and return to LoadedInfoMap
			else if(!(info.GetFlags() & STR_DONTDELETE_MASK))
			{ 
				MoveToLoaded(deps[i]);
			}
		}
	}



	strStreamingInfo &depInfo = GetStreamingInfoRef(dependent);
	if(!(depInfo.GetFlags() & STR_DONTDELETE_MASK))
	{
		MoveToLoaded(dependent);
	}

#if !__FINAL
	--recursionCheck;
#endif // !__FINAL
}

void strStreamingInfoManager::MoveToPersistentLoaded(strIndex index)
{
	strLoadedInfo* loaded = m_loadedInfoMap.Access(index); 
	if(loaded)
	{
		RemoveLoaded(index);
		AddPersistentLoaded(index, loaded->GetVirtualSize(), loaded->GetPhysicalSize());
	}
}

void strStreamingInfoManager::MoveToLoaded(strIndex index)
{
	strLoadedInfo* loaded = m_loadedPersistentInfoMap.Access(index); 
	if(loaded)
	{
		RemovePersistentLoaded(index);
		AddLoaded(index, loaded->GetVirtualSize(), loaded->GetPhysicalSize());
	}
}

void strStreamingInfoManager::SetStatus(strIndex index, strResidentStatus status)
{
	streamAssert(sysThreadType::IsUpdateThread());

	strStreamingInfo& info = GetStreamingInfoRef(index);
	strResidentStatus origStatus = (strResidentStatus) info.m_status;

	info.m_status = status;

	if (origStatus != status)
	{
		STR_TELEMETRY_MESSAGE_LOG("(streaming/status)%s status from %s to %s", GetObjectName(index),
			strStreamingInfo::GetFriendlyStatusName(origStatus), strStreamingInfo::GetFriendlyStatusName(status));

		STRVIS_SET_STATUS(index, status);
	}

	if (origStatus == STRINFO_NOTLOADED && status > STRINFO_NOTLOADED)
	{
		AddDependentCounts(index);
	}
	else if (origStatus > STRINFO_NOTLOADED && status == STRINFO_NOTLOADED)
	{
		RemoveDependentCountsAndUnrequest(index);
	}
}

#if __ASSERT
void strStreamingInfo::ClearFlags() {
	Assert(GetStatus() == STRINFO_NOTLOADED || ((m_flags & STRFLAG_PRIORITY_LOAD) == 0));
	m_flags = 0;
}
#endif // __ASSERT

strRequestInfo& strStreamingInfoManager::AddRequest(strIndex index)
{
	streamAssert(sysThreadType::IsUpdateThread());

	streamAssertf(!m_requestInfoMap.Access(index), "%s is being added to the request list, but it's already on the list. Its current status is %s.",
		GetObjectName(index), strStreamingInfo::GetFriendlyStatusName(GetStreamingInfoRef(index).GetStatus()));

#if __ASSERT
	if (m_requestInfoPool.IsFull())
	{
		PrintRequestList();
		Assertf(false, "Streaming request list is full");
	}
#endif // __ASSERT

	strRequestInfo& request = m_requestInfoMap.Insert(index).data; 
	request.m_Index = index;
	m_requestInfoList.AddToHead(&request);

#if STREAMING_DETAILED_STATS
	request.m_RequestTime = sysTimer::GetSystemMsTime();
#endif // STREAMING_DETAILED_STATS

	m_numRequests++;

	strStreamingInfo &info = GetStreamingInfoRef(index);
	u32 flags = info.GetFlags();

	streamAssert(info.GetStatus() == STRINFO_NOTLOADED);

	if (flags & STRFLAG_PRIORITY_LOAD)
	{
		m_numPriorityRequests++;
	}

	if (!(flags & STRFLAG_INTERNAL_DUMMY))
	{
		pgStreamer::Device device = info.GetPreferredDevice(index);
		AddRealRequestToSortList(device, info, request);

		m_numRealRequests++;

#if __DEV
		m_numRealRequestAdds++;
#endif // __DEV
	}
	else
	{
		m_dummyRequestList.Grow() = index;
	}

	return request;
}

void strStreamingInfoManager::AddRealRequestToSortList(pgStreamer::Device device, strStreamingInfo &info, strRequestInfo &request)
{
	streamAssert(!(info.GetFlags() & STRFLAG_INTERNAL_DUMMY));

	SortRequestArray &requests = m_SortedPhysicalRequests[device];

	if (requests.GetCount() == requests.GetCapacity())
	{
		// If we're full, it means that we accumulated too many dead entries. Let's wipe them out.
		for (int x=requests.GetCount()-1; x>=0; x--)
		{
			if (requests[x].IsDeleted())
			{
				requests.DeleteFast(x);
			}
		}

		if (requests.GetCount() == requests.GetCapacity())
		{
			Quitf(ERR_STR_INFO_1,"Too many active requests - does something leak in the request array?");
		}
	}

	strRequestSortEntry &entry = m_SortedPhysicalRequests[device].Append();
	entry.m_Priority = (info.GetFlags() & STRFLAG_PRIORITY_LOAD) != 0;
	//Assign(entry.m_SortKey, info.GetCdPosn(device == pgStreamer::OPTICAL));
	entry.m_SortKey = info.GetCdPosn(device == pgStreamer::OPTICAL);
	entry.m_Index = request.m_Index;

	m_SortedRequestListDirty[device] = true;
}

void strStreamingInfoManager::RemoveRealRequestFromSortList(pgStreamer::Device device, strRequestInfo &request)
{
	PF_FUNC(RemoveRequest);

#if __STATS && __DEV
	s_RealRequestRemoves++;
#endif // __STATS && __DEV

	int count = m_SortedPhysicalRequests[device].GetCount();

	strRequestSortEntry *entry = m_SortedPhysicalRequests[device].GetElements();
	strIndex index = request.m_Index;

	for (int x=0; x<count; x++)
	{
		if (entry->m_Index == index)
		{
			// Found it.
			entry->m_SortKey = strRequestSortEntry::DELETED;
			entry->m_Index = strIndex();
			m_SortedRequestListDirty[device] = true;
			return;
		}

		entry++;
	}

	// TODO: This assert could conceivably trigger if something is requested while it's on ODD but has
	// been cached to HDD by the time it's removed.
	streamAssertf(false, "Removing request for %s from device %d, but it's not in the list.", GetObjectName(request.m_Index), device);
}


void strStreamingInfoManager::RemoveRequest(strIndex index)
{
	streamAssert(sysThreadType::IsUpdateThread());

	strRequestInfo* request = m_requestInfoMap.Access(index);
	streamAssertf(request, "Can't find request to delete");

	streamAssertf(m_numRequests >= 0, "Number of requests is negative? (%d)", m_numRequests);
	m_numRequests--;

	strStreamingInfo &info = GetStreamingInfoRef(index);
	u32 flags = info.GetFlags();

	streamAssert(info.GetStatus() == STRINFO_LOADREQUESTED);

	if (flags & STRFLAG_PRIORITY_LOAD)
	{
		streamAssertf(m_numPriorityRequests >= 0, "Number of priority requests is negative? (%d)", m_numPriorityRequests);
		m_numPriorityRequests--;
	}

	if (!(flags & STRFLAG_INTERNAL_DUMMY))
	{
		streamAssertf(m_UnrequestsLocked == false, "Removing a non-dummy request (%s) while processing the request list. This can break the integrity of the sorted request list.",
			GetObjectName(index));

		RemoveRealRequestFromSortList(info.GetPreferredDevice(index), *request);

		streamAssertf(m_numRealRequests >= 0, "Number of real requests is negative? (%d)", m_numRealRequests);
		m_numRealRequests--;

#if __DEV
		m_numRealRequestRemoves++;
#endif // __DEV
	}
	else
	{
		int dummyIndex = m_dummyRequestList.Find(index);
		if (streamVerifyf(dummyIndex != -1, "Requested dummy is not in the request list"))
		{
			m_dummyRequestList.DeleteFast(dummyIndex);
		}
	}

	m_requestInfoList.Remove(request);
	m_requestInfoMap.Delete(index);
}


strRequestInfo* strStreamingInfoManager::GetRequest(strIndex index)
{
	return m_requestInfoMap.Access(index);
}


strLoadingInfo& strStreamingInfoManager::AddLoading(strIndex index)
{	
	strLoadingInfo& loading = m_loadingInfoMap.Insert(index).data; 

	return loading;
}


void strStreamingInfoManager::RemoveLoading(strIndex index)
{
	//streamVerifyf(
		m_loadingInfoMap.Delete(index); //, "Trying to remove %s from the 'loading' list, but it's not there.",	GetObjectName(index));
}


strLoadingInfo* strStreamingInfoManager::GetLoading(strIndex index)
{
	return m_loadingInfoMap.Access(index);
}

void strStreamingInfoManager::InitLoaded(strLoadedInfo& loaded, strIndex index, u32 virtSize, u32 physSize)
{
	strStreamingInfo &info = GetStreamingInfoRef(index);
	strStreamingModule *module = GetModule(index);

	loaded.m_Index = index;
	if (virtSize || physSize)
	{
		// The user already provided the size - we can just copy it.
		loaded.SetVirtualSize(virtSize);
		loaded.SetPhysicalSize(physSize);
	}
	else
	{
		strLocalIndex objIndex = module->GetObjectIndex(index);

		size_t baseVirt = 0;
		size_t basePhys = 0;
		if (!module->RequiresTempMemory(objIndex))
		{
			baseVirt = info.ComputeOccupiedVirtualSize(index);
			basePhys = info.ComputePhysicalSize(index);
		}

		size_t extraVirt = module->GetExtraVirtualMemory(objIndex);
		size_t extraPhys = module->GetExtraPhysicalMemory(objIndex);

		loaded.SetVirtualSize(u32(baseVirt + extraVirt));
		loaded.SetPhysicalSize(u32(basePhys + extraPhys));
	}

	// Cache the module, we'll need it later a lot.
	loaded.SetModule(module);
}

strLoadedInfo& strStreamingInfoManager::AddLoaded(strIndex index, u32 virtSize, u32 physSize)
{	
	streamAssert(!m_loadedInfoMap.Access(index));
	if (!streamVerifyf(!m_loadedInfoPool.IsFull(), "Exceeded maximum loading objects - outputting loaded list to TTY"))
	{
		BANK_ONLY(PrintLoadedList(true, true, false));
		g_strStreamingInterface->DumpStreamingInterests();
		Quitf(ERR_STR_INFO_2,"Please include the loaded list in the bug report");
	}
	strLoadedInfo& loaded = m_loadedInfoMap.Insert(index).data; 

	InitLoaded(loaded, index, virtSize, physSize);

	// We're new loaded objects to the tail, so in DeleteLeastUsed we delete oldest first.
	m_loadedInfoList.AddToTail(&loaded); 

	if (m_LastLoadedPrevFrame == NULL)
	{
		m_LastLoadedPrevFrame = &loaded;
	}

	return loaded;
}


void strStreamingInfoManager::RemoveLoaded(strIndex index)
{
	strLoadedInfo* loaded = m_loadedInfoMap.Access(index);
	streamAssertf(loaded, "Trying to remove '%s' (%" STRINDEX_FMT ") from the loaded list, but it's not on there", GetObjectName(index), index.Get());
#if __DEV
	if (loaded == NULL)
	{
		streamWarningf("Trying to remove '%s' (%" STRINDEX_FMT ") from the loaded list, but it's not on there", GetObjectName(index), index.Get());
	}
#endif // __DEV

	if (m_InfoIteratorForDeleteNextLeastUsed.IsValid() && 
		m_InfoIteratorForDeleteNextLeastUsed.Item()->m_Index == index)
	{
		m_InfoIteratorForDeleteNextLeastUsed.Next();
	}

	if (loaded == m_LastLoadedPrevFrame)
	{
		m_LastLoadedPrevFrame = loaded->m_Link.pNext;
	}

	m_loadedInfoList.Remove(loaded);
	m_loadedInfoMap.Delete(index);
}

void strStreamingInfoManager::SnapshotLastLoaded()
{
	m_LastLoadedPrevFrame = m_loadedInfoList.GetLast();
}

strLoadedInfo* strStreamingInfoManager::GetLoaded(strIndex index)
{
	return m_loadedInfoMap.Access(index);
}


strLoadedInfo& strStreamingInfoManager::AddPersistentLoaded(strIndex index, u32 virtSize, u32 physSize)
{
	streamAssert(!m_loadedPersistentInfoMap.Access(index));
	if (!streamVerifyf(!m_loadedInfoPool.IsFull(), "Exceeded maximum loading objects - outputting loaded list to TTY"))
	{
		BANK_ONLY(PrintLoadedList(true, true, false));
		g_strStreamingInterface->DumpStreamingInterests();
		Quitf(ERR_STR_INFO_3,"Please include the loaded list in the bug report");
	}
	strLoadedInfo& loaded = m_loadedPersistentInfoMap.Insert(index).data; 
	InitLoaded(loaded, index, virtSize, physSize);

	return loaded;
}


void strStreamingInfoManager::RemovePersistentLoaded(strIndex index)
{
	streamVerifyf(m_loadedPersistentInfoMap.Delete(index), "Trying to delete %s (%" STRINDEX_FMT ") from the persistent loaded list, but it's not on it", GetObjectName(index), index.Get());
}


strLoadedInfo* strStreamingInfoManager::GetPersistentLoaded(strIndex index)
{
	return m_loadedPersistentInfoMap.Access(index);
}

#if __DEV
void DEBUG_CheckForBreak(strIndex index, s32 /*flags*/)
{
	strLocalIndex txdSlot = g_TxdStore.FindSlot("v_57_frankAunt2txd");

	if (txdSlot != -1)
	{
		strIndex txdStreamIndex = g_TxdStore.GetStreamingModule()->GetStreamingIndex(txdSlot);
		if (index == txdStreamIndex)
		{
			__debugbreak();
		}
	}
}
#endif

bool strStreamingInfoManager::RequestObject(strIndex index, s32 flags)
{
	DIAG_CONTEXT_MESSAGE("Requesting %s", GetObjectName(index));
	//DEBUG_CheckForBreak(index, flags);

#if __ASSERT
	for (int x=0; x<s_StreamablesBeingUnloaded.GetCount(); x++)
	{
		streamAssertf(index != s_StreamablesBeingUnloaded[x], "Requesting %s while it is being unrequested - this can cause major streaming problems", GetObjectName(index));
	}
#endif // __ASSERT

	streamAssert(sysThreadType::IsUpdateThread());

	if (!streamVerifyf(!m_requestInfoPool.IsFull(), "Exceeded maximum number of active requests"))
	{
		BANK_ONLY(PrintRequestList());
		return false;
	}

	streamDebugf3("Request '%" STRINDEX_FMT "' - object %s, flags=%x", index.Get(),GetObjectName(index), flags);

#if SANITY_CHECK_LIST
	SanityCheckInfo(index);
#endif 

	/// streamAssertf(index != CStreamingDebug::GetDebugStreamingObjectIndex(), "Found debug streaming index %d", index);
	streamAssertf(!(flags & STR_PERSISTENT_MASK), "Using non-request flags");

#if !__FINAL
	g_strStreamingInterface->RecordRequest(index);
	REQUESTRECORDER.OnRequest(index, flags);
#endif //!__FINAL

	strStreamingInfo& info = GetStreamingInfoRef(index);
	strResidentStatus status = info.GetStatus();

	//Assertf((!(flags & STRFLAG_LOADSCENE)) || strStreamingEngine::GetLoadSceneActive(), "Requesting %s with a LoadScene flag, but we're not currently doing a LoadScene", GetObjectName(index));
	//Assertf((!(info.GetFlags() & STRFLAG_LOADSCENE)) || strStreamingEngine::GetLoadSceneActive(), "Sanity check: %s has a LoadScene flag, but we're not currently doing a LoadScene", GetObjectName(index));

	// If is in the request list and isn't already a priority then increment priority count and set flag
	if (status == STRINFO_LOADREQUESTED)
	{
		// Are we changing the priority flag?
		if ((flags ^ info.GetFlags()) & STRFLAG_PRIORITY_LOAD)
		{
			streamDebugf3("Priority for the already requested streamable %s has changed", GetObjectName(index));

			// Are we going from prio to normal?
			if (!(flags & STRFLAG_PRIORITY_LOAD))
			{
				// We don't permit downgrading from priority request to normal request. If that happens, keep the existing
				// priority status.
				flags |= STRFLAG_PRIORITY_LOAD;
			}
			else
			{
				// We're upgrading to a priority request.
				// First off, we need to tell StreamingVisualize about that.
				STRVIS_AUTO_CONTEXT(strStreamingVisualize::PRIO_UPGRADE);
				STRVIS_REQUEST(index, flags);

				// Furthermore, we need to update the sorted request list.
				if (!(info.GetFlags() & STRFLAG_INTERNAL_DUMMY))
				{
					int device = info.GetPreferredDevice(index);
					int count = m_SortedPhysicalRequests[device].GetCount();

					strRequestSortEntry *entry = m_SortedPhysicalRequests[device].GetElements();

					for (int x=0; x<count; x++)
					{
						if (entry->m_Index == index)
						{
							// Found it.
							entry->m_Priority = true;
							m_SortedRequestListDirty[device] = true;
							break;
						}

						entry++;
					}
				}

				if (flags & STRFLAG_PRIORITY_LOAD)
				{
					m_numPriorityRequests++;
					STR_TELEMETRY_MESSAGE_LOG("(streaming/request)Bumping up to PRIORITY request %s", GetObjectName(index));
					info.SetFlag(index, STRFLAG_PRIORITY_LOAD, false);
				}
			}
		}
	}	
	// If loaded or loading remove priority flag
	else if (status != STRINFO_NOTLOADED)
	{
		flags &= ~STRFLAG_PRIORITY_LOAD;
	}

	// keep the persistent flags
	flags |= info.GetFlags() & STR_PERSISTENT_MASK;

	if (status != STRINFO_NOTLOADED)
	{
		// if the request is made with additional dontdelete flags, update the object
		u32 dontDeleteFlags = (flags & STR_DONTDELETE_MASK);
		if ( dontDeleteFlags & ~(info.GetFlags() & STR_DONTDELETE_MASK)) {
			SetRequiredFlag(index, dontDeleteFlags);

			streamDebugf3("Required flag for streamable %s has changed", GetObjectName(index));
		}
		info.SetFlag(index, flags);

#if SANITY_CHECK_LIST
		SanityCheckInfo(index);
#endif 
		return true;
	}

	STR_TELEMETRY_MESSAGE_LOG("(streaming/request)Requesting %s (%s)", GetObjectName(index), (flags & STRFLAG_PRIORITY_LOAD) ? "PRIO" : "norm");
	STRVIS_REQUEST(index, flags);

#if STREAMING_DETAILED_STATS
	// Did we just unload it not too long ago?
	u32 currentTimestamp = TIME.GetFrameCount();
	u32 unloadDiff = currentTimestamp - info.GetUnloadedTimestamp();

	if (unloadDiff < CHURN_TRACKING_LENGTH)
	{
		// This request can be considered "churn" - we just unloaded it!
		m_ChurnRequests[unloadDiff]++;
	}
#endif // STREAMING_DETAILED_STATS



	bool isPackfile = info.IsPackfileHandle();

	// if model is not in request list add it in
	if (!isPackfile && !artVerifyf(info.IsInImage(), "Can't find object %s in imgs", GetObjectName(index)))
	{
		if (flags & (STR_FORCELOAD_MASK|STRFLAG_PRIORITY_LOAD))
		{
			streamDebugf2("Request failed for '%" STRINDEX_FMT "' - object %s not available", index.Get(),GetObjectName(index));
		}

#if SANITY_CHECK_LIST
		SanityCheckInfo(index);
#endif 
		return false;
	}

	// Get the dependencies
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	strStreamingModule* pModule = GetModule(index);
	s32 numDeps = pModule->GetDependencies(pModule->GetObjectIndex(index), &deps[0], STREAMING_MAX_DEPENDENCIES);

    // Check they all exist first. 
	for (s32 i = 0; i < numDeps; ++i)
	{
		if(!GetStreamingInfoRef(deps[i]).IsInImage())
		{
#if !__NO_OUTPUT
			char objNameBuffer[256];
			const char *objName = GetObjectName(index, objNameBuffer, sizeof(objNameBuffer));
#endif // !__NO_OUTPUT

			artAssertf(false, "Object <%s> : Can't find dependency <%s> (module:%s) in imgs", objName, GetObjectName(deps[i]), GetModule(deps[i])->GetModuleName());

			if (flags & (STR_FORCELOAD_MASK|STRFLAG_PRIORITY_LOAD))
			{
				streamDebugf2("Request failed for '%s' - dependency '%" STRINDEX_FMT "' : %s not available", objName, deps[i].Get(), GetObjectName(deps[i]));
			}

#if SANITY_CHECK_LIST
			SanityCheckInfo(index);
#endif 
			return false;
		}
	}

	// If object is being forced to load then force loading of dependencies
	// Also dependencies should have priority flag if object has priority flag set
	u32 reqflags = (flags & STRFLAG_PRIORITY_LOAD);

	// We need the archive resident.
	u32 handle = info.GetHandle();

	bool missingArchive = false;
	bool dependenciesMet = true;
	strLocalIndex imageIndex = strLocalIndex(-1);

	if (!isPackfile && strIsValidHandle(handle))
	{
		imageIndex = strPackfileManager::GetImageFileIndexFromHandle(handle);
		if (imageIndex != -1)
		{
			// TODO: Do this only once per file.
			strPackfileManager::AddStreamableReference(imageIndex.Get());

			strIndex archiveStrIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);

			bool archiveIsResident = strPackfileManager::IsImageResident(imageIndex.Get());

			// DO NOT load something from an archive that needs to be reset.
			strStreamingFile *archiveFile = strPackfileManager::GetImageFile(imageIndex.Get());
			if (archiveFile->m_bNeedsReset)
			{
#if !__NO_OUTPUT
				char objNameBuffer[256];
				const char *objName = GetObjectName(index, objNameBuffer, sizeof(objNameBuffer));
#endif // !__NO_OUTPUT

				streamDebugf1("Unable to load %s - archive %s needs to be reset", objName, GetObjectName(archiveStrIndex));
				archiveIsResident = false;

				if (archiveFile->m_requestCount > 1)
				{
					// It's still in use - we can't unload it just yet.
					streamDebugf1("Archive still in use - postponing removal");
					strPackfileManager::RemoveStreamableReference(imageIndex.Get());
					return false;
				}

				// Unload it right now.
				strPackfileManager::GetStreamingModule()->StreamingRemove(imageIndex);
				strPackfileManager::UnloadRpf(imageIndex.Get());
			}

			// Is this a missing dependency?
			missingArchive |= !archiveIsResident;
			dependenciesMet &= archiveIsResident;

			// Count it as a missing dependency if it's not already requested.
#if __BANK
			strStreamingInfo *archiveInfo = GetStreamingInfo(archiveStrIndex);
			if (archiveInfo && archiveInfo->GetStatus() == STRINFO_NOTLOADED /*&& GetRequest(archiveStrIndex) == NULL*/)
			{
				m_numMissingPackfileDpendencies++;
#if __DEV
				AddLogEntry("RPF CACHE MISS", false, s_RPFCacheMissLog, GetObjectName(archiveStrIndex), archiveInfo->ComputeVirtualSize(archiveStrIndex, true), archiveInfo->ComputePhysicalSize(archiveStrIndex, true), GetObjectName(archiveStrIndex));
#endif // __DEV
			}
#endif // __BANK

			STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);

			if (!RequestObject(archiveStrIndex, reqflags /*| STRFLAG_FORCE_LOAD | STR_DONTDELETE_MASK*/))
			{
#if !__NO_OUTPUT
				char objNameBuffer[256];
				const char *objName = GetObjectName(index, objNameBuffer, sizeof(objNameBuffer));
				streamDisplayf("Request failed for '%s' - requesting archive '%" STRINDEX_FMT "' (%s) failed", objName, archiveStrIndex.Get(), GetObjectName(archiveStrIndex));
#endif // !__NO_OUTPUT
#if SANITY_CHECK_LIST
				SanityCheckInfo(index);
#endif 
				strPackfileManager::RemoveStreamableReference(imageIndex.Get());
				return false;
			}
		}
	}

	for (s32 i = 0; i < numDeps; ++i)
	{
		STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);
		if (!RequestObject(deps[i], reqflags))
		{
#if !__NO_OUTPUT
			char objNameBuffer[256];
			const char *objName = GetObjectName(index, objNameBuffer, sizeof(objNameBuffer));
			streamDisplayf("Request failed for '%s' - requesting dependency '%" STRINDEX_FMT "' (%s) failed", GetObjectName(index), deps[i].Get(), objName);
#endif // !__NO_OUTPUT
#if SANITY_CHECK_LIST
			SanityCheckInfo(index);
#endif 

			if (imageIndex != -1)
			{
				strPackfileManager::RemoveStreamableReference(imageIndex.Get());
			}
			return false;
		}

		// Unless this dependency is in memory, we have a missing dependency.
		dependenciesMet &= (GetStreamingInfoRef(deps[i]).GetStatus() == STRINFO_LOADED);
	}

	if (info.IsPackfileHandle())
	{
		int archiveObjIndex = strPackfileManager::GetStreamingModule()->GetObjectIndex(index).Get();

		strPackfileManager::MarkImageQueued(archiveObjIndex, true);
	}

	// set status dependent on whether model should be added to loaded model list
	info.ClearFlags();
	info.SetFlag(index, flags);

	DEV_ONLY(strRequestInfo &request =) AddRequest(index);
#if __DEV
	request.m_StartRequestTime = (missingArchive) ? TIME.GetElapsedTime() : 0.0f;
#endif // __DEV

#if __BANK
	if ((!(flags & STRFLAG_INTERNAL_DUMMY)) || info.IsPackfileHandle())
	{
		m_numTotalRequests++;
#if __DEV
		AddLogEntry("REQUEST", (flags & STRFLAG_PRIORITY_LOAD) != 0, s_RequestLog, GetObjectName(index), info.ComputeVirtualSize(index, true), info.ComputePhysicalSize(index, true));
#endif // __DEV
	}
#endif // __BANK

#if TRACK_PLACE_STATS
	pModule->TrackRequest(info.ComputeVirtualSize(index, true) + info.ComputePhysicalSize(index, true));
#endif // TRACK_PLACE_STATS

	// If we're just switching from NOTLOADED to LOADREQ, now would be a great time to tell StreamingVisualize about all those
	// dependencies.
	if (status == STRINFO_NOTLOADED)
	{
		STRVIS_SEND_DEPENDENCIES(index);
	}

	SetStatus(index, STRINFO_LOADREQUESTED);

#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif
#if SANITY_CHECK_LIST
	SanityCheckInfo(index);
#endif 

	// Dummy requests can be completed immediately.
	if (flags & STRFLAG_INTERNAL_DUMMY)
	{
		if (dependenciesMet)
		{
			// We've verified that all dependencies are met - let's service dummy requests immediately.
			RemoveRequest(index);

			CompleteDummyRequest(index);
			info.ClearFlag(index, STRFLAG_PRIORITY_LOAD);
		}
	}

	return true;
}

#if __BANK
const char *strStreamingInfoManager::RemoveObjectDiagCb(void * indexPtr, void * /*b*/)
{
	static bool nestedCall = false;

	// Prevent endless loop by detecting reentrance.
	if (nestedCall)
	{
		return "[streamError while getting object name]";
	}

	nestedCall = true;
	const char *returnValue = strStreamingEngine::GetInfo().GetObjectName(*(strIndex *) indexPtr);
	nestedCall = false;

	return returnValue;
}
#endif // __BANK


// Remove an object from memory if loaded, or from the request list.
bool strStreamingInfoManager::RemoveObject(strIndex index, bool /*skipCompleteRpfCheck*/)
{
	streamAssert(sysThreadType::IsUpdateThread());
#if SANITY_CHECK_LIST
	SanityCheckInfo(index);
#endif 

	streamAssertf(index != strIndex(strIndex::INVALID_INDEX), "index %" STRINDEX_FMT " is invalid", index.Get());
	/// HACK streamAssertf(index != CStreamingDebug::GetDebugStreamingObjectIndex(), "Found debug streaming index %d", index);
	
#if !__FINAL && !USE_PAGED_POOLS_FOR_STREAMING
	// Got tired of one-off bad asset crashes!
	if (index.Get() >= m_size)
	{
		Assertf(false, "BAD strIndex: %d", index.Get());
		return false;
	}
#endif

	STRINDEX_DEBUG_MANAGER( Remove( index ) );

	strStreamingInfo& info = GetStreamingInfoRef(index);

#if __ASSERT
	for (int x=0; x<s_StreamablesBeingUnloaded.GetCount(); x++)
	{
		streamAssertf(s_StreamablesBeingUnloaded[x] != index, "%s being removed recursively", GetObjectName(index));
	}

	if (IsObjectRequired(index))
	{
		strStreamingModule* pModule = GetModule(index);
		strLocalIndex objIndex = pModule->GetObjectIndex(index);

		streamErrorf("streamError deleting an object, see assert. Dependents:");
		PrintLoadingDependents(index);

		streamAssertf(false, "Trying to delete streamable '%s' (in %s), but it's still required (ref count=%d, dep cont=%d, flags=0x%x). See TTY for more details.",
						GetObjectName(index),pModule->GetModuleName(), pModule->GetNumRefs(objIndex), info.GetDependentCount(), info.GetFlags());
	}
#endif // __ASSERT

	if (info.GetStatus() == STRINFO_NOTLOADED)
	{
		return false;
	}

	STR_TELEMETRY_MESSAGE_LOG("(streaming/remove)Removing %s", GetObjectName(index));
	STRVIS_UNREQUEST(index);
	
#if __BANK
	s_StreamablesBeingUnloaded.Append() = index;
#endif // __BANK

	if (info.IsPackfileHandle())
	{
		int archiveObjIndex = strPackfileManager::GetStreamingModule()->GetObjectIndex(index).Get();
		strPackfileManager::MarkImageQueued(archiveObjIndex, false);
	}

	int status = info.GetStatus();

	streamDebugf2("%d: Removed {%" STRINDEX_FMT "} %s, status=%s, flags=0x%x", fwTimer::GetSystemFrameCount(), index.Get(),
		GetObjectName(index), strStreamingInfo::GetFriendlyStatusName(status), info.GetFlags());
//	streamAssertf(!strStreamingEngine::GetLoadSceneActive() || !info.IsFlagSet(STRFLAG_LOADSCENE),
//		"Removing %s, which is required by the load scene", GetObjectName(index));

	if (status == STRINFO_LOADED || status == STRINFO_LOADING)
	{

#if SANITY_CHECK_REQUESTS
		SanityCheckRequests();
#endif

		// Don't remove objects if there is an object dependent on me.
		if (info.GetDependentCount() > 0)
		{
			streamDisplayf("Trying to remove %s %s but there are objects depending on me", info.GetFriendlyStatusName(), strStreamingEngine::GetObjectName(index));
#if __DEV
			PrintLoadingDependents(index);
#endif // __DEV			
#if __BANK
			s_StreamablesBeingUnloaded.Pop();
#endif // __BANK
			return false;
		}
	}

	if (status == STRINFO_LOADED)
	{
#if !__FINAL
//		safecpy(gRemovedObjName, strStreamingEngine::GetInfo().GetObjectName(index));
#endif 

#if __BANK
		DIAG_CONTEXT_MESSAGE("RemoveObject(%s)", RemoveObjectDiagCb, &s_StreamablesBeingUnloaded[s_StreamablesBeingUnloaded.GetCount()-1], NULL);
#endif // __BANK

#if STREAMING_DETAILED_STATS
		info.MarkUnloadedTimestamp();
#endif // STREAMING_DETAILED_STATS

		strStreamingModule* pModule = GetModuleMgr().GetModuleFromIndex(index);
		strLocalIndex objIndex = pModule->GetObjectIndex(index);

		// have to delete it's clone as well...
#if USE_DEFRAGMENTATION
#if DEBUG_DEFRAG_REFCNT
		if (info.IsFlagSet(STRFLAG_INTERNAL_DEFRAGGING))
		{
			strStreamingEngine::GetDefragmentation()->DumpDefragInfo();
		}
#endif // DEBUG_DEFRAG_REFCNT


		if (info.IsFlagSet(STRFLAG_INTERNAL_DEFRAGGING))
		{
			// If the ref count is 0, then we shouldn't really be defragmenting.
			if(pModule->GetNumRefs(objIndex) == 0)
			{
#if DEBUG_DEFRAG_REFCNT
				strStreamingEngine::GetDefragmentation()->DumpDefragInfo();
#endif // DEBUG_DEFRAG_REFCNT
				streamAssertf(false, "Object %s is being defragged, but the ref count is 0 - something else must have called RemoveRef() too many times, or the defrag system messed up - see TTY for more info.",
					GetObjectName(index));
			}

			const bool bBlockOnGpuMemCpyCompletion = true;
			strStreamingEngine::GetDefragmentation()->Cancel(strIndex(index), bBlockOnGpuMemCpyCompletion);
			streamAssertf(!info.IsFlagSet(STRFLAG_INTERNAL_DEFRAGGING), "Still defragging %" STRINDEX_FMT " (%s) even after canceling", index.Get(), GetObjectName(index));
		}
#endif // USE_DEFRAGMENTATION

		bool isTemp = pModule->RequiresTempMemory(objIndex);

		pgBase* res = NULL;
		datResourceMap map;
		if (!isTemp && info.IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
		{
			res = (pgBase*) pModule->GetDataPtr(objIndex);
			streamAssertf(res, "NULL ptr return for resource from module %s for object index %d", pModule->GetModuleName(), objIndex.Get());
			streamAssertf(dynamic_cast<pgBase*>(res), "Object returned does not derive from pgBase");
			if (res)
			{
				res->RegenerateMap(map);
			}

			strStreamingEngine::GetAllocator().SanityCheck();
			for (int i = 0; i != map.VirtualCount
#if !FREE_PHYSICAL_RESOURCES
				+ map.PhysicalCount
#endif // !FREE_PHYSICAL_RESOURCES			
				; ++i)
			{
				strStreamingEngine::GetAllocator().RemoveFromMemoryUsed(map.Chunks[i].DestAddr);
			}
		}
		else if (!isTemp && !info.IsFlagSet(STRFLAG_INTERNAL_DUMMY) && !info.IsPackfileHandle())
		{
			void* ptr = pModule->GetDataPtr(objIndex);

			if (streamVerifyf(ptr, "Return pointer for %s is NULL", GetObjectName(s_StreamablesBeingUnloaded[s_StreamablesBeingUnloaded.GetCount()-1])))
			{
				Verifyf(strStreamingEngine::GetAllocator().RemoveFromMemoryUsed(ptr), "Unable to remove %p from memory not found Object %s", ptr, GetObjectName(s_StreamablesBeingUnloaded[s_StreamablesBeingUnloaded.GetCount()-1]));
			}
		}

#if USE_DEFRAGMENTATION
		if (pModule->CanDefragment() && !info.IsFlagSet(STRFLAG_INTERNAL_DONT_DEFRAG | STRFLAG_INTERNAL_DUMMY))
			strStreamingEngine::GetDefragmentation()->RemoveResource(pModule->GetResource(objIndex), index);
#endif // USE_DEFRAGMENTATION

		sysMemAllowResourceAlloc++;
		SetStatus(index, STRINFO_NOTLOADED); // Set the flag now, so that in case the remove call needs to allocate streaming memory (I'm looking at you, scaleform) we don't get recursion.
		streamAssert(pModule != NULL);

#if __BANK
		// Useful for tracking leaks in object handling
		//Printf("-=[ Remove Begins %s ]=-\n", strStreamingEngine::GetObjectName(index));
		if(::gStreamingLoaderPreCallback)
		{
			::gStreamingLoaderPreCallback(pModule->GetStreamingModuleId(), objIndex.Get(), false);
		}
#endif	//__BANK

		{
			MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(index));
			pModule->Remove(objIndex);
		}

#if __BANK
		if(::gStreamingLoaderPostCallback)
		{
			::gStreamingLoaderPostCallback(pModule->GetStreamingModuleId(), objIndex.Get(), false);
		}
		// Useful for tracking leaks in object handling
		//Printf("-=[ Remove End %s ]=-\n", strStreamingEngine::GetObjectName(index));
#endif	//__BANK


		streamAssertf(info.GetStatus() == STRINFO_NOTLOADED, "%s changed its status to %s on unload", pModule->GetName(objIndex), strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));
		sysMemAllowResourceAlloc--;

#if __ASSERT && 0 // False positives from multithreaded allocations.
		if (info.IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
		{
			for (int i = 0; i != map.VirtualCount + map.PhysicalCount; ++i)
			{
				TASKLOG_ASSERTF(!strStreamingEngine::GetAllocator().GetSize(map.Chunks[i].DestAddr), "Streaming memory leak - %s [#%d - size %d, 0x%p, virtCt=%d, physCt=%d] - may crash later during defrag - can defrag is %d, DONT_DEFRAG is %d, INTERNAL_DUMMY is %d", 
					GetObjectName(index), i, map.Chunks[i].Size, map.Chunks[i].DestAddr, map.VirtualCount, map.PhysicalCount, pModule->CanDefragment(), info.IsFlagSet(STRFLAG_INTERNAL_DONT_DEFRAG), info.IsFlagSet(STRFLAG_INTERNAL_DUMMY) );
			}
		}
#endif // __ASSERT

#if LIVE_STREAMING
		if (info.IsFlagSet(STRFLAG_INTERNAL_LIVE))
			strStreamingEngine::GetLive().RemovedLiveFile(index);
#endif // LIVE_STREAMING
	}


	if (status == STRINFO_LOADREQUESTED)
	{
		RemoveRequest(index);

		info.ClearFlag(index,STRFLAG_PRIORITY_LOAD, false);

		// Tell the pack file manager that we no longer need the archive this file was in.
		if (!info.IsPackfileHandle())
		{
			s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle()).Get();
			if (imageIndex != -1)
			{
				strPackfileManager::RemoveStreamableReference(imageIndex);
			}
		}
	}
	else if (status == STRINFO_LOADING)
	{
		// If we're already placing it, we have to keep it in LOADING state, or else we might start removing dependencies.
		if (info.GetFlags() & STRFLAG_INTERNAL_PLACING)
		{
			streamDebugf1("%s is being placed - trying to perform a delayed removal", strStreamingEngine::GetObjectName(index));
			strStreamingEngine::GetLoader().CancelRequest(index); 

#if __BANK
			s_StreamablesBeingUnloaded.Pop();
#endif // __BANK

			// We're returning "true" here - that's a bit misleading, and we may reconsider it, but the bottom line is that
			// this resource will be removed in a bit.
			return true;
		}
		else
		{
			// Mark it as "not loaded". It may in theory still be in flight if we were already in the process of streaming it, but we'll kill it immediately.
			strStreamingEngine::GetLoader().CancelRequest(index); 
			RemoveLoading(index);
		}
	}
	else
	{
		if (info.IsFlagSet(STR_DONTDELETE_MASK) || info.GetDependentCount() > 0)
		{
			RemovePersistentLoaded(index);
		}
		else
		{
			RemoveLoaded(index);
		}
	}	

	// set not loaded
	SetStatus(index, STRINFO_NOTLOADED);

#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif 
#if SANITY_CHECK_LIST
	SanityCheckInfo(index);
#endif 

#if __BANK
	s_StreamablesBeingUnloaded.Pop();
#endif // __BANK

	return true;
}

bool strStreamingInfoManager::RemoveObjectAndDependencies(strIndex index, u32 flags)
{
	strIndex deps[STREAMING_MAX_DEPENDENCIES];

	if (IsObjectReadyToDelete(index, flags))
	{		
		if (RemoveObject(index))
		{
			strStreamingModule* pModule = GetModule(index);
			strLocalIndex objIndex = pModule->GetObjectIndex(index);

			s32 count = pModule->GetDependencies(objIndex, &deps[0], STREAMING_MAX_DEPENDENCIES);
			for (int i = 0; i < count; ++i)
			{
				RemoveObjectAndDependencies(deps[i], flags);
			}

			return true;
		}
	}

	return false;
}

void FindDependents(strIndex dependent, atArray<strIndex>& dependents, strIndex objectStrIndex)
{
	if (dependent.IsValid())
	{
		strIndex dependencies[STREAMING_MAX_DEPENDENCIES];
		strStreamingModule* loadedInfoModule = strStreamingEngine::GetInfo().GetModule(dependent);
		s32 numDeps = loadedInfoModule->GetDependencies(loadedInfoModule->GetObjectIndex(dependent), &dependencies[0], STREAMING_MAX_DEPENDENCIES);
		strLocalIndex objectIdx = loadedInfoModule->GetObjectIndex(dependent);

		for (s32 i = 0; i < numDeps; i++)
		{
			if (dependencies[i] == objectStrIndex)
			{
				loadedInfoModule->GetModelMapTypeIndex(objectIdx, dependent);
				dependents.PushAndGrow(dependent, 64);

				break;
			}
		}
	}
}

void strStreamingInfoManager::FindAllDependents(atArray<strIndex>& dependents, strIndex objectStrIndex)
{
	strStreamingInfoIterator it;
	strIndex index;

	while (!it.IsAtEnd())
	{
		index = *it;

		if (IsObjectInImage(index))
			FindDependents(index, dependents, objectStrIndex);

		++it;
	}
}

void strStreamingInfoManager::RemoveObjectAndDependents(strIndex objectStrIndex, eHierarchyModType modType)
{
	atArray<strIndex> dependents;
	strStreamingInfo& objStrInfo = GetStreamingInfoRef(objectStrIndex);
	strStreamingModule* objectModule = strStreamingEngine::GetInfo().GetModule(objectStrIndex);
	strLocalIndex objIndex = objectModule->GetObjectIndex(objectStrIndex);

	if (modType != HMT_ENABLE)
	{
		// Find what objects have objectStrIndex as a dependency...
		FindAllDependents(dependents, objectStrIndex);

		// Remove all dependents...
		for (s32 i = 0; i < dependents.GetCount(); i++)
			RemoveObjectAndDependents(dependents[i], modType);

		// Perform any special steps required for this module before attempting removal..
		objectModule->ModifyHierarchyStatus(objIndex, modType);

		if (!objectStrIndex.IsValid() || !objStrInfo.IsValid())
			return;

		// Cancel any defrag if we are going to remove this item...
	#if USE_DEFRAGMENTATION
		if (objStrInfo.IsFlagSet(STRFLAG_INTERNAL_DEFRAGGING))
			strStreamingEngine::GetDefragmentation()->Cancel(objectStrIndex, true);
	#endif // USE_DEFRAGEMENTATION

		// Attempt to remove this object now that all its dependents should be removed...
		if (IsObjectReadyToDelete(objectStrIndex, 0))
			objectModule->StreamingRemove(objIndex);
		else
		{
			Assertf(false, "Object %s from module %s was not ready for deletion, refs=%i, dependents=%i, foundDeps=%i, flags=%u",
				objectModule->GetName(objIndex), objectModule->GetModuleName(), objectModule->GetNumRefs(objIndex), 
				objStrInfo.GetDependentCount(), dependents.GetCount(), objStrInfo.GetFlags());
		}
	}
	else
	{
		objectModule->ModifyHierarchyStatus(objIndex, modType);
	}
}

void strStreamingInfoManager::DestroyDependentGraphs()
{	
	m_dependentGraphs.Reset();
	m_dependentGraphsLookup.Reset();
}

// Builds reverse order graph of dependents for the given set of archives
void strStreamingInfoManager::CreateDependentsGraph(atArray<s32>& archives)
{
	const s32 MAX_DEP_GRAPHS_SIZE = 16;

	if (archives.GetCount() > 0)
	{
		if (m_dependentGraphs.GetCapacity() != MAX_DEP_GRAPHS_SIZE)
		{
			m_dependentGraphs.Reset();
			m_dependentGraphs.Reserve(MAX_DEP_GRAPHS_SIZE);
		}
		else if (m_dependentGraphs.GetCount() == m_dependentGraphs.GetCapacity())
		{
		#if !__FINAL
			Quitf("CreateDependentsGraph - Increase MAX_DEP_GRAPHS_SIZE current size %i or make sure DestroyDependentGraphs() is called.", 
				MAX_DEP_GRAPHS_SIZE);
		#endif
		}

		atArray<strIndex>& finishedArray = m_dependentGraphs.Append();
		atBinaryMap<bool, u32>* tempMap = NULL;
		atBinaryMap<bool, u32>* frontStrIndMap = NULL;
		atBinaryMap<bool, u32>* backStrIndMap = NULL;
		atBinaryMap<bool, u32> strIndMap1;
		atBinaryMap<bool, u32> strIndMap2;

		finishedArray.ResetCount();
		frontStrIndMap = &strIndMap1;
		backStrIndMap = &strIndMap2;

		// Add all strIndices from the archives to the front map so we can find their dependents
		for (s32 i = 0; i < archives.GetCount(); i++)
		{
			if (archives[i] != -1)
			{
				GetStrIndiciesForArchive(archives[i], *frontStrIndMap);
				m_dependentGraphsLookup.Insert(archives[i], &finishedArray);
			}
		}

		// Add the archives strIndices to the final array since they should be removed last
		for (atBinaryMap<bool, u32>::Iterator frontIter = frontStrIndMap->Begin(); frontIter != frontStrIndMap->End(); frontIter++)
		{
			strIndex currStrIdx = strIndex(frontIter.GetKey());
			strStreamingModule* currModule = strStreamingEngine::GetInfo().GetModule(currStrIdx);
			strLocalIndex currObjIdx = currModule->GetObjectIndex(currStrIdx);

			currModule->GetModelMapTypeIndex(currObjIdx, currStrIdx);

			finishedArray.PushAndGrow(currStrIdx);
		}

		do 
		{
			strStreamingInfoIterator it;
			strIndex index;

			frontStrIndMap->FinishInsertion();

			// Search everything to find dependents of indices in the front map
			while (!it.IsAtEnd())
			{
				index = *it;

				if (IsObjectInImage(index))
				{
					if (index.IsValid())
					{
						strIndex dependencies[STREAMING_MAX_DEPENDENCIES];
						strStreamingModule* loadedInfoModule = strStreamingEngine::GetInfo().GetModule(index);
						s32 numDeps = loadedInfoModule->GetDependencies(loadedInfoModule->GetObjectIndex(index), &dependencies[0], STREAMING_MAX_DEPENDENCIES);
						strLocalIndex objectIdx = loadedInfoModule->GetObjectIndex(index);

						for (s32 i = 0; i < numDeps; i++)
						{
							loadedInfoModule->GetModelMapTypeIndex(objectIdx, index);

							if (frontStrIndMap->Access(dependencies[i].Get()) != NULL)
							{
								backStrIndMap->SafeInsert(index.Get(), true);
							}
						}
					}
				}

				++it;
			}

			// Nothing in the front map has any dependents, safe to finish.
			if (backStrIndMap->GetCount() == 0)
				break;

			backStrIndMap->FinishInsertion();

			// Copy this layer of dependents into the finished array
			for (atBinaryMap<bool, u32>::Iterator backIter = backStrIndMap->Begin(); backIter != backStrIndMap->End(); backIter++)
			{
				finishedArray.PushAndGrow(strIndex(backIter.GetKey()));
			}

			// Swap and reset the maps
			tempMap = frontStrIndMap;
			frontStrIndMap = backStrIndMap;
			backStrIndMap = tempMap;

			backStrIndMap->ResetCount();
		} while (true);
	}
}

void strStreamingInfoManager::GetStrIndiciesForArchive(s32 archiveIndex, atBinaryMap<bool, u32>& strIndicies)
{
	if (archiveIndex != -1)
	{
		strStreamingFile* file = strPackfileManager::GetImageFile((u32)archiveIndex);
		fiPackfile* pack = file ? static_cast<fiPackfile*>(file->m_packfile) : NULL;
		strStreamingModule* currModule = NULL;
		s32 entryCount = -1;

		if (pack && pack->IsInitialized())
			entryCount = pack->GetEntryCount();

		// If the pack file is in memory use its entries immediately, else find all its entries the hard way.
		if (pack && pack->IsInitialized() && pack->GetNameHeap())
		{
			const fiPackEntry* entries = pack->GetEntries();
			char fullName[STORE_NAME_LENGTH] = { 0 };
			char fullNameNoExt[STORE_NAME_LENGTH] = { 0 };
			u32 entryHandle = 0;
			const char* extension = NULL;
			strLocalIndex slotIndex;

			for (s32 i = 0; i < entryCount; ++i) 
			{
				const fiPackEntry& currEntry = entries[i];

				if (!currEntry.IsDir())
				{
					entryHandle = fiCollection::MakeHandle(pack->GetPackfileIndex(), ptrdiff_t_to_int(&currEntry - entries));
					pack->GetEntryFullName(entryHandle, fullName, STORE_NAME_LENGTH);
					extension = ASSET.FindExtensionInPath(fullName);
					currModule = extension ? strStreamingEngine::GetInfo().GetModuleMgr().GetModuleFromFileExtension(++extension) : NULL;

					if (currModule)
					{
						ASSET.RemoveExtensionFromPath(fullNameNoExt, sizeof(fullNameNoExt), fullName);
						slotIndex = currModule->FindSlot(fullNameNoExt);

						strIndex currIndex = currModule->GetStreamingIndex(slotIndex);

						strIndicies.SafeInsert(currIndex.Get(), true);
					}
				}
			}
		}
		else
		{
			strStreamingModuleMgr& mgr = strStreamingEngine::GetInfo().GetModuleMgr();
			s32 moduleCount = mgr.GetNumberOfModules();
			strStreamingInfo* currStrInfo = NULL;
			strLocalIndex currStrFileIdx;
			s32 foundEntries = 0;

			for (s32 i = 0; i < moduleCount; i++)
			{
				currModule = mgr.GetModule(i);

				if (currModule)
				{
					currStrInfo = NULL;

					for (s32 j = 0; j < currModule->GetCount(); j++)
					{
						currStrInfo = currModule->GetStreamingInfo(strLocalIndex(j));

						if (currStrInfo && strIsValidHandle(currStrInfo->m_handle))
						{
							currStrFileIdx = strPackfileManager::GetImageFileIndexFromHandle(currStrInfo->GetHandle());

							if (currStrFileIdx != -1 && file == strPackfileManager::GetImageFile(currStrFileIdx.Get()))
							{
								foundEntries++;

								strIndex currIndex = currModule->GetStreamingIndex(strLocalIndex(j));

								strIndicies.SafeInsert(currIndex.Get(), true);

								// If we know how many entries we are looking for, break when we reach that number...
								if (entryCount != -1 && foundEntries >= entryCount)
								{
									i = moduleCount - 1;
									j = currModule->GetCount() - 1;

									break;
								}
							}
						}
					}
				}
			}
		}
	}
}

// Dump the object and then request it again.
// In case of a load failure.
void strStreamingInfoManager::ReRequestObject(strIndex index)
{
	RemoveObject(index);
	RequestObject(index, GetStreamingInfoRef(index).GetFlags());
}


// Set the state of the object to loading.
void strStreamingInfoManager::SetObjectToLoading(strIndex index)
{
	streamAssert(sysThreadType::IsUpdateThread());

#if __DEV
	streamDebugf2("%d: Loading %s (%" STRINDEX_FMT ")", fwTimer::GetSystemFrameCount(), GetObjectName(index), index.Get());
#endif
	strStreamingInfo& info = GetStreamingInfoRef(index);

#if __DEV || STREAMING_DETAILED_STATS
	strRequestInfo* request = m_requestInfoMap.Access(index);
#if STREAMING_DETAILED_STATS
	u32 startTime = request->m_RequestTime;
	u32 loadTime = sysTimer::GetSystemMsTime();
	int device = info.GetPreferredDevice(index);
	int priority = info.GetPriority();
	m_TotalReqToLoadTime[device][priority] += loadTime - startTime;
	m_ReqToLoads[device][priority]++;
#endif // STREAMING_DETAILED_STATS
#if __DEV
	streamAssertf(request, "Trying to set %s to loading state, but it wasn't even on the request list (state is %s)", GetObjectName(index), 
		strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));
	if (request->m_StartRequestTime != 0.0f) {
		// We had a missing dependency.
		float currentTime = TIME.GetElapsedTime();

		// Ignore numbers skewed by a reset.
		if (currentTime > request->m_StartRequestTime) {
			strPackfileManager::RegisterStreamingDelay(currentTime - request->m_StartRequestTime);
		}
	}

	strPackfileManager::IncrementNumLoads();
#endif // __DEV
#endif // __DEV || STREAMING_DETAILED_STATS


	RemoveRequest(index);
#if STREAMING_DETAILED_STATS
	strLoadingInfo& loading = AddLoading(index);

	loading.m_StartTime = startTime;
	loading.m_LoadTime = loadTime;
	loading.m_IsPriority = (info.GetFlags() & STRFLAG_PRIORITY_LOAD) != 0;
#else // STREAMING_DETAILED_STATS
	// Add this entry to the loading list.
	AddLoading(index);
#endif // STREAMING_DETAILED_STATS

	SetStatus(index, STRINFO_LOADING);
	info.ClearFlag(index, STRFLAG_PRIORITY_LOAD);

#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif
}

// Allow the loader to tell us that an object has loaded.
void strStreamingInfoManager::SetObjectToNotLoadedFromLoading(strIndex index)
{
	strStreamingInfo& info = GetStreamingInfoRef(index);
	ASSERT_ONLY(bool isDummy = (info.GetFlags() & STRFLAG_INTERNAL_DUMMY) != 0;)

	streamAssert(isDummy || info.GetStatus() == STRINFO_LOADING);

	strLoadingInfo *loading = GetLoading(index);

	// It's possible not to have a strLoadingInfo - mostly if this is a dummy request.
	streamAssert(loading || isDummy);
	if (loading)
	{
		streamAssert(!isDummy);
		RemoveLoading(index);
	}

	s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle()).Get();
	if (imageIndex != -1)
	{
		strPackfileManager::RemoveStreamableReference(imageIndex);
	}

	SetStatus(index, STRINFO_NOTLOADED);
}

void strStreamingInfoManager::SetObjectToLoaded(strIndex index)
{
#if STREAMING_DETAILED_STATS
	strLoadingInfo *loading = GetLoading(index);

	u32 startTime = 0;
	u32 loadTime = 0;
	bool isPriority = false;

	// It's possible not to have a strLoadingInfo - mostly if this is a dummy request.
	if (loading)
	{
		startTime = loading->m_StartTime;
		loadTime = loading->m_LoadTime;
		isPriority = loading->m_IsPriority;
	}
#endif // STREAMING_DETAILED_STATS

	strStreamingInfo& info = GetStreamingInfoRef(index);
#if __DEV
	streamDebugf2("%d: Loaded %s", fwTimer::GetSystemFrameCount(), GetObjectName(index));
	LogCompletedObject(GetObjectName(index), info.ComputeVirtualSize(index), info.ComputePhysicalSize(index), info.IsFlagSet(STRFLAG_INTERNAL_DUMMY));
#endif

	if (!info.IsFlagSet(STRFLAG_INTERNAL_DUMMY))
		RemoveLoading(index);

	// It's loaded, so we can clear the FORCE_LOAD flag.
	info.ClearFlag(index, STRFLAG_FORCE_LOAD);

	if (!(info.IsFlagSet(STR_DONTDELETE_MASK)) && info.GetDependentCount() == 0)
	{
		AddLoaded(index);
	}
	else
	{
		AddPersistentLoaded(index);
	}

	if (PARAM_nomtplacement.Get() && info.GetStatus() != STRINFO_LOADREQUESTED && info.GetStatus() != STRINFO_LOADING)
	{
#if __DEV
		streamAssertf(false, "Loading object %" STRINDEX_FMT " - %s that isn't requested anymore (status is %d)", index.Get(), GetObjectName(index), info.GetStatus());
		streamWarningf("Loading object %" STRINDEX_FMT " - %s that isn't requested anymore (status is %d)", index.Get(), GetObjectName(index), info.GetStatus());
#endif // __DEV
	}


	s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle()).Get();
	if (imageIndex != -1)
	{
		strPackfileManager::RemoveStreamableReference(imageIndex);
	}

#if STREAMING_DETAILED_STATS
	if (loading)
	{
		u32 loadedTime = sysTimer::GetSystemMsTime();
		int device = info.GetPreferredDevice(index);
		int priority = (isPriority) ? 1 : 0;

		m_TotalReqToResidentTime[device][priority] += loadedTime - startTime;
		m_TotalLoadToResidentTime[device][priority] += loadedTime - loadTime;

		m_ReqToLoadeds[device][priority]++;
	}
#endif // STREAMING_DETAILED_STATS


	SetStatus(index, STRINFO_LOADED);
}

// Is this a deletable object.
bool strStreamingInfoManager::IsObjectDeletable(strIndex index, u32 ignoreFlags)
{
	return (!GetStreamingInfoRef(index).IsFlagSet(ignoreFlags|STR_DONTDELETE_MASK));
}

// Is this a deletable object.
bool strStreamingInfoManager::IsObjectInUse(strIndex index)
{
	strStreamingModule* pModule = GetModule(index);
	strLocalIndex objIndex = pModule->GetObjectIndex(index);
	return ((GetStreamingInfoRef(index).GetDependentCount() > 0) || (pModule->GetNumRefs(objIndex) > 0));		
}

bool strStreamingInfoManager::IsObjectReadyToDelete(strIndex index, u32 flags)
{
	const strStreamingInfo& info = GetStreamingInfoRef(index);
	if (!info.IsFlagSet(STR_DONTDELETE_MASK|flags) && (info.GetDependentCount() == 0))
	{
		strStreamingModule* pModule = GetModule(index);
		strLocalIndex objIndex = pModule->GetObjectIndex(index);
//		Displayf("strStreamingInfoManager::IsObjectReadyToDelete module:%s obj:%d", pModule->GetModuleName(), objIndex);
		return (pModule->GetNumRefs(objIndex) == 0);
	}
	return false;
}


// Set that the object can not be deleted.
void strStreamingInfoManager::SetRequiredFlag(strIndex index, u32 flag)
{
	streamAssertf(flag & STR_DONTDELETE_MASK, "0x%x is not a required flag", flag);

	strStreamingInfo& info = GetStreamingInfoRef(index);

	if (info.GetStatus() == STRINFO_LOADED && !(info.GetFlags() & STR_DONTDELETE_MASK))
	{
		strLoadedInfo* loaded = GetLoaded(index); 
		if (loaded)
		{
			//if we have a depedentcount we're already in the persistent list
			if(info.GetDependentCount() == 0)
			{
				MoveToPersistentLoaded(index);
			}
		}
	}	

	info.SetFlag(index, flag, false);
}


// Set that this object is not longer required by a mission
void strStreamingInfoManager::ClearRequiredFlag(strIndex index, u32 flag)
{
	streamAssertf(flag & STR_DONTDELETE_MASK, "0x%x is not a required flag", flag);
	streamAssertf((flag & STRFLAG_PRIORITY_LOAD) == 0, "Do NOT use ClearRequiredFlag for STRFLAG_PRIORITY_LOAD, this can cause fatal bookkeeping errors");
//	streamAssertf((flag & ~STR_DONTDELETE_MASK) == 0, "Use this function ONLY for DONTDELETE_MASK flags, not 0x%x", flag);

	strStreamingInfo& info = GetStreamingInfoRef(index);
	// If flag is clear then return immediately to avoid pushing objects to the start of
	// the loaded list
	u32 oldFlags = info.GetFlags();

	// Early out if none of the flags we're trying to clear had been set already.
	if(!(oldFlags & flag))
		return;

	info.ClearFlag(index, flag, false);
	u32 newFlags = info.GetFlags();

	// If we just removed the STR_DONTDELETE_MASK from a loaded object, we need to
	// remove the object from the persistent list.
	if (info.GetStatus() == STRINFO_LOADED)
	{
		// If it's now no longer DONTDELETE...
		if (!(newFlags & STR_DONTDELETE_MASK))
		{
			// But if it was...
			if (oldFlags & STR_DONTDELETE_MASK)
			{
				streamAssert(m_loadedPersistentInfoMap.Access(index)); // Has STR_DONTDELETE_MASK set, but isn't in the list?!

				//move to loaded list if we have no dependents; otherwise stays in persistent
				if(info.GetDependentCount() == 0)
				{
					MoveToLoaded(index);
				}
			}
		}
	}
	else
	{
		if (!(newFlags & (STR_DONTDELETE_MASK|STRFLAG_LOADSCENE|STRFLAG_FORCE_LOAD)))
		{
			RemoveObject(index);
		}
	}
}

int cmpInfo(const void *_a,const void *_b) {
	strRequestInfo* a = *(strRequestInfo**)_a;
	strRequestInfo* b = *(strRequestInfo**)_b;

	u32 offsetA = a->m_PhysicalSortKey;
	u32 offsetB = b->m_PhysicalSortKey;
	if (offsetA > offsetB)
		return +1;
	else if (offsetB > offsetA)
		return -1;
	else
		return 0;
}

/** PURPOSE: This function will first go through all dummy requests, request their dependencies and
 *  insta-serve them if all dependencies are satified.
 *
 *  Then it will create a list of requests that should be streamed. This list will try to minimize seeking,
 *  so the first file will be the closest to the disc head. There will be additional files if they're
 *  right next to the previous file. All requests are guaranteed to have their dependencies satisfied, so it's
 *  safe to issue the request without further checking.
 *
 *  PARAMS:
 *  cdPosn - Position of the drive's disc head.
 *  bUsePriority - If true, only priority requests will be considered. If false, all requests will be checked.
 *                 Note that if this function is called with bUsePriority == true but there aren't any
 *                 priority requests, the function will try again, this time bUsePriority set to false.
 *  indexArary - Array to be filled with requests.
 *  arraySize - Max number of elements that can be stored in indexArray.
 *  deviceMask - Only consider files that are on a device specified in this bitmask.
 *
 *  RETURNS: Number of requests that were written into indexArray.
 */
int strStreamingInfoManager::GetNextFilesOnCdNew(u32 cdPosn, bool bUsePriority, strIndex* indexArray, int arraySize, u32 deviceMask, u32 seekTolerance)
{
#if __STATS && __DEV
	s_GetNextFilesCalls++;
#endif // __STATS && __DEV

	streamAssert(arraySize);
	indexArray[0] = strIndex(strIndex::INVALID_INDEX);
	u32 prevPosnEnd = 0;
	strIndex index;
	u32 posn;
	int numDummyRequests = 0;
	int resultsCount = 0;

//	streamDisplayf("[GetNextFilesOnCd]: %u",cdPosn);

	// Go through request list and find the next file on the Cd and the first file on the Cd

	pgStreamer::Device device = (deviceMask & (1 << pgStreamer::HARDDRIVE)) ? pgStreamer::HARDDRIVE : pgStreamer::OPTICAL;

	// Handle all dummy requests first.
	int dummyCount = m_dummyRequestList.GetCount();

	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	s32 i;

	bool firstTime = g_AREA51_HAS_SORTED[device][(int) bUsePriority] != g_AREA51_FRAME;
	g_AREA51_HAS_SORTED[device][(int) bUsePriority] = g_AREA51_FRAME;

	// Only go through our list of dummies once - they're identical on all devices.
	if (device == 0 && (!g_AREA51 || (firstTime && bUsePriority)))
	{
		ASSERT_ONLY(m_UnrequestsLocked = true;)

		for (int x=0; x<dummyCount; x++)
		{
			// Get the dependencies of this dummy.
			strIndex index = m_dummyRequestList[x];
			strStreamingModule* pModule = GetModuleForDummy(index);
			s32 numDeps = pModule->GetDependencies(pModule->GetObjectIndex(index), &deps[0], STREAMING_MAX_DEPENDENCIES);

			strStreamingInfo& info = GetStreamingInfoRef(index);

			streamAssert(info.GetStatus() != STRINFO_LOADED);	// Should have been removed at this point already.

			strStreamingInfo* pDummyRequest = &info;
			// flag that we have hit a dummy request
			numDummyRequests++;

			bool missingDependency = false;

			// check that everything is loaded not just loading
			for (i = 0; i < numDeps; i++)
			{
				strStreamingInfo &depInfo = GetStreamingInfoRef(deps[i]);
				missingDependency |= (depInfo.GetStatus() != STRINFO_LOADED);

				if (depInfo.GetStatus() == STRINFO_NOTLOADED)
				{
					STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);
	//				streamAssertf(false, "Dependency %s for the requested dummy %s is not on the request list",
	//					strStreamingEngine::GetInfo().GetObjectName(deps[i]), strStreamingEngine::GetInfo().GetObjectName(index));

					u32 flags = info.GetFlags() & STRFLAG_PRIORITY_LOAD;

					RequestObject(deps[i], flags);
				}
			}

			// If all our dependencies are met, we can insta-serve this request.
			if (!missingDependency)
			{
				RemoveRequest(index);

				// Since we removed this request, don't move ahead in the list - this
				// slot has now been filled with a different request.
				x--;
				dummyCount--;

				// if everything is loaded then complete request
				CompleteDummyRequest(index);
				pDummyRequest->ClearFlag(index, STRFLAG_PRIORITY_LOAD);
				numDummyRequests--;

	#if SANITY_CHECK_REQUESTS
				SanityCheckRequests();
	#endif
			}
		}
	}

	// Request all dependencies.
	if (!g_AREA51 || firstTime)
	{
		bool requestedDeps;

		do 
		{
			requestedDeps = false;

			const SortRequestArray &requests = m_SortedPhysicalRequests[device];
			int count = requests.GetCount();

			for (int x=0; x<count; x++)
			{
				// Skip non-priority and deleted requests.
				if (!requests[x].IsDeleted() && (!bUsePriority || requests[x].m_Priority))
				{
					strIndex index = requests[x].m_Index;
					strStreamingInfo& info = GetStreamingInfoRef(index);
					strStreamingModule* pModule = GetModule(index);
					s32 numDeps = pModule->GetDependencies(pModule->GetObjectIndex(index), deps, STREAMING_MAX_DEPENDENCIES);

					// What is it doing in this list if it's not currently requested?
					streamAssertf(info.GetStatus() == STRINFO_LOADREQUESTED, "%s is on the streaming request list, but its status is %s.",
						GetObjectName(index), strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));

					// Check the dependencies.
					// Request dependencies. If object is being forced to load then force loading of dependencies
					// Also dependencies should have priority flag if object has priority flag set
					for (i = 0; i < numDeps; i++)
					{
						strStreamingInfo &depInfo = GetStreamingInfoRef(deps[i]);

						s32 status = depInfo.GetStatus();
						if (status == STRINFO_NOTLOADED)
						{
							STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);
							u32 flags = info.GetFlags() & STRFLAG_PRIORITY_LOAD;
							if (RequestObject(deps[i], flags))
							{
								requestedDeps = true;
							}
						}
					}

					// The image is also a dependency.
					strLocalIndex imageIndex = strLocalIndex(-1);

					if (!info.IsPackfileHandle())
					{
						imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle());
						if (imageIndex != -1)
						{
							STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);
							strIndex archiveStrIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);
							s32 archiveStatus = GetStreamingInfoRef(archiveStrIndex).GetStatus();

							if (archiveStatus == STRINFO_NOTLOADED)
							{
								u32 flags = info.GetFlags() & STRFLAG_PRIORITY_LOAD;
								if (RequestObject(archiveStrIndex, flags))
								{
									requestedDeps = true;
								}
							}
						}
					}
				}
			}

		} while (requestedDeps);
	}

	


	bool needsSorting = NeedsSorting(device);
	EnsureRequestListSorted(device);

	// Let's go through the list.
	int x = 0;
	const SortRequestArray &requests = m_SortedPhysicalRequests[device];
	int count = requests.GetCount();

	// Wait until we find something that is in front of the disc head.
	if (needsSorting)
	{
		while (x < count && requests[x].m_SortKey < cdPosn)
		{
			x++;
		}

		if (x == count)
		{
			// Well, we hit the end of the disc. We need to start at the beginning again.
			x = 0;
		}
	}
	else
	{
		// To ensure a reasonably fair scheduling, we can't always start at 0 -
		// let's start where we began last time.
		x = m_LastGetNextFilesIndex[device];
		
		if (count)
		{
			x %= count;
		}
	}

	// We have our starting location now, but we might need to wrap around the array
	// if we can't find anything past the head of the disc drive.
	int counter = count;

	//STRVIS_SET_MARKER_TEXT("D%d: Real requests: %d, checking prio %d", device, counter, (int) bUsePriority);

	while (counter--)
	{
		// Skip non-priority and deleted requests.
		if (!requests[x].IsDeleted() && (!bUsePriority || requests[x].m_Priority))
		{
			// Is this a suitable candidate?
			strIndex index = requests[x].m_Index;
			//STRVIS_SET_MARKER_TEXT("D%d: %d Checking %s, prio=%d", device, x, GetObjectName(index), (int) requests[x].m_Priority);
			strStreamingInfo& info = GetStreamingInfoRef(index);
			strStreamingModule* pModule = GetModule(index);
			s32 numDeps = pModule->GetDependencies(pModule->GetObjectIndex(index), deps, STREAMING_MAX_DEPENDENCIES);

			// What is it doing in this list if it's not currently requested?
			streamAssertf(info.GetStatus() == STRINFO_LOADREQUESTED, "%s is on the streaming request list, but its status is %s.",
				GetObjectName(index), strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));

			// Check the dependencies.
			bool dependenciesMet = true;

			strLocalIndex imageIndex = strLocalIndex(-1);

			if (!info.IsPackfileHandle())
			{
				imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle());
			}

#if __ASSERT
			u32 sortKey = requests[x].m_SortKey;
#endif // __ASSERT

			// Request dependencies. If object is being forced to load then force loading of dependencies
			// Also dependencies should have priority flag if object has priority flag set
			for (i = 0; i < numDeps; i++)
			{
				strStreamingInfo &depInfo = GetStreamingInfoRef(deps[i]);
#if __ASSERT
				int depImageIndex = strPackfileManager::GetImageFileIndexFromHandle(depInfo.GetHandle()).Get();
				pgStreamer::Device depDevice = depInfo.GetPreferredDevice(deps[i]);
				
				// If these two files are in the same RPF...
				if (s_StreamingCheckSortKeyOrder && imageIndex == depImageIndex && imageIndex.Get() >= 0)
				{
					// Ignore those that are not expected to have proper sorting.
					if (strPackfileManager::IsEnforceLsnSorting(imageIndex.Get()))
					{
						u32 depSortKey = depInfo.GetCdPosn(depDevice == pgStreamer::HARDDRIVE);

						char nameBuffer1[RAGE_MAX_PATH];
						char nameBuffer2[RAGE_MAX_PATH];

						// Make sure the dependency is located BEFORE the file itself.
						streamAssertf(depSortKey <= sortKey || !sortKey || strstr(GetObjectName(deps[i]), "share") || strstr(GetObjectName(deps[i]), "global"),
							"'%s' (%s, index=%" STRINDEX_FMT "), a dependency of '%s' (%s, index=%" STRINDEX_FMT "), is in the same RPF file (%s),"
							"but its sort key is behind the dependency (%d vs %d). That will introduce seek times. Tools should have placed "
							"it differently.",
							GetObjectName(deps[i], nameBuffer1, sizeof(nameBuffer1)), GetModule(deps[i])->GetModuleName(), deps[i].Get(),
							GetObjectName(index, nameBuffer2, sizeof(nameBuffer2)), GetModule(index)->GetModuleName(), index.Get(),
							strPackfileManager::GetImageFile(imageIndex.Get())->m_name.GetCStr(),
							depSortKey, sortKey);
					}
				}
#endif // __ASSERT

				s32 status = depInfo.GetStatus();
				if (status != STRINFO_LOADED)
				{
					if (!g_AREA51 || (status != STRINFO_LOADING || info.GetPreferredDevice(deps[i]) != device))
					{
						// if (!g_AREA51)	 // Calling RequestObject is completely redundant here - we should technically not do it
						{
							STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);
							u32 flags = info.GetFlags() & STRFLAG_PRIORITY_LOAD;
							RequestObject(deps[i], flags);
						}

						dependenciesMet = false;
						//STRVIS_SET_MARKER_TEXT("D%d: Missing dependency %s", device, GetObjectName(deps[i]));
					}
				}
			}

			// The image is also a dependency.
			if (imageIndex != -1)
			{
				STRVIS_AUTO_CONTEXT(strStreamingVisualize::DEPENDENCY);
				strIndex archiveStrIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);
				s32 archiveStatus = GetStreamingInfoRef(archiveStrIndex).GetStatus();
				dependenciesMet &= archiveStatus == STRINFO_LOADED;

				if (archiveStatus != STRINFO_LOADED)
				{
							u32 flags = info.GetFlags() & STRFLAG_PRIORITY_LOAD;
							RequestObject(archiveStrIndex, flags);
						//STRVIS_SET_MARKER_TEXT("D%d: Missing archive %s", device, GetObjectName(archiveStrIndex));
					}
				}

			// We can't begin streaming until all our dependencies are resident.
			if (dependenciesMet)
			{
				posn = info.GetCdPosn();

				// Is this not the first file we added to our list?
				if (needsSorting && prevPosnEnd != 0)
				{
					streamAssert(resultsCount > 0);

					if (posn > prevPosnEnd + seekTolerance)
					{
						// There's a gap - let's stop requesting for now. With any luck, the next
						// frame will have an additional request that fits better.
						//STRVIS_SET_MARKER_TEXT("D%d: LSN threshold exceeded: %d", device, posn - prevPosnEnd);
						break;
					}
				}

				streamAssert(resultsCount < arraySize);
				indexArray[resultsCount++] = index;
				u32 offset = ((info.GetCdByteSize()+2047)>>11);
				prevPosnEnd = posn + offset;

				// Don't write out of bounds of the array!!
				if (resultsCount == arraySize)
				{
					break;
				}
			}
		}

		// Go to the next element. Wrap around if we can't find anything else towards
		// the outer edge of the disc.
		x++;
		x %= count;
	}

	if (count)
	{
		x++;
		x %= count;
	}

	if (!sm_FairHDDScheduler)
	{
		x = 0;
	}

	m_LastGetNextFilesIndex[device] = x;

	ASSERT_ONLY(m_UnrequestsLocked = false;)


	// If we didn't find any priority requests, and there are also no
	// outstanding priority requests, try normal requests then.
	if (bUsePriority /*&& (resultsCount == 0 || (strStreamingEngine::GetBurstMode() */&& resultsCount < arraySize /*&& m_numPriorityRequests == 0*/)
	{
		// We might be waiting for something that is not a priority. SHOULDN'T happen, but does,
		// presumably due to priority requests having non-priority dependencies (which should
		// not happen normally).
		// So let's allow non-priority requests.
		// TODO: This is not a sound way of doing this with the dual-queue mechanism, since the
		// second queue MIGHT have priority requests after all.
		return resultsCount + GetNextFilesOnCdNew(cdPosn, false, indexArray + resultsCount, arraySize - resultsCount, deviceMask, seekTolerance);
	}

	return resultsCount;
}

int strStreamingInfoManager::GetNextRequestsOnCd(u32 cdPosn, bool bUsePriority, strIndex* indexArray, int arraySize, u32 deviceMask, u32 seekTolerance)
{
	PF_FUNC(GetNextRequest);

	int count;

	// Get a list of candidates to stream.
	count = GetNextFilesOnCdNew(cdPosn, bUsePriority, indexArray, arraySize, deviceMask, seekTolerance);

	if (count)
	{
		STR_TELEMETRY_MESSAGE_LOG("(streaming/disc)%s:Disc head at %d, found object %s at %d, chain=%d", (deviceMask & pgStreamer::OPTICAL) ? "ODD" : "HDD", cdPosn, GetObjectName(indexArray[0]), GetStreamingInfoRef(indexArray[0]).GetCdPosn(), count);
	}
	return count;
}

int strStreamingInfoManager::RequestListFunc(const strRequestSortEntry* a, const strRequestSortEntry* b)
{
	if (a->m_SortKey == b->m_SortKey)
	{
		return 0;
	}

	return (a->m_SortKey > b->m_SortKey) ? 1 : -1;
}

class RequestSort
{
public:
	bool operator()(const strRequestSortEntry& a, const strRequestSortEntry& b) const
	{
		return (a.m_SortKey < b.m_SortKey);
	}
};

void strStreamingInfoManager::EnsureRequestListSorted(pgStreamer::Device device)
{
	if (!NeedsSorting(device))
	{
		// Unsorted lists still need to be processed - they need to remove all deleted elements.
		if (m_SortedRequestListDirty[device])
		{
#if __DEV
			m_numListSorts++;
#endif // __DEV

			int count = m_SortedPhysicalRequests[device].GetCount();
			strRequestSortEntry *requests = m_SortedPhysicalRequests[device].GetElements();

			// Remove all the obsolete elements.
			for (int x=0; x<count; x++)
			{
				if (requests[x].IsDeleted())
				{
					m_SortedPhysicalRequests[device].DeleteFast(x);
					x--;
					count--;
				}
			}

			m_SortedRequestListDirty[device] = false;
		}

		return;
	}
	
	if (m_SortedRequestListDirty[device])
	{
#if __DEV
		m_numListSorts++;
#endif // __DEV

		int count = m_SortedPhysicalRequests[device].GetCount();
		strRequestSortEntry *requests = m_SortedPhysicalRequests[device].GetElements();

		std::sort(requests, requests + count, RequestSort());

		// Trim the deleted elements off the end.

		while (count > 0 && m_SortedPhysicalRequests[device][count-1].IsDeleted())
		{
			// Remove the last entry.
			m_SortedPhysicalRequests[device].Pop();
			count--;
		}


		m_SortedRequestListDirty[device] = false;
	}
}

// Return whether the object is in one of the images.
bool strStreamingInfoManager::IsObjectInImage(strIndex index)
{
#if USE_PAGED_POOLS_FOR_STREAMING
	if(m_StreamingInfos.IsAllocated(index))
		return GetStreamingInfoRef(index).IsInImage();
#else // USE_PAGED_POOLS_FOR_STREAMING
	if(m_aInfoForModel.GetCount())
		return GetStreamingInfoRef(index).IsInImage();
#endif // USE_PAGED_POOLS_FOR_STREAMING
	return false;
}

u32 strStreamingInfoManager::GetDependencyDeviceMask(strIndex index) const
{
	u32 result = 0;
	const strStreamingInfo &info = GetStreamingInfoRef(index);

	if (info.GetStatus() == STRINFO_NOTLOADED)
	{
		// Skip dummies.
//		if (!(info.GetFlags() & STRFLAG_INTERNAL_DUMMY))
		if (info.IsValid())
		{
			result |= 1 << info.GetPreferredDevice(index);
		}

		// And now for all the dependencies.
		strIndex deps[256];

		const strStreamingModule *module = GetModule(index);
		strLocalIndex objIndex = module->GetObjectIndex(index);
		int depCount = module->GetDependencies(objIndex, deps, NELEM(deps));

		for (int x=0; x<depCount; x++)
		{
			result |= GetDependencyDeviceMask(deps[x]);
		}
	}

	return result;
}

u8 strStreamingInfoManager::GetModuleId(strIndex index) const
{
	return ms_moduleMgr.GetModuleIdFromIndex(index);
}


strStreamingModule* strStreamingInfoManager::GetModuleForDummy(strIndex index)
{
	// We'll assume that this is an archetype, which is the case in 99% of all cases.
	strStreamingModule *RESTRICT module = fwArchetypeManager::GetStreamingModule();

	// Check to see if this index is within the module's range of strIndices.
	u32 firstIndex = (u32) module->GetStreamingIndex(strLocalIndex(0)).Get();

	if ((u32) index.Get() - firstIndex < (u32) module->GetCount())
	{
		// Yup - we guessed it.
		return module;
	}

	// If not, we'll have to look for it the old-fashioned way.
	return GetModule(index);
}

strStreamingModule* strStreamingInfoManager::GetModule(strIndex index)
{
		return ms_moduleMgr.GetModuleFromIndex(index);
}

const strStreamingModule* strStreamingInfoManager::GetModule(strIndex index) const
{
		return ms_moduleMgr.GetModuleFromIndex(index);
}


//
// name:		strStreamingInfo::IsNew
// description:	Return if object is newer than cache file
bool strStreamingInfoManager::IsObjectNew(strIndex index) 
{
	strStreamingFile* pFile = strPackfileManager::GetImageFileFromHandle(GetStreamingInfoRef(index).GetHandle());
	if (pFile)
	{
		return pFile->m_bNew;
	}
	return true;
}

#if !__NO_OUTPUT
// Return a streaming index by filename
strIndex strStreamingInfoManager::GetStreamingIndexByFilename(const char* filename, s32 module)
{
	strStreamingModule* pModule = ms_moduleMgr.GetModule(module);

	char name[STORE_NAME_LENGTH];
	safecpy(name, filename);
	char* pExt = strrchr(name, '.');
	if (pExt)
	{
		*pExt = '\0';
	}

	for(s32 i=0; i<pModule->GetCount(); i++)
	{
		strIndex index =  pModule->GetStreamingIndex(strLocalIndex(i));

		if (IsObjectInImage(index))
		{
			const char* pName = pModule->GetName(strLocalIndex(i));
			if (!strnicmp(name, pName, STORE_NAME_LENGTH) || !strnicmp(filename, pName, STORE_NAME_LENGTH))
			{
				return index;
			}
		}
	}

	return strIndex(strIndex::INVALID_INDEX);
}
#endif

void strStreamingInfoManager::GetObjectAndDependenciesSizes(strIndex index, u32& virtualSize, u32& physicalSize, const strIndex* ignoreList, s32 numIgnores, bool mayFail)
{
	strIndex backingStore[STREAMING_MAX_DEPENDENCIES];
	atUserArray<strIndex> deps(backingStore, STREAMING_MAX_DEPENDENCIES);

	GetObjectAndDependencies(index, deps, ignoreList, numIgnores);

	for (s32 i = 0; i < deps.GetCount(); ++i)
	{
		virtualSize += GetObjectVirtualSize(deps[i], mayFail);
		physicalSize += GetObjectPhysicalSize(deps[i], mayFail);
	}
}

void strStreamingInfoManager::GetObjectAndDependenciesSizes(const atArray<strIndex>& indices, u32& virtualSize, u32& physicalSize, const strIndex* ignoreList, s32 numIgnores, bool mayFail)
{
#if ENABLE_DEBUG_HEAP
	// Use our debug-only hash map
	// It is supposed to be clean.
	Assert(m_ObjectDependenciesMap.m_Map.GetNumUsed() == 0);

	strIndex depListStore[OBJECT_MAP_MEMORY_POOL_DEFAULT_SIZE];
	atUserArray<strIndex> deps(depListStore, NELEM(depListStore));

	for (int x=0; x<numIgnores; x++)
	{
		m_ObjectDependenciesMap.Insert(ignoreList[x].Get(), ignoreList[x]);
	}

	int indexCount = indices.GetCount();

	for(s32 i = 0; i < indexCount; ++i)
	{
		if (indices[i].IsInvalid())
			continue;

		GetObjectAndDependencies(indices[i], m_ObjectDependenciesMap, deps);
	}

	for (int x=0; x<numIgnores; x++)
	{
		m_ObjectDependenciesMap.Delete(ignoreList[x].Get());
	}

#else
	strIndex backingStore[STREAMING_MAX_DEPENDENCIES];
	atUserArray<strIndex> deps(backingStore, STREAMING_MAX_DEPENDENCIES);

	for(s32 i = 0; i < indices.GetCount(); ++i)
	{
		if (indices[i].IsInvalid())
			continue;

		GetObjectAndDependencies(indices[i], deps, ignoreList, numIgnores);
	}
#endif
	int depCount = deps.GetCount();
	for (s32 i = 0; i < depCount; ++i)
	{
		virtualSize += GetObjectVirtualSize(deps[i], mayFail);
		physicalSize += GetObjectPhysicalSize(deps[i], mayFail);

#if ENABLE_DEBUG_HEAP
		m_ObjectDependenciesMap.Delete(deps[i].Get());
#endif // ENABLE_DEBUG_HEAP
	}
}

void strStreamingInfoManager::GetObjectAndDependencies(strIndex index, atArray<strIndex>& allDeps, const strIndex* ignoreList, s32 numIgnores)
{
	strStreamingModule *pModule = GetModule(index);
	pModule->GetObjectAndDependencies(index, allDeps, ignoreList, numIgnores);
}

#if ENABLE_DEBUG_HEAP
void strStreamingInfoManager::GetObjectAndDependencies(strIndex index, ObjectDependenciesMap& allDepsMap, atArray<strIndex>& allDeps)
{
	strStreamingModule *pModule = GetModule(index);
	pModule->GetObjectAndDependencies(index, allDepsMap, allDeps);
}
#endif

// Get the index of the info that the module uses.
strLocalIndex strStreamingInfoManager::GetObjectIndex(strIndex index) const
{
	const strStreamingModule* pModule = GetModule(index);
	return pModule->GetObjectIndex(index);
}

void* strStreamingInfoManager::GetObjectPtr(strIndex index)
{
	strStreamingModule* pModule = GetModule(index);
	return pModule->GetPtr(pModule->GetObjectIndex(index));
}

strFileHandle strStreamingInfo::GetHandle() const
{
	return GetHandle(m_handle);
}

strFileHandle strStreamingInfo::GetHandle(u32 handle)
{
	if (IsPackfileHandle(handle))
	{
		int archiveIndex = GetPackfileIndex(handle);
		if (archiveIndex == -1)
		{
			return 0;
		}

		streamAssert(strPackfileManager::IsArchiveManaged(archiveIndex));

		strStreamingFile *file = strPackfileManager::GetImageFile(archiveIndex);
		return file->m_StreamerHandle;
	}

	return handle;
}

#if !__NO_OUTPUT
// Get the object name.
const char* strStreamingInfoManager::GetObjectName(strIndex index) const
{
	static char name[255];

	return GetObjectName(index, name, sizeof(name));
}

FASTRETURNCHECK(const char*) strStreamingInfoManager::GetObjectName(strIndex index, char *name, size_t bufferSize) const
{
	if (!index.IsValid())
	{
		return "INVALID";
	}

	const strStreamingInfo& info = GetStreamingInfoRef(index);

	if (strIsValidHandle(info.m_handle) && info.IsPackfileHandle())
	{
		strStreamingFile *file = strPackfileManager::GetImageFile(info.GetPackfileIndex());
		return file->m_name.GetCStr();
	}

	if (info.IsInImage())
	{
		const strStreamingModule* pModule = GetModule(index);
		if (info.IsFlagSet(STRFLAG_INTERNAL_DUMMY))
		{
			formatf(name, bufferSize, "<%s>", pModule->GetName(pModule->GetObjectIndex(index))); 
		}
		else
		{
			formatf(name, bufferSize, "%s.%s", pModule->GetName(pModule->GetObjectIndex(index)), GetModuleMgr().GetFileExtensionFromModule(pModule)); 
		}
	}
	else
	{
		const strStreamingModule* pModule = GetModule(index);
		formatf(name, bufferSize, "<<%s>>", pModule->GetName(pModule->GetObjectIndex(index)));
	}

	return name;
}

const char* strStreamingInfoManager::GetObjectPath(strIndex index) const
{
	static char name[255];

	return GetObjectPath(index, name, sizeof(name));
}

FASTRETURNCHECK(const char*) strStreamingInfoManager::GetObjectPath(strIndex index, char *name, size_t bufferSize) const
{
	if (!index.IsValid())
	{
		return "INVALID";
	}

	const strStreamingInfo& info = GetStreamingInfoRef(index);

	if (strIsValidHandle(info.m_handle) && info.IsPackfileHandle())
	{
		strStreamingFile *file = strPackfileManager::GetImageFile(info.GetPackfileIndex());
		return file->m_name.GetCStr();
	}

	u32 handle = info.GetHandle();
	strLocalIndex imageIndex = strPackfileManager::GetImageFileIndexFromHandle(handle);
	const char* packfileName = "";
	if (imageIndex != -1)
	{
		packfileName = strPackfileManager::GetImageFileNameFromHandle(handle);
	}

	if (info.IsInImage())
	{
		const strStreamingModule* pModule = GetModule(index);
		if (info.IsFlagSet(STRFLAG_INTERNAL_DUMMY))
		{
			formatf(name, bufferSize, "<%s//%s>", packfileName, pModule->GetName(pModule->GetObjectIndex(index))); 
		}
		else
		{
			formatf(name, bufferSize, "%s//%s.%s", packfileName, pModule->GetName(pModule->GetObjectIndex(index)), GetModuleMgr().GetFileExtensionFromModule(pModule)); 
		}
	}
	else
	{
		const strStreamingModule* pModule = GetModule(index);
		formatf(name, bufferSize, "<<%s//%s>>", packfileName, pModule->GetName(pModule->GetObjectIndex(index)));
	}

	return name;
}

#endif

#if SANITY_CHECK_REQUESTS
void strStreamingInfoManager::SanityCheckRequests()
{
	s32 requests = 0;
	s32 priorityRequests = 0;

	RequestInfoList::Iterator it(GetRequestInfoList()->GetFirst());
	while(it.IsValid())
	{
		strStreamingInfo& curr = GetStreamingInfoRef(it.Item()->m_Index);

		streamAssertf(curr.GetStatus() == STRINFO_LOADREQUESTED, "Entry is not requested");
		requests++;
		if (curr.IsFlagSet(STRFLAG_PRIORITY_LOAD))
		{
			priorityRequests++;
		}

		streamAssert(GetRequest(curr.GetIndex()));

		it.Next();
	}

	streamAssertf(requests == m_numRequests, "Number of requests in list (%d) does not match m_numRequests (%d)", requests, m_numRequests);
	streamAssertf(priorityRequests == m_numPriorityRequests,"Number of priority requests in list (%d) does not match m_numRequests (%d)", priorityRequests, m_numPriorityRequests);

	requests = 0;
	priorityRequests = 0;

	for (int i = 0; i < m_size; ++i)
	{
		strStreamingInfo& info = GetStreamingInfoRef(strIndex(i));
	
		if (info.GetStatus() == STRINFO_LOADREQUESTED)
		{
			requests++;

			if (info.IsFlagSet(STRFLAG_PRIORITY_LOAD))
			{
				priorityRequests++;
			}

			streamAssert(GetRequest(info.GetIndex()));
		}
		else
		{
			streamAssert(!GetRequest(info.GetIndex()));
		}
	}

	streamAssertf(requests == m_numRequests, "Number of requests in list (%d) does not match m_numRequests (%d)", requests, m_numRequests);
	streamAssertf(priorityRequests == m_numPriorityRequests,"Number of priority requests in list (%d) does not match m_numRequests (%d)", priorityRequests, m_numPriorityRequests);

	for (int x=0; x<2; x++)
	{
		pgStreamer::Device device = (pgStreamer::Device) x;

		int count = m_SortedPhysicalRequests[device].GetCount();
		strRequestSortEntry *requests = m_SortedPhysicalRequests[device].GetElements();

		for (int y=0; y<count; y++)
		{
			if (!m_SortedPhysicalRequests[device][y].IsDeleted())
			{
				strIndex index = m_SortedPhysicalRequests[device][y].m_Index;
				strStreamingInfo &info = GetStreamingInfoRef(index);
				streamAssertf(info.GetStatus() == STRINFO_LOADREQUESTED, "Status for %s (%" STRINDEX_FMT ") is %s",
					GetObjectName(index), index.Get(), strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));
			}
		}
	}
}
#endif

#if SANITY_CHECK_LIST
void strStreamingInfoManager::SanityCheckList()
{
	for (u32 i = 0; i < m_size; ++i)
	{
		SanityCheckInfo(strIndex(i));
	}
}

void strStreamingInfoManager::SanityCheckInfo(strIndex ASSERT_ONLY(index))
{
#if __ASSERT
	strStreamingInfo& info = GetStreamingInfoRef(index);

	if (info.GetStatus() == STRINFO_NOTLOADED)
	{
		streamAssert(!m_requestInfoMap.Access(index));
		streamAssert(!m_loadingInfoMap.Access(index));
		streamAssert(!m_loadedInfoMap.Access(index));
		streamAssert(!m_loadedPersistentInfoMap.Access(index));
	}
	else if (info.GetStatus() == STRINFO_LOADREQUESTED)
	{
		streamAssert(m_requestInfoMap.Access(index));
		streamAssert(!m_loadingInfoMap.Access(index));
		streamAssert(!m_loadedInfoMap.Access(index));
		streamAssert(!m_loadedPersistentInfoMap.Access(index));

		strRequestInfo* request = GetRequest(index);
		strStreamingInfo& info = GetStreamingInfoRef(index);
		streamAssert(request->m_Index == info.GetIndex());

		pgStreamer::Device device = info.GetPreferredDevice(index);
		int count = m_SortedPhysicalRequests[device].GetCount();
		bool found = false;

		for (int x=0; x<count; x++)
		{
			if (m_SortedPhysicalRequests[device][x].m_Index == index)
			{
				if (!m_SortedPhysicalRequests[device][x].IsDeleted())
				{
					found = true;
					break;
				}
			}
		}

		streamAssert(found == ((info.GetFlags() & STRFLAG_INTERNAL_DUMMY) == 0));	// MUST be in request list if not dummy, MAY NOT be in request list if it's a dummy
	}
	else if (info.GetStatus() == STRINFO_LOADING)
	{
		streamAssert(!m_requestInfoMap.Access(index));
		streamAssert(m_loadingInfoMap.Access(index));
		streamAssert(!m_loadedInfoMap.Access(index));
		streamAssert(!m_loadedPersistentInfoMap.Access(index));
	}
	else if (info.GetStatus() == STRINFO_LOADED)
	{
		streamAssert(!m_requestInfoMap.Access(index));
		streamAssert(!m_loadingInfoMap.Access(index));
		if (!info.IsFlagSet(STR_DONTDELETE_MASK)) {
			streamAssert(m_loadedInfoMap.Access(index));
			streamAssert(!m_loadedPersistentInfoMap.Access(index));
		}
		else
		{
			streamAssert(!m_loadedInfoMap.Access(index));
			streamAssert(m_loadedPersistentInfoMap.Access(index));
		}
	}
#endif // __ASSERT
}
#endif // SANITY_CHECK_LIST


#if __BANK
void strStreamingInfoManager::LogCompletedObject(const char *DEV_ONLY(name), int DEV_ONLY(virtSize), int DEV_ONLY(physSize), bool DEV_ONLY(dummy))
{
#if __DEV
	AddLogEntry("COMPLETED", false, s_CompletedLog, name, virtSize, physSize, dummy?"DUMMY":NULL);
#endif // __DEV
	m_numTotalCompletedFiles++;
}

void strStreamingInfoManager::LogDeletedObject(const char *DEV_ONLY(name), int DEV_ONLY(virtSize), int DEV_ONLY(physSize))
{
#if __DEV
	AddLogEntry("DELETED OBJECT", false, s_DeletedObjectsLog, name, virtSize, physSize);
#endif // __DEV

	m_numTotalDeletedFiles++;
}

void strStreamingInfoManager::LogFailedAllocation(const char *DEV_ONLY(name), int DEV_ONLY(virtSize), int DEV_ONLY(physSize), bool DEV_ONLY(fragmented), bool DEV_ONLY(wasLastAllocPhysical), size_t DEV_ONLY(failedAllocChunkSize))
{
#if __DEV
	char failedAllocString[128];
	if (fragmented)
	{
		formatf(failedAllocString, "FRAGMENTED: %s %4dK", wasLastAllocPhysical ? "PHYS" : "VIRT", failedAllocChunkSize / 1024);
	}
	else
	{
		strStreamingAllocator& allocator = strStreamingEngine::GetAllocator();

#if ONE_STREAMING_HEAP
		formatf(failedAllocString, "OOM: Need   %4dK",
			Max((virtSize + physSize - allocator.GetPhysicalMemoryFree() + 1023) / 1024, 0));
#else // ONE_STREAMING_HEAP
		formatf(failedAllocString, "OOM: Need   %4dK virt, %4dK phys",
			Max((virtSize - allocator.GetVirtualMemoryFree() + 1023) / 1024, 0),
			Max((physSize - allocator.GetPhysicalMemoryFree() + 1023) / 1024, 0));
#endif // ONE_STREAMING_HEAP
	}

	AddLogEntry("FAILED ALLOC", false, s_OutOfMemoryLog, name, virtSize, physSize, failedAllocString);
#endif // __DEV

	m_numTotalOOMFailedLoads++;
}

#endif // __BANK

#if __BANK
void strStreamingInfoManager::PrintRequestList()
{
#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif

	streamDisplayf("************** Currently Requested Object **************");
	RequestInfoList::Iterator it(GetRequestInfoList()->GetFirst());
	while (it.IsValid())
	{
		strIndex index = it.Item()->m_Index;
		strStreamingInfo& info = GetStreamingInfoRef(index);

		streamDisplayf("%s dd:%d ff:%d pl:%d ls:%d mr:%d cr:%d ir:%d dp:%d",	GetObjectName(index),
			info.IsFlagSet(STRFLAG_DONTDELETE),
			info.IsFlagSet(STRFLAG_FORCE_LOAD),
			info.IsFlagSet(STRFLAG_PRIORITY_LOAD),
			info.IsFlagSet(STRFLAG_LOADSCENE),
			info.IsFlagSet(STRFLAG_MISSION_REQUIRED),
			info.IsFlagSet(STRFLAG_CUTSCENE_REQUIRED),
			info.IsFlagSet(STRFLAG_INTERIOR_REQUIRED),
			info.GetDependentCount());

		it.Next();
	}
#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif // SANITY_CHECK_REQUESTS
}

#if __DEV
static void DisplayStreamingLog(strDebugStreamingLog &log, int lines)
{
	// Start at the end - newest ones first.
	int index = log.GetCount()-1;

	while (lines-- > 0)
	{
		if (index >= 0)
		{
			StreamingLogEntry &entry = log[index--];
			grcDebugDraw::AddDebugOutputEx(false,
				(entry.m_Special) ? Color32(1.0f, 0.3f, 0.3f, 1.0f) : Color32(1.0f, 1.0f, 1.0f, 1.0f),
				"%-55s %4dKB %4dKB %-40s",
				entry.m_RscName, entry.m_VirtSize / 1024, entry.m_PhysSize / 1924, entry.m_SecondaryName);
		}
		else
		{
			// Add an empty line so the display doesn't move around as the list fills up.
			grcDebugDraw::AddDebugOutputEx(false, "");
		}
	}
}

void strStreamingInfoManager::DisplayStreamingOverview(int requestLines, int completedLines, int rpfCacheMissLines, int oomLines, int deletedObjectsLines)
{
	grcDebugDraw::AddDebugOutputEx(false, Color32(1.0f, 1.0f, 0.0f, 1.0f), "LAST REQUESTS (Total Count: %d)", m_numTotalRequests);

	DisplayStreamingLog(s_RequestLog, requestLines);

	grcDebugDraw::AddDebugOutputEx(false, Color32(1.0f, 1.0f, 0.0f, 1.0f), "COMPLETED (Total Count: %d)", m_numTotalCompletedFiles);
	DisplayStreamingLog(s_CompletedLog, completedLines);

	grcDebugDraw::AddDebugOutputEx(false, Color32(1.0f, 1.0f, 0.0f, 1.0f), "RPF CACHE MISSES (Total Count: %d)", m_numMissingPackfileDpendencies);
	DisplayStreamingLog(s_RPFCacheMissLog, rpfCacheMissLines);

	grcDebugDraw::AddDebugOutputEx(false, Color32(1.0f, 1.0f, 0.0f, 1.0f), "UNABLE TO STREAM - NO MEMORY (Total Count: %d)", m_numTotalOOMFailedLoads);
	DisplayStreamingLog(s_OutOfMemoryLog, oomLines);

	grcDebugDraw::AddDebugOutputEx(false, Color32(1.0f, 1.0f, 0.0f, 1.0f), "REMOVED OBJECTS TO MAKE ROOM (Total Count: %d)", m_numTotalDeletedFiles);
	DisplayStreamingLog(s_DeletedObjectsLog, deletedObjectsLines);
}
#endif // __DEV

#if STREAMING_DETAILED_STATS && __BANK
static u32 CompReqPercent(u32 totalMilliseconds, u32 requests)
{
	if (!requests)
	{
		return 0;
	}

	return totalMilliseconds / requests;
}
#endif // STREAMING_DETAILED_STATS && __BANK

void strStreamingInfoManager::DisplayRequestStats()
{
	grcDebugDraw::AddDebugOutputEx(false, "Total requests: %d", m_numTotalRequests);
	grcDebugDraw::AddDebugOutputEx(false, "Total real request adds:    %-5d (avg: %4.2f)", m_numRealRequestAdds, m_numAverageRealRequestAdds);
	grcDebugDraw::AddDebugOutputEx(false, "Total real request removes: %-5d (avg: %4.2f)", m_numRealRequestRemoves, m_numAverageRealRequestRemoves);
	grcDebugDraw::AddDebugOutputEx(false, "List sorts:                 %-5d (avg: %4.2f)", m_numListSorts, m_numAverageListSorts);
	grcDebugDraw::AddDebugOutputEx(false, "Current requests: %d", m_numRequests);
	grcDebugDraw::AddDebugOutputEx(false, "Real requests: %d", m_numRealRequests);
	grcDebugDraw::AddDebugOutputEx(false, "Dummy requests: %d", m_numRequests - m_numRealRequests);
	grcDebugDraw::AddDebugOutputEx(false, "Priority requests: %d", m_numPriorityRequests);

#if STREAMING_DETAILED_STATS
	char churnString[192];
	churnString[0] = 0;
	float fTotalRequests = (float) m_numTotalRequests;

	for (int x=0; x<CHURN_TRACKING_LENGTH; x++)
	{
		char substring[128];
		formatf(substring, "%d (%.0f)%% ", m_ChurnRequests[x], float(m_ChurnRequests[x] * 100.0f) / fTotalRequests);
		safecat(churnString, substring);
	}

	grcDebugDraw::AddDebugOutputEx(false, "Streaming churn: %s", churnString);
	for (int x=0; x<STREAMING_SCHEDULER_COUNT; x++)
	{
		grcDebugDraw::AddDebugOutputEx(false, "%s:", strStreamingEngine::GetLoader().GetLoader((strStreamingLoaderManager::Device) x).GetLoaderName());

		grcDebugDraw::AddDebugOutputEx(false, "Average time from request to loading: PRIO %4dms, NORM %4dms",
			CompReqPercent(m_TotalReqToLoadTime[x][1], m_ReqToLoads[x][1]), CompReqPercent(m_TotalReqToLoadTime[x][0], m_ReqToLoads[x][0]));
		grcDebugDraw::AddDebugOutputEx(false, "Average time from loading to loaded:  PRIO %4dms, NORM %4dms",
			CompReqPercent(m_TotalLoadToResidentTime[x][1], m_ReqToLoadeds[x][1]), CompReqPercent(m_TotalLoadToResidentTime[x][0], m_ReqToLoadeds[x][0]));
		grcDebugDraw::AddDebugOutputEx(false, "Average time from request to loaded:  PRIO %4dms, NORM %4dms",
			CompReqPercent(m_TotalReqToResidentTime[x][1], m_ReqToLoadeds[x][1]), CompReqPercent(m_TotalReqToResidentTime[x][0], m_ReqToLoadeds[x][0]));
		grcDebugDraw::AddDebugOutputEx(false, "Completed requests: PRIO %d, NORM %d", m_ReqToLoadeds[x][1], m_ReqToLoadeds[x][0]);
	}
#endif // STREAMING_DETAILED_STATS
}

void strStreamingInfoManager::PrintInformation(int objIndex, strStreamingModule* pModule, bool hasPackfile, strStreamingInfo& info, int indent /*= 0*/)
{
    //create the indent string
    char indentation[80];
    streamAssert(indent < 80);
    indentation[0]='\0';
    for(int i = 0; i < indent; i++)
    {
        indentation[i] = ' ';
    }
    indentation[indent] = '\0';

	strIndex index = pModule->GetStreamingIndex(strLocalIndex(objIndex));

	char refString[32];
	pModule->GetRefCountString(strLocalIndex(objIndex), refString, sizeof(refString));

    streamDisplayf("%s%s : %d{%" STRINDEX_FMT "} %s %dK %dK ref %s %s %s %s %s %s dp:%d %s", 
        indentation,
        pModule->GetModuleName(),
        objIndex,
        index.Get(),
        pModule->GetName(strLocalIndex(objIndex)),
        hasPackfile ? info.ComputeVirtualSize(index, true)>>10 : 0,  
        hasPackfile ? info.ComputePhysicalSize(index, true)>>10 : 0, 
        refString,
        (info.IsFlagSet(STRFLAG_INTERNAL_DUMMY)?"DUMMY ":""),
        (info.IsFlagSet(STRFLAG_DONTDELETE)?"DONTDELETE ":""),
        (info.IsFlagSet(STRFLAG_MISSION_REQUIRED)?"MISSION_REQUIRED ":""),
        (info.IsFlagSet(STRFLAG_CUTSCENE_REQUIRED)?"CUTSCENE_REQUIRED ":""),
        (info.IsFlagSet(STRFLAG_INTERIOR_REQUIRED)?"INTERIOR_REQUIRED ":""),
		info.GetDependentCount(),
        (info.IsFlagSet(STRFLAG_LOADSCENE)?"LOADSCENE ":"")
        );
}

#if __BANK
void strStreamingInfoManager::PrintInformation(strIndex index)
{
	strStreamingModule* pModule = GetModule(index);
	strStreamingInfo& info = GetStreamingInfoRef(index);
	strLocalIndex objIndex = pModule->GetObjectIndex(index);

	char refString[32];
	pModule->GetRefCountString(objIndex, refString, sizeof(refString));

	streamDisplayf("%s : %d{%d} %s %dK %dK ref %s %s %s %s %s %s dp:%d %s", 
		pModule->GetModuleName(),
		pModule->GetObjectIndex(index).Get(),
		objIndex.Get(),
		pModule->GetName(strLocalIndex(objIndex)),
		info.ComputeOccupiedVirtualSize(index, true)>>10,  
		info.ComputePhysicalSize(index, true)>>10, 
		refString,
		(info.IsFlagSet(STRFLAG_INTERNAL_DUMMY)?"DUMMY ":""),
		(info.IsFlagSet(STRFLAG_DONTDELETE)?"DONTDELETE ":""),
		(info.IsFlagSet(STRFLAG_MISSION_REQUIRED)?"MISSION_REQUIRED ":""),
		(info.IsFlagSet(STRFLAG_CUTSCENE_REQUIRED)?"CUTSCENE_REQUIRED ":""),
		(info.IsFlagSet(STRFLAG_INTERIOR_REQUIRED)?"INTERIOR_REQUIRED ":""),
		info.GetDependentCount(),
		(info.IsFlagSet(STRFLAG_LOADSCENE)?"LOADSCENE ":"")
		);
}
#endif // __BANK

void strStreamingInfoManager::PrintInformationAsCSV(int objIndex, strStreamingModule* pModule, bool hasPackfile, strStreamingInfo& info, int indent /*= 0*/)
{
	const int MAX_INDENT = 5;

	//create the indent string
	char indentation[128];

	indent = Min(indent, MAX_INDENT - 1);
	indentation[0]='\0';

	int indCounter = 0;
	for (indCounter = 0; indCounter < MAX_INDENT; indCounter++)
	{
		if (indCounter == indent)
		{
			char paddedName[32];
			formatf(paddedName, "%-22s", pModule->GetModuleName());
			safecat(indentation, paddedName);
		}

		safecat(indentation, ",  ");
	}

	strIndex streamIndex = pModule->GetStreamingIndex(strLocalIndex(objIndex));

	streamDisplayf("%s %5d{%6" STRINDEX_FMT "}, %20s,%4d,%4d,%2d,%s,%s,%s,%s,%s,dp:%d,%s", 
		indentation,
		objIndex,
		streamIndex.Get(),
		pModule->GetName(strLocalIndex(objIndex)),
		hasPackfile ? info.ComputeOccupiedVirtualSize(streamIndex, true)>>10 : 0,  
		hasPackfile ? info.ComputePhysicalSize(streamIndex, true)>>10 : 0, 
		pModule->GetNumRefs(strLocalIndex(objIndex)),
		(info.IsFlagSet(STRFLAG_INTERNAL_DUMMY)?"DUMMY ":""),
		(info.IsFlagSet(STRFLAG_DONTDELETE)?"DONTDELETE ":""),
		(info.IsFlagSet(STRFLAG_MISSION_REQUIRED)?"MISSION_REQUIRED ":""),
		(info.IsFlagSet(STRFLAG_CUTSCENE_REQUIRED)?"CUTSCENE_REQUIRED ":""),
		(info.IsFlagSet(STRFLAG_INTERIOR_REQUIRED)?"INTERIOR_REQUIRED ":""),
		info.GetDependentCount(),
		(info.IsFlagSet(STRFLAG_LOADSCENE)?"LOADSCENE ":"")
		);
}

void strStreamingInfoManager::PrintLoadedListByModule(bool includeDummies, bool requiredOnly, strResidentStatus status)
{
	strStreamingModuleMgr &mgr = strStreamingEngine::GetInfo().GetModuleMgr();

	int moduleCount = mgr.GetNumberOfModules();
	int *counts = Alloca(int, moduleCount);
	sysMemSet(counts, 0, sizeof(int) * moduleCount);

	strStreamingInfoIterator it;

	while (!it.IsAtEnd())
	{
		strIndex index(*it);
		strStreamingInfo& info = GetStreamingInfoRef(index);

		if (info.GetStatus() == status && IsObjectInImage(index))
		{
			if (includeDummies || !info.IsFlagSet(STRFLAG_INTERNAL_DUMMY))
			{
				int moduleIndex = mgr.GetModuleIdFromIndex(index);
				strStreamingModule *pModule = mgr.GetModule(moduleIndex);
				strLocalIndex objIndex = pModule->GetObjectIndex(index);

				if (!requiredOnly || IsObjectRequired(index) || pModule->GetNumRefs(objIndex) > 0)
				{
					streamAssert((u32) moduleIndex < (u32) moduleCount);
					counts[moduleIndex]++;
				}
			}
		}

		++it;
	}

	for (int x=0; x<moduleCount; x++)
	{
		if (counts[x] > 0)
		{
			streamDisplayf("%s: %d", mgr.GetModule(x)->GetModuleName(), counts[x]);
		}
	}
}


void strStreamingInfoManager::PrintLoadedList(bool includeDummies, bool includeDependencies, bool asCSV, bool requiredOnly, strResidentStatus status)
{
	u32 loaded = 0;
	u32 nonDeletable = 0;

	size_t virtualSize = 0;
	size_t physicalSize = 0;

	if (asCSV)
	{
		// Print a header to make it clear in Excel what each column means.
		streamDisplayf("Module,,,,,StrIndex,Name,Virt,Phys,Refs,Flags");
	}

	strStreamingInfoIterator it;

	while (!it.IsAtEnd())
	{
		strIndex index(*it);
		strStreamingInfo& info = GetStreamingInfoRef(index);
		if (info.GetStatus() == status && IsObjectInImage(index))
		{
			if (includeDummies || !info.IsFlagSet(STRFLAG_INTERNAL_DUMMY))
			{
				strStreamingModule* pModule = GetModule(index);
				strLocalIndex objIndex = pModule->GetObjectIndex(index);

				if (!requiredOnly || IsObjectRequired(index) || pModule->GetNumRefs(objIndex) > 0)
				{
					strFileHandle handle = info.GetHandle();
					bool hasPackfile = false;
					if (strIsValidHandle(handle))
					{
						hasPackfile = NULL != fiPackfile::GetCollection(handle);
					}

					if (asCSV)
					{
						PrintInformationAsCSV(objIndex.Get(), pModule, hasPackfile, info);
					}
					else
					{
						PrintInformation(objIndex.Get(), pModule, hasPackfile, info);
					}

					if (includeDependencies)
					{
						PrintDependenciesRecurse(index, (asCSV) ? 1 : 2, asCSV);
					}

					if (pModule->GetNumRefs(objIndex) > 0 || info.IsFlagSet(STR_DONTDELETE_MASK))
					{
						++nonDeletable;
					}
					++loaded;

					virtualSize += hasPackfile ? info.ComputeOccupiedVirtualSize(index, true)>>10 : 0;
					physicalSize += hasPackfile ? info.ComputePhysicalSize(index, true)>>10 : 0;
				}
			}
		}

		++it;
	}

	streamDisplayf("== Count %d, Nondeletable %d, Virtual size %" SIZETFMT "dK, Physical size %" SIZETFMT "dK", loaded, nonDeletable, virtualSize, physicalSize);
	PrintLoadedListByModule(includeDependencies, requiredOnly, status);
}

void strStreamingInfoManager::PrintDependenciesRecurse(strIndex index, int indent, bool asCSV)
{
    strStreamingModule* pModule = GetModule(index);
    streamAssert(pModule);
    strLocalIndex objIndex = pModule->GetObjectIndex(index);

    strIndex deps[ STREAMING_MAX_DEPENDENCIES ];
    int		 depCount = pModule->GetDependencies(objIndex, deps, STREAMING_MAX_DEPENDENCIES);

    for(int dep = 0; dep < depCount; dep++)
    {
        strIndex depidx(deps[dep]);
        strStreamingInfo& depinfo = GetStreamingInfoRef(depidx);
        strStreamingModule* pDepModule = GetModule(depidx);
        int depIndex = pDepModule->GetObjectIndex(depidx).Get();

        strFileHandle handle = depinfo.GetHandle();
        bool hasPackfile = false;
        if (strIsValidHandle(handle))
        {
            hasPackfile = NULL != fiPackfile::GetCollection(handle);
        }

		if (asCSV)
		{
			PrintInformationAsCSV(depIndex, pDepModule, hasPackfile, depinfo, indent);
		}
		else
		{
			PrintInformation(depIndex, pDepModule, hasPackfile, depinfo, indent);
		}
		PrintDependenciesRecurse(depidx, indent+((asCSV) ? 1 : Min(indent, 8)), asCSV);
    }
}

void strStreamingInfoManager::PrintIfDependent(strIndex index, strIndex dependency)
{
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	strStreamingModule* pModule = GetModule(index);
	strLocalIndex objectIdx = pModule->GetObjectIndex(index);
	s32 numDeps = pModule->GetDependencies(objectIdx, &deps[0], STREAMING_MAX_DEPENDENCIES);

	while (numDeps--)
	{
		if (deps[numDeps] == dependency)
		{
			strStreamingInfo &info = GetStreamingInfoRef(index);
			strStreamingModule *module = strStreamingEngine::GetInfo().GetModule(index);
			strLocalIndex objIndex = module->GetObjectIndex(index);
			char refCnt[32];
			module->GetRefCountString(objIndex, refCnt, sizeof(refCnt));
			streamDisplayf("Dependendable %s (%s), flags=0x%x, ref counts=%s, dependents=%d",GetObjectName(index), info.GetFriendlyStatusName(), info.GetFlags(), refCnt, info.GetDependentCount());
			streamDisplayf("-- Dependents for %s", GetObjectName(index));
			PrintAllDependents(index);
			streamDisplayf("-- End Dependents for %s", GetObjectName(index));
			break;
		}
	}
}

void strStreamingInfoManager::PrintLoadingDependents(strIndex index)
{
	if (GetStreamingInfoRef(index).GetDependentCount())
	{
		s32 count = strStreamingEngine::GetLoader().GetLoadingObjectCount();
		for (s32 i = 0; i < count; ++i)
		{
			strIndex loadIndex = strStreamingEngine::GetLoader().GetLoadingObject(i);
			PrintIfDependent(loadIndex, index);
		}
	}
}

void strStreamingInfoManager::PrintAllDependents(strIndex index)
{
	if (GetStreamingInfoRef(index).GetDependentCount())
	{
		strStreamingInfoIterator it;

		while (!it.IsAtEnd())
		{
			strStreamingInfo &info = GetStreamingInfoRef(*it);

			if (info.GetStatus() != STRINFO_NOTLOADED)
			{
				PrintIfDependent(*it, index);
			}

			++it;
		}
	}
}

void strStreamingInfoManager::DebugShowImageContents(strStreamingFile &file)
{
	// Find every object using this image.
	int fileCount = m_size;
	int count = 0;

	grcDebugDraw::AddDebugOutputEx(false, "%-50s %-20s %-20s %-10s", "File", "Size (Virt/Phys)", "Module", "strIndex");

	fiCollection *pack = file.m_packfile;
	
	if (!pack)
	{
		return;
	}

	strStreamingInfoIterator it;

	// Go through every registered object
	while (!it.IsAtEnd())
	{
		strIndex index = *it;
		strStreamingInfo& info = GetStreamingInfoRef(index);

		// Is it part of this image?
		if (strPackfileManager::GetImageFileFromHandle(info.GetHandle()) == &file)
		{
			strStreamingModule *module = GetModule(index);
			FastAssert(module);

			char fileName[128];
			u32 handle = info.GetHandle();
			pack->GetEntryFullName(handle, fileName, sizeof(fileName));

			char sizeSubstring[32];
			formatf(sizeSubstring, "(%dK/%dK)", info.ComputeOccupiedVirtualSize(index) / 1024, info.ComputePhysicalSize(index) / 1024);

			grcDebugDraw::AddDebugOutputEx(false, "%-50s %-20s %-20s %-10d",
				fileName,
				sizeSubstring,
				module->GetModuleName(),
				index.Get()
				);

			count++;
		}

		++it;
	}

	grcDebugDraw::AddDebugOutputEx(false, "File count: %d (total=%d)", count, fileCount);
}

#endif // __BANK

strIndex strStreamingInfoManager::GetStreamingIndexFromMemBlock(sysMemAllocator *allocator, const void *memPtr)
{
	streamAssert(allocator->GetCanonicalBlockPtr(memPtr) == memPtr);
	const u32 userData = allocator->GetUserData(memPtr);
	if (userData >= MEM_FIRST_CUSTOM_USER_DATA)
	{
		return strIndex(strIndex::INVALID_INDEX);
	}

#if USE_PAGED_POOLS_FOR_STREAMING
	return GetStreamingIndexFromUserData(userData);
#else // USE_PAGED_POOLS_FOR_STREAMING
	return strIndex(userData);
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

strIndex strStreamingInfoManager::GetStreamingIndexFromMemBlock(const void *memPtr)
{
	return GetStreamingIndexFromMemBlock(&sysMemAllocator::GetCurrent(), memPtr);
}

#if USE_PAGED_POOLS_FOR_STREAMING
u32 strStreamingInfoManager::CreateUserDataFromStreamingIndex(strIndex index)
{
	return strStreamingEngine::GetInfo().GetStreamingInfoPool().CompressPagedPoolIndex(index) + 1;
}
strIndex strStreamingInfoManager::GetStreamingIndexFromUserData(u32 userData)
{
	return strIndex(strStreamingEngine::GetInfo().GetStreamingInfoPool().DecompressPagedPoolIndex(userData - 1));
}
#else // USE_PAGED_POOLS_FOR_STREAMING
u32 strStreamingInfoManager::CreateUserDataFromStreamingIndex(strIndex index)
{
	return index.Get();
}
strIndex strStreamingInfoManager::GetStreamingIndexFromUserData(u32 userData)
{
	return strIndex(userData);
}
#endif // USE_PAGED_POOLS_FOR_STREAMING

// Remove all the objects that don't have DONTDELETEMASK flags
// set from the request list
bool strStreamingInfoManager::PurgeRequestList(u32 ignoreFlags, bool noDependencies)
{
	bool deleted = false;

#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif

	STRVIS_AUTO_CONTEXT(strStreamingVisualize::REQUESTPURGE);

		if (GetRequestInfoList()->GetCount() == 0)
		{
			return false;
		}

		int requestCount = GetRequestInfoList()->GetCount();
		strIndex *objectsToDeleteStorage = Alloca(strIndex, requestCount);
		atUserArray<strIndex> objectsToDelete(objectsToDeleteStorage, (unsigned short) requestCount);

		RequestInfoList::Iterator it(GetRequestInfoList()->GetFirst());
		while (it.IsValid())
		{
			strIndex index = it.Item()->m_Index;
			objectsToDelete.Append() = index;
			it.Next();
		}

		Assert(requestCount == objectsToDelete.GetCount());

		for (int x=0; x<requestCount; x++)
		{
			strIndex index = objectsToDelete[x];
			strStreamingInfo& info = GetStreamingInfoRef(index);

			if (!(info.IsFlagSet(ignoreFlags)) && (!noDependencies || info.GetDependentCount() == 0))
			{
				deleted |= RemoveObject(index);
	#if STREAM_STATS
				m_Purged++;
	#endif 
			}
		}
#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif

	return deleted;
}


// Remove all the unused objects from memory. Cleans out those models that are 
// cached.
bool strStreamingInfoManager::FlushLoadedList(u32 ignoreFlags)
{
	PF_AUTO_PUSH_DETAIL("strStreamingInfoManager::FlushLoadedList");
#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif

	bool removedObjectThisCycle = true;
	while (m_loadedInfoMap.GetNumUsed() && removedObjectThisCycle) 
	{
		removedObjectThisCycle = false;
		LoadedInfoList::Iterator it(GetLoadedInfoList()->GetFirst());
		while (it.IsValid())
		{
			strIndex index = it.Item()->m_Index;
			it.Next();

			strStreamingInfo& info = GetStreamingInfoRef(index);

			strStreamingModule* pModule = GetModule(index);
			strLocalIndex objIndex = pModule->GetObjectIndex(index);

			// If reference count is zero and object doesn't have flags set then remove it
			if (pModule->GetNumRefs(objIndex) == 0 && !(info.IsFlagSet(ignoreFlags)))
			{
				removedObjectThisCycle |= RemoveObject(index);
			}
		}
	}
#if SANITY_CHECK_REQUESTS
	SanityCheckRequests();
#endif

	return (m_loadedInfoMap.GetNumUsed() == 0);
}

void strStreamingInfoManager::ResetInfoIteratorForDeletingNextLeastUsed() 
{ 
	m_InfoIteratorForDeleteNextLeastUsed.SetTo(GetLoadedInfoList()->GetFirst());
}

bool strStreamingInfoManager::DeleteNextLeastUsedObject(u32 ignoreFlags, strStreamingModule *ignoreModule, bool useTerminator)
{
	ignoreFlags |= STR_DONTDELETE_MASK;

	bool deleted = false;

	strIndex backingStore[STREAMING_MAX_DEPENDENCIES*2];
	atUserArray<strIndex> indices(backingStore, STREAMING_MAX_DEPENDENCIES*2);

	STRVIS_AUTO_CONTEXT(strStreamingVisualize::DELETENEXTUSED);

	strLoadedInfo *terminator = m_LastLoadedPrevFrame;

	while (m_InfoIteratorForDeleteNextLeastUsed.IsValid())
	{
		strLoadedInfo *loadedInfo = m_InfoIteratorForDeleteNextLeastUsed.Item();

		// Don't delete anything that was added this frame. It likely didn't
		// have a chance to add a ref count yet, so we'd be causing churn.
		if (useTerminator && loadedInfo == terminator)
		{
			return false;
		}

		strIndex index = loadedInfo->m_Index;
		strStreamingModule* pModule = loadedInfo->GetModule();
		m_InfoIteratorForDeleteNextLeastUsed.Next();

		if (pModule != ignoreModule)
		{
			strStreamingInfo& info = GetStreamingInfoRef(index);

			if (!(info.IsFlagSet(ignoreFlags) || info.GetDependentCount()))
			{
				strLocalIndex objIndex = pModule->GetObjectIndex(index);
				if (!pModule->GetNumRefs(objIndex))
				{
					streamDebugf3("%d: Delete %s", fwTimer::GetSystemFrameCount(), GetObjectName(index));

					indices.ResetCount();
					pModule->GetObjectAndDependencies(index, indices, NULL, 0);
					const u32 count = indices.GetCount();
					for (u32 i = 0; i < count; ++i)
					{
						strIndex currentIndex = indices[i];
						
						if (IsObjectReadyToDelete(currentIndex, ignoreFlags))
						{
	#if __DEV
							strStreamingInfo& currentInfo = GetStreamingInfoRef(currentIndex);
							if (!currentInfo.IsFlagSet(STRFLAG_INTERNAL_DUMMY) || currentInfo.IsPackfileHandle())
							{
								LogDeletedObject(GetObjectName(currentIndex), currentInfo.ComputeOccupiedVirtualSize(currentIndex), currentInfo.ComputePhysicalSize(currentIndex));
							}
	#endif // __DEV

							if (RemoveObject(currentIndex))
							{
								deleted = true;
	#if STREAM_STATS
								m_Deleted++;
								m_DeletedSize += info.ComputeOccupiedVirtualSize(currentIndex, true) + info.ComputePhysicalSize(currentIndex, true);
	#endif // STREAM_STATS
							}
						}
					}
					
					if (deleted)
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

#if __DEV
void strStreamingInfoManager::DumpResidentObjects(u32 flagCheck)
{
	int totalVirt = 0;
	int totalPhys = 0;

	LoadedInfoList::Iterator it(GetLoadedInfoList()->GetFirst());
	while (it.IsValid())
	{
		strIndex index = it.Item()->m_Index;
		strStreamingInfo& info = GetStreamingInfoRef(index);

		streamDebugf1("%-30s,%5x,%4dK,%4dK,%s %s",
			GetObjectName(index),
			info.GetFlags(),
			info.ComputeOccupiedVirtualSize(index) / 1024,
			info.ComputePhysicalSize(index) / 1024,
			(strStreamingEngine::GetInfo().IsObjectInUse(index)) ? "IN USE" : "available",
			info.IsFlagSet(flagCheck) ? "FLAGS MISMATCH" : "");

		totalVirt += info.ComputeOccupiedVirtualSize(index);
		totalPhys += info.ComputePhysicalSize(index);

		it.Next();
	}

	streamDebugf1("%-30s,%5s,%4dK,%4dK", "TOTAL", "", totalVirt / 1024, totalPhys / 1024);
}
#endif // __DEV

const atArray<s32>* strStreamingInfoManager::GetPackfileRelationships(s32 imgIndex)
{
	if(atArray<s32>** retVal = m_packFileRelationshipMap.Access(imgIndex))
	{
		return *retVal;
	}
	return NULL;
}

#if __BANK
void strStreamingInfoManager::PrintPackfileRelationships()
{
	atMap<s32,atArray<s32>*>::Iterator mapIter = m_packFileRelationshipMap.CreateIterator();
	for(mapIter.Start();!mapIter.AtEnd();++mapIter)
	{
		strStreamingFile* currentFile = strPackfileManager::GetImageFile(mapIter.GetKey());
		Displayf("Associated files for %s",currentFile->m_name.GetCStr());
		for(int i=0;i<(*mapIter)->GetCount();i++)
		{
			currentFile = strPackfileManager::GetImageFile((**mapIter)[i]);
			Displayf("%d : %s",i,currentFile->m_name.GetCStr());
		}
		Assertf((*mapIter)->GetCount()<48,"SANITY CHECK - Seems like we've got a lot of packfile associations for this archive, something might be off, check log for details");

	}
}

atString StringToLower(const char* str)
{
	atString result(str);

	for (char* ptr = const_cast<char*>(result.c_str()); *ptr; ptr++)
	{
		*ptr = char( tolower(*ptr) );
	}

	return result;
}

void strStreamingInfoManager::ValidateRelationships()
{
	atMap<s32,atArray<s32>*>::Iterator mapIter = m_packFileRelationshipMap.CreateIterator();
	atMap<s32, bool> validatedMap;
	atArray<atString> ignoreFolders;

	ignoreFolders.PushAndGrow(atString("vehiclemods"));
	ignoreFolders.PushAndGrow(atString("generic"));
	ignoreFolders.PushAndGrow(atString("vehicles.rpf"));

	for(mapIter.Start();!mapIter.AtEnd();++mapIter)
	{
		atArray<s32>* relationShipsPtr = (*mapIter);

		if (relationShipsPtr)
		{
			for (s32 i = 0; i < relationShipsPtr->GetCount(); i++)
			{
				s32 imgIdx = (*relationShipsPtr)[i];

				if (imgIdx != -1 && validatedMap.Access(imgIdx) == NULL)
				{
					strStreamingFile* currStrFile = strPackfileManager::GetImageFile(imgIdx);
					const char* pPackFileName = currStrFile->m_name.GetCStr();
					char levelDataSubStr[256] = { "/levels/" };
					atString currPackFileSubPath = StringToLower(strstr(pPackFileName, levelDataSubStr));
					bool shouldIgnore = false;

					for (s32 j = 0; j < ignoreFolders.GetCount(); j++)
					{
						if (strstr(currPackFileSubPath.c_str(), ignoreFolders[j].c_str()) != NULL)
						{
							shouldIgnore = true;
							break;
						}
					}

					if (shouldIgnore)
						continue;

					// Check to ensure that level data overlays laid out in the same physical structuring
					if (currPackFileSubPath.length() > 0)
					{
						if (const atArray<s32>* associatedFiles = strStreamingEngine::GetInfo().GetPackfileRelationships(imgIdx))
						{
							bool foundIssues = false;

							for (s32 k = 0; k < associatedFiles->GetCount(); k++)
							{
								s32 associatedImgIdx = (*associatedFiles)[k];

								validatedMap.Insert(associatedImgIdx, true);

								if (strStreamingFile* strFile = strPackfileManager::GetImageFile(associatedImgIdx))
								{
									atString associatedPackFileSubPath = StringToLower(strstr(strFile->m_name.GetCStr(), levelDataSubStr));

									for (s32 j = 0; j < ignoreFolders.GetCount(); j++)
									{
										if (strstr(associatedPackFileSubPath.c_str(), ignoreFolders[j].c_str()) != NULL)
										{
											shouldIgnore = true;
											break;
										}
									}

									if (shouldIgnore)
										continue;

									if (associatedPackFileSubPath.length() <= 0 || currPackFileSubPath != associatedPackFileSubPath)
									{
										foundIssues = true;
										break;
									}
								}
							}

							if (foundIssues)
							{
								Displayf("ValidateRelationships - Found %i rpfs whose associations may be invalid", associatedFiles->GetCount());

								for (s32 k = 0; k < associatedFiles->GetCount(); k++)
								{
									s32 associatedImgIdx = (*associatedFiles)[k];

									if (strStreamingFile* strFile = strPackfileManager::GetImageFile(associatedImgIdx))
										Displayf("%i : %s", k, strFile->m_name.GetCStr());
								}

								Assertf(false, "ValidateRelationships - Found issues! Please check the output before this assert!");
							}	
						}
					}
				}
			}
		}
	}
}
#endif //__BANK

void strStreamingInfoManager::AddPackfileRelation(s32 oldFile, s32 newFile)
{
	if(oldFile==-1||newFile==-1)
	{
		Assertf(0, "Attempting to relate non-existant packfiles");
		return;
	}
	atArray<s32>* currentArray = NULL;
	atArray<s32>** oldFileArray = m_packFileRelationshipMap.Access(oldFile);
	atArray<s32>** newFileArray = m_packFileRelationshipMap.Access(newFile);
	if(oldFileArray&&newFileArray)
	{
		Assertf((*oldFileArray)&&(*newFileArray),"Packfile relationships map is broken");
		if(*oldFileArray!=*newFileArray)
		{
			//Arrays exist but are not associated at all, associate them
			atArray<s32>* arrayToDelete = NULL;
			if((*oldFileArray)->GetCount()>(*newFileArray)->GetCount())
			{
				arrayToDelete = *newFileArray;
				currentArray = *oldFileArray;
			}
			else
			{
				arrayToDelete = *oldFileArray;
				currentArray = *newFileArray;
			}
			for(int i=0;i<arrayToDelete->GetCount();i++)
			{
				currentArray->PushAndGrow((*arrayToDelete)[i]);
				m_packFileRelationshipMap[(*arrayToDelete)[i]]=currentArray;
			}
			arrayToDelete->Reset();
			delete arrayToDelete;
		}
		return;
	}
	if(oldFileArray)
	{
		//Old file has associations, assign old file into that set
		currentArray = *oldFileArray;
		currentArray->PushAndGrow(newFile);
		m_packFileRelationshipMap.Insert(newFile,currentArray);
		return;
	}
	if(newFileArray)
	{
		//New file has associations, assign old file into that set
		currentArray = *newFileArray;
		currentArray->PushAndGrow(oldFile);
		m_packFileRelationshipMap.Insert(oldFile,currentArray);
		return;
	}
	//No associations exist at this point, create them
	currentArray = rage_new atArray<s32>();
	currentArray->PushAndGrow(oldFile);
	currentArray->PushAndGrow(newFile);
	m_packFileRelationshipMap.Insert(oldFile,currentArray);
	m_packFileRelationshipMap.Insert(newFile,currentArray);
}

// Register a Dvd file, as part of an image or as a separate live file.
// Returns streaming index, -1 if unsuccessful.
strIndex strStreamingInfoManager::RegisterObject(const char* filename, strFileHandle handle, s32 imgIndex, bool& bEncrypted, bool NOTFINAL_ONLY(quitOnBadVersion),fiPackEntry *peOverride,bool overlayLooseFile)
{
	const char* extension = strrchr(filename, '.');

	bool isPackfile = strStreamingInfo::IsPackfileHandle(handle);
	u32 streamerHandle = strStreamingInfo::GetHandle(handle);

	strStreamingModule* pModule = NULL;
	if (extension) {
		++extension;
		pModule = ms_moduleMgr.GetModuleFromFileExtension(extension);
	}
	if (!pModule)
	{
#if !__FINAL
		if (!PARAM_dontwarnonmissingmodule.Get() && ms_moduleMgr.IsModuleRequiredForFileExtension(extension))
		{
			streamErrorf("strStreamingModule for file (%s) with ext '%s' not found", filename, extension);
		}
#endif
		return strIndex(strIndex::INVALID_INDEX);
	}

#if !__FINAL
	if (!streamVerifyf(strlen(filename) <= STORE_NAME_LENGTH, "%s:Filename name is longer than %d characters", filename, STORE_NAME_LENGTH))
	{
		return strIndex(strIndex::INVALID_INDEX);
	}
#endif // !__FINAL

	
	size_t length = extension - filename - 1;
	char name[STORE_NAME_LENGTH];

 	strncpy(name, filename, length);
 	name[length] = '\0';

	streamFatalAssertf(streamerHandle != 0xFFFFFFFF, "Bad streamer handle for %s", filename);

	strIndex index = strIndex(strIndex::INVALID_INDEX);
	if (streamerHandle == pgStreamer::Error) {
		return index;
	}

	const fiCollection* packfile = fiPackfile::GetCollection(streamerHandle);

	if (!packfile)
		return index;

	const fiPackEntry &pe = peOverride? *peOverride : packfile->GetEntryInfo(streamerHandle);
	if (pModule != NULL)
	{
#if !__FINAL
		if (!isPackfile)
		{
			if (!packfile)
			{
				streamAssertf(false, "Packfile %s for required file %s is missing", strPackfileManager::GetImageFile(fiPackfile::GetCollectionIndex(streamerHandle))->m_name.GetCStr(), filename);
				return strIndex(strIndex::INVALID_INDEX);
			}

			int expectedResourceVersion = pModule->GetExpectedRscVersion();
			int resourceVersion = expectedResourceVersion? pe.GetVersion() : 0;
			bool matched = (resourceVersion == expectedResourceVersion);
			// This is pretty bad news, any attempt to ignore it is likely to lead to a crash:
			if (!matched)
			{
				if (quitOnBadVersion)
				{
					Quitf("Code is expecting version %d but file %s has version %d!",expectedResourceVersion,filename,resourceVersion);
				}
				else
				{
					streamErrorf("Code is expecting version %d but file %s has version %d!",expectedResourceVersion,filename,resourceVersion);
				}

				return strIndex(strIndex::INVALID_INDEX);
			}
		}
#endif

		strLocalIndex objectindex = pModule->Register(name);
		if (objectindex != -1)
		{
			index = pModule->GetStreamingIndex(objectindex);
#if !USE_PAGED_POOLS_FOR_STREAMING
			streamAssert(index.Get() < m_size);
#endif // USE_PAGED_POOLS_FOR_STREAMING
		}
		else
		{
			return strIndex(strIndex::INVALID_INDEX);
		}

		if(pModule->GetStreamerReadFlags() & pgStreamer::ENCRYPTED)
			bEncrypted = true;
	}
	else
	{
		//streamWarningf("No valid module found for %s", filename);
		return strIndex(strIndex::INVALID_INDEX);
	}

	streamDebugf3("strStreamingInfoManager::RegisterObject Index %" STRINDEX_FMT " is %s, handle %x", index.Get(), filename, handle);
	
	// If modelinfo already has CD information it must have appeared before in the GTA3.DIR file
	strStreamingInfo& info = GetStreamingInfoRef(index);

	bool registeredBefore = false;


	strLocalIndex originalIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle());
	if (!isPackfile)
	{
 		if (info.IsInImage())
 		{
 			if (info.GetHandle() != handle || ms_moduleMgr.GetModuleIdFromIndex(index) != pModule->GetStreamingModuleId())
 			{
				bool legitOverlay = false;
				strStreamingFile* originalFile = strPackfileManager::GetImageFileFromHandle(info.GetHandle());
				strStreamingFile* newFile = strPackfileManager::GetImageFile(imgIndex);

				if (originalFile||overlayLooseFile)
				{
					// If this is an overlay, we'll have to keep track of the change.
					Assert(!(info.GetFlags() & STRFLAG_INTERNAL_DUMMY));

					if (newFile->m_IsOverlay||overlayLooseFile)
					{
						Assert(ms_moduleMgr.GetModuleIdFromIndex(index) == pModule->GetStreamingModuleId());

						u32 *prevHandle = m_OverlayTable.Access(index);
						if(newFile->m_IsPatchfile&&originalFile->m_IsPatchfile)
						{
							Warningf("Attempted double overlay of patch file %s, ignoring %s, over %s",GetObjectName(index),
								strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr(),
								strPackfileManager::GetImageFile(originalIndex.Get())->m_name.GetCStr());
							AddPackfileRelation(imgIndex,originalIndex.Get());
							return strIndex(strIndex::INVALID_INDEX);
						}

						if (prevHandle)
						{
							if( strPackfileManager::GetImageFile(imgIndex)->m_name.IsNotNull() && 
								Verifyf(strnicmp(strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr(),"dlc",3)==0 || 
									   strnicmp(strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr(),"update",6)==0	, 
								"File %s is being overlaid by packfile %s - but it's already been overlaid by %s, original in %s, Double overlays should only be permitted for DLC",
								GetObjectName(index), 
								strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr(),
								originalFile->m_name.GetCStr(),
								strPackfileManager::GetImageFile((*prevHandle) >> 16)->m_name.GetCStr()))
							{
#if __BANK
								if (!ms_bValidDlcOverlay)
								{
									Assertf(0,"File %s is being overlaid by DLC without using the proper setup, this is not good",strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr());
									Quitf("File %s is being overlaid by DLC without using the proper setup, this is not good",strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr());
								}
#endif // __BANK
								m_OverlayTable.Delete(index);
							}
							else
							{
								return strIndex(strIndex::INVALID_INDEX);
							}
						}

						// If this file is an overlay and it's still in memory try to flush out now...
						if (info.GetStatus() != STRINFO_NOTLOADED)
							RemoveObjectAndDependents(index, HMT_FLUSH);

						Assertf(info.GetStatus() == STRINFO_NOTLOADED, "File %s is being overlaid while not completely unloaded.",
							GetObjectName(index));

#if STORE_FILE_SIZES
						// Remember the original file size.
						{
							USE_DEBUG_MEMORY();
							u32 oldVirtSize = info.ComputeVirtualSize(index, true);
							u32 oldPhysSize = info.ComputePhysicalSize(index, true);
							m_OverlayFileSizeTable.Insert(index, std::pair<u32, u32>(oldVirtSize, oldPhysSize));
						}
#endif // STORE_FILE_SIZES

						m_OverlayTable.Insert(index, info.GetHandle());
						streamDebugf3("RPF Overlay: File %s, packfile %s to %s",
							GetObjectName(index), 
							originalFile->m_name.GetCStr(),
							strPackfileManager::GetImageFile(imgIndex)->m_name.GetCStr());

						legitOverlay = true;
						registeredBefore = true;
						if(newFile->m_IsPatchfile)
							AddPackfileRelation(imgIndex,originalIndex.Get());
					}
#if __ASSERT
					else
					{
						Assertf(!ms_bValidDlcOverlay, "Attempting to overlay %s.%s (from packfile %s) with %s but it is an invalid overlay (overlay flag not set?)" ,name,extension,originalFile->m_name.GetCStr(),newFile->m_name.GetCStr());
					}
#endif // __ASSERT
				}

				if (!legitOverlay)
				{
		#if __ASSERT
					if ( !originalFile )
					{
						artAssertf(false, "[ART] %s.%s is registered with the same index as another object (%" STRINDEX_FMT "), both appearing in %s", name, extension, index.Get(), newFile->m_name.GetCStr());
					}
					else
					{
						if (originalFile->m_extraDataIndex == newFile->m_extraDataIndex)
							artAssertf(false, "[ART] %s.%s appears in %s and %s (previous handle=%x, new handle=%x)", name, extension, newFile->m_name.GetCStr(), originalFile->m_name.GetCStr(), info.GetHandle(), handle);

					}
		#endif // __ASSERT
 					return strIndex(strIndex::INVALID_INDEX);
				}
 			}
 			else
 			{
 				registeredBefore = true;
			}
 		}

	}

 	// info.SetCdImage(static_cast<u8>(imgIndex));
	info.m_handle = handle;
	
#if STORE_FILE_SIZES
	if (!isPackfile)
	{
		if (pe.IsResource())
		{
			info.SetVirtualSize((u32)pe.u.resource.m_Info.GetVirtualSize());
			info.SetPhysicalSize((u32)pe.u.resource.m_Info.GetPhysicalSize());
		}
		else
		{
			info.SetVirtualSize((u32)pe.GetUncompressedSize());
			info.SetPhysicalSize(0);
		}

	}
#endif // STORE_FILE_SIZES

	if (!registeredBefore)
	{
		Assert(!(info.GetFlags() & STRFLAG_PRIORITY_LOAD));

		info.ClearFlags();
	}

	if (!isPackfile && pe.IsResource())
		info.SetFlag(index, STRFLAG_INTERNAL_RESOURCE);
	
#if DEBUG_NAMES
	info.SetObjectName(filename);
#endif // DEBUG_NAMES

#if STREAMING_VISUALIZE
	u32 virtSize = 0;
	u32 physSize = 0;
	u32 fileSize = 0;
	strIndex parentIndex;

	if (!isPackfile)
	{
		if (pe.IsResource())
		{
			virtSize = (u32)pe.u.resource.m_Info.GetVirtualSize();
			physSize = (u32)pe.u.resource.m_Info.GetPhysicalSize();
		}
		else
		{
			virtSize = (u32)pe.GetUncompressedSize();
		}
		fileSize = (u32)pe.GetConsumedSize();

		strLocalIndex imageIndex = strLocalIndex(strPackfileManager::GetImageFileIndexFromHandle(handle));

		if (imageIndex != -1)
		{
			parentIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);
		}
	}
	else
	{
		strStreamingFile *archive = strPackfileManager::GetImageFile(info.GetPackfileIndex());
		fileSize = virtSize = (u32) archive->m_CachedHeaderSize;
	}

	STRVIS_REGISTER_OBJECT(index, pModule->GetStreamingModuleId(), filename, virtSize, physSize, fileSize, parentIndex);
#endif // STREAMING_VISUALIZE

	STRINDEX_DEBUG_MANAGER( Add( index, strIndexDebugManager::TYPE_FILE, filename ) );

	return index;
}


// Register a dummy object.
strIndex strStreamingInfoManager::RegisterDummyObject(const char* name, strLocalIndex objectIndex, s32 module)
{
	strStreamingModule* pModule = ms_moduleMgr.GetModule(module);
#if USE_PAGED_POOLS_FOR_STREAMING
	pModule->AllocateSlot(objectIndex);
#endif // USE_PAGED_POOLS_FOR_STREAMING

	strIndex index = pModule->GetStreamingIndex(objectIndex);
#if !USE_PAGED_POOLS_FOR_STREAMING
	streamAssert(index.Get() < m_size);
#endif // !USE_PAGED_POOLS_FOR_STREAMING

	STRINDEX_DEBUG_MANAGER( Add( index, strIndexDebugManager::TYPE_DUMMY, name ) );

	strStreamingInfo& info = GetStreamingInfoRef(index);

	if(info.IsFlagSet(STRFLAG_INTERNAL_DUMMY))
	{
		streamAssertf(ms_moduleMgr.GetModuleIdFromIndex(index) == module, "%s registered as a dummy with a different module id", name);
		return index;
	}

	// If modelinfo already has CD information it must have appeared or been registered before
	if (info.IsInImage())
	{
		streamErrorf("The dummy object '%s' already exists as a separate object '%s' inside image '%s'", name, GetObjectName(index), strPackfileManager::GetImageFileFromHandle(GetStreamingInfoRef(index).GetHandle())->m_name.GetCStr());
		return strIndex(strIndex::INVALID_INDEX);
	}

	info.ClearFlags();
	info.SetFlag(index, STRFLAG_INTERNAL_DUMMY);

#if DEBUG_NAMES
	info.SetObjectName(name);
#endif // DEBUG_NAMES

#if STORE_FILE_SIZES
	info.SetVirtualSize(0);
	info.SetPhysicalSize(0);
#endif // STORE_FILE_SIZES

	streamDebugf3("Registered dummy object %s (%s), strIndex %" STRINDEX_FMT, name, pModule->GetModuleName(), index.Get());

	STRVIS_REGISTER_OBJECT(index, pModule->GetStreamingModuleId(), name, 0, 0, 0, strIndex());

	(void)name;

	return index;	
}

bool strStreamingInfoManager::UnregisterObject(strIndex index)
{
#if !USE_PAGED_POOLS_FOR_STREAMING
	streamAssert(index.Get() < m_size);
#endif // !USE_PAGED_POOLS_FOR_STREAMING
	strStreamingInfo& info = GetStreamingInfoRef(index);
	if (streamVerifyf(info.GetStatus() == STRINFO_NOTLOADED, "Can't unregister an object (%s) that is in use - %s", 
				GetObjectName(index), 
				info.GetFriendlyStatusName(info.GetStatus())))
	{
		streamDebugf3("Unregistered strIndex %" STRINDEX_FMT, index.Get());
		info.Init();
		STRVIS_UNREGISTER_OBJECT(index);
		return true;
	}

	return false;
}

// Call this on a requested dummy request to insta-serve it immediately.
// All dependencies are expected to be already met at this point.
// Dummy request must be requested already, but it's OK for it to be loaded already.
void strStreamingInfoManager::ForceLoadDummyRequest(strIndex index)
{
	strStreamingInfo& info = GetStreamingInfoRef(index);
	streamAssertf(info.IsFlagSet(STRFLAG_INTERNAL_DUMMY), "Asset %s is supposed to be a dummy, but doesn't have that flag set (0x%x)", GetObjectName(index), info.GetFlags());

	if (info.GetStatus() == STRINFO_LOADED)
	{
		return;
	}

	streamAssert(info.GetStatus() != STRINFO_NOTLOADED);	// It's expected to be requested already at this point

#if __ASSERT
	// Don't call this if the dependencies aren't met!
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	strStreamingModule* pModule = GetModule(index);
	strLocalIndex objIndex = pModule->GetObjectIndex(index);
	s32 numDeps = pModule->GetDependencies(objIndex, &deps[0], STREAMING_MAX_DEPENDENCIES);
	for (s32 i = 0; i < numDeps; ++i)
	{
		int status = GetStreamingInfoRef(deps[i]).GetStatus();
		streamAssertf(status == STRINFO_LOADED, "Trying to complete dummy request %s, but dependency %s isn't loaded yet (status=%s).",
			GetObjectName(index), GetObjectName(deps[i]), strStreamingInfo::GetFriendlyStatusName(status));
	}
#endif // __ASSERT

	if (info.GetStatus() == STRINFO_LOADREQUESTED)
	{
		RemoveRequest(index);
	}

	CompleteDummyRequest(index);
	info.ClearFlag(index, STRFLAG_PRIORITY_LOAD);
}

// Before we process any proper requests check that any dummy requests can be
// completed.
void strStreamingInfoManager::CompleteDummyRequest(strIndex index)
{
#if __BANK
	static strIndex s_DummyCompleted;

	s_DummyCompleted = index;
	DIAG_CONTEXT_MESSAGE("CompleteDummyRequest(%s)", RemoveObjectDiagCb, &s_DummyCompleted, NULL);
#endif // __BANK

	USE_MEMBUCKET(MEMBUCKET_RESOURCE);
#if __ASSERT
	strStreamingInfo& info = GetStreamingInfoRef(index);
	streamAssert(info.IsFlagSet(STRFLAG_INTERNAL_DUMMY));
#endif

	// double check.
	ASSERT_ONLY(strIndex deps[STREAMING_MAX_DEPENDENCIES]);
	strStreamingModule* pModule = GetModuleForDummy(index);
	strLocalIndex objIndex = pModule->GetObjectIndex(index);

#if __BANK
	// Useful for tracking leaks in object handling
	//Printf("-=[ Complete Dummy Request Begins %s ]=-\n", pModule->GetName(objIndex));
	if(::gStreamingLoaderPreCallback)
	{
		::gStreamingLoaderPreCallback(pModule->GetStreamingModuleId(), objIndex.Get(), true);
	}
#endif	//__BANK

#if __ASSERT
	s32 numDeps = pModule->GetDependencies(objIndex, &deps[0], STREAMING_MAX_DEPENDENCIES);
	for (int i = 0; i < numDeps; ++i)
	{
		streamAssert(GetStreamingInfoRef(deps[i]).GetStatus() == STRINFO_LOADED);
	}
#endif

	SetObjectToLoaded(index);

	bool success = pModule->Load(objIndex, NULL, 0);

	// Load file
	if (!success)
	{
		// Skip the assert if this is the archetype module - that one is guaranteed to assert in
		// Load() if something goes wrong, so if we assert again with a generic message, we'll just
		// piss everybody off.
		streamAssertf(pModule == fwArchetypeManager::GetStreamingModule(), "Loading object '%s' failed", pModule->GetName(objIndex));
	}

#if __BANK
	if(success)
	{
		if(::gStreamingLoaderPostCallback)
		{
			::gStreamingLoaderPostCallback(pModule->GetStreamingModuleId(), objIndex.Get(), true);
		}
	}
	// Useful for tracking leaks in object handling
	//Printf("-=[ Complete Dummy Request Ends %s ]=-\n", pModule->GetName(objIndex));
#endif	//__BANK


}

s32 strStreamingInfoManager::GetRequestInfoPoolCapacity() const
{
	return fwConfigManager::GetInstance().GetSizeOfPool(ATSTRINGHASH("MaxLoadRequestedInfo",0x41663795), DEFAULT_MAX_ACTIVE_REQUEST_INFO);
}

void strStreamingInfoManager::GetFileList(int packfileIndex, FileInfoList &outFileList)
{
	strStreamingInfoIterator it;

	while (!it.IsAtEnd())
	{
		strStreamingInfo *info = GetStreamingInfo(*it);

		// Get the archive index the quick way. We don't need to sanity-check and look for "0" or something -
		// the value that's being passed in here will never be 0.
		if (!info->IsPackfileHandle())
		{
			u32 handle = info->GetHandle();
			int index = fiCollection::GetCollectionIndex(handle);

			if (index == packfileIndex)
			{
				// We need to store this one.
				FileInfo &fileInfo = outFileList.Grow(32);
				fileInfo.m_Index = *it;
				//fileInfo.m_Filename = GetObjectName(fileInfo.m_Index);
				// GetObjectName() is not available in all builds, so we'll have to do it the hardcore way.
				char filename[256];
				fiCollection::GetCollection(handle)->GetEntryFullName(handle, filename, (int) sizeof(filename));

				fileInfo.m_Filename = filename;
			}
		}

		++it;
	}
}

void strStreamingInfoManager::RemountFileList(int packfileIndex, const FileInfoList &fileList)
{
	int count = fileList.GetCount();

	for (int x=0; x<count; x++)
	{
		strIndex index = fileList[x].m_Index;
		strStreamingInfo &info = GetStreamingInfoRef(index);

		Assertf(info.GetStatus() == STRINFO_NOTLOADED || info.GetStatus() == STRINFO_LOADREQUESTED, "Remapping %s to a new packfile, but it's currently resident", fileList[x].m_Filename.c_str());

		const char *filename = fileList[x].m_Filename.c_str();

		fiCollection *packfile = fiCollection::GetCollection(info.GetHandle());
		char mappedName[RAGE_MAX_PATH];

		if (fiCollection::GetCollectionIndex(info.GetHandle()) != packfileIndex)
		{
			packfile->FixRelativeName(mappedName, sizeof(mappedName), filename);
			filename = mappedName;
		}

		// Create a new handle for this file.
		// Note that our filename doesn't contain the packfile's prefix, so we'll need to fake it via the relative offset. Yes, we're passing down
		// a pointer to memory BEFORE our name, but GetEntryIndex() always adds the relative offset up front, so we're good.
		int relativeOffset = 0;
		if (packfile->IsRpf())
		{
			relativeOffset = static_cast<fiPackfile *>(packfile)->GetRelativeOffset();
		}
		int entryIndex = packfile->GetEntryIndex(filename - relativeOffset);

		if (Verifyf(entryIndex != -1, "Can't find file '%s' when remapping '%s' - is it not in the new packfile %p?", filename, static_cast<fiPackfile *>(packfile)->GetDebugName(), packfile))
		{
			u32 newHandle = fiCollection::MakeHandle(packfileIndex, entryIndex);
			info.SetHandle(newHandle);
			streamDebugf3("Remounting file (%" STRINDEX_FMT ") '%s' to handle %x", index.Get(), filename, newHandle);
		}
		else
		{
			streamDebugf3("Can't find file (%" STRINDEX_FMT ") '%s' inside %s", index.Get(), filename, packfile->GetDebugName());
			// Invalidate the file so we don't try to load it.
			info.SetHandle(0);
		}
	}

}

void strStreamingInfoManager::ModifyHierarchyStatusForArchive(s32 archiveIndex, eHierarchyModType modType)
{
	if (archiveIndex != -1)
	
	{
		if (modType == HMT_ENABLE)
		{
			atBinaryMap<bool, u32> archStrIndices;

			GetStrIndiciesForArchive(archiveIndex, archStrIndices);

			archStrIndices.FinishInsertion();

			for (atBinaryMap<bool, u32>::Iterator iter = archStrIndices.Begin(); iter != archStrIndices.End(); iter++)
			{
				strIndex currStrIdx = strIndex(iter.GetKey());

				if (strStreamingModule* objectModule = strStreamingEngine::GetInfo().GetModule(currStrIdx))
				{
					strLocalIndex objIndex = objectModule->GetObjectIndex(currStrIdx);

					objectModule->ModifyHierarchyStatus(objIndex, modType);
				}
			}
		}
		else
		{
			atArray<strIndex>** invalidationGraph = m_dependentGraphsLookup.Access(archiveIndex);

			if (Verifyf(invalidationGraph, "ModifyHierarchyStatusForArchive - No invalidation graph for archive! %s", 
				strPackfileManager::GetImageFile(archiveIndex)->m_name.GetCStr()))
			{
				atArray<strIndex>& invalGraphRef = **invalidationGraph;

				for (s32 i = invalGraphRef.GetCount() - 1; i >= 0; i--)
				{
					strStreamingInfo& objStrInfo = GetStreamingInfoRef(invalGraphRef[i]);
					strStreamingModule* objectModule = strStreamingEngine::GetInfo().GetModule(invalGraphRef[i]);
					strLocalIndex objIndex = objectModule->GetObjectIndex(invalGraphRef[i]);

					strStreamingEngine::GetLoader().CallKeepAliveCallbackIfNecessary();

					// Perform any special steps required for this module before attempting removal..
					objectModule->ModifyHierarchyStatus(objIndex, modType);

					if (!invalGraphRef[i].IsValid() || !objStrInfo.IsValid())
						continue;
					//the background script index has it's rpf deleted and the collections slot will subsequently be reused
					//we need to ignore it otherwise it will think it belongs to the wrong rpf
					if(invalGraphRef[i] == m_bgScriptIdx)
						continue;

					// Cancel any defrag if we are going to remove this item...
				#if USE_DEFRAGMENTATION
					if (objStrInfo.IsFlagSet(STRFLAG_INTERNAL_DEFRAGGING))
						strStreamingEngine::GetDefragmentation()->Cancel(invalGraphRef[i], true);
				#endif // USE_DEFRAGEMENTATION

					// Attempt to remove this object now that all its dependents should be removed...
					if (IsObjectReadyToDelete(invalGraphRef[i], 0))
						objectModule->StreamingRemove(objIndex);
					else
					{
						Assertf(false, "Object %s from module %s was not ready for deletion, refs=%i, dependents=%i, flags=%u",
							objectModule->GetName(objIndex), objectModule->GetModuleName(), objectModule->GetNumRefs(objIndex), 
							objStrInfo.GetDependentCount(), objStrInfo.GetFlags());
					}
				}
			}
		}
	}
}

void strStreamingInfoManager::InvalidateFilesForArchive(s32 archiveIndex, bool unregister)
{
	// We're gonna have to walk through every file and look for this particular archive - there are
	// no mappings going the other way.
	strStreamingInfoIterator it;

	bool isOverlay = strPackfileManager::GetImageFile(archiveIndex)->m_IsOverlay;

	while (!it.IsAtEnd())
	{
		strStreamingInfo *info = GetStreamingInfo(*it);

		// Get the archive index the quick way. We don't need to sanity-check and look for "0" or something -
		// the value that's being passed in here will never be 0.
		int index = fiCollection::GetCollectionIndex(info->GetHandle());
		//the background script index has it's rpf deleted and the collections slot will subsequently be reused
		//we need to ignore it otherwise it will think it belongs to the wrong rpf
		if(m_bgScriptIdx == *it)
		{
			++it;
			continue;
		}
		if (index == archiveIndex)
		{
			// Here's one. We have to trash it.
			Assertf(info->GetStatus() != STRINFO_LOADING /*&& info->GetStatus() != STRINFO_LOADREQUESTED */,
				"We're invalidating %s, but %s is still being streamed in.",
				strPackfileManager::GetImageFile(index)->m_name.GetCStr(), GetObjectName(*it));
#if __ASSERT
			if (info->GetStatus() == STRINFO_LOADED)
			{
				char numRefStr[32];
				strIndex stIndex = strIndex(*it);
				strStreamingModule *module = GetModule(stIndex);
				strLocalIndex objIndex = module->GetObjectIndex(stIndex);
				const char *refCntString = module->GetRefCountString(objIndex, numRefStr, sizeof(numRefStr));
				strStreamingInfo &info = GetStreamingInfoRef(stIndex);

				Assertf(false, "We're invalidating %s, but %s is still resident. All files in this RPF need to be evicted first. Flags=%x, refCnt=%s",
				strPackfileManager::GetImageFile(index)->m_name.GetCStr(), GetObjectName(stIndex), info.GetFlags(), refCntString);
			}
#endif // __ASSERT

			bool mustInvalidate = true;

			if (isOverlay)
			{
				u32 *prevHandle = m_OverlayTable.Access(*it);

				if (prevHandle)
				{
					streamDebugf3("Undoing overlay for %s", GetObjectName(*it));

#if __BANK
					strStreamingFile* pOverlay= strPackfileManager::GetImageFileFromHandle(info->GetHandle());
					strStreamingFile* pOriginal = strPackfileManager::GetImageFileFromHandle(*prevHandle);
					if(pOriginal->m_device == NULL)
					{
						strIndex stIndex = strIndex(*it);
						fiPackfile* packOr = pOriginal ? static_cast<fiPackfile*>(pOriginal->m_packfile) : NULL;
						fiPackfile* packOv = pOverlay ? static_cast<fiPackfile*>(pOverlay->m_packfile) : NULL;
						Errorf("We're invalidating *%s (%d) (packfile: %s, idx: %d), but it looks like it is reverting to a disabled packfile %s, %d. See url:bugstar:5686706",
							GetObjectName(stIndex), stIndex.Get(), packOv ? packOv->GetFullName().c_str() : "unavailable", info->GetHandle() >> 16, packOr ? packOr->GetFullName().c_str() : "unavailable", *prevHandle >> 16);
						Assertf(false, "We're invalidating *%s (%d) (packfile: %s, idx: %d), but it looks like it is reverting to a disabled packfile %s, %d. See url:bugstar:5686706",
							GetObjectName(stIndex), stIndex.Get(), packOv ? packOv->GetFullName().c_str() : "unavailable", info->GetHandle() >> 16, packOr ? packOr->GetFullName().c_str() : "unavailable", *prevHandle >> 16);
#if !__NO_OUTPUT
 						Quitf("We're invalidating *%s (%d) (packfile: %s, idx: %d), but it looks like it is reverting to a disabled packfile %s, %d. See url:bugstar:5686706",
 							GetObjectName(stIndex), stIndex.Get(), packOv ? packOv->GetFullName().c_str() : "unavailable", info->GetHandle() >> 16, packOr ? packOr->GetFullName().c_str() : "unavailable", *prevHandle >> 16);
#endif // !__NO_OUTPUT
					}
#endif //__BANK

					info->SetHandle(*prevHandle);

					// Restore the file sizes, if we care. This is a debugging function.
#if STORE_FILE_SIZES
					std::pair<u32, u32> *fileSizeInfo = m_OverlayFileSizeTable.Access(*it);
					Assert(fileSizeInfo);

					info->SetVirtualSize(fileSizeInfo->first);
					info->SetPhysicalSize(fileSizeInfo->second);

					{
						USE_DEBUG_MEMORY();
						m_OverlayFileSizeTable.Delete(*it);
					}
#endif // STORE_FILE_SIZES

					m_OverlayTable.Delete(*it);
					mustInvalidate = false;
				}
			}

			if (mustInvalidate)
			{
				if (unregister)
				{
					streamDebugf3("Unregistering file %s", GetObjectName(*it));
					strStreamingModule *module = GetModule(*it);
					strLocalIndex objIndex = module->GetObjectIndex(*it);

					module->RemoveSlot(objIndex);
				}
				else
				{
					streamDebugf3("Invalidating file %s", GetObjectName(*it));
					info->SetHandle(0);
				}
			}
		}

		++it;
	}
}

void strStreamingInfoManager::UpdateLoadedInfo(strIndex index, size_t virtSize, size_t physSize)
{
	strLoadedInfo* loaded = m_loadedInfoMap.Access(index); 
	if(!loaded)
	{
		loaded = m_loadedPersistentInfoMap.Access(index);
		Assertf(loaded, "Updating loaded info of %s, which isn't actually loaded", GetObjectName(index));	 // This object is not currently loaded
	}

	loaded->SetVirtualSize((u32) virtSize);
	loaded->SetPhysicalSize((u32) physSize);
}



// Clear a flag for all infos.
void strStreamingInfoManager::ClearFlagForAll(u32 flag)
{
	strStreamingInfoIterator it;

	while (!it.IsAtEnd())
	{
		GetStreamingInfoRef(*it).ClearFlag(*it, flag);
		++it;
	}
}

#if __ASSERT
void strStreamingInfoManager::PrintRecursiveLoadState(strIndex index, int indent) const
{
	const int MAX_INDENT = 10;
	const char *indentString = "          ";	// Must be MAX_INDENT spaces

	indent = Min(indent, MAX_INDENT);

	const strStreamingInfo &info = GetStreamingInfoRef(index);

	streamDisplayf("%s%s (index %" STRINDEX_FMT "): Status=%s", &indentString[MAX_INDENT-indent], GetObjectName(index),
		index.Get(), strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));

	// Handle all dependencies
	const strStreamingModule* pModule = GetModule(index);
	strLocalIndex objIndex = pModule->GetObjectIndex(index);
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	s32 numDeps = pModule->GetDependencies(objIndex, deps, STREAMING_MAX_DEPENDENCIES);
	for (int i = 0; i != numDeps;++i)
	{
		PrintRecursiveLoadState(deps[i], indent+1);
	}

	// And the packfile too.
	u32 handle = info.GetHandle();

	if (!info.IsPackfileHandle() && strIsValidHandle(handle))
	{
		strLocalIndex imageIndex = strLocalIndex(strPackfileManager::GetImageFileIndexFromHandle(handle));
		if (imageIndex != -1)
		{
			// TODO: Do this only once per file.
			strIndex archiveStrIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);
			strStreamingFile &file = *strPackfileManager::GetImageFile(imageIndex.Get());

			bool archiveIsResident = strPackfileManager::IsImageResident(imageIndex.Get());

			streamDisplayf("%sPackfile: %s, reqCount=%d, resident=%d, locked=%d, UL=%d, ULU=%d",
				&indentString[MAX_INDENT-indent], (archiveIsResident) ? "Resident" : "Not resident",
				file.m_requestCount, file.m_bInitialized,
				file.m_Locked, file.m_UserLocked, file.m_UserLockedAndUnlocked);

			PrintRecursiveLoadState(archiveStrIndex, indent+1);
		}
		else
		{
			streamDisplayf("%sNot in a pack file", &indentString[MAX_INDENT-indent]);
		}
	}
	else
	{
		streamDisplayf("%sIs a pack file (or invalid handle)", &indentString[MAX_INDENT-indent]);
	}
}
#endif // __ASSERT


#if __BANK
strIndex strStreamingInfoManager::GetStreamingIndexByName(const char* name) const
{
	const char* ext = strrchr(name, '.');
	
	if (ext)
	{
		strStreamingModule* pModule = ms_moduleMgr.GetModuleFromFileExtension(++ext);
		if (pModule != NULL)
		{
			int streamingIndex = 0;
			for (int i=0; i != pModule->GetCount(); ++i)
			{
				if (!strnicmp(name, pModule->GetName(strLocalIndex(i)), (ext-name)-1))
					return pModule->GetStreamingIndex(strLocalIndex(streamingIndex));
				streamingIndex++;
			}
		}
	}
	return strIndex();
}
#endif

// Mark a resource as having been changed by the game so we don't move it anymore.
void strStreamingInfoManager::SetDoNotDefrag(strIndex index)
{
	strStreamingInfo& info = GetStreamingInfoRef(index);
	info.SetFlag(index, STRFLAG_INTERNAL_DONT_DEFRAG);

#if __DEV
	DIAG_CONTEXT_MESSAGE("strStreamingInfoManager::MarkDirty(%s)",strStreamingEngine::GetObjectName(index));
#endif

#if USE_DEFRAGMENTATION
	if (info.IsFlagSet(STRFLAG_INTERNAL_DEFRAGGING))
	{
#if __ASSERT
		streamWarningf("MarkDirty canceling a live defrag");
#endif
		strStreamingEngine::GetDefragmentation()->Cancel(index, true);
	}

	if (info.GetStatus() == STRINFO_LOADED) 
	{
		strStreamingModule* pModule = ms_moduleMgr.GetModuleFromIndex(index);
		pgBase *base = pModule->GetResource(pModule->GetObjectIndex(index));

		if (base)
		{
			strStreamingEngine::GetDefragmentation()->MakeUndefragmentable(base, index);
		}
	}
#endif // USE_DEFRAGMENTATION
}



//////////////////////////////////////////////////////////////////////////

datResourceInfo strStreamingInfo::GetResourceInfo()
{
	streamAssert(strIsValidHandle(m_handle));
	streamAssert(static_cast<fiPackfile *>(fiPackfile::GetCollection(m_handle))->IsInitialized());
	const fiPackEntry &pe = fiPackfile::GetCollection(m_handle)->GetEntryInfo(m_handle);
	streamAssertf(pe.IsResource(),"entry '%s' (handle 0x%x) isn't a resource?",pgStreamer::GetFullName(m_handle),m_handle);
	return pe.u.resource.m_Info;
}

void strStreamingInfo::SetResourceInfo(datResourceInfo& UNUSED_PARAM(value))
{
	streamAssert(false);
}

u32 strStreamingInfo::ComputeOccupiedVirtualSize(strIndex index, bool mayFail, bool dontUseLoadedInfo) const
{
	// We only have a special case for PRF files - they need to be padded up.
	if (IsPackfileHandle())
	{
		strStreamingFile *archive = strPackfileManager::GetImageFile(GetPackfileIndex());

		// Switch back to the main heap so ComputeLeafSize() can do its job.
		sysMemStartTemp();
		size_t size = pgRscBuilder::ComputeLeafSize(archive->m_CachedHeaderSize, false);
		sysMemEndTemp();

		return (u32) size;
	}

	// Everything else takes the normal path.
	return ComputeVirtualSize(index, mayFail, dontUseLoadedInfo);
}


u32 strStreamingInfo::ComputeVirtualSize(strIndex index, bool ASSERT_ONLY(mayFail), bool dontUseLoadedInfo) const
{
	if (IsPackfileHandle())
	{
		strStreamingFile *archive = strPackfileManager::GetImageFile(GetPackfileIndex());
#if RSG_PC
		// Needed for anti-tamper
		size_t size = (archive->m_bLoadCompletely)?(archive->m_CachedHeaderSize):((archive->m_CachedHeaderSize + 4095) & ~4095);
#else
		size_t size = archive->m_CachedHeaderSize;
#endif
		return (u32) size;
	}

	// If it's loaded, always use the loaded info since that includes the extra size.
	if (!dontUseLoadedInfo && GetStatus() == STRINFO_LOADED)
	{
		strStreamingInfoManager &infoMgr = strStreamingEngine::GetInfo();
		strLoadedInfo *loadedInfo = infoMgr.GetLoaded(index);

		if (!loadedInfo)
		{
			loadedInfo = infoMgr.GetPersistentLoaded(index);
		}

		if (loadedInfo)
		{
			return loadedInfo->GetVirtualSize();
		}
		streamAssert(false);
	}

	if (strIsValidHandle(m_handle))
	{
		s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(m_handle).Get();
		if (imageIndex != -1 && !strPackfileManager::IsImageResident(imageIndex))
		{
			streamAssertf(mayFail, "Trying to get size of a resource inside a non-resident archive (%s)", strPackfileManager::GetImageFile(imageIndex)->m_name.GetCStr());
		}
		else
		{
			const fiCollection *packfile = fiPackfile::GetCollection(m_handle);

			if (packfile)
			{
				const fiPackEntry &pe = packfile->GetEntryInfo(m_handle);
				size_t size = pe.IsResource()? pe.u.resource.m_Info.GetVirtualSize() : pe.GetUncompressedSize();
				FastAssert(size);

				return (u32) size;
			}
			else
			{
				streamAssertf(mayFail, "Trying to get the size of a file with an invalid packfile");
			}
		}
	}
#if STORE_FILE_SIZES
	return m_VirtualSize;
#else // STORE_FILE_SIZES
	return 0;
#endif // STORE_FILE_SIZES
}

u32 strStreamingInfo::ComputePhysicalSize(strIndex index, bool ASSERT_ONLY(mayFail)) const
{
	// Packfiles don't have physical memory.
	if (IsPackfileHandle())
	{
		return 0;
	}

	// If it's loaded, always use the loaded info since that includes the extra size.
	if (GetStatus() == STRINFO_LOADED)
	{
		strStreamingInfoManager &infoMgr = strStreamingEngine::GetInfo();
		strLoadedInfo *loadedInfo = infoMgr.GetLoaded(index);

		if (!loadedInfo)
		{
			loadedInfo = infoMgr.GetPersistentLoaded(index);
		}

		if (loadedInfo)
		{
			return loadedInfo->GetPhysicalSize();
		}
		streamAssert(false);
	}

	if (strIsValidHandle(m_handle))
	{
		s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(m_handle).Get();
		if (imageIndex != -1 && !strPackfileManager::IsImageResident(imageIndex))
		{
			streamAssertf(mayFail, "Trying to get size of a resource inside a non-resident archive (%s)", strPackfileManager::GetImageFile(imageIndex)->m_name.GetCStr());
		}
		else
		{
			const fiCollection *packfile = fiPackfile::GetCollection(m_handle);

			if (packfile)
			{
				const fiPackEntry &pe = packfile->GetEntryInfo(m_handle);
				return pe.IsResource()? (u32) pe.u.resource.m_Info.GetPhysicalSize() : 0;
			}
		}
	}
#if STORE_FILE_SIZES
	return m_PhysicalSize;
#else // STORE_FILE_SIZES
	return 0;
#endif // STORE_FILE_SIZES
}

bool strStreamingInfo::IsCompressed()
{
	/* if (strIsValidHandle(m_handle))
	{
		const fiCollection *packfile = fiPackfile::GetCollection(m_handle);
		return f
	} */
	// TODO - figure out what this is actually used for.
	// Resources are not compressed in the rpf sense.
	return false;
}

u32 strStreamingInfo::GetDevices(strIndex ASSERT_ONLY(index)) const
{
	strStreamingFile *archiveInfo = NULL;

	// Dummy files are always available.
	if (IsFlagSet(STRFLAG_INTERNAL_DUMMY))
	{
		return (1 << pgStreamer::HARDDRIVE) | (1 << pgStreamer::OPTICAL);
	}

	streamAssertf(strIsValidHandle(m_handle), "Streamable %s (%" STRINDEX_FMT ") doesn't have a handle but isn't a dummy and being requested", strStreamingEngine::GetObjectName(index), index.Get());

	if (strIsValidHandle(m_handle))
	{
		s32 imageIndex;
		if (!IsPackfileHandle())
		{
			// This is a file inside an archive.
			imageIndex = strPackfileManager::GetImageFileIndexFromHandle(m_handle).Get();

			// If it's a loose file, check the file directly.
			if (imageIndex == -1)
			{
				int deviceType = (pgStreamer::GetPosn(m_handle, 0) & (fiDevice::HARDDRIVE_LSN))? pgStreamer::HARDDRIVE : pgStreamer::OPTICAL;
				return 1 << deviceType;
			}
			else
			{
				strStreamingFile* pImageFile = strPackfileManager::GetImageFile( (u32) imageIndex);
				if (pImageFile && pImageFile->m_bLoadCompletely)
				{
					return (1 << pgStreamer::OPTICAL);
				}

			}
		}
		else
		{
			// This is an RPF file.
			imageIndex = GetPackfileIndex();
			streamAssertf(imageIndex != -1, "Bad image index for archive");
		}


		if (imageIndex != -1)
		{
			archiveInfo = strPackfileManager::GetImageFile(imageIndex);
			streamAssert(archiveInfo);
		}
	}

	u32 result = 0;

	if (archiveInfo)
	{
		if (archiveInfo->m_IsOnOdd)
		{
			result |= 1 << pgStreamer::OPTICAL;
		}

		if (archiveInfo->m_IsOnHdd)
		{
			result |= 1 << pgStreamer::HARDDRIVE;

			// TODO: For now, we can't force reading from ODD if a file is on HDD, so let's just
			// pretend like it doesn't exist there.
			result &= ~(1 << pgStreamer::OPTICAL);
		}

		streamAssertf(result != 0, "File is neither on ODD nor HDD?");
	}

	return result;
}

pgStreamer::Device strStreamingInfo::GetPreferredDevice(strIndex index) const
{
	return GetDevices(index) & (1 << pgStreamer::HARDDRIVE) ? pgStreamer::HARDDRIVE : pgStreamer::OPTICAL;
}

#if __BANK && !__RESOURCECOMPILER
void strStreamingInfo::ClearFlag(strIndex index, u32 flag, bool checkLockedFlags)
{
	VerifyFlagChange(m_flags, m_flags & ~flag, checkLockedFlags);
	STRVIS_ON_FLAGS_CHANGE(index, m_flags, m_flags & ~flag);
	m_flags &= ~flag;
}

void strStreamingInfo::SetFlag(strIndex index, u32 flag, bool checkLockedFlags)
{
	VerifyFlagChange(m_flags, m_flags | flag, checkLockedFlags);
	STRVIS_ON_FLAGS_CHANGE(index, m_flags, m_flags | flag);
	m_flags |= flag;
}
#endif // __BANK


u32 strStreamingInfo::GetCdPosn(bool forceOptical) const
{
	if (strIsValidHandle(m_handle))
	{
		if (!IsPackfileHandle())
		{
			s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(m_handle).Get();
			if (imageIndex != -1 && !strPackfileManager::IsImageResident(imageIndex))
			{
				return 0;
			}
		}

		const fiCollection *packfile = fiPackfile::GetCollection(GetHandle());

		if (packfile)
		{
			return packfile->GetEntryPhysicalSortKey(GetHandle(), forceOptical);
		}
	}
	return 0;
}

u32 strStreamingInfo::GetCdByteSize() const
{
	// Can't get the size of an internal dummy
	streamAssert(!IsFlagSet(STRFLAG_INTERNAL_DUMMY));

	if (IsPackfileHandle())
	{
		return SECTOR_SIZE;
	//	return strPackfileManager::GetImageFile(GetPackfileIndex())->m_CachedHeaderSize;
	}

	if (!IsPackfileHandle())
	{
		s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(GetHandle()).Get();
		if (imageIndex != -1 && !strPackfileManager::IsImageResident(imageIndex))
		{
			return 0;
		}
	}

	u32 handle = GetHandle();
	return  fiPackfile::GetCollection(handle)->GetEntryInfo(handle).GetConsumedSize();
}

bool strStreamingInfo::GetCdPosnAndSectorCount(u32& posn, u32& sectors) const
{
	if (strIsValidHandle(m_handle))
	{
		posn = GetCdPosn();
		sectors = (GetCdByteSize() + (SECTOR_SIZE - 1)) / SECTOR_SIZE;
		return true;
	}

	return false;
}

void strStreamingInfo::SetCdPosnAndByteSize(u32 UNUSED_PARAM(posn), u32 ASSERT_ONLY(size))
{
	streamAssertf(size > 0 || IsFlagSet(STRFLAG_INTERNAL_DUMMY), "File in CdImage has size zero");
}


strLoadedInfoIterator::strLoadedInfoIterator(s32 streamingModuleId /* = -1 */)
{
	 m_streamingModule = streamingModuleId; 
	 strStreamingInfoManager& manager = strStreamingEngine::GetInfo();

	 hashpos = -1;
	 cn = 0;
	 loadedMap = manager.GetLoadedInfoMap();
	 persistentMap = manager.GetLoadedPersistentInfoMap();
}

strIndex strLoadedInfoIterator::GetNextIndex() {
	if (loadedMap)
	{
		while((hashpos + 1) < loadedMap->GetNumSlots()) 
		{
			if (!cn) 
			{
				hashpos++;
				cn = loadedMap->GetEntry(hashpos);
			}
			else 
			{
				cn = cn->next;
			}
			
			while (cn) {
				bool matchesModule = m_streamingModule == -1 || m_streamingModule == strStreamingEngine::GetInfo().GetModuleMgr().GetModuleIdFromIndex(cn->key);
				if (matchesModule) 
				{
					return cn->key;
				}

			  cn = cn->next;
			}
			
		}
		loadedMap = NULL;
		cn = NULL;
		hashpos = -1;
	}
	
	if (persistentMap)
	{
		while((hashpos + 1) < persistentMap->GetNumSlots()) 
		{
			if (!cn) 
			{
				hashpos++;
				cn = persistentMap->GetEntry(hashpos);
			}
			else 
			{
				cn = cn->next;
			}

			while (cn) {
				bool matchesModule = m_streamingModule == -1 || m_streamingModule == strStreamingEngine::GetInfo().GetModuleMgr().GetModuleIdFromIndex(cn->key);
				if (matchesModule) 
				{
					return cn->key;
				}

				cn = cn->next;	
			}
		}
		persistentMap = NULL;
	}

	return strIndex(strIndex::INVALID_INDEX);
}

strStreamingInfo* strLoadedInfoIterator::GetNext() {
	strIndex index = GetNextIndex();

	if (index.IsValid())
	{
		return strStreamingEngine::GetInfo().GetStreamingInfo(index);
	}

	return NULL;
}

#if !__NO_OUTPUT
const char *strStreamingInfo::GetFriendlyStatusName(int status)
{
	static const char *sFriendlyNames[] = {
		"Not Loaded",
		"Loaded",
		"Requested",
		"Loading"
	};

	CompileTimeAssert(NELEM(sFriendlyNames) == STRINFO_COUNT);
	streamAssert((u32) status < (u32) STRINFO_COUNT);

	return sFriendlyNames[status];
}
#endif // !__NO_OUTPUT

#if STREAMING_DETAILED_STATS
void strStreamingInfo::MarkUnloadedTimestamp()
{
	m_LastUnload = TIME.GetFrameCount();
}

int strStreamingInfo::GetPriority() const
{
	return (GetFlags() & STRFLAG_PRIORITY_LOAD) ? 1 : 0;
}

#endif // STREAMING_DETAILED_STATS


}		// namespace rage
