//
// streaming/cacheloader.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_CACHELOADER_H
#define STREAMING_CACHELOADER_H

#include "atl/array.h"
#include "atl/map.h"
#include "file/limits.h"
//#include "system/filemgr.h"

#define CACHELOADER_MAX_CACHE_MODULES 6

//------------------------------------------------------------------------------------------------------------------
// The cache loader will load a file check with the streaming system if the dates of the modules contained within 
// are of the same date, if not then the system aspires to write out a new cache file, and if of the same date
// then we have loaded up some 'sub data' ( bounding boxes etc )  of some resources without having to load the 
// whole thing in. This approach although slow the first time the game loads saves a lot of loading time.
//

namespace rage {

enum strCacheLoaderMode
{
	SCL_DEFAULT = 0,
	SCL_DLC
};

class fiStream;

class strCacheLoader
{
public:

	struct CModuleDesc
	{
		CModuleDesc() {}

		typedef bool (*LoadFn)(const void* const pEntry, fiStream* pDebugTextFileToWriteTo);
		typedef void (*SaveFn)(fiStream* pDebugTextFileToWriteTo);

		LoadFn m_loadFn;
		SaveFn m_saveFn;

		char m_moduleName[32];

		char* m_header_pos;
		char* m_footer_pos;

		s32 m_entrySize;
	}; // CModuleDesc

	class CModuleDescArray
	{
		friend class strCacheLoader;
	public:
		CModuleDescArray() {
			m_numModules = 0;
		}

		s32 AddModule(const char* pName,
			CModuleDesc::LoadFn loadFn,
			CModuleDesc::SaveFn saveFn,
			s32 entrySize);

		bool Load(const char* pName);
		void Save(const char* pName);
		bool Check();

	private:
		s32 GetNumModules() { return m_numModules; }
		bool Load(s32 index, fiStream* pTextFileToWriteTo);
		void Save(s32 index, fiStream* pTextFileToWriteTo);

		char* WriteHeader(s32 module);
		void WriteFooter(s32 module);

		char* GetHeader(s32 module);
		char* GetFooter(s32 module);

		CModuleDesc m_modules[CACHELOADER_MAX_CACHE_MODULES];
		s32 m_numModules;
	}; // CModuleDescArray

public:
	static void		Init(const char *pFilename, const char* pDeviceAndPath, strCacheLoaderMode initMode, const atMap<s32, bool>& dlcArchives);
	static void		Disable() { ms_bCacheEnabled = false; } // use this function to set to ignore the cache file in this game currently used for the AnimViewer, but doesnt change the cache file on disc
	static void		Shutdown();

	static bool		Load();

	static void		Save();

	static bool GetReadyOnly();
	static void		SetReadOnly(bool value);
	static CModuleDescArray& GetModules()			{ return ms_modules; }
	static bool		GetLoadCache()					{ return ms_bLoadCache;}
	static bool		GetSaveCache()					{ return ms_bSaveCache;}
	static strCacheLoaderMode GetMode() { return ms_mode; }
	static const atMap<s32, bool>& GetDLCArchives() { return ms_dlcArchives; }
	static bool HasLoadedCache() { return ms_readCaches.Find(atHashString(ms_cCacheFilename)) != -1; }
	static bool GetFilesChanged() { return ms_bFilesChanged; }

	static void		WriteDataToBuffer(void *pData, s32 size);
	static void		WriteStringToBuffer(const char *fmt, ...);

	static void		SetPackagedBuild(bool val)		{ ms_bPackagedBuild = val; }
private:

	static char*	GetCacheFilename()				{ return ms_cCacheFilename; }
	static s32	ParseCache();
	static const char*	GetLine(const char* pBuffer, char* pLine);
	static void		ReadLine(char*& pSavedBufferStart, char* pLine);
	static void		AllocateWorkBuffer();
	static void		FreeWorkBuffer();
	static void		LoadCache();
	static void		BuildCacheFilename(const char* const pFilename);
	static bool		CheckCacheFileDates();
	static void		WriteCacheFileDates();
	static bool		CheckCacheFileIsValid(const char* const pFilename);
	static void		CloseCacheFile();
	static void		SetCacheAsInvalid();
	static void		WriteCacheLoaderHeader();
	static void		WriteWorkBuffer();
	static bool		CheckVersionNumber();
	static void		DetermineCacheFileReadOnly();

	static void		EncryptWorkBuffer();
	static void		WriteSignature(char* signature);
	static void		ReadSignature(char* signature);
	static void		DecryptWorkBuffer();

	static fiStream*		ms_cache_stream;
	static CModuleDescArray ms_modules;
	static s32 ms_cacheBufferSize;

	static bool				ms_bCacheEnabled;
	static bool				ms_bCacheReadonly;
	static bool				ms_bLoadCache;
	static bool				ms_bSaveCache;
	static bool				ms_bPackagedBuild;
	static bool				ms_bCacheUsesEncryption;
	static bool ms_bFilesChanged;

	static strCacheLoaderMode ms_mode;
	static atMap<s32, bool> ms_dlcArchives;
	static atArray<u32> ms_readCaches;
	static char*			ms_WorkBuffer;		
	static char*			ms_WorkBufferPos;	
	static char*			ms_pFileDatesStart;
	static char*			ms_pFileDatesEnd;
	static char				ms_cCacheFilename[RAGE_MAX_PATH];
	static char				ms_cDeviceAndPath[RAGE_MAX_PATH];
}; // strCacheLoader

} // rage

#endif // STREAMING_CACHELOADER_H

