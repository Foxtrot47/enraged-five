//
// streaming/requestrecorder.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#ifndef STREAMING_REQUESTRECORDER_H
#define STREAMING_REQUESTRECORDER_H

#include "streamingdefs.h"
#include "streaminginfo.h"
#include "streamingmodulemgr.h"
#include "file/handle.h"

#include "streamingallocator.h"
#include "streamingloader.h"
#include "streaminglive.h"
#include "streaminginstall.h"

namespace rage {


/** PURPOSE: This is a permanent reference to a resource (i.e. an object managed by a strStreamingModule) that identifies the
 *  resource by its name and its extension.
 */
struct strResourceReference
{
	// Name of the asset, without extension. Would be nice for this to be a hash string some day, but some
	// streaming modules currently require the full string.
	atString m_AssetName;

	// Extension. May have '#' as the first character to indicate the platform character.
	char m_Extension[8];

	// PUROPSE: Resolve this reference into a streaming index.
	strIndex Resolve(bool assertIfMissing = true) const;

#if !__FINAL
	// Set this reference to point to a streamable.
	void Set(strIndex index);
#endif // !__FINAL

	PAR_SIMPLE_PARSABLE;
};

/** PURPOSE: This object represents a single streaming request.
 */
struct strRecordedRequest
{
	// Object to stream
	strResourceReference m_Resource;

	// Streaming flags to use (STRFLAG_).
	s32 m_Flags;

	PAR_SIMPLE_PARSABLE;
};

/** PURPOSE: This is a list of streaming requests.
 */
struct strRequestRecording
{
	atArray<strRecordedRequest> m_Requests;

	PAR_SIMPLE_PARSABLE;
};


/** PURPOSE: The request recorder can hook into the streaming system and create a list of all streamables that were
 *  requested, and save that to a file. It can also load this file and request all the listed files in one go.
 *
 *  Use BeginRecording() and EndRecording() to mark the time between which requests will be recorded, and LoadRecording()
 *  and ExecuteRecording() to play one such list back.
 */
class strRequestRecorder
{
public:
	strRequestRecorder();

	// PURPOSE: Load a recorded list of requests. Call ExecuteRecording() to requests them.
	void LoadRecording(const char *filename);

	// PURPOSE: Go through the list obtained via LoadRecording() and request each file in it.
	void ExecuteRecording();



#if !__FINAL
	// PURPOSE: Callback from the streaming system to record a request.
	void OnRequest(strIndex index, s32 flags);

	// PURPOSE: Save this recording to a file.
	void SaveRecording(const char *filename);

	/* PURPOSE: Start recording requests.
	 *
	 * PARAMS:
	 *  recordLeafAssetsOnly - will only record requests that do not have any dependencies.
	 *  recordPackfiles - only if this is true will packfile requests be recorded.
	 */
	void BeginRecording(bool recordLeafAssetsOnly, bool recordPackfiles);

	// PURPOSE: No longer record requests.
	void EndRecording();
#endif // !__FINAL



private:
	strRequestRecording *m_Recording;

#if !__FINAL
	// Currently recording requests
	bool m_IsRecording;

	// Only record assets without dependencies
	bool m_LeafAssetsOnly;

	// Record packfiles as well
	bool m_RecordPackfileRequests;
#endif // !__FINAL
};

extern strRequestRecorder gRequestRecorder;

#define REQUESTRECORDER gRequestRecorder


}	// namespace rage

#endif // STREAMING_REQUESTRECORDER_H
