//
// streaming/streamingallocator.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//


#ifndef STREAMING_STREAMINGALLOCATOR_H
#define STREAMING_STREAMINGALLOCATOR_H

#include "streamingdefs.h"

#include "system/memory.h"
#include "atl/array.h"

namespace rage {

class strStreamingInfo;

#if __BANK

class strExternalAlloc
{
public:
	strExternalAlloc() 
	{

	}

	// Defines to avoid confusion in the streaming debug graph (which seems to be the opposite of what you would expect)
	bool IsMAIN() { return (m_MemType == MEMTYPE_RESOURCE_VIRTUAL); }
	bool IsVRAM() { return (m_MemType == MEMTYPE_RESOURCE_PHYSICAL); }

	void			*m_pMemory;
	u32				m_Size;
	u32				m_diagContextMessageHash;
	u16				m_BackTraceHandle;
	u8				m_MemType;	// See eMemoryType
	s8				m_BucketID;	// See MemoryBucketIds
};

class strExternalAllocTracker
{
public:
	strExternalAllocTracker() 
	{
		m_Allocations.Reset();
	}

	void AddRecord(void *pMem, u32 size, int memType, int bucketID, u16 backTraceHandle );
	void RemoveRecord(const void *pMem );

	atArray<strExternalAlloc> m_Allocations;
};

#endif	//__BANK

class strStreamingAllocator : public sysMemAllocator
{
public:
	virtual ~strStreamingAllocator() {}

	void Init(size_t virtualSize, size_t physicalSize);
	void Shutdown();
	void Update();

	// Will allocate the memory, but will engage resource reclamation code.
	bool StreamingAllocate(datResourceMap& m_Map, strIndex index, int memBucket = MEMBUCKET_RESOURCE, u32 customUserData = MEM_INVALID_USERDATA);

	// Get the heap page owner from an allocation
	strIndex GetHeapPageOwner(const void *ptr, bool isPhysical);

	// Will try to allocate the memory, but does minimal reclamation and is more 
	// likely to return NULL.
	virtual void* TryAllocate(size_t size, size_t align, int heapIndex = 0);

	virtual void* Allocate(size_t size, size_t align, int heapIndex = 0);
	virtual void Free(const void *ptr);

	virtual void StreamingFree(const void *ptr);
	void StreamingFree(datResourceMap& m_Map 
#if RSG_PC
		, bool bFreePhysical = false
#endif // RSG_PC
		);

	virtual sysMemAllocator *GetPointerOwner(const void *ptr);

	virtual size_t GetSize(const void *ptr) const;
	virtual size_t GetMemoryUsed(int bucket = -1 /*all buckets*/);
	virtual size_t GetMemoryAvailable() { return m_virtualAvailable + m_physicalAvailable; }
	virtual bool IsValidPointer(const void* ptr) const;

	void AddToMemoryUsed(const void* ptr);
	bool RemoveFromMemoryUsed(const void* ptr);

	void StartExternalAllocation();
	void EndExternalAllocation();

	bool HasSpaceFor(size_t virtualSize, size_t physicalSize);
	bool MakeSpaceForAllocation(size_t virtualSize, size_t physicalSize, bool aggressive = false, bool forceFree = false);

	int GetVirtualMemoryFree() const {return int(m_virtualAvailable) - int(m_virtualUsed);}
	int GetPhysicalMemoryFree() const {return int(m_physicalAvailable) - int(m_physicalUsed);}
	size_t GetVirtualMemoryAvailable() const {return m_virtualAvailable;}
	size_t GetVirtualMemoryAvailable_Cached() const {return sm_virtualAvailable_Cached;}
	size_t GetVirtualMemoryUsed() const {return m_virtualUsed;}
	size_t GetVirtualMemoryUsed_Cached() const {return sm_virtualUsed_Cached;}
#if !__FINAL
	size_t GetVirtualSlosh();
	size_t GetPhysicalSlosh();
#endif
	size_t GetPhysicalMemoryAvailable() const {return m_physicalAvailable;}
	size_t GetPhysicalMemoryAvailable_Cached() const {return sm_physicalAvailable_Cached;}
	size_t GetPhysicalMemoryUsed() const {return m_physicalUsed;}
	size_t GetPhysicalMemoryUsed_Cached() const {return sm_physicalUsed_Cached;}

	size_t GetExternalPhysicalMemoryUsed() const {return m_externalPhysicalUsed;}
	size_t GetExternalPhysicalMemoryUsed_Cached() const {return sm_externalPhysicalUsed_Cached;}
	size_t GetExternalVirtualMemoryUsed() const {return m_externalVirtualUsed;}
	size_t GetExternalVirtualMemoryUsed_Cached() const {return sm_externalVirtualUsed_Cached;}

	// Slosh
	static bool GetSloshState() {return s_sloshState;}
	static void SetSloshState(bool state) {s_sloshState = state;}

	void SanityCheck();

#if !__FINAL
	void EnableStackTraces(bool enabled)			{ m_EnableStackTraces = enabled; }
	bool GetEnableStackTraces() const				{ return m_EnableStackTraces; }
#endif // !__FINAL

private:
	size_t ComputeLeafSize(size_t size, bool physical);

	size_t				m_virtualAvailable;					// memory available to streaming
	size_t				m_virtualUsed;						// memory used by streaming
	size_t				m_physicalAvailable;				// memory available to streaming
	size_t				m_physicalUsed;						// memory used by streaming
	size_t				m_externalVirtualUsed;
	size_t				m_externalPhysicalUsed;
	static size_t		sm_virtualAvailable_Cached;
	static size_t		sm_virtualUsed_Cached;
	static size_t		sm_physicalAvailable_Cached;
	static size_t		sm_physicalUsed_Cached;
	static size_t		sm_externalVirtualUsed_Cached;
	static size_t		sm_externalPhysicalUsed_Cached;
//	u32					m_external;

	// Slosh
	static bool s_sloshState;

#if __BANK
public:

	strExternalAllocTracker	m_ExtAllocTracker;

#endif

#if !__FINAL
	bool				m_EnableStackTraces;				// Spew a stack trace if an allocation failed
#endif // !__FINAL

};

} // namespace rage


#endif // STREAMING_STREAMINGALLOCATOR_H
