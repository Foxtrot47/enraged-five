//
// streaming/streamingrequest.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_STREAMINGREQUEST_H
#define STREAMING_STREAMINGREQUEST_H

#include "atl/array.h"
#include "atl/hashstring.h"
#include "streaming/streamingdefs.h"

/*
	A strRequest is nothing more than a form of a smart pointer. It allows you
	to acquire a resource, and hold on to it until you release, or until the 
	pointer falls out of scope.

	1)	Add a request member to your class.

		strRequest m_myRequest;

	2)	Set up the request when you are ready to commence your streaming 
		adventure. You normally don't need to supply flags.

		m_myRequest.Request(vehicle, CModelInfo::GetStreamingModuleId());

	3)	Wait for the data to be streamed in.

		if (m_myRequest.IsLoaded())
			DriveVehicle();
			
	4)	If you decide you don't need it, release the model. You can release it 
		even if the data never got loaded. Once the strRequest drops out of 
		scope it is released anyway, so its safe.

	If you call the Acquire function again with different requestId and module,
	it will release any resource already loaded, and start to load the new 
	object.

*/

namespace rage {

class strRequest
{
public:
	enum {
		INVALID_MODULE=0x7F,
	};

	strRequest() : 
		m_flags(0),
		m_moduleId(INVALID_MODULE),
		m_loaded(false) {}; 
	~strRequest() {Release();}

	bool	Request(strLocalIndex requestId, u32 moduleId, u32 flags = 0);
	bool	HasLoaded();
	bool	IsValid() { return( (m_requestId.IsValid()) && (m_moduleId != INVALID_MODULE)); }
	bool	IsInvalid() { return (!IsValid()); }
	void	Release();
	void	DelayedRelease();
	bool	StreamingRemove(); 
	void	ClearRequiredFlags(u32 flags); 
	void	SetRequiredFlags(u32 flags);

	void	ClearMyRequestFlags(u32 flags);

	strIndex GetIndex() const;
	strLocalIndex		GetRequestId() const {return m_requestId;}
	u32		GetModuleId() const {return m_moduleId;}

	strRequest(const strRequest& that) : 
		m_requestId(that.m_requestId),
		m_flags(that.m_flags),
		m_moduleId(that.m_moduleId),
		m_loaded(false) 
	{
		HasLoaded();
	}
	strRequest& operator=(const strRequest& that) {
		Request(that.m_requestId, that.m_moduleId, that.m_flags);
		return *this;
	}

#if __BANK
	const char* GetName();
#endif	// __BANK

private:

	strLocalIndex	m_requestId;
	u16	m_flags:8;
	u16	m_moduleId:7;
	u16	m_loaded:1;
};

/*	A bunch of streaming requests.

	1)	Add a request to a member of your class.

		strRequestArray<3> m_myRequests;

	2)	Set up the requests when you are ready to commence your streaming 
		adventure. You normally don't need to supply flags. Also, you don't HAVE
		to fill out the array, so pushing two requests will also work.

		m_myRequests.PushRequest(vehicle, CModelInfo::GetStreamingModuleId());
		m_myRequests.PushRequest(othervehicle, CModelInfo::GetStreamingModuleId());
		m_myRequests.PushRequest(waveatvehicleanim, fwAnimManager::GetStreamingModuleId());

	3)	Wait for the data to be streamed in.

		if (m_myRequests.HaveAllLoaded())
			WaveAtVehicle();

	4)	If you decide you don't need the data anymore, release the models with 
		ReleaseAll(). You can release it even if the data never got loaded. Once 
		the strRequestArray drops out of scope it is released anyway, so its safe.

		If you call the Acquire function again with different requestId and 
		module on one of the requests through the accessors, it will release any 
		resource already loaded, and start to load the new object.

*/

template<u32 N> class strRequestArray 
{
public:
	~strRequestArray()
	{
		ReleaseAll();
	}

	void PushRequest(u32 requestId, u32 moduleId, u32 flags = 0)
	{
		if (Verifyf(m_requests.GetAvailable() != 0, "too many requests pushed into strRequestArray<%d>", N))
		{
			m_requests.Append().Request(strLocalIndex(requestId), moduleId, flags);
		}
	}

	void UseAsArray(){
		m_requests.Resize(N);
	}

	void SetRequest(u32 arrayIdx, u32 requestId, u32 moduleId, u32 flags = 0)
	{
		m_requests[arrayIdx].Request(strLocalIndex(requestId), moduleId,flags);
	}

	void ReleaseAll()
	{
		for (int i = 0 ; i < m_requests.GetCount(); ++i)
		{
			m_requests[i].Release();
		}
		m_requests.Reset();
	}

	void ClearRequiredFlags(u32 flags)
	{
		for (int i = 0 ; i < m_requests.GetCount(); ++i)
		{
			if (m_requests[i].IsValid()){
				m_requests[i].ClearRequiredFlags(flags);
			}
		}
	}

	void SetRequiredFlags(u32 flags)
	{
		for (int i = 0 ; i < m_requests.GetCount(); ++i)
		{
			if (m_requests[i].IsValid()){
				m_requests[i].SetRequiredFlags(flags);
			}
		}
	}

	bool HaveAllLoaded()
	{
		bool bResult = true;
		for (int i = 0 ; i < m_requests.GetCount(); ++i)
		{
			if (m_requests[i].IsValid()){
				bResult &= m_requests[i].HasLoaded();
			}
		}
		return bResult;
	}

	int RequestIndex(s32 requestId, u32 moduleId)
	{
		for (int i = 0 ; i < m_requests.GetCount(); ++i)
		{
			if (m_requests[i].GetRequestId().Get() == requestId && m_requests[i].GetModuleId() == moduleId)
				return i;
		}
		return -1;
	}

	int GetRequestCount() {
		return m_requests.GetCount();
	}

	int GetRequestMaxCount() 
	{
		return m_requests.GetMaxCount();
	}

	strRequest& GetRequest(int index)
	{
		return m_requests[index];
	}

	const strRequest& GetRequest(int index) const
	{
		return m_requests[index];
	}

	int GetRequestId(int index)
	{
		strLocalIndex requestId = m_requests[index].GetRequestId();		
		return( (requestId.IsInvalid()) ? -1 : requestId.Get() );
	}

private:
	atFixedArray<strRequest, N> m_requests;
};

// model request for streamed or permanent archetype
class strModelRequest
{
public:
	strModelRequest() : m_modelName(""), m_flags(0), m_bItypReffed(false) {};
	~strModelRequest() { Release(); }

	void Request(const char* qualifiedModelName, u32 flags);
	void Request(const char* pTypeFile, const char* pModelName, u32 flags);
	void Request(strLocalIndex itypSlotIdx, atHashString modelName, u32 flags);

	bool HasLoaded();
	bool IsValid();
	
	strLocalIndex GetRequestId() { return m_archetypeAssetsRequest.GetRequestId(); }

	void Release();
	
	void ClearRequiredFlags(u32 flags); 
	void SetRequiredFlags(u32 flags); 

	s32 GetTypSlotIdx() const { return m_itypSlotIdx.Get(); }
	const atHashString &GetModelName() const { return m_modelName; }
	u32 GetFlags() const { return m_flags; }

	strModelRequest(const strModelRequest& that);
	strModelRequest& operator=(const strModelRequest& that);

private:
	strLocalIndex m_itypSlotIdx;
	atHashString m_modelName;
	u32 m_flags;
	bool	m_bItypReffed;

	strRequest		m_typeFileRequest;
	strRequest		m_archetypeAssetsRequest;
};

}	// namespace rage

#endif // STREAMING_STREAMINGREQUEST_H
