//
// streaming/streamingengine.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "streamingengine.h"
#include "packfilemanager.h"

#include "bank/bank.h"
#include "data/aes.h"
#include "diag/art_channel.h"
#include "diag/output.h"
#include "file/asset.h"
#include "file/diskcache.h"
#include "file/packfile.h"
#include "file/stream.h"
#include "grcore/debugdraw.h"
#include "paging/streamer.h"
#include "parsercore/utils.h"
#include "profile/profiler.h"
#include "profile/timebars.h"
#include "streaming/defragmentation.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingmodule.h"
#include "system/bootmgr.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/xtl.h"

using namespace rage;

RAGE_DEFINE_CHANNEL(streaming)

RAGE_DEFINE_SUBCHANNEL(streaming, asset, rage::DIAG_SEVERITY_DISPLAY, rage::DIAG_SEVERITY_ERROR, rage::DIAG_SEVERITY_ERROR);
RAGE_DEFINE_SUBCHANNEL(streaming, srl, rage::DIAG_SEVERITY_DISPLAY, rage::DIAG_SEVERITY_ERROR, rage::DIAG_SEVERITY_ERROR);

#define INVALID_IMAGE_OFFSET			(0xffffffff)


bool					strStreamingEngine::ms_bDisableStreaming;				
bool					strStreamingEngine::ms_bIsInitialised;				
bool					strStreamingEngine::ms_bLoadingPriorityObjects;
bool					strStreamingEngine::ms_bLoadSceneActive = false;
bool					strStreamingEngine::ms_bBurstMode;

#if STREAMING_TELEMETRY_INTERFACE
bool					strStreamingEngine::ms_EnableTelemetryLog;
#endif // STREAMING_TELEMETRY_INTERFACE

strStreamingAllocator	strStreamingEngine::ms_allocator;
strStreamingInfoManager	strStreamingEngine::ms_info;
strStreamingLoaderManager	strStreamingEngine::ms_loader;
#if USE_DEFRAGMENTATION
strDefragmentation*		strStreamingEngine::ms_pDefragmentation;
#endif // USE_DEFRAGMENTATION

#if LIVE_STREAMING
strStreamingLive			strStreamingEngine::ms_live;
#endif

strStreamingInterface* rage::g_strStreamingInterface = NULL;

PS3_ONLY(static fiInstallerDevice* s_GameData = 0;)
PS3_ONLY(PARAM(noinstall, "Don't install streaming IMGs to PS3 HDD");)
PS3_ONLY(PARAM(reinstall, "Force a reinstall of streaming IMGs to PS3 HDD");)

PARAM(aggressivecleanup, "Cleanup streamed objects at the first opportunity");
PARAM(nodefrag, "Turn off defragmentation");

bool strStreamingEngine::UseDiskCache()
{
//	Always return false for now so that 360 non-__DEV builds behave the same as other builds.
//	Found while investigating Bugs 330942 and 364626
	return false;
// #if __DEV && !__PS3
// 	return sysBootManager::IsBootedFromDisc();
// #else
// 	return !__PS3;
// #endif
}

#if STREAM_STATS
PF_PAGE(StreamSizesPage, "Streaming Numbers");
PF_GROUP(StreamSizes);
PF_LINK(StreamSizesPage, StreamSizes);

PF_VALUE_INT(NumPending, StreamSizes);
PF_VALUE_INT(NumCompleted, StreamSizes);
PF_VALUE_INT(NumFailed, StreamSizes);
PF_VALUE_INT(NumDeleted, StreamSizes);
PF_VALUE_INT(NumPurged, StreamSizes);


PF_PAGE(StreamThruputPage, "Streaming Thruput");
PF_GROUP(StreamThruput);
PF_LINK(StreamThruputPage, StreamThruput);

PF_VALUE_INT(PendingSize, StreamThruput);
PF_VALUE_INT(CompletedSize, StreamThruput);
PF_VALUE_FLOAT(OddRollingCompleted, StreamThruput);
PF_VALUE_FLOAT(OddRollingRedundant, StreamThruput);
PF_VALUE_FLOAT(HddRollingCompleted, StreamThruput);
PF_VALUE_FLOAT(HddRollingRedundant, StreamThruput);
PF_VALUE_INT(FailedSize, StreamThruput);
PF_VALUE_INT(DeletedSize, StreamThruput);
PF_VALUE_INT(RedundantCount, StreamThruput);
#endif // STREAM_STATS

void strStreamingEngine::InitClass(strStreamingInterface * streamingInterface)
{
	g_strStreamingInterface = streamingInterface;
}

// to handle things like BG scripts which aren't necessarily counted early enough
#define NUM_EXTRA_ARCHIVE_FILES		(5)

// Initialise the streaming engine.
void strStreamingEngine::Init()
{
	ms_bDisableStreaming = false;
	ms_bLoadingPriorityObjects = false;
	ms_bLoadSceneActive = false;

	GetAllocator().Init(g_strStreamingInterface->GetVirtualSize(), g_strStreamingInterface->GetPhysicalSize());
	GetInfo().Init();
	GetLoader().Init();

	strPackfileManager::Init((int)g_strStreamingInterface->GetArchiveCount() + NUM_EXTRA_ARCHIVE_FILES);  //

	parUtils::SetPhysicalAllocator(&GetAllocator(), MEMTYPE_RESOURCE_PHYSICAL);

#if USE_DEFRAGMENTATION
	ms_pDefragmentation = rage_new strDefragmentation();
	ms_pDefragmentation->Init();
	if (PARAM_nodefrag.Get())
	{
		ms_pDefragmentation->FlushAndDisable_NoRenderThreadFlush();
	}
#endif

	ms_bIsInitialised = true;

	if (UseDiskCache())
	{
		fiDiskCache::InitClass((__XENON ? PRIO_LOWEST : PRIO_ABOVE_NORMAL), "Fuzzy");
	}

#if __PS3
// Commented out installer setup
//	if (sysBootManager::IsBootedFromDisc() || !PARAM_noinstall.Get() || PARAM_reinstall.Get())
//	{
//		s_GameData = rage_new fiPsnInstallerDevice(PARAM_reinstall.Get());
//		s_GameData->InstallIconFile("platform:/data/gta4ps3.png");
//		fiDevice::Mount("gamedata:/", *s_GameData, false);	
//	}
#endif

#if __ASSERT
	pgStreamer::SetErrorDumpCallback(DumpLoadingStateCb);
#endif // __ASSERT

#if !__NO_OUTPUT
	sysMemAllocator::sm_DebugPrintAllocInfo = DebugPrintAllocInfo;
#endif
}

#if __ASSERT
void strStreamingEngine::DumpLoadingStateCb()
{
	GetLoader().DumpLoadingState();
}
#endif // __ASSERT



// Shutdown the streaming engine.
void strStreamingEngine::Shutdown()
{
	strPackfileManager::Shutdown();

#if !__NO_OUTPUT
	sysMemAllocator::sm_DebugPrintAllocInfo = NULL;
#endif

#if USE_DEFRAGMENTATION
	ms_pDefragmentation->Shutdown();
	delete ms_pDefragmentation;
	ms_pDefragmentation = NULL;
#endif

	GetLoader().Shutdown();
	GetInfo().Shutdown();
	GetAllocator().Shutdown();
}

#if __PPU
void strStreamingEngine::SetInstallProgressCallback(Functor2<eInstallStatus, float> fn)
{
	if (s_GameData)
	    s_GameData->SetProgressCallback(fn);
}
#endif


void CheckCacheSize()
{
	s32 sizeCache0 = 0;
	s32 sizeCache1 = 0;
	int imageCount = strPackfileManager::GetImageFileCount();
	for(s32 i=0; i<imageCount; i++)
	{
		strStreamingFile* pFile = strPackfileManager::GetImageFile(i);
		if (pFile->m_bRegister && pFile->m_name.IsNotNull())
		{
			const fiDevice* pDevice = fiDevice::GetDevice(pFile->m_name.GetCStr());
			if(pFile->m_cachePartition == 0)
				sizeCache0 += (s32)(pDevice->GetFileSize(pFile->m_name.GetCStr()) / 1024);
			if(pFile->m_cachePartition == 1)
				sizeCache1 += (s32)(pDevice->GetFileSize(pFile->m_name.GetCStr()) / 1024);
		}
	}
	streamDisplayf("Size of cached files %d, %d\n", sizeCache0, sizeCache1);
}

void strStreamingEngine::InitLive(const char* NOTFINAL_ONLY(folderOverride))
{
#if LIVE_STREAMING
	// Check if it's already running - we're calling InitLive() twice on startup.
	if (!GetLive().GetIsRunning())
	{
		GetLive().Init();
		char livepath[256];
		const char *finalLivePath = livepath;
		
		strncpy(livepath, ASSET.GetPath(), 256);
		strcat(livepath, "preview/");

		if (folderOverride && folderOverride[0])
		{
			finalLivePath = folderOverride;

			if (!ASSET.Exists(finalLivePath, ""))
			{
				Assertf(false, "The preview folder path %s doesn't exist. Either specify a proper path with -previewfolder, or remove the garbage in your command line after the -previewfolder. Falling back to %s.", finalLivePath, livepath);
				finalLivePath = livepath;
			}
		}

		strStreamingEngine::GetLive().AddLiveFolder(finalLivePath);
	}
#endif // LIVE_STREAMING
}

// Initialise the streaming engine for a new map.
void strStreamingEngine::InitLevelWithMapUnloaded()
{
	InitLive();

	GetInfo().InitLevelWithMapUnloaded();

	CheckCacheSize();

	strPackfileManager::LoadImageDirectories();
}


void strStreamingEngine::ShutdownLevelWithMapUnloaded()
{
	// need to have emptied the disk cache before we can close the image list
	strPackfileManager::ShutdownImageList();

#if __DEV
	if (GetInfo().GetNumberOfObjectsLoaded() != 0)
		GetInfo().PrintLoadedList(false, true);

	if (GetInfo().GetNumberObjectsRequested() != 0)
		GetInfo().PrintRequestList();
	
	streamAssertf(GetInfo().GetNumberOfObjectsLoaded() == 0, "Cannot restart game with streamed objects in memory");
	streamAssertf(GetInfo().GetNumberObjectsRequested() == 0, "Cannot restart game with objects on the streaming request list");
#endif

#if LIVE_STREAMING
	GetLive().Shutdown();
#endif // LIVE_STREAMING
}


void strStreamingEngine::InitLevelWithMapLoaded()
{
	streamDisplayf("strStreamingEngine::InitLevelWithMapLoaded");

	ms_bDisableStreaming = false;

	//fiDiskCache::CreateInstance();
}

void strStreamingEngine::InitPackfiles()
{
	if(!UseDiskCache() PS3_ONLY(&& !s_GameData))
		strPackfileManager::OpenImageList();
}

//	This is currently not being called but I think it should be when the map data is unloaded.
//	Maybe the call to it was lost when the new init/shutdown system went in
//	void strStreamingEngine::ShutdownLevelWithMapLoaded()
//	{
//		streamDisplayf("strStreamingEngine::ShutdownLevelWithMapLoaded");
//	
//		Flush();	//	Does what GetLoader().ShutdownCampaign(); used to do
//		GetInfo().ShutdownCampaign();
//	
//	#if USE_DEFRAGMENTATION
//		GetDefragmentation()->Abort();
//	#endif
//	
//		Flush();
//	
//	#if SANITY_CHECK_REQUESTS
//		GetInfo().SanityCheckRequests();
//	#endif
	// remove everything from the request list
//		GetInfo().PurgeRequestList(0); 
//	
//	#if SANITY_CHECK_REQUESTS
//		GetInfo().SanityCheckRequests();
//	#endif
	// have to shutdown diskcache because I am about to close all the img file handles
	//fiDiskCache::DeleteInstance();

//		if(!UseDiskCache() PPU_ONLY(&& !s_GameData))
//			CloseImageList();

//		streamAssert(GetInfo().GetNumberObjectsRequested() == 0);
//		streamAssert(GetInfo().GetNumberPriorityObjectsRequested() == 0);
//	}

// A copy of strStreamingEngine::ShutdownLevelWithMapLoaded()
//	with GetInfo().ShutdownLevelWithMapUnloaded() removed
void strStreamingEngine::ShutdownSession()
{
	streamDisplayf("strStreamingEngine::ShutdownSession");

	Flush();

#if USE_DEFRAGMENTATION
	GetDefragmentation()->Abort();
#endif

	Flush();

#if SANITY_CHECK_REQUESTS
	GetInfo().SanityCheckRequests();
#endif
	// remove everything from the request list
	// First, let's keep the dependencies around so we don't get asserts about
	// dependencies being removed while they're still needed.
	while (GetInfo().PurgeRequestList(0, true)) {}

	// Now that dependents are gone, kill of the dependencies.
	GetInfo().PurgeRequestList(0, false); 

#if SANITY_CHECK_REQUESTS
	GetInfo().SanityCheckRequests();
#endif
	// have to shutdown diskcache because I am about to close all the img file handles
	//fiDiskCache::DeleteInstance();

#if __ASSERT
	if (GetInfo().GetNumberObjectsRequested() != 0 || GetInfo().GetNumberPriorityObjectsRequested() != 0)
	{
		streamErrorf("Outstanding requests after a shutdown: %d normal, %d priority",
			GetInfo().GetNumberObjectsRequested(), GetInfo().GetNumberPriorityObjectsRequested());

		RequestInfoList* requests = GetInfo().GetRequestInfoList();

		RequestInfoList::Iterator it(requests->GetFirst());
		while (it.IsValid())
		{
			strIndex index = it.Item()->m_Index;
			strStreamingInfo& info = GetInfo().GetStreamingInfoRef(index);
			it.Next();

			streamErrorf("%d\t%s\t%s\t%s", index.Get(), GetObjectName(index),
				(info.GetFlags() & STRFLAG_PRIORITY_LOAD) ? "PRIORITY" : "normal",
				strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));
		}

		streamErrorf("Sanity check:");

		// Because this is a integrity problem in the streaming system, let's be thorough and go through
		// all files. Maybe the state of a streamable got mixed up.
		strStreamingInfoIterator strIt;

		while (!strIt.IsAtEnd())
		{
			strIndex index = *strIt;
			strStreamingInfo& info = GetInfo().GetStreamingInfoRef(index);

			if (info.GetStatus() == STRINFO_LOADREQUESTED || info.GetStatus() == STRINFO_LOADING)
			{
				streamErrorf("%d\t%s\t%s\t%s", index.Get(), GetObjectName(index),
					(info.GetFlags() & STRFLAG_PRIORITY_LOAD) ? "PRIORITY" : "normal",
					strStreamingInfo::GetFriendlyStatusName(info.GetStatus()));
			}

			++strIt;
		}
	}
#endif // __ASSERT

	streamAssert(GetInfo().GetNumberObjectsRequested() == 0);
	streamAssert(GetInfo().GetNumberPriorityObjectsRequested() == 0);
}

void strStreamingEngine::ShutdownPackfiles()
{
	if(!UseDiskCache() PPU_ONLY(&& !s_GameData))
		strPackfileManager::CloseImageList();
}

void strStreamingEngine::PreLoadedUpdate()
{
#if __BANK
	if (PARAM_aggressivecleanup.Get())
	{
		u32 ignoreFlags = STR_DONTDELETE_MASK | STRFLAG_FORCE_LOAD | STRFLAG_LOADSCENE*ms_bLoadSceneActive;
		strStreamingEngine::GetInfo().ResetInfoIteratorForDeletingNextLeastUsed();
		while (strStreamingEngine::GetInfo().DeleteNextLeastUsedObject(ignoreFlags)) {}
	}
#endif // __BANK
}

// Update the streaming engine.
void strStreamingEngine::Update()
{
#if SANITY_CHECK_LIST
	GetInfo().SanityCheckList();
#endif 

	// We were seeing a lot of lock contention between the Main Thread and the Resource Thread during the streaming
	// label.  To minimize the conflicts, we will now wait until the end of the streaming label to actually submit
	// the requests to the Resource Placement thread
	GetLoader().DeferAsyncPlacementRequests(true);

	PF_START_TIMEBAR_BUDGETED("Load requested", 2.5f); 
	GetLoader().LoadRequestedObjects();

	PF_START_TIMEBAR_DETAIL("Purging");
	u32 ignoreFlags = STR_DONTDELETE_MASK | STRFLAG_FORCE_LOAD | STRFLAG_LOADSCENE*ms_bLoadSceneActive;
	while (GetInfo().PurgeRequestList(ignoreFlags, true)) {}



// Update stats
#if STREAM_STATS
	strStreamingLoader& oddDevice = GetLoader().GetLoader(strStreamingLoaderManager::ODD);
	strStreamingLoader& hddDevice = GetLoader().GetLoader(strStreamingLoaderManager::HDD);

	// numbers
	PF_SET(NumPending, oddDevice.GetPending() + hddDevice.GetPending());
	PF_SET(NumCompleted, oddDevice.GetCompleted() + hddDevice.GetCompleted());
	PF_SET(NumFailed, oddDevice.GetFailed() + hddDevice.GetFailed());
	PF_SET(NumDeleted, GetInfo().GetDeleted());
	PF_SET(NumPurged, GetInfo().GetPurged());

	PF_SET(PendingSize, oddDevice.GetFramePendingSize() + hddDevice.GetFramePendingSize());
	PF_SET(CompletedSize, oddDevice.GetCompletedSize() + hddDevice.GetCompletedSize());
	PF_SET(FailedSize, oddDevice.GetFailedSize() + hddDevice.GetFailedSize());
	PF_SET(DeletedSize, GetInfo().GetDeletedSize());

	PF_SET(RedundantCount, (int) (oddDevice.GetRedundantCount() + hddDevice.GetRedundantCount()));
	PF_SET(OddRollingCompleted, oddDevice.GetRollingCompleted());
	PF_SET(OddRollingRedundant, oddDevice.GetRollingRedundant());
	PF_SET(HddRollingCompleted, hddDevice.GetRollingCompleted());
	PF_SET(HddRollingRedundant, hddDevice.GetRollingRedundant());
#endif

	PF_START_TIMEBAR_DETAIL("Loader update");
	GetLoader().Update();

	GetAllocator().Update();

	GetInfo().Update();

#if SANITY_CHECK_LIST
	GetInfo().SanityCheckList();
#endif 

	strPackfileManager::Update();

	// pgDictionaryBase::SetListOwnerThread(sysIpcCurrentThreadIdInvalid);
}


// Flush the tubes.
void strStreamingEngine::Flush()
{
	GetLoader().Flush();
}

void strStreamingEngine::SubmitDeferredAsyncPlacementRequests()
{
	// Kick off the async placement work now
	GetLoader().DeferAsyncPlacementRequests(false);
	GetLoader().SubmitDeferredAsyncPlacementRequests();

}

#if __PS3
fiInstallerDevice *strStreamingEngine::GetGameData()
{
	return s_GameData;
}
#endif // __PS3


#if !__NO_OUTPUT
bool strStreamingEngine::DebugPrintAllocInfo(const void *ptr)
{
	// If pointer doesn't belong to one of the streaming allocators, then can't AddRef it
	sysMemAllocator *const alloc = ms_allocator.GetPointerOwner(ptr);
	if(!alloc)
	{
		return false;
	}

	// Get the start of the memory allocation
	const void *const block = alloc->GetCanonicalBlockPtr(ptr);
	Displayf("Canonical block ptr %p", block);
	streamAssert(block); // if ptr is in one of the streaming allocators, we expect it to return a memory block

	// The strIndex stored in the allocation user data will be a valid streaming
	// index, iff the allocation was made by a streaming module
	const strIndex strIdx = strStreamingInfoManager::GetStreamingIndexFromMemBlock(alloc, block);
	if(strIdx.IsInvalid())
	{
		Displayf("User data STRINDEX_INVALID");
		return true;
	}

#if !USE_PAGED_POOLS_FOR_STREAMING
	u32 userData = alloc->GetUserData(block);
	if(strIndex(MEM_FIRST_CUSTOM_USER_DATA)<=strIdx && strIdx<=strIndex(MEM_LAST_CUSTOM_USERDATA))
	{
		Displayf("Custom user data 0x%08x", userData);
		return true;
	}

	Displayf("User data 0x%08x", userData);

	strStreamingModule *const module = ms_info.GetModule(strIdx);
	Displayf("Streaming module \"%s\"", module->GetModuleName());
	const strLocalIndex objIdx = strLocalIndex(ms_info.GetObjectIndex(strIdx));
	Displayf("Object index 0x%08x", objIdx.Get());
	const void *const objPtr = module->GetPtr(objIdx);
	Displayf("Object %p", objPtr);
	Displayf("Object name from streaming module \"%s\"", module->GetName(objIdx));
#endif // !USE_PAGED_POOLS_FOR_STREAMING
	return true;
}
#endif // !__NO_OUTPUT


strIndex strStreamingEngine::GetStrIndex(const void *ptr)
{
	// If pointer doesn't belong to one of the streaming allocators, then can't AddRef it
	sysMemAllocator *const alloc = ms_allocator.GetPointerOwner(ptr);
	if(!alloc)
		return strIndex(strIndex::INVALID_INDEX);

	// Get the start of the memory allocation
	const void *const block = alloc->GetCanonicalBlockPtr(ptr);
	streamAssert(block); // if ptr is in one of the streaming allocators, we expect it to return a memory block

	return strStreamingInfoManager::GetStreamingIndexFromMemBlock(alloc, block);
}


bool strStreamingEngine::AddRefByPtr(void *ptr, strRefKind strRefKind)
{
	const strIndex strIdx = GetStrIndex(ptr);
	if(strIdx.IsInvalid())
		return false;

	// The strIndex stored in the allocation user data will be a valid streaming
	// index, iff the allocation was made by a streaming module
	strStreamingModule *const module = ms_info.GetModule(strIdx);
	Assert(module);

	const strLocalIndex objIdx = module->GetObjectIndex(strIdx);
	module->AddRef(objIdx, strRefKind);
	return true;
}


bool strStreamingEngine::RemoveRefByPtr(void *ptr, strRefKind strRefKind)
{
	const strIndex strIdx = GetStrIndex(ptr);
	if(strIdx.IsInvalid())
		return false;

	strStreamingModule *const module = ms_info.GetModule(strIdx);
	Assert(module);

	const strLocalIndex objIdx = module->GetObjectIndex(strIdx);
	module->RemoveRef(objIdx, strRefKind);
	return true;
}


// Return the name of an object.
#if !__NO_OUTPUT
const char* strStreamingEngine::GetObjectName(strIndex index)
{	
	return GetInfo().GetObjectName(index);
}

const char* strStreamingEngine::GetObjectPath(strIndex index)
{	
	return GetInfo().GetObjectPath(index);
}
#endif

#if __BANK
void strStreamingEngine::DebugDraw()
{
	strPackfileManager::DebugDraw();
}

#endif // __BANK

