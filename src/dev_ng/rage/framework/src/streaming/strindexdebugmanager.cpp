//
// streaming/strindexdebugmanager.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

// Main Include ///////////////////////////////////////////////////////////////////////////////////

#include "strindexdebugmanager.h"

// Library Includes ///////////////////////////////////////////////////////////////////////////////

#include <limits.h>

// Module Includes ////////////////////////////////////////////////////////////////////////////////

// Misc Includes //////////////////////////////////////////////////////////////////////////////////

#include "string/stringutil.h"
#include "system/bootmgr.h"
#include "system/memops.h"

// Implementation Guard ///////////////////////////////////////////////////////////////////////////

#if !__FINAL

// Definitions ////////////////////////////////////////////////////////////////////////////////////

// Switch this define on if you want to debug the manager.

#if 1
#define STRINDEX_DEBUG_MANAGER_OUTPUT( ... )
#else
#define STRINDEX_DEBUG_MANAGER_OUTPUT( ... )		streamDisplayf( __VA_ARGS__ )
#endif 

// Namespace //////////////////////////////////////////////////////////////////////////////////////

namespace rage
{

// Static Members /////////////////////////////////////////////////////////////////////////////////

strIndexDebugManager::Entry*	strIndexDebugManager::sm_entries		= NULL;
int								strIndexDebugManager::sm_maxEntryCount	= 0;

int								strIndexDebugManager::sm_typeCount[ TYPE_ENUM_COUNT ];
int								strIndexDebugManager::sm_maxTypeCount[ TYPE_ENUM_COUNT ];

// Methods ////////////////////////////////////////////////////////////////////////////////////////

void strIndexDebugManager::Init( int maxIndexCount )
{
	sysMemAllocator* debugAllocator = sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_DEBUG_VIRTUAL );
	
	if ( debugAllocator == NULL )
	{
		return;
	}

	sm_maxEntryCount = maxIndexCount;

	int memoryRequired = sizeof( Entry ) * sm_maxEntryCount;

	sm_entries = ( Entry* ) debugAllocator->Allocate( memoryRequired, 16 );

	if ( sm_entries == NULL )
	{
		return;
	}

	streamDisplayf( "strIndexDebugManager is using %db (%.2fMB) of debug memory for %d entries.",
		memoryRequired, 
		( float( memoryRequired ) / ( 1024.f * 1024.f ) ), 
		sm_maxEntryCount  );

	// Initialize the structures to their default state.

	for ( int index = 0 ; index < sm_maxEntryCount ; ++index )
	{
		Entry& current = sm_entries[ index ];

		current.m_refCount		= 0;
		current.m_type			= TYPE_INVALID;
		*current.m_description	= '\0';
	}

	for ( int index = 0 ; index < TYPE_ENUM_COUNT ; ++index )
	{
		sm_typeCount[ index ]		= 0;
		sm_maxTypeCount[ index ]	= 0;
	}

	// Make sure the debugger support is initialized.

	sysBootManager::StartDebuggerSupport();

	// Register out entries pointer with key 'stri' so that the debugger can find it.

	sysBootManager::SetDebugDataElement(sysBootManager::MakeFourCc('s','t','r','i'), sm_entries );
}

void strIndexDebugManager::Shutdown()
{
	if ( sm_entries == NULL )
	{
		return;
	}

	sysMemAllocator* debugAllocator = sysMemAllocator::GetMaster().GetAllocator( MEMTYPE_DEBUG_VIRTUAL );

	debugAllocator->Free( sm_entries );

	sm_entries			= NULL;
	sm_maxEntryCount	= 0;

}

char const*	strIndexDebugManager::GetTypeName( TYPE_ENUM type )
{
	char const* descriptionName = NULL;

	switch ( type )
	{

	case TYPE_FILE:
		descriptionName = "file";
		break;

	case TYPE_DUMMY:
		descriptionName = "dummy";
		break;

	default:
		descriptionName = "unknown";
		streamAssert( false );
		break;

	}

	return descriptionName;
}

void strIndexDebugManager::Add( const strIndex& index, TYPE_ENUM type, char const* description )
{
	if ( sm_entries == NULL )
	{
		return;
	}

	STRINDEX_TYPE entryIndex = index.Get();

	streamAssertf( entryIndex >= 0 && entryIndex < ( STRINDEX_TYPE ) sm_maxEntryCount, "strIndex index is too big. Max size is %i but has %i", sm_maxEntryCount, entryIndex );

	Entry& entry = sm_entries[ entryIndex ];

	if ( entry.m_refCount > 0 )
	{
		// Check to make sure the type is still the same.

		streamAssertf( entry.m_type == type, "Type mismatch, old type:%d new type:%d", entry.m_type, type );

		// Check to make sure the description hasn't changed with the new index.

		char temp[ MAX_DESCRIPTION_SIZE ];

		StringCopyTruncateFront( temp, description, MAX_DESCRIPTION_SIZE );

		streamAssertf( stricmp( entry.m_description, temp ) == 0, "Description mismatch, old description:%s new description:'%s'", entry.m_description, temp );
	}
	else
	{
		entry.m_type = ( u8 ) type;

		AddTypeCounter( type );

		StringCopyTruncateFront( entry.m_description, description, MAX_DESCRIPTION_SIZE );
	}

	++entry.m_refCount;

	STRINDEX_DEBUG_MANAGER_OUTPUT(	"AddStrIndex: index:%d refcount:%d %s:%s typecount:%d maxtypecount:%d", 
									entryIndex, 
									entry.m_refCount, 
									GetTypeName( ( TYPE_ENUM ) entry.m_type ),
									entry.m_description,
									sm_typeCount[ entry.m_type ],
									sm_maxTypeCount[ entry.m_type ] );
}

void strIndexDebugManager::Remove( const strIndex& index )
{
	if ( sm_entries == NULL )
	{
		return;
	}

	STRINDEX_TYPE entryIndex = index.Get();

	streamAssertf( entryIndex >= 0 && entryIndex < ( STRINDEX_TYPE ) sm_maxEntryCount, "strIndex index is too big. Max size is %d but has %d", sm_maxEntryCount, entryIndex );

	Entry& entry = sm_entries[ entryIndex ];

	if ( entry.m_refCount == 0 )
	{
		return;
	}

	--entry.m_refCount;

	STRINDEX_DEBUG_MANAGER_OUTPUT(	"RemoveStrIndex: index:%d refcount:%d %s:%s typecount:%d maxtypecount:%d", 
									entryIndex, 
									entry.m_refCount, 
									GetTypeName( ( TYPE_ENUM ) entry.m_type ),
									entry.m_description,
									sm_typeCount[ entry.m_type ],
									sm_maxTypeCount[ entry.m_type ] );

	// If this is the last reference then clear out the entry.

	if ( entry.m_refCount == 0 )
	{
		RemoveTypeCounter( ( TYPE_ENUM ) entry.m_type );

		entry.m_type			= TYPE_INVALID;
		*entry.m_description	= '\0';
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

///////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !__FINAL
