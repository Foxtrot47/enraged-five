//
// streaming/streamingallocator.cpp
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved.
//

#include "streaming/streamingallocator.h"

#include "streaming/streaming_channel.h"
#include "streaming/streamingengine.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingmodule.h"

#include "entity/archetypemanager.h"
#include "fwsys/timer.h"
#include "grcore/resourcecache.h"
#include "math/amath.h"
#include "math/simplemath.h"
#include "paging/rscbuilder.h"
#if ENABLE_BUDDY_ALLOCATOR
#include "system/buddyallocator.h"
#else
#include "system/virtualallocator.h"
#endif
#include "system/criticalsection.h"
#include "system/memory.h"
#include "system/memvisualize.h"
#include "system/stack.h"
#include "system/threadtype.h"

#include "diag/output.h"



STREAMING_OPTIMISATIONS()

#define ALLOCATOR_SANITY_CHECK 0
#define HEAPREPORTOFFSET 0x20000000

#if RESOURCE_POOLS
#define RESOURCE_POOL_SIZE (16 << 20)

#if __PS3
#define RESOURCE_POOL_0 (12 << 20)
#define RESOURCE_POOL_1 (4 << 20)
#elif __XENON
#define RESOURCE_POOL_0 (14 << 20)
#define RESOURCE_POOL_1 (2 << 20)
#endif

CompileTimeAssert(RESOURCE_POOL_0 + RESOURCE_POOL_1 == RESOURCE_POOL_SIZE);
#endif

XPARAM(maponly);

namespace rage {

size_t	strStreamingAllocator::sm_virtualAvailable_Cached     = 0;
size_t	strStreamingAllocator::sm_virtualUsed_Cached          = 0;
size_t	strStreamingAllocator::sm_physicalAvailable_Cached    = 0;
size_t	strStreamingAllocator::sm_physicalUsed_Cached         = 0;
size_t	strStreamingAllocator::sm_externalVirtualUsed_Cached  = 0;
size_t	strStreamingAllocator::sm_externalPhysicalUsed_Cached = 0;

#if !__FINAL && __XENON
extern size_t g_sysExtraMemory;
#endif // !__FINAL && __XENON

#if ENABLE_DEBUG_HEAP
extern size_t g_sysExtraStreamingMemory;
#endif

#if __ASSERT
static bool sStreamingAllocatorInitialized = false;
#endif

static sysCriticalSectionToken AllocatorToken;
bool strStreamingAllocator::s_sloshState = false;

void strStreamingAllocator::Init(size_t virtualSize, size_t physicalSize)
{
#if __ASSERT
	sStreamingAllocatorInitialized = true;
#endif

#if !__FINAL && __XENON
	if (g_sysExtraMemory)
	{
		streamWarningf("Reducing streaming buffer for @extramemory = %d KB", (g_sysExtraMemory >> 10));
		physicalSize -= g_sysExtraMemory;
	}
#endif // !__FINAL && __XENON

#if __PS3 && ENABLE_DEBUG_HEAP
	if (g_sysExtraStreamingMemory)
	{
		streamWarningf("Increasing streaming buffer for @extrastreamingmemory = %d KB", (int)(g_sysExtraStreamingMemory >> 10));
#if ONE_STREAMING_HEAP			
		physicalSize += g_sysExtraStreamingMemory;	
#else
		virtualSize += g_sysExtraStreamingMemory;	
#endif // ONE_STREAMING_HEAP
	}
#endif // ENABLE_DEBUG_HEAP

	{
		sysMemAllocator* allocator = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
#if ENABLE_BUDDY_ALLOCATOR
		streamAssert(dynamic_cast<sysMemBuddyAllocator*>(allocator));
		streamAssert(static_cast<sysMemBuddyAllocator*>(allocator)->GetLeafSize()==g_rscVirtualLeafSize);
#endif
		
		// NOTE: 1 leafsize is lost in the allocator
		size_t size = allocator->GetHeapSize() + g_rscVirtualLeafSize;
		if (size < virtualSize)
		{
			CONSOLE_ONLY(streamAssertf(false, "Not enough memory available (%" SIZETFMT "d) to allocate virtual streaming heap (%" SIZETFMT "d)", size, virtualSize);)
#if !__FINAL && __CONSOLE
			printf("Not enough memory available (%" SIZETFMT "d) to allocate virtual streaming heap (%" SIZETFMT "d)\n", size, virtualSize);
#endif			
			virtualSize = size - 4*1024*1204;
		}
		else
		{
			Assertf(size >= virtualSize + 4 * 1024 * 1024, "Not enough virtual slosh, only %dKB - make sure that the actual buddy heap is at least 4MB larger than the virtual streaming buffer in gameconfig.xml.",
				(u32) ((size - virtualSize) / 1024));
		}
	}

	{
		sysMemAllocator* allocator = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
#if ENABLE_BUDDY_ALLOCATOR
		streamAssert(dynamic_cast<sysMemBuddyAllocator*>(allocator));
		streamAssert(static_cast<sysMemBuddyAllocator*>(allocator)->GetLeafSize()==g_rscPhysicalLeafSize);
#endif

		// NOTE: 1 leafsize is lost in the allocator
		size_t size = allocator->GetHeapSize() + g_rscPhysicalLeafSize;
		if (size < physicalSize)
		{
			CONSOLE_ONLY(streamAssertf(false, "Not enough memory available (%" SIZETFMT "d) to allocate physical streaming heap (%" SIZETFMT "d)", size, physicalSize);)
#if !__FINAL && __CONSOLE
			printf("Not enough memory available (%" SIZETFMT "d) to allocate physical streaming heap (%" SIZETFMT "d)\n", size, physicalSize);
#endif			
			physicalSize = size;
		}
		else
		{
			Assertf(size >= physicalSize + 4 * 1024 * 1024, "Not enough physical slosh, only %dKB - make sure that the actual buddy heap is at least 4MB larger than the virtual streaming buffer in gameconfig.xml.",
				(u32) ((size - physicalSize) / 1024));
		}
	}

#if RSG_PC && !__64BIT
	m_virtualAvailable  = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetHeapSize();
	m_physicalAvailable = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetHeapSize();
#else
	size_t virtualLeafSize = ComputeLeafSize(1, false);
	size_t physicalLeafSize = ComputeLeafSize(1, true);

	m_virtualAvailable = (virtualSize/virtualLeafSize)*virtualLeafSize;
	m_physicalAvailable = (physicalSize/physicalLeafSize)*physicalLeafSize;
#endif // RSG_PC

	m_virtualUsed = 0;
	m_physicalUsed = 0;
	m_externalVirtualUsed = 0;
	m_externalPhysicalUsed = 0;
	
#if !__FINAL
	m_EnableStackTraces = true;
#endif // !__FINAL

#if RESOURCE_POOLS
	NOTFINAL_ONLY(if (!PARAM_maponly.Get()))
	{
		USE_MEMBUCKET(MEMBUCKET_RESOURCE);
		sysMemBuddyAllocator* pBuddyAllocator = verify_cast<sysMemBuddyAllocator*>(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL));

		ASSERT_ONLY(sysMemStreamingCount++;)
		ASSERT_ONLY(sysMemAllowResourceAlloc++;)
		u8* ptr = static_cast<u8*>(pBuddyAllocator->Allocate(RESOURCE_POOL_SIZE, 16, MEMTYPE_RESOURCE_VIRTUAL));
		ASSERT_ONLY(sysMemAllowResourceAlloc--;)
		ASSERT_ONLY(sysMemStreamingCount--;)
		Assertf(ptr, "Unable to allocate resource pool: %" SIZETFMT"d!", RESOURCE_POOL_SIZE);

		if (ptr)
		{
#if RAGE_TRACKING
			if (::rage::diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
			{
				diagTracker::GetCurrent()->Tally(ptr, RESOURCE_POOL_SIZE, MEMTYPE_RESOURCE_VIRTUAL);

				if (sysMemVisualize::GetInstance().HasResource())
					diagTracker::GetCurrent()->InitHeap("Resource Virtual Pool", ptr, RESOURCE_POOL_SIZE);
			}
#endif
			pBuddyAllocator->HideUsedMemory(ptr);
			pBuddyAllocator->InitPool(0, ptr, RESOURCE_POOL_0, g_rscVirtualLeafSize);
			pBuddyAllocator->InitPool(1, ptr + RESOURCE_POOL_0, RESOURCE_POOL_1, g_rscVirtualLeafSize << 1);
		}
	}
#endif

#if __XENON && ENABLE_DEBUG_HEAP
	if (g_sysExtraStreamingMemory > 0)
		Allocate(8 << 20, 16, MEMTYPE_RESOURCE_VIRTUAL);
#endif
}

void strStreamingAllocator::Shutdown()
{
	// nada
}

void strStreamingAllocator::Update()
{
	SanityCheck();
	const s64 MaxBlockSize = 8 * 1024 * 1024;

#if RSG_PC && USE_RESOURCE_CACHE
	m_virtualAvailable  = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetHeapSize();
	m_physicalAvailable = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetHeapSize();

	s64 virtualOver = (m_virtualAvailable > 0) ? 0 : Min((s64)datResourceChunk::MAX_CHUNKS * MaxBlockSize,  0 - (s64)m_virtualAvailable);
	s64 physicalOver = (m_physicalAvailable > 0) ? 0 : Min((s64)datResourceChunk::MAX_CHUNKS * MaxBlockSize, 0 - (s64)m_physicalAvailable);
#else
	// Reduce overallocation
	s64 virtualOver  = rage::Max((s64)0, (s64)m_virtualUsed  - (s64)m_virtualAvailable );
	s64 physicalOver = rage::Max((s64)0, (s64)m_physicalUsed - (s64)m_physicalAvailable);
#endif

#if USE_RESOURCE_CACHE && RESOURCE_CACHE_MANAGING
	// Use once tracking on physicalOver is fixed.
	if (!physicalOver && grcResourceCache::ManageResources())
		physicalOver = rage::Max((s64)0, (s64)-grcResourceCache::GetInstance().GetStreamingMemory());

	physicalOver = Min(datResourceChunk::MAX_CHUNKS * MaxBlockSize, physicalOver);
#endif // USE_RESOURCE_CACHE

	bool haveVirtual = (virtualOver > 0);
	bool havePhysical = (physicalOver > 0);

	if (haveVirtual || havePhysical)
	{
		datResourceMap map;

		int chunkCount = 0;
		s_sloshState = true;

		sysMemSet(&map, 0, sizeof(map));

		s64 virtualLeft = virtualOver;
		s64 physicalLeft = physicalOver;

		while (virtualLeft > 0)
		{
			streamAssertf(chunkCount < datResourceChunk::MAX_CHUNKS, "Too many chunks when trying to free up the overallocation of %" I64FMT "dKB Virt/%" I64FMT "dKB Phys", virtualOver / 1024, physicalOver / 1024);	// Unlikely to happen - more than 1GB overallocation?!

			s64 size = Min(virtualLeft, MaxBlockSize);
			map.Chunks[chunkCount++].Size = (size_t) size;

			virtualLeft -= size;
		}

		map.VirtualCount = (u8) chunkCount;

		while (physicalLeft > 0)
		{
			streamAssertf(chunkCount < datResourceChunk::MAX_CHUNKS, "Too many chunks when trying to free up the overallocation of %" I64FMT "dKB Virt/%" I64FMT "dKB Phys", virtualOver / 1024, physicalOver / 1024);	// Unlikely to happen - more than 1GB overallocation?!

			s64 size = Min(physicalLeft, MaxBlockSize);
			map.Chunks[chunkCount++].Size = (size_t) size;

			physicalLeft -= size;
		}

		map.PhysicalCount = (u8) (chunkCount - map.VirtualCount);

		sysMemAllowResourceAlloc++;
		ASSERT_ONLY(sysMemStreamingCount++;)
		STR_TELEMETRY_MESSAGE_LOG("(streaming/memory)Compensating for over-allocation, %" I64FMT "dKB virt, %" I64FMT "dKB phys", virtualOver / 1024, physicalOver / 1024);
		if (g_strStreamingInterface->MakeSpaceForMap(map, strIndex(), false))
		{
			streamDebugf3("Cleaned overallocation of %" I64FMT "d, %" I64FMT "d", virtualOver, physicalOver);
		}
		else
		{
			streamDebugf3("Failed to clean overallocation of %" I64FMT "d, %" I64FMT "d", virtualOver, physicalOver);

		}
		ASSERT_ONLY(sysMemStreamingCount--;)
		sysMemAllowResourceAlloc--;
	}

	// caching values to be accessed from the outside
	sm_virtualAvailable_Cached     = GetVirtualMemoryAvailable();
	sm_virtualUsed_Cached          = GetVirtualMemoryUsed();
	sm_physicalAvailable_Cached    = GetPhysicalMemoryAvailable();
	sm_physicalUsed_Cached         = GetPhysicalMemoryUsed();
	sm_externalPhysicalUsed_Cached = GetExternalPhysicalMemoryUsed();
	sm_externalVirtualUsed_Cached  = GetExternalVirtualMemoryUsed();
}

__THREAD size_t st_TEMP_DEBUG_Size;
__THREAD void *st_TEMP_DEBUG_lastPtr;
__THREAD int st_TEMP_DEBUG_heapIndex;
static __THREAD int st_External;			// If >0, we're allocating or freeing an external allocation


void strStreamingAllocator::AddToMemoryUsed(const void* ptr)
{
#if RESOURCE_HEADER
	if (sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(ptr))
		return;
#endif

	SYS_CS_SYNC(AllocatorToken);
	streamAssert(sStreamingAllocatorInitialized);
	TrapZ((size_t)ptr);

	size_t size;
#if !ONE_STREAMING_HEAP

	size = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetSize(ptr);
	if (size)
	{
#if __DEV
		streamDebugf3("%d: Allocated %p - virtual %d + %d = %d", fwTimer::GetSystemFrameCount(), ptr, size, m_virtualUsed, m_virtualUsed + size);
#endif

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && sysMemVisualize::GetInstance().HasStreaming())
		{
			void* address = (void*) (0x10000000 ^ (size_t) ptr);
			diagTracker::GetCurrent()->Tally(address, size, 0);
		}
#endif
		m_virtualUsed += size;

		if (st_External)
		{
			streamDebugf3("%d: External Allocate %p - virtual %d + %d = %d", fwTimer::GetSystemFrameCount(), ptr, size, m_externalVirtualUsed, m_externalVirtualUsed + size);		
			m_externalVirtualUsed += size;
		}
	}
	else
#endif // !ONE_STREAMING_HEAP
	{
		size = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetSize(ptr);

		if (!size)
		{
			streamErrorf("Last allocation (result=%p, size=%d, heap=%d) is 0", st_TEMP_DEBUG_lastPtr, (int) st_TEMP_DEBUG_Size, st_TEMP_DEBUG_heapIndex);
		}

		// VirtualAllocator seems to be able to recover from allocation failures. As a result, don't crash.
		//FastAssert(size);
		Assert(size);

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && sysMemVisualize::GetInstance().HasStreaming())
		{
			void* address = (void*) (0x10000000 ^ (size_t) ptr);
			diagTracker::GetCurrent()->Tally(address, size, 0);
		}
#endif

#if __DEV
		streamDebugf3("%d: Allocated %p - physical %" SIZETFMT "d + %" SIZETFMT "d = %" SIZETFMT "d", fwTimer::GetSystemFrameCount(), ptr, size, m_physicalUsed, m_physicalUsed + size);
#endif

#if !FREE_PHYSICAL_RESOURCES
		m_physicalUsed += size;
#endif // !FREE_PHYSICAL_RESOURCES

		if (st_External)
		{
			streamDebugf3("%d: External Allocate %p - physical %" SIZETFMT "d + %" SIZETFMT "d = %" SIZETFMT "d", fwTimer::GetSystemFrameCount(), ptr, size, m_externalPhysicalUsed, m_externalPhysicalUsed + size);	
			m_externalPhysicalUsed += size;
		}
	}
}

bool strStreamingAllocator::RemoveFromMemoryUsed(const void* ptr)
{
#if RESOURCE_HEADER
	if (sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(ptr))
		return false;
#endif

	SYS_CS_SYNC(AllocatorToken);
	streamAssert(sStreamingAllocatorInitialized);
	TrapZ((size_t)ptr);

	size_t size;
#if !ONE_STREAMING_HEAP
	size = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetSize(ptr);
	if (size)
	{
		// FastAssert(size <= m_virtualUsed);
		if(!streamVerifyf(size <= m_virtualUsed, "streaming virtual allocator bookkeeping error"))
			m_virtualUsed = 0;

#if __DEV
		streamDebugf3("%d: Free %p - virtual %d - %d = %d", fwTimer::GetSystemFrameCount(), ptr, size, m_virtualUsed, m_virtualUsed - size);
#endif

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && sysMemVisualize::GetInstance().HasStreaming())
		{
			void* address = (void*) (0x10000000 ^ (size_t) ptr);
			diagTracker::GetCurrent()->UnTally(address, size);
		}
#endif
		if (streamVerifyf(size, "Ptr %p already freed", ptr))
		{
			m_virtualUsed -= size;

			if (st_External)
			{
				streamDebugf3("%d: External Free %p - virtual %d - %d = %d", fwTimer::GetSystemFrameCount(), ptr, size, m_externalVirtualUsed, m_externalVirtualUsed - size);		
				m_externalVirtualUsed -= size;
			}
			return true;
		}
	}
	else
#endif // !ONE_STREAMING_HEAP
	{
		size = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetSize(ptr);
		//FastAssert(size);
		//FastAssert(size <= m_physicalUsed);

#if __DEV
		streamDebugf3("%d: Free %p - physical %" SIZETFMT "d - %" SIZETFMT "d = %" SIZETFMT "d", fwTimer::GetSystemFrameCount(), ptr, m_physicalUsed, size, m_physicalUsed - size);
#endif

		if (streamVerifyf(size, "Ptr %p already freed", ptr))
		{
#if RAGE_TRACKING
			if (diagTracker::GetCurrent() && sysMemVisualize::GetInstance().HasStreaming())
			{
				void* address = (void*) (0x10000000 ^ (size_t) ptr);
				diagTracker::GetCurrent()->UnTally(address, size);
			}
#endif
#if !FREE_PHYSICAL_RESOURCES
			if(!streamVerifyf(size <= m_physicalUsed, "streaming physical allocator bookkeeping error"))
				m_physicalUsed = 0;
#endif // !FREE_PHYSICAL_RESOURCES

#if !FREE_PHYSICAL_RESOURCES
			m_physicalUsed -= size;
#endif // !FREE_PHYSICAL_RESOURCES
			if (st_External) {
				streamDebugf3("%d: External Free %p - physical %" SIZETFMT "d - %" SIZETFMT "d = %" SIZETFMT "d", fwTimer::GetSystemFrameCount(), ptr, size, m_externalPhysicalUsed, m_externalPhysicalUsed - size);
				m_externalPhysicalUsed -= size;
			}
			return true;
		}
	}
	return false;
}

#if ENABLE_BUDDY_ALLOCATOR
size_t strStreamingAllocator::ComputeLeafSize(size_t size, bool physical)
{
	if (size)
	{
		int heap = (physical) ? MEMTYPE_RESOURCE_PHYSICAL : MEMTYPE_RESOURCE_VIRTUAL;
		sysMemBuddyAllocator* allocator = static_cast<sysMemBuddyAllocator*>(sysMemAllocator::GetMaster().GetAllocator(heap));
		streamAssert(allocator);
		return sysMemBuddyAllocator::ComputeNodeSize(size,  allocator->GetLeafSize());
	}
	return 0;
}
#else
size_t strStreamingAllocator::ComputeLeafSize(size_t size, bool /*physical*/)
{
	return sysMemVirtualAllocator::ComputeAllocationSize(size);
}
#endif

void* strStreamingAllocator::Allocate(size_t size, size_t align, int heapIndex /* = 0 */)
{
	if (!(streamVerifyf(size != 0, "Zero-sized allocation on the streaming allocator")))
	{
		return NULL;
	}

#if MEM_VALIDATE_USERDATA
	u32 userData = sysMemCurrentUserData;
	Assertf(userData != MEM_INVALID_USERDATA, "Please provide a valid user data with MEM_USE_USERDATA for better memory tracking.");
#endif // MEM_VALIDATE_USERDATA

	streamDebugf3("strStreamingAllocator::Allocate: %" SIZETFMT "d[%" SIZETFMT "d]/%d", size, align, heapIndex);
	if (sysMemCurrentMemoryBucket == MEMBUCKET_RESOURCE || sysMemCurrentMemoryBucket == MEMBUCKET_STREAMING)
	{
		streamAssertf(false, "Non streaming alloc ends up in streaming bucket");
		USE_MEMBUCKET(MEMBUCKET_DEFAULT);
	}

	if (heapIndex == MEMTYPE_GAME_VIRTUAL || heapIndex == MEMTYPE_GAME_PHYSICAL)
	{
		heapIndex++;
	}

	FastAssert(heapIndex==MEMTYPE_RESOURCE_VIRTUAL || heapIndex==MEMTYPE_RESOURCE_PHYSICAL);
	bool isPhysical = heapIndex > MEMTYPE_RESOURCE_VIRTUAL;
	bool overallocating = false;

#if !__NO_OUTPUT
	size_t originalSize = size;
#endif // !__NO_OUTPUT

	size = ComputeLeafSize(size, isPhysical);

	if (sysThreadType::IsUpdateThread())
	{
		sysMemAllocator& oldAllocator = sysMemAllocator::GetCurrent();
		sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());
		sysMemAllowResourceAlloc++;
		if (!MakeSpaceForAllocation(!isPhysical * size, isPhysical * size))
		{
#if !__NO_OUTPUT
			if ((::rage::DIAG_SEVERITY_DEBUG3) <= (Channel_streaming).MaxLevel)
			{
				streamErrorf("Allocating failed (%" SIZETFMT "d bytes, align=%" SIZETFMT "d, heap %d). Callstack here:", size, align, heapIndex);
				sysStack::PrintStackTrace();
			}
#endif // !__NO_OUTPUT
			overallocating = true;
		}
		sysMemAllocator::SetCurrent(oldAllocator);
		sysMemAllowResourceAlloc--;
	}

	sysMemAllowResourceAlloc++;
	SanityCheck();
	ASSERT_ONLY(sysMemStreamingCount++;)
	void* ptr = sysMemAllocator::GetMaster().RAGE_LOG_ALLOCATE_HEAP(size,align,heapIndex);
	ASSERT_ONLY(sysMemStreamingCount--;)
	st_TEMP_DEBUG_heapIndex = heapIndex;
	st_TEMP_DEBUG_lastPtr = ptr;
	st_TEMP_DEBUG_Size = size;

	sysMemAllowResourceAlloc--;
	if (ptr)
	{
		// First sanity check.
#if __ASSERT
		size_t sanityCheck = sysMemAllocator::GetMaster().GetAllocator(heapIndex)->GetSize(ptr);
		streamAssertf(sanityCheck == size && size != 0, "Allocation sanity check failed - size=%d, ptr=%p, heapIndex=%d, result=%d. Show this to Krehan.",
			(int) size, ptr, heapIndex, (int) sanityCheck);
#endif // __ASSERT
		// Add track of this external allocation
#if __BANK

		m_ExtAllocTracker.AddRecord(ptr, (u32)size, heapIndex, sysMemCurrentMemoryBucket, sysStack::RegisterBacktrace() );

#endif

#if MEM_VALIDATE_USERDATA
		((sysMemBuddyAllocator * )sysMemAllocator::GetMaster().GetAllocator(heapIndex))->SetUserData(ptr, userData);
#endif // MEM_VALIDATE_USERDATA

		StartExternalAllocation();
		AddToMemoryUsed(ptr);
#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
			diagTracker::GetCurrent()->MarkExternal(ptr, true);
#endif
		SanityCheck();
		EndExternalAllocation();

		if (overallocating) 
		{
			streamDebugf3("Overallocating for external allocate");
		}
	}
	else
	{
#if !__FINAL
		if (m_EnableStackTraces)
		{
			sysStack::PrintStackTrace();
		}
#endif // !__FINAL
		streamWarningf("Failed to allocate %" SIZETFMT "d (original %" SIZETFMT "d, physical=%d)", size, originalSize, (int) isPhysical);
	}

	return ptr;
}

#if !__FINAL
size_t strStreamingAllocator::GetVirtualSlosh() 
{
	// Resource Virtual
	sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);

	size_t usedBytes; 
	size_t totalBytes;

	// Streaming Virtual
#if ONE_STREAMING_HEAP
	usedBytes = (size_t) strStreamingEngine::GetAllocator().GetPhysicalMemoryUsed();
	totalBytes = (size_t) strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable();
#else
	usedBytes = (size_t) strStreamingEngine::GetAllocator().GetVirtualMemoryUsed();
	totalBytes = (size_t) strStreamingEngine::GetAllocator().GetVirtualMemoryAvailable();
#endif

	WIN32PC_ONLY(usedBytes = strStreamingEngine::GetAllocator().GetMemoryUsed());
	WIN32PC_ONLY(totalBytes = strStreamingEngine::GetAllocator().GetMemoryAvailable());

	size_t freeBytes = totalBytes - usedBytes;

	size_t virtSlosh = ((s32) pAllocator->GetMemoryAvailable()) - freeBytes;
	return virtSlosh;
}

size_t strStreamingAllocator::GetPhysicalSlosh()
{
	size_t physSlosh = 0;

#ifdef ONE_STREAMING_HEAP
	// Resource Physical
	sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);

	// Streaming Physical
	size_t usedBytes = strStreamingEngine::GetAllocator().GetPhysicalMemoryUsed();
	size_t totalBytes = strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable();
	size_t freeBytes = totalBytes - usedBytes;

	physSlosh = pAllocator->GetMemoryAvailable() - freeBytes;
#endif

	return physSlosh;
}
#endif

void strStreamingAllocator::StartExternalAllocation()
{
	++st_External;
}

void strStreamingAllocator::EndExternalAllocation()
{
	Assert(st_External > 0);
	--st_External;
}

bool strStreamingAllocator::StreamingAllocate(datResourceMap& map, strIndex index, int memBucket, u32 customUserData)
{
	Assert(sysThreadType::IsUpdateThread());


	USE_MEMBUCKET(memBucket);

	SanityCheck();

	sysMemAllowResourceAlloc++;
	ASSERT_ONLY(sysMemStreamingCount++;)

	if (g_strStreamingInterface->MakeSpaceForMap(map, index, true))
	{
		ASSERT_ONLY(sysMemStreamingCount--;)
		sysMemAllowResourceAlloc--;

		if (customUserData == MEM_INVALID_USERDATA)
		{
			customUserData = strStreamingInfoManager::CreateUserDataFromStreamingIndex(index);
			Assertf(customUserData != MEM_INVALID_USERDATA && index.IsValid(), "Please provide a valid customUserData argument for better memory tracking.");
		}

		sysMemAllocator* const virtAlloc = (sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL));
		sysMemAllocator* const physAlloc = (sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL));
		unsigned i = 0;
		const unsigned virtCount = map.VirtualCount;
		for (; i<virtCount; ++i)
		{
			void *const ptr = map.Chunks[i].DestAddr;
			virtAlloc->SetUserData(ptr, customUserData);
			AddToMemoryUsed(ptr);
		}
		const unsigned totCount = map.VirtualCount+map.PhysicalCount;
		for (; i<totCount; ++i)
		{
			void *const ptr = map.Chunks[i].DestAddr;
			physAlloc->SetUserData(ptr, customUserData);
			AddToMemoryUsed(ptr);
		}
		SanityCheck();

		return true;
	}	
	
	// Here because of early exit	
	ASSERT_ONLY(sysMemStreamingCount--;)
	sysMemAllowResourceAlloc--;

	return false;
}


strIndex strStreamingAllocator::GetHeapPageOwner(const void *ptr, bool isPhysical)
{
#if RESOURCE_HEADER
	sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);
	if (pAllocator->IsValidPointer(ptr))
		return strStreamingInfoManager::GetStreamingIndexFromMemBlock(pAllocator, ptr);
#endif

	sysMemAllocator& alloc = *sysMemAllocator::GetMaster().GetAllocator(isPhysical?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL);
	if (alloc.IsValidPointer(ptr))
	{
		return strStreamingInfoManager::GetStreamingIndexFromMemBlock(ptr);
	}
	else
	{
		return strIndex(strIndex::INVALID_INDEX);
	}
}


void* strStreamingAllocator::TryAllocate(size_t size, size_t align, int heapIndex /* = 0 */)
{
	streamDebugf3("strStreamingAllocator::TryAllocate: %" SIZETFMT "d[%" SIZETFMT "d]/%d", size, align, heapIndex);
	if (sysMemCurrentMemoryBucket == MEMBUCKET_RESOURCE || sysMemCurrentMemoryBucket == MEMBUCKET_STREAMING)
	{
		streamAssertf(false, "Non streaming alloc ends up in streaming bucket");
		USE_MEMBUCKET(MEMBUCKET_DEFAULT);
	}

#if MEM_VALIDATE_USERDATA
	u32 userData = sysMemCurrentUserData;
	Assertf(userData != MEM_INVALID_USERDATA, "Please provide a valid user data with MEM_USE_USERDATA for better memory tracking.");
#endif // MEM_VALIDATE_USERDATA

	if (heapIndex == MEMTYPE_GAME_VIRTUAL || heapIndex == MEMTYPE_GAME_PHYSICAL)
	{
		heapIndex++;
	}

	FastAssert(heapIndex==MEMTYPE_RESOURCE_VIRTUAL || heapIndex==MEMTYPE_RESOURCE_PHYSICAL);
	void* ptr = NULL;
	bool isPhysical = heapIndex > MEMTYPE_RESOURCE_VIRTUAL;

	size = ComputeLeafSize(size, isPhysical);

	if (sysThreadType::IsUpdateThread())
	{
		sysMemAllocator& oldAllocator = sysMemAllocator::GetCurrent();
		sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

		sysMemAllowResourceAlloc++;
		bool madeSpace = MakeSpaceForAllocation(!isPhysical * size, isPhysical * size);
		sysMemAllowResourceAlloc--;

		sysMemAllocator::SetCurrent(oldAllocator);

		if (madeSpace)
		{
			sysMemAllowResourceAlloc++;
			SanityCheck();
			ASSERT_ONLY(sysMemStreamingCount++;)
			ptr = sysMemAllocator::GetMaster().RAGE_LOG_ALLOCATE_HEAP(size, align,heapIndex);
			ASSERT_ONLY(sysMemStreamingCount--;)
			sysMemAllowResourceAlloc--;
			if (ptr)
			{
				// Add track of this external allocation
#if __BANK
				m_ExtAllocTracker.AddRecord(ptr, (u32)size, heapIndex, sysMemCurrentMemoryBucket, sysStack::RegisterBacktrace());
#endif

#if MEM_VALIDATE_USERDATA
				((sysMemBuddyAllocator * )sysMemAllocator::GetMaster().GetAllocator(heapIndex))->SetUserData(ptr, userData);
#endif // MEM_VALIDATE_USERDATA

				StartExternalAllocation();
				AddToMemoryUsed(ptr);
#if RAGE_TRACKING
				if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
					diagTracker::GetCurrent()->MarkExternal(ptr, true);
#endif
				EndExternalAllocation();
				SanityCheck();
			}
// 			else
// 			{
// 				NOTFINAL_ONLY(sysStack::PrintStackTrace());
//	 			streamWarningf("Failed to allocate %d", size);
// 			}
		}
	}

	return ptr;
}

void strStreamingAllocator::StreamingFree(datResourceMap& map
#if RSG_PC
										  , bool bFreePhysical
#endif // RSG_PC
										  )
{
	streamAssert(sysThreadType::IsUpdateThread());

#if FREE_PHYSICAL_RESOURCES
	if (!bFreePhysical)
		map.PhysicalCount = 0;
#endif // FREE_PHYSICAL_RESOURCES

	for (int i = 0; i < map.VirtualCount + map.PhysicalCount; ++i)
	{
		RemoveFromMemoryUsed(map.Chunks[i].DestAddr);
	}

	sysMemAllowResourceAlloc++;
	pgRscBuilder::FreeMemory(map);
	sysMemAllowResourceAlloc--;
}

void strStreamingAllocator::StreamingFree(const void* ptr)
{
	if (!ptr)
	{
		return;
	}

	RemoveFromMemoryUsed(ptr);

	sysMemAllocator* alloc = sysMemAllocator::GetMaster().GetPointerOwner(const_cast<void*>(ptr)); 
	if (alloc && alloc == this)
	{
		sysMemAllowResourceAlloc++;
		sysMemAllocator::GetMaster().Free(ptr);
		sysMemAllowResourceAlloc--;
	}
	else if (alloc)
	{
		sysMemAllowResourceAlloc++;
		alloc->Free(ptr);
		sysMemAllowResourceAlloc--;
	}
}

void strStreamingAllocator::Free(const void *ptr)
{
#if __BANK
#if RESOURCE_HEADER
	if (!sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(ptr))
#endif
	{
		m_ExtAllocTracker.RemoveRecord(ptr);
	}	
#endif

	StartExternalAllocation();
	StreamingFree(ptr);
	EndExternalAllocation();
}

sysMemAllocator *strStreamingAllocator::GetPointerOwner(const void *ptr)
{
	sysMemAllocator* const master = &sysMemAllocator::GetMaster();
	sysMemAllocator* const phys = master->GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	if (phys->IsValidPointer(ptr))
		return phys;
	
	sysMemAllocator* const virt = master->GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	if (virt->IsValidPointer(ptr))
		return virt;
	
#if RESOURCE_HEADER
	sysMemAllocator* const header = master->GetAllocator(MEMTYPE_HEADER_VIRTUAL);
	if (header->IsValidPointer(ptr))
		return header;
#endif

	return NULL;
}

size_t strStreamingAllocator::GetSize(const void* ptr) const 
{
	return sysMemAllocator::GetMaster().GetSize(ptr);
}

size_t strStreamingAllocator::GetMemoryUsed(int bucket /* = -1 */)
{
	return sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetMemoryUsed(bucket) + 
		sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetMemoryUsed(bucket);
}

bool strStreamingAllocator::IsValidPointer(const void* ptr) const
{ 
	return sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->IsValidPointer(ptr) || sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->IsValidPointer(ptr);
}

bool strStreamingAllocator::HasSpaceFor(size_t virtualSize, size_t physicalSize)
{
#if USE_RESOURCE_CACHE && RESOURCE_CACHE_MANAGING
	if (grcResourceCache::ManageResources())
	{
		s64 iAvailable = grcResourceCache::GetInstance().GetStreamingMemory();
		if (iAvailable < (s64) (virtualSize + physicalSize))
			return false;
	}
#endif // USE_RESOURCE_CACHE && RESOURCE_CACHE_MANAGING

#if __CONSOLE || !USE_RESOURCE_CACHE
	if ((GetVirtualMemoryUsed() > GetVirtualMemoryAvailable() - virtualSize) ||
		(GetPhysicalMemoryUsed() > GetPhysicalMemoryAvailable() - physicalSize))
		return false;

#else // !__CONSOLE

#if ONE_STREAMING_HEAP
	physicalSize += virtualSize;
#endif // #if ONE_STREAMING_HEAP	

	sysMemAllocator* pAllocator = NULL;
	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	if (pAllocator->GetMemoryAvailable() < virtualSize)
		return false;

	if (pAllocator->GetLargestAvailableBlock() < virtualSize)
		return false;

	pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	if (pAllocator->GetMemoryAvailable() < physicalSize)
		return false;

	if (pAllocator->GetLargestAvailableBlock() < physicalSize)
		return false;
#endif // __CONSOLE

	return true;
}

bool strStreamingAllocator::MakeSpaceForAllocation(size_t virtualSize, size_t physicalSize, bool aggressive, bool forceFree)
{
#if ONE_STREAMING_HEAP
	physicalSize += virtualSize;
	virtualSize = 0;
#endif 

	virtualSize = ComputeLeafSize(virtualSize, false);
	physicalSize = ComputeLeafSize(physicalSize, true);

	strStreamingEngine::GetInfo().ResetInfoIteratorForDeletingNextLeastUsed();

	while(forceFree || !HasSpaceFor(virtualSize, physicalSize))
	{	
		forceFree = false;

		if (!strStreamingEngine::GetInfo().DeleteNextLeastUsedObject(STRFLAG_LOADSCENE, fwArchetypeManager::GetStreamingModule()))
		{
			if (!aggressive)
			{
				// Give up right away?
				return false;
			}

			// Let's try one more time.
			datResourceMap map;
			sysMemSet(&map, 0, sizeof(map));

			map.VirtualCount = (int) (virtualSize != 0);
			map.PhysicalCount = (int) (physicalSize != 0);

			int chunk = 0;

			if (virtualSize)
			{
				map.Chunks[chunk++].Size = virtualSize;
			}

			if (physicalSize)
			{
				map.Chunks[chunk++].Size = physicalSize;
			}

			streamDebugf2("Out of memory - performing aggressive cleanup for external allocation");
			sysMemAllowResourceAlloc++;
			ASSERT_ONLY(sysMemStreamingCount++;)
			bool result = g_strStreamingInterface->MakeSpaceForMap(map, strIndex(strIndex::INVALID_INDEX), false);
			ASSERT_ONLY(sysMemStreamingCount--;)
			sysMemAllowResourceAlloc--;

			return result;
		}
	}
	return true;
}


void strStreamingAllocator::SanityCheck()
{
#if ALLOCATOR_SANITY_CHECK
	SYS_CS_SYNC(AllocatorToken);
#if __ASSERT
#if ONE_STREAMING_HEAP
	size_t physicalAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetMemoryUsed();
	streamAssertf(physicalAllocator == m_physicalUsed,
		"physical %d (%dk) != %d (%dk)",
		physicalAllocator, physicalAllocator >> 10,
		m_physicalUsed, m_physicalUsed >> 10);
#else
	size_t virtualAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetMemoryUsed();
	size_t physicalAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL)->GetMemoryUsed();
	streamAssertf(virtualAllocator == m_virtualUsed,
		"virtual %d (%dk) != %d (%dk)",
		virtualAllocator, virtualAllocator >> 10,
		m_virtualUsed, m_virtualUsed >> 10);
	streamAssertf(physicalAllocator == m_physicalUsed,
		"physical %d (%dk) != %d (%dk)",
		physicalAllocator, physicalAllocator >> 10,
		m_physicalUsed, m_physicalUsed >> 10);
#endif

#endif // __ASSERT


	size_t virtualLoadingSize = 0;
	size_t physicalLoadingSize = 0;

	// count up all loading assets
	strStreamingEngine::GetLoader().GetLoadingFileSizes(virtualLoadingSize, physicalLoadingSize);

	// count up all loaded objects
	strLoadedInfoIterator LoadedStreamInfos;
	strIndex index = LoadedStreamInfos.GetNextIndex();
	size_t virtualLoadedSize = 0;
	size_t physicalLoadedSize = 0;
	while (index.IsValid())
	{
		strStreamingInfo *pCurr = strStreamingEngine::GetInfo().GetStreamingInfo(index);
		if (pCurr->IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
		{
			strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(index);
			pgBase* res = (pgBase*) pModule->GetPtr(pModule->GetObjectIndex(index));
	
			datResourceMap map;
			FastAssert(res);
			res->RegenerateMap(map);

			for (int i = 0; i != map.VirtualCount ; ++i)
			{
				size_t size = strStreamingEngine::GetAllocator().GetSize(map.Chunks[i].DestAddr);
				FastAssert(size);
				virtualLoadedSize += size;
			}

			for (int i = map.VirtualCount; i != map.VirtualCount + map.PhysicalCount; ++i)
			{
				size_t size = strStreamingEngine::GetAllocator().GetSize(map.Chunks[i].DestAddr);
				FastAssert(size);
				physicalLoadedSize += size;
			}
		}
		index = LoadedStreamInfos.GetNextIndex();
	}

	size_t virtualSize = virtualLoadingSize + virtualLoadedSize + m_externalVirtualUsed;
	size_t physicalSize = physicalLoadingSize + physicalLoadedSize + m_externalPhysicalUsed;

	streamAssertf(virtualSize == m_virtualUsed, "Frack - %" SIZETFMT "d vs %" SIZETFMT "d", virtualSize, m_virtualUsed);
	streamAssertf(physicalSize == m_physicalUsed, "DoubleFrack%" SIZETFMT "d vs %" SIZETFMT "d", physicalSize, m_physicalUsed);

	streamDebugf3("virtual counted %d - found %d = %d", m_virtualUsed, virtualSize, m_virtualUsed - virtualSize);
	streamDebugf3("physical counted %d - found %d = %d", m_physicalUsed, physicalSize, m_physicalUsed - physicalSize);
#endif // ALLOCATOR_SANITY_CHECK
}

/////////////////////////////////////////

#if __BANK

void	strExternalAllocTracker::AddRecord(void *pMem, u32 size, int memType, int bucketID, u16 backTraceHandle )
{
	SYS_CS_SYNC(AllocatorToken);

strExternalAlloc thisAlloc;

	USE_DEBUG_MEMORY();

	thisAlloc.m_pMemory = pMem;
	thisAlloc.m_Size = size;
	thisAlloc.m_MemType = (u8)memType;
	thisAlloc.m_BucketID = (s8)bucketID;
	thisAlloc.m_BackTraceHandle = backTraceHandle;

#if __BANK
	char msgBuf[128];
	const char *msgPtr = diagContextMessage::GetTopMessage(msgBuf,sizeof(msgBuf));
	if( msgPtr )
	{
		thisAlloc.m_diagContextMessageHash = atDiagHashString(msgPtr).GetHash();
	}
	else
#endif
	{
		thisAlloc.m_diagContextMessageHash = 0;	// No message
	}

	m_Allocations.PushAndGrow(thisAlloc);
}

void strExternalAllocTracker::RemoveRecord(const void *pMem )
{
	// Trying to free NULL pointers. B*424532/424531
	if(!pMem)
		return;

	SYS_CS_SYNC(AllocatorToken);
	for(int i=0;i<m_Allocations.size();i++)
	{
		if( m_Allocations[i].m_pMemory == pMem )
		{
			m_Allocations.Delete(i);
			return;
		}
	}

	streamAssertf(false, "Couldn't find record of memory: %p. THIS IS PROBABLY A MEMORY LEAK!", pMem);
}

#endif	//__BANK

/////////////////////////////////////////

} // namespace rage

