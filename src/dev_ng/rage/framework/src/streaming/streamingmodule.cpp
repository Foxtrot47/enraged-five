//
// streaming/streamingmodule.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

// --- Include Files ------------------------------------------------------------

#include "streaming/streamingmodule.h"

#include "streaming/streamingengine.h"
#include "streaming/streaminginfo.h"

#include "grcore/debugdraw.h"
#include "system/threadtype.h"


namespace rage {

void strStreamingModule::GetObjectAndDependencies(strIndex index, atArray<strIndex>& allDeps, const strIndex* ignoreList, s32 numIgnores) const
{
	for(s32 i=0; i<numIgnores; i++)
		if(ignoreList[i] == index)
			return;

	if(!strStreamingEngine::GetInfo().IsObjectInImage(index))
		return;

	bool duplicate = false;
	for(s32 i=0; i<allDeps.GetCount(); ++i)
	{
		if(allDeps[i] == index)
		{
			duplicate = true;
			break;
		}
	}

	if (!duplicate)
	{
		allDeps.Append() = index;
		strIndex deps[STREAMING_MAX_DEPENDENCIES];

		s32 numDeps = GetDependencies(GetObjectIndex(index), &deps[0], STREAMING_MAX_DEPENDENCIES);
	#if __ASSERT && !USE_PAGED_POOLS_FOR_STREAMING
		int strIndexCount = strStreamingEngine::GetInfo().GetStreamingInfoSize();
	#endif // __ASSERT

		while (numDeps--)
		{
#if !USE_PAGED_POOLS_FOR_STREAMING
			streamAssertf(deps[numDeps].Get() <= (u32) strIndexCount, "Bad dependency %d", deps[numDeps].Get()); 
#endif // !USE_PAGED_POOLS_FOR_STREAMING
			strStreamingModule *depModule = strStreamingEngine::GetInfo().GetModule(deps[numDeps]);
			depModule->GetObjectAndDependencies(deps[numDeps], allDeps, ignoreList, numIgnores);
		}	
	}
}

#if ENABLE_DEBUG_HEAP
void strStreamingModule::GetObjectAndDependencies(strIndex index, ObjectDependenciesMap& allDepsMap, atArray<strIndex>& allDeps) const
{
	if(!strStreamingEngine::GetInfo().IsObjectInImage(index))
		return;

	bool duplicate = false;

	if(allDepsMap.Access(index.Get()))
		duplicate = true;
	Assert(!duplicate || (*allDepsMap.Access(index.Get()) == index));

	if (!duplicate)
	{
		allDepsMap.Insert(index.Get(), index);
		allDeps.Append() = index;

		strIndex deps[STREAMING_MAX_DEPENDENCIES];

		s32 numDeps = GetDependencies(GetObjectIndex(index), &deps[0], STREAMING_MAX_DEPENDENCIES);
	#if __ASSERT && !USE_PAGED_POOLS_FOR_STREAMING
		int strIndexCount = strStreamingEngine::GetInfo().GetStreamingInfoSize();
	#endif // __ASSERT && !USE_PAGED_POOLS_FOR_STREAMING

		while (numDeps--)
		{
#if __ASSERT && !USE_PAGED_POOLS_FOR_STREAMING
			streamAssertf(deps[numDeps].Get() <= (u32) strIndexCount, "Bad dependency %d", deps[numDeps].Get()); 
#endif // __ASSERT && !USE_PAGED_POOLS_FOR_STREAMING
			strStreamingModule *depModule = strStreamingEngine::GetInfo().GetModule(deps[numDeps]);
			depModule->GetObjectAndDependencies(deps[numDeps], allDepsMap, allDeps);
		}		
	}
}
#endif

size_t strStreamingModule::GetVirtualMemoryOfLoadedObj(strLocalIndex index, bool includeTempMemory) const
{
	size_t result = 0;

	strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(GetStreamingIndex(index));
	streamAssert(info.GetStatus() == STRINFO_LOADED);

	strIndex streamIndex = GetStreamingIndex(index);

#if __BANK
	if (includeTempMemory && RequiresTempMemory(index))
	{
		// If the resource is loaded and using temp memory, ComputeVirtualSize() will not include temp memory since it's
		// not part of the "loaded" memory, so we need to add it.
		result += info.ComputeVirtualSize(streamIndex, true, true);
	}
	else
#endif // __BANK
	if (includeTempMemory || !RequiresTempMemory(index))
	{
		result += info.ComputeOccupiedVirtualSize(streamIndex, false);
	}

	if (UsesExtraMemory())
	{
		result += GetExtraVirtualMemory(index);
	}

	return result;
}

size_t strStreamingModule::GetPhysicalMemoryOfLoadedObj(strLocalIndex index, bool includeTempMemory) const
{
	size_t result = 0;

	strIndex streamIndex = GetStreamingIndex(index);
	strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(streamIndex);
	streamAssert(info.GetStatus() == STRINFO_LOADED);

	if (includeTempMemory || !RequiresTempMemory(index))
	{
		result += info.ComputePhysicalSize(streamIndex, false);
	}

	if (UsesExtraMemory())
	{
		result += GetExtraPhysicalMemory(index);
	}

	return result;
}


#if TRACK_PLACE_STATS

sysTimer strStreamingModule::sm_LastPlaceStart;	// Time when the last place operation started
bool strStreamingModule::sm_DumpPlaceStats;	// Dump place stats to the TTY as they occur

void strStreamingModule::TrackRequest(size_t fileSize)
{
	m_RequestedSize += fileSize;
}

void strStreamingModule::StartTrackingPlace()
{
	// This is a check for the update thread simply because we're storing the timer as a global.
	// We can easily make this TLS or per-module if we ever need to.
	streamAssert(sysThreadType::IsUpdateThread());
	sm_LastPlaceStart.Reset();
}

void strStreamingModule::StopTrackingPlace(const char *name, size_t fileSize)
{
	streamAssert(sysThreadType::IsUpdateThread());

	float timeTaken = sm_LastPlaceStart.GetMsTime();

	m_TotalPlaceTime += timeTaken;
	m_MinPlaceTime = Min(m_MinPlaceTime, timeTaken);

	if (timeTaken > m_MaxPlaceTime)
	{
		m_MaxPlaceTime = timeTaken;
		safecpy(m_SlowestFile, name);
	}

	// Now the "per KB" stats.
	float kbInFloat = (float) fileSize;
	float timeTakenPerKb = timeTaken / kbInFloat;

	m_TotalPlaceTimePerKb += timeTakenPerKb;
	m_MinPlaceTimePerKb = Min(m_MinPlaceTimePerKb, timeTakenPerKb);
	m_MaxPlaceTimePerKb = Max(m_MaxPlaceTimePerKb, timeTakenPerKb);

	m_LoadedSize += fileSize;

	m_FilesPlaced++;

	if (sm_DumpPlaceStats)
	{
		streamDisplayf("PLST\t%s\t%s\t%.3f\t%.4f", name, GetModuleName(), timeTaken, timeTakenPerKb);
	}
}

void strStreamingModule::PrintPlaceStats()
{
	if (m_FilesPlaced)
	{
		float filesPlacedInFloat = (float) m_FilesPlaced;
		grcDebugDraw::AddDebugOutputEx(false, "%-25s %5d %7.2f %7.2f %7.2f %7.2f %9.4f %9.4f %s",
			GetModuleName(), m_FilesPlaced,
			m_MinPlaceTime, m_MaxPlaceTime, m_TotalPlaceTime / filesPlacedInFloat,
			m_MinPlaceTimePerKb, m_MaxPlaceTimePerKb, m_TotalPlaceTimePerKb / filesPlacedInFloat,
			m_SlowestFile);
	}
}

void strStreamingModule::UpdatePlaceStats()
{
	m_LoadedSize = 0;
	m_RequestedSize = 0;
}

void strStreamingModule::ResetPlaceStats()
{
	m_TotalPlaceTime = 0.0f;
	m_MinPlaceTime = FLT_MAX;
	m_MaxPlaceTime = 0.0f;
	m_FilesPlaced = 0;
	m_TotalPlaceTimePerKb = 0.0f;
	m_MinPlaceTimePerKb = FLT_MAX;
	m_MaxPlaceTimePerKb = 0.0f;
	m_SlowestFile[0] = 0;
	m_LoadedSize = 0;
	m_RequestedSize = 0;
}
#endif // TRACK_PLACE_STATS


const char *strStreamingModule::GetRefCountString(strLocalIndex index, char *buffer, size_t bufferLen) const
{
	formatf(buffer, bufferLen, "%d", GetNumRefs(index));
	return buffer;
}

#if USE_PAGED_POOLS_FOR_STREAMING
void strStreamingModule::RegisterPagedPool(void * /*data*/, u32 page, u32 ASSERT_ONLY(elementCount), strStreamingInfoPageHeader &header, size_t * /*memUsage*/)
{
	// Keep track of the offset within our objIndex for this page.
	// We should only have a new page when we reach the boundary of our indices.
	Assert(m_used % elementCount == 0);

	header.SetUserData(m_used);
	m_PoolHeaders.Grow() = (u16) page;
}

void strStreamingModule::FreePagedPool(u32 page, u32 /*elementCount*/, strStreamingInfoPageHeader &/*header*/, size_t * /*memUsage*/)
{
	// TODO: Maybe there's something better than fishing for it?
	int headerCount = m_PoolHeaders.GetCount();
	for (int x=0; x<headerCount; x++)
	{
		if (m_PoolHeaders[x] == page)
		{
			// Found it.
			m_PoolHeaders[x] = 0xffff;	// TODO: What to do here? Delete? DeleteFast? We basically have a gap in our object indices now.
			return;
		}
	}

	Assertf(false, "Page %d doesn't belong to module %d (%s)", page, GetStreamingModuleId(), GetModuleName());
}

strIndex strStreamingModule::AllocateObject()
{
	strIndex index = strStreamingEngine::GetInfo().GetStreamingInfoPool().Allocate(GetStreamingModuleId());
	m_used++;
	Assertf(m_used <= m_MaxLimit, "Streaming module %s exceeded its hard limit of %d elements - please update gameconfig.xml and bump the number up.", GetModuleName(), m_MaxLimit);
	sysMemSet(strStreamingEngine::GetInfo().GetStreamingInfoPool().Get(index), 0, sizeof(strStreamingInfo));

	m_IndexWatermark = Max(m_IndexWatermark, (u32) GetObjectIndex(index)+1);

	return index;
}

void strStreamingModule::FreeObject(int index)
{
	strIndex streamIndex = GetStreamingIndex(index);
	m_used--;
	strStreamingEngine::GetInfo().UnregisterObject(streamIndex);
	strStreamingEngine::GetInfo().GetStreamingInfoPool().Free(streamIndex);

	if (index == (int) m_IndexWatermark-1)
	{
		// TODO: SLOW. Improve.
		do
		{
			m_IndexWatermark--;
		}
		while (m_IndexWatermark > 0 && !strStreamingEngine::GetInfo().GetStreamingInfoPool().IsAllocated(GetStreamingIndex(m_IndexWatermark-1)));
	}
}

s32 strStreamingModule::AllocateNewStreamable()
{
	strIndex index = AllocateObject();

	return (s32) strStreamingEngine::GetInfo().GetStreamingInfoHeader(index).GetUserData() + index.GetElement();
}

s32 strStreamingModule::AllocateSlot(int objIndex)
{
	while (m_used <= (u32) objIndex)
	{
		AllocateObject();
	}

	return objIndex;
}

bool strStreamingModule::IsAllocated(s32 objIndex) const
{
	// If we don't have a page for it, it ain't valid.
	u32 elementsPerPage = strStreamingEngine::GetInfo().GetStreamingInfoPool().GetElementsPerPage((u32) GetStreamingModuleId());
	int pageOffset = objIndex / elementsPerPage;
	if (pageOffset >= m_PoolHeaders.GetCount())
	{
		return false;
	}

	int pageIndex = m_PoolHeaders[pageOffset];

	// TODO: This is what we currently do for freed pages - this will change in future.
	if (pageIndex == 0xffff)
	{
		return false;
	}

	const strStreamingInfoPageHeader &header = strStreamingEngine::GetInfo().GetStreamingInfoPool().GetPageHeader(m_PoolHeaders[pageOffset]);
	FastAssert(header.GetSubType() == (u32) GetStreamingModuleId());	// Make sure this page belongs to us - if not, this index is invalid

	return header.IsElementAllocated(objIndex % elementsPerPage);
}

#endif // USE_PAGED_POOLS_FOR_STREAMING


}	// namespace rage

