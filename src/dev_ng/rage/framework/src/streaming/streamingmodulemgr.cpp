//
// filename:	streamingmodulemgr.cpp
// description:	
//

#include "streaming_channel.h"

STREAMING_OPTIMISATIONS()

// --- Include Files ------------------------------------------------------------
#include "streamingmodulemgr.h"

// C headers
// Rage headers
// Game headers

#include "bank/group.h"
#include "fwtl/Pagedpool.h"
#include "grcore/debugdraw.h"
#include "streamingdefs.h"
#include "streaminginfo.h"
#include "streamingmodule.h"
#include "streamingvisualize.h"
#include "system/memory.h"
#include "system/param.h"
#include "fwtl/assetstore.h"
#include "fwsys/config.h"
#include "fwsys/fileExts.h"


namespace rage {

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------
#if USE_PAGED_POOLS_FOR_STREAMING
strStreamingInfo *StreamingInfoPageAllocator::Allocate(u32 page, strStreamingInfoPageHeader &header, u32 elementCount, u32 alignment, size_t *memUsage)
{
	MEM_USE_USERDATA(MEMUSERDATA_STRINFO);

	u32 subType = header.GetSubType();
	strStreamingModule &module = *strStreamingEngine::GetInfo().GetModuleMgr().GetModule(subType);

	size_t baseMemory = sizeof(strStreamingInfo) * elementCount;
//	size_t padding = baseMemory + (__alignof(S) - 1) % __alignof(S);
	size_t padding = (16 - baseMemory) & 15;
	size_t moduleMemory = module.GetModuleMemoryRequirement(elementCount);
	ASSERT_ONLY(size_t elementSize = sizeof(strStreamingInfo) + moduleMemory / elementCount;)

	size_t pageSizeReq = baseMemory + padding + moduleMemory;

	// Get the streaming info page first.
#if USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS

	ASSERT_ONLY(size_t pageSize = pgRscBuilder::ComputeLeafSize(pageSizeReq, false);)

	Assertf((int) (pageSize - 15 - pageSizeReq) < (int) elementSize,
		"Just letting you know that %d strStreamingInfo per page in %s (%d/%d per element) wasting %d bytes of memory per page. I would recommend "
		"an element count of %d.",
		(int) elementCount, module.GetModuleName(), (int) sizeof(strStreamingInfo), (int) (moduleMemory / elementCount),
		(int) (pageSize - pageSizeReq),
		(int) (pageSize / elementSize));

	USE_MEMBUCKET(MEMBUCKET_WORLD);		// SHOULD be streaming, but that's not allocated in the streaming allocator
	void *data = fwDefaultRscMemPagedPoolAllocatorBase::Allocate(pageSizeReq, alignment, MEMUSERDATA_STRINFO, MEMBUCKET_WORLD);

#if !__FINAL
	if (!data)
	{
		g_strStreamingInterface->FullMemoryReport();
		Quitf("Error allocating %u bytes for a %s page", (u32) pageSizeReq, module.GetModuleName());
	}
#endif // !__FINAL

#else // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	USE_MEMBUCKET(MEMBUCKET_STREAMING);
	void *data = rage_aligned_new(alignment) u8[pageSizeReq];
#endif // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS

	*memUsage = pgRscBuilder::ComputeLeafSize(pageSizeReq, false);

	// Give the module a chance to do something with the page - asset stores will want to create a matching array of definitions.
	module.RegisterPagedPool(data, page, elementCount, header, memUsage);

	streamDebugf2("Allocated new page 0x%x for %s, %u elements, at %p", page, module.GetModuleName(), elementCount, data);

	return reinterpret_cast<strStreamingInfo *>(data);
}

void StreamingInfoPageAllocator::Free(u32 page, u32 elementCount, strStreamingInfo *data, strStreamingInfoPageHeader &header, size_t *memUsage)
{
	u32 subType = header.GetSubType();
	strStreamingModule &module = *strStreamingEngine::GetInfo().GetModuleMgr().GetModule(subType);

	streamDebugf2("Free page 0x%x for %s, %u elements, at %p", page, module.GetModuleName(), elementCount, data);

	*memUsage = pgRscBuilder::ComputeLeafSize(sizeof(strStreamingInfo) * elementCount, false);

	// Tell the module about the disappearing page - asset stores will want to delete the matching array of definitions.
	module.FreePagedPool(page, elementCount, header, memUsage);

	// Delete the actual block of streaming infos.
#if USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	fwDefaultRscMemPagedPoolAllocatorBase::Free(data, MEMUSERDATA_STRINFO);
#else // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	delete[] data;
#endif // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
}

size_t StreamingInfoPageAllocator::ComputePageMemUsage(u32 subType, u32 elementCount)
{
	strStreamingModule &module = *strStreamingEngine::GetInfo().GetModuleMgr().GetModule(subType);
	size_t baseCost = pgRscBuilder::ComputeLeafSize(sizeof(strStreamingInfo) * elementCount, false);
	return baseCost + module.ComputePageMemUsage(elementCount);
}
#endif // USE_PAGED_POOLS_FOR_STREAMING


// --- Globals ------------------------------------------------------------------
#if TRACK_PLACE_STATS
PARAM(dumpplacestats, "[streaming] Dump timing stats for placing resources to TTY");
#endif // TRACK_PLACE_STATS

// --- Static Globals -----------------------------------------------------------
#if __ASSERT
bool s_disallowModuleAdd = false;
#endif
static fwNameRegistrar s_moduleByExtension;


// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------
bool strStreamingModule::StreamingBlockingLoad(strLocalIndex objIndex, s32 flags)
{
	streamAssert(objIndex.Get() >= 0); 
	streamAssert(flags&(STRFLAG_FORCE_LOAD|STRFLAG_DONTDELETE));
	strIndex index = GetStreamingIndex(objIndex);
	if(!strStreamingEngine::GetInfo().RequestObject(index, STRFLAG_PRIORITY_LOAD|flags))
	{
		return false;
	}

	strStreamingEngine::GetLoader().LoadAllRequestedObjects(true); // @todo

	return true;
}


u32 strStreamingModule::GetStreamerReadFlags() const 
{ 
	return 0; 
}

void strStreamingModuleMgr::Init()
{
	s_moduleByExtension.Init(MAX_STREAMING_MODULES);

#if TRACK_PLACE_STATS
	*(strStreamingModule::GetDumpPlaceStatsPtr()) = PARAM_dumpplacestats.Get();
#endif // TRACK_PLACE_STATS
}

void strStreamingModuleMgr::Shutdown()
{
	s_moduleByExtension.Reset();
}

#if __ASSERT
void strStreamingModuleMgr::DisallowModuleAdd()
{
	s_disallowModuleAdd = true;
}
bool strStreamingModuleMgr::IsModuleAddDisallowed()
{
	return s_disallowModuleAdd;
}
#endif

int strStreamingModuleMgr::AddModule(strStreamingModule* pModule) 
{
	streamAssertf(s_disallowModuleAdd == false, "You cannot add anymore streaming modules");

	// if there is a previous module get the index offset and add it size to get the
	// next index offset
#if USE_PAGED_POOLS_FOR_STREAMING
	// Let's readjust the pool page sizes to make sure we're not wasting any memory.
	int elementCount = pModule->GetElementsPerPage();
	size_t baseMemory = sizeof(strStreamingInfo) * elementCount;
	size_t padding = (16 - baseMemory) & 15;
	size_t moduleMemory = pModule->GetModuleMemoryRequirement(elementCount);
	size_t elementSize = sizeof(strStreamingInfo) + moduleMemory / elementCount;

	size_t pageSizeReq = baseMemory + padding + moduleMemory;

	size_t pageSize = pgRscBuilder::ComputeLeafSize(pageSizeReq, false);

	int newElementCount = (int) (pageSize / elementSize);

	if (elementCount != newElementCount)
	{
		Warningf("Readjusting paged pool size for %s from %d to %d", pModule->GetModuleName(), elementCount, newElementCount);
		pModule->SetElementsPerPage(newElementCount);
	}

	strStreamingEngine::GetInfo().GetStreamingInfoPool().SetSubtypeElementCount(m_moduleArray.GetCount(), pModule->GetElementsPerPage());

#if __ASSERT
	u32 maxLimit = (u32) fwConfigManager::GetInstance().GetSizeOfPool(atStringHash(pModule->GetModuleName()), CONFIGURED_FROM_FILE);
	// It's not mandatory to specify a max size, so let's not use "1" in that case.
	if (maxLimit == CONFIGURED_FROM_FILE)
	{
		maxLimit = 0x7fffffff;
	}
	pModule->SetMaxLimit(maxLimit);
#endif // __ASSERT
	RegisterInPoolTracker(&strStreamingEngine::GetInfo().GetStreamingInfoPool(), m_moduleArray.GetCount(), pModule->GetModuleName());
#else // USE_PAGED_POOLS_FOR_STREAMING
	if(m_moduleArray.GetCount() > 0)
		pModule->m_indexOffset = strIndex(m_moduleArray.Top()->m_indexOffset.Get() + m_moduleArray.Top()->m_size);
	else
		pModule->m_indexOffset = strIndex(0);
#endif // !USE_PAGED_POOLS_FOR_STREAMING

	pModule->m_moduleId = m_moduleArray.GetCount();
	m_moduleArray.PushAndGrow(pModule);
	if (pModule->GetModuleTypeIndex() != -1)
	{
		s_moduleByExtension.Insert(atStringHash(strModuleTypeIndexToExtension(pModule->GetModuleTypeIndex())), pModule->m_moduleId);
	}

	streamAssertf(m_moduleArray.GetCount() <= MAX_STREAMING_MODULES, "MAX_STREAMING_MODULES is too small - it needs to be at least %d", m_moduleArray.GetCount());

	STRVIS_REGISTER_MODULE(m_moduleArray.GetCount()-1, pModule->GetModuleName());
	
	return pModule->m_moduleId;
}

#if !USE_PAGED_POOLS_FOR_STREAMING
const strStreamingModuleMgr::ModuleIndexInfo *strStreamingModuleMgr::GetModuleInfoFromIndex(strIndex index) const
{
	int moduleCount = m_ModuleIndexRanges.GetCount();

	streamAssert(moduleCount > 0);

	// Perform a binary search in the list of value ranges to find out which
	// module this index belongs to.
	int minValue = 0;
	int maxValue = moduleCount-1;
	int current = maxValue >> 1;

	// Cache out the pointer - the algorithm below has been shown to be safe for a long time, so
	// there's no point to have the extra expense of a bounds check every time.
	const ModuleIndexInfo *RESTRICT indexInfos = m_ModuleIndexRanges.GetElements();

	while (true)
	{
		const ModuleIndexInfo *RESTRICT info = &indexInfos[current];

		if (index < info->m_LastIndex)
		{
			// We need to move left, or we found the right one.
			if (minValue == current)
			{
				streamAssert(index.Get() >= info->m_Module->m_indexOffset.Get() && index.Get() < info->m_Module->m_indexOffset.Get() + info->m_Module->m_size);
				return info;
			}

			maxValue = current;
			int newCurrent = (minValue + maxValue) >> 1;
			
			if (newCurrent == current)
			{
				newCurrent--;
			}

			current = newCurrent;
		}
		else
		{
			// We have to move to the right.
			minValue = current + 1;

			if (minValue > maxValue)
			{
				Quitf(ERR_STR_MOD_INFO,"Streaming index %d appears to be out of range", index.Get());
				return NULL;
			}

			int newCurrent = (minValue + maxValue) >> 1;
			if (newCurrent == current)
			{
				newCurrent++;
			}

			current = newCurrent;
		}
	}
}
#endif // !USE_PAGED_POOLS_FOR_STREAMING


strStreamingModule* strStreamingModuleMgr::GetModuleFromIndex(strIndex streamingIndex) const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	strStreamingInfoPageHeader &header = strStreamingEngine::GetInfo().GetStreamingInfoHeader(streamingIndex);
	return m_moduleArray[header.GetSubType()];
#else // USE_PAGED_POOLS_FOR_STREAMING
	const ModuleIndexInfo *info = GetModuleInfoFromIndex(streamingIndex);

	return info->m_Module;
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

u8 strStreamingModuleMgr::GetModuleIdFromIndex(strIndex streamingIndex) const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	strStreamingInfoPageHeader &header = strStreamingEngine::GetInfo().GetStreamingInfoHeader(streamingIndex);
	return (u8) header.GetSubType();
#else // USE_PAGED_POOLS_FOR_STREAMING
	const ModuleIndexInfo *info = GetModuleInfoFromIndex(streamingIndex);

	return (u8) info->m_ModuleIndex;
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

// Return a module index given an extension, value is >=0 if valid, -1 otherwise.
strStreamingModule* strStreamingModuleMgr::GetModuleFromFileExtension(const char* extension) const
{
	u32 hash = atStringHash(extension);
	s32 index = s_moduleByExtension.Lookup(hash);
	if (index != -1)
		return m_moduleArray[index];
	
	return NULL;
}

const char *strStreamingModuleMgr::GetFileExtensionFromModule(const strStreamingModule *module) const
{
	return strModuleTypeIndexToExtension(module->GetModuleTypeIndex());
}


//
// name:		strStreamingModuleMgr::GetTotalStreamingEntries
// description:	Return the number of objects that can be registered with the streaming modules. 
//				Basically the entry count of all the modules added up
int strStreamingModuleMgr::GetTotalStreamingEntries()
{
#if USE_PAGED_POOLS_FOR_STREAMING
	int result = 0;
	for (int x=0; x<m_moduleArray.GetCount(); x++)
	{
		if (m_moduleArray[x])
		{
			result += m_moduleArray[x]->GetMaxSize();
		}
	}

	return result;
#else // USE_PAGED_POOLS_FOR_STREAMING
	if(m_moduleArray.GetCount() == 0)
		return 0;
	return m_moduleArray.Top()->m_indexOffset.Get() + m_moduleArray.Top()->m_size;
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

#if !__FINAL
// description:	Return true if a streaming module is required for this extension
//				i.e. #mf - rpf manifest file does not required a module
bool strStreamingModuleMgr::IsModuleRequiredForFileExtension(const char* extension) const
{
	// In the future to allow for multiple exceptions be able to query from streaming interface
	// to allow game-specific extensions to be exceptions. Query would return a list of N extensions that 
	// will be checked against the passed in extension. 
	return (stricmp(MANIFEST_FILE_EXT, extension) == 0)? false : true;
}
#endif

#if !USE_PAGED_POOLS_FOR_STREAMING
void strStreamingModuleMgr::CacheModuleIndexRanges()
{
	int moduleCount = m_moduleArray.GetCount();

	// Let's collect information about the module index ranges in
	// no particular order.
	if (m_ModuleIndexRanges.GetCount() != moduleCount)
	{
		m_ModuleIndexRanges.Reset();
	}

	m_ModuleIndexRanges.Resize(m_moduleArray.GetCount());

	for (int x=0; x<moduleCount; x++)
	{
		ModuleIndexInfo &info = m_ModuleIndexRanges[x];
		strStreamingModule *module = m_moduleArray[x];
		info.m_Module = module;
		info.m_ModuleIndex = x;
		info.m_LastIndex = strIndex(module->m_indexOffset.Get() + module->m_size);
	}

	// Now sort this list.
	m_ModuleIndexRanges.QSort(0, -1, ModuleIndexInfoSortFunc);
}

int strStreamingModuleMgr::ModuleIndexInfoSortFunc(const ModuleIndexInfo *a, const ModuleIndexInfo *b)
{
	// There shouldn't be two modules with the same index range
	streamAssert(a->m_LastIndex != b->m_LastIndex);

	return (a->m_LastIndex > b->m_LastIndex) ? 1 : -1;
}
#endif // !USE_PAGED_POOLS_FOR_STREAMING

void strStreamingModuleMgr::PrintModules()
{
#if !__NO_OUTPUT
	for(int i = 0; i < m_moduleArray.GetCount(); ++i)
	{
		strStreamingModule* pModule = m_moduleArray[i];
#if USE_PAGED_POOLS_FOR_STREAMING
		streamDisplayf("%s [%d]", pModule->GetModuleName(), pModule->m_size);
#else // USE_PAGED_POOLS_FOR_STREAMING
		streamDisplayf("%s [%d, %d]", pModule->GetModuleName(), pModule->m_indexOffset.Get(), pModule->m_size);
#endif // USE_PAGED_POOLS_FOR_STREAMING
	}
#endif
}


const char *strModuleTypeIndexToExtension(int i) {
	const char *result = NULL;
#define CASE(x)	case x##_FILE_ID: result = x##_FILE_EXT; break
	switch (i) {
		FILEEXT_CASES
		default:
			Quitf(ERR_STR_MOD,"Unknown module type index %d",i);
	}
#undef CASE

	return result;
}

const char* strStreamingModuleMgr::getPlatExtFromExtPattern(const char* extPattern)
{
	const char* currExtPattern = NULL;
	const char* targetExtPattern = extPattern;

	targetExtPattern++;

#define CASE(x)	currExtPattern = x##_FILE_EXT; currExtPattern++; if (stricmp(currExtPattern, targetExtPattern) == 0) { return --currExtPattern; }
	FILEEXT_CASES
#undef CASE

	return NULL;
}

#if __BANK

void strStreamingModuleMgr::ResetLoadedCounts()
{
	for(int i=0; i<m_moduleArray.GetCount(); i++)
	{
		m_moduleArray[i]->m_loadedCount=0;
	}
}

#endif // __BANK

#if TRACK_PLACE_STATS
void strStreamingModuleMgr::DumpPlaceStats()
{
	grcDebugDraw::AddDebugOutputEx(false, "%-25s Files     Min     Max     Avg  Min/KB    Max/KB    Avg/KB    Slowest File", "");

	for(int i=0; i<m_moduleArray.GetCount(); i++)
	{
		m_moduleArray[i]->PrintPlaceStats();
	}
}

void strStreamingModuleMgr::ResetPlaceStats()
{
	for(int i=0; i<m_moduleArray.GetCount(); i++)
	{
		m_moduleArray[i]->ResetPlaceStats();
	}
}

#else // TRACK_PLACE_STATS
void strStreamingModuleMgr::DumpPlaceStats() {}
void strStreamingModuleMgr::ResetPlaceStats() {}
#endif // TRACK_PLACE_STATS

#if __BANK
void strStreamingModuleMgr::AddWidgets(bkGroup &group)
{
#if TRACK_PLACE_STATS
	group.AddToggle("Dump Resource Place Stats", strStreamingModule::GetDumpPlaceStatsPtr());
#endif // TRACK_PLACE_STATS
#if USE_PAGED_POOLS_FOR_STREAMING
	group.AddButton("Dump Paged Pool Stats", datCallback(MFA(strStreamingModuleMgr::DumpPagedPoolStats), this));
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

void strStreamingModuleMgr::DumpPagedPoolStats()
{
#if USE_PAGED_POOLS_FOR_STREAMING
	strStreamingEngine::GetInfo().GetStreamingInfoPool().DumpUsage();
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

#endif // __BANK



}	// namespace rage
