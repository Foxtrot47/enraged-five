//
// streaming/packfilemanager.cpp
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved.
//

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "packfilemanager.h"

#include "streamingengine.h"


#include "bank/bank.h"
#include "data/aes.h"
#include "diag/art_channel.h"
#include "file/asset.h"
#include "file/device_installer.h"
#include "file/cachepartition.h"
#include "file/diskcache.h"
#include "file/packfile.h"
#include "file/stream.h"
#include "grcore/debugdraw.h"
#include "fwsys/fileExts.h"
#include "fwtl/assetstore.h"		// for fwNameRegistar
#include "paging/rscbuilder.h"
#include "paging/streamer.h"
#include "profile/timebars.h"
#include "system/bootmgr.h"
#include "system/param.h"
#include "system/memory.h"
#include "system/xtl.h"
#include "diag/output.h"
#include "streaming/defragmentation.h"
#include "streaming/streamingengine.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingmodule.h"
#include "streaming/streamingvisualize.h"
#include "system/bootmgr.h"
#include "system/buddyallocator.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "fwsys/timer.h"

#define INVALID_IMAGE_OFFSET			(0xffffffff)

// We'll start marking archives for recycling once this number is hit
#define RESIDENT_ARCHIVE_SOFT_LIMIT		(256)


PARAM(nocacherpf, "Never evict RPF files");

namespace rage
{

XPARAM(forceboothdd);
XPARAM(forceboothddloose);

static int s_ResidentArchiveSoftLimit = RESIDENT_ARCHIVE_SOFT_LIMIT;
extern const int MaxPackfiles;
class strPackfileStreamingModule : public strStreamingModule
{
public:
	strPackfileStreamingModule(int numArchives);

	virtual strLocalIndex Register(const char* name);

	virtual void Remove(strLocalIndex index);

	virtual void RemoveSlot(strLocalIndex index);

	virtual bool Load(strLocalIndex index, void* object, int size);

	virtual int GetNumRefs(strLocalIndex index) const;

	virtual strLocalIndex FindSlot(const char* name) const;

#if !__FINAL
	// PURPOSE:	return name of object at this slot
	// PARAMS:	index - index of object
	// RETURNS: name of object
	virtual const char* GetName(strLocalIndex index) const;
#endif


};

			


atArray<strStreamingFile> strPackfileManager::ms_files;	
bool				strPackfileManager::ms_bIsInitialised;	
s32					strPackfileManager::ms_StreamingModuleId;
atArray<strStreamingFile *> strPackfileManager::ms_LockedFiles;
int					strPackfileManager::ms_QueuedCount;
atArray<atHashString> strPackfileManager::ms_installedFiles;
atBitSet			strPackfileManager::ms_ManagedCollections;

#if __BANK
atArray<strStreamingFile *> strPackfileManager::ms_UnlockedFiles;
#endif // __BANK


static u8 s_ExtraContentIndex		= 0;
ASSERT_ONLY(static int s_RawStreamerImageIdx	= 0;)

#if __BANK
static bool s_ShowImages = false;
static bool s_ShowArchiveDependencyStats = false;
static bool s_ShowResidentImagesOnly = true;
static bool s_LogCacheActivity = false;
static bool s_ShowPackfileCache = false;
static int s_ShowImageContentsIndex = -1;
#endif // __BANK

#if __DEV
static int s_NumLoads;
static int s_NumLoadsWithMissingArchives;
static float s_TotalArchiveLoadDelay;
#endif // __DEV

extern fiCollection* gGetRawStreamer();

//////////////////////////////////////////////////////////////////////////
// Packfile streaming module

strPackfileStreamingModule::strPackfileStreamingModule(int numArchives)
: strStreamingModule("Archive", PI_ARCHIVE_FILE_ID, numArchives, false, false, 0, 512)
{
}


strLocalIndex strPackfileStreamingModule::Register(const char* name)
{
	char fullName[RAGE_MAX_PATH];
	formatf(fullName, "%s." PI_ARCHIVE_FILE_EXT, name);

	return strLocalIndex(strPackfileManager::AddImageToList(fullName, true, -1, false, 0, false, false));
}

void strPackfileStreamingModule::Remove(strLocalIndex index)
{
	strPackfileManager::UnloadRpf(index.Get());
}

void strPackfileStreamingModule::RemoveSlot(strLocalIndex index)
{
	strPackfileManager::CloseArchive(index.Get());

}

bool strPackfileStreamingModule::Load(strLocalIndex index, void* object, int /*size*/)
{
	strPackfileManager::OnStreamIn(index.Get(), object);
	return true;
}

int strPackfileStreamingModule::GetNumRefs(strLocalIndex index) const
{
	if (index.Get() >= strPackfileManager::GetImageFileCount())
	{
		return 0;
	}

	strStreamingFile *file = strPackfileManager::GetImageFile(index.Get());

	// Note that we don't want any refs if the image is not resident.
	return (file->m_bInitialized) ? file->m_requestCount : 0;
}

#if !__FINAL
const char* strPackfileStreamingModule::GetName(strLocalIndex index) const
{
	if (index.Get() >= strPackfileManager::GetImageFileCount())
	{
		return "<<Invalid packfile>>";
	}

	return strPackfileManager::GetImageFile(index.Get())->m_name.GetCStr();
}
#endif

strLocalIndex strPackfileStreamingModule::FindSlot(const char* name) const
{
	char filename[RAGE_MAX_PATH];
	ASSET.FullPath(filename, sizeof(filename), name, "");

	return strLocalIndex(strPackfileManager::FindImage(filename));
}

//////////////////////////////////////////////////////////////////////////
// Packfile Manager

// Initialise the streaming engine.
void strPackfileManager::Init(int archiveCount)
{
	USE_MEMBUCKET(MEMBUCKET_STREAMING);

	static strPackfileStreamingModule archiveStreamingModule(archiveCount);

//	ms_bDisableStreaming = false;
//	ms_bLoadingPriorityObjects = false;

	ms_QueuedCount = 0;
	ms_bIsInitialised = true;
	ms_StreamingModuleId = strStreamingEngine::GetInfo().GetModuleMgr().AddModule(&archiveStreamingModule);
	ms_files.Reserve(archiveCount);
	sysMemSet(ms_files.GetElements(), 0, archiveCount * sizeof(strStreamingFile));
	ms_ManagedCollections.Init(archiveCount);

	// TODO: We don't need the entire archive count for ms_ResidentFiles, we normally don't have all
	// of them resident in memory. However, we MIGHT while certain debugging flags are on.
	ms_LockedFiles.Reserve(archiveCount);

#if __BANK
	USE_DEBUG_MEMORY();

	// Don't make this too big - we're doing lots of O(n) operations on this.
	ms_UnlockedFiles.Reserve(48);
#endif // __BANK
}


// Shutdown the streaming engine.
void strPackfileManager::Shutdown()
{
	Reset();

	// Currently, nothing else ever deletes the fiPackfile objects. At an earlier point,
	// we attempted to do this in strPackfileManager::CloseImageList(), but apparently
	// the array entries for the files get reused when reloading a level without reallocating
	// the fiPackfiles, so that caused crashes. It should be safe to do here, preventing
	// leaks if properly shutting down strPackfileManager.
	const int count = ms_files.GetCount();
	for(int i = 0; i < count; i++)
	{
		if(ms_ManagedCollections.IsSet(i))
		{
			streamAssert(!ms_files[i].m_bInitialized);
			delete ms_files[i].m_packfile;
			ms_files[i].m_packfile = NULL;
		}
	}

	ms_ManagedCollections.Reset();
}

void strPackfileManager::Reset()
{
}


// Update the streaming engine.
void strPackfileManager::Update()
{
	UnlockLRUPackfile(false);
}

// Returns the filetime of the image of the passed index
u64 strPackfileManager::GetImageTime(u32 index)
{
	return (ms_files[index].m_timestamp);
}

const char *strPackfileManager::GetImageFileNameFromHandle(u32 handle)
{
	strStreamingFile *file = GetImageFileFromHandle(handle);

	if (file)
	{
		return file->m_name.GetCStr();
	}

	return "[loose file]";
}

strStreamingFile* strPackfileManager::GetImageFileFromHandle(u32 handle)
{
	int index = GetImageFileIndexFromHandle(handle).Get();

	if (index != -1) {
		return &ms_files[index];
	}

	return NULL;
}

strLocalIndex strPackfileManager::GetImageFileIndexFromHandle(u32 handle)
{
	int index = fiCollection::GetCollectionIndex(handle);

	// stops reference counting, streaming, and unstreaming of the raw streamer
	if ((u32) index >= (u32) ms_ManagedCollections.GetNumBits() || ms_ManagedCollections.IsClear(index))
	{
		// This is not a file within a packfile.
		return strLocalIndex(-1);
	}

	// We may not have a packfile registsred yet for this image.
	if ((u32) index >= (u32) ms_files.GetCount())
	{
		streamAssertf(false, "Cannot find image index for handle %x, assumed index %d, count=%d", handle, index, ms_files.GetCount());
		return strLocalIndex(-1);
	}
	
	streamAssert(ms_files[index].m_packfile == fiCollection::GetCollection(handle));

	return strLocalIndex(index);
}

static fwNameRegistrar ms_images;

// Initialise list of cd images streaming module uses.
void strPackfileManager::InitImageList()
{
	ms_files.Resize(0);	
	//@@: location STRPACKFILEMANAGER_INITIMAGELIST
	ms_ManagedCollections.Reset();
	if (ms_images.IsInitialized())
		ms_images.Reset();
	ms_images.Init(MaxPackfiles);
}

s32 strPackfileManager::FindImage(const char* pImageName) {
	return ms_images.Lookup(atStringHash(pImageName));
}

strStreamingModule* strPackfileManager::GetStreamingModule()
{
	// this is a temporary method for this as eventually the Archetype will contain the module definition
	return strStreamingEngine::GetInfo().GetModuleMgr().GetModule(ms_StreamingModuleId);
}


// Set the extra data index that will be set on the streaming file 
void strPackfileManager::SetExtraContentIndex(s32 id)
{
	s_ExtraContentIndex = (u8)id;
}


// Add an imagefile to the list of files that the streaming module uses.
s32 strPackfileManager::AddImageToList(const char* pImageName, bool bRegisterFiles, s32 cachePartition, bool locked, u8 contentsType, bool loadCompletely, bool overlay, bool patchFile)
{
	char filename[RAGE_MAX_PATH];
	ASSET.FullPath(filename, sizeof(filename), pImageName, "");
#if RSG_ORBIS
	strlwr(filename);
#endif

	// Does it already exist?
	s32 oldImage = FindImage(filename);
 
	if (oldImage != -1) {
		// Nope, it's already been added.
		strStreamingFile& file = ms_files[oldImage];

		if (!file.m_UserLocked && locked)
		{
			file.m_UserLocked = true;

			if (!file.m_bInitialized)
			{
				file.m_UserLockedAndUnlocked = true;
			}
		}
		if(s_ExtraContentIndex>0)
		{
			const fiDevice *pDev;
			pDev = fiDevice::GetDevice(filename);
			char mappedName[RAGE_MAX_PATH];
			if(pDev)
			{
				pDev->FixRelativeName(mappedName, sizeof(mappedName), filename);
				file.m_StreamerHandle =  pgStreamer::Open(mappedName, NULL, 0);
			}
		}
		return oldImage;
	}

// fixme - KS
//	bool bEnableCache = fiDiskCache::IsCacheEnabled() && (cachePartition == 0);
//	if(!bEnableCache)
//		cachePartition = -1;

	const fiDevice *pDev;
	//	if (bEnableCache)
	//		pDev = fiDiskCache::GetCachedDevice(&file.m_name[0]);
	//	else
	pDev = fiDevice::GetDevice(filename);
	u64 offset = 0;

	int index = -1;

	if (streamVerifyf(pDev, "m_device is NULL for file %s. If this file is part of extra content then check that it is listed in the xlast", filename))
	{
		// This packfile might be mapped by a fiDeviceRelative. Let's find out the REAL name.
		char mappedName[RAGE_MAX_PATH];
		pDev->FixRelativeName(mappedName, sizeof(mappedName), filename);

		pDev = fiDevice::GetDevice(mappedName);

		fiHandle streamingHandle = pDev->OpenBulk(mappedName, offset);
		if (streamVerifyf(streamingHandle != fiHandleInvalid, "%s:Cannot open file", pImageName))
		{
			fiPackfile* pack = rage_new fiPackfile();

			// The image index must match the collection index.
			index = pack->GetPackfileIndex();

			// Insert the packfile at the appropriate collection index.
			u32 streamerHandle = pgStreamer::Open(mappedName, NULL, 0);

			streamDebugf3("Opened %s, streamer handle 0x%x", mappedName, streamerHandle);

			// Make sure the array is big enough.
			if (ms_files.GetCount() <= index)
			{
				Assertf(ms_files.GetCapacity() >= index+1, "Trying to allocate %d archives of %d possible. Increase ArchiveCount in gameconfig.xml", index+1, ms_files.GetCapacity());
#if USE_PAGED_POOLS_FOR_STREAMING
				// Also allocate a matching streamable.
				while (ms_files.GetCount() <= index)
				{
					s32 archIndex = strStreamingEngine::GetInfo().GetModuleMgr().GetModule(ms_StreamingModuleId)->AllocateNewStreamable();
					Assert(archIndex == ms_files.GetCount());
					ms_files.Resize(archIndex+1);
				}
#endif // USE_PAGED_POOLS_FOR_STREAMING
				ms_files.Resize(index+1);
			}

			strStreamingFile& file = ms_files[index];

			streamAssertf(file.m_packfile == NULL, "Trying to register packfile '%s' for collection index %d which already hosts packfile '%s'", mappedName, index, file.m_name.GetCStr());

			ms_ManagedCollections.Set(ms_files.GetCount()-1);

			// Be sure to clear out all the elements we just created.
			memset(&file, 0, sizeof(strStreamingFile));

			file.m_packfile = pack;
			file.m_device = pDev;
			file.m_offset = offset;
			file.m_IsOnHdd = true;
			file.m_IsOnOdd = false;
			file.m_Enabled = true;

#if __XENON && !__NO_OUTPUT
			char finalStr[256];
			formatf(finalStr, "#$ %d: Packfile %s HDD %d\n", index, mappedName, (int) file.m_IsOnHdd);
			OutputDebugString(finalStr);
#endif

			file.m_name = filename;
			file.m_mappedName = mappedName;

			streamDebugf3("Adding packfile %s (mapped name %s) as index %d", filename, mappedName, index);
#if !__FINAL
			if (ms_images.Lookup(file.m_name.GetHash()) != -1)
				Warningf("strPackfileManager::AddImageToList: Adding '%s' (%d) to ms_images twice!", filename, file.m_name.GetHash());
#endif // !__FINAL
			ms_images.Insert(file.m_name.GetHash(), index);

			// get timestamp from the original device as the install device isn't completely setup yet
			file.m_timestamp = pDev->GetFileTime(mappedName);
			Assertf(file.m_timestamp, "Could not get timestamp for packfile %s (%s)", mappedName, file.m_name.GetCStr());

	#if __PPU
			if (strStreamingEngine::GetGameData())
				pDev = strStreamingEngine::GetGameData();
	#endif // __PPU

			file.m_device = pDev;
	#if __XENON
			if(sysBootManager::IsBootedFromDisc() && s_ExtraContentIndex == 0)
				file.m_physicalSectorAddr = XGetFilePhysicalSortKey(streamingHandle);
			else
	#endif
				file.m_physicalSectorAddr = (index<<24);

			pDev->CloseBulk(streamingHandle);

			streamDebugf1("Opening image %s, streamer handle 0x%x", file.m_name.GetCStr(), streamerHandle);
			file.m_bRegister = bRegisterFiles && (streamingHandle != fiHandleInvalid);
			file.m_cachePartition = (s8)cachePartition;
			file.m_extraDataIndex = (u8)s_ExtraContentIndex;
			file.m_bNew = (s_ExtraContentIndex > 0);
			file.m_StreamerHandle = streamerHandle;
			file.m_Locked = false;
			file.m_UserLocked = locked;
			file.m_bNeedsReset = false;
			file.m_IsOverlay = overlay;
			file.m_IsPatchfile = patchFile;
			file.m_CachedHeaderSize = 1;	// Dummy size, just to let SVZ know that this file actually has substance
			file.m_bLoadCompletely = loadCompletely;
			file.m_UserLockedAndUnlocked = locked;
			file.m_contentsType = contentsType;	
			file.m_handle = fiHandleInvalid;
			file.m_containsEncryptedData = false;

#if __DEV
			file.m_EnforceLsnSorting = true;
#endif // __DEV

			u32 entryHandle = strStreamingInfo::MakePackfileHandle(index);
			bool bEncrypted = false;
			strStreamingEngine::GetInfo().RegisterObject(file.m_name.GetCStr(), entryHandle, index, bEncrypted);
		}
	}

	return index;
}

void strPackfileManager::RemountDependentPackfiles(fiHandle oldHandle, fiHandle /*newHandle*/)
{
	for (int x=0; x<ms_files.GetCount(); x++)
	{
		strStreamingFile &file = ms_files[x];
		if (file.m_Enabled)
		{
			if (file.m_packfile && file.m_packfile->IsRpf())
			{
				fiPackfile *packfile = static_cast<fiPackfile *>(file.m_packfile);

				if (packfile->GetPackfileHandle() == oldHandle) {
					streamDebugf3("Remounting pack file %s", packfile->GetDebugName());
					strStreamingEngine::GetInfo().InvalidateFilesForArchive(x, false);
					CloseArchive(x);

					// Update the streamer handle.
					streamDebugf3("Replacing old streamer handle 0x%x...", file.m_StreamerHandle);
					file.m_StreamerHandle = pgStreamer::Open(file.m_mappedName.GetCStr(), NULL, 0);
					streamDebugf3("With 0x%x via mapped name %s", file.m_StreamerHandle, file.m_mappedName.GetCStr());

					LoadImageIfUnloaded(x, false);
					UnloadRpf(x);
				}
			}
		}
	}
}

void strPackfileManager::OpenImageList()
{
	// Sanity check
	streamAssert(gGetRawStreamer()==fiCollection::GetCollection(s_RawStreamerImageIdx));

#if __PPU
	if (strStreamingEngine::GetGameData())
	{
		strStreamingEngine::GetGameData()->InitInstall();

		strStreamingEngine::GetGameData()->InstallOpenedFiles();
		if (!strStreamingEngine::GetGameData()->FinishInstallation())
		{	//	TRC 12.2.6 - "The installed game is corrupted. The game will close. You must delete the installed game and then reinstall it to play this game."
			g_strStreamingInterface->InstallFailure();
#if 0
			if (strStreamingEngine::GetGameData()->InstallStatus() != INSTALL_ERROR_NOT_ENOUGH_SPACE &&
				CSystem::WantToExit() == false)
			{
				CWarningScreen::SetAndWaitOnMessageScreen(WARNING_MESSAGE_STANDARD, "PS3_INSTL_ERR", FE_WARNING_OK);	//	Will wait in here until the player presses the Accept button
			}
			CProfileSettings::GetInstance().SetDontWriteProfile(true);
            CFrontEnd::SetState(FRONTEND_STATE_EXITAPP);
#endif
            return;
		}

#if __PPU
		streamAssertf(false, "FIXPLX - thanks Klaas for passing this shit to the future");
//#if __QUICK_SCAN_FOR_AUTOLOAD_AND_SLOW_SCAN_ON_ANOTHER_THREAD
//		//	Once all game files have been installed, start scanning for the display names and dates of all save games.
//		//	This has to be finished before the free space check for save games takes place at the start of the game.
//		CGenericGameStorage::CreateThreadToReadNamesAndDatesOfSaveGames();
//#endif	//	__QUICK_SCAN_FOR_AUTOLOAD_AND_SLOW_SCAN_ON_ANOTHER_THREAD
#endif	//	__PPU

	}
#endif // __PPU

	for (s32 i=0; i<ms_files.GetCount(); ++i)
	{
		if (ms_files[i].m_Enabled)
		{
			LoadImageIfUnloaded(i);

			strStreamingInfo *archiveInfo = GetStreamingModule()->GetStreamingInfo(strLocalIndex(i));
			strStreamingFile &file = ms_files[i];

			if (file.m_packfile && file.m_packfile->IsPackfile() && archiveInfo->GetStatus() == STRINFO_NOTLOADED && file.m_handle != fiHandleInvalid)
			{
				UnloadRpf(i);
			}

			// Reload user-locked archives.
			// We can't just "not unload" since we have to make sure that the
			// streaming system loads the header.
			if (file.m_UserLocked && file.m_UserLockedAndUnlocked)
			{
				LockArchive(i);
				file.m_UserLockedAndUnlocked = false;
			}
		}
	}
}

void strPackfileManager::RemoveUserLock(s32 imageIndex) {

	strStreamingFile& file = ms_files[imageIndex];

	if (file.m_UserLocked)
	{
		RemoveStreamableReference(imageIndex);
		file.m_UserLocked = false;

		// Note that we're not setting m_UserLockedAndUnlocked here - this function implies
		// that the user lock has been removed for good, so we're not intending on bringing it back.
	}
}

void strPackfileManager::LoadImageIfUnloaded(s32 index, bool checkTimestamp) {

	strStreamingFile& file = ms_files[index];

	const char* name = file.m_mappedName.GetCStr();

	// Don't load the image if it's already resident, or if we were told not to load it.
	if (file.m_handle == fiHandleInvalid && file.m_bRegister)
	{
		if (!file.m_device)
		{
			// fixme - KS
			//#if __PPU
			//				if (strStreamingEngine::GetGameData())
			//					file.m_device = strStreamingEngine::GetGameData();
			//				else
			//#endif // __PPU
			//
			//				if (file.m_cachePartition != -1)
			//					file.m_device = fiDiskCache::GetCachedDevice(&file.m_name[0]);
			//				else
			file.m_device = fiDevice::GetDevice(name);
		}
		u64 timestamp = file.m_device->GetFileTime(name);
		if (!checkTimestamp || timestamp > file.m_timestamp)
		{
			// rescan
			LoadRpf(name, index);

			ms_files[index].m_timestamp = timestamp;
		}
	}
}

strIndex strPackfileManager::RegisterIndividualFile(const char* file, bool quitOnBadVersion, const char *relativePath, bool quitIfMissing,bool overlayIfExists)
{
	const char *fileName = (relativePath) ? relativePath : file;
	const fiDevice *device = fiDevice::GetDevice(file, true);

	if (!device)
	{
		if (quitIfMissing)
		{
			Quitf(ERR_STR_PACK_1,"Cannot get device for file %s - is it missing?", file);
		}
		else
		{
			streamAssertf(false, "Cannot get device for file %s - is it missing?", file);
			return strIndex(strIndex::INVALID_INDEX);
		}
	}

	int imgIndex = device->GetPackfileIndex();
	int entryIndex = 0;

	// Is this file inside a packfile?
	if (device->IsRpf())
	{
		// If so, grab the packfile.
		const fiPackfile *packfile = (const fiPackfile *) device;
		entryIndex = packfile->GetEntryIndex(file);
	}
	else
	{
		// If not, use the raw streamer.
		streamAssert(imgIndex == s_RawStreamerImageIdx);
		entryIndex = gGetRawStreamer()->GetEntryIndex(file);
	}

	if (entryIndex == -1)
	{
		if (quitIfMissing)
		{
			Quitf(ERR_STR_PACK_2,"RegisterIndividualFile on '%s' failed",file);
		}
		else
		{
			streamAssertf(false, "RegisterIndividualFile on '%s' failed",file);
			return strIndex(strIndex::INVALID_INDEX);
		}
	}
	u32 entryHandle = fiCollection::MakeHandle(imgIndex, entryIndex);
	bool bEncrypted = false;
	return strStreamingEngine::GetInfo().RegisterObject(fileName, entryHandle, imgIndex, bEncrypted, quitOnBadVersion,NULL,overlayIfExists);
}

void strPackfileManager::InvalidateIndividualFile(const char* file)
{
	int entryIndex = gGetRawStreamer()->GetEntryIndex(file);
	u32 entryHandle = fiCollection::MakeHandle(gGetRawStreamer()->GetPackfileIndex(),entryIndex);
	pgRawStreamerInvalidateEntry(entryHandle);
}

void strPackfileManager::UnregisterArchive(s32 archiveIndex)
{
	strStreamingFile& file = ms_files[archiveIndex];

	if (file.m_name.IsNotNull())
	{
		ms_images.Delete(file.m_name.GetHash());
	}

	// Must be closed already.
	Assert(!file.m_device);

	delete file.m_packfile;
	file.m_packfile = NULL;

	file.m_name = NULL;
}


void strPackfileManager::CloseArchive(s32 archiveIndex)
{
	strStreamingFile& file = ms_files[archiveIndex];

	if (file.m_name.IsNotNull() && file.m_handle != fiHandleInvalid)
	{
		// Remove the LRU lock.
		if (file.m_Locked)
		{
			int index = ms_LockedFiles.Find(&file);
			streamAssert(index != -1);
			ms_LockedFiles.DeleteFast(index);

			file.m_Locked = false;
			RemoveStreamableReference(archiveIndex);
		}

		// TEMPORARILY release the user lock, but remember to re-lock it.
		if (file.m_UserLocked)
		{
			RemoveStreamableReference(archiveIndex);
			file.m_UserLockedAndUnlocked = true;
		}

		Assertf(file.m_requestCount == 0, "Archive %s has a ref count of %d, but we're trying to close it out right now. Somebody might still be using it, that would cause a crash. Code needs to wait for all streaming activity to stop before closing archives.",
			file.m_name.GetCStr(), file.m_requestCount);

		// Unload the RPF from memory, if it's stil there.
		GetStreamingModule()->StreamingRemove(strLocalIndex(archiveIndex));

		strIndex index = GetStreamingModule()->GetStreamingIndex(strLocalIndex(archiveIndex));
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);

		if (info.GetStatus() != STRINFO_NOTLOADED)
		{
			streamAssertf(false, "Archive %s is still resident", file.m_name.GetCStr());
		}
		else
		{
			// Free the RPF header data.
			UnloadRpf(archiveIndex);


			// Close the file handle for the RPF.
			file.m_timestamp = 0;
			if (file.m_packfile)
				file.m_packfile->Shutdown();
			file.m_handle = fiHandleInvalid;
			file.m_device = NULL;
		}
	}
}

void strPackfileManager::UnregisterDevice(fiDevice *device)
{
	// Are we using this device?
	for (s32 i=0; i<ms_files.GetCount(); ++i)
	{
		strStreamingFile &file = ms_files[i];

		if (file.m_packfile && file.m_device == device)
		{
			// We are.
			Assertf(file.m_bInitialized == false, "Deleting the device for packfile %s while it is still in use", file.m_name.GetCStr());

			file.m_packfile->Shutdown();
			file.m_timestamp = 0;
			file.m_device = NULL;
		}
	}
}

void strPackfileManager::CloseImageList()
{
	for (s32 i=0; i<ms_files.GetCount(); ++i)
	{
		CloseArchive(i);
	}

#if __BANK
	ms_UnlockedFiles.Resize(0);
#endif // __BANK
}

void strPackfileManager::ShutdownImageList()
{
	//if(fiDiskCache::IsCacheEnabled())
	//	fiDiskCache::GetInstance().Empty(); fixme KS

	CloseImageList();
}

void strPackfileManager::SetImageInstalledFlag(const char* filename)
{
	s32 imageIdx  = FindImage(filename);
 
	if (imageIdx != -1) {
		strStreamingFile& file = ms_files[imageIdx];
		file.m_IsOnHdd = true;

#if __XENON
		if (file.m_packfile && file.m_packfile->IsRpf() && static_cast<fiPackfile *>(file.m_packfile)->GetForceOnOdd())
		{
			file.m_IsOnHdd = false;
		}
#endif // __XENON


		// We have to re-register all handles since they're now pointing to the HDD, rather than ODD.
		if (file.m_bInitialized)
		{
			file.m_bNeedsReset = true;
		}
	}
	else
	{
		ms_installedFiles.PushAndGrow(atHashString(filename));
	}
}

void strPackfileManager::CleanUpInstalledFiles()
{
	ms_installedFiles.Reset();
}

fiStream *s_CachedResults;
bool s_WriteCacheFile;

// Simple device for encrypting/decrypting a data stream.  Can only have one actual data stream in flight at once.
class fiEncryptingDevice: public fiDeviceLocal
{
	typedef fiDeviceLocal parent;
public:
	fiEncryptingDevice(AES *aes) : m_AES(aes), m_Offset(0), m_Available(0), m_WriteError(false) { }

	void FlushBuffer(fiHandle handle) const
	{
		if (!m_Available && m_Offset)		// We're writing
		{
			// Fill buffer with known data so decryption is consistent
			while (m_Offset & 15)
				m_Buffer[m_Offset++] = 0;
			m_AES->Encrypt(m_Buffer,m_Offset);
			if (parent::Write(handle,m_Buffer,m_Offset) != (int)m_Offset)
				m_WriteError = true;
		}
		m_Offset = m_Available = 0;
	}
	virtual int Close(fiHandle handle) const
	{
		FlushBuffer(handle);
		return parent::Close(handle);
	}
	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const
	{
		int copied = 0;
		while (bufferSize)
		{
			int remain = m_Available - m_Offset;
			if (remain == 0)
			{
				m_Offset = 0;
				m_Available = parent::Read(handle,m_Buffer,Size);
				m_AES->Decrypt(m_Buffer,m_Available);
				if (!m_Available)
					return copied;
				remain = m_Available;
			}
			int thisCopy = bufferSize < remain? bufferSize : remain;
			sysMemCpy(outBuffer, m_Buffer + m_Offset, thisCopy);
			m_Offset += thisCopy;
			outBuffer = (void*)((char*)outBuffer + thisCopy);
			copied += thisCopy;
			bufferSize -= thisCopy;
		}
		return copied;
	}
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const
	{
		int copied = 0;
		while (bufferSize)
		{
			int remain = Size - m_Offset;
			int thisCopy = bufferSize < remain? bufferSize : remain;
			sysMemCpy(m_Buffer + m_Offset, buffer, thisCopy);
			m_Offset += thisCopy;
			buffer = (const void*)((char*)buffer + thisCopy);
			copied += thisCopy;
			bufferSize -= thisCopy;
			if (m_Offset == Size)
			{
				m_AES->Encrypt(m_Buffer,m_Offset);
				if (parent::Write(handle,m_Buffer,m_Offset) != (int)m_Offset)
					m_WriteError = true;
				m_Offset = 0;
			}
		}
		return copied;
	}
	virtual int Seek(fiHandle h,int o,fiSeekWhence w) const
	{
		// This only works to an address that is a multiple of 16
		FlushBuffer(h);
		return parent::Seek(h,o,w);
	}
	virtual u64 Seek64(fiHandle h,s64 o,fiSeekWhence w) const
	{
		// This only works to an address that is a multiple of 16
		FlushBuffer(h);
		return parent::Seek64(h,o,w);
	}
	bool HadWriteError() const { return m_WriteError; }

	static const unsigned Size = 4096;
	AES *m_AES;
	mutable unsigned m_Offset, m_Available;
	mutable char m_Buffer[Size];
	mutable bool m_WriteError;
};

#if __BANK
#define IMAGEDIRSUFFIX "_bank"
#else
#define IMAGEDIRSUFFIX ""
#endif
PARAM(norpfcache,"Disable LoadImageDirectories RPF cache");

inline void mixhash(u32 &hash,u32 newCode)
{
	// Don't just use XOR, because we want the order of the hashes to matter too.
	hash = ((hash << 7) | (hash >> 25)) ^ newCode;
}

// Load directory info from images.
void strPackfileManager::LoadImageDirectories()
{
	sysTimer t;

	static u8 privateKey[32] = {
#if __PS3
		0xbd, 0x2d, 0x38, 0xa4, 0xbd, 0xe6, 0xc9, 0x3b,
		0xb7, 0xba, 0xb0, 0x2e, 0xe5, 0x59, 0xc1, 0x9b,
		0x80, 0xb2, 0x32, 0xe2, 0x10, 0xbb, 0x23, 0x80,
		0xbc, 0x6f, 0x2c, 0x99, 0x6d, 0x8c, 0xfc, 0x48,
#elif __XENON
		0xb4, 0x63, 0x73, 0xa7, 0x78, 0xe1, 0xc1, 0x82,
		0x81, 0xa8, 0xbf, 0x17, 0x99, 0xad, 0xd8, 0x0c,
		0xb1, 0xeb, 0xdc, 0xb3, 0x49, 0xa7, 0xcc, 0xb8,
		0xa2, 0x57, 0xa4, 0xad, 0xe4, 0x18, 0x44, 0xa5,
#else
		0xbd, 0x26, 0x45, 0x17, 0xd0, 0xcc, 0x35, 0x75,
		0x81, 0x71, 0x31, 0x66, 0x08, 0x80, 0xe1, 0xfe,
		0xb7, 0x61, 0xa1, 0xa7, 0x2f, 0x0a, 0x78, 0xab,
		0x87, 0x0e, 0xee, 0x96, 0x30, 0x34, 0xba, 0x26,
#endif
	};
	AES cacheAes(privateKey);
	fiEncryptingDevice cacheDev(&cacheAes);
	int magicValue = 0xDCE00001;		// increment this to invalidate existing cache files on format changes
	struct cacheHeader {
		int magic;
		int filecount;
		u32 namehash;
		int pad0;
	};
	CompileTimeAssert(sizeof(cacheHeader) == 16);		// necessary because compression happens on 16-byte boundaries
	
	strStreamingEngine::GetLoader().CallKeepAliveCallbackIfNecessary();

    bool ngHdd = false;
#if RSG_DURANGO || RSG_ORBIS
    // when using -forceboothdd we mount the folders on the host pc as well, meaning the cache could store
    // absolute paths to old rpfs, which we don't want
    ngHdd = fiDeviceInstaller::GetIsBootedFromHdd() && !PARAM_forceboothdd.Get() && !PARAM_forceboothddloose.Get();
#endif
	if ((sysBootManager::IsBootedFromDisc() || ngHdd) && fiCachePartition::IsAvailable() && !PARAM_norpfcache.Get())
	{
		char cacheName[RAGE_MAX_PATH];
		formatf(cacheName,"%spfm" IMAGEDIRSUFFIX ".dat",fiCachePartition::GetCachePrefix());
		s_CachedResults = fiStream::Open(cacheName,cacheDev,false);
		// If there was a file, make sure the header is valid and consistent with our current file list
		if (s_CachedResults)
		{
			cacheHeader check;
			s_CachedResults->Read(&check,sizeof(check));
			u32 namehash = 0;
			for (int i = 0; i < ms_files.GetCount(); ++i)
			{
				// this code is instantaneous, no reason to do this. strStreamingEngine::GetLoader().CallKeepAliveCallback();
				if (ms_files[i].m_bRegister && !ms_files[i].m_bInitialized)
					mixhash(namehash,ms_files[i].m_name.GetHash());
			}
			if (check.magic != magicValue || check.filecount != ms_files.GetCount() || check.namehash != namehash)
			{
				streamWarningf("Archive cache file header didn't match, invalidating");
				s_CachedResults->Close();
				s_CachedResults = NULL;
			}
		}
		// Try to create (or recreate) a cache file, installing a temporary header.
		if (!s_CachedResults)
		{
			s_WriteCacheFile = true;
			s_CachedResults = fiStream::Create(cacheName,cacheDev);
			if (s_CachedResults)
			{
				// Write a temporary header that will never pass muster
				cacheHeader temp = { 0, 0, 0, 0 };
				s_CachedResults->Write(&temp,sizeof(temp));
			}
		}
	}

	// register dvd image files
	u32 namehash = 0;
	for (int i = 0; i < ms_files.GetCount(); ++i)
	{
		if (ms_files[i].m_bRegister && !ms_files[i].m_bInitialized)
		{
			const char* name = ms_files[i].m_name.GetCStr();
			mixhash(namehash,ms_files[i].m_name.GetHash());
			DIAG_CONTEXT_MESSAGE("registered packfile %s",name);
			LoadRpf(name, i);

			strStreamingEngine::GetLoader().CallKeepAliveCallbackIfNecessary();
		}
	}
	streamWarningf("%f seconds in LoadImageDirectories",t.GetTime());

	if (s_CachedResults)
	{
		// Only validate the cache if every write succeeded (handles the cache drive filling up)
		if (s_WriteCacheFile && !cacheDev.HadWriteError())
		{
			cacheHeader valid = { magicValue, ms_files.GetCount(), namehash, 0 };
			s_CachedResults->Seek(0);
			s_CachedResults->Write(&valid,sizeof(valid));
		}
		s_CachedResults->Close();
		s_CachedResults = NULL;
		s_WriteCacheFile = false;
	}

#if LIVE_STREAMING
	strStreamingEngine::GetLive().Update(); 
#endif // LIVE_STREAMING


	// Unload all RPFs again.
	for (int i = 0; i < ms_files.GetCount(); ++i)
	{
		if (ms_files[i].m_bRegister)
		{
			strStreamingInfo *archiveInfo = GetStreamingModule()->GetStreamingInfo(strLocalIndex(i));

			if (archiveInfo->GetStatus() == STRINFO_NOTLOADED)
			{
				UnloadRpf(i);
			}
		}
	}
}


static const fiPackEntry* s_Entries;

int cmpRemap(const void *a,const void *b) {
	int ia = *(int*)a;
	int ib = *(int*)b;

	u32 offsetA = s_Entries[ia].IsDir()? 0 : s_Entries[ia].GetFileOffset();
	u32 offsetB = s_Entries[ib].IsDir()? 0 : s_Entries[ib].GetFileOffset();
	if (offsetA > offsetB)
		return +1;
	else if (offsetB > offsetA)
		return -1;
	else
		return 0;
}

bool strPackfileManager::LoadRpf(const char* name, s32 imgIndex)
{
	// Don't load twice!
	streamAssert(ms_files[imgIndex].m_bInitialized == false);

	// Create the new pack file
	bool success = false;

	strStreamingFile &file = ms_files[imgIndex];

	streamAssert(file.m_packfile->IsPackfile());
	fiPackfile *pack = static_cast<fiPackfile*>(ms_files[imgIndex].m_packfile);

	char mappedName[RAGE_MAX_PATH];
	fiDevice::GetDevice(name)->FixRelativeName(mappedName, sizeof(mappedName), name);
	bool loadCompletely = file.m_bLoadCompletely;

	if (s_CachedResults && !s_WriteCacheFile && !loadCompletely)
	{
		if (pack->RestoreState(s_CachedResults))
		{
			u32 ec;
			file.m_handle = pack->GetPackfileHandle();
			s_CachedResults->Read(&file.m_CachedHeaderSize,sizeof(file.m_CachedHeaderSize));
			s_CachedResults->Read(&ec,sizeof(ec));
			file.m_bInitialized = true;
			bool bEncrypted = false;

			for (u32 i=0; i<ec; i++)
			{
				int sl = s_CachedResults->GetCh();
				char fullName[ STORE_NAME_LENGTH ];
				u32 entryHandle;
				fiPackEntry peTemp;
				s_CachedResults->Read(fullName,sl);
				s_CachedResults->Read(&entryHandle,sizeof(entryHandle));
				s_CachedResults->Read(&peTemp,sizeof(peTemp));
				fullName[sl] = 0;

				// store that pack file contain encrypted data
				if(!peTemp.IsResource() && peTemp.u.file.m_Encrypted != 0)
					bEncrypted = true;
				
				strStreamingEngine::GetInfo().RegisterObject(fullName, entryHandle, imgIndex, bEncrypted, true, &peTemp);
				strStreamingEngine::GetLoader().CallKeepAliveCallbackIfNecessary();
			}
			file.m_containsEncryptedData = bEncrypted;
			pack->SetKeepNameHeap(file.m_containsEncryptedData);

			g_strStreamingInterface->PostLoadRpf(imgIndex);
			return true;
		}
		else
		{
			streamWarningf("Found mismatch in cached version of '%s', invalidating archive cache.",mappedName);
			s_CachedResults->Seek(0);
			s_CachedResults->Write("InvalidFile12345",16);
			s_CachedResults->Close();
			s_CachedResults = NULL;
		}
	}

	// Initialize the packfile
	if (streamVerifyf(pack->Init(mappedName, true, fiDeviceInstaller::GetIsBootedFromHdd() ? fiPackfile::CACHE_INSTALLER_FILE : fiPackfile::CACHE_NONE), "Unable to initialise rpf %s (mapped to %s)", name, mappedName))
	{
		file.m_handle = pack->GetPackfileHandle();
		const fiPackEntry* entries = pack->GetEntries();
		u32 entryCount = pack->GetEntryCount();

		size_t headerSize = (loadCompletely) ? (size_t) fiDevice::GetDevice(name)->GetFileSize(name) : (size_t) pack->GetHeaderSize(true);
#if STORE_FILE_SIZES
		strIndex imgStrIndex = GetStreamingModule()->GetStreamingIndex(strLocalIndex(imgIndex));
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(imgStrIndex);
		size_t size = pgRscBuilder::ComputeLeafSize(headerSize, false);
		info.SetVirtualSize((u32) size);
		info.SetPhysicalSize(0);
#endif // STORE_FILE_SIZES
	
		strIndex index = strIndex(strIndex::INVALID_INDEX);

		// TODO: We could cache the size with and without metadata, but the streaming system
		// doesn't really support two separate sizes for one streamable.
		file.m_CachedHeaderSize = headerSize;
		file.m_bInitialized = true;

		if (s_CachedResults && !loadCompletely)
		{
			pack->SaveState(s_CachedResults);
			u32 ec = 0;
			for (u32 i=0; i<entryCount; ++i)
				if (!entries[i].IsDir())
					++ec;
			s_CachedResults->Write(&file.m_CachedHeaderSize,sizeof(file.m_CachedHeaderSize));
			s_CachedResults->Write(&ec,sizeof(ec));
		}
		bool bEncrypted = false;
		for (u32 i=0; i<entryCount; ++i) {
			const fiPackEntry& pe = entries[i];
			
			if (!pe.IsDir())
			{
				// Every single file (not directory) will be registered with the streaming system.
				u32 entryHandle = fiCollection::MakeHandle(pack->GetPackfileIndex(),ptrdiff_t_to_int(&pe - entries));
			
				char fullName[ STORE_NAME_LENGTH ];
				pack->GetEntryFullName(entryHandle, fullName, STORE_NAME_LENGTH);				
				index = strStreamingEngine::GetInfo().RegisterObject(fullName, entryHandle, imgIndex, bEncrypted);
				if (s_CachedResults && !loadCompletely)
				{
					int sl = istrlen(fullName);
					s_CachedResults->PutCh((u8)sl);
					s_CachedResults->Write(fullName,sl);
					s_CachedResults->Write(&entryHandle,sizeof(entryHandle));
					s_CachedResults->Write(&pe,sizeof(pe));
					strStreamingEngine::GetLoader().CallKeepAliveCallbackIfNecessary();
				}

				// store that pack file contain encrypted data
				if(!pe.IsResource() && pe.u.file.m_Encrypted != 0)
					bEncrypted = true;
			}
		}
		file.m_containsEncryptedData = bEncrypted;
		pack->SetKeepNameHeap(file.m_containsEncryptedData);

#if __BANK
		// Keep track of the memory usage.
		// Since we're drawing memory from the buddy heap, we're using 4KB blocks.
		// TODO: Would be nice to use a RAGE function for that, if only to handle
		// the case when a large buddy allocator has us use 8KB blocks.
		file.m_memoryUsage = (pack->GetHeaderSize(true) + 4095) & 0xfffff000;
#endif // __BANK

		g_strStreamingInterface->PostLoadRpf(imgIndex);

		success = true;
	}
	return success;
}

bool strPackfileManager::IsFileInstalled(const char* filename)
{
	if (fiDeviceInstaller::GetIsBootedFromHdd())
	{
		return true;
	}

	atHashValue hashedFile(filename);
	for (s32 i = 0; i < ms_installedFiles.GetCount(); ++i)
	{
		if (ms_installedFiles[i].GetHash() == hashedFile.GetHash())
		{
			return true;
		}
	}
	return false;
}

void strPackfileManager::UnloadRpf(s32 imgIndex)
{
	streamAssert(sysThreadType::IsUpdateThread());

	strStreamingFile &file = ms_files[imgIndex];
	fiCollection *packFile = file.m_packfile;

	file.m_bNeedsReset = false;

	streamAssert(!packFile || packFile->IsPackfile());
	if (packFile && file.m_bInitialized && packFile->IsPackfile())
	{
		// Unregister the contents of this file.
		fiPackfile *pack = static_cast<fiPackfile*>(packFile);

		if (file.m_bLoadCompletely)
		{
			pack->Shutdown();
		}
		else
		{
			pack->UnInit();
		}

		file.m_bInitialized = false;

#if __BANK
		if (s_LogCacheActivity)
		{
			streamDisplayf("Unloaded %s", file.m_name.GetCStr());
		}
#endif // __BANK

		if (file.m_RawHeaderData)
		{
			streamDebugf1("Free RPF %s (%d) at %p", file.m_name.GetCStr(), imgIndex, file.m_RawHeaderData);
			strStreamingEngine::GetAllocator().StreamingFree(file.m_RawHeaderData);
			file.m_RawHeaderData = NULL;
		}
	}
}

bool strPackfileManager::IsImageForHandleResident(u32 handle)
{
	int index = fiCollection::GetCollectionIndex(handle);
	return ms_files[index].m_bInitialized;
}

bool strPackfileManager::IsImageResident(s32 imageIndex)
{
	return ms_files[imageIndex].m_bInitialized;
}



void strPackfileManager::OnStreamIn(int imageIndex, void *headerData)
{
	streamAssert(sysThreadType::IsUpdateThread());

	strStreamingFile &file = ms_files[imageIndex];

	streamAssert(!file.m_bInitialized);

	// TODO: Initializing the packfile may or may not be slow.
	// We're holding up the streamer thread here. It would be considerate to
	// do heavy lifting in a separate thread.
	streamAssert(file.m_packfile->IsPackfile());
	streamAssertf(!file.m_RawHeaderData, "RPF file %s (%d) is loaded twice and leaking (old ptr=%p)", file.m_name.GetCStr(), imageIndex, file.m_RawHeaderData);
	file.m_RawHeaderData = headerData;
	streamDebugf1("Loaded RPF %s (%d) at %p", file.m_name.GetCStr(), imageIndex, file.m_RawHeaderData);

	fiPackfile *pack = static_cast<fiPackfile*>(file.m_packfile);

#if RSG_PC
	// HACK - talk to Klaas before changing this
	// Summary: If the header has >= 64 bytes of space in it, then bitwise or together all of those bits. If the sum of the
	// OR'ing is < 64 (Such as in the hacked RPFs we've seen, where the dead space is filled with 0x00), then set off an
	// anti-tamper (Via the fwTimer::GetSystemTimer().Knock(collect); call).
	// If there is < 64 bytes of space in it, then it's not safe to OR the space together, so don't execute the Knock() func.
	size_t alignedCachedHeaderSize = (file.m_CachedHeaderSize + 4095) & ~4095U;
	ptrdiff_t difference = alignedCachedHeaderSize - file.m_CachedHeaderSize;

	// OR bits 6..11 on top of each other; an obfuscated version of: size_t isDiffUnder64 = difference < 64 ? 1 : 0;
	size_t isDiffUnder64 = ((difference & 0xe00) >> 9) | ((difference & 0x1c0) >> 6); // isDiffOver64 now packed to 3 bits (and inverted)
	isDiffUnder64 = (isDiffUnder64 & 0x1) | ((isDiffUnder64 & 0x2) >> 1) | ((isDiffUnder64 & 0x4) >> 2);
	isDiffUnder64 ^= 1;

	// If difference >= 64 and we're not loading the RPF completely (small RPF), then we want to execute the loop, so set collect to zero
	size_t doLoop = !!(difference & 0xfc0) * (1 - (u8)file.m_bLoadCompletely);
	size_t collect = (*(u32*)headerData * isDiffUnder64) + (*(u32*)headerData * (u8)file.m_bLoadCompletely);
	Assert(!!doLoop != !!collect);

	// If difference < 64 then !!(difference & 0xfc0) == 0, so break loop. Else it evaluates to 1, so run normally.
	for (size_t cursor = file.m_CachedHeaderSize; cursor < alignedCachedHeaderSize * doLoop; cursor += sizeof(size_t))
	{
		size_t* header = (size_t*)((char*)headerData + cursor);
		collect |= *header; 
	}
	streamAssertf(collect >= 64, "RPF file %s (%d) has random padding that is less random that expected!", file.m_name.GetCStr(), imageIndex);
	fwTimer::GetSystemTimer().Knock(collect);
	// HACK - talk to Klaas before changing this
#endif

	if (file.m_bLoadCompletely)
	{
		// Initialize the packfile
		ASSERT_ONLY(bool success =) pack->Init(file.m_name.GetCStr(), file.m_containsEncryptedData, fiPackfile::CACHE_MEMORY, (char *) headerData);
		Assertf(success, "Unable to completely load %s", file.m_name.GetCStr());
		file.m_handle = pack->GetPackfileHandle();
	}
	else
	{
		pack->ReInit(file.m_name.GetCStr(), file.m_containsEncryptedData, headerData);
	}

	file.m_bInitialized = true;

#if __BANK
	file.m_StreamInCount++;
#endif // __BANK

	MarkImageQueued(imageIndex, false);
}

s32 strPackfileManager::UnlockLRUPackfile(bool forceUnlock)
{
	// For debugging - if this is set, never evict any RPF files.
	if ((!RSG_PS3 && !RSG_XENON) || PARAM_nocacherpf.Get())
	{
		return -1;
	}

	int count = ms_LockedFiles.GetCount();
	u32 oldestTimestamp = ~0U;
	int bestIndex = -1;

	for (int x=0; x<count; x++)
	{
		strStreamingFile &file = *ms_LockedFiles[x];

		// Consider only resident/queued files, and those that
		// are currently locked.
		if(file.m_bInitialized)
		{
			streamAssert(file.m_Locked);

			// Don't consider those that are still being used.
			// We're checking for 1 since being locked increments
			// the count.
			if (file.m_requestCount == 1)
			{
				if (file.m_bNeedsReset || file.m_LastUse < oldestTimestamp)
				{
					// New LRU archive.
					oldestTimestamp = file.m_LastUse;
					bestIndex = x;
				}
			}
		}
	}

	// Don't evict anything if we don't have at least n number of files resident.
	if (bestIndex != -1 && (forceUnlock || (ms_QueuedCount + ms_LockedFiles.GetCount() >= s_ResidentArchiveSoftLimit)))
	{
		// Mark for recycling.
		strStreamingFile &file = *ms_LockedFiles[bestIndex];
		file.m_Locked = false;

#if __BANK
		if (s_LogCacheActivity)
		{
			streamDisplayf("Unlocking packfile %s - added at %d", file.m_name.GetCStr(), oldestTimestamp);
		}
#endif // __BANK

		ms_LockedFiles.DeleteFast(bestIndex);

#if __BANK
		// Add it to the head of the unlocked list.
		if (ms_UnlockedFiles.GetCount() == ms_UnlockedFiles.GetCapacity())
		{
			ms_UnlockedFiles.Pop();
		}

		ms_UnlockedFiles.Insert(0) = &file;
#endif // __BANK

		int archiveIndex = (int) (&file - ms_files.GetElements());
		RemoveStreamableReference(archiveIndex);

		streamDebugf2("Archive %d (%s) is LRU (timestamp=%d) - marking for potential recycling (%d remain locked)", archiveIndex, file.m_name.GetCStr(), oldestTimestamp, ms_LockedFiles.GetCount());
		return archiveIndex;
	}

	return -1;
}

bool strPackfileManager::DeleteLRUPackfile()
{
	strLocalIndex packfileIndex = strLocalIndex(UnlockLRUPackfile(true));

	if (packfileIndex != -1)
	{
		if (GetStreamingModule()->GetNumRefs(packfileIndex) == 0)
		{
			streamDebugf2("Deleting LRU packfile %s (status=%s)", ms_files[packfileIndex.Get()].m_name.GetCStr(), GetStreamingModule()->GetStreamingInfo(packfileIndex)->GetFriendlyStatusName());
			strStreamingEngine::GetInfo().RemoveObject(GetStreamingModule()->GetStreamingIndex(packfileIndex));
			return true;
		}
	}

	return false;
}

void strPackfileManager::LockArchive(s32 archiveIndex)
{
	AddStreamableReference(archiveIndex);

	// If it's not resident yet, actively stream it in.
	strIndex archiveStrIndex = GetStreamingModule()->GetStreamingIndex(strLocalIndex(archiveIndex));

	strStreamingInfo& info = strStreamingEngine::GetInfo().GetStreamingInfoRef(archiveStrIndex);

	if (info.GetStatus() == STRINFO_NOTLOADED)
	{
		strStreamingEngine::GetInfo().RequestObject(archiveStrIndex, STRFLAG_FORCE_LOAD);
	}
}

void strPackfileManager::UnlockArchive(s32 archiveIndex)
{
	strPackfileManager::RemoveStreamableReference(archiveIndex);
}

void strPackfileManager::RemoveFromLRUList(s32 archiveIndex)
{
	strStreamingFile &file = ms_files[archiveIndex];

	if (file.m_Locked)
	{
		file.m_Locked = false;

		int index = ms_LockedFiles.Find(&file);
		streamAssert(index != -1);
		ms_LockedFiles.DeleteFast(index);

		RemoveStreamableReference(archiveIndex);

		if (file.m_bInitialized && file.m_requestCount == 0)
		{
			GetStreamingModule()->StreamingRemove(strLocalIndex(archiveIndex));
			UnloadRpf(archiveIndex);
		}
	}
}


void strPackfileManager::MarkImageQueued(int imageIndex, bool queued)
{
	if (ms_files[imageIndex].m_IsQueued != queued)
	{
		ms_files[imageIndex].m_IsQueued = queued;

		if (queued)
		{
			ms_QueuedCount++;
		}
		else
		{
			ms_QueuedCount--;
		}
	}
}


void strPackfileManager::AddStreamableReference(s32 imageIndex)
{
	strStreamingFile &file = ms_files[imageIndex];
	file.m_requestCount++;

	// Did we just exceed the range of our data type?
	streamAssertf(file.m_requestCount < 1000, "Reference count overflow for %s is %d - looks like a leak.", file.m_name.GetCStr(), file.m_requestCount);
	streamAssertf(file.m_requestCount != 0, "Reference count overflow for %s", file.m_name.GetCStr());

	// Update timestamp so we know which one is LRU
	file.m_LastUse = TIME.GetFrameCount();

	streamDebugf2("New reference to archive %s, timestamp=%d", file.m_name.GetCStr(), file.m_LastUse);

	// Since we just used this, lock it so the LRU eviction can't touch it.
	if (!file.m_Locked)
	{
		file.m_Locked = true;
		file.m_requestCount++;

		streamAssertf(file.m_requestCount != 0, "Reference count overflow");

		streamAssert(ms_LockedFiles.Find(&file) == -1);
		ms_LockedFiles.Append() = &file;

#if __BANK
		int unlockedIndex = ms_UnlockedFiles.Find(&file);
		if (unlockedIndex != -1)
		{
			ms_UnlockedFiles.Delete(unlockedIndex);
		}

		if (s_LogCacheActivity)
		{
			streamDisplayf("Now locking packfile %s (at %d)", file.m_name.GetCStr(), TIME.GetFrameCount());
		}
#endif // __BANK
	}

	// Make room - mark old archives for eviction.
	// UnlockLRUPackfile();
}

void strPackfileManager::RemoveStreamableReference(s32 imageIndex)
{
	streamAssertf(ms_files[imageIndex].m_requestCount != 0, "Mismatched reference count for a streamable file %s", ms_files[imageIndex].m_name.GetCStr());

	ms_files[imageIndex].m_requestCount--;

	if (ms_files[imageIndex].m_requestCount == 0 )
	{
		STRVIS_AUTO_CONTEXT(strStreamingVisualize::RPFPURGE);

		// We're no longer necessary. Let the streamer kill this file if necessary.
		// TODO: This shouldn't really be necessary, but in real life, it is.
		// The streamer has issues with FORCE_LOAD being set after a file is resident.
		strIndex index = GetStreamingModule()->GetStreamingIndex(strLocalIndex(imageIndex));
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);
		if (info.GetStatus() == STRINFO_LOADREQUESTED)
			strStreamingEngine::GetInfo().RemoveObject(index);

#if __BANK
		if (s_LogCacheActivity)
		{
			streamDisplayf("Releasing packfile %s", ms_files[imageIndex].m_name.GetCStr());
		}
#endif // __BANK
	}
}


// Return the name of an object.
#if __BANK
void strPackfileManager::DebugDraw()
{
	if (s_ShowArchiveDependencyStats)
	{
#if __DEV
		int avgDelayMs = 0;

		if (s_NumLoadsWithMissingArchives)
		{
			avgDelayMs = (int) (s_TotalArchiveLoadDelay * 1000.0f / (float) s_NumLoadsWithMissingArchives);
		}
		grcDebugDraw::AddDebugOutputEx(false, "Requests with missing archive dependencies: %d/%d, avg delay: %dms", s_NumLoadsWithMissingArchives, s_NumLoads, avgDelayMs);

#endif // __DEV
	}
	if (s_ShowImages)
	{
		strStreamingModule *module = GetStreamingModule();
		int fileCount = ms_files.GetCount();
		u32 totalSize = 0;

		// Calculate the stats up front.
		int residentCount = 0;
		int queuedCount = 0;

		for (int i = 0; i < fileCount; ++i)
		{
			strStreamingFile &file = ms_files[i];

			if ( !s_ShowResidentImagesOnly || file.m_bInitialized || file.m_IsQueued)
			{
				totalSize += file.m_memoryUsage / 1024;
			}

			if (file.m_bInitialized)
			{
				residentCount++;
			}

			if (file.m_IsQueued)
			{
				queuedCount++;
			}
		}

		grcDebugDraw::AddDebugOutputEx(false, "Resident: %d, Queued up: %d", residentCount, queuedCount);
		grcDebugDraw::AddDebugOutputEx(false, "%-75s     (%2dKB)", "TOTAL", totalSize);

		for (int i = 0; i < fileCount; ++i)
		{
			strStreamingFile &file = ms_files[i];

			if ( !s_ShowResidentImagesOnly || file.m_bInitialized || file.m_IsQueued)
			{
				// Get the streaming item associated with this.
				strIndex index = module->GetStreamingIndex(strLocalIndex(i));
				strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);

				grcDebugDraw::AddDebugOutputEx(false, "%-60s %2d %15s (%2dKB) %-12s %5d %5d %s%s [%s/%s] %s",
					file.m_name.GetCStr(),
					file.m_requestCount,
					(file.m_bInitialized) ? "RESIDENT" : "Non-resident",
					file.m_memoryUsage / 1024,
					strStreamingInfo::GetFriendlyStatusName(info.GetStatus()),
					file.m_LastUse,
					file.m_StreamInCount,
					(file.m_IsQueued) ? "QUEUED " : "",
					(file.m_Locked) ? "LOCKED " : "",
					(file.m_IsOnOdd) ? "O" : ".",
					(file.m_IsOnHdd) ? "H" : ".",
					(file.m_bLoadCompletely) ? "COMP" : ""
					);
			}
		}
	}

	if (s_ShowImageContentsIndex != -1 && s_ShowImageContentsIndex < ms_files.GetCount())
	{
		strStreamingFile &file = ms_files[s_ShowImageContentsIndex];

		grcDebugDraw::AddDebugOutputEx(true, "%s: %s, Ref count %d, memory usage=%dKB",
			file.m_name.GetCStr(),
			(file.m_packfile != NULL) ? "RESIDENT" : "Non-resident",
			file.m_requestCount,
			file.m_memoryUsage / 1024);
		
		// Get its contents.
		strStreamingEngine::GetInfo().DebugShowImageContents(file);
	}

	if (s_ShowPackfileCache)
	{
		ShowPackfileCache();
	}
}

bool SortPackfileByAge(const strStreamingFile *a, const strStreamingFile *b) { return a->m_LastUse > b->m_LastUse; }


void strPackfileManager::ShowPackfileCache()
{
	// Locked ones first.
	int lockedCount = ms_LockedFiles.GetCount();

	grcDebugDraw::BeginDebugOutputColumnized(44, 35);
	grcDebugDraw::AddDebugOutputEx(false, "LOCKED: %d", lockedCount);

	if (lockedCount)
	{
		strStreamingFile **sortedFiles = Alloca(strStreamingFile *, lockedCount);

		sysMemCpy(sortedFiles, ms_LockedFiles.GetElements(), lockedCount * sizeof(strStreamingFile *));
		std::sort(sortedFiles, sortedFiles + lockedCount, SortPackfileByAge);

		for (int x=0; x<lockedCount; x++)
		{
			//char line[256];
			strStreamingFile *file = sortedFiles[x];

			grcDebugDraw::AddDebugOutputEx(false, Color32(0xffeeeeee), "%-30s%s%d %d",
				ASSET.FileName(file->m_name.GetCStr()), (file->m_bNeedsReset) ? "*" : " ", file->m_requestCount, file->m_LastUse);

			if (grcDebugDraw::GetDebugOutputColumCount() == 3)
			{
				break;
			}
		}

		while (grcDebugDraw::GetDebugOutputColumCount() < 3)
		{
			grcDebugDraw::NewDebugOutputColumn(35);
		}
	}

	// Now the unlocked ones.
	grcDebugDraw::AddDebugOutputEx(false, "UNLOCKED");

	int unlockedCount = ms_UnlockedFiles.GetCount();

	for (int x=0; x<unlockedCount; x++)
	{
		strStreamingFile *file = ms_UnlockedFiles[x];
		Color32 color(file->m_bInitialized ? 0xffeeeeee : 0xffff2222);

		grcDebugDraw::AddDebugOutputEx(false, color, ASSET.FileName(file->m_name.GetCStr()));
	}

	grcDebugDraw::EndDebugOutputColumnized();
}

void strPackfileManager::DumpPackfiles()
{
	strStreamingModule *module = GetStreamingModule();
	int fileCount = ms_files.GetCount();
	u32 totalSize = 0;

	// Calculate the stats up front.
	int residentCount = 0;
	int queuedCount = 0;

	for (int i = 0; i < fileCount; ++i)
	{
		strStreamingFile &file = ms_files[i];

		totalSize += file.m_memoryUsage / 1024;

		if (file.m_bInitialized)
		{
			residentCount++;
		}

		if (file.m_IsQueued)
		{
			queuedCount++;
		}
	}

	streamDisplayf("Resident: %d, Queued up: %d", residentCount, queuedCount);
	streamDisplayf("%-75s     (%2dKB)", "TOTAL", totalSize);

	streamDisplayf("%-60s,%2s,%15s,%-6s,%-12s,%s,%-10s,%4s,%s", "Name", "Rq", "Resident", "Size", "Status", "Index", "LoadCount", "Flags", "Queued/Locked");

	for (int i = 0; i < fileCount; ++i)
	{
		strStreamingFile &file = ms_files[i];

		// Get the streaming item associated with this.
		strIndex index = module->GetStreamingIndex(strLocalIndex(i));
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);

		streamDisplayf("%-60s,%2d,%15s,%2dKB,%-12s,%d,%04x,%-10d,%s%s",
			file.m_name.GetCStr(),
			file.m_requestCount,
			(file.m_bInitialized) ? "RESIDENT" : "Non-resident",
			file.m_memoryUsage / 1024,
			strStreamingInfo::GetFriendlyStatusName(info.GetStatus()),
			index.Get(),
			info.GetFlags(),
			file.m_StreamInCount,
			(file.m_IsQueued) ? "QUEUED " : "",
			(file.m_Locked) ? "LOCKED " : ""
			);
	}
}

void strPackfileManager::AddWidgets(bkBank &bank)
{
	bank.PushGroup("Pack File Manager");
	bank.AddToggle("Show Archives", &s_ShowImages);
	bank.AddToggle("Show Archive Dependency Stats", &s_ShowArchiveDependencyStats);
	bank.AddToggle("Show Resident/Queued Images Only", &s_ShowResidentImagesOnly);
	bank.AddToggle("Show Packfile Cache", &s_ShowPackfileCache);
	bank.AddToggle("Log Packfile Cache Activity", &s_LogCacheActivity);
	bank.AddSlider("Show Image Contents", &s_ShowImageContentsIndex, -1, 500, 1);
	bank.AddSlider("Resident Archive Soft Limit", &s_ResidentArchiveSoftLimit, 0, 2000, 1);
	bank.AddButton("Dump Archive Information", datCallback(CFA(strPackfileManager::DumpPackfiles)));
	bank.AddButton("Evict all unused images", datCallback(CFA(strPackfileManager::EvictUnusedImages)));
	bank.AddButton("Delete LRU Packfile", datCallback(CFA(strPackfileManager::DeleteLRUPackfile)));	
	bank.PopGroup();
}
#endif // __BANK

void strPackfileManager::EvictUnusedImages()
{
	int imageCount = ms_files.GetCount();

	for (int x=0; x<imageCount; x++)
	{
		strStreamingFile &file = ms_files[x];

		// Remove the LRU lock.
		if (file.m_Locked)
		{
			int index = ms_LockedFiles.Find(&file);
			streamAssert(index != -1);
			ms_LockedFiles.DeleteFast(index);

			file.m_Locked = false;
			RemoveStreamableReference(x);
		}

		if (file.m_bInitialized && file.m_requestCount == 0)
		{
			GetStreamingModule()->StreamingRemove(strLocalIndex(x));
			UnloadRpf(x);
		}
	}

#if __BANK
	ms_UnlockedFiles.Resize(0);
#endif
}

#if __DEV
void strPackfileManager::IncrementNumLoads()
{
	s_NumLoads++;
}

void strPackfileManager::RegisterStreamingDelay(float delay)
{
	s_NumLoadsWithMissingArchives++;
	s_TotalArchiveLoadDelay += delay;
}

#endif // __DEV

} // namespace rage


