//
// streaming/requestrecorder.cpp
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "requestrecorder.h"

#include "packfilemanager.h"
#include "streamingengine.h"
#include "streaminginfo.h"
#include "streamingmodule.h"

#include "fwscene/stores/psostore.h"
#include "fwsys/fileExts.h"
#include "parser/macros.h"
#include "parser/manager.h"
#include "system/platform.h"

#include "requestrecorder_parser.h"


namespace rage {

strRequestRecorder gRequestRecorder;


strIndex strResourceReference::Resolve(bool ASSERT_ONLY(assertIfMissing)) const
{
	// Get the module first.
	char extension[sizeof(m_Extension)];

	memcpy(extension, m_Extension, sizeof(m_Extension));

	if (extension[0] == '#')
	{
		extension[0] = g_sysPlatform;
	}

	strStreamingModule *module = strStreamingEngine::GetInfo().GetModuleMgr().GetModuleFromFileExtension(extension);

	if (module)
	{
		strLocalIndex slot = module->FindSlot(m_AssetName.c_str());

		if (slot != -1)
		{
			return module->GetStreamingIndex(slot);
		}
	}

	Assertf(!assertIfMissing, "Unable to resolve %s.%s - %s", m_AssetName.c_str(), m_Extension, (module) ? "Asset not found" : "Module not found");
	return strIndex();
}

#if !__FINAL
void strResourceReference::Set(strIndex index)
{
	if (index.IsValid())
	{
		strStreamingModule *module = strStreamingEngine::GetInfo().GetModule(index);
		Assert(module);

		strLocalIndex objIndex = module->GetObjectIndex(index);

		m_AssetName = module->GetName(objIndex);
		safecpy(m_Extension, strStreamingEngine::GetInfo().GetModuleMgr().GetFileExtensionFromModule(module));

		if (m_Extension[0] == g_sysPlatform)
		{
			m_Extension[0] = '#';
		}
	}
}
#endif // !__FINAL



strRequestRecorder::strRequestRecorder()
: m_Recording(NULL)
{
#if !__FINAL
	m_IsRecording = false;
#endif // !__FINAL
}


void strRequestRecorder::LoadRecording(const char *filename)
{
	Assert(!m_Recording);

	if (!ASSET.Exists(filename, META_FILE_EXT))
	{
		Errorf("Request recording %s.%s does not exist", filename, META_FILE_EXT);
		return;
	}

	m_Recording = rage_new strRequestRecording();
	if(!fwPsoStoreLoader::LoadDataIntoObject(filename, META_FILE_EXT, *m_Recording))
	{
		delete m_Recording;
		m_Recording = NULL;
	}
}

void strRequestRecorder::ExecuteRecording()
{
	strStreamingEngine::GetLoader().CallKeepAliveCallback();

	if (m_Recording)
	{
		for (int x=0; x<m_Recording->m_Requests.GetCount(); x++)
		{
			if (strcmp(m_Recording->m_Requests[x].m_Resource.m_Extension, "#typ"))
			{
				strIndex index = m_Recording->m_Requests[x].m_Resource.Resolve(false);

				if (index.IsValid())
				{
					Displayf("Recorded request playback: Requesting %s", strStreamingEngine::GetObjectName(index));
					strStreamingEngine::GetInfo().RequestObject(index, m_Recording->m_Requests[x].m_Flags | STRFLAG_FORCE_LOAD);
				}
			}
		}
	}

	strStreamingEngine::GetLoader().CallKeepAliveCallback();
}



#if !__FINAL
void strRequestRecorder::BeginRecording(bool recordLeafAssetsOnly, bool recordPackfiles)
{
	Assert(!m_IsRecording);
	m_IsRecording = true;
	m_LeafAssetsOnly = recordLeafAssetsOnly;
	m_RecordPackfileRequests = recordPackfiles;

	Assert(!m_Recording);
	m_Recording = rage_new strRequestRecording();
}

void strRequestRecorder::EndRecording()
{
	Assert(m_IsRecording);
	Assert(m_Recording);

	m_IsRecording = false;
}

void strRequestRecorder::SaveRecording(const char *filename)
{
	Assert(m_Recording);
	PARSER.SaveObject(filename, "xml", m_Recording, parManager::XML);
}

void strRequestRecorder::OnRequest(strIndex index, s32 flags)
{
	if (m_IsRecording)
	{
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);

		if (!(info.GetFlags() & STRFLAG_INTERNAL_DUMMY))
		{
			strStreamingModule *module = strStreamingEngine::GetInfo().GetModule(index);

			if (!m_RecordPackfileRequests && module == strPackfileManager::GetStreamingModule())
			{
				return;
			}

			if (m_LeafAssetsOnly)
			{
				strIndex deps[256];
				int depCount = module->GetDependencies(module->GetObjectIndex(index), deps, NELEM(deps));

				if (depCount)
				{
					return;
				}
			}

			Assert(m_Recording);
			int count = m_Recording->m_Requests.GetCount();

			// Already in the list?
			for (int x=0; x<count; x++)
			{
				if (m_Recording->m_Requests[x].m_Resource.Resolve() == index)
				{
					// It's already in - let's make sure we get all the request flags.
					m_Recording->m_Requests[x].m_Flags |= flags & STR_REQUEST_FLAGS_MASK;
					return;
				}
			}

			// Not in the list yet - let's add it.
			strRecordedRequest &request = m_Recording->m_Requests.Grow(32);
			request.m_Flags = flags;
			request.m_Resource.Set(index);
		}
	}
}

#endif // !__FINAL


} // namespace rage

