//
// streaming/strindexdebugmanager.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

// Include Guard //////////////////////////////////////////////////////////////////////////////////

#ifndef STRINDEXDEBUGMANAGER_H
#define STRINDEXDEBUGMANAGER_H

// Module Includes ////////////////////////////////////////////////////////////////////////////////

#include "streamingdefs.h"
#include "streaming_channel.h"

// Misc Includes //////////////////////////////////////////////////////////////////////////////////

// Definitions ////////////////////////////////////////////////////////////////////////////////////

#if __FINAL
#define STRINDEX_DEBUG_MANAGER( X )			
#else // !__FINAL
#define STRINDEX_DEBUG_MANAGER( X )			strIndexDebugManager::X
#endif // !__FINAL

// Implementation Guard ///////////////////////////////////////////////////////////////////////////

#if !__FINAL

// Forward Declarations ///////////////////////////////////////////////////////////////////////////

namespace rage
{
	class strIndex;
}

// Namespace //////////////////////////////////////////////////////////////////////////////////////

namespace rage
{

// Class Declaration //////////////////////////////////////////////////////////////////////////////

class strIndexDebugManager
{

// Public Enumerations ////////////////////////////////////////////////////////////////////////////

public:

	// NOTES
	//		If you change this enum you also need to update the eeaddin Visual Studio plugin's
	//		copy of it.
	//
	enum TYPE_ENUM
	{
		TYPE_INVALID			= -1,
		TYPE_FILE,
		TYPE_DUMMY,

		TYPE_ENUM_COUNT
	};
	
// Public Static Methods //////////////////////////////////////////////////////////////////////////

	static void					Init( int maxIndexCount );

	static void					Shutdown();

	static void					Add( const strIndex& index, TYPE_ENUM type, char const* description );

	static void					Remove( const strIndex& index );

// Private Constants //////////////////////////////////////////////////////////////////////////////

private:

	static const int			MAX_DESCRIPTION_SIZE	= 62;

// Private Structures /////////////////////////////////////////////////////////////////////////////

	// NOTES
	//		If you change this structure you also need to update the eeaddin Visual Studio plugin's
	//		copy of this structure.
	//
	struct Entry
	{
		s8						m_refCount;
		s8						m_type;
		char					m_description[ MAX_DESCRIPTION_SIZE ];
	};

// Private Static Methods /////////////////////////////////////////////////////////////////////////

	static void					AddTypeCounter( TYPE_ENUM type );

	static void					RemoveTypeCounter( TYPE_ENUM type );

	static char const*			GetTypeName( TYPE_ENUM type );

// Private Static Members /////////////////////////////////////////////////////////////////////////

	static Entry*				sm_entries;

	static int					sm_maxEntryCount;

	static int					sm_typeCount[ TYPE_ENUM_COUNT ];

	static int					sm_maxTypeCount[ TYPE_ENUM_COUNT ];

///////////////////////////////////////////////////////////////////////////////////////////////////

}; // class strIndexDebugManager

// Inline Methods /////////////////////////////////////////////////////////////////////////////////

inline void	strIndexDebugManager::AddTypeCounter( TYPE_ENUM type )
{
	streamAssert( type >= 0 && type < TYPE_ENUM_COUNT );

	++sm_typeCount[ type ];

	if ( sm_typeCount[ type ] > sm_maxTypeCount[ type ] )
	{
		sm_maxTypeCount[ type ] = sm_typeCount[ type ];
	}
}

inline void	strIndexDebugManager::RemoveTypeCounter( TYPE_ENUM type )
{
	streamAssert( type >= 0 && type < TYPE_ENUM_COUNT );

	--sm_typeCount[ type ];
}

///////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace rage

///////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !__FINAL

///////////////////////////////////////////////////////////////////////////////////////////////////

#endif // STRINDEXDEBUGMANAGER_H
