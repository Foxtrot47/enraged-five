//
// streaming/streamingloader.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_STREAMINGLOADER_H
#define STREAMING_STREAMINGLOADER_H

// #include "streaming/streaming.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingdefs.h"

#include "atl/array.h"
#include "atl/queue.h"
#include "data/resource.h"
#include "data/resourceheader.h"
#include "grcore/resourcecache.h"
#include "paging/streamer.h"
#include "system/memory.h"
#include "system/messagequeue.h"
#include "system/timer.h"


#if __BANK
typedef void (*tStreamingLoaderManagerTrackingCallback)(int, int, bool);

extern tStreamingLoaderManagerTrackingCallback gStreamingLoaderPreCallback;
extern tStreamingLoaderManagerTrackingCallback gStreamingLoaderPostCallback;

#endif	//__BANK

//There doesn't seem to be any reasons at all for m_ReadSema to be a semaphore and not just a bool. Make it easily switchable just in case I am wrong
#define LOAD_SEMA 0
namespace rage {

class strStreamingLoader
{
public:
	enum ScheduleStrategy {
		ODD_SCHEDULER,
		HDD_SCHEDULER,

		SCHEDULER_COUNT,
	};

	void Init(pgStreamer::Device device);
	void Shutdown();
#if RSG_PC && defined(USE_RESOURCE_CACHE) && USE_RESOURCE_CACHE
	bool HandleOutOfMemory(size_t virtualSize, size_t physicalSize);
#endif // RSG_PC

	void LoadRequestedObjects();
	void ProcessAsyncFiles();
	void StartLoadAllRequestedObjects(); 
	bool CancelRequest(strIndex index);

	void Update();

	bool Flush();

	// Get additional flags that should be used when using pgStreamer functions
	u32 GetStreamerFlags() const;

	s32 GetLoadingObjectCount();
	strIndex GetLoadingObject(s32 index);
	float GetProcessTime() { return m_fProcessTime; }

#if __ASSERT
	void SanityCheck();
	void DumpLoadingState();
#endif
#if !RSG_FINAL
	void GetLoaderStats(int &loading, int &max);
#endif

#if !__NO_OUTPUT
	const char *GetLoaderName() const;
#endif // !__FINAL

	void GetLoadingFileSizes(size_t& virtualSize, size_t& physicalSize);

	int GetPending() {return m_Pending;}

#if __BANK
	u32 GetFramePendingSize() {return m_FramePendingSize;}

	void ShowFileStatus() const;
	void ShowAsyncStats() const;
#endif

#if STREAM_STATS
	u32 GetCompleted() const				{ return m_Completed; }
	u32 GetFailed() const					{ return m_Failed; }

	u32 GetCompletedSize() const			{ return m_CompletedSize; }
	u32 GetFailedSize() const				{ return m_FailedSize;	}

	float GetPendingTime() const			{ return m_PendingTime; }
	float GetPlaceTime() const				{ return m_PlaceTime; }

	float GetRollingCompleted() const		{ return m_RollingCompleted; }
	float GetRollingRedundant() const		{ return m_RollingRedundant; }

	u32 GetRedundantCount() const			{ return m_RedundantCount; }
#endif


#if __BANK && __ASSERT
	static const char *ProcessFileDiagCb(void * inStrIndex, void * b);
#endif

	enum Status		// THIS MUST MATCH StreamingFile::s_StatusStrings 
	{
		STATUS_IDLE,			// Slot is available for you
		STATUS_LOADING,			// A pgStreamer request has been made
		STATUS_CANCELED,		// This request has been canceled, but it has already been picked up by the streamer. We need to wait for it to finish and then remove it

		STATUS_MAX_VALUE
	};

	struct StreamingFile
	{
		// Max value is 2, but we need to give it an extra bit
		// since enums are signed! (We could handle it in code,
		// but this approach ends up with less confusion.)
		Status					m_Status : 3;
		u32						m_IsPlaced : 1;			// Set to true after the placement thread finishes placing it
		u32						m_Reissue : 1;			// Second time we're trying to place this
		s32						m_LoadTime : 27;
		strIndex				m_Index;
		#if LOAD_SEMA
			sysIpcSema				m_ReadSema;
		#endif
		pgStreamer::Handle		m_Handle;
		pgStreamer::ReadId		m_ReadId;

		datResourceMap			m_LoadingMap;
		datResourceMap			m_AllocationMap; // A superset of m_LoadingMap

#if __BANK
		static const char *sm_StatusStrings[STATUS_MAX_VALUE];
#endif // __BANK
		#if !LOAD_SEMA
			volatile bool					m_Finished;
		#endif
	};

	strIndex GetStrIndexFromHandle(const pgStreamer::Handle & handle) const;
protected:
	bool ConvertToObject(StreamingFile& file);
	bool ProcessFile(StreamingFile& file);
	void MarkPlacementComplete(StreamingFile &file);
	void FinalizePlacement(StreamingFile &file);
	void PerformMainThreadPlacement(StreamingFile &file);

	u32 GetHeadPosition();
	bool ProcessStreamFiles();
	int RequestStreamFiles(strIndex* indexArray, int numNextContiguousRequests);

	bool AllocateMemory(StreamingFile& file, strIndex index, strStreamingModule* module, int memBucket = MEMBUCKET_RESOURCE);
	bool IssueRequest(StreamingFile& file);

	bool OpenFile(StreamingFile& file);
	bool CloseFile(StreamingFile& file);

	int FindExistingRequest(strIndex index) const;
	bool AddNewRequest(strIndex index);
	int FindExistingAsyncRequest(strIndex index) const;

	static void SignalCallback(void *userArg,void *dest,u32 offset,u32 amtRead);

private:
	u32 m_LastFailedRequestPosition;

	pgStreamer::Device m_Device;
	int m_Pending;				// Total pending files, including waiting for async placement
	int m_PendingIO;			// Total pending files, only those that still require disk I/O
	// int	m_MaxStreamingFiles;
	// int	m_MaxStreamingSize;
	float m_fProcessTime;

	atRangeArray<StreamingFile, LOAD_SLOTS_PER_DEVICE>	m_StreamingFiles;
	atQueue<StreamingFile*, LOAD_SLOTS_PER_DEVICE>		m_StreamingQueue;
	
	// These are files that have been loaded and are currently being (or about to be)
	// placed on a separate thread.
	atFixedArray<StreamingFile*, LOAD_SLOTS_PER_DEVICE> m_AsyncPlacements;

	u32 m_PendingSize;			// How much data is scheduled to be streamed in currently
	u32 m_FramePendingSize;		// how much data was loaded this frame 

#if STREAM_STATS
	u32 m_Completed;			// how many completed this frame 
	u32 m_Failed;				// how many failed this frame

	u32 m_CompletedSize;		// how much data was completed this frame
	u32 m_FailedSize;			// how much data did we fail to complete
	u32 m_RedundantSize;		// how much data was loaded and immediately tossed out this frame

	float m_RollingCompleted;	// Rolling average of how much data has been streamed per frame
	float m_RollingRedundant;	// Rolling average of how much data has been streamed and immediately tossed out

	u32 m_RedundantCount;		// Number of streaming operations that ended up being redundant

	float m_PendingTime;		// how long did the data take to get in (ms) of those that finished this frame
	float m_PlaceTime;
#endif

#if __BANK
	int m_LongMtPlacements;		// Number times a file took over a frame to mt-place
	int	m_MissedIdleSlots;		// Number of times a request couldn't be issued because we were out of idle streaming slots

public:
#endif
	static bool	ms_ExtremeStreaming_Enabled;
	static int	ms_MaxPendingRequestThreshold;
	static s32	ms_ExtremeStreaming_MaxPendingKb;
	static s32	ms_ExtremeStreaming_MaxConvertMs;
	static u32	ms_LsnTolerance[pgStreamer::DEVICE_COUNT];
	static int	ms_MaxChainedRequests;

public:
#if __BANK
	static bool ms_IsPumpAndDump;
	static bool ms_IsSirCancelALot;
	static int ms_SirCancelALotFrequency;
#endif // __BANK
};

/** PURPOSE: For every file that has been streamed in and needs to be
 *  placed asynchronously on a separate thread, we put a strPlacementRequest
 *  on the m_AsyncPlaceQueue.
 */
struct strPlacementRequest
{
	// File being placed
	strStreamingLoader::StreamingFile *m_Info;
	
	// Resource memory map for the file being placed
	datResourceMap m_Map;
	
	// Resource info for the file being placed
	datResourceInfo m_ResInfo;
};



class strStreamingLoaderManager
{
public:
	enum EmergencyState
	{
		EMERGENCY_NONE,
		EMERGENCY_OUT_OF_MEMORY,
		EMERGENCY_FRAGMENTED_MEMORY
	};
	enum Device {
		ODD,
		HDD,

		DEVICE_COUNT
	};

	void Init();
	void Shutdown();

	strStreamingLoader& GetLoader(Device device) { return m_loaders[device]; }

	void LoadRequestedObjects();
	void LoadAllRequestedObjects(bool bPriorityRequestsOnly = false); 
	void LoadAllRequestedObjectsWithTimeout(bool bPriorityRequestsOnly = false, u32 timeoutMS = 0); 
	void StartLoadAllRequestedObjects(); 
	void CancelRequest(strIndex index);
	bool UpdateLoadAllRequestedObjects(); 

	void Update();

	void Flush();

	void ProcessAsyncFiles();
	void StartPlacement();
	void WaitOnPlacement();

	void SetKeepAliveCallback(void (*callback)(), u32 intervalInMS);
	void CallKeepAliveCallback();
	void CallKeepAliveCallbackIfNecessary();

	bool IsLoadingAllRequested() const					{ return m_LoadingAllRequested; }

	s32 GetLoadingObjectCount();
	strIndex GetLoadingObject(s32 index);

#if !__FINAL
	float GetMsTimeSinceLastEmergency()					{ return m_lastEmergencyTimer.GetMsTime();}
	void ResetEmergencyTimer()							{ m_lastEmergencyTimer.Reset();}
	float GetMsTimeSinceLastFragmentationEmergency()	{ return m_lastFragmentationTimer.GetMsTime();}
	void ResetFragmentationEmergencyTimer()				{ m_lastFragmentationTimer.Reset();}
	void SetEmergencyState(EmergencyState state)		{ m_emergencyState = state; }
	EmergencyState GetEmergencyState() const			{ return m_emergencyState; }
#endif // !__FINAL
#if __BANK
	void DumpUnsatifiedRequests(bool bPriorityRequestsOnly, bool earlyExit);
	void ShowFileStatus() const;
	void ShowAsyncStats() const;
	void ResetLoadAllBailTimeout();
	bool *GetLoaderSVMarkersPtr()				{ return &m_AddLoaderSVMarkers; }
	bool *GetAssertOnLargeRscVirtChunkPtr()		{ return &m_AssertOnLargeRscVirtChunk; }
#endif // !__BANK

	u32 GetStrIndexFromHandle(const pgStreamer::Handle & handle) const;

#if __ASSERT
	void SanityCheck();
	void DumpLoadingState();
#endif

	void GetLoadingFileSizes(size_t& virtualSize, size_t& physicalSize);

	void RequestAsyncPlacement(strStreamingLoader::StreamingFile &file, const datResourceMap &map, const datResourceInfo &resInfo);

	void SubmitDeferredAsyncPlacementRequests();

	void DeferAsyncPlacementRequests(bool bDefer) { m_DeferAsyncPlacementRequests = bDefer; }

	void SetOutOfMemory(bool state)		{ m_outOfMemory = state; }

private:
	strStreamingLoader m_loaders[DEVICE_COUNT];

	void (*m_callback)();
	u32 m_callbackInterval;

#if !__FINAL
	EmergencyState m_emergencyState;
	rage::sysTimer m_lastEmergencyTimer;
	rage::sysTimer m_lastFragmentationTimer;
#endif

	static void ResourcePlacementThread(void* data);
	void ResourcePlacementFunc();

	sysIpcThreadId	m_iPlacementThreadId;
	
	// Queue of resources that we'd like to place on a separate thread
	sysMessageQueue<strPlacementRequest, LOAD_SLOTS_PER_DEVICE * pgStreamer::DEVICE_COUNT> *m_AsyncPlaceQueue;
	atArray<strPlacementRequest> m_DeferredPlacementRequests;

	// True if async placement requests should be placed on the deferred list instead of directly submitted for
	// the async thread to process
	bool m_DeferAsyncPlacementRequests;

	// True if we're in the process of blocking the thread until all requests are loaded.
	bool m_LoadingAllRequested;

#if __BANK
	bool m_AddLoaderSVMarkers;
	bool m_AssertOnLargeRscVirtChunk;
#endif // __BANK

	bool m_outOfMemory;	
};

CompileTimeAssert(strStreamingLoaderManager::DEVICE_COUNT == STREAMING_SCHEDULER_COUNT);

}		// namespace rage

#endif // STREAMING_STREAMINGLOADER_H
