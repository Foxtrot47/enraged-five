//
// streaming/defragmentation.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//
#include "streaming/streaming_channel.h"
#include "streaming/defragmentation.h" 

STREAMING_OPTIMISATIONS()

#include "fwsys/timer.h"
#include "paging/base.h"
#include "profile/profiler.h"
#include "profile/timebars.h"
#include "system/buddyallocator.h"
#include "system/cache.h"
#include "system/debugmemoryfill.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/param.h"
#include "system/tasklog.h"
#include "grcore/debugdraw.h"
#include "grcore/effect_config.h"

// #include "scene/txdstore.h"
#include "streaming/streaminginfo.h"
#include "streaming/streamingengine.h"
#include "streaming/streamingmodule.h"
#include "system/timemgr.h"
#include "system/memvisualize.h"
#include "fwrenderer/renderthread.h"
#include "diag/output.h"

RAGE_DEFINE_CHANNEL(Defrag,rage::DIAG_SEVERITY_DISPLAY)

#if !__FINAL
PARAM(defragorama,"Defragment like crazy to exercise the system and make it fail sooner");
#endif
#if __ASSERT
PARAM(validateallreferences,"Validate all known defrag references during defrag");
#endif

namespace rage {

#if USE_DEFRAGMENTATION

#if ENABLE_DEFRAG_CALLBACK && ENABLE_DEFRAG_DEBUG
	int strDefragmentation::s_defragRemoveTotal = 0;
	int strDefragmentation::s_defragRemoveCurrent = 0;
	int strDefragmentation::s_defragRemovePeak = 0;
#endif

#if __BANK
static const char *s_StateNames[] = {
	"Idle",
	"Copy Physical",
	"Copy",
	"Place",
	"Check Copy Physical",
	"Wait One Frame",
	"Finalize"
};
#endif // __BANK

#if !__FINAL
static bool s_Defragorama;
#endif // !__FINAL

#if DEBUG_DEFRAG_REFCNT
	struct strDefragDebugInfo
	{
		enum
		{
			IN_USE		= BIT0,
			CANCELED	= BIT1,
		};

		strStreamingModule *m_Module;
		strLocalIndex m_ObjectIndex;
		int m_RefsAdded;
		u32 m_FrameCreated;
		u32 m_FrameReleased;
		u32 m_Flags;

		strDefragDebugInfo()
			: m_Module(NULL)
			, m_ObjectIndex(0)
			, m_RefsAdded(0)
			, m_FrameCreated(0)
			, m_FrameReleased(0)
			, m_Flags(0)
		{
		}

		void RemoveRef()
		{
			streamAssert(m_RefsAdded > 0);
			m_RefsAdded--;
		}
	};

	const int DEFRAG_SLOTS = 32;

	strDefragDebugInfo s_DefragDebugInfos[DEFRAG_SLOTS];
	static int s_NextIndexToUse = 0;

	void strDefragmentation::DumpDefragInfo()
	{
		for (int x=0; x<DEFRAG_SLOTS; x++)
		{
			strDefragDebugInfo &info = s_DefragDebugInfos[x];

			if (info.m_Module)
			{
				streamDisplayf("Slot %d: %s (%s), ref count=%d, age=%d, release age=%d, flags=%x", x, info.m_Module->GetName(info.m_ObjectIndex), info.m_Module->GetModuleName(), info.m_RefsAdded, TIME.GetFrameCount() - info.m_FrameCreated, TIME.GetFrameCount() - info.m_FrameReleased, info.m_Flags);
			}
		}
	}

	strDefragDebugInfo *strDefragmentation::FindDefragDebugInfo(strStreamingModule *module, s32 objectIndex, bool createIfMissing, int state)
	{
		// Is the information available?
		for (int x=0; x<DEFRAG_SLOTS; x++)
		{
			strDefragDebugInfo &info = s_DefragDebugInfos[x];
			if (info.m_Module == module && info.m_ObjectIndex == objectIndex && (info.m_Flags & strDefragDebugInfo::IN_USE))
			{
				return &s_DefragDebugInfos[x];
			}
		}

		streamAssertf(createIfMissing, "Cannot find defrag info for %s (%s) even though it should be there - in state %d", module->GetName(strLocalIndex(objectIndex)), module->GetModuleName(), state);

		if (createIfMissing)
		{
			// Find a free slot.
			for (int x=0; x<DEFRAG_SLOTS; x++)
			{
				int index = s_NextIndexToUse;
				s_NextIndexToUse++;
				s_NextIndexToUse %= DEFRAG_SLOTS;

				strDefragDebugInfo &info = s_DefragDebugInfos[index];
				if (!(info.m_Flags & strDefragDebugInfo::IN_USE))
				{
//					streamDisplayf("Allocating slot %d: %s (%s)", index, module->GetName(objectIndex), module->GetModuleName());
					info.m_Module = module;
					info.m_ObjectIndex = objectIndex;
					info.m_RefsAdded = 0;
					info.m_Flags = strDefragDebugInfo::IN_USE;
					info.m_FrameCreated = TIME.GetFrameCount();
					return &info;
				}
			}
		}
		else
		{
			DumpDefragInfo();
			return NULL;
		}

		static bool alreadyDumped = false;

		if (!alreadyDumped)
		{
			DumpDefragInfo();
			alreadyDumped = true;
		}

		streamAssertf(false, "No free defrag info slots! Too many objects being defragged at the same time - that could be a sign of a ref count leak. Check the TTY for more info.");
		return NULL;
	}

	void FreeDefragDebugInfo(strStreamingModule *module, s32 objectIndex)
	{
		for (int x=0; x<DEFRAG_SLOTS; x++)
		{
			strDefragDebugInfo &info = s_DefragDebugInfos[x];

			if (info.m_Module == module && info.m_ObjectIndex == objectIndex && (info.m_Flags & strDefragDebugInfo::IN_USE))
			{
//				streamDisplayf("Freeing slot %d: %s (%s)", x, module->GetName(objectIndex), module->GetModuleName());
				info.m_Flags &= ~strDefragDebugInfo::IN_USE;
				info.m_FrameReleased = TIME.GetFrameCount();
				return;
			}
		}
	}


#endif // DEBUG_DEFRAG_REFCNT

#if (0 && __DEV)
#define SANITY_CHECK()			DEV_ONLY(SanityCheck())
#else
#define SANITY_CHECK()
#endif

#define INVALID_SUBJECT (0xff)

#define DEFRAG_STATS (__STATS && 1)
#if DEFRAG_STATS
namespace strDefragmentationProfile 
{
	// slightly funky stats collection due to the timing of the defrag update, just as the profile has rendered, resets etc.
	float s_defragTime;
	float s_copyTime;
	float s_placeTime;
	float s_exchangeTime;

	PF_PAGE(strDefragmentationStatsPage, "GTA Defragmentation Size");

	PF_GROUP(strDefragmentationStats);
	PF_LINK(strDefragmentationStatsPage, strDefragmentationStats);

	PF_COUNTER_CUMULATIVE(subjects, strDefragmentationStats);
	PF_COUNTER_CUMULATIVE(chunks, strDefragmentationStats);
	PF_COUNTER_CUMULATIVE(chunks_size, strDefragmentationStats);
	PF_VALUE_INT(blocks, strDefragmentationStats);

	PF_PAGE(strDefragmentationTimePage, "GTA Defragmentation Time");

	PF_GROUP(strDefragmentationTime);
	PF_LINK(strDefragmentationTimePage, strDefragmentationTime);
	
	PF_VALUE_FLOAT(defrag, strDefragmentationTime);
	PF_VALUE_FLOAT(copy, strDefragmentationTime);
	PF_VALUE_FLOAT(place, strDefragmentationTime);
	PF_VALUE_FLOAT(exchange, strDefragmentationTime);
}
using namespace strDefragmentationProfile;
#endif

void strDefragmentation::Init()
{
	memset(m_Subjects, 0, sizeof(m_Subjects));
	for (u16 i = 0; i != MAX_SUBJECTS; ++i)
	{
		m_Subjects[i].m_State = eStateIdle;
		m_Subjects[i].m_Next = (i+1==MAX_SUBJECTS)? INVALID_SUBJECT : i+1;
	}

	m_FirstFreeSubject = 0;
	m_FirstUsedSubject = INVALID_SUBJECT;
	m_DefragMode = DEFRAG_NORMAL;
	m_PendingCopies = 0;
	m_Frack = 0;

	m_Enabled = true;

#if __BANK
	m_Heartbeats = 0;
	m_AllocDefragCalls = 0;
	m_DefragStarts = 0;
	m_CockblockedByMtPlacement = 0;
	m_DontUpdateWhenPaused = false;
	m_AverageProcessTime = 0.0f;
	m_AveragePageSize[0] = 8192.0f;		// Start of with something remotely realistic
	m_AveragePageSize[1] = 8192.0f;
#endif // __BANK

#if !__FINAL
	s_Defragorama = PARAM_defragorama.Get();
#endif // !__FINAL
}


void strDefragmentation::Shutdown()
{
}

void strDefragmentation::FlushAndDisable()
{
	gRenderThreadInterface.Flush();
	FlushAndDisable_NoRenderThreadFlush();
}

void strDefragmentation::FlushAndDisable_NoRenderThreadFlush()
{
	// Finish all the currently scheduled operations.
	while (m_FirstUsedSubject != INVALID_SUBJECT)
	{
		// See what state this guy was in.
		int firstUsed = m_FirstUsedSubject;
		State firstState = m_Subjects[firstUsed].m_State;

		// Now process all subjects.
		ProcessSubjectsIncremental();

		// If the first guy hasn't changed, then he's stuck and we need to boot his ass.
		if (firstUsed == m_FirstUsedSubject && m_Subjects[firstUsed].m_State == firstState)
		{
			// GTFO, sucker.
			Cancel(m_Subjects[firstUsed].m_Index, true);
		}
	}

	m_Enabled = false;
}

void strDefragmentation::Enable()
{
	m_Enabled = true;
}


u32 strDefragmentation::CountSiblingSubjects(strIndex index)
{
	u32 count = 0;
	for (s16 curr = m_FirstUsedSubject; curr != INVALID_SUBJECT; curr = m_Subjects[curr].m_Next)
	{
		if (m_Subjects[curr].m_Index == index)
			count++;
	}
	return count;
}


void strDefragmentation::Cancel(strIndex index, bool PPU_ONLY(bBlockOnGpuMemCpyCompletion))
{	
	strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(index);
	defragDebugf1("defrag cancel %s %d", strStreamingEngine::GetInfo().GetObjectName(index), index.Get());

	ASSERT_ONLY(bool bFound = false;)

	s16 curr = m_FirstUsedSubject;
	s16 prev = INVALID_SUBJECT;
	while (curr != INVALID_SUBJECT)
	{
		Subject& subject = m_Subjects[curr];
		State subjectState = subject.m_State;
		s16 next = subject.m_Next;
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(subject.m_Index);
		strLocalIndex objectIndex = pModule->GetObjectIndex(subject.m_Index);

		if (subject.m_Index == index)
		{
			defragDebugf1("defrag [%d] cancelling subject %s %d", curr, strStreamingEngine::GetInfo().GetObjectName(index), subject.m_State);
			ASSERT_ONLY(bFound = true;)

			// If Lock was called on the resource while defrag was in flight,
			// then we need to transfer the lock count across to the individual
			// memory allocations so that the corresponding Unlock call will
			// work correctly.
			if (subject.m_CopyLockCount)
			{
				pgBase *const resource = (pgBase*)strStreamingEngine::GetInfo().GetObjectPtr(index);
				LockMemAllocs(resource, index, subject.m_CopyLockCount);
				subject.m_CopyLockCount = 0;
			}

			int chunk = 0;

#if RESOURCE_HEADER
			if (subject.m_Map.IsOptimized())
			{
				chunk = 1;
			}
#endif
			for (; chunk < subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount;++chunk)
			{
				State chunkState = subject.m_ChunkState[chunk];
				void* srcAddr = subject.m_Map.Chunks[chunk].SrcAddr;
				void* destAddr = subject.m_Map.Chunks[chunk].DestAddr;
#if __TASKLOG && __ASSERT
				size_t size = subject.m_Map.Chunks[chunk].Size;
#endif

				if (chunkState > eStateIdle)
				{
					TASKLOG_ASSERTF(sysMemAllocator::GetMaster().GetSize(srcAddr), 
						"Defrag %s contains free source chunk %d = 0x%p [%" SIZETFMT "u]", 
						strStreamingEngine::GetInfo().GetObjectName(index), chunk, srcAddr, size);
					TASKLOG("Unlocking src block for Cancel %p",(size_t)srcAddr);
					sysMemAllocator::GetMaster().UnlockBlock(srcAddr);

					TASKLOG_ASSERTF(sysMemAllocator::GetMaster().GetSize(destAddr), 
						"Defrag %s contains free destination chunk %d = 0x%p [%" SIZETFMT "u]", 
						strStreamingEngine::GetInfo().GetObjectName(index), chunk, destAddr, size);
					TASKLOG("Unlocking dest block for Cancel %p",(size_t)destAddr);
					sysMemAllocator::GetMaster().UnlockBlock(destAddr);
						
					defragDebugf1("defrag [%d] freeing: %s 0x%p", curr, strStreamingEngine::GetInfo().GetObjectName(subject.m_Index), chunkState == eStateFinalise?srcAddr:destAddr);

#if __PPU
					streamAssertf(bBlockOnGpuMemCpyCompletion || chunkState != eStateCheckCopyPhysical, 
						"Can't cancel subject '%s' with inflight physical copies (block=%d, state=%d)", 
						strStreamingEngine::GetInfo().GetObjectName(index),bBlockOnGpuMemCpyCompletion,chunkState);
					if (bBlockOnGpuMemCpyCompletion && chunkState == eStateCheckCopyPhysical && !subject.m_ChunkCompleted[chunk])
					{
						ASSERT_ONLY(defragWarningf("Blocking on GPU mempy"));
						while (!subject.m_ChunkCompleted[chunk]) 
							sys_timer_usleep(30);
					}

					// If there was a physical copy (and ONLY if there was a physical copy), remember that it's no longer pending.
					if (chunkState == eStateCheckCopyPhysical)
					{
						m_PendingCopies--;
						streamAssert(m_PendingCopies>=0);

#if DEBUG_DEFRAG_REFCNT
						strDefragDebugInfo *info = FindDefragDebugInfo(pModule, objectIndex, false, eStateCheckCopyPhysical + 1000);

						if (info)
						{
							info->RemoveRef();
						}
#endif // DEBUG_DEFRAG_REFCNT

						pModule->RemoveRef(objectIndex, REF_DEFRAG);
					}
#endif

					sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(int(chunk>=subject.m_Map.VirtualCount)%NUM_HEAPS?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL);
					void *whichAddr = (subjectState>eStatePlace)?srcAddr:destAddr;
					strStreamingEngine::GetAllocator().RemoveFromMemoryUsed(whichAddr);
					RAGE_LOG_DELETE(whichAddr);
					sysMemAllowResourceAlloc++;
					{
#if MEM_VALIDATE_USERDATA
						void *otherAddr = (subjectState>eStatePlace)?destAddr:srcAddr;
						MEM_USE_USERDATA(sysMemAllocator::GetMaster().GetUserData(otherAddr));
#endif // MEM_VALIDATE_USERDATA
#if RESOURCE_HEADER
						Assert(!sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(whichAddr));
#endif
						alloc.Free(whichAddr);
					}
					sysMemAllowResourceAlloc--;
					TASKLOG_ASSERTF(!alloc.GetSize(whichAddr), "Chunk %d not freed", chunk);

					subject.m_ChunkState[chunk] = eStateIdle;
				}
			}

			if (CountSiblingSubjects(subject.m_Index/*, true*/) == 1)
				info->ClearFlag(index, STRFLAG_INTERNAL_DEFRAGGING);

#if DEBUG_DEFRAG_REFCNT
			strDefragDebugInfo *info = FindDefragDebugInfo(pModule, objectIndex.Get(), false, -1);

			if (info)
			{
				info->RemoveRef();
			}
#endif // DEBUG_DEFRAG_REFCNT

			pModule->RemoveRef(objectIndex, REF_DEFRAG);

#if DEBUG_DEFRAG_REFCNT
			info->m_Flags |= strDefragDebugInfo::CANCELED;
			FreeDefragDebugInfo(pModule, objectIndex.Get());
#endif // DEBUG_DEFRAG_REFCNT

			subject.m_State = eStateIdle;
			
			// add to free list
			if (prev == INVALID_SUBJECT)
			{
				m_FirstUsedSubject = subject.m_Next;
			}
			else
			{
				m_Subjects[prev].m_Next = subject.m_Next;
			}

			subject.m_Next = m_FirstFreeSubject;
			m_FirstFreeSubject = curr;

			curr = prev;
		}

		SANITY_CHECK();

		prev = curr;
		curr = next;
		streamAssert(prev == INVALID_SUBJECT || m_Subjects[prev].m_State > eStateIdle);
		streamAssert(curr == INVALID_SUBJECT || m_Subjects[curr].m_State > eStateIdle);
	}

	streamAssertf(bFound, "Canceled %s but no subjects found", strStreamingEngine::GetInfo().GetObjectName(index));
}


void strDefragmentation::Abort()
{
	while (m_FirstUsedSubject != INVALID_SUBJECT)
	{
		Cancel(m_Subjects[m_FirstUsedSubject].m_Index, true);
	}
}

extern void WriteFinalMemoryUsage();

void strDefragmentation::Update()
{
#if __BANK
	if (m_DontUpdateWhenPaused)
	{
		if (fwTimer::IsGamePaused())
		{
			return;
		}
	}

	m_Heartbeats++;
#endif // __BANK

#if DEFRAG_STATS
	PF_COUNTER_VAR(subjects).Clear();
	PF_COUNTER_VAR(chunks).Clear();
	PF_COUNTER_VAR(chunks_size).Clear();

	sysMemBuddyAllocator* buddy = static_cast<sysMemBuddyAllocator*>(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL));
	sysMemDistribution dist;

	buddy->GetMemoryDistribution(dist);

	u32 count = 0;
	for (int i = 0; i < NELEM(dist.FreeBySize); ++i)
	{
		count += dist.FreeBySize[i];
	}

	buddy = static_cast<sysMemBuddyAllocator*>(sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL));
	buddy->GetMemoryDistribution(dist);

	count = 0;
	for (int i = 0; i < NELEM(dist.FreeBySize); ++i)
	{
		count += dist.FreeBySize[i];
	}


	PF_VALUE_VAR(blocks).Set(count);

	PF_VALUE_VAR(defrag).Set(s_defragTime);
	PF_VALUE_VAR(copy).Set(s_copyTime);
	PF_VALUE_VAR(place).Set(s_placeTime);
	PF_VALUE_VAR(exchange).Set(s_exchangeTime);
	s_defragTime = 0.0f;
	s_copyTime = 0.0f;
	s_placeTime = 0.0f;
	s_exchangeTime = 0.0f;
#endif

	if (!m_Enabled)
		return;

	if (!m_DefragLock.TryLock())
	{
#if __BANK
		m_CockblockedByMtPlacement++;
#endif // __BANK
		// Another thread has temporarily disabled defrag. Let's try again next frame.
		return;
	}

	if (m_FirstUsedSubject == INVALID_SUBJECT)
		DefragmentAllocators();

	if (m_FirstUsedSubject != INVALID_SUBJECT)
	{	
#if __BANK
		sysTimer processingTimer;
#endif // __BANK

#if !__FINAL
		if (s_Defragorama)
			ProcessSubjectsTimed(20.0f);
		else
#endif
		switch(m_DefragMode)
		{
		case DEFRAG_NORMAL:
			ProcessSubjectsIncremental();
			break;
		case DEFRAG_PAUSE:
			ProcessSubjectsTimed(20.0f);		
			break;
		case DEFRAG_CUTSCENE_INIT:
			ProcessSubjectsTimed(20.0f);		
			break;
		case DEFRAG_CUTSCENE_RUN:
			ProcessSubjectsTimed(5.0f);
			break;
		default:
			streamAssertf(0, "Unknown mode");
			break;
		}

#if __BANK
		m_AverageProcessTime = m_AverageProcessTime * 0.95f + processingTimer.GetMsTime() * 0.05f;
#endif // __BANK
	}
	
	m_DefragLock.Unlock();

	SANITY_CHECK();
}


void strDefragmentation::ProcessFirstSubject()
{
	s16 curr = m_FirstUsedSubject;

	Process(curr);

	if (m_Subjects[curr].m_State == eStateIdle)
	{
		// add to free list
		m_FirstUsedSubject = m_Subjects[curr].m_Next;
		m_Subjects[curr].m_Next = m_FirstFreeSubject;
		m_FirstFreeSubject = curr;
	}

	SANITY_CHECK();
}


void strDefragmentation::ProcessSubjectsTimed(float fTime)
{
	// this version updates all subjects, but only allows one exchange/placement if enough time remains

	s16 curr = m_FirstUsedSubject;
	s16 prev = INVALID_SUBJECT;

	sysTimer cutoffTimer;
	while (curr != INVALID_SUBJECT)
	{
		Subject& subject = m_Subjects[curr];
		s16 next = subject.m_Next;

		if (subject.m_State == eStateCopy || subject.m_State == eStatePlace WIN32_ONLY(|| subject.m_State == eStateCopyPhysical)) 
		{
			if (cutoffTimer.GetMsTime() < fTime)
			{
				Process(curr);
			}
		}
		else
		{
			Process(curr);
		}

		if (subject.m_State == eStateIdle)
		{
			// add to free list
			if (prev == INVALID_SUBJECT)
			{
				m_FirstUsedSubject = subject.m_Next;
			}
			else
			{
				streamAssert(m_Subjects[prev].m_State > eStateIdle);
				m_Subjects[prev].m_Next = subject.m_Next;
			}

			subject.m_Next = m_FirstFreeSubject;
			m_FirstFreeSubject = curr;

			curr = prev;
		}

		SANITY_CHECK();

		prev = curr;
		curr = next;
		streamAssert(prev == INVALID_SUBJECT || m_Subjects[prev].m_State > eStateIdle);
		streamAssert(curr == INVALID_SUBJECT || m_Subjects[curr].m_State > eStateIdle);
	}
}


void strDefragmentation::ProcessSubjectsIncremental()
{
	// this version updates all subjects, but only allows one exchange/placement call per frame

	s16 curr = m_FirstUsedSubject;
	s16 prev = INVALID_SUBJECT;

	bool bHasPlaced = false;
	while (curr != INVALID_SUBJECT)
	{
		Subject& subject = m_Subjects[curr];
		s16 next = subject.m_Next;

		if (subject.m_State == eStateCopy || subject.m_State == eStatePlace WIN32_ONLY(|| subject.m_State == eStateCopyPhysical)) 
		{
			if (!bHasPlaced || subject.m_bPhysicalOnly)
			{
				Process(curr);
				bHasPlaced = (subject.m_State >= eStateWaitOneFrame);
			}
		}
		else
		{
			Process(curr);
		}

		if (subject.m_State == eStateIdle)
		{
			// add to free list
			if (prev == INVALID_SUBJECT)
			{
				m_FirstUsedSubject = subject.m_Next;
			}
			else
			{
				streamAssert(m_Subjects[prev].m_State > eStateIdle);
				m_Subjects[prev].m_Next = subject.m_Next;
			}

			subject.m_Next = m_FirstFreeSubject;
			m_FirstFreeSubject = curr;

			curr = prev;
		}

		SANITY_CHECK();

		prev = curr;
		curr = next;
		streamAssert(prev == INVALID_SUBJECT || m_Subjects[prev].m_State > eStateIdle);
		streamAssert(curr == INVALID_SUBJECT || m_Subjects[curr].m_State > eStateIdle);
	}
}


void strDefragmentation::Process(int subjectIndex)
{
	Subject& subject = m_Subjects[subjectIndex];
	strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(subject.m_Index);
	strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(subject.m_Index);

	strLocalIndex objectIndex = pModule->GetObjectIndex(subject.m_Index);
	//void* pObjectPtr = pModule->GetPtr(objectIndex);
#if !__NO_OUTPUT
	const char* pObjectName = pModule->GetName(objectIndex);
	BANK_ONLY(diagContextMessage m(pObjectName));

#if RAGE_TIMEBARS
#if INCLUDE_DETAIL_TIMEBARS // Only if we have access to the model name and have detail timebars on
	char timebarName[64];
	formatf(timebarName, "DEFRAG: %s", pObjectName);
	pfAutoMarker defragTimebar(timebarName, 64);
#else // INCLUDE_DETAIL_TIMEBARS
	pfAutoMarker defragTimebar(pObjectName, 64);
#endif
#endif // RAGE_TIMEBARS
#endif

	switch (subject.m_State)
	{
	case eStateCopyPhysical:
		{
#if DEFRAG_STATS || DEFRAG_TIME
			sysTimer copyTimer;
#endif

			PPU_ONLY(bool bRefCount = !!(subject.m_Map.VirtualCount * !m_Frack);)

			for (int chunk = subject.m_Map.VirtualCount; chunk < subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount;++chunk)
			{
				if (subject.m_ChunkState[chunk] == eStateCopyPhysical)
				{
					defragDebugf1("defrag [%d] phys copying %s  0x%p -> 0x%p (%" SIZETFMT "d)", subjectIndex, pObjectName, subject.m_Map.Chunks[chunk].SrcAddr, subject.m_Map.Chunks[chunk].DestAddr, subject.m_Map.Chunks[chunk].Size);
#if __PPU
					 if (m_PendingCopies == 64)
						 return;		// Early exit, don't flood the queue (magic number is the size of the old request queue)

					// There's a hard limit on how big a texture we can copy.
					// GpuMemCpy will trap without an error message if we exceed it, so let's assert here
					// so __DEV builds at least can tell us what's wrong, and which resource is responsible.
					streamAssertf(subject.m_Map.Chunks[chunk].Size < 0x400000, "Resource %s has a video memory chunk of %d KB - that's exceeds the amount of memory we can copy via GPU.",
						pObjectName, subject.m_Map.Chunks[chunk].Size / 1024);

					// once the request gets issued, we can't delete the object, since the gpu copy request 
					// may have reached the gpu and can't be canceled anymore.
					// However, if the gpu copy queue is already full for whatever reason, bail early instead of hanging up.
					if (!GRCDEVICE.GpuMemCpy(subject.m_Map.Chunks[chunk].DestAddr, subject.m_Map.Chunks[chunk].SrcAddr, subject.m_Map.Chunks[chunk].Size, subject.m_ChunkCompleted[chunk])) 
					{
						defragWarningf("GpuMemCpy queue is full somehow?  %d pending copies",m_PendingCopies);
						return;
					}

#if DEBUG_DEFRAG_REFCNT
					 strDefragDebugInfo *info = FindDefragDebugInfo(pModule, objectIndex, true, eStateCopyPhysical);

					 if (info)
					 {
						 info->m_RefsAdded++;
					 }
#endif // DEBUG_DEFRAG_REFCNT

					if (bRefCount)
					{
						 pModule->AddRef(objectIndex, REF_DEFRAG); 
					}
					subject.m_ChunkState[chunk] = eStateCheckCopyPhysical;
					m_PendingCopies++;
#else
					void *const dst = subject.m_Map.Chunks[chunk].DestAddr;
					const void *const src = subject.m_Map.Chunks[chunk].SrcAddr;
					const size_t size = subject.m_Map.Chunks[chunk].Size;
					sysMemCpy(dst, src, size);

#if RSG_DURANGO || RSG_ORBIS || RSG_XENON
					// Chunk may contain data that GPU will access, so make sure the memcpy is visible to the GPU.
					WritebackDC(dst, (int)size);
#endif

					subject.m_ChunkState[chunk] = eStatePlace;
#endif
				}
			}
			subject.m_State = eStateCopy;

#if DEFRAG_TIME
			subject.m_fCopyTime += copyTimer.GetMsTime();
#endif

#if DEFRAG_STATS
			s_copyTime += copyTimer.GetMsTime();
#endif
		}

#if __PPU
		break;
#endif
		// ========================================
		// ====>>>    FALLTHRU (!__PPU)		<<<====
		// ========================================	

	case eStateCopy:
		{
			// Subject state must have been correctly set in fall through case
			// so that we can correctly restart this state if we early out.
			streamAssert(subject.m_State == eStateCopy);

			if (pModule->IsDefragmentCopyBlocked())
			{
				break;
			}

			// If the resource was locked after defrag started, delay placement
			// until fully unlocked again
			if (Unlikely(subject.m_CopyLockCount))
			{
				defragDebugf1("defrag [%d] copy locked (%u)", subjectIndex, subject.m_CopyLockCount);
				break;
			}

			pModule->DefragmentPreprocess(objectIndex);

#if __PPU
			// check all the physical copies using the gpu have actually finished.
			for (int chunk = subject.m_Map.VirtualCount; chunk < subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount; ++chunk)
			{
				if (subject.m_ChunkState[chunk] == eStateCheckCopyPhysical)
				{
					if (subject.m_ChunkCompleted[chunk])
					{
#if DEBUG_DEFRAG_REFCNT
						strDefragDebugInfo *info = FindDefragDebugInfo(pModule, objectIndex, false, eStateCopy);
						if (info)
						{
							info->RemoveRef();
						}
#endif // DEBUG_DEFRAG_REFCNT

						--m_PendingCopies;
						streamAssert(m_PendingCopies>=0);
						pModule->RemoveRef(objectIndex, REF_DEFRAG);
						subject.m_ChunkState[chunk] = eStatePlace;
					}
					else
					{
						defragDebugf1("Early exit from processing subject '%s' since physical copying hasn't completed yet",  strStreamingEngine::GetInfo().GetObjectName(subject.m_Index));
						return; // exit processing
					}
				}
			}
#endif

#if DEFRAG_STATS || DEFRAG_TIME
			sysTimer copyTimer;
#endif

#if GRCORE_ON_SPU
			// We have to block on SPU completion BEFORE doing the copy lest the SPU accidentally modify
			// the linked list of the original version.
			{
#if !__FINAL
				sysTimer flushTimer;
				static float maxFlushTime = 0.250f; 
#endif // !__FINAL
				// GCM_CONTEXT->callback(GCM_CONTEXT,0);
				grcDevice::BlockOnGcmTasks();
#if !__FINAL
				float flushTime = flushTimer.GetMsTime();
				if (flushTime > maxFlushTime)
				{
					defragWarningf("Max block on flushSpuGcm so far %2.3fms", flushTime);
					maxFlushTime = flushTime;
				}
#endif
			}
#endif

			int chunk = 0;

#if RESOURCE_HEADER
			if (subject.m_Map.IsOptimized())
			{
				chunk = 1;
			}
#endif
			for (; chunk < subject.m_Map.VirtualCount;++chunk)
			{
				if (subject.m_ChunkState[chunk] == eStateCopy)
				{
					void *const dst = subject.m_Map.Chunks[chunk].DestAddr;
					const void *const src = subject.m_Map.Chunks[chunk].SrcAddr;
					const size_t size = subject.m_Map.Chunks[chunk].Size;
					defragDebugf1("defrag [%d] copying %s  0x%p -> 0x%p (%" SIZETFMT "d)", subjectIndex, pObjectName, src, dst, size);
					sysMemCpy(dst, src, size);

#if RSG_DURANGO || RSG_ORBIS || RSG_XENON
					// Chunk may contain data that GPU will access, so make sure the memcpy is visible to the GPU.
					WritebackDC(dst, (int)size);
#endif

					subject.m_ChunkState[chunk] = eStatePlace;
				}
			}
			subject.m_State = eStatePlace;

#if DEFRAG_TIME
			subject.m_fCopyTime += copyTimer.GetMsTime();
#endif

#if DEFRAG_STATS
			s_copyTime += copyTimer.GetMsTime();
#endif

		}

		// ========================================
		// ====>>>          FALLTHRU		<<<====
		// ========================================	

	case eStatePlace:
		{
#if DEFRAG_STATS || DEFRAG_TIME
			sysTimer placeTimer;
#endif
			defragDebugf1("defrag [%d] placing: %s", subjectIndex, pObjectName);
			pgBase* resource = (pgBase*) strStreamingEngine::GetInfo().GetObjectPtr(subject.m_Index);

#if !__ASSERT && !__FINAL
			if (info->GetStatus() != STRINFO_LOADED)
				defragErrorf("Invalid state for %s (%d) during defrag", pObjectName, info->GetStatus());

			if (!resource)
				defragErrorf("NULL ptr returned for %s during defrag", pObjectName);
#else
			streamAssertf(info->GetStatus() == STRINFO_LOADED, "Invalid state for %s (%d) during defrag", pObjectName, info->GetStatus());
			streamAssertf(resource, "NULL ptr returned for %s during defrag", pObjectName);
#endif

			streamAssertf(!PARAM_validateallreferences.Get() || pgBase::ValidateAllReferences(),"ValidateAllReferences failed just before rsc construct of %s",pModule->GetModuleName());

			// Fix up all pointers after the copy is finished but before we reinvoke the resource constructor
			// so that we can safely manipulate known references if absolutely necessary.
			pgBase::PatchAllReferences(subject.m_Map);
			
			///// HERE IS WHERE WE INVOKE THE RESOURCE CONSTRUCTOR AGAIN INCLUDING THE NEW LOCATION (Defragment)
			bool flush = false;
			resource = (pgBase*) pModule->Defragment(objectIndex, subject.m_Map, flush);

			// Remember the new location of any nodes that moved!
			// (I think this is redundant, pgBase ctor already does this)
			resource->MakeDefragmentable(subject.m_Map,false);

			int chunk = 0;

#if RESOURCE_HEADER
			if (subject.m_Map.IsOptimized())
			{
				chunk = 1;
			}
#endif
			for (; chunk < subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount;++chunk)
			{
				if (subject.m_ChunkState[chunk] == eStatePlace)
				{
					subject.m_ChunkState[chunk] = eStateFinalise;
				}
			}

#if DEFRAG_TIME
			subject.m_fPlaceTime += placeTimer.GetMsTime();
#endif

#if DEFRAG_STATS
			s_placeTime += placeTimer.GetMsTime();
#endif
			{
#if DEFRAG_STATS || DEFRAG_TIME
				sysTimer exchangeTimer;
#endif
				bool delayDelete = false;
				if (!subject.m_bPhysicalOnly)
					delayDelete = true;

#if DEBUG_DEFRAG_REFCNT
				strDefragDebugInfo *debugInfo = FindDefragDebugInfo(pModule, objectIndex.Get(), false, eStatePlace);

				if (debugInfo)
				{
					streamAssert(debugInfo->m_RefsAdded > 0);
				}
#endif // DEBUG_DEFRAG_REFCNT


				subject.m_State = (delayDelete?eStateWaitOneFrame:eStateFinalise);

				defragDebugf1("defrag [%d] placing finished: %s", subjectIndex,  strStreamingEngine::GetInfo().GetObjectName(subject.m_Index));

#if DEFRAG_TIME
				subject.m_fExchangeTime += exchangeTimer.GetMsTime();
#endif

#if DEFRAG_STATS
				s_exchangeTime += exchangeTimer.GetMsTime();
#endif
			}
		}

		break; 

	case eStateWaitOneFrame:
		{
			defragDebugf1("defrag [%d] waiting one frame: %s", subjectIndex,  strStreamingEngine::GetInfo().GetObjectName(subject.m_Index));
			subject.m_State = eStateFinalise;
		}
		break;

	case eStateFinalise:
		{
			defragDebugf1("defrag [%d] finalising: %s", subjectIndex,  strStreamingEngine::GetInfo().GetObjectName(subject.m_Index));

			int chunk = 0;

#if RESOURCE_HEADER
			if (subject.m_Map.IsOptimized())
			{
				chunk = 1;
			}
#endif
			for (; chunk < subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount;++chunk)
			{
				if (subject.m_ChunkState[chunk] == eStateFinalise)
				{
					sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(int(chunk>=subject.m_Map.VirtualCount)%NUM_HEAPS?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL);

					strStreamingEngine::GetAllocator().SanityCheck();

					TASKLOG("Unlocking src block in eStateFinalise %p",(size_t)subject.m_Map.Chunks[chunk].SrcAddr);
					alloc.UnlockBlock(subject.m_Map.Chunks[chunk].SrcAddr, 1);

					TASKLOG_ASSERT(strStreamingInfoManager::GetStreamingIndexFromUserData(alloc.GetUserData(subject.m_Map.Chunks[chunk].DestAddr)) == subject.m_Index);
					TASKLOG("Unlocking dest block in eStateFinalise %p owner %" SIZETFMT "u",(size_t)subject.m_Map.Chunks[chunk].DestAddr,subject.m_Index.Get());
					// TASKLOG_ASSERTF(alloc.GetUserData(subject.m_Map.Chunks[chunk].DestAddr) != ~0U,"Finalised defrag DEST block %p not owned? subjectIndex=%d, '%s'",subject.m_Map.Chunks[chunk].DestAddr,subjectIndex,strStreamingEngine::GetInfo().GetObjectName(subject.m_Index));
					alloc.UnlockBlock(subject.m_Map.Chunks[chunk].DestAddr, 1);

					strStreamingEngine::GetAllocator().SanityCheck();

					defragDebugf1("defrag [%d] freeing: %s 0x%p", subjectIndex, strStreamingEngine::GetInfo().GetObjectName(subject.m_Index), subject.m_Map.Chunks[chunk].SrcAddr);

#if !__FINAL
					// Make sure memory is totally nuked. 
					if (chunk < subject.m_Map.VirtualCount)
					{
						if (pgBase::IsMemoryTracked(subject.m_Map.Chunks[chunk].SrcAddr, subject.m_Map.Chunks[chunk].Size)) {
							// pgBase::DumpRefs();
							Quitf("Tried to nuke a pointer still being tracked, see TTY for details.");
						}
						IF_DEBUG_MEMORY_FILL_N(sysMemSet(subject.m_Map.Chunks[chunk].SrcAddr, 0xDE,subject.m_Map.Chunks[chunk].Size),DMF_DEFRAG_FREE);
					}
#endif
					RAGE_LOG_DELETE(subject.m_Map.Chunks[chunk].SrcAddr);
					strStreamingEngine::GetAllocator().RemoveFromMemoryUsed(subject.m_Map.Chunks[chunk].SrcAddr);
					sysMemAllowResourceAlloc++;
					{
#if RESOURCE_HEADER					
						Assert(!sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(subject.m_Map.Chunks[chunk].SrcAddr));
#endif
						MEM_USE_USERDATA(sysMemAllocator::GetMaster().GetUserData(subject.m_Map.Chunks[chunk].DestAddr));
						alloc.Free(subject.m_Map.Chunks[chunk].SrcAddr);
					}
					sysMemAllowResourceAlloc--;
					TASKLOG_ASSERTF(!alloc.GetSize(subject.m_Map.Chunks[chunk].SrcAddr), "Leaking - chunk %d (%p) not freed", chunk, subject.m_Map.Chunks[chunk].SrcAddr);

					subject.m_ChunkState[chunk] = eStateIdle;
				}
			}

			if (CountSiblingSubjects(subject.m_Index/*, true*/) == 1)
				info->ClearFlag(subject.m_Index, STRFLAG_INTERNAL_DEFRAGGING);

			defragDebugf1("defrag DONE [%d]: %s", subjectIndex,  strStreamingEngine::GetInfo().GetObjectName(subject.m_Index));

#if DEFRAG_TIME
			streamDisplayf("Defrag of %s took %.2fms (copy %.2fms place %.2fms exchange %.2fms)", 
				info->GetObjectName(), 
				subject.m_fCopyTime + subject.m_fPlaceTime + subject.m_fExchangeTime, 
				subject.m_fCopyTime,
				subject.m_fPlaceTime,
				subject.m_fExchangeTime);
#endif

#if DEBUG_DEFRAG_REFCNT
			strDefragDebugInfo *info = FindDefragDebugInfo(pModule, objectIndex.Get(), false, eStateFinalise);

			if (info)
			{
				info->RemoveRef();
				FreeDefragDebugInfo(pModule, objectIndex.Get());
			}
#endif // DEBUG_DEFRAG_REFCNT

			pModule->RemoveRef(objectIndex, REF_DEFRAG);
			pModule->DefragmentComplete(objectIndex);

			// If Lock was called on the resource while defrag was in flight,
			// then we need to transfer the lock count across to the individual
			// memory allocations so that the corresponding Unlock call will
			// work correctly.
			if (subject.m_CopyLockCount)
			{
				pgBase *const resource = (pgBase*)strStreamingEngine::GetInfo().GetObjectPtr(subject.m_Index);
				LockMemAllocs(resource, subject.m_Index, subject.m_CopyLockCount);
				subject.m_CopyLockCount = 0;
			}

			subject.m_State = eStateIdle;
			subject.m_Index = strIndex(strIndex::INVALID_INDEX);
		}
		break;
	case eStateIdle:
	default:
		streamAssert(false);
		break;
	}
}

void strDefragmentation::DefragmentFree(sysMemBuddyAllocator& alloc, void* ptr)
{
	sysMemAllocator::GetMaster().UnlockBlock(ptr);

	sysMemAllowResourceAlloc++;
	{
#if RESOURCE_HEADER	
		Assert(!sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(ptr));
#endif
		MEM_USE_USERDATA(sysMemAllocator::GetMaster().GetUserData(ptr));
		alloc.Free(ptr);
	}
	sysMemAllowResourceAlloc--;

	TASKLOG_ASSERTF(!alloc.GetSize(ptr), "Defrag node %p not freed", ptr);
}

void strDefragmentation::DefragmentAllocators()
{
#if DEFRAG_STATS
	sysTimer defragTimer;
#endif

#if __BANK
	m_AllocDefragCalls++;
#endif // __BANK

	sysMemDefragmentation defrag;
	sysMemDefragmentationFree defragFree;

#if ENABLE_DEFRAG_CALLBACK
	// downwards, so we bias to physical - they are cheaper to do since there is no exchange
	for (int heap=NUM_HEAPS-1; heap>=0; --heap)
	{
		sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(heap?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL);

		if (alloc.Defragment(defrag, defragFree))
		{
			defragDebugf1("defrag start: %d", defrag.Count);
			for (int i=0; i!=defrag.Count; ++i)
			{
				if (!AddDefragmentableHeapPage(defrag, i, heap))
				{
					TASKLOG("Unlocking To.From blocks in DefragmentAllocators %p/%p",(size_t)defrag.Nodes[i].To,(size_t)defrag.Nodes[i].From);
					sysMemAllocator::GetMaster().UnlockBlock(defrag.Nodes[i].From);
					DefragmentFree(alloc, defrag.Nodes[i].To);
				}
			}
		}

		// Free any nodes that are discardable
		for (int i = 0; i < defragFree.Count; ++i)
		{
#if ENABLE_DEFRAG_DEBUG
			s_defragRemoveCurrent = 0;
#endif
			void* ptr = defragFree.Nodes[i];

			if (alloc.GetSize(ptr))
			{
				strIndex owner = strStreamingEngine::GetAllocator().GetHeapPageOwner(ptr, !!heap);
				if (owner.IsValid())
				{
					if (strStreamingEngine::GetInfo().IsObjectReadyToDelete(owner, STRFLAG_NONE))
					{
						sysMemAllocator::GetMaster().UnlockBlock(ptr);

						// Delete the block right away
						if (strStreamingEngine::GetInfo().RemoveObject(owner))
						{
#if ENABLE_DEFRAG_DEBUG
							s_defragRemoveTotal++;
							s_defragRemoveCurrent++;
							if (s_defragRemoveCurrent > s_defragRemovePeak)
								s_defragRemovePeak = s_defragRemoveCurrent;
#endif
							// Success.
							streamDebugf2("Decided to delete %s instead of defragging it", strStreamingEngine::GetObjectName(owner));
							continue;
						}

						sysMemAllocator::GetMaster().TryLockBlock(ptr);
					}
				}
			}
		}
	}
#else
	// downwards, so we bias to physical - they are cheaper to do since there is no exchange
	for (int heap=NUM_HEAPS-1; heap>=0; --heap)
	{
		sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(heap?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL);

#if __PPU
		size_t maxSize = (heap == 0) ? 256 * 1024 : 2 * 1024 * 1024;	
#elif (RSG_DURANGO || RSG_ORBIS || __WIN32PC)
		size_t maxSize = 32 * 1024 * 1024;
#else
		size_t maxSize = 2 * 1024 * 1024;
#endif

		if (alloc.Defragment(defrag, defragFree, maxSize))
		{
			defragDebugf1("defrag start: %d", defrag.Count);
			for (int i=0; i!=defrag.Count; ++i)
			{
				if (!AddDefragmentableHeapPage(defrag, i, heap))
				{
					TASKLOG("Unlocking To.From blocks in DefragmentAllocators %p/%p",(size_t)defrag.Nodes[i].To,(size_t)defrag.Nodes[i].From);
					sysMemAllocator::GetMaster().UnlockBlock(defrag.Nodes[i].To);
					sysMemAllocator::GetMaster().UnlockBlock(defrag.Nodes[i].From);

					sysMemAllowResourceAlloc++;
					{
#if RESOURCE_HEADER					
						Assert(!sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL)->IsValidPointer(defrag.Nodes[i].To));
#endif
						MEM_USE_USERDATA(sysMemAllocator::GetMaster().GetUserData(defrag.Nodes[i].From));
						alloc.Free(defrag.Nodes[i].To);
					}
					sysMemAllowResourceAlloc--;
					TASKLOG_ASSERTF(!alloc.GetSize(defrag.Nodes[i].To), "Defrag node %d not freed", i);
				}
			}
		}
	}
#endif

#if DEFRAG_STATS
	s_defragTime += defragTimer.GetMsTime();
#endif
}


bool strDefragmentation::AddDefragmentableHeapPage(sysMemDefragmentation& defrag, int index, int heap)
{
	strIndex owner = strStreamingEngine::GetAllocator().GetHeapPageOwner(defrag.Nodes[index].From, !!heap);

	defragDebugf1("defrag node %d [%d]: 0x%p -> 0x%p", index, owner.Get(), defrag.Nodes[index].From, defrag.Nodes[index].To);

	streamAssert(owner != strIndex(strIndex::INVALID_INDEX));

	Subject* subject = NULL;

	s16 curr = m_FirstUsedSubject;
	while (curr != INVALID_SUBJECT)
	{
		if (m_Subjects[curr].m_State < eStateWaitOneFrame && m_Subjects[curr].m_Index == owner)
		{
			return false;
			/* subject = &m_Subjects[curr];
			break; */
		}
		curr = m_Subjects[curr].m_Next;
	}

	pgBase* resource = (pgBase*) strStreamingEngine::GetInfo().GetObjectPtr(owner);
	streamAssertf(resource, "Resource '%s' returned is unexpectedly NULL - state = %d", 
		strStreamingEngine::GetInfo().GetObjectName(owner), 
		strStreamingEngine::GetInfo().GetStreamingInfo(owner)->GetStatus());

	if (!subject && m_FirstFreeSubject != INVALID_SUBJECT)
	{
		subject = &m_Subjects[m_FirstFreeSubject];

		// add to used list
		curr = m_FirstFreeSubject;
		m_FirstFreeSubject = subject->m_Next;
		subject->m_Next = m_FirstUsedSubject;
		m_FirstUsedSubject = curr;
		
		datResourceMap& map = subject->m_Map;
		resource->RegenerateMap(map);

		subject->m_Index = owner;
		subject->m_bPhysicalOnly = true;
		streamAssert(subject->m_State==eStateIdle);

		int i = 0;

#if RESOURCE_HEADER
		if (map.IsOptimized())
		{
			i = 1;
		}
#endif
		for (; i<map.VirtualCount + map.PhysicalCount; ++i)
		{
			subject->m_ChunkState[i] = eStateIdle;
		}

		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(subject->m_Index);
		info->SetFlag(subject->m_Index, STRFLAG_INTERNAL_DEFRAGGING);
		// Don't allow this object to be deleted while being defragged.
		strStreamingModule* module = strStreamingEngine::GetInfo().GetModule(owner);

#if DEBUG_DEFRAG_REFCNT
		strDefragDebugInfo *defragInfo = FindDefragDebugInfo(module, module->GetObjectIndex(owner).Get(), true, -2);

		if (defragInfo)
		{
			defragInfo->m_RefsAdded++;
		}
#endif // DEBUG_DEFRAG_REFCNT


		bool bRefCount = !!(subject->m_Map.VirtualCount * !m_Frack);
		if (bRefCount)
		{
			module->AddRef(module->GetObjectIndex(owner), REF_DEFRAG);
		}

		defragDebugf1(	"defrag START [%d]: %s,%s",		curr, 
															strStreamingEngine::GetInfo().GetObjectName(subject->m_Index), 
															module->GetModuleName());

#if DEFRAG_TIME
		subject->m_fCopyTime = 0.0f;
		subject->m_fPlaceTime = 0.0f;
		subject->m_fExchangeTime = 0.0f;
#endif

#if DEFRAG_STATS
		PF_INCREMENT(subjects);
#endif
	}

	if (subject)
	{
#if __BANK
		m_DefragStarts++;
#endif // __BANK

		int ptrSlot = resource->MapContainsPointer(defrag.Nodes[index].From);
		// Looks like ptrSlot==-1 could be a possible cause of B*737907 (the
		// corruption in the Subject struct looks suspiciously like writing to
		// m_Map.Chunks[-1].DestAddr).  Use a trap here so that we crash and get
		// a coredump (if ptrSlot is -1 we are going to crash and burn soon
		// anyways).
		TrapLT(ptrSlot, 0);
		streamAssert(subject->m_ChunkState[ptrSlot] == eStateIdle);
		streamAssert(subject->m_Index == owner);
		subject->m_Map.Chunks[ptrSlot].DestAddr = defrag.Nodes[index].To;
		strStreamingEngine::GetAllocator().AddToMemoryUsed(defrag.Nodes[index].To);
		USE_MEMBUCKET(MEMBUCKET_DEFAULT);
		RAGE_LOG_NEW(defrag.Nodes[index].To, subject->m_Map.Chunks[ptrSlot].Size, __FILE__, __LINE__);
		subject->m_ChunkState[ptrSlot] = (ptrSlot>=subject->m_Map.VirtualCount?eStateCopyPhysical:eStateCopy);
		subject->m_State = (subject->m_State==eStateCopyPhysical?eStateCopyPhysical:subject->m_ChunkState[ptrSlot]);

#if __BANK
#if __PPU
		int pageHeapIndex = (int) (ptrSlot >= subject->m_Map.VirtualCount);
#else // __PPU
		int pageHeapIndex = 0;
#endif // _PPU
		m_AveragePageSize[pageHeapIndex] = m_AveragePageSize[pageHeapIndex] * 0.95f + (float) subject->m_Map.Chunks[ptrSlot].Size * 0.05f;
#endif // __BANK

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		{
			diagTracker::GetCurrent()->UnTally(defrag.Nodes[index].From, subject->m_Map.Chunks[ptrSlot].Size);
			diagTracker::GetCurrent()->Tally(defrag.Nodes[index].To, subject->m_Map.Chunks[ptrSlot].Size, heap);
			diagTracker::GetCurrent()->MarkMoved(defrag.Nodes[index].To, true);
			diagTracker::GetCurrent()->MarkDefragmentable(defrag.Nodes[index].To, true);
		}
#endif

		if (ptrSlot<subject->m_Map.VirtualCount)
			subject->m_bPhysicalOnly = false;
			
		// Don't test, just patch: probably cheaper.
		subject->m_Map.VirtualBase = subject->m_Map.Chunks[subject->m_Map.RootVirtualChunk].DestAddr;

#if __ASSERT
		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(owner);
		streamAssertf(pModule->CanDefragment(), "Can't defrag nodes that belong to a module (%s) that doesn't support defrag", pModule->GetModuleName());
		
		defragDebugf1("defrag [%d] node added %d: %s  0x%p -> 0x%p (%" SIZETFMT "d) [%d]", curr, owner.Get(),  strStreamingEngine::GetInfo().GetObjectName(owner),  subject->m_Map.Chunks[ptrSlot].SrcAddr, subject->m_Map.Chunks[ptrSlot].DestAddr, subject->m_Map.Chunks[ptrSlot].Size, ptrSlot);
#endif // __ASSERT


		SANITY_CHECK();

#if DEFRAG_STATS
		PF_INCREMENT(chunks);
		PF_INCREMENTBY(chunks_size, defrag.Nodes[index].Size);
#endif

		return true;
	}
	else
	{
		// no free subject...
		return false;
	}
}


void strDefragmentation::AddResource(pgBase* resource, datResourceMap& map, strIndex index)
{
	(void) index;

	int mapCount = map.VirtualCount + map.PhysicalCount;

#if __ASSERT
	for (int j=0; j<mapCount; j++)
		for (int k=j+1; k<mapCount; k++)
			streamAssertf(map.Chunks[j].DestAddr != map.Chunks[k].DestAddr,"Screwed up resource map, this is really bad (%p is in there twice)",map.Chunks[j].DestAddr);
#endif

	int i = 0;

#if RESOURCE_HEADER
	if (map.IsOptimized())
	{
		i = 1;
	}
#endif
	for (; i<mapCount; ++i)
	{
		ASSERT_ONLY(sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(i>=map.VirtualCount?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL));
		AssertMsg(alloc.GetBlockLockCount(map.Chunks[i].DestAddr) >= 1,"Attempting to add a resource that is already unlocked and defragmentable?");
		TASKLOG("Add heap page owner %p: %u",(size_t)map.Chunks[i].DestAddr,index.Get());
		streamAssert(strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)) == index);
	}
	resource->MakeDefragmentable(map,true);
}


void strDefragmentation::RemoveResource(pgBase* TASKLOG_ONLY(resource), strIndex TASKLOG_ONLY(index))
{
#if __TASKLOG
	datResourceMap deathMap;
	resource->RegenerateMap(deathMap);
	int mapCount = deathMap.VirtualCount + deathMap.PhysicalCount;
	sysMemBuddyAllocator *const virtAlloc = (sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	sysMemBuddyAllocator *const physAlloc = (sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
		
	int i = 0;

#if RESOURCE_HEADER
	if (deathMap.IsOptimized())
	{
		i = 1;
	}
#endif
	for (; i<mapCount; ++i)
	{
		TASKLOG("Remove heap page owner %p: %u (size 0x%08x)",(size_t)deathMap.Chunks[i].DestAddr,index.Get(),
			(i<deathMap.VirtualCount?virtAlloc:physAlloc)->GetSize(deathMap.Chunks[i].DestAddr));
	}
#endif // __TASKLOG
}

void strDefragmentation::LockMemAllocs(pgBase* resource, strIndex ASSERT_ONLY(index), unsigned lockCount)
{
	datResourceMap map;
	resource->RegenerateMap(map);

#if __ASSERT
	char nameBuf[128];
	const char *const name = strStreamingEngine::GetInfo().GetObjectName(index, nameBuf, NELEM(nameBuf));
#endif

	sysMemBuddyAllocator* allocator = (sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	
	u32 i = 0;

#if RESOURCE_HEADER
	if (map.IsOptimized())
	{
		i = 1;
	}
#endif
	for (; i<map.VirtualCount; i++) 
	{
		// Lock the underlying memory block so it's not defragmentable.
		streamAssertf(strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)) == index,
			"0x%08x != 0x%08x (\"%s\")\n", strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)).Get(), index.Get(), name);
		TASKLOG("Lock alloc %p",(size_t)map.Chunks[i].DestAddr);
		ASSERT_ONLY(const bool ok0 =) allocator->TryLockBlock(map.Chunks[i].DestAddr, lockCount);
		streamAssert(ok0); // higher level code should have ensured there was no defrag in process

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		{
			diagTracker::GetCurrent()->MarkDefragmentable(map.Chunks[i].DestAddr, false);
			diagTracker::GetCurrent()->MarkLocked(map.Chunks[i].DestAddr, true);			
		}
#endif // RAGE_TRACKING
	}

	allocator = (sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	for (u32 i=map.VirtualCount; i<u32(map.VirtualCount+map.PhysicalCount); i++) {
		// Lock the underlying memory block so it's not defragmentable.
		streamAssertf(strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)) == index,
			"0x%08x != 0x%08x (\"%s\")\n", strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)).Get(), index.Get(), name);
		TASKLOG("Lock alloc %p",(size_t)map.Chunks[i].DestAddr);
		ASSERT_ONLY(const bool ok1 =) allocator->TryLockBlock(map.Chunks[i].DestAddr, lockCount);
		streamAssert(ok1); // higher level code should have ensured there was no defrag in process

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		{
			diagTracker::GetCurrent()->MarkDefragmentable(map.Chunks[i].DestAddr, false);
			diagTracker::GetCurrent()->MarkLocked(map.Chunks[i].DestAddr, true);
		}
#endif // RAGE_TRACKING
	}
}

void strDefragmentation::UnlockMemAllocs(pgBase* resource, strIndex ASSERT_ONLY(index), unsigned unlockCount)
{
	datResourceMap map;
	resource->RegenerateMap(map);

#if __ASSERT
	char nameBuf[128];
	const char *const name = strStreamingEngine::GetInfo().GetObjectName(index, nameBuf, NELEM(nameBuf));
#endif

	sysMemBuddyAllocator* allocator = (sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);

	u32 i = 0;

#if RESOURCE_HEADER
	if (map.IsOptimized())
	{
		i = 1;
	}
#endif
	for (; i<map.VirtualCount; i++) {
		// Unlock the underlying memory block so it's defragmentable.
		streamAssertf(strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)) == index,
			"0x%08x != 0x%08x (\"%s\")\n", strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)).Get(), index.Get(), name);
		TASKLOG("Unlock alloc %p",(size_t)map.Chunks[i].DestAddr);
		allocator->UnlockBlock(map.Chunks[i].DestAddr, unlockCount);

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		{
			diagTracker::GetCurrent()->MarkLocked(map.Chunks[i].DestAddr, false);
			diagTracker::GetCurrent()->MarkDefragmentable(map.Chunks[i].DestAddr, true);
		}
#endif // RAGE_TRACKING
	}

	allocator = (sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	for (u32 i=map.VirtualCount; i<u32(map.VirtualCount+map.PhysicalCount); i++) {
		// Unlock the underlying memory block so it's defragmentable.
		streamAssertf(strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)) == index,
			"0x%08x != 0x%08x (\"%s\")\n", strStreamingEngine::GetAllocator().GetHeapPageOwner(map.Chunks[i].DestAddr, !!(int(i>=map.VirtualCount)%NUM_HEAPS)).Get(), index.Get(), name);
		TASKLOG("Unlock alloc %p",(size_t)map.Chunks[i].DestAddr);
		allocator->UnlockBlock(map.Chunks[i].DestAddr, unlockCount);

#if RAGE_TRACKING
		if (diagTracker::GetCurrent() && !sysMemVisualize::GetInstance().HasXTL())
		{
			diagTracker::GetCurrent()->MarkLocked(map.Chunks[i].DestAddr, false);
			diagTracker::GetCurrent()->MarkDefragmentable(map.Chunks[i].DestAddr, true);
		}
#endif // RAGE_TRACKING
	}
}

void strDefragmentation::Lock(pgBase* resource, strIndex index)
{
	// Check if defrag has already started on this resource
	s16 curr = m_FirstUsedSubject;
	while (curr != INVALID_SUBJECT)
	{
		Subject *const s = m_Subjects+curr;
		if (Unlikely(s->m_Index == index))
		{
			// It has, so increment the lock count to prevent copying and placement
			TASKLOG("Lock subject %p",(size_t)resource);
			++(s->m_CopyLockCount);
			streamAssert(s->m_CopyLockCount);
			return;
		}
		curr = s->m_Next;
	}

	// Defrag hasn't been started, so increment the lock counts on the
	// individual memory allocations to prevent a defrag of this resource
	// beginning.
	LockMemAllocs(resource, index, 1);
}

void strDefragmentation::Unlock(pgBase* resource, strIndex index)
{
	// Check if defrag has already started on this resource
	s16 curr = m_FirstUsedSubject;
	while (curr != INVALID_SUBJECT)
	{
		Subject *const s = m_Subjects+curr;
		if (Unlikely(s->m_Index == index))
		{
			// It has, so decrement the lock count to allow copying and placement
			TASKLOG("Unlock subject %p",(size_t)resource);
			streamAssert(s->m_CopyLockCount);
			--(s->m_CopyLockCount);
			return;
		}
		curr = s->m_Next;
	}

	// Didn't already have a defrag subject, so go through each memory
	// allocation of the resource and unlock them.
	UnlockMemAllocs(resource, index, 1);
}

#if __DEV

void strDefragmentation::SanityCheck()
{
	for (s16 curr = m_FirstUsedSubject; curr != INVALID_SUBJECT; curr = m_Subjects[curr].m_Next)
	{
		Subject& subject = m_Subjects[curr];

		//strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(subject.m_Index);

		streamAssert(subject.m_State != eStateIdle);

		int i = 0;

#if RESOURCE_HEADER
		if (subject.m_Map.IsOptimized())
		{
			i = 1;
		}
#endif
		for (; i != subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount; ++i)
		{
			streamAssert(subject.m_ChunkState[i] == eStateIdle || subject.m_ChunkState[i] >= subject.m_State);

			if (subject.m_ChunkState[i] != eStateIdle)
			{
	#if __TASKLOG && __ASSERT
				sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(int(i>=subject.m_Map.VirtualCount)%NUM_HEAPS?MEMTYPE_RESOURCE_PHYSICAL:MEMTYPE_RESOURCE_VIRTUAL);

				TASKLOG_ASSERT(alloc.GetBlockLockCount(subject.m_Map.Chunks[i].SrcAddr));
				TASKLOG_ASSERTF(alloc.GetSize(subject.m_Map.Chunks[i].SrcAddr), "Defrag %s contains free source chunk %d = 0x%p [%" SIZETFMT "u]", strStreamingEngine::GetInfo().GetObjectName(subject.m_Index), i, subject.m_Map.Chunks[i].SrcAddr, subject.m_Map.Chunks[i].Size);
	#endif
			}
		}
	}

	//sysMemAllocator::GetCurrent().SanityCheck();

	for (s16 curr = m_FirstFreeSubject; curr != INVALID_SUBJECT; curr = m_Subjects[curr].m_Next)
	{
		Subject& subject = m_Subjects[curr];

		streamAssert(subject.m_State == eStateIdle);

		int i = 0;

#if RESOURCE_HEADER
		if (subject.m_Map.IsOptimized())
		{
			i = 1;
		}
#endif
		for (; i != subject.m_Map.VirtualCount + subject.m_Map.PhysicalCount; ++i)
		{
			streamAssert(subject.m_ChunkState[i] == eStateIdle);
		}
	}
}

#endif // __DEV


#if __BANK
void strDefragmentation::DisplayStatus()
{
	for (int x=0; x<MAX_SUBJECTS; x++)
	{
		Subject &subject = m_Subjects[x];

		const char *status = s_StateNames[subject.m_State];
		const char *name = (subject.m_Index.IsValid()) ? strStreamingEngine::GetObjectName(subject.m_Index) : "[empty]";
		grcDebugDraw::AddDebugOutputEx(false, false, "%2d: %-50s %-15s", x, name, status);
	}

	// Overall stats.
	float heartbeatsf = (float) m_Heartbeats;
	float allocDefragCalls = (float) m_AllocDefragCalls / heartbeatsf;
	float defragStarts = (float) m_DefragStarts / heartbeatsf;
	float cockblockPercentage = (float) m_CockblockedByMtPlacement * 100.0f / heartbeatsf;
	grcDebugDraw::AddDebugOutputEx(false, false, "Avg per frame allocator defrags: %.4f, defrag starts: %.4f", allocDefragCalls, defragStarts);
	grcDebugDraw::AddDebugOutputEx(false, false, "Total allocator defrags: %d, defrag starts: %d", m_AllocDefragCalls, m_DefragStarts);
	grcDebugDraw::AddDebugOutputEx(false, false, "Frames skipped due to MT placement: %d / %d (%.2f%%)", m_CockblockedByMtPlacement, m_Heartbeats, cockblockPercentage);
	grcDebugDraw::AddDebugOutputEx(false, false, "Average processing time: %.2fms, page size %dKB"		PPU_ONLY(" Virt, %dKB Phys"),
		m_AverageProcessTime, (int) (m_AveragePageSize[0] / 1024.0f) PPU_ONLY(, (int) (m_AveragePageSize[1] / 1024.0f)));
}

bool *strDefragmentation::GetDefragoramaBoolPtr()
{
	return &s_Defragorama;
}
#endif // __BANK


#endif // USE_DEFRAGMENTATION

}	// namespace rage

#undef SANITY_CHECK
