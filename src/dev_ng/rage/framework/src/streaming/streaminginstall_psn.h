//
// streaming/streaminginstall_psn.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_STREAMINGINSTALL_PSN_H
#define STREAMING_STREAMINGINSTALL_PSN_H

#include "streaminginstall.h"

#if __PPU
#include <np/drm.h>

#include "atl/array.h"
#include "atl/string.h"
#include "atl/queue.h"
#include "system/ipc.h"
#include <sysutil/sysutil_gamecontent.h>
#include "file/device_relative.h"
#include <np/drm.h>

namespace rage {

class fiPsnUpdateDevice : public fiDeviceRelative
{
public:
	static fiPsnUpdateDevice& Instance();

private:
	fiPsnUpdateDevice() {}
	virtual ~fiPsnUpdateDevice() { }

	static fiPsnUpdateDevice m_device;
};


//	Based heavily on fiPsnUpdateDevice above
//	Calls cellGameDataCheckCreate2 to get the path to the game data directory
//	If the GTA4 episodic data exists, it will be in a sub-directory of the game data directory
class fiPsnExtraContentDevice : public fiDeviceRelative
{
public:
	static fiPsnExtraContentDevice& Instance();

	static char *GetGameDataPath() {return &ms_GameDataPath[0]; }

protected:

	static bool CopyContentInfoFile(const char* filename, const char* contentInfoPath);

private:
	static fiPsnExtraContentDevice m_device;
	
	static char ms_GameDataPath[CELL_GAME_PATH_MAX];
};


 
enum eFileOpenFunctionToUse
{
	FILE_OPEN,
	FILE_OPEN_BULK,
	FILE_OPEN_BULK_DRM
};


//	fiFileDetails - this class is used to maintain a static queue of currently open files.
//					The PS3 can only have 31 files open at the same time.
//					The queue is shared by the installed GTA4 files and the files of the currently active episode.
//					fiPsnInstallerDevice (the installed GTA4 files) and fiDeviceRelativePsn (the episodic files)
//					also each contain an array of fiFileDetails for keeping track of the files associated with that device.
class fiFileDetails
{
public:
	char			m_SrcFilename[96];
	char			m_InstallName[32];
	volatile bool	m_Installed;			//	not used by fiDeviceRelativePsn
	eFileOpenFunctionToUse		m_FileOpenFunctionToUse;			//	Should this be volatile?
	bool			m_ReadOnly;
	bool			m_IsContentInfo;		//	not used by fiDeviceRelativePsn
	const fiDevice	*m_pDevice;
	u32				m_SeekPos;
	fiHandle		m_Handle;

	fiFileDetails();

	//	If the queue is already full then close the file that was opened the longest time ago 
	//	and remove it from the queue. This is in preparation for a new file being opened and pushed
	//	on to the queue.
	static void MakeSpaceForOneNewEntryIfNecessary();

	static void PushOnToQueue(fiFileDetails *pFileDetails);
	static void RemoveFromQueue(fiFileDetails *pFileDetails);

private:
	// PS3 only allows 31 open game data files so we manage this here
	static const u32 MAX_OPEN_FILES = 16;
	static atQueue<fiFileDetails*, MAX_OPEN_FILES> m_OpenFiles;
};


class fiPsnInstallerDevice : public fiInstallerDevice
{
public:
	fiPsnInstallerDevice(bool forceReinstall = false);

	virtual fiHandle	InstallFile(const char* path, bool readOnly = true,
							const char* installedname = 0, bool isContentInfo = false);
	virtual fiHandle	InstallIconFile(const char* path);
	virtual bool		InitInstall();
	virtual void		InstallOpenedFiles();
	virtual bool		IsInstalled(fiHandle handle);	
	virtual bool		IsEverythingInstalled();
	virtual bool		FinishInstallation();
	virtual int			Close(fiHandle handle) const;
	virtual float		InstallProgress(); // value from 0 - 1
    virtual eInstallStatus InstallStatus();
	virtual void		SetProgressCallback(Functor2<eInstallStatus, float> fn);
	virtual void		SetProgress(eInstallStatus, float);

private:
	struct fiInstalledFile
	{
		char			m_SrcFilename[96];
		char			m_InstallName[32];
		volatile bool	m_Installed;
		bool			m_ReadOnly;
		bool			m_IsContentInfo;
		u32				m_SeekPos;
		fiHandle		m_Handle;
	};

	virtual fiHandle	LocalHandle(fiHandle handle) const;
	virtual void		GetInstalledPath(char* buf, u32 bufsize, const char* filename) const;
	void				GetInstalledPath(char* buf, u32 bufsize, const fiInstalledFile& f) const;

	u32					CalculateSizeToInstall();
	
	static void			InstallerThreadStub(void* ptr);
	void				InstallerThread();

	bool								m_StartedInstalling;
	volatile bool						m_FinishedInstalling;
	volatile bool						m_InstallFailed;
    volatile bool                       m_NotEnoughSpace;
	u64									m_BytesCopied;
	u64									m_InstallSize;
	bool								m_ForceReinstall;
	char								m_ContentInfoDir[CELL_GAME_PATH_MAX];
	char								m_GameDataDir[CELL_GAME_PATH_MAX];
	mutable atArray<fiInstalledFile>	m_Files;
	sysIpcThreadId						m_Thread;

	Functor2<eInstallStatus, float>		m_progressCallback;

	// PS3 only allows 31 open game data files so we manage this here
	static const u32 MAX_OPEN_FILES = 16; 
	mutable atQueue<fiInstalledFile*, MAX_OPEN_FILES> m_OpenFiles;
};


// fiDeviceRelativePsn - device to read from the episodic data folder on the PS3's HDD
class fiDeviceRelativePsn : public fiDeviceRelative
{
public:
	fiDeviceRelativePsn();
	virtual ~fiDeviceRelativePsn();

	virtual fiHandle Open(const char *filename,bool readOnly) const;
	virtual fiHandle OpenBulk(const char *filename,u64&bias) const;
	virtual fiHandle OpenBulkDrm(const char *filename,u64 &bias,const void *pDrmKey) const;

	virtual int Read(fiHandle handle,void *outBuffer,int bufferSize) const;
	virtual int ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual int Seek(fiHandle handle,int offset,fiSeekWhence whence) const;
	virtual int Write(fiHandle handle,const void *buffer,int bufferSize) const;
//	WriteBulk doesn't appear to exist in fiDeviceRelative so maybe I don't need to implement it
	virtual int Close(fiHandle handle) const;
	virtual int CloseBulk(fiHandle handle) const;

	void ResetForMapDataShutdown();

private:
	virtual fiHandle	LocalHandle(fiHandle handle) const;

//	The following function is based on fiPsnInstallerDevice::InstallFile
	virtual fiHandle	AddToFilesArray(const char* path, bool readOnly, eFileOpenFunctionToUse OpenFunctionToUse);

	//	I couldn't use an atArray here (as is used in fiPsnInstallerDevice) because files are still being
	//	added to the array after fiFileDetails::m_OpenFiles is in use and Growing the atArray was breaking the pointers in m_OpenFiles
	static const u32 MAX_FILES_ON_RELATIVE_DEVICE = 256;
	static const u32 SIZE_OF_FILENAME_MAP = 257;	//	A prime number - does it have to be greater than MAX_FILES_ON_RELATIVE_DEVICE?
	mutable fiFileDetails				m_FileArray[MAX_FILES_ON_RELATIVE_DEVICE];
	atMap<const char *, u32>			m_FilenameMap;		//	To avoid lots of strcmp's in AddToFilesArray
	u32 m_nNumberOfFiles;

	mutable SceNpDrmKey m_DrmKey;
};



}	// namespace rage

#endif // __PPU

#endif // STREAMING_STREAMINGINSTALL_PSN_H
