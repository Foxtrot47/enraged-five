//
// streaming/streamingengine.h
//
// Copyright (C) 1999-2013 Rockstar North.  All Rights Reserved. 
//

#ifndef STREAMING_STREAMINGENGINE_H
#define STREAMING_STREAMINGENGINE_H

#include "streamingdefs.h"
#include "streaminginfo.h"
#include "streamingmodulemgr.h"
#include "file/handle.h"

#include "streamingallocator.h"
#include "streamingloader.h"
#include "streaminglive.h"
#include "streaminginstall.h"

namespace rage {

class bkBank;
class strDefragmentation;
class strStreamingInfoManager;
class fiPackfile;
class fiDevice;

class strStreamingInterface
{
public:
	strStreamingInterface(size_t virtualSize, size_t physicalSize, int archiveCount) :
	  m_VirtualSize(virtualSize), m_PhysicalSize(physicalSize), m_ArchiveCount(archiveCount) {}
  	virtual ~strStreamingInterface() {}

	// CStreaming
	virtual void LoadAllRequestedObjects(bool /*bPriorityRequestsOnly*/ = false) {}

	// CStreamingCleanup entry points:
	virtual void RequestFlush(bool) {}		// CStreaming at game level
	virtual bool MakeSpaceForResourceFile(const char* /*filename*/, const char* /*ext*/, s32 /*version*/) { return true; }
	virtual bool MakeSpaceForMap(datResourceMap& /*map*/, strIndex /*index*/, bool /*keepMemory*/) { return true; }

	virtual bool WasLastAllocationAborted() const { return false; }
	virtual bool WasLastAllocationFragmented() const { return false; }

	// CMissionCleanup:
	virtual void GetScriptMemoryUsage(int&,int&) {}

	// CStreamingDebug:
	virtual void DisplayModuleMemory(bool) {}
	virtual void PrintMemoryDistribution(const sysMemDistribution& /*distribution*/,  char* /*header*/, char* /*usedMemory*/, char* /*freeMemory*/) {}
	virtual void LogStreaming(strIndex) {}
	virtual void AddFailedStreamingAllocation(strIndex, bool, datResourceMap&) {}
	virtual void FullMemoryReport() {}

	// CStreamingRequestList:
	virtual void RecordRequest(strIndex) {}

	// strStreamingEngine PS3 install stuff:
	virtual void InstallFailure() {}

	virtual void PostLoadRpf(int /*index*/) {}
	
	virtual void AddResidentObject(strIndex) {}
	virtual void RemoveResidentObject(strIndex) {}

	virtual void DumpStreamingInterests() {}

	// Accessors
	size_t GetVirtualSize() const	{ return m_VirtualSize; }
	size_t GetPhysicalSize() const	{ return m_PhysicalSize; }
	int GetArchiveCount() const	{ return m_ArchiveCount; }

private:
	size_t m_VirtualSize;
	size_t m_PhysicalSize;

	int m_ArchiveCount;
};

extern strStreamingInterface *g_strStreamingInterface;


class strStreamingEngine
{
public:
	static void InitClass(strStreamingInterface * streamingInterface);

	static bool IsInitialised() {return ms_bIsInitialised;}

	static void Init();
	static void Shutdown();

	static void InitLive(const char *folderOverride = NULL);
	static void InitLevelWithMapUnloaded();
	static void ShutdownLevelWithMapUnloaded();

	static void InitLevelWithMapLoaded();
	static void InitPackfiles();

//	This is currently not being called but I think it should be when the map data is unloaded.
//	Maybe the call to it was lost when the new init/shutdown system went in
//	static void ShutdownLevelWithMapLoaded();

	static void ShutdownSession();
	static void ShutdownPackfiles();

	static void Update();
	static void Flush();
	static void SubmitDeferredAsyncPlacementRequests();

	// Update function, called right before we finalize streaming objects and set them to "loaded"
	static void PreLoadedUpdate();

	
	static void DisableStreaming() {ms_bDisableStreaming = true;}
	static void EnableStreaming() {ms_bDisableStreaming = false;}
	static bool IsStreamingDisabled() {return ms_bDisableStreaming;}
	static void SetIsLoadingPriorityObjects(bool bLoading) {ms_bLoadingPriorityObjects = bLoading;}
	static bool GetIsLoadingPriorityObjects() {return ms_bLoadingPriorityObjects;}
	static void SetLoadSceneActive(bool bActive) { ms_bLoadSceneActive = bActive; }
	static bool GetLoadSceneActive() { return ms_bLoadSceneActive; }

	static void SetBurstMode(bool burstMode)	{ ms_bBurstMode = burstMode; }
	static bool GetBurstMode()					{ return ms_bBurstMode; }

	static void ResetTimers();

#if STREAMING_TELEMETRY_INTERFACE
	static bool IsTelemetryLogEnabled()		{ return ms_EnableTelemetryLog; }
	static bool *GetEnableTelemetryLogPtr()	{ return &ms_EnableTelemetryLog; }

#endif // STREAMING_TELEMETRY_INTERFACE

#if __PS3
	static fiInstallerDevice *GetGameData();
#endif // __PS3

#if LIVE_STREAMING
	static strStreamingLive& GetLive() {return ms_live;}
#endif // LIVE_STREAMING
	static strStreamingAllocator& GetAllocator() {return ms_allocator;}
	static strStreamingInfoManager& GetInfo() {return ms_info;}
	static strStreamingLoaderManager&	GetLoader() {return ms_loader;}
#if USE_DEFRAGMENTATION
	static strDefragmentation*	GetDefragmentation() {return ms_pDefragmentation;}
#endif

#if !__NO_OUTPUT
	static bool DebugPrintAllocInfo(const void *ptr);
#endif

	static strIndex GetStrIndex(const void *ptr);
	static bool AddRefByPtr(void *ptr, strRefKind strRefKind);
	static bool RemoveRefByPtr(void *ptr, strRefKind strRefKind);

#if !__NO_OUTPUT
	static const char* GetObjectName(strIndex index);
	static const char* GetObjectPath(strIndex index);
#endif

#if __ASSERT
	static void DumpLoadingStateCb();
#endif // __ASSERT

#if __BANK
	static void DebugDraw();
#endif // __BANK

#if __PPU
	static void SetInstallProgressCallback(Functor2<eInstallStatus, float> fn);
#endif

	static bool UseDiskCache();
private:
	static bool						ms_bDisableStreaming;		
	static bool						ms_bIsInitialised;				
	static bool						ms_bLoadingPriorityObjects;
	static bool						ms_bLoadSceneActive;
	static bool						ms_bBurstMode;

#if STREAMING_TELEMETRY_INTERFACE
	static bool						ms_EnableTelemetryLog;
#endif // STREAMING_TELEMETRY_INTERFACE

	static strStreamingAllocator	ms_allocator;
	static strStreamingInfoManager	ms_info;
	static strStreamingLoaderManager	ms_loader;

#if LIVE_STREAMING
	static strStreamingLive			ms_live;
#endif // LIVE_STREAMING	

#if USE_DEFRAGMENTATION
	static strDefragmentation*		ms_pDefragmentation;
#endif // USE_DEFRAGMENTATION
};

}	// namespace rage

#endif // STREAMING_STREAMINGENGINE_H
