//
// streaming/streaminginstall.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef STREAMING_STREAMINGINSTALL_H
#define STREAMING_STREAMINGINSTALL_H

#include "file/device.h"
#include "atl/functor.h"

namespace rage {

enum eInstallStatus
{
	INSTALL_OK,
	INSTALL_NOT_STARTED,
	INSTALL_ERROR_NOT_ENOUGH_SPACE,
    INSTALL_ERROR_INSTALLING
};

class fiInstallerDevice : public fiDevice
{
public:
	virtual fiHandle	Open(const char *filename,bool readOnly) const;
	virtual fiHandle	OpenBulk(const char *filename,u64 &outBias) const;
	virtual int			Read(fiHandle handle, void *outBuffer,int bufferSize) const;
	virtual int			ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const;
	virtual u64			GetFileSize(const char *filename) const;
	virtual u64			GetFileTime(const char *filename) const;
	virtual int			Seek(fiHandle handle, int pos, fiSeekWhence whence) const;
	virtual bool		SetFileTime(const char *filename,u64 timestamp) const;
	virtual int			CloseBulk(fiHandle handle) const;

	virtual fiHandle	Create(const char* filename) const;
	virtual fiHandle	CreateBulk(const char* filename) const;
	virtual int			Write(fiHandle h, const void* buf, int bufsize) const;
	virtual int			WriteBulk(fiHandle h, u64 offset, const void* buf, int bufsize) const;	

	virtual fiHandle	InstallIconFile(const char* path) = 0;
	virtual fiHandle	InstallFile(const char* path, bool readOnly = true,
							const char* installedname = 0, bool isContentInfo = false) = 0;
	virtual bool		InitInstall() = 0;
	virtual void		InstallOpenedFiles() = 0;
	virtual bool		IsInstalled(fiHandle handle) = 0;	
	virtual float		InstallProgress() = 0; // value from 0 - 1
    virtual eInstallStatus InstallStatus() = 0;
	virtual bool		IsEverythingInstalled() = 0;
	virtual bool		FinishInstallation() = 0;

	virtual void		SetProgressCallback(Functor2<eInstallStatus, float> fn) = 0;
	virtual void		SetProgress(eInstallStatus, float) = 0;

protected:
	virtual fiHandle	LocalHandle(fiHandle handle) const = 0;
	virtual void		GetInstalledPath(char* buf, u32 bufsize, const char* srcname) const = 0;
};

}	// namespace rage

#endif // STREAMING_STREAMINGINSTALL_H
