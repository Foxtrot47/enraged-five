//
// streaming/streamingloader.cpp 
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

#include "streaming/streaming_channel.h"

STREAMING_OPTIMISATIONS()

#include "streamingloader.h"

#include "diag/output.h"
#include "file/diskcache.h"
#include "file/packfile.h"
#include "grcore/debugdraw.h"
#include "grcore/texturegcm.h"
#include "grcore/resourcecache.h"
#include "fwdrawlist/drawlist.h"
#include "fwrenderer/renderthread.h"
#include "fwscene/stores/txdstore.h"
#include "paging/rscbuilder.h"
#include "paging/streamer.h"
#include "profile/cputrace.h"
#include "profile/profiler.h"
#include "profile/timebars.h"
#include "system/bootmgr.h"
#include "system/buddyallocator.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/tasklog.h"
#include "system/threadtype.h"

#include "streaming/packfilemanager.h"
#include "streaming/streamingengine.h"
#include "streaming/streamingmodule.h"
#include "streaming/streamingvisualize.h"
#include "streaming/defragmentation.h"
#include "string/stringutil.h"
#include "fwsys/timer.h"
#include "grprofile/timebars.h"

// #include "script/mission_cleanup.h"

#if __BANK

tStreamingLoaderManagerTrackingCallback gStreamingLoaderPreCallback = NULL;
tStreamingLoaderManagerTrackingCallback gStreamingLoaderPostCallback = NULL;
static rage::sysTimer s_LoadAllTimer;

#endif	//__BANK

#define EXTRA_TIMEBARS(x)

namespace rage {

#if __BANK
const char *strStreamingLoader::StreamingFile::sm_StatusStrings[strStreamingLoader::STATUS_MAX_VALUE] = {
	"STATUS_IDLE",
	"STATUS_LOADING",
	"STATUS_CANCELED",
};
#endif // __BANK

PARAM(nomtplacement, "[streaming] Enable placing select resources on a separate thread");
PARAM(dontshowrscnameintracker, "[streaming] Don't send the name of each resource to the memory tracker");
PARAM(noassertonlargevirtchunks, "[streaming] Don't assert if we try to load assets with large virtual chunks");
PARAM(disableExtraMem, "Disable streamer from going over video memory budget");

#if __BANK
bool strStreamingLoader::ms_IsPumpAndDump;
bool strStreamingLoader::ms_IsSirCancelALot;
int strStreamingLoader::ms_SirCancelALotFrequency = 10;
#endif // __BANK
bool strStreamingLoader::ms_ExtremeStreaming_Enabled = true;
int strStreamingLoader::ms_MaxPendingRequestThreshold = 30;
s32 strStreamingLoader::ms_ExtremeStreaming_MaxPendingKb = MAX_PENDING_KB;
s32 strStreamingLoader::ms_ExtremeStreaming_MaxConvertMs = MAX_CONVERT_MS;
u32	strStreamingLoader::ms_LsnTolerance[pgStreamer::DEVICE_COUNT] = { /*100*/ 0x4000000 , 0x4000000 };
int strStreamingLoader::ms_MaxChainedRequests = 24;

extern bool g_AREA51;
extern int g_AREA51_FRAME;

#if !__FINAL
//static char gStreamedObjName[255];
#endif

#if __STATS
PF_PAGE(StreamLoaderPage, "Streaming Loader");
PF_GROUP(StreamLoader);
PF_LINK(StreamLoaderPage, StreamLoader);

PF_TIMER(GetNextRequest, StreamLoader);
PF_TIMER(RemoveRequest, StreamLoader);
PF_VALUE_FLOAT(AvgListSorts, StreamLoader);
PF_VALUE_INT(ListSorts, StreamLoader);
PF_VALUE_INT(GetNextCalls, StreamLoader);
PF_VALUE_INT(RealReqRemoves, StreamLoader);
#endif // __STATS

static void StreamLoadSleep() {
#if RSG_WIN32
	_mm_pause();
#elif RSG_SCE
	// Albert: Spinlock on Prospero, waiting 10ms between reads kills load times (due to fast SSD)
	_mm_pause();
#else
	sysIpcYield();
#endif
}

bool AreDependenciesMet(strIndex index)
{
	strIndex deps[256];
	strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);
	strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(index);
	s32 numDeps = pModule->GetDependencies(pModule->GetObjectIndex(index), deps, STREAMING_MAX_DEPENDENCIES);

	for (int i = 0; i < numDeps; i++)
	{
		strStreamingInfo &depInfo = strStreamingEngine::GetInfo().GetStreamingInfoRef(deps[i]);

		if (depInfo.GetStatus() != STRINFO_LOADED)
		{
			return false;
		}
	}

	// The image is also a dependency.
	strLocalIndex imageIndex = strLocalIndex(-1);

	if (!info.IsPackfileHandle())
	{
		imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle());
		if (imageIndex != -1)
		{
			strIndex archiveStrIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);
			return strStreamingEngine::GetInfo().GetStreamingInfoRef(archiveStrIndex).GetStatus() == STRINFO_LOADED;
		}
	}

	return true;
}



#if __ASSERT

void strStreamingLoader::SanityCheck()
{
	strStreamingInfoIterator it;
	while (!it.IsAtEnd())
	{
		strIndex index = *it;
		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(index);

		if (info->IsInImage())
		{
			int requestIndex = FindExistingRequest(index);

			if (info->GetStatus() == STRINFO_LOADING)
			{
				streamAssertf(requestIndex != -1, "%s is loading, but not in request list", strStreamingEngine::GetObjectName(index));
				streamAssertf(m_StreamingQueue[requestIndex]->m_Status == STATUS_LOADING, "%s is loading, but in request list as cancelled", strStreamingEngine::GetObjectName(index));
			}
			else
			{
				streamAssertf(requestIndex == -1 || (requestIndex != -1 && m_StreamingQueue[requestIndex]->m_Status == STATUS_CANCELED), 
					"%s is not loading, but in request list (and not as a canceled request)", strStreamingEngine::GetObjectName(index));
			}
		}

		++it;
	}
}

void strStreamingLoader::DumpLoadingState()
{
	int fileCount = m_StreamingFiles.GetMaxCount();

	streamDisplayf("%s: Files in array: %d", GetLoaderName(), fileCount);

	for (int x=0; x<fileCount; x++)
	{
		StreamingFile &file = m_StreamingFiles[x];

		static const char *status[] = {
			"IDLE",
			"LOADING",
			"LOADED",
			"CANCELED",
			"ERROR",
		};

		const char *name = (file.m_Index.IsValid()) ? strStreamingEngine::GetObjectName(file.m_Index) : "INVALID";
		streamDisplayf("%d: REQUESTED FILE: %s, status=%s", x, name, status[file.m_Status]);

		if (file.m_Index.IsValid())
		{
			strStreamingEngine::GetInfo().PrintRecursiveLoadState(file.m_Index, 1);
		}
	}
}

#define SANITY_CHECK()	// SanityCheck()
#else
#define SANITY_CHECK()	
#endif // __ASSERT

#if !RSG_FINAL
	void strStreamingLoader::GetLoaderStats(int &loading, int &max)
	{
		loading = m_Pending;
		max = m_StreamingFiles.GetMaxCount();
	}
#endif

void strStreamingLoader::Init(pgStreamer::Device device)
{
	for (int i = 0; i < m_StreamingFiles.GetMaxCount(); ++i)
	{
		StreamingFile& file = m_StreamingFiles[i];
		memset(&file, 0, sizeof(StreamingFile));
		#if LOAD_SEMA
			file.m_ReadSema = sysIpcCreateSema(false);
		#else
			file.m_Finished = false;
		#endif
		file.m_Status = STATUS_IDLE;
		file.m_ReadId = NULL;
		file.m_Handle = pgStreamer::Error;
		file.m_IsPlaced = (u32) false;
	}
	m_Pending = 0;
	m_PendingIO = 0;
	m_Device = device;

	m_PendingSize = 0;
	m_FramePendingSize = 0;

#if STREAM_STATS
	m_Completed = 0;
	m_Failed = 0;
	m_CompletedSize = 0;
	m_FailedSize = 0;

	m_PlaceTime = 0;
	m_PendingTime = 0;
	m_RollingCompleted = 0.0f;
	m_RollingRedundant = 0.0f;
	m_RedundantCount = 0;
#endif

#if __BANK
	m_LongMtPlacements = 0;
	m_MissedIdleSlots = 0;
#endif // __BANK

#if STREAMING_VISUALIZE
	STREAM_TICKER_ONLY(StrTicker::SetMaxPendingKB(ms_ExtremeStreaming_MaxPendingKb));
	STREAM_TICKER_ONLY(StrTicker::SetMaxConvertMS(ms_ExtremeStreaming_MaxConvertMs));
#endif
}


void strStreamingLoader::Shutdown()
{
#if __ASSERT
	Flush();
#endif

#if LOAD_SEMA
	for (int i = 0; i < m_StreamingFiles.GetMaxCount(); ++i)
	{
		sysIpcDeleteSema(m_StreamingFiles[i].m_ReadSema);
	}
#endif
}

s32 strStreamingLoader::GetLoadingObjectCount()
{
	return m_StreamingQueue.GetCount() + m_AsyncPlacements.GetCount();
}

strIndex strStreamingLoader::GetLoadingObject(s32 index) 
{
	int queueCount = m_StreamingQueue.GetCount();
	if (index >= queueCount)
	{
		return m_AsyncPlacements[index - queueCount]->m_Index;
	}

	return m_StreamingQueue[index]->m_Index;
}

void strStreamingLoader::ProcessAsyncFiles()
{

#if !__FINAL
	sysTimer timer;
	strIndex longPlaceIndex;
	float fTmpStart = 0.0f;
	float fTimeInMainThreadPlacementMs = 0.0f;
	float fTimeInMarkPlacementCompleteMs = 0.0f;
	float fTimeInFinalizePlacementMs = 0.0f;
	float fTimeInRemoveObjectMs = 0.0f;
	float fTimeInRemoveDepCountsAndUnreqMs = 0.0f;
	float fStartPlaceFile = 0.0f;
	float fLongestProcessingTimeMs = 0.0f;
#endif
	strIndex index;

	// PF_START_TIMEBAR("ProcessAsyncFiles");

	// Now handle the async files - are any of them done?
	for (int x=m_AsyncPlacements.GetCount()-1; x>=0; x--)
	{
		StreamingFile &file = *m_AsyncPlacements[x];
	
		NOTFINAL_ONLY( fStartPlaceFile = timer.GetMsTime() );

		if (file.m_IsPlaced)
		{
			bool canceled = (file.m_Status == STATUS_CANCELED);
			index = file.m_Index;

			//PERFORM MAIN THREAD PLACEMENT
			EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("PerformMainThreadPlacement", 1.0f);)
			NOTFINAL_ONLY( fTmpStart = timer.GetMsTime(); )
			PerformMainThreadPlacement(file);
			NOTFINAL_ONLY( fTimeInMainThreadPlacementMs += ( timer.GetMsTime() - fTmpStart); )
			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)

			//MARK PLACEMENT COMPLETE
			EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("MarkPlacementComplete", 1.0f);)
			NOTFINAL_ONLY( fTmpStart = timer.GetMsTime(); )
			MarkPlacementComplete(file);
			NOTFINAL_ONLY( fTimeInMarkPlacementCompleteMs += ( timer.GetMsTime() - fTmpStart); )
			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)

			//FINALIZE PLACEMENT
			EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("FinalizePlacement", 1.0f);)
			NOTFINAL_ONLY( fTmpStart = timer.GetMsTime(); )
			FinalizePlacement(file);
			m_AsyncPlacements.DeleteFast(x);
			NOTFINAL_ONLY( fTimeInFinalizePlacementMs += ( timer.GetMsTime() - fTmpStart); )
			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)

			// We have to kill it if it was meant to be canceled.

			//REMOVE OBJECT
			EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("RemoveObject", 1.0f);)
			NOTFINAL_ONLY( fTmpStart = timer.GetMsTime(); )
			if (canceled && strStreamingEngine::GetInfo().IsObjectReadyToDelete(index, 0))
			{
				if (strStreamingEngine::GetInfo().RemoveObject(index))
				{
					streamDebugf1("Had to delete %s after placing due to cancel request", strStreamingEngine::GetObjectName(index));
				}
			}
			NOTFINAL_ONLY( fTimeInRemoveObjectMs += ( timer.GetMsTime() - fTmpStart); )
			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)

			//REMOVE DEP COUNTS AND UNREQUEST
			EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("RemoveDependentCountsAndUnrequest", 1.0f);)
			NOTFINAL_ONLY( fTmpStart = timer.GetMsTime(); )
			if (canceled)
			{
				strStreamingEngine::GetInfo().RemoveDependentCountsAndUnrequest(index);
			}
			NOTFINAL_ONLY( fTimeInRemoveDepCountsAndUnreqMs += ( timer.GetMsTime() - fTmpStart); )
			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)
		}
		else
		{
#if __BANK
			m_LongMtPlacements++;
#endif // __BANK
		}

#if !__FINAL
		//////////////////////////////////////////////////////////////////////////
		if ((timer.GetMsTime() - fStartPlaceFile) > fLongestProcessingTimeMs)
		{
			fLongestProcessingTimeMs = (timer.GetMsTime() - fStartPlaceFile);
			longPlaceIndex = index;
		}
		//////////////////////////////////////////////////////////////////////////
#endif

	}

	// PF_START_TIMEBAR("End of placement");

#if !__FINAL
	//////////////////////////////////////////////////////////////////////////
	if (timer.GetMsTime() >= (RSG_PC ? 32.0f : 2000.0f))
	{
		Displayf( "********************************");
		Displayf( "STALL: Async resource placement took %4.2f ms", timer.GetMsTime() );
		Displayf( "... total time in PerformMainThreadPlacement = %4.2f ms",		fTimeInMainThreadPlacementMs );
		Displayf( "... total time in MarkPlacementComplete = %4.2f ms",				fTimeInMarkPlacementCompleteMs );
		Displayf( "... total time in FinalizePlacement = %4.2f ms",					fTimeInFinalizePlacementMs );
		Displayf( "... total time in RemoveObject = %4.2f ms",						fTimeInRemoveObjectMs );
		Displayf( "... total time in RemoveDependentCountAndUnrequest = %4.2f ms",	fTimeInRemoveDepCountsAndUnreqMs );
		if (longPlaceIndex.IsValid())
		{
			Displayf( "... longest file to place: %s took %4.2f ms", strStreamingEngine::GetInfo().GetObjectName(longPlaceIndex), fLongestProcessingTimeMs );
		}
		Displayf( "********************************" );
	}
	//////////////////////////////////////////////////////////////////////////
#endif

}

void strStreamingLoader::LoadRequestedObjects()
{	
#if __ASSERT
	static bool s_ReentranceCheck = false;

	streamAssertf(s_ReentranceCheck == false, "LoadAllRequestedObjects is being called while a blocking load is already in progress. Check the callstack.");
	s_ReentranceCheck = true;
#endif // __ASSERT

	ProcessStreamFiles();

#if STREAMING_VISUALIZE && __BANK
	bool addMarkers = *strStreamingEngine::GetLoader().GetLoaderSVMarkersPtr();
#endif // STREAMING_VISUALIZE && __BANK

	// Early-out if we're still busy.
	if (m_Pending >= ms_MaxPendingRequestThreshold)
	{
#if __ASSERT
		s_ReentranceCheck = false;
#endif // __ASSERT
#if STREAMING_VISUALIZE && __BANK
		if (addMarkers && strStreamingVisualize::IsInstantiated())
		{
			char strmInfo[256];
			formatf(strmInfo, "D%d: Early-out, Pending=%d", (int) m_Device, m_Pending);
			STRVIS_SET_MARKER_TEXT(strmInfo);
   		}
#endif // STREAMING_VISUALIZE && __BANK
		
		return;
	}

	if (g_AREA51)
	{
		ms_MaxChainedRequests = 1;
		*strStreamingEngine::GetInfo().GetSortOnHDDPtr() = true;
	}
	else
	{
		ms_MaxChainedRequests = 24;
		*strStreamingEngine::GetInfo().GetSortOnHDDPtr() = false;
	}

	sysTimer t;
	
	//	if (pgStreamer::IsLocked()) // is the streamer locked by audio? high level check
	//		return;

	EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("Request", 2.0f);)

	strIndex indexArray[512];
	streamAssert(ms_MaxChainedRequests <= NELEM(indexArray));
	
	u32 headposition = GetHeadPosition();
	u32 deviceMask = 1 << m_Device;
	u32 seekTolerance = ms_LsnTolerance[m_Device];

	int maxPendingRequests = (ms_ExtremeStreaming_Enabled) ? ms_MaxPendingRequestThreshold : 1;
	g_AREA51_FRAME++;

#if STREAMING_VISUALIZE && __BANK
	int prePending = m_Pending;
	int prePendingIO = m_PendingIO;
	int prePendingSize = m_PendingSize;
	int preRequests = 0;
#endif // STREAMING_VISUALIZE && __BANK

	sysTimer extremeTime;

	int numNextContiguousRequests = 0;

	while (true)
	{
		strIndex *indexArrayPtr = indexArray;

		numNextContiguousRequests = strStreamingEngine::GetInfo().GetNextRequestsOnCd(headposition, true, indexArray, ms_MaxChainedRequests, deviceMask, seekTolerance);

#if STREAMING_VISUALIZE && __BANK
		preRequests += numNextContiguousRequests;
#endif // STREAMING_VISUALIZE && __BANK


		if (g_AREA51 && !numNextContiguousRequests)
		{
			break;
		}

		bool burstMode = strStreamingEngine::GetBurstMode();
		u32 maxPendingKb = (burstMode) ? 0x7fffffff : (u32) ms_ExtremeStreaming_MaxPendingKb * 1024;
		const float MaxTime = (burstMode) ? 20.0f : (float)ms_ExtremeStreaming_MaxConvertMs;
		u32 pendingSize = m_PendingSize;

		// Let's assume that some of the data already has been streamed.
		pendingSize = (u32) Max((s32) pendingSize - 1024 * 1024, 0);

		if (g_AREA51)
		{
			pendingSize = (u32) Max((s32) pendingSize - 1024 * 1024, 0);
		}

		while (numNextContiguousRequests && m_Pending < maxPendingRequests && pendingSize < maxPendingKb && extremeTime.GetMsTime() < MaxTime)
		{
			u32 prevPendingSize = m_PendingSize;			
			int processed = RequestStreamFiles(indexArrayPtr, numNextContiguousRequests);
			pendingSize += m_PendingSize - prevPendingSize;

			if (!processed)
			{
				break;
			}

			numNextContiguousRequests -= processed;
			indexArrayPtr += processed;
		}

		if (!g_AREA51)
		{
			break;
		}

#if STREAMING_VISUALIZE
		if(pendingSize >= maxPendingKb)
		{
			if(m_Device == pgStreamer::HARDDRIVE)
				STREAM_TICKER_ONLY(StrTicker::AddMarker(StrTicker::HDD_KB_LIMIT));
			if(m_Device == pgStreamer::OPTICAL)
				STREAM_TICKER_ONLY(StrTicker::AddMarker(StrTicker::ODD_KB_LIMIT));			
		}
		if(extremeTime.GetMsTime() >= MaxTime)
		{
			if(m_Device == pgStreamer::HARDDRIVE)
				STREAM_TICKER_ONLY(StrTicker::AddMarker(StrTicker::HDD_MS_LIMIT));
			if(m_Device == pgStreamer::OPTICAL)
				STREAM_TICKER_ONLY(StrTicker::AddMarker(StrTicker::ODD_MS_LIMIT));
		}
		if(m_Pending >= maxPendingRequests)
		{
			if(m_Device == pgStreamer::HARDDRIVE)
				STREAM_TICKER_ONLY(StrTicker::AddMarker(StrTicker::HDD_PENDING_LIMIT));
			if(m_Device == pgStreamer::OPTICAL)
				STREAM_TICKER_ONLY(StrTicker::AddMarker(StrTicker::ODD_PENDING_LIMIT));
		}		
#endif
		if (m_Pending >= maxPendingRequests || pendingSize >= maxPendingKb || extremeTime.GetMsTime() >= MaxTime)
		{
			break;
		}

	}

#if STREAMING_VISUALIZE && __BANK
	if (addMarkers && strStreamingVisualize::IsInstantiated())
	{
		char strmInfo[256];
		float extremeTimerMs = extremeTime.GetMsTime();
		formatf(strmInfo, "D%d: Pending=%d->%d (IO %d->%d), Requests=%d->%d, PendingSize=%d->%d, Time=%.2fms %s", (int) m_Device, prePending, m_Pending, prePendingIO, m_PendingIO, preRequests, numNextContiguousRequests, prePendingSize, m_PendingSize, extremeTimerMs, (strStreamingEngine::GetBurstMode()) ? "(BURST)" : "");
		STRVIS_SET_MARKER_TEXT(strmInfo);
	}
#endif // STREAMING_VISUALIZE && __BANK

	m_fProcessTime = t.GetMsTime();
	EXTRA_TIMEBARS(PF_POP_TIMEBAR();)

#if __ASSERT
	s_ReentranceCheck = false;
#endif // __ASSERT
}

//
// name:		strStreamingLoader::StartLoadAllRequestedObjects
// description:	Start LoadAllRequestedObjects process
void strStreamingLoader::StartLoadAllRequestedObjects()
{
	Flush();		
}

//
// name:		strStreamingLoader::UpdateLoadAllRequestedObjects
// description:	Update a LoadAllRequestedObjects process. Return true when finished
bool strStreamingLoaderManager::UpdateLoadAllRequestedObjects()
{
	sysTimer timer;
	while (strStreamingEngine::GetInfo().GetNumberObjectsRequested() > 0)
	{
		if(timer.GetMsTime() > m_callbackInterval)
		{
			// call the specified callback every specified time interval
			if(m_callback)
			{
				(*m_callback)();
			}
			return false;
		}
		LoadRequestedObjects();
		ProcessAsyncFiles();
		
		StreamLoadSleep();
	}
	Flush();
	streamAssertf(strStreamingEngine::GetInfo().GetNumberObjectsRequested() == 0, "UpdateLoadAllRequestedObjects failed to load all");

	return true;
}

static inline bool HaveAllRequestsLoaded(bool bPriorityRequestsOnly)
{
	return (!bPriorityRequestsOnly && strStreamingEngine::GetInfo().GetNumberObjectsRequested() == 0) || 
		(bPriorityRequestsOnly && strStreamingEngine::GetInfo().GetNumberPriorityObjectsRequested() == 0);
}

#if __BANK
void strStreamingLoaderManager::DumpUnsatifiedRequests(bool bPriorityRequestsOnly, bool earlyExit)
{
	if (!HaveAllRequestsLoaded(bPriorityRequestsOnly))
	{
		strIndex missingFile;

		RequestInfoList *requests = strStreamingEngine::GetInfo().GetRequestInfoList();

		streamWarningf("Unsatisfied requests: (%s)", bPriorityRequestsOnly ? "Loading Priority Only" : "Loading All Files");

		RequestInfoList::Iterator it(requests->GetFirst());

		while (it.IsValid())
		{
			strIndex index = it.Item()->m_Index;
			strStreamingInfo& info = strStreamingEngine::GetInfo().GetStreamingInfoRef(index);

			if (!bPriorityRequestsOnly || info.IsFlagSet(STRFLAG_PRIORITY_LOAD))
			{
				strStreamingEngine::GetInfo().PrintInformation(index);
			}

			it.Next();
		}

		if (earlyExit)
		{
			switch(GetEmergencyState())
			{
			case EMERGENCY_NONE:
				streamErrorf("Exiting from LoadAllRequested - But strangely the last allocation succeeded");
				break;
			case EMERGENCY_OUT_OF_MEMORY:
				streamErrorf("Exiting from LoadAllRequested - Out of memory");
				break;
			case EMERGENCY_FRAGMENTED_MEMORY:
				streamErrorf("Exiting from LoadAllRequested - Fragmented memory");
				break;
			}
		}
	}
}

void strStreamingLoaderManager::ResetLoadAllBailTimeout()
{
	s_LoadAllTimer.Reset();
}
#endif // __BANK

void strStreamingLoaderManager::LoadAllRequestedObjects(bool bPriorityRequestsOnly)
{
	AUTO_HANG_DETECT_CRASH_ZONE;

#if SANITY_CHECK_REQUESTS
	strStreamingEngine::GetInfo().SanityCheckRequests();
#endif

#if __BANK
	if (strStreamingLoader::ms_IsPumpAndDump)
	{
		return;
	}
#endif // __BANK
	 
#if !__FINAL
	sysTimer debugTimer;
	s32 numKeepAliveCallBackCalls = 0;
#endif

	static sysTimer callbackTimer;

	BANK_ONLY(s_LoadAllTimer.Reset());
	BANK_ONLY(bool earlyExit = false);

	m_LoadingAllRequested = true;

	//Assertf(!gRenderThreadInterface.IsUsingDefaultRenderFunction(), "Not using a custom render thread function during a blocking load - possible TRC/TCR violation.");

	// Turn off the deferred placement and move all the deferred stuff 
	// onto the main placement list to make sure that it gets picked up 
	DeferAsyncPlacementRequests(false);
	SubmitDeferredAsyncPlacementRequests();

	Flush();		
	while (!HaveAllRequestsLoaded(bPriorityRequestsOnly))
	{
		HANG_DETECT_TICK();

#if SANITY_CHECK_REQUESTS
	strStreamingEngine::GetInfo().SanityCheckRequests();
#endif
		LoadRequestedObjects();
		ProcessAsyncFiles();

		// call the specified callback every specified time interval
		if ( m_callback && callbackTimer.GetMsTime()>=m_callbackInterval )
		{
			USE_MEMBUCKET(MEMBUCKET_DEFAULT);
			callbackTimer.Reset();
			(*m_callback)();

#if !__FINAL
			if (callbackTimer.GetMsTime() >= sysTimer::GetStallThreshold())
			{
				Displayf("POSSIBLE STALL: A call to LoadAllRequestedObjects callback function took %4.2f ms", debugTimer.GetMsTime() );
			}
#endif

			callbackTimer.Reset();

#if !__FINAL
			numKeepAliveCallBackCalls++;
#endif
		}

#if USE_DEFRAGMENTATION
		if (g_strStreamingInterface->WasLastAllocationFragmented())
		{
			// If we're fragmented, we need to do an emergency clean-up.
			if (gRenderThreadInterface.IsUsingDefaultRenderFunction())
			{
				streamErrorf("Memory fragmented during LoadAllRequestedObjects - can't do emergency defrag, we're not using a custom render function (which is bad in its own way)");
			}
			else
			{
				streamErrorf("Memory fragmented during LoadAllRequestedObjects - performing emergency defrag");
				gRenderThreadInterface.GetDrawThreadParams()->LockRenderMutex();
				strDefragmentation::eDefragMode oldMode = strStreamingEngine::GetDefragmentation()->GetDefragMode();
				strStreamingEngine::GetDefragmentation()->SetDefragMode(strDefragmentation::DEFRAG_PAUSE);
				strStreamingEngine::GetDefragmentation()->Update();
				strStreamingEngine::GetDefragmentation()->SetDefragMode(oldMode);
				gRenderThreadInterface.GetDrawThreadParams()->UnlockRenderMutex();
			}
		}
#endif // USE_DEFRAGMENTATION

#if __BANK
		#define BAIL_TIME 60
		if (s_LoadAllTimer.GetMsTime() > BAIL_TIME * 1000 && 
			((!bPriorityRequestsOnly && strStreamingEngine::GetInfo().GetNumberObjectsRequested() > 0) ||
			(bPriorityRequestsOnly && strStreamingEngine::GetInfo().GetNumberPriorityObjectsRequested() > 0))) 
		{
			static bool dumped;

			if (!dumped)
			{
				Errorf("Trying to load the all files takes over %.2f seconds to far - are we out of memory?", s_LoadAllTimer.GetTime());
				dumped = true;

				strStreamingEngine::GetInfo().PrintRequestList();
				Displayf("=== Loaded Objects ====");
				strStreamingEngine::GetInfo().PrintLoadedList(false, false);
				g_strStreamingInterface->DisplayModuleMemory(true);
				gDCBuffer->DumpDrawListPages();
				Displayf("Used by external alloc: %" SIZETFMT "dK virt, %" SIZETFMT "dK phys",
					strStreamingEngine::GetAllocator().GetExternalVirtualMemoryUsed() / 1024, strStreamingEngine::GetAllocator().GetExternalPhysicalMemoryUsed() / 1024 );
			}

			streamWarningf("strStreamingLoader::LoadAllRequested took longer than %d seconds - giving up", BAIL_TIME);
			earlyExit = true;
			break;
		}
#endif // __BANK 

		StreamLoadSleep();
	}

	Flush();

#if __BANK
	static bool onlyOnce = true;
	if (onlyOnce && !HaveAllRequestsLoaded(bPriorityRequestsOnly))
	{
		streamErrorf("Unable to load all requested objects - doing one more attempt, this time with full logging.");
		u8 oldTtyLevel = Channel_streaming.TtyLevel;
		u8 oldFileLevel = Channel_streaming.FileLevel;
		Channel_streaming.TtyLevel = DIAG_SEVERITY_DEBUG3;
		LoadRequestedObjects();
		Flush();
		Channel_streaming.TtyLevel = oldTtyLevel;
		Channel_streaming.FileLevel = oldFileLevel;

		onlyOnce = false;
		DumpUnsatifiedRequests(bPriorityRequestsOnly, earlyExit);

		streamWarningf("Unable to load all requested objects - see TTY for details. Crucial assets might be missing, the game might crash now.");
	}
#endif // __BANK
#if SANITY_CHECK_REQUESTS
	strStreamingEngine::GetInfo().SanityCheckRequests();
#endif

	m_LoadingAllRequested = false;

#if !__FINAL
	if (debugTimer.GetMsTime() >= sysTimer::GetStallThreshold())
	{
		Displayf("POSSIBLE STALL: LoadAllRequestedObjects took %4.2f ms and called keep-alive CB %d times", debugTimer.GetMsTime(), numKeepAliveCallBackCalls );
	}
#endif
}

//This function is a copy of LoadAllRequestedObjectsWith, the only difference being the timeout value passed through.
void strStreamingLoaderManager::LoadAllRequestedObjectsWithTimeout(bool bPriorityRequestsOnly, u32 timeoutMS)
{
	AUTO_HANG_DETECT_CRASH_ZONE;

#if SANITY_CHECK_REQUESTS
	strStreamingEngine::GetInfo().SanityCheckRequests();
#endif

#if __BANK
	if (strStreamingLoader::ms_IsPumpAndDump)
	{
		return;
	}
#endif // __BANK
	 
#if !__FINAL
	sysTimer debugTimer;
	s32 numKeepAliveCallBackCalls = 0;
#endif

	static sysTimer callbackTimer;

	BANK_ONLY(s_LoadAllTimer.Reset());
	BANK_ONLY(bool earlyExit = false);

	m_LoadingAllRequested = true;

	//Assertf(!gRenderThreadInterface.IsUsingDefaultRenderFunction(), "Not using a custom render thread function during a blocking load - possible TRC/TCR violation.");

	// Turn off the deferred placement and move all the deferred stuff 
	// onto the main placement list to make sure that it gets picked up 
	DeferAsyncPlacementRequests(false);
	SubmitDeferredAsyncPlacementRequests();

	Flush();		
	static sysTimer timeoutTimer;
	timeoutTimer.Reset();
	m_outOfMemory = false;

	while (!HaveAllRequestsLoaded(bPriorityRequestsOnly))
	{
		HANG_DETECT_TICK();

#if SANITY_CHECK_REQUESTS
	strStreamingEngine::GetInfo().SanityCheckRequests();
#endif
		LoadRequestedObjects();
		ProcessAsyncFiles();


		/*
		This is here in case we run out of streaming memory and the cleaning code 
		can't find any space for what we want. It's not a great fix but we need it temporarily
		*/
		if(timeoutMS)
		{
			if ( timeoutTimer.GetMsTime()>=timeoutMS || m_outOfMemory)
			{
				streamDebugf1("We have been blocking for too long  (or are out of memory) in a replay Load All Requested.");
				break;
			}
		}

		// call the specified callback every specified time interval
		if ( m_callback && callbackTimer.GetMsTime()>=m_callbackInterval )
		{
			callbackTimer.Reset();
			(*m_callback)();

#if !__FINAL
			if (callbackTimer.GetMsTime() >= sysTimer::GetStallThreshold())
			{
				Displayf("POSSIBLE STALL: A call to LoadAllRequestedObjects callback function took %4.2f ms", debugTimer.GetMsTime() );
			}
#endif

			callbackTimer.Reset();

#if !__FINAL
			numKeepAliveCallBackCalls++;
#endif
		}

#if USE_DEFRAGMENTATION
		if (g_strStreamingInterface->WasLastAllocationFragmented())
		{
			// If we're fragmented, we need to do an emergency clean-up.
			if (gRenderThreadInterface.IsUsingDefaultRenderFunction())
			{
				streamErrorf("Memory fragmented during LoadAllRequestedObjects - can't do emergency defrag, we're not using a custom render function (which is bad in its own way)");
			}
			else
			{
				streamErrorf("Memory fragmented during LoadAllRequestedObjects - performing emergency defrag");
				gRenderThreadInterface.GetDrawThreadParams()->LockRenderMutex();
				strDefragmentation::eDefragMode oldMode = strStreamingEngine::GetDefragmentation()->GetDefragMode();
				strStreamingEngine::GetDefragmentation()->SetDefragMode(strDefragmentation::DEFRAG_PAUSE);
				strStreamingEngine::GetDefragmentation()->Update();
				strStreamingEngine::GetDefragmentation()->SetDefragMode(oldMode);
				gRenderThreadInterface.GetDrawThreadParams()->UnlockRenderMutex();
			}
		}
#endif // USE_DEFRAGMENTATION

#if __BANK
		#define BAIL_TIME 60
		if (s_LoadAllTimer.GetMsTime() > BAIL_TIME * 1000 && 
			((!bPriorityRequestsOnly && strStreamingEngine::GetInfo().GetNumberObjectsRequested() > 0) ||
			(bPriorityRequestsOnly && strStreamingEngine::GetInfo().GetNumberPriorityObjectsRequested() > 0))) 
		{
			static bool dumped;

			if (!dumped)
			{
				Errorf("Trying to load the all files takes over %.2f seconds to far - are we out of memory?", s_LoadAllTimer.GetTime());
				dumped = true;

				strStreamingEngine::GetInfo().PrintRequestList();
				Displayf("=== Loaded Objects ====");
				strStreamingEngine::GetInfo().PrintLoadedList(false, false);
				g_strStreamingInterface->DisplayModuleMemory(true);
				gDCBuffer->DumpDrawListPages();
				Displayf("Used by external alloc: %" SIZETFMT "dK virt, %" SIZETFMT "dK phys",
					strStreamingEngine::GetAllocator().GetExternalVirtualMemoryUsed() / 1024, strStreamingEngine::GetAllocator().GetExternalPhysicalMemoryUsed() / 1024 );
			}

			streamWarningf("strStreamingLoader::LoadAllRequested took longer than %d seconds - giving up", BAIL_TIME);
			earlyExit = true;
			break;
		}
#endif // __BANK 

		StreamLoadSleep();
	}

	Flush();

#if __BANK
	static bool onlyOnce = true;
	if (onlyOnce && !HaveAllRequestsLoaded(bPriorityRequestsOnly))
	{
		streamErrorf("Unable to load all requested objects - doing one more attempt, this time with full logging.");
		u8 oldTtyLevel = Channel_streaming.TtyLevel;
		u8 oldFileLevel = Channel_streaming.FileLevel;
		Channel_streaming.TtyLevel = DIAG_SEVERITY_DEBUG3;
		LoadRequestedObjects();
		Flush();
		Channel_streaming.TtyLevel = oldTtyLevel;
		Channel_streaming.FileLevel = oldFileLevel;

		onlyOnce = false;
		DumpUnsatifiedRequests(bPriorityRequestsOnly, earlyExit);

		streamWarningf("Unable to load all requested objects - see TTY for details. Crucial assets might be missing, the game might crash now.");
	}
#endif // __BANK
#if SANITY_CHECK_REQUESTS
	strStreamingEngine::GetInfo().SanityCheckRequests();
#endif

	m_LoadingAllRequested = false;

#if !__FINAL
	if (debugTimer.GetMsTime() >= sysTimer::GetStallThreshold())
	{
		Displayf("POSSIBLE STALL: LoadAllRequestedObjects took %4.2f ms and called keep-alive CB %d times", debugTimer.GetMsTime(), numKeepAliveCallBackCalls );
	}
#endif
}


bool strStreamingLoader::CancelRequest(strIndex index)
{
	Assert(sysThreadType::IsUpdateThread());

	strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(index);
	streamAssertf(info->GetStatus() == STRINFO_LOADING, "%s (%d) not loading - but being cancelled nonetheless", strStreamingEngine::GetObjectName(index), info->GetStatus());

	if (info->GetFlags() & STRFLAG_INTERNAL_PLACING)
	{
		streamDebugf1("Canceling %s, but it's already placing", strStreamingEngine::GetObjectName(index));
		// We're too late - we're already placing it. Let's just tell the system to delete it once we're done.
		for (int x=0; x<m_StreamingFiles.GetMaxCount(); x++)
		{
			StreamingFile &file = m_StreamingFiles[x];

			if ((file.m_Status == STATUS_LOADING || file.m_Status == STATUS_CANCELED) && file.m_Index == index)
			{
				if (file.m_Status != STATUS_CANCELED)
				{
					file.m_Status = STATUS_CANCELED;

					// That also needs we need to keep the dependencies in memory - the mt placement thread is
					// actively using them.
					strStreamingEngine::GetInfo().AddDependentCounts(file.m_Index);
				}

				return true;
			}
		}

		return false;
	}

	bool bFound = false;
	for (int i = 0; i < m_StreamingQueue.GetCount(); ++i)
	{
		StreamingFile& file = *m_StreamingQueue[i];
		if (file.m_Index == index)
		{
			streamDebugf1("Canceling %s", strStreamingEngine::GetObjectName(file.m_Index));
#if __ASSERT
			if (bFound)
				streamAssertf(false, "Cancelling multiple copies of '%s' - the streaming system is probably being abused by many calls to request and remove the same object", 
					strStreamingEngine::GetObjectName(index));
#endif

#if __ASSERT
			TASKLOG("CancelRequest on index %d",index.Get());
#endif
			// If the sema can be polled, it means that we have loaded the entire file into memory already.
			#if LOAD_SEMA
				if (sysIpcPollSema(file.m_ReadSema))
			#else
				if( file.m_Finished )
			#endif
			{
				// We can simply free up the memory and remove any trace of this request.
				{
					MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(file.m_Index));
					strStreamingEngine::GetAllocator().StreamingFree(file.m_AllocationMap WIN32PC_ONLY(, true));
				}

				strStreamingInfo *pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);

				u32 fileSize = pInfo->ComputeVirtualSize(file.m_Index, false, true) + pInfo->ComputePhysicalSize(file.m_Index);
				streamAssert(m_PendingSize >= fileSize);
				m_PendingSize -= fileSize;
				m_StreamingQueue.Delete(i--);
				m_Pending--;
				m_PendingIO--;
				streamAssert(m_Pending >= 0);

				// Remove the dependency count on the archive, we don't need it anymore.
				s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();
				if (imageIndex != -1)
				{
					strPackfileManager::RemoveStreamableReference(imageIndex);
				}

				streamAssert(FindExistingAsyncRequest(index) == -1);

				file.m_ReadId = NULL;
				file.m_Handle = pgStreamer::Error;
				file.m_LoadTime = 0;
				file.m_Index = strIndex(strIndex::INVALID_INDEX);
				file.m_IsPlaced = (u32) false;
				#if !LOAD_SEMA
					file.m_Finished = false;
				#endif
				//Moved to be the last thing set, it's the flag that states that this StreamingFile is done with
				//This is all meant to be on a single thread, but I have the paranoias
				file.m_Status = STATUS_IDLE;

				bFound = true;
#if __FINAL || !__ASSERT
				break;
#endif
			}
			else
			{
				// The sema hasn't been signaled yet - that means we're still loading the file. We need to wait for the
				// file to load, THEN we can toss it out.
				file.m_Status = STATUS_CANCELED;

				// That also needs we need to keep the dependencies in memory - we could still revive the request.
				strStreamingEngine::GetInfo().AddDependentCounts(file.m_Index);

				bFound = true;
#if __FINAL || !__ASSERT
				break;
#endif
			}
		}
	}

	return bFound;
}


bool strStreamingLoader::Flush()
{
	Assert(sysThreadType::IsUpdateThread());

	int waited = 0;
	bool hasMoreWork = false;

	int count = m_StreamingQueue.GetCount();
	while (count-- && !m_StreamingQueue.IsEmpty())
	{
		#if LOAD_SEMA
			if (sysIpcPollSema(m_StreamingQueue.Top()->m_ReadSema))
		#else
			if( m_StreamingQueue.Top()->m_Finished )
		#endif
		{
			if (g_AREA51)
			{
				StreamingFile &file = *m_StreamingQueue.Pop();

				if (file.m_Status == STATUS_CANCELED || AreDependenciesMet(file.m_Index))
				{
					streamVerifyf(ProcessFile(file), "Process File was unsuccessful");
				}
				else
				{
					if (!file.m_Reissue)
					{
						file.m_Reissue = true;

						strStreamingInfo *info = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
						u32 fileSize = info->ComputeVirtualSize(file.m_Index, false, true) + info->ComputePhysicalSize(file.m_Index);
						streamAssert(m_PendingSize >= fileSize);
						streamAssert(m_PendingIO > 0);
						m_PendingSize -= fileSize;
						m_PendingIO--;
					}

					// Punt it to the back of the queue.
					m_StreamingQueue.Push(&file);

					// Signal the semaphore, we're not bound by streaming, just dependencies.
					#if LOAD_SEMA
						sysIpcSignalSema(file.m_ReadSema);
					#endif
					hasMoreWork = true;
				}
			}
			else
			{
				streamVerifyf(ProcessFile(*m_StreamingQueue.Pop()), "Process File was unsuccessful");
			}
		}
		else
		{
			hasMoreWork = true;
			
#if __ASSERT
			Status status = m_StreamingQueue.Top()->m_Status;
			streamAssertf(status == STATUS_LOADING || status == STATUS_CANCELED, "Incorrect status");
#endif
			if (!g_AREA51)
			{
				StreamLoadSleep();
				if ((waited += 10) == 1000)
					streamWarningf("strStreamingLoader::Flush - waiting for pending operations");
			}
		}
	}

	// We also need to wait for the async placements to finish.
	// The order doesn't matter, we have to wait for all of them anyway, so we may as well focus
	// on the first element.
	waited = 0;

	while (m_AsyncPlacements.GetCount() != 0)
	{
		if (m_AsyncPlacements[0]->m_IsPlaced)
		{
			StreamingFile &file = *m_AsyncPlacements[0];
			bool canceled = (file.m_Status == STATUS_CANCELED);
			strIndex index = file.m_Index;

			// Done. Remove it.
			PerformMainThreadPlacement(file);
			MarkPlacementComplete(file);
			FinalizePlacement(file);
			m_AsyncPlacements.DeleteFast(0);

			// We have to kill it if it was meant to be canceled.
			if (canceled && strStreamingEngine::GetInfo().IsObjectReadyToDelete(index, 0))
			{
				if (strStreamingEngine::GetInfo().RemoveObject(index))
				{
					streamDebugf1("Had to delete %s after placing due to cancel request", strStreamingEngine::GetObjectName(index));
				}
			}

			if (canceled)
			{
				strStreamingEngine::GetInfo().RemoveDependentCountsAndUnrequest(index);
			}
		}
		else
		{
			hasMoreWork = true;

			if (!g_AREA51)
			{
				StreamLoadSleep();
				if ((waited += 10) == 1000)
				{
					streamWarningf("strStreamingLoader::Flush - waiting for async placement operations");
				}
			}				
			else
			{
				break;
			}
		}
	}

	return hasMoreWork;
}

void strStreamingLoader::Update()
{
#if __BANK
	if (ms_IsSirCancelALot)
	{
		if ((TIME.GetFrameCount() % ms_SirCancelALotFrequency) == 0)
		{
			// Find a random victim to cancel.

			int candidates[LOAD_SLOTS_PER_DEVICE];
			int candidateCount = 0;

			for (int x=0; x<m_StreamingFiles.GetMaxCount(); x++)
			{
				if (m_StreamingFiles[x].m_Status == STATUS_LOADING)
				{
					// Ignore those that need to stream in - we don't want to seriously disrupt the streaming process,
					// just fuck with it a little bit for good fun.
					strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(m_StreamingFiles[x].m_Index);

					if (!(info.GetFlags() & STR_FORCELOAD_MASK))
					{
						// Here's one.
						candidates[candidateCount++] = x;
					}
				}
			}

			if (candidateCount)
			{
				// Pick one.
				strIndex index = m_StreamingFiles[candidates[rand() % candidateCount]].m_Index;
				streamDisplayf("Sir Cancel-A-Lot: Canceling %s", strStreamingEngine::GetObjectName(index));

				strStreamingEngine::GetInfo().RemoveObject(index);
			}
		}
	}
#endif // __BANK

	m_FramePendingSize = 0;

#if STREAM_STATS
	// reset frame values
	m_RollingCompleted = m_RollingCompleted * 0.95f + (float) m_CompletedSize * 0.05f;
	m_RollingRedundant = m_RollingRedundant * 0.95f + (float) m_RedundantSize * 0.05f;

	m_Completed = 0;
	m_Failed = 0;

	m_CompletedSize = 0;
	m_RedundantSize = 0;
	m_FailedSize = 0;

	m_PendingTime = 0;
	m_PlaceTime = 0;
#endif
}


bool strStreamingLoader::ProcessStreamFiles()
{
	Assert(sysThreadType::IsUpdateThread());

	sysTimer t;
	int count = m_StreamingQueue.GetCount();

#if LOAD_SEMA
	while( count-- && !m_StreamingQueue.IsEmpty() && sysIpcPollSema( m_StreamingQueue.Top()->m_ReadSema ) )
#else
	while (count-- && !m_StreamingQueue.IsEmpty() && m_StreamingQueue.Top()->m_Finished)
#endif
	{
		if (g_AREA51)
		{
			StreamingFile &file = *m_StreamingQueue.Pop();

			if (file.m_Status == STATUS_CANCELED || AreDependenciesMet(file.m_Index))
			{
				streamVerifyf(ProcessFile(file), "Process File was unsuccessful");
			}
			else
			{
				// Punt it to the back of the queue.
				m_StreamingQueue.Push(&file);

				// Signal the semaphore, we're not bound by streaming, just dependencies.
				#if LOAD_SEMA
					sysIpcSignalSema(file.m_ReadSema);
				#endif
			}
		}
		else
		{
			streamVerify(ProcessFile(*m_StreamingQueue.Pop()));
		}

#if STREAM_STATS
		if(!m_StreamingQueue.IsEmpty())
		{
			m_StreamingQueue.Top()->m_LoadTime = sysTimer::GetSystemMsTime();
		}
#endif

		// don't spend forever converting
		if (t.GetMsTime() > ms_ExtremeStreaming_MaxConvertMs)
			break;
	}
	return true;
}

int strStreamingLoader::FindExistingRequest(strIndex index) const
{
	for (int i = 0; i < m_StreamingQueue.GetCount(); ++i)
	{
		const StreamingFile& file = *m_StreamingQueue[i];
		streamAssertf(file.m_Status != STATUS_IDLE, "File status is idle");
		if (file.m_Index == index)
		{
			return i;
		}
	}
	return -1;
}

int strStreamingLoader::FindExistingAsyncRequest(strIndex index) const
{
	for (int i = 0; i < m_AsyncPlacements.GetCount(); ++i)
	{
		const StreamingFile* file = m_AsyncPlacements[i];

		if (file)
		{
			streamAssertf(file->m_Status != STATUS_IDLE, "File status %p is idle", file);
			if (file->m_Index == index)
			{
				return i;
			}
		}
	}
	return -1;
}

bool strStreamingLoader::AddNewRequest(strIndex index)
{
	Assert(sysThreadType::IsUpdateThread());

	for (int i = 0; i < m_StreamingFiles.GetMaxCount(); ++i)
	{
		StreamingFile& file = m_StreamingFiles[i];

		if (file.m_Status == STATUS_IDLE)
		{
#if __ASSERT
			for (int j = 0; j != m_StreamingQueue.GetCount();++j)
			{
				streamAssertf(m_StreamingQueue[j] != &file, "File %d is already in the streaming queue", file.m_Index.Get());
			}
#endif

			streamAssertf(file.m_LoadTime == 0, "Load time isn't zero");
			file.m_Index = index;
			file.m_IsPlaced = (u32) false;
			return IssueRequest(file);
		}
	}
	streamAssertf(false, "Too many requests - need more streaming slots on framework level.");
	return false;
}

int strStreamingLoader::RequestStreamFiles(strIndex* indexArray, int numNextContiguousRequests)
{
	// issue requests for files, as long as suitable directly adjacent files are found
	if (!numNextContiguousRequests)
	{
		// Nothing to stream.
		return 0;
	}

#if LIVE_STREAMING
	bool bStarted = false;
#endif

	bool issued = false;

	strIndex index;
	int iterationCount = 0;
	int requestsLeft = MAX_LOAD_PER_FRAME;

	while (iterationCount < numNextContiguousRequests)
	{
		index = indexArray[iterationCount++];

		if (m_Pending == m_StreamingFiles.GetMaxCount())
		{
			// We don't have any more free slots in the streaming list.
#if __BANK
			m_MissedIdleSlots++;
#endif // __BANK
			break;
		}

		bool skip = false;

		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(index);
		streamAssertf(!info->IsFlagSet(STRFLAG_INTERNAL_DUMMY), "You can't request dummy entries"); // make sure dummies are never really issued...

		// Requesting something can implicitly request dependencies, so it's possible that something has already been requested.
		if (info->GetStatus() == STRINFO_LOADING || info->GetStatus() == STRINFO_LOADED)
		{
			skip = true;
		}
		else if (info->GetStatus() != STRINFO_LOADREQUESTED)
		{
			streamDebugf3("File %s is in the request list, but not marked as load requested (it is %s). Skipping.",
				strStreamingEngine::GetObjectName(index), strStreamingInfo::GetFriendlyStatusName(info->GetStatus()));

			// This was probably taken off the list because memory was so low. Skip.
			skip = true;
		}
		else if (!info->IsPackfileHandle())
		{
			// Is the image for this object resident?
			strLocalIndex imageIndex = strLocalIndex(strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()));
			if (imageIndex != -1)
			{
				if (!strPackfileManager::IsImageForHandleResident(info->GetHandle()))
				{
					// STOP. We're trying to request something without its backing archive resident.
					// We could either go to the next file and check that... or wait for the archive
					// to become resident.
					// Let's wait.
					
					streamDebugf3("Postponing streaming of %d - image %d not resident yet", index.Get(), imageIndex.Get());
					skip = true;

					// Sanity check - is it requested? If not, something went afoul.
					// Every request needs to auto-request its dependencies, which includes archives.
					ASSERT_ONLY(strStreamingInfo *archiveInfo = strPackfileManager::GetStreamingModule()->GetStreamingInfo(imageIndex));

					streamAssertf(archiveInfo->GetStatus() != STRINFO_NOTLOADED, "Image %d is a dependency for streamable %x, but it hasn't been requested.", imageIndex.Get(), index.Get());
					streamAssertf(archiveInfo->GetStatus() != STRINFO_LOADED, "Image %d is not resident, even though the streaming system thinks it is.", imageIndex.Get());
					// break;
				}
			}
		}

		if (!skip)
		{
			int requestIndex = FindExistingRequest(index);

			// If we already have an existing entry, then that means it had been canceled before but is still in limbo while we're
			// waiting for the data to come in. Let's just revive the entry.
			if (requestIndex != -1)
			{
				streamAssertf(m_StreamingQueue[requestIndex]->m_Status == STATUS_CANCELED, "Queue entry status isn't cancelled");

				strStreamingEngine::GetInfo().SetObjectToLoading(index);
				strStreamingEngine::GetInfo().RemoveDependentCountsAndUnrequest(index);
				m_StreamingQueue[requestIndex]->m_Status = STATUS_LOADING;

				streamDisplayf("Reviving %s", strStreamingEngine::GetObjectName(index));

				// In order for a request to be at this stage, it must have gone from "LOADING" to
				// "NOTLOADED" to "LOADREQUESTED" and now "LOADING" again. Because the
				// streaming operation itself is still going on, we still have a reference to the
				// archive which we never released. And when the streamable went from "NOTLOADED"
				// to "LOADREQUESTED" a second time, it got another reference.
				//
				// TL;DR: We have an extra reference to the packfile. Need to release it, or else
				// we're leaking archives.
				s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();
				if (imageIndex != -1)
				{
					strPackfileManager::RemoveStreamableReference(imageIndex);
				}
			}
			else
			{
	#if LIVE_STREAMING
				// can not add live streaming file to a running list.
				if (bStarted && strStreamingEngine::GetInfo().GetStreamingInfo(index)->IsFlagSet(STRFLAG_INTERNAL_LIVE))
				{
					break;
				}
	#endif // LIVE_STREAMING

				// check to see all the dependencies are also on the way
				strIndex deps[STREAMING_MAX_DEPENDENCIES];
				strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(index);
				strLocalIndex objIndex = pModule->GetObjectIndex(index);
				s32 numDeps = pModule->GetDependencies(objIndex, &deps[0], STREAMING_MAX_DEPENDENCIES);
				bool bDepsLoaded = true;
				while (numDeps--)
				{
					// has dependency loaded or about to load
					if (strStreamingEngine::GetInfo().GetStreamingInfo(deps[numDeps])->GetStatus() != STRINFO_LOADED &&
						strStreamingEngine::GetInfo().GetStreamingInfo(deps[numDeps])->GetStatus() != STRINFO_LOADING)
					{
						bDepsLoaded = false;
						break;
					}
				}
				// if one of the dependencies isn't loaded then break out loop
				if (!bDepsLoaded)
				{
					break;
				}

				// find idle streamingfile slot
				if (!AddNewRequest(index))
				{
#if __BANK
					m_MissedIdleSlots++;
#endif // __BANK
					return (issued) ? iterationCount : 0;
				}

				issued = true;

#if LIVE_STREAMING
				bStarted = true;
#endif

				// Don't queue up more than MAX_LOADS_PER_FRAME.
				if (--requestsLeft == 0)
				{
					break;
				}
			}

	#if LIVE_STREAMING
			// can not continue to add after a live streaming file.
			if (strStreamingEngine::GetInfo().GetStreamingInfo(index)->IsFlagSet(STRFLAG_INTERNAL_LIVE))
			{
				break;
			}
	#endif // LIVE_STREAMING
		}

//		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(index);
//		streamDisplayf("#%d offset: %u (%s)",iterationCount,strStreamingEngine::GetInfo().GetStreamingInfo(index)->GetCdPosn(),pModule->GetName(pModule->GetObjectIndex(index)));
	}

	SANITY_CHECK();

	return (issued) ? iterationCount : 0;
}

u32 strStreamingLoader::GetStreamerFlags() const
{
	return (m_Device == pgStreamer::OPTICAL) ? pgStreamer::UNCACHED : 0;
}

#if RSG_PC && defined(USE_RESOURCE_CACHE) && USE_RESOURCE_CACHE
bool strStreamingLoader::HandleOutOfMemory(size_t WIN32PC_ONLY(virtualSize), size_t WIN32PC_ONLY(physicalSize))
{
#if defined(USE_RESOURCE_CACHE) && USE_RESOURCE_CACHE
	//size_t streamSize = virtualSize + physicalSize;
	s64 iAvailable = grcResourceCache::GetInstance().GetStreamingMemory();
	streamDebugf1("Available Virt %" I64FMT "d Phys %" I64FMT "d VidMem %" I64FMT "d but Requires Virt %" SIZETFMT "u Phys %" SIZETFMT "d", 
		strStreamingEngine::GetAllocator().GetVirtualMemoryAvailable(), 
		strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable(),
		iAvailable, 
		virtualSize,
		physicalSize);

	if (virtualSize > strStreamingEngine::GetAllocator().GetVirtualMemoryAvailable())
	{
		// Out of Virtual Memory to stream in resources
	}
	else if (physicalSize > strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable())
	{
		// Out of Physical Memory to stream in temp resources
		Assertf(0, "Physical memory fragmented - Should not happen - Non-temp stuff must be placed in there");
		Warningf("Available Virt %" I64FMT "d Phys %" I64FMT "d VidMem %" I64FMT "d but Requires Virt %" SIZETFMT "u Phys %" SIZETFMT "d", 
			strStreamingEngine::GetAllocator().GetVirtualMemoryAvailable(), 
			strStreamingEngine::GetAllocator().GetPhysicalMemoryAvailable(),
			iAvailable, 
			virtualSize,
			physicalSize);
	}
	else if (iAvailable > (s64) physicalSize)
	{
		return true;
	}
	else if (!PARAM_disableExtraMem.Get()) // (iAvailable < (s64)physicalSize)
	{
		s64 iIncreaseAmount = (s64)(Min((u32)physicalSize, (u32)(16*1024*1024)) * 2.0f / grcResourceCache::GetInstance().GetPercentageForStreaming());
		streamDebugf1("Increasing Video Memory %" I64FMT "d Requested %" SIZETFMT "u", iIncreaseAmount, physicalSize);
		iIncreaseAmount += grcResourceCache::GetInstance().GetExtraMemory(MT_Video);
		grcResourceCache::GetInstance().SetExtraMemory(MT_Video, iIncreaseAmount);
		return true;
	}
#endif // USE_RESOURCE_CACHE
	return false;
}
#endif // RSG_PC

bool strStreamingLoader::AllocateMemory(StreamingFile& file, strIndex index, strStreamingModule* module, int memBucket)
{
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(index);
	strStreamingAllocator& allocator = strStreamingEngine::GetAllocator();

	if (pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
	{
		datResourceMap& map = file.m_LoadingMap;
		pgRscBuilder::GenerateMap(pInfo->GetResourceInfo(), map);

#if RESOURCE_HEADER
#if __XENON && ENABLE_DEBUG_HEAP
		if (g_sysExtraStreamingMemory > 0)
		{
			const char* pName = strStreamingEngine::GetObjectName(index);
			const char* pExt = stristr(pName, ".");
			if (pExt)
				pExt++;

			if (!stricmp(pExt, "xsc"))
				map.HeaderType = RESOURCE_TYPE_SCRIPT;
		}
		else
#endif
		{
			if (module == g_TxdStore.GetStreamingModule())
			{
				map.HeaderType = RESOURCE_TYPE_DICTIONARY;
			}
		}		
#endif
	}
	else
	{
		datResourceMap& map = file.m_LoadingMap;
		memset(&file.m_LoadingMap,0, sizeof(datResourceMap));
		map.VirtualCount = 1;
		map.PhysicalCount = 0;
		map.Chunks[0].Size = pInfo->ComputeVirtualSize(index);
		map.Chunks[0].SrcAddr = (char*) datResourceFileHeader::c_FIXED_VIRTUAL_BASE;
		map.Chunks[0].DestAddr = NULL;
		map.RootVirtualChunk = 0;
		map.VirtualBase = map.Chunks[map.RootVirtualChunk].DestAddr;
#if RESOURCE_HEADER
		map.HeaderType = RESOURCE_TYPE_RPF;
#endif
	}

	sysMemCpy(&file.m_AllocationMap, &file.m_LoadingMap, sizeof(datResourceMap));

#if __ASSERT && __PPU
	if (*(strStreamingEngine::GetLoader().GetAssertOnLargeRscVirtChunkPtr()))
	{
		for (int x=0; x<file.m_AllocationMap.VirtualCount; x++)
		{
			Assertf(file.m_AllocationMap.Chunks[x].Size <= 256 * 1024, "%s is using a %dKB chunk - that causes fragmentation problems.", strStreamingEngine::GetObjectName(file.m_Index), file.m_AllocationMap.Chunks[x].Size / 1024);
		}
	}
#endif // __ASSERT


	if (module->UsesExtraMemory())
	{
		datResourceMap extraMap;
		memset(&extraMap, 0, sizeof(datResourceMap));

		strLocalIndex index = module->GetObjectIndex(file.m_Index);

#if __ASSERT && __PPU
		if (*(strStreamingEngine::GetLoader().GetAssertOnLargeRscVirtChunkPtr()))
		{
			for (int x=0; x<extraMap.VirtualCount; x++)
			{
				Assertf(extraMap.Chunks[x].Size <= 256 * 1024, "%s is using a %dKB chunk in its extra memory - that causes fragmentation problems.", strStreamingEngine::GetObjectName(file.m_Index), extraMap.Chunks[x].Size / 1024);
			}
		}
#endif // __ASSERT

		module->RequestExtraMemory(index, extraMap, datResourceChunk::MAX_CHUNKS - (file.m_LoadingMap.VirtualCount + file.m_LoadingMap.PhysicalCount));

		// Merge the loading map and extra map together
		streamAssertf(file.m_LoadingMap.VirtualCount + file.m_LoadingMap.PhysicalCount + 
			extraMap.VirtualCount + extraMap.PhysicalCount < datResourceChunk::MAX_CHUNKS, "Too many allocations (normal + extra) for a single datResourceMap. Normal: (%d Virt, %d Phys). Extra: (%d Virt, %d Phys)",
			file.m_LoadingMap.VirtualCount, file.m_LoadingMap.PhysicalCount,
			extraMap.VirtualCount, extraMap.PhysicalCount);

		// Map should look like:
		// Loading virtual
		// Extra virtual
		// Loading physical
		// Extra physical

		//const datResourceChunk* loadingVirtChunks = &file.m_LoadingMap.Chunks[0];
		const int loadingVirtCount = file.m_LoadingMap.VirtualCount;

		const datResourceChunk* loadingPhysChunks = &file.m_LoadingMap.Chunks[loadingVirtCount];
		const int loadingPhysCount = file.m_LoadingMap.PhysicalCount;

		const datResourceChunk* extraVirtChunks = &extraMap.Chunks[0];
		const int extraVirtCount = extraMap.VirtualCount;

		const datResourceChunk* extraPhysChunks = &extraMap.Chunks[extraVirtCount];
		const int extraPhysCount = extraMap.PhysicalCount;

		datResourceChunk* allocChunks = &file.m_AllocationMap.Chunks[0];

		// Don't need to do this, we copied the first allocations when we copied the whole map up above
		//sysMemCpy(allocChunks, loadingVirtChunks, sizeof(datResourceChunk) * loadingVirtCount);
		allocChunks += loadingVirtCount;

		sysMemCpy(allocChunks, extraVirtChunks, sizeof(datResourceChunk) * extraVirtCount);
		allocChunks += extraVirtCount;

		sysMemCpy(allocChunks, loadingPhysChunks, sizeof(datResourceChunk) * loadingPhysCount);
		allocChunks += loadingPhysCount;

		sysMemCpy(allocChunks, extraPhysChunks, sizeof(datResourceChunk) * extraPhysCount);
		//allocChunks += extraPhysCount;

		file.m_AllocationMap.VirtualCount = (u8) (loadingVirtCount + extraVirtCount);
		file.m_AllocationMap.PhysicalCount = (u8) (loadingPhysCount + extraPhysCount);
		file.m_AllocationMap.DisableMerge = true;
	}

#if __DEV
	char needed[256];
	file.m_AllocationMap.Print(needed, 256);
	streamDebugf2("%d, Device %d: Issued %s [V%" SIZETFMT "dK,M%" SIZETFMT "dk] => %s", 
		fwTimer::GetSystemFrameCount(), 
		m_Device, 
		strStreamingEngine::GetInfo().GetObjectName(file.m_Index),
		file.m_AllocationMap.GetVirtualSize()>>10,
		file.m_AllocationMap.GetPhysicalSize()>>10,
		needed);
#endif // __DEV

#if RAGE_TRACKING
	if (!PARAM_dontshowrscnameintracker.Get() && diagTracker::GetCurrent())
	{
		diagTracker::GetCurrent()->Push(strStreamingEngine::GetInfo().GetObjectName(file.m_Index));
	}
#endif // RAGE_TRACKING

	if (!allocator.StreamingAllocate(file.m_AllocationMap, file.m_Index, memBucket))
	{
		STR_TELEMETRY_MESSAGE_ERROR("(streaming/memory)Not enough memory for %s", strStreamingEngine::GetObjectName(file.m_Index));

#if RAGE_TRACKING
		if (!PARAM_dontshowrscnameintracker.Get() && diagTracker::GetCurrent())
		{
			diagTracker::GetCurrent()->Pop();
		}
#endif // RAGE_TRACKING

		if (file.m_Index.IsValid() && g_strStreamingInterface->WasLastAllocationAborted())
		{
			// The streaming system decided to not free up memory for this allocation cause it ain't worth it.
			// This will not be logged since it was a deliberate action - we may have been able to free up
			// memory but chose not to.
			STRVIS_ADD_FAILED_ALLOCATION(file.m_Index, strStreamingVisualize::CHURN_PROTECTION);
			streamDebugf3("Aborting allocation for %s - deemed not important enough to free memory for", strStreamingEngine::GetInfo().GetObjectName(file.m_Index));
			return false;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Streaming emergency
		

		bool isFragmented = sysThreadType::IsUpdateThread() && g_strStreamingInterface->WasLastAllocationFragmented();

		STRVIS_ADD_FAILED_ALLOCATION(index, (isFragmented) ? strStreamingVisualize::FRAGMENTED : strStreamingVisualize::OOM);

#if __DEV
		strStreamingEngine::GetInfo().LogFailedAllocation(strStreamingEngine::GetInfo().GetObjectName(index), pInfo->ComputeVirtualSize(index), pInfo->ComputePhysicalSize(index), isFragmented, pgRscBuilder::WasLastFailedAllocationPhysical(), pgRscBuilder::GetLastFailedAllocationSize());
#endif // __DEV

#if RSG_PC && defined(USE_RESOURCE_CACHE) && USE_RESOURCE_CACHE
		if (HandleOutOfMemory(pInfo->ComputeVirtualSize(index, true), pInfo->ComputePhysicalSize(index, true)))
		{
			return true;
		}
#endif // RSG_PC

		STRVIS_SET_MARKER_TEXT("EMERGENCY: %s for %dKB/%dKB for %s", (isFragmented) ? "Fragmented" : "OOM", pInfo->ComputeVirtualSize(file.m_Index, true) / 1024, pInfo->ComputePhysicalSize(file.m_Index, true) / 1024,
			strStreamingEngine::GetObjectName(file.m_Index));
		STRVIS_ADD_MARKER(strStreamingVisualize::OOM);

		if (!isFragmented)
		{	
			// Out of memory
			strStreamingEngine::GetLoader().SetOutOfMemory(true);
#if !__FINAL
			strStreamingEngine::GetLoader().SetEmergencyState(strStreamingLoaderManager::EMERGENCY_OUT_OF_MEMORY);
			g_strStreamingInterface->AddFailedStreamingAllocation(index, false, file.m_AllocationMap);
			strStreamingEngine::GetLoader().ResetEmergencyTimer();
			static bool dumped = false;
			if (!dumped)
			{
				streamWarningf("***** Out of Streaming Memory *****");
				streamWarningf("Unable to make space (%dKB virt/%dKB phys) for '%s' because we have no more memory, I give up",
					pInfo->ComputeVirtualSize(file.m_Index, true) / 1024, pInfo->ComputePhysicalSize(file.m_Index, true) / 1024, strStreamingEngine::GetObjectName(file.m_Index));
				int scriptVirtual = 0;
				int scriptPhysical = 0;
				g_strStreamingInterface->GetScriptMemoryUsage(scriptVirtual, scriptPhysical);
				streamDisplayf("*** Script uses %dk total (virtual %dK physical %dK)",(scriptVirtual>>10) + (scriptPhysical>>10), scriptVirtual>>10, scriptPhysical>>10);
				streamDisplayf("*** Streaming modules use:");
				g_strStreamingInterface->DisplayModuleMemory(true);
#if __DEV
				streamDisplayf("*** Objects loaded:");
				strStreamingEngine::GetInfo().PrintLoadedList(true);
#endif // __DEV
				g_strStreamingInterface->DumpStreamingInterests();
				streamWarningf("***** Out of Streaming Memory *****");
				dumped = true;
			}
			else
			{
				streamDebugf1("Unable to make space (%dKB virt/%dKB phys) for '%s' because we have no more memory, I give up",
					pInfo->ComputeVirtualSize(file.m_Index, true) / 1024, pInfo->ComputePhysicalSize(file.m_Index, true) / 1024, strStreamingEngine::GetObjectName(file.m_Index));
			}
#endif // !__FINAL
			return false; 
		}
		else
		{	
			// Fragmented
#if !__FINAL
			strStreamingEngine::GetLoader().SetEmergencyState(strStreamingLoaderManager::EMERGENCY_FRAGMENTED_MEMORY);
			g_strStreamingInterface->AddFailedStreamingAllocation(index, true, file.m_AllocationMap);
			strStreamingEngine::GetLoader().ResetFragmentationEmergencyTimer();
			static bool dumped = false;
			if (!dumped)
			{
				streamWarningf("***** Fragmented Streaming Memory *****");
				streamWarningf("Unable to make space for %s (%d, %d) because memory is fragmented, I give up", strStreamingEngine::GetObjectName(file.m_Index), pInfo->ComputeVirtualSize(file.m_Index), pInfo->ComputePhysicalSize(file.m_Index));

#if ONE_STREAMING_HEAP
				streamWarningf("Physical free: %dKB, rsc virt (%dKB) + phys (%dKB) = %dKB",
					allocator.GetPhysicalMemoryFree() / 1024, pInfo->ComputeVirtualSize(file.m_Index) / 1024, pInfo->ComputePhysicalSize(file.m_Index) / 1024,
					(pInfo->ComputeVirtualSize(index) + pInfo->ComputePhysicalSize(index)) / 1024);
#else  // ONE_STREAMING_HEAP
				streamWarningf("Virtual free: %dKB, rsc virt: %dKB",
					allocator.GetVirtualMemoryFree() / 1024, pInfo->ComputeVirtualSize(file.m_Index) / 1024);
				streamWarningf("Physical free: %dKB, rsc phys: %dKB",
					allocator.GetPhysicalMemoryFree() / 1024, pInfo->ComputePhysicalSize(file.m_Index) / 1024);
#endif // ONE_STREAMING_HEAP

#if __DEV
				char header[256];
				char usedMemory[256];
				char freeMemory[256];

				sysMemAllocator* virtualAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
				sysMemDistribution virtualDistribution;
				virtualAllocator->GetMemoryDistribution(virtualDistribution);
				g_strStreamingInterface->PrintMemoryDistribution(virtualDistribution, header, usedMemory, freeMemory);
				streamDisplayf("%s", header);
				streamDisplayf("%s", usedMemory);
				streamDisplayf("%s", freeMemory);
#if __PPU
				sysMemAllocator* physicalAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
				sysMemDistribution physicalDistribution;
				physicalAllocator->GetMemoryDistribution(physicalDistribution);
				g_strStreamingInterface->PrintMemoryDistribution(physicalDistribution, header, usedMemory, freeMemory);
				streamDisplayf("physical");
				streamDisplayf(header);
				streamDisplayf(usedMemory);
				streamDisplayf(freeMemory);
#endif
				char needed[256];
				file.m_AllocationMap.Print(needed, 256);

				streamDisplayf("%d, Device %d: Issued %s, %s", fwTimer::GetSystemFrameCount(), m_Device, strStreamingEngine::GetInfo().GetObjectName(file.m_Index),
						needed);
#endif // __DEV
				dumped = true;
			}
			else
			{
				streamDebugf1("Unable to make space for %s (%d, %d) because memory is fragmented, I give up", strStreamingEngine::GetObjectName(file.m_Index), pInfo->ComputeVirtualSize(file.m_Index), pInfo->ComputePhysicalSize(file.m_Index));
			}
#endif // !__FINAL
		}

		return false;
	}

#if RAGE_TRACKING
	if (module->CanDefragment())
	{
		u32 i = 0;
		u32 virtCount = file.m_AllocationMap.VirtualCount;
		for (; i<virtCount; ++i)
		{
			void *const ptr = file.m_AllocationMap.Chunks[i].DestAddr;
			if (diagTracker::GetCurrent())
			{
				diagTracker::GetCurrent()->MarkDefragmentable(ptr, true);
			}
		}
		u32 totCount = file.m_AllocationMap.VirtualCount+file.m_AllocationMap.PhysicalCount;
		for (; i<totCount; ++i)
		{
			void *const ptr = file.m_AllocationMap.Chunks[i].DestAddr;
			if (diagTracker::GetCurrent())
			{
				diagTracker::GetCurrent()->MarkDefragmentable(ptr, true);
			}
		}
	}

	if (!PARAM_dontshowrscnameintracker.Get() && diagTracker::GetCurrent())
	{
		diagTracker::GetCurrent()->Pop();
	}
#endif // RAGE_TRACKING

	// We can now do the read all at once.
	streamAssertf(file.m_AllocationMap.PhysicalCount+file.m_AllocationMap.VirtualCount, "file has no blocks");
	NOTFINAL_ONLY(strStreamingEngine::GetLoader().SetEmergencyState(strStreamingLoaderManager::EMERGENCY_NONE));

	return true;
}


bool strStreamingLoader::IssueRequest(StreamingFile& file)
{
	Assert(sysThreadType::IsUpdateThread());

	USE_MEMBUCKET(MEMBUCKET_RESOURCE);
	// make space for - do we have to make space for specific segment sizes?
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);

	streamAssert(pInfo->GetStatus() == STRINFO_LOADREQUESTED);

	strStreamingModule* module = strStreamingEngine::GetInfo().GetModuleMgr().GetModuleFromIndex(file.m_Index);

#if __ASSERT
	// TEMP debugging to track down a rare crash.
	strLocalIndex imageIndex = strLocalIndex(strPackfileManager::GetImageFileIndexFromHandle(pInfo->GetHandle()));
	if (imageIndex != -1)
	{
		if (!strPackfileManager::GetImageFile(imageIndex.Get())->m_bInitialized)
		{
			strIndex archiveStrIndex = strPackfileManager::GetStreamingModule()->GetStreamingIndex(imageIndex);
			strStreamingInfo &archiveInfo = strStreamingEngine::GetInfo().GetStreamingInfoRef(archiveStrIndex);

			char fileNameBuffer[RAGE_MAX_PATH];
			char archNameBuffer[RAGE_MAX_PATH];
			const char *fileName, *archName;

			fileName = strStreamingEngine::GetInfo().GetObjectName(file.m_Index, fileNameBuffer, sizeof(fileName));
			archName = strStreamingEngine::GetInfo().GetObjectName(archiveStrIndex, archNameBuffer, sizeof(archName));
			strLocalIndex archObjIndex = strPackfileManager::GetStreamingModule()->GetObjectIndex(archiveStrIndex);

			streamAssertf(false, "Loading file %s, but its RPF file %s is not resident (status=%s, refs=%d, isInit=%d)",
				fileName, archName, archiveInfo.GetFriendlyStatusName(),
				strPackfileManager::GetStreamingModule()->GetNumRefs(archObjIndex),
				strPackfileManager::GetImageFile(imageIndex.Get())->m_bInitialized
				);
		}
	}
#endif // __ASSERT


	if (pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
	{
#if __DEV
		datResourceMap map;
		pgRscBuilder::GenerateMap(pInfo->GetResourceInfo(), map);

		char needed[256];
		map.Print(needed, 256);
		streamDebugf2("%d, Device %d: Issued %s, %s", fwTimer::GetSystemFrameCount(), m_Device, strStreamingEngine::GetInfo().GetObjectName(file.m_Index),
			needed);
#endif // __DEV
	}
	else
	{
		streamDebugf2("%d, Device %d: Issued %s, virtual=%dKB, physical=%dKB", fwTimer::GetSystemFrameCount(), m_Device, strStreamingEngine::GetInfo().GetObjectName(file.m_Index),
			pInfo->ComputeVirtualSize(file.m_Index, true) / 1024, pInfo->ComputePhysicalSize(file.m_Index, true) / 1024);
	}

		streamVerify(OpenFile(file));

#if RAGE_TRACKING
	int memBucket = MEMBUCKET_RESOURCE | (module->CanDefragment() ? diagTracker::MEMBUCKETFLAG_CAN_DEFRAG : 0);
#else // RAGE_TRACKING
	int memBucket = MEMBUCKET_RESOURCE;
#endif // RAGE_TRACKING

	STR_TELEMETRY_MESSAGE_LOG("(streaming/memory)Getting memory for %s to stream from %s, v=%dK, p=%dK",
		strStreamingEngine::GetObjectName(file.m_Index), GetLoaderName(), pInfo->ComputeVirtualSize(file.m_Index, true) / 1024, pInfo->ComputePhysicalSize(file.m_Index, true) / 1024);

	if (!AllocateMemory(file, file.m_Index, module, memBucket))
	{
		STR_TELEMETRY_MESSAGE_ERROR("(streaming/memory)Could not allocate memory for %s", strStreamingEngine::GetObjectName(file.m_Index));

		// Since we failed because of a streaming emergency, lets bump this request to end of the list.
		// See streamer.cpp::844 for the origin of this logic. 
		const fiCollection *device = fiCollection::GetCollection(file.m_Handle);
		const fiPackEntry &hdr = device->GetEntryInfo(file.m_Handle);
		m_LastFailedRequestPosition = device->GetEntryPhysicalSortKey(file.m_Handle,pgStreamer::NONE/*r.m_Uncached*/) + ((hdr.GetConsumedSize() + 2047) >> 11);

		streamVerify(CloseFile(file));
		return false; //no room in the inn
	}
	m_LastFailedRequestPosition = 0;

	// Allocation succeeded, make sure the m_LoadingMap is up to date (copy file allocation info over from m_AllocationMap)
	file.m_LoadingMap.VirtualBase = file.m_AllocationMap.VirtualBase;
	sysMemCpy(&file.m_LoadingMap.Chunks[0], &file.m_AllocationMap.Chunks[0], sizeof(datResourceChunk) * file.m_LoadingMap.VirtualCount);
	sysMemCpy(&file.m_LoadingMap.Chunks[file.m_LoadingMap.VirtualCount], &file.m_AllocationMap.Chunks[file.m_AllocationMap.VirtualCount], sizeof(datResourceChunk) * file.m_LoadingMap.PhysicalCount);

	u32 offset = 0;
	if (pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
	{
		offset = sizeof(datResourceFileHeader);

		// Make sure all the allocated memory is properly locked
#if __ASSERT && ENABLE_BUDDY_ALLOCATOR
		for (int i=0; i<file.m_LoadingMap.VirtualCount; i++) 
		{
			sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
			streamAssertf(alloc.GetBlockLockCount(file.m_LoadingMap.Chunks[i].DestAddr) == 1,"Virtual block has unexpected lock count %d in IssueRequest",alloc.GetBlockLockCount(file.m_LoadingMap.Chunks[i].DestAddr));
		}
		for (int i=file.m_LoadingMap.VirtualCount; i<file.m_LoadingMap.VirtualCount+file.m_LoadingMap.PhysicalCount;i++) 
		{
			sysMemBuddyAllocator& alloc = *(sysMemBuddyAllocator*)sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
			streamAssertf(alloc.GetBlockLockCount(file.m_LoadingMap.Chunks[i].DestAddr) == 1,"Physical block has unexpected lock count %d in IssueRequest",alloc.GetBlockLockCount(file.m_LoadingMap.Chunks[i].DestAddr));
		}
#endif
	}

	// NOTE - in the process of allocating memory right above there, we may have had to free up some memory. In this process, we may have
	// deleted the only dependent left of this object, which makes requesting this object here unnecessary.
	// We can detect that but verifying our status - if it's not still LOADREQUESTED, then we've been booted and have to jump out.
	if (pInfo->GetStatus() == STRINFO_NOTLOADED)
	{
		// Give up the memory!
		streamDebugf2("%s just lost its last dependent and has been unrequested", strStreamingEngine::GetObjectName(file.m_Index));
		MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(file.m_Index));
		strStreamingEngine::GetAllocator().StreamingFree(file.m_AllocationMap WIN32PC_ONLY(, true));
		streamVerify(CloseFile(file));
		return false;
	}

	// Read from hard disk or optical drive?
	u32 extraFlags = GetStreamerFlags();

#if __BANK
	module->IncrementLoadedCount();
#endif
	STRVIS_SET_TRACKED_STREAMEREAD(true);

	file.m_ReadId = pgStreamer::Read(
		file.m_Handle,
		file.m_LoadingMap.Chunks,
		file.m_LoadingMap.PhysicalCount+file.m_LoadingMap.VirtualCount,
		offset,
		SignalCallback,
		(void*)&file,	//  fixme KS
		this->m_Device,
		module->GetStreamerReadFlags() | extraFlags);

	STRVIS_SET_TRACKED_STREAMEREAD(false);
	
	if (!file.m_ReadId) 
	{
		if (!pgStreamer::IsLocked(m_Device)) // is the streamer locked by audio? low level check to compensate for threading issues - fixme KS
		{
			streamAssertf(false, "Failed to issue read request '%s' - the pgStreamer queue needs to be bigger?", strStreamingEngine::GetObjectName(file.m_Index));
		}

		// Failed a read => free everything
		{
			MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(file.m_Index));
			strStreamingEngine::GetAllocator().StreamingFree(file.m_AllocationMap WIN32PC_ONLY(, true));
		}

		streamVerify(CloseFile(file));
		return false;
	}

#if __XENON && !__NO_OUTPUT
	{
	int deviceType = (pgStreamer::GetPosn(file.m_Handle,module->GetStreamerReadFlags() | extraFlags) & (fiDevice::HARDDRIVE_LSN))? pgStreamer::HARDDRIVE : pgStreamer::OPTICAL;

	if (deviceType != m_Device)
	{
		char finalStr[256];
		formatf(finalStr, "***** Device mismatch (streamer=%d vs framework=%d) for handle %x, strIndex %d. Flags=%x, extra=%x\n", deviceType, m_Device, file.m_Handle, file.m_Index.Get(), module->GetStreamerReadFlags(), extraFlags);
		Printf(finalStr);
	}
	}
#endif


#if STREAMING_VISUALIZE
	int deviceType = this->m_Device;

#if __ASSERT && 0
	// Ignore raw streamer
	if(fiCollection::GetCollection(file.m_Handle) != (fiCollection*)GetRawStreamer())
	{
		streamAssertf(m_Device == deviceType, "File %s is expected to be device %d but is type %d - Posn is 0x%x (extra flags %x)",
			strStreamingEngine::GetObjectName(file.m_Index), m_Device, deviceType, pgStreamer::GetPosn(file.m_Handle,module->GetStreamerReadFlags() | extraFlags), extraFlags);
	}

	if (deviceType != m_Device)
	{
		streamErrorf("Device mismatch (streamer=%d vs framework=%d) for %s. Flags=%x, extra=%x", deviceType, m_Device, strStreamingEngine::GetObjectName(file.m_Index), module->GetStreamerReadFlags(), extraFlags);
	}

#endif //__ASSERT 
	
	STRVIS_ISSUE_REQUEST(file.m_Index, deviceType, (u32) (uptr) file.m_ReadId, fiCollection::GetCollection(file.m_Handle)->GetEntryPhysicalSortKey(file.m_Handle, false), pInfo->GetFlags());
#endif // STREAMING_VISUALIZE

	file.m_Reissue = false;
	file.m_Status = STATUS_LOADING;
	#if !LOAD_SEMA
		file.m_Finished = false;
	#endif
	streamVerify(m_StreamingQueue.Push(&file));
	m_Pending++;
	m_PendingIO++;
	streamAssert(m_Pending <= m_StreamingFiles.GetMaxCount());

	int fileSize = pInfo->ComputeVirtualSize(file.m_Index) + pInfo->ComputePhysicalSize(file.m_Index);
	m_FramePendingSize += fileSize;
	m_PendingSize += fileSize;

	strStreamingEngine::GetInfo().SetObjectToLoading(file.m_Index);

	strStreamingEngine::GetAllocator().SanityCheck();

#if STREAM_STATS
	file.m_LoadTime = sysTimer::GetSystemMsTime();
#endif

#if !__FINAL
	//	streamDisplayf("Head %d\n", GetHeadPosition());
#endif

	return true;
}

#if RESOURCE_HEADER
bool ResourceHeader_GetLastTexturePointer(grcTexture& texture, u32 UNUSED_PARAM(code), void* data)
{
	// Name
	const char* pName = texture.GetName();
	void* ptr = reinterpret_cast<void*>(const_cast<char*>(pName + strlen(pName)));
	if (ptr > *((void**)data))
		*((void**)data) = ptr;

	// Memory Offset	
#if __PPU
	const grcTextureGCM& textureGCM = (grcTextureGCM&) texture;
	void* pOffset = textureGCM.GetMemoryOffset();
	if (pOffset)
	{
		const int mips = texture.GetMipMapCount();
		const u32 layers = texture.GetLayerCount();
		ptr = (reinterpret_cast<void*>(reinterpret_cast<size_t>(pOffset) + static_cast<size_t>(sizeof(u32) * (mips * layers))));
		if (ptr > *((void**)data))
			*((void**)data) = ptr;
	}
#endif

	return true;
}

bool ResourceHeader_UnregisterTexture(grcTexture& texture, u32 UNUSED_PARAM(code), void* UNUSED_PARAM(data))
{
	pgHandleBase::Unregister(&texture);
	return true;
}
#endif

void strStreamingLoader::SignalCallback(void *userArg,void * /*dest*/,u32 /*offset*/,u32 /*amtRead*/) {
	StreamingFile* file = (StreamingFile*)userArg;

	#if LOAD_SEMA
		sysIpcSignalSema(file->m_ReadSema);
	#else
		file->m_Finished = true;
	#endif

	// streamDisplayf("Signalled %s", strStreamingEngine::GetObjectName(file->m_Index));
	// we could call place from here and get the completion of the resource 'for free' on a separate thread
}

#if __BANK && __ASSERT
const char *strStreamingLoader::ProcessFileDiagCb(void * inStrIndex, void * /*b*/)
{
	static bool nestedCall = false;

	// Prevent endless loop by detecting reentrance.
	if (nestedCall)
	{
		return "[streamError while getting object name]";
	}

	nestedCall = true;
	strIndex* theIndex = (strIndex*)inStrIndex;
	const char *returnValue = strStreamingEngine::GetObjectName(*theIndex);
		//strStreamingEngine::GetInfo().GetObjectName(s_StreamablesBeingUnloaded[s_StreamablesBeingUnloaded.GetCount()-1]);
	nestedCall = false;

	return returnValue;
}
#endif // __BANK && __ASSERT

bool strStreamingLoader::ProcessFile(StreamingFile& file)
{
	strStreamingEngine::GetInfo().SanityCheckStrIndex(file.m_Index);

	// Useful for tracking leaks in object handling
	//Printf( "ProcessFile: %s [status: %d]\n", strStreamingEngine::GetObjectName(file.m_Index), file.m_Status);

#if __BANK
	if (ms_IsPumpAndDump && file.m_Status != STATUS_CANCELED)
	{
		streamDebugf2("PUMP AND DUMP - discarding %s", strStreamingEngine::GetObjectName(file.m_Index));

		strStreamingEngine::GetInfo().AddDependentCounts(file.m_Index);

		file.m_Status = STATUS_CANCELED;
		strStreamingEngine::GetInfo().SetObjectToNotLoadedFromLoading(file.m_Index);

		// Undo some of the things that were done by SetObjectToNotLoadedFromLoading() and will be done again below.
		strStreamingInfo &info = strStreamingEngine::GetInfo().GetStreamingInfoRef(file.m_Index);
		s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info.GetHandle()).Get();
		if (imageIndex != -1)
		{
			strPackfileManager::AddStreamableReference(imageIndex);
		}
	}
#endif // __BANK

	if (!g_AREA51 || !file.m_Reissue)
	{
		strStreamingInfo *info = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
		u32 fileSize = info->ComputeVirtualSize(file.m_Index, false, true) + info->ComputePhysicalSize(file.m_Index);
		streamAssert(m_PendingSize >= fileSize);
		m_PendingSize -= fileSize;
		m_PendingIO--;

		streamAssert(m_PendingIO >= 0);
	}

	// Useful for tracking leaks in object handling
	//Printf( "ProcessFile post pump and dump: %s [status: %d]\n", strStreamingEngine::GetObjectName(file.m_Index), file.m_Status);

	if (file.m_Status == STATUS_CANCELED)
	{
		{
			MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(file.m_Index));
			strStreamingEngine::GetAllocator().StreamingFree(file.m_AllocationMap WIN32PC_ONLY(, true));
		}

		// Release the dependency on the archive.
		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);

#if STREAM_STATS
		m_RedundantCount++;
		m_RedundantSize += info->ComputeVirtualSize(file.m_Index, true) + info->ComputePhysicalSize(file.m_Index, true);

		strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModuleFromIndex(file.m_Index);
		pModule->TrackCanceledLoad(info->ComputeVirtualSize(file.m_Index, true), info->ComputePhysicalSize(file.m_Index, true));
#endif // STREAM_STATS

		strStreamingEngine::GetInfo().RemoveDependentCountsAndUnrequest(file.m_Index);

		s32 imageIndex = strPackfileManager::GetImageFileIndexFromHandle(info->GetHandle()).Get();
		if (imageIndex != -1)
		{
			strPackfileManager::RemoveStreamableReference(imageIndex);
		}

		streamAssert(FindExistingAsyncRequest(file.m_Index) == -1);

		file.m_ReadId = NULL;
		file.m_Handle = pgStreamer::Error;
		file.m_LoadTime = 0;
		file.m_Index = strIndex(strIndex::INVALID_INDEX);
		file.m_IsPlaced = (u32) false;
		#if !LOAD_SEMA
			file.m_Finished = false;
		#endif
		//Moved to be the last thing set, it's the flag that states that this StreamingFile is done with
		//This is all meant to be on a single thread, but I have the paranoias
		file.m_Status = STATUS_IDLE;

		m_Pending--;

		streamAssertf(m_Pending >= 0, "Pending files number %d is incorrect", m_Pending);
	}
	else
	{
#if __ASSERT
		strStreamingInfo* info = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
		streamAssertf(info->GetStatus() == STRINFO_LOADING || (info->GetFlags() & STRFLAG_INTERNAL_DUMMY),
			"File '%s' has just finished loading, but its status is %s.",
			strStreamingEngine::GetObjectName(file.m_Index), strStreamingInfo::GetFriendlyStatusName(info->GetStatus()));
#endif // __ASSERT

#if !__FINAL
		//safecpy(gStreamedObjName, strStreamingEngine::GetObjectName());
#endif

#if __ASSERT
		DIAG_CONTEXT_MESSAGE("ProcessFile(%s)", ProcessFileDiagCb, (void*)&(file.m_Index), NULL);
#endif // __ASSERT

		//if the convert fails the index is reset so store it off here so we can get at the name for spew message
		ASSERT_ONLY(strIndex current = file.m_Index);

		EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("ConvertToObject", 2.0f);)
		if (ConvertToObject(file) == false)
		{
			streamAssertf(info->GetStatus() == STRINFO_LOADED || info->GetStatus() == STRINFO_NOTLOADED, "%s (%d) invalid streaming state", strStreamingEngine::GetObjectName(current), info->GetStatus());
			streamAssertf(0, "Failed to convert file %s", strStreamingEngine::GetObjectName(current));
			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)
			return false;
		}
		EXTRA_TIMEBARS(PF_POP_TIMEBAR();)
	}

	SANITY_CHECK();

	return true;
}

bool strStreamingLoader::ConvertToObject(StreamingFile& file)
{
	RAGE_TIMEBARS_ONLY(pfAutoMarker m(strStreamingEngine::GetObjectName(file.m_Index),30);)
	USE_MEMBUCKET(MEMBUCKET_RESOURCE);
	streamAssertf(file.m_Status == STATUS_LOADING, "File status is %d", file.m_Status);
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
	streamAssertf(pInfo->GetStatus() == STRINFO_LOADING, "Info status is %d", pInfo->GetStatus());
	strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModuleMgr().GetModuleFromIndex(file.m_Index);
	strLocalIndex objIndex = pModule->GetObjectIndex(file.m_Index);

#if TIME_LOADING
	sysTimer timeLoad;
#endif

	streamDebugf2("%d: %s Converting %s", fwTimer::GetSystemFrameCount(), GetLoaderName(), strStreamingEngine::GetObjectName(file.m_Index));

#if __BANK
	g_strStreamingInterface->LogStreaming(file.m_Index);
#endif

	streamVerify(CloseFile(file));

	// check if dependencies are loaded
	bool bAreDependenciesLoaded = true;
	strIndex deps[STREAMING_MAX_DEPENDENCIES];
	s32 numDeps = pModule->GetDependencies(objIndex, &deps[0], STREAMING_MAX_DEPENDENCIES);
	for (s32 i = 0; i < numDeps; ++i)
	{
		if(!strStreamingEngine::GetInfo().HasObjectLoaded(deps[i]))
		{
#if __ASSERT
			char depName[RAGE_MAX_PATH];
			strResidentStatus status = strStreamingEngine::GetInfo().GetStreamingInfo(deps[i])->GetStatus();
			int depCount = strStreamingEngine::GetInfo().GetStreamingInfo(deps[i])->GetDependentCount();
			if (status == STRINFO_LOADING)
			{
				streamAssertf(false, "%s (dependency for %s) is not in memory but is about to be, dep count=%d", strStreamingEngine::GetInfo().GetObjectName(deps[i], depName, sizeof(depName)), strStreamingEngine::GetObjectName(file.m_Index), depCount);
			}
			else
			{
				streamAssertf(false, "%s (dependency for %s) is not in memory, and %s requested, dep count=%d", strStreamingEngine::GetInfo().GetObjectName(deps[i], depName, sizeof(depName)), strStreamingEngine::GetObjectName(file.m_Index), (status == STRINFO_LOADREQUESTED) ? "" : "not", depCount);
			}
#endif // __ASSERT

			bAreDependenciesLoaded = false;
		}
	}

#if STREAM_STATS
	m_PendingTime += sysTimer::GetSystemMsTime() - file.m_LoadTime;
#endif

	bool success = false;
	if (bAreDependenciesLoaded)
	{
#if STREAM_STATS
		sysTimer place;
#endif
		if (pModule->UsesExtraMemory())
		{
			// Build a resource map containing just the extra allocations
			datResourceMap extraMap;
			memset(&extraMap, 0, sizeof(datResourceMap));

			extraMap.VirtualCount = file.m_AllocationMap.VirtualCount - file.m_LoadingMap.VirtualCount;
			extraMap.PhysicalCount = file.m_AllocationMap.PhysicalCount - file.m_LoadingMap.PhysicalCount;

			// Skip over the first virtual allocations, they are from the loading map
			const datResourceChunk* extraVirtChunks = &file.m_AllocationMap.Chunks[file.m_LoadingMap.VirtualCount];
			// Skip over all the virtual allocations and the first physical allocations
			const datResourceChunk* extraPhysChunks = extraVirtChunks + extraMap.VirtualCount + file.m_LoadingMap.PhysicalCount;

			sysMemCpy(&extraMap.Chunks[0], extraVirtChunks, sizeof(datResourceChunk) * extraMap.VirtualCount);
			sysMemCpy(&extraMap.Chunks[extraMap.VirtualCount], extraPhysChunks, sizeof(datResourceChunk) * extraMap.PhysicalCount);

			pModule->ReceiveExtraMemory(objIndex, extraMap);
		}

#if STREAM_STATS
		pModule->TrackCompletedLoad(pInfo->ComputeVirtualSize(file.m_Index, true), pInfo->ComputePhysicalSize(file.m_Index, true));
#endif // STREAM_STATS

#if __BANK
		// Useful for tracking leaks in object handling
		//Printf("-=[ Convert to Object Begins %s Int: %c ]=-\n", strStreamingEngine::GetObjectName(file.m_Index), pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE) ? 'Y' : 'N');
		if(::gStreamingLoaderPreCallback)
		{
			::gStreamingLoaderPreCallback(pModule->GetStreamingModuleId(), objIndex.Get(), true);
		}
#endif	//__BANK

		datResourceInfo resInfo;
		if(pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE))
		{
			resInfo = pInfo->GetResourceInfo();	
		}
		else
		{
			sysMemSet(&resInfo, 0, sizeof(resInfo));
		}
		
		datResourceMap map = file.m_LoadingMap;

		if (!PARAM_nomtplacement.Get() && pModule->CanPlaceAsynchronously(objIndex))
		{
			pModule->PlaceAsynchronously(objIndex, file, resInfo);
			streamAssertf(m_AsyncPlacements.Find(&file) == -1, "File %s is in async queue twice", strStreamingEngine::GetObjectName(file.m_Index));
			m_AsyncPlacements.Append() = &file;
			return true;
		}

		// Even when running with -nomtplacement, if CanPlaceAsynchronously is true, the module expects that
		// PlaceResource / SetResource will get called.
		if(pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE) || pModule->CanPlaceAsynchronously(objIndex))
		{

#if TRACK_PLACE_STATS
			pModule->StartTrackingPlace();
#endif // TRACK_PLACE_STATS
			pModule->PlaceResource(objIndex, map, resInfo );
			PerformMainThreadPlacement(file);
#if TRACK_PLACE_STATS
			pModule->StopTrackingPlace(pModule->GetName(objIndex), pInfo->ComputeVirtualSize(file.m_Index, false) + pInfo->ComputePhysicalSize(file.m_Index, false));
#endif // TRACK_PLACE_STATS
			success = true;
		}
		else
		{
			EXTRA_TIMEBARS(PF_PUSH_TIMEBAR_BUDGETED("pModule->Load", 2.0f);)

			int fileSize = pInfo->ComputeVirtualSize(file.m_Index) + pInfo->ComputePhysicalSize(file.m_Index);
#if TRACK_PLACE_STATS
			pModule->StartTrackingPlace();
#endif // TRACK_PLACE_STATS
			success = pModule->Load(objIndex, file.m_LoadingMap.GetVirtualBase(), fileSize);

			if (success)
			{
				PerformMainThreadPlacement(file);
			}
#if TRACK_PLACE_STATS
			pModule->StopTrackingPlace(pModule->GetName(objIndex), fileSize);
#endif // TRACK_PLACE_STATS

			if (!success)
			{
				streamErrorf("Failed to load the file");
			}

			EXTRA_TIMEBARS(PF_POP_TIMEBAR();)
		}

#if __BANK
		if(success)
		{
			if(::gStreamingLoaderPostCallback)
			{
				::gStreamingLoaderPostCallback(pModule->GetStreamingModuleId(), objIndex.Get(), true);
			}
		}
		// Useful for tracking leaks in object handling
		//Printf("-=[ Convert to Object Ends %s ]=-\n", strStreamingEngine::GetObjectName(file.m_Index));
#endif	//__BANK

#if STREAM_STATS
		m_PlaceTime += place.GetMsTime();
#endif
	}
	else 
	{
		// Useful for tracking leaks in object handling
		//Printf("-=[ Dependencies for %s weren't loaded ]=-\n", strStreamingEngine::GetObjectName(file.m_Index));
		//strStreamingEngine::GetInfo().ReRequestObject(file.m_Index);
		success = false;
	}

	if (success)
	{
		MarkPlacementComplete(file);
	}
	else 
	{

		streamWarningf("Streaming %s failed during ConvertObject", strStreamingEngine::GetObjectName(file.m_Index));
#if STREAM_STATS
		m_FailedSize += pInfo->ComputeVirtualSize(file.m_Index, true) + pInfo->ComputePhysicalSize(file.m_Index, true);
		m_Failed++;
#endif
		{
			// delete the file data - we've transferred ownership of any extra memory already
			MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(file.m_Index));
			strStreamingEngine::GetAllocator().StreamingFree(file.m_LoadingMap WIN32PC_ONLY(, true));
		}
		strStreamingEngine::GetInfo().SetObjectToNotLoadedFromLoading(file.m_Index);
	}

#if TIME_LOADING	
	streamDebugf1("%s took %f ms\n", strStreamingEngine::GetObjectName(file.m_Index), timeLoad.GetTime()*1000.0f);
#endif

	// wrap up
	streamAssert(FindExistingAsyncRequest(file.m_Index) == -1);
	FinalizePlacement(file);
	return success;
}

void strStreamingLoader::PerformMainThreadPlacement(StreamingFile &file)
{
	strIndex index = file.m_Index;

	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(index);
	strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(index);

	pInfo->ClearFlag(index, STRFLAG_INTERNAL_PLACING);
	strLocalIndex moduleIndex = pModule->GetObjectIndex(index);

	if (pInfo->IsFlagSet(STRFLAG_INTERNAL_RESOURCE) || pModule->CanPlaceAsynchronously(moduleIndex))
	{
		streamDebugf2("Performing main thread placement for %s", strStreamingEngine::GetObjectName(index));
		streamAssertf(pInfo->GetStatus() == STRINFO_LOADING, "%s (%d) should be LOADING, but is %d", strStreamingEngine::GetObjectName(index), index.Get(), pInfo->GetStatus());
		datResourceMap map = file.m_LoadingMap;
		pModule->SetResource(moduleIndex, map);
	}
}

void strStreamingLoader::MarkPlacementComplete(StreamingFile &file)
{
	strIndex index = file.m_Index;

	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(index);
	strStreamingModule* pModule = strStreamingEngine::GetInfo().GetModule(index);

	streamAssert((pInfo->GetFlags() & STRFLAG_INTERNAL_PLACING) == 0);		// Doing MT placement without calling PerformMainThreadPlacement after that could leave us in a bad state

	streamDebugf3("Placement of %s complete", strStreamingEngine::GetObjectName(index));

	strStreamingEngine::GetInfo().SetObjectToLoaded(index);

	// strIndex deps[STREAMING_MAX_DEPENDENCIES];
	strLocalIndex objIndex = pModule->GetObjectIndex(index);

#if STREAM_STATS
	m_CompletedSize += pInfo->ComputeVirtualSize(index, true) + pInfo->ComputePhysicalSize(index, true);
	m_Completed++;
#else
	(void)pInfo;
#endif

	if (pModule->RequiresTempMemory(objIndex))
	{
		MEM_USE_USERDATA(strStreamingInfoManager::CreateUserDataFromStreamingIndex(file.m_Index));
		strStreamingEngine::GetAllocator().StreamingFree(file.m_LoadingMap);
	}
#if USE_DEFRAGMENTATION
	else if (pModule->CanDefragment() && !pInfo->IsFlagSet(STRFLAG_INTERNAL_DONT_DEFRAG) && !pInfo->IsFlagSet(STRFLAG_INTERNAL_DUMMY))
	{
		strStreamingEngine::GetDefragmentation()->AddResource((pgBase*)file.m_LoadingMap.GetVirtualBase(), file.m_LoadingMap, file.m_Index);
	}
#endif // USE_DEFRAGMENTATION
}

void strStreamingLoader::FinalizePlacement(StreamingFile &file)
{
	file.m_ReadId = NULL;
	file.m_Index = strIndex(strIndex::INVALID_INDEX);
	file.m_IsPlaced = (u32) false;
	file.m_LoadTime = 0;
	#if !LOAD_SEMA
		file.m_Finished = false;
	#endif
	//Moved to be the last thing set, it's the flag that states that this StreamingFile is done with
	//This is all meant to be on a single thread, but I have the paranoias
	file.m_Status = STATUS_IDLE;

	m_Pending--;
	streamAssertf(m_Pending >= 0, "Pending files number %d is incorrect", m_Pending);
}


u32 strStreamingLoader::GetHeadPosition()
{
	return Max(m_LastFailedRequestPosition, pgStreamer::GetLastRequestPosn(m_Device));
}


bool strStreamingLoader::OpenFile(StreamingFile& file)
{
	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);

	file.m_Handle = pInfo->GetHandle();
#if LIVE_STREAMING
	if (fiCollection::GetCollectionIndex(file.m_Handle) == 0)
	{
		strStreamingEngine::GetLive().OpenLiveFile(file.m_Index);
	}
#endif
	return (file.m_Handle != pgStreamer::Error);
}


bool strStreamingLoader::CloseFile(StreamingFile& file)
{
#if LIVE_STREAMING
	if (fiCollection::GetCollectionIndex(file.m_Handle) == 0)
	{
		strStreamingEngine::GetLive().CloseLiveFile(file.m_Index); 
	}
#endif // LIVE_STREAMING
	file.m_Handle = pgStreamer::Error;
	return true;
}

void strStreamingLoader::GetLoadingFileSizes(size_t& virtualSize, size_t& physicalSize)
{
	virtualSize = 0;
	physicalSize = 0;

	for (int f = 0; f < m_StreamingQueue.GetCount(); ++f)
	{
		const StreamingFile& file = *m_StreamingQueue[f];

		FastAssert(file.m_Status == STATUS_LOADING || file.m_Status == STATUS_CANCELED);
		for (int i = 0; i != file.m_LoadingMap.VirtualCount ; ++i)
		{
			size_t size =  strStreamingEngine::GetAllocator().GetSize(file.m_LoadingMap.Chunks[i].DestAddr);
			FastAssert(size);
			virtualSize += size;
		}

		for (int i = file.m_LoadingMap.VirtualCount; i != file.m_LoadingMap.VirtualCount + file.m_LoadingMap.PhysicalCount; ++i)
		{
			size_t size = strStreamingEngine::GetAllocator().GetSize(file.m_LoadingMap.Chunks[i].DestAddr);
			FastAssert(size);
			physicalSize += size;
		}
	}
	
}

#if !__NO_OUTPUT
const char *strStreamingLoader::GetLoaderName() const
{
	return (m_Device == pgStreamer::OPTICAL) ? "Optical" : "Hard Drive";
}
#endif // !__NO_OUTPUT

void strStreamingLoaderManager::Init()
{
	CompileTimeAssert(DEVICE_COUNT == 2);

	m_callback         = 0;
	m_callbackInterval = 50;

	m_loaders[0].Init(pgStreamer::OPTICAL);
	m_loaders[1].Init(pgStreamer::HARDDRIVE);

	m_LoadingAllRequested = false;
#if __BANK
//	m_AddLoaderSVMarkers = true;
#endif // __BANK

	m_DeferAsyncPlacementRequests = false;
	m_DeferredPlacementRequests.Reset();
	m_DeferredPlacementRequests.Reserve(32);

	if (!PARAM_nomtplacement.Get())
	{
		m_iPlacementThreadId = sysIpcCreateThread(ResourcePlacementThread, this, sysIpcMinThreadStackSize * 4, PRIO_BELOW_NORMAL, "ResourcePlacementThread", 2, "ResourcePlacementThread");
	}

	m_outOfMemory = false;

#if __BANK
	*(strStreamingEngine::GetLoader().GetAssertOnLargeRscVirtChunkPtr()) = !PARAM_noassertonlargevirtchunks.Get();
#endif // __BANK
}

void strStreamingLoaderManager::Shutdown()
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].Shutdown();
	}

	if (!PARAM_nomtplacement.Get())
	{
		// Send a NULL request to shut the worker thread down.
		strPlacementRequest placeRequest;

		placeRequest.m_Info = NULL;
			m_AsyncPlaceQueue->Push(placeRequest);
		
		sysIpcWaitThreadExit(m_iPlacementThreadId);
	}
}

void strStreamingLoaderManager::ProcessAsyncFiles()
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].ProcessAsyncFiles();
	}
}

void strStreamingLoaderManager::LoadRequestedObjects()
{
	// 8/31/12 - cthomas - This will reset the iterator used to traverse the info 
	// list looking for stuff to delete. We reset it here, at a higher level, so it
	// is only done once per call to load (versus once per each object being loaded 
	// inside the LoadRequestedObjects() call). Traversing the info list is very 
	// expensive, so this should save us quite a bit on the of time under stressful 
	// loading situations where we are trying to make space for many assets per frame, 
	// while ur streaming memory is full or very near full. 
	// 
	// Doing the reset here in LoadRequestedObjects() covers the normal call to load 
	// we do once per frame, as well as when we call LoadAllRequestedObjects().
	strStreamingEngine::GetInfo().ResetInfoIteratorForDeletingNextLeastUsed();

	// Everybody who became resident last frame had a full frame's worth time to
	// increment their ref counts. Protection is now over, they can be deleted.
	strStreamingEngine::GetInfo().SnapshotLastLoaded();

	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].LoadRequestedObjects();
	}
}

void strStreamingLoaderManager::StartLoadAllRequestedObjects()
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].StartLoadAllRequestedObjects();
	}
}

void strStreamingLoaderManager::CancelRequest(strIndex index)
{
	streamAssert(sysThreadType::IsUpdateThread());
	
	bool bFound = false;

	for (int x=0; x<DEVICE_COUNT; x++)
	{
		bFound |= m_loaders[x].CancelRequest(index);
	}

	if (!bFound)
	{
		if (PARAM_nomtplacement.Get())
		{
			ASSERT_ONLY(strStreamingInfo *info = strStreamingEngine::GetInfo().GetStreamingInfo(index);)
			streamAssertf(bFound, "Cancelled request for %s (%d) but could not locate the request itself", strStreamingEngine::GetObjectName(index), info->GetStatus());
		}
		streamWarningf("CancelRequest Failed to Find Index %d - %s", index.Get(), strStreamingEngine::GetObjectName(index));
	}
	streamDisplayf("CancelRequest Index %d - %s", index.Get(), strStreamingEngine::GetObjectName(index));
}

void strStreamingLoaderManager::Update()
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].Update();
	}
}

void strStreamingLoaderManager::Flush()
{
	bool hasMoreWork = false;
#if !__NO_OUTPUT
	int waited = 0;
#endif // !__NO_OUTPUT
	do 
	{
		hasMoreWork = false;

		for (int x=0; x<DEVICE_COUNT; x++)
		{
			hasMoreWork |= m_loaders[x].Flush();
		}

		// Give the placement thread some time to do work
		if (hasMoreWork)
		{
			StreamLoadSleep();
#if !__NO_OUTPUT
				if ((waited += 10) == 1000)
					streamWarningf("strStreamingLoaderManager::Flush - waiting for pending operations");
#endif // !__NO_OUTPUT
		}

	} while (hasMoreWork);
}


void strStreamingLoaderManager::SetKeepAliveCallback(void (*callback)(), u32 intervalInMS)
{
	streamAssertf(callback, "callback is null");
	streamAssertf(intervalInMS > 0, "interval needs to be greater than 0");

	m_callback         = callback;
	m_callbackInterval = intervalInMS; 
}

static sysTimer s_callbackTimer;

void strStreamingLoaderManager::CallKeepAliveCallback()
{
	// call the specified callback every specified time interval
	if(m_callback)
	{
		(*m_callback)();
		s_callbackTimer.Reset();
	}
}

void strStreamingLoaderManager::CallKeepAliveCallbackIfNecessary()
{
	if(s_callbackTimer.GetTime() > 0.5f)
		CallKeepAliveCallback();
}


s32 strStreamingLoaderManager::GetLoadingObjectCount()
{
	s32 result = 0;

	for (int x=0; x<DEVICE_COUNT; x++)
	{
		result += m_loaders[x].GetLoadingObjectCount();
	}

	return result;
}

strIndex strStreamingLoaderManager::GetLoadingObject(s32 index)
{
	strIndex result;

	for (int x=0; x<DEVICE_COUNT; x++)
	{
		streamAssert(index >= 0);
		int localCount = m_loaders[x].GetLoadingObjectCount();
		if (index < localCount)
		{
			return m_loaders[x].GetLoadingObject(index);
		}

		index -= localCount;
	}

	return strIndex();
}

#if __ASSERT
void strStreamingLoaderManager::SanityCheck()
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].SanityCheck();
	}
}

void strStreamingLoaderManager::DumpLoadingState()
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].DumpLoadingState();
	}
}
#endif

void strStreamingLoaderManager::GetLoadingFileSizes(size_t& virtualSize, size_t& physicalSize)
{
	size_t virtSize = 0, physSize = 0;

	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].GetLoadingFileSizes(virtSize, physSize);
		virtualSize += virtSize;
		physicalSize += physSize;
	}
}

void strStreamingLoaderManager::ResourcePlacementThread(void* data)
{
	((strStreamingLoaderManager *) data)->ResourcePlacementFunc();
}

void strStreamingLoaderManager::ResourcePlacementFunc()
{
	//Cache off for easier access
	strStreamingInfoManager& rInfoManager = strStreamingEngine::GetInfo();

	m_AsyncPlaceQueue = rage_new sysMessageQueue<strPlacementRequest, LOAD_SLOTS_PER_DEVICE * pgStreamer::DEVICE_COUNT>();
	
#if RESOURCE_HEADER	
	void* pBuffer = Alloca(u8, g_rscVirtualLeafSize);
#endif

#if USE_RESOURCE_CACHE
	// RAGETRACE_INITTHREAD("ResourcePlacementThread", 256, 1);
	PF_INIT_TIMEBARS("ResourcePlacementThread", 128, 0.0f);
	grcResourceCache::SetPlacementThreadId(sysIpcGetCurrentThreadId());
#endif

	while (true)
	{
		//TELEMETRY_ONLY(static sysIpcThreadId ThreadId = sysIpcGetThreadId();)
		//TELEMETRY_SET_THREAD_NAME(ThreadId, "ResourcePlacementThread");

		strPlacementRequest request = m_AsyncPlaceQueue->Pop();		
		DURANGO_ONLY(PF_PUSH_MARKERC(0xff000000,"ResourcePlacementThread"));

		strStreamingLoader::StreamingFile *file = request.m_Info;
		if (!file)
		{
			// A NULL pointer indicates that we're supposed to quit.
			break;
		}

		strIndex index = file->m_Index;
		strStreamingModule* pModule = rInfoManager.GetModule(index);
		strLocalIndex objIndex = pModule->GetObjectIndex(index);

#if !__NO_OUTPUT
		// Don't use the single-argument GetObjectName() version from a worker thread, that's rude.
		char debugName[RAGE_MAX_PATH];
		streamDebugf3("Async placement of %s", strStreamingEngine::GetInfo().GetObjectName(index, debugName, sizeof(debugName)));
#endif // !__NO_OUTPUT

#if USE_DEFRAGMENTATION
		strStreamingEngine::GetDefragmentation()->LockDefragCs();
#endif // USE_DEFRAGMENTATION

#if TRACK_PLACE_STATS
		// We COULD track the place stats, but being on a separate thread, do we want to?
		// pModule->StartTrackingPlace();
#endif // TRACK_PLACE_STATS

#if RESOURCE_HEADER
	datResourceMap& map = request.m_Map;

	if (map.IsDictionary() && map.CanOptimize())
	{
		// Copy		
		sysMemCpy(pBuffer, map.VirtualBase, map.Chunks[0].Size);

		// In-place fixup the asset
		datResource rsc(map, "", false);
		pgDictionary<grcTexture>* pDictionary = (pgDictionary<grcTexture>*) map.VirtualBase;
		rage_placement_new (pDictionary) pgDictionary<grcTexture>(rsc);

		// Figure out the true header size
		void* pLast = pDictionary->GetLastTexturePointer();
		pDictionary->ForAll(ResourceHeader_GetLastTexturePointer, &pLast);
		
		// HACK: Add 1 and round up so that the std::lower_bound (SearchByAddress) doesn't make mistakes		
		//const size_t size = !(actual & ~0xF) ? (actual + 0x10) : ((actual + 0xF) & ~0xF);
		const size_t actual = reinterpret_cast<size_t>(pLast) - reinterpret_cast<size_t>(map.VirtualBase);
		const size_t size = (actual + g_rscHeaderLeafSize - 1) & ~(g_rscHeaderLeafSize - 1);

		// Remove the pgHandleBase or we will crash!
		pDictionary->ForAll(ResourceHeader_UnregisterTexture, NULL);
		pgHandleBase::Unregister(pDictionary);

		if (size <= g_rscVirtualLeafSize)
		{
			sysMemAllocator* pHeader = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_HEADER_VIRTUAL);
			void* ptr = pHeader->Allocate(size, g_rscHeaderLeafAlignment);

			if (ptr)
			{
				sysMemAllocator* pStreaming = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
				const u32 data = pStreaming->GetUserData(map.Chunks[0].DestAddr);				
				sysMemManager::GetInstance().SetUserData(ptr, data);

				MEM_USE_USERDATA(data);
				strStreamingEngine::GetAllocator().StreamingFree(map.Chunks[0].DestAddr);

				map.Chunks[0].DestAddr = ptr;
				map.Chunks[0].Size = size;
				map.VirtualBase = ptr;

				file->m_LoadingMap.Chunks[0].DestAddr = ptr;
				file->m_LoadingMap.Chunks[0].Size = size;
				file->m_LoadingMap.VirtualBase = ptr;

				file->m_AllocationMap.Chunks[0].DestAddr = ptr;
				file->m_AllocationMap.Chunks[0].Size = size;
				file->m_AllocationMap.VirtualBase = ptr;
			}					
		}

		sysMemCpy(map.VirtualBase, pBuffer, map.Chunks[0].Size);
	}
#endif
	
	pModule->PlaceResource(objIndex, request.m_Map, request.m_ResInfo);

#if RESOURCE_HEADER
	if (map.IsDictionary() && map.IsOptimized())
	{
		const u32 data = index.Get();
		pgDictionary<grcTexture>* pDictionary = (pgDictionary<grcTexture>*) map.VirtualBase;			
		pDictionary->SetUserData(data);
	}
#endif

		// Release ownership of thread for dictionary management
 		// pgDictionaryBase::SetListOwnerThread(sysIpcCurrentThreadIdInvalid);

#if USE_DEFRAGMENTATION
		strStreamingEngine::GetDefragmentation()->UnlockDefragCs();
#endif // USE_DEFRAGMENTATION

#if TRACK_PLACE_STATS
		//strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file->m_Index);
		//pModule->StopTrackingPlace(pModule->GetName(objIndex), pInfo->ComputeVirtualSize(false) + pInfo->ComputePhysicalSize(false));
#endif // TRACK_PLACE_STATS

		file->m_IsPlaced = (u32) true;

		DURANGO_ONLY(PF_POP_MARKER());
	}
}

void strStreamingLoaderManager::RequestAsyncPlacement(strStreamingLoader::StreamingFile &file, const datResourceMap &map, const datResourceInfo &resInfo)
{
	strPlacementRequest placeRequest;

	placeRequest.m_Info = &file;
	placeRequest.m_Map = map;
	placeRequest.m_ResInfo = resInfo;

	strStreamingInfo* pInfo = strStreamingEngine::GetInfo().GetStreamingInfo(file.m_Index);
	streamAssertf(pInfo->GetStatus() == STRINFO_LOADING, "Info status is %d", pInfo->GetStatus());
	pInfo->SetFlag(file.m_Index, STRFLAG_INTERNAL_PLACING);

	if(m_DeferAsyncPlacementRequests)
	{
		m_DeferredPlacementRequests.PushAndGrow(placeRequest);
	}
	else
	{
		m_AsyncPlaceQueue->Push(placeRequest);
	}
}

void strStreamingLoaderManager::SubmitDeferredAsyncPlacementRequests()
{
	int count = m_DeferredPlacementRequests.GetCount();
	for(int i = 0; i < count; i++)
	{
		m_AsyncPlaceQueue->Push(m_DeferredPlacementRequests[i]);
	}
	m_DeferredPlacementRequests.ResetCount();
}

//
// Debug output
//
#if __BANK

void strStreamingLoader::ShowFileStatus() const
{
	grcDebugDraw::AddDebugOutputEx(false, "%s LOADER STATUS", GetLoaderName());

	for (int x=0; x<m_StreamingFiles.GetMaxCount(); x++)
	{
		const StreamingFile &file = m_StreamingFiles[x];
		const char *subStatus = "    ";

		if (file.m_Index.IsValid())
		{
			if (strStreamingEngine::GetInfo().GetStreamingInfoRef(file.m_Index).GetFlags() & STRFLAG_INTERNAL_PLACING)
			{
				subStatus = "(A) ";
			}
		}

		grcDebugDraw::AddDebugOutputEx(false, "%2d %15s %s %s", x,
			StreamingFile::sm_StatusStrings[file.m_Status], subStatus,
			strStreamingEngine::GetObjectName(file.m_Index));
	}
}

void strStreamingLoader::ShowAsyncStats() const
{
	grcDebugDraw::AddDebugOutputEx(false, "%s ASYNC STATS", GetLoaderName());
	grcDebugDraw::AddDebugOutputEx(false, "Pending: %d", m_AsyncPlacements.GetCount());
	grcDebugDraw::AddDebugOutputEx(false, "Long placements: %d, missed requests due to slot occupancy: %d", m_LongMtPlacements, m_MissedIdleSlots);

	for (int x=0; x<m_AsyncPlacements.GetCount(); x++)
	{
		const StreamingFile &file = *m_AsyncPlacements[x];
		grcDebugDraw::AddDebugOutputEx(false, "%s", strStreamingEngine::GetObjectName(file.m_Index));
	}
}

void strStreamingLoaderManager::ShowAsyncStats() const
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].ShowAsyncStats();
	}
}

void strStreamingLoaderManager::ShowFileStatus() const
{
	for (int x=0; x<DEVICE_COUNT; x++)
	{
		m_loaders[x].ShowFileStatus();
	}
}

#endif // __BANK

strIndex strStreamingLoader::GetStrIndexFromHandle(const pgStreamer::Handle & handle) const
{
	for (int i = 0; i < m_StreamingQueue.GetCount(); ++i)
	{
		StreamingFile& file = *m_StreamingQueue[i];
		if(file.m_Handle == handle)
		{
			return file.m_Index;
		}
	}
	return strIndex(strIndex::INVALID_INDEX);
}

u32 strStreamingLoaderManager::GetStrIndexFromHandle(const pgStreamer::Handle & handle) const
{
	for (int device=0; device<DEVICE_COUNT; device++)
	{
		strIndex temp = m_loaders[device].GetStrIndexFromHandle(handle);
		if(!temp.IsInvalid())
		{
			return temp.Get();
		}
		
	}
	return 0;
}

u32 GetStrIndexFromHandle(const pgStreamer::Handle & handle)
{
	if(handle == 0)
		return 0;
	return strStreamingEngine::GetLoader().GetStrIndexFromHandle(handle);
}


}		// namespace rage
