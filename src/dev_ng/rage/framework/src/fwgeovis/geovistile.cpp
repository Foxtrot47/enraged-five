#include "geovistile.h"

bool SaveTile(const gvTile& tile_, const char* path)
{
	FILE* fp = fopen(path, "wb");

	if (fp)
	{
		gvTile tile = tile_;

		// NULL out any masks which are empty
		for (int n = 0; n < NELEM(tile.m_masks); n++)
		{
			if (tile.m_masks[n])
			{
				u8 bits = 0;

				for (int i = 0; i < (tile.m_maskNumCellsX*tile.m_maskNumCellsY + 7)/8; i++)
				{
					bits |= tile.GetMask((gvMask)n)[i];
				}

				if (bits == 0)
				{
					tile.m_masks[n] = NULL;
				}
			}
		}

		fwrite(&tile, sizeof(tile), 1, fp);

		#define SAVE_MAPS(maps,celltype)\
		for (int n = 0; n < NELEM(maps); n++) \
		{ \
			if (maps[n]) \
			{ \
				fwrite(maps[n], sizeof(celltype), tile.m_numCellsX*tile.m_numCellsY, fp); \
			} \
		}

		SAVE_MAPS(tile.m_heightMaps, float);
		SAVE_MAPS(tile.m_densityMaps_u8, u8);
		SAVE_MAPS(tile.m_densityMaps_fp, float);
		SAVE_MAPS(tile.m_hashIDMaps, u32);

		#undef SAVE_MAPS

		const int maskDownsampleX = tile.m_maskNumCellsX/tile.m_numCellsX;
		const int maskDownsampleY = tile.m_maskNumCellsY/tile.m_numCellsY;

		if (maskDownsampleX == 1 &&
			maskDownsampleY == 1)
		{
			for (int n = 0; n < NELEM(tile.m_masks); n++)
			{
				if (tile.m_masks[n])
				{
					fwrite(tile.m_masks[n], sizeof(u8), (tile.m_numCellsX*tile.m_numCellsY + 7)/8, fp);
				}
			}
		}
		else
		{
			u8* maskImage = new u8[tile.m_numCellsX*tile.m_numCellsY];

			for (int n = 0; n < NELEM(tile.m_masks); n++)
			{
				if (tile.m_masks[n])
				{
					for (int j = 0; j < tile.m_numCellsY; j++)
					{
						for (int i = 0; i < tile.m_numCellsX; i++)
						{
							int sum = 0;

							for (int jj = 0; jj < maskDownsampleY; jj++)
							{
								for (int ii = 0; ii < maskDownsampleX; ii++)
								{
									const int index = (i*maskDownsampleX + ii) + (j*maskDownsampleY + jj)*tile.m_maskNumCellsX;

									if (tile.GetMask((gvMask)n)[index/8] & BIT(index%8))
									{
										sum++;
									}
								}
							}

							maskImage[i + j*tile.m_numCellsX] = (u8)(0.5f + 255.0f*(float)sum/(float)(maskDownsampleX*maskDownsampleY));
						}
					}

					fwrite(maskImage, sizeof(u8), tile.m_numCellsX*tile.m_numCellsY, fp);
				}
			}

			delete[] maskImage;
		}

		fclose(fp);
		return true;
	}
	
	return false;
}

bool LoadTile(gvTile& tile, const char* path)
{
	FILE* fp = fopen(path, "rb");

	if (fp)
	{
		fread(&tile, sizeof(tile), 1, fp);

		#define LOAD_MAPS(maps,celltype)\
		for (int n = 0; n < NELEM(maps); n++) \
		{ \
			if (maps[n]) \
			{ \
				maps[n] = new celltype[tile.m_numCellsX*tile.m_numCellsY]; \
				fread(maps[n], sizeof(celltype), tile.m_numCellsX*tile.m_numCellsY, fp); \
			} \
		}

		LOAD_MAPS(tile.m_heightMaps, float);
		LOAD_MAPS(tile.m_densityMaps_u8, u8);
		LOAD_MAPS(tile.m_densityMaps_fp, float);
		LOAD_MAPS(tile.m_hashIDMaps, u32);

		#undef LOAD_MAPS

		const int maskDownsampleX = tile.m_maskNumCellsX/tile.m_numCellsX;
		const int maskDownsampleY = tile.m_maskNumCellsY/tile.m_numCellsY;

		if (maskDownsampleX == 1 &&
			maskDownsampleY == 1)
		{
			u8* maskBits = new u8[(tile.m_numCellsX*tile.m_numCellsY + 7)/8];

			for (int n = 0; n < NELEM(tile.m_masks); n++)
			{
				if (tile.m_masks[n])
				{
					tile.m_masks[n] = NULL;
					tile.m_maskImages[n] = new u8[tile.m_numCellsX*tile.m_numCellsY];
					fread(maskBits, sizeof(u8), (tile.m_numCellsX*tile.m_numCellsY + 7)/8, fp);

					for (int i = 0; i < tile.m_numCellsX*tile.m_numCellsY; i++)
					{
						tile.GetMaskImage((gvMask)n)[i] = (maskBits[i/8] & BIT(i%8)) ? 255 : 0;
					}
				}
			}

			delete[] maskBits;
		}
		else
		{
			for (int n = 0; n < NELEM(tile.m_masks); n++)
			{
				if (tile.m_masks[n])
				{
					tile.m_masks[n] = NULL;
					tile.m_maskImages[n] = new u8[tile.m_numCellsX*tile.m_numCellsY];
					fread(tile.m_maskImages[n], sizeof(u8), tile.m_numCellsX*tile.m_numCellsY, fp);
				}
			}
		}

		fclose(fp);
		return true;
	}

	return false;
}

void RenderTriangle(gvTile& tile, const gvPrim& prim, Vec3V_In p0, Vec3V_In p1, Vec3V_In p2)
{
	const Vec3V pmin = Min(p0, p1, p2);
	const Vec3V pmax = Max(p0, p1, p2);

	const float x0 = pmin.GetXf();
	const float y0 = pmin.GetYf();
	const float x1 = pmax.GetXf();
	const float y1 = pmax.GetYf();

	const bool maskIsDataResolution = (tile.m_maskNumCellsX == tile.m_numCellsX && tile.m_maskNumCellsY == tile.m_numCellsY);

	// render data (and masks, if same resolution)
	{
		const int i0 = Max<int>((int)floorf((x0 - tile.m_worldMinX)/tile.m_cellSizeX), 0);
		const int j0 = Max<int>((int)floorf((y0 - tile.m_worldMinY)/tile.m_cellSizeY), 0);
		const int i1 = Min<int>((int)ceilf ((x1 - tile.m_worldMinX)/tile.m_cellSizeX), tile.m_numCellsX) - 1;
		const int j1 = Min<int>((int)ceilf ((y1 - tile.m_worldMinY)/tile.m_cellSizeY), tile.m_numCellsY) - 1;

		if (i0 == i1 && j0 == j1)
		{
			RenderCellData(tile, prim, i0, j0, pmin.GetZ(), pmax.GetZ());

			if (maskIsDataResolution)
			{
				RenderCellMask(tile, prim, i0, j0);
			}
		}
		else
		{
			for (int j = j0; j <= j1; j++)
			{
				for (int i = i0; i <= i1; i++)
				{
					const float cell_x0 = tile.m_worldMinX + tile.m_cellSizeX*(float)(i + 0);
					const float cell_y0 = tile.m_worldMinY + tile.m_cellSizeY*(float)(j + 0);
					const float cell_x1 = tile.m_worldMinX + tile.m_cellSizeX*(float)(i + 1);
					const float cell_y1 = tile.m_worldMinY + tile.m_cellSizeY*(float)(j + 1);

					int   count = 3;
					Vec3V temp0[3 + 4] = {p0, p1, p2};
					Vec3V temp1[3 + 4];

					// clip to four planes
					count = PolyClip(temp1, NELEM(temp1), temp0, count, BuildPlane(Vec3V(cell_x0, cell_y0, 0.0f), +Vec3V(V_X_AXIS_WZERO)));
					count = PolyClip(temp0, NELEM(temp0), temp1, count, BuildPlane(Vec3V(cell_x0, cell_y0, 0.0f), +Vec3V(V_Y_AXIS_WZERO)));
					count = PolyClip(temp1, NELEM(temp1), temp0, count, BuildPlane(Vec3V(cell_x1, cell_y1, 0.0f), -Vec3V(V_X_AXIS_WZERO)));
					count = PolyClip(temp0, NELEM(temp0), temp1, count, BuildPlane(Vec3V(cell_x1, cell_y1, 0.0f), -Vec3V(V_Y_AXIS_WZERO)));

					if (count > 0)
					{
						ScalarV zmin = temp0[0].GetZ();
						ScalarV zmax = zmin;

						for (int k = 1; k < count; k++)
						{
							const ScalarV z = temp0[k].GetZ();

							zmin = Min(z, zmin);
							zmax = Max(z, zmax);
						}

						zmin = Clamp(zmin, pmin.GetZ(), pmax.GetZ()); // clip to triangle bounds just to be safe
						zmax = Clamp(zmax, pmin.GetZ(), pmax.GetZ()); // clip to triangle bounds just to be safe

						RenderCellData(tile, prim, i, j, zmin, zmax);

						if (maskIsDataResolution)
						{
							RenderCellMask(tile, prim, i, j);
						}
					}
				}
			}
		}
	}

	if (!maskIsDataResolution)
	{
		const int i0 = Max<int>((int)floorf((x0 - tile.m_worldMinX)/tile.m_maskCellSizeX), 0);
		const int j0 = Max<int>((int)floorf((y0 - tile.m_worldMinY)/tile.m_maskCellSizeY), 0);
		const int i1 = Min<int>((int)ceilf ((x1 - tile.m_worldMinX)/tile.m_maskCellSizeX), tile.m_maskNumCellsX) - 1;
		const int j1 = Min<int>((int)ceilf ((y1 - tile.m_worldMinY)/tile.m_maskCellSizeY), tile.m_maskNumCellsY) - 1;

		if (i0 == i1 && j0 == j1)
		{
			RenderCellMask(tile, prim, i0, j0);
		}
		else
		{
			for (int j = j0; j <= j1; j++)
			{
				for (int i = i0; i <= i1; i++)
				{
					const float cell_x0 = tile.m_worldMinX + tile.m_maskCellSizeX*(float)(i + 0);
					const float cell_y0 = tile.m_worldMinY + tile.m_maskCellSizeY*(float)(j + 0);
					const float cell_x1 = tile.m_worldMinX + tile.m_maskCellSizeX*(float)(i + 1);
					const float cell_y1 = tile.m_worldMinY + tile.m_maskCellSizeY*(float)(j + 1);

					int   count = 3;
					Vec3V temp0[3 + 4] = {p0, p1, p2};
					Vec3V temp1[3 + 4];

					// clip to four planes
					count = PolyClip(temp1, NELEM(temp1), temp0, count, BuildPlane(Vec3V(cell_x0, cell_y0, 0.0f), +Vec3V(V_X_AXIS_WZERO)));
					count = PolyClip(temp0, NELEM(temp0), temp1, count, BuildPlane(Vec3V(cell_x0, cell_y0, 0.0f), +Vec3V(V_Y_AXIS_WZERO)));
					count = PolyClip(temp1, NELEM(temp1), temp0, count, BuildPlane(Vec3V(cell_x1, cell_y1, 0.0f), -Vec3V(V_X_AXIS_WZERO)));
					count = PolyClip(temp0, NELEM(temp0), temp1, count, BuildPlane(Vec3V(cell_x1, cell_y1, 0.0f), -Vec3V(V_Y_AXIS_WZERO)));

					if (count > 0)
					{
						RenderCellMask(tile, prim, i, j);
					}
				}
			}
		}
	}
}

void RenderCellData(gvTile& tile, const gvPrim& prim, int i, int j, ScalarV_In zmin, ScalarV_In zmax)
{
	
}

void RenderCellMask(gvTile& tile, const gvPrim& prim, int i, int j)
{
	for (int n = 0; n < GV_MASK_COUNT; n++)
	{
		const gvMask type = (gvMask)n;

		if (tile.GetMask(type) && (prim.m_mask & BIT(n)))
		{
			tile.SetMaskBit(type, i, j);
		}
	}
}
