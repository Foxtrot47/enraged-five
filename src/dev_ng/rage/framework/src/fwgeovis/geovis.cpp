#include "geovis.h"
#include <stdio.h> // required for ragebuilder too?

#if defined(_OFFLINETOOL)
	#define sprintf sprintf_s
#endif

const char* gv::GetTileNameFromCoords(int i, int j)
{
	static char temp[8];
#if 0&&HACK_RDR3
	i /= WORLD_CELLS_PER_TILE;
	j /= WORLD_CELLS_PER_TILE;
	const int ii = (i&1);
	const int jj = (j&1)^1;
	sprintf(temp, "%c%02d_%c", 'u' - j/2, 2 + i/2, 'a' + ii + jj*2);
#else // GTA
	sprintf(temp, "%03d_%03d", i, j);
#endif
	return temp;
}
