#ifndef _GEOVISPRIM_H_
#define _GEOVISPRIM_H_

#include "geovis.h"

enum gvMask
{
	GV_MASK_WORLD,
	GV_MASK_WATER,
	GV_MASK_STAIR,
	GV_MASK_STAIRSLOPE,
	GV_MASK_SCRIPT,
	GV_MASK_MOVERBOUNDS,
	GV_MASK_WEAPONBOUNDS,
	GV_MASK_VEHICLEBOUNDS,
	GV_MASK_MOVERNOVEHICLEBOUNDS_M,
	GV_MASK_MOVERNOVEHICLEBOUNDS_P,
	GV_MASK_BOUNDTYPE_BOX,
	GV_MASK_BOUNDTYPE_CYLINDER,
	GV_MASK_BOUNDTYPE_CAPSULE,
	GV_MASK_BOUNDTYPE_SPHERE,
	GV_MASK_BOUNDTYPE_NONBVHPRIM,
	GV_MASK_INTERIOR,
	GV_MASK_EXTERIOR_PORTAL,
	GV_MASK_WATER_COLLISION,
	GV_MASK_WATER_OCCLUDER,
	GV_MASK_WATER_DRAWABLE,
	GV_MASK_WATER_OCEAN,
	GV_MASK_WATER_OCEAN_GEOMETRY,
	GV_MASK_WATER_NO_REFLECTION,
	GV_MASK_WATER_PORTAL,
	GV_MASK_MIRROR_DRAWABLE,
	GV_MASK_MIRROR_PORTAL,
	GV_MASK_CABLE,
	GV_MASK_CORONA,
	GV_MASK_ANIMATED_BUILDING,
	GV_MASK_COUNT
};
CompileTimeAssert(GV_MASK_COUNT <= 32);

class gvPrim
{
public:
	u32 m_mask;
};

#endif // GEOVISPRIM_H
