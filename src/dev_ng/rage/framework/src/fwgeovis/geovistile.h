#ifndef _GEOVISTILE_H_
#define _GEOVISTILE_H_

#include "geovis.h"
#include "geovisprim.h" // for GV_MASK_COUNT

enum gvHeightMap
{
	GV_HEIGHTMAP_COLLISION_MAX,
	GV_HEIGHTMAP_COLLISION_MIN,
	GV_HEIGHTMAP_WATER,
	GV_HEIGHTMAP_COUNT
};

enum gvDensityMap
{
	GV_DENSITYMAP_COLLISION_MOVER_POLY,
	GV_DENSITYMAP_COLLISION_MOVER_PRIM,
	GV_DENSITYMAP_COLLISION_WEAPON_POLY,
	GV_DENSITYMAP_COLLISION_WEAPON_PRIM,
	GV_DENSITYMAP_PED,
	GV_DENSITYMAP_LIGHT,
	GV_DENSITYMAP_COUNT
};

enum gvFloatDensityMap
{
	GV_FLOATDENSITYMAP_COLLISION,
	GV_FLOATDENSITYMAP_COUNT
};

enum gvHashIDMap
{
	GV_HASHIDMAP_COLLISION_BOUND,
	GV_HASHIDMAP_COLLISION_RPF,
	GV_HASHIDMAP_COUNT
};

class gvTile
{
public:
	float m_worldMinX;
	float m_worldMinY;
	float m_worldMaxX;
	float m_worldMaxY;
	float m_cellSizeX;
	float m_cellSizeY;
	int   m_numCellsX;
	int   m_numCellsY;
	float m_maskCellSizeX;
	float m_maskCellSizeY;
	int   m_maskNumCellsX;
	int   m_maskNumCellsY;

	float* m_heightMaps[GV_HEIGHTMAP_COUNT];
	u8*    m_densityMaps_u8[GV_DENSITYMAP_COUNT];
	float* m_densityMaps_fp[GV_FLOATDENSITYMAP_COUNT];
	u32*   m_hashIDMaps[GV_HASHIDMAP_COUNT];
	u8*    m_masks[GV_MASK_COUNT];
	u8*    m_maskImages[GV_MASK_COUNT]; // masks converted to 8-bit greyscale

	// materials and propgroups
	u64*  m_materialsMask;
	u16*  m_matProcIdMask;
	int   m_matProcMaskCount;
	u64** m_matProcMasks;
	u16*  m_propGroupMaskMap;

	gvTile()
	{
		memset(this, 0, sizeof(*this));
	}

	~gvTile()
	{
		#define SAFE_DELETE_MAPS(maps) for (int n = 0; n < NELEM(maps); n++) { if (maps[n]) { delete[] maps[n]; }}
		SAFE_DELETE_MAPS(m_heightMaps);
		SAFE_DELETE_MAPS(m_densityMaps_u8);
		SAFE_DELETE_MAPS(m_densityMaps_fp);
		SAFE_DELETE_MAPS(m_hashIDMaps);
		SAFE_DELETE_MAPS(m_masks);
		#undef SAFE_DELETE_MAPS
	}

	u8* GetMask(gvMask maskType)
	{
		return m_masks[maskType];
	}

	const u8* GetMask(gvMask maskType) const
	{
		return m_masks[maskType];
	}

	u8* GetMaskImage(gvMask maskType)
	{
		return m_maskImages[maskType];
	}

	const u8* GetMaskImage(gvMask maskType) const
	{
		return m_maskImages[maskType];
	}

	void SetMaskBit(gvMask maskType, int i, int j)
	{
		const int index = i + j*m_maskNumCellsX;
		GetMask(maskType)[index/8] |= BIT(index%8);
	}
};

void InitTile(gvTile& tile, int i, int j);
bool SaveTile(const gvTile& tile, const char* path);
bool LoadTile(gvTile& tile, const char* path);

void RenderTriangle(gvTile& tile, const gvPrim& prim, Vec3V_In p0, Vec3V_In p1, Vec3V_In p2);
void RenderCellData(gvTile& tile, const gvPrim& prim, int i, int j, ScalarV_In zmin, ScalarV_In zmax);
void RenderCellMask(gvTile& tile, const gvPrim& prim, int i, int j);

#endif // GEOVISTILE_H
