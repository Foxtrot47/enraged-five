//
//
//    Filename: AssetStore.h
//     Creator: Adam Fowler
// Description: Template class for managing the storage of assets. The class interfaces with the streaming system
//				so as long as you implement the loading function you can get streaming for free
//
//
#ifndef INC_ASSET_STORE_H_
#define INC_ASSET_STORE_H_

#if !__SPU
// Rage headers
#include "atl/atfunctor.h"
#include "diag/output.h"
#include "file\device.h"
#include "paging/rscbuilder.h"
#include "string\stringhash.h"
#include "system\platform.h"
#include "system/stack.h"
#include "system/threadtype.h"

// Game headers
#include "fwtl\nameregistrar.h"
#include "fwtl\pool.h"
#include "streaming/streamingallocator.h"
#include "streaming/streamingengine.h"
#include "streaming\streamingmodule.h"

#define ENABLE_STORETRACKING (!__FINAL && !__PROFILE && !__SPU)

#define DETAILED_MODULE_REF_COUNT (__DEV)

#if DETAILED_MODULE_REF_COUNT
	#define DETAILED_REF_ONLY(_x)	_x

	// Set this to 1 to trace the ref count history of a certain asset
	#define TRACE_REFCOUNT	(1 && __DEV)
#else
	#define DETAILED_REF_ONLY(_x)	
#endif


namespace rage {

XPARAM(trackrefcount);

typedef atFunctor4<pgBase*,datResourceInfo&,datResourceMap&, const char *, bool> fwPlaceFunctor;
typedef atFunctor1<pgBase*,datResourceMap&> fwDefragmentFunctor;

extern int g_TrackRefCountKindBitmask;

//
// name:		Definition objects
// description:	Classes containing pointer to object and other stuff you might want to include alongside it
template<typename T, typename S> class fwGenericAssetDef {
public:
	typedef S		strObjectNameType;

	void Init(const S name);

	T* m_pObject;
#if DETAILED_MODULE_REF_COUNT
	union {
		struct {
			u64 m_refCounts0 : REF_RENDER_BITS;	// Rendering
			u64 m_refCounts1 : REF_SCRIPT_BITS;	// Script
			//u64 m_refCounts2 : REF_DEFRAG_BITS;	// Defrag
			u64 m_refCounts3 : REF_OTHER_BITS;	// Other
		} m_refs;

		u64 m_refsUnified;
	};

	int GetSubRefCount(strRefKind refKind)	const {
		switch (refKind) {
		case REF_RENDER:
			return m_refs.m_refCounts0;
		case REF_SCRIPT:
			return m_refs.m_refCounts1;
		//case REF_DEFRAG:
			//return m_refs.m_refCounts2;
		default:
			return m_refs.m_refCounts3;
		}
	}

	void SetSubRefCount(strRefKind refKind, int refCount) {
		switch (refKind) {
		case REF_RENDER:
			m_refs.m_refCounts0 = (u64) refCount;
			break;
		case REF_SCRIPT:
			m_refs.m_refCounts1 = (u64) refCount;
			break;
		//case REF_DEFRAG:
			//m_refs.m_refCounts2 = (u32) refCount;
			//break;
		default:
			m_refs.m_refCounts3 = (u64) refCount;
		}

#if TRACE_REFCOUNT
		const char *trackRefCountName;
		if (PARAM_trackrefcount.Get(trackRefCountName) && ((1 << refKind) & g_TrackRefCountKindBitmask) && m_name.GetCStr() && strstr(m_name.GetCStr(), trackRefCountName))
		{
			Displayf("Trace history: SET: Ref count kind %d for %s set to %d (%p)", refKind, m_name.GetCStr(),
				refCount, this);
			sysStack::PrintStackTrace();
		}
#endif // TRACE_REFCOUNT

	}
#else // DETAILED_MODULE_REF_COUNT
	int m_refCount;
#endif // DETAILED_MODULE_REF_COUNT
	S m_name;
};

template <typename T>
class fwAssetDef : public fwGenericAssetDef< T, strStreamingObjectName > {
};

template <typename T>
class fwAssetNameDef : public fwGenericAssetDef< T, strStreamingObjectNameString > {
};

class fwAssetStoreBase : public strStreamingModule
{
protected:
	fwAssetStoreBase(const char* pModuleName, 
		int moduleTypeIndex, 
		int size,
		u32 elementsPerPage,
		bool requiresTempMemory,
		bool canDefragment = false,
		int rscVersion = 0) : strStreamingModule(pModuleName, moduleTypeIndex, size, requiresTempMemory, canDefragment, rscVersion, elementsPerPage) {}

public:
	virtual s32 GetSize() const {return 0;}
	virtual s32 GetNumUsedSlots() const {return 0;}

#if !__FINAL
	virtual void MarkDirty(strLocalIndex index);
	bool IsDirty(strLocalIndex index) const;
	void ClearDirtyFlag(strLocalIndex index);

	static void InitClass();
	static const char *GetProgressText()		{ return sm_ProgressText; }
#endif // !__FINAL

#if USE_PAGED_POOLS_FOR_STREAMING
	static void *sm_AssetStorePages[STREAMING_INFO_MAX_PAGE_COUNT];
#endif // USE_PAGED_POOLS_FOR_STREAMING

#if !__FINAL
protected:
	static char sm_ProgressText[256];
#endif // !__FINAL
};

//
// name:		fwAssetStore
// description:	Class storing objects and keeping count of references to them. Adds interfaces
//				with streaming.
//				T = streamable object
//				S = definition class which includes point to object and other runtime information
template<typename T, typename S = fwAssetDef<T> > class fwAssetStore : public fwAssetStoreBase
{
public:
#if !USE_PAGED_POOLS_FOR_STREAMING
	typedef fwPool<S> Pool;
#endif // !USE_PAGED_POOLS_FOR_STREAMING

	fwAssetStore(const char* pModuleName, 
		int moduleTypeIndex, 
		int size,
		u32 elementsPerPage,
		bool requiresTempMemory,
		bool canDefragment = false,
		int rscVersion = 0);
	virtual ~fwAssetStore();

	void FinalizeSize();
	void RegisterStreamingModule();
	strStreamingModule* GetStreamingModule() {return this;}
	virtual void Shutdown();

	// These are the functions from strStreamingModule that need overriding
#if !__FINAL || __FINAL_LOGGING
	virtual const char* GetName(strLocalIndex index) const;
	const char* GetName(int index) const {return GetName(strLocalIndex(index));}
#endif
	u32 GetHash(strLocalIndex index) const;
	virtual void* GetPtr(strLocalIndex index);
	virtual strLocalIndex Register(const char* name);
	virtual void Remove(strLocalIndex index);
	virtual bool Load(strLocalIndex index, void* pData, int size);
	// reference counting
	virtual void AddRef(strLocalIndex index, strRefKind strRefKind);
	virtual void RemoveRef(strLocalIndex index, strRefKind strRefKind);
	virtual void RemoveRefWithoutDelete(strLocalIndex index, strRefKind strRefKind);
	virtual void ResetAllRefs(strLocalIndex index);
	virtual int GetNumRefs(strLocalIndex index) const;

#if DETAILED_MODULE_REF_COUNT
	virtual const char *GetRefCountString(strLocalIndex index, char *buffer, size_t bufferLen) const;
#endif // DETAILED_MODULE_REF_COUNT

#if USE_PAGED_POOLS_FOR_STREAMING
	virtual void RegisterPagedPool(void *data, u32 page, u32 elementCount, strStreamingInfoPageHeader &header, size_t *memUsage);
	virtual void FreePagedPool(u32 page, u32 elementCount, strStreamingInfoPageHeader &header, size_t *memUsage);
	virtual size_t ComputePageMemUsage(u32 elementCount) const;
	virtual size_t GetModuleMemoryRequirement(u32 elementCount) const			{ return sizeof(S) * elementCount; }
#endif // USE_PAGED_POOLS_FOR_STREAMING

	virtual bool LoadFile(strLocalIndex index, const char* pFilename);

	// access functions
	int GetSize() const;
	int GetNumUsedSlots() const;
#if !__NO_OUTPUT
	int GetPeakSlotsUsed() const;
	size_t GetStorageSize() const;
	int GetNoOfFreeSpaces() const;
#endif

	T* Get(strLocalIndex index);
	T* Get(strLocalIndex index) const;

	T* GetSafeFromIndex(strLocalIndex index);
	T* GetSafeFromName(const typename S::strObjectNameType name);
	virtual void Set(strLocalIndex index, T* m_pObject);

	// add remove slots
	virtual strLocalIndex AddSlot(const typename S::strObjectNameType name);
	virtual strLocalIndex AddSlot(strLocalIndex index, const typename S::strObjectNameType name);
	virtual void RemoveSlot(strLocalIndex index);
	bool IsValidSlot(strLocalIndex index) const;
	strLocalIndex FindSlot(const typename S::strObjectNameType name) const;
	strLocalIndex FindSlot(const char* name) const;
	strLocalIndex FindSlotFromHashKey(const u32 hashKey) const;
	S*	GetSlot(strLocalIndex index);
	const S* GetSlot(strLocalIndex index) const;
	strLocalIndex GetSlotIndex(const S* pDef) const;

	// Helpful functions
	void LoadAll(bool andRemove);
	void LoadAllNew(bool andRemove);
	void RemoveAll(bool bIgnoreRefCount = false);

#if __DEV
	void PrintLoadedObjs();
	void PrintAllObjs();
#endif

/*
#ifdef  STR_LOCAL_IDX
#if !__FINAL
	const char* GetName(int index) const {return GetName(strLocalIndex(index));}
#endif
private:
	// These are the functions from strStreamingModule that need overriding
	u32 GetHash(int index) const {return GetHash(strLocalIndex(index));}
	virtual void* GetPtr(int index) {return GetPtr(strLocalIndex(index));}
	//virtual int Register(const char* name) 
	virtual void Remove(int index) {return Remove(strLocalIndex(index));}
	virtual bool Load(int index, void* pData, int size) {return Load(strLocalIndex(index), pData, size);}
	// reference counting
	virtual void AddRef(int index, strRefKind strRefKind) {return AddRef(strLocalIndex(index), strRefKind);}
	virtual void RemoveRef(int index, strRefKind strRefKind) {return RemoveRef(strLocalIndex(index), strRefKind);}
	virtual void RemoveRefWithoutDelete(int index, strRefKind strRefKind) {return RemoveRefWithoutDelete(strLocalIndex(index), strRefKind);}
	virtual void ResetAllRefs(int index) {return ResetAllRefs(strLocalIndex(index));}
	virtual int GetNumRefs(int index) const {return GetNumRefs(strLocalIndex(index));}

#if DETAILED_MODULE_REF_COUNT
	virtual const char *GetRefCountString(int index, char *buffer, size_t bufferLen) const {return GetRefCountString(strLocalIndex(index), buffer, bufferLen);}
#endif // DETAILED_MODULE_REF_COUNT

	virtual bool LoadFile(int index, const char* pFilename) {return LoadFile(strLocalIndex(index), pFilename);}

	T* Get(int index) {return Get(strLocalIndex(index));}
	T* Get(int index) const {return Get(strLocalIndex(index));}

	T* GetSafeFromIndex(int index) {return GetSafeFromIndex(strLocalIndex(index));}
	virtual void Set(int index, T* m_pObject) {return Set(strLocalIndex(index), m_pObject);}

	// add remove slots
	//virtual int AddSlot(const typename S::strObjectNameType name);
	virtual strLocalIndex AddSlot(int index, const typename S::strObjectNameType name) {return AddSlot(strLocalIndex(index), name);}
	virtual void RemoveSlot(int index) {return RemoveSlot(strLocalIndex(index));}
	bool IsValidSlot(int index) const {return IsValidSlot(strLocalIndex(index));}
	//int FindSlot(const typename S::strObjectNameType name) const;
	//int FindSlot(const char* name) const;
	//int FindSlotFromHashKey(const u32 hashKey) const;
	S*	GetSlot(int index) {return GetSlot(strLocalIndex(index));}
	const S* GetSlot(int index) const {return GetSlot(strLocalIndex(index));}
	//int GetSlotIndex(const S* pDef) const;
public:
#endif //  STR_LOCAL_IDX
*/

//private:
#if !USE_PAGED_POOLS_FOR_STREAMING
	Pool m_pool;
#endif // !USE_PAGED_POOLS_FOR_STREAMING
protected:
	atString m_fileExt;
	fwNameRegistrar m_reg;

	// PURPOSE:	True if the size of this fwAssetStore object has been determined,
	//			including any size overrides from fwConfigManager. Gets set by
	//			the FinalizeSize() function.
	bool m_SizeFinalized;

	void InsertHashKey(u32 key,int idx) { m_reg.Insert(key,idx); }
	void RemoveHashKey(u32 key) { m_reg.Delete(key); }
};

#if ENABLE_STORETRACKING
extern atArray<fwAssetStoreBase*> g_storesTrackingArray;

inline void fwStoreTrackerAdd(fwAssetStoreBase* storeptr)
{
    sysMemStartTemp();
    rage::g_storesTrackingArray.PushAndGrow(storeptr);
    sysMemEndTemp();
}

inline void fwStoreTrackerRemove(fwAssetStoreBase* storeptr)
{
    int storeidx = g_storesTrackingArray.Find(storeptr);
    if( storeidx > -1 )
    {
        sysMemStartTemp();
        g_storesTrackingArray.DeleteFast(storeidx);
        sysMemEndTemp();
    }
}
#endif

//
// name:		fwAssetRscStore
// description:	Version of fwAssetStore that overrides the functions for placing and defragging resources.
//				You should be able to use this class without any override to create an array of slots to 
//				stream a resource type into
template<typename T, typename S = fwAssetDef<T> > class fwAssetRscStore : public fwAssetStore<T,S>
{
public:
	fwAssetRscStore(
		const char* pModuleName, 
		int moduleTypeIndex, 
		int size,
		u32 elementsPerPage,
		bool canDefragment,
		int rscVersion) : fwAssetStore<T,S>(pModuleName, moduleTypeIndex, size, elementsPerPage, false, canDefragment, rscVersion) {}

	virtual bool LoadFile(strLocalIndex index, const char* pFilename);
	virtual void Set(strLocalIndex index, T* m_pObject);
	virtual void Remove(strLocalIndex index);
	virtual void PlaceResource(strLocalIndex index, datResourceMap& map, datResourceInfo& header);
	virtual void SetResource(strLocalIndex index, datResourceMap& map);
	virtual void* Defragment(strLocalIndex index, datResourceMap& map, bool& flush);

	// PURPOSE: Indicate whether or not a resource in this module can be placed asynchronously in a separate thread.
	virtual bool CanPlaceAsynchronously( strLocalIndex UNUSED_PARAM(objIndex) ) const { return true; }
	virtual void PlaceAsynchronously(strLocalIndex objIndex, strStreamingLoader::StreamingFile& file, datResourceInfo& rsc);

	virtual pgBase *GetResource(strLocalIndex index) const;

#if !__FINAL
	void Validate(strLocalIndex index) const
	{
		const u32 userData = strStreamingInfoManager::CreateUserDataFromStreamingIndex(fwAssetStore<T,S>::GetStreamingIndex(index));
		fwAssetStore<T,S>::Get(index)->Validate(userData);
	}
#endif // !__FINAL

};

//
// name:		fwGenericAssetDef<T,S>::Init
// description:	Initialisation function for asset definition object
template<typename T, typename S> void fwGenericAssetDef<T,S>::Init(const S name) 
{
	m_pObject = NULL; 
#if DETAILED_MODULE_REF_COUNT
	m_refsUnified = 0;
#else // DETAILED_MODULE_REF_COUNT
	m_refCount = 0; 
#endif //  DETAILED_MODULE_REF_COUNT
	memset( &m_name, 0, sizeof(S) );
	m_name = name;
}

// --- fwAssetStore -------------------------------------------------------------------------

extern const char *strModuleTypeIndexToExtension(int);

//
// name:		fwAssetStore<T,S>::fwAssetStore
// description:	Constructor for an asset store
template<typename T, typename S> fwAssetStore<T,S>::fwAssetStore(
	const char* pModuleName, 
	int moduleTypeIndex,
	int size,
	u32 elementsPerPage,
	bool requiresTempMemory,
	bool canDefragment,
	int rscVersion) : 
fwAssetStoreBase(pModuleName, moduleTypeIndex,size, elementsPerPage, requiresTempMemory, canDefragment, rscVersion),
#if !USE_PAGED_POOLS_FOR_STREAMING
m_pool(pModuleName),
#endif // !USE_PAGED_POOLS_FOR_STREAMING
m_fileExt(strModuleTypeIndexToExtension(moduleTypeIndex)),
m_SizeFinalized(false)
{  
#if ENABLE_STORETRACKING
     fwStoreTrackerAdd(this);
#endif
}

//
// name:		fwAssetStore<T,S>::~fwAssetStore
// description:	Remove all objects and shutdown pool
template<typename T, typename S> fwAssetStore<T,S>::~fwAssetStore()
{
	Shutdown();	 
#if ENABLE_STORETRACKING
    fwStoreTrackerRemove(this);
#endif
}

//
// name:		fwAssetStore<T,S>::Shutdown
// description:	Remove all objects and shutdown pool
template<typename T, typename S> void fwAssetStore<T,S>::Shutdown()
{
	if (m_SizeFinalized)
	{
		RemoveAll();
		for(int i=0; i<GetSize(); i++)
		{
			if(IsValidSlot(strLocalIndex(i)))
			{
				Assertf((fwAssetStore<T,S>::Get(strLocalIndex(i)) == NULL), "%s still in memory while deleting fwAssetStore %s", fwAssetStore<T,S>::GetName(strLocalIndex(i)), GetModuleName());
				RemoveSlot(strLocalIndex(i));
			}
		}
#if !USE_PAGED_POOLS_FOR_STREAMING
		m_pool.Reset();
#endif // !USE_PAGED_POOLS_FOR_STREAMING
	}	
}


template<typename T, typename S> void fwAssetStore<T,S>::FinalizeSize() 
{
	if(!m_SizeFinalized)
	{
		// Check if fwConfigManager contains an override of the size of this fwAssetStore's pool.
		const int poolSize = fwConfigManager::GetInstance().GetSizeOfPool(atStringHash(GetModuleName()), m_size);

		// Resize the pool as well as the fwNameRegistrar object, and set the
		// m_size parameter in strStreamingModule.
#if !USE_PAGED_POOLS_FOR_STREAMING
		m_pool.InitAndAllocate(poolSize);
#endif // !USE_PAGED_POOLS_FOR_STREAMING
		m_reg.Init(poolSize);
		m_size = poolSize;

		m_SizeFinalized = true;
	}
}

//
// name:		fwAssetStore<T,S>::RegisterStreamingModule
// description:	Initialize
template<typename T, typename S> void fwAssetStore<T,S>::RegisterStreamingModule() 
{
	// This isn't very nice, but convenient - we force the pool to be allocated
	// here if it hasn't been done already.
	FinalizeSize();

	strStreamingEngine::GetInfo().GetModuleMgr().AddModule(this);
}


//
// name:		fwAssetStore<T,S>::GetSize
// description:	Return the number of elements that can be stored in this asset store
template<typename T, typename S> int fwAssetStore<T,S>::GetSize() const
{
	// The pool size or anything else using the pool wouldn't be valid
	// unless FinalizeSize() has been called.
	Assert(m_SizeFinalized);

#if USE_PAGED_POOLS_FOR_STREAMING
//	Assert(false);
	return m_IndexWatermark;
#else // USE_PAGED_POOLS_FOR_STREAMING
	return m_pool.GetSize();
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

//
// name:		fwAssetStore<T,S>::GetNumUsedSlots
// description:	Return the number of slots used in this asset store
template<typename T, typename S> int fwAssetStore<T,S>::GetNumUsedSlots() const
{
#if USE_PAGED_POOLS_FOR_STREAMING

	return m_used;
#else // USE_PAGED_POOLS_FOR_STREAMING
	return m_pool.GetNoOfUsedSpaces();
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

#if !__NO_OUTPUT
//
// name:		fwAssetStore<T,S>::GetNumUsedSlots
// description:	Return the number of slots used in this asset store
template<typename T, typename S> int fwAssetStore<T,S>::GetPeakSlotsUsed() const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	// TODO: We should keep track of that somewhere.
	return m_used;
#else // USE_PAGED_POOLS_FOR_STREAMING
	return m_pool.GetPeakSlotsUsed();
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

template<typename T, typename S> size_t fwAssetStore<T,S>::GetStorageSize() const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	return sizeof(S);
#else // USE_PAGED_POOLS_FOR_STREAMING
	return m_pool.GetStorageSize();
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

template<typename T, typename S> int fwAssetStore<T,S>::GetNoOfFreeSpaces() const
{
#if USE_PAGED_POOLS_FOR_STREAMING
	// TODO: We should keep track of that somewhere.
	return m_used;
#else // USE_PAGED_POOLS_FOR_STREAMING
	return m_pool.GetNoOfFreeSpaces();
#endif // USE_PAGED_POOLS_FOR_STREAMING
}
#endif

//
// name:		fwAssetStore<T,S>::IsValidSlot
// description:	Return if this slot is used
template<typename T, typename S> bool fwAssetStore<T,S>::IsValidSlot(strLocalIndex index) const
{
	return ((index.Get() >= 0) ? (GetSlot(index) != NULL) : false);
}

#if USE_PAGED_POOLS_FOR_STREAMING

//
// name:		fwAssetStore<T,S>::GetSlot
// description:	Return a def object pointer to this slot
template<typename T, typename S> S*	fwAssetStore<T,S>::GetSlot(strLocalIndex index)
{
	Assert(index >= 0);
	u32 page = GetPageFromObjIndex(index);
	FastAssert(sm_AssetStorePages[page]);

	return &((S *) sm_AssetStorePages[page])[GetElementFromObjIndex(index)];
}

//
// name:		fwAssetStore<T,S>::GetSlot
// description:	Return a def object pointer to this slot
template<typename T, typename S> const S*	fwAssetStore<T,S>::GetSlot(strLocalIndex index) const
{
	Assert(index >= 0);
	u32 page = GetPageFromObjIndex(index);
	FastAssert(sm_AssetStorePages[page]);

	return &((const S *) sm_AssetStorePages[page])[GetElementFromObjIndex(index)];
}

template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::GetSlotIndex(const S* pDef) const
{
	int pageCount = m_PoolHeaders.GetCount();
	u32 elementCount = strStreamingEngine::GetInfo().GetStreamingInfoPool().GetElementsPerPage(GetStreamingModuleId());

	for (int x=0; x<pageCount; x++)
	{
		u32 pageIndex = (u32) m_PoolHeaders[x];
		const S* page = (const S *) sm_AssetStorePages[pageIndex];
		if (page && page <= pDef && &page[elementCount] > pDef)
		{
			int element = ptrdiff_t_to_int(pDef - page);
			// We have the element within the page. Add the offset too.
			return strLocalIndex((int) strStreamingEngine::GetInfo().GetStreamingInfoPool().GetPageHeader(pageIndex).m_UserData + element);
		}
	}

	FastAssert(false);
	return strLocalIndex(-1);
}


#else //  USE_PAGED_POOLS_FOR_STREAMING

//
// name:		fwAssetStore<T,S>::GetSlot
// description:	Return a def object pointer to this slot
template<typename T, typename S> S*	fwAssetStore<T,S>::GetSlot(strLocalIndex index)
{
	return m_pool.GetSlot(index.Get());
}

//
// name:		fwAssetStore<T,S>::GetSlot
// description:	Return a def object pointer to this slot
template<typename T, typename S> const S*	fwAssetStore<T,S>::GetSlot(strLocalIndex index) const
{
	return m_pool.GetSlot(index.Get());
}

template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::GetSlotIndex(const S* pDef) const
{
	return strLocalIndex(m_pool.GetJustIndex(pDef));
}

#endif // USE_PAGED_POOLS_FOR_STREAMING


//
// name:		fwAssetStore<T,S>::Get
// description:	Get object at this slot
template<typename T, typename S> 
#if !__ASSERT
	__forceinline 
#endif
T* fwAssetStore<T,S>::Get(strLocalIndex index)	
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);
	Assertf(pDef, "Slot %d does not exist", index.Get());

	return pDef->m_pObject;
}

//
// name:		fwAssetStore<T,S>::Get
// description:	Get object at this slot
template<typename T, typename S> 
#if !__ASSERT
__forceinline 
#endif
	T* fwAssetStore<T,S>::Get(strLocalIndex index) const
{
	Assert(index.Get() >= 0);
	const S* pDef = GetSlot(index);
	Assertf(pDef, "Slot %d does not exist", index.Get());

	return const_cast<T*>(pDef->m_pObject);
}




template<typename T, typename S> __forceinline T* fwAssetStore<T,S>::GetSafeFromIndex(strLocalIndex index)	
{
	const S* pDef = (index.Get() >= 0) ? GetSlot(index) : NULL;
	return pDef ? pDef->m_pObject : NULL;
}

template<typename T, typename S> __forceinline T* fwAssetStore<T,S>::GetSafeFromName(const typename S::strObjectNameType name)
{
	return GetSafeFromIndex(FindSlotFromHashKey(name.GetHash()));
}

//
// name:		fwAssetStore<T,S>::GetPtr
// description:	Return pointer to object at this slot
template<typename T, typename S> void* fwAssetStore<T,S>::GetPtr(strLocalIndex index)	
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);
	if(pDef)
		return pDef->m_pObject;
	return NULL;
}

#if !__FINAL || __FINAL_LOGGING
//
// name:		fwAssetStore<T,S>::GetName
// description:	Get object at this slot
template<typename T, typename S> const char* fwAssetStore<T,S>::GetName(strLocalIndex index) const
{
	Assert(index.Get() >= 0);
	const S* pDef = GetSlot(index);
	//Assertf(pDef, "Slot %d does not exist", index);

	return pDef ? pDef->m_name.GetCStr() : "";
}
#endif

//
// name:		fwAssetStore<T,S>::GetHash
// description:	Get hash key at this slot
template<typename T, typename S> u32 fwAssetStore<T,S>::GetHash(strLocalIndex index) const
{
	Assert(index.Get() >= 0);
	const S* pDef = GetSlot(index);
	//Assertf(pDef, "Slot %d does not exist", index);

	return pDef ? pDef->m_name.GetHash() : 0;
}

//
// name:		fwAssetStore<T,S>::AddSlot
// description:	Add a new slot with the specified name
template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::AddSlot(const typename S::strObjectNameType name)
{
#if USE_PAGED_POOLS_FOR_STREAMING
	int result = AllocateNewStreamable();
	S* pDef = GetSlot(result);
#else // USE_PAGED_POOLS_FOR_STREAMING
	S* pDef = m_pool.New();

	Assertf(pDef, "No more slots available in store");
	int result = m_pool.GetJustIndex(pDef);
#endif // USE_PAGED_POOLS_FOR_STREAMING

	pDef->Init(name);

	InsertHashKey(name.GetHash(), result);
	return strLocalIndex(result);
}

//
// name:		fwAssetStore<T,S>::AddSlot
// description:	Add a new slot with the specified name
template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::AddSlot(strLocalIndex index, const typename S::strObjectNameType name)
{
#if USE_PAGED_POOLS_FOR_STREAMING
	AllocateSlot(index);
	S *pDef = GetSlot(index);
#else // USE_PAGED_POOLS_FOR_STREAMING
	Assertf(GetSlot(index) == NULL, "Slot is already allocated");
	S* pDef = m_pool.New(index.Get());
#endif // USE_PAGED_POOLS_FOR_STREAMING

	pDef->Init(name);
	InsertHashKey(name.GetHash(), index.Get());

	return index;
}

//
// name:		fwAssetStore<T,S>::FindSlot
// description:	find a slot given a name
template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::FindSlot(const typename S::strObjectNameType name) const
{
	return FindSlotFromHashKey(name.GetHash());
}

template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::FindSlot(const char* name) const
{
	typename S::strObjectNameType	objectName( name );
	return FindSlot( objectName );
}

//
// name:		fwAssetStore<T,S>::Register
// description:	function used by streaming to register a file with a slot in the store
template<typename T, typename S> strLocalIndex fwAssetStore<T,S>::Register(const char* name)
{
	typename S::strObjectNameType	objectName( name );

	strLocalIndex index = strLocalIndex(FindSlot(objectName));
	// If cannot find slot then create one. 
	if(index == -1)
		index = AddSlot(objectName);
	else
	{
		Assertf(!stricmp(name, GetName(index))," (%s : %s) Hash name clash in asset store!", name, GetName(index));
	}
	return index;
}


//
// name:		fwAssetStore<T,S>::FindSlotFromHashKey
// description:	find a slot given a hash key
template<typename T, typename S> __forceinline strLocalIndex fwAssetStore<T,S>::FindSlotFromHashKey(const u32 hashKey) const
{
	strLocalIndex result = strLocalIndex(m_reg.Lookup(hashKey));
	return result != -1 && GetSlot(result) ? result : strLocalIndex(-1);
}

//
// name:		fwAssetStore<T,S>::Set
// description:	set the definition entry 
template<typename T, typename S> void fwAssetStore<T,S>::Set(strLocalIndex index, T* m_pObject)
{
	Assert(sysThreadType::IsUpdateThread());
	S* pDef = GetSlot(index);
	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());
	Assertf(pDef->m_pObject==NULL, "Entry is already in memory");

	pDef->m_pObject = m_pObject;
}

//
// name:		fwAssetStore<T,S>::Remove
// description:	Remove object at this slot
template<typename T, typename S> void fwAssetStore<T,S>::Remove(strLocalIndex index)
{
	Assert(sysThreadType::IsUpdateThread());

	S* pDef = GetSlot(index);
	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());
	Assertf(pDef->m_pObject!=NULL, "Entry is not in memory");

	//SAFE_REMOVE_KNOWN_REF(pDef->m_pObject);
	delete pDef->m_pObject;
	pDef->m_pObject = NULL;
}

//
// name:		fwAssetStore<T,S>::RemoveSlot
// description:	Remove a slot from the store
template<typename T, typename S> void fwAssetStore<T,S>::RemoveSlot(strLocalIndex index)
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);

	// delete texture dictionary
	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());

	Assertf(GetStreamingInfo(index)->GetStatus() == STRINFO_NOTLOADED, "Removing %s from the %s module, but its status is %s.",
		GetName(index), GetModuleName(), GetStreamingInfo(index)->GetFriendlyStatusName());

	if(pDef->m_pObject)
		Remove(index);
	RemoveHashKey(pDef->m_name.GetHash());

#if USE_PAGED_POOLS_FOR_STREAMING
	// Free the element - this will automatically call our callback if we end up deleting the entire page.
	FreeObject(index);
#else // USE_PAGED_POOLS_FOR_STREAMING
	m_pool.Delete(GetSlot(index));
#endif // USE_PAGED_POOLS_FOR_STREAMING
}

//
// name:		fwAssetStore<T,S>::Load
// description:	Load file from memory
template<typename T, typename S> bool fwAssetStore<T,S>::Load(strLocalIndex index, void* pData, int size)
{
	Assert(index.Get() >= 0);
	const int maxFileLength = 128;
	char filename[maxFileLength];

#if __FINAL
	fiDeviceMemory::MakeMemoryFileName(filename, maxFileLength, pData, size, false, NULL);
#else
	fiDeviceMemory::MakeMemoryFileName(filename, maxFileLength, pData, size, false, GetName(index));
#endif

	return LoadFile(index, filename);
}

//
// name:		fwAssetStore<T,S>::LoadFile
// description:	Load file function. Base version of this function does nothing
template<typename T, typename S> bool fwAssetStore<T,S>::LoadFile(strLocalIndex UNUSED_PARAM(index), const char* UNUSED_PARAM(pFilename))
{
	return false;
}

//
// name:		fwAssetStore<T,S>::AddRef
// description:	Reference object. This stops the object from being deleted
template<typename T, typename S> void fwAssetStore<T,S>::AddRef(strLocalIndex index, strRefKind DETAILED_REF_ONLY(refKind))
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);

	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());
	Assertf(pDef->m_pObject, "%s:Object is not in memory", pDef->m_name.GetCStr());

	Assertf(sysThreadType::IsUpdateThread(), "%s: Adding resource ref outside main thread", pDef->m_name.GetCStr());

#if DETAILED_MODULE_REF_COUNT
	pDef->SetSubRefCount(refKind, pDef->GetSubRefCount(refKind) + 1);

#if TRACE_REFCOUNT
	const char *trackRefCountName;
	if (PARAM_trackrefcount.Get(trackRefCountName) && ((1 << refKind) & g_TrackRefCountKindBitmask) && pDef->m_name.GetCStr() && strstr(pDef->m_name.GetCStr(), trackRefCountName))
	{
		Displayf("Trace history: ADD: Ref count kind %d for %s (module %s) is now at %d (index %d, %p)", refKind, pDef->m_name.GetCStr(),
			GetModuleName(), pDef->GetSubRefCount(refKind), index.Get(), pDef);
		sysStack::PrintStackTrace();
	}
#endif // TRACE_REFCOUNT

	Assertf(pDef->GetSubRefCount(refKind) != 0, "%s (%s): The Ref count %s (kind %d) exceeds its max value of %d. Make sure there is no leak, and give it more bits.",
		pDef->m_name.GetCStr(), GetModuleName(), strRefKindNames[refKind], refKind, (1 << strRefKindBits[refKind]) - 1);
#else // DETAILED_MODULE_REF_COUNT
	pDef->m_refCount++;

	Assertf(pDef->m_refCount < 2000, "%s: Ref count very high, make sure there is no leak (ref count=%d)", pDef->m_name.GetCStr(), pDef->m_refCount);
#endif // DETAILED_MODULE_REF_COUNT

}
//
// name:		fwAssetStore<T,S>::RemoveRef
// description:	Remove reference to object
template<typename T, typename S> void fwAssetStore<T,S>::RemoveRef(strLocalIndex index, strRefKind DETAILED_REF_ONLY(refKind))
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);

	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());
	Assertf(pDef->m_pObject, "%s: Object (module %s) is not in memory at slot %d (def at %p), strIndex %d", pDef->m_name.GetCStr(), GetModuleName(), index.Get(), pDef, GetStreamingIndex(index).Get());

	Assertf(sysThreadType::IsUpdateThread(), "%s: Removing resource ref outside main thread", pDef->m_name.GetCStr());

#if DETAILED_MODULE_REF_COUNT
	if (pDef->GetSubRefCount(refKind) == 0)
	{
		char refString[16];
		GetRefCountString(index, refString, sizeof(refString));
		Assertf(false, "%s (%s): Called RemoveRef too many times on kind %s (%d) - ref counts: (%s)",
			pDef->m_name.GetCStr(), GetModuleName(), strRefKindNames[refKind], refKind, refString);
	}

	pDef->SetSubRefCount(refKind, pDef->GetSubRefCount(refKind) - 1);

#if TRACE_REFCOUNT
	const char *trackRefCountName;
	if (PARAM_trackrefcount.Get(trackRefCountName) && ((1 << refKind) & g_TrackRefCountKindBitmask) && pDef->m_name.GetCStr() && strstr(pDef->m_name.GetCStr(), trackRefCountName))
	{
		Displayf("Trace history: REM: Ref count kind %d for %s (module %s) is now at %d (index %d, %p)", refKind, pDef->m_name.GetCStr(),
			GetModuleName(), pDef->GetSubRefCount(refKind), index.Get(), pDef);
		sysStack::PrintStackTrace();
	}
#endif // TRACE_REFCOUNT

	int refCount = fwAssetStore<T,S>::GetNumRefs(index);

#else // DETAILED_MODULE_REF_COUNT
	Assertf(pDef->m_refCount > 0, "%s: Called RemoveRef too many times (ref count=%d)", pDef->m_name.GetCStr(), pDef->m_refCount);
	int refCount = --pDef->m_refCount;
#endif // DETAILED_MODULE_REF_COUNT

	// If object isn't referenced and is outside of an img then remove it
	if(refCount <= 0 && !(IsObjectInImage(index)))
	{
		Assertf(false, "We hit an archaic condition that will now result in the deletion of %s because its ref count is 0 and it's now all of a sudden invalid. Did its RPF get deleted while it was resident?", strStreamingEngine::GetObjectName(GetStreamingIndex(index)));
		Remove(index);
	}
}

// name:		fwAssetStore<T,S>::ResetAllRefs
// description:	Remove all references to object
template<typename T, typename S> void fwAssetStore<T,S>::ResetAllRefs(strLocalIndex index)
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);

	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());

	Assertf(sysThreadType::IsUpdateThread(), "%s: Removing resource ref outside main thread", pDef->m_name.GetCStr());

#if DETAILED_MODULE_REF_COUNT
	pDef->m_refsUnified = 0;

#if TRACE_REFCOUNT
	const char *trackRefCountName;
	if (PARAM_trackrefcount.Get(trackRefCountName) && pDef->m_name.GetCStr() && strstr(pDef->m_name.GetCStr(), trackRefCountName))
	{
		Displayf("Trace history: RESET: Ref counts for %s (module %s) reset to 0", pDef->m_name.GetCStr(),
			GetModuleName());
		sysStack::PrintStackTrace();
	}
#endif // TRACE_REFCOUNT


#else // DETAILED_MODULE_REF_COUNT
	pDef->m_refCount = 0;
#endif // DETAILED_MODULE_REF_COUNT
}

#if DETAILED_MODULE_REF_COUNT
template<typename T, typename S> const char *fwAssetStore<T,S>::GetRefCountString(strLocalIndex index, char *buffer, size_t bufferLen) const
{
	CompileTimeAssert(MAX_REF_KINDS == 3); // Hard-coded to 3 so we can describe each element

	const S* pDef = GetSlot(index);
	formatf(buffer, bufferLen, "R=%d;S=%d;O=%d;T=%d", pDef->GetSubRefCount(REF_RENDER), pDef->GetSubRefCount(REF_SCRIPT), /*pDef->GetSubRefCount(REF_DEFRAG),*/ pDef->GetSubRefCount(REF_OTHER), GetNumRefs(index));
	return buffer;
}
#endif // DETAILED_MODULE_REF_COUNT


//
// name:		fwAssetStore<T,S>::RemoveRefWithoutDelete
// description:	Remove reference to object
template<typename T, typename S> void fwAssetStore<T,S>::RemoveRefWithoutDelete(strLocalIndex index, strRefKind DETAILED_REF_ONLY(refKind))
{
	Assert(index.Get() >= 0);
	S* pDef = GetSlot(index);

	Assertf(sysThreadType::IsUpdateThread(), "%s: Removing resource ref outside main thread", pDef->m_name.GetCStr());

	Assertf(pDef, "No entry at slot %d in %s", index.Get(), GetModuleName());

#if DETAILED_MODULE_REF_COUNT
	Assertf(pDef->GetSubRefCount(refKind) > 0, "%s: Called RemoveRef (Without Delete) too many times on kind %s (%d), ref count=%d",
		pDef->m_name.GetCStr(), strRefKindNames[refKind], refKind, pDef->GetSubRefCount(refKind));

	pDef->SetSubRefCount(refKind, pDef->GetSubRefCount(refKind) - 1);

#else // DETAILED_MODULE_REF_COUNT
	Assertf(pDef->m_refCount>0, "%s:Object reference count is zero", pDef->m_name.GetCStr());

	pDef->m_refCount--;
#endif // DETAILED_MODULE_REF_COUNT

}

#if USE_PAGED_POOLS_FOR_STREAMING
template<typename T, typename S> void fwAssetStore<T,S>::RegisterPagedPool(void *data, u32 page, u32 elementCount, strStreamingInfoPageHeader &header, size_t *memUsage)
{
	strStreamingModule::RegisterPagedPool(data, page, elementCount, header, memUsage);

	MEM_USE_USERDATA(MEMUSERDATA_ASSETSTORE - GetStreamingModuleId());
	// We'll always mirror what the paged pool does - it's allocating a new page, so we're allocating a new page.
#if 0
#if USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS

	size_t pageSize = pgRscBuilder::ComputeLeafSize(sizeof(S) * elementCount, false);

	Assertf(pageSize - elementCount * sizeof(S) < sizeof(S),
		"Just letting you know that %d %s defs per page with a %d size is wasting %d bytes of memory per page. I would recommend "
		"an element count of %d.",
		elementCount, GetModuleName(), (int) sizeof(S),
		(int) (pageSize - elementCount * sizeof(S)),
		(int) (pageSize / sizeof(S)));

	USE_MEMBUCKET(MEMBUCKET_WORLD);		// SHOULD be streaming, but that's not allocated in the streaming allocator
	void *defPage = strStreamingEngine::GetAllocator().Allocate(sizeof(S) * elementCount, __alignof(S), MEMTYPE_RESOURCE_VIRTUAL);
#else // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	USE_MEMBUCKET(MEMBUCKET_STREAMING);
	void *defPage = rage_aligned_new(__alignof(S)) u8[sizeof(S) * elementCount];
#endif // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	*memUsage += pgRscBuilder::ComputeLeafSize(sizeof(S) * elementCount, false);*/
#endif // 0

	u8* defPage = (u8*) data;

	size_t baseMemory = sizeof(strStreamingInfo) * elementCount;
	size_t padding = (16 - baseMemory) & 15;

	defPage += baseMemory + padding;

	sysMemSet(defPage, 0, sizeof(S) * elementCount);	// TODO: Shouldn't be necessary, navmesh currently requires it.

	FastAssert(!sm_AssetStorePages[page]);
	sm_AssetStorePages[page] = defPage;
}

template<typename T, typename S> void fwAssetStore<T,S>::FreePagedPool(u32 page, u32 elementCount, strStreamingInfoPageHeader &header, size_t *memUsage)
{
	strStreamingModule::FreePagedPool(page, elementCount, header, memUsage);

	// Delete our asset definitions.
	FastAssert(sm_AssetStorePages[page]);
	MEM_USE_USERDATA(MEMUSERDATA_ASSETSTORE - GetStreamingModuleId());
	//*memUsage += pgRscBuilder::ComputeLeafSize(sizeof(S) * elementCount, false);
#if USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	//strStreamingEngine::GetAllocator().Free(sm_AssetStorePages[page]);
#else // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	//delete[] sm_AssetStorePages[page];
#endif // USE_RSCVIRT_FOR_STREAMING_PAGED_POOLS
	sm_AssetStorePages[page] = NULL;
}

template<typename T, typename S> size_t fwAssetStore<T,S>::ComputePageMemUsage(u32 elementCount) const
{
	return pgRscBuilder::ComputeLeafSize(sizeof(S) * elementCount, false);
}

#endif // USE_PAGED_POOLS_FOR_STREAMING


//
// name:		fwAssetStore<T,S>::GetNumRefs
// description:	Return number of references object has
template<typename T, typename S> int fwAssetStore<T,S>::GetNumRefs(strLocalIndex index) const
{
	Assert(index.Get() >= 0);
	const S* pDef = GetSlot(index);

	if (!Verifyf(pDef, "Slot %d does not exist in module %s. Are you sure the index is valid? Has the slot been removed?", index.Get(), GetModuleName()))
	{
		return 0;
	}

#if DETAILED_MODULE_REF_COUNT
	int result = (int) pDef->GetSubRefCount((strRefKind) 0);

	for (int x=1; x<MAX_REF_KINDS; x++)
	{
		result += (int) pDef->GetSubRefCount((strRefKind) x);
	}

	return result;
#else // DETAILED_MODULE_REF_COUNT
	return pDef->m_refCount;
#endif // DETAILED_MODULE_REF_COUNT
}

//
// name:		fwAssetStore<T,S>::::LoadAll
// description:	Load all objects
template<typename T, typename S> void fwAssetStore<T,S>::LoadAll(bool andRemove)
{
#if !__FINAL
	sysTimer t;
#endif // !__FINAL

	int numRemove = 0;
	const int maxBatch = 128;
	int currentBatch = 0;
	int *remove = (int *)alloca( maxBatch * sizeof( int ) );
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(strLocalIndex(i)))
		{
#if !__FINAL
			formatf(sm_ProgressText, "Rebuilding cache: Loading all %s files (%d/%d) ...%.f%%", GetModuleName(), i, GetSize(), (float)i * 100.0f / (float)GetSize());

			if (t.GetMsTime() > 10000)
			{
				Displayf("%s", sm_ProgressText);
				t.Reset();
			}
#endif // !__FINAL
			if( StreamingFull() || currentBatch >= maxBatch )
			{
				g_strStreamingInterface->LoadAllRequestedObjects();
				for( int i = 0; i < numRemove; i++ )
				{
					StreamingRemove( strLocalIndex( remove[i] ) );
				}
				numRemove = 0;
				currentBatch = 0;
			}
			StreamingRequest( strLocalIndex( i ) );
			++currentBatch;
			if( andRemove ) remove[numRemove++] = i;
		}
	}
	g_strStreamingInterface->LoadAllRequestedObjects();
	for( int i = 0; i < numRemove; i++ )
	{
		StreamingRemove( strLocalIndex( remove[i] ) );
	}

#if !__FINAL
	sm_ProgressText[0] = 0;
#endif // !__FINAL	
}

//
// name:		fwAssetStore<T,S>::::LoadAllNew
// description:	Load all objects newer than cache file
template<typename T, typename S> void fwAssetStore<T,S>::LoadAllNew(bool andRemove)
{
#if !__FINAL
	sysTimer t;
#endif // !__FINAL

	int numRemove = 0;
	const int maxBatch = 128;
	int currentBatch = 0;
	int *remove = (int *)alloca( maxBatch * sizeof( int ) );
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(strLocalIndex(i)) && IsObjectNew(strLocalIndex(i)))
		{
#if !__FINAL
			formatf(sm_ProgressText, "Rebuilding cache: Loading new (uncached) %s files (%d/%d) ...%.f%%", GetModuleName(), i + 1, GetSize(), (float)i * 100.0f / (float)GetSize());

			if (t.GetMsTime() > 10000)
			{
				Displayf("%s", sm_ProgressText);
				t.Reset();
			}
#endif // !__FINAL
			if( StreamingFull() || currentBatch >= maxBatch )
			{
				g_strStreamingInterface->LoadAllRequestedObjects();
				for( int i = 0; i < numRemove; i++ )
				{
					StreamingRemove( strLocalIndex( remove[i] ) );
				}
				numRemove = 0;
				currentBatch = 0;
			}
			StreamingRequest(strLocalIndex(i));
			++currentBatch;
			if( andRemove ) remove[numRemove++] = i;
		}	
	}
	g_strStreamingInterface->LoadAllRequestedObjects();
	for( int i = 0; i < numRemove; i++ )
	{
		StreamingRemove( strLocalIndex( remove[i] ) );
	}

#if !__FINAL
	sm_ProgressText[0] = 0;
#endif // !__FINAL
}

//
// name:		fwAssetStore<T,S>::RemoveAll
// description:	Remove all items in store
template<typename T, typename S> void fwAssetStore<T,S>::RemoveAll(bool bIgnoreRefCount)
{
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(strLocalIndex(i)) && Get(strLocalIndex(i)))
		{
			if(bIgnoreRefCount || 
				(GetNumRefs(strLocalIndex(i)) == 0 && !(GetStreamingFlags(strLocalIndex(i)) & STR_DONTDELETE_MASK)))
			{
				StreamingRemove(strLocalIndex(i));
			}
#if __DEV
			else
			{
				char refString[16];
				GetRefCountString(strLocalIndex(i), refString, sizeof(refString));

                Displayf("Failed to remove %s.%s asidx [%d] stridx {%d} : refCount %s, flags D%d M%d C%d I%d",
                    GetName(strLocalIndex(i)),
                    fwAssetStore<T,S>::m_fileExt.c_str(),
                    i,
                    GetStreamingIndex(strLocalIndex(i)).Get(),
                    refString,
                    (GetStreamingFlags(strLocalIndex(i)) & STRFLAG_DONTDELETE) != 0,
                    (GetStreamingFlags(strLocalIndex(i)) & STRFLAG_MISSION_REQUIRED) != 0,
                    (GetStreamingFlags(strLocalIndex(i)) & STRFLAG_CUTSCENE_REQUIRED) != 0,
                    (GetStreamingFlags(strLocalIndex(i)) & STRFLAG_INTERIOR_REQUIRED) != 0);
                strStreamingEngine::GetInfo().PrintAllDependents(GetStreamingIndex(strLocalIndex(i)));
				
			}
#endif // __DEV			
		}
	}	
}

#if __DEV
//
// name:		fwAssetStore<T,S>::PrintLoadedObjs
// description:	Print the objects in memory
template<typename T, typename S> void fwAssetStore<T,S>::PrintAllObjs()
{
	Displayf("%s store", GetModuleName());
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(i))
		{
			Displayf("%s[%d] %s", GetModuleName(), i, GetName(i));
		}
	}	
}

//
// name:		fwAssetStore<T,S>::PrintLoadedObjs
// description:	Print the objects in memory
template<typename T, typename S> void fwAssetStore<T,S>::PrintLoadedObjs()
{
	Displayf("%s store", GetModuleName());
	for(int i=0; i<GetSize(); i++)
	{
		if(IsValidSlot(strLocalIndex(i)) && Get(strLocalIndex(i)))
		{
			Displayf("[%d] = %s", i, GetName(strLocalIndex(i)));
		}
	}	
}

#endif // __DEV

// --- fwAssetRscStore ---------------------------------------------------------------------------------------------

template<class T, class S> bool fwAssetRscStore<T,S>::LoadFile(strLocalIndex index, const char* pFilename)
{
	S* pDef = fwAssetStore<T,S>::GetSlot(index);

	Assertf(pDef, "No entry at this slot");
	Assertf(pDef->m_pObject==NULL, "Entry is already in memory");

	// Streaming makes space for this resource
	if (Verifyf(g_strStreamingInterface->MakeSpaceForResourceFile(pFilename, fwAssetStore<T,S>::m_fileExt, fwAssetStore<T,S>::m_expectedRscVersionNumber), "Could not make space for %s", pFilename))
	{
		T* m_pObject;
		pgRscBuilder::Load(m_pObject, pFilename, fwAssetStore<T,S>::m_fileExt, fwAssetStore<T,S>::m_expectedRscVersionNumber);
		if (Verifyf(m_pObject, "Failed to load %s", pFilename))
		{
//			Set(index, m_pObject);
		}
	}

	return (pDef->m_pObject != NULL);
}

template<class T, class S> void fwAssetRscStore<T,S>::PlaceAsynchronously(strLocalIndex UNUSED_PARAM(objIndex), strStreamingLoader::StreamingFile& file, datResourceInfo& resInfo)
{
	strStreamingEngine::GetLoader().RequestAsyncPlacement(file, file.m_LoadingMap, resInfo);
}

//
// name:		fwAssetRscStore<T,S>::Set
// description:	set the definition entry 
template<typename T, typename S> void fwAssetRscStore<T,S>::Set(strLocalIndex index, T* m_pObject)
{
	Assert(sysThreadType::IsUpdateThread());

	S* pDef = fwAssetStore<T,S>::GetSlot(index);
	Assertf(pDef, "No entry at this slot");
	Assertf(pDef->m_pObject==NULL, "Entry is already in memory");

	UPDATE_KNOWN_REF(pDef->m_pObject , m_pObject);
}

template<typename T, typename S> pgBase *fwAssetRscStore<T,S>::GetResource(strLocalIndex index) const
{
	const S* pDef = fwAssetStore<T,S>::GetSlot(index);

	if (!pDef)
	{
		return NULL;
	}

	return pDef->m_pObject;
}

//
// name:		fwAssetRscStore<T,S>::Remove
// description:	Remove object at this slot
template<class T, class S> void fwAssetRscStore<T,S>::Remove(strLocalIndex index)
{
#if ENABLE_KNOWN_REFERENCES
	S* pDef = fwAssetStore<T,S>::GetSlot(index);
	Assertf(pDef, "No entry at this slot");
	Assertf(pDef->m_pObject!=NULL, "Entry is not in memory");
#endif

#if !__FINAL
	DIAG_CONTEXT_MESSAGE("Remove resource %s", fwAssetStore<T,S>::GetName(index));
#endif // !__FINAL

#if ENABLE_KNOWN_REFERENCES	// Already a no-op when disabled but it causes an unused variable error
	SAFE_REMOVE_KNOWN_REF(pDef->m_pObject);
#endif
	fwAssetStore<T,S>::Remove(index);
}

//
// name:		fwAssetRscStore<T,S>::PlaceResource
// description:	
template<class T, class S> void fwAssetRscStore<T,S>::PlaceResource(strLocalIndex NOTFINAL_ONLY(index), datResourceMap& map, datResourceInfo& header)
{
	T* object = NULL;

#if __FINAL
	pgRscBuilder::PlaceStream(object, header, map, "<unknown>");
#else
	pgRscBuilder::PlaceStream(object, header, map, fwAssetRscStore<T,S>::GetName(index));
#endif
}

//
// name:		fwAssetRscStore<T,S>::SetResource
// description:	
template<class T, class S> void fwAssetRscStore<T,S>::SetResource(strLocalIndex index, datResourceMap& map)
{
	T* object = static_cast<T*>(map.GetVirtualBase());
	Set(index, object);
}

//
// name:		CStore<T,S>::Defragment
// description:	
template<class T, class S> void* fwAssetRscStore<T,S>::Defragment(strLocalIndex UNUSED_PARAM(index), datResourceMap& map, bool& UNUSED_PARAM(flush))
{
	T* pObject = NULL;

	datResource rsc(map,"defrag",true);
	T::Place(pObject = (T*)rsc.GetBase(), rsc);

	return pObject;
}


}		// namespace rage

#endif // !__SPU

#endif // !INC_ASSET_STORE_H_

