//
//	fwtl\ExtRefAwareBase.h
//	
//	Copyright (C) 2008-2009 Rockstar Games.  All Rights Reserved.
//
//
//	This is an extension base class to the RegdRef system.  It is an alternative 
//  base class (to fwRefAwareBase) that performs additional duties, namely 
//  reference tracking, and ownership migration for the cost of a slight base
//  class data increase.
//
//	It works with fwRegdRef<T>, with specialisation calling to a fwExtRefAwareBase
//  that performs greater tracking of objects.
//
//  Key features:
//
//  * A fast numeric ref count, with an extra reference on new [by callback]
//  facilitates efficient leak and orphaning.
//
//  Note: 
//
//  * An operation history provides feedback of pointer copying and new ownership.
//  for diagnosing issues with the above.
//
#ifndef FWTL_EXTREFAWAREBASE_H_
#define FWTL_EXTREFAWAREBASE_H_

//	Rage headers
#include "fwtl/regdrefs.h"

#if !__FINAL

namespace rage {

class fwDebugOwnerHistoryFrame;

//
// PURPOSE
//	This is the alternative base class that your reference aware objects 
//  can derive from.
// NOTES
//	This will add a single pointer (from fwRefAwareBase), a debug pointer 
//  and a count to the size of your objects.
//
//  The count will be used for broad strokes orphan/leak control, and the
//  debug pointer for a comprehensive just in time allocation of frames for
//  recording ownership changes.
//
class fwExtRefAwareBase : public fwRefAwareBase
{
private:
	typedef			fwRefAwareBase			tBase;

public:
	enum eFlags
	{
		// PURPOSE:
		//  We passed through at least one official regd-ref.  This should
		//  happen on the first instance of AddKnownRef - although in the 
		//  case of this base class, without storage or debugging, it 
		//  doesn't matter.
		F_WasRegdRefed							= 0x1,

		// PURPOSE:
		//  For derived class which support it, this indicates that we were
		//  definitely dynamically allocated.  Some classes, for example, 
		//  could potentially be created on the stack and wouldn't need the
		//  standard delete function.
		F_DynamicallyAllocated					= 0x2,
	};

	//
	// PURPOSE
	//	The default constructor.  Makes sure this object starts clear of
	//	any tracked refs.
	//
	fwExtRefAwareBase();

	//
	// PURPOSE
	//	The destructor.  Auto-nulls all the known pointers that point to
	//	this object.  This is done to prevent dangling pointers
	//	throughout the system.
	// NOTES
	//	This forces the base class to be virtual...  This is necessary so
	//	that cast pointers to base and raw pointers to base actually point
	//	to the same place...  (basically we can't safely down cast without
	//	it).
	//
	virtual ~fwExtRefAwareBase();

	//
	// PURPOSE
	//  Indicates that deletion came from the correct helper function, rather
	//  than a raw delete.  Necessary because there is no easy way of using
	//  operator delete - to detect the relevant regdref.
	//
	void	SetRefAwareFlags( int flags )				const;

	//
	// PURPOSE
	//	Add the provided pointer to the list of pointers that need to be
	//	updated if this object is deleted.
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	void	AddKnownRef					(void** ppReference) const;

	//
	// PURPOSE
	//	Remove the provided pointer from the list of pointers that need
	//	to be updated if this object is deleted.
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	void	RemoveKnownRef				(void** ppReference) const;

	//
	// PURPOSE
	//	Auto-nulls all the known pointers that point to this object.
	// NOTES
	//	This is done to prevent dangling pointers throughout the system.
	//	It is used directly by the destructor, but is also useful for
	//	objects whose storage is re-used instead of using delete/new to
	//	create a new one.
	//
	void	ClearAllKnownRefs			(void) const;

	//
	// PURPOSE
	//	Determine if the ref (which should be pointing to this) is one
	//	that we are already aware of and tracking or not.
	// PARAMS
	//	ppReference	-	The address of the reference.
	// RETURNS
	//	Whether or not it is known (in the list on know refs).
	//
	bool	IsRefKnown					(void** ppReference) const;

	//
	// PURPOSE
	//	Determine if there are any known references in the list.
	// RETURNS
	//	Whether or not there are know references in the list.
	//
	bool	IsReferenced				(void) const;

	//
	// PURPOSE
	//  Verifies our integrity depending on whether or not we are still
	//  allocated
	//
	// PARAM
	//  isAllocated - true if we are still allocated out
	//
	void	VerifyIntegrity				( bool isAllocated ) const;

	//
	// PURPOSE
	//  A custom fail function that prints out the debug spew then asserts
	//  on the give reason
	//
	// PARAM
	//  reason - Reason why we failed integrity
	//
	void	FailIntegrity				( const char * reason ) const;

private:

	//
	// PURPOSE
	//  Prints the addresses of all existing references
	//
	void	PrintAssigned()			const;

	mutable fwDebugOwnerHistoryFrame *		m_pDebug;
	mutable u16								m_RefCount;
	mutable u16								m_Flags;

	static int								m_DynamicallyAllocated;

	// We should NOT be implicitly copying these around.  All kinds of bad things will happen to the
	// linked list.
	fwExtRefAwareBase( const fwExtRefAwareBase & ) {}
	fwExtRefAwareBase &operator=( const fwExtRefAwareBase & ) { return *this; }

};
CompileTimeAssertSize(fwExtRefAwareBase, 16, 32);

} //	namespace rage

#endif

#endif //	FWTL_EXTREFAWAREBASE_H_
