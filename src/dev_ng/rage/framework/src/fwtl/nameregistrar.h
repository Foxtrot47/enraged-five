//
// fwtl/nameregistrar.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef __FWTL_NAMEREGISTRAR_H__
#define __FWTL_NAMEREGISTRAR_H__

#include "atl/map.h"
#include "diag/trap.h"

namespace rage {

// fwNameRegistrar: essentially a specialized hashmap
// Originally it was created for performance reasons on old 32bit platforms, and had 16bit size limit.
// Once this size limit was approached (and eventually exceeded), the size was changed to a 32bit value, though it's unlikely we'll go beyond 18bits in practical use.

// For ease of maintenance and support on future projects and platforms, we should consider changing the implementation to a standard atHashMap or similar.
// atHashMap will have better support and performance once we have many large fwNameRegistrars (over 16bit sizes), but we could also borrow some of it's functionality if we still desire the specialization.

template<typename _KeyType>
class fwNameRegistrarDef {
public:
	fwNameRegistrarDef();
	fwNameRegistrarDef(u32 size);
	~fwNameRegistrarDef();

	void Init(u32 size);
	void Reset();

	int Lookup(_KeyType key) const;
	void Insert(_KeyType key,u32 value);
	void Delete(_KeyType key);

	bool IsInitialized() const { return m_Hash && m_Buckets; }

	u32 GetCount() { return(m_entries); }

private:
	static const u32 NONE = 0xFFFFFFFF;
	struct Bucket { _KeyType hash; u32 value; u32 next; };
	Bucket *m_Buckets;
	u32 *m_Hash;
	u32 m_HashCount, m_FirstFreeBucket;
	u32 m_entries;
};

typedef fwNameRegistrarDef<u32> fwNameRegistrar;

template<typename _KeyType>
__forceinline int fwNameRegistrarDef<_KeyType>::Lookup(_KeyType key) const
{
	// Empty registrar?
	TrapEQ(m_HashCount,0);

	// Locate the slot that might contain this key
	u32 bucket = m_Hash[key % m_HashCount];

	while (bucket != NONE) {
		const Bucket& b = m_Buckets[bucket];
		if (b.hash != key)
		{
			bucket = b.next;
		}
		else
		{
			return b.value;
		}
	}
	return -1;
}

template<typename _KeyType>
fwNameRegistrarDef<_KeyType>::fwNameRegistrarDef()
	: m_Buckets(NULL)
	, m_Hash(NULL)
	, m_HashCount(0)
	, m_FirstFreeBucket(0)
	, m_entries(0)
{
}


template<typename _KeyType>
fwNameRegistrarDef<_KeyType>::fwNameRegistrarDef(u32 size) : m_Hash(NULL), m_Buckets(NULL)
{
	Init(size);
}

template<typename _KeyType>
fwNameRegistrarDef<_KeyType>::~fwNameRegistrarDef()
{
	Reset();
}

template<typename _KeyType>
void fwNameRegistrarDef<_KeyType>::Init(u32 size)
{
	Assert(!m_Hash && !m_Buckets);
	FatalAssert(size > 0 && size < NONE);	// bail gracefully if size is bad, though other things would have failed by now; also, NONE is used for convenience, and should actually be a much lower MAX_.. value (unlikely we'll want to approach a 32bit size without redoing the hashmap code below)
	// The divide down here is somewhat arbitrary; the bigger this is, the less memory
	// we will consume in the toplevel lookup, but the longer our hash chains will get on average.
	const u32 hashSizeRaw = AssertVerify((size/4) <= ATL_MAP_MAX_SIZE) ? (u32)(size/4) : ATL_MAP_MAX_SIZE;	// clamp the hash table size if the pool size or divisor push it out of range
	m_HashCount = atHashNextSize((unsigned short)hashSizeRaw);
	m_Hash = rage_new u32[m_HashCount];
	m_Buckets = rage_new Bucket[size];
	// The memset below only works by coincidence; it does not initialize the array correctly if NONE is any other value, in which case a manual init should be used:
	//for (u32 i=0; i<m_HashCount; i++)
	//	m_Hash[i] = NONE;
	// However, we'll keep the memset here, assuming it's unlikely NONE will change without a larger implementation overhaul..
	memset(m_Hash,NONE,sizeof(u32) * m_HashCount);
	memset(m_Buckets,0,sizeof(Bucket) * size);
	// Construct the bucket free list.
	m_FirstFreeBucket = 0;
	for (u32 i=0; i<size-1; i++)
		Assign(m_Buckets[i].next,i+1);
	m_Buckets[size-1].next = NONE;
	m_entries = 0;
}

template<typename _KeyType>
void fwNameRegistrarDef<_KeyType>::Reset()
{
	delete[] m_Buckets;
	m_Buckets = NULL;
	delete[] m_Hash;
	m_Hash = NULL;
	m_HashCount = 0;
	m_FirstFreeBucket = NONE;
	m_entries = 0;
}

template<typename _KeyType>
void fwNameRegistrarDef<_KeyType>::Insert(_KeyType key,u32 value)
{
	Assert(m_HashCount);
	Assertf(Lookup(key) == -1,"Key %u already in table with value %d, now %u",(u32)key,Lookup(key),value);

	// Registrar full?  Shouldn't ever happen if kept sized to same size as pool
	if (m_FirstFreeBucket == NONE) {
		Quitf(0, "Name registrar is full - call Init() with a larger size");
	}

	u32 hashIdx = key % m_HashCount;
	Bucket &n = m_Buckets[m_FirstFreeBucket];
	// Remember next free bucket
	u32 nextFree = n.next;
	// Update the new entry, patched into current chain
	n.hash = key;
	Assign(n.value,value);
	n.next = m_Hash[hashIdx];
	// Patch head of chain to this entry
	m_Hash[hashIdx] = m_FirstFreeBucket;
	// Update new free bucket.
	m_FirstFreeBucket = nextFree;
	m_entries++;
}

template<typename _KeyType>
void fwNameRegistrarDef<_KeyType>::Delete(_KeyType key)
{
	Assert(m_HashCount);

	u32 hashIdx = key % m_HashCount;
	u32 *prevBucket = &m_Hash[hashIdx];
	u32 bucket = m_Hash[hashIdx];
	while (bucket != NONE) {
		Bucket &b = m_Buckets[bucket];
		if (b.hash == key) {
			m_entries--;
			// Patch self out of chain; this may mark the entire chain free.
			*prevBucket = b.next;
			b.hash = (_KeyType)0;
			b.value = 0;
			b.next = m_FirstFreeBucket;
			m_FirstFreeBucket = bucket;
			return;
		}
		else {
			prevBucket = &b.next;
			bucket = b.next;
		}
	}

	Assertf(false,"fwNameRegistrar::Delete - Key %lu wasn't found in the registrar", (u64)key);
}

} // namespace

#endif // __FWTL_NAMEREGISTRAR_H__