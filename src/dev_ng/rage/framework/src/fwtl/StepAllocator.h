//
// Simple step allocator, especially suited for SPU scratch buffer
// - handy, cheap, with super low overhead;
// - allows only incremental allocs (hence it's name ;);
//
// 24/11/2011	-	 Andrzej:	- initial;
// 19/03/2012	- Alessandro:	- moved to framework;
//
//
//

#ifndef _INC_STEPALLOCATOR_H_
#define _INC_STEPALLOCATOR_H_

#include "system/new.h"

#ifndef FALSE
const int FALSE = 0;
#endif
#ifndef TRUE
const int TRUE = 1;
#endif

namespace rage {

class fwStepAllocator
{
public:
	fwStepAllocator()							{ m_Ptr = 0; m_SizeLeft = 0;	}
	fwStepAllocator(void *ptr, u32 size)		{ Init(ptr, size);				}
	~fwStepAllocator()							{								}

	void Init(void *ptr, u32 size)
	{
		m_Ptr = (u8*)ptr;
		Assert(m_Ptr);

		m_SizeLeft = size;
		Assert(m_SizeLeft > 0);
	}

	void*	Alloc(u32 size)
	{
		if(size <= m_SizeLeft)
		{
			u8 *p		= m_Ptr;
			m_Ptr		+= size;
			m_SizeLeft	-= size;
			return p; 
		}
		
		Assertf(FALSE, "\n Failed to allocate %dbytes (only %" SIZETFMT "dbytes available in scratch).", size, m_SizeLeft);
		return NULL;
	}

	void* TryAlloc(u32 size)
	{
		if(size <= m_SizeLeft)
		{
			u8 *p		= m_Ptr;
			m_Ptr		+= size;
			m_SizeLeft	-= size;
			return p; 
		}

		return NULL;
	}

	void*	Alloc(u32 size, u32 align)
	{
		const size_t fillup = ((size_t(m_Ptr)+size_t(align-1)) & ~size_t(align-1)) - size_t(m_Ptr);	// alignment fillup

		if((size+fillup) <= m_SizeLeft)
		{
			m_Ptr		+= fillup;
			m_SizeLeft	-= fillup;
			return Alloc(size);
		}

		Assertf(FALSE, "\n Failed to allocate %dbytes aligned at %d (only %" SIZETFMT "dbytes available in scratch).",size,align,m_SizeLeft);
		return NULL;
	}

	template <typename Type>
	Type* Alloc(const u32 count, const u32 align) {
		return reinterpret_cast< Type* >( Alloc( sizeof(Type) * count, align ) );
	}

	template <typename Type>
	Type* Alloc(const u32 count) {
		return reinterpret_cast< Type* >( Alloc( sizeof(Type) * count ) );
	}

private:
	u8*		m_Ptr;		// current ptr to next free chunk
	size_t	m_SizeLeft;	// size of free mem left
};

} // namespace rage

#endif // !defined _INC_STEPALLOCATOR_H_
