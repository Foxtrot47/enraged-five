//
//	fwtl\DebugOwnerHistory.h
//	
//	Copyright (C) 2008-2011 Rockstar Games.  All Rights Reserved.
//
//
#ifndef FWTL_DEBUGOWNERHISTORY_H_
#define FWTL_DEBUGOWNERHISTORY_H_

#include "atl\array.h"
#include "atl\string.h"

namespace rage
{

// PURPOSE
//  This class tracks the addition and removal of pointer references to an object with
//  full call-stack information.  It coalesces matched callstacks, operations + addresses
//  to minimise space.
//
// USAGE
//  You need to embed one of these in the instance, allocating it on first reference
//			fwDebugOwnerHistory::fwDebugOwnerHistoryFrame *mpHead
//
//  Then after that point:
//	  fwDebugOwnerHistory::fwDebugOwnerHistoryMovie mov;
//	  mov.Init( *mpHead );
//
//  Handle mov.IsNearingFull() -> debug info [see shortly]
//  Otherwise:
//    mov.AddRef( p_p_us, registered backtrace index );
//    mov.RemoveRef( p_p_us, registered backtrace index );
//      where p_p_us is a pointer address of a pointer to our instance
//
//  The movie will modify the head as necessary should it need to create new frames.
//
//  Then when you need to display the digest:
//    mov.Init( *mpHead );
//    mov.PrintDigest( <t/f> - if we are in a destructor )
//
// NOTES:
//  For a good use case - see ExtRefAwareBase.h/.cpp

// PURPOSE:
//
// Stores a compressed operation
//
class fwDebugOwnerHistoryCompressedOp
{
public:
	enum Constants
	{
		OFFSET_BITS			= 8,
		MAX_OFFSET			= (1<<OFFSET_BITS)-1
	};

	fwDebugOwnerHistoryCompressedOp();

	union
	{
		struct
		{
			u32			m_Op	: 2;			// true - add; false - remove
			u32			m_Next	: 3;			// next operation
			u32			m_Count : 4;			// number of times we hit this exact op
			u32			__pad	: 7;
			u32			m_Addr	: OFFSET_BITS;	// offset to the stored addr
			u32			m_PC	: OFFSET_BITS;	// offset to the PC
		} m_Component;

		size_t m_All;
	};
};

CompileTimeAssertSize( fwDebugOwnerHistoryCompressedOp, 4, 8 );

class fwDebugOwnerHistoryMovie;

// PURPOSE:
//
// Represents a reconstructed operation
//
class fwDebugOwnerHistoryOp
{
public:

	typedef void*		tPtr;
	typedef void**		tPtrPtr;
	typedef u32			tPC;

	enum Constants
	{
		MAX_OP_COUNTS		= 15
	};


	fwDebugOwnerHistoryOp();

	enum eOperation
	{
		Invalid,
		New,
		Own,
		Relinquish
	};

	// PURPOSE:
	//
	//  Because we may combine operations, it returns the new 
	//  global index
	int ExpandFrom( const fwDebugOwnerHistoryMovie & movie, int globalIndex, bool noCombo );

	eOperation	m_Operation;
	tPtrPtr		m_Addr1;
	tPtrPtr		m_Addr2;
	tPC			m_PC;	
	int			m_Count;

};


// PURPOSE:
//
//  Stores a frames worth of information
//
class fwDebugOwnerHistoryFrame
{
public:
	enum Constants
	{
		FRAME_DATA_SIZE		= 19		// * 4 bytes
	};

	fwDebugOwnerHistoryFrame();

	fwDebugOwnerHistoryFrame *GetNext();
	int		GetCount()		const;
	bool	IsFull()		const;

	void	Push( size_t id );

	size_t &operator[]( int index );
	size_t	operator[]( int index ) const;

	void	SetNext( fwDebugOwnerHistoryFrame & next );

private:

	inline bool IsNextValid() const;

	size_t				m_Data[FRAME_DATA_SIZE];
	union
	{
		int				m_Cursor;
		fwDebugOwnerHistoryFrame *		m_pNext;
	};
};
CompileTimeAssertSize( fwDebugOwnerHistoryFrame, 80, 160 );

// PURPOSE:
//
//  Caches multiple frames worth of information from their linked
//  list for speedy operations
//
// NOTE:
//
//  This shouldn't be stored, but instead worked out on the stack.
//
class fwDebugOwnerHistoryMovie
{
public:
	typedef void*		tPtr;
	typedef void**		tPtrPtr;
	typedef u32			tPC;

	void			Init( fwDebugOwnerHistoryFrame & startFrame );

	bool			IsNearingFull() const;

	void			SetCreationPoint( tPtr addr, tPC loc );
	void			AddRef( tPtrPtr addr, tPC loc );
	void			RemoveRef( tPtrPtr addr, tPC loc );

	fwDebugOwnerHistoryCompressedOp	GetOp( int globalIndex ) const;

	size_t			operator[]( int globalIndex) const;

	tPtrPtr			GetLongestOwner( int globalIndexStop) const;

	void			PrintDigest( bool inDestructor ) const;

private:

	bool			IsRelinquished( tPtrPtr addr, int startOpIndex, int endIndex ) const;

	inline int		GetFrameCount()				const;
	inline int		GetDataCount()				const;
	inline int		GetFirstOperation()			const;

	int				Find( size_t id )				const;

	// Match on PC id, addr1 + operation => repeated
	int				Find( const fwDebugOwnerHistoryOp &op )		const;

	size_t &		operator[]( int globalIndex );

	void			PushAddingFrameIfNecessary( size_t dat );
	int				GetDeltaPushIfNecessary( size_t dat );

	void			Push( fwDebugOwnerHistoryOp::eOperation op, tPtrPtr addr, tPC pc );

	enum Constants
	{
		MAX_DEBUG_DATA_FRAMES = 8
	};

	// This is stack based - so this should be fine.  Stored the linked list
	// of frames as an array for ease of global index determination
	atFixedArray< fwDebugOwnerHistoryFrame*, MAX_DEBUG_DATA_FRAMES >		m_FramePtrs;

	// Global data count, from all frames x frame count + the extra in 
	// the last frame
	int					m_DataCount;
};


// Inline methods /////////////////////////////////////////////////////////

bool fwDebugOwnerHistoryFrame::IsNextValid() const
{
	// This is the valid range for __int__ cursor.  If it's outside this it's
	// an address and the next pointer (sharing it's space) is therefore valid.
	return ( m_Cursor < 0 ) || ( m_Cursor > FRAME_DATA_SIZE );
}

int fwDebugOwnerHistoryMovie::GetDataCount() const
{
	return m_DataCount;
}

int fwDebugOwnerHistoryMovie::GetFrameCount() const
{
	return m_FramePtrs.GetCount();
}

int fwDebugOwnerHistoryMovie::GetFirstOperation() const
{
	// PC -> Addr1 -> Op (hence will always be index 2 if we have at least 1 entry)
	return ( m_DataCount < 3 ) ? -1 : 2;
}

typedef void (*fwtDebugOwnerHistoryResolver)( const void *p_Thing, atString &retName, 
											  bool outsideDestructor, bool isPointAndContentsWanted );

void DebugOwnerHistoryResolveAddress( const void *addr, atString & retName );
void SetDebugOwnerHistoryResolver( fwtDebugOwnerHistoryResolver resolveFunc );

} // namespace rage

#endif // FWTL_DEBUGOWNERHISTORY_H_
