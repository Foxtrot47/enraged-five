//
//	fwtl\regdrefs.cpp
//	
//	Copyright (C) 2008-2009 Rockstar Games.  All Rights Reserved.
//
#include "regdrefs.h"
#include "system/param.h"
#include "system/stack.h"

namespace rage {


FW_INSTANTIATE_CLASS_POOL(fwKnownRefHolder, CONFIGURED_FROM_FILE, atHashString("Known Refs",0xd8d80132));

fwKnownRefHolder::fwKnownRefHolder(	
#if REGREF_VALIDATION
									const fwRefAwareBaseImpl<datBase>* pReference,
#endif //	REGREF_VALIDATION
									void** ppReference,
									fwKnownRefHolder* pNext)
	: //	Initializer list.
#if REGREF_VALIDATION
	m_pReference(pReference),
#endif //	REGREF_VALIDATION
	m_ppReference(ppReference),
	m_pNext(pNext)
{
}

#if REGREF_VALIDATE_CDCDCDCD
PARAM(RegrefValidation, "Enable debug regref validation code");

static const size_t s_MaxInterestingObjects = 16;
static void* s_InterestingObjects[s_MaxInterestingObjects];
static size_t s_NumInterestingObjects = 0;
static atMap<size_t, u16> s_regrefDebugMap[s_MaxInterestingObjects];

void fwRefAwareBaseImpl_AddInterestingObject(void* pThis)
{
	if(!PARAM_RegrefValidation.Get())
	{
		return;
	}

	Assertf(s_NumInterestingObjects < s_MaxInterestingObjects, "Too many interesting objects!");
	if(s_NumInterestingObjects < s_MaxInterestingObjects)
	{
		s_InterestingObjects[s_NumInterestingObjects++] = pThis;
	}
}

void fwRefAwareBaseImpl_RemoveInterestingObject(void* pThis)
{
	if(!PARAM_RegrefValidation.Get())
	{
		return;
	}

	for(size_t i=0; i<s_NumInterestingObjects; ++i)
	{
		if(s_InterestingObjects[i] == pThis)
		{
			s_regrefDebugMap[i].Kill();
			s_InterestingObjects[i] = s_InterestingObjects[s_NumInterestingObjects-1];
			s_InterestingObjects[s_NumInterestingObjects-1] = NULL;
			--s_NumInterestingObjects;
			break;
		}
	}
}

void fwRefAwareBaseImpl_OnAddRef(void* pThis, void** ppReference)
{
	if(!PARAM_RegrefValidation.Get())
	{
		return;
	}

	for(size_t i=0; i<s_NumInterestingObjects; ++i)
	{
		if(s_InterestingObjects[i] == pThis)
		{
			size_t ref = (size_t)ppReference;
			if(s_regrefDebugMap[i].Access(ref))
			{
				Displayf("Ref already in list?");
			}
			u16 backtrace = sysStack::RegisterBacktrace();
			s_regrefDebugMap[i].Insert(ref, backtrace);
		}
	}
}

void fwRefAwareBaseImpl_OnRemoveRef(void* pThis, void** ppReference)
{
	if(!PARAM_RegrefValidation.Get())
	{
		return;
	}

	for(size_t i=0; i<s_NumInterestingObjects; ++i)
	{
		if(s_InterestingObjects[i] == pThis)
		{
			size_t ref = (size_t)ppReference;
			bool ret = s_regrefDebugMap[i].Delete(ref);
			if(!ret)
			{
				Displayf("Ref %p for object %p not in list (%d entries)?", pThis, (void*)ref, s_regrefDebugMap[i].GetNumUsed());
			}
		}
	}
}

void fwRefAwareBaseImpl_OnBadRef(void* pThis, void** ppReference)
{
	if(!PARAM_RegrefValidation.Get())
	{
		return;
	}

	for(size_t i=0; i<s_NumInterestingObjects; ++i)
	{
		if(s_InterestingObjects[i] == pThis)
		{
			size_t ref = (size_t)ppReference;
			u16* backtrace = s_regrefDebugMap[i].Access(ref);
			Assertf(backtrace, "No backtrace found for ref %p!", ppReference);
			Displayf("Invalid ref %p! Points at %p, not %p!", ppReference, *ppReference, pThis);
			if(backtrace)
			{
				sysStack::PrintRegisteredBacktrace(*backtrace);
			}
		}
	}
}

void fwRefAwareBaseImpl_OnClearAllRefs(void* pThis)
{
	if(!PARAM_RegrefValidation.Get())
	{
		return;
	}

	for(size_t i=0; i<s_NumInterestingObjects; ++i)
	{
		if(s_InterestingObjects[i] == pThis)
		{
			s_regrefDebugMap[i].Kill();
		}
	}
}
#endif // REGREF_VALIDATE_CDCDCDCD


} //	namespace rage
