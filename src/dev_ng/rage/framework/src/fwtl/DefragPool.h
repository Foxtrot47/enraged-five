/////////////////////////////////////////////////////////////////////////////////
//
// FILE :    fwtl/DefragPool.h
// PURPOSE : Provides a defragging pool storage for quad tree data
// AUTHOR :  Ian Kiigan
// CREATED : 23/01/09
//
/////////////////////////////////////////////////////////////////////////////////

#ifndef _INC_DEFRAGPOOL_H_
#define _INC_DEFRAGPOOL_H_

#include "atl/array.h"
#include "fwscene/world/EntityDesc.h"
#include "fwutil/PtrList.h"

#define DEFRAGPOOL_SANITYCHECK	(!__FINAL && 0)
#define DEFRAGPOOL_DEBUG		(!__FINAL)

namespace rage {

// represents a gap in the pool allocations
typedef struct
{
	u32	nIndex;
	u32	nLength;

} DefragPool_SGapInfo;

// return type for resize operations
typedef enum
{
	DEFRAGPOOL_RESIZERESULT_SUCCESS,
	DEFRAGPOOL_RESIZERESULT_FAIL,
	DEFRAGPOOL_RESIZERESULT_DELETED,
	DEFRAGPOOL_RESIZERESULT_NOCHANGES

} DefragPool_eResizeResult;


// handle to an array of entries allocated from a fwDescPool.
class fwDescPoolAlloc
{
	friend class fwDescPool;

public:
	inline fwEntityDesc* GetEntries() const { return m_pStorage; }
	inline u32 GetNumEntries() const { return m_nNumEntries; }
	inline u32 GetStorageIndex() const { return m_nStorageIndex; }

private:
	// private ctor & dtor, should be managed by fwDescPool
	fwDescPoolAlloc() { }
	fwDescPoolAlloc(const u32 nNumEntries, fwEntityDesc* const pStorage, const u32 nIndex)
		: m_nNumEntries(nNumEntries), m_pStorage(pStorage), m_nStorageIndex(nIndex) { }
	~fwDescPoolAlloc() { }

	fwPtrNodeSingleLink *	m_pListNode;
	u32 					m_nNumEntries;
	u32						m_nStorageIndex;
	fwEntityDesc*			m_pStorage;
};

// a simple template pool class that supports incremental and full
// defragmentation operations. Designed for the allocation of arrays
// of entries (single element allocation is permitted, but slow).
class fwDescPool
{
public:
	//-- public interface -----------------------------------------------------------------
	fwDescPool(const u32 nMaxEntries, const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT);
	fwDescPool(const u32 nMaxEntries, const u32 nAlignment, const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT);
	 ~fwDescPool();
	void Reset();
	fwDescPoolAlloc* New(const u32 nNumEntries);
	void Delete(fwDescPoolAlloc* const pAlloc);
	DefragPool_eResizeResult Resize(fwDescPoolAlloc* const pAlloc, const u32 nNewSize);
	DefragPool_eResizeResult Grow(fwDescPoolAlloc* const pAlloc, const u32 nAmount = 1);
	DefragPool_eResizeResult RemoveRange(fwDescPoolAlloc* const pAlloc, const u32 nStartIndex, const u32 nNumEntries);
	void DefragPartial();
	void DefragPartialNew();
	void DefragFull();
	inline u32 GetMaxEntries() { return m_nMaxEntries; }
	//-------------------------------------------------------------------------------------

#if __BANK
	const atArray<DefragPool_SGapInfo>& GetGapInfo() const { return m_asGapInfo; }
#endif // __BANK

private:
	bool IndexIsInGap(const u32 nIndex, u32 * const pnGapArrayIndex) const;
	bool FindGap(const u32 nLength, u32 * const pnGapArrayIndex) const;
	void RemoveGap(const u32 nGapArrayIndex);
	void AddGap(const u32 nGapStorageIndex, const u32 nLength);
	void DeleteAllocHandles();
	inline bool WithinRange(u32 nValue, u32 nBottom, u32 nTop) { return ((nValue)>=(nBottom) && (nValue)<=(nTop)); }

	u32 m_nMaxEntries;
	// u32 m_nLastCreatedGapIndex;
	fwEntityDesc* m_pStorage;
	atArray<DefragPool_SGapInfo> m_asGapInfo; // TODO turn this into a linked list to avoid expensive memory copies on insert
	fwPtrListSingleLink m_AllocList;

	// debug functions, handy for various expensive asserts etc
#if DEFRAGPOOL_DEBUG
public:
	void DebugPrintGaps();
	void DebugPrintAllocs();
#endif	//DEFRAGPOOL_DEBUG

#if DEFRAGPOOL_SANITYCHECK
private:
	bool VerifyOrderedGaps();
	bool OverlapsWithAllocated(const u32 nIndex, const u32 nLength);
	bool OverlapsWithGap(const u32 nIndex, const u32 nLength);
	u32 m_nNumAllocs;
#endif	//DEFRAGPOOL_SANITYCHECK
};

} // namespace rage

#endif // !defined _INC_DEFRAGPOOL_H_
