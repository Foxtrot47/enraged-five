//
//	fwtl\regdrefs.h
//	
//	Copyright (C) 2008-2009 Rockstar Games.  All Rights Reserved.
//

//
//	This is the RegdRef system.  It is a pair of classes that help deal with the
//	common problem of objects you have local pointers to being deleted underneath
//	you and then that leading to subsequent bugs (when the pointers are used).
//
//	It works by having you use a fwRegdRef<T> to keep track of the object instead
//	of a normal pointer (T*) and making sure that the type of object being
//	referenced (pointed at) derives from fwRefAwareBase.
//
//	When the above is done then you can be assured that when the referenced object
//	is deleted by anyone anywhere that your fwRegdRef<T> will be informed and
//	updated (to NULL).  You can, and still should, check your fwRegdRef<T> for
//	NULL before using it though.
//
//	A fwRegdRef<T> (through C++ trickery) acts exactly like a normal raw pointer
//	(T*) in almost every other way and should be a painless drop in replacement for
//	the old pointer.  Just replace the pointer type in it's declaration with the
//	fwRegdRef<T> version of the type- you shouldn't have to change any other code.
//	If you do find you have any issues, just use the .Get() function of the
//	fwRegdRef<T> to get the actual raw stored pointer.
//
//	For the class being referenced (pointed to) just make sure that it inherits
//	from fwRefAwareBase. 
//	This adds a single pointer worth of data to your class and a vtable (if your
//	class didn't have one already).
//
//	The system is (crudely) thread safe and uses a nicely debug-able pool for the
//	known references. 
//
//	Use this something like:
//
//	#include "fwtl\regdrefs.h"
//
//	class MyClass : public fwRefAwareBase
//	{public:
//		MyClass(int foo) : m_foo(foo){}
//		~MyClass(){}
//		int m_foo;
//	};
//
//	void SomeFunction(){
//		MyClass* pMyClass = new MyClass(23);
//		fwRegdRef<MyClass> refMyClass(pMyClass);
//
//		//	The following outputs "Ref contains the number 23.".
//		printf("Ref contains the number %d.", refMyClass->m_foo);
//		delete pMyClass;
//
//		//	The following outputs "Ref is null.".
//		if(refMyClass){printf("Error!");}
//		else{printf("Ref is null.");}
//	}
//
//	class MyOtherClass
//	{public:
//		MyOtherClass(MyClass* pMyClass) : m_refMyClass(pMyClass){}
//		~MyOtherClass(){}
//		void Print(){
//			if(m_refMyClass){
//				printf("Ref contains the number %d.", m_refMyClass->m_foo);
//			}else{
//				printf("Ref is null.");
//			}
//		}
//		fwRegdRef<MyClass>	m_refMyClass;
//	};
//
//	void SomeOtherFunction(){
//		MyClass* pMyClass = new MyClass(23);
//		MyOtherClass otherClass(pMyClass);
//
//		//	The following outputs "Ref contains the number 23.".
//		otherClass.Print();
//		delete pMyClass;
//
//		//	The following outputs "Ref is null.".
//		otherClass.Print();
//	}
//
#ifndef FWTL_REGDREFS_H_
#define FWTL_REGDREFS_H_

//	Rage headers
#include "data/base.h"
#include "fwtl/pool.h"
#include "fwtl/poolcriticalsection.h"
#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/threadtype.h"

namespace rage {

//	This is following forward declaration is necessary because this and
//	fwKnownRefHolder are circularly dependent on each other (and both are declared
//	in this file).
template<class _Base> class fwRefAwareBaseImpl;

#define REGREF_VALIDATION 0
#define REGREF_VALIDATE_CDCDCDCD 0 //__DEV

#define fwRegdRefCriticalSection fwPoolCriticalSection<fwKnownRefHolder>
#define REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK fwRegdRefCriticalSection cs;

//
// PURPOSE
//	Useful in the implementation of fwRefAwareBase and fwRegdRef, but should
//	never be explicitly used elsewhere.
// NOTES
//	It only appears in the header so that its pool functions can be initialized
//	and used for debugging in the same places as other pools.
//	The static pool functions are useful for debugging.
//
class fwKnownRefHolder
{
public:
	AT_REGISTER_LOCKING_CLASS_POOL(fwKnownRefHolder, fwRegdRefCriticalSection);

	//
	// PURPOSE
	//	The default and only constructor.  It stores the address of the
	//	ref and a pointer to the next known ref in the list.
	// PARAMS
	//	pReference	-	What the reference is pointing to.
	//	ppReference	-	The address of the reference.
	//	pNext		-	The next fwKnownRefHolder in the list.
	//
	fwKnownRefHolder(	
#if REGREF_VALIDATION
		const fwRefAwareBaseImpl<datBase>* pReference,
#endif //	REGREF_VALIDATION
		void** ppReference,
		fwKnownRefHolder* pNext);

#if REGREF_VALIDATION
	const fwRefAwareBaseImpl<datBase>*	m_pReference;
#endif //	REGREF_VALIDATION
	void**					m_ppReference;
	fwKnownRefHolder*		m_pNext;
};

// When turning fwRefAwareBase into a template, the code size exploded on PS3,
// presumably mostly due to AddKnownRef()/RemoveKnownRef() getting inlined.
// Use of the noinline attribute seems effective at preventing that.
#if __PPU
#define NOINLINE __attribute__((noinline))
#else
#define NOINLINE
#endif

#if REGREF_VALIDATE_CDCDCDCD
	void fwRefAwareBaseImpl_AddInterestingObject(void* pThis);
	void fwRefAwareBaseImpl_RemoveInterestingObject(void* pThis);
	void fwRefAwareBaseImpl_OnAddRef(void* pThis, void** ppReference);
	void fwRefAwareBaseImpl_OnRemoveRef(void* pThis, void** ppReference);
	void fwRefAwareBaseImpl_OnBadRef(void* pThis, void** ppReference);
	void fwRefAwareBaseImpl_OnClearAllRefs(void* pThis);
#endif // REGREF_VALIDATE_CDCDCDCD

//
// PURPOSE
//	Template for a base class that your reference aware objects should derive
//	from.
// NOTES
//	This will add a single pointer to the size of your objects.
//	That pointer will be used as the head of a linked list of
//	known pointers that point to your object.  When your object has its
//	destructor called it will run down that list and null all the known
//	pointers that point to itself.
//
//	You could use raw pointers to your object and manage them manually by
//	using the AddKnownRef and RemoveKnownRed functions, but instead you
//	will find it much safer and easier to have this done for by the fwRegdRef
//	class defined below.
//
//	The class also has a few possibly useful debugging functions and data.
//
template<class _Base> class fwRefAwareBaseImpl : public _Base
{
public:

	//
	// PURPOSE
	//	The default constructor.  Makes sure this object starts clear of
	//	any tracked refs.
	//
	NOINLINE fwRefAwareBaseImpl();

	//
	// PURPOSE
	//	The destructor.  Auto-nulls all the known pointers that point to
	//	this object.  This is done to prevent dangling pointers
	//	throughout the system.
	//
	NOINLINE ~fwRefAwareBaseImpl();

	//
	// PURPOSE
	//	Add the provided pointer to the list of pointers that need to be
	//	updated if this object is deleted.
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	NOINLINE void	AddKnownRef					(void** ppReference) const;

	//
	// PURPOSE
	//	Remove the provided pointer from the list of pointers that need
	//	to be updated if this object is deleted.
	// PARAMS
	//	ppReference	-	The address of the reference.
	//
	NOINLINE void	RemoveKnownRef				(void** ppReference) const;

	//
	// PURPOSE
	//	Auto-nulls all the known pointers that point to this object.
	// NOTES
	//	This is done to prevent dangling pointers throughout the system.
	//	It is used directly by the destructor, but is also useful for
	//	objects whose storage is re-used instead of using delete/new to
	//	create a new one.
	//
	NOINLINE void	ClearAllKnownRefs			(void) const;

	//
	// PURPOSE
	//	Determine if the ref (which should be pointing to this) is one
	//	that we are already aware of and tracking or not.
	// PARAMS
	//	ppReference	-	The address of the reference.
	// RETURNS
	//	Whether or not it is known (in the list on know refs).
	//
	NOINLINE bool	IsRefKnown					(void** ppReference) const;

	//
	// PURPOSE
	//	Determine if there are any known references in the list.
	// RETURNS
	//	Whether or not there are know references in the list.
	//
	bool	IsReferenced				(void) const;

#if REGREF_VALIDATION
	//
	// PURPOSE
	//	Make sure that the tracked pointer is actually pointing here.
	//	If not then something has gone wrong.
	// PARAMS
	//	pReference	-	The supposed reference.
	//	ppReference	-	The address of the reference.
	// RETURNS
	//	Whether or not the ref is valid.
	//
	NOINLINE bool	ValidateRef					(const void* pReference, void** ppReference) const;

	//
	// PURPOSE
	//	Make sure that all the tracked pointers are actually pointing
	//	here.  If not then something has gone wrong.
	// RETURNS
	//	Whether or not all the known refs are valid.
	//
	NOINLINE bool	ValidateAllKnownRefs		(void) const;

	//
	// PURPOSE
	//	To determine how many tracked refs there are to this object.
	// RETURNS
	//	The number of known refs to this object.
	//
	NOINLINE int		CountAllKnownRefs			(void) const;

	//
	// PURPOSE
	//	Remove all the tracked pointers that aren't valid (pointing
	//	to this).
	// RETURNS
	//	Whether or not all the known refs are valid.
	//
	NOINLINE bool	RemoveAllInvalidKnownRefs	(void) const;
#endif //	REGREF_VALIDATION

#if REGREF_VALIDATE_CDCDCDCD
	//
	// PURPOSE
	//	Make sure that all the tracked pointers are not 0xcdcdcdcdcdcdcdcd. There
	//  seems to be an issue currently where these garbage pointers get into the reflist.
	NOINLINE void	ValidateAllKnownRefsNotCDCDCD		(void) const;
#endif // REGREF_VALIDATE_CDCDCDCD

protected:
	mutable fwKnownRefHolder*	m_pKnownRefHolderHead;

private:
	// We should NOT be implicitly copying these around.  All kinds of bad things will happen to the
	// linked list.
	fwRefAwareBaseImpl( const fwRefAwareBaseImpl & )				{}
	fwRefAwareBaseImpl &operator=( const fwRefAwareBaseImpl & )		{ return *this; }
};

template<class _Base> NOINLINE fwRefAwareBaseImpl<_Base>::fwRefAwareBaseImpl()
	: //	Initializer list.
	m_pKnownRefHolderHead(0)
{
}


template<class _Base> NOINLINE fwRefAwareBaseImpl<_Base>::~fwRefAwareBaseImpl()
{
	//	Clear all the refs as this object is ceasing to exist and we
	//	don't want any dangling pointers to it.
	ClearAllKnownRefs();
}


template<class _Base> NOINLINE void fwRefAwareBaseImpl<_Base>::AddKnownRef(void** ppReference) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

#if REGREF_VALIDATION
	//	Validate the incoming ref.
	ValidateRef(*ppReference, ppReference);

	//	Make sure our current list of known refs is valid.
	ValidateAllKnownRefs();
#endif //	REGREF_VALIDATION

#if REGREF_VALIDATE_CDCDCDCD
	ValidateAllKnownRefsNotCDCDCD();
	fwRefAwareBaseImpl_OnAddRef((void*)this, ppReference);
	Assertf((size_t)*ppReference != 0xcdcdcdcdcdcdcdcd, "fwRefAwareBase::AddKnownRef: Reference %p is %p!", this, ppReference);
#endif // REGREF_VALIDATE_CDCDCDCD

	//	Make sure the ref isn't already registered.
	Assertf(!IsRefKnown(ppReference), "fwRefAwareBase::AddKnownRef: Ref is already known to this ReferenceAwareBase! %p, %p", *ppReference, ppReference);

	//	Create (get from the pool) a new fwKnownRefHolder to keep track
	//	of this ref.
	//	Make sure the holder has its next pointer point to the head of
	//	list of KnownRefHolders.
	fwKnownRefHolder* pNewHeadRef = rage_new fwKnownRefHolder(
#if REGREF_VALIDATION
		this,
#endif //	REGREF_VALIDATION
		ppReference,
		m_pKnownRefHolderHead);

	//	Make the new fwKnownRefHolder be the the head of the list.
	m_pKnownRefHolderHead = pNewHeadRef;
}


template<class _Base> NOINLINE void fwRefAwareBaseImpl<_Base>::RemoveKnownRef(void** ppReference) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

#if REGREF_VALIDATION
	//	Validate the incoming ref.
	ValidateRef(*ppReference, ppReference);

	//	Make sure our current list of known refs is valid.
	ValidateAllKnownRefs();
#endif //	REGREF_VALIDATION

#if REGREF_VALIDATE_CDCDCDCD
	ValidateAllKnownRefsNotCDCDCD();
	fwRefAwareBaseImpl_OnRemoveRef((void*)this, ppReference);
	Assertf((size_t)*ppReference != 0xcdcdcdcdcdcdcdcd, "fwRefAwareBase::RemoveKnownRef: Reference %p is %p!", this, ppReference);
#endif // REGREF_VALIDATE_CDCDCDCD

	//	Make sure the ref IS already registered.
	Assertf(IsRefKnown(ppReference), "fwRefAwareBase::RemoveKnownRef: Ref is not already known to this ReferenceAwareBase! %p, %p", *ppReference, ppReference);

	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	fwKnownRefHolder** ppOldRef = &m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		if(pRefHolder->m_ppReference == ppReference)
		{
			//	Found it. Now remove it.

			//	Make the old ref holder point to the next item
			*ppOldRef = pRefHolder->m_pNext;

			//	Delete (return to the pool) the (now invalid) ref holder.
			delete pRefHolder;

			//	We're done, so return now.
			return;
		}

		//	Move to the next ref holder in the list.
		ppOldRef = &(pRefHolder->m_pNext);
		pRefHolder = pRefHolder->m_pNext;
	}
}


template<class _Base> NOINLINE void fwRefAwareBaseImpl<_Base>::ClearAllKnownRefs(void) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK
#if REGREF_VALIDATION
	ValidateAllKnownRefs();
#endif //	REGREF_VALIDATION

#if REGREF_VALIDATE_CDCDCDCD
	ValidateAllKnownRefsNotCDCDCD();
	fwRefAwareBaseImpl_OnClearAllRefs((void*)this);
#endif // REGREF_VALIDATE_CDCDCDCD

	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		Assertf(!*pRefHolder->m_ppReference || *pRefHolder->m_ppReference == this,"fwRefAwareBase ref doesn't point back at us (%p instead of %p)?  Will likely corrupt memory!",*pRefHolder->m_ppReference,this);
		*pRefHolder->m_ppReference = NULL;
		fwKnownRefHolder* pTempRefHolder = pRefHolder;
		pRefHolder = pRefHolder->m_pNext;

		//	Delete (return to the pool) the (now invalid) ref holder.
		delete pTempRefHolder;
	}

	//	Make sure the head is cleared as well since what it may have pointed
	//	to is gone.
	m_pKnownRefHolderHead = NULL;
}


template<class _Base> NOINLINE bool fwRefAwareBaseImpl<_Base>::IsRefKnown(void** ppReference) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		if(pRefHolder->m_ppReference == ppReference)
		{
			//	It is already in the list.
			return true;
		}

		pRefHolder =  pRefHolder->m_pNext;
	}

	//	It was not found in the list.
	return false;
}


template<class _Base> bool fwRefAwareBaseImpl<_Base>::IsReferenced(void) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

	return m_pKnownRefHolderHead != 0;
}

#if REGREF_VALIDATION
template<class _Base> NOINLINE bool fwRefAwareBaseImpl<_Base>::ValidateRef(const void* pReference, void** ppReference) const
{
	//	Make sure that reference is actually one we should track (as it
	//	actually points to this object).
	if(pReference != this)
	{
		Assertf(false, "fwRefAwareBase::ValidateRef: Ref does not point to this! %p != %p, (%p, %p)", pReference, *ppReference, ppReference, this);
		return false;
	}

//	TODO: Some simple tests.
	//void* base = dynamic_cast<void*>(pReference);
	//base = base;

	//fwRefAwareBase* refit = dynamic_cast<fwRefAwareBase*>(*ppReference);
	//refit = refit;
	

	//fwRefAwareBase* nonConstThis = const_cast<fwRefAwareBase*>(this);
	//void* dynamicCastedThis = dynamic_cast<void*>(nonConstThis);
	//const bool matchesDynamicCastsedThis = (((void*)(*ppReference)) == dynamicCastedThis);
	//void* reinterpretCastedThis = reinterpret_cast<void*>(nonConstThis);
	//const bool matchesReinterpretCastedThis = (((void*)(*ppReference)) == reinterpretCastedThis);
	//if(!matchesDynamicCastsedThis && !matchesReinterpretCastedThis)
	//{
	//	Assertf(false, "fwRefAwareBase::ValidateRef: Ref does not point to this!");
	//	return false;
	//}

//	TODO: Something to think about adding in a general way.
	//	Make sure this is a pointer to a valid entity that is
	//	actually present in the world.
	//AssertEntityPointerValid_NotInWorld(*ppReference);

	return true;
}


template<class _Base> NOINLINE bool fwRefAwareBaseImpl<_Base>::ValidateAllKnownRefs(void) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK
//	TODO: Make sure to check the linked list for loops.

	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		//	Check if the given ref is valid.
		if(!ValidateRef(pRefHolder->m_pReference, pRefHolder->m_ppReference))
		{
			//	We know there is at leas one bad ref.
			return false;
		}

		pRefHolder = pRefHolder->m_pNext;
	}

	//	None of the refs appear bad (or we aren't tracking any yet).
	return true;
}


template<class _Base> NOINLINE int fwRefAwareBaseImpl<_Base>::CountAllKnownRefs(void) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

	ValidateAllKnownRefs();

	int numTrackedRefs = 0;
	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		++numTrackedRefs;
		pRefHolder = pRefHolder->m_pNext;
	}

	return numTrackedRefs;
}


template<class _Base> NOINLINE bool fwRefAwareBaseImpl<_Base>::RemoveAllInvalidKnownRefs(void) const
{
	REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

	bool allRefsAreValid = true;
	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		if(!ValidateRef(pRefHolder->m_pReference, pRefHolder->m_ppReference))
		{
//	TODO: Make this actually do as it says.
			//	Delete (return to the pool) the invalid ref holder.

			allRefsAreValid = false;
		}

		pRefHolder = pRefHolder->m_pNext;
	}

	return allRefsAreValid;
}

#endif //	REGREF_VALIDATION

#if REGREF_VALIDATE_CDCDCDCD
template<class _Base> NOINLINE void fwRefAwareBaseImpl<_Base>::ValidateAllKnownRefsNotCDCDCD(void) const
{
	fwKnownRefHolder* pRefHolder = m_pKnownRefHolderHead;
	while(pRefHolder)
	{
		if((size_t)*pRefHolder->m_ppReference == 0xcdcdcdcdcdcdcdcd)
		{
			fwRefAwareBaseImpl_OnBadRef((void*)this, pRefHolder->m_ppReference);
		}
		Assertf((size_t)*pRefHolder->m_ppReference != 0xcdcdcdcdcdcdcdcd, "fwRefAwareBase::ValidateAllKnownRefsNotCDCDCD: Reference %p is %p!", this, pRefHolder->m_ppReference);
		pRefHolder = pRefHolder->m_pNext;
	}
}
#endif // REGREF_VALIDATE_CDCDCDCD

#undef NOINLINE

//
// PURPOSE
//	Dummy base class used to implement fwRefAwareBaseNonVirt.
//
class fwRefAwareBaseNonVirtBase {};

//
// PURPOSE
//	This works just like fwRefAwareBase, but isn't derived from datBase and doesn't
//	have any other virtual functions. This is useful if used for classes that don't
//	require polymorphism, saving memory by omitting the virtual table pointer.
// NOTES
//	To use, you can derive some class X from this, and use fwRegdRef<X, fwRefAwareBaseNonVirtBase>
//	for the reference.
//	
#if REGREF_VALIDATION
class fwRefAwareBaseNonVirt : public fwRefAwareBaseImpl<datBase>
#else
class fwRefAwareBaseNonVirt : public fwRefAwareBaseImpl<fwRefAwareBaseNonVirtBase>
#endif
{
};

//
// PURPOSE
//	This is the base class that your reference aware objects should derive from,
//	assuming that you want a datBase-derived object with virtual function support.
//	
class fwRefAwareBase : public fwRefAwareBaseImpl<datBase>
{
};

#if !REGREF_VALIDATION
CompileTimeAssertSize(fwRefAwareBaseNonVirt,4,8);//	Just enough room for the m_pKnownRefHolderHead.
#endif
CompileTimeAssertSize(fwRefAwareBase,8,16);//	Just enough room for the vtbl pointer and the m_pKnownRefHolderHead.



//
// PURPOSE
//	This is generally what any class member reference to a fwRefAwareBase
//	derived object should use instead of a simple raw pointer.
// PARAMS
//		T	-	The type of object to be pointed at.  It must derive from
//				fwRefAwareBase.
// NOTES
//	When used it will manage the interactions with the fwRefAwareBase object
//	to assure that it is properly nulled out if and when the fwRefAwareBase
//	object is ever removed.  It will also handle the interactions with the old
//	and new ref'd objects whenever the fwRegdRef<T> value is re-assigned.
//
//	Class T must based on an fwRefAwareBase for the Reference Tracking parts
//	of the code to compile.
//
//	It should be both easier to use and much more reliable the doing such
//	Reference Tracking by hand (with C style function calls).
//	All class member vars like "T* m_pT" should be able to transparently become
//	"fwRegdRef<T> m_pT" as long a T publicly inherrits from fwRefAwareBase.
//
//	In design the class is almost like template based smart pointers.
//	In some ways it is like an intrusive_ptr in that it knows certain details
//	about the implementation of the objects it points to (fwRefAwareBase
//	derived objects).
//	The implementation of this class is strongly based on Boost::shared_ptr.
//
//	In general:
//	--T must be derived from fwRefAwareBase.
//	--It's constructor may call AddKnownRef.
//	--Operator= checks if the pointer is different and if so does a
//	 RemoveKnownRef then a AddKnownRef.
//	--Operator* and Operator-> both return the pointer or assert if it no
//	 longer exists.
//	--The destructor calls RemoveKnownRef.
// SEE ALSO
//	fwRefAwareBase.
//	There are some good pages on smart pointer implimentations and use here:
//		http://www.boost.org/doc/libs/1_35_0/libs/smart_ptr/smart_ptr.htm
//	and
//		http://ootips.org/yonat/4dev/smart-pointers.html
//	For some convenient pre-made specific types of fwRegdRef please see
//	RegdRefTypes.h
//
template <class T, class B = fwRefAwareBase>
class fwRegdRef
{
public:
	typedef T*	InternalPointerType;

	//
	// PURPOSE
	//	Default constructor.
	//
	fwRegdRef()
		:
	m_p(0)
	{
	}

	//
	// PURPOSE
	//	Construct from a T*.
	//	Calls AddKnownRef if the passed in pointer isn't null.
	//  If you know for a fact that the pointer is not NULL, consider using
	//  the "T&" constructor instead.
	// PARAMS
	//	p	-	The pointer to copy and contain.
	//
	explicit fwRegdRef(T* p)
		:
		m_p(p)
	{
		if(m_p)
		{
			B* pRefAwareBase = (B*)(m_p);
			pRefAwareBase->AddKnownRef((void**)(&m_p));
		}
	}

	//
	// PURPOSE
	//	Construct from a T&.
	//	Calls AddKnownRef if the passed in pointer isn't null.
	// PARAMS
	//	p	-	The reference of the object to keep and contain. This may not be a reference to NULL.
	//
	explicit fwRegdRef(T& p)
	{
		m_p = &p;
		B* pRefAwareBase = (B*)(m_p);
		pRefAwareBase->AddKnownRef((void**)(&m_p));
	}

	//
	// PURPOSE
	//	Construct from another fwRegdRef.
	//	Calls AddKnownRef if the passed in pointer isn't null.
	// PARAMS
	//	rhs	-	The other fwRegdRef to copy.
	//
	explicit fwRegdRef(fwRegdRef<T, B> const & rhs)
		:
		m_p(rhs.m_p)
	{
		if(m_p)
		{
			B* pRefAwareBase = (B*)(m_p);
			pRefAwareBase->AddKnownRef((void**)(&m_p));
		}
	}

	//
	// PURPOSE
	//	Destructor.
	//	Calls RemoveKnownRef if the stored pointer isn't null.
	//
	~fwRegdRef()
	{
		if(m_p)
		{
			B* pRefAwareBase = (B*)(m_p);
			pRefAwareBase->RemoveKnownRef((void**)(&m_p));
		}
		m_p = NULL;
	}

	//
	// PURPOSE
	//	Setting using the equals sign with another fwRegdRef.
	//	Only does assignment if RHS is a really different object from this.
	//	Checks if the pointer should change and if so updates the registered
	//	references appropriately.
	// PARAMS
	//	rhs	-	The other fwRegdRef to copy.
	//
	fwRegdRef& operator=(fwRegdRef const & rhs)
	{
		if(this != &rhs)
		{
			Reset(rhs.m_p);
		}

		return *this;
	}

	//
	// PURPOSE
	//	Setting using the equals sign with a T*.
	//	Checks if the pointer should change and if so updates the registered
	//	references appropriately.
	// PARAMS
	//	rhs	-	The other fwRegdRef to copy.
	//
	fwRegdRef& operator=(T* rhs)
	{
		Reset(rhs);

		return *this;
	}


	//
	// PURPOSE
	//	Explicitly assign it to hold another T*.
	//	Should be mainly used internally to this class, but is safe to use
	//	elsewhere.
	//	Checks if the pointer should change and if so updates the registered
	//	references appropriately.
	// PARAMS
	//	rhs	-	The other fwRegdRef to copy.
	//
	void Reset(T* rhs)
	{
		//	Check if any update is actually needed.
		if(m_p != rhs)
		{
			//	Update the REGREFs and the m_p.
			if(m_p)
			{
				B* pRefAwareBase = (B*)(m_p);
				pRefAwareBase->RemoveKnownRef((void**)(&m_p));
			}
			m_p = rhs;
			if(m_p)
			{
				B* pRefAwareBase = (B*)(m_p);
				pRefAwareBase->AddKnownRef((void**)(&m_p));
			}
		}
	}

	//
	// PURPOSE
	//	Gets the raw pointer of the object.
	// RETURNS
	//		The raw stored pointer.
	// NOTES
	//	Use it carefully!  And make sure to check for null before doing calls
	//	with it.
	//
	T* Get(void) const
	{
		return m_p;
	}

	//
	// PURPOSE
	//	The * dereference operator.
	//	So we can say things like:
	//	T* myPtr = GetObject();
	//	fwRegdRef<T> foo(myPtr);
	//	(*foo).DoSomething();//	Equivalent to (*myPtr).DoSomething();
	// RETURNS
	//		The dereferenced object.
	//
	T& operator*() const
	{
		Assertf(m_p != 0, "A null RegdRef is trying to be dereferenced (with the * or -> operators)- the refd object has probably been deleted.  Please look at the calling code for the problem.  Don't dereference RegdRefs (or any pointer) without first making sure it is not null.");
		return *m_p;
	}

	//
	// PURPOSE
	//	The -> dereference operator.
	//	So we can say things like:
	//	T* myPtr = GetObject();
	//	fwRegdRef<T> foo(myPtr);
	//	foo->DoSomething();//	Equivalent to myPtr->DoSomething();
	// RETURNS
	//		The dereferenced object.
	//
	T* operator->() const
	{
		Assertf(m_p != 0, "A null RegdRef is trying to be dereferenced (with the * or -> operators)- the refd object has probably been deleted.  Please look at the calling code for the problem.  Don't dereference RegdRefs (or any pointer) without first making sure it is not null.");
		return m_p;
	}

	//
	// PURPOSE
	//	Implicit conversion to T*.
	//	This is so we can do things like:
	//	T* myPtr = GetObject();
	//	fwRegdRef<T> foo(myPtr);
	//	T* bar = foo; //	Equivalent to T* bar = myPtr;
	//	In this case T could be CEntity.
	//	It avoids any unnecessary casting.
	// RETURNS
	//		The raw stored pointer.
	//
	operator T*() const
	{
		return m_p;
	}

	//
	// PURPOSE
	//	Comparisons to other fwRegdRef.
	//	These all compare the values of the stored pointers.
	// PARAMS
	//	rhs	-	The other fwRegdRef to compare against.
	// RETURNS
	//		The value of the comparison using the raw stored pointers.
	// NOTES
	//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
	//	T* == fwRegdRef<T>).
	//	These all compare to the value of the stored pointer in the
	//	fwRegdRef<T>.
	//	The comparisons are defined as global functions below the class
	//	declaration.
	//	Boolean comparison to null (i.e. fwRegdRef<T> == null and
	//	null == fwRegdRef<T>).
	//	The comparisons are defined as global functions below the class
	//	declaration.
	//
	bool operator==(fwRegdRef const & rhs)
	{
		return m_p == rhs.m_p;
	}

	//
	// PURPOSE
	//	Comparisons to other fwRegdRef.
	//	These all compare the values of the stored pointers.
	// PARAMS
	//	rhs	-	The other fwRegdRef to compare against.
	// RETURNS
	//		The value of the comparison using the raw stored pointers.
	// NOTES
	//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
	//	T* == fwRegdRef<T>).
	//	These all compare to the value of the stored pointer in the
	//	fwRegdRef<T>.
	//	The comparisons are defined as global functions below the class
	//	declaration.
	//	Boolean comparison to null (i.e. fwRegdRef<T> == null and
	//	null == fwRegdRef<T>).
	//	The comparisons are defined as global functions below the class
	//	declaration.
	//
	bool operator!=(fwRegdRef const & rhs)
	{
		return m_p != rhs.m_p;
	}

	//
	// PURPOSE
	//	Comparisons to other fwRegdRef.
	//	These all compare the values of the stored pointers.
	// PARAMS
	//	rhs	-	The other fwRegdRef to compare against.
	// RETURNS
	//		The value of the comparison using the raw stored pointers.
	// NOTES
	//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
	//	T* == fwRegdRef<T>).
	//	These all compare to the value of the stored pointer in the
	//	fwRegdRef<T>.
	//	The comparisons are defined as global functions below the class
	//	declaration.
	//	Boolean comparison to null (i.e. fwRegdRef<T> == null and
	//	null == fwRegdRef<T>).
	//	The comparisons are defined as global functions below the class
	//	declaration.
	//
	bool operator<(fwRegdRef const & rhs)
	{
		return m_p < rhs.m_p;
	}

	//
	// PURPOSE
	//	Swap the registered pointers of the two fwRegdRef objects.
	//	For convenience and symmetry with the Boost libraries.
	// PARAMS
	//	rhs	-	The other fwRegdRef to swap internal values with.
	//
	void Swap(fwRegdRef& rhs)
	{
		T* tmp = m_p;
		Reset(rhs.m_p);
		rhs.Reset(tmp);
	}

private:

	T* m_p;
};


//
// PURPOSE
//	Casting operators.
//	For convenience and symmetry with the Boost libraries (though using our
//	naming conventions).
// PARAMS
//	p	-	The other fwRegdRef to cast from.
// RETURNS
//	The newly cast object.
//
template<class T, class U, class B>
T StaticCast(fwRegdRef<U, B> const & p)
{
	return static_cast<T>(p.Get());
}


//
// PURPOSE
//	Casting operators.
//	For convenience and symmetry with the Boost libraries (though using our
//	naming conventions).
// PARAMS
//	p	-	The other fwRegdRef to cast from.
// RETURNS
//	The newly cast object.
//
template<class T, class U, class B>
T ConstCast(fwRegdRef<U, B> const & p)
{
	return const_cast<T>(p.Get());
}


//
// PURPOSE
//	Casting operators.
//	For convenience and symmetry with the Boost libraries (though using our
//	naming conventions).
// PARAMS
//	p	-	The other fwRegdRef to cast from.
// RETURNS
//	The newly cast object.
//
template<class T, class U, class B>
T DynamicCast(fwRegdRef<U, B> const & p)
{
	return dynamic_cast<T>(p.Get());
}

//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator==(fwRegdRef<T, B> const & lhs, T* rhs)
{
	return lhs.Get() == rhs;
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator==(T* lhs, fwRegdRef<T, B> const & rhs)
{
	return lhs == rhs.Get();
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator!=(fwRegdRef<T, B> const & lhs, T* rhs)
{
	return lhs.Get() != rhs;
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator!=(T* lhs, fwRegdRef<T, B> const & rhs)
{
	return lhs != rhs.Get();
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator>(fwRegdRef<T, B> const & lhs, T* rhs)
{
	return lhs.Get() > rhs;
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator>(T* lhs, fwRegdRef<T, B> const & rhs)
{
	return lhs > rhs.Get();
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator<(fwRegdRef<T, B> const & lhs, T* rhs)
{
	return lhs.Get() < rhs;
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator<(T* lhs, fwRegdRef<T, B> const & rhs)
{
	return lhs < rhs.Get();
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator>=(fwRegdRef<T, B> const & lhs, T* rhs)
{
	return lhs.Get() >= rhs;
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator>=(T* lhs, fwRegdRef<T, B> const & rhs)
{
	return lhs >= rhs.Get();
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator<=(fwRegdRef<T, B> const & lhs, T* rhs)
{
	return lhs.Get() <= rhs;
}


//
// PURPOSE
//	Boolean comparison to T pointers (i.e. fwRegdRef<T> == T* and
//	T* == fwRegdRef<T>).
//	These all compare to the value of the stored pointer in the
//	fwRegdRef<T>.
// PARAMS
//	lhs	-	The left hand side fwRegdRef to compare with.
//	rhs	-	The right hand side fwRegdRef to compare with.
// RETURNS
//	The value of the comparison of the internal pointers.
//
template <class T, class B>
inline bool operator<=(T* lhs, fwRegdRef<T, B> const & rhs)
{
	return lhs <= rhs.Get();
}


//
// PURPOSE
//	Swap the registered pointers of the two fwRegdRef objects.
//	For convenience and symmetry with the Boost libraries.
// PARAMS
//	lhs	-	One of the two fwRegdRefs in the swap.
//	rhs	-	One of the two fwRegdRefs in the swap.
//
template <class T, class B>
inline void Swap(fwRegdRef<T, B>& lhs, fwRegdRef<T, B>& rhs)
{
	lhs.Swap(rhs);
}

//
// PURPOSE
//		This is a very useful macro to create direct comparison between two
//		derived types.
// PARAMS
//		regdDerived	-	The child class type.
//		regdBase	-	The parent class type.
// NOTES 
//		It is quite useful for entity based RegdPtrs.  It makes comparisons
//		between say a fwRegdRef<CEntity> foo and fwRegdRef<CVehicle> bar
//		possible (i.e. is foo == bar).
//
#define MAKE_REGDPTR_TYPE_DIRECT_COMPARES(regdDerived, regdBase)			\
	inline bool operator==(regdDerived const & lhs, regdBase const & rhs)	\
	{return (regdBase::InternalPointerType)lhs.Get() == rhs.Get();}			\
	inline bool operator==(regdBase const & lhs, regdDerived const & rhs)	\
	{return lhs.Get() == (regdBase::InternalPointerType)rhs.Get();}			\
	inline bool operator!=(regdDerived const & lhs, regdBase const & rhs)	\
	{return (regdBase::InternalPointerType)lhs.Get() != rhs.Get();}			\
	inline bool operator!=(regdBase const & lhs, regdDerived const & rhs)	\
	{return lhs.Get() != (regdBase::InternalPointerType)rhs.Get();}			\
	inline bool operator<(regdDerived const & lhs, regdBase const & rhs)	\
	{return (regdBase::InternalPointerType)lhs.Get() < rhs.Get();}			\
	inline bool operator<(regdBase const & lhs, regdDerived const & rhs)	\
	{return lhs.Get() < (regdBase::InternalPointerType)rhs.Get();}

#undef REGDREF_VERIFY_IS_MAIN_THREAD_OR_LOCK

} //	namespace rage

#endif //	FWTL_REGDREFS_H_
