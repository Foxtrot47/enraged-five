//
// fwtl/poolallocator.h
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
// Dustin Russell
//
#ifndef FWTL_POOLCRITICALSECTION_H_
#define FWTL_POOLCRITICALSECTION_H_

#include "system/criticalsection.h"
#include "system/ipc.h"
#include "system/threadtype.h"

namespace rage {

	// A templated locking mechanism for use with REGISTER_LOCKING_CLASS_POOL
	template<typename _T>
	class fwPoolCriticalSection
	{
	public:
		fwPoolCriticalSection()
		{
			if (!sm_UseLock)
			{
				Assertf(sysThreadType::IsUpdateThread(), "%s can only be used from main thread - assign bug to whoever owns %s", typeid(_T).name(), g_CurrentThreadName);
			}
			else
			{
				sm_Lock.Lock();
			}
		}

		~fwPoolCriticalSection()
		{
			if (sm_UseLock)
			{
				sm_Lock.Unlock();
			}
		}

		static void SetUseLock(const bool useLock) { sm_UseLock = useLock; }
		static bool GetUseLock() { return sm_UseLock; }

	private:
		static sysCriticalSectionToken sm_Lock;
		static bool sm_UseLock;
	};

	// http://stackoverflow.com/a/3229904
	template<typename _T>
	sysCriticalSectionToken fwPoolCriticalSection<_T>::sm_Lock;
	template<typename _T>
	bool fwPoolCriticalSection<_T>::sm_UseLock = false;

} // namespace rage

#endif // FWTL_POOLCRITICALSECTION_H_ 