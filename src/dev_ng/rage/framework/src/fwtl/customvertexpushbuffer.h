//
// fwtl/customvertexpushbuffer.h
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved. 
//

// handy wrapper for grcVertexBuffer which allows writing vertices directly to command buffers
// works now on __XENON and __PPU and have a fall back default implementation


#ifndef __CUSTOM_VERTEX_PUSH_BUFFER_H__
#define __CUSTOM_VERTEX_PUSH_BUFFER_H__

#if __SPU
// use #include instead of #error, as then gcc gives the include stack
#include "error, customvertexpushbuffer.h not supported in spu code"
#endif

#include "vector/vector4.h"
#include "grcore/fvf.h"
#include "grcore/vertexdecl.h"
#include "grcore/vertexbuffer.h"
#include "grmodel/modelfactory.h"
#if __PPU
	#include "grcore/wrapper_gcm.h"
#endif
#if __D3D9
	#include "system\xtl.h"
	#include "system/d3d9.h"
#endif


// namespaces 
namespace rage {

class Vector3;
class Vector4;


#if __XENON
	// _ReadWriteBarrier intrinsic -- seems to have no effect on local variables.
	extern "C" void _ReadWriteBarrier();
	#pragma intrinsic(_ReadWriteBarrier)
#endif

#define VTXBUFFER_USES_NOOP							(0)
#define VTXBUFFER_USES_DEFAULTVERTEXBUFFER			(RSG_PC)

struct traitParam
{
	int m_VtxElement;
#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
	int m_vtxIdx;
#endif // VTXBUFFER_USES_DEFAULTVERTEXBUFFER

	void *m_lockPointer;
};
typedef struct traitParam traitParam;



// Vertex: Position, Normal, Texture, Texture1, Texture2, Texture3, Texture4
struct VtxPB_PoNoTe5_trait 
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	// POSITION
	float		_normal[3];		// grcFvf::grcdsFloat3;	// NORMAL
	float		_uv0[4];		// grcFvf::grcdsFloat3;	// TEXCOORD0
	float		_uv1[4];		// grcFvf::grcdsFloat3;	// TEXCOORD1
	float		_uv2[4];		// grcFvf::grcdsFloat4;	// TEXCOORD2
	float		_uv3[4];		// grcFvf::grcdsFloat4;	// TEXCOORD3
	float		_uv4[4];		// grcFvf::grcdsFloat4;	// TEXCOORD4

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			// POSITION
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat3);		// NORMAL
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat4);	// TEXCOORD0
		fvf.SetTextureChannel(1, true, grcFvf::grcdsFloat4);	// TEXCOORD1
		fvf.SetTextureChannel(2, true, grcFvf::grcdsFloat4);	// TEXCOORD2
		fvf.SetTextureChannel(3, true, grcFvf::grcdsFloat4);	// TEXCOORD3
		fvf.SetTextureChannel(4, true, grcFvf::grcdsFloat4);	// TEXCOORD4
	}

	static inline int GetComponentCount() { return 7; }

	static inline void SetPosition(traitParam& param, const Vector3& vp)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=vp.x;
		*(ptr++)=vp.y; 
		*(ptr++)=vp.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& param, const Vector3& dp)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP	
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=dp.x;
		*(ptr++)=dp.y;
		*(ptr++)=dp.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetUV(traitParam& param, const Vector4& fv)
	{
		Assert(param.m_VtxElement == 2);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=fv.x;
		*(ptr++)=fv.y;
		*(ptr++)=fv.z;
		*(ptr++)=fv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 3);
	}

	static inline void SetUV1(traitParam& param, const Vector4& sv)
	{
		Assert(param.m_VtxElement == 3);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=sv.x;
		*(ptr++)=sv.y;
		*(ptr++)=sv.z;
		*(ptr++)=sv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 4);
	}

	static inline void SetUV2(traitParam& param, const Vector4& m)
	{
		Assert(param.m_VtxElement == 4);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=m.x;
		*(ptr++)=m.y;
		*(ptr++)=m.z;
		*(ptr++)=m.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 5);
	}

	static inline void SetUV3(traitParam& param, const Vector4& m)
	{
		Assert(param.m_VtxElement == 5);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=m.x;
		*(ptr++)=m.y;
		*(ptr++)=m.z;
		*(ptr++)=m.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 6);
	}

	static inline void SetUV4(traitParam& param, const Vector4& m)
	{
		Assert(param.m_VtxElement == 6);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=m.x;
		*(ptr++)=m.y;
		*(ptr++)=m.z;
		*(ptr++)=m.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 7);
	}

	static inline void SetCPV(traitParam& UNUSED_PARAM(param), const Color32 UNUSED_PARAM(c))
	{
		// No op
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector2& UNUSED_PARAM(t))
	{
		// No op
	}

	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// No op
	}
};

// Vertex: Position, Texture, Texture1
struct VtxPB_PoTe4_trait 
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	
	float		_uv0[4];		// grcFvf::grcdsFloat4;	
	float		_uv1[4];		// grcFvf::grcdsFloat4;	
	float		_uv2[4];		// grcFvf::grcdsFloat4;	
	float		_uv3[4];		// grcFvf::grcdsFloat4;	

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat4);	
		fvf.SetTextureChannel(1, true, grcFvf::grcdsFloat4);	
		fvf.SetTextureChannel(2, true, grcFvf::grcdsFloat4);	
		fvf.SetTextureChannel(3, true, grcFvf::grcdsFloat4);	
	}

	static inline int GetComponentCount() { return 5; }

	static inline void SetPosition(traitParam& param, const Vector3& vp)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=vp.x;
		*(ptr++)=vp.y; 
		*(ptr++)=vp.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& UNUSED_PARAM(param), const Vector3& UNUSED_PARAM(dp))
	{
		// No op
	}

	static inline void SetUV(traitParam& param, const Vector4& fv)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=fv.x;
		*(ptr++)=fv.y;
		*(ptr++)=fv.z;
		*(ptr++)=fv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetUV1(traitParam& param, const Vector4& sv)
	{
		Assert(param.m_VtxElement == 2);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=sv.x;
		*(ptr++)=sv.y;
		*(ptr++)=sv.z;
		*(ptr++)=sv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 3);
	}

	static inline void SetUV2(traitParam& param, const Vector4& sv)
	{
		Assert(param.m_VtxElement == 3);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=sv.x;
		*(ptr++)=sv.y;
		*(ptr++)=sv.z;
		*(ptr++)=sv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 4);
	}

	static inline void SetUV3(traitParam& param, const Vector4& sv)
	{
		Assert(param.m_VtxElement == 4);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=sv.x;
		*(ptr++)=sv.y;
		*(ptr++)=sv.z;
		*(ptr++)=sv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 5);
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(m))
	{
		// No op
	}

	static inline void SetCPV(traitParam& UNUSED_PARAM(param), const Color32 UNUSED_PARAM(c))
	{
		// No op
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector2& UNUSED_PARAM(t))
	{
		// No op
	}

	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// No op
	}
};



// Vertex: Position, Normal, TextureHalf, TextureHalf1, TextureHalf2, TextureHalf3
struct VtxPB_PoNoTe4Half_trait 
{
	Vec4V		_pos;			// grcFvf::grcdsFloat4;	// POSITION
	Vec4V		_normal;		// grcFvf::grcdsFloat4;	// NORMAL
	Vec4V		_uv01;			// grcFvf::grcdsHalf4;	// TEXCOORD0 TEXCOORD1
	Vec4V		_uv23;			// grcFvf::grcdsHalf4;	// TEXCOORD2 TEXCOORD3

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat4);		// POSITION
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat4);	// NORMAL
		fvf.SetTextureChannel(0, true, grcFvf::grcdsHalf4);	// TEXCOORD0
		fvf.SetTextureChannel(1, true, grcFvf::grcdsHalf4);	// TEXCOORD1
		fvf.SetTextureChannel(2, true, grcFvf::grcdsHalf4);	// TEXCOORD2
		fvf.SetTextureChannel(3, true, grcFvf::grcdsHalf4);	// TEXCOORD3
	}

	static inline int GetComponentCount() { return 6; }

	static inline void SetPosition(traitParam& param, const Vector3& vp)
	{
		Assert(param.m_VtxElement == 0);

#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=vp.x;
		*(ptr++)=vp.y; 
		*(ptr++)=vp.z;
		*(ptr++)=vp.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& param, const Vector3& n)
	{
		Assert(param.m_VtxElement == 1);

#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=n.x;
		*(ptr++)=n.y; 
		*(ptr++)=n.z;
		*(ptr++)=n.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetUV(traitParam& param, const Vector4& uv)
	{
		Assert(param.m_VtxElement == 2);

#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y; 
		*(ptr++)=uv.z;
		*(ptr++)=uv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 4);
	}

	static inline void SetUV1(traitParam& param, const Vector4& uv)
	{
		Assert(param.m_VtxElement == 4);

#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y; 
		*(ptr++)=uv.z;
		*(ptr++)=uv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 6);
	}

	static inline void SetUV2(traitParam& UNUSED_PARAM(param), const Vector3& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV3(traitParam& UNUSED_PARAM(param), const Vector3& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetCPV(traitParam& UNUSED_PARAM(param), const Color32 UNUSED_PARAM(cpv))
	{
		// No op
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector2& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// No op
	}
};

// Vertex: Position, Diffuse, Texture, Normal, Tangent
struct VtxPB_PoDiTeNoTa_trait 
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	// POSITION
	float		_normal[3];		// grcFvf::grcdsFloat3;	// NORMAL
	Color32		_cpv;			// grcFvf::grcdsColor;	// COLOR0
	float		_uv0[2];		// grcFvf::grcdsFloat2;	// TEXCOORD0
	float		_tangent0[4];	// grcFvf::grcdsFloat4;	// TANGENT0

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			// POSITION
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);		// COLOR0
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);	// TEXCOORD0
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat3);		// NORMAL
		fvf.SetTangentChannel(0, true, grcFvf::grcdsFloat4);	// TANGENT0
	}

	static inline int GetComponentCount() { return 5; }

	static inline void SetPosition(traitParam& param, const Vector3& p)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=p.x;
		*(ptr++)=p.y; 
		*(ptr++)=p.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& param, const Vector3& n)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP	
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=n.x;
		*(ptr++)=n.y;
		*(ptr++)=n.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetCPV(traitParam& param, const Color32 c)
	{
		Assert(param.m_VtxElement == 2);
#if 0 == VTXBUFFER_USES_NOOP	
		u32 *ptr=(u32*)param.m_lockPointer;
		*ptr = c.GetDeviceColor();
		ptr++;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 3);
	}

	static inline void SetUV(traitParam& param, const Vector2& uv)
	{
		Assert(param.m_VtxElement == 3);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 4);
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV1(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV2(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV3(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetTangent(traitParam& param, const Vector4& t)
	{
		Assert(param.m_VtxElement == 4);
#if 0 == VTXBUFFER_USES_NOOP	
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=t.x;
		*(ptr++)=t.y;
		*(ptr++)=t.z;
		*(ptr++)=t.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 5);
	}

};

// Vertex: Position, Diffuse, Texture4, Normal
struct VtxPB_PoDiTe6No_trait
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	// POSITION
	Color32		_cpv;			// grcFvf::grcdsColor;	// COLOR0
	float		_uv0[2];		// grcFvf::grcdsFloat2;	// TEXCOORD0
	float		_uv1[2];		// grcFvf::grcdsFloat2;	// TEXCOORD1
	float		_uv2[2];		// grcFvf::grcdsFloat2;	// TEXCOORD1
	float		_normal[3];		// grcFvf::grcdsFloat3;	// NORMAL

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			// POSITION
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);		// COLOR0
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);	// TEXCOORD0
		fvf.SetTextureChannel(1, true, grcFvf::grcdsFloat2);	// TEXCOORD1
		fvf.SetTextureChannel(2, true, grcFvf::grcdsFloat2);	// TEXCOORD2
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat3);		// NORMAL
	}

	static inline int GetComponentCount() { return 5; }

	static inline void SetPosition(traitParam& param, const Vector3& p)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=p.x;
		*(ptr++)=p.y; 
		*(ptr++)=p.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& param, const Vector3& n)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP	
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=n.x;
		*(ptr++)=n.y;
		*(ptr++)=n.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetCPV(traitParam& param, const Color32 c)
	{
		Assert(param.m_VtxElement == 2);
#if 0 == VTXBUFFER_USES_NOOP	
		u32 *ptr=(u32*)param.m_lockPointer;
		*ptr = c.GetDeviceColor();
		ptr++;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 3);
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector2& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV(traitParam& param, const Vector4& uv)
	{
		Assert(param.m_VtxElement == 3);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y;
		*(ptr++)=uv.z;
		*(ptr++)=uv.w;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 4);
	}

	static inline void SetUV1(traitParam& param, const Vector2& uv)
	{
		// No op
		Assert(param.m_VtxElement == 4);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 5);
	}

	static inline void SetUV1(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV2(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV3(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// do nothing
	}
};

// Vertex: Position, Diffuse, Texture, Normal
struct VtxPB_PoDiTeNo_trait 
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	// POSITION
	float		_normal[3];		// grcFvf::grcdsFloat3;	// NORMAL
	Color32		_cpv;			// grcFvf::grcdsColor;	// COLOR0
	float		_uv0[2];		// grcFvf::grcdsFloat2;	// TEXCOORD0

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			// POSITION
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);		// COLOR0
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);	// TEXCOORD0
		fvf.SetNormalChannel(true, grcFvf::grcdsFloat3);		// NORMAL
	}

	static inline int GetComponentCount() { return 4; }

	static inline void SetPosition(traitParam& param, const Vector3& p)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=p.x;
		*(ptr++)=p.y; 
		*(ptr++)=p.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& param, const Vector3& n)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP	
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=n.x;
		*(ptr++)=n.y;
		*(ptr++)=n.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetCPV(traitParam& param, const Color32 c)
	{
		Assert(param.m_VtxElement == 2);
#if 0 == VTXBUFFER_USES_NOOP	
		u32 *ptr=(u32*)param.m_lockPointer;
		*ptr = c.GetDeviceColor();
		ptr++;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 3);
	}

	static inline void SetUV(traitParam& param, const Vector2& uv)
	{
		Assert(param.m_VtxElement == 3);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 4);
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV1(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV2(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV3(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// do nothing
	}
};


// Vertex: Position, Diffuse, Texture
struct VtxPB_PoDiTe_trait 
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	// POSITION
	Color32		_cpv;			// grcFvf::grcdsColor;	// COLOR0
	float		_uv0[2];		// grcFvf::grcdsFloat2;	// TEXCOORD0

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			// POSITION
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);		// COLOR0
		fvf.SetTextureChannel(0, true, grcFvf::grcdsFloat2);	// TEXCOORD0
	}

	static inline int GetComponentCount() { return 3; }

	static inline void SetPosition(traitParam& param, const Vector3& p)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=p.x;
		*(ptr++)=p.y; 
		*(ptr++)=p.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& UNUSED_PARAM(param), const Vector3& UNUSED_PARAM(n))
	{
		// No op
	}

	static inline void SetCPV(traitParam& param, const Color32 c)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP	
		u32 *ptr=(u32*)param.m_lockPointer;
		*ptr = c.GetDeviceColor();
		ptr++;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetUV(traitParam& param, const Vector2& uv)
	{
		Assert(param.m_VtxElement == 2);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=uv.x;
		*(ptr++)=uv.y;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 3);
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV1(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV2(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV3(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}
	
	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// No op
	}

};

// Vertex: Position, Diffuse
struct VtxPB_PoDi_trait 
{
	float		_pos[3];		// grcFvf::grcdsFloat3;	// POSITION
	Color32		_cpv;			// grcFvf::grcdsColor;	// COLOR0

	static inline void fvfSetup(grcFvf &fvf)
	{
		fvf.SetPosChannel(true, grcFvf::grcdsFloat3);			// POSITION
		fvf.SetDiffuseChannel(true, grcFvf::grcdsColor);		// COLOR0
	}

	static inline int GetComponentCount() { return 2; }

	static inline void SetPosition(traitParam& param, const Vector3& p)
	{
		Assert(param.m_VtxElement == 0);
#if 0 == VTXBUFFER_USES_NOOP
		float *ptr=(float*)param.m_lockPointer;
		*(ptr++)=p.x;
		*(ptr++)=p.y; 
		*(ptr++)=p.z;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP

		ASSERT_ONLY(param.m_VtxElement = 1);
	}

	static inline void SetNormal(traitParam& UNUSED_PARAM(param), const Vector3& UNUSED_PARAM(n))
	{
		// No op
	}

	static inline void SetCPV(traitParam& param, const Color32 c)
	{
		Assert(param.m_VtxElement == 1);
#if 0 == VTXBUFFER_USES_NOOP	
		u32 *ptr=(u32*)param.m_lockPointer;
		*ptr = c.GetDeviceColor();
		ptr++;
		param.m_lockPointer = ptr;
#endif // 0 == VTXBUFFER_USES_NOOP	
		ASSERT_ONLY(param.m_VtxElement = 2);
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector2& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV1(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV2(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV3(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetUV4(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(uv))
	{
		// No op
	}

	static inline void SetTangent(traitParam& UNUSED_PARAM(param), const Vector4& UNUSED_PARAM(t))
	{
		// No op
	}

};


//
//
//
//
template<class T> class VtxPushBuffer
{
public:

	inline void Init(grcDrawMode drawMode, u32 VertexCount);
	inline void Begin();
	inline void *Lock(u32 vtxCount);
	inline void *LockWarmCache(u32 vtxCount);
	inline void BeginVertex();
	inline void SetPosition(const Vector3& p);
	inline void SetNormal(const Vector3& n);
	inline void SetNormal(const Vector4& n);
	inline void SetCPV(const Color32 c);
	inline void SetUV(const Vector2& uv);
	inline void SetUV(const Vector4& uv);
	inline void SetUV1(const Vector2& uv);
	inline void SetUV1(const Vector4& uv);
	inline void SetUV2(const Vector2& uv);
	inline void SetUV2(const Vector4& uv);
	inline void SetUV3(const Vector4& uv);
	inline void SetUV4(const Vector4& uv);
	inline void SetTangent(const Vector4& t);
	inline void SetFullVertex(void* pSrc);
	inline void EndVertex();
	inline void UnLock();
	inline void End();

	inline T* NewVertex();

	inline void Shutdown();
	
	inline u32 GetVtxBatchSize() const
	{ 
		return m_vtxBatchSize; 
	}

#if RSG_PS3
	inline u32 GetVtxWordSize() const
	{ 
		return m_vtxWordSize; 
	}
#endif // RSG_PS3

	inline int GetVtxCount() const
	{
		return m_vtxCount;
	}
	
	inline void* GetLockPointer() const
	{
		return param[NUMBER_OF_RENDER_THREADS].m_lockPointer;
	}

	inline void SetDrawModeType(grcDrawMode drawModeType)
	{
		m_drawModeType = drawModeType;
	}
	
public:	
	grcVertexDeclaration*		m_vtxDcl;

#if RSG_PS3
	u32							m_vtxWordSize;
#endif // RSG_PS3

	u32							m_vtxBatchSize;
	int							m_vtxCount[NUMBER_OF_RENDER_THREADS];

	u32							m_vtxOffset[NUMBER_OF_RENDER_THREADS];
#if __DEV && !RSG_PS3
	u32							m_frameSinceLastFlip[NUMBER_OF_RENDER_THREADS];
#endif // __DEV && !RSG_PS3

	traitParam					param[NUMBER_OF_RENDER_THREADS];
#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
	grcVertexBuffer				*m_vertexBuffer;
#endif // VTXBUFFER_USES_DEFAULTVERTEXBUFFER

	grcDrawMode					m_drawModeType;	
#if __ASSERT
	struct assertFlags
	{
		u8						m_bIsRunning		 :1;
		u8						m_bIsLocked			 :1;
	};
	assertFlags					m_assertFlags[NUMBER_OF_RENDER_THREADS];
#endif // __ASSERT
};

//
//
//
//
template<class T> inline void VtxPushBuffer<T>::Init(grcDrawMode drawMode, u32 WIN32PC_ONLY(VertexCount))
{
	// custom vertexbuffer stuff:
	grcFvf fvf;
	T::fvfSetup(fvf);

	m_vtxDcl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);
	Assert(m_vtxDcl);

	m_drawModeType = drawMode;

#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
	m_vtxBatchSize					= __WIN32PC ? VertexCount : (RSG_ORBIS ? 8192 : 1024); 
#if __D3D9
	const bool bReadWrite = true;
	const bool bDynamic = __WIN32PC ? true : false;
	m_vertexBuffer					= grcVertexBuffer::Create(m_vtxBatchSize, fvf, bReadWrite, bDynamic, NULL);
#elif RSG_DURANGO
	m_vertexBuffer					= grcVertexBuffer::Create(m_vtxBatchSize, fvf, true, true, NULL);
#else
	m_vertexBuffer					= grcVertexBuffer::Create(m_vtxBatchSize, fvf, rage::grcsBufferCreate_DynamicWrite, rage::grcsBufferSync_None, NULL);
#endif
	
	Assert(m_vertexBuffer);
#else
	#if RSG_XENON
		const u32 maxVertexCountPerBatch		= 1024;
	#elif RSG_PS3
		const u32 maxVertexCountPerBatch		= (8188 / sizeof(T));
		m_vtxWordSize							= sizeof(T) >> 2;
		Assert((sizeof(T) & 0x0000000003) == 0);	// stride must be divisible by 4
	#elif RSG_DURANGO || RSG_ORBIS
		const u32 maxVertexCountPerBatch        = grcDevice::BEGIN_VERTICES_MAX_SIZE / sizeof(T);
	#endif

	const int translateToPrim[][2] = {
		{ 1, 0 },//drawPoints,			// Draw one or more single-pixel points
		{ 2, 0 },//drawLines,			// Draw one or more disconnected line segments
		{ 1, -1 },//drawLineStrip,
		{ 3, 0 },//drawTris,
		{ 1, -2 },//drawTriStrip,
		{ 1, -2 },//drawTriFan
		{ 4, 0 },//drawQuads,
		{ 4, 0 },//drawRects,	
		{ 6, 0 },//drawTrisAdj,
	};
	CompileTimeAssert(NELEM(translateToPrim) == drawModesTotal);

	const u32 maxPrimitiveCountPerBatch		= (( maxVertexCountPerBatch + translateToPrim[m_drawModeType][1] ) /translateToPrim[m_drawModeType][0]) - 1;
	const u32 actualVertexCountPerBatch		= maxPrimitiveCountPerBatch * translateToPrim[m_drawModeType][0];
	m_vtxBatchSize							= actualVertexCountPerBatch;	// used only for asserts in Lock()
#endif

	int i;

	for(i=0; i<NUMBER_OF_RENDER_THREADS; i++)
	{
		ASSERT_ONLY(m_assertFlags[i].m_bIsRunning = false);
		ASSERT_ONLY(m_assertFlags[i].m_bIsLocked = false);
		param[i].m_lockPointer = NULL;
		m_vtxOffset[i] = 0;
#if __DEV && !RSG_PS3
		m_frameSinceLastFlip[i] = 0;
#endif // __DEV && !RSG_PS3
	}
}


template<class T> inline T* VtxPushBuffer<T>::NewVertex()
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T *ret = (T*)param[g_RenderThreadIndex].m_lockPointer;
	param[g_RenderThreadIndex].m_lockPointer = ret+1;
#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
	param[g_RenderThreadIndex].m_vtxIdx++;
#endif // VTXBUFFER_USES_DEFAULTVERTEXBUFFER	
	m_vtxCount[g_RenderThreadIndex]--;
	return ret;
}


template<class T> inline void VtxPushBuffer<T>::Shutdown()
{
	grmModelFactory::FreeDeclarator(m_vtxDcl);
	m_vtxDcl = NULL;
#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
	delete m_vertexBuffer;
	m_vertexBuffer = NULL;
#endif // VTXBUFFER_USES_DEFAULTVERTEXBUFFER
}

template<class T> inline void VtxPushBuffer<T>::Begin()
{
	Assert(false == m_assertFlags[g_RenderThreadIndex].m_bIsRunning);
	Assert(m_vtxDcl);
#if 0 == VTXBUFFER_USES_NOOP
	GRCDEVICE.SetVertexDeclaration(m_vtxDcl);
	#if RSG_PS3
		int modeMap[] =
		{
			CELL_GCM_PRIMITIVE_POINTS,
			CELL_GCM_PRIMITIVE_LINES,
			CELL_GCM_PRIMITIVE_LINE_STRIP,
			CELL_GCM_PRIMITIVE_TRIANGLES,
			CELL_GCM_PRIMITIVE_TRIANGLE_STRIP,
			CELL_GCM_PRIMITIVE_TRIANGLE_FAN,
			CELL_GCM_PRIMITIVE_QUADS,
		};
		GCM_DEBUG(cellGcmSetDrawBegin(GCM_CONTEXT,modeMap[m_drawModeType]));
	#endif // RSG_PS3
#endif // 0 == VTXBUFFER_USES_NOOP
	
	param[g_RenderThreadIndex].m_lockPointer = NULL;
	ASSERT_ONLY(m_assertFlags[g_RenderThreadIndex].m_bIsRunning = true);
	m_vtxCount[g_RenderThreadIndex] = 0;
}


template<class T> inline void *VtxPushBuffer<T>::Lock(u32 vtxCount)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsRunning);
	Assert(false == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	Assert(vtxCount <= m_vtxBatchSize);
	ASSERT_ONLY(m_assertFlags[g_RenderThreadIndex].m_bIsLocked = true);
	ASSERT_ONLY(param[g_RenderThreadIndex].m_VtxElement = -1);

#if 0 == VTXBUFFER_USES_NOOP
	#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
		grcLockType eLockType = grcsNoOverwrite;

		if (m_vtxOffset[g_RenderThreadIndex] + vtxCount > m_vtxBatchSize)
		{
			m_vtxOffset[g_RenderThreadIndex] = 0;
			eLockType = grcsNoOverwrite; // grcsDiscard

#if __DEV && !RSG_PS3
			static u32 uMinDifference = 3;
			if (m_frameSinceLastFlip[g_RenderThreadIndex] + uMinDifference > GRCDEVICE.GetFrameCounter())
			{
				Errorf("VtxPushBuffer %d too small Delta between last flip is too small Last %d Current %d Diff %d", m_vtxBatchSize, m_frameSinceLastFlip[g_RenderThreadIndex], GRCDEVICE.GetFrameCounter(), GRCDEVICE.GetFrameCounter() - m_frameSinceLastFlip[g_RenderThreadIndex]);
			}
			m_frameSinceLastFlip[g_RenderThreadIndex] = GRCDEVICE.GetFrameCounter();
#endif // __DEV && !RSG_PS3
		}
		m_vertexBuffer->Lock(eLockType, m_vertexBuffer->GetVertexStride() * m_vtxOffset[g_RenderThreadIndex], m_vertexBuffer->GetVertexStride() * vtxCount);
		m_vtxOffset[g_RenderThreadIndex] += vtxCount;
		param[g_RenderThreadIndex].m_vtxIdx = 0;
		param[g_RenderThreadIndex].m_lockPointer = m_vertexBuffer->GetLockPtr();
	#elif RSG_PS3
		GCM_DEBUG( cellGcmSetDrawInlineArrayPointer(GCM_CONTEXT,vtxCount * m_vtxWordSize, &param[g_RenderThreadIndex].m_lockPointer));
	#else
		param[g_RenderThreadIndex].m_lockPointer = GRCDEVICE.BeginVertices(m_drawModeType, vtxCount, sizeof(T));
	#endif
	Assert(param[g_RenderThreadIndex].m_lockPointer);
#else // 0 == VTXBUFFER_USES_NOOP	
	param[g_RenderThreadIndex].m_lockPointer = (void *)0xD34db4b3;
#endif // 0 == VTXBUFFER_USES_NOOP

	m_vtxCount[g_RenderThreadIndex] = vtxCount;
	return param[g_RenderThreadIndex].m_lockPointer;
}

template<class T> inline void *VtxPushBuffer<T>::LockWarmCache(u32 vtxCount)
{
#if RSG_PS3 && 0 == VTXBUFFER_USES_NOOP
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsRunning);
	Assert(false == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	Assert(vtxCount <= m_vtxBatchSize);
	ASSERT_ONLY(m_assertFlags[g_RenderThreadIndex].m_bIsLocked = true);
	ASSERT_ONLY(param.m_VtxElement = -1);

	u32 size = vtxCount * m_vtxWordSize;
	GCM_DEBUG(cellGcmSetDrawInlineArrayPointerInline(GCM_CONTEXT, size, &param[g_RenderThreadIndex].m_lockPointer));
	char *begin = (char*)(param[g_RenderThreadIndex].m_lockPointer);
	char *end = begin + size;
	begin = (char*)(((uptr)begin + 127) & ~127);
	// Note that we don't round end down here, as the spu fifo segments we
	// are writing to are always aligned to 128-bytes and 128-byte multiples
	// in size, so we it is always safe to trash up to the end of a cache
	// line boundary.
	while (begin < end)
	{
		__dcbz(begin);
		begin += 128;
	}

	m_vtxCount[g_RenderThreadIndex] = vtxCount;
	return param[g_RenderThreadIndex].m_lockPointer;

#else
	return Lock(vtxCount);
#endif
}

template<class T> inline void VtxPushBuffer<T>::BeginVertex()
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	Assert(param[g_RenderThreadIndex].m_VtxElement == -1);
	ASSERT_ONLY(param[g_RenderThreadIndex].m_VtxElement = 0);
}

template<class T> void VtxPushBuffer<T>::SetPosition(const Vector3& p)
{	
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetPosition(param[g_RenderThreadIndex],p);
}

template<class T> void VtxPushBuffer<T>::SetNormal(const Vector3& n)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetNormal(param[g_RenderThreadIndex],n);
}

template<class T> void VtxPushBuffer<T>::SetNormal(const Vector4& n)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetNormal(param[g_RenderThreadIndex],n);
}

template<class T> void VtxPushBuffer<T>::SetCPV(const Color32 c)
{ 
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetCPV(param[g_RenderThreadIndex],c);
}

template<class T> void VtxPushBuffer<T>::SetUV(const Vector2& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV(const Vector4& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV1(const Vector2& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV1(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV1(const Vector4& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV1(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV2(const Vector2& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV2(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV2(const Vector4& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV2(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV3(const Vector4& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV3(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetUV4(const Vector4& uv)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetUV4(param[g_RenderThreadIndex],uv);
}

template<class T> void VtxPushBuffer<T>::SetTangent(const Vector4& t)
{ 
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	T::SetTangent(param[g_RenderThreadIndex],t);
}

template<class T> void VtxPushBuffer<T>::SetFullVertex(void* pSrc)
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);

	u32* pDst = (u32*)param[g_RenderThreadIndex].m_lockPointer;
	memcpy( pDst, pSrc, sizeof(T));	
	pDst += (sizeof(T)>>2);
	param[g_RenderThreadIndex].m_lockPointer = pDst;
	ASSERT_ONLY(param[g_RenderThreadIndex].m_VtxElement = T::GetComponentCount());
}

template<class T> inline void VtxPushBuffer<T>::EndVertex()
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	Assert(param[g_RenderThreadIndex].m_VtxElement == T::GetComponentCount());
	ASSERT_ONLY(param[g_RenderThreadIndex].m_VtxElement = -1);

#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER
	param[g_RenderThreadIndex].m_vtxIdx++;
#endif // VTXBUFFER_USES_DEFAULTVERTEXBUFFER	
#if RSG_XENON
	_ReadWriteBarrier();
#endif // RSG_XENON
	m_vtxCount[g_RenderThreadIndex]--;
}

template<class T> inline void VtxPushBuffer<T>::UnLock()
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsLocked);
	Assert(0 == m_vtxCount[g_RenderThreadIndex]);

#if VTXBUFFER_USES_DEFAULTVERTEXBUFFER	
	m_vertexBuffer->UnlockRW();
	
	GRCDEVICE.DrawPrimitive(m_drawModeType, m_vtxDcl, *m_vertexBuffer, (m_vtxOffset[g_RenderThreadIndex] - param[g_RenderThreadIndex].m_vtxIdx), param[g_RenderThreadIndex].m_vtxIdx);
	#if RSG_XENON
		GRCDEVICE.ClearStreamSource(0);
	#endif // RSG_XENON
#elif !RSG_PS3
	GRCDEVICE.EndVertices(param[g_RenderThreadIndex].m_lockPointer);
#endif // VTXBUFFER_USES_DEFAULTVERTEXBUFFER

	param[g_RenderThreadIndex].m_lockPointer = NULL;
	ASSERT_ONLY(m_assertFlags[g_RenderThreadIndex].m_bIsLocked = false);
}

template<class T> inline void VtxPushBuffer<T>::End()
{
	Assert(true == m_assertFlags[g_RenderThreadIndex].m_bIsRunning);
#if 0 == VTXBUFFER_USES_NOOP
	#if RSG_PS3
		GCM_DEBUG(cellGcmSetDrawEnd(GCM_CONTEXT));
		InvalidateSpuGcmState(CachedStates.VertexFormats, ~0);
	#endif	
#endif // 0 == VTXBUFFER_USES_NOOP
	ASSERT_ONLY(m_assertFlags[g_RenderThreadIndex].m_bIsRunning = false);
}

} // namespace rage

#endif //__CUSTOM_VERTEX_PUSH_BUFFER_H__....

