//
//	fwtl\ExtRefAwareBase.cpp
//	
//	Copyright (C) 2008-2011 Rockstar Games.  All Rights Reserved.
//
//

// Rage includes
#include "diag/trap.h"
#include "system/stack.h"
#include "ai/aioptimisations.h"

// Header
#include "ExtRefAwareBase.h"
#include "DebugOwnerHistory.h"

#if !__FINAL

FW_AI_OPTIMISATIONS()

#define REF_AWARE_BASE_ADDS_HISTORY	(!__FINAL && __ASSERT)

namespace rage
{

int fwExtRefAwareBase::m_DynamicallyAllocated = 0;

fwExtRefAwareBase::fwExtRefAwareBase() 
	:	m_RefCount(0), m_pDebug(NULL), m_Flags(0)
{
	if ( m_DynamicallyAllocated )
	{
#if REF_AWARE_BASE_ADDS_HISTORY
		m_pDebug = rage_new fwDebugOwnerHistoryFrame;
		fwDebugOwnerHistoryMovie mov;
		mov.Init( *m_pDebug );

		u32 trace = sysStack::RegisterBacktrace();
		mov.SetCreationPoint( const_cast<fwExtRefAwareBase*>(this), trace );
		m_Flags |= F_DynamicallyAllocated;
#endif
		--m_DynamicallyAllocated;
	}
}

fwExtRefAwareBase::~fwExtRefAwareBase()
{
	if ( m_pDebug )
	{
		delete m_pDebug;
	}

}


void fwExtRefAwareBase::VerifyIntegrity( bool isAllocated ) const
{

	if ( isAllocated )
	{
		// We are apparently still out.
		// Use cases:
		//   The stack case
		//   { fwEvent tmp; <blah>  }
		//		!dynamically allocated, even if regdrefed we are not in the pool so 
		//		we can't do this check
		//	 The dynamic case - we will have been dynamically allocated
		//   If we were never regd ref'd then we can't do a check
		//   Not strictly true - we should have been assigned out so just check we have a reference

		//  The dynamic regdref'd case - this we can do
		//   RegdT a = new T;			<a>
		//   a = NULL;					<!a> but still out e.g. 0 refs
		//
		// We are assigned out - so our ref count had better not be 0 [someone hanging onto us]
		if ( m_RefCount == 0 )
		{
			FailIntegrity( "An object has been orphaned.  See the TTY");
		}
	}
}

void fwExtRefAwareBase::FailIntegrity( const char *ASSERT_ONLY(failReason) ) const
{

#if REF_AWARE_BASE_ADDS_HISTORY
		// We have been orphaned!!
		fwDebugOwnerHistoryMovie mov;
		mov.Init( *m_pDebug );
		mov.PrintDigest( false );
#endif
		PrintAssigned();
		AssertMsg( false, failReason );
}


void fwExtRefAwareBase::SetRefAwareFlags( int flags ) const
{
	// We are at the allocation point - just store our new information - must delay it to construction though
	// Also a task could create another task in it's constructor so we need to ref count here
	if ( flags & F_DynamicallyAllocated )
	{
		++m_DynamicallyAllocated;
	}
	
	Assign( m_Flags, flags );
} 

void fwExtRefAwareBase::AddKnownRef(void **ppReference) const
{
	tBase::AddKnownRef( ppReference	);
	// We were regd-refed at least once
	m_Flags |= F_WasRegdRefed;
	++m_RefCount;

	// Debug recording ///////////////////////////
#if REF_AWARE_BASE_ADDS_HISTORY
	if ( m_RefCount == 1 )
	{
		if ( !m_pDebug )
		{
			m_pDebug = rage_new fwDebugOwnerHistoryFrame;
		}
	}
	Assert( m_pDebug );
	fwDebugOwnerHistoryMovie mov;
	mov.Init( *m_pDebug );

	u32 trace = sysStack::RegisterBacktrace();
	if ( mov.IsNearingFull() )
	{
		fwDebugOwnerHistoryMovie mov;
		mov.Init( *m_pDebug );
		mov.PrintDigest( false );
		PrintAssigned();
		AssertMsg( false, "Somehow this object has got close to filling it's debug ownership record.  See the TTY");
	}
	mov.AddRef( ppReference, trace );
#endif
}

void fwExtRefAwareBase::RemoveKnownRef(void** ppReference) const
{
	tBase::RemoveKnownRef(ppReference);
	Assert( m_RefCount > 0 );
	--m_RefCount;

	// Debug recording ///////////////////////////
#if REF_AWARE_BASE_ADDS_HISTORY
	Assert( m_pDebug );
	fwDebugOwnerHistoryMovie mov;
	mov.Init( *m_pDebug );

	u32 trace = sysStack::RegisterBacktrace();
	if ( mov.IsNearingFull() )
	{
		fwDebugOwnerHistoryMovie mov;
		mov.Init( *m_pDebug );
		mov.PrintDigest( false );
		PrintAssigned();
		AssertMsg( false, "Somehow this object has got close to filling it's debug ownership record.  See the TTY");
	}
	mov.RemoveRef( ppReference, trace );
#endif
}

void fwExtRefAwareBase::ClearAllKnownRefs() const
{
	// Don't clear us unless we are the last one.
	Assert( m_RefCount == 1 );
	tBase::ClearAllKnownRefs();
}

bool fwExtRefAwareBase::IsRefKnown(void** ppReference) const
{
	return tBase::IsRefKnown(ppReference);
}

bool fwExtRefAwareBase::IsReferenced(void) const
{
	return tBase::IsReferenced();
}

void fwExtRefAwareBase::PrintAssigned() const
{
	fwKnownRefHolder *pCurr = m_pKnownRefHolderHead;
	char buffer[512];
	atString name;
	DebugOwnerHistoryResolveAddress( this, name );
	sprintf( buffer, "-=[ %s ]=-\n ", name.c_str() );
	diagLoggedPrintf("%s", buffer);
	while (pCurr)
	{
		sprintf( buffer, "    Still assigned out = [ %p ]\n", pCurr->m_ppReference );
		diagLoggedPrintf("%s", buffer);
		pCurr = pCurr->m_pNext;
	}
}

} // namespace rage

#endif // !__FINAL