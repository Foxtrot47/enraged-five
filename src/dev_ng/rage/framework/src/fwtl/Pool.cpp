
#include "fwtl/pool.h"
#include "diag/trap.h"
#include "system/memmanager.h"
#include "system/memvisualize.h"
#include "system/stack.h"

namespace rage {

#if RAGE_POOL_TRACKING
void RegisterInPoolTracker(const fwBasePool* poolptr, const char* className)
{
	sysMemStartTemp();
	fwBasePoolTracker * pTracker = rage_new fwBasePoolTracker;
	pTracker->SetPool(poolptr);
	pTracker->SetClassName(className);
	PoolTracker::Add(pTracker);
	sysMemEndTemp();
}

void UnregisterInPoolTracker(const fwBasePool* poolptr)
{
	PoolTracker::Remove(poolptr);
}

#if __WIN32
#pragma warning(disable: 4073)
#pragma init_seg(lib)
#endif

#endif // RAGE_POOL_TRACKING

//
// name:		fwBasePool::fwBasePool
// description:	Constructor. Allocates pool array
fwBasePool::fwBasePool(const char* OUTPUT_ONLY(pName), s32 ASSERT_ONLY(redZone), s32 storageSize)
#if RAGE_POOL_SPILLOVER
	: m_SpilloverPool(NULL), m_SpilloverSize(0), m_PoolSize(0)
#endif
{
	m_nStorageSize = storageSize;
	m_aStorage = NULL;
	m_aFlags = NULL;
	ASSERT_ONLY(m_redZone = redZone;)
	m_nSize = 0;
	m_numSlotsUsed = 0;

#if !__NO_OUTPUT	
	m_bSizeIsFromConfigFile = true;
	safecpy(m_name, pName);
	m_peakNumSlotsUsed = 0;
	m_logged = false;
#endif	

	BANK_ONLY(m_poolFullCB = NULL;)
}

#if __PPU
class sysMemAllocator;
extern sysMemAllocator* g_pResidualAllocator;
extern u32 g_bDontUseResidualAllocator;
#endif

#if __XENON
class sysMemAllocator;
extern sysMemAllocator* g_pXenonPoolAllocator;
#endif

#if __BANK
void fwBasePool::PoolFullCallback()
{
	if (m_poolFullCB)
	{
		m_poolFullCB(NULL);
		s32 size = GetSize();
		while (size--)
		{
			void* pItem = GetSlot(size);
			if (pItem)
			{
				m_poolFullCB(pItem);
			}
		}
	}
}
#endif //__BANK

//
// name:		ForAll
// description:	Run a callback on all the objects in a pool
void fwBasePool::ForAll(Callback cb, void* data)
{
	s32 size = GetSize();
	while (size--)
	{
		void* pItem = GetSlot(size);
		if (pItem)
		{
			if(!cb(pItem, data))
				return;
		}
	}
}

void fwBasePool::MakeEmpty()
{
#if RAGE_POOL_SPILLOVER
	if (m_SpilloverPool)
	{
		sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
		Assert(pAllocator);

		for (int i = 0; i < m_SpilloverSize; ++i)
		{
			const void* ptr = m_SpilloverPool[i];
			if (ptr)
				pAllocator->Free(ptr);
		}

		memset(m_SpilloverPool, 0, (sizeof(void*) * m_SpilloverSize));
		m_SpilloverMap.Kill();
	}
#endif

	for (s32 i = 0; i < m_nSize; i++)
	{
		SetIsFree(i, true);
		SetReference(i, 0);
	}

	BuildFreeList();
#if !__NO_OUTPUT
	if(m_logged)
	{
		diagLoggedPrintf("%s: Called MakeEmpty()\n", m_name);
		sysStack::PrintStackTrace();
	}
#endif
}

void fwBasePool::BuildFreeList()
{
	s32 *prev = &m_nFirstFreeIndex;
	for (s32 i = 0; i < GetPoolSize(); i++)
	{
		if (GetIsFree(i))
		{
			*prev = i;
			prev = (s32*) (m_aStorage + i * m_nStorageSize);
		}
	}

	if (prev != &m_nFirstFreeIndex)
		m_nLastFreeIndex = ptrdiff_t_to_int(((u8*)prev - m_aStorage) / m_nStorageSize);
	else
		m_nLastFreeIndex = -1;

	*prev = -1;		// Terminate free list
}

#if RAGE_POOL_NODE_TRACKING
void fwBasePool::Tally(void* ptr)
{
	if (ptr && ::rage::diagTracker::GetCurrent() && sysMemVisualize::GetInstance().HasMisc())
		::rage::diagTracker::GetCurrent()->Tally(ptr, m_nStorageSize, 0);
}

void fwBasePool::UnTally(void* ptr)
{
	if (ptr && ::rage::diagTracker::GetCurrent() && sysMemVisualize::GetInstance().HasMisc())
		::rage::diagTracker::GetCurrent()->UnTally(ptr, m_nStorageSize);
}
#endif

//
// name:		fwBasePool::fwBasePool
// description:	Constructor. Allocates pool array
fwBasePool::fwBasePool(s32 nSize, const char* 
#if !__NO_OUTPUT
					   pName
#endif
					   , s32 ASSERT_ONLY(redZone), s32 storageSize, bool useFlex
						)
#if RAGE_POOL_SPILLOVER
	: m_SpilloverPool(NULL), m_SpilloverSize(0), m_PoolSize(0)
#endif
{
	Assert(nSize > 0);
	m_nStorageSize = storageSize;
	Assert(storageSize >= (int)sizeof(s32));

	m_bOwnsArrays = true;
	Assert(redZone == 0);
	m_nSize = nSize;
	m_numSlotsUsed = 0;
	ASSERT_ONLY(m_redZone = redZone;)
#if !__NO_OUTPUT	
	m_bSizeIsFromConfigFile = true;
	safecpy(m_name, pName);
	m_peakNumSlotsUsed = 0;
	m_logged = false;
#endif	

	BANK_ONLY(m_poolFullCB = NULL;)

	m_aStorage = NULL;
	m_aFlags = NULL;

	AllocStorage(useFlex);
	
	Assert(m_aStorage);
	Assert(m_aFlags);
}

//
// name:		fwBasePool::fwBasePool
// description:	Constructor. Allocates pool array
fwBasePool::fwBasePool(s32 nSize, u8* pPool, u8* pFlags, const char* OUTPUT_ONLY(pName), s32 ASSERT_ONLY(redZone), s32 storageSize)
#if RAGE_POOL_SPILLOVER
	: m_SpilloverPool(NULL), m_SpilloverSize(0), m_PoolSize(0)
#endif
{
	Assert(nSize > 0);

	m_nStorageSize = storageSize;
	Assert(storageSize >= (s32) sizeof(s32));

	m_aStorage = pPool;
	m_aFlags = pFlags;

	m_bOwnsArrays = false;
	ASSERT_ONLY(m_redZone = redZone;)
	Assert(m_aStorage);
	Assert(m_aFlags);

	m_nSize = nSize;
	m_numSlotsUsed = 0;

#if !__NO_OUTPUT	
	m_bSizeIsFromConfigFile = true;
	safecpy(m_name, pName);
	m_peakNumSlotsUsed = 0;
	m_logged = false;
#endif	

	MakeEmpty();

	BANK_ONLY(m_poolFullCB = NULL;)

#if ATL_POOL_SPARSE
	m_SparsePool = NULL;
#endif
}

fwBasePool::fwBasePool(s32 nSize, float RAGE_POOL_SPILLOVER_ONLY(spillover), const char* 
#if !__NO_OUTPUT
	pName
#endif
	, s32 ASSERT_ONLY(redZone), s32 storageSize, bool useFlex
	)
#if RAGE_POOL_SPILLOVER
	: m_SpilloverPool(NULL), m_SpilloverSize(0), m_PoolSize(0)
#endif
{
	Assert(nSize > 0);
	m_nStorageSize = storageSize;	
	Assert(storageSize >= (int)sizeof(s32));

	m_bOwnsArrays = true;
	Assert(redZone == 0);
	m_nSize = nSize;
	m_numSlotsUsed = 0;
	ASSERT_ONLY(m_redZone = redZone;)

#if RAGE_POOL_SPILLOVER
	m_SpilloverSize = static_cast<size_t>(static_cast<float>(nSize) * spillover);
	m_PoolSize = nSize - m_SpilloverSize;
#endif

#if !__NO_OUTPUT	
	m_bSizeIsFromConfigFile = true;
	safecpy(m_name, pName);
	m_peakNumSlotsUsed = 0;
	m_logged = false;
#endif	

	BANK_ONLY(m_poolFullCB = NULL;)

	m_aStorage = NULL;
	m_aFlags = NULL;

	AllocStorage(useFlex);

	Assert(m_aStorage);
	Assert(m_aFlags);
}

//
// name:		fwBasePool::~fwBasePool
// description:	Destructor
fwBasePool::~fwBasePool()
{
#if RAGE_POOL_TRACKING
	UnregisterInPoolTracker(this);
#endif 

	Flush();
#if !__NO_OUTPUT
	if(m_logged)
	{
		diagLoggedPrintf("%s: Pool destroyed\n", m_name);
		sysStack::PrintStackTrace();
	}
#endif
}

//
// name:		Init
// description:	Construct pool arrays
void fwBasePool::Init(s32 nSize, void* pPool, u8* pFlags)
{
	Assert(m_aStorage == NULL);
	Assert(nSize > 0);
	Assert(m_nStorageSize >= (s32) sizeof(s32));

#if RAGE_POOL_SPILLOVER
	Assert(!m_SpilloverPool);
#endif

	m_aStorage = (u8*)pPool;
	m_aFlags = pFlags;

	m_bOwnsArrays = false;

	Assert(m_aStorage);
	Assert(m_aFlags);

	m_nSize = nSize;
	m_numSlotsUsed = 0;

	MakeEmpty();

#if ATL_POOL_SPARSE
	m_SparsePool = NULL;
#endif
}

void fwBasePool::InitAndAllocate(s32 nSize, bool useFlex)
{
	// This function works like Init(), for use with
	//	fwBasePool::fwBasePool(const char* pName, s32 storageSize)
	// but allocates storage for you, as if
	//	fwBasePool::fwBasePool(s32 nSize, const char* pName, s32 storageSize)
	// had been used.

	m_nSize = nSize;
	m_bOwnsArrays = true;
	ASSERT_ONLY(m_redZone = 0;)

	AllocStorage(useFlex);
}

void fwBasePool::InitAndAllocate(s32 size, float RAGE_POOL_SPILLOVER_ONLY(spillover), bool useFlex)
{
#if RAGE_POOL_SPILLOVER
	m_SpilloverSize = static_cast<s32>(static_cast<s32>(size) * spillover);
	m_PoolSize = size - m_SpilloverSize;
#endif

	InitAndAllocate(size, useFlex);
}

//
// name:		Flush
// description:	Delete pool arrays
void fwBasePool::Flush()
{
#if ATL_POOL_SPARSE
	sysMemStartTemp();
	for (int i = 0; i < m_nSize; ++i)
		delete [] (char*) m_SparsePool[i];

	delete [] m_SparsePool;
	m_SparsePool = NULL;
	m_SparseMap.Kill();
	sysMemEndTemp();
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
	if (m_SpilloverPool)
	{
		sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
		Assert(pAllocator);

		for (int i = 0; i < m_SpilloverSize; ++i)
		{
			const void* ptr = m_SpilloverPool[i];
			if (ptr)
				pAllocator->Free(ptr);
		}

		pAllocator->Free(m_SpilloverPool);
		m_SpilloverPool = NULL;
		m_SpilloverMap.Kill();
		m_SpilloverSize = 0;
	}
#endif

	if (GetPoolSize() > 0)
	{
		if(m_bOwnsArrays)
		{
#if __XENON
			if (g_pXenonPoolAllocator && g_pXenonPoolAllocator->IsValidPointer(m_aStorage))
			{
				g_pXenonPoolAllocator->Free(m_aStorage);
				m_aStorage = NULL;
			}

			if (g_pXenonPoolAllocator && g_pXenonPoolAllocator->IsValidPointer(m_aFlags))
			{
				g_pXenonPoolAllocator->Free(m_aFlags);
				m_aFlags = NULL;
			}
#endif	//__XENON

#if __PPU			
			if (g_pResidualAllocator && g_pResidualAllocator->IsValidPointer(m_aStorage))
				g_pResidualAllocator->Free(m_aStorage);
			else if (sysMemManager::GetInstance().GetFlexAllocator()->IsValidPointer(m_aStorage))
				sysMemManager::GetInstance().GetFlexAllocator()->Free(m_aStorage);
			else
				delete[] m_aStorage;

			if (g_pResidualAllocator && g_pResidualAllocator->IsValidPointer(m_aFlags))
				g_pResidualAllocator->Free(m_aFlags);
			else if (sysMemManager::GetInstance().GetFlexAllocator()->IsValidPointer(m_aFlags))
				sysMemManager::GetInstance().GetFlexAllocator()->Free(m_aFlags);
			else
				delete[] m_aFlags;
#else
			if (m_aStorage)
				delete[] m_aStorage;

			if (m_aFlags)
				delete[] m_aFlags;
#endif // __PPU
		}

		m_aStorage = NULL;
		m_aFlags = NULL;

		m_nSize = 0;
		m_numSlotsUsed = 0;
	}
}

void fwBasePool::AllocStorage(bool useFlex)
{
	(void)useFlex;
	Assert(!m_aStorage);
	Assert(!m_aFlags);

#if RAGE_POOL_SPILLOVER
	Assert(!m_SpilloverPool);

	if (m_SpilloverSize > 0)
	{
		sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
		Assert(pAllocator);

		m_SpilloverPool = (void**) pAllocator->Allocate(m_SpilloverSize * sizeof(void*), 16);
		memset(m_SpilloverPool, 0, (sizeof(void*) * m_SpilloverSize));
	}
#endif

#if __XENON
	if (g_pXenonPoolAllocator)
	{
		m_aStorage = (u8*) g_pXenonPoolAllocator->RAGE_LOG_ALLOCATE(GetPoolSize() * m_nStorageSize, 16);
		m_aFlags = (u8*) g_pXenonPoolAllocator->RAGE_LOG_ALLOCATE(GetSize(), 16);
	}
#endif	//__XENON

#if __PPU
	if (!g_bDontUseResidualAllocator)
	{
		// EJ: Residual allocator
		if (g_pResidualAllocator)
		{
			m_aStorage = static_cast<u8*>(g_pResidualAllocator->RAGE_LOG_ALLOCATE(GetPoolSize() * m_nStorageSize, 16));
			m_aFlags = static_cast<u8*>(g_pResidualAllocator->RAGE_LOG_ALLOCATE(GetSize(), 16));
		}
	}

	// EJ: Pool allocator
	if (COMMERCE_CONTAINER_ONLY(useFlex &&) sysMemManager::GetInstance().IsFlexHeapEnabled())
	{
		if (!m_aStorage)
			m_aStorage = static_cast<u8*>(sysMemManager::GetInstance().GetFlexAllocator()->RAGE_LOG_ALLOCATE(GetPoolSize() * m_nStorageSize, 16));

		if (!m_aFlags)
			m_aFlags = static_cast<u8*>(sysMemManager::GetInstance().GetFlexAllocator()->RAGE_LOG_ALLOCATE(GetSize(), 16));
	}
#endif // __PPU
	
	// EJ: Default allocator
	if (!m_aStorage)
		m_aStorage = rage_new u8[GetPoolSize() * m_nStorageSize];

	if (!m_aFlags)
		m_aFlags = rage_new u8[GetSize()];

	MakeEmpty();

#if !__NO_OUTPUT
	Displayf("Pool %s size : %d each %d start %p end %p", m_name, GetPoolSize() * m_nStorageSize, m_nStorageSize, m_aStorage, &m_aStorage[GetPoolSize() * m_nStorageSize]);
#endif

#if ATL_POOL_SPARSE
	sysMemStartTemp();
	m_SparsePool = rage_new void*[m_nSize];
	memset(m_SparsePool, 0, sizeof(void*)*m_nSize);
	sysMemEndTemp();
#endif // ATL_POOL_SPARSE
}

//
// name:		Reset
// description:	Reset free index in pool as if the pool has just been constructed
void fwBasePool::Reset()
{
	Assertf(GetNoOfUsedSpaces() == 0, "Can't reset pool while it has entries allocated");

	m_nFirstFreeIndex = 0;
	m_nLastFreeIndex = GetPoolSize() - 1;
	MakeEmpty();
}

void* fwBasePool::New()
{
#if ATL_POOL_SPARSE
	if(m_SparsePool)
	{
		u32 size = m_nSize;
		u32 count = m_nSize;
		u32 index = m_numSlotsUsed;
		for(; count; count--, index++)
		{
			if(index == size) index = 0;
			if(GetIsFree(index))
			{
				sysMemStartTemp();
				u8* ptr = rage_new u8[m_nStorageSize];
				m_SparseMap.Insert(ptr, index);

				sysMemEndTemp();
				m_SparsePool[index] = ptr;

				SetIsFree(index, false);
				SetReference(index, GetReference(index) + 1);

				m_numSlotsUsed++;
#if !__NO_OUTPUT
				m_peakNumSlotsUsed = Max(m_peakNumSlotsUsed, m_numSlotsUsed);
				if(m_logged)
				{
					diagLoggedPrintf("%s: allocating ptr %p from sparse pool\n", m_name, ptr);
					sysStack::PrintStackTrace();
				}
#endif
				return ptr;
			}
		}
		m_nFirstFreeIndex = -1;
	}
#endif // ATL_POOL_SPARSE

	if (m_nFirstFreeIndex == -1)
	{
#if RAGE_POOL_SPILLOVER
		if (m_SpilloverPool)
		{
			s32 count = m_SpilloverSize;
			s32 index = m_numSlotsUsed;

			for (; count; count--, index++)
			{
				if (index == m_nSize)
					index = m_PoolSize;

				const s32 i = index - m_PoolSize;

				if (!m_SpilloverPool[i])
				{
					RAGE_POOL_SPILLOVER_NOTIFY_ONLY(Assertf(0, "fwPool is full (element size=%d, pool size=%d) - Spilling over...", (int) m_nStorageSize, (int) m_PoolSize);)
					sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
					Assert(pAllocator);

					void* ptr = pAllocator->Allocate(m_nStorageSize, 16);
					m_SpilloverMap.Insert(ptr, (u32)index);							
					m_SpilloverPool[i] = ptr;

					SetIsFree(index, false);
					SetReference(index, GetReference(index) + 1);
					m_numSlotsUsed++;
#if !__NO_OUTPUT
					m_peakNumSlotsUsed = Max(m_peakNumSlotsUsed, m_numSlotsUsed);
					if(m_logged)
					{
						diagLoggedPrintf("%s: allocating ptr %p from spillover pool\n", m_name, ptr);
						sysStack::PrintStackTrace();
					}
#endif
					return ptr;
				}
			}
		}		
#endif

#if __BANK
		PoolFullCallback();
#endif
#if !__NO_OUTPUT
		if(m_bSizeIsFromConfigFile)
		{
			Quitf("%s Pool Full, Size == %d (you need to raise %s PoolSize in common/data/gameconfig.xml)", m_name, m_nSize, m_name);
		}
		else
		{
			Quitf("%s Pool Full, Size == %d (See a programmer)", m_name, m_nSize);
		}
#endif
		return NULL;
	}

	s32 freeIndex = m_nFirstFreeIndex;
	s32 *nextFree = (s32*)(m_aStorage + freeIndex * m_nStorageSize);
	m_nFirstFreeIndex = *nextFree;
	if (m_nFirstFreeIndex == -1)
		m_nLastFreeIndex = -1;
	AssertMsg(m_nFirstFreeIndex >= -1 && m_nFirstFreeIndex < m_nSize,"Pool free list corrupted, likely by memory stomp");
	void *result = (void*) nextFree;

	SetIsFree(freeIndex, false);
	SetReference(freeIndex, GetReference(freeIndex) + 1);

	m_numSlotsUsed++;
#if !__NO_OUTPUT
	if(m_numSlotsUsed > m_peakNumSlotsUsed)
	{
		m_peakNumSlotsUsed = m_numSlotsUsed;

		Assertf(!m_redZone || m_numSlotsUsed + m_redZone > m_nSize, "%s Pool almost full, only %d out of %d slots left (See a programmer)", m_name, m_numSlotsUsed - m_nSize, m_nSize);
	}
	if(m_logged)
	{
		diagLoggedPrintf("%s: allocating ptr %p\n", m_name, result);
		sysStack::PrintStackTrace();
	}
#endif

	RAGE_POOL_NODE_TRACKING_ONLY(Tally(result);)
	return result;
}

///////////////////////////////////////////////////////
////////////////ONLY USE FOR LOADING GAME
/////////////////////////////////////////////////////

void* fwBasePool::New(s32 index)
{
	const u32 i = (index >> 8);
	Assertf(i < (u32) m_nSize, "Trying to allocate out of range slot");
	Assertf(GetIsFree(i), "Trying to allocate already allocated slot in pool");
	SetIsFree(i, false);
	SetReference(i, (u8) (index & 255) );

#if ATL_POOL_SPARSE
	if(m_SparsePool)
	{
		sysMemStartTemp();
		u8* pT = rage_new u8[m_nStorageSize];
		m_SparseMap.Insert(pT, i);
		sysMemEndTemp();
		m_SparsePool[i] = pT;
		m_numSlotsUsed++;
#if !__NO_OUTPUT
		if(m_logged)
		{
			diagLoggedPrintf("%s: allocating(idx) ptr %p from sparse pool\n", m_name, pT);
			sysStack::PrintStackTrace();
		}
#endif
		return pT;
	}
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
	if (m_nFirstFreeIndex == -1)
	{
		if (m_SpilloverPool)
		{
			RAGE_POOL_SPILLOVER_NOTIFY_ONLY(Assertf(0, "fwPool is full (element size=%d, pool size=%d) - Spilling over...", (int) m_nStorageSize, (int) m_PoolSize);)
			sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
			Assert(pAllocator);

			void* ptr = pAllocator->Allocate(m_nStorageSize, 16);
			m_SpilloverMap.Insert(ptr, i);
			const int i2 = i - m_PoolSize;
			m_SpilloverPool[i2] = ptr;
			m_numSlotsUsed++;
#if !__NO_OUTPUT
			m_peakNumSlotsUsed = Max(m_peakNumSlotsUsed, m_numSlotsUsed);
			if(m_logged)
			{
				diagLoggedPrintf("%s: allocating(idx) ptr %p from spillover pool\n", m_name, ptr);
				sysStack::PrintStackTrace();
			}
#endif
			return ptr;
		}		
	}
#endif

	u8* pT = &m_aStorage[i * m_nStorageSize];
	// Locate the object in the free list
	index>>=8;
	s32 *prev = &m_nFirstFreeIndex;
	while (*prev != -1 && *prev != index)
		prev = (s32*)(m_aStorage + *prev * m_nStorageSize);
	Assertf(*prev == index,"Free list corrupted");
	// Patch the free list around the item we just allocated
	*prev = *(s32*)(m_aStorage + index * m_nStorageSize);

	// If this item was last on the free list, update that too
	if (m_nLastFreeIndex == index)
		m_nLastFreeIndex = (prev == &m_nFirstFreeIndex)? -1 : ptrdiff_t_to_int(((u8*)prev - m_aStorage) / m_nStorageSize);

	m_numSlotsUsed++;
#if !__NO_OUTPUT
	if(m_numSlotsUsed > m_peakNumSlotsUsed)
		m_peakNumSlotsUsed = m_numSlotsUsed;
	if(m_logged)
	{
		diagLoggedPrintf("%s: allocating(idx) ptr %p\n", m_name, pT);
		sysStack::PrintStackTrace();
	}
#endif
	return (void*)pT;
}

void fwBasePool::Delete(void* pT)
{
	Assert(pT != NULL);

	RAGE_POOL_NODE_TRACKING_ONLY(UnTally(pT));

#if ATL_POOL_SPARSE
	if(m_SparsePool)
	{
		u32 index = *m_SparseMap.Access(pT);
		sysMemStartTemp();
		delete [] (char*) pT;
		m_SparseMap.Delete(pT);
		sysMemEndTemp();
		m_SparsePool[index] = NULL;
		m_numSlotsUsed--;
		SetIsFree(index, true);
#if !__NO_OUTPUT
		if(m_logged)
		{
			diagLoggedPrintf("%s: freeing ptr %p to sparse pool\n", m_name, pT);
			sysStack::PrintStackTrace();
		}
#endif
		return;
	}
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
	if (m_SpilloverPool)
	{
		if (!IsInPool(pT))
		{
			const u32* pIndex = m_SpilloverMap.Access(pT);
			Assertf(pIndex, "Spillover pointer index for %p is NULL! The game will crash now.", pT);

			const u32 index = *pIndex;
			m_SpilloverPool[index - m_PoolSize] = NULL;
			m_SpilloverMap.Delete(pT);

			sysMemAllocator* pAllocator = sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL);
			Assert(pAllocator);

			pAllocator->Free(pT);
			m_numSlotsUsed--;
			SetIsFree(index, true);
#if !__NO_OUTPUT
			if(m_logged)
			{
				diagLoggedPrintf("%s: freeing ptr %p to spillover pool\n", m_name, pT);
				sysStack::PrintStackTrace();
			}
#endif
			return;
		}
	}						
#endif

	s32 index = GetJustIndex(pT);
	s32 *node = (s32*) (m_aStorage + index * m_nStorageSize);
	OUTPUT_ONLY(memset((char*)node, 0xDD, m_nStorageSize));
	// Newly deleted item will be at end of free list
	*node = -1;

	// If the list was empty, make sure the head pointer is now correct
	if (m_nFirstFreeIndex == -1)
		m_nFirstFreeIndex = index;

	// Add item to end of existing free list
	s32 *prev = m_nLastFreeIndex==-1? &m_nLastFreeIndex : (s32*)(m_aStorage + m_nStorageSize * m_nLastFreeIndex);
	*prev = index;
	m_nLastFreeIndex = index;

	SetIsFree(index, true);

	m_numSlotsUsed--;
#if !__NO_OUTPUT
	if(m_logged)
	{
		diagLoggedPrintf("%s: freeing ptr %p\n", m_name, pT);
		sysStack::PrintStackTrace();
	}
#endif
}

// Get index into array
s32 fwBasePool::GetJustIndex_NoFreeAssert(const void* pT) const
{
	s32 index = GetJustIndexFast(pT);

	Assertf(ATL_POOL_SPARSE || RAGE_POOL_SPILLOVER_ONLY(IsValidSpilloverPtr(pT) ||) ((u8*)pT) == ((u8*)&m_aStorage[index*m_nStorageSize]), "fwBasePool::GetJustIndex_NoFreeAssert - pT = %p, &m_aStorage[0] = %p, index = %d, m_nStorageSize = %d", pT, &m_aStorage[0], index, m_nStorageSize);	// Make sure it's a proper address
	Assertf((u32) index < (u32) m_nSize, "fwBasePool::GetJustIndex_NoFreeAssert - index = %d, m_nSize = %d", index, m_nSize);

	return (index);
}

//
// name:		IsValidPtr
// description:	Returns if the pointer is pointing to something in this pool that is
//				currently allocated
bool fwBasePool::IsValidPtr(void* pT) const
{
#if ATL_POOL_SPARSE
	if(m_SparsePool)
		return m_SparseMap.Access(pT) != NULL;
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
	if (!IsInPool(pT))
		return m_SpilloverMap.Access(pT) != NULL;
#endif // RAGE_POOL_SPILLOVER

	u8 *firstValidPtr = (u8*)&m_aStorage[0];
	u8 *lastValidPtr  = (u8*)&m_aStorage[(GetPoolSize() - 1) * m_nStorageSize];
	u8 *testPtr       = (u8*)pT;

	if(testPtr >= firstValidPtr && testPtr <= lastValidPtr)
	{
		if(((testPtr - firstValidPtr) % m_nStorageSize) == 0)
		{
			s32 index = ptrdiff_t_to_int(testPtr - firstValidPtr) / m_nStorageSize;

			if(!GetIsFree(index))
			{
				return true;
			}
		}
	}

	return false;
}

s32 fwBasePool::GetIndex(const void* pT) const
{
	s32 index = GetJustIndex_NoFreeAssert(pT);//(((u8*)pT) - ((u8*)&m_aStorage[0])) / m_nStorageSize;

	return ((index << 8) + m_aFlags[index]);
}

s32 fwBasePool::GetIndexFast(const void* pT) const
{
	s32 index = GetJustIndexFast(pT);//(((u8*)pT) - ((u8*)&m_aStorage[0])) / m_nStorageSize;

	return ((index << 8) + m_aFlags[index]);
}

void* fwBasePool::GetAt(s32 index)
{
	const u32 i = (index >> 8);
	TrapGE(i, (u32) m_nSize);

	if (m_aFlags[i] == (index & 0xff))
	{
#if ATL_POOL_SPARSE
		if (m_SparsePool)
			return m_SparsePool[i];
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
		if (IsInSpillover(i))
			return m_SpilloverPool[i - m_PoolSize];
#endif // RAGE_POOL_SPILLOVER

		return (void*) &m_aStorage[i * m_nStorageSize];	
	}
	else
	{
		return NULL;
	}
}

// Get index into array
s32 fwBasePool::GetJustIndex(const void* pT) const
{
	s32 index = GetJustIndex_NoFreeAssert(pT);

	Assert(!GetIsFree(index));									// Make sure the element isn't free

	return (index);
}

s32 fwBasePool::GetJustIndexFast(const void* pT) const
{
#if ATL_POOL_SPARSE
	if (m_SparsePool)
		return *m_SparseMap.Access(pT);
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
	if (!IsInPool(pT))
	{
		const u32* pIndex = m_SpilloverMap.Access(pT);
		Assertf(pIndex, "Spillover pointer index for %p is NULL! The game will crash now.", pT);
		return *pIndex;
	}
#endif // RAGE_POOL_SPILLOVER

	return ptrdiff_t_to_int((((u8*)pT) - ((u8*)&m_aStorage[0])) / m_nStorageSize);
}

} // namespace rage
