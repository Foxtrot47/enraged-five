
// name:		LinkList.h
// description:	Template link list
// written by:	Adam Fowler
//
#ifndef FWTL_LINKLIST_H
#define FWTL_LINKLIST_H

#include "system/new.h"

template<class T> class fwLinkList;

//
// name:		fwLink
// description:	Template class describing a link in a link list
//
template<class T>
class fwLink
{
	friend class fwLinkList<T>;
public:
	void Insert(fwLink<T>* pLink);
	void Remove();
	
	fwLink* GetPrevious() const {return m_pPrev;}
	fwLink* GetNext() const {return m_pNext;}

	T item;
	
private:
	fwLink* m_pPrev;
	fwLink* m_pNext;
};

//
// name:		fwLinkList
// description:	Template class describing a link list with a maximum number
//				of elements
//
template<class T>
class fwLinkList
{
public:
	fwLinkList() {m_pStore = NULL; m_bLock = false;}
	~fwLinkList() { if (m_pStore){ Shutdown();} }
	void Init(int size, int align=4);
	void Shutdown();
	void Clear();
	
	fwLink<T>* Insert();
	fwLink<T>* Insert(const T& item);
	fwLink<T>* InsertSorted(const T& item);
	void Remove(fwLink<T>* pLink);
	fwLink<T>* GetFirst() {return &m_firstLink;}
	fwLink<T>* GetLast() {return &m_lastLink;}
	const fwLink<T>* GetFirst() const {return &m_firstLink;}
	const fwLink<T>* GetLast() const {return &m_lastLink;}

	const fwLink<T>* GetStorePtr() const { return m_pStore; }

	int GetNumFree() const;
	int GetNumUsed() const;

	void Lock() {m_bLock = true;}
	void Unlock() {m_bLock = false;}

#if __DEV
	void CheckIntegrity();
#endif

private:
	fwLink<T> m_firstLink;
	fwLink<T> m_lastLink;
	fwLink<T> m_firstFreeLink;
	fwLink<T> m_lastFreeLink;
	
	fwLink<T>* m_pStore;

	bool	m_bLock;
};

//
// name:		fwLink::Insert
// description:	Insert a link
//
template<class T>
void fwLink<T>::Insert(fwLink<T>* pLink)
{
	pLink->m_pNext = m_pNext;
	m_pNext->m_pPrev = pLink;
	pLink->m_pPrev = this;
	m_pNext = pLink;
}
//
// name:		fwLink::Remove
// description:	Remove a link
//
template<class T>
void fwLink<T>::Remove()
{
	m_pNext->m_pPrev = m_pPrev;
	m_pPrev->m_pNext = m_pNext;
}


//
// name:		fwLinkList::Init
// description:	Initialises link list
template<class T> void fwLinkList<T>::Init(int size, int align)
{
	Assert(m_pStore == NULL);
	m_pStore = (fwLink<T>*)(rage_aligned_new(align) rage::u8 [ sizeof(fwLink<T>) * size]);
	m_firstLink.m_pNext = &m_lastLink;
	m_lastLink.m_pPrev = &m_firstLink;
	m_firstFreeLink.m_pNext = &m_lastFreeLink;
	m_lastFreeLink.m_pPrev = &m_firstFreeLink;
	
	while(size--)
	{
		m_firstFreeLink.Insert(m_pStore+size);
	}
}

//
// name:		fwLinkList::Shutdown
// description:	Remove linklist
template<class T> void fwLinkList<T>::Shutdown()
{
	Assert(m_bLock == false);

	delete[] m_pStore;
	m_pStore = NULL;
}

//
// name:		fwLinkList<T>::Clear
// description:	Free all the links in the link list
template<class T> void fwLinkList<T>::Clear()
{
	Assert(m_bLock == false);

	fwLink<T>* pLink = m_firstLink.m_pNext;
	while(pLink != &m_lastLink)
	{
		Remove(m_firstLink.m_pNext);
		pLink = m_firstLink.m_pNext;
	}
}

//
// name:		fwLinkList<T>::Insert
// description:	Insert a new link into the link list without setting its contents
template<class T> fwLink<T>* fwLinkList<T>::Insert()
{
	//insert link in here
	fwLink<T>* pNewLink = m_firstFreeLink.m_pNext;
	if(pNewLink != &m_lastFreeLink)
	{
		pNewLink->Remove();
		m_firstLink.Insert(pNewLink);
		return pNewLink;
	}
	return NULL;	
}


//
// name:		fwLinkList<T>::Insert
// description:	Insert a new item into the link list
template<class T> fwLink<T>* fwLinkList<T>::Insert(const T& item)
{
	Assert(m_bLock == false);

	//insert link in here
	fwLink<T>* pNewLink = m_firstFreeLink.m_pNext;
	if(pNewLink != &m_lastFreeLink)
	{
		pNewLink->item = item;
		pNewLink->Remove();
		m_firstLink.Insert(pNewLink);
		return pNewLink;
	}
	return NULL;	
}


//
// name:		fwLinkList<T>::InsertSorted
// description:	Insert a new item into a sorted list
template<class T> fwLink<T>* fwLinkList<T>::InsertSorted(const T& item)
{
	Assert(m_bLock == false);

	fwLink<T>* pLink = m_firstLink.m_pNext;
	
	while(pLink != &m_lastLink)
	{
		if(pLink->item >= item)
		{
			break;
		}
		pLink = pLink->m_pNext;
	}	

	//insert link in here
	fwLink<T>* pNewLink = m_firstFreeLink.m_pNext;
	if(pNewLink != &m_lastFreeLink)
	{
		pNewLink->item = item;
		pNewLink->Remove();
		pLink->m_pPrev->Insert(pNewLink);
		return pNewLink;
	}
	return NULL;
}


//
// name:		fwLinkList<T>::Remove
// description:	Remove a link from the link list
template<class T> void fwLinkList<T>::Remove(fwLink<T>* pLink)
{
	Assert(m_bLock == false);

	//remove link from allocated list and add back into free list
	pLink->Remove();
	m_firstFreeLink.Insert(pLink);
}

//
// name:		fwLinkList<T>::GetNumFree
// description:	Return how many links are free
template<class T> int fwLinkList<T>::GetNumFree() const
{
	fwLink<T>* pLink = m_firstFreeLink.m_pNext;
	int count = 0;
	
	while(pLink != &m_lastFreeLink)
	{
		pLink = pLink->m_pNext;
		count++;
	}	
	return count;	
}

//
// name:		fwLinkList<T>::GetNumUsed
// description:	Return how many links are used
template<class T> int fwLinkList<T>::GetNumUsed() const
{
	fwLink<T>* pLink = m_firstLink.m_pNext;
	int count = 0;
	
	while(pLink != &m_lastLink)
	{
		pLink = pLink->m_pNext;
		count++;
	}	
	return count;	
}

//
// name:		fwLinkList<T>::CheckIntegrity
// description:	Check that link list is still doubly linked
#if __DEV
template<class T> void fwLinkList<T>::CheckIntegrity()
{
	fwLink<T>* pLink = m_firstLink.m_pNext;
	
	while(pLink != &m_lastLink)
	{
		Assert(pLink->m_pPrev->m_pNext == pLink);
		Assert(pLink->m_pNext->m_pPrev == pLink);
		pLink = pLink->m_pNext;
	}	
}
#endif //__DEV...

#endif //FWTL_LINKLIST_H
