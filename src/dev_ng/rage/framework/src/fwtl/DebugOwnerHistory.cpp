//
//	fwtl\DebugOwnerHistory.cpp
//	
//	Copyright (C) 2008-2011 Rockstar Games.  All Rights Reserved.
//

// Rage includes
#include "system/stack.h"

// Framework includes
#include "ai/aioptimisations.h"
#include "diag/channel.h"

// Header
#include "DebugOwnerHistory.h"

FW_AI_OPTIMISATIONS()

namespace rage
{

// us is what we pointed at.
	
// Default ptr storage resolver ////////////////////////////////////////////
void gDefaultDebugOwnerHistoryResolver( const void *p_Thing, atString &retName, bool, bool isPointerAndContentsWanted )
{
	char scratchBuffer[512];
	typedef void * const tpConstVoid;
	if ( isPointerAndContentsWanted )
	{
		sprintf( scratchBuffer, "%p (points @ %p)", p_Thing, *reinterpret_cast<tpConstVoid*>(p_Thing));
	}
	else
	{
		sprintf( scratchBuffer, "%p", p_Thing );
	}
	retName = scratchBuffer;
}

static fwtDebugOwnerHistoryResolver g_Resolver = &gDefaultDebugOwnerHistoryResolver;

void SetDebugOwnerHistoryResolver( fwtDebugOwnerHistoryResolver resolveFunc )
{
	if ( resolveFunc == NULL )
	{
		g_Resolver = &gDefaultDebugOwnerHistoryResolver;
	}
	else
	{
		g_Resolver = resolveFunc;
	}
}

void DebugOwnerHistoryResolveAddress( const void *addr, atString &retName )
{
	(*g_Resolver)( addr, retName, true, false );
}

// fwDebugOwnerHistoryCompressedOp //////////////////////////////////////////////////////////////

fwDebugOwnerHistoryCompressedOp::fwDebugOwnerHistoryCompressedOp() : m_All(0)
{
}

// fwDebugOwnerHistoryFrame /////////////////////////////////////////////////////////////////////

fwDebugOwnerHistoryFrame::fwDebugOwnerHistoryFrame() : m_Cursor(0)
{
}

fwDebugOwnerHistoryFrame *fwDebugOwnerHistoryFrame::GetNext()
{
	if ( IsNextValid() )
	{
		return m_pNext;
	}
	return NULL;
}

int fwDebugOwnerHistoryFrame::GetCount() const
{
	if ( IsNextValid() )
	{
		return fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE;
	}
	return m_Cursor;
}

bool fwDebugOwnerHistoryFrame::IsFull() const
{
	return GetCount() == fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE;
}

size_t &fwDebugOwnerHistoryFrame::operator[]( int index )
{
	TrapLT( index, 0 );
	TrapGE( index, GetCount() );
	return m_Data[ index ];
}

size_t fwDebugOwnerHistoryFrame::operator[]( int index ) const
{
	TrapLT( index, 0 );
	TrapGE( index, GetCount() );
	return m_Data[ index ];
}

void fwDebugOwnerHistoryFrame::Push( size_t id )
{
	TrapGT( m_Cursor, GetCount() );
	m_Data[ m_Cursor ] = id;
	++m_Cursor;
}

void fwDebugOwnerHistoryFrame::SetNext( fwDebugOwnerHistoryFrame & next )
{
	Assert( IsFull() );
	Assert( !GetNext() );
	m_pNext = &next;
}

// fwDebugOwnerHistoryMovie /////////////////////////////////////////////////////////////////////

void fwDebugOwnerHistoryMovie::Init( fwDebugOwnerHistoryFrame & startFrame )
{
	m_DataCount = 0;

	fwDebugOwnerHistoryFrame *p_Curr = &startFrame;
	do 
	{
		m_FramePtrs.Push( p_Curr );
		m_DataCount += p_Curr->GetCount();

		p_Curr = p_Curr->GetNext();

	} while ( p_Curr );
}

size_t &fwDebugOwnerHistoryMovie::operator [](int globalIndex)
{
	TrapGE( globalIndex, GetDataCount() );
	fwDebugOwnerHistoryFrame &frameOfRef = *(m_FramePtrs[ (globalIndex / fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE) ]);
	return frameOfRef.operator[](globalIndex % fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE);
}

size_t fwDebugOwnerHistoryMovie::operator [](int globalIndex) const
{
	TrapGE( globalIndex, GetDataCount() );
	const fwDebugOwnerHistoryFrame &frameOfRef = *(m_FramePtrs[ (globalIndex / fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE) ]);
	return frameOfRef.operator[](globalIndex % fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE);
}

int	fwDebugOwnerHistoryMovie::Find( size_t id ) const
{
	int frameCount = GetFrameCount();
	for ( int i = 0; i < frameCount; ++i )
	{
		fwDebugOwnerHistoryFrame &frame = *m_FramePtrs[ i ];

		int frameOfRefCount = frame.GetCount();

		for ( int j = 0; j < frameOfRefCount; ++j )
		{
			if ( frame[j] == id )
			{
				return i * fwDebugOwnerHistoryFrame::FRAME_DATA_SIZE + j;
			}
		}
	}
	return -1;
}

int fwDebugOwnerHistoryMovie::Find( const fwDebugOwnerHistoryOp &op ) const
{
	fwDebugOwnerHistoryOp opCurrent;
	int currIndex = GetFirstOperation();
	int nextIndex;
	while (currIndex != -1 )
	{
		nextIndex = opCurrent.ExpandFrom(*this, currIndex, false );
		if ( opCurrent.m_Operation == op.m_Operation )
		{
			if ( opCurrent.m_PC == op.m_PC )
			{
				if ( opCurrent.m_Addr1 == op.m_Addr1 )
				{
					return currIndex;
				}
			}
		}
		currIndex = nextIndex;
	};

	return -1;

}

void fwDebugOwnerHistoryMovie::PushAddingFrameIfNecessary( size_t dat )
{
	// Get the last frame
	TrapLT( GetFrameCount(), 1 );
	fwDebugOwnerHistoryFrame * p_EndFrame = m_FramePtrs[GetFrameCount()-1];
	Assert( !p_EndFrame->GetNext() );

	// Is it full?
	if ( p_EndFrame->IsFull() )
	{
		// If so, create a new one
		fwDebugOwnerHistoryFrame *p_Next = rage_new fwDebugOwnerHistoryFrame;
		// Attach us
		p_EndFrame->SetNext( *p_Next );
		// Update the frame store as well
		m_FramePtrs.Push( p_Next );
		// Now re-push us
		return PushAddingFrameIfNecessary( dat );
	}
	// Push us into the frame
	p_EndFrame->Push( dat );
	// Increase the amount of data
	++m_DataCount;

}

int fwDebugOwnerHistoryMovie::GetDeltaPushIfNecessary( size_t dat )
{
	int globalIndex = Find( dat );
	if ( globalIndex == -1 )
	{
		PushAddingFrameIfNecessary( dat );
		return -1;
	}
	// Get the offset (where we found us - where we'd write to)
	int offset = globalIndex - m_DataCount;
	// Outside our storage range? - allow for being incremented up to
	// 3 times extra (-1, -3) in case we have to be shuffled by further
	// things on the stack
	if ( offset < -(( 1 << fwDebugOwnerHistoryCompressedOp::OFFSET_BITS )-4))
	{
		// Can link that far back, so we need to dupe the data
		PushAddingFrameIfNecessary( dat );
		return -1;
	}
	return offset;
}

bool fwDebugOwnerHistoryMovie::IsNearingFull() const
{
	return GetFrameCount() >= (MAX_DEBUG_DATA_FRAMES - 1);
}

void fwDebugOwnerHistoryMovie::Push( fwDebugOwnerHistoryOp::eOperation opCode, tPtrPtr addr, tPC pc  )
{
	fwDebugOwnerHistoryOp check;
	check.m_Operation = opCode;
	check.m_Addr1 = addr;
	check.m_PC = pc;
	int preexisting = Find( check );
	if ( preexisting != -1 )
	{
		fwDebugOwnerHistoryCompressedOp &op = *reinterpret_cast<fwDebugOwnerHistoryCompressedOp *>(&operator[](preexisting));
		if ( op.m_Component.m_Count < fwDebugOwnerHistoryOp::MAX_OP_COUNTS )
		{
			++op.m_Component.m_Count;
		}
		return;
	}


	fwDebugOwnerHistoryCompressedOp op;
	op.m_Component.m_Op = opCode;
	op.m_Component.m_Count = 1;
	int originalPos = GetDataCount();
	int pcOffset = GetDeltaPushIfNecessary( pc );
	int prePushCount = GetDataCount();
	int ptrOffset = GetDeltaPushIfNecessary( reinterpret_cast<size_t>(addr) );
	// Incremented?
	if ( prePushCount != GetDataCount() )
	{
		// pc delta is now another 1 away
		--pcOffset;
	}
	op.m_Component.m_Addr = -ptrOffset;
	op.m_Component.m_PC = -pcOffset;
	// Don't forget we'll push - so we need + 1
	int opDelta = GetDataCount() - originalPos + 1;
	// Don't forget oldCount is recorded after the initial find (in this case of the PC)
	// So when we get it's count stamp, it will be 1 on the very first time.
	if ( originalPos != 0)
	{
		// Back link it
		size_t oldVal = operator[](originalPos-1);
		fwDebugOwnerHistoryCompressedOp tmp;
		tmp.m_All = oldVal;
		tmp.m_Component.m_Next = opDelta;
		operator[](originalPos-1) = tmp.m_All;
	}
	PushAddingFrameIfNecessary( op.m_All );
}

bool fwDebugOwnerHistoryMovie::IsRelinquished( tPtrPtr addr, int startGlobalOpIndex, int endGlobalIndex ) const
{
	fwDebugOwnerHistoryOp op;
	int currIndex = startGlobalOpIndex;
	int nextIndex;
	while (currIndex < endGlobalIndex && currIndex != -1 )
	{
		nextIndex = op.ExpandFrom(*this, currIndex, true );
		if ( op.m_Operation == fwDebugOwnerHistoryOp::Relinquish )
		{
			if ( op.m_Addr1 == addr )
			{
				return true;
			}
		}
		currIndex = nextIndex;
	};
	return false;
}

fwDebugOwnerHistoryMovie::tPtrPtr 
fwDebugOwnerHistoryMovie::GetLongestOwner( int globalIndexStop ) const
{
	int currentOpIndex = GetFirstOperation();
	Assert( currentOpIndex != -1 );
	int nextOpIndex;
	fwDebugOwnerHistoryOp currentOp;

	do 
	{
		nextOpIndex = currentOp.ExpandFrom(*this, currentOpIndex, true );
		// Grabbed a reference?
		if ( currentOp.m_Operation == fwDebugOwnerHistoryOp::Own )
		{
			// Check it's not relinquished 
			if ( !IsRelinquished( currentOp.m_Addr1, nextOpIndex, globalIndexStop ) )
			{
				return currentOp.m_Addr1;
			}
		}
		currentOpIndex = nextOpIndex;

	} while (currentOpIndex != -1 );
	Assert( false );
	return NULL;

}

void fwDebugOwnerHistoryMovie::AddRef( tPtrPtr addr, tPC loc )
{
	// We need to CRC
	Push( fwDebugOwnerHistoryOp::Own, addr, loc );
}

void fwDebugOwnerHistoryMovie::RemoveRef( tPtrPtr addr, tPC loc )
{
	Push( fwDebugOwnerHistoryOp::Relinquish, addr, loc );
}

void fwDebugOwnerHistoryMovie::SetCreationPoint( tPtr addr, tPC loc )
{
	Push( fwDebugOwnerHistoryOp::New, reinterpret_cast<tPtrPtr>(addr), loc );
}

fwDebugOwnerHistoryCompressedOp fwDebugOwnerHistoryMovie::GetOp( int globalIndex ) const
{
	fwDebugOwnerHistoryCompressedOp ret;
	ret.m_All = operator[]( globalIndex );
	return ret;
}

void fwDebugOwnerHistoryMovie::PrintDigest( bool 
#if !__FINAL
						 inDestructor
#endif
						 ) const
{
#if !__FINAL

	fwDebugOwnerHistoryOp op;
	int currIndex = GetFirstOperation();
	Assert( currIndex != -1 );
	int nextIndex;
	char buffer[512];
	char buffer_aux[64];
	struct sOpData 
	{
		const char *name;
	};
	static sOpData opData[] =
	{
		{ "Invalid"  },
		{ "Newed " },
		{ "Owned by" },
		{ "Relinquished by"},
	};
	atString addr1;
	(*g_Resolver)( op.m_Addr1, addr1, !inDestructor, false );

	while (currIndex != -1 )
	{
		nextIndex = op.ExpandFrom(*this, currIndex, false );
#if __ASSERT
		sysStack::PrintRegisteredBacktrace( static_cast<u16>(op.m_PC) );
#endif
		if ( op.m_Count == fwDebugOwnerHistoryOp::MAX_OP_COUNTS )
		{
			sprintf(buffer_aux, "many" );
		}
		else
		{
			sprintf(buffer_aux, "%d", op.m_Count );
		}
		atString addr1;
		(*g_Resolver)( op.m_Addr1, addr1, !inDestructor, false );
		sprintf( buffer, "    %s %s [x %s]\n\n", opData[op.m_Operation].name, addr1.c_str(),
			buffer_aux );
		diagLoggedPrintf( "%s", buffer );

		currIndex = nextIndex;
	};
#endif
}

// fwDebugOwnerHistoryOp ////////////////////////////////////////////////////////////////////////

fwDebugOwnerHistoryOp::fwDebugOwnerHistoryOp() : m_Operation( Invalid ), m_Addr1( NULL ), m_Addr2( NULL ), m_PC( 0 )
{
}

int fwDebugOwnerHistoryOp::ExpandFrom( const fwDebugOwnerHistoryMovie &mov, int globalIndex, bool /*noCombo*/ )
{
	m_Operation = Invalid;
	// TODO: Take the compressed op and expand it into the relevant
	fwDebugOwnerHistoryCompressedOp ourOp = mov.GetOp(globalIndex);
	m_Count = ourOp.m_Component.m_Count;
	fwDebugOwnerHistoryCompressedOp nextOp;
	int nextIndex;

	if ( ourOp.m_Component.m_Next )
	{
		nextOp = mov.GetOp(globalIndex + ourOp.m_Component.m_Next );
		nextIndex = globalIndex + ourOp.m_Component.m_Next;
	}
	else
	{
		nextIndex = -1;
	}

	m_PC = static_cast<tPC>(mov[globalIndex - ourOp.m_Component.m_PC]);
	m_Addr1 = reinterpret_cast<tPtrPtr>(mov[globalIndex - ourOp.m_Component.m_Addr]);
	// Differ between alias and transfer
	m_Addr2 = NULL;
	m_Operation = static_cast<fwDebugOwnerHistoryOp::eOperation>(ourOp.m_Component.m_Op);
	return nextIndex;
}

} // namespace rage