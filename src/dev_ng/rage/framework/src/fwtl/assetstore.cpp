#include "fwtl/assetstore.h"

#include "atl/map.h"

namespace rage {

#if ENABLE_STORETRACKING
#if __WIN32
#pragma warning(disable: 4073)
//#pragma init_seg(lib)
#endif

    atArray<fwAssetStoreBase*> g_storesTrackingArray PPU_ONLY(__attribute__((init_priority(101))));

#endif // ENABLE_STORETRACKING

#if !__FINAL
	static atMap<strIndex, bool> s_DirtyAssetMap;
	char fwAssetStoreBase::sm_ProgressText[256];
#endif // !__FINAL

#if USE_PAGED_POOLS_FOR_STREAMING
void *fwAssetStoreBase::sm_AssetStorePages[STREAMING_INFO_MAX_PAGE_COUNT];
#endif // USE_PAGED_POOLS_FOR_STREAMING


int AssetStoreSymbolToStopLinkerError4221;

#if !__FINAL

int g_TrackRefCountKindBitmask;

PARAM(trackrefcount, "Param for tracking dependency reference count problems for specific assets");
PARAM(trackrefcountkind, "Ref count kind to track (0-3), used with -trackrefcount");



void fwAssetStoreBase::InitClass()
{
	g_TrackRefCountKindBitmask = -1;

	int singleRefCountKind;

	if (PARAM_trackrefcountkind.Get(singleRefCountKind))
	{
		g_TrackRefCountKindBitmask = 1 << singleRefCountKind;
	}
}

void fwAssetStoreBase::MarkDirty(strLocalIndex index)
{
	s_DirtyAssetMap[GetStreamingIndex(index)] = true;
}

bool fwAssetStoreBase::IsDirty(strLocalIndex index) const
{
	if (s_DirtyAssetMap.Access(GetStreamingIndex(index)))
	{
		return true;
	}

	return false;
}

void fwAssetStoreBase::ClearDirtyFlag(strLocalIndex index)
{
	s_DirtyAssetMap.Delete(GetStreamingIndex(index));
}


#endif // !__FINAL


} // namespace rage