
#include "fwtl/DefragPool.h"
#include "system/memory.h"

#if __PPU && !__TOOL
#include "system/memmanager.h"
#include "system/tinyheap.h"
#endif

namespace rage
{

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	fwDescPool
// PURPOSE:		ctor
//////////////////////////////////////////////////////////////////////////
fwDescPool::fwDescPool(const u32 nMaxEntries, const MemoryBucketIds membucketId) : m_nMaxEntries(nMaxEntries)
{
	sysMemUseMemoryBucket membucket( membucketId );

#if __PPU && !__TOOL
	m_pStorage = (fwEntityDesc*) sysMemManager::GetInstance().GetFlexAllocator()->RAGE_LOG_ALLOCATE(m_nMaxEntries * sizeof(fwEntityDesc), 16);
#else
	m_pStorage = (fwEntityDesc*) rage_new char[(m_nMaxEntries) * sizeof(fwEntityDesc)];
#endif
}

fwDescPool::fwDescPool(const u32 nMaxEntries, const u32 nAlignment, const MemoryBucketIds membucketId) : m_nMaxEntries(nMaxEntries)
{
	sysMemUseMemoryBucket membucket( membucketId );

#if __PPU && !__TOOL
	m_pStorage = (fwEntityDesc*) sysMemManager::GetInstance().GetFlexAllocator()->RAGE_LOG_ALLOCATE(m_nMaxEntries * sizeof(fwEntityDesc), nAlignment);
#else
	m_pStorage = (fwEntityDesc*) rage_aligned_new(nAlignment) char[(m_nMaxEntries) * sizeof(fwEntityDesc)];
#endif
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	~fwDescPool
// PURPOSE:		dtor
//////////////////////////////////////////////////////////////////////////
fwDescPool::~fwDescPool()
{
	DeleteAllocHandles();

#if __PPU && !__TOOL
	sysMemManager::GetInstance().GetFlexAllocator()->Free(m_pStorage);
#else
	delete[] m_pStorage;
#endif
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Reset
// PURPOSE:		Resets pool to empty state (NB entries are not zeroed!)
//////////////////////////////////////////////////////////////////////////
void fwDescPool::Reset()
{
	// clean up gap  list
	m_asGapInfo.clear();
	DefragPool_SGapInfo sDefaultGap = { 0, m_nMaxEntries };
	m_asGapInfo.PushAndGrow(sDefaultGap);

	// clean up alloc list
	DeleteAllocHandles();
	m_AllocList.Flush();

#if DEFRAGPOOL_SANITYCHECK
	m_nNumAllocs = 0;
#endif //DEFRAGPOOL_SANITYCHECK
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	New
// PURPOSE:		Allocates a contiguous block of entries, returns handle
//////////////////////////////////////////////////////////////////////////
fwDescPoolAlloc* fwDescPool::New(const u32 nNumEntries)
{
	fwDescPoolAlloc* pRetVal = NULL;
	u32 nGapArrayIndex = 0;
	bool bSuccess = FindGap(nNumEntries, &nGapArrayIndex);

	if ( !Verifyf(bSuccess, "fwDescPool allocation of %d entries failed", nNumEntries) )
	{
		// alloc failed either due to lack of space or due to fragmentation. if fragmentation,
		// do some extra defragging work to make space
		u32 entriesAvailable = 0;
		for (int i=0; i<m_asGapInfo.size();  i++) { entriesAvailable += m_asGapInfo[i].nLength; }
		if (entriesAvailable >= nNumEntries)
		{
			DefragFull();	// could just do a minimal number of calls to DefragPartialNew
			bSuccess = FindGap(nNumEntries, &nGapArrayIndex);
		}
	}

	if (bSuccess)
	{
		// found a gap, so create handle
		DefragPool_SGapInfo * psGap = &m_asGapInfo[nGapArrayIndex];
		fwEntityDesc* pStorage = m_pStorage + psGap->nIndex;
		pRetVal = rage_aligned_new(16) fwDescPoolAlloc(nNumEntries, pStorage, psGap->nIndex);
		pRetVal->m_pListNode = m_AllocList.Add(pRetVal);

		// remove gap and create new one for any remainder
		u32 nDelta = psGap->nLength - nNumEntries;
		RemoveGap(nGapArrayIndex);
		if (nDelta)
		{
			AddGap(pRetVal->m_nStorageIndex + nNumEntries, nDelta);
		}
#if DEFRAGPOOL_SANITYCHECK
		m_nNumAllocs++;
#endif //DEFRAGPOOL_SANITYCHECK
		return pRetVal;
	}

	return pRetVal;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Delete
// PURPOSE:		Deallocates all entries specified by handle.
//				NB once called, pAlloc is invalid!
//////////////////////////////////////////////////////////////////////////
void fwDescPool::Delete(fwDescPoolAlloc* const pAlloc)
{
	if (pAlloc)
	{
		u32 nGapStorageIndex = pAlloc->m_nStorageIndex;
		u32 nGapLength = pAlloc->m_nNumEntries;

		// delete allocated entry and associated handle
		pAlloc->m_pListNode = NULL;
		pAlloc->m_nNumEntries = 0;

		m_AllocList.Remove(pAlloc);

		AddGap(nGapStorageIndex, nGapLength);
		delete(pAlloc);
#if DEFRAGPOOL_SANITYCHECK
		m_nNumAllocs--;
#endif //DEFRAGPOOL_SANITYCHECK
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Resize
// PURPOSE:		Attempts to resize allocation to specified size.
//				NB resizing to 0 is permitted but invalidates pAlloc.
//////////////////////////////////////////////////////////////////////////
DefragPool_eResizeResult fwDescPool::Resize(fwDescPoolAlloc* const pAlloc, const u32 nNewSize)
{
	// trivial case - no change required
	if (nNewSize == pAlloc->m_nNumEntries) { return DEFRAGPOOL_RESIZERESULT_NOCHANGES; }

	// another trivial case - resize to zero
	if (nNewSize == 0) { Delete(pAlloc); return DEFRAGPOOL_RESIZERESULT_DELETED; }

	// if shrinking, simply adjust the entry and create a new gap
	if (nNewSize < pAlloc->m_nNumEntries)
	{
		u32 nDelta = pAlloc->m_nNumEntries - nNewSize;
		u32 nGapStartIndex = pAlloc->m_nStorageIndex + nNewSize;
		pAlloc->m_nNumEntries = nNewSize;
		AddGap(nGapStartIndex, nDelta);
		return DEFRAGPOOL_RESIZERESULT_SUCCESS;
	}
	// if growing, check if there is room to grow, otherwise reallocate
	else if (nNewSize > pAlloc->m_nNumEntries)
	{
		u32 nDelta = nNewSize - pAlloc->m_nNumEntries;
		u32 nGapIndex = 0;

		// is the entry immediately after (the current allocation) available?
		if ((pAlloc->m_nStorageIndex+pAlloc->m_nNumEntries < m_nMaxEntries) && IndexIsInGap(pAlloc->m_nStorageIndex+pAlloc->m_nNumEntries, &nGapIndex))
		{
			DefragPool_SGapInfo * psGap = &m_asGapInfo[nGapIndex];

			// can the gap accommodate the growth?
			if (psGap->nLength >= nDelta && psGap->nIndex == pAlloc->m_nStorageIndex+pAlloc->m_nNumEntries)
			{
				bool bNewGapRequired = nNewSize < (pAlloc->m_nNumEntries + psGap->nLength);
				u32 nOldGapLength = psGap->nLength;

				RemoveGap(nGapIndex);
				pAlloc->m_nNumEntries = nNewSize;

				if (bNewGapRequired)
				{
					AddGap(pAlloc->m_nStorageIndex+pAlloc->m_nNumEntries, nOldGapLength-nDelta);
				}
				return DEFRAGPOOL_RESIZERESULT_SUCCESS;
			}
		}

		// if we couldn't grow the allocation in its existing location, we attempt a reallocation
		if (FindGap(nNewSize, &nGapIndex))
		{
			DefragPool_SGapInfo * psGap = &m_asGapInfo[nGapIndex];

			u32 nOldDataIndex = pAlloc->m_nStorageIndex;
			u32 nOldDataLength = pAlloc->m_nNumEntries;
			u32 nOldGapIndex = psGap->nIndex;
			u32 nOldGapLength = psGap->nLength;

			// copy over the data to new location
			memcpy(m_pStorage+nOldGapIndex, pAlloc->m_pStorage, pAlloc->m_nNumEntries * sizeof(fwEntityDesc));			

			// update the handle pointer, indices etc
			pAlloc->m_nStorageIndex = nOldGapIndex;
			pAlloc->m_nNumEntries = nNewSize;
			pAlloc->m_pStorage = m_pStorage + nOldGapIndex;

			// remove the gap at the new location
			RemoveGap(nGapIndex);

			// create gap for any left over space
			if (nOldGapLength-nNewSize)
			{
				AddGap(pAlloc->m_nStorageIndex+nNewSize, nOldGapLength-nNewSize);
			}
			// create a gap where the data used to be
			AddGap(nOldDataIndex, nOldDataLength);	

			return DEFRAGPOOL_RESIZERESULT_SUCCESS;
		}
	}
	return DEFRAGPOOL_RESIZERESULT_FAIL;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	Grow
// PURPOSE:		Grows allocation by a number of entries
//////////////////////////////////////////////////////////////////////////
DefragPool_eResizeResult fwDescPool::Grow(fwDescPoolAlloc* const pAlloc, const u32 nAmount)
{
	DefragPool_eResizeResult result = Resize(pAlloc, pAlloc->m_nNumEntries+nAmount);
	if (result == DEFRAGPOOL_RESIZERESULT_FAIL)
	{
		// pool is either full or heavily fragmented- usually fatal. try doing a full defrag to allow for recovery
		DefragFull();

		// and try again
		result = Resize(pAlloc, pAlloc->m_nNumEntries+nAmount);
	}
	return result;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveRange
// PURPOSE:		Removes a range of elements from within an existing alloc.
//				NB removing all elements is permitted, but invalidates pAlloc
//////////////////////////////////////////////////////////////////////////
DefragPool_eResizeResult fwDescPool::RemoveRange(fwDescPoolAlloc* const pAlloc, const u32 nStartIndex, const u32 nNumEntries = 1)
{
#if DEFRAGPOOL_SANITYCHECK
	Assertf((nStartIndex+nNumEntries)<=pAlloc->m_nNumEntries, "Attempting to remove range beyond bounds of existing alloc");
#endif

	// trivial case - if range runs to end of alloc, just shrink alloc
	if ((nStartIndex+nNumEntries)==pAlloc->m_nNumEntries)
	{
		return Resize(pAlloc, nStartIndex);
	}
	// otherwise just shift down the rest of the data, and shrink alloc
	else
	{
		u32 nNumEntriesToShift = pAlloc->m_nNumEntries - (nStartIndex + nNumEntries);
		memmove(pAlloc->m_pStorage+nStartIndex, pAlloc->m_pStorage+nStartIndex+nNumEntries, nNumEntriesToShift * sizeof(fwEntityDesc));
		return Resize(pAlloc, pAlloc->m_nNumEntries-nNumEntries);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DefragPartial
// PURPOSE:		Removes a single gap and shifts down subsequent entries.
//				Repeated calls will yield a fully defragged pool.
//////////////////////////////////////////////////////////////////////////
void fwDescPool::DefragPartial()
{
#if DEFRAGPOOL_SANITYCHECK
	Assertf(VerifyOrderedGaps(), "Gap list is not correctly ordered");
#endif //DEFRAGPOOL_SANITYCHECK

	/*
	 * TODO
	 *
	 * Note that this is a very simple and naive defragger.
	 * If a pool is large and highly populated, the defrag
	 * call involves copying large amounts of memory around.
	 * In the future we will probably prefer to break the pool
	 * up into regions which can be defragged separately. So in
	 * frame 1 we might do a partial defrag of region 1, in frame 2
	 * we might do a partial defrag region 2 etc.
	 */
	fwPtrNodeSingleLink * pNode = m_AllocList.GetHeadPtr();

	if (m_asGapInfo.size() && pNode)
	{
		// take the first gap and remove it
		DefragPool_SGapInfo * psGap = &m_asGapInfo[0];
		u32 nGapStorageIndex = psGap->nIndex;
		u32 nGapLength = psGap->nLength;
		// if gap runs to end of storage, no further defragging to be done
		if ((nGapStorageIndex + nGapLength) == m_nMaxEntries)
		{
			return;
		}

		RemoveGap(0);

		// shift down all subsequent entries
		u32 nNumEntriesToShift = m_nMaxEntries - (nGapStorageIndex + nGapLength);
		memmove(m_pStorage+nGapStorageIndex, m_pStorage+nGapStorageIndex+nGapLength, nNumEntriesToShift * sizeof(fwEntityDesc));

		// fix up alloc and gap ptrs
		fwDescPoolAlloc* pHandle;
		while (pNode != NULL)
		{
			pHandle = (fwDescPoolAlloc*) pNode->GetPtr();
			if (pHandle && pHandle->m_nStorageIndex > nGapStorageIndex)
			{
				pHandle->m_nStorageIndex -= nGapLength;
				pHandle->m_pStorage -= nGapLength;
			}

			pNode = (fwPtrNodeSingleLink*) pNode->GetNextPtr();
		}
		u32 nNumGaps = m_asGapInfo.size();

		for (u32 i=0; i<nNumGaps; i++)
		{
			if (m_asGapInfo[i].nIndex > nGapStorageIndex)
			{
				m_asGapInfo[i].nIndex -= nGapLength;
			}
		}

		// create new gap at the end
		AddGap(m_nMaxEntries - nGapLength, nGapLength);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DebugPartialNew
// PURPOSE:		Removes a single gap and shifts down one subsequent alloc
//				Repeated calls will yield a fully defragged pool.
//////////////////////////////////////////////////////////////////////////
void fwDescPool::DefragPartialNew()
{
#if DEFRAGPOOL_SANITYCHECK
	Assertf(VerifyOrderedGaps(), "Gap list is not correctly ordered");
#endif //DEFRAGPOOL_SANITYCHECK

	fwPtrNodeSingleLink * pNode = m_AllocList.GetHeadPtr();

	if (m_asGapInfo.size() && pNode)
	{
		// take the first gap and remove it
		DefragPool_SGapInfo * psGap = &m_asGapInfo[0];
		u32 nGapStorageIndex = psGap->nIndex;
		u32 nGapLength = psGap->nLength;
		// if gap runs to end of storage, no further defrag to be done
		if ((nGapStorageIndex + nGapLength) == m_nMaxEntries)
		{
			return;
		}

		// search the alloc list for the alloc which starts at the end of the gap
		fwDescPoolAlloc* pAllocToShiftDown = NULL;
		u32 nTargetAllocStorageIndex = nGapStorageIndex + nGapLength;
		while (pNode && !pAllocToShiftDown)
		{
			fwDescPoolAlloc* pHandle = (fwDescPoolAlloc*) pNode->GetPtr();
			if (pHandle && pHandle->m_nStorageIndex==nTargetAllocStorageIndex)
			{
				pAllocToShiftDown = pHandle;
			}
			pNode = (fwPtrNodeSingleLink*) pNode->GetNextPtr();
		}

#if DEFRAGPOOL_SANITYCHECK
		Assert(pAllocToShiftDown);
#endif

		// remove gap, shift down alloc and update
		if (pAllocToShiftDown)
		{
			RemoveGap(0);

			// shift down one pAllocToShiftDown
			u32 nAllocStorageIndex = pAllocToShiftDown->GetStorageIndex();
			u32 nAllocLength = pAllocToShiftDown->GetNumEntries();

#if DEFRAGPOOL_SANITYCHECK
			Assert(nAllocStorageIndex == nGapStorageIndex+nGapLength);
#endif

			memmove(m_pStorage+nGapStorageIndex, m_pStorage+nAllocStorageIndex, nAllocLength*sizeof(fwEntityDesc));
			pAllocToShiftDown->m_nStorageIndex -= nGapLength;
			pAllocToShiftDown->m_pStorage -= nGapLength;

			// create new gap from end of alloc's new position
			AddGap(nGapStorageIndex+nAllocLength, nGapLength);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DefragFull
// PURPOSE:		Fully defragments pool.
//////////////////////////////////////////////////////////////////////////
void fwDescPool::DefragFull()
{
	u32 nNumGaps = 0;

	do 
	{
		nNumGaps = m_asGapInfo.size();

		// check if fully defragmented already
		switch (nNumGaps)
		{
		case 0:
			return;
		case 1:
			{
				DefragPool_SGapInfo * psGap = &m_asGapInfo[0];
				if ((psGap->nIndex + psGap->nLength) == m_nMaxEntries)
				{
					return;
				}
			}
			break;
		default:
			DefragPartial();
			break;
		}

	} while (nNumGaps > 1);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	FindGap
// PURPOSE:		Searches for a gap of specified length in the pool.
//				If found, write index into *pnGapArrayIndex, return true.
//////////////////////////////////////////////////////////////////////////
bool fwDescPool::FindGap(const u32 nLength, u32 * const pnGapArrayIndex) const
{
	for (int i=0; i<m_asGapInfo.size(); i++)
	{
		const DefragPool_SGapInfo * psGap = &m_asGapInfo[i];

		if (psGap->nLength >= nLength)
		{
			*pnGapArrayIndex = i;
			return true;
		}
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	RemoveGap
// PURPOSE:		Removes gap entry from gap list, at specified index.
//////////////////////////////////////////////////////////////////////////
void  fwDescPool::RemoveGap(const u32 nGapArrayIndex)
{
	m_asGapInfo.Delete(nGapArrayIndex);
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	AddGap
// PURPOSE:		Adds a gap entry for the specified storage index and length.
//////////////////////////////////////////////////////////////////////////
void fwDescPool::AddGap(const u32 nGapStorageIndex, const u32 nLength)
{
	bool bMergedDown = false;
	bool bMergedUp = false;
	u32 nDownGapArrayIndex = 0;
	u32 nUpGapIndex = 0;

#if DEFRAGPOOL_SANITYCHECK
	Assertf(!OverlapsWithAllocated(nGapStorageIndex, nLength), "Creating gap where entry already exists");
	//Assertf(!OverlapsWithGap(nGapStorageIndex, nLength), "Creating gap where gap already exists");
#endif //DEFRAGPOOL_SANITYCHECK

	// check if newly proposed gap can be merged into an existing neighbour gap below
	if (nGapStorageIndex)
	{
		if (IndexIsInGap(nGapStorageIndex-1, &nDownGapArrayIndex))
		{
			m_asGapInfo[nDownGapArrayIndex].nLength += nLength;
			bMergedDown = true;
		}
	}
	// check if newly proposed gap can be merged into an existing neighbour gap above
	if ((nGapStorageIndex+nLength) < m_nMaxEntries)
	{
		if (IndexIsInGap(nGapStorageIndex+nLength, &nUpGapIndex))
		{
			if (bMergedDown)
			{
				m_asGapInfo[nDownGapArrayIndex].nLength += m_asGapInfo[nUpGapIndex].nLength;
				RemoveGap(nUpGapIndex);
			}
			else
			{
				m_asGapInfo[nUpGapIndex].nIndex = nGapStorageIndex;
				m_asGapInfo[nUpGapIndex].nLength += nLength;
			}

			bMergedUp = true;
		}
	}

	// not merged, so insert a gap into the (ordered) atArray
	if (!bMergedDown && !bMergedUp)
	{
		DefragPool_SGapInfo sNewGap = { nGapStorageIndex, nLength };

		// just push the new entry on to the end of there are no other gaps
		if (m_asGapInfo.size() == 0)
		{	
			m_asGapInfo.PushAndGrow(sNewGap);
		}
		// otherwise figure out where to insert it
		else
		{
			for (int i=0;  i<m_asGapInfo.size(); i++)
			{
				DefragPool_SGapInfo * psGap = &m_asGapInfo[i];

				if (psGap->nIndex > nGapStorageIndex)
				{
					m_asGapInfo.insert(m_asGapInfo.begin()+i, sNewGap);
					return;
				}
			}
			m_asGapInfo.PushAndGrow(sNewGap);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	IndexIsInGap
// PURPOSE:		Checks if specified storage index occurs within a gap.
//				If true, write index of gap into *pnGapArrayIndex.
//////////////////////////////////////////////////////////////////////////
bool fwDescPool::IndexIsInGap(const u32 nIndex, u32 * const pnGapArrayIndex) const
{
	for (int i=0; i<m_asGapInfo.size(); i++)
	{
		const DefragPool_SGapInfo * psGap = &m_asGapInfo[i];
		if ((nIndex >= psGap->nIndex) && (nIndex < psGap->nIndex + psGap->nLength))
		{
			*pnGapArrayIndex = i;
			return true;
		}
		// since the gap info array is ordered, we can quit out early
		else if (psGap->nIndex > nIndex)
		{
			return false;
		}
	}
	return false;
}

#if DEFRAGPOOL_SANITYCHECK

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	OverlapsWithAllocated
// PURPOSE:		Checks if specified storage range overlaps with an allocation
//////////////////////////////////////////////////////////////////////////
bool fwDescPool::OverlapsWithAllocated(const u32 nIndex, const u32 nLength)
{
	// go through linked list of allocs and check for overlap with specified range
	fwPtrNodeSingleLink * pNode = m_AllocList.GetHeadPtr();
	fwDescPoolAlloc* pHandle;
	while (pNode != NULL)
	{
		pHandle = (fwDescPoolAlloc*) pNode->GetPtr();
		if (pHandle)
		{
			// check for overlap between the current allocated handle and the range specified
			if (	WithinRange(pHandle->m_nStorageIndex, nIndex, nIndex+nLength-1)
				||	WithinRange(pHandle->m_nStorageIndex+pHandle->m_nNumEntries-1, nIndex, nIndex+nLength-1)
				||	WithinRange(nIndex, pHandle->m_nStorageIndex, pHandle->m_nStorageIndex+pHandle->m_nNumEntries-1)
				||	WithinRange(nIndex+nLength-1, pHandle->m_nStorageIndex, pHandle->m_nStorageIndex+pHandle->m_nNumEntries-1) )
			{
				return true;
			}
		}

		pNode = (fwPtrNodeSingleLink*) pNode->GetNextPtr();
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	OverlapsWithGap
// PURPOSE:		Checks if specified storage range overlaps with a gap
//////////////////////////////////////////////////////////////////////////
bool fwDescPool::OverlapsWithGap(const u32 nIndex, const u32 nLength)
{
	for (int i=0; i<m_asGapInfo.size(); i++)
	{
		const DefragPool_SGapInfo * psGap = &m_asGapInfo[i];

		if (	WithinRange(psGap->nIndex, nIndex, nIndex+nLength-1)
			||	WithinRange(psGap->nIndex+psGap->nLength-1, nIndex, nIndex+nLength-1)
			||	WithinRange(nIndex, psGap->nIndex, psGap->nIndex+psGap->nLength-1)
			||	WithinRange(nIndex+nLength-1, psGap->nIndex, psGap->nIndex+psGap->nLength-1) )
		{
			return true;
		}
	}

	return false;

}

#endif //DEFRAGPOOL_SANITYCHECK

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DeleteAllocHandles
// PURPOSE:		Deletes all allocated handles in the linked list of allocs
//////////////////////////////////////////////////////////////////////////
void fwDescPool::DeleteAllocHandles()
{
	// go through linked list and clean up allocated handles
	fwPtrNodeSingleLink * pNode = m_AllocList.GetHeadPtr();
	fwDescPoolAlloc* pHandle;
	while (pNode != NULL)
	{
		pHandle = (fwDescPoolAlloc*) pNode->GetPtr();
		if (pHandle)
		{
			delete(pHandle);
		}

		pNode = (fwPtrNodeSingleLink*) pNode->GetNextPtr();
	}
}

#if DEFRAGPOOL_DEBUG
//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DebugPrintGaps
// PURPOSE:		Debug use, prints out all current gaps
//////////////////////////////////////////////////////////////////////////
void fwDescPool::DebugPrintGaps()
{
	Printf("\n**** Gaps ****\n");
	for (int i=0; i<m_asGapInfo.size();  i++)
	{
		Printf("Gap %d index %d length %d\n", i, m_asGapInfo[i].nIndex, m_asGapInfo[i].nLength);
	}
}

//////////////////////////////////////////////////////////////////////////
// FUNCTION:	DebugPrintAllocs
// PURPOSE:		Debug use, prints out all allocations
//////////////////////////////////////////////////////////////////////////
void fwDescPool::DebugPrintAllocs()
{
	Printf("\n**** Allocs ****\n");

	// go through linked list and clean up allocated handles
	fwPtrNodeSingleLink * pNode = m_AllocList.GetHeadPtr();
	fwDescPoolAlloc* pHandle;
	while (pNode != NULL)
	{
		pHandle = (fwDescPoolAlloc*) pNode->GetPtr();
		if (pHandle)
		{
			Printf("Alloc index %d length %d\n", pHandle->m_nStorageIndex, pHandle->m_nNumEntries);
		}

		pNode = (fwPtrNodeSingleLink*) pNode->GetNextPtr();
	}
}
#endif

#if DEFRAGPOOL_SANITYCHECK
//////////////////////////////////////////////////////////////////////////
// FUNCTION:	VerifyOrderedGaps
// PURPOSE:		Debug use, verifies that the list of gaps is still ordered
//////////////////////////////////////////////////////////////////////////
bool fwDescPool::VerifyOrderedGaps()
{
	if (m_asGapInfo.size() <= 1)
	{
		return true;
	}

	for (int i=1; i<m_asGapInfo.size(); i++)
	{
		DefragPool_SGapInfo * psGap1 = &m_asGapInfo[i-1];
		DefragPool_SGapInfo * psGap2 = &m_asGapInfo[i];
		if (psGap1->nIndex >= psGap2->nIndex)
		{
			return false;
		}
	}
	return true;
}

#endif //DEFRAGPOOL_SANITYCHECK





}	//namespace rage