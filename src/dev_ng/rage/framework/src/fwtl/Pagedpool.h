//
// fwtl/pagedpool.h
//
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
//


#ifndef FWTL_PAGEDPOOL_H_
#define FWTL_PAGEDPOOL_H_

// C headers
#include <stdio.h>
#include <string.h>

// rage headers:
#include "fwtl/pool.h"
#include "paging/rscbuilder.h"
#include "streaming/streamingallocator.h"
#include "streaming/streamingengine.h"
#include "system/memory.h"


// This class is similar to atl/pool.h, of course, but it allows a wider variety of flags to be associated
// with the pool entries, and also allows for iterating all entries of the pool.
namespace rage {


//
// Framework layer for atPagedPool
//

struct fwDefaultRscMemPagedPoolAllocatorBase
{
	static void *Allocate(size_t size, u32 alignment, int userData, int memBucket) {

		MEM_USE_USERDATA(userData);
		USE_MEMBUCKET(memBucket);		// SHOULD be streaming, but that's not allocated in the streaming allocator

		(void) userData;

		void *data = strStreamingEngine::GetAllocator().Allocate(size, alignment, MEMTYPE_RESOURCE_VIRTUAL);
/*
		// Try one more time.
		while (!data && strStreamingEngine::GetAllocator().MakeSpaceForAllocation(size, 0, true, true))
		{
			streamDebugf1("Clean-up for paged pool because we're low on memory");
			// Since we have a fallback, let's not spam the TTY if this fails.
#if !__FINAL
			bool oldEnableStackTraces = strStreamingEngine::GetAllocator().GetEnableStackTraces();
			strStreamingEngine::GetAllocator().EnableStackTraces(false);
#endif // !__FINAL

			data = strStreamingEngine::GetAllocator().Allocate(size, alignment, MEMTYPE_RESOURCE_VIRTUAL);

#if !__FINAL
			strStreamingEngine::GetAllocator().EnableStackTraces(oldEnableStackTraces);
#endif // !__FINAL
		}
*/
		if (!data)
		{
			Errorf("Unable to allocate %dKB RscVirt for a paged pool - falling back to GameVirt", (s32) (size / 1024));

			data = rage_aligned_new(alignment) char[size];
		}

		return data;
	}

	static void Free(void *ptr, int userData) {
		// Was this a fallback allocation?
		if (sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL)->IsValidPointer(ptr)) {
			delete[] (char *) ptr;
		} else {
			// This is in RscVirt, like we would expect.
			MEM_USE_USERDATA(userData);
			strStreamingEngine::GetAllocator().Free(ptr);

			(void) userData;
		}
	}
};

template<typename _Type, bool _Iterable = false>
struct fwDefaultRscMemPagedPoolAllocator {

	static _Type *Allocate(u32 /*page*/, atPagedPoolHeader<_Type, _Iterable> &header, u32 elementCount, u32 alignment, size_t *outMemoryUsage) {
		size_t pageSize = ComputePageMemUsage(header.GetSubType(), elementCount);
		*outMemoryUsage = pageSize;
/*
		Assertf(pageSize - elementCount * sizeof(_Type) < sizeof(_Type),
			"Just letting you know that %d elements per page with a %d size is wasting %d bytes of memory per page. I would recommend "
			"an element count of %d.",
			elementCount, (int) sizeof(_Type),
			(int) (pageSize - elementCount * sizeof(_Type)),
			(int) (pageSize / sizeof(_Type)));
*/
		size_t size = sizeof(_Type) * elementCount;

		void *data = fwDefaultRscMemPagedPoolAllocatorBase::Allocate(size, alignment, MEMUSERDATA_STRINFO, MEMBUCKET_WORLD);

#if !__FINAL
		if (!data)
		{
			Errorf("Failed to allocate %u bytes for a page", (u32) (sizeof(_Type) * elementCount));

			// This will be fatal, so let's get the full low-down on what went wrong.
			g_strStreamingInterface->FullMemoryReport();
		}
#endif // !__FINAL

		return reinterpret_cast<_Type *>(data);
	}

	/** PURPOSE: Called when a page is freed.
	 *
	 *  PARAMS:
	 *   data - Pointer to the page data that had been returned by the previous call to Allocate.
	 *   header - Page header.
	 */
	static void Free(u32 /*page*/, u32 elementCount, _Type *data, atPagedPoolHeader<_Type, _Iterable> &header, size_t *outMemoryUsage) {
		size_t pageSize = ComputePageMemUsage(header.GetSubType(), elementCount);
		*outMemoryUsage = pageSize;

		fwDefaultRscMemPagedPoolAllocatorBase::Free(data, MEMUSERDATA_STRINFO);
	}

	/** PURPOSE: Compute how much memory a page costs. This is supposed to include the cost for the payload, but not for the page header.
	 *
	 * PARAMS:
	 *   subType - Subtype of the page in question.
	 *   elementCount - Number of elements in this page.
	 */
	static size_t ComputePageMemUsage(u32 /*subType*/, u32 elementCount) {
#if !__SPU
		return pgRscBuilder::ComputeLeafSize(elementCount * sizeof(_Type), false);
#else // !__SPU
		Assert(false);
		return 0;
#endif // !__SPU
	}
};

template<typename _Type, bool _Iterable = false, typename _Allocator = fwDefaultRscMemPagedPoolAllocator<_Type, _Iterable> >
class fwRscMemPagedPool : public atPagedPool<_Type, _Iterable, _Allocator>
{
public:
	fwRscMemPagedPool()
	{
	}

	// PURPOSE: Compatibility with atPool
	fwRscMemPagedPool(size_t size, u32 pageSize = ComputeGoodPageSize())
		: atPagedPool<_Type, _Iterable, _Allocator>(size, pageSize)
	{
	}

	static u32 ComputeGoodPageSize() {
		return _Allocator::ComputePageMemUsage(0, 512) / sizeof(_Type);
	}

	// PURPOSE: Compatibility with atPool
	fwRscMemPagedPool(s32 size, const char* pName, s32 redZone=0, s32 storageSize=sizeof(_Type), u32 pageSize = 512 /*ComputeGoodPageSize()*/)
		: atPagedPool<_Type, _Iterable, _Allocator>(size, pName, redZone, storageSize, pageSize)
	{
	}

};

#define FW_REGISTER_CLASS_PAGEDPOOL(_T)						\
	/* macro used to register that a class uses a pool to allocate from */	\
	typedef fwRscMemPagedPool<_T, true > Pool;											\
	static Pool* _ms_pPool;												\
	void* operator new(size_t ASSERT_ONLY(nSize) RAGE_NEW_EXTRA_ARGS_UNUSED)						\
{																	\
	Assert(_ms_pPool);												\
	Assertf(nSize <= _ms_pPool->GetStorageSize(), "%u,%u", (u32) nSize, (u32)_ms_pPool->GetStorageSize());	\
	return (void*) _ms_pPool->New();								\
}																	\
	void* operator new(size_t ASSERT_ONLY(nSize), s32 PoolIndex)		\
{																	\
	Assert(_ms_pPool);												\
	Assertf(nSize <= _ms_pPool->GetStorageSize(), "%u,%u", (u32)nSize, (u32)_ms_pPool->GetStorageSize());	\
	return (void*) _ms_pPool->New(PoolIndex);						\
}																	\
	void operator delete(void *pVoid)									\
{																	\
	Assert(_ms_pPool);												\
	_ms_pPool->Delete((_T*)pVoid);									\
}																	\
	static void InitPool(const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);			\
	static void InitPool(int size, const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);	\
	static void ShutdownPool() {delete _ms_pPool; _ms_pPool = NULL;}	\
	static Pool* GetPool() {return _ms_pPool;}					\
	/* end of AT_REGISTER_CLASS_POOL */

#if __FINAL
	#define FW_INSTANTIATE_CLASS_PAGEDPOOL(_T, num, pageSize, name)							\
		/* macro used to instantiate class pools */							\
		_T::Pool* _T::_ms_pPool = NULL;			\
		void _T::InitPool(const MemoryBucketIds membucketId, int /*redZone*/) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(name, num), pageSize); }					\
		void _T::InitPool(int size, const MemoryBucketIds membucketId, int /*redZone*/) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(name, size), pageSize); }		\
		/* end of FW_INSTANTIATE_CLASS_POOL */
#else
	#define FW_INSTANTIATE_CLASS_PAGEDPOOL(_T, num, pageSize, name)							\
		/* macro used to instantiate class pools */							\
		_T::Pool* _T::_ms_pPool = NULL;			\
		void _T::InitPool(const MemoryBucketIds membucketId, int /*redZone*/) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(name, num), pageSize); RegisterInPoolTracker(_ms_pPool, name.GetCStr()); }					\
		void _T::InitPool(int size, const MemoryBucketIds membucketId, int /*redZone*/) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(name, size), pageSize); RegisterInPoolTracker(_ms_pPool, name.GetCStr()); }		\
		/* end of FW_INSTANTIATE_CLASS_POOL */
#endif
}		// namespace rage

#endif // FWTL_PAGEDPOOL_H_
