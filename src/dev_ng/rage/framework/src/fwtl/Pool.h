//
// fwtl/pool.h
//
// Copyright (C) 1999-2012 Rockstar North.  All Rights Reserved. 
//


#ifndef FWTL_POOL_H_
#define FWTL_POOL_H_

// C headers
#include <stdio.h>
#include <string.h>

// rage headers:
#include "system/new.h"
#include "atl/array.h"
#include "atl/pagedpool.h"
#include "atl/pool.h"
#include "fwsys/config.h"	// only needed by users of the FW_INSTANTIATE_..._POOL macros, could let the users include instead. 
#include "fwutil/xmacro.h"
#include "system/memory.h"

// game headers
// #include "debug\debug.h"

// This class is similar to atl/pool.h, of course, but it allows a wider variety of flags to be associated
// with the pool entries, and also allows for iterating all entries of the pool.
namespace rage {

#define POOL_FLAG_ISFREE 0x80
#define POOL_FLAG_REFERENCEMASK 0x7f


//
// name:		REGISTER_CLASS_POOL
// description:	This macro sets up a pool which all allocated versions of 
//				a class use. It generates a static variable which is a 
//				pointer to the pool, new and delete operators, InitPool
//				and ShutdownPool functions. The FW_ variant is twinned with
//				the macro FW_INSTANTIATE_CLASS_POOL which instantiate the
//				static pointer to the pool. FW_REGISTER_CLASS_POOL should
//				be placed inside the class definition and FW_INSTANTIATE_CLASS_POOL
//				should be placed in the related source file. There are 
//				two equivalent macros REGISTER_BASECLASS_POOL and 
//				FW_INSTANTIATE_BASECLASS_POOL which generate pools which 
//				have a different storage size to the class they generate
#define REGISTER_CLASS_POOL(_PoolType, _T)								\
	/* macro used to register that a class uses a pool to allocate from */	\
	typedef _PoolType<_T > Pool;											\
	static Pool* _ms_pPool;												\
	void* operator new(size_t ASSERT_ONLY(nSize) RAGE_NEW_EXTRA_ARGS_UNUSED)						\
	{																	\
		Assert(_ms_pPool);												\
		Assertf(nSize <= _ms_pPool->GetStorageSize(), "%u,%u", (u32) nSize, (u32)_ms_pPool->GetStorageSize());	\
		return (void*) _ms_pPool->New();								\
	}																	\
	void* operator new(size_t ASSERT_ONLY(nSize), s32 PoolIndex)		\
	{																	\
		Assert(_ms_pPool);												\
		Assertf(nSize <= _ms_pPool->GetStorageSize(), "%u,%u", (u32)nSize, (u32)_ms_pPool->GetStorageSize());	\
		return (void*) _ms_pPool->New(PoolIndex);						\
	}																	\
	void operator delete(void *pVoid)									\
	{																	\
		Assert(_ms_pPool);												\
		_ms_pPool->Delete((_T*)pVoid);									\
	}																	\
	static void InitPool(const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);			\
	static void InitPool(int size, const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);	\
	static void ShutdownPool() {delete _ms_pPool; _ms_pPool = NULL;}	\
	static Pool* GetPool() {return _ms_pPool;}					\
	/* end of REGISTER_CLASS_POOL */

#define REGISTER_LOCKING_CLASS_POOL(_PoolType, _T, _CriticalSectionType)	\
	/* macro used to register that a class uses a pool to allocate from */	\
	typedef _PoolType<_T > Pool;											\
	static Pool* _ms_pPool;												\
	void* operator new(size_t ASSERT_ONLY(nSize) RAGE_NEW_EXTRA_ARGS_UNUSED)						\
	{																	\
		_CriticalSectionType cs;										\
		Assert(_ms_pPool);												\
		Assertf(nSize <= _ms_pPool->GetStorageSize(), "%u,%u", (u32) nSize, (u32)_ms_pPool->GetStorageSize());	\
		return (void*) _ms_pPool->New();								\
	}																	\
	void* operator new(size_t ASSERT_ONLY(nSize), s32 PoolIndex)		\
	{																	\
		_CriticalSectionType cs;										\
		Assert(_ms_pPool);												\
		Assertf(nSize <= _ms_pPool->GetStorageSize(), "%u,%u", (u32)nSize, (u32)_ms_pPool->GetStorageSize());	\
		return (void*) _ms_pPool->New(PoolIndex);						\
	}																	\
	void operator delete(void *pVoid)									\
	{																	\
		_CriticalSectionType cs;										\
		Assert(_ms_pPool);												\
		_ms_pPool->Delete((_T*)pVoid);									\
	}																	\
	static void InitPool(const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);			\
	static void InitPool(int size, const MemoryBucketIds membucketId = MEMBUCKET_DEFAULT, int redZone = 0);	\
	static void ShutdownPool() {delete _ms_pPool; _ms_pPool = NULL;}	\
	static Pool* GetPool() {return _ms_pPool;}					\
	/* end of REGISTER_LOCKING_CLASS_POOL */

#define FW_REGISTER_CLASS_POOL(_T) REGISTER_CLASS_POOL(fwPool, _T)
#define AT_REGISTER_CLASS_POOL(_T) REGISTER_CLASS_POOL(atPool, _T)
#define AT_REGISTER_CLASS_ITPOOL(_T) REGISTER_CLASS_POOL(atIteratablePool, _T)
#define FW_REGISTER_LOCKING_CLASS_POOL(_T, _CriticalSectionType) REGISTER_LOCKING_CLASS_POOL(fwPool, _T, _CriticalSectionType)
#define AT_REGISTER_LOCKING_CLASS_POOL(_T, _CriticalSectionType) REGISTER_LOCKING_CLASS_POOL(atPool, _T, _CriticalSectionType)
#define AT_REGISTER_LOCKING_CLASS_ITPOOL(_T, _CriticalSectionType) REGISTER_LOCKING_CLASS_POOL(atIteratablePool, _T, _CriticalSectionType)

#if __FINAL
	#define FW_GET_HASHSTRING(hash) ""
#else
	#define FW_GET_HASHSTRING(hash) hash.GetCStr()
#endif // __FINAL

#define FW_INSTANTIATE_CLASS_POOL(_T, num, hashname)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;			\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), FW_GET_HASHSTRING(hashname), redZone); RegisterInPoolTracker(_ms_pPool, #_T); }					\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), FW_GET_HASHSTRING(hashname), redZone); RegisterInPoolTracker(_ms_pPool, #_T); }		\
	/* end of FW_INSTANTIATE_CLASS_POOL */

#define FW_INSTANTIATE_CLASS_POOL_SPILLOVER(_T, num, spillover, hashname)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;			\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), spillover, FW_GET_HASHSTRING(hashname), redZone); RegisterInPoolTracker(_ms_pPool, #_T); }					\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), spillover, FW_GET_HASHSTRING(hashname), redZone); RegisterInPoolTracker(_ms_pPool, #_T); }		\
	/* end of FW_INSTANTIATE_CLASS_POOL */

#define FW_INSTANTIATE_BASECLASS_POOL(_T, num, hashname, maxsize)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;										\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), FW_GET_HASHSTRING(hashname), redZone, maxsize); RegisterInPoolTracker(_ms_pPool, #_T); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), FW_GET_HASHSTRING(hashname), redZone, maxsize); RegisterInPoolTracker(_ms_pPool, #_T); }	\
	/* end of FW_INSTANTIATE_BASECLASS_POOL */

#define FW_INSTANTIATE_BASECLASS_POOL_SPILLOVER(_T, num, spillover, hashname, maxsize)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;										\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), spillover, FW_GET_HASHSTRING(hashname), redZone, maxsize); RegisterInPoolTracker(_ms_pPool, #_T); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), spillover, FW_GET_HASHSTRING(hashname), redZone, maxsize); RegisterInPoolTracker(_ms_pPool, #_T); }	\
	/* end of FW_INSTANTIATE_BASECLASS_POOL */

#define FW_INSTANTIATE_CLASS_POOL_LOCKED_SIZE(_T, num, hashname)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;										\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); fwConfigManager::GetInstance().LockPoolSize(hashname); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(num, FW_GET_HASHSTRING(hashname), redZone); RegisterInPoolTracker(_ms_pPool, #_T); _ms_pPool->SetSizeIsFromConfigFile(false);}					\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); fwConfigManager::GetInstance().LockPoolSize(hashname); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(size, FW_GET_HASHSTRING(hashname), redZone); RegisterInPoolTracker(_ms_pPool, #_T); _ms_pPool->SetSizeIsFromConfigFile(false);}		\
	/* end of FW_INSTANTIATE_CLASS_POOL_LOCKED_SIZE */

#define FW_INSTANTIATE_BASECLASS_POOL_LOCKED_SIZE(_T, num, hashname, maxsize)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;										\
	void _T::InitPool(const MemoryBucketIds membucketId) {sysMemUseMemoryBucket membucket( membucketId ); fwConfigManager::GetInstance().LockPoolSize(hashname); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(num, FW_GET_HASHSTRING(hashname), redZone, maxsize); RegisterInPoolTracker(_ms_pPool, #_T); _ms_pPool->SetSizeIsFromConfigFile(false); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId) {sysMemUseMemoryBucket membucket( membucketId ); fwConfigManager::GetInstance().LockPoolSize(hashname); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(size, FW_GET_HASHSTRING(hashname), redZone, maxsize); RegisterInPoolTracker(_ms_pPool, #_T); _ms_pPool->SetSizeIsFromConfigFile(false);}		\
	/* end of FW_INSTANTIATE_BASECLASS_POOL_LOCKED_SIZE */

#if COMMERCE_CONTAINER
#define FW_INSTANTIATE_CLASS_POOL_NO_FLEX(_T, num, hashname)				\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;			\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), FW_GET_HASHSTRING(hashname), redZone, sizeof(_T), false); RegisterInPoolTracker(_ms_pPool, #_T); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), FW_GET_HASHSTRING(hashname), redZone, sizeof(_T), false); RegisterInPoolTracker(_ms_pPool, #_T); }
	/* end of FW_INSTANTIATE_CLASS_POOL_NO_FLEX */

#define FW_INSTANTIATE_CLASS_POOL_NO_FLEX_SPILLOVER(_T, num, spillover, hashname)				\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;			\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), spillover, FW_GET_HASHSTRING(hashname), redZone, sizeof(_T), false); RegisterInPoolTracker(_ms_pPool, #_T); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), spillover, FW_GET_HASHSTRING(hashname), redZone, sizeof(_T), false); RegisterInPoolTracker(_ms_pPool, #_T); }
/* end of FW_INSTANTIATE_CLASS_POOL_NO_FLEX */

#define FW_INSTANTIATE_BASECLASS_POOL_NO_FLEX(_T, num, hashname, maxsize)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;										\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), FW_GET_HASHSTRING(hashname), redZone, maxsize, false); RegisterInPoolTracker(_ms_pPool, #_T); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), FW_GET_HASHSTRING(hashname), redZone, maxsize, false); RegisterInPoolTracker(_ms_pPool, #_T); }	\
	/* end of FW_INSTANTIATE_BASECLASS_POOL_NO_FLEX */

#define FW_INSTANTIATE_BASECLASS_POOL_NO_FLEX_SPILLOVER(_T, num, spillover, hashname, maxsize)							\
	/* macro used to instantiate class pools */							\
	_T::Pool* _T::_ms_pPool = NULL;										\
	void _T::InitPool(const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, num), spillover, FW_GET_HASHSTRING(hashname), redZone, maxsize, false); RegisterInPoolTracker(_ms_pPool, #_T); }				\
	void _T::InitPool(int size, const MemoryBucketIds membucketId, int redZone) {sysMemUseMemoryBucket membucket( membucketId ); Assert(!_ms_pPool); _ms_pPool = rage_new Pool(fwConfigManager::GetInstance().GetSizeOfPool(hashname, size), spillover, FW_GET_HASHSTRING(hashname), redZone, maxsize, false); RegisterInPoolTracker(_ms_pPool, #_T); }	\
	/* end of FW_INSTANTIATE_BASECLASS_POOL_NO_FLEX */
#endif /* end of FW_INSTANTIATE_CLASS_POOL_NO_FLEX */

// Helper macro to test if the pool has any slots, returning NULL if it's full before invoking constructor on null pointer.
#define rage_checked_pool_new(_T) !_T::GetPool()->GetNoOfFreeSpaces()? NULL : rage_new _T

class fwBasePool
{
public:
	fwBasePool(const char* pName, s32 redZone, s32 storageSize);
	fwBasePool(s32 nSize, const char* pName, s32 redZone, s32 storageSize, bool useFlex = true);
	fwBasePool(s32 nSize, u8* pPool, u8* pFlags, const char* pName, s32 redZone, s32 storageSize);
	fwBasePool(s32 nSize, float spillover, const char* pName, s32 redZone, s32 storageSize, bool useFlex = true);

	BANK_ONLY(virtual) ~fwBasePool();		// Virtual destructor only because of the virtual pool full callback
	
	size_t GetStorageSize() const { return m_nStorageSize; }
	s32 GetSize() const { return m_nSize; }
	s32 GetNoOfUsedSpaces() const { return m_numSlotsUsed; }
	s32 GetNoOfFreeSpaces() const { return m_nSize - m_numSlotsUsed; }

	s32 GetPoolSize() const
	{ 
#if RAGE_POOL_SPILLOVER
		return m_SpilloverPool ? m_PoolSize : m_nSize;
#else
		return m_nSize;
#endif
	}

	void Init(s32 nSize, void* pPool, u8* pFlags);
	void InitAndAllocate(s32 nSize, bool useFlex = true);
	void InitAndAllocate(s32 size, float spillover, bool useFlex = false);
	void Reset();

	void* New();
	void* New(s32 index);
	void Delete(void* pT);
	
#if RAGE_POOL_SPILLOVER	
	s32 GetSpilloverSize() const { return m_SpilloverSize; }
#endif

#if RAGE_POOL_NODE_TRACKING
	void Tally(void* ptr);
	void UnTally(void* ptr);
#endif
	
	// GetIndex() is used to generate the handle that can then be used to access an element
	// from the pool using GetAt(). The place where these handle are used most is in script.
	// If script has a handle to an object and this object is deleted and then a new object
	// is immediately allocated in the same slot when the script uses the handle the pool
	// will return a NULL pointer. It does this by comparing the reference value stored
	// in the handle and the reference value the pool slot contains.
	// Everytime an allocation is made it reference value is incremented so the newly allocated
	// object won�t have the same reference value and is considered a different object.
	void* GetAt(s32 index);
	
	s32 GetIndex(const void* pT) const;
	s32 GetIndexFast(const void* pT) const;
	s32 GetJustIndex(const void* pT) const;
	s32 GetJustIndexFast(const void* pT) const;
	s32 GetJustIndex_NoFreeAssert(const void* pT) const;

	bool GetIsFree(s32 index) const { return (m_aFlags[index] & POOL_FLAG_ISFREE) != 0; }
	u8 GetReference(s32 index) const { return (m_aFlags[index] & POOL_FLAG_REFERENCEMASK); }
	u8 GetFlags(s32 index) const { return (m_aFlags[index]); }

	static __forceinline size_t GetNonZeroMask(u32 x) // returns (x ? ~0 : 0) without branching
	{
		return (size_t)( ( (ptrdiff_t)(x) | -(ptrdiff_t)(x) ) >> (sizeof(size_t)*8-1) );
	}

	// Branchless versions of GetSlot() - these methods get called a lot.
	__forceinline void* GetSlot(s32 index)
	{
		// NOTE: EJ - No need for duplicate code. This is why the Cylon God invented cast operators!
		Assert(index < m_nSize);
		return const_cast<void*>(static_cast<const fwBasePool*>(this)->GetSlot(index));
	}

	__forceinline const void* GetSlot(s32 index) const
	{
		FastAssert(index >= 0);
		Assert(index < m_nSize);
		const size_t mask = ~GetNonZeroMask( m_aFlags[index] & POOL_FLAG_ISFREE );

#if ATL_POOL_SPARSE
		if (m_SparsePool)
		{
			const size_t addr = reinterpret_cast< size_t >( m_SparsePool[index] );
			const void*	const result = reinterpret_cast< const void* >( addr & mask );
			return result;
		}
#endif // ATL_POOL_SPARSE

#if RAGE_POOL_SPILLOVER
		if (IsInSpillover(index))
		{			
			/*const size_t addr = reinterpret_cast< size_t >(m_SpilloverPool[index - m_PoolSize]);
			const void*	const result = reinterpret_cast< const void* >( addr & mask );*/
			const void* result = m_SpilloverPool[index - m_PoolSize];
			return result;
		}
#endif // RAGE_POOL_SPILLOVER

		Assertf(index < GetPoolSize(), "Invalid pool index %d. The game will crash now.", index);
		const size_t addr = reinterpret_cast< size_t >( m_aStorage + index * m_nStorageSize );
		const void*	const result = reinterpret_cast< const void* >( addr & mask );
		return result;
	}

	bool IsValidPtr(void* ptr) const;

	bool IsInPool(s32 index) const
	{
		return index < GetPoolSize();
	}

#if RAGE_POOL_SPILLOVER
	bool IsValidSpilloverPtr(const void* ptr) const
	{
		return m_SpilloverPool && (m_SpilloverMap.Access(ptr) != NULL);
	}

	bool IsInPool(const void* ptr) const
	{
		return (ptr >= m_aStorage) && (ptr < (m_aStorage + (GetPoolSize() * m_nStorageSize)));
	}

	bool IsInSpillover(s32 index) const
	{
		return m_SpilloverPool && (index >= m_PoolSize) && (index < m_nSize);
	}
#endif

	// for all items in a pool
	typedef bool (*Callback)(void* item, void* data);
	void ForAll(Callback cb, void* data);

#if !__NO_OUTPUT
	void SetSizeIsFromConfigFile(bool b) { m_bSizeIsFromConfigFile = b; }
	void *LowestAddress() const { return(&m_aStorage[0]); }
	void *HighestAddress() const { return(&m_aStorage[GetPoolSize() * m_nStorageSize]); }
	const char* GetName() const {return &m_name[0];}
	s32 GetPeakSlotsUsed() const {return m_peakNumSlotsUsed;}
	void setLogged(bool logged) { m_logged = logged; }
#else
	void SetSizeIsFromConfigFile(bool UNUSED_PARAM(b)) { }
#endif

#if __BANK
	typedef void (*PoolFullCB)(void* item);
	void RegisterPoolCallback(PoolFullCB callback) { m_poolFullCB = callback; }
	virtual void PoolFullCallback();

private:
	PoolFullCB m_poolFullCB;
#endif //!__BANK

private:
	// PURPOSE:	Allocate storage space for the pool.
	void AllocStorage(bool useFlex);
	void Flush();

	void SetIsFree(s32 index, bool bIsFree) 
	{ 
		bIsFree ? (m_aFlags[index] |= POOL_FLAG_ISFREE) : (m_aFlags[index] &= ~POOL_FLAG_ISFREE);
	}
	void SetReference(s32 index, u8 nReference)
	{
		m_aFlags[index] = (m_aFlags[index] & ~POOL_FLAG_REFERENCEMASK) | ( ( (nReference & POOL_FLAG_REFERENCEMASK) > 1 ? (nReference & POOL_FLAG_REFERENCEMASK) : 1 ) );
	}

	// member variables
	// NB be careful moving these around or inserting new member variables here
	// as it can break some SPU code
	u8* m_aStorage;
	u8* m_aFlags;
	s32 m_nSize;
	s32 m_nStorageSize;
	s32 m_nFirstFreeIndex;
	s32 m_nLastFreeIndex;
	s32 m_numSlotsUsed : 30;
	s32 m_bOwnsArrays : 2;

#if RAGE_POOL_SPILLOVER
	void** m_SpilloverPool;
	s32 m_PoolSize, m_SpilloverSize;
	atMap<const void*, u32> m_SpilloverMap;	
#endif

	ASSERT_ONLY(s32 m_redZone;) // the number of objects left when the pool will assert, to early warn for pools that are depleted.	

#if !__NO_OUTPUT
	bool m_bSizeIsFromConfigFile;	// Only used for some error message display now.
	#define TEMPPLATEPOOL_MAXNAMELEN			(32)
	char m_name[TEMPPLATEPOOL_MAXNAMELEN];
	s32 m_peakNumSlotsUsed;
	bool m_logged;
#endif	

#if ATL_POOL_SPARSE
	void** m_SparsePool;
	atMap<const void*,u32> m_SparseMap;
#endif

private:
	void MakeEmpty();
	void BuildFreeList();
};

template<class T> class fwPool : public fwBasePool
{
public:
	CompileTimeAssert(sizeof(T) >= sizeof(s32));

	fwPool(const char* pName, s32 redZone=0, s32 storageSize=sizeof(T)) : fwBasePool( pName, redZone, storageSize ) {}	

#if COMMERCE_CONTAINER
	fwPool(s32 nSize, const char* pName, s32 redZone=0, s32 storageSize=sizeof(T), bool useFlex = true) : fwBasePool( nSize, pName, redZone, storageSize, useFlex) {}
	fwPool(s32 nSize, float spillover, const char* pName, s32 redZone=0, s32 storageSize=sizeof(T), bool useFlex = true) : fwBasePool( nSize, spillover, pName, redZone, storageSize, useFlex) {}
#else
	fwPool(s32 nSize, const char* pName, s32 redZone=0, s32 storageSize=sizeof(T)) : fwBasePool( nSize, pName, redZone, storageSize) {}
	fwPool(s32 nSize, float spillover, const char* pName, s32 redZone=0, s32 storageSize=sizeof(T)) : fwBasePool( nSize, spillover, pName, redZone, storageSize) {}
#endif	

	fwPool(s32 nSize, T* pPool, u8* pFlags, const char* pName, s32 redZone=0, s32 storageSize=sizeof(T)) : fwBasePool( nSize, (u8*) pPool, pFlags, pName, redZone, storageSize ) {}

	T* New() { return static_cast<T*>(fwBasePool::New()); }
	T* New(s32 index) { return static_cast<T*>(fwBasePool::New( index )); }

	T* GetAt(s32 index) { return static_cast<T*>(fwBasePool::GetAt( index )); }
	T* GetSlot(s32 index) { return static_cast<T*>(fwBasePool::GetSlot( index )); }
	const T* GetSlot(s32 index) const { return static_cast<const T*>(fwBasePool::GetSlot( index )); }

	void DeleteAll() { ForAll(DeleteAllCallback, NULL); }

#if __BANK
	virtual void PoolFullCallback() {fwBasePool::PoolFullCallback();}
#endif

private:

	static bool DeleteAllCallback(void* item, void* /*data*/) { delete (T*)item; return true; }
};

#if RAGE_POOL_TRACKING
	class fwBasePoolTracker : public PoolTracker
	{
	public:

		virtual ~fwBasePoolTracker() { /*No op*/ }

		void SetPool(const fwBasePool *pool) { m_poolPointer = pool; }
		const fwBasePool * GetPool() const { return (const fwBasePool *) m_poolPointer; }

		virtual size_t GetStorageSize() const { return GetPool()->GetStorageSize(); }
		virtual s32 GetSize() const { return GetPool()->GetSize(); }
#if RAGE_POOL_SPILLOVER
		virtual s32 GetSpilloverSize() const { return GetPool()->GetSpilloverSize(); }
#endif
		virtual s32 GetNoOfUsedSpaces() const { return GetPool()->GetNoOfUsedSpaces(); }
		virtual s32 GetPeakSlotsUsed() const { return GetPool()->GetPeakSlotsUsed(); }
		virtual s32 GetNoOfFreeSpaces() const { return GetPool()->GetNoOfFreeSpaces(); }
		virtual s32 GetActualMemoryUse() const 
		{
			return	s32(sizeof(*GetPool()) // Size of base object
				- TEMPPLATEPOOL_MAXNAMELEN * sizeof(char) // - size of name
				+ GetSize()*sizeof(u32)	// Flags
				+ GetSize()*GetPool()->GetStorageSize()); // Actual storage

		}
		virtual const char* GetName() const { return GetPool()->GetName(); }

		void SetClassName(const char *name)		{ m_ClassName = name; }
		virtual const char* GetClassName() const { return m_ClassName.c_str(); }

	private:
		ConstString m_ClassName;
	};

	template<typename _Type, bool _Iterable = false, typename _Allocator = atDefaultPagedPoolAllocator<_Type, _Iterable> >
	class atPagedPoolTracker : public PoolTracker 
	{
	public:
		typedef atPagedPool<_Type, _Iterable, _Allocator> Pool;

		virtual ~atPagedPoolTracker() {  }

		void SetPool(const Pool *pool, u32 subType) { m_poolPointer = pool; m_SubType = subType; }
		const Pool * GetPool() const { return reinterpret_cast<const Pool *>(m_poolPointer); }

		void SetName(const char *name)		{ m_Name = name; }

		virtual size_t GetStorageSize() const { return sizeof(_Type); }
	#if PAGED_POOL_TRACKING
		virtual s32 GetSize() const { return (s32)GetPool()->GetPagesUsedForSubType(m_SubType) * GetPool()->GetElementsPerPage(m_SubType); }
		virtual s32 GetPeakSlotsUsed() const { return (s32)GetPool()->GetPeakElementUsageForSubType(m_SubType); }
		virtual s32 GetActualMemoryUse() const 
		{
			return	s32(sizeof(Pool) + // Size of base object
				GetPool()->GetMemoryUsage(m_SubType));
		}
	#else // PAGED_POOL_TRACKING
		virtual s32 GetSize() const { return 0; }
		virtual s32 GetPeakSlotsUsed() const { return 0; }
		virtual s32 GetActualMemoryUse() const { return 0; }
	#endif // PAGED_POOL_TRACKING

		virtual s32 GetNoOfUsedSpaces() const { return (s32)GetPool()->GetElementsUsedForSubType(m_SubType); }
		virtual s32 GetNoOfFreeSpaces() const { return GetSize() - GetNoOfUsedSpaces(); }

		virtual const char* GetName() const { return m_Name.c_str(); }
		virtual const char* GetClassName() const { return GetName(); }

	private:
		ConstString m_Name;
		u32 m_SubType;
	};

	template<typename _Type, bool _Iterable, typename _Allocator>
	inline void RegisterInPoolTracker(atPagedPool<_Type, _Iterable, _Allocator> * poolptr, u32 subType, const char * poolName)
	{
		sysMemStartTemp();
		atPagedPoolTracker<_Type, _Iterable, _Allocator>* pTracker = rage_new atPagedPoolTracker<_Type, _Iterable, _Allocator>();
		pTracker->SetPool(poolptr, subType);
		pTracker->SetName(poolName);
		PoolTracker::Add(pTracker);
		sysMemEndTemp();
	}

	// Compatibility with fwPool - assume subtype 0.
	template<typename _Type, bool _Iterable, typename _Allocator>
	inline void RegisterInPoolTracker(atPagedPool<_Type, _Iterable, _Allocator> * poolptr, const char * poolName)
	{
		RegisterInPoolTracker(poolptr, 0, poolName);
	}

	// fwBasePool handles its own tracker registration.
	void RegisterInPoolTracker(const fwBasePool* poolptr, const char* className);
	void UnregisterInPoolTracker(const fwBasePool* poolptr);

	void AddPoolTracker(const fwBasePool* poolptr);
#else
	template<typename _Type, bool _Iterable, typename _Allocator>
	inline void RegisterInPoolTracker(atPagedPool<_Type, _Iterable, _Allocator> * /*poolptr*/, u32 /*subType*/, const char * /*poolName*/)
	{
	}
#endif

}		// namespace rage

#endif // FWTL_POOL_H_
