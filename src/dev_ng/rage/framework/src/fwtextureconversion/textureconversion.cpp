// =====================================
// textureconversion.cpp
// Copyright (c) 2014 Rockstar San Diego
// =====================================

#if (__BANK || __GAMETOOL) && !__RESOURCECOMPILER

// rage includes
#include "file/serialize.h"
#include "grcore/image.h"
#include "grcore/image_dxt.h"
#include "grcore/texture.h"
#include "grcore/texturedebugflags.h"
#include "math/float16.h"
#include "math/vecshift.h" // for BIT64
#include "parser/manager.h"
#include "string/stringutil.h"
#include "system/endian.h"
#include "system/ipc.h"
#include "system/memory.h"
#include "system/nelem.h"
#include "system/timer.h"

#include "fwtextureconversion/textureconversion.h"
#include "fwtextureconversion/textureconversion_parser.h"
#include "fwtextureconversion/textureconversionimage.h"
#include "fwutil/xmacro.h"

#if RSG_ORBIS
	#define PLATFORM_STRING "PS4"
	#define PLATFORM platform::ORBIS
#elif RSG_DURANGO
	#define PLATFORM_STRING "XB1"
	#define PLATFORM platform::DURANGO
#elif RSG_PC && __64BIT
	#define PLATFORM_STRING "X64"
	#define PLATFORM platform::WIN64PC
#elif RSG_PC && !__64BIT
	#define PLATFORM_STRING "X86"
	#define PLATFORM platform::WIN32PC
#else
	#define PLATFORM_STRING "UNKNOWN"
	#define PLATFORM platform::UNKNOWN
#endif

RAGE_DEFINE_CHANNEL(fwtexconv)
PARAM(fwtexconvlog, "fwtexconvlog");

#define fwtexconvDisplay(fmt, ...) RAGE_DISPLAYF(fwtexconv, fmt, ##__VA_ARGS__)
#define fwtexconvWarning(fmt, ...) RAGE_WARNINGF(fwtexconv, fmt, ##__VA_ARGS__)
#define fwtexconvDebug(  fmt, ...) RAGE_DEBUGF1 (fwtexconv, fmt, ##__VA_ARGS__)
#define fwtexconvError(  fmt, ...) RAGE_ERRORF  (fwtexconv, fmt, ##__VA_ARGS__)
#define fwtexconvLog(    fmt, ...) g_fwTexConvLog.Write    (fmt, ##__VA_ARGS__)

namespace rage {

// ================================================================================================

static u32 g_fwTexConvUniqueHash = 0;

class fwTextureConversionLog
{
public:
	fwTextureConversionLog() : m_file(NULL) {}

	void Write(const char* fmt, ...)
	{
		char temp[4096] = "";
		va_list args;
		va_start(args, fmt);
		vsnprintf(temp, sizeof(temp), fmt, args);
		va_end(args);

		fwtexconvDisplay("%s", temp);

		if (PARAM_fwtexconvlog.Get())
		{
			for (int i = 0; i < 10 && m_file == NULL; i++)
			{
				const utimer_t t = sysTimer::GetTicks(); // QueryPerformanceCounter
				const u32 hash = atDataHash((const char*)&t, sizeof(t), (u32)i);
				char path[RAGE_MAX_PATH] = "";
				sprintf(path, "c:/dump/texconvlog_%s_%08X.txt", PLATFORM_STRING, hash);

				FILE* temp = fopen(path, "r");

				if (temp) // file exists, try another hash .. very unlikely to happen
				{
					fclose(temp);
					continue;
				}

				m_file = fopen(path, "w");
				g_fwTexConvUniqueHash = hash;
			}

			if (m_file)
			{
				fprintf(m_file, "%s\n", temp);
				fflush (m_file);
			}
		}
	}

	FILE* m_file;
};

fwTextureConversionLog g_fwTexConvLog;

// ================================================================================================

enum eTextureFormatChannelGroup
{
	tfcg_RGB     , // R,G,B,1
	tfcg_RGB_A0  , // R,G,B,0
	tfcg_RGBA    , // R,G,B,A
	tfcg_L       , // L,L,L,1
	tfcg_L_A0    , // L,L,L,0
	tfcg_A       , // 0,0,0,A
	tfcg_A_RGB100, // 1,0,0,A
	tfcg_A_RGB111, // 1,1,1,A
	tfcg_LA      , // L,L,L,A
	tfcg_RG      , // R,G,0,1
	tfcg_RG_B1   , // R,G,1,1
	tfcg_R       , // R,0,0,1
};

class fwTextureFormatHelper // constructed from examining all pixels, including mips (ConsiderPixel)
{
public:
	fwTextureFormatHelper(const char* filename);

	__forceinline void ConsiderPixel(const Vector4& v);

	const char* m_filename;
	Vector4     m_min;
	Vector4     m_max;
	Vector4     m_avg; // for computing average colour for mip fading (e.g. detail maps)
	float       m_maxRGBSaturation;
	float       m_maxAlphaDiffFrom01; // maximum absolute alpha difference from 0 or 1
	float       m_maxLumaMinusAlpha0; // maximum difference luminance - max(0,alpha)
	float       m_baseAlphaCoverage;
	int         m_count;
};

fwTextureFormatHelper::fwTextureFormatHelper(const char* filename)
{
	sysMemSet(this, 0, sizeof(*this));

	m_filename = filename;
}

__forceinline void fwTextureFormatHelper::ConsiderPixel(const Vector4& v)
{
	if (m_count == 0)
	{
		m_min = v;
		m_max = v;
	}
	else
	{
		m_min.x = Min<float>(m_min.x, v.x);
		m_min.y = Min<float>(m_min.y, v.y);
		m_min.z = Min<float>(m_min.z, v.z);
		m_min.w = Min<float>(m_min.w, v.w);
		m_max.x = Max<float>(m_max.x, v.x);
		m_max.y = Max<float>(m_max.y, v.y);
		m_max.z = Max<float>(m_max.z, v.z);
		m_max.w = Max<float>(m_max.w, v.w);
	}

	const float minRGB = Min<float>(v.x, v.y, v.z);
	const float maxRGB = Max<float>(v.x, v.y, v.z); // luma

	m_maxRGBSaturation   = Max<float>(m_maxRGBSaturation, maxRGB - minRGB);
	m_maxAlphaDiffFrom01 = Max<float>(m_maxAlphaDiffFrom01, Min<float>(Abs<float>(v.w), Abs<float>(1.0f - v.w)));
	m_maxLumaMinusAlpha0 = Max<float>(m_maxLumaMinusAlpha0, maxRGB - Abs<float>(v.w));

	// note that m_maxLumaMinusAlpha0 really should be computed using max *absolute* RGB minus
	// abs(alpha), not max RGB. this will only make a difference if RGB values can be < 0, which
	// is probably not going to happen.

	m_count++;
}

// ================================================================================================

class fwTextureConversion : public fwTextureConversionParams
{
public:
	fwImage*         ResampleSource      (const grcImage* image, int mip, int scaleX, int scaleY, bool bSkipDistanceField) const;
	int              ChooseMipCount      (const fwImage& image) const;
	void             PreprocessImage     (const fwImage& image) const;
	bool             BuildTerrainBlendMap(grcImage* pImage, eTextureFormatUsage usage, const char* filename, fwTextureFormatHelper& helper);
	int              BuildMipChain       (fwImage**& mipChain, const grcImage* image, const grcImage* diffuseImage = NULL) const; // returns mipCount
	void             BuildMip            (fwImage**  mipChain, int mipIndex) const;
	void             PostprocessMip      (fwImage**  mipChain, int mipIndex, int mipCount, fwTextureFormatHelper& helper) const;
	grcImage::Format ChooseTextureFormat (const fwTextureFormatHelper& helper, char swizzle[6]) const;
	grcImage::Format PreConvert          (fwImage**  mipChain, int mipCount, const fwTextureFormatHelper& helper, char swizzle[6]) const;
	grcImage*        Convert             (fwImage**  mipChain, int mipCount, grcImage::Format formatFlags, bool bForceContiguousMips) const;
	grcImage*        Process             (const grcImage* image, const char* filename, char swizzle[6], u8& conversionFlags, const SecondaryInputs* secondaryInputs, SecondaryOutputs* secondaryOutputs) const;

	// note that this class gets cast directly from fwTextureConversionParams, so it must not contain any data
};

#if defined(_DEBUG) && defined(_MSC_VER)
	__pragma(optimize("", on)); // PRAGMA-OPTIMIZE-ALLOW
#endif

namespace EDT {

// Euclidean Distance Transform (EDT)
// ----------------------------------------------------------------
// Algorithm based on Ricardo Fabbri's implementation of Maurer EDT
// http://www.lems.brown.edu/~rfabbri/stuff/fabbri-EDT-survey-ACMCSurvFeb2008.pdf
// http://tc18.liris.cnrs.fr/subfields/distance_skeletons/DistanceTransform.pdf
// http://www.lems.brown.edu/vision/people/leymarie/Refs/CompVision/DT/DTpaper.pdf
// http://www.comp.nus.edu.sg/~tants/jfa/i3d06.pdf (jump flooding)
// http://www.comp.nus.edu.sg/~tants/jfa/rong-guodong-phd-thesis.pdf
// http://en.wikipedia.org/wiki/Distance_transform

static __forceinline void MaurerEDT_Horizontal(int* img, int w, int UNUSED_PARAM(h), int y)
{
	int* row = &img[y*w];

	if (row[0] != 0)
	{
		row[0] = w;
	}

	for (int x = 1; x < w; x++)
	{
		if (row[x] != 0)
		{
			row[x] = row[x - 1] + 1;
		}
	}

	for (int x = w - 2; x >= 0; x--)
	{
		if (row[x] > row[x + 1] + 1)
		{
			row[x] = row[x + 1] + 1;
		}
	}

	for (int x = 0; x < w; x++)
	{
		if (row[x] >= w)
		{
			row[x] = -1; // -1 is "infinity"
		}
		else
		{
			row[x] = row[x]*row[x]; // distance squared
		}
	}
}

static __forceinline bool MaurerEDT_Remove(int du, int dv, int dw, int u, int v, int w)
{
	s64 a = v - u; // these need to be 64-bit ints so the calculation below doesn't overflow
	s64 b = w - v;
	s64 c = w - u;

	return (c*dv - b*du - a*dw) > a*b*c;
}

static void MaurerEDT_Vertical(int* img, int w, int h, int x, int* G, int* H)
{
	int l1 = -1;
	int l2 = 0;

	int* col = img + x;

	for (int y = 0; y < h; y++)
	{
		const int fi = *col; col += w;

		if (fi != -1)
		{
			while (l1 > 0 && MaurerEDT_Remove(G[l1-1], G[l1], fi, H[l1-1], H[l1], y))
			{
				l1--;
			}

			l1++;
			G[l1] = fi;
			H[l1] = y;
		}
	}

	if (l1 == -1)
	{
		return;
	}

	col = img + x;

	for (int y = 0; y < h; y++)
	{
		int tmp0 = H[l2] - y;
		int tmp1 = G[l2] + tmp0*tmp0;

		while (l2 < l1)
		{
			const int tmp2 = H[l2+1] - y;

			if (tmp1 <= G[l2+1] + tmp2*tmp2)
			{
				break;
			}

			l2++;
			tmp0 = H[l2] - y;
			tmp1 = G[l2] + tmp0*tmp0;
		}

		*col = tmp1; col += w;
	}
}

static void MaurerEDT(int* img, int w, int h)
{
	int* G = rage_new int[h];
	int* H = rage_new int[h];

	for (int y = 0; y < h; y++)
	{
		MaurerEDT_Horizontal(img, w, h, y);
	}

	for (int x = 0; x < w; x++)
	{
		MaurerEDT_Vertical(img, w, h, x, G, H);
	}

	delete[] G;
	delete[] H;
}

} // namespace EDT

static __forceinline float exp2f(float x)
{
	const float kLog2 = logf(2.0f);
	return expf(x*kLog2);
}

static __forceinline Vector4 std_VecPow(const Vector4& v, const Vector4& e)
{
	return Vector4(
		powf(v.x, e.x),
		powf(v.y, e.y),
		powf(v.z, e.z),
		powf(v.w, e.w)
	);
}

static __forceinline Vector4 std_VecExp2(const Vector4& v)
{
	return Vector4(
		exp2f(v.x),
		exp2f(v.y),
		exp2f(v.z),
		exp2f(v.w)
	);
}

static void FilteredDownsample(const fwImage& dst, const fwImage& src, fwTextureConversionParams::eTextureAddress texAddr, const Vector4& texAddrBorderColour, const Vector4& gammaExp, const Vector4& filterAmount, const atArray<float>& filterCoefficients)
{
	const int scaleX = src.m_w/dst.m_w;
	const int scaleY = src.m_h/dst.m_h;

	const Vector4 gamma    = std_VecExp2( gammaExp); // 2^gammaExp
	const Vector4 gammaInv = std_VecExp2(-gammaExp);

	int   filterWidth = Max<int>(2, filterCoefficients.GetCount());
	float filter[100] = {0.5f, 0.5f};

	if (filterWidth > 2)
	{
		float filterSum = 0.0f;

		for (int i = 0; i < filterWidth; i++)
		{
			filterSum += filterCoefficients[i];
		}

		for (int i = 0; i < filterWidth; i++)
		{
			filter[i] = filterCoefficients[i]/filterSum;
		}
	}

	const int x0 = scaleX/2 - filterWidth/2;
	const int y0 = scaleY/2 - filterWidth/2;
	const int x1 = x0 + filterWidth;
	const int y1 = y0 + filterWidth;

	for (int y = 0; y < dst.m_h; y++)
	{
		for (int x = 0; x < dst.m_w; x++)
		{
			Vector4 sumFiltered(0.0f);
			Vector4 sum(0.0f);

			for (int yy = y0; yy < y1; yy++) // general separable filter
			{
				for (int xx = x0; xx < x1; xx++)
				{
					Vector4 c;

					switch (texAddr)
					{
					case fwTextureConversionParams::ta_Wrap         : c = src.GetPixelWrapped(x*scaleX + xx, y*scaleY + yy); break;
					case fwTextureConversionParams::ta_ClampToEdge  : c = src.GetPixelClamped(x*scaleX + xx, y*scaleY + yy); break;
					case fwTextureConversionParams::ta_ClampToBorder: c = src.GetPixelBorder (x*scaleX + xx, y*scaleY + yy, texAddrBorderColour); break;
					}

					sumFiltered += std_VecPow(c, gammaInv)*(filter[xx - x0]*filter[yy - y0]);
				}
			}

			// clamp result, since filter might go outside [0..1] range even though it's normalised
			sumFiltered.x = Clamp<float>(sumFiltered.x, 0.0f, 1.0f);
			sumFiltered.y = Clamp<float>(sumFiltered.y, 0.0f, 1.0f);
			sumFiltered.z = Clamp<float>(sumFiltered.z, 0.0f, 1.0f);
			sumFiltered.w = Clamp<float>(sumFiltered.w, 0.0f, 1.0f);

			for (int yy = 0; yy < scaleY; yy++) // box filter
			{
				for (int xx = 0; xx < scaleX; xx++)
				{
					sum += std_VecPow(src.GetPixel(x*scaleX + xx, y*scaleY + yy), gammaInv);
				}
			}

			sum *= 1.0f/(float)(scaleX*scaleY);

			// blend between general filtered result and box filtered result
			sum.x += filterAmount.x*(sumFiltered.x - sum.x);
			sum.y += filterAmount.y*(sumFiltered.y - sum.y);
			sum.z += filterAmount.z*(sumFiltered.z - sum.z);
			sum.w += filterAmount.w*(sumFiltered.w - sum.w);

			dst.SetPixel(x, y, std_VecPow(sum, gamma));
		}
	}
}

#if defined(_DEBUG) && defined(_MSC_VER)
	__pragma(optimize("", off)); // PRAGMA-OPTIMIZE-ALLOW
#endif

#define MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4 (0)
fwImage* fwTextureConversion::ResampleSource(const grcImage* image, int mip, int scaleX, int scaleY, bool bSkipDistanceField) const
{
	for (int i = 0; i < mip; i++)
	{
		image = image->GetNext();
	}

	const int w0 = image->GetWidth ();
	const int h0 = image->GetHeight();

	scaleX = (int)(((float)(scaleX*m_imageDownsampleScaleX))*m_imageDownsampleScaleRelX);
	scaleY = (int)(((float)(scaleY*m_imageDownsampleScaleY))*m_imageDownsampleScaleRelY);

#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
	while (Min<int>(w0, h0)/scaleX < 4 && scaleX > 1) { scaleX /= 2; }
	while (Min<int>(w0, h0)/scaleY < 4 && scaleY > 1) { scaleY /= 2; }
#endif // MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4

	while (Max<int>(w0, h0)/scaleX > m_imageMaxSizeX)
	{
#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		if (Min<int>(w0, h0)/scaleX <= 4)
		{
			break;
		}
#endif // MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		scaleX *= 2;
	}

	while (Max<int>(w0, h0)/scaleY > m_imageMaxSizeY)
	{
#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		if (Min<int>(w0, h0)/scaleY <= 4)
		{
			break;
		}
#endif // MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
		scaleY *= 2;
	}

	fwImage* source = fwImage::ConvertFrom(image); // resulting image will be w0/scale, h0/scale

	if (m_distanceField && m_distanceFieldRange > 0.0f && !bSkipDistanceField) // distance field
	{
		const float distanceScale = 1.0f/m_distanceFieldRange;
		const int n = w0*h0; // area
		Vector4* img = source->GetPixelRowAddr(0);
		int* temp0 = NULL;
		int* temp1 = NULL;

		if (1) // positive range
		{
			temp0 = rage_new int[w0*h0]; for (int i = 0; i < n; i++) { temp0[i] = img[i].x > 0.5f ? 1 : 0; } EDT::MaurerEDT(temp0, w0, h0);
		}

		if (!m_distanceFieldUnsigned) // negative range
		{
			temp1 = rage_new int[w0*h0]; for (int i = 0; i < n; i++) { temp1[i] = img[i].x > 0.5f ? 0 : 1; } EDT::MaurerEDT(temp1, w0, h0);
		}

		if (1) // positive range
		{
			for (int i = 0; i < n; i++)
			{
				const int d2 = temp0[i];
				Vector4& c = img[i];

				c.x = sqrtf((float)d2);
			}
		}

		if (!m_distanceFieldUnsigned) // negative range
		{
			for (int i = 0; i < n; i++)
			{
				const int d2 = temp1[i];
				Vector4& c = img[i];

				c.x -= sqrtf((float)d2);
			}
		}

		for (int i = 0; i < n; i++)
		{
			Vector4& c = img[i];

			c.x = 0.5f + c.x*distanceScale;
			c.y = c.x;
			c.z = c.x;
			c.w = 1.0f;
		}

		if (temp0) { delete[] temp0; }
		if (temp1) { delete[] temp1; }
	}

	if (!m_preprocessAfterDownsample) // do some processing on top-level mipmap
	{
		PreprocessImage(*source);
	}

	if (scaleX > 1 || scaleY > 1)
	{
		const int w = w0/scaleX;
		const int h = h0/scaleY;

		fwImage* temp = rage_new fwImage(w, h);

		if (IsZeroAll(RCC_VEC4V(m_downsampleGammaExponent)) && m_downsampleFilterCoefficients.GetCount() == 0)
		{
			const Vector4 scaleAreaInv(1.0f/(float)(scaleX*scaleY));

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4 sum(0.0f);

					for (int yy = 0; yy < scaleY; yy++)
					{
						for (int xx = 0; xx < scaleX; xx++)
						{
							sum += source->GetPixel(x*scaleX + xx, y*scaleY + yy);
						}
					}

					temp->SetPixel(x, y, sum*scaleAreaInv);
				}
			}
		}
		else
		{
			FilteredDownsample(*temp, *source, m_texAddr, m_texAddrBorderColour, m_downsampleGammaExponent, m_downsampleFilterAmount, m_downsampleFilterCoefficients);
		}

		delete source;
		source = temp;
	}

	return source;
}

int fwTextureConversion::ChooseMipCount(const fwImage& image) const
{
	const int w0 = image.m_w;
	const int h0 = image.m_h;

	int mipCount = 1;

	while (true)
	{
		const int w = Max<int>(1, w0 >> (mipCount - 1));
		const int h = Max<int>(1, h0 >> (mipCount - 1));

		if (Min<int>(w, h) < m_imageMinMinMipSize ||
			Max<int>(w, h) < m_imageMinMaxMipSize)
		{
			mipCount--;
			break;
		}
		else if (mipCount == m_imageMaxMips)
		{
			break;
		}
		else
		{
			mipCount++;
		}
	}

	return Max<int>(1, mipCount);
}

static __forceinline float SRGBToLinear(float Cgam);

void fwTextureConversion::PreprocessImage(const fwImage& image) const
{
	const int w = image.m_w;
	const int h = image.m_h;

	if (m_sRGBInputR || m_sRGBInputG || m_sRGBInputB || m_sRGBInputA) // apply sRGB to channels if needed
	{
		for (int x = 0; x < w; x++)
		{
			for (int y = 0; y < h; y++)
			{
				Vector4& c = image.GetPixel(x, y);

				if (m_colourKey.GetColor() != 0 &&
					m_colourKey.GetRed  () == (int)(c.x*255.0f + 0.5f) &&
					m_colourKey.GetGreen() == (int)(c.y*255.0f + 0.5f) &&
					m_colourKey.GetBlue () == (int)(c.z*255.0f + 0.5f))
				{
					continue;
				}

				c.x = m_sRGBInputR ? SRGBToLinear(c.x) : c.x;
				c.y = m_sRGBInputG ? SRGBToLinear(c.y) : c.y;
				c.z = m_sRGBInputB ? SRGBToLinear(c.z) : c.z;
				c.w = m_sRGBInputA ? SRGBToLinear(c.w) : c.w;
			}
		}
	}

	if (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap)
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				float nx = c.x*2.0f - 1.0f;
				float ny = c.y*2.0f - 1.0f;
				float nz = c.z*2.0f - 1.0f;
				float nw = nx*nx + ny*ny + nz*nz;

				if (nw > 0.0f)
				{
					nw = 1.0f/sqrtf(nw);

					c.x = (nx*nw + 1.0f)/2.0f;
					c.y = (ny*nw + 1.0f)/2.0f;
					c.z = (nz*nw + 1.0f)/2.0f;
				}
			}
		}
	}
}

int fwTextureConversion::BuildMipChain(fwImage**& mipChain, const grcImage* image, const grcImage* diffuseImage) const
{
	if (grcImage::IsFormatDXTBlockCompressed(image->GetFormat()) ||
		image->GetFormat() == grcImage::R5G6B5 ||
		image->GetFormat() == grcImage::A1R5G5B5 ||
		image->GetFormat() == grcImage::A4R4G4B4)
	{
		if (!m_allowCompressedSource)
		{
			mipChain = NULL;
			return 0;
		}
	}

	int scaleX = 1;
	int scaleY = 1;

	if (diffuseImage)
	{
		scaleX = image->GetWidth ()/diffuseImage->GetWidth ();
		scaleY = image->GetHeight()/diffuseImage->GetHeight();
	}

	fwImage* mip0 = ResampleSource(image, 0, scaleX, scaleY, false);

	if (diffuseImage) // apply diffuse image, if present
	{
		fwImage* diffuse = ResampleSource(diffuseImage, 0, 1, 1, true);

		if (diffuse->m_w == mip0->m_w &&
			diffuse->m_h == mip0->m_h)
		{
			for (int y = 0; y < mip0->m_h; y++)
			{
				for (int x = 0; x < mip0->m_w; x++)
				{
					Vector4& c = mip0->GetPixel(x, y);
					const float d = c.x;

					c = diffuse->GetPixel(x, y);
					c.w = d;
				}
			}
		}

		delete diffuse;
	}

	if (m_preprocessAfterDownsample) // do some processing on top-level mipmap
	{
		PreprocessImage(*mip0);
	}

	const int mipCount = ChooseMipCount(*mip0);

	mipChain = rage_new fwImage*[mipCount];
	mipChain[0] = mip0;

	for (int i = 1; i < mipCount; i++)
	{
		if (m_respectMips && i <= image->GetExtraMipCount())
		{
			mipChain[i] = ResampleSource(image, i, scaleX, scaleY, false);
		}
		else
		{
			const int mipW = Max<int>(1, mip0->m_w >> i);
			const int mipH = Max<int>(1, mip0->m_h >> i);

			mipChain[i] = rage_new fwImage(mipW, mipH);
		}
	}

	for (int i = 1; i < mipCount; i++) // build mipmaps
	{
		if (m_respectMips && i <= image->GetExtraMipCount())
		{
			PreprocessImage(*mipChain[i]);
		}
		else
		{
			BuildMip(mipChain, i);
		}
	}

	return mipCount;
}

void fwTextureConversion::BuildMip(fwImage** mipChain, int mipIndex) const
{
	const fwImage& dst = *mipChain[mipIndex];
	const fwImage& src = *mipChain[mipIndex - 1];

	const int w = dst.m_w;
	const int h = dst.m_h;

	const int scaleX = src.m_w / w;
	const int scaleY = src.m_h / h;

	const Vector4 scaleInv(1.0f/(float)(scaleX*scaleY));

	Assert(scaleX >= 1 && scaleX <= 2); // only supports downsampling by 1x2, 2x1 or 2x2
	Assert(scaleY >= 1 && scaleY <= 2);
	Assert(scaleX == 2 || scaleY == 2);

	if (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap) // use simple linear downsampling .. renormalisation and sharpening are applied in postprocess
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4 sum(0.0f);

				for (int yy = 0; yy < scaleY; yy++)
				{
					for (int xx = 0; xx < scaleX; xx++)
					{
						sum += src.GetPixel(x*scaleX + xx, y*scaleY + yy);
					}
				}

				dst.SetPixel(x, y, sum*scaleInv);
			}
		}
	}
	else if (m_texFormatUsage == tfu_CableMap) // special mode for cables
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				const int xx = x + ((x < w/2) ? 0 : (w*scaleX - w));
				const int yy = y + ((y < h/2) ? 0 : (h*scaleY - h));

				dst.SetPixel(x, y, src.GetPixel(xx, yy));
			}
		}
	}
	// TODO -- this might be useful, but needs to happen only on the channel(s) which store the distance field
	/*else if (m_distanceField) // use special downsampling for distance fields
	{
		const float distanceFieldSlope = 2.0f; // >1 applies contrast to each mip, while keeping 0.5 threshold the same

		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4 sum(0.0f);

				for (int yy = 0; yy < scaleY; yy++)
				{
					for (int xx = 0; xx < scaleX; xx++)
					{
						const Vector4& c = src.GetPixel(x*scaleX + xx, y*scaleY + yy);

						sum.x += Clamp<float>((c.x*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
						sum.y += Clamp<float>((c.y*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
						sum.z += Clamp<float>((c.z*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
						sum.w += Clamp<float>((c.w*2.0f - 1.0f)*distanceFieldSlope, -1.0f, 1.0f)*0.5f + 0.5f;
					}
				}

				dst.SetPixel(x, y, sum*scaleInv);
			}
		}
	}*/
	else
	{
		const Vector4 mipGamma    = std_VecExp2( m_mipGammaExponent); // 2^mipGamma
		const Vector4 mipGammaInv = std_VecExp2(-m_mipGammaExponent);

		if (m_mipFilterCoefficients.GetCount() == 0)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4 sum(0.0f);

					for (int yy = 0; yy < scaleY; yy++)
					{
						for (int xx = 0; xx < scaleX; xx++)
						{
							sum += std_VecPow(src.GetPixel(x*scaleX + xx, y*scaleY + yy), mipGammaInv);
						}
					}

					sum *= scaleInv;
					sum.x = Clamp<float>(sum.x, 0.0f, 1.0f);
					sum.y = Clamp<float>(sum.y, 0.0f, 1.0f);
					sum.z = Clamp<float>(sum.z, 0.0f, 1.0f);
					sum.w = Clamp<float>(sum.w, 0.0f, 1.0f);

					dst.SetPixel(x, y, std_VecPow(sum, mipGamma));
				}
			}
		}
		else
		{
			FilteredDownsample(dst, src, m_texAddr, m_texAddrBorderColour, m_mipGammaExponent, m_mipFilterAmount, m_mipFilterCoefficients);
		}
	}
}

static Vector4 NormalisePixelVector(const Vector4& n)
{
	const float nx = n.x*2.0f - 1.0f;
	const float ny = n.y*2.0f - 1.0f;
	const float nz = n.z*2.0f - 1.0f;
	const float nw = nx*nx + ny*ny + nz*nz;

	if (nw > 0.0f)
	{
		const float q = 1.0f/sqrtf(nw);
		return Vector4(q*nx, q*ny, q*nz, n.w); // [xyz is -1..1]
	}
	else
	{
		return Vector4(0.0f, 0.0f, 0.0f, n.w);
	}
}

static float SpecPowToVariance(float x)
{
	return 1.0f/(x + 1.0f);
}

static float SpecPowFromVariance(float x, float specPowMin, float specPowMax)
{
	const float varianceMin = 1.0f/(specPowMax + 1.0f);
	const float varianceMax = 1.0f/(specPowMin + 1.0f);

	return 1.0f/Clamp<float>(x, varianceMin, varianceMax) - 1.0f;
}

static float UnpackSpecFromGloss(float x, float specPowMin, float specPowMax)
{
	return specPowMin*powf(specPowMax/specPowMin, Clamp<float>(x, 0.0f, 1.0f));
}

static float PackSpecToGloss(float x, float specPowMin, float specPowMax)
{
	return Clamp<float>(logf(x/specPowMin)/logf(specPowMax/specPowMin), 0.0f, 1.0f);
}

static float UnpackSpecOldPWL(float x)
{
	x *= 512.0f; // input range is [0..1]

	// Adjust range (0..500 -> 0..1500 and 500..512 -> 1500..8196)
	const float expandRange = Max<float>(0.0f, x - 500.0f);
	return (x - expandRange)*3.0f + expandRange*558.0f;
}

static float PackSpecOldPWL(float x)
{
	// Adjust range (0..500 <- 0..1500 and 501..512 <- 1500..8196)
	if (x <= 1500.0f)
		x = x/3.0f;
	else
		x = (x + 277500.0f)/558.0f;

	return x/512.0f; // output range is [0..1]
}

static float PackGlossValue(float variance, float specMin, float specMax, float oneOverLogSpecMaxOverSpecMin, bool bUseOldPWLPacking)
{
	if (variance > 0.0f)
	{
		const float specPow = SpecPowFromVariance(variance, specMin, specMax);

		if (bUseOldPWLPacking)
		{
			return PackSpecOldPWL(Clamp<float>(specPow, 1.0f, 8192.0f));
		}
		else
		{
			return Clamp<float>(logf(specPow/specMin)*oneOverLogSpecMaxOverSpecMin, 0.0f, 1.0f);
		}
	}
	else
	{
		return 1.0f;
	}
}

static void ComputeGloss(const fwImage& top, int topFilterRadius, const fwImage& mip, int mipFilterRadius, int texAddr, float specVar, float specMin, float specMax, float normalVarianceScale, bool bStoreVarianceInAlpha, bool bUseOldPWLPacking)
{
	const int mw = mip.m_w;
	const int mh = mip.m_h;
	const int sx = top.m_w/mw;
	const int sy = top.m_h/mh;

	const float topScale = 1.0f/(float)((sx + topFilterRadius*2)*(sy + topFilterRadius*2));
	const float mipScale = 1.0f/(float)((1  + mipFilterRadius*2)*(1  + mipFilterRadius*2));

	const float oneOverLogSpecMaxOverSpecMin = 1.0f/logf(specMax/specMin);

	for (int y = 0; y < mh; y++)
	{
		for (int x = 0; x < mw; x++)
		{
			Vector4 n = mip.GetPixel(x, y);

			if (mipFilterRadius > 0)
			{
				Vector4 sum(0.0f, 0.0f, 0.0f, 0.0f);

				for (int yy = -mipFilterRadius; yy <= mipFilterRadius; yy++)
				{
					for (int xx = -mipFilterRadius; xx <= mipFilterRadius; xx++)
					{
						switch (texAddr)
						{
						case fwTextureConversion::ta_Wrap         : sum += mip.GetPixelWrapped(x + xx, y + yy); break;
						case fwTextureConversion::ta_ClampToEdge  : sum += mip.GetPixelClamped(x + xx, y + yy); break;
						case fwTextureConversion::ta_ClampToBorder: sum += mip.GetPixelBorder (x + xx, y + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)); break;
						}						
					}
				}

				n = sum*mipScale;
			}

			n = NormalisePixelVector(n);

			float variance = 0.0f;

			for (int yy = -topFilterRadius; yy < sy + topFilterRadius; yy++)
			{
				for (int xx = -topFilterRadius; xx < sx + topFilterRadius; xx++)
				{
					Vector4 m(0.0f, 0.0f, 0.0f, 0.0f);

					switch (texAddr)
					{
					case fwTextureConversion::ta_Wrap         : m = top.GetPixelWrapped(x*sx + xx, y*sy + yy); break;
					case fwTextureConversion::ta_ClampToEdge  : m = top.GetPixelClamped(x*sx + xx, y*sy + yy); break;
					case fwTextureConversion::ta_ClampToBorder: m = top.GetPixelBorder (x*sx + xx, y*sy + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)); break;
					}

					m = NormalisePixelVector(m);

					const float d = m.x*n.x + m.y*n.y + m.z*n.z;

					variance += 1.0f - d*d;
				}
			}

			variance = Clamp<float>(variance*topScale*normalVarianceScale, 0.0f, 1.0f);

			if (bStoreVarianceInAlpha)
			{
				mip.GetPixel(x, y).w = variance;
			}
			else
			{
				mip.GetPixel(x, y).w = PackGlossValue(variance + specVar, specMin, specMax, oneOverLogSpecMaxOverSpecMin, bUseOldPWLPacking);
			}
		}
	}
}

static void ComputeGlossFromAvgLength(const fwImage& base, int baseFilterRadius, const fwImage& mip, int texAddr, float specVar, float specMin, float specMax, float normalVarianceScale, bool bStoreVarianceInAlpha, bool bUseOldPWLPacking)
{
	const int mw = mip.m_w;
	const int mh = mip.m_h;

	const float oneOverLogSpecMaxOverSpecMin = 1.0f/logf(specMax/specMin);

	if (baseFilterRadius == 0)
	{
		for (int y = 0; y < mh; y++)
		{
			for (int x = 0; x < mw; x++)
			{
				const Vector4& n = mip.GetPixel(x, y);

				const float nx = n.x*2.0f - 1.0f; // linear downsampled vector
				const float ny = n.y*2.0f - 1.0f;
				const float nz = n.z*2.0f - 1.0f;

				const float variance = Clamp<float>((1.0f - nx*nx - ny*ny - nz*nz)*normalVarianceScale, 0.0f, 1.0f);

				if (bStoreVarianceInAlpha)
				{
					mip.GetPixel(x, y).w = variance;
				}
				else
				{
					mip.GetPixel(x, y).w = PackGlossValue(variance + specVar, specMin, specMax, oneOverLogSpecMaxOverSpecMin, bUseOldPWLPacking);
				}
			}
		}
	}
	else
	{
		const int sx = base.m_w/mw;
		const int sy = base.m_h/mh;

		const float scale = 1.0f/(float)((sx + baseFilterRadius*2)*(sy + baseFilterRadius*2));

		for (int y = 0; y < mh; y++)
		{
			for (int x = 0; x < mw; x++)
			{
				Vector4 sum(0.0f, 0.0f, 0.0f, 0.0f);

				for (int yy = -baseFilterRadius; yy < sy + baseFilterRadius; yy++)
				{
					for (int xx = -baseFilterRadius; xx < sx + baseFilterRadius; xx++)
					{
						switch (texAddr)
						{
						case fwTextureConversion::ta_Wrap         : sum += base.GetPixelWrapped(x*sx + xx, y*sy + yy); break;
						case fwTextureConversion::ta_ClampToEdge  : sum += base.GetPixelClamped(x*sx + xx, y*sy + yy); break;
						case fwTextureConversion::ta_ClampToBorder: sum += base.GetPixelBorder (x*sx + xx, y*sy + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)); break;
						}
					}
				}

				const float nx = scale*sum.x*2.0f - 1.0f; // linear downsampled vector
				const float ny = scale*sum.y*2.0f - 1.0f;
				const float nz = scale*sum.z*2.0f - 1.0f;

				const float variance = Clamp<float>((1.0f - nx*nx - ny*ny - nz*nz)*normalVarianceScale, 0.0f, 1.0f);

				if (bStoreVarianceInAlpha)
				{
					mip.GetPixel(x, y).w = variance;
				}
				else
				{
					mip.GetPixel(x, y).w = PackGlossValue(variance + specVar, specMin, specMax, oneOverLogSpecMaxOverSpecMin, bUseOldPWLPacking);
				}
			}
		}
	}
}

static void ApplyAlphaBlur(const fwImage& mip, int texAddr, float amount, int r, int numPasses, float minFactor)
{
	if (amount > 0.0f)
	{
		const int w = mip.m_w;
		const int h = mip.m_h;

		float* temp = rage_new float[w*h];

		for (int pass = 0; pass < numPasses; pass++)
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					float sum = 0.0f;

					for (int yy = -r; yy <= r; yy++)
					{
						for (int xx = -r; xx <= r; xx++)
						{
							switch (texAddr)
							{
							case fwTextureConversion::ta_Wrap         : sum += mip.GetPixelWrapped(x + xx, y + yy).w; break;
							case fwTextureConversion::ta_ClampToEdge  : sum += mip.GetPixelClamped(x + xx, y + yy).w; break;
							case fwTextureConversion::ta_ClampToBorder: sum += mip.GetPixelBorder (x + xx, y + yy, Vector4(0.5f, 0.5f, 1.0f, 1.0f)).w; break;
							}
						}
					}

					sum /= (float)((r*2 + 1)*(r*2 + 1));
					sum += (Min<float>(sum, mip.GetPixel(x, y).w) - sum)*minFactor;

					temp[x + y*w] = sum;
				}
			}

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					mip.GetPixel(x, y).w = temp[x + y*w];
				}
			}
		}

		delete[] temp;
	}
}

#define ALPHA_COVERAGE_RADIUS_MAX 31

static float ComputeAlphaCoverage(const fwImage& image, float alphaRef, int radius, fwTextureConversionParams::eTextureAddress texAddr, const Vector4& texAddrBorderColour)
{
	const int w = image.m_w;
	const int h = image.m_h;
	int coverage = 0;
	int count = 0;

	CompileTimeAssert(ALPHA_COVERAGE_RADIUS_MAX*2 + 1 <= 64);
	u64 mask[ALPHA_COVERAGE_RADIUS_MAX*2 + 1];

	if (radius <= ALPHA_COVERAGE_RADIUS_MAX)
	{
		sysMemSet(mask, 0, sizeof(mask));

		const float radiusPlusHalf = (float)radius + 0.5f;
		const float radiusPlusHalfSqr = radiusPlusHalf*radiusPlusHalf;

		for (int dy = -radius; dy <= radius; dy++)
		{
			for (int dx = -radius; dx <= radius; dx++)
			{
				if (dx*dx + dy*dy <= radiusPlusHalfSqr)
				{
					mask[dy + radius] |= BIT64(dx + radius);
				}
			}
		}
	}

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			float maxNeighbourAlpha = 0.0f;

			if (radius <= ALPHA_COVERAGE_RADIUS_MAX)
			{
				for (int dy = -radius; dy <= radius; dy++)
				{
					for (int dx = -radius; dx <= radius; dx++)
					{
						if (mask[dy + radius] & BIT64(dx + radius))
						{
							float alpha = 0.0f;

							switch (texAddr)
							{
							case fwTextureConversionParams::ta_Wrap         : alpha = image.GetPixelWrapped(x + dx, y + dy).w; break;
							case fwTextureConversionParams::ta_ClampToEdge  : alpha = image.GetPixelClamped(x + dx, y + dy).w; break;
							case fwTextureConversionParams::ta_ClampToBorder: alpha = image.GetPixelBorder (x + dx, y + dy, texAddrBorderColour).w; break;
							}

							maxNeighbourAlpha = Max<float>(alpha, maxNeighbourAlpha);
						}
					}
				}
			}
			else
			{
				maxNeighbourAlpha = 1.0f;
			}

			if (maxNeighbourAlpha > 0.0f)
			{
				if (image.GetPixel(x, y).w > alphaRef)
				{
					coverage++;
				}

				count++;
			}
		}
	}

	if (count > 0)
	{
		return (float)coverage/(float)count;
	}
	else
	{
		return 0.0f;
	}
}

void fwTextureConversion::PostprocessMip(fwImage** mipChain, int mipIndex, int mipCount, fwTextureFormatHelper& helper) const
{
	const fwImage& mip = *mipChain[mipIndex];

	const int w = mip.m_w;
	const int h = mip.m_h;

	if (m_texFormatUsage == tfu_NormalMap || m_texFormatUsage == tfu_VectorMap)
	{
		if (m_normalMapGlossInAlpha)
		{
			int glossSrcFilterRadius = 1;
			int glossDstFilterRadius = 1;

			if (m_normalMapGlossSrcFilterRadius.GetCount() > 0)
			{
				glossSrcFilterRadius = m_normalMapGlossSrcFilterRadius[Min<int>(mipIndex, m_normalMapGlossSrcFilterRadius.GetCount() - 1)];
			}

			if (m_normalMapGlossDstFilterRadius.GetCount() > 0)
			{
				glossDstFilterRadius = m_normalMapGlossDstFilterRadius[Min<int>(mipIndex, m_normalMapGlossDstFilterRadius.GetCount() - 1)];
			}

			const float specMin = m_normalMapGlossSpecularMin;
			const float specMax = m_normalMapGlossSpecularMax;
			float specVar = 0.0f;

			if (!m_normalMapGlossStoreVarianceAlpha)
			{
				float specPow = m_normalMapGlossSpecularPower;

				if (m_normalMapGlossSpecPowScale.GetCount() > 0)
				{
					if (m_normalMapGlossSpecPowScaleToLastMip)
					{
						specPow *= m_normalMapGlossSpecPowScale[Max<int>(0, mipIndex - mipCount + m_normalMapGlossSpecPowScale.GetCount())];
					}
					else
					{
						specPow *= m_normalMapGlossSpecPowScale[Min<int>(mipIndex, m_normalMapGlossSpecPowScale.GetCount() - 1)];
					}
				}

				specVar = SpecPowToVariance(specPow); // convert specular power to variance
			}

			float normalVarianceScale = 1.0f;

			if (m_normalMapGlossVarianceScale.GetCount() > 0)
			{
				if (m_normalMapGlossVarianceScaleToLastMip)
				{
					normalVarianceScale = m_normalMapGlossVarianceScale[Max<int>(0, mipIndex - mipCount + m_normalMapGlossVarianceScale.GetCount())];
				}
				else
				{
					normalVarianceScale = m_normalMapGlossVarianceScale[Min<int>(mipIndex, m_normalMapGlossVarianceScale.GetCount() - 1)];
				}
			}

			if (m_normalMapGlossUseAverageLength)
			{
				ComputeGlossFromAvgLength(*mipChain[Max<int>(0, mipIndex - 1)], glossSrcFilterRadius, mip, m_texAddr, specVar, specMin, specMax, normalVarianceScale, m_normalMapGlossStoreVarianceAlpha, m_normalMapGlossUseOldPWLPacking);
			}
			else
			{
				ComputeGloss(*mipChain[0], glossSrcFilterRadius, mip, glossDstFilterRadius, m_texAddr, specVar, specMin, specMax, normalVarianceScale, m_normalMapGlossStoreVarianceAlpha, m_normalMapGlossUseOldPWLPacking);
			}

			// you have to set all three of these to use alpha blur
			// note that 'MinFactor' is designed to be used with variance maps .. for gloss maps you probably want 'MaxFactor'
			if (m_normalMapGlossAlphaBlur.GetCount() > 0 &&
				m_normalMapGlossAlphaBlurRadius > 0 &&
				m_normalMapGlossAlphaBlurPasses > 0)
			{
				ApplyAlphaBlur(mip, m_texAddr, m_normalMapGlossAlphaBlur[Min<int>(mipIndex, m_normalMapGlossAlphaBlur.GetCount() - 1)], m_normalMapGlossAlphaBlurRadius, m_normalMapGlossAlphaBlurPasses, m_normalMapGlossAlphaBlurMinFactor);
			}

			if (m_normalMapGlossAlphaOnly)
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						Vector4& n = mip.GetPixel(x, y);
						
						n.x = 0.0f;
						n.y = 0.0f;
						n.z = 0.0f;
					}
				}
			}

			if (m_normalMapGlossAlphaExponent != 1.0f)
			{
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						Vector4& n = mip.GetPixel(x, y);

						n.w = powf(n.w, m_normalMapGlossAlphaExponent);
					}
				}
			}
		}

		if (mipIndex > 0)
		{
			float sharpen = m_normalMapSharpenAmount;
			float scale   = (float)(1<<mipIndex);

			if (m_normalMapSharpen.GetCount() > 0)
			{
				if (m_normalMapSharpenToLastMip)
				{
					sharpen *= m_normalMapSharpen[Max<int>(0, mipIndex - mipCount + m_normalMapSharpen.GetCount())];
				}
				else
				{
					sharpen *= m_normalMapSharpen[Min<int>(mipIndex, m_normalMapSharpen.GetCount() - 1)];
				}
			}

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& n = mip.GetPixel(x, y);

					float nx = n.x*2.0f - 1.0f;
					float ny = n.y*2.0f - 1.0f;
					float nz = n.z*2.0f - 1.0f;

					const float r2 = nx*nx + ny*ny;

					if (r2 > 0.0f)
					{
						if (sharpen > 0.0f)
						{
							const float k = powf(scale, (m_normalMapSharpenAlphaMask ? n.w : 1.0f)*sharpen);

							nz = powf(sqrtf(Max<float>(0.0f, 1.0f - r2)), k);

							const float q = sqrtf(Max<float>(0.0f, 1.0f - nz*nz)/r2);

							nx *= q;
							ny *= q;
						}
						else
						{
							nz = sqrtf(Max<float>(0.0f, 1.0f - r2));
						}
					}
					else
					{
						nz = 1.0f;
					}

					n.x = (nx + 1.0f)/2.0f;
					n.y = (ny + 1.0f)/2.0f;
					n.z = (nz + 1.0f)/2.0f;
				}
			}
		}
	}
	else // not a normal map
	{
		Vector4 filterSharpen = m_mipFilterSharpenAmount;

		if (m_mipFilterSharpen.GetCount() > 0)
		{
			if (m_mipFilterSharpenToLastMip)
			{
				filterSharpen *= m_mipFilterSharpen[Max<int>(0, mipIndex - mipCount + m_mipFilterSharpen.GetCount())];
			}
			else
			{
				filterSharpen *= m_mipFilterSharpen[Min<int>(mipIndex, m_mipFilterSharpen.GetCount() - 1)];
			}
		}

		if (filterSharpen.x != 0.0f ||
			filterSharpen.y != 0.0f ||
			filterSharpen.z != 0.0f ||
			filterSharpen.w != 0.0f)
		{
			fwImage blr(w, h);

			for (int y = 0; y < h; y++) // create a blurred version of mip (using simple 3x3 box filter)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4 sum(0.0f);

					for (int yy = -1; yy <= 1; yy++)
					{
						for (int xx = -1; xx <= 1; xx++)
						{
							switch (m_texAddr)
							{
							case ta_Wrap         : sum += mip.GetPixelWrapped(x + xx, y + yy); break;
							case ta_ClampToEdge  : sum += mip.GetPixelClamped(x + xx, y + yy); break;
							case ta_ClampToBorder: sum += mip.GetPixelBorder (x + xx, y + yy, m_texAddrBorderColour); break;
							}
						}
					}

					blr.SetPixel(x, y, sum*(1.0f/9.0f));
				}
			}

			for (int y = 0; y < h; y++) // apply the mip sharpening as "colour + (colour - blurred)*sharpen"
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& c0 = mip.GetPixel(x, y);
					Vector4& c1 = blr.GetPixel(x, y);

					c0 += (c0 - c1)*filterSharpen;

					c0.x = Clamp<float>(c0.x, 0.0f, 1.0f);
					c0.y = Clamp<float>(c0.y, 0.0f, 1.0f);
					c0.z = Clamp<float>(c0.z, 0.0f, 1.0f);
					c0.w = Clamp<float>(c0.w, 0.0f, 1.0f);
				}
			}
		}

		Vector3 filterDesaturate = m_mipFilterDesaturateAmount;

		if (m_mipFilterDesaturate.GetCount() > 0)
		{
			if (m_mipFilterDesaturateToLastMip)
			{
				filterDesaturate *= m_mipFilterDesaturate[Max<int>(0, mipIndex - mipCount + m_mipFilterDesaturate.GetCount())];
			}
			else
			{
				filterDesaturate *= m_mipFilterDesaturate[Min<int>(mipIndex, m_mipFilterDesaturate.GetCount() - 1)];
			}
		}

		if (filterDesaturate.x != 0.0f ||
			filterDesaturate.y != 0.0f ||
			filterDesaturate.z != 0.0f)
		{
			for (int y = 0; y < h; y++) // apply the mip saturation as "colour + (luma - colour)*(1 - saturation)"
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& c0 = mip.GetPixel(x, y);
					Vector3 rgb = Vector3(c0.x, c0.y, c0.z);
					const float l = Max<float>(c0.x, c0.y, c0.z);

					rgb += (Vector3(l, l, l) - rgb)*filterDesaturate;

					c0.x = rgb.x;
					c0.y = rgb.y;
					c0.z = rgb.z;
				}
			}
		}

		float alphaScale = 1.0f;

		if (m_alphaScale.GetCount() > 0)
		{
			if (m_alphaScaleToLastMip)
			{
				alphaScale *= m_alphaScale[Max<int>(0, mipIndex - mipCount + m_alphaScale.GetCount())];
			}
			else
			{
				alphaScale *= m_alphaScale[Min<int>(mipIndex, m_alphaScale.GetCount() - 1)];
			}
		}

		if (m_alphaCoverageCompensationFactor > 0.0f && mipIndex > 0)
		{
			float alphaRef1 = 0.5f;
			float alphaStep = 0.25f;

			for (int k = 0; k < m_alphaCoverageCompensationIterations; k++) // binary search for find new alpha reference such that coverage = base coverage
			{
				const float alphaCoverage1 = ComputeAlphaCoverage(mip, alphaRef1, m_alphaCoverageCompensationRadius, m_texAddr, m_texAddrBorderColour);

				alphaRef1 += alphaStep*(alphaCoverage1 < helper.m_baseAlphaCoverage ? -1.0f : 1.0f);
				alphaStep *= 0.5f;
			}

			const float alphaCoverageScale = powf(m_alphaCoverageCompensationReference/alphaRef1, m_alphaCoverageCompensationFactor);

			if (alphaCoverageScale > 1.0f || m_alphaCoverageCompensationAllowScaleByLessThanOne)
			{
				alphaScale *= alphaCoverageScale;
			}
		}

		if (alphaScale != 1.0f) // apply alpha scale
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					Vector4& c0 = mip.GetPixel(x, y);

					c0.w = Clamp<float>(c0.w*alphaScale, 0.0f, 1.0f);
				}
			}
		}
	}

	Vector4 fadeColour(0.0f);
	Vector4 fadeAmount(0.0f);
	bool bApplyMipFade = false;

	if (mipIndex > 0)
	{
		fadeColour = m_mipFadeColourAuto ? helper.m_avg : m_mipFadeColour;
		fadeAmount = m_mipFadeAmount;

		if (m_mipFade.GetCount() > 0)
		{
			if (m_mipFadeToLastMip)
			{
				fadeAmount *= m_mipFade[Max<int>(0, mipIndex - mipCount + m_mipFade.GetCount())];
			}
			else
			{
				fadeAmount *= m_mipFade[Min<int>(mipIndex, m_mipFade.GetCount() - 1)];
			}
		}

		bApplyMipFade = !IsZeroAll(RCC_VEC4V(fadeAmount));
	}

	const Vector4 finalGamma = std_VecExp2(m_finalGammaExponent); // 2^finalGamma

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			Vector4& c = mip.GetPixel(x, y);

			if (m_colourKey.GetColor() != 0 &&
				m_colourKey.GetRed  () == (int)(c.x*255.0f + 0.5f) &&
				m_colourKey.GetGreen() == (int)(c.y*255.0f + 0.5f) &&
				m_colourKey.GetBlue () == (int)(c.z*255.0f + 0.5f))
			{
				continue;
			}

			c = std_VecPow(c, finalGamma);
			c *= m_finalMultiplier;

			if (mipIndex == 0) // use fadeColour to accumulate sum of pixel values in top-level mip, for helper.m_avg
			{
				fadeColour += c;
			}
			else if (bApplyMipFade)
			{
				if (IsLessThanAll   (RCC_VEC4V(c), RCC_VEC4V(m_mipFadeThresholdMin)) |
					IsGreaterThanAll(RCC_VEC4V(c), RCC_VEC4V(m_mipFadeThresholdMax)))
				{
					// no mip fade
				}
				else
				{
					c += fadeAmount*(fadeColour - c);
				}
			}

			if (x == 0 || x == w - 1 ||
				y == 0 || y == h - 1)
			{
				if (m_applyBorderColourR) { c.x = m_texAddrBorderColour.x; }
				if (m_applyBorderColourG) { c.y = m_texAddrBorderColour.y; }
				if (m_applyBorderColourB) { c.z = m_texAddrBorderColour.z; }
				if (m_applyBorderColourA) { c.w = m_texAddrBorderColour.w; }
			}

			if (m_alphaThreshold > 0.0f)
			{
				c.w = (c.w < m_alphaThreshold) ? 0.0f : 1.0f;
			}

			if (m_alphaPremultiply)
			{
				c.x *= c.w;
				c.y *= c.w;
				c.z *= c.w;
			}
		}
	}

	if (mipIndex == 0)
	{
		helper.m_avg = fadeColour*(1.0f/(float)(w*h));
	}

	for (int y = 0; y < h; y++) // final pass - consider texels to choose texture format
	{
		for (int x = 0; x < w; x++)
		{
			helper.ConsiderPixel(mip.GetPixel(x, y));
		}
	}
}

#if 0

// from xenon docs
static __forceinline int degamma(int Cgam)
{
	int Clin;

	if      (Cgam <  64) { Clin =   0 + (Cgam -   0)*1; }
	else if (Cgam <  96) { Clin =  64 + (Cgam -  64)*2; }
	else if (Cgam < 192) { Clin = 128 + (Cgam -  96)*4; }
	else                 { Clin = 513 + (Cgam - 192)*8; }

	return Clin;
}

// from xenon docs
static __forceinline int gamma(int Clin)
{
	int Cgam;

	if      (Clin <  64) { Cgam =   0 + (Clin -   0)/1; }
	else if (Clin < 128) { Cgam =  64 + (Clin -  64)/2; }
	else if (Clin < 513) { Cgam =  96 + (Clin - 128)/4; }
	else                 { Cgam = 192 + (Clin - 512)/8; }

	return Cgam;
}

// from PS3 docs
static __forceinline float ApplyGamma_PS3(float Cgam)
{
	if      (Cgam < 0.00000f) { return 0.0f; }
	else if (Cgam < 0.00318f) { return 12.92f*Cgam; }
	else if (Cgam < 1.00000f) { return 1.055f*powf(Cgam, 1.0f/2.4f) - 0.055f; }

	return 1.0f;
}

static __forceinline float GammaToLinearPS3(float Cgam)
{
	if      (Cgam <= 0.00000f) { return 0.0f; }
	else if (Cgam <= 0.03928f) { return Cgam/12.92f; } // website says 0.04045f (as does wikipedia), PS3 docs say 0.03928f
	else if (Cgam <= 1.00000f) { return powf((Cgam + 0.055f)/1.055f, 2.4f); }

	return 1.0f;
}

static __forceinline float GammaToLinearXenon(float Cgam)
{
	float Clin;

	if      (Cgam < 0.250f) { Clin = 0.0000f + (Cgam - 0.000f)*0.25f; }
	else if (Cgam < 0.375f) { Clin = 0.0625f + (Cgam - 0.250f)*0.50f; }
	else if (Cgam < 0.750f) { Clin = 0.1250f + (Cgam - 0.375f)*1.00f; }
	else                    { Clin = 0.5000f + (Cgam - 0.750f)*2.00f; }

	return Clin;
}

// http://en.wikipedia.org/wiki/SRGB
static __forceinline float LinearToSRGB(float Clin)
{
	if      (Clin <= 0.0000000f) { return 0.0f; }
	else if (Clin <= 0.0031308f) { return 12.92f*Clin; }
	else if (Clin <= 1.0000000f) { return 1.055f*powf(Clin, 1.0f/2.4f) - 0.055f; }

	return 1.0f;
}

#endif // 0

// http://en.wikipedia.org/wiki/SRGB
static __forceinline float SRGBToLinear(float Cgam)
{
	if      (Cgam <= 0.00000f) { return 0.0f; }
	else if (Cgam <= 0.04045f) { return Cgam/12.92f; }
	else if (Cgam <= 1.00000f) { return powf((Cgam + 0.055f)/1.055f, 2.4f); }

	return 1.0f;
}

// http://filmicgames.com/archives/14
static __forceinline float LinearToGammaXenon(float Clin)
{
	float Cgam;

	if      (Clin < 0.0625f) { Cgam = 0.000f + (Clin - 0.0000f)*4.0f; }
	else if (Clin < 0.1250f) { Cgam = 0.250f + (Clin - 0.0625f)*2.0f; }
	else if (Clin < 0.5000f) { Cgam = 0.375f + (Clin - 0.1250f)*1.0f; }
	else                     { Cgam = 0.750f + (Clin - 0.5000f)*0.5f; }

	return Cgam;
}

static __forceinline float LinearToGammaPS3(float Clin)
{
	if      (Clin < 0.00000f) { return 0.0f; }
	else if (Clin < 0.00318f) { return 12.92f*Clin; }
	else if (Clin < 1.00000f) { return 1.055f*powf(Clin, 1.0f/2.4f) - 0.055f; }

	return 1.0f;
}

static void LinearToGamma(const fwImage& image, char platform_)
{
	const int w = image.m_w;
	const int h = image.m_h;

	for (int y = 0; y < h; y++)
	{
		if (platform_ == platform::XENON)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				c.x = LinearToGammaXenon(c.x);
				c.y = LinearToGammaXenon(c.y);
				c.z = LinearToGammaXenon(c.z);
			}
		}
		else // sRGB is approximately powf(x, 2.2f) with a small linear segment near 0, so .. could apply final gamma exponent of -1.1375 instead, but this is more straightforward
		{
			for (int x = 0; x < w; x++)
			{
				Vector4& c = image.GetPixel(x, y);

				c.x = LinearToGammaPS3(c.x);
				c.y = LinearToGammaPS3(c.y);
				c.z = LinearToGammaPS3(c.z);
			}
		}
	}
}

static Vector4 PixelPreSwizzle(const Vector4& v, const char swizzle[4]) // this happens right before compression
{
	const float* src = (const float*)&v;

	bool r_valid = false;
	bool g_valid = false;
	bool b_valid = false;
	bool a_valid = false;

	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
	float a = 1.0f;

	for (int i = 0; i < 4; i++) // r,g,b,a
	{
		switch (swizzle[i])
		{
		case 'R' : r = r_valid ? Max<float>(src[i], r) : src[i]; r_valid = true; break;
		case 'G' : g = g_valid ? Max<float>(src[i], g) : src[i]; g_valid = true; break;
		case 'B' : b = b_valid ? Max<float>(src[i], b) : src[i]; b_valid = true; break;
		case 'A' : a = a_valid ? Max<float>(src[i], a) : src[i]; a_valid = true; break;
		case '0' : break;
		case '1' : break;
		}
	}

	return Vector4(r, g, b, a);
}

/*static Vector4 PixelSwizzle(const Vector4& v, const char swizzle[4]) // this happens automatically on the GPU, provided here for reference
{
	float dst[4] = {0.0f, 0.0f, 0.0f, 0.0f};

	for (int i = 0; i < 4; i++) // r,g,b,a
	{
		switch (swizzle[i])
		{
		case 'R' : dst[i] = v.x; break;
		case 'G' : dst[i] = v.y; break;
		case 'B' : dst[i] = v.z; break;
		case 'A' : dst[i] = v.w; break;
		case '0' : dst[i] = 0.f; break;
		case '1' : dst[i] = 1.f; break;
		}
	}

	return Vector4(dst[0], dst[1], dst[2], dst[3]);
}*/

static void ImagePreSwizzle(const fwImage& image, const char swizzle[6])
{
	const int w = image.m_w;
	const int h = image.m_h;

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			Vector4& c = image.GetPixel(x, y);

			c = PixelPreSwizzle(c, swizzle);
		}
	}
}

/*static void ImageSwizzle(const fwImage& image, const char swizzle[6])
{
	const int w = image.m_w;
	const int h = image.m_h;

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			Vector4& c = image.GetPixel(x, y);

			c = PixelSwizzle(c, swizzle);
		}
	}
}*/

grcImage::Format fwTextureConversion::ChooseTextureFormat(const fwTextureFormatHelper& helper, char swizzle[6]) const
{
	eTextureFormat format = m_texFormatOverride;

	if (format == tf_UNKNOWN)
	{
		const float epsilon         = 1.0f/127.0f;
		const bool bIsRedZero       = (helper.m_min.x >= 0.0f - epsilon && helper.m_max.x <= 0.0f + epsilon);
		const bool bIsRedOne        = (helper.m_min.x >= 1.0f - epsilon && helper.m_max.x <= 1.0f + epsilon);
		const bool bIsGreenZero     = (helper.m_min.y >= 0.0f - epsilon && helper.m_max.y <= 0.0f + epsilon);
		const bool bIsGreenOne      = (helper.m_min.y >= 1.0f - epsilon && helper.m_max.y <= 1.0f + epsilon);
		const bool bIsBlueZero      = (helper.m_min.z >= 0.0f - epsilon && helper.m_max.z <= 0.0f + epsilon);
		const bool bIsBlueOne       = (helper.m_min.z >= 1.0f - epsilon && helper.m_max.z <= 1.0f + epsilon);
		const bool bIsAlphaZero     = (helper.m_min.w >= 0.0f - epsilon && helper.m_max.w <= 0.0f + epsilon);
		const bool bIsAlphaOne      = (helper.m_min.w >= 1.0f - epsilon && helper.m_max.w <= 1.0f + epsilon);
		const bool bIsMonochromeRed = (helper.m_max.x - helper.m_min.x > epsilon && bIsGreenZero && bIsBlueZero && bIsAlphaOne); // R001 format
		const bool bIsMonochrome    = (helper.m_maxRGBSaturation   <= epsilon);
		const bool bAlphaIs1bit     = (helper.m_maxAlphaDiffFrom01 <= epsilon);
		const bool bAlphaIsBlack    = (helper.m_maxLumaMinusAlpha0 <= epsilon);

		eTextureFormatChannelGroup channelGroup;

		if (bIsRedZero && bIsGreenZero && bIsBlueZero)
		{
			channelGroup = tfcg_A;
		}
		else if (bIsMonochromeRed)
		{
			channelGroup = tfcg_R;
		}
		else if (((m_texFormatUsage == tfu_NormalMap && m_texFormatHint == tfh_Compressed) || bIsBlueOne) && bIsAlphaOne)
		{
			// Normal maps are special .. during processing they have r,g,b channels (and potentially alpha)
			// but before conversion we are going to overwrite the blue channel with 1.0f, so _if_ alpha is
			// uniform 1.0f too then we can use the 'RG_B1' channel group (i.e. RG11).
			channelGroup = tfcg_RG_B1;
		}
		else if (((m_texFormatUsage == tfu_NormalMap && m_texFormatHint != tfh_Compressed) || bIsBlueZero) && bIsAlphaOne)
		{
			// Normal maps compressed with high quality (DXT5, DXN, G8R8 etc.) will have RG01 swizzle, so
			// that they look different in the texture viewer. Also, next-gen platforms output RG01 for both
			// DXN and G8R8 but they don't support RG11 swizzle.
			channelGroup = tfcg_RG;
		}
		else if (bIsAlphaOne)
		{
			channelGroup = bIsMonochrome ? tfcg_L : tfcg_RGB;
		}
		else
		{
			channelGroup = bIsMonochrome ? tfcg_LA : tfcg_RGBA;
		}

		if (channelGroup == tfcg_RGBA)
		{
			if (bIsRedOne && bIsGreenZero && bIsBlueZero)
			{
				channelGroup = tfcg_A_RGB100; // alpha, rgb={1,0,0}
			}
			else if (bIsRedOne && bIsGreenOne && bIsBlueOne)
			{
				channelGroup = tfcg_A_RGB111; // alpha, rgb={1,1,1}
			}
			else if (bIsAlphaZero)
			{
				channelGroup = tfcg_RGB_A0; // rgb, alpha=0
			}
		}
		else if (channelGroup == tfcg_LA)
		{
			if (bIsAlphaZero)
			{
				channelGroup = tfcg_L_A0; // luminance, alpha=0
			}
		}

		// ========================================================================================
		// ========================================================================================
		//                                    RGB            RGBA          L/A         LA/RG
		// -----------------------------+----------------------------------------------------------
		// tfh_Compressed               |[  4]DXT1      [  8]DXT5      [ 4]DXT5A   [ 4]CTX1       8
		// tfh_CompressedHighQuality    |[  8]DXT5(GAR) [  8]DXT5      [ 4]DXT5A   [ 8]DXN        8
		// tfh_Quantised                |[ 16]565       [ 16]4444      [ 4]DXT5A   [ 8]DXN       16
		// tfh_8bpc                     |[ 32]8888      [ 32]8888      [ 8]8       [16]88        32
		// tfh_16bpc                    |[ 64]16161616  [ 64]16161616  [16]16      [32]1616
		// tfh_16bpc_float              |[ 64]16161616f [ 64]16161616f [16]16f     [32]1616f
		// tfh_32bpc_float              |[128]32323232f [128]32323232f [32]32f     [64]3232f
		//
		// note: PS3 does not support CTX1,DXN,DXT3A,DXT5A ..
		//  * uses DXT1(RG) instead of CTX1
		//  * uses DXT5(AG) instead of DXN
		//  * uses DXT1(G)  instead of DXT5A
		// ========================================================================================
		// ========================================================================================

		/*
		if (PLATFORM == platform::XENON ||
			PLATFORM == platform::PS3)
		{
			switch (channelGroup)
			{
			case tfcg_RGB: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; strcpy(swizzle, "RGB1"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; strcpy(swizzle, "GAR1"); break;
				case tfh_Quantised             : format = tf_R5G6B5       ; strcpy(swizzle, "RGB1"); break;
				case tfh_8bpc                  : format = tf_A8R8G8B8     ; strcpy(swizzle, "RGB1"); break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; strcpy(swizzle, "RGB1"); break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; strcpy(swizzle, "RGB1"); break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; strcpy(swizzle, "RGB1"); break;
				}
				break;

			case tfcg_RGB_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; strcpy(swizzle, "RGB0"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; strcpy(swizzle, "GAR0"); break;
				case tfh_Quantised             : format = tf_R5G6B5       ; strcpy(swizzle, "RGB0"); break;
				case tfh_8bpc                  : format = tf_A8R8G8B8     ; strcpy(swizzle, "RGB0"); break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; strcpy(swizzle, "RGB0"); break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; strcpy(swizzle, "RGB0"); break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; strcpy(swizzle, "RGB0"); break;
				}
				break;

			case tfcg_RGBA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; strcpy(swizzle, "RGBA"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; strcpy(swizzle, m_texFormatHintHighQualityWithAlpha ? "GARB" : "RGBA"); break;
				case tfh_Quantised             : format = tf_A4R4G4B4     ; strcpy(swizzle, "RGBA"); break;
				case tfh_8bpc                  : format = tf_A8R8G8B8     ; strcpy(swizzle, "RGBA"); break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; strcpy(swizzle, "RGBA"); break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; strcpy(swizzle, "RGBA"); break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; strcpy(swizzle, "RGBA"); break;
				}
				break;

			case tfcg_L: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "AAA1"); } else { format = tf_DXT1; strcpy(swizzle, "GGG1"); } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "AAA1"); } else { format = tf_DXT1; strcpy(swizzle, "GGG1"); } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; strcpy(swizzle, "AAA1"); } else { format = tf_L8  ; strcpy(swizzle, "RRR1"); } break;
				case tfh_8bpc                  :             { format = tf_L8   ; strcpy(swizzle, "RRR1"); } break; // could also be tf_A8 "AAA1" or tf_R8 "RRR1"
				case tfh_16bpc                 :             { format = tf_L16  ; strcpy(swizzle, "RRR1"); } break; // could also be tf_R16 "RRR1"
				case tfh_16bpc_float           :             { format = tf_R16F ; strcpy(swizzle, "RRR1"); } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; strcpy(swizzle, "RRR1"); } break;
				}
				break;

			case tfcg_L_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "AAA0"); } else { format = tf_DXT1; strcpy(swizzle, "GGG0"); } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "AAA0"); } else { format = tf_DXT1; strcpy(swizzle, "GGG0"); } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; strcpy(swizzle, "AAA0"); } else { format = tf_L8  ; strcpy(swizzle, "RRR0"); } break;
				case tfh_8bpc                  :             { format = tf_L8   ; strcpy(swizzle, "RRR0"); } break; // could also be tf_A8 "AAA0" or tf_R8 "RRR0"
				case tfh_16bpc                 :             { format = tf_L16  ; strcpy(swizzle, "RRR0"); } break; // could also be tf_R16 "RRR0"
				case tfh_16bpc_float           :             { format = tf_R16F ; strcpy(swizzle, "RRR0"); } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; strcpy(swizzle, "RRR0"); } break;
				}
				break;

			case tfcg_A: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "000A"); } else { format = tf_DXT1; strcpy(swizzle, "000G"); } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "000A"); } else { format = tf_DXT1; strcpy(swizzle, "000G"); } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; strcpy(swizzle, "000A"); } else { format = tf_L8  ; strcpy(swizzle, "000R"); } break;
				case tfh_8bpc                  :             { format = tf_L8   ; strcpy(swizzle, "000R"); } break; // could also be tf_A8 "000A" or tf_R8 "000R"
				case tfh_16bpc                 :             { format = tf_L16  ; strcpy(swizzle, "000R"); } break; // could also be tf_R16 "000R"
				case tfh_16bpc_float           :             { format = tf_R16F ; strcpy(swizzle, "000R"); } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; strcpy(swizzle, "000R"); } break;
				}
				break;

			case tfcg_A_RGB100: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "100A"); } else { format = tf_DXT1; strcpy(swizzle, "100G"); } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "100A"); } else { format = tf_DXT1; strcpy(swizzle, "100G"); } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; strcpy(swizzle, "100A"); } else { format = tf_L8  ; strcpy(swizzle, "100R"); } break;
				case tfh_8bpc                  :             { format = tf_L8   ; strcpy(swizzle, "100R"); } break; // could also be tf_A8 "100A" or tf_R8 "100R"
				case tfh_16bpc                 :             { format = tf_L16  ; strcpy(swizzle, "100R"); } break; // could also be tf_R16 "100R"
				case tfh_16bpc_float           :             { format = tf_R16F ; strcpy(swizzle, "100R"); } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; strcpy(swizzle, "100R"); } break;
				}
				break;

			case tfcg_A_RGB111: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "111A"); } else { format = tf_DXT1; strcpy(swizzle, "111G"); } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "111A"); } else { format = tf_DXT1; strcpy(swizzle, "111G"); } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; strcpy(swizzle, "111A"); } else { format = tf_L8  ; strcpy(swizzle, "111R"); } break;
				case tfh_8bpc                  :             { format = tf_L8   ; strcpy(swizzle, "111R"); } break; // could also be tf_A8 "111A" or tf_R8 "111R"
				case tfh_16bpc                 :             { format = tf_L16  ; strcpy(swizzle, "111R"); } break; // could also be tf_R16 "111R"
				case tfh_16bpc_float           :             { format = tf_R16F ; strcpy(swizzle, "111R"); } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; strcpy(swizzle, "111R"); } break;
				}
				break;

			case tfcg_LA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenonCTX1) { format = tf_CTX1    ; strcpy(swizzle, "RRRG"); } else { format = tf_DXT1; strcpy(swizzle, "RRRG"); } break;
				case tfh_CompressedHighQuality : if (bXenon)     { format = tf_DXN     ; strcpy(swizzle, "RRRG"); } else { format = tf_DXT5; strcpy(swizzle, "GGGA"); } break;
				case tfh_Quantised             :                 { format = tf_A8L8    ; strcpy(swizzle, "RRRA"); } break; // could also be tf_G8R8 "RRRG"
				case tfh_8bpc                  :                 { format = tf_A8L8    ; strcpy(swizzle, "RRRA"); } break; // could also be tf_G8R8 "RRRG"
				case tfh_16bpc                 :                 { format = tf_G16R16  ; strcpy(swizzle, "RRRG"); } break;
				case tfh_16bpc_float           :                 { format = tf_G16R16F ; strcpy(swizzle, "RRRG"); } break;
				case tfh_32bpc_float           :                 { format = tf_G32R32F ; strcpy(swizzle, "RRRG"); } break;
				}
				break;

			case tfcg_RG: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenonCTX1) { format = tf_CTX1   ; strcpy(swizzle, "RG01"); } else { format = tf_DXT1; strcpy(swizzle, "RG01"); } break;
				case tfh_CompressedHighQuality : if (bXenon)     { format = tf_DXN    ; strcpy(swizzle, "RG01"); } else { format = tf_DXT5; strcpy(swizzle, "GA01"); } break;
				case tfh_Quantised             :                 { format = tf_A8L8   ; strcpy(swizzle, "RA01"); } break; // could also be tf_G8R8 "RG01"
				case tfh_8bpc                  :                 { format = tf_A8L8   ; strcpy(swizzle, "RA01"); } break; // could also be tf_G8R8 "RG01"
				case tfh_16bpc                 :                 { format = tf_G16R16 ; strcpy(swizzle, "RG01"); } break;
				case tfh_16bpc_float           :                 { format = tf_G16R16F; strcpy(swizzle, "RG01"); } break;
				case tfh_32bpc_float           :                 { format = tf_G32R32F; strcpy(swizzle, "RG01"); } break;
				}
				break;

			case tfcg_RG_B1: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenonCTX1) { format = tf_CTX1   ; strcpy(swizzle, "RG11"); } else { format = tf_DXT1; strcpy(swizzle, "RG11"); } break;
				case tfh_CompressedHighQuality : if (bXenon)     { format = tf_DXN    ; strcpy(swizzle, "RG11"); } else { format = tf_DXT5; strcpy(swizzle, "GA11"); } break;
				case tfh_Quantised             :                 { format = tf_A8L8   ; strcpy(swizzle, "RA11"); } break; // could also be tf_G8R8 "RG11"
				case tfh_8bpc                  :                 { format = tf_A8L8   ; strcpy(swizzle, "RA11"); } break; // could also be tf_G8R8 "RG11"
				case tfh_16bpc                 :                 { format = tf_G16R16 ; strcpy(swizzle, "RG11"); } break;
				case tfh_16bpc_float           :                 { format = tf_G16R16F; strcpy(swizzle, "RG11"); } break;
				case tfh_32bpc_float           :                 { format = tf_G32R32F; strcpy(swizzle, "RG11"); } break;
				}
				break;

			case tfcg_R: switch (m_texFormatHint)
				{
				case tfh_Compressed            : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "A001"); } else { format = tf_DXT1; strcpy(swizzle, "G001"); } break;
				case tfh_CompressedHighQuality : if (bXenon) { format = tf_DXT5A; strcpy(swizzle, "A001"); } else { format = tf_DXT5; strcpy(swizzle, "A001"); } break;
				case tfh_Quantised             : if (bXenon) { format = tf_DXT3A; strcpy(swizzle, "A001"); } else { format = tf_L8  ; strcpy(swizzle, "R001"); } break;
				case tfh_8bpc                  :             { format = tf_L8   ; strcpy(swizzle, "R001"); } break;
				case tfh_16bpc                 :             { format = tf_L16  ; strcpy(swizzle, "R001"); } break;
				case tfh_16bpc_float           :             { format = tf_R16F ; strcpy(swizzle, "R001"); } break;
				case tfh_32bpc_float           :             { format = tf_R32F ; strcpy(swizzle, "R001"); } break;
				}
				break;
			}
		}
		else*/
		if (PLATFORM == platform::ORBIS)
		{
			switch (channelGroup)
			{
			case tfcg_RGB: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_R5G6B5       ; break;
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_RGB_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_A1R5G5B5     ; break; // alpha does not require precision
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_RGBA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_A4R4G4B4     ; break;
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_L: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; strcpy(swizzle, "RRR1"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5A; strcpy(swizzle, "RRR1"); break;
				case tfh_Quantised             : format = tf_R8   ; strcpy(swizzle, "RRR1"); break;
				case tfh_8bpc                  : format = tf_R8   ; strcpy(swizzle, "RRR1"); break;
				case tfh_16bpc                 : format = tf_R16  ; strcpy(swizzle, "RRR1"); break;
				case tfh_16bpc_float           : format = tf_R16F ; strcpy(swizzle, "RRR1"); break;
				case tfh_32bpc_float           : format = tf_R32F ; strcpy(swizzle, "RRR1"); break;
				}
				break;

			case tfcg_L_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; strcpy(swizzle, "RRR0"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5A; strcpy(swizzle, "RRR0"); break;
				case tfh_Quantised             : format = tf_DXT3 ; strcpy(swizzle, "AAA0"); break; // alpha channel is 4-bit, this is not very efficient though
				case tfh_8bpc                  : format = tf_R8   ; strcpy(swizzle, "RRR0"); break;
				case tfh_16bpc                 : format = tf_R16  ; strcpy(swizzle, "RRR0"); break;
				case tfh_16bpc_float           : format = tf_R16F ; strcpy(swizzle, "RRR0"); break;
				case tfh_32bpc_float           : format = tf_R32F ; strcpy(swizzle, "RRR0"); break;
				}
				break;

			case tfcg_A: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; strcpy(swizzle, "000R"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5A; strcpy(swizzle, "000R"); break;
				case tfh_Quantised             : format = tf_A8   ;                          break;
				case tfh_8bpc                  : format = tf_R8   ; strcpy(swizzle, "000R"); break;
				case tfh_16bpc                 : format = tf_R16  ; strcpy(swizzle, "000R"); break;
				case tfh_16bpc_float           : format = tf_R16F ; strcpy(swizzle, "000R"); break;
				case tfh_32bpc_float           : format = tf_R32F ; strcpy(swizzle, "000R"); break;
				}
				break;

			case tfcg_A_RGB100: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; strcpy(swizzle, "100R"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5A; strcpy(swizzle, "100R"); break;
				case tfh_Quantised             : format = tf_DXT3 ; strcpy(swizzle, "100A"); break; // alpha channel is 4-bit, this is not very efficient though
				case tfh_8bpc                  : format = tf_R8   ; strcpy(swizzle, "100R"); break;
				case tfh_16bpc                 : format = tf_R16  ; strcpy(swizzle, "100R"); break;
				case tfh_16bpc_float           : format = tf_R16F ; strcpy(swizzle, "100R"); break;
				case tfh_32bpc_float           : format = tf_R32F ; strcpy(swizzle, "100R"); break;
				}
				break;

			case tfcg_A_RGB111: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; strcpy(swizzle, "111R"); break;
				case tfh_CompressedHighQuality : format = tf_DXT5A; strcpy(swizzle, "111R"); break;
				case tfh_Quantised             : format = tf_DXT3 ; strcpy(swizzle, "111A"); break; // alpha channel is 4-bit, this is not very efficient though
				case tfh_8bpc                  : format = tf_R8   ; strcpy(swizzle, "111R"); break;
				case tfh_16bpc                 : format = tf_R16  ; strcpy(swizzle, "111R"); break;
				case tfh_16bpc_float           : format = tf_R16F ; strcpy(swizzle, "111R"); break;
				case tfh_32bpc_float           : format = tf_R32F ; strcpy(swizzle, "111R"); break;
				}
				break;

			case tfcg_LA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1   ; strcpy(swizzle, "RRRG"); break;
				case tfh_CompressedHighQuality : format = tf_DXN    ; strcpy(swizzle, "RRRG"); break;
				case tfh_Quantised             : format = tf_G8R8   ; strcpy(swizzle, "RRRG"); break; // TODO -- use 44 format on orbis
				case tfh_8bpc                  : format = tf_G8R8   ; strcpy(swizzle, "RRRG"); break;
				case tfh_16bpc                 : format = tf_G16R16 ; strcpy(swizzle, "RRRG"); break;
				case tfh_16bpc_float           : format = tf_G16R16F; strcpy(swizzle, "RRRG"); break;
				case tfh_32bpc_float           : format = tf_G32R32F; strcpy(swizzle, "RRRG"); break;
				}
				break;

			case tfcg_RG: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1   ; break;
				case tfh_CompressedHighQuality : format = tf_DXN    ; break;
				case tfh_Quantised             : format = tf_G8R8   ; break; // TODO -- use 44 format on orbis
				case tfh_8bpc                  : format = tf_G8R8   ; break;
				case tfh_16bpc                 : format = tf_G16R16 ; break;
				case tfh_16bpc_float           : format = tf_G16R16F; break;
				case tfh_32bpc_float           : format = tf_G32R32F; break;
				}
				break;

			case tfcg_RG_B1: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1   ; strcpy(swizzle, "RG11"); break;
				case tfh_CompressedHighQuality : format = tf_DXN    ; strcpy(swizzle, "RG11"); break; // note that this will _not_ be used for high quality normal maps (it should select tfcg_RG instead)
				case tfh_Quantised             : format = tf_G8R8   ; strcpy(swizzle, "RG11"); break;
				case tfh_8bpc                  : format = tf_G8R8   ; strcpy(swizzle, "RG11"); break;
				case tfh_16bpc                 : format = tf_G16R16 ; strcpy(swizzle, "RG11"); break;
				case tfh_16bpc_float           : format = tf_G16R16F; strcpy(swizzle, "RG11"); break;
				case tfh_32bpc_float           : format = tf_G32R32F; strcpy(swizzle, "RG11"); break;
				}
				break;

			case tfcg_R: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; break; // DXT5A is BC4, which comes through as "R001"
				case tfh_CompressedHighQuality : format = tf_DXT5A; break; // DXT5A is BC4, which comes through as "R001"
				case tfh_Quantised             : format = tf_R8   ; break;
				case tfh_8bpc                  : format = tf_R8   ; break;
				case tfh_16bpc                 : format = tf_R16  ; break;
				case tfh_16bpc_float           : format = tf_R16F ; break;
				case tfh_32bpc_float           : format = tf_R32F ; break;
				}
				break;
			}
		}
		else // no support for swizzle
		{
			switch (channelGroup)
			{
			case tfcg_RGB: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_R5G6B5       ; break;
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_RGB_A0: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_A1R5G5B5     ; break; // alpha does not require precision
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_RGBA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // TODO -- use BC7
				case tfh_Quantised             : format = tf_A8B8G8R8     ; break; // ouch .. no 4444 support, use 8888 instead
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break;
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break;
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break;
				}
				break;

			case tfcg_L: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break; // low quality, would be better to use DXT5A but need to swizzle
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // low quality, would be better to use DXT5A but need to swizzle
				case tfh_Quantised             : format = tf_A1R5G5B5     ; break; // low quality, would be better to use R8 or A8 but need to swizzle
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // ouch .. would be better to use R8 or A8 but need to swizzle
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_A: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_A8           ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_CompressedHighQuality : format = tf_A8           ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_Quantised             : format = tf_A8           ; break;
				case tfh_8bpc                  : format = tf_A8           ; break;
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_A_RGB100:
			case tfcg_A_RGB111: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A but need to swizzle
				case tfh_Quantised             : format = tf_DXT3         ; break; // ouch .. alpha channel is 4-bit, this is not very efficient though
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // ouch .. would be better to use R8 or A8 but need to swizzle
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_L_A0:
			case tfcg_LA: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A (DXT1 for LA) but need to swizzle
				case tfh_CompressedHighQuality : format = tf_DXT5         ; break; // ouch .. would be better to use DXT5A (DXN for LA) but need to swizzle
				case tfh_Quantised             : format = tf_DXT3         ; break; // note that luminance is actually DXT compressed, not 'quantised' .. oh well
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // ouch .. would be better to use R8 or A8 but need to swizzle
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // ouch .. would be better to use R16 but need to swizzle
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // ouch .. would be better to use R16F but need to swizzle
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // ouch .. would be better to use R32F but need to swizzle
				}
				break;

			case tfcg_RG: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1   ; break;
				case tfh_CompressedHighQuality : format = tf_DXN    ; break;
				case tfh_Quantised             : format = tf_G8R8   ; break; // too bad there's no G4R4
				case tfh_8bpc                  : format = tf_G8R8   ; break;
				case tfh_16bpc                 : format = tf_G16R16 ; break;
				case tfh_16bpc_float           : format = tf_G16R16F; break;
				case tfh_32bpc_float           : format = tf_G32R32F; break;
				}
				break;

			case tfcg_RG_B1: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT1         ; break;
				case tfh_CompressedHighQuality : format = tf_DXT1         ; break; // note that this will _not_ be used for high quality normal maps (it should select tfcg_RG instead)
				case tfh_Quantised             : format = tf_R5G6B5       ; break; // can't use G8R8 since that would imply blue=0 ..
				case tfh_8bpc                  : format = tf_A8B8G8R8     ; break; // can't use G8R8 since that would imply blue=0 ..
				case tfh_16bpc                 : format = tf_A16B16G16R16 ; break; // can't use G16R16 since that would imply blue=0 ..
				case tfh_16bpc_float           : format = tf_A16B16G16R16F; break; // can't use G16R16F since that would imply blue=0 ..
				case tfh_32bpc_float           : format = tf_A32B32G32R32F; break; // can't use G32R32F since that would imply blue=0 ..
				}
				break;

			case tfcg_R: switch (m_texFormatHint)
				{
				case tfh_Compressed            : format = tf_DXT5A; break; // DXT5A is BC4, which comes through as "R001"
				case tfh_CompressedHighQuality : format = tf_DXT5A; break; // DXT5A is BC4, which comes through as "R001"
				case tfh_Quantised             : format = tf_R8   ; break;
				case tfh_8bpc                  : format = tf_R8   ; break;
				case tfh_16bpc                 : format = tf_R16  ; break;
				case tfh_16bpc_float           : format = tf_R16F ; break;
				case tfh_32bpc_float           : format = tf_R32F ; break;
				}
				break;
			}
		}

		do // special formats (1555, DXT3, DXT1a, etc.)
		{
			if (bAlphaIs1bit)
			{
				if (format == tf_A4R4G4B4)
				{
					format = tf_A1R5G5B5; // better precision in RGB
					break;
				}
			}

			if (bAlphaIs1bit && bAlphaIsBlack && strcmp(swizzle, "RGBA") == 0)
			{
				if (format == tf_DXT3 || format == tf_DXT5)
				{
					format = tf_DXT1; // equivalent, and uses half the memory
					strcpy(swizzle, "RGBA*"); // not very elegant way to indicate DXT1 with alpha is being used, so that XGCompressSurface can be passed an appropriate AlphaRef
					break;
				}
			}

			if (m_texFormatHintUseDXT3)
			{
				if (format == tf_DXT5)
				{
					format = tf_DXT3;
					break;
				}
				//else if (format == tf_DXT5A && PLATFORM == platform::XENON)
				//{
				//	format = tf_DXT3A;
				//	break;
				//}
			}

			if (m_texFormatHintUse555)
			{
				if (format == tf_R5G6B5)
				{
					format = tf_A1R5G5B5; // force 555 for better monochrome values
					break;
				}
			}
		}
		while (false);
	}

	if (m_texFormatOverrideSwizzle.length() >= 4)
	{
		strcpy(swizzle, m_texFormatOverrideSwizzle.c_str());
	}

	CompileTimeAssert(grcImage::FORMAT_COUNT == tf_COUNT);
	grcImage::Format imageFormat = (grcImage::Format)format;

	if (imageFormat == grcImage::UNKNOWN)
	{
		imageFormat = grcImage::A8B8G8R8;
		strcpy(swizzle, "RGBA");
	}

	return imageFormat;
}

#if RSG_PC
static void AutoOpenFunc(void* data)
{
	if (data)
	{
		char* str = (char*)data;
		system(str);
		delete[] str;
	}

	//exit(0);
}

static void AutoOpen(const char* str, bool bThreaded = false) // TODO -- wtf, threaded version only seems to work from the debugger?
{
	if (bThreaded)
	{
		const int len = istrlen(str);
		char* buffer = rage_new char[len + 1];
		sysMemCpy(buffer, str, len + 1);

		sysIpcCreateThread(AutoOpenFunc, buffer, 1024, PRIO_NORMAL, "AutoOpen");
	}
	else
	{
		system(str);
	}
}
#else
#define AutoOpen(str,bThreaded)
#endif

grcImage::Format fwTextureConversion::PreConvert(fwImage** mipChain, int mipCount, const fwTextureFormatHelper& helper, char swizzle[6]) const
{
	Assert(mipCount > 0);

	grcImage::Format format = ChooseTextureFormat(helper, swizzle);
	grcImage::Format formatFlags = format;

	bool sRGB = m_sRGB;

	if (sRGB)
	{
		if (swizzle[0] == 'A' || swizzle[1] == 'A' || swizzle[2] == 'A' ||
			swizzle[3] == 'R' || swizzle[3] == 'G' || swizzle[3] == 'B')
		{
			sRGB = false; // can't use sRGB if RGB and ALPHA are sharing channels
		}

		if (format != grcImage::DXT1 &&
			format != grcImage::DXT3 &&
			format != grcImage::DXT5 &&
			format != grcImage::BC7 &&
		//	format != grcImage::R5G6B5 &&
		//	format != grcImage::A1R5G5B5 &&
		//	format != grcImage::A4R4G4B4 &&
			format != grcImage::A8R8G8B8 &&
			format != grcImage::A8B8G8R8)
		{
			sRGB = false; // can't use sRGB unless format is compatible
		}
	}

	if (sRGB)
	{
		formatFlags = (grcImage::Format)(formatFlags | (u32)grcImage::FORMAT_FLAG_sRGB);
	}

	if (m_linear) // <-- force this to 1 to make all textures linear, useful for GetPixel test (also need to force bLinear = true in grcTextureGCM::Init, etc.)
	{
		formatFlags = (grcImage::Format)(formatFlags | (u32)grcImage::FORMAT_FLAG_LINEAR);
	}

	if (m_sysmem)
	{
		formatFlags = (grcImage::Format)(formatFlags | (u32)grcImage::FORMAT_FLAG_SYSMEM);
	}

	for (int i = 0; i < mipCount; i++)
	{
		const fwImage& mip = *mipChain[i];

		const int w = mip.m_w;
		const int h = mip.m_h;

		if (m_texFormatUsage == tfu_NormalMap) // force blue channel to 1.0f for slightly better compression quality, since it will be reconstructed anyway
		{
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					mip.GetPixel(x, y).z = 1.0f;
				}
			}
		}

		ImagePreSwizzle(mip, swizzle); // apply preswizzle, immediately before conversion/compression

		if (sRGB)
		{
			LinearToGamma(mip, PLATFORM);
		}
	}

	return formatFlags;
}

grcImage* fwTextureConversion::Convert(fwImage** mipChain, int mipCount, grcImage::Format formatFlags, bool bForceContiguousMips) const
{
	(void)bForceContiguousMips;

	// discard top mips if requested, but we don't want to scale by a non-uniform amount ..
	int topMipDiscard = m_imageTopMipDiscard;

	while ((Min<int>(mipChain[0]->m_w, mipChain[0]->m_h) >> topMipDiscard) < 4)
	{
		topMipDiscard--;
	}

	const int w = mipChain[0]->m_w >> topMipDiscard;
	const int h = mipChain[0]->m_h >> topMipDiscard;
	grcImage* image = grcImage::Create(w, h, 1, formatFlags, grcImage::STANDARD, mipCount - 1 - topMipDiscard, 0);
	grcImage* mip = image;

	for (int i = topMipDiscard; i < mipCount; i++)
	{
		mipChain[i]->ConvertTo(mip);
		mip = mip->GetNext();
	}

	return image;
}

// ================================================================================================

fwTextureConversionParams::fwTextureConversionParams()
{
	// clear everything but the vtable ..
#if RSG_ORBIS
	sysMemSet(&m_FIRST_MEMBER, 0, (ptrdiff_t)((const char*)&m_LAST_MEMBER - (const char*)&m_FIRST_MEMBER) + sizeof(m_LAST_MEMBER));
#else
	sysMemSet(&m_FIRST_MEMBER, 0, offsetof(fwTextureConversionParams, m_LAST_MEMBER) - offsetof(fwTextureConversionParams, m_FIRST_MEMBER) + sizeof(m_LAST_MEMBER));
#endif
}

static void BlendImage(fwImage* imageA, const fwImage* imageB, float blend)
{
	const int w = imageA->m_w;
	const int h = imageA->m_h;

	if (w != imageB->m_w ||
		h != imageB->m_h)
	{
		return;
	}

	if (blend >= 1.0f)
	{
		sysMemCpy(&imageA->GetPixel(0, 0), &imageB->GetPixel(0, 0), w*h*sizeof(Vector4));
	}
	else if (blend > 0.0f)
	{
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				Vector4      & c0 = imageA->GetPixel(x, y);
				Vector4 const& c1 = imageB->GetPixel(x, y);

				c0 += (c1 - c0)*blend;
			}
		}
	}
}

static void DeleteMipChain(fwImage**& mipChain, int mipCount)
{
	if (mipChain)
	{
		for (int i = 0; i < mipCount; i++)
		{
			if (mipChain[i])
			{
				delete mipChain[i];
			}
		}

		delete[] mipChain;
	}

	mipChain = NULL;
}

// TODO -- check if this still makes sense with rdr templates
u16 fwTextureConversionParams::GetTemplateTypeFromTemplatePath(const char* templatePath)
{
	u16 templateType = TEXTURE_TEMPLATE_TYPE_UNKNOWN;

	// NOTE -- the order of these tests is designed to catch cases where multiple keywords are used
	// e.g. "diffusedetail" is tested before "diffuse" and "detail"
	if      (stristr(templatePath, "default"             )) { templateType = TEXTURE_TEMPLATE_TYPE_DEFAULT           ; }
	else if (stristr(templatePath, "terrain"             )) { templateType = TEXTURE_TEMPLATE_TYPE_TERRAIN           ; }
	else if (stristr(templatePath, "clouddensity"        )) { templateType = TEXTURE_TEMPLATE_TYPE_CLOUDDENSITY      ; }
	else if (stristr(templatePath, "cloudnormal"         )) { templateType = TEXTURE_TEMPLATE_TYPE_CLOUDNORMAL       ; }
	else if (stristr(templatePath, "cable"               )) { templateType = TEXTURE_TEMPLATE_TYPE_CABLE             ; }
	else if (stristr(templatePath, "fence"               )) { templateType = TEXTURE_TEMPLATE_TYPE_FENCE             ; }
	else if (stristr(templatePath, "enveff"              )) { templateType = TEXTURE_TEMPLATE_TYPE_ENVEFF            ; }
	else if (stristr(templatePath, "script"              )) { templateType = TEXTURE_TEMPLATE_TYPE_SCRIPT            ; }
	else if (stristr(templatePath, "waterflow"           )) { templateType = TEXTURE_TEMPLATE_TYPE_WATERFLOW         ; }
	else if (stristr(templatePath, "waterfoam"           )) { templateType = TEXTURE_TEMPLATE_TYPE_WATERFOAM         ; }
	else if (stristr(templatePath, "waterfog"            )) { templateType = TEXTURE_TEMPLATE_TYPE_WATERFOG          ; }
	else if (stristr(templatePath, "waterocean"          )) { templateType = TEXTURE_TEMPLATE_TYPE_WATEROCEAN        ; }
	else if (stristr(templatePath, "water"               )) { templateType = TEXTURE_TEMPLATE_TYPE_WATER             ; }
	else if (stristr(templatePath, "foamopacity"         )) { templateType = TEXTURE_TEMPLATE_TYPE_FOAMOPACITY       ; }
	else if (stristr(templatePath, "foam"                )) { templateType = TEXTURE_TEMPLATE_TYPE_FOAM              ; }
	else if (stristr(templatePath, "diffusemipsharpen"   )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEMIPSHARPEN ; }
	else if (stristr(templatePath, "diffusedetail"       )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEDETAIL     ; }
	else if (stristr(templatePath, "diffuse_dark"        )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEDARK       ; }
	else if (stristr(templatePath, "diffuse_alpha_opaque")) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSEALPHAOPAQUE; }
	else if (stristr(templatePath, "diffuse"             )) { templateType = TEXTURE_TEMPLATE_TYPE_DIFFUSE           ; }
	else if (stristr(templatePath, "detail"              )) { templateType = TEXTURE_TEMPLATE_TYPE_DETAIL            ; }
	else if (stristr(templatePath, "normal"              )) { templateType = TEXTURE_TEMPLATE_TYPE_NORMAL            ; }
	else if (stristr(templatePath, "specular"            )) { templateType = TEXTURE_TEMPLATE_TYPE_SPECULAR          ; }
	else if (stristr(templatePath, "emissive"            )) { templateType = TEXTURE_TEMPLATE_TYPE_EMISSIVE          ; }
	else if (stristr(templatePath, "tintpalette"         )) { templateType = TEXTURE_TEMPLATE_TYPE_TINTPALETTE       ; }
	else if (stristr(templatePath, "skipprocessing"      )) { templateType = TEXTURE_TEMPLATE_TYPE_SKIPPROCESSING    ; }
	else if (stristr(templatePath, "donotoptimize"       )) { templateType = TEXTURE_TEMPLATE_TYPE_DONOTOPTIMIZE     ; }
	else if (stristr(templatePath, "test"                )) { templateType = TEXTURE_TEMPLATE_TYPE_TEST              ; } // shouldn't be using these in the final assets

	if (stristr(templatePath, "nothalf") || stristr(templatePath, "not_half")) { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_NOT_HALF ; }
	if (stristr(templatePath, "hdsplit") || stristr(templatePath, "hd_split")) { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_HD_SPLIT ; }
	if (stristr(templatePath, "/FULL_"))                                       { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_FULL     ; }
	if (stristr(templatePath, "/maps_half/"))                                  { templateType |= TEXTURE_TEMPLATE_TYPE_FLAG_MAPS_HALF; }

	return templateType | TEXTURE_TEMPLATE_TYPE_VERSION_CURRENT;
}

template <typename DXT_BlockType> static void fwTextureConversionParams__OptimiseCompressed_GetRange(
	u32& minR, u32& minG, u32& minB, u32& minA,
	u32& maxR, u32& maxG, u32& maxB, u32& maxA,
	const grcImage* image)
{
	const int w = (image->GetWidth () + 3)/4;
	const int h = (image->GetHeight() + 3)/4;

	const DXT_BlockType* blocks = (const DXT_BlockType*)image->GetBits();

	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			DXT::ARGB8888 blockPixels[4*4];
			blocks->Decompress(blockPixels);
			blocks++;

			for (int j = 0; j < 4*4; j++)
			{
				const DXT::ARGB8888 p = blockPixels[j];

				minR = Min<u32>(p.r, minR);
				minG = Min<u32>(p.g, minG);
				minB = Min<u32>(p.b, minB);
				minA = Min<u32>(p.a, minA);

				maxR = Max<u32>(p.r, maxR);
				maxG = Max<u32>(p.g, maxG);
				maxB = Max<u32>(p.b, maxB);
				maxA = Max<u32>(p.a, maxA);
			}
		}
	}
}

bool fwTextureConversionParams::OptimiseCompressed(grcImage*& image, char swizzle[6], u8& conversionFlags)
{
	if (image->GetType() != grcImage::STANDARD) // for now, only optimise standard 2D images
	{
		return false;
	}

	if (image->GetFormat() == grcImage::DXT1 ||
		image->GetFormat() == grcImage::DXT3 ||
		image->GetFormat() == grcImage::DXT5)
	{
		const int   origWidth  = image->GetWidth();
		const int   origHeight = image->GetHeight();
		const char* origFormat = grcImage::GetFormatString(image->GetFormat());

		u32 minR = 255, maxR = 0;
		u32 minG = 255, maxG = 0;
		u32 minB = 255, maxB = 0;
		u32 minA = 255, maxA = 0;

		for (grcImage* mip = image; mip; mip = mip->GetNext())
		{
			switch ((int)image->GetFormat())
			{
			case grcImage::DXT1: fwTextureConversionParams__OptimiseCompressed_GetRange<DXT::DXT1_BLOCK>(minR, minG, minB, minA, maxR, maxG, maxB, maxA, mip); break;
			case grcImage::DXT3: fwTextureConversionParams__OptimiseCompressed_GetRange<DXT::DXT3_BLOCK>(minR, minG, minB, minA, maxR, maxG, maxB, maxA, mip); break;
			case grcImage::DXT5: fwTextureConversionParams__OptimiseCompressed_GetRange<DXT::DXT5_BLOCK>(minR, minG, minB, minA, maxR, maxG, maxB, maxA, mip); break;
			}
		}

		bool bHasAlphaBlock          = (image->GetFormat() == grcImage::DXT3 || image->GetFormat() == grcImage::DXT5);
		bool bSupportTextureSwizzle  = (PLATFORM == platform::XENON || PLATFORM == platform::PS3 || PLATFORM == platform::ORBIS); // TODO -- support swizzle on xb1 when we have it
	//	bool bSupportTexFormatDXT3A  = (PLATFORM == platform::XENON);
		bool bSupportTexFormatDXT5A  = (PLATFORM == platform::XENON || bSupportTextureSwizzle);
		bool bConvertToDXT1_alpha_0  = (bHasAlphaBlock && minA == maxA && minA == 0 && bSupportTextureSwizzle);
		bool bConvertToDXT1_alpha_1  = (bHasAlphaBlock && minA == maxA && minA == 255);
	//	bool bConvertToDXT3A_rgb_000 = false;
	//	bool bConvertToDXT3A_rgb_111 = false;
		bool bConvertToDXT5A_rgb_000 = false;
		bool bConvertToDXT5A_rgb_111 = false;
		bool bConvertTo4x4           = false;

		if (minR == maxR && minG == maxG && minB == maxB)
		{
			if (minR == 0 && minG == 0 && minB == 0)
			{
				/*if (image->GetFormat() == grcImage::DXT3 && bSupportTexFormatDXT3A)
				{
					bConvertToDXT3A_rgb_000 = true;
				}
				else*/ if (image->GetFormat() == grcImage::DXT5 && bSupportTexFormatDXT5A)
				{
					bConvertToDXT5A_rgb_000 = true;
				}
			}
			else if (minR == 255 && minG == 255 && minB == 255 && bSupportTextureSwizzle)
			{
				/*if (image->GetFormat() == grcImage::DXT3 && bSupportTexFormatDXT3A)
				{
					bConvertToDXT3A_rgb_111 = true;
				}
				else*/ if (image->GetFormat() == grcImage::DXT5 && bSupportTexFormatDXT5A)
				{
					bConvertToDXT5A_rgb_111 = true;
				}
			}

			if (minA == maxA && Min<int>(image->GetWidth(), image->GetHeight()) > 4) // no variation
			{
				bConvertTo4x4 = true;
			}
		}

		if (bConvertToDXT1_alpha_0 || bConvertToDXT1_alpha_1)
		{
			grcImage* image2 = NULL;

			if (bConvertTo4x4)
			{
				image2 = grcImage::Create(4, 4, image->GetDepth(), grcImage::DXT1, grcImage::STANDARD, 0, 0);
			}
			else
			{
				image2 = grcImage::Create(image->GetWidth(), image->GetHeight(), image->GetDepth(), grcImage::DXT1, grcImage::STANDARD, image->GetExtraMipCount(), 0);
			}

			for (grcImage* mip1 = image, *mip2 = image2; mip2; mip1 = mip1->GetNext(), mip2 = mip2->GetNext())
			{
				const int w = bConvertTo4x4 ? 1 : (mip1->GetWidth () + 3)/4;
				const int h = bConvertTo4x4 ? 1 : (mip1->GetHeight() + 3)/4;

				DXT::DXT5_BLOCK* src = (DXT::DXT5_BLOCK*)mip1->GetBits();
				DXT::DXT1_BLOCK* dst = (DXT::DXT1_BLOCK*)mip2->GetBits();

				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						*(dst++) = (src++)->m_colour;
					}
				}
			}

			image->Release();
			image = image2;

			if (bConvertToDXT1_alpha_0)
			{
				strcpy(swizzle, "RGB0");

				if (bConvertTo4x4)
				{
					fwtexconvLog("optimised %dx%d %s to 4x4 DXT1 alpha=0 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					fwtexconvLog("optimised %dx%d %s to DXT1 alpha=0", origWidth, origHeight, origFormat);
				}
			}
			else if (bConvertToDXT1_alpha_1)
			{
				if (bSupportTextureSwizzle)
				{
					strcpy(swizzle, "RGB1");
				}

				if (bConvertTo4x4)
				{
					fwtexconvLog("optimised %dx%d %s to 4x4 DXT1 alpha=1 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					fwtexconvLog("optimised %dx%d %s to DXT1 alpha=1", origWidth, origHeight, origFormat);
				}
			}

			conversionFlags |= TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT;
			return true;
		}
		/*else if (bConvertToDXT3A_rgb_000 || bConvertToDXT3A_rgb_111)
		{
			grcImage* image2 = NULL;

			if (bConvertTo4x4)
			{
				image2 = grcImage::Create(4, 4, image->GetDepth(), grcImage::DXT3A, grcImage::STANDARD, 0, 0);
			}
			else
			{
				image2 = grcImage::Create(image->GetWidth(), image->GetHeight(), image->GetDepth(), grcImage::DXT3A, grcImage::STANDARD, image->GetExtraMipCount(), 0);
			}

			for (grcImage* mip1 = image, *mip2 = image2; mip2; mip1 = mip1->GetNext(), mip2 = mip2->GetNext())
			{
				const int w = bConvertTo4x4 ? 1 : (mip1->GetWidth () + 3)/4;
				const int h = bConvertTo4x4 ? 1 : (mip1->GetHeight() + 3)/4;

				DXT::DXT3_BLOCK* src = (DXT::DXT3_BLOCK*)mip1->GetBits();
				DXT::DXT3_ALPHA* dst = (DXT::DXT3_ALPHA*)mip2->GetBits();

				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						*(dst++) = (src++)->m_alpha;
					}
				}
			}

			image->Release();
			image = image2;

			if (bConvertToDXT3A_rgb_000)
			{
				strcpy(swizzle, "000A");

				if (bConvertTo4x4)
				{
					fwtexconvLog("optimised %dx%d %s to 4x4 DXT3A rgb=000 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					fwtexconvLog("optimised %dx%d %s to DXT3A rgb=000", origWidth, origHeight, origFormat);
				}
			}
			else if (bConvertToDXT3A_rgb_111)
			{
				strcpy(swizzle, "111A");

				if (bConvertTo4x4)
				{
					fwtexconvLog("optimised %dx%d %s to 4x4 DXT3A rgb=111 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					fwtexconvLog("optimised %dx%d %s to DXT3A rgb=111", origWidth, origHeight, origFormat);
				}
			}

			conversionFlags |= TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT;
			return true;
		}*/
		else if (bConvertToDXT5A_rgb_000 || bConvertToDXT5A_rgb_111)
		{
			grcImage* image2 = NULL;

			if (bConvertTo4x4)
			{
				image2 = grcImage::Create(4, 4, image->GetDepth(), grcImage::DXT5A, grcImage::STANDARD, 0, 0);
			}
			else
			{
				image2 = grcImage::Create(image->GetWidth(), image->GetHeight(), image->GetDepth(), grcImage::DXT5A, grcImage::STANDARD, image->GetExtraMipCount(), 0);
			}

			for (grcImage* mip1 = image, *mip2 = image2; mip2; mip1 = mip1->GetNext(), mip2 = mip2->GetNext())
			{
				const int w = bConvertTo4x4 ? 1 : (mip1->GetWidth () + 3)/4;
				const int h = bConvertTo4x4 ? 1 : (mip1->GetHeight() + 3)/4;

				DXT::DXT5_BLOCK* src = (DXT::DXT5_BLOCK*)mip1->GetBits();
				DXT::DXT5_ALPHA* dst = (DXT::DXT5_ALPHA*)mip2->GetBits();

				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						*(dst++) = (src++)->m_alpha;
					}
				}
			}

			image->Release();
			image = image2;

			if (bConvertToDXT5A_rgb_000)
			{
				strcpy(swizzle, "000R");

				if (bConvertTo4x4)
				{
					fwtexconvLog("optimised %dx%d %s to 4x4 DXT5A rgb=000 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					fwtexconvLog("optimised %dx%d %s to DXT5A rgb=000", origWidth, origHeight, origFormat);
				}
			}
			else if (bConvertToDXT5A_rgb_111)
			{
				strcpy(swizzle, "111R");

				if (bConvertTo4x4)
				{
					fwtexconvLog("optimised %dx%d %s to 4x4 DXT5A rgb=111 (no variation)", origWidth, origHeight, origFormat);
				}
				else
				{
					fwtexconvLog("optimised %dx%d %s to DXT5A rgb=111", origWidth, origHeight, origFormat);
				}
			}

			conversionFlags |= TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT;
			return true;
		}
		else if (bConvertTo4x4)
		{
			grcImage* image2 = grcImage::Create(4, 4, image->GetDepth(), image->GetFormat(), grcImage::STANDARD, 0, 0);
			const int bitsPerBlock = 4*4*grcImage::GetFormatBitsPerPixel(image->GetFormat());

			sysMemCpy(image2->GetBits(), image->GetBits(), bitsPerBlock/8);

			image->Release();
			image = image2;

			strcpy(swizzle, "RGBA");
			fwtexconvLog("optimised %dx%d %s to 4x4 (no variation)", origWidth, origHeight, origFormat);

			conversionFlags |= TEXTURE_CONVERSION_FLAG_OPTIMISED_DXT;
			return true;
		}
	}

	return false;
}

static bool IsPowerOfTwo(int x)
{
	return x > 0 && ((x - 1)&x) == 0;
}

static int PowerOfTwoRoundDown(int x) // returns 0 if x is 0
{
	int y = 1;

	while (y < x)
	{
		y *= 2;
	}

	if (y > x)
	{
		y /= 2;
	}

	return y;
}

static void FinaliseConversionParams(fwTextureConversionParams& p)
{
	// m_imageDownsampleScale should be a power of 2, but let's enforce it
	if (p.m_imageDownsampleScale  > 0 && !IsPowerOfTwo(p.m_imageDownsampleScale )) { p.m_imageDownsampleScale  = PowerOfTwoRoundDown(p.m_imageDownsampleScale ); }
	if (p.m_imageDownsampleScaleX > 0 && !IsPowerOfTwo(p.m_imageDownsampleScaleX)) { p.m_imageDownsampleScaleX = PowerOfTwoRoundDown(p.m_imageDownsampleScaleX); }
	if (p.m_imageDownsampleScaleY > 0 && !IsPowerOfTwo(p.m_imageDownsampleScaleY)) { p.m_imageDownsampleScaleY = PowerOfTwoRoundDown(p.m_imageDownsampleScaleY); }

	if (p.m_imageSplitHD || p.m_imageSplitHD2)
	{
		p.m_imageTopMipDiscard = false; // TODO -- we should handle this properly
	}

	// Adam said we don't want to reduce the downsampling of HD textures ..
	if (0 && p.m_imageSplitHD)
	{
		if (p.m_imageDownsampleScale  >= 2) { p.m_imageDownsampleScale  /= 2; }
		if (p.m_imageDownsampleScaleX >= 2) { p.m_imageDownsampleScaleX /= 2; }
		if (p.m_imageDownsampleScaleY >= 2) { p.m_imageDownsampleScaleY /= 2; }
	}

	if (p.m_imageDownsampleScale  == 0) { p.m_imageDownsampleScale  = 1; }
	if (p.m_imageDownsampleScaleX == 0) { p.m_imageDownsampleScaleX = p.m_imageDownsampleScale; }
	if (p.m_imageDownsampleScaleY == 0) { p.m_imageDownsampleScaleY = p.m_imageDownsampleScale; }

	if (p.m_imageDownsampleScaleRel  == 0.0f) { p.m_imageDownsampleScaleRel  = 1.0f; }
	if (p.m_imageDownsampleScaleRelX == 0.0f) { p.m_imageDownsampleScaleRelX = p.m_imageDownsampleScaleRel; }
	if (p.m_imageDownsampleScaleRelY == 0.0f) { p.m_imageDownsampleScaleRelY = p.m_imageDownsampleScaleRel; }

	if (p.m_imageMaxSize  == 0) { p.m_imageMaxSize  = 8192; }
	if (p.m_imageMaxSizeX == 0) { p.m_imageMaxSizeX = p.m_imageMaxSize; }
	if (p.m_imageMaxSizeY == 0) { p.m_imageMaxSizeY = p.m_imageMaxSize; }
	if (p.m_imageMaxMips  == 0) { p.m_imageMaxMips  = 16; }

	// apply defaults
	{
		const int defaultMinMinMipSize = (PLATFORM == platform::XENON ? 32 : 4); // default is 4, but can set this down to 1
		const int defaultMinMaxMipSize = (PLATFORM == platform::XENON ? 32 : 4);

		if (p.m_imageMinMinMipSize == 0) { p.m_imageMinMinMipSize = defaultMinMinMipSize; }
		if (p.m_imageMinMaxMipSize == 0) { p.m_imageMinMaxMipSize = defaultMinMaxMipSize; }
	}

	if (p.m_imageMinMaxMipSize == 0)
	{
		p.m_imageMinMaxMipSize = p.m_imageMinMinMipSize;
	}
	else if (p.m_imageMinMinMipSize > p.m_imageMinMaxMipSize)
	{
		Warningf( "Minimum MIP size was greater than maximum MIP size." );
		p.m_imageMinMinMipSize = p.m_imageMinMaxMipSize;
	}

	if (!IsPowerOfTwo(p.m_imageMinMinMipSize))
	{
		p.m_imageMinMinMipSize = PowerOfTwoRoundDown(p.m_imageMinMinMipSize); // warning?
	}

	if (!IsPowerOfTwo(p.m_imageMinMaxMipSize))
	{
		p.m_imageMinMaxMipSize = PowerOfTwoRoundDown(p.m_imageMinMaxMipSize); // warning?
	}

#if MIPMAPS_CANNOT_BE_SMALLER_THAN_4x4
	p.m_imageMinMinMipSize = Max<int>(4, p.m_imageMinMinMipSize);
	p.m_imageMinMaxMipSize = Max<int>(4, p.m_imageMinMaxMipSize);
#else
	p.m_imageMinMinMipSize = Max<int>(1, p.m_imageMinMinMipSize);
	p.m_imageMinMaxMipSize = Max<int>(1, p.m_imageMinMaxMipSize);
#endif

	if (p.m_atlasCubemap4x3Cross)
	{
		if (p.m_atlasCount <= 1) // single cubemap
		{
			p.m_atlasRows = 3;
			p.m_atlasCols = 4;
			p.m_atlasCount = 6;
		}
		else // cubemap array (arranged vertically)
		{
			p.m_atlasRows = 3*p.m_atlasCount;
			p.m_atlasCols = 4;
			p.m_atlasCount = 6*p.m_atlasCount;
		}
	}
	else if (p.m_atlasRows > 0 && p.m_atlasCols > 0)
	{
		if (p.m_atlasCount == 0)
		{
			p.m_atlasCount = p.m_atlasRows*p.m_atlasCols;
		}
		else
		{
			p.m_atlasCount = Min<int>(p.m_atlasRows*p.m_atlasCols, p.m_atlasCount);
		}
	}
	else // not a valid atlas if rows and cols are not both > 0
	{
		p.m_atlasRows = 0;
		p.m_atlasCols = 0;
		p.m_atlasCount = 0;
	}

	if (p.m_downsampleFilterAmount.x == 0.0f &&
		p.m_downsampleFilterAmount.y == 0.0f &&
		p.m_downsampleFilterAmount.z == 0.0f &&
		p.m_downsampleFilterAmount.w == 0.0f &&
		p.m_downsampleFilterCoefficients.GetCount() > 0)
	{
		p.m_downsampleFilterAmount = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (p.m_mipFilterAmount.x == 0.0f &&
		p.m_mipFilterAmount.y == 0.0f &&
		p.m_mipFilterAmount.z == 0.0f &&
		p.m_mipFilterAmount.w == 0.0f &&
		p.m_mipFilterCoefficients.GetCount() > 0)
	{
		p.m_mipFilterAmount = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (p.m_mipFilterSharpenAmount.x == 0.0f &&
		p.m_mipFilterSharpenAmount.y == 0.0f &&
		p.m_mipFilterSharpenAmount.z == 0.0f &&
		p.m_mipFilterSharpenAmount.w == 0.0f &&
		p.m_mipFilterSharpen.GetCount() > 0)
	{
		p.m_mipFilterSharpenAmount = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	if (p.m_mipFilterDesaturateAmount.x == 0.0f &&
		p.m_mipFilterDesaturateAmount.y == 0.0f &&
		p.m_mipFilterDesaturateAmount.z == 0.0f &&
		p.m_mipFilterDesaturate.GetCount() > 0)
	{
		p.m_mipFilterDesaturateAmount = Vector3(1.0f, 1.0f, 1.0f);
	}

	if (p.m_alphaCoverageCompensationFactor > 0.0f)
	{
		if (p.m_alphaCoverageCompensationReference == 0.0f)
		{
			p.m_alphaCoverageCompensationReference = 0.5f; // default
		}

		if (p.m_alphaCoverageCompensationRadius < 0)
		{
			p.m_alphaCoverageCompensationRadius = 0;
		}
		else if (p.m_alphaCoverageCompensationRadius == 0)
		{
			p.m_alphaCoverageCompensationRadius = 3; // default
		}

		if (p.m_alphaCoverageCompensationIterations == 0)
		{
			p.m_alphaCoverageCompensationIterations = 10; // default
		}
	}

	if (p.m_normalMapGlossInAlpha)
	{
		if (p.m_normalMapGlossSpecularMin == 0.0f)
		{
			p.m_normalMapGlossSpecularMin = 1.0f;
		}

		if (p.m_normalMapGlossSpecularMax == 0.0f)
		{
			p.m_normalMapGlossSpecularMax = 8192.0f;
		}

		if (p.m_normalMapGlossSpecularPower == 0.0f)
		{
			p.m_normalMapGlossSpecularPower = p.m_normalMapGlossSpecularMax;
		}

		if (p.m_normalMapGlossAlphaExponent == 0.0f)
		{
			if (p.m_normalMapGlossStoreVarianceAlpha && !p.m_specularMapGlossFromNormalMapVariance)
			{
				p.m_normalMapGlossAlphaExponent = 0.5f; // sqrt before compression for better precision near 0
			}
			else
			{
				p.m_normalMapGlossAlphaExponent = 1.0f;
			}
		}
	}

	if (p.m_mipFadeThresholdMax.x == 0.0f &&
		p.m_mipFadeThresholdMax.y == 0.0f &&
		p.m_mipFadeThresholdMax.z == 0.0f &&
		p.m_mipFadeThresholdMax.w == 0.0f)
	{
		if (p.m_mipFadeThresholdMin.x == 0.0f &&
			p.m_mipFadeThresholdMin.y == 0.0f &&
			p.m_mipFadeThresholdMin.z == 0.0f &&
			p.m_mipFadeThresholdMin.w == 0.0f)
		{
			// if neither thresholds are set, set them to +/-FLT_MAX so we never apply a threshold
			p.m_mipFadeThresholdMin = Vector4(-FLT_MAX, -FLT_MAX, -FLT_MAX, -FLT_MAX);
			p.m_mipFadeThresholdMax = Vector4(+FLT_MAX, +FLT_MAX, +FLT_MAX, +FLT_MAX);
		}
		else
		{
			p.m_mipFadeThresholdMax = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}

	if (p.m_finalMultiplier.x == 0.0f &&
		p.m_finalMultiplier.y == 0.0f &&
		p.m_finalMultiplier.z == 0.0f &&
		p.m_finalMultiplier.w == 0.0f)
	{
		p.m_finalMultiplier = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	}

	// we could also fix stupid values that may have come from the tools .. ideally there would be warnings reported, etc.
}

grcImage* fwTextureConversionParams::Process(const grcImage* image, const char* filename, char swizzle[6], u8& conversionFlags, const fwTextureConversionParams::SecondaryInputs* secondaryInputs, fwTextureConversionParams::SecondaryOutputs* secondaryOutputs) const
{
	fwTextureConversion p = *static_cast<const fwTextureConversion*>(this);
	FinaliseConversionParams(p);
	return p.Process(image, filename, swizzle, conversionFlags, secondaryInputs, secondaryOutputs);
}

static void CopyCubemapFaceSetup(int& ox, int& oy, int& dxx, int& dxy, int& dyx, int& dyy, int faceSize, int faceIndex)
{
	//         +-------+
	//         |face:5 |
	//         |dir=-z |
	//         |rot=0  |
	// +-------+-------+-------+-------+
	// |face:0 |face:3 |face:1 |face:2 |
	// |dir=+x |dir=-y |dir=-x |dir=+y |
	// |rot=270|rot=180|rot=90 |rot=0  |
	// +-------+-------+-------+-------+
	//         |face:4 |
	//         |dir=+z |
	//         |rot=180|
	//         +-------+

	switch (faceIndex)
	{
	case 0: ox = 0*faceSize,     oy = 2*faceSize - 1, dxx = +0, dxy = -1, dyx = +1, dyy = +0; return;
	case 1: ox = 3*faceSize - 1, oy = 1*faceSize,     dxx =  0, dxy = +1, dyx = -1, dyy =  0; return;
	case 2: ox = 3*faceSize,     oy = 1*faceSize,     dxx = +1, dxy =  0, dyx =  0, dyy = +1; return;
	case 3: ox = 2*faceSize - 1, oy = 2*faceSize - 1, dxx = -1, dxy =  0, dyx =  0, dyy = -1; return;
	case 4: ox = 2*faceSize - 1, oy = 3*faceSize - 1, dxx = -1, dxy =  0, dyx =  0, dyy = -1; return;
	case 5: ox = 1*faceSize,     oy = 0*faceSize,     dxx = +1, dxy =  0, dyx =  0, dyy = +1; return;
	}

	Assert(false);
}

template <typename T> static void CopyToCubemapFaceFrom4x3Cross(const T* crossImage, T* faceImage, int faceSize, int faceIndex)
{
	const int w = faceSize*4;
	int ox, oy, dxx, dxy, dyx, dyy;
	CopyCubemapFaceSetup(ox, oy, dxx, dxy, dyx, dyy, faceSize, faceIndex);

	for (int y = 0; y < faceSize; y++)
	{
		for (int x = 0; x < faceSize; x++)
		{
			const int xx = ox + x*dxx + y*dyx;
			const int yy = oy + x*dxy + y*dyy;

			faceImage[x + y*faceSize] = crossImage[xx + yy*w];
		}
	}
}

grcImage* fwTextureConversion::Process(const grcImage* image, const char* filename, char swizzle[6], u8& conversionFlags, const fwTextureConversionParams::SecondaryInputs* secondaryInputs, fwTextureConversionParams::SecondaryOutputs* secondaryOutputs) const
{
	if (m_skipProcessing)
	{
		conversionFlags |= TEXTURE_CONVERSION_FLAG_SKIPPED;
		return NULL;
	}

	fwTextureFormatHelper helper(filename);

	if (m_atlasCount > 0)
	{
		// texture atlas -> texture array
		// NOTE: the following features are incompatible with atlases and will be ignored
		//    - split HD
		//    - respect artist-generated mips
		//    - mip blend
		//    - terrain blend
		//    - distance fields
		//    - optimisation for images with no variation

		const grcImage* atlasImage = image;
		const int bpp = grcImage::GetFormatBitsPerPixel(atlasImage->GetFormat());
		const int tile_w = atlasImage->GetWidth()/m_atlasCols;
		const int tile_h = atlasImage->GetHeight()/m_atlasRows;

		grcImage* tileImage = grcImage::Create(tile_w, tile_h, 1, atlasImage->GetFormat(), grcImage::STANDARD, 0, 0);

		class AtlasTileMipChain
		{
		public:
			fwImage** m_mipChain;
			int       m_mipCount;
		};

		AtlasTileMipChain* atlas = rage_new AtlasTileMipChain[m_atlasCount];
		int mipCountFinal = 16;

		for (int row = 0; row < m_atlasRows; row++)
		{
			for (int col = 0; col < m_atlasCols; col++)
			{
				if (col + row*m_atlasCols < m_atlasCount)
				{
					const int cubemapFaceIndices[] = {
						-1,  5, -1, -1,
						 0,  3,  1,  2,
						-1,  4, -1, -1,
					};
					const int tileIndex = col + row*m_atlasCols;
					const int atlasIndex = m_atlasCubemap4x3Cross ? (cubemapFaceIndices[tileIndex%12] + (tileIndex/12)*6) : tileIndex;

					if (atlasIndex >= 0 && atlasIndex < m_atlasCount)
					{
						if (m_atlasCubemap4x3Cross)
						{
							const u8* src = atlasImage->GetBits() + (atlasIndex/6)*atlasImage->GetStride()*tile_h*3;

							switch (bpp)
							{
							case   8: CopyToCubemapFaceFrom4x3Cross((const   u8*)src, (  u8*)tileImage->GetBits(), tile_w, atlasIndex%6); break;
							case  16: CopyToCubemapFaceFrom4x3Cross((const  u16*)src, ( u16*)tileImage->GetBits(), tile_w, atlasIndex%6); break;
							case  32: CopyToCubemapFaceFrom4x3Cross((const  u32*)src, ( u32*)tileImage->GetBits(), tile_w, atlasIndex%6); break;
							case  64: CopyToCubemapFaceFrom4x3Cross((const  u64*)src, ( u64*)tileImage->GetBits(), tile_w, atlasIndex%6); break;
							case 128: CopyToCubemapFaceFrom4x3Cross((const u128*)src, (u128*)tileImage->GetBits(), tile_w, atlasIndex%6); break;
							}
						}
						else
						{
							const int stride = tile_w*bpp/8;

							for (int j = 0; j < tile_h; j++)
							{
								u8* dst = tileImage->GetBits() + j*stride;
								const u8* src = atlasImage->GetBits() + (col*tile_w + (row*tile_h + j)*atlasImage->GetWidth())*bpp/8;
								sysMemCpy(dst, src, stride);
							}
						}

						fwImage**& mipChain = atlas[atlasIndex].m_mipChain;
						int&       mipCount = atlas[atlasIndex].m_mipCount;

						mipChain = NULL;
						mipCount = BuildMipChain(mipChain, tileImage, NULL);
						mipCountFinal = Min<int>(mipCount, mipCountFinal); // should be the same for all tiles, but let's make sure

						if (m_alphaCoverageCompensationFactor > 0.0f)
						{
							helper.m_baseAlphaCoverage = ComputeAlphaCoverage(*mipChain[0], m_alphaCoverageCompensationReference, m_alphaCoverageCompensationRadius, m_texAddr, m_texAddrBorderColour);
						}

						for (int i = mipCount - 1; i >= 0; i--) // postprocess mipmaps in reverse order
						{
							PostprocessMip(mipChain, i, mipCount, helper);
						}
					}
				}
			}
		}

		//atlasImage->Release(); -- don't release atlasImage, it will be handled by the caller
		atlasImage = NULL;
		tileImage->Release();
		tileImage = NULL;

		if (mipCountFinal == 0)
		{
			conversionFlags |= TEXTURE_CONVERSION_FLAG_FAILED_PROCESSING;
			return NULL;
		}

		grcImage::Format formatFlags = grcImage::UNKNOWN;

		// to call PreConvert, we need to create a single mip chain for all tiles ..
		{
			const int mipCountTemp = mipCountFinal*m_atlasCount;
			fwImage** mipChainTemp = rage_new fwImage*[mipCountTemp];

			for (int i = 0; i < m_atlasCount; i++)
			{
				sysMemCpy(&mipChainTemp[i*mipCountFinal], atlas[i].m_mipChain, mipCountFinal*sizeof(fwImage*));
			}

			formatFlags = PreConvert(mipChainTemp, mipCountTemp, helper, swizzle);

			delete[] mipChainTemp;
		}

		grcImage* arrayImage = grcImage::Create(atlas[0].m_mipChain[0]->m_w, atlas[0].m_mipChain[0]->m_h, 1, formatFlags, m_atlasCubemap4x3Cross ? grcImage::CUBE : grcImage::STANDARD, mipCountFinal - 1, m_atlasCount - 1);
		grcImage* arrayLayer = arrayImage;

		for (int i = 0; i < m_atlasCount; i++)
		{
			grcImage* converted = Convert(atlas[i].m_mipChain, atlas[i].m_mipCount, formatFlags, false);
			grcImage* convertedMip = converted;
			grcImage* arrayLayerMip = arrayLayer;

			for (int j = 0; j < mipCountFinal; j++)
			{
				sysMemCpy(arrayLayerMip->GetBits(), convertedMip->GetBits(), convertedMip->GetSize());

				arrayLayerMip = arrayLayerMip->GetNext();
				convertedMip = convertedMip->GetNext();
			}

			DeleteMipChain(atlas[i].m_mipChain, atlas[i].m_mipCount);

			converted->Release();
			arrayLayer = arrayLayer->GetNextLayer();
		}

		delete[] atlas;

		conversionFlags |= TEXTURE_CONVERSION_FLAG_PROCESSED;

		return arrayImage;
	}
	else
	{
		grcImage* auxNormalMap = NULL;

		if (m_specularMapGlossFromNormalMapVariance &&
			AssertVerify(secondaryInputs) &&
			AssertVerify(secondaryInputs->m_normalMapImage) &&
			AssertVerify(secondaryInputs->m_normalMapImage->GetWidth() == image->GetWidth()) &&
			AssertVerify(secondaryInputs->m_normalMapImage->GetHeight() == image->GetHeight()))
		{
			fwTextureConversion auxNormalMapTCP = *this;

			auxNormalMapTCP.m_texFormatUsage = tfu_NormalMap;
			auxNormalMapTCP.m_texFormatHint = tfh_32bpc_float;
			auxNormalMapTCP.m_texFormatOverride = tf_A32B32G32R32F;
			auxNormalMapTCP.m_specularMapGlossFromNormalMapVariance = false;

			char swizzle2[6] = "RGBA\0";
			u8 conversionFlags2 = 0;
			auxNormalMap = auxNormalMapTCP.Process(secondaryInputs->m_normalMapImage, "auxNormalMap", swizzle2, conversionFlags2, NULL, NULL);
		}

		fwImage** mipChain = NULL;
		int mipCount = BuildMipChain(mipChain, image, secondaryInputs ? secondaryInputs->m_diffuseImage : NULL);

		if (mipCount == 0)
		{
			conversionFlags |= TEXTURE_CONVERSION_FLAG_FAILED_PROCESSING;
			return NULL;
		}

		if (secondaryInputs && secondaryInputs->m_mipBlendImage)
		{
			if (AssertVerify(secondaryInputs->m_mipBlendImage->GetWidth() == mipChain[0]->m_w) &&
				AssertVerify(secondaryInputs->m_mipBlendImage->GetHeight() == mipChain[0]->m_h))
			{
				fwImage** mipBlendChain = NULL;
				const int mipBlendCount = BuildMipChain(mipBlendChain, secondaryInputs->m_mipBlendImage);

				const int i0 = m_mipBlendIndexStart;
				const int i1 = Max<int>(i0 + 1, m_mipBlendIndexEnd > 0 ? m_mipBlendIndexEnd : (m_mipBlendIndexEnd + mipBlendCount));

				for (int i = i0; i < Min<int>(mipCount, mipBlendCount); i++)
				{
					const float blend = m_mipBlendAmountStart + (m_mipBlendAmountEnd - m_mipBlendAmountStart)*Clamp<float>((float)(i - i0)/(float)(i1 - i0), 0.0f, 1.0f);

					if (blend > 0.0f)
					{
						BlendImage(mipChain[i], mipBlendChain[i], blend);
					}
				}

				DeleteMipChain(mipBlendChain, mipBlendCount);
			}
		}

		if (m_alphaCoverageCompensationFactor > 0.0f)
		{
			helper.m_baseAlphaCoverage = ComputeAlphaCoverage(*mipChain[0], m_alphaCoverageCompensationReference, m_alphaCoverageCompensationRadius, m_texAddr, m_texAddrBorderColour);
		}

		if (auxNormalMap) // apply variance in auxNormalMap alpha to specular gloss
		{
			const grcImage* auxNormalMip = auxNormalMap;

			for (int mipIndex = 0; mipIndex < mipCount; mipIndex++)
			{
				fwImage* mip = mipChain[mipIndex];
				const int mw = mip->m_w;
				const int mh = mip->m_h;

				if (AssertVerify(auxNormalMip && auxNormalMip->GetWidth() == mw && auxNormalMip->GetHeight() == mh))
				{
					for (int y = 0; y < mh; y++)
					{
						for (int x = 0; x < mw; x++)
						{
							float& alpha = mip->GetPixel(x, y).w;
							float specPow;

							if (m_specularMapGlossUseOldPWLPackingIn)
							{
								specPow = UnpackSpecOldPWL(alpha);
							}
							else
							{
								specPow = UnpackSpecFromGloss(alpha, m_normalMapGlossSpecularMin, m_normalMapGlossSpecularMax); // convert gloss to specular power
							}

							if (m_normalMapGlossSpecPowScale.GetCount() > 0)
							{
								if (m_normalMapGlossSpecPowScaleToLastMip)
								{
									specPow *= m_normalMapGlossSpecPowScale[Max<int>(0, mipIndex - mipCount + m_normalMapGlossSpecPowScale.GetCount())];
								}
								else
								{
									specPow *= m_normalMapGlossSpecPowScale[Min<int>(mipIndex, m_normalMapGlossSpecPowScale.GetCount() - 1)];
								}
							}

							float specVar = SpecPowToVariance(specPow);

							specVar += ((const Vector4*)auxNormalMip->GetBits())[x + y*auxNormalMip->GetWidth()].w + 0.01f; // add variance from normal map
							specPow = SpecPowFromVariance(specVar, m_normalMapGlossSpecularMin, m_normalMapGlossSpecularMax);

							if (m_specularMapGlossUseOldPWLPackingOut)
							{
								alpha = PackSpecOldPWL(specPow);
							}
							else
							{
								alpha = PackSpecToGloss(specPow, m_normalMapGlossSpecularMin, m_normalMapGlossSpecularMax); // convert specular power to gloss
							}
						}
					}

					auxNormalMip = auxNormalMip->GetNext();
				}
			}

			auxNormalMap->Release();
			auxNormalMap = NULL;
		}

		for (int i = mipCount - 1; i >= 0; i--) // postprocess mipmaps in reverse order
		{
			PostprocessMip(mipChain, i, mipCount, helper);
		}

		// determine if every mip is "flat" (i.e. no variation) and this is not a
		// script_rt texture - if so, throw away all the higher mips. we could do
		// this on the source textures instead of the processed texture, but then
		// we have to do it for all the inputs.
		bool bHasNoVariation = false;

		if (!m_doNotOptimise && mipChain[0]->m_w > 4 && mipChain[0]->m_h > 4)
		{
			const Vec4V c0 = VECTOR4_TO_VEC4V(mipChain[0]->GetPixel(0, 0));

			bHasNoVariation = true;

			for (int i = 0; i < mipCount && bHasNoVariation; i++)
			{
				const fwImage* mip = mipChain[i];
				const int w = mip->m_w;
				const int h = mip->m_h;

				for (int y = 0; y < h && bHasNoVariation; y++)
				{
					for (int x = 0; x < w; x++)
					{
						if (MaxElement(Abs(VECTOR4_TO_VEC4V(mip->GetPixel(x, y)) - c0)).Getf() > 1.0f/127.0f)
						{
							bHasNoVariation = false;
							break;
						}
					}
				}
			}

			if (bHasNoVariation)
			{
				fwtexconvLog("texture has no variation! replacing %dx%d (%d mips) with 4x4 (1 mip)", mipChain[0]->m_w, mipChain[0]->m_h, mipCount);

				DeleteMipChain(mipChain, mipCount);

				mipCount = 1;
				mipChain = rage_new fwImage*[mipCount];
				mipChain[0] = rage_new fwImage(4, 4);

				for (int y = 0; y < mipChain[0]->m_h; y++)
				{
					for (int x = 0; x < mipChain[0]->m_h; x++)
					{
						mipChain[0]->SetPixel(x, y, RCC_VECTOR4(c0));
					}
				}
			}
		}

		const grcImage::Format formatFlags = PreConvert(mipChain, mipCount, helper, swizzle);
		const bool bAllowSplitHD = Min<int>(mipChain[0]->m_w, mipChain[0]->m_h) >= 128 || PLATFORM != platform::XENON;

		grcImage* result = NULL;

		if (bAllowSplitHD && m_imageSplitHD2 && mipCount > 2)
		{
			if (secondaryOutputs)
			{
				secondaryOutputs->m_HDImage = Convert(mipChain, mipCount, formatFlags, false);
			}

			result = Convert(mipChain + 2, mipCount - 2, formatFlags, false);
		}
		else if (bAllowSplitHD && (m_imageSplitHD || m_imageSplitHD2) && mipCount > 1)
		{
			const bool bNoMipsInHD = (PLATFORM == platform::XENON && m_imageSplitHD);
			const bool bForceContiguousMips = bNoMipsInHD;

			if (secondaryOutputs)
			{
				secondaryOutputs->m_HDImage = Convert(mipChain, bNoMipsInHD ? 1 : mipCount, formatFlags, false);
			}

			result = Convert(mipChain + 1, mipCount - 1, formatFlags, bForceContiguousMips);
		}
		else
		{
			result = Convert(mipChain, mipCount, formatFlags, false);
		}

		DeleteMipChain(mipChain, mipCount);

		if (result)
		{
			conversionFlags |= TEXTURE_CONVERSION_FLAG_PROCESSED;
		}

		return result;
	}
}

fwTextureConversionParams::SecondaryInputs::SecondaryInputs()
{
	sysMemSet(this, 0, sizeof(*this));
}

fwTextureConversionParams::SecondaryInputs::~SecondaryInputs()
{
	if (m_diffuseImage) { m_diffuseImage->Release(); m_diffuseImage = NULL; }
	if (m_mipBlendImage) { m_mipBlendImage->Release(); m_mipBlendImage = NULL; }
	if (m_normalMapImage) { m_normalMapImage->Release(); m_normalMapImage = NULL; }
}

void fwTextureConversionParams::SecondaryInputs::Release()
{
	delete this;
}

fwTextureConversionParams::SecondaryOutputs::SecondaryOutputs()
{
	sysMemSet(this, 0, sizeof(*this));
}

fwTextureConversionParams::SecondaryOutputs::~SecondaryOutputs()
{
	if (m_HDImage)
	{
		m_HDImage->Release();
		m_HDImage = NULL;
	}
}

void fwTextureConversionParams::SecondaryOutputs::Release()
{
	delete this;
}

} // namespace rage

#endif // (__BANK || __GAMETOOL) && !__RESOURCECOMPILER
