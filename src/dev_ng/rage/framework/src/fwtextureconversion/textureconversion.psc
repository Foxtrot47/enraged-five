<?xml version="1.0"?>
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema">

<structdef type="::rage::fwTextureConversionImage" autoregister="true">
	<string name="m_filename" type="atString"/>
	<string name="m_usage" type="atString"/>
</structdef>

<!-- ============================================================================= -->

<enumdef type="::rage::fwTextureConversionParams::eTextureFormatUsage">
	<enumval name ="tfu_ColourMap"/>
	<enumval name ="tfu_NormalMap"/>
	<enumval name ="tfu_VectorMap"/>
	<enumval name ="tfu_CableMap"/>
	<enumval name ="tfu_TerrainBlendMap0"/>
	<enumval name ="tfu_TerrainBlendMap1"/>
</enumdef>

<enumdef type="::rage::fwTextureConversionParams::eTextureFormatHint">
	<enumval name ="tfh_Compressed"/>
	<enumval name ="tfh_CompressedHighQuality"/>
	<enumval name ="tfh_Quantised"/>
	<enumval name ="tfh_8bpc"/>
	<enumval name ="tfh_16bpc"/>
	<enumval name ="tfh_16bpc_float"/>
	<enumval name ="tfh_32bpc_float"/>
</enumdef>

<enumdef type="::rage::fwTextureConversionParams::eTextureFormat">
	<enumval name="tf_UNKNOWN"/>
	<enumval name="tf_DXT1"/>
	<enumval name="tf_DXT3"/>
	<enumval name="tf_DXT5"/>
	<enumval name="tf_CTX1"/>
	<enumval name="tf_DXT3A"/>
	<enumval name="tf_DXT3A_1111"/>
	<enumval name="tf_DXT5A"/>
	<enumval name="tf_DXN"/>
	<enumval name="tf_BC6"/>
	<enumval name="tf_BC7"/>
	<enumval name="tf_A8R8G8B8"/>
	<enumval name="tf_A8B8G8R8"/>
	<enumval name="tf_A8"/>
	<enumval name="tf_L8"/>
	<enumval name="tf_A8L8"/>
	<enumval name="tf_A4R4G4B4"/>
	<enumval name="tf_A1R5G5B5"/>
	<enumval name="tf_R5G6B5"/>
	<enumval name="tf_R3G3B2"/>
	<enumval name="tf_A8R3G3B2"/>
	<enumval name="tf_A4L4"/>
	<enumval name="tf_A2R10G10B10"/>
	<enumval name="tf_A2B10G10R10"/>
	<enumval name="tf_A16B16G16R16"/>
	<enumval name="tf_G16R16"/>
	<enumval name="tf_L16"/>
	<enumval name="tf_A16B16G16R16F"/>
	<enumval name="tf_G16R16F"/>
	<enumval name="tf_R16F"/>
	<enumval name="tf_A32B32G32R32F"/>
	<enumval name="tf_G32R32F"/>
	<enumval name="tf_R32F"/>
	<enumval name="tf_D15S1"/>
	<enumval name="tf_D24S8"/>
	<enumval name="tf_D24FS8"/>
	<enumval name="tf_P4"/>
	<enumval name="tf_P8"/>
	<enumval name="tf_A8P8"/>
	<enumval name="tf_R8"/>
	<enumval name="tf_R16"/>
	<enumval name="tf_G8R8"/>
	<enumval name="tf_DEPRECATED_0"/>
	<enumval name="tf_DEPRECATED_1"/>
	<enumval name="tf_DEPRECATED_2"/>
	<enumval name="tf_DEPRECATED_3"/>
</enumdef>

<enumdef type="::rage::fwTextureConversionParams::eTextureAddress">
	<enumval name="ta_Wrap"/>
	<enumval name="ta_ClampToEdge"/>
	<enumval name="ta_ClampToBorder"/>
</enumdef>

<structdef type="::rage::fwTextureConversionParams" autoregister="true">
	<string  name="m_parent" type="atString" ui_values="{filenames/'$(assets)/metadata/textures/templates/*.tcp' Recursive=true FullPath=true}"/>

	<bool    name="m_respectMips" description="respect artist-generated mips"/>

	<bool    name="m_imageSplitHD" description="split top mip into separate HD txd"/>
	<bool    name="m_imageSplitHD2" description="split top two mips into separate HD txd - this should only be used for peds"/>
	<int     name="m_imageDownsampleScale" min="0" max="8" description="scaling factor to apply to image initially, image will be resized using simple linear downsampling"/>
	<int     name="m_imageDownsampleScaleX" min="0" max="8" description="scaling factor in x dimension, or zero to use m_imageDownsampleScale"/>
	<int     name="m_imageDownsampleScaleY" min="0" max="8" description="scaling factor in y dimension, or zero to use m_imageDownsampleScale"/>
	<float   name="m_imageDownsampleScaleRel" min="0.0" max="8.0" description="relative scaling factor applied on top of m_imageDownsampleScale (typically specified in a non-parent TCP)"/>
	<float   name="m_imageDownsampleScaleRelX" min="0.0" max="8.0" description="relative scaling factor in x dimension, or zero to use m_imageDownsampleScaleRel"/>
	<float   name="m_imageDownsampleScaleRelY" min="0.0" max="8.0" description="relative scaling factor in y dimension, or zero to use m_imageDownsampleScaleRel"/>
	<int     name="m_imageMaxSize" min="0" max="8192" description="maximum resolution of top-level image, if source is larger will be resized using simple linear downsampling"/>
	<int     name="m_imageMaxSizeX" min="0" max="8192" description="maximum resolution of top-level image in x dimension, or zero to use m_imageMaxSize"/>
	<int     name="m_imageMaxSizeY" min="0" max="8192" description="maximum resolution of top-level image in y dimension, or zero to use m_imageMaxSize"/>
	<int     name="m_imageMaxMips" min="0" max="16" description="maximum number of mips including top-level, or 0 to indicate full mip chain"/>
	<int     name="m_imageMinMinMipSize" min="0" max="8192" hideWidgets="true"/>
	<int     name="m_imageMinMaxMipSize" min="0" max="8192" hideWidgets="true"/>
	<int     name="m_imageTopMipDiscard" min="0" max="16" description="number of top mips to discard (useful to make LOD textures match lower mips of HD textures)"/>

	<int     name="m_atlasRows" description="number of rows for texture array atlas" hideWidgets="true"/>
	<int     name="m_atlasCols" description="number of columns for texture array atlas" hideWidgets="true"/>
	<int     name="m_atlasCount" description="number of layers in texture array atlas" hideWidgets="true"/>
	<bool    name="m_atlasCubemap4x3Cross" description="atlas is a cubemap laid out as a 4x3 cross" hideWidgets="true"/>

	<enum    name="m_texFormatUsage" type="::rage::fwTextureConversionParams::eTextureFormatUsage" hideWidgets="true"/>
	<enum    name="m_texFormatHint" type="::rage::fwTextureConversionParams::eTextureFormatHint" description="hint to determine final texture format"/>
	<bool    name="m_texFormatHintUseDXT3" hideWidgets="true"/>
	<bool    name="m_texFormatHintUse555" hideWidgets="true"/>
	<bool    name="m_texFormatHintHighQualityWithAlpha" hideWidgets="true"/>
	<enum    name="m_texFormatOverride" type="::rage::fwTextureConversionParams::eTextureFormat" hideWidgets="true"/>
	<string  name="m_texFormatOverrideSwizzle" type="atString" hideWidgets="true"/>

	<enum    name="m_texAddr" type="::rage::fwTextureConversionParams::eTextureAddress" hideWidgets="true"/>
	<Vector4 name="m_texAddrBorderColour" min="0.0" max="1.0" hideWidgets="true"/>

	<bool    name="m_preprocessAfterDownsample" description="old behaviour of calling PreprocessImage after initial downsample"/>

	<bool    name="m_sRGBInputR" hideWidgets="true"/>
	<bool    name="m_sRGBInputG" hideWidgets="true"/>
	<bool    name="m_sRGBInputB" hideWidgets="true"/>
	<bool    name="m_sRGBInputA" hideWidgets="true"/>

	<Color32 name="m_colourKey" hideWidgets="true"/>

	<Vector4 name="m_downsampleGammaExponent" min="-8.0" max="8.0" hideWidgets="true"/>
	<Vector4 name="m_downsampleFilterAmount" min="-1.0" max="1.0" hideWidgets="true"/>
	<array   name="m_downsampleFilterCoefficients" type="atArray"><float min="-1.0" max="1.0" hideWidgets="true"/></array>

	<Vector4 name="m_mipGammaExponent" min="-8.0" max="8.0" hideWidgets="true"/>
	<Vector4 name="m_mipFilterAmount" min="-1.0" max="1.0" hideWidgets="true"/>
	<array   name="m_mipFilterCoefficients" type="atArray"><float min="-1.0" max="1.0" hideWidgets="true"/></array>
	<Vector4 name="m_mipFilterSharpenAmount" min="-1.0" max="1.0" hideWidgets="true"/>
	<array   name="m_mipFilterSharpen" type="atArray"><float min="-1.0" max="1.0" hideWidgets="true"/></array>
	<bool    name="m_mipFilterSharpenToLastMip" hideWidgets="true"/>
	<Vector3 name="m_mipFilterDesaturateAmount" min="-1.0" max="1.0" hideWidgets="true"/>
	<array   name="m_mipFilterDesaturate" type="atArray"><float min="-1.0" max="1.0" hideWidgets="true"/></array>
	<bool    name="m_mipFilterDesaturateToLastMip" hideWidgets="true"/>

	<Vector4 name="m_finalGammaExponent" min="-8.0" max="8.0" hideWidgets="true"/>
	<Vector4 name="m_finalMultiplier" min="0.0" max="8.0" hideWidgets="true"/>

	<bool    name="m_mipFadeColourAuto" description="if true, use the average colour of the top-level image instead of mipFadeColour"/>
	<Vector4 name="m_mipFadeColour" min="0.0" max="1.0" description="colour to fade to (e.g. for detail maps)"/>
	<Vector4 name="m_mipFadeAmount" min="0.0" max="1.0" description="fading amount, 0 indicates no fading, 1 indicates full fading"/>
	<array   name="m_mipFade" type="atArray"><float min="0.0" max="1.0" description="fading amount per mip"/></array>
	<bool    name="m_mipFadeToLastMip" description="if true, mipFade array ends at last mip instead of starting at first mip"/>
	<Vector4 name="m_mipFadeThresholdMin" min="-999.0" max="999.0" description="min threshold to apply mip fading"/>
	<Vector4 name="m_mipFadeThresholdMax" min="-999.0" max="999.0" description="max threshold to apply mip fading"/>
	<float   name="m_mipBlendAmountStart" min="0.0" max="1.0" hideWidgets="true"/>
	<float   name="m_mipBlendAmountEnd" min="0.0" max="1.0" hideWidgets="true"/>
	<int     name="m_mipBlendIndexStart" min="0" max="12" hideWidgets="true"/>
	<int     name="m_mipBlendIndexEnd" min="0" max="12" hideWidgets="true"/>

	<float   name="m_alphaCoverageCompensationFactor" min="0.0" max="1.0" hideWidgets="true"/>
	<float   name="m_alphaCoverageCompensationReference" min="0.0" max="1.0" hideWidgets="true"/>
	<int     name="m_alphaCoverageCompensationRadius" min="-1" max="256" hideWidgets="true"/>
	<bool    name="m_alphaCoverageCompensationAllowScaleByLessThanOne" hideWidgets="true"/>
	<int     name="m_alphaCoverageCompensationIterations" min="0" max="32" hideWidgets="true"/>
	<array   name="m_alphaScale" type="atArray"><float min="0.0" max="64.0" hideWidgets="true"/></array>
	<bool    name="m_alphaScaleToLastMip" hideWidgets="true"/>

	<bool    name="m_normalMapGlossInAlpha" hideWidgets="true"/>
	<bool    name="m_normalMapGlossUseOldPWLPacking" hideWidgets="true"/>
	<bool    name="m_normalMapGlossUseAverageLength" hideWidgets="true"/>
	<array   name="m_normalMapGlossSrcFilterRadius" type="atArray"><int min="0" max="16" hideWidgets="true"/></array>
	<array   name="m_normalMapGlossDstFilterRadius" type="atArray"><int min="0" max="16" hideWidgets="true"/></array>
	<float   name="m_normalMapGlossSpecularMin" min="0.001" max="50000.0" hideWidgets="true"/>
	<float   name="m_normalMapGlossSpecularMax" min="0.001" max="50000.0" hideWidgets="true"/>
	<float   name="m_normalMapGlossSpecularPower" min="0.001" max="50000.0" hideWidgets="true"/>
	<array   name="m_normalMapGlossSpecPowScale" type="atArray"><float min="0.0" max="50000.0" hideWidgets="true"/></array>
	<bool    name="m_normalMapGlossSpecPowScaleToLastMip" hideWidgets="true"/>
	<array   name="m_normalMapGlossVarianceScale" type="atArray"><float min="0.0" max="64.0" hideWidgets="true"/></array>
	<bool    name="m_normalMapGlossVarianceScaleToLastMip" hideWidgets="true"/>
	<bool    name="m_normalMapGlossStoreVarianceAlpha" hideWidgets="true"/>
	<array   name="m_normalMapGlossAlphaBlur" type="atArray"><float min="0.0" max="1.0" hideWidgets="true"/></array>
	<int     name="m_normalMapGlossAlphaBlurRadius" min="0" max="8" hideWidgets="true"/>
	<int     name="m_normalMapGlossAlphaBlurPasses" min="0" max="8" hideWidgets="true"/>
	<float   name="m_normalMapGlossAlphaBlurMinFactor" min="0.0" max="1.0" hideWidgets="true"/>
	<bool    name="m_normalMapGlossAlphaOnly" hideWidgets="true"/>
	<float   name="m_normalMapGlossAlphaExponent" min="0.001" max="16.0" hideWidgets="true"/>

	<bool    name="m_specularMapGlossFromNormalMapVariance" hideWidgets="true"/>
	<bool    name="m_specularMapGlossUseOldPWLPackingIn" hideWidgets="true"/>
	<bool    name="m_specularMapGlossUseOldPWLPackingOut" hideWidgets="true"/>

	<float   name="m_normalMapSharpenAmount" min="0.0" max="1.0" hideWidgets="true"/>
	<array   name="m_normalMapSharpen" type="atArray"><float min="0.0" max="1.0" hideWidgets="true"/></array>
	<bool    name="m_normalMapSharpenToLastMip" hideWidgets="true"/>
	<bool    name="m_normalMapSharpenAlphaMask" hideWidgets="true"/>

	<bool    name="m_applyBorderColourR" hideWidgets="true"/>
	<bool    name="m_applyBorderColourG" hideWidgets="true"/>
	<bool    name="m_applyBorderColourB" hideWidgets="true"/>
	<bool    name="m_applyBorderColourA" hideWidgets="true"/>

	<float   name="m_alphaThreshold" min="0.0" max="1.0" hideWidgets="true"/>
	<bool    name="m_alphaPremultiply" hideWidgets="true"/>

	<bool    name="m_distanceField" hideWidgets="true"/>
	<bool    name="m_distanceFieldUnsigned" hideWidgets="true"/>
	<float   name="m_distanceFieldRange" min="0.0" max="256.0" hideWidgets="true"/>

	<bool    name="m_sRGB" hideWidgets="true"/>
	<bool    name="m_linear" hideWidgets="true"/>
	<bool    name="m_sysmem" hideWidgets="true"/>

	<bool    name="m_allowCompressedSource" hideWidgets="true"/>
	<bool    name="m_skipProcessing" hideWidgets="true"/>
	<bool    name="m_doNotOptimise" hideWidgets="true"/>

	<array   name="m_imageFiles" type="atArray"><struct type="::rage::fwTextureConversionImage" hideWidgets="true"/></array>
</structdef>

</ParserSchema>