// =====================================
// textureconversionimage.cpp
// Copyright (c) 2014 Rockstar San Diego
// =====================================

#if (__BANK || __GAMETOOL) && !__RESOURCECOMPILER

#define TEXCONV_USE_BLOCK_COMPRESSION (1 && !__GAMETOOL) // TODO -- move squish and stb to RageGraphics

#include "grcore/image.h"
#include "grcore/image_dxt.h"
#include "math/float16.h"
#if TEXCONV_USE_BLOCK_COMPRESSION
#include "squish/squish.h"
#include "stb/stb_dxt.h"
#endif // TEXCONV_USE_BLOCK_COMPRESSION
#include "system/memops.h"

#include "fwtextureconversion/textureconversionimage.h"

namespace rage {

template <typename T> fwImage_T<T>* fwImage_T<T>::Load(const char* path)
{
	grcImage* temp = grcImage::Load(path);
	fwImage_T<T>* image = NULL;

	if (temp)
	{
		image = ConvertFrom(temp);
		temp->Release();
	}

	return image;
}

static void CopyFromBlockARGB(const DXT::ARGB8888 block[4*4], Vector4* pixels, int w, int h, int bi, int bj)
{
	for (int jj = 0; jj < 4; jj++)
	{
		for (int ii = 0; ii < 4; ii++)
		{
			const int i = ii + bi*4;
			const int j = jj + bj*4;

			if (i < w && j < h)
			{
				pixels[i + j*w].x = (float)block[ii + jj*4].r/255.0f;
				pixels[i + j*w].y = (float)block[ii + jj*4].g/255.0f;
				pixels[i + j*w].z = (float)block[ii + jj*4].b/255.0f;
				pixels[i + j*w].w = (float)block[ii + jj*4].a/255.0f;
			}
		}
	}
}

static void CopyFromBlockAlpha(const DXT::ARGB8888 block[4*4], Vector4* pixels, int w, int h, int bi, int bj, int channel)
{
	for (int jj = 0; jj < 4; jj++)
	{
		for (int ii = 0; ii < 4; ii++)
		{
			const int i = ii + bi*4;
			const int j = jj + bj*4;

			if (i < w && j < h)
			{
				((float*)pixels)[channel + 4*(i + j*w)] = (float)block[ii + jj*4].a/255.0f;
			}
		}
	}
}

template <> fwImage* fwImage::ConvertFrom(const grcImage* src)
{
	fwImage* image = rage_new fwImage(src->GetWidth(), src->GetHeight());
	const int area = (int)src->GetArea();

	if (src->GetFormat() == grcImage::A8R8G8B8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const Color32*)src->GetBits())[i].GetRed  ()/255.0f;
			image->m_pixels[i].y = (float)((const Color32*)src->GetBits())[i].GetGreen()/255.0f;
			image->m_pixels[i].z = (float)((const Color32*)src->GetBits())[i].GetBlue ()/255.0f;
			image->m_pixels[i].w = (float)((const Color32*)src->GetBits())[i].GetAlpha()/255.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A8B8G8R8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const Color32*)src->GetBits())[i].GetBlue ()/255.0f;
			image->m_pixels[i].y = (float)((const Color32*)src->GetBits())[i].GetGreen()/255.0f;
			image->m_pixels[i].z = (float)((const Color32*)src->GetBits())[i].GetRed  ()/255.0f;
			image->m_pixels[i].w = (float)((const Color32*)src->GetBits())[i].GetAlpha()/255.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = 0.0f;
			image->m_pixels[i].y = 0.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = (float)((const u8*)src->GetBits())[i]/255.0f;
		}
	}
	else if (src->GetFormat() == grcImage::L8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u8*)src->GetBits())[i]/255.0f;
			image->m_pixels[i].y = image->m_pixels[i].x;
			image->m_pixels[i].z = image->m_pixels[i].x;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::L16)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u16*)src->GetBits())[i]/65535.0f;
			image->m_pixels[i].y = image->m_pixels[i].x;
			image->m_pixels[i].z = image->m_pixels[i].x;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A8L8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u8*)src->GetBits())[0 + 2*i]/255.0f;
			image->m_pixels[i].y = image->m_pixels[i].x;
			image->m_pixels[i].z = image->m_pixels[i].x;
			image->m_pixels[i].w = (float)((const u8*)src->GetBits())[1 + 2*i]/255.0f;
		}
	}
	else if (src->GetFormat() == grcImage::R5G6B5)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const DXT::RGB565*)src->GetBits())[i].r/31.0f;
			image->m_pixels[i].y = (float)((const DXT::RGB565*)src->GetBits())[i].g/63.0f;
			image->m_pixels[i].z = (float)((const DXT::RGB565*)src->GetBits())[i].b/31.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A1R5G5B5)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const DXT::ARGB1555*)src->GetBits())[i].r/31.0f;
			image->m_pixels[i].y = (float)((const DXT::ARGB1555*)src->GetBits())[i].g/31.0f;
			image->m_pixels[i].z = (float)((const DXT::ARGB1555*)src->GetBits())[i].b/31.0f;
			image->m_pixels[i].w = (float)((const DXT::ARGB1555*)src->GetBits())[i].a;
		}
	}
	else if (src->GetFormat() == grcImage::A4R4G4B4)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const DXT::ARGB4444*)src->GetBits())[i].r/15.0f;
			image->m_pixels[i].y = (float)((const DXT::ARGB4444*)src->GetBits())[i].g/15.0f;
			image->m_pixels[i].z = (float)((const DXT::ARGB4444*)src->GetBits())[i].b/15.0f;
			image->m_pixels[i].w = (float)((const DXT::ARGB4444*)src->GetBits())[i].a/15.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A2R10G10B10)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const DXT::ARGB2101010*)src->GetBits())[i].r/1023.0f;
			image->m_pixels[i].y = (float)((const DXT::ARGB2101010*)src->GetBits())[i].g/1023.0f;
			image->m_pixels[i].z = (float)((const DXT::ARGB2101010*)src->GetBits())[i].b/1023.0f;
			image->m_pixels[i].w = (float)((const DXT::ARGB2101010*)src->GetBits())[i].a/3.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A2B10G10R10)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const DXT::ABGR2101010*)src->GetBits())[i].r/1023.0f;
			image->m_pixels[i].y = (float)((const DXT::ABGR2101010*)src->GetBits())[i].g/1023.0f;
			image->m_pixels[i].z = (float)((const DXT::ABGR2101010*)src->GetBits())[i].b/1023.0f;
			image->m_pixels[i].w = (float)((const DXT::ABGR2101010*)src->GetBits())[i].a/3.0f;
		}
	}
	else if (src->GetFormat() == grcImage::R8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u8*)src->GetBits())[i]/255.0f;
			image->m_pixels[i].y = 0.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::R16)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u16*)src->GetBits())[i]/65535.0f;
			image->m_pixels[i].y = 0.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::R16F)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = ((const Float16*)src->GetBits())[i].GetFloat32_FromFloat16_NoIntrinsics();
			image->m_pixels[i].y = 0.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::R32F)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = ((const float*)src->GetBits())[i];
			image->m_pixels[i].y = 0.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::G8R8)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u8*)src->GetBits())[0 + 2*i]/255.0f;
			image->m_pixels[i].y = (float)((const u8*)src->GetBits())[1 + 2*i]/255.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::G16R16)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u16*)src->GetBits())[0 + 2*i]/65535.0f;
			image->m_pixels[i].y = (float)((const u16*)src->GetBits())[1 + 2*i]/65535.0f;
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::G16R16F)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = ((const Float16*)src->GetBits())[0 + 2*i].GetFloat32_FromFloat16_NoIntrinsics();
			image->m_pixels[i].y = ((const Float16*)src->GetBits())[1 + 2*i].GetFloat32_FromFloat16_NoIntrinsics();
			image->m_pixels[i].z = 0.0f;
			image->m_pixels[i].w = 1.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A16B16G16R16)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = (float)((const u16*)src->GetBits())[0 + 4*i]/65535.0f;
			image->m_pixels[i].y = (float)((const u16*)src->GetBits())[1 + 4*i]/65535.0f;
			image->m_pixels[i].z = (float)((const u16*)src->GetBits())[2 + 4*i]/65535.0f;
			image->m_pixels[i].w = (float)((const u16*)src->GetBits())[3 + 4*i]/65535.0f;
		}
	}
	else if (src->GetFormat() == grcImage::A16B16G16R16F)
	{
		for (int i = 0; i < area; i++)
		{
			image->m_pixels[i].x = ((const Float16*)src->GetBits())[0 + 4*i].GetFloat32_FromFloat16_NoIntrinsics();
			image->m_pixels[i].y = ((const Float16*)src->GetBits())[1 + 4*i].GetFloat32_FromFloat16_NoIntrinsics();
			image->m_pixels[i].z = ((const Float16*)src->GetBits())[2 + 4*i].GetFloat32_FromFloat16_NoIntrinsics();
			image->m_pixels[i].w = ((const Float16*)src->GetBits())[3 + 4*i].GetFloat32_FromFloat16_NoIntrinsics();
		}
	}
	else if (src->GetFormat() == grcImage::A32B32G32R32F)
	{
		sysMemCpy(image->m_pixels, src->GetBits(), src->GetSize());
	}
	else if (grcImage::IsFormatDXTBlockCompressed(src->GetFormat()))
	{
		const int bw = (image->m_w + 3)/4;
		const int bh = (image->m_h + 3)/4;
		DXT::ARGB8888 block[4*4];

		if (src->GetFormat() == grcImage::DXT1)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					((DXT::DXT1_BLOCK*)src->GetBits())[bi + bj*bw].Decompress(block);
					CopyFromBlockARGB(block, image->m_pixels, image->m_w, image->m_h, bi, bj);
				}
			}
		}
		else if (src->GetFormat() == grcImage::DXT3)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					((DXT::DXT3_BLOCK*)src->GetBits())[bi + bj*bw].Decompress(block);
					CopyFromBlockARGB(block, image->m_pixels, image->m_w, image->m_h, bi, bj);
				}
			}
		}
		else if (src->GetFormat() == grcImage::DXT5)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					((DXT::DXT5_BLOCK*)src->GetBits())[bi + bj*bw].Decompress(block);
					CopyFromBlockARGB(block, image->m_pixels, image->m_w, image->m_h, bi, bj);
				}
			}
		}
		else if (src->GetFormat() == grcImage::DXT5A)
		{
			for (int i = 0; i < area; i++) // clear to default colour
			{
				image->m_pixels[i] = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
			}

			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					((DXT::DXT5_ALPHA*)src->GetBits())[bi + bj*bw].Decompress(block);
					CopyFromBlockAlpha(block, image->m_pixels, image->m_w, image->m_h, bi, bj, 0); // copy to red channel
				}
			}
		}
		else if (src->GetFormat() == grcImage::DXN)
		{
			for (int i = 0; i < area; i++) // clear to default colour
			{
				image->m_pixels[i] = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
			}

			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					((DXT::DXN_BLOCK*)src->GetBits())[bi + bj*bw].m_x.Decompress(block);
					CopyFromBlockAlpha(block, image->m_pixels, image->m_w, image->m_h, bi, bj, 0); // copy to red channel
					((DXT::DXN_BLOCK*)src->GetBits())[bi + bj*bw].m_y.Decompress(block);
					CopyFromBlockAlpha(block, image->m_pixels, image->m_w, image->m_h, bi, bj, 1); // copy to green channel
				}
			}
		}
		else
		{
			Assertf(0, "fwImage::ConvertFrom: unsupported compressed image format %s", grcImage::GetFormatString(src->GetFormat()));
			sysMemSet(image->m_pixels, 0, area*sizeof(Vector4));
		}
	}
	else
	{
		Assertf(0, "fwImage::ConvertFrom: unsupported image format %s", grcImage::GetFormatString(src->GetFormat()));
		sysMemSet(image->m_pixels, 0, area*sizeof(Vector4));
	}

	return image;
}

#if TEXCONV_USE_BLOCK_COMPRESSION

static void CopyToBlockABGR(DXT::ABGR8888 block[4*4], const Vector4* pixels, int w, int h, int bi, int bj)
{
	for (int jj = 0; jj < 4; jj++)
	{
		for (int ii = 0; ii < 4; ii++)
		{
			const int i = Min<int>(ii + bi*4, w - 1);
			const int j = Min<int>(jj + bj*4, h - 1);

			block[ii + jj*4].r = (u32)Clamp<float>(0.5f + 255.0f*pixels[i + j*w].x, 0.0f, 255.0f);
			block[ii + jj*4].g = (u32)Clamp<float>(0.5f + 255.0f*pixels[i + j*w].y, 0.0f, 255.0f);
			block[ii + jj*4].b = (u32)Clamp<float>(0.5f + 255.0f*pixels[i + j*w].z, 0.0f, 255.0f);
			block[ii + jj*4].a = (u32)Clamp<float>(0.5f + 255.0f*pixels[i + j*w].w, 0.0f, 255.0f);
		}
	}
}

static void CopyToBlockAlpha(DXT::ABGR8888 block[4*4], const Vector4* pixels, int w, int h, int bi, int bj, int channel)
{
	for (int jj = 0; jj < 4; jj++)
	{
		for (int ii = 0; ii < 4; ii++)
		{
			const int i = Min<int>(ii + bi*4, w - 1);
			const int j = Min<int>(jj + bj*4, h - 1);

			block[ii + jj*4].abgr = 0;
			block[ii + jj*4].a = (u32)Clamp<float>(0.5f + 255.0f*((const float*)pixels)[channel + 4*(i + j*w)], 0.0f, 255.0f);
		}
	}
}

enum eSquishCompressionType
{
	SQUISH_COMPRESSION_NONE,
	SQUISH_COMPRESSION_CLUSTER_FIT,
	SQUISH_COMPRESSION_ITERATIVE_CLUSTER_FIT,
	SQUISH_COMPRESSION_RANGE_FIT,
};

class fwTextureCompressionParams
{
public:
	fwTextureCompressionParams()
	{
		sysMemSet(this, 0, sizeof(*this));
#if __DEV
		static fwTextureCompressionParams p; // <- change in debugger
		*this = p;
#endif
	}

	eSquishCompressionType m_squishCompressionType;
	float                  m_channelWeights[3];
	bool                   m_perceptual;
	u8                     m_alphaThreshold;
};

static bool ConvertToSquish(grcImage* dst, const fwImage* src, const fwTextureCompressionParams& params = fwTextureCompressionParams())
{
	if (dst->GetFormat() == grcImage::DXT1 ||
		dst->GetFormat() == grcImage::DXT3 ||
		dst->GetFormat() == grcImage::DXT5)
	{
		int flags = params.m_perceptual ? squish::kColourMetricPerceptual : squish::kColourMetricUniform;

		switch (params.m_squishCompressionType)
		{
		case SQUISH_COMPRESSION_NONE                 : return false;
		case SQUISH_COMPRESSION_CLUSTER_FIT          : flags |= squish::kColourClusterFit; break;
		case SQUISH_COMPRESSION_ITERATIVE_CLUSTER_FIT: flags |= squish::kColourIterativeClusterFit; break;
		case SQUISH_COMPRESSION_RANGE_FIT            : flags |= squish::kColourRangeFit; break;
		}

		const u8 alphaThreshold = params.m_alphaThreshold;
		const float* channelWeights = NULL;
		
		if (params.m_channelWeights[0] != 0.0f ||
			params.m_channelWeights[1] != 0.0f ||
			params.m_channelWeights[2] != 0.0f)
		{
			channelWeights = params.m_channelWeights;
		}

		const int srcPitch = src->m_w*sizeof(DXT::ABGR8888);
		const int area = src->m_w*src->m_h;
		const int bw = (src->m_w + 3)/4;
	//	const int bh = (src->m_h + 3)/4;

		DXT::ABGR8888* temp = rage_new DXT::ABGR8888[area];

		for (int i = 0; i < area; i++)
		{
			temp[i].r = (u8)Clamp<float>(0.5f + 255.0f*src->m_pixels[i].x, 0.0f, 255.0f);
			temp[i].g = (u8)Clamp<float>(0.5f + 255.0f*src->m_pixels[i].y, 0.0f, 255.0f);
			temp[i].b = (u8)Clamp<float>(0.5f + 255.0f*src->m_pixels[i].z, 0.0f, 255.0f);
			temp[i].a = (u8)Clamp<float>(0.5f + 255.0f*src->m_pixels[i].w, 0.0f, 255.0f);
		}

		switch ((int)dst->GetFormat())
		{
		case grcImage::DXT1: squish::CompressImage((const u8*)temp, src->m_w, src->m_h, srcPitch, bw*sizeof(DXT::DXT1_BLOCK), dst->GetBits(), flags | squish::kDxt1, alphaThreshold, channelWeights); break;
		case grcImage::DXT3: squish::CompressImage((const u8*)temp, src->m_w, src->m_h, srcPitch, bw*sizeof(DXT::DXT3_BLOCK), dst->GetBits(), flags | squish::kDxt3, alphaThreshold, channelWeights); break;
		case grcImage::DXT5: squish::CompressImage((const u8*)temp, src->m_w, src->m_h, srcPitch, bw*sizeof(DXT::DXT5_BLOCK), dst->GetBits(), flags | squish::kDxt5, alphaThreshold, channelWeights); break;
		}

		delete[] temp;

		return true;
	}

	return false;
}

#endif // TEXCONV_USE_BLOCK_COMPRESSION

template <> void fwImage::ConvertTo(grcImage* dst) const
{
	if (dst->GetWidth() != m_w  ||
		dst->GetHeight() != m_h  ||
		dst->GetDepth() != 1 ||
		dst->GetType() != grcImage::STANDARD)
	{
		sysMemSet(dst->GetBits(), 0, dst->GetSize());
		return;
	}

	const int area = m_w*m_h;

	if (dst->GetFormat() == grcImage::A8R8G8B8)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::ARGB8888*)dst->GetBits())[i].r = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].x, 0.0f, 255.0f);
			((DXT::ARGB8888*)dst->GetBits())[i].g = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].y, 0.0f, 255.0f);
			((DXT::ARGB8888*)dst->GetBits())[i].b = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].z, 0.0f, 255.0f);
			((DXT::ARGB8888*)dst->GetBits())[i].a = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].w, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A8B8G8R8)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::ABGR8888*)dst->GetBits())[i].r = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].x, 0.0f, 255.0f);
			((DXT::ABGR8888*)dst->GetBits())[i].g = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].y, 0.0f, 255.0f);
			((DXT::ABGR8888*)dst->GetBits())[i].b = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].z, 0.0f, 255.0f);
			((DXT::ABGR8888*)dst->GetBits())[i].a = (u32)Clamp<float>(0.5f + 255.0f*m_pixels[i].w, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A8)
	{
		for (int i = 0; i < area; i++)
		{
			((u8*)dst->GetBits())[i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].w, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::L8)
	{
		for (int i = 0; i < area; i++)
		{
			((u8*)dst->GetBits())[i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].x, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::L16)
	{
		for (int i = 0; i < area; i++)
		{
			((u16*)dst->GetBits())[i] = (u8)Clamp<float>(0.5f + 65535.0f*m_pixels[i].x, 0.0f, 65535.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A8L8)
	{
		for (int i = 0; i < area; i++)
		{
			((u8*)dst->GetBits())[0 + 2*i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].x, 0.0f, 255.0f);
			((u8*)dst->GetBits())[1 + 2*i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].w, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::R5G6B5)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::RGB565*)dst->GetBits())[i].r = (u16)Clamp<float>(0.5f + 31.0f*m_pixels[i].x, 0.0f, 31.0f);
			((DXT::RGB565*)dst->GetBits())[i].g = (u16)Clamp<float>(0.5f + 63.0f*m_pixels[i].y, 0.0f, 63.0f);
			((DXT::RGB565*)dst->GetBits())[i].b = (u16)Clamp<float>(0.5f + 31.0f*m_pixels[i].z, 0.0f, 31.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A1R5G5B5)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::ARGB1555*)dst->GetBits())[i].r = (u16)Clamp<float>(0.5f + 31.0f*m_pixels[i].x, 0.0f, 31.0f);
			((DXT::ARGB1555*)dst->GetBits())[i].g = (u16)Clamp<float>(0.5f + 31.0f*m_pixels[i].y, 0.0f, 31.0f);
			((DXT::ARGB1555*)dst->GetBits())[i].b = (u16)Clamp<float>(0.5f + 31.0f*m_pixels[i].z, 0.0f, 31.0f);
			((DXT::ARGB1555*)dst->GetBits())[i].a = m_pixels[i].w > 0.5f ? 1 : 0;
		}
	}
	else if (dst->GetFormat() == grcImage::A4R4G4B4)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::ARGB4444*)dst->GetBits())[i].r = (u16)Clamp<float>(0.5f + 15.0f*m_pixels[i].x, 0.0f, 15.0f);
			((DXT::ARGB4444*)dst->GetBits())[i].g = (u16)Clamp<float>(0.5f + 15.0f*m_pixels[i].y, 0.0f, 15.0f);
			((DXT::ARGB4444*)dst->GetBits())[i].b = (u16)Clamp<float>(0.5f + 15.0f*m_pixels[i].z, 0.0f, 15.0f);
			((DXT::ARGB4444*)dst->GetBits())[i].a = (u16)Clamp<float>(0.5f + 15.0f*m_pixels[i].w, 0.0f, 15.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A2R10G10B10)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::ARGB2101010*)dst->GetBits())[i].r = (u32)Clamp<float>(0.5f + 1023.0f*m_pixels[i].x, 0.0f, 1023.0f);
			((DXT::ARGB2101010*)dst->GetBits())[i].g = (u32)Clamp<float>(0.5f + 1023.0f*m_pixels[i].y, 0.0f, 1023.0f);
			((DXT::ARGB2101010*)dst->GetBits())[i].b = (u32)Clamp<float>(0.5f + 1023.0f*m_pixels[i].z, 0.0f, 1023.0f);
			((DXT::ARGB2101010*)dst->GetBits())[i].a = (u32)Clamp<float>(0.5f +    3.0f*m_pixels[i].w, 0.0f,    3.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A2B10G10R10)
	{
		for (int i = 0; i < area; i++)
		{
			((DXT::ABGR2101010*)dst->GetBits())[i].r = (u32)Clamp<float>(0.5f + 1023.0f*m_pixels[i].x, 0.0f, 1023.0f);
			((DXT::ABGR2101010*)dst->GetBits())[i].g = (u32)Clamp<float>(0.5f + 1023.0f*m_pixels[i].y, 0.0f, 1023.0f);
			((DXT::ABGR2101010*)dst->GetBits())[i].b = (u32)Clamp<float>(0.5f + 1023.0f*m_pixels[i].z, 0.0f, 1023.0f);
			((DXT::ABGR2101010*)dst->GetBits())[i].a = (u32)Clamp<float>(0.5f +    3.0f*m_pixels[i].w, 0.0f,    3.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::R8)
	{
		for (int i = 0; i < area; i++)
		{
			((u8*)dst->GetBits())[i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].x, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::R16)
	{
		for (int i = 0; i < area; i++)
		{
			((u16*)dst->GetBits())[i] = (u8)Clamp<float>(0.5f + 65535.0f*m_pixels[i].x, 0.0f, 65535.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::R16F)
	{
		for (int i = 0; i < area; i++)
		{
			((Float16*)dst->GetBits())[i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].x);
		}
	}
	else if (dst->GetFormat() == grcImage::R32F)
	{
		for (int i = 0; i < area; i++)
		{
			((float*)dst->GetBits())[i] = m_pixels[i].x;
		}
	}
	else if (dst->GetFormat() == grcImage::G8R8)
	{
		for (int i = 0; i < area; i++)
		{
			((u8*)dst->GetBits())[0 + 2*i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].x, 0.0f, 255.0f);
			((u8*)dst->GetBits())[1 + 2*i] = (u8)Clamp<float>(0.5f + 255.0f*m_pixels[i].y, 0.0f, 255.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::G16R16)
	{
		for (int i = 0; i < area; i++)
		{
			((u16*)dst->GetBits())[0 + 2*i] = (u16)Clamp<float>(0.5f + 65535.0f*m_pixels[i].x, 0.0f, 65535.0f);
			((u16*)dst->GetBits())[1 + 2*i] = (u16)Clamp<float>(0.5f + 65535.0f*m_pixels[i].y, 0.0f, 65535.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::G16R16F)
	{
		for (int i = 0; i < area; i++)
		{
			((Float16*)dst->GetBits())[0 + 2*i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].x);
			((Float16*)dst->GetBits())[1 + 2*i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].y);
		}
	}
	else if (dst->GetFormat() == grcImage::A16B16G16R16)
	{
		for (int i = 0; i < area; i++)
		{
			((u16*)dst->GetBits())[0 + 4*i] = (u16)Clamp<float>(0.5f + 65535.0f*m_pixels[i].x, 0.0f, 65535.0f);
			((u16*)dst->GetBits())[1 + 4*i] = (u16)Clamp<float>(0.5f + 65535.0f*m_pixels[i].y, 0.0f, 65535.0f);
			((u16*)dst->GetBits())[2 + 4*i] = (u16)Clamp<float>(0.5f + 65535.0f*m_pixels[i].z, 0.0f, 65535.0f);
			((u16*)dst->GetBits())[3 + 4*i] = (u16)Clamp<float>(0.5f + 65535.0f*m_pixels[i].w, 0.0f, 65535.0f);
		}
	}
	else if (dst->GetFormat() == grcImage::A16B16G16R16F)
	{
		for (int i = 0; i < area; i++)
		{
			((Float16*)dst->GetBits())[0 + 4*i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].x);
			((Float16*)dst->GetBits())[1 + 4*i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].y);
			((Float16*)dst->GetBits())[2 + 4*i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].z);
			((Float16*)dst->GetBits())[3 + 4*i].SetFloat16_FromFloat32_NoIntrinsics(m_pixels[i].w);
		}
	}
	else if (dst->GetFormat() == grcImage::A32B32G32R32F)
	{
		sysMemCpy(dst->GetBits(), m_pixels, dst->GetSize());
	}
#if TEXCONV_USE_BLOCK_COMPRESSION
	else if (grcImage::IsFormatDXTBlockCompressed(dst->GetFormat()))
	{
		if (ConvertToSquish(dst, this))
		{
			return;
		}

		const int bw = (m_w + 3)/4;
		const int bh = (m_h + 3)/4;
		DXT::ABGR8888 block[4*4];

		if (dst->GetFormat() == grcImage::DXT1)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					CopyToBlockABGR(block, m_pixels, m_w, m_h, bi, bj);
					stb_compress_dxt_block(
						(unsigned char*)&((DXT::DXT1_BLOCK*)dst->GetBits())[bi + bj*bw],
						(const unsigned char*)block,
						false, // alpha
						false, // alphaonly
						STB_DXT_HIGHQUAL);
				}
			}
		}
		else if (dst->GetFormat() == grcImage::DXT3)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					CopyToBlockABGR(block, m_pixels, m_w, m_h, bi, bj);
					stb_compress_dxt_block(
						(unsigned char*)&((DXT::DXT3_BLOCK*)dst->GetBits())[bi + bj*bw].m_colour,
						(const unsigned char*)block,
						false, // alpha
						false, // alphaonly
						STB_DXT_HIGHQUAL);

					int indices[4*4];

					for (int i = 0; i < 4*4; i++)
					{
						indices[i] = (int)(block[i].a >> 4);
					}

					((DXT::DXT3_BLOCK*)dst->GetBits())[bi + bj*bw].m_alpha.SetIndices(indices);
				}
			}
		}
		else if (dst->GetFormat() == grcImage::DXT5)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					CopyToBlockABGR(block, m_pixels, m_w, m_h, bi, bj);
					stb_compress_dxt_block(
						(unsigned char*)&((DXT::DXT5_BLOCK*)dst->GetBits())[bi + bj*bw],
						(const unsigned char*)block,
						true, // alpha
						false, // alphaonly
						STB_DXT_HIGHQUAL);
				}
			}
		}
		else if (dst->GetFormat() == grcImage::DXT5A)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					CopyToBlockAlpha(block, m_pixels, m_w, m_h, bi, bj, 0); // copy from red channel
					stb_compress_dxt_block(
						(unsigned char*)&((DXT::DXT5_ALPHA*)dst->GetBits())[bi + bj*bw],
						(const unsigned char*)block,
						true, // alpha
						true, // alphaonly
						STB_DXT_HIGHQUAL);
				}
			}
		}
		else if (dst->GetFormat() == grcImage::DXN)
		{
			for (int bj = 0; bj < bh; bj++)
			{
				for (int bi = 0; bi < bw; bi++)
				{
					CopyToBlockAlpha(block, m_pixels, m_w, m_h, bi, bj, 0); // copy from red channel
					stb_compress_dxt_block(
						(unsigned char*)&((DXT::DXN_BLOCK*)dst->GetBits())[bi + bj*bw].m_x,
						(const unsigned char*)block,
						true, // alpha
						true, // alphaonly
						STB_DXT_HIGHQUAL);
					CopyToBlockAlpha(block, m_pixels, m_w, m_h, bi, bj, 1); // copy from green channel
					stb_compress_dxt_block(
						(unsigned char*)&((DXT::DXN_BLOCK*)dst->GetBits())[bi + bj*bw].m_y,
						(const unsigned char*)block,
						true, // alpha
						true, // alphaonly
						STB_DXT_HIGHQUAL);
				}
			}
		}
		else
		{
			Assertf(0, "fwImage::ConvertTo: unsupported compressed image format %s", grcImage::GetFormatString(dst->GetFormat()));
			sysMemSet(dst->GetBits(), 0, dst->GetSize());
		}
	}
#endif // TEXCONV_USE_BLOCK_COMPRESSION
	else
	{
		Assertf(0, "fwImage::ConvertTo: unsupported image format %s", grcImage::GetFormatString(dst->GetFormat()));
		sysMemSet(dst->GetBits(), 0, dst->GetSize());
	}
}

} // namespace rage

#endif // (__BANK || __GAMETOOL) && !__RESOURCECOMPILER
