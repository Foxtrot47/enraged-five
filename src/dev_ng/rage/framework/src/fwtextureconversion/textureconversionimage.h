// =====================================
// textureconversionimage.h
// Copyright (c) 2014 Rockstar San Diego
// =====================================

#ifndef _FWTEXTURECONVERSION_TEXTURECONVERSIONIMAGE_H_
#define _FWTEXTURECONVERSION_TEXTURECONVERSIONIMAGE_H_

#if (__BANK || __GAMETOOL) && !__RESOURCECOMPILER

#include "vector/vector4.h"

namespace rage {

class grcImage;

template <typename T> class fwImage_T
{
public:
	fwImage_T() : m_w(0), m_h(0), m_pixels(NULL)
	{
	}

	fwImage_T(int w, int h) : m_w(w), m_h(h)
	{
		m_pixels = rage_new T[w*h];
	}

	fwImage_T(int w, int h, const T& fill) : m_w(w), m_h(h)
	{
		m_pixels = rage_new T[w*h];

		for (int i = 0; i < w*h; i++)
		{
			m_pixels[i] = fill;
		}
	}

	~fwImage_T()
	{
		if (m_pixels)
		{
			delete[] m_pixels;
		}
	}

	static fwImage_T* Load(const char* path);
	static fwImage_T* ConvertFrom(const grcImage* src);
	void ConvertTo(grcImage* dst) const;

	static bool IsValid(int w, int h)
	{
		const int kMaxImageSize = 16384; // arbitrary limits ..
		const int kMaxImageArea = 8192*8192;

		return Min<int>(w,h) > 0 && Max<int>(w,h) <= kMaxImageSize && w*h <= kMaxImageArea;
	}

	__forceinline bool IsValid() const { return this && IsValid(m_w, m_h); }

	__forceinline bool IsValidPos(int x, int y) const { return x >= 0 && x < m_w && y >= 0 && y < m_h; }

	static __forceinline int GetAddrClamped(int x, int maxVal) { return x <= 0 ? 0 : (x >= maxVal ? maxVal : x); }
	static __forceinline int GetAddrWrapped(int x, int maxVal) { return x >= 0 ? (x % (maxVal + 1)) : (maxVal - ((-(x + 1)) % (maxVal + 1))); }

	__forceinline T* GetPixelRowAddr       (int y) const { Assert(y >= 0 && y < m_h); return const_cast<T*>(&m_pixels[y*m_w]); }
	__forceinline T* GetPixelRowAddrClamped(int y) const { return const_cast<T*>(&m_pixels[GetAddrClamped(y, m_h - 1)*m_w]); }
	__forceinline T* GetPixelRowAddrWrapped(int y) const { return const_cast<T*>(&m_pixels[GetAddrWrapped(y, m_h - 1)*m_w]); }

	__forceinline T& GetPixel       (int x, int y) const { Assert(IsValidPos(x, y)); return GetPixelRowAddr(y)[x]; }
	__forceinline T& GetPixelClamped(int x, int y) const { return GetPixelRowAddrClamped(y)[GetAddrClamped(x, m_w - 1)]; }
	__forceinline T& GetPixelWrapped(int x, int y) const { return GetPixelRowAddrWrapped(y)[GetAddrWrapped(x, m_w - 1)]; }
	__forceinline T  GetPixelBorder (int x, int y, const T& border) const { if (IsValidPos(x, y)) { return GetPixelRowAddr(y)[x]; } return border; }

	__forceinline void SetPixel       (int x, int y, const T& p) const { Assert(IsValidPos(x, y)); GetPixelRowAddr(y)[x] = p; }
	__forceinline bool SetPixelDiscard(int x, int y, const T& p) const { if (IsValidPos(x, y)) { GetPixelRowAddr(y)[x] = p; return true; } return false; }

	int m_w;
	int m_h;
	T *m_pixels; // for some reason atArray crashes?
};

typedef fwImage_T<Vector4> fwImage;

} // namespace rage

#endif // (__BANK || __GAMETOOL) && !__RESOURCECOMPILER
#endif // _FWTEXTURECONVERSION_TEXTURECONVERSIONIMAGE_H_
