Project framework_lib_psc
ProjectType util
ForceLanguage Cpp

RootDirectory ..

ParserExtract

Include vcproj/RageFramework/RageFramework.makefile

Libraries {
	%RAGE_DIR%\base\src\rage_lib_psc\rage_lib_psc
	%RAGE_DIR%\suite\src\suite_lib_psc\suite_lib_psc
}
