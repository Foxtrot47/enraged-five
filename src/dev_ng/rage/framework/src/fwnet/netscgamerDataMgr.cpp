//===========================================================================
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.             
//===========================================================================
#include "netscgamerdatamgr.h"
#include "fwnet/optimisations.h"
#include "fwsys/timer.h"

#include "bank/bank.h"
#include "data/rson.h"
#include "diag/channel.h"
#include "string/stringhash.h"
#include "rline/cloud/rlcloud.h"

using namespace rage;

NETWORK_OPTIMISATIONS();

RAGE_DECLARE_CHANNEL(net);
RAGE_DEFINE_SUBCHANNEL(net, SCGamerData)
#undef __rage_channel
#define __rage_channel net_SCGamerData

PARAM(isrockstardev, "Set Is Rockstar Dev status");

netSCGamerDataMgr::netSCGamerDataMgr()
{
#if __BANK
	m_bBank_Override = false;
	m_bBank_IsCheater = false;
	m_bBank_IsRockstarDev = false;
	m_bBank_IsRockstarQA = false;
	m_Bank_XPBonus_StartTime = 0;
	m_Bank_XPBonus_EndTime = 0;
	m_Bank_XPBonus = 0;
#endif

	m_iActiveGamerIndex = -1;

	m_bIsRockstarDev = false;
	m_bIsRockstarQA = false;
	m_bIsCheater = false;
	m_bIsBadSport = false;
	m_bIsXPBonusActive = false;
	m_fXPBonus = 0.0f;

	m_ROSDlgt.Bind(this, &netSCGamerDataMgr::OnRosEvent);
}

void netSCGamerDataMgr::Init(const char* gameCode)
{
	formatf(m_gameCode,"%s",gameCode);
	m_bUpdateRequested = false;
	m_iActiveGamerIndex = -1;
	rlRos::AddDelegate(&m_ROSDlgt);
	m_cloudWatcher.Bind(this, &netSCGamerDataMgr::OnCloudFileModified);

#if __BANK
	if (PARAM_isrockstardev.Get())
	{
		m_bBank_Override = true;
		m_bBank_IsRockstarDev = true;
	}
#endif
}

void netSCGamerDataMgr::Update()
{
	if(m_bUpdateRequested && m_cloudFile.IsIdle())
	{
		if(!RL_IS_VALID_LOCAL_GAMER_INDEX(m_iActiveGamerIndex))
		{
			rageDebugf1("Clearing gamer data because bUpdateRequested and invalid active gamer index");
			m_values.ResetCount();
		}
		else if (!rlRos::HasPrivilege(m_iActiveGamerIndex, RLROS_PRIVILEGEID_CLOUD_STORAGE_READ))
		{
			rageDebugf1("Ignoring bUpdateRequested because cloud read privileges are invalid");
		}
		else if(!rlRos::GetCredentials(m_iActiveGamerIndex).IsValid())
		{
			rageDebugf1("Ignoring bUpdateRequested because credentials are invalid");
		}
		else
		{
			rageDebugf1("Processing update request");
			RequestInfo(m_iActiveGamerIndex);
		}
		m_bUpdateRequested = false;
	}
	m_cloudFile.Update();

	// Tick any cached timer stuff once every 16 frames
	if( Unlikely(m_cloudFile.IsIdle() && ( fwTimer::GetFrameCount() & 0xF )==0) )
	{
		//@@: range NETSCGAMERDATAMGR_UPDATE {
		m_bIsCheater = IsCheaterSC();
		m_bIsBadSport = IsBadSportSC();
		m_bIsXPBonusActive = GetActiveXPBonusSC(m_fXPBonus);
		//@@: } NETSCGAMERDATAMGR_UPDATE
	}
}

void netSCGamerDataMgr::Shutdown()
{
	rlRos::RemoveDelegate(&m_ROSDlgt);
}

#if RSG_DURANGO
#define SETTINGS_FILE_PATH_FMT "secure/%s/RosGamersSettings_xboxone.json"
#elif RSG_ORBIS
#define SETTINGS_FILE_PATH_FMT "secure/%s/RosGamersSettings_ps4.json"
#else
#define SETTINGS_FILE_PATH_FMT "secure/%s/RosGamersSettings.json"
#endif

void netSCGamerDataMgr::RequestInfo(int gamerIndex)
{
	char cloudFilePath[128];
	formatf(cloudFilePath, SETTINGS_FILE_PATH_FMT, m_gameCode);
	rageDebugf1("Requesting gamer data from %s", cloudFilePath);
	m_values.ResetCount();
	m_cloudFile.Init(	gamerIndex, 
						rlCloud::INVALID_MEMBER_ID, 
						cloudFilePath, 
						0,
						NET_HTTP_OPTIONS_NONE,
						1024,
						RL_CLOUD_ONLINE_SERVICE_NATIVE, 
						datCallback(MFA1(netSCGamerDataMgr::HandleFileReceived), this), 
						netCloudRequestMemPolicy::NULL_POLICY);
	m_cloudFile.Start();
	
	if (m_cloudWatcher.IsWatching())
	{
		rlCloud::UnwatchMemberItem(&m_cloudWatcher);
	}

	rlCloud::WatchMemberItem(gamerIndex, RL_CLOUD_ONLINE_SERVICE_NATIVE, cloudFilePath, &m_cloudWatcher);
}


void netSCGamerDataMgr::ProcessReceivedData()
{
	if(rageVerifyf(RsonReader::ValidateJson((const char*)m_cloudFile.GetGrowBuffer().GetBuffer(), m_cloudFile.GetGrowBuffer().Length()), "Failed json validation for '%s'", (const char*)m_cloudFile.GetGrowBuffer().GetBuffer()))
	{
		RsonReader reader((const char*)m_cloudFile.GetGrowBuffer().GetBuffer(), m_cloudFile.GetGrowBuffer().Length());

		RsonReader tmp;

		m_values.ResetCount();

		for(bool ok = reader.GetFirstMember(&tmp); ok; ok = tmp.GetNextSibling(&tmp))
		{
			char name[64];
			if(tmp.GetName(name))
			{
				gamerDataValue val;

				switch (name[0])
				{
				case 'b':
					{
						bool bVal = false;
						if (Verifyf(tmp.AsBool(bVal), "name prefixed with 'b' is not a valid boolean"))
						{
							val.Set(name, bVal);
							rageDebugf1("     Received %s [%d] as bool[%s]", name, val.m_nameHash, bVal ? "true" : "false");
						}
					}
					break;
				case 'i':
					{
						int iVal = 0;
						if (Verifyf(tmp.AsInt(iVal), "name prefixed with 'i' is not a valid int"))
						{
							val.Set(name, iVal);
							rageDebugf1("     Received %s [%d] as int[%d]", name, val.m_nameHash, iVal);
						}
					}
					break;
				case 'f':
					{
						float fVal = 0.0f;
						if (Verifyf(tmp.AsFloat(fVal), "name prefixed with 'f' is not a valid float"))
						{
							val.Set(name, fVal);
							rageDebugf1("     Received %s [%d] as float[%f]", name, val.m_nameHash, fVal);
						}
					}
					break;
				case 'u':
					{
						u32 uVal = 0;
						if (Verifyf(tmp.AsUns(uVal), "name prefixed with 'u' is not a u32"))
						{
							val.Set(name, uVal);
							rageDebugf1("     Received %s [%d] as u32[%u]", name, val.m_nameHash, uVal);
						}
					}
					break;
				//treat random stuff as a string
				default:
					char tmpBuf[64];
					if (tmp.AsString(tmpBuf))
					{
						val.Set(name, tmpBuf);
						rageDebugf1("     Received %s [%d] as string data [%s]", name, val.m_nameHash, tmpBuf);
					}
					break;

				}

				if (val.m_nameHash)
				{
					m_values.PushAndGrow(val);
				}
			}
		}
	}

	m_cloudFile.Reset();

	// cache values
	//@@: range NETSCGAMERDATAMGR_PROCESSRECEIVEDDATA_CHECK_QUALIFYING_PROPERTIES {
	m_bIsRockstarDev = IsRockstarDevSC();
	m_bIsRockstarQA = IsRockstarQASC();
	m_bIsCheater = IsCheaterSC();
	m_bIsBadSport = IsBadSportSC();
	m_bIsXPBonusActive = GetActiveXPBonusSC(m_fXPBonus);
	//@@: } NETSCGAMERDATAMGR_PROCESSRECEIVEDDATA_CHECK_QUALIFYING_PROPERTIES
}


void netSCGamerDataMgr::HandleFileReceived(netCloudRequestHelper* )
{
	if(m_cloudFile.DidSucceed())
	{
		rageDebugf1("ROS Gamer Data cloud file receieved successfully");
		ProcessReceivedData();
	}
	else
	{
		rageDebugf1("Gamer doesn't have any ROS Gamer Settings");
		m_cloudFile.Reset();
		//@TODO Reset(); other stuff?
	}
}


void netSCGamerDataMgr::SetActiveGamerIndex( int gamerIndex )
{
	if (gamerIndex != m_iActiveGamerIndex)
	{
		rageDebugf1("Setting Active gamer index from %d to %d", m_iActiveGamerIndex, gamerIndex);
		m_iActiveGamerIndex = gamerIndex;
		m_bUpdateRequested = true;
	}
}


void netSCGamerDataMgr::OnRosEvent( const rlRosEvent& evnt )
{
	const unsigned evtId = evnt.GetId();

	bool dDoRequest = false;
	switch (evtId)
	{
	case RLROS_EVENT_ONLINE_STATUS_CHANGED:
		{
			rageDebugf1("netSCGamerDataMgr::OnRosEvent(RLROS_EVENT_ONLINE_STATUS_CHANGED)");

			const rlRosEventOnlineStatusChanged& changeEvnt = static_cast<const rlRosEventOnlineStatusChanged&>(evnt);

			//If we're online and logged into SC, then request our account info
			if(changeEvnt.m_LocalGamerIndex == m_iActiveGamerIndex && 
				RL_IS_VALID_LOCAL_GAMER_INDEX(m_iActiveGamerIndex) &&
				rlRos::IsOnline(changeEvnt.m_LocalGamerIndex))
			{
				rageDebugf1("Starting request because index %d changed", changeEvnt.m_LocalGamerIndex );
				dDoRequest = true;
			}
		}
		break;
	case RLROS_EVENT_LINK_CHANGED:
		{
			rageDebugf1("netSCGamerDataMgr::OnRosEvent(RLROS_EVENT_LINK_CHANGED)");

			const rlRosEventLinkChanged& linkEvnt = static_cast<const rlRosEventLinkChanged&>(evnt);

			//If we're online and logged into SC, then request our account info
			if( linkEvnt.m_LocalGamerIndex == m_iActiveGamerIndex && 
				RL_IS_VALID_LOCAL_GAMER_INDEX(m_iActiveGamerIndex) &&
				rlRos::IsOnline(linkEvnt.m_LocalGamerIndex))
			{
				rageDebugf1("Starting request because link for index %d changed", linkEvnt.m_LocalGamerIndex );
				dDoRequest = true;
			}
		}
		break;
	case RLROS_EVENT_GET_CREDENTIALS_RESULT:
		{
			netDebug1("netSCGamerDataMgr::OnRosEvent(RLROS_EVENT_GET_CREDENTIALS_RESULT)");
			const rlRosEventGetCredentialsResult& credEvent = static_cast<const rlRosEventGetCredentialsResult&>(evnt);

			if (credEvent.m_LocalGamerIndex == m_iActiveGamerIndex &&
				RL_IS_VALID_LOCAL_GAMER_INDEX(m_iActiveGamerIndex) &&
				rlRos::IsOnline(credEvent.m_LocalGamerIndex))
			{
				netDebug1("Starting request because credentials for index %d changed", credEvent.m_LocalGamerIndex );
				dDoRequest = true;
			}
		}
		break;
	}

	if (dDoRequest)
	{
		RequestDataRefresh();	
	}
}


void netSCGamerDataMgr::OnCloudFileModified( const char* OUTPUT_ONLY(pathSpec), const char* /*fullPath*/ )
{
	rageDebugf1("Received cloud file changed event for %s", pathSpec);
	RequestDataRefresh();
}


#if __BANK
void netSCGamerDataMgr::CreateDebugWidgets( bkBank* pBank )
{
	pBank->PushGroup("SC Gamer Data");
		pBank->AddToggle("Override Cloud Values", &m_bBank_Override);
		pBank->AddToggle("Is Cheater", &m_bBank_IsCheater);
		pBank->AddToggle("Is Rockstar Dev", &m_bBank_IsRockstarDev);
		pBank->AddToggle("Is Rockstar QA", &m_bBank_IsRockstarQA);
		pBank->AddText("XP Bonus", &m_Bank_XPBonus);
		pBank->AddText("XP Bonus Start", &m_Bank_XPBonus_StartTime);
		pBank->AddText("XP Bonus End", &m_Bank_XPBonus_EndTime);
		pBank->AddButton("Generate XP Bonus", datCallback(MFA(netSCGamerDataMgr::Bank_GenerateXPBonus), this));
		pBank->AddButton("Read Latest", datCallback(MFA(netSCGamerDataMgr::Bank_HandleReadData), this));
		pBank->AddButton("Dump Current", datCallback(MFA(netSCGamerDataMgr::Bank_DumpCurrent), this));
	pBank->PopGroup();
}

void netSCGamerDataMgr::Bank_GenerateXPBonus()
{
	if(m_Bank_XPBonus_StartTime > 0 && m_Bank_XPBonus_EndTime > 0 && m_Bank_XPBonus > 0.0f)
	{
		u64 currentTime = rlGetPosixTime();
		u64 startPosix = m_Bank_XPBonus_StartTime;
		u64 endPosix = m_Bank_XPBonus_EndTime;

		// if we're in the time window, pass along the xp bonus
		if(startPosix < currentTime && currentTime < endPosix)
			m_bBank_IsXPBonusActive = true;
	}
}

void netSCGamerDataMgr::Bank_HandleReadData()
{
	rageDebugf1("BANK Reading data");
	if ( m_cloudFile.IsIdle() && RL_IS_VALID_LOCAL_GAMER_INDEX(m_iActiveGamerIndex) )
	{
		RequestInfo(m_iActiveGamerIndex);
	}
}

void netSCGamerDataMgr::Bank_DumpCurrent()
{
	for (int i = 0; i < m_values.GetCount(); ++i)
	{
		m_values[i].DebugPrint();
	}
}
#endif

bool netSCGamerDataMgr::GetValue( u32 namehash, gamerDataValue& val ) const
{
	//Find the value
	for (int i = 0; i < m_values.GetCount(); ++i)
	{
		if(m_values[i].m_nameHash == namehash)
		{
			val = m_values[i];
			return true;
		}
	}

	return false;
}

bool netSCGamerDataMgr::GetValue( u32 namehash, bool &val ) const
{
	gamerDataValue tmp;
	if (GetValue(namehash, tmp))
	{
		if (tmp.m_Type == gamerDataValue::TYPE_BOOL)
		{
			val = tmp.m_bVal;
			return true;
		}
	}

	return false;
}


bool rage::netSCGamerDataMgr::GetValue( u32 namehash, char* outValue, unsigned len ) const
{
	gamerDataValue tmp;
	if (GetValue(namehash, tmp))
	{
		if (tmp.m_Type == gamerDataValue::TYPE_STRING)
		{
			safecpy(outValue, tmp.m_stringValue.c_str(), len);
			return true;
		}
	}

	return false;
}


bool netSCGamerDataMgr::GetValue( u32 namehash, int &val ) const
{
	gamerDataValue tmp;
	if (GetValue(namehash, tmp))
	{
		if (tmp.m_Type == gamerDataValue::TYPE_INT)
		{
			val = tmp.m_iVal;
			return true;
		}
	}
	return false;
}

bool netSCGamerDataMgr::GetValue( u32 namehash, float &val ) const
{
	gamerDataValue tmp;
	if (GetValue(namehash, tmp))
	{
		if (tmp.m_Type == gamerDataValue::TYPE_FLOAT)
		{
			val = tmp.m_fVal;
			return true;
		}
	}
	return false;
}

bool netSCGamerDataMgr::GetValue( u32 namehash, u32 &val ) const
{
	gamerDataValue tmp;
	if (GetValue(namehash, tmp))
	{
		if (tmp.m_Type == gamerDataValue::TYPE_UNS)
		{
			val = tmp.m_uVal;
			return true;
		}
	}
	return false;
}

bool netSCGamerDataMgr::IsCheater() const
{
	//@@: range NETSCGAMERDATAMGR_ISCHEATER {
	return 
#if __BANK
		m_bBank_Override ? 
	m_bBank_IsCheater :
#endif
	m_bIsCheater;
	//@@: } NETSCGAMERDATAMGR_ISCHEATER
}

bool netSCGamerDataMgr::IsCheaterSC() const
{
	//@@: range NETSCGAMERDATAMGR_ISCHEATERSC {
	static const u32 CHEATER_HASH = ATSTRINGHASH("bIsCheater",0x79c325);
	static const u32 i_CHEATER_HASH = ATSTRINGHASH("iIsCheater",0x508D9DAA);
	static const u32 CHEATER_START_HASH = ATSTRINGHASH("uCheater_Start",0x4A06FEDF);
	static const u32 CHEATER_END_HASH =	ATSTRINGHASH("uCheater_End",0x44B64EFD);

	bool bIsCheater = false;
	int iIsCheater = 0;
	
	GetValue(CHEATER_HASH, bIsCheater); 
	GetValue(i_CHEATER_HASH, iIsCheater);
	
	if(iIsCheater > 0)
		bIsCheater = true;
	
	//See if a cheater flag is set
	if(bIsCheater)
	{
		//If we are marked as a cheater, check and see if it's expired.
		u32 start = 0, end = 0;
		GetValue(CHEATER_START_HASH, start);
		GetValue(CHEATER_END_HASH, end);
		if ( start > 0 || end > 0 )
		{
			u64 currentTime = rlGetPosixTime();
			u64 startPosix = start;
			u64 endPosix = end;

			//If we're given a start date and now is before the start date, no dice
			if (startPosix > 0 && currentTime < startPosix )
			{
				bIsCheater = false;
			}

			//If we're still a cheater, have an end date, and we're beyond the end date, NOT A CHEATER
			if (bIsCheater && endPosix > 0 && endPosix < currentTime)
			{
				bIsCheater = false;
			}
		}
	}

	return bIsCheater;
	//@@: } NETSCGAMERDATAMGR_ISCHEATERSC
}

bool netSCGamerDataMgr::IsBadSportSC() const
{
	static const u32 BADSPORT_HASH = ATSTRINGHASH("bIsBadSport", 0x4F12E69F);
	static const u32 BADSPORT_START_HASH = ATSTRINGHASH("uBadSport_Start",0x7EE806C5);
	static const u32 BADSPORT_END_HASH = ATSTRINGHASH("uBadSport_End",0x5058025E);

	bool bIsBadSport = false;

	GetValue(BADSPORT_HASH, bIsBadSport); 

	//See if a badSport flag is set
	if(bIsBadSport)
	{
		//If we are marked as a badsport, check and see if it's expired.
		u32 start = 0, end = 0;
		GetValue(BADSPORT_START_HASH, start);
		GetValue(BADSPORT_END_HASH, end);

		const u64 currentTime = rlGetPosixTime();
		const u64 startPosix = start;
		const u64 endPosix = end;

		//If we're given a start date and now is before the start date, no dice
		if (startPosix > 0 && currentTime < startPosix )
		{
			bIsBadSport = false;
		}

		//If we're given and end date and we're beyond it, not bad sport
		if (endPosix > 0 && endPosix < currentTime)
		{
			bIsBadSport = false;
		}
	}

	return bIsBadSport;
}

bool netSCGamerDataMgr::IsRockstarDev() const
{
	return 
#if __BANK
		m_bBank_Override ? 
		m_bBank_IsRockstarDev :
#endif
	m_bIsRockstarDev;
}

bool netSCGamerDataMgr::IsRockstarDevSC() const
{
	static const u32 HASH = ATSTRINGHASH("bIsRockstarDev",0x77e486a2);

	bool bIsDev = false;
	return GetValue(HASH, bIsDev) && bIsDev;
}

bool netSCGamerDataMgr::IsRockstarQA() const
{
	return 
#if __BANK
		m_bBank_Override ? 
        m_bBank_IsRockstarQA :
#endif
	m_bIsRockstarQA;
}

bool netSCGamerDataMgr::IsRockstarQASC() const
{
	static const u32 HASH = ATSTRINGHASH("bIsRockstarQA",0x2b576c09);

	bool bIsQA = false;
	return GetValue(HASH, bIsQA) && bIsQA;
}

bool netSCGamerDataMgr::GetActiveXPBonus(float &out_xpBonus) const
{
	out_xpBonus = 0.0f;

	if(
#if __BANK
	m_bBank_IsXPBonusActive
#else
	m_bIsXPBonusActive
#endif
	)
	{
		out_xpBonus = 
#if __BANK
		m_Bank_XPBonus;
#else
		m_fXPBonus;
#endif
	}

	return 
#if __BANK
		m_bBank_IsXPBonusActive;
#else
		m_bIsXPBonusActive;
#endif
}

bool netSCGamerDataMgr::HasCheaterOverride() const
{
	//@@: range NETSCGAMERDATAMGR_HASCHEATEROVERRIDE {
	static const u32 HCO_HASH = ATSTRINGHASH("bIsCheaterOverride", 0xc71e28c5);

	bool bHasCheaterOverride = false;
	return GetValue(HCO_HASH, bHasCheaterOverride) && bHasCheaterOverride; 
	//@@: } NETSCGAMERDATAMGR_HASCHEATEROVERRIDE

}

int netSCGamerDataMgr::GetCheaterReason() const
{
    static const u32 HASH = ATSTRINGHASH("uCheaterReason", 0xbc707798);

    int uCheaterReason = -1;
    GetValue(HASH, uCheaterReason);
    return uCheaterReason;
}

bool netSCGamerDataMgr::IsBadSport() const
{
    return m_bIsBadSport;
}

bool netSCGamerDataMgr::HasBadSportOverride() const
{
    static const u32 HASH = ATSTRINGHASH("bIsBadSportOverride", 0xbfa9a875);

    bool bHasBadSportOverride = false;
    return GetValue(HASH, bHasBadSportOverride) && bHasBadSportOverride;
}

bool netSCGamerDataMgr::BadSportOverrideStart(u32& posixTimeStart) const
{
	static const u32 HASH = ATSTRINGHASH("uBadSportOverrideStart", 0xc9424f92);

	posixTimeStart = 0;
	return GetValue(HASH, posixTimeStart);
}

bool netSCGamerDataMgr::BadSportOverrideEnd(u32& posixTimeEnd) const
{
	static const u32 HASH = ATSTRINGHASH("uBadSportOverrideEnd", 0x853d4c3d);

	posixTimeEnd = 0;
	return GetValue(HASH, posixTimeEnd);
}

bool netSCGamerDataMgr::IsAutoMuted() const
{
    static const u32 HASH = ATSTRINGHASH("bApplyAutoMute", 0x009f8e7f);

    bool bApplyAutoMute = false;
    return GetValue(HASH, bApplyAutoMute) && bApplyAutoMute;
}

bool netSCGamerDataMgr::HasAutoMuteOverride() const
{
    static const u32 HASH = ATSTRINGHASH("bIgnoreAutoMute", 0xfc58e972);

    bool bIgnoreAutoMute = false;
    return GetValue(HASH, bIgnoreAutoMute) && bIgnoreAutoMute;
}

bool netSCGamerDataMgr::GetActiveXPBonusSC(float &out_xpBonus) const
{
	static const u32 XPBONUS_HASH = ATSTRINGHASH("fSC_XPBonus",0x885e8ac2);
	static const u32 XPBONUS_START_HASH = ATSTRINGHASH("uSC_XPBonus_Start",0x0fb118660);
	static const u32 XPBONUS_END_HASH =	ATSTRINGHASH("uSC_XPBonus_End",0x003aceaa1);

	float xpBonus = 0.0f;
	out_xpBonus = 0.0f;
	u32 start = 0, end = 0;

	if (GetValue(XPBONUS_START_HASH, start) && GetValue(XPBONUS_END_HASH, end) && GetValue(XPBONUS_HASH, xpBonus))
	{
		if(start > 0 && end > 0 && xpBonus > 0.0f)
		{
			u64 currentTime = rlGetPosixTime();
			u64 startPosix = start;
			u64 endPosix = end;

			//If we're in the time window, pass along the xp bonus
			if (startPosix < currentTime && currentTime < endPosix )
			{
				out_xpBonus = xpBonus;
				return true;
			}
		}
	}

	return false;
}

void netSCGamerDataMgr::gamerDataValue::Set( const char* name, bool val )
{
	BANK_ONLY(safecpy(m_debugName, name );)
	m_nameHash = atStringHash(name);
	m_Type = TYPE_BOOL;
	m_bVal = val;
}

void netSCGamerDataMgr::gamerDataValue::Set( const char* name, int val )
{
	BANK_ONLY(safecpy(m_debugName, name );)
		m_nameHash = atStringHash(name);
	m_Type = TYPE_INT;
	m_iVal = val;
}

void netSCGamerDataMgr::gamerDataValue::Set( const char* name, float val )
{
	BANK_ONLY(safecpy(m_debugName, name );)
		m_nameHash = atStringHash(name);
	m_Type = TYPE_FLOAT;
	m_fVal = val;
}

void netSCGamerDataMgr::gamerDataValue::Set( const char* name, u32 val )
{
	BANK_ONLY(safecpy(m_debugName, name );)
		m_nameHash = atStringHash(name);
	m_Type = TYPE_UNS;
	m_uVal = val;
}

void rage::netSCGamerDataMgr::gamerDataValue::Set( const char* name, const char* val )
{
	BANK_ONLY(safecpy(m_debugName, name );)
	m_nameHash = atStringHash(name);
	m_Type = TYPE_STRING;
	m_uVal = 0;
	m_stringValue = val;
}


#if __BANK
void netSCGamerDataMgr::gamerDataValue::DebugPrint()
{
	switch (m_Type)
	{
	case TYPE_INVALID:
		rageDisplayf("Ros VAlue - INVALID");
		break;
	case TYPE_INT:
		rageDisplayf("%s [0x%08x] INT: %d", m_debugName, m_nameHash, m_iVal);
		break;
	case TYPE_BOOL:
		rageDisplayf("%s [0x%08x] BOOL: %s", m_debugName, m_nameHash, m_bVal ? "true" : "false");
		break;
	case TYPE_FLOAT:
		rageDisplayf("%s [0x%08x] FLOAT: %f", m_debugName, m_nameHash, m_fVal);
		break;
	case TYPE_UNS:
		rageDisplayf("%s [0x%08x] UNS: %u", m_debugName, m_nameHash, m_uVal);
		break;
	case TYPE_STRING:
		rageDisplayf("%s [0x%08x] STRING: %s", m_debugName, m_nameHash, m_stringValue.c_str());
		break;
	}
}

bool netSCGamerDataMgr::AddValue( gamerDataValue& newValue )
{
	//Find the value
	for (int i = 0; i < m_values.GetCount(); ++i)
	{
		if(m_values[i].m_nameHash == newValue.m_nameHash)
		{
			m_values[i] = newValue;
			return true;
		}
	}

	m_values.PushAndGrow(newValue);

	return true;
}

void netSCGamerDataMgr::OverrideCheaterFlag( bool bCheater )
{
	//@@: range NETSCGAMERDATAMGR_OVERRIDECHEATERFLAG {
	if (!bCheater)
	{
		return;
	}

	gamerDataValue newValue;
	newValue.Set("bIsCheater",bCheater);
	AddValue(newValue);

	m_bIsCheater = IsCheaterSC();
	//@@: } NETSCGAMERDATAMGR_OVERRIDECHEATERFLAG
}

void netSCGamerDataMgr::OverrideCheaterRating( int iCheaterRating  )
{
	gamerDataValue newValue;
	newValue.Set("iIsCheater",iCheaterRating);
	AddValue(newValue);

	//@@: range NETSCGAMERDATAMGR_OVERRIDECHEATERRATING_CHECK_SC_CHEATER {
	m_bIsCheater = IsCheaterSC();
	//@@: } NETSCGAMERDATAMGR_OVERRIDECHEATERRATING_CHECK_SC_CHEATER
}

#endif	// __BANK
