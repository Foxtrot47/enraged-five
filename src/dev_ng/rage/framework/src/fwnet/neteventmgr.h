//
// neteventmgr.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_EVENT_MGR_H
#define NETWORK_EVENT_MGR_H

// game includes
#include "atl/dlist.h"
#include "data/base.h"

#include "fwnet/netinterface.h"
#include "fwnet/netbandwidthstats.h"
#include "fwnet/neteventid.h"
#include "fwnet/neteventmessages.h"
#include "fwnet/nettypes.h"
#include "fwnet/netlog.h"
#include "fwnet/netlogsplitter.h"
#include "fwtl/pool.h"

// rage includes
#include "net/connectionmanager.h"

namespace rage
{
    class netBandwidthMgr;
    class netGameEvent;
    class netObject;
    class netPlayer;

//PURPOSE
// Events are one off reliable messages sent to a remote machine - these can be for a variety of things such as
// notifying a remote machine that a gun has been fired or asking a remote machine for control of one of its
// network objects. Some events also require a reply from the machine that they are sent to.
//
// The network event manager deals with sending these events to the correct remote machines, and also keeps a
// list of events that require a reply from the machine they are sent to. The event manager at the other end
// will automatically send a reply if need be and the one at this end will receive it, process it and remove
// it from the list. Events that do not require a reply are not added to the list.
class netEventMgr
{
public:

    // This is a typedef for a function pointer for an event handler
    typedef void (*fnEventHandler)(datBitBuffer& msgBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence);

    //PURPOSE
    // This is a convenience class for allocating the list nodes from a pool
    class atDNetEventNode : public atDNode<netGameEvent*, datBase>
    {
    public:

        FW_REGISTER_CLASS_POOL(atDNetEventNode);
    };

    //PURPOSE
    // Class constructor
    //PARAMS
    // bandwidthMgr - used for tracking of bandwidth usage by this class
    netEventMgr(netBandwidthMgr &bandwidthMgr);
    ~netEventMgr();

    //PURPOSE
    // Initialises the event manager at the start of a network session
    bool Init();

    //PURPOSE
    // Performs any per frame processing required by the event manager. This should be called by
    // your application once per frame
    void Update();

    //PURPOSE
    // Shuts down the event manager at the end of the network session.
    void Shutdown();

    //PURPOSE
    // Returns whether the event manager is initialised
    bool IsInitialised() const;

	//PURPOSE
	// Returns whether the event manager is shutting down
	bool IsShuttingDown() const;

	//PURPOSE
    // Registers a network event with the event manager
    //PARAMS
    // eventType    - The event type for the new event to register
    // eventHandler - The event handler for events of this type
    // eventName    - The name of the new event
    void RegisterNetworkEvent(const NetworkEventType eventType, fnEventHandler eventHandler, const char *eventName);

    //PURPOSE
    // returns the event name for a specified type
    //PARAMS
    // eventType - The event type to return the name for
    const char *GetEventNameFromType(const NetworkEventType eventType) const;

    //PURPOSE
    // Called when a new player joins the game
    //PARAMS
    // player - the player joining the game
    void PlayerHasJoined(const netPlayer& player);

    //PURPOSE
    // Called when an existing player leaves the game
    //PARAMS
    // player - the player leaving the game
    void PlayerHasLeft(const netPlayer& player);

#if __DEV
    //PURPOSE
    // Returns the number of events on the event list
    int GetSizeOfEventList() const;

    //PURPOSE
    // Returns the number of event IDs currently in use
    int GetNumUsedEventIds() const;
#endif

    //PURPOSE
    // Returns the log for the event manager
    netLoggingInterface &GetLog() { return *m_Log; }

    //PURPOSE
    // Returns the log splitter for the event manager
    netLoggingInterface &GetLogSplitter() { return *m_LogSplitter; }

    //PURPOSE
    // Returns the next sequence number for the specified event type
    //PARAMS
    // eventType - The event type to return the sequence number for
    netSequence GetNextSequence(const NetworkEventType eventType);

    //PURPOSE
    // Handles an event that has been received over the network
    //PARAMS
    // event           - The event received
    // messageBuffer   - The message buffer containing the data for the event handled
    // player          - The player that sent the event
    // messageSequence - The message sequence number for the event
    // eventID         - The event ID for the event
    // eventIDSequence - The sequence of the event ID
    void HandleEvent(netGameEvent         *event,
                     datBitBuffer         &messageBuffer,
                     const netPlayer      &fromPlayer,
                     const netPlayer      &toPlayer,
                     const netSequence    messageSequence,
                     const EventId        eventID,
                     const unsigned       eventIDSequence);

    //PURPOSE
    // Prepares an event for sending over the network
    //PARAMS
    // event - The event to prepare
    void PrepareEvent(netGameEvent *event);

    //PURPOSE
    // Checks if there is any space in the event pool for a new event
    //PARAMS
    // tryToCreateSpace - Indicates whether the event manager should try to make space for a new event
    bool CheckForSpaceInPool(bool tryToCreateSpace = true);

    //PURPOSE
    // Returns the first event on the event list of the specified type
    //PARAMS
    // eventType - The type of event to find
    netGameEvent *GetExistingEventFromEventList(const NetworkEventType eventType);

    //PURPOSE
    // destroys all events on the event queue of the specified type
    //PARAMS
    // eventType - The type of event to destroy
    void DestroyAllEventsOfType(const NetworkEventType eventType);

    //PURPOSE
    // Returns the head of the event list
    atDNode<netGameEvent*, datBase> *GetEventListHead();

    //PURPOSE
    // Returns the time since an event of the specified type was sent to the specified player
    unsigned GetTimeSinceEventSentToPlayer(const NetworkEventType eventType, PhysicalPlayerIndex playerIndex) const;

#if __BANK

    //PURPOSE
    // Returns the number of unreliable event manager messages sent since the last call to ResetMessageCounts()
    unsigned GetNumUnreliableEventMessagesSent() const { return m_NumUnreliableEventMessagesSent; }   

    //PURPOSE
    // Returns the number of reliable event manager messages sent since the last call to ResetMessageCounts()
    unsigned GetNumReliableEventMessagesSent() const { return m_NumReliableEventMessagesSent; }

    //PURPOSE
    // Reset the counts of event manager messages sent
    void ResetMessageCounts() { m_NumUnreliableEventMessagesSent = m_NumReliableEventMessagesSent = 0; }

	netBandwidthStatistics& GetBandwidthStatistics() { return m_BandwidthStats; }
#endif 

	atFixedArray<u32, netGameEvent::NETEVENTTYPE_MAXTYPES>& GetEventTriggerCounters() { return m_EventTriggerCounters; }

private:

    //PURPOSE
    // Handles an incoming network message
    //PARAMS
    // messageData - Data describing the incoming message
    void OnMessageReceived(const ReceivedMessageData &messageData);

    //PURPOSE
    // Handles a packed events message
    //PARAMS
    // messageData - Data describing the incoming message
    void HandlePackedEventsMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Handles a packed event reliables message
    //PARAMS
    // messageData - Data describing the incoming message
    void HandlePackedEventReliablesMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Sends an event to the players in scope of it
    //PARAMS
    // event - The event to send
    void SendEvent(class netGameEvent *event);

    //PURPOSE
    // Sends an ACK for the specified event to the specified player
    //PARAMS
    // gameEvent       - The event to ACK
    // toPlayer        - The player to send the event Ack to
    // fromPlayer      - The player sending the event Ack
    // eventID         - The event ID of the event to ACK
    // eventIDSequence - The sequence of the event ID
    void SendAck(netGameEvent    *gameEvent,
                 const netPlayer &toPlayer,
                 const netPlayer &fromPlayer,
                 const EventId    eventID,
                 const unsigned   eventIDSequence);

    //PURPOSE
    // Prepares and send a reply to the specified event to the specified player
    //PARAMS
    // gameEvent       - The event to reply to
    // toPlayer        - The player to send the event reply to
    // fromPlayer      - The player sending the event reply
    // eventID         - The event ID of the event to reply to
    // eventIDSequence - The sequence of the event ID
    void SendReply(netGameEvent    *gameEvent,
                   const netPlayer &toPlayer,
                   const netPlayer &fromPlayer,
                   const EventId    eventID,
                   const unsigned   eventIDSequence);

    //PURPOSE
    // Marks all events as not being packed to the specified player
    //PARAMS
    // player - The player to mark the events as not being packed to
    void ResetAllEventsAlreadyBeenPackedFlags(const netPlayer &player);

    //PURPOSE
    // Adds a event to the packed events message for the specified player
    //PARAMS
    // toPlayer          - The player to send the event to
    // fromPlayer        - The player sending the event
    // eventType         - The type of event to pack
    // eventID           - The event ID of the event to pack
    // eventIDSequence   - The sequence of the event ID
    // isSequential      - Indicates whether this is a sequential event or not (see netevent.h for details)
    // eventTypeSequence - The sequence number for the type of event
    // eventData         - The event data to pack
    // eventDataSize     - The size of the event data to pack
    void AddEvent(const netPlayer       &toPlayer,
                  const netPlayer       &fromPlayer,
                  const NetworkEventType eventType,
                  const EventId          eventID,
                  const unsigned         eventIDSequence,
                  bool                   isSequential,
                  netSequence            eventTypeSequence,
                  u8                    *eventData,
                  const unsigned         eventDataSize);

    //PURPOSE
    // Adds an event ACK to the packed events reliable message for the specified player
    //PARAMS
    // toPlayer        - The player to send the event Ack to
    // fromPlayer      - The player sending the event Ack
    // eventID         - The event ID of the event being ACKed
    // eventIDSequence - The sequence of the event ID
    void AddEventAck(const netPlayer       &toPlayer,
                     const netPlayer       &fromPlayer,
                     const EventId          eventID,
                     const unsigned         eventIDSequence);

    //PURPOSE
    // Adds an event reply to the packed events reliable message for the specified player
    //PARAMS
    // toPlayer           - The player to send the event reply to
    // fromPlayer         - The player sending the event reply
    // eventID            - The event ID of the event being replied to
    // eventIDSequence    - The sequence of the event ID
    // eventReplyData     - The event reply data to pack
    // eventReplyDataSize - The size of the event reply data to pack
    void AddEventReply(const netPlayer       &toPlayer,
                       const netPlayer       &fromPlayer,
                       const EventId          eventID,
                       const unsigned         eventIDSequence,
                       const NetworkEventType eventType,
                       u8                    *eventReplyData,
                       const unsigned         eventReplyDataSize);

    //PURPOSE
    // Sends the packed events message for the specified player over the network
    //PARAMS
    // player - The player to send the message to
    void SendPackedEventsMessage(const netPlayer &player);

    //PURPOSE
    // Sends the packed event reliables message for the specified player over the network
    //PARAMS
    // player - The player to send the message to
    void SendPackedEventReliablesMessage(const netPlayer &player);

    //PURPOSE
    // Process packed events data received over the network
    //PARAMS
    // messageBuffer - The message buffer containing the events data to unpack
    // player        - The player the events data was received from
    void ProcessPackedEvents(datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);

    //PURPOSE
    // Process packed event ACK data received over the network
    //PARAMS
    // messageBuffer - The message buffer containing the events data to unpack
    // player        - The player the event ACK  data was received from
    void ProcessPackedEventAcks(datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);

    //PURPOSE
    // Process packed event reply data received over the network
    //PARAMS
    // messageBuffer - The message buffer containing the event reply data to unpack
    // player        - The player the event reply data was received from
    void ProcessPackedEventReplies(datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);

    //PURPOSE
    // Processes data for an individual event received over the network
    //PARAMS
    // player            - The player the event data was received from
    // eventType         - The event type of the event received
    // eventID           - The event ID of the event received
    // eventIDSequence   - The sequence associated with the event ID
    // eventTypeSequence - The sequence number for events of this type
    // eventData   - The data for the event received
    void ProcessEventData(const netPlayer      &fromPlayer,
                          const netPlayer      &toPlayer,
                          NetworkEventType      eventType,
                          EventId               eventID,
                          unsigned              eventIDSequence,
                          netSequence           eventTypeSequence,
                          datBitBuffer         &eventData);

    //PURPOSE
    // Processes data for an individual event ACK received over the network
    //PARAMS
    // player          - The player the event ACK data was received from
    // eventID         - The event ID of the event being ACKed
    // eventIDSequence - The sequence associated with the event ID
    void ProcessEventAckData(const netPlayer &fromPlayer,
                             const netPlayer &toPlayer,
                             EventId          eventID,
                             unsigned         eventIDSequence);

    //PURPOSE
    // Processes data for an individual event reply received over the network
    //PARAMS
    // player          - The player the event reply data was received from
    // eventID         - The event ID of the event being replied to
    // eventIDSequence - The sequence associated with the event ID
    // eventReplyData  - The event reply data
    void ProcessEventReplyData(const netPlayer &fromPlayer,
                               const netPlayer &toPlayer,
                               EventId          eventID,
                               unsigned         eventIDSequence,
                               datBitBuffer    &eventReplyData);

private:

    // The maximum number of concurrent event IDs supported by the system
    static const unsigned MAX_NUM_EVENT_IDS	= (1<<netEventID::SIZEOF_EVENT_ID);

    // Typedef for an array of sequence numbers received from the different players in the session
    typedef atFixedArray< netSequence, MAX_NUM_PHYSICAL_PLAYERS > EventConnectionSequences;

    // Typedef for an array of the last time an event of a given type was sent
    typedef atFixedArray< unsigned, MAX_NUM_PHYSICAL_PLAYERS> LastTimeEventSent;

    // A wrapper for a callback function called when a network message is received over the network
    NetworkPlayerEventDelegate m_Dlgt;

    // A list of events waiting for ACKS/Replies
    atDList<netGameEvent*, datBase> m_EventList;

    // A buffer used to construct messages
    u8 m_MessageBuffer[MAX_MESSAGE_PAYLOAD_BYTES];

    // The array of event ids
    atFixedArray< netEventID, MAX_NUM_EVENT_IDS > m_EventIds;

    // An array of last sequence numbers received (used by sequential events) down each connection
    atFixedArray< EventConnectionSequences, netGameEvent::NETEVENTTYPE_MAXTYPES > m_EventSequences;

    // An array of packed event messages to send to each connection
    atFixedArray< CMsgPackedEvents, MAX_NUM_PHYSICAL_PLAYERS > m_PackedEventsMsgs;

    // An array of packed event messages to send to each connection
    atFixedArray< CMsgPackedEventReliablesMsgs, MAX_NUM_PHYSICAL_PLAYERS > m_PackedEventReliablesMsgs;

    // An array of packed event messages to send to each connection
    atFixedArray< netSequence, netGameEvent::NETEVENTTYPE_MAXTYPES > m_LocalEventTypeSequences;

    // An array of event handlers for event types
    atFixedArray< fnEventHandler, netGameEvent::NETEVENTTYPE_MAXTYPES > m_EventHandlers;

    // An array of event names for event types
    atFixedArray< const char *, netGameEvent::NETEVENTTYPE_MAXTYPES > m_EventNames;

    // An array of the last time a event of a given type was sent to a given player
    atFixedArray< LastTimeEventSent, netGameEvent::NETEVENTTYPE_MAXTYPES > m_LastTimeEventSent;

	// An array of counters tracking how many times each type of event triggers
	atFixedArray<u32, netGameEvent::NETEVENTTYPE_MAXTYPES> m_EventTriggerCounters;

    // The logging objects for the event manager
    netLogSplitter *m_LogSplitter;
    netLog         *m_Log;

    // The index to start looking for free event IDs (in order to reduce the constant reusing of the same event IDs)
    int m_EventIDSearchStart;

    netBandwidthMgr &m_BandwidthMgr; // The bandwidth manager

    // Flag indicating whether the event manager has been initialised
	bool m_IsInitialised : 1;
	bool m_IsShuttingDown: 1;
	bool m_IsProcessingEventQueue : 1;

    static netGameEvent *ms_EventBeingProcessed; // the current event being processed

#if __BANK
    unsigned m_NumUnreliableEventMessagesSent; // the number of unreliable event manager messages sent since the last call to ResetMessageCounts()
    unsigned m_NumReliableEventMessagesSent;   // the number of reliable event manager messages sent since the last call to ResetMessageCounts()

    // The bandwidth statistics recorder for event management
	netBandwidthStatistics m_BandwidthStats;

	// Bandwidth recorders for the data in the different message types that is not event data
	RecorderId m_EventHeaderRecId;
	RecorderId m_AckMsgRecId;
	RecorderId m_ReplyMsgHeaderRecId;
	RecorderId m_PackedEventsMsgHeaderRecId;

	// Array of bandwidth recorders for each event type
	atFixedArray< RecorderId, netGameEvent::NETEVENTTYPE_MAXTYPES > m_EventRecorderIds;

	// Array of bandwidth recorders for replies for each event type
	atFixedArray< RecorderId, netGameEvent::NETEVENTTYPE_MAXTYPES > m_EventReplyRecorderIds;
#endif
};

} // namespace rage

#endif  // NETWORK_EVENT_MGR_H
