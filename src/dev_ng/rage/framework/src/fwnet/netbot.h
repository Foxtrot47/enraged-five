// 
// netbot.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_BOT_H
#define NET_BOT_H

#include "fwnet/nettypes.h"

#if ENABLE_NETWORK_BOTS

#include "net/status.h"
#include "rline/rl.h"
#include "rline/rlgamerinfo.h"
#include "script/thread.h"

namespace rage
{
class netPlayer;
class netPlayerMgrBase;
class rlSessionInfo;

//PURPOSE
// This class is the base representation of a network bot. Network bots are used for debug purposes to
// fill network sessions with a limited number of physical machines. They are essentially fake players in a network game.
class netBot
{
    friend netPlayerMgrBase;
public:

    //PURPOSE
    // Class constructor
    //PARAMS
    // botIndex  - The index of the network bot based on the number of bots in the system
    // gamerInfo - Gamer info to associated with this bot, NULL for locally created bots
    netBot(unsigned botIndex, const rlGamerInfo *gamerInfo = 0);

    virtual ~netBot();

    const rlGamerInfo &GetGamerInfo() const { return m_GamerInfo; }

    //PURPOSE
    // Accessor for the network player associated with this network bot. Virtual
    // so derived classes can override this and return a project specific type
    virtual netPlayer *GetNetPlayer() { return m_NetPlayer; }

    //PURPOSE
    // Sets the network player associated with this network bot. Virtual to
    // allow derived classes to perform custom handling when the player is set
    //PARAMS
    // player - The player to associate with this network bot
    virtual void SetNetPlayer(netPlayer *player);

    //PURPOSE
    // Returns the local index of this network bot. This is index specific
    // to the number of network bots created by the local machine
    //PARAMS
    // localIndex - Return value for the local index of this network bot
	virtual bool GetLocalIndex(unsigned &localIndex) const = 0;

    //PURPOSE
    // Returns whether this network bot is active (i.e. can be communicated with in a game)
    bool IsActive() const;

    //PURPOSE
    // Returns whether this network bot is currently in a match
    bool IsInMatch() const;

    //PURPOSE
    // Called when a new player joins the game
    //PARAMS
    // player - the player joining the game
    virtual void PlayerHasJoined(const netPlayer& player);

    //PURPOSE
    // Called when an existing player leaves the game
    //PARAMS
    // player - the player leaving the game
    virtual void PlayerHasLeft(const netPlayer& player);

    //PURPOSE
    // Joins a network bot to the specified session using the specified slot type
    //PARAMS
    // sessionInfo - the session to join
    // slotType    - the type of slot to join into (public/private etc...)
    virtual bool JoinMatch(const rlSessionInfo& sessionInfo,
                   const rlSlotType slotType) = 0;

    //PURPOSE
    // Leaves the network bot from the current session
    virtual bool LeaveMatch() = 0;

    //PURPOSE
    // Update function - should be called once per frame on all network bots.
    // Virtual to allow derived classes to perform custom updating of bots
    virtual void Update();

	//PURPOSE
	// Returns the local script Id of this network bot. This is the 
	// id of the script controlling this bot.
	//PARAMS
	// threadId - Return value for the script Id assigned to this network bot.
	virtual bool GetScriptId(scrThreadId& threadId) const { threadId = THREAD_INVALID; return false; }

	//PURPOSE
	// Returns the local script name of this network bot. This is the 
	// name of the script controlling this bot.
	virtual const char*  GetScriptName() const { return "--No Script--"; }

protected:

    enum
    {
        BOT_STATE_IDLE,
        BOT_STATE_JOINING,
        BOT_STATE_CREATE_PLAYER,
        BOT_STATE_LEAVING,
        BOT_STATE_IN_MATCH,
        BOT_STATE_USER             // used for extending states in derived classes
    };

    unsigned GetState() const { return m_State; }
    void SetState(unsigned newState);

    virtual const char *GetStateName(unsigned state) const;

    netStatus &GetStatus() { return m_MyStatus; }

    void SetActive(bool active);

private:

    netBot();
    netBot(const netBot &);

    netBot &operator=(const netBot &);

    bool        m_Active;          // Indicates whether the network bot is active
    rlGamerInfo m_GamerInfo;       // The gamer info for the network bot
    netStatus   m_MyStatus;        // Status variable used for asynchronous network operations related to this bot
    unsigned    m_State;           // Current state variable for the bot, used for internal state machine processing
    netPlayer  *m_NetPlayer;       // The network player associated with this bot
    unsigned    m_ActivationTimer; // used to delay activating network bots once they are added to allow them to get into remote sessions
};

} // namespace rage

#endif // ENABLE_NETWORK_BOTS

#endif // NET_BOT_H
