//
// name:		NetworkTypes.h
// description:	Network types shared by network code
// written by:	John Gurney
//

#ifndef NETWORK_TYPES_H
#define NETWORK_TYPES_H

#include "rline/rl.h"

namespace rage
{

#if __DEV
#define USE_MSG_TAG 1
#else
#define USE_MSG_TAG 0
#endif

#if USE_MSG_TAG
#define TAG_SIZE 16
#define SET_MSG_TAG(a) \
    AssertVerify((a).WriteUns(0xabcd, TAG_SIZE));
#define GET_MSG_TAG(a) \
    {\
        unsigned tag;\
        AssertVerify((a).ReadUns(tag, TAG_SIZE) && 0xabcd == tag);\
    }
#else
#define SET_MSG_TAG(a)
#define GET_MSG_TAG(a)
#define TAG_SIZE 0
#endif

// Network bots
#if !__FINAL && !RSG_DURANGO
#define ENABLE_NETWORK_BOTS 0 // disable network bots for good as no-one is using them and they break some rage level debug output
#else
#define ENABLE_NETWORK_BOTS 0
#endif

#if ENABLE_NETWORK_BOTS
#define NETWORK_BOTS_ONLY(x) x

#define SIZEOF_LOCAL_PLAYER_ID (5)

// network bots abstraction layer header sizes
static const u32 ABSTRACTION_LAYER_ID = (((u8)'N') << 24) | // Network
                                        (((u8)'B') << 16) | // Bot
                                        (((u8)'A') << 8)  | // Abstraction
                                        (((u8)'L') << 0);   // Layer

static const unsigned SIZEOF_NBAL_HEADER_IN_BITS  = (sizeof(ABSTRACTION_LAYER_ID)<<3) + 
                                                     SIZEOF_LOCAL_PLAYER_ID + 
                                                     SIZEOF_LOCAL_PLAYER_ID;

static const unsigned SIZEOF_NBAL_HEADER_IN_BYTES = ((SIZEOF_NBAL_HEADER_IN_BITS + 7)>>3);

#else
#define SIZEOF_LOCAL_PLAYER_ID (0)
#define NETWORK_BOTS_ONLY(x)
static const unsigned SIZEOF_NBAL_HEADER_IN_BITS = 0;
static const unsigned SIZEOF_NBAL_HEADER_IN_BYTES = 0;
#endif

// There are 2 indices for each player, the active and physical index. The active index is the index into the player manager's player pile and is 
// not consistent across the network - i.e. our player will not have the same active player index on other machines. The physical player index is the 
// index into the player manager's physical array and is consistent across the network. This index has a smaller range and is used in various places 
// in the network code (eg to set a flag in a network object's cloned state).

typedef u8	ActivePlayerIndex;	// see comment above	
typedef u8	PhysicalPlayerIndex;	// see comment above
typedef u16 ObjectId;
typedef u16 NetObjFlags;
typedef u32 PlayerFlags;
typedef u16 TeamFlags;
typedef u16 NetworkArrayHandlerType;
typedef u16 NetworkEventType;
typedef u16 NetworkObjectType;
BANK_ONLY(typedef s16 RecorderId;)
typedef u32 DataNodeFlags;

static const ActivePlayerIndex		MAX_NUM_ACTIVE_PLAYERS = 32;
static const PhysicalPlayerIndex	MAX_NUM_PHYSICAL_PLAYERS = 32;
static const PhysicalPlayerIndex	MAX_NUM_SCRIPT_PARTICIPANTS = 32;

#define NETWORK_INVALID_OBJECT_ID (0)
#define INVALID_OBJECT_TYPE ((NetworkObjectType)~0)
#define INVALID_PLAYER_INDEX (0xff)
#define INVALID_CONNECTION_ID (-1)
#define INVALID_RECORDER_ID (-1)
#define INVALID_SEQUENCE_ID (0)
#define INVALID_TEAM (-1)
#define ALIGN_TO_16(n) (((n)+0xF)&~0xF)
#define BITS_TO_BYTES(bits)	(((bits)+7)>>3)

#define SIZEOF_OBJECTTYPE (4)
//(250 object ids * 32 players = 8000 different combinations (signed) )
#define SIZEOF_OBJECTID	(13)
#define SIZEOF_NETOBJ_GLOBALFLAGS (8)
#define SIZEOF_NON_PHYSICAL_DATA (7)
#define SIZEOF_REASSIGNPRIORITY (4)

enum ChannelIds
{
	//Network channel for communication
	NETWORK_VOICE_CHANNEL_ID            = 2,
	//Network channel for the game
    NETWORK_GAME_CHANNEL_ID				= 7,
	NETWORK_PARTY_CHANNEL_ID			= 8,
	//Network channel on which sessions will communicate
    //TODO - move to game definitions
    NETWORK_SESSION_VOICE_CHANNEL_ID    = 11,
    NETWORK_SESSION_ACTIVITY_CHANNEL_ID = 12,
	NETWORK_SESSION_GAME_CHANNEL_ID     = 13,

	// reserved by low-level networking - see net.h
	// NET_TUNNELER_CHANNEL_ID			= 14,
	// NET_RESERVED_CHANNEL_ID			= 15,
};

enum
{
    //CPU affinity for background network threads.
#if __XENON || RSG_DURANGO
	NETWORK_CPU_AFFINITY    = 4
#else
    NETWORK_CPU_AFFINITY    = 0
#endif	
};

//PURPOSE
// Enumeration of result codes for the CanClone() function
enum
{
    CC_SUCCESS,              // The object can be cloned to the target machine
    CC_NO_CLONE_FLAG,        // The object has been flagged not to be cloned
    CC_UNREGISTERING,        // The object is in the process of unregistering
    CC_PENDING_REMOVAL,      // The object is pending removal on another remote player
    CC_PENDING_OWNER_CHANGE, // The object is pending owner change to another remote player
    CC_FAIL_USER             // Start of any derived class result codes
};

//PURPOSE
// Enumeration of result codes for the CanPassControl() function
enum
{
    CPC_SUCCESS,                      // The object can be passed control to the target machine
    CPC_FAIL_NOT_CLONED,              // The object has not been cloned on the target machine
    CPC_FAIL_UNREGISTERING,           // The object is in the process of unregistering
    CPC_FAIL_BEING_REASSIGNED,        // The object is currently being reassigned
    CPC_FAIL_IS_CLONE,                // The object is not controlled by the local machine
    CPC_FAIL_IS_MIGRATING,            // The object is in the process of migrating to another machine
    CPC_FAIL_PERSISTANT_OWNER,        // The object has been flagged to remain owned by the local machine
	CPC_FAIL_PENDING_CLONE_OR_REMOVE, // The object is in the process of being cloned or removed on another machine
	CPC_FAIL_NO_SYNC_DATA,			  // The object has no sync data
    CPC_FAIL_USER                     // Start of any derived class result codes
};

//PURPOSE
// Enumeration of result codes for the CanAcceptControl() function
enum
{
    CAC_SUCCESS,               // We can accept control of the object
    CAC_FAIL_WRONG_OWNER,      // The object is owned by a different player
    CAC_FAIL_BEING_REASSIGNED, // The object is currently being reassigned
    CAC_FAIL_IS_MIGRATING,     // The object is in the process of migrating to another machine   
    CAC_FAIL_NO_GAME_OBJECT,   // The object has no game object
    CAC_FAIL_USER              // Start of any derived class result codes
};

// PURPOSE
// Enumeration of result codes for the CanDelete() function
enum
{
	CTD_MIGRATING,	      // Object Migrating
    CTD_BEING_REASSIGNED, // Object being reassigned
	CTD_BASE_END,
};

enum eMigrationType
{
	MIGRATE_PROXIMITY,
	MIGRATE_OUT_OF_SCOPE,
	MIGRATE_SCRIPT,
	MIGRATE_FORCED,
	MIGRATE_REASSIGNMENT,
    MIGRATE_FROZEN_PED,
	NUM_MIGRATION_TYPES
};

#define MAX_NUM_NETOBJPLAYERS (MAX_NUM_PHYSICAL_PLAYERS)
#define MAX_NUM_NETOBJPEDS (110)
#define MAX_NUM_LOCAL_NETOBJPEDS (100)
#define MAX_NUM_NETOBJVEHICLES (160)
#define MAX_NUM_LOCAL_NETOBJVEHICLES (120)
#define MAX_NUM_NETOBJOBJECTS (80)
#define MAX_NUM_NETOBJPICKUPS (80)
#define MAX_NUM_NETOBJPICKUPPLACEMENTS (50)
#define MAX_NUM_NETOBJGLASSPANE (50)
#define MAX_NUM_NETOBJECTS (MAX_NUM_NETOBJPLAYERS + MAX_NUM_NETOBJPEDS + MAX_NUM_NETOBJVEHICLES + MAX_NUM_NETOBJOBJECTS + MAX_NUM_NETOBJPICKUPS + MAX_NUM_NETOBJPICKUPPLACEMENTS + MAX_NUM_NETOBJGLASSPANE + MAX_NUM_NETOBJDOORS)
#define MAX_NUM_ACTIVE_NETWORK_SCRIPTS (50)
#define MAX_NUM_TEAMS (16)
#define MAX_NUM_PARTY_MEMBERS (RL_MAX_PARTY_SIZE)
#define MAX_NUM_NETOBJDOORS (20)

#define MAX_MESSAGE_PAYLOAD_BITS ((netMessage::MAX_BYTE_SIZEOF_PAYLOAD - SIZEOF_NBAL_HEADER_IN_BYTES) << 3) 
#define MAX_MESSAGE_PAYLOAD_BYTES ((MAX_MESSAGE_PAYLOAD_BITS + 7)>>3)

class netObject;
//PURPOSE
//  Definition of a predicate function for determining whether to include a network
//  object when processing a list of objects. Different systems can use predicates to
//  customise the inclusion criteria for the lists they process
typedef bool(*fnNetObjectPredicate)(const netObject *);

//PURPOSE
//  Definition of a callback function for running on all objects matching a specific criteria.
// Different systems can use callbacks to process batch operations on network objects without walking
// network object lists manually
typedef void(*fnNetObjectCallback)(netObject *, void *param);

#define NETWORK_QUITF(cond,string,...) { if (!(cond)) {netInterface::FlushAllLogFiles(true); Displayf(string,##__VA_ARGS__); gnetFatalAssertf(0,string,##__VA_ARGS__); Quitf(0, string,##__VA_ARGS__); } }

} // namespace rage

#endif  // NETWORK_TYPES_H 
