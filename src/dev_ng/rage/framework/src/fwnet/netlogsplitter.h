//
// netlogsplitter.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_NETWORK_LOG_SPLITTER_H_
#define INC_NETWORK_LOG_SPLITTER_H_

#include "fwnet/netlogginginterface.h"

namespace rage
{

//PURPOSE
// This class can be used to direct logging output to 2 or more different log files.
// It stores references to two logging objects and forwards all calls to both of them.
// Output can be directed to more than 2 logging objects via daisychaining (storing another
// netLogSplitter object as one of the logging objects wrapped by this class)
class netLogSplitter : public netLoggingInterface
{
public:

    //PURPOSE
    // Class constructor
    //PARAMS
    // log1 - The first logging object to direct log output to
    // log2 - The second logging object to direct log output to
    netLogSplitter(netLoggingInterface &log1, netLoggingInterface &log2) :
    m_Log1(log1),
    m_Log2(log2)
    {
    }

    //PURPOSE
    // Logs the specified text
    //PARAMS
    // logText - The text to log
    void Log(const char *logText, ...);

    //PURPOSE
    // Logs a line break
    //PARAMS
    // includeFrameCount - Indicates whether to prepend the frame count
    void LineBreak() { m_Log1.LineBreak(); m_Log2.LineBreak(); };

    //PURPOSE
    // Flushes any buffered log data to the output device
    void Flush(bool waitForFlushToComplete = false) { m_Log1.Flush(waitForFlushToComplete); m_Log2.Flush(waitForFlushToComplete); }

    //PURPOSE
    // Disables this log; any further log output will be ignored until Enable() is called
    void Disable() { m_Log1.Disable(); m_Log2.Disable(); };

    //PURPOSE
    // Enables this log
	void Enable() { m_Log1.Enable(); m_Log2.Enable(); };

    //PURPOSE
    // Returns whether this log file is enabled
    virtual bool IsEnabled() const { return m_Log1.IsEnabled() || m_Log2.IsEnabled(); }

    //PURPOSE
    // Writes a key/value pair in a consistent format
    //PARAMS
    // dataName  - Key name
    // dataValue - Value name
    void WriteDataValue(const char *dataName, const char *dataValue, ...);

    //PURPOSE
    // Logs a message header in a consistent format
    //PARAMS
    // received       - Indicates whether the message was sent or received
    // sequence       - Sequence number for the message being sent
    // messageString1 - First portion of message header text
    // messageString2 - Second portion of message header text
    // playerName     - Name (or IP address) of the player message is sent/received from/to
    void WriteMessageHeader(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName);

    //PURPOSE
    // Logs data relative to a specific player in a consistent format
    //PARAMS
    // dataName   - The name of the data to log
    // dataValue  - The value of the data to log
    // playerName - The name of the player this data is related to
    void WritePlayerText(const char *dataName, const char *dataValue, const char *playerName);

    //PURPOSE
    // Writes a logging event to the specified network log file along with any specified data
    //PARAMS
    // eventName          - Name of the event
    // eventData          - Data associated with the event
    // trailingLineBreaks - Number of new lines to append to the end of the logging event
    void WriteEventData(const char *eventName, const char *eventData, unsigned trailingLineBreaks);

    //PURPOSE
    // Writes a node header to the specified network log file
    //PARAMS
    // nodeName - Name of the node
    void WriteNodeHeader(const char *nodeName);

private:

    netLogSplitter &operator= (const netLogSplitter &);

    netLoggingInterface &m_Log1;
    netLoggingInterface &m_Log2;
};

} // namespace rage

#endif // !INC_NETWORK_LOG_SPLITTER_H_
