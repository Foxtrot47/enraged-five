//===========================================================================
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//===========================================================================
#include "netremotelog.h"

// rage headers
#include "atl/singleton.h"
#include "atl/string.h"
#include "data/aes.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "file/asset.h"
#include "file/cachepartition.h"
#include "file/stream.h"
#include "profile/timebars.h"
#include "rline/cloud/rlcloud.h"
#include "rline/ros/rlros.h"
#include "system/param.h"
#include "system/threadpool.h"
#include "zlib/zlib.h"

#include "fwnet/netchannel.h"
#include "fwsys/timer.h"

PARAM(netremoteloggingUseEncryption, "[network] Encrypt remote logs before sending them.");
PARAM(netremoteloggingNoCompression, "[network] Do not compress remote logs before sending them.");
// If neither of the flags below are set the netRemoteLog class will be disabled, unless it is enabled via tunable.
PARAM(netremoteloggingLocalLog, "[network] Specify a local path to post remote logs to.");
PARAM(netremoteloggingEnableUpload, "[network] Allow uploading remote logs to the cloud.");

namespace rage
{

#undef __net_channel
#define __net_channel net_remote_log
RAGE_DEFINE_SUBCHANNEL(net, remote_log);

static sysThreadPool::Thread s_RemoteLogThread;
static sysThreadPool s_RemoteLogThreadPool;

const int netRemoteLog::THREAD_CPU_ID = 0;

netRemoteLog::netRemoteLog()
    : m_CurrentSendState(SendState::None)
    , m_Enabled(false)
    , m_UseCompression(true)
    , m_UseEncryption(false)
    , m_UploadToCloud(false)
{
	m_UploadToCloud  = PARAM_netremoteloggingEnableUpload.Get();
	m_UseEncryption  = PARAM_netremoteloggingUseEncryption.Get();
	m_UseCompression = !PARAM_netremoteloggingNoCompression.Get();

	formatf(m_titleName, "%s", "rage");
	m_WriteBuffer[0] = '\0';
}

void netRemoteLog::Init(const char* titleName)
{
	formatf(m_titleName, "%s", titleName);

	gnetDebug1("Init :: Upload to cloud is %s.", m_UploadToCloud ? "enabled" : "disabled");
	gnetDebug1("Init :: Local log directory is %s.", PARAM_netremoteloggingLocalLog.Get() ? "set" : "not set");

	if (!m_UploadToCloud && !PARAM_netremoteloggingLocalLog.Get())
	{
		gnetDebug1("Init :: Upload to cloud is disabled and no local location to store logs in is specified, so "
		           "netRemoteLog is disabled.");
		m_Enabled = false;
		return;
	}

	m_Enabled = true;

	gnetDebug1("Init :: netRemoteLog is enabled.");

	gnetDebug1("Init :: Compression is %s.", m_UseCompression ? "enabled" : "disabled");
	gnetDebug1("Init :: Encryption is %s.", m_UseEncryption ? "enabled" : "disabled");

	m_WriteBufferBuilder.Init(m_WriteBuffer, WRITE_BUFFER_SIZE, 0);

	m_TempWriteBufferBuilder.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL),
	                              datGrowBuffer::Flags::NULL_TERMINATE);

	m_SendGrowBuffer.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_GAME_VIRTUAL),
	                      datGrowBuffer::Flags::NULL_TERMINATE);

	// At least enough stack to allocate the writing block plus a little extra.
	const int COMPRESS_THREAD_STACK_SIZE = WRITE_BUFFER_SIZE * 2;
	s_RemoteLogThreadPool.Init();
	s_RemoteLogThreadPool.AddThread(&s_RemoteLogThread, "netRemoteLog Compression", THREAD_CPU_ID,
	                                COMPRESS_THREAD_STACK_SIZE);

	m_CurrentSendState = SendState::None;
}

void netRemoteLog::Shutdown()
{
	if (!m_Enabled)
	{
		return;
	}

	if (m_UploadStatus.Pending())
	{
		rlCloud::Cancel(&m_UploadStatus);
	}

	m_WriteBufferBuilder.Clear();
	m_TempWriteBufferBuilder.Clear();

	m_SendGrowBuffer.Clear();

	s_RemoteLogThreadPool.Shutdown();

	m_Enabled = false;
}

void netRemoteLog::Update()
{
	if (!m_Enabled)
	{
		return;
	}

	SendState nextState = SendState::None;

	switch (m_CurrentSendState)
	{
	case SendState::None:
		break;
	case SendState::Compressing:
		{
			SYS_CS_SYNC(m_Cs);
			nextState = CompressLog(m_CurrentSendState);
		}
		break;
	case SendState::Posting:
		{
			SYS_CS_SYNC(m_Cs);
			nextState = PostLog(m_CurrentSendState);
		}
		break;
	}

	if (nextState != m_CurrentSendState)
	{
		switch (nextState)
		{
		case SendState::None:
			break;
		case SendState::Compressing:
			{
				SYS_CS_SYNC(m_Cs);
				nextState = BeginCompressingLog();
			}
			break;
		case SendState::Posting:
			{
				SYS_CS_SYNC(m_Cs);
				nextState = BeginPostingLog();
			}
			break;
		}

		m_CurrentSendState = nextState;
	}
}

void netRemoteLog::BeginSendInternal()
{
	if (!m_Enabled)
	{
		return;
	}

	if (m_CurrentSendState != SendState::None)
	{
		gnetDebug1("%s :: Already busy sending in the %s state.", __FUNCTION__,
		           SendStateToString(m_CurrentSendState));
		return;
	}

	if (m_WriteBufferBuilder.Length() <= 0)
	{
		gnetDebug1("%s :: Write buffer is empty. Nothing to send.", __FUNCTION__);
		return;
	}

	gnetDebug1("%s :: Beginning send operation...", __FUNCTION__);

	SYS_CS_SYNC(m_Cs);
	m_CurrentSendState = BeginCompressingLog();
}

#if !__NO_OUTPUT
void UploadToLocalPath(const char* localPath, fiStream* source)
{
	gnetAssert(source != nullptr);
	gnetAssert(source->Size() >= 0);

	fiStream* pStream = fiStream::Create(localPath);
	if (gnetVerifyf(pStream, "Could not create fiStream from path \"%s\"", localPath))
	{
		gnetDebug1("Posting file to %s", localPath);
		const int BLOCKSIZE = 65536;
		char dataBuff[BLOCKSIZE];
		int bytesRead = 0;
		int size      = rage::Min(source->Size(), BLOCKSIZE);
		do
		{
			bytesRead = source->Read(dataBuff, size);
			if (bytesRead > 0)
			{
				pStream->Write(dataBuff, bytesRead);
			}
		} while (bytesRead > 0);

		pStream->Flush();
		pStream->Close();
	}
}
#endif

netRemoteLog::SendState netRemoteLog::BeginPostingLog()
{
	// Must come here from the Compressing state.
	if (m_CurrentSendState != SendState::Compressing)
	{
		gnetAssertf(false, "%s was called from the %s state!", __FUNCTION__, SendStateToString(m_CurrentSendState));
		return SendState::None;
	}

	char fileName[64];
	{
		unsigned posix = static_cast<unsigned>(rlGetPosixTime());
		formatf(fileName, "%u", posix);
	}

#if !__NO_OUTPUT
	// For debugging purposes, we can write out to a local path using -netremoteloggingLocalLog=<directory path>
	const char* localLogPath = "X:/remotelogs";
	if (PARAM_netremoteloggingLocalLog.Get(localLogPath))
	{
		atString postName;

		postName += localLogPath;
		postName += "/";
		postName += fileName;
		postName += ".rlog";

		fiStream* sendStream = nullptr;
		{
			char path[RAGE_MAX_PATH];
			fiDevice::MakeMemoryFileName(path, RAGE_MAX_PATH, m_SendGrowBuffer.GetBuffer(), m_SendGrowBuffer.Length(),
			                             false, nullptr);
			sendStream = fiStream::Create(path, fiDeviceMemory::GetInstance());
		}

		if (sendStream)
		{
			UploadToLocalPath(postName, sendStream);

			sendStream->Flush();
			sendStream->Close();
		}
	}
#endif

	if (!m_UploadToCloud)
	{
		gnetDebug1("%s :: Uploading of remote logs to the cloud is currently disabled.", __FUNCTION__);
		return SendState::None;
	}

	if (!rlCloud::IsInitialized() || !rlRos::GetCredentials(0).IsValid())
	{
		gnetDebug1("%s :: rlCloud is not initialized or the local player's credentials are invalid. "
		           "Cannot upload to cloud.",
		           __FUNCTION__);
		return SendState::None;
	}

	if (m_UploadStatus.Pending())
	{
		gnetAssertf(false, "%s :: Should not be trying to enter the %s state while mUploadStatus.Pending()",
		            __FUNCTION__, SendStateToString(SendState::Posting));
		return SendState::None;
	}

	{
		atString postName;

		postName += "remotelogs/";
		postName += m_titleName;
		postName += "/";
		postName += fileName;
		postName += ".xml"; // It's not actually XML, we just need to do this so the server will let us in.

		if (gnetVerify(rlCloud::PostMemberFile(0, RL_CLOUD_ONLINE_SERVICE_NATIVE, postName,
		                                       m_SendGrowBuffer.GetBuffer(), m_SendGrowBuffer.Length(),
		                                       rlCloud::POST_REPLACE, nullptr, nullptr, &m_UploadStatus)))
		{
			gnetDebug1("%s :: Posting file to %s", __FUNCTION__, postName.c_str());
			return SendState::Posting;
		}
	}

	return SendState::None;
}

netRemoteLog::SendState netRemoteLog::PostLog(const SendState currentState)
{
	if (currentState != SendState::Posting)
	{
		gnetAssert(false);
		return SendState::None;
	}

	if (m_UploadStatus.Pending())
	{
		return currentState;
	}

	if (m_UploadStatus.Canceled())
	{
		gnetDebug1("%s :: Upload to cloud was canceled.", __FUNCTION__);
	}

	if (m_UploadStatus.Failed())
	{
		gnetDebug1("%s :: Upload to cloud failed.", __FUNCTION__);
	}

	if (m_UploadStatus.Succeeded())
	{
		gnetDebug1("%s :: Successfully uploaded to the cloud.", __FUNCTION__);
	}

	m_SendGrowBuffer.Clear();

	return SendState::None;
}

bool EncryptBuffer(void* buffer, const int bufferSize)
{
	gnetAssertf(bufferSize % 16 == 0, "bufferSize should be a multiple of 16");

	gnetDebug3("Encrypting %d bytes at %p", bufferSize, buffer);

	AES encoder;
	if (gnetVerify(encoder.Encrypt(buffer, bufferSize)))
	{
		return true;
	}

	return false;
}

bool DecryptBuffer(void* buffer, const int bufferSize)
{
	gnetAssertf(bufferSize % 16 == 0, "bufferSize should be a multiple of 16");

	gnetDebug3("Decrypting %d bytes at %p", bufferSize, buffer);

	AES encoder;
	if (gnetVerify(encoder.Decrypt(buffer, bufferSize)))
	{
		return true;
	}

	return false;
}

int WriteBufferToStream(fiStream* stream, void* buffer, const int bufferSize)
{
	gnetDebug3("Writing %d bytes at %p", bufferSize, buffer);

	return stream->Write(buffer, bufferSize);
}

class CompressAndEncryptFileJob : public sysThreadPool::WorkItem
{
public:
	CompressAndEncryptFileJob() : WorkItem(), m_bCompressLogs(true), m_bUseEncryption(false) /*, m_inputSize(0)*/
	{
		m_inputFileName[0]  = '\0';
		m_outputFileName[0] = '\0';
	}

	~CompressAndEncryptFileJob() {}

	void Init(bool bUseCompression, bool bUseEncryption, const char* inputFilePath, const char* outputFilePath)
	{
		m_bUseEncryption = bUseEncryption;
		m_bCompressLogs  = bUseCompression;
		safecpy(m_inputFileName, inputFilePath);
		safecpy(m_outputFileName, outputFilePath);
	}

	void DoWork() override
	{
		RAGE_TIMEBARS_ONLY(pfAutoMarker mkr("netRemoteLog::DoWork", 7));
		fiStream* inputStream = fiStream::Open(m_inputFileName, true);
		if (inputStream)
		{
			fiStream* pOutputLogStream = fiStream::Create(m_outputFileName);

			if (gnetVerifyf(pOutputLogStream, "Failed to open %s to create compressed file", m_outputFileName))
			{
				if (m_bCompressLogs)
				{
					WriteCompressed(inputStream, pOutputLogStream);
				}
				else // If we're not compressing, just blast the entire stream on as a copy.
				{
					WriteUncompressed(inputStream, pOutputLogStream);
				}

				pOutputLogStream->Close();
			}

			inputStream->Close();
		}
	}

private:
	void WriteUncompressed(fiStream* inputStream, fiStream* outputStream)
	{
		gnetDebug1("Writing UNCOMPRESSED file.");
		char dataBuff[65536];
		int bytesRead = 0;
		do
		{
			bytesRead = inputStream->Read(dataBuff, inputStream->Size());
			if (bytesRead > 0)
			{
				if (m_bUseEncryption)
				{
					EncryptBuffer(dataBuff, bytesRead);
				}
				WriteBufferToStream(outputStream, dataBuff, bytesRead);
			}
		} while (bytesRead > 0);
	}

	void WriteCompressed(fiStream* inputStream, fiStream* outputStream)
	{
		gnetDebug1("Compressing file.");
		// Slowly stream the file through Zlib to compress, and output it somewhere.
		const int blockSize = 64;
		Bytef *uncompBlock, *compBlock;

		uncompBlock = rage_new Bytef[blockSize];
		compBlock   = rage_new Bytef[blockSize];

		z_stream c_stream;
		memset(&c_stream, 0, sizeof(c_stream));

		if (deflateInit(&c_stream, Z_BEST_COMPRESSION) >= 0)
		{
			bool shortRead     = false;
			c_stream.avail_in  = 0;
			c_stream.avail_out = blockSize;
			c_stream.next_out  = compBlock;
			while (shortRead == false || c_stream.avail_in != 0)
			{
				if (c_stream.avail_in == 0)
				{
					// Read some data.
					c_stream.avail_in = inputStream->Read(uncompBlock, blockSize);
					c_stream.next_in  = uncompBlock;
					// Have we reached the end of the file.
					if (c_stream.avail_in < blockSize)
						shortRead = true;
				}

				deflate(&c_stream, Z_NO_FLUSH);

				if (c_stream.avail_out == 0)
				{
					// No more space - write it out and reset.
					if (m_bUseEncryption)
					{
						EncryptBuffer(compBlock, blockSize);
					}
					WriteBufferToStream(outputStream, compBlock, blockSize);

					c_stream.avail_out = blockSize;
					c_stream.next_out  = compBlock;
				}
			}

			bool complete = false;
			while (!complete)
			{
				deflate(&c_stream, Z_FINISH);
				if (c_stream.avail_out == 0)
				{
					// No more space - write it out and reset.
					if (m_bUseEncryption)
					{
						EncryptBuffer(compBlock, blockSize);
					}
					WriteBufferToStream(outputStream, compBlock, blockSize);

					c_stream.avail_out = blockSize;
					c_stream.next_out  = compBlock;
					deflate(&c_stream, Z_FINISH);
				}
				else
					complete = true;
			}

			deflateEnd(&c_stream);

			// One last write to finish it off.
			if (m_bUseEncryption)
			{
				EncryptBuffer(compBlock, blockSize);
			}
			WriteBufferToStream(outputStream, compBlock, blockSize);
		}

		delete[] uncompBlock;
		delete[] compBlock;
	}

	bool m_bUseEncryption;
	bool m_bCompressLogs;
	char m_inputFileName[RAGE_MAX_PATH];
	char m_outputFileName[RAGE_MAX_PATH];
};

CompressAndEncryptFileJob s_CompressAndEncryptJob;

netRemoteLog::SendState netRemoteLog::BeginCompressingLog()
{
	if (m_CurrentSendState != SendState::None)
	{
		gnetAssert(false);
		return SendState::None;
	}

	if (m_WriteBufferBuilder.Length() == 0)
	{
		gnetDebug1("Write buffer is empty. No point running compression job.");
		return SendState::None;
	}

	if (gnetVerifyf(!s_CompressAndEncryptJob.Pending(),
	                "Can't begin compressing when a compression task is already in progress"))
	{
		m_SendGrowBuffer.Clear();

		char writeBufferFileName[RAGE_MAX_PATH];
		fiDevice::MakeMemoryFileName(writeBufferFileName, RAGE_MAX_PATH, m_WriteBufferBuilder.ToString(),
		                             m_WriteBufferBuilder.Length(), false, nullptr);

		char growBufferFileName[RAGE_MAX_PATH];
		fiDevice::MakeGrowBufferFileName(growBufferFileName, RAGE_MAX_PATH, &m_SendGrowBuffer);

		s_CompressAndEncryptJob.Init(m_UseCompression, m_UseEncryption, writeBufferFileName, growBufferFileName);

		s_RemoteLogThreadPool.QueueWork(&s_CompressAndEncryptJob);

		return SendState::Compressing;
	}

	return SendState::None;
}

netRemoteLog::SendState netRemoteLog::CompressLog(const SendState currentState)
{
	if (currentState != SendState::Compressing)
	{
		return SendState::None;
	}

	if (s_CompressAndEncryptJob.Pending())
	{
		return SendState::Compressing;
	}

	// TODO: Probably check for if the compression job was canceled or failed before moving on.

	if (s_CompressAndEncryptJob.WasCanceled())
	{
		gnetDebug1("%s :: Compression job was canceled. Can't proceed to %s state.", __FUNCTION__,
		           SendStateToString(SendState::Posting));
		return SendState::None;
	}

	m_WriteBufferBuilder.Clear();
	m_WriteBufferBuilder.Append(m_TempWriteBufferBuilder.ToString());
	m_TempWriteBufferBuilder.Clear();

	return SendState::Posting;
}

unsigned GetAdjustedSystemTime()
{
	static unsigned baseTime;

	if (!baseTime) {
		baseTime = sysTimer::GetSystemMsTime();
	}

	return (sysTimer::GetSystemMsTime() - baseTime);
}


void netRemoteLog::AppendLineInternal(const int msgId, const int argCount, Arg* args)
{
	if (!m_Enabled)
	{
		return;
	}

	SYS_CS_SYNC(m_Cs);

	char buffer[256] = { 0 };

	RsonWriter rw(buffer, RsonFormat::RSON_FORMAT_JSON);

	rw.Begin(nullptr, nullptr);

	rw.WriteUns("ms", GetAdjustedSystemTime());
	rw.WriteUns64("psx", rlGetPosixTime());

	rw.WriteUns("id", msgId);

	if ((argCount > 0) && (args != nullptr))
	{
		rw.BeginArray("args", nullptr);

		for (int i = 0; i < argCount; ++i)
		{
			Arg* arg = &args[i];

			switch (arg->m_Type)
			{
			case Arg::Type::Int:
				rw.WriteInt(nullptr, arg->m_IntValue);
				break;
			case Arg::Type::Float:
				rw.WriteFloat(nullptr, arg->m_FloatValue);
				break;
			case Arg::Type::Double:
				rw.WriteDouble(nullptr, arg->m_DoubleValue);
				break;
			case Arg::Type::String:
				rw.WriteString(nullptr, arg->m_StringValue);
				break;
			}
		}

		rw.End();
	}

	rw.End();

	gnetDebug3("Appending: %s", rw.ToString());

	if (m_CurrentSendState == SendState::Compressing)
	{
		m_TempWriteBufferBuilder.Append(rw.ToString());
		m_TempWriteBufferBuilder.Append('\n');
	}
	else
	{
		m_WriteBufferBuilder.Append(rw.ToString());
		m_WriteBufferBuilder.Append('\n');

		const unsigned LOG_SIZE_SEND_THRESHOLD = 4 * 1024 * 1024; // Arbitrary bignum < write buffer size.
		if (m_WriteBufferBuilder.Length() > LOG_SIZE_SEND_THRESHOLD)
		{
			BeginSendInternal();
		}
	}
}

typedef atSingleton<netRemoteLog> REMOTELOGGER_INST;

void netRemoteLog::InitClass(const char* titleName)
{
	REMOTELOGGER_INST::Instantiate();
	REMOTELOGGER_INST::GetInstance().Init(titleName);
}

void netRemoteLog::UpdateClass()
{
	if (REMOTELOGGER_INST::IsInstantiated())
	{
		REMOTELOGGER_INST::GetInstance().Update();
	}
}

void netRemoteLog::ShutdownClass()
{
	if (REMOTELOGGER_INST::IsInstantiated())
	{
		REMOTELOGGER_INST::GetInstance().Shutdown();
	}
}

void netRemoteLog::AppendLine(const int msgId, const int argCount, Arg* args)
{
	if (REMOTELOGGER_INST::IsInstantiated())
	{
		REMOTELOGGER_INST::GetInstance().AppendLineInternal(msgId, argCount, args);
	}
}

void netRemoteLog::SetTunables(const bool useCompression, const bool useEncryption, const bool uploadToCloud)
{
	if (REMOTELOGGER_INST::IsInstantiated())
	{
		// Parameters override tunables.

		if (!PARAM_netremoteloggingNoCompression.Get())
		{
			bool wasUsingCompression = REMOTELOGGER_INST::GetInstance().m_UseCompression;
			if (wasUsingCompression != useCompression)
			{
				gnetDebug1("%s :: Compression now %s", __FUNCTION__, useCompression ? "enabled" : "disabled");
				REMOTELOGGER_INST::GetInstance().m_UseCompression = useCompression;
			}
		}

		if (!PARAM_netremoteloggingUseEncryption.Get())
		{
			bool wasUsingEncryption = REMOTELOGGER_INST::GetInstance().m_UseEncryption;
			if (wasUsingEncryption != useEncryption)
			{
				gnetDebug1("%s :: Encryption now %s", __FUNCTION__, useEncryption ? "enabled" : "disabled");
				REMOTELOGGER_INST::GetInstance().m_UseEncryption = useEncryption;
			}
		}

		if (!PARAM_netremoteloggingEnableUpload.Get())
		{
			bool wasCloudUploadEnabled = REMOTELOGGER_INST::GetInstance().m_UploadToCloud;
			if (wasCloudUploadEnabled != uploadToCloud)
			{
				gnetDebug1("%s :: Uploading now %s", __FUNCTION__, uploadToCloud ? "enabled" : "disabled");
				REMOTELOGGER_INST::GetInstance().m_UploadToCloud = uploadToCloud;

				// If no local log was specified then this is a real state change and the logger needs
				// to be shut down or initialised.
				if (!PARAM_netremoteloggingLocalLog.Get())
				{
					if (uploadToCloud)
					{
						gnetDebug1("%s :: Initialising...", __FUNCTION__);
						char titleId[16];
						formatf(titleId, "%s", REMOTELOGGER_INST::GetInstance().m_titleName);
						REMOTELOGGER_INST::GetInstance().Init(titleId);
					}
					else
					{
						gnetDebug1("%s :: Shutting down...", __FUNCTION__);
						REMOTELOGGER_INST::GetInstance().Shutdown();
					}
				}
			}
		}
	}
}

void netRemoteLog::BeginSend()
{
	if (REMOTELOGGER_INST::IsInstantiated())
	{
		REMOTELOGGER_INST::GetInstance().BeginSendInternal();
	}
}

} // namespace rage
