//
// netblender.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORKBLENDER_H
#define NETWORKBLENDER_H

//framework includes
#include "fwtl/pool.h"

namespace rage
{
    class netObject;

	// Enumeration of flags that determine how we can reset a blender
	enum blenderResetFlags
	{
		RESET_POSITION_AND_VELOCITY,
		RESET_VELOCITY,
		RESET_POSITION
	};

//PURPOSE
// This structure contains parameters that are used to customise the blenders behaviour.
struct netBlenderData
{
	virtual ~netBlenderData() {}

	virtual const char *GetName() const = 0;

    bool m_BlendingOn; // Enable / disable blending
};

//PURPOSE
// This is the base class for network blenders for interpolating state updates received
// from remote machines in a network game
class netBlender
{
protected:

    //PURPOSE
    // Network blender update levels. This is used to decide based on the
    // frequency of updates received for the network object associated with the blender
    enum UpdateLevel
    {
        LOW_UPDATE_LEVEL,
        MEDIUM_UPDATE_LEVEL,
        HIGH_UPDATE_LEVEL
    };

public:

    //PURPOSE
    // Class constructor
    //PARAMS
    // blenderData - The network blender data to use for this blender
    netBlender(netBlenderData *blenderData) :
    m_Disable(false),
    m_BlenderData(blenderData),
    m_LastSyncMessageTime(0),
	m_LastOwnershipChangeTime(0),
    m_AverageUpdateTime(50), // start the average update rate at 50ms
    m_LastUpdateLevelChangeTime(0),
    m_EstimatedUpdateLevel(HIGH_UPDATE_LEVEL)
    {
    }

    virtual ~netBlender() {}

    //PURPOSE
    // Indicates whether the blender should perform blending
    virtual bool IsBlendingOn() const { return !m_Disable && m_BlenderData->m_BlendingOn; }

    //PURPOSE
    // Disables the blender, so received updates are not blended
    void Disable() { m_Disable = true; }

    //PURPOSE
    // Enables the blender, so received updates are blended
    void Enable() { m_Disable = false; }

    //PURPOSE
    // Returns the blender data for this blender
    virtual netBlenderData *GetBlenderData() { return m_BlenderData; }

    //PURPOSE
    // Returns the blender data for this blender
    virtual const netBlenderData *GetBlenderData()const  { return m_BlenderData; }

    //PURPOSE
    // Sets the blender data for this blender
    void SetBlenderData(netBlenderData *blenderData) { m_BlenderData = blenderData; }

    //PURPOSE
    // Returns the last time an update message was received for the object associated with this blender
    u32 GetLastSyncMessageTime() const { return m_LastSyncMessageTime; }

    //PURPOSE
    // Sets the last time an update message was received for the object associated with this blender
    //PARAMS
    // time - The time of the last update message received
    void SetLastSyncMessageTime(u32 time);

	//PURPOSE
	// Returns the last time an ownership change occurred for the object associated with this blender
	u32 GetLastOwnershipChangeTime() const { return m_LastOwnershipChangeTime; }

	//PURPOSE
	// Sets the last time an ownership change occurred for the object associated with this blender
	//PARAMS
	// time - The time of the ownership change
	void SetLastOwnershipChangeTime(u32 time);

    //PURPOSE
    // Returns the average time between updates for the object associated with this blender
    u32 GetAverageUpdateTime() const { return m_AverageUpdateTime; }

    //PURPOSE
    // Returns the estimated update level for the blender
    UpdateLevel GetEstimatedUpdateLevel() const { return m_EstimatedUpdateLevel; }

#if __BANK
    //PURPOSE
    // Returns the estimated update level for the blender as a string
    const char *GetEstimatedUpdateLevelName() const;
#endif

    //PURPOSE
    // Returns whether the blender should use prediction
    virtual bool CanExtrapolate() const { return true; }

    //PURPOSE
    // Called when the network object associated with this blender changes owner
    virtual void OnOwnerChange(blenderResetFlags resetFlag = blenderResetFlags::RESET_POSITION_AND_VELOCITY) = 0;

    //PURPOSE
    // Performs the blend and returns true if there was any change to the object
    virtual void Update() = 0;

    //PURPOSE
    // Resets the blender to it's initial state
    virtual void Reset() = 0;

    //PURPOSE
    // Moves the blender target straight to it's target state
    virtual void GoStraightToTarget() = 0;

    //PURPOSE
    // Registers a collision with the blender target
    //PARAMS
    // collider   - The network object colliding with the blender target
    // impulseMag - The magnitude of the impact
    virtual void RegisterCollision(netObject *UNUSED_PARAM(collider), const float UNUSED_PARAM(impulseMag)) {}

    //PURPOSE
    // Called immediately prior to the physics update
    virtual void ProcessPrePhysics() {}

    //PURPOSE
    // Called immediately after the physics update
    virtual void ProcessPostPhysics() {}

protected:

    //PURPOSE
    // Sets the average update time for the blender
    //PARAMS
    // time - The average update time
    void SetAverageUpdateTime(u32 time) { m_AverageUpdateTime = time; }

    //PURPOSE
    // Returns the network object associated with the blender
    virtual netObject *GetNetworkObject() = 0;

    //PURPOSE
    // Returns the network object associated with the blender
    virtual const netObject *GetNetworkObject() const = 0;

    //PURPOSE
    // Returns the time between updates received over the network for the blender target
    virtual u32 GetTimeBetweenUpdates() const = 0;

    //PURPOSE
    // Returns whether the blender target is on screen
    virtual bool GetIsOnScreen() const = 0;

private:

    static const unsigned int UPDATE_ESTIMATION_INTERVAL = 5000; // Time between estimating the update level
    static const unsigned int HIGH_UPDATE_THRESHOLD      = 200;  // Average time between snapshots for the update rate to be high
    static const unsigned int MEDIUM_UPDATE_THRESHOLD    = 500;  // Average time between snapshots for the update rate to be medium
    static const unsigned int STOPPED_INTERVAL           = 3000; // Time between snapshots when the object is considered to have come to rest

    bool            m_Disable;                   // Indicates whether blending should be disabled
    netBlenderData *m_BlenderData;               // The blender data - contains parameters to change the blender behaviour
    u32             m_LastSyncMessageTime;       // The time the last sync message was received
	u32             m_LastOwnershipChangeTime;   // The time the last sync message was received
    u32             m_AverageUpdateTime;         // The rolling average time between updates
    u32             m_LastUpdateLevelChangeTime; // The time the update rate last changed
    UpdateLevel     m_EstimatedUpdateLevel;      // The estimated update rate (low, medium and high)
};

} // namespace rage

#endif  // NETWORKBLENDER_H
