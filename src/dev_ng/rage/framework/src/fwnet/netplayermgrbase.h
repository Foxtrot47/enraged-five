//
// netplayermgrbase.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORKPLAYERMGRBASE_H
#define NETWORKPLAYERMGRBASE_H

// rage headers
#include "atl/array.h"
#include "net/transaction.h"

// framework headers
#include "fwnet/netbandwidthstats.h"
#include "fwnet/netinterface.h"
#include "fwnet/netlog.h"
#include "fwnet/netplayer.h"

namespace rage
{
    class netBandwidthMgr;
    class nonPhysicalPlayerDataBase;
    class playerDataMsg;
    class rlGamerInfo;

//PURPOSE
// The network player manager is responsible for managing functionality relating
// to the different players in a network session
class netPlayerMgrBase
{
public:

    typedef atDelegate<bool (unsigned, netPlayer &)> PhysicalOnRemoteCallback;

    netPlayerMgrBase(netBandwidthMgr &bandwidthMgr);

    virtual ~netPlayerMgrBase();

    //PURPOSE
    // Initialises the network player manager
    //PARAMS
    // connectionMgr            - The connection manager used to communicate between network players
    // channelID                - The channel ID for sending player manager network messages
    // physicalOnRemoteCallback - Callback function for determining whether the local player is physical/non-physical
    //                            on other players machines
    virtual bool Init(netConnectionManager    *connectionMgr,
                      const unsigned           channelId,
                      PhysicalOnRemoteCallback physicalOnRemoteCallback);

    //PURPOSE
    // Shuts down the network player manager
    virtual void Shutdown();

    //PURPOSE
    // Returns whether the network player manager is initialised
    bool IsInitialized() const;

    //PURPOSE
    // Returns whether the network player manager is shutting down
    bool IsShuttingDown() const;

    //PURPOSE
    // Performs any per frame processing required by the network player manager; this should
    // be called once per frame by your application
    virtual void Update();

    //PURPOSE
    //  Adds a player to the player manager
    //PARAMS
    // gamerInfo             - The gamer info of the new player
    // endpointId            - The network endpointId of the new player
	// playerData            - The network player data for the new player
    // nonPhysicalPlayerData - The non-physical data for the new player
    virtual netPlayer* AddPlayer(const rlGamerInfo         &gamerInfo,
                                 const EndpointId		   endpointId,
                                 const playerDataMsg      &playerData,
                                 nonPhysicalPlayerDataBase *nonPhysicalPlayerData);

    //PURPOSE
    // Removes a player from the player manager
    //PARAMS
    // gamerInfo - The gamer info of the player to remove
    virtual void RemovePlayer(const rlGamerInfo& gamerInfo);

	//PURPOSE
	// Allocates a temporary player
	//PARAMS
	// gamerInfo             - The gamer info of the new player
	// endpointId            - The network endpoint id of the new player
	// playerData            - The network player data for the new player
	// nonPhysicalPlayerData - The non-physical data for the new player
	virtual netPlayer* AddTemporaryPlayer(const rlGamerInfo         &gamerInfo,
									const EndpointId		 endpointId,
									const playerDataMsg      &playerData,
									nonPhysicalPlayerDataBase *nonPhysicalPlayerData);

	//PURPOSE
	// Deallocates a temporary player
	//PARAMS
	// player - The player to remove
	virtual void RemoveTemporaryPlayer(const rlGamerInfo& gamerInfo);


	// PURPOSE
	// Retrieves a netPlayer from the temporary array
	virtual netPlayer *GetTempPlayer(const rlGamerInfo& info) const = 0;
	virtual netPlayer *GetTempPlayer(const rlGamerId& gamerId) const = 0;

    //PURPOSE
    // Returns a network player controlled by the specified peer. Peers can support more
    // than one local player.
    //PARAMS
    // peerPlayerIndex - The player index of the player on the specified remote peer
    // peerInfo        - Description of the remote peer the player is associated with
    virtual netPlayer* GetPlayerFromPeerPlayerIndex(const unsigned peerPlayerIndex, const rlPeerInfo &peerInfo);

    //PURPOSE
    // Checks the specified network players physical status and updates the player
    // lists if necessary
    //PARAMS
    // player - The player to update the physical status for
    void UpdatePlayerPhysicalStatus(netPlayer *player);

    //PURPOSE
    // Adds a network event handler
    //PARAMS
    // dlgt - The event handler
    void AddDelegate(NetworkPlayerEventDelegate* dlgt);

    //PURPOSE
    // Removes a network event handler
    //PARAMS
    // dlgt - The event handler
    void RemoveDelegate(NetworkPlayerEventDelegate* dlgt);

    //PURPOSE
    // Returns whether a connection is open for the specified player
    //PARAMS
    // player - The player to check
    bool IsConnectionOpen(const netPlayer &player);

	//PURPOSE
	// Closes a connection on a player
	//PARAMS
	// player - The player to close
	void CloseConnection(netPlayer &player);

    //PURPOSE
    //  Sends data to the specified player
    //PARAMS
    // toPlayer   - The player to send the data to
    // buffer     - The data to send
    // numBytes   - The size of the data in bytes
    // sendFlags  - The send flags to use when sending the data
    // sequence   - The sequence number to use when sending the data
    // fromPlayer - The player sending the data (0 indicates local player, used for bots)
    bool SendBuffer(const netPlayer *toPlayer,
                    const void* buffer,
                    const unsigned numBytes,
                    const unsigned sendFlags,
                    netSequence* seq = NULL,
                    const netPlayer *fromPlayer = 0);

    //PURPOSE
    // Adds a transaction request handler
    //PARAMS
    // requestHandler - The request handler to add
    void AddRequestHandler(NetworkPlayerRequestHandler *requestHandler);

    //PURPOSE
    // Removes a transaction request handler
    //PARAMS
    // requestHandler - The request handler to remove
    void RemoveRequestHandler(NetworkPlayerRequestHandler* requestHandler);

    //PURPOSE
    // Sends a transaction request to the given player
    //PARAMS
    // player   - The player to send the transaction request to
    // buffer   - The transaction data to send
    // numBytes - The size of the transaction data in bytes
    // timeout  - The timeout period before the transaction expires
    // handler  - The response handler for the transaction
    bool SendRequestBuffer(const netPlayer* player,
                            const void* buffer,
                            const unsigned numBytes,
                    		const unsigned timeout,
		                    netResponseHandler* handler);

    //PURPOSE
    // Sends a transaction response
    //PARAMS
    // txInfo   - The transaction info structure for the transaction
    // buffer   - The response data
    // numBytes - The size of the response data in bytes
    bool SendResponseBuffer(const netTransactionInfo& txInfo,
                            const void* buffer,
                            const unsigned numBytes);

    //PURPOSE
    // Returns the player object associated with the specified connection ID
    //PARAMS
    // connectionID - The connection ID
    netPlayer*			GetPlayerFromConnectionId(const int connectionID);
    const netPlayer*	GetPlayerFromConnectionId(const int connectionID) const;

	//PURPOSE
	// Returns the player object associated with the specified endpoint ID
	//PARAMS
	// endpointId - The endpoint ID
	netPlayer*			GetPlayerFromEndpointId(const EndpointId endpointId);
	const netPlayer*	GetPlayerFromEndpointId(const EndpointId endpointId) const;

    //PURPOSE
    // Returns the player object associated with the specified gamer ID
    //PARAMS
    // gamerID - The gamer ID
    netPlayer*			GetPlayerFromGamerId(const rlGamerId& gamerID, bool bAllowTempPlayer = true);
    const netPlayer*	GetPlayerFromGamerId(const rlGamerId& gamerID, bool bAllowTempPlayer = true) const;

    //PURPOSE
    // Returns the player object associated with the specified peer ID
    //PARAMS
    // peerID - The peer ID
    netPlayer*			GetPlayerFromPeerId(const u64 peerId);
    const netPlayer*	GetPlayerFromPeerId(const u64 peerId) const;

    //PURPOSE
    // Returns the player object associated with the specified player index only if they are active
    //PARAMS
    // playerIndex - The player index
    virtual netPlayer*         GetActivePlayerFromIndex(const ActivePlayerIndex playerIndex) = 0;
    virtual const netPlayer*   GetActivePlayerFromIndex(const ActivePlayerIndex playerIndex) const = 0;

	//PURPOSE
	// Returns the physical player object associated with the specified physical player index if they are physical
	//PARAMS
	// playerIndex - The player index
	netPlayer*         GetPhysicalPlayerFromIndex(const PhysicalPlayerIndex playerIndex);
	const netPlayer*   GetPhysicalPlayerFromIndex(const PhysicalPlayerIndex playerIndex) const;

	//PURPOSE
    // Returns the player object for the local player
    netPlayer*			GetMyPlayer();
    const netPlayer*	GetMyPlayer() const;

    //PURPOSE
    // Returns the RLGamerID for the local player
    const rlGamerId&		GetMyRlGamerId() const;

    //PURPOSE
    // Returns the active player index for the local player
    ActivePlayerIndex		GetMyActivePlayerIndex() const;

	//PURPOSE
	// Returns the physical player index for the local player
	PhysicalPlayerIndex		GetMyPhysicalPlayerIndex() const;

	//PURPOSE
	// Returns number of pending players
    unsigned GetNumPendingPlayers() const { return m_NumPendingPlayers; }

	//PURPOSE
    // Returns number of active players
    unsigned GetNumActivePlayers() const { return m_NumAllActivePlayers; }

    //PURPOSE
    // Returns number of remotely controlled active players
    unsigned GetNumRemoteActivePlayers() const { return m_NumRemoteActivePlayers; }

    //PURPOSE
    // Returns number of physical players
    unsigned GetNumPhysicalPlayers() const { return m_NumAllPhysicalPlayers; }

    //PURPOSE
    // Returns the number of locally controlled physical players
    unsigned GetNumLocalPhysicalPlayers()  const { return m_NumLocalPhysicalPlayers; }

    //PURPOSE
    // Returns the number of remotely controlled physical players
    unsigned GetNumRemotePhysicalPlayers() const { return m_NumRemotePhysicalPlayers; }

    //PURPOSE
    // Returns the list of pending players (players we have not established a connection to yet)
    netPlayer * const *GetPendingPlayers() const { return m_AllPendingPlayers; }

    //PURPOSE
    // Returns the list of pending players (players we have not established a connection to yet)
    netPlayer * const *GetAllActivePlayers() const { return m_AllActivePlayers; }

    //PURPOSE
    // Returns the list of pending players (players we have not established a connection to yet)
    netPlayer * const *GetRemoteActivePlayers() const { return m_RemoteActivePlayers; }

    //PURPOSE
    // Returns the list of locally controlled physical players
    netPlayer * const *GetAllPhysicalPlayers() const { return m_AllPhysicalPlayers; }

    //PURPOSE
    // Returns the list of locally controlled physical players
    netPlayer * const *GetLocalPhysicalPlayers() const { return m_LocalPhysicalPlayers; }

    //PURPOSE
    // Returns the list of remotelt controlled physical players
    netPlayer * const *GetRemotePhysicalPlayers() const { return m_RemotePhysicalPlayers; }

    //PURPOSE
    // Returns a bitmask indicating the remote physical players in the current session
    u32 GetRemotePhysicalPlayersBitmask() const { return m_RemotePhysicalPlayersInSession; }

#if ENABLE_NETWORK_BOTS
    //PURPOSE
    // Returns the number of total active bots in the network session
    unsigned GetNumTotalActiveBots() const;

    //PURPOSE
    // Returns the number of active locally controlled bots in the network session
    unsigned GetNumLocalActiveBots() const;

    //PURPOSE
    // Returns the number of active remotely controlled bots in the network session
    unsigned GetNumRemoteActiveBots() const;
#endif // ENABLE_NETWORK_BOTS

    //PURPOSE
    // Returns the logging object for this class
    netLoggingInterface &GetLog() { return *m_Log; }

	//PURPOSE
	// Assigns a physical player index to a player and updates the physical players array
	void SetPhysicalPlayerIndex(netPlayer& player, PhysicalPlayerIndex index);

#if __BANK

    //PURPOSE
    // Increases the count of reliable messages not tracked directly
    void IncreaseMiscReliableMessagesSent() { m_NumMiscReliableMessagesSent++; }

    //PURPOSE
    // Increases the count of reliable messages not tracked directly
    void IncreaseMiscUnreliableMessagesSent() { m_NumMiscUnreliableMessagesSent++; }

    //PURPOSE
    // Returns the number of non-physical update message sent since the last call to ResetMessageCounts()
    unsigned GetNumNonPhysicalUpdateMessagesSent() const { return m_NumNonPhysicalUpdateMessagesSent; }

    //PURPOSE
    // Returns the number of reliable messages sent not tracked directly since the last call to ResetMessageCounts()
    unsigned GetNumMiscReliableMessagesSent() const { return m_NumMiscReliableMessagesSent; }

    //PURPOSE
    // Returns the number of unreliable messages sent not tracked directly since the last call to ResetMessageCounts()
    unsigned GetNumMiscUnreliableMessagesSent() const { return m_NumMiscUnreliableMessagesSent; }

    //PURPOSE
    // Reset the counts of player manager messages sent
    void ResetMessageCounts() { m_NumNonPhysicalUpdateMessagesSent = m_NumMiscReliableMessagesSent = m_NumMiscUnreliableMessagesSent = 0; }

	netBandwidthStatistics& GetBandwidthStatistics() { return m_BandwidthStats; }
#endif

protected:

    //PURPOSE
    // Adds a remote player to the player manager
    //PARAMS
    // gamerInfo             - The gamer info for the new player
    // playerData            - The player data for the new player
    // endpointId            - The network endpointId for the new player
	// nonPhysicalPlayerData - The non-physical player data for the new player
    virtual netPlayer* AddRemotePlayer(const rlGamerInfo& gamerInfo,
                               const playerDataMsg& playerData,
                               const EndpointId endpointId,
                               nonPhysicalPlayerDataBase *nonPhysicalPlayerData);

    //PURPOSE
    // Adds the local player to the player manager
    //PARAMS
    // gamerInfo             - The gamer info for the new player
    // playerData            - The player data for the new player
    // nonPhysicalPlayerData - The non-physical player data for the new player
    virtual void AddLocalPlayer(const rlGamerInfo& gamerInfo,
                                const playerDataMsg& playerData,
                                nonPhysicalPlayerDataBase *nonPhysicalPlayerData);

	//PURPOSE
	// Called when a player moves onto the active list
	//PARAMS
	// player - The player becoming active
	virtual void ActivatePlayer(netPlayer*) {}

    //PURPOSE
    // Removes the specified player from the player manager
    //PARAMS
    // player - The player to remove
    virtual void RemovePlayer(netPlayer* player);

    typedef inlist<netPlayer, &netPlayer::m_ListLink> PlayerList;

	//PURPOSE
	// Updates the player lists (existing, active, physical, etc.) for a particular player
	void UpdatePlayerListsForPlayer(netPlayer* player);

private:

    //PURPOSE
    // Allocates a new network player for use by the player manager
    virtual netPlayer *AllocatePlayer() = 0;

    //PURPOSE
    // Deallocates a network player for reuse by the player manager
    virtual void DeallocatePlayer(netPlayer *player) = 0;

	//PURPOSE
	// Allocates a new network player for use by the player manager
	virtual netPlayer *AllocateTempPlayer() = 0;

	//PURPOSE
	// Deallocates a network player for reuse by the player manager
	virtual void DeallocateTempPlayer(netPlayer *player) = 0;

    //PURPOSE
    // Private shutdown function for this class
    void ShutdownPrivate();

    //PURPOSE
    // Sends updates to all players we have not cloned our player on
    void SendNonPhysicalUpdates();

    //PURPOSE
    // Event handler for connection manager events
    //PARAMS
    // cxnMgr - The connection manager
    // evt    - The connection manager event
    void OnNetEvent(netConnectionManager* cxnMgr, const netEvent* evt);

    //PURPOSE
    // Event handler for transaction requests
    //PARAMS
    // transactorcxnMgr - The transaction
    // rqstHandler      - The request handler
    // rqst             - The request
    void OnTxRqst(netTransactor* transactorcxnMgr,
                    netRequestHandler* rqstHandler,
                    const netRequest* rqst);

    //PURPOSE
    // Handles non physical player update messages
    //PARAMS
    // messageData - Data describing the incoming message
    void HandleNonPhysicalUpdateMsg(const ReceivedMessageData &messageData);

	//PURPOSE
	// Update the various player lists (existing, active, physical, etc.)
	void UpdatePlayerLists();

	//PURPOSE
	// Update the player arrays (existing, active, physical, etc.)
	void UpdatePlayerArrays();

    // The connection manager, used to send messages
    netConnectionManager* m_CxnMgr;

    // The bandwidth manager, used to record data sent/received by the manager
    netBandwidthMgr &m_BandwidthMgr;

    // Network time object used by the network transactor
    netTimeStep m_NetTimeStep;

    // Network transactor object
    netTransactor m_Transactor;

    // Event handler wrapper object for connection manager events
    netConnectionManager::Delegate m_CxnMgrDlgt;

    // Event handler for transaction requests
    netRequestHandler m_TxRqstHandler;

    // ID of channel over which player managers will communicate.
    unsigned m_ChannelId;

    // The player representing the local machine
    netPlayer *m_MyPlayer;

    // Logging object for the event manager class
    netLog *m_Log;
    
	typedef inlist<netPlayer, &netPlayer::m_PendingListLink>	PendingPlayerList;
	typedef inlist<netPlayer, &netPlayer::m_ActiveListLink>		ActivePlayerList;
	typedef inlist<netPlayer, &netPlayer::m_NonPhysListLink>	NonPhysicalPlayerList;
	typedef inlist<netPlayer, &netPlayer::m_PhysListLink>		PhysicalPlayerList;

    // This callback is used to determine whether our local player is
    // physical on a remote machine - this is necessary to determine whether
    // we need to send non-physical updates to another player
    PhysicalOnRemoteCallback m_PhysicalOnRemoteMachine;

protected:

	// Players who exist (union of pending,active,nonphys,phys).
    PlayerList m_ExistingPlayers;

	// Players with connections not yet fully open.
	PendingPlayerList m_PendingPlayers;

    // Players with fully open connections.
    ActivePlayerList m_ActivePlayers;

private:

    // Active players with no PlayerPed.
    NonPhysicalPlayerList m_NonPhysicalPlayers;

	// Active players with a PlayerPed.
	PhysicalPlayerList m_PhysicalPlayers;

	// An array of physical players, where each player has been allocated a slot by a session host or roaming bubble owner, etc.
	// This array will be the same for all physical players involved held in the array, so each player will have a physical player index that
	// is consistent across the network, and can be accessed quickly via this index. This is separate from the physical players list because
	// we can sometimes have players that are physical but have not yet been allocated a slot in the m_PhysicalPlayersArray yet.
 	atFixedArray<netPlayer*, MAX_NUM_PHYSICAL_PLAYERS> m_PhysicalPlayersArray;

    // Cached local/remote players list - used for fast iteration over players in the session
    unsigned   m_NumPendingPlayers;
    unsigned   m_NumAllActivePlayers;
    unsigned   m_NumRemoteActivePlayers;
    unsigned   m_NumAllPhysicalPlayers;
    unsigned   m_NumLocalPhysicalPlayers;
    unsigned   m_NumRemotePhysicalPlayers;
    netPlayer *m_AllPendingPlayers[MAX_NUM_ACTIVE_PLAYERS];
    netPlayer *m_AllActivePlayers[MAX_NUM_PHYSICAL_PLAYERS];
    netPlayer *m_RemoteActivePlayers[MAX_NUM_PHYSICAL_PLAYERS - 1];
    netPlayer *m_AllPhysicalPlayers[MAX_NUM_PHYSICAL_PLAYERS];
    netPlayer *m_LocalPhysicalPlayers[MAX_NUM_PHYSICAL_PLAYERS];
    netPlayer *m_RemotePhysicalPlayers[MAX_NUM_PHYSICAL_PLAYERS - 1];

    // Bitmask of remote physical players in the session
    u32 m_RemotePhysicalPlayersInSession;

	// Event handler for network events
    NetworkPlayerEventDelegator m_NetEventDelegator;

    // Event handler for transaction requests
    NetworkPlayerRequestDelegator m_TxRqstDelegator;

    // Indicates whether the player manager is initialised
    bool m_IsInitialized    : 1;

    // Indicates whether the player manager is shutting down
    bool m_IsShuttingDown	: 1;

#if __BANK
    unsigned m_NumNonPhysicalUpdateMessagesSent; // the number of non-physical update message sent since the last call to ResetMessageCounts()
    unsigned m_NumMiscReliableMessagesSent;      // the number of reliable messages sent not tracked directly since the last call to ResetMessageCounts()
    unsigned m_NumMiscUnreliableMessagesSent;    // the number of unreliable messages sent not tracked directly since the last call to ResetMessageCounts()

    // Bandwidth statistics for data sent/received
    netBandwidthStatistics m_BandwidthStats;

    // Bandwidth recorder used for recording player data sent/received
	RecorderId m_BandwidthRecorderId;
#endif
};

inline netPlayer* netPlayerMgrBase::GetMyPlayer()
{
	Assert(m_IsInitialized);
	Assert(m_MyPlayer);
	return m_MyPlayer;
}

inline const netPlayer* netPlayerMgrBase::GetMyPlayer() const
{
	return const_cast<netPlayerMgrBase*>(this)->GetMyPlayer();
}

inline netPlayer* netPlayerMgrBase::GetPhysicalPlayerFromIndex(const PhysicalPlayerIndex playerIndex)
{
	if (AssertVerify(playerIndex < MAX_NUM_PHYSICAL_PLAYERS))
	{
		return m_PhysicalPlayersArray[playerIndex];
	}

	return 0;
}

inline const netPlayer* netPlayerMgrBase::GetPhysicalPlayerFromIndex(const PhysicalPlayerIndex playerIndex) const
{
	return const_cast<netPlayerMgrBase*>(this)->GetPhysicalPlayerFromIndex(playerIndex);
}

inline PhysicalPlayerIndex netPlayerMgrBase::GetMyPhysicalPlayerIndex() const
{
	return GetMyPlayer()->GetPhysicalPlayerIndex();
}

} // namespace rage

#endif  // NETWORKPLAYERMGRBASE_H
