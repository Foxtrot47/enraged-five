//
// neteventtypes.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#ifndef NET_EVENTTYPES_H
#define NET_EVENTTYPES_H

#include "fwnet/netarraymgr.h"
#include "fwnet/netgameevent.h"
#include "fwnet/neteventid.h"

namespace rage
{
class netArrayIdentifierBase;

enum NetGameEventTypes
{
    OBJECT_ID_FREED_EVENT,
    OBJECT_ID_REQUEST_EVENT,
	ARRAY_DATA_VERIFY_EVENT,
    USER_EVENT_START
};

// ================================================================================================================
// objectIdFreedEvent - informs the machine that generated an object id that this id is free now
// ================================================================================================================

class objectIdFreedEvent : public netGameEvent
{
public:

    static const unsigned SIZEOF_NUM_OBJECT_IDS  = 6;
    static const unsigned MAX_OBJECT_IDS_TO_FREE = ((1<<SIZEOF_NUM_OBJECT_IDS)-1);

    objectIdFreedEvent();
    objectIdFreedEvent(ObjectId   *objectIDsToFree,
                       unsigned    numObjectIDs,
                       PhysicalPlayerIndex playerIndex);

    const char* GetEventName() const { return "OBJECT_ID_FREED_EVENT"; }

    static void EventHandler(datBitBuffer& msgBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence);
    static void Trigger(ObjectId *objectIDsToFree, unsigned numObjectIDs, PhysicalPlayerIndex playerIndex);

    static void GetPendingFreedObjectIDs(ObjectId *pendingFreedObjectIds, unsigned &numPendingFreedObjectIDs);

    virtual bool IsInScope          (const netPlayer&) const;
    virtual void Prepare            ( datBitBuffer &messageBuffer, const netPlayer &toPlayer );
    virtual void Handle             ( datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);

#if ENABLE_NETWORK_LOGGING
    virtual void WriteEventToLogFile(bool wasSent, bool eventLogOnly, datBitBuffer *messageBuffer) const;
#endif // ENABLE_NETWORK_LOGGING

private:

    template <class Serialiser> void SerialiseEvent(datBitBuffer &bitBuffer);

    unsigned			m_NumObjectIDs;                            // The number of object IDs in this event
    ObjectId			m_ObjectIDsToFree[MAX_OBJECT_IDS_TO_FREE]; // The object ids to free
    PhysicalPlayerIndex m_PlayerIndex;                             // The player to send this event to
#if ENABLE_NETWORK_LOGGING
    bool                m_IgnoredIDs;                              // This event will be ignored if received during the join process
#endif // ENABLE_NETWORK_LOGGING
};

// ================================================================================================================
// objectIdRequestEvent - sent to request some object IDs from a remote player when the local machine is running low
// ================================================================================================================

class objectIdRequestEvent : public netGameEvent
{
public:

    static const unsigned SIZEOF_NUM_OBJECT_IDS  = 6;
    static const unsigned MAX_OBJECT_IDS_TO_FREE = ((1<<SIZEOF_NUM_OBJECT_IDS)-1);

    objectIdRequestEvent();
    objectIdRequestEvent(unsigned numObjectIDs);

    const char* GetEventName() const { return "OBJECT_ID_REQUEST_EVENT"; }

    static void EventHandler(datBitBuffer& msgBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence);
    static void Trigger(unsigned numObjectIDs);

    virtual bool IsInScope          (const netPlayer&) const;
    virtual void Prepare            (datBitBuffer &messageBuffer, const netPlayer &toPlayer);
    virtual void Handle             (datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);
    virtual bool Decide             (const netPlayer &fromPlayer, const netPlayer &toPlayer);
    virtual void PrepareReply       (datBitBuffer &messageBuffer, const netPlayer &toPlayer);
    virtual void HandleReply        (datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);

#if ENABLE_NETWORK_LOGGING
    virtual void WriteEventToLogFile(bool wasSent, bool eventLogOnly, datBitBuffer *messageBuffer) const;
    virtual void WriteReplyToLogFile(bool wasSent) const;
#endif // ENABLE_NETWORK_LOGGING

private:

    template <class Serialiser> void SerialiseEvent(datBitBuffer &bitBuffer);
    template <class Serialiser> void SerialiseEventReply(datBitBuffer &messageBuffer);

    unsigned			m_NumObjectIDRequested;                      // The number of object IDs requested
    unsigned			m_NumObjectIDReturned;                       // The number of object IDs returned
    ObjectId			m_ObjectIDsReturned[MAX_OBJECT_IDS_TO_FREE]; // The object ids returned
};

// ================================================================================================================
// arrayDataVerifyEvent - triggered when the arbitrator of an array changes - all relevant machines send out checksum 
//							of all the array data to the new arbitrator to make sure that they are up to date with it.
// ================================================================================================================

class arrayDataVerifyEvent : public netGameEvent
{
public:

	arrayDataVerifyEvent();
	arrayDataVerifyEvent(NetworkArrayHandlerType arrayType, u32 arrayChecksum);

	const char* GetEventName() const { return "ARRAY_DATA_VERIFY_EVENT"; }

	static void EventHandler(datBitBuffer &msgBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence);
	static void Trigger(NetworkArrayHandlerType arrayType, u32 arrayChecksum);

	virtual bool IsInScope( const netPlayer &player ) const;
	virtual void Prepare ( datBitBuffer &messageBuffer, const netPlayer &toPlayer );
	virtual void Handle ( datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer);
	virtual bool Decide (const netPlayer &fromPlayer, const netPlayer &toPlayer);
	virtual bool CanChangeScope() const { return true; }

#if ENABLE_NETWORK_LOGGING
	virtual void WriteEventToLogFile(bool wasSent, bool eventLogOnly, datBitBuffer *messageBuffer) const;
#endif // ENABLE_NETWORK_LOGGING

protected:

	virtual netArrayHandlerBase* GetArrayHandler();
	virtual const netArrayHandlerBase* GetArrayHandler() const;

	static const unsigned SIZEOF_ARRAY_TYPE = datBitsNeeded<netArrayManager::MAX_NUM_ARRAY_HANDLER_TYPES>::COUNT;

	template <class Serialiser> void SerialiseEvent(datBitBuffer &messageBuffer);

	NetworkArrayHandlerType	m_ArrayType;				// the type of array
	u32						m_ArrayChecksum;			// The checksum of the data in the array
	bool					m_ArrayChecksumDiffers;
	bool					m_ArrayDataVerified;		// set when the host of the script successfully verifies the data
	bool					m_Ignored;					// set when the event received is ignored
};	

namespace netEventTypes
{
    void RegisterNetGameEvents();
}

} // namespace rage

#endif  // NET_EVENTTYPES_H
