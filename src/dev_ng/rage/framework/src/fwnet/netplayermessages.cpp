//
// netplayermessages.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwnet/netplayermessages.h"

// game includes
#include "fwnet/netinterface.h"
#include "fwnet/optimisations.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netlogsplitter.h"
#include "fwnet/netlogutils.h"

// rage includes
#include "data/autoid.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

NET_MESSAGE_IMPL(playerDataMsg);
NET_MESSAGE_IMPL(nonPhysicalDataMsg);
NET_MESSAGE_IMPL(activateNetworkBotMsg);

///////////////////////////////////////////////////////////////////////////////////////
// playerDataMsg
///////////////////////////////////////////////////////////////////////////////////////

playerDataMsg::playerDataMsg()
: m_GameVersion(0)
, m_NatType(NET_NAT_UNKNOWN)
{
}

playerDataMsg::playerDataMsg(const unsigned gameVersion,
                             const netNatType natType)
{
    this->Reset(gameVersion, natType);
}

void playerDataMsg::Reset(const unsigned gameVersion,
                          const netNatType natType)
{
    m_GameVersion = gameVersion;
    m_NatType     = natType;
}

int playerDataMsg::GetMessageDataBitSize() const
{
    return SIZEOF_GAMEVERSION + SIZEOF_NAT_TYPE;
}


void playerDataMsg::WriteToLogFile( bool bReceived, netSequence seqNum, const netPlayer &player ) const
{
    netLogSplitter log(netInterface::GetMessageLog(), netInterface::GetPlayerMgr().GetLog());

    if (bReceived)
    {
        NetworkLogUtils::WriteMessageHeader(log, bReceived, seqNum, player, "RECEIVED_PLAYER_DATA", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(log, bReceived, seqNum, player, "SENDING_PLAYER_DATA", "");
    }

    log.WriteDataValue("Game version", "%d", m_GameVersion);
    log.WriteDataValue("NAT Type", "%d", m_NatType);
}

///////////////////////////////////////////////////////////////////////////////////////
// nonPhysicalDataMsg
///////////////////////////////////////////////////////////////////////////////////////
void nonPhysicalDataMsg::WriteToLogFile( bool bReceived, netSequence seqNum, const netPlayer &player ) const
{
    netLogSplitter log(netInterface::GetMessageLog(), netInterface::GetPlayerMgr().GetLog());

    if (bReceived)
    {
        NetworkLogUtils::WriteMessageHeader(log, bReceived, seqNum, player, "RECEIVED_NON_PHYSICAL_UPDATE", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(log, bReceived, seqNum, player, "SENDING_NON_PHYSICAL_UPDATE", "");
    }

    if(m_NonPhysicalData)
    {
        m_NonPhysicalData->WriteToLogFile(log);
    }
}


} // namespace rage
