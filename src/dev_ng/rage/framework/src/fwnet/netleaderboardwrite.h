// 
// netleaderboardwrite.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

#ifndef NET_LEADERBOARD_UPDATE_H
#define NET_LEADERBOARD_UPDATE_H

//Rage headers
#include "fwnet/netchannel.h"
#include "fwtl/regdrefs.h"
#include "rline/clan/rlclancommon.h"
#include "rline/rlstats.h"
#include "system/memory.h"

namespace rage
{
class  rlLeaderboard2Update;
class  rlLeaderboardInputValue;
class  rlLeaderboard2GroupSelector;

//PURPOSE
//  Class responsible for caching gamer statistics to write an update to one leaderboard.
class netLeaderboardWrite : public rlLeaderboard2Update, public fwRefAwareBase
{
private:
	bool  m_PendingFlush;
    rlLeaderboardInputValue* m_InputValueHeap;
	sysMemAllocator* m_Allocator;
	
public:
	netLeaderboardWrite() : m_PendingFlush(true), m_InputValueHeap(0), m_Allocator(0) {;}
	~netLeaderboardWrite();

	bool  Init(sysMemAllocator* pAllocator, const unsigned ids, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId& clanId);
	bool  AddColumn(const rlStatValue& inputValue, const unsigned inputId);
	bool  ColumnExists(const unsigned inputId) const;

	void  ClearPendingFlush() { m_PendingFlush = false; }
	bool  PendingFlush()  const { return m_PendingFlush; }

private:
	//Manage memory
	template<class _Type> _Type* Allocate(int size) const
	{
		gnetAssert(m_Allocator);
		if (m_Allocator && CanAllocate < _Type >(size))
		{
			gnetDebug1("[net_leaderboard_write] - Allocate size=%" SIZETFMT "d", size*sizeof(_Type));
			_Type* types = (_Type*)m_Allocator->RAGE_LOG_ALLOCATE(size*sizeof(_Type), 0);
			gnetAssert(types);

			if (types)
			{
				for(int i=0; i < size; i++)
				{
					rage_placement_new(types+i) _Type;
				}
				return types;
			}
		}

		return NULL;
	}

	//Manage memory
	template<class _Type> bool CanAllocate(int size) const
	{
		gnetAssert(m_Allocator);

		bool canAllocate = false;

		if (m_Allocator)
		{
			gnetAssertf(m_Allocator->GetLargestAvailableBlock() > size*sizeof(_Type), "[net_leaderboard_read] - Not enough memory available(%" SIZETFMT "d) for %" SIZETFMT "d", m_Allocator->GetMemoryAvailable(), size*sizeof(_Type));
			canAllocate = (m_Allocator->GetLargestAvailableBlock() > size*sizeof(_Type));
		}

		return canAllocate;
	}

	template<class _Type> void Free(_Type* mem) const
	{
		Assert(m_Allocator);
		if(m_Allocator && mem)
		{
			m_Allocator->Free(mem);
		}
	}
};

} // namespace rage

#endif  // NET_LEADERBOARD_UPDATE_H