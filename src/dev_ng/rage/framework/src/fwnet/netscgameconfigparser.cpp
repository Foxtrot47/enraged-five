#include "netscgameconfigparser.h"

#include "parser/manager.h"
#include "rline/rlstats.h"

#include "fwnet/optimisations.h"
#include "fwnet/netchannel.h"

#include "bank/bkmgr.h"
#include "bank/bank.h"

using namespace rage;

NETWORK_OPTIMISATIONS()

RAGE_DEFINE_SUBCHANNEL(net, gameconfigparser)
#undef __net_channel
#define __net_channel net_gameconfigparser

#ifdef findInt
#undef findInt
#undef findIntVerifyf
#undef findString
#undef findStringVerifyf
#endif

#define findInt(elt, attr, value)  (elt.FindAttribute(attr) ? elt.FindAttribute(attr)->FindIntValue() : value)
#define findIntVerifyf(elt, attr, value)  (gnetVerify(elt.FindAttribute(attr)) ? elt.FindAttribute(attr)->FindIntValue() : value)
#define findString(elt, attr)  (elt.FindAttribute(attr) ? elt.FindAttribute(attr)->GetStringValue() : "")
#define findStringVerifyf(elt, attr)  (gnetVerify(elt.FindAttribute(attr)) ? elt.FindAttribute(attr)->GetStringValue() : "")

//////////////////////////////////////////////////////////////////////////
//
//	Member function for structures under the ScsGameConfiguration tree
//
//
LeaderboardInput& LeaderboardsConfiguration::AddInput()
{
	return m_inputs.Grow(32);
}

void LeaderboardsConfiguration::AddCategory( const char* name )
{
	m_categories.Grow().SetName(name);
}

Leaderboard* LeaderboardsConfiguration::AddLeaderboard()
{
	return &m_leaderboards.Grow();
}

LeaderboardColumn* Leaderboard::AddColumn( u8 id, const char* name, u8 ordinal, bool isRanking )
{
	LeaderboardColumn& col = m_columns.Grow(4);
	col.m_id = id;
	col.m_nameHashStr = name;
	col.m_ordinal = ordinal;
	col.m_isRakingColumn = isRanking;

	return &col;
}

void LeaderboardColAggregation::SetAggTypeFromString( const char* typeStr )
{
	if (!(typeStr && strlen(typeStr) > 0))
	{
		m_type = AggType_INVALID;
		return;
	}

	char firstLetter = typeStr[0];
	switch(firstLetter)
	{
	case 'M':
		m_type = typeStr[1] == 'a' ? AggType_Max : AggType_Min;
		break;
	case 'S':
		m_type = AggType_Sum;
		break;
	case 'L':
		m_type = AggType_Last;
		break;
	case 'R':
		m_type = AggType_Ratio;
		break;
	default:
		gnetError("Undefined Agg type %s ", typeStr);
		m_type = AggType_INVALID;
		break;
	}
}

eLeaderboardInputDataTypesEnum LeaderboardInput::GetInputDataTypeFromString( const char* typeStr )
{
	if (!gnetVerify(typeStr != NULL && strlen(typeStr) > 0 ))
	{
		return InputDataType_INVALID;
	}

	char firstLetter = typeStr[0];
	switch (firstLetter)
	{
	case 'i': //INT
		return InputDataType_int;
	case 'l': //long
		return InputDataType_long;
	case 'd':
		return InputDataType_double;
	default:
		gnetError("Invalid InputType %s", typeStr);
		return InputDataType_INVALID;
	}
}
//////////////////////////////////////////////////////////////////////////


///////////////////////////netSCGamerConfigParser///////////////////////////////////////////////
//
//	XML parsing functions.
//
bool netSCGamerConfigParser::LoadDataXMLFile( const char* fileName )
{
	parStreamIn* pStreamIn = PARSER.OpenInputStream(fileName, "xml");
	if (!gnetVerifyf(pStreamIn, "Failed to open %s", fileName))
		return false;

	m_pCurrentLeaderboard = NULL;
	m_bInCategoryPermBlock = false;

	pStreamIn->SetBeginElementCallback(parStreamIn::BeginElementCB(this, &netSCGamerConfigParser::CallBackOnBeginElement));
	pStreamIn->SetEndElementCallback(parStreamIn::EndElementCB(this, &netSCGamerConfigParser::CallBackOnEndElement));
	pStreamIn->SetDataCallback(parStreamIn::DataCB(this, &netSCGamerConfigParser::CallBackOnData));
	pStreamIn->ReadWithCallbacks();

	pStreamIn->Close();

	return true;
}

inline bool IsEltNamed(const char* eltName, const char* name)
{
	return (stricmp(eltName, name) == 0);
}

void netSCGamerConfigParser::CallBackOnBeginElement( parElement& elt, bool )
{
	const char* eltName = elt.GetName();

	//Inputs
	if(IsEltNamed(eltName, "Input"))
	{
		ParseInputDef(elt);
	}
	else if (IsEltNamed(eltName, "Category"))
	{
		ParseCategoryDef(elt);
	}
	else if (IsEltNamed(eltName, "Leaderboard"))
	{
		ParseLeaderboardDef(elt);
	}
	else if(IsEltNamed(eltName, "CategoryPermutation"))
	{
		ParseCategoryPermDef(elt);
	}
	else if (IsEltNamed(eltName, "Column"))
	{
		ParseColumnDef(elt);
	}
	else if (IsEltNamed(eltName, "Aggregation"))
	{
		ParseAggregation(elt);
	}
	else if (IsEltNamed(eltName, "GameInput"))
	{
		ParseAggGameInput(elt);
	}
	else if (IsEltNamed(eltName, "ColumnInput"))
	{
		ParseAggColumnInput(elt);
	}
}

void netSCGamerConfigParser::CallBackOnEndElement( bool  )
{
	if (m_bInCategoryPermBlock)
	{
		m_bInCategoryPermBlock = false;

#if !__NO_OUTPUT
		int count = m_pCurrentLeaderboard ? m_pCurrentLeaderboard->m_categoryGroups.GetCount() : 0;
		if (count > 0)
		{
			gnetDebug3("    %s", m_pCurrentLeaderboard->m_categoryGroups[count-1].m_value.c_str());
		}
#endif
		
	}
}

void netSCGamerConfigParser::CallBackOnData( char* data, int /*size*/, bool /*dataIncomplete*/ )
{
	if (m_pCurrentLeaderboard && m_bInCategoryPermBlock)
	{
		//We want to add the incoming data to the last one in the list
		int count = m_pCurrentLeaderboard->m_categoryGroups.GetCount();
		if(gnetVerify(count > 0))
			m_pCurrentLeaderboard->m_categoryGroups[count-1].m_value += data;
	}
}

void netSCGamerConfigParser::ParseInputDef( parElement& elt )
{
	int id = findIntVerifyf(elt, "id", 0xFFFF);
	if (id >= 0)
	{
		const char* name = findString(elt,"name");
		const char* dataTypeStr = findString(elt, "dataType");
		eLeaderboardInputDataTypesEnum  dataType = LeaderboardInput::GetInputDataTypeFromString(dataTypeStr);

		gnetAssert(name && dataType != InputDataType_INVALID);
		m_config.m_leaderboardConfiguration.AddInput().Set((u16)id, name, dataType);
	}
}

void netSCGamerConfigParser::ParseCategoryDef( parElement& elt )
{
	const char* name = findStringVerifyf(elt, "name");
	m_config.m_leaderboardConfiguration.AddCategory(name);
}

void netSCGamerConfigParser::ParseLeaderboardDef( parElement& elt )
{
	//We're into a new leaderboard element. Make a new LB and cache it away.
	m_pCurrentLeaderboard = m_config.m_leaderboardConfiguration.AddLeaderboard();

	if (!gnetVerify(m_pCurrentLeaderboard))
	{
		return;
	}

	int id = findInt(elt, "id", 0xFFFF);
	const char* name = findString(elt, "name");

	m_pCurrentLeaderboard->m_id = (u16)id;
	m_pCurrentLeaderboard->m_nameHashStr = name;

	gnetDebug3("Leaderboard %d - %s", id, name);
}

void netSCGamerConfigParser::ParseCategoryPermDef( parElement& )
{
	if (!gnetVerify(m_pCurrentLeaderboard))
	{
		return;
	}

	//This one is a bit weird.  The comma seperated list of categories are going to come in as data (in the DataCallback)
	//Mark that we're in a Category Perm block
	m_bInCategoryPermBlock = true;

	//Add a new one to the list
	m_pCurrentLeaderboard->m_categoryGroups.Grow(4);

	//Now we wait in the CallBackOnData() callback for the data to be read in.
}

void netSCGamerConfigParser::ParseColumnDef( parElement& elt )
{
	if (!gnetVerify(m_pCurrentLeaderboard))
	{
		return;
	}

	u8 id = (u8)findInt(elt, "id", 0xFF);
	const char* name = findString(elt, "name");
	u8 ordinal = (u8) findInt(elt, "ordinal", 0xFF);
	bool isRanking = elt.FindAttribute("isRankingColumn") != NULL;

	gnetDebug3("\t%s [%d] %s", name, id, isRanking ? "Ranking" : "");

	m_pCurrentLeaderboardColumn = m_pCurrentLeaderboard->AddColumn(id, name, ordinal, isRanking);
	gnetAssert(m_pCurrentLeaderboardColumn);
}

void netSCGamerConfigParser::ParseAggregation( parElement& elt )
{
	if(!gnetVerify(m_pCurrentLeaderboardColumn))
		return;

	const char* aggType = findString(elt, "type");
	m_pCurrentLeaderboardColumn->m_aggregation.SetAggTypeFromString(aggType);

	u16 condColId = (u16) findInt(elt, "conditionalOnColumnId", 0xFFFF);
	m_pCurrentLeaderboardColumn->m_aggregation.m_conditionalOnColumnId = condColId;

	gnetDebug3("\t\tAgg: %s", aggType);
	if(condColId != 0xFFFF)
		gnetDebug3("\t\t\tConditional on Col: %d", condColId);
}

void netSCGamerConfigParser::ParseAggGameInput( parElement& elt )
{
	if(!gnetVerify(m_pCurrentLeaderboardColumn))
		return;

	u16 inputId = (u16) findInt(elt, "inputId", 0xFFFF);
	m_pCurrentLeaderboardColumn->m_aggregation.m_gameInput.m_inputId = inputId;

	gnetDebug3("\t\t\tGameInputId - %d", inputId);
}

void netSCGamerConfigParser::ParseAggColumnInput( parElement& elt )
{
	if(!gnetVerify(m_pCurrentLeaderboardColumn))
		return;

	u8 ordinal = (u8) findInt(elt, "ordinal", 0xFF);
	u8 columnId = (u8) findInt(elt, "columnId", 0xFF);

	m_pCurrentLeaderboardColumn->m_aggregation.m_columnInput.Grow(2).Set(ordinal, columnId);

	gnetDebug3("\t\t\tColInputId - %d", columnId);
}

//////////////////////////////////////////////////////////////////////////
//
//		BANK stuff
//
#if __BANK
void Bank_HandleCreateWidgets()
{
	netSCGamerConfigParser::Bank_CreateWidgets();
}
void netSCGamerConfigParser::Bank_InitWidgets( bkBank* bank )
{
	GAME_CONFIG_PARSER.m_bankGroup = bank->PushGroup("NetGameConfigParser");
		bank->AddButton("Create Game Parser Widgets", CFA(Bank_HandleCreateWidgets));
	bank->PopGroup();
}

void netSCGamerConfigParser::Bank_CreateWidgets()
{
	GAME_CONFIG_PARSER.Bank_DoCreateWidgets();
}
void netSCGamerConfigParser::Bank_DoCreateWidgets()
{
	//Find our bank.
	bkBank* bank = BANKMGR.FindBank("Network");
	bank->SetCurrentGroup(*m_bankGroup);
	{
		//Add a new group for each LB
		const LeaderboardsConfiguration::LeaderboardConfigList& leaderboardList = m_config.m_leaderboardConfiguration.GetLeadeboardList();
		for( int i = 0; i < leaderboardList.GetCount(); i++)
		{
			const Leaderboard& rLeaderboard = leaderboardList[i];
			char tempString[128];
			formatf(tempString, "%s [%d]", rLeaderboard.GetName(), rLeaderboard.m_id);
			bank->PushGroup(tempString);
			for (int cols = 0; cols < rLeaderboard.m_columns.GetCount(); cols++)
			{
				const LeaderboardColumn& rCol = rLeaderboard.m_columns[cols];
				formatf(tempString, "%s", rCol.GetName());
				bank->PushGroup(tempString);
				bank->PopGroup();
			}
			bank->PopGroup();
		}
	}
	bank->UnSetCurrentGroup(*m_bankGroup);
	
}
#endif //__BANK

//////////////////////////////////////////////////////////////////////////
//
//				Static Accessor and Member functions
//
void netSCGamerConfigParser::Init( const char* gameconfigFileName )
{
	//Create the instance if it doesn't exist.
	if(!netSCGamerConfigParserSingleton::IsInstantiated())
	{
		netSCGamerConfigParserSingleton::Instantiate();
	}

	GAME_CONFIG_PARSER.LoadDataXMLFile(gameconfigFileName);
}

const LeaderboardsConfiguration::LeaderboardConfigList& netSCGamerConfigParser::GetLeaderboardList() const
{
	return m_config.m_leaderboardConfiguration.GetLeadeboardList();
}

const Leaderboard* netSCGamerConfigParser::GetLeaderboard( const char* name ) const
{
	const LeaderboardsConfiguration::LeaderboardConfigList& leaderboardList = GetLeaderboardList();
	u32 nameHash = atStringHash(name);

	for(int iter = 0; iter < leaderboardList.GetCount(); iter++)
	{
		const Leaderboard& lb = leaderboardList[iter];
		if (nameHash == lb.GetNameHash())
		{
			return &lb;
		}
	}

	return NULL;
}

const Leaderboard* netSCGamerConfigParser::GetLeaderboard( const u32 lbId ) const
{
	const LeaderboardsConfiguration::LeaderboardConfigList& leaderboardList = GetLeaderboardList();
	for(int iter = 0; iter < leaderboardList.GetCount(); iter++)
	{
		const Leaderboard& lb = leaderboardList[iter];
		if (lbId == (u32)lb.m_id)
		{
			return &lb;
		}
	}

	return NULL;
}

const LeaderboardInput* netSCGamerConfigParser::GetInputById( const u32 inputId ) const
{
	const atArray<LeaderboardInput>& inputList = m_config.m_leaderboardConfiguration.GetInputList();
	for(int inputIter = 0; inputIter < inputList.GetCount(); ++inputIter)
	{
		if(inputList[inputIter].m_id == inputId)
			return &inputList[inputIter];
	}

	return NULL;
}

int netSCGamerConfigParser::GetInputsOnLeaderboard( const Leaderboard& rLeaderboard, LeaderboardInputList& out_InputList ) const
{
	//Go through each column and snag the ones that have game inputs defined.
	//NOT ALL COLUMNS with have game inputs
	for(int colIter = 0; colIter <  rLeaderboard.m_columns.GetCount(); ++colIter)
	{
		const LeaderboardColumn& column = rLeaderboard.m_columns[colIter];
		if(column.m_aggregation.m_gameInput.m_inputId != 0)
		{
			//Find this input in the big input definition list
			const LeaderboardInput* pInput = GetInputById(column.m_aggregation.m_gameInput.m_inputId);
			if (pInput)
			{
				//Add it to the output list
				out_InputList.PushAndGrow(pInput);
			}
		}
	}

	return out_InputList.GetCount();
}

int netSCGamerConfigParser::GetSelectorFromCategoryGroup( const LeaderboardCategoryGroup& in_lbCatGroup, rlLeaderboard2GroupSelector& out_grpSelctor ) const
{
	out_grpSelctor.Clear();

	//Each LeaderboardCategoryGroup is stored as a string comma separated list of categories
	if (in_lbCatGroup.m_value.length() > 0)
	{
		//Iterate to get each category in the group
		bool done = false;
		const char* categoryList = in_lbCatGroup.m_value.c_str();
		for(int listIter = 0; listIter < RL_LEADERBOARD2_MAX_GROUPS && !done; listIter++)
		{
			char categoryName[RL_LEADERBOARD2_CATEGORY_MAX_CHARS];
			unsigned bufLen = sizeof(categoryName);
			done = parMemberArray::ReadCsvDatum(categoryName, &bufLen, categoryList);
			if (bufLen > 0)
			{
				//We don't fill the id, just the group (which is a category).
				out_grpSelctor.m_Group[out_grpSelctor.m_NumGroups].m_Id[0] = '\0';  
				
				//copy the name of the category (incrementing the count).
				safecpy(out_grpSelctor.m_Group[out_grpSelctor.m_NumGroups++].m_Category, categoryName);
			}
		}
	}

	return out_grpSelctor.m_NumGroups;
}

const int netSCGamerConfigParser::GetCategoryGroupsForLeaderboard( const Leaderboard& rLeaderboard, atArray<rlLeaderboard2GroupSelector>& out_CatGroups ) const
{
	for(int iter = 0; iter < rLeaderboard.m_categoryGroups.GetCount(); ++iter)
	{
		const LeaderboardCategoryGroup& catGroupDef = rLeaderboard.m_categoryGroups[iter];
		if (catGroupDef.m_value.length() > 0)
		{
			//Add an new entry to the return list
			rlLeaderboard2GroupSelector& newEntry = out_CatGroups.Grow();
			gnetVerify(GetSelectorFromCategoryGroup(catGroupDef, newEntry));
		}
	}

	return out_CatGroups.GetCount();
}



