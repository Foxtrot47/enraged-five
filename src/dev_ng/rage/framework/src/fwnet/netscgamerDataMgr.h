//===========================================================================
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.             
//===========================================================================

#ifndef ROSDATAMGR_H
#define ROSDATAMGR_H


#include "data/base.h"
#include "atl/string.h"
#include "rline/ros/rlros.h"
#include "fwnet/netcloudfiledownloadhelper.h"

namespace rage {

class bkBank;

class netSCGamerDataMgr : public datBase
{
public:
	netSCGamerDataMgr();

	void Init(const char* gameCode);
	void Update();
	void Shutdown();

	void SetActiveGamerIndex(int gamerIndex);

	void RequestDataRefresh() { m_bUpdateRequested = true; }

	struct gamerDataValue
	{
		u32 m_nameHash;

		enum valType
		{
			TYPE_INVALID,
			TYPE_INT,
			TYPE_FLOAT,
			TYPE_BOOL,
			TYPE_UNS,
			TYPE_STRING,
		} m_Type;

		union 
		{
			bool m_bVal;
			int m_iVal;
			float m_fVal;
			u32 m_uVal;
		};

		atString m_stringValue;

#if __BANK
		char m_debugName[32];
		void DebugPrint();
#endif

		gamerDataValue() { Reset(); }

		void Set(const char* name, bool val);
		void Set(const char* name, int val);
		void Set(const char* name, float val);
		void Set(const char* name, u32 val);
		void Set(const char* name, const char* val);

		void Reset()
		{
			m_Type = TYPE_INVALID;
			m_iVal = 0;
			m_nameHash = 0;
			m_stringValue.Clear();
		}
	};

	template<typename T>
	bool GetValue(const char* name, T &val) const
	{
		return GetValue(atStringHash(name), val);
	}

	bool GetValue(u32 namehash, bool &val) const;
	bool GetValue(u32 namehash, int &val) const;
	bool GetValue(u32 namehash, float &val) const;
	bool GetValue(u32 namehash, u32 &val) const;
	bool GetValue(u32 namehash, gamerDataValue& val) const;


	bool GetValue(u32 namehash, char* val, unsigned len) const;
	template<int SIZE>
	bool GetValue(u32 namehash, char (&buf)[SIZE]) const
	{
		return GetValue(namehash, buf, SIZE);
	}

	bool IsCheater() const;
	bool IsRockstarDev() const;
	bool IsRockstarQA() const;
	bool GetActiveXPBonus(float &xpBonus) const;
    bool HasCheaterOverride() const;
    int GetCheaterReason() const;
    bool IsBadSport() const;
	bool HasBadSportOverride() const;

	bool BadSportOverrideStart(u32& posixTimeStart) const;
	bool BadSportOverrideEnd(u32& posixTimeEnd) const;

    bool IsAutoMuted() const;
    bool HasAutoMuteOverride() const;

#if __BANK
	bool AddValue(gamerDataValue& newValue);
	void OverrideCheaterFlag(bool bCheater);
	void OverrideCheaterRating(int bCheater);
#endif

private:

	void RequestInfo(int gamerIndex);
	void HandleFileReceived(netCloudRequestHelper*);
	void ProcessReceivedData();

	// retrieve values as delivered by SC
	bool IsCheaterSC() const;
	bool IsBadSportSC() const;
	bool IsRockstarDevSC() const;
	bool IsRockstarQASC() const;
	bool GetActiveXPBonusSC(float &xpBonus) const;

	netCloudRequestGetMemberFile m_cloudFile;

	rlCloudWatcher m_cloudWatcher;
	void OnCloudFileModified( const char* OUTPUT_ONLY(pathSpec), const char* /*fullPath*/ );

	char m_gameCode[5];  //game specific 3-4 letter abbreviation (e.g. mp3, gtav) plus \0

	// Event handler for processing Ros
	void OnRosEvent(const rlRosEvent&);
	
	// delegate for ROS event handlers
	rlRos::Delegate m_ROSDlgt;
	bool m_bUpdateRequested;
	int m_iActiveGamerIndex;
	
	atArray<gamerDataValue> m_values;

	// cache values of frequently accessed attributes
	bool m_bIsRockstarDev;
	bool m_bIsRockstarQA;
	bool m_bIsCheater;
	bool m_bIsBadSport;
	bool m_bIsXPBonusActive;
	float m_fXPBonus;

#if __BANK
public:
	void CreateDebugWidgets(bkBank* pBank);
	void Bank_HandleReadData();
	void Bank_DumpCurrent();
	void Bank_GenerateXPBonus();

	bool m_bBank_Override;
	bool m_bBank_IsCheater;
	bool m_bBank_IsRockstarDev;
	bool m_bBank_IsRockstarQA;

	float m_Bank_XPBonus;
	int m_Bank_XPBonus_StartTime;
	int m_Bank_XPBonus_EndTime;
	bool m_bBank_IsXPBonusActive;
	netStatus m_bankPostStatus;
#endif
};

} // namespace rage

#endif // ROSDATAMGR_H
