// 
// netArrayHandlerTypes.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

// game headers
#include "fwnet/netarrayhandlertypes.h"
#include "fwnet/optimisations.h"
#include "fwscript/scriptinterface.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

unsigned CBroadcastDataElement::ms_numUnusedUnits = 0;

// ================================================================================================================
// netScriptBroadcastDataHandlerBase
// ================================================================================================================

void netScriptBroadcastDataHandlerBase::SetScriptHandler(const scriptHandler* scriptHandler)
{
	m_pScriptHandler = scriptHandler;

	for (u32 i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
	{
		m_participantSlots[i] = INVALID_PARTICIPANT_SLOT;

		if (m_pScriptHandler && AssertVerify(m_pScriptHandler->GetNetworkComponent()))
		{
			netPlayer* pPlayer = netInterface::GetPhysicalPlayerFromIndex((PhysicalPlayerIndex)i);

			if (pPlayer && m_pScriptHandler->GetNetworkComponent()->IsPlayerAParticipant(*pPlayer))
			{
				int slot = m_pScriptHandler->GetNetworkComponent()->GetSlotParticipantIsUsing(*pPlayer);
				Assert(slot >= 0);
				m_participantSlots[i] = (u8) slot;

				char logStr[20];
				sprintf(logStr, "Participant slot %d", slot);
				netInterface::GetArrayManager().GetLog().WriteDataValue(logStr, "%s", pPlayer->GetLogName());
			}
		}
	}
}

void netScriptBroadcastDataHandlerBase::PlayerHasLeft(const netPlayer& player)
{
	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::PlayerHasLeft(player);

	u8 slot = m_participantSlots[player.GetPhysicalPlayerIndex()];

	if (slot != INVALID_PARTICIPANT_SLOT)
	{
		if (IsArrayLocallyArbitrated())
		{
			NetworkLogUtils::WriteLogEvent(netInterface::GetArrayManager().GetLog(), "PLAYER_HAS_LEFT_SCRIPT", "%s", player.GetLogName());
			netInterface::GetArrayManager().GetLog().WriteDataValue("Identifier", GetIdentifier()->GetLogName());
			netInterface::GetArrayManager().GetLog().WriteDataValue("Participant slot was", "%u", slot);
		}
	}

	m_participantSlots[player.GetPhysicalPlayerIndex()] = INVALID_PARTICIPANT_SLOT;
}

void netScriptBroadcastDataHandlerBase::PlayerHasJoined(const netPlayer& player)
{
	if (AssertVerify(m_pScriptHandler && m_pScriptHandler->GetNetworkComponent()))
	{
		if (m_pScriptHandler->GetNetworkComponent()->IsPlayerAParticipant(player))
		{
			int slot = m_pScriptHandler->GetNetworkComponent()->GetSlotParticipantIsUsing(player);
			Assert(slot >= 0);
			m_participantSlots[player.GetPhysicalPlayerIndex()] = (u8) slot;

			if (IsArrayLocallyArbitrated())
			{
				NetworkLogUtils::WriteLogEvent(netInterface::GetArrayManager().GetLog(), "PLAYER_HAS_JOINED_SCRIPT", "%s", player.GetLogName());
				netInterface::GetArrayManager().GetLog().WriteDataValue("Identifier", GetIdentifier()->GetLogName());
				netInterface::GetArrayManager().GetLog().WriteDataValue("Participant slot", "%u", slot);
			}
		}
	}

	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::PlayerHasJoined(player);
}

bool netScriptBroadcastDataHandlerBase::CanSendUpdate(const netPlayer& player) const
{
	static const unsigned int PLAYER_TIME_IN_SCRIPT_SESSION_BEFORE_UPDATE = 500; 

	if (m_participantSlots[player.GetPhysicalPlayerIndex()] == INVALID_PARTICIPANT_SLOT)
	{
		netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
		log.Log("\t## %s %s : trying to send update to %s - the player is not a participant ##\r\n", GetHandlerName(), GetIdentifier()->GetLogName(), player.GetLogName());
		gnetAssertf(0, "%s %s : trying to send update to %s - the player is not a participant", GetHandlerName(), GetIdentifier()->GetLogName(), player.GetLogName());
		return false;
	}

	if (AssertVerify(m_pScriptHandler && m_pScriptHandler->GetNetworkComponent()))
	{
		u32 timeAsParticipant =  m_pScriptHandler->GetNetworkComponent()->GetTimePlayerHasBeenAParticipant(m_participantSlots[player.GetPhysicalPlayerIndex()]);
		
		if (timeAsParticipant < PLAYER_TIME_IN_SCRIPT_SESSION_BEFORE_UPDATE)
		{
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
			log.Log("\t## %s %s : holding back sending data to %s - player not been in script session long enough (%d ms) ##\r\n", GetHandlerName(), GetIdentifier()->GetLogName(), player.GetLogName(), timeAsParticipant);
			return false;
		}
	}

	return netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::CanSendUpdate(player);
}

PlayerFlags netScriptBroadcastDataHandlerBase::GetRemotePlayersInScopeBitmask() const
{
	PlayerFlags playersBitmask = 0;

	// the script handler slot is used as the sync data index for broadcast data arrays
	for (u32 i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
	{
		if (m_participantSlots[i] != INVALID_PARTICIPANT_SLOT)
		{
			netPlayer* pPlayer = netInterface::GetPhysicalPlayerFromIndex((PhysicalPlayerIndex)i);

			if (AssertVerify(pPlayer) && !pPlayer->IsLocal())
			{
				playersBitmask |= (1<<m_participantSlots[i]);
			}
		}
	}

	return playersBitmask;
}

#if __BANK
void netScriptBroadcastDataHandlerBase::WriteUpdate(const netPlayer& player, datBitBuffer& bitBuffer, netSequence updateSeq, unsigned& currentElement, bool logSizes)
{
	int cursorPos = bitBuffer.GetCursorPos();

	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::WriteUpdate(player, bitBuffer, updateSeq, currentElement, logSizes);

	AddBandwidthOut(bitBuffer.GetCursorPos()-cursorPos);
}

bool netScriptBroadcastDataHandlerBase::ReadUpdate(const netPlayer& player, datBitBuffer& bitBuffer, unsigned dataSize, netSequence updateSeq)
{
	int cursorPos = bitBuffer.GetCursorPos();

	bool bRet = netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::ReadUpdate(player, bitBuffer, dataSize, updateSeq);

	AddBandwidthIn(bitBuffer.GetCursorPos()-cursorPos);

	return bRet;
}

void netScriptBroadcastDataHandlerBase::AddBandwidthOut(unsigned dataWritten)
{
	if (dataWritten > m_peakBandwidthOut)
	{
		m_peakBandwidthOut = dataWritten;
	}

	m_totalBandwidthOut += dataWritten;

	if (AssertVerify(m_pScriptHandler && m_pScriptHandler->GetNetworkComponent()))
	{
		m_pScriptHandler->GetNetworkComponent()->AddBandwidthOut(dataWritten);
	}
}

void netScriptBroadcastDataHandlerBase::AddBandwidthIn(unsigned dataRead)
{
	if (AssertVerify(m_pScriptHandler && m_pScriptHandler->GetNetworkComponent()))
	{
		m_pScriptHandler->GetNetworkComponent()->AddBandwidthIn(dataRead);
	}
}
#endif // __BANK

void netScriptBroadcastDataHandlerBase::SetHasHighFrequencyUpdates(bool bSet)
{
	if (bSet)
	{
		u32 sizeOfScriptWord = sizeof(scrValue);
		u32 sizeOfElement = CBroadcastDataElement::SIZEOF_BROADCAST_DATA_ELEMENT;
		u32 wordsPerElement = sizeOfElement / sizeOfScriptWord;
		u32 numWordsInArray = m_NumElementsInUse*wordsPerElement;

		if (numWordsInArray > MAX_HIGH_FREQUENCY_SCRIPT_WORDS)
		{
			gnetAssertf(0, "%s broadcast array handler is too large for high frequency updates - contains %d script ints - max size is %d ints", 
							GetHandlerType() == HOST_BROADCAST_DATA_ARRAY_HANDLER ? "Host" : "Player", 
							numWordsInArray,
							MAX_HIGH_FREQUENCY_SCRIPT_WORDS);
			return; 
		}
	}

	m_bHasHighFrequencyUpdates = bSet;
}

//PURPOSE
// Sets the unused units for the last element in the array. Used when the array size is less than a multiple of the size of CBroadcastDataElement
void netScriptBroadcastDataHandlerBase::SetUnusedUnits(unsigned index)
{
	unsigned byteSizeOfThisIndex = (index*sizeof(CBroadcastDataElement));
	unsigned byteSizeOfNextIndex = ((index+1)*sizeof(CBroadcastDataElement));

	if (byteSizeOfThisIndex >= m_SizeOfArrayInBytes)
	{
		m_Element.SetAllUnused();
	}
	else if (byteSizeOfNextIndex > m_SizeOfArrayInBytes)
	{
		unsigned numUnusedBytes = (byteSizeOfNextIndex - m_SizeOfArrayInBytes);
		Assert(numUnusedBytes%4 == 0);
		m_Element.SetNumUnusedUnits(numUnusedBytes>>2);
	}
	else
	{
		m_Element.SetNumUnusedUnits(0);
	}
}

void netScriptBroadcastDataHandlerBase::SetArrayData(CBroadcastDataElement* arrayData, unsigned sizeOfArrayInBytes) 
{
	m_SizeOfArrayInBytes = sizeOfArrayInBytes;

	// recalculate the bitsize of the index, as it only needs to be big enough for the number of elements we will be using
	m_BitsizeOfIndex = 0;

	if (arrayData)
	{
		unsigned numUsedElements = m_SizeOfArrayInBytes / GetMaxElementSizeInBytes();

		if (m_SizeOfArrayInBytes % GetMaxElementSizeInBytes() != 0)
			numUsedElements++;

		m_NumElementsInUse = static_cast<u16>(numUsedElements);

		if (!gnetVerify(m_NumElementsInUse != 0))
		{
			m_NumElementsInUse = 1;
		}

		unsigned n = numUsedElements;

		while (n != 0)
		{
			n >>= 1;
			m_BitsizeOfIndex++;
		}
	}
	else
	{
		m_NumElementsInUse = 0;
	}

	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::SetArrayData(arrayData, sizeOfArrayInBytes);

#if __DEV
	if (m_Array && !IsArrayLocallyArbitrated())
	{
		CacheArrayContents(CalculateChecksum());
	}

	m_pEndOfArray = reinterpret_cast<u8*>(arrayData)+sizeOfArrayInBytes;

#endif
}

void netScriptBroadcastDataHandlerBase::ApplyElementData(unsigned index, const netPlayer& player)
{
	ASSERT_ONLY(u32 endOfArrayValue = *m_pEndOfArray);

	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::ApplyElementData(index, player);

	Assert(endOfArrayValue == *m_pEndOfArray);

#if __DEV
	m_bUpdatedThisFrame = true;
	m_bClientAlterationPermitted = false;
#endif

	m_bHasReceivedAnUpdate = true;
}

#if __DEV

void netScriptBroadcastDataHandlerBase::Update()
{
	m_bDirtiedThisFrame = false;

	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::Update();

	// if the data is remotely arbitrated, assert when we detect that the data has been altered locally
	if (!m_arrayDataInvalid && !m_bClientAlterationPermitted && (!GetPlayerArbitrator() || !GetPlayerArbitrator()->IsLocal()))
	{
		unsigned currentChecksum = CalculateChecksum();

		if (currentChecksum != m_dataChecksum)
		{
			netBroadcastDataArrayIdentifierBase* pIdentifier = static_cast<netBroadcastDataArrayIdentifierBase*>(GetIdentifier());

			// determine which elements have changed
			if (GetSyncData())
			{
				GetSyncData()->GetShadowBuffer()->SetCursorPos(0);

				unsigned* pArray = reinterpret_cast<unsigned*>(m_Array);
				unsigned element;

				int numIntsInArrayData = static_cast<int>(GetSizeOfArrayDataInBytes()/sizeof(element));

				for (int i=0; i<numIntsInArrayData; i++)
				{
					GetSyncData()->GetShadowBuffer()->ReadUns<unsigned>(element, sizeof(element)<<3);

					if (pArray[i] != element)
					{
						Displayf("%s broadcast data element %d has been altered (was %d, now %d)\n", pIdentifier->GetLogName(), i, element, pArray[i]);
					}
				}
			}
			else
			if(GetBDBackupBuffer())
			{
				unsigned* pArray			= reinterpret_cast<unsigned*>(m_Array);
				unsigned* pArrayBDBackup	= reinterpret_cast<unsigned*>(GetBDBackupBuffer());

				int numIntsInArrayData = static_cast<int>(GetSizeOfArrayDataInBytes()/sizeof(unsigned));

				for (int i=0; i<numIntsInArrayData; i++)
				{
					if (pArray[i] != pArrayBDBackup[i])
					{
						Displayf("%s broadcast data element %d has been altered (was %d, now %d)\n", pIdentifier->GetLogName(), i, pArrayBDBackup[i], pArray[i]);
					}
				}
			}

			if (GetHandlerType() == HOST_BROADCAST_DATA_ARRAY_HANDLER)
			{
				Assertf(currentChecksum == m_dataChecksum, "%s: the host broadcast data has been illegally altered locally", pIdentifier->GetLogName());
			}
			else if (GetHandlerType() == PLAYER_BROADCAST_DATA_ARRAY_HANDLER)
			{
				if (GetPlayerArbitrator())
				{
					Assertf(currentChecksum == m_dataChecksum, "%s: the broadcast data for player %s (participant %d) has been illegally altered locally", pIdentifier->GetLogName(), GetPlayerArbitrator()->GetLogName(), pIdentifier->GetInstanceId());
				}
			}

			CacheArrayContents(currentChecksum);
		}
	}

	if (m_bDirtiedThisFrame && !RequiresHighFrequencyUpdates())
	{
		m_numFramesRepeatedlyDirtied++;

		if (m_numFramesRepeatedlyDirtied > 200)
		{
			netBroadcastDataArrayIdentifierBase* pIdentifier = static_cast<netBroadcastDataArrayIdentifierBase*>(GetIdentifier());

			if (GetHandlerType() == HOST_BROADCAST_DATA_ARRAY_HANDLER)
			{
				Assertf(0, "%s: the host broadcast data has been altered too frequently - does it contain a timer?", pIdentifier->GetLogName());
			}
			else if (GetHandlerType() == PLAYER_BROADCAST_DATA_ARRAY_HANDLER)
			{
				if (GetPlayerArbitrator())
				{
					Assertf(0, "%s: the local player broadcast data been altered too frequently - does it contain a timer?", pIdentifier->GetLogName());
				}
			}
		}
	}
	else
	{
		m_numFramesRepeatedlyDirtied = 0;
	}
}

void netScriptBroadcastDataHandlerBase::HandleChangeOfArbitration(bool bHadPreviousArbitrator)
{
	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::HandleChangeOfArbitration(bHadPreviousArbitrator);

	// calculate the checksum again in case the arbitration has migrated remotely
	if (bHadPreviousArbitrator && !(GetPlayerArbitrator() && !GetPlayerArbitrator()->IsLocal()))
	{
		CacheArrayContents(CalculateChecksum());
	}
}

void netScriptBroadcastDataHandlerBase::DoPostReadProcessing()
{
	netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::DoPostReadProcessing();

	// recalculate the checksum after an update by the remote arbitrator
	if (m_bUpdatedThisFrame)
	{
		if (!m_arrayDataInvalid)
		{
			CacheArrayContents(CalculateChecksum());
		}

		m_bUpdatedThisFrame = false;
	}
}

void netScriptBroadcastDataHandlerBase::CacheArrayContents(unsigned checksum)
{
	m_dataChecksum = checksum;

	// use the shadow buffer to cache the current array contents
	if (AssertVerify(m_Array) && GetSyncData() && AssertVerify(!IsArrayLocallyArbitrated()))
	{
		GetSyncData()->GetShadowBuffer()->SetCursorPos(0);

		unsigned* pArray = reinterpret_cast<unsigned*>(m_Array);
		int numIntsInArrayData = static_cast<int>(GetSizeOfArrayDataInBytes()/sizeof(unsigned));

		for (int i=0; i<numIntsInArrayData; i++)
		{
			GetSyncData()->GetShadowBuffer()->WriteUns<unsigned>(pArray[i], sizeof(unsigned)<<3);
		}
	}
	else
	// see if there is a remote player BD buffer shadow buffer to cache the current array contents
	if (AssertVerify(m_Array) && GetBDBackupBuffer() && AssertVerify(!IsArrayLocallyArbitrated()))
	{
		unsigned* pArray			= reinterpret_cast<unsigned*>(m_Array);
		unsigned* pArrayBDBackup	= reinterpret_cast<unsigned*>(GetBDBackupBuffer());
		int numIntsInArrayData = static_cast<int>(GetSizeOfArrayDataInBytes()/sizeof(unsigned));

		for (int i=0; i<numIntsInArrayData; i++)
		{
			pArrayBDBackup[i] = pArray[i];
		}
	}
}

#endif //DEV

// ================================================================================================================
// netHostBroadcastDataHandlerBase
// ================================================================================================================

void netHostBroadcastDataHandlerBase::SetNextFreeHostObjectId(u32 n)  
{ 
	Assert(n < (1<<SIZE_OF_OBJECT_NUM)); 
	m_nextFreeHostObjectId = n; 
	m_nextFreeHostObjectIdChanged = true;
};

void  netHostBroadcastDataHandlerBase::SetPlayerArbitrator(const netPlayer* player)
{
	netPlayerArrayIdentifier* playerIdentifier = static_cast<netPlayerArrayIdentifier*>(GetIdentifier());

	if (player != playerIdentifier->GetPlayerArbitrator())
	{
		netScriptBroadcastDataHandlerBase::SetPlayerArbitrator(player);

		// force an update when the host migrates locally as some machines may be waiting for a host broadcast data update before proceeding
		if (player && player->IsLocal())
		{
			 GetSyncData()->SetSyncDataUnitDirty(0);
		}
	}
}

void netHostBroadcastDataHandlerBase::PlayerHasJoined(const netPlayer& player)
{
	netPlayerArrayIdentifier* playerIdentifier = static_cast<netPlayerArrayIdentifier*>(GetIdentifier());

	netScriptBroadcastDataHandlerBase::PlayerHasJoined(player);

	// force array element 0 dirty so that this player definitely gets at least one host broadcast update 
	if (playerIdentifier->GetPlayerArbitrator() && playerIdentifier->GetPlayerArbitrator()->IsLocal() && IsPlayerInScope(player))
	{
		GetSyncData()->SetSyncDataUnitDirtyForPlayer(0, GetSyncDataIndexForPlayer(player));
	}
}

bool netHostBroadcastDataHandlerBase::CanProcessUpdate(const netPlayer& player, netSequence updateSeq, bool bSplitUpdate) const
{
	// check that this update is from the host of the script
	if (AssertVerify(m_pScriptHandler && m_pScriptHandler->GetNetworkComponent()))
	{
		if (m_pScriptHandler->GetNetworkComponent()->GetHost() != &player)
		{
			return false;
		}
	}

	return netScriptBroadcastDataHandlerBase::CanProcessUpdate(player, updateSeq, bSplitUpdate);
}

unsigned netHostBroadcastDataHandlerBase::GetTotalSizeOfUpdateData(const netPlayer& player, unsigned messageSize, unsigned &numUpdates, u16 updateStartElements[])
{
	unsigned size = netScriptBroadcastDataHandlerBase::GetTotalSizeOfUpdateData(player, messageSize-SIZE_OF_OBJECT_NUM, numUpdates, updateStartElements);
	
	// m_nextFreeHostObjectId has to be sent out in every split update 
	size += SIZE_OF_OBJECT_NUM*numUpdates;

	return size;
}

unsigned netHostBroadcastDataHandlerBase::GetSizeOfMessageUpdateData(const netPlayer& player, unsigned messageSize, unsigned startElement, bool logSizes)
{
	LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());

	unsigned size = SIZE_OF_OBJECT_NUM;

	if (logSizes)
	{
		LOGGING_ONLY(log.Log("\tSIZE OF FREE HOST ID : %d\r\n", SIZE_OF_OBJECT_NUM));
	}
	
	size += netScriptBroadcastDataHandlerBase::GetSizeOfMessageUpdateData(player, messageSize-SIZE_OF_OBJECT_NUM, startElement);

	return size;
}

void netHostBroadcastDataHandlerBase::Init()
{
	netScriptBroadcastDataHandlerBase::Init();

	netPlayerArrayIdentifier* playerIdentifier = static_cast<netPlayerArrayIdentifier*>(GetIdentifier());

	// force an update if the host is local as some machines may be waiting for a host broadcast data update before proceeding
	if (playerIdentifier && playerIdentifier->GetPlayerArbitrator() && playerIdentifier->GetPlayerArbitrator()->IsLocal())
	{
		GetSyncData()->SetSyncDataUnitDirty(0);
	}

	m_nextFreeHostObjectId = 0;
}

void netHostBroadcastDataHandlerBase::RecalculateDirtyElements()
{
	// we can't do this during a host migration ( we do not want to send out any updates while this is happening)
	if (gnetVerify(m_pScriptHandler && m_pScriptHandler->GetNetworkComponent()) && m_pScriptHandler->GetNetworkComponent()->IsHostMigrating())
	{
		return;
	}

	if (m_nextFreeHostObjectIdChanged && !HasUnackedSplitUpdates())
	{
		// dirty element 0 to make sure an update goes out with the new id
		SetElementDirty(0);
		m_nextFreeHostObjectIdChanged = false;
	}

	netScriptBroadcastDataHandlerBase::RecalculateDirtyElements();
}

void netHostBroadcastDataHandlerBase::WriteUpdate(const netPlayer& player, datBitBuffer& bitBuffer, netSequence updateSeq, unsigned& currentElement, bool logSizes)
{
	LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());

	LOGGING_ONLY(u32 startPos = bitBuffer.GetCursorPos());

#if  __ASSERT
	if (m_pScriptHandler && m_pScriptHandler->GetNetworkComponent())
	{
		const netPlayer* pPendingHost = m_pScriptHandler->GetNetworkComponent()->GetPendingHost();
		gnetAssertf(!pPendingHost || pPendingHost->IsLocal(), "Sending a host broadcast data update for %s during a host migration!", m_pScriptHandler->GetLogName());
	}
#endif

	bitBuffer.WriteUns(m_nextFreeHostObjectId, SIZE_OF_OBJECT_NUM);

	if (logSizes)
	{
		LOGGING_ONLY(log.Log("\tWRITTEN SIZE OF FREE HOST ID : %d\r\n", bitBuffer.GetCursorPos() - startPos));
	}
	else
	{
		LOGGING_ONLY(netInterface::GetArrayManager().GetLog().WriteDataValue("Next free host object id", "%d", m_nextFreeHostObjectId));
	}

	netScriptBroadcastDataHandlerBase::WriteUpdate(player, bitBuffer, updateSeq, currentElement);
}

bool netHostBroadcastDataHandlerBase::ReadUpdate(const netPlayer& player, datBitBuffer& bitBuffer, unsigned dataSize, netSequence updateSeq)
{
	bitBuffer.ReadUns(m_nextFreeHostObjectId, SIZE_OF_OBJECT_NUM);

	LOGGING_ONLY(netInterface::GetArrayManager().GetLog().WriteDataValue("Next free host object id", "%d", m_nextFreeHostObjectId));

	dataSize -= SIZE_OF_OBJECT_NUM;

	return netScriptBroadcastDataHandlerBase::ReadUpdate(player, bitBuffer, dataSize, updateSeq);
}

} // namespace rage

