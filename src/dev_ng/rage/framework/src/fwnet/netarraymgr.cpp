// 
// netarraymgr.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "fwnet/netarraymgr.h"

// C includes
#include <functional>

// rage includes
#include "net/event.h"
#include "net/packet.h"

// framework includes
#include "fwnet/netarrayhandlertypes.h"
#include "fwnet/netbandwidthstatscollector.h"
#include "fwnet/netchannel.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netplayer.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"
#include "fwscript/scripthandler.h"
#include "fwscript/scripthandlermgr.h"
#include "fwscript/scriptinterface.h"
#include "fwsys/timer.h"

#if !__FINAL
	#include "script/thread.h"
#endif
NETWORK_OPTIMISATIONS()

PARAM(logArrayHandlerBatching, "Adds logging for array handler update batching");
PARAM(netArraySizeAssert, "If set, the array manager asserts when the size of an array exceeds that which can fit in a single update message");

namespace rage
{

NET_MESSAGE_IMPL( netArrayManager::msgUpdate );
NET_MESSAGE_IMPL( netArrayManager::msgUpdateAck );
NET_MESSAGE_IMPL( netArrayManager::msgSplitUpdateAck );

FW_INSTANTIATE_CLASS_POOL(netArrayManager::updateMsgHistory, (NUM_ARRAY_UPDATES_INFO_HELD_FOR * MAX_NUM_PHYSICAL_PLAYERS),	atHashString("arrayUpdateInfo",0xaf80b31d));

// ================================================================================================================
// netArrayManager
// ================================================================================================================

netArrayManager::netArrayManager(netBandwidthMgr &bandwidthMgr)
: m_UpdateMsgs(MAX_NUM_PHYSICAL_PLAYERS)
, m_ArrayHandlerBatches(NUM_UPDATE_BATCH_TYPES)
, m_LastSentSequences(MAX_NUM_PHYSICAL_PLAYERS)
, m_LastReceivedSequences(MAX_NUM_PHYSICAL_PLAYERS)
, m_UpdateHistories(MAX_NUM_PHYSICAL_PLAYERS)
, m_ArrayIdentifiers(MAX_NUM_ARRAY_HANDLER_TYPES)
, m_CachedUpdates(MAX_CACHED_UPDATES)
, m_Log(0)
, m_LogSplitter(0)
, m_IsInitialized(false)
, m_IsShuttingDown(false)
, m_BitsizeOfHandlerType(0)
, m_BandwidthMgr(bandwidthMgr)
, m_CurrentLocalUpdateBatch(UBT_LOCAL_1)
, m_LastUpdateThrottleTime(0)
, m_MaxMessagesPerThrottleInterval(DEFAULT_MAX_MESSAGES_PER_PLAYER)
#if __DEV
, m_UpdateMsgHeaderRecorderId(INVALID_RECORDER_ID)
, m_UpdateAckMsgRecorderId(INVALID_RECORDER_ID)
#endif
#if __BANK
, m_NumArrayManagerUpdateMessagesSent(0)
, m_NumArrayManagerAckMessagesSent(0)
#endif // __BANK
{
    // Initialize our message handler.  This will be registered with a
    // netConnectionmanager in Init().
    m_Dlgt.Bind(this, &netArrayManager::OnMessageReceived);
}

netArrayManager::~netArrayManager()
{
	if(m_IsInitialized)
	{
	    Shutdown();
	}
}

void netArrayManager::Init()
{
    if(gnetVerify(!m_IsInitialized))
    {
		const unsigned LOG_BUFFER_SIZE = 16 *1024;
		static bool s_HasCreatedLog = false; 
		m_Log = rage_new netLog("ArrayManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                            netInterface::GetDefaultLogFileBlockingMode(),
                                                                            LOG_BUFFER_SIZE);
		s_HasCreatedLog = true;

		m_LogSplitter = rage_new netLogSplitter(*m_Log, netInterface::GetMessageLog());
		
        // Register our message handler.
        netInterface::AddDelegate(&m_Dlgt);

		gnetAssertf(MAX_NUM_ARRAY_HANDLER_TYPES >= static_cast<unsigned>(GetNumArrayHandlerTypes()), "Too many array handler types, increase size of netArrayManager::MAX_NUM_ARRAY_HANDLER_TYPES");

		unsigned i;
		for (i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			m_LastSentSequences[i] = 0;
			m_LastReceivedSequences[i] = 0;
            m_NumUpdatesSent[i]        = 0;
		}

		for (i=0; i<MAX_NUM_ARRAY_HANDLER_TYPES; i++)
		{
			m_ArrayIdentifiers[i] = NULL;
		}
	
		// allocate the update history pool out of the network heap
		updateMsgHistory::InitPool( MEMBUCKET_NETWORK );

#if __BANK
		m_BandwidthMgr.RegisterStatistics(m_BandwidthStats, "Array Manager");

		m_UpdateMsgHeaderRecorderId	= m_BandwidthStats.AllocateBandwidthRecorder("Array message headers");
		m_UpdateAckMsgRecorderId = m_BandwidthStats.AllocateBandwidthRecorder("Array ack messages");

		m_ArrayRecorderIds.Resize(MAX_NUM_ARRAY_HANDLER_TYPES);

		for (i=0; i<MAX_NUM_ARRAY_HANDLER_TYPES; i++)
		{
			m_ArrayRecorderIds[i] = INVALID_RECORDER_ID;
		}

        m_NumArrayManagerUpdateMessagesSent = 0;
        m_NumArrayManagerAckMessagesSent    = 0;
#endif

		unsigned numHandlerTypes = GetNumArrayHandlerTypes();

		m_BitsizeOfHandlerType = 0;

		while (numHandlerTypes != 0)
		{
			numHandlerTypes >>= 1;
			m_BitsizeOfHandlerType++;
		}

		m_CurrentLocalUpdateBatch = UBT_LOCAL_1;
        m_LastUpdateThrottleTime  = 0;
		
		m_ArrayHandlers.Resize(GetNumArrayHandlerTypes());

		m_IsInitialized = true;
    }
}

void netArrayManager::Shutdown()
{
    if(gnetVerify(m_IsInitialized))
    {
		m_IsShuttingDown = true;

		for (u32 i=0; i<NUM_UPDATE_BATCH_TYPES; i++)
		{
			while (!m_ArrayHandlerBatches[i].empty())
			{
				UnregisterArrayHandler(*m_ArrayHandlerBatches[i].begin());
			}
		}

        netInterface::RemoveDelegate(&m_Dlgt);

		// check the update history queues are empty
		for (unsigned player=0; player<MAX_NUM_PHYSICAL_PLAYERS; player++)
		{
			while(!m_UpdateHistories[player].empty())
			{
				updateMsgHistory *updateInfo = m_UpdateHistories[player].front();
				m_UpdateHistories[player].pop_front();
				delete updateInfo;
			}
		}

		updateMsgHistory::ShutdownPool();

        delete m_LogSplitter;
        delete m_Log;

        m_LogSplitter = 0;
        m_Log         = 0;

		for (int i=0; i<m_ArrayHandlers.GetCount(); i++)
		{
			m_ArrayHandlers[i].clear();
		}

		m_IsInitialized = false;
		m_IsShuttingDown = false;
	}
}

void netArrayManager::Update()
{
    netTimeBarWrapper timeBarWrapper(netInterface::ShouldUpdateTimebars());

	// precalculate the size of the array manager header data at the start of a standard update message
	const unsigned commonUpdateHeaderSize = GetCommonUpdateHeaderSize();

	// precalculate the size of the array manager header data at the start of a split update message
	const unsigned commonSplitUpdateHeaderSize = commonUpdateHeaderSize + SIZEOF_UPDATE_NUM*2 + 1;

	// periodically update the handlers so that the update messages are not sent too often
    timeBarWrapper.StartTimer("Update handlers");

	arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[m_CurrentLocalUpdateBatch].begin();
	arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[m_CurrentLocalUpdateBatch].end();

	// update the handlers
	for(; curr != end; ++curr)
	{
		netArrayHandlerBase* handler = *curr;

		handler->Update(); 
	}
    
	// high frequency handlers are updated every frame
	curr = m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].begin();
	end = m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].end();

	for(; curr != end; ++curr)
	{
		netArrayHandlerBase* handler = *curr;

		handler->Update(); 
	}

#if !__FINAL
	scrThread::CheckIfMonitoredScriptArraySizeHasChanged("Update array handlers", 0);
#endif

	timeBarWrapper.StartTimer("Player Updates");

    // send out any handler updates that pending to each player. Unfortunately we can't send updates from a local player to a local
	// bot or vice versa because this won't work properly with the update history. 
	// TODO: Possibly modify update history to cope with this 
    unsigned                 numRemotePhysicalPlayers	= netInterface::GetNumRemotePhysicalPlayers();
    const netPlayer * const *remotePhysicalPlayers		= netInterface::GetRemotePhysicalPlayers();
    unsigned                 numLocalPhysicalPlayers	= netInterface::GetPlayerMgr().GetNumLocalPhysicalPlayers();
    const netPlayer * const *localPhysicalPlayers		= netInterface::GetPlayerMgr().GetLocalPhysicalPlayers();


    // Iterate over all local players (i.e. our player + bots). Each local player needs to send it's own update.
    for(unsigned index = 0; index < numLocalPhysicalPlayers; index++)
    {
        const netPlayer *localPlayer = localPhysicalPlayers[index];

        for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
	       const netPlayer *remotePlayer = remotePhysicalPlayers[index];

		   // ignore players who have just joined: we don't want to send any updates to them until they have had a chance to register all their
		   // array handlers. If we don't do this we get a lot of ignored updates and a huge bandwidth spike just after they join.
           if (remotePlayer && remotePlayer->GetTimeInSession() > PLAYER_TIME_IN_SESSION_BEFORE_UPDATE)
           {
                PhysicalPlayerIndex playerIndex = remotePlayer->GetPhysicalPlayerIndex();

                datBitBuffer* updateMsgBB = &m_UpdateMsgs[playerIndex].m_bb;

				static const unsigned MAX_NUM_ARRAY_HANDLERS_TO_SORT = 100;

				handlerUpdateInfo sortedHandlers[MAX_NUM_ARRAY_HANDLERS_TO_SORT];
	            unsigned numSortedHandlers = 0;

				// always include high frequency arrays
				static const unsigned NUM_BATCH_TYPES_TO_SORT = 2; // UBT_HIGH_FREQUENCY and current batch type

				u32 batchesToProcess[NUM_BATCH_TYPES_TO_SORT] = { UBT_HIGH_FREQUENCY, m_CurrentLocalUpdateBatch };

				for (u32 bt = 0; bt < NUM_BATCH_TYPES_TO_SORT; bt++)
				{
					u32 batchType = batchesToProcess[bt];

					arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[batchType].begin();
					arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[batchType].end();

					// calculate the update information for each handler so we can sort the handlers based on their update size
					for(; curr != end; ++curr)
					{
						netArrayHandlerBase* handler = *curr;
    				
						if (handler->RequiresHighFrequencyUpdates())
						{
							gnetAssertf(batchType==UBT_HIGH_FREQUENCY, "%s (%s) is a high frequency update array but is not in the high frequency update batch", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "");
						}
						else
						{
							gnetAssertf(batchType!=UBT_HIGH_FREQUENCY, "%s (%s) is not a high frequency update array but is in the high frequency update batch", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "");
						}

						const netPlayer* playerArbitrator = handler->GetPlayerArbitratorOfArray();

						// an array can be locally arbitrated, but have no single player as the arbitrator (eg a shared array). This just means that there 
						// are some elements that we are locally arbitrating.
						if (handler->IsArrayLocallyArbitrated() && ((!playerArbitrator && localPlayer->IsMyPlayer()) || playerArbitrator == localPlayer))
						{
							if (!handler->IsSyncedWithPlayer(*remotePlayer))
							{
								if (handler->CanSendUpdate(*remotePlayer))
								{
									unsigned maxBits = updateMsgBB->GetMaxBits() - commonSplitUpdateHeaderSize - (handler->GetIdentifier() ? handler->GetIdentifier()->GetSize() : 0);
									unsigned updateSize, numUpdates;

									handlerUpdateInfo* updateInfo = &sortedHandlers[numSortedHandlers];

									updateSize = handler->GetTotalSizeOfUpdateData(*remotePlayer, maxBits, numUpdates, updateInfo->m_UpdateStartElements);

									// this can be 0 if the array handler has unsynced elements that are not currently in scope with this player
									if (updateSize > 0)
									{
										updateInfo->m_Handler = handler;
										updateInfo->m_TotalUpdateSize = updateSize;
										updateInfo->m_NumUpdates = numUpdates;

										numSortedHandlers++;

										NETWORK_QUITF(numSortedHandlers < MAX_NUM_ARRAY_HANDLERS_TO_SORT, "Num sorted handlers (%d) exceeded MAX_NUM_ARRAY_HANDLERS_TO_SORT (%d)", numSortedHandlers, MAX_NUM_ARRAY_HANDLERS_TO_SORT);
									}
								}
								else
								{
	#if ENABLE_NETWORK_LOGGING
									netLogSplitter logSplitter(netInterface::GetArrayManager().GetLog(), *scriptInterface::GetScriptManager().GetLog());
									logSplitter.Log("\t## %s %s : can't send update to %s ##\r\n", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "", remotePlayer->GetLogName());
	#endif
								}
							}
							else 
							{
								SplitUpdateFlags splitUpdateFlags = handler->GetUnackedSplitUpdateFlags(*remotePlayer);

								if (splitUpdateFlags != 0)
								{
									gnetAssertf(0, "%s (%s) has unacked split update flags for player %s (%d) but is synced with this player", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "", remotePlayer->GetLogName(), splitUpdateFlags);
									handler->ResetUnackedSplitUpdateFlagsForPlayer(*remotePlayer);
								}
							}
						}
					}
   			
					// Sort the array handlers based on how much data they need to send to the player. This is done to maximise the number of handler 
					// updates that can be packed into one message.
					if (numSortedHandlers > 2)
					{
						qsort(&sortedHandlers[0], numSortedHandlers, sizeof(handlerUpdateInfo), &SortHandlerByUpdateSize);
					}

					bool canWriteUpdates = true;

					// now process the sorted list, sending out the update messages
					for (unsigned i=0; i<numSortedHandlers && canWriteUpdates; i++)
					{
						handlerUpdateInfo*		updateInfo			= &sortedHandlers[i];
						netArrayHandlerBase*	handler				= updateInfo->m_Handler;
						bool					bSplitUpdate		= updateInfo->m_NumUpdates > 1;
						bool					bCacheSplitUpdates	= handler->CacheSplitUpdates();
						unsigned				identifierSize		= handler->GetIdentifier() ? handler->GetIdentifier()->GetSize() : 0;
						unsigned				headerSize			= (bSplitUpdate ? commonSplitUpdateHeaderSize : commonUpdateHeaderSize) + identifierSize;
						unsigned				updateNum			= 0;
						unsigned				currElement			= 0;

						if (batchType != UBT_HIGH_FREQUENCY && m_NumUpdatesSent[playerIndex] >= m_MaxMessagesPerThrottleInterval)
						{
#if ENABLE_NETWORK_LOGGING
							netLogSplitter logSplitter(netInterface::GetArrayManager().GetLog(), *scriptInterface::GetScriptManager().GetLog());
							logSplitter.Log("\t## %s %s : holding back sending data to %s - max messages per throttle interval exceeded (%d). Num updates sent to player in last second = %d ##\r\n", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "", remotePlayer->GetLogName(), m_MaxMessagesPerThrottleInterval, m_NumUpdatesSent[playerIndex]);
#else
							canWriteUpdates = false;
#endif
						}
						else
						{
							bool allocateNewSequence = true;

							if (bCacheSplitUpdates)
							{
								// Split updates share the same update sequence. This is so that the receiver knows that they belong together. Split updates are sent
								// one at a time and acked individually using a different ack message (msgSplitUpdateAck), except for the last update.
								// The update sequence is not incremented when the split update is being resent (i.e. when it is not dirty). This is so that it is easier 
								// for the receiver to receive all the split updates. If the sequence was incremented every time the update is resent, the receiver will 
								// be forced to dump cached split update messages with an older sequence, even if they are valid. When the handler is dirty again we do 
								// need to increment the sequence so that previous cached split update messages are dumped. This is because the number of array elements 
								// being sent may have changed and so the split data will differ and previously cached updates will be invalid.
								if (updateInfo->m_NumUpdates > 1)
								{
									if (handler->GetUnackedSplitUpdateFlags(*remotePlayer) != 0)
									{
										allocateNewSequence = false;
									}

									handler->SetUnackedSplitUpdateFlags(*remotePlayer, updateInfo->m_NumUpdates);
								}

								SplitUpdateFlags splitUpdateFlags = handler->GetUnackedSplitUpdateFlags(*remotePlayer);

								if (bSplitUpdate)
								{
									// Arrays that are set to cache split updates but do not use sync data buffers will not sync properly. This is because the array contents cannot change while 
									// the split updates are waiting to be acked. If they do then the cached data will be out of date. Handlers that use sync data buffers send out the buffer contents 
									// while waiting for split update acks, and stop recalculating the dirty elements. Handlers that do not use buffers have to grab the current state of the array, 
									// which may have change since the first split update was initially sent out.
									gnetAssertf(!handler->IsManualDirtyArray(), "%s (%s) has a split update and is set to cache split updates. This is not currently possible for array handlers that do not use sync buffers.", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "");
									
									// high frequency arrays should never be large enough to have a split update
									gnetAssertf(batchType != UBT_HIGH_FREQUENCY, "%s (%s) is a high frequency update array and has a split update. Something has gone wrong", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "");
								
									Assert(splitUpdateFlags);

									// send the next unacked split update
									for (u32 j=0; j<updateInfo->m_NumUpdates; j++)
									{
										if (splitUpdateFlags & (1<<j))
										{
											updateNum = j;
											break;
										}
									}

									currElement = updateInfo->m_UpdateStartElements[updateNum];
								}
								else if (splitUpdateFlags != 0)
								{
									gnetAssertf(0, "%s (%s) has unacked split update flags for player %s (%d) but is sending an un-split update", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "", remotePlayer->GetLogName(), splitUpdateFlags);
									handler->ResetUnackedSplitUpdateFlagsForPlayer(*remotePlayer);
								}
							}

							int firstElement = -1;
							int lastElement = -1;

							if (bSplitUpdate && !bCacheSplitUpdates)
							{
								firstElement = updateInfo->m_UpdateStartElements[0];
								lastElement = updateInfo->m_UpdateStartElements[1]-1;
							}	

							netSequence updateSeq = handler->GetNextUpdateSequence(*remotePlayer, allocateNewSequence); 

							StartNewUpdate(*remotePlayer, handler, updateInfo->m_TotalUpdateSize + headerSize, localPlayer, firstElement, lastElement);

							NET_ASSERTS_ONLY(int startPos = updateMsgBB->GetCursorPos());

							// split updates should always come at the start of the update message
							gnetAssert(!bSplitUpdate || startPos == 0); 

							// write a tag to make sure the arrays get processed properly
							SET_MSG_TAG(*updateMsgBB);

							// write the handler type
							updateMsgBB->WriteUns(handler->GetHandlerType(), m_BitsizeOfHandlerType);

							// Write the update sequence.
							updateMsgBB->WriteUns(updateSeq, sizeof(netSequence)<<3);

							// write whether this is a split update
							updateMsgBB->WriteBool(bSplitUpdate);

							if (bSplitUpdate)
							{
								// for a split update we need to send the total number of messages containing the update,
								// plus the number of this message 
								updateMsgBB->WriteUns(updateInfo->m_NumUpdates, SIZEOF_UPDATE_NUM+1);
								updateMsgBB->WriteUns(updateNum, SIZEOF_UPDATE_NUM);
							}

							// write a flag indicating whether there is an identifier or not
							updateMsgBB->WriteBool(handler->GetIdentifier() != NULL);
        		
							DEV_ONLY(unsigned initialElement = currElement);
							unsigned maxUpdateDataSize = updateMsgBB->GetMaxBits() - updateMsgBB->GetCursorPos() - identifierSize - SIZE_OF_HANDLER_DATA_LEN;
							unsigned updateDataSize = handler->GetSizeOfMessageUpdateData(*remotePlayer, maxUpdateDataSize, currElement);

							// write the size of the handler data (the update data + the identifier data)
							updateMsgBB->WriteUns ( updateDataSize + identifierSize, SIZE_OF_HANDLER_DATA_LEN );

							// write the handler identifier
							if (handler->GetIdentifier())
							{
								ASSERT_ONLY(u32 identifierStart = updateMsgBB->GetCursorPos();)
								handler->GetIdentifier()->Write(*updateMsgBB);
								Assert(updateMsgBB->GetCursorPos() - identifierStart == identifierSize);
							}

							// sanity check that the correct amount of header data was written
							gnetAssert(updateMsgBB->GetCursorPos() - startPos == (int)headerSize);

							LogArrayElementsMsgHeader(handler, NULL, true, *remotePlayer, updateSeq, bSplitUpdate, updateNum, updateInfo->m_NumUpdates);

#if __BANK
							// record the header data out
							m_BandwidthStats.AddBandwidthOut(m_UpdateMsgHeaderRecorderId, updateMsgBB->GetCursorPos() - startPos);

							// record the array data out
							netBandwidthStatsWriteCollector writeCollector(m_BandwidthStats, m_ArrayRecorderIds[handler->GetHandlerType()], *updateMsgBB);

#endif
							DEV_ONLY(unsigned dataStart = updateMsgBB->GetCursorPos());

							// write the handler data
							handler->WriteUpdate(*remotePlayer, *updateMsgBB, updateSeq, currElement);

							updateNum++;
#if __DEV
							// sanity check that the right amount of data was written
							unsigned dataWritten = updateMsgBB->GetCursorPos() - dataStart;

							if (dataWritten != updateDataSize)
							{
								LOGGING_ONLY(GetLog().Log("\t*** WRITTEN DATA (%d) DIFFERS FROM CALCULATED DATA SIZE (%d). INITIAL ELEMENT: %d\r\n", dataWritten, updateDataSize, initialElement));
								handler->GetSizeOfMessageUpdateData(*remotePlayer, maxUpdateDataSize, initialElement, true);
								updateMsgBB->SetCursorPos(dataStart);
								handler->WriteUpdate(*remotePlayer, *updateMsgBB, updateSeq, initialElement, true);
								NETWORK_QUITF(0, "Array %s written data (%d) differs from calculated data size (%d). Initial element: %d", handler->GetHandlerName(), dataWritten, updateDataSize, initialElement);
							}

							if (bSplitUpdate && !bCacheSplitUpdates)
							{
								if (gnetVerify(updateInfo->m_TotalUpdateSize >= dataWritten))
								{
									updateInfo->m_TotalUpdateSize -= dataWritten;
								}
							}
							else if (!bSplitUpdate)
							{
								gnetAssert(dataWritten == updateInfo->m_TotalUpdateSize);
								gnetAssert(updateMsgBB->GetCursorPos() - startPos == (int)(updateInfo->m_TotalUpdateSize + headerSize));

								// sanity check that the whole array was processed
								gnetAssert(currElement == handler->GetNumElementsInUse());
							}
#endif
						}
					}

					// send any remaining data in the array update message
					SendUpdateMessage(*remotePlayer, localPlayer);
				}
			}
		}
	}

	m_CurrentLocalUpdateBatch = (++m_CurrentLocalUpdateBatch) % NUM_LOCAL_NORMAL_UPDATE_BATCHES;

    // reset the message counts if the throttle interval has elapsed
    unsigned currentTime = sysTimer::GetSystemMsTime();

    if((currentTime - m_LastUpdateThrottleTime) > UPDATE_THROTTLE_INTERVAL)
    {
        for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
        {
            m_NumUpdatesSent[index] = 0;
        }

        m_LastUpdateThrottleTime = currentTime;

        // update the throttle amount
        static const unsigned LOW_BANDWIDTH_THRESHOLD = 256;
        unsigned targetUpstreamBandwidth = netInterface::GetObjectManager().GetBandwidthMgr().GetTargetUpstreamBandwidth();

        if(targetUpstreamBandwidth <= LOW_BANDWIDTH_THRESHOLD)
        {
            m_MaxMessagesPerThrottleInterval = LOW_BANDWIDTH_MAX_MESSAGES_PER_PLAYER;
        }
        else
        {
            m_MaxMessagesPerThrottleInterval = DEFAULT_MAX_MESSAGES_PER_PLAYER;
        }
    }
}

void netArrayManager::PlayerHasJoined(const netPlayer& player)
{
	NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_JOINED", "%s", player.GetLogName());

	// inform all the handlers
	for (u32 i=0; i<NUM_UPDATE_BATCH_TYPES; i++)
	{
		arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
		arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

		for(; curr != end; ++curr)
		{
			netArrayHandlerBase* handler = *curr;

			handler->PlayerHasJoined(player);
		}
	}

	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		m_LastSentSequences[player.GetPhysicalPlayerIndex()] = 0;
        m_NumUpdatesSent   [player.GetPhysicalPlayerIndex()] = 0;

		// we ignore updates from bots
		if (!player.IsBot())
		{
			m_LastReceivedSequences[player.GetPhysicalPlayerIndex()] = 0;
		}
	}

#if !__FINAL
	scrThread::CheckIfMonitoredScriptArraySizeHasChanged("netArrayManager::PlayerHasJoined", 0);
#endif

}

void netArrayManager::PlayerHasLeft(const netPlayer& player)
{
	NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_LEFT", "%s", player.GetLogName());

	// inform all the handlers
	for (u32 i=0; i<NUM_UPDATE_BATCH_TYPES; i++)
	{
		arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
		arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

		for(; curr != end; ++curr)
		{
			netArrayHandlerBase* handler = *curr;

			handler->PlayerHasLeft(player);
		}
	}

	// empty the update history queue for this player
	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX) && !player.IsBot())
	{
		updateMsgHistoryQueue& updateQueue = m_UpdateHistories[player.GetPhysicalPlayerIndex()];

		while(!updateQueue.empty())
		{
			updateMsgHistory *updateInfo = updateQueue.front();
			updateQueue.pop_front();
			delete updateInfo;
		}
	}

	// free up any cached updates for this player
	for (int i=0; i<m_CachedUpdates.GetMaxCount(); i++)
	{
		if (!m_CachedUpdates[i].IsFree() && m_CachedUpdates[i].m_Player == &player)
		{
			m_CachedUpdates[i].Free();
		}
	}
}

void netArrayManager::NewHost()
{
	// inform all the handlers
	for (u32 i=0; i<NUM_UPDATE_BATCH_TYPES; i++)
	{
		arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
		arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

		for(; curr != end;)
		{
			netArrayHandlerBase* handler = *curr++;

			handler->NewHost();
		}
	}
}

netArrayHandlerBase* netArrayManager::GetArrayHandler(const NetworkArrayHandlerType handlerType, const netArrayIdentifierBase* identifier)
{
	if (gnetVerifyf(handlerType < m_ArrayHandlers.GetCount(), "Handler type (%d) exceeds max type permitted (%d)", handlerType, m_ArrayHandlers.GetCount()))
	{
		arrayHandlerList::iterator curr = m_ArrayHandlers[handlerType].begin();
		arrayHandlerList::const_iterator end = m_ArrayHandlers[handlerType].end();

		for(; curr != end; ++curr)
		{
			netArrayHandlerBase* handler = *curr;

			if (handler->GetIdentifier())
			{
				if (identifier && *handler->GetIdentifier() == *identifier)
				{
					return handler;
				}
			}
			else if (!identifier)
			{
				return handler;
			}
		}
	}

	return NULL;
}


void netArrayManager::LogArrayElementsMsgHeader(netArrayHandlerBase* LOGGING_ONLY(handler), netArrayIdentifierBase* LOGGING_ONLY(identifier), 
												bool LOGGING_ONLY(sent), const netPlayer &LOGGING_ONLY(player), netSequence LOGGING_ONLY(seqNum), 
												bool LOGGING_ONLY(bSplitUpdate), unsigned LOGGING_ONLY(updateNum), unsigned LOGGING_ONLY(totalUpdates))
{
#if ENABLE_NETWORK_LOGGING
	NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), !sent, seqNum, player, sent ? "SENDING_ARRAY_ELEMENTS" : "RECEIVED_ARRAY_ELEMENTS", handler ? handler->GetHandlerName() : "UNKNOWN");

	if (handler && handler->GetIdentifier())
	{
		identifier = handler->GetIdentifier();
	}

	if (identifier)
	{
		GetLogSplitter().WriteDataValue("Identifier", identifier->GetLogName());
	}

	if (bSplitUpdate)
	{
		GetLogSplitter().WriteDataValue("Split update", "%d / %d", updateNum+1, totalUpdates);
	}
#endif
}

void netArrayManager::LogArrayElementsMsgRejection(const char *LOGGING_ONLY(messageText), ...)
{
#if ENABLE_NETWORK_LOGGING
	char buffer[netLog::MAX_LOG_STRING];
	va_list args;
	va_start(args,messageText);
	vsprintf(buffer,messageText,args);
	va_end(args);

	GetLogSplitter().WriteDataValue("IGNORED", buffer);
#endif
}

void netArrayManager::LogArrayElementsAck(const netPlayer& LOGGING_ONLY(player), bool LOGGING_ONLY(sent), netSequence LOGGING_ONLY(seqNum), netSequence LOGGING_ONLY(ackSeq), unsigned LOGGING_ONLY(handlersProcessed))
{
#if ENABLE_NETWORK_LOGGING
	NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), !sent, seqNum, player, sent ? "SENT_ARRAY_ELEMENTS_ACK" : "RECEIVED_ARRAY_ELEMENTS_ACK", "");
	GetLogSplitter().WriteDataValue("Ack for sync msg", "%d", ackSeq);
	GetLogSplitter().WriteDataValue("Handlers processed", "%d", handlersProcessed);
#endif
}

void netArrayManager::LogArrayElementsSplitUpdateAck(const netPlayer& LOGGING_ONLY(player), bool LOGGING_ONLY(sent), netArrayHandlerBase& LOGGING_ONLY(handler), netSequence LOGGING_ONLY(messageSeq), netSequence LOGGING_ONLY(updateSeq), unsigned LOGGING_ONLY(splitUpdateNum), int LOGGING_ONLY(unsyncedPlayers))
{
#if ENABLE_NETWORK_LOGGING
	NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, messageSeq, player, sent ? "SENT_ARRAY_ELEMENTS_SPLIT_UPDATE_ACK" : "RECEIVED_ARRAY_ELEMENTS_SPLIT_UPDATE_ACK", "");
	
	GetLogSplitter().WriteDataValue("Handler", handler.GetHandlerName());

	if (handler.GetIdentifier())
	{
		GetLogSplitter().WriteDataValue("Identifier", handler.GetIdentifier()->GetLogName());
	}
	
	GetLogSplitter().WriteDataValue("Sync msg seq", "%d", messageSeq);
	GetLogSplitter().WriteDataValue("Handler update seq", "%d", updateSeq);
	GetLogSplitter().WriteDataValue("Split update num", "%d", splitUpdateNum+1);

	if (handler.IsArrayLocallyArbitrated())
	{
		GetLogSplitter().WriteDataValue("Split update flags", "%d", (int)handler.GetUnackedSplitUpdateFlags(player));
		GetLogSplitter().WriteDataValue("Unsynced players", "%d", unsyncedPlayers);
	}
#endif
}

void netArrayManager::ArrayHandlerArbitrationChanged(netArrayHandlerBase* handler)
{
	// move the handler to the appropriate batch, only if it is currently in one
	if (handler->GetArrayManagerBatch() != INVALID_BATCH)
	{
		AddArrayHandlerToBatch(handler);
	}

	UpdateSizeOfLocallyArbitratedArrayData();
}

u32 netArrayManager::GetSizeOfLocallyArbitratedArrayData()
{
	u32 size = 0;

	for (u32 i=0; i<NUM_UPDATE_BATCH_TYPES; i++)
	{
		arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
		arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

		for(; curr != end; ++curr)
		{
			netArrayHandlerBase* handler = *curr;

			if (handler->IsArrayLocallyArbitrated() && !handler->IsSharedArray())
			{
				size += handler->GetNumElementsInUse() * handler->GetMaxElementSizeInBytes();
			}
		}
	}

	return size;
}

void netArrayManager::OnMessageReceived(const ReceivedMessageData &messageData)
{
    unsigned msgId = 0;
    if(gnetVerify(messageData.IsValid()) && netMessage::GetId(&msgId, messageData.m_MessageData, messageData.m_MessageDataSize))
    {
        if(msgId == msgUpdate::MSG_ID())
        {
            HandleArrayUpdateMsg(messageData);
        }
        else if (msgId == msgUpdateAck::MSG_ID())
        {
            HandleArrayUpdateACKMsg(messageData);
        }
		else if (msgId == msgSplitUpdateAck::MSG_ID())
		{
			HandleArraySplitUpdateACKMsg(messageData);
		}
    }
}

void netArrayManager::RegisterArrayHandler(netArrayHandlerBase* handler)
{
    gnetAssert(handler);

#if ENABLE_NETWORK_LOGGING
	NetworkLogUtils::WriteLogEvent(GetLog(), "REGISTER_ARRAY_HANDLER", "%s (Type: %d)", handler->GetHandlerName(), handler->GetHandlerType());

	if (handler->GetIdentifier())
	{
		GetLog().WriteDataValue("Identifier", handler->GetIdentifier()->GetLogName());
	}

	GetLog().WriteDataValue("Num elements", "%d", handler->GetNumArrayElements());
	GetLog().WriteDataValue("Num elements in use", "%u", handler->GetNumElementsInUse());

	if (handler->RequiresHighFrequencyUpdates())
	{
		GetLog().WriteDataValue("High frequency", "true");
	}

	GetLog().WriteDataValue("Address", "0x%p", handler->GetArrayData());
	GetLog().WriteDataValue("Element size (bits)", "%d", handler->GetMaxElementSizeInBits());
#endif // ENABLE_NETWORK_LOGGING

	gnetAssertf(!GetArrayHandler(handler->GetHandlerType(), handler->GetIdentifier()), "Trying to register an array handler which already exists");

	handler->Init();

	if (gnetVerify(handler->GetHandlerType() < m_ArrayHandlers.GetCount()))
	{
		m_ArrayHandlers[handler->GetHandlerType()].push_back(handler);
	}

	AddArrayHandlerToBatch(handler);

	// if an array has an identifier, make sure that it also has a static identifier defined
	gnetAssertf(!handler->GetIdentifier() || handler->GetStaticIdentifier(), "%s does not have a static identifier", handler->GetHandlerName());

	if (handler->GetStaticIdentifier() && gnetVerify(handler->GetHandlerType() < MAX_NUM_ARRAY_HANDLER_TYPES))
	{
		m_ArrayIdentifiers[handler->GetHandlerType()] = handler->GetStaticIdentifier();
	}

#if __BANK
	if (m_ArrayRecorderIds[handler->GetHandlerType()] == INVALID_RECORDER_ID)
	{
		m_ArrayRecorderIds[handler->GetHandlerType()] = m_BandwidthStats.AllocateBandwidthRecorder(handler->GetHandlerName());
	}
#endif

#if __ASSERT
	if (PARAM_netArraySizeAssert.Get())
	{
		unsigned maxDataSizePerMessage = msgUpdate::BUFFER_SIZE_IN_BYTES - BITS_TO_BYTES(GetCommonUpdateHeaderSize() - (handler->GetIdentifier() ? handler->GetIdentifier()->GetSize() : 0));
		unsigned maxHandlerDataSize = handler->GetMaxUpdateSizeInBytes();

		gnetAssertf(maxHandlerDataSize < maxDataSizePerMessage, "Array handler %s (%s) is too large (%d) - max size is (%d)", handler->GetHandlerName(), handler->GetIdentifier() ? handler->GetIdentifier()->GetLogName() : "", maxHandlerDataSize, maxDataSizePerMessage);
	}
#endif

	UpdateSizeOfLocallyArbitratedArrayData();
}

void netArrayManager::UnregisterArrayHandler(netArrayHandlerBase* handler, bool bDestroyHandler)
{
#if ENABLE_NETWORK_LOGGING
	NetworkLogUtils::WriteLogEvent(GetLog(), "UNREGISTER_ARRAY_HANDLER", handler->GetHandlerName());

	if (handler->GetIdentifier())
	{
		GetLog().WriteDataValue("Identifier", handler->GetIdentifier()->GetLogName());
	}
#endif // ENABLE_NETWORK_LOGGING

	// remove any update histories for this array 
	for (PhysicalPlayerIndex player=0; player<MAX_NUM_PHYSICAL_PLAYERS; player++)
	{
		updateMsgHistoryQueue::iterator curr = m_UpdateHistories[player].begin();
		updateMsgHistoryQueue::const_iterator end = m_UpdateHistories[player].end();

		for(; curr != end; ++curr)
		{
			updateMsgHistory *updateHistory = *curr;

			for (unsigned i=0; i<updateHistory->m_NumHandlers; i++)
			{
				if (updateHistory->m_Handlers[i] == handler)
				{
					updateHistory->m_Handlers[i] = NULL;
				}
			}
		}
	}

	// free up any cached updates for this handler
	for (int i=0; i<m_CachedUpdates.GetMaxCount(); i++)
	{
		if (!m_CachedUpdates[i].IsFree() && m_CachedUpdates[i].m_Handler == handler)
		{
			m_CachedUpdates[i].Free();
		}
	}

	if (AssertVerify(handler->GetArrayManagerBatch() != -1))
	{
		m_ArrayHandlerBatches[handler->GetArrayManagerBatch()].erase(handler);
		handler->SetArrayManagerBatch(INVALID_BATCH);
	}

	if (gnetVerify(handler->GetHandlerType() < m_ArrayHandlers.GetCount()))
	{
		m_ArrayHandlers[handler->GetHandlerType()].erase(handler);
	}

	if (bDestroyHandler)
	{
		handler->Shutdown();

		DestroyArrayHandler(handler);
	}

	UpdateSizeOfLocallyArbitratedArrayData();
}

void netArrayManager::StartNewUpdate(const netPlayer& player, netArrayHandlerBase* handler, unsigned updateSize, const netPlayer* fromPlayer, int firstElement, int lastElement)
{
	PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	msgUpdate* pMsg = &m_UpdateMsgs[playerIndex];

	updateMsgHistoryQueue& historyQueue = m_UpdateHistories[playerIndex];

	updateMsgHistory* currentHistory = historyQueue.empty() ? NULL : historyQueue.back();

	// if the handler update cannot fit in the existing update message, send it 
	if (!pMsg->m_bb.CanWriteBits(updateSize) || (currentHistory && currentHistory->m_NumHandlers == MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG))
	{
		SendUpdateMessage(player, fromPlayer);
	}

	if (!currentHistory || currentHistory->m_TimeSent)
	{
		// there should have been some data written to the existing message
		gnetAssert(pMsg->m_bb.GetNumBitsWritten() == 0);

		// create a new sequence number for this message
		pMsg->m_seqNum = ++m_LastSentSequences[playerIndex];

		NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), false, pMsg->m_seqNum, player, "SENDING_ARRAY_MGR_UPDATE", "");

		// add a new entry to the history queue
		while (historyQueue.size() >= NUM_ARRAY_UPDATES_INFO_HELD_FOR)
		{
			updateMsgHistory *info = historyQueue.front();

			//gnetWarning("Array update history queue is full, dumping an entry");
			GetLog().Log("\t\t## Array update history queue is full! Dumping an entry (seq: %d). ##\r\n", info->m_MessageSeqNum);

			historyQueue.pop_front();
			delete info;
		}

		updateMsgHistory* newHistory = rage_new updateMsgHistory(pMsg->m_seqNum);

		if (gnetVerify(newHistory))
		{
			historyQueue.push_back(newHistory);

			gnetAssert(historyQueue.back() == newHistory);
		}

		if (firstElement != -1)
		{
			newHistory->m_FirstElement = firstElement;
			newHistory->m_LastElement = lastElement;
		}

		currentHistory = newHistory;
	}
	else
	{
		// if the first and last elements are used, then there should be only one handler in this update history
		gnetAssert(currentHistory->m_FirstElement == -1);
		gnetAssert(currentHistory->m_LastElement == -1);
	}

	currentHistory->AddHandler(handler);
}

void netArrayManager::SendUpdateMessage(const netPlayer& toPlayer, const netPlayer* fromPlayer)
{
	Assert(&toPlayer != fromPlayer);

	PhysicalPlayerIndex playerIndex = toPlayer.GetPhysicalPlayerIndex();

	msgUpdate* pMsg = &m_UpdateMsgs[playerIndex];

    if(pMsg && (pMsg->m_bb.GetNumBitsWritten() > 0))
	{
		updateMsgHistoryQueue& historyQueue = m_UpdateHistories[playerIndex];

		updateMsgHistory* currentHistory = historyQueue.empty() ? NULL : historyQueue.back();

		if (currentHistory && !currentHistory->m_TimeSent)
		{
			netInterface::SendMsg(&toPlayer, *pMsg, 0, NULL, fromPlayer);

            BANK_ONLY(m_NumArrayManagerUpdateMessagesSent++);

			pMsg->Reset();

            currentHistory->m_TimeSent = netInterface::GetSynchronisationTime();

            m_NumUpdatesSent[playerIndex]++;

			GetLogSplitter().WriteDataValue("Time sent", "%u", currentHistory->m_TimeSent);
			GetLogSplitter().WriteDataValue("Num updates sent this throttle interval", "%u", m_NumUpdatesSent[playerIndex]);
		}
	}
}

void netArrayManager::CacheSplitHandlerUpdate(netArrayHandlerBase& handler, const netPlayer& fromPlayer, const netPlayer& toPlayer, datBitBuffer& bb, unsigned dataSize, 
											  netSequence messageSeq, netSequence updateSeq, unsigned splitUpdateNum, unsigned totalSplitUpdates)
{
	gnetAssert(handler.CacheSplitUpdates());

	bool		bUpdateApplied	= false;
	unsigned	numCached		= 0;
	int			freeSlot		= -1;
	bool		bAlreadyCached	= false;
	bool		bCachingFailed	= false;

	// process previously cached updates for this handler
	for (int i=0; i<m_CachedUpdates.GetMaxCount(); i++)
	{
		if (m_CachedUpdates[i].m_Handler == &handler)
		{
			if (m_CachedUpdates[i].m_Player != &fromPlayer)
			{
				// free up this cached entry if the player arbitration has changed
				m_CachedUpdates[i].Free();
			}
			else if (updateSeq != m_CachedUpdates[i].m_Seq)
			{
				// free up this cached entry if it is out of date
				GetLogSplitter().WriteDataValue("Old seq freed in slot", "%d", i);
				m_CachedUpdates[i].Free();
			}
			else if (m_CachedUpdates[i].m_UpdateNum == splitUpdateNum)
			{
				GetLogSplitter().WriteDataValue("Already cached in slot", "%d", i);
				bAlreadyCached = true;
				break;
			}
			else 
			{
				numCached++;
			}
		}

		if (freeSlot == -1 && m_CachedUpdates[i].IsFree())
		{
			freeSlot = i;
		}
	}
	
	if (!bAlreadyCached)
	{
		if (numCached >= totalSplitUpdates-1)
		{
			gnetAssert(numCached <= totalSplitUpdates);

			// this is the last update from the split group. We can now apply all of the cached updates simultaneously
			bUpdateApplied = true;

			// process previously cached updates for this handler
			for (int i=0; i<m_CachedUpdates.GetMaxCount() && bUpdateApplied; i++)
			{
				if (m_CachedUpdates[i].m_Handler == &handler)
				{
					if (!handler.ReadUpdate(fromPlayer, m_CachedUpdates[i].m_UpdateMsg.m_bb, m_CachedUpdates[i].m_UpdateSize, updateSeq))
					{
						bUpdateApplied = false;
					}
				}
			}

			// read the data in the current update
			if (bUpdateApplied && !bAlreadyCached && !handler.ReadUpdate(fromPlayer, bb, dataSize, updateSeq))
			{
				bUpdateApplied = false;
			}

			if (bUpdateApplied)
			{
				// We successfully processed the array data. Free up the cached entries.
				for (int i=0; i<m_CachedUpdates.GetMaxCount(); i++)
				{
					if (m_CachedUpdates[i].m_Handler == &handler)
					{
						m_CachedUpdates[i].Free();
					}
				}
			}
		}

		if (!bUpdateApplied)
		{
			if (freeSlot != -1)
			{
				GetLogSplitter().WriteDataValue("Cached in slot", "%d", freeSlot);

				// cache this update
				m_CachedUpdates[freeSlot].m_Handler = &handler;
				m_CachedUpdates[freeSlot].m_Player = &fromPlayer;
				m_CachedUpdates[freeSlot].m_Seq = updateSeq;
				m_CachedUpdates[freeSlot].m_UpdateNum = splitUpdateNum;
				m_CachedUpdates[freeSlot].m_UpdateSize = dataSize;

				// copy the handler update data to the cached message
				m_CachedUpdates[freeSlot].m_UpdateMsg.Reset();

				datBitBuffer::CopyBits(m_CachedUpdates[freeSlot].m_UpdateMsg.m_bb.GetReadWriteBits(), bb.GetReadOnlyBits(), dataSize, 0, bb.GetCursorPos());

				m_CachedUpdates[freeSlot].m_UpdateMsg.m_bb.SetNumBitsWritten(dataSize);
				m_CachedUpdates[freeSlot].m_UpdateMsg.m_bb.SetCursorPos(0);

				handler.SetLastReceivedUpdateSequence(fromPlayer, updateSeq);
			}
			else
			{
				GetLogSplitter().WriteDataValue("**Cannot cache**", "No free slots!");
				bCachingFailed = true;
			}
		}
	}

	if (!bCachingFailed)
	{
		msgSplitUpdateAck ackMsg( messageSeq, updateSeq, splitUpdateNum );

		datBitBuffer* ackMsgBB = &ackMsg.m_bb;

		ackMsgBB->WriteUns(handler.GetHandlerType(), m_BitsizeOfHandlerType);

		if (handler.GetIdentifier())
		{
			ackMsgBB->WriteBool(true);
			handler.GetIdentifier()->Write(*ackMsgBB);
		}
		else
		{
			ackMsgBB->WriteBool(false);
		}
	
		netSequence msgSeq;

		netInterface::SendMsg(&fromPlayer, ackMsg, 0, &msgSeq, &toPlayer );

		BANK_ONLY(m_NumArrayManagerAckMessagesSent++);

		LogArrayElementsSplitUpdateAck(fromPlayer, true, handler, ackMsg.m_messageSeqNum, ackMsg.m_handlerSeqNum, ackMsg.m_splitUpdateNum);

		// record the ack data out
		BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_UpdateAckMsgRecorderId, ackMsg.GetMessageDataBitSize()));
	}
}

int netArrayManager::SortHandlerByUpdateSize(const void *paramA, const void *paramB)
{
	const handlerUpdateInfo* handlerA = ((handlerUpdateInfo *)(paramA));
	const handlerUpdateInfo* handlerB = ((handlerUpdateInfo *)(paramB));

	if (handlerA->m_TotalUpdateSize < handlerB->m_TotalUpdateSize)
	{
		return -1;
	}
	else if(handlerA->m_TotalUpdateSize > handlerB->m_TotalUpdateSize)
	{
		return 1;
	}

	return 0;
}

void netArrayManager::AddArrayHandlerToBatch(netArrayHandlerBase* handler)
{
#if ENABLE_NETWORK_LOGGING
	u32 totalHandlerSize = handler->GetMaxElementSizeInBytes() * handler->GetNumElementsInUse();
#endif // ENABLE_NETWORK_LOGGING

	bool bLocalArray = false;
	bool bReorganiseBatches = false;

	u32 batchSizes[NUM_LOCAL_NORMAL_UPDATE_BATCHES];

	for (int i=0; i<NUM_LOCAL_NORMAL_UPDATE_BATCHES; i++)
	{
		batchSizes[i] = 0;
	}

	// move the handler from its existing batch, if there is one
	if (handler->GetArrayManagerBatch() != -1)
	{
		if (handler->GetArrayManagerBatch() != UBT_HIGH_FREQUENCY && handler->GetArrayManagerBatch() != UBT_REMOTE)
		{
			bReorganiseBatches = true;
		}

#if ENABLE_NETWORK_LOGGING
		if (PARAM_logArrayHandlerBatching.Get())
		{
			bLocalArray = handler->GetArrayManagerBatch() != UBT_REMOTE;

			if (bLocalArray)
			{
				NetworkLogUtils::WriteLogEvent(GetLog(), "REMOVE_ARRAY_FROM_LOCAL_BATCH", "%s (Type: %d)", handler->GetHandlerName(), handler->GetHandlerType());

				GetLog().WriteDataValue("Batch", "%d", handler->GetArrayManagerBatch());			
			}
			else
			{
				NetworkLogUtils::WriteLogEvent(GetLog(), "REMOVE_ARRAY_FROM_REMOTE_BATCH", "%s (Type: %d)", handler->GetHandlerName(), handler->GetHandlerType());
			}

			if (handler->GetIdentifier())
			{
				GetLog().WriteDataValue("Identifier", handler->GetIdentifier()->GetLogName());
			}

			if (bLocalArray)
			{
				GetLog().WriteDataValue("Handler size", "%d", totalHandlerSize);
			}
		}
#endif // ENABLE_NETWORK_LOGGING		

		m_ArrayHandlerBatches[handler->GetArrayManagerBatch()].erase(handler);
		handler->SetArrayManagerBatch(INVALID_BATCH);
	}

	bLocalArray = handler->IsArrayLocallyArbitrated();

#if ENABLE_NETWORK_LOGGING
	if (PARAM_logArrayHandlerBatching.Get())
	{
		if (bLocalArray)
		{
			NetworkLogUtils::WriteLogEvent(GetLog(), "ADD_ARRAY_TO_LOCAL_BATCH", "%s (Type: %d)", handler->GetHandlerName(), handler->GetHandlerType());
		}
		else
		{
			NetworkLogUtils::WriteLogEvent(GetLog(), "ADD_ARRAY_TO_REMOTE_BATCH", "%s (Type: %d)", handler->GetHandlerName(), handler->GetHandlerType());
		}

		if (handler->GetIdentifier())
		{
			GetLog().WriteDataValue("Identifier", handler->GetIdentifier()->GetLogName());
		}

		if (bLocalArray)
		{
			GetLog().WriteDataValue("Handler size", "%d", totalHandlerSize);
		}
	}
#endif // ENABLE_NETWORK_LOGGING		

	bool bAddToHighFreqBatch = handler->RequiresHighFrequencyUpdates();

	if (bLocalArray)
	{
		if (bAddToHighFreqBatch && m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].size() >= MAX_HIGH_FREQUENCY_ARRAYS)
		{
			gnetAssertf(0, "Only %d broadcast data arrays are allowed to update at a high frequency. This has been exceeded, dropping to normal frequency", MAX_HIGH_FREQUENCY_ARRAYS);
			bAddToHighFreqBatch = false;
		}

		if (bAddToHighFreqBatch)
		{
			// add to high frequency batch
			m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].push_back(handler);
			handler->SetArrayManagerBatch(UBT_HIGH_FREQUENCY);
#if ENABLE_NETWORK_LOGGING
			if (PARAM_logArrayHandlerBatching.Get())
			{
				GetLog().WriteDataValue("Batch", "High frequency");			
			}
#endif
		}
		else
		{
			// just add to local batch LOCAL_1 temporarily, the local batches are about to be sorted
			m_ArrayHandlerBatches[UBT_LOCAL_1].push_back(handler);
			handler->SetArrayManagerBatch(UBT_LOCAL_1);
			bReorganiseBatches = true;
		}
	}
	else
	{
		// add to remote batch 
		m_ArrayHandlerBatches[UBT_REMOTE].push_back(handler);
		handler->SetArrayManagerBatch(UBT_REMOTE);
#if ENABLE_NETWORK_LOGGING
		if (PARAM_logArrayHandlerBatching.Get())
		{
			GetLog().WriteDataValue("Remote batch size", "%d", m_ArrayHandlerBatches[UBT_REMOTE].size());
		}
#endif // ENABLE_NETWORK_LOGGING		
	}

	if (bReorganiseBatches)
	{
		// have to sort all the local array handlers by size so we can spread them evenly across all batches. This needs to be done whenever a new local
		// array is added to the batches
		static const unsigned MAX_NUM_ARRAY_HANDLERS_TO_SORT = 200;
		handlerUpdateInfo sortedHandlers[MAX_NUM_ARRAY_HANDLERS_TO_SORT];
		unsigned numSortedHandlers = 0;

		for (int i=0; i<NUM_LOCAL_NORMAL_UPDATE_BATCHES; i++)
		{
			arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
			arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

			for(; curr != end; ++curr)
			{
				netArrayHandlerBase* currHandler = *curr;

				Assert(numSortedHandlers < MAX_NUM_ARRAY_HANDLERS_TO_SORT);

				sortedHandlers[numSortedHandlers].m_Handler = currHandler;
				sortedHandlers[numSortedHandlers].m_TotalUpdateSize = currHandler->GetMaxUpdateSizeInBytes();
				numSortedHandlers++;

				currHandler->SetArrayManagerBatch(INVALID_BATCH);
			}

			m_ArrayHandlerBatches[i].clear();
		}

		// Sort the array handlers based on how much data they need to send to the player. This is done to maximise the number of handler 
		// updates that can be packed into one message.
		if (numSortedHandlers > NUM_LOCAL_NORMAL_UPDATE_BATCHES)
		{
			qsort(&sortedHandlers[0], numSortedHandlers, sizeof(handlerUpdateInfo), &SortHandlerByUpdateSize);
		}

		// add the sorted handlers starting at the largest first
		for (int i=numSortedHandlers-1; i>=0; i--)
		{
			netArrayHandlerBase* currHandler = sortedHandlers[i].m_Handler;

			if (currHandler)
			{
				int smallestBatch = 0;
				u32 smallestSize = batchSizes[0];

				// put the handler in the smallest batch
				for (int j=1; j<NUM_LOCAL_NORMAL_UPDATE_BATCHES; j++)
				{
					if (batchSizes[j] < smallestSize)
					{
						smallestBatch = j;
						smallestSize = batchSizes[j];
					}
				}

				m_ArrayHandlerBatches[smallestBatch].push_back(currHandler);
				currHandler->SetArrayManagerBatch(smallestBatch);

#if ENABLE_NETWORK_LOGGING
				if (PARAM_logArrayHandlerBatching.Get())
				{
					if (currHandler == handler)
					{
						GetLog().WriteDataValue("Batch", "%d", smallestBatch);			
					}
				}
#endif // ENABLE_NETWORK_LOGGING

				batchSizes[smallestBatch] += sortedHandlers[i].m_TotalUpdateSize;
			}
		}
	}

#if ENABLE_NETWORK_LOGGING
	if (PARAM_logArrayHandlerBatching.Get())
	{
		if (bReorganiseBatches || (bAddToHighFreqBatch && bLocalArray))
		{
			NetworkLogUtils::WriteLogEvent(GetLog(), "LOCAL_BATCHES_UPDATED", "");
			char batchName[30];

			for (int i=0; i<NUM_LOCAL_NORMAL_UPDATE_BATCHES; i++)
			{
				sprintf(batchName, "Batch %d", i); 
				GetLog().WriteDataValue(batchName, "%d handlers, total size: %d", m_ArrayHandlerBatches[i].size(), batchSizes[i]);
			}

			u32 highFreqBatchSize = 0;

			arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].begin();
			arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].end();

			for(; curr != end; ++curr)
			{
				netArrayHandlerBase* currHandler = *curr;
				highFreqBatchSize += currHandler->GetMaxUpdateSizeInBytes();
			}

			GetLog().WriteDataValue("High freq batch", "%d handlers, total size: %d", m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].size(), highFreqBatchSize);
		}
	}
#endif // ENABLE_NETWORK_LOGGING
}

void netArrayManager::DestroyArrayHandler(netArrayHandlerBase* handler)
{
    delete handler;
}

void netArrayManager::HandleArrayUpdateMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        msgUpdate updateMsg;

		const netPlayer& fromPlayer = *messageData.m_FromPlayer;
		const netPlayer& toPlayer = *messageData.m_ToPlayer;

        if(AssertVerify(updateMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
        {
            datBitBuffer& bb = updateMsg.m_bb;
		    netArrayIdentifierBase* identifier = NULL;
		    unsigned handlerType=0;
		    unsigned handlerNum = 0;
		    unsigned handlerDataSize;
		    unsigned handlersProcessed = 0;
		    unsigned splitUpdateNum = 0;
		    unsigned totalSplitUpdates = 1;

		    NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, updateMsg.m_seqNum, fromPlayer, "RECEIVED_ARRAY_MGR_UPDATE", "");

			if (messageData.m_FromPlayer->GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX)
		    {
			    LogArrayElementsMsgRejection("Player does not have a valid physical player index");
		    }
			else if (!toPlayer.IsBot() && !netSeqGt(updateMsg.m_seqNum, m_LastReceivedSequences[fromPlayer.GetPhysicalPlayerIndex()]))
		    {
			    LogArrayElementsMsgRejection("We have received a more recent update (%d)", m_LastReceivedSequences[fromPlayer.GetPhysicalPlayerIndex()]);
		    }
		    else
		    {
                m_LastReceivedSequences[fromPlayer.GetPhysicalPlayerIndex()] = updateMsg.m_seqNum;

			    while (bb.GetCursorPos() < bb.GetBitLength())
			    {
				    BANK_ONLY(int readPos = bb.GetCursorPos());
				    bool bSplitUpdate = false;
				    bool bIdentifier = false;
				    netArrayHandlerBase* handler = NULL;
				    netSequence updateSeq = 0;

				    // read the tag to make sure we are processing the arrays properly
				    GET_MSG_TAG(bb);

				    // read the handler id these elements belong to
				    bb.ReadUns(handlerType, m_BitsizeOfHandlerType);

				    // read the update sequence
				    bb.ReadUns(updateSeq, sizeof(netSequence)<<3);

				    // read a flag indicating whether this is a split update
				    bb.ReadBool(bSplitUpdate);

				    // read the split update information if necessary
				    if (bSplitUpdate)
				    {
					    bb.ReadUns(totalSplitUpdates, SIZEOF_UPDATE_NUM+1);
					    bb.ReadUns(splitUpdateNum, SIZEOF_UPDATE_NUM);
				    }

				    // read a flag indicating whether there is an identifier for this array
				    bb.ReadBool(bIdentifier);

				    // read the length of the handler data (including the identifier data)
				    bb.ReadUns( handlerDataSize, SIZE_OF_HANDLER_DATA_LEN );

				    int dataStartPos = bb.GetCursorPos();

				    // get the static identifier used to find the array
				    if (gnetVerify(handlerType < MAX_NUM_ARRAY_HANDLER_TYPES))
				    {
					    identifier = m_ArrayIdentifiers[handlerType];
				    }

				    if (bIdentifier && identifier)
				    {
					    identifier->Reset(&fromPlayer);
					    identifier->Read(bb);
				    }
				    else
				    {
					    gnetAssert(!identifier);
					    identifier = NULL;
				    }

				    if (!bIdentifier || identifier)
				    {
					    handler = GetArrayHandler(static_cast<NetworkArrayHandlerType>(handlerType), identifier);
				    }

				    LogArrayElementsMsgHeader(handler, identifier, false, fromPlayer, updateSeq, bSplitUpdate, splitUpdateNum, totalSplitUpdates);

				    // record the header data in
				    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_UpdateMsgHeaderRecorderId, bb.GetCursorPos() - readPos));

				    if (handler)
				    {
					    // record the array data in
					    BANK_ONLY(netBandwidthStatsReadCollector readCollector(m_BandwidthStats, m_ArrayRecorderIds[handler->GetHandlerType()], bb));

						if (toPlayer.IsBot())
						{
							// updates for bots are ignored, but acked as if they have been processed. We will have already received this update for our
							// main player and it will be rejected anyway because the sequence number of the update will have already been processed. 
							GetLogSplitter().WriteDataValue("IGNORED", "Update is for a bot");

							bb.SetNumBitsRead( dataStartPos + handlerDataSize);
						
							// flag the handler as processed
							gnetAssert(handlerNum < MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG);
							handlersProcessed |= (1<<handlerNum);
						}
					    else if (handler->CanProcessUpdate(fromPlayer, updateSeq, bSplitUpdate))
					    {
						    // the dataSize contains the identifier data as well, so we need to subtract this to calculate the size of the array data proper				
						    unsigned updateDataSize = handlerDataSize - (identifier ? identifier->GetSize() : 0);

						    bool bProcessed = false;

						    if (bSplitUpdate && handler->CacheSplitUpdates())
						    {
							    // if this is part of a split update we must cache the update until we have received all the parts of the update
							    CacheSplitHandlerUpdate(*handler, fromPlayer, toPlayer, bb, updateDataSize, updateMsg.m_seqNum, updateSeq, splitUpdateNum, totalSplitUpdates);
							}
						    else if (handler->ReadUpdate(fromPlayer, bb, updateDataSize, updateSeq))
						    {
							    bProcessed = true;
						    }
						    else
						    {
							    LogArrayElementsMsgRejection("The array handler cannot apply the update");
						    }

						    if (bProcessed)
						    {
							    // flag the handler as processed
							    gnetAssert(handlerNum < MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG);
							    handlersProcessed |= (1<<handlerNum);
						    }
					    }
					    else
					    {
						    netInterface::GetArrayManager().LogArrayElementsMsgRejection("The update could not be processed");
					    }
				    }
				    else
				    {
					    if (identifier)
						    LogArrayElementsMsgRejection("Array handler does not exist(Type %d, ID %s)", handlerType, identifier->GetLogName());
					    else
						    LogArrayElementsMsgRejection("Array handler does not exist(Type %d)", handlerType);
				    }

				    // if the handler data was not read, skip it
				    if ((handlersProcessed & (1<<handlerNum)) == 0)
				    {
					    bb.SetNumBitsRead( dataStartPos + handlerDataSize);
				    }
				    else
				    {
					    gnetAssert(bb.GetCursorPos() == dataStartPos + static_cast<int>(handlerDataSize));
				    }

				    handlerNum++;
			    }
     
			    if (handlersProcessed != 0)
			    {
				    // send an ack back
				    msgUpdateAck ackMsg( updateMsg.m_seqNum, handlersProcessed );
				    netSequence msgSeq;

				    netInterface::SendMsg(&fromPlayer, ackMsg, 0, &msgSeq, &toPlayer );

                    BANK_ONLY(m_NumArrayManagerAckMessagesSent++);

				    LogArrayElementsAck(fromPlayer, true, msgSeq, ackMsg.m_seqNum, handlersProcessed);

				    // record the ack data out
				    BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_UpdateAckMsgRecorderId, ackMsg.GetMessageDataBitSize()));
			    }
		    }
	    }
    }
}

void netArrayManager::HandleArrayUpdateACKMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        msgUpdateAck ackMsg;
	    netArrayHandlerBase* handler = NULL;

		const netPlayer& fromPlayer = *messageData.m_FromPlayer;

		bool bFoundHistoryEntry = false;

		if(gnetVerify(ackMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
        {
			LogArrayElementsAck(fromPlayer, false, messageData.m_NetSequence, ackMsg.m_seqNum, ackMsg.m_handlersProcessed);

		    // record the ack data in
		    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_UpdateAckMsgRecorderId, ackMsg.GetMessageDataBitSize()));

		    // find the corresponding update in the update history
		    if (gnetVerify(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
		    {
				updateMsgHistoryQueue& historyQueue = m_UpdateHistories[fromPlayer.GetPhysicalPlayerIndex()];

			    updateMsgHistoryQueue::iterator curr = historyQueue.begin();
			    updateMsgHistoryQueue::const_iterator end = historyQueue.end();

			    for(; curr != end; ++curr)
			    {
				    updateMsgHistory *updateHistory = *curr;

				    if (updateHistory->m_MessageSeqNum == ackMsg.m_seqNum)
				    {
						bFoundHistoryEntry = true;

					    if (gnetVerify(ackMsg.m_handlersProcessed != 0))
					    {
						    for (unsigned i=0; i<updateHistory->m_NumHandlers; i++)
						    {
							    if ((ackMsg.m_handlersProcessed & (1<<i)) != 0)
							    {
								    handler = updateHistory->m_Handlers[i];

								    if (handler)
								    {
										int firstElement = 0;
										int lastElement = 0;

										if (gnetVerify(handler->GetNumElementsInUse() > 0))
										{
											lastElement = (int)handler->GetNumElementsInUse()-1;
										}

										if (updateHistory->m_FirstElement != -1)
										{
											firstElement = updateHistory->m_FirstElement;
											lastElement = updateHistory->m_LastElement;
										}

										if (gnetVerify(firstElement<(int)handler->GetNumElementsInUse()) &&
											gnetVerify(lastElement<(int)handler->GetNumElementsInUse()))
										{
											handler->HandleAck(fromPlayer, updateHistory->m_TimeSent, (unsigned)firstElement, (unsigned)lastElement);
										}
								    }
							    }
						    }
					    }

					    historyQueue.erase(curr);
					    delete updateHistory;
					    break;
				    }
			    }
		    }
	    }

		if (!bFoundHistoryEntry)
		{
			GetLogSplitter().WriteDataValue("REJECTED", "** NO HISTORY ENTRY **");
		}
    }
}

void netArrayManager::HandleArraySplitUpdateACKMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        msgSplitUpdateAck ackMsg;
	    netArrayHandlerBase* handler = NULL;

		const netPlayer& fromPlayer = *messageData.m_FromPlayer;
		const netPlayer& toPlayer = *messageData.m_ToPlayer;

		if(gnetVerify(ackMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
        {
		    // record the ack data in
		    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_UpdateAckMsgRecorderId, ackMsg.GetMessageDataBitSize()));

			datBitBuffer& ackMsgBB = ackMsg.m_bb;
		    unsigned handlerType = 0;
		    netArrayIdentifierBase* identifier = NULL;
			bool bIdentifier = false;

			ackMsgBB.ReadUns(handlerType, m_BitsizeOfHandlerType);

			if (gnetVerify(handlerType < MAX_NUM_ARRAY_HANDLER_TYPES))
			{
				identifier = m_ArrayIdentifiers[handlerType];
			}

			ackMsgBB.ReadBool(bIdentifier);

			if (bIdentifier && identifier)
			{
				identifier->Reset(&toPlayer);
				identifier->Read(ackMsgBB);
		    }
			else
			{
				gnetAssert(!identifier);
				identifier = NULL;
			}

			if (!bIdentifier || identifier)
			{
				handler = GetArrayHandler(static_cast<NetworkArrayHandlerType>(handlerType), identifier);
			}
			
			if (handler)
			{
				unsigned timeMessageSent = 0;

				updateMsgHistoryQueue& historyQueue = m_UpdateHistories[fromPlayer.GetPhysicalPlayerIndex()];

				updateMsgHistoryQueue::iterator curr = historyQueue.begin();
				updateMsgHistoryQueue::const_iterator end = historyQueue.end();

				updateMsgHistory *updateHistory = NULL;

				for(; curr != end && timeMessageSent==0; ++curr)
				{
					updateHistory = *curr;

					if (updateHistory->m_MessageSeqNum == ackMsg.m_messageSeqNum)
					{
						for (unsigned i=0; i<updateHistory->m_NumHandlers && timeMessageSent==0; i++)
						{
							if (handler == updateHistory->m_Handlers[i])
							{
								timeMessageSent = updateHistory->m_TimeSent;
							}
						}

						Assert(timeMessageSent!=0);

						// a final split update may go out with other handler updates. In this case we can't remove the update history.
						if (updateHistory->m_NumHandlers == 1)
						{
							historyQueue.erase(curr);
							delete updateHistory;
							updateHistory = NULL;
						}

						break;
					}
				}
			
				if (timeMessageSent != 0)
				{
					LogArrayElementsSplitUpdateAck(fromPlayer, false, *handler, ackMsg.m_messageSeqNum, ackMsg.m_handlerSeqNum, ackMsg.m_splitUpdateNum, (int)handler->GetUnsyncedPlayers());
					
					handler->HandleSplitUpdateAck(fromPlayer, ackMsg.m_handlerSeqNum, ackMsg.m_splitUpdateNum, timeMessageSent);
				}
			}
		}
    }
}

void netArrayManager::UpdateSizeOfLocallyArbitratedArrayData()
{
	u32 sizeOfLocallyArbitratedData = netInterface::GetArrayManager().GetSizeOfLocallyArbitratedArrayData();
	netInterface::GetLocalPlayer()->SetSizeOfNetArrayData(sizeOfLocallyArbitratedData);
}

// ================================================================================================================
// netArrayManager_Script
// ================================================================================================================

netHostBroadcastDataHandlerBase* netArrayManager_Script::RegisterScriptHostBroadcastData(unsigned* pArray,
																						unsigned sizeOfData, 
																						const scriptHandler& scriptHandler,
																						unsigned dataId,
																						bool HasHighFrequencyUpdates
																						BANK_ONLY(, const char* debugArrayName))
{
	netHostBroadcastDataHandlerBase *pHostHandler = NULL;

	if (gnetVerifyf(scriptHandler.GetNetworkComponent(), "Trying to register host broadcast data for a non-networked script %s!", scriptHandler.GetLogName()))
	{
		if (gnetVerifyf(!GetScriptHostBroadcastDataArrayHandler(scriptHandler.GetScriptId(), dataId), "A host broadcast data array already exists for script %s", scriptHandler.GetLogName()))
		{
			pHostHandler = CreateHostBroadcastDataArrayHandler(pArray, sizeOfData, scriptHandler, dataId BANK_ONLY(, debugArrayName));

			if (pHostHandler)
			{
				pHostHandler->SetHasHighFrequencyUpdates(HasHighFrequencyUpdates);

				RegisterArrayHandler(pHostHandler);
			}
		}
	}

	return pHostHandler;
}

void netArrayManager_Script::RegisterScriptPlayerBroadcastData(unsigned* pArray,
															  unsigned sizeOfPlayerData,
															  const scriptHandler& scriptHandler,
															  unsigned dataId,
															  bool HasHighFrequencyUpdates
															  BANK_ONLY(, const char* debugArrayName))
{
	if (!gnetVerifyf(scriptHandler.GetNetworkComponent(), "Trying to register player broadcast data for a non-networked script %s!", scriptHandler.GetLogName()))
	{
		return;
	}

	unsigned maxNumParticipants = scriptHandler.GetNetworkComponent()->GetMaxNumParticipants();

	// the size of the array must be divisible by the number of participants
	Assert(sizeOfPlayerData%maxNumParticipants == 0);

	unsigned numArrayBytesPerClient = sizeOfPlayerData / maxNumParticipants;

	Assert(netInterface::GetLocalPlayer());

	u8* pByteArray = reinterpret_cast<u8*>(pArray);

	// register an array handler for each player. The client variable array is divided up amongst the players.
	for (unsigned i=0; i<maxNumParticipants; i++)
	{
		gnetAssertf(!GetScriptPlayerBroadcastDataArrayHandler(scriptHandler.GetScriptId(), dataId, i), "A player broadcast data local array handler for script %s already exists for slot %d", scriptHandler.GetLogName(), i);

		// if there is a participant using this slot, assign him to this handler
		const netPlayer* player = scriptHandler.GetNetworkComponent()->GetParticipantUsingSlot(i);

		if (player)
		{
			gnetAssertf(!GetScriptPlayerBroadcastDataArrayHandler(scriptHandler.GetScriptId(), dataId, *player), "A player broadcast data local array handler for script %s already exists for player %s in slot %d", scriptHandler.GetLogName(), player->GetLogName(), i);
			scriptInterface::GetScriptManager().WriteScriptHeader(scriptInterface::GetScriptManager().GetLog(), scriptHandler.GetScriptId(), "Allocated a client variable array handler for player %s in slot %d", player->GetLogName(), i);
		}

		netPlayerBroadcastDataHandlerBase *pPlayerHandler = CreatePlayerBroadcastDataArrayHandler(reinterpret_cast<unsigned*>(pByteArray), numArrayBytesPerClient, scriptHandler, dataId, i BANK_ONLY(, debugArrayName));

		if (pPlayerHandler)
		{
			if (pPlayerHandler->IsLocal())
			{
				pPlayerHandler->SetHasHighFrequencyUpdates(HasHighFrequencyUpdates);
			}

			RegisterArrayHandler(pPlayerHandler);
		}

		pByteArray += numArrayBytesPerClient;
	}
}

void netArrayManager_Script::UnregisterAllScriptBroadcastData(const scriptHandler& scriptHandler)
{
	for (int i=0; i<NUM_UPDATE_BATCH_TYPES; i++)
	{
		arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
		arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

		for(; curr != end; ++curr)
		{
			netArrayHandlerBase* handler = *curr;

			if (handler->GetHandlerType() == HOST_BROADCAST_DATA_ARRAY_HANDLER ||
				handler->GetHandlerType() == PLAYER_BROADCAST_DATA_ARRAY_HANDLER)
			{
				if (static_cast<netScriptBroadcastDataHandlerBase*>(handler)->GetScriptHandler() == &scriptHandler)
				{
					Assert(handler->GetArrayManagerBatch() == i);
					curr--;
					UnregisterArrayHandler(handler);
				}
			}
		}
	}
}

void netArrayManager_Script::SwapLocalAndRemotePlayerBroadcastDataHandlers(netPlayerBroadcastDataHandlerBase& playerHandler)
{
	unsigned* pArray = playerHandler.GetArray();
	unsigned sizeOfArrayInBytes = playerHandler.GetSizeOfArrayDataInBytes();

	netBroadcastDataArrayIdentifierBase* pIdentifier = static_cast<netBroadcastDataArrayIdentifierBase*>(playerHandler.GetIdentifier());

	int slot = pIdentifier->GetInstanceId();
	int dataId = pIdentifier->GetDataId();

	netPlayerBroadcastDataHandlerBase* newPlayerHandler = CreatePlayerBroadcastDataArrayHandler(pArray, sizeOfArrayInBytes, *playerHandler.GetScriptHandler(), dataId, slot BANK_ONLY(, pIdentifier->GetDataDebugArrayName() ) );

	UnregisterArrayHandler(&playerHandler);

	if (gnetVerify(newPlayerHandler))
	{
		RegisterArrayHandler(newPlayerHandler);
	}
}

bool netArrayManager_Script::IsAllHostBroadcastDataSynced(const scriptIdBase& scriptId)
{
	for (int i=0; i<NUM_LOCAL_UPDATE_BATCHES; i++)
	{
		arrayHandlerBatchList::iterator curr = m_ArrayHandlerBatches[i].begin();
		arrayHandlerBatchList::const_iterator end = m_ArrayHandlerBatches[i].end();

		for(; curr != end; ++curr)
		{
			netArrayHandlerBase* handler = *curr;

			if (handler->GetHandlerType() == HOST_BROADCAST_DATA_ARRAY_HANDLER)
			{
				if (handler->IsArrayLocallyArbitrated() &&
					static_cast<netBroadcastDataArrayIdentifierBase*>(handler->GetIdentifier())->GetScriptId() == scriptId)
				{
					// force an update so any dirty elements are recalculated
					handler->Update();

					if (!handler->IsSyncedWithAllPlayers())
					{
						return false;
					}
				}
			}
		}
	}

	return true;
}

unsigned netArrayManager_Script::GetNumArrayHandlerTypes() const 
{ 
	return NUM_BASE_ARRAY_HANDLER_TYPES; 
}

} // namespace rage
