//
// netobjectmessages.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORKOBJECTMESSAGES_H
#define NETWORKOBJECTMESSAGES_H

// rage headers
#include "net/message.h"
#include "rline/rlgamerinfo.h"

// framework headers
#include "fwnet/netlog.h"
#include "fwnet/netserialisers.h"

namespace rage
{
class netPlayer;

//PURPOSE
// Acknowledgement codes passed in response to object update messages
enum NetObjectAckCode
{
    ACKCODE_SUCCESS,            // Success
    ACKCODE_FAIL,               // Fail (unspecified reason)
    ACKCODE_WRONG_OWNER,        // The object was not owned by the correct player to perform the request)
    ACKCODE_OUT_OF_SEQUENCE,    // The message was ignored as it was received out of sequence)
    ACKCODE_NO_OBJECT,          // The object specified by the request was not available on the target machine
	ACKCODE_TOO_MANY_OBJECTS,   // Too many objects are on the target machine already
	ACKCODE_CANT_APPLY_DATA,    // The sync data couldn't be applied
    ACKCODE_MATCH_NOT_STARTED,  // The match has not been started yet
    ACKCODE_NONE
};

// The number of bits required to sync a NetObjectAckCode
#define SIZEOF_ACKCODE 4

static const unsigned MAX_SYNC_ACKS = 128;

// PURPOSE
// This is a structure for storing packed message data. This is done as a bandwidth
// optimisation to reduce message header information (saving both RAGENet and UDP header data).
// The structure allows multiple game messages to be packed in a single netMessage
struct packedMessageType
{
    packedMessageType() :
    m_MessageCount(0)
    {
        m_MessageBuffer.SetReadWriteBits(m_MessageStorage, MAX_MESSAGE_PAYLOAD_BITS, 0);
    }

    //PURPOSE
    // Resets the packed message data
    void Reset()
    {
        m_MessageCount = 0;
        m_MessageBuffer.SetCursorPos(0);
    }

    //PURPOSE
    // Reads packed message data from an import data buffer
    //PARAMS
    // bb - The import data buffer
    void Serialise(datImportBuffer &bb)
    {
        unsigned bufferSize = 0;

        bb.SerUns(m_MessageCount, SIZEOF_MESSAGE_COUNT);

        if (m_MessageCount > 0)
        {
            bb.SerUns(bufferSize, SIZEOF_BUFFER_SIZE);
            u8 *buffer = static_cast<u8 *>(m_MessageBuffer.GetReadWriteBits());
            bb.SerBits(buffer, bufferSize);
            m_MessageBuffer.SetReadOnlyBits(buffer, bufferSize, 0);
        }
     }

    //PURPOSE
    // Reads packed message data to an export data buffer
    //PARAMS
    // bb - The export data buffer
    void Serialise(datExportBuffer &bb) const
    {
        const u8 *buffer     = static_cast<const u8 *>(m_MessageBuffer.GetReadWriteBits());
        const unsigned bufferSize = m_MessageBuffer.GetCursorPos();

        bb.SerUns(m_MessageCount, SIZEOF_MESSAGE_COUNT);

        if (m_MessageCount > 0)
        {
           bb.SerUns(bufferSize, SIZEOF_BUFFER_SIZE);
           bb.SerBits(buffer, bufferSize);
        }
    }

    //PURPOSE
    // Returns the size of data written to the packed message buffer
    unsigned GetSize() const
    {
        unsigned size = 0;

        // message won't be sent if message count is 0
        if (m_MessageCount > 0)
        {
            size += m_MessageBuffer.GetCursorPos();
        }

        return size;
    }

    static const unsigned int SIZEOF_MESSAGE_COUNT  = 5;  // The number of bits required to sync the message count
    static const unsigned int SIZEOF_BUFFER_SIZE    = 13; // The number of bits required to sync the message buffer size

    unsigned     m_MessageCount;  // The number of game messages packed
    datBitBuffer m_MessageBuffer; // The buffer for the packed message data
    u8           m_MessageStorage[MAX_MESSAGE_PAYLOAD_BYTES]; // Storage for the packed message data
};

//PURPOSE
// a message containing one or more object updates (sync messages)
class cloneSyncMsg
{
public:

    NET_MESSAGE_DECL(cloneSyncMsg, CLONE_SYNC_MSG);

    cloneSyncMsg() {this->Reset();}

    //PURPOSE
    // Resets the message contents for reuse
    void Reset()
    {
        m_OwnerPlayer         = 0;
        m_CurrentTimestamp    = 0;
        m_Sequence            = 0;
        m_NumObjects          = 0;
        m_NumObjectsCursorPos = 0;
        m_BitBuffer.SetReadWriteBytes(m_Buffer, sizeof(m_Buffer));
    }

    //PURPOSE
    // Updates the count of objects within the sync message written to the bit buffer.
    // This is initially written as 0 as the number of objects is not known until later
    void UpdateNumObjectsCount()
    {
        if(m_NumObjects > 0)
        {
            int oldCursorPos = m_BitBuffer.GetCursorPos();
            m_BitBuffer.SetCursorPos(m_NumObjectsCursorPos);
            m_BitBuffer.WriteUns(m_NumObjects, SIZEOF_NUM_OBJECTS);
            m_BitBuffer.SetCursorPos(oldCursorPos);
            m_NumObjects          = 0;
        }

        m_NumObjectsCursorPos = m_BitBuffer.GetCursorPos();
    }

    //PURPOSE
    // Writes the contents of the specified message to an export buffer
    //PARAMS
    // bb  - export buffer
    // msg - message
    NET_MESSAGE_EXPORT(bb, msg)
    {
        const unsigned numBits = msg.m_BitBuffer.GetBitLength();
        const int cursorPos = msg.m_BitBuffer.GetCursorPos();
        msg.m_BitBuffer.SetCursorPos(0);
        return
            AssertVerify( bb.WriteUns(msg.m_Sequence, SIZEOF_SEQUENCE)) &&
            AssertVerify( bb.WriteUns(numBits, SIZEOF_BIT_COUNT))
            && (!numBits || AssertVerify(bb.WriteBits(msg.m_BitBuffer, numBits)))
            && (msg.m_BitBuffer.SetCursorPos(cursorPos), true);
    }

    //PURPOSE
    // Reads the contents of the specified import buffer to a message
    //PARAMS
    // bb  - export buffer
    // msg - message
    NET_MESSAGE_IMPORT(bb, msg)
    {
        msg.Reset();

        unsigned numBits = 0;

        return
            AssertVerify( bb.ReadUns(msg.m_Sequence, SIZEOF_SEQUENCE)) &&
            AssertVerify(bb.ReadUns(numBits, SIZEOF_BIT_COUNT))
            && (!numBits || AssertVerify(bb.ReadBits(msg.m_BitBuffer, numBits)))
            && (msg.m_BitBuffer.SetCursorPos(0), true);
    }

    //PURPOSE
    // Returns the size of the message data stored in this message
    int GetMessageDataBitSize() const
    {
        return m_BitBuffer.GetCursorPos() + SIZEOF_SEQUENCE + SIZEOF_BIT_COUNT;
    }

    //PURPOSE
    // Returns the size of the header data for an individual sync message (there will be
    // one of these for each object update contained within the message)
    static int GetSyncHeaderBitSize()
    {
        return SIZEOF_OBJECTTYPE + SIZEOF_OBJECTID + TAG_SIZE;
    }

    //PURPOSE
    // Returns the size of the message header data stored in this message (there will be one
    // of these for this entire message)
    static int GetMessageHeaderBitSize()
    {
        return netMessage::GetHeaderSize(MSG_ID()) + SIZEOF_SEQUENCE + SIZEOF_BIT_COUNT;
    }

	//PURPOSE
    // Returns the maximum size sync data that can fit in an individual message, taking required header bits
    // into account
    static int GetMaxSyncDataSize()
    {
        return BUFFER_BITSIZE - (SIZEOF_NBAL_HEADER_IN_BYTES << 3) - GetMessageHeaderBitSize() - SIZEOF_TIMESTAMP - SIZEOF_NUM_OBJECTS - cloneSyncMsg::GetSyncHeaderBitSize();
    }

    // Constants for message data sizes
    static const unsigned SIZEOF_SEQUENCE       = (sizeof(netSequence)<<3);
    static const unsigned SIZEOF_TIMESTAMP      = 32;
    static const unsigned SIZEOF_NUM_OBJECTS    = 3;
    static const unsigned MAX_NUM_OBJECTS       = (1<<SIZEOF_NUM_OBJECTS) - 1;
    static const unsigned MAX_BITSIZEOF_PAYLOAD = MAX_MESSAGE_PAYLOAD_BITS;
    static const unsigned SIZEOF_BIT_COUNT      = datBitsNeeded<(MAX_BITSIZEOF_PAYLOAD-SIZEOF_SEQUENCE)>::COUNT;
    static const unsigned BUFFER_BITSIZE        = MAX_BITSIZEOF_PAYLOAD - SIZEOF_SEQUENCE - SIZEOF_BIT_COUNT;
    static const unsigned BUFFER_BYTESIZE       = (BUFFER_BITSIZE%8) ? ((BUFFER_BITSIZE>>3)+1) : (BUFFER_BITSIZE>>3);

    const netPlayer *m_OwnerPlayer;
    u32              m_CurrentTimestamp;        // timestamp of current set of objects being packed
    netSequence      m_Sequence;                // object manager sequence number of message
    u8               m_NumObjects;              // number of objects packed for the current timestamp
    int              m_NumObjectsCursorPos;     // cursor position into the bit buffer storing the number of objects packed for the current timestamp
    datBitBuffer     m_BitBuffer;               // bit buffer for storing the contents of the packed sync messages
    u8               m_Buffer[BUFFER_BYTESIZE]; // storage for the bit buffer
};

//PURPOSE
// A class for representing a network message containing packed sync ACK messages.
// Each object update received within a cloneSyncMsg message will be acknowledged
// by the receiver. Multiple object acknowledgements (ACK codes)can be sent via an
// individual instance of this class in order to avoid sending unnecessary message header data
class packedCloneSyncACKsMsg
{
public:

    NET_MESSAGE_DECL(packedCloneSyncACKsMsg, PACKED_CLONE_SYNC_ACKS_MSG);

    packedCloneSyncACKsMsg();

    //PURPOSE
    // Writes the contents of the specified message to an export buffer
    //PARAMS
    // bb  - export buffer
    // msg - message
    NET_MESSAGE_EXPORT(bb, msg)
    {
        gnetAssertf(msg.m_CurrentSyncAckSequence == INVALID_SEQUENCE_ID, "Exporting packed sync ACK with pending ACK data!");

        unsigned dataSize = static_cast<unsigned>(msg.GetMessageDataBitSize());

        gnetVerifyf(bb.WriteUns(dataSize, SIZEOF_BUFFER_SIZE), "Failed writing packed sync ack buffer size to message!");

	    // set the cursos pos to 0 because WriteBits will write from the current pos
	    msg.m_BitBuffer.SetCursorPos(0);

	    if (!bb.WriteBits(msg.m_BitBuffer, dataSize))
	    {
		    gnetAssertf(0, "Failed to write sync ack data to a packedCloneSyncACKsMsg");
	    }

        return true;
    }

    //PURPOSE
    // Reads the contents of the specified import buffer to a message
    //PARAMS
    // bb  - export buffer
    // msg - message
    NET_MESSAGE_IMPORT(bb, msg)
    {
        unsigned dataSize = 0;
        gnetVerifyf(bb.ReadUns(dataSize, SIZEOF_BUFFER_SIZE), "Failed reading packed sync ack buffer size from message!");

	    // set the cursos pos to 0 because ReadBits will read from the current pos
	    msg.m_BitBuffer.SetCursorPos(0);

	    if (!bb.ReadBits(msg.m_BitBuffer, dataSize))
	    {
		    Assertf(0, "Failed to read sync ack data from a packedCloneSyncACKsMsg");
	    }

        return true;
    }

    //PURPOSE
    // Adds an acknowledgement for an object update to the packed message data
    //PARAMS
    // fromPlayer      - the player sending the sync ACK
    // syncSequence    - the message sequence being ACKed
    // numObjects      - the number of objects being ACKed
    // numFailedACKs   - the number of failed ACKs
    // objectIDs       - the IDs of the network objects being ACKed
    // ackCodes        - the ACK codes
    bool AddSyncAcks(const netPlayer &fromPlayer,
                     netSequence      syncSequence,
                     unsigned         numObjects,
                     unsigned         numFailedACKs,
                     ObjectId         objectIDs[MAX_SYNC_ACKS],
                     NetObjectAckCode ackCodes[MAX_SYNC_ACKS]);

    //PURPOSE
    // Returns the number of bits required to sync an ACK to an individual object update
    static unsigned GetSyncAckSize();

    //PURPOSE
    // Returns the number of bits required to sync the message header data
    unsigned GetMessageHeaderBitSize() const;

    //PURPOSE
    // Returns the number of bits required to sync the message body data
    unsigned GetMessageDataBitSize() const;

    //PURPOSE
    // Returns the total number of bits required to sync the contents of this message over the network
    unsigned GetMessageTotalBitSize() const { return GetMessageHeaderBitSize() + GetMessageDataBitSize(); }

    //PURPOSE
    // Returns whether this message contains any packed data to send
    bool HasDataToSend() const;

    //PURPOSE
    // Resets the contents of the message for reuse
    void Reset();

    //PURPOSE
    // Writes any pending sync ack data to the bit buffer for this message. Called when
    // a sync ack for a new sequence is added or before sending the message
    //PARAMS
    // newSequence - The sequence of the next sync ACk
    void WritePendingSyncAcksToBitBuffer(netSequence newSequence);

private:

    //PURPOSE
    // Returns whether there is space available within this messages packed data to store
    // another ack
    bool IsSpaceAvailableForAck() const;

    //PURPOSE
    // Returns whether there is space available within this messages packed data to store
    // the specified number of ACKs
    //PARAMS
    // numFailedACKs - The number of failed ACKs
    bool IsSpaceAvailableForAcks(unsigned numFailedACKs) const;

public:

    // Constants for message data sizes
    static const unsigned SIZEOF_SYNC_ACK_SEQUENCE = (sizeof(netSequence)<<3);
    static const unsigned MAX_NUM_SYNC_ACKS        = 8;
    static const unsigned SIZEOF_NUM_SYNC_ACKS     = datBitsNeeded<MAX_NUM_SYNC_ACKS>::COUNT;
    static const unsigned BUFFER_BITSIZE           = 4096;
    static const unsigned BUFFER_BYTESIZE          = (BUFFER_BITSIZE%8) ? ((BUFFER_BITSIZE>>3)+1) : (BUFFER_BITSIZE>>3);
    static const unsigned SIZEOF_BUFFER_SIZE       = datBitsNeeded<BUFFER_BITSIZE>::COUNT;

    //PURPOSE
    // Stores information about an Ack code for specific object
    struct AckData
    {
        ObjectId m_ObjectID; // ID of object being acked
        u8       m_AckCode;  // Ack code
    };

    // maximum number of different sequences per message
    static const unsigned MAX_NUM_SYNC_SEQUENCES = 20;

    // number of Acks to store per sequence. If there are more Acks than this the same sequence
    // will be repeated in the message
    static const unsigned MAX_STORED_ACKS = 4;

    const netPlayer  *m_OwnerPlayer;
    netSequence       m_CurrentSyncAckSequence;      // Clone sync ACK sequence currently being used
    u16               m_NumSequencesAcked;           // The number of different sequences ACKed in this message
    u16               m_NumSyncAcks;                 // The number of sync acks packed in this message for the current sync ACK sequence
    AckData           m_StoredAcks[MAX_STORED_ACKS]; // Caches ack information - this will be written to the bit buffer when the sequence changes
    bool              m_HasDataToSend;               // Indicates this message has data ready to send
    datBitBuffer      m_BitBuffer;                   // Bit buffer for storing the packed sync ack data
    u8                m_Buffer[BUFFER_BYTESIZE];     // Storage for the bit buffer

#if ENABLE_NETWORK_LOGGING
    // keeps track of the sequences ACKed and number of failed ACKs for logging purposes
    netSequence       m_SequenceAcked[MAX_NUM_SYNC_SEQUENCES];
    u16               m_NumFailedAcks[MAX_NUM_SYNC_SEQUENCES];
#endif // ENABLE_NETWORK_LOGGING
};

//PURPOSE
// A class for representing a network message containing packed game messages that are
// sent reliably over the network. Reliable messages are particularly expensive in terms
// of header data, and can cause other bandwidth bottlenecks as they are guaranteed order.
// Packing the reliable game messages into a single message limits these network costs.
// The reliable game messages packed into these network messages are as follows:
//
// Create messages     - Object creation messages containing data to create a clone of a locally
//                       owned object to be created on a remote machine
// Create ACK messages - ACK messages for object creation messages received from a remote machine
// Remove messages     - Object removal messages containing data to remove a clone of a locally
//                       owned object on a remote machine
// Remove ACK messages - ACK messages for object removal messages received from a remote machine
class packedReliablesMsg
{
public:

    NET_MESSAGE_DECL(packedReliablesMsg, PACKED_RELIABLES_MSG);

    packedReliablesMsg();

    //PURPOSE
    // Serialisation function for the message data in the specified message. The manages both
    // the reading and writing of the packed message data depending on the serialisation mode.
    //PARAMS
    // bb  - import/export buffer (depends on the serialisation mode)
    // msg - message to read/write the data from/to
    NET_MESSAGE_SER(bb, msg)
    {
        u32 bitFlags = msg.GetBitFlags();
        bb.SerUns(bitFlags, SIZEOF_BITFLAGS);

        if(bitFlags & (1<<CREATE_MSG))
        {
            // we only need to serialise the timestamp if the message contains create messages
            bb.SerUns(msg.m_Timestamp, SIZEOF_TIMESTAMP);

            msg.m_Creates.Serialise(bb);
        }

        if(bitFlags & (1<<CREATE_ACK_MSG))
        {
            msg.m_CreateAcks.Serialise(bb);
        }

        if(bitFlags & (1<<REMOVE_MSG))
        {
            msg.m_Removes.Serialise(bb);
        }

        if(bitFlags & (1<<REMOVE_ACK_MSG))
        {
            msg.m_RemoveAcks.Serialise(bb);
        }

        return true;
    }

    //PURPOSE
    // Adds the data for an object creation message to the packed message data
    //PARAMS
    // fromPlayer       - The player this create message is from
    // objectType       - The network object type of the object to create
    // objectID         - The ID of the object to create (see netobjectidmgr.h/.cpp for more details)
    // flags            - Creation Flags
    // createData       - Game specific creation data
    // createDataSize   - Size of the game specific data
    bool AddCreate(const netPlayer  &fromPlayer, 
                   NetworkObjectType objectType,
                   const ObjectId    objectID,
                   NetObjFlags       flags,
                   u8               *createData,
                   unsigned          createDataSize);

    //PURPOSE
    // Adds the data for an object creation ACK message to the packed message data
    //PARAMS
    // fromPlayer       - The player this create message is ACKed from
    // objectID         - The ID of the object for which creation is being acknowledged
    // ackCode          - The ACK code for the creation request
    bool AddCreateAck(const netPlayer &fromPlayer, const ObjectId objectID, NetObjectAckCode ackCode);

    //PURPOSE
    // Adds the data for an object removal message to the packed message data
    //PARAMS
    // fromPlayer       - The player this remove message is from
    // objectID         - The ID of the object to remove
    // ownershipToken   - The ownership token for the object to remove, this is required
    //                    in order for the receiver to check whether it is fully up-to-date
    //                    with all ownership changes of the object
    bool AddRemove(const netPlayer &fromPlayer, const ObjectId objectID, u32 ownershipToken);

    //PURPOSE
    // Adds the data for an object removal ACK message to the packed message data
    //PARAMS
    // fromPlayer       - The player this remove message is ACKed from
    // objectID         - The ID of the object for which the removal is being acknowledged
    // ackCode          - The ACK code for the removal request
    bool AddRemoveAck(const netPlayer &fromPlayer, const ObjectId objectID, NetObjectAckCode ackCode);

    //PURPOSE
    // Returns the total number of bits required to pack a creation message
    //PARAMS
    // createDataSizeInBits - The size of the game specific creation data
    static unsigned GetCreateSize(const u32 createDataSizeInBits);

    //PURPOSE
    // Returns the total number of bits required to pack a creation ACK message
    static unsigned GetCreateAckSize();

    //PURPOSE
    // Returns the total number of bits required to pack a removal message
    static unsigned GetRemoveSize();

    //PURPOSE
    // Returns the total number of bits required to pack a remove ACK message
    static unsigned GetRemoveAckSize();

    //PURPOSE
    // Returns the total number of bits required for the message header data.
    // This is used to determine how much data can be stored in the message body data.
    unsigned GetMessageHeaderBitSize() const;

    //PURPOSE
    // Returns the total number of bits required to send the current packed message data
    // excluding the message headers covered by the GetMessageHeaderBitSize()
    unsigned GetMessageDataBitSize() const;

    //PURPOSE
    // Returns the total number of bits required to send the entire message contents (header and body)
    unsigned GetMessageTotalBitSize() const{ return GetMessageHeaderBitSize() + GetMessageDataBitSize(); }

    //PURPOSE
    // Returns whether this message contains any packed message data
    bool HasDataToSend() const;

    //PURPOSE
    // Resets the contents of this message for reuse
    void Reset();

private:

    //PURPOSE
    // Enum for the different message types that can be packed
    enum
    {
        CREATE_MSG,
        CREATE_ACK_MSG,
        REMOVE_MSG,
        REMOVE_ACK_MSG,
        NUM_MSG_TYPES
    };

    //PURPOSE
    // Returns the bit flags which indicate the which game message types are
    // packed in this message
    u32 GetBitFlags() const;

    //PURPOSE
    // Returns whether there is enough space to packed the required number of bits
    //PARAMS
    // numRequiredBits - The number of bits to pack
    bool IsSpaceAvailable(unsigned numRequiredBits) const;

public:

    // Constants for message data sizes
    static const unsigned SIZEOF_BITFLAGS       = 4;
    static const unsigned SIZEOF_TIMESTAMP      = 32;
    static const unsigned MAX_MESSAGES_PER_TYPE = (1<<packedMessageType::SIZEOF_MESSAGE_COUNT);

    const netPlayer  *m_OwnerPlayer;
    u32               m_Timestamp;   // timestamp of creation data (only valid if message contains creates)
    packedMessageType m_Creates;     // creates
    packedMessageType m_CreateAcks;  // create acks
    packedMessageType m_Removes;     // remove
    packedMessageType m_RemoveAcks;  // remove acks
};

//PURPOSE
// Response codes used when replying to object reassignment messages
enum ReassignMsgResponseCode
{
    RESPONSE_NOT_ON_THIS_MACHINE, // Sender does not know about a leaving peer
    RESPONSE_NOT_READY,           // Not ready to process this message (sender is at an earlier stage of the reassignment process)
    RESPONSE_NOT_REASSIGNING,     // Sender is not in the process of reassigning objects
    RESPONSE_PROCESSED,           // Sender has processed this message successfully
    RESPONSE_PROCESSED_ALREADY,   // Sender has already processed this message successfully
    RESPONSE_FINISHED,            // Sender has finished the reassignment process
    RESPONSE_MAX_TYPES            // Maximum number of response types
};

//PURPOSE
// The maximum value of the restart variable used to
// distinguish different attempts to complete an object reassignment
// process. The value wraps if the process restarts once this limit
// has been reached
const unsigned MAX_RESTART_NUM = MAX_NUM_PHYSICAL_PLAYERS * 2;

//PURPOSE
// A class for representing a network message containing a list of objects belonging to a player that has left
// the network session (see netobjectreassignmgr.h/.cpp for details)
class reassignNegotiateMsg
{
public:

    NET_MESSAGE_DECL(reassignNegotiateMsg, REASSIGN_NEGOTIATE_MSG);

    //PURPOSE
    // Contructor for creating an instance of the message from
    // packed message buffer data
    //PARAMS
    // leavingPeers        - storage for leaving peer IDs
    // buffer              - buffer to import message data into
    // maxByteSizeofBuffer - size of the buffer
    reassignNegotiateMsg(u64           *leavingPeers,
                         u8            *buffer,
                         const unsigned maxByteSizeofBuffer);

    //PURPOSE
    // Contructor for creating an instance of the message
    // for sending to a remote player
    //PARAMS
    // numObjects       - The number of objects contained in the message
    // leavingPeers     - The peer IDs of the players to leave the session involved in the reassignment 
    //                    that we haven't already negotiated with the target player for
    // numLeavingPeers  - The number of elements in the leavingPeers array
    // buffer           - Buffer containing the data about the objects to reassign
    // byteSizeofBuffer - The size of the buffer
    reassignNegotiateMsg(const unsigned     numObjects,
                         u64               *leavingPeers,
                         const unsigned     numLeavingPeers,
                         u8                *buffer,
                         const unsigned     byteSizeofBuffer);

    //PURPOSE
    // Returns the array of leaving peers and the number of elements
    //PARAMS
    // numLeavingPeers - The number of elements in the returned array
    u64 *GetLeavingPeersToNegotiateFor(unsigned &numLeavingPeers);

    //PURPOSE
    // Writes the contents of this message to a message log file
    //PARAMS
    // received - Was this message sent or received?
    // player   - The player the message is to/from
    void WriteToLogFile(bool received, const netPlayer &player) const;

    //PURPOSE
    // Serialisation function for the message data in the specified message. The manages both
    // the reading and writing of the packed message data depending on the serialisation mode.
    //PARAMS
    // bb  - import/export buffer (depends on the serialisation mode)
    // msg - message to read/write the data from/to
    NET_MESSAGE_SER(bb, msg)
    {
#if __ASSERT
        int oldCursorPos = bb.GetCursorPos();
#endif

        bool success = bb.SerUns(msg.m_NumObjects, SIZEOF_NUM_LOCAL_OBJECTS);

        success &= bb.SerUns(msg.m_NumLeavingPeers, SIZEOF_NUM_LEAVING_PEERS);

        // A bit dodgy but the game uses an array with MAX_NUM_PHYSICAL_PLAYERS elements
        if (!netVerifyf(msg.m_NumLeavingPeers <= MAX_NUM_PHYSICAL_PLAYERS, "m_NumLeavingPeers[%u] overflow", msg.m_NumLeavingPeers))
        {
            return false;
        }

        for(unsigned index = 0; index < msg.m_NumLeavingPeers; index++)
        {
            success &= bb.SerUns(msg.m_LeavingPeers[index], SIZEOF_LEAVING_PEER_ID);
        }

        success &= bb.SerUns(msg.m_ByteSizeofBuffer, SIZEOF_OBJECT_DATA_BUFFER);
        success &= AssertVerify(msg.m_ByteSizeofBuffer <= msg.m_MaxByteSizeofBuffer) &&
                       msg.m_ByteSizeofBuffer ? bb.SerBytes(msg.m_MessageBuffer, msg.m_ByteSizeofBuffer) : true;

#if __ASSERT
        int bitsWritten = bb.GetCursorPos() - oldCursorPos;
        Assert(bitsWritten == msg.GetMessageDataBitSize());
#endif

        return success;
    }

    static const unsigned SIZEOF_NUM_LOCAL_OBJECTS  = 8;
    static const unsigned SIZEOF_NUM_LEAVING_PEERS  = 5;
    static const unsigned SIZEOF_LEAVING_PEER_ID    = 64;
    static const unsigned SIZEOF_OBJECT_DATA_BUFFER = 16;

    //PURPOSE
    // Returns the total number of bits required to send the entire message contents (header and body)
    int GetMessageDataBitSize() const
    {
        return SIZEOF_NUM_LOCAL_OBJECTS                     +
               SIZEOF_NUM_LEAVING_PEERS                     +
               (SIZEOF_LEAVING_PEER_ID * m_NumLeavingPeers) +
               SIZEOF_OBJECT_DATA_BUFFER                    +
               (m_ByteSizeofBuffer ? (m_ByteSizeofBuffer<<3) : 0);
    }

    unsigned    m_NumObjects;          // The number of objects the player left
    u64        *m_LeavingPeers;        // The peer IDs of the players we haven't already negotiated with the target player for
    unsigned    m_NumLeavingPeers;     // The number of elements in the leavingPeers array
    u8         *m_MessageBuffer;       // The data used by the event
    unsigned    m_ByteSizeofBuffer;    // The size of the data used by the event
    unsigned    m_MaxByteSizeofBuffer; // The maximum size of the buffer
};

//PURPOSE
// A class for representing a network message containing the objects that the local machine is
// taking control of, and the objects we think the recipient is taking control of (see netobjectreassignmgr.h/.cpp for details)
class reassignConfirmMsg
{
public:

    NET_MESSAGE_DECL(reassignConfirmMsg, REASSIGN_CONFIRM_MSG);

    //PURPOSE
    // Contructor for creating an instance of the message from
    // packed message buffer data
    //PARAMS
    // buffer              - buffer to import message data into
    // maxByteSizeofBuffer - size of the buffer
    reassignConfirmMsg(u8            *buffer,
                       const unsigned maxByteSizeofBuffer);

    //PURPOSE
    // Contructor for creating an instance of the message
    // for sending to a remote player
    //PARAMS
    // numLocalObjects   - The number of objects contained in the message the local player is taking control of
    // numRemoteObjects  - The number of objects contained in the message the remote player is taking control of
    // leavingPeerID     - The peer ID of the last player to leave the session involved in the reassignment
    // buffer            - Buffer containing the data about the objects to reassign
    // byteSizeofBuffer  - The size of the buffer
    reassignConfirmMsg(const unsigned  numLocalObjects,
                       const unsigned  numRemoteObjects,
                       const u64       leavingPeerID,
                       u8             *buffer,
                       const unsigned  byteSizeofBuffer);

    //PURPOSE
    // Writes the contents of this message to a message log file
    //PARAMS
    // received - Was this message sent or received?
    // player   - The player the message is to/from
    void WriteToLogFile(bool received, const netPlayer &player) const;

    //PURPOSE
    // Serialisation function for the message data in the specified message. The manages both
    // the reading and writing of the packed message data depending on the serialisation mode.
    //PARAMS
    // bb  - import/export buffer (depends on the serialisation mode)
    // msg - message to read/write the data from/to
    NET_MESSAGE_SER(bb, msg)
    {
#if __ASSERT
        int oldCursorPos = bb.GetCursorPos();
#endif

        bool success = bb.SerUns(msg.m_NumLocalObjects,  SIZEOF_NUM_LOCAL_OBJECTS)   &&
                       bb.SerUns(msg.m_NumRemoteObjects, SIZEOF_NUM_REMOTE_OBJECTS)  &&
                       bb.SerUns(msg.m_LeavingPeerID,    SIZEOF_LEAVING_PEER_ID)     &&
                       bb.SerUns(msg.m_ByteSizeofBuffer, SIZEOF_OBJECT_DATA_BUFFER)  &&
                       AssertVerify(msg.m_ByteSizeofBuffer <= msg.m_MaxByteSizeofBuffer) &&
                       msg.m_ByteSizeofBuffer ? bb.SerBytes(msg.m_MessageBuffer, msg.m_ByteSizeofBuffer) : true;

#if __ASSERT
        int bitsWritten = bb.GetCursorPos() - oldCursorPos;
        Assert(bitsWritten == msg.GetMessageDataBitSize());
#endif

        return success;
    }

    static const unsigned SIZEOF_NUM_LOCAL_OBJECTS  = 8;
    static const unsigned SIZEOF_NUM_REMOTE_OBJECTS = 8;
    static const unsigned SIZEOF_LEAVING_PEER_ID    = 64;
    static const unsigned SIZEOF_OBJECT_DATA_BUFFER = 16;

    //PURPOSE
    // Returns the total number of bits required to send the entire message contents (header and body)
    int GetMessageDataBitSize() const
    {
        return SIZEOF_NUM_LOCAL_OBJECTS  +
               SIZEOF_NUM_REMOTE_OBJECTS +
               SIZEOF_LEAVING_PEER_ID    +
               SIZEOF_OBJECT_DATA_BUFFER +
               (m_ByteSizeofBuffer ? (m_ByteSizeofBuffer<<3) : 0);
    }

    unsigned  m_NumLocalObjects;     // The number of objects we are taking control of
    unsigned  m_NumRemoteObjects;    // The number of objects the recipient is taking control of
    u64       m_LeavingPeerID;       // The peer ID of the last player to leave the session involved in the reassignment
    u8       *m_MessageBuffer;       // The data used by the event
    unsigned  m_ByteSizeofBuffer;    // The size of the data used by the event
    unsigned  m_MaxByteSizeofBuffer; // The maximum size of the buffer
};

//PURPOSE
// A class for representing a network message containing a response to a reassignNegotiateMsg or reassignConfirmMsg message
class reassignResponseMsg
{
public:

    //PURPOSE
    // Enumeration of possible response types
    enum ResponseType
    {
        NEGOTIATION_RESPONSE,
        CONFIRM_RESPONSE
    };

    NET_MESSAGE_DECL(reassignResponseMsg, REASSIGN_RESPONSE_MSG);

    reassignResponseMsg();

    //PURPOSE
    // Contructor for creating an instance of the message
    // for sending to a remote player
    //PARAMS
    // responseType - The type of this response (negotiate or confirm)
    // leavingPeerID - The peer ID of the last player to leave the session involved in the reassignment
    // responseID    - The ID of the response
    reassignResponseMsg(const ResponseType responseType,
                        const u64          leavingPeerID = 0,
                        const u32          responseID = 0);
    
    //PURPOSE
    // Returns whether this is a response to a negotiation request
    bool IsNegotiateResponse() const { return m_ResponseType == NEGOTIATION_RESPONSE; }

    //PURPOSE
    // Returns whether this is a response to a confirm request
    bool IsConfirmResponse() const { return m_ResponseType == CONFIRM_RESPONSE;}

    //PURPOSE
    // Returns whether this response indicates the sender has moved to the confirmation stage of
    // the object reassignment process (only valid to call as a response to a negotiation request)
    bool IsConfirming();

    //PURPOSE
    // Flags this response to indicate that sender has moved on to the confirmation
    // stage of the object reassignment process (only valid to call as a response to a negotiation request)
    void SetConfirming();

    //PURPOSE
    // Returns whether the negotiation data was processed (only valid to call as a response to a negotiation request)
    bool HasProcessedNegotiationData();

    //PURPOSE
    // Flags this response to indicate that the negotiation data was processed
    // negotiation data (only valid to call as a response to a negotiation request)
    void SetHasProcessedNegotiationData();

    //PURPOSE
    // Returns the number of peer negotiation responses
    unsigned GetNumPeerNegotiationResponses();

    //PURPOSE
    // Returns peer negotiation response data
    //PARAMS
    // index        - Index into the list of responses 
    // peerID       - Peer ID the response is related to
    // responseCode - Response code for the related peer
    void GetPeerNegotiationResponseData(unsigned index, u64 &peerID, unsigned &responseCode);

    //PURPOSE
    // Adds a negotation response for the specified peer
    //PARAMS
    // peerID       - The peer ID the response is relative to
    // responseCode - The response code. This indicates whether this peer can be negotiated for yet
    void AddPeerNegotiationResponse(u64 peerID, unsigned responseCode);

    //PURPOSE
    // Writes the contents of this message to a message log file
    //PARAMS
    // received - Was this message sent or received?
    // player   - The player the message is to/from
    void WriteToLogFile(bool received, const netPlayer &player) const;

    //PURPOSE
    // Serialisation function for the message data in the specified message. The manages both
    // the reading and writing of the packed message data depending on the serialisation mode.
    //PARAMS
    // bb  - import/export buffer (depends on the serialisation mode)
    // msg - message to read/write the data from/to
    NET_MESSAGE_SER(bb, msg)
    {
#if __ASSERT
        int oldCursorPos = bb.GetCursorPos();
#endif

        bool success = bb.SerUns(msg.m_ResponseType, SIZEOF_RESPONSE_TYPE);
        
        if(msg.m_ResponseType == NEGOTIATION_RESPONSE)
        {
            success &= bb.SerBool(msg.m_Confirming);
            success &= bb.SerBool(msg.m_HasProcessedNegotiationData);
            success &= bb.SerUns(msg.m_NumPeerNegotiationResponses, SIZEOF_NUM_PEER_RESPONSES);

            if (!netVerifyf(msg.m_NumPeerNegotiationResponses <= MAX_NUM_PHYSICAL_PLAYERS, "m_NumPeerNegotiationResponses[%u] overflow", msg.m_NumPeerNegotiationResponses))
            {
                return false;
            }

            for(unsigned index = 0; index < msg.m_NumPeerNegotiationResponses; index++)
            {
                success &= bb.SerUns(msg.m_PeerNegotiationResponses[index].m_PeerID, SIZEOF_PEER_ID);
                success &= bb.SerUns(msg.m_PeerNegotiationResponses[index].m_ResponseCode, SIZEOF_RESPONSE_ID);
            }
        }
        else
        {
            success &= bb.SerUns(msg.m_LeavingPeerID, SIZEOF_PEER_ID);
            success &= bb.SerUns(msg.m_ResponseID, SIZEOF_RESPONSE_ID);
        }

#if __ASSERT
        int bitsWritten = bb.GetCursorPos() - oldCursorPos;
        Assert(bitsWritten == msg.GetMessageDataBitSize());
#endif

        return success;
    }

    static const unsigned SIZEOF_RESPONSE_TYPE      = 1;
    static const unsigned SIZEOF_RESPONSE_ID        = 3;
    static const unsigned SIZEOF_PEER_ID            = 64;
    static const unsigned SIZEOF_NUM_PEER_RESPONSES = 5;

    //PURPOSE
    // Returns the total number of bits required to send the entire message contents (header and body)
    int GetMessageDataBitSize() const
    {
        if(m_ResponseType == NEGOTIATION_RESPONSE)
        {
            return SIZEOF_RESPONSE_TYPE + 1 + 1 + SIZEOF_NUM_PEER_RESPONSES + (m_NumPeerNegotiationResponses * (SIZEOF_RESPONSE_ID + SIZEOF_PEER_ID));
        }
        else
        {
            return SIZEOF_RESPONSE_TYPE + SIZEOF_RESPONSE_ID + SIZEOF_PEER_ID;
        }
    }

    struct PeerNegotiationResponseData
    {
        u64 m_PeerID;
        u32 m_ResponseCode;
    };

    ResponseType m_ResponseType;                // Flag indicating whether this is a response to a negotiate or confirm message
    u64          m_LeavingPeerID;               // The peer ID of the last player to leave the session involved in the reassignment
    u32          m_ResponseID;                  // The response id
    bool         m_Confirming;                  // Indicates this is a negotiation response that the sender is in the confirmation stage
    bool         m_HasProcessedNegotiationData; // Indicates this is a negotiation response that the sender has processed the data
    unsigned     m_NumPeerNegotiationResponses; // Number of peer negotiation responses

    PeerNegotiationResponseData m_PeerNegotiationResponses[MAX_NUM_PHYSICAL_PLAYERS];
};

} // namespace rage

#endif  // NETWORKOBJECTMESSAGES_H
