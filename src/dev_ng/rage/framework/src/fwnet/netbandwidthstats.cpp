// 
// netBandwidthStatistics.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

// game includes
#include "fwnet/netbandwidthstats.h"

#include "fwsys/timer.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

#if __BANK

namespace rage
{

unsigned int				netBandwidthStatistics::sm_LastFrameSorted = 0;
unsigned int				netBandwidthStatistics::sm_NumTotalRecorders = 0;
netBandwidthRecorderWrapper netBandwidthStatistics::sm_GrandTotalsRecorder;
atFixedArray<netBandwidthRecorderWrapper*,  netBandwidthStatistics::MAX_TOTAL_RECORDERS> netBandwidthStatistics::sm_UnsortedRecorders( netBandwidthStatistics::MAX_TOTAL_RECORDERS);
atFixedArray<netBandwidthRecorderWrapper*,  netBandwidthStatistics::MAX_TOTAL_RECORDERS> netBandwidthStatistics::sm_HighestInboundBandwidth(netBandwidthStatistics::MAX_TOTAL_RECORDERS);
atFixedArray<netBandwidthRecorderWrapper*,  netBandwidthStatistics::MAX_TOTAL_RECORDERS> netBandwidthStatistics::sm_HighestOutboundBandwidth(netBandwidthStatistics::MAX_TOTAL_RECORDERS);

netBandwidthStatistics::netBandwidthStatistics() 
: m_Recorders(MAX_RECORDERS)
, m_DisableBandwidthRecording(false)
, m_NumRecorders(0)
, m_SampleInterval(0)
{
	m_Name[0] = '\0';
	CompileTimeAssert(MAX_RECORDERS<=(1<<((sizeof(RecorderId)<<3)-1)));
}


void netBandwidthStatistics::Init()
{
	m_NumRecorders = 0;
}

void netBandwidthStatistics::Update()
{
	for(unsigned recorderIndex = 0; recorderIndex < m_NumRecorders; recorderIndex++)
	{
		m_Recorders[recorderIndex].Update();
	}

	m_TotalsRecorder.Update();
}

RecorderId netBandwidthStatistics::AllocateBandwidthRecorder(const char* pRecorderName)
{
	if (!Verifyf(m_NumRecorders < MAX_RECORDERS, "Ran out of bandwidth recorders for '%s', m_NumRecorders='%d', MAX_RECORDERS'=%d'.", pRecorderName, m_NumRecorders, MAX_RECORDERS))
	{
		return INVALID_RECORDER_ID;
	}

	Assertf(m_Name, "Stats have not been registered with the bandwidth manager");
    Assertf(GetBandwidthRecorder(pRecorderName)==INVALID_RECORDER_ID, "A bandwidth recorder with the specified name has already been allocated!");

#if !__NO_OUTPUT
	//Added warning for builds that have asserts disabled.
	if (m_NumRecorders >= MAX_RECORDERS)
	{
		Warningf("netBandwidthStatistics::AllocateBandwidthRecorder--pRecorderName[%s] m_NumRecorders[%d] >= MAX_RECORDERS[%d] -- Ran out of bandwidth recorders. Increase MAX_RECORDERS.",pRecorderName,m_NumRecorders,MAX_RECORDERS);
	}
#endif

	RecorderId recorderId = static_cast<RecorderId>(m_NumRecorders);

	netBandwidthRecorderWrapper& newRecorder = m_Recorders[recorderId];

	newRecorder.SetName(pRecorderName);
	newRecorder.SetSampleInterval(m_SampleInterval);

	if (Verifyf(sm_NumTotalRecorders < MAX_TOTAL_RECORDERS, "Exceeded max total recorders for %s", pRecorderName))
	{
		sm_UnsortedRecorders[sm_NumTotalRecorders] = &newRecorder;
		sm_HighestInboundBandwidth [sm_NumTotalRecorders] = &newRecorder;
		sm_HighestOutboundBandwidth[sm_NumTotalRecorders] = &newRecorder;
		sm_NumTotalRecorders++;
	}

	m_NumRecorders++;

	return recorderId;
}

RecorderId netBandwidthStatistics::GetBandwidthRecorder(const char* pRecorderName) const
{
    if(Verifyf(pRecorderName, "Invalid recorder name specified!"))
    {
        for(unsigned index = 0; index < m_NumRecorders; index++)
        {
            if(m_Recorders[index].GetName() && strcmp(m_Recorders[index].GetName(), pRecorderName) == 0)
            {
                return (RecorderId)index;
            }
        }
    }

    return INVALID_RECORDER_ID;
}

const netBandwidthRecorderWrapper* netBandwidthStatistics::GetBandwidthRecorder(RecorderId id) const
{
	if (AssertVerify(id < (RecorderId)m_NumRecorders))
	{
		return &m_Recorders[id];
	}

	return NULL;
}

void netBandwidthStatistics::AddBandwidthIn(RecorderId recorderId, unsigned bandwidthIn, bool addToTotals)
{
    if(m_DisableBandwidthRecording)
    {
        return;
    }

	Assertf(static_cast<unsigned>(recorderId) < m_NumRecorders, "AddBandwidthIn: Invalid recorder id");

	m_Recorders[recorderId].AddBandwidthIn(bandwidthIn);

	if (addToTotals)
	{
		m_TotalsRecorder.AddBandwidthIn(bandwidthIn);
		sm_GrandTotalsRecorder.AddBandwidthIn(bandwidthIn);
	}
}

void netBandwidthStatistics::AddBandwidthOut(RecorderId recorderId, unsigned bandwidthOut, bool addToTotals)
{
    if(m_DisableBandwidthRecording)
    {
        return;
    }

	Assertf(static_cast<unsigned>(recorderId) < m_NumRecorders, "AddBandwidthOut: Invalid recorder id");

	m_Recorders[recorderId].AddBandwidthOut(bandwidthOut);

	if (addToTotals)
	{
		m_TotalsRecorder.AddBandwidthOut(bandwidthOut);
		sm_GrandTotalsRecorder.AddBandwidthOut(bandwidthOut);
	}
}

float netBandwidthStatistics::GetBandwidthInRate(RecorderId recorderId) const
{
    Assertf(static_cast<unsigned>(recorderId) < m_NumRecorders, "GetBandwidthInRate: Invalid recorder id");

	return m_Recorders[recorderId].GetBandwidthInRate();
}

float netBandwidthStatistics::GetBandwidthOutRate(RecorderId recorderId) const
{
    Assertf(static_cast<unsigned>(recorderId) < m_NumRecorders, "GetBandwidthOutRate: Invalid recorder id");

	return m_Recorders[recorderId].GetBandwidthOutRate();
}


float netBandwidthStatistics::GetBandwidthInRate() const
{
	return m_TotalsRecorder.GetBandwidthInRate();
}

float netBandwidthStatistics::GetBandwidthOutRate() const
{
	return m_TotalsRecorder.GetBandwidthOutRate();
}

void netBandwidthStatistics::SetSampleInterval(unsigned interval) 
{ 
	m_SampleInterval = interval; 
}

void netBandwidthStatistics::SampleData(unsigned currentTime)
{
	for(unsigned index = 0; index < m_NumRecorders; index++)
	{
		m_Recorders[index].SampleData(currentTime);
	}

    m_TotalsRecorder.SampleData(currentTime);
}

void netBandwidthStatistics::ResetStaticData()
{
	sm_NumTotalRecorders = 0;
	sm_LastFrameSorted = 0;
}

float netBandwidthStatistics::GetTotalBandwidthInRate() 
{
	return sm_GrandTotalsRecorder.GetBandwidthInRate();
}

float netBandwidthStatistics::GetTotalBandwidthOutRate()
{
	return sm_GrandTotalsRecorder.GetBandwidthOutRate();
}

static int CompareHighestInboundBandwidth(const void *paramA, const void *paramB)
{
	const netBandwidthRecorderWrapper *recorderA = *((netBandwidthRecorderWrapper **)(paramA));
	const netBandwidthRecorderWrapper *recorderB = *((netBandwidthRecorderWrapper **)(paramB));
	Assert(recorderA);
	Assert(recorderB);

	if(recorderA && recorderB)
	{
		float inboundbandwidthA = recorderA->GetBandwidthInRate();
		float inboundbandwidthB = recorderB->GetBandwidthInRate();

		if(inboundbandwidthA > inboundbandwidthB)
		{
			return -1;
		}
		else if(inboundbandwidthA < inboundbandwidthB)
		{
			return 1;
		}
	}

	return 0;
}

static int CompareHighestOutboundBandwidth(const void *paramA, const void *paramB)
{
	const netBandwidthRecorderWrapper *recorderA = *((netBandwidthRecorderWrapper **)(paramA));
	const netBandwidthRecorderWrapper *recorderB = *((netBandwidthRecorderWrapper **)(paramB));
	Assert(recorderA);
	Assert(recorderB);

	if(recorderA && recorderB)
	{
		float outboundbandwidthA = recorderA->GetBandwidthOutRate();
		float outboundbandwidthB = recorderB->GetBandwidthOutRate();

		if(outboundbandwidthA > outboundbandwidthB)
		{
			return -1;
		}
		else if(outboundbandwidthA < outboundbandwidthB)
		{
			return 1;
		}
	}

	return 0;
}

void netBandwidthStatistics::UpdateBandwidthRankings()
{
    if(sm_NumTotalRecorders > 0)
    {
        // rank recorders by bandwidth used
        qsort(&sm_HighestInboundBandwidth[0],  sm_NumTotalRecorders, sizeof(netBandwidthRecorderWrapper *), CompareHighestInboundBandwidth);
        qsort(&sm_HighestOutboundBandwidth[0], sm_NumTotalRecorders, sizeof(netBandwidthRecorderWrapper *), CompareHighestOutboundBandwidth);
    }

    sm_LastFrameSorted = fwTimer::GetFrameCount();
}

netBandwidthRecorderWrapper *netBandwidthStatistics::GetHighestBandwidthIn(unsigned rank, const char *&name, float &bandwidth)
{
    Assert(rank < sm_NumTotalRecorders);

    if(rank < sm_NumTotalRecorders)
    {
        name      = sm_HighestInboundBandwidth[rank]->GetName();
        bandwidth = sm_HighestInboundBandwidth[rank]->GetBandwidthInRate();

        return sm_HighestInboundBandwidth[rank];
    }

    return 0;
}

netBandwidthRecorderWrapper *netBandwidthStatistics::GetHighestBandwidthOut(unsigned rank, const char *&name, float &bandwidth)
{
    Assert(rank < sm_NumTotalRecorders);

    if(rank < sm_NumTotalRecorders)
    {
        name      = sm_HighestOutboundBandwidth[rank]->GetName();
        bandwidth = sm_HighestOutboundBandwidth[rank]->GetBandwidthOutRate();

        return sm_HighestOutboundBandwidth[rank];
    }

    return 0;
}

} // namespace rage

#endif  // __BANK
