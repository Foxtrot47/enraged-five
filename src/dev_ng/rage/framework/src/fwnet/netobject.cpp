//
// netobject.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwnet/netobject.h"

#include "fwsys/timer.h"
#include "fwnet/netlogginginterface.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netblender.h"
#include "fwnet/netplayer.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/nettypes.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{
netLoggingInterface *netObject::sm_Log = 0;

netObject::~netObject()
{
   // not allowed to delete objects that are still registered
    Assert(!GetListNode());
    Assert(!m_GameObject);
    Assert(!IsLocalFlagSet(LOCALFLAG_BEINGREASSIGNED));

    // this should have been removed from its list by the object manager before this object is deleted
    Assert(m_Node == NULL);

	m_ClonedState            = 0;
	m_PendingCloningState    = 0;
	m_PendingRemovalState    = 0;
    m_InScopeState           = 0;
	m_ClonedPlayersThatLeft	 = 0;
    m_LastPlayersSyncUpdated = 0;

    if (m_NetBlender)
    {
        delete m_NetBlender;
        m_NetBlender = NULL;
    }
}

void netObject::Init(const unsigned DEV_ONLY(numPlayers))
{
	m_ClonedState            = 0;
	m_PendingCloningState    = 0;
	m_PendingRemovalState    = 0;
    m_InScopeState           = 0;
	m_ClonedPlayersThatLeft	 = 0;
    m_LastPlayersSyncUpdated = 0;

#if __DEV
	Assert(numPlayers == MAX_NUM_PHYSICAL_PLAYERS);

	m_CloningTimers.Resize(numPlayers);

    for (unsigned i=0; i<numPlayers; i++)
    {
        m_CloningTimers[i] = 0;
    }
#endif

#if ENABLE_NETWORK_LOGGING
	// The last reason why a remote player was in or out of scope with this net object
	for (unsigned i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
	{
		m_LastScopeReason[i] = 0;
	}
#endif
	
#if !__FINAL
        // set up the logging name for this object
        sprintf(m_LogName, "%s_%d", GetObjectTypeName(), m_ObjectID);
#endif
}

NetworkObjectType netObject::GetObjectType() const
{
    return m_ObjectType;
}

void netObject::SetObjectType(const NetworkObjectType type)
{
    m_ObjectType = type;
}

void netObject::SetSyncTreeUpdateLevelAndBatch(const u8 updateLevel, const u8 batch)
{
    m_SyncTreeUpdateLevel      = updateLevel;
    m_NumPossibleUpdateBatches = GetSyncTree()->GetNumAllowedBatches(updateLevel);
    m_SyncTreeUpdateBatch      = batch;

    gnetAssertf(m_SyncTreeUpdateBatch < m_NumPossibleUpdateBatches, "Assigning to an invalid batch! (%d - max allowed %d)", m_SyncTreeUpdateBatch, m_NumPossibleUpdateBatches);
}

PhysicalPlayerIndex netObject::GetPhysicalPlayerIndex() const
{
    return m_PlayerIndex;
}

void netObject::SetPlayerIndex(const PhysicalPlayerIndex player)
{
    m_PlayerIndex = player;
}

netPlayer* netObject::GetPlayerOwner() const
{
    if (m_PlayerIndex == INVALID_PLAYER_INDEX)
        return NULL;

    return netInterface::GetPhysicalPlayerFromIndex(m_PlayerIndex);
}

PhysicalPlayerIndex netObject::GetPendingPlayerIndex() const
{
    return m_PendingPlayerIndex;
}

netPlayer* netObject::GetPendingPlayerOwner() const
{
    if (m_PendingPlayerIndex == INVALID_PLAYER_INDEX)
        return NULL;

    return netInterface::GetPhysicalPlayerFromIndex(m_PendingPlayerIndex);
}

void netObject::SetPendingPlayerIndex(const PhysicalPlayerIndex id)
{
    Assert(id != INVALID_PLAYER_INDEX && id < MAX_NUM_PHYSICAL_PLAYERS);
    m_PendingPlayerIndex = id;
}

void netObject::ClearPendingPlayerIndex()
{
    m_PendingPlayerIndex = (PhysicalPlayerIndex) INVALID_PLAYER_INDEX;
}

bool netObject::IsPendingOwnerChange() const
{
    return (m_PendingPlayerIndex != INVALID_PLAYER_INDEX);
}

NetObjFlags netObject::GetLocalFlags() const
{
    return m_LocalFlags;
}

bool netObject::IsLocalFlagSet(unsigned int flag) const
{
    return (m_LocalFlags & flag)!= 0;
}

void netObject::SetLocalFlag(unsigned int flag, bool b)
{
    if (b)
    {
        m_LocalFlags |= flag;
    }
    else
    {
        m_LocalFlags &= ~flag;
    }
}

NetObjFlags netObject::GetGlobalFlags() const
{
    return m_GlobalFlags;
}

bool netObject::IsGlobalFlagSet(unsigned int flag) const
{
    return (m_GlobalFlags & flag)!= 0;
}

void netObject::SetGlobalFlag(unsigned int flag, bool b)
{
    if (b)
    {
        m_GlobalFlags |= flag;
    }
    else
    {
        m_GlobalFlags &= ~flag;
    }
}

const char *netObject::GetLogName() const
{
#if !__FINAL
    return m_LogName;
#else
    return "";
#endif
}

void netObject::IncreaseOwnershipToken()
{
    m_OwnershipData.m_ownershipToken++;

    if(m_OwnershipData.m_ownershipToken > MAX_OWNERSHIP_TOKEN_VALUE)
    {
        m_OwnershipData.m_ownershipToken = 0;
    }

    if(sm_Log)
    {
        sm_Log->WriteDataValue("New token", "%d", m_OwnershipData.m_ownershipToken);
    }
}

bool netObject::HasBeenCloned() const
{
    return m_ClonedState != 0;
}

bool netObject::HasBeenCloned(const netPlayer& player) const
{
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		return ((m_ClonedState & (1<<player.GetPhysicalPlayerIndex()))!=0);
	}

	return false;
}

void netObject::SetBeenCloned(const netPlayer& player, bool bCloned)
{
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
#if __ASSERT
		if (bCloned)
		{
			Assert(!IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING));
			Assert(!IsPendingCloning(player));
			Assert(!IsPendingRemoval(player));
		}
#endif

		if (bCloned)
		{
			m_ClonedState |= (1<<player.GetPhysicalPlayerIndex());
		}
		else
		{
			m_ClonedState &= ~(1<<player.GetPhysicalPlayerIndex());
		}
	}
}

bool netObject::IsSyncedWithAllPlayers() const
{
	gnetAssert(!IsClone());

	if (GetSyncData())
	{
		unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
		const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

		for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
		{
			const netPlayer *remotePlayer = remotePhysicalPlayers[index];

			if (HasBeenCloned(*remotePlayer) && !GetSyncData()->IsSyncedWithPlayer(remotePlayer->GetPhysicalPlayerIndex()))
			{
				return false; 
			}
		}
	}

	return true;
}

bool netObject::IsCriticalStateSyncedWithAllPlayers() const
{
	gnetAssert(!IsClone());

	if (GetSyncData())
	{
		unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
		const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

		for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
		{
			const netPlayer *remotePlayer = remotePhysicalPlayers[index];

			if (HasBeenCloned(*remotePlayer) && !GetSyncData()->IsCriticalStateSyncedWithPlayer(remotePlayer->GetPhysicalPlayerIndex()))
			{
				return false; 
			}
		}
	}

	return true;
}

void netObject::GetClonedState(PlayerFlags &clonedState, PlayerFlags &createState, PlayerFlags &removeState) const
{
    clonedState = m_ClonedState;
    createState = m_PendingCloningState;
    removeState = m_PendingRemovalState;

	gnetAssert((clonedState & createState) == 0);
	gnetAssert((createState & removeState) == 0);
	gnetAssert((clonedState & removeState) == 0);
}

void netObject::SetClonedState(PlayerFlags clonedState, PlayerFlags createState, PlayerFlags removeState)
{
	gnetAssertf((clonedState & createState) == 0, "netObject::SetClonedState - invalid combination of states");
	gnetAssertf((createState & removeState) == 0, "netObject::SetClonedState - invalid combination of states");
	gnetAssertf((clonedState & removeState) == 0, "netObject::SetClonedState - invalid combination of states");

	m_ClonedState = m_PendingCloningState = m_PendingRemovalState = 0;

    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *pPlayer = allPhysicalPlayers[index];

        if (pPlayer->IsMyPlayer())
        {
#if __ASSERT
                // make sure that the object is not marked as cloned on our player. This will only be
                // the case when a local network bot is taking control of an object from another machine
                if(GetPlayerOwner() && GetPlayerOwner()->IsRemote())
                {
#if ENABLE_NETWORK_BOTS
                    if(netInterface::GetNumLocalActiveBots() == 0)
#endif // ENABLE_NETWORK_BOTS
                    {
                        bool clonedOnOurPlayer = (clonedState & (1<<pPlayer->GetPhysicalPlayerIndex()))!= 0;
                        Assert(!clonedOnOurPlayer);
                    }
                }
#endif
        }
        else
        {
            if (clonedState & (1<<pPlayer->GetPhysicalPlayerIndex()))
            {
                SetBeenCloned(*pPlayer, true);
            }
            else if (createState & (1<<pPlayer->GetPhysicalPlayerIndex()))
            {
                 SetPendingCloning(*pPlayer, true);
            }
            else if (removeState & (1<<pPlayer->GetPhysicalPlayerIndex()))
            {
                SetPendingRemoval(*pPlayer, true);
            }
        }
    }

    m_InScopeState = m_ClonedState;
}

bool netObject::IsPendingCloning() const
{
    return m_PendingCloningState != 0;
}

bool netObject::IsPendingCloning(const netPlayer& player) const
{
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		return ((m_PendingCloningState & (1<<player.GetPhysicalPlayerIndex()))!=0);
	}

	return false;
}

void netObject::SetPendingCloning(const netPlayer& player, bool bCloned)
{
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
#if __ASSERT
		if (bCloned)
		{
			Assert(!IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING));
			Assert(!HasBeenCloned(player));
			Assert(!IsPendingRemoval(player));
		}
#endif

		if (bCloned)
		{
			m_PendingCloningState |= (1<<player.GetPhysicalPlayerIndex());
		}
		else
		{
			m_PendingCloningState &= ~(1<<player.GetPhysicalPlayerIndex());
		}
	}
}

bool netObject::IsPendingRemoval() const
{
    return m_PendingRemovalState != 0;
}

bool netObject::IsPendingRemoval(const netPlayer& player) const
{
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		return ((m_PendingRemovalState & (1<<player.GetPhysicalPlayerIndex()))!=0);
	}

	return false;
}

void netObject::SetPendingRemoval(const netPlayer& player, bool bRemoval)
{
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
#if __ASSERT
		if (bRemoval)
		{
			Assert(!IsPendingCloning(player));
			Assert(!HasBeenCloned(player));
		}
#endif

		if (bRemoval)
		{
			m_PendingRemovalState |= (1<<player.GetPhysicalPlayerIndex());
		}
		else
		{
			m_PendingRemovalState &= ~(1<<player.GetPhysicalPlayerIndex());
		}
	}
}

void netObject::ResetCloningTimer(const netPlayer& DEV_ONLY(player))
{
#if __DEV
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		 m_CloningTimers[player.GetPhysicalPlayerIndex()] = 0;
	}
#endif
}

void netObject::IncCloningTimer(const netPlayer& DEV_ONLY(player))
{
#if __DEV
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		m_CloningTimers[player.GetPhysicalPlayerIndex()] += fwTimer::GetSystemTimeStepInMilliseconds();
	}
#endif
}

bool netObject::HasCloningTimedOut(const netPlayer& DEV_ONLY(player)) const
{
#if __DEV
	if (AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		static const unsigned int CLONING_TIMEOUT_TIME  = 5000; // max time allowed for a network object to try and clone or remove itself

		if (m_CloningTimers[player.GetPhysicalPlayerIndex()] >= CLONING_TIMEOUT_TIME)
		{
			return true;
		}
	}
#endif

    return false;
}

netBlender* netObject::GetNetBlender() const
{
    return m_NetBlender;
}

void netObject::DestroyNetBlender()
{
    if (m_NetBlender)
    {
        delete m_NetBlender;
        m_NetBlender = 0;
    }
}

atDNode<netObject*, datBase>* netObject::GetListNode() const
{
    return m_Node;
}

void netObject::SetListNode(atDNode<netObject*, datBase>* pNode)
{
    m_Node = pNode;
}

void netObject::SetUpdateLevel(PhysicalPlayerIndex playerIndex, u8 updateLevel)
{
    u8 oldUpdateLevel = GetUpdateLevel(playerIndex);

    // only change the update level if it is higher or this object will be updated this frame,
    // otherwise it could be possible for objects to be starved of updates if they frequently moved
    // between update levels and missed their batch update
    if(updateLevel > oldUpdateLevel || IsInCurrentUpdateBatch())
    {
        GetSyncDataUL()->SetUpdateLevel(playerIndex, updateLevel);
    }
}

bool netObject::IsInScope(const netPlayer& UNUSED_PARAM(player), unsigned* UNUSED_PARAM(scopeReason)) const
{
    return IsGlobalFlagSet(netObject::GLOBALFLAG_CLONEALWAYS);
}

bool netObject::Update()
{
	// We're expecting an update at least once per second, so assuming a maximum frame rate of 60
	// Note we can't use the maximum number of batches used by the sync tree code here as this may be lower
	ASSERT_ONLY(static const unsigned MAX_FRAMES_WITHOUT_UPDATE = 62);
	ASSERT_ONLY(gnetAssertf(IsClone() ||(fwTimer::GetFrameCount() - m_LastFrameSyncTreeUpdated) < MAX_FRAMES_WITHOUT_UPDATE, "%s has not been updated via the sync tree for %d frames! (currtime is %d)", GetLogName(), (fwTimer::GetFrameCount() - m_LastFrameSyncTreeUpdated), netInterface::GetSynchronisationTime());)

    // create or destroy sync data as needed
    if(m_syncDataChangeTimer > 0)
    {
        m_syncDataChangeTimer--;
    }
    else
    {
        if (CanSynchronise(false))
        {
            if (!m_SyncData.GetSyncData())
            {
                StartSynchronising();
            }
        }
        else if (m_SyncData.GetSyncData() && !IsPendingCloning() && CanStopSynchronising())
        {
			StopSynchronising();
        }
    }

    if (!IsClone())
    {		
		bool forceManageUpdateLevelThisFrame = ForceManageUpdateLevelThisFrame();

        if(IsInCurrentUpdateBatch() || forceManageUpdateLevelThisFrame)
        {
            u8 numBatches         = gnetVerifyf(GetSyncTree(), "Network object has no sync tree!") ? GetSyncTree()->GetNumAllowedBatches(GetMinimumUpdateLevel()) : 1;
            u8 playerToUpdate     = m_StartIndexForPlayerBatch;
            u8 numPlayersToUpdate = GetNumPlayersToUpdatePerBatch() * numBatches;

            numPlayersToUpdate = rage::Min(numPlayersToUpdate, MAX_NUM_PHYSICAL_PLAYERS);

			if(forceManageUpdateLevelThisFrame)
			{
				numPlayersToUpdate = MAX_NUM_PHYSICAL_PLAYERS;
			}

            // check if the objects needs to change its update level to the remote players in the game
            for(u8 index = 0; index < numPlayersToUpdate; index++)
            {
                const netPlayer *remotePlayer = netInterface::GetPlayerMgr().GetPhysicalPlayerFromIndex(playerToUpdate);

                if(remotePlayer && remotePlayer != GetPlayerOwner())
                {
					// Update this only when we are in batch, forceManageUpdateLevelThisFrame is for ManageUpdateLevel
					if (IsInCurrentUpdateBatch())
					{
						UpdateInScopeState(*remotePlayer);
					}					
 
                    if(HasBeenCloned(*remotePlayer))
                    {
                        ManageUpdateLevel(*remotePlayer);
                    }
                }

                playerToUpdate++;

                if(playerToUpdate >= MAX_NUM_PHYSICAL_PLAYERS)
                {
                    playerToUpdate = 0;
                }
            }

            m_StartIndexForPlayerBatch = playerToUpdate;
        }


        // sanity check pending player id is for a valid player!
        if (GetPendingPlayerIndex() != INVALID_PLAYER_INDEX)
        {
			if (GetPendingPlayerIndex() == netInterface::GetLocalPhysicalPlayerIndex())
			{
				Assertf(0, "%s has a pending player id that is the local player", GetLogName());
				ClearPendingPlayerIndex();
			}
            else if (!netInterface::GetPhysicalPlayerFromIndex(GetPendingPlayerIndex()))
            {
                Assertf(0, "%s has a pending player id for a player that does not exist", GetLogName());
                ClearPendingPlayerIndex();
            }
        }
    }

    return false;
}

void netObject::StartSynchronising()
{
	if (AssertVerify(!GetSyncData()))
	{
		m_SyncData.SetSyncData(CreateSyncData());

		if (AssertVerify(m_SyncData.GetSyncData()))
		{
			m_SyncData.GetSyncData()->Init();
		}

		GetSyncTree()->InitialiseData(this);

		ResetSyncDataChangeTimer();

		m_InScopeState = 0;

        u8 defaultUpdateLevel = GetDefaultUpdateLevel();

		// determine the scope immediately so there is not a delay before the object is cloned on other machines
		unsigned           numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
		netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

		for (unsigned i = 0; i < numRemotePhysicalPlayers; i++)
		{
			netPlayer* remotePlayer = remotePhysicalPlayers[i];

			unsigned scopeReason = 0;

			if (IsInScope(*remotePlayer, &scopeReason))
			{
				PhysicalPlayerIndex playerIndex = remotePhysicalPlayers[i]->GetPhysicalPlayerIndex();

				m_InScopeState |= (1<<playerIndex);

				LogScopeReason(true, *remotePlayer, scopeReason);

				// default update level to high when we start syncing - better to sync too much data than too little
				// the update level will be set to it's correct value based on the objects attributes in the next few frames
				m_SyncData.SetUpdateLevel(playerIndex, defaultUpdateLevel);
			}
			else
			{
				LogScopeReason(false, *remotePlayer, scopeReason);
			}
		}
	}
}

void netObject::StopSynchronising()
{
    if (AssertVerify(m_SyncData.GetSyncData()))
    {
        m_SyncData.GetSyncData()->Shutdown();
    }

    m_SyncData.DestroySyncData();

    ResetSyncDataChangeTimer();

    netLoggingInterface *log = GetLog();

    if(log && !IsClone())
    {
        NetworkLogUtils::WriteLogEvent(*log, GetLogName(), "STOPPED_SYNCING");
    }
}

void netObject::ResetSyncDataChangeTimer()
{
    const unsigned SYNC_DATA_CHANGE_DELAY_IN_FRAMES = 15; // 15 frames ~0.5s when things are running as they should
    m_syncDataChangeTimer = SYNC_DATA_CHANGE_DELAY_IN_FRAMES;
}

bool netObject::CanPassControl(const netPlayer& player, eMigrationType UNUSED_PARAM(migrationType), unsigned *resultCode) const
{
    if(resultCode)
    {
        *resultCode = CPC_SUCCESS;
    }

    if (player.IsMyPlayer())
        return true;

    if (!HasBeenCloned(player))
    {
        // local network bots don't have the object marked as cloned on their player at the moment,
        // so only reject remote player in this case
        if(player.IsRemote())
        {
            bool clonedOnParent = false;

            if(player.IsBot())
            {
                netPlayer *parentPlayer = netInterface::GetPlayerFromPeerId(player.GetRlPeerId());
                if(gnetVerify(parentPlayer && !parentPlayer->IsBot()))
                {
                    clonedOnParent = HasBeenCloned(*parentPlayer);
                }
            }

            if(!clonedOnParent)
            {
                if(resultCode)
                {
                    *resultCode = CPC_FAIL_NOT_CLONED;
                }

                return false;
            }
        }
    }

    if (IsLocalFlagSet(LOCALFLAG_UNREGISTERING))
    {
        if(resultCode)
        {
            *resultCode = CPC_FAIL_UNREGISTERING;
        }

        return false;
    }

    if (IsLocalFlagSet(LOCALFLAG_BEINGREASSIGNED))
    {
        if(resultCode)
        {
            *resultCode = CPC_FAIL_BEING_REASSIGNED;
        }

        return false;
    }

    if (IsClone())
    {
        if(resultCode)
        {
            *resultCode = CPC_FAIL_IS_CLONE;
        }

        return false;
    }

    if (IsPendingOwnerChange() && GetPendingPlayerIndex() != player.GetPhysicalPlayerIndex())
    {
        if(resultCode)
        {
            *resultCode = CPC_FAIL_IS_MIGRATING;
        }

        return false;
    }

    // we can't pass control if the persistent owner flag is set in any cases
    if (IsGlobalFlagSet(GLOBALFLAG_PERSISTENTOWNER))
    {
        if(resultCode)
        {
            *resultCode = CPC_FAIL_PERSISTANT_OWNER;
        }

        return false;
    }

    // don't pass if the object is in the process of being cloned or removed
    // ** this check must be done or objects can become orphaned **
    PlayerFlags clonedState, createState, removeState;

    GetClonedState(clonedState, createState, removeState);

    if (createState != 0 || removeState != 0)
    {
        if(resultCode)
        {
            *resultCode = CPC_FAIL_PENDING_CLONE_OR_REMOVE;
        }
        return false;
    }

    return true;
}

bool netObject::CanAcceptControl(const netPlayer& player, eMigrationType UNUSED_PARAM(migrationType), unsigned *resultCode) const
{
    if (&player != GetPlayerOwner())
    {
        if(resultCode)
        {
            *resultCode = CAC_FAIL_WRONG_OWNER;
        }

        return false;
    }

    if (IsLocalFlagSet(LOCALFLAG_BEINGREASSIGNED))
    {
        if(resultCode)
        {
            *resultCode = CAC_FAIL_BEING_REASSIGNED;
        }

        return false;
    }

    if (IsPendingOwnerChange())
    {
        // we can still accept control of an object if we are
        // passing control between local players
        netPlayer *pendingOwner = GetPendingPlayerOwner();

        if(pendingOwner && pendingOwner->IsRemote())
        {
            if(resultCode)
            {
                *resultCode = CAC_FAIL_IS_MIGRATING;
            }

            return false;
        }
    }

	//If we dont have a game object dont accept ownership
	if (!HasGameObject() && !CanPassControlWithNoGameObject())
	{
		if(resultCode)
		{
			*resultCode = CAC_FAIL_NO_GAME_OBJECT;
		}
		return false;
	}

    if(resultCode)
    {
        *resultCode = CAC_SUCCESS;
    }

    return true;
}

void netObject::ChangeOwner(const netPlayer& player, eMigrationType UNUSED_PARAM(migrationType))
{
    Assert(m_PendingPlayerIndex == (PhysicalPlayerIndex) INVALID_PLAYER_INDEX || m_PendingPlayerIndex == player.GetPhysicalPlayerIndex());

    netPlayer *pOldOwner = m_PlayerIndex != INVALID_PLAYER_INDEX ? netInterface::GetPhysicalPlayerFromIndex(m_PlayerIndex) : NULL;

    if(sm_Log)
    {
        NetworkLogUtils::WriteLogEvent(*sm_Log, "CHANGE_OWNER", GetLogName());
        sm_Log->WriteDataValue("Last owner", pOldOwner ? pOldOwner->GetLogName() : "Unknown");
        sm_Log->WriteDataValue("New owner", player.GetLogName());
    }

    bool bWasClone = m_IsClone;

    m_IsClone = !player.IsLocal();
    m_PlayerIndex = player.GetPhysicalPlayerIndex();

    ClearPendingPlayerIndex();

	if (bWasClone && !IsClone())
    {
#if __DEV
        // reset cloning timers
        for (int i=0; i<m_CloningTimers.GetMaxCount(); i++)
        {
            m_CloningTimers[i] = 0;
        }
#endif
    }
    else if (!bWasClone && IsClone())
    {
		m_ClonedState            = 0;
		m_PendingCloningState    = 0;
		m_PendingRemovalState    = 0;
		m_ClonedPlayersThatLeft  = 0;
        m_LastPlayersSyncUpdated = ~0u;

        if (m_SyncData.GetSyncData())
        {
            StopSynchronising();
        }

        m_StarvationData.Reset();
    }

    // inform the network blender ownership of the object has changed
    if (m_NetBlender && HasGameObject())
    {
        m_NetBlender->OnOwnerChange();
    }
}

bool netObject::NeedsReassigning() const 
{ 
	if (IsLocalFlagSet(LOCALFLAG_NOREASSIGN) || (!CanReassignWithNoGameObject() && !HasGameObject()))
	{
		return false;
	}

	return true;
}

void netObject::PostMigrate(eMigrationType UNUSED_PARAM(migrationType))
{
	if (!IsClone())
	{
		// we have to start synchronising so that the ownership token is sent out
		if((HasGameObject() || CanSyncWithNoGameObject()) && !GetSyncData())
		{
            StartSynchronising();
		}

		// update the ownership token information
		IncreaseOwnershipToken();
	}
}

void netObject::PlayerHasJoined(const netPlayer& player)
{
    if (!IsClone() && GetSyncData())
    {
        GetSyncData()->InitialisePlayerSyncData(player.GetPhysicalPlayerIndex(), true);
    }
	
	m_ClonedPlayersThatLeft &= ~player.GetPhysicalPlayerIndex();
}

void netObject::PlayerHasLeft(const netPlayer& player)
{
	if (!IsClone() && HasBeenCloned(player))
	{
		m_ClonedPlayersThatLeft |= player.GetPhysicalPlayerIndex();
	}

    SetBeenCloned(player, false);
    SetPendingCloning(player, false);
    SetPendingRemoval(player, false);

	if (GetPendingPlayerIndex() == player.GetPhysicalPlayerIndex())
	{
		ClearPendingPlayerIndex();
	}

    ResetStarvationCount(player.GetPhysicalPlayerIndex());

    // need to reset the players update level to very low here, otherwise the minimum update level calculation will be wrong
    m_SyncData.SetUpdateLevel(player.GetPhysicalPlayerIndex(), CNetworkSyncDataULBase::UPDATE_LEVEL_VERY_LOW);

#if ENABLE_NETWORK_LOGGING
	if (player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
	{
		m_LastScopeReason[player.GetPhysicalPlayerIndex()] = 0;
	}
#endif // ENABLE_NETWORK_LOGGING
}

bool netObject::CheckPlayerHasAuthorityOverObject(const netPlayer& player)
{
    bool   playerHasAuthority = (GetPhysicalPlayerIndex() == player.GetPhysicalPlayerIndex()) && !IsPendingOwnerChange();

    return playerHasAuthority;
}

void netObject::SetStarvationThreshold(u8 threshold)
{
    netStarvationData::SetStarvationThreshold(threshold);
}

bool netObject::IsStarving(PhysicalPlayerIndex playerIndex) const
{
    if(playerIndex == INVALID_PLAYER_INDEX)
    {
        return m_StarvationData.IsStarvingOnAnyPlayer();
    }
    else
    {
        return m_StarvationData.IsStarving(playerIndex);
    }
}

void netObject::IncreaseStarvationCount(PhysicalPlayerIndex playerIndex)
{
    m_StarvationData.IncreaseStarvationCount(playerIndex);
}

void netObject::ResetStarvationCount(PhysicalPlayerIndex playerIndex)
{
    m_StarvationData.ResetStarvationCount(playerIndex);
}

void netObject::UpdateInScopeState(const netPlayer &remotePlayer)
{
#if !ENABLE_NETWORK_BOTS
    gnetAssertf(!remotePlayer.IsLocal(), "Trying to update the is in scope state for a local player!");
#endif // !ENABLE_NETWORK_BOTS

    PhysicalPlayerIndex playerIndex = remotePlayer.GetPhysicalPlayerIndex();

    if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Trying to update the in scope state for a player with an invalid physical player index!"))
    {
		unsigned scopeReason = 0;
#if ENABLE_NETWORK_LOGGING		
		unsigned lastScopeReason = 0;

		bool wasInScope = (m_InScopeState & (1<<playerIndex)) != 0;

		if (remotePlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
		{
			lastScopeReason = (unsigned)m_LastScopeReason[remotePlayer.GetPhysicalPlayerIndex()];
		}
#endif // ENABLE_NETWORK_LOGGING

        if (IsInScope(remotePlayer, &scopeReason))
        {
            m_InScopeState |= (1<<playerIndex);

#if ENABLE_NETWORK_LOGGING
			if (!wasInScope || scopeReason != lastScopeReason)
			{
				LogScopeReason(true, remotePlayer, scopeReason);
			}
#endif // ENABLE_NETWORK_LOGGING
        }
        else
        {
            m_InScopeState &= ~(1<<playerIndex);

#if ENABLE_NETWORK_LOGGING
			if (wasInScope || scopeReason != lastScopeReason)
			{
				LogScopeReason(false, remotePlayer, scopeReason);
			}
#endif // ENABLE_NETWORK_LOGGING
		}
    }
}

#if ENABLE_NETWORK_LOGGING

const char *netObject::GetCanCloneErrorString(unsigned resultCode)
{
    return GetCanCloneErrorString(resultCode);
}

const char *netObject::GetCannotDeleteReason(unsigned reason)
{
	switch(reason)
	{
	case CTD_MIGRATING:
		return "Object Migrating";
    case CTD_BEING_REASSIGNED:
        return "Object being reassigned";
	default:
		gnetAssertf(0, "Unknown can't delete reason!");
		return "Unknown";
	}
}

#endif // ENABLE_NETWORK_LOGGING

bool netObject::CanClone(const netPlayer& player, unsigned *resultCode) const
{
    if(resultCode)
    {
        *resultCode = CC_SUCCESS;
    }

    if(IsLocalFlagSet(LOCALFLAG_NOCLONE))
    {
        if(resultCode)
        {
            *resultCode = CC_NO_CLONE_FLAG;
        }

        return false;
    }

    if (IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
    {
        if(resultCode)
        {
            *resultCode = CC_UNREGISTERING;
        }

        return false;
    }

    // don't clone if we are in the process of removal!
    if (IsPendingRemoval(player))
    {
        if(resultCode)
        {
            *resultCode = CC_PENDING_REMOVAL;
        }

        return false;
    }

     // don't clone if the object is about to change owner
    if (GetPendingPlayerOwner() && &player != GetPendingPlayerOwner())
    {
        if(resultCode)
        {
            *resultCode = CC_PENDING_OWNER_CHANGE;
        }

        return false;
    }

    return true;
}

bool netObject::CanStopSynchronising() const
{
	return (IsClone() || IsSyncedWithAllPlayers());
}

bool netObject::CanDelete(unsigned* LOGGING_ONLY(reason))
{
    if (!IsClone())
    {
        // generally an object pending ownership change CANNOT be deleted because if the object was a vehicle the peds within it will not
        // change owner with the vehicle if the vehicle game object has gone.
        if (IsPendingOwnerChange())
        {
#if ENABLE_NETWORK_LOGGING
			if(reason)
				*reason = CTD_MIGRATING;
#endif // ENABLE_NETWORK_LOGGING
            return false;
        }

        if(IsLocalFlagSet(LOCALFLAG_BEINGREASSIGNED))
        {
#if ENABLE_NETWORK_LOGGING
            if(reason)
                *reason = CTD_BEING_REASSIGNED;
#endif // ENABLE_NETWORK_LOGGING
            return false;
        }
    }

    return true;
}

u32 netObject::CalcReassignPriority() const
{
    return 0;
}

void netObject::SetGlobalFlags(NetObjFlags globalFlags)
{
    m_GlobalFlags = globalFlags;
}

//
// netStarvationData
// 
u8 netObject::netStarvationData::ms_StarvationThreshold = 1;

netObject::netStarvationData::netStarvationData()
{
    Reset();
}

void netObject::netStarvationData::Reset()
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS>>1; index++)
    {
        m_StarvationData[index] = 0;
    }
}

void netObject::netStarvationData::SetStarvationThreshold(u8 threshold)
{
    if(gnetVerifyf(threshold <= MAX_STARVATION_COUNT, "The maximum possible starvation threshold is %d!", MAX_STARVATION_COUNT))
    {
        ms_StarvationThreshold = threshold;
    }
}

bool netObject::netStarvationData::IsStarvingOnAnyPlayer() const
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS>>1; index++)
    {
        u8 oddPlayerData   = (m_StarvationData[index] & PLAYER1_MASK);
        u8 evenPlayerData  = (m_StarvationData[index] & PLAYER2_MASK)>>PLAYER2_SHIFT;

        if(oddPlayerData  >= ms_StarvationThreshold ||
           evenPlayerData >= ms_StarvationThreshold)
        {
            return true;
        }
    }

    return false;
}

bool netObject::netStarvationData::IsStarving(PhysicalPlayerIndex playerIndex) const
{
    bool starving = false;

    if(gnetVerifyf(playerIndex < MAX_NUM_PHYSICAL_PLAYERS, "Invalid player index!"))
    {
        unsigned dataIndex = playerIndex / 2;

        u8 oddPlayerData  = (m_StarvationData[dataIndex] & PLAYER1_MASK);
        u8 evenPlayerData = (m_StarvationData[dataIndex] & PLAYER2_MASK)>>PLAYER2_SHIFT;

        bool playerOdd = ((playerIndex & 1) != 0);

        if((playerOdd  && (oddPlayerData  >= ms_StarvationThreshold)) ||
           (!playerOdd && (evenPlayerData >= ms_StarvationThreshold)))
        {
            return true;
        }
    }

    return starving;
}

void netObject::netStarvationData::IncreaseStarvationCount(PhysicalPlayerIndex playerIndex)
{
    if(gnetVerifyf(playerIndex < MAX_NUM_PHYSICAL_PLAYERS, "Invalid player index!"))
    {
        unsigned dataIndex = playerIndex / 2;

        u8 starvationData  = m_StarvationData[dataIndex];
        u8 oddPlayerData   = (starvationData & PLAYER1_MASK);
        u8 evenPlayerData  = (starvationData & PLAYER2_MASK)>>PLAYER2_SHIFT;

        bool playerOdd = ((playerIndex & 1) != 0);

        if((playerOdd && oddPlayerData < MAX_STARVATION_COUNT) ||
           (!playerOdd && evenPlayerData < MAX_STARVATION_COUNT))
        {
            playerOdd ? oddPlayerData++ : evenPlayerData++;

            m_StarvationData[dataIndex]  = oddPlayerData;
            m_StarvationData[dataIndex] |= (evenPlayerData << PLAYER2_SHIFT);
        }
    }
}

void netObject::netStarvationData::ResetStarvationCount(PhysicalPlayerIndex playerIndex)
{
    if(gnetVerifyf(playerIndex < MAX_NUM_PHYSICAL_PLAYERS, "Invalid player index!"))
    {
        unsigned dataIndex = playerIndex / 2;

        u8 starvationData  = m_StarvationData[dataIndex];
        u8 oddPlayerData   = (starvationData & PLAYER1_MASK);
        u8 evenPlayerData  = (starvationData & PLAYER2_MASK)>>PLAYER2_SHIFT;

        bool playerOdd = ((playerIndex & 1) != 0);

        if(playerOdd)
        {
            oddPlayerData = 0;
        }
        else
        {
            evenPlayerData = 0;
        }

        m_StarvationData[dataIndex]  = oddPlayerData;
        m_StarvationData[dataIndex] |= (evenPlayerData << PLAYER2_SHIFT);
    }
}

} // namespace rage
