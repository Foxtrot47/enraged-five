// 
// netleaderboardwrite.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

//Rage includes
#include "rline/rlstats.h"

//Framework Includes
#include "fwnet/netleaderboardwrite.h"
#include "fwnet/optimisations.h"
#include "fwnet/netscgameconfigparser.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, leaderboard_write, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_leaderboard_write


// --------------------- netLeaderboard2write

const char* GetWriteTypeString(const rlLeaderboard2Type type)
{
	if (type == RL_LEADERBOARD2_TYPE_INVALID)
		return "TYPE_INVALID";
	else if (type == RL_LEADERBOARD2_TYPE_PLAYER)
		return "TYPE_PLAYER";
	else if (type == RL_LEADERBOARD2_TYPE_CLAN)
		return "TYPE_CLAN";
	else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		return "TYPE_CLAN_MEMBER";
	else if (type == RL_LEADERBOARD2_TYPE_GROUP)
		return "TYPE_GROUP";
	else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
		return "TYPE_GROUP_MEMBER";

	return "LEADERBOARD_TYPE_UNKNOWN";
}

netLeaderboardWrite::~netLeaderboardWrite()
{
	if (m_InputValueHeap)
	{
		Free(m_InputValueHeap);
		m_InputValueHeap = 0;
	}

	m_Allocator = 0;
}

bool
netLeaderboardWrite::Init(sysMemAllocator* pAllocator, const unsigned id, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId& clanId)
{
	ASSERT_ONLY( FastAssert( pAllocator ); )
	ASSERT_ONLY( FastAssert( !m_Allocator ); )
	ASSERT_ONLY( FastAssert( !m_InputValueHeap ); )

	bool result = false;
	 
	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard( id );
	if (!gnetVerify(lbConf))
		return result;

	m_Allocator = pAllocator;

	m_InputValueHeap = Allocate< rage::rlLeaderboardInputValue >(lbConf->m_columns.GetCount());
	if (!gnetVerify(m_InputValueHeap))
		return result;

	result = true;

	this->rlLeaderboard2Update::Init(m_InputValueHeap, lbConf->m_columns.GetCount());

	m_LeaderboardId = id;
	m_ClanId        = clanId;
	m_GroupSelector.m_NumGroups = groupSelector.m_NumGroups;

	//Reset all
	for(int i=0; i<RL_LEADERBOARD2_MAX_GROUPS; ++i)
	{
		m_GroupSelector.m_Group[i].m_Category[0] = '\0';
		m_GroupSelector.m_Group[i].m_Id[0] = '\0';
	}

	//Setup the ones that should be set.
	for(int i=0; i<(int)groupSelector.m_NumGroups; ++i)
	{
		const rlLeaderboard2Group& fromGrp = groupSelector.m_Group[i];
		rlLeaderboard2Group& toGrp = m_GroupSelector.m_Group[i];

		gnetAssertf(strlen(fromGrp.m_Category) > 0, "Invalid m_Category name in leaderboard id=\"%s:%d\" update", lbConf->GetName(), m_LeaderboardId);
		formatf(toGrp.m_Category, "%s", fromGrp.m_Category);

		gnetAssertf(strlen(fromGrp.m_Id) > 0, "Invalid m_Id name in leaderboard id=\"%s:%d\" update", lbConf->GetName(), m_LeaderboardId);
		formatf(toGrp.m_Id, "%s", fromGrp.m_Id);
	}

	return (result);
}

bool  
netLeaderboardWrite::AddColumn(const rlStatValue& inputValue, const unsigned inputId)
{
	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard( m_LeaderboardId );
	gnetAssertf(lbConf, "Invalid leaderboard id=\"%d\".", m_LeaderboardId);
	if (!lbConf)
		return false;

	const LeaderboardInput* input = GAME_CONFIG_PARSER.GetInputById(inputId);
	gnetAssertf(input, "Invalid Columns ID, leaderboard id=\"%s:%d\", numColumns=%d, inputId=%d.", lbConf->GetName(), m_LeaderboardId, m_NumValues, inputId);
	if (!input)
		return false;

	gnetAssertf(m_NumValues < m_MaxValues, "Max number of columns already set, leaderboard id=\"%s:%d\", numColumns=%d, inputId=%s(%d).", lbConf->GetName(), m_LeaderboardId, m_NumValues, input->GetName(), input->m_id);
	if (m_NumValues >= m_MaxValues)
		return false;

#if __ASSERT
	for (int i=0; i<lbConf->m_columns.GetCount(); i++)
	{
		if (inputId == lbConf->m_columns[i].m_aggregation.m_gameInput.m_inputId)
		{
			gnetAssertf(lbConf->m_columns[i].m_aggregation.m_type != AggType_Ratio, "Leaderboard %s, Input id=%s(%d), Invalid Aggregation type - RATIO.", lbConf->GetName(), input->GetName(), input->m_id);
			break;
		}
	}
#endif // __ASSERT

	m_InputValues[m_NumValues].Value   = inputValue;
	m_InputValues[m_NumValues].InputId = input->m_id;
	m_NumValues++;

	return true;
}

bool  
netLeaderboardWrite::ColumnExists(const unsigned inputId) const 
{
	bool  ret = false;

	for (unsigned i=0; i<m_NumValues && !ret; i++)
	{
		if (m_InputValues[m_NumValues].InputId == inputId)
		{
			ret = true;
			break;
		}
	}

	return ret;
}

}; // namespace

//eof
