//
// netObjectReassignMgr.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NET_OBJECT_REASSIGN_MGR_H
#define NET_OBJECT_REASSIGN_MGR_H

// rage headers
#include "net/transaction.h"

#include "fwnet/netbandwidthstats.h"
#include "fwnet/netlog.h"
#include "fwnet/netchannel.h"
#include "fwnet/netinterface.h"
#include "fwnet/nettypes.h"
#include "fwtl/pool.h"

namespace rage
{
class netObject;
class netObjectMgrBase;
class netPlayer;
class reassignNegotiateMsg;
class reassignConfirmMsg;

//PURPOSE
//  A class holding info on an orphaned object: its current reassign priority and which player is the current contender for its ownership
class reassignObjectInfo
{
public:
    FW_REGISTER_CLASS_POOL(reassignObjectInfo);

    //PURPOSE
    // Class constructor
    //PARAMS
    // object          - The network object associated with this instance of the class
    // initialPriority - The initial reassignment priority (how much the initial owner wants this object)
    // initialOwner    - The initial owner of this player (player with the highest reassignment priority)
    reassignObjectInfo(netObject &object, unsigned initialPriority, PhysicalPlayerIndex initialOwner);

    netObject          *GetObject()                                                 { return m_Object; }
    const netObject    *GetObject() const                                           { return m_Object; }
    PlayerFlags         GetPlayerFlags() const                                      { return m_PlayerFlags; }
    void                SetPlayerHasObject(PhysicalPlayerIndex player)              { m_PlayerFlags |= (1<<player); }
    void                ClearPlayerHasObject(PhysicalPlayerIndex player)            { m_PlayerFlags &= ~(1<<player); }
    bool                HasPlayerGotObject(PhysicalPlayerIndex player) const        { return (m_PlayerFlags & (1<<player)) != 0; }
    void                SetPlayerNeedsAnUpdate(PhysicalPlayerIndex player)          { m_UpdateFlags |= (1<<player); }
    bool                DoesPlayerNeedAnUpdate(PhysicalPlayerIndex player) const    { return (m_UpdateFlags & (1<<player)) != 0; }
    unsigned            GetOurReassignPriority() const                              { return m_LocalReassignPriority;   }
    unsigned            GetCurrentReassignPriority() const                          { return m_CurrentReassignPriority; }
    PhysicalPlayerIndex GetOwnerPlayer() const                                      { return m_OwnerPlayer; }
    void                SetOwnerPlayer(PhysicalPlayerIndex player)                  { m_OwnerPlayer = player; }
    bool                IsIncludedInNegotiation() const                             { return m_IncludedInNegotiation; }
    void                SetIncludedInNegotiation(bool included)                     { m_IncludedInNegotiation = included; }
    void                SetConfirmed()                                              { m_Confirmed = true; }
    bool                IsConfirmed() const                                         { return m_Confirmed; }
    void                SetToBeRemoved()                                            { m_ToBeRemoved = true; }
    bool                IsSetToBeRemoved() const                                    { return m_ToBeRemoved; }

    //PURPOSE
    // Called when there is a conflict over which player gets an object.
    // A player with a higher priority for this object wins it.
    // If the priorities are equal then highest player id wins.
    //PARAMS
    // contenderPlayer   - player contending for ownership of this object
    // contenderPriority - reassignment priority of the player contending for ownership
    void DecideOwnership(const netPlayer &contenderPlayer, unsigned newPriority);

    //PURPOSE
    // Reset the contents of the object info. This is typically done when a reassignment process
    // restarts, due to another player leaving while a reassignment is already in progress
    void Reset(PhysicalPlayerIndex localPlayerIndex)
    {
        m_PlayerFlags             = 0;
        m_UpdateFlags             = 0;
        m_OwnerPlayer             = localPlayerIndex;
        m_IncludedInNegotiation   = true;
        m_Confirmed               = false;
        m_CurrentReassignPriority = m_LocalReassignPriority;
    }

    inlist_node<reassignObjectInfo> m_ListLink; // Link node for using this class with the inlist class

private:

    reassignObjectInfo(const reassignObjectInfo&);

    netObject          *m_Object;                    // A pointer to the object being reassigned
    PlayerFlags         m_PlayerFlags;               // Flags indicating which other players also have this object
    PlayerFlags         m_UpdateFlags;               // Flags indicating which players need to be sent an update of important states (if ownership is taken locally)
    u8                  m_LocalReassignPriority;     // Local reassignment priority for this object
    u8                  m_CurrentReassignPriority;   // The current highest reassign priority for this object
    PhysicalPlayerIndex m_OwnerPlayer;               // The index of the player with the current highest reassignment priority for this object
    bool                m_IncludedInNegotiation : 1; // Indicates whether the local player should negotiate for control of this object
    bool                m_Confirmed             : 1; // Set once the player with the highest reassignment priority has accepted control of this object
    bool                m_ToBeRemoved           : 1; // Set if this object should be removed once the reassignment process completes
};

//PURPOSE
// A class holding the orphaned objects and reassign information for a player that has left.
class reassignmentProcessInfo
{
private:

    //PURPOSE
    // Reassignment states instances of the class can be in
    enum ReassignmentState
    {
        INACTIVE,       // Inactive; no reassignment in progress
        NEGOTIATING,    // Negotiating; in the process of determining which objects the different players involved know about
        CONFIRMING,     // Confirming; In the process of agreeing which players will take control of which objects
        FINISHING       // Finishing; In the process of reassigning the objects based on the confirmation of the players involved
    };

public:

    reassignmentProcessInfo();
    ~reassignmentProcessInfo();

    typedef inlist<reassignObjectInfo, &reassignObjectInfo::m_ListLink> ObjectList;

    netResponseHandler& GetResponseHandler(PhysicalPlayerIndex player) { gnetAssert(player < MAX_NUM_PHYSICAL_PLAYERS); return m_ResponseHandlers[player]; }

    ObjectList& GetObjectList()                                     { return m_ObjectList; }
    const ObjectList& GetObjectList() const                         { return m_ObjectList; }

    bool        IsActive() const                                    { return m_State != INACTIVE; }
    bool        IsNegotiating() const                               { return m_State == NEGOTIATING;}
    bool        IsConfirming() const                                { return m_State == CONFIRMING;}
    bool        IsFinishing() const                                 { return m_State == FINISHING;}
    void        SetConfirming()                                     { gnetAssert(m_State == NEGOTIATING); gnetAssert(m_PlayerMsg==0 && m_PlayerAck==0); m_State = CONFIRMING;}
    void        SetFinishing()                                      { gnetAssert(m_State == CONFIRMING); gnetAssert(m_PlayerMsg==0 && m_PlayerAck==0); m_State = FINISHING;}
    void        SetPlayerInvolved(PhysicalPlayerIndex player)       { m_PlayersInvolved |= (1<<player); }
    void        ClearPlayerInvolved(PhysicalPlayerIndex player);
    PlayerFlags GetPlayersInvolved() const                          { return m_PlayersInvolved; }
    bool        IsPlayerInvolved(PhysicalPlayerIndex player) const  { return (m_PlayersInvolved & (1<<player))!= 0; }
    void        SetWaitingOnMsg(PhysicalPlayerIndex player)         { m_PlayerMsg |= (1<<player); }
    bool        IsWaitingOnMsg(PhysicalPlayerIndex player) const    { return (m_PlayerMsg & (1<<player))!= 0; }
    bool        IsWaitingOnAnyMsg() const                           { return (m_PlayerMsg != 0); }
    void        SetGotMsg(PhysicalPlayerIndex player)               { gnetAssert(IsWaitingOnMsg(player)); m_PlayerMsg &= ~(1<<player); }
    void        SetWaitingOnAck(PhysicalPlayerIndex player)         { m_PlayerAck |= (1<<player); }
    bool        IsWaitingOnAck(PhysicalPlayerIndex player) const    { return (m_PlayerAck & (1<<player))!= 0; }
    bool        IsWaitingOnAnyAck() const                           { return (m_PlayerAck != 0); }
    void        SetGotAck(PhysicalPlayerIndex player)               { gnetAssert(IsWaitingOnAck(player)); m_PlayerAck &= ~(1<<player); }
    unsigned    GetRestartNumber() const                            { return static_cast<unsigned>(m_RestartNum);}
    u32		    GetRestartNumberForTelemetry() const                { return m_RestartNumForTelemetry;}
	u32         GetNegotiationStartTime() const                     { return m_NegotiationStartTime; }

    //PURPOSE
    // Returns the peer ID of the player last leaving the session involved in the reassignment process
    u64 GetLastLeavingPeerID() const;

    //PURPOSE
    // Returns the name of the leaving peer with the specified peer ID
    //PARAMS
    // peerID - The peer ID of a player that has left the session
    const char *GetLeavingPeerName(u64 peerID) const;

    //PURPOSE
    // Returns the leaving peers involved in this reassignment process to negotiate objects for with
    // the specified player.
    //PARAMS
    // targetPlayer    - Player to negotiate with
    // leavingPeers    - Array to store the leaving peers to negotiate objects with
    // numLeavingPeers - The number of elements returned in the leaving peers array
    void GetLeavingPeersToNegotiateFor(PhysicalPlayerIndex targetPlayer, u64 *leavingPeers, unsigned &numLeavingPeers);

    //PURPOSE
    // Returns whether we are currently reassigning for this peer
    //PARAMS
    // peerID - The peer to check
    bool IsReassigningForPeerID(u64 peerID) const;

    //PURPOSE
    // Marks that we are awaiting a negotiation response from the specified player,
    // for the reassignment process related to the specified peer
    //PARAMS
    // player - Player we are awaiting a response from
    // peerID - The peer ID of the player that left the session for this reassignment process
    void SetWaitingOnNegResponse(PhysicalPlayerIndex player, u64 peerID);

    //PURPOSE
    // Marks that we are awaiting a negotiation response from the specified player,
    // for the reassignment process related to the specified peer
    //PARAMS
    // player - Player we are awaiting a response from
    // peerID - The peer ID of the player that left the session for this reassignment process
    void ClearWaitingOnNegResponse(PhysicalPlayerIndex player, u64 peerID);

    //PURPOSE
    // Returns whether we are waiting on a negotiation response for the specified peer
    //PARAMS
    // player - Player to check if we are awaiting a response from
    // peerID - The peer ID of the player that left the session for this reassignment process
    bool IsWaitingOnNegResponse(PhysicalPlayerIndex player, u64 peerID);

    //PURPOSE
    // Returns whether we are waiting on a negotiation response for the specified peer
    //PARAMS
    // peerID - The peer ID of the player that left the session for this reassignment process
    bool IsWaitingOnNegResponse(u64 peerID);

    //PURPOSE
    // Returns whether we are waiting on a negotiation response for any peer
    bool IsWaitingOnAnyNegResponse();

    //PURPOSE
    // Returns whether we have processed negotiation data from the specified player
    // for the specified leaving peer
    //PARAMS
    // player - The player we may have negotiated with
    // peerID - The peer ID of a player that has previous left the session
    bool HasProcessedNegotiationData(PhysicalPlayerIndex player, u64 peerID);

    //PURPOSE
    // Marks that we have processed negotiation data from the specified player
    // for the specified leaving peer
    //PARAMS
    // player - The player we have negotiated with
    // peerID - The peer ID of a player that has previous left the session
    void MarkProcessedNegotiationData(PhysicalPlayerIndex player, u64 peerID);

    //PURPOSE
    // Sets whether the specified player knows any of the players that has left the session that
    // are included in this reassignment process
    //PARAMS
    // player  - The player to check
    bool HasAnyPeer(PhysicalPlayerIndex player) const;

    //PURPOSE
    // Sets whether the specified player knows about the peerID of a player that has left the session
    //PARAMS
    // player  - The player to flag whether they know about the peer
    // peerID  - The peer ID of a player that has left the session
    // hasPeer - Flag indicating whether the player knows about the peer
    void SetHasPeer(PhysicalPlayerIndex player, u64 peerID, bool hasPeer);

#if ENABLE_NETWORK_LOGGING
    //PURPOSE
    // Logs the current state of the reasignment process
    void LogReassignmentState(netLoggingInterface &log);
#endif // ENABLE_NETWORK_LOGGING

    //PURPOSE
    //  Adds an orphaned object to the reassignment process object list
    //PARAMS
    // objectInfo - reassignment info for an object to add to the reassignment process
    void AddObjectInfo(reassignObjectInfo &objectInfo);

    //PURPOSE
    //  Removes an orphaned object from the reassignment process object list
    //PARAMS
    // objectInfo - reassignment info for an object to remove from the reassignment process
    void RemoveInfo(reassignObjectInfo& objectInfo);

    //PURPOSE
    // Finds an orphaned object on the object list for the reassignment process
    //PARAMS
    // objectId - ID of the object to find the info for from the reassignment process
    reassignObjectInfo *FindInfo(const ObjectId objectId);

    //PURPOSE
    // Starts a reassignment process
    //PARAMS
    // leavingPeerID   - The peer ID of the gamer to reassign objects for
    // leavingPeerName - The name of the gamer to reassign objects for
    void Start(u64 leavingPeerID, const char *LOGGING_ONLY(leavingPeerName));

    // PURPOSE
    //  Finishes a reassignment process
    void Finish();

    //PURPOSE
    // Restarts the current reassignment process
    //PARAMS
    // leavingPeerID   - The peer ID of the gamer to reassign objects for
    // leavingPeerName - The name of the gamer to reassign objects for
    void Restart(u64 leavingPeerID, const char *LOGGING_ONLY(leavingPeerName));

    //PURPOSE
    // Marks all of the objects in the reassignment process as confirmed
    void MarkAllObjectsConfirmed();

    //PURPOSE
    // Marks any objects that only the local machine knows about as confirmed.
    // These objects don't need to be confirmed with remote machines.
    void MarkLocalOnlyObjectsConfirmed();

#if __BANK
    //PURPOSE
    // Displays debug text describing the current object reassignment state
    void DisplayDebugInfo();
#endif // __BANK

private:

#if ENABLE_NETWORK_LOGGING
    //PURPOSE
    // Returns a string describing the current reassignment state
    const char *GetReassignmentStateName();

    static const unsigned MAX_LEAVING_PEER_NAME = 128;
#endif // ENABLE_NETWORK_LOGGING

    //PURPOSE
    // Keeps track of which players we are awaiting on a negotiation response from for a player
    // that has left the session
    struct NegotiationData
    {
        PlayerFlags m_PlayerHasPeer;       // Which players know about this peer (so can include this peer in the reassignment process)
        PlayerFlags m_PlayerNegResponse;   // Which reassigning players we are waiting for a negotiate response from
        PlayerFlags m_HasProcessedNegData; // Which reassigning players have we processed negotiation data for
        u64         m_LeavingPlayerPeerID; // The Peer ID of a player who has left the game and is part of a reassignment process
#if ENABLE_NETWORK_LOGGING
        char        m_LeavingPeerName[MAX_LEAVING_PEER_NAME];
#endif // ENABLE_NETWORK_LOGGING
    };

    reassignmentProcessInfo(const reassignmentProcessInfo&);
    reassignmentProcessInfo& operator=(const reassignmentProcessInfo&);

    atFixedArray<netResponseHandler, MAX_NUM_PHYSICAL_PLAYERS> m_ResponseHandlers; // Response handlers for each other player

    ObjectList        m_ObjectList;            // The objects the player owned
    ReassignmentState m_State;                 // NONE / NEGOTIATING / CONFIRMING / FINISHING
    PlayerFlags       m_PlayersInvolved;       // The players involved in this reassignment negotiation
    PlayerFlags       m_PlayerMsg;             // Flags for each player set when we are waiting for a reassign msg
    PlayerFlags       m_PlayerAck;             // Flags for each player set when we are waiting for an ack for one of our reassign msgs
	u8                m_RestartNum;            // The number of times the reassignment has been restarted
	u32               m_RestartNumForTelemetry;// The number of times the reassignment has been restarted, used for telemetry purposes
	unsigned          m_LastUsedReassignIndex; // last used index in the m_PlayersToReassign array. This can wrap if too many players leave for the same process
	u32               m_NegotiationStartTime;  // The time in milliseconds when the negotiation has last started

    // keeps track of negotiation state for players that have left the session
    NegotiationData m_PlayerNegotiationData[MAX_NUM_PHYSICAL_PLAYERS];
};

//PURPOSE
// When a player leaves, any game objects they were in control of need to be reassigned to other players in the session. This class manages this
// object reassignment process. Once a player leaves, the remaining players in the session send each other a negotiation message describing
// the objects the leaving player was controlling along with a reassignment priority, which is a measure of how much the player wants to
// take control of the object. Once all of the players have sent and received the negotiation messages they can decide who should take control
// of the orphaned objects be comparing the priorities. Once this process is complete each player sends a confirmation message to the other players
// confirming who the objects will be reassigned to.
class netObjectReassignMgr
{
public:

    netObjectReassignMgr();
    ~netObjectReassignMgr();

    //PURPOSE
    // Initialises the reassignment manager at the start of a network session
    //PARAMS
    // objectMgr - The object manager for the network session
    void Init(netObjectMgrBase &objectMgr);

    //PURPOSE
    // Shuts down the reassignment manager at the end of a network session
    void Shutdown();

    //PURPOSE
    // Updates the reassignment manager (should be called once per frame)
    void Update();

    netLoggingInterface& GetLog() { return *m_Log;}

    //PURPOSE
    // Finds the object with the specified id if it is part of an active reassignment process
    //PARAMS
    // objectId - ID of the object to retrieve
    netObject* FindNetworkObject(const ObjectId objectId);

    //PURPOSE
    // Called when a player joins the session
    //PARAMS
    // player - The player joining the session
    void PlayerHasJoined(const netPlayer &player);

    //PURPOSE
    // Called when a player leaves the session
    //PARAMS
    // player - The player leaving the session
    void PlayerHasLeft(const netPlayer &player);

    //PURPOSE
    // Returns true if there is an object reassignment process active
    bool IsReassignmentInProgress() const;
	static bool IsReassignmentInProgress_Cached() { return sm_IsReassignmentInProgress; }

	// PURPOSE
	// Returns the number of restart
	unsigned GetRestartNumber() const { return m_ReassignmentInfo.GetRestartNumber(); }

	// PURPOSE
	// Returns the number of restart, used for telemetry
	u32 GetRestartNumberForTelemetry() const { return m_ReassignmentInfo.GetRestartNumberForTelemetry(); }

	// PURPOSE
	// Returns the start time of the negotiations in milliseconds 
	u32 GetNegotiationStartTime() const { return m_ReassignmentInfo.GetNegotiationStartTime(); }

	// PURPOSE
	// Returns how long the negotiation have been running for (in milliseconds)
	u32 GetReassignmentRunningTimeMs() const;

	// PURPOSE
	// Returns how many players are involved in the reassignment
	u32 GetReassignmentPlayersInvolvedCount() const;

    //PURPOSE
    // Returns the number of objects currently part of an active object reassignment process
    //PARAMS
    // includePredicate - Predicate function to determine whether to include a reassigning object in the count
    unsigned GetNumObjectsBeingReassigned(fnNetObjectPredicate includePredicate = 0) const;

    //PURPOSE
    // Returns true if the given object is currently part of an active object reassignment process
    //PARAMS
    // networkObject - The network object to check
    bool IsObjectOnReassignmentList(netObject &networkObject) const;

    //PURPOSE
    // Fills an array with all of the network objects currently part of an active object reassignment process
    //RETURNS
    // The number of objects in the array
    //PARAMS
    //  objectArray       - the array to store the objects
    //  sizeofArray       - the size of the array
    //  scriptObjectsOnly - determines whether other script objects should be added to the array
    unsigned int GetAllObjectsBeingReassigned(netObject **objectArray, unsigned int sizeofArray, bool scriptObjectsOnly);

    //PURPOSE
    // Removes the given object from a currently active object reassignment process
    //PARAMS
    // object - The object to remove
    void RemoveObject(netObject &object);

    //PURPOSE
    // Returns the name of the leaving peer with the specified peer ID
    //PARAMS
    // peerID - The peer ID of a player that has left the session
    const char *GetLeavingPeerName(u64 peerID) const;

	//PURPOSE
	// Adds an object onto the reassign list from a player list in the object manager
	//PARAMS
	// networkObject - the object to add to the list.
	void  AddObjectToReassignList(netObject& networkObject);


	//PURPOSE
	// Sets a callback to be called when an object is removed from the reassign list.
	//PARAMS
	// objectRemovedCb - the callback.
	typedef void(*fnTelemetryRemovedCallback)();
	void SetTelemetryCallback(fnTelemetryRemovedCallback objectRemovedCb);

#if __BANK
    //PURPOSE
    // Adds debug widgets for testing the object reassignment code
    static void AddDebugWidgets();

    //PURPOSE
    // Displays debug text describing the current object reassignment state
    void DisplayDebugInfo();

    //PURPOSE
    // Returns the number of reassign negotiate messages sent since the last call to ResetMessageCounts()
    unsigned GetNumReassignNegotiateMessagesSent() const { return m_NumReassignNegotiateMessagesSent; }

    //PURPOSE
    // Returns the number of reassign confirm messages sent since the last call to ResetMessageCounts()
    unsigned GetNumReassignConfirmMessagesSent()   const { return m_NumReassignConfirmMessagesSent;   }

    //PURPOSE
    // Returns the number of reassign response messages sent since the last call to ResetMessageCounts()
    unsigned GetNumReassignResponseMessagesSent()  const { return m_NumReassignResponseMessagesSent;  }

    //PURPOSE
    // Reset the counts of object reassignment manager messages sent
    void ResetMessageCounts() { m_NumReassignNegotiateMessagesSent = m_NumReassignConfirmMessagesSent = m_NumReassignResponseMessagesSent = 0; }

	netBandwidthStatistics& GetBandwidthStatistics() { return m_BandwidthStats; }

#endif // __BANK

private:

	//PURPOSE
    // Finds the reassignment info for an object with the specified id if it is part of an active reassignment process
    //PARAMS
    // objectId - ID of the object to retrieve info for
    reassignObjectInfo* FindObjectInfo(const ObjectId objectId);

    //PURPOSE
    // Destroys any objects owned by a leaving player that do not need to be reassigned
    // to other players
    //PARAMS
    // leavingPlayerIndex - physical player index of the leaving player
    void DestroyObjectsNotToBeReassigned(PhysicalPlayerIndex leavingPlayerIndex);

    //PURPOSE
    // Adds an object onto the reassign list from a player list in the object manager
    //PARAMS
    // object           - the object to add to the list
    // reassignPriority - the reassignment priority calculated for this object by the current leading player candidate
    // leadingCandidate - the player who is currently the leading candidate to own the object when it is reassigned
    //RETURNS
    //  The reassignment info created to help manage the objects reassignment
    reassignObjectInfo *AddObjectToReassignList(netObject       &object,
                                                unsigned         reassignPriority,
                                                const netPlayer &leadingCandidate);

    //PURPOSE
    // Removes an object from the reassign list to its new owner's list in the object manager
    //PARAMS
    // objectInfo - the reassignment info for the object being reassigned
    void RemoveObjectFromReassignList(reassignObjectInfo &objectInfo);

    //PURPOSE
    // Starts the object reassignment process for the objects that were controlled by the leaving player
    //PARAMS
    // leavingPlayer - The player leaving the session
    void StartReassignNegotiation(const netPlayer &leavingPlayer);

    //PURPOSE
    // Restarts the object reassignment process. This occurs when another player leaves while an
    // object reassignment is already in progress
    //PARAMS
    // rlLeavingPeerID - peer ID of the peer leaving the session
    // leavingPeerName - name of the peer leaving the session
    void RestartReassignNegotiation(u64 rlLeavingPeerID, const char *leavingPeerName);

    //PURPOSE
    // Starts the confirmation stage of the object reassignment process
    void StartReassignConfirmation();

    //PURPOSE
    // Reassigns the orphaned objects after the negotiation and confirmation stages of the
    // object reassignment process are complete
    void ReassignObjects();

    //PURPOSE
    // Processes a reassignment message request received over the network, mediated via a netTransactor
    //PARAMS
    // player   - the player the message was received from
    // request - a netTransactor request, containing a reassignment message
    void HandleReassignRequest(netPlayer        *player,
                               const netRequest *request);

    //PURPOSE
    //  Processes a reassignment message response received over the network, mediated via a netTransactor
    //PARAMS
    //  transactor - the transactor
    //  handler    - the response handler for the player we received the message from
    //  response   - a netTransactor response, containing a reassignment message
    void HandleReassignResponse(netTransactor      *transactor,
                                netResponseHandler *handler,
                                const netResponse  *response);

    //PURPOSE
    // Handles a negotiation message sent from another player
    //PARAMS
    // negotiateMsg  - The negotiate message
    // sendingPlayer - The player sending the message
    // request       - A netTransactor request, containing a reassignment message
    void HandleNegotiateMsg(reassignNegotiateMsg &negotiateMsg, const netPlayer &sendingPlayer, const netRequest *request);

    //PURPOSE
    // Handles a confirmation message sent from another player
    //PARAMS
    // confirmMsg - The confirmation message
    // player     - The player sending the message
    // request       - A netTransactor request, containing a reassignment message
    void HandleConfirmMsg(reassignConfirmMsg &confirmMsg, const netPlayer &player, const netRequest *request);

    //PURPOSE
    // Prepares for starting the negotiation process. This involves calculating whether a
    // negotiation stage is needed and if so deciding which players will be involved in the process
    //PARAMS
    // leavingPlayer - The player leaving that we will be negotiating for
    void PrepareForNegotiation(const netPlayer *leavingPlayer);

    //PURPOSE
    // Processes negotiation data received from another player
    //PARAMS
    // confirmMsg - The negotiation message
    // player     - The player the message was received from
    void ProcessNegotiationData(reassignNegotiateMsg &negotiateMsg, const netPlayer &player);

    //PURPOSE
    // Negotiates for control of an object on our local reassignment list with a remote player
    //PARAMS
    // objectInfo             - Describes the object being negotiated for
    // remoteReassignPriority - The remote player's reassign priority for this object
    // remotePlayer           - The remote player we are negotiating with
    void NegotiateForKnownObject(reassignObjectInfo &objectInfo, unsigned remoteReassignPriority, const netPlayer &remotePlayer);

    //PURPOSE
    // Negotiates for control of an object not on our local reassignment list with a remote player
    //PARAMS
    // objectID               - ID of the object being negotiated for
    // remoteReassignPriority - The remote player's reassign priority for this object
    // remotePlayer           - The remote player we are negotiating with
    void NegotiateForUnknownObject(ObjectId objectID, unsigned remoteReassignPriority, const netPlayer &remotePlayer);

    //PURPOSE
    // Handles a confirmation data received from another player
    //PARAMS
    // confirmMsg - The confirmation message
    // player     - The player sending the message
    void ProcessConfirmationData(reassignConfirmMsg &confirmMsg, const netPlayer &player);

    //PURPOSE
    // Confirms the reassignment of an object to the specified remote player
    //PARAMS
    // objectID     - ID of the object to confirm
    // remotePlayer - The remote player we are confirming with
    bool ConfirmRemoteObjectWithRemotePlayer(ObjectId objectID, const netPlayer &remotePlayer);

    //PURPOSE
    // Confirms the reassignment of an object to the local player
    //PARAMS
    // objectID     - ID of the object to confirm
    // checksum     - checksum of important state data for the object
    // remotePlayer - The remote player we are confirming with
    bool ConfirmLocalObjectWithRemotePlayer(ObjectId objectID, unsigned checksum, const netPlayer &remotePlayer);

    //PURPOSE
    // Sends a negotiation message to one of more players involved in the reassignment process
    //PARAMS
    // destinationPlayer - If specified, the message is only sent to the specified player, otherwise
    //                     the message is sent to all players involved in the reassignment process
    void SendNegotiateMsg(const netPlayer *destinationPlayer = 0);

    //PURPOSE
    // Sends a confirmation message to one of more players involved in the reassignment process
    //PARAMS
    // destinationPlayer - If specified, the message is only sent to the specified player, otherwise
    //                     the message is sent to all players involved in the reassignment process
    void SendConfirmMsg(const netPlayer *destinationPlayer = 0);

    //PURPOSE
    // Calculates a checksum of a network object's important state data, in relation to the specified player
    //PARAMS
    // networkObject - The network object to checksum
    // playerIndex   - Index of the player the checksum is for
    unsigned GetImportantStateChecksum(netObject &networkObject, PhysicalPlayerIndex playerIndex) const;

    //PURPOSE
    // Type of message pending to be sent to another player
    enum PendingMessageType
    {
        NO_MESSAGE,
        NEGOTIATE_MESSAGE,
        CONFIRM_MESSAGE
    };

    //PURPOSE
    // This struct contains information about when to send a delayed message to a player
    struct PlayerPendingMessageInfo
    {
        unsigned           m_TimeToSend;  // time to send the message in system time
        PendingMessageType m_MessageType; // type of message to send (negotiate or confirm);
    };

    netObjectMgrBase           *m_ObjectMgr;         // The object manager for the network game
    reassignmentProcessInfo     m_ReassignmentInfo;  // Information about the current reassignment, if one is in progress
    NetworkPlayerRequestHandler m_MessageHandler;    // Handles transaction requests from players
    netLog                     *m_Log;               // Logging object for this class
    bool                        m_IsInitialised : 1; // Set when the manager is initialised

    // pending messages to be sent to other players
    PlayerPendingMessageInfo    m_PendingMessageInfo[MAX_NUM_PHYSICAL_PLAYERS];

	// Callback called after an object has been removed from the list.
	fnTelemetryRemovedCallback m_ObjectRemovedCb;

	static bool sm_IsReassignmentInProgress;

#if __BANK
    unsigned m_NumReassignNegotiateMessagesSent; // the number of reassign negotiate messages sent since the last call to ResetMessageCounts()
    unsigned m_NumReassignConfirmMessagesSent;   // the number of reassign confirm messages sent since the last call to ResetMessageCounts()
    unsigned m_NumReassignResponseMessagesSent;  // the number of reassign response messages sent since the last call to ResetMessageCounts()

    netBandwidthStatistics m_BandwidthStats;

    // bandwidth recorders for the different message types
    RecorderId m_NegotiateMsgRecorderId;
    RecorderId m_ConfirmMsgRecorderId;
    RecorderId m_ResponseMsgRecorderId;

    // debug widgets for reproducing test cases easily
    static bool sm_RejectAllNegotiationMessages;
    static bool sm_RejectAllConfirmationMessages;
    static bool sm_RejectIgnoreLeavingPlayers;
#endif

};

} // namespace rage

#endif  // NET_OBJECT_REASSIGN_MGR_H
