//===========================================================================
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.             
//===========================================================================
#include "netcloudfiledownloadhelper.h"
#include "diag/channel.h"
#include "net/http.h"
#include "rline/cloud/rlcloud.h"
#include "rline/rl.h"
#include "rline/ros/rlros.h"
#include "rline/ugc/rlugc.h"
#include "system/timer.h"

RAGE_DECLARE_CHANNEL(net);
RAGE_DEFINE_SUBCHANNEL(net, cloudRequest);
#undef __rage_channel
#define __rage_channel net_cloudRequest

static int s_LastRequestId = 0;

namespace rage
{
netCloudRequestMemPolicy netCloudRequestMemPolicy::NULL_POLICY;

netCloudRequestHelper::netCloudRequestHelper(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
	: m_State(kState_Idle)
	, m_MaxDownloadRetries(0)
	, m_NumDownloadRetries(0)
	, m_RetryOnFail(false)
    , m_SecurityFlags(RLROS_SECURITY_DEFAULT)
	, m_MaxPrepareMemoryRetries(PREPARE_ATTEMPTS)
	, m_NumPrepareMemoryRetries(0)
	, m_PrepareMemoryRetryTimestamp(0)
	, m_FailedDueToOutOfMemory(false)
{
	Init(szCloudFilePath, uModifiedTimestamp, httpOptions, cbFinished, rMemPolicy);
}

void netCloudRequestHelper::Init(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	if(rageVerifyf(strlen(szCloudFilePath) > 0, "Invalid cloud file path specified!"))
	{
		m_CloudFilePath = szCloudFilePath;
		m_ModifiedTimestamp = uModifiedTimestamp;
		m_HttpOptions = httpOptions;
		m_cbFinished = cbFinished;
		m_MemPolicy = rMemPolicy;

		m_NumDownloadRetries = 0;
		m_RetryOnFail = false;
		m_NumPrepareMemoryRetries = 0;
		m_PrepareMemoryRetryTimestamp = 0;
		m_FailedDueToOutOfMemory = false;
		m_RequestId = s_LastRequestId++;

		rageDebugf1("%s [%d] Init :: Path: %s", GetRequestName(), m_RequestId, szCloudFilePath);
	}
}

netCloudRequestHelper::~netCloudRequestHelper()
{
    Cancel();
	Reset();
}

void netCloudRequestHelper::Start()
{
	if(rageVerifyf(m_State == kState_Idle, "Starting from invalid state!"))
	{
        rageDebugf1("%s [%d] Start", GetRequestName(), m_RequestId);
		m_NumDownloadRetries = 0;
		m_RetryOnFail = false;
		m_NumPrepareMemoryRetries = 0;
		m_PrepareMemoryRetryTimestamp = 0;
		m_FailedDueToOutOfMemory = false;
		m_State = kState_Waiting;
	}
}

void netCloudRequestHelper::Reset()
{
    CleanUpRequest();
	m_State = kState_Idle;
}

void netCloudRequestHelper::Update()
{
	switch(m_State)
	{
	case kState_Idle:
		break;
	case kState_Waiting:
		{
			// can't start - bail
			if(!CanStartRequest())
				return;
#if !__NO_OUTPUT
			m_RequestStartedTimestamp = sysTimer::GetSystemMsTime();
			rageDebugf1("%s [%d] Starting. Time: %d", GetRequestName(), m_RequestId, m_RequestStartedTimestamp);
#endif
			m_State = kState_Start;
		}
		break;
	case kState_Start:
		{
			m_CloudFileStatus.Reset();

			//Start out in the finished state.  We'll set the state depending on how we do in the next couple of lines.
			m_State = kState_Finish;

			//Prepare our memory a appropriate.  Depending on the allocator, we may need to try a couple times to allocate
			if(!PrepareMemory())
			{
				rageDebugf1("%s [%d] Failed to prepare memory [%d/%d]", GetRequestName(), m_RequestId, m_NumPrepareMemoryRetries, m_MaxPrepareMemoryRetries);
                if(m_NumPrepareMemoryRetries < m_MaxPrepareMemoryRetries)
				{
					m_State = kState_PrepareMemoryRetryWait;
					m_PrepareMemoryRetryTimestamp = sysTimer::GetSystemMsTime();
					m_NumPrepareMemoryRetries++;
				}
			}
			//Otherwise, if our memory is good to go, start the request.
			else if(DoRequest())
			{
				rageDebugf1("%s [%d] Started", GetRequestName(), m_RequestId);
				m_State = kState_Processing;
			}
			
			//We'll still be in the finish state if something failed 
			if(m_State == kState_Finish)
			{
				rageDebugf1("%s [%d] Failed to start", GetRequestName(), m_RequestId);
                CleanUpRequest();
				
				m_CloudFileStatus.SetPending();
				m_CloudFileStatus.SetFailed();

				// fire callback
				m_cbFinished.Call(this);
			}
		}
		break;
	case kState_PrepareMemoryRetryWait:
		{
			u32 nCurrentTime = sysTimer::GetSystemMsTime();
			if((nCurrentTime - m_PrepareMemoryRetryTimestamp) > PREPARE_WAIT_TIME)
			{
				rageDebugf1("%s [%d] Waited %dms. Retrying memory request", GetRequestName(), m_RequestId, PREPARE_WAIT_TIME);
				m_State = kState_Start;
			}
		}
		break;
	case kState_Processing:
		{
			// check if the request has completed
			if(!m_CloudFileStatus.Pending())
			{
				// if we didn't succeed and we should retry (and the result was not that the file wasn't found)...
				if(!m_CloudFileStatus.Succeeded() && (m_CloudFileStatus.GetResultCode() != NET_HTTPSTATUS_NOT_FOUND) && ((m_NumDownloadRetries < m_MaxDownloadRetries) || m_RetryOnFail))
				{
					// move back to the waiting state - this will begin the request again
					m_State = kState_Waiting;

					// increment retries and reset retry variable
					m_NumDownloadRetries++;
					m_RetryOnFail = false; 

					// out of memory error
					m_FailedDueToOutOfMemory = (m_CloudFileStatus.GetResultCode() == RL_CLOUD_ERROR_OUT_OF_MEMORY);

					// log
					rageDebugf1("%s [%d] Request failed. Error: %d. Retrying [%d/%d]", GetRequestName(), m_RequestId, m_CloudFileStatus.GetResultCode(), m_NumDownloadRetries, m_MaxDownloadRetries);
				}
				else
				{
#if !__NO_OUTPUT
					// add logging
					if(m_CloudFileStatus.Succeeded())
						rageDebugf1("%s [%d] Request succeeded. Time: %d", GetRequestName(), m_RequestId, sysTimer::GetSystemMsTime() - m_RequestStartedTimestamp);
                    else
						rageDebugf1("%s [%d] Request failed. Error: %d, Time: %d", GetRequestName(), m_RequestId, m_CloudFileStatus.GetResultCode(), sysTimer::GetSystemMsTime() - m_RequestStartedTimestamp);				
#endif
					// Move to finished state and fire callback (which maybe will reset our state)
					m_State = kState_Finish;
					m_cbFinished.Call(this);
				}
			}
		}
		break;
	case kState_Finish:
		break;
	}
}

void netCloudRequestHelper::Cancel()
{
    if(m_State == kState_Idle)
        return;

    if(m_State == kState_Processing)   
	{
        // cancel pending requests
        if(m_CloudFileStatus.Pending())
            rlCloud::Cancel(&m_CloudFileStatus);
        else
        {
            // force cancel
            m_CloudFileStatus.SetPending();
            m_CloudFileStatus.SetCanceled();
        }
	}

	// dependents need to know
	if(m_State != kState_Finish)
	{
		// add logging
		rageDebugf1("%s [%d] Cancel. Time: %d", GetRequestName(), m_RequestId, (m_RequestStartedTimestamp > 0) ? sysTimer::GetSystemMsTime() - m_RequestStartedTimestamp : 0);

		m_State = kState_Finish;
		m_cbFinished.Call(this);
	}
}

void netCloudRequestHelper::FlagRetry()
{
	// this is a one-time retry on failure
	m_RetryOnFail = true;
}

void netCloudRequestHelper::SetNumRetries(const unsigned numRetries)
{
	// number of times we should retry on download failure
	if(m_MaxDownloadRetries != numRetries)
	{
		rageDebugf1("%s [%d] SetNumRetries :: %u -> %u", GetRequestName(), m_RequestId, m_MaxDownloadRetries, numRetries);
		m_MaxDownloadRetries = numRetries;
	}
}

void netCloudRequestHelper::MarkAsCritical()
{
	rageDebugf1("%s [%d] MarkAsCritical :: %u retries", GetRequestName(), m_RequestId, PREPARE_ATTEMPTS_CRITICAL_FILE);
	m_MaxPrepareMemoryRetries = PREPARE_ATTEMPTS_CRITICAL_FILE;
}

rlCloudFileInfo* netCloudRequestHelper::GetFileInfo()
{
	if(HasFileInfo())
		return &m_CloudFileInfo;
	
	return nullptr;
}

bool netCloudRequestHelper::AreROSCredentialsValid(const int localGamerIndex) const
{
	if(localGamerIndex >= 0)
	{
		if(!RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))
			return false;

		const rlRosCredentials& creds = rlRos::GetCredentials(localGamerIndex);
		return creds.IsValid();
	}
	else
	{
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS; ++i)
		{
			const rlRosCredentials& creds = rlRos::GetCredentials(i);
			if(creds.IsValid())
				return true;
		}
		return false;
	}
}

#if !__NO_OUTPUT
const char* GetNullAllocatorPolicyAsString(const NullAllocatorPolicy policy)
{
	static const char* s_Strings[] =
	{
		"Policy_UseNull",
		"Policy_UseRline",
		"Policy_FailRequest",
	};
	CompileTimeAssert(COUNTOF(s_Strings) == NullAllocatorPolicy::Policy_Num);


	return ((policy >= NullAllocatorPolicy::Policy_UseNull) && (policy < NullAllocatorPolicy::Policy_Num)) ? s_Strings[policy] : "Policy_Invalid";
}
#endif

NullAllocatorPolicy netCloudRequestGetFile::sm_NullAllocatorPolicy = NullAllocatorPolicy::Policy_UseRline;

void netCloudRequestGetFile::SetNullAllocatorPolicy(const NullAllocatorPolicy policy)
{
	if(sm_NullAllocatorPolicy != policy)
	{
		rageDebugf1("SetNullAllocatorPolicy :: %s -> %s", GetNullAllocatorPolicyAsString(sm_NullAllocatorPolicy), GetNullAllocatorPolicyAsString(policy));
		sm_NullAllocatorPolicy = policy;
	}
}

netCloudRequestGetFile::netCloudRequestGetFile(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

void netCloudRequestGetFile::Init(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	m_MemoryPresize = uPresize;
	netCloudRequestHelper::Init(szCloudFilePath, uModifiedTimestamp, httpOptions, cbFinished, rMemPolicy);
}

void netCloudRequestGetFile::CleanUpRequest()
{
	rageDebugf1("%s [%d] CleanUpRequest. Memory: %d", GetRequestName(), m_RequestId, m_GB.GetCapacity());
	
    m_GB.Clear();
	m_NumPrepareMemoryRetries = 0;
}

bool netCloudRequestGetFile::TryAllocate(sysMemAllocator* allocator OUTPUT_ONLY(, const char* szAllocatorName))
{
	// initialise grow buffer with primary allocator
	m_GB.Init(allocator, datGrowBuffer::NULL_TERMINATE, m_MemPolicy.m_GrowBufferGrowIncrement);

	// check if we have a presize
	if(m_MemoryPresize > 0)
	{
		if(m_GB.Preallocate(m_MemoryPresize))
		{
			rageDebugf1("%s [%d] PrepareMemory :: Allocated: %u, Allocator: %s", GetRequestName(), m_RequestId, m_MemoryPresize, szAllocatorName);
		}
		else
		{
			rageDebugf1("%s [%d] PrepareMemory. Failed to allocate %db, Allocator: %s", GetRequestName(), m_RequestId, m_MemoryPresize, szAllocatorName);
			return false;
		}
	}

	// either no allocation to make or it was successful
	return true; 
}

bool netCloudRequestGetFile::PrepareMemory()
{
	// detect if a previous request ran out of memory and add some additional memory (with retry structure)
	if(m_FailedDueToOutOfMemory)
	{
		// additional memory for an out of memory failure		
		if(m_CloudFileInfo.m_ContentLength > 0 && m_CloudFileInfo.m_ContentLength > m_MemoryPresize)
		{
			rageDebugf1("%s [%d] PrepareMemory :: Failed - Capacity: %u, Required: %u", GetRequestName(), m_RequestId, m_GB.GetCapacity(), m_CloudFileInfo.m_ContentLength);
			m_MemoryPresize = m_CloudFileInfo.m_ContentLength;
		}
		else
		{
			static const float ADDITIONAL_MEMORY_RATIO = 0.2f;  // 20%
			m_MemoryPresize += static_cast<unsigned>(ADDITIONAL_MEMORY_RATIO * static_cast<float>(m_MemoryPresize));
			rageDebugf1("%s [%d] PrepareMemory :: Failed - Capacity: %u, No FileInfo, Bumping to: %u", GetRequestName(), m_RequestId, m_GB.GetCapacity(), m_MemoryPresize);
		}

		// wipe our previous allocation
		m_GB.Clear();

		// remove this flag
		m_FailedDueToOutOfMemory = false;
	}

	// if we already have the requested amount, move on
	if(m_MemoryPresize > 0 && m_GB.GetCapacity() >= m_MemoryPresize)
	{
		rageDebugf1("%s [%d] PrepareMemory :: Already Prepared - Capacity: %u, Required: %u", GetRequestName(), m_RequestId, m_GB.GetCapacity(), m_MemoryPresize);
		m_GB.Reset();
		return true;
	}

	OUTPUT_ONLY(const char* allocatorName = "Primary");

	// check whether we have a valid allocator (just check the main allocator, the fallback will only be used if not null)
	sysMemAllocator* allocator = m_MemPolicy.m_pGrowBufferAllocator;
	if(!allocator)
	{
		switch(sm_NullAllocatorPolicy)
		{
		case NullAllocatorPolicy::Policy_UseNull:
			rageDebugf1("%s [%d] PrepareMemory :: No allocator provided, using null allocator!", GetRequestName(), m_RequestId);
			OUTPUT_ONLY(allocatorName = "Null");
			break;

		case NullAllocatorPolicy::Policy_UseRline:
			rageDebugf1("%s [%d] PrepareMemory :: No allocator provided, using rline allocator!", GetRequestName(), m_RequestId);
			allocator = rlGetAllocator();
			OUTPUT_ONLY(allocatorName = "Rline");
			break;

		default:
			// assert and fall through to the fail request policy
			rageAssertf(0, "%s [%d] PrepareMemory :: Unknown null allocator policy!", GetRequestName(), m_RequestId);

		case NullAllocatorPolicy::Policy_FailRequest:
			// we can fail the task by just maxing our retries so that it moves to a finished state
			m_NumPrepareMemoryRetries = m_MaxPrepareMemoryRetries;
			rageDebugf1("%s [%d] PrepareMemory :: Invalid allocator! Failing task!", GetRequestName(), m_RequestId);
			return false;
		}
	}

	// allocate using our assigned allocator
	if(!TryAllocate(allocator OUTPUT_ONLY(, allocatorName)))
	{
		// only use the back-up if we have a valid primary allocator
		if(m_MemPolicy.m_pGrowBufferAllocator != nullptr && 
			m_MemPolicy.m_pGrowBufferBackup != nullptr &&
			TryAllocate(m_MemPolicy.m_pGrowBufferBackup OUTPUT_ONLY(, "Backup")))
		{
			rageDebugf1("%s [%d] PrepareMemory :: Failed to allocate memory! Requested: %u", GetRequestName(), m_RequestId, m_MemoryPresize);
			return false;
		}
	}

	return true;
}

bool netCloudRequestGetFile::DoRequest()
{
	return rageVerify(IsMemoryPrepared() || m_MemoryPresize == 0); //For the base class, make sure our memory is prepared prior to starting.
}

netCloudRequestGetMemberFile::netCloudRequestGetMemberFile(const int localGamerIndex, rlCloudMemberId cloudMemberId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(localGamerIndex, cloudMemberId, szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, svc, cbFinished, rMemPolicy);
}

void netCloudRequestGetMemberFile::Init(const int localGamerIndex, rlCloudMemberId cloudMemberId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	m_LocalGamerIndex = localGamerIndex;
	m_CloudMemberId = cloudMemberId;
	m_Service = svc;
	netCloudRequestGetFile::Init(szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetMemberFile::GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const
{
    atStringBuilder url;
    atStringBuilder absCloudPathBuilder(szAbsolutePath, nMaxLength);
    rlCloud::CreateUrl(&url, RL_CLOUD_NAMESPACE_MEMBERS, m_Service, m_CloudMemberId, m_CloudFilePath.c_str(), &absCloudPathBuilder);
    return szAbsolutePath;
}

bool netCloudRequestGetMemberFile::DoRequest()
{
	// call up 
	bool success = netCloudRequestGetFile::DoRequest();

	// kick off the download
	return success && rlCloud::GetMemberFile(m_LocalGamerIndex,
                                             m_CloudMemberId,
                                             m_Service,
                                             m_CloudFilePath.c_str(),
                                             m_SecurityFlags,
                                             m_ModifiedTimestamp,
                                             m_HttpOptions,
                                             m_GB.GetFiDevice(),
                                             m_GB.GetFiHandle(),
                                             &m_CloudFileInfo,
                                             m_MemPolicy.m_pHttpAllocator,
                                             &m_CloudFileStatus);
}

netCloudRequestPostMemberFile::netCloudRequestPostMemberFile(const int localGamerIndex, const char* szCloudFilePath, const void* pData, const unsigned uSizeOfData, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(localGamerIndex, szCloudFilePath, pData, uSizeOfData, svc, cbFinished, rMemPolicy);
}

void netCloudRequestPostMemberFile::Init(const int localGamerIndex, const char* szCloudFilePath, const void* pData, const unsigned uSizeOfData, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	m_LocalGamerIndex = localGamerIndex;
	m_Service = svc;
	m_pData = pData;
	m_uSizeOfData = uSizeOfData;
	netCloudRequestHelper::Init(szCloudFilePath, 0, NET_HTTP_OPTIONS_NONE, cbFinished, rMemPolicy);
}

bool netCloudRequestPostMemberFile::DoRequest()
{
	return rlCloud::PostMemberFile(m_LocalGamerIndex, m_Service, m_CloudFilePath, m_pData, m_uSizeOfData, rlCloud::POST_REPLACE, &m_CloudFileInfo, m_MemPolicy.m_pHttpAllocator, &m_CloudFileStatus); 
}

netCloudRequestDeleteMemberFile::netCloudRequestDeleteMemberFile(const int localGamerIndex, const char* szCloudFilePath, rlCloudOnlineService svc, datCallback cbFinished)
{
	Init(localGamerIndex, szCloudFilePath, svc, cbFinished);
}

void netCloudRequestDeleteMemberFile::Init(const int localGamerIndex, const char* szCloudFilePath, rlCloudOnlineService svc, datCallback cbFinished)
{
	m_LocalGamerIndex = localGamerIndex;
	m_Service = svc;
	
	netCloudRequestHelper::Init(szCloudFilePath, 0, NET_HTTP_OPTIONS_NONE, cbFinished, netCloudRequestMemPolicy::NULL_POLICY);
}

bool netCloudRequestDeleteMemberFile::DoRequest()
{
	// kick off the download
	return rlCloud::DeleteMemberFile(m_LocalGamerIndex, m_Service, m_CloudFilePath.c_str(), m_MemPolicy.m_pHttpAllocator, &m_CloudFileStatus);
}

netCloudRequestGetCrewFile::netCloudRequestGetCrewFile(const int localGamerIndex, rlCloudMemberId targetCrewId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(localGamerIndex, targetCrewId, szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, svc, cbFinished, rMemPolicy);
}

void netCloudRequestGetCrewFile::Init(const int localGamerIndex, rlCloudMemberId targetCrewId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	m_LocalGamerIndex = localGamerIndex;
	m_TargetCrewId = targetCrewId;
	m_Service = svc;
	netCloudRequestGetFile::Init(szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetCrewFile::GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const
{
    atStringBuilder url;
    atStringBuilder absCloudPathBuilder(szAbsolutePath, nMaxLength);
    rlCloud::CreateUrl(&url, RL_CLOUD_NAMESPACE_CREWS, m_Service, m_TargetCrewId, m_CloudFilePath.c_str(), &absCloudPathBuilder);
    return szAbsolutePath;
}

bool netCloudRequestGetCrewFile::DoRequest()
{
	bool success = netCloudRequestGetFile::DoRequest();

	// kick off the download
	return success && rlCloud::GetCrewFile(m_LocalGamerIndex,
                                           m_TargetCrewId,
                                           m_Service,
                                           m_CloudFilePath.c_str(),
                                           m_SecurityFlags,
                                           m_ModifiedTimestamp,
                                           m_HttpOptions,
                                           m_GB.GetFiDevice(),
                                           m_GB.GetFiHandle(),
                                           &m_CloudFileInfo,
                                           m_MemPolicy.m_pHttpAllocator,
                                           &m_CloudFileStatus);
}

netCloudRequestGetUgcFile::netCloudRequestGetUgcFile(const int localGamerIndex, 
                                                     const rlUgcContentType contentType,
                                                     const char* contentId,
                                                     const int fileId, 
                                                     const int fileVersion,
                                                     const rlScLanguage language,
                                                     u64 uModifiedTimestamp,
                                                     netHttpOptions httpOptions,
                                                     u32 uPresize, 
                                                     datCallback cbFinished, 
                                                     netCloudRequestMemPolicy& rMemPolicy)
{
	Init(localGamerIndex, contentType, contentId, fileId, fileVersion, language, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

void netCloudRequestGetUgcFile::Init(const int localGamerIndex, 
                                     const rlUgcContentType contentType,
                                     const char* contentId,
                                     const int fileId, 
                                     const int fileVersion,
                                     const rlScLanguage language,
                                     u64 uModifiedTimestamp,
                                     netHttpOptions httpOptions,
                                     u32 uPresize, 
                                     datCallback cbFinished, 
                                     netCloudRequestMemPolicy& rMemPolicy)
{
    m_LocalGamerIndex = localGamerIndex;
    m_ContentType = contentType;
    safecpy(m_ContentId, contentId, sizeof(m_ContentId));
    m_FileId = fileId;
    m_FileVersion = fileVersion;
    m_Language = language;

    netCloudRequestGetFile::Init(m_ContentId, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetUgcFile::GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const
{
    // UGC abs path calculated differently
    char szCloudAbsPath[RLUGC_MAX_CLOUD_ABS_PATH_CHARS];
    rlUgcMetadata::ComposeCloudAbsPath(m_ContentType,
                                       m_ContentId,
                                       m_FileId, 
                                       m_FileVersion,
                                       m_Language,
                                       szCloudAbsPath, 
                                       sizeof(szCloudAbsPath));

    atStringBuilder url;
    atStringBuilder absCloudPathBuilder(szAbsolutePath, nMaxLength);
    rlCloud::CreateCompleteUrl(&url, szCloudAbsPath, &absCloudPathBuilder);
    return szAbsolutePath;
}

bool netCloudRequestGetUgcFile::DoRequest()
{
	bool success = netCloudRequestGetFile::DoRequest();

	// kick off the download
	return success && rlCloud::GetUgcFile(m_LocalGamerIndex,
							              m_ContentType,
                                          m_ContentId,
                                          m_FileId,
                                          m_FileVersion,
                                          m_Language,
                                          m_SecurityFlags,
                                          m_ModifiedTimestamp,
                                          m_HttpOptions,
                                          m_GB.GetFiDevice(),
                                          m_GB.GetFiHandle(),
                                          &m_CloudFileInfo,
                                          m_MemPolicy.m_pHttpAllocator,   //allocator
	                                      &m_CloudFileStatus);
}

////////////////////////////////////////////////////////////////////////////////
// netCloudRequestGetUgcCdnFile
////////////////////////////////////////////////////////////////////////////////
netCloudRequestGetUgcCdnFile::netCloudRequestGetUgcCdnFile(
	const int localGamerIndex,
    const char* contentName,
    const rlUgcTokenValues& tokenValues,
    const rlUgcUrlTemplate* urlTemplate,
    u64 uModifiedTime,
    netHttpOptions httpOptions,
    u32 uPresize,
    datCallback cbFinished,
    netCloudRequestMemPolicy& rMemPolicy)
{
    Init(localGamerIndex, contentName, tokenValues, urlTemplate, uModifiedTime, httpOptions, uPresize, cbFinished, rMemPolicy);
}

void netCloudRequestGetUgcCdnFile::Init(
	const int localGamerIndex,
    const char* contentName,
    const rlUgcTokenValues& tokenValues,
    const rlUgcUrlTemplate* urlTemplate,
    u64 uModifiedTime,
    netHttpOptions httpOptions,
    u32 uPresize,
    datCallback cbFinished,
    netCloudRequestMemPolicy& rMemPolicy)
{
    m_LocalGamerIndex = localGamerIndex;
    m_ContentName = contentName;
    m_TokenValues = tokenValues;
    m_UrlTemplate = urlTemplate;

    // build a cloud path for logging
    char szCloudAbsPath[MAX_URI_PATH_BUF_SIZE] = { 0 };
    rlUgcBuildUrlFromTemplate(urlTemplate->templateUrl, urlTemplate->tokens, tokenValues, szCloudAbsPath, (unsigned)sizeof(szCloudAbsPath));

    netCloudRequestGetFile::Init(szCloudAbsPath, uModifiedTime, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetUgcCdnFile::GetAbsPath(char* szAbsolutePath, unsigned /*nMaxLength*/) const
{
    rlAssertf(false, "Should not be used");
    szAbsolutePath[0] = 0;
    return szAbsolutePath;
}

bool netCloudRequestGetUgcCdnFile::DoRequest()
{
    bool success = netCloudRequestGetFile::DoRequest();

    // call up to UGC
    return success && rlUgc::GetFeaturedContentData(
		m_LocalGamerIndex,
        m_TokenValues,
        m_UrlTemplate,
        &m_CloudFileInfo,
        m_ModifiedTimestamp,
        m_MemPolicy.m_pGrowBufferAllocator,
        &m_GB,
        &m_CloudFileStatus);
}

////////////////////////////////////////////////////////////////////////////////
// netCloudRequestGetTitleFile
////////////////////////////////////////////////////////////////////////////////
netCloudRequestGetTitleFile::netCloudRequestGetTitleFile(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

void netCloudRequestGetTitleFile::Init(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	netCloudRequestGetFile::Init(szCloudFilePath, uModifiedTimestamp, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetTitleFile::GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const
{
    atStringBuilder url;
    atStringBuilder absCloudPathBuilder(szAbsolutePath, nMaxLength);
    rlCloudMemberId invalidMemberID;
    rlCloud::CreateUrl(&url, RL_CLOUD_NAMESPACE_TITLES, RL_CLOUD_ONLINE_SERVICE_INVALID, invalidMemberID, m_CloudFilePath.c_str(), &absCloudPathBuilder);
    return szAbsolutePath;
}

bool netCloudRequestGetTitleFile::DoRequest()
{
	bool success = netCloudRequestGetFile::DoRequest();

	// kick off the download
	return success && rlCloud::GetTitleFile(m_CloudFilePath.c_str(),
                                            m_SecurityFlags,
                                            m_ModifiedTimestamp,
                                            m_HttpOptions,
                                            m_GB.GetFiDevice(),
                                            m_GB.GetFiHandle(),
                                            &m_CloudFileInfo,
                                            m_MemPolicy.m_pHttpAllocator,
                                            &m_CloudFileStatus);
}

////////////////////////////////////////////////////////////////////////////////
// netCloudRequestGetGlobalFile
////////////////////////////////////////////////////////////////////////////////
netCloudRequestGetGlobalFile::netCloudRequestGetGlobalFile(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(szCloudFilePath, uModifiedTime, httpOptions, uPresize, cbFinished, rMemPolicy);
}

void netCloudRequestGetGlobalFile::Init(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	netCloudRequestGetFile::Init(szCloudFilePath, uModifiedTime, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetGlobalFile::GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const
{
    atStringBuilder url;
    atStringBuilder absCloudPathBuilder(szAbsolutePath, nMaxLength);
    rlCloudMemberId invalidMemberID;
    rlCloud::CreateUrl(&url, RL_CLOUD_NAMESPACE_GLOBAL, RL_CLOUD_ONLINE_SERVICE_INVALID, invalidMemberID, m_CloudFilePath.c_str(), &absCloudPathBuilder);
    return szAbsolutePath;
}

bool netCloudRequestGetGlobalFile::DoRequest()
{
	bool success = netCloudRequestGetFile::DoRequest();

	// kick off the download
	return success && rlCloud::GetGlobalFile(m_CloudFilePath.c_str(),
                                             m_SecurityFlags,
                                             m_ModifiedTimestamp,
                                             m_HttpOptions,
                                             m_GB.GetFiDevice(),
                                             m_GB.GetFiHandle(),
                                             &m_CloudFileInfo,
                                             m_MemPolicy.m_pHttpAllocator,
                                             &m_CloudFileStatus);
}

////////////////////////////////////////////////////////////////////////////////
// netCloudRequestGetWWWFile
////////////////////////////////////////////////////////////////////////////////
netCloudRequestGetWWWFile::netCloudRequestGetWWWFile(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	Init(szCloudFilePath, uModifiedTime, httpOptions, uPresize, cbFinished, rMemPolicy);
}

void netCloudRequestGetWWWFile::Init(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy)
{
	netCloudRequestGetFile::Init(szCloudFilePath, uModifiedTime, httpOptions, uPresize, cbFinished, rMemPolicy);
}

const char* netCloudRequestGetWWWFile::GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const
{
	atStringBuilder absCloudPathBuilder(szAbsolutePath, nMaxLength);
	absCloudPathBuilder.Append(m_CloudFilePath.c_str());
	return szAbsolutePath;
}

bool netCloudRequestGetWWWFile::DoRequest()
{
	bool success = netCloudRequestGetFile::DoRequest();

	// kick off the download
	return success && rlCloud::GetGlobalFile(m_CloudFilePath.c_str(),
		m_SecurityFlags,
		m_ModifiedTimestamp,
		m_HttpOptions,
		m_GB.GetFiDevice(),
		m_GB.GetFiHandle(),
		&m_CloudFileInfo,
		m_MemPolicy.m_pHttpAllocator,
		&m_CloudFileStatus);
}

} // namespace rage
