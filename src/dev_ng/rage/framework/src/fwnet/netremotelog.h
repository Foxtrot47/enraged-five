//===========================================================================
// Copyright (C) 1999-2016 Rockstar Games.  All Rights Reserved.
//===========================================================================

#ifndef NETREMOTELOG_H
#define NETREMOTELOG_H

#include "data/growbuffer.h"
#include "file/handle.h"
#include "file/limits.h"
#include "net/status.h"
#include "string/stringbuilder.h"
#include "system/criticalsection.h"

namespace rage
{

class netRemoteLog
{
public:
	static void InitClass(const char* titleName);
	static void UpdateClass();
	static void ShutdownClass();
	static void SetTunables(const bool useEncryption, const bool useCompression, const bool uploadToCloud);
	static void BeginSend();

	netRemoteLog();

private:
	struct Arg
	{
		enum class Type
		{
			Int,
			Float,
			Double,
			String
		};

		Arg(const int value) : m_Type(Type::Int), m_IntValue(value){};
		Arg(const float value) : m_Type(Type::Float), m_FloatValue(value){};
		Arg(const double value) : m_Type(Type::Double), m_DoubleValue(value){};
		Arg(const char* value) : m_Type(Type::String), m_StringValue(value){};

		Type m_Type;

		union {
			int m_IntValue;
			float m_FloatValue;
			double m_DoubleValue;
			const char* m_StringValue;
		};
	};

	enum class SendState
	{
		None,
		Compressing,
		Posting
	};

#if !__NO_OUTPUT
	// TODO: Make constexpr.
	const char* SendStateToString(const SendState sendState)
	{
		switch (sendState)
		{
		case SendState::None:
			return "None";
		case SendState::Compressing:
			return "Compressing";
		case SendState::Posting:
			return "Posting";
			// Deliberately no default so we get a compile error if we miss one.
		}

		return nullptr;
	}
#endif

	void Init(const char* titleName);
	void Shutdown();
	void Update();

	static void AppendLine(const int msgId, const int argCount, Arg* args);

	void AppendLineInternal(const int msgId, const int argCount, Arg* args);

	void BeginSendInternal();

	SendState BeginCompressingLog();
	SendState BeginPostingLog();

	SendState CompressLog(const SendState currentState);
	SendState PostLog(const SendState currentState);

	atStringBuilder m_WriteBufferBuilder;

	// StringBuilder we write to when the compression job is in progress and the main buffer is in use.
	atStringBuilder m_TempWriteBufferBuilder;

	// Buffer used to store the (probably compressed) log between the compression phase and completion
	// of the sending phase.
	datGrowBuffer m_SendGrowBuffer;

	static const int WRITE_BUFFER_SIZE = 65536;

	// Underlying buffer used by m_WriteBufferBuilder. Size is somewhat arbitrary.
	char m_WriteBuffer[WRITE_BUFFER_SIZE];

	// Name of the title for cloud path construction.
	char m_titleName[5];

	netStatus m_UploadStatus;

	static const int THREAD_CPU_ID;

	SendState m_CurrentSendState;

	bool m_Enabled;
	bool m_UseEncryption;
	bool m_UseCompression;
	bool m_UploadToCloud;

	mutable sysCriticalSectionToken m_Cs;

public:
// Append(...) templates
#if RSG_ORBIS // full C++11 support
	template <typename... Args> static void Append(const int msgId, const Args... args)
	{
		Arg argArray[] = { args... };
		AppendLine(msgId, sizeof...(Args), argArray);
	}
#else
	// We'll get rid of these once we have full support for variadic function templates in our compilers on all
	// platforms.
	static void Append(const int msgId) { AppendLine(msgId, 0, nullptr); }
	template <typename T0> static void Append(const int msgId, T0 a)
	{
		const int count = 1;
		Arg args[count] = { Arg(a) };
		AppendLine(msgId, count, args);
	}
	template <typename T0, typename T1> static void Append(const int msgId, T0 a, T1 b)
	{
		const int count = 2;
		Arg args[count] = { Arg(a), Arg(b) };
		AppendLine(msgId, count, args);
	}
	template <typename T0, typename T1, typename T2> static void Append(const int msgId, T0 a, T1 b, T2 c)
	{
		const int count = 3;
		Arg args[count] = { Arg(a), Arg(b), Arg(c) };
		AppendLine(msgId, count, args);
	}
	template <typename T0, typename T1, typename T2, typename T3>
	static void Append(const int msgId, T0 a, T1 b, T2 c, T3 d)
	{
		const int count = 4;
		Arg args[count] = { Arg(a), Arg(b), Arg(c), Arg(d) };
		AppendLine(msgId, count, args);
	}
	template <typename T0, typename T1, typename T2, typename T3, typename T4>
	static void Append(const int msgId, T0 a, T1 b, T2 c, T3 d, T4 e)
	{
		const int count = 5;
		Arg args[count] = { Arg(a), Arg(b), Arg(c), Arg(d), Arg(e) };
		AppendLine(msgId, count, args);
	}
	template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5>
	static void Append(const int msgId, T0 a, T1 b, T2 c, T3 d, T4 e, T5 f)
	{
		const int count = 6;
		Arg args[count] = { Arg(a), Arg(b), Arg(c), Arg(d), Arg(e), Arg(f) };
		AppendLine(msgId, count, args);
	}
// Need more than 6 arguments? Add another overload of Append.
#endif

}; // class netRemoteLog

}; // namespace rage.

#endif // NETREMOTELOG_H
