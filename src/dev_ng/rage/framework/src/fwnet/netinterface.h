//
// netinterface.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#ifndef NET_INTERFACE_H
#define NET_INTERFACE_H

#include "atl/delegate.h"
#include "net/packet.h"

#include "fwnet/netlog.h"
#include "fwnet/nettypes.h"

namespace rage
{
class netArrayManager;
class netConnectionManager;
class netEvent;
class netEventMgr;
class netLoggingInterface;
class netObject;
class netObjectMgrBase;
class rlPeerInfo;
class netPlayer;
class netPlayerMgrBase;
class netRequestHandler;
class netResponseHandler;
class netRequest;
class netTimeSync;
class netTransactionInfo;
class rlGamerId;

//PURPOSE
// Contains the data relating to a network message received
// by a player. Includes the message data and size, the sequence
// number and the players involved in the sending/receiving of the message
struct ReceivedMessageData
{
    ReceivedMessageData(const void *messageData,
                        u32 messageDataSize,
                        netSequence sequence,
                        netPlayer  *fromPlayer,
                        netPlayer  *toPlayer) :
    m_MessageData(messageData)
    , m_MessageDataSize(messageDataSize)
    , m_FromPlayer(fromPlayer)
    , m_ToPlayer(toPlayer)
    , m_NetSequence(sequence)
    {
    }

    bool IsValid() const { return m_MessageData && m_FromPlayer && m_ToPlayer; }

    const void  *m_MessageData;
    u32          m_MessageDataSize;
    netPlayer   *m_FromPlayer;
    netPlayer   *m_ToPlayer;
    netSequence  m_NetSequence;
};

//Network event delegator and delegate
typedef atDelegator<void (const ReceivedMessageData &messageData)> NetworkPlayerEventDelegator;
typedef NetworkPlayerEventDelegator::Delegate NetworkPlayerEventDelegate;

//Transaction request delegator and delegate
typedef atDelegator<void (netPlayer*, const netRequest*)> NetworkPlayerRequestDelegator;
typedef NetworkPlayerRequestDelegator::Delegate NetworkPlayerRequestHandler;

//PURPOSE
// Provides access to common network functionality for the network framework code
class netInterface
{
public:

    //PURPOSE
    // Interface class for any query functions that rely on functionality implemented
    // at the game level
    class queryFunctions
    {
    public:

		virtual bool     IsSnSessionEstablished() const = 0;
		virtual bool     IsSessionEstablished() const = 0;
		virtual unsigned GetNetworkTime() = 0;
		virtual void	 FlushAllLogFiles(bool waitForFlush) = 0;

    protected:

        virtual ~queryFunctions() {}
    };

    //PURPOSE
    // Initialisation for the network interface, your application must call this before using
    // any network framework functionality
    //PARAMS
    // connectionMgr - The connection manager used by the application
    // playerMgr     - The player manager used by the application
    // timeSync      - The time sync object used by the application to synchronise time between players
    // messageLog    - The logging object for writing data relating to sending/receiving network messages
    // queryFuncs    - Interface for network queries implemented by game specific code
    static void Init(netConnectionManager &connectionMgr,
                     netPlayerMgrBase     &playerMgr,
                     netTimeSync          &timeSync,
                     netLoggingInterface  &messageLog,
                     queryFunctions       &queryFuncs);

    //PURPOSE
    // Shutdown function for the network interface
    static void Shutdown();

    //PURPOSE
    // Returns whether the network interface has been initialised
    static bool IsInitialised() { return m_IsInitialised; }

    //PURPOSE
    // Returns whether the network interface has been initialised
    static bool IsShuttingDown() { return m_ShuttingDown; }

    //PURPOSE
    // Tells the network interface the network code is shutting down
    static void SetShuttingDown() { m_ShuttingDown = true; }

    //PURPOSE
    // Sets the array manager used by the application
    //PARAMS
    // arrayMgr - The array manager used by the application
    static void SetArrayManager(netArrayManager &arrayMgr);

    //PURPOSE
    // Sets the event manager used by the application
    //PARAMS
    // eventMgr - The event manager used by the application
    static void SetEventManager(netEventMgr &eventMgr);

    //PURPOSE
    // Sets the object manager used by the application
    //PARAMS
    // objectMgr - The object manager used by the application
    static void SetObjectManager(netObjectMgrBase &objectMgr);

    //PURPOSE
    // Register the framework network events
    static void RegisterNetGameEvents();

    //PURPOSE
    // Returns whether this machine is the host of the session
    static bool IsHost();

	//PURPOSE
	// Returns the player who is the hosting the session
	static const netPlayer* GetHostPlayer();

	//PURPOSE
	// Returns whether the network session has been created
	static bool IsSnSessionEstablished();

    //PURPOSE
    // Returns whether the game is currently in a network session. This can be used to
    // decide when to start any initial negotiations between the local player and
    // any pre-existing players in the session they have joined.
    static bool IsSessionEstablished();

    //PURPOSE
    // Returns whether the network clock has synced to the other players in the session
    static bool NetworkClockHasSynced();

	//PURPOSE
	// Flushes the network log files for each manager
	static void FlushAllLogFiles(bool waitForFlush);

   //PURPOSE
    // Called when the timestep has just been calculated at the start of a frame.
    // This is necessary to ensure that any timestamps we send to remote machines
    // relating to object positions are as accurate as possible
    static void StartOfFrame();

    //PURPOSE
    // Returns the current network time
    static unsigned GetNetworkTime();

    //PURPOSE
    // Returns the network time to associate with positional data retrieved this
    // frame.
    static unsigned GetTimestampForPositionUpdates();

    //PURPOSE
    // Returns the network time recorded at the start of the current frame
    static unsigned GetTimestampForStartOfFrame();

	//PURPOSE
	// Returns a system time used by the sync data that does not wrap
	static unsigned GetSynchronisationTime();

	//PURPOSE
	// Returns the current number of pending players in the session
	static unsigned GetNumPendingPlayers();

    //PURPOSE
    // Returns the current number of active players in the session
    static unsigned GetNumActivePlayers();

    //PURPOSE
    // Returns the current number of remote active players in the session
    static unsigned GetNumRemoteActivePlayers();

    //PURPOSE
    // Returns the current number of physical players in the session
    static unsigned GetNumPhysicalPlayers();

    //PURPOSE
    // Returns the current number of local physical players in the session
    static unsigned GetNumLocalPhysicalPlayers();

    //PURPOSE
    // Returns the current number of remote physical players in the session
    static unsigned GetNumRemotePhysicalPlayers();

    //PURPOSE
    // Returns the array of pending players
    static netPlayer * const *GetPendingPlayers();

    //PURPOSE
    // Returns the array of all physical players
    static netPlayer * const *GetAllActivePlayers();

    //PURPOSE
    // Returns the array of remote active players
    static netPlayer * const *GetRemoteActivePlayers();

    //PURPOSE
    // Returns the array of all physical players
    static netPlayer * const *GetAllPhysicalPlayers();

    //PURPOSE
    // Returns the array of remote active players
    static netPlayer * const *GetRemotePhysicalPlayers();

    //PURPOSE
    // Returns whether there is a connection with the specified ID currently open
    //PARAMS
    // connectionID - The connection ID
    static bool IsConnectionOpen(const int cxnId);

    //PURPOSE
    // Returns the network player using the specified connection ID
    //PARAMS
    // connectionID - The connection ID
    static netPlayer* GetPlayerFromConnectionId(const int cxnId);

    //PURPOSE
    // Returns the network player using the specified gamer ID
    //PARAMS
    // gamerID - The gamer ID
    static netPlayer* GetPlayerFromGamerId(const rlGamerId& gamerId);

    //PURPOSE
    // Returns the network player using the specified peer ID
    //PARAMS
    // peerID - The peer ID
    static netPlayer* GetPlayerFromPeerId(const u64 peerId);

    //PURPOSE
    // Returns the network player with the specified player index
    //PARAMS
    // playerIndex - The player index
    static netPlayer* GetPhysicalPlayerFromIndex(const PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Returns the network player with the specified player index, only if they are active.
    // A player is considered active once they have completed the join process into the session
    //PARAMS
    // playerIndex - The player index
    static netPlayer* GetActivePlayerFromIndex(const ActivePlayerIndex playerIndex);

    //PURPOSE
    // Returns a network player controlled by the specified peer. Peers can support more
    // than one local player.
    //PARAMS
    // peerPlayerIndex - The player index of the player on the specified remote peer
    // peerInfo        - Description of the remote peer the player is associated with
    static netPlayer* GetPlayerFromPeerPlayerIndex(const unsigned peerPlayerIndex, const rlPeerInfo &peerInfo);

    //PURPOSE
    // Returns the network player representing the local player
    static netPlayer* GetLocalPlayer();

    //PURPOSE
    // Returns the active player index of the local player's network player
    static PhysicalPlayerIndex GetLocalActivePlayerIndex();

	//PURPOSE
	// Returns the physical player index of the local player's network player
	static PhysicalPlayerIndex GetLocalPhysicalPlayerIndex();

    //PURPOSE
	// Returns the network player using the specified object ID range index
    //PARAMS
    // idRangeIndex - The id range index of the player to return
    static const netPlayer *GetPlayerFromObjectIdRangeIndex(const int idRangeIndex);

    //PURPOSE
    // Returns the object ID range index for the specified player
    //PARAMS
    // player - The player to return the ID range index for
    static int GetObjectIdRangeIndexFromPlayer(const netPlayer &player);

    //PURPOSE
    // Returns the object IDs currently in use by the local machine within the specified ID range
    //PARAMS
    // idsInUseLocally    - array for returning the IDs in use
    // numInUseIDs        - the number of IDs with the specified range in use
    // numLocallyOwnedIDs - the number of in use IDs controlled by this machine
    // idStart            - The start of the ID range to check
    // idEnd              - The end of the ID range to check
    static void GetObjectIDsInUseLocally(ObjectId *idsInUseLocally, unsigned &numInUseIDs, unsigned &numLocallyOwnedIDs, unsigned idStart, unsigned idEnd);

    //PURPOSE
    // Returns the network object with the specified global object ID
    //PARAMS
    // objectID - The global ID of the object to return
    static netObject *GetNetworkObject(const ObjectId objectID);

	//PURPOSE
	// Player manager validity checks
	static bool IsPlayerMgrValid();
	static bool IsPlayerMgrInitialised();

    //PURPOSE
    // Returns the player manager
    static netPlayerMgrBase &GetPlayerMgr();

    //PURPOSE
    // Returns the time sync object
    static netTimeSync &GetNetworkClock();

    //PURPOSE
    // Returns the object manager
    static netObjectMgrBase &GetObjectManager();

    //PURPOSE
    // Returns the event manager
    static netEventMgr &GetEventManager();

    //PURPOSE
    // Returns the array manager
    static netArrayManager &GetArrayManager();

    //PURPOSE
    // Returns the object manager log
    static netLoggingInterface &GetObjectManagerLog();

    //PURPOSE
    // Returns the message log
    static netLoggingInterface &GetMessageLog();

    //PURPOSE
    // Sets whether the timebars should be updated, if networking code
    // is called multiple times per frame the timebars should only be
    // updated the first run through the code
    //PARAM
    // updateTimebars - Whether the timebars should be updated
    static void SetShouldUpdateTimebars(bool updateTimebars);

    //PURPOSE
    // Returns whether the timebars should be updated, if networking code
    // is called multiple times per frame the timebars should only be
    // updated the first run through the code
    static bool ShouldUpdateTimebars();

    //PURPOSE
    // Returns the system time the sync message with the specified sequence was sent to the specified player. Also checks whether this
    // message has expired so we can track unreliable resend failures without having to search the message history twice
    //PARAMS
    // playerIndex     - The physical player index of the player sent the sync message
    // syncMessageSeq  - The sequence number of the sync message to check
    // expiryTime      - The system time the sync message expires
    static unsigned GetTimeSyncMessageSentAndCheckExpiry(PhysicalPlayerIndex playerIndex, netSequence syncMessageSeq, unsigned expiryTime);

    //PURPOSE
    // Returns the default log file target type to use for network log files
    static LogTargetType GetDefaultLogFileTargetType();

    //PURPOSE
    // Returns the default log file blocking mode to use for network log files
    static LogBlockingMode GetDefaultLogFileBlockingMode();

#if ENABLE_NETWORK_BOTS
    //PURPOSE
    // Returns the number of total active bots in the network session
    static unsigned GetNumTotalActiveBots();

    //PURPOSE
    // Returns the number of active locally controlled bots in the network session
    static unsigned GetNumLocalActiveBots();

    //PURPOSE
    // Returns the number of active remotely controlled bots in the network session
    static unsigned GetNumRemoteActiveBots();
#endif // ENABLE_NETWORK_BOTS

#if __BANK

    //PURPOSE
    // Returns the number of messages sent unreliably since the last call to ResetMessageCounts()
    static unsigned GetNumUnreliableMessagesSent();

    //PURPOSE
    // Returns the number of messages sent reliably since the last call to ResetMessageCounts()
    static unsigned GetNumReliableMessagesSent();

    //PURPOSE
    // Resets the counts of messages sent that have been counted by framework level systems
    static void ResetMessageCounts();
#endif // __BANK

    //////////////////////////////////////////////////////////////////////////
    // Messaging
    //////////////////////////////////////////////////////////////////////////

    //PURPOSE
    // Sends data to the specified player
    //PARAMS
    // toPlayer    - The player to send the data to
    // buffer      - The data to send
    // bufferSize  - The size of the data to send in bytes
    // sendFlags   - The send flags to use when sending the data
    // sequenceNum - The sequence number to use when sending the data
    // fromPlayer - The player sending the data (0 indicates local player, used for bots)
    static bool SendBuffer(const netPlayer *player,
                           const void      *buffer,
                           const unsigned   bufferSize,
                           const unsigned   sendFlags,
                           netSequence     *sequenceNum = 0,
                           const netPlayer *fromPlayer = 0);

    //PURPOSE
    // Sends a reliable message to the given player
    //PARAMS
    // toPlayer    - The player to send the message to
    // message     - The message to send
    // sequenceNum - The sequence number to use when sending the message
    // fromPlayer - The player sending the data (0 indicates local player, used for bots)
    template<typename T>
    static bool SendReliableMessage(const netPlayer *toPlayer,
                                    const T         &message,
                                    netSequence     *sequenceNum = 0,
                                    const netPlayer *fromPlayer = 0)
    {
        return SendMsg(toPlayer, message, NET_SEND_RELIABLE, sequenceNum, fromPlayer);
    }

    //PURPOSE
    // Sends a message to the given player 
    //PARAMS
    // toPlayer    - The player to send the message to
    // message     - The message to send
    // sendFlags   - The send flags to use when sending the message
    // sequenceNum - The sequence number to use when sending the message
    // fromPlayer  - The player sending the data (0 indicates local player, used for bots)
    template<typename T>
    static bool SendMsg(const netPlayer *toPlayer,
                        const T         &message,
                        const unsigned   sendFlags,
                        netSequence     *sequenceNum = 0,
                        const netPlayer *fromPlayer = 0)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return message.Export(buf, sizeof(buf), &size)
            && SendBuffer(toPlayer, buf, size, sendFlags, sequenceNum, fromPlayer);
    }

    //PURPOSE
    // Broadcasts a message to all players
    //PARAMS
    // message      - The message to broadcast
    // sendFlags    - The send flags to use when broadcasting the message
    // iteratorType - The player iterator type to use to determine who to broadcast the message to
    // fromPlayer   - The player sending the data (0 indicates local player, used for bots)
    template<typename T>
    static void BroadcastMsg(const T              &message,
                             const unsigned        sendFlags,
                             const netPlayer      *fromPlayer = 0)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        if(AssertVerify(message.Export(buf, sizeof(buf), &size)))
        {
            unsigned                 numRemoteActivePlayers = GetNumRemoteActivePlayers();
            const netPlayer * const *remoteActivePlayers    = GetRemoteActivePlayers();
            
	        for(unsigned index = 0; index < numRemoteActivePlayers; index++)
            {
                const netPlayer *player = remoteActivePlayers[index];
                SendBuffer(player, buf, size, sendFlags, 0, fromPlayer);
            }
        }
    }

    //PURPOSE
    // Adds a network player event handler
    //PARAMS
    // dlgt - The event handler to add
    static void AddDelegate(NetworkPlayerEventDelegate* dlgt);

    //PURPOSE
    // Removes a network player event handler
    //PARAMS
    // dlgt - The event handler to remove
    static void RemoveDelegate(NetworkPlayerEventDelegate* dlgt);

    //////////////////////////////////////////////////////////////////////////
    // Transactions
    //////////////////////////////////////////////////////////////////////////

    //PURPOSE
    // Sends request data to the specified player
    //PARAMS
    // player     - The player to send the request data to
    // buffer     - The request data to send
    // bufferSize - The size of the request data to send in bytes
    // timeout    - The timeout period before the transaction is cancelled
    // handler    - The response handler for this request
    static bool SendRequestBuffer(const netPlayer    *player,
                                  const void         *buffer,
                                  const unsigned      numBytes,
                                  const unsigned      timeout,
                                  netResponseHandler *handler);

    //PURPOSE
    // Sends response data to a received request
    //PARAMS
    // txInfo     - The transaction info structure
    // buffer     - The response data to send
    // bufferSize - The size of the response data to send
    static bool SendResponseBuffer(const netTransactionInfo& txInfo,
                                   const void* buffer,
                                   const unsigned numBytes);


    //PURPOSE
    // Sends a request message to the specified player
    //PARAMS
    // player  - The player to send the request message to
    // message - The message to send
    // timeout - The timeout period before the transaction is cancelled
    // handler - The response handler for this request
    template<typename T>
    static bool SendRequest(const netPlayer* player,
                            const T& msg,
                            const unsigned timeout,
                            netResponseHandler* handler)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
            && SendRequestBuffer(player, buf, size, timeout, handler);
    }

    //PURPOSE
    // Sends a response message to a received request
    //PARAMS
    // txInfo  - The transaction info structure
    // message - The message to send
    template<typename T>
    static bool SendResponse(const netTransactionInfo& txInfo,
                             const T& msg)
    {
        u8 buf[netFrame::MAX_BYTE_SIZEOF_PAYLOAD];
        unsigned size;
        return msg.Export(buf, sizeof(buf), &size)
            && SendResponseBuffer(txInfo, buf, size);
    }

    //PURPOSE
    // Adds a transaction request handlers
    //PARAMS
    // handler - The handler to add
    static void AddRequestHandler(NetworkPlayerRequestHandler *handler);

    //PURPOSE
    // Removes a transaction request handlers
    //PARAMS
    // handler - The handler to remove
    static void RemoveRequestHandler(NetworkPlayerRequestHandler *handler);

private:

    static bool                  m_IsInitialised; // Indicates whether the network interface has been initialised
    static bool                  m_ShuttingDown;  // Indicates whether the network interface is shutting down
    static netArrayManager      *m_ArrayMgr;      // The array manager
    static netConnectionManager *m_ConnectionMgr; // The connection manager
    static netEventMgr          *m_EventMgr;      // The event manager
    static netObjectMgrBase     *m_ObjectMgr;     // The object manager
    static netPlayerMgrBase     *m_PlayerMgr;     // The player manager
    static netTimeSync          *m_TimeSync;      // The time sync object used to synchronised time between players in a network session
    static netLoggingInterface  *m_MessageLog;    // The object used for logging messages send/received
    static queryFunctions       *m_QueryFuncs;    // Interface for queries dependent on game level functionality
};

} // namespace rage

#endif // NET_INTERFACE_H
