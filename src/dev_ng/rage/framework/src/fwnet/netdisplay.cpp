//
// netdisplay.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

// --- Include Files ------------------------------------------------------------
#include "fwnet/optimisations.h"
NETWORK_OPTIMISATIONS()

#include "fwnet/netdisplay.h"

#include "grcore/debugdraw.h"

#include <stdio.h>

namespace rage
{

#if DEBUG_DRAW
void netLogDisplay::Log(const char *logText, ...)
{
	char TextBuffer[200];
	va_list args;
	va_start(args,logText);
	vsprintf(TextBuffer,logText,args);
	va_end(args);

	grcDebugDraw::Text( m_displayPos, m_displayColor, m_displayOffset, m_displayLine*grcDebugDraw::GetScreenSpaceTextHeight(), TextBuffer);

	m_displayLine++;
}

void netLogDisplay::WriteDataValue(const char *dataName, const char *dataValue, ...)
{
	char TextBuffer[200];
	va_list args;
	va_start(args,dataValue);
	vsprintf(TextBuffer,dataValue,args);
	va_end(args);

	Log("%s: %s", dataName, TextBuffer);
}
#endif // DEBUG_DRAW

} // namespace rage
