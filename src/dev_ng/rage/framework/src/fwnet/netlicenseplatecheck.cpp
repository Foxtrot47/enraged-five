
#include "netlicenseplatecheck.h"
#include "fwnet/netchannel.h"
#include "fwnet/optimisations.h"

#include "atl/array.h"
#include "diag/seh.h"
#include "net/status.h"
#include "rline/ros/rlros.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/socialclub/rlsocialclubcommon.h"

using namespace rage;

RAGE_DEFINE_SUBCHANNEL(net, licenseplate)
#undef __net_channel
#define __net_channel net_licenseplate

NETWORK_OPTIMISATIONS() 

namespace netLicensePlateCheck {

static CheckTextToken GenerateToken(u16 slot, u16 generation)
{
	//TOKEN = [ generation (16 bits) | slot (16 bits) ]
	CheckTextToken newToken = ((u32)generation) << 16 | slot;
	return newToken;
}

static void DisectToken(const CheckTextToken& inToken, u16& outSlot, u16& outGeneration)
{
	outSlot = (u16) inToken & 0x0000ffff;
	outGeneration = (u16)((inToken & 0xffff0000) >> 16);
}

//////////////////////////////////////////////////////////////////////////
//	
//	netLicensePlateCheckRequest - helper class to wrap a request to check text.
//
//////////////////////////////////////////////////////////////////////////
class netLicensePlateCheckRequest
{
public:
	netLicensePlateCheckRequest() : m_generation(0), m_succeeded(false) {}

	bool IsPending() const { return m_status.Pending(); }
	void Reset()
	{
		if (m_status.Pending())
		{
			rlSocialClub::Cancel(&m_status);
		}

		m_succeeded = false;

		m_countReceived = 0;

		m_status.Reset();
		m_generation++;
	}

	bool CheckText(int localGamerIndex, const char* inText, const rlScLanguage)
	{
		if (IsPending())
		{
			return false;
		}

		Reset();

		//store the text off for comparison once we retrieve the viable text from social club services for comparison
		safecpy(m_licensePlateCheckText, inText);

		return rlSocialClub::GetLicensePlateInfo(localGamerIndex, m_licensePlateInfoList.GetElements(), MAX_PLATES, &m_countReceived, &m_status);
	}

	ReturnCode GetStatus()
	{
		if (m_status.Pending())
		{
			gnetDebug3("GetStatus--Pending");
			return RESULT_PENDING;
		}
		else if (m_status.Succeeded())
		{
			//We have viable results and can actively check the results against the incoming text value now...
			gnetDebug3("GetStatus--Succeeded--m_countReceived[%d]",m_countReceived);

			if (m_succeeded)
			{
				gnetDebug3("GetStatus--m_succeeded-->RESULT_STRING_OK");
				return RESULT_STRING_OK;
			}

			//ensure the internal list count matches up
			m_licensePlateInfoList.SetCount(m_countReceived);

			if (m_countReceived > 0)
			{
				gnetDebug3("m_licensePlateCheckText[%s]",m_licensePlateCheckText);

				const rlScLicensePlateInfo* pLicensePlateInfo = m_licensePlateInfoList.GetElements();
				if (pLicensePlateInfo)
				{
					for (unsigned i=0; i < m_countReceived; i++)
					{
						if (pLicensePlateInfo[i].m_plateText)
						{
							gnetDebug3("pLicensePlateInfo[%d].m_plateText[%s]",i,pLicensePlateInfo[i].m_plateText);

							if (strncasecmp(pLicensePlateInfo[i].m_plateText,m_licensePlateCheckText,rlScLicensePlateInfo::MAX_PLATE_NAME_CHARS) == 0)
							{
								gnetDebug3("GetStatus--Succeeded--EQUAL-->RESULT_STRING_OK");
								m_succeeded = true;
								return RESULT_STRING_OK;
							}
						}
					}
				}

			}

			gnetDebug3("GetStatus--Succeeded-->RESULT_STRING_FAILED");
			return RESULT_STRING_FAILED;
		}
		else if (m_status.Failed())
		{
#if !__NO_OUTPUT
			//Get the error code and see if it fail the profanity check
			int err = m_status.GetResultCode();
			gnetError( "Error code in netLicensePlateCheck: %d", err);
#endif
			return RESULT_ERROR;
		}
		
		gnetDebug3("GetStatus--Other-->RESULT_ERROR");
		return RESULT_ERROR;
	}

	const u16 GetGeneration() const { return m_generation; }
	const CheckTextToken GenerateToken (const u16 slot) const 
	{
		return netLicensePlateCheck::GenerateToken(slot, m_generation);
	}

	unsigned GetCount() { return m_countReceived; }

	const char* GetPlate(int index) { return ((index >= 0) && (((unsigned)index) < m_countReceived)) ? m_licensePlateInfoList[index].m_plateText : ""; }
	const char* GetPlateData(int index) { return ((index >= 0) && (((unsigned)index) < m_countReceived)) ? m_licensePlateInfoList[index].m_plateData: ""; }
	
	bool AddPlate( rlScLicensePlateInfo& plate )
	{
		if (m_countReceived >= MAX_PLATES)
			return false;

		if(!gnetVerify(plate.m_plateText))
			return false;

		for (int i=0; i<m_licensePlateInfoList.GetCount(); i++)
		{
			if (m_licensePlateInfoList[i].m_plateText)
			{
				if (strncasecmp(m_licensePlateInfoList[i].m_plateText, plate.m_plateText, rlScLicensePlateInfo::MAX_PLATE_NAME_CHARS) == 0)
				{
					gnetDebug1("m_licensePlateInfoList[%d].m_plateText[%s] already exists", i, m_licensePlateInfoList[i].m_plateText);
					return false;
				}
			}
		}

		safecpy(m_licensePlateInfoList[m_countReceived].m_plateText, plate.m_plateText);
		sysMemSet(m_licensePlateInfoList[m_countReceived].m_plateData, 0, rlScLicensePlateInfo::MAX_PLATE_DATA_CHARS);

		gnetDebug1("m_licensePlateInfoList[%d].m_plateText[%s]", m_countReceived, plate.m_plateText);

		m_countReceived++;

		return true;
	}

private:
	netStatus m_status;
	u16 m_generation;

	unsigned m_countReceived;

	static const unsigned MAX_PLATES = netLicensePlateCheck::MAX_NUM_PLATES;

	typedef atFixedArray<rlScLicensePlateInfo, MAX_PLATES> LicensePlateInfoList;
	const LicensePlateInfoList& GetLicensePlateInfoList() const {return m_licensePlateInfoList;}

	LicensePlateInfoList m_licensePlateInfoList;

	char m_licensePlateCheckText[rlScLicensePlateInfo::MAX_PLATE_NAME_CHARS + 1];

	bool m_succeeded;
}; //class netLicensePlateCheckRequest


//////////////////////////////////////////////////////////////////////////
//
//  netLicensePlateCheckMgr - Manage set of requests to check request, using 
//		a token system to reference them.
//
//////////////////////////////////////////////////////////////////////////
class netLicensePlateCheckMgr
{
public:
	netLicensePlateCheckMgr() : m_lastUsedSlot(0xFFFF) {}

	bool CheckText(int localGamerIndex, const char* inText, const rlScLanguage language, CheckTextToken& outToken);
	ReturnCode GetStatusByToken(const CheckTextToken& inToken);
	unsigned GetCount( const CheckTextToken& token );
	const char* GetPlate( const CheckTextToken& token, const int& index );
	const char* GetPlateData( const CheckTextToken& token, const int& index );
	bool UpdateWithAddPlate(const CheckTextToken& inToken, rlScLicensePlateInfo& plate);

private:

	static const int MAX_REQUESTS = 1;
	netLicensePlateCheckRequest m_RequestPool[MAX_REQUESTS];
	u16 m_lastUsedSlot;

}; //class netLicensePlateCheckMgr




bool netLicensePlateCheckMgr::CheckText( int localGamerIndex, const char* inText, const rlScLanguage language, CheckTextToken& outToken )
{
#if !__NO_OUTPUT
	char debugSTring[64];
	safecpy(debugSTring, inText);
#endif

	//First time
	if (m_lastUsedSlot == 0xFFFF)
	{
		m_lastUsedSlot = 0;
		if (!m_RequestPool[m_lastUsedSlot].IsPending())
		{
			if(m_RequestPool[m_lastUsedSlot].CheckText(localGamerIndex, inText, language))
			{
				outToken = m_RequestPool[m_lastUsedSlot].GenerateToken(m_lastUsedSlot);
				gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
				return true;
			}
		}
	}

	//Find a slot.
	u16 slotToUse = m_lastUsedSlot + 1;
	do 
	{
		//Wrap around
		if (slotToUse >= MAX_REQUESTS)
		{
			slotToUse = 0;
		}

		//See if this one is available
		if (!m_RequestPool[slotToUse].IsPending())
		{
			if(gnetVerify(m_RequestPool[slotToUse].CheckText(localGamerIndex, inText, language)))
			{
				outToken = m_RequestPool[slotToUse].GenerateToken(slotToUse);
				gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
				m_lastUsedSlot = slotToUse;
				return true;
			}
		}

		slotToUse++;

	} while (slotToUse != m_lastUsedSlot);

	gnetError("Unable to request CheckText for string %s", debugSTring);
	return false;
	
}

netLicensePlateCheck::ReturnCode netLicensePlateCheckMgr::GetStatusByToken( const CheckTextToken& inToken )
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netLicensePlateCheck::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			netLicensePlateCheck::ReturnCode retCode = m_RequestPool[slot].GetStatus();

#if !__NO_OUTPUT
			if(retCode != RESULT_PENDING)
				gnetDebug1("Result code for token 0x%x is %d", inToken, retCode);
#endif

			return retCode;
		}

		gnetError("Token 0x%x is invalid.  Slot %d is generation %d", inToken, slot, m_RequestPool[slot].GetGeneration() );
		return RESULT_INVALID_TOKEN;
	}

	return RESULT_INVALID_TOKEN;
}

unsigned netLicensePlateCheckMgr::GetCount( const CheckTextToken& inToken ) 
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netLicensePlateCheck::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			return m_RequestPool[slot].GetCount();
		}
	}

	return 0;
}

const char* netLicensePlateCheckMgr::GetPlate( const CheckTextToken& inToken, const int& index ) 
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netLicensePlateCheck::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			return m_RequestPool[slot].GetPlate(index);
		}
	}

	return 0;
}

const char* netLicensePlateCheckMgr::GetPlateData( const CheckTextToken& inToken, const int& index ) 
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netLicensePlateCheck::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			return m_RequestPool[slot].GetPlateData(index);
		}
	}

	return 0;
}

bool netLicensePlateCheckMgr::UpdateWithAddPlate( const CheckTextToken& inToken, rlScLicensePlateInfo& plate )
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netLicensePlateCheck::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			return m_RequestPool[slot].AddPlate(plate);
		}
	}

	return false;
}

} //namespace netLicensePlateCheck

netLicensePlateCheck::netLicensePlateCheckMgr s_netLicensePlateCheckMgr;

bool netLicensePlateCheck::ValidateLicensePlate( const char* inString, const rlScLanguage language, CheckTextToken& outToken )
{
	if (!gnetVerify(inString))
	{
		return false;
	}

	//String size
	u32 len = (u32)istrlen(inString);
	if (len > rlScLicensePlateInfo::MAX_PLATE_NAME_CHARS)
	{
		gnetDebug3("%s", inString);
		gnetError("String is too large for license plate checking [len:%d Max:%d Diff:%d]", len, rlScLicensePlateInfo::MAX_PLATE_NAME_CHARS, len - rlScLicensePlateInfo::MAX_PLATE_NAME_CHARS );
		return false;
	}

	//Check for any valid credentials...who cares
	int validGamerIndex = -1;
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS && !RL_IS_VALID_LOCAL_GAMER_INDEX(validGamerIndex); ++i)
	{
		if(rlRos::GetCredentials(i).IsValid())
		{
			validGamerIndex = i;
		}
	}

	if (!RL_VERIFY_LOCAL_GAMER_INDEX(validGamerIndex))
	{
		return false;
	}

	return s_netLicensePlateCheckMgr.CheckText(validGamerIndex, inString, language, outToken);
}


netLicensePlateCheck::ReturnCode netLicensePlateCheck::GetStatusForRequest( const CheckTextToken& token )
{
	return s_netLicensePlateCheckMgr.GetStatusByToken(token);
}

unsigned netLicensePlateCheck::GetCount( const CheckTextToken& token )
{
	return s_netLicensePlateCheckMgr.GetCount(token);
}

const char* netLicensePlateCheck::GetPlate( const CheckTextToken& token, const int& index )
{
	return s_netLicensePlateCheckMgr.GetPlate(token, index);
}

const char* netLicensePlateCheck::GetPlateData( const CheckTextToken& token, const int& index )
{
	return s_netLicensePlateCheckMgr.GetPlateData(token, index);
}

bool netLicensePlateCheck::UpdateWithAddPlate(const CheckTextToken& inToken, rage::rlScLicensePlateInfo& plate)
{
	return s_netLicensePlateCheckMgr.UpdateWithAddPlate(inToken, plate);
}

//--------------------------------------------------

namespace netLicensePlateAdd {

	static AddTextToken GenerateToken(u16 slot, u16 generation)
	{
		//TOKEN = [ generation (16 bits) | slot (16 bits) ]
		AddTextToken newToken = ((u32)generation) << 16 | slot;
		return newToken;
	}

	static void DisectToken(const AddTextToken& inToken, u16& outSlot, u16& outGeneration)
	{
		outSlot = (u16) inToken & 0x0000ffff;
		outGeneration = (u16)((inToken & 0xffff0000) >> 16);
	}

	//////////////////////////////////////////////////////////////////////////
	//	
	//	netLicensePlateAddRequest - helper class to wrap a request to change text.
	//
	//////////////////////////////////////////////////////////////////////////
	class netLicensePlateAddRequest
	{
	public:
		netLicensePlateAddRequest() : m_generation(0) {}

		bool IsPending() const { return m_status.Pending(); }
		void Reset()
		{
			if (m_status.Pending())
			{
				rlSocialClub::Cancel(&m_status);
			}

			m_status.Reset();
			m_generation++;
		}

		bool Add(int localGamerIndex, const char* inText, const char* inData, const rlScLanguage)
		{
			if (IsPending())
			{
				return false;
			}

			Reset();

			return rlSocialClub::AddLicensePlate(localGamerIndex, inText, inData, &m_status);
		}

		ReturnCode GetStatus()
		{
			if (m_status.Pending())
			{
				gnetDebug3("GetStatus--Pending");
				return RESULT_PENDING;
			}
			else if (m_status.Succeeded())
			{
				return RESULT_OK;
			}
			else if (m_status.Failed())
			{
#if !__NO_OUTPUT
				int err = m_status.GetResultCode();
				gnetError( "Error code in netLicensePlateAdd: %d", err);
#endif
				return RESULT_ERROR;
			}

			gnetDebug3("GetStatus--Other-->RESULT_ERROR");
			return RESULT_ERROR;
		}

		const u16 GetGeneration() const { return m_generation; }
		const AddTextToken GenerateToken (const u16 slot) const 
		{
			return netLicensePlateAdd::GenerateToken(slot, m_generation);
		}

	private:
		netStatus m_status;
		u16 m_generation;

	}; //class netLicensePlateAddRequest


	//////////////////////////////////////////////////////////////////////////
	//
	//  netLicensePlateAddMgr - Manage set of requests to add request, using 
	//		a token system to reference them.
	//
	//////////////////////////////////////////////////////////////////////////
	class netLicensePlateAddMgr
	{
	public:
		netLicensePlateAddMgr() : m_lastUsedSlot(0xFFFF) {}

		bool Add(int localGamerIndex, const char* inText, const char* inData, const rlScLanguage language, AddTextToken& outToken);
		ReturnCode GetStatusByToken(const AddTextToken& inToken);

	private:

		static const int MAX_REQUESTS = 1;
		netLicensePlateAddRequest m_RequestPool[MAX_REQUESTS];
		u16 m_lastUsedSlot;

	}; //class netLicensePlateAddMgr

	bool netLicensePlateAddMgr::Add( int localGamerIndex, const char* inText, const char* inData, const rlScLanguage language, AddTextToken& outToken )
	{
#if !__NO_OUTPUT
		char debugSTring[64];
		safecpy(debugSTring, inText);
#endif

		//First time
		if (m_lastUsedSlot == 0xFFFF)
		{
			m_lastUsedSlot = 0;
			if (!m_RequestPool[m_lastUsedSlot].IsPending())
			{
				if(m_RequestPool[m_lastUsedSlot].Add(localGamerIndex, inText, inData, language))
				{
					outToken = m_RequestPool[m_lastUsedSlot].GenerateToken(m_lastUsedSlot);
					gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
					return true;
				}
			}
		}

		//Find a slot.
		u16 slotToUse = m_lastUsedSlot + 1;
		do 
		{
			//Wrap around
			if (slotToUse >= MAX_REQUESTS)
			{
				slotToUse = 0;
			}

			//See if this one is available
			if (!m_RequestPool[slotToUse].IsPending())
			{
				if(gnetVerify(m_RequestPool[slotToUse].Add(localGamerIndex, inText, inData, language)))
				{
					outToken = m_RequestPool[slotToUse].GenerateToken(slotToUse);
					gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
					m_lastUsedSlot = slotToUse;
					return true;
				}
			}

			slotToUse++;

		} while (slotToUse != m_lastUsedSlot);

		gnetError("Unable to request Add for string %s", debugSTring);
		return false;

	}

	netLicensePlateAdd::ReturnCode netLicensePlateAddMgr::GetStatusByToken( const AddTextToken& inToken )
	{
		u16 slot = 0xffff;
		u16 generation = 0;
		netLicensePlateAdd::DisectToken(inToken, slot, generation);

		//Check if it's a valid token. 
		//NOTE 0 will get passed into here (from script)
		if(slot < MAX_REQUESTS && generation > 0)
		{
			if(m_RequestPool[slot].GetGeneration() == generation)
			{
				netLicensePlateAdd::ReturnCode retCode = m_RequestPool[slot].GetStatus();

#if !__NO_OUTPUT
				if(retCode != RESULT_PENDING)
					gnetDebug1("Result code for token 0x%x is %d", inToken, retCode);
#endif

				return retCode;
			}

			gnetError("Token 0x%x is invalid.  Slot %d is generation %d", inToken, slot, m_RequestPool[slot].GetGeneration() );
			return RESULT_INVALID_TOKEN;
		}

		return RESULT_INVALID_TOKEN;
	}

} //namespace netLicensePlateAdd

//--------------------------------------------------

netLicensePlateAdd::netLicensePlateAddMgr s_netLicensePlateAddMgr;

bool netLicensePlateAdd::AddLicensePlate(const int localGamerIndex, const char* inString, const char* inData, const rage::rlScLanguage language, AddTextToken& outToken)
{
	if (!gnetVerify(inString))
	{
		return false;
	}

	if (!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
	{
		return false;
	}

	return s_netLicensePlateAddMgr.Add(localGamerIndex, inString, (inData && strlen(inData) > 0) ? inData : NULL, language, outToken);
}

netLicensePlateAdd::ReturnCode netLicensePlateAdd::GetStatusForRequest(const AddTextToken& token)
{
	return s_netLicensePlateAddMgr.GetStatusByToken(token);
}


//--------------------------------------------------

namespace netLicensePlateIsValid {

	static IsValidTextToken GenerateToken(u16 slot, u16 generation)
	{
		//TOKEN = [ generation (16 bits) | slot (16 bits) ]
		IsValidTextToken newToken = ((u32)generation) << 16 | slot;
		return newToken;
	}

	static void DisectToken(const IsValidTextToken& inToken, u16& outSlot, u16& outGeneration)
	{
		outSlot = (u16) inToken & 0x0000ffff;
		outGeneration = (u16)((inToken & 0xffff0000) >> 16);
	}

	//////////////////////////////////////////////////////////////////////////
	//	
	//	netLicensePlateIsValidRequest - helper class to wrap a request text validity.
	//
	//////////////////////////////////////////////////////////////////////////
	class netLicensePlateIsValidRequest
	{
	public:
		netLicensePlateIsValidRequest() : m_generation(0), m_bIsValid(false), m_bIsProfane(false), m_bIsReserved(false), m_bIsMalformed(false) {}

		bool IsPending() const { return m_status.Pending(); }
		void Reset()
		{
			if (m_status.Pending())
			{
				rlSocialClub::Cancel(&m_status);
			}

			m_status.Reset();
			m_generation++;

			m_bIsValid = false;
			m_bIsProfane = false;
			m_bIsReserved = false;
			m_bIsMalformed = false;
		}

		bool IsValid(int localGamerIndex, const char* inText, const rlScLanguage)
		{
			if (IsPending())
			{
				return false;
			}

			Reset();

			return rlSocialClub::IsValidLicensePlate(localGamerIndex, inText, &m_bIsValid, &m_bIsProfane, &m_bIsReserved, &m_bIsMalformed, &m_status);
		}

		ReturnCode GetStatus()
		{
			if (m_status.Pending())
			{
				gnetDebug3("GetStatus--Pending");
				return RESULT_PENDING;
			}
			else if (m_status.Succeeded())
			{
				return RESULT_OK;
			}
			else if (m_status.Failed())
			{
#if !__NO_OUTPUT
				int err = m_status.GetResultCode();
				gnetError( "Error code in netLicensePlateIsValid: %d", err);
#endif

				if (m_bIsMalformed)
					return RESULT_MALFORMED;

				if (m_bIsReserved)
					return RESULT_RESERVED;

				if (m_bIsProfane)
					return RESULT_PROFANE;

				if (!m_bIsValid)
					return RESULT_NOTVALID;

				return RESULT_ERROR;
			}

			gnetDebug3("GetStatus--Other-->RESULT_ERROR");
			return RESULT_ERROR;
		}

		const u16 GetGeneration() const { return m_generation; }
		const IsValidTextToken GenerateToken (const u16 slot) const 
		{
			return netLicensePlateIsValid::GenerateToken(slot, m_generation);
		}

	private:
		netStatus m_status;
		u16 m_generation;
		bool m_bIsValid;
		bool m_bIsProfane;
		bool m_bIsReserved;
		bool m_bIsMalformed;

	}; //class netLicensePlateIsValidRequest


	//////////////////////////////////////////////////////////////////////////
	//
	//  netLicensePlateIsValidMgr - Manage set of requests to isvalid request, using 
	//		a token system to reference them.
	//
	//////////////////////////////////////////////////////////////////////////
	class netLicensePlateIsValidMgr
	{
	public:
		netLicensePlateIsValidMgr() : m_lastUsedSlot(0xFFFF) {}

		bool IsValid(int localGamerIndex, const char* inText, const rlScLanguage language, IsValidTextToken& outToken);
		ReturnCode GetStatusByToken(const IsValidTextToken& inToken);

	private:

		static const int MAX_REQUESTS = 1;
		netLicensePlateIsValidRequest m_RequestPool[MAX_REQUESTS];
		u16 m_lastUsedSlot;

	}; //class netLicensePlateIsValidMgr

	bool netLicensePlateIsValidMgr::IsValid( int localGamerIndex, const char* inText, const rlScLanguage language, IsValidTextToken& outToken )
	{
#if !__NO_OUTPUT
		char debugSTring[64];
		safecpy(debugSTring, inText);
#endif

		//First time
		if (m_lastUsedSlot == 0xFFFF)
		{
			m_lastUsedSlot = 0;
			if (!m_RequestPool[m_lastUsedSlot].IsPending())
			{
				if(m_RequestPool[m_lastUsedSlot].IsValid(localGamerIndex, inText, language))
				{
					outToken = m_RequestPool[m_lastUsedSlot].GenerateToken(m_lastUsedSlot);
					gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
					return true;
				}
			}
		}

		//Find a slot.
		u16 slotToUse = m_lastUsedSlot + 1;
		do 
		{
			//Wrap around
			if (slotToUse >= MAX_REQUESTS)
			{
				slotToUse = 0;
			}

			//See if this one is available
			if (!m_RequestPool[slotToUse].IsPending())
			{
				if(gnetVerify(m_RequestPool[slotToUse].IsValid(localGamerIndex, inText, language)))
				{
					outToken = m_RequestPool[slotToUse].GenerateToken(slotToUse);
					gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
					m_lastUsedSlot = slotToUse;
					return true;
				}
			}

			slotToUse++;

		} while (slotToUse != m_lastUsedSlot);

		gnetError("Unable to request IsValid for string %s", debugSTring);
		return false;
	}

	netLicensePlateIsValid::ReturnCode netLicensePlateIsValidMgr::GetStatusByToken( const IsValidTextToken& inToken )
	{
		u16 slot = 0xffff;
		u16 generation = 0;
		netLicensePlateIsValid::DisectToken(inToken, slot, generation);

		//Check if it's a valid token. 
		//NOTE 0 will get passed into here (from script)
		if(slot < MAX_REQUESTS && generation > 0)
		{
			if(m_RequestPool[slot].GetGeneration() == generation)
			{
				netLicensePlateIsValid::ReturnCode retCode = m_RequestPool[slot].GetStatus();

#if !__NO_OUTPUT
				if(retCode != RESULT_PENDING)
					gnetDebug1("Result code for token 0x%x is %d", inToken, retCode);
#endif

				return retCode;
			}

			gnetError("Token 0x%x is invalid.  Slot %d is generation %d", inToken, slot, m_RequestPool[slot].GetGeneration() );
			return RESULT_INVALID_TOKEN;
		}

		return RESULT_INVALID_TOKEN;
	}

} //namespace netLicensePlateIsValid

//--------------------------------------------------

netLicensePlateIsValid::netLicensePlateIsValidMgr s_netLicensePlateIsValidMgr;

bool netLicensePlateIsValid::IsValidLicensePlate(const char* inString, const rage::rlScLanguage language, IsValidTextToken& outToken)
{
	if (!gnetVerify(inString))
	{
		return false;
	}

	//Check for any valid credentials...who cares
	int validGamerIndex = -1;
	for(int i = 0; i < RL_MAX_LOCAL_GAMERS && !RL_IS_VALID_LOCAL_GAMER_INDEX(validGamerIndex); ++i)
	{
		if(rlRos::GetCredentials(i).IsValid())
		{
			validGamerIndex = i;
		}
	}

	if (!RL_VERIFY_LOCAL_GAMER_INDEX(validGamerIndex))
	{
		return false;
	}

	return s_netLicensePlateIsValidMgr.IsValid(validGamerIndex, inString, language, outToken);
}

netLicensePlateIsValid::ReturnCode netLicensePlateIsValid::GetStatusForRequest(const IsValidTextToken& token)
{
	return s_netLicensePlateIsValidMgr.GetStatusByToken(token);
}

// -------------------------
namespace netLicensePlateChange
{
	//PURPOSE
	//  Helper class to help making plate change requests.
	class netLicensePlateChangeRequest
	{
	public:
		bool IsPending() const { return m_status.Pending(); }

		void Reset()
		{
			if (m_status.Pending())
			{
				rlSocialClub::Cancel(&m_status);
			}

			m_count = 0;
			m_status.Reset();
		}

		bool Change(const int localGamerIndex
					,const char* OldPlateText
					,const char* newPlateText
					,const char* plateData)
		{
			if (IsPending())
				return false;

			Reset();

			return rlSocialClub::ChangePlateDataNoInsert(localGamerIndex
															,&m_LicensePlateInfo
															,1
															,&m_count 
															,OldPlateText
															,newPlateText
															,plateData
															,&m_status);
		}

		ReturnCode GetStatus()
		{
			if (m_status.Pending())
			{
				gnetDebug3("netLicensePlateChangeRequest:: GetStatus() - Pending");
				return RESULT_PENDING;
			}
			else if (m_status.Succeeded())
			{
				return RESULT_OK;
			}
			else if (m_status.Failed())
			{
				gnetError("netLicensePlateChangeRequest:: GetStatus() - Error code='%d'", m_status.GetResultCode());
				return RESULT_ERROR;
			}

			gnetDebug3("netLicensePlateChangeRequest:: GetStatus() - RESULT_ERROR");

			return RESULT_ERROR;
		}

	private:
		netStatus  m_status;
		u32        m_count;
		rlScLicensePlateInfo m_LicensePlateInfo;
	};

	//////////////////////////////////////////////////////////////////////////
	//
	//  netLicensePlateChangeMgr - Manage set of requests to change license
	//                            plate data
	//
	//////////////////////////////////////////////////////////////////////////
	class netLicensePlateChangeMgr
	{
	public:
		netLicensePlateChangeMgr() {}

		bool Change(const int localGamerIndex
					,const char* OldPlateText
					,const char* newPlateText
					,const char* plateData);

		ReturnCode GetStatus();

	private:
		netLicensePlateChangeRequest m_Request;
	};

	bool netLicensePlateChangeMgr::Change(const int localGamerIndex
											,const char* oldPlateText
											,const char* newPlateText
											,const char* plateData)
	{
		if (!m_Request.IsPending())
		{
			if(m_Request.Change(localGamerIndex, oldPlateText, newPlateText, plateData))
			{
				gnetDebug1("netLicensePlateChangeMgr::Change - Started request change for <old='%s', new='%s', data='%s'>", oldPlateText, newPlateText, plateData);
				return true;
			}
		}

		gnetDebug1("netLicensePlateChangeMgr::Change - FAILED to start request change for <old='%s', new='%s', data='%s'>", oldPlateText, newPlateText, plateData);
		return false;
	}

	netLicensePlateChange::ReturnCode  netLicensePlateChangeMgr::GetStatus( )
	{
		netLicensePlateChange::ReturnCode retCode = m_Request.GetStatus();

#if !__NO_OUTPUT
		if(retCode != RESULT_PENDING)
			gnetDebug1("netLicensePlateChangeMgr:: Result code is '%d'.", retCode);
#endif // !__NO_OUTPUT

		return retCode;
	}

	netLicensePlateChange::netLicensePlateChangeMgr s_netLicensePlateChangeMgr;

	bool ChangeLicensePlate(const int localGamerIndex
							,const char* oldPlateText
							,const char* newPlateText
							,const char* plateData)
	{
		if (!gnetVerify(oldPlateText))
			return false;

		if (!gnetVerify(newPlateText))
			return false;

		if (!gnetVerify(plateData))
			return false;

		if (!RL_VERIFY_LOCAL_GAMER_INDEX(localGamerIndex))
			return false;

		return s_netLicensePlateChangeMgr.Change(localGamerIndex, oldPlateText, newPlateText, plateData);
	}

	netLicensePlateChange::ReturnCode GetStatusForRequest( )
	{
		return s_netLicensePlateChangeMgr.GetStatus();
	}
};

// eof
