// 
// netArrayHandlerTypes.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_ARRAY_HANDLER_TYPES_H
#define NET_ARRAY_HANDLER_TYPES_H

#include "fwnet/netarrayhandler.h"
#include "fwnet/netarraymgr.h"
#include "fwnet/netchannel.h"
#include "fwnet/netinterface.h"
#include "fwnet/netlogsplitter.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobject.h"
#include "fwnet/netserialisers.h"
#include "fwnet/netsyncdata.h"
#include "fwnet/netplayer.h"
#include "fwnet/netplayermgrbase.h"
#include "fwscript/scripthandler.h"
#include "fwscript/scriptid.h"
#include "fwscript/scriptobjinfo.h"
#include "rline/rlgamerinfo.h"		// for rlGamerId
#include "atl/atfixedstring.h"

namespace rage
{

enum NetworkBaseArrayTypes
{
	HOST_BROADCAST_DATA_ARRAY_HANDLER,
	PLAYER_BROADCAST_DATA_ARRAY_HANDLER,
	NUM_BASE_ARRAY_HANDLER_TYPES
};

class scriptHandler;

//========================================================================================================================================
// netSharedArrayHandler
//========================================================================================================================================

//PURPOSE
// Deals with shared arrays (where individual elements are arbitrated by different machines)
//
//	T - the type of the array elements
//	Derived - the name of the class derived from this class.
//
template<class T, class Derived>
class netSharedArrayHandler : public netArrayHandler<T, Derived>
{
public:

	netSharedArrayHandler(NetworkArrayHandlerType handlerType, 
							T* arrayData,
							unsigned numElements, 
							unsigned maxNumPlayers = MAX_NUM_PHYSICAL_PLAYERS) :
	netArrayHandler<T, Derived>(handlerType, arrayData, numElements, maxNumPlayers)
	{
		m_ElementArbitrators.Resize(numElements);
	}

	~netSharedArrayHandler()
	{
		m_ElementArbitrators.Reset();
	}

	void Init()
	{
		for (unsigned i=0; i < this->m_NumArrayElements; i++)
		{
			m_ElementArbitrators[i] = INVALID_PLAYER_INDEX;
		}

		netArrayHandler<T, Derived>::Init();
	}

	void PlayerHasLeft(const netPlayer& player)
	{
		netArrayHandler<T, Derived>::PlayerHasLeft(player);

		// clear any elements the player is in control of
		for (unsigned i=0; i<this->m_NumArrayElements; i++)
		{
			if (m_ElementArbitrators[i] == player.GetPhysicalPlayerIndex())
			{
				if (RemoveElementWhenArbitratorLeaves(i))
				{
					this->SetElementEmpty(i);
					this->ResetElementSyncData(i);
				}
			}
		}
	}

	bool IsArrayLocallyArbitrated() const
	{
		// some elements are locally arbitrated, so return true for the array also. 
		return true;
	}

	const netPlayer* GetPlayerArbitratorOfArray() const  
	{ 
		// no one player arbitrates over the whole array
		return NULL; 
	}

    bool IsSharedArray() const
    {
        return true;
    }

	bool IsElementLocallyArbitrated(unsigned index) const
	{
		const netPlayer* arbitrator = GetElementArbitration(index);

		return (arbitrator && arbitrator->IsLocal());
	}

	virtual bool IsElementRemotelyArbitrated(unsigned index) const 
	{
		const netPlayer* arbitrator = GetElementArbitration(index);

		return (arbitrator && !arbitrator->IsLocal());
	}

	//PURPOSE
	//	Get the player arbitrating the given element 
	const netPlayer*	GetElementArbitration(unsigned index) const
	{
		netPlayer* playerOwner = NULL;

		if (m_ElementArbitrators[index] != INVALID_PLAYER_INDEX)
		{
			playerOwner = netInterface::GetPhysicalPlayerFromIndex(m_ElementArbitrators[index]);
		}

		return playerOwner;
	}

	bool SetElementDirty(unsigned index)
	{
		if (this->GetElementArbitration(index) == NULL && this->IsElementEmpty(index))
		{
			Assertf(0, "Trying to dirty an empty element with no arbitrator (%d) in shared array %s", index, this->GetHandlerName());
			return false;
		}

		bool bDirtied = netArrayHandler<T, Derived>::SetElementDirty(index);

		if (bDirtied)
		{
#if ENABLE_NETWORK_LOGGING
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
			const netPlayer* pPlayer = GetElementArbitration(index);
			log.WriteDataValue("Element authority", pPlayer ? pPlayer->GetLogName() : "none");
#endif // ENABLE_NETWORK_LOGGING

			if (this->GetSyncData() && !this->IsElementEmpty(index) && !GetElementArbitration(index))
			{
				SetElementArbitration(index, *netInterface::GetLocalPlayer()); // set our machine as arbitrating this element
			}
		}

		return bDirtied;
	}

	void GetNumUsedElements(u32 &local, u32 &remote, u32& emptyArbitrated)
	{
		local = remote = emptyArbitrated = 0;

		for (u32 i=0; i<this->m_NumArrayElements; i++)
		{
			if (!this->IsElementEmpty(i))
			{
				if (IsElementLocallyArbitrated(i))
				{
					local++;
				}
				else
				{
					remote++;
				}
			}
			else if (m_ElementArbitrators[i] != INVALID_PLAYER_INDEX)
			{
				emptyArbitrated++;
			}
		}
	}

protected:

	virtual void ResetElementSyncData(unsigned index)
	{
		netArrayHandler<T, Derived>::ResetElementSyncData(index);
		ClearElementArbitration(index);
	}

	virtual PlayerFlags GetElementScope(unsigned UNUSED_PARAM(index)) const
	{
		return netInterface::GetPlayerMgr().GetRemotePhysicalPlayersBitmask();
	}

	virtual bool RemoveElementWhenArbitratorLeaves(unsigned UNUSED_PARAM(index)) const { return true; }

	//PURPOSE
	//	Returns true if the element data just read in an update message can be applied to the array itself.
	//PARAMS
	//	index			- the index of the array element.
	//	player			- the player who sent the update message.
	//  elementEmpty	- if true, the element is empty.
	bool CanApplyElementData(unsigned index, const netPlayer& player, bool elementEmpty) 
	{
		PhysicalPlayerIndex currArbitratorIndex = m_ElementArbitrators[index];

		if (currArbitratorIndex != INVALID_PLAYER_INDEX &&
			currArbitratorIndex != player.GetPhysicalPlayerIndex())
		{
			LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());
			LOGGING_ONLY(netLogSplitter logSplitter(netInterface::GetMessageLog(), log));

			const netPlayer* currentArbitrator = GetElementArbitration(index);

			// if there is a conflict of arbitration over this element, then decide who is the proper arbitrator
			if (ResolveArbitrationConflict(index, player))
			{
				if (AssertVerify(currentArbitrator))
				{
					LOGGING_ONLY(logSplitter.WriteDataValue("ARBITRATION LOSS", "Player %s loses control of element", currentArbitrator->GetLogName()));
				}

				if (!this->IsElementEmpty(index))
				{
					// previous owner has lost control of the element so handle this and change the arbitration
					if (currentArbitrator->IsLocal())
					{
						HandleLossOfArbitration(index);
					}
					else
					{
						this->SetElementEmpty(index);
						this->ResetElementSyncData(index);
					}
				}
			}
			else
			{
				// the player does not control this element
				const char   *playerName          = currentArbitrator ? currentArbitrator->GetLogName() : 0;
				playerName = playerName ? playerName : "not in the session";

				LOGGING_ONLY(logSplitter.WriteDataValue("FAIL", "Element is arbitrated by player %s", playerName));

				return false;
			}
		}

		if (elementEmpty)
		{
			// the element is being emptied by the current arbitrator, so it can be freed.
			ClearElementArbitration(index);
		}
		else
		{
			SetElementArbitration(index, player);
		}

		return true;
	}

	//PURPOSE
	//	Set the given player as the arbitrator of the given element 
	virtual void SetElementArbitration(unsigned index, const netPlayer& player)
	{
		Assert((this->IsPlayerInScope(player)));

		unsigned playerIndex = this->GetSyncDataIndexForPlayer(player);

		if (m_ElementArbitrators[index] != playerIndex)
		{
#if ENABLE_NETWORK_LOGGING
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();

			char logStr[30];
			formatf(logStr, sizeof(logStr), "SET_ELEMENT_%d_ARBITRATION", index);
			NetworkLogUtils::WriteLogEvent(log, logStr, "%s", this->GetHandlerName());

			if (this->GetIdentifier())
			{
				log.WriteDataValue("Identifier", this->GetIdentifier()->GetLogName());
			}

			if (player.IsValid())
			{
				log.WriteDataValue("Arbitrator", "%s", player.GetLogName());
			}
			else
			{
				log.WriteDataValue("Arbitrator", "Player %d", playerIndex);
			}
#endif // ENABLE_NETWORK_LOGGING

			m_ElementArbitrators[index] = static_cast<PhysicalPlayerIndex>(playerIndex);

			if (player.IsLocal())
			{
				// dirty the element so that the element data will be sent out to all players
				netArrayHandler<T, Derived>::SetElementDirty(index);
			}
			else
			{
				netArrayHandler<T, Derived>::ResetElementSyncData(index);
			}
		}
	}

	//PURPOSE
	//	Clear the player arbitration of the given element
	virtual void ClearElementArbitration(unsigned index)
	{
		if (m_ElementArbitrators[index] != INVALID_PLAYER_INDEX)
		{
			m_ElementArbitrators[index] = INVALID_PLAYER_INDEX;

#if ENABLE_NETWORK_LOGGING
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();

			char logStr[30];
			formatf(logStr, sizeof(logStr), "CLEAR_ELEMENT_%d_ARBITRATION", index);
			NetworkLogUtils::WriteLogEvent(log, logStr, "%s", this->GetHandlerName());

			if (this->GetIdentifier())
			{
				log.WriteDataValue("Identifier", this->GetIdentifier()->GetLogName());
			}
#endif // ENABLE_NETWORK_LOGGING
		}
	}

	//PURPOSE
	//	Resolves a conflict over the arbitration of the given element.
	//RETURNS
	//	True if the given player is the rightful arbitrator.
	virtual bool ResolveArbitrationConflict(unsigned index, const netPlayer& player) const
	{
		// the player with the highest gamer id becomes the rightful arbitrator
		const netPlayer* playerOwner = GetElementArbitration(index);

		return (playerOwner && playerOwner->GetRlGamerId() < player.GetRlGamerId());
	}

	//PURPOSE
	//	Handles the loss of arbitration of an array element. 
	virtual void HandleLossOfArbitration(unsigned index)
	{
		u32 newSlot = 0;

		LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());

		if (MoveElementToFreeSlot(index, newSlot))
		{
			LOGGING_ONLY(NetworkLogUtils::WriteLogEvent(log, "MOVE_LOCAL_ELEMENT", "%s", this->GetHandlerName()));
			LOGGING_ONLY(log.WriteDataValue("New slot", "%d", newSlot));
			SetElementDirty(newSlot);
		}
		else
		{
			LOGGING_ONLY(log.Log("\t\t## %s : Can't move element %d to free slot. No slots left. ##\r\n", this->GetHandlerName(), index));
			this->SetElementEmpty(index);
			this->ResetElementSyncData(index);
		}
	}

	//PURPOSE
	//	Moves a locally arbitrated element to a free slot after we lose arbitration over it in an arbitration conflict
	virtual bool MoveElementToFreeSlot(unsigned index, unsigned& newSlot) = 0;

	//PURPOSE
	//	Returns true if an element can be cleared
	virtual bool ElementCanBeCleared(unsigned UNUSED_PARAM(index)) const { return false; }

protected:

	// An array of the player arbitrators of each element.
	atArray<PhysicalPlayerIndex> m_ElementArbitrators;
};

//========================================================================================================================================
// netSharedArrayHandlerWithElementScope
//========================================================================================================================================

//PURPOSE
// A shared array handler where each element can be in scope with a subset of remote machines and can also migrate arbitration.
template<class T, class Derived>
class netSharedArrayHandlerWithElementScope : public netSharedArrayHandler<T, Derived>
{
protected:

	// Shared arrays with scope have to identify slots using ids, this is because the data in a slot on one machine may be stored in a different slot on another
	// machine. This is because we can have the situation where 2 machines are using the same slot for a different element and are not in scope with each other, 
	// but a 3rd machine is in scope with both slots and so must move one of them into a free slot locally. 
	// eg.
	// Machine A has dataA in slot 0, not in scope with machine B but in scope with machine C
	// Machine B has dataB in slot 0, not in scope with machine A but in scope with machine C
	// Machine C receives dataA and dataB, and keeps dataA in slot 0 but has to move dataB to slot 1.

	struct sSharedArrayElementInfo
	{
		PlayerFlags  m_ElementScope;		// the players the element is in scope with (when local)
		u32			 m_ElementID;			// the unique id identifying this element
	};

	static const unsigned INVALID_ELEMENT_ID = 0;
	static const unsigned INVALID_SLOT = (unsigned)~0;

public:

	netSharedArrayHandlerWithElementScope(NetworkArrayHandlerType handlerType, 
											T* arrayData,
											unsigned numElements, 
											unsigned maxNumPlayers = MAX_NUM_PHYSICAL_PLAYERS) :
	netSharedArrayHandler<T, Derived>(handlerType, arrayData, numElements, maxNumPlayers)
	{
		m_ElementInfo.Resize(numElements);
	}

	~netSharedArrayHandlerWithElementScope()
	{
		m_ElementInfo.Reset();
	}

	void Init()
	{
		netSharedArrayHandler<T, Derived>::Init();

		for (unsigned i=0; i < this->m_NumArrayElements; i++)
		{
			m_ElementInfo[i].m_ElementScope = 0;
			m_ElementInfo[i].m_ElementID = INVALID_ELEMENT_ID;
		}

		m_updateNum = 0;
	}

	void Update()
	{
		if (ElementsCanMigrate())
		{
			for (unsigned i=0; i<this->m_NumArrayElements; i++)
			{
				if (this->m_ElementArbitrators[i] != INVALID_PLAYER_INDEX)
				{
					UpdateElementArbitration(i);
				}
			}
		}

		// optimisation: update the scope in batches
		const unsigned NUM_UPDATE_BATCHES = 8;

		u32 numElementsPerBatch = 1;
		
		if (this->m_NumArrayElements > NUM_UPDATE_BATCHES)
		{
			numElementsPerBatch = this->m_NumArrayElements / NUM_UPDATE_BATCHES;
		}

		unsigned batchStart = numElementsPerBatch * m_updateNum;
		unsigned batchEnd = batchStart + numElementsPerBatch;

		if (m_updateNum == NUM_UPDATE_BATCHES-1)
		{
			batchEnd = this->m_NumArrayElements;
			m_updateNum = 0;
		}
		else
		{
			m_updateNum++;
		}

		RecalculateElementScope(batchStart, batchEnd);

		netSharedArrayHandler<T, Derived>::Update();
	}

	bool SetElementDirty(unsigned index)
	{
		bool bDirtied = netSharedArrayHandler<T, Derived>::SetElementDirty(index);

		if (bDirtied)
		{
			bool bElementCanBeCleared = this->CanHaveEmptyElements() && this->ElementCanBeCleared(index);

			// do not recalculate element scope when the element can be cleared, as this will remove the element and potentially cause a crash in any code that
			// is trying to use a pointer to the element after it has been dirtied
			if (!bElementCanBeCleared)
			{
				RecalculateElementScope(index, index+1);
			}

			if (m_ElementInfo[index].m_ElementID == INVALID_ELEMENT_ID)
			{
				Assertf(!this->IsElementEmpty(index), "Assigning a new element id to an empty shared array element (%d) in array %s", index, this->GetHandlerName());

				u32 newId = GetElementID(index);
#if __ASSERT
				for (unsigned i=0; i<this->m_NumArrayElements; i++)
				{
					if (m_ElementInfo[i].m_ElementID == newId)
					{
						Assertf(0, "Trying to add a new element to shared array %s, with an id %d that is already being used by slot %d", this->GetHandlerName(), newId, i);
						break;
					}
				}
#endif
				m_ElementInfo[index].m_ElementID = newId;

				Assert(m_ElementInfo[index].m_ElementID != INVALID_ELEMENT_ID);
			}
		}

		return bDirtied;
	}

	void PlayerHasLeft(const netPlayer& player)
	{
		for (unsigned i=0; i<this->m_NumArrayElements; i++)
		{
			m_ElementInfo[i].m_ElementScope &= ~(1<<player.GetPhysicalPlayerIndex());
		}

		netSharedArrayHandler<T, Derived>::PlayerHasLeft(player);
	}

	virtual u32 GetSizeOfElementIndex() const { return GetSizeOfElementID(); }

	virtual bool MoveElementToFreeSlot(unsigned, unsigned&) { return false; }

	u32 GetElementIDForIndex(unsigned index) const { return m_ElementInfo[index].m_ElementID; }

	bool IsIDInUse(u32 id) const
	{
		for (unsigned i=0; i<this->m_NumArrayElements; i++)
		{
			if (m_ElementInfo[i].m_ElementID == id)
			{
				return true;
			}
		}

		return false;
	}

	//PURPOSE
	//	Decides whether to migrate the arbitration of an array element
	const netPlayer* UpdateElementArbitration(unsigned index, const netPlayer* pCandidateArbitrator = NULL)
	{
		gnetAssert(ElementsCanMigrate());
		
		const netPlayer* targetArbitrator = this->GetTargetElementArbitration(index);

		// don't migrate a locally controlled element to a remote player if he has not received the element yet
		if (targetArbitrator &&
			targetArbitrator != pCandidateArbitrator &&
			this->IsElementLocallyArbitrated(index) && 
			!this->IsElementSyncedWithPlayer(index, this->GetSyncDataIndexForPlayer(*targetArbitrator)))
		{
			return this->GetElementArbitration(index);
		}

		if (targetArbitrator && targetArbitrator->GetPhysicalPlayerIndex() != this->m_ElementArbitrators[index])
		{
#if ENABLE_NETWORK_LOGGING
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();

			char logStr[30];
			formatf(logStr, sizeof(logStr), "MIGRATING_ELEMENT_%d", index);
			NetworkLogUtils::WriteLogEvent(log, logStr, "%s", this->GetHandlerName());

			if (this->GetIdentifier())
			{
				log.WriteDataValue("Identifier", this->GetIdentifier()->GetLogName());
			}

			const netPlayer* pCurrentArbitrator = this->GetElementArbitration(index);

			log.WriteDataValue("Previous arbitrator", "%s", pCurrentArbitrator ? pCurrentArbitrator->GetLogName() : "none");
			log.WriteDataValue("New arbitrator", "%s", targetArbitrator->GetLogName());
#endif // ENABLE_NETWORK_LOGGING

			this->SetElementArbitration(index, *targetArbitrator);
		}

		return targetArbitrator;
	}

	virtual PlayerFlags GetElementScope(unsigned index) const
	{
		return m_ElementInfo[index].m_ElementScope;
	}

protected:

	virtual bool AreElementsAlwaysInScope() const { return false; }

	virtual bool ElementsCanMigrate() const = 0;

	virtual u32 GetSizeOfElementID() const = 0;

	virtual u32 GetElementID(unsigned index) const = 0;

	virtual const netPlayer* GetTargetElementArbitration(unsigned index) const = 0;

	virtual bool IsElementInScopeInternal(unsigned index, const netPlayer& player) const 
	{	
		return (m_ElementInfo[index].m_ElementScope & (1<<player.GetPhysicalPlayerIndex())) != 0;
	}

	virtual void ElementScopeChanged(unsigned) {}

	virtual void ClearElementArbitration(unsigned index)
	{
		m_ElementInfo[index].m_ElementScope = 0;
		m_ElementInfo[index].m_ElementID = INVALID_ELEMENT_ID;

		netSharedArrayHandler<T, Derived>::ClearElementArbitration(index);
	}

	virtual void WriteElementIndex(datBitBuffer& bitBuffer, unsigned index)
	{
		LOGGING_ONLY(netInterface::GetArrayManager().GetLog().Log("\tELEMENT ID %u : \r\n", m_ElementInfo[index].m_ElementID));

		bitBuffer.WriteUns(m_ElementInfo[index].m_ElementID, GetSizeOfElementID());
	}

	virtual void ReadElementIndex(datBitBuffer& bitBuffer, unsigned& index)
	{
		bitBuffer.ReadUns(m_ElementID, GetSizeOfElementID());

		LOGGING_ONLY(netInterface::GetArrayManager().GetLog().Log("\tELEMENT ID %u : \r\n", m_ElementID));

		s32 freeSlot = -1;

		for (u32 i=0; i<this->m_NumArrayElements; i++)
		{
			if (this->m_ElementInfo[i].m_ElementID == m_ElementID)
			{
				index = i;
				return;
			}
			else if (freeSlot==-1 && this->IsElementEmpty(i) && !this->GetElementArbitration(i))
			{
				freeSlot = i;
			}
		}

		if (freeSlot != -1)
		{
			index = (unsigned) freeSlot; 
		}
		else
		{
			Assertf(0, "Ran out of free slots for shared array %s", this->GetHandlerName());
			index = INVALID_SLOT; 
		}
	}

	bool IsValidIndex(unsigned index)
	{
		return ((index == INVALID_SLOT) || netSharedArrayHandler<T, Derived>::IsValidIndex(index));
	}

	//PURPOSE
	//	Returns true if the element data just read in an update message can be applied to the array itself.
	//PARAMS
	//	index			- the index of the array element.
	//	player			- the player who sent the update message.
	//  elementEmpty	- if true, the element is empty.
	bool CanApplyElementData(unsigned index, const netPlayer& player, bool elementEmpty) 
	{
		LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());
		LOGGING_ONLY(netLogSplitter logSplitter(netInterface::GetMessageLog(), log));

		if (index == INVALID_SLOT)
		{
			LOGGING_ONLY(logSplitter.WriteDataValue("FAIL", "Ran out of free slots"));
			return false;
		}

		const netPlayer* pCurrentArbitrator = this->GetElementArbitration(index);

		if (ElementsCanMigrate())
		{
			// make sure the arbitration is up to date
			pCurrentArbitrator = UpdateElementArbitration(index, &player);
		}

		if (pCurrentArbitrator && pCurrentArbitrator != &player)
		{
			// if our element is already empty just allow the update anyway (this can happen if the other machine has accepted our update and then
			// reused the id immediately
			if (this->IsElementEmpty(index))
			{
				LOGGING_ONLY(logSplitter.WriteDataValue("WARNING", "Element is arbitrated by %s but element is empty so allowing", pCurrentArbitrator->GetLogName()));
			}
			else
			{
				LOGGING_ONLY(logSplitter.WriteDataValue("FAIL", "Element is arbitrated by %s", pCurrentArbitrator->GetLogName()));
				return false;
			}
		}

		if (elementEmpty)
		{
			// the element is being emptied by the current arbitrator, so it can be freed.
			this->ClearElementArbitration(index);
		}
		else
		{
			this->SetElementArbitration(index, player);
		}

		return true;
	} 

	void DoPostElementReadProcessing(unsigned index)
	{
		this->m_ElementInfo[index].m_ElementID = m_ElementID;
	}

	virtual void RecalculateElementScope(unsigned startIndex, unsigned endIndex)
	{
		if (this->IsArrayLocallyArbitrated())
		{
			LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());
			unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
			const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

			//netPlayer* localPlayer = netInterface::GetLocalPlayer();

			for (unsigned i=startIndex; i<endIndex; i++)
			{
				if (this->m_ElementArbitrators[i] != INVALID_PLAYER_INDEX)
				{
					// first decide whether this element can be cleared
					if (this->CanHaveEmptyElements() && this->ElementCanBeCleared(i))
					{
						this->SetElementEmpty(i);

						// we have to keep the element locally arbitrated so that the empty state is broadcast
						if (!this->IsElementLocallyArbitrated(i))
						{
							this->ClearElementArbitration(i);
						}
					}

					if (!this->IsElementEmpty(i) && this->IsElementLocallyArbitrated(i))
					{
#if ENABLE_NETWORK_LOGGING
						bool bLoggedHeader = false;
#endif // ENABLE_NETWORK_LOGGING

						for (u32 p=0; p<numRemotePhysicalPlayers; p++)
						{
							const netPlayer* pPlayer = remotePhysicalPlayers[p];

							PlayerFlags playerFlag = (1<<pPlayer->GetPhysicalPlayerIndex());

							bool bWasInScope = (m_ElementInfo[i].m_ElementScope & playerFlag) != 0;
							bool bNowInScope = this->IsElementInScope(i, *pPlayer);

							if (bWasInScope != bNowInScope)
							{
#if ENABLE_NETWORK_LOGGING
								if (!bLoggedHeader)
								{
									char logStr[30];
									formatf(logStr, sizeof(logStr), "ELEMENT_SCOPE_CHANGED_%d", i);
									NetworkLogUtils::WriteLogEvent(log, logStr, "%s", this->GetHandlerName());

									if (this->GetIdentifier())
									{
										log.WriteDataValue("Identifier", this->GetIdentifier()->GetLogName());
									}

									bLoggedHeader = true;
								}
#endif // ENABLE_NETWORK_LOGGING

								if (bNowInScope)
								{
									LOGGING_ONLY(log.WriteDataValue("Player in scope", pPlayer->GetLogName()));
									m_ElementInfo[i].m_ElementScope |= playerFlag;
								}
								else
								{
									LOGGING_ONLY(log.WriteDataValue("Player out of scope", pPlayer->GetLogName()));
									m_ElementInfo[i].m_ElementScope &= ~playerFlag;
								}

								// the scope has changed, so this player now requires an update
								unsigned syncDataIndex = this->GetSyncDataIndexForPlayer(*pPlayer);

								this->GetSyncData()->SetSyncDataUnitDirtyForPlayer(i, syncDataIndex);

								ElementScopeChanged(i);
							}
						}
					}
				}
				else
				{
					m_ElementInfo[i].m_ElementScope = 0;
				}
			}
		}
	}

	bool IsElementInScopeAndSyncedWithPlayer(unsigned index, const netPlayer& player) const
	{
		bool bInScopeAndSynced = false;

		if (IsElementInScopeInternal(index, player))
		{
			bInScopeAndSynced = IsElementSynced(index, this->GetSyncDataIndexForPlayer(player));
		}

		return bInScopeAndSynced;
	}

protected:

	atArray<sSharedArrayElementInfo>	m_ElementInfo;
	u32									m_ElementID;
	u16									m_updateNum;
};

//========================================================================================================================================
// netSharedNetObjArrayHandler
//========================================================================================================================================

//PURPOSE
// A shared array where each element is associated with a network object, and migrates with that object
template<class T, class Derived>
class netSharedNetObjArrayHandler : public netSharedArrayHandlerWithElementScope<T, Derived>
{
public:

	netSharedNetObjArrayHandler(NetworkArrayHandlerType handlerType, 
								T* arrayData,
								unsigned numElements, 
								unsigned maxNumPlayers = MAX_NUM_PHYSICAL_PLAYERS) :
	netSharedArrayHandlerWithElementScope<T, Derived>(handlerType, arrayData, numElements, maxNumPlayers)
	{
	}

	// called when a network object pointed to by one of t
	void NetworkObjectHasMigrated(netObject* pNetObj);

	virtual bool SetElementDirty(unsigned index)
	{
		bool bDirtied = netSharedArrayHandlerWithElementScope<T, Derived>::SetElementDirty(index);

#if __ASSERT
		if (bDirtied && !this->IsElementEmpty(index) && this->m_Array[index].GetNetworkObject())
		{
			u32 currId = this->GetElementIDForIndex(index);
			u32 netId = this->GetElementID(index);

			gnetAssertf(currId==netId, "%s: Setting element %d dirty when element network object has changed (curr: %d, net obj: %d)", this->GetHandlerName(), index, currId, netId);
		}
#endif
		return bDirtied;
	}

protected:

	// elements will migrate with the network object, so there is no need to remove them when the current owner leaves
	virtual bool RemoveElementWhenArbitratorLeaves(unsigned index) const { return !this->m_Array[index].GetNetworkObject(); }

	virtual bool ElementsCanMigrate() const { return true; }

	virtual u32 GetSizeOfElementID() const { return SIZEOF_OBJECTID; }

	virtual u32 GetElementID(unsigned index) const
	{
		netObject* pNetObj = this->m_Array[index].GetNetworkObject();

		Assertf(pNetObj, "No network object for slot %d in shared array %s", index, this->GetHandlerName());

		return pNetObj ? (u32)pNetObj->GetObjectID() : this->INVALID_ELEMENT_ID;
	}

	virtual const netPlayer* GetTargetElementArbitration(unsigned index) const
	{
		netObject* netObj = this->m_Array[index].GetNetworkObject();

		if (netObj && netObj->GetPlayerOwner())
		{
			return netObj->GetPlayerOwner();
		}

		return NULL;
	}

	virtual bool AreElementsAlwaysInScope() const { return false; }

	virtual void SetElementArbitration(unsigned index, const netPlayer& player)
	{
#if ENABLE_NETWORK_LOGGING
		bool bArbitratorChanging = this->m_ElementArbitrators[index] != this->GetSyncDataIndexForPlayer(player);
#endif // ENABLE_NETWORK_LOGGING

		netSharedArrayHandlerWithElementScope<T, Derived>::SetElementArbitration(index, player);

#if ENABLE_NETWORK_LOGGING
		if (bArbitratorChanging)
		{
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
			netObject* pNetObj = this->m_Array[index].GetNetworkObject();

			if (pNetObj)
			{
				log.WriteDataValue("Network object", "%s", pNetObj->GetLogName());
				log.WriteDataValue("Player owner", "%s", pNetObj->GetPlayerOwner() ? pNetObj->GetPlayerOwner()->GetLogName() : "none");
			}
		}
#endif // ENABLE_NETWORK_LOGGING

	}

	virtual bool CanApplyElementData(unsigned index, const netPlayer& player, bool elementEmpty) 
	{
		if (!elementEmpty)
		{
			LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());
			LOGGING_ONLY(netLogSplitter logSplitter(netInterface::GetMessageLog(), log));

			netObject* pNetObj = netInterface::GetNetworkObject((ObjectId)this->m_ElementID);

			if (!pNetObj)
			{
				LOGGING_ONLY(logSplitter.WriteDataValue("FAIL", "Network object does not exist"));
				return false;
			}
		}

		return netSharedArrayHandlerWithElementScope<T, Derived>::CanApplyElementData(index, player, elementEmpty);
	}
	
	virtual bool IsElementInScope(unsigned index, const netPlayer& player) const
	{
		netObject* netObj = this->m_Array[index].GetNetworkObject();

		if (netObj)
		{
			// if the obj is a clone, we are waiting from the update from the new owner before migrating the element to him. In this case
			// just keep the existing scope and make sure he has the element.
			if (netObj->IsClone())
			{
				if (&player == netObj->GetPlayerOwner())
				{
					return true;
				}
				else
				{
					return ((this->m_ElementInfo[index].m_ElementScope & (1<<player.GetPhysicalPlayerIndex())) != 0);
				}
			}
			else
			{
				PlayerFlags clonedState = netObj->GetClonedState();

				if ((clonedState & (1<<player.GetPhysicalPlayerIndex())) != 0)
				{
					return true;
				}
			}
		}

		return false;
	}

	virtual bool ElementCanBeCleared(unsigned index) const
	{
		netObject* netObj = this->m_Array[index].GetNetworkObject();

		if (!netObj)
		{
			LOGGING_ONLY(netInterface::GetArrayManager().GetLog().Log("\t\t## %s : Clearing %s arbitrated element %d. The network object has been removed. ##\r\n", this->GetHandlerName(), this->IsElementRemotelyArbitrated(index) ? "remotely" : "locally", index ));
			return true;
		}

		return netSharedArrayHandlerWithElementScope<T, Derived>::ElementCanBeCleared(index);
	}
};

//========================================================================================================================================
// netHostArrayHandler
//========================================================================================================================================

//PURPOSE
//  Handles an array that is arbitrated by the session host
template<class T, class Derived>
class netHostArrayHandler : public netArrayHandler<T, Derived>
{
public:

	netHostArrayHandler(NetworkArrayHandlerType handlerType, 
						T* arrayData,
						unsigned numElements, 
						unsigned maxNumPlayers = MAX_NUM_PHYSICAL_PLAYERS) :
	netArrayHandler<T, Derived>(handlerType, arrayData, numElements, maxNumPlayers)
	, m_locallyArbitrated(false)
	{
	}

	virtual void Init()
	{
		netArrayHandler<T, Derived>::Init();
		m_locallyArbitrated = netInterface::IsHost();
	}

	virtual void NewHost()
	{
		m_locallyArbitrated = netInterface::IsHost();
		netArrayHandler<T, Derived>::HandleChangeOfArbitration(true);
	}

	virtual bool IsArrayLocallyArbitrated() const { return m_locallyArbitrated; }

	virtual const netPlayer* GetPlayerArbitratorOfArray() const
	{
		const netPlayer* host = netInterface::GetHostPlayer();

		if (host && host->IsLocal() && !m_locallyArbitrated)
		{
			host = NULL;
		}

		return host;
	}

private:

	// This flag is only set once NewHost() is called. netInterface::IsHost can return true before NewHost is called, 
	// resulting in the dirty array elements being recalculated before the shadow buffer is initialised.
	bool m_locallyArbitrated;
};

//========================================================================================================================================
// netPlayerArrayHandler
//========================================================================================================================================


//PURPOSE
//	An identifier that has a pointer to a player arbitrator and and instance id. An instance id set to -1 will not be used.
// The player arbitrator is the player that is dictating the contents of the array and sending updates to the other players.
// The instance id is used to distinguish between multiple arrays of the same type arbitrated by the same player.
class netPlayerArrayIdentifier : public netArrayIdentifierBase
{
protected:

	static const int UNUSED_INSTANCE_ID = -1;

public:

	netPlayerArrayIdentifier() : m_PlayerArbitrator(NULL), m_InstanceId(UNUSED_INSTANCE_ID), m_hadPreviousArbitrator(false) { }

	netPlayerArrayIdentifier(const netPlayer* player, int id = UNUSED_INSTANCE_ID) : 
		m_PlayerArbitrator(NULL), 
		m_InstanceId(UNUSED_INSTANCE_ID),
		m_hadPreviousArbitrator(false)
	{
		Set(player, id);
	}

	virtual ~netPlayerArrayIdentifier() { }

	void Set(const netPlayer* player, int id = UNUSED_INSTANCE_ID)
	{
		m_PlayerArbitrator = player;
		m_InstanceId = id;
		m_hadPreviousArbitrator = (player != NULL);
	}

	virtual void Reset(const netPlayer* player) 
	{
		SetPlayerArbitrator(player);
		m_InstanceId = UNUSED_INSTANCE_ID;
	}

	virtual void Read(datBitBuffer& bitBuffer) 
	{
		// (the player arbitrator is not serialised as it can be determined from the connection)
		if (GetSizeOfInstanceId() > 0)
		{
			Assert(m_InstanceId >= 0);
			CSyncDataReader serialiser(bitBuffer, 0);
			SERIALISE_UNSIGNED(serialiser, m_InstanceId, GetSizeOfInstanceId());
		}
	}

	virtual void Write(datBitBuffer& bitBuffer)
	{
		if (GetSizeOfInstanceId() > 0)
		{
			Assert(m_InstanceId >= 0);
			CSyncDataWriter serialiser(bitBuffer, 0);
			SERIALISE_UNSIGNED(serialiser, m_InstanceId, GetSizeOfInstanceId());
		}
	}

	virtual unsigned	GetSize() { return GetSizeOfInstanceId(); }

	virtual const char*	GetLogName() const
	{
#if __BANK
		if (m_InstanceId != UNUSED_INSTANCE_ID)
		{
			formatf(ms_logName, LOG_NAME_LEN, "Owner %s. Id: %d.", m_PlayerArbitrator ? m_PlayerArbitrator->GetLogName() : "none", m_InstanceId);
		}
		else
		{
			formatf(ms_logName, LOG_NAME_LEN, "Owner %s", m_PlayerArbitrator ? m_PlayerArbitrator->GetLogName() : "none");
		}
		return ms_logName;
#else
		return "";
#endif
	}

	virtual const netPlayer*	GetPlayerArbitrator() const							{ return m_PlayerArbitrator;}
	virtual void				SetPlayerArbitrator(const netPlayer* player)		{ m_PlayerArbitrator = player; if (player != NULL) m_hadPreviousArbitrator = true; }
	virtual const unsigned		GetInstanceId() const								{ return m_InstanceId;}
	
	//PURPOSE
	//  Returns true if the identifier was previously assigned to another player (it may have been assigned to a null player subsequently)
	bool HadPreviousArbitrator() const { return m_hadPreviousArbitrator; }

	//PURPOSE
	// Returns the number of bits required to serialise m_InstanceId. This defaults to 0, which means that m_InstanceId will not be 
	// serialised. Overload this function if you want to serialise m_InstanceId.
	virtual unsigned				GetSizeOfInstanceId() const { return 0; }

	virtual bool operator==(const netArrayIdentifierBase& identifier) const 
	{
		Assert(dynamic_cast<const netPlayerArrayIdentifier*>(&identifier));

		const netPlayerArrayIdentifier& that = static_cast<const netPlayerArrayIdentifier&>(identifier);

		return (that.m_PlayerArbitrator == m_PlayerArbitrator &&
			that.m_InstanceId == m_InstanceId);
	}

protected:

	// The player arbitrating this array
	const netPlayer* m_PlayerArbitrator;

	// An additional id, used to distinguish between multiple arrays of the same type arbitrated by the same player. 
	int m_InstanceId;

	// A flag used to store whether the identifier was previously assigned to another player
	bool m_hadPreviousArbitrator;
};

//PURPOSE
//	Deals with arrays arbitrated by an individual player. The identifier must derive from netPlayerArrayIdentifier.
//
//	T - the type of the array elements
//	Derived - the name of the class derived from this class.
//
template<class T, class Derived>
class netPlayerArrayHandler : public netArrayHandler<T, Derived>
{
public:

	netPlayerArrayHandler(NetworkArrayHandlerType handlerType, 
						  T* arrayData,
						  unsigned numElements, 
						  unsigned maxNumPlayers = MAX_NUM_PHYSICAL_PLAYERS) 
	: netArrayHandler<T, Derived>(handlerType, arrayData, numElements, maxNumPlayers)
	{
	}

	void Init()
	{
		// sanity check that the identifier is a player array identifier
		gnetAssert(dynamic_cast<netPlayerArrayIdentifier*>(static_cast<Derived*>(this)->GetIdentifier()));

		netArrayHandler<T, Derived>::Init();
	}

	virtual void  SetPlayerArbitrator(const netPlayer* player)
	{ 
		netPlayerArrayIdentifier* playerIdentifier = static_cast<netPlayerArrayIdentifier*>(static_cast<Derived*>(this)->GetIdentifier());

		if (gnetVerify(playerIdentifier))
		{
			if (player != playerIdentifier->GetPlayerArbitrator())
			{
#if ENABLE_NETWORK_LOGGING
				netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
				NetworkLogUtils::WriteLogEvent(log, "SET_PLAYER_ARBITRATOR", "%s", this->GetHandlerName());
				log.WriteDataValue("Identifier", playerIdentifier->GetLogName());
				log.WriteDataValue("New arbitrator", player ? player->GetLogName() : "-none-");
#endif
				bool bHadPreviousArbitrator = playerIdentifier->HadPreviousArbitrator();

				playerIdentifier->SetPlayerArbitrator(player); 

				netArrayHandler<T, Derived>::HandleChangeOfArbitration(bHadPreviousArbitrator);
			}
		}
	}

	const netPlayer* GetPlayerArbitrator() const
	{ 
		const netPlayerArrayIdentifier* playerIdentifier = static_cast<const netPlayerArrayIdentifier*>(static_cast<const Derived*>(this)->GetIdentifier());

		if (gnetVerify(playerIdentifier))
		{
			return playerIdentifier->GetPlayerArbitrator(); 
		}

		return NULL;
	}

	virtual void PlayerHasLeft(const netPlayer& player)
	{
        netArrayHandler<T, Derived>::PlayerHasLeft(player);

		netPlayerArrayIdentifier* playerIdentifier = static_cast<netPlayerArrayIdentifier*>(static_cast<Derived*>(this)->GetIdentifier());

		if (gnetVerify(playerIdentifier) &&
			&player == playerIdentifier->GetPlayerArbitrator())
		{
			SetPlayerArbitrator(NULL); 
		}
	}
	
	virtual bool IsArrayLocallyArbitrated() const
	{
		const netPlayerArrayIdentifier* playerIdentifier = static_cast<const netPlayerArrayIdentifier*>(static_cast<const Derived*>(this)->GetIdentifier());

		if (gnetVerify(playerIdentifier) && playerIdentifier->GetPlayerArbitrator())
		{
			return (playerIdentifier->GetPlayerArbitrator()->IsLocal());
		}

		return false;
	}

	virtual const netPlayer* GetPlayerArbitratorOfArray() const
	{
		const netPlayerArrayIdentifier* playerIdentifier = static_cast<const netPlayerArrayIdentifier*>(static_cast<const Derived*>(this)->GetIdentifier());

		if (gnetVerify(playerIdentifier))
		{
			return playerIdentifier->GetPlayerArbitrator();
		}

		return NULL;
	}

	bool CanProcessUpdate(const netPlayer& player, netSequence updateSeq, bool bSplitUpdate) const
	{
		if (GetPlayerArbitrator() != &player)
		{
			return false;
		}

		return (netArrayHandler<T, Derived>::CanProcessUpdate(player, updateSeq, bSplitUpdate));
	}
};

//========================================================================================================================================
// netScriptBroadcastDataArrayHandler
//========================================================================================================================================

//PURPOSE
//	Base non-templated broadcast array handler identifier
class netBroadcastDataArrayIdentifierBase : public netPlayerArrayIdentifier
{
protected:

	static const unsigned SIZEOF_DATAID = 5;

public:

	netBroadcastDataArrayIdentifierBase() : netPlayerArrayIdentifier(NULL) {}

	netBroadcastDataArrayIdentifierBase(const netPlayer* player, unsigned clientSlot, unsigned dataId BANK_ONLY(, const char* debugArrayName ))
	: netPlayerArrayIdentifier(player, static_cast<int>(clientSlot))
	, m_DataId(dataId)
	{
#if __BANK
		SetDataDebugArrayNameSafe( debugArrayName );
#endif
	}

	netBroadcastDataArrayIdentifierBase(const netPlayer* player, unsigned dataId BANK_ONLY(, const char* debugArrayName ))  
	: netPlayerArrayIdentifier(player)
	, m_DataId(dataId)
	{
#if __BANK
		SetDataDebugArrayNameSafe( debugArrayName );
#endif
	}

	virtual scriptIdBase& GetScriptId()	= 0;
	virtual const scriptIdBase& GetScriptId() const = 0;
	
	void Set(const netPlayer* player, unsigned clientSlot, unsigned dataId BANK_ONLY(, const char* debugArrayName))
	{
		netPlayerArrayIdentifier::Set(player, clientSlot);	
		m_DataId = dataId;
#if __BANK
		SetDataDebugArrayNameSafe( debugArrayName );
#endif
	}

	void Set(const netPlayer* player, unsigned dataId BANK_ONLY(, const char* debugArrayName))
	{
		netPlayerArrayIdentifier::Set(player);	
		m_DataId = dataId;
#if __BANK
		SetDataDebugArrayNameSafe( debugArrayName );
#endif
	}

	int GetDataId() const { return m_DataId; }

#if __BANK
	const char* GetDataDebugArrayName() const { return m_DataDebugArrayName; }
#endif

	virtual void Read(datBitBuffer& bitBuffer)
	{
		netPlayerArrayIdentifier::Read(bitBuffer);
		CSyncDataReader serialiser(bitBuffer, 0);
		SERIALISE_UNSIGNED(serialiser, m_DataId, SIZEOF_DATAID);
	}

	virtual void Write(datBitBuffer& bitBuffer) 
	{
		netPlayerArrayIdentifier::Write(bitBuffer);
		CSyncDataWriter serialiser(bitBuffer, 0);
		SERIALISE_UNSIGNED(serialiser, m_DataId, SIZEOF_DATAID);
 	}

	virtual unsigned GetSize() 
	{ 
		return netPlayerArrayIdentifier::GetSize() + SIZEOF_DATAID; 
	}

	virtual unsigned	GetSizeOfInstanceId() const { return 0; } // m_InstanceId does not need to be serialised (the player is used to find the array)

protected:

	// An id used to distinguish multiple broadcast datas allocated to a single script
	unsigned m_DataId;

#if __BANK
	void SetDataDebugArrayNameSafe( const char* debugArrayName )
	{
		m_DataDebugArrayName.Reset();
		if ( debugArrayName )
		{
			// Assignment isn't safe if the string is longer than the fixed array so we check here
			m_DataDebugArrayName.Append(debugArrayName, Min( strlen(debugArrayName), (size_t)m_DataDebugArrayName.GetInternalBufferSize()-1 ));
		}
	}

	atFixedString<32> m_DataDebugArrayName;
#endif
};

//PURPOSE
//	A broadcast array handler identifier
//
//  ScriptIdType - the script id class, derived from scriptId
//
template<class ScriptIdType>
class netBroadcastDataArrayIdentifier : public netBroadcastDataArrayIdentifierBase
{
public:

	netBroadcastDataArrayIdentifier() {}

	netBroadcastDataArrayIdentifier(const scriptIdBase& scriptId, const netPlayer* player, unsigned clientSlot, unsigned dataId BANK_ONLY(, const char* debugArrayName))  
	: netBroadcastDataArrayIdentifierBase(player, static_cast<int>(clientSlot), dataId BANK_ONLY(, debugArrayName))
	, m_ScriptId(scriptId) 
	{
	}

	netBroadcastDataArrayIdentifier(const scriptIdBase& scriptId, const netPlayer* player, unsigned dataId BANK_ONLY(, const char* debugArrayName))  
		: netBroadcastDataArrayIdentifierBase(player, dataId BANK_ONLY(, debugArrayName))
		, m_ScriptId(scriptId) 
	{
	}

	scriptIdBase& GetScriptId()				{ return m_ScriptId;}
	const scriptIdBase& GetScriptId() const { return m_ScriptId;}

	void Set(const scriptIdBase& scriptId, const netPlayer* player, unsigned clientSlot, unsigned dataId BANK_ONLY(, const char* debugArrayName))
	{
		netBroadcastDataArrayIdentifierBase::Set(player, clientSlot, dataId BANK_ONLY(, debugArrayName));	
		m_ScriptId = scriptId;
	}

	void Set(const scriptIdBase& scriptId, const netPlayer* player, unsigned dataId BANK_ONLY(, const char* debugArrayName))
	{
		netBroadcastDataArrayIdentifierBase::Set(player, dataId BANK_ONLY(, debugArrayName));	
		m_ScriptId = scriptId;
	}

	virtual void Read(datBitBuffer& bitBuffer)
	{
		netBroadcastDataArrayIdentifierBase::Read(bitBuffer);
		m_ScriptId.Read(bitBuffer);
	}

	virtual void Write(datBitBuffer& bitBuffer) 
	{
		netBroadcastDataArrayIdentifierBase::Write(bitBuffer);
		m_ScriptId.Write(bitBuffer);
	}

	virtual unsigned GetSize() 
	{ 
		return netBroadcastDataArrayIdentifierBase::GetSize() + m_ScriptId.GetSizeOfMsgData(); 
	}

	virtual const char* GetLogName() const
	{
#if ENABLE_NETWORK_LOGGING
		char tmp[LOG_NAME_LEN];
		safecpy(tmp, netPlayerArrayIdentifier::GetLogName());
#if __BANK
		if (!m_DataDebugArrayName.empty())
		{
			formatf(ms_logName, LOG_NAME_LEN, "%s (array %s). %s", m_ScriptId.GetLogName(), m_DataDebugArrayName.c_str(), tmp);
		}
		else
#endif
		{
			formatf(ms_logName, LOG_NAME_LEN, "%s (array %d). %s", m_ScriptId.GetLogName(), m_DataId, tmp);
		}

		return ms_logName;
#else
		return "";
#endif // ENABLE_NETWORK_LOGGING
	}

	virtual bool operator==(const netArrayIdentifierBase& identifier) const
	{
		Assert(dynamic_cast<const netBroadcastDataArrayIdentifier*>(&identifier));

		const netBroadcastDataArrayIdentifier& that = static_cast<const netBroadcastDataArrayIdentifier&>(identifier);

		// Broadcast data array handlers may not necessarily have a player arbitrator (they are initially created without
		// a player arbitrator and assigned an arbitrator once players start participating in the script)
		//
		// The instance id is used by the client broadcast array handler, and is used to hold the player slot number.
		if (that.m_ScriptId == m_ScriptId && 
			that.m_DataId == m_DataId)
		{
			if (m_PlayerArbitrator && that.m_PlayerArbitrator)
			{
				if (that.m_PlayerArbitrator == m_PlayerArbitrator)
				{
					Assert(that.m_InstanceId == UNUSED_INSTANCE_ID || that.m_InstanceId == m_InstanceId);
					return true;
				}
			}
			else if (that.m_InstanceId == m_InstanceId)
			{
				return true;
			}
		}

		return false;
	}

protected:

	// The id of the script these variables belong to
	ScriptIdType m_ScriptId;
};

//PURPOSE
// A struct used to serialise an element in the broadcast data array handlers.
class CBroadcastDataElement
{
public:

	static const unsigned NUM_BROADCAST_DATA_ELEMENT_UNITS = 4;
	static const unsigned SIZEOF_BROADCAST_DATA_ELEMENT_UNIT = sizeof(unsigned);
	static const unsigned SIZEOF_BROADCAST_DATA_ELEMENT = NUM_BROADCAST_DATA_ELEMENT_UNITS*SIZEOF_BROADCAST_DATA_ELEMENT_UNIT;

	CBroadcastDataElement()
	{
		sysMemSet(m_data, 0, sizeof(m_data));
	}

	unsigned GetSizeOfDataInBits() const { return (GetNumUsedUnits()*sizeof(unsigned))<<3; }

	int  GetNumUsedUnits() const { return (NUM_BROADCAST_DATA_ELEMENT_UNITS - ms_numUnusedUnits); }
	void SetNumUnusedUnits(unsigned units) { Assert(units < NUM_BROADCAST_DATA_ELEMENT_UNITS); ms_numUnusedUnits = units; }
	void SetAllUnused() { ms_numUnusedUnits = NUM_BROADCAST_DATA_ELEMENT_UNITS; }

	CBroadcastDataElement& operator=(const CBroadcastDataElement& element)
	{
		memcpy(m_data, element.m_data, sizeof(unsigned)*GetNumUsedUnits());
		return *this;
	}

	void Serialise(CSyncDataBase& serialiser)
	{
		if (GetSizeOfDataInBits() > 0)
		{
			SERIALISE_DATABLOCK(serialiser, (u8*)m_data, GetSizeOfDataInBits());
		}

		if (serialiser.GetLog())
		{
			char name[10];
			for (int i=0; i<GetNumUsedUnits(); i++)
			{
				formatf(name, 10, "Unit %d", i);
				serialiser.GetLog()->WriteDataValue(name, "%d", m_data[i]);
			}
		}
	}

public:

	unsigned m_data[NUM_BROADCAST_DATA_ELEMENT_UNITS];
	static unsigned ms_numUnusedUnits;	// some elements do not use all the data (the last element in the array)
};

//PURPOSE
//	Deals with script broadcast data arrays (broadcast data is a contiguous block of script variables synched across the network).
class netScriptBroadcastDataHandlerBase : public netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>
{
public:
	
	static const unsigned ELEMENT_SIZE_IN_BYTES				= sizeof(CBroadcastDataElement);
	static const unsigned ELEMENT_SIZE_IN_BITS				= ELEMENT_SIZE_IN_BYTES<<3;
	static const unsigned INVALID_PARTICIPANT_SLOT			= 255;
	static const unsigned MAX_HIGH_FREQUENCY_SCRIPT_WORDS	= 20; // this limits the size of arrays updating with a high frequency

public:

	netScriptBroadcastDataHandlerBase(NetworkArrayHandlerType handlerType,
										unsigned* arrayData, 
										unsigned sizeOfArrayInBytes, 
										const scriptHandler& scriptHandler) 
	: netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>(handlerType, reinterpret_cast<CBroadcastDataElement*>(arrayData), sizeOfArrayInBytes/ELEMENT_SIZE_IN_BYTES + (sizeOfArrayInBytes%ELEMENT_SIZE_IN_BYTES != 0), scriptHandler.GetNetworkComponent()->GetMaxNumParticipants())
	, m_SizeOfArrayInBytes(sizeOfArrayInBytes)
	, m_Initialised(false)
	, m_bHasHighFrequencyUpdates(false)
#if __DEV
	, m_bUpdatedThisFrame(false)
	, m_bDirtiedThisFrame(false)
	, m_dataChecksum(0)
	, m_pEndOfArray(reinterpret_cast<u8*>(arrayData)+sizeOfArrayInBytes)
#endif
	{
#if __DEV
		if (AssertVerify(m_Array))
		{
			CacheArrayContents(CalculateChecksum());
		}
#endif

		for (u32 i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			m_participantSlots[i] = INVALID_PARTICIPANT_SLOT;
		}

		SetScriptHandler(&scriptHandler);
	}

	netScriptBroadcastDataHandlerBase(NetworkArrayHandlerType handlerType,
										unsigned sizeOfArrayInBytes, 
										unsigned maxNumPlayers)
	: netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>(handlerType, NULL, sizeOfArrayInBytes/ELEMENT_SIZE_IN_BYTES + (sizeOfArrayInBytes%ELEMENT_SIZE_IN_BYTES != 0), maxNumPlayers)
	, m_SizeOfArrayInBytes(sizeOfArrayInBytes)
	, m_Initialised(false)
	, m_pScriptHandler(NULL)
	, m_bHasReceivedAnUpdate(false)
	, m_bHasHighFrequencyUpdates(false)
#if __BANK
	, m_totalBandwidthOut(0)
	, m_peakBandwidthOut(0)
#endif
#if __DEV
	, m_dataChecksum(0)
	, m_pEndOfArray(NULL)
	, m_bUpdatedThisFrame(false)
	, m_bDirtiedThisFrame(false)
	, m_numFramesRepeatedlyDirtied(0)
#endif
	{
#if __DEV
		if (m_Array)
		{
			CacheArrayContents(CalculateChecksum());
		}
#endif

		for (u32 i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			m_participantSlots[i] = INVALID_PARTICIPANT_SLOT;
		}
	}

	void Serialise(CSyncDataBase& serialiser)
	{
		m_Element.Serialise(serialiser);
	}

	//PURPOSE
	// Returns a ptr to the array, used when swapping between local and remote broadcast data handlers
	unsigned* GetArray() { return reinterpret_cast<unsigned*>(m_Array); }

	virtual void SetArrayData(CBroadcastDataElement* arrayData, unsigned sizeOfArrayInBytes); 

	void SetScriptHandler(const scriptHandler* scriptHandler);
	const scriptHandler* GetScriptHandler() const { return m_pScriptHandler; }

	//PURPOSE
	// Returns true if we have ever received an update for this handler
	bool HasReceivedAnUpdate() const	{ return m_bHasReceivedAnUpdate; }

	virtual bool CacheSplitUpdates() const  { return true; }

	virtual void Init()
	{
		// This can be called a number of times if the broadcast data array handler persists between uses
		if (!m_Initialised)
		{
			m_Element.SetNumUnusedUnits(0);
			netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::Init();
			m_Initialised = true;
		}

		m_bHasReceivedAnUpdate = false;

#if __BANK
		m_totalBandwidthOut = 0;
		m_peakBandwidthOut = 0;
#endif
#if __DEV
		m_numFramesRepeatedlyDirtied = 0;
		m_bUpdatedThisFrame = false;
		m_bDirtiedThisFrame = false;
		m_bClientAlterationPermitted = false;
#endif
	}

	virtual void Shutdown()
	{
		if (m_Initialised)
		{
			netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::Shutdown();
			m_Initialised = false;
		}
	}

#if __DEV
	virtual void Update();
	virtual void HandleChangeOfArbitration(bool bHadPreviousArbitrator);
#endif // __DEV

	virtual void PlayerHasLeft(const netPlayer& player);
	virtual void PlayerHasJoined(const netPlayer& player);
	virtual bool CanSendUpdate(const netPlayer& player) const;

	virtual bool SetElementDirty(unsigned index)
	{
		bool bDirtied = false;

		SetUnusedUnits(index);
		if (m_Element.GetNumUsedUnits() > 0)
		{
			bDirtied = netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::SetElementDirty(index);
		}

		DEV_ONLY(m_bDirtiedThisFrame = true);

		return bDirtied;
	}

	virtual void WriteElement(datBitBuffer& bitBuffer, unsigned index, netLoggingInterface* pLog = NULL)
	{
		SetUnusedUnits(index);
		netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::WriteElement(bitBuffer, index, pLog);
	}

	virtual void ReadElement(datBitBuffer& bitBuffer, unsigned index, netLoggingInterface* pLog = NULL)
	{
		SetUnusedUnits(index);
		netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::ReadElement(bitBuffer, index, pLog);
	}

	virtual void LogElement(unsigned index, netLoggingInterface& log)
	{
		SetUnusedUnits(index);
		netPlayerArrayHandler<CBroadcastDataElement, netScriptBroadcastDataHandlerBase>::LogElement(index, log);
	}

	virtual unsigned GetCurrentElementSizeInBits(unsigned index)
	{
		SetUnusedUnits(index);
		return m_Element.GetSizeOfDataInBits();
	}

	virtual unsigned GetMaxElementSizeInBits()
	{
		return ELEMENT_SIZE_IN_BITS; 
	}

	// returns true if the given player requires updates from this handler
	bool IsPlayerInScope(const netPlayer& player) const
	{
		return m_participantSlots[player.GetPhysicalPlayerIndex()] != INVALID_PARTICIPANT_SLOT;
	}

	//PURPOSE
	//  Return the size of the array data in bytes
	virtual unsigned GetSizeOfArrayDataInBytes() const { return m_SizeOfArrayInBytes; }

	//PURPOSE
	//	Returns a bitmask of all the remote players in scope of this array.
	virtual PlayerFlags GetRemotePlayersInScopeBitmask() const;

	//PURPOSE
	//	Returns the buffer used to shadow broadcast data from remote player for debug.
	virtual u8* GetBDBackupBuffer()		{ return NULL; }

	//PURPOSE
	//	If a script requires any local changes of remote BD to be tracked create the required buffer.
	virtual void CreateBDBackupBuffer()		{ }

	//PURPOSE
	//	If a script previously created a buffer to track local changes to remote BD remove here.
	virtual void RemoveBDBackupBuffer()		{ }

	virtual bool RequiresHighFrequencyUpdates() { return m_bHasHighFrequencyUpdates; }

	void SetHasHighFrequencyUpdates(bool bSet);

#if __DEV
	void AllowClientAlteration() {	m_bClientAlterationPermitted = true; }
#endif

#if __BANK
	virtual void WriteUpdate(const netPlayer& player, datBitBuffer& bitBuffer, netSequence updateSeq, unsigned& currentElement, bool logSizes = false);
	virtual bool ReadUpdate(const netPlayer& player, datBitBuffer& bitBuffer, unsigned dataSize, netSequence updateSeq);

	virtual void AddBandwidthOut(unsigned dataWritten);
	virtual void AddBandwidthIn(unsigned dataRead);

	u32 GetTotalBandwidthOut() const { return m_totalBandwidthOut; } 
	u32 GetAverageBandwidthOut(u32 timeInMs) const { return timeInMs > 0 ? (u32)((float)m_totalBandwidthOut / ((float)timeInMs/1000.0f)) : 0; } 
	u32 GetPeakBandwidthOut() const { return m_peakBandwidthOut; }
#endif //__BANK

protected:

	// returns the index used to access any data associated with a network player, held within the handler  
	virtual unsigned GetSyncDataIndexForPlayer(const netPlayer& player) const
	{
		Assert(m_participantSlots[player.GetPhysicalPlayerIndex()] != INVALID_PARTICIPANT_SLOT);
		return m_participantSlots[player.GetPhysicalPlayerIndex()];
	}

	//PURPOSE
	// Sets the unused units for the lest element in the array. Used when the array size is less than a multiple of the size of CBroadcastDataElement
	void SetUnusedUnits(unsigned index);

	//PURPOSE
	// Permit the client to alter remotely controlled broadcast data
	virtual void ApplyElementData(unsigned index, const netPlayer& player);

#if __DEV
	virtual void DoPostReadProcessing();

	void CacheArrayContents(unsigned checksum);
#endif // __DEV

	// a ptr to the script handler which registered this array, for quick access
	const scriptHandler* m_pScriptHandler;

	// the participant slots for each player in the script that owns this broadcast data. This is to avoid looking them up continously.
	u8 m_participantSlots[MAX_NUM_PHYSICAL_PLAYERS];

	// the size of the array in bytes (this might be less than a multiple of the size of CBroadcastDataElement)
	u32	m_SizeOfArrayInBytes; 

	bool m_Initialised;

private:

#if __BANK
	u32 m_totalBandwidthOut;			// the total bandwidth recorded going out
	u32 m_peakBandwidthOut;				// the peak bandwidth recorded going out
#endif

#if __DEV
	// a checksum of the current contents of the array. Used to catch when a script client alters the array data locally.
	u32 m_dataChecksum;

	// a counter keeping track of how often the array data is dirtied. This is to catch the scripts doing bad things like putting a timer in the broadcast data,
	// which will result in it getting dirtied every update.
	u16 m_numFramesRepeatedlyDirtied;

	// a ptr to the first byte after the end of the array, used to sanity check that the array handler does not intefere with the memory outwith the array
	u8* m_pEndOfArray;

	bool m_bUpdatedThisFrame : 1;
	bool m_bDirtiedThisFrame : 1;

	bool m_bClientAlterationPermitted : 1; // set when we permit the client to alter remotely controlled host broadcast data
#endif // __DEV

	bool m_bHasReceivedAnUpdate : 1;
	bool m_bHasHighFrequencyUpdates : 1;
};


// ================================================================================================================
// netHostBroadcastDataHandlerBase
// ================================================================================================================

//PURPOSE
//   Deals with script broadcast data sent from the host of the script to all script clients.
//	 Your host broadcast data handler must derive from this and declare the identifiers.
class netHostBroadcastDataHandlerBase : public netScriptBroadcastDataHandlerBase
{
public:

	netHostBroadcastDataHandlerBase(unsigned* pArray, 
									unsigned sizeOfArrayInBytes, 
									const scriptHandler& scriptHandler) 
	: netScriptBroadcastDataHandlerBase(HOST_BROADCAST_DATA_ARRAY_HANDLER, pArray, sizeOfArrayInBytes, scriptHandler)
	, m_SyncData(scriptHandler.GetNetworkComponent()->GetMaxNumParticipants(), m_NumArrayElements, m_NumArrayElements*ELEMENT_SIZE_IN_BYTES)
	, m_nextFreeHostObjectId(0)
	, m_nextFreeHostObjectIdChanged(false)
	{
	}

	netHostBroadcastDataHandlerBase(unsigned sizeOfArrayInBytes, unsigned maxNumPlayers)
	: netScriptBroadcastDataHandlerBase(HOST_BROADCAST_DATA_ARRAY_HANDLER, sizeOfArrayInBytes, maxNumPlayers)
	, m_SyncData(maxNumPlayers, m_NumArrayElements, m_NumArrayElements*ELEMENT_SIZE_IN_BYTES)
	, m_nextFreeHostObjectId(0)
	, m_nextFreeHostObjectIdChanged(false)
	{
	}

	const char* GetHandlerName() const { return "SCRIPT_HOST_BROADCAST_DATA"; }

	ScriptObjectId	GetNextFreeHostObjectId() const { return m_nextFreeHostObjectId; };
	void			SetNextFreeHostObjectId(u32 n);

	virtual void  SetPlayerArbitrator(const netPlayer* player);
	virtual void  PlayerHasJoined(const netPlayer& player);

	virtual bool CanProcessUpdate(const netPlayer& player, netSequence updateSeq, bool bSplitUpdate) const;

	unsigned GetTotalSizeOfUpdateData(const netPlayer& player, unsigned messageSize, unsigned &numUpdates, u16 updateStartElements[]);
	unsigned GetSizeOfMessageUpdateData(const netPlayer& player, unsigned messageSize, unsigned startElement, bool logSizes = false);

	virtual void Init();
	virtual void RecalculateDirtyElements();
	virtual void WriteUpdate(const netPlayer& player, datBitBuffer& bitBuffer, netSequence updateSeq, unsigned& currentElement, bool logSizes = false);
	virtual bool ReadUpdate(const netPlayer& player, datBitBuffer& bitBuffer, unsigned dataSize, netSequence updateSeq);

protected:

	netSyncDataBase* GetSyncData() { return &m_SyncData; }
	const netSyncDataBase* GetSyncData() const { return &m_SyncData; }

private:

	static unsigned const SIZE_OF_OBJECT_NUM = SIZEOF_HANDLER_OBJECT_ID;

	netSyncData_Dynamic m_SyncData;

	ScriptObjectId m_nextFreeHostObjectId;	// this is sent with every update and used to prevent duplicate script objects when the host migrates

	bool m_nextFreeHostObjectIdChanged;
};

// ================================================================================================================
// netPlayerBroadcastDataHandlerBase
// ================================================================================================================

//PURPOSE
//   Player script broadcast data (script data sent from each player to all other players).
//	 Your player broadcast data handlers must derive from this and declare the identifiers.
class netPlayerBroadcastDataHandlerBase : public netScriptBroadcastDataHandlerBase
{
public:

	netPlayerBroadcastDataHandlerBase(unsigned* pArray, 
									  unsigned sizeOfArrayInBytes, 
									  const scriptHandler& scriptHandler) 
	: netScriptBroadcastDataHandlerBase(PLAYER_BROADCAST_DATA_ARRAY_HANDLER, pArray, sizeOfArrayInBytes, scriptHandler)
	{
	}

	netPlayerBroadcastDataHandlerBase(unsigned sizeOfArrayInBytes, unsigned maxNumPlayers)
	: netScriptBroadcastDataHandlerBase(PLAYER_BROADCAST_DATA_ARRAY_HANDLER, sizeOfArrayInBytes, maxNumPlayers)
	{
	}

	const char* GetHandlerName() const { return "SCRIPT_REMOTE_PLAYER_BROADCAST_DATA"; }

	//PURPOSE
	// Returns true if this is a local player broadcast data handler
	virtual bool IsLocal() { return false;}

	// never send checksums for player broadcast data
	virtual void SendChecksumToNewArbitrator() {}
};

//PURPOSE
//  Array handler for locally arbitrated player script broadcast data, containing sync data.
class netPlayerBroadcastDataHandlerBase_Local : public netPlayerBroadcastDataHandlerBase
{
public:

	netPlayerBroadcastDataHandlerBase_Local(unsigned* pArray, 
											unsigned sizeOfArrayInBytes, 
											const scriptHandler& scriptHandler) 
	: netPlayerBroadcastDataHandlerBase(pArray, sizeOfArrayInBytes, scriptHandler)
	, m_SyncData(scriptHandler.GetNetworkComponent()->GetMaxNumParticipants(), m_NumArrayElements, m_NumArrayElements*ELEMENT_SIZE_IN_BYTES)
	{
	}

	netPlayerBroadcastDataHandlerBase_Local(unsigned sizeOfArrayInBytes, unsigned maxNumPlayers)
	: netPlayerBroadcastDataHandlerBase(sizeOfArrayInBytes, maxNumPlayers)
	, m_SyncData(maxNumPlayers, m_NumArrayElements, m_NumArrayElements*ELEMENT_SIZE_IN_BYTES)
	{
	}

	const char* GetHandlerName() const { return "SCRIPT_LOCAL_PLAYER_BROADCAST_DATA"; }

	virtual bool IsLocal() { return true;}

protected:

	netSyncDataBase* GetSyncData() { return &m_SyncData; }
	const netSyncDataBase* GetSyncData() const { return &m_SyncData; }

private:

	netSyncData_Dynamic m_SyncData;
};

} // namespace rage


#endif  // NET_ARRAY_HANDLER_TYPES_H
