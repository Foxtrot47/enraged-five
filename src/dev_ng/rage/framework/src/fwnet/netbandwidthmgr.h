//
// netbandwidthmanager.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#ifndef NETBANDWIDTHMGR_H
#define NETBANDWIDTHMGR_H

#include "atl/array.h"
#include "fwnet/netbandwidthstats.h"
#include "fwnet/netinterface.h"
#include "fwnet/netlog.h"
#include "fwnet/nettypes.h"

namespace rage
{
class netConnectionManager;
class netPlayer;


class netBandwidthMgr
{
public:

    enum
    {
        MINIMUM_BANDWIDTH_SMALL_SESSION = 128,   // Kbps
        MINIMUM_BANDWIDTH_LARGE_SESSION = 256,  // Kbps
        MAXIMUM_BANDWIDTH               = 1024, // Kbps
    };

    enum
    {
        BANDWIDTH_THROTTLE_CHECK_RELIABLES    = BIT(0),
        BANDWIDTH_THROTTLE_CHECK_UNRELIABLES  = BIT(1)
    };

    static const unsigned NUM_SYNC_FAILURES_TO_CACHE  = 10;
    static const unsigned LOW_SEND_INTERVAL_THRESHOLD = 128;
    static const unsigned DEFAULT_SEND_INTERVAL_LOW   = 100;
    static const unsigned DEFAULT_SEND_INTERVAL_HIGH  = 50;

public:

    netBandwidthMgr();
    virtual ~netBandwidthMgr();

    virtual void Init(netConnectionManager *connectionMgr);
    virtual void Shutdown();
    virtual void Update();

    //PURPOSE
    // Gets the current send interval for network packets
    unsigned GetSendInterval() const { return m_SendInterval; }

    //PURPOSE
    // Sets the current send interval for network packets
    void    SetSendInterval(unsigned sendInterval);

    //PURPOSE
    // Increases the count of unreliable messages that have been resent
    void    IncreaseUnreliableResendCount(PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Returns the count of unreliable messages that have been resent
    unsigned GetUnreliableResendCount(const netPlayer& player) const;

    //PURPOSE
    //  Called when a new player has joins the session.
    virtual void PlayerHasJoined(const netPlayer& player);

    //PURPOSE
    //  Called when a player leaves the session.
    virtual void PlayerHasLeft(const netPlayer& player);

    //PURPOSE
    // Sets the current target upstream bandwidth for this machine - this will
    // be used to set the data to be sent to other players in each update interval
    //PARAMS
    // bandwidth - Target bandwidth in kilo-bits per second.
    void SetTargetUpstreamBandwidth(const unsigned bandwidth);

    //PURPOSE
    // Returns the current target upstream bandwidth
    unsigned GetTargetUpstreamBandwidth() const;

    //PURPOSE
    // Registers outgoing data for the specified player
    void AddBandwidthOut(const netPlayer& player, unsigned bandwidthOut);

    //PURPOSE
    // Manually samples the data for all the statistics
    void SampleData(unsigned currentTime);

    //PURPOSE
    // Returns a boolean value indicating whether the specified amount of data
    // can be sent to the specified player without exceeding the target bandwidth
    bool CanSendData(const netPlayer& player, unsigned bandwidthOut) const;

    //PURPOSE
    // Returns the number of times that data could not be sent, per bandwidth mgr update, due to target bandwidth being exceeded
    unsigned GetNumFailuresPerUpdate() const { return m_NumFailuresPerUpdate; }

    //PURPOSE
    // Returns the minimum bandwidth based on the number of players in the session
    unsigned GetMinimumBandwidth() const;

    //PURPOSE
    // Resets the average number of failures per update
    void ResetAverageFailuresPerUpdate();

    //PURPOSE
    // Returns the running average of the failures
    unsigned GetAverageFailuresPerUpdate() const { return m_AverageFailuresPerUpdate; }

    //PURPOSE
    // Returns the time in milliseconds since the average failure count was last non zero
    unsigned GetTimeSinceAverageFailuresNonZero() const;

    //PURPOSE
    // Returns whether we should throttle the current bandwidth on this connection
    bool        GetBandwidthThrottlingRequired(const netPlayer& player, unsigned throttleCheckFlags = (BANDWIDTH_THROTTLE_CHECK_RELIABLES | BANDWIDTH_THROTTLE_CHECK_UNRELIABLES)) const;

    //PURPOSE
    // Returns whether we can increase the current bandwidth for this connection
    bool        GetBandwidthCanBeAdvanced(const netPlayer& player) const;

    //PURPOSE
    // Returns whether we have any reliable messages that have been unacked for too long
    bool        GetReliableMessagesHaveBeenUnackedTooLong(int cxnId) const;

    //PURPOSE
    // Returns whether we have any reliable messages that have been unacked for too long
    bool        GetUnreliableMessagesHaveBeenUnackedTooLong(const netPlayer& player) const;

    // log file access
    netLoggingInterface &GetLog() { return *m_Log; }

#if __BANK
    //PURPOSE
    // Registers bandwidth statistics with the manager
    void        RegisterStatistics(netBandwidthStatistics& stats, const char* name);

    //PURPOSE
    // Returns the current number of bandwidth statistics
    unsigned GetNumStatistics() const { return m_NumBandwidthStats; }

    //PURPOSE
    // Returns the bandwidth statistics held at the given array index
    const netBandwidthStatistics* GetStatistics(unsigned i) const { Assert(i<m_NumBandwidthStats); return m_BandwidthStats[i]; }

    const netBandwidthRecorder& GetSocketRecorder() const                   { return m_socketRecorder; }
    const netBandwidthRecorder& GetGameChannelRecorder() const              { return m_gameChannelRecorder; }
    const netBandwidthRecorder& GetVoiceSessionChannelRecorder() const      { return m_sessionVoiceChannelRecorder; }
    const netBandwidthRecorder& GetActivitySessionChannelRecorder() const   { return m_sessionActivityChannelRecorder; }
    const netBandwidthRecorder& GetGameSessionChannelRecorder() const       { return m_sessionGameChannelRecorder; }
    const netBandwidthRecorder& GetVoiceChannelRecorder() const             { return m_voiceChannelRecorder; }

#endif // __BANK

protected:

    //PURPOSE
    // Sets whether bandwidth throttling is enabled
    void SetAllowBandwidthThrottling(bool allowThrottling) { m_AllowBandwidthThrottling = allowThrottling; }

    //PURPOSE
    // Handler for derived classes to implement any game specific logic when the
    // target upstream bandwidth changes
    virtual void OnTargetUpstreamBandwidthChanged(const unsigned UNUSED_PARAM(bandwidth)) {}

    //PURPOSE
    // Contains information used to determine whether to throttle bandwidth based
    // on a network player's connection. We don't want to throttle connections in
    // the process of timing out or machines stalling for periods lower than the timeout interval
    struct PlayerThrottleData
    {
        static const unsigned PREVENT_THROTTLE_INTERVAL = 3000;
        static const unsigned PREVENT_THROTTLE_TIME     = 10000;

        PlayerThrottleData() :
        m_LastMessageReceivedTime(0)
            , m_PreventThrottleTimer(0)
        {
        }

        bool ConsiderForThrottling() const;

        u32 m_LastMessageReceivedTime;
        u32 m_PreventThrottleTimer;
    };

    atFixedArray<PlayerThrottleData, MAX_NUM_PHYSICAL_PLAYERS> m_PlayerThrottleData;

    //PURPOSE
    // Contains information about the target data to send to each player per update interval
    // along with how much data has currently been sent
    struct PlayerBandwidthData
    {
        enum UpdateLevel
        {
            LOW_UPDATE_LEVEL,
            MEDIUM_UPDATE_LEVEL,
            HIGH_UPDATE_LEVEL
        };

        PlayerBandwidthData() :
        m_UpdateLevel(LOW_UPDATE_LEVEL),
        m_CurrentBandwidth(0),
        m_TargetBandwidth(0)
        {
        }

        UpdateLevel   m_UpdateLevel;      // current update level
        unsigned      m_CurrentBandwidth; // current data sent in this update interval
        unsigned      m_TargetBandwidth;  // target data to be sent in each update interval
    };

    atFixedArray<PlayerBandwidthData, MAX_NUM_PHYSICAL_PLAYERS> m_PlayerBandwidthInfo;

    static bank_u32 BANDWIDTH_ADVANCE_INTERVAL;  // time interval before the target bandwidth can be increased
    static bank_u32 RELIABLE_RESEND_THRESHOLD;   // number of unacked reliable messages before we start throttling bandwidth
    static bank_u32 UNRELIABLE_RESEND_THRESHOLD; // number of unacked unreliable messages before we start throttling bandwidth

    unsigned m_SendInterval;                 // The current send interval for network packets.

#if __BANK
    bool     m_OverridePlayerBandwidthTargets;
    bool     m_OverrideTargetUpstreamBandwidth;
    unsigned m_OverriddenTargetUpstreamBandwidth;
#endif // __BANK

private:

    static const unsigned GAME_UPDATE_INTERVAL       = 100;   // expected network update interval in milliseconds
    static const unsigned UPDATE_INTERVAL            = 1000;  // bandwidth manager's update interval
    static const unsigned ESTIMATED_UDP_HEADER_SIZE  = 480;   // estimated size of UDP packet header
    static const unsigned VOICE_BANDWIDTH_USAGE      = 30000; // bandwidth reserved for use by voice library

    //PURPOSE
    // Handles an incoming network message
    //PARAMS
    // messageData - Data describing the incoming message
    void OnMessageReceived(const ReceivedMessageData &messageData);

    //PURPOSE
    // Calculate the update levels for the players. The default behaviour is
    // to assign all active players with a high update level, this can be overridden
    // to assign update levels on game specific criteria
    //PARAMS
    // numLowLevelPlayers    - The number of player assigned a low update level
    // numMediumLevelPlayers - The number of player assigned a medium update level
    // numHighLevelPlayers   - The number of player assigned a high update level
    virtual unsigned CalculatePlayerUpdateLevels(unsigned &numLowLevelPlayers,
                                                 unsigned &numMediumLevelPlayers,
                                                 unsigned &numHighLevelPlayers);

    //PURPOSE
    // Updates the target outgoing game data bandwidth into each player based on their player's location
    void UpdateGameDataTargetBandwidth();

    //PURPOSE
    // Calculates which share of bandwidth each player in the session is allocated. This
    // can be overriden for game specific allocation strategies
    virtual void UpdatePlayerBandwidthInfo();

    //PURPOSE
    // Resets the current data sent for each player (called once per update interval)
    void ResetPlayerBandwidthData();

    //PURPOSE
    // Recalculates the average number of sync failures over a period of time
    void RecalculateSyncFailureAverage();

    //PURPOSE
    // Resets the number of unreliable resends for all players
    void ResetUnreliableResends();

    netConnectionManager *m_ConnectionMgr;
    netLog               *m_Log;

    // Event handler wrapper object for connection manager events
    NetworkPlayerEventDelegate m_Dlgt;

    bool     m_AllowBandwidthThrottling;     // Indicates whether bandwidth should be throttled based on the number of dropped updates

    unsigned m_TargetTotalUpstreamBandwidth; // target upstream bandwidth in Kbps for local connection
    unsigned m_TargetGameUpstreamBandwidth;  // target upstream bandwidth in Kbps for game data (excludes UDP and Rage headers)
    unsigned m_LastUpdate;                   // last update period
    unsigned m_LastTargetAdjustment;         // last target adjustment
    BANK_ONLY(unsigned m_LastBandwidthSampleTime);      // last time bandwidth was sampled

    atFixedArray<unsigned, MAX_NUM_PHYSICAL_PLAYERS> m_NumUnreliableResends;

    mutable unsigned m_NumFailuresPerUpdate;           // The number of times that data could not be sent, per bandwidth mgr update, due to target bandwidth being exceeded
    unsigned         m_AverageFailuresPerUpdate;       // The running average of the failures, calculated from m_CachedFailuresPerSync
    unsigned         m_LastTimeAverageFailuresNonZero; // The last time we had a non-zero average failure
    unsigned         m_CachedFailurePos;               // The current cached failure

    // Cached number of sync failures - used to calculate m_AverageFailuresPerUpdate
    atFixedArray<unsigned, NUM_SYNC_FAILURES_TO_CACHE> m_CachedFailuresPerSync;

#if __BANK
    static const unsigned MAX_STATS = 10;    // The maximum number of bandwidth stats

    unsigned m_NumBandwidthStats; // The number of bandwidth stats

    // bandwidth stats storage
    atFixedArray<netBandwidthStatistics*, MAX_STATS> m_BandwidthStats;

    // socket & channel recorders
    netBandwidthRecorder    m_socketRecorder;
    netBandwidthRecorder    m_gameChannelRecorder;
    netBandwidthRecorder    m_sessionVoiceChannelRecorder;
    netBandwidthRecorder    m_sessionActivityChannelRecorder;
    netBandwidthRecorder    m_sessionGameChannelRecorder;
    netBandwidthRecorder    m_voiceChannelRecorder;
#endif // __BANK
};

} // namespace rage

#endif  // NETBANDWIDTHMGR_H
