//
// netobjectidmgr.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "netobjectidmgr.h"

// game includes
#include "fwnet/netinterface.h"
#include "fwnet/neteventmgr.h"
#include "fwnet/neteventtypes.h"
#include "fwnet/netlogginginterface.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netplayer.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"
#include "fwscript/scriptinterface.h"
#include "fwsys/timer.h"

NETWORK_OPTIMISATIONS()

// DAN TEMP
PARAM(disableObjectIDCheck, "[network] Disable the object ID free list check");

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, objectidmgr, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_objectidmgr

//PURPOSE
// This message is sent to remote players in a session to request a list of any
// object IDs within the player's object ID range
class RequestObjectIdsMsg
{
public:

    NET_MESSAGE_DECL(RequestObjectIdsMsg, REQUEST_OBJECT_IDS_MSG);

    //PURPOSE
    // Class constructor
    // PARAMS
    // playerIndex        - Player index of the player sending the message
    // idRangeOffsetIndex - ID range offset of the player sending the message
    RequestObjectIdsMsg(PhysicalPlayerIndex playerIndex,
                        int                 idRangeOffsetIndex) :
    m_IdRangeOffsetIndex(idRangeOffsetIndex)
    , m_PlayerIndex(playerIndex) 
    {}

    RequestObjectIdsMsg()  {}

    int                 GetIdRangeOffsetIndex() const { return m_IdRangeOffsetIndex; }
    PhysicalPlayerIndex GetPhysicalPlayerIndex() const { return m_PlayerIndex; }
    //PURPOSE
    // Logs the contents of the message
    //PARAMS
    // received - Is this message being sent or received?
    // seqNum   - Sequence number for the message
    // player   - Player the message was from/to
    void WriteToLogFile(bool received, netSequence seqNum, const netPlayer &player) const;

    //PURPOSE
    // Serialise the contents of the specified message to/from the specified bit buffer
    //PARAMS
    // bb -  The bit buffer to serialise the message data from/to
    // msg - The message to serialise
    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerInt(msg.m_IdRangeOffsetIndex, SIZEOF_ID_RANGE_OFFSET) &&
               bb.SerUns(msg.m_PlayerIndex, SIZEOF_PLAYER_ID);
    }

private:

    static const unsigned SIZEOF_ID_RANGE_OFFSET = datBitsNeeded<MAX_NUM_PHYSICAL_PLAYERS>::COUNT + 1;
    static const unsigned SIZEOF_PLAYER_ID       = datBitsNeeded<MAX_NUM_PHYSICAL_PLAYERS>::COUNT;

    int                 m_IdRangeOffsetIndex;
    PhysicalPlayerIndex m_PlayerIndex;
};

//PURPOSE
// a message describing objects IDs in use within a specified players ID range
// The message also contains a list of packed IDs outside of the target players range
// that are used to replace an IDs with the player range in use by the local machine.
class InformObjectIdsMsg
{
public:

    NET_MESSAGE_DECL(InformObjectIdsMsg, INFORM_OBJECT_IDS_MSG);

    InformObjectIdsMsg(netObjectIDMgr      &idMgr,
                       PhysicalPlayerIndex  playerIndex,
                       netLoggingInterface &log) :
    m_IdMgr(idMgr)
    , m_PlayerIndex(playerIndex)
    , m_IdRangeOffsetIndex(-1)
    , m_Log(log)
    {}

    InformObjectIdsMsg(netObjectIDMgr		&idMgr,
                       PhysicalPlayerIndex  playerIndex,
                       int					idRangeOffsetIndex,
                       netLoggingInterface &log) :
    m_IdMgr(idMgr)
    , m_PlayerIndex(playerIndex)
    , m_IdRangeOffsetIndex(idRangeOffsetIndex) 
    , m_Log(log)
    {}

    //PURPOSE
    // Logs the contents of the message
    //PARAMS
    // received - Is this message being sent or received?
    // seqNum   - Sequence number for the message
    // player   - Player the message was from/to
    void WriteToLogFile(bool received, netSequence seqNum, const netPlayer &player) const;

    NET_MESSAGE_EXPORT(bb, msg)
	{
		msg.m_IdMgr.PackObjectIDData(msg.m_PlayerIndex, msg.m_IdRangeOffsetIndex, bb, msg.m_Log);
        return true;
	}

	NET_MESSAGE_IMPORT(bb, msg)
	{
		msg.m_IdMgr.UnpackObjectIDData(msg.m_PlayerIndex, bb, msg.m_Log);
        return true;
	}

private:

    netLoggingInterface    &m_Log;
    PhysicalPlayerIndex     m_PlayerIndex;
    int						m_IdRangeOffsetIndex;
    netObjectIDMgr			&m_IdMgr;
};

NET_MESSAGE_IMPL(RequestObjectIdsMsg);
NET_MESSAGE_IMPL(InformObjectIdsMsg);

void RequestObjectIdsMsg::WriteToLogFile(bool received, netSequence seqNum, const netPlayer &player) const
{
    if (received)
    {
        NetworkLogUtils::WriteMessageHeader(netInterface::GetMessageLog(), received, seqNum, player, "RECEIVED_REQUEST_OBJECT_IDS", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(netInterface::GetMessageLog(), received, seqNum, player, "SENDING_REQUEST_OBJECT_IDS", "");
    }

    netInterface::GetMessageLog().WriteDataValue("ID Range Offset Index", "%d", m_IdRangeOffsetIndex);
    netInterface::GetMessageLog().WriteDataValue("Physical Player Index", "%d", m_PlayerIndex);
}

void InformObjectIdsMsg::WriteToLogFile(bool received, netSequence seqNum, const netPlayer &player) const
{
    if (received)
    {
        NetworkLogUtils::WriteMessageHeader(netInterface::GetMessageLog(), received, seqNum, player, "RECEIVED_INFORM_OBJECT_IDS", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(netInterface::GetMessageLog(), received, seqNum, player, "SENDING_INFORM_OBJECT_IDS", "");
    }
}

unsigned netObjectIDMgr::ms_RequestIdThreshold = netObjectIDMgr::DEFAULT_REQUEST_ID_THRESHOLD;
unsigned netObjectIDMgr::ms_NumIdsToRequest = netObjectIDMgr::DEFAULT_NUM_IDS_TO_REQUEST;
unsigned netObjectIDMgr::ms_MinTimeBetweenRequests = netObjectIDMgr::DEFAULT_MIN_TIME_BETWEEN_REQUESTS;
unsigned netObjectIDMgr::ms_ObjectIdReleaseThreshold = netObjectIDMgr::DEFAULT_OBJECT_ID_RELEASE_THRESHOLD;

netObjectIDMgr::netObjectIDMgr() 
: m_Initialised(false)
, m_InitialIDsAllocated(false)
, m_ObjectIdRangeIndex(-1)
, m_WaitingForObjectIDs(0)
, m_LastObjectIDmessage(0)
, m_TimeOfLastIDRequest(0)
, m_Log(0)
{
    m_Dlgt.Bind(this, &netObjectIDMgr::OnMessageReceived);
}

void netObjectIDMgr::Init(netLoggingInterface *log)
{
    gnetDebug1("Init");
    
    netInterface::GetPlayerMgr().AddDelegate(&m_Dlgt);

    m_Log                 = log;
    m_ObjectIdRangeIndex  = -1;
    m_WaitingForObjectIDs = 0;
    m_LastObjectIDmessage = 0;
    m_InitialIDsAllocated = TryToAllocateInitialObjectIDs();
    m_TimeOfLastIDRequest = 0;
    m_Initialised         = true;
}

void netObjectIDMgr::Shutdown()
{
    gnetDebug1("Shutdown");
    
    netInterface::GetPlayerMgr().RemoveDelegate(&m_Dlgt);

    m_FreeObjectIdQueue.Reset();

    m_InitialIDsAllocated = false;
    m_Initialised         = false;
}

void netObjectIDMgr::Update()
{
    // try to allocate the initial object IDs if this has not already been done
    if(!m_InitialIDsAllocated)
    {
        m_InitialIDsAllocated = TryToAllocateInitialObjectIDs();
    }
    else
    {
        // check if we are running low on IDs
        if(GetNumFreeObjectIDs() <= ms_RequestIdThreshold)
        {
            unsigned numOtherPlayers = netInterface::GetNumPhysicalPlayers() - 1;

            if(numOtherPlayers > 0)
            {
                unsigned currentTime = sysTimer::GetSystemMsTime();

                if((currentTime - m_TimeOfLastIDRequest) > ms_MinTimeBetweenRequests)
                {
                    unsigned numIDsToRequest = ms_NumIdsToRequest / numOtherPlayers;
                    objectIdRequestEvent::Trigger(numIDsToRequest);

                    m_TimeOfLastIDRequest = currentTime;

                    gnetDebug1("Update :: Requesting %d IDs from each player", numIDsToRequest);
                }
            }
        }

        // DAN TEMP - extra validation for bad object ID behaviour
#if __BANK
        if(!PARAM_disableObjectIDCheck.Get())
        {
            if(!IsWaitingForObjectIDs(false))
            {
                int numFreeIDs = m_FreeObjectIdQueue.GetCount();

                for(int index = 0; index < numFreeIDs; index++)
                {
                    ObjectId objectID = m_FreeObjectIdQueue[index];
                    netObject *networkObject = netInterface::GetObjectManager().GetNetworkObject(objectID, true);
                    NETWORK_QUITF(networkObject==0, "A network object exists with a network id on the free list! (%p:%d)", networkObject, objectID);
                }
            }
        }
#endif // __BANK
    }
}

void netObjectIDMgr::PlayerHasJoined(const netPlayer &player)
{
    gnetDebug1("PlayerHasJoined :: %s [%u], InitialIdsAllocated: %s, SessionEstablished: %s", 
        player.GetLogName(), 
        player.GetPhysicalPlayerIndex(), 
        m_InitialIDsAllocated ? "True" : "False",
        netInterface::IsSessionEstablished() ? "True" : "False");
    
    // send the newly joining player which IDs within their range
    // we are currently using
    if(gnetVerify(player.GetPhysicalPlayerIndex() < MAX_NUM_PHYSICAL_PLAYERS) && player.IsRemote() && !player.IsBot())
    {
        if(m_InitialIDsAllocated && m_ObjectIdRangeIndex >= 0)
        {
            gnetDebug1("PlayerHasJoined :: Requesting Object Ids, ObjectIdRangeIndex: %d", m_ObjectIdRangeIndex);
            
            RequestObjectIdsMsg requestMsg(netInterface::GetLocalPhysicalPlayerIndex(), m_ObjectIdRangeIndex);
            netSequence seq = 0;
            netInterface::SendReliableMessage(&player, requestMsg, &seq);
            requestMsg.WriteToLogFile(false, seq, player);

            BANK_ONLY(netInterface::GetPlayerMgr().IncreaseMiscReliableMessagesSent());

            // if we are currently joining a session and waiting for other players to
            // inform us about their object IDs include this new joiner in the process
			if(!netInterface::IsSessionEstablished())
            {
                gnetDebug1("PlayerHasJoined :: WaitingForObjectIDs: %08x -> %08x", m_WaitingForObjectIDs, m_WaitingForObjectIDs | (1 << player.GetPhysicalPlayerIndex()));
                m_WaitingForObjectIDs |= (1<<player.GetPhysicalPlayerIndex());
            }
        }
    }
}

void netObjectIDMgr::PlayerHasLeft(const netPlayer &player)
{
    gnetDebug1("PlayerHasLeft :: %s [%u]", player.GetLogName(), player.GetPhysicalPlayerIndex());
    
    if(gnetVerify(player.GetPhysicalPlayerIndex() < MAX_NUM_PHYSICAL_PLAYERS))
    {
        gnetDebug1("PlayerHasLeft :: WaitingForObjectIDs: %08x -> %08x", m_WaitingForObjectIDs, m_WaitingForObjectIDs & ~(1 << player.GetPhysicalPlayerIndex()));
        m_WaitingForObjectIDs &= ~(1<<player.GetPhysicalPlayerIndex());
    }
}

void netObjectIDMgr::SetObjectIdRangeIndex(int objectIdRangeIndex)
{
    gnetAssertf((m_ObjectIdRangeIndex == -1) || (m_ObjectIdRangeIndex == objectIdRangeIndex), "SetObjectIdRangeIndex :: Object ID range index has already been set!");
    if(m_ObjectIdRangeIndex != objectIdRangeIndex)
    {
        gnetDebug1("SetObjectIdRangeIndex :: %d -> %d", m_ObjectIdRangeIndex, objectIdRangeIndex);
        m_ObjectIdRangeIndex = objectIdRangeIndex;
    }
}

bool netObjectIDMgr::IsWaitingForObjectIDs(bool includeBufferTime)
{
    u32 physicalPlayerBits = 0;

    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
    const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
    {
	    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

        if(remotePlayer && remotePlayer->IsValid() && gnetVerify(!remotePlayer->IsMyPlayer()))
        {
            PhysicalPlayerIndex playerIndex = remotePlayer->GetPhysicalPlayerIndex();
            if(gnetVerify(playerIndex < MAX_NUM_PHYSICAL_PLAYERS))
            {
                physicalPlayerBits |= (1<<playerIndex);
            }
        }
    }

    bool waitingOnIDs = ((m_WaitingForObjectIDs & physicalPlayerBits) != 0);
    waitingOnIDs |= !m_InitialIDsAllocated;

	if(includeBufferTime)
	{
		waitingOnIDs |= ((fwTimer::GetSystemTimeInMilliseconds() - m_LastObjectIDmessage) < 500);
	}

#if !__NO_OUTPUT
    static unsigned s_LastLoggingStamp = 0;
    static const unsigned LOG_INTERVAL = 1000;

    if(waitingOnIDs)
    {
        if((sysTimer::GetSystemMsTime() - s_LastLoggingStamp) > LOG_INTERVAL)
        {
            gnetDebug1("IsWaitingForObjectIDs :: Waiting - InitialIDsAllocated: %s, WaitingForObjectIDs: 0x%08x, PhysicalPlayerBits: 0x%08x, IncludeBufferTime: %s, SystemTimeMs: %u, LastObjectIDmessage: %u",
                       m_InitialIDsAllocated ? "True" : "False",
                       m_WaitingForObjectIDs,
                       physicalPlayerBits,
                       includeBufferTime ? "True" : "False",
                       fwTimer::GetSystemTimeInMilliseconds(),
                       m_LastObjectIDmessage);

            // log individual players that we are waiting for ids from
            for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
            {
                const netPlayer *remotePlayer = remotePhysicalPlayers[index];

                if(remotePlayer && remotePlayer->IsValid() && gnetVerify(!remotePlayer->IsMyPlayer()))
                {
                    PhysicalPlayerIndex playerIndex = remotePlayer->GetPhysicalPlayerIndex();
                    if(IsWaitingForObjectIDs(playerIndex))
                    {
                        gnetDebug1("IsWaitingForObjectIDs :: Waiting on Object Ids from %s [%u]", remotePlayer->GetLogName(), playerIndex);
                    }
                }
            }

            s_LastLoggingStamp = sysTimer::GetSystemMsTime();
        }
    }
#endif

    return waitingOnIDs;
}

bool netObjectIDMgr::IsWaitingForObjectIDs(PhysicalPlayerIndex playerIndex)
{
	return (m_WaitingForObjectIDs & (1<<playerIndex)) != 0;
}

bool netObjectIDMgr::CanAcceptObjectIDEventsFrom(PhysicalPlayerIndex playerIndex)
{
    if(m_InitialIDsAllocated && playerIndex != INVALID_PLAYER_INDEX)
    {
        if((m_WaitingForObjectIDs & (1<<playerIndex)) == 0)
        {
            return true;
        }
    }

    return false;
}

ObjectId netObjectIDMgr::GetNewObjectId()
{
    gnetAssertf(m_Initialised, "GetNewObjectId :: Not initialised!");

#if __DEV
    if(m_FreeObjectIdQueue.IsEmpty())
    {
        Quitf("GetNewObjectId :: Ran out of object Ids!");
    }
#endif

    ObjectId newObjectID = m_FreeObjectIdQueue.Pop();

    if(m_Log)
    {
        NetworkLogUtils::WriteLogEvent(*m_Log, "ALLOCATE_ID", "%d", newObjectID);
    }

#if __ASSERT
    netObject *networkObject = netInterface::GetObjectManager().GetNetworkObject(newObjectID, true);
    gnetAssertf(!networkObject, "GetNewObjectId :: Network Object Already Exists! ObjectId: %u", newObjectID);
#endif

    return newObjectID;
}

void netObjectIDMgr::FreeObjectId(ObjectId objectID)
{
    gnetAssertf(m_Initialised, "FreeObjectId :: Not initialised!");

    bool success = FreeObjectIdPrivate(objectID);

    if(success)
    {
        if(m_Log)
        {
            NetworkLogUtils::WriteLogEvent(*m_Log, "FREE_ID", "%d", objectID);
        }

        ReleaseExcessObjectIDs();
    }
}

bool netObjectIDMgr::HasFreeObjectIdsAvailable(u32 count, bool forScriptObject)
{
    gnetAssertf(m_Initialised, "HasFreeObjectIdsAvailable :: Not initialised!");

    u32 numFreeIDs  = GetNumFreeObjectIDs();
    u32 requiredIDs = count;

    if(!forScriptObject)
    {
        u32 numReservedIds = scriptInterface::GetNumRequiredScriptEntities();

        requiredIDs += numReservedIds;
    }

    bool hasFreeObjectIDs = (numFreeIDs >= requiredIDs);

    return hasFreeObjectIDs;
}

bool netObjectIDMgr::ObjectIDIsFree(ObjectId objectID)
{
    return m_FreeObjectIdQueue.Find(objectID);
}

static int CompareObjectIDs(const void *paramA, const void *paramB)
{
    const ObjectId objectIDA = *(static_cast<const ObjectId *>(paramA));
    const ObjectId objectIDB = *(static_cast<const ObjectId *>(paramB));

    if(objectIDA < objectIDB)
    {
        return -1;
    }
    else if(objectIDA > objectIDB)
    {
        return 1;
    }

    return 0;
}

void netObjectIDMgr::OnMessageReceived(const ReceivedMessageData &messageData)
{
    unsigned msgId = 0;
    if(netMessage::GetId(&msgId, messageData.m_MessageData, messageData.m_MessageDataSize))
    {
        if(msgId == RequestObjectIdsMsg::MSG_ID())
        {
            HandleRequestObjectIdsMsg(messageData);
        }
        else if(msgId == InformObjectIdsMsg::MSG_ID())
        {
            HandleInformObjectIdsMsg(messageData);
        }
    }
}

void netObjectIDMgr::HandleRequestObjectIdsMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        RequestObjectIdsMsg requestMsg;
        requestMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize);
        requestMsg.WriteToLogFile(true, messageData.m_NetSequence, *messageData.m_FromPlayer);

        gnetDebug3("HandleRequestObjectIdsMsg :: PlayerId: %s, IdRangeOffsetIndex: %d",
                   messageData.m_FromPlayer->GetLogName(),
                   requestMsg.GetIdRangeOffsetIndex());

		if(gnetVerifyf(requestMsg.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "HandleRequestObjectIdsMsg :: Invalid physical player index"))
		{
			InformObjectIdsMsg informMsg(*this, requestMsg.GetPhysicalPlayerIndex(), requestMsg.GetIdRangeOffsetIndex(), netInterface::GetMessageLog());
            netSequence seq = 0;
			netInterface::SendReliableMessage(messageData.m_FromPlayer, informMsg, &seq); 
            informMsg.WriteToLogFile(false, seq, *messageData.m_FromPlayer);

            BANK_ONLY(netInterface::GetPlayerMgr().IncreaseMiscReliableMessagesSent());
		}
	}
}

void netObjectIDMgr::HandleInformObjectIdsMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        InformObjectIdsMsg informMsg(*this, messageData.m_FromPlayer->GetPhysicalPlayerIndex(), netInterface::GetMessageLog());
        informMsg.WriteToLogFile(true, messageData.m_NetSequence, *messageData.m_FromPlayer);
        informMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize);
    }
}

bool netObjectIDMgr::FreeObjectIdPrivate(ObjectId objectID)
{
    bool queueFull = m_FreeObjectIdQueue.IsFull();
    bool IdExists  = m_FreeObjectIdQueue.Find(objectID);
    bool success   = false;

    gnetAssertf(!queueFull, "FreeObjectIdPrivate :: Network object manager object id queue full!");
    gnetAssertf(!IdExists, "FreeObjectIdPrivate :: Freed Object id %d already on the object ID queue!", objectID);

    if(!queueFull && !IdExists)
    {
        success = m_FreeObjectIdQueue.Push(objectID);

        if(!success)
        {
            gnetAssertf(0, "FreeObjectIdPrivate :: Failed pushing free ID to queue!");
        }
    }

    return success;
}

void netObjectIDMgr::ReleaseExcessObjectIDs()
{
    gnetAssertf(m_Initialised, "ReleaseExcessObjectIDs :: Not initialised!");
    gnetDebug3("ReleaseExcessObjectIDs");

    // don't try to release object IDs when the event manager is not
    // initialised, this can happen when the game is shutting down
    if(!netInterface::IsInitialised() || netInterface::IsShuttingDown())
    {
        return;
    }

    if(GetNumFreeObjectIDs() >= ms_ObjectIdReleaseThreshold)
    {
        // build a sorted list of objectIDs on the queue
        ObjectId freeIDs[MAX_TOTAL_OBJECT_IDS];
        u32 numFreeIDs = 0;

        while(!m_FreeObjectIdQueue.IsEmpty())
        {
            gnetAssert(numFreeIDs < MAX_TOTAL_OBJECT_IDS);

            if(numFreeIDs < MAX_TOTAL_OBJECT_IDS)
            {
                freeIDs[numFreeIDs] = m_FreeObjectIdQueue.Pop();
                numFreeIDs++;
            }
        }

        qsort(freeIDs, numFreeIDs, sizeof(ObjectId), CompareObjectIDs);

        // find how many object Ids that we own we initially assigned to each player
        u32 playerObjectIDsOwned[MAX_NUM_PHYSICAL_PLAYERS];

        u32              objectIDIndex       = 0;
        u32              maxObjectIdsOwned   = 0;
        u32              startIndex          = 0;
        const netPlayer *maxOwnedPlayer      = 0;

        for(PhysicalPlayerIndex idRangeIndex = 0; idRangeIndex < MAX_NUM_PHYSICAL_PLAYERS; idRangeIndex++)
        {
            u32 playerObjectIDStart            = (idRangeIndex * MAX_NUM_OBJECT_IDS_PER_PLAYER) + 1;
            u32 playerObjectIDEnd              = playerObjectIDStart + MAX_NUM_OBJECT_IDS_PER_PLAYER;
            playerObjectIDsOwned[idRangeIndex] = 0;

            while(freeIDs[objectIDIndex] >= playerObjectIDStart && freeIDs[objectIDIndex] < playerObjectIDEnd && objectIDIndex < numFreeIDs)
            {
                playerObjectIDsOwned[idRangeIndex]++;
                objectIDIndex++;
            }

            if(playerObjectIDsOwned[idRangeIndex] > maxObjectIdsOwned)
            {
                if(idRangeIndex != m_ObjectIdRangeIndex)
                {
                    const netPlayer *player = netInterface::GetPlayerFromObjectIdRangeIndex(idRangeIndex);

                    if(player && player->IsValid())
                    {
                        gnetAssert(!player->IsMyPlayer());

                        maxObjectIdsOwned = playerObjectIDsOwned[idRangeIndex];
                        maxOwnedPlayer    = player;
                        startIndex        = objectIDIndex - maxObjectIdsOwned;
                    }
                }
            }
        }

        if(maxOwnedPlayer)
        {
            if(maxObjectIdsOwned > objectIdFreedEvent::MAX_OBJECT_IDS_TO_FREE)
            {
                maxObjectIdsOwned = objectIdFreedEvent::MAX_OBJECT_IDS_TO_FREE;
            }

            // send a network event containing a list of object IDs to free
            ObjectId *objectIDsToFree = freeIDs + startIndex;
            objectIdFreedEvent::Trigger(objectIDsToFree, maxObjectIdsOwned, maxOwnedPlayer->GetPhysicalPlayerIndex());
        }
        else
        {
            // make sure we push all object IDs back on the queue if don't have a player to free them
            startIndex        = 0;
            maxObjectIdsOwned = 0;
        }

        // push the remaining object IDs back onto the queue
        for(u32 index = 0; index < startIndex; index++)
        {
            FreeObjectIdPrivate(freeIDs[index]);
        }

        for(u32 index = startIndex + maxObjectIdsOwned; index < numFreeIDs; index++)
        {
            FreeObjectIdPrivate(freeIDs[index]);
        }
    }
}

u32 netObjectIDMgr::GetNumFreeObjectIDs()
{
    gnetAssertf(m_Initialised, "GetNumFreeObjectIDs :: Not initialised!");
    u32 numFreeObjectIDs = m_FreeObjectIdQueue.GetCount();
    return numFreeObjectIDs;
}

bool netObjectIDMgr::RemoveObjectIDIfExists(ObjectId objectID)
{
    int  index   = 0;
    bool success = m_FreeObjectIdQueue.Find(objectID, &index);

    if(success)
    {
        NetworkLogUtils::WriteLogEvent(*m_Log, "DELETE_ID", "%d", objectID);

        m_FreeObjectIdQueue.Delete(index);
        gnetAssertf(!m_FreeObjectIdQueue.Find(objectID), "RemoveObjectIDIfExists :: Object ID removed is still on the free queue!");
    }

    return success;
}

void netObjectIDMgr::PackObjectIDData(PhysicalPlayerIndex playerIndex, int idRangeOffsetIndex, datBitBuffer &bitBuffer, netLoggingInterface &log)
{
    gnetAssertf(m_Initialised, "PackObjectIDData :: Not initialised!");
    gnetAssert(playerIndex < MAX_NUM_PHYSICAL_PLAYERS);

    gnetDebug3("PackObjectIDData :: IdRangeOffsetIndex: %d", idRangeOffsetIndex);

    if(playerIndex < MAX_NUM_PHYSICAL_PLAYERS)
    {
        if(gnetVerifyf(idRangeOffsetIndex >= 0, "PackObjectIDData :: Invalid id range offset!"))
        {
            u32 playerObjectIDStart = (idRangeOffsetIndex * MAX_NUM_OBJECT_IDS_PER_PLAYER) + 1;
            u32 playerObjectIDEnd   = playerObjectIDStart + MAX_NUM_OBJECT_IDS_PER_PLAYER;

            ObjectId idsInUseLocally[MAX_TOTAL_OBJECT_IDS];
            unsigned numInUseIDs = 0;
            unsigned numLocallyOwnedIDs = 0;
            netInterface::GetObjectIDsInUseLocally(idsInUseLocally, numInUseIDs, numLocallyOwnedIDs, playerObjectIDStart, playerObjectIDEnd);

            bitBuffer.WriteUns(numInUseIDs, SIZEOF_OBJECTID);
            gnetDebug3("PackObjectIDData :: NumInUseIDs: %d", numInUseIDs);
            log.WriteDataValue("PackObjectIDData :: NumInUseIDs", "%d", numInUseIDs);

            for(unsigned index = 0; index < numInUseIDs; index++)
            {
                bitBuffer.WriteUns(idsInUseLocally[index], SIZEOF_OBJECTID);
                gnetDebug3("PackObjectIDData :: InUseId: %d", idsInUseLocally[index]);
                log.WriteDataValue("PackObjectIDData: InUseId", "%d", idsInUseLocally[index]);
            }

            ObjectId freeIDs [MAX_TOTAL_OBJECT_IDS];
            u32 numFreeIDs = 0;

            // now write any ids that are on our free queue
            while(!m_FreeObjectIdQueue.IsEmpty())
            {
                gnetAssert(numFreeIDs < MAX_TOTAL_OBJECT_IDS);

                if(numFreeIDs < MAX_TOTAL_OBJECT_IDS)
                {
                    freeIDs[numFreeIDs] = m_FreeObjectIdQueue.Pop();
                    numFreeIDs++;
                }
            }

            int oldCursorPos = bitBuffer.GetCursorPos();
            bitBuffer.WriteUns(0, SIZEOF_OBJECTID);

            u32 numIDsReleased  = 0;
            u32 numIDsToReplace = numLocallyOwnedIDs;

            // don't replace any IDs before we have finished joining the session fully due
            // to waiting for object IDs
            if(!netInterface::IsSessionEstablished() || !gnetVerifyf(m_ObjectIdRangeIndex != -1, "The match is started but the local player has an invalid id range offset!"))
            {
                numIDsToReplace = 0;
            }

            unsigned localRangeStart  = (m_ObjectIdRangeIndex * MAX_NUM_OBJECT_IDS_PER_PLAYER) + 1;
            unsigned localRangeEnd    = localRangeStart + MAX_NUM_OBJECT_IDS_PER_PLAYER;

            for(u32 index = 0; index < numFreeIDs; index++)
            {
                ObjectId objectID = freeIDs[index];

                if(objectID >= playerObjectIDStart && objectID < playerObjectIDEnd)
                {
                    if(m_Log)
                    {
                        m_Log->Log("\t\tReleasing free Object id %d\r\n", objectID);
                    }
                }
                else
                {
                    if((numIDsToReplace > 0) && (objectID >= localRangeStart && objectID < localRangeEnd))
                    {
                        if(m_Log)
                        {
                            m_Log->Log("\t\tPacking free Object id %d\r\n", objectID);
                        }
                        log.WriteDataValue("Replacement ID", "%d", objectID);

                        bitBuffer.WriteUns(objectID, SIZEOF_OBJECTID);
                        numIDsReleased++;
                        numIDsToReplace--;
                    }
                    else
                    {
                        // return it to the queue
                        bool bSuccess = m_FreeObjectIdQueue.Push(objectID);

                        if(!bSuccess)
                        {
                            gnetAssertf(0, "Failed pushing free ID to queue!");
                        }
                    }
                }
            }

            // update the ID count
            int newCursorPos = bitBuffer.GetCursorPos();
            bitBuffer.SetCursorPos(oldCursorPos);
            bitBuffer.WriteUns(numIDsReleased, SIZEOF_OBJECTID);
            bitBuffer.SetCursorPos(newCursorPos);
        }
    }
}

void netObjectIDMgr::UnpackObjectIDData(PhysicalPlayerIndex playerIndex, const datBitBuffer &bitBuffer, netLoggingInterface &log)
{
    gnetAssertf(m_Initialised, "UnpackObjectIDData :: Not initialised!");

    gnetAssert(playerIndex < MAX_NUM_PHYSICAL_PLAYERS);

    if(playerIndex < MAX_NUM_PHYSICAL_PLAYERS)
    {
        gnetDebug3("UnpackObjectIDData :: ObjectIdRangeIndex: %d, From: %d", m_ObjectIdRangeIndex, playerIndex);
        if(gnetVerifyf(m_ObjectIdRangeIndex >= 0, "UnpackObjectIDData :: Invalid ObjectIdRangeIndex!"))
        {
            u32 playerObjectIDStart = (m_ObjectIdRangeIndex * MAX_NUM_OBJECT_IDS_PER_PLAYER) + 1;
            u32 playerObjectIDEnd   = playerObjectIDStart + MAX_NUM_OBJECT_IDS_PER_PLAYER;

            // keep track of which object IDs are in use
            bool aObjectIDsInUse[MAX_NUM_OBJECT_IDS_PER_PLAYER];
            
            for(u32 index = 0; index < MAX_NUM_OBJECT_IDS_PER_PLAYER; index++)
            {
                aObjectIDsInUse[index] = true;
            }

            // grab all free IDs off the queue
            ObjectId outOfRangeIDs [MAX_TOTAL_OBJECT_IDS];
            u32 numOutOfRangeIDs = 0;

            while(!m_FreeObjectIdQueue.IsEmpty())
            {
                ObjectId objectID = m_FreeObjectIdQueue.Pop();

                if(objectID >= playerObjectIDStart && objectID < playerObjectIDEnd)
                {
                    aObjectIDsInUse[objectID - playerObjectIDStart] = false;
                }
                else
                {
                    if(gnetVerify(numOutOfRangeIDs < MAX_TOTAL_OBJECT_IDS))
                    {
                        outOfRangeIDs[numOutOfRangeIDs] = objectID;
                        numOutOfRangeIDs++;
                    }
                }
            }

            // Add the out of range IDs back to the free list
            for(u32 index = 0; index < numOutOfRangeIDs; index++)
            {
                FreeObjectIdPrivate(outOfRangeIDs[index]);
            }

            // update the flags based on the contents of the buffer
            u32 numInUseIDs = 0;
            bitBuffer.ReadUns(numInUseIDs, SIZEOF_OBJECTID);
            log.WriteDataValue("Num IDs in use", "%d", numInUseIDs);
            gnetDebug3("UnpackObjectIDData :: Num IDs in use: %d", numInUseIDs);
            gnetDebug3("UnpackObjectIDData :: Player Object ID Start: %d", playerObjectIDStart);
            gnetDebug3("UnpackObjectIDData :: Player Object ID End:   %d", playerObjectIDEnd);

            for(u32 index = 0; index < numInUseIDs; index++)
            {
                ObjectId objectID = NETWORK_INVALID_OBJECT_ID;
                bitBuffer.ReadUns(objectID, SIZEOF_OBJECTID);
                log.WriteDataValue("In Use ID", "%d", objectID);

                if(objectID < playerObjectIDStart || objectID >= playerObjectIDEnd)
                {
                    gnetAssertf(0, "UnpackObjectIDData :: Received ID in use outside of expected range! ObjectId: %u", objectID);
                }
                else
                {
                    u32 adjustedIndex = objectID - playerObjectIDStart;
                    aObjectIDsInUse[adjustedIndex] = true;
                }
            }

            // add any object IDs not marked in use back onto the queue
            for(u32 index = 0; index < MAX_NUM_OBJECT_IDS_PER_PLAYER; index++)
            {
                if(aObjectIDsInUse[index] == false)
                {
                    ObjectId objectID = static_cast<ObjectId>(playerObjectIDStart + index);
                    FreeObjectIdPrivate(objectID);
                }
            }

            // now read any additional object IDs we have been given
            u32 numReplacedIDs = 0;
            bitBuffer.ReadUns(numReplacedIDs, SIZEOF_OBJECTID);

            for(u32 index = 0; index < numReplacedIDs; index++)
            {
                ObjectId objectID = NETWORK_INVALID_OBJECT_ID;
                bitBuffer.ReadUns(objectID, SIZEOF_OBJECTID);
                log.WriteDataValue("Replacement ID", "%d", objectID);
                gnetDebug3("UnpackObjectIDData :: ReplacementId: %d. Free this ID.", objectID);

                FreeObjectIdPrivate(objectID);
            }

            ReleaseExcessObjectIDs();

#if !__NO_OUTPUT
            // log if we were waiting on objects Ids from this player
            if((m_WaitingForObjectIDs & (1 << playerIndex)) != 0)
            {
                gnetDebug1("UnpackObjectIDData :: WaitingForObjectIDs: %08x -> %08x", m_WaitingForObjectIDs, m_WaitingForObjectIDs & ~(1 << playerIndex));
            }
#endif
            
            // mark that we have received an object ID message from this player
            m_WaitingForObjectIDs &= ~(1<<playerIndex);
            m_LastObjectIDmessage = fwTimer::GetSystemTimeInMilliseconds();
        }
    }
}

void netObjectIDMgr::RequestObjectIDs(const netPlayer &player, unsigned numObjectIDsRequested, ObjectId *returnedObjectIDs, unsigned &numReturnedObjectIDs)
{
    numReturnedObjectIDs = 0;

    if(netInterface::IsSessionEstablished())
    {
        int remoteIdRangeIndex = netInterface::GetObjectIdRangeIndexFromPlayer(player);

        gnetDebug3("RequestObjectIDs :: RemoteIdRangeIndex: %d, ObjectIdRangeIndex: %d", remoteIdRangeIndex, m_ObjectIdRangeIndex);
        if(remoteIdRangeIndex >= 0 && m_ObjectIdRangeIndex >= 0)
        {
            gnetAssertf(remoteIdRangeIndex != m_ObjectIdRangeIndex, "Local and remote object ID ranges are the same!");

            if(gnetVerifyf(numObjectIDsRequested > 0, "RequestObjectIDs :: Requesting 0 object IDs!") &&
               gnetVerifyf(returnedObjectIDs,         "RequestObjectIDs :: Requesting object IDs with NULL for storing the returned IDs!"))
            {
                const unsigned MIN_IDS_TO_KEEP = 100;

                u32 numIDsToReturn = 0;
                u32 numFreeIDs     = GetNumFreeObjectIDs();

                if(numFreeIDs > MIN_IDS_TO_KEEP)
                {
                    numIDsToReturn = Min(numObjectIDsRequested, (numFreeIDs - MIN_IDS_TO_KEEP));
                }

                if(numIDsToReturn > 0)
                {
                    // build a sorted list of objectIDs on the queue
                    ObjectId freeIDs[MAX_TOTAL_OBJECT_IDS];
                    u32 numFreeIDs = 0;

                    while(!m_FreeObjectIdQueue.IsEmpty())
                    {
                        gnetAssert(numFreeIDs < MAX_TOTAL_OBJECT_IDS);

                        if(numFreeIDs < MAX_TOTAL_OBJECT_IDS)
                        {
                            freeIDs[numFreeIDs] = m_FreeObjectIdQueue.Pop();
                            numFreeIDs++;
                        }
                    }

                    qsort(freeIDs, numFreeIDs, sizeof(ObjectId), CompareObjectIDs);

                    int      localIDsOffset   = -1;
                    int      remoteIDsOffset  = -1;
                    unsigned localRangeStart  = (m_ObjectIdRangeIndex * MAX_NUM_OBJECT_IDS_PER_PLAYER) + 1;
                    unsigned remoteRangeStart = (remoteIdRangeIndex   * MAX_NUM_OBJECT_IDS_PER_PLAYER) + 1;
                    unsigned localRangeEnd    = localRangeStart  + MAX_NUM_OBJECT_IDS_PER_PLAYER;
                    unsigned remoteRangeEnd   = remoteRangeStart + MAX_NUM_OBJECT_IDS_PER_PLAYER;
                    unsigned numLocalIDs      = 0;
                    unsigned numRemoteIDs     = 0;

                    for(unsigned index = 0; index < numFreeIDs; index++)
                    {
                        ObjectId freeID = freeIDs[index];

                        if(freeID >= localRangeStart && freeID < localRangeEnd)
                        {
                            if(localIDsOffset == -1)
                            {
                                localIDsOffset = index;
                            }

                            numLocalIDs++;
                        }
                        else if(freeID >= remoteRangeStart && freeID < remoteRangeEnd)
                        {
                            if(remoteIDsOffset == -1)
                            {
                                remoteIDsOffset = index;
                            }

                            numRemoteIDs++;
                        }
                        else
                        {
                            FreeObjectIdPrivate(freeID);
                        }
                    }

                    // return IDs from the requesters range first
                    if(remoteIDsOffset > -1)
                    {
                        ObjectId *remoteIDs = freeIDs + remoteIDsOffset;
                        for(unsigned index = 0; index < numRemoteIDs; index++)
                        {
                            if(numReturnedObjectIDs < numIDsToReturn)
                            {
                                returnedObjectIDs[numReturnedObjectIDs] = remoteIDs[index];
                                numReturnedObjectIDs++;
                            }
                            else
                            {
                                FreeObjectIdPrivate(remoteIDs[index]);
                            }
                        }
                    }

                    // return any additionally required IDs from the local range
                    if(localIDsOffset > -1)
                    {
                        ObjectId *localIDs = freeIDs + localIDsOffset;
                        for(unsigned index = 0; index < numLocalIDs; index++)
                        {
                            if(numReturnedObjectIDs < numIDsToReturn)
                            {
                                returnedObjectIDs[numReturnedObjectIDs] = localIDs[index];
                                numReturnedObjectIDs++;
                            }
                            else
                            {
                                FreeObjectIdPrivate(localIDs[index]);
                            }
                        }
                    }
                }
            }
        }
    }
}

bool netObjectIDMgr::TryToAllocateInitialObjectIDs()
{
    bool allocatedIDs = false;

	// we can't allocate ids until our player has a physical player index assigned
	if (netInterface::GetLocalPlayer()->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
	{
        // we can't allocate IDs until we are in a started session (and all pre-existing players have fully joined)
        if(netInterface::IsSnSessionEstablished() && (netInterface::GetNumActivePlayers() == netInterface::GetNumPhysicalPlayers()))
        {
		    if(gnetVerifyf(!m_InitialIDsAllocated, "TryToAllocateInitialObjectIDs :: Already allocated!"))
		    {
			    if(m_ObjectIdRangeIndex >= 0)
			    {
                    // add all object ids to the queue
				    m_FreeObjectIdQueue.Reset();

				    for(unsigned int index = 0; index < MAX_NUM_OBJECT_IDS_PER_PLAYER; index++)
				    {
					    ObjectId objectID = static_cast<ObjectId>((MAX_NUM_OBJECT_IDS_PER_PLAYER * m_ObjectIdRangeIndex) + index) + 1;
					    FreeObjectIdPrivate(objectID);
				    }

				    m_LastObjectIDmessage = fwTimer::GetSystemTimeInMilliseconds();

				    allocatedIDs = true;

				    // request other players in the session for any object Ids they are using within our range
				    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
                    const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	                for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
                    {
		                const netPlayer *player = remotePhysicalPlayers[index];

					    if(player && 
						   player->IsValid() && 
                           gnetVerify(!player->IsMyPlayer()) &&
                           gnetVerify(player->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
					    {
                            gnetDebug1("TryToAllocateInitialObjectIDs :: Sending RequestObjectIdsMsg to %s [%u], WaitingForObjectIDs: %08x -> %08x", 
                                player->GetLogName(), 
                                player->GetPhysicalPlayerIndex(),
                                m_WaitingForObjectIDs,
                                m_WaitingForObjectIDs | (1 << player->GetPhysicalPlayerIndex()));

						    RequestObjectIdsMsg requestMsg(netInterface::GetLocalPhysicalPlayerIndex(), m_ObjectIdRangeIndex);
                            netSequence seq = 0;
						    netInterface::SendReliableMessage(player, requestMsg, &seq);
                            requestMsg.WriteToLogFile(false, seq, *player);

                            BANK_ONLY(netInterface::GetPlayerMgr().IncreaseMiscReliableMessagesSent());

						    m_WaitingForObjectIDs |= (1<<player->GetPhysicalPlayerIndex());
					    }
				    }

                    gnetDebug1("TryToAllocateInitialObjectIDs :: Succeeded - ObjectIdRangeIndex: %d, WaitingForObjectIDs: %08x, LastObjectIDmessage: %ums", m_ObjectIdRangeIndex, m_WaitingForObjectIDs, m_LastObjectIDmessage);
			    }
		    }
        }
	}

#if !__NO_OUTPUT
    static unsigned s_LastLoggingStamp = 0;
    static const unsigned LOG_INTERVAL = 1000;

    if(!allocatedIDs)
    {
        if((sysTimer::GetSystemMsTime() - s_LastLoggingStamp) > LOG_INTERVAL)
        {
            gnetDebug1("TryToAllocateInitialObjectIDs :: Failed - PhysicalPlayerIndex: %d, SessionEstablished: %s, NumActivePlayers: %u, NumPhysicalPlayers: %u, ObjectIdRangeIndex: %d", 
                       netInterface::GetLocalPlayer()->GetPhysicalPlayerIndex(),
                       netInterface::IsSnSessionEstablished() ? "True" : "False",
                       netInterface::GetNumActivePlayers(),
                       netInterface::GetNumPhysicalPlayers(),
                       m_ObjectIdRangeIndex);

            s_LastLoggingStamp = sysTimer::GetSystemMsTime();
        }
    }
#endif

    return allocatedIDs;
}

void netObjectIDMgr::SetRequestIdThreshold(const unsigned requestIdThreshold)
{
	if(ms_RequestIdThreshold != requestIdThreshold)
	{
		gnetDebug1("SetRequestIdThreshold :: Was: %u, Now: %u", ms_RequestIdThreshold, requestIdThreshold);
		ms_RequestIdThreshold = requestIdThreshold;
	}
}

void netObjectIDMgr::SetNumIdsToRequest(const unsigned numIdsToRequest)
{
	if(ms_NumIdsToRequest != numIdsToRequest)
	{
		gnetDebug1("SetNumIdsToRequest :: Was: %u, Now: %u", ms_NumIdsToRequest, numIdsToRequest);
		ms_NumIdsToRequest = numIdsToRequest;
	}
}

void netObjectIDMgr::SetMinTimeBetweenRequests(const unsigned minTimeBetweenRequests)
{
	if(ms_MinTimeBetweenRequests != minTimeBetweenRequests)
	{
		gnetDebug1("SetMinTimeBetweenRequests :: Was: %u, Now: %u", ms_MinTimeBetweenRequests, minTimeBetweenRequests);
		ms_MinTimeBetweenRequests = minTimeBetweenRequests;
	}
}

void netObjectIDMgr::SetObjectIdReleaseThreshold(const unsigned objectIdReleaseThreshold)
{
	if(ms_ObjectIdReleaseThreshold != objectIdReleaseThreshold)
	{
		gnetDebug1("SetObjectIdReleaseThreshold :: Was: %u, Now: %u", ms_ObjectIdReleaseThreshold, objectIdReleaseThreshold);
		ms_ObjectIdReleaseThreshold = objectIdReleaseThreshold;
	}
}

} // namespace rage
