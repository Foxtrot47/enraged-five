// 
// netleaderboardmgr.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

#ifndef NET_LEADERBOARD_MGR_H
#define NET_LEADERBOARD_MGR_H

//Rage headers
#include "rline/clan/rlclancommon.h"
#include "atl/dlist.h"
#include "data/base.h"
#include "net/status.h"
#include "fwnet/netchannel.h"

namespace rage
{

//Forward definitions
class  sysMemAllocator;
class  rlLeaderboard2GroupSelector;
class netLeaderboardRead;
class  netLeaderboardWrite;
class  netLeaderboard2WriteSession;

//PURPOSE
//  Class responsible to manage the memory allocators used by the read and update leaderboard managers.
class netLeaderboardManageAllocator
{
private:

	//Memory allocator pointer
	sysMemAllocator*  m_Allocator;

	//Previous Memory allocator pointer
	sysMemAllocator*  m_PreviousAllocator;

	//Control start and stop memory usage
	s32  m_AllocatorInUse;

#if !__NO_OUTPUT
	s32  m_UsedMemorySize;
	s32  m_PreviousMemorySize;
	const char* m_AllocatorName;
#endif // !__NO_OUTPUT


public:

	netLeaderboardManageAllocator();
	~netLeaderboardManageAllocator();

	void  Init(sysMemAllocator* allocator, const char* allocatorName = NULL);
	void  Shutdown();
	void  StartUsingMemAllocator();
	void  StopUsingMemAllocator();
	sysMemAllocator* GetAllocator();
};

class netLeaderboardManageAllocatorScopeHelper
{
public:
	netLeaderboardManageAllocatorScopeHelper(netLeaderboardManageAllocator& allocator)
		: m_allocator(allocator)
	{
		m_allocator.StartUsingMemAllocator();
	}

	~netLeaderboardManageAllocatorScopeHelper()
	{
		m_allocator.StopUsingMemAllocator();
	}

	template<class _Type> bool CanAllocate(const char* ASSERT_ONLY(func))
	{
		ASSERT_ONLY(gnetAssertf(m_allocator.GetAllocator()->GetLargestAvailableBlock() > sizeof(_Type), "%s - Not enough memory available(%" SIZETFMT "d) for %" SIZETFMT "d", func ? func : "NO_FUNC_NAME", m_allocator.GetAllocator()->GetMemoryAvailable(), sizeof(_Type));)
		return (m_allocator.GetAllocator()->GetLargestAvailableBlock() > sizeof(_Type));
	}

	netLeaderboardManageAllocator& m_allocator;
};

//PURPOSE
//  This class is responsible for managing any leaderboard read operation.
class netLeaderboardReadMgr
{
public:
	typedef atDNode< netLeaderboardRead*, datBase > netLeaderboardReadListNode;

private:
	//Leaderboards being managed
	atDList< netLeaderboardRead*, datBase > m_Leaderboards;

protected:
	//Manage used memory.
	netLeaderboardManageAllocator  m_Allocator;

public:

	netLeaderboardReadMgr();
	virtual ~netLeaderboardReadMgr();

	virtual void  Init(sysMemAllocator* allocator);
	virtual void  Shutdown();
	virtual void  Update();

	//PURPOSE
	//  Returns true if there are any pending operations.
	bool  Pending() const;

	//PURPOSE
	//  Read statistics, returns true if the read is added sucessfully.
	const netLeaderboardRead*
		AddReadByRow(const int localGamerIndex
						,const unsigned id
						,const rlLeaderboard2Type type
						,const rlLeaderboard2GroupSelector& groupSelector
						,const rlClanId clanId
						,const unsigned numGamers, const rage::rlGamerHandle* gamerHandlers
						,const unsigned numClans,  const rlClanId* clanIds 
						,const unsigned numGroups, const rlLeaderboard2GroupSelector* groupSelectors);
	const netLeaderboardRead*
		AddReadByPlatform(const int localGamerIndex
						,const unsigned id
						,const rlLeaderboard2GroupSelector& groupSelector
						,const char* gamerHandler
						,const char* platform);
	const netLeaderboardRead*
		AddReadByRank(const int localGamerIndex
						,const unsigned id
						,const rlLeaderboard2Type type
						,const rlLeaderboard2GroupSelector& groupSelector
						,const rlClanId clanId
						,const unsigned maxNumRows
						,const unsigned rankStart);
	const netLeaderboardRead*
		AddReadByRadius(const int localGamerIndex
						,const unsigned id
						,const rlLeaderboard2Type type
						,const rlLeaderboard2GroupSelector& groupSelector
						,unsigned int radius
						,const rlGamerHandle& pivotGamerHandle
						,const rlClanId pivotClanId);
	const netLeaderboardRead*
		AddReadByScoreInt(const int localGamerIndex
		,const unsigned id
		,const rlLeaderboard2Type type
		,const rlLeaderboard2GroupSelector& groupSelector
		,const rlClanId clanId
		,const unsigned maxNumRows
		,const s64 pivotScore);
	const netLeaderboardRead*
		AddReadByScoreFloat(const int localGamerIndex
		,const unsigned id
		,const rlLeaderboard2Type type
		,const rlLeaderboard2GroupSelector& groupSelector
		,const rlClanId clanId
		,const unsigned maxNumRows
		,const double pivotScore);

	//PURPOSE
	//  Leaderboard read accessors - ByType, ByIndex, ByGamer, ByGroup.
	const netLeaderboardRead*  GetLeaderboardByType(const unsigned id, const rlLeaderboard2Type type, const int lbIndex) const;
	const netLeaderboardRead*  GetLeaderboardByIndex(const unsigned id, const unsigned index) const;
	const netLeaderboardReadListNode* GetHead() const {return m_Leaderboards.GetHead();}

	//PURPOSE
	//  Return true if the gamer is already present.
	bool  IsGamerPresent(const unsigned id, const rlGamerHandle& gamerHandler) const;

	//PURPOSE
	//  Games are permitted one read and one write to the leaderboards, 
	//   during a single interactive game session for each player. 
	//  If a game session exceeds five minutes in duration, the game is permitted 
	//   an additional read and/or write at each subsequent five-minute interval to the leaderboards. 
	bool  CanReadStatistics(const rlGamerHandle& gamerHandler) const;

	//PURPOSE
	//  Returns the number of leaderboards being managed with id "id".
	unsigned   CountOfLeaderboards(const unsigned id) const;
	unsigned   CountOfLeaderboards(const unsigned id, const rlLeaderboard2Type type) const;

	//PURPOSE
	//  Accessor to a convenient leaderboard name given a leaderboard id.
	virtual const char* GetIdName(const unsigned id, const bool isGroup) const;

protected:

	//PURPOSE
	//  Returns the number of leaderboards being managed.
	unsigned   CountOfLeaderboards() const;

	//PURPOSE
	//  Cancel, Delete and free all leaderboards being managed.
	void   Clear();

	//PURPOSE
	//  Cancel all leaderboards with pending operations.
	//PARAMS
	//  id   - Leaderboard Id
	void  Cancel(const unsigned id);

	//PURPOSE
	//  Cancel all leaderboards with pending operations.
	void  CancelAll();

	//PURPOSE
	//  Delete and remove a leaderboard from the list of leaderboards being managed.
	//PARAMS
	//  id   - Leaderboard Id
	bool  Delete(const unsigned id, const rlLeaderboard2Type type, const int lbIndex);
	bool  Delete(const netLeaderboardRead* pThisLb);

	//PURPOSE
	//  Delete and remove all leaderboard from the list of leaderboards being managed.
	void  DeleteAll();

	//PURPOSE
	//  Adds this read to the read node list to kept track of.
	//PARAMS
	//  lb - the leaderboard read to add to the list
	//RETURNS
	//  If the node was successfully added to the list
	bool AddReadToReadList(netLeaderboardRead* lb);
};

class netLeaderboardWriteMgr
{
private:
	//Manage used memory.
	netLeaderboardManageAllocator  m_Allocator;

	//Leaderboards submission status.
	netStatus m_Status;

	//Leaderboards being managed
	typedef atDNode< netLeaderboardWrite*, datBase > netLeaderboardWriteListNode; 
	atDList< netLeaderboardWrite*, datBase > m_Leaderboards;

public:
	netLeaderboardWriteMgr();
	virtual ~netLeaderboardWriteMgr();

	void  Init(sysMemAllocator* allocator);
	void  Shutdown();
	void  Update();

	//PURPOSE: Add leaderboard to write stats.
	netLeaderboardWrite* AddLeaderboard(const unsigned id, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId& clanId);

	//PURPOSE: Leaderboards Submission Status
	bool  InProgress()  const  { return (Started() && Pending()); }
	bool  Finished()    const  { return (netStatus::NET_STATUS_NONE != m_Status.GetStatus() && !Pending()); }
	bool  Pending()     const  { return m_Status.Pending(); }
	bool  Succeeded()   const  { return m_Status.Succeeded(); }
	bool  Failed()      const  { return m_Status.Failed(); }
	bool  Started()     const  { return (netStatus::NET_STATUS_NONE != m_Status.GetStatus()); }

protected:
	//PURPOSE: Cancels all the active writes and deletes them.
	void Clear();

	//PURPOSE: Cancel all leaderboards with pending operations.
	void Cancel();

	//PURPOSE: Delete and remove all leaderboard from the list of leaderboards being managed.
	void DeleteAll();

	//PURPOSE: Starts the submission of all the pending write operations on the given session.
	void FlushAll(const int localGamerIndex);

	//PURPOSE: Return current number of leaderboards.
	unsigned  GetLeaderboardCount() const;
};

} // namespace rage

#endif  // NET_LEADERBOARD_MGR_H