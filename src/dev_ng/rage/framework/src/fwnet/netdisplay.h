//
// netdisplay.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#ifndef INC_NETDISPLAY_H_
#define INC_NETDISPLAY_H_

#include "grcore/debugdraw.h"
#include "fwnet/netlogginginterface.h"

#include "vector/color32.h"
#include "vector/vector3.h"

namespace rage
{

//PURPOSE
// This is a network logger used to display log information on screen
class netLogDisplay : public netLoggingInterface
{
public:

	netLogDisplay(const Vector3& displayPos, const Color32& displayColor) : 
		m_displayPos(displayPos),
		m_displayColor(displayColor),
		m_displayLine(0),
		m_displayOffset(0)
	{
	}

	netLogDisplay(const Vector3& displayPos, u8 displayAlpha = 255) : 
		m_displayPos(displayPos),
		m_displayLine(0),
		m_displayOffset(0)
	{
		m_displayColor.SetAlpha(displayAlpha);
	}


#if DEBUG_DRAW
	virtual void Log(const char *logText, ...);
	virtual void WriteDataValue(const char *dataName, const char *dataValue, ...);
#else
	virtual void Log(const char *, ...) {}
	virtual void WriteDataValue(const char *, const char *, ...) {}
#endif

	const Color32& GetDisplayColor() const { return m_displayColor; }
	void ChangeDisplayColor(const Color32& color) { m_displayColor = color; }
	
	void SetDisplayOffset(unsigned offset) { m_displayOffset = offset; }

	// stubs:
	static void SetFileAccessInterface(class netLogFileAccessInterface *UNUSED_PARAM(fileAccess)) {}
	static void SetMaximumFileSize(u32 UNUSED_PARAM(maxSize)){}

	void LineBreak() {};
	void Flush(bool UNUSED_PARAM(waitForFlushToComplete) = false) {};

	void Disable() {};
	void Enable() {};
    bool IsEnabled() const {return true;}

    void WriteMessageHeader(bool UNUSED_PARAM(received), netSequence UNUSED_PARAM(sequence), const char *UNUSED_PARAM(messageString1), const char *UNUSED_PARAM(messageString2), const char *UNUSED_PARAM(playerName)) {}

    void WritePlayerText(const char *UNUSED_PARAM(type), const char *UNUSED_PARAM(buffer), const char *UNUSED_PARAM(playerName)) {}

    void WriteEventData(const char *UNUSED_PARAM(eventName), const char *UNUSED_PARAM(eventData), unsigned UNUSED_PARAM(trailingLineBreaks)) {}

    void WriteNodeHeader(const char *UNUSED_PARAM(nodeName)) {}

protected:

	Vector3		m_displayPos;		// where the text is to be displayed in world coords
	Color32		m_displayColor;		// the color of the text
	unsigned	m_displayLine;		// the current text display line
	unsigned	m_displayOffset;	// the horizontal display offset
};

} // namespace rage

#endif // INC_NETDISPLAY_H__
