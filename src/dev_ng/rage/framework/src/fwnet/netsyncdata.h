// 
// netSyncData.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_SYNC_DATA_H
#define NET_SYNC_DATA_H

//rage includes
#include "atl/array.h"
#include "data/bitbuffer.h"
#include "net/netsequence.h"
#include "system/new.h"

//framework includes
#include "fwnet/netchannel.h"
#include "fwnet/netinterface.h"
#include "fwnet/netsyncdataunit.h"
#include "fwnet/nettypes.h"

namespace rage
{
//PURPOSE
//	Holds an array of sync data units and potentially the current state and shadow buffers they operate on. 
class netSyncDataBase
{
public:
	netSyncDataBase() : m_UnsyncedPlayers(0), m_UpdatePlayers(0), m_CriticalUnsyncedPlayers(0) {}
	virtual ~netSyncDataBase() {}

	virtual datBitBuffer*					GetCurrentStateBuffer()=0;
	virtual datBitBuffer*					GetShadowBuffer()=0;
	virtual unsigned						GetMaxNumSyncedPlayers() const =0;
	virtual unsigned						GetNumSyncDataUnits() const =0;
	virtual netSyncDataUnitBase&			GetSyncDataUnit(unsigned i)=0;
	virtual const netSyncDataUnitBase&		GetSyncDataUnit(unsigned i) const =0;
	virtual unsigned						GetBufferSize() const =0;

    virtual bool                            HasSyncDataUnitEverBeenDirtied(unsigned UNUSED_PARAM(index)) const = 0;

    virtual unsigned GetTimeToSendDataUnitsAtUpdateLevel(u8 UNUSED_PARAM(updateLevel))                { return 0; }
    virtual void     SetTimeToSendDataUnitsAtUpdateLevel(u8 UNUSED_PARAM(updateLevel), unsigned UNUSED_PARAM(time)) {}
    virtual bool     IsAnyDataUnitFlaggedForForceSend(PhysicalPlayerIndex UNUSED_PARAM(player)) { return false; }
    virtual bool     IsDataUnitFlaggedForForceSend(PhysicalPlayerIndex UNUSED_PARAM(player), unsigned UNUSED_PARAM(nodeIndex)) { return false; }
    virtual void     FlagDataUnitForForceSendToAllPlayers(unsigned UNUSED_PARAM(nodeIndex)) {}
    virtual void     FlagDataUnitForForceSend     (PhysicalPlayerIndex UNUSED_PARAM(player), unsigned UNUSED_PARAM(nodeIndex)) {}
    virtual void     ClearDataUnitForForceSend    (PhysicalPlayerIndex UNUSED_PARAM(player), unsigned UNUSED_PARAM(nodeIndex)) {}

	virtual void Init()
	{
		m_UnsyncedPlayers = m_UpdatePlayers = m_CriticalUnsyncedPlayers = 0;

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			GetSyncDataUnit(i).Init();
		}
	}

	virtual void Update()
	{
		m_UnsyncedPlayers = m_UpdatePlayers = 0;

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			netSyncDataUnitBase& unit = GetSyncDataUnit(i);

			m_UnsyncedPlayers |= unit.GetUnsyncedPlayers();
			m_UpdatePlayers |= unit.GetUpdatePlayers();
		}
	}

	virtual void Reset()
	{
		m_UnsyncedPlayers = m_UpdatePlayers = m_CriticalUnsyncedPlayers = 0;

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			GetSyncDataUnit(i).Reset();
		}
	}	
	
	virtual void Shutdown() {}

	void InitialisePlayerSyncData(const unsigned playerSyncDataIndex, bool setUnsynced)
	{
		SetSyncedWithPlayer(playerSyncDataIndex);

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			InitialisePlayerSyncDataForDataUnit(i, playerSyncDataIndex, setUnsynced);

            if (setUnsynced && HasSyncDataUnitEverBeenDirtied(i))
            {
                FlagDataUnitForForceSend(static_cast<PhysicalPlayerIndex>(playerSyncDataIndex), i);
            }
            else
            {
                // clear out any old force send flags for this sync data
                ClearDataUnitForForceSend(static_cast<PhysicalPlayerIndex>(playerSyncDataIndex), i);
            }
		}
	}

	void InitialisePlayerSyncDataForDataUnit(unsigned i, const unsigned playerSyncDataIndex, bool setUnsynced)
	{
		if (setUnsynced && !HasSyncDataUnitEverBeenDirtied(i))
			setUnsynced = false;

		GetSyncDataUnit(i).InitialisePlayerSyncData(playerSyncDataIndex, setUnsynced);

		if (setUnsynced)
		{
			SetUnsyncedWithPlayer(playerSyncDataIndex);
		}
	}

	bool CompareBufferData(unsigned startPos, unsigned numBytesToCompare)
	{
		const u8 *currentData = (const u8 *)GetCurrentStateBuffer()->GetReadWriteBits();
		const u8 *shadowData  = (const u8 *)GetShadowBuffer()->GetReadWriteBits();

		return memcmp(currentData + startPos, shadowData + startPos, numBytesToCompare) != 0;
	}

	void CopyCurrentStateToShadowState(u32 startPos, u32 numBytesToCopy)
	{
		const u8 *currentData = (const u8 *)GetCurrentStateBuffer()->GetReadWriteBits();
		u8 *shadowData  = (u8*)GetShadowBuffer()->GetReadWriteBits();

		memcpy(shadowData + startPos, currentData + startPos, numBytesToCopy);
	}

	void CopyCurrentStateData(u32 startCurrentStatePos, datBitBuffer& bitBuffer, u32 startMessageBufferPos, u32 numBitsToCopy)
	{
		if(gnetVerifyf(((startMessageBufferPos + numBitsToCopy) <= static_cast<unsigned>(bitBuffer.GetMaxBits())), "Destination buffer is too small for copying current state into!"))
		{
			datBitBuffer::CopyBits(bitBuffer.GetReadWriteBits(), GetCurrentStateBuffer()->GetReadOnlyBits(), numBitsToCopy, startMessageBufferPos, startCurrentStatePos);

			int newCursorPos = bitBuffer.GetCursorPos() + numBitsToCopy;

			if(newCursorPos > bitBuffer.GetNumBitsWritten())
			{
				bitBuffer.SetNumBitsWritten(newCursorPos);
			}
			else
			{
				bitBuffer.SetCursorPos(newCursorPos);
			}
		}
	}

    virtual void SetSyncDataUnitDirty(unsigned index)
    {
        GetSyncDataUnit(index).SetDirty();
        SetUnsyncedWithAllPlayers();
    }

	virtual void ClearSyncDataUnitDirty(unsigned index)
	{
		GetSyncDataUnit(index).Init();
	}

	virtual void SetSyncDataUnitDirtyForPlayer(unsigned i, u32 playerSyncDataIndex)
	{
		GetSyncDataUnit(i).SetDirtyForPlayer(playerSyncDataIndex);

		SetUnsyncedWithPlayer(playerSyncDataIndex);
	}

	void ForceDataUnitSend(unsigned i)
	{
        FlagDataUnitForForceSendToAllPlayers(i);
		GetSyncDataUnit(i).ForceSend();
		SetSyncDataUnitDirty(i);
	}

	void ForceDataUnitSendToPlayer(unsigned i, const u32 playerSyncDataIndex)
	{
        FlagDataUnitForForceSend(static_cast<PhysicalPlayerIndex>(playerSyncDataIndex), i);
		GetSyncDataUnit(i).ForceSendToPlayer(playerSyncDataIndex);
		SetUnsyncedWithPlayer(playerSyncDataIndex);
	}

	void SetSyncedWithPlayer(const unsigned playerIndex)
	{
		m_UnsyncedPlayers &= ~(1<<playerIndex);
		m_UpdatePlayers &= ~(1<<playerIndex);
	}

	void SetUnsyncedWithPlayer(const unsigned playerIndex)
	{
		m_UnsyncedPlayers |= (1<<playerIndex);
	}

	void SetUnsyncedWithAllPlayers()
	{
		m_UnsyncedPlayers = ~0U;
	}

	void SetSyncedWithAllPlayers()
	{
		m_UnsyncedPlayers = 0;
		m_UpdatePlayers = 0;
		m_CriticalUnsyncedPlayers = 0;

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			netSyncDataUnitBase& unit = GetSyncDataUnit(i);

			unit.SetSyncedWithAllPlayers();
		}
	}

	void SetCriticalStateUnsyncedPlayers(PlayerFlags criticalUnsyncedPlayers)
	{ 
		m_CriticalUnsyncedPlayers = criticalUnsyncedPlayers; 
	}

	PlayerFlags GetCriticalStateUnsyncedPlayers() const { return m_CriticalUnsyncedPlayers; }

	bool IsCriticalStateSyncedWithPlayer(const unsigned playerIndex) const
	{ 
		bool bSynced = (m_CriticalUnsyncedPlayers & (1<<playerIndex)) == 0;
		return bSynced; 
	}

	bool IsSyncedWithPlayers(PlayerFlags playersMask) const
	{
		bool bSynced = ((m_UnsyncedPlayers & playersMask) == 0);
		return bSynced;
	}

	bool IsSyncedWithPlayer(const unsigned playerIndex) const
	{
		bool bSynced = (m_UnsyncedPlayers & (1<<playerIndex)) == 0;
		return bSynced;
	}

	PlayerFlags GetUnsyncedPlayers(PlayerFlags playersMask) const
	{
		return (m_UnsyncedPlayers & playersMask);
	}

	void SetPlayerRequiresAnUpdate(const unsigned playerIndex)
	{
#if __DEV
		ASSERT_ONLY(bool bNodesRequireAnUpdate = false);

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			if (GetSyncDataUnit(i).GetPlayerRequiresAnUpdate(playerIndex))
			{
				ASSERT_ONLY(bNodesRequireAnUpdate = true);
				break;
			}
		}
		Assertf(bNodesRequireAnUpdate, "Sync data update state differs from node update states");
#endif
		m_UpdatePlayers |= (1<<playerIndex);
	}	

    PlayerFlags GetUpdatePlayers() const { return m_UpdatePlayers; }

	bool GetPlayerRequiresAnUpdate(const unsigned playerIndex) const
	{
		return ((m_UpdatePlayers & (1<<playerIndex)) != 0);
	}

	void UpdateSentToPlayer(const unsigned playerIndex)
	{
		bool bAllNodesUpdated = true;

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			if (GetSyncDataUnit(i).GetPlayerRequiresAnUpdate(playerIndex))
			{
				bAllNodesUpdated = false;
				break;
			}
		}

		if (bAllNodesUpdated)
		{
			m_UpdatePlayers &= ~(1<<playerIndex);
		}
	}

	void SetAllDataUnitsUnsyncedWithPlayer(const unsigned playerIndex)
	{
		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			if (HasSyncDataUnitEverBeenDirtied(i))
			{
				SetSyncDataUnitDirtyForPlayer(i, playerIndex);
			}
		}
	}

	DataNodeFlags GetUnsyncedNodes(PlayerFlags playersMask) const
	{
		DataNodeFlags unsyncedNodes = 0;

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			const netSyncDataUnitBase& unit = GetSyncDataUnit(i);

			if (!unit.IsSyncedWithPlayers(playersMask))
			{
				unsyncedNodes |= (1<<i);
			}
		}

		return unsyncedNodes;
	}

#if __DEV
	void SanityCheckState();
#endif // __DEV

private:

	PlayerFlags m_UnsyncedPlayers;			// flags indicating which players are unsynced with this data
	PlayerFlags m_UpdatePlayers;			// flags indicating which players are due an update this frame
	PlayerFlags	m_CriticalUnsyncedPlayers;	// flags indicating which players are unsynced with critical data
};

//PURPOSE
// Stores when nodes should be sent updates for based on the update level, and keeps track of nodes that have been marked
// to be forcibly sent
template<unsigned maxNumSyncedPlayers, unsigned numUpdateLevels>
struct netSyncDataUnit_UpdateLevels
{
    unsigned GetTimeToSendDataUnitsAtUpdateLevel(u8 updateLevel)                { return m_TimeToSendNodes[updateLevel]; }
    void     SetTimeToSendDataUnitsAtUpdateLevel(u8 updateLevel, unsigned time) { m_TimeToSendNodes[updateLevel] = time; }
    bool     IsAnyDataUnitFlaggedForForceSend(PhysicalPlayerIndex player)       { return m_NodesForceSentToPlayer[player] != 0; }
    bool     IsDataUnitFlaggedForForceSend(PhysicalPlayerIndex player, unsigned nodeIndex) { return ((m_NodesForceSentToPlayer[player] & (1<<nodeIndex)) != 0); }
    void     FlagDataUnitForForceSend     (PhysicalPlayerIndex player, unsigned nodeIndex) { m_NodesForceSentToPlayer[player] |= (1<<nodeIndex); }
    void     ClearDataUnitForForceSend    (PhysicalPlayerIndex player, unsigned nodeIndex) { m_NodesForceSentToPlayer[player] &= ~(1<<nodeIndex);}

    void FlagDataUnitForForceSendToAllPlayers(unsigned nodeIndex)
    {
        for(PhysicalPlayerIndex player = 0; player < MAX_NUM_PHYSICAL_PLAYERS; player++)
        {
            m_NodesForceSentToPlayer[player] |= (1<<nodeIndex);
        }
    }

    void Reset()
    {
        for(unsigned index = 0; index < numUpdateLevels; index++)
        {
            m_TimeToSendNodes[index] = netInterface::GetSynchronisationTime();
        }

        for(unsigned index = 0; index < maxNumSyncedPlayers; index++)
        {
            m_NodesForceSentToPlayer[index] = 0;
        }
    }

    u32 m_TimeToSendNodes[numUpdateLevels];
    u32 m_NodesForceSentToPlayer[maxNumSyncedPlayers];
};

template<unsigned maxNumSyncedPlayers>
struct netSyncDataUnit_UpdateLevels<maxNumSyncedPlayers, 0>
{
    unsigned GetTimeToSendDataUnitsAtUpdateLevel(u8 UNUSED_PARAM(updateLevel))                { return 0; }
    void     SetTimeToSendDataUnitsAtUpdateLevel(u8 UNUSED_PARAM(updateLevel), unsigned UNUSED_PARAM(time)) {}
    bool     IsAnyDataUnitFlaggedForForceSend(PhysicalPlayerIndex UNUSED_PARAM(player))                                { return false; }
    bool     IsDataUnitFlaggedForForceSend(PhysicalPlayerIndex UNUSED_PARAM(player), unsigned UNUSED_PARAM(nodeIndex)) { return false; }
    void     FlagDataUnitForForceSendToAllPlayers(unsigned UNUSED_PARAM(nodeIndex)) {}
    void     FlagDataUnitForForceSend     (PhysicalPlayerIndex UNUSED_PARAM(player), unsigned UNUSED_PARAM(nodeIndex)) {}
    void     ClearDataUnitForForceSend    (PhysicalPlayerIndex UNUSED_PARAM(player), unsigned UNUSED_PARAM(nodeIndex)) {}
    void Reset() {}
};

//PURPOSE
//	The sync data units are statically allocated. There are no current state and shadow buffers. The sync units must be manually dirtied
// 
//	maxNumSyncedPlayers - the maximum number of players this data can be synced with
//	numSyncDataUnits	- the number of sync data units this class manages
//
template <unsigned maxNumSyncedPlayers, unsigned numSyncDataUnits, unsigned numUpdateLevels>
class netSyncData_Static_NoBuffers : public netSyncDataBase
{
private:
    static const unsigned NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS = ((numSyncDataUnits+31)/32);
public:

	datBitBuffer*					GetCurrentStateBuffer()				{ return NULL; }
	datBitBuffer*					GetShadowBuffer()					{ return NULL; }
	unsigned						GetMaxNumSyncedPlayers() const		{ return maxNumSyncedPlayers; }
	unsigned						GetNumSyncDataUnits() const			{ return numSyncDataUnits; }
	netSyncDataUnitBase&			GetSyncDataUnit(unsigned i)			{ return m_SyncDataUnits[i]; }
	const netSyncDataUnitBase&		GetSyncDataUnit(unsigned i) const	{ return m_SyncDataUnits[i]; }
	unsigned						GetBufferSize() const				{ return 0; }

    unsigned GetTimeToSendDataUnitsAtUpdateLevel(u8 updateLevel)                { return m_PlayerUpdateLevelsSyncData.GetTimeToSendDataUnitsAtUpdateLevel(updateLevel); }
    void     SetTimeToSendDataUnitsAtUpdateLevel(u8 updateLevel, unsigned time) { m_PlayerUpdateLevelsSyncData.SetTimeToSendDataUnitsAtUpdateLevel(updateLevel, time); }

    bool     IsAnyDataUnitFlaggedForForceSend(PhysicalPlayerIndex player)                  { return m_PlayerUpdateLevelsSyncData.IsAnyDataUnitFlaggedForForceSend(player); }
    bool     IsDataUnitFlaggedForForceSend(PhysicalPlayerIndex player, unsigned nodeIndex) { return m_PlayerUpdateLevelsSyncData.IsDataUnitFlaggedForForceSend(player, nodeIndex); }
    void     FlagDataUnitForForceSend     (PhysicalPlayerIndex player, unsigned nodeIndex) { m_PlayerUpdateLevelsSyncData.FlagDataUnitForForceSend(player, nodeIndex); }
    void     ClearDataUnitForForceSend    (PhysicalPlayerIndex player, unsigned nodeIndex) { m_PlayerUpdateLevelsSyncData.ClearDataUnitForForceSend(player, nodeIndex); }
    void     FlagDataUnitForForceSendToAllPlayers(unsigned nodeIndex)                      { m_PlayerUpdateLevelsSyncData.FlagDataUnitForForceSendToAllPlayers(nodeIndex); }

	void Init()
	{
		m_SyncDataUnits.Resize(numSyncDataUnits);

		netSyncDataBase::Init();

        m_PlayerUpdateLevelsSyncData.Reset();

        for(unsigned index = 0; index < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS; index++)
        {
            m_SyncDataUnitsDirty[index] = 0;
        }
	}

	void Reset()
	{
		for (unsigned i=0; i<numSyncDataUnits; i++)
		{
			m_SyncDataUnits[i].Reset();
		}

		netSyncDataBase::Reset();

        m_PlayerUpdateLevelsSyncData.Reset();

        for(unsigned index = 0; index < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS; index++)
        {
            m_SyncDataUnitsDirty[index] = 0;
        }
	}

	void Shutdown() 
	{
		m_SyncDataUnits.Reset();

		netSyncDataBase::Shutdown();

        m_PlayerUpdateLevelsSyncData.Reset();

        for(unsigned index = 0; index < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS; index++)
        {
            m_SyncDataUnitsDirty[index] = 0;
        }
	}

    bool HasSyncDataUnitEverBeenDirtied(unsigned index) const
    {
        unsigned arrayElement = index / 32;
        unsigned bitOffset    = index - (32 * arrayElement);

        gnetAssertf(arrayElement < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS, "Calculated array element is out of bounds!");
        gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

        return (m_SyncDataUnitsDirty[arrayElement] & (1<<bitOffset)) != 0;
    }

    void SetSyncDataUnitDirty(unsigned index)
    {
        unsigned arrayElement = index / 32;
        unsigned bitOffset    = index - (32 * arrayElement);

        gnetAssertf(arrayElement < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS, "Calculated array element is out of bounds!");
        gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

        m_SyncDataUnitsDirty[arrayElement] |= (1<<bitOffset);

        netSyncDataBase::SetSyncDataUnitDirty(index);

        gnetAssertf(HasSyncDataUnitEverBeenDirtied(index), "Sync data unit reported as not being dirtied immediately after being dirtied!");
    }

	void SetSyncDataUnitDirtyForPlayer(unsigned index, u32 playerSyncDataIndex)
	{
		unsigned arrayElement = index / 32;
		unsigned bitOffset    = index - (32 * arrayElement);

		gnetAssertf(arrayElement < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS, "Calculated array element is out of bounds!");
		gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

		m_SyncDataUnitsDirty[arrayElement] |= (1<<bitOffset);

		netSyncDataBase::SetSyncDataUnitDirtyForPlayer(index, playerSyncDataIndex);
	}

	void ClearSyncDataUnitDirty(unsigned index) 
	{
		unsigned arrayElement = index / 32;
		unsigned bitOffset    = index - (32 * arrayElement);

		gnetAssertf(arrayElement < NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS, "Calculated array element is out of bounds!");
		gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

		m_SyncDataUnitsDirty[arrayElement] &= ~(1<<bitOffset);

		netSyncDataBase::ClearSyncDataUnitDirty(index);
	}

protected:

    netSyncDataUnit_UpdateLevels<maxNumSyncedPlayers, numUpdateLevels> m_PlayerUpdateLevelsSyncData;
	atFixedArray<netSyncDataUnit_Static<maxNumSyncedPlayers>, numSyncDataUnits>	m_SyncDataUnits;

    u32 m_SyncDataUnitsDirty[NUM_SYNC_DATA_UNITS_DIRTY_ELEMENTS];
};

//PURPOSE
//	The sync data units are dynamically allocated. There are no current state and shadow buffers. The sync units must be manually dirtied.
class netSyncData_Dynamic_NoBuffers : public netSyncDataBase
{
public:

	//PARAMS
	//	maxNumSyncedPlayers - the maximum number of players this data can be synced with
	//	numSyncDataUnits	- the number of sync data units this class manages
	netSyncData_Dynamic_NoBuffers(unsigned maxNumSyncedPlayers, unsigned numSyncNodes) :
	m_MaxNumSyncedPlayers(static_cast<u16>(maxNumSyncedPlayers)),
	m_NumSyncDataUnits(static_cast<u16>(numSyncNodes)),
    m_NumSyncDataUnitsDirtyElements(static_cast<u16>((numSyncNodes+31)/32))
	{
	}

	datBitBuffer*					GetCurrentStateBuffer()				{ return NULL; }
	datBitBuffer*					GetShadowBuffer()					{ return NULL; }
	unsigned						GetMaxNumSyncedPlayers() const		{ return m_MaxNumSyncedPlayers; }
	unsigned						GetNumSyncDataUnits() const			{ return m_NumSyncDataUnits; }
	netSyncDataUnitBase&			GetSyncDataUnit(unsigned i)			{ return *m_SyncDataUnits[i]; }
	const netSyncDataUnitBase&		GetSyncDataUnit(unsigned i) const	{ return *m_SyncDataUnits[i]; }
	unsigned						GetBufferSize() const				{ return 0; }

	void Init()
	{
		m_SyncDataUnits.Resize(m_NumSyncDataUnits);

		for (unsigned i=0; i<m_NumSyncDataUnits; i++)
		{
			m_SyncDataUnits[i] = rage_new netSyncDataUnit_Dynamic(m_MaxNumSyncedPlayers);
		}

        m_SyncDataUnitsDirty.Resize(m_NumSyncDataUnitsDirtyElements);

        for(unsigned index = 0; index < m_NumSyncDataUnitsDirtyElements; index++)
        {
            m_SyncDataUnitsDirty[index] = 0;
        }

		netSyncDataBase::Init();
	}

	void Reset()
	{
		for (unsigned i=0; i<m_NumSyncDataUnits; i++)
		{
			m_SyncDataUnits[i]->Reset();
		}

        for(unsigned index = 0; index < m_NumSyncDataUnitsDirtyElements; index++)
        {
            m_SyncDataUnitsDirty[index] = 0;
        }

		netSyncDataBase::Reset();
	}

	void Shutdown()
	{
		for (unsigned i=0; i<m_NumSyncDataUnits; i++)
		{
			delete m_SyncDataUnits[i];
		}

        for(unsigned index = 0; index < m_NumSyncDataUnitsDirtyElements; index++)
        {
            m_SyncDataUnitsDirty[index] = 0;
        }

		m_SyncDataUnits.Reset();

		netSyncDataBase::Shutdown();
	}

    bool HasSyncDataUnitEverBeenDirtied(unsigned index) const
    {
        unsigned arrayElement = index / 32;
        unsigned bitOffset    = index - (32 * arrayElement);

        gnetAssertf(arrayElement < m_NumSyncDataUnitsDirtyElements, "Calculated array element is out of bounds!");
        gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

        return (m_SyncDataUnitsDirty[arrayElement] & (1<<bitOffset)) != 0;
    }

    void SetSyncDataUnitDirty(unsigned index)
    {
        unsigned arrayElement = index / 32;
        unsigned bitOffset    = index - (32 * arrayElement);

        gnetAssertf(arrayElement < m_NumSyncDataUnitsDirtyElements, "Calculated array element is out of bounds!");
        gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

        m_SyncDataUnitsDirty[arrayElement] |= (1<<bitOffset);

        netSyncDataBase::SetSyncDataUnitDirty(index);

        gnetAssertf(HasSyncDataUnitEverBeenDirtied(index), "Sync data unit reported as not being dirtied immediately after being dirtied!");
    }

	void SetSyncDataUnitDirtyForPlayer(unsigned index, u32 playerSyncDataIndex)
	{
		unsigned arrayElement = index / 32;
		unsigned bitOffset    = index - (32 * arrayElement);

		gnetAssertf(arrayElement < m_NumSyncDataUnitsDirtyElements, "Calculated array element is out of bounds!");
		gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

		m_SyncDataUnitsDirty[arrayElement] |= (1<<bitOffset);

		netSyncDataBase::SetSyncDataUnitDirtyForPlayer(index, playerSyncDataIndex);
	}

	void ClearSyncDataUnitDirty(unsigned index) 
	{
		unsigned arrayElement = index / 32;
		unsigned bitOffset    = index - (32 * arrayElement);

		gnetAssertf(arrayElement < m_NumSyncDataUnitsDirtyElements, "Calculated array element is out of bounds!");
		gnetAssertf(bitOffset < 32, "Calculated bit offset is out of bounds!");

		m_SyncDataUnitsDirty[arrayElement] &= ~(1<<bitOffset);
	}

protected:

	atArray<netSyncDataUnit_Dynamic*> m_SyncDataUnits;
    atArray<u32> m_SyncDataUnitsDirty;

	u16 m_MaxNumSyncedPlayers;
	u16 m_NumSyncDataUnits;
    u16 m_NumSyncDataUnitsDirtyElements;
};

//PURPOSE
//	The sync data units and current state/shadow buffers are statically allocated.
//
//	maxNumSyncedPlayers - the maximum number of players this data can be synced with
//	numSyncDataUnits	- the number of sync data units this class manages
//	bufferSizeInBytes	- the size of the current state and shadow buffers.
//
template <unsigned maxNumSyncedPlayers, unsigned numSyncDataUnits, unsigned bufferSizeInBytes, unsigned numUpdateLevels>
class netSyncData_Static : public netSyncData_Static_NoBuffers<maxNumSyncedPlayers, numSyncDataUnits, numUpdateLevels>
{
public:

    netSyncData_Static()
	{
		m_CurrentStateBuffer.SetReadWriteBits(m_CSBuffer, bufferSizeInBytes<<3, 0);
		m_ShadowBuffer.SetReadWriteBits(m_SBuffer, bufferSizeInBytes<<3, 0);
	}

    void Init()
    {
        netSyncData_Static_NoBuffers<maxNumSyncedPlayers, numSyncDataUnits, numUpdateLevels>::Init();
    }

    void Reset()
    {
        netSyncData_Static_NoBuffers<maxNumSyncedPlayers, numSyncDataUnits, numUpdateLevels>::Reset();
    }

    void Shutdown()
    {
        netSyncData_Static_NoBuffers<maxNumSyncedPlayers, numSyncDataUnits, numUpdateLevels>::Shutdown();
    }

	datBitBuffer*				GetCurrentStateBuffer()			{ return &m_CurrentStateBuffer; }
	datBitBuffer*				GetShadowBuffer()				{ return &m_ShadowBuffer; }
	unsigned					GetBufferSize() const			{ return bufferSizeInBytes; }

protected:

	datBitBuffer m_CurrentStateBuffer;                        // holds the current serialized state of the object
	datBitBuffer m_ShadowBuffer;                              // holds the previous serialized current state of the object
	u8           m_CSBuffer[bufferSizeInBytes];               // the raw buffer used by the current state buffer
	u8           m_SBuffer[bufferSizeInBytes];                // the raw buffer used by the shadow buffer
};

//PURPOSE
//	The sync data units and current state/shadow buffers are dynamically allocated.
class netSyncData_Dynamic : public netSyncData_Dynamic_NoBuffers
{
public:

	//PARAMS
	//	maxNumSyncedPlayers - the maximum number of players this data can be synced with
	//	bufferSizeInBytes	- the size of the current state and shadow buffers.
	//	numSyncDataUnits	- the number of sync data units this class manages
	netSyncData_Dynamic(unsigned maxNumSyncedPlayers, unsigned numSyncDataUnits, unsigned bufferSizeInBytes) :
		netSyncData_Dynamic_NoBuffers(maxNumSyncedPlayers, numSyncDataUnits),
		m_bufferSizeInBytes(bufferSizeInBytes),
		m_csBuffer(NULL),
		m_sBuffer(NULL)
	{
		gnetAssert(bufferSizeInBytes > 0);
	}

	void Init()
	{
		if (gnetVerify(m_csBuffer == NULL) && gnetVerify(m_sBuffer == NULL))
		{
			m_csBuffer	= rage_new u8[m_bufferSizeInBytes];
			m_sBuffer	= rage_new u8[m_bufferSizeInBytes];

			m_CurrentStateBuffer.SetReadWriteBits(m_csBuffer, (m_bufferSizeInBytes<<3), 0);
			m_ShadowBuffer.SetReadWriteBits(m_sBuffer, (m_bufferSizeInBytes<<3), 0);

			netSyncData_Dynamic_NoBuffers::Init();
		}
	}

	void Shutdown()
	{
		if (m_csBuffer)
		{
			delete[] m_csBuffer;
			m_csBuffer = NULL;
		}

		if (m_sBuffer)
		{
			delete[] m_sBuffer;
			m_sBuffer = NULL;
		}

		netSyncData_Dynamic_NoBuffers::Shutdown();
	}

	datBitBuffer*				GetCurrentStateBuffer()			{ return &m_CurrentStateBuffer; }
	datBitBuffer*				GetShadowBuffer()				{ return &m_ShadowBuffer; }
	unsigned					GetBufferSize() const			{ return m_bufferSizeInBytes; }

protected:

	datBitBuffer					m_CurrentStateBuffer;				// holds the current serialized state of the object
	datBitBuffer					m_ShadowBuffer;						// holds the previous serialized current state of the object
	u8*								m_csBuffer;							// the raw buffer used by the current state buffer
	u8*								m_sBuffer;							// the raw buffer used by the shadow buffer
	u32								m_bufferSizeInBytes;				// the size of m_csBuffer & m_sBuffer arrays
};

} // namespace rage

#endif  // NETWORK_SYNC_DATA_H
