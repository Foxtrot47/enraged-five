//===========================================================================
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.             
//===========================================================================

#ifndef CLOUDFILEDOWNLOADHELPER_H
#define CLOUDFILEDOWNLOADHELPER_H

#include "atl/string.h"
#include "data/callback.h"
#include "data/growbuffer.h"
#include "net/http.h"
#include "net/status.h"
#include "rline/cloud/rlcloud.h"

//
// Wrapper around an rlCloud file to make it a little easier to cloud enable a system
//
//	Example callback:
//
//
//	Usage for:
//		
//		netCloudRequestGetMemberFile membercloudFile;
//		netCloudRequestGetTitleFile titlecloudFile;
//
//		void Init()
//		{
//			const char* membercloudpath = "rage/data/somefile.txt";
//			int estimatedFileSize = 1024;
//			cloudFile.StartRequest(localGamerIndex, rlCloud::INVALID_MEMBER_ID, membercloudpath, estimatedFileSize, datCallback(CFA1(HandleFileReceivedCallback)));
//
//			const char* titlecloudpath = "gta5/datafile.txt";
//			titlecloudFile.StartRequest(titlecloudpath, estimatedFileSize, datCallback(CFA1(HandleFileReceivedCallback)));			
//		}
//
//		void Update()
//		{
//			cloudFile.Update();
//		}
//
//		void Shutdown()
//		{
//			cloudFile.shutdown();
//		}
//
//		void HandleFileReceivedCallback(netCloudFileDownloadHelper* cloudFile)
//		{
//			if (!cloudFile.GetNetStatus().Pending())
//			{
//				if(cloudFile.GetNetStatus().Succeeded())
//				{
//					//File successfully downloaded
//					datGrowBuffer& gbuf = cloudFile->GetGrowBuffer();
//				}
//				else
//				{
//		 			//File failed to download
//				}
//			}	
//		}
//		

namespace rage
{

enum eOpType
{
	eOp_GET,
	eOp_POST,
	eOp_DELETE,
};

class netCloudRequestMemPolicy
{
public:
	netCloudRequestMemPolicy() 
		: m_pHttpAllocator(nullptr)
		, m_pGrowBufferAllocator(nullptr)
		, m_pGrowBufferBackup(nullptr)
		, m_GrowBufferGrowIncrement(datGrowBuffer::DEFAULT_GROW_INCR)
	{

	}

	netCloudRequestMemPolicy(sysMemAllocator* pHttpAllocator, 
		sysMemAllocator* pGrowBufferAllocator, 
		sysMemAllocator* pGrowBufferBackup,
		unsigned growBufferGrowIncrement = datGrowBuffer::DEFAULT_GROW_INCR) 
		: m_pHttpAllocator(pHttpAllocator)
		, m_pGrowBufferAllocator(pGrowBufferAllocator)
		, m_pGrowBufferBackup(pGrowBufferBackup)
		, m_GrowBufferGrowIncrement(growBufferGrowIncrement)
	{
	}

	sysMemAllocator* m_pHttpAllocator;
	sysMemAllocator* m_pGrowBufferAllocator;
	sysMemAllocator* m_pGrowBufferBackup;
	unsigned		 m_GrowBufferGrowIncrement;

	static netCloudRequestMemPolicy NULL_POLICY;
};

class netCloudRequestHelper
{
public:

	netCloudRequestHelper() 
		: m_State(kState_Idle)
		, m_ModifiedTimestamp(0)
		, m_HttpOptions(NET_HTTP_OPTIONS_NONE)
		, m_MaxDownloadRetries(0)
		, m_NumDownloadRetries(0)
		, m_RetryOnFail(false)
        , m_SecurityFlags(RLROS_SECURITY_DEFAULT)
		, m_MaxPrepareMemoryRetries(PREPARE_ATTEMPTS)
		, m_NumPrepareMemoryRetries(0)
		, m_PrepareMemoryRetryTimestamp(0)
		, m_RequestId(0)
	{ }

	netCloudRequestHelper(const char* cloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, datCallback doneCallback, netCloudRequestMemPolicy& rMemPolicy);
	virtual ~netCloudRequestHelper();

	void Update();
	void Start();
	void Reset();
	void Cancel();

	bool IsIdle() const { return m_State == kState_Idle; }
	bool IsWaiting() const { return m_State == kState_Waiting; }
	bool IsProcessing() const { return m_State == kState_Processing; }
	bool HasFinished() const { return m_State == kState_Finish; }
	bool DidSucceed() const { return m_CloudFileStatus.Succeeded(); }
	bool WasCancelled() const { return m_CloudFileStatus.Canceled(); }
	int GetResultCode() const { return m_CloudFileStatus.GetResultCode(); }

	void FlagRetry(); 
	void SetNumRetries(unsigned nRetries);
	void MarkAsCritical(); 

	void UpdateTimestamp(u64 uModifiedTimestamp) { m_ModifiedTimestamp = uModifiedTimestamp; }
	u64 GetTimestamp() const{ return m_ModifiedTimestamp; }

    void SetSecurityFlags(const rlRosSecurityFlags flags) { m_SecurityFlags = flags; }

	rlCloudFileInfo* GetFileInfo();

	const char* GetPath() const { return m_CloudFilePath.c_str(); }
    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const = 0;
    virtual eOpType GetOpType() const = 0;
    virtual const char* GetRequestName() const { return "Base"; };

protected:

	void Init(const char* cloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, datCallback doneCallback, netCloudRequestMemPolicy& rMemPolicy);
	bool AreROSCredentialsValid(const int localGamerIndex = -1) const;

	virtual bool CanStartRequest() const = 0;
	virtual bool PrepareMemory() = 0;
	virtual bool DoRequest() = 0;
	virtual bool HasFileInfo() const = 0;
	virtual void CleanUpRequest() { }

	static const int PREPARE_WAIT_TIME = 250;
	static const int PREPARE_ATTEMPTS = 8;
	static const int PREPARE_ATTEMPTS_CRITICAL_FILE = 40;

	netStatus m_CloudFileStatus;
	atString m_CloudFilePath;
	u64 m_ModifiedTimestamp;
	netHttpOptions m_HttpOptions;
	rlCloudFileInfo m_CloudFileInfo;
	netCloudRequestMemPolicy m_MemPolicy;

	int m_RequestId;

	bool m_RetryOnFail;
	unsigned m_MaxDownloadRetries;
	unsigned m_NumDownloadRetries;

	bool m_FailedDueToOutOfMemory;
	unsigned m_MaxPrepareMemoryRetries; 
	unsigned m_NumPrepareMemoryRetries; 
	unsigned m_PrepareMemoryRetryTimestamp; 

    rlRosSecurityFlags m_SecurityFlags;

private:
	
	enum State
	{
		kState_Idle,			// no actions
		kState_Waiting,			// start signaled, pending 
		kState_Start,			// kick off request, intermediate state
		kState_PrepareMemoryRetryWait,	// retry starting the request
		kState_Processing,		// request processing, waiting for result
		kState_Finish,			// received content
	} m_State;

#if !__NO_OUTPUT
	unsigned m_RequestStartedTimestamp; 
#endif

	datCallback m_cbFinished;
};

enum NullAllocatorPolicy
{
	Policy_UseNull,
	Policy_UseRline,
	Policy_FailRequest,
	Policy_Num,
};

class netCloudRequestGetFile : public netCloudRequestHelper
{
public:

	netCloudRequestGetFile() : m_MemoryPresize(0) { }
	netCloudRequestGetFile(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	virtual ~netCloudRequestGetFile() 
	{
		CleanUpRequest();
	}

	datGrowBuffer& GetGrowBuffer() { return m_GB; }
	eOpType GetOpType() const { return eOp_GET; }
	u32 GetPresize() const { return m_MemoryPresize; }

	// tunables
	static void SetNullAllocatorPolicy(const NullAllocatorPolicy policy);

protected:

	virtual bool PrepareMemory();
	virtual bool TryAllocate(sysMemAllocator* allocator OUTPUT_ONLY(, const char* szAllocatorName));

	virtual void CleanUpRequest();
	virtual bool HasFileInfo() const { return true; }
	virtual bool DoRequest();
	virtual bool IsMemoryPrepared() const { return m_GB.GetCapacity() > 0; }

	u32 m_MemoryPresize;
	datGrowBuffer m_GB;

	static bool sm_UseRlineForNullAllocator;
	static NullAllocatorPolicy sm_NullAllocatorPolicy;
};

class netCloudRequestGetMemberFile : public netCloudRequestGetFile
{
public:

	netCloudRequestGetMemberFile() : m_LocalGamerIndex(-1), m_Service(RL_CLOUD_ONLINE_SERVICE_INVALID) { } 
	netCloudRequestGetMemberFile(const int localGamerIndex, rlCloudMemberId cloudMemberId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const int localGamerIndex, rlCloudMemberId cloudMemberId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:

	virtual const char* GetRequestName() const { return "MemberGet"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(m_LocalGamerIndex); }
	virtual bool DoRequest();

private:

	int m_LocalGamerIndex;
	rlCloudMemberId m_CloudMemberId;
	rlCloudOnlineService m_Service;
};

class netCloudRequestPostMemberFile : public netCloudRequestHelper
{
public:

	netCloudRequestPostMemberFile() : m_LocalGamerIndex(-1), m_Service(RL_CLOUD_ONLINE_SERVICE_INVALID), m_pData(nullptr), m_uSizeOfData(0) { }
	netCloudRequestPostMemberFile(const int localGamerIndex, const char* szCloudFilePath, const void* pData, const unsigned uSizeOfData, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const int localGamerIndex, const char* szCloudFilePath, const void* pData, const unsigned uSizeOfData, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
    virtual const char* GetAbsPath(char*, unsigned) const { return nullptr; }

	eOpType GetOpType() const { return eOp_POST; }

protected:

	virtual const char* GetRequestName() const { return "MemberPost"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(m_LocalGamerIndex); }
	virtual bool DoRequest();
	virtual bool PrepareMemory() { return true; } //No memory to prepare
	virtual bool HasFileInfo() const { return true; }

private:

	int m_LocalGamerIndex;
	rlCloudOnlineService m_Service;
	const void* m_pData;
	unsigned m_uSizeOfData;
};

class netCloudRequestDeleteMemberFile : public netCloudRequestHelper
{
public:

	netCloudRequestDeleteMemberFile() : m_LocalGamerIndex(-1), m_Service(RL_CLOUD_ONLINE_SERVICE_INVALID) { }
	netCloudRequestDeleteMemberFile(const int localGamerIndex, const char* szCloudFilePath, rlCloudOnlineService svc, datCallback cbFinished);
	void Init(const int localGamerIndex, const char* szCloudFilePath, rlCloudOnlineService svc, datCallback cbFinished);
    virtual const char* GetAbsPath(char*, unsigned) const { return nullptr; }

	eOpType GetOpType() const { return eOp_DELETE; }

protected:

	virtual const char* GetRequestName() const { return "MemberDelete"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(m_LocalGamerIndex); }
	virtual bool DoRequest();
	virtual bool PrepareMemory() { return true; } //No memory to prepare
	virtual bool HasFileInfo() const { return false; }

private:

	int m_LocalGamerIndex;
	rlCloudOnlineService m_Service;
};


class netCloudRequestGetCrewFile : public netCloudRequestGetFile
{
public:

	netCloudRequestGetCrewFile() : m_LocalGamerIndex(-1), m_Service(RL_CLOUD_ONLINE_SERVICE_INVALID) { }
	netCloudRequestGetCrewFile(const int localGamerIndex, rlCloudMemberId targetCrewId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const int localGamerIndex, rlCloudMemberId targetCrewId, const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, rlCloudOnlineService svc, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:

	virtual const char* GetRequestName() const { return "CrewGret"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(m_LocalGamerIndex); }
	virtual bool DoRequest();

private:

	int m_LocalGamerIndex;
	rlCloudMemberId m_TargetCrewId;
	rlCloudOnlineService m_Service;
};

class netCloudRequestGetUgcFile : public netCloudRequestGetFile
{
public:
    netCloudRequestGetUgcFile() : m_LocalGamerIndex(-1) { } 

    netCloudRequestGetUgcFile(const int localGamerIndex, 
                              const rlUgcContentType contentType,
                              const char* contentId,
                              const int fileId, 
                              const int fileVersion,
                              const rlScLanguage language,
                              u64 uModifiedTimestamp, 
                              netHttpOptions httpOptions,
                              u32 uPresize, 
                              datCallback cbFinished, 
                              netCloudRequestMemPolicy& rMemPolicy);

	void Init(const int localGamerIndex, 
              const rlUgcContentType contentType,
              const char* contentId,
              const int fileId, 
              const int fileVersion,
              const rlScLanguage language,
              u64 uModifiedTimestamp, 
              netHttpOptions httpOptions,
              u32 uPresize, 
              datCallback cbFinished, 
              netCloudRequestMemPolicy& rMemPolicy);

    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:
	virtual const char* GetRequestName() const { return "UgcGet"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(m_LocalGamerIndex); }
	virtual bool DoRequest();

private:
    int m_LocalGamerIndex;
    rlUgcContentType m_ContentType;
    char m_ContentId[RLUGC_MAX_CONTENTID_CHARS];
    int m_FileId;
    int m_FileVersion;
    rlScLanguage m_Language;
};


class netCloudRequestGetUgcCdnFile : public netCloudRequestGetFile
{
public:
    netCloudRequestGetUgcCdnFile() : m_LocalGamerIndex(RL_INVALID_GAMER_INDEX) { }

    netCloudRequestGetUgcCdnFile(const int localGamerIndex,
        const char* contentName,
        const rlUgcTokenValues& tokenValues,
        const rlUgcUrlTemplate* urlTemplate,
        u64 uModifiedTime,
        netHttpOptions httpOptions,
        u32 uPresize,
        datCallback cbFinished,
        netCloudRequestMemPolicy& rMemPolicy);

    void Init(const int localGamerIndex,
        const char* contentName,
        const rlUgcTokenValues& tokenValues,
        const rlUgcUrlTemplate* urlTemplate,
        u64 uModifiedTime,
        netHttpOptions httpOptions,
        u32 uPresize,
        datCallback cbFinished,
        netCloudRequestMemPolicy& rMemPolicy);

    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:
    virtual const char* GetRequestName() const { return "GetUgcCdn"; }
    virtual bool CanStartRequest() const { return AreROSCredentialsValid(m_LocalGamerIndex); }
    virtual bool DoRequest();

private:
    int m_LocalGamerIndex;
    atString m_ContentName;
    rlUgcTokenValues m_TokenValues;
    const rlUgcUrlTemplate* m_UrlTemplate;
};

class netCloudRequestGetTitleFile : public netCloudRequestGetFile
{
public:

	netCloudRequestGetTitleFile() {}
	netCloudRequestGetTitleFile(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const char* szCloudFilePath, u64 uModifiedTimestamp, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:

	virtual const char* GetRequestName() const { return "TitleGet"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(); }
	virtual bool DoRequest();
};

class netCloudRequestGetGlobalFile : public netCloudRequestGetFile
{
public:

	netCloudRequestGetGlobalFile() {}
	netCloudRequestGetGlobalFile(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy = netCloudRequestMemPolicy::NULL_POLICY);
    virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:

	virtual const char* GetRequestName() const { return "GlobalGet"; } 
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(); }
	virtual bool DoRequest();
};

class netCloudRequestGetWWWFile : public netCloudRequestGetFile
{
public:

	netCloudRequestGetWWWFile() {}
	netCloudRequestGetWWWFile(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy);
	void Init(const char* szCloudFilePath, u64 uModifiedTime, netHttpOptions httpOptions, u32 uPresize, datCallback cbFinished, netCloudRequestMemPolicy& rMemPolicy = netCloudRequestMemPolicy::NULL_POLICY);
	virtual const char* GetAbsPath(char* szAbsolutePath, unsigned nMaxLength) const;

protected:

	virtual const char* GetRequestName() const { return "GetWWW"; }
	virtual bool CanStartRequest() const { return AreROSCredentialsValid(); }
	virtual bool DoRequest();
};

} // namespace rage

#endif // CLOUDFILEDOWNLOADHELPER_H
