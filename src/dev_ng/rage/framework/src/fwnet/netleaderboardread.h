// 
// netleaderboardread.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

#ifndef NET_LEADERBOARD_READ_H
#define NET_LEADERBOARD_READ_H

// rage headers
#include "rline/clan/rlclancommon.h"
#include "net/status.h"
#include "atl/array.h"
#include "system/memory.h"

// framework headers
#include "fwnet/netleaderboardcommon.h"
#include "fwnet/netchannel.h"
#include "fwtl/regdrefs.h"

namespace rage
{

class rlStatValue;
class rlGamerHandle;

class rlLeaderboard2Row;
class rlLeaderboardColumnInfo;
class rlLeaderboard2GroupHandle;
class rlLeaderboard2GroupSelector;

//PURPOSE
//  Struct that represents the leaderboard Info
struct  netLeaderboardInfo
{
public:
	unsigned                  m_Id;
	unsigned                  m_NumColumnIds;
	rlLeaderboardColumnInfo*  m_ColumnIds;
	rlLeaderboard2Type        m_ReadType;

	netLeaderboardInfo() : m_Id(0), m_NumColumnIds(0), m_ColumnIds(0), m_ReadType(RL_LEADERBOARD2_TYPE_INVALID) {;}

	int GetColumnIndex(const unsigned columnId) const;
};


//PURPOSE
//  Base class for reading one leaderboard.
class netLeaderboardRead : public fwRefAwareBase
{
protected:

	//Default read interval for leaderboards is 5 minutes.
	static const unsigned READ_INTERVAL = 5 * 60000;

protected:
	//Leaderboard Info.
	netLeaderboardInfo  m_Info;

	//Destination rows.
	rage::rlLeaderboard2Row*    m_RowHeap;
	rage::rlLeaderboard2Row**   m_RowPtrs;
    rage::rlStatValue*          m_StatValueHeap;


	//Pool status from read operations.
	netStatus  m_MyStatus;

	//Upon completion will contain the number of rows returned.
	unsigned  m_NumRowsReturned;

	//Upon completion will contain the total number of rows in the leaderboard.
	unsigned  m_NumRowsInLeaderboard;

	//Control TCR136 read times
	unsigned  m_LastReadTime;

	//Allocator to use for allocating rows.
	sysMemAllocator* m_Allocator;

public:
	netLeaderboardRead(const unsigned id, const rlLeaderboard2Type type);
	virtual ~netLeaderboardRead();

	void  Init(const unsigned id, const rlLeaderboard2Type type);
	void  Cancel();
	void  Shutdown();

	//PURPOSE
	//  Returns the value of the column with id columnId for the gamer with handler gamerHandle.
	bool GetData(const unsigned columnId, const rlGamerHandle& gamerHandle, rlStatValue* statValue) const;
	bool GetGroupData(const unsigned columnId, rlStatValue* statValue) const;

	bool  Idle()     const { return (netStatus::NET_STATUS_NONE == m_MyStatus.GetStatus()); }
	bool  Pending()  const { return (m_MyStatus.Pending());    }
	bool  Suceeded() const { return (m_MyStatus.Succeeded());  }
	bool  Failed()   const { return (m_MyStatus.Failed());     }
	bool  Finished() const { return (!Idle() && !Pending());   }
	int GetResultCode() const { return m_MyStatus.GetResultCode(); }

	//PURPOSE:  Accessor to leaderboard rows after a successful read
	const rage::rlLeaderboard2Row* GetRow(const unsigned index) const;

	//PURPOSE: Returns true if a gamer is present in the results.
	bool IsGamerPresent(const rlGamerHandle& gamerHandle) const;

	//PURPOSE:  Return the number of Rows.
	unsigned  GetNumRows() const { return m_NumRowsReturned; }

	//PURPOSE:  Return the number of Total Rows.
	unsigned  GetNumTotalRows() const { return m_NumRowsInLeaderboard; }

	//PURPOSE:  Return the number of Columns.
	unsigned  GetNumColumns() const { return m_Info.m_NumColumnIds; }

	//PURPOSE:  Return the leaderboard id.
	unsigned  GetId() const { return m_Info.m_Id; }

	//PURPOSE:  Return the leaderboard read type.
	rlLeaderboard2Type GetReadType() const { return m_Info.m_ReadType; }
	const char* GetReadTypeName() const;

	//PURPOSE:  Accessor to leaderboard columns.
	const rlLeaderboardColumnInfo* GetColumnsIds() const
	{
		if (AssertVerify(m_Info.m_ColumnIds))
		{
			return m_Info.m_ColumnIds;
		}

		return NULL;
	}

	//PURPOSE:  Accessor to leaderboard columns.
	const unsigned GetColumnId(const unsigned index) const;
	const int GetColumnType(const unsigned index) const;
	const char* GetColumnTypeName(const unsigned index) const;

	//PURPOSE
	//  Reset the read interval.
	void  ResetReadInterval() { m_LastReadTime = 0; }

	//PURPOSE
	//  Returns true if we can start a read.
	bool  CanReadStatistics() const;

	//PURPOSE
	//  Print all statistics.
	void  PrintStatistics() const;

	void  SetAllocator( sysMemAllocator* pAllocator );

protected:
	//PURPOSE:  Create the leaderboard for reading and returns true on success.
	virtual bool CreateConcreteLeaderboard();

    bool AllocateRowData(const unsigned numRows, const unsigned numColumns);

    void FreeRowData();

	//Manage memory
	template<class _Type> _Type* Allocate(int size) const
	{
		gnetAssert(m_Allocator);
		if (m_Allocator && CanAllocate < _Type >(size))
		{
			gnetDebug1("[net_leaderboard_read] - Allocate size=%" SIZETFMT "d", size*sizeof(_Type));
			_Type* types = (_Type*)m_Allocator->RAGE_LOG_ALLOCATE(size*sizeof(_Type), 0);
			gnetAssert(types);

			if (types)
			{
				for(int i=0; i < size; i++)
				{
					rage_placement_new(types+i) _Type;
				}
				return types;
			}
		}

		return NULL;
	}

	//Manage memory
	template<class _Type> bool CanAllocate(int size) const
	{
		gnetAssert(m_Allocator);

		bool canAllocate = false;

		if (m_Allocator)
		{
			gnetAssertf(m_Allocator->GetLargestAvailableBlock() > size*sizeof(_Type), "[net_leaderboard_read] - Not enough memory available(%" SIZETFMT "d) for %" SIZETFMT "d", m_Allocator->GetMemoryAvailable(), size*sizeof(_Type));
			canAllocate = (m_Allocator->GetLargestAvailableBlock() > size*sizeof(_Type));
		}

		return canAllocate;
	}

	template<class _Type> void Free(_Type* mem) const
	{
		Assert(m_Allocator);
		if(m_Allocator && mem)
		{
			m_Allocator->Free(mem);
		}
	}
};

//////////////////////////////////////////////////////////////////////////
// ROW

//PURPOSE: Reads Gamers by row.
class netLeaderboardReadGamersByRow : public netLeaderboardRead
{
public:
	netLeaderboardReadGamersByRow(const unsigned id, const rlLeaderboard2Type type) : netLeaderboardRead(id, type)
	{;}

	//PURPOSE: Start statistics reading process.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned numGamers, const rage::rlGamerHandle* gamerHandles);
};

//PURPOSE: Reads Gamers by row.
class netLeaderboardReadByGamerByPlatformTask : public netLeaderboardRead
{
public:
	netLeaderboardReadByGamerByPlatformTask(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_PLAYER)
	{;}

	//PURPOSE: Start statistics reading process.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const char* gamerHandle, const char* platform);
};


//PURPOSE: Reads Gamers by row.
class netLeaderboardReadClansByRow : public netLeaderboardRead
{
public:
	netLeaderboardReadClansByRow(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN)
	{;}

	//PURPOSE: Start statistics reading process.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId* clanIds, const unsigned numClans);
};

//PURPOSE: Reads Gamers by row.
class netLeaderboardReadClanMembersByRow : public netLeaderboardRead
{
public:
	netLeaderboardReadClanMembersByRow(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
	{;}

	//PURPOSE: Start statistics reading process.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned numGamers, const rage::rlGamerHandle* gamerHandles);
};

//PURPOSE: Reads Groups by row.
class netLeaderboardReadGroupsByRow : public netLeaderboardRead
{
public:
	netLeaderboardReadGroupsByRow(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_GROUP)
	{;}

	//PURPOSE: Start statistics reading process.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector* groupSelectors, const unsigned numGroups);
};

//PURPOSE
//  Reads Gamers in Groups by row.
class netLeaderboardReadGroupMembersByRow : public netLeaderboardReadGamersByRow
{
public:
	netLeaderboardReadGroupMembersByRow(const unsigned id) : netLeaderboardReadGamersByRow(id, RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
	{;}
};

//////////////////////////////////////////////////////////////////////////
//  RANK

//PURPOSE:  Reads Gamers by rank.
class netLeaderboardReadGamersByRank : public netLeaderboardRead
{
public:
	netLeaderboardReadGamersByRank(const unsigned id, const rlLeaderboard2Type type) : netLeaderboardRead(id, type)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const unsigned rankStart);
};

//PURPOSE:  Reads Clans by rank.
class netLeaderboardReadClansByRank : public netLeaderboardRead
{
public:
	netLeaderboardReadClansByRank(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const unsigned rankStart);
};

//PURPOSE:  Reads Clans Membersby rank.
class netLeaderboardReadClanMembersByRank : public netLeaderboardRead
{
public:
	netLeaderboardReadClanMembersByRank(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned maxNumRows, const unsigned rankStart);
};

//PURPOSE
//  Reads Groups by rank.
class netLeaderboardReadGroupsByRank : public netLeaderboardRead
{
public:
	netLeaderboardReadGroupsByRank(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_GROUP)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const unsigned rankStart);
};

//PURPOSE
//  Reads Gamers in Groups by rank.
class netLeaderboardReadGamersInGroupsByRank : public netLeaderboardReadGamersByRank
{
public:
	netLeaderboardReadGamersInGroupsByRank(const unsigned id) : netLeaderboardReadGamersByRank(id, RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
	{;}
};


//////////////////////////////////////////////////////////////////////////
//  RADIUS

//PURPOSE: Reads Gamer by radius.
class netLeaderboardReadGamersByRadius : public netLeaderboardRead
{
public:
	netLeaderboardReadGamersByRadius(const unsigned id, const rlLeaderboard2Type type) : netLeaderboardRead(id, type)
	{;}

	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, unsigned int radius, const rlGamerHandle& pivotGamer);
};

//PURPOSE: Reads Clans by radius.
class netLeaderboardReadClansByRadius : public netLeaderboardRead
{
public:
	netLeaderboardReadClansByRadius(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN)
	{;}

	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, unsigned int radius, const rlClanId pivotClanId);
};

//PURPOSE: Reads Clan Members by radius.
class netLeaderboardReadClanMembersByRadius : public netLeaderboardRead
{
public:
	netLeaderboardReadClanMembersByRadius(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
	{;}

	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, unsigned int radius, const rlGamerHandle& pivotGamer, const rlClanId clanId);
};

//PURPOSE: Reads Group by radius.
class netLeaderboardReadGroupsByRadius : public netLeaderboardRead
{
public:
	netLeaderboardReadGroupsByRadius(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_GROUP)
	{;}

	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& pivotGroup, unsigned int radius);
};

//PURPOSE: Reads Group by radius.
class netLeaderboardReadGroupMembersByRadius : public netLeaderboardReadGamersByRadius
{
public:
	netLeaderboardReadGroupMembersByRadius(const unsigned id) : netLeaderboardReadGamersByRadius(id, RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
	{;}
};


//////////////////////////////////////////////////////////////////////////
//  BY SCORE INT

//PURPOSE:  Reads Gamers by rank.
class netLeaderboardReadGamersByScoreInt : public netLeaderboardRead
{
public:
	netLeaderboardReadGamersByScoreInt(const unsigned id, const rlLeaderboard2Type type) : netLeaderboardRead(id, type)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const s64 pivotScore);
};

//PURPOSE:  Reads Clans by rank.
class netLeaderboardReadClansByScoreInt : public netLeaderboardRead
{
public:
	netLeaderboardReadClansByScoreInt(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const s64 pivotScore);
};

//PURPOSE:  Reads Clans Membersby rank.
class netLeaderboardReadClanMembersByScoreInt : public netLeaderboardRead
{
public:
	netLeaderboardReadClanMembersByScoreInt(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned maxNumRows, const s64 pivotScore);
};

//PURPOSE
//  Reads Gamers in Groups by rank.
class netLeaderboardReadGamersInGroupsByScoreInt : public netLeaderboardReadGamersByScoreInt
{
public:
	netLeaderboardReadGamersInGroupsByScoreInt(const unsigned id) : netLeaderboardReadGamersByScoreInt(id, RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
	{;}
};

//////////////////////////////////////////////////////////////////////////
//  BY SCORE FLOAT

//PURPOSE:  Reads Gamers by rank.
class netLeaderboardReadGamersByScoreFloat : public netLeaderboardRead
{
public:
	netLeaderboardReadGamersByScoreFloat(const unsigned id, const rlLeaderboard2Type type) : netLeaderboardRead(id, type)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const double pivotScore);
};

//PURPOSE:  Reads Clans by rank.
class netLeaderboardReadClansByScoreFloat : public netLeaderboardRead
{
public:
	netLeaderboardReadClansByScoreFloat(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const double pivotScore);
};

//PURPOSE:  Reads Clans Membersby rank.
class netLeaderboardReadClanMembersByScoreFloat : public netLeaderboardRead
{
public:
	netLeaderboardReadClanMembersByScoreFloat(const unsigned id) : netLeaderboardRead(id, RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
	{;}

	//PURPOSE: Start statistics reading process. Rank Must be >= 1.
	bool Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned maxNumRows, const double pivotScore);
};

//PURPOSE
//  Reads Gamers in Groups by rank.
class netLeaderboardReadGamersInGroupsByScoreFloat : public netLeaderboardReadGamersByScoreFloat
{
public:
	netLeaderboardReadGamersInGroupsByScoreFloat(const unsigned id) : netLeaderboardReadGamersByScoreFloat(id, RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
	{;}
};


} // namespace rage

#endif  // NET_LEADERBOARD_READ_H

//eof

