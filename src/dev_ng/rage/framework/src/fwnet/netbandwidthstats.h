// 
// netBandwidthStatistics.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETWORK_BANDWIDTH_STATISTICS_H
#define NETWORK_BANDWIDTH_STATISTICS_H

#if __BANK

#include "fwnet/netbandwidthrecorder.h"
#include "fwnet/nettypes.h"

#include "atl/array.h"
#include "net/netsocket.h"

namespace rage
{

//PURPOSE
//	Manages a collection of bandwidth recorders
class netBandwidthStatistics
{
	friend class netBandwidthMgr;

public:

	static const unsigned MAX_RECORDERS = 180;				// the maximum number of recorders held in these statistics
	static const unsigned MAX_TOTAL_RECORDERS = 320;		// the total number of recorders permitted for all statistics

public:

	netBandwidthStatistics();
	virtual ~netBandwidthStatistics() {}

	const char* GetName() const { return m_Name; }

	void Init();
	void Update();

    //PURPOSE
    // Enable bandwidth recording for sync nodes
    void EnableBandwidthRecording() { m_DisableBandwidthRecording = false; }

    //PURPOSE
    // Disable bandwidth recording for sync nodes
    void DisableBandwidthRecording() { m_DisableBandwidthRecording = true; }

	//PURPOSE
	// Assigns a free bandwidth recorder and returns the id of the recorder
	RecorderId AllocateBandwidthRecorder(const char* pRecorderName);

	// Returns the id of the bandwidth recorder with the specified recorder name
	RecorderId GetBandwidthRecorder(const char* pRecorderName) const;

	//PURPOSE
    // Returns the bandwidth recorder with the given id
	const netBandwidthRecorderWrapper* GetBandwidthRecorder(RecorderId id) const;

	//PURPOSE
	// Adds bandwidth in to the recorder with the given id 
	void AddBandwidthIn(RecorderId recorderId, unsigned bandwidthIn, bool addToTotals = true);

	//PURPOSE
	// Adds bandwidth out to the recorder with the given id 
	void AddBandwidthOut(RecorderId recorderId, unsigned bandwidthOut, bool addToTotals = true);

    //PURPOSE
	// Returns the bandwidth in rate for the specified recorder
	float GetBandwidthInRate(RecorderId recorderId) const;

	//PURPOSE
	// Returns the bandwidth out rate for the specified recorder
	float GetBandwidthOutRate(RecorderId recorderId) const;

	//PURPOSE
	// Returns the total bandwidth in rate for these statistics
	float GetBandwidthInRate() const;

	//PURPOSE
	// Returns the total bandwidth out rate for these statistics
	float GetBandwidthOutRate() const;

	//PURPOSE
	// Sets the sample interval for all bandwidth recorders
	void SetSampleInterval(unsigned interval);

	//PURPOSE
	// Samples the data on all recorders
	void SampleData(unsigned currentTime);

	//PURPOSE
	// Returns the number of bandwidth recorders allocated via AllocateBandwidthRecorder()
	u32 GetNumBandwidthRecordersAllocated() const { return m_NumRecorders; }

	///////////////////////////////////////////////////////////////////////
	// STATIC

	//PURPOSE
	// Returns the last time all the bandwidth recorders were sorted on bandwidth usage
	static unsigned int GetLastFrameSorted()		{ return sm_LastFrameSorted; }

	//PURPOSE
	// Initialises the static data, called when network session is started
	static void ResetStaticData();

	//PURPOSE
	// Returns the total bandwidth in rate for all the statistics
	static float GetTotalBandwidthInRate();

	//PURPOSE
	// Returns the total bandwidth out rate for all the statistics
	static float GetTotalBandwidthOutRate();

	//PURPOSE
	// Sorts all of the recorders for all statistics in order of highest bandwidth 
	static void UpdateBandwidthRankings();

	//PURPOSE
	// Calculates the grand bandwidth totals for all the statistics
	static void UpdateGrandTotals() { sm_GrandTotalsRecorder.Update(); }

	//PURPOSE
	// Manually samples the grand bandwidth totals data
	static void SampleGrandTotalsData(unsigned currentTime) { sm_GrandTotalsRecorder.SampleData(currentTime); }

	//PURPOSE
	// Returns the name and bandwidth of the recorder with the given rank in the sorted in array 
	static netBandwidthRecorderWrapper *GetHighestBandwidthIn (unsigned rank, const char *&name, float &bandwidth);

	//PURPOSE
	// Returns the name and bandwidth of the recorder with the given rank in the sorted out array 
	static netBandwidthRecorderWrapper *GetHighestBandwidthOut(unsigned rank, const char *&name, float &bandwidth);

protected:

	void SetName(const char* name) { strncpy(m_Name, name, MAX_NAME_LENGTH); }

private:

	netBandwidthStatistics(const netBandwidthStatistics &);
	netBandwidthStatistics &operator=(const netBandwidthStatistics &);

    static const unsigned MAX_NAME_LENGTH = 128;

	char m_Name[MAX_NAME_LENGTH];

	atFixedArray<netBandwidthRecorderWrapper, MAX_RECORDERS> m_Recorders;		// The bandwidth recorders for these statistics 

    bool                            m_DisableBandwidthRecording;        // used to  disable bandwidth recording
	unsigned						m_NumRecorders;						// The number of bandwidth recorders for these statistics
	netBandwidthRecorderWrapper		m_TotalsRecorder;					// A recorder storing the total bandwidth in and out for these statistics

	unsigned						m_SampleInterval;					// The sample interval for all recorders

	///////////////////////////////////////////////////////////////////////
	// STATIC

	static unsigned 					sm_LastFrameSorted;			// The last time UpdateBandwidthRankings() was called and the recorders were sorted on bandwidth usage
	static unsigned						sm_NumTotalRecorders;		// The total number of recorders used by all statistics
	static netBandwidthRecorderWrapper	sm_GrandTotalsRecorder;		// A recorder storing the total bandwidth in and out for all statistics
	
	static atFixedArray<netBandwidthRecorderWrapper*, MAX_TOTAL_RECORDERS>	sm_UnsortedRecorders;			// Stores ptrs to all recorders used by all statistics
	static atFixedArray<netBandwidthRecorderWrapper*, MAX_TOTAL_RECORDERS>	sm_HighestInboundBandwidth;		// All bandwidth recorders sorted by highest inbound bandwidth
    static atFixedArray<netBandwidthRecorderWrapper*, MAX_TOTAL_RECORDERS>	sm_HighestOutboundBandwidth;	// All bandwidth recorders sorted by highest outbound bandwidth
};

} // namespace rage

#endif  // __BANK

#endif  // NETWORK_BANDWIDTH_STATISTICS_H
