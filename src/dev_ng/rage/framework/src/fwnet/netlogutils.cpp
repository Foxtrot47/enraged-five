//
// netlogutils.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "netlogutils.h"

#include "fwnet/netlog.h"
#include "fwnet/netplayer.h"
#include "fwnet/netinterface.h"
#include "fwnet/optimisations.h"

#include "string/string.h"

#include <stdio.h>

NETWORK_OPTIMISATIONS()

namespace rage
{

namespace NetworkLogUtils
{

#if ENABLE_NETWORK_LOGGING

	// Writes a message header to the specified log file - this is used by the different
    // network systems that send and receive messages
    void WriteMessageHeader(netLoggingInterface &log,
                            bool bReceived,
                            netSequence seqNum,
                            const netPlayer &player,
                            const char *type,
                            const char *messageText, ...)
    {

        if(log.IsEnabled())
        {
            char buffer[netLog::MAX_LOG_STRING];
            va_list args;
            va_start(args,messageText);
            vsprintf(buffer,messageText,args);
            va_end(args);

            log.WriteMessageHeader(bReceived, seqNum, type, buffer, player.GetLogName());
        }

    }

    void WritePlayerText(netLoggingInterface &log,
                         PhysicalPlayerIndex playerIndex,
                         const char *type,
                         const char *messageText, ...)
    {
        if(log.IsEnabled())
        {
		    netPlayer* pPlayer = NULL;
    		
		    if (playerIndex != INVALID_PLAYER_INDEX) 
		    {
			    pPlayer = netInterface::GetPhysicalPlayerFromIndex(playerIndex);
		    }

            char buffer[netLog::MAX_LOG_STRING];
            va_list args;
            va_start(args,messageText);
            vsprintf(buffer,messageText,args);
            va_end(args);

            log.WritePlayerText(type, buffer, pPlayer ? pPlayer->GetLogName() : "##INVALID PLAYER##");
        }
    }

    void WriteLogEvent(netLoggingInterface &log,
                       const char *eventName,
                       const char *extraData, ...)
    {
        if(log.IsEnabled())
        {
            char buffer[netLog::MAX_LOG_STRING];
            va_list args;
            va_start(args,extraData);
            vsprintf(buffer,extraData,args);
            va_end(args);

            // parse and remove any new lines from the extra data
            u32 trailingLineBreaks = 0;

            size_t textLength = strlen(buffer);

            const char *left  = buffer;
            const char *right = buffer + textLength - 1;

            bool newLineFound = true;

            while((left < right) && newLineFound)
            {
                newLineFound = false;

                if(*(right-1) == '\r' && *(right)=='\n')
                {
                    trailingLineBreaks++;
                    newLineFound = true;
                    right-=2;
                }
            }

            if(AssertVerify(textLength >= (trailingLineBreaks * 2)))
            {
                buffer[textLength - (trailingLineBreaks * 2)] = '\0';
            }

            log.WriteEventData(eventName, buffer, trailingLineBreaks);
        }
    }

#endif // ENABLE_NETWORK_LOGGING

} // namespace NetworkLogUtils

} // namespace rage
