//
// neteventmessages.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwnet/neteventmessages.h"

#include "net/message.h"

#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

NET_MESSAGE_IMPL( CMsgPackedEvents );
NET_MESSAGE_IMPL( CMsgPackedEventReliablesMsgs );

///////////////////////////////////////////////////////////////////////////////////////
// CMsgPackedEvents
///////////////////////////////////////////////////////////////////////////////////////

CMsgPackedEvents::CMsgPackedEvents()
{
    Reset();
}

bool CMsgPackedEvents::AddEvent(const netPlayer       &fromPlayer,
                                const NetworkEventType eventType,
                                const EventId          eventID,
                                const unsigned         eventIDSequence,
                                bool                   isSequential,
                                netSequence            eventTypeSequence,
                                u8                    *eventData,
                                const unsigned         eventDataSizeInBits)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_Events.m_MessageCount < (MAX_EVENTS_TO_PACK-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetEventSize(eventDataSizeInBits, isSequential);

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_Events.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<PackedEventData::SIZEOF_BUFFER_SIZE))
            {
                messageBuffer.WriteUns(eventType,           netGameEvent::SIZEOF_EVENTTYPE);
                messageBuffer.WriteUns(eventID,             netEventID::SIZEOF_EVENT_ID);
                messageBuffer.WriteUns(eventIDSequence,     netEventID::SIZEOF_EVENT_SEQUENCE);
                messageBuffer.WriteUns(eventDataSizeInBits, PackedEventData::SIZEOF_BUFFER_SIZE);
                messageBuffer.WriteBool(isSequential);

                if(isSequential)
                {
                    messageBuffer.WriteUns(eventTypeSequence, (sizeof(netSequence)<<3));
                }

                if(eventDataSizeInBits)
                {
                    messageBuffer.WriteBits(eventData, eventDataSizeInBits, 0);
                }

                m_Events.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

unsigned CMsgPackedEvents::GetEventSize(const unsigned eventDataSizeInBits, bool isSequential)
{
    unsigned size = 0;

    size += netGameEvent::SIZEOF_EVENTTYPE;
    size += netEventID::SIZEOF_EVENT_ID;
    size += netEventID::SIZEOF_EVENT_SEQUENCE;
    size += PackedEventData::SIZEOF_BUFFER_SIZE;
    size ++;

    if(isSequential)
    {
        size += (sizeof(netSequence)<<3);
    }

    if(eventDataSizeInBits)
    {
        size += eventDataSizeInBits;
    }

    return size;
}

unsigned CMsgPackedEvents::GetMessageHeaderBitSize() const
{
    unsigned size = netMessage::GetHeaderSize(MSG_ID());

    size += PackedEventData::SIZEOF_MESSAGE_COUNT;
    size += PackedEventData::SIZEOF_BUFFER_SIZE;

    return size;
}

unsigned CMsgPackedEvents::GetTotalPackedEventCount() const
{
    unsigned messageCount = 0;

    messageCount += m_Events.m_MessageCount;

    return messageCount;
}

unsigned CMsgPackedEvents::GetTotalSizeOfMsg() const
{
	return m_Events.m_MessageBuffer.GetCursorPos();
}

void CMsgPackedEvents::Reset()
{
    m_OwnerPlayer = 0;
    m_Events.Reset();
}

bool CMsgPackedEvents::IsSpaceAvailable(unsigned numRequiredBits) const
{
    static const unsigned maxSizeOfPackedEventData = MAX_MESSAGE_PAYLOAD_BITS - (PackedEventData::SIZEOF_MESSAGE_COUNT + PackedEventData::SIZEOF_BUFFER_SIZE);

    unsigned bitsUsed = 0;
    
    bitsUsed += m_Events.m_MessageBuffer.GetCursorPos();

    Assert(bitsUsed <= maxSizeOfPackedEventData);

    return ((bitsUsed + numRequiredBits) < maxSizeOfPackedEventData);
}

///////////////////////////////////////////////////////////////////////////////////////
// CMsgPackedEventReliablesMsgs
///////////////////////////////////////////////////////////////////////////////////////

CMsgPackedEventReliablesMsgs::CMsgPackedEventReliablesMsgs()
{
    Reset();
}

bool CMsgPackedEventReliablesMsgs::AddEventAck(const netPlayer &fromPlayer, const EventId eventID, const unsigned eventIDSequence)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_EventAcks.m_MessageCount < (MAX_MESSAGES_PER_TYPE-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetEventAckSize();

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_EventAcks.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<PackedEventData::SIZEOF_BUFFER_SIZE))
            {
                messageBuffer.WriteUns(eventID,         netEventID::SIZEOF_EVENT_ID);
                messageBuffer.WriteUns(eventIDSequence, netEventID::SIZEOF_EVENT_SEQUENCE);

                m_EventAcks.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

bool CMsgPackedEventReliablesMsgs::AddEventReply(const netPlayer &fromPlayer, const EventId eventID, const unsigned eventIDSequence, u8 *eventReplyData, const unsigned eventReplyDataSizeInBits)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_EventReplies.m_MessageCount < (MAX_MESSAGES_PER_TYPE-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetEventReplySize(eventReplyDataSizeInBits);

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_EventReplies.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<PackedEventData::SIZEOF_BUFFER_SIZE))
            {
                messageBuffer.WriteUns(eventID,         netEventID::SIZEOF_EVENT_ID);
                messageBuffer.WriteUns(eventIDSequence, netEventID::SIZEOF_EVENT_SEQUENCE);
                messageBuffer.WriteUns(eventReplyDataSizeInBits, PackedEventData::SIZEOF_BUFFER_SIZE);

                if(eventReplyDataSizeInBits)
                {
                    messageBuffer.WriteBits(eventReplyData, eventReplyDataSizeInBits, 0);
                }

                m_EventReplies.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

unsigned CMsgPackedEventReliablesMsgs::GetEventAckSize()
{
    return netEventID::SIZEOF_EVENT_ID + netEventID::SIZEOF_EVENT_SEQUENCE;
}

unsigned CMsgPackedEventReliablesMsgs::GetEventReplySize(const unsigned eventReplyDataSizeInBits)
{
    unsigned size = 0;

    size += netEventID::SIZEOF_EVENT_ID;
    size += netEventID::SIZEOF_EVENT_SEQUENCE;
    size += PackedEventData::SIZEOF_BUFFER_SIZE;

    if(eventReplyDataSizeInBits)
    {
        size += eventReplyDataSizeInBits;
    }

    return size;
}

unsigned CMsgPackedEventReliablesMsgs::GetMessageHeaderBitSize() const
{
    unsigned size = netMessage::GetHeaderSize(MSG_ID());

    size += SIZEOF_BITFLAGS;

    unsigned bitFlags = GetBitFlags();

    if(bitFlags & (1<<EVENT_ACK_MSG))
    {
        size += PackedEventData::SIZEOF_MESSAGE_COUNT;
        size += PackedEventData::SIZEOF_BUFFER_SIZE;
    }

    if(bitFlags & (1<<EVENT_REPLY_MSG))
    {
        size += PackedEventData::SIZEOF_MESSAGE_COUNT;
        size += PackedEventData::SIZEOF_BUFFER_SIZE;
    }

    return size;
}

unsigned CMsgPackedEventReliablesMsgs::GetTotalPackedMessageCount() const
{
    unsigned messageCount = 0;

    messageCount += m_EventAcks.m_MessageCount;
    messageCount += m_EventReplies.m_MessageCount;

    return messageCount;
}

void CMsgPackedEventReliablesMsgs::Reset()
{
    m_OwnerPlayer = 0;
    m_EventAcks.Reset();
    m_EventReplies.Reset();
}

unsigned CMsgPackedEventReliablesMsgs::GetBitFlags() const
{
    unsigned bitFlags = 0;

    if(m_EventAcks.m_MessageCount)
        bitFlags |= (1<<EVENT_ACK_MSG);

    if(m_EventReplies.m_MessageCount)
        bitFlags |= (1<<EVENT_REPLY_MSG);

    return bitFlags;
}

bool CMsgPackedEventReliablesMsgs::IsSpaceAvailable(unsigned numRequiredBits) const
{
    static const unsigned maxSizeOfPackedEventData = MAX_MESSAGE_PAYLOAD_BITS - SIZEOF_BITFLAGS - ((PackedEventData::SIZEOF_MESSAGE_COUNT + PackedEventData::SIZEOF_BUFFER_SIZE) * NUM_MSG_TYPES);

    unsigned bitsUsed = 0;
    
    bitsUsed += m_EventAcks.m_MessageBuffer.GetCursorPos();
    bitsUsed += m_EventReplies.m_MessageBuffer.GetCursorPos();

    Assert(bitsUsed <= maxSizeOfPackedEventData);

    return ((bitsUsed + numRequiredBits) < maxSizeOfPackedEventData);
}

} // namespace rage
