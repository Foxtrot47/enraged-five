// 
// netSyncData.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "fwnet/netsyncdata.h"

#include "math/amath.h"

#include "fwnet/optimisations.h"
#include "fwsys/timer.h"

NETWORK_OPTIMISATIONS()

namespace rage
{
#if __DEV
void netSyncDataBase::SanityCheckState()
{
	for (u32 player=0; player<GetMaxNumSyncedPlayers(); player++)
	{
		bool bSyncedWithPlayer = IsSyncedWithPlayer(player);
		bool bRequiresUpdate = GetPlayerRequiresAnUpdate(player);
		ASSERT_ONLY(bool bNodesSynced = true);
		ASSERT_ONLY(bool bNodesRequireUpdate = false);

		for (u32 i=0; i<GetNumSyncDataUnits(); i++)
		{
			netSyncDataUnitBase& unit = GetSyncDataUnit(i);

			if (!unit.IsSyncedWithPlayer(player))
			{
				ASSERT_ONLY(bNodesSynced = false);

				if (unit.GetPlayerRequiresAnUpdate(player))
				{
					ASSERT_ONLY(bNodesRequireUpdate = true);
				}
			}
		}

		if (bSyncedWithPlayer)
		{
			Assertf(bNodesSynced, "Sync data is synced with player %d but the nodes aren't", player);
		}
		else
		{
			Assertf(!bNodesSynced, "Sync data is not synced with player %d but the nodes are", player);

			if (bRequiresUpdate)
			{
				Assertf(bNodesRequireUpdate, "Sync data requires an update for player %d but the nodes don't", player);
			}
			else
			{
				Assertf(!bNodesRequireUpdate, "Sync data does not require an update for player %d but the nodes do", player);
			}
		}
	}
}
#endif // __DEV

} // namespace rage
