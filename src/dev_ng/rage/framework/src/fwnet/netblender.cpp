//
// netblender.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwnet/netblender.h"

#include "fwnet/netinterface.h"
#include "fwnet/nettypes.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

void netBlender::SetLastSyncMessageTime(u32 time)
{
    if(m_LastSyncMessageTime != 0)
    {
        u32 timeBetweenUpdates = GetTimeBetweenUpdates();

        if(timeBetweenUpdates < STOPPED_INTERVAL && (timeBetweenUpdates != 0))
        {
            m_AverageUpdateTime = (m_AverageUpdateTime + timeBetweenUpdates)>>1;

            if(netInterface::GetNetworkTime() > m_LastUpdateLevelChangeTime)
            {
                u32 timeSinceLastUpdateLevelChange = netInterface::GetNetworkTime() - m_LastUpdateLevelChangeTime;

                if(timeSinceLastUpdateLevelChange > UPDATE_ESTIMATION_INTERVAL)
                {
                    if(m_AverageUpdateTime < HIGH_UPDATE_THRESHOLD)
                    {
                        m_EstimatedUpdateLevel = HIGH_UPDATE_LEVEL;
                    }
                    else if(m_AverageUpdateTime < MEDIUM_UPDATE_THRESHOLD)
                    {
                        m_EstimatedUpdateLevel = MEDIUM_UPDATE_LEVEL;
                    }
                    else
                    {
                        m_EstimatedUpdateLevel = LOW_UPDATE_LEVEL;
                    }

                    m_LastUpdateLevelChangeTime = netInterface::GetNetworkTime();
                }
            }
        }
    }

    m_LastSyncMessageTime = time;
}

void netBlender::SetLastOwnershipChangeTime(u32 time)
{
	m_LastOwnershipChangeTime = time;
}

#if __BANK

const char *netBlender::GetEstimatedUpdateLevelName() const
{
    switch(m_EstimatedUpdateLevel)
    {
    case LOW_UPDATE_LEVEL:
        return "Low";
    case MEDIUM_UPDATE_LEVEL:
        return "Medium";
    case HIGH_UPDATE_LEVEL:
        return "High";
    default:
        return "Unknown";
    }
}

#endif

} // namespace rage
