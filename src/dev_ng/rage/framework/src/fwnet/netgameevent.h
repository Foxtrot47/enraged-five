//
// netgameevent.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_EVENT_H
#define NETWORK_EVENT_H

#include "fwnet/nettypes.h"
#include "fwtl/pool.h"

#include "net/netsequence.h"

// this must be a multiple of 16 otherwise we will get alignment problems on Xbox 360
#if __64BIT
#define LARGEST_NETWORKEVENT_CLASS  560
#else
#define LARGEST_NETWORKEVENT_CLASS  288
#endif

#define MAX_ACTIVE_NETWORK_EVENTS 300

namespace rage
{
    class datBitBuffer;
    class netEventID;
    class netEventMgr;
    class netPlayer;

//PURPOSE
// Network game events are one off reliable messages sent to a remote machine - these could be a variety of things such as
// notifying a remote machine that a gun has been fired or asking a remote machine for control of one of its
// network objects. Some events also require a reply from the machine that they are sent to.
class netGameEvent
{
public:

    FW_REGISTER_CLASS_POOL(netGameEvent);

    static const int SIZEOF_EVENTTYPE       = 7;   // The number of bits to use when sending the event type
    static const int DEFAULT_TIME_TO_RESEND = 200; // The default time period to wait before resending an event
    static const int NETEVENTTYPE_MAXTYPES  = 91;  // The maximum number of event types

    //PURPOSE
    // Class constructor
    //PARAMS
    // eventType     - The type of event being created
    // requiresReply - Indicates whether this event requires a reply from the target machine(s)
    // isSequential  - Indicates that events of this type should be processed in order
    // owner         - The player that created this event. Unless this is non-zero this is the local
    //                 player. This parameter is used by network bots
    netGameEvent( const NetworkEventType eventType,
                  bool requiresReply,
                  bool isSequential = false,
                  netPlayer *owner = 0);

    virtual ~netGameEvent();

    //PURPOSE
    // Returns the event ID assigned to this event
    netEventID &GetEventId() const { Assert(m_EventId); return *m_EventId; }

    //PURPOSE
    // Assigns this event a new event ID
    //PARAMS
    // eventID - The event ID to assign
    void SetEventId(netEventID *eventID);

    //PURPOSE
    // Returns the type of this event
    NetworkEventType GetEventType() const { return m_EventType; }

    //PURPOSE
    // Sets the player that created this event
    //PARAMS
    // owner - The player owner
    void SetOwnerPlayer(netPlayer *owner) { m_OwnerPlayer = owner; }

    //PURPOSE
    // Returns the player that created this event
    netPlayer *GetOwnerPlayer() const { return m_OwnerPlayer; }

    //PURPOSE
    // Returns whether this event requires a reply from the target machine(s)
    bool RequiresAReply() const { return m_RequiresReply; }

    //PURPOSE
    // Returns whether events of this type should be processed in order (whether out of order events are dropped)
    bool IsSequential() const { return m_IsSequential; }

    //PURPOSE
    // Sets whether events of this type should be processed in order (whether out of order events are dropped)
    netSequence GetEventSequence() const { Assert(IsSequential()); return m_EventSequence;}

    //PURPOSE
    // Marks this event as being sent over the network
    void SetBeenSent() { m_HasBeenSent = true; }

    //PURPOSE
    // Returns whether this event has been sent over the network
    bool HasBeenSent() const { return m_HasBeenSent; }

	//PURPOSE
	// Flag the event for removal nexu event manager update
	void FlagForRemoval() { m_FlaggedForRemoval = true; }

	//PURPOSE
	// Returns whether this event is flagged for removal during the event manager update
	bool IsFlaggedForRemoval() const { return m_FlaggedForRemoval; }

    //PURPOSE
    // Marks this event has been packed for the specified network player
    //PARAMS
    // player - The network player the event has been packed for
    void SetHasAlreadyBeenPacked(const netPlayer &player);

    //PURPOSE
    // Resets the flag indicating this event has been packed for the specified network player
    //PARAMS
    // player - The network player the event has been packed for
    void ResetHasAlreadyBeenPacked(const netPlayer &player);

    //PURPOSE
    // Returns whether this event has been packed for the specified network player
    //PARAMS
    // player - The network player the event has been packed for
    bool HasAlreadyBeenPacked(const netPlayer &player) const;

    //PURPOSE
    // Delays sending of the event for the specified time (if it has not already been sent)
    void DelaySending(u32 delay);

    //PURPOSE
    // Returns the event name with its sequence number and ID appended
    const char* GetEventNameAndNumber() const;

    //PURPOSE
    // Returns the name of this event
    virtual const char* GetEventName() const = 0;

    //PURPOSE
    // Returns whether this event should be sent to the specified network player
    //PARAMS
    // player - The player to check
    virtual bool IsInScope(const netPlayer&) const = 0;

    //PURPOSE
    // Returns true if the event is ready to be sent again
    //PARAMS
    // currTime - The current time
    virtual bool TimeToResend(u32 currTime);

	//PURPOSE
	// Returns true if the event can change scope while on the event queue
	virtual bool CanChangeScope() const { return false; }

	//PURPOSE
    // Prepares the event to be sent over the network: fills the specified message buffer with data
    // data to be sent to the players in scope of the event
    //PARAMS
    // messageBuffer - The message buffer to write the event data to
	// toPlayer      - The player the event data is being sent to
    virtual void Prepare(datBitBuffer &messageBuffer, const netPlayer &toPlayer) = 0;

    //PURPOSE
    // Processes the data that arrives in an event message sent from another machine
    //PARAMS
    // messageBuffer - The message buffer to read the event data from
	// fromPlayer    - The player the event data was received from
	// toPlayer      - The player or bot the event data was sent to
    virtual void Handle(datBitBuffer &messageBuffer, 
                        const netPlayer &fromPlayer,
                        const netPlayer &toPlayer) = 0;

    //PURPOSE
    // Decides whether to ack/reply to a received event. Returns true if an ack/reply is to be sent
    //PARAMS
    // player - The player the event data was received from
	// toPlayer      - The player or bot the event data was sent to
   virtual bool Decide(const netPlayer &UNUSED_PARAM(fromPlayer),
                        const netPlayer &UNUSED_PARAM(toPlayer)) { return true; }

    //PURPOSE
    // Prepares the reply to be sent over the network: fills the given message buffer with the
    // data to send in reply to an event received from another machine
    //PARAMS
    // messageBuffer - The message buffer to write the event reply data to
   // toPlayer      - The player the reply is being sent to
    virtual void PrepareReply(datBitBuffer &UNUSED_PARAM(messageBuffer), const netPlayer &UNUSED_PARAM(toPlayer)) {}

    //PURPOSE
    // Processes the data that arrives in an event reply message sent from another machine
    //PARAMS
    // messageBuffer - The message buffer to read the event reply data from
    // player        - The player the event reply data was received from
    virtual void HandleReply(datBitBuffer &UNUSED_PARAM(messageBuffer),
                             const netPlayer &UNUSED_PARAM(fromPlayer),
                             const netPlayer &UNUSED_PARAM(toPlayer)) {}

    //PURPOSE
    // Prepares any extra data that is to be sent along with the event data and/or reply data
    //PARAMS
    // messageBuffer - The message buffer to write the extra data to
    // isReply       - Indicates whether this extra data is associated with an event or event reply
	// player        - The player the extra data is being sent to
  virtual void PrepareExtraData(datBitBuffer &UNUSED_PARAM(messageBuffer), bool UNUSED_PARAM(isReply), const netPlayer &UNUSED_PARAM(player)) {}

    //PURPOSE
    // Processes any extra data that is received along with the event data and/or reply data
    //PARAMS
    // messageBuffer - The message buffer to write the extra data to
    // isReply       - Indicates whether this extra data is associated with an event or event reply
    virtual void HandleExtraData(datBitBuffer         &UNUSED_PARAM(messageBuffer),
                                 bool                  UNUSED_PARAM(isReply),
                                 const netPlayer &UNUSED_PARAM(fromPlayer),
                                 const netPlayer &UNUSED_PARAM(toPlayer)) {}

    //PURPOSE
    // Writes the event data sent/received to a log file
    //PARAMS
    // wasSent       - Indicates whether the event data was sent or received
    // eventLogOnly  - Indicates whether this data should only be logged to the event logging system
    // messageBuffer - The message buffer containing the event data
    virtual void WriteEventToLogFile(bool UNUSED_PARAM(wasSent), bool UNUSED_PARAM(eventLogOnly), datBitBuffer *UNUSED_PARAM(messageBuffer) = 0) const {}

    //PURPOSE
    // Writes the data relating to whether an event was ACKed/Replied to a log file
    virtual void WriteDecisionToLogFile() const {}

    //PURPOSE
    // Writes the event reply data sent/received to a log file
    //PARAMS
    // wasSent - Indicates whether the reply data was sent or received
    virtual void WriteReplyToLogFile(bool UNUSED_PARAM(wasSent)) const {}

    //PURPOSE
    // Writes the extra data sent/received to a log file
    //PARAMS
    // messageBuffer - The message buffer containing the extra data
    // isReply       - Indicates whether this extra data is associated with an event or event reply
    virtual void WriteExtraDataToLogFile(datBitBuffer &UNUSED_PARAM(messageBuffer), bool UNUSED_PARAM(isReply)) const {}

    //PURPOSE
    // The equals operator is used to stop events being added to the event queue if they are equivalent to an event already on it
    //PARAMS
    // networkEvent - The network event to compare to this event
    virtual bool operator==(const netGameEvent*) const { return false; }
    virtual bool operator!=(const netGameEvent *networkEvent) const { return !(networkEvent && (*networkEvent==this)); }

    //PURPOSE
    // Indicates whether this event is important and must be sent, otherwise it can be dumped if the event queue gets too full
    virtual bool MustPersist() const { return true; }

    //PURPOSE
    // Indicates whether the event should remain on the event queue even when not in scope with any other players
    virtual bool MustPersistWhenOutOfScope() const { return false; }

    //PURPOSE
    // Some events may no longer be valid while they are still being resent to remote machines,
    // overriding this function allows an event to be removed when it becomes stale
    virtual bool HasTimedOut() const { return false; }

protected:

	NetworkEventType  m_EventType;          // The type of event

	// FRAMEWORK TODO - Move the m_RequiresReply back to private
    //                  This is used by the weapon damage events in Agent presently, but needs refactoring
    bool m_RequiresReply : 1; // Flag indicating this event requires a reply from the machine it was sent to

private:

    netEventID       *m_EventId;				// The event id assigned to this event
    netPlayer        *m_OwnerPlayer;			// The player sending the event
    u32               m_TimeLastSent;			// The last time the event was sent
    netSequence       m_EventSequence;			// The sequence number of this event if it is sequential
    bool              m_IsSequential   : 1;		// Flag indicating that events that arrive out of sequence should be ignored
    bool              m_HasBeenSent    : 1;		// Flag set when the event is first sent
	bool			  m_FlaggedForRemoval : 1;	// Flag the event to be removed in the next update
    PlayerFlags       m_AlreadyPackedFlags;		// Flag set when this event is currently in the packed event message pending sending for the different players
};

} // namespace rage

#endif  //NETWORK_OBJECT_H
