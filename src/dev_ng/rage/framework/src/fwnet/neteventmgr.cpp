//
// neteventmgr.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/neteventmgr.h"

#include "fwsys/timer.h"

#include "fwnet/netinterface.h"
#include "fwnet/netgameevent.h"
#include "fwnet/neteventmessages.h"
#include "fwnet/netbandwidthmgr.h"
#include "fwnet/netbandwidthstatscollector.h"
#include "fwnet/netchannel.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netplayer.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"
#include "fwtl/regdrefs.h"

NETWORK_OPTIMISATIONS()

namespace rage
{
#if !__NO_OUTPUT && REGREF_VALIDATE_CDCDCDCD
	XPARAM(RegrefValidation);
#endif

FW_INSTANTIATE_CLASS_POOL(netEventMgr::atDNetEventNode, MAX_ACTIVE_NETWORK_EVENTS, atHashString("atDNetEventNode",0x26b79f6f));

netGameEvent *netEventMgr::ms_EventBeingProcessed = 0;

netEventMgr::netEventMgr(netBandwidthMgr &bandwidthMgr) :
m_BandwidthMgr(bandwidthMgr)
, m_Log(0)
, m_LogSplitter(0)
, m_EventIDSearchStart(0)
, m_IsInitialised(false)
, m_IsShuttingDown(false)
, m_IsProcessingEventQueue(false)
#if __BANK
, m_NumUnreliableEventMessagesSent(0)
, m_NumReliableEventMessagesSent(0)
#endif // __BANK
{
#if __DEV
    m_EventHeaderRecId              = INVALID_RECORDER_ID;
    m_AckMsgRecId                   = INVALID_RECORDER_ID;
    m_ReplyMsgHeaderRecId           = INVALID_RECORDER_ID;
    m_PackedEventsMsgHeaderRecId    = INVALID_RECORDER_ID;
#endif

    // Initialize our message handler.
    m_Dlgt.Bind(this, &netEventMgr::OnMessageReceived);
}

netEventMgr::~netEventMgr()
{
    Shutdown();
}

bool netEventMgr::Init()
{
    bool success = false;

    if(gnetVerify(!m_IsInitialised))
    {
		const unsigned LOG_BUFFER_SIZE = 16 *1024;
		static bool s_HasCreatedLog = false; 
		m_Log         = rage_new netLog("EventManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                          netInterface::GetDefaultLogFileBlockingMode(), LOG_BUFFER_SIZE);
		s_HasCreatedLog = true; 
		m_LogSplitter = rage_new netLogSplitter(*m_Log, netInterface::GetMessageLog());
		
        // Register our message handler.
        netInterface::AddDelegate(&m_Dlgt);

        // allocate the pools out of the network heap
        netGameEvent::InitPool( MEMBUCKET_NETWORK );
        atDNetEventNode::InitPool( MEMBUCKET_NETWORK );
#if !__NO_OUTPUT && REGREF_VALIDATE_CDCDCDCD
		if(PARAM_RegrefValidation.Get())
		{
			netGameEvent::GetPool()->setLogged(true);
		}
#endif

        m_EventIds.Resize(MAX_NUM_EVENT_IDS);

        for(int i=0; i<MAX_NUM_EVENT_IDS; i++)
        {
            m_EventIds[i].Init(i);
        }

        m_EventSequences.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);
        m_LocalEventTypeSequences.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);
        m_EventHandlers.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);
		m_EventTriggerCounters.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);
        m_EventNames.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);
        m_LastTimeEventSent.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);

        for(int i=0; i<netGameEvent::NETEVENTTYPE_MAXTYPES; i++)
        {
            m_EventSequences[i].Resize(MAX_NUM_PHYSICAL_PLAYERS);
            m_LastTimeEventSent[i].Resize(MAX_NUM_PHYSICAL_PLAYERS);

            for(unsigned j=0; j<MAX_NUM_PHYSICAL_PLAYERS; j++)
            {
                m_EventSequences[i][j]    = 0;
                m_LastTimeEventSent[i][j] = 0;
            }

            m_LocalEventTypeSequences[i] = 1;
            m_EventHandlers[i] = 0;
			m_EventTriggerCounters[i] = 0;
            m_EventNames[i] = 0;
        }

        m_PackedEventsMsgs.Resize(MAX_NUM_PHYSICAL_PLAYERS);
        m_PackedEventReliablesMsgs.Resize(MAX_NUM_PHYSICAL_PLAYERS);
			
#if __BANK
        // Register bandwidth stats
        m_BandwidthMgr.RegisterStatistics(m_BandwidthStats, "Event Manager");

        // allocate bandwidth recorders for the different event manager messages
        m_EventHeaderRecId              = m_BandwidthStats.AllocateBandwidthRecorder("Event headers");
        m_AckMsgRecId                   = m_BandwidthStats.AllocateBandwidthRecorder("Event ack messages");
        m_ReplyMsgHeaderRecId           = m_BandwidthStats.AllocateBandwidthRecorder("Event reply msg headers");
        m_PackedEventsMsgHeaderRecId    = m_BandwidthStats.AllocateBandwidthRecorder("Event packed msg header");

        m_EventRecorderIds.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);
        m_EventReplyRecorderIds.Resize(netGameEvent::NETEVENTTYPE_MAXTYPES);

        for (int i=0; i<netGameEvent::NETEVENTTYPE_MAXTYPES; i++)
        {
            m_EventRecorderIds[i] = INVALID_RECORDER_ID;
            m_EventReplyRecorderIds[i] = INVALID_RECORDER_ID;
        }

        m_NumUnreliableEventMessagesSent = 0;
        m_NumReliableEventMessagesSent   = 0;
#endif
        success = m_IsInitialised = true;
    }

    return success;
}

void netEventMgr::Update()
{
    // resend all events on the event list
    atDNode<netGameEvent*, datBase> *pNode     = m_EventList.GetHead();
    atDNode<netGameEvent*, datBase> *pNextNode = 0;

	m_IsProcessingEventQueue = true;

    while (pNode)
    {
        pNextNode = pNode->GetNext();

        netGameEvent* pEvent = pNode->Data;

		bool eventTimedOut = pEvent->HasTimedOut();
		bool eventFlaggedForRemoval = pEvent->IsFlaggedForRemoval();

        if (pEvent->TimeToResend(fwTimer::GetSystemTimeInMilliseconds()) && !eventTimedOut && !eventFlaggedForRemoval)
        {
			if (pEvent->CanChangeScope())
			{
#if ENABLE_NETWORK_LOGGING
				// log the event data
				NetworkLogUtils::WriteLogEvent(GetLogSplitter(), "RECALCULATING_SCOPE", "%s_%d", GetEventNameFromType(pEvent->GetEventType()), pEvent->GetEventId().GetID());
#endif // ENABLE_NETWORK_LOGGING

				PlayerFlags prevScopeFlags = pEvent->GetEventId().GetPlayerInScopeFlags();

				pEvent->GetEventId().ClearPlayersInScope();

				// flag all connections that the event needs to be sent over in the event's id
				unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
				const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

				for(unsigned index = 0; index < numPhysicalPlayers; index++)
				{
					const netPlayer *player = allPhysicalPlayers[index];

					if (!player->IsLeaving() && pEvent->IsInScope(*player))
					{
						if (Verifyf(!player->IsLocal(), "Event %s_%d is in scope with local player!", GetEventNameFromType(pEvent->GetEventType()), pEvent->GetEventId().GetID()))
						{
							bool bIncrementSequence = false;
							PhysicalPlayerIndex physicalPlayerIndex = player->GetPhysicalPlayerIndex();

							//If the player wasn't previous in scope, and hasn't acked this event already - then it's a new player that just came into scope - in which case we will need to increment the
							//sequence number for this player that just came into session.
							if (((prevScopeFlags & (1<<physicalPlayerIndex)) == 0) && !pEvent->GetEventId().IsAckedByPlayer(physicalPlayerIndex))
							{
								bIncrementSequence = true;
							}

							pEvent->GetEventId().SetPlayerInScope(player->GetPhysicalPlayerIndex(), bIncrementSequence);
							GetLog().Log("\t\t>> %s (%d)\r\n", player->GetLogName(), pEvent->GetEventId().GetSendSequence(player->GetPhysicalPlayerIndex()));
						}
					}
				}
			}

			if (pEvent->GetEventId().IsWaitingForAck())
			{
				SendEvent(pEvent);
			}
        }

        // events can become free during the send event process if a connection is dropped
        // so we might need to release this event and remove it from the list
        bool        destroyEvent = false;
        const char *reason       = "";

		if(eventFlaggedForRemoval)
		{
			destroyEvent = true;
			reason       = "Flagged for removal";
		}

		if(!pEvent->GetEventId().IsWaitingForAck() && !pEvent->MustPersistWhenOutOfScope())
        {
            destroyEvent = true;
            reason       = "No players left in scope";
        }

        if(eventTimedOut)
        {
            destroyEvent = true;
            reason       = "Timed out";
        }

        if(destroyEvent)
        {
            NetworkLogUtils::WriteLogEvent(GetLog(), "DESTROYING_EVENT", "%s_%d", pEvent->GetEventName(), pEvent->GetEventId().GetID());
            GetLog().WriteDataValue("Reason", reason);

            delete pEvent;
            m_EventList.PopNode(*pNode);
            delete pNode;
        }

        pNode = pNextNode;
    }

	m_IsProcessingEventQueue = false;

    // send any packed event messages that remain
    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *player = allPhysicalPlayers[index];

        if(player->GetConnectionId() >= 0 || player->IsLocal())
        {
            // hold off sending the messages if the game is already struggling to send existing ones
            // this will just add latency as when the messages get full they will be sent
            if(player->IsLocal() || !m_BandwidthMgr.GetReliableMessagesHaveBeenUnackedTooLong(player->GetConnectionId()))
            {
                SendPackedEventsMessage(*player);
                SendPackedEventReliablesMessage(*player);
            }
        }
    }
}

void netEventMgr::Shutdown()
{
    if(m_IsInitialised)
    {
		m_IsShuttingDown = true;

        // need to restore the game heap here as any memory allocations (including the nodes)
        // will be allocated from the main heap
        sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
        sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

        atDNode<netGameEvent*, datBase> *pNode = m_EventList.GetHead();

        while (pNode)
        {
            delete pNode->Data;
            pNode->Data = NULL;
            pNode = pNode->GetNext();
        }

        sysMemAllocator::SetCurrent(*oldHeap);

		m_EventList.DeleteAll();

        // the pools are allocated out of the network heap
        netGameEvent::ShutdownPool();
        atDNetEventNode::ShutdownPool();

        netInterface::RemoveDelegate(&m_Dlgt);

        if (!m_EventSequences.empty())
        {
            for(int i=0; i<netGameEvent::NETEVENTTYPE_MAXTYPES; i++)
            {
                m_EventSequences[i].Reset();
            }
        }

        for(int i=0; i<netGameEvent::NETEVENTTYPE_MAXTYPES; i++)
        {
            m_LastTimeEventSent[i].Reset();
        }

		for(int i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			m_PackedEventsMsgs[i].Reset();
			m_PackedEventReliablesMsgs[i].Reset();
		}

        m_EventSequences.Reset();
        m_EventIds.Reset();
        m_PackedEventsMsgs.Reset();
        m_PackedEventReliablesMsgs.Reset();
        m_LocalEventTypeSequences.Reset();
        m_EventHandlers.Reset();
		m_EventTriggerCounters.Reset();
        m_EventNames.Reset();
        m_LastTimeEventSent.Reset();

        delete m_LogSplitter;
        delete m_Log;

        m_LogSplitter = 0;
        m_Log         = 0;

        m_IsInitialised = false;
		m_IsShuttingDown = false;
    }
}

bool netEventMgr::IsInitialised() const
{
    return m_IsInitialised;
}


bool netEventMgr::IsShuttingDown() const
{
	return m_IsShuttingDown;
}

void netEventMgr::RegisterNetworkEvent(const NetworkEventType eventType, fnEventHandler eventHandler, const char *eventName)
{
    if(gnetVerify(eventType < netGameEvent::NETEVENTTYPE_MAXTYPES))
    {
        gnetAssert(m_EventHandlers[eventType] == 0);
		gnetAssert(m_EventTriggerCounters[eventType] == 0);
        gnetAssert(m_EventNames[eventType]    == 0);
        gnetAssert(eventHandler);
        gnetAssert(eventName);

        m_EventHandlers[eventType] = eventHandler;
        m_EventNames[eventType]    = eventName;

#if __BANK
        // allocate a bandwidth recorder for this event
		m_EventRecorderIds[eventType] = m_BandwidthStats.AllocateBandwidthRecorder(m_EventNames[eventType]);

        // allocate a bandwidth recorder for this event's reply
        char replyName[100];
        sprintf(replyName, "%s Reply", eventName);
        m_EventReplyRecorderIds[eventType] = m_BandwidthStats.AllocateBandwidthRecorder(replyName);
#endif // __BANK
    }
}

const char *netEventMgr::GetEventNameFromType(const NetworkEventType eventType) const
{
    const char *eventName = "Unknown Event";

    if(gnetVerify(eventType < netGameEvent::NETEVENTTYPE_MAXTYPES))
    {
        if(m_EventNames[eventType])
        {
            eventName = m_EventNames[eventType];
        }
    }

    return eventName;
}

void netEventMgr::PlayerHasJoined(const netPlayer& player)
{
	NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_JOINED", "%s", player.GetLogName());

    if (m_IsInitialised)
    {
		if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
		{
			m_PackedEventsMsgs        [player.GetPhysicalPlayerIndex()].Reset();
			m_PackedEventReliablesMsgs[player.GetPhysicalPlayerIndex()].Reset();

			for(int i=0; i<MAX_NUM_EVENT_IDS; i++)
			{
				m_EventIds[i].ClearPlayerSequences(player.GetPhysicalPlayerIndex());
			}
		}
    }
}

void netEventMgr::PlayerHasLeft(const netPlayer& player)
{
	NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_LEFT", "%s", player.GetLogName());

    if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
        atDNode<netGameEvent*, datBase> *pNode = m_EventList.GetHead();
        atDNode<netGameEvent*, datBase> *pNextNode = NULL;

        // find any events waiting for acknowledgment over this players connection and remove them if necessary
        while (pNode)
        {
            netGameEvent* pEvent = pNode->Data;

            pNextNode = pNode->GetNext();

            if (pEvent->GetEventId().MustSendToPlayer(player.GetPhysicalPlayerIndex()))
            {
                pEvent->GetEventId().AckedByPlayer(player.GetPhysicalPlayerIndex());

                if (!pEvent->GetEventId().IsWaitingForAck())
                {
                    GetLog().Log("\t\tSetting %s acked by player - flagging event for removal\r\n", pEvent->GetEventNameAndNumber());

#if ENABLE_NETWORK_LOGGING
					NetworkLogUtils::WriteLogEvent(GetLog(), "FLAGGING_EVENT_FOR_REMOVAL", "%s", pEvent->GetEventName());
					pEvent->WriteEventToLogFile(true, true);
					GetLog().WriteDataValue("Reason", "PlayerHasLeft - no remaining acks to wait for");
#endif // ENABLE_NETWORK_LOGGING

					pEvent->FlagForRemoval();
                }
                else
                {
                    GetLog().Log("\t\tSetting %s acked by player\r\n", pEvent->GetEventNameAndNumber());
                }
            }

            pNode = pNextNode;
        }

		m_PackedEventsMsgs        [player.GetPhysicalPlayerIndex()].Reset();
		m_PackedEventReliablesMsgs[player.GetPhysicalPlayerIndex()].Reset();

		for(unsigned i=0; i<netGameEvent::NETEVENTTYPE_MAXTYPES; i++)
		{
			m_EventSequences[i][player.GetPhysicalPlayerIndex()] = 0;
            m_LastTimeEventSent[i][player.GetPhysicalPlayerIndex()] = 0;
		}

        for(int i=0; i<MAX_NUM_EVENT_IDS; i++)
        {
           m_EventIds[i].ClearPlayerSequences(player.GetPhysicalPlayerIndex());
        }
    }
}

#if __DEV
int netEventMgr::GetSizeOfEventList() const
{
    int numEvents = 0;

    const atDNode<netGameEvent*, datBase> *pNode = m_EventList.GetHead();

    while (pNode)
    {
        numEvents++;
        pNode = pNode->GetNext();
    }

    return numEvents;
}

int netEventMgr::GetNumUsedEventIds() const
{
    int numUsed = 0;

    // find a free event id for this event
    for (int i=0; i<MAX_NUM_EVENT_IDS; i++)
    {
        if (!m_EventIds[i].IsFree())
        {
            numUsed++;
        }
    }

    return numUsed;
}
#endif

void netEventMgr::OnMessageReceived(const ReceivedMessageData &messageData)
{
    unsigned msgId = 0;
    if(netMessage::GetId(&msgId, messageData.m_MessageData, messageData.m_MessageDataSize))
    {
        if ( msgId == CMsgPackedEvents::MSG_ID() )
        {
            HandlePackedEventsMsg(messageData);
        }
        else if ( msgId == CMsgPackedEventReliablesMsgs::MSG_ID() )
        {
            HandlePackedEventReliablesMsg(messageData);
        }
    }
}

void netEventMgr::HandlePackedEventsMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        CMsgPackedEvents packedEventsMsg;

        if(gnetVerify(packedEventsMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
        {
            // record the packed events header
            BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_PackedEventsMsgHeaderRecId, packedEventsMsg.GetMessageHeaderBitSize()));

            if(gnetVerifyf(messageData.m_FromPlayer, "Received a message from an invalid player!"))
            {
                const netSequence msgSeq = messageData.m_NetSequence;

                NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, msgSeq, *messageData.m_FromPlayer, "PACKED_EVENTS", "");

                GetLogSplitter().WriteDataValue("Num events", "%d", packedEventsMsg.m_Events.m_MessageCount);
            }

            ProcessPackedEvents(packedEventsMsg.m_Events.m_MessageBuffer, *messageData.m_FromPlayer, *messageData.m_ToPlayer);
        }
    }
}

void netEventMgr::HandlePackedEventReliablesMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        CMsgPackedEventReliablesMsgs packedEventsMsg;

        if(gnetVerify(packedEventsMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize)))
        {
            // record the packed events header
            BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_PackedEventsMsgHeaderRecId, packedEventsMsg.GetMessageHeaderBitSize()));

            if(gnetVerifyf(messageData.m_FromPlayer, "Received a message from an invalid player!"))
            {
                const netSequence msgSeq = messageData.m_NetSequence;

                NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, msgSeq, *messageData.m_FromPlayer, "PACKED_EVENT_RELIABLES", "");

                GetLogSplitter().WriteDataValue("Num event acks", "%d", packedEventsMsg.m_EventAcks.m_MessageCount);
                GetLogSplitter().WriteDataValue("Num event replies", "%d", packedEventsMsg.m_EventReplies.m_MessageCount);
            }

            ProcessPackedEventAcks(packedEventsMsg.m_EventAcks.m_MessageBuffer, *messageData.m_FromPlayer, *messageData.m_ToPlayer);
            ProcessPackedEventReplies(packedEventsMsg.m_EventReplies.m_MessageBuffer, *messageData.m_FromPlayer, *messageData.m_ToPlayer);
        }
    }
}

void netEventMgr::HandleEvent(class netGameEvent *pEvent,
                              datBitBuffer       &msgBuffer,
                              const netPlayer    &fromPlayer,
                              const netPlayer    &toPlayer,
                              const netSequence   messageSeq,
                              const EventId       eventID,
                              const unsigned      eventIDSequence)
{
    if(!gnetVerifyf(eventID < MAX_NUM_EVENT_IDS, "Processing an event with an invalid event ID! (Id = %d)", eventID))
    {
        return;
    }

    pEvent->Handle(msgBuffer, fromPlayer, toPlayer);

    PhysicalPlayerIndex fromPlayerIndex = fromPlayer.GetPhysicalPlayerIndex();
    NetworkEventType    eventType       = pEvent->GetEventType();

    NETWORK_QUITF(eventType < netGameEvent::NETEVENTTYPE_MAXTYPES, "Handling a network event with an invalid event type!");

	if (pEvent->IsSequential() && gnetVerify(fromPlayerIndex != INVALID_PLAYER_INDEX))
	{
        netSequence eventTypeSequence = m_EventSequences[eventType][fromPlayerIndex];

        // dump out of sequence messages (also catch case where the sequence number goes back to 0 after 65535)
        if (messageSeq < eventTypeSequence && (eventTypeSequence - messageSeq) < 10000 )
        {
#if ENABLE_NETWORK_LOGGING
            pEvent->WriteEventToLogFile(false, false);
            GetLogSplitter().WriteDataValue("IGNORED", "Out of order");
#endif // ENABLE_NETWORK_LOGGING
            return;
        }
    }

    ms_EventBeingProcessed = pEvent;

    if (pEvent->Decide(fromPlayer, toPlayer))
    {
        unsigned cursorPos = msgBuffer.GetCursorPos();

#if ENABLE_NETWORK_LOGGING
        pEvent->WriteEventToLogFile(false, false, &msgBuffer);
#endif // ENABLE_NETWORK_LOGGING

        // restore cursor pos at start of extra data
        msgBuffer.SetCursorPos(cursorPos);

        // Some events have some extra data tagged on (eg a GiveControl event contains the migration data for the object)
        pEvent->HandleExtraData(msgBuffer, false, fromPlayer, toPlayer);

#if ENABLE_NETWORK_LOGGING
  		pEvent->WriteDecisionToLogFile();
#endif // ENABLE_NETWORK_LOGGING

        if (pEvent->RequiresAReply())
        {
            SendReply(pEvent, fromPlayer, toPlayer, eventID, eventIDSequence);
        }
        else
        {
            SendAck(pEvent, fromPlayer, toPlayer, eventID, eventIDSequence);
        }

		// record this message seq for sequential events, so we can subsequently ignore any with an equal or lower seq after this
		if (pEvent->IsSequential() && gnetVerify(fromPlayerIndex != INVALID_PLAYER_INDEX))
		{
			m_EventSequences[eventType][fromPlayerIndex] = messageSeq;
		}

		// mark the event as processed
        m_EventIds[eventID].MarkProcessed(fromPlayerIndex, eventIDSequence);
    }
    else
    {
#if ENABLE_NETWORK_LOGGING
        pEvent->WriteEventToLogFile(false, false);
        pEvent->WriteDecisionToLogFile();

        GetLogSplitter().WriteDataValue("RESPONSE", "Event decided not to reply");
#endif // ENABLE_NETWORK_LOGGING
    }

    ms_EventBeingProcessed = 0;
}

void netEventMgr::PrepareEvent(netGameEvent* pEvent)
{
    gnetAssertf(m_EventHandlers[pEvent->GetEventType()] != 0, "Event %s has not been registered", pEvent->GetEventName());

    if (!pEvent) // this can happen if the event pool runs out
        return;

    // check to see whether this event is already on the queue, don't add it if so
    atDNode<netGameEvent*, datBase> *pNode     = m_EventList.GetHead();
	atDNode<netGameEvent*, datBase> *pNextNode = NULL;

    while (pNode)
    {
		netGameEvent* pExistingEvent = pNode->Data;
		pNextNode = pNode->GetNext();

        if (*pExistingEvent == pEvent)
        {
#if ENABLE_NETWORK_LOGGING
            NetworkLogUtils::WriteLogEvent(GetLog(), "PREPARING_EVENT", "%s", pEvent->GetEventName());
            pEvent->WriteEventToLogFile(true, true);

            GetLog().WriteDataValue("DESTROY", "An equivalent event (%s_%d) already exists on the queue", pNode->Data->GetEventName(), pNode->Data->GetEventId().GetID());
#endif // ENABLE_NETWORK_LOGGING

            delete pEvent;
            return;
        }
		else if (pExistingEvent->IsSequential() && pExistingEvent->GetEventType() == pEvent->GetEventType() && pExistingEvent != ms_EventBeingProcessed)
		{
#if ENABLE_NETWORK_LOGGING
			NetworkLogUtils::WriteLogEvent(GetLog(), "FLAGGING_EVENT_FOR_REMOVAL", "%s", pExistingEvent->GetEventName());
			pEvent->WriteEventToLogFile(true, true);

			GetLog().WriteDataValue("Reason", "This is an old sequential event");
#endif // ENABLE_NETWORK_LOGGING

			pExistingEvent->FlagForRemoval();
		}

        pNode = pNextNode;
    }

    // find a free event id for this event
    gnetAssert(m_EventIDSearchStart < MAX_NUM_EVENT_IDS);

	int numEvents = MAX_NUM_EVENT_IDS;
	int currEvent = m_EventIDSearchStart;

	while (numEvents > 0)
	{
		if (m_EventIds[currEvent].IsFree())
		{
			// we've found a free event ID, so associated it with
			// the new event.
			pEvent->SetEventId( &m_EventIds[currEvent] );

			m_EventIDSearchStart  = (currEvent+1)%MAX_NUM_EVENT_IDS;
			break;
		}
		else
		{
			currEvent = (currEvent+1)%MAX_NUM_EVENT_IDS;
		}

		numEvents--;
	}
 
	if (numEvents==0)
    {
        delete pEvent;

        Quitf(ERR_NET_EVENT,"Network event manager ran out of event ids");

        return;
    }
 
    NetworkLogUtils::WriteLogEvent(GetLog(), "PREPARING_EVENT", "%s_%d", pEvent->GetEventName(), pEvent->GetEventId().GetID());

	if(pEvent->GetEventType() < netGameEvent::NETEVENTTYPE_MAXTYPES)
	{
		m_EventTriggerCounters[pEvent->GetEventType()] += 1; // Increment counter
	}

	// flag all connections that the event needs to be sent over in the event's id
	unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *player = allPhysicalPlayers[index];

		if (!player->IsLeaving() && pEvent->IsInScope(*player))
		{
			if (Verifyf(!player->IsLocal(), "Event %s_%d is in scope with local player!", pEvent->GetEventName(), pEvent->GetEventId().GetID()))
			{
				pEvent->GetEventId().SetPlayerInScope(player->GetPhysicalPlayerIndex());
				GetLog().Log("\t\t>> %s (%d)\r\n", player->GetLogName(), pEvent->GetEventId().GetSendSequence(player->GetPhysicalPlayerIndex()));
			}
		}
	}

#if ENABLE_NETWORK_LOGGING
    pEvent->WriteEventToLogFile(true, true);

    if (!pEvent->GetEventId().IsWaitingForAck())
    {
		GetLog().WriteDataValue("Player's In scope of event", "NONE");
    }
#endif // ENABLE_NETWORK_LOGGING

    // only use the event if it is in scope with someone, otherwise destroy it
    // host events must still go on the queue if there is no host as they will eventually be sent to the new one
    // (unless they deliberately set themselves as out of scope - eg the RequestPickup event)
    if (!pEvent->GetEventId().IsWaitingForAck() && !pEvent->MustPersistWhenOutOfScope())
    {
        NetworkLogUtils::WriteLogEvent(GetLog(), "DESTROYING_EVENT", "%s_%d", pEvent->GetEventName(), pEvent->GetEventId().GetID());
        GetLog().WriteDataValue("Reason", "No players left in scope");

        delete pEvent;
    }
    else
    {
		GetLog().WriteDataValue("Event", "Added to event list");

        // add to event list to wait until ack/reply- the event will be sent next update
        atDNode<netGameEvent*, datBase> *pNode = rage_new atDNetEventNode;

        if (gnetVerify(pNode))
        {
            pNode->Data = pEvent;
            m_EventList.Append(*pNode);
        }
        else
        {
            delete pEvent;
        }
    }
}

void netEventMgr::SendEvent(netGameEvent* pEvent)
{
    gnetAssert(pEvent);

    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *player = allPhysicalPlayers[index];

        if (pEvent->GetEventId().MustSendToPlayer(player->GetPhysicalPlayerIndex()))
        {
            if (!pEvent->HasAlreadyBeenPacked(*player) && Verifyf(!player->IsLocal(), "Trying to send event %s_%d to local player!", GetEventNameFromType(pEvent->GetEventType()), pEvent->GetEventId().GetID()))
            {
                datBitBuffer msgBuffer;
                msgBuffer.SetReadWriteBits(m_MessageBuffer, MAX_MESSAGE_PAYLOAD_BITS, 0);

                // prepares the data to be sent
                pEvent->Prepare(msgBuffer, *player);

#if ENABLE_NETWORK_LOGGING
                // store cursor pos at start of extra data
                int cursorPos = msgBuffer.GetCursorPos();
#endif // ENABLE_NETWORK_LOGGING

                // Some events have some extra data tagged on (eg a GiveControl event contains the migration data for the object)
                pEvent->PrepareExtraData( msgBuffer, false, *player);

                unsigned    eventIDSequence   = pEvent->GetEventId().GetSendSequence(player->GetPhysicalPlayerIndex());
                netSequence eventTypeSequence = pEvent->IsSequential() ? pEvent->GetEventSequence() : 0;
                AddEvent(*player, *pEvent->GetOwnerPlayer(), pEvent->GetEventType(), pEvent->GetEventId().GetID(), eventIDSequence, pEvent->IsSequential(), eventTypeSequence, (u8*) msgBuffer.GetReadWriteBits(), msgBuffer.GetCursorPos());

#if ENABLE_NETWORK_LOGGING
                // log the event data
                NetworkLogUtils::WriteLogEvent(GetLogSplitter(), "PACKING_EVENT", "%s_%d (%d)", GetEventNameFromType(pEvent->GetEventType()), pEvent->GetEventId().GetID(), eventIDSequence);

                msgBuffer.SetCursorPos(cursorPos);
                pEvent->WriteEventToLogFile(true, false, &msgBuffer);
                pEvent->WriteExtraDataToLogFile(msgBuffer, false);
                gnetAssert(msgBuffer.GetCursorPos()==cursorPos);
#endif // ENABLE_NETWORK_LOGGING

                pEvent->SetBeenSent();
                pEvent->SetHasAlreadyBeenPacked(*player);

                if(gnetVerifyf(pEvent->GetEventType() < netGameEvent::NETEVENTTYPE_MAXTYPES, "Sending an event with an invalid type!") &&
                   gnetVerifyf(player->GetPhysicalPlayerIndex() < MAX_NUM_PHYSICAL_PLAYERS,  "Sending an event to a player with an invalid physical index!"))
                {
                    m_LastTimeEventSent[pEvent->GetEventType()][player->GetPhysicalPlayerIndex()] = sysTimer::GetSystemMsTime();
                }
            }
        }
    }
}

void netEventMgr::SendAck(netGameEvent    *pEvent,
                          const netPlayer &toPlayer,
                          const netPlayer &fromPlayer,
                          const EventId    eventID,
                          const unsigned   eventIDSequence)
{
    AddEventAck(toPlayer, fromPlayer, eventID, eventIDSequence);

    NetworkLogUtils::WriteLogEvent(GetLogSplitter(), "PACKING_EVENT_ACK", "%s_%d (%d)", GetEventNameFromType(pEvent->GetEventType()), eventID, eventIDSequence);
}

void netEventMgr::SendReply(netGameEvent    *pEvent,
                            const netPlayer &toPlayer,
                            const netPlayer &fromPlayer,
                            const EventId    eventID,
                            const unsigned   eventIDSequence)
{
    datBitBuffer msgBuffer;
    msgBuffer.SetReadWriteBits(m_MessageBuffer, MAX_MESSAGE_PAYLOAD_BITS, 0);

    // prepares the data to be sent
    pEvent->PrepareReply( msgBuffer, toPlayer );

#if ENABLE_NETWORK_LOGGING
    // store cursor pos at start of extra data
    unsigned cursorPos = msgBuffer.GetCursorPos();
#endif // ENABLE_NETWORK_LOGGING

    // Some events have some extra data tagged on (eg a GiveControl event contains the migration data for the object)
    pEvent->PrepareExtraData( msgBuffer, true, toPlayer);

    AddEventReply(toPlayer, fromPlayer, eventID, eventIDSequence, pEvent->GetEventType(), (u8*) msgBuffer.GetReadWriteBits(), msgBuffer.GetCursorPos());

    // log the send reply event
    NetworkLogUtils::WriteLogEvent(GetLogSplitter(), "PACKING_EVENT_REPLY", "%s_%d (%d)", GetEventNameFromType(pEvent->GetEventType()), eventID, eventIDSequence);

#if ENABLE_NETWORK_LOGGING
    pEvent->WriteReplyToLogFile(true);
    msgBuffer.SetCursorPos(cursorPos);
    pEvent->WriteExtraDataToLogFile(msgBuffer, true);
#endif // ENABLE_NETWORK_LOGGING
}

bool netEventMgr::CheckForSpaceInPool(bool bCreateSpace)
{
    netGameEvent::Pool * pEventPool = netGameEvent::GetPool();

    if (pEventPool->GetNoOfFreeSpaces() == 0)
    {
        if (bCreateSpace)
        {
            gnetAssertf(0, "Warning - network event pool full, creating space");

			if (m_IsProcessingEventQueue)
			{
				gnetAssertf(0, "Error - could not create space for new event - processing event queue");
				return false;
			}
        }
        else
        {
            return false;
        }

        atDNode<netGameEvent*, datBase> *pNode     = m_EventList.GetHead();
        atDNode<netGameEvent*, datBase> *pNextNode = 0;

        while (pNode)
        {
            pNextNode = pNode->GetNext();

            netGameEvent* pEvent = pNode->Data;

            // find an event which can be removed
            if (gnetVerify(pEvent) && !pEvent->MustPersist())
            {
                NetworkLogUtils::WriteLogEvent(GetLog(), "DESTROYING_EVENT", "%s_%d", pEvent->GetEventName(), pEvent->GetEventId().GetID());
                GetLog().WriteDataValue("Reason", "Creating space in Q");

                delete pEvent;
                m_EventList.PopNode(*pNode);
                delete pNode;
                return true;
            }

            pNode = pNextNode;
        }

#if !__FINAL
		Displayf("** NET EVENT POOL FULL **");

		pNode = m_EventList.GetHead();

		while (pNode)
		{
			netGameEvent* pEvent = pNode->Data;

			if (pEvent)
			{
				Displayf("%s", pEvent->GetEventNameAndNumber());
			}

			pNode = pNode->GetNext();
		}
#endif // !__FINAL

        NETWORK_QUITF(0, "Network event pool is full");

        return false;
    }

    return true;
}

netSequence netEventMgr::GetNextSequence(const NetworkEventType eventType)
{
    gnetAssert(eventType <  netGameEvent::NETEVENTTYPE_MAXTYPES);

    netSequence nextSequence = 0;

    if(eventType <  netGameEvent::NETEVENTTYPE_MAXTYPES)
    {
        nextSequence = m_LocalEventTypeSequences[eventType];
        m_LocalEventTypeSequences[eventType]++;

        // check for and handle wrap
        if(m_LocalEventTypeSequences[eventType] == 0)
        {
            m_LocalEventTypeSequences[eventType]++;
        }
    }

    return nextSequence;
}

netGameEvent *netEventMgr::GetExistingEventFromEventList(const NetworkEventType eEventType)
{
    netGameEvent *existingEvent = 0;

    atDNode<netGameEvent*, datBase> *node = m_EventList.GetHead();

    while (node && !existingEvent)
    {
        netGameEvent *currentEvent = node->Data;

        if(currentEvent && currentEvent->GetEventType() == eEventType)
        {
            existingEvent = currentEvent;
        }

        node = node->GetNext();
    }

    return existingEvent;
}

void netEventMgr::DestroyAllEventsOfType(const NetworkEventType eEventType)
{
    NetworkLogUtils::WriteLogEvent(GetLog(), "DESTROYING_ALL_EVENTS_OF_TYPE", GetEventNameFromType(eEventType));

    atDNode<netGameEvent*, datBase> *node     = m_EventList.GetHead();
    atDNode<netGameEvent*, datBase> *nextNode = 0;

    while (node)
    {
        nextNode = node->GetNext();

        netGameEvent *currentEvent = node->Data;

        if(currentEvent && currentEvent->GetEventType() == eEventType)
        {
            NetworkLogUtils::WriteLogEvent(GetLog(), "FLAGGING_EVENT_FOR_REMOVAL", "%s_%d\r\n", GetEventNameFromType(currentEvent->GetEventType()), currentEvent->GetEventId().GetID());
			GetLog().WriteDataValue("Reason", "DestroyAllEventsOfType");

			currentEvent->GetEventId().ClearPlayersInScope();

			currentEvent->FlagForRemoval();
		}

        node = nextNode;
    }
}

atDNode<netGameEvent*, datBase> *netEventMgr::GetEventListHead()
{
    return m_EventList.GetHead();
}

unsigned netEventMgr::GetTimeSinceEventSentToPlayer(const NetworkEventType eventType, PhysicalPlayerIndex playerIndex) const
{
    if(gnetVerifyf(eventType   < netGameEvent::NETEVENTTYPE_MAXTYPES, "Invalid event type!") &&
       gnetVerifyf(playerIndex < MAX_NUM_PHYSICAL_PLAYERS,            "Invalid physical player index!"))
    {
        unsigned timeLastSent = m_LastTimeEventSent[eventType][playerIndex];
        unsigned currentTime  = sysTimer::GetSystemMsTime();

        if(currentTime >= timeLastSent)
        {
            return currentTime - timeLastSent;
        }
    }

    return 0;
}

void netEventMgr::ResetAllEventsAlreadyBeenPackedFlags(const netPlayer& player)
{
    atDNode<netGameEvent*, datBase> *node = m_EventList.GetHead();

    while (node)
    {
        netGameEvent *networkEvent = node->Data;

        if(networkEvent)
        {
            networkEvent->ResetHasAlreadyBeenPacked(player);
        }

        node = node->GetNext();
    }
}

void netEventMgr::AddEvent(const netPlayer&           toPlayer,
                           const netPlayer&           fromPlayer,
                           const NetworkEventType     eventType,
                           const EventId              eventID,
                           const unsigned             eventIDSequence,
                           bool                       isSequential,
                           netSequence                eventTypeSequence,
                           u8                        *eventData,
                           const unsigned             eventDataSize)
{
	if (gnetVerify(toPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
	   bool success = m_PackedEventsMsgs[toPlayer.GetPhysicalPlayerIndex()].AddEvent(fromPlayer, eventType, eventID, eventIDSequence, isSequential, eventTypeSequence, eventData, eventDataSize);

		if(!success)
		{
			SendPackedEventsMessage(toPlayer);
			success = m_PackedEventsMsgs[toPlayer.GetPhysicalPlayerIndex()].AddEvent(fromPlayer, eventType, eventID, eventIDSequence, isSequential, eventTypeSequence, eventData, eventDataSize);
			gnetAssertf(success, "Failed to send packed events message to a player!");
		}
		else
		{
			// record the event header data out
			BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_EventHeaderRecId, CMsgPackedEvents::GetEventSize(0, isSequential)));

			// record the event data out
			BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_EventRecorderIds[eventType], eventDataSize));
		}
	}
}

void netEventMgr::AddEventAck(const netPlayer       &toPlayer,
                              const netPlayer       &fromPlayer,
                              const EventId          eventID,
                              const unsigned         eventIDSequence)
{
	if (gnetVerify(toPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		bool success = m_PackedEventReliablesMsgs[toPlayer.GetPhysicalPlayerIndex()].AddEventAck(fromPlayer, eventID, eventIDSequence);

		if(!success)
		{
			SendPackedEventReliablesMessage(toPlayer);
			success = m_PackedEventReliablesMsgs[toPlayer.GetPhysicalPlayerIndex()].AddEventAck(fromPlayer, eventID, eventIDSequence);
			gnetAssertf(success, "Failed to send packed event reliables message to a player!");
		}
		else
		{
			// record the ack data out
			BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_AckMsgRecId, CMsgPackedEventReliablesMsgs::GetEventAckSize()));
		}
	}
}

void netEventMgr::AddEventReply(const netPlayer       &toPlayer,
                                const netPlayer       &fromPlayer,
                                const EventId          eventID,
                                const unsigned         eventIDSequence,
                                const NetworkEventType BANK_ONLY(eventType),
                                u8                    *eventReplyData,
                                const unsigned         eventReplyDataSize)
{
	if (gnetVerify(toPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		bool success = m_PackedEventReliablesMsgs[toPlayer.GetPhysicalPlayerIndex()].AddEventReply(fromPlayer, eventID, eventIDSequence, eventReplyData, eventReplyDataSize);

		if(!success)
		{
			SendPackedEventReliablesMessage(toPlayer);
			success = m_PackedEventReliablesMsgs[toPlayer.GetPhysicalPlayerIndex()].AddEventReply(fromPlayer, eventID, eventIDSequence, eventReplyData, eventReplyDataSize);
			gnetAssertf(success, "Failed to send packed event reliables message to a player!");
		}
		else
		{
			// record the reply header data out
			BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_ReplyMsgHeaderRecId, CMsgPackedEventReliablesMsgs::GetEventReplySize(0)));

			// record the reply data out
			BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_EventReplyRecorderIds[eventType], eventReplyDataSize));
		}
	}
}

void netEventMgr::SendPackedEventsMessage(const netPlayer& player)
{
    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	if (gnetVerify(playerIndex != INVALID_PLAYER_INDEX))
	{
	    if(m_PackedEventsMsgs[playerIndex].GetTotalPackedEventCount() > 0)
		{
			netSequence sequence = 0;
			netInterface::SendMsg(&player,
								  m_PackedEventsMsgs[playerIndex],
								  0,
								  &sequence,
                                  m_PackedEventsMsgs[playerIndex].GetOwnerPlayer());

            BANK_ONLY(m_NumUnreliableEventMessagesSent++);

			NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), false, sequence, player, "PACKED_EVENTS", "");

			GetLogSplitter().WriteDataValue("Num events", "%d", m_PackedEventsMsgs[playerIndex].m_Events.m_MessageCount);

			m_PackedEventsMsgs[playerIndex].Reset();
			ResetAllEventsAlreadyBeenPackedFlags(player);

			// record the packed events msg header
			BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_PackedEventsMsgHeaderRecId, m_PackedEventsMsgs[playerIndex].GetMessageHeaderBitSize()));
		}
	}
}

void netEventMgr::SendPackedEventReliablesMessage(const netPlayer& player)
{
	PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	if (gnetVerify(playerIndex != INVALID_PLAYER_INDEX))
    {
        if (m_PackedEventReliablesMsgs[playerIndex].GetTotalPackedMessageCount() > 0)
        {
            netSequence sequence = 0;
            netInterface::SendMsg(&player,
                                  m_PackedEventReliablesMsgs[playerIndex],
                                  NET_SEND_RELIABLE,
                                  &sequence,
                                  m_PackedEventReliablesMsgs[playerIndex].GetOwnerPlayer());

            BANK_ONLY(m_NumReliableEventMessagesSent++);

            NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), false, sequence, player, "PACKED_EVENTS_RELIABLES", "");

            GetLogSplitter().WriteDataValue("Num event acks", "%d", m_PackedEventReliablesMsgs[playerIndex].m_EventAcks.m_MessageCount);
            GetLogSplitter().WriteDataValue("Num event replies", "%d", m_PackedEventReliablesMsgs[playerIndex].m_EventReplies.m_MessageCount);

            m_PackedEventReliablesMsgs[playerIndex].Reset();

            // record the packed events msg header
            BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_PackedEventsMsgHeaderRecId, m_PackedEventsMsgs[playerIndex].GetMessageHeaderBitSize()));
        }
    }
}

void netEventMgr::ProcessPackedEvents(datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer)
{
    static const int bitsPerEvent = netGameEvent::SIZEOF_EVENTTYPE    +
                                    netEventID::SIZEOF_EVENT_ID       +
                                    netEventID::SIZEOF_EVENT_SEQUENCE +
                                    PackedEventData::SIZEOF_BUFFER_SIZE;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    while(bitsRemaining >= bitsPerEvent)
    {
        NetworkEventType                eventType           = netGameEvent::NETEVENTTYPE_MAXTYPES;
        EventId                         eventID             = 0;
        unsigned                        eventIDSequence     = 0;
        unsigned                        eventDataSizeInBits = 0;
        bool                            isSequential        = false;
        netSequence                     eventTypeSequence   = 0;

        messageBuffer.ReadUns(eventType,           netGameEvent::SIZEOF_EVENTTYPE);
        messageBuffer.ReadUns(eventID,             netEventID::SIZEOF_EVENT_ID);
        messageBuffer.ReadUns(eventIDSequence,     netEventID::SIZEOF_EVENT_SEQUENCE);
        messageBuffer.ReadUns(eventDataSizeInBits, PackedEventData::SIZEOF_BUFFER_SIZE);
        messageBuffer.ReadBool(isSequential);

        if(isSequential)
        {
            messageBuffer.ReadUns(eventTypeSequence, (sizeof(netSequence)<<3));
            gnetAssert(eventTypeSequence != 0);
        }

        u8 eventData[((1<<PackedEventData::SIZEOF_BUFFER_SIZE)>>3)+1];
        if(eventDataSizeInBits)
        {
            messageBuffer.ReadBits(eventData, eventDataSizeInBits, 0);
        }

        datBitBuffer eventDataMessageBuffer;
        eventDataMessageBuffer.SetReadOnlyBits(eventData, (eventDataSizeInBits+1), 0);

        ProcessEventData(fromPlayer, toPlayer, eventType, eventID, eventIDSequence, eventTypeSequence, eventDataMessageBuffer);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

void netEventMgr::ProcessPackedEventAcks(datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer)
{
    static const int bitsPerEventAck = netEventID::SIZEOF_EVENT_ID + netEventID::SIZEOF_EVENT_SEQUENCE;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    while(bitsRemaining >= bitsPerEventAck)
    {
        EventId  eventID         = 0;
        unsigned eventIDSequence = 0;
        messageBuffer.ReadUns(eventID,         netEventID::SIZEOF_EVENT_ID);
        messageBuffer.ReadUns(eventIDSequence, netEventID::SIZEOF_EVENT_SEQUENCE);

        ProcessEventAckData(fromPlayer, toPlayer, eventID, eventIDSequence);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

void netEventMgr::ProcessPackedEventReplies(datBitBuffer &messageBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer)
{
    static const int bitsPerEventReply = netEventID::SIZEOF_EVENT_ID + netEventID::SIZEOF_EVENT_SEQUENCE + PackedEventData::SIZEOF_BUFFER_SIZE;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    while(bitsRemaining >= bitsPerEventReply)
    {
        EventId  eventID                  = 0;
        unsigned eventIDSequence          = 0;
        unsigned eventReplyDataSizeInBits = 0;

        messageBuffer.ReadUns(eventID,                  netEventID::SIZEOF_EVENT_ID);
        messageBuffer.ReadUns(eventIDSequence,          netEventID::SIZEOF_EVENT_SEQUENCE);
        messageBuffer.ReadUns(eventReplyDataSizeInBits, PackedEventData::SIZEOF_BUFFER_SIZE);

        u8 eventReplyData[((1<<PackedEventData::SIZEOF_BUFFER_SIZE)>>3)+1];
        if(eventReplyDataSizeInBits)
        {
            messageBuffer.ReadBits(eventReplyData, eventReplyDataSizeInBits, 0);
        }

        datBitBuffer eventReplyDataMessageBuffer;
        eventReplyDataMessageBuffer.SetReadOnlyBits(eventReplyData, (eventReplyDataSizeInBits+1), 0);
        ProcessEventReplyData(fromPlayer, toPlayer, eventID, eventIDSequence, eventReplyDataMessageBuffer);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

void netEventMgr::ProcessEventData(const netPlayer &fromPlayer,
                                   const netPlayer &toPlayer,
                                   NetworkEventType eventType,
                                   EventId          eventID,
                                   unsigned         eventIDSequence,
                                   netSequence      eventTypeSequence,
                                   datBitBuffer    &eventDataMessageBuffer)
{
	gnetAssertf(eventType < netGameEvent::NETEVENTTYPE_MAXTYPES, "Invalid assert id %d, max value is %d", eventType, netGameEvent::NETEVENTTYPE_MAXTYPES);
	if (eventType >= netGameEvent::NETEVENTTYPE_MAXTYPES)
		return;

	// record the event header data in
    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_EventHeaderRecId, CMsgPackedEvents::GetEventSize(0, eventTypeSequence!=0)));

    // record the event data in
    BANK_ONLY(netBandwidthStatsReadCollector readCollector(m_BandwidthStats, m_EventRecorderIds[eventType], eventDataMessageBuffer));

	NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, static_cast<netSequence>(eventIDSequence), fromPlayer, "RECEIVED_EVENT", "%s_%d (%d)", GetEventNameFromType(eventType), eventID, eventIDSequence);

	if (fromPlayer.GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX)
	{
		GetLog().WriteDataValue("IGNORED", "Player does not have a valid physical player index");
		return;
	}

    if (toPlayer.IsMyPlayer() && m_EventIds[eventID].AlreadyProcessed(fromPlayer.GetPhysicalPlayerIndex(), eventIDSequence))
    {
        GetLog().WriteDataValue("IGNORED", "Handled already");
        return;
    }

    if(m_EventHandlers[eventType] == 0)
    {
        GetLog().WriteDataValue("IGNORED", "No event handler registered!");
        return;
    }
    else
    {
        (*m_EventHandlers[eventType])(eventDataMessageBuffer, fromPlayer, toPlayer, eventTypeSequence, eventID, eventIDSequence);
    }
}

void netEventMgr::ProcessEventAckData(const netPlayer &fromPlayer, const netPlayer &UNUSED_PARAM(toPlayer), EventId eventID, unsigned eventIDSequence)
{
    // record the ack data in
    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_AckMsgRecId, CMsgPackedEventReliablesMsgs::GetEventAckSize()));

	if (!gnetVerify(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		GetLog().WriteDataValue("IGNORED", "Player does not have a valid physical player index");
		return;
	}

	atDNode<netGameEvent*, datBase> *node = m_EventList.GetHead();

    while(node)
    {
        netGameEvent *networkEvent = node->Data;

        if (networkEvent->GetEventId() == eventID)
        {
            NetworkLogUtils::WriteLogEvent(GetLogSplitter(), "RECEIVED_EVENT_ACK", "%s_%d (%d)\r\n", GetEventNameFromType(networkEvent->GetEventType()), eventID, eventIDSequence);

            networkEvent->GetEventId().AckedByPlayer(fromPlayer.GetPhysicalPlayerIndex());

            if (!networkEvent->GetEventId().IsWaitingForAck())
            {
                NetworkLogUtils::WriteLogEvent(GetLog(), "FLAGGING_EVENT_FOR_REMOVAL", "%s_%d\r\n", GetEventNameFromType(networkEvent->GetEventType()), eventID);
				GetLog().WriteDataValue("Reason", "ProcessEventAckData - no remaining acks to wait for");
				networkEvent->FlagForRemoval();
            }

            return;
        }

        node = node->GetNext();
    }

    NetworkLogUtils::WriteLogEvent(GetLogSplitter(), "RECEIVED_EVENT_ACK", "?_%d (%d)\r\n", eventID, eventIDSequence);
}

void netEventMgr::ProcessEventReplyData(const netPlayer &fromPlayer, const netPlayer &toPlayer, EventId eventID, unsigned eventIDSequence, datBitBuffer &eventReplyDataMessageBuffer)
{
    // record the reply header data in
    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_ReplyMsgHeaderRecId, CMsgPackedEventReliablesMsgs::GetEventReplySize(0)));

	if (!gnetVerify(fromPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		GetLog().WriteDataValue("IGNORED", "Player does not have a valid physical player index");
		return;
	}

	// find the corresponding event for this reply in the event list
    atDNode<netGameEvent*, datBase> *node = m_EventList.GetHead();

    while(node)
    {
        netGameEvent *networkEvent = node->Data;

        if (networkEvent->GetEventId() == eventID)
        {
			NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, static_cast<netSequence>(eventIDSequence), fromPlayer, "RECEIVED_EVENT_REPLY", "%s_%d (%d)", GetEventNameFromType(networkEvent->GetEventType()), eventID, eventIDSequence);

            // record the event reply data in
            BANK_ONLY(netBandwidthStatsReadCollector readCollector(m_BandwidthStats, m_EventReplyRecorderIds[networkEvent->GetEventType()], eventReplyDataMessageBuffer));

            ms_EventBeingProcessed = networkEvent;

            networkEvent->HandleReply(eventReplyDataMessageBuffer, fromPlayer, toPlayer);
#if ENABLE_NETWORK_LOGGING
            networkEvent->WriteReplyToLogFile(false);
#endif // ENABLE_NETWORK_LOGGING

             // Some events have some extra data tagged on (eg a GiveControl event contains the migration data for the object)
            networkEvent->HandleExtraData(eventReplyDataMessageBuffer, true, fromPlayer, toPlayer);
 
            networkEvent->GetEventId().AckedByPlayer(fromPlayer.GetPhysicalPlayerIndex());

            if (!networkEvent->GetEventId().IsWaitingForAck())
            {
                NetworkLogUtils::WriteLogEvent(GetLog(), "FLAGGING_EVENT_FOR_REMOVAL", "%s_%d\r\n", GetEventNameFromType(networkEvent->GetEventType()), eventID);
				GetLog().WriteDataValue("Reason", "ProcessEventReplyData - no remaining acks to wait for");
				networkEvent->FlagForRemoval();
            }

            ms_EventBeingProcessed = 0;

            return;
        }

        node = node->GetNext();
    }

	NetworkLogUtils::WriteMessageHeader(GetLogSplitter(), true, static_cast<netSequence>(eventIDSequence), fromPlayer, "RECEIVED_EVENT_REPLY", "?_%d (%d)", eventID, eventIDSequence);

    GetLog().WriteDataValue("IGNORED", "Reply has already been received\r\n");
}

} // namespace rage
