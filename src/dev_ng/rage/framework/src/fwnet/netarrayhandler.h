// 
// networkArrayHandler.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_ARRAY_HANDLER_H
#define NET_ARRAY_HANDLER_H

// rage headers
#include "atl/array.h"
#include "atl/inlist.h"
#include "net/netsequence.h"

// framework headers
#include "fwnet/netserialisers.h"

// the sync data size must includes space for the elements data and a guard bit for each element
#define CALCULATE_ARRAY_SYNC_DATA_SIZE(NumElements, MaxElementSizeInBits) (BITS_TO_BYTES(MaxElementSizeInBits*NumElements + NumElements))

namespace rage
{
class datBitBuffer;
class netLoggingInterface;
class netPlayer;
class netSyncDataBase;

typedef u16 SplitUpdateFlags;

//PURPOSE
//	Used to identify an array handler. Identifiers are required when there are multiple array handlers of the same type.
//  Array handlers which use identifiers must derive their identifier from this class.
class netArrayIdentifierBase
{
public:

	netArrayIdentifierBase() {}
	virtual ~netArrayIdentifierBase() {}

	// PURPOSE
	//	Resets the identifier. Precedes a Read() call. 
	//PARAMS
	//	player - the player who sent the data we are about to read into the identifier
	virtual void		Reset(const netPlayer* player) = 0;

	// PURPOSE
	//	Read the identifier data from the given bitbuffer.
	virtual void		Read(datBitBuffer& bitBuffer) = 0;

	//PURPOSE
	//	Writes the identifier data to the given bitbuffer.
	virtual void		Write(datBitBuffer& bitBuffer) = 0;

	//PURPOSE
	//	Returns the size of the identifier data in bits.
	virtual unsigned	GetSize()=0;

	//PURPOSE
	//	Returns the log name of the identifier
	virtual const char*	GetLogName() const =0;

	virtual bool operator==(const netArrayIdentifierBase& identifier) const = 0;

protected:

#if __BANK
	static const unsigned LOG_NAME_LEN = 100;
	static char ms_logName[LOG_NAME_LEN];
#endif

private:

	netArrayIdentifierBase(netArrayIdentifierBase&);
	netArrayIdentifierBase& operator=(netArrayIdentifierBase&);

	bool operator==(netArrayIdentifierBase& identifier);
	bool operator==(netArrayIdentifierBase* identifier);
	bool operator==(const netArrayIdentifierBase* identifier);
};

//PURPOSE
//  An array handler deals with syncing an array (or any contiguous block of data) across the network.
//
//  The handler contains an instance of netSyncData, which is used to determine which 
//	elements of the array have changed (become dirty). The data in these elements is then
//	sent out to all the players the array is synced with. This data is sent reliably: it 
//	will be periodically resent until acknowledged by all players.
//
//	Ownership of an array can migrate and there can be multiple array handlers of the same
//	type. These are identified using an arrayIdentifier.
//

class netArrayHandlerBase
{
public:

	//PARAMS
	//  handlerType			 - The enumerated type of the array handler
	//  numElements			 - The number of elements in the array.
	//	maxNumSyncedPlayers	 - The maximum number of players that this array will be synced with.
	// 	emptyArrayOnShutdown - If set, all the array elements are emptied when the array handler is shut down.
    netArrayHandlerBase(NetworkArrayHandlerType handlerType, unsigned numElements, unsigned maxNumSyncedPlayers = MAX_NUM_PHYSICAL_PLAYERS);

	virtual ~netArrayHandlerBase() {}

	//PURPOSE
	//	Returns the type of the handler.
	NetworkArrayHandlerType GetHandlerType() const { return m_HandlerType; }
	
	//PURPOSE
	//	Returns the number of elements in the array.
	unsigned GetNumArrayElements() const { return m_NumArrayElements; }

		//PURPOSE
	//	Returns the number of elements in the array that are in use.
	unsigned GetNumElementsInUse() const { return m_NumElementsInUse; }

	//PURPOSE
	//	Returns the maximum number of players this array will be synced with
	unsigned GetMaxNumSyncedPlayers() const { return m_MaxNumSyncedPlayers; }

	//PURPOSE
	//	Returns the maximum serialised size of an element index
	virtual u32 GetSizeOfElementIndex() const { return m_BitsizeOfIndex; }

	//PURPOSE
	//	Returns true if the array is waiting on acks for a split update from any players. The array cannot be dirtied if this is the case
	bool HasUnackedSplitUpdates() const { return m_unackedSplitUpdates; }

	//PURPOSE
	//	Initializes the handler. 
	//NOTES
	//	This involves initializing the sync data, current state and shadow buffers.
	virtual void Init();

	//PURPOSE
	//	Shuts down the handler.
	virtual void Shutdown();

	//PURPOSE
	//	Updates the handler.
	//NOTES
	//	Determines which elements are dirty by comparing the current state and shadow buffers.
	virtual void Update();

	//PURPOSE
	//	Informs the handler that a new player has joined the session.
	virtual void PlayerHasJoined(const netPlayer& joiningPlayer);

	//PURPOSE
	//	Informs the handler that a player has left the session.
	virtual void PlayerHasLeft(const netPlayer& leavingPlayer);

	//PURPOSE
	//	Called when the session host changes
	virtual void NewHost() {}

	//PURPOSE
	// gets the array manager update batch that this handler belongs to 
	int GetArrayManagerBatch() const { return static_cast<int>(m_arrayMgrBatch); }

	//PURPOSE
	// sets the array manager update batch that this handler belongs to 
	void SetArrayManagerBatch(int batch) { m_arrayMgrBatch = static_cast<s8>(batch); }

	//PURPOSE
	// returns true if the broadcast data needs to be updated very frequently. 
	virtual bool RequiresHighFrequencyUpdates() { return false; }

	//PURPOSE
	//	Returns the maximum size of an element in bytes
	unsigned GetMaxElementSizeInBytes() const { return m_MaxSizeOfElementInBits>>3; }

	//PURPOSE
	//	Returns the maximum size of a handler update
	unsigned GetMaxUpdateSizeInBytes() const { return GetMaxElementSizeInBytes() * m_NumElementsInUse; }

	//PURPOSE
	//	Returns the total size of all the update data waiting to be sent to the given player, and calculates the number of update messages
	//  that the update data will have to be sent in.
	//PARAMS		
	//  player				- The player the update message will be sent to.
	//  messageSize			- The size of the update message in bits.
	//  numUpdates			- The number of update messages needed to send the update. 
	//  updateStartElements - The first elements of each update  
	virtual unsigned GetTotalSizeOfUpdateData(const netPlayer& player, unsigned messageSize, unsigned &numUpdates, u16 updateStartElements[]);

	//PURPOSE
	//	Returns the size of the update data that will be sent in the current update message.
	//PARAMS		
	//  player		   - The player the update message will be sent to.
	//  messageSize	   - The size of the update message in bits.
	//  startElement   - The element to start the calculation from.
	//  logSizes	   - If true, the size of the elements are logged
	virtual unsigned GetSizeOfMessageUpdateData(const netPlayer& player, unsigned messageSize, unsigned startElement, bool logSizes = false);

	//PURPOSE
	//  Writes dirty or unacked elements to a bit buffer to be sent in an update message to a remote player.
	//PARAMS		
	//  player			- The player the update message will be sent to.
	//  bitBuffer		- The bit buffer in the update message.
	//  updateSeq		- The sequence number of this update
	//  currentElement  - The next element to be (potentially) written. Incremented by the function. Only used in split updates
	//  logSizes		- 
	virtual void WriteUpdate(const netPlayer& player, datBitBuffer& bitBuffer, netSequence updateSeq, unsigned& currentElement, bool logSizes = false);

	//PURPOSE
	//	Reads a bit buffer received in an update message from a remote player.
	//PARAMS		-
	//  player		- The player who sent the update message.
	//  bitBuffer	- The bit buffer in the update message.
	//  dataSize	- The size of the update data to be read.
	//  updateSeq	- The sequence number of this update
	//RETURNS
	//	True if the data was successfully applied to the array.
	virtual bool ReadUpdate(const netPlayer& player, datBitBuffer& bitBuffer, unsigned dataSize, netSequence updateSeq);

	//PURPOSE
	//	Handles an ack received for an array elements update message sent to another player.
	//PARAMS
	//	player		         - The player who sent the ack.
	//	timeAckedMessageSent - the time the message being acked was sent
	//  firstElement		 - the first element sent in the update
	//  lastElement			 - the last element sent in the update
	void HandleAck(const netPlayer& player, const unsigned timeAckedMessageSent, unsigned firstElement, unsigned lastElement);

	//PURPOSE
	//	Handles an ack received for an array elements update message sent to another player.
	//PARAMS
	//	player		    - The player who sent the ack.
	//	updateSeq		- the sequence number of the update being acked
	//	splitUpdateNum	- the number of the split update being acked
	//	timeAckedMessageSent - the time the message being acked was sent
	void HandleSplitUpdateAck(const netPlayer& player, const netSequence updateSeq, const unsigned splitUpdateNum, const unsigned timeAckedMessageSent);

	//PURPOSE
	//	Returns the sequence number of the next update to be sent to the given player.
	//PARAMS
	//	player		- The player who we are sending the update to
	//  bIncrement	- If set, the current update sequence is incremented
	netSequence GetNextUpdateSequence(const netPlayer& player, bool bIncrement);

	//PURPOSE
	//	Sets the sequence number of the last update received from the given player
	void SetLastReceivedUpdateSequence(const netPlayer& player, netSequence seq);

	//PURPOSE
	//	Returns true if the given player is up to date with the current contents of the array.
	virtual bool IsSyncedWithPlayer(const netPlayer& player) const;

	//PURPOSE
	//	Returns true if all players are up to date with the current contents of the array.
	bool IsSyncedWithAllPlayers() const;

	//PURPOSE
	//	Returns the flags for the players not up to date with the current contents of the array.
	PlayerFlags GetUnsyncedPlayers() const;

	//PURPOSE
	//	Returns a bitmask of all the remote players in scope of this array.
	virtual PlayerFlags GetRemotePlayersInScopeBitmask() const;

	//PURPOSE
	//	Returns true if the given element is up to date on the given player's machine (i.e. not dirty or unacked).
	bool IsElementSyncedWithPlayer(unsigned index, unsigned playerSyncDataIndex) const;

	//PURPOSE
	//	Returns true if the given element is up to date on all remote players
	bool IsElementSyncedWithAllPlayers(unsigned index) const;

	//PURPOSE
	//	Returns true if an update message can be sent to the given player. Some handlers may want to delay sending.
	virtual bool CanSendUpdate(const netPlayer& UNUSED_PARAM(player)) const { return true; }

	//PURPOSE
	//	Returns true if the data received in an update message can be processed by the array.
	//PARAMS
	//	player		- the player we got the update from.
	//  updateSeq	- the sequence number of the update
	//  splitUpdate - if set this update is part of a split update
	virtual bool CanProcessUpdate(const netPlayer& player, netSequence updateSeq, bool splitUpdate) const;

	//PURPOSE
	//	Flags an array element as being dirty (changed). Returns true if the element was successfully dirtied
	virtual bool SetElementDirty(unsigned elementIndex);

	//PURPOSE
	//	Flags all array elements as being dirty (changed)
	void DirtyAllElements();

	//PURPOSE
	//	Returns the identifier for this array. 
	virtual netArrayIdentifierBase* GetIdentifier()					{ return NULL; }		 
	virtual const netArrayIdentifierBase* GetIdentifier() const 	{ return NULL; }

	//PURPOSE
	//	Returns the static identifier for this array type. 
	//NOTES
	//	Static identifiers are used to find an array when reading an array update. 
	//  If an array has an identifier it MUST have a corresponding static identifier.
	virtual netArrayIdentifierBase* GetStaticIdentifier()			{ return NULL; } 

	//PURPOSE
	// Some handlers can be initially created without being associated with any array data
	virtual bool HasArrayData() const								{ return true; }

#if ENABLE_NETWORK_LOGGING
	virtual const void* GetArrayData() const						{ return 0; } // used for logging only
#endif
	//PURPOSE
	//  Returns whether this array is shared, which means different elements can be
	//  shared by different machines
	virtual bool IsSharedArray() const                              { return false; }

	//PURPOSE
	//  Returns whether this array must received cache split updates and apply them all at once. This is for arrays where elements are potentially dependent on 
	// each other (eg script broadcast data). *NB* this feature is currently only supported for arrays that use sync data buffers.
	virtual bool CacheSplitUpdates() const                              { return false; }

	//PURPOSE
	//  Returns whether this array must be manually dirtied externally, this is when the sync data contains no current or shadow buffers to detect dirty elements 
	// automatically
	bool IsManualDirtyArray();

	//PURPOSE
	//	Returns true if our machine dictates the contents of the given element
	//NOTES
	//	Only used by shared arrays in which different elements can be controlled by different machines. 
	virtual bool IsElementLocallyArbitrated(unsigned UNUSED_PARAM(index)) const 
	{ 
		return IsArrayLocallyArbitrated(); 
	}

	//PURPOSE
	//	Returns true if our machine can start arbitrating over the given element
	//NOTES
	//	Only used by shared arrays in which different elements can be controlled by different machines. 
	virtual bool IsElementRemotelyArbitrated(unsigned UNUSED_PARAM(index)) const 
	{
		return !IsArrayLocallyArbitrated(); 
	}

	//PURPOSE
	//	Called when the arbitration changes on this array
	virtual void HandleChangeOfArbitration(bool bHadPreviousArbitrator);
	
    //PURPOSE
    // Returns whether all elements in the array are alway in scope with all players
    virtual bool AreElementsAlwaysInScope() const { return true; }

	//PURPOSE
	//	Returns true if the element is in scope with the given player and therefore needs to send updates to that player.
	virtual bool IsElementInScope(unsigned UNUSED_PARAM(index), const netPlayer& UNUSED_PARAM(player)) const { return true; }

	//PURPOSE
	// Sends a checksum of the current state of the array data to the new arbitrator of the array
	virtual void SendChecksumToNewArbitrator();

	//PURPOSE
	//	Processes a checksum of the array data received from another player. If the checksum differs from our current one then
	// all of the dirty array data is resent to the player.
	virtual bool VerifyChecksum(const netPlayer& player, unsigned checksum);

	// PURPOSE
	// Sets the unacked split update flags for the given player
	void SetUnackedSplitUpdateFlags(const netPlayer& player, u32 numUpdates);

	// PURPOSE
	// Returns the unacked split update flags for the given player
	SplitUpdateFlags GetUnackedSplitUpdateFlags(const netPlayer& player) const;

	// PURPOSE
	// Resets the unacked split update flags for the given player
	void ResetUnackedSplitUpdateFlagsForPlayer(const netPlayer& player);

	// ========================== PUBLIC PURE VIRTUAL FUNCTIONS ====================================

	//PURPOSE
	//	Returns the name of the handler (used for logging)
	virtual const char* GetHandlerName() const = 0;

	//PURPOSE
	//	Returns true if our machine arbitrates the contents of the array.
	virtual bool IsArrayLocallyArbitrated() const = 0;

	//PURPOSE
	//	Returns the player that arbitrates over the contents of the array (can be null)
	virtual const netPlayer* GetPlayerArbitratorOfArray() const = 0;

	//PURPOSE
	//	Returns the maximum size of the serialised element data in bits.
	virtual unsigned GetMaxElementSizeInBits() = 0;

	//PURPOSE
	//  Calculates the checksum of the array data
	virtual unsigned CalculateChecksum() const = 0;

	//PURPOSE
	//  Calculates the size of the array data in bytes
	virtual unsigned GetSizeOfArrayDataInBytes() const = 0;

protected:

	//PURPOSE
	//	Returns the sync data used to sync the array over the network.
	virtual netSyncDataBase* GetSyncData()				{ return NULL; }
	virtual const netSyncDataBase* GetSyncData() const { return NULL; }

	//PURPOSE
	//	Initialises the current state and shadow buffers with the current state of the array
	void InitialiseSyncDataBuffers();

	//PURPOSE
	//	Resets all sync data for the array
	void ResetSyncData();

	//PURPOSE
	//	Resets the sync data for empty elements that have been acked by all players. Returns true if any elements were reset
	void ResetEmptyElements();

	//PURPOSE
	//	Returns true if the given player requires updates from this handler.
	virtual bool IsPlayerInScope(const netPlayer&) const { return true; }

	//PURPOSE
	//	Returns true if the element is in scope with the given player and therefore needs to send updates to that player.
	virtual bool IsElementInScopeInternal(unsigned UNUSED_PARAM(index), const netPlayer& UNUSED_PARAM(player)) const { return true; }

	//PURPOSE
	//	Returns the index used to access the sync data associated with a network player, held within the handler. 
	//NOTES
	//	This is required when there is a subset of players the array is being synced between. In this case there is only sync data for 
	// those players and we need a way of mapping the player to his slot in the sync data.
	virtual unsigned GetSyncDataIndexForPlayer(const netPlayer& player) const;

	//PURPOSE
	//	Returns true if the initial state of the array is always full of empty elements. This is necessary to save on bandwidth. Once an element returns to being
	// empty and is synced with all players, it can be marked as never having being dirtied. This will prevent it being sent out to any players that join subsequently
	virtual bool AllElementsStartEmpty() const { return true; }

	//PURPOSE
	//	Returns true if some elements can be classed as empty
	virtual bool CanHaveEmptyElements() const { return false; }

	//PURPOSE
	//	Returns true if the given element is empty.
	virtual bool IsElementEmpty(unsigned UNUSED_PARAM(index)) const { return false; }

	//PURPOSE
	//	Empties the given element.
	virtual void SetElementEmpty(unsigned UNUSED_PARAM(index)) {}

	//PURPOSE
	//	Writes the element index to the given bitbuffer
	virtual void WriteElementIndex(datBitBuffer& bitBuffer, unsigned index);

	//PURPOSE
	//	Reads the element index to the given bitbuffer
	virtual void ReadElementIndex(datBitBuffer& bitBuffer, unsigned& index);

	//PURPOSE
	//	Returns true if the index (which has just been read) is valid
	virtual bool IsValidIndex(unsigned index);

	//PURPOSE
	//	Determines which elements have changed since this function was last called.
	virtual void RecalculateDirtyElements();

    //PURPOSE
    //  Resets the sync data for this element, clearing all dirty flags and
    //  last times sent and checked.
    //PARAMS
	//	index - the index of the array element.
    virtual void ResetElementSyncData(unsigned index);

	//PURPOSE
	//	Performs any special processing after the array has been read.
	virtual void DoPostReadProcessing() {}

	//PURPOSE
	//	Performs any special processing after an array element has been read.
	virtual void DoPostElementReadProcessing(unsigned UNUSED_PARAM(index)) {}

	//PURPOSE
	//	Returns true if the element data just read in an update message can be applied to the array itself.
	//PARAMS
	//	index			- the index of the array element.
	//	player			- the player who sent the update message.
	//  elementEmpty	- if true, the element is empty.
	virtual bool CanApplyElementData(unsigned index, const netPlayer& player, bool elementEmpty); 

	//PURPOSE
	// Returns the hash of the given buffer
	unsigned CalcHash(const char* buffer, unsigned dataSize) const;

	// ========================== PROTECTED PURE VIRTUAL FUNCTIONS ====================================

	//PURPOSE
	//	Retrieves the array data at the given index.
	virtual void ExtractDataForSerialising(unsigned index) = 0;

	//PURPOSE
	//	Writes the given element to the given bit buffer.
	//PARAMS
	//	bitBuffer	- The bit buffer, with cursor position correctly set for this element.
	//	index		- The index of the element.
	//	pLog		- If set, the data written will also be logged to this log.
	virtual void WriteElement(datBitBuffer& bitBuffer, unsigned index, netLoggingInterface* pLog = NULL) = 0;

	//PURPOSE
	//	Reads the given element from the given bit buffer.
	//PARAMS
	//	bitBuffer	- The bit buffer, with cursor position correctly set for this element.
	//	index		- The index of the element.
	//	pLog		- If set, the data read will also be logged to this log.
	virtual void ReadElement(datBitBuffer& bitBuffer, unsigned index, netLoggingInterface* pLog = NULL) = 0;

	//PURPOSE
	//	Logs the given element from data currently held in the array.
	virtual void LogElement(unsigned index, netLoggingInterface& log) = 0;

	//PURPOSE
	//	Returns the current size of the serialised data of the given element.
	virtual unsigned GetCurrentElementSizeInBits(unsigned index) = 0;

	//PURPOSE
	//	Applies the data just read for the given element to the array itself.
	//PARAMS
	//	index		- The index of the element.
	//	player		- The player we got the data from.
	virtual void ApplyElementData(unsigned index, const netPlayer& player) = 0;

    //PURPOSE
	//	Returns the cursor position of the given element in the current and shadow buffers, this includes the guard bit indicating whether the element is empty or not.
	unsigned GetElementCursorPos(unsigned index) const 
	{ 
		return m_ElementEmptyFlagsSize + (m_MaxSizeOfElementInBits*index);
	}

private:

    //PURPOSE
    // Returns whether the specified element in the array is different between the
    // current state and shadow buffers
    //PARAMS
    //  currentStateBuffer      - The current state buffer for the array handler
    //  shadowBuffer            - The shadow buffer for the array handler
	//	elementBufferByteOffset - The offset in the buffers in bytes of the element
    bool CompareElement(datBitBuffer &currentStateBuffer, datBitBuffer &shadowBuffer, unsigned elementBufferByteOffset);

public:

	// The list link used by the array handler list in the network array manager.
	inlist_node<netArrayHandlerBase> m_ListLink;

	// The list link used by the array handler update batch in the network array manager.
	inlist_node<netArrayHandlerBase> m_BatchListLink;

protected:

    static const unsigned EMPTY_FLAGS_BLOCK_SIZE = 8;

	// The last sequence numbers of array updates sent to each player. 
	atFixedArray<netSequence, MAX_NUM_PHYSICAL_PLAYERS> m_LastSentSequences;

	// The last sequence numbers of array updates received from each player. 
	atFixedArray<netSequence, MAX_NUM_PHYSICAL_PLAYERS> m_LastReceivedSequences;

	// Flags indicating which split updates are unacked for each player
	atFixedArray<SplitUpdateFlags, MAX_NUM_PHYSICAL_PLAYERS> m_UnackedSplitUpdateFlags;

	// the enumerated type of the handler
	NetworkArrayHandlerType m_HandlerType;

	// The number of elements in the array.
    u16 m_NumArrayElements;

	// The number of elements currently in use.
	u16 m_NumElementsInUse;

	// The maximum size of an array element in bits.
	u8 m_MaxSizeOfElementInBits;

	// The maximum number of players that this array will be synced with.
	u8 m_MaxNumSyncedPlayers;

	// The number of bits needed to write the index. 
	u8 m_BitsizeOfIndex;

    // The number of bits used to store the is element empty flags (byte aligned)
    u16 m_ElementEmptyFlagsSize;

	// A checksum of the array data, calculated when the array becomes locally controlled
	u32 m_arrayDataChecksum;

	// The array manager batch this handler belongs to
	s8 m_arrayMgrBatch;

	bool m_checksumCalculated;

	bool m_unackedSplitUpdates;
};


//========================================================================================================================================
// netArrayHandler
//========================================================================================================================================

//PURPOSE
//  Performs the actual element operations on the array (read, write, serialise, log, etc)
//
//	T - the type of the array elements
//	Derived - the name of the class derived from this class.
//
template<typename T, class Derived>
class netArrayHandler : public netArrayHandlerBase
{
public:

	//PARAMS
	//  handlerType			 - The enumerated type of the array handler
	//	arrayData			 - A pointer to the start of the array. 
	//  numElements			 - The number of elements in the array.
	//	maxNumSyncedPlayers	 - The maximum number of players that this array will be synced with.
	netArrayHandler(NetworkArrayHandlerType handlerType, 
					T* arrayData,
					unsigned numElements,
					unsigned maxNumSyncedPlayers = MAX_NUM_PHYSICAL_PLAYERS)
	: netArrayHandlerBase(handlerType, numElements, maxNumSyncedPlayers)
	, m_Array(arrayData)
	, m_arrayDataInvalid(false)
	{
	}

	//PURPOSE
	// Some handlers can be initially created without being associated with any array data
	virtual bool HasArrayData() const { return (m_Array != NULL); }

#if ENABLE_NETWORK_LOGGING
	virtual const void* GetArrayData() const { return (void*)m_Array; } // used for logging only
#endif

	//PURPOSE
	// Some handlers can be initially created without being associated with any array data
	virtual void SetArrayData(T* arrayData, unsigned ASSERT_ONLY(sizeOfArrayInBytes)) 
	{ 
		m_Array = arrayData; 
		
		Assert(!m_Array || (sizeOfArrayInBytes > 0 && sizeOfArrayInBytes <= GetMaxSizeOfArrayDataInBytes()));

		if (m_Array)
		{
			ResetSyncData();
			m_arrayDataChecksum = CalculateChecksum();
			ASSERT_ONLY(m_checksumCalculated = true);
		}

		m_arrayDataInvalid = false;
	}

	//PURPOSE
	//	Called when the array data the handler points to is to be freed. The array handler will then use the contents of the current state buffer
	// for any future sync updates.
	void SetArrayDataInvalid()
	{
		if (!m_arrayDataInvalid && IsArrayLocallyArbitrated())
		{
			// grab the current state of the array data one last time.
			RecalculateDirtyElements();
		}

		m_arrayDataInvalid = true;
	}

	bool IsArrayDataValid() const { return !m_arrayDataInvalid; }

	//PURPOSE
	//  Only updates the array when there is valid array data
	virtual void Update()
	{
		if (!m_arrayDataInvalid)
		{
			netArrayHandlerBase::Update();
		}
	}

	//PURPOSE
	//	Processes a checksum of the array data received from another player. If the checksum differs from our current one then
	// all of the dirty array data is resent to the player.
	virtual bool VerifyChecksum(const netPlayer& player, unsigned checksum)
	{
		bool bChecksumDiffers = false;

		if (!m_arrayDataInvalid)
		{
			bChecksumDiffers = netArrayHandlerBase::VerifyChecksum(player, checksum);
		}

		return bChecksumDiffers;
	}

	//PURPOSE
	//  Return the size of the array data in bytes
	unsigned GetMaxSizeOfArrayDataInBytes() const { return m_NumArrayElements*sizeof(T); }

	//PURPOSE
	//	The default serialiser, it assumes T has a serialiser. If not you need to define your own serialiser
	// in a derived class.
	void Serialise(CSyncDataBase& serialiser)
	{
		m_Element.Serialise(serialiser);
	}

	//PURPOSE
	//	Returns the maximum size of the serialised element data.
	virtual unsigned GetMaxElementSizeInBits()
	{
		CSyncDataSizeCalculator serialiser;
		Derived *derived = static_cast<Derived *>(this);
		derived->Serialise(serialiser);
		return serialiser.GetSize();
	}

protected:

	//PURPOSE
	//  Return the size of the array data in bytes
	virtual unsigned GetSizeOfArrayDataInBytes() const { return GetMaxSizeOfArrayDataInBytes(); }

	//PURPOSE
	//  Calculates the checksum of the array data
	unsigned CalculateChecksum() const
	{
		unsigned hash = 0;

		if (AssertVerify(m_Array) && !m_arrayDataInvalid)
		{
			hash = CalcHash(reinterpret_cast<const char *>(m_Array), GetSizeOfArrayDataInBytes());
		}

		return hash;
	}

	//PURPOSE
	//	Retrieves the array data at the given index.
	virtual void ExtractDataForSerialising(unsigned index)
	{
		if (m_arrayDataInvalid)
		{
			// use the contents of the current state buffer if the array data has become invalid
			GetSyncData()->GetCurrentStateBuffer()->SetCursorPos(GetElementCursorPos(index));
			ReadElement(*GetSyncData()->GetCurrentStateBuffer(), index);
		}
		else if (AssertVerify(m_Array))
		{
			m_Element = m_Array[index];
		}
	}

	//PURPOSE
	//	Writes the given element to the given bit buffer.
	//PARAMS
	//	bitBuffer	- The bit buffer, with cursor position correctly set for this element.
	//	index		- The index of the element.
	//	pLog		- If set, the data written will also be logged to this log.
	virtual void WriteElement(datBitBuffer& bitBuffer, unsigned index, netLoggingInterface* pLog = NULL)
	{
		ExtractDataForSerialising(index);
		CSyncDataWriter serialiser(bitBuffer, pLog);
		Derived *derived = static_cast<Derived *>(this);
		derived->Serialise(serialiser);
	}

	//PURPOSE
	//	Reads the given element from the given bit buffer.
	//PARAMS
	//	bitBuffer	- The bit buffer, with cursor position correctly set for this element.
	//	index		- The index of the element.
	//	pLog		- If set, the data read will also be logged to this log.
	virtual void ReadElement(datBitBuffer& bitBuffer, unsigned UNUSED_PARAM(index), netLoggingInterface* pLog = NULL)
	{
		CSyncDataReader serialiser(bitBuffer, pLog);
		Derived *derived = static_cast<Derived *>(this);
		derived->Serialise(serialiser);
	}

	//PURPOSE
	//	Logs the given element from data currently held in the array.
	virtual void LogElement(unsigned index, netLoggingInterface& log)
	{
		ExtractDataForSerialising(index);
		CSyncDataLogger serialiser(&log);
		Derived *derived = static_cast<Derived *>(this);
		derived->Serialise(serialiser);
	}

	//PURPOSE
	//	Returns the current size of the serialised element data, which may differ from the maximum size,
	// depending on the array.
	virtual unsigned GetCurrentElementSizeInBits(unsigned index)
	{
		if (IsElementEmpty(index))
		{
			return 0;
		}
		else if (GetSyncData() && GetSyncData()->GetCurrentStateBuffer())
		{
			return GetSyncData()->GetSyncDataUnit(index).GetSizeOfCurrentData();
		}
		else
		{
			ExtractDataForSerialising(index);
			CSyncDataSizeCalculator serialiser;
			serialiser.SetIsMaximumSizeSerialiser(false);
			Derived *derived = static_cast<Derived *>(this);
			derived->Serialise(serialiser);
			return serialiser.GetSize();
		}
	}

	//PURPOSE
	//	Applies the data just read for the given element to the array itself.
	//PARAMS
	//	index		- The index of the element.
	//	player		- The player we got the data from.
	virtual void ApplyElementData(unsigned index, const netPlayer& UNUSED_PARAM(player))
	{
		if (AssertVerify(m_Array) && !m_arrayDataInvalid)
		{
			m_Array[index] = m_Element;
		}
	}

protected:

	// A pointer to the array
	T*	m_Array;

	// Used to temporarily store the value of an element during serialisation.
	T	m_Element;

	// if this flag is set, the array data the handler points to has been freed. The handler can still remain active while it syncs with
	// other players, it has to use the contents of the current state buffer to do this.
	bool m_arrayDataInvalid;

};

} // namespace rage

#endif  // NET_ARRAY_HANDLER_H
