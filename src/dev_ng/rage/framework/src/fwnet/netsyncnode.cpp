// 
// netSyncNode.cpp
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "fwnet/netsyncnode.h"

// framework includes
#include "fwnet/netchannel.h"
#include "fwnet/optimisations.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netsynctree.h"

// rage includes
#include "bank/bank.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

AUTOID_IMPL(netINodeDataAccessor);

#if __DEV
#define ENABLE_MAGIC_TAG 1
#else
#define ENABLE_MAGIC_TAG 0
#endif

#if ENABLE_MAGIC_TAG
#define MAGIC_TAG_ONLY(...) __VA_ARGS__
namespace
{
    const unsigned int MAGIC_TAG      = 0xabcd;
    const unsigned int MAGIC_TAG_SIZE = 16;

    void READ_MAGIC_TAG(datBitBuffer &bitBuffer, const char* ASSERT_ONLY(nodeName))
    {
        unsigned int magicTag = 0;
        bitBuffer.ReadUns(magicTag, MAGIC_TAG_SIZE);
        gnetAssertf(magicTag == MAGIC_TAG, "Data mismatch in %s - expected to find magic tag after end of data block!", nodeName);
        NETWORK_QUITF(magicTag == MAGIC_TAG, "Data mismatch in %s - expected to find magic tag after end of data block!", nodeName);
    }

    void WRITE_MAGIC_TAG(datBitBuffer &bitBuffer)
    {
        unsigned int magicTag = MAGIC_TAG;
        bitBuffer.WriteUns(magicTag, MAGIC_TAG_SIZE);
    }
}

#else
#define MAGIC_TAG_ONLY(...)
#define READ_MAGIC_TAG(bitBuffer, nodeName)
#define WRITE_MAGIC_TAG(bitBuffer)
#endif // ENABLE_MAGIC_TAG 

///////////////////////////////////////////////////////////////////////////////////////
// netSyncNodeBase
///////////////////////////////////////////////////////////////////////////////////////
netSyncNodeBase::netSyncNodeBase() : 
	m_ParentTree(NULL),
	m_ParentNode(NULL),
	m_SerialiseModeFlags(0),
	m_ConditionalFlags(0),
	m_ActivationFlags(0)
{
#if !__NO_OUTPUT
	m_NodeName[0] = '\0';
#endif
}

netSyncNodeBase::netSyncNodeBase(SerialiseModeFlags serFlags, SerialiseModeFlags conditionalFlags, ActivationFlags actFlags) :
	m_ParentTree(NULL),
	m_ParentNode(NULL),
	m_SerialiseModeFlags(serFlags),
	m_ConditionalFlags(conditionalFlags),
	m_ActivationFlags(actFlags)
{
#if !__NO_OUTPUT
	m_NodeName[0] = '\0';
#endif
}

void netSyncNodeBase::SetParentTree(netSyncTree* pParentTree)
{
	if (gnetVerify(!m_ParentTree))
	{
		m_ParentTree = pParentTree;
	}
}

void netSyncNodeBase::SetParentNode(netSyncParentNode* pParentNode)
{
	if (gnetVerify(!m_ParentNode))
	{
		m_ParentNode = pParentNode;
	}
}

#if !__NO_OUTPUT
void netSyncNodeBase::SetNodeName(const char* nodeName)
{
	if (gnetVerify(nodeName) && gnetVerify(strlen(nodeName) <= NODE_NAME_LEN))
	{
		strncpy(m_NodeName, nodeName, NODE_NAME_LEN);
	}
}
#endif

///////////////////////////////////////////////////////////////////////////////////////
// netSyncParentNode
///////////////////////////////////////////////////////////////////////////////////////

netSyncParentNode::netSyncParentNode() : 
	m_NumChildren(0),
	m_NumDataChildren(0),
	m_ConditionalFlagsSet(false),
	m_ActivationFlagsSet(false),
	m_RootNode(false),
	m_ChildNodesDependent(false)
{
}

netSyncParentNode::~netSyncParentNode()
{
}

void netSyncParentNode::AddSerialiseModeFlags(SerialiseModeFlags serFlags)
{
	m_SerialiseModeFlags |= serFlags;

	if (m_ParentNode)
	{
		m_ParentNode->AddSerialiseModeFlags(serFlags);
	}
}

void netSyncParentNode::AddConditionalFlags(SerialiseModeFlags condFlags)
{
	// for each serialise mode, the parent is only conditional if all its children are
	for (unsigned i=0; i<(sizeof(SerialiseModeFlags)<<3); i++)
	{
		if (m_SerialiseModeFlags & (1<<i))
		{
			if (condFlags & (1<<i))
			{
				if (!m_ConditionalFlagsSet)
				{
					m_ConditionalFlags |= (1<<i);
				}
			}
			else if (m_ConditionalFlagsSet)
			{
				m_ConditionalFlags &= ~(1<<i);
			}
		}
	}

	m_ConditionalFlagsSet = true;

	if (m_ParentNode)
	{
		m_ParentNode->AddConditionalFlags(condFlags);
	}
}

void netSyncParentNode::AddActivationFlags(ActivationFlags actFlags)
{
	// for each activation flag, the parent is only set active if all its children are
	if (!m_ActivationFlagsSet)
	{
		m_ActivationFlags = actFlags;
		m_ActivationFlagsSet = true;
	}
	else
	{
		m_ActivationFlags &= actFlags;
	}

	if (m_ParentNode)
	{
		m_ParentNode->AddActivationFlags(actFlags);
	}
}

void netSyncParentNode::ShutdownNode()
{
	ChildList::iterator it = m_ChildList.begin();
	ChildList::const_iterator stop = m_ChildList.end();

	for(; stop != it; ++it)
	{
		netSyncNodeBase* pNode = *it;

		pNode->ShutdownNode();
	}

	m_ChildList.clear();
}

void netSyncParentNode::InitialiseDataNodesInTree(unsigned& dataPosition, unsigned& nodeIndex, unsigned& syncNodeIndex, unsigned &maxNumHeaderBits)
{
	if(!m_RootNode)
	{
		maxNumHeaderBits++;
	}

	ChildList::iterator it = m_ChildList.begin();
	ChildList::const_iterator stop = m_ChildList.end();

	for(; stop != it; ++it)
	{
		netSyncNodeBase* pNode = *it;

		pNode->InitialiseDataNodesInTree(dataPosition, nodeIndex, syncNodeIndex, maxNumHeaderBits);
	}
}

bool netSyncParentNode::Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, 
							  const unsigned currTime, netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags, unsigned &maxNumHeaderBitsRemaining)
{
	bool bDataWritten = false;

	if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
	{
		unsigned startCursorPos = bitBuffer.GetCursorPos();

		// leave a bit in the msgBuffer for the guard flag we will write indicating whether any of the children wrote any data
		// we don't need to do this for the root node or parents with no conditional children
		if (!m_RootNode && GetIsConditional(serMode))
		{
			bitBuffer.WriteBool(false);
		
			if(gnetVerifyf(maxNumHeaderBitsRemaining > 0, "Unexpected maximum number of header bits!"))
			{
				maxNumHeaderBitsRemaining--;
			}
		}

		bool hasSpace = true;

		if(m_ChildNodesDependent)
		{
			unsigned sizeOfChildData = GetDataSize(serMode, actFlags, *pObj);

			if(!bitBuffer.CanWriteBits(sizeOfChildData + maxNumHeaderBitsRemaining))
			{
				gnetAssertf((int)(sizeOfChildData + maxNumHeaderBitsRemaining) < bitBuffer.GetMaxBits(), "%s child nodes data exceeds maximum packet size for %s! Try to reduce the depth of the sub tree or size of dependent data", m_NodeName, pObj->GetLogName());

				if(m_ParentTree)
				{
					m_ParentTree->MarkFailedToWriteAllData();
				}

				hasSpace = false;
			}
		}

		if(hasSpace)
		{
			ChildList::iterator it = m_ChildList.begin();
			ChildList::const_iterator stop = m_ChildList.end();

			for(; stop != it; ++it)
			{
				netSyncNodeBase* pNode = *it;

				if (pNode->Write(serMode, actFlags, pObj, bitBuffer, currTime, pLog, player, pNodeFlags, maxNumHeaderBitsRemaining))
				{
					bDataWritten = true;
				}
			}
		}

		if (m_RootNode)
		{
			// ignore 0 bits written to message buffer from children if none of them wrote any real data
			if (!bDataWritten)
			{
				bitBuffer.SetCursorPos(startCursorPos);
			}
		}
		else if (GetIsConditional(serMode))
		{
			// if this is a conditional node, write the guard flag indicating whether the children wrote any data
			unsigned endCursorPos = bitBuffer.GetCursorPos();

			bitBuffer.SetCursorPos(startCursorPos);
			bitBuffer.WriteBool(bDataWritten);

			// ignore 0 bits written to message buffer from children if none of them wrote any real data
			if (bDataWritten)
			{
				bitBuffer.SetCursorPos(endCursorPos);
			}
		}
	}

	return bDataWritten;
}


bool netSyncParentNode::Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog)
{
	bool bDataRead = false;

	if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
	{
		bool bHasData = false;

		// don't need to read the guard flag for the tree root node or parents with no conditional children
		if (!m_RootNode && GetIsConditional(serMode))
		{
			bitBuffer.ReadBool(bHasData);
		}
		else
		{
			bHasData = true;
		}

		if (bHasData)
		{
			ChildList::iterator it = m_ChildList.begin();
			ChildList::const_iterator stop = m_ChildList.end();

			for(; stop != it; ++it)
			{
				netSyncNodeBase* pNode = *it;

				if (pNode->Read(serMode, actFlags, bitBuffer, pLog))
				{
					bDataRead = true;
				}
			}
		}
	}

	return bDataRead;
}

unsigned netSyncParentNode::GetDataSize(SerialiseModeFlags        serialisationMode,
										ActivationFlags           activationFlags,
										netSyncTreeTargetObject  &targetObject)
{
	unsigned size = 0;

	if (UsedBySerialiseMode(serialisationMode) && GetIsActive(activationFlags))
	{
		ChildList::iterator       it   = m_ChildList.begin();
		ChildList::const_iterator stop = m_ChildList.end();

		for(; stop != it; ++it)
		{
			netSyncNodeBase *node = *it;

			if(node)
			{
				size += node->GetDataSize(serialisationMode, activationFlags, targetObject);
			}
		}
	}

	return size;
}


unsigned netSyncParentNode::GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool outputToTTY)
{
    unsigned maximumDataSize = 0;

    if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
    {
        ChildList::iterator it = m_ChildList.begin();
        ChildList::const_iterator stop = m_ChildList.end();

        for(; stop != it; ++it)
        {
            netSyncNodeBase* pNode = *it;

            maximumDataSize += pNode->GetMaximumDataSize(serMode, actFlags, outputToTTY);
        }

#if !__NO_OUTPUT
        if(outputToTTY)
        {
            gnetDebug2("Maximum data size for %s (Serialisation mode: %d, Activation flags: %d) is %d", GetNodeName(), serMode, actFlags, maximumDataSize);
        }
#endif //!__NO_OUTPUT
    }

    return maximumDataSize;
}

void netSyncParentNode::AddChildNode(netSyncNodeBase* pChildNode)
{
    if (gnetVerify(pChildNode) && gnetVerify(m_ParentTree))
	{
		m_ChildList.push_back(pChildNode);
		pChildNode->SetParentNode(this);

		m_NumChildren++;

		if (pChildNode->GetIsDataNode())
		{
			m_NumDataChildren++;

			AddSerialiseModeFlags(pChildNode->GetSerialiseModeFlags());
			AddConditionalFlags(pChildNode->GetConditionalFlags());
			AddActivationFlags(pChildNode->GetActivationFlags());
		}
	}
}

#if __BANK

void netSyncParentNode::AddWidgets(bkBank *bank)
{
	bank->PushGroup(m_NodeName, false);

	ChildList::iterator it = m_ChildList.begin();
	ChildList::const_iterator stop = m_ChildList.end();

	for(; stop != it; ++it)
	{
		netSyncNodeBase* pNode = *it;

		pNode->AddWidgets(bank);
	}

	bank->PopGroup();
}

#endif

///////////////////////////////////////////////////////////////////////////////////////
// netSyncDataNode
///////////////////////////////////////////////////////////////////////////////////////
netSyncDataNode::netSyncDataNode() :
	m_DataIndex(INVALID_DATA_INDEX),
	m_DataStart(0),
	m_Updated(false),
	m_pCurrentSyncDataUnit(0),
    m_DependentNode(0),
    m_NumExternalNodeDependencies(0),
	m_HasDoneDependencySizeCheck(false)
{
    DEV_ONLY(m_MaxDataSize = 0;)
	BANK_ONLY(m_SelectedInBank = false;)

    for(unsigned index = 0; index < MAX_EXTERNAL_NODES_DEPENDENT; index++)
    {
        m_ExternalNodesDependent[index] = 0;
    }
};

netSyncDataNode::netSyncDataNode(SerialiseModeFlags serFlags, SerialiseModeFlags conditionalFlags, ActivationFlags actFlags ) :
	netSyncNodeBase(serFlags, conditionalFlags, actFlags),
	m_pCurrentSyncDataUnit(0),
	m_DataIndex(0),
	m_DataStart(INVALID_DATA_INDEX),
	m_Updated(false),
    m_DependentNode(0),
    m_NumExternalNodeDependencies(0),
	m_HasDoneDependencySizeCheck(false)
{
	gnetAssert((conditionalFlags & serFlags) == conditionalFlags);
    DEV_ONLY(m_MaxDataSize = 0;)
	BANK_ONLY(m_SelectedInBank = false;)

    for(unsigned index = 0; index < MAX_EXTERNAL_NODES_DEPENDENT; index++)
    {
        m_ExternalNodesDependent[index] = 0;
    }
};

netSyncDataNode::~netSyncDataNode() 
{
}

void netSyncDataNode::SetNodeDependency(netSyncDataNode &node) 
{
    // ensure this node is not an external dependency of this node (this would create a circular dependency)
    for(unsigned index = 0; index < m_NumExternalNodeDependencies; index++)
    {
        if(m_ExternalNodesDependent[index] == &node)
        {
            gnetAssertf(0, "Trying to set a node dependency for a node that is dependent on this node!");
            return;
        }
    }

    if(!gnetVerifyf(m_DependentNode == 0, "Trying to overwrite an existing node dependency!"))
    {
        return;
    }

    m_DependentNode = &node;

    node.AddExternalNodeDependency(*this);
}

void netSyncDataNode::AddExternalNodeDependency(netSyncDataNode &node)
{
    if(gnetVerifyf(m_NumExternalNodeDependencies < MAX_EXTERNAL_NODES_DEPENDENT, "Adding to many external node dependencies!"))
    {
        // ensure this node is not the dependent node (this would create a circular dependency)
        if(gnetVerifyf(m_DependentNode != &node, "Trying to set the dependent node as an external dependency!"))
        {
            m_ExternalNodesDependent[m_NumExternalNodeDependencies] = &node;
            m_NumExternalNodeDependencies++;
        }
    }
}

void netSyncDataNode::InitialiseData(netSyncTreeTargetObject* pObj)
{
	if (GetUsesCurrentStateBuffer())
	{
		netSyncDataBase* pObjectSyncData = NULL;

		if (gnetVerify(m_ParentTree) && gnetVerify(m_pCurrentSyncDataUnit))
		{
			pObjectSyncData = pObj->GetSyncData(); 

			if (gnetVerify(pObjectSyncData->GetCurrentStateBuffer()))
			{
				// write current state buffer and compare with delta buffer to determine whether the data has changed
				datBitBuffer &currentStateBuffer	= *pObjectSyncData->GetCurrentStateBuffer();
				datBitBuffer &shadowBuffer			= *pObjectSyncData->GetShadowBuffer();

				gnetAssertf(m_pCurrentSyncDataUnit->GetSizeOfShadowData() == 0, "Shadow buffer data has already been written - has InitialiseData() been called twice?");

				// advance cursor pos to start of the data for this node
				currentStateBuffer.SetNumBitsWritten(GetDataStart());
				shadowBuffer.SetNumBitsWritten(GetDataStart());

				// write node data to current state buffer
				WriteData(pObj, currentStateBuffer, 0);

				unsigned dataSizeWritten = currentStateBuffer.GetCursorPos() - GetDataStart();

				BANK_ONLY(gnetAssertf(dataSizeWritten > 0, "netSyncDataNode::InitialiseData: No data was written for node %s", GetNodeName());)
				DEV_ONLY(gnetAssertf(dataSizeWritten <= GetMaxSizeOfData(), "Current state buffer overrun detected for node %s! This will corrupt data written by other nodes!", GetNodeName()));

				// copy current state data to shadow buffer
				pObj->GetSyncData()->CopyCurrentStateData(GetDataStart(), shadowBuffer, GetDataStart(), dataSizeWritten);

				if (dataSizeWritten%8 != 0)
				{
					// write remaining bits to next byte alignment
					unsigned pad = 0;
					unsigned padSize = 8 - dataSizeWritten%8;

					currentStateBuffer.WriteUns(pad, padSize);
					shadowBuffer.WriteUns(pad, padSize);
				}

				m_pCurrentSyncDataUnit->SetSizeOfCurrentData(dataSizeWritten);
				m_pCurrentSyncDataUnit->SetSizeOfShadowData(dataSizeWritten);
			}
		}
	}
	else if(GetIsManualDirty())
	{
		if (gnetVerify(m_pCurrentSyncDataUnit))
		{
			unsigned sizeOfData = GetDataSize(*pObj);
			m_pCurrentSyncDataUnit->SetSizeOfCurrentData(sizeOfData);
		}
	}
}

void netSyncDataNode::SetDirty(netSyncTreeTargetObject* pObj)
{
	if (gnetVerify(m_pCurrentSyncDataUnit))
	{
		pObj->GetSyncData()->SetSyncDataUnitDirty(m_DataIndex);

		// for manual dirty nodes we need to calculate the sync size and store it in the sync data.
		// This allows us to determine whether the node data will fit when we are writing it to a bit buffer
		if(GetIsManualDirty())
		{
			unsigned sizeOfData = GetDataSize(*pObj);
			m_pCurrentSyncDataUnit->SetSizeOfCurrentData(sizeOfData);
		}
	}
}

void netSyncDataNode::InitialiseDataNodesInTree(unsigned& dataPosition, unsigned& nodeIndex, unsigned& syncNodeIndex, unsigned &maxNumHeaderBits)
{
    DEV_ONLY(unsigned initPosition = dataPosition);
	
	// the data for each node should all be byte aligned in the buffer
	Assert(initPosition%8 == 0);

	maxNumHeaderBits++;

	if (GetIsSyncUpdateNode())
	{
		SetDataIndex(syncNodeIndex);

		m_ParentTree->SetDataNodeIndices(this, nodeIndex++, (int)syncNodeIndex++);

		if (GetUsesCurrentStateBuffer())
		{
			SetDataStart(dataPosition);

			dataPosition += GetMaximumDataSize();
		
			// byte align the data position, to enable quick mem comparisons later
			unsigned mod = dataPosition%8;

			if (mod != 0)
			{
				dataPosition += 8-mod;
			}
#if __DEV
			SetMaxSizeOfData(dataPosition - initPosition);
			Assert(GetMaxSizeOfData()%8 == 0);
#endif
		}
	}
	else
	{
		m_ParentTree->SetDataNodeIndices(this, nodeIndex++);
	}
}

bool netSyncDataNode::Update(netSyncTreeTargetObject* pObj)
{
	if (GetUsesCurrentStateBuffer())
	{
		netSyncDataBase* pObjectSyncData = pObj->GetSyncData();

		Assert(pObjectSyncData);
		Assert(m_pCurrentSyncDataUnit);

		datBitBuffer *currentStateBuffer = pObjectSyncData->GetCurrentStateBuffer();

		if (currentStateBuffer)
		{
			u32 dataStart = GetDataStart();

			gnetAssertf(m_pCurrentSyncDataUnit->GetSizeOfShadowData() > 0, "Shadow buffer data has not been written - has InitialiseData() been called?");
			Assert(dataStart%8==0); // this should be byte aligned

			currentStateBuffer->SetCursorPos(dataStart);

			// write node data to current state buffer
			WriteData(pObj, *currentStateBuffer, 0);

			unsigned dataSizeWritten = currentStateBuffer->GetCursorPos() - GetDataStart();

			m_pCurrentSyncDataUnit->SetSizeOfCurrentData(dataSizeWritten);

			if (dataSizeWritten%8 != 0)
			{
				// write remaining bits to next byte alignment
				unsigned pad = 0;
				currentStateBuffer->WriteUns(pad, 8 - dataSizeWritten%8);
			}

			Assert(currentStateBuffer->GetCursorPos()%8 == 0);

			BANK_ONLY(gnetAssertf(dataSizeWritten > 0, "netSyncDataNode::Update: No data was written for node %s", GetNodeName());)
			DEV_ONLY(gnetAssertf(dataSizeWritten <= GetMaxSizeOfData(), "Current state buffer overrun detected for node %s! This will corrupt data written by other nodes!", GetNodeName()));

			u32 dataStartByte = dataStart>>3;
			u32 numBytesToCompare = (currentStateBuffer->GetCursorPos()>>3) - dataStartByte;

			// compare the node's current state data with the previous data held in the shadow buffer
			if ((dataSizeWritten != m_pCurrentSyncDataUnit->GetSizeOfShadowData()) || 
				pObj->GetSyncData()->CompareBufferData(dataStartByte, numBytesToCompare))
			{
				// data differs, so this node's data is flagged as dirty
				SetDirty(pObj);

				pObjectSyncData->GetShadowBuffer()->SetCursorPos(GetDataStart());

				// copy the current state data to shadow buffer
				pObj->GetSyncData()->CopyCurrentStateToShadowState(dataStartByte, numBytesToCompare);

				m_pCurrentSyncDataUnit->SetSizeOfShadowData(dataSizeWritten);

				return true;
			}
		}
	}

	return false;
}

bool netSyncDataNode::HasDoneDependencySizeCheck()
{
	if(GetNodeDependency())
	{
		return GetNodeDependency()->HasDoneDependencySizeCheck();
	}

	return m_HasDoneDependencySizeCheck;
}

bool netSyncDataNode::Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, 
							const unsigned currTime, netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags, unsigned &maxNumHeaderBitsRemaining)
{
	if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
	{
		bool updated = m_Updated;

		if (GetIsConditional(serMode))
		{
			// if we have got this far the node must be ready to send data
			bool bReadyToSend = IsReadyToSendToPlayer(pObj, player, serMode, actFlags, currTime);

			// ensure there is space to fit this update
			if(bReadyToSend && m_pCurrentSyncDataUnit)
			{
				unsigned sizeOfData = 0;

				if(HasNodeDependencies() && !HasDoneDependencySizeCheck())
				{
					if(GetNodeDependency())
					{
						GetNodeDependency()->SetHasDoneDependencySizeCheck(true);
					}
					
					// build a list of node dependencies
					const unsigned MAX_DEPENDENCIES = 20;
					netSyncDataNode *nodeDependencies[MAX_DEPENDENCIES];
					unsigned numDependentNodes = 0;

					pObj->GetAllNodeDependencies(*this, nodeDependencies, MAX_DEPENDENCIES, numDependentNodes);
					
					for(unsigned index = 0; index < numDependentNodes; index++)
					{
						const netSyncDataNode *dependentNode = nodeDependencies[index];

						if(dependentNode && dependentNode->IsReadyToSendToPlayer(pObj, player, serMode, actFlags, currTime))
						{
							const netSyncDataUnitBase *dependentSyncDataUnit = dependentNode->GetCurrentSyncDataUnit();

							if(dependentSyncDataUnit)
							{
								unsigned dependentDataSize = dependentSyncDataUnit->GetSizeOfCurrentData();

								MAGIC_TAG_ONLY(dependentDataSize += (MAGIC_TAG_SIZE * 2));

								sizeOfData += dependentDataSize;
							}
						}
					}
				}
				else
				{
					sizeOfData = m_pCurrentSyncDataUnit->GetSizeOfCurrentData();
					MAGIC_TAG_ONLY(sizeOfData += (MAGIC_TAG_SIZE * 2));
				}

				if(!bitBuffer.CanWriteBits(sizeOfData + maxNumHeaderBitsRemaining))
				{
					gnetAssertf((int)(sizeOfData + maxNumHeaderBitsRemaining) < bitBuffer.GetMaxBits(), "%s: Dependent nodes data exceeds maximum packet size! Try to reduce number of dependencies or size of dependent data", m_NodeName);

					if(m_ParentTree)
					{
						m_ParentTree->MarkFailedToWriteAllData();
					}

					bReadyToSend = false;
				}
			}

			bitBuffer.WriteBool(bReadyToSend);

			if(gnetVerifyf(maxNumHeaderBitsRemaining > 0, "Unexpected maximum number of header bits!"))
			{
				maxNumHeaderBitsRemaining--;
			}

			if (bReadyToSend)
			{
				updated = true;

				if (pNodeFlags)
				{
					Assert((*pNodeFlags & (1<<m_DataIndex)) == 0);

					*pNodeFlags |= (1<<m_DataIndex);
				}
			}
		}
		else
		{
			updated = true;
		}

		m_HasDoneDependencySizeCheck = true;
        m_Updated = updated;

		if (updated)
		{
			if (GetUsesCurrentStateBuffer(serMode) && pObj->GetSyncData() && gnetVerify(m_pCurrentSyncDataUnit))
			{
				WRITE_MAGIC_TAG(bitBuffer);

				unsigned cursorPos = bitBuffer.GetCursorPos();

				// copy current state data to message buffer
				unsigned sizeOfData = m_pCurrentSyncDataUnit->GetSizeOfCurrentData();
				gnetAssertf(sizeOfData > 0, "%s: Writing no data!", m_NodeName);
				pObj->GetSyncData()->CopyCurrentStateData(GetDataStart(), bitBuffer, bitBuffer.GetCursorPos(), sizeOfData);

				if (pLog)
				{
					// read the buffer data so that the node contains a copy of the data in the current state buffer
					bitBuffer.SetCursorPos(cursorPos);
					ReadData(bitBuffer, pLog);
				}

				WRITE_MAGIC_TAG(bitBuffer);
			}
			else
			{
				WRITE_MAGIC_TAG(bitBuffer);
				WriteData(pObj, bitBuffer, pLog);
				WRITE_MAGIC_TAG(bitBuffer);
			}		
		}
	}

	return m_Updated;
}

bool netSyncDataNode::Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog)
{
    bool updated = m_Updated;

	if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
	{
		if (GetIsConditional(serMode))
		{
			bool bGuardFlag = false;

			bitBuffer.ReadBool(bGuardFlag);

			if (bGuardFlag)
			{
				BANK_ONLY(READ_MAGIC_TAG(bitBuffer, GetNodeName()));
				ReadData(bitBuffer, pLog);
				BANK_ONLY(READ_MAGIC_TAG(bitBuffer, GetNodeName()));

				updated = true;
			}
		}
		else
		{
			BANK_ONLY(READ_MAGIC_TAG(bitBuffer, GetNodeName()));
			ReadData(bitBuffer, pLog);
			BANK_ONLY(READ_MAGIC_TAG(bitBuffer, GetNodeName()));

			updated = true;
		}
	}

    m_Updated = updated;
	return updated;
}

unsigned netSyncDataNode::GetDataSize(SerialiseModeFlags        serialisationMode,
									  ActivationFlags           activationFlags,
									  netSyncTreeTargetObject  &targetObject)
{
	unsigned size = 0;

	if (UsedBySerialiseMode(serialisationMode) && GetIsActive(activationFlags))
	{
		size += GetDataSize(targetObject);

		MAGIC_TAG_ONLY(size += (MAGIC_TAG_SIZE * 2));
	}

	return size;
}

unsigned netSyncDataNode::GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool OUTPUT_ONLY(outputToTTY))
{
    unsigned maximumDataSize = 0;

    if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
    {
        maximumDataSize = GetMaximumDataSize();

#if !__NO_OUTPUT
        if(outputToTTY)
        {
            gnetDebug2("Maximum data size for %s (Serialisation mode: %d, Activation flags: %d) is %d", GetNodeName(), serMode, actFlags, maximumDataSize);
        }
#endif //!__NO_OUTPUT
    }

    return maximumDataSize;
}

void netSyncDataNode::ForceSend(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj)
{
	if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags) && pObj && pObj->GetSyncData())
	{
        if (gnetVerify(m_pCurrentSyncDataUnit))
        {
            if(serMode == SERIALISEMODE_FORCE_SEND_OF_DIRTY)
            {
                PlayerFlags unsyncedPlayers = m_pCurrentSyncDataUnit->GetUnsyncedPlayers() & netInterface::GetPlayerMgr().GetRemotePhysicalPlayersBitmask();

                if(unsyncedPlayers != 0)
                {
                    for (PhysicalPlayerIndex player=0; player<MAX_NUM_PHYSICAL_PLAYERS; player++)
				    {
				        if (unsyncedPlayers & (1<<player))
					    {
                            ForceSendToPlayer(player, serMode, actFlags, pObj);
                        }
                    }
                }
            }
            else
            {
		        pObj->GetSyncData()->ForceDataUnitSend(GetDataIndex());

                PlayerFlags playersToIgnore = StopSend(pObj, SERIALISEMODE_UPDATE);

		        // the node may not want to send data to some players
		        m_pCurrentSyncDataUnit->ClearUpdateForPlayers(playersToIgnore);
            }
        }
	}
}

void netSyncDataNode::ForceSendToPlayer(const PhysicalPlayerIndex player, SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj)
{
	if (UsedBySerialiseMode(serMode) && GetIsActive(actFlags))
	{
        PlayerFlags playersToIgnore = StopSend(pObj, SERIALISEMODE_UPDATE);

        if((playersToIgnore & (1<<player)) == 0)
        {
            if (gnetVerify(m_pCurrentSyncDataUnit))
            {
                bool forceSendData = true;

                if(serMode == SERIALISEMODE_FORCE_SEND_OF_DIRTY)
                {
                    forceSendData = !m_pCurrentSyncDataUnit->IsSyncedWithPlayer(player);
                }

                if(forceSendData)
                {
		            pObj->GetSyncData()->ForceDataUnitSendToPlayer(GetDataIndex(), player);
                    pObj->GetSyncData()->SetUnsyncedWithPlayer(player);
                }
            }
        }
	}
}

#if __BANK
void netSyncDataNode::AddWidgets(bkBank *bank)
{
	bank->AddToggle(m_NodeName, &m_SelectedInBank);
}
#endif

bool netSyncDataNode::GetUsesCurrentStateBuffer(SerialiseModeFlags serMode) const
{
	// the checksum serialise mode is used for calculating a checksum of important data on cloned network objects, so we do not write to the current
	// state buffer in this case (they do not have any sync data).
	if (serMode == SERIALISEMODE_CHECKSUM)
		return false;

    return (GetIsSyncUpdateNode() && !GetIsManualDirty());
}

} // namespace rage
