// 
// netBandwidthRecorderWrapper.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

// game includes
#include "fwnet/netbandwidthrecorder.h"

#include "fwsys/timer.h"
#include "fwnet/optimisations.h"

// rage includes
#include "net/netaddress.h"

NETWORK_OPTIMISATIONS()

#if __BANK

namespace rage
{

netBandwidthRecorderWrapper::netBandwidthRecorderWrapper() 
{
	m_Bandwidth.SetSampleInterval(0);
}

void netBandwidthRecorderWrapper::AddBandwidthIn(unsigned bandwidthIn)
{
	netSocketAddress addr;
	m_Bandwidth.RecordInboundPacket(addr, 0, bandwidthIn);
}

void netBandwidthRecorderWrapper::AddBandwidthOut(unsigned bandwidthOut)
{
	netSocketAddress addr;
	m_Bandwidth.RecordOutboundPacket(addr, 0, bandwidthOut);
}

void netBandwidthRecorderWrapper::SetSampleInterval(unsigned interval) 
{
	m_Bandwidth.SetSampleInterval(interval); 
}

void netBandwidthRecorderWrapper::SampleData(unsigned currentTime)
{
	m_Bandwidth.SampleData(currentTime);
}

float netBandwidthRecorderWrapper::GetBandwidthInRate() const
{
	return static_cast<float>(m_Bandwidth.GetInboundBandwidth());
}

float netBandwidthRecorderWrapper::GetBandwidthOutRate() const
{
	return static_cast<float>(m_Bandwidth.GetOutboundBandwidth());
}

const char *netBandwidthRecorderWrapper::GetName() const
{
	return m_Name != 0 ? m_Name : "Undefined";
}

void netBandwidthRecorderWrapper::Update()
{
	m_Bandwidth.SampleData(fwTimer::GetSystemTimeInMilliseconds());
}

} // namespace rage

#endif  // __BANK
