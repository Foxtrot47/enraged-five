#if !__FINAL && !__PROFILE

#include "HttpQuery.h"
#include "parser/manager.h"						// XML
#include "parser/tree.h"
#include "system/param.h"

PARAM(httpoutput, "display what is posted to and returned by http query");

namespace rage
{

sysIpcSema fwHttpQuery::sm_NonEmptySignal;
sysCriticalSectionToken fwHttpQuery::sm_queryListLock;
atFixedArray<fwHttpQuery*, MAX_HTTP_QUERIES> fwHttpQuery::sm_queryList;

// ----- CHttpSearch --------------------------------------------------------------

fwHttpQuery::fwHttpQuery(const char* addr, const char* baseUri, bool useRageHttp)
	: m_address(addr), m_baseUri(baseUri), m_useRageHttp(useRageHttp)
{
}

fwHttpQuery::fwHttpQuery(const char* addr, const char* baseUri, const char* username, const char* password, bool useRageHttp)
	: m_address(addr), m_baseUri(baseUri), m_username(username), m_password(password), m_useRageHttp(useRageHttp)
{
}

fwHttpQuery::~fwHttpQuery()
{
	Shutdown();
}

bool fwHttpQuery::Init(const char* search, int timeout, int bufferSize)  
{
	sysMemUseMemoryBucket mem_debug(MEMBUCKET_DEBUG);
	m_buffer.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL), datGrowBuffer::NULL_TERMINATE);
	m_buffer.Preallocate(bufferSize);
	m_finished = false;

	const s32 MaxLengthOfString = 1024;
	Assertf( (strlen(search) + m_baseUri.length()) < MaxLengthOfString, "CHttpQuery::Init - query string is too long");

	char fullSearch[MaxLengthOfString];
	netSocketAddress ip(m_address);

	sprintf(fullSearch, "%s%s", m_baseUri.c_str(), search);
	m_HttpRequest.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL), netTcp::GetDebugSslCtx());
	if(m_useRageHttp)
	{
		m_HttpRequest.SetOptions(NET_HTTP_USE_RAGE_HTTP);
	}
	m_HttpRequest.BeginGet(fullSearch
							,&ip
							,timeout
							,&m_buffer
							,NULL
							,NULL
							,&m_status);
	if (m_username.length() > 0 || m_password.length() > 0)
	{
		m_HttpRequest.AddBasicAuth(m_username, m_password);
	}

	bool rt = m_HttpRequest.Commit();
	if(rt)
		AddToHttpQueryQueue();
	return rt;
}

void fwHttpQuery::InitPost(const char* search, int timeout, int bufferSize)  
{
	sysMemUseMemoryBucket mem_debug(MEMBUCKET_DEBUG);
	m_buffer.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL), datGrowBuffer::NULL_TERMINATE);
	m_buffer.Preallocate(bufferSize);
	m_finished = false;

	const s32 MaxLengthOfString = 1024;
	Assertf( (strlen(search) + m_baseUri.length()) < MaxLengthOfString, "CHttpQuery::Init - query string is too long");

	char fullSearch[MaxLengthOfString];
	netSocketAddress ip(m_address);

	sprintf(fullSearch, "%s%s", m_baseUri.c_str(), search);
	m_HttpRequest.Init(sysMemAllocator::GetMaster().GetAllocator(MEMTYPE_DEBUG_VIRTUAL), netTcp::GetDebugSslCtx());

	m_HttpRequest.BeginPost(fullSearch,
								&ip,
								timeout,
								&m_buffer,
								NULL,
								NULL,
								&m_status);

	if (m_username.length() > 0 || m_password.length() > 0)
	{
		m_HttpRequest.AddBasicAuth(m_username, m_password);
	}
}

void fwHttpQuery::AppendXmlContent(const char* pXml, int length)
{
	m_HttpRequest.AddRequestHeaderValue("Content-type", "application/xml");
	m_HttpRequest.AppendContent(pXml, length);

	if(PARAM_httpoutput.Get())
	{
		fiSafeStream pStream(ASSET.Create("common:/data/debug/HttpPost.xml", ""));
		if(pStream)	
			pStream->Write(pXml, length);
	}
}

bool fwHttpQuery::CommitPost()
{
	bool rt = m_HttpRequest.Commit();
	if(rt)
		AddToHttpQueryQueue();
	return rt;
}

void fwHttpQuery::Finish()
{
	if(PARAM_httpoutput.Get())
		WriteBufferToFile("common:/data/debug/HttpOutput.txt");
	m_finished = true;
	OnFinish(); // this class could be deleted after the call
}

void fwHttpQuery::Shutdown()
{
	m_buffer.Clear();
}

bool fwHttpQuery::Pending()
{
	return (m_status.Pending() || m_finished == false);
}

void fwHttpQuery::Update()
{
	m_HttpRequest.Update();
}

void fwHttpQuery::WriteBufferToFile(const char* pFile)	
{
	fiSafeStream pReadStream(CreateStream());
	fiSafeStream pStream(ASSET.Create(pFile, ""));
	if (pReadStream && pStream)
	{
		int c;
		while((c=pReadStream->GetCh()) > 0)
		{
			pStream->PutCh((char) c);
		}
	}
}

fiStream* fwHttpQuery::CreateStream()
{
	char filename[RAGE_MAX_PATH];
	fiDeviceMemory::MakeMemoryFileName(filename, sizeof(filename), m_buffer.GetBuffer(), m_buffer.Length(), false, NULL);
	fiStream* memStream = fiStream::Open(filename, true);

	return memStream;
}

parTree* fwHttpQuery::CreateTree()
{
	USE_DEBUG_MEMORY();

	Assert(m_status.Succeeded());

	// If no response came back return NULL
	if(m_buffer.Length() == 0)
		return NULL;

	fiStream* memStream = CreateStream();
	parTree* pTree = PARSER.LoadTree(memStream);
	if(!pTree)
	{
		Errorf("Failed to parse rest xml");
	}
	memStream->Close();

	return pTree;
}

void fwHttpQuery::InitHttpQueryQueue()
{
    sm_NonEmptySignal = sysIpcCreateSema(false);
	sysIpcCreateThread(&HttpQueryUpdateThread, NULL, 32*1024, PRIO_NORMAL, "HttpQuery" );
}

void fwHttpQuery::AddToHttpQueryQueue()
{
    SYS_CS_SYNC(sm_queryListLock);
    //Old behavior was to silently fail if the queue is full
    if (!sm_queryList.IsFull())
    {        
        sm_queryList.Push(this);

        // If this is the first item, signal that it's no longer empty
        if (sm_queryList.GetCount() == 1)
        {
            sysIpcSignalSema(sm_NonEmptySignal);
        }
    }
    else
    {
        //Assert, and immediately mark this task as finished
        Assert(false);
        this->Finish();
    }
}

void fwHttpQuery::HttpQueryUpdateThread(void*)
{
    //TODO: netHttpRequest is not thread-safe, so we need to see about moving this to the main thread

	while( true )
	{
        //Wait until we have something to process
        sysIpcWaitSema(sm_NonEmptySignal);

        //Now that we have something to process, loop until we have no more
        while( true )
        {
            {
                SYS_CS_SYNC(sm_queryListLock);
            
                atFixedArray<fwHttpQuery*, MAX_HTTP_QUERIES>::iterator it = sm_queryList.begin();
                while (it != sm_queryList.end())
                {
                    (*it)->Update();

                    if (!(*it)->m_status.Pending())
                    {
                        (*it)->Finish();

                        it = sm_queryList.erase(it);
                    }
                    else
                    {
                        ++it;
                    }
                }

                if (sm_queryList.IsEmpty())
                {
                    break;
                }
            }

            sysIpcSleep(1);
        } 
	}
}

} // namespace rage

#endif // !__FINAL

