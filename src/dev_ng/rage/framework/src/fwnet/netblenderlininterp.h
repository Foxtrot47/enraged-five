//
// netblenderlininterp.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETBLENDERLININTERP_H
#define NETBLENDERLININTERP_H

//rage includes
#include "vector/Vector3.h"

// game includes
#include "fwnet/netblender.h"
#include "fwnet/netutils.h"
#include "fwsys/timer.h"

namespace rage
{

//PURPOSE
// This class is used to store the data required to perform the blending.
class netLinInterpPredictionData
{
    struct netSnapshot;

public:

    //PURPOSE
    // Class Constructor
    //PARAMS
    // position - The initial position of the object associated with the blender
    // velocity - The intial velocity of the object associated with the blender
    // time     - The time the position and velocity was sampled
    netLinInterpPredictionData(const Vector3 &position,
                               const Vector3 &velocity,
                               const u32      time);
    ~netLinInterpPredictionData();

    //PURPOSE
    // Resets the contents of the blender data to the specified parameters
    //PARAMS
    // position - The initial position of the object associated with the blender
    // velocity - The initial velocity of the object associated with the blender
    // time     - The time the position and velocity was sampled
    void Reset(const Vector3 &position,
               const Vector3 &velocity,
               const u32      time,
			   blenderResetFlags resetFlag = RESET_POSITION_AND_VELOCITY);

    netSnapshot &GetSnapshotPast()    { return m_SnapshotPast;    }
    netSnapshot &GetSnapshotPresent() { return m_SnapshotPresent; }

    const netSnapshot &GetSnapshotPast()    const { return m_SnapshotPast;    }
    const netSnapshot &GetSnapshotPresent() const { return m_SnapshotPresent; }

private:

    netLinInterpPredictionData();

    // snapshot of data to predict from
    struct netSnapshot
    {
        Vector3 m_Position;          // The position of the blend target when the snapshot was taken
        Vector3 m_Velocity;          // The velocity of the blend target when the snapshot was taken
        u32     m_PositionTimestamp; // The time the snapshot of the blend target position was taken
        u32     m_VelocityTimestamp; // The time the snapshot of the blend target velocity was taken
    };

    netSnapshot m_SnapshotPast;    // first snapshot (past)
    netSnapshot m_SnapshotPresent; // second snapshot (present)
};

struct VelocityChangeData
{
    float m_MinVelChange;           // minimum change in velocity for the blender to apply
    float m_MaxVelChange;           // maximum change in velocity for the blender to apply
    float m_VelChangeRate;          // current velocity change rate to used
    float m_ErrorIncreaseVel;       // the error distance to start increasing the change rate
    float m_ErrorDecreaseVel;       // the error distance to start decreasing the change rate
    float m_SmallVelocitySquared;   // The minimum velocity difference to blend towards
    float m_MaxVelDiffFromTarget;   // the maximum velocity difference from the last received velocity allowed
    float m_MaxVelChangeToTarget;   // the maximum velocity change to the last received velocity allowed (used when the blender has accelerated faster than the target speed)
    float m_PositionDeltaMaxLow;    // the position delta before a pop for objects being updated at a low update rate
    float m_PositionDeltaMaxMedium; // the position delta before a pop for objects being updated at a medium update rate
    float m_PositionDeltaMaxHigh;   // the position delta before a pop for objects being updated at a high update rate
};

//PURPOSE
// This structure contains parameters that are used to customise the blenders behaviour.
struct CLinInterpBlenderData : public netBlenderData
{
    bool   m_AllowAdjustVelChangeFromUpdates;     // Indicates we are allowing the per frame velocity change to be adjusted to match the changes coming over the network
    bool   m_UseBlendVelSmoothing;                // Indicates whether the blender will smooth velocity changes over a time interval, or just work frame to frame
    u32    m_BlendVelSmoothTime;                  // The time in ms to base blender behaviour over (looks at where the object will be this time in the future)
    float  m_PositionDeltaMin;                    // The minimum distance from the blend target position for which we attempt to blend
    float  m_MaxPredictTime;                      // The maximum time to predict the objects position into the future
    bool   m_ApplyVelocity;                       // This flag is used to determine whether the blender changes the target objects position
    bool   m_PredictAcceleration;                 // Indicates whether the blender should predict acceleration of the target object
    u32    m_BlendRampTime;                       // This is the amount of time to blend towards the target position (in addition to velocity changes)
    u32    m_BlendStopTime;                       // This is the maximum time the blender can run for without an additional update
    float  m_VelocityErrorThreshold;              // The velocity error value that when detected will change the blender mode
    float  m_NormalModePositionThreshold;         // The distance from the blend target that will return the blender mode to normal (when in velocity error mode)
    float  m_LowSpeedThresholdSqr;                // The blend target speed when the blender will move to low speed mode
    float  m_HighSpeedThresholdSqr;               // The blend target speed when the blender will move to velocity error mode
    float  m_MaxOnScreenPositionDeltaAfterBlend;  // The maximum position the object is allowed to be from its target once blending has stopped while on screen
    float  m_MaxOffScreenPositionDeltaAfterBlend; // The maximum position the object is allowed to be from its target once blending has stopped while off screen
    bool   m_HighPrecisionMode;                   // Indicates the blender is operating in high precision mode (custom rules to keep an object very tightly in sync, sacrificing motion smoothness)
    float  m_LogarithmicBlendRatio;               // The ratio to use when blending to the target position logarithmically
    float  m_LogarithmicBlendMaxVelChange;        // The maximum velocity change allowed when blending logarithmically

    // data controlling how fast to accelerate the object to it's blend target
    VelocityChangeData m_LowSpeedMode;   // Used when blending an object traveling at lower speeds
    VelocityChangeData m_NormalMode;     // Normal operation
    VelocityChangeData m_HighSpeedMode;  // Used when blending an object traveling at higher speed
};

//PURPOSE
// This class implements a network blender that uses linear interpolation to blend
// network updates of an object that has a position and velocity to give smooth motion.
class netBlenderLinInterp : public netBlender
{
public:

    //PURPOSE
    // Class constructor
    //PARAMS
    // blenderData     - The network blender data to use for this blender
    // initialPosition - The initial position of the object being blended
    // initialVelocity - The initial velocity of the object being blended
    netBlenderLinInterp(CLinInterpBlenderData *blenderData,
                        const Vector3         &initialPosition,
                        const Vector3         &initialVelocity);

    virtual ~netBlenderLinInterp() ;

    //PURPOSE
    // Called when the network object associated with this blender changes owner
    virtual void OnOwnerChange(blenderResetFlags resetFlag = RESET_POSITION_AND_VELOCITY);

    //PURPOSE
    // Performs the blend and returns true if there was any change to the object
    virtual void Update();

    //PURPOSE
    // Resets the blender to it's initial state
    virtual void Reset();

    //PURPOSE
    // Moves the blender target straight to it's target state
    virtual void GoStraightToTarget();

    //PURPOSE
    // Called on the blender after the physics update
    virtual void ProcessPostPhysics();

    //PURPOSE
    // Called to update the current mode the blender is operating in
    virtual void UpdateBlenderMode(const float distanceFromTarget);

    //PURPOSE
    // Returns the last position received for the blend target object
    //PARAMS
    // timestamp - If this parameter is non-NULL it will be set to
    //             the time the position was received
    Vector3 GetLastPositionReceived(u32 *timestamp = 0) const;

    //PURPOSE
    // Returns the last velocity received for the blend target object
    //PARAMS
    // timestamp - If this parameter is non-NULL it will be set to
    //             the time the velocity was received
    Vector3 GetLastVelocityReceived(u32 *timestamp = 0) const;

    //PURPOSE
    // Adds a snapshot of the blend target object position
    //PARAMS
    // position  - The position of the blend target object
    // time      - The time the position of the blend target object was sampled
    void UpdatePosition(const Vector3 &position, u32 time);

    //PURPOSE
    // Adds a snapshot of the blend target object velocity
    //PARAMS
    // position  - The velocity of the blend target object
    // time      - The time the velocity of the blend target object was sampled
    void UpdateVelocity(const Vector3 &velocity, u32 time);

    //PURPOSE
    // Returns the predicted position at the current time (this is the position the
    // blender is using as its target)
    virtual const Vector3 GetCurrentPredictedPosition() const;

    //PURPOSE
    // Returns the predicted position at the specified time in the future
    //PARAMS
    // timeInFutureMs - The time in the future to predict in ms
    virtual const Vector3 GetPredictedFuturePosition(u32 timeInFutureMs) const;

    //PURPOSE
    // Returns the predicted position at the specified time
    const Vector3 GetPredictedPosition(unsigned time) const;

    //PURPOSE
    // Returns a predicted acceleration based on previous received velocity updates.
    // The units returned are metres per second per second
    const Vector3 GetPredictedAccelerationVector() const;

    //PURPOSE
    // Returns a predicted acceleration squared based on previous received velocity updates.
    // The units returned are metres per second per second
    const float GetPredictedAccelerationSquared() const;

    //PURPOSE
    // Predicts a velocity based on previously received position updates, this can be used
    // for objects that are not syncing their velocity as a rough estimate
    //PARAMS
    // maxSpeedToPredict - Maximum speed calculated before the predicted velocity
    //                     is set to zero. This is useful for avoiding predicting
    //                     massive velocities when an object warps across the map
    virtual const Vector3 GetPredictedVelocity(float maxSpeedToPredict) const;
	
	//PURPOSE
	// Applies velocity and position on entities at the end of the blend time when velocity is non-zero
	virtual void ProcessEndOfBlendTime(Vector3 &objectPosition);

	const char *GetBlenderDataName() const { return GetBlenderData()->GetName(); }

#if __BANK

    //PURPOSE
    // Returns the last predicted position used by the blender
    const Vector3 &GetLastPredictedPos() const { return m_LastPredictedPos; }

    //PURPOSE
    // Returns the last position snapshot used by the blender for predicting
    const Vector3 &GetLastSnapPos() const { return m_LastSnapPos; }

    //PURPOSE
    // Returns the last velocity snapshot used by the blender for predicting
    const Vector3 &GetLastSnapVel() const { return m_LastSnapVel; }

    //PURPOSE
    // Returns the timestamp of the snapshot used by the blender for predicting
    unsigned GetLastSnapTime() const { return m_LastSnapTime; }

    //PURPOSE
    // Returns last network time predicted to by the blender
    unsigned GetLastTargetTime() const { return m_LastTargetTime; }

    //PURPOSE
    // Returns whether z blending was disabled during the last blender update
    bool GetLastDisableZBlending() const { return m_LastDisableZBlending; }
		
    //PURPOSE
    // Returns the distance between the current and target position
    float  GetPositionDelta(bool &blendedTooFar) const;

    //PURPOSE
    // Returns whether the blender was reset (via the Reset() function) in the last frame
    bool WasResetLastFrame() const { return (m_LastFrameReset != 0) && (fwTimer::GetFrameCount() == (m_LastFrameReset + 1));}

#endif // __BANK

	//PURPOSE
	// Sets the maximum position delta multiplier (default is 1.0!)
	void SetMaxPositionDeltaMultiplier(float val) { m_MaxPositionDeltaMultiplier = val; }

    //PURPOSE
    // Returns whether the blender is ramping towards the target position
    bool GetIsBlendRamping() const { return m_IsRamping; }

    //PURPOSE
    // Returns the last time the blend target object was snapped to the target position
    u32 GetLastPopTime() const { return m_LastPopTime; }

	//PURPOSE
	// Set the last time this blender object was snapped to the target position
	void SetLastPopTime(u32 time) { m_LastPopTime = time; }

    //PURPOSE
    // Returns the time between the present and past snapshots being taken
    virtual u32 GetTimeBetweenUpdates() const;

    //PURPOSE
    // Returns the difference between the predicted position at the time a new position
    // update was received from the network and the position received
    float GetPredictionError() const { return m_PredictionError; }

    //PURPOSE
    // Returns the current velocity change the blender will apply this frame
    float GetCurrentVelChange() const { return m_CurrentVelChange; }

    //PURPOSE
    // Returns the last velocity error amount detected by the blender (position change less than velocity suggests)
    float GetLastVelocityError() const { return m_FrameRateError.GetLastValueAdded(); }

    //PURPOSE
    // Returns the average velocity error amount detected by the blender over a time interval (position change less than velocity suggests)
    float GetVelocityError() const { return m_FrameRateError.GetAverage(); }

    //PURPOSE
    // Returns the peak velocity error amount detected by the blender over a time interval (position change less than velocity suggests)
    float GetVelocityErrorPeak() const { return m_FrameRateError.GetHighest(); }

    //PURPOSE
    // Returns the current blender mode name
    const char *GetBlenderModeName() const;

    //PURPOSE
    // Returns whether the blender should use logarithmic blending to reach the target position
    bool IsUsingLogarithmicBlending() const { return m_UseLogarithmicBlending || m_UseLogarithmicBlendingThisFrame; }

    //PURPOSE
    // Sets whether the blender should use logarithmic blending to reach the target position
    void SetUseLogarithmicBlending(bool useLogarithmicBlending) { m_UseLogarithmicBlending = useLogarithmicBlending; }

    //PURPOSE
    // Sets whether the blender should use logarithmic blending to reach the target position for this frame only
    void UseLogarithmicBlendingThisFrame() { m_UseLogarithmicBlendingThisFrame = true; }

    //PURPOSE
    // Returns whether the blender has stopped predicting the objects position (due to not receiving a position update for too long)
    bool HasStoppedPredictingPosition() const;

	//PURPOSE
	// Returns whether the blender should not blend Z positions for the target object
	virtual bool DisableZBlending() const { return false; }

protected:

    //PURPOSE
    // Returns the blender data
    virtual CLinInterpBlenderData *GetBlenderData() { return static_cast<CLinInterpBlenderData *>(netBlender::GetBlenderData()); }
    virtual const CLinInterpBlenderData *GetBlenderData() const { return static_cast<const CLinInterpBlenderData *>(netBlender::GetBlenderData()); }

    //PURPOSE
    // Returns whether the blender should only predict future positions projected to the object's forward vector
    virtual bool OnlyPredictRelativeToForwardVector() const { return false; }

    //PURPOSE
    // Returns whether the blender should not blend in the opposite direction of the target velocity of the object
    //PARAMS
    // positionDelta - the difference in metres between the predicted and current positions
    virtual bool PreventBlendingAwayFromVelocity(float UNUSED_PARAM(positionDelta)) const { return false; }

    //PURPOSE
    // Returns whether the blender needs to do a position correction due to the height difference between
    // the predicted and current position. By default this is not done.
    //PARAMS
    // heightDelta - the difference in height between the predicted and current positions
    virtual bool DoNoZBlendHeightCheck(float UNUSED_PARAM(heightDelta)) const { return false; }

    //PURPOSE
    // Returns whether the blender should use prediction or the last target position when calling GoStraightToTarget()
    virtual bool GoStraightToTargetUsesPrediction() const { return true; }

    //PURPOSE
    // Returns the predicted position of the target object for the next frame
    //PARAMS
    // predictedCurrentPosition - The current predicted position
    // targetVelocity           - The target velocity of the object
    virtual const Vector3 GetPredictedNextFramePosition(const Vector3 &predictedCurrentPosition,
                                                        const Vector3 &targetVelocity) const;

    //PURPOSE
    // Blender modes - allows separate blender data to be used for different conditions
    enum BlenderMode
    {
        BLEND_MODE_LOW_SPEED,   // Low speed mode - used for blending slower moving objects
        BLEND_MODE_NORMAL,      // Normal mode
        BLEND_MODE_HIGH_SPEED   // High speed mode - used for blending fast moving objects
    };

    //PURPOSE
    // Returns the maximum position delta before snapping to the target position based on the blender update level
    float GetPositionMaxForUpdateLevel() const;

    //PURPOSE
    // Returns the previous position of the blend target object (last frame)
    virtual const Vector3  GetPreviousPositionFromObject() const = 0;

    //PURPOSE
    // Returns the current position of the blend target object
    virtual const Vector3  GetPositionFromObject() const = 0;

    //PURPOSE
    // Returns the current velocity of the blend target object
    virtual const Vector3  GetVelocityFromObject() const = 0;

    //PURPOSE
    // Returns the current forward vector of the blend target object
    virtual const Vector3  GetForwardVectorFromObject() const = 0;

    //PURPOSE
    // Sets the position of the blend target object
    //PARAMS
    // position - The new position
    // warp     - Whether the object is being snapped to the new position
    virtual bool SetPositionOnObject(const Vector3 &position, bool warp) = 0;

    //PURPOSE
    // Called when the blend target object has been warped to it's new position due to
    // getting to far away from the target position
    virtual void OnPredictionPop() const {};

    //PURPOSE
    // Sets the velocity of the blend target object
    //PARAMS
    // velocity - The new velocity
    virtual void SetVelocityOnObject(const Vector3 &velocity) = 0;

    //PURPOSE
    // Sets the current blender mode
    //PARAMS
    // blenderMode - The new blender mode
    void SetBlenderMode(BlenderMode blenderMode);

    netLinInterpPredictionData m_PredictionData;                  // Data used for prediction
    u32                        m_BlendLinInterpStartTime;         // The time to start blending upon receiving an update
    bool                       m_BlendingPosition;                // Whether we are current blending
    bool                       m_IsRamping;                       // Whether we are blend ramping
    bool                       m_PerformedAfterBlendCorrection;   // Whether an after blend correction has been applied since the last position update
    u32                        m_LastPopTime;                     // The last pop time
    float                      m_PredictionError;                 // The difference between the predicted position when a new position arriving from an update
    float                      m_CurrentVelChange;                // The current velocity change to accelerate towards to the target position
    BlenderMode                m_BlenderMode;                     // The current blender mode (Normal, or Velocity Error mode)
    bool                       m_UseLogarithmicBlending;          // Indicates whether the blender should use logarithmic blending
    bool                       m_UseLogarithmicBlendingThisFrame; // Indicates whether the blender should use logarithmic blending for this frame only
	float                      m_MaxPositionDeltaMultiplier;      // The multiplier for all pop distances (can be set through script, default should be 1!)

    // Keeps track of the error between how far the object has moved relative to it's specified velocity. The physics code always runs at a fixed frame rate.
    // When a remote machine is running at a frame rate lower than this, objects they are controlling will not move as far as expected.
    // The error rate is tracked a ratio between the expected movement and actual movement.
    static const unsigned MAX_NUM_SAMPLES = 15;
    static const unsigned SAMPLE_INTERVAL = 500;
    DataAverageCalculator<MAX_NUM_SAMPLES, SAMPLE_INTERVAL> m_FrameRateError;

protected:

    //PURPOSE
    // Specifies how the blending should be done in logarithmic mode.
    enum LogarithmicBlendMode
    {
        LOGARITHMIC_BLEND_USE_VEL, // adjust the object's velocity to move towards the target
        LOGARITHMIC_BLEND_USE_POS  // set the objects's position closer to the target each frame
    };

    //PURPOSE
    // Use logarithmic interpolation to blend between positions. This performs no prediction
    // so will lag behind on remote machine, but will never blend further the target and is
    // guaranteed to always blend to the last position received
    //PARAMS
    // targetPosition - target position to logarithmically blend to
    // blendMode      - indicates how the blend should be done
    void DoLogarithmicBlend(const Vector3 &targetPosition, LogarithmicBlendMode blendMode);

    //PURPOSE
    // Returns velocity change data based on the current blender mode
    const VelocityChangeData &GetCurrentVelocityChangeData() const;

#if __BANK
    //PURPOSE
    // Updates the position delta between the current and target position, this is done
    // at the same time the blender is processed so debug displays show accurate values
    void UpdatePositionDelta();

	//PURPOSE
	// Debug function for catching bad target velocities being set on the blender for the target object
	//PARAMS
	// velocity - The velocity to validate.
	virtual void ValidateTargetVelocity(const Vector3 &UNUSED_PARAM(velocity)) {}

	Vector3  m_LastPredictedPos;           // The last predicted position used by the blender
	Vector3  m_LastSnapPos;                // The last position snapshot used by the blender for predicting
	Vector3  m_LastSnapVel;                // The last velocity snapshot used by the blender for predicting
	unsigned m_LastSnapTime;               // The timestamp of the snapshot used by the blender for predicting
	unsigned m_LastTargetTime;             // The last network time predicted to by the blender
    unsigned m_LastFrameReset;             // The last frame the blender was reset (via the Reset() function)
	float    m_PositionDelta;              // The distance between the last predicted position and current position
	bool     m_LastDisableZBlending;       // Indicates whether z blending was disabled during the last blender update
	bool     m_BlendedTooFar;              // Indicates whether the object has blended past the target position
	
public:
	static bool ms_bDebugNetworkBlend_UpdatePosition;
#endif // __BANK
};

} // namespace rage

#endif  // NETBLENDERLININTERP_H
