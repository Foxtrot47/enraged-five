// 
// netSyncTree.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETWORK_SYNC_TREE_H
#define NETWORK_SYNC_TREE_H

#include "fwnet/nettypes.h"
#include "fwnet/netsyncnode.h"
#include "fwnet/netsyncdataul.h"
#include "fwnet/netutils.h"
#include "fwsys/timer.h"

namespace rage
{
class netSyncNodeBase;
class netSyncParentNode;
class netSyncDataNode;
class netSyncDataBase;
class netINodeDataAccessor;

class netSyncTree;

class netLoggingInterface;
class netPlayer;

//PURPOSE
// Any objects referenced by a sync tree must derive from this class
class netSyncTreeTargetObject 
{
protected:

	virtual ~netSyncTreeTargetObject() {}

public:

    // returns a reference to the data accessor for this class
    virtual netINodeDataAccessor *GetDataAccessor(unsigned dataAccessorType) = 0;

	// returns true if the object has sync data 
	virtual bool HasSyncData() const = 0;

	// returns the sync data used with the sync tree
	virtual netSyncDataBase* GetSyncData()= 0;

	// returns the sync tree used by the network object
	virtual netSyncTree* GetSyncTree() = 0;

    // returns the sync data used with the sync tree
	virtual const netSyncDataBase* GetSyncData() const = 0;

	// returns the sync tree used by the network object
	virtual const netSyncTree* GetSyncTree() const = 0;

	// returns true if the network object has an associated game object
	virtual bool HasGameObject() const = 0;

	// creates a new sync data for this object, which is passed into m_syncData
	virtual netSyncDataBase* CreateSyncData() = 0;

    // returns which players this object has been created on
    virtual PlayerFlags GetClonedState() const = 0;

    // returns a bitfield indicating which players were checked for sending sync updates (used for batching)
    virtual PlayerFlags GetLastPlayersSyncUpdated() const = 0;

    // return the minimum update level for the object
    virtual u8 GetMinimumUpdateLevel() const = 0;

    // return the update level for the object for the specified player
    virtual u8 GetUpdateLevel(PhysicalPlayerIndex playerIndex) const = 0;

	// returns the name of the object for logging purposes
	virtual const char * GetLogName() const = 0;

	// returns whether the object can accept any node data
	virtual bool CanApplyNodeData() const = 0;

	// returns true if the data node being processed has any node dependencies. Can be overridden to allow for dynamic node dependencies.
	virtual bool HasNodeDependencies(const netSyncDataNode& dataNode) const;

	// returns the dependent nodes to be updated. Can be overridden to allow for dynamic node dependencies.
	virtual void GetAllNodeDependencies(netSyncDataNode& dataNode, netSyncDataNode **nodeDependencies, const unsigned arraySize, unsigned &numDependentNodes);

};

///////////////////////////////////////////////////////////////////////////////////////
//
// A network sync tree is a tree of sync nodes and is used to process data held in
// a message buffer. There are two types of nodes: parent and data nodes. Parent nodes 
// manage a list of children and flag whether their children have any data to read or
// write. Data nodes are leaf nodes and represent a block of data used by a network object. 
// These have no children and read or write their data to a message buffer.
//
// The reason for the tree is to reduce the amount of data sent in an update and to 
// simplify the process of adding or rearranging existing data. To add new data we can just
// create a new node, specify what data it serializes, then add it to the tree at the 
// appropriate place. 
//
// Only one static instance of the tree exists for each network object type. This is so 
// that the tree can be used to process data for an object that does not exist, or it
// can be used by network bots, which are not real network objects. The netSyncData
// used by the nodes must be allocated out with the tree and held in the object that is 
// using the tree. The tree stores a pointer to the current object that is using it and 
// the nodes use this pointer to get at the network sync data.
//
///////////////////////////////////////////////////////////////////////////////////////

class netSyncTree
{
public:

    static const unsigned MAX_BATCHES = 20;

	netSyncTree();
	virtual ~netSyncTree();

	unsigned GetNumSyncUpdateNodes() const				{ return m_NumSyncUpdateNodes; }

	unsigned GetMaxSizeOfBufferDataInBits() const		{ return m_MaxBufferDataSize; }
	unsigned GetMaxSizeOfBufferDataInBytes() const		{ return ((m_MaxBufferDataSize + (8-m_MaxBufferDataSize%8))>>3); }

#if __BANK
    unsigned GetNumTimesUpdateCalled() const            { return m_NumTimesUpdateCalled; }
    void     ResetNumTimesUpdateCalled()                { m_NumTimesUpdateCalled = 0; }

    float    GetAverageReadsPerUpdate() const           { return m_AverageReadsPerUpdate.GetAverage();  }
    float    GetAverageWritesPerUpdate() const          { return m_AverageWritesPerUpdate.GetAverage(); }

    const char *GetNodeName(unsigned nodeIndex) const
    {
        if(nodeIndex < m_NumSyncUpdateNodes && m_SyncUpdateNodes[nodeIndex]) 
        {
            return m_SyncUpdateNodes[nodeIndex]->GetNodeName();
        } 
        else
        {
            gnetAssertf(0, "Trying to get the node name for a node not in the tree!");
            return "Invalid Node!";
        }
    }
#endif // __BANK

#if __ASSERT
    void SetIgnoreBatchAssert(bool ignore) { m_IgnoreBatchAssert = ignore; }
#endif // __ASSERT

	//PURPOSE
	// Returns whether the last write operation failed to write all data to the specified buffer (generally due to a lack of space)
	bool HasFailedToWriteAllData() const { return m_FailedToWriteAllData; }

	//PURPOSE
	// Marks that the last write operation failed to write all data to the specified buffer (generally due to a lack of space)
	// This is called by the sync node classes during write operations.
	void MarkFailedToWriteAllData() { m_FailedToWriteAllData = true; }


	//PURPOSE
	// Adds the given node to the m_DataNodes and m_SyncDataNodes array at the given indicies. 
	// Called by a data node when it is initialised in the tree (via InitialiseDataNodeInTree())
	void SetDataNodeIndices(netSyncDataNode* pNode, unsigned dataIndex, int syncDataIndex = -1);

    //PURPOSE
    // Returns the number of allowed batches of objects of the type synchronised by the tree at the specified update level
    u8 GetNumAllowedBatches(unsigned updateLevel) const;

    //PURPOSE
    // Increases the count of the number of objects that are at the specified update level and batch
    //PARAMS
    // updateLevel - update level of the object
    // batch       - batch the object is in
    void IncreaseObjectCount(unsigned updateLevel, unsigned batch);

    //PURPOSE
    // Increases the count of the number of objects that are at the specified update level and batch
    //PARAMS
    // updateLevel - update level of the object
    // batch       - batch the object is in
    void DecreaseObjectCount(unsigned updateLevel, unsigned batch);

    //PURPOSE
    // Returns the number of objects in the specified batch for the specified update level
    //PARAMS
    // updateLevel - update level to check
    // batch       - batch to check
    unsigned GetObjectCount(unsigned updateLevel, unsigned batch) const;
    
    //PURPOSE
    // Returns which batch to assign an object to at the specified update level. This
    // is used to balance the number of objects at the different number of batches
    //PARAMS
    // updateLevel - update level of the object
    u8 GetBatchForNewObject(unsigned updateLevel) const;

    //PURPOSE
    // Checks whether an object at the specified update level and batch should be moved
    // to another batch for balancing purposes
    //PARAMS
    // updateLevel - update level to check
    // batch       - batch to check
    u8 TryToMoveBatch(unsigned updateLevel, u8 batch);

    //PURPOSE
    // Returns the current update batch for the specified update level
    u8 GetCurrentUpdateBatch(u8 updateLevel) const 
    {
        ASSERT_ONLY(gnetAssertf(m_IgnoreBatchAssert || (m_LastObjectBatchTick == fwTimer::GetSystemFrameCount()), "Trying to get current update batch before calling UpdateCurrentObjectBatches()!");)
        return m_BatchUpdateInfo[updateLevel].m_CurrentBatchToProcess; 
    }

	//PURPOSE
	//	Called immediately after the tree has been constructed. 
	//  Initialises all of start write positions of the data for each node
	//  when writing the data to the current state buffer and shadow buffer
	void InitialiseTree();

	//PURPOSE
	//	Called immediately before the tree is destroyed. 
	void ShutdownTree();

	//PURPOSE
	//	Initialises the sync data. Called immediately after the target object is created.
	//PARAMS
	//	pObj - the target object.
	void InitialiseData(netSyncTreeTargetObject* pObj);

    //PURPOSE
    //  Updates which batches of objects are due for processing this update. This should be called
    //  once per frame
    //PARAMS
    //  currTime - the current sync time
    void UpdateCurrentObjectBatches(const unsigned currTime);

	//PURPOSE
	//	Updates all nodes and determines which nodes are dirty (if they use a shadow buffer),
    //  and which nodes need to send updates to the remote players in the session
	//PARAMS
	//	pObj              - the target object.
    //  actFlags          - the activation flags for the object - so we only update necessary nodes
	//  currTime	      - the current sync time
    //  checkNodeForDirty - indicates whether the node should be checked to see if their state has changed
	void Update(netSyncTreeTargetObject* pObj, ActivationFlags actFlags, const unsigned currTime, bool checkNodeForDirty = true);

	//PURPOSE
	//	Writes the tree data to a message buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	//	bitBuffer	- the message buffer.
	//  currTime	- the current sync time
	//  pLog		- if this is set the message buffer data is written to the log as it is written
	//	player		- the player we are sending the data to.
	//  pNodeFlags	- A bitfield where each set bit corresponds to a data node that wrote some data to the message buffer. For update messages this is
	//				  stored and passed into ProcessAck when an ack arrives from another machine. 
	//RETURNS
	//	true if any data was written 
	virtual bool Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, const unsigned currTime, 
				netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags = NULL);

	//PURPOSE
	//	Reads the node data from a message buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	bitBuffer	- the message buffer received in an update message
	//  pLog		- if this is set the message buffer data is written to the log as it is read
	void Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog);

    //PURPOSE
    //	Calculates the maximum data size that can be written for the tree with the specified serialisation mode and
    //  activation flags. This is the amount of data that would be written if all nodes were dirty and ready to send
    //  at the same time. Optionally writes the total data size and the data size of the individual nodes to the TTY output
    //  for debugging purposes
    //PARAMS
    //  serMode		- the serialisation mode, (eg update, create, migrate) 
    //  actFlags	- flags to activate or deactivate certain nodes
    unsigned GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool outputToTTY);

	//PURPOSE
	//	Sets the bits in the node flags for nodes that are dirty
	void GetDirtyNodes(netSyncTreeTargetObject* pObj, DataNodeFlags& nodeFlags);

	//PURPOSE
	//	Dirties a single node 
	void DirtyNode(netSyncTreeTargetObject* pObj, netSyncDataNode& dataNode);

	//PURPOSE
	//	Dirties the nodes that have their corresponding bit set in the node flags
	void DirtyNodes(netSyncTreeTargetObject* pObj, DataNodeFlags& nodeFlags);

	//PURPOSE
	//	Returns TRUE if the node is synched with player with index playerIndex
	bool IsNodeSyncedWithPlayer(netSyncTreeTargetObject* pObj, const netSyncDataNode& dataNode, const unsigned playerIndex) const;

	//PURPOSE
	//	Returns TRUE if the node is synched with a group of players
	bool IsNodeSyncedWithPlayers(netSyncTreeTargetObject* pObj, const netSyncDataNode& dataNode, PlayerFlags playersMask) const;

	//PURPOSE
	//	Returns TRUE if the node is synched with all players
	bool IsNodeSyncedWithAllPlayers(netSyncTreeTargetObject* pObj, const netSyncDataNode& dataNode) const;

	//PURPOSE
	//	Returns true if the data held in the recently updated nodes can be applied to the object
	bool CanApplyNodeData(netSyncTreeTargetObject* pObj);

	//PURPOSE
	//	Applies the data held in the recently updated nodes to the target object
	//PARAMS
	//	pObj			- the target object.
	//  updatedNodes	- flags indicating which nodes had data applied
	virtual void ApplyNodeData(netSyncTreeTargetObject* pObj, DataNodeFlags* updatedNodes = NULL);

	//PURPOSE
	//	Logs the data held in the recently updated nodes 
	//PARAMS
	//	log			- the log that will be written to
	virtual void LogData(netLoggingInterface& log);

	//PURPOSE
	//	Called after the data in the updated nodes is sent to another player, registers the sequence number of the message with the sync data
	//PARAMS
	//	pObj		- the object using the tree
	//	player		- the player we are sending 
	//	pMsgSeq		- the sequence number of the update message (if NULL then the data does not need an ack. eg if it is part of a create message)
	//  currTime    - the current sync data time
	void NodeDataSent(netSyncTreeTargetObject* pObj, const PhysicalPlayerIndex player, const netSequence* pMsgSeq);

	//PURPOSE
	//  Handles an ack received from another player for an update message sent by our player.
	//PARAMS
	//	pObj		         - the object using the tree
	//	player		         - the player we received the ack from
    //  sequence             - the sequence number of the message being acked
	//	timeAckedMessageSent - the time the message being acked was sent
	//	nodeFlags	         - flags indicating which nodes wrote data to the update message
	void ProcessAck(netSyncTreeTargetObject* pObj, const PhysicalPlayerIndex player, const netSequence sequence, const unsigned timeAckedMessageSent, const DataNodeFlags nodeFlags);

	//PURPOSE
	//	Forces a node to send its data immediately 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	void ForceSendOfNodeData(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, netSyncDataNode& dataNode);

    //PURPOSE
	//	Forces a node to send its data immediately to the specified player
	//PARAMS
    //  player      - the player to forcibly send the data to
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	void ForceSendOfNodeDataToPlayer(const PhysicalPlayerIndex player, SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, netSyncDataNode& dataNode);

	//PURPOSE
	//	Forces the sending of the data of the sync update nodes using the given serialise mode 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	void ForceSendOfSyncUpdateNodes(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj);

#if __BANK
	const char*	GetTreeName() const { return m_TreeName; }
	void		SetTreeName(const char* treeName);

	//PURPOSE
	//	Adds bank widgets for the nodes in this tree
	void AddWidgets(bkBank *bank);

	//PURPOSE
	//	Displays the the tree nodes data on screen 
	virtual void DisplayNodeInformation(netSyncTreeTargetObject* pObj, netLogDisplay& displayLog);
#elif !__NO_OUTPUT
	const char*	GetTreeName() const { return ""; }
#endif

	const atFixedArray<netSyncDataNode*, netSyncDataNode::MAX_SYNC_DATA_NODES>& GetDataNodes() const { return m_DataNodes; }

protected:

	//PURPOSE
	//	Sets the current target object (the object currently using the tree)
	void SetTargetObject(netSyncTreeTargetObject* pObj) const;

	//PURPOSE
	// Adds a new parent node as a child of the current parent, and sets the current parent to be this node.
	void PushParent(netSyncParentNode& parentNode);

	//PURPOSE
	// Changes the current parent to the parent of the current parent (moves up a level in the tree)
	void PopParent();

	//PURPOSE
	// Forces the current parent to the given parent node, which already exists in the tree
	void SetCurrentParent(netSyncParentNode& parentNode);

	//PURPOSE
	//	Adds a child data node to the current pushed parent node.
	//PARAMS
	//  serMode		 - the serialisation flags for the new child node
	//  bConditional - if true, the conditional flags are set for all flags set in serFlags, otherwise they are all left unset
	void AddChild(netSyncDataNode& childNode);

	//PURPOSE
	//	Adds a child data node to the current pushed parent node, and sets the flags on that child.
	//PARAMS
	//  serMode				- the serialisation flags for the new child node
	//  conditionalFlags	- the conditional flags for the new child node
	//  actFlags			- the activation flags for the new child node
	void AddChild(netSyncDataNode& childNode, SerialiseModeFlags serFlags, SerialiseModeFlags conditionalFlags, ActivationFlags actFlags);

    //PURPOSE
	//	Writes the activation flags to the specified message buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	bitBuffer	- the message buffer.
	//  pLog		- if this is set the message buffer data is written to the log as it is written
    virtual void WriteActivationFlags(SerialiseModeFlags UNUSED_PARAM(serMode), ActivationFlags UNUSED_PARAM(actFlags), datBitBuffer& UNUSED_PARAM(bitBuffer), netLoggingInterface* UNUSED_PARAM(pLog)) {}

    //PURPOSE
	//	Reads the activation flags from the specified message buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	bitBuffer	- the message buffer.
	//  pLog		- if this is set the message buffer data is written to the log as it is written
    virtual void ReadActivationFlags(SerialiseModeFlags UNUSED_PARAM(serMode), ActivationFlags &UNUSED_PARAM(actFlags), datBitBuffer& UNUSED_PARAM(bitBuffer), netLoggingInterface* UNUSED_PARAM(pLog)) {}

	//PURPOSE
	//	Called for a node when it is added to the tree
	void RegisterNode(netSyncNodeBase* pNewNode);

	//PURPOSE
	//  Clears the updated flags for all nodes
	void ClearUpdatedFlags();

	//PURPOSE
	//	Applies the data held in the recently updated nodes to the target object
	virtual void PostApplyData(netSyncTreeTargetObject*) {}

private:

    //PURPOSE
    // Calculates the number of batches of objects a tree of this type supports. This is based
    // on the minimum update frequency of the nodes in the tree. The allows different groups of
    // objects of the same type to be updated in different frames, as a performance optimisation
    void CalculateNumAllowedBatches();

protected:

	mutable netSyncTreeTargetObject*	m_TargetObject;							// the current target object (the object currently using the tree)
	netSyncParentNode*					m_RootNode;								// the root node of the tree
	netSyncParentNode*					m_CurrentParent;						// the current pushed parent, any children added will be added to this parent's child list.
	unsigned							m_NumDataNodes;							// the total number of data nodes in the tree
	unsigned							m_NumSyncUpdateNodes;					// the number of leaf data nodes that require a corresponding sync data unit to represent them in the sync data 
	unsigned							m_MaxBufferDataSize;					// the maximum size of the data (in bits) that the tree can potentially write to a message buffer
	unsigned                            m_MaxNumHeaderBits;						// the maximum number of header bits that can be written by the tree (used when calculating whether data can fit in the supplied buffer when writing)

	atFixedArray<netSyncDataNode*, netSyncDataNode::MAX_SYNC_DATA_NODES>	m_DataNodes;		// pointers to all data nodes in the tree
	atFixedArray<netSyncDataNode*, netSyncDataNode::MAX_UPDATE_DATA_NODES>	m_SyncUpdateNodes;	// pointers to all the sync update data nodes

#if __BANK
	static const int			TREE_NAME_LEN = 50;
	char						m_TreeName[TREE_NAME_LEN];	//	the name of the tree (used for debugging)
#endif

private:

    u8 m_NumAllowedBatches             [CNetworkSyncDataULBase::NUM_UPDATE_LEVELS];
    u8 m_NumObjectsInBatchAtUpdateLevel[CNetworkSyncDataULBase::NUM_UPDATE_LEVELS][MAX_BATCHES];

    struct BatchUpdateInfo
    {
        u8       m_CurrentBatchToProcess;
        u8       m_NextBatchToProcess;
        unsigned m_BatchUpdateFrequency;
        unsigned m_NextBatchUpdateTime[MAX_BATCHES];
    };

    BatchUpdateInfo m_BatchUpdateInfo[CNetworkSyncDataULBase::NUM_UPDATE_LEVELS];

	 bool m_FailedToWriteAllData; // used to track when the last write operation failed to write all data to the specified buffer (generally due to a lack of space)

#if __BANK
    unsigned m_NumTimesUpdateCalled; // keeps track of how many times the Update() function has been called for profiling purposes
    unsigned m_NumTimesReadCalled;   // keeps track of how many times the Read()   function has been called for profiling purposes
    unsigned m_NumTimesWriteCalled;  // keeps track of how many times the Write() function has been called for profiling purposes

    static const unsigned MAX_UPDATE_SAMPLES = 30;
    static const unsigned SAMPLE_INTERVAL    = 1000;
    DataAverageCalculator<MAX_UPDATE_SAMPLES, SAMPLE_INTERVAL> m_AverageReadsPerUpdate;
    DataAverageCalculator<MAX_UPDATE_SAMPLES, SAMPLE_INTERVAL> m_AverageWritesPerUpdate;
#endif // __BANK

#if __ASSERT
    bool     m_TreeInitialised;
    bool     m_IgnoreBatchAssert;
    unsigned m_LastObjectBatchTick;
#endif
};

} // namespace rage

#endif  // NETWORK_SYNC_TREE_H
