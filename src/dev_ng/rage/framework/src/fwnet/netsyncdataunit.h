// 
// netSyncDataUnit.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_SYNC_DATA_UNIT_H
#define NET_SYNC_DATA_UNIT_H

//rage includes
#include "atl/array.h"
#include "data/bitbuffer.h"
#include "net/netsequence.h"
#include "system/new.h"

//framework includes
#include "fwnet/netchannel.h"
#include "fwnet/nettypes.h"

namespace rage
{
	class netSyncDataBase;

//PURPOSE
//	Manages the synchronisation of a single indivisible unit of data. Stores where this data is 
// located in a current state or shadow buffer and keeps track of when this data  
// has become dirty and needs to be sent to another player. Dirty data is detected by
// comparing the unit data held in the current state and shadow buffers. Updates are sent 
// regularly to a player while the data is being dirtied. Once it stops being dirtied
// then updates are sent less frequently until we get an ack back for the last
// bit of dirty data (i.e. we know that the player at the other end definitely has the
// current state of the data)
class netSyncDataUnitBase
{
	friend class netSyncDataBase;

public:
	netSyncDataUnitBase() : 
	  m_UnsyncedPlayers(0),
	  m_UpdatePlayers(0),
	  m_SizeofCurrentData(0),
      m_SizeofShadowData(0),
      m_TimeLastDirtied(0)
	{
	}

	virtual ~netSyncDataUnitBase() {}

	// =============== PURE VIRTUAL FUNCTIONS =======================

	//PURPOSE
	//	Returns the unacked message sequence number for the given player
	virtual netSequence&	   GetPlayerUnackedSequence(unsigned playerSyncDataIndex) = 0;
	virtual const netSequence& GetPlayerUnackedSequence(unsigned playerSyncDataIndex) const = 0;
	virtual void			   SetPlayerUnackedSequence(unsigned playerSyncDataIndex, netSequence seq) = 0;
	virtual void			   ClearPlayerUnackedSequence(unsigned playerSyncDataIndex) = 0;

	//PURPOSE
	//	Returns the maximum number of players this unit is synced with
	virtual unsigned			  GetMaxNumSyncedPlayers() const = 0;

    virtual void                  ClearAllUnackedSeqs() = 0;

	// ===============================================================

	PlayerFlags GetUnsyncedPlayers() const	{ return m_UnsyncedPlayers; }
	PlayerFlags GetUpdatePlayers() const	{ return m_UpdatePlayers; }

    u32 GetTimeLastDirtied() const { return m_TimeLastDirtied; }
    
	//PURPOSE
	// Initialises the sync data
	virtual void Init()
	{
		m_UnsyncedPlayers = m_UpdatePlayers = 0;

        m_TimeLastDirtied = 0;

		for (u32 playerSyncDataIndex=0; playerSyncDataIndex<GetMaxNumSyncedPlayers(); playerSyncDataIndex++)
		{
			ClearPlayerUnackedSequence(playerSyncDataIndex);
		}
	}

	//PURPOSE
	// Resets the sync data
	virtual void Reset()
	{
		netSyncDataUnitBase::Init();
	}

	//PURPOSE
	// Shuts down the sync data
	virtual void Shutdown() {}

	//PURPOSE
	//	Returns true if the data is synced with the given player 
	bool IsSyncedWithPlayer(unsigned playerSyncDataIndex) const
	{
		return ((m_UnsyncedPlayers & (1<<playerSyncDataIndex)) == 0);	
	}

	//PURPOSE
	//	Returns true if the data is synced with the given players 
	bool IsSyncedWithPlayers(PlayerFlags playersMask) const
	{
		return ((m_UnsyncedPlayers & playersMask) == 0);	
	}

	//PURPOSE
	//	Flags the data represented by the sync unit as not being dirty (changed) relative to the given player
	void ClearDirtyForPlayer(unsigned playerSyncDataIndex)
	{
		ClearPlayerUnackedSequence(playerSyncDataIndex);

		m_UnsyncedPlayers &= ~(1<<playerSyncDataIndex);
		m_UpdatePlayers &= ~(1<<playerSyncDataIndex);
	}

	//PURPOSE
	//	Sets the data as synced with all players.
	void SetSyncedWithAllPlayers()
	{
		m_UnsyncedPlayers = m_UpdatePlayers = 0;
	}

	//PURPOSE
	//	Flags the given player to receive an update of the sync unit's data
	void SetPlayerRequiresAnUpdate(unsigned playerSyncDataIndex)
	{
		m_UpdatePlayers |= (1<<playerSyncDataIndex);
	}

	//PURPOSE
	//	Returns true if given player is ready to receive an update of the sync unit's data
	bool GetPlayerRequiresAnUpdate(unsigned playerSyncDataIndex) const
	{
		return (m_UpdatePlayers & (1<<playerSyncDataIndex)) != 0;
	}

    //PURPOSE
	//	Sets the players that require an update of the sync unit's data
    void SetUpdatePlayers(PlayerFlags players)
    {
        m_UpdatePlayers = players;
    }

    //PURPOSE
    //  Flags the specified players as ready to receive an update of the sync unit's data
    void SetPlayersRequireAnUpdate(PlayerFlags players)
    {
        m_UpdatePlayers |= players;
    }

	//PURPOSE
	//	Flags the given player to receive an update of the sync unit's data
	void SetAllPlayersRequireAnUpdate()
	{
		m_UpdatePlayers = ~0U;
	}

	//PURPOSE
	// Called when the data represented by the sync unit is sent to a player
	//PARAMS
	//  playerSyncDataIndex     - the index of the player we are sending the data to
	//	pMsgSeq		            - the sequence number of the update message (if NULL then the data does not need an ack. eg if it is part of a create message)
    //  forceSetUnackedSequence - forces an update to the unacked sequence
	void UpdateSentToPlayer(unsigned playerSyncDataIndex, const netSequence* pMsgSeq)
	{
		if (!pMsgSeq)
		{
			// if there is no message sequence then this update was sent reliably in a create so we can clear the unsynced flag for this player
			m_UnsyncedPlayers &= ~(1<<playerSyncDataIndex);
		}
		else
		{
			// if the state is newly dirty, record the sequence number of this update message so we can wait for the appropriate ack.
			SetPlayerUnackedSequence(playerSyncDataIndex, *pMsgSeq);
		}

		m_UpdatePlayers &= ~(1<<playerSyncDataIndex);
	}

	//PURPOSE
	// Clears the update flags for the given players
	void ClearUpdateForPlayers(PlayerFlags players)
	{
		m_UpdatePlayers &= ~players;
	}

	//PURPOSE
	// Clears the update flags for the all players
	void ClearUpdateForAllPlayers()
	{
		m_UpdatePlayers = 0;
	}

	//PURPOSE
	// Called when we receive an ack from a remote player
	//PARAMS
	// playerSyncDataIndex  - sync data index for player we have received the ack from
	// timeAckedMessageSent - the time the message being acked was sent
	//RETURNS
	// true if the sequence number in the ack was one that we were waiting for 
	bool ReceivedAck(unsigned playerSyncDataIndex, const unsigned timeAckedMessageSent)
	{
		if(timeAckedMessageSent >= m_TimeLastDirtied)
		{
			ClearDirtyForPlayer(playerSyncDataIndex);

			return true;
		}

		return false;
	}

	//PURPOSE
	//	Sets the size of the data that was last written to the current state buffer for this sync unit.
	void SetSizeOfCurrentData(unsigned size)
	{ 
		m_SizeofCurrentData = static_cast<u16>(size); 
	}

	//PURPOSE
	//	Returns the size of the data that was last written to the current state buffer for this sync unit.
	unsigned GetSizeOfCurrentData() const
	{ 
		return m_SizeofCurrentData; 
	}

    //PURPOSE
	//	Sets the size of the data that was last written to the shadow buffer for this sync unit.
	void SetSizeOfShadowData(unsigned size)
	{ 
		m_SizeofShadowData = static_cast<u16>(size); 
	}

	//PURPOSE
	//	Returns the size of the data that was last written to the shadow buffer for this sync unit.
	unsigned GetSizeOfShadowData() const
	{ 
		return m_SizeofShadowData; 
	}

private: 

	//PURPOSE
	// Resets the data associated with a remote player, called when a new player joins.
	//PARAMS
	// playerIndex - the index of the joining player
	void InitialisePlayerSyncData(const unsigned playerSyncDataIndex, bool hasBeenDirtied)
	{
		ClearDirtyForPlayer(playerSyncDataIndex);

		if(hasBeenDirtied)
		{
			SetDirtyForPlayer(playerSyncDataIndex);
		}
	}

	//PURPOSE
	//	Forces the sync unit data to be sent to all players
	void ForceSend()
	{
		SetDirty();
		m_UpdatePlayers = ~0U;
	}

	//PURPOSE
	//	Forces the sync unit data to be sent to a specific players
	//PARAMS
	//  playerSyncDataIndex - the player to send the data to
	void ForceSendToPlayer(const u32 playerSyncDataIndex)
	{
		SetDirtyForPlayer(playerSyncDataIndex);

		if(gnetVerifyf(playerSyncDataIndex < MAX_NUM_PHYSICAL_PLAYERS, "Invalid sync data index"))
		{
			m_UpdatePlayers |= (1<<playerSyncDataIndex);
		}
	}

	//PURPOSE
	//	Flags the data represented by the sync unit as being dirty (changed)
	void SetDirty()
	{
		m_UnsyncedPlayers = ~0U;
		m_TimeLastDirtied = netInterface::GetSynchronisationTime();
	}

	//PURPOSE
	//	Flags the data represented by the sync unit as being dirty for the given player
	//PARAMS
	//  pParentSyncData - a ptr to the sync data that owns this unit
	void SetDirtyForPlayer(unsigned playerSyncDataIndex)
	{
		m_UnsyncedPlayers |= (1<<playerSyncDataIndex);
		m_TimeLastDirtied = netInterface::GetSynchronisationTime();
	}


protected:

	PlayerFlags	m_UnsyncedPlayers;	 // flags indicating which players do not have the current state of the data represented by this unit
	PlayerFlags m_UpdatePlayers;	 // players requiring an update this frame
	u16			m_SizeofCurrentData; // the size of the data that was last written to the current state buffer for this sync unit.
    u16			m_SizeofShadowData;  // the size of the data that was last written to the shadow buffer for this sync unit.
    u32         m_TimeLastDirtied;   // the time the data was last dirtied
};

//PURPOSE
//	The player sync data is statically allocated. 
//
// maxNumSyncedPlayers - the maximum number of players this unit can be synced with
//
template <int maxNumSyncedPlayers>
class netSyncDataUnit_Static : public netSyncDataUnitBase
{
public:

	virtual netSequence&			GetPlayerUnackedSequence(unsigned playerSyncDataIndex)		 { return m_PlayerUnackedSeqs[playerSyncDataIndex]; }
	virtual const netSequence&		GetPlayerUnackedSequence(unsigned playerSyncDataIndex) const { return m_PlayerUnackedSeqs[playerSyncDataIndex]; }
	virtual void					SetPlayerUnackedSequence(unsigned playerSyncDataIndex, netSequence seq) 
	{ 
		Assert(seq != 0);
        m_PlayerUnackedSeqs[playerSyncDataIndex] = seq;
	}
	virtual void					ClearPlayerUnackedSequence(unsigned playerSyncDataIndex) 
	{ 
		m_PlayerUnackedSeqs[playerSyncDataIndex] = 0; 
	}

	virtual unsigned GetMaxNumSyncedPlayers() const	{ return maxNumSyncedPlayers; }

    virtual void ClearAllUnackedSeqs()
    {
        for(int index = 0; index < maxNumSyncedPlayers; index++)
        {
            m_PlayerUnackedSeqs[index] = 0;
        }
    }

	void Init()
	{
		m_PlayerUnackedSeqs.Resize(maxNumSyncedPlayers);

		netSyncDataUnitBase::Init();
	}

	void Reset()
	{
		for (unsigned i=0; i<maxNumSyncedPlayers; i++)
		{
			m_PlayerUnackedSeqs[i] = 0;
		}

		netSyncDataUnitBase::Reset();
	}

	void Shutdown()
	{
		m_PlayerUnackedSeqs.Reset();

		netSyncDataUnitBase::Shutdown();
	}

protected:

	atFixedArray<netSequence, maxNumSyncedPlayers> m_PlayerUnackedSeqs; // the unacked message sequence numbers for each player
};

//PURPOSE
//	The player sync data is dynamically allocated. 
class netSyncDataUnit_Dynamic : public netSyncDataUnitBase
{
public:

	//PARAMS
	//	maxNumSyncedPlayers : the maximum number of players this unit can be synced with
	netSyncDataUnit_Dynamic(unsigned maxNumSyncedPlayers) : 
	m_MaxNumSyncedPlayers(static_cast<u16>(maxNumSyncedPlayers))
	{ 
	}
	
	virtual netSequence&			GetPlayerUnackedSequence(unsigned playerSyncDataIndex)		 { return m_PlayerUnackedSeqs[playerSyncDataIndex]; }
	virtual const netSequence&		GetPlayerUnackedSequence(unsigned playerSyncDataIndex) const { return m_PlayerUnackedSeqs[playerSyncDataIndex]; }
	virtual void					SetPlayerUnackedSequence(unsigned playerSyncDataIndex, netSequence seq) 
	{ 
		Assert(seq != 0);
        m_PlayerUnackedSeqs[playerSyncDataIndex] = seq;
	}
	virtual void					ClearPlayerUnackedSequence(unsigned playerSyncDataIndex) 
	{ 
		m_PlayerUnackedSeqs[playerSyncDataIndex] = 0;
	}

	virtual unsigned GetMaxNumSyncedPlayers() const	{ return static_cast<unsigned>(m_MaxNumSyncedPlayers); }

	virtual void ClearAllUnackedSeqs()
    {
		for(int index = 0; index < m_MaxNumSyncedPlayers; index++)
		{
			m_PlayerUnackedSeqs[index] = 0;
		}
    }

	void Init()
	{
		m_PlayerUnackedSeqs.Resize(m_MaxNumSyncedPlayers);

		netSyncDataUnitBase::Init();
	}

	void Reset()
	{
		for (unsigned i=0; i<m_MaxNumSyncedPlayers; i++)
		{
			m_PlayerUnackedSeqs[i] = 0;
		}

		netSyncDataUnitBase::Reset();
	}

	void Shutdown()
	{
		m_PlayerUnackedSeqs.Reset();

		netSyncDataUnitBase::Shutdown();
	}

protected:

	atArray<netSequence>	m_PlayerUnackedSeqs; // the unacked message sequence numbers for each player
	u16						m_MaxNumSyncedPlayers;
};

} // namespace rage

#endif  // NET_SYNC_DATA_UNIT_H
