//
// netobjectmgrmessagehandler.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#include "fwnet/netobjectmgrmessagehandler.h"

#include "fwnet/netinterface.h"
#include "fwnet/netchannel.h"
#include "fwnet/netbandwidthmgr.h"
#include "fwnet/netbandwidthstatscollector.h"
#include "fwnet/netserialisers.h"
#include "fwnet/netlogginginterface.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobject.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netplayer.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"
#include "fwsys/timer.h"

NETWORK_OPTIMISATIONS()

XPARAM(additionalBandwidthLogging);

namespace rage
{

netObjectMgrMessageHandler::netObjectMgrMessageHandler() :
m_ObjectManager(0)
, m_SendConnectionSyncImmediately(0)
#if __BANK
, m_NumCloneSyncMessagesSent(0)
, m_NumCloneSyncAckMessagesSent(0)
, m_NumPackedReliableMessagesSent(0)
#endif
{
    // Initialize our message handler.
    m_Dlgt.Bind(this, &netObjectMgrMessageHandler::OnMessageReceived);

    ResetCurrentFrameSyncTimestamps();

#if __DEV
    m_packedReliablesId = INVALID_RECORDER_ID;
    m_creationDataId    = INVALID_RECORDER_ID;
    m_syncHeaderDataId  = INVALID_RECORDER_ID;
    m_syncAckDataId     = INVALID_RECORDER_ID;
#endif
}

netObjectMgrMessageHandler::~netObjectMgrMessageHandler()
{
}

void netObjectMgrMessageHandler::Init(netObjectMgrBase &objectManager)
{
    m_ObjectManager = &objectManager;

    netInterface::AddDelegate(&m_Dlgt);

    m_SendConnectionSyncImmediately = 0;

    m_ConnectionSyncMsgs.Resize(MAX_NUM_ACTIVE_PLAYERS);
    m_PackedSyncAcks.Resize(MAX_NUM_ACTIVE_PLAYERS);
    m_PackedReliables.Resize(MAX_NUM_ACTIVE_PLAYERS);

    for (unsigned i=0; i<MAX_NUM_ACTIVE_PLAYERS; i++)
    {
        m_Sequence[i]             = 1;
        m_LastReceivedSequence[i] = INVALID_SEQUENCE_ID;
        m_CloneSyncPacketLossTrackers[i].Reset();
    }

    ResetCurrentFrameSyncTimestamps();

#if __BANK
        // register bandwidth stats
        m_ObjectManager->GetBandwidthMgr().RegisterStatistics(m_bandwidthStats, "Object Manager");

        // allocate bandwidth recorders for the different event manager messages
        m_packedReliablesId = m_bandwidthStats.AllocateBandwidthRecorder("Object packed reliables");
        m_creationDataId    = m_bandwidthStats.AllocateBandwidthRecorder("Object creates");
        m_syncHeaderDataId  = m_bandwidthStats.AllocateBandwidthRecorder("Object sync headers");
        m_syncAckDataId     = m_bandwidthStats.AllocateBandwidthRecorder("Object sync acks");

        m_NumCloneSyncMessagesSent      = 0;
        m_NumCloneSyncAckMessagesSent   = 0;
        m_NumPackedReliableMessagesSent = 0;
#endif
}

void netObjectMgrMessageHandler::Shutdown()
{
    netInterface::RemoveDelegate(&m_Dlgt);

    for (unsigned index = 0; index <MAX_NUM_ACTIVE_PLAYERS; index++)
    {
        m_ConnectionSyncMsgs[index].Reset();
        m_PackedSyncAcks[index].Reset();
        m_PackedReliables[index].Reset();
    }

    m_ConnectionSyncMsgs.Reset();
    m_PackedSyncAcks.Reset();
    m_PackedReliables.Reset();
}

void netObjectMgrMessageHandler::Update()
{
    // send any packed connection messages that remain
    unsigned                 numRemoteActivePlayers = netInterface::GetNumRemoteActivePlayers();
    const netPlayer * const *remoteActivePlayers    = netInterface::GetRemoteActivePlayers();
    
	for(unsigned index = 0; index < numRemoteActivePlayers; index++)
    {
        const netPlayer *player = remoteActivePlayers[index];

	    // hold off sending the messages if the game is already struggling to send existing ones
	    // this will just add latency as when the messages get full they will be sent
	    if((player->GetConnectionId() < 0) || !m_ObjectManager->GetBandwidthMgr().GetReliableMessagesHaveBeenUnackedTooLong(player->GetConnectionId()))
	    {
		    SendPackedSyncAckMessage(*player);
		    SendPackedReliableMessage(*player);
		    SendConnectionSyncMessage(*player);
	    }

        ActivePlayerIndex playerIndex = player->GetActivePlayerIndex();

        if(playerIndex < MAX_NUM_ACTIVE_PLAYERS)
        {
            m_CloneSyncPacketLossTrackers[playerIndex].Update();
        }
    }

    ResetCurrentFrameSyncTimestamps();
}

void netObjectMgrMessageHandler::PlayerHasJoined(const netPlayer& UNUSED_PARAM(player))
{
}

void netObjectMgrMessageHandler::PlayerHasLeft(const netPlayer& player)
{
    ActivePlayerIndex playerIndex = player.GetActivePlayerIndex();

    if(gnetVerify(playerIndex < MAX_NUM_ACTIVE_PLAYERS))
    {
        m_Sequence[playerIndex]             = 1;
        m_LastReceivedSequence[playerIndex] = INVALID_SEQUENCE_ID;

        m_CloneSyncPacketLossTrackers[playerIndex].Reset();

        m_ConnectionSyncMsgs[playerIndex].Reset();
        m_PackedSyncAcks    [playerIndex].Reset();
        m_PackedReliables   [playerIndex].Reset();
    }
}

netSequence netObjectMgrMessageHandler::GetSequenceNumberForPlayer(const netPlayer& player) const
{
    if(gnetVerifyf(player.GetActivePlayerIndex() < MAX_NUM_ACTIVE_PLAYERS, "Player index is out of range!"))
    {
        return m_Sequence[player.GetActivePlayerIndex()];
    }

    return INVALID_SEQUENCE_ID;
}

float netObjectMgrMessageHandler::GetPacketLoss(ActivePlayerIndex playerIndex) const
{
    if(gnetVerifyf(playerIndex < MAX_NUM_ACTIVE_PLAYERS, "Player index is out of range!"))
    {
        return m_CloneSyncPacketLossTrackers[playerIndex].GetPacketLossRatio();
    }

    return 0.0f;
}

void netObjectMgrMessageHandler::OnMessageReceived(const ReceivedMessageData &messageData)
{
    unsigned msgId = 0;
    if(netMessage::GetId(&msgId, messageData.m_MessageData, messageData.m_MessageDataSize))
    {
        bool bRecognised = true;
        bool bHandled    = false;

        if (msgId == cloneSyncMsg::MSG_ID())
        {
            bHandled = HandleCloneSyncMsg(messageData);
        }
        else if (msgId == packedCloneSyncACKsMsg::MSG_ID())
        {
            bHandled = HandlePackedCloneSyncAck(messageData);
        }
        else if (msgId == packedReliablesMsg::MSG_ID())
        {
            bHandled = HandlePackedReliablesMsg(messageData);
        }
        else
        {
            bRecognised = false;
        }

        if (!bHandled && bRecognised)
        {
            const int connectionId = messageData.m_FromPlayer ? messageData.m_FromPlayer->GetConnectionId() : INVALID_CONNECTION_ID;

            if (connectionId == INVALID_CONNECTION_ID)
            {
                if(m_Log)
                {
                    m_Log->WriteDataValue("IGNORE", "Unknown connection");
                }

                m_ObjectManager->GetLog().WriteDataValue("IGNORE", "Unknown connection");
            }
            else
            {
                gnetAssertf(0, "Unhandled a recognised message!");
            }

            if(m_Log)
            {
                m_Log->LineBreak();
            }
        }
    }
}

bool netObjectMgrMessageHandler::HandleCloneSyncMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        cloneSyncMsg msg;

        if(!gnetVerifyf(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize), "Failed importing clone sync message data!"))
        {
            return false;
        }

#if __BANK
        if(PARAM_additionalBandwidthLogging.Get())
        {
            gnetDebug2("Handling cloneSyncMsg with sequence %d", messageData.m_NetSequence);
        }
#endif // __BANK

        // get the sequence
        netSequence seq = msg.m_Sequence;

#if ENABLE_NETWORK_BOTS
        // don't process message sequencing for bots - assume they are always in order
        if(!messageData.m_ToPlayer->IsBot())
#endif // ENABLE_NETWORK_BOTS
        {
            // check for an out-of-order sequence
            netSequence lastSequence = GetLastSequenceReceivedFromPlayer(*messageData.m_FromPlayer);

            if(!netUtils::IsSeqGreater(seq, lastSequence, sizeof(netSequence)<<3))
            {
                if(m_Log)
                {
                    NetworkLogUtils::WriteMessageHeader(*m_Log, true, seq, *messageData.m_FromPlayer, "RECEIVED_CLONE_SYNC", "");

                    m_Log->WriteDataValue("IGNORE", "Out of sequence (last seq = %d)", lastSequence);
                }

                ObjectId         objectID = NETWORK_INVALID_OBJECT_ID;
                NetObjectAckCode ackCode  = ACKCODE_OUT_OF_SEQUENCE;
                AddSyncAcks(*messageData.m_FromPlayer, *messageData.m_ToPlayer, seq, 1, 1, &objectID, &ackCode);
                return true;
            }
            else
            {
                SetLastSequenceReceivedFromPlayer(*messageData.m_FromPlayer, seq);
            }
        }

        // cache the local IDs and ACK codes for sending after processing the entire message
        ObjectId         objectIDs[MAX_SYNC_ACKS];
        NetObjectAckCode ackCodes[MAX_SYNC_ACKS];
        unsigned numObjectsInMessage = 0;
        unsigned numFailedObjects    = 0;

        // keep reading messages until the packed msg is empty
        // (payload bit size is padded out to the nearest byte)
        const int payloadBitSize   = msg.m_BitBuffer.GetBitLength();
        int       bitsRemaining    = payloadBitSize - msg.m_BitBuffer.GetCursorPos();
        bool      timeStampChecked = false;

        while( bitsRemaining > (cloneSyncMsg::SIZEOF_TIMESTAMP + cloneSyncMsg::SIZEOF_NUM_OBJECTS))
        {
            int oldBitsRemaining = bitsRemaining;

            BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_syncHeaderDataId, cloneSyncMsg::SIZEOF_TIMESTAMP + cloneSyncMsg::SIZEOF_NUM_OBJECTS));

            // read the timestamp and number of objects
            u32 timestamp  = 0;
            u8  numObjects = 0;
            msg.m_BitBuffer.ReadUns(timestamp,  cloneSyncMsg::SIZEOF_TIMESTAMP);

            PhysicalPlayerIndex playerIndex = messageData.m_FromPlayer->GetPhysicalPlayerIndex();

            if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Processing a clone sync message from a player with an invalid physical player index!"))
            {
                // update latency metrics
                u32 networkTime = netInterface::GetTimestampForStartOfFrame();
                u32 latency     = (networkTime > timestamp) ? (networkTime - timestamp) : 0;
                m_ObjectManager->AddSyncLatencySample(playerIndex, latency);

                // we don't want to process too many clone sync messages from the same player each frame.
                // only process sync messages within a 50ms time interval and drop the rest
                if(!timeStampChecked)
                {
                    if(m_CurrentFrameSyncTimestamp[playerIndex] != 0)
                    {
                        u32 timeDiff = timestamp - m_CurrentFrameSyncTimestamp[playerIndex];

                        static const unsigned MAX_TIMESTAMP_DIFF = 100;

                        if(timeDiff > MAX_TIMESTAMP_DIFF)
                        {
                            if(m_Log)
                            {
                                NetworkLogUtils::WriteMessageHeader(*m_Log, true, seq, *messageData.m_FromPlayer, "RECEIVED_CLONE_SYNC", "");

                                m_Log->WriteDataValue("IGNORE", "Timestamp diff too large (this timestamp = %d, first timestamp = %d)", timestamp, m_CurrentFrameSyncTimestamp[playerIndex]);
                            }

                            return true;
                        }
                    }
                    else
                    {
                        m_CurrentFrameSyncTimestamp[playerIndex] = timestamp;
                    }
                }

                timeStampChecked = true;
            }

            msg.m_BitBuffer.ReadUns(numObjects, cloneSyncMsg::SIZEOF_NUM_OBJECTS);

            for(u8 index = 0; index < numObjects; index++)
            {
                BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_syncHeaderDataId, cloneSyncMsg::GetSyncHeaderBitSize()));

                u32 objectType = 0;
                u32 objectId   = 0;
                msg.m_BitBuffer.ReadUns(objectType, SIZEOF_OBJECTTYPE);
                msg.m_BitBuffer.ReadUns(objectId, SIZEOF_OBJECTID);

                int readBitPos  = msg.m_BitBuffer.GetCursorPos();
                int readBytePos = readBitPos>>3;
                int offset      = readBitPos - readBytePos*8;
                int bufferSize  = ((payloadBitSize - readBitPos)>>3) + 1;

                datBitBuffer msgBuffer;
                msgBuffer.SetReadOnlyBits(&((u8*)msg.m_BitBuffer.GetReadOnlyBits())[ readBytePos ],
                                            bufferSize<<3,
                                            offset);

				m_ObjectManager->SetForceCrashOnShutdownWhileProcessing(true); // this is temp code to try to help track down this crash: url:bugstar:7734502
                NetObjectAckCode ackCode = m_ObjectManager->ProcessCloneSync(*messageData.m_FromPlayer,
                                                                             *messageData.m_ToPlayer,
                                                                             static_cast<NetworkObjectType>(objectType),
                                                                             static_cast<ObjectId>(objectId),
                                                                             msgBuffer,
                                                                             seq,
                                                                             timestamp);

				m_ObjectManager->SetForceCrashOnShutdownWhileProcessing(false); // this is temp code to try to help track down this crash: url:bugstar:7734502
                if(AssertVerify(numObjectsInMessage < MAX_SYNC_ACKS))
                {
                    objectIDs[numObjectsInMessage] = (ObjectId)objectId;
                    ackCodes[numObjectsInMessage]  = ackCode;

                    if(ackCode != ACKCODE_NONE)
                    {
                        numFailedObjects++;
                    }

                    numObjectsInMessage++;
                }

                readBitPos += msgBuffer.GetCursorPos();
                msg.m_BitBuffer.SetCursorPos( readBitPos );

                // read tag to make sure everything is properly in sync
                GET_MSG_TAG(msg.m_BitBuffer);
            }

            bitsRemaining = payloadBitSize - msg.m_BitBuffer.GetCursorPos();

            // ensure we have actually read some more data than last time around this loop, otherwise we will get stuck
            if(bitsRemaining == oldBitsRemaining)
            {
                break;
            }
        }

        // send acks to all of the objects synced
        AddSyncAcks(*messageData.m_FromPlayer, *messageData.m_ToPlayer, seq, numObjectsInMessage, numFailedObjects, objectIDs, ackCodes);

        return true;
    }

    return false;
}

bool netObjectMgrMessageHandler::HandlePackedCloneSyncAck(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        packedCloneSyncACKsMsg packedCloneSyncAckMsg;

        if(!gnetVerifyf(packedCloneSyncAckMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize), "Failed importing clone sync ack message data!"))
        {
            return false;
        }

        const netSequence msgSeq = messageData.m_NetSequence;

        if(m_Log)
        {
            NetworkLogUtils::WriteMessageHeader(*m_Log, true, msgSeq, *messageData.m_FromPlayer, "PACKED_SYNC_ACK", "");
        }

        datBitBuffer &messageBuffer = packedCloneSyncAckMsg.m_BitBuffer;
        messageBuffer.SetCursorPos(0);

        CSyncDataReader serialiser(packedCloneSyncAckMsg.m_BitBuffer);

        int bitsPerSyncSequence = packedCloneSyncACKsMsg::SIZEOF_SYNC_ACK_SEQUENCE + 1; // sequence + has failed ACKs flag
        int bitsRemaining       = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

        const unsigned MAX_FAILED_SYNCS    = 128;
        ObjectId       failedSyncObjectIDs[MAX_FAILED_SYNCS];
        int            numFailedSyncs      = 0;
        bool           outOfOrder          = false;
        netSequence    syncAckSequence     = INVALID_SEQUENCE_ID;
        netSequence    lastSyncAckSequence = INVALID_SEQUENCE_ID;

        while(bitsRemaining >= bitsPerSyncSequence)
        {
            int oldBitsRemaining = bitsRemaining;

            SERIALISE_UNSIGNED(serialiser, syncAckSequence, packedCloneSyncACKsMsg::SIZEOF_SYNC_ACK_SEQUENCE);

            ActivePlayerIndex playerIndex = messageData.m_FromPlayer->GetActivePlayerIndex();

            if(playerIndex != INVALID_PLAYER_INDEX)
            {
                m_CloneSyncPacketLossTrackers[playerIndex].OnAck(syncAckSequence);
            }

            if(syncAckSequence != lastSyncAckSequence)
            {
                if(lastSyncAckSequence != INVALID_SEQUENCE_ID)
                {
                    // all other sync messages sent with this sequence have succeeded, so mark them as such
                    m_ObjectManager->ProcessSyncAckInSyncMessageInfoQueues(*messageData.m_FromPlayer, *messageData.m_ToPlayer, lastSyncAckSequence, failedSyncObjectIDs, numFailedSyncs, outOfOrder);
                }

                numFailedSyncs      = 0;
                outOfOrder          = false;
                lastSyncAckSequence = syncAckSequence;
            }

            if(m_Log)
            {
                m_Log->WriteDataValue("Sync ack seq", "(%d)", syncAckSequence);
            }

            bool hasAcks = false;
            SERIALISE_BOOL(serialiser, hasAcks);

            if(hasAcks)
            {
                SERIALISE_BOOL(serialiser, outOfOrder);

                if(outOfOrder)
                {
                    if(m_Log)
                    {
                        m_Log->WriteDataValue("Failed", "Out-of order");
			            m_Log->LineBreak();
                    }
                }
                else
                {
                    unsigned numSyncAcks = 0;
                    SERIALISE_UNSIGNED(serialiser, numSyncAcks, packedCloneSyncACKsMsg::SIZEOF_NUM_SYNC_ACKS);

                    gnetAssertf(numSyncAcks != 0, "Packed sync ack message is flagged as having ACKs but has none!");

                    if(m_Log)
                    {
                        m_Log->WriteDataValue("Num failed acks", "%d", numSyncAcks);
			            m_Log->LineBreak();
                    }

                    for(unsigned index = 0; index < numSyncAcks; index++)
                    {
                        ObjectId         objectID = NETWORK_INVALID_OBJECT_ID;
                        NetObjectAckCode ackCode  = ACKCODE_NONE;

                        SERIALISE_OBJECTID(serialiser, objectID);
                        SERIALISE_UNSIGNED(serialiser, reinterpret_cast<u32&>(ackCode), SIZEOF_ACKCODE);

                        m_ObjectManager->ProcessCloneSyncAckData(*messageData.m_FromPlayer, *messageData.m_ToPlayer, syncAckSequence, objectID, ackCode);

                        // we shouldn't be getting success ACK codes here, they aren't sent in packed reliable messages
                        Assert(ackCode != ACKCODE_SUCCESS);

                        if(ackCode != ACKCODE_SUCCESS)
                        {
                            Assert(numFailedSyncs < MAX_FAILED_SYNCS);

                            if(numFailedSyncs < MAX_FAILED_SYNCS)
                            {
                                failedSyncObjectIDs[numFailedSyncs] = objectID;
                                numFailedSyncs++;
                            }
                        }
                    }
                }
            }

            bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

            // ensure we have actually read some more data than last time around this loop, otherwise we will get stuck
            if(bitsRemaining == oldBitsRemaining)
            {
                break;
            }
        }

        // all other sync messages sent with this sequence have succeeded, so mark them as such
        m_ObjectManager->ProcessSyncAckInSyncMessageInfoQueues(*messageData.m_FromPlayer, *messageData.m_ToPlayer, syncAckSequence, failedSyncObjectIDs, numFailedSyncs, outOfOrder);

		BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_syncAckDataId, packedCloneSyncAckMsg.GetMessageTotalBitSize()));

       return true;
    }

    return false;
}

bool netObjectMgrMessageHandler::HandlePackedReliablesMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        packedReliablesMsg reliablesMsg;

        if(!gnetVerifyf(reliablesMsg.Import(messageData.m_MessageData, messageData.m_MessageDataSize), "Failed importing packed reliables message data!"))
        {
            return false;
        }

        const netSequence msgSeq = messageData.m_NetSequence;

        if(m_Log)
        {
            NetworkLogUtils::WriteMessageHeader(*m_Log, true, msgSeq, *messageData.m_FromPlayer, "PACKED_RELIABLES", "");

            m_Log->LineBreak();
            m_Log->WriteDataValue("Num creates",     "%d", reliablesMsg.m_Creates.m_MessageCount);
            m_Log->WriteDataValue("Num create acks", "%d", reliablesMsg.m_CreateAcks.m_MessageCount);
            m_Log->WriteDataValue("Num removes",     "%d", reliablesMsg.m_Removes.m_MessageCount);
            m_Log->WriteDataValue("Num remove acks", "%d", reliablesMsg.m_RemoveAcks.m_MessageCount);
            m_Log->LineBreak();
        }

        ProcessPackedCloneCreates   (reliablesMsg.m_Creates.m_MessageBuffer,    *messageData.m_FromPlayer, *messageData.m_ToPlayer, reliablesMsg.m_Timestamp);
        ProcessPackedCloneCreateAcks(reliablesMsg.m_CreateAcks.m_MessageBuffer, *messageData.m_FromPlayer, *messageData.m_ToPlayer);
        ProcessPackedCloneRemoves   (reliablesMsg.m_Removes.m_MessageBuffer,    *messageData.m_FromPlayer, *messageData.m_ToPlayer);
        ProcessPackedCloneRemoveAcks(reliablesMsg.m_RemoveAcks.m_MessageBuffer, *messageData.m_FromPlayer, *messageData.m_ToPlayer);

        return true;
    }

    return false;
}

void netObjectMgrMessageHandler::SendConnectionSyncMessage(const netPlayer& player)
{
    ActivePlayerIndex playerIndex = player.GetActivePlayerIndex();

    cloneSyncMsg* pMsg = &m_ConnectionSyncMsgs[playerIndex];
    Assert(pMsg);

    if(pMsg)
    {
        pMsg->UpdateNumObjectsCount();

        if(pMsg->m_BitBuffer.GetBitLength() > 0)
        {
            unsigned sendFlags = 0;

            if((m_SendConnectionSyncImmediately & (1 << playerIndex)) != 0)
            {
                sendFlags |= NET_SEND_IMMEDIATE;

                m_SendConnectionSyncImmediately &= ~(1 << playerIndex);
            }

            // set the sequence and timestamp for the message
            pMsg->m_Sequence = m_Sequence[playerIndex];

            netSequence sequence = 0;
            netInterface::SendMsg(&player, *pMsg, sendFlags, &sequence, pMsg->m_OwnerPlayer);

#if __BANK
            if(PARAM_additionalBandwidthLogging.Get())
            {
                gnetDebug2("Sending cloneSyncMsg with sequence %d", sequence);
            }
#endif // __BANK

            BANK_ONLY(m_NumCloneSyncMessagesSent++);

            m_CloneSyncPacketLossTrackers[playerIndex].OnSend(pMsg->m_Sequence);
            m_ObjectManager->OnSendConnectionSync(pMsg->m_CurrentTimestamp);

            ++m_Sequence[playerIndex];

            // check for sequence wrap to an invalid sequence ID
            if(m_Sequence[playerIndex] == INVALID_SEQUENCE_ID)
            {
                m_Sequence[playerIndex] = 1;
            }

            // begin a new message
            pMsg->Reset();
        }
    }
}

void netObjectMgrMessageHandler::SendCloneSync(const netPlayer &player,
                                               const netObject *pObject,
                                               datBitBuffer    &msgBuffer,
                                               netSequence     &seqNum,
                                               bool             sendImmediately)
{
    // pack the sync message into the connection sync message
    cloneSyncMsg* pMsg = &m_ConnectionSyncMsgs[player.GetActivePlayerIndex()];
    Assert(pMsg);

    if(pMsg->m_OwnerPlayer == 0)
    {
        pMsg->m_OwnerPlayer = pObject->GetPlayerOwner();
    }

    gnetAssertf(pMsg->m_NumObjects <= cloneSyncMsg::MAX_NUM_OBJECTS, "Too many objects have been packed in this sync message!");

    datBitBuffer* bb = &pMsg->m_BitBuffer;

    int thisSyncSize = cloneSyncMsg::GetSyncHeaderBitSize() + msgBuffer.GetCursorPos();

    bool newFrame  = false;
    u32  timestamp = netInterface::GetTimestampForPositionUpdates();

    if(pMsg->m_CurrentTimestamp == 0 || (pMsg->m_CurrentTimestamp != timestamp) || (pMsg->m_NumObjects == cloneSyncMsg::MAX_NUM_OBJECTS))
    {
        newFrame = true;
    }

    if(newFrame)
    {
        thisSyncSize = thisSyncSize + cloneSyncMsg::SIZEOF_TIMESTAMP + cloneSyncMsg::SIZEOF_NUM_OBJECTS;

        pMsg->m_CurrentTimestamp = timestamp;
    }

    // Will the sync fit in the message?
    if( !bb->CanWriteBits(thisSyncSize + netMessage::MAX_BIT_OFFSETOF_PAYLOAD + pMsg->GetMessageHeaderBitSize()) ||
        (pMsg->m_OwnerPlayer != pObject->GetPlayerOwner()))
    {
        gnetAssertf(bb->GetBitLength() > 0, "%s's data didn't fit into an empty message buffer. Sync size: %d. Same owner: %s", pObject->GetLogName(), thisSyncSize, (pMsg->m_OwnerPlayer == pObject->GetPlayerOwner())?"TRUE":"FALSE");

        // The sync didn't fit in the message.
        // Send the current message, then begin a new message.
        SendConnectionSyncMessage(player);

        pMsg->m_OwnerPlayer         = pObject->GetPlayerOwner();
        pMsg->m_CurrentTimestamp    = timestamp;
        newFrame                    = true;
    }

    Assert( bb->CanWriteBits(thisSyncSize) );

    if(sendImmediately)
    {
        m_SendConnectionSyncImmediately |= (1<< player.GetActivePlayerIndex());
    }

    if(newFrame)
    {
        BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_syncHeaderDataId, cloneSyncMsg::SIZEOF_TIMESTAMP + cloneSyncMsg::SIZEOF_NUM_OBJECTS));

        bb->WriteUns(timestamp, cloneSyncMsg::SIZEOF_TIMESTAMP);

        pMsg->UpdateNumObjectsCount();
        bb->WriteUns(0, cloneSyncMsg::SIZEOF_NUM_OBJECTS);
    }

    // count sync header data
    BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_syncHeaderDataId, cloneSyncMsg::GetSyncHeaderBitSize()));

    bb->WriteUns(pObject->GetObjectType(), SIZEOF_OBJECTTYPE);
    bb->WriteUns(pObject->GetObjectID(), SIZEOF_OBJECTID);
    bb->WriteBits( msgBuffer.GetReadWriteBits(), msgBuffer.GetCursorPos(), 0 );
    pMsg->m_NumObjects++;

    SET_MSG_TAG(*bb);

    // get sequence number that this packed message will have when sent
    seqNum = m_Sequence[player.GetActivePlayerIndex()];

    if(m_Log)
    {
        // log the send clone sync information
        NetworkLogUtils::WriteMessageHeader(*m_Log, false, seqNum, player, "SENDING_CLONE_SYNC", "%s", pObject->GetLogName());
        m_Log->WriteDataValue("Timestamp", "%d", timestamp);
        m_Log->WriteDataValue("Update Level", "%s", GetUpdateLevelString(pObject->GetUpdateLevel(player.GetPhysicalPlayerIndex())));

        if(sendImmediately)
        {
            m_Log->WriteDataValue("Send Immediately", "TRUE");
        }
    }

    m_ObjectManager->OnSendCloneSync(pObject, timestamp);
}

void netObjectMgrMessageHandler::AddCreate(const netPlayer       &toPlayer,
                                           const netPlayer       &fromPlayer,
                                           NetworkObjectType      objectType,
                                           const ObjectId         objectID,
                                           NetObjFlags            flags,
                                           u8*                    createData,
                                           unsigned               createDataSize)
{
    packedReliablesMsg &message = m_PackedReliables[toPlayer.GetActivePlayerIndex()];

    // DAN TEMP - Only allow clone creates messages from the same game frame in the same message, as the timestamp
    //            is relative to positional update data. Eventually we will have to support creates from multiple frames
    bool frameChanged = false;

    if(message.m_Timestamp == 0)
    {
        message.m_Timestamp = netInterface::GetTimestampForPositionUpdates();
    }
    else
    {
        frameChanged = (message.m_Timestamp != netInterface::GetTimestampForPositionUpdates());
    }

    u32 timestamp = message.m_Timestamp;

    bool success = false;

    if(!frameChanged)
    {
        success = message.AddCreate(fromPlayer, objectType, objectID, flags, createData, createDataSize);

        // count the create header data
        BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_packedReliablesId, packedReliablesMsg::GetCreateSize(0)));

        // count the create data
        BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_creationDataId, createDataSize));
    }

    if(!success)
    {
        SendPackedReliableMessage(toPlayer);

        message.m_Timestamp = timestamp;
        success = message.AddCreate(fromPlayer, objectType, objectID, flags, createData, createDataSize);
        gnetAssertf(success, "Failed to send packed reliable message to a player!");
    }
}

void netObjectMgrMessageHandler::AddCreateAck(const netPlayer       &toPlayer,
                                              const netPlayer       &fromPlayer,
                                              ObjectId               objectID,
                                              NetObjectAckCode       ackCode)
{
    bool success = m_PackedReliables[toPlayer.GetActivePlayerIndex()].AddCreateAck(fromPlayer, objectID, ackCode);

    // count the create ack data
    BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_packedReliablesId, packedReliablesMsg::GetCreateAckSize()));

    if(!success)
    {
        SendPackedReliableMessage(toPlayer);
        success = m_PackedReliables[toPlayer.GetActivePlayerIndex()].AddCreateAck(fromPlayer, objectID, ackCode);
        gnetAssertf(success, "Failed to send packed reliable message to a player!");
    }
}

void netObjectMgrMessageHandler::AddSyncAcks(const netPlayer       &toPlayer,
                                             const netPlayer       &fromPlayer,
                                             netSequence            syncSequence,
                                             unsigned               numObjects,
                                             unsigned               numFailedACKs,
                                             ObjectId               objectIDs[cloneSyncMsg::MAX_NUM_OBJECTS],
                                             NetObjectAckCode       ackCodes[cloneSyncMsg::MAX_NUM_OBJECTS])
{
    bool success = m_PackedSyncAcks[toPlayer.GetActivePlayerIndex()].AddSyncAcks(fromPlayer, syncSequence, numObjects, numFailedACKs, objectIDs, ackCodes);

    if(!success)
    {
        SendPackedSyncAckMessage(toPlayer);
        success = m_PackedSyncAcks[toPlayer.GetActivePlayerIndex()].AddSyncAcks(fromPlayer, syncSequence, numObjects, numFailedACKs, objectIDs, ackCodes);
        gnetAssertf(success, "Failed to send packed sync ack message to a player!");
    }
}

void netObjectMgrMessageHandler::AddRemove(const netPlayer       &toPlayer,
                                           const netPlayer       &fromPlayer,
                                           ObjectId               objectID,
                                           u32                    ownershipToken)
{
    bool success = m_PackedReliables[toPlayer.GetActivePlayerIndex()].AddRemove(fromPlayer, objectID, ownershipToken);

    // count the remove data
    BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_packedReliablesId, packedReliablesMsg::GetRemoveSize()));

    if(!success)
    {
        SendPackedReliableMessage(toPlayer);
        success = m_PackedReliables[toPlayer.GetActivePlayerIndex()].AddRemove(fromPlayer, objectID, ownershipToken);
        gnetAssertf(success, "Failed to send packed reliable message to a player!");
    }
}

void netObjectMgrMessageHandler::AddRemoveAck(const netPlayer       &toPlayer,
                                              const netPlayer       &fromPlayer,
                                              ObjectId               objectID,
                                              NetObjectAckCode       ackCode)
{
    bool success = m_PackedReliables[toPlayer.GetActivePlayerIndex()].AddRemoveAck(fromPlayer, objectID, ackCode);

    // count the remove ack data
    BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_packedReliablesId, packedReliablesMsg::GetRemoveAckSize()));

    if(!success)
    {
        SendPackedReliableMessage(toPlayer);
        success = m_PackedReliables[toPlayer.GetActivePlayerIndex()].AddRemoveAck(fromPlayer, objectID, ackCode);
        gnetAssertf(success, "Failed to send packed reliable message to a player!");
    }
}

void netObjectMgrMessageHandler::SendPackedSyncAckMessage(const netPlayer& player)
{
    if (!player.IsMyPlayer())
    {
        ActivePlayerIndex playerIndex = player.GetActivePlayerIndex();

        if(m_PackedSyncAcks[playerIndex].HasDataToSend())
        {
            m_PackedSyncAcks[playerIndex].WritePendingSyncAcksToBitBuffer(INVALID_SEQUENCE_ID);

            netSequence sequence = 0;

            netInterface::SendMsg(&player,
                                  m_PackedSyncAcks[playerIndex],
                                  0,
                                  &sequence,
                                  m_PackedSyncAcks[playerIndex].m_OwnerPlayer);

            BANK_ONLY(m_NumCloneSyncAckMessagesSent++);

			// count the sync ack data
			BANK_ONLY(m_bandwidthStats.AddBandwidthOut(m_syncAckDataId, m_PackedSyncAcks[playerIndex].GetMessageTotalBitSize()));

#if ENABLE_NETWORK_LOGGING
            if(m_Log)
            {
                NetworkLogUtils::WriteMessageHeader(*m_Log, false, sequence, player, "PACKED_SYNC_ACK", "");

                for(unsigned index = 0; index < m_PackedSyncAcks[playerIndex].m_NumSequencesAcked; index++)
                {
                    m_Log->WriteDataValue("Sync ack seq", "(%d)", m_PackedSyncAcks[playerIndex].m_SequenceAcked[index]);

				    if (m_PackedSyncAcks[playerIndex].m_NumSyncAcks > 0)
				    {
					    m_Log->WriteDataValue("Num failed acks", "%d",  m_PackedSyncAcks[playerIndex].m_NumFailedAcks[index]);
				    }
                }
            }
#endif // ENABLE_NETWORK_LOGGING

            m_PackedSyncAcks[playerIndex].Reset();
        }
    }
}

void netObjectMgrMessageHandler::SendPackedReliableMessage(const netPlayer &player)
{
    if (!player.IsMyPlayer())
    {
        ActivePlayerIndex playerIndex = player.GetActivePlayerIndex();

        if(m_PackedReliables[playerIndex].HasDataToSend())
        {
            netSequence sequence = 0;
            netInterface::SendMsg(&player,
                                  m_PackedReliables[playerIndex],
                                  NET_SEND_RELIABLE,
                                  &sequence,
                                  m_PackedReliables[playerIndex].m_OwnerPlayer);

            BANK_ONLY(m_NumPackedReliableMessagesSent++);

            if(m_Log)
            {
                NetworkLogUtils::WriteMessageHeader(*m_Log, false, sequence, player, "PACKED_RELIABLES", "");

                m_Log->WriteDataValue("Num creates",     "%d", m_PackedReliables[playerIndex].m_Creates.m_MessageCount);
                m_Log->WriteDataValue("Num create acks", "%d", m_PackedReliables[playerIndex].m_CreateAcks.m_MessageCount);
                m_Log->WriteDataValue("Num removes",     "%d", m_PackedReliables[playerIndex].m_Removes.m_MessageCount);
                m_Log->WriteDataValue("Num remove acks", "%d", m_PackedReliables[playerIndex].m_RemoveAcks.m_MessageCount);
            }

            m_PackedReliables[playerIndex].Reset();
        }
    }
}

void netObjectMgrMessageHandler::ProcessPackedCloneCreates(datBitBuffer    &messageBuffer,
                                                           const netPlayer &fromPlayer,
                                                           const netPlayer &toPlayer,
                                                           unsigned         timestamp)
{
    static const int bitsPerCreate = SIZEOF_OBJECTTYPE +
                                     SIZEOF_OBJECTID +
                                     SIZEOF_NETOBJ_GLOBALFLAGS +
                                     packedMessageType::SIZEOF_BUFFER_SIZE;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    CSyncDataReader serialiser(messageBuffer);

    while(bitsRemaining >= bitsPerCreate)
    {
        NetworkObjectType objectType           = INVALID_OBJECT_TYPE;
        ObjectId          objectID             = NETWORK_INVALID_OBJECT_ID;
        NetObjFlags       flags                = 0;
        unsigned          createDataSizeInBits = 0;

        SERIALISE_UNSIGNED(serialiser, objectType, SIZEOF_OBJECTTYPE);
        SERIALISE_OBJECTID(serialiser, objectID);

        if(SIZEOF_NETOBJ_GLOBALFLAGS > 0)
        {
            SERIALISE_UNSIGNED(serialiser, flags, SIZEOF_NETOBJ_GLOBALFLAGS);
        }

        SERIALISE_UNSIGNED(serialiser, createDataSizeInBits, packedMessageType::SIZEOF_BUFFER_SIZE);

        u8 createData[((1<<packedMessageType::SIZEOF_BUFFER_SIZE)>>3)+1];
        if(createDataSizeInBits > 0)
        {
            SERIALISE_DATABLOCK(serialiser, createData, createDataSizeInBits);
        }

        datBitBuffer createDataMessageBuffer;
        createDataMessageBuffer.SetReadOnlyBits(createData, (createDataSizeInBits+1), 0);

        // count the create header data
        BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_packedReliablesId, packedReliablesMsg::GetCreateSize(0)));

        // count the create data
        BANK_ONLY(netBandwidthStatsReadCollector collector(m_bandwidthStats, m_creationDataId, createDataMessageBuffer));

        m_ObjectManager->ProcessCloneCreateData(fromPlayer, toPlayer, objectType, objectID, flags, createDataMessageBuffer, timestamp);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

void netObjectMgrMessageHandler::ProcessPackedCloneCreateAcks(datBitBuffer    &messageBuffer,
                                                              const netPlayer &fromPlayer,
                                                              const netPlayer &toPlayer)
{
    static const int bitsPerCreateAck = SIZEOF_OBJECTID + SIZEOF_ACKCODE;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    CSyncDataReader serialiser(messageBuffer);

    while(bitsRemaining >= bitsPerCreateAck)
    {
        ObjectId         objectID = NETWORK_INVALID_OBJECT_ID;
        NetObjectAckCode ackCode  = ACKCODE_NONE;

        SERIALISE_OBJECTID(serialiser, objectID);
        SERIALISE_UNSIGNED(serialiser, reinterpret_cast<u32&>(ackCode), SIZEOF_ACKCODE);

        // count the create ack header data
        BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_packedReliablesId, packedReliablesMsg::GetCreateAckSize()));

        m_ObjectManager->ProcessCloneCreateAckData(fromPlayer, toPlayer, objectID, ackCode);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

void netObjectMgrMessageHandler::ProcessPackedCloneRemoves(datBitBuffer    &messageBuffer,
                                                           const netPlayer &fromPlayer,
                                                           const netPlayer &toPlayer)
{
    static const int bitsPerRemove = SIZEOF_OBJECTID + netObject::SIZEOF_OWNERSHIP_TOKEN;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    CSyncDataReader serialiser(messageBuffer);

    while(bitsRemaining >= bitsPerRemove)
    {
        ObjectId objectID        = NETWORK_INVALID_OBJECT_ID;
        u32      ownershipToken  = 0;

        SERIALISE_OBJECTID(serialiser, objectID);
        SERIALISE_UNSIGNED(serialiser, ownershipToken, netObject::SIZEOF_OWNERSHIP_TOKEN);

        // count the remove data
        BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_packedReliablesId, packedReliablesMsg::GetRemoveSize()));

        m_ObjectManager->ProcessCloneRemoveData(fromPlayer, toPlayer, objectID, ownershipToken);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

void netObjectMgrMessageHandler::ProcessPackedCloneRemoveAcks(datBitBuffer    &messageBuffer,
                                                              const netPlayer &fromPlayer,
                                                              const netPlayer &toPlayer)
{
    static const int bitsPerRemoveAck = SIZEOF_OBJECTID + SIZEOF_ACKCODE;

    int bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();

    CSyncDataReader serialiser(messageBuffer);

    while(bitsRemaining >= bitsPerRemoveAck)
    {
        ObjectId         objectID = NETWORK_INVALID_OBJECT_ID;
        NetObjectAckCode ackCode  = ACKCODE_NONE;

        SERIALISE_OBJECTID(serialiser, objectID);
        SERIALISE_UNSIGNED(serialiser, reinterpret_cast<u32&>(ackCode), SIZEOF_ACKCODE);

        // count the remove ack data
        BANK_ONLY(m_bandwidthStats.AddBandwidthIn(m_packedReliablesId, packedReliablesMsg::GetRemoveAckSize()));

        m_ObjectManager->ProcessCloneRemoveAckData(fromPlayer, toPlayer, objectID, ackCode);

        bitsRemaining = messageBuffer.GetBitLength() - messageBuffer.GetCursorPos();
    }
}

netSequence netObjectMgrMessageHandler::GetLastSequenceReceivedFromPlayer(const netPlayer& player) const
{
    if(gnetVerifyf(player.GetActivePlayerIndex() < MAX_NUM_ACTIVE_PLAYERS, "Player index is out of range!"))
    {
        return m_LastReceivedSequence[player.GetActivePlayerIndex()];
    }

    return INVALID_SEQUENCE_ID;
}

void netObjectMgrMessageHandler::SetLastSequenceReceivedFromPlayer(const netPlayer& player, const netSequence sequence)
{
    if(gnetVerifyf(player.GetActivePlayerIndex() < MAX_NUM_ACTIVE_PLAYERS, "Player index is out of range!"))
    {
        gnetAssertf(sequence != INVALID_SEQUENCE_ID, "Setting an invalid sequence ID!");
        m_LastReceivedSequence[player.GetActivePlayerIndex()] = sequence;
    }
}

void netObjectMgrMessageHandler::ResetCurrentFrameSyncTimestamps()
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_CurrentFrameSyncTimestamp[index] = 0;
    }
}

void netObjectMgrMessageHandler::CloneSyncPacketLossTracker::OnSend(const netSequence sequence)
{
    if(m_StartSeq == INVALID_SEQUENCE_ID)
    {
        m_StartSeq = sequence;
    }
    else if(m_TimeAllSequencesSent == 0)
    {
        const unsigned seqDiff = netSeqDiff(sequence, m_StartSeq);

        if(seqDiff > NUM_SEQUENCES_TRACKED)
        {
            m_TimeAllSequencesSent = sysTimer::GetSystemMsTime();
        }
    }
}

void netObjectMgrMessageHandler::CloneSyncPacketLossTracker::OnAck(const netSequence sequence)
{
    if(m_StartSeq != INVALID_SEQUENCE_ID)
    {
        const unsigned seqDiff = netSeqDiff(sequence, m_StartSeq);

        if(seqDiff < NUM_SEQUENCES_TRACKED)
        {
            m_AckFlags |= (1 << seqDiff);

            const u16 ALL_SEQUENCES_ACKED = 0xffff;

            if(m_AckFlags == ALL_SEQUENCES_ACKED)
            {
                Reset();

                // not strictly necessary as packet loss is reset in the Reset() function above, but make the code clearer
                m_LastPacketLossRatio = 0.0f;
            }
        }
    }
}

void netObjectMgrMessageHandler::CloneSyncPacketLossTracker::Update()
{
    if(m_TimeAllSequencesSent != 0)
    {
        u32 currentTime = sysTimer::GetSystemMsTime();
        u32 timeElapsed = (currentTime > m_TimeAllSequencesSent) ? (currentTime - m_TimeAllSequencesSent) : 0;

        if(timeElapsed > TIME_TO_WAIT_FOR_ACKS)
        {
            float packetLossRatio = 0.0f;

            unsigned numAcksReceived = netUtils::CountBitsSet(m_AckFlags);

            if(numAcksReceived != NUM_SEQUENCES_TRACKED)
            {
                packetLossRatio = 1.0f - (static_cast<float>(numAcksReceived) / NUM_SEQUENCES_TRACKED);
            }

            Reset();

            m_LastPacketLossRatio = packetLossRatio;
        }
    }
}

} // namespace rage
