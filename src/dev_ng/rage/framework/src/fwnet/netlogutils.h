//
// netlogutils.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_LOG_UTILS_H
#define NETWORK_LOG_UTILS_H

#include "net/netsequence.h"
#include "fwnet/netlog.h"		// ENABLE_NETWORK_LOGGING
#include "fwnet/netTypes.h"

namespace rage
{
    class netLoggingInterface;
    class netPlayer;

//PURPOSE
// Contains various functions for created common logging output that isn't related to a single system
namespace NetworkLogUtils
{
    //PURPOSE
    // Writes a message header to the specified log file - this is used by the different
    // network systems that send and receive messages
    //PARAMS
    // log          - The logging object to direct output to
    // received     - A flag indicating whether the message was sent or received
    // seqNum       - The sequence number of the message
    // player       - The player the message was sent/received
    // type         - The message type text
    // messageText  - The message text
    void WriteMessageHeader(netLoggingInterface &log,
                            bool                 received,
                            netSequence          seqNum,
                            const netPlayer     &player,
                            const char          *type,
                            const char          *messageText, ...);

    //PURPOSE
    // Writes some data related to a network player to the specified log file - this
    // is used by lots of network systems to log actions related to a specific player
    //PARAMS
    // log          - The logging object to direct output to
    // playerIndex  - The player index of the player this output is related to
    // type         - The player type text
    // messageText  - The player text
    void WritePlayerText(netLoggingInterface &log,
                         PhysicalPlayerIndex playerIndex,
                         const char          *type,
                         const char          *messageText, ...);

    //PURPOSE
    // Writes a logging event to the specified network log file along with any specified data
    //PARAMS
    // log       - The logging object to direct output to
    // eventName - The name of the event to log
    // extraData - Any extra data to log related to the event
    void WriteLogEvent(netLoggingInterface &log,
                       const char          *eventName,
                       const char          *extraData, ...);

#if !ENABLE_NETWORK_LOGGING
	inline void WriteMessageHeader(netLoggingInterface &, bool, netSequence, const netPlayer&, const char*, const char*, ...) {}
	inline void WritePlayerText(netLoggingInterface&, PhysicalPlayerIndex, const char*, const char*, ...) {}
	inline void WriteLogEvent(netLoggingInterface &, const char*, const char*, ...) {}
#endif // !ENABLE_NETWORK_LOGGING

} // namespace NetworkLogUtils

} // namespace rage

#endif // NETWORK_LOG_UTILS_H
