// 
// netSyncDataUL.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETWORK_SYNC_DATA_UL_H
#define NETWORK_SYNC_DATA_UL_H

// game includes
#include "fwnet/netsyncdata.h"

namespace rage
{

//PURPOSE
//	Contains a NetworkSyncData plus synchronisation update levels
class CNetworkSyncDataULBase
{
public:

	// update levels
	enum
	{
		UPDATE_LEVEL_VERY_LOW,
		UPDATE_LEVEL_LOW,
		UPDATE_LEVEL_MEDIUM,
		UPDATE_LEVEL_HIGH,
		UPDATE_LEVEL_VERY_HIGH,
		NUM_UPDATE_LEVELS
	};

    // update frequencies
	enum
	{
		UPDATE_RATE_FREQUENCY_VERY_HIGH	=	25,
		UPDATE_RATE_FREQUENCY_HIGH		=	100,
		UPDATE_RATE_FREQUENCY_MEDIUM	=	300,
		UPDATE_RATE_FREQUENCY_LOW		=	400,
		UPDATE_RATE_FREQUENCY_VERY_LOW	=	1000
	};

    //PURPOSE
    // Returns the update frequency associated with the specified update level
    //PARAMS
    // updateLevel - The update level to return the frequency for
    static unsigned GetUpdateFrequency(u8 updateLevel)
    {
        switch(updateLevel)
        {
        case UPDATE_LEVEL_VERY_LOW:
            return UPDATE_RATE_FREQUENCY_VERY_LOW;
        case UPDATE_LEVEL_LOW:
            return UPDATE_RATE_FREQUENCY_LOW;
        case UPDATE_LEVEL_MEDIUM:
            return UPDATE_RATE_FREQUENCY_MEDIUM;
        case UPDATE_LEVEL_HIGH:
            return UPDATE_RATE_FREQUENCY_HIGH;
        case UPDATE_LEVEL_VERY_HIGH:
            return UPDATE_RATE_FREQUENCY_VERY_HIGH;
        default:
            gnetAssertf(0, "Trying to get the update frequency of an unexpected update level!");
            return 0;
        }
    }

public:

	virtual ~CNetworkSyncDataULBase() {}

	virtual netSyncDataBase*			GetSyncData()=0;
	virtual void						SetSyncData(netSyncDataBase* pData)=0;
	virtual datBitBuffer*				GetCurrentStateBuffer()=0;
	virtual datBitBuffer*				GetShadowBuffer()=0;
	virtual netSyncDataUnitBase&		GetSyncDataUnit(unsigned i) const =0;
	virtual unsigned					GetBufferSize() const=0;
	virtual unsigned					GetNumSyncDataUnits() const=0;
	virtual void						InitialisePlayerSyncData(const unsigned playerIndex, bool setUnsynced)=0;
	virtual u8							GetUpdateLevel(unsigned playerIndex) const = 0;
	virtual u8							GetMinimumUpdateLevel() const = 0;
	virtual void						SetUpdateLevel(unsigned playerIndex, u8 level)=0;
};

template<unsigned maxNumSyncedPlayers>
class CNetworkSyncDataUL : public CNetworkSyncDataULBase
{
public:

	CNetworkSyncDataUL() :
    m_SyncData(NULL)
    , m_MinimumUpdateLevel(UPDATE_LEVEL_VERY_LOW)
	{
		for (unsigned p=0; p<maxNumSyncedPlayers; p++)
		{
			m_UpdateLevels[p] = UPDATE_LEVEL_VERY_LOW;
		}
	}

	virtual ~CNetworkSyncDataUL()
	{
		if (m_SyncData)
			delete m_SyncData;
	}

	netSyncDataBase*			GetSyncData()															{ return m_SyncData; }
    netSyncDataBase*			GetSyncData() const														{ return m_SyncData; }
	void						SetSyncData(netSyncDataBase* pData)										{ Assert(!m_SyncData); m_SyncData = pData; }
	void						DestroySyncData()														{ Assert(m_SyncData); delete m_SyncData; m_SyncData = NULL; }
	datBitBuffer*				GetCurrentStateBuffer()													{ Assert(m_SyncData); return m_SyncData->GetCurrentStateBuffer(); }
	datBitBuffer*				GetShadowBuffer()														{ Assert(m_SyncData); return m_SyncData->GetShadowBuffer(); }
	netSyncDataUnitBase&		GetSyncDataUnit(unsigned i) const										{ Assert(m_SyncData); return m_SyncData->GetSyncDataUnit(i); }
	unsigned					GetBufferSize() const													{ Assert(m_SyncData); return m_SyncData->GetBufferSize(); }
	unsigned					GetNumSyncDataUnits() const												{ Assert(m_SyncData); return m_SyncData->GetNumSyncDataUnits(); }
	void						InitialisePlayerSyncData(const unsigned playerIndex, bool setUnsynced)	{ Assert(m_SyncData); return m_SyncData->InitialisePlayerSyncData(playerIndex, setUnsynced); }

	//PURPOSE
	//	Gets the update level to the given player
	u8 GetUpdateLevel(unsigned playerIndex) const
	{
		if (AssertVerify(playerIndex<maxNumSyncedPlayers))
		{
			return m_UpdateLevels[playerIndex];
		}

		return UPDATE_LEVEL_VERY_LOW;
	}

	//PURPOSE
	//	Gets the current minimum update level to all the other players
	u8 GetMinimumUpdateLevel() const
	{
		return m_MinimumUpdateLevel;
	}

	//PURPOSE
	//	Sets the update level to the given player
	void SetUpdateLevel(unsigned playerIndex, u8 level)
	{
		if (AssertVerify(playerIndex<maxNumSyncedPlayers) && 
			AssertVerify(level < NUM_UPDATE_LEVELS))
		{
			if (m_UpdateLevels[playerIndex] != level)
			{
				m_UpdateLevels[playerIndex] = level;

                // update the minimum update level
                m_MinimumUpdateLevel = level;

                for (unsigned playerIndex=0; playerIndex<maxNumSyncedPlayers; playerIndex++)
		        {
			        if (m_UpdateLevels[playerIndex] > m_MinimumUpdateLevel)
			        {
				        m_MinimumUpdateLevel = m_UpdateLevels[playerIndex];
			        }
		        }
			}
		}
	}

protected:

    netSyncDataBase*		m_SyncData;
	u8						m_UpdateLevels[maxNumSyncedPlayers]; // update levels relative to each other player
    u8                      m_MinimumUpdateLevel;                // minimum update level for this object
};

} // namespace rage

#endif  // NETWORK_SYNC_DATA_UL_H
