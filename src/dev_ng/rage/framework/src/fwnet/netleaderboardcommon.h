// 
// netleaderboardcommon.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

#ifndef NET_LEADERBOARD_COMMON_H
#define NET_LEADERBOARD_COMMON_H

namespace rage
{

//Maximun Number of Gamers/Rows that we read leaderboard values for. 
#define MAX_LEADERBOARD_ROWS  (101)

//Maximum number of Groups that we can read in leaderboards for 
#define MAX_LEADERBOARD_READ_GROUPS  (10)
#define MAX_LEADERBOARD_CLAN_IDS  (10)

} // namespace rage

#endif  // NET_LEADERBOARD_COMMON_H