// 
// netutils.cpp
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
//
#include "fwnet/netutils.h"

#include "fwnet/netchannel.h"
#include "fwnet/netobject.h"

// rage headers
#include "profile/timebars.h"

namespace rage
{

// ================================================================================================================
// netObjectTypePredicate
// ================================================================================================================
NetworkObjectType netObjectTypePredicate::m_ObjectType = 0;

void netObjectTypePredicate::SetObjectType(NetworkObjectType objectType)
{
    m_ObjectType = objectType;
}

bool netObjectTypePredicate::ObjectInclusionTest(const netObject *networkObject)
{
    if(networkObject && networkObject->GetObjectType() == m_ObjectType)
    {
        return true;
    }

    return false;
}

// ================================================================================================================
// netTimeBarWrapper
// ================================================================================================================

netTimeBarWrapper::netTimeBarWrapper(bool RAGE_TIMEBARS_ONLY(updateTimeBars))
#if RAGE_TIMEBARS
: m_UpdateTimeBars(updateTimeBars)
#if __ASSERT
, m_PushDepth(0)
#endif // __ASSERT
#endif // RAGE_TIMEBARS
{
}

netTimeBarWrapper::~netTimeBarWrapper()
{
#if RAGE_TIMEBARS
    ASSERT_ONLY(gnetAssertf(m_PushDepth == 0, "PushTimer called more times than PopTimer!");)
#endif //RAGE_TIMEBARS
}

#if RAGE_TIMEBARS
void netTimeBarWrapper::StartTimer(const char *name, bool detail)
{
    if(m_UpdateTimeBars)
    {
		if (detail)
		{
	        PF_START_TIMEBAR_DETAIL(name);
		}
		else
		{
			PF_START_TIMEBAR(name);
		}
    }
}

void netTimeBarWrapper::PushTimer(const char *name, bool detail)
{
    ASSERT_ONLY(m_PushDepth++);

    if(m_UpdateTimeBars)
    {
		if (detail)
		{
	        PF_PUSH_TIMEBAR_DETAIL(name);
		}
		else
		{
			PF_PUSH_TIMEBAR(name);
		}
    }
}

void netTimeBarWrapper::PushBudgetTimer(const char *name, float budget)
{
    ASSERT_ONLY(m_PushDepth++);

	if(m_UpdateTimeBars)
	{
		PF_PUSH_TIMEBAR_BUDGETED(name,budget);
	}
}

void netTimeBarWrapper::PushIdleTimer(const char *name)
{
    ASSERT_ONLY(m_PushDepth++);

	if(m_UpdateTimeBars)
	{
		PF_PUSH_TIMEBAR_IDLE(name);
	}
}

void netTimeBarWrapper::PopTimer(bool detail)
{
    ASSERT_ONLY(gnetAssertf(m_PushDepth > 0, "Calling PopTimer too many times!");)
    ASSERT_ONLY(m_PushDepth--);

    if(m_UpdateTimeBars)
    {
		if (detail)
		{
	        PF_POP_TIMEBAR_DETAIL();
		}
		else
		{
			PF_POP_TIMEBAR();
		}
    }
}

#else
void netTimeBarWrapper::StartTimer(const char *, bool){}
void netTimeBarWrapper::PushTimer(const char *, bool){}
void netTimeBarWrapper::PushBudgetTimer(const char *,float){}
void netTimeBarWrapper::PushIdleTimer(const char *){}
void netTimeBarWrapper::PopTimer(bool){}
#endif

#if ENABLE_NETWORK_LOGGING

const char *GetUpdateLevelString(unsigned updateLevel)
{
    switch (updateLevel)
	{
	case CNetworkSyncDataULBase::UPDATE_LEVEL_VERY_HIGH:
		return "VERY HIGH";
	case CNetworkSyncDataULBase::UPDATE_LEVEL_HIGH:
		return "HIGH";
	case CNetworkSyncDataULBase::UPDATE_LEVEL_MEDIUM:
		return "MEDIUM";
	case CNetworkSyncDataULBase::UPDATE_LEVEL_LOW:
		return "LOW";
	case  CNetworkSyncDataULBase::UPDATE_LEVEL_VERY_LOW:
		return "VERY LOW";
	default:
		gnetAssertf(0, "Trying to get the string for an unexpected update level!");
        return "UNKNOWN";
	}
}

const char *GetCanCloneErrorString(unsigned resultCode)
{
    switch(resultCode)
    {
    case CC_SUCCESS:
        return "Succeeded";
    case CC_NO_CLONE_FLAG:
        return "No clone flag set";
    case CC_UNREGISTERING:
        return "Object is unregistering";
    case CC_PENDING_REMOVAL:
        return "Object is pending removal on another remote player";
    case CC_PENDING_OWNER_CHANGE:
        return "Obejct is pending owner change to another remote player";
    default:
        gnetAssertf(0, "Unexpected CanClone() error code!");
        return "Unknown";
    }
}

const char *GetCanPassControlErrorString(unsigned resultCode)
{
    switch(resultCode)
    {
    case CPC_SUCCESS:
        return "Succeeded";
    case CPC_FAIL_NOT_CLONED:
        return "Object not cloned on target machine";
    case CPC_FAIL_UNREGISTERING:
        return "Object is unregistering";
    case CPC_FAIL_BEING_REASSIGNED:
        return "Object is being reassigned";
    case CPC_FAIL_IS_CLONE:
        return "Object not controlled locally";
    case CPC_FAIL_IS_MIGRATING:
        return "Object Migrating";
    case CPC_FAIL_PERSISTANT_OWNER:
        return "Persistant Owner Flag Set";
	case CPC_FAIL_PENDING_CLONE_OR_REMOVE:
		return "Pending Clone/Remove";
	case CPC_FAIL_NO_SYNC_DATA:
		return "Object has no sync data";
    default:
        gnetAssertf(0, "Unexpected CanPassControl() error code!");
        return "Unknown";
    }
}

const char *GetCanAcceptControlErrorString(unsigned resultCode)
{
    switch(resultCode)
    {
    case CAC_SUCCESS:
        return "Succeeded";
    case CAC_FAIL_WRONG_OWNER:
        return "Wrong owner";
    case CAC_FAIL_BEING_REASSIGNED:
        return "Object is being reassigned";
    case CAC_FAIL_IS_MIGRATING:
        return "Object Migrating";
    case CAC_FAIL_NO_GAME_OBJECT:
        return "No game object";
    default:
        gnetAssertf(0, "Unexpected CanAcceptControl() error code!");
        return "Unknown";
    }
}

#endif // ENABLE_NETWORK_LOGGING
}
