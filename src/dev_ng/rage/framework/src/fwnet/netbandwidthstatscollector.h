// 
// netBandwidthStatsCollector.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETWORK_BANDWIDTH_STATS_COLLECTOR_H
#define NETWORK_BANDWIDTH_STATS_COLLECTOR_H

#include "fwnet/netbandwidthmgr.h"
#include "fwnet/netbandwidthstats.h"

#if __BANK

namespace rage
{

class datBitBuffer;

//PURPOSE
// This is a simple helper class for collecting statistics information using 
// the supplied bandwidth recorder.
//
// Objects of this class gather the current read and write positions
// of the specified message buffer in the constructor, and then compare
// the values against the rage_new read and write positions in the destructor.
// 
// If the positions have changes a bandwidth in/out and sync in/out event
// is passed to the contained stats object.
// 
// This allows a client to gather stats automatically by simply constructing
// a netBandwidthStatsCollector object on the stack of a sync function.
class netBandwidthStatsCollector
{
public:

	netBandwidthStatsCollector(netBandwidthStatistics& stats, RecorderId recorderId, const rage::datBitBuffer &bitBuffer) 
	: m_Stats(stats)
	, m_RecorderId(recorderId)
	, m_BitBuffer(bitBuffer)
	, m_OldCursorPos(bitBuffer.GetCursorPos())
	{

	}

	virtual ~netBandwidthStatsCollector()
	{

	}

private:

	netBandwidthStatsCollector();
	netBandwidthStatsCollector(const netBandwidthStatsCollector &);

protected:

	netBandwidthStatistics& m_Stats;
	RecorderId				  m_RecorderId;
	const rage::datBitBuffer& m_BitBuffer;
	int						  m_OldCursorPos;
};

//PURPOSE
// Stats collector used for reading
class netBandwidthStatsReadCollector : public netBandwidthStatsCollector
{
public:

	netBandwidthStatsReadCollector(netBandwidthStatistics& stats, RecorderId recorderId, const rage::datBitBuffer &bitBuffer) :
	netBandwidthStatsCollector(stats, recorderId, bitBuffer)
	{

	}

	~netBandwidthStatsReadCollector()
	{
		int bandwidthIn = m_BitBuffer.GetCursorPos() > m_OldCursorPos;

		if (bandwidthIn > 0)
		{
			m_Stats.AddBandwidthIn(m_RecorderId, bandwidthIn);
		}
	}
};

//PURPOSE
// Stats collector used for writing
class netBandwidthStatsWriteCollector : public netBandwidthStatsCollector
{
public:

	netBandwidthStatsWriteCollector(netBandwidthStatistics& stats, RecorderId recorderId, const rage::datBitBuffer &bitBuffer) :
	netBandwidthStatsCollector(stats, recorderId, bitBuffer)
	{

	}
	~netBandwidthStatsWriteCollector()
	{
		int bandwidthOut = m_BitBuffer.GetCursorPos() > m_OldCursorPos;

		if (bandwidthOut > 0)
		{
			m_Stats.AddBandwidthOut(m_RecorderId, bandwidthOut);
		}
	}
};

} // namespace rage

#endif  // __BANK

#endif  // NETWORK_BANDWIDTH_STATS_COLLECTOR_H
