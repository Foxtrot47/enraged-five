//
// filename:	netprofanityfilter.h
// description:	
//

#ifndef INC_NETPROFANITYFILTER_H_
#define INC_NETPROFANITYFILTER_H_

#include "rline\socialclub\rlsocialclubcommon.h"

namespace netProfanityFilter
{
	typedef int CheckTextToken;
	static const CheckTextToken INVALID_TOKEN = 0;

	enum ReturnCode
	{
		 RESULT_STRING_OK = 0
		,RESULT_STRING_FAILED
		,RESULT_PENDING
		,RESULT_INVALID_TOKEN
		,RESULT_ERROR = -1
	};
	
	static const rage::u32 MAX_STRING_SIZE = 1024;
	
	bool         VerifyStringForProfanity(const char* inString, const rage::rlScLanguage language, bool ugcCheck, CheckTextToken& outToken);
	const char*  GetProfaneWordForRequest(const CheckTextToken& token);
	ReturnCode        GetStatusForRequest(const CheckTextToken& token);

	bool		ClearRequest(const CheckTextToken& token);
}

#endif // !INC_NETPROFANITYFILTER_H_
