//
// netobjectreassignmgr.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/netobjectreassignmgr.h"

// rage includes

#include "bank/bkmgr.h"
#include "net/message.h"
#include "profile/profiler.h"
#include "string/stringhash.h"

// framework includes
#include "grcore/debugdraw.h"
#include "fwnet/netbandwidthmgr.h"
#include "fwnet/netchannel.h"
#include "fwnet/netinterface.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobject.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/optimisations.h"
#include "fwsys/timer.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

bool netObjectReassignMgr::sm_IsReassignmentInProgress      = false;

#if __BANK
bool netObjectReassignMgr::sm_RejectAllNegotiationMessages  = false;
bool netObjectReassignMgr::sm_RejectAllConfirmationMessages = false;
bool netObjectReassignMgr::sm_RejectIgnoreLeavingPlayers    = false;
#endif // __BANK

FW_INSTANTIATE_CLASS_POOL(reassignObjectInfo, MAX_NUM_NETOBJECTS, atHashString("reassignObjectInfo",0xabca8075));

///////////////////////////////////////////////////////////////////////////////////////////////////////
// reassignObjectInfo
///////////////////////////////////////////////////////////////////////////////////////////////////////
reassignObjectInfo::reassignObjectInfo(netObject &object, unsigned initialPriority, PhysicalPlayerIndex initialOwner) :
m_Object(&object)
, m_LocalReassignPriority(static_cast<u8>(initialPriority))
, m_CurrentReassignPriority(static_cast<u8>(initialPriority))
, m_OwnerPlayer(initialOwner)
, m_PlayerFlags(0)
, m_UpdateFlags(0)
, m_IncludedInNegotiation(true)
, m_Confirmed(false)
, m_ToBeRemoved(false)
{
    Assert(!object.GetListNode());
    Assert(m_OwnerPlayer != INVALID_PLAYER_INDEX);
}

void reassignObjectInfo::DecideOwnership(const netPlayer& contenderPlayer, unsigned newPriority)
{
    PhysicalPlayerIndex contenderPlayerIndex = contenderPlayer.GetPhysicalPlayerIndex();

    if(gnetVerifyf(contenderPlayerIndex != INVALID_PLAYER_INDEX, "Trying to decide ownership on a player with an invalid player index!"))
    {
        if (newPriority > m_CurrentReassignPriority)
        {
            m_CurrentReassignPriority = static_cast<u8>(newPriority);
            m_OwnerPlayer             = contenderPlayerIndex;
        }
        else if (newPriority == m_CurrentReassignPriority)
        {
            if(m_OwnerPlayer < contenderPlayerIndex)
            {
                m_OwnerPlayer = contenderPlayerIndex;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// reassignmentProcessInfo
///////////////////////////////////////////////////////////////////////////////////////////////////////

reassignmentProcessInfo::reassignmentProcessInfo() :
m_State(INACTIVE)
, m_PlayersInvolved(0)
, m_PlayerMsg(0)
, m_PlayerAck(0)
, m_RestartNum(0)
, m_RestartNumForTelemetry(0)
, m_LastUsedReassignIndex(0)
, m_NegotiationStartTime(0)
{
    m_ResponseHandlers.Resize(MAX_NUM_PHYSICAL_PLAYERS);

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PlayerNegotiationData[index].m_PlayerHasPeer       = 0;
        m_PlayerNegotiationData[index].m_PlayerNegResponse   = 0;
        m_PlayerNegotiationData[index].m_HasProcessedNegData = 0;
        m_PlayerNegotiationData[index].m_LeavingPlayerPeerID = 0;
#if ENABLE_NETWORK_LOGGING
        m_PlayerNegotiationData[index].m_LeavingPeerName[0] = '\0';
#endif // ENABLE_NETWORK_LOGGING
    }
}

reassignmentProcessInfo::~reassignmentProcessInfo()
{
    m_ResponseHandlers.Reset();
}

void reassignmentProcessInfo::ClearPlayerInvolved(PhysicalPlayerIndex player)
{
    m_PlayersInvolved &= ~(1<<player);
    m_PlayerAck       &= ~(1<<player);
    m_PlayerMsg       &= ~(1<<player);

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PlayerNegotiationData[index].m_PlayerHasPeer       &= ~(1<<player);
        m_PlayerNegotiationData[index].m_PlayerNegResponse   &= ~(1<<player);
        m_PlayerNegotiationData[index].m_HasProcessedNegData &= ~(1<<player);
    }
}

u64 reassignmentProcessInfo::GetLastLeavingPeerID() const
{
    return m_PlayerNegotiationData[m_LastUsedReassignIndex].m_LeavingPlayerPeerID;
}

bool reassignmentProcessInfo::IsReassigningForPeerID(u64 peerID) const
{
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            return true;
        }
    }

    return false;
}

void reassignmentProcessInfo::GetLeavingPeersToNegotiateFor(PhysicalPlayerIndex targetPlayer, u64 *leavingPeers, unsigned &numLeavingPeers)
{
    numLeavingPeers = 0;

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID != 0 &&
          (m_PlayerNegotiationData[index].m_PlayerNegResponse & (1<<targetPlayer)) != 0)
        {
            leavingPeers[numLeavingPeers] = m_PlayerNegotiationData[index].m_LeavingPlayerPeerID;
            numLeavingPeers++;
        }
    }
}

void reassignmentProcessInfo::SetWaitingOnNegResponse(PhysicalPlayerIndex player, u64 peerID)
{
    if(gnetVerifyf(player < MAX_NUM_PHYSICAL_PLAYERS, "Invalid physical player index specified!"))
    {
        bool found = false;

        for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS && !found; index++)
        {
            if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
            {
                m_PlayerNegotiationData[index].m_PlayerHasPeer     |= 1<<player;
                m_PlayerNegotiationData[index].m_PlayerNegResponse |= 1<<player;
                found = true;
            }
        }

        gnetAssertf(found, "Trying to set waiting on a negotiation response for a peer that is not being reassigned!");
    }
}

void reassignmentProcessInfo::ClearWaitingOnNegResponse(PhysicalPlayerIndex player, u64 peerID)
{
    if(gnetVerifyf(player < MAX_NUM_PHYSICAL_PLAYERS, "Invalid physical player index specified!"))
    {
        bool found = false;

        for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS && !found; index++)
        {
            if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
            {
                m_PlayerNegotiationData[index].m_PlayerNegResponse &= ~(1<<player);
                found = true;
            }
        }

        gnetAssertf(found, "Trying to clear waiting on a negotiation response for a peer that is not being reassigned!");
    }
}

bool reassignmentProcessInfo::IsWaitingOnNegResponse(PhysicalPlayerIndex player, u64 peerID)
{
    bool waiting = false;
    bool found   = false;

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS && !found; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            if((m_PlayerNegotiationData[index].m_PlayerNegResponse & (1<<player)) != 0)
            {
                waiting = true;
            }

            found = true;
        }
    }

    return waiting;
}

bool reassignmentProcessInfo::IsWaitingOnNegResponse(u64 peerID)
{
    bool waiting = false;
    bool found   = false;

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS && !found; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            if(m_PlayerNegotiationData[index].m_PlayerNegResponse != 0)
            {
                waiting = true;
            }

            found = true;
        }
    }

    return waiting;
}

#if ENABLE_NETWORK_LOGGING

const char *reassignmentProcessInfo::GetReassignmentStateName()
{
    switch(m_State)
    {
    case INACTIVE:
        return "INACTIVE";
    case NEGOTIATING:
        return "NEGOTIATING";
    case CONFIRMING:
        return "CONFIRMING";
    case FINISHING:
        return "FINISHING";
    default:
        gnetAssertf(0, "Invalid reassignment state: %d!", m_State);
        return "UNKNOWN";
    }
}

void reassignmentProcessInfo::LogReassignmentState(netLoggingInterface &log)
{
    NetworkLogUtils::WriteLogEvent(log, "REASSIGN_PROCESS_STATE" , "\r\n");
    log.WriteDataValue("Current State", GetReassignmentStateName());

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if((m_PlayersInvolved & (1<<index))!= 0)
        {
            bool waitingOnMessage = IsWaitingOnMsg(index);
            bool waitingOnACK     = IsWaitingOnAck(index);

            netPlayer  *player     = netInterface::GetPlayerMgr().GetPhysicalPlayerFromIndex(index);
            const char *playerName = player ? player->GetLogName() : "Unknown";

            if(m_State == NEGOTIATING)
            {
                log.WriteDataValue(playerName, "%s%s%s", waitingOnMessage ? "WAITING_FOR_NEG " : "", waitingOnACK ? "WAITING_FOR_NEG_ACK" : "", (!waitingOnMessage && !waitingOnACK) ? "FINISHED_NEGOTIATING" : "");
            }
            else if(m_State == CONFIRMING)
            {
                log.WriteDataValue(playerName, "%s%s%s", waitingOnMessage ? "WAITING_FOR_CFM " : "", waitingOnACK ? "WAITING_FOR_CFM_ACK" : "", (!waitingOnMessage && !waitingOnACK) ? "FINISHED_CONFIRMING" : "");
            }
        }
    }

    if(m_State == NEGOTIATING)
    {
        for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
        {
            const NegotiationData &negotiationData = m_PlayerNegotiationData[index];

            if(negotiationData.m_LeavingPlayerPeerID != 0)
            {
                log.WriteDataValue("LEAVING_PEER_NEGOTIATION_STATE" , "%s", negotiationData.m_LeavingPeerName);

                for(PhysicalPlayerIndex index2 = 0; index2 < MAX_NUM_PHYSICAL_PLAYERS; index2++)
                {
                    if((negotiationData.m_PlayerHasPeer & (1<<index2))!= 0)
                    {
                        bool waitingOnResponse = (negotiationData.m_PlayerNegResponse   & (1<<index2)) != 0;
                        bool processedData     = (negotiationData.m_HasProcessedNegData & (1<<index2)) != 0;

                        netPlayer  *player     = netInterface::GetPlayerMgr().GetPhysicalPlayerFromIndex(index2);
                        const char *playerName = player ? player->GetLogName() : "Unknown";

                        log.WriteDataValue(playerName, "%s%s%s", waitingOnResponse ? "WAITING_FOR_RESPONSE " : "", processedData ? "PROCESSED_DATA" : "", (!waitingOnResponse && !processedData) ? "WAITING_FOR_NEG_DATA" : "");
                    }
                }
            }
        }
    }
}

#endif // ENABLE_NETWORK_LOGGING

bool reassignmentProcessInfo::IsWaitingOnAnyNegResponse()
{
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID != 0 && m_PlayerNegotiationData[index].m_PlayerNegResponse != 0)
        {
            return true;
        }
    }

    return false;
}

bool reassignmentProcessInfo::HasProcessedNegotiationData(PhysicalPlayerIndex player, u64 peerID)
{
    bool playerExists                = false;
    bool hasProcessedNegotiationData = true;

    // Note the same peer may exist twice in the negotiation data if the player has
    // managed to rejoin and leave again while the reassignment process has not completed
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            playerExists = true;

            if((m_PlayerNegotiationData[index].m_HasProcessedNegData & (1<<player)) == 0)
            {
                hasProcessedNegotiationData = false;
            }
        }
    }

    return playerExists && hasProcessedNegotiationData;
}

void reassignmentProcessInfo::MarkProcessedNegotiationData(PhysicalPlayerIndex player, u64 peerID)
{
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            m_PlayerNegotiationData[index].m_HasProcessedNegData |= (1<<player);
        }
    }
}

bool reassignmentProcessInfo::HasAnyPeer(PhysicalPlayerIndex player) const
{
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID != 0 &&
          (m_PlayerNegotiationData[index].m_PlayerHasPeer & (1<<player)) != 0)
        {
            return true;
        }
    }

    return false;
}

void reassignmentProcessInfo::SetHasPeer(PhysicalPlayerIndex player, u64 peerID, bool hasPeer)
{
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            if(hasPeer)
            {
                m_PlayerNegotiationData[index].m_PlayerHasPeer     |= (1<<player);
                m_PlayerNegotiationData[index].m_PlayerNegResponse |= (1<<player);
            }
            else
            {
                m_PlayerNegotiationData[index].m_PlayerHasPeer     &= ~(1<<player);
                m_PlayerNegotiationData[index].m_PlayerNegResponse &= ~(1<<player);
            }
        }
    }
}

void reassignmentProcessInfo::AddObjectInfo(reassignObjectInfo &objectInfo)
{
    m_ObjectList.push_back(&objectInfo);
}

void reassignmentProcessInfo::RemoveInfo(reassignObjectInfo& objectInfo)
{
    m_ObjectList.erase(&objectInfo);
}

reassignObjectInfo *reassignmentProcessInfo::FindInfo(const ObjectId objectId)
{
    gnetAssertf(IsActive(), "Trying to find info for an object while a reassignment is not in progress!");
    ObjectList::iterator curr      = m_ObjectList.begin();
    ObjectList::const_iterator end = m_ObjectList.end();

    for(; curr != end; ++curr)
    {
        reassignObjectInfo *objectInfo    = *curr;
        netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

        if (gnetVerifyf(networkObject, "Invalid object on the reassignment list!") &&
            networkObject->GetObjectID() == objectId)
        {
            return objectInfo;
        }
    }

    return 0;
}

void reassignmentProcessInfo::Start(u64 leavingPeerID, const char *LOGGING_ONLY(leavingPeerName))
{
    gnetAssertf(m_State == INACTIVE, "Starting a reassignment when one is already in progress, use Restart instead!");
    m_State                 = NEGOTIATING;
    m_PlayersInvolved       = 0;
    m_PlayerMsg             = 0;
    m_PlayerAck             = 0;
    m_RestartNum            = 0;
	m_RestartNumForTelemetry= 0;
	m_LastUsedReassignIndex = 0;
	m_NegotiationStartTime = fwTimer::GetSystemTimeInMilliseconds();

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PlayerNegotiationData[index].m_PlayerHasPeer       = 0;
        m_PlayerNegotiationData[index].m_PlayerNegResponse   = 0;
        m_PlayerNegotiationData[index].m_HasProcessedNegData = 0;
        m_PlayerNegotiationData[index].m_LeavingPlayerPeerID = 0;
#if ENABLE_NETWORK_LOGGING
        m_PlayerNegotiationData[index].m_LeavingPeerName[0]  = '\0';
#endif // ENABLE_NETWORK_LOGGING
    }

    m_PlayerNegotiationData[m_LastUsedReassignIndex].m_LeavingPlayerPeerID = leavingPeerID;
#if ENABLE_NETWORK_LOGGING
    safecpy(m_PlayerNegotiationData[m_LastUsedReassignIndex].m_LeavingPeerName, leavingPeerName, MAX_LEAVING_PEER_NAME);
#endif // ENABLE_NETWORK_LOGGING
}

void reassignmentProcessInfo::Finish()
{
    m_State           = INACTIVE;
    m_PlayersInvolved = 0;
    m_PlayerMsg       = 0;
    m_PlayerAck       = 0;
    m_RestartNum      = 0;
	m_RestartNumForTelemetry = 0;
	m_NegotiationStartTime = 0;

    // cancel any responses we are waiting for
    for (PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_ResponseHandlers[index].Cancel();

        m_PlayerNegotiationData[index].m_PlayerHasPeer       = 0;
        m_PlayerNegotiationData[index].m_PlayerNegResponse   = 0;
        m_PlayerNegotiationData[index].m_HasProcessedNegData = 0;
        m_PlayerNegotiationData[index].m_LeavingPlayerPeerID = 0;
#if ENABLE_NETWORK_LOGGING
        m_PlayerNegotiationData[index].m_LeavingPeerName[0]  = '\0';
#endif // ENABLE_NETWORK_LOGGING
    }
}

void reassignmentProcessInfo::Restart(u64 leavingPeerID, const char *LOGGING_ONLY(leavingPeerName))
{
    gnetAssertf(m_RestartNum <= MAX_RESTART_NUM, "Unexpected restart number when restarting the object reassignment process!");

    m_State           = NEGOTIATING;
    m_PlayersInvolved = 0;
    m_PlayerMsg       = 0;
    m_PlayerAck       = 0;
    m_RestartNum      = (m_RestartNum == MAX_RESTART_NUM) ? 0 : m_RestartNum+1;
	m_RestartNumForTelemetry++;

    // cancel any responses we are waiting for
    for (PhysicalPlayerIndex player = 0; player < MAX_NUM_PHYSICAL_PLAYERS; player++)
    {
        m_ResponseHandlers[player].Cancel();
    }

    if(m_LastUsedReassignIndex == MAX_NUM_PHYSICAL_PLAYERS)
    {
        gnetWarning("Reassigned players count is wrapping...");
    }

    m_LastUsedReassignIndex++;
    m_LastUsedReassignIndex%=MAX_NUM_PHYSICAL_PLAYERS;
    m_PlayerNegotiationData[m_LastUsedReassignIndex].m_PlayerHasPeer       = 0;
    m_PlayerNegotiationData[m_LastUsedReassignIndex].m_PlayerNegResponse   = 0;
    m_PlayerNegotiationData[m_LastUsedReassignIndex].m_HasProcessedNegData = 0;
    m_PlayerNegotiationData[m_LastUsedReassignIndex].m_LeavingPlayerPeerID = leavingPeerID;
#if ENABLE_NETWORK_LOGGING
    safecpy(m_PlayerNegotiationData[m_LastUsedReassignIndex].m_LeavingPeerName, leavingPeerName, MAX_LEAVING_PEER_NAME);
#endif // ENABLE_NETWORK_LOGGING
}

void reassignmentProcessInfo::MarkAllObjectsConfirmed()
{
    gnetAssertf(IsActive(), "Trying to mark all objects as confirmed while a reassignment is not in progress!");
    ObjectList::iterator curr      = m_ObjectList.begin();
    ObjectList::const_iterator end = m_ObjectList.end();

    for(; curr != end; ++curr)
    {
        reassignObjectInfo *objectInfo = *curr;

        if (gnetVerifyf(objectInfo, "Invalid object on the reassignment list!"))
        {
            objectInfo->SetConfirmed();
        }
    }
}

void reassignmentProcessInfo::MarkLocalOnlyObjectsConfirmed()
{
    gnetAssertf(IsActive(), "Trying to mark all objects as confirmed while a reassignment is not in progress!");
    ObjectList::iterator curr      = m_ObjectList.begin();
    ObjectList::const_iterator end = m_ObjectList.end();

    for(; curr != end; ++curr)
    {
        reassignObjectInfo *objectInfo = *curr;

        if (gnetVerifyf(objectInfo, "Invalid object on the reassignment list!"))
        {
            if(objectInfo->GetPlayerFlags() == 0)
            {
                gnetAssertf(objectInfo->GetOwnerPlayer() == netInterface::GetLocalPhysicalPlayerIndex(),
                            "Marking a local object confirmed not belonging to the local player!");
                objectInfo->SetConfirmed();
            }
        }
    }
}

#if __BANK

void reassignmentProcessInfo::DisplayDebugInfo()
{
    bool reassignInProgress = IsActive();

    grcDebugDraw::AddDebugOutput("Object reassignment is%s in progress", reassignInProgress ? "" : " not");

    if(reassignInProgress)
    {
        switch(m_State)
        {
        case NEGOTIATING:
            {
                grcDebugDraw::AddDebugOutput("Current state: NEGOTIATING");

                for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
                {
                    u64         playerToReassign  = m_PlayerNegotiationData[index].m_LeavingPlayerPeerID;
                    PlayerFlags playerNegResponse = m_PlayerNegotiationData[index].m_PlayerNegResponse;
                    if(playerToReassign != 0 && playerNegResponse != 0)
                    {
                        grcDebugDraw::AddDebugOutput("Reassignment for peer: %llx", playerToReassign);
                        grcDebugDraw::AddDebugOutput("\tWaiting for responses from:");

                        for (PhysicalPlayerIndex index2 = 0; index2 < MAX_NUM_PHYSICAL_PLAYERS; index2++)
                        {
                            if((playerNegResponse & (1<<index2))!= 0)
                            {
                                netPlayer *player = netInterface::GetPlayerMgr().GetPhysicalPlayerFromIndex(index2);

                                if(gnetVerifyf(player, "Waiting for a negotiation response from a player that doesn't exist!"))
                                {
                                    grcDebugDraw::AddDebugOutput("\t%s", player->GetLogName());
                                }
                            }
                        }
                    }
                }
            }
            break;
        case CONFIRMING:
            {
                grcDebugDraw::AddDebugOutput("Current state: CONFIRMING");
                grcDebugDraw::AddDebugOutput("Waiting for responses from:");

                for (PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
                {
                    if(IsWaitingOnAck(index))
                    {
                        netPlayer *player = netInterface::GetPlayerMgr().GetPhysicalPlayerFromIndex(index);

                        if(gnetVerifyf(player, "Waiting for an ACK from a player that doesn't exist!"))
                        {
                            grcDebugDraw::AddDebugOutput(player->GetLogName());
                        }
                    }
                }
            }
            break;
        case FINISHING:
            {
                grcDebugDraw::AddDebugOutput("Current state: FINISHING");
            }
            break;
        default:
            gnetAssertf(0, "Unexpected state!");
        }

        grcDebugDraw::AddDebugOutput("Objects Involved:");

        ObjectList::iterator curr      = m_ObjectList.begin();
        ObjectList::const_iterator end = m_ObjectList.end();

        for(; curr != end; ++curr)
        {
            reassignObjectInfo *objectInfo = *curr;

            if (gnetVerifyf(objectInfo, "Invalid object on the reassignment list!"))
            {
                netObject *networkObject = objectInfo ? objectInfo->GetObject() : 0;

                if(gnetVerifyf(networkObject, "Invalid object on the reassignment list!"))
                {
                    grcDebugDraw::AddDebugOutput("\t%s", networkObject->GetLogName());
                }
            }
        }
    }
}

#endif // __BANK

const char *reassignmentProcessInfo::GetLeavingPeerName(u64 peerID) const
{
#if ENABLE_NETWORK_LOGGING
    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerNegotiationData[index].m_LeavingPlayerPeerID == peerID)
        {
            return m_PlayerNegotiationData[index].m_LeavingPeerName;
        }
    }
#else
	(void)peerID;
#endif // ENABLE_NETWORK_LOGGING

    return "Unknown";
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// netObjectReassignMgr
///////////////////////////////////////////////////////////////////////////////////////////////////////
netObjectReassignMgr::netObjectReassignMgr() :
m_ObjectMgr(NULL)
, m_Log(0)
, m_IsInitialised(false)
, m_ObjectRemovedCb(nullptr)
#if __DEV
, m_NegotiateMsgRecorderId(INVALID_RECORDER_ID)
, m_ConfirmMsgRecorderId(INVALID_RECORDER_ID)
, m_ResponseMsgRecorderId(INVALID_RECORDER_ID)
#endif
#if __BANK
, m_NumReassignNegotiateMessagesSent(0)
, m_NumReassignConfirmMessagesSent(0)
, m_NumReassignResponseMessagesSent(0)
#endif // __BANK
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PendingMessageInfo[index].m_TimeToSend  = 0;
        m_PendingMessageInfo[index].m_MessageType = NO_MESSAGE;
    }
}

netObjectReassignMgr::~netObjectReassignMgr()
{
}

void netObjectReassignMgr::Init(netObjectMgrBase &objectManager)
{
    if(gnetVerifyf(!m_IsInitialised, "Initialising the object reassignment manager when it is already initialised!"))
    {
		static bool s_HasCreatedLog = false; 
		m_Log = rage_new netLog("ReassignManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                       netInterface::GetDefaultLogFileBlockingMode());
		s_HasCreatedLog = true; 
		
        m_ObjectMgr = &objectManager;

        m_MessageHandler.Bind(this, &netObjectReassignMgr::HandleReassignRequest);

        netInterface::AddRequestHandler(&m_MessageHandler);

        reassignObjectInfo::InitPool(MEMBUCKET_NETWORK);

        for (PhysicalPlayerIndex player = 0; player < MAX_NUM_PHYSICAL_PLAYERS; player++)
        {
            m_ReassignmentInfo.GetResponseHandler(player).Bind(this, &netObjectReassignMgr::HandleReassignResponse);

            m_PendingMessageInfo[player].m_TimeToSend  = 0;
            m_PendingMessageInfo[player].m_MessageType = NO_MESSAGE;
        }

#if __BANK
        m_ObjectMgr->GetBandwidthMgr().RegisterStatistics(m_BandwidthStats, "Reassign Manager");

        m_NegotiateMsgRecorderId = m_BandwidthStats.AllocateBandwidthRecorder("Reassign negotiate msgs");
        m_ConfirmMsgRecorderId   = m_BandwidthStats.AllocateBandwidthRecorder("Reassign confirm msgs");
        m_ResponseMsgRecorderId  = m_BandwidthStats.AllocateBandwidthRecorder("Reassign response msgs");

        m_NumReassignNegotiateMessagesSent = 0;
        m_NumReassignConfirmMessagesSent   = 0;
        m_NumReassignResponseMessagesSent  = 0;
#endif

        m_IsInitialised = true;
    }
}

void netObjectReassignMgr::Shutdown()
{
    if (gnetVerifyf(m_IsInitialised, "Shutting down the object reassignment manager before it has been initialised!"))
    {
        if(m_ReassignmentInfo.IsActive())
        {
            reassignmentProcessInfo::ObjectList::iterator       obj_curr = m_ReassignmentInfo.GetObjectList().begin();
            reassignmentProcessInfo::ObjectList::const_iterator obj_end  = m_ReassignmentInfo.GetObjectList().end();

            while (obj_curr != obj_end)
            {
                reassignObjectInfo *objectInfo    = *obj_curr;
                netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

                if(gnetVerifyf(networkObject, "Invalid object on the reassignment list!"))
                {
                    networkObject->SetLocalFlag(netObject::LOCALFLAG_BEINGREASSIGNED, false);
                    m_ObjectMgr->UnregisterNetworkObject(networkObject, netObjectMgrBase::REASSIGNMENT_SHUTDOWN, true, true);
                }

                obj_curr = m_ReassignmentInfo.GetObjectList().erase(obj_curr);
                delete objectInfo;
            }

            m_ReassignmentInfo.Finish();
        }

        netInterface::RemoveRequestHandler(&m_MessageHandler);

        reassignObjectInfo::ShutdownPool();

        delete m_Log;
        m_Log = 0;

        m_IsInitialised = false;
    }
}

void netObjectReassignMgr::Update()
{
    if(m_ReassignmentInfo.IsActive())
    {        
        // send any pending messages
        const unsigned           currentTime              = sysTimer::GetSystemMsTime();
        unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *player = remotePhysicalPlayers[index];

            PhysicalPlayerIndex playerIndex = player->GetPhysicalPlayerIndex();

            if(m_PendingMessageInfo[playerIndex].m_MessageType != NO_MESSAGE)
            {
                bool clearPendingMessage = false;

                if(!m_ReassignmentInfo.IsPlayerInvolved(playerIndex))
                {
                    clearPendingMessage = true;
                }
                else
                {
                    if(currentTime >= m_PendingMessageInfo[playerIndex].m_TimeToSend)
                    {
                        if(m_PendingMessageInfo[playerIndex].m_MessageType == NEGOTIATE_MESSAGE && m_ReassignmentInfo.IsNegotiating())
                        {
                            SendNegotiateMsg(player);
                        }
                        else if(gnetVerifyf(m_PendingMessageInfo[playerIndex].m_MessageType == CONFIRM_MESSAGE, "Unexpected pending message type!") &&
                                m_ReassignmentInfo.IsConfirming())
                        {
                            SendConfirmMsg(player);
                        }

                        clearPendingMessage = true;
                    }
                }

                if(clearPendingMessage)
                {
                    m_PendingMessageInfo[playerIndex].m_TimeToSend  = 0;
                    m_PendingMessageInfo[playerIndex].m_MessageType = NO_MESSAGE;
                }
            }
        }

		// see if we can move onto the next stage of the reassignment process
		if (!m_ReassignmentInfo.IsWaitingOnAnyMsg() && !m_ReassignmentInfo.IsWaitingOnAnyAck())
		{
			if (m_ReassignmentInfo.IsNegotiating())
			{
				if(!m_ReassignmentInfo.IsWaitingOnAnyNegResponse())
				{
					StartReassignConfirmation();
				}
			}
			else
			{
				ReassignObjects();
			}
		}

		sm_IsReassignmentInProgress = IsReassignmentInProgress();
    }
}

netObject* netObjectReassignMgr::FindNetworkObject(const ObjectId objectId)
{
    reassignObjectInfo *info   = FindObjectInfo(objectId);
    netObject          *object = info ? info->GetObject() : 0;

    gnetAssertf(!info || info->GetObject(), "Reassignment info exists without a valid network object!");
    gnetAssertf(!object || object->IsLocalFlagSet(netObject::LOCALFLAG_BEINGREASSIGNED),
                "%s is on the reassign list without the reassign flag set!", object->GetLogName());

    return object;
}

void netObjectReassignMgr::PlayerHasJoined(const netPlayer &player)
{
    NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_JOINED", "%s", player.GetLogName());

    if(m_ReassignmentInfo.IsActive())
    {
        reassignmentProcessInfo::ObjectList::iterator       obj_curr = m_ReassignmentInfo.GetObjectList().begin();
        reassignmentProcessInfo::ObjectList::const_iterator obj_end  = m_ReassignmentInfo.GetObjectList().end();

        for(; obj_curr != obj_end; ++obj_curr)
        {
            reassignObjectInfo *objectInfo = *obj_curr;
            netObject          *object     = objectInfo ? objectInfo->GetObject() : 0;

            if(gnetVerifyf(object, "Invalid object on the reassignment process list!"))
            {
                object->PlayerHasJoined(player);
            }
        }
    }

    // ensure any old pending messages have been cleared out
    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

    if(playerIndex != INVALID_PLAYER_INDEX)
    {
        m_PendingMessageInfo[playerIndex].m_TimeToSend  = 0;
        m_PendingMessageInfo[playerIndex].m_MessageType = NO_MESSAGE;
    }
}

void netObjectReassignMgr::PlayerHasLeft(const netPlayer &player)
{
#if __BANK
    if(sm_RejectIgnoreLeavingPlayers)
    {
        NetworkLogUtils::WriteLogEvent(GetLog(), "IGNORING_PLAYER_HAS_LEFT", "%s (DEBUG ONLY)", player.GetLogName());

        PhysicalPlayerIndex leavingPlayerIndex = player.GetPhysicalPlayerIndex();

        if (gnetVerifyf(leavingPlayerIndex != INVALID_PLAYER_INDEX, 
            "Ignoring a player has left for a player with an invalid physical player index!"))
        {
            DestroyObjectsNotToBeReassigned(leavingPlayerIndex);
        }
        return;
    }
#endif // __BANK
    NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_LEFT", "%s%s", player.GetLogName(), player.IsHost() ? " (host)" : "");

    if(m_ReassignmentInfo.IsActive())
    {
        reassignmentProcessInfo::ObjectList::iterator       obj_curr = m_ReassignmentInfo.GetObjectList().begin();
        reassignmentProcessInfo::ObjectList::const_iterator obj_end  = m_ReassignmentInfo.GetObjectList().end();

        for(; obj_curr != obj_end; ++obj_curr)
        {
            reassignObjectInfo *objectInfo = *obj_curr;
            netObject          *object     = objectInfo ? objectInfo->GetObject() : 0;

            if(gnetVerifyf(object, "Invalid object on the reassignment process list!"))
            {
                object->PlayerHasLeft(player);
            }

            // clear the player flags for the leaving player, and if this object is now only known
            // about locally and we are in the confirming state, confirm the object. All other local
            // only objects will already have been confirmed by this stage
            if(objectInfo)
            {
                objectInfo->ClearPlayerHasObject(player.GetPhysicalPlayerIndex());

                if(objectInfo->GetPlayerFlags() == 0 && m_ReassignmentInfo.IsConfirming())
                {
                    objectInfo->SetConfirmed();
                }
            }
        }

        m_ReassignmentInfo.ClearPlayerInvolved(player.GetPhysicalPlayerIndex());
    }

    // ensure any old pending messages are cleared out
    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

    if(playerIndex != INVALID_PLAYER_INDEX)
    {
        m_PendingMessageInfo[playerIndex].m_TimeToSend  = 0;
        m_PendingMessageInfo[playerIndex].m_MessageType = NO_MESSAGE;
    }

    StartReassignNegotiation(player);
}

bool netObjectReassignMgr::IsReassignmentInProgress() const
{
    return m_ReassignmentInfo.IsActive();
}

u32 netObjectReassignMgr::GetReassignmentRunningTimeMs() const
{
	if (gnetVerifyf(IsReassignmentInProgress(), "Trying to get the reassignment running time while no assignment in process"))
	{
		return (fwTimer::GetSystemTimeInMilliseconds() - GetNegotiationStartTime());
	}
	return 0;
}

u32 netObjectReassignMgr::GetReassignmentPlayersInvolvedCount() const
{
	u32 playersInvolved = 0;
	if (gnetVerifyf(IsReassignmentInProgress(), "Trying to get the reassignment players count while no assignment in process"))
	{
		for (u8 i = 0; i < MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			if (m_ReassignmentInfo.IsPlayerInvolved(i))
				playersInvolved++;
		}
	}
	return playersInvolved;
}

unsigned netObjectReassignMgr::GetNumObjectsBeingReassigned(fnNetObjectPredicate includePredicate) const
{
    unsigned count = 0;

    if(m_ReassignmentInfo.IsActive())
    {
        // if there is no inclusion predicate specified we include all objects, so
        // we implement a special case for performance reasons
        if(includePredicate == 0)
        {
            count = (unsigned)m_ReassignmentInfo.GetObjectList().size();
        }
        else
        {
            reassignmentProcessInfo::ObjectList::const_iterator obj_curr = m_ReassignmentInfo.GetObjectList().begin();
            reassignmentProcessInfo::ObjectList::const_iterator obj_end  = m_ReassignmentInfo.GetObjectList().end();

            for(; obj_curr != obj_end; ++obj_curr)
            {
                const reassignObjectInfo *objectInfo    = *obj_curr;
                const netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

                if(gnetVerifyf(networkObject, "Invalid object on the reassignment list!") && (*includePredicate)(networkObject))
                {
                    count++;
                }
            }
        }
    }

    return count;
}

bool netObjectReassignMgr::IsObjectOnReassignmentList(netObject &networkObject) const
{
    if(m_ReassignmentInfo.IsActive())
    {
        reassignmentProcessInfo::ObjectList::const_iterator obj_curr = m_ReassignmentInfo.GetObjectList().begin();
        reassignmentProcessInfo::ObjectList::const_iterator obj_end  = m_ReassignmentInfo.GetObjectList().end();

        for(; obj_curr != obj_end; ++obj_curr)
        {
            const reassignObjectInfo *objectInfo     = *obj_curr;
            const netObject          *reassignObject = objectInfo ? objectInfo->GetObject() : 0;

            if(reassignObject == &networkObject)
            {
                return true;
            }
        }
    }

    return false;
}

void netObjectReassignMgr::RemoveObject(netObject &networkObject)
{
    if(m_ReassignmentInfo.IsActive())
    {
        reassignObjectInfo *objectInfo = m_ReassignmentInfo.FindInfo(networkObject.GetObjectID());

        if (objectInfo)
        {
            objectInfo->SetToBeRemoved();
        }
    }
}

const char *netObjectReassignMgr::GetLeavingPeerName(u64 peerID) const
{
    return m_ReassignmentInfo.GetLeavingPeerName(peerID);
}

void  netObjectReassignMgr::AddObjectToReassignList(netObject& networkObject)
{
	unsigned reassignPriority = networkObject.CalcReassignPriority();

	netPlayer *localPlayer = netInterface::GetLocalPlayer();

	if(gnetVerifyf(localPlayer, "Invalid local player when starting reassignment negotiation process!"))
	{
		AddObjectToReassignList(networkObject, reassignPriority, *netInterface::GetLocalPlayer());
	}
}

void netObjectReassignMgr::SetTelemetryCallback(fnTelemetryRemovedCallback objectRemovedCb)
{
	m_ObjectRemovedCb = objectRemovedCb;
}

#if __BANK

void netObjectReassignMgr::AddDebugWidgets()
{
    bkBank *bank = BANKMGR.FindBank("Network");

    if(Verifyf(bank, "Unable to find network bank!"))
    {
        bank->PushGroup("Network Object Reassignment", false);
        {
            bank->AddToggle("Reject all negotiation  messages",    &sm_RejectAllNegotiationMessages);
            bank->AddToggle("Reject all confirmation messages",    &sm_RejectAllConfirmationMessages);
            bank->AddToggle("Ignore leaving players",              &sm_RejectIgnoreLeavingPlayers);
        }
        bank->PopGroup();
    }
}

void netObjectReassignMgr::DisplayDebugInfo()
{
    m_ReassignmentInfo.DisplayDebugInfo();
}

#endif // __BANK

unsigned int netObjectReassignMgr::GetAllObjectsBeingReassigned(netObject **objectArray, unsigned int sizeofArray, bool scriptObjectsOnly)
{
    unsigned int objectCount = 0;

    if(m_ReassignmentInfo.IsActive())
    {
        reassignmentProcessInfo::ObjectList::iterator       obj_curr = m_ReassignmentInfo.GetObjectList().begin();
        reassignmentProcessInfo::ObjectList::const_iterator obj_end  = m_ReassignmentInfo.GetObjectList().end();

        for(; obj_curr != obj_end; ++obj_curr)
        {
            reassignObjectInfo *objectInfo    = *obj_curr;
            netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

            if(gnetVerifyf(networkObject, "Invalid object on the reassignment list!") &&
               gnetVerifyf(objectCount < sizeofArray, "Size of array passed to netObjectReassignMgr::GetAllObjectsBeingReassigned is too small!"))
            {
                if (!scriptObjectsOnly || networkObject->GetScriptHandlerObject())
                {
                    objectArray[objectCount] = networkObject;
                    objectCount++;
                }
            }
        }
    }

    return objectCount;
}

reassignObjectInfo *netObjectReassignMgr::FindObjectInfo(const ObjectId objectId)
{
    reassignObjectInfo *objectInfo = 0;

    if(m_ReassignmentInfo.IsActive())
    {
        objectInfo = m_ReassignmentInfo.FindInfo(objectId);
    }

    return objectInfo;
}

void netObjectReassignMgr::DestroyObjectsNotToBeReassigned(PhysicalPlayerIndex leavingPlayerIndex)
{
    if(gnetVerifyf(leavingPlayerIndex != INVALID_PLAYER_INDEX, "Trying to destroy objects for an invalid player!"))
    {
        atDNode<netObject*, datBase> *objectNode = m_ObjectMgr->m_aPlayerObjectList[leavingPlayerIndex].GetHead();

        while(objectNode)
        {
            atDNode<netObject*, datBase> *nextNode = objectNode->GetNext();

            netObject *networkObject = objectNode->Data;

            if (gnetVerifyf(networkObject, "Invalid object on the reassignment list!") &&
                !networkObject->NeedsReassigning())
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Removed");

                m_ObjectMgr->UnregisterNetworkObject(networkObject, netObjectMgrBase::REASSIGNMENT_NO_REASSIGN, true, true);
            }

            objectNode = nextNode;
        }
    }
}

reassignObjectInfo *netObjectReassignMgr::AddObjectToReassignList(netObject       &networkObject,
                                                                  unsigned         reassignPriority,
                                                                  const netPlayer &leadingCandidate)
{
    reassignObjectInfo *objectInfo = 0;

    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Adding an object to the reassignment list when a reassignment is not in progress!"))
    {
        atDNode<netObject*, datBase> *objectNode = networkObject.GetListNode();

        if (gnetVerifyf(objectNode, "Adding a network object to the reassignment list that is not on a player list!") &&
            gnetVerifyf(networkObject.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Unexpected physical player index!"))
		{
			DEV_ONLY(m_ObjectMgr->AssertObjectOnList(m_ObjectMgr->m_aPlayerObjectList[networkObject.GetPhysicalPlayerIndex()], networkObject));
			m_ObjectMgr->m_aPlayerObjectList[networkObject.GetPhysicalPlayerIndex()].PopNode(*objectNode);
			delete objectNode;

			networkObject.SetLocalFlag(netObject::LOCALFLAG_BEINGREASSIGNED, true);
			networkObject.ClearPendingPlayerIndex();
			networkObject.SetListNode(0);
			networkObject.SetPlayerIndex(INVALID_PLAYER_INDEX);
		}

        objectInfo = rage_new reassignObjectInfo(networkObject, reassignPriority, leadingCandidate.GetPhysicalPlayerIndex());

        if (gnetVerifyf(objectInfo, "Failed to allocate new reassignObjectInfo!"))
        {
            m_ReassignmentInfo.AddObjectInfo(*objectInfo);
        }
    }

    return objectInfo;
}

void netObjectReassignMgr::RemoveObjectFromReassignList(reassignObjectInfo &objectInfo)
{
    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Removing an object from the reassignment list when a reassignment is not in progress!"))
    {
        netObject *networkObject = objectInfo.GetObject();

        if(gnetVerifyf(networkObject, "Trying to remove an invalid object from the reassignment list!"))
		{
			if (m_ObjectRemovedCb)
			{
				m_ObjectRemovedCb();
			}

            m_ReassignmentInfo.RemoveInfo(objectInfo);

            netPlayer *newOwner = netInterface::GetPhysicalPlayerFromIndex(objectInfo.GetOwnerPlayer());

            if(!gnetVerifyf(newOwner, "Object being reassigned to player %d which does not exist!", objectInfo.GetOwnerPlayer()))
            {
                networkObject->SetLocalFlag(netObject::LOCALFLAG_BEINGREASSIGNED, false);
                m_ObjectMgr->UnregisterNetworkObject(networkObject, netObjectMgrBase::REASSIGNMENT_NO_NEW_OWNER, true, true);
            }
            else
            {
				gnetAssertf(objectInfo.IsSetToBeRemoved() || networkObject->HasGameObject() || networkObject->CanReassignWithNoGameObject(), "Trying to reassign %s, with no game object!", networkObject->GetLogName());

				netInterface::GetObjectManager().ChangeOwner(*networkObject, *newOwner, MIGRATE_REASSIGNMENT);
 
				networkObject->SetLocalFlag(netObject::LOCALFLAG_BEINGREASSIGNED, false);

                // setup the cloned state of the object based on the players that we know own it also
                if (objectInfo.GetOwnerPlayer() == netInterface::GetLocalPhysicalPlayerIndex())
                {
                    networkObject->SetClonedState(objectInfo.GetPlayerFlags(), 0, 0);

                    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
                    const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

                    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
                    {
	                    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

                        if (objectInfo.DoesPlayerNeedAnUpdate(remotePlayer->GetPhysicalPlayerIndex()) && !objectInfo.IsSetToBeRemoved())
                        {
                            if(networkObject->GetSyncData())
                            {
                                ActivationFlags actFlags = networkObject->GetActivationFlags();
							    networkObject->GetSyncTree()->ForceSendOfSyncUpdateNodes(SERIALISEMODE_CRITICAL, actFlags, networkObject);
                            }
                        }
                    }
 
					if (networkObject->OnReassigned() || objectInfo.IsSetToBeRemoved())
                    {
                        m_ObjectMgr->UnregisterNetworkObject(networkObject, netObjectMgrBase::REASSIGNMENT_REMOVAL, false, true);
                    }
                }
            }

            delete &objectInfo;
        }
    }
}

void netObjectReassignMgr::StartReassignNegotiation(const netPlayer &leavingPlayer)
{
    NetworkLogUtils::WriteLogEvent(GetLog(), "START_REASSIGN_NEGOTIATE" , "%s\r\n", leavingPlayer.GetLogName());

    PhysicalPlayerIndex leavingPlayerIndex = leavingPlayer.GetPhysicalPlayerIndex();

    if (gnetVerifyf(leavingPlayerIndex != INVALID_PLAYER_INDEX, 
        "Trying to start an object reassignment for a player with an invalid physical player index!"))
    {
        DestroyObjectsNotToBeReassigned(leavingPlayerIndex);

        if(m_ReassignmentInfo.IsActive())
        {
            RestartReassignNegotiation(leavingPlayer.GetRlPeerId(), leavingPlayer.GetLogName());
        }
        else
        {
            m_ReassignmentInfo.Start(leavingPlayer.GetRlPeerId(), leavingPlayer.GetLogName());
        }

        // now reassign the remaining objects
        atDNode<netObject*, datBase> *objectNode = m_ObjectMgr->m_aPlayerObjectList[leavingPlayerIndex].GetHead();

        while(objectNode)
        {
            atDNode<netObject*, datBase> *nextNode = objectNode->GetNext();

            netObject *networkObject = objectNode->Data;

            if(gnetVerifyf(networkObject, "Invalid network object on leaving player's object list!"))
            {
                unsigned reassignPriority = networkObject->CalcReassignPriority();

                netPlayer *localPlayer = netInterface::GetLocalPlayer();

                if(gnetVerifyf(localPlayer, "Invalid local player when starting reassignment negotiation process!"))
                {
                    AddObjectToReassignList(*networkObject, reassignPriority, *netInterface::GetLocalPlayer());

                    GetLog().WriteDataValue(networkObject->GetLogName(), "Initial Priority %d (Initial Player %s)", reassignPriority, netInterface::GetLocalPlayer()->GetLogName());
                }
            }

            objectNode = nextNode;
        }

        gnetAssertf(m_ObjectMgr->m_aPlayerObjectList[leavingPlayerIndex].GetHead()== 0, "Unexpected object on player list!");

        PrepareForNegotiation(&leavingPlayer);
    }
}

void netObjectReassignMgr::PrepareForNegotiation(const netPlayer *leavingPlayer)
{
    // flag all the players that will be involved in the reassignment process
    bool foundPlayer = false;

    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
    const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
    {
        const netPlayer *player = remotePhysicalPlayers[index];

        if(player && player != leavingPlayer)
        {
            PhysicalPlayerIndex playerIndex = player->GetPhysicalPlayerIndex();

            if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Trying to send a negotiation message to a player with an invalid physical player index!"))
            {
                m_ReassignmentInfo.SetPlayerInvolved(player->GetPhysicalPlayerIndex());
                m_ReassignmentInfo.SetWaitingOnMsg(player->GetPhysicalPlayerIndex());

                m_PendingMessageInfo[player->GetPhysicalPlayerIndex()].m_TimeToSend  = sysTimer::GetSystemMsTime();
                m_PendingMessageInfo[player->GetPhysicalPlayerIndex()].m_MessageType = NEGOTIATE_MESSAGE;

                GetLog().WriteDataValue("Player involved", "%s", player->GetLogName());
                foundPlayer = true;
            }
        }
    }

    if(!foundPlayer)
    {
        GetLog().WriteDataValue("Players involved", "None");
        m_ReassignmentInfo.MarkAllObjectsConfirmed();
    }
}

void netObjectReassignMgr::RestartReassignNegotiation(u64 rlLeavingPeerID, const char *leavingPeerName)
{
    NetworkLogUtils::WriteLogEvent(GetLog(), "RESTART_REASSIGN_NEGOTIATE" , "\r\n");

    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Restarting a reassignment when one is not already in progress!"))
    {
        m_ReassignmentInfo.Restart(rlLeavingPeerID, leavingPeerName);

        reassignmentProcessInfo::ObjectList::iterator curr      = m_ReassignmentInfo.GetObjectList().begin();
        reassignmentProcessInfo::ObjectList::const_iterator end = m_ReassignmentInfo.GetObjectList().end();

        for(; curr != end; ++curr)
        {
            reassignObjectInfo *objectInfo = *curr;

            if(gnetVerifyf(objectInfo, "Invalid object info on reassignment process list!"))
            {
                netObject *networkObject = objectInfo->GetObject();

                if (gnetVerifyf(networkObject, "Invalid network object on reassignment process list!"))
                {
                    unsigned reassignPriority = networkObject->CalcReassignPriority();
                    GetLog().WriteDataValue(networkObject->GetLogName(), "New Priority %d (New Player %s)", reassignPriority, netInterface::GetLocalPlayer()->GetLogName());
                }

                objectInfo->Reset(netInterface::GetLocalPhysicalPlayerIndex());
            }
        }

        gnetAssertf(m_ReassignmentInfo.GetPlayersInvolved() == 0, "Unexpected player involved in object reassignment process!");
    }
}

void netObjectReassignMgr::StartReassignConfirmation()
{
    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Starting a reassignment confirmation when a reassignment is not in progress!"))
    {
        m_ReassignmentInfo.SetConfirming();
        m_ReassignmentInfo.MarkLocalOnlyObjectsConfirmed();

        // flag all the players we are waiting on confirm msgs from
        unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *player = remotePhysicalPlayers[index];

            if (m_ReassignmentInfo.IsPlayerInvolved(player->GetPhysicalPlayerIndex()))
            {
                m_ReassignmentInfo.SetWaitingOnMsg(player->GetPhysicalPlayerIndex());
                m_ReassignmentInfo.SetWaitingOnAck(player->GetPhysicalPlayerIndex());

                const unsigned CONFIRM_MESSAGE_DELAY = 250;
                m_PendingMessageInfo[player->GetPhysicalPlayerIndex()].m_TimeToSend  = sysTimer::GetSystemMsTime() + CONFIRM_MESSAGE_DELAY;
                m_PendingMessageInfo[player->GetPhysicalPlayerIndex()].m_MessageType = CONFIRM_MESSAGE;
            }
        }
    }
}

void netObjectReassignMgr::ReassignObjects()
{
    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Trying to reassign objects when a reassignment is not in progress!"))
    {
        m_ReassignmentInfo.SetFinishing();

        NetworkLogUtils::WriteLogEvent(GetLog(), "REASSIGNING_OBJECTS" , "\r\n");

        /// reassignmentProcessInfo::ObjectList::iterator       curr = m_ReassignmentInfo.GetObjectList().begin();
        /// reassignmentProcessInfo::ObjectList::const_iterator end  = m_ReassignmentInfo.GetObjectList().end();

        while(!m_ReassignmentInfo.GetObjectList().empty())
        {
            reassignObjectInfo *objectInfo    = m_ReassignmentInfo.GetObjectList().front();
            netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

            if(gnetVerifyf(networkObject, "Trying to reassign an invalid network object!"))
            {
                if(gnetVerifyf(objectInfo->IsConfirmed(), "Unconfirmed object reassignment: %s", networkObject->GetLogName()))
                {
                    if (objectInfo->GetOwnerPlayer() == netInterface::GetLocalPhysicalPlayerIndex() || netInterface::GetNumRemoteActivePlayers() == 0)
                    {
                        if (objectInfo->GetOwnerPlayer() != netInterface::GetLocalPhysicalPlayerIndex())
                        {
                            gnetAssertf(0, "%s is set to be reassigned to player %d but there are no players left!", networkObject->GetLogName(), objectInfo->GetOwnerPlayer());
                            objectInfo->SetOwnerPlayer(netInterface::GetLocalPhysicalPlayerIndex());
                        }

                        GetLog().WriteDataValue(networkObject->GetLogName(), "Reassigned to my player%s. Cloned state %d",
                                             objectInfo->IsSetToBeRemoved() ? " and removed" : "",
                                             objectInfo->GetPlayerFlags());
                    }
                    else
                    {
                        netPlayer *newOwner = netInterface::GetPhysicalPlayerFromIndex(objectInfo->GetOwnerPlayer());
                        GetLog().WriteDataValue(networkObject->GetLogName(), "Reassigned to %s%s.",
                                             newOwner ? newOwner->GetLogName() : "Unknown",
                                             objectInfo->IsSetToBeRemoved() ? " and removed" : "");
                    }                    
                }

                RemoveObjectFromReassignList(*objectInfo);
            }
        }

        m_ObjectMgr->OnReassignmentFinished();

        m_ReassignmentInfo.Finish();
    }
}

void netObjectReassignMgr::SendNegotiateMsg(const netPlayer *destinationPlayer)
{
    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Trying to send a negotiate message when a reassignment is not in progress!"))
    {
        u8 messageBuffer[MAX_MESSAGE_PAYLOAD_BYTES];
        datBitBuffer msgBuffer;
        msgBuffer.SetReadWriteBits(messageBuffer, MAX_MESSAGE_PAYLOAD_BITS, 0);

        CSyncDataWriter serialiser(msgBuffer);

        int numObjectsReassigned = 0;

        reassignmentProcessInfo::ObjectList::iterator       curr = m_ReassignmentInfo.GetObjectList().begin();
        reassignmentProcessInfo::ObjectList::const_iterator end  = m_ReassignmentInfo.GetObjectList().end();

        for(; curr != end; ++curr)
        {
            reassignObjectInfo *objectInfo = *curr;

            if(objectInfo && objectInfo->IsIncludedInNegotiation())
            {
                netObject *networkObject = objectInfo->GetObject();

                if(gnetVerifyf(networkObject, "Trying to negotiate for an invalid object!"))
                {
					ObjectId id = networkObject->GetObjectID();
					u32 priority = objectInfo->GetOurReassignPriority();

                    SERIALISE_OBJECTID(serialiser, id);					
                    SERIALISE_UNSIGNED(serialiser, priority, SIZEOF_REASSIGNPRIORITY);
                    numObjectsReassigned++;
                }
            }
        }

        unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *player = remotePhysicalPlayers[index];

            PhysicalPlayerIndex playerIndex = player->GetPhysicalPlayerIndex();

            if(m_ReassignmentInfo.IsPlayerInvolved(playerIndex) &&
              (!destinationPlayer || destinationPlayer == player))
            {
                m_ReassignmentInfo.SetWaitingOnAck(playerIndex);
                m_ReassignmentInfo.SetWaitingOnNegResponse(playerIndex, m_ReassignmentInfo.GetLastLeavingPeerID());

                u64     leavingPeers[MAX_NUM_PHYSICAL_PLAYERS];
                unsigned numLeavingPeers = 0;
                m_ReassignmentInfo.GetLeavingPeersToNegotiateFor(playerIndex, leavingPeers, numLeavingPeers);

                reassignNegotiateMsg negotiateMsg(numObjectsReassigned,
                                                  leavingPeers,
                                                  numLeavingPeers,
                                                  reinterpret_cast<u8 *>(msgBuffer.GetReadWriteBits()),
                                                  msgBuffer.GetNumBytesWritten());

                negotiateMsg.WriteToLogFile(false, *player);

                // we need to check this connection is open here, as this function can be called in response to 
                // a player leaving the game. It's possible for more than one player to leave in the same frame,
                // but the leave events are processed sequentially, and the connection for another leaving player may
                // already have been closed
                if(netInterface::IsConnectionOpen(player->GetConnectionId()))
                {
                    netInterface::SendRequest(player, negotiateMsg, 0, &m_ReassignmentInfo.GetResponseHandler(playerIndex));

                    BANK_ONLY(m_NumReassignNegotiateMessagesSent++);
                    BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_NegotiateMsgRecorderId, negotiateMsg.GetMessageDataBitSize()));

                    m_ObjectMgr->GetBandwidthMgr().AddBandwidthOut(*player, negotiateMsg.GetMessageDataBitSize());
                }

                m_PendingMessageInfo[playerIndex].m_TimeToSend  = 0;
                m_PendingMessageInfo[playerIndex].m_MessageType = NO_MESSAGE;
            }
        }
    }
}

void netObjectReassignMgr::SendConfirmMsg(const netPlayer *destinationPlayer)
{
    if(gnetVerifyf(m_ReassignmentInfo.IsActive(), "Trying to send a confirm message when a reassignment is not in progress!"))
    {
        datBitBuffer msgBuffer;
        u8 messageBuffer[MAX_MESSAGE_PAYLOAD_BYTES];
        msgBuffer.SetReadWriteBits(messageBuffer, MAX_MESSAGE_PAYLOAD_BITS, 0);

        unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *player = remotePhysicalPlayers[index];

            if (m_ReassignmentInfo.IsPlayerInvolved(player->GetPhysicalPlayerIndex()) &&
                (!destinationPlayer || destinationPlayer == player))
            {
                unsigned localObjects  = 0;
                unsigned remoteObjects = 0;

                msgBuffer.SetCursorPos(0);

                CSyncDataWriter serialiser(msgBuffer);

                // We send the player the objects we now own as well as the list of objects we think they own, together with the checksums
                // of their important states that we have. We need to send all of the objects we own as the other players may still have them 
                // but not included them in the negotiate message. This can happen if a player thinks an object was owned by an existing player rather than the one that left
                reassignmentProcessInfo::ObjectList::iterator       curr = m_ReassignmentInfo.GetObjectList().begin();
                reassignmentProcessInfo::ObjectList::const_iterator end  = m_ReassignmentInfo.GetObjectList().end();

                for(; curr != end; ++curr)
                {
                    reassignObjectInfo *objectInfo    = *curr;
                    netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

                    if(gnetVerifyf(networkObject, "Invalid object on the reassignment list!") &&
                                   objectInfo->GetOwnerPlayer() == netInterface::GetLocalPhysicalPlayerIndex())
                    {
                        SERIALISE_OBJECTID(serialiser, const_cast<ObjectId&>(networkObject->GetObjectID()));
                        localObjects++;
                    }
                }

                // serialise their objects
                curr = m_ReassignmentInfo.GetObjectList().begin();

                for(; curr != end; ++curr)
                {
                    reassignObjectInfo *objectInfo    = *curr;
                    netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

                    if(gnetVerifyf(networkObject, "Invalid object on the reassignment list!") &&
                                   objectInfo->GetOwnerPlayer() == player->GetPhysicalPlayerIndex())
                    {
                        SERIALISE_OBJECTID(serialiser, const_cast<ObjectId&>(networkObject->GetObjectID()));
                        remoteObjects++;

                        unsigned checkSum = GetImportantStateChecksum(*networkObject, player->GetPhysicalPlayerIndex());

						bool value = checkSum != 0;
                        SERIALISE_BOOL(serialiser, value);

                        if (checkSum != 0)
                        {
                            SERIALISE_UNSIGNED(serialiser, checkSum, sizeof(checkSum)<<3);
                        }
                    }
                }

                reassignConfirmMsg confirmMsg(localObjects,
                                              remoteObjects,
                                              m_ReassignmentInfo.GetLastLeavingPeerID(),
                                              (u8*)msgBuffer.GetReadWriteBits(),
                                              msgBuffer.GetNumBytesWritten());
                confirmMsg.WriteToLogFile(false, *player);

                m_ReassignmentInfo.SetWaitingOnAck(player->GetPhysicalPlayerIndex());

                netInterface::SendRequest(player, confirmMsg, 0, &m_ReassignmentInfo.GetResponseHandler(player->GetPhysicalPlayerIndex()));

                // record confirm message data out
                BANK_ONLY(m_NumReassignConfirmMessagesSent++);
                BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_ConfirmMsgRecorderId, confirmMsg.GetMessageDataBitSize()));

                m_ObjectMgr->GetBandwidthMgr().AddBandwidthOut(*player, confirmMsg.GetMessageDataBitSize());

                m_PendingMessageInfo[player->GetPhysicalPlayerIndex()].m_TimeToSend  = 0;
                m_PendingMessageInfo[player->GetPhysicalPlayerIndex()].m_MessageType = NO_MESSAGE;
            }
        }
    }
}

void netObjectReassignMgr::HandleReassignRequest(netPlayer *player, const netRequest *request)
{
    unsigned msgId = ~0u;
    netMessage::GetId(&msgId, request->m_Data, request->m_SizeofData);
    if(reassignNegotiateMsg::MSG_ID() == msgId)
    {
        u64 leavingPeers[MAX_NUM_PHYSICAL_PLAYERS];
        u8  messageBuffer[MAX_MESSAGE_PAYLOAD_BYTES];
        reassignNegotiateMsg negotiateMsg(leavingPeers, messageBuffer, sizeof(messageBuffer));

        if(gnetVerifyf(negotiateMsg.Import(request->m_Data, request->m_SizeofData), "Failed importing negotiation message!"))
        {
            negotiateMsg.WriteToLogFile(true, *player);
            HandleNegotiateMsg(negotiateMsg, *player, request);
        }
    }
    else if (reassignConfirmMsg::MSG_ID() == msgId)
    {
        u8 messageBuffer[MAX_MESSAGE_PAYLOAD_BYTES];
        reassignConfirmMsg confirmMsg(messageBuffer, sizeof(messageBuffer));

        if(gnetVerifyf(confirmMsg.Import(request->m_Data, request->m_SizeofData), "Failed importing confirmation message!"))
        {
            confirmMsg.WriteToLogFile(true, *player);
            HandleConfirmMsg(confirmMsg, *player, request);
        }
    }
}

void netObjectReassignMgr::HandleReassignResponse(netTransactor      *UNUSED_PARAM(transactor),
                                                  netResponseHandler *UNUSED_PARAM(handler),
                                                  const netResponse  *response)
{
    const int  connectionId  = response->m_TxInfo.m_CxnId;
    netPlayer *sendingPlayer = netInterface::GetPlayerFromConnectionId(connectionId);

    if(!sendingPlayer)
    {
        gnetDebug2("Received reassign response message from a connection without an associated player!");
        return;
    }

    reassignResponseMsg responseMsg;
    if(response->Answered() && gnetVerifyf(responseMsg.Import(response->m_Data, response->m_SizeofData), "Failed importing response message!"))
    {
        if(gnetVerifyf(sendingPlayer, "Received a message from an invalid player!"))
        {
            responseMsg.WriteToLogFile(true, *sendingPlayer);
            const char *headerText = responseMsg.IsNegotiateResponse() ? "GOT_NEGOTIATE_RESPONSE" : "GOT_CONFIRM_RESPONSE";
            NetworkLogUtils::WriteMessageHeader(GetLog(), true, 0, *sendingPlayer, headerText, "");
        }

        if(!m_ReassignmentInfo.IsActive())
        {
            GetLog().WriteDataValue("Warning", "Reassignment not in progress!");
        }

        const unsigned MESSAGE_TRY_INTERVAL = 1000;

        if(responseMsg.IsNegotiateResponse())
        {
            bool processedNegotiationData = responseMsg.HasProcessedNegotiationData();

            GetLog().WriteDataValue("PROCESSED DATA", "%s", processedNegotiationData ? "TRUE" : "FALSE");

            if(!processedNegotiationData)
            {
                if(responseMsg.IsConfirming())
                {
                    GetLog().WriteDataValue("REASON", "Player is in the confirmation state");
                }
            }

            unsigned numPeersNegotiatedFor = responseMsg.GetNumPeerNegotiationResponses();

            for(unsigned index = 0; index < numPeersNegotiatedFor; index++)
            {
                u64      peerID   = 0;
                unsigned response = RESPONSE_MAX_TYPES;
                responseMsg.GetPeerNegotiationResponseData(index, peerID, response);

                GetLog().WriteDataValue("Leaving peer", "%s", GetLeavingPeerName(peerID));
                GetLog().WriteDataValue("Leaving peer ID", "%llx", peerID);

                bool stopNegotiatingForPeer = false;

                switch(response)
                {
                case RESPONSE_NOT_ON_THIS_MACHINE:
                    {
                        GetLog().WriteDataValue("NOT_ON_THIS_MACHINE", "Peer is not on remote machine!");
                        stopNegotiatingForPeer = true;

                        m_ReassignmentInfo.SetHasPeer(sendingPlayer->GetPhysicalPlayerIndex(), peerID, false);
                    }
                    break;
                case RESPONSE_NOT_READY:
                    {
                        GetLog().WriteDataValue("NOT_READY", "Player still in the remote session!");
                    }
                    break;
                case RESPONSE_PROCESSED_ALREADY:
                    {
                        GetLog().WriteDataValue("PROCESSED_ALREADY", "Remote player already negotiated this peer with us!");
                        stopNegotiatingForPeer = true;
                    }
                    break;
                case RESPONSE_PROCESSED:
                    {
                        GetLog().WriteDataValue("PROCESSED", "Remote player can negotiate for this peer");
                        stopNegotiatingForPeer = processedNegotiationData;
                    }
                    break;
                }

                if(stopNegotiatingForPeer)
                {
                    m_ReassignmentInfo.ClearWaitingOnNegResponse(sendingPlayer->GetPhysicalPlayerIndex(), peerID);
                }
            }

            if(m_ReassignmentInfo.IsActive())
            {
                // This player doesn't know about any of the peers that have left -
                // They must have joined the session after these peers left the session
                if(!m_ReassignmentInfo.HasAnyPeer(sendingPlayer->GetPhysicalPlayerIndex()))
                {
                    if(m_ReassignmentInfo.IsWaitingOnAck(sendingPlayer->GetPhysicalPlayerIndex()))
                    {
                        m_ReassignmentInfo.ClearPlayerInvolved(sendingPlayer->GetPhysicalPlayerIndex());
                    }
                }
                // This player has processed the negotiation data successfully
                else if(processedNegotiationData)
                {
                    m_ReassignmentInfo.SetGotAck(sendingPlayer->GetPhysicalPlayerIndex());
                }
                else
                {
                    // the remote player didn't process the negotiation data - we need to retry
                    m_PendingMessageInfo[sendingPlayer->GetPhysicalPlayerIndex()].m_TimeToSend  = sysTimer::GetSystemMsTime() + MESSAGE_TRY_INTERVAL;
                    m_PendingMessageInfo[sendingPlayer->GetPhysicalPlayerIndex()].m_MessageType = NEGOTIATE_MESSAGE;
                }
            }
        }
        else
        {
            switch (responseMsg.m_ResponseID)
            {
            case RESPONSE_NOT_READY:
                {
                    GetLog().WriteDataValue("Response", "NOT_READY");

                    // if the remote player was not ready we need to resend the message until they are ready
                    if (sendingPlayer && m_ReassignmentInfo.IsActive() && m_ReassignmentInfo.IsWaitingOnAck(sendingPlayer->GetPhysicalPlayerIndex()))
                    {
                        // the remote player didn't process the confirm data - we need to retry
                        m_PendingMessageInfo[sendingPlayer->GetPhysicalPlayerIndex()].m_TimeToSend  = sysTimer::GetSystemMsTime() + MESSAGE_TRY_INTERVAL;
                        m_PendingMessageInfo[sendingPlayer->GetPhysicalPlayerIndex()].m_MessageType = CONFIRM_MESSAGE;
                    }
                }
                break;
            case RESPONSE_NOT_REASSIGNING:
                {
                    GetLog().WriteDataValue("Response", "NOT_REASSIGNING");

                    // this player is not part of the reassignment process as it has just joined the game
                    if (sendingPlayer && m_ReassignmentInfo.IsActive() && m_ReassignmentInfo.IsWaitingOnAck(sendingPlayer->GetPhysicalPlayerIndex()))
                    {
                        m_ReassignmentInfo.ClearPlayerInvolved(sendingPlayer->GetPhysicalPlayerIndex());
                    }
                }
                break;
            case RESPONSE_PROCESSED:
                {
                    GetLog().WriteDataValue("Response", "PROCESSED");

                    if (m_ReassignmentInfo.IsActive())
                    {
                        m_ReassignmentInfo.SetGotAck(sendingPlayer->GetPhysicalPlayerIndex());
                    }
                }
                break;
            case RESPONSE_PROCESSED_ALREADY:
            case RESPONSE_FINISHED:
                {
                    if(responseMsg.m_ResponseID == RESPONSE_PROCESSED_ALREADY)
                    {
                        GetLog().WriteDataValue("Response", "PROCESSED_ALREADY");
                    }
                    else if(responseMsg.m_ResponseID == RESPONSE_FINISHED)
                    {
                        GetLog().WriteDataValue("Response", "FINISHED");
                    }

                    if (m_ReassignmentInfo.IsActive())
                    {
                        m_ReassignmentInfo.SetGotAck(sendingPlayer->GetPhysicalPlayerIndex());
                    }
                }
                break;
            default:
                gnetAssertf(0, "Received unexpected response!");
            }
        }

        // record response message in
        BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_ResponseMsgRecorderId, responseMsg.GetMessageDataBitSize()));

        LOGGING_ONLY(m_ReassignmentInfo.LogReassignmentState(GetLog()));
    }
    else if (!response->Received())
    {
        if(gnetVerifyf(sendingPlayer, "Received a message from an invalid player!"))
        {
            NetworkLogUtils::WriteMessageHeader(GetLog(), true, 0, *sendingPlayer, "GOT_RESPONSE", "");
        }

        if(response->TimedOut())
        {
            GetLog().WriteDataValue("Response", "TIMED OUT");
        }
        else if(response->Failed())
        {
            GetLog().WriteDataValue("Response", "FAILED");
        }
        else if(response->Canceled())
        {
            GetLog().WriteDataValue("Response", "CANCELED");
        }
        else
        {
            gnetAssertf(0, "Unexpected object reassignment response failure!");
        }

        // we need to reset any of our waiting flags here, otherwise the object reassignment process with get stuck
        if (m_ReassignmentInfo.IsActive())
        {
            if(gnetVerifyf(sendingPlayer, "Received a reassignment response from an invalid player!"))
            {
                if(m_ReassignmentInfo.IsWaitingOnAck(sendingPlayer->GetPhysicalPlayerIndex()))
                {
                    m_ReassignmentInfo.SetGotAck(sendingPlayer->GetPhysicalPlayerIndex());
                }
            }
        }
    }
}

void netObjectReassignMgr::HandleNegotiateMsg(reassignNegotiateMsg& negotiateMsg, const netPlayer &player, const netRequest *request)
{
    bool canProcessNegotiationData = false;

    unsigned numLeavingPeers = 0;
    u64 *leavingPeers = negotiateMsg.GetLeavingPeersToNegotiateFor(numLeavingPeers);

    reassignResponseMsg responseMsg(reassignResponseMsg::NEGOTIATION_RESPONSE);

    bool notReady = false;

    for(unsigned index = 0; index < numLeavingPeers; index++)
    {
        u64 leavingPeerID = leavingPeers[index];

        GetLog().WriteDataValue("Leaving peer", "%s", GetLeavingPeerName(leavingPeerID));
        GetLog().WriteDataValue("Leaving peer ID", "%llx", leavingPeerID);

        if(!m_ReassignmentInfo.IsReassigningForPeerID(leavingPeerID))
        {
            if(!netInterface::GetPlayerMgr().GetPlayerFromPeerId(leavingPeerID) BANK_ONLY(|| sm_RejectIgnoreLeavingPlayers))
            {
                if(m_ReassignmentInfo.IsActive())
                {
                    GetLog().WriteDataValue("ACCEPTED", "This peer is not on this machine but we are already reassigning! Restarting reassignment and accepting data.");
                    responseMsg.AddPeerNegotiationResponse(leavingPeerID, RESPONSE_PROCESSED);

                    RestartReassignNegotiation(leavingPeerID, "NOT_ON_THIS_MACHINE");
                    PrepareForNegotiation(0);

                    canProcessNegotiationData = true;
                }
                else
                {
                    GetLog().WriteDataValue("NOT_ON_THIS_MACHINE", "This peer is not on this machine!");

                    responseMsg.AddPeerNegotiationResponse(leavingPeerID, RESPONSE_NOT_ON_THIS_MACHINE);
                }
            }
            else
            {
                GetLog().WriteDataValue("NOT_READY", "This message is for a player that is still in our session!");

                responseMsg.AddPeerNegotiationResponse(leavingPeerID, RESPONSE_NOT_READY);

                notReady                  = true;
                canProcessNegotiationData = false;
            }
        }
        else if(m_ReassignmentInfo.HasProcessedNegotiationData(player.GetPhysicalPlayerIndex(), leavingPeerID))
        {
            GetLog().WriteDataValue("PROCESSED", "We have already got the reassign msg");

            responseMsg.AddPeerNegotiationResponse(leavingPeerID, RESPONSE_PROCESSED_ALREADY);
        }
#if __BANK
        else if(sm_RejectAllNegotiationMessages)
        {
            GetLog().WriteDataValue("NOT_READY", "Rejecting all negotiation messages (DEBUG ONLY)!");

            responseMsg.AddPeerNegotiationResponse(leavingPeerID, RESPONSE_NOT_READY);

            notReady                  = true;
            canProcessNegotiationData = false;
        }
#endif // __BANK
        else
        {
            GetLog().WriteDataValue("ACCEPTED", "We can negotiate objects for this peer");

            responseMsg.AddPeerNegotiationResponse(leavingPeerID, RESPONSE_PROCESSED);

            if(!notReady)
            {
                canProcessNegotiationData = true;
            }
        }
    }

    if(m_ReassignmentInfo.IsConfirming())
    {
        GetLog().WriteDataValue("FINISHED", "Negotiating finished");

        responseMsg.SetConfirming();
    }
    else
    {
        GetLog().WriteDataValue("CAN PROCESS DATA", "%s", canProcessNegotiationData ? "TRUE" : "FALSE");

        if(canProcessNegotiationData)
        {
            ProcessNegotiationData(negotiateMsg, player);

            responseMsg.SetHasProcessedNegotiationData();

            for(unsigned index = 0; index < numLeavingPeers; index++)
            {
                u64 leavingPeerID = leavingPeers[index];

                m_ReassignmentInfo.MarkProcessedNegotiationData(player.GetPhysicalPlayerIndex(), leavingPeerID);

                // handle the case where we receive negotiation data from a player we don't
                // think is involved in the process
                if(!m_ReassignmentInfo.IsPlayerInvolved(player.GetPhysicalPlayerIndex()))
                {
                    m_ReassignmentInfo.SetPlayerInvolved(player.GetPhysicalPlayerIndex());
                    SendNegotiateMsg(&player);
                }
            }
        }
    }

    responseMsg.WriteToLogFile(false, player);

    netInterface::SendResponse(request->m_TxInfo, responseMsg);

    // record negotiate message data in
    BANK_ONLY(m_NumReassignResponseMessagesSent++);
    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_NegotiateMsgRecorderId, negotiateMsg.GetMessageDataBitSize()));

    // record response message out
    BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_ResponseMsgRecorderId, responseMsg.GetMessageDataBitSize()));
    m_ObjectMgr->GetBandwidthMgr().AddBandwidthOut(player, responseMsg.GetMessageDataBitSize());

    LOGGING_ONLY(m_ReassignmentInfo.LogReassignmentState(GetLog()));
}

void netObjectReassignMgr::NegotiateForKnownObject(reassignObjectInfo &objectInfo, unsigned remoteReassignPriority, const netPlayer &remotePlayer)
{
    objectInfo.SetPlayerHasObject(remotePlayer.GetPhysicalPlayerIndex());

    PhysicalPlayerIndex oldPlayerOwner      = objectInfo.GetOwnerPlayer();
    unsigned            oldReassignPriority = objectInfo.GetCurrentReassignPriority();
    objectInfo.DecideOwnership(remotePlayer, remoteReassignPriority);

    netObject *networkObject = objectInfo.GetObject();

    if (gnetVerifyf(networkObject, "Trying to negotiate for an invalid object!"))
    {
        netPlayer *oldOwner = netInterface::GetPhysicalPlayerFromIndex(oldPlayerOwner);

        if (objectInfo.GetOwnerPlayer() == remotePlayer.GetPhysicalPlayerIndex())
        {
            if (oldReassignPriority == remoteReassignPriority)
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Priority %d ( == ). %s takes ownership from %s",
                                     remoteReassignPriority,
                                     remotePlayer.GetLogName(),
                                     oldOwner ? oldOwner->GetLogName() : "Invalid");
            }
            else
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Priority %d ( > %d). %s takes ownership from %s",
                                     remoteReassignPriority,
                                     oldReassignPriority,
                                     remotePlayer.GetLogName(),
                                     oldOwner ? oldOwner->GetLogName() : "Invalid");
            }
        }
        else
        {
            if (oldReassignPriority == remoteReassignPriority)
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Priority %d ( == ). %s keeps ownership from %s",
                                     remoteReassignPriority,
                                     oldOwner ? oldOwner->GetLogName() : "Invalid",
                                     remotePlayer.GetLogName());
            }
            else
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Priority %d ( < %d ). %s keeps ownership from %s",
                                     remoteReassignPriority,
                                     oldReassignPriority,
                                     oldOwner ? oldOwner->GetLogName() : "Invalid",
                                     remotePlayer.GetLogName());
            }
        }
    }
}

void netObjectReassignMgr::NegotiateForUnknownObject(ObjectId objectID, unsigned remoteReassignPriority, const netPlayer &remotePlayer)
{
    // if the object exists but is not on the reassign list then it needs to be added to the reassign process
    // Or it may exist on another reassign list, in which case it is added to this list as well
    netObject *networkObject = m_ObjectMgr->GetNetworkObject(objectID, true);

    if (networkObject == 0)
    {
        char logName[100];
        sprintf(logName, "Object_%d", objectID);
        GetLog().WriteDataValue(logName, "Not on this machine");

        if(m_ObjectMgr->GetObjectIDManager().RemoveObjectIDIfExists(objectID))
        {
            GetLog().WriteDataValue(logName, "Object ID was on free queue!");
        }
    }
    else
    {
        if (gnetVerifyf(networkObject->GetPlayerOwner(), "An object that is not on the reassignment list has an invalid player owner!"))
        {
            if (networkObject->IsClone())
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Owned by %s! Added to reassign list.", networkObject->GetPlayerOwner()->GetLogName());
            }
            else if (networkObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Owned locally and unregistering. Destroyed.");
            }
            else
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Owned locally. Added to reassign list.");

                m_ObjectMgr->RemoveObjectFromSyncMessageInfoQueues(objectID);
            }

            if (!networkObject->IsClone() && networkObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
            {
                // remove the object locally without freeing the ID, as it is in use on a remote machine.
                // as we did not include this object in our negotiated objects lists when the object is
                // reassigned it will not be marked as cloned on this machine, and will be cloned if we are in scope
                networkObject->SetPendingPlayerIndex(remotePlayer.GetPhysicalPlayerIndex());
                m_ObjectMgr->ForceRemovalOfUnregisteringObject(networkObject);
            }
            else
            {
                // add the object to the reassign list
                reassignObjectInfo *newInfo = AddObjectToReassignList(*networkObject, remoteReassignPriority, remotePlayer);

                if(newInfo)
                {
                    newInfo->SetPlayerHasObject(remotePlayer.GetPhysicalPlayerIndex());

                    // don't include this object when we negotiate for objects from now on, as we may have already
                    // negotiated with some of the players involved in the process so this would lead to inconsistent results
                    newInfo->SetIncludedInNegotiation(false);
                }
            }
        }
    }
}

void netObjectReassignMgr::ProcessNegotiationData(reassignNegotiateMsg &negotiateMsg, const netPlayer &player)
{
    gnetAssertf(m_ReassignmentInfo.IsActive(), "Trying to process negotiation data when an object reassignment is not in progress!");

    if(m_ReassignmentInfo.IsWaitingOnMsg(player.GetPhysicalPlayerIndex()))
    {
        m_ReassignmentInfo.SetGotMsg(player.GetPhysicalPlayerIndex());
    }

    datBitBuffer msgBuffer;
    msgBuffer.SetReadOnlyBits(negotiateMsg.m_MessageBuffer, negotiateMsg.m_ByteSizeofBuffer<<3, 0);
    CSyncDataReader serialiser(msgBuffer);

    for (unsigned objectIndex = 0; objectIndex < negotiateMsg.m_NumObjects; objectIndex++)
    {
        ObjectId objectID         = NETWORK_INVALID_OBJECT_ID;
        unsigned reassignPriority = 0;
        SERIALISE_OBJECTID(serialiser, objectID);
        SERIALISE_UNSIGNED(serialiser, reassignPriority, SIZEOF_REASSIGNPRIORITY);

        // find the object on the reassign list
        reassignObjectInfo *objectInfo = m_ReassignmentInfo.FindInfo(objectID);

        if(objectInfo)
        {
            NegotiateForKnownObject(*objectInfo, reassignPriority, player);
        }
        else
        {
            NegotiateForUnknownObject(objectID, reassignPriority, player);
        }
    }
}

void netObjectReassignMgr::HandleConfirmMsg(reassignConfirmMsg &confirmMsg, const netPlayer &player, const netRequest *request)
{
    ReassignMsgResponseCode response = RESPONSE_PROCESSED;

    if(!m_ReassignmentInfo.IsActive())
    {
        GetLog().WriteDataValue("NOT_REASSIGNING", "We are not reassigning objects");
        response = RESPONSE_NOT_REASSIGNING;
    }
    else if(m_ReassignmentInfo.IsWaitingOnNegResponse(m_ReassignmentInfo.GetLastLeavingPeerID()))
    {
        GetLog().WriteDataValue("NOT_READY", "We are still waiting on a negotiation response!");

        response = RESPONSE_NOT_READY;
    }
    else if (m_ReassignmentInfo.IsNegotiating())
    {
        GetLog().WriteDataValue("NOT_READY", "We are still negotiating!");

        response = RESPONSE_NOT_READY;
    }
    else if (!m_ReassignmentInfo.IsConfirming())
    {
        GetLog().WriteDataValue("FINISHED", "Confirmation finished");
        response = RESPONSE_FINISHED;
    }
#if __BANK
    else if (sm_RejectAllConfirmationMessages)
    {
        GetLog().WriteDataValue("NOT_READY", "Rejecting all confirmation messages (DEBUG ONLY)!");

        response = RESPONSE_NOT_READY;
    }
#endif // __BANK
    else if (!m_ReassignmentInfo.IsWaitingOnMsg(player.GetPhysicalPlayerIndex()))
    {
        GetLog().WriteDataValue("PROCESSED", "We have already got the confirm msg");
        response = RESPONSE_PROCESSED_ALREADY;
    }

    if (response == RESPONSE_PROCESSED)
    {
        ProcessConfirmationData(confirmMsg, player);
    }

    reassignResponseMsg responseMsg(reassignResponseMsg::CONFIRM_RESPONSE, confirmMsg.m_LeavingPeerID, response);
    responseMsg.WriteToLogFile(false, player);

    netInterface::SendResponse(request->m_TxInfo, responseMsg);

    // record confirm message data in
    BANK_ONLY(m_NumReassignResponseMessagesSent++);
    BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_ConfirmMsgRecorderId, confirmMsg.GetMessageDataBitSize()));

    // record response message out
    BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_ResponseMsgRecorderId, responseMsg.GetMessageDataBitSize()));
    m_ObjectMgr->GetBandwidthMgr().AddBandwidthOut(player, responseMsg.GetMessageDataBitSize());

    LOGGING_ONLY(m_ReassignmentInfo.LogReassignmentState(GetLog()));
}

bool netObjectReassignMgr::ConfirmRemoteObjectWithRemotePlayer(ObjectId objectID, const netPlayer &remotePlayer)
{
    bool                success    = true;
    reassignObjectInfo *objectInfo = m_ReassignmentInfo.FindInfo(objectID);

    if(objectInfo == 0)
    {
        char logName[100];
        sprintf(logName, "Object_%d", objectID);
        GetLog().WriteDataValue(logName, "Not on this machine.");
    }
    else
    {
        objectInfo->SetPlayerHasObject(remotePlayer.GetPhysicalPlayerIndex());

        netObject *networkObject = objectInfo->GetObject();

        if (gnetVerifyf(networkObject, "Trying to confirm for an invalid object!"))
        {
            if (objectInfo->GetOwnerPlayer() == remotePlayer.GetPhysicalPlayerIndex())
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Confirmed");
                objectInfo->SetConfirmed();
            }
            else
            {
                netPlayer *ownerPlayer = netInterface::GetPhysicalPlayerFromIndex(objectInfo->GetOwnerPlayer());

                if (ownerPlayer && ownerPlayer->IsMyPlayer())
                {
                    GetLog().WriteDataValue(networkObject->GetLogName(), "** Error: We think this object is being reassigned to our player! **");
                    success = false;
                }
                else
                {
                    // this case can happen when an object create arrives for on our machine for an object being reassigned - we can miss it from the
                    // negotiate msg and then end up reassigning it to the wrong player. This confirm msg will be correct though.
                    if (ownerPlayer)
                    {
                        GetLog().WriteDataValue(networkObject->GetLogName(),
                                             "We think this object is being reassigned to %s! Let this player take ownership",
                                             remotePlayer.GetLogName());
                    }
                    else
                    {
                        GetLog().WriteDataValue(networkObject->GetLogName(),
                                             "We think this object is being reassigned to non-existent player %d! Let this player take ownership",
                                             objectInfo->GetOwnerPlayer());
                    }

                    objectInfo->SetOwnerPlayer(remotePlayer.GetPhysicalPlayerIndex());
                    objectInfo->SetConfirmed();
                }
            }
        }
    }

    return success;
}

bool netObjectReassignMgr::ConfirmLocalObjectWithRemotePlayer(ObjectId objectID, unsigned checksum, const netPlayer &remotePlayer)
{
    bool                success       = true;
    reassignObjectInfo *objectInfo    = m_ReassignmentInfo.FindInfo(objectID);
    netObject          *networkObject = objectInfo ? objectInfo->GetObject() : 0;

    gnetAssertf(!objectInfo || networkObject, "Invalid object data is on the reassignment list!");

    if(networkObject == 0)
    {
        GetLog().WriteDataValue("ERROR", "** error - this object does not exist! **");
        success = false;
    }
    else
    {
        objectInfo->SetPlayerHasObject(remotePlayer.GetPhysicalPlayerIndex());

        if (objectInfo->GetOwnerPlayer() != netInterface::GetLocalPhysicalPlayerIndex())
        {
            netPlayer *ownerPlayer = netInterface::GetPhysicalPlayerFromIndex(objectInfo->GetOwnerPlayer());

            if (ownerPlayer)
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "**error - we think this object is being reassigned to %s! **", ownerPlayer->GetLogName());
            }
            else
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "**error - we think this object is being reassigned to non-existent player %d! **", objectInfo->GetOwnerPlayer());
            }

            success = false;
        }
        else
        {
            unsigned localChecksum = GetImportantStateChecksum(*networkObject, remotePlayer.GetPhysicalPlayerIndex());

            if (localChecksum != checksum && checksum != 0)
            {
                objectInfo->SetPlayerNeedsAnUpdate(remotePlayer.GetPhysicalPlayerIndex());
                GetLog().WriteDataValue(networkObject->GetLogName(), "Checksums differ");
            }
            else
            {
                GetLog().WriteDataValue(networkObject->GetLogName(), "Checksums match");
            }

            objectInfo->SetConfirmed();
        }
    }

    return success;
}

void netObjectReassignMgr::ProcessConfirmationData(reassignConfirmMsg &confirmMsg, const netPlayer &player)
{
    if (confirmMsg.m_NumLocalObjects > 0)
    {
        NetworkLogUtils::WriteLogEvent(GetLog(), "CONFIRMED_OBJECTS", player.GetLogName());
    }

    gnetAssertf(m_ReassignmentInfo.IsActive(), "Processing confirmation data when an object reassignment is not in progress!");
    gnetAssertf(m_ReassignmentInfo.IsWaitingOnMsg(player.GetPhysicalPlayerIndex()), "Processing confirmation data from an unexpected player!");

    m_ReassignmentInfo.SetGotMsg(player.GetPhysicalPlayerIndex());

    datBitBuffer msgBuffer;
    msgBuffer.SetReadOnlyBits(confirmMsg.m_MessageBuffer, confirmMsg.m_ByteSizeofBuffer<<3, 0);
    CSyncDataReader serialiser(msgBuffer);

    bool confirmationSucceeded = true;

    for (unsigned objectIndex = 0; objectIndex < confirmMsg.m_NumLocalObjects; objectIndex++)
    {
        ObjectId objectID = NETWORK_INVALID_OBJECT_ID;
        SERIALISE_OBJECTID(serialiser, objectID);

        confirmationSucceeded &= ConfirmRemoteObjectWithRemotePlayer(objectID, player);
    }

    if (confirmMsg.m_NumRemoteObjects > 0)
    {
        NetworkLogUtils::WriteLogEvent(GetLog(), "CONFIRMED_OBJECTS", netInterface::GetLocalPlayer()->GetLogName());
    }

    for (unsigned objectIndex = 0; objectIndex < confirmMsg.m_NumRemoteObjects; objectIndex++)
    {
        ObjectId objectID    = NETWORK_INVALID_OBJECT_ID;
        bool     hasChecksum = false;
        unsigned checksum    = 0;
        SERIALISE_OBJECTID(serialiser, objectID);
        SERIALISE_BOOL(serialiser, hasChecksum);

        if (hasChecksum)
        {
            SERIALISE_UNSIGNED(serialiser, checksum, sizeof(checksum)<<3);
        }

        confirmationSucceeded &= ConfirmLocalObjectWithRemotePlayer(objectID, checksum, player);
    }

    gnetAssertf(confirmationSucceeded, "Object reassignment failed");
}

unsigned netObjectReassignMgr::GetImportantStateChecksum(netObject &networkObject, PhysicalPlayerIndex playerIndex) const
{
    unsigned checksum = 0;

    if (networkObject.HasGameObject())
    {
        u8 tempBuffer[MAX_MESSAGE_PAYLOAD_BYTES];
        datBitBuffer msgBuffer;
        msgBuffer.SetReadWriteBits(tempBuffer, MAX_MESSAGE_PAYLOAD_BITS, 0);

        // write the data for all critical nodes
        ActivationFlags actFlags = networkObject.GetActivationFlags();
        networkObject.GetSyncTree()->Write(SERIALISEMODE_CHECKSUM, actFlags, &networkObject, msgBuffer, 0, NULL, playerIndex);

        // pad the bits to a byte boundary with 0 before performing the checksum
        unsigned bitsToPad = 8 - (msgBuffer.GetCursorPos() % 8);

        if(bitsToPad > 0)
        {
            msgBuffer.WriteInt(0, bitsToPad);
        }

        // calculate the checksum using the sync data
        const char  *buffer     = reinterpret_cast<const char *>(msgBuffer.GetReadWriteBits());
        const unsigned bufferSize = (msgBuffer.GetCursorPos() >> 3);

        checksum = atStringHash(buffer, bufferSize);
    }

    return checksum;
}

} // namespace rage
