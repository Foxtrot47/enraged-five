//
// netobjectmessages.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwnet/netobjectmessages.h"

#include "fwnet/netlogsplitter.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netobject.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netobjectreassignmgr.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

NET_MESSAGE_IMPL( cloneSyncMsg );
NET_MESSAGE_IMPL( packedCloneSyncACKsMsg );
NET_MESSAGE_IMPL( packedReliablesMsg );
NET_MESSAGE_IMPL( reassignNegotiateMsg );
NET_MESSAGE_IMPL( reassignConfirmMsg );
NET_MESSAGE_IMPL( reassignResponseMsg );

///////////////////////////////////////////////////////////////////////////////////////
// packedCloneSyncACKsMsg
///////////////////////////////////////////////////////////////////////////////////////
packedCloneSyncACKsMsg::packedCloneSyncACKsMsg() :
m_OwnerPlayer(0)
, m_CurrentSyncAckSequence(INVALID_SEQUENCE_ID)
, m_NumSequencesAcked(0)
, m_NumSyncAcks(0)
, m_HasDataToSend(false)
{
    m_BitBuffer.SetReadWriteBytes(m_Buffer, sizeof(m_Buffer));

    for(unsigned index = 0; index < MAX_STORED_ACKS; index++)
    {
        m_StoredAcks[index].m_ObjectID = NETWORK_INVALID_OBJECT_ID;
        m_StoredAcks[index].m_AckCode  = ACKCODE_NONE;
    }

#if ENABLE_NETWORK_LOGGING
    for(unsigned index = 0; index < MAX_NUM_SYNC_SEQUENCES; index++)
    {
        m_SequenceAcked[index] = INVALID_SEQUENCE_ID;
        m_NumFailedAcks[index] = 0;
    }
#endif // ENABLE_NETWORK_LOGGING
}

bool packedCloneSyncACKsMsg::AddSyncAcks(const netPlayer &fromPlayer,
                                         netSequence      syncSequence,
                                         unsigned         numObjects,
                                         unsigned         numFailedACKs,
                                         ObjectId         objectIDs[MAX_SYNC_ACKS],
                                         NetObjectAckCode ackCodes[MAX_SYNC_ACKS])
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    // first check if we have space for all of the ACK codes for this sequence - it's
    // important these go in the same message otherwise they won't be processed correctly
    // on the remote machine
    if(!IsSpaceAvailableForAcks(numFailedACKs))
    {
        return false;
    }

    // if the sequence has changed, we need to update the count of sync acks
    // for the previous sequence
    if(syncSequence != m_CurrentSyncAckSequence)
    {
        if(m_NumSequencesAcked >= (MAX_NUM_SYNC_SEQUENCES - 1))
        {
            gnetAssertf(0, "No space for new ACK sequence! This should have already been checked!");
            return false;
        }

        WritePendingSyncAcksToBitBuffer(syncSequence);
    }

    if(m_OwnerPlayer == &fromPlayer)
    {
        for(unsigned index = 0; index < numObjects; index++)
        {
            ObjectId         objectID = objectIDs[index];
            NetObjectAckCode ackCode  = ackCodes[index];

            if (ackCode == ACKCODE_NONE)
            {
                success = true;
            }
            else if(ackCode == ACKCODE_OUT_OF_SEQUENCE)
            {
                gnetAssertf((m_NumSyncAcks == 0) || (m_NumSyncAcks == 1 && (m_StoredAcks[0].m_AckCode == ACKCODE_OUT_OF_SEQUENCE)), "Adding an out of sequence ack when other acks for this sequence has already been added!");

                m_StoredAcks[0].m_AckCode = ACKCODE_OUT_OF_SEQUENCE;
                m_NumSyncAcks = 1;

                success = true;
            }
            else
            {
                if(m_NumSyncAcks >= MAX_STORED_ACKS)
                {
                    if(!gnetVerifyf(m_NumSequencesAcked < (MAX_NUM_SYNC_SEQUENCES - 1), "Unexpectedly no space available to pack ACKs!"))
                    {
                        return false;
                    }

                    WritePendingSyncAcksToBitBuffer(syncSequence);
                }

                // we've already checked for space for all of the acks so this should never fail
                if(gnetVerifyf(IsSpaceAvailableForAck(), "Unexpectedly no space available to pack ACK!"))
                {
                    if(gnetVerifyf(m_NumSyncAcks < MAX_STORED_ACKS, "No space to pack ACK!"))
                    {
                        m_StoredAcks[m_NumSyncAcks].m_ObjectID = objectID;
                        m_StoredAcks[m_NumSyncAcks].m_AckCode  = static_cast<u8>(ackCode);

                        m_NumSyncAcks++;
                        success = true;
                    }
                }
            }
        }
    }

    if(success)
    {
        m_HasDataToSend = true;
    }

    return success;
}

unsigned packedCloneSyncACKsMsg::GetSyncAckSize()
{
    return (SIZEOF_OBJECTID + SIZEOF_ACKCODE);
}

unsigned packedCloneSyncACKsMsg::GetMessageHeaderBitSize() const
{
    int size = netMessage::GetHeaderSize(MSG_ID());

    return size;
}

unsigned packedCloneSyncACKsMsg::GetMessageDataBitSize() const
{
    int size = 0;

    size += m_BitBuffer.GetCursorPos();

    return size;
}

bool packedCloneSyncACKsMsg::HasDataToSend() const
{
    return m_HasDataToSend;
}

void packedCloneSyncACKsMsg::Reset()
{
    m_OwnerPlayer            = 0;
    m_CurrentSyncAckSequence = 0;
    m_NumSequencesAcked      = 0;
    m_NumSyncAcks            = 0;
    m_HasDataToSend          = false;

    m_BitBuffer.SetCursorPos(0);
}

void packedCloneSyncACKsMsg::WritePendingSyncAcksToBitBuffer(netSequence newSequence)
{
    // we don't need to do this for the first message
    if(m_CurrentSyncAckSequence != INVALID_SEQUENCE_ID)
    {
        if(gnetVerifyf(m_NumSequencesAcked < MAX_NUM_SYNC_SEQUENCES, "Too many sequences are being ACKed!"))
        {
#if ENABLE_NETWORK_LOGGING
            m_SequenceAcked[m_NumSequencesAcked] = m_CurrentSyncAckSequence;
            m_NumFailedAcks[m_NumSequencesAcked] = m_NumSyncAcks;
#endif // ENABLE_NETWORK_LOGGING
            m_NumSequencesAcked++;
        }

        CSyncDataWriter serialiser(m_BitBuffer);

        SERIALISE_UNSIGNED(serialiser, m_CurrentSyncAckSequence, SIZEOF_SYNC_ACK_SEQUENCE);

        bool hasAcks = m_NumSyncAcks > 0;
        SERIALISE_BOOL(serialiser, hasAcks);

        if(hasAcks)
        {
            bool outOfOrder = m_StoredAcks[0].m_AckCode == ACKCODE_OUT_OF_SEQUENCE;
            SERIALISE_BOOL(serialiser, outOfOrder);

            if(!outOfOrder)
            {
                SERIALISE_UNSIGNED(serialiser, m_NumSyncAcks, SIZEOF_NUM_SYNC_ACKS);

                for(unsigned index = 0; index < m_NumSyncAcks; index++)
                {
                    SERIALISE_OBJECTID(serialiser, m_StoredAcks[index].m_ObjectID);
                    SERIALISE_UNSIGNED(serialiser, m_StoredAcks[index].m_AckCode, SIZEOF_ACKCODE);
                }
            }
        }
    }

    m_CurrentSyncAckSequence = newSequence;
    m_NumSyncAcks            = 0;
}

bool packedCloneSyncACKsMsg::IsSpaceAvailableForAck() const
{
    static const unsigned maxSizeOfPackedMessageData = BUFFER_BITSIZE;

    unsigned syncAckSize = GetSyncAckSize();
    unsigned bitsUsed    = 0;
    
    bitsUsed += m_BitBuffer.GetCursorPos();

    // check any pending sync ack data
    if(m_NumSyncAcks > 0)
    {
        bitsUsed += SIZEOF_SYNC_ACK_SEQUENCE;
        bitsUsed++; // has acks
        bitsUsed++; // out of order
        bitsUsed += syncAckSize * m_NumSyncAcks;
    }

    Assert(bitsUsed <= maxSizeOfPackedMessageData);

    return ((bitsUsed + syncAckSize) < maxSizeOfPackedMessageData);
}

bool packedCloneSyncACKsMsg::IsSpaceAvailableForAcks(unsigned numFailedACKs) const
{
    static const unsigned maxSizeOfPackedMessageData = BUFFER_BITSIZE;

    unsigned syncAckSize = GetSyncAckSize();
    unsigned bitsUsed    = 0;
    
    bitsUsed += m_BitBuffer.GetCursorPos();

    // check any pending sync ack data
    if(m_NumSyncAcks > 0)
    {
        bitsUsed += SIZEOF_SYNC_ACK_SEQUENCE;
        bitsUsed++; // has acks
        bitsUsed++; // out of order
        bitsUsed += syncAckSize * m_NumSyncAcks;
    }

    Assert(bitsUsed <= maxSizeOfPackedMessageData);

    // now we have to work out how many ack blocks will be used
    unsigned numAckBlocks = 1;
    
    if(numFailedACKs > 0)
    {
        numAckBlocks = (numFailedACKs + (MAX_STORED_ACKS-1)) / MAX_STORED_ACKS;
    }

    // ensure we have space for all of the ACK blocks in this message
    if((m_NumSequencesAcked + numAckBlocks) >= (MAX_NUM_SYNC_SEQUENCES - 1))
    {
        return false;
    }

    bitsUsed += (SIZEOF_SYNC_ACK_SEQUENCE * numAckBlocks);

    if(numFailedACKs > 0)
    {
        bitsUsed += numAckBlocks; // has acks
        bitsUsed += numAckBlocks; // out of order
        bitsUsed += (syncAckSize * numFailedACKs);
    }

    return ((bitsUsed + syncAckSize) < maxSizeOfPackedMessageData);
}

///////////////////////////////////////////////////////////////////////////////////////
// packedReliablesMsg
///////////////////////////////////////////////////////////////////////////////////////
packedReliablesMsg::packedReliablesMsg()
{
    Reset();
}

bool packedReliablesMsg::AddCreate(const netPlayer       &fromPlayer,
                                   NetworkObjectType      objectType,
                                   const ObjectId         objectID,
                                   NetObjFlags            flags,
                                   u8                    *createData,
                                   unsigned               createDataSizeInBits)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_Creates.m_MessageCount < (MAX_MESSAGES_PER_TYPE-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetCreateSize(createDataSizeInBits);

        Assert(bitsRequired > 0);

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_Creates.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<packedMessageType::SIZEOF_BUFFER_SIZE))
            {
                CSyncDataWriter serialiser(messageBuffer);

                SERIALISE_UNSIGNED(serialiser, objectType, SIZEOF_OBJECTTYPE);
                SERIALISE_OBJECTID(serialiser, const_cast<ObjectId&>(objectID));
                SERIALISE_UNSIGNED(serialiser, flags, SIZEOF_NETOBJ_GLOBALFLAGS);

                SERIALISE_UNSIGNED(serialiser, createDataSizeInBits, packedMessageType::SIZEOF_BUFFER_SIZE);

                Assert(createDataSizeInBits > 0);

                if(createDataSizeInBits > 0)
                {
                    SERIALISE_DATABLOCK(serialiser, createData, createDataSizeInBits);
                }

                m_Creates.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

bool packedReliablesMsg::AddCreateAck(const netPlayer &fromPlayer, const ObjectId objectID, NetObjectAckCode ackCode)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_CreateAcks.m_MessageCount < (MAX_MESSAGES_PER_TYPE-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetCreateAckSize();

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_CreateAcks.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<packedMessageType::SIZEOF_BUFFER_SIZE))
            {
                CSyncDataWriter serialiser(messageBuffer);

                SERIALISE_OBJECTID(serialiser, const_cast<ObjectId&>(objectID));
                SERIALISE_UNSIGNED(serialiser, reinterpret_cast<u32&>(ackCode),  SIZEOF_ACKCODE);

                m_CreateAcks.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

bool packedReliablesMsg::AddRemove(const netPlayer &fromPlayer, const ObjectId objectID, u32 ownershipToken)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_Removes.m_MessageCount < (MAX_MESSAGES_PER_TYPE-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetRemoveSize();

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_Removes.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<packedMessageType::SIZEOF_BUFFER_SIZE))
            {
                CSyncDataWriter serialiser(messageBuffer);

                SERIALISE_OBJECTID(serialiser, const_cast<ObjectId&>(objectID));
                SERIALISE_UNSIGNED(serialiser, ownershipToken, netObject::SIZEOF_OWNERSHIP_TOKEN);

                m_Removes.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

bool packedReliablesMsg::AddRemoveAck(const netPlayer &fromPlayer, const ObjectId objectID, NetObjectAckCode ackCode)
{
    bool success = false;

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = &fromPlayer;
    }

    if(m_RemoveAcks.m_MessageCount < (MAX_MESSAGES_PER_TYPE-1) && (m_OwnerPlayer == &fromPlayer))
    {
        unsigned bitsRequired = GetRemoveAckSize();

        if(IsSpaceAvailable(bitsRequired))
        {
            datBitBuffer &messageBuffer = m_RemoveAcks.m_MessageBuffer;

            if((messageBuffer.GetCursorPos() + bitsRequired) < (1<<packedMessageType::SIZEOF_BUFFER_SIZE))
            {
                CSyncDataWriter serialiser(messageBuffer);

                SERIALISE_OBJECTID(serialiser, const_cast<ObjectId&>(objectID));
                SERIALISE_UNSIGNED(serialiser, reinterpret_cast<u32&>(ackCode),  SIZEOF_ACKCODE);

                m_RemoveAcks.m_MessageCount++;
                success = true;
            }
        }
    }

    return success;
}

unsigned packedReliablesMsg::GetCreateSize(const unsigned createDataSizeInBits)
{
    return SIZEOF_OBJECTTYPE +
           SIZEOF_OBJECTID + 
           SIZEOF_NETOBJ_GLOBALFLAGS +
           packedMessageType::SIZEOF_BUFFER_SIZE +
           createDataSizeInBits;
}

unsigned packedReliablesMsg::GetCreateAckSize()
{
    return SIZEOF_OBJECTID + SIZEOF_ACKCODE;
}

unsigned packedReliablesMsg::GetRemoveSize()
{
    return SIZEOF_OBJECTID + netObject::SIZEOF_OWNERSHIP_TOKEN;
}

unsigned packedReliablesMsg::GetRemoveAckSize()
{
    return SIZEOF_OBJECTID + SIZEOF_ACKCODE;
}

unsigned packedReliablesMsg::GetMessageHeaderBitSize() const
{
    unsigned size = netMessage::GetHeaderSize(MSG_ID());

    size += SIZEOF_BITFLAGS;

    u32 bitFlags = GetBitFlags();

    if(bitFlags & (1<<CREATE_MSG))
    {
        size += SIZEOF_TIMESTAMP;
        size += packedMessageType::SIZEOF_MESSAGE_COUNT;
        size += packedMessageType::SIZEOF_BUFFER_SIZE;
    }

    if(bitFlags & (1<<CREATE_ACK_MSG))
    {
        size += packedMessageType::SIZEOF_MESSAGE_COUNT;
        size += packedMessageType::SIZEOF_BUFFER_SIZE;
    }

    if(bitFlags & (1<<REMOVE_MSG))
    {
        size += packedMessageType::SIZEOF_MESSAGE_COUNT;
        size += packedMessageType::SIZEOF_BUFFER_SIZE;
    }

    if(bitFlags & (1<<REMOVE_ACK_MSG))
    {
        size += packedMessageType::SIZEOF_MESSAGE_COUNT;
        size += packedMessageType::SIZEOF_BUFFER_SIZE;
    }

    return size;
}

unsigned packedReliablesMsg::GetMessageDataBitSize() const
{
    unsigned size = 0;

    size += m_Creates.GetSize();
    size += m_CreateAcks.GetSize();
    size += m_Removes.GetSize();
    size += m_RemoveAcks.GetSize();

    return size;
}

bool packedReliablesMsg::HasDataToSend() const
{
    unsigned messageCount = 0;

    messageCount += m_Creates.m_MessageCount;
    messageCount += m_CreateAcks.m_MessageCount;
    messageCount += m_Removes.m_MessageCount;
    messageCount += m_RemoveAcks.m_MessageCount;

    return (messageCount > 0);
}

void packedReliablesMsg::Reset()
{
    m_OwnerPlayer = 0;
    m_Timestamp   = 0;
    m_Creates.Reset();
    m_CreateAcks.Reset();
    m_Removes.Reset();
    m_RemoveAcks.Reset();
}

u32 packedReliablesMsg::GetBitFlags() const
{
    u32 bitFlags = 0;

    if(m_Creates.m_MessageCount)
        bitFlags |= (1<<CREATE_MSG);

    if(m_CreateAcks.m_MessageCount)
        bitFlags |= (1<<CREATE_ACK_MSG);

    if(m_Removes.m_MessageCount)
        bitFlags |= (1<<REMOVE_MSG);

    if(m_RemoveAcks.m_MessageCount)
        bitFlags |= (1<<REMOVE_ACK_MSG);

    return bitFlags;
}

bool packedReliablesMsg::IsSpaceAvailable(unsigned numRequiredBits) const
{
    static const unsigned maxSizeOfPackedMessageData = MAX_MESSAGE_PAYLOAD_BITS - SIZEOF_BITFLAGS - SIZEOF_TIMESTAMP - ((packedMessageType::SIZEOF_MESSAGE_COUNT + packedMessageType::SIZEOF_BUFFER_SIZE) * NUM_MSG_TYPES);

    unsigned bitsUsed = 0;
    
    bitsUsed += m_Creates.m_MessageBuffer.GetCursorPos();
    bitsUsed += m_CreateAcks.m_MessageBuffer.GetCursorPos();
    bitsUsed += m_Removes.m_MessageBuffer.GetCursorPos();
    bitsUsed += m_RemoveAcks.m_MessageBuffer.GetCursorPos();

    Assert(bitsUsed <= maxSizeOfPackedMessageData);

    return ((bitsUsed + numRequiredBits) < maxSizeOfPackedMessageData);
}

///////////////////////////////////////////////////////////////////////////////////////
// reassignNegotiateMsg
///////////////////////////////////////////////////////////////////////////////////////

reassignNegotiateMsg::reassignNegotiateMsg(u64           *leavingPeers,
                                           u8            *buffer,
                                           const unsigned maxByteSizeofBuffer) :
m_NumObjects(0),
m_LeavingPeers(leavingPeers),
m_NumLeavingPeers(0),
m_MessageBuffer(buffer),
m_ByteSizeofBuffer(0),
m_MaxByteSizeofBuffer(maxByteSizeofBuffer)
{
}

reassignNegotiateMsg::reassignNegotiateMsg(const unsigned  numObjects,
                                           u64            *leavingPeers,
                                           const unsigned  numLeavingPeers,
                                           u8             *buffer,
                                           const unsigned  byteSizeofBuffer ) :
m_NumObjects(numObjects),
m_LeavingPeers(leavingPeers),
m_NumLeavingPeers(numLeavingPeers),
m_MessageBuffer(buffer),
m_ByteSizeofBuffer(byteSizeofBuffer),
m_MaxByteSizeofBuffer(byteSizeofBuffer)
{
}

u64 *reassignNegotiateMsg::GetLeavingPeersToNegotiateFor(unsigned &numLeavingPeers)
{
    numLeavingPeers = m_NumLeavingPeers;

    return m_LeavingPeers;
}

void reassignNegotiateMsg::WriteToLogFile(bool received, const netPlayer &player) const
{
    netLogSplitter log(netInterface::GetMessageLog(), netInterface::GetObjectManager().GetReassignMgr().GetLog());

    if (received)
    {
        NetworkLogUtils::WriteMessageHeader(log, received, 0, player, "RECEIVED_REASSIGN_NEGOTIATE", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(log, received, 0, player, "SENDING_REASSIGN_NEGOTIATE", "");
    }

    log.WriteDataValue("Num objects", "%d", m_NumObjects);

    if(!received)
    {
        for(unsigned index = 0; index < m_NumLeavingPeers; index++)
        {
            log.WriteDataValue("Leaving peer Name", "%s", netInterface::GetObjectManager().GetReassignMgr().GetLeavingPeerName(m_LeavingPeers[index]));
            log.WriteDataValue("Leaving peer ID", "%llx", m_LeavingPeers[index]);
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////
// reassignConfirmMsg
///////////////////////////////////////////////////////////////////////////////////////

reassignConfirmMsg::reassignConfirmMsg(u8            *buffer,
                                       const unsigned maxByteSizeofBuffer) :
m_NumLocalObjects(0),
m_NumRemoteObjects(0),
m_LeavingPeerID(0),
m_MessageBuffer(buffer),
m_ByteSizeofBuffer(0),
m_MaxByteSizeofBuffer(maxByteSizeofBuffer)
{
}

reassignConfirmMsg::reassignConfirmMsg(const unsigned  numLocalObjects,
                                       const unsigned  numRemoteObjects,
                                       const u64       leavingPeerID,
                                       u8             *buffer,
                                       const unsigned  byteSizeofBuffer ) :
m_NumLocalObjects(numLocalObjects),
m_NumRemoteObjects(numRemoteObjects),
m_LeavingPeerID(leavingPeerID),
m_MessageBuffer(buffer),
m_ByteSizeofBuffer(byteSizeofBuffer),
m_MaxByteSizeofBuffer(byteSizeofBuffer)
{
}

void reassignConfirmMsg::WriteToLogFile(bool received, const netPlayer &player) const
{
    netLogSplitter log(netInterface::GetMessageLog(), netInterface::GetObjectManager().GetReassignMgr().GetLog());

    if (received)
    {
        NetworkLogUtils::WriteMessageHeader(log, received, 0, player, "RECEIVED_REASSIGN_CONFIRM", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(log, received, 0, player, "SENDING_REASSIGN_CONFIRM", "");
    }

    log.WriteDataValue("Num local objects",   "%d", m_NumLocalObjects);
    log.WriteDataValue("Num remote objects",  "%d", m_NumRemoteObjects);
    log.WriteDataValue("Leaving peer Name", "%s", netInterface::GetObjectManager().GetReassignMgr().GetLeavingPeerName(m_LeavingPeerID));
    log.WriteDataValue("Leaving peer ID", "%llx", m_LeavingPeerID);
}

///////////////////////////////////////////////////////////////////////////////////////
// reassignResponseMsg
///////////////////////////////////////////////////////////////////////////////////////

static const char *GetResponseIDByName(u32 responseCode)
{
    const char *name = "UNKNOWN";
    switch(responseCode)
    {
    case RESPONSE_NOT_ON_THIS_MACHINE:
        name = "NOT_ON_THIS_MACHINE";
        break;
    case RESPONSE_NOT_READY:
        name = "NOT_READY";
        break;
    case RESPONSE_NOT_REASSIGNING:
        name = "NOT_REASSIGNING";
        break;
    case RESPONSE_PROCESSED:
        name = "PROCESSED";
        break;
    case RESPONSE_PROCESSED_ALREADY:
        name = "PROCESSED_ALREADY";
        break;
    case RESPONSE_FINISHED:
        name = "FINISHED";
        break;
    default:
        break;
    };

    return name;
}

reassignResponseMsg::reassignResponseMsg() :
m_ResponseType(NEGOTIATION_RESPONSE)
, m_ResponseID(0)
, m_LeavingPeerID(0)
, m_Confirming(false)
, m_HasProcessedNegotiationData(false)
, m_NumPeerNegotiationResponses(0)
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PeerNegotiationResponses[index].m_PeerID       = 0;
        m_PeerNegotiationResponses[index].m_ResponseCode = 0;
    }
}

reassignResponseMsg::reassignResponseMsg(const ResponseType responseType,
                                         const u64          leavingPeerID,
                                         const unsigned     responseID) :
m_ResponseType(responseType)
, m_ResponseID(responseID)
, m_LeavingPeerID(leavingPeerID)
, m_Confirming(false)
, m_HasProcessedNegotiationData(false)
, m_NumPeerNegotiationResponses(0)
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PeerNegotiationResponses[index].m_PeerID       = 0;
        m_PeerNegotiationResponses[index].m_ResponseCode = 0;
    }
}

bool reassignResponseMsg::IsConfirming()
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "IsConfirming() should only be called on negotation responses");

    return m_Confirming;
}

void reassignResponseMsg::SetConfirming()
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "SetConfirming() should only be called on negotation responses");

    m_Confirming = true;
}

bool reassignResponseMsg::HasProcessedNegotiationData()
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "HasProcessedNegotiationData() should only be called on negotation responses");

    return m_HasProcessedNegotiationData;
}

void reassignResponseMsg::SetHasProcessedNegotiationData()
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "SetHasProcessedNegotiationData() should only be called on negotation responses");

    m_HasProcessedNegotiationData = true;
}

unsigned reassignResponseMsg::GetNumPeerNegotiationResponses()
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "GetNumPeerNegotiationResponses() should only be called on negotation responses");

    return m_NumPeerNegotiationResponses;
}

void reassignResponseMsg::GetPeerNegotiationResponseData(unsigned index, u64 &peerID, unsigned &responseCode)
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "GetPeerNegotiationResponseData() should only be called on negotation responses");

    if(gnetVerifyf(index < m_NumPeerNegotiationResponses, "Invalid index passed to GetPeerNegotiationResponseData()!"))
    {
        peerID       = m_PeerNegotiationResponses[index].m_PeerID;
        responseCode = m_PeerNegotiationResponses[index].m_ResponseCode;
    }
}

void reassignResponseMsg::AddPeerNegotiationResponse(u64 peerID, unsigned responseCode)
{
    gnetAssertf(m_ResponseType == NEGOTIATION_RESPONSE, "AddPeerNegotiationResponse() should only be called on negotation responses");

    if(gnetVerifyf(m_NumPeerNegotiationResponses < MAX_NUM_PHYSICAL_PLAYERS, "Adding too many peer negotiation responses!"))
    {
        m_PeerNegotiationResponses[m_NumPeerNegotiationResponses].m_PeerID       = peerID;
        m_PeerNegotiationResponses[m_NumPeerNegotiationResponses].m_ResponseCode = responseCode;

        m_NumPeerNegotiationResponses++;
    }
}

void reassignResponseMsg::WriteToLogFile(bool received, const netPlayer &player) const
{
    netLogSplitter log(netInterface::GetMessageLog(), netInterface::GetObjectManager().GetReassignMgr().GetLog());

    if (received)
    {
        NetworkLogUtils::WriteMessageHeader(log, received, 0, player, "RECEIVED_REASSIGN_RESPONSE", "");
    }
    else
    {
        NetworkLogUtils::WriteMessageHeader(log, received, 0, player, "SENDING_REASSIGN_RESPONSE", "");
    }
    
    if(m_ResponseType == NEGOTIATION_RESPONSE)
    {
        log.WriteDataValue("Response Type",  "NEGOTIATE");
        log.WriteDataValue("Confirming",     m_Confirming                  ? "TRUE" : "FALSE");
        log.WriteDataValue("Processed Data", m_HasProcessedNegotiationData ? "TRUE" : "FALSE");

        for(unsigned index = 0; index < m_NumPeerNegotiationResponses; index++)
        {
            log.WriteDataValue("Peer",        "%s",   netInterface::GetObjectManager().GetReassignMgr().GetLeavingPeerName(m_PeerNegotiationResponses[index].m_PeerID));
            log.WriteDataValue("Peer ID",     "%llx", m_PeerNegotiationResponses[index].m_PeerID);
            log.WriteDataValue("Response ID", "%s",   GetResponseIDByName(m_PeerNegotiationResponses[index].m_ResponseCode));
        }
    }
    else
    {
        log.WriteDataValue("Response Type",   "CONFIRM");
        log.WriteDataValue("Response ID",     "%s",   GetResponseIDByName(m_ResponseID));
        log.WriteDataValue("Leaving peer Name", "%s", netInterface::GetObjectManager().GetReassignMgr().GetLeavingPeerName(m_LeavingPeerID));
        log.WriteDataValue("Leaving peer ID", "%llx", m_LeavingPeerID);
    }
}

} // namespace rage
