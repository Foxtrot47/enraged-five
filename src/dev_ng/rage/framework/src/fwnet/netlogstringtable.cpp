//
// netlogstringtable.cpp
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

// --- Include Files ------------------------------------------------------------
#include "fwnet/optimisations.h"
NETWORK_OPTIMISATIONS()

#include "fwnet/netlogstringtable.h"
#include "fwnet/nettypes.h"
#include "fwnet/netinterface.h"

#include <string.h>

#include "string/stringhash.h"
#include "system/memory.h"
#include "system/new.h"
#include "system/tinyheap.h"

#include "fwnet/netchannel.h"

namespace rage
{

netLogStringTable::netLogStringTable() :
m_StringTableHeapBuffer(0)
, m_StringTableHeap(0)
, m_StringCount(0)
{
    for(unsigned index = 0; index < MAX_STRINGS; index++)
    {
        m_StringInfo [index] = 0;
        m_StringTable[index] = 0;
    }
}

netLogStringTable::~netLogStringTable()
{
}

void netLogStringTable::Init(const unsigned stringTableHeapSize)
{
#if __ASSERT
    const unsigned MIN_STRING_TABLE_HEAP = 4 * 1024;
    NETWORK_QUITF(stringTableHeapSize >= MIN_STRING_TABLE_HEAP, "Initialising the network log string table with too small a heap size (size=%d, min=%d)", stringTableHeapSize, MIN_STRING_TABLE_HEAP);
#endif // __ASSERT

    // need to ensure we are using the master heap so we can get access to the debug allocator
    sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
    sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

    sysMemStartDebug();
    m_StringTableHeapBuffer = rage_new u8[stringTableHeapSize];
    sysMemEndDebug();

    sysMemAllocator::SetCurrent(*oldHeap);

    m_StringTableHeap = rage_new sysTinyHeap(m_StringTableHeapBuffer, stringTableHeapSize);

    NETWORK_QUITF(m_StringTableHeap, "Failed to allocate string table heap!");

    for(unsigned index = 0; index < MAX_STRINGS; index++)
    {
        m_StringInfo [index] = 0;
        m_StringTable[index] = 0;
    }
}

void netLogStringTable::Shutdown()
{
    // delete strings
    for(unsigned index = 0; index < m_StringCount; index++)
    {
        m_StringTableHeap->Free(const_cast<char *>(m_StringTable[index]));
    }

    // delete string infos
    for(unsigned index = 0; index < MAX_STRINGS; index++)
    {
        stringInfo *head = m_StringInfo[index];
        stringInfo *next = head ? head->m_NextInfo : 0;

        while(head)
        {
            m_StringTableHeap->Free(head);
            head = next;
            next = head ? head->m_NextInfo : 0;
        }
    }

    delete m_StringTableHeap;
    m_StringTableHeap = 0;

    sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
    sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

    delete [] m_StringTableHeapBuffer;
    m_StringTableHeapBuffer = 0;

    sysMemAllocator::SetCurrent(*oldHeap);
}

bool netLogStringTable::IsInitialised()
{
    return m_StringTableHeap != 0;
}

void netLogStringTable::AddString(const char *string)
{
    if(m_StringCount < MAX_STRINGS)
    {
        unsigned stringID = 0;
        if(gnetVerifyf(string, "Trying to add an invalid string to the table!") &&
           gnetVerifyf(!FindString(string, stringID), "Trying to add a string to the table this is already present!"))
        {
            stringInfo *info = (stringInfo *)m_StringTableHeap->Allocate(sizeof(stringInfo));

            if(info)
            {
                char *copyOfString = (char *)m_StringTableHeap->Allocate((sizeof(char) * (strlen(string) + 1)));

                if(copyOfString == 0)
                {
                    m_StringTableHeap->Free(info);
                }
                else
                {
                    strcpy(copyOfString, string);
                    info->m_String   = copyOfString;
                    info->m_NextInfo = 0;

                    m_StringTable[m_StringCount] = info->m_String;
                    info->m_StringID = m_StringCount;
                    m_StringCount++;

                    u32 hash = atStringHash(string) % MAX_STRINGS;

                    stringInfo *existingString = m_StringInfo[hash];

                    if(existingString == 0)
                    {
                        m_StringInfo[hash] = info;
                    }
                    else
                    {
                        while(existingString->m_NextInfo != 0)
                        {
                            existingString = existingString->m_NextInfo;
                        }

                        existingString->m_NextInfo = info;
                    }
                }
            }
        }
    }
}

const char *netLogStringTable::GetString(unsigned stringID)
{
    const char *string = 0;

    if(gnetVerifyf(stringID < m_StringCount, "Trying to get a string that does not exist!"))
    {
        string = m_StringTable[stringID];
    }

    return string;
}

bool netLogStringTable::FindString(const char *string, unsigned &stringID)
{
    bool stringFound = false;

    if(gnetVerifyf(string, "Trying to find an invalid string!"))
    {
        u32 hash = atStringHash(string) % MAX_STRINGS;

        stringInfo *existingString = m_StringInfo[hash];

        while(existingString && !stringFound)
        {
            if(strcmp(existingString->m_String, string)==0)
            {
                stringID    = existingString->m_StringID;
                stringFound = true;
            }
            else
            {
                existingString = existingString->m_NextInfo;
            }
        }
    }

    return stringFound;
}

} // namespace rage
