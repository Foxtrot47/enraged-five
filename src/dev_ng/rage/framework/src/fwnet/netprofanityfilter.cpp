
#include "netprofanityfilter.h"
#include "fwnet/netchannel.h"
#include "fwnet/optimisations.h"
#include "fwutil/ProfanityFilter.h"

#include "diag/seh.h"
#include "net/status.h"
#include "rline/ros/rlros.h"
#include "rline/socialclub/rlsocialclub.h"
#include "rline/socialclub/rlsocialclubtask.h"

#include "rline/rlprofanitycheck.h"
#include "net/task.h"

using namespace rage;

RAGE_DEFINE_SUBCHANNEL(net, profanity)
#undef __net_channel
#define __net_channel net_profanity

NETWORK_OPTIMISATIONS() 

namespace netProfanityFilter {

static CheckTextToken GenerateToken(u16 slot, u16 generation)
{
	//TOKEN = [ generation (16 bits) | slot (16 bits) ]
	CheckTextToken newToken = ((u32)generation) << 16 | slot;
	return newToken;
}

static void DisectToken(const CheckTextToken& inToken, u16& outSlot, u16& outGeneration)
{
	outSlot = (u16) inToken & 0x0000ffff;
	outGeneration = (u16)((inToken & 0xffff0000) >> 16);
}

//////////////////////////////////////////////////////////////////////////
//	
//	netProfanityFilterRequest - helper class to wrap a request to check text.
//
//////////////////////////////////////////////////////////////////////////
class netProfanityFilterRequest
{
public:
	netProfanityFilterRequest() 
		: m_generation(0)
        , m_passedProfanityCheck(false)
		, m_numProfaneWord(0) 
	{}

	bool IsPending() const { return m_status.Pending(); }
	void Reset()
	{
		if (m_status.Pending())
		{
			netTask::Cancel(&m_status);
		}

		m_status.Reset();
		m_generation++;
		m_numProfaneWord = 0;
        m_passedProfanityCheck = false;
		m_profaneWord[0] = '\0';
	}

	// the parameter ugcCheck must be set to true if the text is meant to be used for an UGC (dev rights are different for UGC)
	bool CheckText(const char* inText, const rlScLanguage UNUSED_PARAM(language), bool ugcCheck)
	{
		if (IsPending())
		{
			return false;
		}

		Reset();

		fwProfanityFilter::eRESULT offlineResult = fwProfanityFilter::RESULT_NOT_READY;

		// Check for whitespace-only strings so we can return success right away
		bool characterFound = false;
		const int length = istrlen(inText);
		int i = 0;
		while(!characterFound && i<length)
		{
			characterFound = inText[i] != ' ';
			i++;
		}
		 // if no character was found, succeed
		if(!characterFound)
		{
			m_status.ForceSucceeded();
			return true;
		}



		//
		// check text in offline profanity filter 1st
		//
		if( fwProfanityFilter::GetInstance().IsAvailable() )  // failed the offline profanity filter
		{
			const char* profaneWord=NULL;
			offlineResult = fwProfanityFilter::GetInstance().CheckForProfanityAndReservedTerms( inText , false, &profaneWord);

			if( offlineResult != fwProfanityFilter::RESULT_VALID )
			{
				safecpy(m_profaneWord, profaneWord);
				m_status.ForceFailed( RLSC_ERROR_PROFANE_TEXT );  // fail out
				return true;
			}
		}

		//Check for any valid credentials...who cares
		int localGamerIndex = -1;
		for(int i = 0; i < RL_MAX_LOCAL_GAMERS && !RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex); ++i)
		{
			if(rlRos::GetCredentials(i).IsValid())
			{
				localGamerIndex = i;
			}
		}

		if (RL_IS_VALID_LOCAL_GAMER_INDEX(localGamerIndex))  // online is available
		{
			return rlProfanityCheck::ProfanityCheck(localGamerIndex, inText, m_profaneWord, rlScCheckTextTask::MAX_PROFANE_WORD_LENGTH, &m_passedProfanityCheck, &m_numProfaneWord, ugcCheck, &m_status);
		}
		else  // online is not available
		{
#if RSG_PC
			if (offlineResult == fwProfanityFilter::RESULT_VALID)
			{
				m_status.ForceSucceeded();
			}
			else
			{
				m_status.ForceFailed();
			}
#else	//	RSG_PC
			m_status.ForceFailed();
#endif	//	RSG_PC

			return true;
		}
	}

	ReturnCode GetStatus() const
	{
		if (m_status.Pending())
		{
			return RESULT_PENDING;
		}
		else if (m_status.Succeeded())
		{
			if (!m_passedProfanityCheck)
			{
				return RESULT_STRING_FAILED;
			}

			return RESULT_STRING_OK;
		}
		else if (m_status.Failed())
		{
			//Get the error code and see if it fail the profanity check
			int err = m_status.GetResultCode();
			if (err == RLSC_ERROR_PROFANE_TEXT)
			{
				return RESULT_STRING_FAILED;
			}
			else
			{
				gnetError( "Unexpected Error code in netProfanityFilterRequest: %d", err);
				return RESULT_ERROR;
			}
		}
		
		return RESULT_ERROR;
	}

	const char* GetProfaneWord() const
	{
		if (gnetVerifyf(!m_passedProfanityCheck, "This profanity check didnt fail, no profane word to retrieve"))
		{
			//Get the error code and see if it fail the profanity check
			Assert(m_status.GetResultCode() == RLSC_ERROR_PROFANE_TEXT);
			return m_profaneWord;
		}

		return NULL;
	}

	const u16 GetGeneration() const { return m_generation; }
	const CheckTextToken GenerateToken (const u16 slot) const 
	{
		return netProfanityFilter::GenerateToken(slot, m_generation);
	}

private:

	netStatus  m_status;
	u16        m_generation;
	char       m_profaneWord[rlScCheckTextTask::MAX_PROFANE_WORD_LENGTH];

    bool       m_passedProfanityCheck;
	unsigned   m_numProfaneWord;

}; //class netProfanityFilterRequest


//////////////////////////////////////////////////////////////////////////
//
//  netProfanityFilterMgr - Manage set of requests to check request, using 
//		a token system to reference them.
//
//////////////////////////////////////////////////////////////////////////
class netProfanityFilterMgr
{
public:
	netProfanityFilterMgr() : m_lastUsedSlot(0xFFFF) {}

	bool CheckText(const char* inText, const rlScLanguage language, bool ugcCheck, CheckTextToken& outToken);
	ReturnCode GetStatusByToken(const CheckTextToken& inToken);
	const char* GetProfaneWordByToken(const CheckTextToken& inToken);

	bool ClearRequestForToken(const CheckTextToken& inToken);

private:

	static const int MAX_REQUESTS = 5;
	netProfanityFilterRequest m_RequestPool[MAX_REQUESTS];
	u16 m_lastUsedSlot;

}; //class netProfanityFilterMgr

bool netProfanityFilterMgr::CheckText(const char* inText, const rlScLanguage language, bool ugcCheck, CheckTextToken& outToken)
{
#if !__NO_OUTPUT
	char debugSTring[netProfanityFilter::MAX_STRING_SIZE];
	safecpy(debugSTring, inText);
#endif

	//First time
	if (m_lastUsedSlot == 0xFFFF)
	{
		m_lastUsedSlot = 0;
		if (!m_RequestPool[m_lastUsedSlot].IsPending())
		{
			if(m_RequestPool[m_lastUsedSlot].CheckText(inText, language, ugcCheck))
			{
				outToken = m_RequestPool[m_lastUsedSlot].GenerateToken(m_lastUsedSlot);
				gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
				return true;
			}
		}
	}

	//Find a slot.
	u16 slotToUse = m_lastUsedSlot + 1;
	u16 slotTested=0;
	do 
	{
		//Wrap around
		if (slotToUse >= MAX_REQUESTS)
		{
			slotToUse = 0;
		}

		//See if this one is available
		if (!m_RequestPool[slotToUse].IsPending())
		{
			if(gnetVerify(m_RequestPool[slotToUse].CheckText(inText, language, ugcCheck)))
			{
				outToken = m_RequestPool[slotToUse].GenerateToken(slotToUse);
				gnetDebug1("Token 0x%x for %s", outToken, debugSTring);
				m_lastUsedSlot = slotToUse;
				return true;
			}
		}

		slotToUse++;
		slotTested++;

	} while (slotToUse != m_lastUsedSlot && slotTested!=MAX_REQUESTS);

	gnetError("Unable to request CheckText for string %s", debugSTring);
	return false;
	
}

netProfanityFilter::ReturnCode netProfanityFilterMgr::GetStatusByToken( const CheckTextToken& inToken )
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netProfanityFilter::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			netProfanityFilter::ReturnCode retCode = m_RequestPool[slot].GetStatus();

#if !__NO_OUTPUT
			if(retCode != RESULT_PENDING)
			{
				gnetDebug1("Result code for token 0x%x is %d", inToken, retCode);
			}
#endif //!__NO_OUTPUT

			return retCode;
		}

		gnetError("Token 0x%x is invalid.  Slot %d is generation %d", inToken, slot, m_RequestPool[slot].GetGeneration() );

		return RESULT_INVALID_TOKEN;
	}

	return RESULT_INVALID_TOKEN;
}

const char* netProfanityFilterMgr::GetProfaneWordByToken(const CheckTextToken& inToken)
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netProfanityFilter::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			const char* profaneWord = m_RequestPool[slot].GetProfaneWord();

#if !__NO_OUTPUT
			if(profaneWord != NULL)
				gnetDebug1("Profane word for token 0x%x is %s", inToken, profaneWord);
#endif

			return profaneWord;
		}

		gnetError("Token 0x%x is invalid.  Slot %d is generation %d", inToken, slot, m_RequestPool[slot].GetGeneration() );
	}

	return NULL;
}


bool netProfanityFilterMgr::ClearRequestForToken(const CheckTextToken& inToken)
{
	u16 slot = 0xffff;
	u16 generation = 0;
	netProfanityFilter::DisectToken(inToken, slot, generation);

	//Check if it's a valid token. 
	//NOTE 0 will get passed into here (from script)
	if(slot < MAX_REQUESTS && generation > 0)
	{
		if(m_RequestPool[slot].GetGeneration() == generation)
		{
			m_RequestPool[slot].Reset();
			return true;
		}
	}

	return false;
}

} //namespace netProfanityFilter

netProfanityFilter::netProfanityFilterMgr s_profanityFilterMgr;

bool netProfanityFilter::VerifyStringForProfanity( const char* inString, const rlScLanguage language, bool ugcCheck, CheckTextToken& outToken )
{
	if (!gnetVerify(inString))
	{
		return false;
	}

	//String size
	u32 len = (u32)istrlen(inString);
	if (len > netProfanityFilter::MAX_STRING_SIZE)
	{
		gnetDebug3("%s", inString);
		gnetError("String is too large for profanity checking [len:%d Max:%d Diff:%d]", len, netProfanityFilter::MAX_STRING_SIZE, len - netProfanityFilter::MAX_STRING_SIZE );
		return false;
	}

	return s_profanityFilterMgr.CheckText(inString, language, ugcCheck, outToken);
}


netProfanityFilter::ReturnCode netProfanityFilter::GetStatusForRequest( const CheckTextToken& token )
{
	return s_profanityFilterMgr.GetStatusByToken(token);
}


const char* netProfanityFilter::GetProfaneWordForRequest(const netProfanityFilter::CheckTextToken& token)
{
	return s_profanityFilterMgr.GetProfaneWordByToken(token);
}

bool netProfanityFilter::ClearRequest(const CheckTextToken& token)
{
	return s_profanityFilterMgr.ClearRequestForToken(token);
}


