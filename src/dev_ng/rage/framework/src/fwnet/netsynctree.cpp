// 
// netSyncTree.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "fwnet/netsynctree.h"

// framework includes
#include "fwnet/netchannel.h"
#include "fwnet/netinterface.h"
#include "fwnet/optimisations.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netsyncnode.h"
#include "fwsys/timer.h"

// rage includes
#include "bank/bank.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

static const u8 INVALID_BATCH_ID = 0xff;

netSyncTree::netSyncTree() : 
m_TargetObject(0)
, m_RootNode(0)
, m_CurrentParent(0)
, m_NumDataNodes(0)
, m_NumSyncUpdateNodes(0)
, m_MaxBufferDataSize(0)
, m_MaxNumHeaderBits(0)
, m_DataNodes(netSyncDataNode::MAX_SYNC_DATA_NODES)
, m_SyncUpdateNodes(netSyncDataNode::MAX_UPDATE_DATA_NODES)
, m_FailedToWriteAllData(false)
BANK_ONLY(, m_NumTimesUpdateCalled(0))
BANK_ONLY(, m_NumTimesReadCalled(0))
BANK_ONLY(, m_NumTimesWriteCalled(0))
ASSERT_ONLY(, m_IgnoreBatchAssert(false))
ASSERT_ONLY(, m_LastObjectBatchTick(0))
{
    ASSERT_ONLY(m_TreeInitialised = false);
	BANK_ONLY(m_TreeName[0] = '\0';)

    for(unsigned updateLevel = 0; updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS; updateLevel++)
    {
        for(unsigned batch = 0; batch < MAX_BATCHES; batch++)
        {
            m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch] = 0;

            m_BatchUpdateInfo[updateLevel].m_NextBatchUpdateTime[batch] = 0;
        }

        m_NumAllowedBatches[updateLevel] = 0;

        m_BatchUpdateInfo[updateLevel].m_CurrentBatchToProcess = INVALID_BATCH_ID;
        m_BatchUpdateInfo[updateLevel].m_NextBatchToProcess    = INVALID_BATCH_ID;
        m_BatchUpdateInfo[updateLevel].m_BatchUpdateFrequency  = 0;
    }

	for (int i=0; i<m_DataNodes.GetMaxCount(); i++)
	{
		m_DataNodes[i] = 0;
	}

	for (int i=0; i<m_SyncUpdateNodes.GetMaxCount(); i++)
	{
		m_SyncUpdateNodes[i] = 0;
	}
}

netSyncTree::~netSyncTree()
{
}

void netSyncTree::SetDataNodeIndices(netSyncDataNode* pNode, unsigned dataIndex, int syncDataIndex)
{
	if (gnetVerify((int)dataIndex < m_DataNodes.GetMaxCount()))
	{
		BANK_ONLY(gnetAssertf(!m_DataNodes[dataIndex], "Data node %s already added at index %d", m_DataNodes[dataIndex]->GetNodeName(), dataIndex);)
		m_DataNodes[dataIndex] = pNode;
	}

	if (syncDataIndex >= 0)
	{
		if (gnetVerify(syncDataIndex < m_SyncUpdateNodes.GetMaxCount()))
		{
			BANK_ONLY(gnetAssertf(!m_SyncUpdateNodes[syncDataIndex], "Sync data node %s already added at index %d", m_SyncUpdateNodes[syncDataIndex]->GetNodeName(), syncDataIndex);)
			m_SyncUpdateNodes[syncDataIndex] = pNode;
		}
	}
}

u8 netSyncTree::GetNumAllowedBatches(unsigned updateLevel) const
{
    if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Invalid update level!"))
    {
        gnetAssertf(m_NumAllowedBatches[updateLevel] <= MAX_BATCHES, "Invalid number of batches!");
        return m_NumAllowedBatches[updateLevel];
    }

    return 1;
}

void netSyncTree::IncreaseObjectCount(unsigned updateLevel, unsigned batch)
{
    if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Invalid update level!") &&
       gnetVerifyf(batch       < MAX_BATCHES,                               "Invalid batch!"))
    {
        gnetAssertf(m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch] < 255, "Too many objects at this update level and batch!");
        m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch]++;
    }
}

void netSyncTree::DecreaseObjectCount(unsigned updateLevel, unsigned batch)
{
    if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Invalid update level!") &&
       gnetVerifyf(batch       < MAX_BATCHES,                               "Invalid batch!"))
    {
        gnetAssertf(m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch] > 0, "Invalid number of objects at this update level and batch!");
        m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch]--;
    }
}

unsigned netSyncTree::GetObjectCount(unsigned updateLevel, unsigned batch) const
{
    if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Invalid update level!") &&
       gnetVerifyf(batch       < MAX_BATCHES,                               "Invalid batch!"))
    {
        return m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch];
    }

    return 0;
}

u8 netSyncTree::GetBatchForNewObject(unsigned updateLevel) const
{
    u8 newBatch = 0;

    if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Invalid update level!"))
    {
        u8 minNumObjects = m_NumObjectsInBatchAtUpdateLevel[updateLevel][0];

        for(u8 batch = 1; batch < m_NumAllowedBatches[updateLevel]; batch++)
        {
            if(m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch] < minNumObjects)
            {
                newBatch      = batch;
                minNumObjects = m_NumObjectsInBatchAtUpdateLevel[updateLevel][batch];
            }
        }
    }

    return newBatch;
}

u8 netSyncTree::TryToMoveBatch(unsigned updateLevel, u8 oldBatch)
{
    if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Invalid update level!") &&
       gnetVerifyf(oldBatch    < MAX_BATCHES,                               "Invalid batch!"))
    {
        u8 newBatch             = oldBatch;
        u8 numObjectsAtOldBatch = m_NumObjectsInBatchAtUpdateLevel[updateLevel][oldBatch];
        u8 numObjectsAtNewBatch = numObjectsAtOldBatch;

        for(u8 index = 0; index < m_NumAllowedBatches[updateLevel]; index++)
        {
            if(m_NumObjectsInBatchAtUpdateLevel[updateLevel][index] < numObjectsAtNewBatch)
            {
                newBatch             = index;
                numObjectsAtNewBatch = m_NumObjectsInBatchAtUpdateLevel[updateLevel][index];
            }
        }

        const unsigned THRESHOLD_TO_MOVE_BATCH = 2;

        if((numObjectsAtOldBatch - numObjectsAtNewBatch) > THRESHOLD_TO_MOVE_BATCH)
        {
            return newBatch;
        }
    }

    return oldBatch;
}

void netSyncTree::InitialiseTree()
{
	ASSERT_ONLY(gnetAssertf(!m_TreeInitialised, "Sync tree already initialised!"));
	ASSERT_ONLY(m_TreeInitialised = true);

	if (gnetVerify(m_RootNode))
	{
		// initialise sync data nodes: build the array, set data indicies and initialise buffer data start positions. This has to be done in the order the 
		// data nodes appear in the tree from left to right.
		unsigned dataPosition = 0;
		unsigned dataIndex = 0;
		unsigned syncDataIndex = 0;

		m_MaxNumHeaderBits = 0;

		m_RootNode->InitialiseDataNodesInTree(dataPosition, dataIndex, syncDataIndex, m_MaxNumHeaderBits);

		Assert(m_NumDataNodes == dataIndex);
		Assert(m_NumSyncUpdateNodes == syncDataIndex);

		m_MaxBufferDataSize = dataPosition;

        CalculateNumAllowedBatches();
	}
}

void netSyncTree::ShutdownTree()
{
	if (gnetVerify(m_RootNode))
	{
		m_RootNode->ShutdownNode();
	}
}

void netSyncTree::InitialiseData(netSyncTreeTargetObject* pObj)
{
	SetTargetObject(pObj);

	if (gnetVerify(pObj) && gnetVerify(m_RootNode) && gnetVerify(pObj->GetSyncData()))
	{
		pObj->GetSyncData()->GetCurrentStateBuffer()->SetCursorPos(0);
		pObj->GetSyncData()->GetShadowBuffer()->SetCursorPos(0);

		for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
		{
			m_SyncUpdateNodes[i]->InitialiseData(pObj);
		}
	}

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::UpdateCurrentObjectBatches(const unsigned currTime)
{
    for(u8 updateLevel = 0; updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS; updateLevel++)
    {
        BatchUpdateInfo &batchInfo = m_BatchUpdateInfo[updateLevel];

        unsigned nextBatchUpdateTime = batchInfo.m_NextBatchUpdateTime[batchInfo.m_NextBatchToProcess];

        if(currTime >= nextBatchUpdateTime)
        {
            u8 currentBatchToProcess = batchInfo.m_NextBatchToProcess;
            u8 nextBatchToProcess    = ((batchInfo.m_NextBatchToProcess+1)%m_NumAllowedBatches[updateLevel]);

            // set the next update time for the current batch
            batchInfo.m_NextBatchUpdateTime[currentBatchToProcess] = currTime + batchInfo.m_BatchUpdateFrequency;

            batchInfo.m_CurrentBatchToProcess = currentBatchToProcess;
            batchInfo.m_NextBatchToProcess    = nextBatchToProcess;
        }
        else
        {
            batchInfo.m_CurrentBatchToProcess = INVALID_BATCH_ID;
        }
    }

    ASSERT_ONLY(m_LastObjectBatchTick = fwTimer::GetSystemFrameCount());

#if __BANK
    m_AverageReadsPerUpdate.AddSample((float)m_NumTimesReadCalled);
    m_AverageWritesPerUpdate.AddSample((float)m_NumTimesWriteCalled);

    m_NumTimesReadCalled  = 0;
    m_NumTimesWriteCalled = 0;
#endif // __BANK
}

inline void UpdateNodeAndFlagToSendToPlayers(netSyncTreeTargetObject *targetObject, netSyncDataNode &node, netSyncDataBase &syncData, PlayerFlags playersToUpdate)
{
    netSyncDataUnitBase *currentSyncDataUnit = &syncData.GetSyncDataUnit(node.GetDataIndex());
    node.SetCurrentSyncDataUnit(currentSyncDataUnit);
    node.Update(targetObject);
    node.SetCurrentSyncDataUnit(0);
    PlayerFlags playersToIgnore = node.StopSend(targetObject, SERIALISEMODE_UPDATE);
    playersToUpdate &= ~playersToIgnore;
    currentSyncDataUnit->SetPlayersRequireAnUpdate(playersToUpdate & currentSyncDataUnit->GetUnsyncedPlayers());
}

bool netSyncTreeTargetObject::HasNodeDependencies(const netSyncDataNode& dataNode) const
{
	return dataNode.HasNodeDependencies();
}

void AddNodeAndExternalDependentNodes(netSyncDataNode **nodeDependencies, const unsigned arraySize, unsigned &numDependentNodes, netSyncDataNode &dependentNode)
{
    if(gnetVerifyf(numDependentNodes < arraySize, "Ran out of space in dependent nodes list!"))
    {
        nodeDependencies[numDependentNodes] = &dependentNode;
        numDependentNodes++;

        for(unsigned dependencyIndex = 0; dependencyIndex < dependentNode.GetNumExternalDependentNodes(); dependencyIndex++)
        {
            netSyncDataNode *externalDependentNode = dependentNode.GetExternalDependentNode(dependencyIndex);

            if(gnetVerifyf(externalDependentNode, "External dependent node doesn't exist!"))
            {
                AddNodeAndExternalDependentNodes(nodeDependencies, arraySize, numDependentNodes, *externalDependentNode);
            }
        }
    }
}

void netSyncTreeTargetObject::GetAllNodeDependencies(netSyncDataNode& dataNode, netSyncDataNode **nodeDependencies, const unsigned arraySize, unsigned &numDependentNodes)
{
	netSyncDataNode *rootDependentNode = dataNode.GetNodeDependency();

	if (rootDependentNode)
	{
		while (rootDependentNode && rootDependentNode->GetNodeDependency())
		{
			rootDependentNode = rootDependentNode->GetNodeDependency();
		}
	}
	else
	{
		rootDependentNode = &dataNode;
	}

	if (rootDependentNode)
	{
		AddNodeAndExternalDependentNodes(nodeDependencies, arraySize, numDependentNodes, *rootDependentNode);
	}
}

void netSyncTree::Update(netSyncTreeTargetObject *targetObject, ActivationFlags actFlags, const unsigned currTime, bool checkNodesForDirty)
{
    BANK_ONLY(m_NumTimesUpdateCalled++);

    PlayerFlags criticalStateUnsynced = 0;

    bool readyToSendForUpdateLevel[CNetworkSyncDataULBase::NUM_UPDATE_LEVELS] =
    {
        false,
        false,
        false,
        false,
        false
    };

	netSyncDataBase *syncData = targetObject->GetSyncData();

    if(checkNodesForDirty)
    {
        syncData->SetTimeToSendDataUnitsAtUpdateLevel(targetObject->GetMinimumUpdateLevel(), currTime);

        readyToSendForUpdateLevel[targetObject->GetMinimumUpdateLevel()] = true;
    }

    for(int index = (targetObject->GetMinimumUpdateLevel() - 1); index >= 0; index--)
    {
        u8  updateLevel                  = static_cast<u8>(index);
        u32 timeToSendNodesAtUpdateLevel = syncData->GetTimeToSendDataUnitsAtUpdateLevel(updateLevel);

        if(currTime >= timeToSendNodesAtUpdateLevel)
        {
            readyToSendForUpdateLevel[updateLevel] = true;

            syncData->SetTimeToSendDataUnitsAtUpdateLevel(updateLevel, currTime + CNetworkSyncDataULBase::GetUpdateFrequency(updateLevel));
        }
    }

    PlayerFlags lastPlayersSyncUpdated = targetObject->GetLastPlayersSyncUpdated();

    for (u32 nodeIndex = 0; nodeIndex < m_NumSyncUpdateNodes; nodeIndex++)
	{
		netSyncDataNode     *dataNode            = m_SyncUpdateNodes[nodeIndex];
		netSyncDataUnitBase *currentSyncDataUnit = &syncData->GetSyncDataUnit(nodeIndex);

		dataNode->SetCurrentSyncDataUnit(currentSyncDataUnit);

        PlayerFlags unsyncedPlayers = currentSyncDataUnit->GetUnsyncedPlayers();

        PlayerFlags updatePlayers = currentSyncDataUnit->GetUpdatePlayers();
        updatePlayers &= ~lastPlayersSyncUpdated;

        currentSyncDataUnit->SetUpdatePlayers(updatePlayers);

        if(dataNode->GetIsActive(actFlags))
		{
			if (checkNodesForDirty)
			{
                if(dataNode->Update(targetObject))
                {
				    unsyncedPlayers = currentSyncDataUnit->GetUnsyncedPlayers();
                }
			}

            // only check for players still in the game
            unsyncedPlayers &= targetObject->GetClonedState();

            // the node may not want to send data to some players
            PlayerFlags playersToIgnore = dataNode->StopSend(targetObject, SERIALISEMODE_UPDATE);
            unsyncedPlayers &= ~playersToIgnore;

            if(unsyncedPlayers != 0)
            {
                for (PhysicalPlayerIndex player=0; player<MAX_NUM_PHYSICAL_PLAYERS; player++)
				{
				    if (unsyncedPlayers & (1<<player))
					{
                        u8 updateLevel = dataNode->GetNodeUpdateLevel(targetObject->GetUpdateLevel(player));

                        bool forceSend = syncData->IsDataUnitFlaggedForForceSend(player, nodeIndex);

                        if(forceSend)
                        {
                            updatePlayers |= (1<<player);
                        }
                        else
                        {
                            bool timeToSend = readyToSendForUpdateLevel[updateLevel];

                            unsigned timeLastSent = 0;

                            netSequence unackedSeq = currentSyncDataUnit->GetPlayerUnackedSequence(player);

                            if(unackedSeq != 0)
                            {
                                timeLastSent = netInterface::GetTimeSyncMessageSentAndCheckExpiry(player, unackedSeq, dataNode->GetResendFrequency(player));

                                if(timeLastSent != 0)
                                {
                                    timeToSend = ((currTime - timeLastSent) >= CNetworkSyncDataULBase::GetUpdateFrequency(updateLevel));
                                }

                                if((currTime - timeLastSent) >= dataNode->GetResendFrequency(player))
                                {
                                    updatePlayers |= (1<<player);
                                }
                            }

                            if(timeToSend)
                            {
                                updatePlayers |= (1<<player);
                            }
                        }
                    }
                }

                // we still need to clear the update flags for player marked to be ignored as we may have some update flags set
                // from before this function was called
                updatePlayers &= ~playersToIgnore;

                currentSyncDataUnit->SetPlayersRequireAnUpdate(updatePlayers);

				if(dataNode->UsedBySerialiseMode(SERIALISEMODE_CRITICAL))
				{
					criticalStateUnsynced |= unsyncedPlayers;
				}

                // ensure if the node always contains the current state of the object when sending data to a player
                if(!checkNodesForDirty)
                {
                    if(updatePlayers != 0)
                    {
                        // check for dependencies
                        if(dataNode->HasNodeDependencies())
                        {
                            // build a list of node dependencies
                            const unsigned MAX_DEPENDENCIES = 10;
                            netSyncDataNode *nodeDependencies[MAX_DEPENDENCIES];
                            unsigned numDependentNodes = 0;
                            
                            netSyncDataNode *rootDependentNode = dataNode->GetNodeDependency();

                            if(rootDependentNode)
                            {
                                while(rootDependentNode && rootDependentNode->GetNodeDependency())
                                {
                                    rootDependentNode = rootDependentNode->GetNodeDependency();
                                }
                            }
                            else
                            {
                                rootDependentNode = dataNode;
                            }

                            if(rootDependentNode)
                            {
                                AddNodeAndExternalDependentNodes(nodeDependencies, MAX_DEPENDENCIES, numDependentNodes, *rootDependentNode);
                            }

                            for(unsigned index = 0; index < numDependentNodes; index++)
                            {
                                UpdateNodeAndFlagToSendToPlayers(targetObject, *nodeDependencies[index], *syncData, updatePlayers);
                            }
                        }
                        else
                        {
                            dataNode->Update(targetObject);
                        }
                    }
                }
            }
        }

        dataNode->SetCurrentSyncDataUnit(0);
    }

    targetObject->GetSyncData()->Update();
	targetObject->GetSyncData()->SetCriticalStateUnsyncedPlayers(criticalStateUnsynced);
}

bool netSyncTree::Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, 
						const unsigned currTime, netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags)
{
    BANK_ONLY(m_NumTimesWriteCalled++);

	bool bDataWritten = false;

	SetTargetObject(pObj);

	if (gnetVerify(pObj))
	{
		WriteActivationFlags(serMode, actFlags, bitBuffer, pLog);

		if (pNodeFlags) *pNodeFlags = 0;

		ClearUpdatedFlags();

		m_FailedToWriteAllData = false;
		// need to track maximum possible header bits to ensure we always have space for these in the supplied bit buffer
		unsigned numHeaderBitsRemaining = m_MaxNumHeaderBits;

		bool bUpdateWritten = m_RootNode->Write(serMode, actFlags, pObj, bitBuffer, currTime, pLog, player, pNodeFlags, numHeaderBitsRemaining);
		Assertf(serMode != SERIALISEMODE_UPDATE || bUpdateWritten || !pObj->HasGameObject(), "No data written for %s in an update to player %d", pObj->GetLogName(), player);
		return bUpdateWritten;
	}

	DEV_ONLY(SetTargetObject(NULL));

	return bDataWritten;
}

void netSyncTree::Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog)
{
    BANK_ONLY(m_NumTimesReadCalled++);

	if (gnetVerify(m_RootNode))
	{
        ReadActivationFlags(serMode, actFlags, bitBuffer, pLog);

		ClearUpdatedFlags();

		m_RootNode->Read(serMode, actFlags, bitBuffer, pLog);
	}
}

unsigned netSyncTree::GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool outputToTTY)
{
    unsigned maximumDataSize = 0;

    if (gnetVerify(m_RootNode))
    {
        maximumDataSize = m_RootNode->GetMaximumDataSize(serMode, actFlags, outputToTTY);

#if !__NO_OUTPUT
        if(outputToTTY)
        {
            gnetDebug2("Maximum data size for %s (Serialisation mode: %d, Activation flags: %d) is %d", GetTreeName(), serMode, actFlags, maximumDataSize);
        }
#endif //!__NO_OUTPUT
    }

    return maximumDataSize;
}

void netSyncTree::GetDirtyNodes(netSyncTreeTargetObject* pObj, DataNodeFlags& nodeFlags) 
{
	nodeFlags = 0;

	SetTargetObject(pObj);

	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pObj->GetSyncData()->HasSyncDataUnitEverBeenDirtied(i))
		{
			nodeFlags |= (1<<pDataNode->GetDataIndex());
		}
	}

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::DirtyNode(netSyncTreeTargetObject* pObj, netSyncDataNode& dataNode)
{
	SetTargetObject(pObj);

#if __DEV
	ASSERT_ONLY(bool bFound = false);
	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode == &dataNode)
		{
			ASSERT_ONLY(bFound = true);
			break;
		}
	}
	gnetAssertf(bFound, "DirtyNode: node %s not found in tree!", dataNode.GetNodeName());
#endif //__DEV

	dataNode.SetDirty(pObj);

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::DirtyNodes(netSyncTreeTargetObject* pObj, DataNodeFlags& nodeFlags)
{
	SetTargetObject(pObj);

	netSyncDataBase* pSyncData = pObj->GetSyncData();

	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (nodeFlags & (1<<pDataNode->GetDataIndex()))
		{
			pSyncData->SetSyncDataUnitDirty(i);
		}
	}

	DEV_ONLY(SetTargetObject(NULL));
}

bool  netSyncTree::IsNodeSyncedWithPlayer(netSyncTreeTargetObject* pObj, const netSyncDataNode& dataNode, const unsigned playerIndex) const
{
	bool isNodeSynced = true;

	SetTargetObject(pObj);

#if __DEV
	ASSERT_ONLY(bool bFound = false);
	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode == &dataNode)
		{
			ASSERT_ONLY(bFound = true);
			break;
		}
	}
	gnetAssertf(bFound, "IsNodeSyncedWithPlayer: node %s not found in tree!", dataNode.GetNodeName());
#endif //__DEV

	if (dataNode.GetCurrentSyncDataUnit())
	{
		isNodeSynced = dataNode.GetCurrentSyncDataUnit()->IsSyncedWithPlayer(playerIndex);
	}

	DEV_ONLY(SetTargetObject(NULL));

	return isNodeSynced;
}

bool netSyncTree::IsNodeSyncedWithPlayers(netSyncTreeTargetObject* pObj, const netSyncDataNode& dataNode, PlayerFlags playersMask) const
{
	bool isNodeSynced = true;

	SetTargetObject(pObj);

#if __DEV
	ASSERT_ONLY(bool bFound = false);
	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode == &dataNode)
		{
			ASSERT_ONLY(bFound = true);
			break;
		}
	}
	gnetAssertf(bFound, "IsNodeSyncedWithPlayer: node %s not found in tree!", dataNode.GetNodeName());
#endif //__DEV

	if (dataNode.GetCurrentSyncDataUnit())
	{
		isNodeSynced = dataNode.GetCurrentSyncDataUnit()->IsSyncedWithPlayers(playersMask);
	}

	DEV_ONLY(SetTargetObject(NULL));

	return isNodeSynced;
}


bool netSyncTree::IsNodeSyncedWithAllPlayers(netSyncTreeTargetObject* pObj, const netSyncDataNode& dataNode) const
{
	bool isNodeSynced = true;

	SetTargetObject(pObj);

#if __DEV
	ASSERT_ONLY(bool bFound = false);
	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode == &dataNode)
		{
			ASSERT_ONLY(bFound = true);
			break;
		}
	}
	gnetAssertf(bFound, "IsNodeSyncedWithAllPlayers: node %s not found in tree!", dataNode.GetNodeName());
#endif //__DEV

	if (dataNode.GetCurrentSyncDataUnit())
	{
		isNodeSynced = dataNode.GetCurrentSyncDataUnit()->GetUnsyncedPlayers() == 0;
	}

	DEV_ONLY(SetTargetObject(NULL));

	return isNodeSynced;
}

void netSyncTree::ForceSendOfNodeData(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, netSyncDataNode& dataNode)
{
	SetTargetObject(pObj);

#if __DEV
	ASSERT_ONLY(bool bFound = false);
	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode == &dataNode)
		{
			ASSERT_ONLY(bFound = true);
			break;
		}
	}
	gnetAssertf(bFound, "ForceSendOfNodeData: node %s not found in tree!", dataNode.GetNodeName());
#endif //__DEV

	dataNode.ForceSend(serMode, actFlags, pObj);

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::ForceSendOfNodeDataToPlayer(const PhysicalPlayerIndex player, SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, netSyncDataNode& dataNode)
{
	SetTargetObject(pObj);

#if __DEV
	ASSERT_ONLY(bool bFound = false);
	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode == &dataNode)
		{
			ASSERT_ONLY(bFound = true);
			break;
		}
	}
	gnetAssertf(bFound, "ForceSendOfNodeData: node %s not found in tree!", dataNode.GetNodeName());
#endif //__DEV

	dataNode.ForceSendToPlayer(player, serMode, actFlags, pObj);

	DEV_ONLY(SetTargetObject(NULL));
}

bool netSyncTree::CanApplyNodeData(netSyncTreeTargetObject* pObj)
{
	bool bCanApplyData = true;

	SetTargetObject(pObj);

	if (!pObj->CanApplyNodeData())
	{
		bCanApplyData = false;
	}
	else
	{
		for (u32 i=0; i<m_NumDataNodes; i++)
		{
			netSyncDataNode* pDataNode = m_DataNodes[i];

			if (pDataNode->GetWasUpdated() && !pDataNode->CanApplyData(pObj))
			{
				bCanApplyData = false;
				break;
			}
		}
	}

	DEV_ONLY(SetTargetObject(NULL));

	return bCanApplyData;
}

void netSyncTree::ApplyNodeData(netSyncTreeTargetObject* pObj, DataNodeFlags* updatedNodes)
{
	SetTargetObject(pObj);

	for (u32 i=0; i<m_NumDataNodes; i++)
	{
		netSyncDataNode* pDataNode = m_DataNodes[i];

		if (pDataNode->GetWasUpdated())
		{
			pDataNode->ApplyData(pObj);

			if (updatedNodes && pDataNode->GetIsSyncUpdateNode())
			{
				*updatedNodes |= (1<<pDataNode->GetDataIndex());
			}
		}
	}

	PostApplyData(pObj);

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::LogData(netLoggingInterface& log)
{
	for (u32 i=0; i<m_NumDataNodes; i++)
	{
		netSyncDataNode* pDataNode = m_DataNodes[i];

		if (pDataNode->GetWasUpdated())
		{
			pDataNode->LogData(log);
		}
	}
}

void netSyncTree::NodeDataSent(netSyncTreeTargetObject* pObj, const PhysicalPlayerIndex player, const netSequence* pMsgSeq)
{
	SetTargetObject(pObj);

	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		if (pDataNode->GetWasUpdated())
		{
			pDataNode->GetCurrentSyncDataUnit()->UpdateSentToPlayer(player, pMsgSeq);
            
            pObj->GetSyncData()->ClearDataUnitForForceSend(player, i);
		}
	}

	// update sync data so that the update state matches that of the sync units
	pObj->GetSyncData()->Update();

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::ProcessAck(netSyncTreeTargetObject* pObj, const PhysicalPlayerIndex player, const netSequence sequence, const unsigned timeAckedMessageSent, const DataNodeFlags nodeFlags)
{
	SetTargetObject(pObj);

	bool bAckProcessed = false;

	if (!pObj->GetSyncData()->IsSyncedWithPlayer(player))
	{
		for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
		{
			netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];
			netSyncDataUnitBase* pSyncDataUnit = pDataNode->GetCurrentSyncDataUnit();

			if (!pSyncDataUnit->IsSyncedWithPlayer(player))
			{
				if (nodeFlags & (1<<pDataNode->GetDataIndex())) 
				{
                    netSyncDataUnitBase *syncDataUnit = pDataNode->GetCurrentSyncDataUnit();

					if (syncDataUnit->ReceivedAck(player, timeAckedMessageSent))
					{
						bAckProcessed = true;
					}

                    if(syncDataUnit->GetPlayerUnackedSequence(player) == sequence)
                    {
                        syncDataUnit->ClearPlayerUnackedSequence(player);
                    }
				}
			}
		}

		if (bAckProcessed)
		{
			// update sync data so that the unsynced state matches that of the sync units
			pObj->GetSyncData()->Update();
		}
	}

	DEV_ONLY(SetTargetObject(NULL));
}

void netSyncTree::ForceSendOfSyncUpdateNodes(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj)
{
	SetTargetObject(pObj);

	for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
	{
		netSyncDataNode* pDataNode = m_SyncUpdateNodes[i];

		pDataNode->ForceSend(serMode, actFlags, pObj);
	}

	DEV_ONLY(SetTargetObject(NULL));
}

#if __BANK
void netSyncTree::SetTreeName(const char* treeName)
{
	if (gnetVerify(treeName) && gnetVerify(strlen(treeName) <= TREE_NAME_LEN))
	{
		strncpy(m_TreeName, treeName, TREE_NAME_LEN);
	}
}

void netSyncTree::AddWidgets(bkBank* bank)
{
	if (gnetVerify(m_RootNode))
	{
		bank->PushGroup(m_TreeName, false);
		m_RootNode->AddWidgets(bank);
		bank->PopGroup();
	}
}

void netSyncTree::DisplayNodeInformation(netSyncTreeTargetObject* pObj, netLogDisplay& displayLog)
{
	for (u32 i=0; i<m_NumDataNodes; i++)
	{
		m_DataNodes[i]->DisplayNodeInformation(pObj, displayLog);
	}
}

#endif

void netSyncTree::PushParent(netSyncParentNode& parentNode)
{
	if (!m_CurrentParent)
	{
		if (gnetVerify(!m_RootNode))
		{
			m_RootNode = m_CurrentParent = &parentNode;
			RegisterNode(m_RootNode);
			parentNode.SetIsRootNode();
		}
	}
	else
	{
		m_CurrentParent->AddChildNode(&parentNode);
		RegisterNode(&parentNode);
		gnetAssert(!parentNode.GetIsRootNode());

		m_CurrentParent = &parentNode;
	}
}

void netSyncTree::PopParent()
{
	if (gnetVerify(m_CurrentParent))
	{
		m_CurrentParent = m_CurrentParent->GetParentNode();
	}
}

void netSyncTree::SetCurrentParent(netSyncParentNode& parentNode)
{
	if (gnetVerify(parentNode.GetParentTree() == this))
	{
		m_CurrentParent = &parentNode;
	}
}

void netSyncTree::AddChild(netSyncDataNode& childNode)
{
	if (gnetVerify(m_CurrentParent))
	{
		m_CurrentParent->AddChildNode(&childNode);
		RegisterNode(&childNode);
	}
}

void netSyncTree::AddChild(netSyncDataNode& childNode, SerialiseModeFlags serFlags, SerialiseModeFlags conditionalFlags, ActivationFlags actFlags)
{
	if (gnetVerify(m_CurrentParent))
	{
		// if we are setting the node flags via this method, presume they are not set previously when the node was constructed
		gnetAssert(childNode.GetSerialiseModeFlags() == 0);
		gnetAssert(childNode.GetConditionalFlags() == 0);
		gnetAssert(childNode.GetActivationFlags() == 0);

		childNode.SetSerialiseModeFlags(serFlags);
		childNode.SetConditionalFlags(conditionalFlags);
		childNode.SetActivationFlags(actFlags);

		m_CurrentParent->AddChildNode(&childNode);
		RegisterNode(&childNode);
	}
}

void netSyncTree::SetTargetObject(netSyncTreeTargetObject* pObj) const
{
	m_TargetObject = pObj;

	if (pObj && pObj->HasSyncData())
	{
		netSyncDataBase* pSyncData = pObj->GetSyncData();
		for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
		{
			m_SyncUpdateNodes[i]->SetCurrentSyncDataUnit(&pSyncData->GetSyncDataUnit(i));
		}
	}
	else
	{
		for (u32 i=0; i<m_NumSyncUpdateNodes; i++)
		{
			m_SyncUpdateNodes[i]->SetCurrentSyncDataUnit(NULL);
		}
	}
}

void netSyncTree::RegisterNode(netSyncNodeBase* pNewNode)
{
	if (gnetVerify(pNewNode))
	{
		pNewNode->SetParentTree(this);

		if (pNewNode->GetIsDataNode())
		{
			m_NumDataNodes++;

			netSyncDataNode* pDataNode = SafeCast(netSyncDataNode, pNewNode);

			if (pDataNode->GetIsSyncUpdateNode())
			{
				m_NumSyncUpdateNodes++;
			}
		}

		pNewNode->InitialiseNode();
	}
}

void netSyncTree::ClearUpdatedFlags()
{
	for (u32 i=0; i<m_NumDataNodes; i++)
	{
		m_DataNodes[i]->ClearUpdatedFlag();
	}
}

void netSyncTree::CalculateNumAllowedBatches()
{
    const unsigned MIN_BATCH_UPDATE_INTERVAL = 50;

    for(unsigned updateLevel = 0; updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS; updateLevel++)
    {
        unsigned minimumUpdateFrequency = 1000;

        for (u32 nodeIndex=0; nodeIndex<m_NumSyncUpdateNodes; nodeIndex++)
        {
            netSyncDataNode *dataNode = m_SyncUpdateNodes[nodeIndex];

            if(dataNode)
            {
                unsigned updateFrequency = dataNode->GetUpdateFrequency(updateLevel);

                // DAN TEMP - very high update frequency is set to 25 currently
                if(updateFrequency < MIN_BATCH_UPDATE_INTERVAL)
                {
                    updateFrequency = MIN_BATCH_UPDATE_INTERVAL;
                }

                BANK_ONLY(gnetAssertf((updateFrequency - ((updateFrequency/MIN_BATCH_UPDATE_INTERVAL) * MIN_BATCH_UPDATE_INTERVAL)) == 0, "Node update frequencies must always be divisible by %d! (%s: UF:%d)", MIN_BATCH_UPDATE_INTERVAL, dataNode->GetNodeName(), updateFrequency);)

                if(updateFrequency < minimumUpdateFrequency)
                {
                    minimumUpdateFrequency = updateFrequency;
                }
            }
        }

        m_NumAllowedBatches[updateLevel] = static_cast<u8>(minimumUpdateFrequency / MIN_BATCH_UPDATE_INTERVAL);

        if(m_NumAllowedBatches[updateLevel] > MAX_BATCHES)
        {
            m_NumAllowedBatches[updateLevel] = MAX_BATCHES;
        }

        m_BatchUpdateInfo[updateLevel].m_CurrentBatchToProcess = INVALID_BATCH_ID;
        m_BatchUpdateInfo[updateLevel].m_NextBatchToProcess    = 0;
        m_BatchUpdateInfo[updateLevel].m_BatchUpdateFrequency = minimumUpdateFrequency;

        unsigned currTime = netInterface::GetSynchronisationTime();

        for(unsigned batchIndex = 0; batchIndex < m_NumAllowedBatches[updateLevel]; batchIndex++)
        {
            m_BatchUpdateInfo[updateLevel].m_NextBatchUpdateTime[batchIndex] = currTime + (batchIndex * MIN_BATCH_UPDATE_INTERVAL);
        }
    }
}

} // namespace rage
