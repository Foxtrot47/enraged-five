//
// netobjectmgrbase.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "fwnet/netobjectmgrbase.h"

#include "fwsys/timer.h"
#include "system/param.h"
#include "system/threadtype.h"

#include "fwnet/netinterface.h"
#include "fwnet/netbandwidthmgr.h"
#include "fwnet/neteventtypes.h"
#include "fwnet/netlogsplitter.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netgameobjectbase.h"
#include "fwnet/netobject.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

PARAM(additionalBandwidthLogging, "[network] Log additional information about bandwidth usage");

namespace rage
{

netObjectMgrBase::netObjectMgrBase(netBandwidthMgr& bandwidthMgr) :
m_IsInitialized(false)
, m_ShuttingDown(false)
, m_Log(0)
, m_MessageHandler(0)
, m_BandwidthMgr(bandwidthMgr)
, m_NumUpdatesSent(0)
, m_PlayersToUpdate(0)
, m_bAllowOtherThreadsAccess(false)
BANK_ONLY(, m_LastSyncTreeUpdateDuration(0))
BANK_ONLY(, ms_NumGetNetworkObjectCalls(0))
ASSERT_ONLY(, m_AssertOnUnregister(false))
{
}

netObjectMgrBase::~netObjectMgrBase()
{
    Shutdown();
}

bool netObjectMgrBase::sm_forceCrashOnShutdownWhileProcessing = false;
static const unsigned int NUM_SYNC_MESSAGES_INFO_HELD_FOR = 30;

FW_INSTANTIATE_CLASS_POOL(netObjectMgrBase::syncMessageInfo, (NUM_SYNC_MESSAGES_INFO_HELD_FOR * MAX_NUM_PHYSICAL_PLAYERS), atHashString("syncMessageInfo",0xf5cad027));
FW_INSTANTIATE_CLASS_POOL(netObjectMgrBase::atDNetObjectNode, MAX_NUM_NETOBJECTS, atHashString("atDNetObjectNode",0x49fbe5b4));
FW_INSTANTIATE_CLASS_POOL(netObjectMgrBase::syncMessageInfoNode, 60000, atHashString("syncMessageInfoNode",0xb5424b9c));

bool netObjectMgrBase::Init()
{
    bool success = false;

    if(AssertVerify(!m_IsInitialized))
    {
		const unsigned LOG_BUFFER_SIZE = 16 *1024;
		static bool s_HasCreatedLog = false; 
		m_Log = rage_new netLog("ObjectManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                     netInterface::GetDefaultLogFileBlockingMode(), LOG_BUFFER_SIZE);
		s_HasCreatedLog = true; 
		
        m_aSyncMessageInfo.Resize(MAX_NUM_PHYSICAL_PLAYERS);

        // make sure the network object map always comes out of the game heap (so if it grows while the network heap
        // is inactive it doesn't cause problems)
        sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
		sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

        m_NetworkObjectMap.Reset();
        m_NetworkObjectMap.Reserve(MAX_NUM_NETOBJECTS);

        sysMemAllocator::SetCurrent(*oldHeap);

        // allocate the pools out of the network heap
        syncMessageInfo::InitPool( MEMBUCKET_NETWORK );
        atDNetObjectNode::InitPool( MEMBUCKET_NETWORK );
        syncMessageInfoNode::InitPool( MEMBUCKET_NETWORK );

        m_ObjectIDMgr.Init(m_Log);

        m_ObjectReassignMgr.Init(*this);

        NETWORK_QUITF(m_MessageHandler == 0, "Message handler is already initialised!");
        m_MessageHandler = rage_new netObjectMgrMessageHandler;

        m_MessageHandler->Init(*this);

        m_NumUpdatesSent  = 0;
        m_PlayersToUpdate = 0;

		m_bAllowOtherThreadsAccess = false;

        success = m_IsInitialized = true;
    }

    return success;
}

void netObjectMgrBase::Shutdown()
{
    if(m_IsInitialized)
    {
        m_ShuttingDown = true;

        // remove object lists
        for (unsigned i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
        {
            // remove all objects for the player
            atDNode<netObject*, datBase> *pNode = m_aPlayerObjectList[i].GetHead();

            while (pNode)
            {
                UnregisterNetworkObject(pNode->Data, SHUTTING_DOWN, true, false);
                pNode = m_aPlayerObjectList[i].GetHead();
            }
        }

        // make sure sync message info queue is empty (objects should be removed as they are unregistered)
        for (unsigned player=0; player<MAX_NUM_PHYSICAL_PLAYERS; player++)
        {
            while(!m_aSyncMessageInfo[player].empty())
            {
                syncMessageInfo *syncInfo = m_aSyncMessageInfo[player].front();

                Assert(syncInfo);

                if(syncInfo)
                {
                    syncMessageInfoNode *currentNode = syncInfo->m_ObjectList;

                    while (currentNode)
                    {
                        // we have to manually flush this log file before this assert as this isn't
                        // done automatically when the network game is shutting down
                        if(m_Log)
                        {
                            m_Log->Flush(true);
                        }

						Assertf(0, "Sync message info queue has not been emptied on shutdown: object %d", currentNode->m_Data.m_ObjectID);
                        syncMessageInfoNode *nextNode = currentNode->m_Next;
                        delete currentNode;
                        currentNode = nextNode;
                    }

                    syncInfo->m_ObjectList = 0;
                }

                m_aSyncMessageInfo[player].pop_front();
                delete syncInfo;
            }
        }

        ASSERT_ONLY(ValidateSyncMessageInfos());

        // make sure the network object map is freed out of the game heap
        sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
		sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

        m_NetworkObjectMap.Reset();
        BANK_ONLY(CheckNetworkObjectMapForDuplicates("After Reset"));

        sysMemAllocator::SetCurrent(*oldHeap);

        m_MessageHandler->Shutdown();
        delete m_MessageHandler;
        m_MessageHandler = 0;

        m_ObjectReassignMgr.Shutdown();

        m_ObjectIDMgr.Shutdown();

        // the pools are allocated out of the network heap
        syncMessageInfo::ShutdownPool();
        atDNetObjectNode::ShutdownPool();
        syncMessageInfoNode::ShutdownPool();

        delete m_Log;
        m_Log = 0;

        m_ShuttingDown = false;
        m_IsInitialized = false;
    }
}

void netObjectMgrBase::Update(bool bUpdateNetworkObjects)
{
    netTimeBarWrapper timeBarWrapper(netInterface::ShouldUpdateTimebars());
    timeBarWrapper.StartTimer("Update network objects");

#if __BANK
    ms_AverageGetNetworkObjectCalls.AddSample(static_cast<float>(ms_NumGetNetworkObjectCalls));
    ms_NumGetNetworkObjectCalls = 0;
#endif // __BANK

   // update all network objects
    if (bUpdateNetworkObjects)
    {
        UpdateAllNetworkObjects();

        timeBarWrapper.StartTimer("Update all objects for remote players");

        m_NumUpdatesSent  = 0;
        m_PlayersToUpdate = ~0u;

        unsigned                 numLocalPlayers = netInterface::GetPlayerMgr().GetNumLocalPhysicalPlayers();
        const netPlayer * const *localPlayers    = netInterface::GetPlayerMgr().GetLocalPhysicalPlayers();

        for(unsigned index = 0; index < numLocalPlayers; index++)
        {
            const netPlayer *sourcePlayer = localPlayers[index];

            if(sourcePlayer)
            {
                UpdateAllObjectsForRemotePlayers(*sourcePlayer);
            }
        }
    }

    timeBarWrapper.StartTimer("Object Manager Misc");

    m_ObjectIDMgr.Update();

    m_ObjectReassignMgr.Update();

    m_MessageHandler->Update();

    // remove any unregistered objects that have become network clones
    RemoveUnregisteringClones();

    // DAN TEMP - extra validation for bad object ID behaviour
    BANK_ONLY(CheckNetworkObjectMapForDuplicates("Object Manager Update"));
}

void netObjectMgrBase::UnregisterObject(netGameObjectBase* pGameObject, bool bForce)
{
    USE_MEMBUCKET(MEMBUCKET_NETWORK);
    Assert(pGameObject->GetNetworkObject());
    UnregisterNetworkObject(pGameObject->GetNetworkObject(), GAME_UNREGISTRATION, bForce, false);
}

void netObjectMgrBase::RegisterObject(netGameObjectBase* pGameObject,
                                       const ObjectId   &objectID,
                                       const NetObjFlags localFlags,
                                       const NetObjFlags globalFlags)
{
    Assert(!pGameObject->GetNetworkObject());

    PhysicalPlayerIndex playerIndex = netInterface::GetLocalPhysicalPlayerIndex();

	gnetAssert(playerIndex != INVALID_PLAYER_INDEX);

    // create the network object corresponding to this game object
    netObject* pNetObj = pGameObject->CreateNetworkObject( objectID,
                                                           playerIndex,
                                                           localFlags,
                                                           globalFlags,
                                                           MAX_NUM_PHYSICAL_PLAYERS);
    RegisterNetworkObject(pNetObj);

    Assert(pGameObject->GetNetworkObject());
}

netObject* netObjectMgrBase::GetNetworkObject(const ObjectId objectID, bool includeAll)
{
    gnetAssertf(sysThreadType::IsUpdateThread() || m_bAllowOtherThreadsAccess, "Calling object manager functions from outside the main thread!");

    if (!m_IsInitialized || objectID == NETWORK_INVALID_OBJECT_ID)
    {
        return 0;
    }

    BANK_ONLY(ms_NumGetNetworkObjectCalls++);

    // sort the network object map if it is no longer sorted
    if(!m_NetworkObjectMap.IsSorted())
    {
		gnetAssertf(sysThreadType::IsUpdateThread(), "sorting network map from outside the main thread!");
        m_NetworkObjectMap.FinishInsertion();
        BANK_ONLY(CheckNetworkObjectMapForDuplicates("GetNetworkObject"));
    }

    netObject **ppNetObject = m_NetworkObjectMap.SafeGet(objectID);

    netObject *pObject = ppNetObject ? *ppNetObject : m_ObjectReassignMgr.FindNetworkObject(objectID);

    if(pObject)
    {
        if (includeAll || !pObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
        {
            gnetAssertf(GetNetworkObjectSlow(objectID, includeAll) == pObject, "Network object on the player lists doesn't match network object in the binary map! (%d->%p:%p)", objectID, GetNetworkObjectSlow(objectID, includeAll), pObject);
            return pObject;
        }
    }

    gnetAssertf(GetNetworkObjectSlow(objectID, includeAll) == 0, "Found network object on the player lists but not the binary map! %d:%p", objectID, GetNetworkObjectSlow(objectID, includeAll));
    return 0;
}

netObject* netObjectMgrBase::GetNetworkObjectSlow(const ObjectId objectID, bool includeAll)
{
    netObject* pObject;

    if (!m_IsInitialized || objectID == NETWORK_INVALID_OBJECT_ID)
    {
        return NULL;
    }

    BANK_ONLY(ms_NumGetNetworkObjectCalls++);

    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *player = allPhysicalPlayers[index];

        pObject = GetNetworkObjectFromPlayer(objectID, *player, includeAll);

        if (pObject)
        {
            return pObject;
        }
    }

    return m_ObjectReassignMgr.FindNetworkObject(objectID);
}

netObject* netObjectMgrBase::GetNetworkObjectFromPlayer(const ObjectId objectID, const netPlayer& player, bool includeAll)
{
    if(gnetVerifyf(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Trying to get a network object from a player with an invalid physical player index!"))
	{
        const atDNode<netObject*, datBase> *pNode = m_aPlayerObjectList[player.GetPhysicalPlayerIndex()].GetHead();

        while (pNode)
        {
	        netObject *pObject = pNode->Data;

	        if (pObject->GetObjectID() == objectID)
	        {
                if (includeAll || !pObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
                {
		            return pObject;
                }
	        }

	        pNode = pNode->GetNext();
        }
    }

    return 0;
}

void netObjectMgrBase::ChangeOwner(netObject& object, const netPlayer& player, eMigrationType migrationType)
{
    bool bWasClone = object.IsClone();

	atDNode<netObject*, datBase> *objectNode = 0;

	if (migrationType == MIGRATE_REASSIGNMENT)
	{
		Assert(!object.GetPlayerOwner());

		// create a new list node for this object
		objectNode = rage_new netObjectMgrBase::atDNetObjectNode;

		if(gnetVerifyf(objectNode, "Failed to allocate a new object node!"))
		{
			objectNode->Data = &object;
			object.SetListNode(objectNode);
		}
		else
		{
			return;
		}
	}
	else
	{
		if (object.GetPlayerOwner() == &player)
		{
			NetworkLogUtils::WriteLogEvent(GetLog(), "CHANGE_OWNER_FAILED!", "new owner already owns object");
			GetLog().WriteDataValue("OBJECT", object.GetLogName());
			GetLog().WriteDataValue("PLAYER", player.GetLogName());
			return;
		}

		if (object.IsLocalFlagSet(netObject::LOCALFLAG_BEINGREASSIGNED))
		{
			NetworkLogUtils::WriteLogEvent(GetLog(), "CHANGE_OWNER_FAILED!", "the object is being reassigned");
			GetLog().WriteDataValue("OBJECT", object.GetLogName());
			GetLog().WriteDataValue("PLAYER", player.GetLogName());
			return;
		}

		// remove object from previous owner's list
		if(gnetVerifyf(object.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Changing owner of a network object without an owner!"))
		{
			atDNode<netObject*, datBase> *node = m_aPlayerObjectList[object.GetPhysicalPlayerIndex()].GetHead();

			while(node)
			{
				if (&object == node->Data)
				{
					objectNode = node;
					BANK_ONLY(AssertObjectOnList(m_aPlayerObjectList[object.GetPhysicalPlayerIndex()], object));
					m_aPlayerObjectList[object.GetPhysicalPlayerIndex()].PopNode(*objectNode);
					break;
				}

				node = node->GetNext();
			}
		}

		DEV_ONLY(ValidateObjectLists());

#if __ASSERT
		if(objectNode == 0)
		{
			netPlayer *pPlayer = object.GetPlayerOwner();

			if (AssertVerify(pPlayer))
			{
				Assertf(0, "%s is not on %s's object list!", object.GetLogName(), pPlayer->GetLogName());
			}

			NetworkLogUtils::WriteLogEvent(GetLog(), "CHANGE_OWNER_FAILED!", "the object is not on the previous owner's list");
			GetLog().WriteDataValue("OBJECT", object.GetLogName());
			GetLog().WriteDataValue("PLAYER", player.GetLogName());
			return;
		}
#endif
	}

	if (gnetVerifyf(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Changing owner of a network object to a player with an invalid physical player index!"))
	{
		// add to new owners list
		m_aPlayerObjectList[player.GetPhysicalPlayerIndex()].Append(*objectNode);

		if(player.IsLocal())
		{
            // add to the very low update level initially, we can't calculate the minimum
            // update level while this object is still a clone (before ChangeOwner is called).
            // The object will be moved to the correct update level in the next update
            u8 batch = object.GetSyncTree()->GetBatchForNewObject(CNetworkSyncDataULBase::UPDATE_LEVEL_VERY_LOW);

            object.SetSyncTreeUpdateLevelAndBatch(CNetworkSyncDataULBase::UPDATE_LEVEL_VERY_LOW, batch);

            object.GetSyncTree()->IncreaseObjectCount(CNetworkSyncDataULBase::UPDATE_LEVEL_VERY_LOW, batch);

            // reset the last sync tree update frame to prevent asserts immediately after migration
            DEV_ONLY(object.SetLastSyncTreeUpdateFrame(fwTimer::GetFrameCount()));
        }
		else if(!bWasClone)
        {
            object.GetSyncTree()->DecreaseObjectCount(object.GetSyncTreeUpdateLevel(), object.GetSyncTreeUpdateBatch());
        }
	}

    DEV_ONLY(ValidateObjectLists());

    object.ChangeOwner(player, migrationType);
	object.PostMigrate(migrationType);

	if (!object.IsClone())
	{
		if (object.HasGameObject() && object.GetSyncData())
		{
			ActivationFlags actFlags = object.GetActivationFlags();
			object.GetSyncTree()->Update(&object, actFlags, netInterface::GetSynchronisationTime());
		}
	}

	Assert(GetNetworkObjectFromPlayer(object.GetObjectID(), player, true));

    // if the object has become a clone we need to remove its sync message info
	if (!bWasClone && object.IsClone())
	{
		RemoveObjectFromSyncMessageInfoQueues(object.GetObjectID());
	}
	else if (bWasClone && !object.IsClone())
	{
		unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
		const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

		for(unsigned index = 0; index < numPhysicalPlayers; index++)
		{
			const netPlayer *player = allPhysicalPlayers[index];

			bool pendingCloning = object.IsPendingCloning(*player);
			bool pendingRemoval = object.IsPendingRemoval(*player);
			Assert(!(pendingCloning && pendingRemoval));

			if(pendingCloning || pendingRemoval)
			{
				if(player->IsMyPlayer())
				{
					object.SetPendingCloning(*player, false);
					object.SetPendingRemoval(*player, false);
				}
				else
				{
					if(pendingCloning)
					{
                        u8 buffer[MAX_MESSAGE_PAYLOAD_BYTES];
                        datBitBuffer createDataBuffer;
                        createDataBuffer.SetReadWriteBits(buffer, MAX_MESSAGE_PAYLOAD_BITS, 0);
						CloneObject(&object, *player, createDataBuffer);
					}
					else if(pendingRemoval)
					{
						RemoveClone(&object, *player, true);
					}
				}
			}
		}
	}

    Assert(GetNetworkObjectFromPlayer(object.GetObjectID(), player, true));

    DEV_ONLY(ValidateObjectLists());
}


void netObjectMgrBase::RegisterNetworkObject(netObject* pNetObj)
{
    gnetAssert(pNetObj);
    gnetAssert(pNetObj->GetListNode() == 0);
    gnetAssert(pNetObj->GetPlayerOwner());
    NETWORK_QUITF(GetNetworkObject(pNetObj->GetObjectID(), true)==0, "Trying to register a network object with a network id (%p:%d) that already exists", GetNetworkObject(pNetObj->GetObjectID(), true), pNetObj->GetObjectID());

    // add this object to the list for the relevant player
    atDNode<netObject*, datBase>* pNode = rage_new atDNetObjectNode;
    gnetAssert(pNode);
    pNode->Data = pNetObj;

	gnetAssert(pNetObj->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX);
	if (pNetObj->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
	{
		 m_aPlayerObjectList[pNetObj->GetPhysicalPlayerIndex()].Append(*pNode);

        if(!pNetObj->IsClone())
        {
            u32 updateLevel = pNetObj->GetMinimumUpdateLevel();

            if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Registering a network object with an invalid update level!"))
            {
                u8 batch = pNetObj->GetSyncTree()->GetBatchForNewObject(updateLevel);
                pNetObj->SetSyncTreeUpdateLevelAndBatch(static_cast<u8>(updateLevel), batch);

                pNetObj->GetSyncTree()->IncreaseObjectCount(updateLevel, batch);

                // reset the last sync tree update frame to prevent asserts immediately after migration
                DEV_ONLY(pNetObj->SetLastSyncTreeUpdateFrame(fwTimer::GetFrameCount()));
            }
        }
	}
    pNetObj->SetListNode(pNode);

    gnetAssertf(!m_NetworkObjectMap.Has(pNetObj->GetObjectID()), "Inserting an object ID into the map that already exists!");
    m_NetworkObjectMap.Insert(pNetObj->GetObjectID(), pNetObj);

    DEV_ONLY(ValidateObjectLists());
    BANK_ONLY(CheckNetworkObjectMapForDuplicates("After Insert"));

    // create a blender for the network object
	if (!pNetObj->GetNetBlender())
	{
		pNetObj->CreateNetBlender();
	}

    NetworkLogUtils::WritePlayerText(GetLog(), pNetObj->GetPhysicalPlayerIndex(), "REGISTERING_OBJECT", pNetObj->GetLogName());

	if (pNetObj->GetEntity())
	{
		GetLog().WriteDataValue("Entity", "0x%p", pNetObj->GetEntity());
	}

#if __ASSERT
    // we assert when updating network objects via the sync tree that the current batches have
    // been ticked this frame, which may not be the case when updating objects immediately after
    // registration, so we need to disable the assert
    if(pNetObj->GetSyncTree())
    {
        pNetObj->GetSyncTree()->SetIgnoreBatchAssert(true);
    }
#endif // __ASSERT

	if(!pNetObj->IsClone())
    {
		// starts synchronising the object immediately otherwise it will start a frame later and any changes to the object
		// during that frame will get lost
		if (pNetObj->CanSynchronise(true))
		{
			pNetObj->StartSynchronising();
		}
 
		// update the object scope state immediately so there is not a delay before the object is cloned on other machines
        UpdateAllInScopeStateImmediately(pNetObj); 
	}

#if __ASSERT
    if(pNetObj->GetSyncTree())
    {
        pNetObj->GetSyncTree()->SetIgnoreBatchAssert(false);
    }
#endif // __ASSERT

	pNetObj->OnRegistered();
}

void netObjectMgrBase::UnregisterNetworkObject(netObject* pNetObj, unsigned reason, bool bForce, bool destroyObject)
{
    ASSERT_ONLY(gnetAssertf(!m_AssertOnUnregister, "A network object is being unregistered at an unexpected time!");)

    bool bDestroyNow = (pNetObj->IsClone() || bForce);

    Assert(pNetObj);

    NetworkLogUtils::WritePlayerText(GetLog(), pNetObj->GetPhysicalPlayerIndex(), "UNREGISTERING_OBJECT", pNetObj->GetLogName());
    GetLog().WriteDataValue("Reason", GetUnregistrationReason(reason));

#if ENABLE_NETWORK_LOGGING
	unsigned cantDeleteReason = 0;
    gnetAssertf(bForce || pNetObj->CanDelete(&cantDeleteReason), "Trying to delete %s which cannot be deleted! We are not forcibly deleting this object. Reason: %s", pNetObj->GetLogName(), pNetObj->GetCannotDeleteReason(cantDeleteReason));
#endif // ENABLE_NETWORK_LOGGING

    pNetObj->OnUnregistered();

    // clear the pointers between this network object and its game object and delete the
    // game object if required
    if (pNetObj->HasGameObject())
    {
        NetworkLogUtils::WritePlayerText(GetLog(), pNetObj->GetPhysicalPlayerIndex(), "CLEANING_UP_OBJECT", pNetObj->GetLogName());

        pNetObj->CleanUpGameObject(pNetObj->IsClone() || destroyObject);
        Assert(!pNetObj->HasGameObject());
    }

    if (pNetObj->IsLocalFlagSet(netObject::LOCALFLAG_BEINGREASSIGNED))
    {
        GetLog().Log("\t\tObject is being reassigned, cannot be destroyed now\r\n");

        pNetObj->SetLocalFlag(netObject::LOCALFLAG_UNREGISTERING, true);
        m_ObjectReassignMgr.RemoveObject(*pNetObj);
        bDestroyNow = false;
    }
    else if (!pNetObj->IsClone())
    {
        // flag object to clean itself up (remove all its clones first)
        pNetObj->SetLocalFlag(netObject::LOCALFLAG_UNREGISTERING, true);

        // can destroy this object now if it is not cloned on another machine
        if (!pNetObj->IsPendingCloning() &&
            !pNetObj->HasBeenCloned() &&
            !pNetObj->IsPendingRemoval())
        {
            bDestroyNow = true;
        }
    }

    if (!pNetObj->IsClone())
    {
        RemoveObjectFromSyncMessageInfoQueues(pNetObj->GetObjectID());
    }

    if (bDestroyNow)
    {
        // can just destroy the network object now
        DestroyNetworkObject(pNetObj, bForce);
    }
}

void netObjectMgrBase::DestroyNetworkObject(netObject* pNetObj, bool ASSERT_ONLY(bForce))
{
	if (AssertVerify(pNetObj) && AssertVerify(!pNetObj->IsLocalFlagSet(netObject::LOCALFLAG_BEINGREASSIGNED)))
	{
		NetworkLogUtils::WritePlayerText(GetLog(), pNetObj->GetPhysicalPlayerIndex(), "DESTROYING_OBJECT", pNetObj->GetLogName());

		if (!pNetObj->IsClone())
		{
			Assert(bForce || pNetObj->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING));

			// double check object is properly unregistered
			if (!m_ShuttingDown)
			{
				Assert (!pNetObj->HasBeenCloned());
				Assert (!pNetObj->IsPendingCloning());
				Assert (!pNetObj->IsPendingRemoval());
			}
		}

		// release this objects ID
		if(!pNetObj->IsClone() && !pNetObj->IsPendingOwnerChange())
		{
			GetObjectIDManager().FreeObjectId(pNetObj->GetObjectID());
		}

		// remove this object from the player's list, we should only be destroying objects on a
        // valid players list unless we are shutting down, in which case the object reassignment
        // manager can destroy objects on the reassignment list
        atDList<netObject*, datBase> *listToRemoveFrom = 0;

        PhysicalPlayerIndex playerIndex = pNetObj->GetPhysicalPlayerIndex();
	    if (AssertVerify(playerIndex != INVALID_PLAYER_INDEX || m_ShuttingDown))
	    {
            if(playerIndex != INVALID_PLAYER_INDEX)
            {
		        listToRemoveFrom = &m_aPlayerObjectList[playerIndex];
            }
        }

        if(!pNetObj->IsClone())
        {
            u32 updateLevel = pNetObj->GetSyncTreeUpdateLevel();

            if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Unregistering a network object with an invalid update level!"))
            {
                pNetObj->GetSyncTree()->DecreaseObjectCount(updateLevel, pNetObj->GetSyncTreeUpdateBatch());
            }
        }

	    atDNode<netObject*, datBase> *pNode = pNetObj->GetListNode();

	    if (pNode)
	    {
            if(listToRemoveFrom)
            {
                BANK_ONLY(AssertObjectOnList(*listToRemoveFrom, *pNetObj));
		        listToRemoveFrom->PopNode(*pNode);
            }
		    delete pNode;
		    pNetObj->SetListNode(0);

            DEV_ONLY(ValidateObjectLists());
	    }

        int        indexRemoved = -1;
#if __BANK
        ObjectId   priorKey     = NETWORK_INVALID_OBJECT_ID;
        netObject *priorValue   = 0;
        ObjectId   removeKey     = NETWORK_INVALID_OBJECT_ID;
        netObject *removeValue   = 0;
        ObjectId   nextKey      = NETWORK_INVALID_OBJECT_ID;
        netObject *nextValue    = 0;
#endif // __BANK

        BANK_ONLY(CheckNetworkObjectMapForDuplicates("Destroy Before Sort"));
        m_NetworkObjectMap.FinishInsertion();
        BANK_ONLY(CheckNetworkObjectMapForDuplicates("Destroy After Sort"));

        netObject **ppObject = m_NetworkObjectMap.SafeGet(pNetObj->GetObjectID());
        if(ppObject)
        {
            indexRemoved = m_NetworkObjectMap.GetIndexFromDataPtr(ppObject);

#if __BANK
            atArray<atBinaryMap<netObject *, ObjectId>::DataPair> &rawData = m_NetworkObjectMap.GetRawDataArray();

            if(indexRemoved > 0)
            {
                priorKey   = rawData[indexRemoved-1].key;
                priorValue = rawData[indexRemoved-1].data;
            }

            removeKey   = rawData[indexRemoved].key;
            removeValue = rawData[indexRemoved].data;

            if(indexRemoved < rawData.GetCount()-1)
            {
                nextKey   = rawData[indexRemoved+1].key;
                nextValue = rawData[indexRemoved+1].data;
            }
#endif // __BANK

            m_NetworkObjectMap.Remove(indexRemoved);

            BANK_ONLY(CheckNetworkObjectMapForDuplicates("After Remove"));
        }
        else
        {
            gnetError("Failed to find object ID in network map for removal!");
        }

#if __BANK
        if(!m_ShuttingDown)
        {
            if(GetNetworkObject(pNetObj->GetObjectID(), true) != 0)
            {
                gnetDebug2("Failed to remove network object properly!");
                gnetDebug2("Index-1 before removal was %d, key:%d, value: %p", indexRemoved-1, priorKey,  priorValue);
                gnetDebug2("Index   before removal was %d, key:%d, value: %p", indexRemoved,   removeKey, removeValue);
                gnetDebug2("Index+1 before removal was %d, key:%d, value: %p", indexRemoved+1, nextKey,   nextValue);
                gnetDebug2("Dumping binary map contents:");

                atArray<atBinaryMap<netObject *, ObjectId>::DataPair> &rawData = m_NetworkObjectMap.GetRawDataArray();

                gnetDebug2("pointer to pointer was %p", ppObject);
                gnetDebug2("start of raw data is %p",   (char *)rawData.GetElements());

                for(int index = 0; index < rawData.GetCount(); index++)
                {
                    gnetDebug2("Index: %d, Key: %d, Value: %p", index, rawData[index].key, rawData[index].data);
                }

                NETWORK_QUITF(0, "Failed to remove network object properly!");
            }
        }
#endif // __BANK

		delete pNetObj;
	}
}

void netObjectMgrBase::PlayerHasJoined(const netPlayer& player)
{
	NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_JOINED", "%s", player.GetLogName());

    atDNode<netObject*, datBase> *pNode;
    netObject* pNetObj;

    // inform all the objects
    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *pPlayer = allPhysicalPlayers[index];

        if (pPlayer != &player)
        {
            pNode = m_aPlayerObjectList[pPlayer->GetPhysicalPlayerIndex()].GetHead();

            while (pNode)
            {
                pNetObj = pNode->Data;
                pNetObj->PlayerHasJoined(player);
                pNode = pNode->GetNext();
            }
        }
    }

    m_ObjectIDMgr.PlayerHasJoined(player);
    m_ObjectReassignMgr.PlayerHasJoined(player);
    m_MessageHandler->PlayerHasJoined(player);
}

void netObjectMgrBase::PlayerHasLeft(const netPlayer& player)
{
	NetworkLogUtils::WriteLogEvent(GetLog(), "PLAYER_HAS_LEFT", "%s", player.GetLogName());

    atDNode<netObject*, datBase> *pNode;
    netObject* pNetObj;

    // update the clones from our player lists
    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *pPlayer     = allPhysicalPlayers[index];
        bool             playerLocal = pPlayer->IsLocal();

        pNode = m_aPlayerObjectList[pPlayer->GetPhysicalPlayerIndex()].GetHead();

        while (pNode)
        {
            pNetObj = pNode->Data;
            pNetObj->PlayerHasLeft(player);
            pNode = pNode->GetNext();

            if(playerLocal)
            {
                // if any of our objects are unregistering, see if they can be destroyed now
                CheckForUnregistrationProcessComplete(pNetObj);
            }
        }
    }

    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	if (gnetVerify(playerIndex != INVALID_PLAYER_INDEX))
	{
		// remove sync message info for this player
		SyncMessageInfoQueue &messageInfoQueue = m_aSyncMessageInfo[playerIndex];

		while(!messageInfoQueue.empty())
		{
			syncMessageInfo *messageInfo = messageInfoQueue.front();
			Assert(messageInfo);

			if(messageInfo)
			{
				messageInfo->m_MessageSeqNum = 0;

				syncMessageInfoNode *currentNode = messageInfo->m_ObjectList;
                while(currentNode)
				{
                    syncMessageInfoNode *nextNode = currentNode->m_Next;
					delete currentNode;
                    currentNode = nextNode;
				}
                messageInfo->m_ObjectList = 0;
			}

			messageInfoQueue.pop_front();
			delete messageInfo;
		}
	}

    ASSERT_ONLY(ValidateSyncMessageInfos());

    m_ObjectIDMgr.PlayerHasLeft(player);
    m_ObjectReassignMgr.PlayerHasLeft(player);
    m_MessageHandler->PlayerHasLeft(player);
}

void netObjectMgrBase::CloneObject(netObject* pObject, const netPlayer& player, datBitBuffer &createDataBuffer)
{
    Assert(!pObject->HasBeenCloned(player));
    Assert(!pObject->IsPendingRemoval(player));

    unsigned resultCode = 0;

    if (!pObject->CanClone(player, &resultCode))
    {
        NetworkLogUtils::WriteLogEvent(GetLog(), "CANNOT_CLONE", "%s", pObject->GetLogName());
        GetLog().WriteDataValue("Player", "%s", player.GetLogName());
        GetLog().WriteDataValue("Reason", "%s", pObject->GetCanCloneErrorString(resultCode));
        return;
    }

	if (!AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		return;
	}

    if(m_MessageHandler->GetLog())
    {
        NetworkLogUtils::WriteLogEvent(*m_MessageHandler->GetLog(), "PACKING_CLONE_CREATE", "%s\r\n", pObject->GetLogName());

        m_MessageHandler->GetLog()->WriteDataValue("Timestamp", "%d", netInterface::GetTimestampForPositionUpdates());
    }

    // check if the create data is already available
    if(createDataBuffer.GetCursorPos() == 0)
    {
        ActivationFlags actFlags = pObject->GetActivationFlags();

	    // cache this here so that all nodes get updated with the same time
	    unsigned syncTime = netInterface::GetSynchronisationTime();

	    // force an update of the tree so that the create contains the most up to date state of the object
	    if (pObject->GetSyncData())
	    {
		    pObject->GetSyncTree()->Update(pObject, pObject->GetActivationFlags(), syncTime);
	    }
        else 
        {
            pObject->StartSynchronising();
        }

        pObject->GetSyncTree()->Write(SERIALISEMODE_CREATE, actFlags, pObject, createDataBuffer, syncTime, m_MessageHandler->GetLog(), player.GetPhysicalPlayerIndex());

#if __DEV
        // ensure the data is readable via the sync tree
        createDataBuffer.SetCursorPos(0);
        pObject->GetSyncTree()->Read(SERIALISEMODE_CREATE, actFlags, createDataBuffer, NULL);
#endif
    }

    if (m_BandwidthMgr.CanSendData(player, createDataBuffer.GetCursorPos()))
    {
        m_MessageHandler->AddCreate(player, *pObject->GetPlayerOwner(), pObject->GetObjectType(), pObject->GetObjectID(), pObject->GetGlobalFlags(), (u8 *)createDataBuffer.GetReadWriteBits(), createDataBuffer.GetCursorPos());

        // reset the sync failure count for this object
		pObject->ResetStarvationCount(player.GetPhysicalPlayerIndex());

        pObject->SetPendingCloning(player, true);
        pObject->IncCloningTimer(player);

        if (pObject->GetSyncData())
        {
            pObject->GetSyncData()->InitialisePlayerSyncData(player.GetPhysicalPlayerIndex(), true);
            pObject->GetSyncTree()->NodeDataSent(pObject, player.GetPhysicalPlayerIndex(), NULL);
        }

        pObject->LogAdditionalData(*m_MessageHandler->GetLog());
    }
    else
    {
        if(m_MessageHandler->GetLog())
        {
            m_MessageHandler->GetLog()->LineBreak();
            m_MessageHandler->GetLog()->WriteDataValue("FAILED", "Couldn't send data, not enough bandwidth");
        }

        // increment the sync failure count for this object
        ObjectId objectID = pObject->GetObjectID();
        Assert(objectID != NETWORK_INVALID_OBJECT_ID);

		if(objectID != NETWORK_INVALID_OBJECT_ID && AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
        {
            pObject->IncreaseStarvationCount(player.GetPhysicalPlayerIndex());
        }
    }
 }


bool netObjectMgrBase::RemoveClone(netObject* pObject, const netPlayer& player, bool bForceSend)
{
    // don't remove if we are in the process of cloning!
    if (pObject->IsPendingCloning(player))
        return false;

    // don't remove if we are in the process of passing control to another machine
    if (pObject->IsPendingOwnerChange())
        return false;

    unsigned sizeOfRemoveData = packedReliablesMsg::GetRemoveSize();

    if (m_BandwidthMgr.CanSendData(player, sizeOfRemoveData) || bForceSend)
    {
        // send the message across the connection
        m_MessageHandler->AddRemove(player, *pObject->GetPlayerOwner(), pObject->GetObjectID(), pObject->GetOwnershipToken());

        pObject->SetBeenCloned(player, false);
        pObject->SetPendingCloning(player, false);
        pObject->SetPendingRemoval(player, true);

        // log the clone create information in the message log
        if(m_MessageHandler->GetLog())
        {
            NetworkLogUtils::WriteLogEvent(*m_MessageHandler->GetLog(), "PACKING_CLONE_REMOVE", "%s\r\n", pObject->GetLogName());

            if (pObject->HasCloningTimedOut(player))
            {
                netLogSplitter logSplitter(GetLog(), *m_MessageHandler->GetLog());
                logSplitter.WriteDataValue("ERROR", "%s unable to remove itself on %s", pObject->GetLogName(), player.GetLogName());
            }
        }

        pObject->IncCloningTimer(player);

        return true;
    }

    return false;
}

bool netObjectMgrBase::SyncClone(netObject* pObject, const netPlayer& player)
{
    bool          syncDataSent = false;
    DataNodeFlags nodeFlags;
    netSequence   seqNum;
    ObjectId      objectID = pObject->GetObjectID();

    if (!pObject->CanSync(player))
    {
        NetworkLogUtils::WritePlayerText(GetLog(), player.GetPhysicalPlayerIndex(), "CAN'T_SYNC_TO_PLAYER", pObject->GetLogName());
        return false;
    }

    // a buffer used to construct messages
    u8 messageBuffer[MAX_MESSAGE_PAYLOAD_BYTES];

    datBitBuffer msgBuffer;
    const int maxSyncDataSize = cloneSyncMsg::GetMaxSyncDataSize();

    ActivationFlags actFlags = pObject->GetActivationFlags();

	if (!AssertVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		return false;
	}

    m_NumUpdatesSent++;

	// cache this here so that all nodes get updated with the same time
	unsigned syncTime = netInterface::GetSynchronisationTime();

	const unsigned MAX_WRITE_OPERATIONS = 2;
	bool doSyncTreeWrite = true;
	netSyncTree *syncTree = pObject->GetSyncTree();
	if(gnetVerifyf(syncTree, "Trying to send a clone sync for an object (%s) without a valid sync tree!", pObject->GetLogName()))
	{
		for(unsigned index = 0; index < MAX_WRITE_OPERATIONS && doSyncTreeWrite; index++)
		{
			msgBuffer.SetReadWriteBits(messageBuffer, maxSyncDataSize, 0);

#if ENABLE_NETWORK_LOGGING
			int oldCursorPos = msgBuffer.GetCursorPos();
#endif // ENABLE_NETWORK_LOGGING
		
			if (syncTree->Write(SERIALISEMODE_UPDATE, actFlags, pObject, msgBuffer, syncTime, NULL, player.GetPhysicalPlayerIndex(), &nodeFlags))
			{
				// try to do another write operation if the data didn't fit in one sync message
				doSyncTreeWrite = syncTree->HasFailedToWriteAllData();

				if (doSyncTreeWrite)
				{
					gnetDebug1("HasFailedToWriteAllData for object (%s) to %s", pObject->GetLogName(), player.GetLogName());
				}

				if (m_BandwidthMgr.CanSendData(player, msgBuffer.GetCursorPos()))
				{
					bool sendImmediately = ShouldSendCloneSyncImmediately(player, *pObject);

					// send the sync data in an update message
					m_MessageHandler->SendCloneSync(player,
													pObject,
													msgBuffer,
													seqNum,
													sendImmediately);

		#if ENABLE_NETWORK_LOGGING
					if(PARAM_additionalBandwidthLogging.Get() && pObject->GetSyncData())
					{
						if(pObject->GetSyncData()->IsAnyDataUnitFlaggedForForceSend(player.GetPhysicalPlayerIndex()))
						{
							m_MessageHandler->GetLog()->WriteNodeHeader("Additional send info");

							unsigned numSyncTreeNodes = pObject->GetSyncTree()->GetNumSyncUpdateNodes();

							for(unsigned index = 0; index < numSyncTreeNodes; index++)
							{
								if((nodeFlags & (1<<index)) != 0)
								{
									if(pObject->GetSyncData()->IsDataUnitFlaggedForForceSend(player.GetPhysicalPlayerIndex(), index))
									{
										m_MessageHandler->GetLog()->WriteDataValue(pObject->GetSyncTree()->GetNodeName(index), "Forcibly Sent");
									}
								}
							}
						}
					}
		#endif // ENABLE_NETWORK_LOGGING

					// store the sync data so that we can clear the unacked states when a sync ack arrives
					AddNewObjectInfoToSyncMessageInfoQueue(player, seqNum, objectID, nodeFlags);

					// reset the sync failure count for this object
					pObject->ResetStarvationCount(player.GetPhysicalPlayerIndex());

					syncDataSent = true;

		#if ENABLE_NETWORK_LOGGING
					if(m_MessageHandler->GetLog()->IsEnabled())
					{
		#if __ASSERT
						int bitsWritten = msgBuffer.GetCursorPos() - oldCursorPos;
		#endif // __ASSERT
		#if __DEV
						// ensure the data is readable via the sync tree
						msgBuffer.SetCursorPos(oldCursorPos);
						pObject->GetSyncTree()->Read(SERIALISEMODE_VERIFY, actFlags, msgBuffer, NULL);
		#endif
						// disable bandwidth recording while writing the sync tree for logging purposes
						m_MessageHandler->GetBandwidthStatistics().DisableBandwidthRecording();

						// this is required for logging output
						msgBuffer.SetCursorPos(oldCursorPos);
						pObject->GetSyncTree()->Write(SERIALISEMODE_UPDATE, actFlags, pObject, msgBuffer, syncTime, m_MessageHandler->GetLog(), player.GetPhysicalPlayerIndex(), &nodeFlags);

						m_MessageHandler->GetBandwidthStatistics().EnableBandwidthRecording();

						pObject->LogAdditionalData(*m_MessageHandler->GetLog());

						Assert(msgBuffer.GetCursorPos() - oldCursorPos == bitsWritten);
					}
		#endif
				}
				else
				{
					// increment the sync failure count for this object
					pObject->IncreaseStarvationCount(player.GetPhysicalPlayerIndex());

					seqNum = m_MessageHandler->GetSequenceNumberForPlayer(player);
				}

				// register the sequence number of the message with the sync data in the tree
				pObject->GetSyncTree()->NodeDataSent(pObject, player.GetPhysicalPlayerIndex(), &seqNum);

				// If there's nothing left to write we stop.
				// The GetDataSize function is not accurate so currently it can mark nodes as failed even if there's nothing to write
				if (doSyncTreeWrite && pObject->GetSyncData() && !pObject->GetSyncData()->GetPlayerRequiresAnUpdate(player.GetPhysicalPlayerIndex()))
				{
					gnetWarning("HasFailedToWriteAllData but no update is needed for object (%s) to player %s", pObject->GetLogName(), player.GetLogName());
					doSyncTreeWrite = false;
				}
			}
			else
			{
				 doSyncTreeWrite = false;
			}
		}

		 gnetAssertf(!syncTree->HasFailedToWriteAllData() || !doSyncTreeWrite, "Failed to write all data for %s during clone sync after %d iterations!", pObject->GetLogName(), MAX_WRITE_OPERATIONS);
    }

    return syncDataSent;
}

void netObjectMgrBase::UpdateAllInScopeStateImmediately(netObject *networkObject)
{
    if(gnetVerifyf(networkObject, "Passing a NULL network object to be update on all players immediately!"))
    {
        unsigned           numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
	    netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for (unsigned i = 0; i < numRemotePhysicalPlayers; i++)
	    {
            netPlayer *remotePlayer = remotePhysicalPlayers[i];

            networkObject->UpdateInScopeState(*remotePlayer);
	    }
    }
}

void netObjectMgrBase::UpdateObjectOnAllPlayers(netObject *networkObject)
{
    unsigned                 numRemoteActivePlayers = netInterface::GetNumRemoteActivePlayers();
    const netPlayer * const *remoteActivePlayers    = netInterface::GetRemoteActivePlayers();

    u8 buffer[MAX_MESSAGE_PAYLOAD_BYTES];
    datBitBuffer createDataBuffer;
    createDataBuffer.SetReadWriteBits(buffer, MAX_MESSAGE_PAYLOAD_BITS, 0);

    networkObject->SetLastPlayersSyncUpdated(m_PlayersToUpdate);

    for(unsigned index = 0; index < numRemoteActivePlayers; index++)
    {
        const netPlayer &player = *remoteActivePlayers[index];

	    //TODO - this needs handled in a nicer way. We should be able to iterate through all players that are active with a valid physical player index
	    //The physical player iterator can't be used because player peds will never be cloned.
        PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();
	    if((playerIndex != INVALID_PLAYER_INDEX))
	    {
            bool bHasBeenCloned  = networkObject->HasBeenCloned(player);
            bool bPendingCloning = networkObject->IsPendingCloning(player);
            bool bPendingRemoval = networkObject->IsPendingRemoval(player);

            Assert(!(bHasBeenCloned && bPendingCloning));
            Assert(!(bHasBeenCloned && bPendingRemoval));
            Assert(!(bPendingCloning && bPendingRemoval));

            // Check if the object needs to by cloned, synched or removed in respect to the players on the
            // remote player

            // cloning and removal are done reliably so we need to wait for the acks before continuing
            if (!bPendingRemoval && !bPendingCloning)
            {
                if (networkObject->IsLocalFlagSet(netObject:: LOCALFLAG_UNREGISTERING))
                {
                    if (bHasBeenCloned)
                    {
                        RemoveClone(networkObject, player);
                    }
                }
                else
                {
                    if ((networkObject->GetInScopeState() & (1<<player.GetPhysicalPlayerIndex())) != 0)
                    {
				        netSyncDataBase* pObjSyncData = networkObject->GetSyncData();

                        if (!bHasBeenCloned)
                        {
                            // Create a new clone if the clone is in scope with the remote players on the other player.
                            CloneObject(networkObject, player, createDataBuffer);
                        }
                        else if (pObjSyncData && 
						        pObjSyncData->GetPlayerRequiresAnUpdate(player.GetPhysicalPlayerIndex()))
                        {
                            if((m_PlayersToUpdate & (1<<playerIndex)) != 0)
                            {
                                SyncClone(networkObject, player);
                            }
                        }
                    }
                    else if (bHasBeenCloned)
                    {
                        // Remove the clone if it has gone out of scope with the players on the other player
                        RemoveClone(networkObject, player);
                    }
                }
            }
        }
    }
}

int netObjectMgrBase::GetNumLocalObjects(fnNetObjectPredicate predicate) const
{
    int numObjects = 0;

    if(netInterface::GetPlayerMgr().IsInitialized() && gnetVerifyf(netInterface::GetLocalPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Local player has an invalid physical player index!"))
    {
        const atDList<netObject*, datBase> *listsToProcess[MAX_NUM_PHYSICAL_PLAYERS];
        unsigned numListsToProcess = GetLocalObjectLists(listsToProcess);

        for(unsigned index = 0; index < numListsToProcess; index++)
        {
            const atDNode<netObject*, datBase> *node = listsToProcess[index] ? listsToProcess[index]->GetHead() : 0;

            while (node)
            {
                netObject *networkObject = node->Data;

                if(networkObject && (predicate == 0 || (*predicate)(networkObject)))
                {
                    numObjects++;
                }

                node = node->GetNext();
            }
        }
    }

    return numObjects;
}

int netObjectMgrBase::GetNumRemoteObjects(fnNetObjectPredicate predicate) const
{
    int numObjects = 0;

    if (netInterface::GetPlayerMgr().IsInitialized())
    {
        unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

            const atDNode<netObject*, datBase> *node = m_aPlayerObjectList[remotePlayer->GetPhysicalPlayerIndex()].GetHead();

            while (node)
            {
                netObject *networkObject = node->Data;

                if(networkObject && (predicate == 0 || (*predicate)(networkObject)))
                {
                    numObjects++;
                }

                node = node->GetNext();
            }
        }
    }

    return numObjects;
}

int netObjectMgrBase::GetTotalNumObjects(fnNetObjectPredicate predicate) const
{
    int numObjects = 0;

    if(netInterface::GetPlayerMgr().IsInitialized())
    {
        unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
        const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

        for(unsigned index = 0; index < numPhysicalPlayers; index++)
        {
            const netPlayer *pPlayer = allPhysicalPlayers[index];

            if(gnetVerifyf(pPlayer->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Invalid Physical Player Index!"))
            {
                const atDNode<netObject*, datBase> *node = m_aPlayerObjectList[pPlayer->GetPhysicalPlayerIndex()].GetHead();
                
                while(node)
                {
                    netObject *networkObject = node->Data;

                    if(networkObject && (predicate == 0 || (*predicate)(networkObject)))
                    {
                        numObjects++;
                    }

                    node = node->GetNext();
                }
            }
        }

        numObjects += m_ObjectReassignMgr.GetNumObjectsBeingReassigned(predicate);
    }

    return numObjects;
}

void netObjectMgrBase::GetLocallyOwnedObjectsInfo(unsigned int &numLocalNetworkObjects, unsigned int &numTotalSyncTargets) const
{
    numLocalNetworkObjects = 0;
    numTotalSyncTargets    = 0;

    if (!netInterface::GetPlayerMgr().IsInitialized())
        return;

    const atDList<netObject*, datBase> *listsToProcess[MAX_NUM_PHYSICAL_PLAYERS];
    unsigned numListsToProcess = GetLocalObjectLists(listsToProcess);

    for(unsigned index = 0; index < numListsToProcess; index++)
    {
        const atDNode<netObject*, datBase> *node = listsToProcess[index] ? listsToProcess[index]->GetHead() : 0;

        while(node)
        {
            numLocalNetworkObjects++;

            netObject *networkObject = node->Data;

            if(networkObject)
            {
                unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
                const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

                for(unsigned index = 0; index < numPhysicalPlayers; index++)
                {
                    const netPlayer *player = allPhysicalPlayers[index];

                    if(networkObject->HasBeenCloned(*player))
                    {
                        numTotalSyncTargets++;
                    }
                }
            }

            node = node->GetNext();
        }
    }
}

unsigned int netObjectMgrBase::GetAllObjects(netObject **objectArray, unsigned int sizeofArray, bool includeReassignments, bool scriptObjectsOnly, bool includeNoGameObjects)
{
	unsigned int objectCount = 0;

    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *player = allPhysicalPlayers[index];

        atDNode<netObject*, datBase> *node = m_aPlayerObjectList[player->GetPhysicalPlayerIndex()].GetHead();

        while (node)
		{
            netObject *networkObject = node->Data;

			if(networkObject && (networkObject->HasGameObject() || includeNoGameObjects))
			{
				if (!scriptObjectsOnly || networkObject->GetScriptHandlerObject())
				{
					if(gnetVerifyf(objectCount < sizeofArray, "Size of array passed to netObjectMgrBase::GetAllObjects is too small!"))
					{
						objectArray[objectCount] = networkObject;
						objectCount++;
					}
				}
			}

			node = node->GetNext();
		}
    }

	if (includeReassignments)
	{
		objectCount += netInterface::GetObjectManager().GetReassignMgr().GetAllObjectsBeingReassigned(&objectArray[objectCount], sizeofArray-objectCount, scriptObjectsOnly);
	}

	return objectCount;
}

void netObjectMgrBase::GetObjectIDsInUseLocally(ObjectId *idsInUseLocally, unsigned &numInUseIDs, unsigned &numLocallyOwnedIDs, unsigned idStart, unsigned idEnd)
{
    if(gnetVerify(idsInUseLocally) &&
       gnetVerifyf(idStart <= idEnd, "Calling GetObjectIDsInUseLocally with start of range larger than end of range!"))
    {
        numInUseIDs = 0;
        numLocallyOwnedIDs = 0;

        unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
        const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

        for(unsigned index = 0; index < numPhysicalPlayers; index++)
        {
            const netPlayer *player = allPhysicalPlayers[index];

            atDNode<netObject*, datBase> *node = m_aPlayerObjectList[player->GetPhysicalPlayerIndex()].GetHead();

            while (node)
		    {
                netObject *networkObject = node->Data;

		        if (networkObject && networkObject->GetObjectID() >= idStart && networkObject->GetObjectID() < idEnd)
		        {
			        idsInUseLocally[numInUseIDs] = networkObject->GetObjectID();
			        numInUseIDs++;

                    if(!networkObject->IsClone())
                    {
                        numLocallyOwnedIDs++;
                    }
		        }

		        node = node->GetNext();
	        }
        }

        // check any objects currently in the process of being reassigned
        netObject *objectsBeingReassigned[MAX_NUM_NETOBJECTS];
        unsigned numObjectsBeingReassigned = netInterface::GetObjectManager().GetReassignMgr().GetAllObjectsBeingReassigned(objectsBeingReassigned, MAX_NUM_NETOBJECTS, false);

        for(unsigned index = 0; index < numObjectsBeingReassigned; index++)
        {
            netObject *networkObject = objectsBeingReassigned[index];

	        if (networkObject && networkObject->GetObjectID() >= idStart && networkObject->GetObjectID() < idEnd)
	        {
		        idsInUseLocally[numInUseIDs] = networkObject->GetObjectID();
		        numInUseIDs++;
	        }
        }

        // check any object IDs in the process of being freed to another machine
        ObjectId pendingFreedObjectIDs[netObjectIDMgr::MAX_TOTAL_OBJECT_IDS];
        unsigned numPendingFreedObjectIDs = 0;
        objectIdFreedEvent::GetPendingFreedObjectIDs(pendingFreedObjectIDs, numPendingFreedObjectIDs);

        for(unsigned index = 0; index < numPendingFreedObjectIDs; index++)
        {
            ObjectId pendingID = pendingFreedObjectIDs[index];

            if (pendingID >= idStart && pendingID < idEnd)
            {
                idsInUseLocally[numInUseIDs] = pendingID;
                numInUseIDs++;
            }
        }
    }
}

void netObjectMgrBase::ForAllLocalObjects(fnNetObjectCallback callback, void *param)
{
    if(netInterface::GetPlayerMgr().IsInitialized() && gnetVerifyf(netInterface::GetLocalPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Local player has an invalid physical player index!"))
    {
        const atDList<netObject*, datBase> *listsToProcess[MAX_NUM_PHYSICAL_PLAYERS];
        unsigned numListsToProcess = GetLocalObjectLists(listsToProcess);

        for(unsigned index = 0; index < numListsToProcess; index++)
        {
            const atDNode<netObject*, datBase> *node = listsToProcess[index] ? listsToProcess[index]->GetHead() : 0;

            while (node)
            {
                netObject *networkObject = node->Data;

                if(networkObject)
                {
                    (*callback)(networkObject, param);
                }

                node = node->GetNext();
            }
        }
    }
}

void netObjectMgrBase::ForAllRemoteObjects(fnNetObjectCallback callback, void *param)
{
    if (netInterface::GetPlayerMgr().IsInitialized())
    {
        unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

            const atDNode<netObject*, datBase> *node = m_aPlayerObjectList[remotePlayer->GetPhysicalPlayerIndex()].GetHead();

            while (node)
            {
                netObject *networkObject = node->Data;

                if(networkObject)
                {
                    (*callback)(networkObject, param);
                }

                node = node->GetNext();
            }
        }
    }
}

void netObjectMgrBase::ForAllObjects(fnNetObjectCallback callback, void *param)
{
    if(netInterface::GetPlayerMgr().IsInitialized())
    {
        unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
        const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

        for(unsigned index = 0; index < numPhysicalPlayers; index++)
        {
            const netPlayer *pPlayer = allPhysicalPlayers[index];

            if(gnetVerifyf(pPlayer->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Invalid Physical Player Index!"))
            {
                const atDNode<netObject*, datBase> *node = m_aPlayerObjectList[pPlayer->GetPhysicalPlayerIndex()].GetHead();
                
                while(node)
                {
                    netObject *networkObject = node->Data;

                    if(networkObject)
                    {
                        (*callback)(networkObject, param);
                    }

                    node = node->GetNext();
                }
            }
        }
    }
}

void netObjectMgrBase::ForceRemovalOfUnregisteringObject(netObject* pObject)
{
    Assert(pObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING));

    pObject->SetClonedState(0,0,0);
    CheckForUnregistrationProcessComplete(pObject);
}

void netObjectMgrBase::GetLogName(char *logName, size_t logNameSize,const ObjectId objectID)
{
	if (logName && (logNameSize > (size_t) 0))
	{
		if(objectID == NETWORK_INVALID_OBJECT_ID)
		{
			safecpy(logName, "None", logNameSize);
		}
		else
		{
			netObject *networkObject = GetNetworkObject(objectID, true);

			if(networkObject)
			{
				safecpy(logName, networkObject->GetLogName(), logNameSize);
			}
			else
			{
				formatf(logName, logNameSize, "?_%d", objectID);
			}
		}
	}
}

const char* netObjectMgrBase::GetAckCodeName(NetObjectAckCode ackCode) const
{
    switch (ackCode)
    {
    case ACKCODE_SUCCESS:
        return "ACKCODE_SUCCESS";
    case ACKCODE_FAIL:
        return "ACKCODE_FAIL";
    case ACKCODE_OUT_OF_SEQUENCE:
        return "ACKCODE_OUT_OF_SEQUENCE";
    case ACKCODE_WRONG_OWNER:
        return "ACKCODE_WRONG_OWNER";
    case ACKCODE_NO_OBJECT:
        return "ACKCODE_NO_OBJECT";
    case ACKCODE_TOO_MANY_OBJECTS:
        return "ACKCODE_TOO_MANY_OBJECTS";
	case ACKCODE_CANT_APPLY_DATA:
		return "ACKCODE_CANT_APPLY_DATA";
	case ACKCODE_MATCH_NOT_STARTED:
		return "ACKCODE_MATCH_NOT_STARTED";
    case ACKCODE_NONE:
        return "ACKCODE_NONE";
    default:
        Assert(0);
    }

    return NULL;
}


void netObjectMgrBase::CheckForUnregistrationProcessComplete(netObject *pNetworkObject)
{
    if(AssertVerify(pNetworkObject))
    {
        // if an object is unregistering and cleaning up it can destroy itself once all its clones
        // are removed
        if (pNetworkObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
        {
            if (!pNetworkObject->IsPendingCloning() &&
                !pNetworkObject->HasBeenCloned() &&
                !pNetworkObject->IsPendingRemoval())
            {
                DestroyNetworkObject(pNetworkObject);
            }
        }
    }
}

const char *netObjectMgrBase::GetUnregistrationReason(unsigned reason) const
{
    switch(reason)
    {
    case SHUTTING_DOWN:
        return "Object manager shutdown";
    case GAME_UNREGISTRATION:
        return "Game code Unregistration";
    case DELETED_VIA_UPDATE:
        return "Deleting after Update()";
    case REASSIGNMENT_SHUTDOWN:
        return "Reassignment manager shutdown";
    case REASSIGNMENT_NO_REASSIGN:
        return "Object marked to not reassign";
    case REASSIGNMENT_NO_NEW_OWNER:
        return "Object reassigned to non-existant owner";
    case REASSIGNMENT_REMOVAL:
        return "Removed by reassignment process";
    default:
        gnetAssertf(0, "Unknown unregistration reason!");
        return "Unknown";
    }
}

unsigned netObjectMgrBase::GetLocalObjectLists(const atDList<netObject*, datBase> *localObjectLists[MAX_NUM_PHYSICAL_PLAYERS]) const
{
    unsigned numLocalObjectLists = 0;

    PhysicalPlayerIndex localPlayerIndex = netInterface::GetLocalPhysicalPlayerIndex();

    if(localPlayerIndex != INVALID_PLAYER_INDEX)
    {
        localObjectLists[numLocalObjectLists] = &m_aPlayerObjectList[localPlayerIndex];
        numLocalObjectLists++;
    }

#if ENABLE_NETWORK_BOTS
    unsigned                 numLocalPhysicalPlayers = netInterface::GetPlayerMgr().GetNumLocalPhysicalPlayers();
    const netPlayer * const *localPhysicalPlayers    = netInterface::GetPlayerMgr().GetLocalPhysicalPlayers();

    for(unsigned index = 0; index < numLocalPhysicalPlayers; index++)
    {
        const netPlayer *localPlayer = localPhysicalPlayers[index];

        if(localPlayer)
        {
            PhysicalPlayerIndex playerIndex = localPlayer->GetPhysicalPlayerIndex();

            if(playerIndex != INVALID_PLAYER_INDEX && playerIndex != localPlayerIndex)
            {
                if(gnetVerifyf(numLocalObjectLists < MAX_NUM_PHYSICAL_PLAYERS, "Adding too many local object lists!"))
                {
                    localObjectLists[numLocalObjectLists] = &m_aPlayerObjectList[playerIndex];
                    numLocalObjectLists++;
                }
            }
        }
    }
#endif // ENABLE_NETWORK_BOTS

    return numLocalObjectLists;
}

void netObjectMgrBase::UpdateAllNetworkObjects()
{
    netTimeBarWrapper timeBarWrapper(netInterface::ShouldUpdateTimebars());
    timeBarWrapper.StartTimer("Update objects");

    netObject *localObjects[MAX_NUM_NETOBJECTS];
    unsigned numLocalObjects = 0;    

    ASSERT_ONLY(m_AssertOnUnregister = true);

    ObjectId objectsToUnregister[MAX_NUM_NETOBJECTS];
    unsigned numObjectsToUnregister = 0;

    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *player = allPhysicalPlayers[index];

        PhysicalPlayerIndex physicalIndex = player->GetPhysicalPlayerIndex();

        if(gnetVerifyf(physicalIndex != INVALID_PLAYER_INDEX, "Local player has an invalid physical player index!"))
        {
            atDNode<netObject*, datBase> *node = m_aPlayerObjectList[physicalIndex].GetHead();

            while (node)
            {
                netObject *networkObject = node->Data;
                gnetAssertf(node == networkObject->GetListNode(), "Node is pointing to a network object that doesn't point to itself!");
                node = node->GetNext();

                bool addObjectToList = !networkObject->IsClone();

                if (!networkObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
                {
                    if (networkObject->Update())
                    {
                        Assert(!networkObject->IsClone());

                        LOGGING_ONLY(unsigned reason = 0);

                        if(!networkObject->CanDelete(LOGGING_ONLY(&reason)))
                        {
#if ENABLE_NETWORK_LOGGING
                            NetworkLogUtils::WriteLogEvent(GetLog(), "UNREGISTER_AFTER_UPDATE_FAILED", networkObject->GetLogName());
                            GetLog().WriteDataValue("Cannot Delete", "%s", networkObject->GetCannotDeleteReason(reason));
#endif // ENABLE_NETWORK_LOGGING
                        }
                        else
                        {
                            if(gnetVerifyf(numObjectsToUnregister < MAX_NUM_NETOBJECTS, "Unexpected number of object flagged to unregister!"))
                            {
                                objectsToUnregister[numObjectsToUnregister] = networkObject->GetObjectID();
                                numObjectsToUnregister++;
                                addObjectToList = false;
                            }
                        }
                    }
#if __ASSERT
                    if(networkObject->IsClone())
                    {
                        gnetAssertf(!player->IsLocal(), "Clone %s is on the object list for local player %s!", networkObject->GetLogName(), player->GetLogName());

                        if(gnetVerifyf(networkObject->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "A network clone with no owner is on one of the object lists!"))
                        {
                            gnetAssertf(networkObject->GetPhysicalPlayerIndex() == physicalIndex, "A network clone is on the wrong player's object list!");
                        }
                    }
                    else
                    {
                        gnetAssertf(player->IsLocal(), "Local %s is on the object list for remote player %s!", networkObject->GetLogName(), player->GetLogName());
                    }
#endif  //__ASSERT
                }

                if(addObjectToList)
                {
                    NETWORK_QUITF(numLocalObjects < MAX_NUM_NETOBJECTS, "Too many local network objects!");
                    localObjects[numLocalObjects] = networkObject;
                    numLocalObjects++;
                }
            }
        }
    }

    ASSERT_ONLY(m_AssertOnUnregister = false);

    // unregister any objects flagged for removal
    for(unsigned index = 0; index < numObjectsToUnregister; index++)
    {
        // we need to re-lookup the object here as deleting one network object may delete another,
        // which might be in this list. This can happen if network objects are attached to another
        netObject *networkObject = GetNetworkObject(objectsToUnregister[index]);

        if(networkObject && gnetVerifyf(networkObject->CanDelete(), "Trying to unregister %s that cannot be deleted! This should have already been checked!", networkObject->GetLogName()))
        {
            UnregisterNetworkObject(networkObject, DELETED_VIA_UPDATE, false, true);
        }
    }

    timeBarWrapper.StartTimer("Update sync trees");

#if __BANK
    unsigned startTime = sysTimer::GetSystemMsTime();
#endif // __BANK

    // update the sync tree data for any objects owned by local players
    for(unsigned index = 0; index < numLocalObjects; index++)
    {
        netObject *networkObject      = localObjects[index];
        bool       updateLevelChanged = networkObject->GetMinimumUpdateLevel() != networkObject->GetSyncTreeUpdateLevel();

        if ((networkObject->HasGameObject() || networkObject->CanSyncWithNoGameObject()) && gnetVerifyf(!networkObject->IsClone(), "A network clone is on a local player's object list!"))
        {
            // check if this object should be updated this frame - always update in the same frame as the object changes update levels, the update level
            // is only allowed to increase when it is not going to be updated in the current frame and depending on which batch an object is assigned could delays updates too long
            bool updateThisFrame = networkObject->IsInCurrentUpdateBatch() || updateLevelChanged;

            if(!networkObject->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING) && networkObject->GetSyncData())
            {
			    networkObject->GetSyncTree()->Update(networkObject, networkObject->GetActivationFlags(), netInterface::GetSynchronisationTime(), updateThisFrame);

                if(updateThisFrame)
                {
                    DEV_ONLY(networkObject->SetLastSyncTreeUpdateFrame(fwTimer::GetFrameCount()));

                    // Check whether object needs moving to another batch
                    if(!updateLevelChanged)
                    {
                        u8 updateLevel = networkObject->GetMinimumUpdateLevel();
                        u8 oldBatch    = networkObject->GetSyncTreeUpdateBatch();
                        u8 newBatch    = networkObject->GetSyncTree()->TryToMoveBatch(updateLevel, oldBatch);

                        if(newBatch != oldBatch)
                        {
                            networkObject->SetSyncTreeUpdateLevelAndBatch(updateLevel, newBatch);

                            networkObject->GetSyncTree()->DecreaseObjectCount(updateLevel, oldBatch);
                            networkObject->GetSyncTree()->IncreaseObjectCount(updateLevel, newBatch);
                        }
                    }
                }
            }
#if __ASSERT
            else if(updateThisFrame)
            {
                DEV_ONLY(networkObject->SetLastSyncTreeUpdateFrame(fwTimer::GetFrameCount()));
            }
#endif // __ASSERT
        }
#if __ASSERT
        else
        {
            DEV_ONLY(networkObject->SetLastSyncTreeUpdateFrame(fwTimer::GetFrameCount()));
        }
#endif // __ASSERT

        if(updateLevelChanged)
        {
            u32 updateLevel = networkObject->GetMinimumUpdateLevel();

            if(gnetVerifyf(updateLevel < CNetworkSyncDataULBase::NUM_UPDATE_LEVELS, "Changing owner of a network object with an invalid update level!"))
            {
                u8 batch = networkObject->GetSyncTree()->GetBatchForNewObject(updateLevel);

                networkObject->GetSyncTree()->DecreaseObjectCount(networkObject->GetSyncTreeUpdateLevel(), networkObject->GetSyncTreeUpdateBatch());
                networkObject->GetSyncTree()->IncreaseObjectCount(updateLevel, batch);

                networkObject->SetSyncTreeUpdateLevelAndBatch(static_cast<u8>(updateLevel), batch);
                DEV_ONLY(ValidateObjectLists());
            }
        }
    }

#if __BANK
    unsigned endTime = sysTimer::GetSystemMsTime();

    m_LastSyncTreeUpdateDuration = (endTime > startTime) ? (endTime - startTime) : 0;
#endif // __BANK
}

void netObjectMgrBase::RemoveUnregisteringClones()
{
    unsigned numObjectsToRemove = 0;
    netObject *objectsToRemove[netObjectIDMgr::MAX_TOTAL_OBJECT_IDS];

    unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
    const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
    {
	    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

        PhysicalPlayerIndex playerIndex = remotePlayer->GetPhysicalPlayerIndex();

        if(playerIndex != INVALID_PLAYER_INDEX)
        {
            atDNode<netObject*, datBase> *pNode = m_aPlayerObjectList[playerIndex].GetHead();

            while (pNode)
            {
                netObject *pNetObj = pNode->Data;
                pNode = pNode->GetNext();

                if (pNetObj && pNetObj->IsLocalFlagSet(netObject::LOCALFLAG_UNREGISTERING))
                {
                    Assert(pNetObj->IsClone());

                    if(numObjectsToRemove < netObjectIDMgr::MAX_TOTAL_OBJECT_IDS)
                    {
                        objectsToRemove[numObjectsToRemove++] = pNetObj;
                    }
                }
            }
        }
    }

    for(unsigned index = 0; index < numObjectsToRemove; index++)
    {
        Assert(objectsToRemove[index]);
        DestroyNetworkObject(objectsToRemove[index]);
    }
}

void netObjectMgrBase::AddNewObjectInfoToSyncMessageInfoQueue(const netPlayer& player, netSequence messageSeqNum, ObjectId objectID, DataNodeFlags nodeFlags)
{
    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	if (!AssertVerify(playerIndex != INVALID_PLAYER_INDEX))
	{
		return;
	}

    ASSERT_ONLY(ValidateSyncMessageInfos());

    // do we need to create a new message info?
    if(m_aSyncMessageInfo[playerIndex].empty() || (AssertVerify(m_aSyncMessageInfo[playerIndex].back()) && m_aSyncMessageInfo[playerIndex].back()->m_MessageSeqNum != messageSeqNum))
    {
        // if queue is full remove the last node and clean up the list
        Assert(m_aSyncMessageInfo[playerIndex].size() <= NUM_SYNC_MESSAGES_INFO_HELD_FOR);

        while(m_aSyncMessageInfo[playerIndex].size() >= NUM_SYNC_MESSAGES_INFO_HELD_FOR)
        {
            syncMessageInfo     *messageInfo = m_aSyncMessageInfo[playerIndex].front();
            syncMessageInfoNode *currentNode = messageInfo ? messageInfo->m_ObjectList : 0;

            while(currentNode)
            {
                syncMessageInfoNode *nextNode = currentNode->m_Next;
                delete currentNode;
                currentNode = nextNode;
            }

            if(!messageInfo->m_Expired)
            {
#if __BANK
                if(PARAM_additionalBandwidthLogging.Get())
                {
                    gnetDebug2("Increasing unreliable resend count for %s as sync message info is full!", player.GetLogName());
                }
#endif // __BANK

                m_BandwidthMgr.IncreaseUnreliableResendCount(playerIndex);
            }

            messageInfo->m_ObjectList = 0;
            m_aSyncMessageInfo[playerIndex].pop_front();
            delete messageInfo;
        }

#if __ASSERT
        bool bAssert = false;

        // sanity check the queue to make sure there is not more than one entry per message
        if(!m_aSyncMessageInfo[playerIndex].empty())
        {
            SyncMessageInfoQueue::iterator currentPos = m_aSyncMessageInfo[playerIndex].begin();
            SyncMessageInfoQueue::iterator endPos     = m_aSyncMessageInfo[playerIndex].end();

            while(currentPos != endPos)
            {
                syncMessageInfo *syncInfo = *currentPos;

                Assert(syncInfo);

                if(syncInfo && syncInfo->m_MessageSeqNum == messageSeqNum)
                {
                    bAssert = true;
                }

                ++currentPos;
            }
        }

        if (bAssert)
        {
            SyncMessageInfoQueue::iterator currentPos = m_aSyncMessageInfo[playerIndex].begin();
            SyncMessageInfoQueue::iterator endPos     = m_aSyncMessageInfo[playerIndex].end();

            while(currentPos != endPos)
            {
                syncMessageInfo *syncInfo = *currentPos;

                Assert(syncInfo);

                if(m_MessageHandler->GetLog())
                {
                    if(syncInfo)
                    {
                        m_MessageHandler->GetLog()->Log("\t\t %d\r\n", syncInfo->m_MessageSeqNum);
                    }
                    else
                    {
                        m_MessageHandler->GetLog()->Log("\t\t **none**\r\n");
                    }
                }

                ++currentPos;
            }

            Assertf(0, "More than one history item has the same sequence number %d!", messageSeqNum);
        }
#endif

        syncMessageInfo *newInfo = rage_new syncMessageInfo;

        if (!AssertVerify(newInfo))
            return;

        newInfo->m_MessageSeqNum = messageSeqNum;
        newInfo->m_TimeSent      = netInterface::GetSynchronisationTime();

        m_aSyncMessageInfo[playerIndex].push_back(newInfo);
    }

    ASSERT_ONLY(ValidateSyncMessageInfos());

    syncMessageInfoNode* pNewNode = rage_new syncMessageInfoNode;

    if (AssertVerify(pNewNode))
    {
        pNewNode->m_Data.m_ObjectID  = objectID;
        pNewNode->m_Data.m_NodeFlags = nodeFlags;

        if (AssertVerify(m_aSyncMessageInfo[playerIndex].back()))
        {
            Assert(m_aSyncMessageInfo[playerIndex].back()->m_MessageSeqNum == messageSeqNum);
            pNewNode->m_Next = m_aSyncMessageInfo[playerIndex].back()->m_ObjectList;
            m_aSyncMessageInfo[playerIndex].back()->m_ObjectList = pNewNode;
        }
    }

    Assert((unsigned)netObjectMgrBase::syncMessageInfo::_ms_pPool->GetNoOfUsedSpaces() <= (netInterface::GetNumRemoteActivePlayers()*NUM_SYNC_MESSAGES_INFO_HELD_FOR));
}

void netObjectMgrBase::ProcessSyncAckInSyncMessageInfoQueues(const netPlayer &fromPlayer,
                                                             const netPlayer &toPlayer,
                                                             netSequence      messageSeqNum, 
                                                             ObjectId        *failedSyncIDs,
                                                             unsigned         numFailedSyncs,
                                                             bool             outOfOrder)
{
    PhysicalPlayerIndex playerIndex = fromPlayer.GetPhysicalPlayerIndex();

	if (!AssertVerify(playerIndex != INVALID_PLAYER_INDEX))
	{
		return;
	}

    // we only remove the sync message info if none of the syncs failed, this is so
    // the sync tree code can know when a failed ACK needs to be resent via the same
    // rules as if this message was dropped
    bool removeSyncMessageInfo = !outOfOrder && (numFailedSyncs == 0);

	if(!m_aSyncMessageInfo[playerIndex].empty())
    {
        SyncMessageInfoQueue::iterator it = m_aSyncMessageInfo[playerIndex].begin();
        SyncMessageInfoQueue::iterator next = it;
        SyncMessageInfoQueue::const_iterator stop     = m_aSyncMessageInfo[playerIndex].end();

        for(++next; stop != it; it = next, ++next)
        {
            syncMessageInfo *syncInfo = *it;

            Assert(syncInfo);

            if(syncInfo && syncInfo->m_MessageSeqNum == messageSeqNum)
            {
                // update ping metrics based on the time between sending the sync and receiving the ACK
                u32 syncTime    = netInterface::GetSynchronisationTime();
                u32 ping        = (syncTime > syncInfo->m_TimeSent) ? (syncTime - syncInfo->m_TimeSent) : 0;
                AddSyncPingSample(playerIndex, ping);

                syncMessageInfoNode *currentNode = syncInfo->m_ObjectList;

                while (currentNode)
                {
                    syncMessageInfoNode *nextNode = currentNode->m_Next;

                    netObject *pNetworkObject = GetNetworkObjectFromPlayer(currentNode->m_Data.m_ObjectID, toPlayer);

                    if (pNetworkObject && pNetworkObject->GetSyncData())
                    {
                        bool objectFailedSync = outOfOrder;

                        if(!objectFailedSync)
                        {
                            for(unsigned index = 0; index < numFailedSyncs; index++)
                            {
                                if(failedSyncIDs[index] == pNetworkObject->GetObjectID())
                                {
                                    objectFailedSync = true;
                                }
                            }
                        }

                        if(!objectFailedSync)
                        {
                            pNetworkObject->GetSyncTree()->ProcessAck(pNetworkObject, playerIndex, messageSeqNum, syncInfo->m_TimeSent, currentNode->m_Data.m_NodeFlags);
                        }
                    }

                    if(removeSyncMessageInfo)
                    {
                        delete currentNode;
                    }

                    currentNode = nextNode;
                }

                if(removeSyncMessageInfo)
                {
                    syncInfo->m_ObjectList = 0;
                    m_aSyncMessageInfo[playerIndex].erase(it);
                    delete syncInfo;
                }
                else
                {
                    // set this as expired otherwise it will be counted as
                    // an unacked unreliable message when it is cleaned up
                    syncInfo->m_Expired = true;
                }

                break;
            }
        }
    }

    ASSERT_ONLY(ValidateSyncMessageInfos());
}

unsigned netObjectMgrBase::GetTimeSyncMessageSentAndCheckExpiry(PhysicalPlayerIndex playerIndex, netSequence syncMessageSeq, unsigned expiryTime)
{
    if (gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Trying to get sync message info for an invalid player!"))
	{
		if(!m_aSyncMessageInfo[playerIndex].empty())
        {
            SyncMessageInfoQueue::const_iterator stop = m_aSyncMessageInfo[playerIndex].end();
			for(SyncMessageInfoQueue::iterator it = m_aSyncMessageInfo[playerIndex].begin(); 
				it != stop; it++)
            {
                syncMessageInfo *syncInfo = *it;

                Assert(syncInfo);

                if(syncInfo->m_MessageSeqNum == syncMessageSeq)
                {
                    unsigned currTime = netInterface::GetSynchronisationTime();

                    if((currTime - syncInfo->m_TimeSent) > expiryTime)
                    {
                        if(!syncInfo->m_Expired)
                        {
#if __BANK
                            if(PARAM_additionalBandwidthLogging.Get())
                            {
                                netPlayer *player = netInterface::GetPhysicalPlayerFromIndex(playerIndex);
                                gnetDebug2("Increasing unreliable resend count for %s as sequence %d has expired!", player ? player->GetLogName() : "Unknown", syncMessageSeq);
                            }
#endif // __BANK
                            m_BandwidthMgr.IncreaseUnreliableResendCount(playerIndex);

                            syncInfo->m_Expired = true;
                        }
                    }

                    return syncInfo->m_TimeSent;
                }
            }
        }
	}

    return 0;
}

static void ResetProximityControlTimer(netObject *networkObject, void *)
{
	if (networkObject)
		networkObject->ResetProximityControlTimer();
}

void netObjectMgrBase::ForAllLocalObjectsResetProximityControlTimer()
{
	ForAllLocalObjects(ResetProximityControlTimer, 0);
}

void netObjectMgrBase::RemoveObjectFromSyncMessageInfoQueues(ObjectId objectID)
{
    for (int playerIndex=0; playerIndex<m_aSyncMessageInfo.GetCount(); playerIndex++)
    {
        if(!m_aSyncMessageInfo[playerIndex].empty())
        {
            SyncMessageInfoQueue::iterator it =  m_aSyncMessageInfo[playerIndex].begin();
            SyncMessageInfoQueue::iterator next = it;
            SyncMessageInfoQueue::const_iterator stop =  m_aSyncMessageInfo[playerIndex].end();

            for(++next; stop != it; it = next, ++next)
            {
                syncMessageInfo *syncInfo = *it;

                Assert(syncInfo);

                if(syncInfo)
                {
                    syncMessageInfoNode *currentNode = syncInfo->m_ObjectList;
                    syncMessageInfoNode *prevNode    = 0;
                    syncMessageInfoNode *nextNode    = 0;

                    while (currentNode)
                    {
                        nextNode = currentNode->m_Next;

                        if (currentNode->m_Data.m_ObjectID == objectID)
                        {
                            if(prevNode == 0)
                            {
                                syncInfo->m_ObjectList = nextNode;
                            }
                            else
                            {
                                prevNode->m_Next = nextNode;
                            }

                            delete currentNode;
                        }
                        else
                        {
                            prevNode = currentNode;
                        }

                        currentNode = nextNode;
                    }
                }

                // we need to remove this syncInfo from the list now that it is empty
                if(syncInfo && syncInfo->m_ObjectList == 0)
                {
                    m_aSyncMessageInfo[playerIndex].erase(it);
                    delete syncInfo;
                }
            }
        }
    }

    ASSERT_ONLY(ValidateSyncMessageInfos());
}

void netObjectMgrBase::AddSyncLatencySample(PhysicalPlayerIndex playerIndex, unsigned latency)
{
    if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Invalid physical player index!"))
    {
        m_AverageLatency[playerIndex].AddSample(static_cast<float>(latency));
    }
}

void netObjectMgrBase::AddSyncPingSample(PhysicalPlayerIndex playerIndex, unsigned ping)
{
    if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Invalid physical player index!"))
    {
        m_AveragePing[playerIndex].AddSample(static_cast<float>(ping));
    }
}

float netObjectMgrBase::GetAverageLatency(PhysicalPlayerIndex playerIndex) const
{
    if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Invalid physical player index!"))
    {
        return m_AverageLatency[playerIndex].GetAverage();
    }

    return 0.0f;
}

float netObjectMgrBase::GetAveragePing(PhysicalPlayerIndex playerIndex) const
{
    if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Invalid physical player index!"))
    {
        return m_AveragePing[playerIndex].GetAverage();
    }

    return 0.0f;
}

float netObjectMgrBase::GetPacketLoss(PhysicalPlayerIndex playerIndex) const
{
    if(gnetVerifyf(playerIndex != INVALID_PLAYER_INDEX, "Invalid physical player index!"))
    {
        // The message handler deals with active player indices so we need to look this up here
        netPlayer *player = netInterface::GetPhysicalPlayerFromIndex(playerIndex);

        if(player)
        {
            return m_MessageHandler->GetPacketLoss(player->GetActivePlayerIndex());
        }
    }

    return 0.0f;
}

void netObjectMgrBase::ForceSortNetworkObjectMap()
{
	gnetAssertf(sysThreadType::IsUpdateThread(), "Calling object manager functions from outside the main thread!");

	// sort the network object map if it is no longer sorted
	if(m_IsInitialized && !m_NetworkObjectMap.IsSorted())
	{
		m_NetworkObjectMap.FinishInsertion();
		BANK_ONLY(CheckNetworkObjectMapForDuplicates("GetNetworkObject"));
	}
}

#if __BANK

void netObjectMgrBase::AssertObjectOnList(atDList<netObject*, datBase> &list, netObject &networkObject)
{
    atDNode<netObject*, datBase> *node = list.GetHead();

    bool found = false;

    while(node && !found)
    {
        netObject *object = node->Data;

        if(object == &networkObject)
        {
            if(!gnetVerifyf(networkObject.GetListNode()==node, "Incorrect net object list node!"))
            {
                NetworkLogUtils::WriteLogEvent(GetLog(), "OBJECT_LIST_ERROR_DETECTED!", networkObject.GetLogName());
                GetLog().WriteDataValue("Error", "Incorrect net object list node!");
            }

            found = true;
        }

        node = node->GetNext();
    }

    if(!gnetVerifyf(found, "Network object is not on the expected object list!"))
    {
        NetworkLogUtils::WriteLogEvent(GetLog(), "OBJECT_LIST_ERROR_DETECTED!", networkObject.GetLogName());
        GetLog().WriteDataValue("Error", "Network object is not on the expected object list!");
        GetLog().WriteDataValue("Expected List Index", "%d", networkObject.GetPhysicalPlayerIndex());

        for(PhysicalPlayerIndex playerIndex = 0; playerIndex < MAX_NUM_PHYSICAL_PLAYERS; playerIndex++)
        {
            node = m_aPlayerObjectList[playerIndex].GetHead();

            found = false;

            while(node && !found)
            {
                netObject *object = node->Data;

                if(object == &networkObject)
                {
                    GetLog().WriteDataValue("Actual List Index", "%d", playerIndex);

                    found = true;
                }

                node = node->GetNext();
            }
        }
    }
}

void netObjectMgrBase::CheckNetworkObjectMapForDuplicates(const char *locationOfCheck)
{
    gnetAssertf(sysThreadType::IsUpdateThread(), "Calling object manager functions from outside the main thread!");

    // make sure we don't have the same keys in the map twice
    m_NetworkObjectMap.FinishInsertion();

    atArray<atBinaryMap<netObject *, ObjectId>::DataPair> &rawData = m_NetworkObjectMap.GetRawDataArray();

    for(int index = 0; index < rawData.GetCount() - 1; index++)
    {
        if(!gnetVerifyf(rawData[index].key != rawData[index + 1].key, "Object ID %d is in the network map multiple times! Checking from %s", rawData[index].key, locationOfCheck))
        {
            // found a duplicate in the list, so now check for objects on the player lists not in the map
            if(netInterface::GetPlayerMgr().IsInitialized())
            {
                unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
                const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

                for(unsigned index = 0; index < numPhysicalPlayers; index++)
                {
                    const netPlayer *pPlayer = allPhysicalPlayers[index];

                    if(gnetVerifyf(pPlayer->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX, "Invalid Physical Player Index!"))
                    {
                        const atDNode<netObject*, datBase> *node = m_aPlayerObjectList[pPlayer->GetPhysicalPlayerIndex()].GetHead();

                        while(node)
                        {
                            netObject *networkObject = node->Data;

                            if(networkObject && !m_NetworkObjectMap.SafeGet(networkObject->GetObjectID()))
                            {
                                gnetDebug2("%s is in the player lists but not in the network map!", networkObject->GetLogName());
                            }

                            node = node->GetNext();
                        }
                    }
                }
            }

            NETWORK_QUITF(0, "Corrupt network object map detected!");
        }
    }
}

#endif // __BANK

#if __DEV

void netObjectMgrBase::ValidateObjectLists()
{
    for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        atDNode<netObject*, datBase> *node     = m_aPlayerObjectList[index].GetHead();
        ASSERT_ONLY(atDNode<netObject*, datBase> *prevNode = 0);
        atDNode<netObject*, datBase> *nextNode = node ? node->GetNext() : 0;

        while(node)
        {
            netObject *networkObject = node->Data;
            gnetAssertf(node->GetPrev() == prevNode, "Unexpected prev node!");
            gnetAssertf(node->GetNext() == nextNode, "Unexpected prev node!");

            if(networkObject)
            {
                gnetAssertf(networkObject->GetListNode()==node, "Incorrect net object list node!");
            }

            ASSERT_ONLY(prevNode = node);
            node     = nextNode;
            nextNode = node ? node->GetNext() : 0;
        }
    }
}

void netObjectMgrBase::ValidateSyncMessageInfos()
{
    // don't do this check when players are in the process of connecting/disconnecting
    unsigned numExistingPlayers = netInterface::GetNumActivePlayers() + netInterface::GetNumPendingPlayers();
	if(numExistingPlayers != netInterface::GetNumPhysicalPlayers())
    {
        return;
    }

    unsigned                 numSyncInfos           = 0;
    unsigned                 numRemoteActivePlayers = netInterface::GetNumRemoteActivePlayers();
    const netPlayer * const *remoteActivePlayers    = netInterface::GetRemoteActivePlayers();
    
    for(unsigned index = 0; index < numRemoteActivePlayers; index++)
    {
        const netPlayer *player = remoteActivePlayers[index];

	    if (player->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
	    {
            numSyncInfos += static_cast<unsigned>(m_aSyncMessageInfo[player->GetPhysicalPlayerIndex()].size());
        }
    }

    if(numSyncInfos != static_cast<unsigned>(netObjectMgrBase::syncMessageInfo::_ms_pPool->GetNoOfUsedSpaces()))
    {
        // check if this is actually a leak or the sync infos are stuck on a none-active players list
        unsigned total = 0;

        for(unsigned index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
        {
            gnetDebug2("Index %d: %d", index, static_cast<unsigned>(m_aSyncMessageInfo[index].size()));
            total += static_cast<unsigned>(m_aSyncMessageInfo[index].size());
        }

        gnetDebug2("Total: %d", total);

        gnetAssertf(0, "Possible sync info leak! Expected %d, Actual %d", numSyncInfos, (unsigned)netObjectMgrBase::syncMessageInfo::_ms_pPool->GetNoOfUsedSpaces());
    }
}

#endif // __DEV

} // namespace rage
