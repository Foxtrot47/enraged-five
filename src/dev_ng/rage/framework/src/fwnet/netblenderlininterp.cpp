//
// netblenderlininterp.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

// rage includes
#include "grcore/debugdraw.h"
#include "vector/color32.h"
#include "vectormath/legacyconvert.h"

// game includes
#include "fwnet/netblenderlininterp.h"

#include "fwnet/netinterface.h"
#include "fwnet/optimisations.h"
#include "fwsys/timer.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

#if __BANK
	bool netBlenderLinInterp::ms_bDebugNetworkBlend_UpdatePosition = false;
#endif

netLinInterpPredictionData::netLinInterpPredictionData(const Vector3 &position,
                                                       const Vector3 &velocity,
                                                       const u32      time)
{
    Reset(position, velocity, time);
}

netLinInterpPredictionData::~netLinInterpPredictionData()
{
}

void netLinInterpPredictionData::Reset(const Vector3 &position,
                                       const Vector3 &velocity,
                                       const u32      time,
									   blenderResetFlags resetFlag)
{
	if(resetFlag == RESET_POSITION || resetFlag == RESET_POSITION_AND_VELOCITY)
	{
		m_SnapshotPast.m_Position = position;
		m_SnapshotPast.m_PositionTimestamp = time;

		m_SnapshotPresent.m_Position = position;	
		m_SnapshotPresent.m_PositionTimestamp = time;
	}

	if(resetFlag == RESET_VELOCITY || resetFlag == RESET_POSITION_AND_VELOCITY)
	{
		m_SnapshotPast.m_Velocity = velocity;
		m_SnapshotPast.m_VelocityTimestamp = time;

		m_SnapshotPresent.m_Velocity = velocity;
		m_SnapshotPresent.m_VelocityTimestamp = time;
	}
}

netBlenderLinInterp::netBlenderLinInterp(CLinInterpBlenderData* pBlenderData, const Vector3 &initialPosition, const Vector3 &initialVelocity) :
netBlender(pBlenderData),
m_PredictionData(initialPosition,
                 initialVelocity,
                 netInterface::GetNetworkTime())
, m_BlendLinInterpStartTime(netInterface::GetNetworkTime())
, m_BlendingPosition(false)
, m_IsRamping(false)
, m_LastPopTime(0)
, m_PredictionError(0.0f)
, m_CurrentVelChange(pBlenderData->m_NormalMode.m_MinVelChange)
, m_BlenderMode(BLEND_MODE_NORMAL)
, m_UseLogarithmicBlending(false)
, m_UseLogarithmicBlendingThisFrame(false)
, m_PerformedAfterBlendCorrection(false)
, m_MaxPositionDeltaMultiplier(1.0f)
#if __BANK
, m_LastPredictedPos(VEC3_ZERO)
, m_LastSnapPos(VEC3_ZERO)
, m_LastSnapVel(VEC3_ZERO)
, m_LastSnapTime(0)
, m_LastTargetTime(0)
, m_LastFrameReset(0)
, m_PositionDelta(0.0f)
, m_LastDisableZBlending(false)
, m_BlendedTooFar(false)
#endif // __BANK
{
}

netBlenderLinInterp::~netBlenderLinInterp()
{
}

void netBlenderLinInterp::OnOwnerChange(blenderResetFlags resetFlag)
{
    // reset all snapshot data to time 0 so the first position update from the new owner is always applied
    Vector3 position = GetPositionFromObject();
    Vector3 velocity = VEC3_ZERO;

    if(GetBlenderData()->m_ApplyVelocity)
    {
        velocity = GetVelocityFromObject();
    }

	BANK_ONLY(ValidateTargetVelocity(velocity));

    m_PredictionData.Reset(position, velocity, GetLastSyncMessageTime(), resetFlag);

	SetLastOwnershipChangeTime(GetLastSyncMessageTime());
}

void netBlenderLinInterp::Update()
{
    if (!IsBlendingOn())
        return;

    if(IsUsingLogarithmicBlending())
    {
        DoLogarithmicBlend(m_PredictionData.GetSnapshotPresent().m_Position, LOGARITHMIC_BLEND_USE_VEL);
        return;
    }

    Vector3 objectPosition           = GetPositionFromObject();
    Vector3 predictedCurrentPosition = GetCurrentPredictedPosition();
    float   dist = (predictedCurrentPosition - objectPosition).Mag();

    m_IsRamping = false;

    u32 timeBlending = netInterface::GetNetworkTime() - m_BlendLinInterpStartTime;

#if __BANK
    m_LastPredictedPos     = GetPredictedNextFramePosition(predictedCurrentPosition, m_PredictionData.GetSnapshotPresent().m_Velocity);
    m_LastSnapPos          = m_PredictionData.GetSnapshotPresent().m_Position;
    m_LastSnapVel          = m_PredictionData.GetSnapshotPresent().m_Velocity;
    m_LastSnapTime         = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
    m_LastTargetTime       = netInterface::GetTimestampForPositionUpdates() + fwTimer::GetTimeStepInMilliseconds();
    m_LastDisableZBlending = DisableZBlending();
#endif // __BANK

    if((timeBlending > GetBlenderData()->m_BlendRampTime) || !m_BlendingPosition)
    {
        if(GetBlenderData()->m_ApplyVelocity)
        {
            // if we've hit the end of our blend time set the object to the target position
            // if the velocity is zero, otherwise just let it continue at it's current velocity
            if(m_PredictionData.GetSnapshotPresent().m_Velocity.IsZero())
            {
                if(!GetIsOnScreen() && m_BlendingPosition)
                {
                    SetPositionOnObject(m_PredictionData.GetSnapshotPresent().m_Position, true);
                    SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);
                    m_BlendingPosition = false;
                }

                if(dist <= GetBlenderData()->m_PositionDeltaMin)
                {
                    Vector3 velocity = GetVelocityFromObject();

                    if(!velocity.IsZero())
                    {
						ProcessEndOfBlendTime(objectPosition);
                    }
                }

                m_CurrentVelChange = GetCurrentVelocityChangeData().m_MinVelChange;
            }

            if(timeBlending < GetBlenderData()->m_BlendStopTime)
            {
                Vector3 blendedVelocity = m_PredictionData.GetSnapshotPresent().m_Velocity;

                if(OnlyPredictRelativeToForwardVector())
                {
                    Vector3 forwardVector = GetForwardVectorFromObject();
                    float forwardMag   = blendedVelocity.Dot(forwardVector);
                    blendedVelocity = forwardVector * forwardMag;
                }

                Vector3 currentVelocity = GetVelocityFromObject();
                Vector3 velocityDiff = blendedVelocity - currentVelocity;

                // only change the velocity by the specified amount towards the target to keep things smooth
                float velChange = GetCurrentVelocityChangeData().m_MinVelChange;

                if(velocityDiff.Mag2() > rage::square(velChange))
                {
                    velocityDiff.Normalize();
                    velocityDiff.Scale(velChange);
                }

                // don't bother correcting small changes to the velocity to keep things smoother
                if(velocityDiff.Mag2() > GetCurrentVelocityChangeData().m_SmallVelocitySquared)
                {
                    blendedVelocity = currentVelocity + velocityDiff;

                    SetVelocityOnObject(blendedVelocity);
                }
            }
            else
            {
                bool  onScreen    = GetIsOnScreen();
                float popDistance = onScreen ? GetBlenderData()->m_MaxOnScreenPositionDeltaAfterBlend : GetBlenderData()->m_MaxOffScreenPositionDeltaAfterBlend;

                if(dist > popDistance)
                {
                    float flatDist = (predictedCurrentPosition - objectPosition).XYMag();

                    if(!m_PerformedAfterBlendCorrection || flatDist > popDistance || dist >= GetPositionMaxForUpdateLevel())
                    {
                        bool wasPositionSet = SetPositionOnObject(predictedCurrentPosition, true);
						if (wasPositionSet)
						{
							SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);

							if(onScreen)
							{
								OnPredictionPop();
							}
						}                        

                        m_BlendingPosition              = false;
                        m_PerformedAfterBlendCorrection = true;
                    }
                }
                else
                {
                    SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);
                }
            }
        }

        // this code is to fix a bug where a player is left in his old position after respawning as a new round starts
        // ( the blender is not updated due to a load scene and then times out )
        if(dist >= GetPositionMaxForUpdateLevel() && m_PredictionData.GetSnapshotPresent().m_Velocity.IsZero())
        {
            bool wasPositionSet = SetPositionOnObject(m_PredictionData.GetSnapshotPresent().m_Position, true);
			if (wasPositionSet)
			{
				SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);
				OnPredictionPop();
			}

            m_BlendingPosition = false;
        }
    }
    else
    {
        // if we aren't blending z position check height constraints separately
        bool popDueToZDiff = false;

        if(DisableZBlending())
        {
            dist = (predictedCurrentPosition - objectPosition).XYMag();

            popDueToZDiff = DoNoZBlendHeightCheck(predictedCurrentPosition.z - objectPosition.z);
        }

        if(dist >= GetPositionMaxForUpdateLevel() || popDueToZDiff)
        {
            // pop straight to the new position
            bool wasPositionSet = SetPositionOnObject(predictedCurrentPosition, true);
			if (wasPositionSet)
			{
				SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);
				OnPredictionPop();
			}

            m_LastPopTime       = fwTimer::GetTimeInMilliseconds();
            m_BlendingPosition = false;
        }
        else
        {
            m_IsRamping = true;

            if(dist > GetBlenderData()->m_PositionDeltaMin)
            {
                if(GetBlenderData()->m_ApplyVelocity)
                {
                    u32   timeToBlendOverInMs = GetBlenderData()->m_UseBlendVelSmoothing ? GetBlenderData()->m_BlendVelSmoothTime : fwTimer::GetTimeStepInMilliseconds();
                    float timeToBlendOver     = static_cast<float>(timeToBlendOverInMs) / 1000.0f * fwTimer::GetTimeWarpActive();

                    Vector3 targetVelocity          = m_PredictionData.GetSnapshotPresent().m_Velocity;
                    Vector3 predictedFuturePosition = GetPredictedFuturePosition(timeToBlendOverInMs);
                    Vector3 blendedVelocity = (predictedFuturePosition - objectPosition) / timeToBlendOver;

                    const float maxSpeedDiff = GetCurrentVelocityChangeData().m_MaxVelDiffFromTarget;

                    Vector3 correctionVector = blendedVelocity - targetVelocity;

                    if(correctionVector.Mag2() > rage::square(maxSpeedDiff))
                    {
                        correctionVector.Normalize();
                        correctionVector.Scale(maxSpeedDiff);
                    }

                    blendedVelocity = targetVelocity + correctionVector;

                    Vector3 currentVelocity = GetVelocityFromObject();
                    Vector3 velocityDiff = blendedVelocity - currentVelocity;

                    // don't bother correcting small changes to the velocity to keep things smoother
                    if(velocityDiff.Mag2() > GetCurrentVelocityChangeData().m_SmallVelocitySquared)
                    {
                        const VelocityChangeData &currentVelChangeData = GetCurrentVelocityChangeData();

                        float maxVelocityChange = m_CurrentVelChange;

                        if(dist > currentVelChangeData.m_ErrorIncreaseVel)
                        {
                            if(m_CurrentVelChange < currentVelChangeData.m_MaxVelChange)
                            {
                                m_CurrentVelChange += currentVelChangeData.m_VelChangeRate;

                                if(m_CurrentVelChange > currentVelChangeData.m_MaxVelChange)
                                {
                                    m_CurrentVelChange = currentVelChangeData.m_MaxVelChange;
                                }
                            }
                        }
                        else if(dist < currentVelChangeData.m_ErrorDecreaseVel)
                        {
                            if(m_CurrentVelChange > currentVelChangeData.m_MinVelChange)
                            {
                                m_CurrentVelChange -= currentVelChangeData.m_VelChangeRate;

                                if(m_CurrentVelChange < currentVelChangeData.m_MinVelChange)
                                {
                                    m_CurrentVelChange = currentVelChangeData.m_MinVelChange;
                                }
                            }
                        }

                        // boost velocity change rate if local change was higher
                        if(maxVelocityChange > currentVelChangeData.m_MaxVelChange)
                        {
                            m_CurrentVelChange = maxVelocityChange = currentVelChangeData.m_MaxVelChange;
                        }

                        if(GetBlenderData()->m_AllowAdjustVelChangeFromUpdates)
                        {
                            float velocityChangeFromUpdates = GetPredictedAccelerationVector().Mag() * fwTimer::GetTimeStep();

                            if(velocityChangeFromUpdates > maxVelocityChange)
                            {
                                m_CurrentVelChange = maxVelocityChange = velocityChangeFromUpdates;
                            }
                        }

                        // only change the velocity by the specified amount towards the target to keep things smooth
                        if(velocityDiff.Mag2() > rage::square(maxVelocityChange))
                        {
                            velocityDiff.Normalize();
                            velocityDiff.Scale(maxVelocityChange);
                        }

                        blendedVelocity = currentVelocity + velocityDiff;

                        if(PreventBlendingAwayFromVelocity(dist))
                        {
                            Vector3 currVelNormal    = currentVelocity;
                            Vector3 targetVelNormal  = targetVelocity;
                            Vector3 blendedVelNormal = blendedVelocity;
                            currVelNormal.Normalize();
                            targetVelNormal.Normalize();
                            blendedVelNormal.Normalize();

                            if(currVelNormal.Dot(targetVelNormal) > 0.0f)
                            {
                                if(((targetVelocity.Mag2()  > 0.01f) && blendedVelNormal.Dot(targetVelNormal) < 0.0f) ||
                                   ((currentVelocity.Mag2() > 0.01f) && blendedVelNormal.Dot(currVelNormal)   < 0.0f))
                                {
                                    blendedVelocity.Zero();
                                }
                            }
                        }

                        if(DisableZBlending())
                        {
                            blendedVelocity.z = currentVelocity.z;
                        }

                        SetVelocityOnObject(blendedVelocity);

#if __BANK
                        m_LastPredictedPos = GetPredictedNextFramePosition(predictedCurrentPosition, targetVelocity);
#endif // __BANK
                    }
                }
            }
            else
            {
                if(!GetBlenderData()->m_ApplyVelocity)
                {
                    m_BlendingPosition = false;
                }
                else
                {
                    Vector3 currentVelocity    = GetVelocityFromObject();
                    Vector3 targetVelocity     = m_PredictionData.GetSnapshotPresent().m_Velocity;
                    Vector3 targetVelocityDiff = targetVelocity - currentVelocity;

                    if(targetVelocityDiff.Mag2() > rage::square(GetCurrentVelocityChangeData().m_MaxVelChangeToTarget))
                    {
                        targetVelocityDiff.Normalize();
                        targetVelocityDiff.Scale(GetCurrentVelocityChangeData().m_MaxVelChangeToTarget);
                    }

                    // don't bother correcting small changes to the velocity to keep things smoother
                    if(targetVelocityDiff.Mag2() > GetCurrentVelocityChangeData().m_SmallVelocitySquared)
                    {
                        Vector3 blendedVelocity = currentVelocity + targetVelocityDiff;

                        // move to target velocity once we have reached the target
                        SetVelocityOnObject(blendedVelocity);
                    }

                    m_BlendingPosition = false;
                }
            }
        }

        UpdateBlenderMode(dist);
    }

    return;
}

void netBlenderLinInterp::ProcessEndOfBlendTime(Vector3 &objectPosition)
{
	Vector3 previousPosition = GetPreviousPositionFromObject();

	// we don't want to correct the z position in case the object was in the air
	// in which case the blender would fight with gravity
	previousPosition.z = objectPosition.z;

	SetPositionOnObject(previousPosition, false);
	SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);
}

void netBlenderLinInterp::Reset()
{
    Vector3 position = GetPositionFromObject();
    Vector3 velocity = VEC3_ZERO;

    if(GetBlenderData()->m_ApplyVelocity)
    {
        velocity = GetVelocityFromObject();
    }

	BANK_ONLY(ValidateTargetVelocity(velocity));

    m_PredictionData.Reset(position, velocity, GetLastSyncMessageTime());

    m_BlendingPosition = false;

    BANK_ONLY(m_LastFrameReset = fwTimer::GetFrameCount());
}

void netBlenderLinInterp::GoStraightToTarget()
{
    if(GoStraightToTargetUsesPrediction())
    {
        Vector3 predictedCurrentPosition = GetCurrentPredictedPosition();
        SetPositionOnObject(predictedCurrentPosition, true);
    }
    else
    {
        SetPositionOnObject(m_PredictionData.GetSnapshotPresent().m_Position, true);
    }

    SetVelocityOnObject(m_PredictionData.GetSnapshotPresent().m_Velocity);

    m_BlendingPosition = false;
}

void netBlenderLinInterp::ProcessPostPhysics()
{
#if __BANK
    UpdatePositionDelta();
#endif // __BANK
}

void netBlenderLinInterp::UpdateBlenderMode(const float distanceFromTarget)
{
    // see if we are moving fast enough to use vel error mode
    Vector3 targetVelocity  = m_PredictionData.GetSnapshotPresent().m_Velocity;

    if(targetVelocity.Mag2() > GetBlenderData()->m_HighSpeedThresholdSqr)
    {
        SetBlenderMode(BLEND_MODE_HIGH_SPEED);
    }
    else if(targetVelocity.Mag2() <= GetBlenderData()->m_LowSpeedThresholdSqr)
    {
        SetBlenderMode(BLEND_MODE_LOW_SPEED);
    }
    else
    {
        // see if we have caught up enough to return to normal mode
        if(m_BlenderMode == BLEND_MODE_HIGH_SPEED)
        {
            if(distanceFromTarget <= GetBlenderData()->m_NormalModePositionThreshold)
            {
                SetBlenderMode(BLEND_MODE_NORMAL);
            }
        }
        else
        {
            SetBlenderMode(BLEND_MODE_NORMAL);
        }
    }
}

Vector3 netBlenderLinInterp::GetLastPositionReceived(u32 *timestamp) const
{
    if(timestamp)
    {
        *timestamp = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
    }

    return m_PredictionData.GetSnapshotPresent().m_Position;
}

Vector3 netBlenderLinInterp::GetLastVelocityReceived(u32 *timestamp) const
{
    if(timestamp)
    {
        *timestamp = m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp;
    }

    return m_PredictionData.GetSnapshotPresent().m_Velocity;
}

void netBlenderLinInterp::UpdatePosition(const Vector3 &position, u32 time)
{
    // needs to be >= so that data processed in a clone create will work properly
    if(time >= m_PredictionData.GetSnapshotPresent().m_PositionTimestamp)
    {
        if(m_PredictionData.GetSnapshotPresent().m_PositionTimestamp != 0)
        {
            Vector3 predictedPosition = GetPredictedPosition(time);
            m_PredictionError = (position - predictedPosition).Mag();
        }

        // move snapshot B position to snapshot A, and copy new position to snapshot B
        m_PredictionData.GetSnapshotPast().m_Position             = m_PredictionData.GetSnapshotPresent().m_Position;
        m_PredictionData.GetSnapshotPast().m_PositionTimestamp    = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
        m_PredictionData.GetSnapshotPresent().m_Position          = position;
        m_PredictionData.GetSnapshotPresent().m_PositionTimestamp = time;

#if __DEV
		if (ms_bDebugNetworkBlend_UpdatePosition)
		{
			grcDebugDraw::Sphere(VECTOR3_TO_VEC3V(m_PredictionData.GetSnapshotPast().m_Position), 0.05f, Color32(255,0,0), true, 300); //red
			grcDebugDraw::Sphere(VECTOR3_TO_VEC3V(m_PredictionData.GetSnapshotPresent().m_Position), 0.05f, Color32(0,255,0), true, 300); //green
		}
#endif // __DEV

        m_BlendLinInterpStartTime       = netInterface::GetNetworkTime();
        m_BlendingPosition              = true;
        m_PerformedAfterBlendCorrection = false;
    }
}

void netBlenderLinInterp::UpdateVelocity(const Vector3 &velocity, u32 time)
{
	BANK_ONLY(ValidateTargetVelocity(velocity));

    if(time >= m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp)
    {
        // calculate any error from the remote machine running at a low frame rate, we're only concerned with entities moving less than
        // should be possible here, so any prediction error isn't taken into account. If the object has moved less than the minimum velocity
        // between the two snapshots this is reported as an error
        Vector3 positionDiff       = (m_PredictionData.GetSnapshotPresent().m_Position          - m_PredictionData.GetSnapshotPast().m_Position);
        u32     timeDiff           = (m_PredictionData.GetSnapshotPresent().m_PositionTimestamp - m_PredictionData.GetSnapshotPast().m_PositionTimestamp);
        Vector3 calculatedVelocity = positionDiff / ((timeDiff / 1000.0f) * fwTimer::GetTimeWarpActive());

        Vector3 minVelocity = (m_PredictionData.GetSnapshotPresent().m_Velocity.Mag2() > velocity.Mag2()) ? velocity : m_PredictionData.GetSnapshotPresent().m_Velocity;

        static float ERROR_THRESHOLD = 5.0f;
        if(!minVelocity.IsZero() && (calculatedVelocity.Mag2() < minVelocity.Mag2()) && ((calculatedVelocity - minVelocity).Mag2() > ERROR_THRESHOLD))
        {
            float frameRateError = 1.0f - fabsf(calculatedVelocity.Mag() / minVelocity.Mag());
            m_FrameRateError.AddSample(frameRateError);

            if(frameRateError >= GetBlenderData()->m_VelocityErrorThreshold)
            {
                SetBlenderMode(BLEND_MODE_HIGH_SPEED);
            }
        }
        else
        {
            m_FrameRateError.AddSample(0.0f);
        }

        // move snapshot B velocity to snapshot A, and copy new velocity to snapshot B
        m_PredictionData.GetSnapshotPast().m_Velocity             = m_PredictionData.GetSnapshotPresent().m_Velocity;
        m_PredictionData.GetSnapshotPast().m_VelocityTimestamp    = m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp;
        m_PredictionData.GetSnapshotPresent().m_Velocity          = velocity;
        m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp = time;
    }
}

const char *netBlenderLinInterp::GetBlenderModeName() const
{
    switch(m_BlenderMode)
    {
        case BLEND_MODE_LOW_SPEED:
            return "LOW SPEED";
        case BLEND_MODE_NORMAL:
            return "NORMAL";
        case BLEND_MODE_HIGH_SPEED:
            return "HIGH SPEED";
        default:
            gnetAssertf(0, "Unexpected blender mode!");
            return "NORMAL";
    }
}

bool netBlenderLinInterp::HasStoppedPredictingPosition() const
{
    unsigned time = netInterface::GetTimestampForStartOfFrame();

    if(time < m_PredictionData.GetSnapshotPresent().m_PositionTimestamp)
    {
        time = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
    }

    float timeElapsed = (time - m_PredictionData.GetSnapshotPresent().m_PositionTimestamp) / 1000.0f * fwTimer::GetTimeWarpActive();

    if(timeElapsed > GetBlenderData()->m_MaxPredictTime)
    {
        return true;
    }

    return false;
}

const Vector3 netBlenderLinInterp::GetCurrentPredictedPosition() const
{
    return GetPredictedPosition(netInterface::GetTimestampForStartOfFrame());
}

const Vector3 netBlenderLinInterp::GetPredictedFuturePosition(u32 timeInFutureMs) const
{
    return GetPredictedPosition(netInterface::GetTimestampForStartOfFrame() + timeInFutureMs);
}

const Vector3 netBlenderLinInterp::GetPredictedPosition(unsigned time) const
{
    if(time < m_PredictionData.GetSnapshotPresent().m_PositionTimestamp)
    {
        time = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
    }

    float timeElapsed = (time - m_PredictionData.GetSnapshotPresent().m_PositionTimestamp) / 1000.0f * fwTimer::GetTimeWarpActive();
    if(timeElapsed > GetBlenderData()->m_MaxPredictTime)
    {
        timeElapsed = GetBlenderData()->m_MaxPredictTime;
    }

    Vector3 velocity = m_PredictionData.GetSnapshotPresent().m_Velocity;

    if(OnlyPredictRelativeToForwardVector())
    {
        Vector3 forwardVector = GetForwardVectorFromObject();
        float forwardMag   = velocity.Dot(forwardVector);
        velocity = forwardVector * forwardMag;
    }

    Vector3 predictedCurrentPosition = m_PredictionData.GetSnapshotPresent().m_Position + velocity * timeElapsed;

    if(GetBlenderData()->m_PredictAcceleration)
    {
        predictedCurrentPosition += (0.5f * GetPredictedAccelerationVector() * (timeElapsed * timeElapsed));
    }

    return predictedCurrentPosition;
}

const Vector3 netBlenderLinInterp::GetPredictedAccelerationVector() const
{
    if(m_PredictionData.GetSnapshotPresent().m_Velocity.IsZero())
    {
        return VEC3_ZERO;
    }
    else
    {
        Vector3 pastVelocity     = m_PredictionData.GetSnapshotPast().m_Velocity;
        Vector3 presentVelocity  = m_PredictionData.GetSnapshotPresent().m_Velocity;
        u32 timeBetweenSnapshots = 0;
        
        float ratio = 0.0f;

        if(m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp > m_PredictionData.GetSnapshotPast().m_VelocityTimestamp)
        {
            timeBetweenSnapshots = m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp - m_PredictionData.GetSnapshotPast().m_VelocityTimestamp;

            u32 MAX_TIME_DIFF = 200;

            if(timeBetweenSnapshots < MAX_TIME_DIFF)
            {
                ratio = (1000.0f * fwTimer::GetTimeWarpActive()) / timeBetweenSnapshots;
            }
        }
        
        return ((presentVelocity - pastVelocity) * ratio);
    }
}

const float netBlenderLinInterp::GetPredictedAccelerationSquared() const
{
    Vector3 pastVelocity     = m_PredictionData.GetSnapshotPast().m_Velocity;
    Vector3 presentVelocity  = m_PredictionData.GetSnapshotPresent().m_Velocity;
    u32 timeBetweenSnapshots = 0;
    
    float ratio = 0.0f;

    if(m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp > m_PredictionData.GetSnapshotPast().m_VelocityTimestamp)
    {
        timeBetweenSnapshots = m_PredictionData.GetSnapshotPresent().m_VelocityTimestamp - m_PredictionData.GetSnapshotPast().m_VelocityTimestamp;

        u32 MAX_TIME_DIFF = 200;

        if(timeBetweenSnapshots < MAX_TIME_DIFF)
        {
            ratio = (1000.0f * fwTimer::GetTimeWarpActive()) / timeBetweenSnapshots;
        }
    }
    
    const float accelerationSqr = ((presentVelocity - pastVelocity)).Mag2() * ratio;

    return accelerationSqr;
}

const Vector3 netBlenderLinInterp::GetPredictedVelocity(float maxSpeedToPredict) const
{
    Vector3 calculatedVelocity  = VEC3_ZERO;
    u32     presentSnapshotTime = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
    u32     pastSnapshotTime    = m_PredictionData.GetSnapshotPast().m_PositionTimestamp;

    if(presentSnapshotTime > pastSnapshotTime 
		&& m_PredictionData.GetSnapshotPast().m_PositionTimestamp > GetLastOwnershipChangeTime()
		&& m_PredictionData.GetSnapshotPresent().m_PositionTimestamp > GetLastOwnershipChangeTime())
    {
        Vector3 positionDiff = (m_PredictionData.GetSnapshotPresent().m_Position - m_PredictionData.GetSnapshotPast().m_Position);
        u32     timeDiff     = presentSnapshotTime - pastSnapshotTime;

        float timeElapsed = (timeDiff / 1000.0f) * fwTimer::GetTimeWarpActive();

        const float MAX_TIME_DIFF = 1.5f;

        if(timeElapsed < MAX_TIME_DIFF)
        {
             calculatedVelocity = positionDiff / timeElapsed;
        }
    }
    else
    {
        // just use the objects current velocity if we don't have valid snapshot information
        calculatedVelocity = GetVelocityFromObject();
    }

    if(calculatedVelocity.Mag2() > rage::square(maxSpeedToPredict))
    {
        calculatedVelocity = VEC3_ZERO;
    }

    return calculatedVelocity;
}

#if __BANK

float netBlenderLinInterp::GetPositionDelta(bool &blendedTooFar) const
{
    blendedTooFar = m_BlendedTooFar;
    return m_PositionDelta;
}

#endif // __BANK

const VelocityChangeData &netBlenderLinInterp::GetCurrentVelocityChangeData() const
{
    switch(m_BlenderMode)
    {
    case BLEND_MODE_LOW_SPEED:
        return GetBlenderData()->m_LowSpeedMode;
    case BLEND_MODE_NORMAL:
        return GetBlenderData()->m_NormalMode;
    case BLEND_MODE_HIGH_SPEED:
        return GetBlenderData()->m_HighSpeedMode;
    default:
        gnetAssertf(0, "Unexpected blender mode!");
        return GetBlenderData()->m_NormalMode;
    }
}

const Vector3 netBlenderLinInterp::GetPredictedNextFramePosition(const Vector3 &predictedCurrentPosition,
                                                                 const Vector3 &targetVelocity) const
{
    Vector3 velocity = targetVelocity;

    if(OnlyPredictRelativeToForwardVector())
    {
        Vector3 forwardVector = GetForwardVectorFromObject();
        float forwardMag   = velocity.Dot(forwardVector);
        velocity = forwardVector * forwardMag;
    }

    float timestep = fwTimer::GetTimeStep();

    Vector3 predictedNextFramePosition = predictedCurrentPosition + (velocity * timestep);

    if(GetBlenderData()->m_PredictAcceleration)
    {
        predictedNextFramePosition += (0.5f * GetPredictedAccelerationVector() * (timestep * timestep));
    }

    return predictedNextFramePosition;
}

u32 netBlenderLinInterp::GetTimeBetweenUpdates() const
{
    u32 timeBetweenUpdates = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp - m_PredictionData.GetSnapshotPast().m_PositionTimestamp;

    return timeBetweenUpdates;
}

float netBlenderLinInterp::GetPositionMaxForUpdateLevel() const
{
	float positionDelta;
    switch(GetEstimatedUpdateLevel())
    {
    case LOW_UPDATE_LEVEL:
        positionDelta = GetCurrentVelocityChangeData().m_PositionDeltaMaxLow;
    case MEDIUM_UPDATE_LEVEL:
        positionDelta = GetCurrentVelocityChangeData().m_PositionDeltaMaxMedium;
    case HIGH_UPDATE_LEVEL:
        positionDelta = GetCurrentVelocityChangeData().m_PositionDeltaMaxHigh;
    default:
        positionDelta = GetCurrentVelocityChangeData().m_PositionDeltaMaxHigh;
    }

	positionDelta *= m_MaxPositionDeltaMultiplier;

	return positionDelta;
}

void netBlenderLinInterp::SetBlenderMode(BlenderMode blenderMode)
{
    if(m_BlenderMode != blenderMode)
    {
        m_BlenderMode = blenderMode;
    }
}

#if __BANK

void netBlenderLinInterp::UpdatePositionDelta()
{
    const Vector3 predictedCurrentPosition = m_LastPredictedPos;
    const Vector3 objectPosition           = GetPositionFromObject();

    Vector3 delta = objectPosition - predictedCurrentPosition;
    m_PositionDelta = delta.Mag();

    m_BlendedTooFar = false;

    Vector3 velocity = m_LastSnapVel;

    if(!velocity.IsZero())
    {
        delta.Normalize();
        velocity.Normalize();
        m_BlendedTooFar = velocity.Dot(delta) > 0.0f;
    }
}

#endif // __BANK

void netBlenderLinInterp::DoLogarithmicBlend(const Vector3 &targetPosition, LogarithmicBlendMode blendMode)
{
    m_UseLogarithmicBlendingThisFrame = false;

    Vector3 objectPosition = GetPositionFromObject();
    Vector3 positionDelta  = targetPosition - objectPosition;

    float dist = positionDelta.Mag();

#if __BANK
    m_LastPredictedPos     = targetPosition;
    m_LastSnapPos          = m_PredictionData.GetSnapshotPresent().m_Position;
    m_LastSnapVel          = m_PredictionData.GetSnapshotPresent().m_Velocity;
    m_LastSnapTime         = m_PredictionData.GetSnapshotPresent().m_PositionTimestamp;
    m_LastTargetTime       = netInterface::GetTimestampForPositionUpdates() + fwTimer::GetTimeStepInMilliseconds();
    m_LastDisableZBlending = DisableZBlending();
#endif // __BANK

    // if we aren't blending z position check height constraints separately
    bool popDueToZDiff = false;

    if(DisableZBlending())
    {
        dist = (targetPosition - objectPosition).XYMag();

        popDueToZDiff = DoNoZBlendHeightCheck(targetPosition.z - objectPosition.z);

        positionDelta.z = 0.0f;
    }

    if(dist >= GetPositionMaxForUpdateLevel() || popDueToZDiff)
    {
        // pop straight to the new position
		bool wasPositionSet = SetPositionOnObject(targetPosition, true);
		if (wasPositionSet)
		{
			SetVelocityOnObject(VEC3_ZERO);
			OnPredictionPop();
		}
        

        m_LastPopTime       = fwTimer::GetTimeInMilliseconds();
        m_BlendingPosition = false;
    }
    else
    {
        u32 timeBlending = netInterface::GetNetworkTime() - m_BlendLinInterpStartTime;

        if((timeBlending > GetBlenderData()->m_BlendRampTime) || !m_BlendingPosition)
        {
            bool  onScreen    = GetIsOnScreen();
            float popDistance = onScreen ? GetBlenderData()->m_MaxOnScreenPositionDeltaAfterBlend : GetBlenderData()->m_MaxOffScreenPositionDeltaAfterBlend;

            if(dist > popDistance)
            {
                float flatDist = (targetPosition - objectPosition).XYMag();

                if(!m_PerformedAfterBlendCorrection || flatDist > popDistance)
                {
                    bool wasPositionSet = SetPositionOnObject(targetPosition, true);
					if (wasPositionSet)
					{
						SetVelocityOnObject(VEC3_ZERO);

						if(onScreen)
						{
							OnPredictionPop();
						}
					}                   

                    m_BlendingPosition              = false;
                    m_PerformedAfterBlendCorrection = true;
                }
            }
        }
        else
        {
            if(dist <= GetBlenderData()->m_PositionDeltaMin)
            {
                Vector3 newPosition = targetPosition;

                if(DisableZBlending())
                {
                    newPosition.z = objectPosition.z;
                }

                SetPositionOnObject(newPosition, false);
                SetVelocityOnObject(VEC3_ZERO);
                m_BlendingPosition = false;
            }
            else
            {
                float positionTimeRatio = GetBlenderData()->m_LogarithmicBlendRatio;

                // ensure we always move a reasonable distance towards the target position
                static float minBlendDistance = 0.01f;
                if((dist * positionTimeRatio) < minBlendDistance)
                {
                    positionTimeRatio = minBlendDistance / dist;
                }

                // clamp the position time ratio and move towards the target
                if(positionTimeRatio >= 1.0f)
                {
                    positionTimeRatio = 1.0f;
                }

                Vector3 blendedPosition = objectPosition + positionDelta * positionTimeRatio;

                if(blendMode == LOGARITHMIC_BLEND_USE_VEL)
                {
                    Vector3 blendedVelocity = (blendedPosition - objectPosition) / fwTimer::GetTimeStep();
                    Vector3 currentVelocity = GetVelocityFromObject();
                    Vector3 velocityDiff    = blendedVelocity - currentVelocity;

                    // cap the velocity change
                    const float maxVelocityChange = GetBlenderData()->m_LogarithmicBlendMaxVelChange;

                    if(velocityDiff.Mag2() > rage::square(maxVelocityChange))
                    {
                        velocityDiff.Normalize();
                        velocityDiff.Scale(maxVelocityChange);

                        blendedVelocity = currentVelocity + velocityDiff;
                    }

                    if(DisableZBlending())
                    {
                        blendedVelocity.z = currentVelocity.z;
                    }

                    SetVelocityOnObject(blendedVelocity);
                }
                else if(blendMode == LOGARITHMIC_BLEND_USE_POS)
                {
                    SetPositionOnObject(blendedPosition, false);
                }
                else
                {
                    gnetAssertf(0, "Invalid Logarithmic Blend Mode!");
                }
            }
        }
    }
}

} // namespace rage
