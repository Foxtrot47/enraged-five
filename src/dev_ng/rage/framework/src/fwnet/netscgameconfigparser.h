//
// filename:	netscgameconfigparser.h
// description:	
//

#ifndef INC_NETSCGAMECONFIGPARSER_H_
#define INC_NETSCGAMECONFIGPARSER_H_

#include "string/string.h"
#include "string/stringhash.h"
#include "atl/array.h"
#include "atl/hashstring.h"
#include "atl/string.h"
#include "rline/rlstats.h"
#include "atl/singleton.h"

namespace rage
{
	class parElement;
	class bkBank;
	class bkGroup;

	class rlLeaderboard2GroupSelector;
};

using namespace rage;

enum eLeaderboardInputDataTypesEnum
{
	InputDataType_INVALID = RL_STAT_TYPE_NULL,
	InputDataType_int     = RL_STAT_TYPE_INT32,
	InputDataType_long    = RL_STAT_TYPE_INT64,
	InputDataType_double  = RL_STAT_TYPE_DOUBLE,
};
typedef u16 LeaderboardInputDataTypeVal;

struct LeaderboardInput
{
	static eLeaderboardInputDataTypesEnum GetInputDataTypeFromString(const char* typeStr);

	void Set(u16 id, const char* name, eLeaderboardInputDataTypesEnum dataType) 
	{
		m_id = id;
		m_dataType = (u16)dataType;
		m_nameHashStr = name;
	}

	u16 m_id;
	LeaderboardInputDataTypeVal m_dataType; //Should be a eLeaderboardInputDataTypesEnum but make it a u16 for memory packing
	atHashString m_nameHashStr;
	
	LeaderboardInput() : m_dataType(InputDataType_INVALID), m_id(0) {}

#if !__NO_OUTPUT
	const char* GetName() const { const char* str = m_nameHashStr.TryGetCStr(); return str ? str : ""; }
#endif
};

struct LeaderboardCategory
{
	void SetName(const char* name)
	{
		m_hname = name;
	}

	atHashString m_hname;//char m_name[32];
};

enum eLeaderboardColumnAggregationType
{
	AggType_INVALID,
	AggType_Max,
	AggType_Min,
	AggType_Sum,
	AggType_Last,
	AggType_Ratio,
};

struct LeaderboardColAggregation
{
	void SetAggTypeFromString(const char* typeStr);

	struct ColumnInput
	{
		u8 m_ordinal;
		u8 m_columnId;

		ColumnInput(): m_columnId(0), m_ordinal(0) {}
		void Set(u8 ord, u8 col) { m_ordinal = ord; m_columnId = col; }
	};

	struct GameInput
	{
		u16 m_inputId;

		GameInput() : m_inputId(0) {}
	};

	atArray<ColumnInput> m_columnInput;
	GameInput m_gameInput;
	u16 m_conditionalOnColumnId;
	eLeaderboardColumnAggregationType m_type;
	

	LeaderboardColAggregation() 
		: m_type(AggType_INVALID)
		, m_conditionalOnColumnId(0) 
	{}
};

struct LeaderboardColumn 
{
	bool m_isRakingColumn;
	u8 m_id;
	u8 m_ordinal;
	atHashString m_nameHashStr;
	LeaderboardColAggregation m_aggregation;

#if !__NO_OUTPUT
	const char* GetName() const { const char* str = m_nameHashStr.TryGetCStr(); return str ? str : ""; }
#endif

};

struct LeaderboardCategoryGroup 
{
	atString m_value; //Comma seperated list of categories (LeaderboardCategory).
};

enum eLeaderboardResetTypes
{
	ResetType_Never,
	ResetType_Weekly,
	ResetType_BiWeekly,
	ResetType_Monthly,
	ResetType_BiMonthly,
	ResetType_Annually,

};

struct Leaderboard
{

	atArray<LeaderboardCategoryGroup> m_categoryGroups;
	atArray<LeaderboardColumn> m_columns;

	LeaderboardColumn* AddColumn(u8 id, const char* name, u8 ordinal, bool isRanking);

	u16 m_id;
	atHashString m_nameHashStr;

	u32 GetNameHash() const { return m_nameHashStr.GetHash(); }
#if !__NO_OUTPUT
	const char* GetName() const { const char* str = m_nameHashStr.TryGetCStr(); return str ? str : ""; }
#endif
};


class LeaderboardsConfiguration
{
public:
	typedef atArray<Leaderboard> LeaderboardConfigList;
	
	LeaderboardInput& AddInput();
	void AddCategory( const char* name );
	Leaderboard* AddLeaderboard();

	const LeaderboardConfigList& GetLeadeboardList() const {return m_leaderboards; }
	const atArray<LeaderboardInput>& GetInputList() const { return m_inputs; }
private:
	
	atArray<LeaderboardInput> m_inputs;
	atArray<LeaderboardCategory> m_categories;
	LeaderboardConfigList m_leaderboards;

};

struct ScsGameConfiguration
{
	LeaderboardsConfiguration m_leaderboardConfiguration;
	char m_gameTitle[8];
	u32 m_configVersion;
	u32 m_fileVersion;
};

class netSCGamerConfigParser
{
public:

	static void Init(const char* gameconfigFileName);
	//void Shutdown();

#if __BANK
	static void Bank_InitWidgets(bkBank* bank);
	static void	Bank_CreateWidgets();
	bkGroup*	m_bankGroup;
	void Bank_DoCreateWidgets();
#endif

	const LeaderboardsConfiguration::LeaderboardConfigList& GetLeaderboardList() const;

	//PURPOSE:
	//  Get the leaderboard given various identifiers
	const Leaderboard* GetLeaderboard(const char* name) const;
	const Leaderboard* GetLeaderboard(const u32 lbId) const;

	//PURPOSE:
	// Given a leaderboard, get a list of the inputs needed for that leaderboard. 
	// Returns the number of Inputs added to the list
	typedef atArray<const LeaderboardInput*> LeaderboardInputList;
	int GetInputsOnLeaderboard(const Leaderboard& rLeaderboard, LeaderboardInputList& out_InputList) const;

	//PURPOSE:
	// Given an input Id, get the LeaderboardInput for it.
	const LeaderboardInput* GetInputById(const u32 inputId) const;

	//PURPOSE:
	//	Helper function to get a rlLeaderboard2GroupSelector filled with the categories of a given CategoryGroup
	int GetSelectorFromCategoryGroup(const LeaderboardCategoryGroup& in_lbCatGroup, rlLeaderboard2GroupSelector& out_grpSelctor) const;

	//PUPROSE:
	//  Given a leaderboard, filles in the the set of category groups for this leaderboard
	const int GetCategoryGroupsForLeaderboard(const Leaderboard& rLeaderboard, atArray<rlLeaderboard2GroupSelector>& out_Groups) const;

private:
	//The one true game config that gets loaded
	ScsGameConfiguration m_config;
	
	bool LoadDataXMLFile(const char* fileName);


	//Callback and xml parsing support
	void  CallBackOnBeginElement(parElement& elt, bool isLeaf);
	void  CallBackOnEndElement(bool isLeaf);
	void  CallBackOnData(char* data, int size, bool dataIncomplete);
	
	void ParseInputDef( parElement& elt );
	void ParseCategoryDef( parElement& elt);
	void ParseLeaderboardDef( parElement& elt );
	void ParseCategoryPermDef( parElement& elt );
	void ParseColumnDef( parElement& elt );
	void ParseAggregation( parElement& elt );
	void ParseAggGameInput( parElement& elt );
	void ParseAggColumnInput( parElement& elt );
	
	Leaderboard* m_pCurrentLeaderboard; //Current Leaderboard Item we're processing.
	LeaderboardColumn* m_pCurrentLeaderboardColumn; //Current column we're working on.
	bool m_bInCategoryPermBlock;
};
typedef atSingleton<netSCGamerConfigParser> netSCGamerConfigParserSingleton;
#define GAME_CONFIG_PARSER netSCGamerConfigParserSingleton::GetInstance()

#endif // !INC_NETSCGAMECONFIGPARSER_H_
