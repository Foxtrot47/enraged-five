//
// netobjectmgrbase.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_OBJECT_MGR_BASE_H
#define NETWORK_OBJECT_MGR_BASE_H

// rage headers
#include "atl/binmap.h"
#include "atl/dlist.h"
#include "data/base.h"

// framework headers
#include "fwnet/netobjectidmgr.h"
#include "fwnet/netobjectmgrmessagehandler.h"
#include "fwnet/netobjectreassignmgr.h"
#include "fwnet/netsyncdataul.h"
#include "fwnet/netsyncnode.h"
#include "fwnet/netutils.h"
#include "fwtl/pool.h"

namespace rage
{
    class netBandwidthMgr;
    class netGameObjectBase;

//PURPOSE
// This class manages the cloning and synchronization of game objects that are shared across remote
// machines in a network game. See netobject.h for a full description of how this is done.
class netObjectMgrBase
{
    friend class netObjectReassignMgr;
    friend class netObjectMgrMessageHandler;

private:

    // The maximum number of concurrent network objects that can be active at any time in the network game
    static const unsigned int MAX_NUM_TOTAL_NETWORK_OBJECTS = netObjectIDMgr::MAX_TOTAL_OBJECT_IDS;

    //PURPOSE
    // This structure is used for recording which update data was sent for an individual object in a sync message
    struct syncMessageObjectInfo
    {
        ObjectId      m_ObjectID;  // The ID of the object recorded
        DataNodeFlags m_NodeFlags; // The node flags indicating which data was sent in the sync message (see netsyncnode.h for details)
    };

    //PURPOSE
    // This is a convenience class for allocating the syncMessageObjectInfo list nodes from a pool
    class syncMessageInfoNode
    {
    public:
        FW_REGISTER_CLASS_POOL(syncMessageInfoNode);

        syncMessageInfoNode() : m_Next(0) {}

        syncMessageObjectInfo m_Data;
        syncMessageInfoNode  *m_Next;
    };

    //PURPOSE
    // This structure is used for storing which object updates were sent out in a sync message
    class syncMessageInfo
    {
    public:

    FW_REGISTER_CLASS_POOL(syncMessageInfo);

        syncMessageInfo() : m_MessageSeqNum(0), m_TimeSent(0), m_Expired(false), m_ObjectList(0) {}
        ~syncMessageInfo() { Assert(m_ObjectList == 0); }

        netSequence          m_MessageSeqNum; // the sequence number of the sync message
        u32                  m_TimeSent;      // the system time the message was sent
        bool                 m_Expired;       // indicates this sync info has expired
        syncMessageInfoNode *m_ObjectList;    // a list of objects that were in the message

        bool operator==(const syncMessageInfo& info) const { return info.m_MessageSeqNum == m_MessageSeqNum; }

        inlist_node<syncMessageInfo> m_ListLink;
    };

    // typedef for code readability
    typedef inlist<syncMessageInfo, &syncMessageInfo::m_ListLink> SyncMessageInfoQueue;

public:

    //PURPOSE
    // This is a convenience class for allocating the list nodes from a pool
    class atDNetObjectNode : public atDNode<netObject*, datBase>
    {
    public:
        FW_REGISTER_CLASS_POOL(atDNetObjectNode);
    };

    //PURPOSE
    // Enumeration of reasons network objects have been unregistered, used
    // for logging purposes
    enum UnregistrationReason
    {
        SHUTTING_DOWN,
        GAME_UNREGISTRATION,
        DELETED_VIA_UPDATE,
        REASSIGNMENT_SHUTDOWN,
        REASSIGNMENT_NO_REASSIGN,
        REASSIGNMENT_NO_NEW_OWNER,
        REASSIGNMENT_REMOVAL,
        USER_REASON                 // derived class can specify there own unregistration reasons, they should start at this value
    };

    //PURPOSE
    // Class Constructor
    //PARAMS
    // bandwidthMgr - The bandwidth manager for recording bandwidth usage by the object manager
    netObjectMgrBase(netBandwidthMgr &bandwidthMgr);
    virtual ~netObjectMgrBase();

    netLoggingInterface			&GetLog()				{ return *m_Log; }
    netObjectIDMgr				&GetObjectIDManager()	{ return m_ObjectIDMgr; }
	netObjectReassignMgr		&GetReassignMgr()		{ return m_ObjectReassignMgr; }
	const netObjectReassignMgr  &GetReassignMgr()const  { return m_ObjectReassignMgr; }
    netBandwidthMgr				&GetBandwidthMgr()		{ return m_BandwidthMgr; }

#if __BANK

    //PURPOSE
    // Returns the average number of times GetNetworkObject() is being called per update
    float GetAverageNumGetNetworkObjectCalls() const { return ms_AverageGetNetworkObjectCalls.GetAverage(); }

    //PURPOSE
    // Returns the number of clone sync messages sent since the last call to ResetMessageCounts()
    unsigned GetNumCloneSyncMessagesSent() const { return GetMessageHandler().GetNumCloneSyncMessagesSent(); }   

    //PURPOSE
    // Returns the number of clone sync ack messages sent since the last call to ResetMessageCounts()
    unsigned GetNumCloneSyncAckMessagesSent() const { return GetMessageHandler().GetNumCloneSyncAckMessagesSent(); }   

    //PURPOSE
    // Returns the number of packed reliable messages sent since the last call to ResetMessageCounts()
    unsigned GetNumPackedReliableMessagesSent() const { return GetMessageHandler().GetNumPackedReliableMessagesSent(); }   

    //PURPOSE
    // Reset the counts of object manager messages sent
    void ResetMessageCounts() { GetMessageHandler().ResetMessageCounts(); GetReassignMgr().ResetMessageCounts(); }

    unsigned GetLastSyncTreeUpdateDuration() const { return m_LastSyncTreeUpdateDuration; }
#endif // __BANK

    //PURPOSE
    // Initialises the object manager at the start of the network session. This can be overridden by
    // derived classes, but this version of the Init() function should still be called
    virtual bool Init();

    //PURPOSE
    // Shuts down the object manager at the end of the network session. This can be overridden by
    // derived classes, but this version of the Shutdown() function should still be called
    virtual void Shutdown();

    //PURPOSE
    // Performs any per frame processing required by the object manager. This should be called by
    // your application once per frame
    //PARAMS
    // updateNetworkObjects - Flag indicating whether the network objects managed by the class
    //                        should be updated. This may be undesirable if this function is being
    //                        called more than once per frame (which may be necessary if the game is
    //                        doing a blocking streaming operation)
    virtual void Update(bool updateNetworkObjects);

    //PURPOSE
    // Registers a game object with the manager, to be synchronised on other machines.
    //PARAMS
    // gameObject  - the game object to register with the manager
    // objectID    - the unique identifier for the object within the game session
    // localFlags  - the local flags for the network object (flags not synchronised to remote machines)
    // globalFlags - the global flags for the network object (flags synchronised to remote machines)
    virtual void RegisterObject(netGameObjectBase     *gameObject,
                                const ObjectId        &objectID,
                                const NetObjFlags      localFlags,
                                const NetObjFlags      globalFlags);

    //PURPOSE
    // Unregisters the game object from the object manager and informs all other remote machines
    // to remove their clone of this object.
    //PARAMS
    // gameObject  - the game object to unregister from the manager
    // force       - Flag indicating that the object is being forcibly removed
    virtual void UnregisterObject(netGameObjectBase* gameObject, bool force = false);

    //PURPOSE
    // Unregisters a network object with the manager and removes clones on remote machines if necessary
    //PARAMS
    // object             - The network object to unregister
    // reason             - The reason the network object is being unregistered (used for debugging purposes)
    // force              - Flag indicating that the object is being forcibly removed
    // destroyObject	  - Flag indicating whether the game object associated with this network object should be destroyed
    virtual void UnregisterNetworkObject(netObject *object, unsigned reason, bool force, bool destroyObject);

    //PURPOSE
    // Finds and returns the network object with the given object ID using the binary map lookup.
    //PARAMS
    // objectID   - The ID of the network object to retrieve
    // includeAll - If this flag is set the function will also return unregistering objects and those being reassigned
    netObject* GetNetworkObject(const ObjectId objectID, bool includeAll = false);

    //PURPOSE
    // Finds and returns the network object with the given object ID using a linear search across all player object lists.
    //PARAMS
    // objectID   - The ID of the network object to retrieve
    // includeAll - If this flag is set the function will also return unregistering objects and those being reassigned
    netObject* GetNetworkObjectSlow(const ObjectId objectID, bool includeAll = false);

    //PURPOSE
    // Finds and returns the network object with the given global object ID if they are owned by the specified network player.
    //PARAMS
    // objectID - The ID of the network object to retrieve
    // player   - The network player owning the object
    // includeAll - If this flag is set the function will also return unregistering objects and those being reassigned
    netObject* GetNetworkObjectFromPlayer(const ObjectId objectID, const netPlayer &player, bool includeAll = false);

    //PURPOSE
    // Changes the ownership of a network object
    //PARAMS
    // object           - The network object changing owner
    // player           - The new owner player
    // migrationType	- indicates how the ownership was changed 
    virtual void ChangeOwner(netObject &object, const netPlayer &player, eMigrationType migrationType);

    //PURPOSE
    // Returns whether the manager is in the process of shutting down
    bool IsShuttingDown() const { return m_ShuttingDown; }

    //PURPOSE
    // Called when a new player joins the game
    //PARAMS
    // player - the player joining the game
    virtual void PlayerHasJoined(const netPlayer &player);

    //PURPOSE
    // Called when an existing player leaves the game
    //PARAMS
    // player - the player leaving the game
    virtual void PlayerHasLeft(const netPlayer &player);
 
    //PURPOSE
    // Updates all network objects owned by the specified player to the other remote players,
    // subject to available bandwidth. This involves checking whether objects should be created
    // or removed on the player's remote machine, as well as sending synchronisation updates to the player
    //PARAMS
    // player - the local player to update objects on remote players
    virtual void UpdateAllObjectsForRemotePlayers(const netPlayer& sourcePlayer) = 0;

    //PURPOSE
    // Updates the scope state of the network object on all players immediately.
    // This state is generally updated in batch over multiple frames, but in certain
    // critical events (such as when an object is first created) it is important to update this
    // immediately
    //PARAMS
    // object - The network object to update scope state for
    void UpdateAllInScopeStateImmediately(netObject *object);

    //PURPOSE
    // This function is called when an object reassignment process has finished
    virtual void OnReassignmentFinished() {}

    //PURPOSE
    // Returns the number of local network objects that satisfy the conditions defined by the supplied predicate function
    //PARAMS
    // predicate - Predicate function to determine whether to include an object in the count
    int GetNumLocalObjects(fnNetObjectPredicate predicate = 0) const;

    //PURPOSE
    // Returns the number of network objects controlled by other machines that satisfy
    // the conditions defined by the supplied predicate function
    //PARAMS
    // predicate - Predicate function to determine whether to include an object in the count
    int GetNumRemoteObjects(fnNetObjectPredicate predicate = 0) const;

    //PURPOSE
    // Returns the total number of network objects currently in the game
    //PARAMS
    // predicate - Predicate function to determine whether to include an object in the count
    int GetTotalNumObjects(fnNetObjectPredicate predicate = 0) const;

    //PURPOSE
    // Returns the total number of locally owned network objects, and total number of sync targets (total number of
    // clones across all machines)
    //PARAMS
    // numLocalNetworkObjects - The total number of locally owned network objects
    // numTotalSyncTargets    - The total number of sync targets (total number of clones across all machines)
    void GetLocallyOwnedObjectsInfo(unsigned int &numLocalNetworkObjects, unsigned int &numTotalSyncTargets) const;

	//PURPOSE
	// Fills an array with all of the objects currently held in the manager
	//PARAMS
	//  objectArray - the array
	//	sizeofArray - the size of the array
	//  includeReassignments - if set, then all of the objects held in the reassignment manager are also included
	//  scriptObjectsOnly - if set, then only script objects are considered
	//  includeNoGameObjects - if set, include network objects with no game object are also included
	//RETURNS
	// The number of objects in the array
	unsigned int GetAllObjects(netObject **objectArray, unsigned int sizeofArray, bool includeReassignments, bool scriptObjectsOnly, bool includeNoGameObjects = false);

    //PURPOSE
    // Returns the object IDs currently in use by the local machine within the specified ID range
    //PARAMS
    // idsInUseLocally    - array for returning the IDs in use
    // numInUseIDs        - the number of IDs with the specified range in use
    // numLocallyOwnedIDs - the number of in use IDs controlled by this machine
    // idStart            - The start of the ID range to check
    // idEnd              - The end of the ID range to check
    void GetObjectIDsInUseLocally(ObjectId *idsInUseLocally, unsigned &numInUseIDs, unsigned &numLocallyOwnedIDs, unsigned idStart, unsigned idEnd);

    //PURPOSE
    // Runs the specified callback function on all locally owned network objects
    //PARAMS
    // callback - The function to call on all of the locally owned network objects
    // param    - Any parameter used by the function (can be pointer to a struct containing multiple parameters)
    void ForAllLocalObjects(fnNetObjectCallback callback, void *param);

    //PURPOSE
    // Runs the specified callback function on all remotely owned network objects
    //PARAMS
    // callback - The function to call on all of the remotely owned network objects
    // param    - Any parameter used by the function (can be pointer to a struct containing multiple parameters)
    void ForAllRemoteObjects(fnNetObjectCallback callback, void *param);

    //PURPOSE
    // Runs the specified callback function on all network objects
    //PARAMS
    // callback - The function to call on all of the network objects
    // param    - Any parameter used by the function (can be pointer to a struct containing multiple parameters)
    void ForAllObjects(fnNetObjectCallback callback, void *param);

    //PURPOSE
    // Forcibly removes an unregistering network object
    //PARAMS
    // object - The object to forcibly remove
    void ForceRemovalOfUnregisteringObject(netObject *object);

    //PURPOSE
    // Returns the log name of the network object with the specified global ID
    //PARAMS
    // logName  - Storage for the returned log name
    // objectID - The ID of the network object to return the logging name
    void GetLogName(char *logName, size_t logNameSize,const ObjectId objectID);

    //PURPOSE
    // Returns the system time the sync message with the specified sequence was sent to the specified player. Also checks whether this
    // message has expired so we can track unreliable resend failures without having to search the message history twice
    //PARAMS
    // playerIndex     - The physical player index of the player sent the sync message
    // syncMessageSeq  - The sequence number of the sync message to check
    // expiryTime      - The system time the sync message expires
    unsigned GetTimeSyncMessageSentAndCheckExpiry(PhysicalPlayerIndex playerIndex, netSequence syncMessageSeq, unsigned expiryTime);

    //PURPOSE
    // Returns the number of sync updates sent in the last update
    unsigned GetNumUpdatesSent() const { return m_NumUpdatesSent; }

	//PURPOSE
	// Reset the proximity control timer for all local objects.
	// Provide this as a method here in case it is needed in more than one location.
	//PARAMS
	//
	void ForAllLocalObjectsResetProximityControlTimer();
	
    //PURPOSE
    // Returns the average latency for data received from the specified remote player
    float GetAverageLatency(PhysicalPlayerIndex playerIndex) const;

    //PURPOSE
    // Returns the average ping for data sent/received to the specified remote player
    float GetAveragePing(PhysicalPlayerIndex playerIndex) const;

    //PURPOSE
    // Returns the packet loss for data sent/received to the specified remote player
    float GetPacketLoss(PhysicalPlayerIndex playerIndex) const;

	//PURPOSE
	// Force sort the network map (e.g. to ensure GetNetworkObject() is thread safe).
	void ForceSortNetworkObjectMap();

	//PURPOSE
	// Allow other threads access to certain code if we believe it will be safe.
	void AllowOtherThreadsAccess(bool bAllowAccess) { m_bAllowOtherThreadsAccess = bAllowAccess; } 

	// PURPOSE
	// Forces crashing out on network shutdown if it happens while a sync message is being processed
	static void SetForceCrashOnShutdownWhileProcessing(bool set) { sm_forceCrashOnShutdownWhileProcessing = set; }
	static bool GetForceCrashOnShutdownWhileProcessing() { return sm_forceCrashOnShutdownWhileProcessing; }

protected:

    //PURPOSE
    // Sets the logging object for message handling
    //PARAMS
    // log - The logging object for message handling
    void SetMessageHandlerLog(netLoggingInterface *log) { m_MessageHandler->SetLog(log); }

    //PURPOSE
    // Updates the specified network object on all remote players, subject to available bandwidth
    // This involves checking whether the objects should be created or removed on the player's
    // remote machine, as well as sending synchronisation updates to the player
    //PARAMS
    // networkObject - The network object to update
    void UpdateObjectOnAllPlayers(netObject *networkObject);

    //PURPOSE
    // Registers a new network object with the manager and initialises this object. Then clones this object on
    // remote machines if necessary
    //PARAMS
    // object - The network object to register
    virtual void RegisterNetworkObject(netObject *object);

    //PURPOSE
    // Destroys an object once it has been unregistered properly
    //PARAMS
    // object - The network object to destroy
    // force  - Flag indicating whether the network object should be forcibly removed
    void DestroyNetworkObject(netObject *object, bool force = false);

    //PURPOSE
    // Clones an object on the given player (sends a creation message)
    //PARAMS
    // object     - The network object to create on the remote machine
    // player     - The network player to create the object on
    // createData - Buffer to store creation data for this object, can be reused for multiple creates
    virtual void CloneObject(netObject *object, const netPlayer &player, datBitBuffer &createData);

    //PURPOSE
    // Removes an object's clone on the given player (sends a removal message)
    //PARAMS
    // object    - The network object to remove on the remote machine
    // player    - The network player to remove the object on
    // forceSend - Flag indicating the remove message must be sent (regardless of bandwidth constraints)
    virtual bool RemoveClone(netObject *object, const netPlayer &player, bool forceSend = false);

    //PURPOSE
    // Syncs an object with its remote clone on the given player (sends state updates)
    //PARAMS
    // object    - The network object to sync on the remote machine
    // player    - The network player to sync the object on
    virtual bool SyncClone(netObject *object, const netPlayer &player);

    //PURPOSE
    // Called when a clone sync message is packed for the specified object
    //PARAMS
    // object    - object being packed
    // timestamp - timestamp the message is tagged with
    virtual void OnSendCloneSync(const netObject *UNUSED_PARAM(object), unsigned UNUSED_PARAM(timestamp)) {};

    //PURPOSE
    // Called when a connection sync message (containing multiple packed sync messages) is sent for the specified object
    //PARAMS
    // timestamp - timestamp the message is tagged with
    virtual void OnSendConnectionSync(unsigned UNUSED_PARAM(timestamp)) {};
    

    //PURPOSE
    // Checks whether a network object has finished unregistering and handles the cleanup
    //PARAMS
    // object - The network object to check
    void CheckForUnregistrationProcessComplete(netObject *object);

    //PURPOSE
    // Returns a string describing the reason a network object has been unregistered
    virtual const char *GetUnregistrationReason(unsigned reason) const;

    //PURPOSE
    // Fills the specified array with the object lists for all local players, and returns
    // the number of lists retrieved
    //PARAMS
    // localObjectLists - An array to retrieve the object lists
    unsigned GetLocalObjectLists(const atDList<netObject*, datBase> *localObjectLists[MAX_NUM_PHYSICAL_PLAYERS]) const;

private:

    //PURPOSE
    // Processes object creation data received from a remote machine. This must be implemented by derived classes.
    //PARAMS
    // fromPlayer              - The network player in control of the object being created
    // toPlayer                - The network player this create data is for
    // objectType              - The object type of the object being created
    // objectID                - The ID of the new object
    // flags                   - Generic flags for the object
    // createDataMessageBuffer - Creation data for the new object
    // timestamp               - Timestamp for the creation data
    virtual void ProcessCloneCreateData(const netPlayer   &fromPlayer,
                                        const netPlayer   &toPlayer,
                                        NetworkObjectType  objectType,
                                        const ObjectId     objectID,
                                        NetObjFlags        flags,
                                        datBitBuffer      &createDataMessageBuffer,
                                        unsigned           timestamp) = 0;

    //PURPOSE
    // Processes object creation ACK data received from a remote machine. This must be implemented by derived classes.
    //PARAMS
    // fromPlayer      - The network player sending the ACK data
    // toPlayer        - The network player the ACK data is for
    // peerPlayerIndex - The index of the player on the local machine the ACK is for
    // objectID        - The ID of the object being ACKed
    // ackCode         - The ACK code received
    virtual void ProcessCloneCreateAckData(const netPlayer &fromPlayer,
                                           const netPlayer &toPlayer,
                                           ObjectId         objectID,
                                           NetObjectAckCode ackCode) = 0;

    //PURPOSE
    // Processes object updates received from a remote machine. This must be implemented by derived classes.
    //PARAMS
    // fromPlayer      - The player the update message has been received from
    // toPlayer        - The player the update message is for
    // objectType      - The object type of the object being updated
    // objectID        - The ID of the object being updated
    // msgBuffer       - The message buffer containing the update data
    // sequenceNum     - The sequence number of the message this update was received in
    // timeStamp       - Timestamp of the message this update was received in
    virtual NetObjectAckCode ProcessCloneSync(const netPlayer        &fromPlayer,
                                              const netPlayer        &toPlayer,
                                              const NetworkObjectType objectType,
                                              const ObjectId          objectID,
                                              datBitBuffer           &msgBuffer,
                                              const netSequence       seqNum,
                                              u32                     timeStamp) = 0;

    //PURPOSE
    // Processes object update ACKs received from a remote machine. This must be implemented by derived classes.
    //PARAMS
    // fromPlayer      - The player the ACK was received from
    // toPlayer        - The player the ACK is for
    // syncSequence    - The sequence number of the message the update being ACKed was received in
    // objectID        - The object ID of the object being ACKed
    // ackCode         - The ACK code received
    virtual void ProcessCloneSyncAckData(const netPlayer        &fromPlayer,
                                         const netPlayer        &toPlayer,
                                         netSequence             syncSequence,
                                         ObjectId                objectID,
                                         NetObjectAckCode        ackCode) = 0;

    //PURPOSE
    // Processes object removal data received from a remote machine. This must be implemented by derived classes.
    //PARAMS
    // fromPlayer     - The player the object removal data was received from
    // toPlayer       - The player the object removal data is for
	// objectID       - The ID of the object to remove
    // ownershipToken - The latest ownership token of the object (this can be used to determine whether the local machine
    //                  is completely up-to-date with the number of ownership changes that have been made to the object.
    virtual void ProcessCloneRemoveData(const netPlayer &fromPlayer,
                                        const netPlayer &toPlayer,
                                        ObjectId         objectID,
                                        u32              ownershipToken) = 0;

    //PURPOSE
    // Processes object removal ACKs received from a remote machine. This must be implemented by derived classes.
    //PARAMS
    // fromPlayer - The player the object removal ACK data was received from
    // toPlayer   - The player the object removal ACK data is for
	// objectID   - The ID of the object being ACKed
    // ackCode    - The ACK code received
    virtual void ProcessCloneRemoveAckData(const netPlayer &fromPlayer,
                                           const netPlayer &toPlayer,
                                           ObjectId         objectID,
                                           NetObjectAckCode ackCode) = 0;

    //PURPOSE
    // Updates all network objects (called once per frame)
    void UpdateAllNetworkObjects();

    //PURPOSE
    // Removes any clones that have the unregistering flag set on them
    void RemoveUnregisteringClones();

    //PURPOSE
    // Records which sections of data were sent to a player for an individual object in an individual
    // update message. This information is used calculate which data is being acknowledged when an
    // ACK message arrives containing the sequence number of the message being ACKed
    //PARAMS
    // player          - The player the data being recorded was sent to
    // messageSeqNum   - The sequence number of the message containing the update data being recorded
    // objectID        - The ID of the network object update data was sent for
    // nodeFlags       - The node flags indicating which data was sent in the update being recorded
    void AddNewObjectInfoToSyncMessageInfoQueue(const netPlayer& player, netSequence messageSeqNum, ObjectId objectID, DataNodeFlags nodeFlags);

    //PURPOSE
    // Takes a sequence number received in an ACK message and ackowledges data sent for the objects updated in the message
    //PARAMS
    // fromPlayer     - The player the data was sent to
    // toPlayer       - The player controlling the message being ACKed
    // messageSeqNum  - The sequence number of the message being ACKed
    // failedSyncIDs  - An array of IDs of any objects that did not receive a success ACK code
    // numFailedSyncs - The number of IDs in the failed sync array
    // outOfOrder     - The message was ACKed as out of order, so we just remove the related item from the history
    void ProcessSyncAckInSyncMessageInfoQueues(const netPlayer &fromPlayer,
                                               const netPlayer &toPlayer,
                                               netSequence      messageSeqNum,
                                               ObjectId        *failedSyncIDs,
                                               unsigned         numFailedSyncs,
                                               bool             outOfOrder);

    //PURPOSE
    // Adds a sync latency sample to be used in the average latency calculation. This is a one way latency
    // from a remote player sending us a network message.
    //PARAMS
    // playerIndex - The player index of the remote player the latency sample is associated with
    // latency     - The latency sample to add to the calculation in milliseconds
    void AddSyncLatencySample(PhysicalPlayerIndex playerIndex, unsigned latency);

    //PURPOSE
    // Adds a sync ping sample to be used in the average RTT calculation. This is the round trip time
    // between the local player sending a message to a remote player and receiving ackowledgement
    //PARAMS
    // playerIndex - The player index of the remote player the ping sample is associated with
    // latency     - The ping sample to add to the calculation in milliseconds
    void AddSyncPingSample(PhysicalPlayerIndex playerIndex, unsigned ping);

protected:

    //PURPOSE
    // Accessor to the message handler for derived classes
    const netObjectMgrMessageHandler &GetMessageHandler() const { return *m_MessageHandler; }
          netObjectMgrMessageHandler &GetMessageHandler()       { return *m_MessageHandler; }

    //PURPOSE
    // Returns the string name corresponding to the specified ack code
    //PARAMS
    // ackCode - The ack code to retrieve the name for
    const char* GetAckCodeName(NetObjectAckCode ackCode) const;

    //PURPOSE
    // Returns whether clone sync messages sent to the specified player for the specified object should be send immediately (ignoring the send interval)
    //PARAMS
    // player        - The remote player the clone sync data is being sent to
    // networkObject - The network object associated with the clone sync data
    virtual bool ShouldSendCloneSyncImmediately(const netPlayer &UNUSED_PARAM(player), const netObject &UNUSED_PARAM(networkObject)) { return false; }

    //PURPOSE
    // Removes an object from all message info queues
    //PARAMS
    // objectID - The ID of the object to remove from the message info queues
    void RemoveObjectFromSyncMessageInfoQueues(ObjectId objectID);

    // The array holding the lists of objects owned by each remote player in the game
    atDList<netObject*, datBase> m_aPlayerObjectList[MAX_NUM_PHYSICAL_PLAYERS];

    void SetPlayersToUpdate(unsigned playersToUpdate) { m_PlayersToUpdate = playersToUpdate; }

private:

#if __ASSERT
    bool m_AssertOnUnregister; // used to catch network objects being unregistered when not allowed (iterating all objects for example)
#endif // __ASSERT

#if __BANK
    void AssertObjectOnList(atDList<netObject*, datBase> &list, netObject &networkObject);
    void CheckNetworkObjectMapForDuplicates(const char *locationOfCheck);
#endif // __BANK
#if __DEV
    void ValidateObjectLists();
    void ValidateSyncMessageInfos();
#endif // __DEV

    static const unsigned MAX_UPDATE_SAMPLES = 30;
    static const unsigned SAMPLE_INTERVAL    = 1000;
    DataAverageCalculator<MAX_UPDATE_SAMPLES, SAMPLE_INTERVAL> m_AverageLatency[MAX_NUM_PHYSICAL_PLAYERS];
    DataAverageCalculator<MAX_UPDATE_SAMPLES, SAMPLE_INTERVAL> m_AveragePing[MAX_NUM_PHYSICAL_PLAYERS];

#if __BANK
    unsigned m_LastSyncTreeUpdateDuration; // Used for debugging performance of the sync tree updates
    unsigned ms_NumGetNetworkObjectCalls;  // Used to count the number of calls to GetNetworkObject() in an update

    DataAverageCalculator<MAX_UPDATE_SAMPLES, SAMPLE_INTERVAL> ms_AverageGetNetworkObjectCalls;

#endif // __BANK

	static bool					sm_forceCrashOnShutdownWhileProcessing;
    bool                        m_IsInitialized;        // Set when the manager has been initialised
    bool                        m_ShuttingDown;         // Set when manager is shutting down
    netLog                     *m_Log;                  // Logging object for the object manager class
    netObjectIDMgr              m_ObjectIDMgr;          // Object ID manager   
    netObjectReassignMgr        m_ObjectReassignMgr;    // Object reassign manager
    netObjectMgrMessageHandler *m_MessageHandler;       // Object manager message handler
    netBandwidthMgr&            m_BandwidthMgr;         // The bandwidth manager
    unsigned                    m_NumUpdatesSent;       // Number of object updates sent this frame
    unsigned                    m_PlayersToUpdate;      // Bitmask of players we can send updates to this frame

    // An array storing what sync states were sent out with each object the most recent sync messages
    atFixedArray<SyncMessageInfoQueue, MAX_NUM_PHYSICAL_PLAYERS> m_aSyncMessageInfo;

    // Binary map of network objects for faster lookup
    atBinaryMap<netObject *, ObjectId> m_NetworkObjectMap;

	bool m_bAllowOtherThreadsAccess;
};

} // namespace rage

#endif  // NETWORK_OBJECT_MGR_BASE_H
