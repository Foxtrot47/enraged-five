// 
// netleaderboardread.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

// rage headers
#include "rline/rlstats.h"
#include "rline/rlschema.h"
#include "rline/rlgamerinfo.h"
#include "rline/clan/rlclancommon.h"
#include "diag/channel.h"
#include "system/timer.h"
#if !__NO_OUTPUT
#include "diag/output.h"
#endif

// framework headers
#include "netleaderboardread.h"
#include "fwnet/netscgameconfigparser.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, leaderboard_read, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_leaderboard_read


const char*  StatNameFromId(const unsigned id)
{
	switch (id)
	{
	case RL_STAT_TYPE_CONTEXT: return "context";
	case RL_STAT_TYPE_INT32:   return "int32";
	case RL_STAT_TYPE_INT64:   return "int64";
	case RL_STAT_TYPE_DOUBLE:  return "double";
	case RL_STAT_TYPE_UNICODE: return "unicode";
	case RL_STAT_TYPE_FLOAT:   return "float";
	case RL_STAT_TYPE_BINARY:  return "binary";
	case RL_STAT_TYPE_NULL:    return "null";

	default:
		gnetAssertf(0, "Invalid column id=\"%u\"", id);
	}

	return "unknown";
}

const char* GetReadTypeString(const rlLeaderboard2Type type)
{
	if (type == RL_LEADERBOARD2_TYPE_PLAYER)
	{
		return "TYPE_PLAYER";
	}
	else if (type == RL_LEADERBOARD2_TYPE_CLAN)
	{
		return "TYPE_CLAN";
	}
	else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
	{
		return "TYPE_CLAN_MEMBER";
	}
	else if (type == RL_LEADERBOARD2_TYPE_GROUP)
	{
		return "TYPE_GROUP";
	}
	else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
	{
		return "TYPE_GROUP_MEMBER";
	}

	return "TYPE_UNKNOWN";
}

int 
netLeaderboardInfo::GetColumnIndex(const unsigned columnId) const
{
	for(unsigned i=0; i<m_NumColumnIds; ++i)
	{
		if(columnId == m_ColumnIds[i].Id)
		{
			return i;
		}
	}

	return -1;
}

netLeaderboardRead::netLeaderboardRead(const unsigned id, const rlLeaderboard2Type type)
: m_RowHeap(NULL)
, m_RowPtrs(NULL)
, m_StatValueHeap(NULL)
, m_NumRowsReturned(0)
, m_NumRowsInLeaderboard(0)
, m_LastReadTime(0)
, m_Allocator(0)
{
	Init(id, type);
}

netLeaderboardRead::~netLeaderboardRead()
{
	Shutdown();
}

void
netLeaderboardRead::Init(const unsigned id, const rlLeaderboard2Type type)
{
	gnetAssert(0 == m_Info.m_Id);
	gnetAssert(!m_Info.m_ColumnIds);
	gnetAssert(0 == m_Info.m_NumColumnIds);

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return;

	gnetDebug2("Init for Reading Leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), id, GetReadTypeString(type));

	m_Info.m_Id           = id;
	m_Info.m_ReadType     = type;
	m_Info.m_NumColumnIds = 0;

	m_NumRowsInLeaderboard = 0;
	m_NumRowsReturned      = 0;
	m_LastReadTime         = 0;

	m_MyStatus.Reset();
}

void
netLeaderboardRead::Cancel()
{
	if (m_MyStatus.Pending())
	{
		OUTPUT_ONLY(const Leaderboard* pLB = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id));
		gnetWarning("Cancel read operation leaderboard id=\"%s:%d\""
				,pLB ? pLB->GetName() : ""
				,m_Info.m_Id);

		if (m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER)
		{
			rlLeaderboard2PlayerStats::Cancel(&m_MyStatus);
		}
		else if (m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN)
		{
			rlLeaderboard2ClanStats::Cancel(&m_MyStatus);
		}
		else if (m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		{
			rlLeaderboard2ClanMemberStats::Cancel(&m_MyStatus);
		}
		else if (m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP)
		{
			rlLeaderboard2GroupStats::Cancel(&m_MyStatus);
		}
		else if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP_MEMBER))
		{
			rlLeaderboard2GroupStats::Cancel(&m_MyStatus);
		}
	}
}

void
netLeaderboardRead::Shutdown()
{
	Cancel();

    FreeRowData();

	if(m_Info.m_ColumnIds)
	{
		Free(m_Info.m_ColumnIds);
		m_Info.m_ColumnIds = 0;
	}
	m_Info.m_NumColumnIds = 0;

	m_Info.m_Id = 0;

	m_NumRowsInLeaderboard = 0;
	m_NumRowsReturned      = 0;
	m_LastReadTime         = 0;

	m_MyStatus.Reset();

	m_Allocator = 0;
}

bool 
netLeaderboardRead::GetData(const unsigned columnId, const rlGamerHandle& gamerHandle, rlStatValue* statValue) const
{
	bool ret = false;

	if (gnetVerify(gamerHandle.IsValid())
		&& gnetVerify(m_Info.m_ColumnIds)
		&& gnetVerify(0 < GetNumRows()))
	{
		for (unsigned i=0; i<m_NumRowsReturned; i++)
		{
			if (gnetVerify(m_RowPtrs))
			{
				const rlLeaderboard2Row& row = *m_RowPtrs[i];

				if (gnetVerify(row.m_GamerHandle.IsValid()) && gamerHandle == row.m_GamerHandle)
				{
					for(unsigned col=0; col<GetNumColumns(); col++)
					{
						const unsigned id = GetColumnId(col);

						if (columnId == id)
						{
							gnetAssert(m_Info.m_NumColumnIds == row.m_NumValues);

							const unsigned index = m_Info.GetColumnIndex(columnId);

							if (gnetVerify(index < row.m_NumValues) && gnetVerify(col < row.m_MaxValues))
							{
								const rlStatValue columnValue = row.m_ColumnValues[col];

								ret = true;

								const int columnType = RL_STAT_TYPE_FROM_ID(columnId);

								if (RL_STAT_TYPE_INT32 == columnType
									|| RL_STAT_TYPE_CONTEXT == columnType
									|| RL_STAT_TYPE_DATETIME == columnType
									|| RL_STAT_TYPE_INT64 == columnType)
								{
									statValue->Int64Val = columnValue.Int64Val;
								}
								else if (RL_STAT_TYPE_FLOAT == columnType || RL_STAT_TYPE_DOUBLE == columnType)
								{
									statValue->DoubleVal = columnValue.DoubleVal;
								}
								else
								{
									ret = false;
								}
							}

							break;
						}
					}

					break;
				}
			}
		}
	}

	return ret;
}

bool 
netLeaderboardRead::GetGroupData(const unsigned columnId, rlStatValue* statValue) const
{
	bool ret = false;

	gnetAssert(m_Info.m_ColumnIds);
	gnetAssert(m_RowPtrs);

	if (m_Info.m_ColumnIds && 0 < GetNumRows() && m_RowPtrs)
	{
		const rlLeaderboard2Row& row = *m_RowPtrs[0];

		for(unsigned j=0; j<GetNumColumns(); j++)
		{
			const unsigned id = GetColumnId(j);

			if (columnId == id)
			{
				gnetAssert(m_Info.m_NumColumnIds == row.m_NumValues);

				const unsigned index = m_Info.GetColumnIndex(columnId);

				if (gnetVerify(index < row.m_NumValues) && gnetVerify(j < row.m_MaxValues))
				{
					const rlStatValue columnValue = row.m_ColumnValues[j];

					ret = true;

					const int columnType = RL_STAT_TYPE_FROM_ID(columnId);

					if (RL_STAT_TYPE_INT32 == columnType
						|| RL_STAT_TYPE_CONTEXT == columnType
						|| RL_STAT_TYPE_DATETIME == columnType
						|| RL_STAT_TYPE_INT64 == columnType)
					{
						statValue->Int64Val = columnValue.Int64Val;
					}
					else if (RL_STAT_TYPE_FLOAT == columnType || RL_STAT_TYPE_DOUBLE == columnType)
					{
						statValue->DoubleVal = columnValue.DoubleVal;
					}
					else
					{
						ret = false;
					}
				}

				break;
			}
		}
	}

	return ret;
}

const rage::rlLeaderboard2Row* 
netLeaderboardRead::GetRow(const unsigned index) const
{
	if (gnetVerify(index < GetNumRows()))
	{
		return m_RowPtrs[index];
	}

	return NULL;
}

bool
netLeaderboardRead::IsGamerPresent(const rlGamerHandle& gamerHandle) const
{
	for(unsigned i=0; i<GetNumRows(); i++)
	{
		if (gamerHandle == m_RowPtrs[i]->m_GamerHandle)
		{
			return true;
		}
	}

	return false;
}

const char*
netLeaderboardRead::GetReadTypeName() const
{
	return GetReadTypeString(GetReadType());
}

bool
netLeaderboardRead::CreateConcreteLeaderboard()
{
	bool result = false;

	gnetAssert(0 == GetNumColumns());

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return result;

	gnetDebug2("Create concrete leaderboard id=\"%s:%d\" type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	const unsigned columns = lbConf->m_columns.GetCount();
	gnetAssert(0 < columns);

	if (0 == GetNumColumns() && 0 < columns)
	{
		result = true;

		m_Info.m_NumColumnIds = columns;

		gnetAssert(!m_Info.m_ColumnIds);
		m_Info.m_ColumnIds = Allocate< rage::rlLeaderboardColumnInfo >(m_Info.m_NumColumnIds);

		if(gnetVerifyf(m_Info.m_ColumnIds != NULL, "Unable to allocate memory for leaderboard Read"))
		{
			for (unsigned idx=0; idx<columns && result; idx++)
			{
				m_Info.m_ColumnIds[idx].Id = lbConf->m_columns[idx].m_id;
				m_Info.m_ColumnIds[idx].IsRankingColumn = lbConf->m_columns[idx].m_isRakingColumn;

				const LeaderboardInput* input = GAME_CONFIG_PARSER.GetInputById( lbConf->m_columns[idx].m_aggregation.m_gameInput.m_inputId );
				if (!input)
				{
					if (lbConf->m_columns[idx].m_aggregation.m_type == AggType_Ratio)
					{
						m_Info.m_ColumnIds[idx].Type = InputDataType_double;
					}
					else
					{
						gnetAssertf(0, "FAILED to Find column Type for Create concrete leaderboard id=\"%s:%d\", Column=\"%d:%s:%d\""
							,lbConf->GetName(),m_Info.m_Id
							,idx,lbConf->m_columns[idx].GetName(),lbConf->m_columns[idx].m_id);

						result = false;
					}
				}
				else
				{
					m_Info.m_ColumnIds[idx].Type = input->m_dataType;
				}
			}
		}
	}

	return result;
}

bool
netLeaderboardRead::AllocateRowData(const unsigned numRows, const unsigned numColumns)
{
	if(gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap))
    {
		m_RowHeap = Allocate< rage::rlLeaderboard2Row >(numRows);
        if(!m_RowHeap)
        {
            FreeRowData();
            return false;
        }

		m_RowPtrs = Allocate< rage::rlLeaderboard2Row* >(numRows);
        if(!m_RowPtrs)
        {
            FreeRowData();
            return false;
        }

		m_StatValueHeap = Allocate< rage::rlStatValue >(numRows*numColumns);
        if(!m_StatValueHeap)
        {
            FreeRowData();
            return false;
        }

        unsigned heapOffset = 0;
        for(int i = 0; i < (int)numRows; ++i, heapOffset += numColumns)
        {
            m_RowHeap[i].Init(&m_StatValueHeap[heapOffset], numColumns);
            m_RowPtrs[i] = &m_RowHeap[i];
        }

        return true;
    }

    return false;
}

void
netLeaderboardRead::FreeRowData()
{
    if(m_RowHeap)
    {
        Free(m_RowHeap);
        m_RowHeap = NULL;
    }

    if(m_RowPtrs)
    {
        Free(m_RowPtrs);
        m_RowPtrs = NULL;
    }

    if(m_StatValueHeap)
    {
        Free(m_StatValueHeap);
        m_StatValueHeap = NULL;
    }
}

const unsigned 
netLeaderboardRead::GetColumnId(const unsigned index) const
{
	if (AssertVerify(m_Info.m_ColumnIds) && AssertVerify(index < m_Info.m_NumColumnIds))
	{
		return m_Info.m_ColumnIds[index].Id;
	}

	return 0;
}

const int 
netLeaderboardRead::GetColumnType(const unsigned index) const
{
	if (AssertVerify(m_Info.m_ColumnIds) && AssertVerify(index < m_Info.m_NumColumnIds))
	{
		return m_Info.m_ColumnIds[index].Type;
	}

	return 0;
}

const char*
netLeaderboardRead::GetColumnTypeName(const unsigned index) const
{
	if (AssertVerify(m_Info.m_ColumnIds) && AssertVerify(index < m_Info.m_NumColumnIds))
	{
		return StatNameFromId(m_Info.m_ColumnIds[index].Type);
	}

	return "unknown";
}




bool  
netLeaderboardRead::CanReadStatistics() const
{
	const unsigned curTime = sysTimer::GetSystemMsTime() | 0x01;
	if ((curTime - m_LastReadTime) >= READ_INTERVAL)
	{
		return true;
	}

	return false;
}

void
netLeaderboardRead::PrintStatistics() const
{
#if !__NO_OUTPUT
	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return;

	for (unsigned i=0; i<m_NumRowsReturned; i++)
	{
		const rlLeaderboard2Row& row = *m_RowPtrs[i];
		if (0 < row.m_NumValues)
		{
			gnetDebug2("..... Gamer name=\"%s\", rank=\"%u\", leaderboard id=\"%s:%d\", numcolumns=\"%d\", type=\"%s\""
							,row.m_GamerDisplayName
							,row.m_Rank
							,lbConf->GetName(), m_Info.m_Id
							,m_Info.m_NumColumnIds
							,GetReadTypeString(m_Info.m_ReadType));

			gnetAssert(m_Info.m_NumColumnIds == row.m_NumValues);

			for (unsigned j=0; j<m_Info.m_NumColumnIds; j++)
			{
				bool found = false;

				u32 idx=0;
				for (; idx<m_Info.m_NumColumnIds && !found; idx++)
				{
					found = (m_Info.m_ColumnIds[j].Id == lbConf->m_columns[idx].m_id);
				}

				if (!found)
					continue;

				switch(m_Info.m_ColumnIds[j].Type)
				{
				case RL_STAT_TYPE_CONTEXT:
				case RL_STAT_TYPE_INT32:
				case RL_STAT_TYPE_INT64:
					gnetDebug2("............ stat index=\"%d\", field=\"%d:%s:0x%08x\", value=\"%" I64FMT "d\""
															,idx
															,lbConf->m_columns[idx].m_ordinal
															,lbConf->m_columns[idx].GetName()
															,lbConf->m_columns[idx].m_id
															,row.m_ColumnValues[j].Int64Val);
					break;

				case RL_STAT_TYPE_DOUBLE:
				case RL_STAT_TYPE_FLOAT:
					gnetDebug2("............ stat index=\"%d\", field=\"%d:%s:0x%08x\", value=\"%f\""
															,idx
															,lbConf->m_columns[idx].m_ordinal
															,lbConf->m_columns[idx].GetName()
															,lbConf->m_columns[idx].m_id
															,row.m_ColumnValues[j].DoubleVal);
					break;

				case RL_STAT_TYPE_BINARY:
				case RL_STAT_TYPE_DATETIME:
				case RL_STAT_TYPE_NULL:
				case RL_STAT_TYPE_UNICODE:
					gnetWarning("............ stat index=\"%d\", field=\"%d:%s:0x%08x\", value=\"NOT_SUPPORTED\""
															,idx
															,lbConf->m_columns[idx].m_ordinal
															,lbConf->m_columns[idx].GetName()
															,lbConf->m_columns[idx].m_id);
					break;
				}
			}
		}
	}
#endif // !__NO_OUTPUT
}

void 
netLeaderboardRead::SetAllocator( sysMemAllocator* pAllocator )
{
	gnetAssert((!m_Allocator && pAllocator) || (m_Allocator && !pAllocator));
	m_Allocator = pAllocator;
}


// ---- Class netLeaderboardReadGamersByRow ----------------------------------
bool  
netLeaderboardReadGamersByRow::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned numGamers, const rage::rlGamerHandle* gamerHandles)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if(gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER || m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
		&& gnetVerify(gamerHandles) 
		&& gnetVerify(numGamers > 0) 
		&& gnetVerify(numGamers <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Max number Rows =\"%u\"", numGamers);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		bool gamersAreValid = true;
		for (unsigned i=0; i<numGamers && gamersAreValid; i++)
		{
			if(!gnetVerify(gamerHandles[i].IsValid()))
			{
				gamersAreValid = false;
				break;
			}
		}

		if (gamersAreValid)
		{
            if(gnetVerifyf(AllocateRowData(numGamers, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
			{
				ret = rlLeaderboard2PlayerStats::ReadByGamer(localGamerIndex
																,m_Info.m_Id
																,groupSelector
																,gamerHandles
																,numGamers
																,m_Info.m_ColumnIds
																,m_Info.m_NumColumnIds
																,m_RowPtrs
																,&m_NumRowsReturned
																,&m_NumRowsInLeaderboard
																,&m_MyStatus);
			}

			m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}


// ---- Class netLeaderboardReadByGamerByPlatformTask ----------------------------------
bool  
netLeaderboardReadByGamerByPlatformTask::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const char* gamerHandle, const char* platform)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if(gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER)
		&& gnetVerify(gamerHandle) 
		&& gnetVerify(platform) 
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("................. Gamer =\"%s\"", gamerHandle);
		gnetDebug2(".............. Platform =\"%s\"", platform);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(1, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			ret = rlLeaderboard2PlayerStats::ReadByGamerByPlatformTask(localGamerIndex
																	,m_Info.m_Id
																	,groupSelector
																	,gamerHandle
																	,platform
																	,m_Info.m_ColumnIds
																	,m_Info.m_NumColumnIds
																	,m_RowPtrs
																	,&m_NumRowsReturned
																	,&m_NumRowsInLeaderboard
																	,&m_MyStatus);
		}

		m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}


// ---- Class netLeaderboardReadClansByRow ----------------------------------
bool  
netLeaderboardReadClansByRow::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId* clanIds, const unsigned numClans)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if(gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN)
		&& gnetVerify(clanIds) 
		&& gnetVerify(numClans > 0) 
		&& gnetVerify(numClans <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Max number Rows =\"%u\"", numClans);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

        if(gnetVerifyf(AllocateRowData(numClans, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			ret = rlLeaderboard2ClanStats::ReadByClan(localGamerIndex
														,m_Info.m_Id
														,groupSelector
														,clanIds
														,numClans
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}

		m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClanMembersByRow ----------------------------------
bool  
netLeaderboardReadClanMembersByRow::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned numGamers, const rage::rlGamerHandle* gamerHandles)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if(gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		&& gnetVerify(gamerHandles) 
		&& gnetVerify(clanId != RL_INVALID_CLAN_ID) 
		&& gnetVerify(numGamers > 0) 
		&& gnetVerify(numGamers <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Clan Id =\"%" I64FMT "d\"", clanId);
		gnetDebug2("...... Max number Rows =\"%u\"", numGamers);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		bool gamersAreValid = true;
		for (unsigned i=0; i<numGamers && gamersAreValid; i++)
		{
			if(!gnetVerify(gamerHandles[i].IsValid()))
			{
				gamersAreValid = false;
				break;
			}
		}

		if (gamersAreValid)
		{
			if(gnetVerifyf(AllocateRowData(numGamers, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
			{
				ret = rlLeaderboard2ClanMemberStats::ReadByMember(localGamerIndex
																	,m_Info.m_Id
																	,groupSelector
																	,clanId
																	,gamerHandles
																	,numGamers
																	,m_Info.m_ColumnIds
																	,m_Info.m_NumColumnIds
																	,m_RowPtrs
																	,&m_NumRowsReturned
																	,&m_NumRowsInLeaderboard
																	,&m_MyStatus);
			}
		}

		m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadGroupsByRow ----------------------------------

bool  
netLeaderboardReadGroupsByRow::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector* groupSelectors, const unsigned numGroups)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if(gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP)
		&& gnetVerify(groupSelectors) 
		&& gnetVerify(numGroups > 0) 
		&& gnetVerify(numGroups <= MAX_LEADERBOARD_ROWS) 
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Number of Rows =\"%u\"", numGroups);
		for (unsigned i=0; i<numGroups; i++)
		{
			gnetDebug2("...... Group %d", i);
			for (unsigned j=0; j<groupSelectors[i].m_NumGroups; j++)
			{
				gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", j, groupSelectors[i].m_Group[j].m_Category, groupSelectors[i].m_Group[j].m_Id);
			}
		}

		if(gnetVerifyf(AllocateRowData(numGroups, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			ret = rlLeaderboard2GroupStats::ReadByGroup(localGamerIndex
														,m_Info.m_Id
														,groupSelectors
														,numGroups
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}

		m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;
	}
	
	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadByRank ----------------------------------

bool
netLeaderboardReadGamersByRank::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const unsigned rankStart)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER || m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
		&& gnetVerify(1 <= rankStart)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Rank start =\"%u\"", rankStart);
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2PlayerStats::ReadByRank(localGamerIndex
														,m_Info.m_Id
														,groupSelector
														,rankStart
														,maxNumRows
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClansByRank ----------------------------------

bool
netLeaderboardReadClansByRank::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const unsigned rankStart)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN)
		&& gnetVerify(1 <= rankStart)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Rank start =\"%u\"", rankStart);
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2ClanStats::ReadByRank(localGamerIndex
														,m_Info.m_Id
														,groupSelector
														,rankStart
														,maxNumRows
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClanMembersByRank ----------------------------------
bool
netLeaderboardReadClanMembersByRank::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned maxNumRows, const unsigned rankStart)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		&& gnetVerify(clanId != RL_INVALID_CLAN_ID)
		&& gnetVerify(1 <= rankStart)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... clanId =\"%" I64FMT "d\"", clanId);
		gnetDebug2("...... Rank start =\"%u\"", rankStart);
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2ClanMemberStats::ReadByRank(localGamerIndex
															,m_Info.m_Id
															,groupSelector
															,clanId
															,rankStart
															,maxNumRows
															,m_Info.m_ColumnIds
															,m_Info.m_NumColumnIds
															,m_RowPtrs
															,&m_NumRowsReturned
															,&m_NumRowsInLeaderboard
															,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadGroupsByRank ----------------------------------

bool
netLeaderboardReadGroupsByRank::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const unsigned rankStart)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP)
		&& gnetVerify(1 <= rankStart)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Rank start =\"%u\"", rankStart);
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2GroupStats::ReadByRank(localGamerIndex
														,m_Info.m_Id
														,groupSelector
														,rankStart
														,maxNumRows
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadGamerByRadius ----------------------------------
bool 
netLeaderboardReadGamersByRadius::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, unsigned int radius, const rlGamerHandle& pivotGamer)
{
	bool ret = false;

	//Number of rows is based on the radius.
	unsigned numberOfRows = radius * 2 + 1;

	//Make sure we stay within our bounds
	if (numberOfRows > MAX_LEADERBOARD_ROWS)
	{
		numberOfRows = MAX_LEADERBOARD_ROWS;
		radius = (MAX_LEADERBOARD_ROWS / 2) - 2;  //new maximum radius
	}

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER || m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
		&& gnetVerify(numberOfRows >= 1)
		&& gnetVerify(numberOfRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(pivotGamer.IsValid())
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Radius =\"%u\"", radius);
		gnetDebug2("...... Number of Rows Requested =\"%u\"", numberOfRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(numberOfRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;

			ret = rlLeaderboard2PlayerStats::ReadByRadius(localGamerIndex
															,m_Info.m_Id
															,groupSelector
															,pivotGamer
															,numberOfRows
															,m_Info.m_ColumnIds
															,m_Info.m_NumColumnIds
															,m_RowPtrs
															,&m_NumRowsReturned
															,&m_NumRowsInLeaderboard
															,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClansByRadius ----------------------------------
bool 
netLeaderboardReadClansByRadius::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, unsigned int radius, const rlClanId pivotClanId)
{
	bool ret = false;

	//Number of rows is based on the radius.
	unsigned numberOfRows = radius * 2 + 1;

	//Make sure we stay within our bounds
	if (numberOfRows > MAX_LEADERBOARD_ROWS)
	{
		numberOfRows = MAX_LEADERBOARD_ROWS;
		radius = (MAX_LEADERBOARD_ROWS / 2) - 2;  //new maximum radius
	}

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN)
		&& gnetVerify(numberOfRows >= 1)
		&& gnetVerify(numberOfRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(pivotClanId != RL_INVALID_CLAN_ID)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Pivot Clan Id =\"%" I64FMT "d\"", pivotClanId);
		gnetDebug2("...... Radius =\"%u\"", radius);
		gnetDebug2("...... Number of Rows Requested =\"%u\"", numberOfRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(numberOfRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;

			ret = rlLeaderboard2ClanStats::ReadByRadius(localGamerIndex
														,m_Info.m_Id
														,groupSelector
														,pivotClanId
														,numberOfRows
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClanMembersByRadius ----------------------------------
bool 
netLeaderboardReadClanMembersByRadius::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, unsigned int radius, const rlGamerHandle& pivotGamer, const rlClanId clanId)
{
	bool ret = false;

	//Number of rows is based on the radius.
	unsigned numberOfRows = radius * 2 + 1;

	//Make sure we stay within our bounds
	if (numberOfRows > MAX_LEADERBOARD_ROWS)
	{
		numberOfRows = MAX_LEADERBOARD_ROWS;
		radius = (MAX_LEADERBOARD_ROWS / 2) - 2;  //new maximum radius
	}

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		&& gnetVerify(numberOfRows >= 1)
		&& gnetVerify(numberOfRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(clanId != RL_INVALID_CLAN_ID)
		&& gnetVerify(pivotGamer.IsValid())
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Clan Id =\"%" I64FMT "d\"", clanId);
		gnetDebug2("...... Radius =\"%u\"", radius);
		gnetDebug2("...... Number of Rows Requested =\"%u\"", numberOfRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(numberOfRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;

			ret = rlLeaderboard2ClanMemberStats::ReadByRadius(localGamerIndex
															,m_Info.m_Id
															,groupSelector
															,clanId
															,pivotGamer
															,numberOfRows
															,m_Info.m_ColumnIds
															,m_Info.m_NumColumnIds
															,m_RowPtrs
															,&m_NumRowsReturned
															,&m_NumRowsInLeaderboard
															,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadGroupByRadius ----------------------------------
bool 
netLeaderboardReadGroupsByRadius::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& pivotGroup, unsigned int radius)
{
	bool ret = false;

	//Number of rows is based on the radius.
	unsigned numberOfRows = radius * 2 + 1;

	//Make sure we stay within our bounds
	if (numberOfRows > MAX_LEADERBOARD_ROWS)
	{
		numberOfRows = MAX_LEADERBOARD_ROWS;
		radius = (MAX_LEADERBOARD_ROWS / 2) - 2;  //new maximum radius
	}

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP)
		&& gnetVerify(numberOfRows >= 1)
		&& gnetVerify(numberOfRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Radius =\"%u\"", radius);
		gnetDebug2("...... Number of Rows Requested =\"%u\"", numberOfRows);
		gnetDebug2("...... Number of Groups =\"%u\"", pivotGroup.m_NumGroups);
		for (unsigned i=0; i<pivotGroup.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, pivotGroup.m_Group[i].m_Category, pivotGroup.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(numberOfRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			m_LastReadTime = sysTimer::GetSystemMsTime() | 0x01;

			ret = rlLeaderboard2GroupStats::ReadByRadius(localGamerIndex
														,m_Info.m_Id
														,pivotGroup
														,numberOfRows
														,m_Info.m_ColumnIds
														,m_Info.m_NumColumnIds
														,m_RowPtrs
														,&m_NumRowsReturned
														,&m_NumRowsInLeaderboard
														,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}


// ---- Class netLeaderboardReadByScoreInt ----------------------------------

bool
netLeaderboardReadGamersByScoreInt::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const s64 pivotScore)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER || m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2PlayerStats::ReadByScoreInt(localGamerIndex
				,m_Info.m_Id
				,groupSelector
				,pivotScore
				,maxNumRows
				,m_Info.m_ColumnIds
				,m_Info.m_NumColumnIds
				,m_RowPtrs
				,&m_NumRowsReturned
				,&m_NumRowsInLeaderboard
				,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClansByScoreInt ----------------------------------

bool
netLeaderboardReadClansByScoreInt::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const s64 pivotscore)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2ClanStats::ReadByScoreInt(localGamerIndex
				,m_Info.m_Id
				,groupSelector
				,pivotscore
				,maxNumRows
				,m_Info.m_ColumnIds
				,m_Info.m_NumColumnIds
				,m_RowPtrs
				,&m_NumRowsReturned
				,&m_NumRowsInLeaderboard
				,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClanMembersByScoreInt ----------------------------------
bool
netLeaderboardReadClanMembersByScoreInt::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned maxNumRows, const s64 pivotscore)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		&& gnetVerify(clanId != RL_INVALID_CLAN_ID)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... clanId =\"%" I64FMT "d\"", clanId);
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2ClanMemberStats::ReadByScoreInt(localGamerIndex
				,m_Info.m_Id
				,groupSelector
				,clanId
				,pivotscore
				,maxNumRows
				,m_Info.m_ColumnIds
				,m_Info.m_NumColumnIds
				,m_RowPtrs
				,&m_NumRowsReturned
				,&m_NumRowsInLeaderboard
				,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}


// ---- Class netLeaderboardReadByScoreFloat ----------------------------------

bool
netLeaderboardReadGamersByScoreFloat::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const double pivotscore)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_PLAYER || m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_GROUP_MEMBER)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2PlayerStats::ReadByScoreFloat(localGamerIndex
				,m_Info.m_Id
				,groupSelector
				,pivotscore
				,maxNumRows
				,m_Info.m_ColumnIds
				,m_Info.m_NumColumnIds
				,m_RowPtrs
				,&m_NumRowsReturned
				,&m_NumRowsInLeaderboard
				,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClansByScoreFloat ----------------------------------

bool
netLeaderboardReadClansByScoreFloat::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const unsigned maxNumRows, const double pivotscore)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2ClanStats::ReadByScoreFloat(localGamerIndex
				,m_Info.m_Id
				,groupSelector
				,pivotscore
				,maxNumRows
				,m_Info.m_ColumnIds
				,m_Info.m_NumColumnIds
				,m_RowPtrs
				,&m_NumRowsReturned
				,&m_NumRowsInLeaderboard
				,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

// ---- Class netLeaderboardReadClanMembersByScoreFloat ----------------------------------
bool
netLeaderboardReadClanMembersByScoreFloat::Start(const int localGamerIndex, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId clanId, const unsigned maxNumRows, const double pivotscore)
{
	bool ret = false;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(m_Info.m_Id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", m_Info.m_Id))
		return ret;

	if (gnetVerify(m_Info.m_ReadType == RL_LEADERBOARD2_TYPE_CLAN_MEMBER)
		&& gnetVerify(clanId != RL_INVALID_CLAN_ID)
		&& gnetVerify(0 < maxNumRows)
		&& gnetVerify(maxNumRows <= MAX_LEADERBOARD_ROWS)
		&& gnetVerify(!m_RowHeap)
		&& gnetVerify(!m_RowPtrs)
		&& gnetVerify(!m_StatValueHeap)
		&& gnetVerify(this->CreateConcreteLeaderboard()))
	{
		gnetDebug2("Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));
		gnetDebug2("...... clanId =\"%" I64FMT "d\"", clanId);
		gnetDebug2("...... Max number Rows =\"%u\"", maxNumRows);
		gnetDebug2("...... Number of Groups =\"%u\"", groupSelector.m_NumGroups);
		for (unsigned i=0; i<groupSelector.m_NumGroups; i++)
		{
			gnetDebug2("...... Group[%d]: m_Category=\"%s\" m_Id=\"%s\".", i, groupSelector.m_Group[i].m_Category, groupSelector.m_Group[i].m_Id);
		}

		if(gnetVerifyf(AllocateRowData(maxNumRows, lbConf->m_columns.GetCount()), "Unable to allocate memory for leaderboard Read"))
		{
			m_MyStatus.Reset();

			ret = rlLeaderboard2ClanMemberStats::ReadByScoreFloat(localGamerIndex
				,m_Info.m_Id
				,groupSelector
				,clanId
				,pivotscore
				,maxNumRows
				,m_Info.m_ColumnIds
				,m_Info.m_NumColumnIds
				,m_RowPtrs
				,&m_NumRowsReturned
				,&m_NumRowsInLeaderboard
				,&m_MyStatus);
		}
	}

	if (!ret)
	{
		m_MyStatus.SetPending();
		m_MyStatus.SetFailed();
	}

	gnetAssertf(ret, "Failed to Start Read leaderboard id=\"%s:%d\", type=\"%s\"", lbConf->GetName(), m_Info.m_Id, GetReadTypeString(m_Info.m_ReadType));

	return ret;
}

} // namespace rage

//eof

