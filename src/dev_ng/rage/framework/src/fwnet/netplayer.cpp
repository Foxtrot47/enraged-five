//
// netplayer.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/netplayer.h"

#include "rline/rlgamerinfo.h"
#include "system/param.h"

#include "fwnet/netarraymgr.h"
#include "fwnet/netbot.h"
#include "fwnet/netinterface.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/optimisations.h"
#include "fwsys/timer.h"
#include "fwnet/netchannel.h"

NETWORK_OPTIMISATIONS()

PARAM(netPlayerDisableLeakPrevention, "[net] Disables non-physical player leak prevention");

namespace rage
{

// Kick votes we have cast for other players
PlayerFlags netPlayer::sm_LocalPlayerKickVotes = 0;

netPlayer::netPlayer() :
m_PlayerType(NETWORK_PLAYER)
, m_CxnId(INVALID_CONNECTION_ID)
, m_NatType(NET_NAT_UNKNOWN)
, m_ActivePlayerIndex(INVALID_PLAYER_INDEX)
, m_PhysicalPlayerIndex(INVALID_PLAYER_INDEX)
, m_PeerPlayerIndex(0)
, m_TimeAdded(0)
, m_Team(INVALID_TEAM)
, m_VoiceFlagsDirty(true)
, m_VoiceChannel(VOICE_CHANNEL_NONE)
, m_NonPhysicalPlayerData(0)
, m_sizeOfNetArrayData(0)
, m_Leaving(false)
, m_Local(false)
, m_Active(false)
, m_KickVotes(0)
, m_NumKickVotes(0)
#if ENABLE_NETWORK_BOTS
, m_Bot(0)
#endif // ENABLE_NETWORK_BOTS
{
}

netPlayer::~netPlayer()
{
    ShutdownPrivate();
}

// init for local player:
void netPlayer::Init(const netNatType natType)
{
    //Assert(IsMyPlayer());

    //NOTE: Don't store our local address to our local player.
    //Our address will be different for each player from the point
    //of view of that peer.  IOW, our address from our point of
    //view is different from what remote players see as our address.

	m_EndpointId				= NET_INVALID_ENDPOINT_ID;
    m_CxnId						= -1;
    m_PeerPlayerIndex			= 0;
    m_NatType					= natType;
	m_PhysicalPlayerIndex		= INVALID_PLAYER_INDEX;
	m_Team						= INVALID_TEAM;
	m_TimeAdded					= fwTimer::GetSystemTimeInMilliseconds();
    m_KickVotes					= 0;
    m_NumKickVotes				= 0;
	m_sizeOfNetArrayData		= 0;
    m_VoiceFlagsDirty			= true;
	m_VoiceChannel				= VOICE_CHANNEL_NONE;
	m_Leaving					= false;
    m_Local						= false;
    m_Active					= false;
#if ENABLE_NETWORK_BOTS
    m_Bot                 = 0;
#endif // ENABLE_NETWORK_BOTS

	sm_LocalPlayerKickVotes = 0;
}

// init for remote player:
void netPlayer::Init(const rlGamerInfo& UNUSED_PARAM(gamerInfo),
					 const int         connectionId,
					 const EndpointId endpointId,
                     const netNatType  natType)
{
	m_EndpointId				= endpointId;
    m_CxnId						= connectionId;
	m_PhysicalPlayerIndex		= INVALID_PLAYER_INDEX;
    m_PeerPlayerIndex			= 0;
    m_NatType					= natType;
	m_TimeAdded					= fwTimer::GetSystemTimeInMilliseconds();
    m_KickVotes					= 0;
    m_NumKickVotes				= 0;
	m_sizeOfNetArrayData		= 0;
	m_VoiceChannel				= VOICE_CHANNEL_NONE;
	m_Leaving					= false;
    m_Local						= false;
    m_Active					= false;
#if ENABLE_NETWORK_BOTS
    m_Bot                 = 0;
#endif // ENABLE_NETWORK_BOTS
}

void netPlayer::Shutdown()
{
    ShutdownPrivate();
}

bool netPlayer::IsValid() const
{
    bool isBot = false;

#if ENABLE_NETWORK_BOTS
    isBot = IsBot();
#endif // ENABLE_NETWORK_BOTS

    return (netInterface::GetPlayerMgr().IsInitialized()
            && (IsMyPlayer() || (NET_IS_VALID_ENDPOINT_ID(m_EndpointId)) || isBot));
}

bool netPlayer::IsPending() const
{
    return this->IsValid() && !this->IsActive();
}

bool netPlayer::IsActive() const
{
    bool valid = false;

    if(IsValid())
    {
#if ENABLE_NETWORK_BOTS
        if(IsBot())
        {
            valid = IsBotActive();
        }
        else
#endif // ENABLE_NETWORK_BOTS
        {
            valid = m_Active;
        }
    }

    return valid;
}

bool netPlayer::IsPhysical() const
{
    return this->IsActive();
}

bool netPlayer::IsSignedIn() const
{
    return (this->IsValid() && this->GetGamerInfo().IsSignedIn());
}

bool netPlayer::IsOnline() const
{
    return (this->IsValid() && this->GetGamerInfo().IsOnline());
}

bool netPlayer::IsLocal() const
{
    return m_Local;
}

bool netPlayer::IsBot() const
{
#if ENABLE_NETWORK_BOTS
    return m_PlayerType == NETWORK_BOT;
#else
    return false;
#endif // ENABLE_NETWORK_BOTS
}

bool netPlayer::IsRemote() const
{
    return this->GetGamerInfo().IsRemote();
}

void netPlayer::SetLeaving()
{
	Assert(!m_Leaving);
	m_Leaving = true;
}

netNatType
netPlayer::GetNatType() const
{
    return m_NatType;
}

const rlGamerId& netPlayer::GetRlGamerId() const
{
    return this->GetGamerInfo().GetGamerId();
}

u64 netPlayer::GetRlPeerId() const
{
    return this->GetPeerInfo().GetPeerId();
}

ActivePlayerIndex netPlayer::GetActivePlayerIndex() const
{
    return m_ActivePlayerIndex;
}

void netPlayer::SetActivePlayerIndex(ActivePlayerIndex playerIndex)
{
	m_ActivePlayerIndex = playerIndex;
}

void netPlayer::SetPhysicalPlayerIndex(PhysicalPlayerIndex playerIndex)
{
    m_PhysicalPlayerIndex = playerIndex;
}

void netPlayer::SetIsLocal(bool local)
{
    m_Local = local;
}

EndpointId netPlayer::GetEndpointId() const
{
    return m_EndpointId;
}

int netPlayer::GetConnectionId() const
{
    return m_CxnId;
}

void netPlayer::SetConnectionId(int connectionID)
{
    m_CxnId = connectionID;
}

unsigned netPlayer::GetPeerPlayerIndex() const
{
    return m_PeerPlayerIndex;
}

void netPlayer::SetPeerPlayerIndex(const unsigned peerPlayerIndex)
{
    m_PeerPlayerIndex = peerPlayerIndex;
}

void netPlayer::SetNonPhysicalData(nonPhysicalPlayerDataBase *nonPhysicalData)
{
    gnetAssert(IsValid());

	// make sure we don't leak
#if !__FINAL
	if(!PARAM_netPlayerDisableLeakPrevention.Get())
#endif
	{
		if(m_NonPhysicalPlayerData)
		{
			gnetDebug1("SetNonPhysicalData :: Preventing Leak!"); 
			delete m_NonPhysicalPlayerData;
			m_NonPhysicalPlayerData = 0;
		}
	}
	
    m_NonPhysicalPlayerData = nonPhysicalData;
}

nonPhysicalPlayerDataBase *netPlayer::GetNonPhysicalData() const
{
    gnetAssert(IsValid());
    return m_NonPhysicalPlayerData;
}

bool netPlayer::HasTeam()
{
	return m_Team != INVALID_TEAM;
}

int netPlayer::GetTeam() const
{
	return m_Team;
}

void netPlayer::SetTeam(int team)
{
	m_Team = team;
}

#if ENABLE_NETWORK_BOTS

netBot *netPlayer::GetBot() const
{
    gnetAssertf(IsBot(), "GetBot() should only be called on network bot players!");
    return m_Bot;
}

void netPlayer::SetBot(netBot *bot)
{
    gnetAssertf(IsBot(), "SetBot() should only be called on network bot players!");
    m_Bot = bot;
}

#endif // ENABLE_NETWORK_BOTS

void netPlayer::UpdateIsActiveState()
{
    m_Active = IsMyPlayer() || netInterface::GetPlayerMgr().IsConnectionOpen(*this);
}

netPlayer*
netPlayer::NextPendingPlayer(netPlayer *currentPlayer)
{
	return AssertVerify(currentPlayer->IsPending()) ? currentPlayer->m_ListLink.m_next : NULL;
}

const netPlayer*
netPlayer::NextPendingPlayer(const netPlayer *currentPlayer)
{
	return NextPendingPlayer(const_cast<netPlayer*>(currentPlayer));
}

netPlayer*
netPlayer::NextExistingPlayer(netPlayer *currentPlayer)
{
    return AssertVerify(currentPlayer->IsValid()) ? currentPlayer->m_ListLink.m_next : NULL;
}

const netPlayer*
netPlayer::NextExistingPlayer(const netPlayer *currentPlayer)
{
    return NextExistingPlayer(const_cast<netPlayer*>(currentPlayer));
}

netPlayer*
netPlayer::NextActivePlayer(netPlayer *currentPlayer)
{
    return AssertVerify(currentPlayer->IsActive()) ? currentPlayer->m_ActiveListLink.m_next : NULL;
}

const netPlayer*
netPlayer::NextActivePlayer(const netPlayer *currentPlayer)
{
    return NextActivePlayer(const_cast<netPlayer*>(currentPlayer));
}

netPlayer*
netPlayer::NextPhysicalPlayer(netPlayer *currentPlayer)
{
	return AssertVerify(currentPlayer->IsPhysical()) ? currentPlayer->m_PhysListLink.m_next : NULL;
}

const netPlayer*
netPlayer::NextPhysicalPlayer(const netPlayer *currentPlayer)
{
	return NextPhysicalPlayer(const_cast<netPlayer*>(currentPlayer));
}

bool netPlayer::IsMyPlayer() const
{
    return this == netInterface::GetLocalPlayer();
}

const rlPeerInfo& netPlayer::GetPeerInfo() const
{
    return this->GetGamerInfo().GetPeerInfo();
}

void 
netPlayer::SetVoiceChannel(int nVoiceChannel) 
{ 
	if(!gnetVerifyf((nVoiceChannel >= 0) || (nVoiceChannel == VOICE_CHANNEL_NONE), "SetVoiceChannel :: Channel %d out of range. Must be positive!", nVoiceChannel))
		return;

	m_VoiceChannel = nVoiceChannel; 
}

void 
netPlayer::ClearVoiceChannel() 
{ 
	SetVoiceChannel(VOICE_CHANNEL_NONE); 
}

int 
netPlayer::GetVoiceChannel() const 
{ 
	return m_VoiceChannel; 
}

void
netPlayer::AddKickVote(const netPlayer& player)
{
    if (!(m_KickVotes & (1<<player.GetPhysicalPlayerIndex())))
    {
        m_KickVotes |= (1<<player.GetPhysicalPlayerIndex());
        m_NumKickVotes++;
    }

	//Set local player kick votes.
	if (IsRemote() && !IsBot() && player.IsLocal() && !player.IsBot())
	{
		sm_LocalPlayerKickVotes |= BIT(GetPhysicalPlayerIndex());
	}
}

void
netPlayer::RemoveKickVote(const netPlayer& player)
{
    if (m_KickVotes & (1<<player.GetPhysicalPlayerIndex()))
    {
        m_KickVotes &= ~(1<<player.GetPhysicalPlayerIndex());

        if (AssertVerify(m_NumKickVotes > 0))
        {
            m_NumKickVotes--;
        }
    }

	//Set local player kick votes.
	if (IsRemote() && !IsBot() && player.IsLocal() && !player.IsBot())
	{
		sm_LocalPlayerKickVotes &= ~BIT(GetPhysicalPlayerIndex());
	}
}

void
netPlayer::RemoveAllKickVotes()
{
    m_KickVotes = 0;
    m_NumKickVotes = 0;
}

unsigned
netPlayer::GetNumKickVotes() const
{
    return m_NumKickVotes;
}

void
netPlayer::SetKickVotes(PlayerFlags kickVotes)
{
	m_KickVotes = kickVotes;
	m_NumKickVotes = 0;

	for(int i=0; kickVotes && i<MAX_NUM_ACTIVE_PLAYERS; ++i)
	{
		PlayerFlags currFlag = (1<<i);

		if(kickVotes & currFlag)
		{
			kickVotes &= ~currFlag;
			++m_NumKickVotes;
		}
	}
}

void
netPlayer::AddLocalKickVote(const netPlayer& player)
{
	if (gnetVerify(player.IsRemote()) && gnetVerify(!player.IsBot()))
	{
		sm_LocalPlayerKickVotes |= BIT(player.GetPhysicalPlayerIndex()); 
	}
}

void  
netPlayer::RemoveLocalKickVote(const netPlayer& player)
{
	if (gnetVerify(player.IsRemote()) && gnetVerify(!player.IsBot()))
	{
		sm_LocalPlayerKickVotes &= ~BIT(player.GetPhysicalPlayerIndex());
	}
}

unsigned netPlayer::GetTimeInSession() const
{
	Assert(m_TimeAdded != 0);

	u32 time = fwTimer::GetSystemTimeInMilliseconds() - m_TimeAdded;

	return time;
}

void netPlayer::ShutdownPrivate()
{
    m_PlayerType      = NETWORK_PLAYER;
    m_EndpointId      = NET_INVALID_ENDPOINT_ID;
	m_CxnId           = INVALID_CONNECTION_ID;
    m_PeerPlayerIndex = 0;
    m_NatType         = NET_NAT_UNKNOWN;
	m_Leaving         = false;
    m_Local           = false;
    m_Active          = false;

    m_VoiceChannel = VOICE_CHANNEL_NONE;

    if(m_NonPhysicalPlayerData)
    {
        delete m_NonPhysicalPlayerData;
        m_NonPhysicalPlayerData = 0;
    }

#if ENABLE_NETWORK_BOTS
    m_Bot = 0;
#endif // ENABLE_NETWORK_BOTS
}

#if ENABLE_NETWORK_BOTS

bool netPlayer::IsBotActive() const
{
    gnetAssertf(IsBot(), "IsBotActive can only be called on network bot players!");

    bool active = false;

    if(m_Bot)
    {
        active = m_Bot->IsActive();
    }

    return active;
}

#endif // ENABLE_NETWORK_BOTS

} // namespace rage
