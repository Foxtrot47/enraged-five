//
// netlogstringtable.h
//
// Copyright (C) Rockstar Games.  All Rights Reserved.
//

#ifndef INC_NETWORKLOGSTRINGTABLE_H_
#define INC_NETWORKLOGSTRINGTABLE_H_

namespace rage
{

class sysTinyHeap;

//PURPOSE
// The netLogStringTable class implements a simple string table for storing logging strings
// that can then be associated with an ID. This is useful for network logs which contain a lot
// of duplicate strings as a compression technique (as a string can be referenced by ID rather than storing
// the entire string itself
class netLogStringTable
{
public:

    static const unsigned MAX_STRINGS = 1024; // Maximum number of strings that can be stored in the table

    //PURPOSE
    // Class constructor
    netLogStringTable();

    //PURPOSE
    // Class destructor
    ~netLogStringTable();

    //PURPOSE
    // Initialises the string table
    //PARAMS
    // stringTableHeapSize - Size in bytes of the memory to allocate for use by the string table
    void Init(const unsigned stringTableHeapSize);

    //PURPOSE
    // Shuts downs the string table
    void Shutdown();

    //PURPOSE
    // Returns whether the string table has been initialised
    bool IsInitialised();

    //PURPOSE
    // Adds a string to the string table
    //PARAMS
    // string - The string to add
    void AddString(const char *string);

    //PURPOSE
    // Returns the string with the specified string table ID
    //PARAMS
    // stringID - The ID of the string to return 
    const char *GetString(unsigned stringID);

    //PURPOSE
    // Finds the string ID of a specified string if it is in the string table. Returns false
    // if the string is not found
    //PARAMS
    // string   - The string to find
    // stringID - The ID of the string in the string table if it was found
    bool FindString(const char *string, unsigned &stringID);

private:

    //PURPOSE
    // The stringInfo struct contains information about a string stored in the string table
    struct stringInfo
    {
        const char *m_String;   // String - required to find correct string ID when multiple string share the same m_StringInfo index
        unsigned    m_StringID; // String ID, can be used to lookup string in m_StringTable
        stringInfo *m_NextInfo; // pointer to info for the next string sharing the hash value
    };

    u8          *m_StringTableHeapBuffer;    // Buffer memory for string table heap
    sysTinyHeap *m_StringTableHeap;          // Heap for memory allocation by the string table
    unsigned     m_StringCount;              // Current number of strings in the table
    stringInfo  *m_StringInfo[MAX_STRINGS];  // Hash table for strings - fast lookup for a string (string hash % max strings is array index)
    const char  *m_StringTable[MAX_STRINGS]; // Strings stored in the class  - their unique ID is their index into this array
};

} // namespace rage

#endif // !INC_NETWORKLOGSTRINGTABLE_H_
