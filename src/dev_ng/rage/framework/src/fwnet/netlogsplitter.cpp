//
// netlogsplitter.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/netlogsplitter.h"

#include <stdarg.h>
#include <stdio.h>

#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

static const unsigned int MAX_LOG_STRING = 1024;

void netLogSplitter::Log(const char *logText, ...)
{
    char buffer[MAX_LOG_STRING];

    va_list args;
    va_start(args,logText);
    vsprintf(buffer,logText,args);
    va_end(args);

    m_Log1.Log(buffer);
    m_Log2.Log(buffer);
}

void netLogSplitter::WriteDataValue(const char *dataName, const char *dataValue, ...)
{
    char buffer[MAX_LOG_STRING];

    va_list args;
    va_start(args,dataValue);
    vsprintf(buffer,dataValue,args);
    va_end(args);

    m_Log1.WriteDataValue(dataName, buffer);
    m_Log2.WriteDataValue(dataName, buffer);
}

void netLogSplitter::WriteMessageHeader(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName)
{
    m_Log1.WriteMessageHeader(received, sequence, messageString1, messageString2, playerName);
    m_Log2.WriteMessageHeader(received, sequence, messageString1, messageString2, playerName);
}

void netLogSplitter::WritePlayerText(const char *dataName, const char *dataValue, const char *playerName)
{
    m_Log1.WritePlayerText(dataName, dataValue, playerName);
    m_Log2.WritePlayerText(dataName, dataValue, playerName);
}

void netLogSplitter::WriteEventData(const char *eventName, const char *eventData, unsigned trailingLineBreaks)
{
    m_Log1.WriteEventData(eventName, eventData, trailingLineBreaks);
    m_Log2.WriteEventData(eventName, eventData, trailingLineBreaks);
}

void netLogSplitter::WriteNodeHeader(const char *nodeName)
{
    m_Log1.WriteNodeHeader(nodeName);
    m_Log2.WriteNodeHeader(nodeName);
}

} // namespace rage
