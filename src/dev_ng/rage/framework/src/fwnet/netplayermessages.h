//
// netplayermessages.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORKPLAYERMESSAGES_H
#define NETWORKPLAYERMESSAGES_H

// Rage headers
#include "net/message.h"
#include "net/net.h"
#include "rline/rlgamerinfo.h"

// framework headers
#include "fwnet/netinterface.h"
#include "fwnet/netplayer.h"

namespace rage
{

//PURPOSE
// A message containing data about a network player when joining a session
class playerDataMsg
{
public:

    NET_MESSAGE_DECL(playerDataMsg, PLAYER_DATA_MSG);

    playerDataMsg();
    virtual ~playerDataMsg() {}

    //PURPOSE
    // Class constructor
    //PARAMS
    // gameVersion - The version of the game the player is using
    // natType     - The NAT type the player is using
    playerDataMsg(const unsigned gameVersion,
                  const netNatType natType);

    //PURPOSE
    // Resets the contents of this message
    //PARAMS
    // gameVersion - The version of the game the player is using
    // natType     - The NAT type the player is using
    void Reset(const unsigned gameVersion,
				const netNatType natType);

    //PURPOSE
    // Serialisation function for the message data in the specified message. The manages both
    // the reading and writing of the message data depending on the serialisation mode.
    //PARAMS
    // bb  - import/export buffer (depends on the serialisation mode)
    // msg - message to read/write the data from/to
    NET_MESSAGE_SER(bb, msg)
    {
        return bb.SerUns(msg.m_GameVersion, SIZEOF_GAMEVERSION)
                && bb.SerUns(msg.m_NatType, SIZEOF_NAT_TYPE)
                && msg.SerialiseAdditionalPlayerData(bb);
    }

    //PURPOSE
    // Returns the size of the message data in bits
    virtual int GetMessageDataBitSize() const;

    //PURPOSE
    // Writes the contents of this message to a log file
    //PARAMS
    // wasReceived  - Indicates whether this message is being sent or has been received
    // sequenceNum  - The sequence number used for sending/receiving the message
    // player       - The player sending/receiving the message
    virtual void WriteToLogFile( bool bReceived, netSequence seqNum, const netPlayer &player ) const;

    static const unsigned SIZEOF_GAMEVERSION = 32;
    static const unsigned SIZEOF_NAT_TYPE    = datBitsNeeded<NET_NAT_NUM_TYPES>::COUNT;

    virtual bool SerialiseAdditionalPlayerData(datImportBuffer &UNUSED_PARAM(bb)) { return true; }
    virtual bool SerialiseAdditionalPlayerData(datExportBuffer &UNUSED_PARAM(bb)) const { return true; }

    // The version of the game the player is using
    unsigned m_GameVersion;

    // The NAT type the player is using
    netNatType m_NatType;
};

class nonPhysicalDataMsg
{
public:

    NET_MESSAGE_DECL(nonPhysicalDataMsg, NON_PHYSICAL_DATA_MSG);

    //PURPOSE
    // Class constructor
    nonPhysicalDataMsg(const rlPeerInfo &peerInfo) :
    m_RemotePeerInfo(&peerInfo),
    m_PeerPlayerIndex(0),
    m_NonPhysicalData(0) 
    {
    }

    //PURPOSE
    // Class constructor
    //PARAMS
    // nonPhysicalData - The non-physical data structure containing the non-physical data
    nonPhysicalDataMsg(unsigned peerPlayerIndex, nonPhysicalPlayerDataBase &nonPhysicalData) :
    m_RemotePeerInfo(0),
    m_PeerPlayerIndex(peerPlayerIndex),
    m_NonPhysicalData(&nonPhysicalData) 
    {
    }

    //PURPOSE
    // Reads the contents of the specified import buffer to a message
    //PARAMS
    // bb  - export buffer
    // msg - message
    NET_MESSAGE_IMPORT(bb, msg)
    {
        bool success = true;

        if(SIZEOF_LOCAL_PLAYER_ID > 0)
        {
            success &= bb.SerUns(msg.m_PeerPlayerIndex, SIZEOF_LOCAL_PLAYER_ID);
        }
        else
        {
            msg.m_PeerPlayerIndex = 0;
        }

        unsigned dataSize = 0;
        success &= bb.SerUns(dataSize, SIZEOF_NON_PHYSICAL_DATA);

        netPlayer *player = msg.m_RemotePeerInfo ? netInterface::GetPlayerFromPeerPlayerIndex(msg.m_PeerPlayerIndex, *msg.m_RemotePeerInfo) : 0;

        msg.m_NonPhysicalData = player ? player->GetNonPhysicalData() : 0;

        if(msg.m_NonPhysicalData)
        {
            msg.m_NonPhysicalData->Read(bb);
        }
        else
        {
            bb.SetNumBitsRead(bb.GetNumBitsRead() + dataSize);
        }

        return success;
    }

    //PURPOSE
    // Writes the contents of the specified message to an export buffer
    //PARAMS
    // bb  - export buffer
    // msg - message
    NET_MESSAGE_EXPORT(bb, msg)
    {
        bool success = true;

        if(SIZEOF_LOCAL_PLAYER_ID > 0)
        {
            success &= bb.SerUns(msg.m_PeerPlayerIndex, SIZEOF_LOCAL_PLAYER_ID);
        }

        if(msg.m_NonPhysicalData == 0)
        {
            const unsigned size = 0;
            success &= bb.SerUns(size, SIZEOF_NON_PHYSICAL_DATA);
        }
        else
        {
            const unsigned size = msg.m_NonPhysicalData->GetSize();
            success &= bb.SerUns(size, SIZEOF_NON_PHYSICAL_DATA);
            msg.m_NonPhysicalData->Write(bb);
        }

        return success;
    }

	int GetMessageDataBitSize() const
	{
		int size = SIZEOF_LOCAL_PLAYER_ID + SIZEOF_NON_PHYSICAL_DATA;

		if (m_NonPhysicalData != 0)
		{
			size += m_NonPhysicalData->GetSize();
		}
	
		return size;
	}

	//PURPOSE
    // Writes the contents of this message to a log file
    //PARAMS
    // wasReceived  - Indicates whether this message is being sent or has been received
    // sequenceNum  - The sequence number used for sending/receiving the message
    // player       - The player sending/receiving the message
    void WriteToLogFile( bool bReceived, netSequence seqNum, const netPlayer &player ) const;

private:

    // The peer info of the remote player the message has been received from (only valid when receiving this message)
    const rlPeerInfo *m_RemotePeerInfo;
    // The index of the player on the local machine
    unsigned m_PeerPlayerIndex;
    // The non-physical data structure containing the non-physical data
    nonPhysicalPlayerDataBase *m_NonPhysicalData;
};

//PURPOSE
// This message is sent by a network bot when it is activated on
// the machine it was created by. This ensures network bots are
// not processed by remote machines until they have finished joining
// the session on the machine that created them
class activateNetworkBotMsg
{
public:

    NET_MESSAGE_DECL(activateNetworkBotMsg, ACTIVATE_NETWORK_BOT_MSG);

    activateNetworkBotMsg() {}

    NET_MESSAGE_SER(UNUSED_PARAM(bb), UNUSED_PARAM(msg))
	{
        return true;
	}
};

} // namespace rage

#endif  // NETWORKMESSAGES_H
