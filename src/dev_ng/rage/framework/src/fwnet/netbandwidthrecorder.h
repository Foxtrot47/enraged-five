// 
// netBandwidthRecorderWrapper.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETWORK_BANDWIDTH_RECORDER_H
#define NETWORK_BANDWIDTH_RECORDER_H

// rage includes
#include "net/bandwidth.h"

#if __BANK

namespace rage
{

//PURPOSE
// A wrapper for a rage netBandwidthRecorderWrapper, which also contains a name used for debugging / logging.
class netBandwidthRecorderWrapper 
{
	friend class netBandwidthStatistics;

public:

	netBandwidthRecorderWrapper();

protected:

	void SetName(const char* name) { strncpy(m_Name, name, MAX_NAME_LENGTH); }

	void Update();

	//PURPOSE
	// Adds an incoming bandwidth sample
	void AddBandwidthIn(unsigned bandwidthIn);

	//PURPOSE
	// Adds an outgoing bandwidth sample
    void AddBandwidthOut(unsigned bandwidthOut);

	//PURPOSE
	// Sets the sample interval for the bandwidth recorder
	void SetSampleInterval(unsigned interval);

	//PURPOSE
	// Samples the bandwidth data
	void SampleData(unsigned currentTime);

public:

	//PURPOSE
	// Gets the incoming bandwidth rate (bits/sec)
	float GetBandwidthInRate() const;

	//PURPOSE
	// Gets the outgoing bandwidth rate (bits/sec)
	float GetBandwidthOutRate() const;

	const char *GetName() const;

private:

	netBandwidthRecorderWrapper(const netBandwidthRecorderWrapper &);
	netBandwidthRecorderWrapper &operator=(const netBandwidthRecorderWrapper &);

    static const unsigned int MAX_NAME_LENGTH = 128;

    char m_Name[MAX_NAME_LENGTH];		// the name of this recorder
    netBandwidthRecorder m_Bandwidth;	// the rage bandwidth recorder
};

} // namespace rage

#endif  // __BANK

#endif  // NETWORK_BANDWIDTH_RECORDER_H
