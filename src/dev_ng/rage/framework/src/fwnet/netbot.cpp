// 
// netbot.cpp
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//
#include "netbot.h"
 
#include "rline/rlpresence.h"
#include "net/netHardware.h"
#include "string/string.h"
#include "string/stringhash.h"
#include "system/timer.h"

#include "fwnet/netchannel.h"
#include "fwnet/netplayermessages.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

#if ENABLE_NETWORK_BOTS

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, bots, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_bots

netBot::netBot(unsigned botIndex, const rlGamerInfo *gamerInfo)
{
    bool success = rlPresence::GetGamerInfo(0, &m_GamerInfo);
    AssertMsg(success, "Failed retrieving local gamer info for network bot!");

    // initialise the rlGamerInfo based on the bot index
    if(success)
    {
		if(gamerInfo == 0)
		{
			static const unsigned int MAX_NETWORK_BOT_NAME = 64;
			char botName[MAX_NETWORK_BOT_NAME];
			formatf(botName, sizeof(botName), "Network Bot %d", botIndex+1);

			rlGamerId gamerID;

			u8 mac[6];
			if(gnetVerify(netHardware::GetMacAddress(mac)))
			{
				const unsigned macA = (mac[3] << 24)
									| (mac[1] << 16)
									| (mac[4] << 8)
									| (mac[0]);

				const unsigned macB = (mac[5] << 24)
									| (mac[2] << 16)
									| (mac[0] << 8)
									| (mac[4]);

				const unsigned hashA = atDataHash(botName, ::strlen(botName), macA);
				const unsigned hashB = atDataHash(botName, ::strlen(botName), macB);

				gamerID.SetId((u64(hashA) << 32) | hashB);
			}

            rlGamerHandle gamerHandle = m_GamerInfo.GetGamerHandle();
            gamerHandle.SetBotIndex(static_cast<u8>(botIndex+1));
			m_GamerInfo.SetGamerInfoData(m_GamerInfo.GetPeerInfo(), gamerID, gamerHandle, 0, botName);
		}
		else
		{
			m_GamerInfo = *gamerInfo;
		}

        m_Active          = false;
        m_State           = BOT_STATE_IDLE;
        m_NetPlayer       = 0;
        m_ActivationTimer = 0;
    }
}

netBot::~netBot()
{
    gnetAssert(!IsInMatch());
}

bool netBot::IsActive() const
{
    return m_Active;
}

bool netBot::IsInMatch() const
{
    return m_State >= BOT_STATE_IN_MATCH;
}

void netBot::SetNetPlayer(netPlayer *player)
{
    m_NetPlayer = player;

    if(m_NetPlayer)
    {
        m_NetPlayer->SetBot(this);
    }
}

 void netBot::SetState(unsigned newState)
 {
#if !__NO_OUTPUT
     if(gnetVerifyf(m_GamerInfo.GetName(), "Invalid Network bot name") && newState != m_State)
     {
        gnetDebug2("%s changing state from %s to %s", m_GamerInfo.GetName(), GetStateName(m_State), GetStateName(newState));
     }
#endif // !__NO_OUTPUT
     m_State = newState; 
 }

 const char *netBot::GetStateName(unsigned state) const
 {
     switch(state)
     {
     case BOT_STATE_IDLE:
         return "BOT_STATE_IDLE";
     case BOT_STATE_JOINING:
         return "BOT_STATE_JOINING";
     case BOT_STATE_CREATE_PLAYER:
         return "BOT_STATE_CREATE_PLAYER";
     case BOT_STATE_LEAVING:
         return "BOT_STATE_LEAVING";
     case BOT_STATE_IN_MATCH:
         return "BOT_STATE_IN_MATCH";
     case BOT_STATE_USER:
         gnetAssertf(0, "Getting user state for network bot! This should be handled by derived classes!");
         return "BOT_STATE_USER";
     default:
         gnetAssertf(0, "Unexpected bot state!");
         return "BOT_STATE_UNKNOWN";
     }
 }

void netBot::SetActive(bool active)
{
    m_Active = active;

    if(active && gnetVerify(m_NetPlayer) && m_NetPlayer->IsLocal())
    {
        // send an activation message to the other players in the session after a
        // small delay, this gives time for the bot to be added to all the other remote sessions
        const unsigned BOT_ACTIVATION_DELAY = 500;
        m_ActivationTimer = sysTimer::GetSystemMsTime() + BOT_ACTIVATION_DELAY;
    }
}

void netBot::PlayerHasJoined(const netPlayer &player)
{
    if(player.IsRemote() && !player.IsBot() && m_GamerInfo.IsLocal())
    {
        activateNetworkBotMsg activateMsg;
        netInterface::SendReliableMessage(&player, activateMsg, 0, m_NetPlayer);

        BANK_ONLY(netInterface::GetPlayerMgr().IncreaseMiscReliableMessagesSent());
    }
}

void netBot::PlayerHasLeft(const netPlayer &UNUSED_PARAM(player))
{
}

void netBot::Update()
{
    if(m_ActivationTimer > 0 && IsActive())
    {
        if(m_ActivationTimer <= sysTimer::GetSystemMsTime())
        {
            gnetDebug2("Sending activation message");
            activateNetworkBotMsg activateMsg;
            netInterface::BroadcastMsg(activateMsg,
                                       NET_SEND_RELIABLE,
                                       m_NetPlayer);

            m_ActivationTimer = 0;
        }
    }
}

} // namespace rage

#endif // ENABLE_NETWORK_BOTS
