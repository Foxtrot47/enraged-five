// 
// netarraymgr.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NET_ARRAY_MGR_H
#define NET_ARRAY_MGR_H

//// rage headers
#include "atl/array.h"
#include "atl/dlist.h"
#include "data/bitbuffer.h"
#include "fwtl/pool.h"
#include "net/message.h"

//// framework headers
#include "fwnet/netbandwidthstats.h"
#include "fwnet/netarrayhandler.h"
#include "fwnet/netinterface.h"
#include "fwnet/netlog.h"
#include "fwnet/netlogsplitter.h"
#include "fwscript/scriptid.h"

namespace rage
{

class netArrayHandlerBase;
class netBandwidthMgr;
class scriptId;
class scriptHandler;
class netPlayer;
class netPlayerMgr;
class scrThread;
class netScriptBroadcastDataHandlerBase;
class netHostBroadcastDataHandlerBase;
class netPlayerBroadcastDataHandlerBase;

// ================================================================================================================
// netArrayManager
// ================================================================================================================

//PURPOSE
//	Manages a list of network array handlers and handles the sending and receiving of their sync updates.
//
//	NB. An array handler must be registered separately on each machine it is to be synced with.
class netArrayManager
{
public:

	static const unsigned int NUM_ARRAY_UPDATES_INFO_HELD_FOR = 20;
	static const unsigned int MAX_NUM_ARRAY_HANDLER_TYPES = 20;
	static const unsigned int MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG = 10;
	static const unsigned int SIZE_OF_HANDLER_DATA_LEN = datBitsNeeded<netMessage::MAX_BYTE_SIZEOF_PAYLOAD<<3>::COUNT;
	static const unsigned int MAX_SPLIT_UPDATES = sizeof(SplitUpdateFlags)<<3;
	static const unsigned int MAX_CACHED_UPDATES = 50;
	static const unsigned int SIZEOF_UPDATE_NUM = datBitsNeeded<MAX_SPLIT_UPDATES>::COUNT;
	static const unsigned int PLAYER_TIME_IN_SESSION_BEFORE_UPDATE = 2000; // the length of time a player must be in the session before we start broadcasting array handler updates to him
	static const unsigned int MAX_HIGH_FREQUENCY_ARRAYS = 2; // the max permitted number of arrays updating at a high frequency
	static const int INVALID_BATCH = -1;

	enum eUpdateBatchType
	{
		UBT_LOCAL_1,
		UBT_LOCAL_2,
		UBT_LOCAL_3,
		UBT_LOCAL_4,
		UBT_LOCAL_5,
		UBT_HIGH_FREQUENCY,	// reserved for array handlers that need to be updated at a very high frequency
		UBT_REMOTE,			// for remotely controlled array handlers
		NUM_UPDATE_BATCH_TYPES
	};

	static const int NUM_LOCAL_UPDATE_BATCHES = UBT_REMOTE;
	static const int NUM_LOCAL_NORMAL_UPDATE_BATCHES = UBT_HIGH_FREQUENCY; // non high frequency batches

protected:

	//PURPOSE
	//	Message potentially containing a number of array handler updates, each containing updates for a number of
	//	array elements.
	class msgUpdate
	{
	public:

		NET_MESSAGE_DECL(msgUpdate, MSG_UPDATE);

		msgUpdate()
			: m_seqNum(0)
		{
			Reset();
		}

		void Reset()
		{
			m_bb.SetReadWriteBytes(m_buffer, sizeof(m_buffer));
		}

		NET_MESSAGE_EXPORT(bb, msg)
		{
			const unsigned numBits = msg.m_bb.GetBitLength();
			const int cursorPos = msg.m_bb.GetCursorPos();
			msg.m_bb.SetCursorPos(0);
			return
				AssertVerify( bb.WriteUns(msg.m_seqNum, sizeof(netSequence) << 3 ))
				&& AssertVerify( bb.WriteUns(numBits, datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT ))
				&& (!numBits || AssertVerify(bb.WriteBits(msg.m_bb, numBits)))
				&& (msg.m_bb.SetCursorPos(cursorPos), true);
		}

		NET_MESSAGE_IMPORT(bb, msg)
		{
			msg.Reset();

			unsigned numBits = 0;

			return
				AssertVerify(bb.ReadUns(msg.m_seqNum, sizeof(netSequence) << 3))
				&& AssertVerify(bb.ReadUns(numBits, datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT))
				&& (!numBits || AssertVerify(bb.ReadBits(msg.m_bb, numBits)))
				&& (msg.m_bb.SetCursorPos(0), true);
		}

		int GetMessageDataBitSize() const
		{
			return (sizeof(netSequence) << 3) + datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT + m_bb.GetBitLength();
		}

		static const unsigned int MAX_BITSIZEOF_DATA = MAX_MESSAGE_PAYLOAD_BITS;
		static const unsigned int BITSIZEOF_HEADER = (sizeof(netSequence) << 3) + (datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT);
		static const unsigned int BUFFER_SIZE_IN_BYTES = ((MAX_BITSIZEOF_DATA - (BITSIZEOF_HEADER + 7))>>3);

		u8			 m_buffer[BUFFER_SIZE_IN_BYTES];  
		datBitBuffer m_bb;
		netSequence	 m_seqNum;
	};

	//PURPOSE
	//	An ack message for an array handler's update message
	class msgUpdateAck
	{
	public:

		NET_MESSAGE_DECL(msgUpdateAck, MSG_UPDATE_ACK);

		msgUpdateAck() 
		: m_seqNum(0)
		, m_handlersProcessed(0)
		{
		}

		msgUpdateAck(netSequence seqNum, unsigned handlersProcessed) 
		: m_seqNum(seqNum)
		, m_handlersProcessed(handlersProcessed)
		{
		}

		NET_MESSAGE_SER(bb, msg)
		{
			return (bb.SerUns(msg.m_seqNum, sizeof(netSequence) << 3) && bb.SerUns(msg.m_handlersProcessed, sizeof(unsigned) << 3));
		}

		static int GetMessageDataBitSize()
		{
			return ((sizeof(netSequence) << 3) + (sizeof(unsigned) << 3));
		}

		netSequence	m_seqNum;
		unsigned    m_handlersProcessed;
	};

	//PURPOSE
	//	An ack message for an array handler's split update message
	class msgSplitUpdateAck
	{
	public:

		NET_MESSAGE_DECL(msgSplitUpdateAck, MSG_SPLIT_UPDATE_ACK);

		msgSplitUpdateAck()
			: m_messageSeqNum(0),
			m_handlerSeqNum(0),
			m_splitUpdateNum(0)
		{
			Reset();
		}

		msgSplitUpdateAck(netSequence messageSeqNum, netSequence handlerSeqNum, unsigned splitUpdateNum) 
			: m_messageSeqNum(messageSeqNum)
			, m_handlerSeqNum(handlerSeqNum)
			, m_splitUpdateNum((u8)splitUpdateNum)
		{
			Reset();
		}

		void Reset()
		{
			m_bb.SetReadWriteBytes(m_buffer, sizeof(m_buffer));
		}

		NET_MESSAGE_EXPORT(bb, msg)
		{
			const unsigned numBits = msg.m_bb.GetBitLength();
			const int cursorPos = msg.m_bb.GetCursorPos();
			msg.m_bb.SetCursorPos(0);
			return
				AssertVerify( bb.WriteUns(msg.m_messageSeqNum, sizeof(netSequence) << 3 )) 
				&& AssertVerify( bb.WriteUns(msg.m_handlerSeqNum, sizeof(netSequence) << 3))
				&& AssertVerify( bb.WriteUns(msg.m_splitUpdateNum, SIZEOF_UPDATE_NUM))
				&& AssertVerify( bb.WriteUns(numBits, datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT ))
				&& (!numBits || AssertVerify(bb.WriteBits(msg.m_bb, numBits)))
				&& (msg.m_bb.SetCursorPos(cursorPos), true);
		}

		NET_MESSAGE_IMPORT(bb, msg)
		{
			msg.Reset();

			unsigned numBits = 0;

			return
				AssertVerify(bb.ReadUns(msg.m_messageSeqNum, sizeof(netSequence) << 3))
				&& AssertVerify( bb.ReadUns(msg.m_handlerSeqNum, sizeof(netSequence) << 3))
				&& AssertVerify( bb.ReadUns(msg.m_splitUpdateNum, SIZEOF_UPDATE_NUM))
				&& AssertVerify(bb.ReadUns(numBits, datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT))
				&& (!numBits || AssertVerify(bb.ReadBits(msg.m_bb, numBits)))
				&& (msg.m_bb.SetCursorPos(0), true);
		}

		int GetMessageDataBitSize() const
		{
			return ((sizeof(netSequence) << 3))*2 + SIZEOF_UPDATE_NUM + datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT + m_bb.GetBitLength();
		}

		static const unsigned int MAX_BITSIZEOF_DATA = MAX_MESSAGE_PAYLOAD_BITS;
		static const unsigned int BITSIZEOF_HEADER = (sizeof(netSequence) << 3) + (datBitsNeeded<MAX_BITSIZEOF_DATA>::COUNT);
		static const unsigned int BUFFER_SIZE_IN_BYTES = ((MAX_BITSIZEOF_DATA - (BITSIZEOF_HEADER + 7))>>3);

		u8			 m_buffer[BUFFER_SIZE_IN_BYTES];  // contains the array handler identifier
		datBitBuffer m_bb;
		netSequence	 m_messageSeqNum;	// the seq number of the array manager update message
		netSequence	 m_handlerSeqNum;	// the seq number of the array handler update
		u8			 m_splitUpdateNum;
	};

	//PURPOSE
	//	Caches the sequence numbers of array handler sync updates.
	class updateMsgHistory
	{
		friend netArrayManager;

		FW_REGISTER_CLASS_POOL(updateMsgHistory);

	protected:

		updateMsgHistory(netSequence seq) : m_MessageSeqNum(seq), m_NumHandlers(0), m_TimeSent(0), m_FirstElement(-1), m_LastElement(-1)
		{
			for (u32 i=0; i<MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG; i++)
			{
				m_Handlers[i] = NULL;
			}
		}

		void AddHandler(netArrayHandlerBase* pHandler)
		{
			if (gnetVerify(m_NumHandlers < MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG))
			{
				m_Handlers[m_NumHandlers] = pHandler;
				m_NumHandlers++;
			}
		}

		netSequence				m_MessageSeqNum;												// the sequence number of the update message
		netArrayHandlerBase    *m_Handlers[MAX_NUM_ARRAY_HANDLER_UPDATES_PER_UPDATE_MSG];		// the handlers which had updates in the message
		unsigned		 		m_NumHandlers;													// the number of handlers added
		unsigned				m_TimeSent;														// set when the update for this history is sent
		int						m_FirstElement;													// The first element in the update (only used if there is only one handler)
		int						m_LastElement;													// The last element in the update (only used if there is only one handler)

		inlist_node<updateMsgHistory> m_ListLink;
	};

	//PURPOSE
	//	Holds a split update message from an array handler until it can be processed. When an array handler's update data exceeds the size of a 
	// single message packet, the data is split across a number of updates and these are cached at the receiving end until they can all be processed
	// simultaneously.
	class cachedSplitUpdate
	{
		friend netArrayManager;

	public:

		cachedSplitUpdate() : m_Handler(NULL), m_Player(NULL) {}

		bool IsFree() const { return (m_Handler == NULL); }

		void Free()
		{
			m_Handler = NULL;
			m_Player = NULL;
		}

	protected:

		const netArrayHandlerBase*	m_Handler;		// the handler the update was for
		const netPlayer*			m_Player;		// the player we received the update from
		netSequence					m_Seq;			// the sequence number of the update
		unsigned					m_UpdateNum;	// the number of the update
		unsigned					m_UpdateSize;	// the size of the update data
		msgUpdate					m_UpdateMsg;	// the update received 
	};

	// struct used to sort handlers based on the size of their update data
	struct handlerUpdateInfo
	{
		netArrayHandlerBase*	m_Handler;									// the handler the update is for
		unsigned				m_TotalUpdateSize;							// the total size of the handler element update data
		unsigned				m_NumUpdates;								// the number of update messages required to send all of the element update data
		u16						m_UpdateStartElements[MAX_SPLIT_UPDATES];	// the first element of each update
	};

	typedef inlist<updateMsgHistory, &updateMsgHistory::m_ListLink> updateMsgHistoryQueue;
	typedef inlist<netArrayHandlerBase, &netArrayHandlerBase::m_ListLink>	arrayHandlerList;
	typedef inlist<netArrayHandlerBase, &netArrayHandlerBase::m_BatchListLink>	arrayHandlerBatchList;

public:

    netArrayManager(netBandwidthMgr &bandwidthMgr);
    virtual ~netArrayManager();

	//PURPOSE
	// Initialises the array manager. Called once a network game has been initialised.
    virtual void Init();
 
	//PURPOSE
	//	Shuts down the array manager and reclaims resources.
	virtual void Shutdown();

	//PURPOSE
	// Updates the array manager and all array handlers.
    virtual void Update();

	//PURPOSE
	// Called when the given player joins the session. Informs all the handlers.
    void PlayerHasJoined(const netPlayer& player);
 
	//PURPOSE
	// Called when the given player leaves the session. Informs all the handlers.
	void PlayerHasLeft(const netPlayer& player);

	//PURPOSE
	// Called when the session host changes
	void NewHost();

	//PURPOSE
	// Returns the array handler matching the given type and identifier.
	netArrayHandlerBase* GetArrayHandler(const NetworkArrayHandlerType handlerType, const netArrayIdentifierBase* identifier = NULL);

    // log file access
    netLoggingInterface &GetLog() { return *m_Log; }

	// logging
	void LogArrayElementsMsgHeader(netArrayHandlerBase* handler, netArrayIdentifierBase* identifier, bool sent, const netPlayer &player, netSequence seqNum, bool bSplitUpdate = false, unsigned updateNum = 0, unsigned totalUpdates = 0);
	void LogArrayElementsMsgRejection(const char *messageText, ...);
	void LogArrayElementsAck(const netPlayer& player, bool sent, netSequence seqNum, netSequence ackSeq, unsigned handlersProcessed);
	void LogArrayElementsSplitUpdateAck(const netPlayer& player, bool sent, netArrayHandlerBase& handler, netSequence messageSeq, netSequence updateSeq, unsigned splitUpdateNum, int unsyncedPlayers=-1);

	// called when the arbitration of a handler changes
	void ArrayHandlerArbitrationChanged(netArrayHandlerBase* handler);

	// returns the total size of all array data that is locally arbitrated
	u32 GetSizeOfLocallyArbitratedArrayData();

	bool HasMaxNumberOfHighFrequencyUpdateArrays() { return m_ArrayHandlerBatches[UBT_HIGH_FREQUENCY].size() >= MAX_HIGH_FREQUENCY_ARRAYS; }

#if __BANK

    //PURPOSE
    // Returns the number of array manager update messages sent since the last call to ResetMessageCounts()
    unsigned GetNumArrayManagerUpdateMessagesSent() const { return m_NumArrayManagerUpdateMessagesSent; }   

    //PURPOSE
    // Returns the number of array manager ACK messages sent since the last call to ResetMessageCounts()
    unsigned GetNumArrayManagerAckMessagesSent() const { return m_NumArrayManagerAckMessagesSent; }

    //PURPOSE
    // Reset the counts of array manager messages sent
    void ResetMessageCounts() { m_NumArrayManagerUpdateMessagesSent = m_NumArrayManagerAckMessagesSent = 0; }

	netBandwidthStatistics& GetBandwidthStatistics() { return m_BandwidthStats; }
#endif 

protected:

	virtual unsigned GetNumArrayHandlerTypes() const = 0;

    //PURPOSE
    // Handles an incoming network message
    //PARAMS
    // messageData - Data describing the incoming message
    void OnMessageReceived(const ReceivedMessageData &messageData);

	//PURPOSE
	// Registers a new array handler with the manager.
    virtual void RegisterArrayHandler(netArrayHandlerBase* handler);
 
	//PURPOSE
	// Unregisters and destroys the given array handler.
	virtual void UnregisterArrayHandler(netArrayHandlerBase* handler, bool bDestroyHandler = true);

	//PURPOSE
	// Called when a new array handler update is being started. Manages the update history queue and the current update message for the given player.
	//PARAMS
	// toPlayer		- the player the update is being sent to
	// handler		- the handler generating the update
	// updateSize	- the total size of the handler update
	// fromPlayer	- the player sending the update (only used by bots)
	// firstElement - the first element of the update (only used for split updates)
	// lastElement  - the last element of the update (only used for split updates)
	void StartNewUpdate(const netPlayer& toPlayer, netArrayHandlerBase* handler, unsigned updateSize, const netPlayer* fromPlayer = NULL, int firstElement = -1, int lastElement = -1);

	//PURPOSE
	// Sends out the current update message for the given player, if necessary. 
	//PARAMS
	//	toPlayer - the player the update message is being sent to
	// fromPlayer - the player sending the message (only used by player bots)
	void SendUpdateMessage(const netPlayer& toPlayer, const netPlayer* fromPlayer = NULL);

	//PURPOSE
	// Caches a split update received for an array handler
	//PARAMS
	// handler			 - the handler this update is for
	// fromPlayer	     - the player we got the update from
	// toPlayer		     - the player the update was sent to
	// bb				 - the bit buffer containing the handler update
	// dataSize			 - the size of the handler data in the bit buffer
	// messageSeq		 - the sequence number of the array manager update message 
	// updateSeq		 - the sequence number of the array handler update 
	// splitUpdateNum	 - the number of this split update 
	// totalSplitUpdates - the total number of split updates 
	void CacheSplitHandlerUpdate(netArrayHandlerBase& handler, const netPlayer& fromPlayer, const netPlayer& toPlayer, datBitBuffer& bb, unsigned dataSize, netSequence messageSeq, netSequence updateSeq, unsigned splitUpdateNum, unsigned totalSplitUpdates);

	//PURPOSE
	// Used to sort handlers based on the size of their current update data
	static int SortHandlerByUpdateSize(const void *paramA, const void *paramB);

private:

    //PURPOSE
    // Returns the log splitter object for use internally by this class
    netLogSplitter &GetLogSplitter() { return *m_LogSplitter; }

	//PURPOSE
	// Adds the array handler to a batch list
	void AddArrayHandlerToBatch(netArrayHandlerBase* handler);

    //PURPOSE
    // Destroys the specified network array handler
    virtual void DestroyArrayHandler(netArrayHandlerBase* handler);

    //PURPOSE
    // Handles an array update message
    //PARAMS
    // messageData - The message data
    void HandleArrayUpdateMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Handles an array update ACK message
    //PARAMS
    // messageData - The message data
    void HandleArrayUpdateACKMsg(const ReceivedMessageData &messageData);

   //PURPOSE
    // Handles an array split update ACK message
    //PARAMS
    // messageData - The message data
    void HandleArraySplitUpdateACKMsg(const ReceivedMessageData &messageData);

	//PURPOSE
	// Recalculates the size of all the locally arbitrated array data and caches it in the local netPlayer
	void UpdateSizeOfLocallyArbitratedArrayData();

	unsigned GetCommonUpdateHeaderSize() const
	{
		// handler type + update sequence + split update flag + identifier flag + size of handler data
		return TAG_SIZE + m_BitsizeOfHandlerType + (sizeof(netSequence)<<3) + 2 + SIZE_OF_HANDLER_DATA_LEN;
	}

protected:

    // A unique message handler only used by the game manager.
    NetworkPlayerEventDelegate m_Dlgt;

	// an array of array handler lists, one for each array handler type. Used for quick lookup.
	atArray<arrayHandlerList> m_ArrayHandlers;

	// arrays of array handler lists, in batches. Used for reducing update load
	atFixedArray<arrayHandlerBatchList, NUM_UPDATE_BATCH_TYPES> m_ArrayHandlerBatches;

	// The sequence numbers of the most recently sent array updates to each player. 
	atFixedArray<netSequence, MAX_NUM_PHYSICAL_PLAYERS> m_LastSentSequences;

	// The sequence numbers of the most recently received array updates from each player. 
	atFixedArray<netSequence, MAX_NUM_PHYSICAL_PLAYERS> m_LastReceivedSequences;

	// Stores the recent history of sent array update messages.
	atFixedArray<updateMsgHistoryQueue, MAX_NUM_PHYSICAL_PLAYERS> m_UpdateHistories;

	// Pointers to the static identifiers used by each array type.
	atFixedArray<netArrayIdentifierBase*, MAX_NUM_ARRAY_HANDLER_TYPES> m_ArrayIdentifiers;

	// Cached update messages, used when an array handler's update exceeds the length of a message packet. 
	// Updates are cached until all of the update messages have been received. They are then all simultaneously processed by the handler.
	atFixedArray<cachedSplitUpdate, MAX_CACHED_UPDATES> m_CachedUpdates;

    // The update messages for each network player.
    atFixedArray< msgUpdate, MAX_NUM_PHYSICAL_PLAYERS> m_UpdateMsgs;

    // The logging objects
    netLogSplitter *m_LogSplitter;
    netLog         *m_Log;

	// The number of bits needed to write a handler type.
	unsigned m_BitsizeOfHandlerType;

    netBandwidthMgr &m_BandwidthMgr; // The bandwidth manager

	u32 m_CurrentLocalUpdateBatch; // the current array handler batch being processed

	bool m_IsInitialized;
	bool m_IsShuttingDown;

#if __BANK
	unsigned m_NumArrayManagerUpdateMessagesSent; // The number of array manager update messages sent since the last call to ResetMessageCounts()
    unsigned m_NumArrayManagerAckMessagesSent;    // The number of array manager ACK messages sent since the last call to ResetMessageCounts()

    netBandwidthStatistics m_BandwidthStats;

	// The bandwidth recorders for the different message types that is not array data.
	RecorderId m_UpdateMsgHeaderRecorderId;
	RecorderId m_UpdateAckMsgRecorderId;

	// The bandwidth recorders for each array type.
	atFixedArray<RecorderId, MAX_NUM_ARRAY_HANDLER_TYPES> m_ArrayRecorderIds;
#endif

private:

    static const unsigned MAX_PLAYERS_TO_UPDATE_PER_FRAME       = 8;
    static const unsigned UPDATE_THROTTLE_INTERVAL              = 1000;
    static const unsigned DEFAULT_MAX_MESSAGES_PER_PLAYER       = 5;
    static const unsigned LOW_BANDWIDTH_MAX_MESSAGES_PER_PLAYER = 1;

    // The number of messages sent to each network player in the last throttle interval
    unsigned m_NumUpdatesSent[MAX_NUM_PHYSICAL_PLAYERS];

    // The last time the update throttle interval was reset
    unsigned m_LastUpdateThrottleTime;

    // The maximum number of update messages sent per player during the current throttle intercal
    unsigned m_MaxMessagesPerThrottleInterval;
};

// ================================================================================================================
// netArrayManager_Script
// ================================================================================================================

//PURPOSE
//	An array manager with added script support, to be used in conjunction with the netScript code.
class netArrayManager_Script : public netArrayManager
{
public:

	netArrayManager_Script(netBandwidthMgr &bandwidthMgr) :
	netArrayManager(bandwidthMgr)
	{
	}

	virtual netHostBroadcastDataHandlerBase* GetScriptHostBroadcastDataArrayHandler(const scriptIdBase& scriptId, unsigned dataId) = 0;
	virtual netPlayerBroadcastDataHandlerBase* GetScriptPlayerBroadcastDataArrayHandler(const scriptIdBase& scriptId, unsigned dataId, const netPlayer& player) = 0;
	virtual netPlayerBroadcastDataHandlerBase* GetScriptPlayerBroadcastDataArrayHandler(const scriptIdBase& scriptId, unsigned dataId, unsigned clientSlot) = 0;

	netHostBroadcastDataHandlerBase*			RegisterScriptHostBroadcastData(unsigned* pArray, unsigned sizeOfArrayInBytes, const scriptHandler& scriptHandler, unsigned dataId, bool HasHighFrequencyUpdates BANK_ONLY(, const char* debugArrayName));
	void										RegisterScriptPlayerBroadcastData(unsigned* pArray, unsigned sizeOfArrayInBytes, const scriptHandler& scriptHandler, unsigned dataId, bool HasHighFrequencyUpdates BANK_ONLY(, const char* debugArrayName));
	void										UnregisterAllScriptBroadcastData(const scriptHandler& scriptHandler);

	//PURPOSE
	//  Swaps out a locally controlled player broadcast array handler for a remote one, or vice versa. If the new player arbitrator is set
	// this will mean that the new handler will be local, otherwise it will be remote.
	void SwapLocalAndRemotePlayerBroadcastDataHandlers(netPlayerBroadcastDataHandlerBase& playerHandler);

	//PURPOSE
	//   Returns true if all host broadcast data associated with the given script id is fully synced with all remote participant machines
	bool IsAllHostBroadcastDataSynced(const scriptIdBase& scriptId);

protected:

	// remember to overload this if you have more array handler types than the script broadcast data ones
	virtual unsigned GetNumArrayHandlerTypes() const;

	virtual netHostBroadcastDataHandlerBase* CreateHostBroadcastDataArrayHandler(unsigned* pArray, 
																				   unsigned sizeOfArrayInBytes, 
																				   const scriptHandler& scriptHandler,
																				   unsigned dataId
																				   BANK_ONLY(, const char* debugArrayName)) = 0;

	virtual netPlayerBroadcastDataHandlerBase* CreatePlayerBroadcastDataArrayHandler(unsigned* pArray, 
																					 unsigned sizeOfArrayInBytes, 
																					 const scriptHandler& scriptHandler, 
																					 unsigned dataId,
																					 unsigned slot
																					 BANK_ONLY(, const char* debugArrayName)) = 0;
};

} // namespace rage

#endif  // NET_ARRAYMGR_H
