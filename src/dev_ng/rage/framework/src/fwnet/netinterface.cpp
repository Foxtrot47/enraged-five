//
// netinterface.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#include "netinterface.h"

#include "net/timesync.h"
#include "system/timer.h"

#include "fwnet/netarraymgr.h"
#include "fwnet/neteventmgr.h"
#include "fwnet/neteventtypes.h"
#include "fwnet/netlogginginterface.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/optimisations.h"
#include "fwscript/scripthandlermgr.h"
#include "fwscript/scriptinterface.h"

NETWORK_OPTIMISATIONS()

PARAM(localnetlogs,"[network] Write network logs to local storage");
PARAM(noneblockingnetlogs,"[network] Prevent network logs from ever blocking the game update");

namespace rage
{

bool                          netInterface::m_IsInitialised = false;
bool                          netInterface::m_ShuttingDown  = false;
netArrayManager              *netInterface::m_ArrayMgr      = 0;
netConnectionManager         *netInterface::m_ConnectionMgr = 0;
netEventMgr                  *netInterface::m_EventMgr      = 0;
netObjectMgrBase             *netInterface::m_ObjectMgr     = 0;
netPlayerMgrBase             *netInterface::m_PlayerMgr     = 0;
netTimeSync                  *netInterface::m_TimeSync      = 0;
netLoggingInterface          *netInterface::m_MessageLog    = 0;
netInterface::queryFunctions *netInterface::m_QueryFuncs    = 0;

bool     s_UpdateTimebars            = true;
bool     s_NetworkTimeUnInitialised  = false;
unsigned s_NetworkTimeLastFrameStart = 0;
unsigned s_NetworkTimeThisFrameStart = 0;

void netInterface::Init(netConnectionManager &connectionMgr,
                        netPlayerMgrBase     &playerMgr,
                        netTimeSync          &timeSync,
                        netLoggingInterface  &messageLog,
                        queryFunctions       &queryFuncs)
{
    Assert(!m_IsInitialised);
    Assert(m_ConnectionMgr == 0);
    Assert(m_PlayerMgr     == 0);
    Assert(m_TimeSync      == 0);
    Assert(m_MessageLog    == 0);

    m_ConnectionMgr = &connectionMgr;
    m_PlayerMgr     = &playerMgr;
    m_TimeSync      = &timeSync;
    m_MessageLog    = &messageLog;
    m_QueryFuncs    = &queryFuncs;

    s_NetworkTimeUnInitialised  = false;
    s_NetworkTimeLastFrameStart = 0;
    s_NetworkTimeThisFrameStart = 0;

    m_IsInitialised = true;
    m_ShuttingDown  = false;
}

void netInterface::Shutdown()
{
    m_ArrayMgr      = 0;
    m_ConnectionMgr = 0;
    m_EventMgr      = 0;
    m_ObjectMgr     = 0;
    m_PlayerMgr     = 0;
    m_TimeSync      = 0;
    m_MessageLog    = 0;
    m_QueryFuncs    = 0;

    m_IsInitialised = false;
    m_ShuttingDown  = false;
}

void netInterface::SetArrayManager(netArrayManager &arrayMgr)
{
    Assert(m_ArrayMgr == 0);

    m_ArrayMgr = &arrayMgr;
}

void netInterface::SetEventManager(netEventMgr &eventMgr)
{
    Assert(m_EventMgr == 0);

    m_EventMgr = &eventMgr;
}

void netInterface::SetObjectManager(netObjectMgrBase &objectMgr)
{
    Assert(m_ObjectMgr == 0);

    m_ObjectMgr = &objectMgr;
}

void netInterface::RegisterNetGameEvents()
{
    netEventTypes::RegisterNetGameEvents();
}

bool netInterface::IsHost()
{
    return GetLocalPlayer() && GetLocalPlayer()->IsHost();
}

const netPlayer* netInterface::GetHostPlayer()
{
	unsigned                 numActivePlayers = GetNumActivePlayers();
    const netPlayer * const *activePlayers    = GetAllActivePlayers();
    
    for(unsigned index = 0; index < numActivePlayers; index++)
    {
        const netPlayer *player = activePlayers[index];

		if(player && player->IsHost())
		{
			return player;
		}
	}

	return 0;
}

bool netInterface::IsSnSessionEstablished()
{
	Assert(m_QueryFuncs);
	return m_QueryFuncs->IsSnSessionEstablished();
}

bool netInterface::IsSessionEstablished()
{
    Assert(m_QueryFuncs);
    return m_QueryFuncs->IsSessionEstablished();
}

bool netInterface::NetworkClockHasSynced()
{
    Assert(m_TimeSync);
    return m_TimeSync->HasSynched();
}

unsigned netInterface::GetNetworkTime()
{
    Assert(m_QueryFuncs);
    return m_QueryFuncs->GetNetworkTime();
}

void netInterface::FlushAllLogFiles(bool waitForFlush)
{
	if (m_QueryFuncs)
	{
		m_QueryFuncs->FlushAllLogFiles(waitForFlush);
	}
}

unsigned netInterface::GetTimestampForPositionUpdates()
{
    return s_NetworkTimeThisFrameStart;
}

unsigned netInterface::GetTimestampForStartOfFrame()
{
    return s_NetworkTimeThisFrameStart;
}

unsigned netInterface::GetSynchronisationTime()
{
	u32 sysTime = sysTimer::GetSystemMsTime();

	// we need non-cached system time which changes during a single frame and won't wrap. 
	static u32 startTime = sysTime;

	u32 currentTime = sysTime - startTime;

	if (startTime > sysTime)
	{
		currentTime = ((u32)~0)-startTime+sysTime;
	}

	return currentTime;
}

unsigned netInterface::GetNumPendingPlayers()
{
	if(IsInitialised())
	{
		return m_PlayerMgr->GetNumPendingPlayers();
	}

	return 0;
}

unsigned netInterface::GetNumActivePlayers()
{
    if(IsInitialised())
    {
	    return m_PlayerMgr->GetNumActivePlayers();
    }

    return 0;
}

unsigned netInterface::GetNumRemoteActivePlayers()
{
    if(IsInitialised())
    {
	    return m_PlayerMgr->GetNumRemoteActivePlayers();
    }

    return 0;
}

unsigned netInterface::GetNumPhysicalPlayers()
{
    if(IsInitialised())
    {
	    return m_PlayerMgr->GetNumPhysicalPlayers();
    }

    return 0;
}

unsigned netInterface::GetNumLocalPhysicalPlayers()
{
   if(IsInitialised())
    {
	    return m_PlayerMgr->GetNumLocalPhysicalPlayers();
    }

    return 0;
}
unsigned netInterface::GetNumRemotePhysicalPlayers()
{
    if(IsInitialised())
    {
	    return m_PlayerMgr->GetNumRemotePhysicalPlayers();
    }

    return 0;
}

netPlayer * const *netInterface::GetPendingPlayers()
{
    if(IsInitialised())
    {
        return m_PlayerMgr->GetPendingPlayers();
    }

    return 0;
}

netPlayer * const *netInterface::GetAllActivePlayers()
{
    if(IsInitialised())
    {
        return m_PlayerMgr->GetAllActivePlayers();
    }

    return 0;
}

netPlayer * const *netInterface::GetRemoteActivePlayers()
{
    if(IsInitialised())
    {
	    return m_PlayerMgr->GetRemoteActivePlayers();
    }
    

    return 0;
}

netPlayer * const *netInterface::GetAllPhysicalPlayers()
{
    if(IsInitialised())
    {
        return m_PlayerMgr->GetAllPhysicalPlayers();
    }

    return 0;
}

netPlayer * const *netInterface::GetRemotePhysicalPlayers()
{
    if(IsInitialised())
    {
	    return m_PlayerMgr->GetRemotePhysicalPlayers();
    }

    return 0;
}

bool netInterface::IsConnectionOpen(const int cxnId)
{
    Assert(m_ConnectionMgr);
    return m_ConnectionMgr->IsOpen(cxnId);
}

netPlayer *netInterface::GetPlayerFromConnectionId(const int cxnId)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetPlayerFromConnectionId(cxnId);
}

netPlayer *netInterface::GetPlayerFromGamerId(const rlGamerId& gamerId)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetPlayerFromGamerId(gamerId);
}

netPlayer *netInterface::GetPlayerFromPeerId(const u64 peerId)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetPlayerFromPeerId(peerId);
}

netPlayer *netInterface::GetPhysicalPlayerFromIndex(const PhysicalPlayerIndex playerIndex)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetPhysicalPlayerFromIndex(playerIndex);
}

netPlayer *netInterface::GetActivePlayerFromIndex(const ActivePlayerIndex playerIndex)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetActivePlayerFromIndex(playerIndex);
}

netPlayer *netInterface::GetPlayerFromPeerPlayerIndex(const unsigned peerPlayerIndex, const rlPeerInfo &peerInfo)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetPlayerFromPeerPlayerIndex(peerPlayerIndex, peerInfo);
}

netPlayer *netInterface::GetLocalPlayer()
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->GetMyPlayer();
}

ActivePlayerIndex netInterface::GetLocalActivePlayerIndex()
{
    return GetLocalPlayer()->GetActivePlayerIndex();
}

PhysicalPlayerIndex netInterface::GetLocalPhysicalPlayerIndex()
{
	return GetLocalPlayer()->GetPhysicalPlayerIndex();
}

void netInterface::GetObjectIDsInUseLocally(ObjectId *idsInUseLocally, unsigned &numInUseIDs, unsigned &numLocallyOwnedIDs, unsigned idStart, unsigned idEnd)
{
    Assert(m_ObjectMgr);
    m_ObjectMgr->GetObjectIDsInUseLocally(idsInUseLocally, numInUseIDs, numLocallyOwnedIDs, idStart, idEnd);
}

const netPlayer *netInterface::GetPlayerFromObjectIdRangeIndex(const int idRangeIndex)
{
    const netPlayer *player = 0;

    unsigned                 numPhysicalPlayers = netInterface::GetNumPhysicalPlayers();
    const netPlayer * const *allPhysicalPlayers = netInterface::GetAllPhysicalPlayers();

    for(unsigned index = 0; index < numPhysicalPlayers; index++)
    {
        const netPlayer *currPlayer = allPhysicalPlayers[index];

        if(currPlayer &&
           currPlayer->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX &&
           currPlayer->GetPhysicalPlayerIndex() == idRangeIndex)
        {
            player = currPlayer;
            break;
        }
    }

    return player;
}

int netInterface::GetObjectIdRangeIndexFromPlayer(const netPlayer &player)
{
    if(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
    {
        return player.GetPhysicalPlayerIndex();
    }

    return -1;
}

netObject *netInterface::GetNetworkObject(const ObjectId objectID)
{
    Assert(m_ObjectMgr);
	return m_ObjectMgr->GetNetworkObject(objectID);
}

bool netInterface::IsPlayerMgrValid()
{
	return m_PlayerMgr != NULL;
}

bool netInterface::IsPlayerMgrInitialised()
{
	return m_PlayerMgr && m_PlayerMgr->IsInitialized();
}

netPlayerMgrBase &netInterface::GetPlayerMgr()
{
    Assert(m_PlayerMgr);
	return *m_PlayerMgr;
}

netTimeSync &netInterface::GetNetworkClock()
{
    Assert(m_TimeSync);
    return *m_TimeSync;
}

netObjectMgrBase &netInterface::GetObjectManager()
{
    Assert(m_ObjectMgr);
	return *m_ObjectMgr;
}

netEventMgr &netInterface::GetEventManager()
{
    Assert(m_EventMgr);
	return *m_EventMgr;
}

netArrayManager &netInterface::GetArrayManager()
{
    Assert(m_ArrayMgr);
	return *m_ArrayMgr;
}

netLoggingInterface &netInterface::GetObjectManagerLog()
{
    Assert(m_ObjectMgr);
	return m_ObjectMgr->GetLog();
}

netLoggingInterface &netInterface::GetMessageLog()
{
    Assert(m_MessageLog);
    return *m_MessageLog;
}

void netInterface::SetShouldUpdateTimebars(bool updateTimebars)
{
    s_UpdateTimebars = updateTimebars;
}

void netInterface::StartOfFrame()
{
    if(IsInitialised() && NetworkClockHasSynced())
    {
        s_NetworkTimeLastFrameStart = s_NetworkTimeThisFrameStart;
        s_NetworkTimeThisFrameStart = GetNetworkTime();

        // ensure we use a sensible value for the first frame after the network code is initialised
        if(!s_NetworkTimeUnInitialised)
        {
            s_NetworkTimeLastFrameStart = s_NetworkTimeThisFrameStart;
            s_NetworkTimeUnInitialised  = true;
        }
    }
}

bool netInterface::ShouldUpdateTimebars()
{
    return s_UpdateTimebars;
}

unsigned netInterface::GetTimeSyncMessageSentAndCheckExpiry(PhysicalPlayerIndex playerIndex, netSequence syncMessageSeq, unsigned expiryTime)
{
    Assert(m_ObjectMgr);
	return m_ObjectMgr->GetTimeSyncMessageSentAndCheckExpiry(playerIndex, syncMessageSeq, expiryTime);
}

LogTargetType netInterface::GetDefaultLogFileTargetType()
{
    if(PARAM_localnetlogs.Get())
    {
        return LOG_TARGET_LOCAL;
    }
    else
    {
        return LOG_TARGET_DEFAULT;
    }
}

LogBlockingMode netInterface::GetDefaultLogFileBlockingMode()
{
    if(PARAM_noneblockingnetlogs.Get())
    {
        return LOG_NON_BLOCKING;
    }
    else
    {
        return LOG_BLOCKING;
    }
}

#if ENABLE_NETWORK_BOTS

unsigned netInterface::GetNumTotalActiveBots()
{
    return GetPlayerMgr().GetNumTotalActiveBots();
}

unsigned netInterface::GetNumLocalActiveBots()
{
    return GetPlayerMgr().GetNumLocalActiveBots();
}

unsigned netInterface::GetNumRemoteActiveBots()
{
    return GetPlayerMgr().GetNumRemoteActiveBots();
}

#endif // ENABLE_NETWORK_BOTS

#if __BANK

unsigned netInterface::GetNumUnreliableMessagesSent()
{
    unsigned numMessages = 0;

    numMessages += GetArrayManager().GetNumArrayManagerUpdateMessagesSent();
    numMessages += GetArrayManager().GetNumArrayManagerAckMessagesSent();
    numMessages += GetEventManager().GetNumUnreliableEventMessagesSent();
    numMessages += GetObjectManager().GetNumCloneSyncMessagesSent();
    numMessages += GetObjectManager().GetNumCloneSyncAckMessagesSent();
    numMessages += GetPlayerMgr().GetNumNonPhysicalUpdateMessagesSent();
    numMessages += GetPlayerMgr().GetNumMiscUnreliableMessagesSent();

    return numMessages;
}

unsigned netInterface::GetNumReliableMessagesSent()
{
    unsigned numMessages = 0;

    numMessages += GetEventManager().GetNumReliableEventMessagesSent();
    numMessages += GetObjectManager().GetNumPackedReliableMessagesSent();
    numMessages += GetObjectManager().GetReassignMgr().GetNumReassignNegotiateMessagesSent();
    numMessages += GetObjectManager().GetReassignMgr().GetNumReassignConfirmMessagesSent();
    numMessages += GetObjectManager().GetReassignMgr().GetNumReassignResponseMessagesSent();
    numMessages += GetPlayerMgr().GetNumMiscReliableMessagesSent();
    numMessages += scriptInterface::GetScriptManager().GetNumScriptManagerMessagesSent();

    return numMessages;
}

void netInterface::ResetMessageCounts()
{
    GetArrayManager().ResetMessageCounts();
    GetEventManager().ResetMessageCounts();
    GetObjectManager().ResetMessageCounts();
    GetPlayerMgr().ResetMessageCounts();
    scriptInterface::GetScriptManager().ResetMessageCounts();
}

#endif // __BANK

bool netInterface::SendBuffer(const netPlayer* toPlayer,
                              const void* buffer,
                              const unsigned numBytes,
                              const unsigned sendFlags,
                              netSequence* seq,
                              const netPlayer *fromPlayer)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->SendBuffer(toPlayer,
                                    buffer,
                                    numBytes,
                                    sendFlags,
                                    seq,
                                    fromPlayer);
}

bool netInterface::SendResponseBuffer(const netTransactionInfo& txInfo,
                                      const void* buffer,
                                      const unsigned numBytes)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->SendResponseBuffer(txInfo,
                                            buffer,
                                            numBytes);
}

void netInterface::AddDelegate(NetworkPlayerEventDelegate* dlgt)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->AddDelegate(dlgt);
}

void netInterface::RemoveDelegate(NetworkPlayerEventDelegate* dlgt)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->RemoveDelegate(dlgt);
}

bool netInterface::SendRequestBuffer(const netPlayer* player,
                                     const void* buffer,
                                     const unsigned numBytes,
                		             const unsigned timeout,
	                                 netResponseHandler* handler)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->SendRequestBuffer(player,
                                            buffer,
                                            numBytes,
                                            timeout,
                                            handler);
}

void netInterface::AddRequestHandler(NetworkPlayerRequestHandler* rqstHandler)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->AddRequestHandler(rqstHandler);
}

void netInterface::RemoveRequestHandler(NetworkPlayerRequestHandler* rqstHandler)
{
    Assert(m_PlayerMgr);
	return m_PlayerMgr->RemoveRequestHandler(rqstHandler);
}

} // namespace rage
