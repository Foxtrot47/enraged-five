//
// netlog.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#ifndef INC_NETWORKLOG_H_
#define INC_NETWORKLOG_H_

#include <string.h>

#include "fwnet/netlogginginterface.h"
#include "system/ipc.h"

#if __DEV || __BANK
#define ENABLE_NETWORK_LOGGING 1
#else
#define ENABLE_NETWORK_LOGGING 0
#endif

#if ENABLE_NETWORK_LOGGING
#define LOGGING_ONLY(...) __VA_ARGS__
#else
#define LOGGING_ONLY(...)
#endif

namespace rage
{

class netLogStringTable;
class packedLogDataWriter;
class packedLogDataReader;

//PURPOSE
// Determines how the log file is opened if a file already exists
enum LogOpenType
{
	LOGOPEN_CREATE,
	LOGOPEN_APPEND,
};

//PURPOSE
// Determines where the log files are written to. This allows
// users of builds streamed from a PC to send network log files to
// local storage (e.g. a console hard drive)
enum LogTargetType
{
	LOG_TARGET_DEFAULT,
	LOG_TARGET_LOCAL,
};

//PURPOSE
// Determines the blocking mode for a log file - blocking log files
// will never lose data, but can cause the game to stall, while non-blocking
// log files never stall the game but any pending log data will be lost if the
// data can't be spooled in time
enum LogBlockingMode
{
    LOG_BLOCKING,
    LOG_NON_BLOCKING
};

//PURPOSE
// This class is the interface for file access for the network logging code.
// This allows projects to use there local file management systems for log output
class netLogFileAccessInterface
{
public:

    typedef void* LogHandle;

    //PURPOSE
    // Creates a packed log data writer, which can be project specific to support
    // additional packed logged data
    virtual packedLogDataWriter *CreatePackedLogDataWriter(char     *logBuffer,
                                                           unsigned  logBufferSize) = 0;

    //PURPOSE
    // Creates a packed log data reader, which can be project specific to support
    // additional packed logged data
    virtual packedLogDataReader *CreatePackedLogDataReader(const char                           *packedLogData,
                                                           unsigned                              packedDataSize,
                                                           netLogFileAccessInterface::LogHandle &logHandle) = 0;

    //PURPOSE
    // Opens a log with the specified filename for writing output
    //PARAMS
    // filename - The name of the file to open
    virtual LogHandle OpenFile(const char *filename, LogOpenType openType) = 0;

    //PURPOSE
    // Closes the log with the specified handle
    //PARAMS
    // logHandle - The file handle to close
    virtual void CloseFile(LogHandle logHandle) = 0;

    //PURPOSE
    // Flushes the log with the specified handle
    //PARAMS
    // logHandle - The file handle to flush
    virtual void Flush(LogHandle logHandle) = 0;

    //PURPOSE
    // Write log output the file with the specified handle
    //PARAMS
    // logHandle - The log handle to close
    // logText    - The text to write
    // textSize   - The size of the text to write
    virtual void Write(LogHandle logHandle, const char *logText, unsigned textSize) = 0;

protected:

    netLogFileAccessInterface() {}
    virtual ~netLogFileAccessInterface() {}
};

//PURPOSE
// Packed log data tokens
enum
{
    NEW_FRAME_DATA,
    WRITE_DATA_VALUE,
    STANDARD_DATA,
    END_OF_DATA,
    MESSAGE_HEADER_DATA,
    PLAYER_TEXT_DATA,
    LOG_EVENT_DATA,
    NODE_HEADER_DATA,
    START_USER_DATA
};

const unsigned SIZE_OF_LOG_TOKEN       = 1;
const unsigned SIZE_OF_BOOL            = 1;
const unsigned SIZE_OF_STRING_TABLE_ID = 2;
const unsigned SIZE_OF_SEQUENCE        = 2;

//PURPOSE
// Class for writing packed log data to a buffer
class packedLogDataWriter
{
public:
    //PURPOSE
    // Class constructor
    //PARAMS
    // logBuffer     - The buffer to write the packed data into
    // logBufferSize - The size of the buffer in bytes
    packedLogDataWriter(char     *logBuffer,
                        unsigned  logBufferSize);

    //PURPOSE
    // Class destructor
    virtual ~packedLogDataWriter() {}

    //PURPOSE
    // Returns the buffer used to write packed data into
    const char *GetBuffer();

    //PURPOSE
    // Returns the size of the buffer
    unsigned GetBufferSize();

    //PURPOSE
    // Returns the number of bytes that have been written to the buffer
    unsigned GetSizeWritten();

    //PURPOSE
    // Sets new values for the buffer, this also resets this object back to an empty state
    //PARAMS
    // logBuffer     - The buffer to write the packed data into
    // logBufferSize - The size of the buffer in bytes
    void SetBuffer(char    *logBuffer,
                   unsigned logBufferSize);

    //PURPOSE
    // Writes the current throughput of data for the object. This is the number
    // of bytes written to the buffer since this function was last called, or since
    // the class was created on the first call
    unsigned GetThroughputInBytes();

    //PURPOSE
    // Adds data describing a new logging frame. This is called when the
    // current logging frame count, system time or network time changes inbetween logging calls
    //PARAMS
    // newFrameData - string describing the logging frame
    bool AddNewFrameData(const char *newFrameData);

    //PURPOSE
    // Add a data item for logging as a key/value pair
    //PARAMS
    // dataName  - The name of the data item to log
    // dataValue - The value for the data item to log
    bool AddWriteDataValueData(const char *dataName,
                               const char *dataValue);

    //PURPOSE
    // Adds unpacked log data to the buffer
    //PARAMS
    // logString - The unpacked log data
    bool AddStandardLogData(const char *logString);

    //PURPOSE
    // Adds an end of data marker to the file. Subsequent calls to pack logging
    // data will fail until the buffer is reset (via a call to SetBuffer())
    bool AddEndOfDataMarker();

    //PURPOSE
    // Adds message header data for logging
    //PARAMS
    // received       - Indicates whether the message was sent or received
    // sequence       - Sequence number for the message being sent
    // messageString1 - First portion of message header text
    // messageString2 - Second portion of message header text
    // playerName     - Name (or IP address) of the player message is sent/received from/to
    bool AddMessageHeaderData(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName);

    //PURPOSE
    // Adds logging data relative to a specific player to log
    //PARAMS
    // dataName   - name of the data to log
    // dataValue  - value of the data to log
    // playerName - The name of the player this data is relative to
    bool AddPlayerTextData(const char *dataName,
                           const char *dataValue,
                           const char *playerName);

    //PURPOSE
    // Adds a logging event to log
    //PARAMS
    // eventName          - Name of the event
    // eventData          - Data associated with the event
    // trailingLineBreaks - Number of new lines to append to the end of the logging event
    bool AddLogEventData(const char *eventName,
                         const char *eventData,
                         unsigned    numTrailingLineBreaks);

    //PURPOSE
    // Adds a node header event to log
    //PARAMS
    // nodeName - Name of the node
    bool AddNodeHeaderData(const char *nodeName);

protected:

    inline unsigned SIZE_OF_LOG_STRING(const char *string)
    {
        return (unsigned)strlen(string) + 1;
    }

    //PURPOSE
    // Checks whether the current network logging frame has changed
    void CheckForFrameDataChange();

    //PURPOSE
    // Checks whether there is space in the buffer for the specified number of bytes
    bool HasSpace(unsigned size);

    //PURPOSE
    // Writes a logging token to the buffer. The token describes what type of data follows it in the buffer
    //PARAMS
    // token - The token to write to the buffer
    bool WriteToken(char token);

    //PURPOSE
    // Writes a boolean value to the buffer
    //PARAMS
    // value - The boolean value to write
    bool WriteBool(bool value);

    //PURPOSE
    // Writes a netsequence to the buffer
    //PARAMS
    // value - The netsequence to write
    bool WriteSequence(netSequence sequence);

    //PURPOSE
    // Writes a string table ID to the buffer.
    //PARAMS
    // token - The ID to write to the buffer
    bool WriteStringTableID(unsigned stringID);

    //PURPOSE
    // Writes a string to the buffer from the string table, if it is present there.
    // If not, a standard string is written (via WriteString).
    bool WriteStringFromTable(const char *string);

    //PURPOSE
    // Writes a string to the buffer
    //PARAMS
    // string - The string to write to the buffer
    bool WriteString(const char *string);

    //PURPOSE
    // Writes a single character to the buffer
    //PARAMS
    // character - The character to write to the buffer
    bool WriteChar(const char character);

    char    *m_LogBuffer;              // The buffer to use for packing log data
    char    *m_WritePos;               // The position within the buffer to write subsequent data to
    unsigned m_LogBufferSize;          // The size of the packed log data buffer
    bool     m_EndOfFileWritten;       // Indicates whether an end of file token has been written to the buffer
    unsigned m_LastFrameCount;         // The last frame count when some logging data was packed (used for updating the network logging frame)
    unsigned m_LastSystemTime;         // The last system time when some logging data was packed (used for updating the network logging frame)
    unsigned m_LastNetworkTime;        // The last network time when some logging data was packed (used for updating the network logging frame)
    unsigned m_LastThroughputStartPos; // The number of bytes written to the buffer when the throughput of the object was last checked
};

//PURPOSE
// Class for Reading packed log data and sending it to an output device in a readable format
class packedLogDataReader
{
public:

    //PURPOSE
    // Class constructor
    //PARAMS
    // packedLogData  - The packed logging data to unpack
    // packedDataSize - The size of the packed logging data to unpack
    // fileAccess     - File access interface - provides access to writing logging data to an output device
    // logHandle      - The logging handle to write unpacked log data to
    packedLogDataReader(const char *packedLogData,
                        unsigned packedDataSize,
                        netLogFileAccessInterface &fileAccess,
                        netLogFileAccessInterface::LogHandle &logHandle);

    //PURPOSE
    // Class destructor
    virtual ~packedLogDataReader() {}

    //PURPOSE
    // Returns the size of the data written to the output device
    unsigned GetSizeWrittenToOuput() { return m_UnpackedDataSize; }

    //PURPOSE
    // Sets new values for the logging data to unpacked. This also resets the object to a start state (no data unpacked)
    //PARAMS
    // packedLogData  - The packed logging data to unpack
    // packedDataSize - The size of the packed logging data to unpack
    void SetPackedLogData(char     *packedLogData,
                          unsigned  packedLogDataSize);

    //PURPOSE
    // Unpacks the logging data and writes it to an output device. This function returns the number of bytes written to the output device
    void WriteData();

protected:

    //PURPOSE
    // Processes packed logging data for the specified token. The value of token indicates which type
    // of data follows in the buffer
    //PARAMS
    // logToken             - Indicates which type of data follows in the buffer
    // bytesWrittenToOutput - The number of bytes written to the output device
    virtual bool ProcessPackedLogData(unsigned logToken, unsigned &bytesWrittenToOutput);

    //PURPOSE
    // Gets a bool from the packed log data
    //PARAMS
    // value - storage for result
    bool GetBool(bool &value);

    //PURPOSE
    // Gets a netsequence from the packed log data
    //PARAMS
    // sequence - storage for result
    bool GetSequence(netSequence &sequence);

    //PURPOSE
    // Gets a string from the packed log data
    //PARAMS
    // buffer     - buffer for copying the string into
    // bufferSize - size of the buffer
    bool GetString(char *buffer, const unsigned bufferSize);

    //PURPOSE
    // Gets a string from the string table, based on an ID in the packed log data
    //PARAMS
    // buffer     - buffer for copying the string into
    // bufferSize - size of the buffer
    bool GetStringFromTable(char *buffer, const unsigned bufferSize);

    //PURPOSE
    // Gets an unsigned integer from the packed log data
    //PARAMS
    // value - The value unpacked
    bool GetUnsignedInteger(unsigned &value);

    static const unsigned MAX_FRAME_DATA = 128;

    const char                           *m_PackedLogData;    // The packed logging data to write to output
    const char                           *m_ReadPos;          // The current read position within the buffer
    unsigned                              m_PackedDataSize;   // The size of the data to unpack
    unsigned                              m_UnpackedDataSize; // The size of the unpacked data that was written to the output device
    netLogFileAccessInterface            &m_FileAccess;       // Provides access to writing the data to an output device
    netLogFileAccessInterface::LogHandle &m_LogHandle;        // Handle to output file to write to

    // String describing the current logging frame
    char m_CurrentFrameData[MAX_FRAME_DATA];

private:

    //PURPOSE
    // Processes new frame data from the packed log data
    void ProcessNewFrameData();

    //PURPOSE
    // Processes a key/value pair from the packed log data
    unsigned ProcessWriteDataValueData();

    //PURPOSE
    // Processes unpacked logging data from the packed log data
    unsigned ProcessStandardData();

    //PURPOSE
    // Processes message header data from the packed log data
    unsigned ProcessMessageHeaderData();

    //PURPOSE
    // Processes logging data relative to a specific player from the packed log data
    unsigned ProcessPlayerTextData();

    //PURPOSE
    // Processes logging event data from the packed log data
    unsigned ProcessLogEventData();

    //PURPOSE
    // Processes node header data from the packed log data
    unsigned ProcessNodeHeaderData();
};

//PURPOSE
// This is a stub class for use when logging is disabled. It provides
// empty implementations of the network logging interface
class netLogStub : public netLoggingInterface
{
public:

    explicit netLogStub(const char     *UNUSED_PARAM(fileName),
                        LogOpenType     UNUSED_PARAM(openType),
                        LogTargetType   UNUSED_PARAM(targetType),
                        LogBlockingMode UNUSED_PARAM(blockingMode),
                        unsigned        UNUSED_PARAM(logBufferSize) = 0) {}
    ~netLogStub() {}

    static void SetFileAccessInterface(netLogFileAccessInterface *UNUSED_PARAM(fileAccess)) {}
    static void SetMaximumFileSize(u32 UNUSED_PARAM(maxSize)){}
    static void StartLoggingThread() {}
    static void StopLoggingThread() {}

#if __BANK
    static const char *GetLogFileBeingSpooled(float &UNUSED_PARAM(completionPercent)) { return 0; }
#endif // __BANK

    void Log      (const char *UNUSED_PARAM(logText), ...) {};
    void LineBreak() {};
    void Flush(bool UNUSED_PARAM(waitForFlushToComplete) = false) {};

    void Disable() {};
    void Enable() {};
    bool IsEnabled() const {return false;}

    void SetUsesStringTable() {};

    void SetBlockingMode(LogBlockingMode UNUSED_PARAM(blockingMode));

    void WriteDataValue(const char *UNUSED_PARAM(dataName), const char *UNUSED_PARAM(dataValue), ...) {};

    void WriteMessageHeader(bool UNUSED_PARAM(received), netSequence UNUSED_PARAM(sequence), const char *UNUSED_PARAM(messageString1), const char *UNUSED_PARAM(messageString2), const char *UNUSED_PARAM(playerName)) {}

    void WritePlayerText(const char *UNUSED_PARAM(type), const char *UNUSED_PARAM(buffer), const char *UNUSED_PARAM(playerName)) {}

    void WriteEventData(const char *UNUSED_PARAM(eventName), const char *UNUSED_PARAM(eventData), unsigned UNUSED_PARAM(trailingLineBreaks)) {}

    void WriteNodeHeader(const char *UNUSED_PARAM(nodeName)) {}

#if __BANK
    void GetDebugStatisticsText(char *buffer, const unsigned UNUSED_PARAM(bufferSize)) { buffer[0] = '\0'; };
#endif // __BANK
};

//PURPOSE
// This is an implementation of the network logging interface
class netLogImpl : public netLoggingInterface
{
public:

    friend class packedLogDataReader;
    friend class packedLogDataWriter;

    // The maximum string that can be output in one logging call
    static const unsigned MAX_LOG_STRING = 1500;

    // The default size of the log buffers (for spooling via the log spool thread)
    static const unsigned DEFAULT_BUFFER_SIZE = 4 * 1024;

    //PURPOSE
    // Class constructor
    //PARAMS
    // fileName      - The name of the file to output logging to
    // openType      - The way to open the file (create/append)
    // targetType    - The target for the log files to be written to
    // blockingMode  - The blocking mode for the log file
    // logBufferSize - The size of the buffers used (2 buffers of this size are created)
    explicit netLogImpl(const char     *fileName, 
                        LogOpenType     openType,
                        LogTargetType   targetType,
                        LogBlockingMode blockingMode,
                        unsigned        logBufferSize = DEFAULT_BUFFER_SIZE);
    ~netLogImpl();

    //PURPOSE
    // Sets the file access interface for the log file, this allows the class to use
    // project specific implementations for file access
    //PARAMS
    // fileAccess - The file access interface to use
    static void SetFileAccessInterface(netLogFileAccessInterface *fileAccess) { sm_FileAccessInterface = fileAccess; }

    //PURPOSE
    // Sets the maximum file size of the log file generated before starting a new file.
    //PARAMS
    // maxSize - Maximum size in KB of the log files generated
    static void SetMaximumFileSize(u32 maxSize) { sm_MaximumFileSize = maxSize<<10; }

    //PURPOSE
    // Set the size of the heap to use for the string table (must be done before any log files are marked
    // to use the table)
    //PARAMS
    // heapSize - The size of the heap to use for the string table
    static void SetStringTableHeapSize(u32 heapSize) { sm_StringTableHeapSize = heapSize; }

    //PURPOSE
    // Starts the logging thread for spooling log output. No log files should be created before this is called
    static void StartLoggingThread();

    //PURPOSE
    // Stops the logging thread for spooling log output. All log file instances should be destroyed before this is called.
    static void StopLoggingThread();

    //PURPOSE
    // Returns whether the logging thread is currently running
    static bool IsLoggingThreadRunning();

#if __BANK
    //PURPOSE
    // Returns the file name of the log file currently being spooled. NULL if no file is being flushed
    //PARAMS
    // completionPercent - The percentage of the data currently spooled
    static const char *GetLogFileBeingSpooled(float &completionPercent);
#endif // __BANK

    //PURPOSE
    // Logs the specified text
    //PARAMS
    // logText - The text to log
    void Log(const char *logText, ...);

    //PURPOSE
    // Logs a line break
    //PARAMS
    // includeFrameCount - Indicates whether to prepend the frame count
    void LineBreak();

    //PURPOSE
    // Flushes any buffered log data to the output device
    void Flush(bool waitForFlushToComplete = false);

    //PURPOSE
    // Disables this log; any further log output will be ignored until Enable() is called
    void Disable() { m_Disable = true;}

    //PURPOSE
    // Enables this log
    void Enable() { m_Disable = false;}

    //PURPOSE
    // Returns whether this log file is enabled
    bool IsEnabled() const { return !m_Disable; }

    //PURPOSE
    // Marks the log file to use the string table (increases packing efficiency for log files with much string duplication). The string table
    // will only be created when this is called on one of the log files
    void SetUsesStringTable();

    //PURPOSE
    // Sets the blocking mode for the log file
    //PARAMS
    // blockingMode - The blocking mode to set for the log file
    void SetBlockingMode(LogBlockingMode blockingMode);

    //PURPOSE
    // Writes a key/value pair in a consistent format
    //PARAMS
    // dataName  - Key name
    // dataValue - Value name
    void WriteDataValue(const char *dataName, const char *dataValue, ...);

    //PURPOSE
    // Logs a message header in a consistent format
    //PARAMS
    // received       - Indicates whether the message was sent or received
    // sequence       - Sequence number for the message being sent
    // messageString1 - First portion of message header text
    // messageString2 - Second portion of message header text
    // playerName     - Name (or IP address) of the player message is sent/received from/to
    void WriteMessageHeader(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName);

    //PURPOSE
    // Logs data relative to a specific player in a consistent format
    //PARAMS
    // dataName   - The name of the data to log
    // dataValue  - The value of the data to log
    // playerName - The name of the player this data is related to
    void WritePlayerText(const char *dataName, const char *dataValue, const char *playerName);

    //PURPOSE
    // Writes a logging event to the specified network log file along with any specified data
    //PARAMS
    // eventName          - Name of the event
    // eventData          - Data associated with the event
    // trailingLineBreaks - Number of new lines to append to the end of the logging event
    void WriteEventData(const char *eventName, const char *eventData, unsigned trailingLineBreaks);

    //PURPOSE
    // Writes a node header to the specified network log file
    //PARAMS
    // nodeName - Name of the node
    void WriteNodeHeader(const char *nodeName);

    //PURPOSE
    // Checks whether this log has enough data pending to trigger a buffer swap at the end of the frame
    void PostNetworkUpdate();

    //PURPOSE
    // Spools data for this log file to disk. This function is called from the log file spool thread
    void SpoolLogData();

#if __BANK
    //PURPOSE
    // Writes debug statistics about this log file to the specified buffer
    //PARAMS
    // buffer     - buffer to write statistics to
    // bufferSize - size of the buffer in characters
    void GetDebugStatisticsText(char *buffer, const unsigned bufferSize);
#endif // __BANK

	//PURPOSE
	// Writes the file name to the specified buffer
	//PARAMS
	// buffer     - buffer to write the file name to
	// bufferSize - size of the buffer in characters
	void GetFileName(char *filename, u32 filenameSize) const;
	
	u32 GetLogSizeInBytes() { return m_BytesWritten; }

private:

    // The maximum supported filename size
    static const unsigned int MAX_LOG_FILE_NAME = 256;

    netLogImpl();
    netLogImpl(const netLogImpl &);

    netLogImpl &operator=(const netLogImpl &);

    //PURPOSE
    // Adjusts the specified filename to the required format for the target platform. The IP
    // Address of the console the application is running on is prepended here also.
    //PARAMS
    // filename     - output for final filename to use
    // filenameSize - size of the filename buffer specified
    bool SetUpFilename(char *filename, u32 filenameSize) const;

    //PURPOSE
    // Opens the log file ready for writing output
    bool Open();

	//PURPOSE
	// Internal function that opens a file and returns its logHandle
	netLogFileAccessInterface::LogHandle OpenInternal();

    //PURPOSE
    // Closes the log file
    void Close();

	//PURPOSE
	// Internal function that closes the specified log file
	void CloseInternal(netLogFileAccessInterface::LogHandle logHandle);
		
    //PURPOSE
    // Returns whether the log file is open for writing to
    bool IsOpen();

    //PURPOSE
    // Tries to swap the logging buffers - if the read buffer still has data on it, the function fails.
    bool TryToSwapBuffers();

    //PURPOSE
    // Handles the case where the log file could not swap buffers to spool new log output. This is due
    // to one buffer being still in the process of being spooled when the second buffer has filled. This function
    // decides what to do in this case based on the blocking mode of the log file
    void HandleBufferSwapFailure();

    //PURPOSE
    // Blocks until the logging buffers can be swapped and then swaps them
    void WaitAndSwapBuffers();

    //PURPOSE
    // Returns the throughput in bytes (amount written) per second
    unsigned GetThroughputInBytes();

    //PURPOSE
    // Returns the number of waits (due to writing more data that the logging thread can consume) per second
    unsigned GetWaitsPerSecond();

    //PURPOSE
    // Updates the statistics for this log file - this does not have to be called from client code
    void UpdateStatistics();

    //PURPOSE
    // Tries to add the specified string to the string table
    //PARAMS
    // string - The string to try and add to the table
    void TryToAddToStringTable(const char *string);

    // File access interface object for the network logging system
    static netLogFileAccessInterface *sm_FileAccessInterface;

    // Maximum size of the log files to create (in bytes)
    static u32 sm_MaximumFileSize;

    // String table for storing logging strings
    static netLogStringTable sm_StringTable;

    // Heap size to use for the string table
    static u32 sm_StringTableHeapSize;

    char                                 m_FileName[MAX_LOG_FILE_NAME]; // The filename
	LogOpenType                          m_OpenType;                    // How the file is to be opened
    LogTargetType                        m_TargetType;                  // The target for log files to be written to
    LogBlockingMode                      m_BlockingMode;                // The blocking mode for the log file
    netLogFileAccessInterface::LogHandle m_LogHandle;                   // The handle to log file
    LoggingLevel                         m_LogLevelIn;                  // The current log level in
    LoggingLevel                         m_LogLevelOut;                 // The current log level out
    bool                                 m_Disable;                     // Set to disable logging temporarily
    bool                                 m_UsesStringTable;             // Indicates whether this log files uses the string table (to restrict usage for logs with lots of duplicate strings)
    bool                                 m_AddedToSpool;                // True if the logfile has been added to the spool list
    u32                                  m_LastFrameHeaderWritten;      // The last frame a frame header was written
    u32                                  m_BytesWritten;                // Number of bytes written to the log file
    u32                                  m_PreviousFileCount;           // Number of files previously created (by writing more than the maximum file size)
    u32                                  m_MaxFileCount;                // Maximum number of files created before older files will be overwritten
    u32                                  m_LastTimeBuffersSwapped;      // The last time the buffers were swapped for this log file
    packedLogDataWriter                 *m_PackedLogDataWriter;         // class for writing packed log data to a buffer
    packedLogDataReader                 *m_PackedLogDataReader;         // class for reading packed log data and sending it to an output device in a readable format
    unsigned                             m_LogBufferSize;               // The size of the read and write buffers allocated
    sysIpcMutex                          m_BufferMutex;                 // Mutex for protecting the buffers for the spool thread
    sysIpcSema                           m_BufferSema;                  // Semaphore for indicating data is available for the spool thread
    char                                *m_ReadBuffer;                  // This is the buffer read from the spool thread (buffers swapped periodically)
    char                                *m_WriteBuffer;                 // This is the buffer written to by the game (buffers swapped periodically)
    unsigned                             m_ReadBufferSize;              // The size of data currently in the read buffer
    bool                                 m_FlushRequested;              // Signals to the spool thread that a request to flush the data has been received
    unsigned                             m_StatsStartTime;              // The system time the log file started recording stats for the current interval
    unsigned                             m_LastThroughputInBytes;       // The throughput in bytes for the last second recorded
    unsigned                             m_ThroughputInBytes;           // The throughput in bytes currently being recorded
    unsigned                             m_LastWaitsPerSecond;          // The number of waits per second for the last second recorded
    unsigned                             m_WaitsPerSecond;              // The number of waits currently being recorded
};

#if ENABLE_NETWORK_LOGGING
typedef netLogImpl netLog;
#else
typedef netLogStub netLog;
#endif
// --- Globals ------------------------------------------------------------------

} // namespace rage

#endif // !INC_NETWORKLOG_H_
