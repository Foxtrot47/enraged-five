//
// network/optimisations.h
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_OPTIMISATIONS_H
#define NETWORK_OPTIMISATIONS_H

#define NETWORK_OPTIMISATIONS_OFF		0


#if NETWORK_OPTIMISATIONS_OFF
#define NETWORK_OPTIMISATIONS()	OPTIMISATIONS_OFF()
#else
#define NETWORK_OPTIMISATIONS()
#endif	

#endif // !NETWORK_OPTIMISATIONS_H
