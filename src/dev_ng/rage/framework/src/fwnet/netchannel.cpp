 //
// netchannel.cpp
//
// Copyright (C) 1999-2014 Rockstar Games.  All Rights Reserved.
//

#include "netchannel.h"

#include <stdio.h>

RAGE_DEFINE_CHANNEL(net, 
					rage::DIAG_SEVERITY_DEBUG3, 
					rage::DIAG_SEVERITY_WARNING, 
					rage::DIAG_SEVERITY_ASSERT,
					rage::CHANNEL_POSIX_ON)