//
// neteventid.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#ifndef NETWORK_EVENTID_H
#define NETWORK_EVENTID_H

#include "atl/array.h"

#include "fwnet/nettypes.h"
#include "fwnet/netinterface.h"

namespace rage
{

typedef unsigned EventId;

//PURPOSE
// Each network game event is given a unique id when it is created. There are a limited number of id numbers that are available
// and only one number can be assigned to one event at a time. When an event is created it is assigned a free id.
// The event is then repeatedly sent to a player until an acknowledgement containing that EventId arrives. Once all
// players acknowledge the event the id number is freed again. At the other end, when an event arrives the sequence
// number of the id is checked against the last sequence number received for that id number. If it is greater then the
// event is processed and the stored sequence number is incremented. This prevents events being processed more than once.
// The sequence number sent to players is different as all events are not sent to all players.
class netEventID
{
public:

    static const unsigned SIZEOF_EVENT_ID       = 9; // The size in bits of the ID
    static const unsigned SIZEOF_EVENT_SEQUENCE = 8; // The size in bits of the ID sequence

public:

    netEventID() {}

    ~netEventID()
    {
        m_SeqsSent.Reset();
        m_SeqsReceived.Reset();
    }

    //PURPOSE
    // Returns the ID component of the event ID
    unsigned GetID() const { return m_ID; }

    //PURPOSE
    // Returns the sequence component of the event ID
    unsigned GetSendSequence(PhysicalPlayerIndex playerIndex) const { return m_SeqsSent[playerIndex]; }

    //PURPOSE
    // Initialises the event id at the start of a network game
    //PARAMS
    // ID - ID number
    void Init(const unsigned ID)
    {
        Assert(ID < (1<<SIZEOF_EVENT_ID));
        m_ID          = ID;
        m_PlayerInScopeFlags = 0;
		m_PlayerAckedFlags   = 0;
        m_InUse       = false;

        m_SeqsSent.Resize(MAX_NUM_PHYSICAL_PLAYERS);
        m_SeqsReceived.Resize(MAX_NUM_PHYSICAL_PLAYERS);

        // initialise the sent and received sequence IDs to 0 for each player
        for(PhysicalPlayerIndex playerIndex = 0; playerIndex < MAX_NUM_PHYSICAL_PLAYERS; playerIndex++)
        {
            m_SeqsSent[playerIndex]     = 0;
            m_SeqsReceived[playerIndex] = 0;
        }
    }

    //PURPOSE
    // Returns whether this event ID is free to use
    bool IsFree() const
    {
        return (!m_InUse);
    }

    //PURPOSE
    // Called when this event id is assigned to an event
    void Use()
    {
        Assert(!m_InUse);
        m_InUse = true;
    }

    //PURPOSE
    // Called when this event ID is ready for reuse
    void StopUsing()
    {
        Assert(m_InUse);
        m_InUse = false;
		m_PlayerInScopeFlags = 0;
		m_PlayerAckedFlags   = 0;
    }

    //PURPOSE
    // Flags a player that the event using this ID is in scope of the player
    //PARAMS
    // playerIndex - The player index of the player this event will be sent to
	// incrementSequence - When set then increment the sequence number sent to this player. Default is true.
	void SetPlayerInScope(const PhysicalPlayerIndex playerIndex, bool incrementSequence = true)
    {
        Assert(!(m_PlayerInScopeFlags &(1<<playerIndex)));
        m_PlayerInScopeFlags |= (1<<playerIndex);

		if (incrementSequence)
		{
			m_SeqsSent[playerIndex]++;

	       if (m_SeqsSent[playerIndex] >= (1<<SIZEOF_EVENT_SEQUENCE))
		    {
			    m_SeqsSent[playerIndex] = 0;
			}
		}
    }

    //PURPOSE
    // Returns true if the event using this ID needs to be sent to this player
    //PARAMS
    // playerIndex - The player index of the player to check
    bool MustSendToPlayer(const PhysicalPlayerIndex playerIndex) const
    {
		return ((m_PlayerAckedFlags & (1<<playerIndex)) == 0) && ((m_PlayerInScopeFlags & (1<<playerIndex)) != 0);
    }

    //PURPOSE
    // Marks a player after an acknowledgement arrives for the event using this id
    //PARAMS
    // playerIndex - The player index of the player to check
    void AckedByPlayer(const PhysicalPlayerIndex playerIndex)
    {
		m_PlayerAckedFlags |= (1<<playerIndex);
    }

	//PURPOSE
	// Returns the players that have still to ack this event
	PlayerFlags GetUnackedPlayers() const 
	{
		PlayerFlags returnFlags = 0;
		for (u32 i = 0; i < MAX_NUM_PHYSICAL_PLAYERS; ++i)
		{
			if (((m_PlayerAckedFlags & (1<<i)) == 0) && ((m_PlayerInScopeFlags & (1<<i)) != 0))
			{
				returnFlags |= (1<<i);
			}
		}

		return returnFlags;
	}

    //PURPOSE
    // Returns whether this event ID with the specified sequence has already been processed by the specified player
    //PARAMS
    // playerIndex - The player index of the player to check
    // sequence    - The sequence of the event ID to check
    bool AlreadyProcessed(const PhysicalPlayerIndex playerIndex, const unsigned sequence) const
    {
        // check if the old sequence number is greater or equal to the new sequence
        u32 oldSeq = static_cast<u32>(m_SeqsReceived[playerIndex]);
        u32 newSeq = static_cast<u32>(sequence);

        // This code handles sequence number wrapping by shifting the sequence component up into
        // the most significant bits of a 32-bit integer and casting it to a value of this type. This causes
        // the most significant bit of the sequence component to be used as the sign bit, which provides
        // a floating window of half the range of the sequence for sequence comparisons. As an example of this,
        // an old sequence number of 255 (the maximum value for the sequence) will be determined as less than
        // a new sequence number in the range (0-127)
        return (static_cast<s32>((newSeq<<(32-SIZEOF_EVENT_SEQUENCE)) - (oldSeq<<(32-SIZEOF_EVENT_SEQUENCE))) <= 0);
    }

    //PURPOSE
    // Called when an event arrives using this id: updates the sequence number for this event id or rejects the event by
    // returning false
    //PARAMS
    // playerIndex - The player index of the player to mark the event ID as processed for
    // sequence    - The new sequence number for this event for the specified player
    void MarkProcessed(const PhysicalPlayerIndex playerIndex, const unsigned sequence)
    {
        Assert(!AlreadyProcessed(playerIndex, sequence));

        m_SeqsReceived[playerIndex] = sequence;
    }

    //PURPOSE
    // Returns whether the event using this ID is waiting for acknowledgement from other players in the session
    bool IsWaitingForAck() const
    {
		for (u32 i = 0; i < MAX_NUM_PHYSICAL_PLAYERS; ++i)
		{
			if ((m_PlayerAckedFlags & (1<<i)) == 0 && (m_PlayerInScopeFlags & (1<<i)) != 0)
			{
				return true;
			}
		}

		return false;
    }

    //PURPOSE
    // Clears the flags used to indicate the event using this ID is waiting for acknowledgement from other players in the session
    void ClearPlayersInScope()
    {
       m_PlayerInScopeFlags = 0;
    }

	//PURPOSE
	// Return a copy of the player in scope flags
	PlayerFlags GetPlayerInScopeFlags() { return m_PlayerInScopeFlags; }

	//PURPOSE
	// Returns true if the player has already acked for this event
	bool IsAckedByPlayer(const PhysicalPlayerIndex playerIndex) const { return (m_PlayerAckedFlags & (1<<playerIndex)) != 0; }

    //PURPOSE
    // Clears the sequence numbers sent/received for the specified player for this event ID
    //PARAMS
    // playerIndex - The player index of the sequences to clear
    void ClearPlayerSequences(const PhysicalPlayerIndex playerIndex)
    {
        m_SeqsSent[playerIndex] = 0;
        m_SeqsReceived[playerIndex] = 0;
    }

    //PURPOSE
    // Returns whether the specified event ID is equivalent to this event ID
    //PARAMS
    // eventID - The event ID to compare to this one
    bool operator==(const EventId eventID) const
    {
        return (eventID == m_ID);
    }

private:

    EventId                                          m_ID;           // The numeric ID
    PlayerFlags                                      m_PlayerInScopeFlags;  // Flags indicating whether this id is waiting to be acknowedged by each player
    PlayerFlags                                      m_PlayerAckedFlags;  // Flags indicating which players have acked the event
	atFixedArray<unsigned, MAX_NUM_PHYSICAL_PLAYERS> m_SeqsSent;     // The last sequence number sent for this ID to each player
    atFixedArray<unsigned, MAX_NUM_PHYSICAL_PLAYERS> m_SeqsReceived; // The most recent sequence numbers received for this event id from each player
    bool                                             m_InUse;        // Set when the id is being used
};

} // namespace rage

#endif  // NETWORK_EVENTID_H
