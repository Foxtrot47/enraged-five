//

// --- Include Files ------------------------------------------------------------

#include "fwnet/netserialisers.h"

#include "fwnet/netinterface.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{
//============================================================================================
// netSerialiserUtils
//============================================================================================
netSerialiserUtils::fnAdjustWorldExtents netSerialiserUtils::m_AdjustWorldExtentsCallback = 0;
netSerialiserUtils::fnGetRealMapCoords   netSerialiserUtils::m_GetRealMapCoordsCallback   = 0;
netSerialiserUtils::fnAdjustMapExtents   netSerialiserUtils::m_AdjustMapExtentsCallback   = 0;

void netSerialiserUtils::GetAdjustedWorldExtents(Vector3& min, Vector3& max)
{
    if(m_AdjustWorldExtentsCallback)
    {
        (*m_AdjustWorldExtentsCallback)(min, max);
    }
}

void netSerialiserUtils::GetRealMapCoords(const Vector3& adjustedPos, Vector3& realPos)
{
    if(m_GetRealMapCoordsCallback)
    {
        (*m_GetRealMapCoordsCallback)(adjustedPos, realPos);
    }
}

void netSerialiserUtils::GetAdjustedMapCoords(const Vector3& realPos, Vector3& adjustedPos)
{
    if(m_AdjustMapExtentsCallback)
    {
        (*m_AdjustMapExtentsCallback)(realPos, adjustedPos);
    }
}

//============================================================================================
// CSyncDataBase
//============================================================================================
u32 CSyncDataBase::GetSize()
{
	return 0;
}

bool CSyncDataBase::GetIsMaximumSizeSerialiser()
{
	return false;
}

// Bool
void CSyncDataBase::SerialiseBool(bool& value, const char* name /*= NULL*/)
{
	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%s", value ? "true" : "false");
	}
}

// Integer
void CSyncDataBase::SerialiseInteger(s8& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
	ASSERT_ONLY(s8 maxVal = static_cast<s8>(((1<<(numBits-1)))-1));
	Assertf(value >= -maxVal && value <= maxVal, "%s is trying to serialise an int (%d) exceeding its permitted range (%d - %d)", name ? name : "Unnamed", static_cast<int>(value), static_cast<int>(-maxVal), static_cast<int>(maxVal));

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%d", value);
	}
}

void CSyncDataBase::SerialiseInteger(s16& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
	ASSERT_ONLY(s16 maxVal = static_cast<s16>(((1<<(numBits-1)))-1));
	Assertf(value >= -maxVal && value <= maxVal, "%s is trying to serialise an int (%d) exceeding its permitted range (%d - %d)", name ? name : "Unnamed", static_cast<int>(value), static_cast<int>(-maxVal), static_cast<int>(maxVal));

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%d", value);
	}
}

void CSyncDataBase::SerialiseInteger(s32& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
	ASSERT_ONLY(s32 maxVal = static_cast<s32>(((1<<(numBits-1)))-1));
	Assertf(value >= -maxVal && value <= maxVal, "%s is trying to serialise an int (%d) exceeding its permitted range (%d - %d)", name ? name : "Unnamed", static_cast<int>(value), static_cast<int>(-maxVal), static_cast<int>(maxVal));

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%d", value);
	}
}

void CSyncDataBase::SerialiseInteger(s64& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
#if __ASSERT
    if(numBits < 64)
    {
	    s64 maxVal = numBits; 
	    const s64 theNumber1 = 1;  //1I64 doesn't work on PS3 apparently...
	    maxVal = static_cast<s64>((((theNumber1)<<(maxVal-1)))-1);
	    Assertf(value >= -maxVal && value <= maxVal, "%s is trying to serialise an s64 exceeding its permitted range", name ? name : "Unnamed");
    }
#endif

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%d", value);
	}
}

// Unsigned
void CSyncDataBase::SerialiseUnsigned(u8& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
	ASSERT_ONLY(u8 maxVal = static_cast<u8>(((u64)1u<<numBits)-1));
	Assertf( value <= maxVal, "%s is trying to serialise an unsigned int (%u) exceeding its permitted maximum value (%u)", name ? name : "Unnamed", static_cast<int>(value), static_cast<int>(maxVal));

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%u", value);
	}
}

void CSyncDataBase::SerialiseUnsigned(u16& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
	ASSERT_ONLY(u16 maxVal = static_cast<u16>(((u64)1u<<numBits)-1));
	Assertf( value <= maxVal, "%s is trying to serialise an unsigned int (%u) exceeding its permitted maximum value (%u)", name ? name : "Unnamed", static_cast<int>(value), static_cast<int>(maxVal));
	
	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%u", value);
	}
}

void CSyncDataBase::SerialiseUnsigned(u32& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
	ASSERT_ONLY(u32 maxVal = static_cast<u32>(((u64)1u<<numBits)-1));
	Assertf( value <= maxVal, "%s is trying to serialise an unsigned int (%u) exceeding its permitted maximum value (%u)", name ? name : "Unnamed", static_cast<int>(value), static_cast<int>(maxVal));

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%u", value);
	}
}

void CSyncDataBase::SerialiseUnsigned(u64& value, const int ASSERT_ONLY(numBits), const char* name /*= NULL*/)
{
#if __ASSERT
    if(numBits < 64)
    {
        u64 maxVal = numBits;
        const u64 theNumber1 = 1;  //1I64 doesn't work on PS3 apparently...
        maxVal = static_cast<u64>((((theNumber1)<<(maxVal)))-1);
        Assertf(value <= maxVal, "%s is trying to serialise an u64 exceeding its permitted range", name ? name : "Unnamed");
    }
#endif

    if (m_log && name)
    {
        m_log->WriteDataValue(name, "%u", value);
    }
}

// Float
void CSyncDataBase::SerialisePackedFloat(float& value, const float ASSERT_ONLY(maxValue), const int UNUSED_PARAM(numBits), const char* name /*= NULL*/)
{
	Assertf(value >= -maxValue && value <= maxValue, "%s is trying to serialise a float (%f) exceeding its permitted range (%f - %f)", name ? name : "Unnamed", value, -maxValue, maxValue);

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%.2f", value);
	}
}

void CSyncDataBase::SerialisePackedUnsignedFloat(float& value, const float ASSERT_ONLY(maxValue), const int UNUSED_PARAM(numBits), const char* name /*= NULL*/)
{
	Assertf(value >= 0.0f && value <= maxValue, "%s is trying to serialise an unsigned float (%f) exceeding its permitted maximum value (%f)", name ? name : "Unnamed", value, maxValue);

	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%.2f", value);
	}
}

void CSyncDataBase::SerialiseObjectID(ObjectId& objectID, const char* name /*= NULL*/)
{
	if (m_log && name)
	{
		char logName[256];
		netInterface::GetObjectManager().GetLogName(logName, sizeof(logName), objectID);
		m_log->WriteDataValue(name, logName);
	}
}

void CSyncDataBase::SerialisePosition(Vector3& position, const char* name /*= NULL*/, u32 UNUSED_PARAM(sizeOfPosition))
{
	if (m_log && name)
	{
		m_log->WriteDataValue(name, "%f, %f, %f", position.x, position.y, position.z);
	}
}

void CSyncDataBase::SerialiseOrientation(Matrix34& orientation, const char* name /*= NULL*/)
{
	if (m_log && name)
	{
		const Vector3 eulers = orientation.GetEulers();
		m_log->WriteDataValue(name, "%f, %f, %f", eulers.x, eulers.y, eulers.z);
	}
}

// Vector
void CSyncDataBase::SerialiseVector(Vector3& vec, const float UNUSED_PARAM(maxLen), const int UNUSED_PARAM(numBits), const char* name /*= NULL*/)
{
	if(m_log && name)
	{
		m_log->WriteDataValue(name, "%f, %f, %f", vec.x, vec.y, vec.z);
	}
}

// Quaternion
void CSyncDataBase::SerialiseQuaternion(Quaternion& quat, const int UNUSED_PARAM(numBits), const char* name /*= NULL*/)
{
	if(m_log && name)
	{
		m_log->WriteDataValue(name, "%f, %f, %f, %f", quat.x, quat.y, quat.z, quat.w);
	}
}

// Data
void CSyncDataBase::SerialiseDataBlock(u8* UNUSED_PARAM(dataBlock), const int UNUSED_PARAM(numBits), const char* UNUSED_PARAM(name))
{
}

void CSyncDataBase::SerialiseString(char* string, const u32 UNUSED_PARAM(maxChars), const char* name /*= NULL*/)
{
    if (m_log && name && string)
    {
        m_log->WriteDataValue(name, string);
    }
}

//============================================================================================
// CSyncDataBase
//============================================================================================

} // namespace rage
