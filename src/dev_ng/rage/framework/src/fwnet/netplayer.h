//
// netplayer.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef PLAYER_H
#define PLAYER_H

// rage headers
#include "atl/inlist.h"
#include "net/connection.h"
#include "net/net.h"
#include "net/netaddress.h"

#include "fwnet/nettypes.h"

namespace rage
{
    class datBitBuffer;
    class netBot;
    class netLoggingInterface;
    class netPlayerMgrBase;
    class rlGamerId;
    class rlGamerInfo;
    class rlPeerInfo;

//PURPOSE
// The nonPhysicalPlayerDataBase class is used for storing and serialising application
// specific data about non physical players and sending this data to/from other players
// in the session.

// This information can be used to determine when to switch players between physical and
// non-physical.
class nonPhysicalPlayerDataBase
{
protected:

    nonPhysicalPlayerDataBase() {;}

public:

    virtual ~nonPhysicalPlayerDataBase() {;}

    virtual void Read(const datBitBuffer &messageBuffer) = 0;
    virtual void Write(datBitBuffer &messageBuffer) = 0;
    virtual unsigned GetSize() = 0;
    virtual void WriteToLogFile(netLoggingInterface &log) = 0;
};

//PURPOSE
// This class encapsulates and manages data associated with network players in a network game
class netPlayer
{
    friend class netPlayerMgrBase;

public:

    enum PlayerType
    {
        NETWORK_PLAYER,
        NETWORK_BOT,
        MAX_PLAYER_TYPES
    };

	static const int VOICE_CHANNEL_NONE = -1;

    netPlayer();
    virtual ~netPlayer();

    //PURPOSE
    // Initialisation function for the local player
    //PARAMS
    // playerIndex - The player index of the local player
    // natType     - The nat type the local player is using
    void Init(const netNatType natType);

    //PURPOSE
    // Initialisation function for a remote player
    //PARAMS
	// gamerInfo	- GamerInfo of the remote player
    // connectionID - Connection ID used to communicate with this player
	// endpointId	- Endpoint ID used to communicate with this player
    // natType      - The nat type the local player is using
    void Init(const rlGamerInfo& gamerInfo,
			  const int connectionId,
			  const EndpointId endpointId,
              const netNatType natType);

    //PURPOSE
    // Shutdown function for network players
    virtual void Shutdown();

    //PURPOSE
    // Returns true if the network player is in use
    bool IsValid() const;

    //PURPOSE
    // Returns true if the network player is pending active
    bool IsPending() const;

    //PURPOSE
    // Returns true if the network player is active (i.e. has an open connection)
    bool IsActive() const;  

    //PURPOSE
    // Returns true if the network player is physical (i.e. has a physical presence in the game world)
    virtual bool IsPhysical() const;

    //PURPOSE
    // Returns true if the gamer is signed in.
    bool IsSignedIn() const;

    //PURPOSE
    // Returns true if the gamer is signed in.
    bool IsOnline() const;

    //PURPOSE
    // Returns true if the gamer is local.
    bool IsLocal() const;

    //PURPOSE
    // Returns true if the gamer is a bot.
    bool IsBot() const;

    //PURPOSE
    // Returns true if the gamer is remote.
    bool IsRemote() const;

	//PURPOSE
	// Returns true if the gamer is leaving the session.
	bool IsLeaving() const { return m_Leaving; }

	//PURPOSE
	// Called when the gamer is leaving the session.
	void SetLeaving();

    //PURPOSE
    // Returns the player type (player or bot)
    PlayerType GetPlayerType() const { return m_PlayerType; }

    //PURPOSE
    // Sets the player type (player or bot)
    //PARAMS
    // type - The type for the player
    void SetPlayerType(const PlayerType type) { m_PlayerType = type; }

    //PURPOSE
    // Returns the name of the player.
    virtual const char* GetLogName() const { return ""; }

	//PURPOSE
	// Returns the NAT type the player is using
	netNatType GetNatType() const;

    //PURPOSE
    // Returns the gamer id.
    const rlGamerId& GetRlGamerId() const;

    //PURPOSE
    // Returns the rline peer id.
    u64 GetRlPeerId() const;

    //PURPOSE
    // Returns the index of this player in the player manager's player pile.
    // **This must only be used locally, use the rlGamerId or physical player index when referring to a player over the network**
    ActivePlayerIndex GetActivePlayerIndex() const;

	//PURPOSE
	// Returns the index of this player in the player manager's physical player pile.
	// This index should be consistent across the network - all physical players in a session should agree on which slot a player occupies.
	PhysicalPlayerIndex GetPhysicalPlayerIndex() const { return m_PhysicalPlayerIndex; }
	bool HasValidPhysicalPlayerIndex() const { return (m_PhysicalPlayerIndex != INVALID_PLAYER_INDEX); }

	//PURPOSE
    // Returns the connection id of the remote machine the player exists on
    int GetConnectionId() const;

    //PURPOSE
    // Sets the connection id of the remote machine the player exists on
    //PARAMS
    // connectionID - the connection ID
    void SetConnectionId(int connectionID);

	//PURPOSE
	// Returns the endpoint id of the remote machine the player exists on
	EndpointId GetEndpointId() const;

	//PURPOSE
	// Sets the endpoint id of the remote machine the player exists on
	//PARAMS
	// endpointId - the endpoint ID
	void SetEndpointId(const EndpointId endpointId);

    //PURPOSE
    // Returns the index of this network player on the machine that is in control of it
    unsigned GetPeerPlayerIndex() const;

    //PURPOSE
    // Sets the index of this network player on the machine that is in control of it
    //PARAMS
    // peerPlayerIndex - The peer player index
    void SetPeerPlayerIndex(const unsigned peerPlayerIndex);

    //PURPOSE
    // Sets the non-physical player data associated with this network player
    //PARAMS
    // nonPhysicalData - The nonPhysicalData to set
    void SetNonPhysicalData(nonPhysicalPlayerDataBase *nonPhysicalData);

    //PURPOSE
    // Returns the non-physical player data associated with this network player
    nonPhysicalPlayerDataBase *GetNonPhysicalData() const;
 
	//PURPOSE
	//	Returns true if the player is a member of a team
	bool HasTeam();

	//PURPOSE
	//	Returns the player's team
	int GetTeam() const;

	//PURPOSE
	//	Sets the player's team
	void SetTeam(int team);

#if ENABLE_NETWORK_BOTS
    //PURPOSE
    // Returns the network bot associated with this player. This can only be
    // called on network players representing network bots (check IsBot() first)
    // before calling this
    netBot *GetBot() const;

    //PURPOSE
    // Sets the network bot associated with this player. This can only be
    // called on network players representing network bots (check IsBot() first)
    // before calling this
    //PARAMS
    // bot - The network bot to associate with the player
    void SetBot(netBot *bot);
#endif // ENABLE_NETWORK_BOTS

    //PURPOSE
    // Updates the cached flag for whether the player is active, called once per frame. This
    // is necessary as the active state is determined based on whether the connection for the
    // player is open. Connections can be closed by the connection manager thread which can cause
    // issues depending where the main thread is when this occurs
    void UpdateIsActiveState();

	//PURPOSE
	// Update function called once per frame only when the player is active.
	virtual void ActiveUpdate() { }

	//PURPOSE
	// Returns the next player in the pending players list
	static netPlayer* NextPendingPlayer(netPlayer*);
	static const netPlayer* NextPendingPlayer(const netPlayer*);

	//PURPOSE
    // Returns the next player in the existing players list
    static netPlayer* NextExistingPlayer(netPlayer*);
    static const netPlayer* NextExistingPlayer(const netPlayer*);

    //PURPOSE
    // Returns the next player in the active players list
    static netPlayer* NextActivePlayer(netPlayer*);
    static const netPlayer* NextActivePlayer(const netPlayer*);

    //PURPOSE
    // Returns the next player in the physical players list
    static netPlayer* NextPhysicalPlayer(netPlayer*);
    static const netPlayer* NextPhysicalPlayer(const netPlayer*);

    //PURPOSE
    // Returns true if this player is the session host. If you game supports
    // a concept of a session host you must implement this function
    virtual bool IsHost() const { return false; }

    //PURPOSE
    // Returns true if this player represents players local to this machine
    bool IsMyPlayer() const;

    //PURPOSE
    // Returns the gamer info for this player
    virtual const rlGamerInfo& GetGamerInfo() const = 0;

    //PURPOSE
    // Returns the peer info for this player
    const rlPeerInfo& GetPeerInfo() const;

	//PURPOSE
	// Sets the voice channel for this player
	void SetVoiceChannel(int nVoiceChannel);
	
	//PURPOSE
	// Clears the voice channel for this player - utility function 
	void ClearVoiceChannel();
	
	//PURPOSE
	// Returns the voice channel for this player
	int GetVoiceChannel() const;

    //PURPOSE
    // Adds a kick vote for this player from the specified player
    void AddKickVote(const netPlayer& player);

    //PURPOSE
    // Removes a kick vote for this player from the specified player
    void RemoveKickVote(const netPlayer& player);

    //PURPOSE
    // Remove all kick votes for this player
    void RemoveAllKickVotes();

    //PURPOSE
    // Returns the current number of kick votes for this player
    unsigned GetNumKickVotes() const;

	//PURPOSE
	// Returns the current votes for this player
	PlayerFlags GetKickVotes() const { return m_KickVotes; }

	//PURPOSE
	// Sets and counts up the kick votes
	void SetKickVotes(PlayerFlags kickVotes);

	//PURPOSE
	// Returns the current votes cast by the local player.
	static PlayerFlags  GetLocalPlayerKickVotes() { return sm_LocalPlayerKickVotes; }

	//PURPOSE
	// Adds a kick vote for this player from the local player.
	static void  AddLocalKickVote(const netPlayer& player);

	//PURPOSE
	// Removes a kick vote for this player from the local player.
	static void  RemoveLocalKickVote(const netPlayer& player);

	// returns the total size of all locally arbitrated network array data for this player
	u32 GetSizeOfNetArrayData() const { return m_sizeOfNetArrayData; }

	void SetSizeOfNetArrayData(unsigned size) { m_sizeOfNetArrayData = size; }

	// returns the length of time in milliseconds this player has been in the session
	unsigned GetTimeInSession() const;

protected:

	//PURPOSE
	// Sets the active player index for the player. Should only be called from the player manager.
	//PARAMS
	// playerIndex - The player index
	void SetActivePlayerIndex(ActivePlayerIndex playerIndex);

	//PURPOSE
	// Sets the physical player index for the player. Should only be called from the player manager.
	//PARAMS
	// playerIndex - The player index
	void SetPhysicalPlayerIndex(PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Sets the player is local flag
    //PARAMS
    // local - indicates whether the player is local
    void SetIsLocal(bool local);

private:

    //PURPOSE
    // Internal shutdown function for the class
    void ShutdownPrivate();

#if ENABLE_NETWORK_BOTS
    //PURPOSE
    // Returns true if the gamer is an active bot. Should only be called on bots.
    bool IsBotActive() const;
#endif // ENABLE_NETWORK_BOTS

    // The type of player
    PlayerType m_PlayerType;

    // The non physical player data for this class
    nonPhysicalPlayerDataBase *m_NonPhysicalPlayerData;

    // The connection id of the remote machine the player exists on
    int m_CxnId;

	// The endpoint id of the remote machine the player exists on
	EndpointId m_EndpointId;

 	// The index of this network player in the player manager's player pile. ** This is not consistent across the network - i.e. a player will not have 
	// the same active player index on different machines! **
    ActivePlayerIndex m_ActivePlayerIndex;

	// The index of this network player in the player manager's physical player array. This index is consistent across different machines and is
	// determined out-with the player manager - by the session host or roaming bubble owner, for example. It possibly has a smaller range than the
	// active player index and is used by other areas of the network code (eg when setting a bit for the player in some player flags).
	PhysicalPlayerIndex m_PhysicalPlayerIndex;

	// The NAT type this player is using
    netNatType m_NatType;

    // The index of this network player on the machine that is in control of it
    unsigned m_PeerPlayerIndex;

	// The time the player was added (the synced network time)
	u32 m_TimeAdded;

	// the player's team
	int m_Team;

    // List nodes for the different player lists
    // (inactive, active, physical).
    inlist_node<netPlayer> m_ListLink;
	inlist_node<netPlayer> m_PendingListLink;
	inlist_node<netPlayer> m_ActiveListLink;
	inlist_node<netPlayer> m_NonPhysListLink;
	inlist_node<netPlayer> m_PhysListLink;

	// The voice channel for this player. -1 for no channel
	// Players assigned a channel can only talk with each other
	int m_VoiceChannel; 

	// the total size of all net array handler data arbitrated by this player
	unsigned m_sizeOfNetArrayData;

    // Flag indicating whether the player is local
    bool m_Local : 1;

    // Flag indicating whether the player is active (updated once per frame)
    bool m_Active : 1;

    // Flag indicating whether the voice flags are dirty
    bool m_VoiceFlagsDirty  : 1;

	// set when the player is leaving the session
	bool m_Leaving  : 1;

    // Kick votes received for this player
    PlayerFlags m_KickVotes;
    unsigned m_NumKickVotes;

	// Kick votes local Player has cast for other players.
	static PlayerFlags  sm_LocalPlayerKickVotes;

#if ENABLE_NETWORK_BOTS
    // The network bot associated with this player, if it is representing a bot
    netBot *m_Bot;
#endif // ENABLE_NETWORK_BOTS
};

} // namespace rage

#endif  // PLAYER_H
