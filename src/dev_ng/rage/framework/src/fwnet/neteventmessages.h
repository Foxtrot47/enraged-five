//
// neteventmessages.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//


#ifndef NETWORKEVENTMESSAGES_H
#define NETWORKEVENTMESSAGES_H

#include "net/message.h"

#include "fwnet/netgameevent.h"
#include "fwnet/neteventid.h"

namespace rage
{

//PURPOSE
// This structure is used to pack data for multiple events together for sending over the network.
// This is used to reduce the cost of netMessage and UDP headers as a bandwidth optimisation
struct PackedEventData
{
    PackedEventData() :
    m_MessageCount(0),
    m_MessageBuffer()
    {
        m_MessageBuffer.SetReadWriteBits(m_MessageStorage, MAX_MESSAGE_PAYLOAD_BITS, 0);
    }

    //PURPOSE
    // Resets the contents of the packed data for reuse
    void Reset()
    {
        m_MessageCount = 0;
        m_MessageBuffer.SetCursorPos(0);
    }

    //PURPOSE
    // Serialises the contents of the specified import buffer into the packed data
    //PARAMS
    // bb - The bit buffer to serialise the data from
    void Serialise(datImportBuffer &bb)
    {
        unsigned bufferSize = 0;

        bb.SerUns(m_MessageCount, SIZEOF_MESSAGE_COUNT);
        bb.SerUns(bufferSize, SIZEOF_BUFFER_SIZE);

        netAssertf(bufferSize <= static_cast<unsigned>(m_MessageBuffer.GetMaxBits()), "bufferSize[%u] overflow (value in bits)", bufferSize);
        bufferSize = rage::Min(bufferSize, static_cast<unsigned>(m_MessageBuffer.GetMaxBits()));

        u8 *buffer = static_cast<u8 *>(m_MessageBuffer.GetReadWriteBits());
        bb.SerBits(buffer, bufferSize);
        m_MessageBuffer.SetReadOnlyBits(buffer, bufferSize, 0);
    }

    //PURPOSE
    // Serialises the contents of the packed data to the specified import buffer
    //PARAMS
    // bb - The bit buffer to serialise the data to
    void Serialise(datExportBuffer &bb) const
    {
        Assert(m_MessageCount > 0);

        const u8 *buffer     = static_cast<const u8 *>(m_MessageBuffer.GetReadWriteBits());
        const unsigned bufferSize = m_MessageBuffer.GetCursorPos();

        bb.SerUns(m_MessageCount, SIZEOF_MESSAGE_COUNT);
        bb.SerUns(bufferSize, SIZEOF_BUFFER_SIZE);
        bb.SerBits(buffer, bufferSize);
    }

    //PURPOSE
    // Returns the number of bits used by the packed data
	int GetMessageDataBitSize() const
	{
		return SIZEOF_MESSAGE_COUNT + SIZEOF_BUFFER_SIZE + m_MessageBuffer.GetCursorPos();
	}

	static const unsigned int SIZEOF_MESSAGE_COUNT  = 5;  // The number of bits to use when serialising the message count
    static const unsigned int SIZEOF_BUFFER_SIZE    = 15; // The number of bits to use when serialising the buffer

    unsigned       m_MessageCount;  // The number of packed events stored
    datBitBuffer   m_MessageBuffer; // The message buffer used to store the packed event data
    u8             m_MessageStorage[MAX_MESSAGE_PAYLOAD_BYTES]; // Storage for the message buffer
};

//PURPOSE
// This class represents a network message for sending multiple packed game event messages over the network
class CMsgPackedEvents
{
public:

    NET_MESSAGE_DECL(CMsgPackedEvents, CMSG_PACKED_EVENTS);

    CMsgPackedEvents();

    const netPlayer *GetOwnerPlayer() const { return m_OwnerPlayer;}

    //PURPOSE
    // Serialises the contents of the specified message to the specified bit buffer
    //PARAMS
    // bb -  The bit buffer to serialise the message data from/to
    // msg - The message to serialise
    NET_MESSAGE_SER(bb, msg)
    {
        msg.m_Events.Serialise(bb);
        return true;
    }

    //PURPOSE
    // Adds a game event to the packed events data
    //PARAMS
    // fromPlayer        - The player sending the event
    // eventType         - The type of event to pack
    // eventID           - The event ID used for the event to pack
    // eventIDSequence   - The sequence of the event ID
    // isSequential      - Indicates whether this is a sequential event or not (see netevent.h for details)
    // eventTypeSequence - The sequence number for the event type
    // eventData     - The event data to pack
    // eventDataSize - The size of the event data to pack
    bool AddEvent(const netPlayer       &fromPlayer,
                  const NetworkEventType eventType,
                  const EventId          eventID,
                  const unsigned         eventIDSequence,
                  bool                   isSequential,
                  netSequence            eventSequence,
                  u8                    *eventData,
                  const unsigned         eventDataSize);

    //PURPOSE
    // Returns the number of bits required to pack an event with the specified amount of event data
    //PARAMS
    // bufferSize   - Size of the event data for the event to pack
    // isSequential - Indicates whether this is a sequential event or not (see netevent.h for details)
    static unsigned GetEventSize(const unsigned bufferSize, bool isSequential);

    //PURPOSE
    // Returns the number of bits required to pack the message header
    unsigned GetMessageHeaderBitSize() const;

    //PURPOSE
    // Returns the number of game events currently packed
    unsigned GetTotalPackedEventCount() const;

    //PURPOSE
    // Returns the total size of the packed event data (excluding header)
	unsigned GetTotalSizeOfMsg() const;

    //PURPOSE
    // Resets the contents of this message for reuse
    void Reset();

    //PURPOSE
    // Returns the total size of the packed event data (including header data)
	int GetMessageDataBitSize() const
	{
		return m_Events.GetMessageDataBitSize();
	}

private:

    //PURPOSE
    // Returns whether there is enough space to pack the specified number of bits
    //PARAMS
    // numRequiredBits - The number of bits to check
    bool IsSpaceAvailable(unsigned numRequiredBits) const;

public:

    // The maximum number of game events that can be packed in this message
    static const unsigned int MAX_EVENTS_TO_PACK = (1<<PackedEventData::SIZEOF_MESSAGE_COUNT);

    const netPlayer *m_OwnerPlayer;
    PackedEventData  m_Events; // The packed events data
};

//PURPOSE
// This class represents a network message for sending multiple packed game event ACK/Reply messages over the network
class CMsgPackedEventReliablesMsgs
{
public:

    NET_MESSAGE_DECL(CMsgPackedEventReliablesMsgs, CMSG_PACKED_EVENT_RELIABLES_MSGS);

    CMsgPackedEventReliablesMsgs();

    const netPlayer *GetOwnerPlayer() const { return m_OwnerPlayer;}

    //PURPOSE
    // Serialise the contents of the specified message to/from the specified bit buffer
    //PARAMS
    // bb -  The bit buffer to serialise the message data from/to
    // msg - The message to serialise
    NET_MESSAGE_SER(bb, msg)
    {
        unsigned bitFlags = msg.GetBitFlags();
        bb.SerUns(bitFlags, SIZEOF_BITFLAGS);

        if(bitFlags & (1<<EVENT_ACK_MSG))
        {
            msg.m_EventAcks.Serialise(bb);
        }

        if(bitFlags & (1<<EVENT_REPLY_MSG))
        {
            msg.m_EventReplies.Serialise(bb);
        }

        return true;
    }

    //PURPOSE
    // Returns the size of this message's data in bits
	int GetMessageDataBitSize() const
	{
		unsigned bitFlags = GetBitFlags();
		
		int size = SIZEOF_BITFLAGS;

		if(bitFlags & (1<<EVENT_ACK_MSG))
		{
			size += m_EventAcks.GetMessageDataBitSize();
		}

		if(bitFlags & (1<<EVENT_REPLY_MSG))
		{
			size += m_EventReplies.GetMessageDataBitSize();
		}

		return size;
	}

    //PURPOSE
    // Adds an event ACK to the event with the specified ID
    //PARAMS
    // fromPlayer      - The player sending the event
    // eventID         - The event ID of the event being ACKed
    // eventIDSequence - The sequence of the event ID being ACKed
    bool AddEventAck(const netPlayer &fromPlayer, const EventId eventID, const unsigned eventIDSequence);

    //PURPOSE
    // Adds an event reply and the associated data
    //PARAMS
    // fromPlayer         - The player sending the event
    // eventID            - The event ID of the event to reply to
    // eventIDSequence    - The sequence of the event ID to reply to
    // eventReplyData     - The event reply data
    // eventReplyDataSize - The size of the event reply data
    bool AddEventReply(const netPlayer &fromPlayer, const EventId eventID, const unsigned eventIDSequence, u8 *eventReplyData, const unsigned eventReplyDataSize);

    //PURPOSE
    // Returns the number of bits required to pack an event ACK
    static unsigned GetEventAckSize();

    //PURPOSE
    // Returns the number of bits to pack an event reply
    //PARAMS
    // bufferSize - The size of the event reply data
    static unsigned GetEventReplySize(const unsigned bufferSize);

    //PURPOSE
    // Returns the size of the message's header in bits
    unsigned GetMessageHeaderBitSize() const;

    //PURPOSE
    // Returns the number of event ACK/Reply messages packed
    unsigned GetTotalPackedMessageCount() const;

    //PURPOSE
    // Resets the contents of this message for reuse
    void Reset();

private:

    //PURPOSE
    // Enum for the different message types that can be packed
    enum
    {
        EVENT_ACK_MSG,
        EVENT_REPLY_MSG,
        NUM_MSG_TYPES
    };

    //PURPOSE
    // Returns the bit flags for which message types have been packed
    unsigned GetBitFlags() const;

    //PURPOSE
    // Returns whether their is enough space to pack the specified number of bits
    //PARAMS
    // numRequiredBits - The number of bits to check
    bool IsSpaceAvailable(unsigned numRequiredBits) const;

public:

    static const unsigned int SIZEOF_BITFLAGS       = 2; // The number of bits required to pack the bit flags
    static const unsigned int MAX_MESSAGES_PER_TYPE = (1<<PackedEventData::SIZEOF_MESSAGE_COUNT); // The maximum number of messages that can be packed for each message type

    const netPlayer *m_OwnerPlayer;
    PackedEventData  m_EventAcks;    // Packed event acks
    PackedEventData  m_EventReplies; // Packed event replies
};

} // namespace rage

#endif  // NETWORKEVENTMESSAGES_H
