//
// netplayermgrbase.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/netplayermgrbase.h"

// rage headers
#include "net/connectionmanager.h"
#include "net/nethardware.h"
#include "system/timer.h"
#include "system/param.h"
#include "diag/channel.h"

// framework headers
#include "fwnet/netinterface.h"
#include "fwnet/netbandwidthmgr.h"
#include "fwnet/netbot.h"
#include "fwnet/netchannel.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netplayermessages.h"
#include "fwnet/netutils.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

PARAM(netSuppressGameTraffic, "[netPlayerMgrBase] If present, we won't send any game traffic");

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, playermgr, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_playermgr

netPlayerMgrBase::netPlayerMgrBase(netBandwidthMgr &bandwidthMgr)
: m_CxnMgr(0)
, m_BandwidthMgr(bandwidthMgr)
, m_ChannelId(NET_INVALID_CHANNEL_ID)
, m_MyPlayer(NULL)
, m_Log(0)
, m_IsInitialized(false)
, m_IsShuttingDown(false)
, m_PhysicalPlayersArray(MAX_NUM_PHYSICAL_PLAYERS)
, m_RemotePhysicalPlayersInSession(0)
, m_NumPendingPlayers(0)
, m_NumAllActivePlayers(0)
, m_NumRemoteActivePlayers(0)
, m_NumAllPhysicalPlayers(0)
, m_NumLocalPhysicalPlayers(0)
, m_NumRemotePhysicalPlayers(0)
#if __BANK
, m_NumNonPhysicalUpdateMessagesSent(0)
, m_NumMiscReliableMessagesSent(0)
, m_NumMiscUnreliableMessagesSent(0)
#endif
{
    m_CxnMgrDlgt.Bind(this, &netPlayerMgrBase::OnNetEvent);
    m_TxRqstHandler.Bind(this, &netPlayerMgrBase::OnTxRqst);

#if __BANK
    m_BandwidthRecorderId = INVALID_RECORDER_ID;
#endif
}

netPlayerMgrBase::~netPlayerMgrBase()
{
    ShutdownPrivate();
}

bool netPlayerMgrBase::Init(netConnectionManager* cxnMgr,
                            const unsigned channelId,
                            PhysicalOnRemoteCallback physicalOnRemoteCallback)
{
    gnetAssert(physicalOnRemoteCallback.IsBound());

    bool success = false;

    if(gnetVerify(!m_IsInitialized))
    {
		static bool s_HasCreatedLog = false; 
		m_Log = rage_new netLog("PlayerManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                     netInterface::GetDefaultLogFileBlockingMode());
		s_HasCreatedLog = true; 
		
        m_IsInitialized = true;
        m_CxnMgr = cxnMgr;
        m_ChannelId = channelId;
        m_PhysicalOnRemoteMachine = physicalOnRemoteCallback;

        m_Transactor.Init(cxnMgr);
        m_Transactor.AddRequestHandler(&m_TxRqstHandler, channelId);

        m_CxnMgr->AddChannelDelegate(&m_CxnMgrDlgt, channelId);

        gnetAssert(m_ExistingPlayers.empty());
        gnetAssert(m_PendingPlayers.empty());
        gnetAssert(m_ActivePlayers.empty());
        gnetAssert(m_NonPhysicalPlayers.empty());
        gnetAssert(m_PhysicalPlayers.empty());

        // assign a player to us
        m_MyPlayer = AllocatePlayer();

        m_MyPlayer->Init(netHardware::GetNatType());

        m_ExistingPlayers.push_back(m_MyPlayer);
        m_PendingPlayers.push_back(m_MyPlayer); // add to pending list so our player is activated like all the others, and CNetwork::PlayerHasJoinedSession is called

		for (unsigned i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			m_PhysicalPlayersArray[i] = 0;
		}

        m_RemotePhysicalPlayersInSession = 0;
        m_NumPendingPlayers              = 0;
        m_NumAllActivePlayers            = 0;
        m_NumRemoteActivePlayers         = 0;
        m_NumAllPhysicalPlayers          = 0;
        m_NumLocalPhysicalPlayers        = 0;
        m_NumRemotePhysicalPlayers       = 0;

        NetworkLogUtils::WriteLogEvent(GetLog(), "CREATING_MY_PLAYER", "");
        GetLog().WriteDataValue("Active player index", "%d", m_MyPlayer->GetActivePlayerIndex());

#if __BANK
        // Register bandwidth stats
        m_BandwidthMgr.RegisterStatistics(m_BandwidthStats, "Player Manager");

		m_BandwidthRecorderId = m_BandwidthStats.AllocateBandwidthRecorder("Player mgr messages");

        m_NumNonPhysicalUpdateMessagesSent = 0;
        m_NumMiscReliableMessagesSent      = 0;
        m_NumMiscUnreliableMessagesSent    = 0;
#endif

        success = true;
    }

    return success;
}

void netPlayerMgrBase::Shutdown()
{
    ShutdownPrivate();
}

bool
netPlayerMgrBase::IsInitialized() const
{
    return m_IsInitialized;
}

bool netPlayerMgrBase::IsShuttingDown() const
{
    return m_IsShuttingDown;
}

void netPlayerMgrBase::Update()
{
    if(m_IsInitialized)
    {
        m_NetTimeStep.SetTime(sysTimer::GetSystemMsTime());

        m_Transactor.Update(m_NetTimeStep.GetTimeStep());

        this->UpdatePlayerLists();

#if __ASSERT
		// verify that active players have not had their connection pulled from underneath
		ActivePlayerList::iterator actIt = m_ActivePlayers.begin();
		ActivePlayerList::const_iterator actStop = m_ActivePlayers.end();
		for(; actIt != actStop; ++actIt)
		{
			netPlayer* player = *actIt;
			if(player->GetConnectionId() >= 0)
			{
                gnetAssert(!m_CxnMgr->IsClosed(player->GetConnectionId()));
			}
		}
#endif

        SendNonPhysicalUpdates();
    }
}

netPlayer*
netPlayerMgrBase::AddPlayer(const rlGamerInfo& gamerInfo,
                            const EndpointId endpointId,
                            const playerDataMsg& playerData,
                            nonPhysicalPlayerDataBase *nonPhysicalPlayerData)
{
    netPlayer* pPlayer = NULL;

    if(gamerInfo.GetGamerId() != m_MyPlayer->GetRlGamerId())
    {
        pPlayer = this->AddRemotePlayer(gamerInfo, playerData, endpointId, nonPhysicalPlayerData);
    }
    else
    {
        this->AddLocalPlayer(gamerInfo, playerData, nonPhysicalPlayerData);

        pPlayer = m_MyPlayer;
    }

	// remove temporary player, if present
	RemoveTemporaryPlayer(gamerInfo);

    return pPlayer;
}

void
netPlayerMgrBase::RemovePlayer(const rlGamerInfo& gamerInfo)
{
    if(gnetVerify(m_MyPlayer) && m_MyPlayer->GetRlGamerId() != gamerInfo.GetGamerId())
    {
        netPlayer* pPlayer = this->GetPlayerFromGamerId(gamerInfo.GetGamerId(), false);

        if(!pPlayer)
        {
            //This case occurs when we've restarted the game to
            //transition to the next game mode or return to the
            //rendezvous session and players who are members of the
            //session are leaving and restarting their games to do the same.
            //Because we've restarted, we don't have references to
            //these players in the player mgr, but they are still members
            //of the underlying session.

			// Could be a temporary player
			if (GetTempPlayer(gamerInfo.GetGamerId()))
			{
				RemoveTemporaryPlayer(gamerInfo);
			}
        }
        else
        {
			pPlayer->SetLeaving();

            this->RemovePlayer(pPlayer);
        }
    }
}

netPlayer* 
netPlayerMgrBase::AddTemporaryPlayer(const rlGamerInfo         &gamerInfo,
									const EndpointId		endpointId,
									const playerDataMsg      &playerData,
									nonPhysicalPlayerDataBase *nonPhysicalPlayerData)
{
	netPlayer* newPlayer = AllocateTempPlayer();

	if(gnetVerify(newPlayer))
	{
        gnetAssert(NET_IS_VALID_ENDPOINT_ID(endpointId));

#if ENABLE_NETWORK_BOTS
		gnetAssert(!newPlayer->IsBot());
#endif // ENABLE_NETWORK_BOTS

		const int cxnId = m_CxnMgr->OpenConnection(endpointId, m_ChannelId, NULL, 0, NULL);

        gnetAssert(cxnId >= 0);

		newPlayer->Init(gamerInfo, cxnId, endpointId, playerData.m_NatType);		

		NetworkLogUtils::WriteLogEvent(GetLog(), "ADDING_TEMPORARY_PLAYER", "");
#if ENABLE_NETWORK_LOGGING
		const netAddress& addr = m_CxnMgr->GetAddress(newPlayer->GetEndpointId());
		const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ? addr.GetProxyAddress() : addr.GetTargetAddress();
		GetLog().WriteDataValue("IP Address", "%s", NET_ADDR_FOR_PRINTF(sockAddr));
#endif
		GetLog().WriteDataValue("Player Name", "%s", gamerInfo.GetName());
		GetLog().WriteDataValue("Active Player index", "%d",  newPlayer->GetActivePlayerIndex());

        gnetAssert(nonPhysicalPlayerData);
        gnetAssert(newPlayer->GetNonPhysicalData() == 0);
		newPlayer->SetNonPhysicalData(nonPhysicalPlayerData);
	}

	return newPlayer;
}

void 
netPlayerMgrBase::RemoveTemporaryPlayer(const rlGamerInfo& gamerInfo)
{
	netPlayer* pPlayer = GetTempPlayer(gamerInfo);
	if(pPlayer)
	{
		// We need to check the log here since we remove temporary players on shutdown and the log is no longer valid.
		if (m_Log != NULL)
		{
			NetworkLogUtils::WriteLogEvent(GetLog(), "REMOVING_TEMPORARY_PLAYER", "");

			if (!pPlayer->IsMyPlayer())
			{
	#if ENABLE_NETWORK_LOGGING
				const netAddress& addr = m_CxnMgr->GetAddress(pPlayer->GetEndpointId());
				const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ? addr.GetProxyAddress() : addr.GetTargetAddress();
				GetLog().WriteDataValue("IP Address", "%s", NET_ADDR_FOR_PRINTF(sockAddr));
	#endif
			}

			GetLog().WriteDataValue("Active Index", "%d", pPlayer->GetActivePlayerIndex());
			GetLog().WriteDataValue("Physical Index", "%d", pPlayer->GetPhysicalPlayerIndex());
			GetLog().WriteDataValue("Player Name", "%s",  pPlayer->GetLogName());
		}

		// close the connection (decrement ref count).
		if (m_CxnMgr)
		{
			m_CxnMgr->CloseConnection(pPlayer->GetConnectionId(), NET_CLOSE_GRACEFULLY);
		}

		// shut down the temporary player
		pPlayer->Shutdown();

		// deallocate the temporary player
		DeallocateTempPlayer(pPlayer);
	}
}

netPlayer *netPlayerMgrBase::GetPlayerFromPeerPlayerIndex(const unsigned NET_ASSERTS_ONLY(peerPlayerIndex), const rlPeerInfo &peerInfo)
{
    // the default implementation of this function does not support multiple players per peer
    gnetAssert(peerPlayerIndex == 0);
    return GetPlayerFromPeerId(peerInfo.GetPeerId());
}

void
netPlayerMgrBase::AddDelegate(NetworkPlayerEventDelegate* dlgt)
{
    m_NetEventDelegator.AddDelegate(dlgt);
}

void
netPlayerMgrBase::RemoveDelegate(NetworkPlayerEventDelegate* dlgt)
{
    m_NetEventDelegator.RemoveDelegate(dlgt);
}

bool netPlayerMgrBase::IsConnectionOpen(const netPlayer &player)
{
    return m_CxnMgr->IsOpen(player.GetConnectionId());
}

void netPlayerMgrBase::CloseConnection(netPlayer &player)
{
	if(IsConnectionOpen(player))
	{
		m_CxnMgr->CloseConnection(player.GetConnectionId(), NET_CLOSE_GRACEFULLY);
		player.SetConnectionId(INVALID_CONNECTION_ID);
	}
}

bool
netPlayerMgrBase::SendBuffer(const netPlayer* toPlayer,
                             const void* buffer,
                             const unsigned numBytes,
                             const unsigned sendFlags,
                             netSequence* seq,
                             const netPlayer *fromPlayer)
{
#if !__FINAL
	if(PARAM_netSuppressGameTraffic.Get())
	{
		return true;
	}
#endif

	bool success = false;

    // hook up the player sending the message if it has not been specified
    if(fromPlayer == 0)
    {
        fromPlayer = netInterface::GetLocalPlayer();
    }

    if(toPlayer && gnetVerifyf(toPlayer != fromPlayer, "A player is trying to send a message to itself!"))
    {
        if(toPlayer->IsLocal())
        {
            ReceivedMessageData messageData(buffer,
                                            numBytes,
                                            0,
                                            (netPlayer*)fromPlayer,
                                            (netPlayer*)toPlayer);

            m_NetEventDelegator.Dispatch(messageData);

			success = true;
        }
        else
        {
            const void *sendBuffer   = buffer;
            unsigned    sendNumBytes = numBytes;

#if ENABLE_NETWORK_BOTS

            u8 tempBuffer[netMessage::MAX_BYTE_SIZEOF_PAYLOAD + rage::netMessage::MAX_BYTE_SIZEOF_HEADER];

            if(SIZEOF_LOCAL_PLAYER_ID > 0)
            {
                datBitBuffer bitBuffer;
                bitBuffer.SetReadWriteBits(tempBuffer, (netMessage::MAX_BYTE_SIZEOF_PAYLOAD + rage::netMessage::MAX_BYTE_SIZEOF_HEADER)<<3, 0);
                bitBuffer.WriteUns(ABSTRACTION_LAYER_ID, sizeof(ABSTRACTION_LAYER_ID)<<3);
                bitBuffer.WriteUns(toPlayer->GetPeerPlayerIndex(), SIZEOF_LOCAL_PLAYER_ID);
                bitBuffer.WriteUns(fromPlayer->GetPeerPlayerIndex(), SIZEOF_LOCAL_PLAYER_ID);
                datBitBuffer::CopyBits(bitBuffer.GetReadWriteBits(), buffer, numBytes<<3, SIZEOF_NBAL_HEADER_IN_BITS, 0);
                bitBuffer.SetNumBitsWritten(SIZEOF_NBAL_HEADER_IN_BITS + (numBytes<<3));

                sendBuffer   = tempBuffer;
                sendNumBytes = bitBuffer.GetNumBytesWritten();
            }
#endif // ENABLE_NETWORK_BOTS

            gnetAssert(m_CxnMgr);
            const int cxnId = toPlayer->GetConnectionId(); 
			const bool isOpen = m_CxnMgr->IsOpen(cxnId) || m_CxnMgr->IsPendingOpen(cxnId);
            success = (cxnId >= 0 && isOpen && m_CxnMgr->Send(cxnId, sendBuffer, sendNumBytes, sendFlags, seq));
        }
    }

    // record bandwidth for message sent to local bots as well, this allows
    // us to use network bots for bandwidth testing purposes
    if(success)
    {
        m_BandwidthMgr.AddBandwidthOut(*toPlayer, numBytes<<3);
    }

    return success;
}

void
netPlayerMgrBase::AddRequestHandler(NetworkPlayerRequestHandler* rqstHandler)
{
    m_TxRqstDelegator.AddDelegate(rqstHandler);
}

void
netPlayerMgrBase::RemoveRequestHandler(NetworkPlayerRequestHandler* rqstHandler)
{
    m_TxRqstDelegator.RemoveDelegate(rqstHandler);
}

bool
netPlayerMgrBase::SendRequestBuffer(const netPlayer* player,
                                    const void* buffer,
                                    const unsigned numBytes,
                                    const unsigned timeout,
                                    netResponseHandler* handler)
{
    const int cxnId = player->GetConnectionId();
    const bool success =
        (cxnId >= 0
		&& m_CxnMgr->IsOpen(cxnId)
        && m_Transactor.SendRequest(cxnId, NULL, buffer, numBytes, timeout, handler));

    if(success)
    {
        m_BandwidthMgr.AddBandwidthOut(*player, numBytes);
    }

    return success;
}

bool
netPlayerMgrBase::SendResponseBuffer(const netTransactionInfo& txInfo,
                                    const void* buffer,
                                    const unsigned numBytes)
{
    return m_Transactor.SendResponse(txInfo, buffer, numBytes);
}

netPlayer* netPlayerMgrBase::GetPlayerFromConnectionId(const int cxnId)
{
    netPlayer* pPlayer = NULL;
    if(m_IsInitialized && cxnId >= 0)
    {
        PlayerList::iterator       existIt   = m_ExistingPlayers.begin();
	    PlayerList::const_iterator existStop = m_ExistingPlayers.end();
	    for(; existIt != existStop; ++existIt)
	    {
		    netPlayer* player = *existIt;

    		if(player->GetConnectionId() == cxnId)
            {
                pPlayer = player;
                break;
            }
	    }
    }

    return pPlayer;
}

const netPlayer* netPlayerMgrBase::GetPlayerFromConnectionId(const int cxnId) const
{
    return const_cast<netPlayerMgrBase*>(this)->GetPlayerFromConnectionId(cxnId);
}

netPlayer* netPlayerMgrBase::GetPlayerFromEndpointId(const EndpointId endpointId)
{
    netPlayer* pPlayer = NULL;
    if(m_IsInitialized && (NET_IS_VALID_ENDPOINT_ID(endpointId)))
    {
        PlayerList::iterator       existIt   = m_ExistingPlayers.begin();
	    PlayerList::const_iterator existStop = m_ExistingPlayers.end();
	    for(; existIt != existStop; ++existIt)
	    {
		    netPlayer* player = *existIt;

    		if(player->GetEndpointId() == endpointId)
            {
                pPlayer = player;
                break;
            }
	    }
    }

    return pPlayer;
}

const netPlayer* netPlayerMgrBase::GetPlayerFromEndpointId(const EndpointId endpointId) const
{
    return const_cast<netPlayerMgrBase*>(this)->GetPlayerFromEndpointId(endpointId);
}

netPlayer* netPlayerMgrBase::GetPlayerFromGamerId(const rlGamerId& gamerId, bool bAllowTempPlayer)
{
    netPlayer* pPlayer = NULL;

    if(m_IsInitialized)
    {
        pPlayer = this->GetMyPlayer();

        if(!pPlayer || pPlayer->GetRlGamerId() != gamerId)
        {
            pPlayer = NULL;

            PlayerList::iterator       existIt   = m_ExistingPlayers.begin();
	        PlayerList::const_iterator existStop = m_ExistingPlayers.end();
	        for(; existIt != existStop; ++existIt)
	        {
		        netPlayer* player = *existIt;
                if(player->GetRlGamerId() == gamerId)
                {
                    pPlayer = player;
                    break;
                }
            }
        }
    }

	// Maybe they are a temporary player
	if (!pPlayer && bAllowTempPlayer)
	{
		pPlayer = GetTempPlayer(gamerId);
	}

    return pPlayer;
}

const netPlayer* netPlayerMgrBase::GetPlayerFromGamerId(const rlGamerId& gamerId, bool bAllowTempPlayer) const
{
    return const_cast<netPlayerMgrBase*>(this)->GetPlayerFromGamerId(gamerId, bAllowTempPlayer);
}

netPlayer* netPlayerMgrBase::GetPlayerFromPeerId(const u64 peerId)
{
    netPlayer* pPlayer = NULL;

    if(m_IsInitialized)
    {
        pPlayer = this->GetMyPlayer();

        if(!pPlayer || pPlayer->GetRlPeerId() != peerId)
        {
            pPlayer = NULL;
            PlayerList::iterator       existIt   = m_ExistingPlayers.begin();
	        PlayerList::const_iterator existStop = m_ExistingPlayers.end();
	        for(; existIt != existStop; ++existIt)
	        {
		        netPlayer* player = *existIt;
                if(player->GetRlPeerId() == peerId)
                {
                    pPlayer = player;
                    break;
                }
            }
        }
    }

    return pPlayer;
}

const netPlayer* netPlayerMgrBase::GetPlayerFromPeerId(const u64 peerId) const
{
    return const_cast<netPlayerMgrBase*>(this)->GetPlayerFromPeerId(peerId);
}

const rlGamerId& netPlayerMgrBase::GetMyRlGamerId() const
{
    return GetMyPlayer()->GetRlGamerId();
}

ActivePlayerIndex netPlayerMgrBase::GetMyActivePlayerIndex() const
{
    return GetMyPlayer()->GetActivePlayerIndex();
}

#if ENABLE_NETWORK_BOTS

unsigned netPlayerMgrBase::GetNumTotalActiveBots() const
{
    unsigned count = 0;

    for(unsigned index = 0; index < m_NumAllActivePlayers; index++)
    {
        const netPlayer *player = m_AllActivePlayers[index];

        if(player && player->IsBot())
        {
            count++;
        }
    }

    return count;
}

unsigned netPlayerMgrBase::GetNumLocalActiveBots() const
{
    unsigned count = 0;

    for(unsigned index = 0; index < m_NumAllActivePlayers; index++)
    {
        const netPlayer *player = m_AllActivePlayers[index];

        if(player && player->IsBot() && player->IsLocal())
        {
            count++;
        }
    }

    return count;
}

unsigned netPlayerMgrBase::GetNumRemoteActiveBots() const
{
    unsigned count = 0;

    for(unsigned index = 0; index < m_NumAllActivePlayers; index++)
    {
        const netPlayer *player = m_AllActivePlayers[index];

        if(player && player->IsBot() && player->IsRemote())
        {
            count++;
        }
    }

    return count;
}

#endif // ENABLE_NETWORK_BOTS

void netPlayerMgrBase::SetPhysicalPlayerIndex(netPlayer& player, PhysicalPlayerIndex index)
{
	if (gnetVerify(index == INVALID_PLAYER_INDEX || index < MAX_NUM_PHYSICAL_PLAYERS))
	{
		PhysicalPlayerIndex previousIndex = player.m_PhysicalPlayerIndex;

		NetworkLogUtils::WriteLogEvent(GetLog(), "SETTING_PHYSICAL_INDEX", "");
		GetLog().WriteDataValue("Player Name", "%s", player.GetLogName());
		GetLog().WriteDataValue("Active player index", "%d", player.GetActivePlayerIndex());
		GetLog().WriteDataValue("Previous physical index", "%d", previousIndex);
		GetLog().WriteDataValue("New physical index", "%d", index);

		if (previousIndex != index && 
			previousIndex != INVALID_PLAYER_INDEX && 
            gnetVerify(previousIndex < MAX_NUM_PHYSICAL_PLAYERS))
		{
			m_PhysicalPlayersArray[previousIndex] = 0;
		}

		if (index != INVALID_PLAYER_INDEX)
		{
			netPlayer* existingPlayer = m_PhysicalPlayersArray[index];

			if (existingPlayer && existingPlayer != &player)
			{
				existingPlayer->m_PhysicalPlayerIndex = INVALID_PLAYER_INDEX;

				NetworkLogUtils::WriteLogEvent(GetLog(), "SETTING_PHYSICAL_INDEX", "");
				GetLog().WriteDataValue("Player Name", "%s", existingPlayer->GetLogName());
				GetLog().WriteDataValue("Active player index", "%d", existingPlayer->GetActivePlayerIndex());
				GetLog().WriteDataValue("Physical player index", "%d", INVALID_PLAYER_INDEX);

				// update the existing player in the player lists
				UpdatePlayerListsForPlayer(existingPlayer);
			}

			m_PhysicalPlayersArray[index] = &player;
		}

		player.m_PhysicalPlayerIndex = index;

		// this player may now be able to be added to the physical players list
		UpdatePlayerListsForPlayer(&player);
	}
}

void netPlayerMgrBase::ShutdownPrivate()
{
    if(m_IsInitialized)
    {
        m_IsShuttingDown = true;

        m_PhysicalOnRemoteMachine.Unbind();

        m_Transactor.RemoveRequestHandler(&m_TxRqstHandler);
        m_CxnMgr->RemoveChannelDelegate(&m_CxnMgrDlgt);

        m_NetEventDelegator.Clear();
        m_TxRqstDelegator.Clear();

        // build a list of players to remove
        netPlayer *playersToRemove[MAX_NUM_PHYSICAL_PLAYERS];
        unsigned   numPlayersToRemove = 0;

        ActivePlayerList::iterator       actIt   = m_ActivePlayers.begin();
	    ActivePlayerList::const_iterator actStop = m_ActivePlayers.end();

	    for(; actIt != actStop; ++actIt)
	    {
            if(gnetVerifyf(numPlayersToRemove < MAX_NUM_PHYSICAL_PLAYERS, "Adding too many players to the removal array!"))
            {
                netPlayer *player = *actIt;

                if(player)
                {
                    gnetDebug1("Adding active player:%s to the removal list", player->GetGamerInfo().GetName());

                    playersToRemove[numPlayersToRemove] = *actIt;
                    numPlayersToRemove++;
                }
            }
        }

        PendingPlayerList::iterator       pendIt   = m_PendingPlayers.begin();
	    PendingPlayerList::const_iterator pendStop = m_PendingPlayers.end();

	    for(; pendIt != pendStop; ++pendIt)
	    {
            if(gnetVerifyf(numPlayersToRemove < MAX_NUM_PHYSICAL_PLAYERS, "Adding too many players to the removal array!"))
            {
                netPlayer *player = *pendIt;

                if(player)
                {
                    gnetDebug1("Adding pending player:%s to the removal list", player->GetGamerInfo().GetName());
                    playersToRemove[numPlayersToRemove] = *pendIt;
                    numPlayersToRemove++;
                }
            }
        }

        for(unsigned index = 0; index < numPlayersToRemove; index++)
        {
            RemovePlayer(playersToRemove[index]);
        }

        m_Transactor.Shutdown();

        m_CxnMgr = NULL;

        m_ChannelId = NET_INVALID_CHANNEL_ID;

        gnetAssert(m_ExistingPlayers.empty());
        gnetAssert(m_NonPhysicalPlayers.empty());
        gnetAssert(m_PhysicalPlayers.empty());

        m_ExistingPlayers.clear();
        m_PendingPlayers.clear();
        m_ActivePlayers.clear();
        m_NonPhysicalPlayers.clear();
		m_PhysicalPlayers.clear();
 
		for (unsigned i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			m_PhysicalPlayersArray[i] = 0;
		}

		if(m_MyPlayer)
        {
            m_MyPlayer->Shutdown();
            m_MyPlayer = 0;
        }

        delete m_Log;
        m_Log = 0;

        m_IsInitialized = false;
        m_IsShuttingDown = false;
    }
}

void
netPlayerMgrBase::UpdatePlayerListsForPlayer(netPlayer* player)
{
	// assume we won't be activating this player
	bool activate = false;

	// check if we should move from pending to active
	bool isPending = std::find(m_PendingPlayers.begin(), m_PendingPlayers.end(), player) != m_PendingPlayers.end();
	if(isPending)
	{
		player->UpdateIsActiveState();
		if(player->IsActive())
		{
			m_PendingPlayers.erase(player);
			m_ActivePlayers.push_back(player);
			m_NonPhysicalPlayers.push_back(player);
			activate = true; 
		}
	}

	// check if we should move from active to pending (if this player wasn't added to the active list above)
	if(!activate)
	{
		bool isActive = std::find(m_ActivePlayers.begin(), m_ActivePlayers.end(), player) != m_ActivePlayers.end();
		if(isActive)
		{
			player->UpdateIsActiveState();
			if(!player->IsActive())
			{
				m_ActivePlayers.erase(player);
				m_PendingPlayers.push_back(player);
			}
		}
	}

	// check if we should move from non-physical to physical
	bool isNonPhysical = std::find(m_NonPhysicalPlayers.begin(), m_NonPhysicalPlayers.end(), player) != m_NonPhysicalPlayers.end();
	if(isNonPhysical)
	{
		if(player->IsPhysical())
		{
			m_NonPhysicalPlayers.erase(player);
			m_PhysicalPlayers.push_back(player);
		}
		else if(!player->IsActive())
		{
			m_NonPhysicalPlayers.erase(player);
		}
	}

	// check if we should move from physical to non-physical
	bool isPhysical = std::find(m_PhysicalPlayers.begin(), m_PhysicalPlayers.end(), player) != m_PhysicalPlayers.end();
	if(isPhysical)
	{
		if(!player->IsPhysical())
		{
			m_PhysicalPlayers.erase(player);

			if(player->IsActive())
			{
				m_NonPhysicalPlayers.push_back(player);
			}
		}
	}

	// activate this player if indicated
	if(activate)
	{
		ActivatePlayer(player);
	}

	// make sure the arrays are up to date
	UpdatePlayerArrays();
}

void
netPlayerMgrBase::UpdatePlayerLists()
{
	atFixedArray<netPlayer*, MAX_NUM_ACTIVE_PLAYERS> playersToActivate;

    //Move players from pending to active
	PendingPlayerList::iterator pendIt = m_PendingPlayers.begin();
	PendingPlayerList::const_iterator pendStop = m_PendingPlayers.end();
	for(; pendIt != pendStop; ++pendIt)
	{
		netPlayer* player = *pendIt;

		player->UpdateIsActiveState();
		if(player->IsActive())
		{
			pendIt--;
			m_PendingPlayers.erase(player);
			m_ActivePlayers.push_back(player);
			m_NonPhysicalPlayers.push_back(player);

			playersToActivate.Push(player);
		}
	}

    // Move players from active to pending - this happens when a connection is suddenly terminated and the player has not been cleaned up yet
	ActivePlayerList::iterator actIt = m_ActivePlayers.begin();
	ActivePlayerList::const_iterator actStop = m_ActivePlayers.end();
	for(; actIt != actStop; ++actIt)
	{
		netPlayer* player = *actIt;
		player->UpdateIsActiveState();
		if(!player->IsActive())
		{
			actIt--;
			m_ActivePlayers.erase(player);
			m_PendingPlayers.push_back(player);
		}
	}

	//Move players from non-physical to physical
	NonPhysicalPlayerList::iterator nonphysIt = m_NonPhysicalPlayers.begin();
	NonPhysicalPlayerList::const_iterator nonphysStop = m_NonPhysicalPlayers.end();
	for(; nonphysStop != nonphysIt; ++nonphysIt)
	{
		netPlayer* player = *nonphysIt;
		if(player->IsPhysical())
		{
			nonphysIt--;
			m_NonPhysicalPlayers.erase(player);
			m_PhysicalPlayers.push_back(player);
		}
		else if (!player->IsActive())
		{
			m_NonPhysicalPlayers.erase(nonphysIt);
		}
	}

	PhysicalPlayerList::iterator physIt = m_PhysicalPlayers.begin();
	PhysicalPlayerList::const_iterator physStop = m_PhysicalPlayers.end();
	for(; physStop != physIt; ++physIt)
	{
		netPlayer* player = *physIt;
		if(!player->IsPhysical())
		{
			physIt--;
			m_PhysicalPlayers.erase(player);

			if (player->IsActive())
			{
				m_NonPhysicalPlayers.push_back(player);
			}
		}
	}

	// activate any flagged player
	for (int i=0; i<playersToActivate.GetCount(); i++)
	{
		ActivatePlayer(playersToActivate[i]);
	}

	//Now that all the lists have been updated, call the active update on each of the active players.
	actIt = m_ActivePlayers.begin();
	actStop = m_ActivePlayers.end();
	for(; actIt != actStop; ++actIt)
	{
		netPlayer* player = *actIt;
		player->ActiveUpdate();
	}

	// make sure the arrays are up to date
	UpdatePlayerArrays();
}

void 
netPlayerMgrBase::UpdatePlayerArrays()
{
	m_NumPendingPlayers = 0;

	PendingPlayerList::iterator pendIt = m_PendingPlayers.begin();
	PendingPlayerList::const_iterator pendStop = m_PendingPlayers.end();
	for(; pendIt != pendStop; ++pendIt)
	{
		if(gnetVerifyf(m_NumPendingPlayers < MAX_NUM_ACTIVE_PLAYERS, "Too many pending players!"))
		{
			m_AllPendingPlayers[m_NumPendingPlayers] = *pendIt;
			m_NumPendingPlayers++;
		}
	}

	m_NumAllActivePlayers    = 0;
	m_NumRemoteActivePlayers = 0;

	ActivePlayerList::iterator actIt = m_ActivePlayers.begin();
	ActivePlayerList::const_iterator actStop = m_ActivePlayers.end();
	for(; actIt != actStop; ++actIt)
	{
		netPlayer* player = *actIt;

		if(player->IsRemote())
		{
			if(gnetVerifyf(m_NumRemoteActivePlayers < MAX_NUM_ACTIVE_PLAYERS - 1, "Too many remote active players!"))
			{
				m_RemoteActivePlayers[m_NumRemoteActivePlayers] = player;
				m_NumRemoteActivePlayers++;
			}
		}

		if(gnetVerifyf(m_NumAllActivePlayers < MAX_NUM_ACTIVE_PLAYERS, "Too many active players!"))
		{
			m_AllActivePlayers[m_NumAllActivePlayers] = player;
			m_NumAllActivePlayers++;
		}
	}

	m_RemotePhysicalPlayersInSession = 0;
	m_NumAllPhysicalPlayers          = 0;
	m_NumLocalPhysicalPlayers        = 0;
	m_NumRemotePhysicalPlayers       = 0;

	PhysicalPlayerList::iterator physIt = m_PhysicalPlayers.begin();
	PhysicalPlayerList::const_iterator physStop = m_PhysicalPlayers.end();
	for(; physStop != physIt; ++physIt)
	{
		netPlayer* player = *physIt;

		if(player->IsRemote())
		{
			m_RemotePhysicalPlayersInSession |= (1<<player->GetPhysicalPlayerIndex());

			if(gnetVerifyf(m_NumRemotePhysicalPlayers < MAX_NUM_PHYSICAL_PLAYERS - 1, "Too many remote physical players!"))
			{
				m_RemotePhysicalPlayers[m_NumRemotePhysicalPlayers] = player;
				m_NumRemotePhysicalPlayers++;
			}
		}
		else
		{
			gnetAssertf(player->IsLocal(), "Player is not local or remote!");

			if(gnetVerifyf(m_NumLocalPhysicalPlayers < MAX_NUM_PHYSICAL_PLAYERS, "Too many local physical players!"))
			{
				m_LocalPhysicalPlayers[m_NumLocalPhysicalPlayers] = player;
				m_NumLocalPhysicalPlayers++;
			}
		}

		if(gnetVerifyf(m_NumAllPhysicalPlayers < MAX_NUM_PHYSICAL_PLAYERS, "Too many physical players!"))
		{
			m_AllPhysicalPlayers[m_NumAllPhysicalPlayers] = player;
			m_NumAllPhysicalPlayers++;
		}
	}

#if __ASSERT
	for(unsigned index = 0; index < m_NumPendingPlayers; index++)
	{
		NETWORK_QUITF(m_AllPendingPlayers[index] != 0, "Unexpected NULL pending player!");
	}

	for(unsigned index = 0; index < m_NumAllActivePlayers; index++)
	{
		NETWORK_QUITF(m_AllActivePlayers[index] != 0, "Unexpected NULL active player!");
	}

	for(unsigned index = 0; index < m_NumRemoteActivePlayers; index++)
	{
		NETWORK_QUITF(m_RemoteActivePlayers[index] != 0, "Unexpected NULL remote active player!");
	}

	for(unsigned index = 0; index < m_NumAllPhysicalPlayers; index++)
	{
		NETWORK_QUITF(m_AllPhysicalPlayers[index] != 0, "Unexpected NULL physical player!");
	}
	for(unsigned index = 0; index < m_NumLocalPhysicalPlayers; index++)
	{
		NETWORK_QUITF(m_LocalPhysicalPlayers[index] != 0, "Unexpected NULL local physical player!");
	}
	for(unsigned index = 0; index < m_NumRemotePhysicalPlayers; index++)
	{
		NETWORK_QUITF(m_RemotePhysicalPlayers[index] != 0, "Unexpected NULL remote physical player!");
	}
#endif // __ASSERT
}

void netPlayerMgrBase::SendNonPhysicalUpdates()
{
    if(gnetVerify(m_PhysicalOnRemoteMachine.IsBound()))
    {
        static const unsigned int TIME_BETWEEN_NON_PHYSICAL_UPDATES = 2000;
        static unsigned int lastTimeSent = sysTimer::GetSystemMsTime();
        unsigned int currentTime = sysTimer::GetSystemMsTime();

        //TODO - This will currently send non-physical updates to all players every update interval,
        //       which will cause the bandwidth to spike - this code should load balance these updates
        //       between players in the future

        //TODO - Implement LOD for non-phys updates so players who are
        //       far away receive less frequent updates.
        if((currentTime - lastTimeSent) > TIME_BETWEEN_NON_PHYSICAL_UPDATES)
        {
            lastTimeSent = currentTime;

            netPlayer *localPlayers[MAX_NUM_ACTIVE_PLAYERS];
            netPlayer *remotePlayers[MAX_NUM_ACTIVE_PLAYERS];
            unsigned numLocalPlayers  = 0;
            unsigned numRemotePlayers = 0;

            ActivePlayerList::iterator it = m_ActivePlayers.begin();
            ActivePlayerList::iterator next = it;
            ActivePlayerList::const_iterator stop = m_ActivePlayers.end();
            for(++next; stop != it; it = next, ++next)
            {
                netPlayer* player = *it;
                if(player->IsActive())
                {
                    if(player->IsLocal())
                    {
                        if(gnetVerify(numLocalPlayers < MAX_NUM_ACTIVE_PLAYERS))
                        {
                            localPlayers[numLocalPlayers++] = player;
                        }
                    }
                    else
                    {
                        gnetAssert(player->IsRemote());

                        if(gnetVerify(numRemotePlayers < MAX_NUM_ACTIVE_PLAYERS))
                        {
                            remotePlayers[numRemotePlayers++] = player;
                        }
                    }
                }
            }

            for(unsigned localIndex = 0; localIndex < numLocalPlayers; localIndex++)
            {
                netPlayer *localPlayer = localPlayers[localIndex];

                if(localPlayer)
                {
                    unsigned localPeerPlayerIndex = localPlayer->GetPeerPlayerIndex();

                    for(unsigned remoteIndex = 0; remoteIndex < numRemotePlayers; remoteIndex++)
                    {
                        netPlayer *remotePlayer = remotePlayers[remoteIndex];

                        if(remotePlayer)
                        {
#if ENABLE_NETWORK_BOTS
                            if(!remotePlayer->IsBot())
#endif // ENABLE_NETWORK_BOTS
                            {
                                bool physicalOnRemoteMachine = m_PhysicalOnRemoteMachine(localPeerPlayerIndex, *remotePlayer);

                                if(!physicalOnRemoteMachine)
                                {
                                    nonPhysicalDataMsg nonPhysicalUpdateMsg(localPeerPlayerIndex, *localPlayer->GetNonPhysicalData());
                                    netSequence seq = 0;
                                    if (netInterface::SendMsg(remotePlayer, nonPhysicalUpdateMsg, 0, &seq))
									{
                                        BANK_ONLY(m_NumNonPhysicalUpdateMessagesSent++);
										BANK_ONLY(m_BandwidthStats.AddBandwidthOut(m_BandwidthRecorderId, nonPhysicalUpdateMsg.GetMessageDataBitSize()));
									}
                                    nonPhysicalUpdateMsg.WriteToLogFile(false, seq, *remotePlayer);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

netPlayer* netPlayerMgrBase::AddRemotePlayer(const rlGamerInfo& gamerInfo,
                                         const playerDataMsg& playerData,
                                         const EndpointId endpointId,
                                         nonPhysicalPlayerDataBase *nonPhysicalPlayerData)
{
    gnetAssert(!this->GetPlayerFromGamerId(gamerInfo.GetGamerId(), false));
    gnetAssert(!this->GetPlayerFromPeerId(gamerInfo.GetPeerInfo().GetPeerId()));
    gnetAssert(gamerInfo.IsRemote());

    netPlayer* newPlayer = AllocatePlayer();

    if(gnetVerify(newPlayer))
    {
        gnetAssert(NET_IS_VALID_ENDPOINT_ID(endpointId));

#if ENABLE_NETWORK_BOTS
		gnetAssert(!newPlayer->IsBot());
#endif // ENABLE_NETWORK_BOTS
  
		const int cxnId = m_CxnMgr->OpenConnection(endpointId, m_ChannelId, NULL, 0, NULL);

        gnetAssert(cxnId >= 0);

        newPlayer->Init(gamerInfo, cxnId, endpointId, playerData.m_NatType);

        m_ExistingPlayers.push_back(newPlayer);
        m_PendingPlayers.push_back(newPlayer);

        NetworkLogUtils::WriteLogEvent(GetLog(), "ADDING_REMOTE_PLAYER", "");
#if ENABLE_NETWORK_LOGGING
		const netAddress& addr = m_CxnMgr->GetAddress(endpointId);
		const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ? addr.GetProxyAddress() : addr.GetTargetAddress();
        GetLog().WriteDataValue("IP Address", "%s", NET_ADDR_FOR_PRINTF(sockAddr));
#endif
        GetLog().WriteDataValue("Player Name", "%s", gamerInfo.GetName());
        GetLog().WriteDataValue("Active Player index", "%d",  newPlayer->GetActivePlayerIndex());

        gnetAssert(nonPhysicalPlayerData);
        gnetAssert(newPlayer->GetNonPhysicalData() == 0);
        newPlayer->SetNonPhysicalData(nonPhysicalPlayerData);

        UpdatePlayerArrays();
    }

    return newPlayer;
}

void netPlayerMgrBase::AddLocalPlayer(const rlGamerInfo& NET_ASSERTS_ONLY(gamerInfo),
                                       const playerDataMsg& UNUSED_PARAM(playerData),
                                       nonPhysicalPlayerDataBase *nonPhysicalPlayerData)
{
    gnetAssert(gamerInfo == m_MyPlayer->GetGamerInfo());

    NetworkLogUtils::WriteLogEvent(GetLog(), "ADDING_LOCAL_PLAYER", "");
    //GetLog().WriteDataValue("Our player colour", "%d", playerData.m_Colour);

    gnetAssert(nonPhysicalPlayerData);
    gnetAssert(m_MyPlayer->GetNonPhysicalData() == 0);
    m_MyPlayer->SetNonPhysicalData(nonPhysicalPlayerData);
}

void netPlayerMgrBase::RemovePlayer(netPlayer* pPlayer)
{
    if(gnetVerify(pPlayer))
    {
        gnetDebug1("Removing player:%s", pPlayer->GetGamerInfo().GetName());

        NetworkLogUtils::WriteLogEvent(GetLog(), "REMOVING_PLAYER", "");

        if (!pPlayer->IsMyPlayer())
        {
#if ENABLE_NETWORK_LOGGING
			const netAddress& addr = m_CxnMgr->GetAddress(pPlayer->GetEndpointId());
			const netSocketAddress& sockAddr = addr.IsPeerRelayAddr() ? addr.GetProxyAddress() : addr.GetTargetAddress();
            GetLog().WriteDataValue("IP Address", "%s", NET_ADDR_FOR_PRINTF(sockAddr));
#endif
        }

		GetLog().WriteDataValue("Active Index", "%d", pPlayer->GetActivePlayerIndex());
		GetLog().WriteDataValue("Physical Index", "%d", pPlayer->GetPhysicalPlayerIndex());
        GetLog().WriteDataValue("Player Name", "%s",  pPlayer->GetLogName());

        //Make sure this player is in the correct lists before
        //attempting to remove.
        UpdatePlayerListsForPlayer(pPlayer);

        if(pPlayer->IsPhysical())
        {
			m_PhysicalPlayers.erase(pPlayer);
        }
        else if(pPlayer->IsActive())
        {
			m_NonPhysicalPlayers.erase(pPlayer);
        }

        if(pPlayer->IsActive())
        {
			m_ActivePlayers.erase(pPlayer);
        }
        else
        {
            gnetAssertf(pPlayer->IsPending(), "Player is not active or pending!");

  			m_PendingPlayers.erase(pPlayer);
        }

		m_ExistingPlayers.erase(pPlayer);

        gnetAssert(std::find(m_PendingPlayers.begin(), m_PendingPlayers.end(), pPlayer) == m_PendingPlayers.end());
        gnetAssert(std::find(m_ActivePlayers.begin(), m_ActivePlayers.end(), pPlayer) == m_ActivePlayers.end());
        gnetAssert(std::find(m_NonPhysicalPlayers.begin(), m_NonPhysicalPlayers.end(), pPlayer) == m_NonPhysicalPlayers.end());
        gnetAssert(std::find(m_PhysicalPlayers.begin(), m_PhysicalPlayers.end(), pPlayer) == m_PhysicalPlayers.end());

		PhysicalPlayerIndex physIndx = pPlayer->GetPhysicalPlayerIndex();

		if (physIndx != INVALID_PLAYER_INDEX && 
            gnetVerify(physIndx < MAX_NUM_PHYSICAL_PLAYERS) &&
			m_PhysicalPlayersArray[physIndx] == pPlayer)
		{
			m_PhysicalPlayersArray[physIndx] = 0;
		}

#if ENABLE_NETWORK_BOTS
		if (!pPlayer->IsMyPlayer() && !pPlayer->IsBot())
#else
		if (!pPlayer->IsMyPlayer())
#endif // ENABLE_NETWORK_BOTS
		{
            m_CxnMgr->CloseConnection(pPlayer->GetConnectionId(),
                                        NET_CLOSE_GRACEFULLY);
        }

        pPlayer->Shutdown();

        DeallocatePlayer(pPlayer);

        // the lists needs updating again once the player has been removed from the lists
        UpdatePlayerArrays();
    }
}

void netPlayerMgrBase::UpdatePlayerPhysicalStatus(netPlayer *player)
{
    this->UpdatePlayerListsForPlayer(player);
}

void
netPlayerMgrBase::OnNetEvent(netConnectionManager* /*cxnMgr*/,
                                const netEvent* evt)
{
    if(evt->m_CxnId >= 0)
    {
        const netEventFrameReceived* frame =
        evt->GetId() == NET_EVENT_FRAME_RECEIVED ? evt->m_FrameReceived : 0;

        unsigned msgId;

        if(frame)
        {
            netPlayer* player = this->GetPlayerFromConnectionId(evt->m_CxnId);

            if(player)
            {
                const void *recvBuffer = frame->m_Payload;
                unsigned recvNumBytes  = frame->m_SizeofPayload;

                netPlayer *toPlayer   = netInterface::GetLocalPlayer();
                netPlayer *fromPlayer = player;

#if ENABLE_NETWORK_BOTS

                u8 tempBuffer[netMessage::MAX_BYTE_SIZEOF_PAYLOAD];

                if(SIZEOF_LOCAL_PLAYER_ID > 0 && recvNumBytes >= SIZEOF_NBAL_HEADER_IN_BYTES)
                {
                    u32      abstractionLayerID  = 0;
                    unsigned toPeerPlayerIndex   = 0;
                    unsigned fromPeerPlayerIndex = 0;

                    datBitBuffer bitBuffer;
                    bitBuffer.SetReadOnlyBytes(frame->m_Payload, frame->m_SizeofPayload);
                    bitBuffer.ReadUns(abstractionLayerID, sizeof(ABSTRACTION_LAYER_ID)<<3);
                    bitBuffer.ReadUns(toPeerPlayerIndex, SIZEOF_LOCAL_PLAYER_ID);
                    bitBuffer.ReadUns(fromPeerPlayerIndex, SIZEOF_LOCAL_PLAYER_ID);
                    datBitBuffer::CopyBits(tempBuffer,
                                           bitBuffer.GetReadOnlyBits(),
                                           (frame->m_SizeofPayload<<3)-SIZEOF_NBAL_HEADER_IN_BITS,
                                           0,
                                           SIZEOF_NBAL_HEADER_IN_BITS);

                    if(abstractionLayerID == ABSTRACTION_LAYER_ID)
                    {
                        recvBuffer    = tempBuffer;
                        recvNumBytes -= SIZEOF_NBAL_HEADER_IN_BYTES;

                        if(toPeerPlayerIndex > 0)
                        {
                            toPlayer = netInterface::GetPlayerFromPeerPlayerIndex(toPeerPlayerIndex, toPlayer->GetGamerInfo().GetPeerInfo());
                        }

                        if(fromPeerPlayerIndex > 0)
                        {
                            fromPlayer = netInterface::GetPlayerFromPeerPlayerIndex(fromPeerPlayerIndex, fromPlayer->GetGamerInfo().GetPeerInfo());
                        }
                    }
                }
#endif // ENABLE_NETWORK_BOTS

                if(gnetVerify(toPlayer) && gnetVerify(fromPlayer))
                {
                    ReceivedMessageData messageData(recvBuffer,
                                                    recvNumBytes,
                                                    frame->m_Sequence,
                                                    fromPlayer,
                                                    toPlayer);
                    m_NetEventDelegator.Dispatch(messageData);

                    if(netMessage::GetId(&msgId, messageData.m_MessageData, messageData.m_MessageDataSize))
                    {
                        if (msgId == nonPhysicalDataMsg::MSG_ID())
                        {
                            HandleNonPhysicalUpdateMsg(messageData);
                        }
#if ENABLE_NETWORK_BOTS
                        else if (msgId == activateNetworkBotMsg::MSG_ID())
                        {
                            gnetAssertf(fromPlayer->IsBot(), "Received an activate bot message from a player!");
                            if(gnetVerifyf(fromPlayer->GetBot(), "Received an activate bot message from an uninitialised bot!"))
                            {
                                fromPlayer->GetBot()->SetActive(true);
								UpdatePlayerListsForPlayer(fromPlayer);
                            }
                        }
#endif // ENABLE_NETWORK_BOTS
                    }
                }
            }
        }
    }
}

void
netPlayerMgrBase::OnTxRqst(netTransactor* /*transactor*/,
                            netRequestHandler* /*rqstHandler*/,
                            const netRequest* rqst)
{
    if(rqst->m_TxInfo.m_CxnId >= 0)
    {
        netPlayer* player = this->GetPlayerFromConnectionId(rqst->m_TxInfo.m_CxnId);

        if(player)
        {
            m_TxRqstDelegator.Dispatch(player, rqst);
        }
    }
}

void netPlayerMgrBase::HandleNonPhysicalUpdateMsg(const ReceivedMessageData &messageData)
{
    if(gnetVerify(messageData.IsValid()))
    {
        nonPhysicalDataMsg msg(messageData.m_FromPlayer->GetGamerInfo().GetPeerInfo());

        if(msg.Import(messageData.m_MessageData, messageData.m_MessageDataSize) == false)
        {
            gnetAssertf(false, "Failed importing nonPhysicalDataMsg message");
        }
        else
        {
            msg.WriteToLogFile(true, messageData.m_NetSequence, *messageData.m_FromPlayer);

			BANK_ONLY(m_BandwidthStats.AddBandwidthIn(m_BandwidthRecorderId, msg.GetMessageDataBitSize()));
        }
    }
}

} // namespace rage
