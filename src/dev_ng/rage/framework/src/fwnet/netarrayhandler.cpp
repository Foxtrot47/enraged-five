// 
// networkArrayHandler.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "fwnet/netarrayhandler.h"

// framework includes
#include "fwnet/netarraymgr.h"
#include "fwnet/neteventtypes.h"
#include "fwnet/netlogginginterface.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netplayer.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netsyncdata.h"
#include "fwnet/netinterface.h"
#include "fwnet/netchannel.h"
#include "fwnet/optimisations.h"

// rage includes
#include "data/bitbuffer.h"
#include "string/stringhash.h"

NETWORK_OPTIMISATIONS()

#define BITS_TO_BYTES(bits)			(((bits)+7)>>3)
#define ELEMENT_HEADER_SIZE			(GetSizeOfElementIndex() + (CanHaveEmptyElements() ? 1 : 0) + TAG_SIZE)

#if __FINAL
#define QUIT_ON_BAD_ARRAY_DATA 0
#else
#define QUIT_ON_BAD_ARRAY_DATA 1
#endif

namespace rage
{
#if __BANK
char  netArrayIdentifierBase::ms_logName[netArrayIdentifierBase::LOG_NAME_LEN];
#endif

netArrayHandlerBase::netArrayHandlerBase( NetworkArrayHandlerType handlerType,  
										  unsigned numElements,
										  unsigned maxNumSyncedPlayers) 
: m_LastSentSequences(MAX_NUM_PHYSICAL_PLAYERS)
, m_LastReceivedSequences(MAX_NUM_PHYSICAL_PLAYERS)
, m_UnackedSplitUpdateFlags(MAX_NUM_PHYSICAL_PLAYERS)
, m_HandlerType(handlerType)
, m_NumArrayElements(static_cast<u16>(numElements))
, m_NumElementsInUse(static_cast<u16>(numElements))
, m_MaxNumSyncedPlayers(static_cast<u8>(maxNumSyncedPlayers))
, m_MaxSizeOfElementInBits(0)
, m_BitsizeOfIndex(0)
, m_ElementEmptyFlagsSize(0)
, m_arrayDataChecksum(0)
, m_arrayMgrBatch(netArrayManager::INVALID_BATCH)
, m_checksumCalculated(false)
, m_unackedSplitUpdates(false)
{
	gnetAssert(m_NumArrayElements > 0);

	unsigned n = m_NumArrayElements;

	while (n != 0)
	{
		n >>= 1;
		m_BitsizeOfIndex++;
	}
}

void netArrayHandlerBase::Init()
{
	m_MaxSizeOfElementInBits = static_cast<u8>(GetMaxElementSizeInBits());

	Assert(m_MaxSizeOfElementInBits > 0);

    // element data is byte aligned to allow comparing element data at the byte level
    if((m_MaxSizeOfElementInBits % 8) != 0)
    {
        m_MaxSizeOfElementInBits += (8 - (m_MaxSizeOfElementInBits % 8));
    }

	gnetAssert(m_MaxSizeOfElementInBits > 0);

	if (CanHaveEmptyElements())
	{
		m_ElementEmptyFlagsSize = BITS_TO_BYTES(m_NumArrayElements) << 3;

		gnetAssert(m_ElementEmptyFlagsSize >= m_NumArrayElements);
	}

	if (GetSyncData())
	{
		GetSyncData()->Init();
	}

	ResetSyncData();

	m_arrayDataChecksum = 0;
	m_arrayMgrBatch = netArrayManager::INVALID_BATCH;

	m_unackedSplitUpdates = false;
}

void netArrayHandlerBase::Shutdown()
{
	if (GetSyncData())
	{
		GetSyncData()->Shutdown();
	}
}

void netArrayHandlerBase::Update()
{
	Assert(IsArrayLocallyArbitrated());

	if (!IsManualDirtyArray())
	{
		RecalculateDirtyElements();
	}

	ResetEmptyElements();
}

void netArrayHandlerBase::PlayerHasJoined(const netPlayer& joiningPlayer)
{
	if (IsPlayerInScope(joiningPlayer))
	{
		unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(joiningPlayer);

		bool canHaveEmptyElements = CanHaveEmptyElements() && AllElementsStartEmpty();
		bool elementsAllInScope = AreElementsAlwaysInScope();

		if (IsArrayLocallyArbitrated() && 
			gnetVerify(GetSyncData()) && 
			gnetVerify(playerSyncDataIndex<m_MaxNumSyncedPlayers))
		{
			// reset sync data for joining player
			for (unsigned i=0; i<m_NumElementsInUse; i++)
			{
				if (IsElementLocallyArbitrated(i))
				{
					bool bElementInScope = elementsAllInScope || IsElementInScopeInternal(i, joiningPlayer);
					bool bElementEmpty = canHaveEmptyElements && IsElementEmpty(i);

					// prevent empty elements being sent to new players if necessary. Some handlers can have all of their elements initially empty when the
					// array is registered. If the elements have been dirtied, then emptied again, there is no point in sending them to a new player. 
					GetSyncData()->InitialisePlayerSyncDataForDataUnit(i, playerSyncDataIndex, bElementInScope && !bElementEmpty);
				}
			}

			GetSyncData()->Update();
		}

		if (gnetVerify(joiningPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
		{
			m_LastSentSequences[joiningPlayer.GetPhysicalPlayerIndex()] = 0;
			m_LastReceivedSequences[joiningPlayer.GetPhysicalPlayerIndex()] = 0;
			ResetUnackedSplitUpdateFlagsForPlayer(joiningPlayer);
		}
	}
}

void netArrayHandlerBase::PlayerHasLeft(const netPlayer& leavingPlayer)
{
    if (IsPlayerInScope(leavingPlayer) && GetSyncData())
	{
		unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(leavingPlayer);

		for(unsigned index = 0; index < m_NumElementsInUse; index++)
		{
			GetSyncData()->InitialisePlayerSyncDataForDataUnit(index, playerSyncDataIndex, false);
		}

		GetSyncData()->SetSyncedWithPlayer(playerSyncDataIndex);

		if (gnetVerify(leavingPlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
		{
			ResetUnackedSplitUpdateFlagsForPlayer(leavingPlayer);
		}
    }
}

bool netArrayHandlerBase::IsSyncedWithPlayer(const netPlayer& player) const
{
	if (IsPlayerInScope(player) && gnetVerify(GetSyncData()))
	{
		unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

		return GetSyncData()->IsSyncedWithPlayer(playerSyncDataIndex);
	}

	return true;
}

unsigned netArrayHandlerBase::GetTotalSizeOfUpdateData(const netPlayer& player, unsigned messageSize, unsigned &numUpdates, u16 updateStartElements[])
{
	unsigned totalSize = 0;
	unsigned updateSize = 0;
	
	numUpdates = 0;

	static u16 UNASSIGNED_ELEMENT = 0xffff;

	updateStartElements[0] = UNASSIGNED_ELEMENT;

	if (IsPlayerInScope(player))
	{
		u32 playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

        bool elementsAllInScope = AreElementsAlwaysInScope();
        bool isSharedArray      = IsSharedArray();

		for (unsigned i=0; i<m_NumElementsInUse; i++)
		{
			if ((!isSharedArray || IsElementLocallyArbitrated(i)) &&
				!GetSyncData()->GetSyncDataUnit(i).IsSyncedWithPlayer(playerSyncDataIndex))
  			{
				bool bElementInScope = elementsAllInScope || IsElementInScopeInternal(i, player);

				unsigned elementSize = ELEMENT_HEADER_SIZE + (bElementInScope ? GetCurrentElementSizeInBits(i) : 0);

				if (numUpdates==0 && updateStartElements[0] == UNASSIGNED_ELEMENT)
				{
					updateStartElements[0] = (u16)i;
				}

				if (updateSize + elementSize > messageSize)
				{
					numUpdates++;
					gnetAssert(updateSize > 0);
					updateSize = 0;

					if (numUpdates < netArrayManager::MAX_SPLIT_UPDATES)
					{
						updateStartElements[numUpdates] = (u16)i;
					}
					else
					{
						FatalAssertf(0, "%s (%s) : The array is too large, maximum number of split updates exceeded", GetHandlerName(), GetIdentifier() ? GetIdentifier()->GetLogName() : "");
					}
				}

				updateSize += elementSize;
				totalSize += elementSize;
			}
		}
	}

	if (updateSize > 0)
		numUpdates++;

	return totalSize;
}

unsigned netArrayHandlerBase::GetSizeOfMessageUpdateData(const netPlayer& player, unsigned messageSize, unsigned startElement, bool logSizes)
{
	unsigned size = 0;

	LOGGING_ONLY(netLoggingInterface &log = netInterface::GetArrayManager().GetLog());

	// the size of the update data must fit in messageSize and only include complete element data
	if (IsPlayerInScope(player) && gnetVerify(GetSyncData()))
	{
		u32 playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

        bool elementsAllInScope = AreElementsAlwaysInScope();
        bool isSharedArray      = IsSharedArray();

		for (unsigned i=startElement; i<m_NumElementsInUse; i++)
		{
			if ((!isSharedArray || IsElementLocallyArbitrated(i)) &&
				!GetSyncData()->GetSyncDataUnit(i).IsSyncedWithPlayer(playerSyncDataIndex))
			{
				bool bElementInScope = elementsAllInScope || IsElementInScopeInternal(i, player);

				unsigned elementSize = ELEMENT_HEADER_SIZE + (bElementInScope ? GetCurrentElementSizeInBits(i) : 0);
		
				if (size + elementSize > messageSize)
				{
					break;
				}

				size += elementSize;

				if (logSizes)
				{
					LOGGING_ONLY(log.Log("\tSIZE OF ELEMENT %d : %d (total size : %d)\r\n", i, elementSize, size));
				}
			}
		}
	}

	return size;
}

void netArrayHandlerBase::WriteUpdate(const netPlayer& player, datBitBuffer& bitBuffer, netSequence updateSeq, unsigned& currentElement, bool logSizes)
{
	netLoggingInterface &log = netInterface::GetArrayManager().GetLog();

	gnetAssert(IsArrayLocallyArbitrated());

	unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

	NET_ASSERTS_ONLY(int startPos = bitBuffer.GetCursorPos());

	if (gnetVerify(GetSyncData()) && gnetVerify(playerSyncDataIndex<m_MaxNumSyncedPlayers))
	{
        bool elementsAllInScope		= AreElementsAlwaysInScope();
        bool isSharedArray			= IsSharedArray();
		bool canHaveEmptyElements	= CanHaveEmptyElements();

		// elements that can have scope must also be able to be empty
		Assert(elementsAllInScope || canHaveEmptyElements);

		unsigned i;
		for (i=currentElement; i<m_NumElementsInUse; i++)
		{
			if ((!isSharedArray || IsElementLocallyArbitrated(i)) && 
				!GetSyncData()->GetSyncDataUnit(i).IsSyncedWithPlayer(playerSyncDataIndex))
			{
				NET_ASSERTS_ONLY(int elementStartPos = bitBuffer.GetCursorPos());

				bool bElementInScope = elementsAllInScope || IsElementInScopeInternal(i, player);

				unsigned elementSize = ELEMENT_HEADER_SIZE + (bElementInScope ? GetCurrentElementSizeInBits(i) : 0);

				// only write the element if it will fit in the bit buffer
				if (!bitBuffer.CanWriteBits(elementSize))
				{
					break;
				}

				bool elementIsEmpty = false;
				
				// write a tag so we can check everything is properly in sync 
				SET_MSG_TAG(bitBuffer);

				// write element index
				WriteElementIndex(bitBuffer, i);

				if (!logSizes)
				{
					LOGGING_ONLY(log.Log("\tELEMENT_%d : \r\n", i));
				}

				// write whether the element is empty
				if (canHaveEmptyElements)
				{
					elementIsEmpty = IsElementEmpty(i);

					// write an empty element for the player if it has gone out of scope with him
					bitBuffer.WriteBool(elementIsEmpty || !bElementInScope);
				}

				gnetAssert(u32(bitBuffer.GetCursorPos() - elementStartPos) == ELEMENT_HEADER_SIZE);

				// write the element into the bit buffer if it is not empty
				if (elementIsEmpty || !bElementInScope)
				{
					if (!logSizes)
					{
						if (elementIsEmpty)
						{
							log.Log("\t\tEmpty\r\n" );
						}
						else
						{
							log.Log("\t\tEmpty (out of scope)\r\n" );
						}
					}
				}
				else if (GetSyncData()->GetCurrentStateBuffer())
				{
					// copy current state to message buffer
					unsigned sizeOfData = GetSyncData()->GetSyncDataUnit(i).GetSizeOfCurrentData();
 					GetSyncData()->CopyCurrentStateData(GetElementCursorPos(i), bitBuffer, bitBuffer.GetCursorPos(), sizeOfData);

					if (!logSizes)
					{
						LogElement(i, log);
					}
				}
				else
				{
					WriteElement(bitBuffer, i, logSizes ? NULL : &log);
				}

#if __DEV
				if (logSizes)
				{
					if (elementIsEmpty)
					{
						log.Log("\tWRITTEN SIZE OF EMPTY ELEMENT %d : %d (total size : %d)\r\n", i, elementSize, bitBuffer.GetCursorPos() - startPos);
					}
					else
					{
						log.Log("\tWRITTEN SIZE OF ELEMENT %d : %d (total size : %d)\r\n", i, elementSize, bitBuffer.GetCursorPos() - startPos);
					}
				}
#endif
				gnetAssert(bitBuffer.GetCursorPos() - elementStartPos == (int)elementSize);

				GetSyncData()->GetSyncDataUnit(i).UpdateSentToPlayer(playerSyncDataIndex, &updateSeq);
			}
		}

		currentElement = i;
	}

	// sanity check that some data was written
	gnetAssert(bitBuffer.GetCursorPos() > startPos);
}

bool netArrayHandlerBase::ReadUpdate(const netPlayer& player, datBitBuffer& bitBuffer, unsigned dataSize, netSequence updateSeq)
{
	unsigned index;
    bool elementEmpty;
    bool success = true;

    netLoggingInterface &log = netInterface::GetArrayManager().GetLog();

	int startPos = bitBuffer.GetCursorPos();

	bool canHaveEmptyElements = CanHaveEmptyElements();

	while( (bitBuffer.GetCursorPos()-startPos) < static_cast<int>(dataSize))
    {
		int lastCursorPos = bitBuffer.GetCursorPos();

		// read tag to make sure everything is properly in sync
		GET_MSG_TAG(bitBuffer);

		ReadElementIndex(bitBuffer, index);  

		log.Log("\tELEMENT_%d : \r\n", index);

		if (!IsValidIndex(index))
		{
#if QUIT_ON_BAD_ARRAY_DATA
			NETWORK_QUITF(0, "netArrayHandlerBase::ReadUpdate - invalid index for %s (%d - max elements %d). This usually happens when running with incompatible builds", GetHandlerName(), index, m_NumElementsInUse);
#else
			log.WriteDataValue("FAIL", "Element is invalid. Array data is corrupt\r\n");
			return false;
#endif
		}
 
		if (canHaveEmptyElements)
		{
			bitBuffer.ReadBool(elementEmpty);
		}
		else
		{
			elementEmpty = false;
		}

		if (!elementEmpty)
		{
			ReadElement(bitBuffer, index, &log);
		}

        if (!CanApplyElementData(index, player, elementEmpty))
		{
			log.WriteDataValue("FAIL", "Could not apply element data\r\n");
			success = false;
		}
		else
        {
			if (elementEmpty)
			{
				log.Log("\t\tEmpty\r\n" );

                SetElementEmpty(index);

				ResetElementSyncData(index);
			}
            else
			{
				// Flag the sync data unit as dirty for this element - this will ensure that the element will be sent out in any future updates
				// if the arbitration for the array migrates locally. This must be done here, before SetElementEmpty is called (shared arrays do not
				// need empty elements to be flagged as dirty and will reset this state in SetElementEmpty).
				if (GetSyncData())
				{
					GetSyncData()->SetSyncDataUnitDirty(index);
				}

				ApplyElementData(index, player);

				DoPostElementReadProcessing(index);
            }
		}

		if (lastCursorPos == bitBuffer.GetCursorPos())
		{
#if QUIT_ON_BAD_ARRAY_DATA
			Quitf(ERR_NET_ARRAY,"Cannot read array handler update from %s - incompatible builds", player.GetLogName());
#else
			log.WriteDataValue("FAIL", "Cannot read array handler update. Array data is corrupt\r\n");
			return false;
#endif
		}
    }

	gnetAssert(bitBuffer.GetCursorPos() == startPos + static_cast<int>(dataSize));

	DoPostReadProcessing();

	if (success && gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		m_LastReceivedSequences[player.GetPhysicalPlayerIndex()] = updateSeq;
	}

    return success;
}

void netArrayHandlerBase::HandleAck(const netPlayer& player, const unsigned timeAckedMessageSent, unsigned firstElement, unsigned lastElement)
{
	netLoggingInterface &log = netInterface::GetArrayManager().GetLog();

	NetworkLogUtils::WriteLogEvent(log, "RECEIVED_ACK", "%s", GetHandlerName());
	if (GetIdentifier())
	{
		log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
	}

	log.WriteDataValue("Time message sent", "%u", timeAckedMessageSent);

	if (firstElement != 0 || lastElement != GetNumArrayElements()-1)
	{
		log.WriteDataValue("First element", "%d", firstElement);
		log.WriteDataValue("Last element", "%d", lastElement);
	}

	if (IsPlayerInScope(player) && gnetVerify(GetSyncData()))
	{
		unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

		bool bAckProcessed = false;

		if (gnetVerify(playerSyncDataIndex<m_MaxNumSyncedPlayers))
		{
			PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

			if (gnetVerify(playerIndex != INVALID_PLAYER_INDEX) && m_UnackedSplitUpdateFlags[playerIndex] != 0)
			{
				// m_UnackedSplitUpdateFlags should not be set for arrays that do not cache split updates
				gnetAssert(CacheSplitUpdates());

				log.WriteDataValue("REJECTED", "%s", playerIndex == INVALID_PLAYER_INDEX ? "Invalid player index" : "m_UnackedSplitUpdateFlags set");

				// don't process acks from previous updates if we have sent a split update for this handler, otherwise some elements will be removed from the next update
				return;
			}

			for (unsigned i=firstElement; i<=lastElement; i++)
			{
				if (IsElementLocallyArbitrated(i))
				{
					GetSyncData()->GetSyncDataUnit(i).ReceivedAck(playerSyncDataIndex, timeAckedMessageSent);
					bAckProcessed = true;
				}
			}
		}

		if (bAckProcessed)
		{
			log.WriteDataValue("Processed", "True. Unsynced players = %u", GetUnsyncedPlayers());

			// update sync data so that the unsynced state matches that of the sync units
			GetSyncData()->Update();
		}
	}
	else
	{
		log.WriteDataValue("REJECTED", "%s", IsPlayerInScope(player) ? "Player out of scope" : "No sync data");
	}
}

void netArrayHandlerBase::HandleSplitUpdateAck(const netPlayer& player, const netSequence updateSeq, unsigned splitUpdateNum, const unsigned timeAckedMessageSent)
{
	PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	gnetAssert(CacheSplitUpdates());

	if (AssertVerify(playerIndex != INVALID_PLAYER_INDEX) && 
		m_LastSentSequences[playerIndex] == updateSeq && 
		m_UnackedSplitUpdateFlags[playerIndex] != 0)
	{
		m_UnackedSplitUpdateFlags[playerIndex] &= ~(1<<splitUpdateNum);

		if (m_UnackedSplitUpdateFlags[playerIndex] == 0)
		{
			ResetUnackedSplitUpdateFlagsForPlayer(player);

			// all updates have now been received by this player
			unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

			if (gnetVerify(playerSyncDataIndex<m_MaxNumSyncedPlayers))
			{
				for (unsigned i=0; i<m_NumArrayElements; i++)
				{
					if (IsElementLocallyArbitrated(i))
					{
						GetSyncData()->GetSyncDataUnit(i).ReceivedAck(playerSyncDataIndex, timeAckedMessageSent);
					}
				}

				// update sync data so that the unsynced state matches that of the sync units
				GetSyncData()->Update();

				if (!GetSyncData()->IsSyncedWithPlayer(playerSyncDataIndex))
				{
#if __ASSERT
					for (u32 i=0; i<GetSyncData()->GetNumSyncDataUnits(); i++)
					{
						netSyncDataUnitBase& unit = GetSyncData()->GetSyncDataUnit(i);

						if (unit.GetUnsyncedPlayers() & playerSyncDataIndex)
						{
							gnetDebug2("Array element %d still unsynced. Locally arbitrated: %d. Time last dirtied: %u. Time of acked message sent: %u.", i, IsElementLocallyArbitrated(i), unit.GetTimeLastDirtied(), timeAckedMessageSent);
							unit.ClearDirtyForPlayer(playerSyncDataIndex);
						}
						else if (unit.GetUpdatePlayers() & playerSyncDataIndex)
						{
							gnetDebug2("Array element %d flagged for update", i);
						}
					}

					gnetDebug2("%s (%s) : HandleSplitUpdateAck - all updates acked but the handler is still flagged as unsynced for %s", GetHandlerName(), GetIdentifier() ? GetIdentifier()->GetLogName() : "", player.GetLogName());
#endif	

#if ENABLE_NETWORK_LOGGING
					netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
					log.WriteDataValue("Error", "All updates acked but the handler is still flagged as unsynced for %s", player.GetLogName());
#endif	
					GetSyncData()->SetSyncedWithPlayer(playerSyncDataIndex);
				}
			}
		}
	}
}

netSequence netArrayHandlerBase::GetNextUpdateSequence(const netPlayer& player, bool bIncrement) 
{ 
	gnetAssert(IsArrayLocallyArbitrated());
	gnetAssert(IsPlayerInScope(player));

	PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

	if (!gnetVerify(playerIndex != INVALID_PLAYER_INDEX))
	{
		return 0;
	}

	if (bIncrement)
	{
		m_LastSentSequences[playerIndex]++;
	}

	// 0 isn't a valid sequence number, as it is used to indicate there is no unacked sequence in the sync data
	if (m_LastSentSequences[playerIndex] == 0)
	{
		m_LastSentSequences[playerIndex]++;
	}

	return m_LastSentSequences[playerIndex];
}

void netArrayHandlerBase::SetLastReceivedUpdateSequence(const netPlayer& player, netSequence updateSeq) 
{ 
	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		m_LastReceivedSequences[player.GetPhysicalPlayerIndex()] = updateSeq; 
	}
}

bool netArrayHandlerBase::IsSyncedWithAllPlayers() const
{
	// this should only be called on the machine which has authority over the array
	gnetAssert(IsArrayLocallyArbitrated());

	if (GetSyncData())
	{
		if (!GetSyncData()->IsSyncedWithPlayers(GetRemotePlayersInScopeBitmask()))
		{
			return false;
		}
	}

	return true;
}

PlayerFlags netArrayHandlerBase::GetUnsyncedPlayers() const
{
	PlayerFlags unsyncedPlayers = 0;

	if (GetSyncData())
	{
		unsyncedPlayers = GetSyncData()->GetUnsyncedPlayers(GetRemotePlayersInScopeBitmask());
	}

	return unsyncedPlayers;
}

PlayerFlags netArrayHandlerBase::GetRemotePlayersInScopeBitmask() const
{
	PlayerFlags remotePlayersMask = netInterface::GetPlayerMgr().GetRemotePhysicalPlayersBitmask();
	return remotePlayersMask;
}

bool netArrayHandlerBase::IsElementSyncedWithPlayer(const unsigned index, unsigned playerSyncDataIndex) const
{
	bool bSynced = GetSyncData()->GetSyncDataUnit(index).IsSyncedWithPlayer(playerSyncDataIndex);

	return bSynced;
}

bool netArrayHandlerBase::IsElementSyncedWithAllPlayers(const unsigned index) const
{
	bool bSynced = GetSyncData()->GetSyncDataUnit(index).GetUnsyncedPlayers() == 0;

	return bSynced;
}

bool netArrayHandlerBase::CanProcessUpdate(const netPlayer& player, netSequence updateSeq, bool bSplitUpdate) const
{
	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		netSequence lastReceivedSeq = m_LastReceivedSequences[player.GetPhysicalPlayerIndex()];

		// if 0 is stored as the last sequence, this means that we haven't received any updates from this player yet. If the arbitration
		// of an array changes then this held sequence is reset to 0.
		if (lastReceivedSeq != 0)
		{
			if (bSplitUpdate)
			{
				// the parts of a split update all share the same update sequence, so accept an update sequence that is greater than or 
				// equivalent to our current one
				if (!netSeqGe(updateSeq, lastReceivedSeq))
				{
					netInterface::GetArrayManager().LogArrayElementsMsgRejection("A more recent split update has been received");
					return false;
				}
			}
			else if (!netSeqGt(updateSeq, lastReceivedSeq))
			{
				// only accept an update with a greater sequence than our current one
				netInterface::GetArrayManager().LogArrayElementsMsgRejection("A more recent update has been received");
				return false;
			}
		}
	}

	return true;
}

void netArrayHandlerBase::InitialiseSyncDataBuffers()
{
	// fill out the current state and shadow buffers with the current state of the array
	if (GetSyncData()->GetCurrentStateBuffer() && gnetVerify( GetSyncData()->GetShadowBuffer()))
	{
		datBitBuffer *currentStateBuffer = GetSyncData()->GetCurrentStateBuffer();
		datBitBuffer *shadowBuffer       = GetSyncData()->GetShadowBuffer();

#if __ASSERT
		unsigned targetBufferSizeInBits = m_MaxSizeOfElementInBits*m_NumArrayElements + m_ElementEmptyFlagsSize; // include guard bits for each element
		unsigned currentBufferSizeInBits = GetSyncData()->GetShadowBuffer()->GetMaxBits();

		gnetAssertf(currentBufferSizeInBits >= targetBufferSizeInBits && currentBufferSizeInBits <= targetBufferSizeInBits+8, "%s: Sync data buffers are incorrect size (%d) - they should be %d", GetHandlerName(), BITS_TO_BYTES(currentBufferSizeInBits), BITS_TO_BYTES(targetBufferSizeInBits));
#endif

        // empty flags are stored at the start of the buffers, flag data and element data is byte aligned
        // to allow faster buffer comparisons
        u32 currentEmptyFlags = 0;

        currentStateBuffer->SetNumBitsWritten(m_ElementEmptyFlagsSize);
        shadowBuffer->SetNumBitsWritten(m_ElementEmptyFlagsSize);

		bool canHaveEmptyElements = CanHaveEmptyElements();

		for (unsigned i=0; i<m_NumElementsInUse; i++)
		{
			if (IsElementLocallyArbitrated(i))
			{
                bool isElementEmpty = false;
				
				if (canHaveEmptyElements)
				{
					isElementEmpty = IsElementEmpty(i);

					if(isElementEmpty)
					{
						unsigned bit = i % EMPTY_FLAGS_BLOCK_SIZE;
						currentEmptyFlags |= (1<<bit);
					}
				}

				if (!isElementEmpty)
				{
                    unsigned elementDataCursorPos = GetElementCursorPos(i);
                    currentStateBuffer->SetCursorPos(elementDataCursorPos);
		            shadowBuffer->SetCursorPos(elementDataCursorPos);

					WriteElement(*currentStateBuffer, i);
					WriteElement(*shadowBuffer, i);

					int currentDataSize = currentStateBuffer->GetCursorPos() - elementDataCursorPos;

					GetSyncData()->GetSyncDataUnit(i).SetSizeOfCurrentData(currentDataSize);
					GetSyncData()->GetSyncDataUnit(i).SetSizeOfShadowData(currentDataSize);

                    // pad any extra data with zeroes (required so elements can be compared at the byte level)
                    unsigned remainingBits = m_MaxSizeOfElementInBits - currentDataSize;

                    while (remainingBits > 0)
                    {
						if (remainingBits <= 32)
						{
							currentStateBuffer->WriteUns(0, remainingBits);
							shadowBuffer->WriteUns(0, remainingBits);
							remainingBits = 0;
						}
						else
						{
							currentStateBuffer->WriteUns(0, 32);
							shadowBuffer->WriteUns(0, 32);
							remainingBits -= 32;
						}
                    }
				}
			}

            // write the empty flags to the buffers
			if (canHaveEmptyElements)
			{
				if(((i+1)%EMPTY_FLAGS_BLOCK_SIZE==0))
				{
					unsigned emptyFlagPos = i + 1 - EMPTY_FLAGS_BLOCK_SIZE;
					currentStateBuffer->SetCursorPos(emptyFlagPos);
					shadowBuffer->SetCursorPos(emptyFlagPos);
	                
					currentStateBuffer->WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
					shadowBuffer->WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);

					currentEmptyFlags = 0;
				}
			}

            currentStateBuffer->SetNumBitsWritten(GetElementCursorPos(i+1));
		    shadowBuffer->SetNumBitsWritten(GetElementCursorPos(i+1));
		}

        // write any remaining flags
		if (canHaveEmptyElements)
		{
			unsigned remainingFlags = m_ElementEmptyFlagsSize - m_NumElementsInUse;
		
			if(remainingFlags > 0)
			{
				unsigned emptyFlagPos = m_ElementEmptyFlagsSize - EMPTY_FLAGS_BLOCK_SIZE;
				currentStateBuffer->SetCursorPos(emptyFlagPos);
				shadowBuffer->SetCursorPos(emptyFlagPos);

				currentStateBuffer->WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
				shadowBuffer->WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
			}
		}
	}
}

void netArrayHandlerBase::ResetSyncData()
{
	for (unsigned i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
	{
		m_LastSentSequences[i]     = 0;
		m_LastReceivedSequences[i] = 0;
		m_UnackedSplitUpdateFlags[i] = 0;
	}

	m_unackedSplitUpdates = false;

	if (GetSyncData())
	{
		GetSyncData()->Reset();
	
		InitialiseSyncDataBuffers(); 
	}
}

void netArrayHandlerBase::ResetEmptyElements()
{
	// reset dirty state of all empty elements that are synced, to prevent them being sent out to any players that join subsequently
	if (CanHaveEmptyElements() && AllElementsStartEmpty())
	{
		bool bElementReset			= false;
		bool bSharedArray			= IsSharedArray();
		PlayerFlags playersInScope	= GetRemotePlayersInScopeBitmask();

		for (unsigned i=0; i<m_NumElementsInUse; i++)
		{
			bool elementDirty = false;

			if (bSharedArray)
			{
				// shared arrays can have elements that have not been dirtied (because they were not in scope with other players), but still need
				// to have their arbitrator cleared when they become empty
				elementDirty = IsElementLocallyArbitrated(i);
			}
			else
			{
				elementDirty = GetSyncData()->HasSyncDataUnitEverBeenDirtied(i);
			}

			if (elementDirty && IsElementEmpty(i))
			{
#if ENABLE_NETWORK_LOGGING
				netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
				char logStr[40];
#endif // ENABLE_NETWORK_LOGGING

				if (GetSyncData()->GetSyncDataUnit(i).IsSyncedWithPlayers(playersInScope))
				{
#if ENABLE_NETWORK_LOGGING
					formatf(logStr, sizeof(logStr), "RESETTING_EMPTY_ELEMENT_%d", i);
					NetworkLogUtils::WriteLogEvent(log, logStr, "%s", GetHandlerName());

					if (GetIdentifier())
					{
						log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
					}
#endif // ENABLE_NETWORK_LOGGING

					ResetElementSyncData(i);

					bElementReset = true;
				}
				else
				{
#if ENABLE_NETWORK_LOGGING
					formatf(logStr, sizeof(logStr), "PENDING_RESET_EMPTY_ELEMENT_%d", i);
					NetworkLogUtils::WriteLogEvent(log, logStr, "%s", GetHandlerName());

					if (GetIdentifier())
					{
						log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
					}

					log.WriteDataValue("Unsynced players", "%u", GetSyncData()->GetSyncDataUnit(i).GetUnsyncedPlayers());
					log.WriteDataValue("Players in scope", "%u", playersInScope);
#endif // ENABLE_NETWORK_LOGGING
				}
			}
		}

		if (bElementReset)
		{
			// update sync data so that the unsynced state matches that of the sync units
			GetSyncData()->Update();
		}
	}
}

unsigned netArrayHandlerBase::GetSyncDataIndexForPlayer(const netPlayer& player) const
{
	return player.GetPhysicalPlayerIndex();
}

bool netArrayHandlerBase::SetElementDirty(unsigned index)
{
#if ENABLE_NETWORK_LOGGING
	netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
	char logStr[30];
	formatf(logStr, sizeof(logStr), "SETTING_ELEMENT_%d_DIRTY", index);
	NetworkLogUtils::WriteLogEvent(log, logStr, "%s", GetHandlerName());

	if (GetIdentifier())
	{
		log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
	}

	if (IsElementEmpty(index))
	{
		log.WriteDataValue("Element empty", "true");

		if (IsSharedArray() && GetSyncData())
		{
			log.WriteDataValue("Unsynced players", "%u", GetSyncData()->GetSyncDataUnit(index).GetUnsyncedPlayers());
		}
	}
	else if (!IsSharedArray()) // can't log dirty elements for shared arrays because this may get called during an arbitration conflict, wiping out the element ready to be applied
	{
		LogElement(index, log);
	}
	else
	{
		log.WriteDataValue("Element empty", "false");
	}

	if (HasUnackedSplitUpdates())
	{
		log.WriteDataValue("FAILED", "Unacked split updates");
	}

#endif // ENABLE_NETWORK_LOGGING

	if (HasUnackedSplitUpdates())
	{
		gnetAssertf(0, "%s (%s): DirtyElement %d failed - waiting on split update acks", GetHandlerName(), GetIdentifier() ? GetIdentifier()->GetLogName() : "", index);
		return false;
	}

	Assertf(!IsElementRemotelyArbitrated(index), "We are trying to alter an array element that is arbitrated by another machine");

	if (gnetVerify(GetSyncData()))
	{
		if (AreElementsAlwaysInScope())
		{
			LOGGING_ONLY(log.WriteDataValue("Set dirty for all players", "true"));
			GetSyncData()->SetSyncDataUnitDirty(index);
		}
		else
		{
			// reset sync data unit dirty state, which may have some unsynced flags still set 
			GetSyncData()->ClearSyncDataUnitDirty(index);

			// only dirty the element for players in scope to prevent the handler trying to send updates to players out of scope
			unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
            const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	        for(unsigned p = 0; p < numRemotePhysicalPlayers; p++)
            {
		        const netPlayer *player = remotePhysicalPlayers[p];

				if (IsElementInScopeInternal(index, *player))
				{
					LOGGING_ONLY(log.WriteDataValue("Set dirty for player", "%s", player->GetLogName()));
					GetSyncData()->SetSyncDataUnitDirtyForPlayer(index, GetSyncDataIndexForPlayer(*player));
				}
			}
		}
	}

	return true;
}

void netArrayHandlerBase::DirtyAllElements()
{
	if (gnetVerify(IsArrayLocallyArbitrated()))
	{
		for (unsigned i=0; i<m_NumElementsInUse; i++)
		{
			if (IsElementLocallyArbitrated(i))
			{
				SetElementDirty(i);
			}
		}
	}
}

bool netArrayHandlerBase::IsManualDirtyArray() 
{ 
	return !(GetSyncData() && GetSyncData()->GetCurrentStateBuffer()); 
}

void netArrayHandlerBase::HandleChangeOfArbitration(bool bHadPreviousArbitrator)
{
	// When arbitration becomes local we do not want to send out the entire state of the array to all other players, or this can cause a huge bandwidth spike.
	// We assume all client players are up to date with the current state of the array. Each client will send out a checksum of the current state of 
	// the array to the new arbitrator, who will only send an update for the array to a client if his checksum differs.
	if (bHadPreviousArbitrator)
	{
		if (IsArrayLocallyArbitrated())
		{
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
			NetworkLogUtils::WriteLogEvent(log, "HANDLE_ARBITRATION_CHANGE", GetHandlerName());

			if (GetIdentifier())
			{
				log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
			}

			// Reset the sync data buffers to prevent every dirty element being sent out again to other players. Only the elements that are 
			// dirtied in future will be sent out. Players entering the session will still receive the previously dirtied elements as they 
			// will be flagged in the last dirty times in the sync data.
			InitialiseSyncDataBuffers();

			// clear the dirty state for all player data 
			for (unsigned i=0; i<m_NumElementsInUse; i++)
			{
				for (unsigned player = 0; player < GetSyncData()->GetMaxNumSyncedPlayers(); player++)
				{
					GetSyncData()->GetSyncDataUnit(i).ClearDirtyForPlayer(player);
				}
			}

			GetSyncData()->SetSyncedWithAllPlayers();

			if (HasArrayData())
			{
				m_arrayDataChecksum = CalculateChecksum();
				m_checksumCalculated = true;
			}
		}
		else if (GetPlayerArbitratorOfArray())
		{
			if (AssertVerify(GetPlayerArbitratorOfArray()->GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
			{
				// reset last received seq number ready for updates from the new arbitrator
				m_LastReceivedSequences[GetPlayerArbitratorOfArray()->GetPhysicalPlayerIndex()] = 0;
			}

			// send a checksum of the current array data to the new arbitrator
			if (HasArrayData())
			{
				SendChecksumToNewArbitrator();
			}
		}
	}

	netInterface::GetArrayManager().ArrayHandlerArbitrationChanged(this);
}

bool netArrayHandlerBase::VerifyChecksum(const netPlayer& player, unsigned checksum)
{
	netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
	NetworkLogUtils::WriteLogEvent(log, "VERIFY_CHECKSUM", GetHandlerName());

	if (GetIdentifier())
	{
		log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
	}

	log.WriteDataValue("From player", player.GetLogName());

	if (!m_checksumCalculated)
	{
		m_arrayDataChecksum = CalculateChecksum();
	}

	bool ignoreEmptyElements = CanHaveEmptyElements() && AllElementsStartEmpty();

	if (checksum != m_arrayDataChecksum && AssertVerify(GetSyncData()))
	{
		log.WriteDataValue("Checksum differs", "true");

		for (unsigned i=0; i<m_NumElementsInUse; i++)
		{
			if (GetSyncData()->HasSyncDataUnitEverBeenDirtied(i))
			{
				// ignore empty elements for some arrays
				if (!(ignoreEmptyElements && IsElementEmpty(i)))
				{
					unsigned playerSyncDataIndex = GetSyncDataIndexForPlayer(player);

					if (AssertVerify(playerSyncDataIndex < MAX_NUM_PHYSICAL_PLAYERS))
					{
						GetSyncData()->SetSyncDataUnitDirtyForPlayer(i, playerSyncDataIndex);
					}
				}
			}
		}

		return true;
	}
	else
	{
		log.WriteDataValue("Checksum differs", "false");
	}

	return false;
}

void netArrayHandlerBase::SetUnackedSplitUpdateFlags(const netPlayer& player, u32 numUpdates)
{
	if (CacheSplitUpdates())
	{
		// the update data does not fit in one packet and so must be split into a number of separate packets. Flag each of these so they can be sent one at a time to the player.
		PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

		if (numUpdates > 1)
		{
			if (gnetVerify(playerIndex != INVALID_PLAYER_INDEX) && m_UnackedSplitUpdateFlags[playerIndex] == 0)
			{
				for (u32 i=0; i<numUpdates; i++)
				{
					m_UnackedSplitUpdateFlags[playerIndex] |= (1<<i);
				}

#if ENABLE_NETWORK_LOGGING
				netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
				NetworkLogUtils::WriteLogEvent(log, "SETTING_UNACKED_SPLIT_UPDATE_FLAGS", "%s", GetHandlerName());

				if (GetIdentifier())
				{
					log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
				}

				log.WriteDataValue("Player", player.GetLogName());
				log.WriteDataValue("Flags", "%d", (int)m_UnackedSplitUpdateFlags[playerIndex]);
#endif		
				m_unackedSplitUpdates = true;
			}
		}
		else if (m_UnackedSplitUpdateFlags[playerIndex] != 0)
		{
			gnetAssertf(0, "%s (%s) has unacked split update flags for %s (%d) but GetTotalSizeOfUpdateData() has now calculated a single update", GetHandlerName(), GetIdentifier() ? GetIdentifier()->GetLogName() : "", player.GetLogName(), m_UnackedSplitUpdateFlags[playerIndex]);
			ResetUnackedSplitUpdateFlagsForPlayer(player);
		}
	}
}

SplitUpdateFlags netArrayHandlerBase::GetUnackedSplitUpdateFlags(const netPlayer& player) const 
{ 
	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		return m_UnackedSplitUpdateFlags[player.GetPhysicalPlayerIndex()]; 
	}

	return 0;
}

void netArrayHandlerBase::ResetUnackedSplitUpdateFlagsForPlayer(const netPlayer& player)
{
	if (gnetVerify(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX))
	{
		m_UnackedSplitUpdateFlags[player.GetPhysicalPlayerIndex()] = 0; 
	}

	if (m_unackedSplitUpdates)
	{
		m_unackedSplitUpdates = false;

		for (int i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
		{
			if (m_UnackedSplitUpdateFlags[i] != 0)
			{
				m_unackedSplitUpdates = true;
			}
		}
	}
#if ENABLE_NETWORK_LOGGING
	netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
	NetworkLogUtils::WriteLogEvent(log, "RESET_UNACKED_SPLIT_UPDATE_FLAGS", "%s", GetHandlerName());

	if (GetIdentifier())
	{
		log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
	}

	log.WriteDataValue("Player", player.GetLogName());
#endif		
}

void netArrayHandlerBase::WriteElementIndex(datBitBuffer& bitBuffer, unsigned index)
{
	bitBuffer.WriteUns(index, m_BitsizeOfIndex);
}

void netArrayHandlerBase::ReadElementIndex(datBitBuffer& bitBuffer, unsigned& index)
{
	bitBuffer.ReadUns(index, m_BitsizeOfIndex);
}

bool netArrayHandlerBase::IsValidIndex(unsigned index)
{
	return index < m_NumElementsInUse;
}

void netArrayHandlerBase::RecalculateDirtyElements()
{
	if (gnetVerify(IsArrayLocallyArbitrated()) && 
		gnetVerify(!IsManualDirtyArray()))
	{
		if (HasUnackedSplitUpdates())
		{
#if ENABLE_NETWORK_LOGGING
			netLoggingInterface &log = netInterface::GetArrayManager().GetLog();
			NetworkLogUtils::WriteLogEvent(log, "WAITING_ON_SPLIT_UPDATE_ACKS", "%s", GetHandlerName());

			if (GetIdentifier())
			{
				log.WriteDataValue("Identifier", GetIdentifier()->GetLogName());
			}

			// don't recalculate dirty elements if the handler is waiting for split update acks. We want to make sure that all players have received the split updates before sending
			// any new ones (otherwise we will need to resend the split updates)
			for (u32 i=0; i<MAX_NUM_PHYSICAL_PLAYERS; i++)
			{
				if (m_UnackedSplitUpdateFlags[i] && gnetVerify(CacheSplitUpdates()))
				{
					netPlayer* pPlayer = netInterface::GetPhysicalPlayerFromIndex((PhysicalPlayerIndex)i);

					if (pPlayer)
					{
						log.WriteDataValue("Player", pPlayer->GetLogName());
					}
					else
					{
						log.WriteDataValue("*Non existent player*", "%d", i);
					}

					log.WriteDataValue("Flags", "%d", (int)m_UnackedSplitUpdateFlags[i]);
				}
			}
#endif
			return;
		}

        const bool isSharedArray = IsSharedArray();

		datBitBuffer& currentStateBuffer	= *GetSyncData()->GetCurrentStateBuffer();
		datBitBuffer& shadowBuffer			= *GetSyncData()->GetShadowBuffer();

        u32 previousEmptyFlags = 0;
        u32 currentEmptyFlags  = 0;

		bool canHaveEmptyElements = CanHaveEmptyElements();

		for (unsigned i=0; i<m_NumElementsInUse; i++)
		{
			gnetAssertf(canHaveEmptyElements || !IsElementEmpty(i), "Array %s has an empty element but has not defined CanHaveEmptyElements() to return true", GetHandlerName());

            if(canHaveEmptyElements && (i % EMPTY_FLAGS_BLOCK_SIZE) == 0)
            {
                shadowBuffer.SetCursorPos(i);
                shadowBuffer.ReadUns(previousEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
                currentEmptyFlags = 0;
            }

			if (!isSharedArray || IsElementLocallyArbitrated(i))
			{
                unsigned bit = i % EMPTY_FLAGS_BLOCK_SIZE;
                bool isElementEmpty  = false;
				bool elementDirty = false;
				
				if (canHaveEmptyElements)
				{
					isElementEmpty = IsElementEmpty(i);
					bool wasElementEmpty = (previousEmptyFlags & (1<<bit)) != 0;

					elementDirty = isElementEmpty != wasElementEmpty;
				
					if(isElementEmpty)
					{
						currentEmptyFlags |= (1<<bit);
					}
				}

				if (!isElementEmpty)
				{
					netSyncDataUnitBase& dataUnit = GetSyncData()->GetSyncDataUnit(i);

                    int elementDataCursorPos = GetElementCursorPos(i);
			        currentStateBuffer.SetCursorPos(elementDataCursorPos);
					WriteElement(currentStateBuffer, i);

                    unsigned currentStateDataSize = currentStateBuffer.GetCursorPos() - elementDataCursorPos;
					dataUnit.SetSizeOfCurrentData(currentStateDataSize);

                    // pad any extra data with zeroes (required so elements can be compared at the byte level)
                    unsigned remainingBits = m_MaxSizeOfElementInBits - currentStateDataSize;

					while (remainingBits > 0)
					{
						if (remainingBits <= 32)
						{
							currentStateBuffer.WriteUns(0, remainingBits);
							remainingBits = 0;
						}
						else
						{
							currentStateBuffer.WriteUns(0, 32);
							remainingBits -= 32;
						}
					}

					gnetAssertf(currentStateBuffer.GetCursorPos() <= static_cast<int>(GetElementCursorPos(i+1)), "%s: WriteElement wrote more data than expected", GetHandlerName());

					if (!elementDirty &&
						((dataUnit.GetSizeOfCurrentData() != dataUnit.GetSizeOfShadowData()) || 
						CompareElement(currentStateBuffer, shadowBuffer, elementDataCursorPos>>3)))
					{
						elementDirty = true;
					}

					if (elementDirty)
					{
						// copy current state to shadow buffer
						shadowBuffer.SetCursorPos(elementDataCursorPos);

						Assert(elementDataCursorPos%8 == 0);
						Assert(m_MaxSizeOfElementInBits%8 == 0);

						GetSyncData()->CopyCurrentStateToShadowState(elementDataCursorPos>>3, m_MaxSizeOfElementInBits>>3);

						dataUnit.SetSizeOfShadowData(currentStateDataSize);
					}
				}

				if (elementDirty)
				{
					SetElementDirty(i);
				}
			}

			if (canHaveEmptyElements)
			{
			    if(((i+1) % EMPTY_FLAGS_BLOCK_SIZE) == 0)
				{
					const unsigned lastFlagsPos = i+1-EMPTY_FLAGS_BLOCK_SIZE;
					shadowBuffer.SetCursorPos(lastFlagsPos);
					currentStateBuffer.SetCursorPos(lastFlagsPos);
					shadowBuffer.WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
					currentStateBuffer.WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
				}
			}
		}

        // write any remaining flags
		if (canHaveEmptyElements)
		{
		    unsigned remainingFlags = m_ElementEmptyFlagsSize - m_NumElementsInUse;
			if(remainingFlags > 0)
			{
				unsigned emptyFlagPos = m_ElementEmptyFlagsSize - EMPTY_FLAGS_BLOCK_SIZE;
				currentStateBuffer.SetCursorPos(emptyFlagPos);
				shadowBuffer.SetCursorPos(emptyFlagPos);

				currentStateBuffer.WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
				shadowBuffer.WriteUns(currentEmptyFlags, EMPTY_FLAGS_BLOCK_SIZE);
			}
		}
	}
}

void netArrayHandlerBase::ResetElementSyncData(unsigned index)
{
    if(gnetVerifyf(index < m_NumElementsInUse, "Resetting sync data for an out of range element!") && GetSyncData())
    {
		GetSyncData()->ClearSyncDataUnitDirty(index);
    }
}

bool netArrayHandlerBase::CanApplyElementData(unsigned index, const netPlayer& UNUSED_PARAM(player), bool UNUSED_PARAM(elementEmpty))  
{ 
	if (index >= m_NumElementsInUse)
	{
		gnetAssertf(0, "Received an array handler update for array %s with an invalid array element", GetHandlerName());
		return false;
	}
	return true; 
}

unsigned netArrayHandlerBase::CalcHash(const char* buffer, unsigned dataSize) const
{
	return atDataHash(buffer, dataSize);
}

void netArrayHandlerBase::SendChecksumToNewArbitrator()
{
	// arrays with identifiers need to send a different event that contains the identifier (see netHostBroadcastDataHandlerBase)
	gnetAssert(GetIdentifier() == NULL);
	arrayDataVerifyEvent::Trigger(GetHandlerType(), CalculateChecksum());
}

bool netArrayHandlerBase::CompareElement(datBitBuffer &currentStateBuffer, datBitBuffer &shadowBuffer, unsigned elementBufferByteOffset)
{
    unsigned elementByteSize = m_MaxSizeOfElementInBits>>3;

    const u8 *currentData = (const u8 *)currentStateBuffer.GetReadOnlyBits();
    const u8 *shadowData  = (const u8 *)shadowBuffer.GetReadOnlyBits();

    return memcmp(currentData + elementBufferByteOffset, shadowData + elementBufferByteOffset, elementByteSize) != 0;
}

} // namespace rage

