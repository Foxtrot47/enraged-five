// 
// netbandwidthmanager.cpp 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#include "netbandwidthmgr.h"

#include "fwsys/timer.h"
#include "net/connectionmanager.h"

#include "fwnet/netinterface.h"
#include "fwnet/netplayer.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/netchannel.h"
#include "fwnet/optimisations.h"
#include "math/amath.h"

NETWORK_OPTIMISATIONS()

PARAM(nobandwidththrottling, "[network] No bandwidth throttling");
XPARAM(additionalBandwidthLogging);

namespace rage
{

bank_u32 netBandwidthMgr::BANDWIDTH_ADVANCE_INTERVAL  = 10000; // time interval before the target bandwidth can be increased
bank_u32 netBandwidthMgr::RELIABLE_RESEND_THRESHOLD   = 5;     // number of unacked reliable messages before we start throttling bandwidth
bank_u32 netBandwidthMgr::UNRELIABLE_RESEND_THRESHOLD = 20;    // number of unacked unreliable messages before we start throttling bandwidth

netBandwidthMgr::netBandwidthMgr()
: m_ConnectionMgr(0)
, m_TargetTotalUpstreamBandwidth(0)
, m_TargetGameUpstreamBandwidth(0)
, m_LastUpdate(0)
, m_LastTargetAdjustment(0)
, m_SendInterval(DEFAULT_SEND_INTERVAL_HIGH)
, m_NumFailuresPerUpdate(0)
, m_LastTimeAverageFailuresNonZero(0)
, m_AverageFailuresPerUpdate(0)
, m_CachedFailurePos(0)
, m_PlayerBandwidthInfo(MAX_NUM_PHYSICAL_PLAYERS)
, m_NumUnreliableResends(MAX_NUM_PHYSICAL_PLAYERS)
, m_CachedFailuresPerSync(NUM_SYNC_FAILURES_TO_CACHE)
, m_PlayerThrottleData(MAX_NUM_PHYSICAL_PLAYERS)
, m_Log(0)
#if __BANK
, m_NumBandwidthStats(0)
, m_BandwidthStats(MAX_STATS)
, m_OverridePlayerBandwidthTargets(false)
, m_OverrideTargetUpstreamBandwidth(false)
, m_OverriddenTargetUpstreamBandwidth(MAXIMUM_BANDWIDTH)
, m_LastBandwidthSampleTime(0)
#endif // __BANK
, m_AllowBandwidthThrottling(false)
{
    m_Dlgt.Bind(this, &netBandwidthMgr::OnMessageReceived);
}

netBandwidthMgr::~netBandwidthMgr()
{
}

void netBandwidthMgr::Init(netConnectionManager *connectionMgr)
{
    m_ConnectionMgr = connectionMgr;
	SetTargetUpstreamBandwidth(MAXIMUM_BANDWIDTH);
	m_LastUpdate           = fwTimer::GetSystemTimeInMilliseconds();
	m_LastTargetAdjustment = fwTimer::GetSystemTimeInMilliseconds();
	m_SendInterval        = DEFAULT_SEND_INTERVAL_HIGH;

    netInterface::GetPlayerMgr().AddDelegate(&m_Dlgt);

	const unsigned LOG_BUFFER_SIZE = 16 *1024;
	static bool s_HasCreatedLog = false; 
	m_Log = rage_new netLog("BandwidthManager.log", s_HasCreatedLog ? LOGOPEN_APPEND : LOGOPEN_CREATE, netInterface::GetDefaultLogFileTargetType(),
                                                                    netInterface::GetDefaultLogFileBlockingMode(), LOG_BUFFER_SIZE);
	s_HasCreatedLog = true; 

	ResetPlayerBandwidthData();
	ResetAverageFailuresPerUpdate();
	ResetUnreliableResends();

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_PlayerThrottleData[index].m_LastMessageReceivedTime = 0;
        m_PlayerThrottleData[index].m_PreventThrottleTimer    = 0;
    }

    // don't allow bandwidth throttling in dev builds or if it has been
    // disabled by the command line
#if !__DEV
    if(PARAM_nobandwidththrottling.Get())
#endif // !__DEV
    {
        m_AllowBandwidthThrottling = false;
    }

#if __BANK
	for (unsigned i=0; i<MAX_STATS; i++)
	{
		m_BandwidthStats[i] = NULL;
	}

	m_NumBandwidthStats = 0;

	// we are using manual sampling for these packet recorders
	m_socketRecorder.SetSampleInterval(0);
	m_gameChannelRecorder.SetSampleInterval(0);
    m_sessionVoiceChannelRecorder.SetSampleInterval(0);
    m_sessionActivityChannelRecorder.SetSampleInterval(0);
    m_sessionGameChannelRecorder.SetSampleInterval(0);
	m_voiceChannelRecorder.SetSampleInterval(0);

	if (m_ConnectionMgr)
	{
		
		if (gnetVerify(m_ConnectionMgr->GetSocketManager()))
		{
			m_ConnectionMgr->GetSocketManager()->GetMainSocket()->RegisterPacketRecorder(&m_socketRecorder);
		}

		m_ConnectionMgr->RegisterChannelPacketRecorder(NETWORK_GAME_CHANNEL_ID,		        &m_gameChannelRecorder);
		m_ConnectionMgr->RegisterChannelPacketRecorder(NETWORK_SESSION_VOICE_CHANNEL_ID,	&m_sessionVoiceChannelRecorder);
        m_ConnectionMgr->RegisterChannelPacketRecorder(NETWORK_SESSION_ACTIVITY_CHANNEL_ID,	&m_sessionActivityChannelRecorder);
        m_ConnectionMgr->RegisterChannelPacketRecorder(NETWORK_SESSION_GAME_CHANNEL_ID,	    &m_sessionGameChannelRecorder);
        m_ConnectionMgr->RegisterChannelPacketRecorder(NETWORK_VOICE_CHANNEL_ID,	        &m_voiceChannelRecorder);
    }

	netBandwidthStatistics::ResetStaticData();

#endif // __BANK
}

#define BW_ADVANCE(x)   (((x)*3)/2)
#define BW_THROTTLE(x)  (((x)*3)/4)

void netBandwidthMgr::Update()
{
	UpdatePlayerBandwidthInfo();

#if __BANK
    static const unsigned BANDWIDTH_COLLECTOR_SAMPLE_INTERVAL = 1000;

    const unsigned currTime    = fwTimer::GetSystemTimeInMilliseconds();
    const unsigned timeElapsed = (currTime > m_LastBandwidthSampleTime) ? (currTime - m_LastBandwidthSampleTime) : 0;

    if(timeElapsed > BANDWIDTH_COLLECTOR_SAMPLE_INTERVAL)
    {
	    SampleData(currTime);

        m_LastBandwidthSampleTime = currTime;
    }
#endif

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        if(m_PlayerThrottleData[index].m_PreventThrottleTimer > 0)
        {
            m_PlayerThrottleData[index].m_PreventThrottleTimer -= rage::Min(m_PlayerThrottleData[index].m_PreventThrottleTimer, fwTimer::GetSystemTimeStepInMilliseconds());
        }
    }

	const unsigned curTime = fwTimer::GetSystemTimeInMilliseconds();

	if((curTime - m_LastUpdate) > UPDATE_INTERVAL)
	{
		m_LastUpdate = curTime;

		const unsigned curBandwidth = netBandwidthMgr::GetTargetUpstreamBandwidth();

		unsigned targetBandwidth = curBandwidth;

		bool advanceBandwidth = true;

		unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();

	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
		    const netPlayer *remotePlayer = remotePhysicalPlayers[index];

			if(GetBandwidthThrottlingRequired(*remotePlayer))
			{
				//We're likely exceeding our bandwidth capacity - throttle
				//it back.
				targetBandwidth  = BW_THROTTLE(curBandwidth);
				advanceBandwidth = false;

#if __BANK
                // check if throttling is required for any other players so we get the debug logging
                for(unsigned index2 = index + 1; index2 < numRemotePhysicalPlayers; index2++)
                {
                    remotePlayer = remotePhysicalPlayers[index2];

                    GetBandwidthThrottlingRequired(*remotePlayer);
                }
#endif // __BANK
				break;
			}
			else if(!GetBandwidthCanBeAdvanced(*remotePlayer))
			{
				advanceBandwidth = false;
			}
		}

		if(advanceBandwidth)
		{
			targetBandwidth = BW_ADVANCE(curBandwidth);
		}

		if(targetBandwidth != curBandwidth)
		{
			if(targetBandwidth < GetMinimumBandwidth())
			{
				targetBandwidth = GetMinimumBandwidth();
			}
			else if(targetBandwidth > MAXIMUM_BANDWIDTH)
			{
				targetBandwidth = MAXIMUM_BANDWIDTH;
			}

			// give the changes some time to take effect
			if((curTime - m_LastTargetAdjustment) > BANDWIDTH_ADVANCE_INTERVAL)
			{
				netBandwidthMgr::SetTargetUpstreamBandwidth(targetBandwidth);

				m_LastTargetAdjustment = curTime;
			}
		}

		ResetPlayerBandwidthData();

		m_CachedFailuresPerSync[m_CachedFailurePos] = m_NumFailuresPerUpdate;
		m_CachedFailurePos++;
		m_CachedFailurePos%=NUM_SYNC_FAILURES_TO_CACHE;
		m_NumFailuresPerUpdate = 0;

		RecalculateSyncFailureAverage();
		ResetUnreliableResends();
	}

#if __BANK
	if(m_OverrideTargetUpstreamBandwidth)
	{
		SetTargetUpstreamBandwidth(m_OverriddenTargetUpstreamBandwidth);
	}
#endif
}

void netBandwidthMgr::Shutdown()
{
	if (m_Log)
	{
		delete m_Log;
		m_Log = 0;
	}

    netInterface::GetPlayerMgr().RemoveDelegate(&m_Dlgt);

#if __BANK
	if (m_ConnectionMgr)
	{
		if (gnetVerify(m_ConnectionMgr->GetSocketManager()))
		{
			m_ConnectionMgr->GetSocketManager()->GetMainSocket()->UnregisterPacketRecorder(&m_socketRecorder);
		}

		m_ConnectionMgr->UnregisterChannelPacketRecorder(NETWORK_GAME_CHANNEL_ID,	            &m_gameChannelRecorder);
		m_ConnectionMgr->UnregisterChannelPacketRecorder(NETWORK_SESSION_VOICE_CHANNEL_ID,      &m_sessionVoiceChannelRecorder);
        m_ConnectionMgr->UnregisterChannelPacketRecorder(NETWORK_SESSION_ACTIVITY_CHANNEL_ID,   &m_sessionActivityChannelRecorder);
        m_ConnectionMgr->UnregisterChannelPacketRecorder(NETWORK_SESSION_GAME_CHANNEL_ID,       &m_sessionGameChannelRecorder);
        m_ConnectionMgr->UnregisterChannelPacketRecorder(NETWORK_VOICE_CHANNEL_ID,              &m_voiceChannelRecorder);
	}
#endif // __BANK
}

void netBandwidthMgr::SetSendInterval(unsigned sendInterval)
{
    Assert(m_ConnectionMgr);

    m_SendInterval = sendInterval;

    m_ConnectionMgr->SetGlobalSendInterval(m_SendInterval);
}

void netBandwidthMgr::IncreaseUnreliableResendCount(PhysicalPlayerIndex playerIndex)
{
	m_NumUnreliableResends[playerIndex]++;
}

unsigned netBandwidthMgr::GetUnreliableResendCount(const netPlayer& player) const
{
    if(player.GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX)
    {
        return 0;
    }

	return m_NumUnreliableResends[player.GetPhysicalPlayerIndex()];
}

void netBandwidthMgr::PlayerHasJoined(const netPlayer& player)
{
    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

    if(playerIndex != INVALID_PLAYER_INDEX)
    {
        m_PlayerThrottleData[playerIndex].m_LastMessageReceivedTime = 0;
        m_PlayerThrottleData[playerIndex].m_PreventThrottleTimer    = 0;
    }

    UpdateGameDataTargetBandwidth();
}

void netBandwidthMgr::PlayerHasLeft(const netPlayer& player)
{
    PhysicalPlayerIndex playerIndex = player.GetPhysicalPlayerIndex();

    if(playerIndex != INVALID_PLAYER_INDEX)
    {
        m_PlayerThrottleData[playerIndex].m_LastMessageReceivedTime = 0;
        m_PlayerThrottleData[playerIndex].m_PreventThrottleTimer    = 0;
    }

    UpdateGameDataTargetBandwidth();
}

void netBandwidthMgr::SetTargetUpstreamBandwidth(const unsigned bandwidth)
{
    if(bandwidth != m_TargetTotalUpstreamBandwidth)
    {
        gnetDebug1("Setting target outbound bandwidth to:%d", bandwidth);

        gnetAssertf(bandwidth >= GetMinimumBandwidth(), "Setting target upstream bandwidth too low!");
        gnetAssertf(bandwidth <= MAXIMUM_BANDWIDTH,     "Setting target upstream bandwidth too high!");

        m_TargetTotalUpstreamBandwidth = bandwidth;

        UpdateGameDataTargetBandwidth();

        if(bandwidth <= LOW_SEND_INTERVAL_THRESHOLD)
        {
            SetSendInterval(DEFAULT_SEND_INTERVAL_LOW);
        }
        else
        {
            SetSendInterval(DEFAULT_SEND_INTERVAL_HIGH);
        }

        OnTargetUpstreamBandwidthChanged(bandwidth);
    }
}

unsigned
netBandwidthMgr::GetTargetUpstreamBandwidth() const
{
    return m_TargetTotalUpstreamBandwidth;
}

void netBandwidthMgr::AddBandwidthOut(const netPlayer& player, unsigned bandwidthOut)
{
    if(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
    {
 	    m_PlayerBandwidthInfo[player.GetPhysicalPlayerIndex()].m_CurrentBandwidth += bandwidthOut;
    }
}

void netBandwidthMgr::SampleData(unsigned BANK_ONLY(currentTime))
{
#if __BANK
	m_socketRecorder.SampleData(currentTime);
	m_gameChannelRecorder.SampleData(currentTime);
	m_sessionVoiceChannelRecorder.SampleData(currentTime);
    m_sessionActivityChannelRecorder.SampleData(currentTime);
    m_sessionGameChannelRecorder.SampleData(currentTime);
    m_voiceChannelRecorder.SampleData(currentTime);

	for (unsigned i=0; i<m_NumBandwidthStats; i++)
	{
		m_BandwidthStats[i]->SampleData(currentTime);
	}

	netBandwidthStatistics::SampleGrandTotalsData(currentTime);
#endif
}

bool netBandwidthMgr::CanSendData(const netPlayer& player, unsigned bandwidthOut) const
{
	Assert(!player.IsMyPlayer());

    if(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
    {
        unsigned currentBandwidth = m_PlayerBandwidthInfo[player.GetPhysicalPlayerIndex()].m_CurrentBandwidth;
        unsigned targetBandwidth  = m_PlayerBandwidthInfo[player.GetPhysicalPlayerIndex()].m_TargetBandwidth;

	    if((currentBandwidth + bandwidthOut) <= targetBandwidth)
        {
		    return true;
        }

        gnetDebug2("Failed to send data to %s: Bandwidth Exceeded: Allocated: %d, Required: %d, Remaining: %d", player.GetLogName(), targetBandwidth, bandwidthOut, (targetBandwidth > currentBandwidth) ? (targetBandwidth - currentBandwidth) : 0);

        m_NumFailuresPerUpdate++;
    }

    return false;
}

unsigned netBandwidthMgr::GetMinimumBandwidth() const
{
    static const unsigned int SMALL_GAME_THRESHOLD = 4;

    const unsigned numPlayers = netInterface::GetNumPhysicalPlayers();

    if(numPlayers > SMALL_GAME_THRESHOLD)
    {
        return MINIMUM_BANDWIDTH_LARGE_SESSION;
    }
    else
    {
        return MINIMUM_BANDWIDTH_SMALL_SESSION;
    }
}

void netBandwidthMgr::ResetAverageFailuresPerUpdate()
{
    for(unsigned index = 0; index < NUM_SYNC_FAILURES_TO_CACHE; index++)
    {
        m_CachedFailuresPerSync[index] = 0;
    }

    m_NumFailuresPerUpdate           = 0;
    m_AverageFailuresPerUpdate       = 0;
    m_LastTimeAverageFailuresNonZero = 0;
    m_CachedFailurePos               = 0;
}

unsigned netBandwidthMgr::GetTimeSinceAverageFailuresNonZero() const
{
    return fwTimer::GetSystemTimeInMilliseconds() - m_LastTimeAverageFailuresNonZero;
}

bool netBandwidthMgr::GetBandwidthThrottlingRequired(const netPlayer& player, unsigned throttleCheckFlags) const
{
    if(!m_AllowBandwidthThrottling)
		return false;

	if(!m_ConnectionMgr->IsOpen(player.GetConnectionId()))
		return false;

    if(player.GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX || !m_PlayerThrottleData[player.GetPhysicalPlayerIndex()].ConsiderForThrottling())
        return false;

    bool throttleBandwidth = false;
    bool checkReliables    = (throttleCheckFlags & BANDWIDTH_THROTTLE_CHECK_RELIABLES)   != 0;
    bool checkUnreliables  = (throttleCheckFlags & BANDWIDTH_THROTTLE_CHECK_UNRELIABLES) != 0;
    
    if(checkReliables && GetReliableMessagesHaveBeenUnackedTooLong(player.GetConnectionId()))
    {
        gnetDebug2("Bandwidth throttling required due to %d unacked reliable messages to %s", m_ConnectionMgr->GetOldestResendCount(player.GetConnectionId()), player.GetLogName());
        throttleBandwidth = true;
    }

    if(checkUnreliables && GetUnreliableMessagesHaveBeenUnackedTooLong(player))
    {
        gnetDebug2("Bandwidth throttling required due to %d unacked unreliable messages to %s", GetUnreliableResendCount(player), player.GetLogName());
        throttleBandwidth = true;
    }

    return throttleBandwidth;
}

bool netBandwidthMgr::GetBandwidthCanBeAdvanced(const netPlayer& player) const
{
    Assert(m_ConnectionMgr);

    if(!m_ConnectionMgr->IsOpen(player.GetConnectionId()))
		return true;

    if(player.GetPhysicalPlayerIndex() == INVALID_PLAYER_INDEX || !m_PlayerThrottleData[player.GetPhysicalPlayerIndex()].ConsiderForThrottling())
        return true;

    const netConnectionManager& cxnMgr = *m_ConnectionMgr;

    //Point at which can't advance our bandwidth
    const unsigned advanceReliablesSendCount = 2;

    if(cxnMgr.GetUnAckedCount(player.GetConnectionId()) && cxnMgr.GetOldestResendCount(player.GetConnectionId()) > advanceReliablesSendCount)
    {
        gnetDebug2("Bandwidth cannot be advanced due to %d unacked reliable messages to %s", m_ConnectionMgr->GetOldestResendCount(player.GetConnectionId()), player.GetLogName());
        return false;
    }

    // now check if there are any unreliables that have been resent
    //Point at which can't advance our bandwidth
    const unsigned advanceUnreliablesSendCount = 5;

    if(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
    {
	    if(m_NumUnreliableResends[player.GetPhysicalPlayerIndex()] >= advanceUnreliablesSendCount)
        {
            gnetDebug2("Bandwidth cannot be advanced due to %d unacked unreliable messages to %s", GetUnreliableResendCount(player), player.GetLogName());
            return false;
        }
    }

    return true;
}

bool netBandwidthMgr::GetReliableMessagesHaveBeenUnackedTooLong(int cxnId) const
{
    Assert(m_ConnectionMgr);
    Assert(cxnId >= 0);

    //Point at which we throttle back our bandwidth usage.
    const unsigned throttleSendCount = RELIABLE_RESEND_THRESHOLD;

    if(m_ConnectionMgr->GetUnAckedCount(cxnId) && m_ConnectionMgr->GetOldestResendCount(cxnId) > throttleSendCount)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool netBandwidthMgr::GetUnreliableMessagesHaveBeenUnackedTooLong(const netPlayer& player) const
{
    if(player.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
    {
	    return m_NumUnreliableResends[player.GetPhysicalPlayerIndex()] >= UNRELIABLE_RESEND_THRESHOLD;
    }
    else
    {
        return false;
    }
}

#if __BANK
void netBandwidthMgr::RegisterStatistics(netBandwidthStatistics& stats, const char* name)
{
	Assertf(m_NumBandwidthStats < MAX_STATS, "Too many bandwidth stats");

	if (m_NumBandwidthStats < MAX_STATS)
	{
		m_BandwidthStats[m_NumBandwidthStats++] = &stats;
		stats.SetName(name);

		stats.Init();
	}
}
#endif // __BANK

bool netBandwidthMgr::PlayerThrottleData::ConsiderForThrottling() const
{
    u32 currentTime          = sysTimer::GetSystemMsTime();
    u32 timeSinceLastMessage = (currentTime > m_LastMessageReceivedTime) ? (currentTime - m_LastMessageReceivedTime) : 0;
    
    if(timeSinceLastMessage > PREVENT_THROTTLE_INTERVAL || m_PreventThrottleTimer > 0)
    {
        return false;
    }

    return true;
}

void netBandwidthMgr::OnMessageReceived(const ReceivedMessageData &messageData)
{
    if(messageData.m_FromPlayer)
    {
        PhysicalPlayerIndex playerIndex = messageData.m_FromPlayer->GetPhysicalPlayerIndex();

        if(playerIndex != INVALID_PLAYER_INDEX)
        {
            u32 lastMessageReceivedTime                                 = m_PlayerThrottleData[playerIndex].m_LastMessageReceivedTime;
            m_PlayerThrottleData[playerIndex].m_LastMessageReceivedTime = sysTimer::GetSystemMsTime();

            if(lastMessageReceivedTime != 0 && (m_PlayerThrottleData[playerIndex].m_LastMessageReceivedTime - lastMessageReceivedTime) > PlayerThrottleData::PREVENT_THROTTLE_INTERVAL)
            {
                m_PlayerThrottleData[playerIndex].m_PreventThrottleTimer = PlayerThrottleData::PREVENT_THROTTLE_TIME;
            }
        }
    }
}

unsigned netBandwidthMgr::CalculatePlayerUpdateLevels(unsigned &UNUSED_PARAM(numLowLevelPlayers),
                                                      unsigned &UNUSED_PARAM(numMediumLevelPlayers),
                                                      unsigned &numHighLevelPlayers)
{
    // The default behaviour is to assign all players a high update level
    unsigned numPhysicalPlayers = 0;

	unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
    const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();
    
	for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
    {
        const netPlayer *player = remotePhysicalPlayers[index];

		m_PlayerBandwidthInfo[player->GetPhysicalPlayerIndex()].m_UpdateLevel = PlayerBandwidthData::HIGH_UPDATE_LEVEL;
		numHighLevelPlayers++;
		numPhysicalPlayers++;
	}

    return numPhysicalPlayers;
}

void netBandwidthMgr::UpdateGameDataTargetBandwidth()
{
    // set the game bandwidth
    // scale down by 10% to make more realistic estimate for data rate actually supported
    float gameUpstreamBandwidth = m_TargetTotalUpstreamBandwidth * 1024.0f * 0.9f;

    // now remove a fixed amount for usage by voice library (this will use a voice library setting when available)
    gameUpstreamBandwidth -= VOICE_BANDWIDTH_USAGE;
    gameUpstreamBandwidth = Max(gameUpstreamBandwidth, static_cast<float>(GetMinimumBandwidth() << 10));

    // finally remove data for UDP headers for the specified update rate
    gameUpstreamBandwidth -= (ESTIMATED_UDP_HEADER_SIZE * ((1000.0f/GAME_UPDATE_INTERVAL) * netInterface::GetNumRemotePhysicalPlayers()));
    gameUpstreamBandwidth = Max(gameUpstreamBandwidth, 0.0f);

    m_TargetGameUpstreamBandwidth = static_cast<unsigned>(gameUpstreamBandwidth);

    // the channel policies expect a value in bytes/sec rather than bits/sec
    // so we must divide by 8
    netChannelPolicies policies;
    m_ConnectionMgr->GetChannelPolicies(NETWORK_GAME_CHANNEL_ID, &policies);
    policies.m_OutboundBandwidthLimit = (m_TargetGameUpstreamBandwidth>>3);
    m_ConnectionMgr->SetChannelPolicies(NETWORK_GAME_CHANNEL_ID, policies);
}

void netBandwidthMgr::UpdatePlayerBandwidthInfo()
{
#if __BANK
    if(m_OverridePlayerBandwidthTargets)
        return;
#endif //__BANK

    unsigned numLowLevelObjects    = 0;
    unsigned numMediumLevelObjects = 0;
    unsigned numHighLevelObjects   = 0;

    unsigned numPhysicalPlayers = CalculatePlayerUpdateLevels(numLowLevelObjects, numMediumLevelObjects, numHighLevelObjects);

    if(numPhysicalPlayers)
    {
        // now share out the remaining bandwidth based on the update levels
        unsigned availableBandwidth = m_TargetGameUpstreamBandwidth;

        // now share out the remaining bandwidth based on the update level

        /*
            This code decides bandwidth allocation based on update levels via the following formula:
            Available bandwidth = (low level player count    * bandwidth multiplier per player) + 
                                  (medium level player count * (bandwidth multiplier per player * 2)) +
                                  (high level player count   * (bandwidth multiplier per player * 3))

            AB = (LP*B) + (MP*B*2) + (HP*B*3)
            AB = B(LP + 2*MP + 3*HP)
            B  = AB / (LP + 2*MP + 3*HP)

            Hence high level players get 3 times as much data as low level players, while medium level players get twice as much
            as low level players.
        */
        float bandwidthMultiplier = static_cast<float>(availableBandwidth) / (numLowLevelObjects + (2 * numMediumLevelObjects) + (3 * numHighLevelObjects));

        unsigned lowLevelBandwidth    = static_cast<unsigned>(bandwidthMultiplier);
        unsigned mediumLevelBandwidth = static_cast<unsigned>(bandwidthMultiplier * 2.0f);
        unsigned highLevelBandwidth   = static_cast<unsigned>(bandwidthMultiplier * 3.0f);

		unsigned                 numRemotePhysicalPlayers = netInterface::GetNumRemotePhysicalPlayers();
        const netPlayer * const *remotePhysicalPlayers    = netInterface::GetRemotePhysicalPlayers();
        
	    for(unsigned index = 0; index < numRemotePhysicalPlayers; index++)
        {
            const netPlayer *player = remotePhysicalPlayers[index];

			switch(m_PlayerBandwidthInfo[player->GetPhysicalPlayerIndex()].m_UpdateLevel)
			{
			case PlayerBandwidthData::LOW_UPDATE_LEVEL:
				m_PlayerBandwidthInfo[player->GetPhysicalPlayerIndex()].m_TargetBandwidth = lowLevelBandwidth;
				break;
			case PlayerBandwidthData::MEDIUM_UPDATE_LEVEL:
				m_PlayerBandwidthInfo[player->GetPhysicalPlayerIndex()].m_TargetBandwidth = mediumLevelBandwidth;
				break;
			case PlayerBandwidthData::HIGH_UPDATE_LEVEL:
				m_PlayerBandwidthInfo[player->GetPhysicalPlayerIndex()].m_TargetBandwidth = highLevelBandwidth;
				break;
			default:
				Assertf(0, "Unexpected update level!");
			}
        }
    }
}

void netBandwidthMgr::ResetPlayerBandwidthData()
{
    for(PhysicalPlayerIndex playerIndex = 0; playerIndex < MAX_NUM_PHYSICAL_PLAYERS; playerIndex++)
    {
        m_PlayerBandwidthInfo[playerIndex].m_UpdateLevel      = PlayerBandwidthData::LOW_UPDATE_LEVEL;
        m_PlayerBandwidthInfo[playerIndex].m_CurrentBandwidth = 0;
    }
}

void netBandwidthMgr::RecalculateSyncFailureAverage()
{
    unsigned totalSyncFailures = 0;

    for(unsigned index = 0; index < NUM_SYNC_FAILURES_TO_CACHE; index++)
    {
        totalSyncFailures += m_CachedFailuresPerSync[index];
    }

    m_AverageFailuresPerUpdate = totalSyncFailures / NUM_SYNC_FAILURES_TO_CACHE;

    if(m_AverageFailuresPerUpdate > 0)
    {
        m_LastTimeAverageFailuresNonZero = fwTimer::GetSystemTimeInMilliseconds();
    }
}

void netBandwidthMgr::ResetUnreliableResends()
{
#if __BANK
    if(PARAM_additionalBandwidthLogging.Get())
    {
        gnetDebug2("Resetting unreliable resend counts");
    }
#endif // __BANK

    for(PhysicalPlayerIndex index = 0; index < MAX_NUM_PHYSICAL_PLAYERS; index++)
    {
        m_NumUnreliableResends[index] = 0;
    }
}

} // namespace rage
