// 
// debug/network_channel.h 
// 
// Copyright (C) 1999-2008 Rockstar Games.  All Rights Reserved. 
// 

//This is deliberately outside of the #include guards
//in order to mitigate problems with unity builds.
#undef __net_channel
#define __net_channel net

#ifndef INC_NETWORK_CHANNEL_H 
#define INC_NETWORK_CHANNEL_H 

#include "diag/channel.h"
#include "system/param.h"
#include "string/string.h"
#include "system/stack.h"
#include "rline/rl.h"

RAGE_DECLARE_CHANNEL(net)   // defined in netchannel.cpp

#define gnetDebug1(fmt, ...)			RAGE_DEBUGF1(__net_channel, fmt, ##__VA_ARGS__)
#define gnetDebug2(fmt, ...)			RAGE_DEBUGF2(__net_channel, fmt, ##__VA_ARGS__)
#define gnetDebug3(fmt, ...)			RAGE_DEBUGF3(__net_channel, fmt, ##__VA_ARGS__)
#define gnetError(fmt, ...)				RAGE_ERRORF(__net_channel, fmt, ##__VA_ARGS__)
#define gnetWarning(fmt, ...)			RAGE_WARNINGF(__net_channel, fmt, ##__VA_ARGS__)
#define gnetDisplay(fmt, ...)			RAGE_DISPLAYF(__net_channel, fmt, ##__VA_ARGS__)
#define gnetFatalAssertf(cond,fmt,...)	RAGE_FATALASSERTF(__net_channel, cond, fmt, ##__VA_ARGS__)

#if USE_NET_ASSERTS
#if __ASSERT
#define gnetVerify(cond)					RAGE_VERIFY(__net_channel, cond)
#define gnetVerifyf(cond, fmt, ...)			RAGE_VERIFYF(__net_channel, cond, fmt, ##__VA_ARGS__)
#else
#define gnetVerify(cond)					( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__net_channel), #cond, __FILE__, __LINE__, "") )
#define gnetVerifyf(cond, fmt, ...)			( Likely(cond) || _NetworkVerifyf(RAGE_CAT2(Channel_,__net_channel), #cond, __FILE__, __LINE__, fmt, ##__VA_ARGS__) )
#endif  // __ASSERT

#if __ASSERT
#define gnetAssert(cond)					RAGE_ASSERT(__net_channel, cond)
#define gnetAssertf(cond, fmt, ...)			RAGE_ASSERTF(__net_channel, cond, fmt, ##__VA_ARGS__)
#else
#define gnetAssert(cond)			do { static bool __FILE__l__LINE__ =false;  \
										if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); \
										RAGE_ERRORF(__net_channel, "NetworkAssertf(%s) FAILED", #cond); __FILE__l__LINE__=true;  } } while(false)
#define gnetAssertf(cond, fmt, ...)	do { static bool __FILE__l__LINE__ =false;  \
										if( !Likely(cond) && !(__FILE__l__LINE__) ) { sysStack::PrintStackTrace(); char formatted[1024]; formatf(formatted, fmt, ##__VA_ARGS__); \
										RAGE_ERRORF(__net_channel, "NetworkAssertf(%s) FAILED: %s", #cond, formatted); __FILE__l__LINE__=true;  } } while(false)
#endif  // __ASSERT

#else
#define gnetVerify(cond)			RAGE_VERIFY(__net_channel, cond)
#define gnetVerifyf(cond, fmt, ...) RAGE_VERIFYF(__net_channel, cond, fmt, ##__VA_ARGS__)
#define gnetAssert(cond)			RAGE_ASSERT(__net_channel, cond)
#define gnetAssertf(cond, fmt, ...)	RAGE_ASSERTF(__net_channel, cond, fmt, ##__VA_ARGS__)
#endif // USE_NET_ASSERTS

#endif // INC_NETWORK_CHANNEL_H 

