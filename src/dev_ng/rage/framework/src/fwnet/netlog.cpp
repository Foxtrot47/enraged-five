//
// netlog.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

// --- Include Files ------------------------------------------------------------
#include "fwnet/optimisations.h"
NETWORK_OPTIMISATIONS()

#include "fwnet/netlog.h"

#if __XENON
#include "system/xtl.h"
#elif __PPU
#include <netex/libnetctl.h>
#include <sysutil/sysutil_gamecontent.h>
#elif RSG_ORBIS
#include <libnetctl.h>
#endif  //__XENON

// C headers
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

// Rage headers
#include "system/bootmgr.h"

// framework headers
#include "fwnet/netchannel.h"
#include "fwnet/netlogstringtable.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netplayer.h"
#include "fwnet/netplayermgrbase.h"
#include "fwsys/timer.h"

extern __THREAD int RAGE_LOG_DISABLE;

#if RSG_PC && !__NO_OUTPUT
namespace rage
{
	XPARAM(processinstance);
}
#endif

namespace rage
{
	XPARAM(logfile);
}

PARAM(nonetlogs, "[network] disables network logging");
PARAM(enablenetlogs, "[network] enables network logging");
PARAM(maxloginstances, "[network] limits the number of instances of a individual log file that can be generated before old files are overwritten");
XPARAM(localnetlogs);

#if RSG_DURANGO || RSG_PC || RSG_ORBIS

// This is required on Xbox 1 as IPv6 addresses aren't in a friendly format for prefixing to log files (IPv4 addresses are prefixed on other platforms)
// PC uses the same system as a PC may have multiple IP addresses (including 127.0.0.1, which isn't helpful for this usage)
PARAM(netlogprefix, "[network] specifies a prefix applied to the log file, can be used when running multiple instances of a game from the same PC");

#endif // RSG_DURANGO || RSG_PC || RSG_ORBIS

#if ENABLE_NETWORK_LOGGING

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, logimpl, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_logimpl

const unsigned DEFAULT_STRING_TABLE_HEAP_SIZE = 32 * 1024;

netLogFileAccessInterface *netLogImpl::sm_FileAccessInterface = 0;
u32                        netLogImpl::sm_MaximumFileSize     = 0;
netLogStringTable          netLogImpl::sm_StringTable;
u32                        netLogImpl::sm_StringTableHeapSize = DEFAULT_STRING_TABLE_HEAP_SIZE;

#if __BANK
static netLogImpl *s_LogFileBeingSpooled       = 0;
static float       s_SpoolCompletionPercentage = 0.0f;
#endif // __BANK

const unsigned MAX_LOGS_TO_SPOOL = 16;

sysIpcMutex    ms_SpoolThreadMutex   = 0;
sysIpcSema     ms_SpoolThreadSema    = 0;
sysIpcSema     ms_SpoolDataReadySema = 0;
sysIpcThreadId ms_SpoolThreadID      = sysIpcThreadIdInvalid;
bool           ms_WaitingToSpoolData = false;
bool           ms_StopLoggingThread  = false;
unsigned       ms_NumLogsToSpool     = 0;
netLogImpl    *ms_LogsToSpool[MAX_LOGS_TO_SPOOL] = { 0 };

namespace
{
    netLogImpl::LoggingLevel const gDefaultLogLevel = netLogImpl::LOG_LEVEL_LOW;

    const unsigned MAX_LOG_BUFFER_SIZE = 128 * 1024;

    const unsigned DEFAULT_MAX_FILE_COUNT = 2;
}

extern char *XEX_TITLE_ID;	//	defined in gta5\src\dev\game\Core\main.cpp

#define ENABLE_THROUGHPUT_LOGGING 0

#if ENABLE_THROUGHPUT_LOGGING
#define LOG_THROUGHPUT(X) X
#else
#define LOG_THROUGHPUT(X)
#endif // ENABLE_THROUGHPUT_LOGGING

packedLogDataWriter::packedLogDataWriter(char     *logBuffer,
                                         unsigned  logBufferSize) :
m_LogBuffer(logBuffer)
, m_WritePos(m_LogBuffer)
, m_LogBufferSize(logBufferSize)
, m_EndOfFileWritten(false)
, m_LastFrameCount(0)
, m_LastSystemTime(0)
, m_LastNetworkTime(0)
, m_LastThroughputStartPos(0)
{
}

const char *packedLogDataWriter::GetBuffer()
{
    return m_LogBuffer;
}

unsigned packedLogDataWriter::GetBufferSize()
{
    return m_LogBufferSize;
}

unsigned packedLogDataWriter::GetSizeWritten()
{
    return ptrdiff_t_to_int(m_WritePos - m_LogBuffer);
}

void packedLogDataWriter::SetBuffer(char     *logBuffer,
                                    unsigned  logBufferSize)
{
    m_LogBuffer              = logBuffer;
    m_WritePos               = logBuffer;
    m_LogBufferSize          = logBufferSize;
    m_EndOfFileWritten       = false;
    m_LastThroughputStartPos = 0;
}

unsigned packedLogDataWriter::GetThroughputInBytes()
{
    unsigned throughPut = GetSizeWritten() - m_LastThroughputStartPos;
    m_LastThroughputStartPos = GetSizeWritten();

    return throughPut;
}

bool packedLogDataWriter::AddNewFrameData(const char *newFrameData)
{
    bool success = false;

    if(gnetVerifyf(newFrameData, "Invalid new frame data specified!"))
    {
        if(HasSpace(SIZE_OF_LOG_TOKEN + SIZE_OF_LOG_STRING(newFrameData)))
        {
            success  = WriteToken(NEW_FRAME_DATA);
            success &= WriteString(newFrameData);
        }
    }

    return success;
}

bool packedLogDataWriter::AddWriteDataValueData(const char *dataName,
                                                const char *dataValue)
{
    bool success = false;

    if(gnetVerifyf(dataName,  "Invalid data name specified!") &&
       gnetVerifyf(dataValue, "Invalid data value specified!"))
    {
        CheckForFrameDataChange();

        if(HasSpace(SIZE_OF_LOG_TOKEN + SIZE_OF_STRING_TABLE_ID + SIZE_OF_LOG_STRING(dataName) + SIZE_OF_LOG_STRING(dataValue)))
        {
            success  = WriteToken(WRITE_DATA_VALUE);

            success &= WriteStringFromTable(dataName);
            success &= WriteString(dataValue);
        }
    }

    return success;
}

bool packedLogDataWriter::AddStandardLogData(const char *logString)
{
    bool success = false;

    if(gnetVerifyf(logString, "Invalid log data specified!"))
    {
        CheckForFrameDataChange();

        if(HasSpace(SIZE_OF_LOG_TOKEN + SIZE_OF_LOG_STRING(logString)))
        {
            success  = WriteToken(STANDARD_DATA);
            success &= WriteString(logString);
        }
    }

    return success;
}

bool packedLogDataWriter::AddEndOfDataMarker()
{
    if(HasSpace(SIZE_OF_LOG_TOKEN))
    {
        bool success = WriteToken(END_OF_DATA);

        if(success)
        {
            m_EndOfFileWritten = true;
        }
    }

    return m_EndOfFileWritten;
}

bool packedLogDataWriter::AddMessageHeaderData(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName)
{
    bool success = false;

    if(gnetVerifyf(messageString1, "Invalid message string specified!") &&
       gnetVerifyf(messageString2, "Invalid message string specified!") &&
       gnetVerifyf(playerName,     "Invalid player name specified!"))
    {
        CheckForFrameDataChange();

        if(HasSpace(SIZE_OF_LOG_TOKEN + SIZE_OF_BOOL + SIZE_OF_SEQUENCE + 
                    SIZE_OF_STRING_TABLE_ID + SIZE_OF_LOG_STRING(messageString1) +
                    SIZE_OF_STRING_TABLE_ID + SIZE_OF_LOG_STRING(messageString2) +
                    SIZE_OF_STRING_TABLE_ID + SIZE_OF_LOG_STRING(playerName)))
        {
            success  = WriteToken(MESSAGE_HEADER_DATA);
            success &= WriteBool(received);
            success &= WriteSequence(sequence);
            success &= WriteStringFromTable(messageString1);
            success &= WriteStringFromTable(messageString2);
            success &= WriteStringFromTable(playerName);
        }
    }

    return success;
}

bool packedLogDataWriter::AddPlayerTextData(const char *type,
                                            const char *data,
                                            const char *playerName)
{
    bool success = false;

    if(gnetVerifyf(type,       "Invalid type text specified!") &&
       gnetVerifyf(data,       "Invalid data text specified!") &&
       gnetVerifyf(playerName, "Invalid player name specified!"))
    {
        CheckForFrameDataChange();

        if(HasSpace(SIZE_OF_LOG_TOKEN + 
                    SIZE_OF_LOG_STRING(type) +
                    SIZE_OF_LOG_STRING(data) +
                    SIZE_OF_LOG_STRING(playerName)))
        {
            success  = WriteToken(PLAYER_TEXT_DATA);
            success &= WriteString(type);
            success &= WriteString(data);
            success &= WriteString(playerName);
        }
    }

    return success;
}

bool packedLogDataWriter::AddLogEventData(const char *eventName,
                                          const char *eventData,
                                          unsigned    numTrailingLineBreaks)
{
    bool success = false;

    if(gnetVerifyf(eventName, "Invalid event name specified!") &&
       gnetVerifyf(eventData, "Invalid event data specified!"))
    {
        CheckForFrameDataChange();

        if(HasSpace(SIZE_OF_LOG_TOKEN + SIZE_OF_LOG_STRING(eventName) + SIZE_OF_LOG_STRING(eventData) + 1))
        {
            success  = WriteToken(LOG_EVENT_DATA);
            success &= WriteString(eventName);
            success &= WriteString(eventData);
            success &= WriteChar(static_cast<char>(numTrailingLineBreaks));
        }
    }

    return success;
}

bool packedLogDataWriter::AddNodeHeaderData(const char *nodeName)
{
    bool success = false;

    if(gnetVerifyf(nodeName, "Invalid node name specified!"))
    {
        CheckForFrameDataChange();

        if(HasSpace(SIZE_OF_LOG_TOKEN + SIZE_OF_STRING_TABLE_ID + SIZE_OF_LOG_STRING(nodeName)))
        {
            success  = WriteToken(NODE_HEADER_DATA);
            success &= WriteStringFromTable(nodeName);
        }
    }

    return success;
}

void packedLogDataWriter::CheckForFrameDataChange()
{
    unsigned frameCount = fwTimer::GetFrameCount();
    unsigned sysTime    = diagGetAdjustedSystemTime();
    unsigned netTime    = diagGetAdjustedSystemTime();

    if (netInterface::IsInitialised() && netInterface::NetworkClockHasSynced())
    {
	    netTime = netInterface::GetNetworkTime();
    }

    if(m_LastFrameCount   != frameCount ||
       m_LastSystemTime   != sysTime    || 
       m_LastNetworkTime  != netTime)
    {
        const unsigned MAX_FRAME_DATA = 128;
        char frameData[MAX_FRAME_DATA];
        sprintf(frameData, "Frame:%u [%u][%u]", frameCount, sysTime, netTime);
        AddNewFrameData(frameData);

        m_LastFrameCount  = frameCount;
        m_LastSystemTime  = sysTime; 
        m_LastNetworkTime = netTime;
    }
}

bool packedLogDataWriter::HasSpace(unsigned size)
{
    // check whether there is space for the requested size plus and EOF token, as we always want to write this
    int bytesLeft = (static_cast<int>(m_LogBufferSize) - ptrdiff_t_to_int(m_WritePos - m_LogBuffer));
    if((bytesLeft >= static_cast<int>(size + SIZE_OF_LOG_TOKEN)) && !m_EndOfFileWritten)
    {
        return true;
    }
    else
    {
        if(!m_EndOfFileWritten && gnetVerifyf(bytesLeft >= SIZE_OF_LOG_TOKEN, "Unexpectedly got no room for EOF token!"))
        {
            *m_WritePos = END_OF_DATA;
            m_WritePos++;
            m_EndOfFileWritten = true;
        }
        return false;
    }
}

bool packedLogDataWriter::WriteToken(char token)
{
    if(gnetVerifyf(HasSpace(SIZE_OF_LOG_TOKEN), "Trying to write a token when there is no space in the buffer!"))
    {
        *m_WritePos = token;
        m_WritePos++;
        return true;
    }

    return false;
}

bool packedLogDataWriter::WriteBool(bool value)
{
    if(gnetVerifyf(HasSpace(SIZE_OF_BOOL), "Trying to write a token when there is no space in the buffer!"))
    {
        *m_WritePos = value ? 1 : 0;
        m_WritePos++;
        return true;
    }

    return false;
}

bool packedLogDataWriter::WriteSequence(netSequence sequence)
{
    if(gnetVerifyf(HasSpace(SIZE_OF_SEQUENCE), "Trying to write a string table ID when there is no space in the buffer!"))
    {
        u8 lowByte  = sequence & 0xff;
        u8 highByte = ((sequence >> 8) & 0xff);
        *m_WritePos = highByte;
        m_WritePos++;
        *m_WritePos = lowByte;
        m_WritePos++;
        return true;
    }

    return false;
}

bool packedLogDataWriter::WriteStringTableID(unsigned stringID)
{
    if(gnetVerifyf(HasSpace(SIZE_OF_STRING_TABLE_ID), "Trying to write a string table ID when there is no space in the buffer!"))
    {
        u8 lowByte  = stringID & 0xff;
        u8 highByte = ((stringID >> 8) & 0xff);
        *m_WritePos = highByte;
        m_WritePos++;
        *m_WritePos = lowByte;
        m_WritePos++;
        return true;
    }

    return false;
}

bool packedLogDataWriter::WriteStringFromTable(const char *string)
{
    bool success = false;

    if(gnetVerifyf(HasSpace(SIZE_OF_STRING_TABLE_ID + SIZE_OF_LOG_STRING(string)), "Trying to write a string when there is no space in the buffer!"))
    {
        unsigned stringID = netLogStringTable::MAX_STRINGS;

        if(netLogImpl::sm_StringTable.IsInitialised() && netLogImpl::sm_StringTable.FindString(string, stringID))
        {
            success = WriteStringTableID(stringID);
        }
        else
        {
            gnetAssertf(stringID == netLogStringTable::MAX_STRINGS, "Writing a bad string table ID for a string not in the table!");
            success  = WriteStringTableID(stringID);
            success &= WriteString(string);
        }
    }

    return success;
}

bool packedLogDataWriter::WriteString(const char *string)
{
    if(gnetVerifyf(HasSpace(SIZE_OF_LOG_STRING(string)), "Trying to write a string when there is no space in the buffer!"))
    {
        strcpy(m_WritePos, string);
        m_WritePos += strlen(string);
        *m_WritePos = '\0';
        m_WritePos++;
        return true;
    }

    return false;
}

bool packedLogDataWriter::WriteChar(const char character)
{
    if(gnetVerifyf(HasSpace(1), "Trying to write a character when there is no space in the buffer!"))
    {
        *m_WritePos = character;
        m_WritePos++;
        return true;
    }

    return false;
}

packedLogDataReader::packedLogDataReader(const char                           *packedLogData,
                                         unsigned                              packedDataSize,
                                         netLogFileAccessInterface            &fileAccess,
                                         netLogFileAccessInterface::LogHandle &logHandle) :
m_PackedLogData(packedLogData)
, m_ReadPos(m_PackedLogData)
, m_PackedDataSize(packedDataSize)
, m_UnpackedDataSize(0)
, m_FileAccess(fileAccess)
, m_LogHandle(logHandle)
{
    m_CurrentFrameData[0] = '\0';
}

void packedLogDataReader::SetPackedLogData(char     *packedLogData,
                                           unsigned  packedLogDataSize)
{
    m_PackedLogData    = packedLogData;
    m_ReadPos          = packedLogData;
    m_PackedDataSize   = packedLogDataSize;
    m_UnpackedDataSize = 0;
}

void packedLogDataReader::WriteData()
{
    bool endOfData = false;

    m_UnpackedDataSize = 0;

    while(!endOfData)
    {
#if __BANK
        float percentComplete = (static_cast<float>(m_ReadPos - m_PackedLogData) / static_cast<float>(m_PackedDataSize)) * 100.0f;
        s_SpoolCompletionPercentage = percentComplete;
#endif // __BANK
        if((m_ReadPos - m_PackedLogData) >= static_cast<int>(m_PackedDataSize))
        {
            endOfData = true;
        }
        else
        {
            unsigned logToken = static_cast<unsigned>(*m_ReadPos);

            m_ReadPos++;

            if(gnetVerifyf((m_ReadPos - m_PackedLogData) <= static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
            {
                endOfData = ProcessPackedLogData(logToken, m_UnpackedDataSize);
            }
        }
    }

    BANK_ONLY(s_SpoolCompletionPercentage = 0.0f);
}

bool packedLogDataReader::ProcessPackedLogData(unsigned logToken, unsigned &bytesWrittenToOutput)
{
    bool endOfData = false;

    switch(logToken)
    {
    case NEW_FRAME_DATA:
        ProcessNewFrameData();
        break;
    case WRITE_DATA_VALUE:
        bytesWrittenToOutput += ProcessWriteDataValueData();
        break;
    case STANDARD_DATA:
        bytesWrittenToOutput += ProcessStandardData();
        break;
    case END_OF_DATA:
        endOfData = true;
        break;
    case MESSAGE_HEADER_DATA:
        bytesWrittenToOutput += ProcessMessageHeaderData();
        break;
    case PLAYER_TEXT_DATA:
        bytesWrittenToOutput += ProcessPlayerTextData();
        break;
    case LOG_EVENT_DATA:
        bytesWrittenToOutput += ProcessLogEventData();
        break;
    case NODE_HEADER_DATA:
        bytesWrittenToOutput += ProcessNodeHeaderData();
        break;
    default:
        gnetAssertf(0, "Unexpected log token found: logToken ASCII: %d. lotToken char: %c", logToken, logToken);
        endOfData = true;
    };

    return endOfData;
}

void packedLogDataReader::ProcessNewFrameData()
{
    if(gnetVerifyf((m_ReadPos - m_PackedLogData) < static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
    {
        safecpy(m_CurrentFrameData, m_ReadPos);
        m_ReadPos += strlen(m_CurrentFrameData) + 1;
    }
}

unsigned packedLogDataReader::ProcessWriteDataValueData()
{
    unsigned bytesWritten = 0;

	const unsigned BUFFER_SIZE = netLogImpl::MAX_LOG_STRING;
    char dataName[BUFFER_SIZE];
    bool success = GetStringFromTable(dataName, BUFFER_SIZE);

    char dataValue[BUFFER_SIZE];
    success &= GetString(dataValue, BUFFER_SIZE);

    if(success)
    {
        const unsigned MAX_LOG_BUFFER = netLogImpl::MAX_LOG_STRING;
        char logBuffer[MAX_LOG_BUFFER];
        formatf(logBuffer, MAX_LOG_BUFFER, "%s\t\t\t%-40s:  %s\r\n", m_CurrentFrameData, dataName, dataValue);
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));
        bytesWritten += ustrlen(logBuffer);
    }

    return bytesWritten;
}

unsigned packedLogDataReader::ProcessStandardData()
{
    unsigned bytesWritten = 0;

    const unsigned BUFFER_SIZE = netLogImpl::MAX_LOG_STRING;
    char logBuffer[BUFFER_SIZE];
    bool success = GetString(logBuffer, BUFFER_SIZE);

    if(success)
    {
        m_FileAccess.Write(m_LogHandle, m_CurrentFrameData, istrlen(m_CurrentFrameData));
        m_FileAccess.Write(m_LogHandle, "\t", istrlen("\t"));
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));

        bytesWritten += (ustrlen(m_CurrentFrameData) + ustrlen("\t") + ustrlen(logBuffer));
    }

    return bytesWritten;
}

unsigned packedLogDataReader::ProcessMessageHeaderData()
{
    unsigned bytesWritten = 0;

    bool        received = false;
    netSequence sequence = 0;

    bool success = GetBool(received);
    success &= GetSequence(sequence);

    const unsigned MAX_MESSAGE_HEADER_TEXT = netLogImpl::MAX_LOG_STRING;
    char messageText1[MAX_MESSAGE_HEADER_TEXT];
    success &= GetStringFromTable(messageText1, MAX_MESSAGE_HEADER_TEXT);

    char messageText2[MAX_MESSAGE_HEADER_TEXT];
    success &= GetStringFromTable(messageText2, MAX_MESSAGE_HEADER_TEXT);

    char playerText[MAX_MESSAGE_HEADER_TEXT];
    success &= GetStringFromTable(playerText, MAX_MESSAGE_HEADER_TEXT);

    if(success)
    {
        const unsigned MAX_LOG_BUFFER = netLogImpl::MAX_LOG_STRING;
        char logBuffer[MAX_LOG_BUFFER];

        safecpy(logBuffer, "-------------------------------------------------------------------------------------------------------------\r\n");
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));

        bytesWritten += ustrlen(logBuffer);

        if (received)
        {
            formatf(logBuffer, MAX_LOG_BUFFER, "%s\t\t%-35s%-28s<< %-10s (%d)\r\n", m_CurrentFrameData, messageText1, messageText2, playerText, sequence);
        }
        else
        {
            formatf(logBuffer, MAX_LOG_BUFFER, "%s\t\t%-35s%-28s>> %-10s (%d)\r\n", m_CurrentFrameData, messageText1, messageText2, playerText, sequence);
        }

        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));

        bytesWritten += ustrlen(logBuffer);
    }

    return bytesWritten;
}

unsigned packedLogDataReader::ProcessPlayerTextData()
{
    unsigned bytesWritten = 0;

    const unsigned BUFFER_SIZE = netLogImpl::MAX_LOG_STRING;
    
    char type[BUFFER_SIZE];
    bool success = GetString(type, BUFFER_SIZE);

    char messageText[BUFFER_SIZE];
    success &= GetString(messageText, BUFFER_SIZE);

    char playerName[BUFFER_SIZE];
    success &= GetString(playerName, BUFFER_SIZE);

    if(success)
    {
        const unsigned MAX_LOG_BUFFER = netLogImpl::MAX_LOG_STRING;
        char logBuffer[MAX_LOG_BUFFER];

        safecpy(logBuffer, "-------------------------------------------------------------------------------------------------------------\r\n");
        
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));
        m_FileAccess.Write(m_LogHandle, m_CurrentFrameData, istrlen(m_CurrentFrameData));

        bytesWritten += ustrlen(logBuffer);
        bytesWritten += ustrlen(m_CurrentFrameData);

        formatf(logBuffer, MAX_LOG_BUFFER, "\t\t%-35s%-18s   %s\r\n", type, messageText, playerName);
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));

        bytesWritten += ustrlen(logBuffer);
    }

    return bytesWritten;
}

unsigned packedLogDataReader::ProcessLogEventData()
{
    unsigned bytesWritten = 0;

    const unsigned BUFFER_SIZE = netLogImpl::MAX_LOG_STRING;

    char eventName[BUFFER_SIZE];
    bool success = GetString(eventName, BUFFER_SIZE);

    char eventData[BUFFER_SIZE];
    success &= GetString(eventData, BUFFER_SIZE);

    unsigned trailingLineBreaks = 0;
    success &= GetUnsignedInteger(trailingLineBreaks);

    if(success)
    {
        const unsigned MAX_LOG_BUFFER = netLogImpl::MAX_LOG_STRING;
        char logBuffer[MAX_LOG_BUFFER];

        safecpy(logBuffer, "-------------------------------------------------------------------------------------------------------------\r\n");

        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));
        m_FileAccess.Write(m_LogHandle, m_CurrentFrameData, istrlen(m_CurrentFrameData));

        bytesWritten += ustrlen(logBuffer);
        bytesWritten += ustrlen(m_CurrentFrameData);

        formatf(logBuffer, MAX_LOG_BUFFER, "\t\t%-35s%-18s\r\n", eventName, eventData);
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));

        bytesWritten += ustrlen(logBuffer);

        // add any trailing line breaks
        formatf(logBuffer, MAX_LOG_BUFFER, "%s\r\n", m_CurrentFrameData);

        while(trailingLineBreaks > 0)
        {
            m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));
            bytesWritten += ustrlen(logBuffer);
            trailingLineBreaks--;
        }
    }

    return bytesWritten;
}

unsigned packedLogDataReader::ProcessNodeHeaderData()
{
    unsigned bytesWritten = 0;

    const unsigned BUFFER_SIZE = 1024;

    char nodeName[BUFFER_SIZE];
    bool success = GetStringFromTable(nodeName, BUFFER_SIZE);

    if(success)
    {
        const unsigned MAX_LOG_BUFFER = 1024;
        char logBuffer[MAX_LOG_BUFFER];
        formatf(logBuffer, MAX_LOG_BUFFER, "%s\t\t%s:\r\n", m_CurrentFrameData, nodeName);
        m_FileAccess.Write(m_LogHandle, logBuffer, istrlen(logBuffer));

        bytesWritten += ustrlen(logBuffer);
    }

    return bytesWritten;
}

bool packedLogDataReader::GetBool(bool &result)
{
    bool success = false;

    if(gnetVerifyf((m_ReadPos - m_PackedLogData) < static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
    {
        u8 value = static_cast<u8>(*m_ReadPos);
        m_ReadPos++;

        gnetAssertf((result == 0) || (result == 1), "Unexpected boolean value encountered (expecting only 0 or 1)");
        result = (value!=0) ? true : false;
        success = true;
    }

    return success;
}

bool packedLogDataReader::GetSequence(netSequence &sequence)
{
    bool success = false;

    // get the string table ID
    if(gnetVerifyf((m_ReadPos - m_PackedLogData) < static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
    {
        u8 highByte = static_cast<u8>(*m_ReadPos);
        m_ReadPos++;
        u8 lowByte  = static_cast<u8>(*m_ReadPos);
        m_ReadPos++;
            
        sequence = highByte;
        sequence<<=8;
        sequence |= lowByte;
        success = true;
    }

    return success;
}

bool packedLogDataReader::GetString(char *buffer, const unsigned bufferSize)
{
    bool success = false;

    if(gnetVerifyf((m_ReadPos - m_PackedLogData) < static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
    {
        safecpy(buffer, m_ReadPos, bufferSize);
        m_ReadPos += strlen(buffer) + 1;
        success = true;
    }

    return success;
}

bool packedLogDataReader::GetStringFromTable(char *buffer, const unsigned bufferSize)
{
    bool success = false;

    // get the string table ID
    if(gnetVerifyf((m_ReadPos - m_PackedLogData) < static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
    {
        u8 highByte = static_cast<u8>(*m_ReadPos);
        m_ReadPos++;
        u8 lowByte  = static_cast<u8>(*m_ReadPos);
        m_ReadPos++;
            
        unsigned stringID = (highByte<<8);
        stringID |= lowByte;

        if(stringID == netLogStringTable::MAX_STRINGS)
        {
            success = GetString(buffer, bufferSize);
        }
        else
        {
            NETWORK_QUITF(netLogImpl::sm_StringTable.IsInitialised(), "Trying to get a string from the string table when it is not initialised!");

            const char *string = netLogImpl::sm_StringTable.GetString(stringID);
            safecpy(buffer, string, bufferSize);

            success = true;
        }
    }

    return success;
}

bool packedLogDataReader::GetUnsignedInteger(unsigned &value)
{
    bool success = false;

    if(gnetVerifyf((m_ReadPos - m_PackedLogData) < static_cast<int>(m_PackedDataSize), "Unexpectedly reached end of data!"))
    {
        value = static_cast<unsigned>(*m_ReadPos);
        m_ReadPos++;

        success = true;
    }

    return success;
}

netLogImpl::netLogImpl(const char *fileName, LogOpenType openType, LogTargetType targetType, LogBlockingMode blockingMode, unsigned logBufferSize) :
m_OpenType(openType)
, m_TargetType(targetType)
, m_BlockingMode(blockingMode)
, m_LogLevelIn(gDefaultLogLevel)
, m_LogLevelOut(gDefaultLogLevel)
, m_LogHandle(0)
, m_Disable(false)
, m_UsesStringTable(false)
, m_AddedToSpool(false)
, m_LastFrameHeaderWritten(0)
, m_BytesWritten(0)
, m_PreviousFileCount(0)
, m_MaxFileCount(DEFAULT_MAX_FILE_COUNT)
, m_LastTimeBuffersSwapped(sysTimer::GetSystemMsTime())
, m_LogBufferSize(logBufferSize)
, m_PackedLogDataWriter(0)
, m_PackedLogDataReader(0)
, m_BufferMutex(0)
, m_BufferSema(0)
, m_ReadBuffer(0)
, m_WriteBuffer(0)
, m_ReadBufferSize(0)
, m_FlushRequested(false)
, m_StatsStartTime(0)
, m_LastThroughputInBytes(0)
, m_ThroughputInBytes(0) 
, m_LastWaitsPerSecond(0)  
, m_WaitsPerSecond(0) 
{
#if __BANK && !__DEV
    m_Disable = !PARAM_enablenetlogs.Get();
#endif

    if(!PARAM_maxloginstances.Get(m_MaxFileCount))
    {
        m_MaxFileCount = DEFAULT_MAX_FILE_COUNT;
    }

    if(!AssertVerify(fileName) ||
        !AssertVerify(strlen(fileName) < MAX_LOG_FILE_NAME))
    {
        Quitf("Invalid filename passed to netLogImpl constructor!");
    }
    else
    {
        safecpy(m_FileName, fileName, sizeof(m_FileName));

#if __WIN32PC && !__FINAL
		u32 processInstance = 0;
		if(PARAM_processinstance.Get(processInstance))
		{
			if(AssertVerify(processInstance > 0))
			{
				formatf(m_FileName, sizeof(m_FileName), "instance%d_%s", processInstance, fileName);
			}
		}
#endif
    }

    NETWORK_QUITF(m_LogBufferSize > 0 && 
                     m_LogBufferSize < MAX_LOG_BUFFER_SIZE, "Invalid Buffer Size (%d) specified (Max is %d).", m_LogBufferSize, MAX_LOG_BUFFER_SIZE);

    m_BufferMutex  = sysIpcCreateMutex();
    NETWORK_QUITF(m_BufferMutex, "Failed to create mutex for protecting spool buffers!");

    m_BufferSema = sysIpcCreateSema(0);
    NETWORK_QUITF(m_BufferSema, "Failed to create semaphore for signalling spool thread data is ready!");

    // need to ensure we are using the master heap so we can get access to the debug allocator
    sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
    sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

    sysMemStartDebug();
    m_ReadBuffer  = rage_new char [m_LogBufferSize];
    m_WriteBuffer = rage_new char [m_LogBufferSize];
    sysMemEndDebug();

    sysMemAllocator::SetCurrent(*oldHeap);

    if((!sysBootManager::IsBootedFromDisc() || PARAM_localnetlogs.Get()) && (!PARAM_nonetlogs.Get()))
    {
        if (!IsLoggingThreadRunning())
        {
            gnetError("Creating a log file when the logging thread has not been started! You may lose data for %s!", m_FileName);
        }

        if(gnetVerifyf(ms_SpoolThreadMutex, "Spool thread mutex does not exist!, cannot add %s to logging thread for spooling!", m_FileName))
        {
            if(gnetVerifyf(sysIpcLockMutex(ms_SpoolThreadMutex), "Failed to lock spool thread mutex!"))
            {
                for(unsigned index = 0; index < MAX_LOGS_TO_SPOOL && !m_AddedToSpool; index++)
                {
                    if(ms_LogsToSpool[index] == 0)
                    {
                        ms_LogsToSpool[index] = this;

                        m_AddedToSpool = true;
                    }
                }

                gnetAssertf(m_AddedToSpool, "Failed to find free slot for %s!", m_FileName);
                gnetVerifyf(sysIpcUnlockMutex(ms_SpoolThreadMutex), "Failed to unlock spool thread mutex!");
            }
        }
    }

    // open the file ready for the spool thread
    Open();

    NETWORK_QUITF(sm_FileAccessInterface, "File access interface has not been set!");
    m_PackedLogDataWriter = sm_FileAccessInterface->CreatePackedLogDataWriter(m_WriteBuffer, m_LogBufferSize);
    m_PackedLogDataReader = sm_FileAccessInterface->CreatePackedLogDataReader(m_ReadBuffer,  m_LogBufferSize, m_LogHandle);
}

netLogImpl::~netLogImpl()
{
    if((!sysBootManager::IsBootedFromDisc() || PARAM_localnetlogs.Get()) && (!PARAM_nonetlogs.Get()))
    {
        gnetAssertf(IsLoggingThreadRunning(), "Destroying a log file (%s) when the logging thread has not been started!", m_FileName);

        if(gnetVerifyf(ms_SpoolThreadMutex, "Spool thread mutex does not exist!, cannot remove %s from logging thread for spooling!", m_FileName))
        {
            if(gnetVerifyf(sysIpcLockMutex(ms_SpoolThreadMutex), "Failed to lock spool thread mutex!"))
            {
                bool removed = false;

                for(unsigned index = 0; index < MAX_LOGS_TO_SPOOL && !removed; index++)
                {
                    if(ms_LogsToSpool[index] == this)
                    {
                        ms_LogsToSpool[index] = 0;
                        m_AddedToSpool = false;
                        removed = true;
                    }
                }

                gnetVerifyf(sysIpcUnlockMutex(ms_SpoolThreadMutex), "Failed to unlock spool thread mutex!");
            }
        }
    }

    if(IsOpen())
    {
        Close();
    }

    sysIpcDeleteMutex(m_BufferMutex);
    m_BufferMutex  = 0;

    sysIpcDeleteSema(m_BufferSema);
    m_BufferSema = 0;

    // the read and write buffers are allocated from the master heap
    sysMemAllocator *oldHeap = &sysMemAllocator::GetCurrent();
    sysMemAllocator::SetCurrent(sysMemAllocator::GetMaster());

    delete [] m_ReadBuffer;
    delete [] m_WriteBuffer;
    m_ReadBuffer  = 0;
    m_WriteBuffer = 0;

    sysMemAllocator::SetCurrent(*oldHeap);

    delete m_PackedLogDataWriter;
    delete m_PackedLogDataReader;
    m_PackedLogDataWriter = 0;
    m_PackedLogDataReader = 0;
}

void netLogImpl::SpoolLogData()
{	
    if(gnetVerifyf(m_BufferMutex, "Log file buffer mutex does not exist!"))
    {
        unsigned dataToRead = sysInterlockedRead(&m_ReadBufferSize);

        if(dataToRead > 0)
        {
            if(gnetVerifyf(sysIpcLockMutex(m_BufferMutex), "Failed to lock log file buffer mutex!"))
            {
                gnetFatalAssertf(IsOpen(), "Trying to spool data for a log file that has not been opened! Try restarting SysTrayRFS or check log file directory permissions.");

                if(m_ReadBufferSize != 0)
                {
                    gnetAssertf(sm_FileAccessInterface, "File access interface not set. Please call netLog::SetFileAccessInterface when your game is initialised!");

                    if(sm_FileAccessInterface && gnetVerifyf(m_PackedLogDataReader, "Packed log data reader not set, cannot spool log data!"))
                    {
                        LOG_THROUGHPUT(unsigned startTime = sysTimer::GetSystemMsTime());

                        LOG_THROUGHPUT(gnetDebug3("Starting spooling for %s...", m_FileName));

                        m_PackedLogDataReader->SetPackedLogData(m_ReadBuffer, m_ReadBufferSize);
                        m_PackedLogDataReader->WriteData();

                        m_ReadBufferSize = 0;

                        if(m_FlushRequested)
                        {
                            sm_FileAccessInterface->Flush(m_LogHandle);
                            m_FlushRequested = false;

                            LOG_THROUGHPUT(gnetDebug3("Finished spooling and flushing for %s...", m_FileName));
                        }
                        else
                        {
                            LOG_THROUGHPUT(gnetDebug3("Finished spooling for %s...", m_FileName));
                        }

                        LOG_THROUGHPUT(unsigned endTime = sysTimer::GetSystemMsTime());

                        LOG_THROUGHPUT(gnetDebug3("Spooling for %s took %dms...", m_FileName, endTime - startTime));
                    }
                }

                gnetVerifyf(sysIpcUnlockMutex(m_BufferMutex), "Failed to unlock log file buffer mutex!");
            }
        }
    }
}

void LogSpoolFunction(void *UNUSED_PARAM(param))
{
    if(gnetVerifyf(ms_SpoolThreadSema, "Started the logging spool thread without a valid thread semaphore!"))
    {
        sysIpcSignalSema(ms_SpoolThreadSema);

        while(!ms_StopLoggingThread)
        {
            if(gnetVerifyf(ms_SpoolDataReadySema, "Spool thread sema does not exist!, cannot spool log files!"))
            {
                ms_WaitingToSpoolData = true;
                sysIpcWaitSema(ms_SpoolDataReadySema);
                ms_WaitingToSpoolData = false;

                if(gnetVerifyf(ms_SpoolThreadMutex, "Spool thread mutex does not exist!, cannot spool log files!"))
                {
                    if(gnetVerifyf(sysIpcLockMutex(ms_SpoolThreadMutex), "Failed to lock spool thread mutex!, cannot spool log files!"))
                    {
                        for(unsigned index = 0; index < MAX_LOGS_TO_SPOOL; index++)
                        {
                            if(ms_LogsToSpool[index] != 0)
                            {
                                BANK_ONLY(sysInterlockedExchangePointer((void**)(void*)&s_LogFileBeingSpooled, ms_LogsToSpool[index]));

                                ms_LogsToSpool[index]->SpoolLogData();

                                BANK_ONLY(sysInterlockedExchangePointer((void**)(void*)&s_LogFileBeingSpooled, 0));
                            }
                        }

                        gnetVerifyf(sysIpcUnlockMutex(ms_SpoolThreadMutex), "Failed to unlock spool thread mutex!");
                    }
                }
            }
        }

        sysIpcSignalSema(ms_SpoolThreadSema);
    }
}

void netLogImpl::StartLoggingThread()
{
    if(PARAM_nonetlogs.Get())
        return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

	gnetDebug1("StartLoggingThread");

#if __ASSERT
    bool success = false;
#endif // __ASSERT

    if(gnetVerifyf(ms_SpoolThreadID == sysIpcThreadIdInvalid, "Calling StartLoggingThread() when the thread is already started!"))
    {
        ms_StopLoggingThread  = false;

        if (ms_SpoolThreadMutex == nullptr)
        {
            ms_SpoolThreadMutex = sysIpcCreateMutex();
        }
        ms_SpoolThreadSema    = sysIpcCreateSema(0);
        ms_SpoolDataReadySema = sysIpcCreateSema(0);

        if(ms_SpoolThreadSema && ms_SpoolDataReadySema && ms_SpoolThreadMutex)
        {
            ms_SpoolThreadID = sysIpcCreateThread(&LogSpoolFunction,
                                                 NULL,
                                                 32768,
                                                 PRIO_ABOVE_NORMAL,
                                                 "netLog Spooler", 
												 5, 
												 "NetLogSpooler");

            if(sysIpcThreadIdInvalid != ms_SpoolThreadID)
            {
                //Wait for the thread to start.
                sysIpcWaitSema(ms_SpoolThreadSema);
                ASSERT_ONLY(success = true);
            }
            else
            {
                sysIpcDeleteSema(ms_SpoolThreadSema);
                sysIpcDeleteSema(ms_SpoolDataReadySema);
                ms_SpoolThreadSema    = 0;
                ms_SpoolDataReadySema = 0;

                // ms_SpoolThreadMutex is not deleted here so logs can be added while the spool is being recreated
                // Needed because Gameplay.log is being shutdown and created in SHUTDOWN_SESSION and there's no trivial fix for that.
            }
        }
        else
        {
            if(ms_SpoolThreadSema)
            {
                sysIpcDeleteSema(ms_SpoolThreadSema);
                ms_SpoolThreadSema = 0;
            }

            if(ms_SpoolDataReadySema)
            {
                sysIpcDeleteSema(ms_SpoolDataReadySema);
                ms_SpoolDataReadySema = 0;
            }

            // ms_SpoolThreadMutex is not deleted here so logs can be added while the spool is being recreated
            // Needed because Gameplay.log is being shutdown and created in SHUTDOWN_SESSION and there's no trivial fix for that.
        }
    }

    ASSERT_ONLY(gnetAssertf(success, "Failed to start the network logging thread!");)
}

void netLogImpl::StopLoggingThread()
{
    if(PARAM_nonetlogs.Get())
        return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

	gnetDebug1("netLogImpl - StopLoggingThread");

    if(gnetVerifyf(ms_SpoolThreadID != sysIpcThreadIdInvalid, "Calling StopLoggingThread() when the thread has not been started!"))
    {
        ms_StopLoggingThread = true;

        //Wait for the thread to complete.
        sysIpcSignalSema(ms_SpoolDataReadySema);
        sysIpcWaitSema(ms_SpoolThreadSema);
        sysIpcWaitThreadExit(ms_SpoolThreadID);
        sysIpcDeleteSema(ms_SpoolThreadSema);

        ms_SpoolThreadID    = sysIpcThreadIdInvalid;
        ms_SpoolThreadSema = 0;

        sysIpcDeleteSema(ms_SpoolDataReadySema);
        ms_SpoolDataReadySema = 0;

        // ms_SpoolThreadMutex is not deleted here so logs can be added while the spool is being recreated
        // Needed because Gameplay.log is being shutdown and created in SHUTDOWN_SESSION and there's no trivial fix for that.
    }

    // when the logging thread is stopped we know all log files have been destroyed,
    // so we can now release the string table now
    if(sm_StringTable.IsInitialised())
    {
        sm_StringTable.Shutdown();
    }
}

bool netLogImpl::IsLoggingThreadRunning()
{
    return (sysIpcThreadIdInvalid != ms_SpoolThreadID);
}

#if __BANK

const char *netLogImpl::GetLogFileBeingSpooled(float &completionPercent)
{
    netLogImpl *logFileBeingSpooled = (netLogImpl *)sysInterlockedReadPointer((void**)(void*)&s_LogFileBeingSpooled);

    if(logFileBeingSpooled)
    {
        completionPercent = s_SpoolCompletionPercentage;

        return logFileBeingSpooled->m_FileName;
    }

    return 0;
}

#endif // __BANK

bool netLogImpl::SetUpFilename(char *filename, u32 filenameSize) const
{
    bool success = false;

    if(AssertVerify(filename) && AssertVerify(filenameSize >= MAX_LOG_FILE_NAME))
    {
		// Check for bad netlog prefix (i.e. containing paths)
		const char *netPrefix = "";
		if(PARAM_netlogprefix.Get(netPrefix))
		{			
			bool containsPath = (strchr(netPrefix, '/') != NULL) || (strchr(netPrefix, '\\') != NULL);
						
			if (containsPath)
			{
				gnetAssertf(0, "netLogImpl::SetUpFilename -netlogprefix looks like a path! (%s) This is not how the command was intended to be used!", netPrefix);			
			}			
		}

        // calculate the file extension (including file count)
        const char *fileExt = m_FileName + strlen(m_FileName);
        while(fileExt > m_FileName && *fileExt != '.') fileExt--;

        char mainName[MAX_LOG_FILE_NAME];
        if(fileExt == m_FileName)
        {
            safecpy(mainName, m_FileName, MAX_LOG_FILE_NAME);
            fileExt = "";
        }
        else
        {
            safecpy(mainName, m_FileName, fileExt - m_FileName + 1);
        }

        const u32 MAX_EXTENSION = 128;
        char extension[MAX_EXTENSION];
        if(m_PreviousFileCount > 0)
        {
            formatf(extension, MAX_EXTENSION, "_%d%s", m_PreviousFileCount + 1, fileExt);
        }
        else
        {
            safecpy(extension, fileExt, MAX_EXTENSION);
        }
#if __XENON

        const char *prefix = "";

        if(m_TargetType == LOG_TARGET_LOCAL)
        {
            prefix = "game:/";
        }

        XNADDR xnaddr;
        DWORD dw = XNetGetTitleXnAddr(&xnaddr);

        if(dw & (XNET_GET_XNADDR_DHCP
                | XNET_GET_XNADDR_PPPOE
                | XNET_GET_XNADDR_STATIC))
        {
            formatf(filename,
                    filenameSize,
                    "%s%d.%d.%d.%d_%s%s",
                    prefix,
                    xnaddr.ina.S_un.S_un_b.s_b1,
                    xnaddr.ina.S_un.S_un_b.s_b2,
                    xnaddr.ina.S_un.S_un_b.s_b3,
                    xnaddr.ina.S_un.S_un_b.s_b4,
                    mainName,
                    extension);

            success = true;
        }
#elif __PPU

        const char *prefix = "";

        if(m_TargetType == LOG_TARGET_LOCAL)
        {
            bool directoryAvailable = true;

            CellGameContentSize contentSize;
		    u32 result = cellGameDataCheck(CELL_GAME_GAMETYPE_GAMEDATA, XEX_TITLE_ID, &contentSize);

            if(result == CELL_GAME_RET_NONE)
		    {
			    char tmp_contentInfoPath[CELL_GAME_PATH_MAX];
			    char tmp_usrdirPath[CELL_GAME_PATH_MAX];
			    CellGameSetInitParams init;
			    memset(&init, 0x00, sizeof(init));
			    strncpy(init.title,   "Grand Theft Auto V", CELL_GAME_SYSP_TITLE_SIZE - 1) ;
			    strncpy(init.titleId, XEX_TITLE_ID, CELL_GAME_SYSP_TITLEID_SIZE - 1) ;
			    strncpy(init.version, "01.00", CELL_GAME_SYSP_VERSION_SIZE - 1) ;
			    result = cellGameCreateGameData( &init, tmp_contentInfoPath, tmp_usrdirPath ) ;

			    if(result != CELL_OK)
			    {
                    directoryAvailable = false;
			    }
		    }
            else if(result != CELL_GAME_RET_OK)
            {
                directoryAvailable = false;
            }

            if(directoryAvailable)
            {
                prefix = "gamedata:/";

                char contentInfoPath[CELL_GAME_PATH_MAX];
                char cellGameDataPath[CELL_GAME_PATH_MAX];

                result = cellGameContentPermit(contentInfoPath, cellGameDataPath);
            }
            else
            {
                const_cast<netLogImpl*>(this)->m_TargetType = LOG_TARGET_DEFAULT;

                gnetDebug3("Failed to create game data directory for %s, reverting back to default target directory", m_FileName);
            }
        }

        CellNetCtlInfo info = {0};
        const int err = cellNetCtlGetInfo(CELL_NET_CTL_INFO_IP_ADDRESS, &info);

        if(0 <= err)
        {
            formatf(filename,
                    filenameSize,
                    "%s%s_%s%s",
                    prefix,
                    info.ip_address,
                    mainName,
                    extension);

            success = true;
        }
#elif RSG_ORBIS

        const char *prefix = "debug:/";
		if(PARAM_logfile.Get())
		{
			prefix = "";
		}

        if(m_TargetType == LOG_TARGET_LOCAL)
        {
			prefix = "/data/";
        }

		SceNetCtlInfo info;
		int err = sceNetCtlGetInfo(SCE_NET_CTL_INFO_IP_ADDRESS, &info);

        if(0 <= err)
        {
			const char *logPrefix = "";
			if(PARAM_netlogprefix.Get(logPrefix))
			{
				formatf(filename,
					filenameSize,
					"%s%s_%s%s",
					prefix,
					logPrefix,
					mainName,
					extension);
			}
			else
			{
				formatf(filename,
					filenameSize,
					"%s%s_%s%s",
					prefix,
                    info.ip_address,
                    mainName,
                    extension);
			}
			success = true;
        }

#elif RSG_DURANGO

        const char *dirPrefix = "debug:/";
		if(PARAM_logfile.Get())
		{
			dirPrefix = "";
		}

        if(m_TargetType == LOG_TARGET_LOCAL)
        {
			dirPrefix = "T:/";
        }

        const char *logPrefix = "";
        if(PARAM_netlogprefix.Get(logPrefix))
        {
            formatf(filename,
                    filenameSize,
                    "%s%s_%s%s",
                    dirPrefix,
                    logPrefix,
                    mainName,
                    extension);
        }
        else
        {
            formatf(filename,
                    filenameSize,
                    "%s%s%s",
                    dirPrefix,
                    mainName,
                    extension);
        }

        success = true;
#elif RSG_PC

        const char *logPrefix = "";
        if(PARAM_netlogprefix.Get(logPrefix))
        {
            formatf(filename,
                    filenameSize,
                    "%s_%s%s",
                    logPrefix,
                    mainName,
                    extension);
        }
        else
        {
            formatf(filename,
                    filenameSize,
                    "%s%s",
                    mainName,
                    extension);
        }

        success = true;

#endif  //__XENON
		
		if(!success)
		{
			safecpy(filename, m_FileName, filenameSize);
			success = true;
		}
		
#if __BANK
		const char *logFilePath = "";
		if(PARAM_logfile.Get(logFilePath))
		{			
			++RAGE_LOG_DISABLE;	// Disable the assert for the call to new within shared_ptr::Resetp
			std::string logFilePathStr(logFilePath);				
			std::string logFilePathFinal = logFilePathStr.substr(0, logFilePathStr.find_last_of("\\"));
			logFilePathFinal.append("\\");
			logFilePathFinal.append(filename);
			std::strcpy(filename, logFilePathFinal.c_str());
			--RAGE_LOG_DISABLE; // Re-enable the assert		
		}		
#endif //#if __BANK
    }

    return success;
}

bool netLogImpl::Open()
{
    if(PARAM_nonetlogs.Get())
    {
        return false;
    }

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return false;
		
	m_LogHandle = OpenInternal();

	gnetAssertf(IsOpen(), "%s could not be opened!", m_FileName);
	return IsOpen();
}

netLogFileAccessInterface::LogHandle netLogImpl::OpenInternal()
{
	gnetDebug1("OpenInternal %s", m_FileName);

	char filename[MAX_LOG_FILE_NAME];
	safecpy(filename, m_FileName, sizeof(filename));
	AssertVerify(this->SetUpFilename(filename, MAX_LOG_FILE_NAME));

	gnetAssertf(sm_FileAccessInterface, "File access interface not set. Please call netLog::SetFileAccessInterface when your game is initialised!");

	if(sm_FileAccessInterface)
	{
		return sm_FileAccessInterface->OpenFile(filename, m_OpenType);			
	}
	
	return 0;
}

void netLogImpl::Close()
{
    if(m_LogHandle)
    {
        CloseInternal(m_LogHandle);
        m_LogHandle = 0;
    }
}

void netLogImpl::CloseInternal(netLogFileAccessInterface::LogHandle logHandle)
{
	if(logHandle)
	{
		gnetDebug1("CloseInternal %s", m_FileName);

		gnetAssertf(sm_FileAccessInterface, "File access interface not set. Please call netLog::SetFileAccessInterface when your game is initialised!");

		if(sm_FileAccessInterface)
		{
			sm_FileAccessInterface->CloseFile(logHandle);
		}
	}
}

bool netLogImpl::IsOpen()
{
    return m_LogHandle != 0;
}

void netLogImpl::Log(const char *logText, ...)
{
    if(PARAM_nonetlogs.Get())
        return;

    if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

    char buffer[MAX_LOG_STRING];

    va_list args;
    va_start(args,logText);
    vsprintf(buffer,logText,args);
    va_end(args);

    // parse leading and trailing line breaks out of the log buffer
    unsigned leadingLineBreaks  = 0;
    unsigned trailingLineBreaks = 0;

    size_t textLength = strlen(buffer);
    
    if(textLength == 0)
    {
        return;
    }

    const char *left  = buffer;
    const char *right = buffer + textLength - 1;

    bool newLineFound = true;

    while((left < right) && newLineFound)
    {
        newLineFound = false;

        if(*(right-1) == '\r' && *(right)=='\n')
        {
            trailingLineBreaks++;
            newLineFound = true;
            right-=2;
        }
    }

    newLineFound = true;

    while((left < right) && newLineFound)
    {
        newLineFound = false;

        if(*left == '\r' && *(left+1)=='\n')
        {
            leadingLineBreaks++;
            newLineFound = true;
            left+=2;
        }
    }

    // keep the first newline at the end of the log text as we don't want the frame count
    // appended to the end of all log output
    trailingLineBreaks = trailingLineBreaks > 0 ? (trailingLineBreaks - 1) : trailingLineBreaks;

    // Remove the original line breaks from the log text by pointing after
    // the last of the leading line breaks and NULL terminating the first of
    // the trailing line breaks
    if(AssertVerify(textLength > (leadingLineBreaks * 2)) && AssertVerify(textLength >= (trailingLineBreaks * 2)))
    {
        const char *logText = buffer + (leadingLineBreaks * 2);
        buffer[textLength - (trailingLineBreaks * 2)] = '\0';

        // add leading line breaks
        while(leadingLineBreaks > 0)
        {
            LineBreak();
            leadingLineBreaks--;
        }

        if(m_PackedLogDataWriter->AddStandardLogData(logText) == false)
        {
            // we've filled our buffer, swap buffers so it can be written
            if(TryToSwapBuffers() == false)
            {
                HandleBufferSwapFailure();
            }

            gnetVerifyf(m_PackedLogDataWriter->AddStandardLogData(logText), "Unexpectedly failed adding log data!");
        }

        while(trailingLineBreaks > 0)
        {
            LineBreak();
            trailingLineBreaks--;
        }
    }
}

void netLogImpl::LineBreak()
{
    if(PARAM_nonetlogs.Get())
        return;

	if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;
    
    if(m_PackedLogDataWriter->AddStandardLogData("\r\n") == false)
    {
        // we've filled our buffer, swap buffers so it can be written
        if(TryToSwapBuffers() == false)
        {
            HandleBufferSwapFailure();
        }

        gnetVerifyf(m_PackedLogDataWriter->AddStandardLogData("\r\n"), "Unexpectedly failed adding log data!");
    }
}

void netLogImpl::Flush(bool waitForFlushToComplete)
{
	sysIpcThreadId threadId = (sysIpcThreadId)sysIpcGetCurrentThreadId();
	if (threadId == ms_SpoolThreadID)
	{
		gnetWarning("netLogImpl::Flush() Called from the spool thread!");
		return;
	}

    if(IsOpen())
    {
        TryToSwapBuffers();

        m_FlushRequested = true;

        if(waitForFlushToComplete)
        {
            unsigned readBufferSize = sysInterlockedRead(&m_ReadBufferSize);

            while(readBufferSize > 0)
            {
                gnetDebug3("Waiting for %s to be flushed...", m_FileName);

                // Note that this bool is not checked atomically, this is just used as a rough safety check to prevent the semaphore being signalled too often
                if(ms_WaitingToSpoolData)
                {
                    sysIpcSignalSema(ms_SpoolDataReadySema);
                    sysIpcSleep(5);
                }

                readBufferSize = sysInterlockedRead(&m_ReadBufferSize);
            }

            if(gnetVerifyf(m_PackedLogDataWriter, "Invalid packed log data writer!") &&
               m_PackedLogDataWriter->GetSizeWritten() > 0)
            {
                Flush(waitForFlushToComplete);
                gnetAssertf(m_PackedLogDataWriter->GetSizeWritten() == 0, "Write buffer size should be 0!");
            }
        }
    }
}

void netLogImpl::SetUsesStringTable()
{
    m_UsesStringTable = true;
}

void netLogImpl::SetBlockingMode(LogBlockingMode blockingMode)
{
    m_BlockingMode = blockingMode;
}

void netLogImpl::WriteDataValue(const char *dataName, const char *dataValue, ...)
{
    if(PARAM_nonetlogs.Get())
        return;

	if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

	char buffer[MAX_LOG_STRING];
	va_list args;
	va_start(args,dataValue);
	vsprintf(buffer,dataValue,args);
	va_end(args);

    if(m_UsesStringTable)
    {
        TryToAddToStringTable(dataName);
    }

    if(m_PackedLogDataWriter->AddWriteDataValueData(dataName, buffer) == false)
    {
        // we've filled our buffer, swap buffers so it can be written
        if(TryToSwapBuffers() == false)
        {
            HandleBufferSwapFailure();
        }

        gnetVerifyf(m_PackedLogDataWriter->AddWriteDataValueData(dataName, buffer), "Unexpectedly failed adding log data!");
    }
}

void netLogImpl::WriteMessageHeader(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName)
{
    if(PARAM_nonetlogs.Get())
        return;

	if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

    if(m_UsesStringTable)
    {
        TryToAddToStringTable(messageString1);
        TryToAddToStringTable(messageString2);
        TryToAddToStringTable(playerName);
    }

    if(m_PackedLogDataWriter->AddMessageHeaderData(received, sequence, messageString1, messageString2, playerName) == false)
    {
        // we've filled our buffer, swap buffers so it can be written
        if(TryToSwapBuffers() == false)
        {
            HandleBufferSwapFailure();
        }

        gnetVerifyf(m_PackedLogDataWriter->AddMessageHeaderData(received, sequence, messageString1, messageString2, playerName), "Unexpectedly failed to add message header data!");
    }
}

void netLogImpl::WritePlayerText(const char *type, const char *buffer, const char *playerName)
{
    if(PARAM_nonetlogs.Get())
        return;

	if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

    if(m_PackedLogDataWriter->AddPlayerTextData(type, buffer, playerName) == false)
    {
        // we've filled our buffer, swap buffers so it can be written
        if(TryToSwapBuffers() == false)
        {
            HandleBufferSwapFailure();
        }

        gnetVerifyf(m_PackedLogDataWriter->AddPlayerTextData(type, buffer, playerName), "Unexpectedly failed to add player text data!");
    }
}

void netLogImpl::WriteEventData(const char *eventName, const char *eventData, unsigned trailingLineBreaks)
{
    if(PARAM_nonetlogs.Get())
        return;

	if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

    if(m_PackedLogDataWriter->AddLogEventData(eventName, eventData, trailingLineBreaks) == false)
    {
        // we've filled our buffer, swap buffers so it can be written
        if(TryToSwapBuffers() == false)
        {
            HandleBufferSwapFailure();
        }

        gnetVerifyf(m_PackedLogDataWriter->AddLogEventData(eventName, eventData, trailingLineBreaks), "Unexpectedly failed to add log event data!");
    }
}

void netLogImpl::WriteNodeHeader(const char *nodeName)
{
    if(PARAM_nonetlogs.Get())
        return;

	if (m_Disable)
		return;

    if(sysBootManager::IsBootedFromDisc() && !PARAM_localnetlogs.Get())
        return;

    if(m_UsesStringTable)
    {
        TryToAddToStringTable(nodeName);
    }

    if(m_PackedLogDataWriter->AddNodeHeaderData(nodeName) == false)
    {
        // we've filled our buffer, swap buffers so it can be written
        if(TryToSwapBuffers() == false)
        {
            HandleBufferSwapFailure();
        }

        gnetVerifyf(m_PackedLogDataWriter->AddNodeHeaderData(nodeName), "Unexpectedly failed to add node header data!");
    }
}

void netLogImpl::PostNetworkUpdate()
{
    unsigned sizeWritten = m_PackedLogDataWriter->GetSizeWritten();

    if(sizeWritten > 0)
    {
        unsigned bufferSize  = m_PackedLogDataWriter->GetBufferSize();
        float    percentFull = (static_cast<float>(sizeWritten) / static_cast<float>(bufferSize)) * 100.0f;

        LOG_THROUGHPUT(gnetDebug3("%s, written %d/%d bytes (%f%%)", m_FileName, sizeWritten, bufferSize, percentFull));

        static const unsigned TIME_TO_TRY_BUFFER_SWAP = 1000;

        bool       timeToSwapBuffers = (sysTimer::GetSystemMsTime() - m_LastTimeBuffersSwapped) > TIME_TO_TRY_BUFFER_SWAP;
        bank_float swapThreshold     = 10.0f;

        if(percentFull > swapThreshold || timeToSwapBuffers)
        {
            TryToSwapBuffers();
        }
    }
}

#if __BANK

void netLogImpl::GetDebugStatisticsText(char *buffer, const unsigned bufferSize)
{
    if(gnetVerifyf(buffer, "Invalid buffer specified!") &&
       gnetVerifyf(bufferSize > 0, "Invalid buffer size specified"))
    {
        unsigned byteThroughput = GetThroughputInBytes();
        unsigned waitsPerSecond = GetWaitsPerSecond();

        const unsigned BUFFER_SIZE = 64;
        char throughPutText[BUFFER_SIZE];

        if(byteThroughput > 1024 * 1024)
        {
            formatf(throughPutText, BUFFER_SIZE, "%.2fMB", byteThroughput / (1024.0f * 1024.0f));
        }
        else if(byteThroughput > 1024)
        {
            formatf(throughPutText, BUFFER_SIZE, "%.2fK", byteThroughput / 1024.0f);
        }
        else
        {
            formatf(throughPutText, BUFFER_SIZE, "%d bytes", byteThroughput);
        }

        unsigned sizeWritten      = m_PackedLogDataWriter->GetSizeWritten();
        unsigned packedBufferSize = m_PackedLogDataWriter->GetBufferSize();
        float    percentFull      = (static_cast<float>(sizeWritten) / static_cast<float>(packedBufferSize)) * 100.0f;

        const char *readInProgress = "";

        if(sysInterlockedRead(&m_ReadBufferSize) != 0)
        {
            readInProgress = "Read In Progress";
        }

        const char *waitOrDrop = (m_BlockingMode == LOG_BLOCKING) ? "Waits" : "Drops";

        float memoryAllocated = (m_LogBufferSize * 2) / 1024.0f;
        formatf(buffer, bufferSize, "%s: Allocated %.2fK: Throughput/s %s: %s/s %d written %d/%d bytes (%.2f%%%%) %s", m_FileName, memoryAllocated, throughPutText, waitOrDrop, waitsPerSecond, sizeWritten, packedBufferSize, percentFull, readInProgress);
    }
}

#endif // __BANK

void netLogImpl::GetFileName(char *filename, u32 filenameSize) const
{
	strcpy_s(filename, filenameSize, m_FileName);
	SetUpFilename(filename, filenameSize);
}

bool netLogImpl::TryToSwapBuffers()
{
    bool success = false;

    unsigned dataToRead = sysInterlockedRead(&m_ReadBufferSize);

    if(dataToRead == 0 && gnetVerifyf(m_PackedLogDataWriter, "No packed log data writer!"))
    {
        if(gnetVerifyf(m_BufferMutex, "Log file buffer mutex does not exist!"))
        {
            if(gnetVerifyf(sysIpcLockMutex(m_BufferMutex),  "Failed to lock log file buffer mutex!"))
            {
                if(m_ReadBufferSize == 0)
                {
                    gnetVerifyf(m_PackedLogDataWriter->AddEndOfDataMarker(), "Failed to add EOF marker when swapping packed log data buffers!");

                    m_ThroughputInBytes += m_PackedLogDataWriter->GetThroughputInBytes();

                    char *tempBuffer = m_ReadBuffer;

                    m_ReadBuffer  = m_WriteBuffer;
                    m_WriteBuffer = tempBuffer;

                    m_ReadBufferSize  = m_PackedLogDataWriter->GetSizeWritten();
                    m_PackedLogDataWriter->SetBuffer(m_WriteBuffer, m_LogBufferSize);

                    if(sm_MaximumFileSize > 0)
                    {
                        m_BytesWritten += m_PackedLogDataReader->GetSizeWrittenToOuput();

                        if(m_BytesWritten >= sm_MaximumFileSize)
                        {
							LogOpenType oldOpenType							  = m_OpenType;
							u32         oldPreviousFileCount                  = m_PreviousFileCount;
							netLogFileAccessInterface::LogHandle oldLogHandle = m_LogHandle;

							m_PreviousFileCount++;
							if(m_PreviousFileCount == m_MaxFileCount)
							{
								m_PreviousFileCount = 0;
								m_OpenType          = LOGOPEN_CREATE;
							}
							
							netLogFileAccessInterface::LogHandle newHandle = OpenInternal();

							// If we opened the file successfully, then cache the current log handle, set it as the old one, 
							// close the old file and finally restore the cached current handle. 
							if (newHandle != 0)
							{
								CloseInternal(m_LogHandle);
								m_LogHandle = newHandle;
								m_BytesWritten = 0;
							}     
							else
							{
								m_OpenType          = oldOpenType;
								m_LogHandle		    = oldLogHandle;
								m_PreviousFileCount = oldPreviousFileCount;
							}
                        }
                    }

                    m_LastTimeBuffersSwapped = sysTimer::GetSystemMsTime();

                    success = true;

                    LOG_THROUGHPUT(gnetDebug3("Swapping buffers for %s", m_FileName));

                    sysIpcSignalSema(ms_SpoolDataReadySema);
                }

                gnetVerifyf(sysIpcUnlockMutex(m_BufferMutex), "Failed to unlock log file buffer mutex!");
            }
        }
    }

    return success;
}

void netLogImpl::HandleBufferSwapFailure()
{
    // If the file has never been added to the spool WaitAndSwapBuffers would block forever
    if(m_BlockingMode == LOG_NON_BLOCKING || !m_AddedToSpool)
    {
        // we've failed to swap the buffers and the log file is set to non-blocking,
        // we now have to throw away the new buffer of data to prevent stalling the game
        gnetDebug3("Unable to swap buffers - dropping log data for %s...", m_FileName);

        m_PackedLogDataWriter->SetBuffer(m_WriteBuffer, m_LogBufferSize);
        
        WriteEventData("DROPPING_LOG_DATA", "", 0);
    }
    else
    {
        gnetAssertf(m_BlockingMode == LOG_BLOCKING, "Unexpected log file blocking mode");

        WaitAndSwapBuffers();
    }

    m_WaitsPerSecond++;
}

void netLogImpl::WaitAndSwapBuffers()
{
    // we're running ahead of the spool thread, we have to wait
    gnetDebug3("Performance warning! Waiting for log spool thread to flush...");

    gnetDebug3("Waiting for %s to be flushed...", m_FileName);

    do
    {
        unsigned waitCount = 0;

        unsigned readBufferSize = sysInterlockedRead(&m_ReadBufferSize);

        while(readBufferSize > 0)
        {
            const unsigned numLoopsBeforeWarning = 3;
            if(waitCount == numLoopsBeforeWarning)
            {
                gnetDebug3("Logging for %s blocking for a long time!", m_FileName);
            }

            // Note that this bool is not checked atomically, this is just used as a rough safety check to prevent the semaphore being signalled too often
            if(ms_WaitingToSpoolData)
            {
                sysIpcSignalSema(ms_SpoolDataReadySema);
                sysIpcSleep(5);
            }

            readBufferSize = sysInterlockedRead(&m_ReadBufferSize);

            waitCount++;
        }
    }
    while(TryToSwapBuffers() == false);

    gnetDebug3("Finished waiting for %s to be flushed...", m_FileName);
}

unsigned netLogImpl::GetThroughputInBytes()
{
    UpdateStatistics();
    return m_LastThroughputInBytes;
}

unsigned netLogImpl::GetWaitsPerSecond()
{
    UpdateStatistics();
    return m_LastWaitsPerSecond;
}

void netLogImpl::UpdateStatistics()
{
    const unsigned STATS_UPDATE_INTERVAL = 1000;
    u32 currentTime = sysTimer::GetSystemMsTime();

    if((currentTime - m_StatsStartTime) > STATS_UPDATE_INTERVAL)
    {
        m_StatsStartTime        = currentTime;
        m_LastThroughputInBytes = m_ThroughputInBytes;
        m_LastWaitsPerSecond    = m_WaitsPerSecond;
        m_ThroughputInBytes     = 0;
        m_WaitsPerSecond        = 0;
    }

    m_ThroughputInBytes += m_PackedLogDataWriter->GetThroughputInBytes();
}

void netLogImpl::TryToAddToStringTable(const char *string)
{
    if(!sm_StringTable.IsInitialised())
    {
        sm_StringTable.Init(sm_StringTableHeapSize);
    }

    unsigned id = 0;
    if(!sm_StringTable.FindString(string, id))
    {
        sm_StringTable.AddString(string);
    }
}

} // namespace rage

#endif // ENABLE_NETWORK_LOGGING
