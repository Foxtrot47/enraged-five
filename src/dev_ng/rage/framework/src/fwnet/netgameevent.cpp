//
// netgameevent.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/netgameevent.h"

#include "fwnet/netinterface.h"
#include "fwnet/neteventid.h"
#include "fwnet/neteventmgr.h"
#include "fwnet/netlogutils.h"
#include "fwnet/netplayermgrbase.h"
#include "fwnet/optimisations.h"
#include "fwsys/timer.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

FW_INSTANTIATE_BASECLASS_POOL(netGameEvent, MAX_ACTIVE_NETWORK_EVENTS, atHashString("netGameEvent",0xf4794a41), LARGEST_NETWORKEVENT_CLASS);

netGameEvent::netGameEvent( const NetworkEventType eventType,
                            bool bReply,
                            bool bSequential,
                            netPlayer *owner) :
m_EventType(eventType),
m_EventId(NULL),
m_EventSequence(0),
m_OwnerPlayer(owner),
m_RequiresReply(bReply),
m_IsSequential(bSequential),
m_HasBeenSent(false),
m_AlreadyPackedFlags(0),
m_TimeLastSent(0),
m_FlaggedForRemoval(false)
{
    if(m_IsSequential)
    {
        m_EventSequence = netInterface::GetEventManager().GetNextSequence(m_EventType);
    }

    if(m_OwnerPlayer == 0)
    {
        m_OwnerPlayer = netInterface::GetLocalPlayer();
    }
}

netGameEvent::~netGameEvent()
{
    if (m_EventId)
    {
        m_EventId->StopUsing();
    }
}

void netGameEvent::SetEventId( netEventID* pID )
{
    Assert(pID && pID->IsFree());
    m_EventId = pID;
    m_EventId->Use();
}

void netGameEvent::SetHasAlreadyBeenPacked(const netPlayer& player)
{
    m_AlreadyPackedFlags |= (1<<player.GetPhysicalPlayerIndex());
}

void netGameEvent::ResetHasAlreadyBeenPacked(const netPlayer& player)
{
    m_AlreadyPackedFlags &= (~(1<<player.GetPhysicalPlayerIndex()));
}

bool netGameEvent::HasAlreadyBeenPacked(const netPlayer& player) const
{
    return (m_AlreadyPackedFlags & (1<<player.GetPhysicalPlayerIndex()))!= 0;
}

bool netGameEvent::TimeToResend(u32 currTime)
{
    if (static_cast<int>(currTime) - static_cast<int>(m_TimeLastSent) > DEFAULT_TIME_TO_RESEND)
    {
        m_TimeLastSent = currTime;
        return true;
    }

    return false;
}

void netGameEvent::DelaySending(u32 delay)
{
#if ENABLE_NETWORK_LOGGING
	// log the event data
    NetworkLogUtils::WriteLogEvent(netInterface::GetEventManager().GetLog(), "DELAY_SENDING", "%s (%d ms)", GetEventName(), delay);
#endif // ENABLE_NETWORK_LOGGING

    int newTimeLastSent = static_cast<int>(fwTimer::GetSystemTimeInMilliseconds()) + static_cast<int>(delay) - DEFAULT_TIME_TO_RESEND;

    if(newTimeLastSent < 0)
    {
        newTimeLastSent = 0;
    }

    m_TimeLastSent = newTimeLastSent;
}

const char* netGameEvent::GetEventNameAndNumber() const
{
    static char nameStr[100];

    sprintf(nameStr, "%s_%d", GetEventName(), GetEventId().GetID());

    return nameStr;
}

} // namespace rage
