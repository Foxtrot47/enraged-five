// 
// netgameobjectbase.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
// 

#ifndef NETWORK_GAMEOBJECT_BASE_H
#define NETWORK_GAMEOBJECT_BASE_H

#include "fwnet/nettypes.h"

namespace rage
{

class netObject;

//PURPOSE
// Base class for game objects to be synchronised by the network object manager
class netGameObjectBase
{
public:

    virtual ~netGameObjectBase() {}

    //PURPOSE
    //  Creates a network object of the required type for the game object.
    //PARAMS
    //  objectID    - The network object ID for the network object to create (for more details see netobjectidmgr.cpp/.h)
    //  playerIndex - The player index of the initial owner of the network object to create
    //  localFlags  - The local network object flags for the network object to create (for more details see netobject.cpp/.h)
    //  globalFlags - The global network object flags for the network object to create (for more details see netobject.cpp/.h)
    //  numPlayers  - The maximum number of players this object can be cloned on
    virtual netObject* CreateNetworkObject(const ObjectId,
                                           const PhysicalPlayerIndex ,
                                           const NetObjFlags ,
                                           const NetObjFlags ,
                                           const unsigned ) = 0;

    //PURPOSE
    //  Returns a unique identifier for the type of network object associated with the game object.
    virtual unsigned GetNetworkObjectType() = 0;

    //PURPOSE
    //  Returns the network object associated with an individual instance of a game object
    virtual netObject *GetNetworkObject() const = 0;
};

//PURPOSE
// A wrapper class for passing game objects to the object manager without introducing unnecessary dependencies.
template <class T> class netGameObjectWrapper : public netGameObjectBase
{
public:

    netGameObjectWrapper(T *gameObject) :
    m_GameObject(gameObject)
    {
    }

    //PURPOSE
    //  Creates a network object of the required type for the game object.
    //PARAMS
    //  objectID    - The network object ID for the network object to create (for more details see netobjectidmgr.cpp/.h)
    //  playerIndex - The player index of the initial owner of the network object to create
    //  localFlags  - The local network object flags for the network object to create (for more details see netobject.cpp/.h)
    //  globalFlags - The global network object flags for the network object to create (for more details see netobject.cpp/.h)
    //  numPlayers  - The maximum number of players this object can be cloned on
    netObject* CreateNetworkObject(const ObjectId objectID,
                                   const PhysicalPlayerIndex playerIndex,
                                   const NetObjFlags localFlags,
                                   const NetObjFlags globalFlags,
                                   const unsigned numPlayers)
    {
        return m_GameObject->CreateNetworkObject(objectID, playerIndex, localFlags, globalFlags, numPlayers);
    }

    //PURPOSE
    //  Returns a unique identifier for the type of network object associated with the game object.
    unsigned GetNetworkObjectType()
    {
        return m_GameObject->GetNetworkObjectType();
    }

    //PURPOSE
    //  Returns the network object associated with an individual instance of a game object
    netObject *GetNetworkObject() const
    {
        return m_GameObject->GetNetworkObject();
    }

    //PURPOSE
    //  Returns the game object
	T const* GetGameObject() const
	{
		return m_GameObject;
	}

private:

    // The game object being wrapped
    T *m_GameObject;
};

}   // namespace rage

#endif  // NETWORK_GAMEOBJECT_BASE_H
