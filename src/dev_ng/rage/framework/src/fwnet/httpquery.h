//
// filename:	HttpQuery.h
// description: Mechanism to run http queries
// 
// This allows the game to communicate with the various rest servers.
//
//
#ifndef INC_HTTP_QUERY_H_
#define INC_HTTP_QUERY_H_

#if !__FINAL && !__PROFILE

// --- Include Files ------------------------------------------------------------

// C headers

// Rage headers
#include "atl/string.h"
#include "data/growbuffer.h"
#include "system/ipc.h"
#include "system/criticalsection.h"
#include "net/http.h"

// Game headers

// --- Defines ------------------------------------------------------------------

#define MAX_HTTP_QUERIES	16

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// Forward declare Rage classes
namespace rage {
	class parTree;
	class fiStream;

// Class Declarations
class fwHttpQuery
{
public:
	fwHttpQuery(const char* addr, const char* baseUri, bool useRageHttp);
	fwHttpQuery(const char* addr, const char* baseUri, const char* username, const char* password, bool useRageHttp);
	virtual ~fwHttpQuery();

	bool Init(const char* search, int timeout=10, int bufferSize=32*1024);
	void InitPost(const char* search, int timeout=10, int bufferSize=32*1024);
	bool CommitPost();
	void AppendXmlContent(const char* pXml, int length);

	void Shutdown();
	bool Pending();
	virtual void OnFinish() {}
	bool Succeeded() {return m_status.Succeeded();}
	void WriteBufferToFile(const char* pFile);

	fiStream* CreateStream();
	parTree* CreateTree();

	static void InitHttpQueryQueue();

protected:
	void AddToHttpQueryQueue();
	void Update();
	void Finish();

	static void HttpQueryUpdateThread(void* );

    static sysIpcSema sm_NonEmptySignal;
    static sysCriticalSectionToken sm_queryListLock;
    static atFixedArray<fwHttpQuery*, MAX_HTTP_QUERIES> sm_queryList;

	const atString m_address;
	const atString m_baseUri;
	const atString m_username;
	const atString m_password;
	const bool m_useRageHttp;

	netHttpRequest m_HttpRequest;
	netStatus m_status;
	bool m_finished;
	datGrowBuffer m_buffer;
};

class CHttpQuerySelfDeleting : public fwHttpQuery
{
public:
	CHttpQuerySelfDeleting(const char* addr, const char* baseUri, const char* username, const char* password, bool useRageHttp) : fwHttpQuery(addr, baseUri, username, password, useRageHttp) { }

	void OnFinish() { delete this; }
};

class CHttpQuerySaveToFile : public fwHttpQuery
{
public:
	CHttpQuerySaveToFile(const char* addr, const char* baseUri, const char* username, const char* password, const char* pFilename, bool useRageHttp) : fwHttpQuery(addr, baseUri, username, password, useRageHttp), m_filename(pFilename) { }

	void OnFinish()
	{
		if(Succeeded())
		{
			WriteBufferToFile(m_filename);
		}
		delete this;
	}
protected:
	atString m_filename;
};

// --- Globals ------------------------------------------------------------------

} // namespace rage

#endif // !__FINAL && !__PROFILE

#endif // INC_HTTP_QUERY_H_


