//
// netobjectidmgr.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_OBJECT_ID_MGR_H
#define NETWORK_OBJECT_ID_MGR_H

// rage headers
#include "atl/queue.h"

// framework headers
#include "fwnet/netinterface.h"
#include "fwnet/nettypes.h"

namespace rage
{

class datBitBuffer;
class netEvent;
class netEventFrameReceived;
class netLoggingInterface;
class netPlayer;

class netObjectIDMgr
{
public:

    static const unsigned int MAX_NUM_OBJECT_IDS_PER_PLAYER = 250;
    static const unsigned int MAX_TOTAL_OBJECT_IDS          = (MAX_NUM_OBJECT_IDS_PER_PLAYER * MAX_NUM_PHYSICAL_PLAYERS);

	enum
	{
		DEFAULT_REQUEST_ID_THRESHOLD			= 50,
		DEFAULT_NUM_IDS_TO_REQUEST				= 50,
		DEFAULT_MIN_TIME_BETWEEN_REQUESTS		= 30 * 1000,
		DEFAULT_OBJECT_ID_RELEASE_THRESHOLD		= 250,
	};

    netObjectIDMgr();

    //PURPOSE
    // Initialises the manager
    //PARAMS
    // log - The logging object for the class to use for debugging information
    void Init(netLoggingInterface *log);

    //PURPOSE
    // Shuts down the manager
    void Shutdown();

    //PURPOSE
    // Updates down the manager
    void Update();

    //PURPOSE
    // Called when a new player joins the game
    //PARAMS
    // player - the player joining the game
    void PlayerHasJoined(const netPlayer &player);

    //PURPOSE
    // Called when an existing player leaves the game
    //PARAMS
    // player - the player leaving the game
    void PlayerHasLeft(const netPlayer &player);

    //PURPOSE
    // Sets the object Id range index for this machine to use when allocating IDs
    //PARAMS
    // objectIdRangeIndex - The object Id range index
    void SetObjectIdRangeIndex(int objectIdRangeIndex);

    //PURPOSE
    // Returns whether we are waiting for object IDs
    bool IsWaitingForObjectIDs(bool includeBufferTime);

	//PURPOSE
	// Returns whether we are waiting for object IDs from a particular player
	//PARAMS
	// playerIndex - The player to check against
	bool IsWaitingForObjectIDs(ActivePlayerIndex playerIndex);

    //PURPOSE
    // Returns whether we can accept object ID freed events from a specific player
    //PARAMS
    // playerIndex - The physical player index of the player to check
    bool CanAcceptObjectIDEventsFrom(PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Returns next free object id
    ObjectId GetNewObjectId();

    //PURPOSE
    // Tries to free-up an object id if it was generated locally
    //PARAMS
    // objectID - The object ID to free
    void FreeObjectId(ObjectId objectID);

    //PURPOSE
    // Returns whether we have the specified number of free object IDs to allocate
    //PARAMS
    // count           - The number of object IDs to check whether available
    // forScriptObject - Indicates whether the request is for the creation of script objects
    bool HasFreeObjectIdsAvailable(u32 count, bool forScriptObject);

    //PURPOSE
    // Returns whether the specified object ID is on the free queue
    //PARAMS
    // objectID - The object ID to check whether free
    bool ObjectIDIsFree(ObjectId objectID);

    //PURPOSE
    // Returns the current number of free objectIDs
    u32 GetNumFreeObjectIDs();

    //PURPOSE
    // Removes an object ID from the free queue if it is on the free queue
    //PARAMS
    // objectID - The object ID to remove if present
    bool RemoveObjectIDIfExists(ObjectId objectID);

    //PURPOSE
    // Packs the specified bitBuffer with any object IDs owned locally within the specified peers default range
    //PARAMS
    // playerIndex - The player ID to pack object ID data to
    // bitBuffer   - The bit buffer to pack the data
    // log         - log file
    void PackObjectIDData(PhysicalPlayerIndex playerIndex, int idRangeOffsetIndex, datBitBuffer &bitBuffer, netLoggingInterface &log);

    //PURPOSE
    // Unpacks the specified bitBuffer with object ID data
    //PARAMS
    // playerIndex - The player ID to unpack object ID data from
    // bitBuffer   - The bit buffer to unpack the data from
    // log         - log file
    void UnpackObjectIDData(PhysicalPlayerIndex playerIndex, const datBitBuffer &bitBuffer, netLoggingInterface &log);

    //PURPOSE
    // Requests the specified number of object IDs from the free list
    //PARAMS
    // player                - The player requesting the object IDs
    // numObjectIDsRequested - The number of object IDs requested
    // returnedObjectIDs     - Storage for the returned IDs
    // numReturnedObjectIDs  - The number of IDs returned
    void RequestObjectIDs(const netPlayer &player, unsigned numObjectIDsRequested, ObjectId *returnedObjectIDs, unsigned &numReturnedObjectIDs);

	//PURPOSE
	// Tweak tunable values for object Id thresholds
	static void SetRequestIdThreshold(const unsigned requestIdThreshold);
	static void SetNumIdsToRequest(const unsigned numIdsToRequest);
	static void SetMinTimeBetweenRequests(const unsigned minTimeBetweenRequests);
	static void SetObjectIdReleaseThreshold(const unsigned objectIdReleaseThreshold);

private:

    netObjectIDMgr(const netObjectIDMgr &);

    netObjectIDMgr &operator=(const netObjectIDMgr &);

    //PURPOSE
    // Handles an incoming network message
    //PARAMS
    // messageData - Data describing the incoming message
    void OnMessageReceived(const ReceivedMessageData &messageData);

    //PURPOSE
    // Message handler for an request object IDs message
    //PARAMS
    // messageData - Data describing the incoming message
    void HandleRequestObjectIdsMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Message handler for an inform object IDs message
    //PARAMS
    // messageData - Data describing the incoming message
    void HandleInformObjectIdsMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Private implementation of the free object ID function
    //PARAMS
    // objectID - The object ID to free
    bool FreeObjectIdPrivate(ObjectId objectID);

    //PURPOSE
    // Release any excess object IDs if we own too many
    void ReleaseExcessObjectIDs();

    //PURPOSE
    // Tries to allocates the initial object ID range available to this machine
    bool TryToAllocateInitialObjectIDs();

    // Event handler wrapper object for connection manager events
    NetworkPlayerEventDelegate m_Dlgt;

    bool                                    m_Initialised;         // Indicates whether the manager has been initialised
    bool                                    m_InitialIDsAllocated; // Indicates whether the initial object IDs have been allocated
    int                                     m_ObjectIdRangeIndex;  // Indicates the range of IDs this machine owns
    u32                                     m_WaitingForObjectIDs; // Bitfield indicating which players we are waiting for object IDs from
    u32                                     m_LastObjectIDmessage; // Timestamp the last object ID message was received
    u32                                     m_TimeOfLastIDRequest; // Time last request for object IDs was sent
    atQueue<ObjectId, MAX_TOTAL_OBJECT_IDS> m_FreeObjectIdQueue;   // Queue of free object IDs
    netLoggingInterface                    *m_Log;                 // Log file for this class

	static unsigned ms_RequestIdThreshold;
	static unsigned ms_NumIdsToRequest;
	static unsigned ms_MinTimeBetweenRequests;
	static unsigned ms_ObjectIdReleaseThreshold;
};

} // namespace rage

#endif  // NETWORK_OBJECT_ID_MGR_H
