// 
// netSyncNode.h 
// 
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved. 
// 

#ifndef NETWORK_SYNC_NODE_H
#define NETWORK_SYNC_NODE_H

#include "fwnet/nettypes.h"
#include "fwnet/netsyncdata.h"

//rage includes
#include "atl/inlist.h"
#include "data/base.h"
#include "data/autoid.h"

namespace rage
{
class netLoggingInterface;
class netSyncTree;
class netSyncParentNode;
class netSyncTreeTargetObject;
class netLogDisplay;

typedef unsigned SerialiseModeFlags;
typedef unsigned ActivationFlags;

// serialise modes
enum 
{
	SERIALISEMODE_CREATE					= (1<<0),
	SERIALISEMODE_UPDATE					= (1<<1),
	SERIALISEMODE_MIGRATE					= (1<<2),
	SERIALISEMODE_CRITICAL					= (1<<3),
	SERIALISEMODE_FORCE_SEND_OF_DIRTY		= (1<<4),
	SERIALISEMODE_CHECKSUM					= (1<<5),
	SERIALISEMODE_VERIFY					= (1<<6),

	SERIALISEMODE_ALL						= SERIALISEMODE_CREATE|SERIALISEMODE_UPDATE|SERIALISEMODE_MIGRATE|SERIALISEMODE_CRITICAL|SERIALISEMODE_FORCE_SEND_OF_DIRTY|SERIALISEMODE_CHECKSUM|SERIALISEMODE_VERIFY
};

//PURPOSE
// Interface class for setting/getting data to serialise node data.
class netINodeDataAccessor
{
public:

    AUTOID_DECL_ROOT(netINodeDataAccessor);

protected:

    virtual ~netINodeDataAccessor() {}
};

// this define should be placed in the class definition for each data accessor
#define DATA_ACCESSOR_ID_DECL(name)\
	AUTOID_DECL(name, netINodeDataAccessor)\
    static unsigned DATA_ACCESSOR_ID() { return name::GetAutoId(); }\

// this define should be placed in the corresponding .cpp file for each DATA_ACCESSOR_ID_DECL
#define DATA_ACCESSOR_ID_IMPL AUTOID_IMPL


///////////////////////////////////////////////////////////////////////////////////////
//
// A network sync tree is a tree of sync nodes and is used to process data held in
// a message buffer. There are two types of nodes: parent and data nodes. Parent nodes 
// manage a list of children and flag whether their children have any data to read or
// write. Data nodes are leaf nodes and represent a block, or unit, of data used by a 
// network object. These have no children and read or write their data to a message buffer.
//
// The reason for the tree is to reduce the amount of data sent in an update (the number of
// guard bits sent) and to simplify the process of adding or rearranging existing data. To 
// add new data we can just create a new node, specify what data it serializes, then add it 
// to the tree at the appropriate place. 
//
// Only one static instance of the tree exists for each network object type. This is so 
// that the tree can be used to process data for an object that does not exist, or it
// can be used by network bots, which are not real network objects. The netSyncData
// used by the nodes must be dynamically allocated out with the tree and held in the 
// network object that is using the tree. The tree stores a pointer to the current object
// that is using it and the nodes use this pointer to get at the network sync data.
//
///////////////////////////////////////////////////////////////////////////////////////

//PURPOSE
// Base class for sync tree nodes.
class netSyncNodeBase
{
public:

	netSyncNodeBase();
	netSyncNodeBase(SerialiseModeFlags serFlags, SerialiseModeFlags conditionalFlags, ActivationFlags actFlags);

	virtual ~netSyncNodeBase() {}

	netSyncTree*			GetParentTree() const { return m_ParentTree; }
	netSyncParentNode*		GetParentNode() const { return m_ParentNode; }
	void					SetParentTree(netSyncTree* pParentTree);
	void					SetParentNode(netSyncParentNode* pParentNode);

	void					SetSerialiseModeFlags(SerialiseModeFlags serFlags) { m_SerialiseModeFlags = serFlags; } 
	SerialiseModeFlags		GetSerialiseModeFlags() const { return m_SerialiseModeFlags; }

	void					SetConditionalFlags(SerialiseModeFlags condFlags) { m_ConditionalFlags = condFlags; } 
	SerialiseModeFlags		GetConditionalFlags() const { return m_ConditionalFlags; }

	void					SetActivationFlags(ActivationFlags actFlags) { m_ActivationFlags = actFlags; } 
	ActivationFlags			GetActivationFlags() const { return m_ActivationFlags; }

	virtual bool			GetIsDataNode() const	 { return false; }
	virtual bool			GetIsParentNode() const { return false; }
	
#if !__NO_OUTPUT
	const char*				GetNodeName() const { return m_NodeName; }
	void					SetNodeName(const char* nodeName);
#endif

	//PURPOSE
	//	Returns whether this node can potentially write any data for a given serialise mode
	bool UsedBySerialiseMode(SerialiseModeFlags serFlags) const { return (m_SerialiseModeFlags & serFlags) != 0; }

	//PURPOSE
	//	Returns whether this node has any conditional data to write for a given serialise mode, if false the data is always written.
	bool GetIsConditional(SerialiseModeFlags serFlags) const { return (m_ConditionalFlags & serFlags) != 0; }

	//PURPOSE
	//	Returns whether this node as active (any of the activation flags must be set on this node)
	bool GetIsActive(ActivationFlags actFlags) const { return (m_ActivationFlags==0 || (m_ActivationFlags & actFlags)); }

	//PURPOSE
	// Called when the node is added to the tree.
	virtual void InitialiseNode() {}

	//PURPOSE
	// Called when the node is removed from the tree.
	virtual void ShutdownNode() {}

	// pure virtual functions to be implemented by derived classes:

	//PURPOSE
	// Initialises the data start position in the sync buffers & data index for this node 
	//PARAMS
	//  dataPosition - the current data position in the sync buffers, added to by each data node 
	//  nodeIndex	 - the current data node index, incremented by each data node
	//  syncNodeIndex	 - the current data node index, incremented by each sync update data node
	//  maxNumHeaderBits - adds the maximum number of header bits written by this node and its children to the existing value
	virtual void InitialiseDataNodesInTree(unsigned& dataPosition, unsigned& nodeIndex, unsigned& syncNodeIndex, unsigned &maxNumHeaderBits) = 0;

 	//PURPOSE
	//	Writes the node data to a bit buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	//	bitBuffer	- the bit buffer.
	//  currTime    - the current sync time
	//  pLog		- if this is set the message buffer data is written to the log as it is written
	//	player		- the player we are sending the data to.
	//  pNodeFlags	- A bitfield where each set bit corresponds to a data node that wrote some data to the message buffer. For update messages this is
	//				  stored and passed into ProcessAck when an ack arrives from another machine. 
	//  maxNumHeaderBitsRemaining - The maximum number of header bits that could still be written during this write operation (used to ensure there is space in the bit buffer)
	// RETURNS
	//	true if any data node data was written
	virtual bool Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, const unsigned currTime, 
					   netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags, unsigned &maxNumHeaderBitsRemaining) = 0;

	//PURPOSE
	//	Reads the node data from a bit buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	bitBuffer	- the bit buffer received in an update message
	//  pLog		- if this is set the message buffer data is written to the log as it is read
	// RETURNS
	//	true if any data node data was read
	virtual bool Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog) = 0;

	//PURPOSE
	//  Returns the size of data that would be written for the specified serialisation mode, activation flags and target object
	//PARAMS
	//  serialisationMode   - the serialisation mode, (e.g. update, create, migrate) 
	//  activationFlags     - flags to activate or deactivate certain nodes
	//  targetObject        - the object using the tree
	virtual unsigned GetDataSize(SerialiseModeFlags        serialisationMode,
								 ActivationFlags           activationFlags,
								 netSyncTreeTargetObject  &targetObject) = 0;

    //PURPOSE
    //	Calculates the maximum data size that can be written for the node with the specified serialisation mode and
    //  activation flags. This is the amount of data that would be written if the node is dirty and ready to send.
    //  Optionally writes the data size of the node to the TTY output for debugging purposes
    //PARAMS
    //  serMode		- the serialisation mode, (eg update, create, migrate) 
    //  actFlags	- flags to activate or deactivate certain nodes
    virtual unsigned GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool outputToTTY) = 0;

#if __BANK
	//PURPOSE
	//	Adds a bank widget for this node
	virtual void AddWidgets(bkBank*) {}

	//PURPOSE
	//	Displays the node's data on screen 
	virtual void DisplayNodeInformation(netSyncTreeTargetObject*, netLogDisplay&) {}
#endif

public:

	inlist_node<netSyncNodeBase> m_ListLink;				

protected:

	netSyncTree*				m_ParentTree;			// the tree the node is a part of
	netSyncParentNode*			m_ParentNode;			// the parent node that this node is a child of
	SerialiseModeFlags			m_SerialiseModeFlags;	// flags indicating whether this node is used by each serialisation mode
	SerialiseModeFlags			m_ConditionalFlags;		// flags indicating whether this node is conditional for each serialisation mode
	ActivationFlags				m_ActivationFlags;		// the activation flags for this node, these are used to activate or deactivate nodes when serialising

#if !__NO_OUTPUT
	static const int			NODE_NAME_LEN = 50;
	char						m_NodeName[NODE_NAME_LEN];	//	the name of the node (used for debugging)
#endif

private:

    netSyncNodeBase(const netSyncNodeBase &);
    netSyncNodeBase &operator=(const netSyncNodeBase &);
};

//PURPOSE
// Parent sync tree node, has no data associated with it and may have other parent nodes or
// data nodes as its children. Its only responsibility is to flag whether its children
// have any data to read or write.
class netSyncParentNode : public netSyncNodeBase
{
public:

    typedef inlist<netSyncNodeBase, &netSyncNodeBase::m_ListLink> ChildList;

	netSyncParentNode();
	virtual ~netSyncParentNode();

	unsigned		GetNumChildren() const		         { return m_NumChildren; }
	unsigned		GetNumDataChildren() const	         { return m_NumDataChildren; }
	void			SetIsRootNode() 			         { m_RootNode = true; }
	bool			GetIsRootNode() const		         { return m_RootNode; }
	virtual bool	GetIsParentNode() const		         { return true; }
    ChildList      &GetChildList()                       { return m_ChildList; }

	//PURPOSE
	// Sets whether all child nodes are dependent (must be sent together) on each other. This allows ensuring nodes with soft dependencies
	// are always written together during sync tree write operations. This is different to the dependencies specified on the netSyncDataNode
	// class, which is a harder dependency relationship
	//PARAMS
	//  dependent - whether child nodes should be dependent
	void SetChildNodesDependent(bool dependent) { m_ChildNodesDependent = dependent; }

	//PURPOSE
	//	Adds serialise mode flags which are used by a child, and passes these flags up to the parent of this parent
	void AddSerialiseModeFlags(SerialiseModeFlags serFlags);

	//PURPOSE
	//	Adds conditional flags which are used by a child, and passes these flags up to the parent of this parent
	void AddConditionalFlags(SerialiseModeFlags condFlags);

	//PURPOSE
	//	Adds activation flags which are used by a child, and passes these flags up to the parent of this parent
	void AddActivationFlags(ActivationFlags actFlags);

	//PURPOSE
	// Called when the node is removed from the tree.
	virtual void ShutdownNode();

	//PURPOSE
	// Initialises the data start position in the sync buffers & data index for this node 
	//PARAMS
	//  dataPosition - the current data position in the sync buffers, added to by each data node 
	//  nodeIndex	 - the current data node index, incremented by each data node
	//  syncNodeIndex	 - the current data node index, incremented by each sync update data node
	//  maxNumHeaderBits - adds the maximum number of header bits written by this node and its children to the existing value
	virtual void InitialiseDataNodesInTree(unsigned& dataPosition, unsigned& nodeIndex, unsigned& syncNodeIndex, unsigned &maxNumHeaderBits);

	//PURPOSE
	//	Writes the node data to a bit buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	//	bitBuffer	- the message buffer.
	//  currTime    - the current sync time
	//  pLog		- if this is set the message buffer data is written to the log as it is written
	//	player		- the player we are sending the data to.
	//  pNodeFlags	- A bitfield where each set bit corresponds to a data node that wrote some data to the message buffer. For update messages this is
	//				  stored and passed into ProcessAck when an ack arrives from another machine. 
	//  maxNumHeaderBitsRemaining - The maximum number of header bits that could still be written during this write operation (used to ensure there is space in the bit buffer)
	// RETURNS
	//	true if any data node data was written
	virtual bool Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, const unsigned currTime, 
		netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags, unsigned &maxNumHeaderBitsRemaining);

	//PURPOSE
	//	Reads the node data from a bit buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	bitBuffer	- the message buffer received in an update message
	//  pLog		- if this is set the message buffer data is written to the log as it is read
	// RETURNS
	//	true if any data node data was read
	virtual bool Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog);

	//PURPOSE
	//  Returns the size of data that would be written for the specified serialisation mode, activation flags and target object
	//PARAMS
	//  serialisationMode   - the serialisation mode, (e.g. update, create, migrate) 
	//  activationFlags     - flags to activate or deactivate certain nodes
	//  targetObject        - the object using the tree
	virtual unsigned GetDataSize(SerialiseModeFlags        serialisationMode,
								 ActivationFlags           activationFlags,
								 netSyncTreeTargetObject  &targetObject);

    //PURPOSE
    //	Calculates the maximum data size that can be written for the node with the specified serialisation mode and
    //  activation flags. This is the amount of data that would be written if the node is dirty and ready to send.
    //  Optionally writes the data size of the node to the TTY output for debugging purposes
    //PARAMS
    //  serMode		- the serialisation mode, (eg update, create, migrate) 
    //  actFlags	- flags to activate or deactivate certain nodes
    virtual unsigned GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool outputToTTY);

	//PURPOSE
	//	Adds a new child node to the child list, can be a data or parent node.
	void AddChildNode(netSyncNodeBase* pChildNode);

#if __BANK
	//PURPOSE
	//	Adds a bank widget for this node
	virtual void AddWidgets(bkBank *bank);
#endif

protected:

	ChildList								m_ChildList;						// the list of children
	unsigned								m_NumChildren;						// the number of children
	unsigned								m_NumDataChildren;					// the number of child data nodes
	bool									m_ConditionalFlagsSet : 1;		    // true when the conditional flags are first set by a child
	bool									m_ActivationFlagsSet : 1;		    // true when the activation flags are first set by a child
	bool									m_RootNode : 1;						// flag indicating whether this is a tree root node
	bool                                    m_ChildNodesDependent : 1;          // flag indicating whether all child node data needs to be written together
};

//PURPOSE
// A sync tree leaf node, reads or writes the data it is responsible for. Determines
// whether the data is dirty by comparing the current state of the data with the previous
// state held in a shadow buffer. Manual dirty nodes do not use a shadow buffer and rely 
// on being told the data is dirty.
class netSyncDataNode : public netSyncNodeBase
{
public: 

	static const unsigned MAX_UPDATE_DATA_NODES = sizeof(DataNodeFlags)<<3;
	static const unsigned MAX_SYNC_DATA_NODES   = MAX_UPDATE_DATA_NODES + 10; // +10 to take into account of one creation and potentially multiple migration nodes
	static const unsigned INVALID_DATA_INDEX    = 0xffffffff;

public:

	netSyncDataNode();
	netSyncDataNode(SerialiseModeFlags serFlags, SerialiseModeFlags conditionalFlags, ActivationFlags actFlags);

	virtual ~netSyncDataNode();

	unsigned	GetDataIndex()	const					{ return m_DataIndex; }
	unsigned	GetDataStart()	const					{ return m_DataStart; }
	void		SetDataIndex(unsigned const ind)		{ m_DataIndex = ind; }
	void		SetDataStart(unsigned const s)			{ m_DataStart = s; }
	bool		GetWasUpdated() const					{ return m_Updated; }
	void		ClearUpdatedFlag()						{ m_Updated = false; m_HasDoneDependencySizeCheck = false; }

	void				 SetCurrentSyncDataUnit(netSyncDataUnitBase* unit)	{ m_pCurrentSyncDataUnit = unit; }
	netSyncDataUnitBase* GetCurrentSyncDataUnit() const						{ return m_pCurrentSyncDataUnit; }

    //PURPOSE
    // Returns whether this node is dependent on another node, or other nodes have a dependency on this node
    bool HasNodeDependencies() const { return (m_DependentNode != 0) || (m_NumExternalNodeDependencies > 0); }

    //PURPOSE
    // Returns the node this node is dependent on
    netSyncDataNode *GetNodeDependency() const { return m_DependentNode; }

    //PURPOSE
	// Sets up a node dependency. The data from this node will always be sent with the data from the dependent node
    //PARAMS
    // node - the node to make this node dependent upon
	void SetNodeDependency(netSyncDataNode &node);

    //PURPOSE
    // Returns the number of nodes that are dependent upon this node
    unsigned GetNumExternalDependentNodes() const { return m_NumExternalNodeDependencies; }

    //PURPOSE
    // Returns a node dependent upon this node
    //PARAMS
    // index - Index into the list of nodes dependent on this node
    netSyncDataNode *GetExternalDependentNode(unsigned index) const { return m_ExternalNodesDependent[index]; }

#if __DEV
    unsigned	GetMaxSizeOfData() const				{ return m_MaxDataSize; }
    void		SetMaxSizeOfData(unsigned const size)   { m_MaxDataSize = size; }
#endif

	//PURPOSE
	//	Returns whether this node is considered during sync updates. Data nodes of this type need a corresponding CNetworkSyncUnit entry
	//  to keep a track of whether they need to send updates to other machines.
	virtual bool GetIsSyncUpdateNode() const = 0;

    //PURPOSE
    // Returns whether this is a data node
	virtual bool GetIsDataNode() const { return true; } 

    //PURPOSE
    // Returns whether the data synced via this node should always be sent
    // when a target object is created
    virtual bool IsAlwaysSentWithCreate() const { return false; }

	//PURPOSE
	// Returns whether the data synced via this node should always be sent
	// when a target object is migrated
	virtual bool IsAlwaysSentWithMigrate() const { return false; }

	//PURPOSE
	// Initialises the sync data corresponding to this node
	//PARAMS
	//	pObj	- the object using the tree
	virtual void InitialiseData(netSyncTreeTargetObject* pObj);

    //PURPOSE
    // Returns the node adjusted update level for the node based on the target objects update level
    //PARAMS
    // updateLevel - the update level to check
    virtual u8 GetNodeUpdateLevel(const u32 updateLevel) const = 0;

	//PURPOSE
    // Returns the update frequency for the node at the specified update level
    //PARAMS
    // updateLevel - the update level to check
    virtual	u32 GetUpdateFrequency(const u32 updateLevel) const = 0;

	//PURPOSE
	// Returns the resend frequency for the node when it is unacked
	//PARAMS
	// playerIndex - the player to check resend frequency for
	virtual	u32 GetResendFrequency(PhysicalPlayerIndex playerIndex) const = 0;

	//PURPOSE
	//	Manually flags the data represented by this node as being dirty (changed)
	//PARAMS
	//	pObj		- the object using the tree
	virtual void SetDirty(netSyncTreeTargetObject* pObj);

	//PURPOSE
	// Initialises the data start position in the sync buffers & data index for this node 
	//PARAMS
	//  dataPosition - the current data position in the sync buffers, added to by each data node 
	//  nodeIndex	 - the current data node index, incremented by each data node
	//  syncNodeIndex	 - the current data node index, incremented by each sync update data node
	//  maxNumHeaderBits - adds the maximum number of header bits written by this node and its children to the existing value
	virtual void InitialiseDataNodesInTree(unsigned& dataPosition, unsigned& nodeIndex, unsigned& syncNodeIndex, unsigned &maxNumHeaderBits);

	//PURPOSE
	// Called every frame.
	//PARAMS
	//	pObj - the object using the tree
	bool Update(netSyncTreeTargetObject* pObj);

	//PURPOSE
	//	Returns true if the data represented by the node is ready to be sent to the given player, either because it is dirty or unacked.
	//	Can be overidden with different rules. 
	//PARAMS
	//	pObj		- the object using the tree
	//	player		- the player we are sending to
	//  serMode		- the serialise mode (eg update, create, migrate)
	//  currTime	- the current sync time
	virtual bool IsReadyToSendToPlayer(netSyncTreeTargetObject* pObj, const PhysicalPlayerIndex player, SerialiseModeFlags serMode, ActivationFlags actFlags, const unsigned currTime ) const = 0;

	//PURPOSE
	//	Writes the node data to a bit buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	//	bitBuffer	- the bit buffer.
	//  currTime    - the current sync time
	//  pLog		- if this is set the message buffer data is written to the log as it is written
	//	player		- the player we are sending the data to.
	//  pNodeFlags	- A bitfield where each set bit corresponds to a data node that wrote some data to the message buffer. For update messages this is
	//				  stored and passed into ProcessAck when an ack arrives from another machine. 
	//  maxNumHeaderBitsRemaining - The maximum number of header bits that could still be written during this write operation (used to ensure there is space in the bit buffer)
	// RETURNS
	//	true if any data node data was written
	virtual bool Write(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, const unsigned currTime, 
		netLoggingInterface* pLog, const PhysicalPlayerIndex player, DataNodeFlags* pNodeFlags, unsigned &maxNumHeaderBitsRemaining);

	//PURPOSE
	//	Reads the node data from a bit buffer 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	bitBuffer	- the bit buffer received in an update message
	//  pLog		- if this is set the message buffer data is written to the log as it is read
	// RETURNS
	//	true if any data node data was read
	virtual bool Read(SerialiseModeFlags serMode, ActivationFlags actFlags, datBitBuffer& bitBuffer, netLoggingInterface* pLog);

    //PURPOSE
    //	Calculates the maximum data size that can be written for the node with the specified serialisation mode and
    //  activation flags. This is the amount of data that would be written if the node is dirty and ready to send.
    //  Optionally writes the data size of the node to the TTY output for debugging purposes
    //PARAMS
    //  serMode		- the serialisation mode, (eg update, create, migrate) 
    //  actFlags	- flags to activate or deactivate certain nodes
    virtual unsigned GetMaximumDataSize(SerialiseModeFlags serMode, ActivationFlags actFlags, bool outputToTTY);

	//PURPOSE
	//	Forces the sending of the data 
	//PARAMS
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	void ForceSend(SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj);

    //PURPOSE
	//	Forces the sending of the data to the specified player
	//PARAMS
    //  player      - the player to forcibly send the data to
	//  serMode		- the serialisation mode, (eg update, create, migrate) 
	//  actFlags	- flags to activate or deactivate certain nodes
	//	pObj		- the object using the tree
	void ForceSendToPlayer(const PhysicalPlayerIndex player, SerialiseModeFlags serMode, ActivationFlags actFlags, netSyncTreeTargetObject* pObj);

	//PURPOSE
	//  Returns whether this node is dirtied manually, and does not use a shadow buffer.
	virtual bool GetIsManualDirty() const { return false; }

	//PURPOSE
	//	Returns true if the data held in the node can be applied to the object
	//PARAMS
	//	pObj		- the object using the tree
	virtual bool CanApplyData(netSyncTreeTargetObject*) const { return true; }

	//PURPOSE
	//  Returns true if we want to forcibly stop the data being sent for this node, even if it is dirty.
	//PARAMS
	//	pObj		- the object using the tree
	//  serMode		- the serialisation mode (eg update, create, migrate)
	//RETURNS
	// player flags, with each flag set for a player we want to stop sending to
	virtual PlayerFlags StopSend(const netSyncTreeTargetObject* UNUSED_PARAM(pObj), SerialiseModeFlags UNUSED_PARAM(serMode)) const { return 0; }

#if __BANK
	//PURPOSE
	//	Adds a bank widget for this node
	virtual void AddWidgets(bkBank *bank);

	bool IsSelectedInBank() const			{ return m_SelectedInBank; }
#endif

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// PURE VIRTUALS :
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//PURPOSE
	//  Reads the data represented by this node from the given bit buffer.
	//PARAMS
	//	bitBuffer	- the bit buffer 
	//  pLog		- if this is set the message buffer data is written to the log as it is read
	virtual void ReadData(datBitBuffer& bitBuffer, netLoggingInterface* pLog) = 0;

	//PURPOSE
	//  Writes the data represented by this node to the given bit buffer.
	//PARAMS
	//	pObj				- the object using the tree
	//  bitBuffer			- the bit buffer
	//  pLog				- if this is set the message buffer data is written to the log as it is written
	// extractFromObject	- if true, the data is extracted from the object before writing
	virtual void WriteData(netSyncTreeTargetObject* pObj, datBitBuffer& bitBuffer, netLoggingInterface* pLog, bool extractFromObject = true) = 0;

	//PURPOSE
	//  Returns the size of data that would be written for the specified serialisation mode, activation flags and target object
	//PARAMS
	//  serialisationMode   - the serialisation mode, (e.g. update, create, migrate) 
	//  activationFlags     - flags to activate or deactivate certain nodes
	//  targetObject        - the object using the tree
	virtual unsigned GetDataSize(SerialiseModeFlags        serialisationMode,
								 ActivationFlags           activationFlags,
								 netSyncTreeTargetObject  &targetObject);

	//PURPOSE
	// Returns the size of the data the node would write for the specified target object
	//PARAMS
	// targetObject - The target object to extract the data from for calculating the size
	virtual unsigned GetDataSize(netSyncTreeTargetObject &targetObject) const = 0;

	//PURPOSE
	//	Returns the maximum size of the data the node can write to a message buffer
	virtual unsigned GetMaximumDataSize() const = 0;

	//PURPOSE
	//	Applies the data held in the node to the target object
	//PARAMS
	//	pObj		- the object using the tree
	virtual void ApplyData(netSyncTreeTargetObject* pObj) = 0;

	//PURPOSE
	//	Logs the data held in the node 
	//PARAMS
	//  log			- the log
	virtual void LogData(netLoggingInterface& log) = 0;

	//PURPOSE
	// Sets the flag that informs the node that during the write process the node and its children's fitting into the message was checked
	void SetHasDoneDependencySizeCheck(bool checked) { m_HasDoneDependencySizeCheck = checked; }

	//PURPOSE
	// Returns true if the node or the parent node has completed the fit check already
	bool HasDoneDependencySizeCheck();

protected:

    //PURPOSE
	//  Returns whether this node uses the current state buffer for detecting changes in state
	//PARAMS
	//	serMode - if this is set then this function returns true if the current state buffer is updated when the node data is written for the given
	//			  serialise mode.
    virtual bool GetUsesCurrentStateBuffer(SerialiseModeFlags serMode = 0) const;

private:

    //PURPOSE
    // Adds the specified node as to the list of nodes dependent on this node
    //PARAMS
    // node - the node to add to the node dependency list
    void AddExternalNodeDependency(netSyncDataNode &node);

protected:

    static const unsigned MAX_EXTERNAL_NODES_DEPENDENT = 8; // maximum number of external nodes

	unsigned	m_DataIndex;		// used to index into the netSyncData array and set the corresponding flag in a guard flag bitfield
	unsigned	m_DataStart;		// the cursor pos of the start of the data that this node deals with in a current or shadow buffer
	bool		m_Updated;			// set when the node has been used to read or write 
	bool		m_HasDoneDependencySizeCheck; // used in write operations to calculate if all dependent children's data will fit into the sync message

	netSyncDataUnitBase *m_pCurrentSyncDataUnit; // the current sync data unit the node is using
	netSyncDataNode     *m_DependentNode;        // the data for this node will always be sent out with the data from this dependant node.

    // nodes dependent on this node - data will be sent for these nodes when data for this node is sent
    unsigned         m_NumExternalNodeDependencies;
    netSyncDataNode *m_ExternalNodesDependent[MAX_EXTERNAL_NODES_DEPENDENT];

#if __DEV
    unsigned	m_MaxDataSize;      // the maximum size of the data block written by this node - used for validation checking
#endif

#if __BANK
	bool		m_SelectedInBank;	// set when this node is ticked in the bank toggle representing this node
#endif
};

} // namespace rage

#endif  // NETWORK_SYNC_NODE_H
