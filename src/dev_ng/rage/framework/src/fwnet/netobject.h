//
// netobject.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_OBJECT_H
#define NETWORK_OBJECT_H

// rage includes
#include "atl/array.h"
#include "atl/dlist.h"
#include "atl/string.h"
#include "data/base.h"
#include "vector/matrix34.h"
#include "net/message.h"

// framework includes
#include "fwnet/nettypes.h"
#include "fwnet/netinterface.h"
#include "fwnet/netplayer.h"
#include "fwnet/netsyncdataul.h"
#include "fwnet/netsynctree.h"
#include "fwtl/pool.h"

class CEntity;

namespace rage
{
    class netBlender;
    class netLoggingInterface;
    class netPlayer;
	class scriptObjInfoBase;
	class scriptHandlerObject;

//PURPOSE
// This class is a base class for classes to encapsulate and manage the data required to synchronised different types
// of game object over the network.
//
// TODO: - Full description on how to use this class in a game project
//       - Describe ownership tokens
//       - describe terminology (cloning, syncing etc...)
//
class netObject : public netSyncTreeTargetObject
{
public:

    // local flags used by network objects
    static const unsigned LOCALFLAG_UNREGISTERING        = BIT(0); // if set this object has been unregistered and is cleaning itself up
    static const unsigned LOCALFLAG_NOCLONE              = BIT(1); // if set this object is not cloned on other machines
    static const unsigned LOCALFLAG_BEINGREASSIGNED      = BIT(2); // set when an object is ownerless and on the reassign list
    static const unsigned LOCALFLAG_NOREASSIGN           = BIT(3); // if set this object will not be reassigned when a player leaves
    static const unsigned LOCALFLAG_USER                 = 4;

    // global flags used by network objects
    static const unsigned GLOBALFLAG_PERSISTENTOWNER     = BIT(0); // disallows proximity or other common ownership changes to this object - ownership can only be altered via explicit means (usually script commands)
    static const unsigned GLOBALFLAG_CLONEALWAYS         = BIT(1); // forces cloning of this object on all players
    static const unsigned GLOBALFLAG_USER                 = 2;
    // don't forget to update SIZEOF_NETOBJ_GLOBALFLAGS if you add a new bit flag!

    static const unsigned SIZEOF_TIMESTAMP          = 32; // number of bits required for syncing network timestamps
    static const unsigned SIZEOF_OWNERSHIP_TOKEN    = 5;  // number of bits required for syncing ownership tokens
    static const unsigned MAX_OWNERSHIP_TOKEN_VALUE = (1<<SIZEOF_OWNERSHIP_TOKEN) - 1; // maximum value for an ownership token
    static const unsigned MAX_LOG_NAME              = 30; // maximum size of the log name for a network object

    //PURPOSE
    // This structure contains the variables that determine the ranges for a network object to either come
    // into scope or switch between update levels (which determine the frequency at which data is sent for an object)
    struct netScopeData
    {
        float   m_scopeDistance;            // object is in scope with a player if he is less than this distance away
        float   m_scopeDistanceInterior;    // scope distance if the object is in an interior
        float   m_syncDistanceNear;         // at this distance update messages are sent most frequently (m_syncTimeClose apart)
        float   m_syncDistanceFar;          // at this distance update messages are sent most infrequently (m_syncTimeFar apart)
    };

public:

    //PURPOSE
    // Templated contructor for the network object class, template parameter is the type of
    // game object associated with this network object.
    //PARAMS
    //  gameObject  - The game object this network object is going to be synchronising data for
    //  type        - The type of network object, a unique identifier for this type of network object
    //  objectID    - The network object ID for the network object to create (for more details see netobjectidmgr.cpp/.h)
    //  playerIndex - The physical player index of the initial owner of the network object to create
    //  localFlags  - The local network object flags for the network object to create (for more details see netobject.cpp/.h)
    //  globalFlags - The global network object flags for the network object to create (for more details see netobject.cpp/.h)
    template< class T >
    netObject(T*							gameObject,
              const NetworkObjectType		type,
              const ObjectId				objectID,
              const PhysicalPlayerIndex     playerIndex,
              const NetObjFlags				localFlags,
              const NetObjFlags				globalFlags) :
    m_GameObject(gameObject)
    , m_ObjectType(type)
    , m_ObjectID(objectID)
    , m_IsClone(false)
    , m_SyncTreeUpdateLevel(0)
    , m_SyncTreeUpdateBatch(0)
    , m_NumPossibleUpdateBatches(1)
    , m_StartIndexForPlayerBatch(0)
    , m_syncDataChangeTimer(0)
    , m_PlayerIndex(playerIndex)
    , m_PendingPlayerIndex(INVALID_PLAYER_INDEX)
    , m_LocalFlags(localFlags)
    , m_GlobalFlags(globalFlags)
    , m_NetBlender(NULL)
    , m_Node(NULL)
    , m_ClonedState(0)
    , m_PendingCloningState(0)
    , m_PendingRemovalState(0)
    , m_InScopeState(0)
    DEV_ONLY(, m_LastFrameSyncTreeUpdated(0))
    {
        netPlayer* pPlayer = GetPlayerOwner();

        if (pPlayer && !pPlayer->IsLocal())
        {
            m_IsClone = true;
        }
#if !__FINAL
        // set up the logging name for this object
        sprintf(m_LogName, "%d_%d", m_ObjectType, m_ObjectID);
#endif
    }

    //PURPOSE
    // Default contructor for a network object. Initialises all members to default values.
    netObject() :
    m_GameObject(0)
    , m_ObjectType(INVALID_OBJECT_TYPE)
    , m_ObjectID(NETWORK_INVALID_OBJECT_ID)
    , m_IsClone(false)
    , m_SyncTreeUpdateLevel(0)
    , m_SyncTreeUpdateBatch(0)
    , m_NumPossibleUpdateBatches(1)
    , m_StartIndexForPlayerBatch(0)
    , m_syncDataChangeTimer(0)
    , m_PlayerIndex(INVALID_PLAYER_INDEX)
    , m_PendingPlayerIndex(INVALID_PLAYER_INDEX)
    , m_LocalFlags(0)
    , m_GlobalFlags(0)
    , m_NetBlender(NULL)
    , m_Node(NULL)
    , m_ClonedState(0)
    , m_PendingCloningState(0)
    , m_PendingRemovalState(0)
    , m_InScopeState(0)
    DEV_ONLY(, m_LastFrameSyncTreeUpdated(0))
    {
 #if !__FINAL
        // set up the logging name for this object
        safecpy(m_LogName, "***INVALID***");
#endif
    }

    virtual ~netObject();

    //PURPOSE
    // Initialises this network object ready for use
    //PARAMS
    // numPlayers -  maximum number of players this network will be synchronised with
    virtual void Init(const unsigned numPlayers);

    //PURPOSE
    // Returns whether this network object is a clone (is controlled by a remote machine)
    bool IsClone() const  { return m_IsClone; }

    //PURPOSE
    // Returns the update level the network object is registered with the sync tree at
    u8 GetSyncTreeUpdateLevel() const { return m_SyncTreeUpdateLevel; }

    //PURPOSE
    // Returns the current update batch for this object (used to update network objects
    // via the sync tree over multiple frames as a CPU optimisation)
    u8 GetSyncTreeUpdateBatch() const { return m_SyncTreeUpdateBatch; }

    //PURPOSE
    // Returns whether this object is in a batch that should be updated this frame
    bool IsInCurrentUpdateBatch() const { return m_SyncTreeUpdateBatch == GetSyncTree()->GetCurrentUpdateBatch(GetMinimumUpdateLevel()); }

    //PURPOSE
    // Sets the sync tree update level and batch the network object is registered with the sync tree
    //PARAMS
    // updateLevel - update level this object is registered with
    // batch       - batch is registered with
    void SetSyncTreeUpdateLevelAndBatch(const u8 updateLevel, const u8 batch);

    //PURPOSE
    // Returns the physical player index of the player to start with when performing operations that are batched
    u8 GetStartIndexForPlayerBatch() const { return m_StartIndexForPlayerBatch; }

    //PURPOSE
    // Returns the entity for this network object
    virtual CEntity*  GetEntity() const { return NULL; }

    //PURPOSE
    // Returns the network object type for this network object
    NetworkObjectType GetObjectType() const;

    //PURPOSE
    // Sets the network object type for this network object
    //PARAMS
    // type - network object type of this network object
    void SetObjectType(const NetworkObjectType type);

    //PURPOSE
    // Returns the global network object ID for this object (for more details see netobjectidmgr.cpp/.h)
    const ObjectId &GetObjectID() const { return m_ObjectID; }

    //PURPOSE
    // Sets the network object ID for this object (for more details see netobjectidmgr.cpp/.h)
    //PARAMS
    // objectID - the ID
    void SetObjectID(const ObjectId objectID) { m_ObjectID = objectID; }

    //PURPOSE
    // Returns the player index of the player currently in control of this network object
    PhysicalPlayerIndex GetPhysicalPlayerIndex() const;

    //PURPOSE
    // Sets the player index of the player currently in control of this network object
    //PARAMS
    // playerIndex - the player index of the player currently in control
    void SetPlayerIndex(const PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Returns the network player object of the player currently in control of this object
    netPlayer* GetPlayerOwner() const;

    //PURPOSE
    // Returns the player index of the player this object is in the process of being passed control to
    PhysicalPlayerIndex GetPendingPlayerIndex() const;

    //PURPOSE
    // Returns the network player object this object is in the process of being passed control to
    netPlayer* GetPendingPlayerOwner() const;

    //PURPOSE
    // Sets the player index of a network player this object is going to be passed control to
    //PARAMS
    // playerIndex - the player index
    virtual void SetPendingPlayerIndex(const PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Clears the pending player index of this object, which indicates this object is not in the process of being passed control of
    virtual void ClearPendingPlayerIndex();

    //PURPOSE
    // Returns whether this object is currently in the process of being passed control of to another machine
    bool IsPendingOwnerChange() const;

    //PURPOSE
    // Returns the local network object flags for this object. Local flags are not synchronised to other machines.
    NetObjFlags GetLocalFlags() const;

    //PURPOSE
    // Returns whether the specified local flag is set on this network object
    //PARAMS
    // flag - the local flag to check
    bool IsLocalFlagSet(unsigned int flag) const;

    //PURPOSE
    // Sets or clears the specified local flag
    //PARAMS
    // flag - the flag to change the value of
    // set  - whether the flag should be set or cleared
    void SetLocalFlag(unsigned int flag, bool set);

    //PURPOSE
    // Returns the global network object flags for this object. Global flags are synchronised to other machines.
    NetObjFlags GetGlobalFlags() const;

    //PURPOSE
    // Sets the global network object flags for this object. Global flags are synchronised to other machines.
    //PARAMS
    // globalFlags - the new value for the global flags for this network object
    void SetGlobalFlags(NetObjFlags globalFlags);

    //PURPOSE
    // Returns whether the specified global flag is set on this network object
    //PARAMS
    // flag - the global flag to check
    bool IsGlobalFlagSet(unsigned int flag) const;

    //PURPOSE
    // Sets or clears the specified global flag
    //PARAMS
    // flag - the flag to change the value of
    // set  - whether the flag should be set or cleared
    virtual void SetGlobalFlag(unsigned int flag, bool set);

    //PURPOSE
    // Returns the activation flags to use when synchronising this object via the sync trees
    // system (see netsynctree.h/.cpp for details).
    virtual ActivationFlags GetActivationFlags() const { return 0; }

    //PURPOSE
    // Returns the log name for this network object (used for debugging purposes)
    const char *GetLogName() const;

    //PURPOSE
    // Returns the ownership token for this network object
    u32 GetOwnershipToken() const { return m_OwnershipData.m_ownershipToken; }

    //PURPOSE
    // Sets the ownership token for this network object
    // PARAMS
    // token - the new value for the ownership token of this object
    void SetOwnershipToken(u32 token) { m_OwnershipData.m_ownershipToken = token; }

    //PURPOSE
    // Increases the ownership token, handling wrapping
    void IncreaseOwnershipToken();

    //PURPOSE
    // Returns whether this network object has been cloned on any other machines
    bool HasBeenCloned() const;

    //PURPOSE
    // Returns whether this network object has been cloned on the specified players machine
    //PARAMS
    // player - the player to check for
    bool HasBeenCloned(const netPlayer& player) const;

    //PURPOSE
    // Sets whether this network object has been cloned on the specified players machine
    //PARAMS
    // player - the player to set the cloned state for
    // cloned - whether the object is cloned on the specified players machine
    void SetBeenCloned(const netPlayer& player, bool cloned);

    //PURPOSE
    // Returns whether the objects state is fully up-to-date on all other players machines.
    // This function can only be called on network objects controlled by the local machine
    bool IsSyncedWithAllPlayers() const;

	//PURPOSE
	// Returns whether the objects critical state is fully up-to-date on all other players machines.
	// This function can only be called on network objects controlled by the local machine
	bool IsCriticalStateSyncedWithAllPlayers() const;

	//PURPOSE
    // Returns information on which machines this object is currently cloned on, is in the process
    // of being cloned on, and is in the process of being removed on. This function only returns valid
    // results if the object is being controlled by the local machine
    //PARAMS
    // clonedState - a bitfield representing which machines an object is cloned on
    // createState - a bitfield representing which machines an object is in the process of being cloned on
    // removeState - a bitfield representing which machines an object is in the process of being removed on
    void GetClonedState(PlayerFlags &clonedState, PlayerFlags &createState, PlayerFlags &removeState) const;

	//PURPOSE
	// Returns a bitfield representing which machines an object is cloned on
	PlayerFlags GetClonedState() const { return m_ClonedState; }

	//PURPOSE
	// Returns a bitfield representing which machines an is in the process of being cloned on
	PlayerFlags GetPendingCloningState() const { return m_PendingCloningState; }

	//PURPOSE
	// Returns a bitfield representing which machines an object is in the process of being removed on
	PlayerFlags GetPendingRemovalState() const { return m_PendingRemovalState; }

    //PURPOSE
	// Returns a bitfield representing which machines an object is in the process of being removed on
	PlayerFlags GetInScopeState() const { return m_InScopeState; }

	//PURPOSE
	// Returns a bitfield representing which machines disconnected while the object was cloned on their machine when local
	PlayerFlags GetClonedPlayersThatLeft() const { return m_ClonedPlayersThatLeft; }

    //PURPOSE
	// Returns a bitfield indicating which players were checked for sending sync updates (used for batching)
	PlayerFlags GetLastPlayersSyncUpdated() const { return m_LastPlayersSyncUpdated; }

	//PURPOSE
    // Sets information on which machines this object is currently cloned on, is in the process
    // of being cloned on, and is in the process of being removed on.
    //PARAMS
    // clonedState - a bitfield representing which machines an object is cloned on
    // createState - a bitfield representing which machines an object is in the process of being cloned on
    // removeState - a bitfield representing which machines an object is in the process of being removed on
    void SetClonedState(PlayerFlags clonedState, PlayerFlags createState, PlayerFlags removeState);

    //PURPOSE
	// Sets a bitfield representing which machines disconnected while the object was cloned on their machine when local
	void SetClonedPlayersThatLeft(PlayerFlags clonedState) { m_ClonedPlayersThatLeft = clonedState; }

    //PURPOSE
	// Sets a bitfield indicating which players were checked for sending sync updates (used for batching)
	void SetLastPlayersSyncUpdated(PlayerFlags lastPlayersSyncUpdated) { m_LastPlayersSyncUpdated = lastPlayersSyncUpdated; }

    //PURPOSE
    // Returns whether this object is in the process of being cloned on any other remote machine. This is the
    // case when an object creation message has been sent to a machine but the ACK message has not been received yet
    bool IsPendingCloning() const;

    //PURPOSE
    // Returns whether this object is in the process of being cloned on the specified remote machine. This is the
    // case when an object creation message has been sent to a machine but the ACK message has not been received yet
    //PARAMS
    // player - the player to check
    bool IsPendingCloning(const netPlayer& player) const;

    //PURPOSE
    // Sets whether this object is in the process of being cloned on the specified remote machine.
    //PARAMS
    // player - the player to set
    void SetPendingCloning(const netPlayer& player, bool bCloned);

    //PURPOSE
    // Returns whether this object is in the process of being removed on any other remote machine. This is the
    // case when an object removal message has been sent to a machine but the ACK message has not been received yet
    bool IsPendingRemoval() const;

    //PURPOSE
    // Returns whether this object is in the process of being removed on the specified remote machine. This is the
    // case when an object removal message has been sent to a machine but the ACK message has not been received yet
    //PARAMS
    // player - the player to check
    bool IsPendingRemoval(const netPlayer& player) const;

    //PURPOSE
    // Sets whether this object is in the process of being removed on the specified remote machine.
    //PARAMS
    // player - the player to set
    void SetPendingRemoval(const netPlayer& player, bool bRemoval);

	//PURPOSE
	// Returns whether this network object currently has a valid game object associated with it
	bool HasGameObject() const { return m_GameObject != NULL; }

	//PURPOSE
	// Set a valid game object associated with the network object.
	virtual void SetGameObject(void* gameObject) { NETWORK_QUITF(!gameObject || !HasGameObject(), "Network object already has a game object!"); m_GameObject = gameObject; }

    //PURPOSE
    // Resets the cloning timer for this object for the specified player. The cloning timer is started when a
    // creation or removal message is sent to a remote machine to ensure an ACK message is received in a timely manner.
    //PARAMS
    // player - the player to reset the timer for.
    void ResetCloningTimer (const netPlayer& player);

    //PURPOSE
    // Increments the cloning timer for this object for the specified player. The cloning timer is started when a
    // creation or removal message is sent to a remote machine to ensure an ACK message is received in a timely manner.
    //PARAMS
    // player - the player to reset the timer for.
    void IncCloningTimer   (const netPlayer& player);

    //PURPOSE
    // Checks whether the cloning timer for this object for the specified player has timed out. The cloning timer is started when a
    // creation or removal message is sent to a remote machine to ensure an ACK message is received in a timely manner.
    //PARAMS
    // player - the player to reset the timer for.
    bool HasCloningTimedOut(const netPlayer& player) const;

    //PURPOSE
    // Returns the network blender for this network object (see netblender.h/.cpp for details)
    netBlender* GetNetBlender() const;

    //PURPOSE
    // Creates a network blender specific to the type of network object
    virtual void CreateNetBlender() {}

    //PURPOSE
    // Destroys the network blender
    void DestroyNetBlender();

    //PURPOSE
    // Returns the list node for this object used to link this object into the network object manager lists
    atDNode<netObject*, datBase>* GetListNode() const;

    //PURPOSE
    // Sets the list node for this object used to link this object into the network object manager lists
    //PARAMS
    // node - the list node
    void SetListNode(atDNode<netObject*, datBase>* node);

    //PURPOSE
    // Returns access to the sync tree
    virtual netSyncTree* GetSyncTree() = 0;

    //PURPOSE
    // Returns a const version of the sync tree
    virtual const netSyncTree* GetSyncTree() const { return const_cast<netObject *>(this)->GetSyncTree(); }

    //PURPOSE
    // Returns the sync data for this network object (see netsyncdata.h/.cpp for details)
	bool HasSyncData() const				  { return m_SyncData.GetSyncData() != NULL; }
    netSyncDataBase* GetSyncData()            { Assert(!IsClone()); return m_SyncData.GetSyncData(); }
    netSyncDataBase* GetSyncData() const      { Assert(!IsClone()); return m_SyncData.GetSyncData(); }


	//PURPOSE
	// Returns any script info associated with this network object.
	virtual const scriptObjInfoBase*	GetScriptObjInfo() const = 0;

	//PURPOSE
	// Sets the script info for this network object.
	virtual void SetScriptObjInfo(scriptObjInfoBase* info) = 0;

	//PURPOSE
	// Returns the script handler object associated with this network object.
	virtual scriptHandlerObject* GetScriptHandlerObject() const = 0;

    //PURPOSE
    // Returns the default update level for the network object. This is the update level
    // used when first synchronising the object before ManageUpdateLevel() has been called
    virtual u8 GetDefaultUpdateLevel() const { return CNetworkSyncDataULBase::UPDATE_LEVEL_MEDIUM; }

    //PURPOSE
    // Returns the minimum update level for the network object
    u8 GetMinimumUpdateLevel() const { return GetSyncDataUL()->GetMinimumUpdateLevel(); }

    //PURPOSE
    // Returns the current update level for the specified player
    //PARAMS
    // playerIndex - Index of the player to get the update level for
    u8 GetUpdateLevel(PhysicalPlayerIndex playerIndex) const { return GetSyncDataUL()->GetUpdateLevel(playerIndex); }

    //PURPOSE
    // Returns the current update level for the specified player
    //PARAMS
    // playerIndex - Index of the player to set the update level for
    // updateLevel - New update level to set for the player
    void SetUpdateLevel(PhysicalPlayerIndex playerIndex, u8 updateLevel);

#if __DEV
    void SetLastSyncTreeUpdateFrame(unsigned frame) { m_LastFrameSyncTreeUpdated = frame; }
#endif // __DEV

    //PURPOSE
    // Cleans up the pointers between this network object and the game object which owns it
    //PARAMS
    // destroyGameObject - flag indicating whether to just clear the pointers or actually destroy the object as well
    virtual void CleanUpGameObject(bool destroyGameObject) = 0;

    //PURPOSE
    // Returns the scope data for this network object
    virtual netScopeData& GetScopeData() = 0;
    virtual netScopeData& GetScopeData() const = 0;

    //PURPOSE
    // Checks to see if this network object is in scope with a player in the game. An object is generally in scope if
    // it is close by or visible to the player's player.
    //PARAMS
    // player - the player to check whether the object is in scope of
	// scopeReason - the reason why the object is in scope / out of scope with this player
    virtual bool IsInScope(const netPlayer& player, unsigned* scopeReason = NULL) const;

    //PURPOSE
    // Decides what the current update level of the object should be relative to the given player
    //PARAMS
    // player - the player to manage the update level for
    virtual void ManageUpdateLevel(const netPlayer& UNUSED_PARAM(player)) {}

    //PURPOSE
    // Called once a frame to do network object related management
    virtual bool Update();

	// Are we forcing a scope or minimum update rate level this frame?
	virtual bool ForceManageUpdateLevelThisFrame() { return false; }

    //PURPOSE
    // Creates sync data ready for synchronisation of the object with its clones
    virtual void StartSynchronising();

    //PURPOSE
    // Destroys sync data when synchronisation of the object with its clones is finished
    virtual void StopSynchronising();

    //PURPOSE
    // Resets the synchronisation timeout - this ensures the network object will not change state
    // immediately the next time CanSynchronise returns a different value
    void ResetSyncDataChangeTimer();

	// returns whether the object can accept any node data
	virtual bool CanApplyNodeData() const { return true; }

    //PURPOSE
    // Returns whether this object can be cloned on the specified player's remote machine
    //PARAMS
    // player     - the player to check
    // resultCode - the result of the can clone (contains failure codes if the function returns false)
    virtual bool CanClone(const netPlayer& player, unsigned *resultCode = 0) const;

    //PURPOSE
    // Returns whether this object can be synced on the specified player's remote machine
    //PARAMS
    // player - the player to check
    virtual bool CanSync(const netPlayer& UNUSED_PARAM(player)) const { return true; }

    //PURPOSE
    // Returns whether the object needs to synchronise with its clones (and therefore needs sync data)
    virtual bool CanSynchronise(bool UNUSED_PARAM(bOnRegistration)) const { return HasGameObject() && !IsClone(); }

	//PURPOSE
	// Returns whether the object can stop synchronising with its clones 
	virtual bool CanStopSynchronising() const;

	//PURPOSE
    // Returns whether this object can be deleted
    virtual bool CanDelete(unsigned* reason = nullptr);

    //PURPOSE
	// Some network objects can sync with no associated game object 
	virtual bool CanSyncWithNoGameObject() const { return false; }

	//PURPOSE
	// Some network objects can migrate with no associated game object 
	virtual bool CanPassControlWithNoGameObject() const { return false; }

	//PURPOSE
	// Some network objects can migrate with no associated game object 
	virtual bool CanReassignWithNoGameObject() const { return false; }

	//PURPOSE
    // Returns whether local control of this object can be passed to the given remote player
    //PARAMS
    // player           - the player to check
    // migrationType	- indicates whether to check whether control passing can be done based on the migration type
    // resultCode       - the result of the pass control (contains failure codes if the function returns false)
    //                    such as control being explicitly requested by a remote machine
    virtual bool CanPassControl(const netPlayer& player, eMigrationType migrationType, unsigned *resultCode = 0) const;

    //PURPOSE
    // Returns whether control of this object can be accepted by our player
    //PARAMS
    // player           - the player currently in control of this object
    // migrationType	- indicates whether to check whether control can be accepted based on the migration type
    // resultCode       - the result of the accept control (contains failure codes if the function returns false)
    virtual bool CanAcceptControl(const netPlayer& player, eMigrationType migrationType, unsigned *resultCode = 0) const;

    //PURPOSE
    // Returns whether this object can use the network blender to smooth updates (see netblender.h/.cpp for details)
    //PARAMS
    // resultCode - The reason this object can't use network blending
    virtual bool CanBlend(unsigned *UNUSED_PARAM(resultCode) = 0) const { return true; }

    //PURPOSE
    // Returns true if the network blender is being overridden (see netblender.h/.cpp for details)
    //PARAMS
    // resultCode - The reason the network blender is overridden for this object
    virtual bool NetworkBlenderIsOverridden(unsigned *UNUSED_PARAM(resultCode) = 0) const { return false; }

	//PURPOSE
	// Returns true if the network attached object is being overridden and handled separately.
	virtual bool NetworkAttachmentIsOverridden() const { return false; }

	//PURPOSE
    // Called when the player controlling this object changes
    //PARAMS
    // player           - the player ownership is being changed to
    // migrationType	- indicates how the object migrated
    virtual void ChangeOwner(const netPlayer& player, eMigrationType migrationType);

    //PURPOSE
    // Called when a network object is registered
    virtual void OnRegistered() {}

	//PURPOSE
	// Called when a network object is unregistered
	virtual void OnUnregistered() {}

	//PURPOSE
    // Returns true if the object is to be included in the reassigning process (see netobjectreassignmgr.h/.cpp for details)
    virtual bool NeedsReassigning() const;

    //PURPOSE
    // Called when an object has been reassigned to another machine after the initial owner
    // has left the session (see netobjectreassignmgr.h/.cpp for details)
	//RETURNS
	// True if the object has to be destroyed
    virtual bool OnReassigned() { return false; }

    //PURPOSE
    // Calculates the reassign priority used during the reassignment process (see netobjectreassignmgr.h/.cpp for details).
    // This is a measure of how much the local machine wants control of the object when it is being reassigned.
    virtual u32 CalcReassignPriority() const;

    //PURPOSE
    // Called when a new player joins the game
    //PARAMS
    // player - the player joining the game
    virtual void PlayerHasJoined(const netPlayer& player);

    //PURPOSE
    // Called when an existing player leaves the game
    //PARAMS
    // player - the player leaving the game
    virtual void PlayerHasLeft(const netPlayer& player);

    //PURPOSE
    // Called after an object has been created from a clone create message
    virtual void PostCreate() {}

    //PURPOSE
    // Called after an object clone has received an update message
    virtual void PreSync() {}

    //PURPOSE
    // Called after an object clone has received an update message
    virtual void PostSync() {}

	//PURPOSE
	// Called after an object clone has migrated, and ChangeOwner() called
	virtual void PostMigrate(eMigrationType migrationType);

	//PURPOSE
    // Checks whether a player is allowed to be sending sync data for the object - check current ownership and ownership changes
    //**This must be called after the data from the player has been read into the sync tree**
    //PARAMS
    // player - the player to check authority for
    virtual bool CheckPlayerHasAuthorityOverObject(const netPlayer& player);

    //PURPOSE
    // Logs any additional data based on a network update
    virtual void LogAdditionalData(netLoggingInterface &UNUSED_PARAM(log)) const {}

    //PURPOSE
    // Returns the name of the type of network object
    virtual const char *GetObjectTypeName() const = 0;

	//PURPOSE
	// Allow resetting the proximity control timer in subclass
	virtual void ResetProximityControlTimer() {};
	
	//PURPOSE
	// Static helper function for determining whether a migration is critical, ie must contain critical unacked data. 
	static bool IsCriticalMigration(eMigrationType mt) { return mt > MIGRATE_OUT_OF_SCOPE; }

	//PURPOSE
	// Static helper function for determining whether a migration is by proximity, ie not critical. Proximity migrations only happen if there 
	// is no unacked critical state and so contain less migration data.
	static bool IsProximityMigration(eMigrationType mt) { return !IsCriticalMigration(mt); }

	//PURPOSE
    // Static helper function for retrieving a network ID from a game object. If the specified
    // game object has an associated network object the ID will be returned (see netobjectidmgr.h/.cpp for details)
    //PARAMS
    // gameObject - the game object to retrieve the network ID for
    template <typename T> static const ObjectId &GetObjectIDFromGameObject(T *gameObject);

    //PURPOSE
    // Displays debug info above the game object this network object is associated with. Used for debugging purposes.
    virtual void DisplayNetworkInfo() {}

    //PURPOSE
    // Returns the logging object to use for logging network object data
    static netLoggingInterface *GetLog() { return sm_Log; }

    //PURPOSE
    // Sets the logging object to use for logging network object data
    //PARAMS
    // log - the logging object
    static void SetLog(netLoggingInterface *log) { sm_Log = log; }

    //PURPOSE
    // Sets the threshold at which an object is consider to be starving.
    // Everytime the bandwidth management code prevents an update being
    // sent for an object the starvation count is increased, this threshold
    // is the count at which the object will report it is starving
    //PARAMS
    // threshold - The starvation threshold
    static void SetStarvationThreshold(u8 threshold);

    //PURPOSE
    // Returns whether this network object is starving, which occurs when
    // the bandwidth manager does not allow an update for the object to be
    // send for an extended period of time due to low bandwidth conditions
    //PARAMS
    // playerIndex - optional index to check whether the object is starving
    //               relative to a specific player. If this is not specified
    //               the function checks whether the object is starving relative
    //               to any player in the session
    bool IsStarving(PhysicalPlayerIndex playerIndex = INVALID_PLAYER_INDEX) const;

    //PURPOSE
    // Increases the starvation count for this object relative to the
    // specified player. This is called when there is not enough available
    // bandwidth to send an update for this object to a player
    //PARAMS
    // playerIndex - index of the player to increase the starvation count for.
    void IncreaseStarvationCount(PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Resets the starvation count for this object relative to the
    // specified player. This is called when a sync update is successfully
    // sent to the player.
    //PARAMS
    // playerIndex - index of the player to reset the starvation count for.
    void ResetStarvationCount(PhysicalPlayerIndex playerIndex);

    //PURPOSE
    // Updates the is in scope state for the specified player immediately. This
    // is done periodically in netObject::Update() but sometimes this can't wait
    //PARAMS
    // remotePlayer - The player to update the in scope state for
    void UpdateInScopeState(const netPlayer &remotePlayer);

#if ENABLE_NETWORK_LOGGING
    //PURPOSE
    // Returns a debug string describing the reason an object cannot be cloned to a remote machine
    //PARAMS
    // resultCode - The result code to return the debug string for
    virtual const char *GetCanCloneErrorString(unsigned resultCode);

	//PURPOSE
	// Returns a debug string describing the reason an object cannot be deleted
	//PARAMS
	// reason - The reason code to return the debug string for
	virtual const char *GetCannotDeleteReason(unsigned reason);
#else
    inline const char *GetCanCloneErrorString(unsigned) { return ""; }

	inline const char *GetCannotDeleteReason(unsigned)  { return ""; }
#endif

protected:

    static const unsigned NUM_PLAYERS_TO_UPDATE_DEFAULT = 1; // the default number of players to update per frame for operations batched by player

    //PURPOSE
    // Sets whether this network object is a clone (is controlled by a remote machine)
    // Normally this is handled automatically in change owner but in some cases this may be required
    //PARAMS
    // isClone - flag to set
    void SetIsClone(bool isClone)  { m_IsClone = isClone; }

    //PURPOSE
    // Returns the game object as a void pointer (used for casting to correct type on derived classes)
    void *GetGameObject() const { return m_GameObject; }

    //PURPOSE
    // Sets the network blender for this network object (see netblender.h/.cpp for details)
    //PARAMS
    // blender - The network blender to set
    void SetNetBlender(netBlender *blender) { m_NetBlender = blender; }

    //PURPOSE
    // Returns how many players should be updated per frame for operations batched by players
    virtual u8 GetNumPlayersToUpdatePerBatch() const { return NUM_PLAYERS_TO_UPDATE_DEFAULT; }

	//PURPOSE
	// Returns the reason an object is in/out of scope with the specified player
    //PARAMS
    // inScope      - indicates whether the object is in/out of scope
    // remotePlayer - the remote player in/out of scope
    // reason       - a result code indicating the reason the object is in/out of scope with the specified player
	virtual void LogScopeReason(bool UNUSED_PARAM(inScope), const netPlayer& LOGGING_ONLY(remotePlayer), unsigned LOGGING_ONLY(reason))
	{
#if ENABLE_NETWORK_LOGGING
		if (remotePlayer.GetPhysicalPlayerIndex() != INVALID_PLAYER_INDEX)
		{
			m_LastScopeReason[remotePlayer.GetPhysicalPlayerIndex()] = (u8)reason;
		}
#endif
	}

private:

    //PURPOSE
    // This class needs replacing with the ownership token only
    struct netOwnershipData
    {
        netOwnershipData() :
        m_ownershipToken(MAX_OWNERSHIP_TOKEN_VALUE)
        {
        }
        u32 m_ownershipToken;
    };

    //PURPOSE
    // This class is used to keep track of the starvation
    // counts for this object relative to the remote players
    // in the session
    class netStarvationData
    {
    public:
        //PURPOSE
        // Class constructor
        netStarvationData();

        //PURPOSE
        // Resets the starvation data
        void Reset();

        //PURPOSE
        // Returns whether the object is starving on any player
        bool IsStarvingOnAnyPlayer() const;

        //PURPOSE
        // Returns whether the object is starving on the specified player
        //PARAMS
        // playerIndex - the player to check
        bool IsStarving(PhysicalPlayerIndex playerIndex) const;

        //PURPOSE
        // Increases the starvation count for the specified player
        //PARAMS
        // playerIndex - the player to increase the count for
        void IncreaseStarvationCount(PhysicalPlayerIndex playerIndex);

        //PURPOSE
        // Resets the starvation count for the specified player
        //PARAMS
        // playerIndex - the player to reset the count for
        void ResetStarvationCount(PhysicalPlayerIndex playerIndex);
    
        //PURPOSE
        // Sets the threshold at which an object is consider to be starving.
        // Everytime the bandwidth management code prevents an update being
        // sent for an object the starvation count is increased, this threshold
        // is the count at which the object will report it is starving
        //PARAMS
        // threshold - The starvation threshold
        static void SetStarvationThreshold(u8 threshold);

    private:

        // starvation count is stored in 4 bits per player, so
        // data for two players can be stored in each u8. The player
        // with the odd player index is stored in the low nibble of
        // the byte, while even players stored in the high nibble
        static const unsigned PLAYER1_MASK         = 0x0f;
        static const unsigned PLAYER2_MASK         = 0xf0;
        static const unsigned PLAYER2_SHIFT        = 4;
        static const unsigned MAX_STARVATION_COUNT = 0x0f;

        static u8 ms_StarvationThreshold;

        u8 m_StarvationData[MAX_NUM_PHYSICAL_PLAYERS>>1];
    };

    //PURPOSE
    // Returns the update level sync data for this network object (see netsyncdataul.h/.cpp for details)
    CNetworkSyncDataULBase* GetSyncDataUL()     { Assert(!IsClone()); return static_cast<CNetworkSyncDataULBase*>(&m_SyncData); }

    //PURPOSE
    // Returns the update level sync data for this network object (see netsyncdataul.h/.cpp for details)
    const CNetworkSyncDataULBase* GetSyncDataUL() const     { Assert(!IsClone()); return static_cast<const CNetworkSyncDataULBase*>(&m_SyncData); }

    // The type of network object
    NetworkObjectType m_ObjectType;

    // The unique ID assigned to this object
    ObjectId m_ObjectID;

    // The sync data, used by the sync tree to sync the object with its clones over the network
    CNetworkSyncDataUL<MAX_NUM_PHYSICAL_PLAYERS> m_SyncData;

    // Timer used to avoid thrashing of the creation/deletion of sync data - keeps
    // track of the number of frames before a change is allowed rather than storing
    // a time in order to save memory
    u8 m_syncDataChangeTimer;

    // The physical player in control of this network object
    PhysicalPlayerIndex m_PlayerIndex;

    // The next physical player in the process of being given control
    PhysicalPlayerIndex m_PendingPlayerIndex;

    // Is this object a network clone?
    bool m_IsClone;

    // Local flags used by this object (see flag definitions above)
    NetObjFlags m_LocalFlags;

    // Global flags used by this object (flags broadcast to other machines)
    NetObjFlags m_GlobalFlags;

    // The game object associated with this network object
    void* m_GameObject;

    // Pointer to a blender used to interpolate between sync messages
    netBlender* m_NetBlender;

    // The update level the network object is registered with the sync tree
    u8 m_SyncTreeUpdateLevel;

    // The update batch the network object is registered with the sync tree
    u8 m_SyncTreeUpdateBatch;

    // Stores how many possible update batches this object could be in (1 means updated every frame, 2 every other frame etc...)
    u8 m_NumPossibleUpdateBatches;

    // The physical player index of the player to start with when performing operations that are batched
    // by groups of players (for example some expensive processes may be applied to only one player per frame)
    u8 m_StartIndexForPlayerBatch;

    // Log for use by network objects
    static netLoggingInterface *sm_Log;

    // Pointer to the node containing this object in a network object list
    atDNode<netObject*, datBase> *m_Node;

 	// flags indicating which player machines this object is cloned on
	PlayerFlags m_ClonedState;

	// flags indicating which player machines this object is pending cloning on
	PlayerFlags m_PendingCloningState;

	// flags indicating which player machines this object is pending removal on
	PlayerFlags m_PendingRemovalState;

    // flags indicating which player machines this object is in scope on
    PlayerFlags m_InScopeState;

	// flags indicating which players left while the object was cloned on their machine when local
	PlayerFlags m_ClonedPlayersThatLeft;

    // flags indicating which players this object was updated on (used for batching sends by players)
    PlayerFlags m_LastPlayersSyncUpdated;

	// The data for tracking ownership changing of network objects
    netOwnershipData m_OwnershipData;

    // The data for tracking object starvation for the players in the session
    netStarvationData m_StarvationData;

#if ENABLE_NETWORK_LOGGING
	// The last reason why a remote player was in or out of scope with this net object
	u8 m_LastScopeReason[MAX_NUM_PHYSICAL_PLAYERS];
#endif

#if __DEV
    // An array of the time it is taking to create or remove a clone over each player
    // This is used to catch bad things, eg a network object unable to clone or remove itself
    atFixedArray<u32, MAX_NUM_PHYSICAL_PLAYERS> m_CloningTimers;

    // Keeps track the last frame this object was updated (used to catch objects getting starved of updates)
    unsigned m_LastFrameSyncTreeUpdated;
#endif

#if !__FINAL
    // a descriptive name of this network object for use with logging
    char m_LogName[MAX_LOG_NAME];
#endif
};

//PURPOSE
// This is a helper class to ensure the network object pointer is always set to NULL
// for game objects that are not registered with the network code
class netObjectPtr
{
public:

    netObjectPtr() { m_NetObject = 0; }
    netObject* m_NetObject;
};

// Add this macro to the header file of a base class whose derived classes may become network
// objects
#define NETWORK_OBJECT_PTR_DECL()\
public:\
    bool IsNetworkClone() const { return (GetNetworkObject() && GetNetworkObject()->IsClone()); } \
    netObject* GetNetworkObject() const { return m_networkObject.m_NetObject; }\
    void SetNetworkObject(netObject* pNetObj) { Assert(!(pNetObj && m_networkObject.m_NetObject)); m_networkObject.m_NetObject = pNetObj; }\
    netObjectPtr m_networkObject;

#define NETWORK_OBJECT_PTR_DECL_EX(GameNetworkObjectBaseType)\
public:\
    bool IsNetworkClone() const { return (GetNetworkObject() && GetNetworkObject()->IsClone()); } \
    GameNetworkObjectBaseType* GetNetworkObject() const { return static_cast<GameNetworkObjectBaseType*>(m_networkObject.m_NetObject); }\
    void SetNetworkObject(netObject* pNetObj) { Assert(!(pNetObj && m_networkObject.m_NetObject)); m_networkObject.m_NetObject = pNetObj; }\
    netObjectPtr m_networkObject;

// Add this macro to the header file of a base class whose derived classes may become network
// objects, but not the base class itself.
#define NETWORK_OBJECT_BASECLASS_DECL()\
public:\
    virtual netObject* CreateNetworkObject(const ObjectId,\
                                           const PhysicalPlayerIndex ,\
                                           const NetObjFlags ,\
                                           const NetObjFlags ,\
                                           const unsigned ) { Assertf(false, "Trying to create an invalid network object!"); return 0; } \
    virtual unsigned GetNetworkObjectType() {Assertf(false, "Trying to create an invalid network object!"); return INVALID_OBJECT_TYPE;};

// Add this macro to the header of ALL classes that will become network objects.
// netobjclass : the class name this declaration exists in
// netobjtype : the NETOBJTYPE_ associated with this object.
// You need to include the header that contains the netobjclass as well.
#define NETWORK_OBJECT_TYPE_DECL( netobjclass, netobjtype)\
public:\
    virtual netObject* CreateNetworkObject(const ObjectId objectID,\
                                           const PhysicalPlayerIndex playerIndex,\
                                           const NetObjFlags localFlags,\
                                           const NetObjFlags globalFlags,\
                                           const unsigned numPlayers)\
    {\
        Assert(m_networkObject.m_NetObject == 0);\
        m_networkObject.m_NetObject = rage_new netobjclass(this, netobjtype, objectID, playerIndex, localFlags, globalFlags);\
        m_networkObject.m_NetObject->Init(numPlayers);\
        return m_networkObject.m_NetObject;\
    }\
    static netObject* StaticCreateNetworkObject(const ObjectId objectID,\
                                                const PhysicalPlayerIndex playerIndex,\
                                                const NetObjFlags globalFlags,\
                                                const unsigned numPlayers)\
    {\
        netObject* pNetObj = (netObject*) rage_new netobjclass(NULL, netobjtype, objectID, playerIndex, 0, globalFlags);\
        pNetObj->Init(numPlayers);\
        return pNetObj;\
    }\
    virtual unsigned GetNetworkObjectType()\
    {\
        return netobjtype;\
    }

} // namespace rage

#endif  // NETWORK_OBJECT_H
