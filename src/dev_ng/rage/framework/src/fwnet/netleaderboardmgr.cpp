// 
// netleaderboardmgr.h 
// 
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved. 
//

//Rage headers
#include "rline/rlstats.h"
#include "rline/ros/rlroscommon.h"
#include "rline/ros/rlros.h"
#include "system/memory.h"
#if !__NO_OUTPUT
#include "diag/output.h"
#endif

// framework headers
#include "fwnet/netleaderboardmgr.h"
#include "fwnet/netleaderboardcommon.h"
#include "fwnet/netleaderboardread.h"
#include "fwnet/netleaderboardwrite.h"
#include "fwnet/netscgameconfigparser.h"
#include "fwnet/optimisations.h"

NETWORK_OPTIMISATIONS()

namespace rage
{

RAGE_DEFINE_SUBCHANNEL(net, leaderboard_mgr, DIAG_SEVERITY_DEBUG3)
#undef __net_channel
#define __net_channel net_leaderboard_mgr

/*
#define LARGEST_LEADERBOARD_READ_CLASS sizeof(netLeaderboardRead)
CompileTimeAssert( sizeof(netLeaderboardReadGamersByRow          ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGroupsByRow          ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGamersInGroupsByRow  ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGamersByRank         ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGroupsByRank         ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGamersInGroupsByRank ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGamersByRadius       ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssert( sizeof(netLeaderboardReadGroupsByRadius       ) <= LARGEST_LEADERBOARD_READ_CLASS );
CompileTimeAssertSize(netLeaderboardRead, 44, 44);

#define LARGEST_LEADERBOARD_WRITE_CLASS sizeof(netLeaderboardWrite)
#if __XENON
CompileTimeAssertSize(netLeaderboardWrite, 40, 40);
#else
CompileTimeAssertSize(netLeaderboardWrite, 68, 68);
#endif

#define LARGEST_LEADERBOARD_UPDATE_ROW  sizeof(rlLeaderboardUpdate)
CompileTimeAssertSize(rlLeaderboardUpdate, 1176, 1176);

#define LARGEST_LEADERBOARD_ROW  sizeof(rlLeaderboardRow)
#if __XENON
CompileTimeAssertSize(rlLeaderboardRow, 720, 720);
#else
CompileTimeAssertSize(rlLeaderboardRow, 800, 800);
#endif
*/


// ----------------- netLeaderboardManageAllocator

netLeaderboardManageAllocator::netLeaderboardManageAllocator()
: m_PreviousAllocator(0)
, m_AllocatorInUse(0)
{
}

netLeaderboardManageAllocator::~netLeaderboardManageAllocator()
{
	Shutdown();
}

void
netLeaderboardManageAllocator::Init(sysMemAllocator* allocator, const char* OUTPUT_ONLY(allocatorName))
{
	gnetAssert(0 == m_Allocator);
	gnetAssert(0 == m_PreviousAllocator);
	gnetAssert(0 == m_UsedMemorySize);
	gnetAssert(0 == m_AllocatorInUse);

	if (gnetVerify(allocator))
	{
		m_Allocator = allocator;

#if !__NO_OUTPUT
		if (allocatorName)
		{
			m_AllocatorName = allocatorName;
		}
		else
		{
			m_AllocatorName = "UNKNOWN";
		}
#endif // !__FINAL
	}
}

void  
netLeaderboardManageAllocator::Shutdown()
{
#if !__FINAL
		gnetAssert(0 == m_AllocatorInUse);
		m_UsedMemorySize    = 0;
#endif // !__FINAL

	m_Allocator         = 0;
	m_PreviousAllocator = 0;
	m_AllocatorInUse    = 0;
}

#if __PPU
extern u32 g_bDontUseResidualAllocator;
#endif

void 
netLeaderboardManageAllocator::StartUsingMemAllocator()
{
	gnetAssertf(m_Allocator != 0, "\"%s\" Leaderboard memory allocator has not been initialized.", m_AllocatorName);

	if(m_Allocator)
	{
		if (m_AllocatorInUse == 0)
		{
			m_PreviousAllocator = &sysMemAllocator::GetCurrent();
			sysMemAllocator::SetCurrent(*m_Allocator);
		}

#if !__NO_OUTPUT
		m_UsedMemorySize = m_PreviousMemorySize = (s32) m_Allocator->GetMemoryUsed();
		gnetDebug2("\"%s\" Start allocator use with used=\"%d\"bytes (Allocator says \"%d\") ", m_AllocatorName, m_UsedMemorySize, m_PreviousMemorySize);
#endif // !__FINAL
	}
	m_AllocatorInUse++;

	PPU_ONLY(++g_bDontUseResidualAllocator);
}

void 
netLeaderboardManageAllocator::StopUsingMemAllocator()
{
	PPU_ONLY(--g_bDontUseResidualAllocator);
	m_AllocatorInUse--;

	gnetAssertf(m_AllocatorInUse >= 0, "\"%s\" StopUsingMemAllocator called more times than StartUsingMemAllocator!", m_AllocatorName);

	if(gnetVerifyf(m_Allocator, "\"%s\" Memory allocator heap doesn't exist", m_AllocatorName))
	{
		if(m_AllocatorInUse == 0)
		{
			sysMemAllocator::SetCurrent(*m_PreviousAllocator);
			m_PreviousAllocator = 0;
		}

#if !__FINAL
		int currentMemSize = (s32) m_Allocator->GetMemoryUsed();
		m_UsedMemorySize += (currentMemSize - m_PreviousMemorySize);

		gnetDebug2("\"%s\" Stop allocator use with used=\"%d\"bytes (Allocator says \"%d\", previous=\"%d\") ", m_AllocatorName, m_UsedMemorySize, currentMemSize, m_PreviousMemorySize);

		m_PreviousMemorySize  = currentMemSize;
#endif // !__FINAL
	}
}

sysMemAllocator* 
netLeaderboardManageAllocator::GetAllocator()
{
	return m_Allocator;
}

// ----------------- netLeaderboardReadMgr 

netLeaderboardReadMgr::netLeaderboardReadMgr() 
{
}

netLeaderboardReadMgr::~netLeaderboardReadMgr()
{
	Shutdown();
}

void
netLeaderboardReadMgr::Init(sysMemAllocator* allocator)
{
	if (gnetVerify(allocator))
	{
		m_Allocator.Init(allocator, "READ_STATS");
	}
}

void
netLeaderboardReadMgr::Shutdown()
{
	Clear();
	m_Allocator.Shutdown();
}

void  
netLeaderboardReadMgr::Update()
{
	/*
	netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	netLeaderboardReadListNode* nextNode = node;
	while (nextNode)
	{
		//Set the one we're working on now.
		node = nextNode;

		//Pre-increment for the next iteration.
		nextNode = nextNode->GetNext();

		netLeaderboardRead* lb = node->Data;
		if (gnetVerify(lb) && lb->Finished())
		{
			//Download statistics has Failed
			if (lb->Failed())
			{
				gnetError("Read leaderboard Statistics has FAILED.");

				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

				if (node->Data)
				{
					delete node->Data;
					node->Data = NULL;
				}

				m_Leaderboards.PopNode(*node);

				delete node;
			}
		}
	}
	*/
}

bool
netLeaderboardReadMgr::Pending() const
{
	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while(node)
	{
		const netLeaderboardRead* lb = node->Data;
		if (gnetVerify(lb) && lb->Pending())
		{
			return true;
		}

		node = node->GetNext();
	}

	return false;
}

const netLeaderboardRead*
netLeaderboardReadMgr::AddReadByRow(const int localGamerIndex
									,const unsigned id
									,const rlLeaderboard2Type type
									,const rlLeaderboard2GroupSelector& groupSelector
									,const rlClanId clanId
									,const unsigned numGamers, const rage::rlGamerHandle* gamerHandlers
									,const unsigned numClans,  const rlClanId* clanIds 
									,const unsigned numGroups, const rlLeaderboard2GroupSelector* groupSelectors)
{

	netLeaderboardRead* lb = NULL;
	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return lb;

	gnetDebug3("netLeaderboardReadMgr::AddReadByRow - %s - TotalCount=%d", lbConf->GetName(), CountOfLeaderboards());

	bool ret = false;

	if (type == RL_LEADERBOARD2_TYPE_PLAYER && gnetVerify(0<numGamers) && gnetVerify(gamerHandlers) && gnetVerify(groupSelector.m_NumGroups == 0))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGamersByRow >("AddReadByRow"))
			{
				lb = rage_new netLeaderboardReadGamersByRow(id, type);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersByRow*>(lb)->Start(localGamerIndex, groupSelector, numGamers, gamerHandlers)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}
	else if (type == RL_LEADERBOARD2_TYPE_CLAN && gnetVerify(0<numClans) && gnetVerify(clanIds))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadClansByRow >("AddReadByRow"))
			{
				lb = rage_new netLeaderboardReadClansByRow(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClansByRow*>(lb)->Start(localGamerIndex, groupSelector, clanIds, numClans)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER && gnetVerify(0<numGamers) && gnetVerify(gamerHandlers) && gnetVerify(clanId != RL_INVALID_CLAN_ID))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadClanMembersByRow >("AddReadByRow"))
			{
				lb = rage_new netLeaderboardReadClanMembersByRow(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClanMembersByRow*>(lb)->Start(localGamerIndex, groupSelector, clanId, numGamers, gamerHandlers)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_GROUP && gnetVerify(0 < numGroups) && gnetVerify(groupSelectors))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGroupsByRow >("AddReadByRow"))
			{
				lb = rage_new netLeaderboardReadGroupsByRow(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGroupsByRow*>(lb)->Start(localGamerIndex, groupSelectors, numGroups)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}
	else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER && gnetVerify(0<numGamers) && gnetVerify(gamerHandlers) && gnetVerify(groupSelector.m_NumGroups > 0))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGroupMembersByRow >("AddReadByRow"))
			{
				lb = rage_new netLeaderboardReadGroupMembersByRow(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGroupMembersByRow*>(lb)->Start(localGamerIndex, groupSelector, numGamers, gamerHandlers)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	if (!gnetVerify(ret))
	{
		gnetDebug2("FAILED to Add read leaderboard id=\"%s:%d\"", lbConf->GetName(), id);

		if (lb)
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			delete lb;
		}

		lb = NULL;
	}

	return lb;
}

const netLeaderboardRead*
netLeaderboardReadMgr::AddReadByPlatform(const int localGamerIndex
										,const unsigned id
										,const rlLeaderboard2GroupSelector& groupSelector
										,const char* gamerHandler
										,const char* platform)
{
	netLeaderboardRead* lb = NULL;
	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return lb;

	gnetDebug3("netLeaderboardReadMgr::AddReadByRowByPlatform - %s - TotalCount=%d", lbConf->GetName(), CountOfLeaderboards());

	bool ret = false;

	if (gnetVerify(gamerHandler) && gnetVerify(platform))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadByGamerByPlatformTask >("AddReadByPlatform"))
			{
				lb = rage_new netLeaderboardReadByGamerByPlatformTask(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadByGamerByPlatformTask*>(lb)->Start(localGamerIndex, groupSelector, gamerHandler, platform)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	if (!gnetVerify(ret))
	{
		gnetDebug2("FAILED to Add read leaderboard id=\"%s:%d\"", lbConf->GetName(), id);

		if (lb)
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			delete lb;
		}

		lb = NULL;
	}

	return lb;
}

const netLeaderboardRead*
netLeaderboardReadMgr::AddReadByRank(const int localGamerIndex
									 ,const unsigned id
									 ,const rlLeaderboard2Type type
									 ,const rlLeaderboard2GroupSelector& groupSelector
									 ,const rlClanId clanId
									 ,const unsigned maxNumRows
									 ,const unsigned rankStart)
{
	netLeaderboardRead* lb = NULL;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return lb;

	bool ret = false;
	if (gnetVerify(0 < rankStart))
	{
		gnetDebug3("netLeaderboardReadMgr::AddReadByRank - %s - TotalCount=%d", lbConf->GetName(), CountOfLeaderboards());

		if (type == RL_LEADERBOARD2_TYPE_PLAYER && gnetVerify(0<rankStart) && gnetVerify(0<maxNumRows))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadGamersByRank >("AddReadByRank"))
				{
					lb = rage_new netLeaderboardReadGamersByRank(id, type);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersByRank*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, rankStart)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_CLAN && gnetVerify(0<rankStart) && gnetVerify(0<maxNumRows))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadClansByRank >("AddReadByRank"))
				{
					lb = rage_new netLeaderboardReadClansByRank(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClansByRank*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, rankStart)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER && gnetVerify(0<rankStart) && gnetVerify(0<maxNumRows) && gnetVerify(clanId != RL_INVALID_CLAN_ID))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadClanMembersByRank >("AddReadByRank"))
				{
					lb = rage_new netLeaderboardReadClanMembersByRank(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClanMembersByRank*>(lb)->Start(localGamerIndex, groupSelector, clanId, maxNumRows, rankStart)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_GROUP && gnetVerify(0<rankStart) && gnetVerify(0<maxNumRows))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadGroupsByRank >("AddReadByRank"))
				{
					lb = rage_new netLeaderboardReadGroupsByRank(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGroupsByRank*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, rankStart)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}
		else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER && gnetVerify(0<rankStart) && gnetVerify(0<maxNumRows))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadGamersInGroupsByRank >("AddReadByRank"))
				{
					lb = rage_new netLeaderboardReadGamersInGroupsByRank(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersInGroupsByRank*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, rankStart)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}

		if (!gnetVerify(ret))
		{
			gnetDebug2("FAILED to Add read leaderboard id=\"%s:%d\"", lbConf->GetName(), id);

			if (lb)
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				delete lb;
			}

			lb = NULL;
		}
	}

	return lb;
}

const netLeaderboardRead*
netLeaderboardReadMgr::AddReadByRadius(const int localGamerIndex
									   ,const unsigned id
									   ,const rlLeaderboard2Type type
									   ,const rlLeaderboard2GroupSelector& groupSelector
									   ,unsigned int radius
									   ,const rlGamerHandle& pivotGamerHandle
									   ,const rlClanId pivotClanId)
{
	netLeaderboardRead* lb = NULL;
	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return lb;

	bool ret = false;
	if (gnetVerify(radius > 0))
	{
		gnetDebug3("netLeaderboardReadMgr::AddReadByRadius - %s - TotalCount=%d", lbConf->GetName(), CountOfLeaderboards());

		if (type == RL_LEADERBOARD2_TYPE_PLAYER && gnetVerify(pivotGamerHandle.IsValid()))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadGamersByRadius >("AddReadByRadius"))
				{
					lb = rage_new netLeaderboardReadGamersByRadius(id, type);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersByRadius*>(lb)->Start(localGamerIndex, groupSelector, radius, pivotGamerHandle)))
			{
				ret = AddReadToReadList(static_cast< netLeaderboardRead* > (lb));
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_CLAN && gnetVerify(pivotClanId != RL_INVALID_CLAN_ID))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadClansByRadius >("AddReadByRadius"))
				{
					lb = rage_new netLeaderboardReadClansByRadius(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClansByRadius*>(lb)->Start(localGamerIndex, groupSelector, radius, pivotClanId)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER && gnetVerify(pivotGamerHandle.IsValid()) && gnetVerify(pivotClanId != RL_INVALID_CLAN_ID))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadClanMembersByRadius >("AddReadByRadius"))
				{
					lb = rage_new netLeaderboardReadClanMembersByRadius(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClanMembersByRadius*>(lb)->Start(localGamerIndex, groupSelector, radius, pivotGamerHandle, pivotClanId)))
			{
				ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_GROUP && gnetVerify(groupSelector.m_NumGroups>0))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadGroupsByRadius >("AddReadByRadius"))
				{
					lb = rage_new netLeaderboardReadGroupsByRadius(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGroupsByRadius*>(lb)->Start(localGamerIndex, groupSelector, radius)))
			{
				ret = AddReadToReadList(static_cast< netLeaderboardRead* > (lb));
			}
		}

		else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER && gnetVerify(pivotGamerHandle.IsValid()) && gnetVerify(groupSelector.m_NumGroups>0))
		{
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				if (allocHelper.CanAllocate< netLeaderboardReadGroupMembersByRadius >("AddReadByRadius"))
				{
					lb = rage_new netLeaderboardReadGroupMembersByRadius(id);
					if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
				}
			}

			if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGroupMembersByRadius*>(lb)->Start(localGamerIndex, groupSelector, radius, pivotGamerHandle)))
			{
				ret = AddReadToReadList(static_cast< netLeaderboardRead* > (lb));
			}
		}

		if (!gnetVerify(ret))
		{
			gnetDebug2("FAILED to Add read leaderboard id=\"%s:%d\"", lbConf->GetName(), id);

			if (lb)
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				delete lb;
			}

			lb = NULL;
		}
	}

	return lb;
}


const netLeaderboardRead*
netLeaderboardReadMgr::AddReadByScoreInt(const int localGamerIndex
										 ,const unsigned id
										 ,const rlLeaderboard2Type type
										 ,const rlLeaderboard2GroupSelector& groupSelector
										 ,const rlClanId clanId
										 ,const unsigned maxNumRows
										 ,const s64 score)
{
	netLeaderboardRead* lb = NULL;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return lb;

	bool ret = false;
	gnetDebug3("netLeaderboardReadMgr::AddReadByScoreInt - %s - TotalCount=%d", lbConf->GetName(), CountOfLeaderboards());

	if (type == RL_LEADERBOARD2_TYPE_PLAYER && gnetVerify(0<maxNumRows))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGamersByScoreInt >("AddReadByScoreInt"))
			{
				lb = rage_new netLeaderboardReadGamersByScoreInt(id, type);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersByScoreInt*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_CLAN && gnetVerify(0<maxNumRows))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadClansByScoreInt >("AddReadByScoreInt"))
			{
				lb = rage_new netLeaderboardReadClansByScoreInt(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClansByScoreInt*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER && gnetVerify(0<maxNumRows) && gnetVerify(clanId != RL_INVALID_CLAN_ID))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadClanMembersByScoreInt >("AddReadByScoreInt"))
			{
				lb = rage_new netLeaderboardReadClanMembersByScoreInt(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClanMembersByScoreInt*>(lb)->Start(localGamerIndex, groupSelector, clanId, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_GROUP)
	{
		gnetDebug3("netLeaderboardReadMgr::AddReadByScoreInt - %s - Type not supported RL_LEADERBOARD2_TYPE_GROUP", lbConf->GetName());
	}
	else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER && gnetVerify(0<maxNumRows))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGamersInGroupsByScoreInt >("AddReadByScoreInt"))
			{
				lb = rage_new netLeaderboardReadGamersInGroupsByScoreInt(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersInGroupsByScoreInt*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	if (!gnetVerify(ret))
	{
		gnetDebug2("FAILED to Add read leaderboard id=\"%s:%d\"", lbConf->GetName(), id);

		if (lb)
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			delete lb;
		}

		lb = NULL;
	}

	return lb;
}

const netLeaderboardRead*
netLeaderboardReadMgr::AddReadByScoreFloat(const int localGamerIndex
										   ,const unsigned id
										   ,const rlLeaderboard2Type type
										   ,const rlLeaderboard2GroupSelector& groupSelector
										   ,const rlClanId clanId
										   ,const unsigned maxNumRows
										   ,const double score)
{
	netLeaderboardRead* lb = NULL;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);
	if (!gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
		return lb;

	bool ret = false;
	gnetDebug3("netLeaderboardReadMgr::AddReadByScoreFloat - %s - TotalCount=%d", lbConf->GetName(), CountOfLeaderboards());

	if (type == RL_LEADERBOARD2_TYPE_PLAYER && gnetVerify(0<maxNumRows))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGamersByScoreFloat >("AddReadByScoreFloat"))
			{
				lb = rage_new netLeaderboardReadGamersByScoreFloat(id, type);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersByScoreFloat*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_CLAN && gnetVerify(0<maxNumRows))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadClansByScoreFloat >("AddReadByScoreFloat"))
			{
				lb = rage_new netLeaderboardReadClansByScoreFloat(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClansByScoreFloat*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_CLAN_MEMBER && gnetVerify(0<maxNumRows) && gnetVerify(clanId != RL_INVALID_CLAN_ID))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadClanMembersByScoreFloat >("AddReadByScoreFloat"))
			{
				lb = rage_new netLeaderboardReadClanMembersByScoreFloat(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadClanMembersByScoreFloat*>(lb)->Start(localGamerIndex, groupSelector, clanId, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	else if (type == RL_LEADERBOARD2_TYPE_GROUP && gnetVerify(0<maxNumRows))
	{
		gnetDebug3("netLeaderboardReadMgr::AddReadByScoreFloat - %s - Type not supported RL_LEADERBOARD2_TYPE_GROUP", lbConf->GetName());
	}
	else if (type == RL_LEADERBOARD2_TYPE_GROUP_MEMBER && gnetVerify(0<maxNumRows))
	{
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardReadGamersInGroupsByScoreFloat >("AddReadByScoreFloat"))
			{
				lb = rage_new netLeaderboardReadGamersInGroupsByScoreFloat(id);
				if(lb) lb->SetAllocator( m_Allocator.GetAllocator() );
			}
		}

		if (gnetVerify(lb) && gnetVerify(static_cast<netLeaderboardReadGamersInGroupsByScoreFloat*>(lb)->Start(localGamerIndex, groupSelector, maxNumRows, score)))
		{
			ret = AddReadToReadList( static_cast< netLeaderboardRead* > (lb) );
		}
	}

	if (!gnetVerify(ret))
	{
		gnetDebug2("FAILED to Add read leaderboard id=\"%s:%d\"", lbConf->GetName(), id);

		if (lb)
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			delete lb;
		}

		lb = NULL;
	}
	return lb;
}

const netLeaderboardRead*  
netLeaderboardReadMgr::GetLeaderboardByType(const unsigned id, const rlLeaderboard2Type type, const int lbIndex) const
{
	int index = 0;

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		const netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && id == lb->GetId() && type == lb->GetReadType())
		{
			if (index == lbIndex)
			{
				return lb;
			}

			index++;
		}

		node = node->GetNext();
	}

	return NULL;
}

const netLeaderboardRead*  
netLeaderboardReadMgr::GetLeaderboardByIndex(const unsigned id, const unsigned index) const
{
	unsigned count = 0;

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		const netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && id == lb->GetId())
		{
			if (index == count)
			{
				return lb;
			}

			count++;
		}

		node = node->GetNext();
	}

	return NULL;
}

bool
netLeaderboardReadMgr::IsGamerPresent(const unsigned id, const rlGamerHandle& gamerHandler) const
{
	gnetAssertf(gamerHandler.IsValid(), "netreadleaderboardsmgr::IsGamerPresent - gamerHandler is not valid");

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while(node)
	{
		const netLeaderboardRead* lb = node->Data;
		if (gnetVerify(lb) && id == lb->GetId() && lb->IsGamerPresent(gamerHandler))
		{
			return true;
		}

		node = node->GetNext();
	}

	return false;
}

bool
netLeaderboardReadMgr::CanReadStatistics(const rlGamerHandle& gamerHandler) const
{
	gnetAssertf(gamerHandler.IsValid(), "netreadleaderboardsmgr::IsGamerPresent - gamerHandler is not valid");

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while(node)
	{
		const netLeaderboardRead* lb = node->Data;
		if (gnetVerify(lb) && lb->IsGamerPresent(gamerHandler) && !lb->CanReadStatistics())
		{
			return false;
		}

		node = node->GetNext();
	}

	return true;
}

unsigned 
netLeaderboardReadMgr::CountOfLeaderboards() const 
{
	unsigned count = 0;

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb))
		{
			count++;
		}

		node = node->GetNext();
	}

	return count;
}

unsigned 
netLeaderboardReadMgr::CountOfLeaderboards(const unsigned id) const 
{
	unsigned count = 0;

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && id == lb->GetId())
		{
			count++;
		}

		node = node->GetNext();
	}

	return count;
}

unsigned 
netLeaderboardReadMgr::CountOfLeaderboards(const unsigned id, const rlLeaderboard2Type type) const 
{
	unsigned count = 0;

	const netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && id == lb->GetId() && type == lb->GetReadType())
		{
			count++;
		}

		node = node->GetNext();
	}

	return count;
}

const char* 
netLeaderboardReadMgr::GetIdName(const unsigned, const bool) const
{
	return "NAME_NOT_AVAILABLE";
}

void
netLeaderboardReadMgr::Clear()
{
	gnetDebug3("netLeaderboardReadMgr::Clear");
	CancelAll();
	DeleteAll();
}

void
netLeaderboardReadMgr::Cancel(const unsigned id)
{
	gnetDebug3("netLeaderboardReadMgr::Cancel");

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

	netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && id == lb->GetId() && lb->Pending())
			{
				lb->Cancel();
			}

		node = node->GetNext();
	}
}

void
netLeaderboardReadMgr::CancelAll()
{
	gnetDebug3("netLeaderboardReadMgr::CancelAll");

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

	netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && lb->Pending())
		{
			lb->Cancel();
		}

		node = node->GetNext();
	}
}

bool 
netLeaderboardReadMgr::Delete(const unsigned id, const rlLeaderboard2Type type, const int lbIndex)
{
	bool ret = false;

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

	int index = 0;

	netLeaderboardReadListNode* node     = m_Leaderboards.GetHead();
	netLeaderboardReadListNode* nextNode = node;

	while (nextNode)
	{
		//Set the one we're working on now.
		node = nextNode;

		//Pre-increment for the next iteration.
		nextNode = nextNode->GetNext();

		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb) && id == lb->GetId() && type == lb->GetReadType())
		{
			if (lbIndex == index || -1 == lbIndex)
			{
				lb->Cancel();

				if (gnetVerify(node->Data))
				{
					delete node->Data;
				}
				node->Data = NULL;

				m_Leaderboards.PopNode(*node);

				delete node;

				ret = true;
			}
			index++;
		}
	}

	return ret;
}

bool 
netLeaderboardReadMgr::Delete(const netLeaderboardRead* pThisLb)
{
	bool ret = false;

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

	int index = 0;

	netLeaderboardReadListNode* node     = m_Leaderboards.GetHead();
	netLeaderboardReadListNode* nextNode = node;

	while (nextNode)
	{
		//Set the one we're working on now.
		node = nextNode;

		//Pre-increment for the next iteration.
		nextNode = nextNode->GetNext();

		netLeaderboardRead* lb = node->Data;

		if (gnetVerify(lb))
		{
			if (lb == pThisLb)
			{
				lb->Cancel();

				if (gnetVerify(node->Data))
				{
					delete node->Data;
				}
				node->Data = NULL;

				m_Leaderboards.PopNode(*node);

				delete node;

				ret = true;
			}
			index++;
		}
	}

	return ret;
}

void
netLeaderboardReadMgr::DeleteAll()
{
	gnetDebug3("netLeaderboardReadMgr::DeleteAll");

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

	//First, delete all the lb readers
	netLeaderboardReadListNode* node = m_Leaderboards.GetHead();
	while (node)
	{
		gnetAssert(node->Data);
		if (node->Data)
		{
			delete node->Data;
			node->Data = NULL;
		}

		node = node->GetNext();
	}

	//Then let the list delete all its nodes.
	m_Leaderboards.DeleteAll();
}

bool 
netLeaderboardReadMgr::AddReadToReadList(netLeaderboardRead* lb)
{
	bool ret = false;

	if (gnetVerify(lb))
	{
		netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

		netLeaderboardReadListNode* node = rage_new netLeaderboardReadListNode();
		if (gnetVerify(node))
		{
			gnetDebug2("Pushed leaderboard %s:%d for reading", GetIdName(lb->GetId(), lb->GetReadType() != RL_LEADERBOARD2_TYPE_PLAYER), lb->GetId());
			node->Data = lb;
			m_Leaderboards.Append(*node);
			ret = true;
		}
	}

	return ret;
}

//////////////////////////////////////////////////////////////////////////
//		netLeaderboardWriteMgr
//
netLeaderboardWriteMgr::netLeaderboardWriteMgr()
{
	m_Status.Reset();
}

netLeaderboardWriteMgr::~netLeaderboardWriteMgr()
{
	Shutdown();
}	

void 
netLeaderboardWriteMgr::Init( sysMemAllocator* allocator )
{
	if (gnetVerify(allocator))
	{
		m_Allocator.Init(allocator, "WRITE_STATS");
	}

	m_Status.Reset();
}

void
netLeaderboardWriteMgr::Shutdown()
{
	Clear();
	m_Allocator.Shutdown();
}

void
netLeaderboardWriteMgr::Update()
{
	if(Finished())
	{
		gnetDebug2("Write leaderboard Statistics FINISHED - %s", (Succeeded() ? "SUCCEEDED" : Failed() ? "FAILED" : "CANCELLED") );

		netLeaderboardWriteListNode* node = m_Leaderboards.GetHead();
		netLeaderboardWriteListNode* nextNode = node;
		while (nextNode)
		{
			node = nextNode;
			nextNode = nextNode->GetNext();

			netLeaderboardWrite* lb = node->Data;
			if (gnetVerify(lb) && !lb->PendingFlush())
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

				delete node->Data;
				node->Data = NULL;

				m_Leaderboards.PopNode(*node);

				delete node;
			}
		}

		m_Status.Reset();
	}
}

void
netLeaderboardWriteMgr::Clear()
{
	Cancel();
	DeleteAll();
}

void
netLeaderboardWriteMgr::Cancel()
{
	gnetDebug3("netLeaderboardWriteMgr::Cancel");

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
	if (gnetVerify(Pending()))
	{
		rlStats::Cancel(&m_Status);
	}

	m_Status.Reset();
}

void
netLeaderboardWriteMgr::DeleteAll()
{
	gnetAssertf(netStatus::NET_STATUS_NONE == m_Status.GetStatus(), "Invalid flush status=%d.", m_Status.GetStatus());

	gnetDebug3("netLeaderboardWriteMgr::DeleteAll");

	netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);

	//First delete all the LBs writers
	netLeaderboardWriteListNode* node = m_Leaderboards.GetHead();

	while (node)
	{
		gnetAssert(node->Data);

		//Delete the netLeaderboardWrite stuff
		if (node->Data)
		{
			delete node->Data;
			node->Data = NULL;
		}

		//Now get the new tail.
		node = node->GetNext();
	}

	//Then have the list delete all it's nodes.
	m_Leaderboards.DeleteAll();
}

unsigned
netLeaderboardWriteMgr::GetLeaderboardCount() const
{
	unsigned count = 0;

	for(const netLeaderboardWriteListNode* node = m_Leaderboards.GetHead(); node != NULL; node = node->GetNext())
	{
		const netLeaderboardWrite* lb = node->Data;
		if (gnetVerify(lb))
		{
			count++;
		}
	}

	return count;
}

netLeaderboardWrite* 
netLeaderboardWriteMgr::AddLeaderboard(const unsigned id, const rlLeaderboard2GroupSelector& groupSelector, const rlClanId& clanId)
{
	netLeaderboardWrite* lbw = 0;

	const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard(id);

	//Validate that all our stuff is valid
	if (gnetVerifyf(lbConf, "Missing Leaderboard id=\"%d\"", id))
	{
		//Create a netLeaderboardWrite object to represent this submission/
		{
			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardWrite >("SubmitStatsForWrite"))
			{
				lbw = rage_new netLeaderboardWrite();
			}
		}

		bool result = false;

		//Initialize the guys
		//@TODO we're ignoring the whole ranked thing for now
		if(gnetVerify(lbw))
		{
			lbw->Init(m_Allocator.GetAllocator(), id, groupSelector, clanId);

			netLeaderboardWriteListNode* node = 0;

			netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
			if (allocHelper.CanAllocate< netLeaderboardWriteListNode >("SubmitStatsForWrite"))
			{
				//Since we've successfully initialized, add to the list
				node = rage_new netLeaderboardWriteListNode();
			}

			if (gnetVerify(node))
			{
				gnetDebug2("Pushed leaderboard id=\"%s : %d\" for submission.", lbConf->GetName(), id);
				node->Data = lbw;
				m_Leaderboards.Append(*node);
				result = true;
			}
		}

		//If we failed, we need to clean up after ourselves
		if(!result)
		{
			gnetDebug2("FAILED to submit leaderboard id=\"%s : %d\"", lbConf->GetName(), id);

			if (lbw)
			{
				netLeaderboardManageAllocatorScopeHelper allocHelper(m_Allocator);
				delete lbw;
			}

			lbw = 0;
		}
	}

	return lbw;
}

void
netLeaderboardWriteMgr::FlushAll(const int localGamerIndex)
{
	gnetAssertf(netStatus::NET_STATUS_NONE == m_Status.GetStatus(), "Invalid flush status=%d.", m_Status.GetStatus());
	if (netStatus::NET_STATUS_NONE != m_Status.GetStatus())
		return;

	if (!rlRos::HasPrivilege(localGamerIndex, RLROS_PRIVILEGEID_LEADERBOARD_WRITE))
	{
		gnetError("Can not make any leaderboard submissions - RLROS_PRIVILEGEID_LEADERBOARD_WRITE.");

		for(netLeaderboardWriteListNode* node=m_Leaderboards.GetHead(); node!=NULL; node=node->GetNext())
		{
			netLeaderboardWrite* lb = node->Data;
			if (gnetVerify(lb) && lb->PendingFlush())
				lb->ClearPendingFlush();
		}

		return;
	}

	gnetDebug2("Flushing all current writes");

	atArray < const rlLeaderboard2Update* > updates;
	updates.clear();

	for(netLeaderboardWriteListNode* node=m_Leaderboards.GetHead(); node!=NULL; node=node->GetNext())
	{
		netLeaderboardWrite* lb = node->Data;

		if (gnetVerify(lb) && lb->PendingFlush())
		{
			lb->ClearPendingFlush();

			const Leaderboard* lbConf = GAME_CONFIG_PARSER.GetLeaderboard( lb->m_LeaderboardId );
			gnetAssert(lbConf);

			if (!lbConf)
				continue;

			gnetDebug2("..... leaderboard id=\"%s : %d\"", lbConf->GetName(), lb->m_LeaderboardId);

			u32 numValues = lbConf->m_columns.GetCount();
			for (int i=0; i<lbConf->m_columns.GetCount(); i++)
			{
				if (lbConf->m_columns[i].m_aggregation.m_type == AggType_Ratio)
				{
					gnetDebug2("..... Ignore Ratio column \"%s\"", lbConf->m_columns[i].GetName());
					numValues--;
				}
			}

			gnetAssertf(lb->m_NumValues > 0, "Failed to Flush leaderboard id=\"%s : %d\", because there are no columns set, Pending Flush has been cleared for the board.", lbConf->GetName(), lb->m_LeaderboardId);

#if HACK_GTA4
			//Ian requested this for V... 
			{
				if (lb->m_NumValues != numValues)
				{
					gnetError("Columns Expected:");
					for (int i=0; i<lbConf->m_columns.GetCount(); i++)
					{
						if (lbConf->m_columns[i].m_aggregation.m_type == AggType_Ratio)
						{
							gnetError("..... Column (%d): \"%s, %d\" - Ratio column is ignored.", i, lbConf->m_columns[i].GetName(), lbConf->m_columns[i].m_id);
						}
						else
						{
							const LeaderboardInput* input = GAME_CONFIG_PARSER.GetInputById( lbConf->m_columns[i].m_aggregation.m_gameInput.m_inputId );
							if (gnetVerifyf(input, "Input Id %d, not found", lb->m_InputValues[i].InputId))
							{
								gnetError("..... (%d): Column=\"%s,%d\", Input id=\"%s, %d\""
									,i
									,lbConf->m_columns[i].GetName()
									,lbConf->m_columns[i].m_id
									,input->GetName()
									,input->m_id);
							}
						}
					}
					gnetError("Columns Actually Set:");
					for (unsigned i=0; i<lb->m_NumValues; i++)
					{
						const LeaderboardInput* input = GAME_CONFIG_PARSER.GetInputById( lb->m_InputValues[i].InputId );
						if (gnetVerifyf(input, "Input Id %d, not found", lb->m_InputValues[i].InputId))
						{
							gnetError("..... (%d): Input id=\"%s, %d\"", i, input->GetName(), input->m_id);
						}
					}
				}

				gnetAssertf(lb->m_NumValues == numValues, "Warning Flush leaderboard id=\"%s : %d\", \
														  because the number of columns set %d dont match \
														  the expected number %d."
							,lbConf->GetName()
							,lb->m_LeaderboardId
							,lb->m_NumValues
							,numValues);
			}
#endif // HACK_GTA4

			if (lb->m_NumValues > 0)
			{
				updates.PushAndGrow(lb);

#if !__NO_OUTPUT
				for (unsigned j=0; j<lb->m_NumValues; j++)
				{
					const LeaderboardInput* input = GAME_CONFIG_PARSER.GetInputById( lb->m_InputValues[j].InputId );
					if (gnetVerify(input))
					{
						if (InputDataType_int == input->m_dataType || InputDataType_long == input->m_dataType)
						{
							gnetDebug2("............ stat column=\"%d\"(%d), value=\"%" I64FMT "d\""
								,lb->m_InputValues[j].InputId
								,input->m_dataType
								,lb->m_InputValues[j].Value.Int64Val);
						}
						else if (gnetVerify(InputDataType_double == input->m_dataType))
						{
							gnetDebug2("............ stat column=\"%d\"(%d), value=\"%f\""
								,lb->m_InputValues[j].InputId
								,input->m_dataType
								,lb->m_InputValues[j].Value.DoubleVal);
						}
					}
				}
#endif // !__NO_OUTPUT
			}
		}
	}

	if (updates.GetCount() > 0)
	{
		rlLeaderboard2PlayerStats::Write(localGamerIndex, updates.GetElements(), updates.GetCount(), &m_Status);
	}
}

}; // namespace

//eof
