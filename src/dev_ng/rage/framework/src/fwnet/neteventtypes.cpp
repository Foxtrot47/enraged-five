//
// neteventtypes.cpp
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//
#include "fwnet/neteventtypes.h"

#include "fwnet/netarrayhandlertypes.h"
#include "fwnet/netchannel.h"
#include "fwnet/neteventmgr.h"
#include "fwnet/netinterface.h"
#include "fwnet/netobjectidmgr.h"
#include "fwnet/netobjectmgrbase.h"
#include "fwnet/netplayer.h"
#include "fwnet/netserialisers.h"

namespace rage
{

// ===========================================================================================================
// OBJECT ID FREED EVENT
// ===========================================================================================================

objectIdFreedEvent::objectIdFreedEvent() :
netGameEvent(OBJECT_ID_FREED_EVENT, false)
, m_NumObjectIDs(0)
, m_PlayerIndex(INVALID_PLAYER_INDEX)
#if ENABLE_NETWORK_LOGGING
, m_IgnoredIDs(false)
#endif // ENABLE_NETWORK_LOGGING
{
}

objectIdFreedEvent::objectIdFreedEvent(ObjectId   *objectIDsToFree,
                                       unsigned    numObjectIDs,
                                       PhysicalPlayerIndex playerIndex) :
netGameEvent(OBJECT_ID_FREED_EVENT, false)
, m_NumObjectIDs(numObjectIDs)
, m_PlayerIndex(playerIndex)
#if ENABLE_NETWORK_LOGGING
, m_IgnoredIDs(false)
#endif // ENABLE_NETWORK_LOGGING
{
    for(unsigned index = 0; index < m_NumObjectIDs; index++)
    {
        m_ObjectIDsToFree[index] = objectIDsToFree[index];
    }
}

void objectIdFreedEvent::EventHandler(datBitBuffer &bitBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence)
{
    objectIdFreedEvent netGameEvent;
    netInterface::GetEventManager().HandleEvent(&netGameEvent, bitBuffer, fromPlayer, toPlayer, messageSeq, eventID, eventIDSequence);
}

void objectIdFreedEvent::Trigger(ObjectId *objectIDsToFree, unsigned numObjectIDs, PhysicalPlayerIndex playerIndex)
{
    if(gnetVerifyf(objectIDsToFree && numObjectIDs > 0, "No IDs specified to objectIdFreedEvent"))
    {
        objectIdFreedEvent *netGameEvent = rage_new objectIdFreedEvent(objectIDsToFree, numObjectIDs, playerIndex);
        netInterface::GetEventManager().PrepareEvent(netGameEvent);
    }
}

void objectIdFreedEvent::GetPendingFreedObjectIDs(ObjectId *pendingFreedObjectIds, unsigned &numPendingFreedObjectIDs)
{
    numPendingFreedObjectIDs = 0;

    atDNode<netGameEvent*, datBase> *node = netInterface::GetEventManager().GetEventListHead();

    while (node)
    {
        atDNode<netGameEvent*, datBase> *nextNode = node->GetNext();

        netGameEvent *currentEvent = node->Data;

        if(currentEvent && currentEvent->GetEventType() == OBJECT_ID_FREED_EVENT)
        {
            objectIdFreedEvent *objectIDEvent = static_cast<objectIdFreedEvent *>(currentEvent);

            for(unsigned index = 0; index < objectIDEvent->m_NumObjectIDs; index++)
            {
                if(gnetVerifyf(numPendingFreedObjectIDs < netObjectIDMgr::MAX_TOTAL_OBJECT_IDS, "Maximum number of freed object IDs exceeded!"))
                {
                    pendingFreedObjectIds[numPendingFreedObjectIDs] = objectIDEvent->m_ObjectIDsToFree[index];
                    numPendingFreedObjectIDs++;
                }
            }
        }

        node = nextNode;
    }
}


bool objectIdFreedEvent::IsInScope(const netPlayer &player) const
{
    // send to the machine specified by the peer id
    if (player.GetPhysicalPlayerIndex() == m_PlayerIndex)
    {
        return true;
    }

    return false;
}

template <class Serialiser> void objectIdFreedEvent::SerialiseEvent(datBitBuffer &bitBuffer)
{
    Serialiser serialiser(bitBuffer);

    SERIALISE_UNSIGNED(serialiser, m_NumObjectIDs, SIZEOF_NUM_OBJECT_IDS, "Num Object IDs");

    gnetAssertf(m_NumObjectIDs <= MAX_OBJECT_IDS_TO_FREE, "Received a object id free event with too much data!");
    m_NumObjectIDs = Min(m_NumObjectIDs, MAX_OBJECT_IDS_TO_FREE);

    for(unsigned index = 0; index < m_NumObjectIDs; index++)
    {
        if(gnetVerifyf(index < MAX_OBJECT_IDS_TO_FREE, "Received an object ID freed event with too many free object IDs!"))
        {
            SERIALISE_OBJECTID(serialiser, m_ObjectIDsToFree[index], "Object ID");
        }
    }
}

void objectIdFreedEvent::Prepare(datBitBuffer &bitBuffer, const netPlayer &UNUSED_PARAM(toPlayer))
{
    SerialiseEvent<CSyncDataWriter>(bitBuffer);
}

void objectIdFreedEvent::Handle(datBitBuffer &bitBuffer, const netPlayer &fromPlayer, const netPlayer &ASSERT_ONLY(toPlayer))
{
	ASSERT_ONLY(gnetAssertf(toPlayer.IsMyPlayer(), "Received object ID freed event for a network bot!");)

    SerialiseEvent<CSyncDataReader>(bitBuffer);

    // if we receive an object ID freed event while we are in the process of joining the game
    // we can safely ignore it as the player sending the event will not include these IDs in
    // the informObjectIdsMsg message sent during the join process so they will already be on our list
    if(!netInterface::GetObjectManager().GetObjectIDManager().CanAcceptObjectIDEventsFrom(fromPlayer.GetPhysicalPlayerIndex()))
    {
        LOGGING_ONLY(m_IgnoredIDs = true);
    }
    else
    {
        for(unsigned index = 0; index < m_NumObjectIDs; index++)
        {
            // check the IDs are not already on the free list, this can happen if the event is received after
            // an inform object IDs message when a player is joining the game
            if(!netInterface::GetObjectManager().GetObjectIDManager().ObjectIDIsFree(m_ObjectIDsToFree[index]))
            {
                if(gnetVerifyf(netInterface::GetObjectManager().GetNetworkObject(m_ObjectIDsToFree[index], true) == 0, "Freeing an object ID that is in use!"))
                {
                    netInterface::GetObjectManager().GetObjectIDManager().FreeObjectId(m_ObjectIDsToFree[index]);
                }
            }
        }

        LOGGING_ONLY(m_IgnoredIDs = false);
    }
}

#if ENABLE_NETWORK_LOGGING

void objectIdFreedEvent::WriteEventToLogFile(bool UNUSED_PARAM(wasSent), bool LOGGING_ONLY(bEventLogOnly), datBitBuffer *UNUSED_PARAM(pMessageBuffer)) const
{
	netLogSplitter logSplitter(netInterface::GetEventManager().GetLog(), netInterface::GetMessageLog());
	netLoggingInterface &log = bEventLogOnly ? netInterface::GetEventManager().GetLog() : logSplitter;

    if(m_IgnoredIDs)
    {
        log.WriteDataValue("Ignored Event", "Can't accept IDs from this player");
    }

    for(unsigned index = 0; index < m_NumObjectIDs; index++)
    {
        log.WriteDataValue("Object ID", "%d", m_ObjectIDsToFree[index]);
    }
}

#endif //ENABLE_NETWORK_LOGGING

// ===========================================================================================================
// OBJECT ID REQUEST EVENT
// ===========================================================================================================

objectIdRequestEvent::objectIdRequestEvent() :
netGameEvent(OBJECT_ID_REQUEST_EVENT, true)
, m_NumObjectIDRequested(0)
, m_NumObjectIDReturned(0)
{
}

objectIdRequestEvent::objectIdRequestEvent(unsigned numObjectIDs) :
netGameEvent(OBJECT_ID_REQUEST_EVENT, true)
, m_NumObjectIDRequested(numObjectIDs)
, m_NumObjectIDReturned(0)
{
}

void objectIdRequestEvent::EventHandler(datBitBuffer &bitBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence)
{
    objectIdRequestEvent netGameEvent;
    netInterface::GetEventManager().HandleEvent(&netGameEvent, bitBuffer, fromPlayer, toPlayer, messageSeq, eventID, eventIDSequence);
}

void objectIdRequestEvent::Trigger(unsigned numObjectIDs)
{
    if(gnetVerifyf(numObjectIDs > 0, "No IDs specified to objectIdRequestEvent"))
    {
        objectIdRequestEvent *netGameEvent = rage_new objectIdRequestEvent(numObjectIDs);
        netInterface::GetEventManager().PrepareEvent(netGameEvent);
    }
}

bool objectIdRequestEvent::IsInScope(const netPlayer &player) const
{
    if (player.IsRemote() && !player.IsBot())
    {
        return true;
    }

    return false;
}

template <class Serialiser> void objectIdRequestEvent::SerialiseEvent(datBitBuffer &bitBuffer)
{
    Serialiser serialiser(bitBuffer);

    SERIALISE_UNSIGNED(serialiser, m_NumObjectIDRequested, SIZEOF_NUM_OBJECT_IDS, "Num Object IDs Requested");
}

template <class Serialiser> void objectIdRequestEvent::SerialiseEventReply(datBitBuffer &bitBuffer)
{
    Serialiser serialiser(bitBuffer);

    SERIALISE_UNSIGNED(serialiser, m_NumObjectIDReturned, SIZEOF_NUM_OBJECT_IDS, "Num Object IDs Returned");

    gnetAssertf(m_NumObjectIDReturned <= MAX_OBJECT_IDS_TO_FREE, "Received a object id request event with too much data!");
    m_NumObjectIDReturned = Min(m_NumObjectIDReturned, MAX_OBJECT_IDS_TO_FREE);
	
    for(unsigned index = 0; index < m_NumObjectIDReturned; index++)
    {
        SERIALISE_OBJECTID(serialiser, m_ObjectIDsReturned[index], "Object ID");
    }
}

void objectIdRequestEvent::Prepare(datBitBuffer &bitBuffer, const netPlayer &UNUSED_PARAM(toPlayer))
{
    SerialiseEvent<CSyncDataWriter>(bitBuffer);
}

void objectIdRequestEvent::Handle(datBitBuffer &bitBuffer, const netPlayer &UNUSED_PARAM(fromPlayer), const netPlayer &ASSERT_ONLY(toPlayer))
{
    ASSERT_ONLY(gnetAssertf(toPlayer.IsMyPlayer(), "Received object ID request event for a network bot!");)

    SerialiseEvent<CSyncDataReader>(bitBuffer);
}

bool objectIdRequestEvent::Decide(const netPlayer &fromPlayer, const netPlayer &UNUSED_PARAM(toPlayer))
{
    m_NumObjectIDRequested = Min(m_NumObjectIDRequested, MAX_OBJECT_IDS_TO_FREE);
    m_NumObjectIDReturned  = 0;
    netInterface::GetObjectManager().GetObjectIDManager().RequestObjectIDs(fromPlayer, m_NumObjectIDRequested, m_ObjectIDsReturned, m_NumObjectIDReturned);

    return true;
}

void objectIdRequestEvent::PrepareReply(datBitBuffer &messageBuffer, const netPlayer &UNUSED_PARAM(toPlayer))
{
    SerialiseEventReply<CSyncDataWriter>(messageBuffer);
}

void objectIdRequestEvent::HandleReply(datBitBuffer &messageBuffer, const netPlayer &UNUSED_PARAM(fromPlayer), const netPlayer &UNUSED_PARAM(toPlayer))
{
    SerialiseEventReply<CSyncDataReader>(messageBuffer);

    for(unsigned index = 0; index < m_NumObjectIDReturned; index++)
    {
        // check the IDs are not already on the free list, this can happen if the event is received after
        // an inform object IDs message when a player is joining the game
        if(!netInterface::GetObjectManager().GetObjectIDManager().ObjectIDIsFree(m_ObjectIDsReturned[index]))
        {
            netInterface::GetObjectManager().GetObjectIDManager().FreeObjectId(m_ObjectIDsReturned[index]);
        }
    }
}

#if ENABLE_NETWORK_LOGGING

void objectIdRequestEvent::WriteEventToLogFile(bool UNUSED_PARAM(wasSent), bool LOGGING_ONLY(bEventLogOnly), datBitBuffer *UNUSED_PARAM(pMessageBuffer)) const
{
	netLogSplitter logSplitter(netInterface::GetEventManager().GetLog(), netInterface::GetMessageLog());
	netLoggingInterface &log = bEventLogOnly ? netInterface::GetEventManager().GetLog() : logSplitter;

    log.WriteDataValue("Num IDs Requested", "%d", m_NumObjectIDRequested);
}

void objectIdRequestEvent::WriteReplyToLogFile(bool UNUSED_PARAM(wasSent)) const
{
	netLogSplitter logSplitter(netInterface::GetEventManager().GetLog(), netInterface::GetMessageLog());

    for(unsigned index = 0; index < m_NumObjectIDReturned; index++)
    {
        logSplitter.WriteDataValue("Object ID", "%d", m_ObjectIDsReturned[index]);
    }
}

#endif //ENABLE_NETWORK_LOGGING

// ===========================================================================================================
// ARRAY DATA VERIFY EVENT
// ===========================================================================================================

arrayDataVerifyEvent::arrayDataVerifyEvent()
: netGameEvent( ARRAY_DATA_VERIFY_EVENT, false, false )
, m_ArrayType(0)
, m_ArrayChecksum(0)
, m_ArrayChecksumDiffers(false)
, m_ArrayDataVerified(false)
, m_Ignored(false)
{
}

arrayDataVerifyEvent::arrayDataVerifyEvent(NetworkArrayHandlerType arrayType, u32 arrayChecksum)
: netGameEvent( ARRAY_DATA_VERIFY_EVENT, false, false )
, m_ArrayType(arrayType)
, m_ArrayChecksum(arrayChecksum)
, m_ArrayChecksumDiffers(false)
, m_ArrayDataVerified(false)
, m_Ignored(false)
{
}

void arrayDataVerifyEvent::EventHandler(datBitBuffer &msgBuffer, const netPlayer &fromPlayer, const netPlayer &toPlayer, const netSequence messageSeq, const EventId eventID, const unsigned eventIDSequence)
{
	arrayDataVerifyEvent networkEvent;
	netInterface::GetEventManager().HandleEvent(&networkEvent, msgBuffer, fromPlayer, toPlayer, messageSeq, eventID, eventIDSequence);
}

void arrayDataVerifyEvent::Trigger(NetworkArrayHandlerType arrayType, u32 arrayChecksum)
{
	netInterface::GetEventManager().CheckForSpaceInPool();
	arrayDataVerifyEvent *pEvent = rage_new arrayDataVerifyEvent(arrayType, arrayChecksum);
	netInterface::GetEventManager().PrepareEvent(pEvent);
}

bool arrayDataVerifyEvent::IsInScope( const netPlayer &player ) const
{
	bool inScope = false;

	const netArrayHandlerBase* pArray = GetArrayHandler();

	if (pArray && 
		pArray->GetPlayerArbitratorOfArray() && 
		!pArray->GetPlayerArbitratorOfArray()->IsLocal() && 
		pArray->HasArrayData())
	{
		inScope = (pArray->GetPlayerArbitratorOfArray() == &player);
	}

	return inScope;
}

const netArrayHandlerBase* arrayDataVerifyEvent::GetArrayHandler() const
{
	const netArrayHandlerBase* pArray = netInterface::GetArrayManager().GetArrayHandler(m_ArrayType);

	return pArray;
}

netArrayHandlerBase* arrayDataVerifyEvent::GetArrayHandler()
{
	netArrayHandlerBase* pArray = netInterface::GetArrayManager().GetArrayHandler(m_ArrayType);

	return pArray;
}

template <class Serialiser> void arrayDataVerifyEvent::SerialiseEvent(datBitBuffer &messageBuffer)
{
	Serialiser serialiser(messageBuffer);

	SERIALISE_UNSIGNED(serialiser, m_ArrayType, SIZEOF_ARRAY_TYPE, "Array type");
	SERIALISE_UNSIGNED(serialiser, m_ArrayChecksum, 32, "Array checksum");
}

void arrayDataVerifyEvent::Prepare ( datBitBuffer &messageBuffer, const netPlayer &UNUSED_PARAM(toPlayer) )
{
	SerialiseEvent<CSyncDataWriter>(messageBuffer);
}

void arrayDataVerifyEvent::Handle ( datBitBuffer &messageBuffer, const netPlayer &UNUSED_PARAM(fromPlayer), const netPlayer &UNUSED_PARAM(toPlayer))
{
	SerialiseEvent<CSyncDataReader>(messageBuffer);
}

bool arrayDataVerifyEvent::Decide(const netPlayer &fromPlayer, const netPlayer &UNUSED_PARAM(toPlayer))
{
	netArrayHandlerBase* pArray = GetArrayHandler();

	if (pArray && pArray->IsArrayLocallyArbitrated())
	{
		if (pArray->GetHandlerType() == HOST_BROADCAST_DATA_ARRAY_HANDLER)
		{
			const scriptHandler* pHandler = static_cast<netScriptBroadcastDataHandlerBase*>(pArray)->GetScriptHandler();

			// we cannot process this event if the host is migrating, so it is ignored. The sender will keep resending until he finds a valid host
			if (gnetVerify(pHandler && pHandler->GetNetworkComponent()) && pHandler->GetNetworkComponent()->IsHostMigrating())
			{
				m_Ignored = true;
				return false;
			}
		}

		m_ArrayChecksumDiffers = pArray->VerifyChecksum(fromPlayer, m_ArrayChecksum);
		m_ArrayDataVerified = true;
	}

	return true;
}

#if ENABLE_NETWORK_LOGGING

void arrayDataVerifyEvent::WriteEventToLogFile(bool LOGGING_ONLY(wasSent), bool LOGGING_ONLY(eventLogOnly), datBitBuffer *UNUSED_PARAM(messageBuffer)) const
{
	netLoggingInterface &log = eventLogOnly ? netInterface::GetEventManager().GetLog() : netInterface::GetEventManager().GetLogSplitter();

	const netArrayHandlerBase* pArray = GetArrayHandler();

	log.WriteDataValue("Array", "%s", pArray ? pArray->GetHandlerName() : "??");
	log.WriteDataValue("Array Checksum", "%d", m_ArrayChecksum);

	if (!wasSent)
	{
		if (m_Ignored)
		{
			log.WriteDataValue("Ignored", "Host migration in progress");
		}
		else if (m_ArrayDataVerified)
		{
			log.WriteDataValue("Array Checksum Differs", "%s", m_ArrayChecksumDiffers ? "true" : "false");
		}
		else
		{
			log.WriteDataValue("Array Data Verified", "false");
		}
	}
}

#endif //ENABLE_NETWORK_LOGGING

namespace netEventTypes
{
    void RegisterNetGameEvents()
    {
		netInterface::GetEventManager().RegisterNetworkEvent(OBJECT_ID_FREED_EVENT,				objectIdFreedEvent::EventHandler,			"OBJECT_ID_FREED_EVENT");
        netInterface::GetEventManager().RegisterNetworkEvent(OBJECT_ID_REQUEST_EVENT,			objectIdRequestEvent::EventHandler,			"OBJECT_ID_REQUEST_EVENT");
		netInterface::GetEventManager().RegisterNetworkEvent(ARRAY_DATA_VERIFY_EVENT,			arrayDataVerifyEvent::EventHandler,			"ARRAY_DATA_VERIFY_EVENT");
    }
} // namespace netEventTypes

} // namespace rage
