//
// filename:	NetworkSerialisers.h
// description:	
//

#ifndef INC_NETWORKSERIALISERS_H_
#define INC_NETWORKSERIALISERS_H_

// --- Include Files ------------------------------------------------------------
#include "data/bitbuffer.h"

#include "fwnet/nettypes.h"
#include "fwnet/netlog.h"
#include "fwnet/netlogginginterface.h"

#include "vector/Matrix34.h"
#include "vector/Quaternion.h"


//-------------------------------------------------------------------------------
//Dead strip out name
#if ENABLE_NETWORK_LOGGING
#define SERIALISE_BOOL(serialiser,value,...)									serialiser.SerialiseBool(value, ##__VA_ARGS__);
#define SERIALISE_INTEGER(serialiser,value,numbits,...)							serialiser.SerialiseInteger(value, numbits, ##__VA_ARGS__);
#define SERIALISE_UNSIGNED(serialiser,value,numbits,...)						serialiser.SerialiseUnsigned(value, numbits, ##__VA_ARGS__);
#define SERIALISE_BITFIELD(serialiser,value,numbits,...)						serialiser.SerialiseBitField(value, numbits, ##__VA_ARGS__);
#define SERIALISE_PACKED_FLOAT(serialiser,value,maxvalue,numbits,...)			serialiser.SerialisePackedFloat(value, maxvalue, numbits, ##__VA_ARGS__);
#define SERIALISE_PACKED_UNSIGNED_FLOAT(serialiser,value,maxvalue,numbits,...)	serialiser.SerialisePackedUnsignedFloat(value, maxvalue, numbits, ##__VA_ARGS__);
#define SERIALISE_OBJECTID(serialiser,objectid,...)								serialiser.SerialiseObjectID(objectid, ##__VA_ARGS__);
#define SERIALISE_POSITION(serialiser,position,...)								serialiser.SerialisePosition(position, ##__VA_ARGS__);
#define SERIALISE_POSITION_SIZE(serialiser,position,name,sizeofposition)		serialiser.SerialisePosition(position, name, sizeofposition);
#define SERIALISE_ORIENTATION(serialiser,orientation,...)						serialiser.SerialiseOrientation(orientation, ##__VA_ARGS__);
#define SERIALISE_VECTOR(serialiser,vector,maxlen,numbits,...)					serialiser.SerialiseVector(vector, maxlen, numbits, ##__VA_ARGS__);
#define SERIALISE_QUATERNION(serialiser,quaternion,numbits,...)					serialiser.SerialiseQuaternion(quaternion, numbits, ##__VA_ARGS__);
#define SERIALISE_DATABLOCK(serialiser,datablock,numbits,...)					serialiser.SerialiseDataBlock(datablock, numbits, ##__VA_ARGS__);
#define SERIALISE_STRING(serialiser,string,maxchars,...)						serialiser.SerialiseString(string, maxchars, ##__VA_ARGS__);
#else
#define SERIALISE_BOOL(serialiser,value,...)									serialiser.SerialiseBool(value);
#define SERIALISE_INTEGER(serialiser,value,numbits,...)							serialiser.SerialiseInteger(value, numbits);
#define SERIALISE_UNSIGNED(serialiser,value,numbits,...)						serialiser.SerialiseUnsigned(value, numbits);
#define SERIALISE_BITFIELD(serialiser,value,numbits,...)						serialiser.SerialiseBitField(value, numbits);
#define SERIALISE_PACKED_FLOAT(serialiser,value,maxvalue,numbits,...)			serialiser.SerialisePackedFloat(value, maxvalue, numbits);
#define SERIALISE_PACKED_UNSIGNED_FLOAT(serialiser,value,maxvalue,numbits,...)	serialiser.SerialisePackedUnsignedFloat(value, maxvalue, numbits);
#define SERIALISE_OBJECTID(serialiser,objectid,...)								serialiser.SerialiseObjectID(objectid);
#define SERIALISE_POSITION(serialiser,position,...)								serialiser.SerialisePosition(position);
#define SERIALISE_POSITION_SIZE(serialiser,position,name,sizeofposition)		serialiser.SerialisePosition(position, "", sizeofposition);
#define SERIALISE_ORIENTATION(serialiser,orientation,...)						serialiser.SerialiseOrientation(orientation);
#define SERIALISE_VECTOR(serialiser,vector,maxlen,numbits,...)					serialiser.SerialiseVector(vector, maxlen, numbits);
#define SERIALISE_QUATERNION(serialiser,quaternion,numbits,...)					serialiser.SerialiseQuaternion(quaternion,numbits);
#define SERIALISE_DATABLOCK(serialiser,datablock,numbits,...)					serialiser.SerialiseDataBlock(datablock, numbits);
#define SERIALISE_STRING(serialiser,string,maxchars,...)						serialiser.SerialiseString(string, maxchars);
#endif
//-------------------------------------------------------------------------------

namespace rage
{
static const unsigned int SIZEOF_POSITION           = 19;
static const unsigned int SIZEOF_ORIENTATION		= 9;
static const unsigned int SIZEOF_ORIENTATION_POINT  = 6;
static const int MAX_ORIENTATION           = (1<<(SIZEOF_ORIENTATION-1))-1;
static const unsigned int SIZEOF_VELOCITY          =   12;
static const unsigned int SIZEOF_VELOCITY_POINT    =   4;
static const unsigned int SIZEOF_ANGVELOCITY       =   10;
static const unsigned int SIZEOF_ANGVELOCITY_POINT =   5;

class netSerialiserUtils
{
public:

    typedef void (*fnAdjustWorldExtents)(Vector3& min, Vector3& max);
    typedef void (*fnGetRealMapCoords)(const Vector3 &adjustedPos, Vector3 &realPos);
    typedef void (*fnAdjustMapExtents)(const Vector3 &realPos, Vector3 &adjustedPos);

    static void SetWorldExtentsCallbacks(fnAdjustWorldExtents adjustWorldCallback,
                                         fnGetRealMapCoords   getRealMapCallback,
                                         fnAdjustMapExtents   adjustMapExtentsCallback) 
    {
        m_AdjustWorldExtentsCallback = adjustWorldCallback; 
        m_GetRealMapCoordsCallback   = getRealMapCallback;
        m_AdjustMapExtentsCallback   = adjustMapExtentsCallback;
    }

    static void GetAdjustedWorldExtents(Vector3& min, Vector3& max);
    static void GetRealMapCoords(const Vector3 &adjustedPos, Vector3 &realPos);
    static void GetAdjustedMapCoords(const Vector3 &realPos, Vector3 &adjustedPos);

    // pack a float into a fixed point signed short
    // point is the bit representing the fixed point
    inline static s16 PackFixedPoint16(float f, u8 point)
    {
	    Assert(point > 0 && point <= 16);
	    Assert(Abs(f) < (float)(1<<(16-point-1)));
	    return (s16) (f * (1<<point));
    }

    // unpack a fixed point signed short into a float
    // point is the bit representing the fixed point
    inline static float UnPackFixedPoint16(s16 packed, u8 point)
    {
	    Assert(point > 0 && point <= 16);
	    return ((float) packed / (float) (1<<point));
    }

private:

    static fnAdjustWorldExtents m_AdjustWorldExtentsCallback;
    static fnGetRealMapCoords   m_GetRealMapCoordsCallback;
    static fnAdjustMapExtents   m_AdjustMapExtentsCallback;
};

//============================================================================================
// CSyncDataBase
//============================================================================================
class CSyncDataBase
{
protected:
	CSyncDataBase(netLoggingInterface* pLog) : m_log(pLog), m_type(NONE) {}

	// EJ: Defaulting this to a u32 since the data doesn't need to change and size doesn't matter (this time at least)
	void SerialiseBits(const u32 value, const int numBits, const char* name /*= NULL*/)
	{
		ASSERT_ONLY(u64 maxVal = ((u64)1u << (u64)numBits) - 1);
		Assertf((u64)value <= maxVal, "%s is trying to serialise an int (%u) exceeding its permitted maximum value (%" I64FMT "u)", name ? name : "Unnamed", value, maxVal);

		if(m_log && name)
		{
			const int BUFFER_SIZE = 256;
			char buffer[BUFFER_SIZE] = "";

			for (int index = 0; index < numBits; index++)
			{
				if((value & (1 << index)) != 0)
				{
					buffer[numBits - index - 1] = '1';
				}
				else
				{
					buffer[numBits - index - 1] = '0';
				}
			}

			m_log->WriteDataValue(name, buffer);
		}
	}

public:
	virtual ~CSyncDataBase() {}

	// Bit
	virtual void SerialiseBitField(s8& value, const int numBits, const char* name = NULL) {SerialiseBits(value, numBits, name);}
	virtual void SerialiseBitField(s16& value, const int numBits, const char* name = NULL) {SerialiseBits(value, numBits, name);}
	virtual void SerialiseBitField(s32& value, const int numBits, const char* name = NULL) {SerialiseBits(value, numBits, name);}

	virtual void SerialiseBitField(u8& value, const int numBits, const char* name = NULL) {SerialiseBits(value, numBits, name);}
	virtual void SerialiseBitField(u16& value, const int numBits, const char* name = NULL) {SerialiseBits(value, numBits, name);}
	virtual void SerialiseBitField(u32& value, const int numBits, const char* name = NULL) {SerialiseBits(value, numBits, name);}

	// Bool
	virtual void SerialiseBool(bool& value, const char* name = NULL);

	// Integer
	virtual void SerialiseInteger(s8& value, const int numBits, const char* name = NULL);
	virtual void SerialiseInteger(s16& value, const int numBits, const char* name = NULL);
	virtual void SerialiseInteger(s32& value, const int numBits, const char* name = NULL);
	virtual void SerialiseInteger(s64& value, const int numBits, const char* name = NULL);

	void SerialiseInteger(u8& value, const int numBits, const char* name = NULL) {SerialiseInteger(reinterpret_cast<s8&>(value), numBits, name);}
	void SerialiseInteger(u16& value, const int numBits, const char* name = NULL) {SerialiseInteger(reinterpret_cast<s16&>(value), numBits, name);}
	void SerialiseInteger(u32& value, const int numBits, const char* name = NULL) {SerialiseInteger(reinterpret_cast<s32&>(value), numBits, name);}
	void SerialiseInteger(u64& value, const int numBits, const char* name = NULL) {SerialiseInteger(reinterpret_cast<s64&>(value), numBits, name);}

	// Unsigned
	virtual void SerialiseUnsigned(u8& value, const int numBits, const char* name = NULL);
	virtual void SerialiseUnsigned(u16& value, const int numBits, const char* name = NULL);
	virtual void SerialiseUnsigned(u32& value, const int numBits, const char* name = NULL);
    virtual void SerialiseUnsigned(u64& value, const int numBits, const char* name = NULL);

	void SerialiseUnsigned(s8& value, const int numBits, const char* name = NULL) {SerialiseUnsigned(reinterpret_cast<u8&>(value), numBits, name);}
	void SerialiseUnsigned(s16& value, const int numBits, const char* name = NULL) {SerialiseUnsigned(reinterpret_cast<u16&>(value), numBits, name);}
	void SerialiseUnsigned(s32& value, const int numBits, const char* name = NULL) {SerialiseUnsigned(reinterpret_cast<u32&>(value), numBits, name);}
    void SerialiseUnsigned(s64& value, const int numBits, const char* name = NULL) {SerialiseUnsigned(reinterpret_cast<u64&>(value), numBits, name);}

	// Float
	virtual void SerialisePackedFloat(float& value, const float maxValue, const int numBits, const char* name = NULL);
	virtual void SerialisePackedUnsignedFloat(float& value, const float maxValue, const int numBits, const char* name = NULL);

	// Misc
	virtual void SerialiseObjectID(ObjectId& objectID, const char* name = NULL);
	virtual void SerialisePosition(Vector3& position, const char* name = NULL, u32 sizeOfPosition = SIZEOF_POSITION);
	virtual void SerialiseOrientation(Matrix34& orientation, const char* name = NULL);
	virtual void SerialiseVector(Vector3& vec, const float maxLen, const int numBits, const char* name = NULL);
	virtual void SerialiseQuaternion(Quaternion& quat, const int numBits, const char* name = NULL);
	virtual void SerialiseDataBlock(u8* dataBlock, const int numBits, const char* name = NULL);
	virtual void SerialiseString(char* string, const u32 maxChars, const char* name = NULL);

	// Accessors	
	netLoggingInterface* GetLog() { return m_log; } 
	u8 GetType() const {return m_type;}
	virtual bool GetIsMaximumSizeSerialiser();
	virtual u32 GetSize();	

	// Mutators
	void SetLog(netLoggingInterface *log) { m_log = log; }

public:
	enum {NONE, SYNC_DATA_READ, SYNC_DATA_WRITE, SYNC_DATA_CALC, SYNC_DATA_LOG};

protected:
	u8 m_type;

private:
    netLoggingInterface *m_log;
};

//============================================================================================
// CSyncDataReader
//============================================================================================
class CSyncDataReader : public CSyncDataBase
{
public:	
	using CSyncDataBase::SerialiseInteger;
	using CSyncDataBase::SerialiseUnsigned;

    CSyncDataReader(datBitBuffer &messageBuffer, netLoggingInterface* pLog = NULL) : CSyncDataBase(pLog), m_messageBuffer(messageBuffer)
	{
		m_type = SYNC_DATA_READ;
	}

	datBitBuffer& GetBitBuffer() { return m_messageBuffer; }

	// Bool
	virtual void SerialiseBool(bool& value, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.ReadBool(value);

		LOGGING_ONLY(CSyncDataBase::SerialiseBool(value, name);)
    }

	// Integer
	virtual void SerialiseInteger(s8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	virtual void SerialiseInteger(s16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	virtual void SerialiseInteger(s32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	virtual void SerialiseInteger(s64& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	// Unsigned
	virtual void SerialiseUnsigned(u8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
	}

	virtual void SerialiseUnsigned(u16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
	}

	virtual void SerialiseUnsigned(u32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
	}

    virtual void SerialiseUnsigned(u64& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.ReadUns(value, numBits);

        LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
    }

	// Bits
	virtual void SerialiseBitField(s8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(s16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(s32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(u8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(u16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.ReadUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(u32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.ReadUns(value, numBits);

        LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
    }

	// Float
    virtual void SerialisePackedFloat(float& value, const float maxValue, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        int v;
        m_messageBuffer.ReadInt(v, numBits);
        value = ((float) v / (float) ((1<<(numBits-1))-1)) * maxValue;

        LOGGING_ONLY(CSyncDataBase::SerialisePackedFloat(value, maxValue, numBits, name);)
    }

    virtual void SerialisePackedUnsignedFloat(float& value, const float maxValue, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        unsigned v;
        m_messageBuffer.ReadUns(v, numBits);
        value = ((float) v / (float) ((1<<(numBits))-1)) * maxValue;

        LOGGING_ONLY(CSyncDataBase::SerialisePackedUnsignedFloat(value, maxValue, numBits, name);)
    }

	// Misc
    virtual void SerialiseObjectID(ObjectId &objectID, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.ReadUns(objectID, SIZEOF_OBJECTID);

        LOGGING_ONLY(CSyncDataBase::SerialiseObjectID(objectID, name);)
    }

    virtual void SerialisePosition(Vector3 &position, const char* LOGGING_ONLY(name) = NULL, u32 sizeOfPosition = SIZEOF_POSITION)
    {
        Vector3 minExtent, maxExtent;
	    netSerialiserUtils::GetAdjustedWorldExtents(minExtent, maxExtent);

	    Vector3 worldCoordsAdjusted;

	    SerialisePackedFloat(worldCoordsAdjusted.x, maxExtent.x, sizeOfPosition);
	    SerialisePackedFloat(worldCoordsAdjusted.y, maxExtent.y, sizeOfPosition);
	    SerialisePackedUnsignedFloat(worldCoordsAdjusted.z, maxExtent.z, sizeOfPosition);

	    netSerialiserUtils::GetRealMapCoords(worldCoordsAdjusted, position);

        LOGGING_ONLY(CSyncDataBase::SerialisePosition(position, name);)
    }

    virtual void SerialiseOrientation(Matrix34 &orientation, const char* LOGGING_ONLY(name) = NULL)
    {
        s32 x, y, z;
        Vector3 eulers;

        // read the packed euler angles, unpack them and convert them to a matrix
        SerialiseInteger(x, SIZEOF_ORIENTATION);
        SerialiseInteger(y, SIZEOF_ORIENTATION);
        SerialiseInteger(z, SIZEOF_ORIENTATION);
        eulers.x = netSerialiserUtils::UnPackFixedPoint16((s16)x, SIZEOF_ORIENTATION_POINT);
        eulers.y = netSerialiserUtils::UnPackFixedPoint16((s16)y, SIZEOF_ORIENTATION_POINT);
        eulers.z = netSerialiserUtils::UnPackFixedPoint16((s16)z, SIZEOF_ORIENTATION_POINT);

        orientation.FromEulersXYZ(eulers);

        Assert(orientation.IsOrthonormal());

        LOGGING_ONLY(CSyncDataBase::SerialiseOrientation(orientation, name);)
    }

	virtual void SerialiseVector(Vector3 &vec, const float maxLen, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		SerialisePackedFloat(vec.x, maxLen, numBits);
		SerialisePackedFloat(vec.y, maxLen, numBits);
		SerialisePackedFloat(vec.z, maxLen, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseVector(vec, maxLen, numBits, name);)
	}

    virtual void SerialiseQuaternion(Quaternion &quat, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        SerialisePackedFloat(quat.x, 1.0f, numBits);
		SerialisePackedFloat(quat.y, 1.0f, numBits);
		SerialisePackedFloat(quat.z, 1.0f, numBits);
        SerialisePackedFloat(quat.w, 1.0f, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseQuaternion(quat, numBits, name);)
    }

	virtual void SerialiseDataBlock(u8 *dataBlock, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.ReadBits(dataBlock, numBits, 0);

		LOGGING_ONLY(CSyncDataBase::SerialiseDataBlock(dataBlock, numBits, name);)
    }

    virtual void SerialiseString(char *string, const u32 maxChars, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.ReadStr(string, maxChars);

        LOGGING_ONLY(CSyncDataBase::SerialiseString(string, maxChars, name);)
    }

private:

    CSyncDataReader &operator= (const CSyncDataReader &);

    datBitBuffer &m_messageBuffer;
};

//============================================================================================
// CSyncDataWriter
//============================================================================================
class CSyncDataWriter : public CSyncDataBase
{
public:
	using CSyncDataBase::SerialiseInteger;
	using CSyncDataBase::SerialiseUnsigned;

	CSyncDataWriter(datBitBuffer &messageBuffer, netLoggingInterface* pLog = NULL) : CSyncDataBase(pLog), m_messageBuffer(messageBuffer)
	{
		m_type = SYNC_DATA_WRITE;
	}

	datBitBuffer &GetBitBuffer() { return m_messageBuffer; }

	// Bool
	virtual void SerialiseBool(bool& value, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.WriteBool(value);

        LOGGING_ONLY(CSyncDataBase::SerialiseBool(value, name);)
    }

	// Integer
	virtual void SerialiseInteger(s8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	virtual void SerialiseInteger(s16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	virtual void SerialiseInteger(s32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
		m_messageBuffer.WriteInt(value, numBits);

        LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
    }

	virtual void SerialiseInteger(s64& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseInteger(value, numBits, name);)
	}

	// Unsigned
	virtual void SerialiseUnsigned(u8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
	}

	virtual void SerialiseUnsigned(u16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
	}

	virtual void SerialiseUnsigned(u32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.WriteUns(value, numBits);

        LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
    }

    virtual void SerialiseUnsigned(u64& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.WriteUns(value, numBits);

        LOGGING_ONLY(CSyncDataBase::SerialiseUnsigned(value, numBits, name);)
    }

	// Bits
	virtual void SerialiseBitField(s8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(s16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(s32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteInt(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(u8& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(u16& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	virtual void SerialiseBitField(u32& value, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		m_messageBuffer.WriteUns(value, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseBitField(value, numBits, name);)
	}

	// Float
    virtual void SerialisePackedFloat(float& value, const float maxValue, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
#if ENABLE_NETWORK_LOGGING
		Assertf(value >= -maxValue && value <= maxValue, "%s is trying to serialise a float (%f) exceeding its permitted range (%f - %f)", name ? name : "Unnamed", value, -maxValue, maxValue);
#else
		Assertf(value >= -maxValue && value <= maxValue, "%s is trying to serialise a float (%f) exceeding its permitted range (%f - %f)", "Unnamed", value, -maxValue, maxValue);
#endif

        const int maxIntegerValue = ((1<<(numBits-1))-1);

        float floatMappedToInt = (value / maxValue) * (float)maxIntegerValue;
        s32   floatPackedAsInt = (s32)floatMappedToInt;

        // floating point inaccuracy can leave the packed value outside the allowable range
        if(floatPackedAsInt < -maxIntegerValue)
        {
            floatPackedAsInt = -maxIntegerValue;
        }

        if(floatPackedAsInt > maxIntegerValue)
        {
            floatPackedAsInt = maxIntegerValue;
        }

        m_messageBuffer.WriteInt(floatPackedAsInt,  numBits);

        LOGGING_ONLY(CSyncDataBase::SerialisePackedFloat(value, maxValue, numBits, name);)
    }

    virtual void SerialisePackedUnsignedFloat(float& value, const float maxValue, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
#if ENABLE_NETWORK_LOGGING
		Assertf(value >= 0.0f && value <= (maxValue + SMALL_FLOAT), "%s is trying to serialise an unsigned float (%f) exceeding its permitted maximum value (%f)", name ? name : "Unnamed", value, maxValue);
#else
		Assertf(value >= 0.0f && value <= (maxValue + SMALL_FLOAT), "%s is trying to serialise an unsigned float (%f) exceeding its permitted maximum value (%f)", "Unnamed", value, maxValue);
#endif

        const u32 maxIntegerValue = ((1<<numBits)-1);

        float floatMappedToInt = (value / maxValue) * (float)maxIntegerValue;
        u32   floatPackedAsInt = (u32)floatMappedToInt;

        // floating point inaccuracy can leave the packed value outside the allowable range
        if(floatPackedAsInt > maxIntegerValue)
        {
            floatPackedAsInt = maxIntegerValue;
        }

        m_messageBuffer.WriteUns(floatPackedAsInt,  numBits);

        LOGGING_ONLY(CSyncDataBase::SerialisePackedUnsignedFloat(value, maxValue, numBits, name);)
    }

	// Misc
    virtual void SerialiseObjectID(ObjectId& objectID, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.WriteUns(objectID, SIZEOF_OBJECTID);

        LOGGING_ONLY(CSyncDataBase::SerialiseObjectID(objectID, name);)
    }

    virtual void SerialisePosition(Vector3& position, const char* LOGGING_ONLY(name) = NULL, u32 sizeOfPosition = SIZEOF_POSITION)
    {
        Vector3 minExtent, maxExtent;
	    netSerialiserUtils::GetAdjustedWorldExtents(minExtent, maxExtent);

	    Vector3 worldCoordsAdjusted;
    	
	    netSerialiserUtils::GetAdjustedMapCoords(position, worldCoordsAdjusted);

#if __ASSERT
		if (worldCoordsAdjusted.x < minExtent.x || worldCoordsAdjusted.x > maxExtent.x ||
			worldCoordsAdjusted.y < minExtent.y || worldCoordsAdjusted.y > maxExtent.y ||
			worldCoordsAdjusted.z < minExtent.z || worldCoordsAdjusted.z > maxExtent.z)
		{
			Assertf(0, "Trying to serialise an invalid position %f, %f, %f", position.x, position.y, position.z);
		}
#endif

 	    SerialisePackedFloat(worldCoordsAdjusted.x, maxExtent.x, sizeOfPosition);
	    SerialisePackedFloat(worldCoordsAdjusted.y, maxExtent.y, sizeOfPosition);
	    SerialisePackedUnsignedFloat(worldCoordsAdjusted.z, maxExtent.z, sizeOfPosition);

        LOGGING_ONLY(CSyncDataBase::SerialisePosition(position, name);)
    }

    virtual void SerialiseOrientation(Matrix34& orientation, const char* LOGGING_ONLY(name) = NULL)
    {
        Assert(orientation.IsOrthonormal());

        // pack orientation into euler angles
        Vector3 eulers;
        orientation.ToEulersXYZ(eulers);

        s32 x = netSerialiserUtils::PackFixedPoint16(eulers.x, SIZEOF_ORIENTATION_POINT);
        s32 y = netSerialiserUtils::PackFixedPoint16(eulers.y, SIZEOF_ORIENTATION_POINT);
        s32 z = netSerialiserUtils::PackFixedPoint16(eulers.z, SIZEOF_ORIENTATION_POINT);

        Assert(x >= -MAX_ORIENTATION && x <= MAX_ORIENTATION);
        Assert(y >= -MAX_ORIENTATION && y <= MAX_ORIENTATION);
        Assert(z >= -MAX_ORIENTATION && z <= MAX_ORIENTATION);

        m_messageBuffer.WriteInt(x, SIZEOF_ORIENTATION);
        m_messageBuffer.WriteInt(y, SIZEOF_ORIENTATION);
        m_messageBuffer.WriteInt(z, SIZEOF_ORIENTATION);

        LOGGING_ONLY(CSyncDataBase::SerialiseOrientation(orientation, name);)
    }

	virtual void SerialiseVector(Vector3& vec, const float maxLen, const int numBits, const char* LOGGING_ONLY(name) = NULL)
	{
		SerialisePackedFloat(vec.x, maxLen, numBits);
		SerialisePackedFloat(vec.y, maxLen, numBits);
		SerialisePackedFloat(vec.z, maxLen, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseVector(vec, maxLen, numBits, name);)
	}

    virtual void SerialiseQuaternion(Quaternion& quat, const int numBits, const char* LOGGING_ONLY(name) = NULL)
    {
        SerialisePackedFloat(quat.x, 1.0f, numBits);
		SerialisePackedFloat(quat.y, 1.0f, numBits);
		SerialisePackedFloat(quat.z, 1.0f, numBits);
        SerialisePackedFloat(quat.w, 1.0f, numBits);

		LOGGING_ONLY(CSyncDataBase::SerialiseQuaternion(quat, numBits, name);)
    }

	virtual void SerialiseDataBlock(u8 *dataBlock, const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_messageBuffer.WriteBits(dataBlock, numBits, 0);
    }

    virtual void SerialiseString(char* string, const u32 maxChars, const char* LOGGING_ONLY(name) = NULL)
    {
        m_messageBuffer.WriteStr(string, maxChars);

        LOGGING_ONLY(CSyncDataBase::SerialiseString(string, maxChars, name);)
    }

private:

    CSyncDataWriter &operator= (const CSyncDataWriter &);

    datBitBuffer &m_messageBuffer;
};

//============================================================================================
// CSyncDataSizeCalculator
//============================================================================================
class CSyncDataSizeCalculator : public CSyncDataBase
{
public:
	using CSyncDataBase::SerialiseInteger;
	using CSyncDataBase::SerialiseUnsigned;

	CSyncDataSizeCalculator(netLoggingInterface* pLog = NULL) : CSyncDataBase(pLog), m_currentSize(0), m_calcMaxSize(true)
	{
		m_type = SYNC_DATA_CALC;
	}

	void SetIsMaximumSizeSerialiser(bool b) { m_calcMaxSize = b; }
    
	virtual u32 GetSize() { return m_currentSize; }
	
	virtual bool GetIsMaximumSizeSerialiser() { return 0 != m_calcMaxSize; }

	// Bool
    virtual void SerialiseBool(bool& UNUSED_PARAM(value), const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize++;
    }

	// Integer
	virtual void SerialiseInteger(s8& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseInteger(s16& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseInteger(s32& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += numBits;
    }

	virtual void SerialiseInteger(s64& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	// Unsigned
	virtual void SerialiseUnsigned(u8& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseUnsigned(u16& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseUnsigned(u32& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += numBits;
    }

    virtual void SerialiseUnsigned(u64& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += numBits;
    }

	// Bits
	virtual void SerialiseBitField(s8& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseBitField(s16& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseBitField(s32& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseBitField(u8& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseBitField(u16& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	virtual void SerialiseBitField(u32& UNUSED_PARAM(value), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += numBits;
	}

	// Float
    virtual void SerialisePackedFloat(float& UNUSED_PARAM(value), const float UNUSED_PARAM(maxValue), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += numBits;
    }

    virtual void SerialisePackedUnsignedFloat(float& UNUSED_PARAM(value), const float UNUSED_PARAM(maxValue), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += numBits;
    }

	// Misc
    virtual void SerialiseObjectID(ObjectId& UNUSED_PARAM(objectID), const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += SIZEOF_OBJECTID;
    }

    virtual void SerialisePosition(Vector3& UNUSED_PARAM(position), const char* UNUSED_PARAM(name) = NULL, u32 sizeOfPosition = SIZEOF_POSITION)
    {
        m_currentSize += (sizeOfPosition * 3);
    }

    virtual void SerialiseOrientation(Matrix34& UNUSED_PARAM(orientation), const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += (SIZEOF_ORIENTATION*3);
    }

	virtual void SerialiseVector(Vector3& UNUSED_PARAM(vec), const float UNUSED_PARAM(maxLen), const int numBits, const char* UNUSED_PARAM(name) = NULL)
	{
		m_currentSize += (numBits*3);
	}

    virtual void SerialiseQuaternion(Quaternion& UNUSED_PARAM(quat), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += (numBits*4);
    }

	virtual void SerialiseDataBlock(u8* UNUSED_PARAM(dataBlock), const int numBits, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += numBits;
    }

    virtual void SerialiseString(char *UNUSED_PARAM(string), const u32 maxChars, const char* UNUSED_PARAM(name) = NULL)
    {
        m_currentSize += maxChars << 3;  //Number of chars x 8 to make it in bits!
    }

private:

	u32		m_currentSize;
	bool	m_calcMaxSize; 
};

//============================================================================================
// CSyncDataLogger
//============================================================================================
class CSyncDataLogger : public CSyncDataBase
{
public:
	CSyncDataLogger(netLoggingInterface* pLog = NULL) : CSyncDataBase(pLog)
	{
		m_type = SYNC_DATA_LOG;
	}
};

} // namespace rage

// --- Globals ------------------------------------------------------------------
#endif // !INC_NETWORKSERIALISERS_H_
