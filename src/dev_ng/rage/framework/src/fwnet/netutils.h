// 
// netutils.h 
// 
// Copyright (C) 1999-2013 Rockstar Games.  All Rights Reserved. 
//

#ifndef NET_UTILS_H
#define NET_UTILS_H

#include "fwnet/netchannel.h"
#include "fwnet/netlog.h"
#include "fwnet/nettypes.h"
#include "profile/timebars.h"
#include "system/timer.h"
#include "net/netfuncprofiler.h"

#include <float.h>

namespace rage
{

//PURPOSE
//  Predicate function for processing network objects with a specified network object ype
class netObjectTypePredicate
{
public:

    static void SetObjectType(NetworkObjectType objectType);
    static bool ObjectInclusionTest(const netObject *networkObject);

private:
    static NetworkObjectType m_ObjectType;
};

//PURPOSE
//	This is a wrapper class for the tiembar macros in order to
//  easily enable/disable the updating of the timebars during the
//  network update function. This is necessary as the update function
//  can be called multiple times per frame if the game is doing a long
//  blocking load.
class netTimeBarWrapper
{
public:

    // PURPOSE
	//  Class contructor
	//PARAMS
	//	updateTimeBars - whether the time bars should be updated from the member function calls
    netTimeBarWrapper(bool updateTimeBars);

    // PURPOSE
	//  Class destructor
    ~netTimeBarWrapper();

    // PURPOSE
	//  Starts a new timebar for profiling a section of code
	//PARAMS
	//	name - name of the system to profile
    void StartTimer(const char *name, bool detail = true);

    // PURPOSE
	//  Starts a new timebar group for profiling a system or groups of systems
    //  that can contain further nested groups or timers
	//PARAMS
	//	name - name of the system to profile
    void PushTimer(const char *name, bool detail = true);
	void PushBudgetTimer(const char *name, float budget);
	void PushIdleTimer(const char *name);


    // PURPOSE
	//  Closes a timebar group previously started with a call to PushTimer
    void PopTimer(bool detail = true);

private:

#if RAGE_TIMEBARS
    bool m_UpdateTimeBars; // Indicates whether time bars should be updated when the member functions are called
#if __ASSERT
    u32  m_PushDepth;      // Keeps track of how many times push has been called on this timebar
#endif // __ASSERT
#endif // RAGE_TIMEBARS
};




//PURPOSE
// This class keeps track of the average value of a collection of data samples,
// updated when the specified time interval elapses or the specified number of samples
// is received
template <unsigned MAX_SAMPLES, unsigned SAMPLE_INTERVAL> class DataAverageCalculator
{
public:

    DataAverageCalculator() :
    m_DataSampled(false)
    , m_LastValueAdded(0.0f)
    , m_LastHighestValue(0.0f)
    , m_LastLowestValue(0.0f)
    , m_LastAverageValue(0.0f)
    , m_NumSamples(0)
    , m_CollectionStartTime(0)
    {
    }

    bool HasDataBeenSampled() const
    {
        return m_DataSampled;
    }

    unsigned GetNumCurrentSamples() const
    {
        return m_NumSamples;
    }

    void AddSample(float value)
    {
        CompileTimeAssert(MAX_SAMPLES > 0);

        if(m_NumSamples >= MAX_SAMPLES)
        {
            UpdateAverage();
        }

        if(gnetVerifyf(m_NumSamples < MAX_SAMPLES, "Unexpected number of samples!"))
        {
            m_DataSamples[m_NumSamples] = value;
            m_NumSamples++;

            m_LastValueAdded = value;
        }
    }

    float GetLastValueAdded() const
    {
        return m_LastValueAdded;
    }

    float GetLowest() const
    {
        CheckForAverageUpdate();

        return m_LastLowestValue;
    }

    float GetHighest() const
    {
        CheckForAverageUpdate();

        return m_LastHighestValue;
    }

    float GetAverage() const
    {
        CheckForAverageUpdate();

        return m_LastAverageValue;
    }

private:

    void CheckForAverageUpdate() const
    {
        unsigned currentTime = sysTimer::GetSystemMsTime();

        if((currentTime - m_CollectionStartTime) > SAMPLE_INTERVAL && (SAMPLE_INTERVAL != 0))
        {
            UpdateAverage();
        }
    }

    void UpdateAverage() const
    {
        unsigned currentTime  = sysTimer::GetSystemMsTime();
        m_CollectionStartTime = currentTime;

        float lowestValue   = FLT_MAX;
        float highestValue  = -FLT_MAX;
        float averageValue  = 0.0f;
        for(unsigned index  = 0; index < m_NumSamples; index++)
        {
            float value = m_DataSamples[index];

            if(value < lowestValue)
            {
                lowestValue = value;
            }

            if(value > highestValue)
            {
                highestValue = value;
            }

            averageValue += value;
        }

        if(m_NumSamples != 0)
        {
            m_LastAverageValue = averageValue / m_NumSamples;
            m_LastLowestValue  = lowestValue;
            m_LastHighestValue = highestValue;
            m_DataSampled      = true;
        }
        else
        {
            m_LastAverageValue = 0.0f;
            m_LastLowestValue  = 0.0f;
            m_LastHighestValue = 0.0f;
        }

        m_NumSamples = 0;
    }

    mutable bool     m_DataSampled;
    mutable float    m_LastAverageValue;
    mutable float    m_LastLowestValue;
    mutable float    m_LastHighestValue;
    mutable float    m_LastValueAdded;
    float            m_DataSamples[MAX_SAMPLES];
    mutable unsigned m_NumSamples;
    mutable unsigned m_CollectionStartTime;
};

#if ENABLE_NETWORK_LOGGING

const char *GetUpdateLevelString          (unsigned updateLevel);
const char *GetCanCloneErrorString        (unsigned resultCode);
const char *GetCanPassControlErrorString  (unsigned resultCode);
const char *GetCanAcceptControlErrorString(unsigned resultCode);

#else

inline const char *GetUpdateLevelString          (unsigned) { return ""; }
inline const char *GetCanCloneErrorString        (unsigned) { return ""; }
inline const char *GetCanPassControlErrorString  (unsigned) { return ""; }
inline const char *GetCanAcceptControlErrorString(unsigned) { return ""; }

#endif // ENABLE_NETWORK_LOGGING


namespace netUtils
{
    // calculates whether a sequence number is greater than another, taking the fact that the sequence numbers
    // wrap into consideration
    inline bool IsSeqGreater(u32 newSeq, u32 oldSeq, u32 numBits)
    {
        // This code handles sequence number wrapping by shifting the sequence component up into
        // the most significant bits of a 32-bit integer and casting it to a value of this type. This causes
        // the most significant bit of the sequence component to be used as the sign bit, which provides
        // a floating window of half the range of the sequence for sequence comparisons. As an example of this,
        // an old sequence number of 255 (the maximum value for the sequence) will be determined as less than
        // a new sequence number in the range (0-127)
        return (static_cast<s32>((newSeq<<(32-numBits)) - (oldSeq<<(32-numBits))) > 0);
    }

    // calculates whether a sequence number is greater or equal than another, taking the fact that the sequence numbers
    // wrap into consideration
    inline bool IsSeqGreaterOrEqual(u32 newSeq, u32 oldSeq, u32 numBits)
    {
        // This code handles sequence number wrapping by shifting the sequence component up into
        // the most significant bits of a 32-bit integer and casting it to a value of this type. This causes
        // the most significant bit of the sequence component to be used as the sign bit, which provides
        // a floating window of half the range of the sequence for sequence comparisons. As an example of this,
        // an old sequence number of 255 (the maximum value for the sequence) will be determined as less than
        // a new sequence number in the range (0-127)
        return (static_cast<s32>((newSeq<<(32-numBits)) - (oldSeq<<(32-numBits))) >= 0);
    }

    // calculate the number of bits set in the specified value
    inline unsigned CountBitsSet(unsigned value)
    {
        unsigned int numBitsSet = 0;

        while(value)
        {
            value &= (value-1);

            numBitsSet++;
        }

        return numBitsSet;
    }
}

} // namespace rage

#endif  // NET_UTILS_H
