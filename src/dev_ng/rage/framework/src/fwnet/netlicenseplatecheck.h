//
// filename:	netlicenseplatecheck.h
// description:	
//

#ifndef INC_NETLICENSEPLATECHECK_H_
#define INC_NETLICENSEPLATECHECK_H_

#include "rline\socialclub\rlsocialclubcommon.h"

namespace netLicensePlateCheck
{
	static const unsigned MAX_NUM_PLATES = 30;

	typedef int CheckTextToken;
	static const CheckTextToken INVALID_TOKEN = 0;

	enum ReturnCode
	{
		RESULT_STRING_OK = 0,
		RESULT_STRING_FAILED,
		RESULT_PENDING,
		RESULT_INVALID_TOKEN,
		RESULT_ERROR = -1,
	};

	bool ValidateLicensePlate(const char* inString, const rage::rlScLanguage language, CheckTextToken& outToken);
	ReturnCode GetStatusForRequest(const CheckTextToken& token);
	unsigned GetCount( const CheckTextToken& token );
	const char* GetPlate( const CheckTextToken& token, const int& index );
	const char* GetPlateData( const CheckTextToken& token, const int& index );
	bool UpdateWithAddPlate(const CheckTextToken& inToken, rage::rlScLicensePlateInfo& plate);
}

namespace netLicensePlateAdd
{
	typedef int AddTextToken;
	static const AddTextToken INVALID_TOKEN = 0;

	enum ReturnCode
	{
		RESULT_OK = 0,
		RESULT_PENDING,
		RESULT_INVALID_TOKEN,
		RESULT_ERROR = -1,
	};

	bool AddLicensePlate(const int localGamerIndex, const char* inString, const char* inData, const rage::rlScLanguage language, AddTextToken& outToken);
	ReturnCode GetStatusForRequest(const AddTextToken& token);
}

namespace netLicensePlateIsValid
{
	typedef int IsValidTextToken;
	static const IsValidTextToken INVALID_TOKEN = 0;

	enum ReturnCode
	{
		RESULT_OK = 0,
		RESULT_PENDING,
		RESULT_PROFANE,
		RESULT_RESERVED,
		RESULT_MALFORMED,
		RESULT_NOTVALID,
		RESULT_INVALID_TOKEN,
		RESULT_ERROR = -1,
	};

	bool IsValidLicensePlate(const char* inString, const rage::rlScLanguage language, IsValidTextToken& outToken);
	ReturnCode GetStatusForRequest(const IsValidTextToken& token);
}

namespace netLicensePlateChange
{
	enum ReturnCode
	{
		RESULT_OK = 0,
		RESULT_PENDING,
		RESULT_ERROR = -1,
	};

	bool ChangeLicensePlate(const int localGamerIndex
							,const char* oldPlateText
							,const char* newPlateText
							,const char* plateData);

	ReturnCode GetStatusForRequest( );
}

#endif // !INC_NETLICENSEPLATECHECK_H_
