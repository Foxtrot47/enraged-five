//
// netlogginginterface.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_NETWORK_LOGGING_INTERFACE_H_
#define INC_NETWORK_LOGGING_INTERFACE_H_

#include "net/netsequence.h"

#include "fwnet/netchannel.h"

namespace rage
{
//PURPOSE
// Interface class for providing logging functionality for the network game
class netLoggingInterface
{
public:

    //PURPOSE
    // This enumeration can be used to tag logging output with differing priority levels.
    // This allows an application to customise the level of output that is required at run-time,
    // for example to only display error information or more detailed output
    enum LoggingLevel
    {
        LOG_LEVEL_LOW,    // Low importance messages
        LOG_LEVEL_MEDIUM, // Medium importance messages
        LOG_LEVEL_HIGH,   // High importance messages
        LOG_LEVEL_ERROR,  // Error messages
    };

    //PURPOSE
    // Logs the specified text
    //PARAMS
    // logText - The text to log
    virtual void Log(const char *logText, ...) = 0;

    //PURPOSE
    // Logs a line break
    //PARAMS
    // includeFrameCount - Indicates whether to prepend the frame count
    virtual void LineBreak() = 0;

    //PURPOSE
    // Flushes any buffered log data to the output device
    //PARAMS
    // waitForFlushToComplete - indicates whether the function should block waiting for the flush to complete
    virtual void Flush(bool waitForFlushToComplete = false) = 0;

    //PURPOSE
    // Disables this log; any further log output will be ignored until Enable() is called
	virtual void Disable() = 0;

    //PURPOSE
    // Enables this log
	virtual void Enable() = 0;

    //PURPOSE
    // Returns whether this log file is enabled
    virtual bool IsEnabled() const = 0;

    //PURPOSE
    // Writes a key/value pair in a consistent format
    //PARAMS
    // dataName  - Key name
    // dataValue - Value name
    virtual void WriteDataValue(const char *dataName, const char *dataValue, ...) = 0;

    //PURPOSE
    // Logs a message header in a consistent format
    //PARAMS
    // received       - Indicates whether the message was sent or received
    // sequence       - Sequence number for the message being sent
    // messageString1 - First portion of message header text
    // messageString2 - Second portion of message header text
    // playerName     - Name (or IP address) of the player message is sent/received from/to
    virtual void WriteMessageHeader(bool received, netSequence sequence, const char *messageString1, const char *messageString2, const char *playerName) = 0;

    //PURPOSE
    // Logs data relative to a specific player in a consistent format
    //PARAMS
    // dataName   - The name of the data to log
    // dataValue  - The value of the data to log
    // playerName - The name of the player this data is related to
    virtual void WritePlayerText(const char *dataName, const char *dataValue, const char *playerName) = 0;

    //PURPOSE
    // Writes a logging event to the specified network log file along with any specified data
    //PARAMS
    // eventName          - Name of the event
    // eventData          - Data associated with the event
    // trailingLineBreaks - Number of new lines to append to the end of the logging event
    virtual void WriteEventData(const char *eventName, const char *eventData, unsigned trailingLineBreaks) = 0;

    //PURPOSE
    // Writes a node header to the specified network log file
    //PARAMS
    // nodeName - Name of the node
    virtual void WriteNodeHeader(const char *nodeName) = 0;

    //PURPOSE
    // Called at the end of the network update
    virtual void PostNetworkUpdate() {}

    //PURPOSE
    // Writes debug statistics about this log file to the specified buffer
    //PARAMS
    // buffer     - buffer to write statistics to
    // bufferSize - size of the buffer in characters
    virtual void GetDebugStatisticsText(char *UNUSED_PARAM(buffer), const unsigned UNUSED_PARAM(bufferSize)) { gnetAssertf(0, "Calling GetDebugStatisticsText() on a class that doesn't support it!"); }
	

	//PURPOSE
	// Writes the file name to the specified buffer
	//PARAMS
	// buffer     - buffer to write the file name to
	// bufferSize - size of the buffer in characters
	virtual void GetFileName(char *UNUSED_PARAM(buffer), u32 UNUSED_PARAM(bufferSize)) const { }

protected:

    virtual ~netLoggingInterface() {}
};

} // namespace rage

#endif // !INC_NETWORK_LOGGING_INTERFACE_H_
