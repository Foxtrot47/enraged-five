//
// netobjectmgrmessagehandler.h
//
// Copyright (C) 1999-2009 Rockstar Games.  All Rights Reserved.
//

#ifndef NETWORK_OBJECT_MGR_MESSAGE_HANDLER_H
#define NETWORK_OBJECT_MGR_MESSAGE_HANDLER_H

#include "fwnet/netbandwidthstats.h"
#include "fwnet/nettypes.h"
#include "fwnet/netobjectmessages.h"
#include "fwnet/netinterface.h"
#include "atl/array.h"
#include "net/event.h"

namespace rage
{
    class netObject;
    class netObjectMgrBase;
    class netPlayer;

//PURPOSE
// This class is responsible for managing the object management network messages
// sent between the different machines in the network game
class netObjectMgrMessageHandler
{
public:

    netObjectMgrMessageHandler();
    ~netObjectMgrMessageHandler();

    //PURPOSE
    // Initialises the message handler
    //PARAMS
    // objectManager - The object manager associated with the message handler
    void Init(netObjectMgrBase &objectManager);

    //PURPOSE
    // Shuts down the message handler
    void Shutdown();

    //PURPOSE
    // Performs any per frame processing required by the manager
    void Update();

    //PURPOSE
    // This function is called when a new player joins the network session
    //PARAMS
    // player - The player joining the session
    void PlayerHasJoined(const netPlayer& player);

    //PURPOSE
    // This function is called when an existing player leaves the network session
    //PARAMS
    // player - The player leaving the session
    void PlayerHasLeft(const netPlayer& player);

    netLoggingInterface *GetLog() { return m_Log; }
    void SetLog(netLoggingInterface *log) { m_Log = log; }

    //PURPOSE
    // This function returns the sequence number to use
    // when sending a clone sync message to the specified player
    //PARAMS
    // player - The player the message is to be sent to
    netSequence GetSequenceNumberForPlayer(const netPlayer& player) const;

    //PURPOSE
    // Returns the current packet loss metric for the specified player. This is calculated
    // by keeping track of how many of a floating window of sequences were acked within a time limit
    float GetPacketLoss(ActivePlayerIndex playerIndex) const;

    //PURPOSE
    // Adds a clone sync to the clone sync message for this connection
    //PARAMS
    // player    - The network player to send the update data to
    // object    - The network object to send update data for
    // msgBuffer - The message buffer containing the packed object update data to send
    // seqNum    - The sequence number to use when sending the message
    void SendCloneSync( const netPlayer &player,
                        const netObject *pObject,
                        datBitBuffer    &msgBuffer,
                        netSequence     &seqNum,
                        bool             sendImmediately);

    //PURPOSE
    // Add clone creation data to the packed reliables message for the specified player
    //PARAMS
    // toPlayer         - The player to send the creation data to
    // fromPlayer       - The player on the local peer that controls the object being created
    // objectType       - The object type of the object being created
    // objectID         - The ID of the object being created
    // flags            - Any flags to send with the creation data
    // createData       - The game specific creation data for the object being created
    // createDataSize   - The size of the game specific data
    void AddCreate   (const netPlayer       &toPlayer,
                      const netPlayer       &fromPlayer,
                      NetworkObjectType      objectType,
                      const ObjectId         objectID,
                      NetObjFlags            flags,
                      u8*                    createData,
                      unsigned               createDataSize);

    //PURPOSE
    // Adds an acknowledgement to a clone creation message to the packed reliables message for the specified player
    //PARAMS
    // toPlayer   - The player to send the ACK to
    // fromPlayer - The player sending the ACK
    // objectID   - The object ID of the object being ACKed
    // ackCode    - The Ack code to send
    void AddCreateAck(const netPlayer       &toPlayer,
                      const netPlayer       &fromPlayer,
                      ObjectId               objectID,
                      NetObjectAckCode       ackCode);

    //PURPOSE
    // Adds an acknowledgement to a clone sync message to the packed reliables message for the specified player
    //PARAMS
    // toPlayer      - The player to send the ACK to
    // fromPlayer    - The player sending the ACK
    // syncSequence  - The sequence number of the clone sync message to acknowledge
    // numObjects    - The number of objects being ACKed
    // numFailedACKs - the number of failed ACKs
    // objectIDs     - The IDs of the objects being ACKed
    // ackCodes      - The Ack codes for the objects being ACKed
    void AddSyncAcks(const netPlayer       &toPlayer,
                     const netPlayer       &fromPlayer,
                     netSequence            syncSequence,
                     unsigned               numObjects,
                     unsigned               numFailedACKs,
                     ObjectId               objectIDs[MAX_SYNC_ACKS],
                     NetObjectAckCode       ackCodes[MAX_SYNC_ACKS]);

    //PURPOSE
    // Adds clone removal data to the packed reliables message for the specified player
    //PARAMS
    // toPlayer         - The player to send the removal data to
    // fromPlayer       - The player on the local peer that controls the object being removed
    // objectID         - The object ID of the object to remove
    // ackCode          - The Ack code to send
    void AddRemove   (const netPlayer       &toPlayer,
                      const netPlayer       &fromPlayer,
                      ObjectId               objectID, 
                      u32                    ownershipToken);

    //PURPOSE
    // Adds an acknowledgement to a clone removal message to the packed reliables message for the specified player
    //PARAMS
    // toPlayer   - The player to send the ACK to
    // fromPlayer - The player sending the ACK
    // objectID   - The object ID of the object being ACKed
    // ackCode    - The Ack code to send
    void AddRemoveAck(const netPlayer       &toPlayer,
                      const netPlayer       &fromPlayer,
                      ObjectId               objectID,
                      NetObjectAckCode       ackCode);

#if __BANK

    //PURPOSE
    // Returns the number of clone sync messages sent since the last call to ResetMessageCounts()
    unsigned GetNumCloneSyncMessagesSent() const { return m_NumCloneSyncMessagesSent; }   

    //PURPOSE
    // Returns the number of clone sync ack messages sent since the last call to ResetMessageCounts()
    unsigned GetNumCloneSyncAckMessagesSent() const { return m_NumCloneSyncAckMessagesSent; }   

    //PURPOSE
    // Returns the number of packed reliable messages sent since the last call to ResetMessageCounts()
    unsigned GetNumPackedReliableMessagesSent() const { return m_NumPackedReliableMessagesSent; }   

    //PURPOSE
    // Reset the counts of object manager messages sent
    void ResetMessageCounts() { m_NumCloneSyncMessagesSent = m_NumCloneSyncAckMessagesSent = m_NumPackedReliableMessagesSent = 0; }

    netBandwidthStatistics& GetBandwidthStatistics() { return m_bandwidthStats; }
#endif 

private:

    //PURPOSE
    // Handles an incoming network message
    //PARAMS
    // messageData - Data describing the incoming message
    void OnMessageReceived(const ReceivedMessageData &messageData);

    //PURPOSE
    // Handles clone sync messages
    //PARAMS
    // messageData - Data describing the incoming message
    bool HandleCloneSyncMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Handles packed clone sync ack messages
    //PARAMS
    // messageData - Data describing the incoming message
    bool HandlePackedCloneSyncAck(const ReceivedMessageData &messageData);

    //PURPOSE
    // Handles packed reliables messages
    //PARAMS
    // messageData - Data describing the incoming message
    bool HandlePackedReliablesMsg(const ReceivedMessageData &messageData);

    //PURPOSE
    // Sends a the packed clone sync message to the specified player
    //PARAMS
    // player - The network player to send the message to
    void SendConnectionSyncMessage(const netPlayer& player);

    //PURPOSE
    // Sends a the packed sync ACK message to the specified player
    //PARAMS
    // player - The network player to send the message to
    void SendPackedSyncAckMessage(const netPlayer& player);

    //PURPOSE
    // Sends a the packed reliables message to the specified player
    //PARAMS
    // player - The network player to send the message to
    void SendPackedReliableMessage(const netPlayer& player);

    //PURPOSE
    // Processes packed clone creation data
    //PARAMS
    // messageBuffer - The message buffer containing the packed clone creation data
    // fromPlayer    - The player the clone creation data was received from
    // toPlayer      - The player the clone creation data is for
    // timestamp     - The timestamp for the creation data
    void ProcessPackedCloneCreates(datBitBuffer    &messageBuffer,
                                   const netPlayer &fromPlayer,
                                   const netPlayer &toPlayer,
                                   unsigned         timestamp);

    //PURPOSE
    // Processes packed clone creation ACK data
    //PARAMS
    // messageBuffer - The message buffer containing the packed clone creation ACK data
    // fromPlayer    - The player the clone creation ACKdata was received from
    // toPlayer      - The player the clone creation ACKdata is for
    void ProcessPackedCloneCreateAcks(datBitBuffer    &messageBuffer,
                                      const netPlayer &fromPlayer,
                                      const netPlayer &toPlayer);

    //PURPOSE
    // Processes packed clone removal data
    //PARAMS
    // messageBuffer - The message buffer containing the packed clone removal data
    // fromPlayer    - The player the clone removal data was received from
    // toPlayer      - The player the clone removal data is for
    void ProcessPackedCloneRemoves(datBitBuffer    &messageBuffer,
                                   const netPlayer &fromPlayer,
                                   const netPlayer &toPlayer);

    //PURPOSE
    // Processes packed clone creation data
    //PARAMS
    // messageBuffer - The message buffer containing the packed clone removal ACK data
    // fromPlayer    - The player the packed clone removal ACK data was received from
    // toPlayer      - The player the packed clone removal ACK data is for
    void ProcessPackedCloneRemoveAcks(datBitBuffer    &messageBuffer,
                                      const netPlayer &fromPlayer,
                                      const netPlayer &toPlayer);

    //PURPOSE
    // This function returns the last sequence number received from
    // clone sync messages from the specified player
    //PARAMS
    // player - The player to get the last sequence received
    netSequence GetLastSequenceReceivedFromPlayer(const netPlayer& player) const;

    //PURPOSE
    // This function sets the last sequence number received from
    // clone sync messages from the specified player
    //PARAMS
    // player   - The player to set the last sequence received
    // sequence - The last sequence number
    void SetLastSequenceReceivedFromPlayer(const netPlayer& player, const netSequence sequence);

    //PURPOSE
    // Resets the stored timestamps for the first clone sync messages received in the current frame
    void ResetCurrentFrameSyncTimestamps();

#if __BANK
    unsigned m_NumCloneSyncMessagesSent;      // The number of clone sync messages sent in the last second
    unsigned m_NumCloneSyncAckMessagesSent;   // The number of clone sync ack message sent in the last second
    unsigned m_NumPackedReliableMessagesSent; // The number of packed reliable messages sent in the last second
#endif // __BANK

#if __BANK

    // Bandwidth recorders for the data in the different message types that is not sync or create data
	RecorderId m_packedReliablesId;
	RecorderId m_creationDataId;
	RecorderId m_syncHeaderDataId;
	RecorderId m_syncAckDataId;

    netBandwidthStatistics m_bandwidthStats;

#endif // __DEV

    //PURPOSE
    // Very simple packet loss tracking mechanism. Low memory and CPU overhead.
    // When tracking starts the start sequence is set to the sequence of the next message sent. Once
    // the number of tracked messages has been sent a timer is started, once it elapses any unacked
    // messages are counted as dropped and the process restarts. This relies on data being constantly
    // sent to provide valid results
    class CloneSyncPacketLossTracker
    {
    public:

        CloneSyncPacketLossTracker() :
        m_StartSeq(INVALID_SEQUENCE_ID)
        , m_AckFlags(0)
        , m_TimeAllSequencesSent(0)
        , m_LastPacketLossRatio(0.0f)
        {
        }

        void Reset()
        {
            m_StartSeq             = INVALID_SEQUENCE_ID;
            m_AckFlags             = 0;
            m_TimeAllSequencesSent = 0;
            m_LastPacketLossRatio  = 0.0f;
        }

        float GetPacketLossRatio() const { return m_LastPacketLossRatio; }

        void OnSend(const netSequence sequence);
        void OnAck(const netSequence sequence);
        void Update();

    private:
        static const unsigned NUM_SEQUENCES_TRACKED = 16;
        static const unsigned TIME_TO_WAIT_FOR_ACKS = 2000;

        netSequence m_StartSeq;
        u16         m_AckFlags;
        u32         m_TimeAllSequencesSent;
        float       m_LastPacketLossRatio;
    };

    // Network object manager using this message handler
    netObjectMgrBase *m_ObjectManager;

    // Logging object for message handling
    netLoggingInterface *m_Log;

    // A unique message handler only used by the object manager
    NetworkPlayerEventDelegate m_Dlgt;

    // Bitflags specifying whether the next connection sync message should be send immediately (ignoring the send interval)
    PlayerFlags m_SendConnectionSyncImmediately;

    // An array of sync messages to be sent down each connection,
    // each message can contain a number of syncs
    atFixedArray< cloneSyncMsg, MAX_NUM_ACTIVE_PLAYERS > m_ConnectionSyncMsgs;

    // An array of packed clone sync ack messages to send to each connection
    atFixedArray< packedCloneSyncACKsMsg, MAX_NUM_ACTIVE_PLAYERS > m_PackedSyncAcks;

    // An array of packed reliable messages to send to each connection
    atFixedArray< packedReliablesMsg, MAX_NUM_ACTIVE_PLAYERS > m_PackedReliables;

    // Sequence number used for next outbound update per player
    netSequence m_Sequence[MAX_NUM_ACTIVE_PLAYERS];

    // Sequence number last received by the players in the session
    netSequence m_LastReceivedSequence[MAX_NUM_ACTIVE_PLAYERS];

    // packet loss tracking for clone sync messages
    CloneSyncPacketLossTracker m_CloneSyncPacketLossTrackers[MAX_NUM_ACTIVE_PLAYERS];

    // this stores the last sync timestamp received in the current game frame. This is reset each frame.
    // used to limit the number of backed up sync messages processed to prevent CPU spikes
    u32 m_CurrentFrameSyncTimestamp[MAX_NUM_PHYSICAL_PLAYERS];
};

} // namespace rage

#endif // NETWORK_OBJECT_MGR_MESSAGE_HANDLER_H
