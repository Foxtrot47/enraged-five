//
// sample_base/sample_base.cpp
//
// Copyright (C) 1999-2010 Rockstar Games.  All Rights Reserved.
//

#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "grcore/im.h"
#include "grcore/setup.h"
#include "grcore/device.h"
#include "grcore/viewport.h"
#include "grcore/state.h"
#include "system/main.h"
#include "system/param.h"
#include "input/input.h"
#include "profile/timebars.h"
#include "spatialdata/sphere.h"
#include "fwdrawlist/drawlistmgr.h"

#include "input/input.h"
#include "input/mapper.h"
#include "parser/manager.h"
#include "fwsys/threadtype.h"
#include "fwrenderer/renderthread.h"
#include "fwdebug/debugdraw.h"
#include "streaming/streamingengine.h"

#include "atl/delegate.h"

#include "fwsys/timer.h"
#include "fwsys/fsm.h"

using namespace rage;

PARAM(oneframe, "Only render a single frame");

class grcSampleOrthoCam
{
public:
	enum OrthoMode
	{
		ORTHO_TOP,ORTHO_BOTTOM,ORTHO_LEFT,ORTHO_RIGHT,ORTHO_FRONT,ORTHO_BACK,ORTHO_MODE_COUNT
	};

	grcSampleOrthoCam();

	void			InitViews();

	void			Frame(const spdSphere& frameSphere);

	float			GetNearZ() const { return m_OrthoNearZ; }
	float			GetFarZ() const { return m_OrthoFarZ; }

	float			GetOrthoZoom() const { return m_OrthoZooms[(int)m_OrthoMode]; }
	float			GetOrthoZoom(int idx) const { FastAssert(idx<ORTHO_MODE_COUNT); return m_OrthoZooms[idx]; }

	void			SetOrthoZoom(float zoom) { m_OrthoZooms[(int)m_OrthoMode] = zoom; }
	void			SetOrthoZoom(int idx, float zoom) { FastAssert(idx<ORTHO_MODE_COUNT); m_OrthoZooms[idx] = zoom;}

	const Matrix34&	GetWorldMatrix() const { return m_OrthoMats[(int)m_OrthoMode]; }
	const Matrix34& GetWorldMatrix(int idx) const { FastAssert(idx<ORTHO_MODE_COUNT); return m_OrthoMats[idx]; }

	void			SetWorldMatrix(const Matrix34& mat) { m_OrthoMats[(int)m_OrthoMode] = mat; }
	void			SetWorldMatrix(int idx, const Matrix34& mat) { FastAssert(idx<ORTHO_MODE_COUNT); m_OrthoMats[idx] = mat;}

	void			SetOrthoMode(OrthoMode mode) { m_OrthoMode = mode; }

	void			Update();

	void			SetUseZUp(bool bUseZUp) { m_UseZUp = bUseZUp; }
	bool			GetUseZUp() const { return m_UseZUp; }

private:
	bool			UpdateMouse(float dt);
	bool			UpdatePad(float dt);
	bool			ProcessInputs(bool pan, bool zoom, float x, float y, float dt);

private:
	OrthoMode	m_OrthoMode;
	float		m_OrthoNearZ;
	float		m_OrthoFarZ;
	float		m_SpeedScalar;

	ioMapper	m_Mapper;
	ioValue		m_Left;
	ioValue		m_Right;
	ioValue		m_Up;
	ioValue		m_Down;
	ioValue		m_Zoom;
	ioValue		m_Xaxis;
	ioValue		m_Yaxis;
	ioValue		m_MoveSlowly;
	ioValue		m_MoveQuickly;
	ioValue		m_RequiredKey;

	int			m_LastMouseX;
	int			m_LastMouseY;

	Matrix34	m_OrthoMats[ORTHO_MODE_COUNT];
	float		m_OrthoZooms[ORTHO_MODE_COUNT];

	bool		m_UseZUp;
	Vector3		m_vUp;
};

grcSampleOrthoCam::grcSampleOrthoCam()
: m_OrthoMode(ORTHO_TOP)
, m_OrthoNearZ(-1000.0f)
, m_OrthoFarZ(1000.0f)
, m_LastMouseX(0)
, m_LastMouseY(0)
, m_UseZUp(false)
, m_vUp(YAXIS)
{
	m_Mapper.Reset();
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RLEFT, m_Left);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RRIGHT, m_Right);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RDOWN, m_Down);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RUP, m_Up);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L1, m_Zoom);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LX, m_Xaxis);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_Yaxis);

	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_MoveSlowly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_MoveQuickly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_RequiredKey);
}

void grcSampleOrthoCam::InitViews()
{
	m_OrthoMats[ORTHO_TOP].Identity();

	//Initialize the TOP cam
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_TOP].d = Vector3(0.0f, 0.0f, 10.0f);
		m_OrthoMats[ORTHO_TOP].LookDown(-ZAXIS, YAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_TOP].d = Vector3(0.0f, 10.0f, 0.0f);
		m_OrthoMats[ORTHO_TOP].LookDown(-YAXIS, ZAXIS);
	}
	m_OrthoZooms[ORTHO_TOP] = 1.0f;

	//Initialzie the BOTTOM cam
	m_OrthoMats[ORTHO_BOTTOM].Identity();
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_BOTTOM].d = Vector3(0.0f, 0.0f, -10.0f);
		m_OrthoMats[ORTHO_BOTTOM].LookDown(ZAXIS, -YAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_BOTTOM].d = Vector3(0.0f, -10.0f, 0.0f);
		m_OrthoMats[ORTHO_BOTTOM].LookDown(YAXIS, -ZAXIS);
	}
	m_OrthoZooms[ORTHO_BOTTOM] = 1.0f;

	//Initialize the LEFT cam
	m_OrthoMats[ORTHO_LEFT].Identity();
	m_OrthoMats[ORTHO_LEFT].d = Vector3(-10.0f, 0.0f, 0.0f);
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_LEFT].LookDown(-XAXIS, m_vUp);
		m_OrthoMats[ORTHO_LEFT].RotateX(DtoR*90.0f);
	}
	else
	{
		m_OrthoMats[ORTHO_LEFT].LookDown(XAXIS, m_vUp);
	}
	m_OrthoZooms[ORTHO_LEFT] = 1.0f;

	//Initialize the RIGHT cam
	m_OrthoMats[ORTHO_RIGHT].Identity();
	m_OrthoMats[ORTHO_RIGHT].d = Vector3(10.0f, 0.0f, 0.0f);
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_RIGHT].LookDown(XAXIS, m_vUp);
		m_OrthoMats[ORTHO_RIGHT].RotateX(DtoR*90.0f);
	}
	else
	{
		m_OrthoMats[ORTHO_RIGHT].LookDown(-XAXIS, m_vUp);
	}
	m_OrthoZooms[ORTHO_RIGHT] = 1.0f;

	//Initialize the FRONT cam
	m_OrthoMats[ORTHO_FRONT].Identity();
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_FRONT].d = Vector3(0.0f, -10.0f, 0.0f);
		m_OrthoMats[ORTHO_FRONT].LookDown(YAXIS, ZAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_FRONT].d = Vector3(0.0f, 0.0f, -10.0f);
		m_OrthoMats[ORTHO_FRONT].LookDown(ZAXIS, YAXIS);
	}
	m_OrthoZooms[ORTHO_FRONT] = 1.0f;

	//Initialize the BACK cam
	m_OrthoMats[ORTHO_BACK].Identity();
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_BACK].d = Vector3(0.0f, 10.0f, 0.0f);
		m_OrthoMats[ORTHO_BACK].LookDown(-YAXIS, ZAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_BACK].d = Vector3(0.0f, 0.0f, 10.0f);
		m_OrthoMats[ORTHO_BACK].LookDown(-ZAXIS, YAXIS);
	}
	m_OrthoZooms[ORTHO_BACK] = 1.0f;
}

void grcSampleOrthoCam::Update()
{
	float dt = TIME.GetUnwarpedRealtimeSeconds();

	m_Mapper.Update();
	if( UpdateMouse(dt) )
		return;
	else
		UpdatePad(dt);
}

bool grcSampleOrthoCam::UpdateMouse(float dt)
{
	float x = (float) (ioMouse::GetX() - m_LastMouseX); 
	float y = (float) (ioMouse::GetY() - m_LastMouseY); 

	float movementScalar = 1.0;
	if(m_MoveSlowly.IsDown())
	{
		movementScalar /= 5.0f;
	}
	if(m_MoveQuickly.IsDown())
	{
		movementScalar *= 5.0f;
	}
	x *= movementScalar * 0.5f;
	y *= movementScalar * 0.5f;

	m_LastMouseX = ioMouse::GetX();
	m_LastMouseY = ioMouse::GetY();

	bool pan = (ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE) != 0;
	bool zoom = (ioMouse::GetButtons() & ioMouse::MOUSE_RIGHT) != 0;

	return ProcessInputs(pan, zoom, x, y, dt);
}

bool grcSampleOrthoCam::UpdatePad(float dt)
{
	float x = m_Xaxis.GetNorm(0.15f);
	float y = m_Yaxis.GetNorm(0.15f);

	return ProcessInputs( (m_Left.IsDown() || m_Right.IsDown() || m_Up.IsDown() || m_Down.IsDown()),
		m_Zoom.IsDown(),
		x, y, 
		dt);
}

bool grcSampleOrthoCam::ProcessInputs(bool pan, bool zoom, float x, float y, float dt)
{
	/*if (rage::INPUT.GetUseKeyboard() && m_RequiredKey.IsUp())
	{
		return false;
	}*/

	if(zoom)
	{
		float zoomAmount = x+y;

		float curOrthoZoom = GetOrthoZoom();
		if( (curOrthoZoom + (zoomAmount * dt)) < 0.1f )
			SetOrthoZoom(0.1f);
		else 
			SetOrthoZoom(curOrthoZoom + (zoomAmount * dt));
	}
	else if(pan)
	{
		Vector3 offsetVector(0.0f,0.0f,0.0f);

		switch(m_OrthoMode)
		{
		case ORTHO_FRONT:
			if(m_UseZUp)
			{
				offsetVector.SetX(-x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetX(x);
				offsetVector.SetY(y);
			}
			break;
		case ORTHO_BACK:
			if(m_UseZUp)
			{
				offsetVector.SetX(x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetX(-x);
				offsetVector.SetY(y);
			}
			break;
		case ORTHO_TOP:
			if(m_UseZUp)
			{
				offsetVector.SetX(-x);
				offsetVector.SetY(y);
			}
			else
			{
				offsetVector.SetX(x);
				offsetVector.SetZ(y);
			}
			break;
		case ORTHO_BOTTOM:
			if(m_UseZUp)
			{
				offsetVector.SetX(x);
				offsetVector.SetY(y);
			}
			else
			{
				offsetVector.SetX(-x);
				offsetVector.SetZ(y);
			}
			break;
		case ORTHO_LEFT:
			if(m_UseZUp)
			{
				offsetVector.SetY(-x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetZ(-x);
				offsetVector.SetY(y);
			}
			break;
		case ORTHO_RIGHT:
			if(m_UseZUp)
			{
				offsetVector.SetY(x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetZ(x);
				offsetVector.SetY(y);
			}
			break;
		default:
			break;
		}
		m_OrthoMats[m_OrthoMode].d.AddScaled(offsetVector, dt);
	}
	return true;
}

void grcSampleOrthoCam::Frame(const spdSphere& frameSphere)
{
	m_OrthoMats[m_OrthoMode].d.Set(frameSphere.GetCenter());
	SetOrthoZoom(frameSphere.GetRadius() * 2.0f);
}

//////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////

// bit flags because a thread might be both main and render thread...
enum eThreadId {
	SYS_THREAD_INVALID			= 0,
	SYS_THREAD_UPDATE				= sysThreadType::THREAD_TYPE_UPDATE,
	SYS_THREAD_RENDER			= sysThreadType::THREAD_TYPE_RENDER,
	SYS_THREAD_AUDIO			= BIT0 << sysThreadType::USER_THREAD_TYPE_SHIFT,
	SYS_THREAD_PATHSERVER		= BIT1 << sysThreadType::USER_THREAD_TYPE_SHIFT,
	SYS_NUM_THREADS
};

class fwApp : public fwFsm
{
public:

protected:
	virtual fwFsm::Return UpdateState(const s32 state, const s32 iMessage, const fwFsm::Event event) = 0;

private:
	bool WantToExit();
};

static sysTimer gFrameTimer;
int gFrameLimit = 1;

void Zeno()
{
	if(gFrameLimit > 0
#if __DEV
		|| fwTimer::GetForceMinimumFrameRate()
#endif
		)
	{
		// frame limiter. Game runs at max speed of some many frames per second. This has to be at the end of this function
		float frameTime = gFrameTimer.GetTime();
		float minFrameTime = gFrameLimit/60.0f;
#if __DEV
		// debug to let us force a slow and/or uneven frame rate
		if(fwTimer::GetForceMinimumFrameRate())
			minFrameTime = fwTimer::GetMaximumFrameTime();
		else if(fwTimer::GetForceUnevenFrameRate() && fwTimer::GetSystemFrameCount()&1)
			minFrameTime = fwTimer::GetMaximumFrameTime();
#endif

		while(frameTime < minFrameTime)
		{
			// Sleep for half the difference
			int zeno = int((minFrameTime-frameTime)*1000)/2; 
			sysIpcSleep(zeno);

			frameTime = gFrameTimer.GetTime();
		}
		// frame timer has to be at the top of this function
		gFrameTimer.Reset();

	}
}

void Timebars()
{
#if DEBUG_DRAW
	if(fwDebugDraw::GetDisplayDebugText())
	{
		PF_DISPLAY_TIMEBARS();
		if(fwDebugDraw::GetDisplayOverBudgetTimebarText())
		{
			PF_DISPLAY_OVERBUDGET_TIMEBARS();
		}
	}
#endif
}

//////////////////////////////////////////////////////////////////////////
class fwSampleBaseRenderThreadInterface : public fwRenderThreadGameInterface
{
public:
	virtual void DefaultPreRenderFunction()
	{
		PF_FRAMEINIT_TIMEBARS();
	}
};

class fwSampleBaseStreamingInterface : public strStreamingInterface
{
public:
	fwSampleBaseStreamingInterface() : strStreamingInterface(1 << 20, 1 << 20)
	{

	}

};

class fwSampleBaseSceneInterface : public fwSceneInterface
{
public:

};

class fwSampleBaseGameInterface : public fwGameInterface
{
public:

};

class fwSampleBase : public fwApp
{
	grcSetup*				m_Setup;
	grcViewport*			m_Viewport;
	grcVertexDeclaration*	m_GridDecl;
	Color32				    m_ClearColor;
	grcSampleOrthoCam		m_OrthoCam;


	float	m_GridSize;
	float	m_GridLines;
	float	m_GridSubLine;
	bool	m_WantsExit;
	bool	m_DrawGrid;
	bool	m_DrawThreeGrids;
	bool	m_UseZUp;
	int		m_ViewMode;
public:
	enum
	{
		State_InitSystem = 0,
		State_InitGame,
		State_RunGame,
		State_PausedGame,
		State_ShutdownGame,
		State_ShutdownSystem
	};

	enum
	{
		Message_Quit = 0,
		Message_Pause,
		Message_Unpause
	};

	enum
	{
		DL_RENDERPHASE					= 0, // Ensure it starts at 0...
		DL_RENDERPHASE_START			= 1,
		DL_RENDERPHASE_UPDATE			= 2,
		DL_RENDERPHASE_END				= 3,
		DL_MAX_TYPES
	};

	fwSampleBase() : 
		m_GridSize(100),
		m_GridLines(2),
		m_GridSubLine(1),
		m_WantsExit(false),
		m_ClearColor(0,10,100),
		m_UseZUp(true),
		m_DrawGrid(__DEV),
		m_DrawThreeGrids(false),
		m_ViewMode(1)
	{

	}

	void AddWidgets();

	bool WantsExit();
	void InitCamera();
	void SetupCamera();
	void UpdateCamera();
	const Matrix34& GetCameraMatrix() const;

	void Update();
	void Paused();
	void Draw();
	void DrawScene();
	void Render();
	void DrawGrid(const Vector3& normal = Vector3(0.0f, 1.0f, 0.0f), const Vector3& tangent = Vector3(1.0f, 0.0f, 0.0f));

	const char* GetFullAssetPath() const;
	void SetFullAssetPath(const char* fullPath);
	const char* GetSampleName() const { return "blergh"; }


	fwFsm::Return InitSystem_OnUpdate() {

		PF_INIT_STARTUPBAR("Startup", 100);
		PF_START_STARTUPBAR("System Init");

		PF_INIT_TIMEBARS("Main", 1260, 0.0f);

		// Initialize the streamer
		pgStreamer::InitClass(256);

		strStreamingEngine::InitClass(rage_new fwSampleBaseStreamingInterface() );
		strStreamingEngine::Init();

		INIT_PARSER;

		sysThreadType::AddCurrentThreadType(SYS_THREAD_UPDATE);
		sysThreadType::AddCurrentThreadType(SYS_THREAD_RENDER);

		m_Setup = rage_new grcSetup();

		char sampleName[1024];
		sprintf(sampleName,"%s (version %s)", GetSampleName() ,RAGE_RELEASE_STRING);
		m_Setup->Init(GetFullAssetPath(),sampleName);
		m_Setup->BeginGfx(true);
		rage::INPUT.Begin(true);

		grcVertexElement e[3];
		e[0].type = grcVertexElement::grcvetPosition;
		e[0].size = 12;
		e[0].format = grcFvf::grcdsFloat3;
		e[1].type = grcVertexElement::grcvetColor;
		e[1].size = 4;
		e[1].format = grcFvf::grcdsColor;
		e[2].type = grcVertexElement::grcvetTexture;
		e[2].size = 8;
		e[2].format = grcFvf::grcdsFloat2;
		m_GridDecl = GRCDEVICE.CreateVertexDeclaration(e,3);

		m_Viewport=rage_new grcViewport;

		m_Setup->CreateDefaultFactories();

		SetState(State_InitGame);

		return fwFsm::Continue;
	}

	fwFsm::Return InitGame_OnUpdate() {

		// initialise renderthread and drawlist mgr
		sysIpcPriority renderThreadPriority = PRIO_BELOW_NORMAL;
		gRenderThreadInterface.SetGameInterface(rage_new fwSampleBaseRenderThreadInterface());
		gRenderThreadInterface.Init(renderThreadPriority);
		sysThreadType::ClearCurrentThreadType(SYS_THREAD_RENDER); // this is no longer the render thread

		gRenderThreadInterface.SetDefaultRenderFunction();

		gDCBuffer = rage_new dlDrawCommandBuffer();


		gDrawListMgr = rage_new dlDrawListMgr();
		gDrawListMgr->Init(DL_MAX_TYPES);

		gDrawListMgr->SetDrawListInfo(0, "INVALID", 0.0f, 0.0f);
		gDrawListMgr->SetDrawListInfo(DL_RENDERPHASE_START, "DL_LIGHTING", 0.0f, 1.5f);
		gDrawListMgr->SetDrawListInfo(DL_RENDERPHASE_UPDATE, "DL_RADAR", 0.0f, 0.0f);
		gDrawListMgr->SetDrawListInfo(DL_RENDERPHASE_END, "DL_PRERNDR_VP", 0.0f, 0.0f);

		InitCamera();

		AddWidgets();

		SetState(State_RunGame);
		return fwFsm::Continue;
	}

	fwFsm::Return RunGame_OnEnter() {
		PF_FINISH_STARTUPBAR();

		return fwFsm::Continue;
	}

	fwFsm::Return RunGame_OnUpdate() {
		PF_FRAMEINIT_TIMEBARS();
		Update();

		if (WantsExit())
			SetMessage(Message_Quit);

		Render();

		return fwFsm::Continue;
	}

	fwFsm::Return PausedGame_OnUpdate()
	{
		PF_FRAMEINIT_TIMEBARS();

		Paused();

		if (WantsExit())
			SetMessage(Message_Quit);

		Render();

		return fwFsm::Continue;
	}

	fwFsm::Return RunGame_OnExit() {
		gRenderThreadInterface.Flush();
		return fwFsm::Continue;
	}

	fwFsm::Return ShutdownGame_OnUpdate() {
		
		gRenderThreadInterface.Shutdown();		// halt render thread and return d3d device to this thread

		gDrawListMgr->Shutdown();
		delete gDrawListMgr;

		delete gDCBuffer;

		SetState(State_ShutdownSystem);
		return fwFsm::Continue;
	}

	fwFsm::Return ShutdownSystem_OnUpdate() {

		m_Setup->DestroyFactories();

		delete m_Viewport;

		rage::INPUT.End();

		GRCDEVICE.DestroyVertexDeclaration(m_GridDecl);
		m_Setup->EndGfx();
		m_Setup->Shutdown();

		delete m_Setup;

		SHUTDOWN_PARSER;

		pgStreamer::ShutdownClass();

		return fwFsm::Quit;
	}
	
	virtual fwFsm::Return UpdateState(const s32 state, const s32, const fwFsm::Event);
};


const char* fwSampleBase::GetFullAssetPath() const
{
	return ASSET.GetPath();
}

void fwSampleBase::SetFullAssetPath(const char* fullPath)
{
	if (fullPath)
	{
		char tempPath[RAGE_MAX_PATH];
		safecpy(tempPath,fullPath,sizeof(tempPath));
		StringNormalize(tempPath,tempPath,sizeof(tempPath));
		char *assets = strstr(tempPath,"/assets/");
		char *lastAssets = NULL;
		while ( assets )
		{
			lastAssets = assets;
			assets = strstr( (assets + 7), "/assets/" );
		}

		if ( lastAssets ) {
			lastAssets[7] = '\0';
			ASSET.SetPath(tempPath);
		}
	}
}

void fwSampleBase::InitCamera()
{
	if(m_UseZUp)
	{
		m_OrthoCam.SetUseZUp(true);
		m_OrthoCam.InitViews();
	}
	else
	{
		m_OrthoCam.InitViews();
	}
}


void fwSampleBase::AddWidgets()
{
	{
		bkBank& bank = BANKMGR.CreateBank("Sample");

		static const char* viewModeList[] = {"Top", "Bottom", "Left", "Right", "Front", "Back"};
		bank.AddCombo("Views", &m_ViewMode, 6, viewModeList);
	}

	{
		bkBank& bank = BANKMGR.CreateBank("Debug");
		fwDebugDraw::AddWidgets(bank);
		gDCBuffer->AddWidgets(bank);
	}
}

void fwSampleBase::SetupCamera()
{
	Matrix34 mtx;

	{
		mtx = m_OrthoCam.GetWorldMatrix();

		float orthoZoom = m_OrthoCam.GetOrthoZoom();

		int scWidth = GRCDEVICE.GetWidth();
		int scHeight = GRCDEVICE.GetHeight();
		float aspectAdjust = 0.0f;

		if (scWidth >= scHeight)
		{
			aspectAdjust = ((float)scWidth / (float)scHeight);
			m_Viewport->Ortho((-orthoZoom*aspectAdjust), (orthoZoom*aspectAdjust), -orthoZoom, orthoZoom, m_OrthoCam.GetNearZ(), m_OrthoCam.GetFarZ());
		}
		else
		{
			aspectAdjust = ((float)scHeight / (float)scWidth);
			m_Viewport->Ortho(-orthoZoom, orthoZoom, (-orthoZoom*aspectAdjust), (orthoZoom*aspectAdjust), m_OrthoCam.GetNearZ(), m_OrthoCam.GetFarZ());
		}
	}

	m_Viewport->SetCameraMtx(mtx);
	m_Viewport->SetWorldMtx(M34_IDENTITY);

	// grcViewport::SetCurrent(m_Viewport);
}

void fwSampleBase::UpdateCamera() 
{
	grcSampleOrthoCam::OrthoMode mode = static_cast<grcSampleOrthoCam::OrthoMode>(m_ViewMode - 1);
	m_OrthoCam.SetOrthoMode(mode);
	m_OrthoCam.Update();
}

const Matrix34& fwSampleBase::GetCameraMatrix() const 
{
	return m_OrthoCam.GetWorldMatrix();
}

void fwSampleBase::DrawGrid(const Vector3& normal, const Vector3& tangent)
{
	const float FLOOR_HEIGHT = 0.0f;

	grcViewport::SetCurrentWorldIdentity();

	Color32 color(0.0f,0.5f,1.0f,0.3f);

	grcBindTexture(NULL);
	grcState::SetDepthWrite(false);

	Color32 colorAxes(0.3f, 0.8f, 1.0f, 0.8f);

	Color32 subGridColor(0.0f,0.2f,0.7f,0.3f);

	Vector3 binormal;
	binormal.Cross(tangent, normal);

	grcNormal3f(0.0f, 1.0f, 0.0f);

	float halfSize = m_GridSize / 2.0f;

	// draw the major axis lines
	Vector3 basePoint, v0, v1;
	basePoint.Scale(normal, FLOOR_HEIGHT);

	struct GridVtx { float x, y, z; u32 c; float t,u ;};

	GRCDEVICE.SetDefaultEffect(false,false);
	GRCDEVICE.SetVertexDeclaration(m_GridDecl);

	v0=basePoint;
	v1=basePoint;
	v0.AddScaled(binormal, -halfSize);
	v1.AddScaled(binormal, halfSize);
	GridVtx *majorAxis = (GridVtx*) GRCDEVICE.BeginVertices(drawLineStrip, 2, sizeof(GridVtx));
	if (majorAxis) {
		majorAxis[0].x=v0.x; majorAxis[0].y=v0.y; majorAxis[0].z=v0.z; majorAxis[0].c=colorAxes.GetDeviceColor();
		majorAxis[1].x=v1.x; majorAxis[1].y=v1.y; majorAxis[1].z=v1.z; majorAxis[1].c=colorAxes.GetDeviceColor();
		GRCDEVICE.EndVertices();
	}

	v0=basePoint;
	v1=basePoint;
	v0.AddScaled(tangent, -halfSize);
	v1.AddScaled(tangent, halfSize);

	GridVtx *minorAxis = (GridVtx*) GRCDEVICE.BeginVertices(drawLineStrip, 2, sizeof(GridVtx));
	if (minorAxis) {
		minorAxis[0].x=v0.x; minorAxis[0].y=v0.y; minorAxis[0].z=v0.z; minorAxis[0].c=colorAxes.GetDeviceColor();
		minorAxis[1].x=v1.x; minorAxis[1].y=v1.y; minorAxis[1].z=v1.z; minorAxis[1].c=colorAxes.GetDeviceColor();
		GRCDEVICE.EndVertices();
	}

	int lineVtxCount = (int)((halfSize / m_GridLines)*2);

	int subLineVtxCount = 0;
	float subLineStep = 0.f;

	if(m_GridSubLine)
	{
		subLineVtxCount = (int)(((m_GridSubLine-1.f)*2.f) * (halfSize / m_GridLines));
		subLineStep = m_GridLines / m_GridSubLine;
	}

	// draw the z lines...
	grcBegin(drawLines, (lineVtxCount+subLineVtxCount)*2);
	for(float x=0.0f; x<(halfSize+m_GridLines); x+=m_GridLines)
	{
		if(x != 0.0f)
		{
			grcColor(color);

			v0 = basePoint;
			v0.AddScaled(tangent, x);
			v0.AddScaled(binormal, -halfSize);
			grcVertex3f(v0);

			v1 = basePoint;
			v1.AddScaled(tangent, x);
			v1.AddScaled(binormal, halfSize);
			grcVertex3f(v1);

			grcVertex3f(-v0);
			grcVertex3f(-v1);
		}

		if(x<halfSize)
		{
			grcColor(subGridColor);
			for(float xs=1; xs < m_GridSubLine; xs+=1.0f)
			{
				v0 = basePoint;
				v0.AddScaled(tangent, x+(xs*subLineStep));
				v0.AddScaled(binormal, -halfSize);
				grcVertex3f(v0);

				v1 = basePoint;
				v1.AddScaled(tangent, x+(xs*subLineStep));
				v1.AddScaled(binormal, halfSize);
				grcVertex3f(v1);

				grcVertex3f(-v0);
				grcVertex3f(-v1);
			}
		}
	}
	grcEnd();

	//draw the x lines...
	grcBegin(drawLines, (lineVtxCount+subLineVtxCount)*2);
	for(float x=0.0f; x<(halfSize+m_GridLines); x+=m_GridLines)
	{
		if(x!=0.0f)
		{
			grcColor(color);

			v0 = basePoint;
			v0.AddScaled(tangent, -halfSize);
			v0.AddScaled(binormal, x);
			grcVertex3f(v0);

			v1 = basePoint;
			v1.AddScaled(tangent, halfSize);
			v1.AddScaled(binormal, x);
			grcVertex3f(v1);

			grcVertex3f(-v0);
			grcVertex3f(-v1);
		}

		if(x<halfSize)
		{
			grcColor(subGridColor);
			for(float xs=1; xs < m_GridSubLine; xs+=1.0f)
			{
				v0 = basePoint;
				v0.AddScaled(tangent, -halfSize);
				v0.AddScaled(binormal, x+(xs*subLineStep));
				grcVertex3f(v0);

				v1 = basePoint;
				v1.AddScaled(tangent, halfSize);
				v1.AddScaled(binormal, x+(xs*subLineStep));
				grcVertex3f(v1);

				grcVertex3f(-v0);
				grcVertex3f(-v1);
			}
		}
	}
	grcEnd();

	grcState::SetDepthWrite(true);
}


bool fwSampleBase::WantsExit()
{
	const bool oneFrame = PARAM_oneframe.Get();
#if IS_CONSOLE && !__FINAL
	const bool weWantExit = (ioPad::GetPad(0).GetDebugButtons() & ioPad::SELECT)!=0;
#else
	const bool weWantExit = false;
#endif
	if (m_WantsExit)
		return true;
	else 
		return m_WantsExit=m_Setup->WantExit() || oneFrame || weWantExit;
}


void fwSampleBase::Update()
{
	PF_PUSH_TIMEBAR("Update");

	fwDebugDraw::Update();

	m_Setup->BeginUpdate();

	rage::INPUT.Update();

	if (ioKeyboard::KeyPressed(KEY_P))
		SetState(State_PausedGame);

	TIME.Update();

	UpdateCamera();

	m_Setup->EndUpdate();

	m_Setup->SetClearColor(m_ClearColor);

	PF_POP_TIMEBAR();
}

void fwSampleBase::Paused()
{
	PF_PUSH_TIMEBAR("Paused");

	fwDebugDraw::AddDebugOutput("Paused");

	fwDebugDraw::Update();

	m_Setup->BeginUpdate();

	rage::INPUT.Update();
	if (ioKeyboard::KeyPressed(KEY_P))
		SetMessage(Message_Unpause);

	TIME.Update();

	m_Setup->EndUpdate();

	m_Setup->SetClearColor(m_ClearColor);

	PF_POP_TIMEBAR();
}
 
void fwSampleBase::Render()
{
	gRenderThreadInterface.GPU_IdleSection();		// call stuff which needs done when GPU is idle

	// Pre
	DLC( dlCmdNewDrawList, (DL_RENDERPHASE_START) );
	DLC( dlCmdBeginDraw, (m_Setup, false) );
	gDrawListMgr->EndCurrentDrawList();

	//
	// Update
	//
	DLC( dlCmdNewDrawList, (DL_RENDERPHASE_UPDATE) );
	DLC_Add(grcEffect::ResetAllGlobalsToDefaults);

	// Make a copy of the object to create a double buffer
	fwSampleBase* clone = static_cast<fwSampleBase*>(gDCBuffer->AddSharedDataBlock(DL_MEMTYPE_COMMAND_HELPER, this, sizeof(fwSampleBase)));

	// Store calls to the buffered copy's member functions.
	DLC_Delegate(void(void), (clone, &fwSampleBase::SetupCamera));

	DLC( dlCmdSetCurrentViewport, (m_Viewport));

	DLC( dlCmdClearRenderTarget, (true,m_Setup->GetClearColor(),true,1.0f,0,0));

	DLC_Add(grcState::Default);
	DLC_Add(grcState::SetLightingMode, (grclmNone));
	DLC_Add(grcState::SetDepthTest, (false));
	DLC_Add(grcState::Flush);
	// Super sexy
	DLC_Delegate(void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleBase::DrawGrid), Vector3(0.0f, 0.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f));

	if (m_DrawGrid || m_DrawThreeGrids)
	{
		// PIX_AUTO_TAG(0, "Grid");

		if (m_DrawThreeGrids)
		{
			DLC_Delegate(void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleBase::DrawGrid), Vector3(1.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));
			DLC_Delegate(void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleBase::DrawGrid), Vector3(0.0f, 1.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f));
			DLC_Delegate(void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleBase::DrawGrid), Vector3(0.0f, 0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f));
		}
		else if(m_UseZUp)
		{
			DLC_Delegate(void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleBase::DrawGrid), Vector3(0.0f, 0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f));
		}
		else
		{
			DLC_Delegate(void(const Vector3& normal, const Vector3& tangent), (clone, &fwSampleBase::DrawGrid), Vector3(0.0f, 1.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f));
		}
	}

	DLC_Delegate(void(void), (clone, &fwSampleBase::DrawScene));

	DLC_Add(fwDebugDraw::Render2D);
	DLC_Add(fwDebugDraw::PrintBufferedStringsToScreen);
	DLC_Add(Timebars);

	DLC_Add(grcState::SetDepthTest,(true));
	DLC_Add(grcState::Flush);

	DLC_Add(grcState::Default);	
	DLC_Add(grcState::Flush);
	DLC_Add(grcWorldIdentity);

	gDrawListMgr->EndCurrentDrawList();

	// Post
	DLC( dlCmdNewDrawList, (DL_RENDERPHASE_END) );
	DLC( dlCmdInsertEndFence, () );
	DLC( dlCmdEndDraw, (m_Setup) );
	DLC_Add(Zeno);
	gDrawListMgr->EndCurrentDrawList();

	gRenderThreadInterface.Synchronise();
}

void fwSampleBase::DrawScene()
{
	// STEP #1. Set our transformation matrix.

	//-- Set the matrix that gets applied to all subsequent geometry.
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

	// STEP #2. Setup our graphics state.

	//-- We don't want texturing 
	grcBindTexture(NULL);

	int passCount = 1;
	for (int passes=0; passes<passCount; passes++) {
		// STEP #3. Draw a set of points.

		// -- Here we draw 25 white colored points. 
		// 
		// To start drawing a set of primitives, you must call <c>grcBegin()</c>.  You 
		// must specify the primitive type (in this case, <c>drawPoints</c>), and the vertex
		// count (in this case, 25 points.)  The number of vertices must be at least 1.
		grcBegin(drawPoints,5*5);

		// -- Here's where we send down the verts.
		for (int i=0;i<5;i++)
			for (int j=0;j<5;j++)
				grcVertex(-45.0f+j*2.0f,-5.0f+i*2.0f,0.0f,0,0,1,Color32(255,255,255),0.0f,0.0f);

		// -- Call <c>grcEnd()</c> to signify we're done drawing our primitives.  <c>grcEnd()</c>
		// will assert if the number of verts in the previous call to <c>grcBegin()</c> does not 
		// match the number of verts sent down.
		grcEnd();

		// STEP #4. Draw a triangle.

		// -- Here we draw a single, multi-colored triangle.  The number of verts must be 
		// <c>numTris*3</c>.
		grcBegin(drawTris,3);
		grcVertex(-30.0f,-5.0f,0.0f,0,1,0,Color32(255,0,0),0.0f,0.0f);
		grcVertex(-20.0f,-5.0f,0.0f,1,0,0,Color32(0,0,255),0.0f,0.0f);
		grcVertex(-25.0f,5.0f,0.0f,-1,0,0,Color32(0,255,0),0.0f,0.0f);
		grcEnd();

		// STEP #5.  Draw a set of disconnected lines.

		// -- Here we draw two green colored disconnected lines.  The number of verts must be
		// <c>numLines*2</c>.  Notice that we set the color once using <c>grcColor()</c> and 
		// send down verts using <c>grcVertex3f()</c>.
		grcBegin(drawLines,4);
		grcNormal3f(0,0,1);
		grcColor(Color32(0,255,0));
		grcVertex3f(-15.0f,-5.0f,0.0f);
		grcVertex3f(-10.0f,-5.0f,0.0f);
		grcVertex3f(-10.0f,5.0f,0.0f);
		grcVertex3f(-15.0f,5.0f,0.0f);
		grcEnd();

		// STEP #6.  Draw a set of connected lines.

		// -- Here we draw 4 red colored connected lines.  The number of verts must be 
		// <c>numLines+1</c>. 
		grcBegin(drawLineStrip,5);
		grcColor(Color32(255,0,0));
		grcVertex3f(-5.0f,-5.0f,0.0f);
		grcVertex3f(0.0f,-5.0f,0.0f);
		grcVertex3f(0.0f,5.0f,0.0f);
		grcVertex3f(-5.0f,5.0f,0.0f);
		grcVertex3f(-5.0f,-5.0f,0.0f);
		grcEnd();

		// STEP #7. Draw a tri-strip primitive.

		// -- Here we draw two multi-colored triangles.  The number of verts must be 
		// <c>NumTriangles+1</c>.
		grcBegin(drawTriStrip,4);
		grcVertex(10.0f,5.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
		grcVertex(5.0f,-5.0f,0.0f,0,0,-1,Color32(0,0,255),0.0f,0.0f);
		grcVertex(20.0f,5.0f,0.0f,0,0,-1,Color32(255,0,0),0.0f,0.0f);
		grcVertex(15.0f,-5.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
		grcEnd();

		// STEP #8. Draw a tri-fan primitive.

		// -- Here we draw two multi-colored triangles.  The number of verts must be 
		// <c>NumTriangles+1</c>.
		grcBegin(drawTriFan,4);
		grcVertex(35.0f,-5.0f,0.0f,0,0,-1,Color32(0,255,255),0.0f,0.0f);
		grcVertex(45.0f,5.0f,0.0f,0,0,-1,Color32(255,255,0),0.0f,0.0f);
		grcVertex(35.0f,5.0f+2.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
		grcVertex(25.0f,5.0f,0.0f,0,0,-1,Color32(0,255,0),0.0f,0.0f);
		grcEnd();
	}
}

void fwSampleBase::Draw()
{
	// Don't clear here, want that to be predicated?
	if (!m_Setup->BeginDraw(false))
		return;

	grcEffect::ResetAllGlobalsToDefaults();

	SetupCamera();
	grcViewport::SetCurrent(m_Viewport);

	GRCDEVICE.Clear(true,m_Setup->GetClearColor(),true,1.0f,0);

	grcState::Default();
	grcLightState::SetEnabled(false);
	grcState::SetDepthTest(false);
	

	if (m_DrawGrid || m_DrawThreeGrids)
	{
		// PIX_AUTO_TAG(0, "Grid");

		if (m_DrawThreeGrids)
		{
			DrawGrid(Vector3(1.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));
			DrawGrid(Vector3(0.0f, 1.0f, 0.0f));
			DrawGrid(Vector3(0.0f, 0.0f, 1.0f));
		}
		else if(m_UseZUp)
		{
			DrawGrid(Vector3(0.0f, 0.0f, 1.0f));
		}
		else
		{
			DrawGrid(Vector3(0.0f, 1.0f, 0.0f));
		}
	}

	grcState::SetDepthTest(true);
	

	grcState::Default();	
	
	grcWorldIdentity();

	m_Setup->EndDraw();
}

fwFsm::Return fwSampleBase::UpdateState(const s32 state, const s32 message, const fwFsm::Event event)
{
	fwFsmBegin

		fwFsmState(State_InitSystem)
			fwFsmOnMsg(Message_Quit)
				SetState(State_ShutdownSystem);
			fwFsmOnUpdate
				InitSystem_OnUpdate();

		fwFsmState(State_InitGame)
			fwFsmOnMsg(Message_Quit)
				SetState(State_ShutdownGame);
			fwFsmOnUpdate
				InitGame_OnUpdate();

		fwFsmState(State_RunGame)
			fwFsmOnUpdate
				RunGame_OnUpdate();
			fwFsmOnExit
				RunGame_OnExit();
			fwFsmOnMsg(Message_Quit)
				SetState(State_ShutdownGame);
			fwFsmOnMsg(Message_Pause)
				SetState(State_PausedGame);

		fwFsmState(State_PausedGame)
			fwFsmOnUpdate
				PausedGame_OnUpdate();
			fwFsmOnMsg(Message_Unpause)
				SetState(State_RunGame);
	
		fwFsmState(State_ShutdownGame)
			fwFsmOnUpdate
				ShutdownGame_OnUpdate();
			fwFsmOnExit
				Displayf("Goodbye");

		fwFsmState(State_ShutdownSystem)
			fwFsmOnUpdate
				ShutdownSystem_OnUpdate();
				return Quit;

	fwFsmEnd
}

fwSampleBase s_app;


// main application
int Main(void){

	s_app.Run();

	return 0;
}