// 
// /sample_gta_viewer.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_rmcore/sample_rmcore.h"

#include "devcam/cammgr.h"
#include "file/asset.h"
#include "file/device_relative.h"
#include "file/packfile.h"

#include "fragment/type.h"

#include "grcore/texture.h"
#include "rmcore/drawable.h"

#include "system/main.h"
#include "system/param.h"
#include "system/typeinfo.h"

#include "fwsys/fileExts.h"
#include "fwtl/assetstore.h"

using namespace rage;

PARAM(file,"[sample_gta_viewer] Toplevel datafile to load (default: common:/data/gta.dat)");
XPARAM(shaderlibs);
XPARAM(shaderdb);

#define ROOT "x:/jimmy/build/dev/"

RAGE_DEFINE_CHANNEL(streaming)


class gtaSample: public ragesamples::rmcSampleManager 
{
	struct Type
	{
		Vector4 Center;
		u32 Hash;
		s16 DrIndex;
		s16 TdIndex;
		s16 DdIndex;
		s16 FtIndex;
	};
	struct Instance
	{
		Matrix34 Mtx;
		int TypeIndex;
	};
public:
	gtaSample()
		: m_TdStore("TxdStore", TXD_FILE_EXT_PATTERN, 5500 /*TXD_STORE_POOL_SIZE*/, true, TXD_FILE_VERSION)
		, m_DdStore("DwdStore", DWD_FILE_EXT_PATTERN, 1800 /*DWD_STORE_POOL_SIZE*/, true, DWD_FILE_VERSION)
		// , m_FtStore(800,MODULE_FRAGTYPE)
		, m_DrStore("DrawableStore", DRAWABLE_FILE_EXT_PATTERN, 20000, true, DRAWABLE_FILE_VERSION)
	{
		// Might as well force the correct GTA shader paths here.
		PARAM_shaderlibs.Set(ROOT "common/shaders");
		PARAM_shaderdb.Set(ROOT "common/shaders/db");
	}

	virtual const char *GetSampleName() const
	{
		return "sample_gta_viewer";
	}

	static bool MakeSpaceFor(s32 /*vSize*/,s32 /*pSize*/)
	{
		return false;
	}

	void InitClient()
	{
		// Initialize the streamer
		pgStreamer::InitClass(256);

		// Setup proper GTA relative devices so files load correctly.
		m_Common.Init(ROOT "common/");
		m_Common.MountAs("common:/");

		m_CommonImg.Init(ROOT "common/");
		m_CommonImg.MountAs("commonimg:/");

		m_Platform.Init(ROOT "independent/");
		m_Platform.MountAs("platform:/");

		m_PlatformImg.Init(__PPU? ROOT "ps3/" : ROOT "xbox360/");
		m_PlatformImg.MountAs("platformimg:/");

		const char *datFile = "common:/data/levels/alpine/alpine.dat";
		// const char *datFile = "common:/data/animviewer.dat";
		PARAM_file.Get(datFile);

		// Load the toplevel world definition file
		// LoadDatFile(datFile);

		strStreamingEngine::Init(MakeSpaceFor);

		Displayf("%d instances, %d types, %d drawables, %d drawable dicts, %d tex dicts loaded",
			m_Instances.GetCount(),m_Types.GetCount(),m_DrStore.GetCount(),m_DdStore.GetCount(),m_TdStore.GetCount());

		m_LightMode = grclmNone;
	}

	void InitCamera()
	{
		SetZUp(true);
		GetCamMgr().SetUseZUp(true);
		GetCamMgr().SetCamera(dcamCamMgr::CAM_ROAM);
	}

#if 0
	void LoadDatFile(const char *filename)
	{
		fiSafeStream S(ASSET.Open(filename,"dat"));
		if (!S)
			Quitf("Unable to open '%s'",filename);
		char linebuf[256];
		while (fgetline(linebuf,sizeof(linebuf),S))
		{
			if (linebuf[0]=='#' || linebuf[0]==0)
				continue;
			const char *verb = strtok(linebuf," \t");
			const char *noun = strtok(NULL," \t");
			if (!strcmp(verb,"IMGLIST"))
			{
				fiSafeStream L(ASSET.Open(noun,""));
				if (!L)
					Quitf("Unable to open imglist '%s'",noun);
				char linebuf[256];
				while (fgetline(linebuf,sizeof(linebuf),L))
				{
					if (linebuf[0]=='#' || linebuf[0]==0)
						continue;
					const char *img = strtok(linebuf," \t");
					// ignore the loadflag
					strcat(linebuf,".img");		// cheesy but will work because we already consumed loadFlag
					LoadImgFile(img);
				}
				Displayf("Done processing IMGLIST.");
			}
			else if (!strcmp(verb,"IMG"))
				LoadImgFile(noun);
			else if (!strcmp(verb,"IDE"))
				LoadIdeFile(noun);
			else if (!strcmp(verb,"IPL"))
				LoadIplFile(noun);
			else
				Warningf("Ignoring unknown verb '%s'",verb);
		}
	}

	void LoadImgFile(const char *imgFile)
	{
		ImgDevice::AddImgFile(imgFile);
	}

	void LoadIdeFile(const char *ideFile)
	{
		enum IDELoadingStatus {
			LOADING_NOTHING,
			LOADING_OBJECTS,
			LOADING_MLO,
			LOADING_MLO_INST,
			LOADING_MLO_ROOM,
			LOADING_MLO_ROOM_OBJLIST,
			LOADING_MLO_PORTALS,
			LOADING_TIMEOBJS,
			LOADING_WEAPONS,
			LOADING_CLUMP,
			LOADING_ANIMATED_CLUMP,
			LOADING_VEHICLES,
			LOADING_PEDS,
			LOADING_TREES,
			LOADING_2DEFFECTS,
			LOADING_TXDPARENTS,
			LOADING_PATHNODES,
			LOADING_PATHLINKS,
			LOADING_AUDIO_MATS,
		};

		fiSafeStream S(ASSET.Open(ideFile,""));
		if (!S)
			Quitf("Cannot open IDE file '%s'",ideFile);
		char linebuf[256];
		IDELoadingStatus status = LOADING_NOTHING;
		while (fgetline(linebuf,sizeof(linebuf),S))
		{
			// Remove commas
			char *comma;
			while ((comma = strchr(linebuf,',')) != NULL)
				*comma = ' ';
			// Check for section changes.
			if (!strcmp(linebuf,"end"))
				status = LOADING_NOTHING;
			else if (status == LOADING_NOTHING)
			{
				if (!strcmp(linebuf,"objs"))
					status = LOADING_OBJECTS;
				else if (!strcmp(linebuf,"tobj"))
					status = LOADING_TIMEOBJS;
				else if (!strcmp(linebuf,"txdp"))
					status = LOADING_TXDPARENTS;
			}
			else if (status == LOADING_OBJECTS || status == LOADING_TIMEOBJS)
			{
				const int MODEL_NAME_LEN = 24;
				char name[MODEL_NAME_LEN];
				char txdName[MODEL_NAME_LEN];
				char ddName[MODEL_NAME_LEN];
				float lodDist;
				Vector3 bbMin(0.0f, 0.0f, 0.0f);
				Vector3 bbMax(0.0f, 0.0f, 0.0f);
				Vector3 bsCentre(0.0f, 0.0f, 0.0f);
				float bsRadius = 0.0f;
				u32 flags=0;
				float specialAttribute=0.0f;		// This is a float just now but should be changed into an uint32 once the IDEs have been re-exported.
				int numParams;

				ddName[0] = '\0';

				// Object types now have an extra field 'attribute' that is an exclusive attribute that the specifies whether
				// a type is a garage door/clothing rack in a shop etc. This makes some of the bits in the flags field obsolete.
				// To make sure things still work in the transition phase we deal with 15 AND 16 parameters.
				// (timeobjs have an extra number at the end that we ignore)
				numParams = sscanf(linebuf, "%s %s %f %d %f %f %f %f %f %f %f %f %f %f %f %s",
					&name[0], &txdName[0], &lodDist, &flags, &specialAttribute,
					&bbMin.x, &bbMin.y, &bbMin.z,
					&bbMax.x, &bbMax.y, &bbMax.z,
					&bsCentre.x, &bsCentre.y, &bsCentre.z,
					&bsRadius, &ddName[0]);
				if (numParams != 16)
					Quitf("Old IDE file format found: [%s] %d",linebuf,numParams);
				// Displayf("%s.%s.%s %x %.f",name,txdName,ddName,flags,specialAttribute);

				s16 tdIndex = strcmp(txdName,"null")? m_TdStore.Add(txdName) : -1;
				s16 ddIndex = strcmp(ddName,"null")? m_DdStore.Add(ddName) : -1;
				s16 drIndex = ddIndex==-1? m_DrStore.Add(name) : -1;
				s16 ftIndex = drIndex==-1 && ddIndex==-1? m_FtStore.Add(name) : -1;
				ftIndex = -1;	// HACK - fragtypes don't load right now.
				// Make sure we found either a drawable dict, a drawable, or a fragment.
				/* Assert((ddIndex != -1 && drIndex == -1 && ftIndex == -1)
					|| (ddIndex == -1 && (drIndex != -1 || ftIndex != -1))); */

				Type &type = m_Types.Grow();
				type.Hash = atStringHash(name);
				type.Center.Set(bsCentre.x,bsCentre.y,bsCentre.z,bsRadius);
				type.DrIndex = drIndex;
				type.TdIndex = tdIndex;
				type.DdIndex = ddIndex;
				type.FtIndex = ftIndex;

				Streamable *dependent = NULL;
				if (ddIndex != -1 && tdIndex != -1)
				{
					// Drawable dict depends on tex dict
					m_DdStore.GetStreamable(ddIndex).SetParent(&m_TdStore.GetStreamable(tdIndex));
				}
				if (ddIndex != -1)
					dependent = &m_DdStore.GetStreamable(ddIndex);
				else if (tdIndex != -1)
					dependent = &m_TdStore.GetStreamable(tdIndex);
				if (drIndex != -1)
					m_DrStore.GetStreamable(drIndex).SetParent(dependent);
				if (ftIndex != -1)
					m_FtStore.GetStreamable(ftIndex).SetParent(dependent);
			}
			else if (status == LOADING_TXDPARENTS)
			{
				char txdName[25], txdParentName[25];
				sscanf(linebuf,"%s %s",txdName,txdParentName);
				s16 txd = m_TdStore.Add(txdName);
				if (txd != -1)
				{
					if (strcmp(txdParentName,"root"))
					{
						s16 txdParent = m_TdStore.Add(txdParentName);
						Assert(txdParent != -1);
						m_TdStore.GetStreamable(txd).SetParent(&m_TdStore.GetStreamable(txdParent));
					}
				}
				else
					Errorf("Unable to find texture dictionary '%s' anywhere.",txdName);
			}
		}
	}

	void LoadIplFile(const char *iplFile)
	{
		enum IPLLoadingStatus {
			LOADING_NOTHING,
			LOADING_IGNORE,
			LOADING_INSTANCES,
			LOADING_MULTIBUILDINGS,
			LOADING_ZONES,
			LOADING_MAPZONES,
			LOADING_CULLZONES,
			LOADING_OCCLUSION,
			LOADING_GARAGE,
			LOADING_ENTRYEXIT,
			LOADING_PICKUPS,
			LOADING_CARGENERATORS,
			LOADING_STUNTJUMPS,
			LOADING_TIMECYCLEMODS,
			LOADING_AUDIOZONES,
			LOADING_PATHNODES,
			LOADING_PATHLINKS,
			LOADING_BLOCKS,
			LOADING_MLOAPPEND,
			LOADING_2DEFFECTS
		};

		fiSafeStream S(ASSET.Open(iplFile,""));
		if (!S)
			Quitf("Cannot open IPL file '%s'",iplFile);
		Displayf("Parsing instances from '%s'",iplFile);
		char linebuf[256];
		IPLLoadingStatus status = LOADING_NOTHING;
		while (fgetline(linebuf,sizeof(linebuf),S))
		{
			// Remove commas
			char *comma;
			while ((comma = strchr(linebuf,',')) != NULL)
				strcpy(comma,comma+1);
			// Check for section changes.
			if (!strcmp(linebuf,"end"))
				status = LOADING_NOTHING;
			else if (status == LOADING_NOTHING)
			{
				if (!strcmp(linebuf,"inst"))
					status = LOADING_INSTANCES;
			}
			else if (status == LOADING_INSTANCES)
			{
				char typeName[24];
				int flags;
				Vector3 pos;
				Quaternion quat;
				int lodIndex, blockIndex;
				float lodDistance;
				int numParams = sscanf(linebuf,"%s %d  %f %f %f  %f %f %f %f  %d %d %f",
					typeName,&flags,&pos.x,&pos.y,&pos.z,&quat.x,&quat.y,&quat.z,&quat.w,&lodIndex,&blockIndex,&lodDistance);
				if (numParams != 12)
					Quitf("Old IPL file found: [%s] %d",linebuf,numParams);

				Instance &inst = m_Instances.Grow();
				inst.Mtx.FromQuaternion(quat);
				inst.Mtx.d.Set(pos);
				u32 hash = atStringHash(typeName);
				int i = 0;
				for (i=0; i<m_Types.GetCount(); i++)
					if (m_Types[i].Hash == hash)
						break;
				if (i == m_Types.GetCount())
				{
					Errorf("Unknown instance %s",typeName);
					m_Instances.Pop();
				}
				else
					inst.TypeIndex = i;
			}
		}
	}
#endif

#if 0
	bool FoundOwner(const sysMemDefragmentation &defrag,int i,Streamable *j)
	{
		pgBase *p = j->GetPointer();
		int ptrSlot = p->MapContainsPointer(defrag.Nodes[i].From);
		if (ptrSlot != -1)
		{
			// Regenerate the entire map from compressed store
			datResourceMap defragMap;
			p->RegenerateMap(defragMap);

			// Disconnect the object from any global lists before potentially moving parts of them
			p->BeforeCopy();

			// Fix the destination address of the portion we're moving
			// TODO: Could identify multiple nodes in same allocation and do the work at once.
			defragMap.Chunks[ptrSlot].DestAddr = defrag.Nodes[i].To;
			// Make sure the size we think the node is and the size the buddy heap think it is match.
			Assert(defrag.Nodes[i].Size == defragMap.Chunks[ptrSlot].Size);

			// Perform the copy (intentionally leave the source unmolested)
			memcpy(defrag.Nodes[i].To, defrag.Nodes[i].From, defrag.Nodes[i].Size);

			// Mark previous location of chunk as having no owner
			Streamable::SetHeapPageOwner(defrag.Nodes[i].From, NULL);

			// Mark new location of chunk as having this owner.
			Streamable::SetHeapPageOwner(defrag.Nodes[i].To, j);

			// Reinvoke the resource constructor so that this object knows its new state
			j->Defragment(defragMap);

			// Now go through everything else and patch any outstanding references. This is a heinous N*N loop!
			// You should not patch the object you just defragmented.  It ought to be harmless because
			// the pointer will have already been adjusted out of the source range and will not be
			// adjusted again.  Patching is defined as a separate operation from resource construction because
			// objects typically have many fewer external references than internal pointers.  Also, by defining
			// them as separate objects, non-resourced game-level objects can participate in patching as well.
			datResourcePatch patch(defragMap.Chunks[ptrSlot]);
			for (Streamable *k = Streamable::GetFirstResident(); k; k=k->GetNext())
				if (k != j)
					k->PatchReferences(patch);
			return true;
		}
		return false;
	}

	void UpdateClient()
	{
		// Already something in progress.
		if (m_Pending != pgStreamer::Error)
			return;

		// Locate next best candidate
		const Vector3 &camPos = GetCameraMatrix().d;
		float bestDist2 = 200.0f * 200.0f;		// Don't bother with anything further away than this
		float worstDist2 = 0;
		Streamable *bestAsset = NULL, *worstAsset = NULL;
		for (int i=0; i<m_Instances.GetCount(); i++)
		{
			float dist2 = m_Instances[i].Mtx.d.Dist2(camPos);
			Type &type = m_Types[m_Instances[i].TypeIndex];
			Streamable *asset = (type.DrIndex != -1)? &m_DrStore.GetStreamable(type.DrIndex) : (type.FtIndex != -1)? &m_FtStore.GetStreamable(type.FtIndex) : (type.DdIndex != -1)? &m_DdStore.GetStreamable(type.DdIndex) : NULL;
			if (asset)
			{
				if (dist2 < bestDist2 && asset->GetState() == Streamable::NONRESIDENT)
				{
					bestAsset = asset;
					bestDist2 = dist2;
				}
				else if (m_OutOfMemory && dist2 > worstDist2 && asset->GetState() == Streamable::RESIDENT)
				{
					worstDist2 = dist2;
					worstAsset = asset;
				}
			}
		}

		// If there's something that needs to be resident, schedule it.
		m_OutOfMemory = false;
		if (bestAsset)
		{
			// Identify the lowest-level asset in dependency chain that isn't resident yet.
			while (bestAsset->GetParent() && bestAsset->GetParent()->GetState() == Streamable::NONRESIDENT)
				bestAsset = bestAsset->GetParent();
			datResourceInfo info;
			int rscVer = bestAsset->GetResourceInfo(info);

			if (!pgRscBuilder::ValidateMap("file",m_PendingMap,info))
			{
				sysMemAllocator &alloc = *sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
				sysMemDefragmentation defrag;
				if (alloc.BeginDefragmentation(defrag))
				{
					for (int i=0; i<defrag.Count; i++)
						AssertVerify(FoundOwner(defrag,i,Streamable::GetHeapPageOwner(defrag.Nodes[i].From)));
					alloc.EndDefragmentation();

					// Try to allocate again after the defrag is done
					m_OutOfMemory = !pgRscBuilder::ValidateMap("file",m_PendingMap,info);
					if (!m_OutOfMemory)
						Displayf("Defragmentation succeeded!  Allocation now worked.");
					else
						Displayf("Defragmentation didn't free up enough memory.");
				}
				else
				{
					Displayf("Out of memory and nothing to defragment!");
					m_OutOfMemory = true;
				}
			}
			if (!m_OutOfMemory)
			{
				char buf[32];
				formatf(buf,sizeof(buf),"stream:/%u",bestAsset->GetLocation());
				m_Pending = pgStreamer::Open(buf, NULL, rscVer);
				Assert(m_Pending != pgStreamer::Error);

				if (pgStreamer::Read(m_Pending, m_PendingMap.Chunks,m_PendingMap.PhysicalCount+m_PendingMap.VirtualCount,
					sizeof(datResourceFileHeader),ReadCompleteStub,this)) 
				{
					m_PendingAsset = bestAsset;
					bestAsset->SetState(Streamable::PENDING);
				}
				else
				{
					pgStreamer::Close(m_Pending);
					m_Pending = pgStreamer::Error;
				}
			}
		}

		// If there's something that is far away, kill it.
		if (worstAsset)
		{
			// Only leaf assets should show up here.
			Assert(worstAsset->GetRefCount() == 0);
			worstAsset->SetState(Streamable::LEAVING);
		}


		// Anything already on the death list will count down before dying.
		Streamable *deathRow = Streamable::GetFirstLeaving();
		while (deathRow)
		{
			// Copy the next ptr before calling Release since the state may change and modify the next ptr.
			Streamable *nextToDie = deathRow->GetNext();
			deathRow->Release();
			deathRow = nextToDie;
		}
	}
#endif

	void DrawDrawable(const Instance &inst,rmcDrawable *drawable)
	{
		if (drawable)
		{
			u8 lod;
			if (drawable->IsVisible(inst.Mtx,*grcViewport::GetCurrent(),lod,NULL))
				drawable->Draw(inst.Mtx,0,lod);
		}
	}

	void DrawClient()
	{
		for (int i=0; i<m_Instances.GetCount(); i++)
		{
			Type &type = m_Types[m_Instances[i].TypeIndex];
			if (type.DrIndex != -1 && m_DrStore.Get(type.DrIndex))
				DrawDrawable(m_Instances[i],m_DrStore.Get(type.DrIndex));
			else if (type.DdIndex != -1 && m_DdStore.Get(type.DdIndex))
				DrawDrawable(m_Instances[i],m_DdStore.Get(type.DdIndex)->Lookup(type.Hash));
		}
	}

private:
	fiDeviceRelative m_Common, m_CommonImg, m_Platform, m_PlatformImg;

	fwAssetRscStore< pgDictionary<grcTexture> > m_TdStore;
	fwAssetRscStore< pgDictionary<rmcDrawable> > m_DdStore;
	// fwAssetRscStore< pgDictionary<fragType> > m_FtStore;
	fwAssetRscStore< rmcDrawable > m_DrStore;
	atArray<Type> m_Types;
	atArray<Instance> m_Instances;
};


int Main()
{
	gtaSample sampleGta;
	sampleGta.Init("x:/jimmy/build/dev");

	sampleGta.UpdateLoop();

	sampleGta.Shutdown();

	return 0;
}
