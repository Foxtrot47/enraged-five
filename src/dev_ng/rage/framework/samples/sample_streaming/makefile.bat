set TESTERS=sample_streaming
set SAMPLE_LIBS=sample_rmcore sample_physics sample_fragment sample_simpleworld
set LIBS=%SAMPLE_LIBS% %RAGE_SAMPLE_GRCORE_LIBS% sample_rmcore %RAGE_GFX_LIBS% %RAGE_CORE_LIBS% %RAGE_CR_LIBS% %RAGE_SUITE_CR_LIBS% %RAGE_AUD_LIBS% spatialdata %RAGE_PH_LIBS%
set LIBS=%LIBS% breakableglass phglass fragment event cloth grrope rmptfx cliptools
set LIBS=%LIBS% %RAGE_DIR%/framework/src/vcproj/RageFramework/RageFramework.vcproj
echo %LIBS%
set XPROJ=%RAGE_DIR%\suite\src %RAGE_DIR%\suite\samples %RAGE_DIR%\base\src %RAGE_DIR%\base\samples %RAGE_DIR%\suite\tools %RAGE_DIR%\framework\src %RAGE_DIR%\suite\tools\cli
