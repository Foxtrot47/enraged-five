// 
// sample_skeleton.cpp 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "sample_skeleton.h"
#include "system/main.h"
#include "system/param.h"

using namespace rage;

PARAM(file,"[sample_gta_viewer] Toplevel datafile to load (default: common:/data/gta.dat)");

ragesamples::rmcSampleManager* CRmcSampleWrapper::m_rmcSample = NULL;

#define INIT_CORE 0
#define SHUTDOWN_CORE 0

sampleSkeleton::sampleSkeleton()
{
	// ------------ REGISTER INIT FUNCTIONS ---------------

	m_skeleton.SetCurrentInitType(INIT_CORE);

	m_skeleton.SetCurrentDependencyLevel(1);

	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitParams,					"Params");

	m_skeleton.SetCurrentDependencyLevel(2);

	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitCamera,					"Camera");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitCameraPresets,				"Camera Presets");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitClient,					"Client");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitCustomLighting,			"Custom Lighting");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitGrcDevice,					"Grc Device");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitInput,						"Input");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitLights,					"Lights");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitLightPresets,				"Light Presets");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitSetup,						"Setup");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitViewport,					"Viewport");

	m_skeleton.SetCurrentDependencyLevel(3);
	
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitGfxFactories,				"Gfx Factories");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitLightManipulators,			"Light Manipulators");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitLogo,						"Logo");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitRenderTarget,				"Render Target");
	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitStats,						"Stats");

	m_skeleton.SetCurrentDependencyLevel(4);

	m_skeleton.RegisterInitSystem(CRmcSampleWrapper::InitDepthBuffer,				"Depth Buffer");

	// ------------ REGISTER SHUTDOWN FUNCTIONS ---------------

	m_skeleton.SetCurrentShutdownType(SHUTDOWN_CORE);

	m_skeleton.SetCurrentDependencyLevel(1);

	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownGrcDevice,			"Grc Device");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownStats,				"Stats");

	m_skeleton.SetCurrentDependencyLevel(2);

	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownCamera,			"Camera");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownClient,			"Client");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownCustomLighting,	"Custom Lighting");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownDepthBuffer,		"Depth Buffer");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownGfxFactories,		"Gfx Factories");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownInput,				"Input");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownLights,			"Lights");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownLightManipulators,	"Light Manipulators");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownLogo,				"Logo");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownRenderTarget,		"Render Target");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownViewport,			"Viewport");

	m_skeleton.SetCurrentDependencyLevel(3);

	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownParams,			"Params");
	m_skeleton.RegisterShutdownSystem(CRmcSampleWrapper::ShutdownSetup,				"Setup");
}

void sampleSkeleton::Init()
{
	m_skeleton.Init(INIT_CORE);
}

void sampleSkeleton::Shutdown()
{
	m_skeleton.Shutdown(INIT_CORE);
}

int Main()
{
	CRmcSampleWrapper::Init();

	sampleSkeleton skeleton;

	skeleton.Init();

	// skeleton code does not handle updates yet
	CRmcSampleWrapper::UpdateLoop();

	skeleton.Shutdown();

	CRmcSampleWrapper::Shutdown();

	return 0;
}

