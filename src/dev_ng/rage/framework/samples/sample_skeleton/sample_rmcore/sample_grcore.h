// 
// sample_grcore\sample_grcore.h 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 

#ifndef SAMPLE_GRCORE_SAMPLE_GRCORE_H
#define SAMPLE_GRCORE_SAMPLE_GRCORE_H

#include "atl/map.h"
#include "atl/string.h"
#include "devcam/cammgr.h"
#include "grcore/font.h"
#include "grcore/light.h"
#include "grcore/setup.h"
#include "grcore/state.h"
#include "grcore/texturedefault.h"
#include "grcore/viewport.h"
#include "sample_file.h"
#include "system/rageroot.h"

//-----------------------------------------------------------------
//Define the XML syntax used in the light preset ".lgroup" files
#define GRCLIGHTGROUP_VERSION				"2"

#define GRCLIGHTGROUP_STRUCTURE				"grclightgroup"
#define GRCLIGHTGROUP_VERSION_ATTR			"version"
#define GRCLIGHTGROUP_MAXLIGHTS_ATTR		"maxlights"
#define GRCLIGHTGROUP_ACTIVECOUNT_ATTR		"activecount"
#define GRCLIGHTGROUP_AMBIENT_ATTR			"ambient"

#define GRCLIGHTGROUP_LIGHTMODE_PARAM		"lightingmode"
#define GRCLIGHTGROUP_DRAWLIGHTS_PARAM		"drawlights"
#define GRCLIGHTGROUP_DRAWLIGHTLABEL_PARAM	"drawlightlables"
#define GRCLIGHTGROUP_DRAWLIGHTRADIUS_PARAM "drawlightradius"
#define GRCLIGHTGROUP_ANIMRADIUS_PARAM		"animradius"
#define GRCLIGHTGROUP_ANIMATELIGHTS_PARAM	"animlights"
#define GRCLIGHTGROUP_ANIMATERATE_PARAM		"animrate"
#define GRCLIGHTGROUP_ORBITTARGET_PARAM		"orbittarget"
#define GRCLIGHTGROUP_FORCECOLOR_PARAM		"forcecolor"

#define GRCLIGHTGROUP_LIGHT_STRUCT_LIST		"lights"
#define GRCLIGHTGROUP_LIGHT_STRUCTURE		"light"
#define GRCLIGHTGROUP_ID_PARAM				"id"
#define GRCLIGHTGROUP_POSITION_PARAM		"position"
#define GRCLIGHTGROUP_DIRECTION_PARAM		"direction"
#define GRCLIGHTGROUP_COLOR_PARAM			"color"
#define GRCLIGHTGROUP_INTENSITY_PARAM		"intensity"
#define	GRCLIGHTGROUP_OVERSATURATE_PARAM	"oversaturate"
#define GRCLIGHTGROUP_NEGATIVE_PARAM		"negative"
#define GRCLIGHTGROUP_FALLOFF_PARAM			"falloff"
#define GRCLIGHTGROUP_TYPE_PARAM			"type"
//-----------------------------------------------------------------
//Define the XML syntax used in the camera preset ".camset" files
#define GRCCAMPRESET_VERSION				"2"

#define GRCCAMPRESET_STRUCTURE				"grccamerapreset"
#define GRCCAMPRESET_VERSION_ATTR			"version"
#define GRCCAMPRESET_CURCAM_PARAM			"curdevcam"
#define GRCCAMPRESET_VIEWMODE_PARAM			"viewmode"
#define GRCCAMPRESET_FOV_PARAM				"fov"
#define GRCCAMPRESET_NEARCLIP_PARAM			"nearclip"
#define GRCCAMPRESET_FARCLIP_PARAM			"farclip"

#define GRCCAMPRESET_DEVCAM_STRUCT_LIST		"devcameras"
#define GRCCAMPRESET_CAMERA_COUNT_ATTR		"count"
#define GRCCAMPRESET_DEVCAM_STRUCTURE		"devcamera"
#define GRCCAMPRESET_CAMTYPE_ATTR			"type"
#define GRCCAMPRESET_WORLDMAT_PARAM			"worldmatrix"
#define GRCCAMPRESET_ORTHOCAM_STRUCT_LIST	"orthocameras"
#define GRCCAMPRESET_ORTHOCAM_STRUCTURE		"orthocam"
#define GRCCAMPRESET_ORTHO_ZOOM_PARAM		"orthozoom"
//-----------------------------------------------------------------

namespace rage {

class gzGizmo;
class parTree;
class parTreeNode;
class XmlLog;

}

//-----------------------------------------------------------------

namespace ragesamples {

using namespace rage;

//-----------------------------------------------------------------
struct grcSampleLightPreset
{
	int				m_LightMode;
	bool			m_DrawLights;
	bool			m_DrawLightLabels;
	bool			m_DrawLightRadii;
	float			m_LightRadius;
	bool			m_AnimateLights;
	float			m_LightAnimateRate;
	bool			m_DirLightOrbitTarget;
	Color32			m_ForceColor;
	grcLightGroup	m_LightGroup;
	atString		m_FileName;
};
//-----------------------------------------------------------------
class grcSampleOrthoCam
{
public:
	enum OrthoMode
	{
		ORTHO_TOP,ORTHO_BOTTOM,ORTHO_LEFT,ORTHO_RIGHT,ORTHO_FRONT,ORTHO_BACK,ORTHO_MODE_COUNT
	};

	grcSampleOrthoCam();

	void			InitViews();

	void			Frame(const spdSphere& frameSphere);

	float			GetNearZ() const { return m_OrthoNearZ; }
	float			GetFarZ() const { return m_OrthoFarZ; }

	float			GetOrthoZoom() const { return m_OrthoZooms[(int)m_OrthoMode]; }
	float			GetOrthoZoom(int idx) const { FastAssert(idx<ORTHO_MODE_COUNT); return m_OrthoZooms[idx]; }

	void			SetOrthoZoom(float zoom) { m_OrthoZooms[(int)m_OrthoMode] = zoom; }
	void			SetOrthoZoom(int idx, float zoom) { FastAssert(idx<ORTHO_MODE_COUNT); m_OrthoZooms[idx] = zoom;}

	const Matrix34&	GetWorldMatrix() const { return m_OrthoMats[(int)m_OrthoMode]; }
	const Matrix34& GetWorldMatrix(int idx) const { FastAssert(idx<ORTHO_MODE_COUNT); return m_OrthoMats[idx]; }

	void			SetWorldMatrix(const Matrix34& mat) { m_OrthoMats[(int)m_OrthoMode] = mat; }
	void			SetWorldMatrix(int idx, const Matrix34& mat) { FastAssert(idx<ORTHO_MODE_COUNT); m_OrthoMats[idx] = mat;}

	void			SetOrthoMode(OrthoMode mode) { m_OrthoMode = mode; }

	void			Update();

	void			SetUseZUp(bool bUseZUp) { m_UseZUp = bUseZUp; }
	bool			GetUseZUp() const { return m_UseZUp; }

private:
	bool			UpdateMouse(float dt);
	bool			UpdatePad(float dt);
	bool			ProcessInputs(bool pan, bool zoom, float x, float y, float dt);

private:
	OrthoMode	m_OrthoMode;
	float		m_OrthoNearZ;
	float		m_OrthoFarZ;
	float		m_SpeedScalar;

	ioMapper	m_Mapper;
	ioValue		m_Left;
	ioValue		m_Right;
	ioValue		m_Up;
	ioValue		m_Down;
	ioValue		m_Zoom;
	ioValue		m_Xaxis;
	ioValue		m_Yaxis;
	ioValue		m_MoveSlowly;
	ioValue		m_MoveQuickly;
	ioValue		m_RequiredKey;

	int			m_LastMouseX;
	int			m_LastMouseY;

	Matrix34	m_OrthoMats[ORTHO_MODE_COUNT];
	float		m_OrthoZooms[ORTHO_MODE_COUNT];

	bool		m_UseZUp;
	Vector3		m_vUp;
};
//-----------------------------------------------------------------

struct grcSampleCameraPreset
{
	rage::dcamCamMgr::eCamera	m_CurrentDevCamera;
	int		m_ViewMode;
	float	m_Fov;
	float	m_ClipNear;
	float	m_ClipFar;

	rage::atMap<rage::dcamCamMgr::eCamera, rage::Matrix34>		m_CamMats;

	rage::atMap<grcSampleOrthoCam::OrthoMode, rage::Matrix34>	m_OrthoMats;
	rage::atMap<grcSampleOrthoCam::OrthoMode, float>			m_OrthoZooms;

	atString m_FileName;
};

class grcCachingTextureFactory : public grcTextureFactoryDefault
{
	virtual grcTexture* Create(const char *filename,TextureCreateParams *params);
	virtual grcTexture* Create(grcImage *image,TextureCreateParams *params);
};

//-----------------------------------------------------------------
//-----------------------------------------------------------------

class grcSampleManager : public filSampleManager
{
public:
	grcSampleManager();
	virtual ~grcSampleManager();

	// must call this right after constructor is called:
	virtual void Init(const char* path=NULL);

	// must call this before the destructor is called:
	virtual void Shutdown();

	// use UpdateLoop() if I'm the highest level tester:
	virtual void UpdateLoop();

	// single step of the update loop, used for more explicit control of the updates
	virtual void UpdateStep();

	// call every frame to update:
	virtual void Update();
	virtual void UpdateClient() {}

	// call every frame to draw:
	virtual void Draw();

	virtual void DrawHelpClient() {}

	// draw passes
	virtual void PreDrawClient() {}
	virtual void PostShadowPreDrawClient() {}
	virtual void DrawClient() {}

	// utility methods for loading/storing light preset
	bool SaveLightPreset(const char* fileName) const;
	bool SaveLightPreset(parTree* pTree, parTreeNode* pRootNode) const;
	static bool LoadLightPreset(const char* fileName, grcSampleLightPreset* pPreset);
	static bool LoadLightPreset(parTreeNode* pRootNode, grcSampleLightPreset* pPreset);
	void SetLightPreset(const grcSampleLightPreset* pLightPreset);
	
	// utility methods for loading/storing camera presets
	bool SaveCameraPreset(const char* fileName) const;
	bool SaveCameraPreset(parTree* pTree, parTreeNode* pRootNode) const;
	bool LoadCameraPreset(const char* fileName, grcSampleCameraPreset* pCamPreset) const;
	bool LoadCameraPreset(parTreeNode* pRootNode, grcSampleCameraPreset* pCamPreset) const;
	void SetCameraPreset(grcSampleCameraPreset *pCamPreset);

	// checks exit state..
	bool WantsExit();
	void SetZUp(bool b){m_UseZUp = b;}
	bool GetZUp(){return m_UseZUp;}

	// override these:
	virtual void InitParams();
	virtual void InitInput();
	virtual void InitSetup();
	virtual void InitGrcDevice();
	virtual void InitViewport();
	virtual void InitCustomLighting();
	virtual void InitGfxFactories();
	virtual void InitLogo();
	virtual void InitStats();
	virtual void InitClient() {}
	virtual void InitCamera();
	virtual void InitCameraPresets();
	virtual void InitLights();
	virtual void InitLightPresets();
	virtual void InitLightManipulators();
	virtual void InitRenderTarget();
	virtual void InitDepthBuffer();

	virtual void ShutdownGrcDevice();
	virtual void ShutdownRenderTarget();
	virtual void ShutdownDepthBuffer();
	virtual void ShutdownCustomLighting();
	virtual void ShutdownLightManipulators();
	virtual void ShutdownStats();
	virtual void ShutdownLogo();
	virtual void ShutdownGfxFactories();
	virtual void ShutdownViewport();
	virtual void ShutdownInput();
	virtual void ShutdownSetup();
	virtual void ShutdownParams();

	virtual void ShutdownLights();
	virtual void ShutdownCamera();
	virtual void ShutdownClient() {}

protected:

	virtual void InitSampleCamera(const Vector3& lookFrom,const Vector3& lookTo);
	virtual void SetLightingGroup(grcLightGroup &lightGroup);

	grcSetup& GetSetup() 
	{
		FastAssert(m_Setup); return *m_Setup;
	}

	virtual grcSetup&	AllocateSetup();

	virtual void UpdateCamera();
	virtual void UpdateLights();

    void UpdateLight( int idx );

	virtual bool DoWeKeepDrawing(int drawIteration)
	{
		return drawIteration==0;
	}

    virtual bool IsPaused()
    {
        return false;
    }

	void SetFog();
	void SetLights();


	void DrawStatsAndInfo();


	inline void EnableBackgroundDraw(bool enable=true);
	inline void EnableGridDraw(bool enable=true);
	inline void EnableGrid3DDraw(bool enable=false);
	inline void EnableDrawHelp(bool enable=true);
	inline void EnableWorldAxesDraw(bool enable=true);
	inline void EnableLogoDraw(bool enable=true);
	inline void EnableCameraDraw(bool enable=true);
	inline void SetClearColor(Color32 color);

	virtual const Matrix34& GetCameraMatrix() const;
	virtual float GetCameraFOV() const {return m_CamFOV;}
	virtual void SetupCamera();

	virtual const char* GetSampleName() const
	{
		return "Graphics Sample";
	}

public:
    dcamCamMgr& GetCamMgr() {return m_CamMgr;}
    const dcamCamMgr& GetCamMgr() const {return m_CamMgr;}
    void SetViewMode( int mode ) { m_ViewMode = mode; }
    int GetViewMode() const { return m_ViewMode; }

    //utility structure used to maintain the light manipulator
    //gizmos and their associated matrices.
    struct grcSampleLightManipulator
    {
        gzGizmo*	pLightSrcGizmo;
        gzGizmo*	pLightTgtGizmo;
        Matrix34	lightSrcMtx;
        Matrix34	lightTgtMtx;

        grcSampleLightManipulator() :
        pLightSrcGizmo(NULL),
            pLightTgtGizmo(NULL) {};
    };

	const grcViewport* GetViewport() const	
	{	
		return m_Viewport;
	}

    grcLightGroup* GetLightGroup()
    {
        return &m_Lights;
    }

    atRangeArray<grcSampleLightManipulator, grcLightGroup::MAX_LIGHTS>& GetLightManipulators()
    {
        return m_LightManipulators;
    }

	void TakeSnapShot( XmlLog& log , const char* picName );

protected:

	
    virtual bool ClientWantExit() const { return false; }

	void SetCameraHelpY(int y)	
	{
		m_CameraHelpY=y;
	}

public:
    void DrawString(const char* string, bool fadeWithTime = true);
    void SetDrawStringPos(float x, float y);

protected:
#if __BANK
	virtual void AddWidgetsCamera();
	virtual void AddWidgetsGfxSubsystem();
	virtual void AddWidgetsClient() {}
	void		 AddWidgetsCull();
	virtual void AddWidgetsFog();
	virtual void AddWidgetsLights();
	
	void SaveLightPresetBankCbk();
	void LoadLightPresetBankCbk();
	void LightPresetChangedBankCbk();

	int	 LoadDefaultLightPresets(const char**& retPresetNames);

	void SaveCameraPresetBankCbk();
	void LoadCameraPresetBankCbk();
	
	void DirLightOrbitTargetChanged();
#endif

	void DirLightOrbitTargetGizmoDragged();

	grcLightGroup					m_Lights;				//"User" light group
	atArray<grcSampleLightPreset>	m_LightGroupPresets;	//Set of pre-loaded light group presets
	int								m_curLightPresetIdx;	

	atRangeArray<grcSampleLightManipulator, grcLightGroup::MAX_LIGHTS>	m_LightManipulators;

	atArray<grcSampleCameraPreset>	m_CameraPresets;
	int								m_CurCameraPresetIdx;

	enum {NUM_BUCKETS=4};
		
	dcamCamMgr				m_CamMgr;
	const char*				m_LibPath;
	int						m_LightMode;
	grcViewport*			m_Viewport;
	grcFillMode				m_FillMode;
	bool					m_BucketEnable[NUM_BUCKETS];
	Color32					m_FogColor;
	Color32				    m_ClearColor;
	Color32					m_ForceColor;
	float					m_FogStart;
	float					m_FogEnd;
	float					m_FogMin;
	float					m_FogMax;
	bool					m_FogEnable;
	float					m_ClipNear;
	float					m_ClipFar;
	float					m_CamFOV;
	bool					m_UserClip;
	Vector4					m_UserClipPlane;
	grcTexture*				m_RsLogo;
	grcSetup*				m_Setup;
	bool					m_DrawBackground; // do we want a background?
	bool					m_DisplayTimebars;// should the timebars be displayed?
	bool					m_DrawGrid; // do we want a grid?
	bool					m_DrawThreeGrids; // do we want a triple grid?
	bool					m_DrawHelp; // do we want help?
	bool					m_DrawWorldAxes; // do we want to draw the world coordinate system in the corner?
	bool					m_DrawLogo; // do we want a logo?
	bool					m_DrawLights; // do we want the lights?
	bool					m_DrawLightLabels; // do we want the light labels?
	bool					m_DrawLightRadii; // do we want the lights?
	bool					m_DrawCam;	// draw the camera instructions on screen?
	int						m_UseAA;	// use predicated tile to perform Fullscreen AA? (must be 0, 2, or 4)
	float					m_DrawStringPosX, m_DrawStringPosY;
	int						m_CameraHelpY;	// the vertical position to display camera help
	bool					m_AnimateLights;	// animate the lights
	float					m_LightUpdateTime;
	float					m_LightAnimateRate;
	float					m_LightRadius;
	bool					m_WantsExit;
	bool					m_DirLightOrbitTarget;
	int						m_ViewMode;
	bool					m_UseZUp;
	float					m_GridSize;
	float					m_GridLines;
	int						m_GridSubLine;
	float					m_ExitDuration;

	grcRenderTarget*		m_RenderTarget;
	grcRenderTarget*		m_DepthBuffer;

	grcSampleOrthoCam		m_OrthoCam;

	enum EnumColorSchemes
	{
		SCHEME_MAYA,
		SCHEME_RAGE,
		SCHEME_USER
	};
	EnumColorSchemes		m_ColorScheme;
	Color32					m_UserBackgroundColor;
	Color32					m_UserGridColor;
	Color32					m_UserGridAxisColor;
	Color32					m_UserSubGridColor;

	Color32					GetBackGroundColor();

	void DrawBackground();
	virtual void DrawGrid(const Vector3& normal = Vector3(0.0f, 1.0f, 0.0f), const Vector3& tangent = Vector3(1.0f, 0.0f, 0.0f));
	void DrawWorldAxes();
	void DrawLogo();
	void DrawHelp();
	void DrawLights();
	void DrawStats();

	grcVertexDeclaration	*m_GridDecl;
};

//
// grcSampleManager inline implementations
//
inline void grcSampleManager::SetClearColor(Color32 color)
{
	m_ClearColor = color;
}
inline void grcSampleManager::EnableBackgroundDraw(bool enable) 
{
	m_DrawBackground = enable;
}

inline void grcSampleManager::EnableGridDraw(bool enable) 
{
	m_DrawGrid = enable;
}
inline void grcSampleManager::EnableGrid3DDraw(bool enable) 
{
	m_DrawThreeGrids = enable;
}

inline void grcSampleManager::EnableDrawHelp(bool enable)
{
	m_DrawHelp = enable;
}

inline void grcSampleManager::EnableWorldAxesDraw(bool enable)
{
	m_DrawWorldAxes = enable;
}

inline void grcSampleManager::EnableLogoDraw(bool enable)
{
	m_DrawLogo = enable;
}

inline void grcSampleManager::EnableCameraDraw(bool enable)
{
	m_DrawCam = enable;
}

} // namespace ragesamples

#endif // SAMPLE_GRCORE_SAMPLE_GRCORE_H
