//
// sample_grcore/sample_grcore.cpp
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#include "sample_grcore.h"

#include "rsny.h"

#include "atl/string.h"
#include "bank/bank.h"
#include "bank/bkmgr.h"
#include "diag/tracker.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/embedded.h"
#include "file/stream.h"
#include "gizmo/manager.h"
#include "gizmo/rotation.h"
#include "gizmo/translation.h"
#include "grcore/effect.h"
#include "grcore/im.h"
#include "grcore/quads.h"
#include "grprofile/pix.h"
#include "grcore/setup.h"
#include "grcore/state.h"
#include "grcustom/customlighting.h"
#include "input/input.h"
#include "parser/manager.h"
#include "parser/tree.h"
#include "parser/treenode.h"
#include "parsercore/attribute.h"
#include "parsercore/element.h"
#include "parsercore/stream.h"
#include "grprofile/ekg.h"
#include "profile/profiler.h"
#include "grprofile/stats.h"
#include "profile/trace.h"
#include "profile/cputrace.h"
#include "profile/timebars.h"
#include "spatialdata/sphere.h"
#include "system/alloca.h"
#include "system/param.h"
#include "system/timemgr.h"
#include "system/stack.h"
#include "vector/geometry.h"
#include "diag/output.h"
#include "diag/xmllog.h"
#include "system/simpleallocator.h"

#if  __PPU
#include <stdlib.h>		// for malloc_stats
#endif

using namespace rage;

fiEmbeddedFile rsny("rsny.dds",rsny_dds,sizeof(rsny_dds));

namespace rage {
	XPARAM(debugshaders);
	XPARAM(nopopups);
}

namespace ragesamples {

PARAM(assets, "[sample_grcore] Path to the assets directory, e.g. T:\\samples\\assets");
PARAM(path,"[sample_grcore] Path to use for files (default is \"../../assets/<sample_dir>/\"");
PARAM(oneframe,"[sample_grcore] Render one frame and quit - useful for testing shutdown procedures");
#if __XENON
PARAM(aatiling,"[sample_grcore] Render with 4xAA and Predicated Tiling (does not work with render target samples) (default is off)/\"");
#endif
PARAM(hdr,"[device_d3d] Set the whole rendering pipeline to 10-bit on 360 and 16-bit on PC");
PARAM(ati,"[sample_grcore] Sets ATI specific render targets");
PARAM(zup,"[sample_grcore] Set the viewer to use Z-up");

PARAM(gridsize,"[sample_grcore] Grid size");
PARAM(gridspacing,"[sample_grcore] Grid line spacing");
PARAM(gridcellsub,"[sample_grcore] Number of subdivisions per grid cell");
PARAM(drawthreegrids,"[sample_grcore] Draw three grids");
PARAM(cachetextures, "[sample_grcore] Load all textures from a cache");

PARAM(smoketest,"[sample_grcore] specifies that this sample is run in smoke test mode so will start up and wait for a duration" );
PARAM(quiet,"[sample_grcore] only displays errors and warnings" );
PARAM(smokesnapshot,"[sample_grcore] takes a snapshot of the scene when the smoketest is done, and specifies screenshot name" );

PARAM(leaksAsErrors,"[sample_grcore] will display leaks as errors rather than warnings, useful for smoke tests");
PARAM(faultinjectionFolder,"[sample_grcore] will cause all file loads from that folder to fail, useful for testing erroring handling");
PARAM(lightpresets,"[sample_grcore] load a light presets file");
PARAM(campresets,"[sample_grcore] load a camera presets file");
PARAM(farclip,"[sample_grcore] Sets the farclip distance [1-10000]");

PF_PAGE(Rage_Sample_Page, "RAGE Sample - top level");
PF_GROUP(Rage_Sample);
PF_LINK(Rage_Sample_Page, Rage_Sample);
PF_TIMER(Update, Rage_Sample);
PF_TIMER(Draw, Rage_Sample);
PF_TIMER(ClientUpdate, Rage_Sample);
PF_TIMER(ClientDraw, Rage_Sample);
PF_TIMER(EndDraw, Rage_Sample);
#if __STATS
static float s_LastDrawTime;
#endif

static const int renderTargetSize = 128;

static char* camTypeEnumToString[] =  { "polar", "maya", "fly", "free", "roam", "track", "quake", "lhcam"};
static char* orthoCamTypeEnumToString[] = { "top", "bottom", "left", "right", "front", "back" };

static void DrawDashedLine(const Vector3& from, const Vector3& to, float dashSep);

grcTexture* grcCachingTextureFactory::Create(const char* filename,grcTextureFactory::TextureCreateParams *params)
{
	grcTexture* tex = grcTextureFactoryDefault::Create(filename,params);

	if ( tex && tex->GetRefCount() == 1)
	{
		char	Buffer[256];
		StringNormalize(Buffer, filename, sizeof(Buffer));
sysMemStartTemp();
		RegisterTextureReference(Buffer, tex);
sysMemEndTemp();
	}
	return tex;
}

grcTexture* grcCachingTextureFactory::Create(grcImage *image,grcTextureFactory::TextureCreateParams *params)
{
	return grcTextureFactoryDefault::Create(image,params);
}

grcSampleManager::grcSampleManager()
	: m_curLightPresetIdx(0)
	, m_LightMode(grclmPoint)
	, m_ForceColor(255,255,255)
	, m_FogStart(1.0f)
	, m_FogEnd(200.0f)
	, m_FogMin(0.0f)
	, m_FogMax(1.0f)
	, m_FogEnable(false)
	, m_ClipNear(0.05f)
	, m_ClipFar(1000.0f)
	, m_CamFOV(64.0f)
	, m_UserClip(false)
	, m_UserClipPlane(0,0,1,0)
	, m_DrawGrid(__DEV)
	, m_DrawThreeGrids(false)
	, m_DrawHelp(__DEV)
	, m_DrawWorldAxes(__DEV)
	, m_DrawLogo(__DEV)
	, m_DrawLights(false)
	, m_DrawLightLabels(true)
	, m_DrawLightRadii(false)
	, m_DrawCam(true)
	, m_UseAA(0)
	, m_CameraHelpY(50)
	, m_AnimateLights(false)
	, m_LightUpdateTime(0.0f)
	, m_LightAnimateRate(0.5f)
	, m_LightRadius(3.0f)
	, m_WantsExit(false)
	, m_DirLightOrbitTarget(false)
	, m_ViewMode(0)
	, m_UseZUp(false)
	, m_RenderTarget(NULL)
	, m_DepthBuffer(NULL)
	, m_ColorScheme(SCHEME_RAGE)
	, m_UserBackgroundColor(40,40,40)
	, m_UserGridColor(80,80,80)
	, m_UserGridAxisColor(77, 204, 255)
	, m_UserSubGridColor(60, 60, 60, 100)
	, m_GridSize(100)
	, m_GridLines(2)
	, m_GridSubLine(1)
	, m_DrawBackground(true)
	, m_DisplayTimebars(true)
	, m_ExitDuration( 0.0f )
{
	RAGETRACE_INITTHREAD("MainLoop", 1024, 1);
	PF_INIT_TIMEBARS(1000);
	PARAM_farclip.Get(m_ClipFar);

#if !__FINAL && __WIN32PC
	if ( PARAM_debugshaders.Get() )
	{
		m_DrawGrid = false;
		m_DrawWorldAxes = false;
		m_DrawLogo = false;
		m_DrawCam = false;
	}
#endif	// !__FINAL && __WIN32PC
	if( PARAM_drawthreegrids.Get() )
	{
		m_DrawThreeGrids = true;
	}
	m_ClearColor=Color32(0,10,100);
#if __XENON
	PARAM_aatiling.GetDebug(m_UseAA);
	if (m_UseAA==1)
		m_UseAA=2;
	else if (m_UseAA > 2)
		m_UseAA=4;
	if (m_UseAA)
		m_DrawWorldAxes = false;		// rendertargets break us
#endif

	m_UseZUp = PARAM_zup.Get();
	if(m_UseZUp)
	{
		SetUnitUp(ZAXIS);
	}

	INIT_GIZMOS;
}

grcSampleManager::~grcSampleManager()
{
	SHUTDOWN_GIZMOS;

	// Verify the resource heaps got cleaned up properly.
	sysMemAllocator* allocator = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_VIRTUAL);
	if (allocator->GetMemoryUsed())
		Errorf("Resource virtual heap still has %u bytes in use, probable leak!",allocator->GetMemoryUsed());
	allocator = sysMemAllocator::GetCurrent().GetAllocator(MEMTYPE_RESOURCE_PHYSICAL);
	if (allocator->GetMemoryUsed())
		Errorf("Resource physical heap still has %u bytes in use, probable leak!",allocator->GetMemoryUsed());
}

#if __WIN32PC
// PURPOSE : Callbacks for controlling error display when in automated mode.
//
static void DefaultDisplayErrorLine(u32 addr,const char *sym,u32 offset) {
	Errorf("%x - %s+%x",addr,sym,offset);
}
static void PrintMiniDumpMessage(  const char* fileName )
{
	//TODO : use environment variables for COMPUTER_NAME to work out prefix
	const char* filePrefix = "\\\\sanw-frg01";
	Errorf("To Run The Mini Dump, Run The Location Below ( copy, Windows + R, paste) and then F5 to get breakpoint"); 
	Errorf("Mini Dump Saved at ' %s%s '", filePrefix, fileName + 2 );
}
#endif
// PURPOSE
//
//	Sets the sample into automated mode so that there are no pops and output is in the correct format.
//
void SetAutomatedMode()
{
#if __WIN32PC && !__FINAL
	diagOutput::UseVSFormatOutput( ); 
	PARAM_nopopups.Set("");

	// Display the stack as an error so it is highlighted in the bug reports
	sysStack::ExceptionDisplayLine = DefaultDisplayErrorLine;

	// Use mini dumps to help tracking errors
	sysStack::SetEnableMinidumps( true );

	sysStack::SetMiniDumpMessageHandler( PrintMiniDumpMessage );
#endif
}

void grcSampleManager::Init(const char* path)
{

#if __XENON
	if ( m_UseAA )
	{
		grcDevice::SetRingBufferParameters( 32 , 8192, 0, true);
	}
#endif

	INIT_PARSER;

	float duration;
	if ( PARAM_smoketest.Get( duration ) )
	{
		m_ExitDuration =duration;
		SetAutomatedMode();
	}
#if !__NO_OUTPUT
	if ( PARAM_quiet.Get() )
	{
		diagOutput::SetOutputMask( diagOutput::OUTPUT_WARNINGS | diagOutput::OUTPUT_ERRORS );
	}
#endif

	const char* faultFolder = 0;
	if ( PARAM_faultinjectionFolder.Get( faultFolder ) )
	{
		fiStream::SetFaultInjectionPath( faultFolder );
	}
	sysMemSimpleAllocator::DisplayLeaksAsErrors( PARAM_leaksAsErrors.Get() );

	InitSetup();

	if (!ASSET.GetPath()[0]) 
	{
		Displayf("Setting root asset path '%s', from default RAGE setting.", RAGE_ASSET_ROOT);
		SetFullAssetPath(RAGE_ASSET_ROOT);

		if (path)
		{
			Displayf("Adding application path '%s' to the asset search path.", path);
			ASSET.PushFolder(path);
		}
	}
	else
	{
		//The asset path has already been set from another location
		const char* assetPath = ASSET.GetPath();
		Displayf("Root asset path set to '%s'", assetPath);
	}

	rage::INPUT.Begin(true);

	grcVertexElement e[3];
	e[0].type = grcVertexElement::grcvetPosition;
	e[0].size = 12;
	e[0].format = grcFvf::grcdsFloat3;
	e[1].type = grcVertexElement::grcvetColor;
	e[1].size = 4;
	e[1].format = grcFvf::grcdsColor;
	e[2].type = grcVertexElement::grcvetTexture;
	e[2].size = 8;
	e[2].format = grcFvf::grcdsFloat2;
	m_GridDecl = GRCDEVICE.CreateVertexDeclaration(e,3);

	m_Viewport=rage_new grcViewport;

	if (!grcCustomLighting::IsInstantiated())
	{
		grcCustomLighting::InitClass();
	}

	InitGfxFactories();

	// The logo is declared in the fiEmbeddedFile object at the top of the file.
	m_RsLogo=grcTextureFactory::GetInstance().Create("embedded:/rsny.dds");

#if __STATS
	GetRageProfiler().Init();
	RAGETRACE_INIT();
#endif

	InitClient();

	InitCamera();
	InitLights();

	if(PARAM_gridsize.Get())
	{
		PARAM_gridsize.Get(m_GridSize);
		//TODO : This parameter needs to be validated so that it doesn't cause a crash in the drawing code
	}

	if(PARAM_gridspacing.Get())
	{
		PARAM_gridspacing.Get(m_GridLines);
		//TODO : This parameter needs to be validated so that it doesn't cause a crash in the drawing code
	}

	if(PARAM_gridcellsub.Get())
	{
		PARAM_gridcellsub.Get(m_GridSubLine);
		//TODO : This parameter needs to be validated so that it doesn't cause a crash in the drawing code
	}

	const char* lightPresetsFileName;
	if (PARAM_lightpresets.Get(lightPresetsFileName))
	{
		if (ASSET.Exists(lightPresetsFileName, "lgroup"))
		{
			char fullPath[RAGE_MAX_PATH];
			ASSET.FullPath(fullPath, RAGE_MAX_PATH, lightPresetsFileName, "lgroup");

			bool found = false;
			for (int i = 0; i < m_LightGroupPresets.GetCount() && !found; ++i)
			{
				found = m_LightGroupPresets[i].m_FileName == fullPath;
			}

			if (!found)
			{
				grcSampleLightPreset &preset = m_LightGroupPresets.Grow();
				LoadLightPreset(fullPath, &preset);
				SetLightPreset(&preset);
			}
		}
	}

	const char* camPresetsFileName;
	if (PARAM_campresets.Get(camPresetsFileName))
	{
		if (ASSET.Exists(camPresetsFileName, "camset"))
		{
			char fullPath[RAGE_MAX_PATH];
			ASSET.FullPath(fullPath, RAGE_MAX_PATH, camPresetsFileName, "camset");

			bool found = false;
			for (int i = 0; i < m_CameraPresets.GetCount() && !found; ++i)
			{
				found = m_CameraPresets[i].m_FileName == fullPath;
			}

			if (!found)
			{
				grcSampleCameraPreset &preset = m_CameraPresets.Grow();
				LoadCameraPreset(fullPath, &preset);
				SetCameraPreset(&preset);
			}
		}
	}

#if __BANK
	ioPad::AddWidgets();
	bkBank &B = BANKMGR.CreateBank("Debug Spew");
	diagChannel::InitWidgets(B);

	AddWidgetsGfxSubsystem();
	AddWidgetsCamera();
	AddWidgetsCull();
	AddWidgetsFog();
	AddWidgetsLights();
	AddWidgetsClient();
#endif
}

void grcSampleManager::InitParams()
{
	const char* path = 0;

#if __XENON
	if ( m_UseAA )
	{
		grcDevice::SetRingBufferParameters( 32 , 8192, 0, true);
	}
#endif

	INIT_PARSER;

	float duration;
	if ( PARAM_smoketest.Get( duration ) )
	{
		m_ExitDuration =duration;
		SetAutomatedMode();
	}
#if !__NO_OUTPUT
	if ( PARAM_quiet.Get() )
	{
		diagOutput::SetOutputMask( diagOutput::OUTPUT_WARNINGS | diagOutput::OUTPUT_ERRORS );
	}
#endif

	const char* faultFolder = 0;
	if ( PARAM_faultinjectionFolder.Get( faultFolder ) )
	{
		fiStream::SetFaultInjectionPath( faultFolder );
	}
	sysMemSimpleAllocator::DisplayLeaksAsErrors( PARAM_leaksAsErrors.Get() );

	if (!ASSET.GetPath()[0]) 
	{
		Displayf("Setting root asset path '%s', from default RAGE setting.", RAGE_ASSET_ROOT);
		SetFullAssetPath(RAGE_ASSET_ROOT);

		if (path)
		{
			Displayf("Adding application path '%s' to the asset search path.", path);
			ASSET.PushFolder(path);
		}
	}
	else
	{
		//The asset path has already been set from another location
		const char* assetPath = ASSET.GetPath();
		Displayf("Root asset path set to '%s'", assetPath);
	}

	if(PARAM_gridsize.Get())
	{
		PARAM_gridsize.Get(m_GridSize);
		//TODO : This parameter needs to be validated so that it doesn't cause a crash in the drawing code
	}

	if(PARAM_gridspacing.Get())
	{
		PARAM_gridspacing.Get(m_GridLines);
		//TODO : This parameter needs to be validated so that it doesn't cause a crash in the drawing code
	}

	if(PARAM_gridcellsub.Get())
	{
		PARAM_gridcellsub.Get(m_GridSubLine);
		//TODO : This parameter needs to be validated so that it doesn't cause a crash in the drawing code
	}
}

void grcSampleManager::InitInput()
{
	rage::INPUT.Begin(true);
}

void grcSampleManager::InitSetup()
{
	m_Setup=&AllocateSetup();

	char sampleName[1024];
	sprintf(sampleName,"%s (version %s)",GetSampleName(),RAGE_RELEASE_STRING);
	m_Setup->Init(GetFullAssetPath(),sampleName);
	m_Setup->BeginGfx(true);
}

void grcSampleManager::InitGrcDevice()
{
	grcVertexElement e[3];
	e[0].type = grcVertexElement::grcvetPosition;
	e[0].size = 12;
	e[0].format = grcFvf::grcdsFloat3;
	e[1].type = grcVertexElement::grcvetColor;
	e[1].size = 4;
	e[1].format = grcFvf::grcdsColor;
	e[2].type = grcVertexElement::grcvetTexture;
	e[2].size = 8;
	e[2].format = grcFvf::grcdsFloat2;
	m_GridDecl = GRCDEVICE.CreateVertexDeclaration(e,3);
}

void grcSampleManager::InitViewport()
{
	m_Viewport=rage_new grcViewport;
}

void grcSampleManager::InitCustomLighting()
{
	if (!grcCustomLighting::IsInstantiated())
	{
		grcCustomLighting::InitClass();
	}
}

void grcSampleManager::InitGfxFactories()
{
	m_Setup->CreateDefaultFactories();
	if (PARAM_cachetextures.Get())
	{
		grcTextureFactory::SetInstance(* (rage_new grcCachingTextureFactory));
	}

	grcCustomLighting::GetInstance().RegisterImplementations();
}

void grcSampleManager::InitLogo()
{
	// The logo is declared in the fiEmbeddedFile object at the top of the file.
	m_RsLogo=grcTextureFactory::GetInstance().Create("embedded:/rsny.dds");
}

void grcSampleManager::InitStats()
{
#if __STATS
	GetRageProfiler().Init();
	RAGETRACE_INIT();
#endif
}

void grcSampleManager::InitLightPresets()
{
	const char* lightPresetsFileName;
	if (PARAM_lightpresets.Get(lightPresetsFileName))
	{
		if (ASSET.Exists(lightPresetsFileName, "lgroup"))
		{
			char fullPath[RAGE_MAX_PATH];
			ASSET.FullPath(fullPath, RAGE_MAX_PATH, lightPresetsFileName, "lgroup");

			bool found = false;
			for (int i = 0; i < m_LightGroupPresets.GetCount() && !found; ++i)
			{
				found = m_LightGroupPresets[i].m_FileName == fullPath;
			}

			if (!found)
			{
				grcSampleLightPreset &preset = m_LightGroupPresets.Grow();
				LoadLightPreset(fullPath, &preset);
				SetLightPreset(&preset);
			}
		}
	}
}

void grcSampleManager::InitCameraPresets()
{
	const char* camPresetsFileName;
	if (PARAM_campresets.Get(camPresetsFileName))
	{
		if (ASSET.Exists(camPresetsFileName, "camset"))
		{
			char fullPath[RAGE_MAX_PATH];
			ASSET.FullPath(fullPath, RAGE_MAX_PATH, camPresetsFileName, "camset");

			bool found = false;
			for (int i = 0; i < m_CameraPresets.GetCount() && !found; ++i)
			{
				found = m_CameraPresets[i].m_FileName == fullPath;
			}

			if (!found)
			{
				grcSampleCameraPreset &preset = m_CameraPresets.Grow();
				LoadCameraPreset(fullPath, &preset);
				SetCameraPreset(&preset);
			}
		}
	}
}

void grcSampleManager::InitCamera()
{
	if(m_UseZUp)
	{
		m_OrthoCam.SetUseZUp(true);
		m_OrthoCam.InitViews();
		
		m_CamMgr.SetUseZUp(true);
		InitSampleCamera(Vector3(0.0f, -5.f, 2.5f), Vector3(0.f,0.f,0.f));
	}
	else
	{
		m_OrthoCam.InitViews();
		InitSampleCamera(Vector3(0.f, 2.5f, -5.f), Vector3(0.f, 0.f, 0.f));
	}
}

PARAM(defaultcamera, "[sample_grcore] The camera active when the sample launches");

void grcSampleManager::InitSampleCamera(const Vector3& lookFrom,const Vector3& lookTo)
{
	dcamCamMgr::eCamera cameraType=dcamCamMgr::CAM_POLAR;
	const char* cameraString = NULL;

#if __BANK
	if (bkRemotePacket::IsConnectedToRag())
	{
		cameraType=dcamCamMgr::CAM_MAYA;
	}
#endif

#if __WIN32PC
	cameraType=dcamCamMgr::CAM_MAYA;

	cameraString = getenv("RAGE_DEFAULT_CAMERA");
#endif

	PARAM_defaultcamera.Get(cameraString);

	if (cameraString)
	{
		if (stricmp(cameraString, "POLAR") == 0)
		{
			cameraType = dcamCamMgr::CAM_POLAR;
		}
		else if (stricmp(cameraString, "FLY") == 0)
		{
			cameraType = dcamCamMgr::CAM_FLY;
		}
		else if (stricmp(cameraString, "FREE") == 0)
		{
			cameraType = dcamCamMgr::CAM_FREE;
		}
		else if (stricmp(cameraString, "ROAM") == 0)
		{
			cameraType = dcamCamMgr::CAM_ROAM;
		}
		else if (stricmp(cameraString, "TRACK") == 0)
		{
			cameraType = dcamCamMgr::CAM_TRACK;
		}
		else if (stricmp(cameraString, "QUAKE") == 0)
		{
			cameraType = dcamCamMgr::CAM_QUAKE;
		}
		else if (stricmp(cameraString, "WAR") == 0)
		{
			cameraType = dcamCamMgr::CAM_WAR;
		}
	}

	m_CamMgr.Init(lookFrom, lookTo, cameraType);
    m_CamMgr.SetEnableDrawHelp(m_DrawCam);
}

const Matrix34& grcSampleManager::GetCameraMatrix() const 
{
	if(m_ViewMode == 0)
		return m_CamMgr.GetWorldMtx();
	else
		return m_OrthoCam.GetWorldMatrix();
}

void grcSampleManager::InitLights()
{
	// These numbers make Daren happy.
	m_Lights.SetColor(0, 1.0f,1.0f,0.71f);
	m_Lights.SetColor(1, 0.1f,0.1f,0.4f);
	m_Lights.SetColor(2, 0.3f,0.3f,0.1f);
	m_Lights.SetPosition(0, Vector3(0.725f,3.725f,2.0f));
	m_Lights.SetPosition(1, Vector3(-1.27f,-0.5f,-1.0f));
	m_Lights.SetPosition(2, Vector3(1.2f,-0.575f,-2.15f));
	m_Lights.SetIntensity(0, 1.f);
	m_Lights.SetIntensity(1, 1.f);
	m_Lights.SetIntensity(2, 1.f);
	m_Lights.SetFalloff(0, 5.f);
	m_Lights.SetFalloff(1, 5.f);
	m_Lights.SetFalloff(2, 5.f);
	m_Lights.SetLightType(0, grcLightGroup::LTTYPE_POINT);
	m_Lights.SetLightType(1, grcLightGroup::LTTYPE_POINT);
	m_Lights.SetLightType(2, grcLightGroup::LTTYPE_POINT);

	// MP3
	m_Lights.SetColor(3, 1.0f,1.0f,0.71f);
	m_Lights.SetIntensity(3, 1.0f);
	m_Lights.SetLightType(3, grcLightGroup::LTTYPE_DIR);
	if (PARAM_zup.Get())
	{
		m_Lights.SetDirection(3, Vector3(0, 0, -1));
	}
	else
	{
		m_Lights.SetDirection(3, Vector3(0, -1, 0));
	}

	m_Lights.SetActiveCount(4);
}

void grcSampleManager::InitLightManipulators()
{
	for (int lightIdx = 0; lightIdx < grcLightGroup::MAX_LIGHTS; ++lightIdx)
	{
		grcLightGroup::grcLightType type = m_Lights.GetLightType(lightIdx);

		m_LightManipulators[lightIdx].lightSrcMtx.Identity();
		m_LightManipulators[lightIdx].lightSrcMtx.d = m_Lights.GetPosition(lightIdx);
		m_LightManipulators[lightIdx].lightTgtMtx.Identity();
		
		float t;
		Vector3 dir = m_Lights.GetDirection(lightIdx);
		Vector3 pos = m_Lights.GetPosition(lightIdx);

		//If possible try to initially position the light target on the ground plane
		geomSegments::CollideRayPlane(pos, dir,Vector4(0.0f,1.0f,0.0f,0.0f),&t);
		Vector3 targetPos = m_Lights.GetPosition(lightIdx);
		if(t <= 0.0f)
		{
			//No intersection, so just place it 2 units out along the direction vector
			targetPos.AddScaled(m_Lights.GetDirection(lightIdx), 2.0f);
		}
		else
		{
			targetPos.AddScaled(m_Lights.GetDirection(lightIdx), t);
		}
		m_LightManipulators[lightIdx].lightTgtMtx.d = targetPos;

		//Create the gizmos and hook them up to their associated matrices
		if(m_LightManipulators[lightIdx].pLightSrcGizmo)
			delete m_LightManipulators[lightIdx].pLightSrcGizmo;
		m_LightManipulators[lightIdx].pLightSrcGizmo = rage_new gzTranslation(m_LightManipulators[lightIdx].lightSrcMtx);
		if (lightIdx<m_Lights.GetActiveCount())
			m_LightManipulators[lightIdx].pLightSrcGizmo->Activate();
		else
			m_LightManipulators[lightIdx].pLightSrcGizmo->Deactivate();
		
		
		if(m_LightManipulators[lightIdx].pLightTgtGizmo)
				delete m_LightManipulators[lightIdx].pLightTgtGizmo;

		if((type == grcLightGroup::LTTYPE_DIR) && m_DirLightOrbitTarget)
		{
			m_LightManipulators[lightIdx].pLightTgtGizmo = rage_new gzRotation(m_LightManipulators[lightIdx].lightTgtMtx, Functor0::NullFunctor(), MakeFunctor(*this, &grcSampleManager::DirLightOrbitTargetGizmoDragged));
			m_LightManipulators[lightIdx].lightTgtMtx.LookAt(m_LightManipulators[lightIdx].lightTgtMtx.d,
															 m_LightManipulators[lightIdx].lightSrcMtx.d);
		}
		else
		{
			m_LightManipulators[lightIdx].pLightTgtGizmo = rage_new gzTranslation(m_LightManipulators[lightIdx].lightTgtMtx);
			if (lightIdx<m_Lights.GetActiveCount())
				m_LightManipulators[lightIdx].pLightTgtGizmo->Activate();
			else
				m_LightManipulators[lightIdx].pLightTgtGizmo->Deactivate();
		}
	}
}

void grcSampleManager::InitRenderTarget()
{
	grcTextureFactory::CreateParams params;
	params.Multisample = 0;
	params.HasParent = false;
	if(PARAM_hdr.Get())
	#if __WIN32PC
	params.Format = grctfA16B16G16R16F;
	if(PARAM_ati.Get())
	// needs to be implemented for ATI X1 cards
	params.Multisample = 0;
	#elif __XENON
	params.Format = grctfA2B10G10R10;
	#elif __PPU
	params.Format = grctfA16B16G16R16F;
	#endif

	if (m_RenderTarget == NULL)
	{
		m_RenderTarget = grcTextureFactory::GetInstance().CreateRenderTarget("WorldAxes",grcrtPermanent,renderTargetSize,renderTargetSize,32, &params);
	}
}

void grcSampleManager::InitDepthBuffer()
{
	if (m_DepthBuffer == NULL)
	{
		m_DepthBuffer = grcTextureFactory::GetInstance().CreateRenderTarget("Depth",grcrtDepthBuffer,renderTargetSize,renderTargetSize,32);
	}
}

grcSetup& grcSampleManager::AllocateSetup()
{
	return *(rage_new grcSetup());
}


void grcSampleManager::Shutdown()
{
	// Make sure GPU is done with anything we're about to kill.
	GRCDEVICE.BlockOnFence(GRCDEVICE.InsertFence());

	ShutdownLights();
	ShutdownCamera();
	ShutdownClient();

	if (m_RenderTarget)
	{
		m_RenderTarget->Release();
	}
	if (m_DepthBuffer)
	{
		m_DepthBuffer->Release();
	}

	if (grcCustomLighting::IsInstantiated())
		grcCustomLighting::ShutdownClass();


	//Clean-up the light manipulator gizmos...
	for (int lightIdx = 0; lightIdx < grcLightGroup::MAX_LIGHTS; ++lightIdx)
	{
		delete m_LightManipulators[lightIdx].pLightSrcGizmo;
		m_LightManipulators[lightIdx].pLightSrcGizmo = NULL;
		delete m_LightManipulators[lightIdx].pLightTgtGizmo;
		m_LightManipulators[lightIdx].pLightTgtGizmo = NULL;
	}

	SHUTDOWN_PARSER;
	SHUTDOWN_GIZMOS;

#if __STATS
	RAGETRACE_SHUTDOWN();
	GetRageProfiler().Shutdown();
#endif

	m_RsLogo->Release();
	m_RsLogo = NULL;

	ShutdownGfxFactories();

	delete m_Viewport;
	m_Viewport = NULL;

	rage::INPUT.End();

	GRCDEVICE.DestroyVertexDeclaration(m_GridDecl);
	m_Setup->EndGfx();
	m_Setup->Shutdown();

	delete m_Setup;
	m_Setup = NULL;
}

void grcSampleManager::ShutdownGrcDevice()
{
	// Make sure GPU is done with anything we're about to kill.
	GRCDEVICE.BlockOnFence(GRCDEVICE.InsertFence());
	GRCDEVICE.DestroyVertexDeclaration(m_GridDecl);
}

void grcSampleManager::ShutdownRenderTarget()
{
	if (m_RenderTarget)
	{
		m_RenderTarget->Release();
		m_RenderTarget = NULL;
	}
}

void grcSampleManager::ShutdownDepthBuffer()
{
	if (m_DepthBuffer)
	{
		m_DepthBuffer->Release();
		m_DepthBuffer = NULL;
	}
}

void grcSampleManager::ShutdownCustomLighting()
{
	if (grcCustomLighting::IsInstantiated())
		grcCustomLighting::ShutdownClass();
}

void grcSampleManager::ShutdownLightManipulators()
{
	for (int lightIdx = 0; lightIdx < grcLightGroup::MAX_LIGHTS; ++lightIdx)
	{
		delete m_LightManipulators[lightIdx].pLightSrcGizmo;
		m_LightManipulators[lightIdx].pLightSrcGizmo = NULL;
		delete m_LightManipulators[lightIdx].pLightTgtGizmo;
		m_LightManipulators[lightIdx].pLightTgtGizmo = NULL;
	}
}

void grcSampleManager::ShutdownStats()
{
#if __STATS
	RAGETRACE_SHUTDOWN();
	GetRageProfiler().Shutdown();
#endif
}

void grcSampleManager::ShutdownLogo()
{
	m_RsLogo->Release();
	m_RsLogo = NULL;
}

void grcSampleManager::ShutdownGfxFactories() 
{
	m_Setup->DestroyFactories();
}

void grcSampleManager::ShutdownViewport() 
{
	delete m_Viewport;
	m_Viewport = NULL;
}

void grcSampleManager::ShutdownInput()
{
	rage::INPUT.End();
}

void grcSampleManager::ShutdownSetup()
{
	m_Setup->EndGfx();
	m_Setup->Shutdown();

	delete m_Setup;
	m_Setup = NULL;
}

void grcSampleManager::ShutdownParams()
{
	SHUTDOWN_PARSER;
}

void grcSampleManager::ShutdownLights()
{
}

void grcSampleManager::ShutdownCamera()
{
}

bool grcSampleManager::WantsExit()
{
	const bool oneFrame = PARAM_oneframe.Get();
#if IS_CONSOLE && !__FINAL
	const bool weWantExit = (ioPad::GetPad(0).GetDebugButtons() & ioPad::SELECT)!=0;
#else
	const bool weWantExit = false;
#endif
	if (m_WantsExit)
		return true;
	else 
		return m_WantsExit=m_Setup->WantExit() || oneFrame || weWantExit || ClientWantExit();
}

void grcSampleManager::UpdateLoop()
{
#if __PPU
	struct std::malloc_managed_size mms;
	std::malloc_stats(&mms);
	Displayf("**** PS3 malloc current system usage %dK; in use is %dk",mms.current_system_size>>10,mms.current_inuse_size>>10);
#endif

	const bool oneFrame = PARAM_oneframe.Get();
	RAGE_TRACK_REPORT_NAME( "FirstFrame" );

	do 
	{
		PF_FRAMEINIT_TIMEBARS();

		Update();
		if (WantsExit())
			break;

		int numDraws=0;

#if __STATS
		sysTimer drawTime;
#endif
		while (DoWeKeepDrawing(numDraws++))
			Draw();
#if __STATS
		s_LastDrawTime = drawTime.GetTime();
#endif

	} while (!WantsExit());

	if ( oneFrame )
		Displayf("One frame update requested -- Exiting....");
}

//-----------------------------------------------------------------------
//PURPOSE:	single step of the above UpdateLoop, used by the game to fine control the updates.
//-----------------------------------------------------------------------
void grcSampleManager::UpdateStep()
{   
	Update();

	int numDraws=0;

	while (DoWeKeepDrawing(numDraws++))
		Draw();
}

void grcSampleManager::UpdateCamera() 
{
	if(m_ViewMode == 0)
		GetCamMgr().Update();
	else
	{
		grcSampleOrthoCam::OrthoMode mode = static_cast<grcSampleOrthoCam::OrthoMode>(m_ViewMode - 1);
		m_OrthoCam.SetOrthoMode(mode);
		m_OrthoCam.Update();
	}
}

void grcSampleManager::UpdateLights()
{
	for ( int i = 0; i < grcLightGroup::MAX_LIGHTS; ++i )
	{
		if ( i < m_Lights.GetActiveCount() )
        {
            UpdateLight( i );
        }
	}

    if ( m_AnimateLights )
    {
        m_LightUpdateTime += TIME.GetSeconds() * m_LightAnimateRate;
    }
}

void grcSampleManager::UpdateLight( int idx )
{
    if ( m_AnimateLights )
    {
        float x,y,z;

        if ( m_Lights.GetLightType( idx ) == grcLightGroup::LTTYPE_POINT )
        {
            if ( idx == 0 )
            {
                x = m_LightRadius * cosf( 2.0f * PI * m_LightUpdateTime );
                y = m_LightRadius * sinf( 2.0f * PI * m_LightUpdateTime );
                z = 0.0f;
                m_LightManipulators[idx].lightSrcMtx.d = Vector3( x, y, z );
            }
            else if ( idx == 1 )
            {
                x = 0.0f;
                y = m_LightRadius * cosf( 2.0f * PI * m_LightUpdateTime );
                z = m_LightRadius * sinf( 2.0f * PI * m_LightUpdateTime );
                m_LightManipulators[idx].lightSrcMtx.d = Vector3( x, y, z );
            }
            else if ( idx == 2 )
            {
                x = m_LightRadius * sinf( 2.0f * PI * m_LightUpdateTime );
                y = 0.0f;
                z = m_LightRadius * cosf( 2.0f * PI * m_LightUpdateTime );
                m_LightManipulators[idx].lightSrcMtx.d = Vector3( x, y, z );
            }           
        }
    }

    grcLightGroup::grcLightType lightType = m_Lights.GetLightType( idx );
    switch ( lightType )
    {
    case grcLightGroup::LTTYPE_DIR:
        {
            if ( m_DirLightOrbitTarget )
            {
                //Make sure the proper gizmo is on the target if we are in orbit mode since this can get out of sync due to the 
                //lack of a callback when the light type is changed by the user
                gzRotation* pRotGiz = dynamic_cast<gzRotation*>( m_LightManipulators[idx].pLightTgtGizmo );
                if ( !pRotGiz )
                {
                    if ( m_LightManipulators[idx].pLightTgtGizmo )
                    {
                        delete m_LightManipulators[idx].pLightTgtGizmo;
                    }

                    m_LightManipulators[idx].pLightTgtGizmo = rage_new gzRotation( m_LightManipulators[idx].lightTgtMtx, 
                        Functor0::NullFunctor(), MakeFunctor( *this, &grcSampleManager::DirLightOrbitTargetGizmoDragged ) );

                    //Adjust the manipulator matrix so the z-axis is pointing in the same direction as the light
                    m_LightManipulators[idx].lightTgtMtx.LookAt( 
                        m_LightManipulators[idx].lightTgtMtx.d, m_LightManipulators[idx].lightSrcMtx.d );
                }
            }
          
			m_Lights.SetPosition( idx, m_LightManipulators[idx].lightSrcMtx.d );
            
            Vector3 dir;
			dir.Subtract( m_LightManipulators[idx].lightTgtMtx.d, m_LightManipulators[idx].lightSrcMtx.d );
            dir.Normalize();
            m_Lights.SetDirection( idx, dir );

            //Turn the manipulator drawing on or off.
            if ( m_LightManipulators[idx].pLightSrcGizmo )
            {
                if ( m_DrawLights )
                {
                    m_LightManipulators[idx].pLightSrcGizmo->Activate();
                    m_LightManipulators[idx].pLightTgtGizmo->Activate();
                }
                else
                {
                    m_LightManipulators[idx].pLightSrcGizmo->Deactivate();
                    m_LightManipulators[idx].pLightTgtGizmo->Deactivate();
                }
            }
        }
        break;
    case grcLightGroup::LTTYPE_POINT:
        {
			m_Lights.SetPosition( idx, m_LightManipulators[idx].lightSrcMtx.d );
            //Turn the manipulator drawing on or off.
            if ( m_LightManipulators[idx].pLightSrcGizmo )
            {
                if ( m_DrawLights )
                {
                    m_LightManipulators[idx].pLightSrcGizmo->Activate();
                    m_LightManipulators[idx].pLightTgtGizmo->Deactivate();
                }
                else
                {
                    m_LightManipulators[idx].pLightSrcGizmo->Deactivate();
                    m_LightManipulators[idx].pLightTgtGizmo->Deactivate();
                }
            }
        }
        break;
    default:
        break;
    }
}

bool IsWaitTimeOver( float waitTime )
{
	static sysTimer	Timer;
	return ( waitTime < Timer.GetTime() );
}
void grcSampleManager::TakeSnapShot( XmlLog& NOTFINAL_ONLY(log) , const char* NOTFINAL_ONLY(picName) )
{
#if !__FINAL
	log.GroupStart( "SnapShot");
	log.Write( "Name", ASSET.FileName( sysParam::GetProgramName() ));
	log.Write( "Time", m_ExitDuration);
	m_Setup->TakeSnapShot( log );

	log.Write( "Image", picName);
	log.Write( "ImageName", ASSET.FileName( picName) );
	m_Setup->TakeScreenShot(  picName, true );

	// write warnings
	log.Write("Warnings", diagOutput::GetWarningCount() );

	log.GroupEnd( "SnapShot");
#endif
}


void grcSampleManager::Update()
{
	PF_PUSH_TIMEBAR("Update");

	m_Setup->BeginUpdate();

	// early out so we don't update anything:
	if (WantsExit())
		return;

	rage::INPUT.Update();
	TIME.Update();
	
#if __STATS
    if (!IsPaused())
    {
	    GetRageProfiler().BeginFrame();
    }
	GetRageProfiler().UpdateInput();
	RAGETRACE_FRAMESTART();

	PF_SETTIMER(Draw, s_LastDrawTime);
#endif
	PF_FUNC(Update);

	UpdateCamera();

	{ // scope for PF_FUNC
		PF_FUNC(ClientUpdate);
		UpdateClient();
	}
	GIZMOMGR.Update();

	UpdateLights();

	m_Setup->EndUpdate();

	m_Setup->SetClearColor(m_ClearColor);

	if ( m_ExitDuration == -100.0f ) // signify's duration + 1 frame has ended
	{
		m_WantsExit = true;
	} 
	else if ( m_ExitDuration != 0.0f )		// duration exit
	{
		if  ( IsWaitTimeOver( m_ExitDuration ) )
		{
#if !__FINAL
			const char* picName = 0;
			if ( PARAM_smokesnapshot.Get( picName ))
			{
				XmlFileLog	logFile( picName, "xml", false);
				TakeSnapShot( logFile, picName );
				fprintf(stderr,"picname:%s\n",picName);
			}
#endif
			m_ExitDuration = -100.0f;// wait one extra frame to allow for snap shot.
		}
	}

	PF_POP_TIMEBAR();
}

void grcSampleManager::SetupCamera()
{
	Matrix34 mtx;

	if(m_ViewMode == 0)
	{
		mtx = GetCameraMatrix();
		m_Viewport->Perspective(GetCameraFOV(), 0.0f, m_ClipNear, m_ClipFar);
	}
	else
	{
		mtx = m_OrthoCam.GetWorldMatrix();

		float orthoZoom = m_OrthoCam.GetOrthoZoom();

		int scWidth = GRCDEVICE.GetWidth();
		int scHeight = GRCDEVICE.GetHeight();
		float aspectAdjust = 0.0f;
		
		if(scWidth >= scHeight)
		{
			aspectAdjust = ((float)scWidth / (float)scHeight);
			m_Viewport->Ortho((-orthoZoom*aspectAdjust), (orthoZoom*aspectAdjust), -orthoZoom, orthoZoom, m_OrthoCam.GetNearZ(), m_OrthoCam.GetFarZ());
		}
		else
		{
			aspectAdjust = ((float)scHeight / (float)scWidth);
			m_Viewport->Ortho(-orthoZoom, orthoZoom, (-orthoZoom*aspectAdjust), (orthoZoom*aspectAdjust), m_OrthoCam.GetNearZ(), m_OrthoCam.GetFarZ());
		}
	}

	m_Viewport->SetCameraMtx(mtx);
	m_Viewport->SetWorldMtx(M34_IDENTITY);

	grcViewport::SetCurrent(m_Viewport);
}

void grcSampleManager::Draw()
{
	PF_START(Draw);

	// Don't clear here, want that to be predicated?
	if (!m_Setup->BeginDraw(false))
		return;

	grcEffect::ResetAllGlobalsToDefaults();

	PF_PUSH_TIMEBAR("Draw");

	PF_START_TIMEBAR("PreDrawClient");

	PreDrawClient();

	PF_START_TIMEBAR("SetupCamera");

	SetupCamera();

	PF_START_TIMEBAR("SetFog");

	SetFog();

	PF_START_TIMEBAR("DrawBackground");

	if(m_DrawBackground)
		DrawBackground();

#if __WIN32
	// Clip planes must be respecified after any camera change because they
	// are transformed by the current composite matrix before being sent
	// down to the hardware.
	if (m_UserClip) {
		GRCDEVICE.SetClipPlaneEnable(1 << 0);
		GRCDEVICE.SetClipPlane(0, m_UserClipPlane);
	}
#endif
#if __XENON
	if (m_UseAA)
	{
		grcResolveFlags clearParams;
		clearParams.Color = m_ClearColor;
		GRCDEVICE.BeginTiledRendering(NULL,NULL,&clearParams,m_UseAA);
	}
#endif

	PF_START_TIMEBAR("Clear");

	GRCDEVICE.Clear(true,m_Setup->GetClearColor(),true,1.0f,0);

	//grcDevice * device=0;
	grcState::Default();
	grcLightState::SetEnabled(false);
	grcState::SetDepthTest(false);
	

	PF_START_TIMEBAR("Grids");

	if (m_DrawGrid || m_DrawThreeGrids)
	{
		PIX_AUTO_TAG(0, "Grid");

		if (m_DrawThreeGrids)
		{
			DrawGrid(Vector3(1.0f, 0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f));
			DrawGrid(Vector3(0.0f, 1.0f, 0.0f));
			DrawGrid(Vector3(0.0f, 0.0f, 1.0f));
		}
		else if(m_UseZUp)
		{
			DrawGrid(Vector3(0.0f, 0.0f, 1.0f));
		}
		else
		{
			DrawGrid(Vector3(0.0f, 1.0f, 0.0f));
		}
	}


	grcState::SetDepthTest(true);
	

#if __WIN32
	if (m_UserClip) {
		GRCDEVICE.SetClipPlaneEnable(0);
	}
#endif

	PF_START_TIMEBAR("DrawLights");

	// Draw the lights
	DrawLights();

	PF_START_TIMEBAR("Gizmos");

	// Draw the gizmos
	grcBindTexture(NULL);
	GIZMOMGR.Draw();

	SetLights();

	PF_PUSH_TIMEBAR("DrawClient");

	PF_START(ClientDraw);
	DrawClient();
	PF_STOP(ClientDraw);

	PF_POP_TIMEBAR();

#if __XENON
	if (m_UseAA)
		GRCDEVICE.EndTiledRendering();
#endif

	PF_START_TIMEBAR("STATS");

	// Try to recover from shaderfx rendering
	grcState::Default();	
	
	grcWorldIdentity();

	DrawStatsAndInfo();

	PF_POP_TIMEBAR();	// Draw

	PF_FRAMEEND_TIMEBARS();

	if(m_DisplayTimebars)
	{
		PF_DISPLAY_TIMEBARS();
	}

	m_Setup->EndDraw();
}

void grcSampleManager::SetFog()
{
	// grcState::SetFogParams(m_FogColor,m_FogStart,m_FogEnd,m_FogMin,m_FogMax);
	// grcState::SetFogBlend(m_FogEnable);
}

void grcSampleManager::SetLights()
{
#if __PS2
	grcState::SetBestLightingMode(grclmDirectional);
#else
	grcState::SetBestLightingMode(grclmPoint);
#endif
	grcState::SetLightingMode((grcLightingMode)m_LightMode);
	SetLightingGroup(m_Lights);
}

void grcSampleManager::SetLightingGroup(grcLightGroup &lightGroup)
{
	grcCustomLighting::GetInstance().SetLightingGroup(lightGroup);
}
Color32 grcSampleManager::GetBackGroundColor()
{
	if (m_ColorScheme==SCHEME_MAYA)
		return Color32(161,161,161);
	else if(m_ColorScheme==SCHEME_RAGE)
		return Color32(0,10,100);
	return  m_UserBackgroundColor;
}

void grcSampleManager::DrawBackground()
{
	// just use clear
	GRCDEVICE.Clear(true,GetBackGroundColor(),true,1.0f,0);

	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);
	grcState::SetState(grcsDepthFunc,grccfAlways);
	grcState::SetState(grcsDepthWrite,false);
	grcState::SetState(grcsDepthFunc,grccfLessEqual);
}

void grcSampleManager::DrawGrid(const Vector3& normal, const Vector3& tangent)
{
	const float FLOOR_HEIGHT = 0.0f;

	grcViewport::SetCurrentWorldIdentity();

	Color32 color(0.0f,0.5f,1.0f,0.3f);
	if (m_ColorScheme==SCHEME_MAYA)
		color.Set(128,128,128,255);
	else if(m_ColorScheme==SCHEME_USER)
		color = m_UserGridColor;

	grcBindTexture(NULL);
	grcState::SetDepthWrite(false);

	Color32 colorAxes(0.3f, 0.8f, 1.0f, 0.8f);
	if (m_ColorScheme==SCHEME_MAYA)
		colorAxes.Set(0,0,0,255);
	else if(m_ColorScheme==SCHEME_USER)
		colorAxes = m_UserGridAxisColor;

	Color32 subGridColor(0.0f,0.2f,0.7f,0.3f);
	if (m_ColorScheme==SCHEME_MAYA)
		subGridColor.Set(140,140,140,128);
	else if(m_ColorScheme==SCHEME_USER)
		subGridColor = m_UserSubGridColor;

	Vector3 binormal;
	binormal.Cross(tangent, normal);

	grcNormal3f(0.0f, 1.0f, 0.0f);

	float halfSize = m_GridSize / 2.0f;

	// draw the major axis lines
	Vector3 basePoint, v0, v1;
	basePoint.Scale(normal, FLOOR_HEIGHT);
	
	struct GridVtx { float x, y, z; u32 c; float t,u ;};

	GRCDEVICE.SetDefaultEffect(false,false);
	GRCDEVICE.SetVertexDeclaration(m_GridDecl);

	v0=basePoint;
	v1=basePoint;
	v0.AddScaled(binormal, -halfSize);
	v1.AddScaled(binormal, halfSize);
	GridVtx *majorAxis = (GridVtx*) GRCDEVICE.BeginVertices(drawLineStrip, 2, sizeof(GridVtx));
	if (majorAxis) {
		majorAxis[0].x=v0.x; majorAxis[0].y=v0.y; majorAxis[0].z=v0.z; majorAxis[0].c=colorAxes.GetDeviceColor();
		majorAxis[1].x=v1.x; majorAxis[1].y=v1.y; majorAxis[1].z=v1.z; majorAxis[1].c=colorAxes.GetDeviceColor();
		GRCDEVICE.EndVertices();
	}

	v0=basePoint;
	v1=basePoint;
	v0.AddScaled(tangent, -halfSize);
	v1.AddScaled(tangent, halfSize);

	GridVtx *minorAxis = (GridVtx*) GRCDEVICE.BeginVertices(drawLineStrip, 2, sizeof(GridVtx));
	if (minorAxis) {
		minorAxis[0].x=v0.x; minorAxis[0].y=v0.y; minorAxis[0].z=v0.z; minorAxis[0].c=colorAxes.GetDeviceColor();
		minorAxis[1].x=v1.x; minorAxis[1].y=v1.y; minorAxis[1].z=v1.z; minorAxis[1].c=colorAxes.GetDeviceColor();
		GRCDEVICE.EndVertices();
	}

	int lineVtxCount = (int)((halfSize / m_GridLines)*2);
	
	int subLineVtxCount = 0;
	float subLineStep = 0.f;

	if(m_GridSubLine)
	{
		subLineVtxCount = (int)(((m_GridSubLine-1.f)*2.f) * (halfSize / m_GridLines));
		subLineStep = m_GridLines / m_GridSubLine;
	}

	// draw the z lines...
	grcBegin(drawLines, (lineVtxCount+subLineVtxCount)*2);
		for(float x=0.0f; x<(halfSize+m_GridLines); x+=m_GridLines)
		{
			if(x != 0.0f)
			{
				grcColor(color);

				v0 = basePoint;
				v0.AddScaled(tangent, x);
				v0.AddScaled(binormal, -halfSize);
				grcVertex3f(v0);

				v1 = basePoint;
				v1.AddScaled(tangent, x);
				v1.AddScaled(binormal, halfSize);
				grcVertex3f(v1);

				grcVertex3f(-v0);
				grcVertex3f(-v1);
			}

			if(x<halfSize)
			{
				grcColor(subGridColor);
				for(float xs=1; xs < m_GridSubLine; xs+=1.0f)
				{
					v0 = basePoint;
					v0.AddScaled(tangent, x+(xs*subLineStep));
					v0.AddScaled(binormal, -halfSize);
					grcVertex3f(v0);

					v1 = basePoint;
					v1.AddScaled(tangent, x+(xs*subLineStep));
					v1.AddScaled(binormal, halfSize);
					grcVertex3f(v1);

					grcVertex3f(-v0);
					grcVertex3f(-v1);
				}
			}
		}
	grcEnd();

	//draw the x lines...
	grcBegin(drawLines, (lineVtxCount+subLineVtxCount)*2);
		for(float x=0.0f; x<(halfSize+m_GridLines); x+=m_GridLines)
		{
			if(x!=0.0f)
			{
				grcColor(color);

				v0 = basePoint;
				v0.AddScaled(tangent, -halfSize);
				v0.AddScaled(binormal, x);
				grcVertex3f(v0);

				v1 = basePoint;
				v1.AddScaled(tangent, halfSize);
				v1.AddScaled(binormal, x);
				grcVertex3f(v1);

				grcVertex3f(-v0);
				grcVertex3f(-v1);
			}

			if(x<halfSize)
			{
				grcColor(subGridColor);
				for(float xs=1; xs < m_GridSubLine; xs+=1.0f)
				{
					v0 = basePoint;
					v0.AddScaled(tangent, -halfSize);
					v0.AddScaled(binormal, x+(xs*subLineStep));
					grcVertex3f(v0);

					v1 = basePoint;
					v1.AddScaled(tangent, halfSize);
					v1.AddScaled(binormal, x+(xs*subLineStep));
					grcVertex3f(v1);

					grcVertex3f(-v0);
					grcVertex3f(-v1);
				}
			}
		}
	grcEnd();

	grcState::SetDepthWrite(true);
}

void grcSampleManager::DrawWorldAxes()
{
#if __WIN32PC || __XENON || __PPU
	const float BOTTOM_BUFFER = 22;
	const float SIDE_BUFFER = 22;

	if (m_RenderTarget && m_DepthBuffer) {
		Matrix34 camera(grcViewport::GetCurrentCameraMtx());
		camera.d.SetScaled(camera.c,11.0f);

		grcTextureFactory::GetInstance().LockRenderTarget(0, m_RenderTarget, m_DepthBuffer, grcPositiveX, false);
		GRCDEVICE.Clear(true,Color32(0.0f,0.0f,0.0f,0.0f),true,1.0f,0);

		grcViewport * oldViewport = grcViewport::GetCurrent();
		grcViewport viewport;
		viewport.Perspective(35.0f,1.0f,0.1f,20.0f);
		grcViewport::SetCurrent(&viewport);

		bool oldDepthTest = grcState::GetDepthTest();
		grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
		grcViewport::SetCurrentCameraMtx(camera);
		grcState::SetDepthTest(false);
		
		grcColor4f(0.0f,0.0f,0.0f,0.5f);
		grcDrawSphere(3.1f,M34_IDENTITY,25,false,true);
		grcDrawAxis(3.0f,M34_IDENTITY,true);

		grcTextureFactory::GetInstance().UnlockRenderTarget(0);

		grcState::SetDepthTest(oldDepthTest);
		grcViewport::SetCurrent(oldViewport);

		PUSH_DEFAULT_SCREEN();
		grcBindTexture( m_RenderTarget);
		
		float bottom = GRCDEVICE.GetHeight() - BOTTOM_BUFFER;
		float top = bottom - renderTargetSize;
		float left = SIDE_BUFFER;
		float right = left + renderTargetSize;
		grcBeginQuads(1);
		grcDrawQuadf(left, top, right, bottom, 0.0f, 0, 0, 1, 1, Color32(1.0f,1.0f,1.0f));
		grcEndQuads();
		POP_DEFAULT_SCREEN();

	}
#endif // __WIN32PC || __XENON || __PPU
}

void grcSampleManager::DrawLights()
{
	static char strBuf[255];

	if (m_DrawLights)
	{
		grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
		for (int i=0;i<m_Lights.GetActiveCount();i++)
		{
			grcLightGroup::grcLightType lightType = m_Lights.GetLightType(i);
			if(lightType == grcLightGroup::LTTYPE_DIR)
			{
				grcBindTexture(NULL);
				grcLightState::SetEnabled(false);
				DrawDashedLine(m_LightManipulators[i].lightSrcMtx.d, m_LightManipulators[i].lightTgtMtx.d, 0.1f);

				grcDrawBox(Vector3(0.1f,0.1f,0.1f), m_LightManipulators[i].lightTgtMtx, m_Lights.GetColor(i));

				Matrix34 frustMtx;
				frustMtx.Identity();
				frustMtx.LookAt(m_LightManipulators[i].lightSrcMtx.d, m_LightManipulators[i].lightTgtMtx.d, ZAXIS);
				frustMtx.d = m_LightManipulators[i].lightSrcMtx.d;
				grcDrawFrustum(frustMtx, 0.5f, 0.5f, 0.8f, m_Lights.GetColor(i));

				//Scale the gizmos based on the length between the source and target positions
				Vector3 dirVec = m_LightManipulators[i].lightTgtMtx.d - m_LightManipulators[i].lightSrcMtx.d;
				float srcGizmoSize = dirVec.Mag() / 10.0f;
				srcGizmoSize = Max(srcGizmoSize, 1.0f);
				srcGizmoSize = Min(srcGizmoSize, 5.0f);
				m_LightManipulators[i].pLightSrcGizmo->SetSize( srcGizmoSize );

				if(m_DirLightOrbitTarget)
				{
					const Matrix34 &camMtx = m_CamMgr.GetWorldMtx();
					Vector3 camVec = m_LightManipulators[i].lightTgtMtx.d - camMtx.d;
					float tgtGizmoSize = camVec.Mag() / 10.0f;
					tgtGizmoSize = Max(tgtGizmoSize, 0.3f);
					tgtGizmoSize = Min(tgtGizmoSize, srcGizmoSize);
					m_LightManipulators[i].pLightTgtGizmo->SetSize( tgtGizmoSize );
				}
				else
				{
					m_LightManipulators[i].pLightTgtGizmo->SetSize( srcGizmoSize );
				}
			}
			else if(lightType == grcLightGroup::LTTYPE_POINT)
			{
				grcColor3f(m_Lights.GetColor(i));
				grcDrawSphere(0.1f,m_LightManipulators[i].lightSrcMtx,8,true);
				
				// Draw the falloff radius
				if (m_DrawLightRadii)
				{
					grcDrawSphere(m_Lights.GetFalloff(i), m_LightManipulators[i].lightSrcMtx, 32, true);
				}
			}

			if(m_DrawLightLabels)
			{
				grcViewport::SetCurrentWorldMtx(M34_IDENTITY);
				Vector2 windowPos;
				Vector3 worldPos = m_LightManipulators[i].lightSrcMtx.d;
				worldPos.Add(-0.1f, -0.1f, 0.0f);
				if(grcViewport::GetCurrent()->IsPointSetVisible(&worldPos,1))
				{
					grcViewport::GetCurrent()->Transform(worldPos,windowPos.x,windowPos.y);
					sprintf(strBuf,"Light%d",i);
					grcFont::GetCurrent().DrawScaled(windowPos.x,windowPos.y,0.0f,Color32(255,255,255,128),1.0f,1.0f,strBuf);
				}
			}
		}
	}//End if (m_DrawLights)...
}

void grcSampleManager::DrawLogo()
{
	grcBindTexture(m_RsLogo);

	grcState::SetState(grcsDepthFunc,grccfAlways);
	grcState::SetState(grcsDepthWrite,false);
	grcState::SetState(grcsAlphaBlend,true);

	float endY=GRCDEVICE.GetHeight()-20.0f;
	float startY=endY-m_RsLogo->GetHeight();
    float endX=GRCDEVICE.GetWidth()-30.0f;
    float startX=endX-m_RsLogo->GetWidth();

	PUSH_DEFAULT_SCREEN();
	grcBeginQuads(1);
	grcDrawQuadf(startX,startY,endX,endY,0.0f,0.0f,0.0f,1.0f,1.0f,Color32(255,255,255));
	grcEndQuads();
	POP_DEFAULT_SCREEN();
}

void grcSampleManager::DrawHelp()
{
	PUSH_DEFAULT_SCREEN();
	grcFont::GetCurrent().DrawCharBegin();		// set the font texture once in the outer code

	const float TOP = 20.0f;
	const float LEFT = 30.0f;

	SetDrawStringPos(LEFT, TOP);

    if (!IS_CONSOLE BANK_ONLY(|| bkRemotePacket::IsConnectedToRag()))
    {
        DrawString("Help:  F1, L1", false);
    }
    else
    {
        DrawString("Help:  L1", false);
    }

#if !IS_CONSOLE
	DrawString("Quit:  Alt-F4", false);
#elif __XENON
	DrawString("Quit:  Start+Back", false);
#else
	DrawString("Quit:  Start+Select", false);
#endif

#if __STATS
	if (PFSTATS.HasPages())
#endif
	{
		// Positioned to appear after the "Help: ..." text.  Kinda hackish
		// and could be integrated directly into the original string.
		SetDrawStringPos(LEFT + 180.0f, TOP);
		DrawString("- F2: Stats, Shift-F8: EKG, Shift-F9 & Shift-F10: EKG Scale");
	}
	if (m_DrawCam)
	{
		m_CamMgr.Draw(30.0f, (float)m_CameraHelpY);
	}

	DrawHelpClient();

	grcFont::GetCurrent().DrawCharEnd();		// and finally unset the font texture
	POP_DEFAULT_SCREEN();
}

void grcSampleManager::DrawStats ()
{
#if __STATS
	PFSTATS.Cull();

	// Draw the ekg graphs.
	GetRageProfiler().Draw();
	RAGETRACE_UPDATE();
#endif
}

void grcSampleManager::DrawString(const char* string, bool fadeWithTime)
{
	Color32 black(  0,   0,   0, 255);
	Color32 white(255, 255, 255, 255);

	if (fadeWithTime)
	{
		black.SetAlpha(static_cast<int>(m_CamMgr.GetInfoFadeTime() * 255.0f));
		white.SetAlpha(static_cast<int>(m_CamMgr.GetInfoFadeTime() * 255.0f));
	}

	if (white.GetAlpha() > 0.0f)
	{
		grcFont::GetCurrent().DrawScaled(m_DrawStringPosX + 1, m_DrawStringPosY + 2,0.0f,black,1.0f,1.0f, string);
		grcFont::GetCurrent().DrawScaled(m_DrawStringPosX    , m_DrawStringPosY    ,0.0f,white,1.0f,1.0f, string);
	}

	m_DrawStringPosY += grcFont::GetCurrent().GetHeight();
}

void grcSampleManager::SetDrawStringPos(float x, float y)
{
	m_DrawStringPosX = x;
	m_DrawStringPosY = y;
}

inline parTreeNode* AddBooleanNode(parTreeNode* pParent, const char* tag, bool value)
{
	parTreeNode *pBoolNode; 
	if(value)
		pBoolNode = parTreeNode::CreateStdLeaf(tag, "true", false);
	else
		pBoolNode = parTreeNode::CreateStdLeaf(tag, "false", false);

	if(pParent)
		pBoolNode->AppendAsChildOf(pParent);

	return pBoolNode;
}

inline bool GetBooleanNodeValue(parTreeNode* pNode)
{
	if(strcmp(pNode->GetData(), "true")==0)
		return true;
	else
		return false;
}

inline parTreeNode* AddFloatNode(parTreeNode* pParent, const char* tag, float value)
{
	char tmpBuf[80];
	sprintf(tmpBuf,"%f", value);
	parTreeNode *pFloatNode = parTreeNode::CreateStdLeaf(tag, tmpBuf, false);
	if(pParent)
		pFloatNode->AppendAsChildOf(pParent);
	return pFloatNode;
}

inline float GetFloatNodeValue(parTreeNode *pNode)
{
	float retVal;
	sscanf(pNode->GetData(),"%f", &retVal);
	return retVal;
}

inline parTreeNode* AddIntNode(parTreeNode* pParent, const char* tag, int value)
{
	char tmpBuf[80];
	sprintf(tmpBuf,"%d", value);
	parTreeNode *pIntNode = parTreeNode::CreateStdLeaf(tag, tmpBuf, false);
	if(pParent)
		pIntNode->AppendAsChildOf(pParent);
	return pIntNode;
}

inline int GetIntNodeValue(parTreeNode *pNode)
{
	int retVal;
	sscanf(pNode->GetData(), "%d", &retVal);
	return retVal;
}

inline parTreeNode* AddVector3Node(parTreeNode* pParent, const char* tag, const Vector3& value)
{
	parTreeNode *pVec3Node = rage_new parTreeNode();
	pVec3Node->GetElement().SetName(tag);
	pVec3Node->GetElement().AddAttribute("content", "vector3_array", false, false);
	pVec3Node->SetData(reinterpret_cast<const char*>(&value), sizeof(Vector3));
	if(pParent)
		pVec3Node->AppendAsChildOf(pParent);
	return pVec3Node;
}

inline parTreeNode* AddMatrix34Node(parTreeNode* pParent, const char* tag, const Matrix34& mat)
{
	char tmpBuf[256];

	parTreeNode *pMatrix34Node = rage_new parTreeNode();
	pMatrix34Node->GetElement().SetName(tag);
	pMatrix34Node->GetElement().AddAttribute("content", "ascii", false, false);
	sprintf(tmpBuf, "%f %f %f %f %f %f %f %f %f %f %f %f",
		mat.a.x, mat.a.y, mat.a.z,
		mat.b.x, mat.b.y, mat.b.z,
		mat.c.x, mat.c.y, mat.c.z,
		mat.d.x, mat.d.y, mat.d.z);
	pMatrix34Node->SetData(tmpBuf, (u32) strlen(tmpBuf));
	if(pParent)
		pMatrix34Node->AppendAsChildOf(pParent);
	return pMatrix34Node;
}

inline void GetMatrix34NodeValue(parTreeNode* pNode, Matrix34& retMat)
{
	sscanf(pNode->GetData(), "%f %f %f %f %f %f %f %f %f %f %f %f",
		&retMat.a.x, &retMat.a.y, &retMat.a.z,
		&retMat.b.x, &retMat.b.y, &retMat.b.z,
		&retMat.c.x, &retMat.c.y, &retMat.c.z,
		&retMat.d.x, &retMat.d.y, &retMat.d.z);
}

bool grcSampleManager::LoadLightPreset(parTreeNode* pRootNode, grcSampleLightPreset* pPreset)
{
	int presetVersion;
	parAttribute *pVersionAttr = pRootNode->GetElement().FindAttribute(GRCLIGHTGROUP_VERSION_ATTR);
	if(!pVersionAttr)
		presetVersion = 1;
	else
	{
		pVersionAttr->ConvertToInt();
		presetVersion = pVersionAttr->GetIntValue();
	}
	
	//Load the grcLight group settings
	grcLightGroup *pLightGroup = &pPreset->m_LightGroup;

	//Set the active light count
	parAttribute* pActiveAttr = pRootNode->GetElement().FindAttribute(GRCLIGHTGROUP_ACTIVECOUNT_ATTR);
	Assert(pActiveAttr);
	pLightGroup->SetActiveCount(pActiveAttr->FindIntValue());

	//Set the ambient value
	parAttribute* pAmbientAttr = pRootNode->GetElement().FindAttribute(GRCLIGHTGROUP_AMBIENT_ATTR);
	Assert(pAmbientAttr);
	const char* ambientStr = pAmbientAttr->GetStringValue();
	Vector4 ambientVal;
	sscanf(ambientStr,"%f %f %f %f", &ambientVal.x, &ambientVal.y, &ambientVal.z, &ambientVal.w);
	pLightGroup->SetAmbient(ambientVal);

	if(presetVersion >=  2)
	{
		parTreeNode *pNode = NULL;

		//Lighting Mode
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_LIGHTMODE_PARAM);
		if(pNode)
			pPreset->m_LightMode = GetIntNodeValue(pNode);

		//Draw Lights flag
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_DRAWLIGHTS_PARAM);
		if(pNode)
			pPreset->m_DrawLights = GetBooleanNodeValue(pNode);

		//Draw Light Labels flag
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_DRAWLIGHTLABEL_PARAM);
		if(pNode)
			pPreset->m_DrawLightLabels = GetBooleanNodeValue(pNode);

		//Draw Light radius flag
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_DRAWLIGHTRADIUS_PARAM);
		if(pNode)
			pPreset->m_DrawLightRadii = GetBooleanNodeValue(pNode);

		//Animate radius
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_ANIMRADIUS_PARAM);
		if(pNode)
			pPreset->m_LightRadius = GetFloatNodeValue(pNode);

		//Animate lights flag
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_ANIMATELIGHTS_PARAM);
		if(pNode)
			pPreset->m_AnimateLights = GetBooleanNodeValue(pNode);

		//Animation rate
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_ANIMATERATE_PARAM);
		if(pNode)
			pPreset->m_LightAnimateRate = GetFloatNodeValue(pNode);

		//Orbit light around target
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_ORBITTARGET_PARAM);
		if(pNode)
			pPreset->m_DirLightOrbitTarget = GetBooleanNodeValue(pNode);

		//Force Color
		pNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_FORCECOLOR_PARAM);
		if(pNode)
		{
			int r,g,b,a;
			const char* forceColorStr = pNode->GetData();
			sscanf(forceColorStr,"%d %d %d %d", &r, &g, &b, &a);
			pPreset->m_ForceColor.Set(r,g,b,a);
		}

	}

	parTreeNode *pLightGroupStructListNode = pRootNode->FindChildWithName(GRCLIGHTGROUP_LIGHT_STRUCT_LIST);

	for(int i=0; i<pLightGroup->GetActiveCount(); i++)
	{
		parTreeNode *pLightNode = NULL;
		if(presetVersion == 1)
		{
			pLightNode = pRootNode->FindChildWithIndex(i);
			Assert(pLightNode);
		}
		else
		{
			pLightNode = pLightGroupStructListNode->FindChildWithIndex(i);
			Assert(pLightNode);
		}

		//Position
		parTreeNode *pPosNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_POSITION_PARAM);
		Assert(pPosNode);
        Vector3 pos;
        memcpy( &pos, vector_cast<Vector3*>(pPosNode->GetData()), sizeof(pos) );
		pLightGroup->SetPosition(i, pos);

		//Direction
		parTreeNode *pDirNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_DIRECTION_PARAM);
		Assert(pDirNode);
        Vector3 dir;
        memcpy( &dir, vector_cast<Vector3*>(pDirNode->GetData()), sizeof(dir) );
		pLightGroup->SetDirection(i, dir);

		//Color
		parTreeNode *pClrNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_COLOR_PARAM);
		Assert(pClrNode);
        Vector3 clr;
        memcpy( &clr, vector_cast<Vector3*>(pClrNode->GetData()), sizeof(clr) );
		pLightGroup->SetColor(i, clr);

		//Intensity
		parTreeNode *pIntNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_INTENSITY_PARAM);
		Assert(pIntNode);
		float intensity = GetFloatNodeValue(pIntNode);
		pLightGroup->SetIntensity(i, intensity);

		//Falloff
		parTreeNode *pFallNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_FALLOFF_PARAM);
		Assert(pFallNode);
		float falloff = GetFloatNodeValue(pFallNode);
		pLightGroup->SetFalloff(i, falloff);

		//Type
		parTreeNode *pTypeNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_TYPE_PARAM);
		Assert(pTypeNode);
		int type = GetIntNodeValue(pTypeNode);
		pLightGroup->SetLightType(i, (grcLightGroup::grcLightType)type);

		//Oversaturate
		parTreeNode *pOverNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_OVERSATURATE_PARAM);
		Assert(pOverNode);
		pLightGroup->SetOversaturate(i, GetBooleanNodeValue(pOverNode));
		
		//Negative
		parTreeNode *pNegNode = pLightNode->FindChildWithName(GRCLIGHTGROUP_NEGATIVE_PARAM);
		Assert(pNegNode);
		pLightGroup->SetNegative(i, GetBooleanNodeValue(pNegNode));
	}

	return true;
}

bool grcSampleManager::LoadLightPreset(const char* fileName, grcSampleLightPreset* pPreset)
{
	Assert(pPreset);
	Assert(fileName);

	//Load the XML "lgroup" file
	parTree* pTree = PARSER.LoadTree(fileName, "");
	if(!pTree)
		return false;

	parTreeNode *pRootNode = pTree->GetRoot();
	if(strcmp(pRootNode->GetElement().GetName(), GRCLIGHTGROUP_STRUCTURE) != 0)
	{
		Errorf("Failed to load light group file '%s', file is not a valid light group file.", fileName);
		return false;
	}

	bool bRes =  LoadLightPreset(pRootNode, pPreset);

	if (bRes)
		pPreset->m_FileName = fileName;

	delete pTree;
	return bRes;
}

bool grcSampleManager::SaveLightPreset(parTree *pTree, parTreeNode* pRootNode) const 
{
	static char tmpBuf[255];

	const grcLightGroup* pLightGroup = &m_Lights;

	if(!pRootNode)
	{
        Assert( pTree );
		pRootNode = pTree->CreateRoot();
		pTree->SetRoot(pRootNode);
	}

	parElement &pRootElm = pRootNode->GetElement();

	pRootElm.SetName(GRCLIGHTGROUP_STRUCTURE);
	pRootElm.AddAttribute(GRCLIGHTGROUP_VERSION_ATTR, GRCLIGHTGROUP_VERSION, false, false);
	pRootElm.AddAttribute(GRCLIGHTGROUP_MAXLIGHTS_ATTR, grcLightGroup::MAX_LIGHTS);
	pRootElm.AddAttribute(GRCLIGHTGROUP_ACTIVECOUNT_ATTR, pLightGroup->GetActiveCount());

	const Vector4 &ambient = pLightGroup->GetAmbient();
	sprintf(tmpBuf,"%f %f %f %f", ambient.x, ambient.y, ambient.z, ambient.w);
	pRootElm.AddAttribute(GRCLIGHTGROUP_AMBIENT_ATTR, tmpBuf);

	//Lighting mode
	AddIntNode(pRootNode, GRCLIGHTGROUP_LIGHTMODE_PARAM, m_LightMode);

	//Draw Lights flag
	AddBooleanNode(pRootNode, GRCLIGHTGROUP_DRAWLIGHTS_PARAM, m_DrawLights);

	//Draw Light Labels flag
	AddBooleanNode(pRootNode, GRCLIGHTGROUP_DRAWLIGHTLABEL_PARAM, m_DrawLightLabels);

	//Draw Light Radius flag
	AddBooleanNode(pRootNode, GRCLIGHTGROUP_DRAWLIGHTRADIUS_PARAM, m_DrawLightRadii);

	//Animate radius 
	AddFloatNode(pRootNode, GRCLIGHTGROUP_ANIMRADIUS_PARAM, m_LightRadius);

	//Animate Lights Flag
	AddBooleanNode(pRootNode, GRCLIGHTGROUP_ANIMATELIGHTS_PARAM, m_AnimateLights);

	//Animation rate
	AddFloatNode(pRootNode, GRCLIGHTGROUP_ANIMATERATE_PARAM, m_LightAnimateRate);

	//Orbit light around target
	AddBooleanNode(pRootNode, GRCLIGHTGROUP_ORBITTARGET_PARAM, m_DirLightOrbitTarget);

	//Force color
	sprintf(tmpBuf,"%d %d %d %d", m_ForceColor.GetRed(), m_ForceColor.GetGreen(), m_ForceColor.GetBlue(), m_ForceColor.GetAlpha());
	parTreeNode *pForceColorNode = parTreeNode::CreateStdLeaf(GRCLIGHTGROUP_FORCECOLOR_PARAM, tmpBuf);
	pForceColorNode->AppendAsChildOf(pRootNode);

	//Light list
	parTreeNode *pLightListNode = rage_new parTreeNode();
	pLightListNode->GetElement().SetName(GRCLIGHTGROUP_LIGHT_STRUCT_LIST);
	pLightListNode->AppendAsChildOf(pRootNode);
	for(int i=0; i<pLightGroup->GetActiveCount(); i++)
	{
		parTreeNode *pLightNode = rage_new parTreeNode();
		parElement& lightElm = pLightNode->GetElement();
		lightElm.SetName(GRCLIGHTGROUP_LIGHT_STRUCTURE);
		lightElm.AddAttribute(GRCLIGHTGROUP_ID_PARAM, i);

		//Position
		AddVector3Node(pLightNode, GRCLIGHTGROUP_POSITION_PARAM, pLightGroup->GetPosition(i));

		//Direction
		AddVector3Node(pLightNode, GRCLIGHTGROUP_DIRECTION_PARAM, pLightGroup->GetDirection(i));

		//Color
		AddVector3Node(pLightNode, GRCLIGHTGROUP_COLOR_PARAM, pLightGroup->GetColor(i));

		//Intensity
		AddFloatNode(pLightNode, GRCLIGHTGROUP_INTENSITY_PARAM, pLightGroup->GetIntensity(i));

		//Falloff
		AddFloatNode(pLightNode, GRCLIGHTGROUP_FALLOFF_PARAM, pLightGroup->GetFalloff(i));

		//Type
		AddIntNode(pLightNode, GRCLIGHTGROUP_TYPE_PARAM, (int)pLightGroup->GetLightType(i));

		//Oversaturate
		AddBooleanNode(pLightNode, GRCLIGHTGROUP_OVERSATURATE_PARAM, pLightGroup->IsOversaturate(i)); 

		//Negative
		AddBooleanNode(pLightNode, GRCLIGHTGROUP_NEGATIVE_PARAM, pLightGroup->IsNegative(i));

		//Add the light node
		pLightNode->AppendAsChildOf(pLightListNode);
	}
	return true;
}

bool grcSampleManager::SaveLightPreset(const char* fileName) const
{
	Assert(fileName);

	parTree* pTree = rage_new parTree();
	SaveLightPreset(pTree, NULL);

	//Save the tree as an XML file
	bool bSaveRes = PARSER.SaveTree(fileName,"lgroup",pTree,parManager::XML);
	if(!bSaveRes)
		Errorf("Failed to save light group file '%s'.",fileName);

	delete pTree;

	return bSaveRes;
}

void grcSampleManager::SetLightPreset(const grcSampleLightPreset* pLightPreset)
{
	m_Lights = pLightPreset->m_LightGroup;
	m_LightMode = pLightPreset->m_LightMode;
	m_DrawLights = pLightPreset->m_DrawLights;
	m_DrawLightLabels = pLightPreset->m_DrawLightLabels;
	m_DrawLightRadii = pLightPreset->m_DrawLightRadii;
	m_LightRadius = pLightPreset->m_LightRadius;
	m_AnimateLights = pLightPreset->m_AnimateLights;
	m_LightAnimateRate = pLightPreset->m_LightAnimateRate;
	m_DirLightOrbitTarget = pLightPreset->m_DirLightOrbitTarget;
	m_ForceColor = pLightPreset->m_ForceColor;

	InitLightManipulators();
}

dcamCamMgr::eCamera CameraStringToType(const char* camString)
{
	for(int i=0; i<(int)dcamCamMgr::CAM_COUNT; i++)
	{
		if(strcmpi(camString, camTypeEnumToString[i])==0)
			return (dcamCamMgr::eCamera)i;
	}
	return dcamCamMgr::CAM_COUNT;
}

grcSampleOrthoCam::OrthoMode OrthoCameraStringToType(const char* camString)
{
	for(int i=0; i<(int)grcSampleOrthoCam::ORTHO_MODE_COUNT; i++)
	{
		if(strcmpi(camString, orthoCamTypeEnumToString[i])==0)
			return (grcSampleOrthoCam::OrthoMode)i;
	}
	return grcSampleOrthoCam::ORTHO_MODE_COUNT;
}

bool grcSampleManager::SaveCameraPreset(const char* fileName) const
{
	Assert(fileName);

	parTree* pTree = rage_new parTree();
	SaveCameraPreset(pTree, NULL);

	//Save the tree as an XML file
	bool bSaveRes = PARSER.SaveTree(fileName,"camset",pTree,parManager::XML);
	if(!bSaveRes)
		Errorf("Failed to save camera preset file '%s'.",fileName);

	delete pTree;

	return bSaveRes;
}

bool grcSampleManager::SaveCameraPreset(parTree* pTree, parTreeNode* pRootNode) const
{
	if(!pRootNode)
	{
        Assert(pTree);

		//The preset is being written into its own preset file, so create the root node
		pRootNode = pTree->CreateRoot();
		pTree->SetRoot(pRootNode);
	}

	parElement &rootElm = pRootNode->GetElement();
	rootElm.SetName(GRCCAMPRESET_STRUCTURE);
	rootElm.AddAttribute(GRCCAMPRESET_VERSION_ATTR, GRCCAMPRESET_VERSION, false, false);
	
	//Current DevCamera Type
	parTreeNode *pCurCamNode = parTreeNode::CreateStdLeaf(GRCCAMPRESET_CURCAM_PARAM,camTypeEnumToString[m_CamMgr.GetCurrentCamera()]);
	pCurCamNode->AppendAsChildOf(pRootNode);

	//Current view mode
	AddIntNode(pRootNode, GRCCAMPRESET_VIEWMODE_PARAM, m_ViewMode);

	//FOV
	AddFloatNode(pRootNode, GRCCAMPRESET_FOV_PARAM, m_CamFOV);

	//Near Clip Plane
	AddFloatNode(pRootNode, GRCCAMPRESET_NEARCLIP_PARAM, m_ClipNear);

	//Far Clip Plane
	AddFloatNode(pRootNode, GRCCAMPRESET_FARCLIP_PARAM, m_ClipFar);

	//Dev Cam Camera list
	parTreeNode *pCameraListNode = rage_new parTreeNode();
	pCameraListNode->GetElement().SetName(GRCCAMPRESET_DEVCAM_STRUCT_LIST);
	pCameraListNode->GetElement().AddAttribute(GRCCAMPRESET_CAMERA_COUNT_ATTR, (int)dcamCamMgr::CAM_COUNT);
	pCameraListNode->AppendAsChildOf(pRootNode);

	for(int i=0; i<dcamCamMgr::CAM_COUNT; i++)
	{
		dcamCam& curCam = (const_cast<dcamCamMgr*>(&m_CamMgr))->GetCamera((dcamCamMgr::eCamera)i);

		parTreeNode *pCamNode = rage_new parTreeNode();
		pCamNode->GetElement().SetName(GRCCAMPRESET_DEVCAM_STRUCTURE);
		pCamNode->GetElement().AddAttribute(GRCCAMPRESET_CAMTYPE_ATTR, camTypeEnumToString[i]);

		const Matrix34 &mat = curCam.GetWorldMtx();
		AddMatrix34Node(pCamNode, GRCCAMPRESET_WORLDMAT_PARAM, mat);
		
		pCamNode->AppendAsChildOf(pCameraListNode);
	}

	//Ortho Cams
	parTreeNode *pOrthoCamListNode = rage_new parTreeNode();
	pOrthoCamListNode->GetElement().SetName(GRCCAMPRESET_ORTHOCAM_STRUCT_LIST);
	pOrthoCamListNode->GetElement().AddAttribute(GRCCAMPRESET_CAMERA_COUNT_ATTR, (int)grcSampleOrthoCam::ORTHO_MODE_COUNT);
	pOrthoCamListNode->AppendAsChildOf(pRootNode);

	for(int i=0; i<grcSampleOrthoCam::ORTHO_MODE_COUNT; i++)
	{
		parTreeNode *pOrthoCamNode = rage_new parTreeNode();
		pOrthoCamNode->GetElement().SetName(GRCCAMPRESET_ORTHOCAM_STRUCTURE);
		pOrthoCamNode->GetElement().AddAttribute(GRCCAMPRESET_CAMTYPE_ATTR, orthoCamTypeEnumToString[i]);

		AddMatrix34Node(pOrthoCamNode, GRCCAMPRESET_WORLDMAT_PARAM, m_OrthoCam.GetWorldMatrix(i));
		AddFloatNode(pOrthoCamNode, GRCCAMPRESET_ORTHO_ZOOM_PARAM, m_OrthoCam.GetOrthoZoom(i));

		pOrthoCamNode->AppendAsChildOf(pOrthoCamListNode);
	}

	return true;
}

bool grcSampleManager::LoadCameraPreset(const char* fileName, grcSampleCameraPreset* pCamPreset) const
{
	//Load the XML "camset" file
	parTree* pTree = PARSER.LoadTree(fileName, "");
	if(!pTree)
		return false;

	parTreeNode *pRootNode = pTree->GetRoot();
	if(strcmp(pRootNode->GetElement().GetName(), GRCCAMPRESET_STRUCTURE) != 0)
	{
		Errorf("Failed to load camera preset file '%s', file is not a valid camera preset file.", fileName);
		return false;
	}
	
	bool bRes = LoadCameraPreset(pRootNode, pCamPreset);

	if (bRes)
		pCamPreset->m_FileName = fileName;

	delete pTree;
	return bRes;
}

bool grcSampleManager::LoadCameraPreset(parTreeNode* pRootNode, grcSampleCameraPreset* pCamPreset) const
{
	/* int presetVesion;
	parAttribute* pVersionAttr = pRootNode->GetElement().FindAttribute(GRCCAMPRESET_VERSION_ATTR);
	if(!pVersionAttr)
		presetVesion = 1;
	else
	{
		pVersionAttr->ConvertToInt();
		presetVesion = pVersionAttr->GetIntValue();
	} */

	//Current dev camera type
	parTreeNode *pCurCamNode = pRootNode->FindChildWithName(GRCCAMPRESET_CURCAM_PARAM);
	if(pCurCamNode)
	{
		const char* curCamName = pCurCamNode->GetData();
		dcamCamMgr::eCamera curCamType = CameraStringToType(curCamName);
		if(curCamType == dcamCamMgr::CAM_COUNT)
		{
			Errorf("Unknown camera type '%s'", curCamName );
			return false;
		}
		pCamPreset->m_CurrentDevCamera = curCamType;
	}

	//Current view mode
	parTreeNode *pCurViewModeNode = pRootNode->FindChildWithName(GRCCAMPRESET_VIEWMODE_PARAM);
	if(pCurViewModeNode)
		pCamPreset->m_ViewMode = GetIntNodeValue(pCurViewModeNode);

	//FOV
	parTreeNode *pFovNode = pRootNode->FindChildWithName(GRCCAMPRESET_FOV_PARAM);
	if(pFovNode)
		pCamPreset->m_Fov = GetFloatNodeValue(pFovNode);

	//Near Clip
	parTreeNode *pNearClipNode = pRootNode->FindChildWithName(GRCCAMPRESET_NEARCLIP_PARAM);
	if(pNearClipNode)
		pCamPreset->m_ClipNear = GetFloatNodeValue(pNearClipNode);
		
	//Far Clip
	parTreeNode *pFarClipNode = pRootNode->FindChildWithName(GRCCAMPRESET_FARCLIP_PARAM);
	if(pFarClipNode)
		pCamPreset->m_ClipFar = GetFloatNodeValue(pFarClipNode);
		
	//Dec Cam Camera List
	parTreeNode *pDevCamListNode = pRootNode->FindChildWithName(GRCCAMPRESET_DEVCAM_STRUCT_LIST);
	if(pDevCamListNode)
	{
		parTreeNode::ChildNodeIterator cIter = pDevCamListNode->BeginChildren();
		while(cIter != pDevCamListNode->EndChildren())
		{
			parTreeNode *pChild = *cIter;
		
			parAttribute *pCamTypeAttr = pChild->GetElement().FindAttribute(GRCCAMPRESET_CAMTYPE_ATTR);
			dcamCamMgr::eCamera curCamType = CameraStringToType(pCamTypeAttr->GetStringValue());

			if( curCamType != dcamCamMgr::CAM_COUNT)
			{
				parTreeNode* pWrldMtxNode = pChild->FindChildWithName(GRCCAMPRESET_WORLDMAT_PARAM);
				Matrix34 camMat;
				GetMatrix34NodeValue(pWrldMtxNode, camMat);
				pCamPreset->m_CamMats[curCamType] = camMat;
			}

			++cIter;
		}
	}
	
	//Ortho Cam
	parTreeNode *pOrthoCamNode = pRootNode->FindChildWithName(GRCCAMPRESET_ORTHOCAM_STRUCT_LIST);
	if(pOrthoCamNode)
	{
		parTreeNode::ChildNodeIterator cIter =  pOrthoCamNode->BeginChildren();
		while(cIter != pOrthoCamNode->EndChildren())
		{
			parTreeNode *pChild = *cIter;

			parAttribute *pTypeAttr = pChild->GetElement().FindAttribute(GRCCAMPRESET_CAMTYPE_ATTR);
			grcSampleOrthoCam::OrthoMode camIdx = OrthoCameraStringToType(pTypeAttr->GetStringValue());
			
			parTreeNode *pNode = pChild->FindChildWithName(GRCCAMPRESET_WORLDMAT_PARAM);
			Matrix34 camMat;
			GetMatrix34NodeValue(pNode, camMat);
			pCamPreset->m_OrthoMats[camIdx] = camMat;

			pNode = pChild->FindChildWithName(GRCCAMPRESET_ORTHO_ZOOM_PARAM);
			pCamPreset->m_OrthoZooms[camIdx] = GetFloatNodeValue(pNode);

			++cIter;
		}
	}//End if(pOrthoCamNode)

	return true;
}

void grcSampleManager::SetCameraPreset(grcSampleCameraPreset *pCamPreset)
{
	Assert(pCamPreset);

	m_CamMgr.SetCamera(pCamPreset->m_CurrentDevCamera);

	m_ViewMode = pCamPreset->m_ViewMode;
	m_ClipNear = pCamPreset->m_ClipNear;
	m_ClipFar = pCamPreset->m_ClipFar;
	m_CamFOV = pCamPreset->m_Fov;

	atMap<rage::dcamCamMgr::eCamera, Matrix34>::Iterator it = pCamPreset->m_CamMats.CreateIterator();
	it.Start();
	while(!it.AtEnd())
	{
		dcamCam& cam = m_CamMgr.GetCamera(it.GetKey());
		cam.SetWorldMtx(it.GetData());
		it.Next();
	}

	rage::atMap<grcSampleOrthoCam::OrthoMode, rage::Matrix34>::Iterator orthoMatIt = pCamPreset->m_OrthoMats.CreateIterator();
	orthoMatIt.Start();
	while(!orthoMatIt.AtEnd())
	{
		m_OrthoCam.SetWorldMatrix((int)orthoMatIt.GetKey(), orthoMatIt.GetData());
		orthoMatIt.Next();
	}

	rage::atMap<grcSampleOrthoCam::OrthoMode, float>::Iterator orthoZoomIt = pCamPreset->m_OrthoZooms.CreateIterator();
	orthoZoomIt.Start();
	while(!orthoZoomIt.AtEnd())
	{
		m_OrthoCam.SetOrthoZoom((int)orthoZoomIt.GetKey(), orthoZoomIt.GetData());
		orthoZoomIt.Next();
	}
}

#if __BANK
void grcSampleManager::AddWidgetsGfxSubsystem() {
	bkBank &B = BANKMGR.CreateBank("rage - Gfx");
	m_Setup->AddWidgets(B);
	B.AddSlider("Enable AA", &m_UseAA,0,4,1);
	B.AddToggle("Triple Grids", &m_DrawThreeGrids);
	
	grcCustomLighting::GetInstance().AddWidgets();
}

void grcSampleManager::AddWidgetsCamera()
{
	bkBank &bk = BANKMGR.CreateBank("rage - Camera");
	m_CamMgr.AddWidgets(bk);
	
	bk.AddSlider("FOV", &m_CamFOV, 0.0f, 180.0f, 1.0f);

	static const char* viewModeList[] = {"Perspective","Top", "Bottom", "Left", "Right", "Front", "Back"};
	bk.AddCombo("Views", &m_ViewMode, 7, viewModeList);

	static const char* schemes[] = {"Maya", "RAGE", "User"};
	bk.AddCombo("Color Schemes",(int*)(void*)&m_ColorScheme,3,schemes);

	bk.AddColor("Background Color", &m_UserBackgroundColor);
	bk.AddColor("Grid Color", &m_UserGridColor);
	bk.AddColor("Grid Axis Color", &m_UserGridAxisColor);
	bk.AddColor("Grid Cell Subdivision Color", &m_UserSubGridColor);

	bk.AddSlider("Grid Cell Subdivisions", &m_GridSubLine, 1, 10, 1);

	bk.AddButton("Save Camera Preset", datCallback(MFA(grcSampleManager::SaveCameraPresetBankCbk),this));
	bk.AddButton("Load Camera Preset", datCallback(MFA(grcSampleManager::LoadCameraPresetBankCbk),this));
}

void grcSampleManager::AddWidgetsCull()
{	
	bkBank& bank = BANKMGR.CreateBank("rage - Viewport");
	bank.AddSlider("Nearclip",&m_ClipNear,0.000f,1000.0f,0.001f);
	bank.AddSlider("Farclip",&m_ClipFar,1,10000,10);

	bank.AddToggle("Userclip",&m_UserClip);
    bank.AddVector( "Plane", &m_UserClipPlane, -1000.0f, 1000.0f, 0.1f );

	m_Viewport->AddWidgets(bank);
}

void grcSampleManager::AddWidgetsFog()
{
	bkBank &bank = BANKMGR.CreateBank("rage - Fog");
	bank.AddToggle("Enable",&m_FogEnable);
	bank.AddSlider("Start",&m_FogStart,-500,500,1);
	bank.AddSlider("End",&m_FogEnd,10,5000,10);
	bank.AddSlider("Min",&m_FogMin,0,1,0.01f);
	bank.AddSlider("Max",&m_FogMax,0,1,0.01f);
	bank.AddColor("Color",&m_FogColor);
}

void grcSampleManager::AddWidgetsLights()
{
	bkBank &bank = BANKMGR.CreateBank("rage - Lights");
	static const char *names[] = { "None", "Directional", "Point" };
	bank.AddCombo("Mode",&m_LightMode,3,names);
	bank.AddToggle("Draw Lights",&m_DrawLights);
	bank.AddToggle("Draw Light Labels", &m_DrawLightLabels);
	bank.AddToggle("Draw Light Radii",&m_DrawLightRadii);

	bank.AddToggle("Animate Lights",&m_AnimateLights);
	bank.AddSlider("Animate Light Radius",&m_LightRadius,0.0f,10000.0f,1.0f);
	bank.AddSlider("Animate Rate",&m_LightAnimateRate,0.0f,10000.0f,1.0f);

	//bank.AddToggle("Dir Light Orbit Target", &m_DirLightOrbitTarget, datCallback(MFA(grcSampleManager::DirLightOrbitTargetChanged),this));

	m_Lights.AddWidgets(bank);
	//bank.AddColor("Force Color",&m_ForceColor);

	bank.AddButton("Save Light Preset", datCallback(MFA(grcSampleManager::SaveLightPresetBankCbk),this));
	bank.AddButton("Load Light Preset", datCallback(MFA(grcSampleManager::LoadLightPresetBankCbk),this));

	//Load the list of available light presets
	const char** presetNames;
	int numPresets = LoadDefaultLightPresets(presetNames);
	bank.AddCombo("Light Presets", &m_curLightPresetIdx, numPresets, reinterpret_cast<const char**>(presetNames),
		datCallback(MFA(grcSampleManager::LightPresetChangedBankCbk),this));
	for (int i = 0; i<numPresets; i++)
        StringFree(presetNames[i]);
    delete [] presetNames;
}

void grcSampleManager::SaveCameraPresetBankCbk()
{
    char selFileName[256];
    memset( selFileName, 0, 256 );

	if ( BANKMGR.OpenFile(selFileName,256,"*.camset", true, "Camera Presets (*.camset)") )
	{
		//The BANKMGR doesn't force an extension if one isn't provided, so make sure its there...
		atString camFileName = selFileName;
		const char* ext = fiAssetManager::FindExtensionInPath((const char*)camFileName);
		if(!ext)
			camFileName += ".camset";

		//Save the camera preset data
		SaveCameraPreset((const char*)camFileName);
	}
}

void grcSampleManager::LoadCameraPresetBankCbk()
{
    char selFileName[256];
    memset( selFileName, 0, 256 );

	if ( BANKMGR.OpenFile(selFileName,256,"*.camset", false, "Camera Presets (*.camset)") )
	{
		//Load the selected camera preset file
		grcSampleCameraPreset *pCamPreset = rage_new grcSampleCameraPreset();
		LoadCameraPreset(selFileName, pCamPreset);
		SetCameraPreset(pCamPreset);
		delete pCamPreset;
	}
}

void grcSampleManager::SaveLightPresetBankCbk()
{
	char fullPresetPath[256];
	memset(fullPresetPath, 0, 256);

	if(BANKMGR.OpenFile(fullPresetPath, 256, "*.lgroup", true, "Light Presets (*.lgroup)"))
	{
		//The BANKMGR doesn't force an extension if one isn't provided, so make sure its there...
		atString lightFileName = fullPresetPath;
		const char* ext = fiAssetManager::FindExtensionInPath((const char*)lightFileName);
		if(!ext)
			lightFileName += ".lgroup";

		//Save the light group data
		SaveLightPreset((const char*)lightFileName);
	}
}

void grcSampleManager::LoadLightPresetBankCbk()
{
	char fullPresetPath[256];
	memset(fullPresetPath, 0, 256);

	if(BANKMGR.OpenFile(fullPresetPath, 256, "*.lgroup", false, "Light Presets (*.lgroup)"))
	{
		grcSampleLightPreset lightPreset;

		//Load the selected light preset file
		LoadLightPreset(fullPresetPath, &lightPreset);
		SetLightPreset(&lightPreset);
	}
}

static void EnumerateLightPresetFileFn(const fiFindData& fileFind, void* user)
{
	const char* ext = fiAssetManager::FindExtensionInPath(fileFind.m_Name);
	if(ext && (strcmpi(ext, ".lgroup")==0))
	{
		atArray<atString> *fileList = static_cast<atArray<atString>*>(user);
		char baseFileName[256];
		fiAssetManager::BaseName(baseFileName, 256, fileFind.m_Name);
		fileList->PushAndGrow(baseFileName);
	}
}

int grcSampleManager::LoadDefaultLightPresets(const char**& retPresetNames)
{
	fiAssetManager fiMgr;		
	atArray<atString> fileList;
	atString presetDir = GetFullAssetPath();
		presetDir += "lightPresets";
		fiMgr.SetPath(presetDir);

	fiMgr.EnumFiles("", EnumerateLightPresetFileFn, static_cast<void*>(&fileList));
	int numPresets = fileList.GetCount();
	
	retPresetNames = rage_new const char*[numPresets];
	for(int i=0; i<numPresets; i++)
	{
		retPresetNames[i] = StringDuplicate((const char*)fileList[i]);

		//Load the light group preset
		char fullPath[256];
		sprintf(fullPath, "%s/%s.lgroup", (const char*)presetDir, retPresetNames[i]);
		grcSampleLightPreset &preset = m_LightGroupPresets.Grow();
		
		if(!LoadLightPreset(fullPath, &preset))
			Errorf("Failed to load light preset file `%s`, preset will be set to the default.", fullPath);
	}
	return numPresets;
}

void grcSampleManager::LightPresetChangedBankCbk()
{
	//Copy the preset into the user group
	SetLightPreset(&m_LightGroupPresets[m_curLightPresetIdx]);
}

void grcSampleManager::DirLightOrbitTargetChanged()
{
	if(m_DirLightOrbitTarget)
	{
		//Switch the target gizmo for all directional lights to a rotate gizmo
		for(int i=0; i<m_Lights.GetActiveCount(); i++)
		{
			grcLightGroup::grcLightType type = m_Lights.GetLightType(i);
			if(type == grcLightGroup::LTTYPE_DIR)
			{
				grcSampleLightManipulator &manip = m_LightManipulators[i];
				if(manip.pLightTgtGizmo)
					delete manip.pLightTgtGizmo;

				manip.pLightTgtGizmo = rage_new gzRotation(manip.lightTgtMtx, Functor0::NullFunctor(), MakeFunctor(*this, &grcSampleManager::DirLightOrbitTargetGizmoDragged));

				//Adjust the manipulator matrix so the z-axis is pointing in the same direction as the light
				manip.lightTgtMtx.LookAt(m_LightManipulators[i].lightTgtMtx.d,m_LightManipulators[i].lightSrcMtx.d);				
			}
		}
	}
	else
	{
		//Switch the target gizmo for all directional lights to a translate gizmo
		for(int i=0; i<m_Lights.GetActiveCount(); i++)
		{
			grcLightGroup::grcLightType type = m_Lights.GetLightType(i);
			if(type == grcLightGroup::LTTYPE_DIR)
			{
				grcSampleLightManipulator &manip = m_LightManipulators[i];
				if(manip.pLightTgtGizmo)
					delete manip.pLightTgtGizmo;

				manip.pLightTgtGizmo = rage_new gzTranslation(manip.lightTgtMtx);

				//Return the rotation portion of the matrix to the identity
				manip.lightTgtMtx.a = Vector3(1.0f, 0.0f, 0.0f);
				manip.lightTgtMtx.b = Vector3(0.0f, 1.0f, 0.0f);
				manip.lightTgtMtx.c = Vector3(0.0f, 0.0f, 1.0f);
			}
		}
	}
}

#endif

void grcSampleManager::DirLightOrbitTargetGizmoDragged()
{
	for (int i = 0; i < grcLightGroup::MAX_LIGHTS; ++i)
	{
		grcSampleLightManipulator &manip = m_LightManipulators[i];

		if (m_Lights.GetLightType(i) == grcLightGroup::LTTYPE_DIR)
		{
			Vector3 dir;
			dir.Subtract(manip.lightSrcMtx.d, manip.lightTgtMtx.d);
			float mag = dir.Mag();

			Vector3 gizmoDir = manip.lightTgtMtx.c;
			gizmoDir.Normalize();

			Vector3 newSrcPos = manip.lightTgtMtx.d;
			newSrcPos.AddScaled(-gizmoDir, mag);
			manip.lightSrcMtx.d = newSrcPos;
		}
	}
}

void grcSampleManager::DrawStatsAndInfo()
{
#if __STATS
	if (!IsPaused())
	{
		GetRageProfiler().EndFrame();
		GetRageProfiler().FlipElapsedTimes();
		GetRageProfiler().Update();
	}


	RAGETRACE_UPDATE();

	if (PFSTATS.IsActive() || GetRageProfiler().AnyEkgsActive())
	{
		PIX_AUTO_TAG(0, "Profiler");
		DrawStats();
	}
	else
#endif
	{
		if(m_DrawHelp)
		{
			PIX_AUTO_TAG(0, "Help Text");
			DrawHelp();
		}

		if(m_DrawWorldAxes)
		{
			PIX_AUTO_TAG(0, "WorldAxes");
			DrawWorldAxes();
		}

		if (m_DrawLogo)
		{
			DrawLogo();
		}
	}
}

grcSampleOrthoCam::grcSampleOrthoCam()
	: m_OrthoMode(ORTHO_TOP)
	, m_OrthoNearZ(-1000.0f)
	, m_OrthoFarZ(1000.0f)
	, m_LastMouseX(0)
	, m_LastMouseY(0)
	, m_UseZUp(false)
	, m_vUp(YAXIS)
{
	m_Mapper.Reset();
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RLEFT, m_Left);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RRIGHT, m_Right);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RDOWN, m_Down);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RUP, m_Up);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::L1, m_Zoom);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LX, m_Xaxis);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_Yaxis);

	m_Mapper.Map(IOMS_KEYBOARD, KEY_SHIFT, m_MoveSlowly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_CONTROL, m_MoveQuickly);
	m_Mapper.Map(IOMS_KEYBOARD, KEY_ALT, m_RequiredKey);
}

void grcSampleOrthoCam::InitViews()
{
	m_OrthoMats[ORTHO_TOP].Identity();
	
	//Initialize the TOP cam
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_TOP].d = Vector3(0.0f, 0.0f, 10.0f);
		m_OrthoMats[ORTHO_TOP].LookDown(-ZAXIS, YAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_TOP].d = Vector3(0.0f, 10.0f, 0.0f);
		m_OrthoMats[ORTHO_TOP].LookDown(-YAXIS, ZAXIS);
	}
	m_OrthoZooms[ORTHO_TOP] = 1.0f;

	//Initialzie the BOTTOM cam
	m_OrthoMats[ORTHO_BOTTOM].Identity();
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_BOTTOM].d = Vector3(0.0f, 0.0f, -10.0f);
		m_OrthoMats[ORTHO_BOTTOM].LookDown(ZAXIS, -YAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_BOTTOM].d = Vector3(0.0f, -10.0f, 0.0f);
		m_OrthoMats[ORTHO_BOTTOM].LookDown(YAXIS, -ZAXIS);
	}
	m_OrthoZooms[ORTHO_BOTTOM] = 1.0f;

	//Initialize the LEFT cam
	m_OrthoMats[ORTHO_LEFT].Identity();
	m_OrthoMats[ORTHO_LEFT].d = Vector3(-10.0f, 0.0f, 0.0f);
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_LEFT].LookDown(-XAXIS, m_vUp);
		m_OrthoMats[ORTHO_LEFT].RotateX(DtoR*90.0f);
	}
	else
	{
		m_OrthoMats[ORTHO_LEFT].LookDown(XAXIS, m_vUp);
	}
	m_OrthoZooms[ORTHO_LEFT] = 1.0f;

	//Initialize the RIGHT cam
	m_OrthoMats[ORTHO_RIGHT].Identity();
	m_OrthoMats[ORTHO_RIGHT].d = Vector3(10.0f, 0.0f, 0.0f);
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_RIGHT].LookDown(XAXIS, m_vUp);
		m_OrthoMats[ORTHO_RIGHT].RotateX(DtoR*90.0f);
	}
	else
	{
		m_OrthoMats[ORTHO_RIGHT].LookDown(-XAXIS, m_vUp);
	}
	m_OrthoZooms[ORTHO_RIGHT] = 1.0f;

	//Initialize the FRONT cam
	m_OrthoMats[ORTHO_FRONT].Identity();
	if(m_UseZUp)
	{
		m_OrthoMats[ORTHO_FRONT].d = Vector3(0.0f, -10.0f, 0.0f);
		m_OrthoMats[ORTHO_FRONT].LookDown(YAXIS, ZAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_FRONT].d = Vector3(0.0f, 0.0f, -10.0f);
		m_OrthoMats[ORTHO_FRONT].LookDown(ZAXIS, YAXIS);
	}
	m_OrthoZooms[ORTHO_FRONT] = 1.0f;

	//Initialize the BACK cam
	m_OrthoMats[ORTHO_BACK].Identity();
	if(m_UseZUp)
	{
			m_OrthoMats[ORTHO_BACK].d = Vector3(0.0f, 10.0f, 0.0f);
		m_OrthoMats[ORTHO_BACK].LookDown(-YAXIS, ZAXIS);
	}
	else
	{
		m_OrthoMats[ORTHO_BACK].d = Vector3(0.0f, 0.0f, 10.0f);
		m_OrthoMats[ORTHO_BACK].LookDown(-ZAXIS, YAXIS);
	}
	m_OrthoZooms[ORTHO_BACK] = 1.0f;
}

void grcSampleOrthoCam::Update()
{
	float dt = TIME.GetUnwarpedRealtimeSeconds();
	
	m_Mapper.Update();
	if( UpdateMouse(dt) )
		return;
	else
		UpdatePad(dt);
}

bool grcSampleOrthoCam::UpdateMouse(float dt)
{
	float x = (float) (ioMouse::GetX() - m_LastMouseX); 
	float y = (float) (ioMouse::GetY() - m_LastMouseY); 

	float movementScalar = 1.0;
	if(m_MoveSlowly.IsDown())
	{
		movementScalar /= 5.0f;
	}
	if(m_MoveQuickly.IsDown())
	{
		movementScalar *= 5.0f;
	}
	x *= movementScalar * 0.5f;
	y *= movementScalar * 0.5f;

	m_LastMouseX = ioMouse::GetX();
	m_LastMouseY = ioMouse::GetY();

	bool pan = (ioMouse::GetButtons() & ioMouse::MOUSE_MIDDLE) != 0;
	bool zoom = (ioMouse::GetButtons() & ioMouse::MOUSE_RIGHT) != 0;

	return ProcessInputs(pan, zoom, x, y, dt);
}

bool grcSampleOrthoCam::UpdatePad(float dt)
{
	float x = m_Xaxis.GetNorm(0.15f);
	float y = m_Yaxis.GetNorm(0.15f);

	return ProcessInputs( (m_Left.IsDown() || m_Right.IsDown() || m_Up.IsDown() || m_Down.IsDown()),
						  m_Zoom.IsDown(),
						  x, y, 
						  dt);
}

bool grcSampleOrthoCam::ProcessInputs(bool pan, bool zoom, float x, float y, float dt)
{
	if (rage::INPUT.GetUseKeyboard() && m_RequiredKey.IsUp())
	{
		return false;
	}

	if(zoom)
	{
		float zoomAmount = x+y;

		float curOrthoZoom = GetOrthoZoom();
		if( (curOrthoZoom + (zoomAmount * dt)) < 0.1f )
			SetOrthoZoom(0.1f);
		else 
			SetOrthoZoom(curOrthoZoom + (zoomAmount * dt));
	}
	else if(pan)
	{
		Vector3 offsetVector(0.0f,0.0f,0.0f);

		switch(m_OrthoMode)
		{
		case ORTHO_FRONT:
			if(m_UseZUp)
			{
				offsetVector.SetX(-x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetX(x);
				offsetVector.SetY(y);
			}
			break;
		case ORTHO_BACK:
			if(m_UseZUp)
			{
				offsetVector.SetX(x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetX(-x);
				offsetVector.SetY(y);
			}
			break;
		case ORTHO_TOP:
			if(m_UseZUp)
			{
				offsetVector.SetX(-x);
				offsetVector.SetY(y);
			}
			else
			{
				offsetVector.SetX(x);
				offsetVector.SetZ(y);
			}
			break;
		case ORTHO_BOTTOM:
			if(m_UseZUp)
			{
				offsetVector.SetX(x);
				offsetVector.SetY(y);
			}
			else
			{
				offsetVector.SetX(-x);
				offsetVector.SetZ(y);
			}
			break;
		case ORTHO_LEFT:
			if(m_UseZUp)
			{
				offsetVector.SetY(-x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetZ(-x);
				offsetVector.SetY(y);
			}
			break;
		case ORTHO_RIGHT:
			if(m_UseZUp)
			{
				offsetVector.SetY(x);
				offsetVector.SetZ(y);
			}
			else
			{
				offsetVector.SetZ(x);
				offsetVector.SetY(y);
			}
			break;
		default:
			break;
		}
		m_OrthoMats[m_OrthoMode].d.AddScaled(offsetVector, dt);
	}
	return true;
}

void grcSampleOrthoCam::Frame(const spdSphere& frameSphere)
{
	m_OrthoMats[m_OrthoMode].d.Set(frameSphere.GetCenter());
	SetOrthoZoom(frameSphere.GetRadius() * 2.0f);
}

//Utility fn to draw a dashed line between two points.
static void DrawDashedLine(const Vector3& from, const Vector3& to, float dashSep)
{
	grcBindTexture(NULL);
	grcLightState::SetEnabled(false);
	grcViewport::SetCurrentWorldMtx(M34_IDENTITY);

	Vector3 lineVec;
	lineVec.Subtract(to, from);
	float mag = lineVec.Mag();

	lineVec.Normalize();

	int dashCount = (int)(mag / dashSep);
	if(dashCount == 0)
		return;

	Vector3 startPt, endPt;
	startPt = from;
	endPt = from;
	endPt.AddScaled(lineVec, dashSep);

	for(int i=0; i<dashCount; i++)
	{
		if(i&0x01)
		{
			startPt = endPt;
			endPt.AddScaled(lineVec, dashSep);
		}
		else
		{
			grcDrawLine(startPt, endPt, Color32(1.0f,1.0f,1.0f));
			startPt = endPt;
			endPt.AddScaled(lineVec, dashSep);
		}
	}
}

} // namespace ragesamples
