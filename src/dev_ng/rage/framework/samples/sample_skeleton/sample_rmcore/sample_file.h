//
// sample_file/sample_file.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef SAMPLE_FILE_SAMPLE_FILE_H
#define SAMPLE_FILE_SAMPLE_FILE_H

#include "atl/string.h"
#include "data/base.h"
#include "system/rageroot.h"

namespace rage
{
	class fiPackfile;
};


namespace ragesamples {

	
class filSampleManager : public rage::datBase
{
public:
	filSampleManager();
	~filSampleManager();

	const char*	GetFullAssetPath() const;
	
	void SetFullAssetPath(const char* fullPath);

    void MountArchive();

    void MountArchive( const char* archive );

private:
	rage::fiPackfile* m_pf;
};

} // namespace ragesamples

#endif // SAMPLE_FILE_SAMPLE_FILE_H
