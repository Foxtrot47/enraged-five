// 
// sample_skeleton.h 
// 
// Copyright (C) 1999-2007 Rockstar Games.  All Rights Reserved. 
// 

#include "data/base.h"
#include "sample_rmcore/sample_rmcore.h"
#include "fwsys/gameskeleton.h"

class CRmcSampleWrapper
{
public:

	static void Init()								{ m_rmcSample = rage_new ragesamples::rmcSampleManager();}
	static void InitCamera(unsigned)				{ m_rmcSample->InitCamera(); }
	static void InitCameraPresets(unsigned)			{ m_rmcSample->InitCameraPresets(); }
	static void InitClient(unsigned)				{ m_rmcSample->InitClient(); }
	static void InitCustomLighting(unsigned)		{ m_rmcSample->InitCustomLighting(); }
	static void InitDepthBuffer(unsigned)			{ m_rmcSample->InitDepthBuffer(); }
	static void InitGfxFactories(unsigned)			{ m_rmcSample->InitGfxFactories(); }
	static void InitGrcDevice(unsigned)				{ m_rmcSample->InitGrcDevice(); }
	static void InitInput(unsigned)					{ m_rmcSample->InitInput(); }
	static void InitLights(unsigned)				{ m_rmcSample->InitLights(); }
	static void InitLightManipulators(unsigned)		{ m_rmcSample->InitLightManipulators(); }
	static void InitLightPresets(unsigned)			{ m_rmcSample->InitLightPresets(); }
	static void InitLogo(unsigned)					{ m_rmcSample->InitLogo(); }
	static void InitParams(unsigned)				{ m_rmcSample->InitParams(); }
	static void InitRenderTarget(unsigned)			{ m_rmcSample->InitRenderTarget(); }
	static void InitSetup(unsigned)					{ m_rmcSample->InitSetup(); }
	static void InitStats(unsigned)					{ m_rmcSample->InitStats(); }
	static void InitViewport(unsigned)				{ m_rmcSample->InitViewport(); }

	static void UpdateLoop()						{ m_rmcSample->UpdateLoop(); }

	static void Shutdown()							{ delete m_rmcSample;}
	static void ShutdownCamera(unsigned)			{ m_rmcSample->ShutdownCamera(); }
	static void ShutdownClient(unsigned)			{ m_rmcSample->ShutdownClient(); }
	static void ShutdownCustomLighting(unsigned)	{ m_rmcSample->ShutdownCustomLighting(); }
	static void ShutdownDepthBuffer(unsigned)		{ m_rmcSample->ShutdownDepthBuffer(); }
	static void ShutdownGfxFactories(unsigned)		{ m_rmcSample->ShutdownGfxFactories(); }
	static void ShutdownGrcDevice(unsigned)			{ m_rmcSample->ShutdownGrcDevice(); }
	static void ShutdowntGrcDevice(unsigned)		{ m_rmcSample->ShutdownGrcDevice(); }
	static void ShutdownInput(unsigned)				{ m_rmcSample->ShutdownInput(); }
	static void ShutdownLights(unsigned)			{ m_rmcSample->ShutdownLights(); }
	static void ShutdownLightManipulators(unsigned)	{ m_rmcSample->ShutdownLightManipulators(); }
	static void ShutdownLogo(unsigned)				{ m_rmcSample->ShutdownLogo(); }
	static void ShutdownParams(unsigned)			{ m_rmcSample->ShutdownParams(); }
	static void ShutdownRenderTarget(unsigned)		{ m_rmcSample->ShutdownRenderTarget(); }
	static void ShutdownSetup(unsigned)				{ m_rmcSample->ShutdownSetup(); }
	static void ShutdownStats(unsigned)				{ m_rmcSample->ShutdownStats(); }
	static void ShutdownViewport(unsigned)			{ m_rmcSample->ShutdownViewport(); }

private:

	static ragesamples::rmcSampleManager* m_rmcSample;

};

class sampleSkeleton: public rage::datBase
{
public:
	sampleSkeleton();

	void Init();
	void Shutdown();

protected:
		
	gameSkeleton m_skeleton;
};


