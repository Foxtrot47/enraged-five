﻿//---------------------------------------------------------------------------------------------
// <copyright file="Rage.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Platform
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Core.Attributes;
    using RSG.Pipeline.Services;
    using RSG.Pipeline.Services.Platform;
    using RSG.Platform;
    using SIO = System.IO;
    using XGE = RSG.Interop.Incredibuild.XGE;

    /*! \mainpage
     *
     * \section intro_sec Purpose
     *
     * The RSG.Pipeline.Processor.Platform assembly provides the platform 
     * conversion processor for our RAGE engine based games.  The processor
     * is essentially a wrapper around the 'Ragebuilder' executable; feeding
     * it files to convert and repackaging the resources produced.
     *
     * There are core classes to handle RPF (Rage Pack File) archives and the
     * texture processing pipeline and its dependencies.
     * 
     * Another core component of the processor assembly is the path string
     * remapping and conversion between the platform-independent and platform
     * targets.
     * 
     * \section unit_test Unit Tests
     * 
     * There are a few very simple unit tests available in this assembly.
     */

    /// <summary>
    /// RAGE resource platform conversion processor.  This is the processor
    /// that invokes Ragebuilder and handles its dependency pass.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.SeparatePass)]
    public class Rage :
        RagebuilderBaseProcessor,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Character used to prefix independent/export  assets.
        /// </summary>
        public static readonly Char EXPORT_TARGET_CHARACTER = 'i';

        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Ragebuilder Platform Conversion Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Rage";

        /// <summary>
        /// RAGE Processor Attribute: specifies which targets the process will
        /// run for (default: "*", all).
        /// </summary>
        public static readonly String ATTR_TARGETS = "targets";

        /// <summary>
        /// Default packet size in kilobytes (for when no option is available).
        /// </summary>
        public static readonly int DEFAULT_PACKET_SIZE = 8192;

        /// <summary>
        /// Default maximum number of resources to convert in a single packet.
        /// </summary>
        public static readonly int DEFAULT_PACKET_MAX_FILES = 10;
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Rage()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Copy metadata directory parameter (RPF sorting).
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="inputProcess"></param>
        /// <param name="outputProcess"></param>
        private void AddMetadataDirectoryParameter(IContentTree tree, IProcess inputProcess, ProcessBuilder outputProcess)
        {
            if (inputProcess.Parameters.ContainsKey(Constants.RpfProcess_MetadataDirectory))
            {
                String metadataDirectory = (String)inputProcess.Parameters[Constants.RpfProcess_MetadataDirectory];
                outputProcess.Parameters.Add(Constants.RpfProcess_MetadataDirectory, metadataDirectory);
                outputProcess.AdditionalDependentContent.Add(tree.CreateDirectory(metadataDirectory));
            }
        }

        /// <summary>
        /// Copy metadata directory parameter (RPF sorting).
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="inputProcess"></param>
        /// <param name="outputProcesses"></param>
        private void AddMetadataDirectoryParameter(IContentTree tree, IProcess inputProcess, IEnumerable<ProcessBuilder> outputProcesses)
        {
            if (inputProcess.Parameters.ContainsKey(Constants.RpfProcess_MetadataDirectory))
            {
                String metadataDirectory = (String)inputProcess.Parameters[Constants.RpfProcess_MetadataDirectory];
                outputProcesses.ForEach(p =>
                {
                    p.Parameters.Add(Constants.RpfProcess_MetadataDirectory, metadataDirectory);
                    p.AdditionalDependentContent.Add(tree.CreateDirectory(metadataDirectory));
                });
            }
        }

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            this.LoadParameters(param);
            this.CacheDependencyParameters(param, owner);
#warning DHM FIX ME : consider adding shader dependencies for drawables.
            ISet<IContentNode> dependencies = new HashSet<IContentNode>();
            ICollection<IProcess> processes = new List<IProcess>();
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            String resourcePrefix = String.Empty;
            if (process.Parameters.ContainsKey(Constants.RageProcess_ResourcePrefix))
                resourcePrefix = (String)process.Parameters[Constants.RageProcess_ResourcePrefix];

            bool result = true;
            foreach (IContentNode output in process.Outputs)
            {
                Debug.Assert(output is Content.Asset, "RAGE Process requires an Asset output.  Aborting.");
                if (!(output is Content.Asset))
                {
                    param.Log.ErrorCtx(LOG_CTX, "RAGE Process requires an Asset output.  Skipping.");
                    continue;
                }

                Content.Asset asset = (output as Content.Asset);
                switch (asset.AssetType)
                {
                    case FileType.RagePackFile:
                        if (param.Flags.HasFlag(EngineFlags.Preview))
                        {
                            // We will produce raw game resources.
                            IEnumerable<IContentNode> inputNodes = process.Inputs;
                            IDictionary<ITarget, ICollection<IProcess>> convertProcesses = null;
                            IDictionary<ITarget, ICollection<IContentNode>> convertedNodes = null;

                            List<IContentNode> collapsedInputs = new List<IContentNode>();
                            foreach (IContentNode input in inputNodes)
                            {
                                if (input is Content.File)
                                    collapsedInputs.Add(input);
                                else if (input is Content.Directory)
                                {
                                    Content.Directory directory = (input as Content.Directory);
                                    if (process.Parameters.ContainsKey(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName))
                                    {
                                        foreach (ITarget target in enabledTargets)
                                        {
                                            IProcess directoryConversionRageProcess = this.CreateDeferredRageDirectoryProcess(
                                                param, owner, dependencies, target, directory, true);
                                            processes.Add(directoryConversionRageProcess);
                                        }
                                    }
                                    else
                                    {
                                        collapsedInputs.AddRange(directory.EvaluateInputs());
                                    }
                                }
                            }

                            if (!process.Parameters.ContainsKey(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName))
                            {
                                result &= PrebuildResources(param, process, owner, collapsedInputs,
                                    dependencies, false, String.Empty, resourcePrefix, out convertProcesses,
                                    out convertedNodes);
                                IEnumerable<IProcess> outputProcesses = convertProcesses.SelectMany(kvp => kvp.Value);
                                processes.AddRange(outputProcesses);
                            }
                        }
                        else
                        {
                            // We will produce an RPF.
                            IEnumerable<IProcess> outputProcesses = null;
                            result &= PrebuildPackFile(param, process, processors,
                                owner, dependencies, resourcePrefix, out outputProcesses);
                            processes.AddRange(outputProcesses);
                        }
                        break;
                    default:
                        {
                            // We will produce raw game resources.
                            IEnumerable<IContentNode> inputNodes = process.Inputs;
                            IDictionary<ITarget, ICollection<IProcess>> convertProcesses = null;
                            IDictionary<ITarget, ICollection<IContentNode>> convertedNodes = null;
                            result &= PrebuildResources(param, process, owner, 
                                inputNodes, dependencies, false, String.Empty, resourcePrefix,
                                out convertProcesses, out convertedNodes);
                            processes.AddRange(convertProcesses.SelectMany(kvp => kvp.Value));
                        }
                        break;
                }
            }

            // We synched the dependencies after the intialisation in the engine.
            // IF ASSETBUILDER user - check for missing tune files!
            bool warnForMissingTunes = this.Parameters.ContainsKey(Constants.RageProcess_WarnForMissingTuneFiles) &&
                                       ((bool)this.Parameters[Constants.RageProcess_WarnForMissingTuneFiles]);

            if (IsAssetBuildProcess(param) && warnForMissingTunes && process.State == ProcessState.Prebuilding)
            {
                foreach (IContentNode contentNode in process.Inputs)
                {
                    if (!contentNode.HasParameter(Constants.ParamMap_NeedsTuning))
                        continue;

                    // First removing zip, then ift extension
                    string filename = SIO.Path.GetFileNameWithoutExtension((contentNode as IFilesystemNode).AbsolutePath);
                    string fragmentName = SIO.Path.GetFileNameWithoutExtension(filename);
                    string tuneFilePath = SIO.Path.Combine(param.Branch.Assets, "fragments", SIO.Path.ChangeExtension(filename, "tune"));

                    if (!SIO.File.Exists(tuneFilePath))
                    {
                        param.Log.ErrorCtx(fragmentName, "[Assetbuilder only check] Map fragment {0}'s tune file {1} does not exist. Please contact the creator of the fragment.", fragmentName, tuneFilePath);
                    }
                }
            }

            // Propagate source process parameters onto all targets.
            foreach (IProcess p in processes)
            {
                IEnumerable<KeyValuePair<String, Object>> parameters = process.Parameters.Where(
                    kvp => !p.Parameters.ContainsKey(kvp.Key));
                p.Parameters.AddRange(parameters);
            }
            
            if (process.State == ProcessState.Initialised)
            {
                process.State = ProcessState.Prebuilding;
                foreach (IProcess p in processes)
                    p.State = ProcessState.Discard;
                resultantProcesses = new List<IProcess>();
                syncDependencies = dependencies;
            }
            else
            {
                process.State = ProcessState.Discard;
                resultantProcesses = processes;
                syncDependencies = dependencies;
            }
            return (true);
        }


        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a
        /// set of IProcess objects after determining all required inputs, 
        /// outputs and whether the process' needs to change (this allows the 
        /// concept of 'preprocessors').
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes">RawProcesses to prebuild.</param>
        /// <param name="processors">Processor collection to reference for new resultant proceses.</param>
        /// <param name="owner">Owning IContentTree.</param>
        /// <param name="syncDependencies">Dependencies to sync.</param>
        /// <param name="resultantProcesses">RawProcesses that will actually be built.</param>
        /// <returns>true iff successful; false otherwise</returns>
        /// The resultant processes are used by the pipeline engine after these
        /// calls.
        /// 
        public override bool Prebuild(IEngineParameters param,
            IEnumerable<IProcess> processes, IProcessorCollection processors,
            IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            bool result = true;
            using (ProfileContext ctx = new ProfileContext(param.Log, LOG_CTX,
                "RSG.Pipeline.Processor.Platform.Rage Prebuild."))
            {
                result = base.Prebuild(param, processes, processors, owner, 
                    out syncDependencies, out resultantProcesses);
            }
            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Add tools, sort processes and create packets per-target.
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            IDictionary<ITarget, XGE.ITool> targetTools = new Dictionary<ITarget, XGE.ITool>();
            IDictionary<ITarget, XGE.ITaskJobGroup> targetGroups = new Dictionary<ITarget, XGE.ITaskJobGroup>();
            ICollection<RagePacket> packets = new List<RagePacket>();

            foreach (ITarget target in enabledTargets)
            {
                List<IProcess> targetProcesses = new List<IProcess>();
                List<IProcess> targetManifestProcesses = new List<IProcess>();
                foreach (IProcess process in processes)
                {
                    IContentNode input = process.Inputs.First();
                    IContentNode output = process.Outputs.First();
                    if ((input is Content.File) && (output is Content.Asset))
                    {
                        if (((Asset)output).Platform.Equals(target.Platform))
                        {
                            if (process.Parameters.ContainsKey(Constants.ProcessXGE_ManifestConvert))
                                targetManifestProcesses.Add(process);
                            else
                                targetProcesses.Add(process);
                        }
                    }
                    else if ((input is Content.Directory) && (output is Content.Directory))
                    {
                        if (process.Parameters.ContainsKey("platform") && (RSG.Platform.Platform)process.Parameters["platform"] == target.Platform)
                        {
                            targetProcesses.Add(process);
                        }
                    }
                    else
                    {
                        param.Log.WarningCtx(LOG_CTX, "Unsupported conversion type: {0} => {1}.",
                            input.GetType().ToString(), output.GetType().ToString());
                    }
                }

                // Create packets for regular processes.
                Ragebuilder.CreateXGEPacketsForRagebuilderProcesses(param, this, targetProcesses, target,
                    packets);
                // Create packets for manifest processes; filtered separately
                // so we don't accidentally add circular dependencies between 
                // Rage processes and the manifest construction.
                Ragebuilder.CreateXGEPacketsForRagebuilderProcesses(param, this, targetManifestProcesses,
                    target, packets);

                // Add tools.
                String taskGroupName = String.Format("Rage [{0}]", target.Platform);
                bool allow_remote = GetXGEAllowRemote(param, true);
                XGE.ITool tool = XGEFactory.GetRagebuilderTool(target, allow_remote);
                targetTools.Add(target, tool);
                targetGroups.Add(target, new XGE.TaskGroup(taskGroupName));
            }

            param.Log.MessageCtx(LOG_CTX, "Creating {0} XGE packets for Rage processor.", packets.Count);
            bool result = true;
            foreach (RagePacket packet in packets)
            {
                XGE.ITask task = Ragebuilder.GetXgeConvertTask(param.Log,
                    packet.Target, packet.Processes, targetTools[packet.Target]);
                targetGroups[packet.Target].Tasks.Add(task);
            }

            // Assign method output.
            tools = targetTools.Values;
            tasks = targetGroups.Values;

            return (result);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        #region IProcess Construction Methods
        /// <summary>
        /// Create a conversion process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="target"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private ProcessBuilder CreateConvertProcess(IEngineParameters param,
            ITarget target, IContentTree owner, ISet<IContentNode> syncDependencies, 
            String input, String output, ProcessBuilder parent = null)
        {
            IContentNode inputNode = owner.CreateAsset(input, RSG.Platform.Platform.Independent);
            if (null == inputNode)
                inputNode = owner.CreateFile(input);

            return (this.CreateConvertProcess(param, target, owner, 
                syncDependencies, inputNode, output, parent));
        }

        /// <summary>
        /// Create a conversion process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="target"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private ProcessBuilder CreateConvertProcess(IEngineParameters param, ITarget target,
            IContentTree owner, ISet<IContentNode> syncDependencies, IContentNode input,
            String output, ProcessBuilder parent = null)
        {
            String outputPlatform = PlatformPathConversion.ConvertFilenameToPlatform(target, output, false);
            IContentNode outputNode = owner.CreateAsset(outputPlatform, target.Platform);
            IContentNode resourceTool = owner.CreateFile(target.GetRagebuilderExecutable());

            ProcessBuilder pb = new ProcessBuilder(this, owner);
            pb.Inputs.Add(input);
            pb.Outputs.Add(outputNode);
            pb.AdditionalDependentContent.Add(resourceTool);
            AddDependenciesFor(param, owner, syncDependencies, input, outputNode, ref pb);
            
            if (null != parent)
                parent.Inputs.Add(outputNode);
            return (pb);
        }
        #endregion // IProcess Construction Methods
                
        /// <summary>
        /// Prebuild an RPF output.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="dependencies"></param>
        /// <param name="resourcePrefix"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        private bool PrebuildPackFile(IEngineParameters param, IProcess process,
            IProcessorCollection processors, IContentTree owner,
            ISet<IContentNode> dependencies, String resourcePrefix,
            out IEnumerable<IProcess> resultantProcesses)
        {
            Debug.Assert(!param.Flags.HasFlag(EngineFlags.Preview),
                "RAGE Processor: building RPF for preview build.  Not supported.");
            if (param.Flags.HasFlag(EngineFlags.Preview))
            {
                param.Log.ErrorCtx(LOG_CTX, "RAGE Processor: building RPF for preview build.  Not supported.");
                resultantProcesses = new List<IProcess>();
                return (false);
            }

            bool result = true;
            // Enumerable of our enabled platform targets.
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            
            // Retrieve any extra parameter set on the edge of this processor, to filter out based on platforms
            IEnumerable<Platform> supportedPlatformsParam = process.GetParameter(Constants.Supported_Platforms, "").Split(';').Select(PlatformUtils.PlatformFromString);
            ISet<Platform> supportedPlatforms = supportedPlatformsParam.Where(platform => platform != Platform.Independent).ToSet();

            if (supportedPlatforms.Any())
            {
                enabledTargets = enabledTargets.Where(t => supportedPlatforms.Contains(t.Platform));
            }

            // Construct resultant RPF construction process.
            IDictionary<ITarget, ICollection<IProcess>> processes = new Dictionary<ITarget, ICollection<IProcess>>();
            foreach (ITarget target in enabledTargets)
            {
                processes.Add(target, new List<IProcess>());
            }

            // Loop through inputs creating the necessary conversion processes.
            foreach (IContentNode input in process.Inputs)
            {
                if (input.Parameters.ContainsKey(Constants.RageProcess_DoNotConvert) &&
                    (bool)input.Parameters[Constants.RageProcess_DoNotConvert])
                    continue;

                if (input is Content.Asset)
                {
                    Content.Asset asset = (input as Content.Asset);
                    switch (asset.AssetType)
                    {
                        case FileType.ZipArchive:
                            String destination = GetZipAssetCacheDir(param, asset);

                            IEnumerable<String> files;
                            
                            // test for zip presence on disk, to avoid a zip extract crash
                            if (!asset.Exists())
                            {
                                param.Log.ErrorCtx(LOG_CTX, "Cannot extract '{0}', as the file does not exist.", asset.AbsolutePath);
                                continue;
                            }

                            // Extract.
                            bool extractOk;

                            // clearing cache because
                            RemoveDeletedEntriesFromCache(asset.AbsolutePath, destination);

                            if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                            {
                                extractOk = Zip.ExtractAll(asset.AbsolutePath, destination, true, null, out files);
                            }
                            else
                            {
                                extractOk = Zip.ExtractNewer(asset.AbsolutePath, destination, out files);
                            }

                            if (extractOk)
                            {
                                IEnumerable<IContentNode> inputNodes = files.Select(f => owner.CreateFile(f));
                                IDictionary<ITarget, ICollection<IProcess>> convertProcesses = null;
                                IDictionary<ITarget, ICollection<IContentNode>> convertedNodes = null;
                                result &= PrebuildResources(param, process,
                                    owner, inputNodes, dependencies, true, destination,
                                    resourcePrefix, out convertProcesses, out convertedNodes);
                                foreach (ITarget target in enabledTargets)
                                    processes[target].AddRange(convertProcesses[target]);
                            }
                            else
                            {
                                param.Log.ErrorCtx(LOG_CTX, "Extracting '{0}' to '{1}' failed.", asset.AbsolutePath, destination);
                                continue;
                            }
                            break;
                        default:
                            {
                                IEnumerable<IContentNode> inputNodes = new IContentNode[] { input };
                                IDictionary<ITarget, ICollection<IProcess>> convertProcesses = null;
                                IDictionary<ITarget, ICollection<IContentNode>> convertedNodes = null;
                                result &= PrebuildResources(param, process,
                                    owner, inputNodes, dependencies, false, String.Empty,
                                    resourcePrefix, out convertProcesses, out convertedNodes);
                                foreach (ITarget target in enabledTargets)
                                    processes[target].AddRange(convertProcesses[target]);
                            }
                            break;
                    }
                }
                else if (input is Content.Directory)
                {
                    Content.Directory directory = (input as Content.Directory);

                    // JWR - We use the Common.Consts.Rage_DelayedDirectoryProcessingParameterName to signify that
                    // the content of the directory shouldn't be evaluated till Ragebuilder itself runs
                    if (process.Parameters.ContainsKey(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName))
                    {
                        foreach (ITarget target in enabledTargets)
                        {
                            IProcess directoryConversionRageProcess = this.CreateDeferredRageDirectoryProcess(
                                param, owner, dependencies, target, directory, false);
                            if (!String.IsNullOrEmpty(resourcePrefix))
                                directoryConversionRageProcess.Parameters.Add(Constants.RageProcess_ResourcePrefix, resourcePrefix);

                            processes[target].Add(directoryConversionRageProcess);
                        }
                    }
                    else
                    {
                        IEnumerable<IContentNode> inputNodes = directory.EvaluateInputs();

                        if (!inputNodes.Any())
                        {
                            Debug.Assert(false, "Directory {0} has no inputs; did you mean to use StaticDirectory?", directory.AbsolutePath);
                            param.Log.ErrorCtx(LOG_CTX, "Directory {0} has no inputs; did you mean to use StaticDirectory?", directory.AbsolutePath);
                            result = false;
                        }

                        // ACB HACK - Stop directories (peds) being considered for BVH dependencies for raytracing assets.
                        if (directory.GetParameter(Constants.No_BVH_Generation, false))
                        {
                            foreach (IContentNode inputNode in inputNodes)
                            {
                                inputNode.SetParameter(Constants.No_BVH_Generation, true);
                            }
                        }

                        IDictionary<ITarget, ICollection<IProcess>> convertProcesses = null;
                        IDictionary<ITarget, ICollection<IContentNode>> convertedNodes = null;
                        result &= PrebuildResources(param, process, owner,
                            inputNodes, dependencies, true, directory.AbsolutePath,
                            resourcePrefix, out convertProcesses, out convertedNodes);
                        foreach (ITarget target in enabledTargets)
                            processes[target].AddRange(convertProcesses[target]);
                    }
                }
                else
                {
                    param.Log.WarningCtx(LOG_CTX, "Input node type: {0} not supported.  Ignoring.",
                        input.GetType());
                }
            }

            // Cache the RPF Create Processor.
            IProcessor rpfCreateProcessor = processors.OfType<RpfCreateProcessor>().FirstOrDefault();
            Debug.Assert(null != rpfCreateProcessor, "RPF Create Processor Required",
                "RPF Create Processor not found in Processor Collection!");
            if (null == rpfCreateProcessor)
            {
                param.Log.ErrorCtx(LOG_CTX, "RPF Create Processor not found in Processor Collection!");
                resultantProcesses = new List<IProcess>();
                return (false);
            }

            // Post-process; scoop up the processes and create the RPF processes
            // for each target.
            Asset outputRpf = process.Outputs.First() as Asset;
            foreach (ITarget target in enabledTargets)
            {
                IEnumerable<IContentNode> rpfInputs = processes[target].SelectMany(p => p.Outputs);
                ProcessBuilder pb = RpfCreateProcessor.CreateRpfProcess(param, target, owner,
                    rpfCreateProcessor, rpfInputs, outputRpf.AbsolutePath, outputRpf);
                RpfCreateProcessor.AddDependencyITYP(param, owner, process.Inputs, ref pb);

                AddMetadataDirectoryParameter(owner, process, pb); // For RPF Sort.
                IProcess rpfProcess = pb.ToProcess();
                processes[target].Add(rpfProcess);
            }

            // Flatten the collection of IProcess objects for returning to the engine.
            IProcess[] finalProcesses = processes.SelectMany(kvp => kvp.Value).ToArray();
            resultantProcesses = finalProcesses;

            return (result);
        }

        /// <summary>
        /// Prebuild a set of resources; creating conversion IProcess' objects.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="useResourceCache"></param>
        /// <param name="rootSourceDir"></param>
        /// <param name="resourcePrefix"></param>
        /// <param name="resultantProcesses"></param>
        /// <param name="outputs"></param>
        /// <returns></returns>
        private bool PrebuildResources(IEngineParameters param, IProcess process,
            IContentTree owner, IEnumerable<IContentNode> inputs, ISet<IContentNode> syncDependencies,
            bool useResourceCache, String rootSourceDir, String resourcePrefix,
            out IDictionary<ITarget, ICollection<IProcess>> resultantProcesses,
            out IDictionary<ITarget, ICollection<IContentNode>> outputs)
        {
            bool result = true;
            // Enumerable of our enabled platform targets.
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);

            // Retrieve any extra parameter set on the edge of this processor, to filter out based on platforms
            IEnumerable<Platform> supportedPlatformsParam = process.GetParameter(Constants.Supported_Platforms, "").Split(';').Select(PlatformUtils.PlatformFromString);
            ISet<Platform> supportedPlatforms = supportedPlatformsParam.Where(platform => platform != Platform.Independent).ToSet();

            if (supportedPlatforms.Any())
            {
                enabledTargets = enabledTargets.Where(t => supportedPlatforms.Contains(t.Platform));
            }

            resultantProcesses = new Dictionary<ITarget, ICollection<IProcess>>();
            outputs = new Dictionary<ITarget, ICollection<IContentNode>>();

            foreach (ITarget target in enabledTargets)
            {
                resultantProcesses[target] = new List<IProcess>();
                outputs[target] = new List<IContentNode>();
            }
            
            foreach (IContentNode input in inputs)
            {
                if (!(input is Content.File))
                    continue;
                if (input.Parameters.ContainsKey(Constants.RageProcess_DoNotConvert) &&
                    (bool)input.Parameters[Constants.RageProcess_DoNotConvert])
                    continue;

                Content.File inputFile = (input as Content.File);

                // Create a new IProcess per-target (enabled) for the conversion.
                foreach (ITarget target in enabledTargets)
                {
                    String convertFilename = String.Empty;
                    String rpfPath = String.Empty;
                    if (param.Flags.HasFlag(EngineFlags.Preview))
                    {
                        convertFilename = PlatformPathConversion.ConvertAndRemapFilenameToPreview(target, inputFile);
                        if (!String.IsNullOrEmpty(resourcePrefix))
                            convertFilename = PlatformPathConversion.PrependResourcePrefixToFilename(convertFilename, resourcePrefix);
                    }
                    else if (useResourceCache)
                    {
                        String resourceCacheDir = PlatformProcessBuilder.GetResourceCacheRootDir(param);
                        convertFilename = PlatformPathConversion.ConvertAndRemapFilenameToDirectory(target, inputFile.AbsolutePath, resourceCacheDir);
                        if (!String.IsNullOrEmpty(resourcePrefix))
                            convertFilename = PlatformPathConversion.PrependResourcePrefixToFilename(convertFilename, resourcePrefix);

                        String directory = SIO.Path.GetDirectoryName(inputFile.AbsolutePath);
                        if (directory.StartsWith(rootSourceDir))
                            rpfPath = directory.Replace(rootSourceDir, "");
                    }
                    else
                    {
                        convertFilename = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, inputFile.AbsolutePath);
                        if (!String.IsNullOrEmpty(resourcePrefix))
                            convertFilename = PlatformPathConversion.PrependResourcePrefixToFilename(convertFilename, resourcePrefix);
                    }
                    ProcessBuilder pb = CreateConvertProcess(param, target, owner,
                        syncDependencies, inputFile, convertFilename);
                    IProcess conversionProcess = pb.ToProcess();
                    conversionProcess.State = ProcessState.Prebuilt;

                    // Update outputs with any RPF Path information.
                    if (!String.IsNullOrEmpty(rpfPath))
                    {
                        foreach (IContentNode output in conversionProcess.Outputs)
                        {
                            output.Parameters[Constants.Asset_RPFPath] =
                                SIO.Path.Combine(rpfPath, (String)output.Parameters[Constants.Asset_RPFPath]);
                        }
                    }

                    resultantProcesses[target].Add(conversionProcess);
                    outputs[target].AddRange(conversionProcess.Outputs);
                }
            }

            return (result);
        }

        /// <summary>
        /// Force clearing the cache for default rage conversion.
        /// Prevents picking up deleted data in source.
        /// </summary>
        /// <param name="assetPath"></param>
        /// <param name="cachePath"></param>
        private void RemoveDeletedEntriesFromCache(string assetPath, string cachePath)
        {
            IEnumerable<string> fileList;
            Zip.GetFileList(assetPath, out fileList);

            // get the zip file entries
            var filenames = fileList.Select(SIO.Path.GetFileName).ToList();

            // if the cache directory does not exist, bail out.
            if (!SIO.Directory.Exists(cachePath))
            {
                return;
            }

            // enumerate through each files of the cache; if an entry has been deleted in the source zip, delete the corresponding cache 
            foreach (string file in SIO.Directory.EnumerateFiles(cachePath, "*.*", SIO.SearchOption.TopDirectoryOnly))
            {
                if(filenames.Contains(SIO.Path.GetFileName(file)))
                    continue;

                try
                {
                    SIO.File.Delete(file);
                }
                catch (Exception)
                {
                    // do nothing
                }
            }
        }

        /// <summary>
        /// Return resource cache directory for an asset.
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        private String GetZipAssetCacheDir(IEngineParameters param, File asset)
        {
            Debug.Assert(this.Parameters.ContainsKey(Constants.RageProcess_ResourceCache),
                "Cache directory not found in parameters!  Internal error.");
            if (!this.Parameters.ContainsKey(Constants.RageProcess_ResourceCache))
            {
                param.Log.ErrorCtx(LOG_CTX, "Cache directory not found in parameters!  Internal error.");
                throw (new NotSupportedException("Cache directory not found in parameters!  Internal error."));
            }

            String cacheDirectory = (String)this.Parameters[Constants.RageProcess_ResourceCache];
            String cacheRoot = SIO.Path.GetFullPath(param.Branch.Environment.Subst(cacheDirectory));

            String relativePath = SIO.Path.GetFileNameWithoutExtension(asset.AbsolutePath)
                .Replace(param.Branch.Export, "", StringComparison.OrdinalIgnoreCase)
                .Replace(param.Branch.Processed, "", StringComparison.OrdinalIgnoreCase);

            String cacheDir = SIO.Path.Combine(cacheRoot, relativePath);
            return cacheDir;
        }

        /// <summary>
        /// Return resource cache directory for a directory.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="directory"></param>
        /// <returns></returns>
        private String GetDirectoryAssetCacheDir(IEngineParameters param, Content.Directory directory)
        {
            String cacheRoot = PlatformProcessBuilder.GetResourceCacheRootDir(param);
            String relativePath = directory.AbsolutePath
                .Replace(param.Branch.Export, "", StringComparison.OrdinalIgnoreCase)
                .Replace(param.Branch.Processed, "", StringComparison.OrdinalIgnoreCase);

            String cacheDir = SIO.Path.Combine(cacheRoot, relativePath);
            return cacheDir;
        }

        /// <summary>
        /// helper method to determine whether the process runs on the assetbuilders
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private bool IsAssetBuildProcess(IEngineParameters param)
        {
            IConfig config = param.Branch.Project.Config;
            IUsertype builderClientUsertype = config.Usertypes.FirstOrDefault(
                usertype => "builder_client".Equals(usertype.Name, StringComparison.OrdinalIgnoreCase));
            IUsertype builderServerUsertype = config.Usertypes.FirstOrDefault(
                usertype => "builder_server".Equals(usertype.Name, StringComparison.OrdinalIgnoreCase));
            return ((builderClientUsertype != null) && ((config.Usertype & builderClientUsertype.Flags) != 0)) ||
                                                   ((builderServerUsertype != null) && ((config.Usertype & builderServerUsertype.Flags) != 0));
        }

        /// <summary>
        /// Create a process for deferred directory processing in ragebuilder.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="target"></param>
        /// <param name="inputDirectory"></param>
        /// <param name="isPreview"></param>
        /// <returns></returns>
        private IProcess CreateDeferredRageDirectoryProcess(IEngineParameters param, IContentTree owner, 
            ISet<IContentNode> syncDependencies,
            ITarget target, Content.Directory inputDirectory, bool isPreview = false)
        {
            ProcessBuilder directoryConversionRageProcessBuilder = new ProcessBuilder(this, owner);
            directoryConversionRageProcessBuilder.Inputs.Add(inputDirectory);
            String platformDirectory = String.Empty;

            if (isPreview)
                platformDirectory = target.Branch.Preview;
            else
                platformDirectory = SIO.Path.Combine(inputDirectory.AbsolutePath, target.Platform.ToString());
                
            AddParameterAllGenericDependencies(param, owner, syncDependencies, ref directoryConversionRageProcessBuilder);

            directoryConversionRageProcessBuilder.Outputs.Add(owner.CreateDirectory(platformDirectory));
            directoryConversionRageProcessBuilder.Parameters.Add("platform", target.Platform);
            IProcess directoryConversionRageProcess = directoryConversionRageProcessBuilder.ToProcess();
            directoryConversionRageProcess.State = ProcessState.Prebuilt;

            return (directoryConversionRageProcess);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Platform namespace
