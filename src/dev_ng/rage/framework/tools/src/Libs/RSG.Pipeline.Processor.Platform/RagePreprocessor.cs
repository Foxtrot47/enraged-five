﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Ionic.Zip;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services.Platform;
using RSG.Pipeline.Services.Platform.Texture;
using RSG.Platform;
using SIO = System.IO;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Platform
{
    using Platform = RSG.Platform.Platform;

    /// <summary>
    /// RAGE platform pre-processor.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.SeparatePass)]
    public class RagePreprocessor :
        RagebuilderBaseProcessor,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Character used to prefix independent/export  assets.
        /// </summary>
        public static readonly Char EXPORT_TARGET_CHARACTER = 'i';

        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Ragebuilder Preprocessor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "RagePreprocessor";

        /// <summary>
        /// RAGE Processor Attribute: specifies which targets the process will
        /// run for (default: "*", all).
        /// </summary>
        public static readonly String ATTR_TARGETS = "targets";

        /// <summary>
        /// 
        /// </summary>
        private static readonly String RAGE_PROCESSOR = "RSG.Pipeline.Processor.Platform.RageProcessor";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// File-type dependencies (cached) from parameter XML.
        /// </summary>
        private static IDictionary<RSG.Platform.FileType, IEnumerable<String>> m_fileTypeDependencies;
        #endregion // Member Data

        #region Class Constructor
        /// <summary>
        /// 
        /// </summary>
        static RagePreprocessor()
        {
            m_fileTypeDependencies = new Dictionary<RSG.Platform.FileType, IEnumerable<String>>();
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RagePreprocessor()
            : base(DESCRIPTION)
        {
            
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            this.LoadParameters(param);
            this.CacheDependencyParameters(param, owner);
#warning DHM FIX ME : consider adding shader dependencies for drawables.
            ISet<IContentNode> dependencies = new HashSet<IContentNode>();
            IDictionary<ITarget, ICollection<IProcess>> processes =
                new Dictionary<ITarget, ICollection<IProcess>>();
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);

            // Retrieve any extra parameter set on the edge of this processor, to filter out based on platforms
            IEnumerable<Platform> supportedPlatformsParam = process.GetParameter(Constants.Supported_Platforms, "").Split(';').Select(PlatformUtils.PlatformFromString);
            ISet<Platform> supportedPlatforms = supportedPlatformsParam.Where(platform => platform != Platform.Independent).ToSet();

            if (supportedPlatforms.Any())
            {
                enabledTargets = enabledTargets.Where(t => supportedPlatforms.Contains(t.Platform));
            }

            foreach (ITarget target in enabledTargets)
            {
                processes.Add(target, new List<IProcess>());
            }

            IDictionary<ITarget, ICollection<IContentNode>> convertedNodes = null;

            bool result = true;

            // We will produce raw game resources.
            IEnumerable<IContentNode> inputNodes = process.Inputs;
            List<IContentNode> collapsedInputs = new List<IContentNode>();
            List<Content.Directory> deferredDirectoryInputs = new List<Content.Directory>();

            foreach (IContentNode input in inputNodes)
            {
                if (input is Content.File)
                {
                    collapsedInputs.Add(input);
                }
                else if (input is Content.Directory)
                {
                    // If the Directory content has a platform override, propagate that through inputs.
                    Content.Directory directory = (input as Content.Directory);
                    if (!process.Parameters.ContainsKey(Core.Constants.RageProcess_DelayedDirectoryEvaluationParameterName))
                    {
                        IEnumerable<IContentNode> dirInputs = directory.EvaluateInputs();
                        foreach (IContentNode dirInput in dirInputs)
                        {
                            if (input.Parameters.ContainsKey(Constants.Platform_Override))
                            {
                                dirInput.Parameters.Add(Constants.Platform_Override, (RSG.Platform.Platform)input.Parameters[Constants.Platform_Override]);
                            }
                            collapsedInputs.AddRange(dirInputs);
                        }
                    }
                    else
                    {
                        deferredDirectoryInputs.Add(directory);
                    }         
                }  
            }

            if (deferredDirectoryInputs.Any())
            {
                CreateDeferredRageDirectoryProcess(param, owner, processors, processes, process, dependencies, deferredDirectoryInputs);
            }

            IDictionary<ITarget, ICollection<IProcess>> convertProcesses = null;
            result &= this.PrebuildResources(param, processors, owner, collapsedInputs, dependencies, true, String.Empty, out convertProcesses, out convertedNodes);
            foreach (ITarget target in enabledTargets)
            {
                processes[target].AddRange(convertProcesses.Where(kvp => kvp.Key.Platform == target.Platform).SelectMany(kvp => kvp.Value));
            }

            // Create RPF packing processes
            // Cache the RPF Create Processor.
            IProcessor rpfCreateProcessor = processors.OfType<RpfCreateProcessor>().FirstOrDefault();
            Debug.Assert(null != rpfCreateProcessor, "RPF Create Processor Required",
                "RPF Create Processor not found in Processor Collection!");
            if (null == rpfCreateProcessor)
            {
                param.Log.ErrorCtx(LOG_CTX, "RPF Create Processor not found in Processor Collection!");
                resultantProcesses = new List<IProcess>();
                syncDependencies = new HashSet<IContentNode>();
                return (false);
            }

            // Only create rpf packing processes if we are NOT in preview mode
            if (!param.Flags.HasFlag(EngineFlags.Preview))
            {
                RpfCreateProcessor.CreateRpfPackingProcesses(param, owner, process, processes, rpfCreateProcessor);
            }
            
#warning LPXO: Removing brute force recursion required for sync dependency chains (such as TCL->TCS->Texture). This needs to be replaced!
            IProcess[] finalProcesses = processes.SelectMany(kvp => kvp.Value).ToArray();
            
            // Propagate source process parameters onto all targets.
            foreach (IProcess p in finalProcesses)
            {
                IEnumerable<KeyValuePair<String, Object>> parameters = process.Parameters.Where(
                    kvp => !p.Parameters.ContainsKey(kvp.Key));
                p.Parameters.AddRange(parameters);
            }


            if (process.State == ProcessState.Initialised)
            {
                process.State = ProcessState.Prebuilding;
                foreach (IProcess p in finalProcesses)
                    p.State = ProcessState.Discard;
                resultantProcesses = new List<IProcess>();
                syncDependencies = dependencies;
            }
            else
            {
                process.State = ProcessState.Discard;
                resultantProcesses = finalProcesses;
                syncDependencies = dependencies;
            }

            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            IDictionary<ITarget, XGE.ITool> targetTools = new Dictionary<ITarget, XGE.ITool>();
            IDictionary<ITarget, XGE.ITaskJobGroup> targetGroups = new Dictionary<ITarget, XGE.ITaskJobGroup>();

            // Assign method output.
            tools = targetTools.Values;
            tasks = targetGroups.Values;

            return (true);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Prebuild a set of resources; creating conversion IProcess' objects.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="useResourceCache"></param>
        /// <param name="rootSourceDir"></param>
        /// <param name="resultantProcesses"></param>
        /// <param name="outputs"></param>
        /// <returns></returns>
        private bool PrebuildResources(IEngineParameters param,
            IProcessorCollection processors, IContentTree owner,
            IEnumerable<IContentNode> inputs, ISet<IContentNode> syncDependencies,
            bool useResourceCache, String rootSourceDir,
            out IDictionary<ITarget, ICollection<IProcess>> resultantProcesses,
            out IDictionary<ITarget, ICollection<IContentNode>> outputs)
        {
            // Enumerable of our enabled platform targets.
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            resultantProcesses = new Dictionary<ITarget, ICollection<IProcess>>();
            outputs = new Dictionary<ITarget, ICollection<IContentNode>>();

            // List of input content with overridden platform.
            Dictionary<String, List<RSG.Platform.Platform>> overriddenOutput = new Dictionary<String, List<RSG.Platform.Platform>>();

            foreach (ITarget target in enabledTargets)
            {
                resultantProcesses[target] = new List<IProcess>();
                outputs[target] = new List<IContentNode>();
            }

            RagePreprocessor.CollectOverrides(param, inputs, overriddenOutput);
            /*
            // Prepass the input so we know what's platform overriden data upfront.
            foreach (IContentNode input in inputs)
            {
                if (!(input is Content.File))
                    continue;
                if (input.Parameters.ContainsKey(Constants.RageProcess_DoNotConvert) &&
                    (bool)input.Parameters[Constants.RageProcess_DoNotConvert])
                    continue;

                Content.File inputFile = (input as Content.File);
                if (input.Parameters.ContainsKey(Constants.Platform_Override))
                {
                    RSG.Platform.Platform plat = (RSG.Platform.Platform)input.Parameters[Constants.Platform_Override];
                    Debug.Assert(null != plat, "Invalid target supplied for platform override",
                        "A platform override parameter has been provided but it is not a valid target!");
                    if (null == plat)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "A platform override parameter has been provided but it is not a valid platform! Platform's target will be treated as default.");
                    }
                    else
                    {
                        if (overriddenOutput.ContainsKey(SIO.Path.GetFileName(inputFile.AbsolutePath)))
                            overriddenOutput[SIO.Path.GetFileName(inputFile.AbsolutePath)].Add(plat);
                        else
                        {
                            List<RSG.Platform.Platform> targets = new List<RSG.Platform.Platform>();
                            targets.Add(plat);
                            overriddenOutput.Add(SIO.Path.GetFileName(inputFile.AbsolutePath), targets);
                        }
                    }
                }
                else
                {
                     if (overriddenOutput.ContainsKey(SIO.Path.GetFileName(inputFile.AbsolutePath)))
                            overriddenOutput[SIO.Path.GetFileName(inputFile.AbsolutePath)].Add(RSG.Platform.Platform.Independent);
                        else
                        {
                            List<RSG.Platform.Platform> targets = new List<RSG.Platform.Platform>();
                            targets.Add(RSG.Platform.Platform.Independent);
                            overriddenOutput.Add(SIO.Path.GetFileName(inputFile.AbsolutePath), targets);
                        }
                }
            }
            */
             
            foreach (IContentNode input in inputs)
            {
                if (!(input is Content.File))
                    continue;
                if (input.Parameters.ContainsKey(Constants.RageProcess_DoNotConvert) &&
                    (bool)input.Parameters[Constants.RageProcess_DoNotConvert])
                    continue;

                Content.File inputFile = (input as Content.File);

                // Create a new IProcess per-target (enabled) for the conversion.
                foreach (ITarget target in enabledTargets)
                {

                    if ((input.Parameters.ContainsKey(Constants.Platform_Override)
                        && ((RSG.Platform.Platform)input.Parameters[Constants.Platform_Override]) == target.Platform)
                        || (!input.Parameters.ContainsKey(Constants.Platform_Override) && !overriddenOutput[SIO.Path.GetFileName(inputFile.AbsolutePath)].Contains(target.Platform)))
                    {
                   
                        String convertFilename = String.Empty;
                        String rpfPath = String.Empty;
                        if (param.Flags.HasFlag(EngineFlags.Preview))
                        {
                            convertFilename = PlatformPathConversion.ConvertAndRemapFilenameToPreview(target, inputFile);
                        }
                        else if (useResourceCache)
                        {
                            String resourceCacheDir = PlatformProcessBuilder.GetResourceCacheRootDir(param);
                            convertFilename = PlatformPathConversion.ConvertAndRemapFilenameToDirectory(target, inputFile.AbsolutePath, resourceCacheDir);
                            String directory = SIO.Path.GetDirectoryName(inputFile.AbsolutePath);
                            if (!String.IsNullOrEmpty(rootSourceDir) && directory.StartsWith(rootSourceDir))
                                rpfPath = directory.Replace(rootSourceDir, "");
                        }
                        else
                        {
                            convertFilename = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(target, inputFile.AbsolutePath);
                        }
                        ProcessBuilder pb = CreateConvertProcess(param, processors, target, owner,
                            syncDependencies, inputFile, convertFilename);
                        IProcess conversionProcess = pb.ToProcess();
                        conversionProcess.State = ProcessState.Prebuilt;

                        // Update outputs with any RPF Path information.
                        if (!String.IsNullOrEmpty(rpfPath))
                        {
                            foreach (IContentNode output in conversionProcess.Outputs)
                            {
                                output.Parameters[Constants.Asset_RPFPath] =
                                    SIO.Path.Combine(rpfPath, (String)output.Parameters[Constants.Asset_RPFPath]);
                            }
                        }

                        resultantProcesses[target].Add(conversionProcess);
                        outputs[target].AddRange(conversionProcess.Outputs);
                    }
                    else
                    {
                        //param.Log.MessageCtx(LOG_CTX, "Content has been setup with overidden processed data: {0}.", inputFile.AbsolutePath);
                    }
                }
            }

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="inputs"></param>
        /// <param name="overriddenOutput"></param>
        private static void CollectOverrides(IEngineParameters param, IEnumerable<IContentNode> inputs, IDictionary<String, List<RSG.Platform.Platform>> overriddenOutput)
        {
            // Prepass the input so we know what's platform overriden data upfront.
            foreach (IContentNode input in inputs)
            {
                if (input.Parameters.ContainsKey(Constants.RageProcess_DoNotConvert) &&
                    (bool)input.Parameters[Constants.RageProcess_DoNotConvert])
                    continue;

                IFilesystemNode inputFileSystemNode = input as IFilesystemNode;
                if (input.Parameters.ContainsKey(Constants.Platform_Override))
                {
                    RSG.Platform.Platform plat = (RSG.Platform.Platform)input.Parameters[Constants.Platform_Override];
                    Debug.Assert(null != plat, "Invalid target supplied for platform override",
                        "A platform override parameter has been provided but it is not a valid target!");
                    if (null == plat)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "A platform override parameter has been provided but it is not a valid platform! Platform's target will be treated as default.");
                    }
                    else
                    {
                        if (overriddenOutput.ContainsKey(SIO.Path.GetFileName(inputFileSystemNode.AbsolutePath)))
                            overriddenOutput[SIO.Path.GetFileName(inputFileSystemNode.AbsolutePath)].Add(plat);
                        else
                        {
                            List<RSG.Platform.Platform> targets = new List<RSG.Platform.Platform>();
                            targets.Add(plat);
                            overriddenOutput.Add(SIO.Path.GetFileName(inputFileSystemNode.AbsolutePath), targets);
                        }
                    }
                }
                else
                {
                    if (overriddenOutput.ContainsKey(SIO.Path.GetFileName(inputFileSystemNode.AbsolutePath)))
                        overriddenOutput[SIO.Path.GetFileName(inputFileSystemNode.AbsolutePath)].Add(RSG.Platform.Platform.Independent);
                    else
                    {
                        List<RSG.Platform.Platform> targets = new List<RSG.Platform.Platform>();
                        targets.Add(RSG.Platform.Platform.Independent);
                        overriddenOutput.Add(SIO.Path.GetFileName(inputFileSystemNode.AbsolutePath), targets);
                    }
                }
            }
        }

        /// <summary>
        /// Create a process for deferred directory processing in Ragebuilder.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="processors"></param>
        /// <param name="processes"></param>
        /// <param name="process"></param>
        /// <param name="dependencies"></param>
        /// <param name="deferredDirectoryInputs"></param>
        private void CreateDeferredRageDirectoryProcess(IEngineParameters param, 
            IContentTree owner, IProcessorCollection processors, 
            IDictionary<ITarget, ICollection<IProcess>> processes,
            IProcess process,
            ISet<IContentNode> dependencies, 
            IEnumerable<Content.Directory> deferredDirectoryInputs)
        {
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);

            // List of input content with overridden platform.
            Dictionary<String, List<RSG.Platform.Platform>> overriddenOutput = new Dictionary<String, List<RSG.Platform.Platform>>();
            RagePreprocessor.CollectOverrides(param, deferredDirectoryInputs, overriddenOutput);

            foreach (Content.Directory directory in deferredDirectoryInputs)
            {
                if (directory.Parameters.ContainsKey(Constants.Platform_Override))
                {
                    ITarget target = directory.Parameters[Constants.Platform_Override] as ITarget;
                    Debug.Assert(null != target, "Invalid target supplied for platform override",
                        "A platform override parameter has been provided but it is not a valid target!");
                    if (null == target)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "A platform override parameter has been provided but it is not a valid target!");
                        return;
                    }
                    IProcess directoryConversionRageProcess = CreateDeferredRageDirectoryProcess(
                        param, owner, processors, target, dependencies, directory);
                    processes[target].Add(directoryConversionRageProcess);
                }
                else
                {
                    foreach (ITarget target in enabledTargets)
                    {
                        if ((directory.Parameters.ContainsKey(Constants.Platform_Override)
                        && ((RSG.Platform.Platform)directory.Parameters[Constants.Platform_Override]) == target.Platform)
                        || (!directory.Parameters.ContainsKey(Constants.Platform_Override) && !overriddenOutput[SIO.Path.GetFileName(directory.AbsolutePath)].Contains(target.Platform)))
                        {
                            IProcess directoryConversionRageProcess = CreateDeferredRageDirectoryProcess(
                                param, owner, processors, target, dependencies, directory);
                            processes[target].Add(directoryConversionRageProcess);
                        }
                        else
                        {
                            //param.Log.MessageCtx(LOG_CTX, "Content has been setup with overidden processed data: {0}.", directory.AbsolutePath);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Create a process for deferred directory processing in ragebuilder.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="target"></param>
        /// <param name="inputDirectory"></param>
        /// <returns></returns>
        private IProcess CreateDeferredRageDirectoryProcess(IEngineParameters param, IContentTree owner, 
            IProcessorCollection processors, ITarget target, ISet<IContentNode> dependencies, 
            Content.Directory inputDirectory)
        {
            ProcessBuilder directoryConversionRageProcessBuilder = new ProcessBuilder(RAGE_PROCESSOR, processors, owner);
            directoryConversionRageProcessBuilder.Inputs.Add(inputDirectory);
            String platformDirectory = String.Empty;

            if (param.Flags.HasFlag(EngineFlags.Preview))
                platformDirectory = target.Branch.Preview;
            else
                platformDirectory = SIO.Path.Combine(inputDirectory.AbsolutePath, target.Platform.ToString());

            AddParameterAllGenericDependencies(param, owner, dependencies, ref directoryConversionRageProcessBuilder);

            directoryConversionRageProcessBuilder.Outputs.Add(owner.CreateDirectory(platformDirectory));
            directoryConversionRageProcessBuilder.Parameters.Add("platform", target.Platform);
            IProcess directoryConversionRageProcess = directoryConversionRageProcessBuilder.ToProcess();
            directoryConversionRageProcess.State = ProcessState.Prebuilt;

            return (directoryConversionRageProcess);
        }
        #endregion // Private Methods

        #region IProcess Construction Methods
#warning LPXO TODO: Need to move the dependency generation into the RageResource Preprocess.
        /// <summary>
        /// Create a conversion process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="target"></param>
        /// <param name="owner"></param>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private ProcessBuilder CreateConvertProcess(IEngineParameters param, IProcessorCollection processors,
            ITarget target, IContentTree owner, ISet<IContentNode> syncDependencies,
            String input, String output, ProcessBuilder parent = null)
        {
            IContentNode inputNode = owner.CreateAsset(input, RSG.Platform.Platform.Independent);
            if (null == inputNode)
                inputNode = owner.CreateFile(input);

            return (CreateConvertProcess(param, processors, target, owner,
                syncDependencies, inputNode, output, parent));
        }

        /// <summary>
        /// Create a conversion process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="target"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="input"></param>
        /// <param name="output"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private ProcessBuilder CreateConvertProcess(IEngineParameters param, IProcessorCollection processors, 
            ITarget target, IContentTree owner, ISet<IContentNode> syncDependencies, 
            IContentNode input, String output, ProcessBuilder parent = null)
        {
            String outputPlatform = PlatformPathConversion.ConvertFilenameToPlatform(target, output, false);
            IContentNode outputNode = owner.CreateAsset(outputPlatform, target.Platform);
            IContentNode resourceTool = owner.CreateFile(target.GetRagebuilderExecutable());

            ProcessBuilder pb = new ProcessBuilder(RAGE_PROCESSOR, processors, owner);
            pb.Inputs.Add(input);
            pb.Outputs.Add(outputNode);
            pb.AdditionalDependentContent.Add(resourceTool);
            AddDependenciesFor(param, owner, syncDependencies, input, outputNode, ref pb);

            if (null != parent)
                parent.Inputs.Add(outputNode);
            return (pb);
        }
        #endregion // IProcess Construction Methods
    }

} // RSG.Pipeline.Processor.Platform namespace
