﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Pipeline.Services.Platform.Manifest;

namespace RSG.Pipeline.Processor.Platform
{

    /// <summary>
    /// Pipeline processor used to construct platform-independent game manifest 
    /// files.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class ManifestCreateProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String DESCRIPTION = "Manifest Creation Processor";

        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "RPF Manifest";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ManifestCreateProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Manifest creation processor has an empty prebuild; typically this
        /// processor is a resultant from the Prebuild of the RPF creation 
        /// processor.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            ICollection<IProcess> processes = new List<IProcess>();
            
            // Mark process as prebuilt; and append.
            process.State = ProcessState.Prebuilt;
            processes.Add(process);

            // We deliberately do not handle the platform conversion here so
            // we have definite access to the conversion output as additional
            // inputs into the RPF construction process.

            syncDependencies = new List<IContentNode>();
            resultantProcesses = processes;
            return (true);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            ICollection<XGE.ITool> xgeTools = new List<XGE.ITool>();
            ICollection<XGE.ITask> xgeTasks = new List<XGE.ITask>();
            bool allow_remote = GetXGEAllowRemote(param, false);
            XGE.ITool tool = XGEFactory.GetRpfTool(param.Log, param.Branch.Project.Config,
                "RPF Manifest Create", XGEFactory.RpfToolType.RpfManifest, allow_remote);
            xgeTools.Add(tool);
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("RPF Manifest");

            // Loop through each process; adding an XGE task for each manifest
            // file to be constructed.
            int packet = 0;
            foreach (IProcess process in processes)
            {
                String outputFilename = ((Content.File)process.Outputs.First()).AbsolutePath;
                String taskName = String.Format("RPF Manifest {0}", packet++);
                XGE.ITaskJob task = new XGE.TaskJob(taskName);

                // Get map export additions (optional)
                String mapExportAdditions = String.Empty;
                if (process.Parameters.ContainsKey(Constants.ManifestProcess_MapExportAdditions))
                {
                    Debug.Assert(process.Parameters[Constants.ManifestProcess_MapExportAdditions] is String);
                    if (!(process.Parameters[Constants.ManifestProcess_MapExportAdditions] is String))
                        param.Log.WarningCtx(LOG_CTX, "Map export additions specified as non-String filename!  Internal warning.");
                    else
                        mapExportAdditions = (String)process.Parameters[Constants.ManifestProcess_MapExportAdditions];
                }

                // Get bounds processor additions (optional)
                String boundsProcessorAdditions = String.Empty;
                if (process.Parameters.ContainsKey(Constants.ManifestProcess_BoundsProcessorAdditions))
                {
                    Debug.Assert(process.Parameters[Constants.ManifestProcess_BoundsProcessorAdditions] is String);
                    if (!(process.Parameters[Constants.ManifestProcess_BoundsProcessorAdditions] is String))
                        param.Log.WarningCtx(LOG_CTX, "Bounds processor additions specified as non-String filename!  Internal warning.");
                    else
                        boundsProcessorAdditions = (String)process.Parameters[Constants.ManifestProcess_BoundsProcessorAdditions];
                }

                // Get instance placement processor additions (optional)
                List<String> instancePlacementProcessorAdditions = new List<String>();
                String instancePlacementManifestListingFile = "";
                if (process.Parameters.ContainsKey(Constants.ManifestProcess_InstancePlacementProcessorAdditions))
                {
                    string containerDirectory = ((Content.Directory)process.Inputs.First()).AbsolutePath;
                    instancePlacementManifestListingFile = SIO.Path.Combine(containerDirectory, "InstancePlacementManifestListing.xml");
                    Debug.Assert(process.Parameters[Constants.ManifestProcess_InstancePlacementProcessorAdditions] is List<String>);
                    if (!(process.Parameters[Constants.ManifestProcess_InstancePlacementProcessorAdditions] is List<String>))
                        param.Log.WarningCtx(LOG_CTX, "Instance placement processor additions specified as non-List<String>!  Internal warning.");
                    else
                        instancePlacementProcessorAdditions = (List<String>)process.Parameters[Constants.ManifestProcess_InstancePlacementProcessorAdditions];

                    if (!SIO.Directory.Exists(containerDirectory))
                        SIO.Directory.CreateDirectory(containerDirectory);

                    InstancePlacementManifest instancePlacementManifest = new InstancePlacementManifest(instancePlacementProcessorAdditions);
                    instancePlacementManifest.Serialize(instancePlacementManifestListingFile);
                }

                // Get asset bindings.
                String assetBindingFilename = SIO.Path.Combine(XGEFactory.GetTempDirectory(param.Branch),
                    String.Format("{0}_AssetBindings.xml", taskName.Replace(' ', '_')));
                WriteAssetBindings(param, process, assetBindingFilename);

                StringBuilder arguments = new StringBuilder();
                arguments.AppendFormat("-taskname {0} ", taskName.Replace(' ', '_'));
                if (!String.IsNullOrWhiteSpace(mapExportAdditions))
                    arguments.AppendFormat("-map {0} ", mapExportAdditions);
                if (!String.IsNullOrWhiteSpace(boundsProcessorAdditions))
                    arguments.AppendFormat("-bounds {0} ", boundsProcessorAdditions);
                if (instancePlacementProcessorAdditions.Count > 0 && !String.IsNullOrEmpty(instancePlacementManifestListingFile))
                    arguments.AppendFormat("-instancePlacement {0} ", instancePlacementManifestListingFile);
                arguments.AppendFormat("-assetbindings {0} ", assetBindingFilename);
                arguments.AppendFormat("-output {0} ", outputFilename);

                task.Caption = taskName;
                task.Tool = tool;
                task.InputFiles.AddRange(process.Inputs.OfType<Content.IFilesystemNode>().Select(
                    file => file.AbsolutePath));
                task.OutputFiles.Add(outputFilename);
                task.Parameters = arguments.ToString();
                taskGroup.Tasks.Add(task);

                // Define the XGE task for the IProcess.
                process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(' ', '_'));
                process.Parameters.Add(Constants.ProcessXGE_Task, task);
            }

            xgeTasks.Add(taskGroup);
            tools = xgeTools;
            tasks = xgeTasks;
            return (result);
        }

        /// <summary>
        /// Parse log information.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="taskLogData"></param>
        public override bool ParseLog(IEngineParameters param, 
            IEnumerable<String> taskLogData)
        {
            bool hasErrors = false;
            RSG.Pipeline.Services.LogParsing.ParseConsoleLogOutput(
                param.Log, LOG_CTX, taskLogData, out hasErrors);
            return (!hasErrors);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Write asset bindings; from process inputs.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private bool WriteAssetBindings(IEngineParameters param, 
            IProcess process, String filename)
        {
            bool result = true;
            try
            {
                ICollection<HDTXDAssetBinding> hdTxdAssetBindings = 
                    new List<HDTXDAssetBinding>();

                // Find asset bindings set up in Rage processor.
                foreach (IContentNode input in process.Inputs)
                {
                    if (!(input is Asset) || !input.Parameters.ContainsKey(Constants.Asset_HDTXD))
                        continue;
                    Asset asset = (input as Asset);
                    Asset hdtxdAsset = (input.Parameters[Constants.Asset_HDTXD] as Asset);

                    switch (asset.AssetType)
                    {
                        case RSG.Platform.FileType.TextureDictionary:
                            hdTxdAssetBindings.Add(new HDTXDAssetBinding(
                                HDTXDBindingType.TextureDictionary, asset.Name, hdtxdAsset.Name));
                            break;
                        case RSG.Platform.FileType.Fragment:
                            hdTxdAssetBindings.Add(new HDTXDAssetBinding(
                                HDTXDBindingType.Fragment, asset.Name, hdtxdAsset.Name));
                            break;
                        case RSG.Platform.FileType.Drawable:
                            hdTxdAssetBindings.Add(new HDTXDAssetBinding(
                                HDTXDBindingType.Drawable, asset.Name, hdtxdAsset.Name));
                            break;
                        case RSG.Platform.FileType.DrawableDictionary:
                            hdTxdAssetBindings.Add(new HDTXDAssetBinding(
                                HDTXDBindingType.DrawableDictionary, asset.Name, hdtxdAsset.Name));
                            break;
                        default:
                            param.Log.WarningCtx(LOG_CTX, "Unrecognised asset type for HD TXD binding: {0}.  Ignored.",
                                asset.AssetType);
                            continue;
                    }
                }

                param.Log.MessageCtx(LOG_CTX, 
                    "Saving manifest asset binding information to {0}.",
                    filename);
                String assetBindingDirectory = SIO.Path.GetDirectoryName(filename);
                if (!SIO.Directory.Exists(assetBindingDirectory))
                    SIO.Directory.CreateDirectory(assetBindingDirectory);

                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("HDTxdAssetBindings",
                        hdTxdAssetBindings.Select(b => b.AsCHDTxdAssetBinding("Item"))));
                xmlDoc.Save(filename);
            }
            catch (Exception ex)
            {
                param.Log.ExceptionCtx(LOG_CTX, ex,
                    "Manifest asset binding write failure: {0}.", filename);
                result = false;
            }
            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Platform namespace
