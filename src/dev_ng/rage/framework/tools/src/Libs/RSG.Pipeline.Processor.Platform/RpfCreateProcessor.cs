﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;

namespace RSG.Pipeline.Processor.Platform
{

    /// <summary>
    /// Pipeline processor used to construct RPF files.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class RpfCreateProcessor :
        RagebuilderBaseProcessor,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String DESCRIPTION = "RPF7 Creation Processor";

        /// <summary>
        /// 
        /// </summary>
        private static readonly String LOG_CTX = "RPF Create";

        /// <summary>
        /// Parameter to tell system to delete temporary file lists.
        /// </summary>
        private static readonly String PARAM_DELETE_FILES = "Delete Temp Files";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RpfCreateProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)


        /// <summary>
        /// Copy metadata directory parameter (RPF sorting).
        /// </summary>
        /// <param name="inputProcess"></param>
        /// <param name="outputProcess"></param>
        private void AddMetadataDirectoryParameter(IContentTree tree, IProcess inputProcess, ProcessBuilder outputProcess)
        {
            if (inputProcess.Parameters.ContainsKey(Constants.RpfProcess_MetadataDirectory) &&
                !outputProcess.Parameters.ContainsKey(Constants.RpfProcess_MetadataDirectory))
            {
                String metadataDirectory = (String)inputProcess.Parameters[Constants.RpfProcess_MetadataDirectory];
                outputProcess.Parameters.Add(Constants.RpfProcess_MetadataDirectory, metadataDirectory);
                outputProcess.AdditionalDependentContent.Add(tree.CreateDirectory(metadataDirectory));
            }
        }

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            Debug.Assert(process.Inputs.Any(),
                "RPF Create Processor Input Error", "No input specified.");
            if (!process.Inputs.Any())
            {
                param.Log.ErrorCtx(LOG_CTX, "No input specified.");
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return (false);
            }
            Debug.Assert(1 == process.Outputs.Count(),
                "RPF Create Processor Output Error", 
                "Expecting a single output; given: {0}.", process.Outputs.Count());
            if (1 != process.Outputs.Count())
            {
                param.Log.ErrorCtx(LOG_CTX, "Expecting a single output; given: {0}.",
                    process.Outputs.Count());
                resultantProcesses = new List<IProcess>();
                syncDependencies = new List<IContentNode>();
                return (false);
            }

            Asset output = process.Outputs.First() as Asset;
            List<IProcess> processes = new List<IProcess>();
            if (RSG.Platform.Platform.Independent == output.Platform)
            {
                // We need to expand to each enabled platform; throwing away
                // our initial process.
                Debug.Assert(output.AbsolutePath.Contains("$(target)"),
                    "RPF Create Processor Platform Error",
                    "Path {0} does not contain $(target) expansion and asset has independent target.",
                    output.AbsolutePath);
                IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
                foreach (ITarget target in enabledTargets)
                {
                    ProcessBuilder pb = new ProcessBuilder(this, owner);
                    String outputFilename = target.Environment.Subst(output.AbsolutePath);
                    IContentNode outputNode = owner.CreateAsset(outputFilename, target.Platform);
                    pb.Inputs.AddRange(process.Inputs);
                    pb.Outputs.Add(outputNode);

                    AddMetadataDirectoryParameter(owner, process, pb); // For RPF Sort.
                    IProcess rpfProcess = pb.ToProcess();
                    processes.Add(rpfProcess);
                }
                // Discard old process as we've replaced it with target expansion.
                process.State = ProcessState.Discard;
            }
            else
            {
                Debug.Assert(param.Branch.Targets.ContainsKey(output.Platform),
                    "RPF Create Processor Platform Error",
                    "Branch does not have RPF asset's platform: {0}.", output.Platform);
                if (!param.Branch.Targets.ContainsKey(output.Platform))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Branch does not have RPF asset's platform: {0}.", output.Platform);
                    resultantProcesses = new List<IProcess>();
                    syncDependencies = new List<IContentNode>();
                    return (false);
                }
                ITarget target = param.Branch.Targets[output.Platform];
                if (!target.Enabled)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Branch does not have RPF asset's platform enabled: {0}.", output.Platform);
                    resultantProcesses = new List<IProcess>();
                    syncDependencies = new List<IContentNode>();
                    return (false);
                }

                IContentNode platformManifestAsset =  null;
                IContentNode manifestAsset = null;

                // we check every output for this process, to determine if we skip the manifest for it.
                bool generateManifest = true;
                foreach (IContentNode node in process.Outputs)
                {
                    // could be written as LINQ Aggregate, but this is clearer
                    generateManifest &= node.GetParameter(Constants.RpfCreate_GenerateManifest, true);
                }
               
                if (generateManifest)
                {
                    // Add Manifest Creation processor steps.
                    IProcessor manifestCreateProc = processors.OfType<ManifestCreateProcessor>().FirstOrDefault();
                    ProcessBuilder manifestPb = new ProcessBuilder(manifestCreateProc, owner);
                    String manifestTempDir = SIO.Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "manifest");
                    String manifestDataDir = SIO.Path.Combine(manifestTempDir, output.Basename);
                    String manifestUniqueName = output.AbsolutePath.Replace("/", "\\").Replace(param.Branch.Build, "").Replace("\\", "_");

                    String manifestFilename = SIO.Path.Combine(manifestTempDir,
                        String.Format("{0}_{1}_manifest.imf", SIO.Path.GetFileNameWithoutExtension(manifestUniqueName), target.Platform));
                    IContentNode manifestData = owner.CreateDirectory(manifestDataDir, "*.*");
                    manifestAsset = owner.CreateAsset(manifestFilename, RSG.Platform.Platform.Independent);
                    manifestPb.Inputs.Add(manifestData);
                    manifestPb.Inputs.AddRange(process.Inputs);
                    manifestPb.Outputs.Add(manifestAsset);

                    if (process.Parameters.ContainsKey(Constants.ManifestProcess_MapExportAdditions))
                    {
                        String mapExportAdditionsPathname = (String)process.Parameters[Constants.ManifestProcess_MapExportAdditions];
                        manifestPb.Parameters.Add(Constants.ManifestProcess_MapExportAdditions, mapExportAdditionsPathname);
                        IContentNode maxExportAdditionsNode = owner.CreateFile(mapExportAdditionsPathname);
                        manifestPb.Inputs.Add(maxExportAdditionsNode);
                    }

                    if (process.Parameters.ContainsKey(Constants.ManifestProcess_BoundsProcessorAdditions))
                    {
                        String boundsProcessorAdditionsPathname = (String)process.Parameters[Constants.ManifestProcess_BoundsProcessorAdditions];
                        manifestPb.Parameters.Add(Constants.ManifestProcess_BoundsProcessorAdditions, boundsProcessorAdditionsPathname);
                        IContentNode boundsProcessorAdditionsNode = owner.CreateFile(boundsProcessorAdditionsPathname);
                        manifestPb.Inputs.Add(boundsProcessorAdditionsNode);
                    }

                    if (process.Parameters.ContainsKey(Constants.ManifestProcess_InstancePlacementProcessorAdditions))
                    {
                        List<String> instancePlacementProcessorAdditions = (List<String>)process.Parameters[Constants.ManifestProcess_InstancePlacementProcessorAdditions];
                        manifestPb.Parameters.Add(Constants.ManifestProcess_InstancePlacementProcessorAdditions, instancePlacementProcessorAdditions);

                        foreach (String addition in instancePlacementProcessorAdditions)
                        {
                            IContentNode instancePlacementProcessorAdditionsNode = owner.CreateFile(addition);
                            manifestPb.Inputs.Add(instancePlacementProcessorAdditionsNode);
                        }
                    }

                    ProcessBuilder manifestPlatformProcess = PlatformProcessBuilder.CreateManifestConvertProcessor(param, processors, owner, manifestAsset, target);
                    platformManifestAsset = manifestPlatformProcess.Outputs.First();
                    if (!platformManifestAsset.Parameters.ContainsKey(Constants.RpfProcess_DoNotPack))
                    {
                        platformManifestAsset.Parameters.Add(Constants.RpfProcess_DoNotPack, true);
                    }

                    IProcess manifestResourceProc = manifestPlatformProcess.ToProcess();
                    manifestResourceProc.State = ProcessState.Prebuilt;
                    processes.Add(manifestResourceProc);
                    processes.Add(manifestPb.ToProcess());
                }

                // Discard old process; as we need to add a new input of the manifest file.
                process.State = ProcessState.Discard;

                ProcessBuilder rpfCreatePb = new ProcessBuilder(process);
                AddMetadataDirectoryParameter(owner, process, rpfCreatePb); // For RPF Sort.
                if (generateManifest)
                {
                    rpfCreatePb.Inputs.Add(platformManifestAsset);
                    rpfCreatePb.AdditionalDependentContent.Add(manifestAsset);
                }

                IProcess rpfCreateProc = rpfCreatePb.ToProcess();
                rpfCreateProc.State = ProcessState.Prebuilt;
                processes.Add(rpfCreateProc);
            }

            resultantProcesses = processes;
            syncDependencies = new List<IContentNode>();
            return (true);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
#warning DHM FIX ME: handle duplicate tools; send project?

            // Single RpfCreate tool for all targets.
            bool allow_remote = GetXGEAllowRemote(param, false);
            XGE.ITool tool = XGEFactory.GetRpfTool(param.Log, param.Branch.Project.Config,
                "RPF Create", XGEFactory.RpfToolType.RpfCreate, allow_remote);

            // Add task groups.       
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            IDictionary<ITarget, XGE.ITaskJobGroup> targetGroups = new Dictionary<ITarget, XGE.ITaskJobGroup>();
            foreach (ITarget target in enabledTargets)
            {
                String taskGroupName = String.Format("RpfCreate [{0}]", target.Platform);
                targetGroups.Add(target, new XGE.TaskGroup(taskGroupName));
            }

            bool result = true;
            foreach (IProcess process in processes)
            {
                Asset output = process.Outputs.First() as Asset;
                Debug.Assert(param.Branch.Targets.ContainsKey(output.Platform),
                    "RPF Create Processor Platform Error",
                    "Branch does not have RPF asset's platform: {0}.", output.Platform);
                if (!param.Branch.Targets.ContainsKey(output.Platform))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Branch does not have RPF asset's platform: {0}.  Skipping packet.", output.Platform);
                    continue;
                }
                ITarget target = param.Branch.Targets[output.Platform];
                if (!param.Branch.Targets[output.Platform].Enabled)
                {
                    param.Log.ErrorCtx(LOG_CTX, "Branch does not have RPF asset's platform enabled: {0}.  Skipping packet.", output.Platform);
                    continue;
                }

#warning DHM FIX ME: how do we handle sub-directories in here?
                ICollection<Pair<String, String>> files = GetRpfProcessFileMapping(param, process);

                XGE.ITask task = Rpf.GetXgeRpfPackTask(param.Log, process,
                    target, output.AbsolutePath, files, tool);
                targetGroups[target].Tasks.Add(task);
            }

            // Assign method output.
            ICollection<XGE.ITool> allTools = new List<XGE.ITool>();
            allTools.Add(tool);

            tools = allTools;
            tasks = targetGroups.Values;

            return (result);
        }

        /// <summary>
        /// Callback for when the build process completes.
        /// </summary>
        /// <param name="param"></param>
        public override void Finished(IEngineParameters param)
        {
            this.LoadParameters(param);
            if (param.Log.HasErrors ||
                (this.Parameters.ContainsKey(PARAM_DELETE_FILES) &&
                 (bool)this.Parameters[PARAM_DELETE_FILES]))
            {
                String rpfTmpDir = Rpf.GetRpfCreateTempDir(param.Branch);
                if (SIO.Directory.Exists(rpfTmpDir))
                {
                    String[] tmpfiles = SIO.Directory.GetFiles(rpfTmpDir, "*.*",
                        SIO.SearchOption.AllDirectories);
                    foreach (String tmpfile in tmpfiles)
                    {
                        try
                        {
                            SIO.File.Delete(tmpfile);
                        }
                        catch (Exception ex)
                        {
                            param.Log.ExceptionCtx(LOG_CTX, ex, "Error deleting temporary RPF data file: {0}.",
                                tmpfile);
                        }
                    }

                    String[] tmpdirs = SIO.Directory.GetDirectories(rpfTmpDir, "*.*", 
                        SIO.SearchOption.AllDirectories);
                    foreach (String tmpdir in tmpdirs)
                    {
                        try
                        {
                            SIO.Directory.Delete(tmpdir);
                        }
                        catch (Exception ex)
                        {
                            param.Log.ExceptionCtx(LOG_CTX, ex, "Error deleting temporary RPF data file: {0}.",
                                tmpdir);
                        }
                    }
                }
            }
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Return collection of input/dest pairs for RPF script construction.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        /// Sub-folders are handled using the Asset_RPFPath parameter.
        /// 
        private ICollection<Pair<String, String>> GetRpfProcessFileMapping(IEngineParameters param, IProcess process)
        {
            ICollection<Pair<String, String>> files = new List<Pair<String, String>>();
            foreach (IContentNode input in process.Inputs)
            {
                // Skip content nodes flagged to skip.
                if (input.Parameters.ContainsKey(Constants.RpfProcess_DoNotPack) &&
                    (bool)input.Parameters[Constants.RpfProcess_DoNotPack])
                    continue;

                if (input is File)
                {
                    File file = (File)input;
                    String inputFilename = file.AbsolutePath;
                    String rpfFilename = String.Empty;
                    if (file.Parameters.ContainsKey(Constants.Asset_RPFPath))
                        rpfFilename = (String)(file.Parameters[Constants.Asset_RPFPath]);
                    else
                        rpfFilename = System.IO.Path.GetFileName(file.AbsolutePath);
                    files.Add(new Pair<String, String>(inputFilename, rpfFilename));
                }
                else if (input is Directory)
                {
                    Directory directory = (Directory)input;
                    String inputFilename = directory.AbsolutePath + '\\' + directory.Wildcard;
                    String rpfFilename = "*";
                    files.Add(new Pair<String, String>(inputFilename, rpfFilename));
                }
                else
                {
                    param.Log.WarningCtx(LOG_CTX, "Attempting to pack an input that is neither File nor Directory: '{0}'.", input.Name);
                }
            }

            return (files);
        }
        #endregion // Private Methods

        #region Static Methods
        /// <summary>
        /// Add an ITYP dependency node to a process (expects RPF Create Processor).
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="owner"></param>
        /// <param name="files"></param>
        /// <param name="process"></param>
        public static void AddDependencyITYP(IEngineParameters param, IContentTree owner,
            IEnumerable<String> files, ref ProcessBuilder process)
        {
            Debug.Assert(process.ProcessorClassName.Contains("RpfCreateProcessor"));
            if (!process.ProcessorClassName.Contains("RpfCreateProcessor"))
                return;

            String ityp_filename = files.FirstOrDefault(f => 
                String.Equals(".ityp", SIO.Path.GetExtension(f)));
            if (!String.IsNullOrEmpty(ityp_filename))
            {
                process.AdditionalDependentContent.Add(owner.CreateFile(ityp_filename));
            }
        }

        /// <summary>
        /// Add an ITYP dependency node to a process (expects RPF Create Processor).
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="owner"></param>
        /// <param name="files"></param>
        /// <param name="process"></param>
        public static void AddDependencyITYP(IEngineParameters param, IContentTree owner,
            IEnumerable<IContentNode> files, ref ProcessBuilder process)
        {
            IEnumerable<String> filenames = files.OfType<IFilesystemNode>().Select(f => f.AbsolutePath);
            RpfCreateProcessor.AddDependencyITYP(param, owner, filenames, ref process);
        }

        #region IProcess Construction Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="processes"></param>
        public static void CreateRpfPackingProcesses(IEngineParameters param,
            IContentTree owner, IProcess process, IDictionary<ITarget, ICollection<IProcess>> processes, IProcessor rpfCreateProcessor)
        {
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            Asset outputRpf = process.Outputs.First() as Asset;

            foreach (ITarget target in enabledTargets)
            {
                IEnumerable<IContentNode> rpfInputs = processes[target].SelectMany(p => p.Outputs);
                ProcessBuilder pb = CreateRpfProcess(param, target, owner, rpfCreateProcessor, rpfInputs, outputRpf.AbsolutePath, outputRpf);
                AddDependencyITYP(param, owner, process.Inputs, ref pb);

                IProcess rpfProcess = pb.ToProcess();
                processes[target].Add(rpfProcess);
            }
        }

        /// <summary>
        /// Create a RPF construction process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="target"></param>
        /// <param name="owner"></param>
        /// <param name="processor"></param>
        /// <param name="inputs"></param>
        /// <param name="output"></param>
        /// <param name="originalOutputNode">source output node, used to copy parameter from original ContentTree data.</param>
        /// <returns></returns>
        public static ProcessBuilder CreateRpfProcess(IEngineParameters param, ITarget target,
            IContentTree owner, IProcessor processor, IEnumerable<IContentNode> inputs, String output, IContentNode originalOutputNode = null)
        {
            String outputPath = param.Branch.Targets[target.Platform].Environment.Subst(output);
            IContentNode outputRpf = owner.CreateAsset(outputPath, target.Platform);

            // allows propagation of parameters from source file-based ContentTree and the in memory temp ContentTree.
            if (originalOutputNode != null)
            {
                outputRpf.CopyParametersFrom(originalOutputNode);
            }
            IContentNode resourceTool = owner.CreateFile(target.GetRagebuilderExecutable());

            ProcessBuilder pb = new ProcessBuilder(processor, owner);
            pb.Inputs.AddRange(inputs);
            pb.Outputs.Add(outputRpf);
            pb.AdditionalDependentContent.Add(resourceTool);

            return (pb);
        }
        #endregion
        #endregion
    }

} // RSG.Pipeline.Processor.Platform
