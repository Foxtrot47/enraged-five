﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using IB = RSG.Interop.Incredibuild;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.Platform;
using Ionic.Zip;
using RSG.Pipeline.Services.Platform.Texture;

namespace RSG.Pipeline.Processor.Platform
{

    /// <summary>
    /// Abstract base processor class for all processors that invoke Ragebuilder.
    /// </summary>
    /// Includes common Ragebuilder output logging methods.
    /// 
    public abstract class RagebuilderBaseProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        private static readonly Regex REGEX_FILETYPE_DEPENDENCIES = new Regex(@"Dependencies\((?<type>.*)\)", RegexOptions.IgnoreCase);
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// File-type dependencies (cached) from parameter XML.
        /// </summary>
        private IDictionary<RSG.Platform.FileType, IEnumerable<String>> m_fileTypeDependencies;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public RagebuilderBaseProcessor(String description)
            : base(description)
        {
            this.m_fileTypeDependencies = new Dictionary<RSG.Platform.FileType, IEnumerable<String>>();
        }
        #endregion // Constructor(s)

        #region IProcessor Controller Methods
        /// <summary>
        /// Parse log information; processors are only passed log data for their
        /// respective IProcess output.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="taskLogData">Log string data.</param>
        public override bool ParseLog(IEngineParameters param, IEnumerable<String> taskLogData)
        {
            bool hasErrors = false;
            bool captureResBinningStats = false;
            if (this.Parameters.ContainsKey(Constants.RageProcess_CaptureResourceStats))
                captureResBinningStats = (bool)this.Parameters[Constants.RageProcess_CaptureResourceStats];

            int captureResFileSize = ResourceStatsLogFile.FILE_SIZE_LIMIT_DEFAULT;
            if (this.Parameters.ContainsKey(Constants.RageProcess_CaptureResourceStatsSizeLimit))
                captureResFileSize = (int)this.Parameters[Constants.RageProcess_CaptureResourceStatsSizeLimit];

            Ragebuilder.ParseRagebuilderOutput(param.Branch, param.Log, taskLogData,
                captureResBinningStats, (long)captureResFileSize, out hasErrors);
            return (!hasErrors);
        }
        #endregion // IProcessor Controller Methods
        
        #region Dependency Methods
        /// <summary>
        /// Cache the generic dependencies specified in the parameters XML.
        /// </summary>
        /// <param name="param"></param>
        protected virtual void CacheDependencyParameters(IEngineParameters param, IContentTree owner)
        {
            if (this.m_fileTypeDependencies.Count > 0)
                return; // already cached from previous Prebuild call.

            // Loop through the parameters; caching the dependencies.
            foreach (KeyValuePair<String, Object> kvp in this.Parameters)
            {
                Match match = REGEX_FILETYPE_DEPENDENCIES.Match(kvp.Key);
                if (!match.Success)
                    continue; // Skip; not a dependency parameter.

                Debug.Assert(2 == match.Groups.Count);
                if (2 != match.Groups.Count)
                    param.Log.Warning("Dependency FileType unexpected match result '{0}' ({1}).",
                        kvp.Key, match.Groups.Count);

                String fileTypeString = match.Groups["type"].Value;

                FileType filetype = FileType.Unknown;
                if (!Enum.TryParse<RSG.Platform.FileType>(fileTypeString, out filetype))
                {
                    param.Log.Warning("Invalid dependency parameter: '{0}'; file type '{1}' not found.",
                        kvp.Key, fileTypeString);
                    continue;
                }

                ICollection<String> dependencies = null;
                if (this.m_fileTypeDependencies.ContainsKey(filetype))
                    dependencies = (ICollection<String>)(this.m_fileTypeDependencies[filetype]);
                else
                {
                    dependencies = new List<String>();
                    this.m_fileTypeDependencies.Add(filetype, dependencies);
                }

                IEnumerable<Object> dependencyValues = (kvp.Value as IEnumerable<Object>);
                dependencies.AddRange(dependencyValues.Select(d => d.ToString()));
            }
        }

        /// <summary>
        /// Add all dependencies for a particular asset (wrapper).
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="inputNode"></param>
        /// <param name="outputNode"></param>
        /// <param name="process"></param>
        protected virtual void AddDependenciesFor(IEngineParameters param, IContentTree owner,
            ISet<IContentNode> syncDependencies, IContentNode inputNode,
            IContentNode outputNode, ref ProcessBuilder process)
        {
            if ((inputNode is Asset) && (outputNode is Asset))
            {
                Asset inputAsset = (inputNode as Asset);
                Asset outputAsset = (outputNode as Asset);
                AddParameterGenericDependencies(param, owner, syncDependencies,
                    inputAsset, outputAsset, ref process);
                AddTexturePipelineDependencies(param, owner, syncDependencies,
                    inputAsset, outputAsset, ref process);
                if (outputAsset.Platform == RSG.Platform.Platform.XBSX || outputAsset.Platform == RSG.Platform.Platform.PS5)
                {
                    if (!inputNode.GetParameter(Constants.No_BVH_Generation, false))
                    {
                        AddBvhDependencies(param, owner, syncDependencies,
                            inputAsset, outputAsset, ref process);
                    }
                }
            }
            else if (inputNode is Directory)
            {
#warning DHM FIX ME: this is a bit of a hack to handle Directory dependencies.
                AddParameterAllGenericDependencies(param, owner, syncDependencies,
                    ref process);
            }
        }

        /// <summary>
        /// Add generic dependencies as defined in the processor XML parameter file.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="inputAsset"></param>
        /// <param name="outputAsset"></param>
        /// <param name="process"></param>
        protected virtual void AddParameterGenericDependencies(IEngineParameters param,
            IContentTree owner, ISet<IContentNode> syncDependencies,
            Content.Asset inputAsset, Content.Asset outputAsset,
            ref ProcessBuilder process)
        {
            if (!this.m_fileTypeDependencies.ContainsKey(inputAsset.AssetType))
                return; // Skip; no generic dependencies defined.

            IEnumerable<String> dependencies = this.m_fileTypeDependencies[inputAsset.AssetType];
            IEnvironment environment = param.Branch.Environment;
            environment.Push();
            param.Branch.Import(environment);
            try
            {
                // Add additional environment options here.
                environment.Add("basename", inputAsset.Basename);

                List<IContentNode> deps = new List<IContentNode>();
                foreach (String d in dependencies)
                {
                    String substDep = environment.Subst(d);
                    if (SIO.Directory.Exists(substDep))
                        deps.Add(owner.CreateDirectory(substDep));
                    else
                        deps.Add(owner.CreateFile(substDep));
                }

                // Add the dependencies to our input asset.
                syncDependencies.AddRange(deps);
                process.AdditionalDependentContent.AddRange(deps);
            }
            finally
            {
                environment.Pop();
            }
        }

        /// <summary>
        /// Add all generic dependencies as defined in the processor XML parameter file
        /// (where they do not have $(basename) requirement).
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="process"></param>
        protected virtual void AddParameterAllGenericDependencies(IEngineParameters param,
            IContentTree owner, ISet<IContentNode> syncDependencies, ref ProcessBuilder process)
        {
#warning DHM FIX ME: this is a bit of a hack to handle Directory dependencies.
            IEnvironment environment = param.Branch.Environment;
            environment.Push();
            param.Branch.Import(environment);
            try
            {
                foreach (KeyValuePair<FileType, IEnumerable<String>> kvp in this.m_fileTypeDependencies)
                {
                    IEnumerable<String> nonAssetSpecificDependencies = kvp.Value.Where(
                        v => !v.Contains("basename"));
                    List<IContentNode> deps = new List<IContentNode>();
                    foreach (String d in nonAssetSpecificDependencies)
                    {
                        String substDep = environment.Subst(d);
                        if (SIO.Directory.Exists(substDep))
                            deps.Add(owner.CreateDirectory(substDep));
                        else
                            deps.Add(owner.CreateFile(substDep));
                    }

                    // Add the dependencies to our input asset.
                    syncDependencies.AddRange(deps);
                    process.AdditionalDependentContent.AddRange(deps);
                }
            }
            finally
            {
                environment.Pop();
            }
        }

        /// <summary>
        /// Add texture pipeline dependencies to the dependencies list.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="owner"></param>
        /// <param name="inputAsset"></param>
        /// <param name="process"></param>
        /// This method opens up the asset zip file and looks for any texture
        /// pipeline specifications; adding any parent references to the
        /// dependencies list.
        /// 
        protected virtual void AddTexturePipelineDependencies(IEngineParameters param,
            IContentTree owner, ISet<IContentNode> syncDependencies,
            Content.Asset inputAsset, Content.Asset outputAsset,
            ref ProcessBuilder process)
        {
            Debug.Assert(param.Branch.Targets.ContainsKey(outputAsset.Platform),
                "Internal error: asset platform not found.");
            if (!param.Branch.Targets.ContainsKey(outputAsset.Platform))
            {
                param.Log.Error("Internal error: asset platform not found for texture pipeline dependencies.");
                return;
            }
            ITarget outputTarget = param.Branch.Targets[outputAsset.Platform];

            switch (inputAsset.AssetType)
            {
                case FileType.Drawable:
                case FileType.DrawableDictionary:
                case FileType.Fragment:
                case FileType.TextureDictionary:
                case FileType.VisualEffectDictionary:
                    try
                    {
                        Debug.Assert(inputAsset.Platform == RSG.Platform.Platform.Independent);

                        // DHM FIX ME: this gets hit reloading the same zip for each target.
                        if (SIO.File.Exists(inputAsset.AbsolutePath))
                        {
                            Debug.Assert(ZipFile.IsZipFile(inputAsset.AbsolutePath));
                            using (ZipFile zf = ZipFile.Read(inputAsset.AbsolutePath))
                            {
                                foreach (ZipEntry ze in zf.Entries)
                                {
                                    try
                                    {
                                        if (!Specification.IsSpecification(ze.FileName))
                                            continue;

                                        using (SIO.MemoryStream ms = new SIO.MemoryStream())
                                        {
                                            ze.Extract(ms);
                                            ms.Seek(0, SIO.SeekOrigin.Begin);

#if USE_SPECIFICATION_FACTORY
                                            ISpecification spec = SpecificationFactory.Load(
                                                outputTarget, ms);
#else
                                            ISpecification spec = new Specification(outputTarget, ms);
#endif // !USE_SPECIFICATION_FACTORY

                                            // Add input dependencies.
                                            IEnumerable<IContentNode> specTemplates =
                                                spec.Filenames.Select(s => owner.CreateFile(s));
                                            IEnumerable<IContentNode> imageFiles =
                                                spec.ImageFiles.Select(s => owner.CreateFile(s));
                                            IEnumerable<IContentNode> resourceTextures =
                                                spec.ResourceTextures.Select(s => owner.CreateFile(s.AbsolutePath));

                                            syncDependencies.AddRange(specTemplates);
                                            syncDependencies.AddRange(imageFiles);
                                            syncDependencies.AddRange(resourceTextures);
                                            process.AdditionalDependentContent.AddRange(specTemplates);
                                            process.AdditionalDependentContent.AddRange(imageFiles);
                                            process.Inputs.AddRange(resourceTextures);

                                            // Validation for mis-configured specifications.
                                            if (spec.SkipProcessing && spec.ResourceTextures.Any(tex => tex.AbsolutePath.IndexOf(".tif") > -1))
                                            {
                                                param.Log.Error("Mis-configured texture specification '{0}:{1}': skipProcessing set with tiff input texture!",
                                                    inputAsset.AbsolutePath, ze.FileName);
                                            }

                                            // Validation for mis-configured specifications.
                                            if (spec.ImageSplitHD && spec.SkipProcessing)
                                            {
                                                param.Log.Warning("Mis-configured texture specification '{0}:{1}': imageSplitHD and skipProcessing set!",
                                                    inputAsset.AbsolutePath, ze.FileName);
                                            }
                                            else if (spec.ImageSplitHD && !spec.SkipProcessing)
                                            {
                                                // Add additional output(s); e.g. HD TXDs.
                                                String extension = SIO.Path.GetExtension(outputAsset.AbsolutePath);
                                                StringBuilder outputFilenameSb = new StringBuilder();
                                                outputFilenameSb.Append(SIO.Path.Combine(
                                                    SIO.Path.GetDirectoryName(outputAsset.AbsolutePath),
                                                    SIO.Path.GetFileNameWithoutExtension(outputAsset.AbsolutePath)));
                                                switch (inputAsset.AssetType)
                                                {
                                                    case FileType.Drawable:
                                                        outputFilenameSb.AppendFormat("+hidr{0}", extension);
                                                        break;
                                                    case FileType.DrawableDictionary:
                                                        outputFilenameSb.AppendFormat("+hidd{0}", extension);
                                                        break;
                                                    case FileType.Fragment:
                                                        outputFilenameSb.AppendFormat("+hifr{0}", extension);
                                                        break;
                                                    case FileType.TextureDictionary:
                                                        outputFilenameSb.AppendFormat("+hi{0}", extension);
                                                        break;
                                                }

                                                String outputFilename = outputFilenameSb.ToString();
                                                ITarget target = param.Branch.Targets[outputAsset.Platform];
                                                String txdExtension = FileType.TextureDictionary.GetExportExtension();
                                                String platformExtension = PlatformPathConversion.ConvertExtensionToPlatform(target, txdExtension);
                                                outputFilename = SIO.Path.ChangeExtension(outputFilename, platformExtension);

                                                IContentNode newOutput = owner.CreateAsset(outputFilename, outputAsset.Platform);
                                                if (!process.Outputs.Contains(newOutput))
                                                    process.Outputs.Add(newOutput);

                                                // Add HD TXD parameter to our output asset.
                                                if (!outputAsset.Parameters.ContainsKey(Constants.Asset_HDTXD))
                                                    outputAsset.Parameters.Add(Constants.Asset_HDTXD, newOutput);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        param.Log.Error("Error loading texture specification {0}:{1}.",
                                            inputAsset.AbsolutePath, ze.FileName);
                                        param.Log.Exception(ex, "Exception loading texture specification.");
                                    }
                                }
                            }
                        }
                    }
                    catch (Ionic.Zip.ZipException ex)
                    {
                        param.Log.ToolException(ex, "Ionic.Zip read exception during texture dependency fetch.");
                        param.Log.Error("Error reading Zip file: {0}.",
                            inputAsset.AbsolutePath);
                    }
                    catch (System.Exception ex)
                    {
                        param.Log.ToolException(ex, "Unhandled exception during texture dependency fetch.");
                        param.Log.Error("Error reading Zip file: {0}.",
                            inputAsset.AbsolutePath);
                    }
                    break;
            }
        }

        protected virtual void AddBvhDependencies(IEngineParameters param,
            IContentTree owner, ISet<IContentNode> syncDependencies,
            Content.Asset inputAsset, Content.Asset outputAsset,
            ref ProcessBuilder process)
        {
            Debug.Assert(param.Branch.Targets.ContainsKey(outputAsset.Platform),
                "Internal error: asset platform not found.");
            if (!param.Branch.Targets.ContainsKey(outputAsset.Platform))
            {
                param.Log.Error("Internal error: asset platform not found for BVH dependencies.");
                return;
            }
            ITarget outputTarget = param.Branch.Targets[outputAsset.Platform];

            Debug.Assert(inputAsset.Platform == RSG.Platform.Platform.Independent);
            if (SIO.File.Exists(inputAsset.AbsolutePath))
            {
                ITarget target = param.Branch.Targets[outputAsset.Platform];
                switch (inputAsset.AssetType)
                {
                    case FileType.DrawableDictionary:
                    case FileType.Drawable:
                    case FileType.Fragment:
                        {
                            String extension = (inputAsset.AssetType == FileType.DrawableDictionary ? 
                                FileType.ModelBvhDictionary.GetExportExtension() : FileType.ModelBvhData.GetExportExtension());
                            String platformExtension = PlatformPathConversion.ConvertExtensionToPlatform(target, extension);
                            
                            StringBuilder outputFilenameSb = new StringBuilder();
                            outputFilenameSb.Append(SIO.Path.Combine(
                                SIO.Path.GetDirectoryName(outputAsset.AbsolutePath),
                                SIO.Path.GetFileNameWithoutExtension(outputAsset.AbsolutePath)));

                            outputFilenameSb.AppendFormat(".{0}", platformExtension);
                            String outputFilename = outputFilenameSb.ToString();
                            IContentNode newOutput = owner.CreateAsset(outputFilename, outputAsset.Platform);
                            if (!process.Outputs.Contains(newOutput))
                                process.Outputs.Add(newOutput);
                        }
                        break;
                }
            }
        }
        #endregion // Dependency Methods
    }

} // RSG.Pipeline.Processor.Platform namespace
