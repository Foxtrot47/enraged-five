﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Text;
using SIO = System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using IB = RSG.Interop.Incredibuild;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using Content = RSG.Pipeline.Content;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor.Common;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.Platform;

namespace RSG.Pipeline.Processor.Platform
{
    /// <summary>
    /// RAGE resource platform conversion processor.  This is the processor
    /// that invokes Ragebuilder and handles its dependency pass.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class ManifestConvert :
        RagebuilderBaseProcessor,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Ragebuilder Manifest Conversion Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Manifest Convert";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ManifestConvert()
            : base(DESCRIPTION)
        {

        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            bool result = true;
            this.LoadParameters(param);

            ISet<IContentNode> dependencies = new HashSet<IContentNode>();
            ICollection<IProcess> processes = new List<IProcess>();

            process.State = ProcessState.Prebuilt;
            resultantProcesses = processes;
            syncDependencies = dependencies;

            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
           out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Add tools, sort processes and create packets per-target.
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            IDictionary<ITarget, XGE.ITool> targetTools = new Dictionary<ITarget, XGE.ITool>();
            IDictionary<ITarget, XGE.ITaskJobGroup> targetGroups = new Dictionary<ITarget, XGE.ITaskJobGroup>();
            ICollection<RagePacket> packets = new List<RagePacket>();

            foreach (ITarget target in enabledTargets)
            {
                List<IProcess> targetProcesses = new List<IProcess>();
                foreach (IProcess process in processes)
                {
                    IContentNode input = process.Inputs.First();
                    IContentNode output = process.Outputs.First();
                    if ((input is Content.File) && (output is Content.Asset))
                    {
                        if (((Asset)output).Platform.Equals(target.Platform))
                        {
                            targetProcesses.Add(process);
                        }
                    }
                    else
                    {
                        param.Log.WarningCtx(LOG_CTX, "Unsupported conversion type: {0} => {1}.",
                            input.GetType().ToString(), output.GetType().ToString());
                    }
                }

                Ragebuilder.CreateXGEPacketsForRagebuilderProcesses(param, this, targetProcesses, target,
                    packets);

                // Add tools.
                String taskGroupName = String.Format("Manifest Convert [{0}]", target.Platform);
                bool allow_remote = GetXGEAllowRemote(param, true);
                XGE.ITool tool = XGEFactory.GetRagebuilderTool(target, allow_remote);
                targetTools.Add(target, tool);
                targetGroups.Add(target, new XGE.TaskGroup(taskGroupName));
            }

            param.Log.MessageCtx(LOG_CTX, "Creating {0} XGE packets for manifest conversion processor.", packets.Count);
            bool result = true;
            foreach (RagePacket packet in packets)
            {
                XGE.ITask task = Ragebuilder.GetXgeConvertTask(param.Log,
                    packet.Target, packet.Processes, targetTools[packet.Target]);
                targetGroups[packet.Target].Tasks.Add(task);
            }

            // Assign method output.
            tools = targetTools.Values;
            tasks = targetGroups.Values;

            return (result);
        }
        #endregion // IProcessor Interface Methods
    }
}
