﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Processor.Animation.Networks.Resources;
using RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Animation.Networks
{
    /// <summary>
    /// Handles conversion of MoVE .mtf files to .imvf files.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class MakeNetworkProccessor : 
        Common.BaseAnimationProcessor,
        IProcessor
    {
        #region Fields
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly string DESCRIPTION =
            "Network processor for creating a .mrt file from a .ivmf";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly string LOG_CTX = "Animation:MakeNetworkProccessor";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="T:MakeNetworkProccessor"/> class.
        /// </summary>
        public MakeNetworkProccessor()
            : base(DESCRIPTION)
        {
        }
        #endregion

        #region IProcessor Methods
        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a set of IProcess
        /// objects after determining all required inputs, outputs and whether the process'
        /// needs to change (this allows the concept of 'preprocessors').
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to prepare.
        /// </param>
        /// <param name="processors">
        /// Processor collection to reference for new resultant proceses.
        /// </param>
        /// <param name="owner">
        /// Owning IContentTree.
        /// </param>
        /// <param name="syncDependencies">
        /// Dependencies to sync.
        /// </param>
        /// <param name="resultantProcesses">
        /// RawProcesses that will actually be built.
        /// </param>
        /// <returns>
        /// True if successfully prebuilt; otherwise, false.
        /// </returns>
        public override bool Prebuild(
            IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Validate inputs.
            if (!this.ValidateInputCount(param, process) || !this.ValidateInputType(param, process))
            {
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return false;
            }

            // Validate outputs.
            if (!this.ValidateOutputCount(param, process) || !this.ValidateOutputType(param, process))
            {
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return false;
            }

            this.LoadParameters(param);
            syncDependencies = new List<IContentNode>();
            resultantProcesses = Enumerable.Repeat(process, 1);

            process.State = ProcessState.Prebuilt;
            return true;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with the tools,
        /// environments and tasks required.  Including setting up the task dependency chain.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="processes">
        /// </param>
        /// <param name="tools">
        /// </param>
        /// <param name="tasks">
        /// </param>
        /// <returns>
        /// </returns>
        public override bool Prepare(
            IEngineParameters param,
            IEnumerable<IProcess> processes,
            out IEnumerable<ITool> tools,
            out IEnumerable<ITask> tasks)
        {
            List<ITool> prepTools = new List<ITool>();
            List<ITask> prepTasks = new List<ITask>();

            try
            {
                object executable;
                if (!this.Parameters.TryGetValue("MakeNetworkExe", out executable))
                {
                    param.Log.ErrorCtx(LOG_CTX, StringTable.MissingExeParameter);
                    tools = new List<ITool>();
                    tasks = new List<ITask>();
                    return false;
                }

                string executablePath = param.Branch.Environment.Subst(executable.ToString());
                Tool tool = new Tool(StringTable.ToolName, executablePath, string.Empty);
                tool.AllowRemote = true;
                prepTools.Add(tool);

                foreach (IProcess process in processes)
                {
                    string input = (process.Inputs.First() as Content.File).AbsolutePath;
                    string name = System.IO.Path.GetFileNameWithoutExtension(input);
                    string taskName = string.Format(StringTable.TaskName, name);
                    string safeTaskName = taskName.Replace(' ', '_');
                    ITaskJob task = new TaskJob(safeTaskName);
                    task.Caption = taskName;
                    task.Tool = tool;

                    // Define the XGE task for the IProcess.
                    process.Parameters.Add(Constants.ProcessXGE_TaskName, safeTaskName);
                    process.Parameters.Add(Constants.ProcessXGE_Task, task);
                    
                    string output = (process.Outputs.First() as Content.File).AbsolutePath;
                    task.Parameters = string.Format(StringTable.Args, input, output);

                    prepTasks.Add(task);
                }
            }
            catch (System.Exception ex)
            {
                param.Log.ToolException(ex, StringTable.ExceptionPrepare);
            }

            tools = prepTools;
            tasks = prepTasks;

            return true;
        }
        #endregion // IProcessor Methods
        
        #region Private Methods
        /// <summary>
        /// Validates the prebuild input values to make sure that the specified process only
        /// has one input parameter and returns a value indicating the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateInputCount(IEngineParameters param, IProcess process)
        {
            int inputCount = process.Inputs.Count();
            if (inputCount == 1)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidInputCount, inputCount);
            Debug.Assert(inputCount == 1, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }

        /// <summary>
        /// Validates the prebuild input values to make sure that the first input value is
        /// of type <see cref="T:Content.Directory"/> and returns a value indicating
        /// the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateInputType(IEngineParameters param, IProcess process)
        {
            IContentNode input = process.Inputs.FirstOrDefault();
            Content.File file = input as Content.File;
            if (file != null)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidInputType, input.GetType().Name);
            Debug.Assert(file != null, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }

        /// <summary>
        /// Validates the prebuild output values to make sure that the specified process only
        /// has one output parameter and returns a value indicating the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateOutputCount(IEngineParameters param, IProcess process)
        {
            int outputCount = process.Outputs.Count();
            if (outputCount == 1)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidOutputCount, outputCount);
            Debug.Assert(outputCount == 1, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }

        /// <summary>
        /// Validates the prebuild input values to make sure that the first output value is
        /// of type <see cref="T:Content.Asset"/> and returns a value indicating
        /// the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateOutputType(IEngineParameters param, IProcess process)
        {
            IContentNode output = process.Outputs.FirstOrDefault();
            Content.File file = output as Content.File;
            if (file != null)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidOutputType, output.GetType().Name);
            Debug.Assert(file != null, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }
        #endregion // Private Methods
    } // RSG.Pipeline.Processor.Animation.Networks.NetworkProcessor {Class}
} // RSG.Pipeline.Processor.Animation.Networks {Namespace}
