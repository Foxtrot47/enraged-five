﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Processor.Animation.Networks.Resources;

namespace RSG.Pipeline.Processor.Animation.Networks
{
    /// <summary>
    /// Handles conversion of MoVE .mtf files throught the maknetwork execuatable to being
    /// built in the networkdefs.rpf file.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class NetworkPreProcess : 
        Common.BaseAnimationProcessor,
        IProcessor
    {
        #region Fields
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly string DESCRIPTION =
            "Network pre-processor for a directory of .imvf files to a single .rpf file.";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly string LOG_CTX = "Animation:NetworkPreProcess";
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="T:NetworkPreProcess"/> class.
        /// </summary>
        public NetworkPreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion

        #region IProcessor Methods
        /// <summary>
        /// Prepare content; first pass of the on-disk content-tree.  Returns a set of IProcess
        /// objects after determining all required inputrs, outputs and whether the pocess'
        /// needs to change (this allows the concept of 'preprocessors').
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to prepare.
        /// </param>
        /// <param name="processors">
        /// Processor collection to reference for new resultant proceses.
        /// </param>
        /// <param name="owner">
        /// Owning IContentTree.
        /// </param>
        /// <param name="syncDependencies">
        /// Dependencies to sync.
        /// </param>
        /// <param name="resultantProcesses">
        /// RawProcesses that will actually be built.
        /// </param>
        /// <returns>
        /// True if successfully prebuilt; otherwise, false.
        /// </returns>
        public override bool Prebuild(
            IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Validate inputs.
            if (!this.ValidateInputCount(param, process) || !this.ValidateInputType(param, process))
            {
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return false;
            }

            // Validate outputs.
            if (!this.ValidateOutputCount(param, process) || !this.ValidateOutputType(param, process))
            {
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return false;
            }

            Content.Directory networkDirectory = process.Inputs.First() as Content.Directory;
            IEnumerable<IContentNode> networkInputs = networkDirectory.EvaluateInputs();

            ICollection<IProcess> processes = new List<IProcess>();
            IProcessor makeNetworkProcess =
                processors.FirstOrDefault(p => p is MakeNetworkProccessor);
            if (makeNetworkProcess == null)
            {
                param.Log.ErrorCtx(LOG_CTX, StringTable.MissingProcessExport);
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return false;
            }

            List<IContentNode> outputNodes = new List<IContentNode>();
            foreach (IContentNode node in networkInputs)
            {
                Content.File network = node as Content.File;
                if (network == null)
                {
                    continue;
                }

                string outputPath = network.AbsolutePath;
                if (param.Flags.HasFlag(EngineFlags.Preview))
                {
                    string filename = System.IO.Path.GetFileName(outputPath);
                    outputPath = System.IO.Path.Combine(param.Branch.Project.Config.CoreProject.Branches[param.Branch.Name].Preview, filename);
                }
                else
                {
                    outputPath = outputPath.Replace(param.Branch.Export, param.Branch.Processed);
                }

                outputPath = System.IO.Path.ChangeExtension(outputPath, ".mrf");

                // Fix needed since makenetwork crashes if the folder doesn't exist.
                string outputDirectory = System.IO.Path.GetDirectoryName(outputPath);
                if (!System.IO.Directory.Exists(outputDirectory))
                {
                    System.IO.Directory.CreateDirectory(outputDirectory);
                }


                String makeNetworkFilename = System.IO.Path.Combine("$(toolsbin)", "MoVE", "tool", "makenetwork.exe");
                IContentNode makeNetworkNode = owner.CreateFile(param.Branch.Environment.Subst(makeNetworkFilename));
                IContentNode outputNode = owner.CreateFile(outputPath);
                outputNodes.Add(outputNode);
                ProcessBuilder pb = new ProcessBuilder(makeNetworkProcess, owner);
                pb.Inputs.Add(network);
                pb.Outputs.Add(outputNode);
                pb.AdditionalDependentContent.Add(makeNetworkNode);
                processes.Add(pb.ToProcess());
            }

            if (!param.Flags.HasFlag(EngineFlags.Preview))
            {
                ProcessBuilder rpfCreate = new ProcessBuilder(
                    StringTable.RpfCreateProcess, processors, owner);
                rpfCreate.Outputs.Add(process.Outputs.First());
                rpfCreate.Inputs.AddRange(outputNodes);
                processes.Add(rpfCreate.ToProcess());
            }

            resultantProcesses = processes;
            process.State = ProcessState.Discard;
            syncDependencies = new List<IContentNode>() { networkDirectory };
            return true;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with the tools,
        /// environments and tasks required.  Including setting up the task dependency chain.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="processes">
        /// </param>
        /// <param name="tools">
        /// </param>
        /// <param name="tasks">
        /// </param>
        /// <returns>
        /// </returns>
        public override bool Prepare(
            IEngineParameters param,
            IEnumerable<IProcess> processes,
            out IEnumerable<RSG.Interop.Incredibuild.XGE.ITool> tools,
            out IEnumerable<RSG.Interop.Incredibuild.XGE.ITask> tasks)
        {
            throw new NotImplementedException();
        }
        #endregion // IProcessor Methods

        #region Private Methods
        /// <summary>
        /// Validates the prebuild input values to make sure that the specified process only
        /// has one input parameter and returns a value indicating the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateInputCount(IEngineParameters param, IProcess process)
        {
            int inputCount = process.Inputs.Count();
            if (inputCount == 1)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidInputCount, inputCount);
            Debug.Assert(inputCount == 1, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }

        /// <summary>
        /// Validates the prebuild input values to make sure that the first input value is
        /// of type <see cref="T:Content.Directory"/> and returns a value indicating
        /// the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateInputType(IEngineParameters param, IProcess process)
        {
            IContentNode input = process.Inputs.FirstOrDefault();
            Content.Directory directory = input as Content.Directory;
            if (directory != null)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidInputType, input.GetType().Name);
            Debug.Assert(directory != null, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }

        /// <summary>
        /// Validates the prebuild output values to make sure that the specified process only
        /// has one output parameter and returns a value indicating the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateOutputCount(IEngineParameters param, IProcess process)
        {
            int outputCount = process.Outputs.Count();
            if (outputCount == 1)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidOutputCount, outputCount);
            Debug.Assert(outputCount == 1, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }

        /// <summary>
        /// Validates the prebuild input values to make sure that the first output value is
        /// of type <see cref="T:Content.Asset"/> and returns a value indicating
        /// the outcome.
        /// </summary>
        /// <param name="param">
        /// Engine build parameters.
        /// </param>
        /// <param name="process">
        /// Process to validate.
        /// </param>
        /// <returns>
        /// True if the validation is past successfully; otherwise, false.
        /// </returns>
        private bool ValidateOutputType(IEngineParameters param, IProcess process)
        {
            IContentNode output = process.Outputs.FirstOrDefault();
            Content.Asset asset = output as Content.Asset;
            if (asset != null)
            {
                return true;
            }

            string msg = string.Format(StringTable.InvalidOutputType, output.GetType().Name);
            Debug.Assert(asset != null, msg);
            param.Log.ErrorCtx(LOG_CTX, msg);
            return false;
        }
        #endregion // Private Methods
    } // RSG.Pipeline.Processor.Animation.Networks.NetworkProcessor {Class}
} // RSG.Pipeline.Processor.Animation.Networks {Namespace}
