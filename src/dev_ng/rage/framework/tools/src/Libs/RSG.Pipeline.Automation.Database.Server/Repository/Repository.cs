﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using RSG.Pipeline.Automation.Database.Domain;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.Repository
{
    /// <summary>
    /// NHibernate repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Repository<T> : IRepository<T> where T : class, IDBEntity
    {
        #region Member Data
        /// <summary>
        /// NHibernate session
        /// </summary>
        protected readonly ISession Session;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        public Repository(ISession session)
        {
            Session = session;
        }
        #endregion // Constructor(s)

        #region IRepository<T> Implementation
        /// <summary>
        /// Saves the given entity
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public T Save(T instance)
        {
            Session.Save(instance);
            return instance;
        }

        /// <summary>
        /// Updates the given entity
        /// </summary>
        /// <param name="instance"></param>
        public void Update(T instance)
        {
            Session.Update(instance);
        }

        /// <summary>
        /// Save or updates the given entity
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public void SaveOrUpdate(T instance)
        {
            Session.SaveOrUpdate(instance);

        }

        /// <summary>
        /// Deletes the given entity
        /// </summary>
        /// <param name="instance"></param>
        public void Delete(T instance)
        {
            Session.Delete(instance);
        }

        /// <summary>
        /// Retrieves the entity with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>the entity or null if it doesn't exist</returns>
        public T GetById(long id)
        {
            return Session.Get<T>(id);
        }

        /// <summary>
        /// Returns the one entity that matches the given criteria. Throws an exception if
        /// more than one entity matches the criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public T FindOne(DetachedCriteria criteria)
        {
            return criteria.GetExecutableCriteria(Session).UniqueResult<T>();
        }

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public T FindFirst(DetachedCriteria criteria)
        {
            IList<T> results = criteria.SetFirstResult(0).SetMaxResults(1).GetExecutableCriteria(Session).List<T>();

            if (results.Count > 0)
            {
                return results[0];
            }

            return default(T);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria, ordered by the given order
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public T FindFirst(DetachedCriteria criteria, Order order)
        {
            return FindFirst(criteria.AddOrder(order));
        }

        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> FindAll()
        {
            return Session.Query<T>();
        }

        /// <summary>
        /// Creates a criteria for this domain type.
        /// </summary>
        public ICriteria CreateCriteria(String alias = null)
        {
            if (!String.IsNullOrEmpty(alias))
            {
                return Session.CreateCriteria<T>(alias);
            }
            else
            {
                return Session.CreateCriteria<T>();
            }
        }

        /// <summary>
        /// Returns each entity that matches the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public IList<TResult> FindAll<TResult>(DetachedCriteria criteria)
        {
            return criteria.GetExecutableCriteria(Session).List<TResult>();
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        public IList<TResult> FindAll<TResult>(DetachedCriteria criteria, params Order[] orders)
        {
            if (orders != null)
            {
                foreach (Order order in orders)
                {
                    criteria.AddOrder(order);
                }
            }

            return FindAll<TResult>(criteria);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="firstResult"></param>
        /// <param name="numberOfResults"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        public IList<TResult> FindAll<TResult>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders)
        {
            criteria.SetFirstResult(firstResult).SetMaxResults(numberOfResults);
            return FindAll<TResult>(criteria, orders);
        }

        /// <summary>
        /// Returns the total number of entities that match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public long Count(DetachedCriteria criteria)
        {
            return Convert.ToInt64(criteria.GetExecutableCriteria(Session).SetProjection(Projections.RowCountInt64()).UniqueResult());
        }
        #endregion // IRepository<T> Implementation
    } // Repository
}
