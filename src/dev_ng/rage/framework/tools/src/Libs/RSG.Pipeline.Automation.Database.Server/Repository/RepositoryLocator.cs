﻿using NHibernate;
using NHibernate.Criterion;
using RSG.Pipeline.Automation.Database.Domain;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.Repository
{
    /// <summary>
    /// NHibernate repository locator
    /// </summary>
    public class RepositoryLocator : IRepositoryLocator
    {
        #region Properties
        /// <summary>
        /// NHibernate session.
        /// NOTE: This shouldn't be publicly exposed!
        /// </summary>
        public ISession Session
        {
            get
            {
                return m_session;
            }
        }
        private readonly ISession m_session;

        /// <summary>
        /// Lookup for repositories based on a db object type.
        /// </summary>
        private IDictionary<Type, object> RepositoryMap { get; set; }

        /// <summary>
        /// Map of db object type to repository type.
        /// </summary>
        private static IDictionary<Type, Type> RepositoryTypeMap { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RepositoryLocator(ISession session)
        {
            m_session = session;
            RepositoryMap = new Dictionary<Type, object>();
        }

        /// <summary>
        /// 
        /// </summary>
        static RepositoryLocator()
        {
            RepositoryTypeMap = new Dictionary<Type, Type>();

            //RepositoryTypeMap[typeof(ToolsVersions)] = typeof(LabelRepository<ToolsVersions>);


        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        protected IRepository<T> CreateRepository<T>() where T : class, IDBEntity
        {
            // Check whether we need to create a specific repository for this type.
            if (RepositoryTypeMap.ContainsKey(typeof(T)))
            {
                Type repoType = RepositoryTypeMap[typeof(T)];
                Debug.Assert(repoType.GetInterfaces().Contains(typeof(IRepository<T>)), "Non-repository type added for db object type.");
                return (IRepository<T>)Activator.CreateInstance(repoType, Session);
            }

            return new Repository<T>(Session);
        }
        #endregion // Protected Methods

        #region IRepositoryLocator Members
        /// <summary>
        /// Saves the given entity in the appropriate repository
        /// </summary>
        public T Save<T>(T instance) where T : class, IDBEntity
        {
            return GetRepository<T>().Save(instance);
        }

        /// <summary>
        /// Updates the given entity in the appropriate repository
        /// </summary>
        public void Update<T>(T instance) where T : class, IDBEntity
        {
            GetRepository<T>().Update(instance);
        }

        /// <summary>
        /// Saves or updates the given entity in the appropriate repository
        /// </summary>
        public void SaveOrUpdate<T>(T instance) where T : class, IDBEntity
        {
            GetRepository<T>().SaveOrUpdate(instance);
        }

        /// <summary>
        /// Deletes the given entity from the appropriate repository
        /// </summary>
        public void Delete<T>(T instance) where T : class, IDBEntity
        {
            GetRepository<T>().Delete(instance);
        }

        /// <summary>
        /// Retrieves the entity with the given id from the appropriate repository
        /// </summary>
        public T GetById<T>(long id) where T : class, IDBEntity
        {
            return GetRepository<T>().GetById(id);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        public T FindOne<T>(DetachedCriteria criteria) where T : class, IDBEntity
        {
            return GetRepository<T>().FindOne(criteria);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        public T FindFirst<T>(DetachedCriteria criteria) where T : class, IDBEntity
        {
            return GetRepository<T>().FindFirst(criteria);
        }

        /// <summary>
        /// Returns the first entity to match the given criteria, ordered by the given order
        /// </summary>
        public T FindFirst<T>(DetachedCriteria criteria, Order order) where T : class, IDBEntity
        {
            return GetRepository<T>().FindFirst(criteria, order);
        }

        /// <summary>
        /// Returns all entities from the appropriate repository
        /// </summary>
        public IQueryable<T> FindAll<T>() where T : class, IDBEntity
        {
            return GetRepository<T>().FindAll();
        }

        /// <summary>
        /// Creates a criteria for the appropriate type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public ICriteria CreateCriteria<T>(String alias = null) where T : class, IDBEntity
        {
            return GetRepository<T>().CreateCriteria(alias);
        }

        /// <summary>
        /// Returns a repository for the specific type.
        /// </summary>
        public IRepository<T> GetRepository<T>() where T : class, IDBEntity
        {
            // Try and find the repository in our map
            Type type = typeof(T);
            if (RepositoryMap.Keys.Contains(type))
            {
                return RepositoryMap[type] as IRepository<T>;
            }

            IRepository<T> repository = CreateRepository<T>();
            RepositoryMap.Add(type, repository);
            return repository;
        }

        /// <summary>
        /// Returns each entity that matches the given criteria
        /// </summary>
        public IList<T> FindAll<T>(DetachedCriteria criteria) where T : class, IDBEntity
        {
            return GetRepository<T>().FindAll<T>(criteria);
        }

        /// <summary>
        /// 
        /// </summary>
        public IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria) where TEntity : class, IDBEntity
        {
            return GetRepository<TEntity>().FindAll<TResult>(criteria);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        public IList<T> FindAll<T>(DetachedCriteria criteria, params Order[] orders) where T : class, IDBEntity
        {
            return GetRepository<T>().FindAll<T>(criteria, orders);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        public IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria, params Order[] orders) where TEntity : class, IDBEntity
        {
            return GetRepository<TEntity>().FindAll<TResult>(criteria, orders);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        public IList<T> FindAll<T>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders) where T : class, IDBEntity
        {
            return GetRepository<T>().FindAll<T>(criteria, firstResult, numberOfResults, orders);
        }

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        public IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders) where TEntity : class, IDBEntity
        {
            return GetRepository<TEntity>().FindAll<TResult>(criteria, firstResult, numberOfResults, orders);
        }

        /// <summary>
        /// Returns the total number of entities that match the given criteria
        /// </summary>
        public long Count<T>(DetachedCriteria criteria) where T : class, IDBEntity
        {
            return GetRepository<T>().Count(criteria);
        }
        #endregion // IRepositoryLocator Members

        #region Unmanaged Queries
        /// <summary>
        /// Creates an unmanaged query
        /// </summary>
        public IQuery CreateUnmanagedHQLQuery(string sql)
        {
            return Session.CreateQuery(sql);
        }

        /// <summary>
        /// Creates an unmanaged query
        /// </summary>
        public ISQLQuery CreateUnmanagedSQLQuery(string sql)
        {
            return Session.CreateSQLQuery(sql);
        }
        #endregion // Unmanaged Queries
    } // RepositoryLocator
}
