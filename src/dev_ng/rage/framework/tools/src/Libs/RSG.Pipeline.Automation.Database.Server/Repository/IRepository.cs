﻿using NHibernate;
using NHibernate.Criterion;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class, IDBEntity
    {
        #region Basic Methods
        /// <summary>
        /// Saves the given entity
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        T Save(T instance);

        /// <summary>
        /// Updates the given entity
        /// </summary>
        /// <param name="instance"></param>
        void Update(T instance);

        /// <summary>
        /// Save or updates the given entity
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        void SaveOrUpdate(T instance);

        /// <summary>
        /// Deletes the given entity
        /// </summary>
        /// <param name="instance"></param>
        void Delete(T instance);

        /// <summary>
        /// Retrieves the entity with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>the entity or null if it doesn't exist</returns>
        T GetById(long id);

        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns></returns>
        IQueryable<T> FindAll();

        /// <summary>
        /// Creates a criteria for this domain type.
        /// </summary>
        ICriteria CreateCriteria(String alias = null);
        #endregion // Basic Methods

        #region Criteria Based Methods
        /// <summary>
        /// Returns the one entity that matches the given criteria. Throws an exception if
        /// more than one entity matches the criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        T FindOne(DetachedCriteria criteria);

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        T FindFirst(DetachedCriteria criteria);

        /// <summary>
        /// Returns the first entity to match the given criteria, ordered by the given order
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        T FindFirst(DetachedCriteria criteria, Order order);

        /// <summary>
        /// Returns each entity that matches the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        IList<TResult> FindAll<TResult>(DetachedCriteria criteria);

        /// <summary>
        /// Returns each entity that maches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        IList<TResult> FindAll<TResult>(DetachedCriteria criteria, params Order[] orders);

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="firstResult"></param>
        /// <param name="numberOfResults"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        IList<TResult> FindAll<TResult>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders);

        /// <summary>
        /// Returns the total number of entities that match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        long Count(DetachedCriteria criteria);
        #endregion // Criteria Based Methods
    } // IRepository<T>
}
