﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.TransactionManager
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITransactionManagerFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ITransactionManager CreateManager(bool readOnly);
    } // ITransManagerFactory
}
