﻿using NHibernate;
using NHibernate.Criterion;
using RSG.Pipeline.Automation.Database.Domain;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRepositoryLocator
    {
        #region Properties
        /// <summary>
        /// NHibernate session
        /// </summary>
        ISession Session { get; }
        #endregion // Properties

        #region Basic Methods
        /// <summary>
        /// Saves the given entity in the appropriate repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        /// <returns></returns>
        T Save<T>(T instance) where T : class, IDBEntity;

        /// <summary>
        /// Updates the given entity in the appropriate repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        void Update<T>(T instance) where T : class, IDBEntity;

        /// <summary>
        /// Saves or updates the given entity in the appropriate repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        void SaveOrUpdate<T>(T instance) where T : class, IDBEntity;

        /// <summary>
        /// Deletes the given entity from the appropriate repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="instance"></param>
        void Delete<T>(T instance) where T : class, IDBEntity;

        /// <summary>
        /// Retrieves the entity with the given id from the appropriate repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById<T>(long id) where T : class, IDBEntity;

        /// <summary>
        /// Returns all entities from the appropriate repository
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IQueryable<T> FindAll<T>() where T : class, IDBEntity;

        /// <summary>
        /// Creates a criteria for the appropriate type.
        /// </summary>
        ICriteria CreateCriteria<T>(String alias = null) where T : class, IDBEntity;

        /// <summary>
        /// Returns a repository for the specific type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IRepository<T> GetRepository<T>() where T : class, IDBEntity;
        #endregion // Basic Methods

        #region Criteria Based Methods
        /// <summary>
        /// Returns the one entity that matches the given criteria. Throws an exception if
        /// more than one entity matches the criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criteria"></param>
        /// <returns></returns>
        T FindOne<T>(DetachedCriteria criteria) where T : class, IDBEntity;

        /// <summary>
        /// Returns the first entity to match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        T FindFirst<T>(DetachedCriteria criteria) where T : class, IDBEntity;

        /// <summary>
        /// Returns the first entity to match the given criteria, ordered by the given order
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        T FindFirst<T>(DetachedCriteria criteria, Order order) where T : class, IDBEntity;

        /// <summary>
        /// Returns each entity that matches the given criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="criteria"></param>
        /// <returns></returns>
        IList<T> FindAll<T>(DetachedCriteria criteria) where T : class, IDBEntity;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="criteria"></param>
        /// <returns></returns>
        IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria) where TEntity : class, IDBEntity;

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        IList<T> FindAll<T>(DetachedCriteria criteria, params Order[] orders) where T : class, IDBEntity;

        /// <summary>
        /// Returns each entity that matches the given criteria, and orders the results
        /// according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria, params Order[] orders) where TEntity : class, IDBEntity;

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="firstResult"></param>
        /// <param name="numberOfResults"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        IList<T> FindAll<T>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders) where T : class, IDBEntity;

        /// <summary>
        /// Returns each entity that matches the given criteria, with support for paging,
        /// and orders the results according to the given Orders
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="firstResult"></param>
        /// <param name="numberOfResults"></param>
        /// <param name="orders"></param>
        /// <returns></returns>
        IList<TResult> FindAll<TEntity, TResult>(DetachedCriteria criteria, int firstResult, int numberOfResults, params Order[] orders) where TEntity : class, IDBEntity;

        /// <summary>
        /// Returns the total number of entities that match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        long Count<T>(DetachedCriteria criteria) where T : class, IDBEntity;
        #endregion // Criteria Based Methods

        #region Unmanaged Query
        /// <summary>
        /// Creates an unmanaged HQL query.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        IQuery CreateUnmanagedHQLQuery(string sql);

        /// <summary>
        /// Creates an unmanaged SQL query.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        ISQLQuery CreateUnmanagedSQLQuery(string sql);
        #endregion // Raw SQL Query
    } // IRepositoryLocator
}
