﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using RSG.Base.Configuration.Services;
using RSG.Pipeline.Automation.Database.Domain.Config;
using RSG.Pipeline.Automation.Database.Server.TransactionManager;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Database.Server.Context
{
    /// <summary>
    /// Global context that is shared by the whole server.
    /// TODO: I dislike the purpose of this class.  We should be initialising nhibernate
    /// in either the console host or the service host factory and passing the transaction
    /// manager into the service's themselves.
    /// </summary>
    public class GlobalContext : IGlobalContext
    {
        #region Fields
        /// <summary>
        /// Singleton instance.
        /// </summary>
        private static IGlobalContext _instance;

        /// <summary>
        /// NHibernate configuration object.
        /// </summary>
        private static INHibernateConfig _nhibernateConfig;

        /// <summary>
        /// Private field for the <see cref="DomainConfig"/> property.
        /// </summary>
        private readonly IDomainConfig _domainConfig;

        /// <summary>
        /// Private field for the <see cref="TransManagerFactory"/> property.
        /// </summary>
        private readonly ITransactionManagerFactory _transManagerFactory;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Singleton instance of the global context.
        /// </summary>
        public static IGlobalContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GlobalContext();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Domain config object.
        /// </summary>
        public IDomainConfig DomainConfig
        {
            get { return _domainConfig; }
        }

        /// <summary>
        /// Transaction manager factory.
        /// </summary>
        public ITransactionManagerFactory TransManagerFactory
        {
            get { return _transManagerFactory; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        private GlobalContext()
        {
            Debug.Assert(_nhibernateConfig != null, "Trying to create a GlobalContext object when the class hasn't yet been initialised with the nhibernate config object.");
            if (_nhibernateConfig == null)
            {
                throw new ArgumentNullException("Trying to create a GlobalContext object when the class hasn't yet been initialised with the nhibernate config object.");
            }
            _domainConfig = new DomainConfig(LogFactory.ApplicationLog, _nhibernateConfig);

            if (_domainConfig == null)
            {
                throw new ArgumentNullException("Trying to create a DomainConfig object when the class hasn't yet been initialised ");
            }

            _transManagerFactory = new TransManagerFactory(_domainConfig, _nhibernateConfig.ReadOnly);
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        public static void Initialise(INHibernateConfig config)
        {
            _nhibernateConfig = config;
        }
        #endregion // Static Methods
    } // GlobalContext
}
