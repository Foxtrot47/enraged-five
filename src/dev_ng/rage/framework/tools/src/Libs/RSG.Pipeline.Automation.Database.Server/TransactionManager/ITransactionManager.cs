﻿using RSG.Pipeline.Automation.Database.Common.Async;
using RSG.Pipeline.Automation.Database.Server.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.TransactionManager
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITransactionManager : IDisposable
    {
        #region Properties
        /// <summary>
        /// Results associated with an asynchronous task.
        /// </summary>
        IAsyncCommandResult AsyncTaskResults { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Execute an action that doesn't require a return value.
        /// </summary>
        /// <param name="command"></param>
        void ExecuteCommand(Action<Repository.IRepositoryLocator> command);

        /// <summary>
        /// Exceute an action that returns a result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        T ExecuteCommand<T>(Func<IRepositoryLocator, T> command);

        /// <summary>
        /// Execute an action asynchronously.
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        IAsyncCommandResult ExecuteAsyncCommand<T>(Func<Repository.IRepositoryLocator, IAsyncCommandResult, T> command);
        #endregion // Methods
    } // ITransManager
}
