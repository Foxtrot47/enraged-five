﻿using RSG.Base.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Common.Services;
using RSG.Base.Configuration.Services;
using RSG.Pipeline.Automation.Database.Server.Repository;
using RSG.Pipeline.Automation.Database.Server.Context;
using RSG.Pipeline.Automation.Database.Server.TransactionManager;

namespace RSG.Pipeline.Automation.Database.Server.Services
{
    /// <summary>
    /// Base class from which all services that interact with a database should inherit from.
    /// </summary>
    public abstract class ServiceBase : ConfigAwareService
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ServiceBase(IServiceHostConfig config)
            : base(config)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Executes a synchronously command through the transaction manager
        /// </summary>
        /// <param name="command"></param>
        protected void ExecuteCommand(Action<IRepositoryLocator> command, bool readOnly = false)
        {
            using (ITransactionManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(readOnly))
            {
                manager.ExecuteCommand(command);
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        protected void ExecuteReadOnlyCommand(Action<IRepositoryLocator> command)
        {
            ExecuteCommand(command, true);
        }

        /// <summary>
        /// Executes a command synchronously through the transaction manager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        protected T ExecuteCommand<T>(Func<IRepositoryLocator, T> command, bool readOnly = false)
        {
            using (ITransactionManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(readOnly))
            {
                return manager.ExecuteCommand(command);
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        protected T ExecuteReadOnlyCommand<T>(Func<IRepositoryLocator, T> command)
        {
            return ExecuteCommand(command, true);
        }
        #endregion
    } // ServiceBase
}
