﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Deployment.Server.ExceptionHandling
{
    public class GlobalExceptionHandler : IErrorHandler
    {
        #region IErrorHandler Members

        public bool HandleError(Exception ex)
        {
            return true;
        }

        /// <summary>
        /// Handles exceptions inside services
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="version"></param>
        /// <param name="msg"></param>
        public void ProvideFault(Exception ex, MessageVersion version, ref Message msg)
        {
            // Do some logging here

            var newEx = new FaultException(
                string.Format("Exception occured calling {0} \n\r Error: {1}", ex.TargetSite.Name, ex.Message));

            MessageFault msgFault = newEx.CreateMessageFault();
            msg = Message.CreateMessage(version, msgFault, newEx.Action);
        }

        #endregion
    }
}
