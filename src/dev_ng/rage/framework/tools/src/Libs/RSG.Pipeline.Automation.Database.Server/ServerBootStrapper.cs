﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Diagnostics;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Configuration.Services;
using RSG.Pipeline.Automation.Database.Server.Context;
using RSG.Pipeline.Automation.Database.Domain.Config;

namespace RSG.Pipeline.Automation.Database.Server
{
    /// <summary>
    /// Server Bootstrapper
    /// </summary>
    public static class ServerBootStrapper
    {
        #region Properties
        /// <summary>
        /// Flag indicating that the server bootstrapper has already run.
        /// </summary>
        private static bool _initialised;

        /// <summary>
        /// Log method for the server boot strapper to use.
        /// </summary>
        private static ILog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static ServerBootStrapper()
        {
            _log = LogFactory.CreateUniversalLog("Server boot strapper");
        }
        #endregion // Constructor(s)

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        public static void Initialise(INHibernateConfig config, bool updateSchema = false)
        {
            if (!_initialised)
            {
                GlobalContext.Initialise(config);
                IDomainConfig domainConfig = GlobalContext.Instance.DomainConfig;

                if (updateSchema)
                {
                    string connString = domainConfig.NHibernateConfig.GetProperty("connection.connection_string");
                    _log.Profile("Updating database schema.");
                    domainConfig.SchemaUpdate.Execute(SchemaUpdateScriptAction, true);
                    _log.ProfileEnd();
                    _log.Message("Database schema successfully updated.");
                }
                _initialised = true;
            }
        }
        #endregion // Static Methods

        #region Private Methods
        /// <summary>
        /// Callback for logging messages generated during the schema update.
        /// </summary>
        /// <param name="line"></param>
        private static void SchemaUpdateScriptAction(string line)
        {
            _log.Message(line);
        }
        #endregion // Private Methods
    } // BootStrapper
}
