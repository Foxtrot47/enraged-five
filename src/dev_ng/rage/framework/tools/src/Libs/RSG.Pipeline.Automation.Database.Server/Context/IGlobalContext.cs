﻿using RSG.Pipeline.Automation.Database.Domain.Config;
using RSG.Pipeline.Automation.Database.Server.TransactionManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Database.Server.Context
{
    /// <summary>
    /// Interface for getting at objects shared on the global level
    /// </summary>
    public interface IGlobalContext
    {
        /// <summary>
        /// Database config object.
        /// </summary>
        IDomainConfig DomainConfig { get; }

        /// <summary>
        /// Transaction manager factory.
        /// </summary>
        ITransactionManagerFactory TransManagerFactory { get; }

    } // IGlobalContext
}
