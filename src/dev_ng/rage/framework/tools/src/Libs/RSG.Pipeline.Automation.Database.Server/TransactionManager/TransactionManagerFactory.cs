﻿using NHibernate;
using RSG.Base.Logging;
using RSG.Pipeline.Automation.Database.Domain.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.Database.Server.TransactionManager
{
    /// <summary>
    /// NHibernate based transaction manager factory
    /// </summary>
    public class TransManagerFactory : ITransactionManagerFactory
    {
        #region Fields
        /// <summary>
        /// Factory for creating NHibernate sessions.
        /// </summary>
        private ISessionFactory _sessionFactory;

        /// <summary>
        /// Flag indicating whether we should be running in read-only mode.
        /// </summary>
        private bool _readOnly;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TransManagerFactory(IDomainConfig config, bool readOnlyMode)
        {
            _sessionFactory = config.NHibernateConfig.BuildSessionFactory();
            _readOnly = readOnlyMode;
        }
        #endregion // Constructor(s)

        #region ITransFactory Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ITransactionManager CreateManager(bool readOnly)
        {
            ISession session = _sessionFactory.OpenSession();
            session.DefaultReadOnly = _readOnly || readOnly;
            return new TransManager(LogFactory.ApplicationLog, session);
        }
        #endregion // ITransFactory Implementation
    } // TransManagerFactory
}
