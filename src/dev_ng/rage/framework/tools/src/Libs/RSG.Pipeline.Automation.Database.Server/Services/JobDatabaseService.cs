﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using RSG.Base.Configuration.Services;
using RSG.Pipeline.Automation.Database.Server.Services;
using RSG.Pipeline.Automation.Database.Common.ServiceContract;
using RSG.Pipeline.Automation.Database.Server.Repository;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.JobResults;
using RSG.Pipeline.Automation.Database.Domain.Entities.Stats;
using RSG.Pipeline.Automation.Database.Domain.Entities.Logs;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Database.Server.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class JobDatabaseService : ServiceBase, IJobDatabaseService
    {
        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        public JobDatabaseService(IServiceHostConfig config)
            : base(config)
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Endpoint for getting all jobs. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        public IList<JobModel> GetAllJobs(String server = "", int limit = -1, int offset = -1)
        {
            return ExecuteCommand(locator => GetAllJobsCommand(locator, server, limit, offset, default(DateTime), default(DateTime)));
        }

        /// <summary>
        /// Endpoint for getting all jobs in a date range. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        public IList<JobModel> GetAllJobsByDate(String from, String to, String server = "", int limit = -1, int offset = -1)
        {
            DateTime fromDate = DateTime.Parse(from);
            DateTime toDate = DateTime.Parse(to);
            return ExecuteCommand(locator => GetAllJobsCommand(locator, server, limit, offset, fromDate, toDate));
        }

        /// <summary>
        /// Endpoint for getting all jobs. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        public IList<JobModel> GetAllJobsByType(string typeStr, String server = "", int limit = -1, int offset = -1)
        {
            Type type = Type.GetType(typeStr);
            return ExecuteCommand(locator => GetAllJobsByTypeCommand(locator, type, server, limit, offset, default(DateTime), default(DateTime)));
        }

        /// <summary>
        /// Endpoint for getting all jobs in a date range. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        public IList<JobModel> GetAllJobsByDateAndType(string typeStr, String from, String to, String server = "", int limit = -1, int offset = -1)
        {
            DateTime fromDate = DateTime.Parse(from);
            DateTime toDate = DateTime.Parse(to);
            Type type = Type.GetType(typeStr);
            return ExecuteCommand(locator => GetAllJobsByTypeCommand(locator, type, server, limit, offset, fromDate, toDate));
        }

        /// <summary>
        /// Endpoint for getting all jobs for a single processing host. Limit and offset are optional parameters which allow us to paginate 
        /// the data on the portal. This will result in faster queries.
        /// </summary>
        /// <param name="session"></param>
        public IList<JobModel> GetAllJobsForProcessingHost(string host, int limit = -1, int offset = -1)
        {
            return ExecuteCommand(locator => GetAllJobsForProcessingHostCommand(locator, host, limit, offset, default(DateTime), default(DateTime)));
        }

        /// <summary>
        /// Endpoint for getting a job
        /// </summary>
        /// <param name="session"></param>
        public JobModel GetJob(String id)
        {
            return ExecuteCommand(locator => GetJobCommand(locator, id));
        }

        /// <summary>
        /// Endpoint for getting a job result
        /// </summary>
        /// <param name="session"></param>
        public JobResultModel GetJobResult(String id)
        {
            return ExecuteCommand(locator => GetJobResultCommand(locator, id));
        }

        /// <summary>
        /// Endpoint for getting all jobs
        /// </summary>
        /// <param name="session"></param>
        public bool CreateJob(JobModel job)
        {
            if (job != null)
            {
                ExecuteCommand(locator => CreateJobCommand(locator, job));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Endpoint for getting all jobs
        /// </summary>
        /// <param name="session"></param>
        public bool UpdateJob(JobModel job)
        {
            if (job != null)
            {
                ExecuteCommand(locator => UpdateJobCommand(locator, job));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Endpoint for getting all jobs
        /// </summary>
        /// <param name="session"></param>
        public bool CreateJobResult(JobResultModel job)
        {
            if (job != null)
            {
                ExecuteCommand(locator => CreateJobResultCommand(locator, job));
                return true;
            }

            return false;
        }

        /// <summary>
        /// Endpoint for getting all jobs
        /// </summary>
        /// <param name="session"></param>
        public bool UpdateJobResult(JobResultModel job)
        {
            if (job != null)
            {
                ExecuteCommand(locator => UpdateJobResultCommand(locator, job));
                return true;
            }

            return false;
        }
        /// <summary>
        /// Endpoint for getting all changes for a job
        /// </summary>
        /// <param name="session"></param>
        public IEnumerable<Change> GetAllChangesForJob(string id)
        {
            return ExecuteCommand(locator => GetAllChangesForJobCommand(locator, id));
        }

        public ServerSummary GetJobStats(String server, String typeStr)
        {
            Type type = Type.GetType(typeStr);

            return ExecuteCommand(locator => ServerSummaryCommand(locator, server, type));
        }

        /// <summary>
        /// Create log message entries in the database.
        /// </summary>
        /// <param name="session"></param>
        public bool CreateLogMessage(MessageModel logMessage)
        {
            return ExecuteCommand(locator => CreateLogMessage(locator, logMessage));
        }


        /// <summary>
        /// Create log message entries in the database.
        /// </summary>
        /// <param name="session"></param>
        public bool CreateLogMessageFromList(IEnumerable<MessageModel> logMessages)
        {
            foreach (MessageModel message in logMessages)
            {
                return ExecuteCommand(locator => CreateLogMessage(locator, message));
            }

            return true;
        }

        /// <summary>
        /// Retrieves a list of errors for the job
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<MessageModel> GetErrorsForJob(String id)
        {
            return ExecuteCommand(locator => GetErrorsForJobCommand(locator, id));
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// </summary>
        /// <param name="locator"></param>
        private IList<JobModel> GetAllJobsCommand(IRepositoryLocator locator, String server, int limit, int offset, DateTime from, DateTime to)
        {
            DetachedCriteria criteria = DetachedCriteria.For<JobModel>();
            criteria = BuildLimit(criteria, server, limit, offset, from, to);

            return locator.FindAll<JobModel>(criteria).ToList();
        }

        /// <summary>
        /// </summary>
        /// <param name="locator"></param>
        private IList<JobModel> GetAllJobsByTypeCommand(IRepositoryLocator locator, Type type, String server, int limit, int offset, DateTime from, DateTime to)
        {
            DetachedCriteria criteria = DetachedCriteria.For(type);
            criteria = BuildLimit(criteria,server, limit, offset, from, to);

            return locator.FindAll<JobModel>(criteria).ToList();
        }

        /// <summary>
        /// </summary>
        /// <param name="locator"></param>
        private IList<JobModel> GetAllJobsForProcessingHostCommand(IRepositoryLocator locator, String host, int limit, int offset, DateTime from, DateTime to)
        {
            DetachedCriteria criteria = DetachedCriteria.For<JobModel>();
            
            criteria.Add(Restrictions.Eq("ProcessingHost", host));
            criteria = BuildLimit(criteria, String.Empty, limit, offset, from, to);

            return locator.FindAll<JobModel>(criteria).ToList();
        }

        /// <summary>
        /// </summary>
        /// <param name="locator"></param>
        private JobModel GetJobCommand(IRepositoryLocator locator, String id)
        {
            var criteria = DetachedCriteria.For(typeof(JobModel));
            
            criteria.Add(Restrictions.Eq("ID", new Guid(id)));

            return locator.FindOne<JobModel>(criteria);
        }

        /// <summary>
        /// </summary>
        /// <param name="locator"></param>
        private JobResultModel GetJobResultCommand(IRepositoryLocator locator, String id)
        {
            var criteria = DetachedCriteria.For(typeof(JobResultModel));

            criteria.Add(Restrictions.Eq("JobId", new Guid(id)));

            return locator.FindOne<JobResultModel>(criteria);
        }

        /// <summary>
        /// Saves the job to the database
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="job"></param>
        private void CreateJobCommand(IRepositoryLocator locator, JobModel job)
        {
            locator.SaveOrUpdate(job);
        }

        /// <summary>
        /// Updates an already existing job in the database
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="job"></param>
        private void UpdateJobCommand(IRepositoryLocator locator, JobModel job)
        {
            locator.Update(job);
        }

        /// <summary>
        /// Saves the job result to the database
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="job"></param>
        private void CreateJobResultCommand(IRepositoryLocator locator, JobResultModel jobResult)
        {
            locator.SaveOrUpdate(jobResult);
        }

        /// <summary>
        /// Updates an already existing job in the database
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="job"></param>
        private void UpdateJobResultCommand(IRepositoryLocator locator, JobResultModel jobResult)
        {
            locator.Update(jobResult);
        }

        private IEnumerable<Change> GetAllChangesForJobCommand(IRepositoryLocator locator, string id)
        {
            JobModel job = GetJobCommand(locator, id);
            if (job != null)
            {
                return job.Changes;
            }

            return null;
        }

        /// <summary>
        /// Gets a breakdown of the status of a server
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="server"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private ServerSummary ServerSummaryCommand(IRepositoryLocator locator, String server, Type type)
        {
            DetachedCriteria criteria = DetachedCriteria.For(type);
            BuildLimit(criteria, server, -1, -1, default(DateTime), default(DateTime));

            var stats = locator.FindAll<JobModel>(criteria).GroupBy(j => j.State).Select(j => new {State = j.Key, Count = j.Count()});

            ServerSummary summary = new ServerSummary();
            summary.Name = server;
            summary.TotalJobs = stats.Sum(j => j.Count);

            foreach (var state in stats.ToList())
            {
                summary.JobStateTotals.Add(state.State, state.Count);
            }

            return summary;
        }

        /// <summary>
        /// Build limits for get queries
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="server"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private DetachedCriteria BuildLimit(DetachedCriteria criteria, String server, int limit, int offset, DateTime from, DateTime to)
        {
            if (limit > -1)
            {
                criteria.SetMaxResults(limit);
            }

            if (offset > -1)
            {
                criteria.SetFirstResult(offset);
            }

            if (from != default(DateTime))
            {
                criteria.Add(Restrictions.Ge("CreatedAt", from));
            }

            if (to != default(DateTime))
            {
                criteria.Add(Restrictions.Le("CreatedAt", to));
            }

            if (!String.IsNullOrEmpty(server))
            {
                criteria.Add(Restrictions.InsensitiveLike("Server", server));
            }

            return criteria;
        }


        private bool CreateLogMessage(IRepositoryLocator locator, MessageModel message)
        {
            try
            {
                locator.Save(message);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        /// <summary>
        /// Gets a list of all errors for a job
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private IEnumerable<MessageModel> GetErrorsForJobCommand(IRepositoryLocator locator, string id)
        {
            Guid jobId = new Guid(id);

            return locator.FindAll<MessageModel>().Where(j => j.JobId == jobId && j.Level == LogLevel.Error);
        }

        #endregion // Private Methods
    }
}
