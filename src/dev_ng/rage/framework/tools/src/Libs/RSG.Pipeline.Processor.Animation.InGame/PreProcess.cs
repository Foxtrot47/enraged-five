﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using SIO = System.IO;
using System.Linq;
using System.Diagnostics;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Processor.Animation.Common;
using RSG.Pipeline.Services.Animation;
using RSG.Pipeline.Services;
using RSG.Platform;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Base.Configuration;
using RSG.Pipeline.Services.Platform;
using System.Xml;

namespace RSG.Pipeline.Processor.Animation.InGame
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class PreProcess :
        BaseAnimationProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Pre-processor for in-game Animation";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Animation:PreProcess";

        /// <summary>
        /// Clip file extension.
        /// </summary>
        private static readonly String CLIP_EXT = ".clip";

        /// <summary>
        /// Anim file extension.
        /// </summary>
        private static readonly String ANIM_EXT = ".anim";
        
        /// <summary>
        /// Inclusion file name.
        /// </summary>
        private static readonly String INCLUSION_FILE = "inclusions.modlist";

        /// <summary>
        ///Exclusion file name.
        /// </summary>
        private static readonly String EXCLUSION_FILE = "exclusions.modlist";
        
        #endregion // Constants

        #region Properties
       
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            this.LoadParameters(param);

            List<IProcess> processes = new List<IProcess>();
            List<IContentNode> syncDepends = new List<IContentNode>();

            bool result = true;

            XGEUtil.ResetTaskCounter();

            String dictModFile = param.Branch.Environment.Subst("$(toolsconfig)/config/anim/ingame/dictionary_mods.xml");
            IContentNode dictModContent = owner.CreateFile(dictModFile);
            if (process.State == ProcessState.Initialised)
            {
                syncDepends.Add(dictModContent);
                syncDependencies = syncDepends;
                resultantProcesses = processes;
                process.State = ProcessState.Prebuilding;
                return result;
            }
           
            String cache = PreProcess.GetCacheDir(param);

            // Initialise the dictionary modification manager
            DictionaryModManager.Instance.InitWith(param.CoreBranch, dictModFile, param.Log);
           
            // Collect all the independent dictionaries and build anim dictionary entries for merging with out clip dictionary defs
            IEnumerable<IContentNode> inputContentMap;
            if (!GenerateInputContentMap(param, owner, process.Inputs, out inputContentMap))
            {
                param.Log.ErrorCtx(LOG_CTX, "Error generating input map.");
                syncDependencies = new List<IContentNode>();
                resultantProcesses = processes;
                return (false);
            }

            Dictionary<string, string> dictPlatformCompressions = ProcessPlatformCompressionSettings(param);

            // Loop through all the dictionaries and create our process/content chain
            List<IContentNode> outputContent = new List<IContentNode>();
            foreach (IContentNode content in inputContentMap)
            {
                RSG.Pipeline.Content.Directory directoryContent = content as Directory;
                IEnumerable<IContentNode> dictionaryInputs = directoryContent.EvaluateInputs();

                RSG.Pipeline.Content.StaticDirectory staticDirectoryProcessedContent = owner.CreateStaticDirectory((content as Directory).AbsolutePath) as StaticDirectory;

                // Don't use dictionary modifications when in preview mode
                DictionaryUnitCollection includedDictionaries = null;
                if(!param.Flags.HasFlag(EngineFlags.Preview))
                {
                    if(DictionaryModManager.Instance.DictionaryUnits.ContainsKey(directoryContent.Name))
                        includedDictionaries = DictionaryModManager.Instance.DictionaryUnits[directoryContent.Name];

                    if (includedDictionaries != null)
                    {
                        foreach (IDictionaryUnit dictionaryUnit in includedDictionaries)
                        {
                            String fullPath = SIO.Path.Combine(directoryContent.AbsolutePath, dictionaryUnit.DictionaryName.ToLower() + ".icd.zip");
                            IContentNode newContent = owner.CreateFile(fullPath);
                            if (!dictionaryInputs.Contains(newContent));
                                (dictionaryInputs as List<IContentNode>).Add(newContent);
                        }
                    }
                }

                String processedDirectory = SIO.Path.Combine(param.Branch.Processed, "anim", "ingame", String.Format("clip_{0}\\", directoryContent.Name));
                foreach (IContentNode dictionary in dictionaryInputs)
                {
                    String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                    String modDirectory = SIO.Path.Combine(cache, dictionaryName, "mf\\");
                    String prepDirectory = SIO.Path.Combine(cache, dictionaryName, "wr\\");
                    String coalesceDirectory = SIO.Path.Combine(cache, dictionaryName, "cp\\");
                    String compressionDirectory = SIO.Path.Combine(cache, dictionaryName, "cmp\\");
                    String compressionTemplateDirectory = SIO.Path.Combine(param.Branch.Project.Config.ToolsConfig, "config", "anim", "compression_templates");
                    String processedDictionary = SIO.Path.Combine(processedDirectory, SIO.Path.GetFileName((dictionary as File).AbsolutePath));

                    if (!System.IO.Directory.Exists(modDirectory))
                        System.IO.Directory.CreateDirectory(modDirectory);

                    // Generate dictionary edit metadata and serialise to the waiting room
                    IDictionaryUnit dictionaryUnit = null;
                    if(includedDictionaries != null)
                        dictionaryUnit = includedDictionaries.FindDictionaryNamed(dictionaryName);
                    String inclusionFile = SIO.Path.Combine(modDirectory, INCLUSION_FILE);
                    String exclusionFile = SIO.Path.Combine(modDirectory, EXCLUSION_FILE);
                    
                    // A bit nasty but our clean function for the preparation processor does not clean up these files.  Reason
                    // being that I want to generate them in the preprocess.
                    try
                    {
                        if (System.IO.File.Exists(inclusionFile))
                            System.IO.File.Delete(inclusionFile);
                        if (System.IO.File.Exists(exclusionFile))
                            System.IO.File.Delete(exclusionFile);
                    }
                    catch (Exception ex)
                    {
                        param.Log.ExceptionCtx(LOG_CTX, ex, "Exception deleteing inclusion/exclusion files: {0}, {1}: {2}", inclusionFile, exclusionFile, ex.Message);
                    }
                    
                    // Setup the preparation processor to extract the data and generate clip lists based on
                    // settings that affect how each animation is processed (compression, additive, etc)
                    IContentNode prepDirNode = owner.CreateDirectory(prepDirectory, "*.*");
                    ProcessBuilder prepBuilder = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.ClipDictionaryProcessor",
                        processors, owner);
                    prepBuilder.Inputs.Add(dictionary);
                    prepBuilder.AdditionalDependentContent.Add(owner.CreateFile(modDirectory));
                    
                    if (dictionaryUnit != null)
                    {
                        prepBuilder.Inputs.Add(dictModContent);
                        SerialiseDictionaryEdits(param, dictionaryUnit, prepDirectory, inclusionFile, exclusionFile);
                    }
                    object defaultSkel;
                    if (this.Parameters.TryGetValue("DefaultSkel", out defaultSkel))
                    {
                        prepBuilder.AdditionalDependentContent.Add(owner.CreateFile((string)defaultSkel));
                    }
                    prepBuilder.Outputs.Add(prepDirNode);
                    prepBuilder.Parameters.Add("Grouping", true);


                    // Setup the coalesce processor
                    IContentNode coalesceDirNode = owner.CreateDirectory(coalesceDirectory, "*.*", true);
                    ProcessBuilder coalesceBuilder = new ProcessBuilder("RSG.Pipeline.Processor.Animation.InGame.Coalesce",
                        processors, owner);
                    coalesceBuilder.Inputs.Add(prepDirNode);
                    coalesceBuilder.Outputs.Add(coalesceDirNode);

                    ICollection<IContentNode> platformProcessedDirNodes = PerformPlatformCompression(dictPlatformCompressions, owner, param, processors, coalesceDirNode, dictionary, processes, cache, dictionaryName, processedDirectory, defaultSkel);

                    // Create immutable processes and add to return collection.
                    IProcess preparation = prepBuilder.ToProcess();
                    processes.Add(preparation);
                    IProcess coalesce = coalesceBuilder.ToProcess();
                    processes.Add(coalesce);

#warning DHM FIX ME: the StaticDirectory breaks the dependency-immutability of Process!
#warning LPXO: Once support for directories is in the rage processor (imminent as of 5/10/12) it can be fixed.
                    staticDirectoryProcessedContent.StaticFiles.AddRange(platformProcessedDirNodes);
                }
                outputContent.Add(staticDirectoryProcessedContent);
            }

            // Generate all Rage processes for all the processed content we have created
            foreach (IContentNode content in outputContent)
            {
                StaticDirectory sd = content as StaticDirectory;

                ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RagePreprocessor",
                   processors, owner);
                pb.Inputs.AddRange(sd.StaticFiles);
                pb.Outputs.Add(process.Outputs.First());

                IProcess resourcingProcess = pb.ToProcess();
                if (resourcingProcess.Inputs.Count() == 0)
                {
                    param.Log.ErrorCtx(LOG_CTX, "No inputs for rage process...");
                    result = false;
                }

                processes.Add(resourcingProcess);
            }
            
            process.State = ProcessState.Discard;
            resultantProcesses = processes;
            syncDependencies = syncDepends;

            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Assign method output.
            tools = new List<XGE.ITool>();
            tasks = new List<XGE.ITask>();

            return (false);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionaryUnit"></param>
        /// <param name="prepDirectory"></param>
        /// <returns></returns>
#warning LPXO: I want to move this out of the processor and into utilities
        bool SerialiseDictionaryEdits(IEngineParameters param, IDictionaryUnit dictionaryUnit, String prepDirectory, String inclusionFile, String exclusionFile)
        {
            bool result = true;
            try
            {
                using (SIO.StreamWriter sw = new SIO.StreamWriter(inclusionFile))
                {
                    foreach (DictionaryItem dictionaryItem in dictionaryUnit.Inclusions)
                    {
                        String sourceFile = SIO.Path.Combine(param.Branch.Environment.Subst(dictionaryItem.Path), dictionaryItem.Basename);
                        String writeLine = String.Format("{0}, {1}, {2}", dictionaryItem.Basename, param.Branch.Environment.Subst(dictionaryItem.Path), dictionaryItem.OverrideName);
                        sw.WriteLine(writeLine);
                    }
                    sw.Close();
                }

                using (SIO.StreamWriter sw = new SIO.StreamWriter(exclusionFile, false))
                {
                    foreach (String excludedItem in dictionaryUnit.Exclusions)
                    {
                        String writeLine = String.Format("{0}", excludedItem);
                        sw.WriteLine(writeLine);
                    }
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                param.Log.ExceptionCtx(LOG_CTX, ex, "Exception serialising inclusion/exclusion files to: {0}.", prepDirectory);
            }
            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private bool GenerateInputContentMap(IEngineParameters param, IContentTree owner, IEnumerable<IContentNode> inputs, out IEnumerable<IContentNode> realInputs)
        {
            bool result = true;
            realInputs = new List<IContentNode>();

            List<String> excludedDictionaries = DictionaryModManager.Instance.ExcludedDictionaries as List<String>;
            // Loop through our input directories and create a dictionary for the directory node and the contents of the directory, minus any excluded directories
            foreach (IContentNode contentnode in inputs)
            {
                if (contentnode is RSG.Pipeline.Content.Directory)
                {
                    IEnumerable<IContentNode> dictionaryList = (contentnode as RSG.Pipeline.Content.Directory).EvaluateInputs();
                    if (dictionaryList.Count() == 0)
                        continue;
                    List<String> dictionaryListLower = new List<String>();
                    foreach (IContentNode dictionary in dictionaryList)
                    {
                        dictionaryListLower.Add((dictionary as RSG.Pipeline.Content.File).AbsolutePath.ToLower());
                    }

                    if (excludedDictionaries.Contains(contentnode.Name.ToLower()))
                    {
                        String message = String.Format("Ignoring dictionary {0}.  It is listed as an excluded clip dictionary.", contentnode.Name);
                        param.Log.MessageCtx(LOG_CTX, message);
                    }
                    else
                    {
                        (realInputs as List<IContentNode>).Add(contentnode);
                    }
                }
                else if (contentnode is RSG.Pipeline.Content.File)
                {
                    String filePath = SIO.Path.GetFullPath((contentnode as RSG.Pipeline.Content.File).AbsolutePath);
                    String message = String.Format("File content has made it into animation Preprocessor.  This should never happen: {0}.", filePath);
                    param.Log.ErrorCtx(LOG_CTX, message);
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        private Dictionary<string, string> ProcessPlatformCompressionSettings(IEngineParameters param)
        {
            Dictionary<string, string> dictPlatforms = new Dictionary<string, string>();

            if (this.Parameters.ContainsKey("CompressionFile"))
            {
                string compressionFile = (string)this.Parameters["CompressionFile"];
                compressionFile = param.Branch.Environment.Subst(compressionFile);
                compressionFile = compressionFile.Replace(SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar);

                if (SIO.File.Exists(compressionFile))
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(compressionFile);

                    XmlNodeList platformNodes = xmlDoc.SelectNodes("/compression/platform");
                    foreach (XmlNode node in platformNodes)
                    {
                        string compressionSettingsDir = node.Attributes["settings"].Value;
                        compressionSettingsDir = param.Branch.Environment.Subst(compressionSettingsDir);
                        compressionSettingsDir = compressionSettingsDir.Replace(SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar);

                        dictPlatforms.Add(node.Attributes["name"].Value.ToLower(), compressionSettingsDir);
                    }
                }
            }

            return dictPlatforms;
        }

        /// <summary>
        /// Prepare the compression/post process processes, calculate which platform uses what.
        /// </summary>
        /// <param name="platformCompression"></param>
        /// <param name="owner"></param>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="groupDirNode"></param>
        /// <param name="dictionary"></param>
        /// <param name="finaliseDirNode"></param>
        /// <param name="processes"></param>
        /// <param name="cache"></param>
        /// <param name="dictionaryName"></param>
        /// <param name="processedDirectory"></param>
        /// <param name="defaultSkel"></param>
        /// <returns></returns>
        private ICollection<IContentNode> PerformPlatformCompression(Dictionary<string, string> platformCompression, IContentTree owner, IEngineParameters param, IProcessorCollection processors, IContentNode groupDirNode, IContentNode dictionary, List<IProcess> processes, String cache, String dictionaryName, String processedDirectory, object defaultSkel)
        {
            List<IContentNode> contentNodes = new List<IContentNode>();
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);

            Dictionary<string, IContentNode> platformToDir = new Dictionary<string, IContentNode>();

            foreach (ITarget target in enabledTargets)
            {
                String compressionDirectory = String.Empty;
                String compressionSettingsFolder = String.Empty;
                if (platformCompression.ContainsKey(target.Platform.ToString().ToLower()))
                {
                    compressionSettingsFolder = (string)platformCompression[target.Platform.ToString().ToLower()];
                    compressionDirectory = SIO.Path.Combine(cache, dictionaryName, "cmp", SIO.Path.GetFileName(compressionSettingsFolder));
                }
                else // fall back if the platform does not exist
                {
                    compressionSettingsFolder = (string)platformCompression["default"];
                    compressionDirectory = SIO.Path.Combine(cache, dictionaryName, "cmp", SIO.Path.GetFileName(compressionSettingsFolder));
                }

                // Processor for compressing animation
                IContentNode compressionDirNode = owner.CreateDirectory(compressionDirectory, "*.*");
                ProcessBuilder compression = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.Compression", processors, owner);
                compression.Inputs.Add(groupDirNode);
                compression.AdditionalDependentContent.Add(owner.CreateFile((string)defaultSkel));
                compression.AdditionalDependentContent.Add(owner.CreateDirectory(compressionSettingsFolder, "*.*")); // Compression folder
                compression.Outputs.Add(compressionDirNode);

                // If any platforms share the same compression dir then we use the existing node that has being created (if any)
                if (!platformToDir.ContainsValue(compressionDirNode))
                {
                    IProcess compressionProcess = compression.ToProcess();
                    processes.Add(compressionProcess);

                    platformToDir.Add(target.Platform.ToString().ToLower(), compressionDirNode);
                }
                else
                {
                    foreach (IContentNode node in platformToDir.Values)
                    {
                        if (node == compressionDirNode)
                        {
                             platformToDir.Add(target.Platform.ToString().ToLower(), node);
                             break;
                        }
                    }
                }
            }

            // We have a per platform set of zips, but re-use the compression folders
            foreach (ITarget target in enabledTargets)
            {
                String platformProcessedDirectory = SIO.Path.Combine(processedDirectory, target.Platform.ToString().ToLower());
                IContentNode directoryProcessedContent = owner.CreateFile(SIO.Path.Combine(platformProcessedDirectory, dictionaryName + ".icd.zip"));
                IContentNode compressionDirNode = platformToDir[target.Platform.ToString().ToLower()];

                ProcessBuilder postProcess = new ProcessBuilder("RSG.Pipeline.Processor.Animation.InGame.PostProcess", processors, owner);
                postProcess.Inputs.Add(compressionDirNode);
                postProcess.Outputs.Add(directoryProcessedContent);

                if (!directoryProcessedContent.Parameters.ContainsKey(Constants.Platform_Override))
                    directoryProcessedContent.Parameters.Add(Constants.Platform_Override, target.Platform);

                IProcess postProcessProcess = postProcess.ToProcess();
                processes.Add(postProcessProcess);

                contentNodes.Add(directoryProcessedContent);
            }

            return contentNodes;
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private bool GenerateInputContentMap(IEngineParameters param, IContentTree owner, IEnumerable<IContentNode> inputs, out IDictionary<IContentNode, IEnumerable<String>>  inputContentMap)
        {
            bool result = true;
            inputContentMap = new Dictionary<IContentNode, IEnumerable<String>>();

            List<String> excludedDictionaries = DictionaryModManager.Instance.ExcludedDictionaries as List<String>;
            // Loop through our input directories and create a dictionary for the directory node and the contents of the directory, minus any excluded directories
            foreach (IContentNode contentnode in inputs)
            {
                if (contentnode is RSG.Pipeline.Content.Directory)
                {
                    IEnumerable<IContentNode> dictionaryList = (contentnode as RSG.Pipeline.Content.Directory).EvaluateInputs();
                    if (dictionaryList.Count() == 0)
                        continue;
                    List<String> dictionaryListLower = new List<String>();
                    foreach (IContentNode dictionary in dictionaryList)
                    {
                        dictionaryListLower.Add((dictionary as RSG.Pipeline.Content.File).AbsolutePath.ToLower());
                    }

                    if (excludedDictionaries.Contains(contentnode.Name.ToLower()))
                    {
                        String message = String.Format("Ignoring dictionary {0}.  It is listed as an excluded clip dictionary.", contentnode.Name);
                        param.Log.MessageCtx(LOG_CTX, message);
                    }
                    else
                    {
                        IContentNode staticDirNode = contentnode;
                        //IContentNode staticDirNode = null; 
                        //if(contentnode is StaticDirectory)
                        //    staticDirNode = contentnode;
                        //else
                        //    staticDirNode = owner.CreateStaticDirectory((contentnode as RSG.Pipeline.Content.Directory).AbsolutePath);
                        inputContentMap.Add(staticDirNode, dictionaryListLower);
                    }
                }
                else if (contentnode is RSG.Pipeline.Content.File)
                {
                    String filePath = Path.GetFullPath((contentnode as RSG.Pipeline.Content.File).AbsolutePath);
                    String message = String.Format("File content has made it into animation Preprocessor.  This should never happen: {0}.", filePath);
                    param.Log.ErrorCtx(LOG_CTX, message);
                    result = false;
                }
            }
            return result;
        }
         * */
        #endregion

        #region Private Static Methods
        /// <summary>
        /// Return cache dir for the branch.  Could be a property if we kept the params as a member?
        /// </summary>
        /// <returns></returns>
        private static String GetCacheDir(IEngineParameters param)
        {
            return (SIO.Path.Combine(param.Branch.Project.Cache, param.Branch.Name, "iga"));  
            //return (SIO.Path.Combine(param.Branch.Project.Cache, "convert", param.Branch.Name, "anim", "ingame"));  
        }
        #endregion // Private Static Methods
    }

} // RSG.Pipeline.Processor.Animation.InGame namespace
