﻿using System;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Interface describing that a asset node is related to a particular 
    /// platform; e.g. PS3, Xenon, Win32, Win64.
    /// </summary>
    [Obsolete]
    public interface IHasTarget
    {
        /// <summary>
        /// Associated target platform information.
        /// </summary>
        ITarget Target { get; }
    }

} // RSG.Pipeline.Content namespace
