﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content.Test
{

    /// <summary>
    /// 
    /// </summary>
    [TestFixture]
    public class ContentTreeDynamic
    {
        /// <summary>
        /// 
        /// </summary>
        [Test]
        public void TestDynamicTree()
        {
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;
            IContentTree tree = Factory.CreateEmptyTree(branch);

            Assert.AreEqual(0, tree.Processes.Count());
        }
    }

} // RSG.Pipeline.Content.Test namespace
