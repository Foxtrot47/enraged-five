﻿//---------------------------------------------------------------------------------------------
// <copyright file="Search.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Content.Algorithm
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Pipeline.Core;

    /// <summary>
    /// Content-tree extension method for search algorithms.
    /// </summary>
    public static class Search
    {
        /// <summary>
        /// Return a collection of processes that reference a specific node
        /// as an input.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> FindProcessesWithInput(this IContentTree tree, IContentNode node)
        {
            return (tree.Processes.AsParallel().Where(p => p.Inputs.Contains(node)));
        }

        /// <summary>
        /// Return a collection of processes that reference a specific node
        /// as an input (using a specific processor).
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="node"></param>
        /// <param name="processorClassName"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> FindProcessesWithInput(this IContentTree tree, IContentNode node, String processorClassName)
        {
            Debug.Assert(!String.IsNullOrEmpty(processorClassName),
                "processorClassName cannot be null or empty!");
            if (String.IsNullOrEmpty(processorClassName))
                throw (new ArgumentException("processorClassName cannot be null or empty!", "processorClassName"));

            return (tree.Processes.AsParallel().Where(p => p.ProcessorClassName.Equals(processorClassName) && p.Inputs.Contains(node)));
        }

        /// <summary>
        /// Return a collection of processes that reference a specific node
        /// as an output.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> FindProcessesWithOutput(this IContentTree tree, IContentNode node)
        {
            return (tree.Processes.AsParallel().Where(p => p.Outputs.Contains(node)));
        }

        /// <summary>
        /// Return a collection of processes that reference a specific node
        /// as an output (using a specific processor).
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="node"></param>
        /// <param name="processorClassName"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> FindProcessesWithOutput(this IContentTree tree, IContentNode node, String processorClassName)
        {
            Debug.Assert(!String.IsNullOrEmpty(processorClassName),
                "processorClassName cannot be null or empty!");
            if (String.IsNullOrEmpty(processorClassName))
                throw (new ArgumentException("processorClassName cannot be null or empty!", "processorClassName"));

            return (tree.Processes.AsParallel().Where(p => p.ProcessorClassName.Equals(processorClassName) && p.Outputs.Contains(node)));
        }

        /// <summary>
        /// Return a collection of processes that reference a specific node
        /// as an input or an output.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> FindProcessesWithNode(this IContentTree tree, IContentNode node)
        {
            IEnumerable<IProcess> asInput = FindProcessesWithInput(tree, node);
            IEnumerable<IProcess> asOutput = FindProcessesWithOutput(tree, node);
            return (asInput.Concat(asOutput));
        }

        public static IEnumerable<IProcess> FindProcessesOfType(this IContentTree tree, string processorName)
        {
            return tree.Processes.AsParallel().Where(p => p.ProcessorClassName == processorName);
        }
    }

} // RSG.Pipeline.Content.Algorithm namespace
