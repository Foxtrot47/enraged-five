﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using RSG.Base.Extensions;
using RSG.Base.IO;
using RSG.Pipeline.Core;
using RSG.Platform;
using Ionic.Zip;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Archive directory node; wrapping an archive file and wildcard mask 
    /// representing a collection of files within an archive.
    /// </summary>
    public class ArchiveDirectory :
        ArchiveFilesystemNodeBase,
        IArchiveFilesystemNode
    {
        #region Properties
        /// <summary>
        /// Relative directory paths within archive.
        /// </summary>
        public IEnumerable<String> RelativeDirectoryPaths
        {
            get { return (this.m_RelativeFilePaths); }
        }

        /// <summary>
        /// Files wildcard to include as inputs.
        /// </summary>
        public String Wildcard
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether to search subdirectories.
        /// </summary>
        public bool Recursive
        {
            get;
            private set;
        }
        #endregion // Properties        

        #region Constructor(s)
        /// <summary>
        /// Constructor; from combined path string.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="combinedPath"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        internal ArchiveDirectory(IContentTree owner, String combinedPath, 
            String wildcard = "*.*", bool recursive = false)
            : this(owner, Guid.NewGuid(), combinedPath, wildcard, recursive)
        {
        }

        /// <summary>
        /// Constructor; from combined path string with GUID.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="combinedPath"></param>
        internal ArchiveDirectory(IContentTree owner, Guid id, String combinedPath,
            String wildcard = "*.*", bool recursive = false)
            : base(owner, id, combinedPath)
        {
            this.Wildcard = wildcard;
            this.Recursive = recursive;
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Modified time (UTC) of this archive filesystem object.
        /// </summary>
        public override DateTime GetModifiedTimestamp()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                IEnumerable<ZipEntry> entries = GetZipEntriesRecursive(zf, 
                    this.m_RelativeFilePaths, this.Wildcard, this.Recursive);
                
                if (!entries.Any())
                    return (DateTime.MinValue);

                DateTime dt = DateTime.MinValue;
                foreach (ZipEntry entry in entries)
                {
                    if (entry.ModifiedTime.IsLaterThan(dt))
                        dt = entry.ModifiedTime;
                }
                return (dt);
            }
        }

        /// <summary>
        /// Set modified time (UTC) of this archive filesystem object.
        /// </summary>
        /// <param name="value"></param>
        public override void SetModifiedTimestamp(DateTime value)
        {
            throw (new NotSupportedException());
        }

        /// <summary>
        /// Creation time (UTC) of this archive filesystem object.
        /// </summary>
        public override DateTime GetCreationTimestamp()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                IEnumerable<ZipEntry> entries = GetZipEntriesRecursive(zf,
                    this.m_RelativeFilePaths, this.Wildcard, this.Recursive);
                
                if (!entries.Any())
                    return (DateTime.MinValue);

                DateTime dt = DateTime.MinValue;
                foreach (ZipEntry entry in entries)
                {
                    if (entry.CreationTime.IsLaterThan(dt))
                        dt = entry.CreationTime;
                }
                return (dt);
            }
        }

        /// <summary>
        /// Calculate and return the size (in bytes) of the archive filesystem 
        /// data (uncompressed size).
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        public override long GetSize()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (0);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (0);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                IEnumerable<ZipEntry> entries = GetZipEntriesRecursive(zf,
                    this.m_RelativeFilePaths, this.Wildcard, this.Recursive);
                
                if (!entries.Any())
                    return (0);

                long size = 0;
                foreach (ZipEntry entry in entries)
                    size += entry.UncompressedSize;
                return (size);
            }
        }

        /// <summary>
        /// Determine if the archive filesystem node exists.
        /// </summary>
        /// <returns>true iff object exists; false otherwise</returns>
        public override bool Exists()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (false);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (false);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                IEnumerable<ZipEntry> entries = GetZipEntriesRecursive(zf,
                    this.m_RelativeFilePaths, this.Wildcard, this.Recursive);
                return (entries.Any());
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Content namespace
