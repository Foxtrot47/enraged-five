using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using RSG.Base.Configuration;
using GML = RSG.GraphML;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content.Attributes;
using RSG.Platform;

namespace RSG.Pipeline.Content
{

    /*! \mainpage
     *
     * \section intro_sec Purpose
     *
     * The RSG.Pipeline.Content assembly provides the asset pipeline's content
     * tree representation, factory and core algorithms.
     * 
     * The core interfaces applications will use are the Factory and 
     * IContentTree/ContentTree classes.  The Factory should only be used 
     * directly if content is not part of a content tree.
     *
     * \section unit_test Unit Tests
     * 
     * There are a few very simple unit tests available in this assembly.
     */

    /// <summary>
    /// GraphML to content tree nodes and processes factory class.
    /// </summary>
    /// This is the only place that implies understanding to the GraphML
    /// attributes.
    /// 
    /// There are a few public helper methods to aid users creating content
    /// nodes at runtime.
    /// 
    public static class Factory
    {
        #region Constants
        /// <summary>
        /// Parameters whose insertions are ignored into the generic parameter
        /// dictionary.
        /// </summary>
        private static readonly String[] IgnoredParameters = new String[]{
            "path", "content_type", "wildcard", "recursive", // Node Parameters
            "processor", // Edge Parameters
        };
        #endregion // Constants

        #region Static Controller Methods
        /// <summary>
        /// Create a new content tree; initially empty.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        /// This method is typically used for testing or creating dynamic 
        /// content trees in code.
        /// 
        public static IContentTree CreateEmptyTree(IBranch branch)
        {
            return (new ContentTree(branch));
        }


        /// <summary>
        /// Create content-tree for a branch.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        /// <exception cref="ContentException" />
        /// 
        public static IContentTree CreateTree(IBranch branch)
        {
            GML.GraphCollection mainGraphs = new GML.GraphCollection(branch);
            IContentTree mainTree = null;

            // If we're loading DLC content-tree then first load the main project's tree.
            if (branch.Project.IsDLC)
            {
                IConfig config = branch.Project.Config;
                IBranch coreBranch = null;
                if (config.CoreProject.Branches.ContainsKey(branch.Name))
                    coreBranch = config.CoreProject.Branches[branch.Name];
                else
                    coreBranch = config.CoreProject.DefaultBranch;

                String coreContentFilename = coreBranch.Content;
                if (!String.IsNullOrEmpty(coreContentFilename) && SIO.File.Exists(coreContentFilename))
                {
                    if (!mainGraphs.LoadGraphs(coreContentFilename))
                        throw (new ContentException("GraphML load failed; see log for details.  Aborting Content-XML transform."));
                }
                mainTree = CreateTree(coreBranch, mainGraphs);
            }

            // Load branch-specific content-tree.
            GML.GraphCollection branchGraphs = new GML.GraphCollection(branch);
            String contentFilename = branch.Content;
            if (!String.IsNullOrEmpty(contentFilename) && SIO.File.Exists(contentFilename))
            {
                if (!branchGraphs.LoadGraphs(contentFilename))
                    throw (new ContentException("GraphML load failed; see log for details.  Aborting Content-XML transform."));
            }
            else if (!String.IsNullOrEmpty(contentFilename))
            {
                throw (new ContentException("GraphML load failed; see log for details.  Aborting Content-XML transform.",
                    new SIO.FileNotFoundException("Requested content-tree XML does not exist.", contentFilename)));
            }

            IContentTree branchTree = CreateTree(branch, branchGraphs);
            if (null != mainTree)
                branchTree.Merge(mainTree);
            
            return (branchTree);
        }

        /// <summary>
        /// Construct a content-tree from a GraphML GraphCollection.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="graphs"></param>
        /// <param name="processors"></param>
        /// <returns></returns>
        public static IContentTree CreateTree(IBranch branch, GML.GraphCollection graphs)
        {
            ContentTree.Log.Profile("Processing GraphML into Content-Tree");
            IContentTree tree = CreateEmptyTree(branch);
            foreach (GML.Graph graph in graphs)
            {
                if (!graph.AllNodes().Any())
                {
                    ContentTree.Log.Error("The content tree for this file is broken and doesn't seem to contain nodes!");
                    continue;
                }
                if (!graph.AllEdges().Any())
                {
                    ContentTree.Log.Error("The content tree for this file is broken and doesn't seem to contain edges!");
                    continue;
                }

                // Create all edges and nodes in one fell swoop.
                Create(tree, graph.AllEdges());
            }
            ContentTree.Log.ProfileEnd();

            return (tree);
        }

        /// <summary>
        /// Return an enumerable of all XML files referenced in a content-tree.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static IEnumerable<String> GetFilenames(IBranch branch)
        {
            ICollection<String> filenames = new List<String>();
            String rootContentFilename = branch.Environment.Subst(branch.Content);
            filenames.Add(rootContentFilename);
        
            // Read our root file with a custom XML xi:include resolver to return all files.
            if (SIO.File.Exists(rootContentFilename))
            {
                using (SIO.FileStream fs = new SIO.FileStream(rootContentFilename, SIO.FileMode.Open,
                    SIO.FileAccess.Read, SIO.FileShare.Read))
                {
                    String currentDir = SIO.Directory.GetCurrentDirectory();
                    try
                    {
                        SIO.Directory.SetCurrentDirectory(SIO.Path.GetDirectoryName(rootContentFilename));
                        using (Mvp.Xml.XInclude.XIncludingReader xmlReader =
                            new Mvp.Xml.XInclude.XIncludingReader(fs))
                        {
                            xmlReader.XmlResolver = new LocalFileResolver(filenames, branch);
                            xmlReader.MoveToContent();
                            while (xmlReader.Read())
                            {
                                if (!xmlReader.IsStartElement())
                                    continue;
                                ParseSkip(xmlReader);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // Catch and continue; might need multiple passes at this.  
                    }
                    finally
                    {
                        SIO.Directory.SetCurrentDirectory(currentDir);
                    }
                }
            }
            return (filenames);
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Construct a INode object from a GraphML Node.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        private static IContentNode Create(IContentTree owner, GML.Node node)
        {
            GML.GraphCollection container = node.Owner.Container;
            String path = (ReadAttributeData(container, node, "path") as String);
            IContentNode content_node = null;

            String content_type = (ReadAttributeData(container, node, "content_type") as String);
            if (String.IsNullOrEmpty(content_type))
            {
                ContentTree.Log.ErrorCtx(node.ID, "Misconfigured node: '{0}' ('{1}'): missing 'content_type' data.",
                    node.ID, node.QualifiedID);
                return (null);
            }

            if (String.Equals(content_type, File.XML_CONTENT))
            {
                content_node = owner.CreateFile(path);
            }
            else if (String.Equals(content_type, Asset.XML_CONTENT))
            {
                content_node = owner.CreateAsset(path, RSG.Platform.Platform.Independent);
            }
            else if (String.Equals(content_type, Directory.XML_CONTENT))
            {
                String wildcard = (String)ReadAttributeData(container, node, "wildcard");
                bool recursive = (bool)ReadAttributeData(container, node, "recursive");
                content_node = owner.CreateDirectory(path, wildcard, recursive);
            }
            else if (String.Equals(content_type, RegexDirectory.XML_CONTENT))
            {
                String wildcard = (String)ReadAttributeData(container, node, "wildcard");
                String expression = (String)ReadAttributeData(container, node, "expression");
                bool recursive = (bool)ReadAttributeData(container, node, "recursive");
                content_node = owner.CreateRegexDirectory(path, wildcard, expression, recursive);
            }
            else if (String.Equals(content_type, "dummy"))
            {
                // DHM FIX ME: what the hell are dummy content-nodes?  Why is this required?
                content_node = owner.CreateFile(path);
            }
            else
            {
                ContentTree.Log.Error("Unrecognised content node type: '{0}'.  Node will be ignored.", content_type);
            }

            ReadAllAttributeData(node, content_node.Parameters);
            
            return (content_node);
        }

        /// <summary>
        /// Construct IProcess objects for each shared output.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="edges"></param>
        /// <param name="procCollection"></param>
        /// <returns></returns>
        private static void Create(IContentTree owner, IEnumerable<GML.Edge> edges)
        {
            Debug.Assert(edges.Any(), "No edges specified.");
            if (!edges.Any())
                throw (new ArgumentException("No edges specified.", "edges"));

            // Find all edges that share each inputs or outputs; GML.Node ID key.
            IDictionary<GML.Node, ICollection<GML.Edge>> inputToEdgeMapping = 
                new Dictionary<GML.Node, ICollection<GML.Edge>>();
            IDictionary<GML.Node, ICollection<GML.Edge>> outputToEdgeMapping =
                new Dictionary<GML.Node, ICollection<GML.Edge>>();
            foreach (GML.Edge edge in edges)
            {                
                GML.GraphCollection container = edge.Owner.Container;
                String processor = (String)ReadAttributeData(container, edge, "processor");

                if (!inputToEdgeMapping.ContainsKey(edge.Source))
                    inputToEdgeMapping.Add(edge.Source, new List<GML.Edge>());
                if (!outputToEdgeMapping.ContainsKey(edge.Target))
                    outputToEdgeMapping.Add(edge.Target, new List<GML.Edge>());
                inputToEdgeMapping[edge.Source].Add(edge);
                outputToEdgeMapping[edge.Target].Add(edge);
            }

            // Create all nodes; and cache for lookup later.
            IDictionary<GML.Node, IContentNode> nodeCache = new Dictionary<GML.Node, IContentNode>();
            foreach (GML.Node node in edges.First().Owner.AllNodes())
                nodeCache.Add(node, Create(owner, node));

            // Iterate through our processors; using our input and output mappings
            // to construct processes that share inputs or share outputs (of the
            // same processor type).
            List<GML.Edge> processedEdges = new List<GML.Edge>();
            foreach (GML.Edge edge in edges)
            {
                GML.GraphCollection container = edge.Owner.Container;
                String processor = (String)ReadAttributeData(container, edge, "processor");
                IEnumerable<GML.Edge> edgesWithInput = inputToEdgeMapping[edge.Source].Where(e => 
                    processor.Equals((String)ReadAttributeData(container, e, "processor")));
                IEnumerable<GML.Edge> edgesWithOutput = outputToEdgeMapping[edge.Target].Where(e =>
                    processor.Equals((String)ReadAttributeData(container, e, "processor")));

                // Produce us a process; unless we have already considered these edges.
                if (processedEdges.Contains(edge))
                    continue;

                // Read edge attribute data.
                // DHM FIX ME: this isn't ideal as different edges could be flagged differently!
                bool allowRemoteAsBool = (bool)ReadAttributeData(container, edge, "allow_remote");
                AllowRemoteType allowRemote = allowRemoteAsBool ? AllowRemoteType.Yes : AllowRemoteType.No;
                bool rebuildAsBool = (bool)ReadAttributeData(container, edge, "rebuild");
                RebuildType rebuild = rebuildAsBool ? RebuildType.Yes : RebuildType.No;

                // Create process; taking the outputs from all edges with the shared input,
                // and the inputs from all edges with shared output.
                IEnumerable<IContentNode> outputNodes = edgesWithInput.Select(e => nodeCache[e.Target]);
                IEnumerable<IContentNode> inputNodes = edgesWithOutput.Select(e => nodeCache[e.Source]);
                IProcess process = owner.CreateProcess(processor, inputNodes, outputNodes, 
                    new IContentNode[] { }, allowRemote, rebuild);

                ReadAllAttributeData(edge, process.Parameters);

                // Flag all edges are being handled.
                processedEdges.AddRange(edgesWithInput);
                processedEdges.AddRange(edgesWithOutput);
            }
        }

        /// <summary>
        /// Read GraphML attribute data; returning value, default, or null.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static Object ReadAttributeData(GML.GraphCollection container, GML.IHasData element, String key)
        {
            GML.Data data = element.Data.FirstOrDefault(d => d.Key == key);
            if (null == data)
            {
                if (container.Attributes.ContainsKey(key))
                    return (container.Attributes[key].DefaultValue);
                return (null);
            }
            else
            {
                return (data.Value);
            }
        }

        /// <summary>
        /// Read all attributes into element parameter dictionary.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="parameters"></param>
        private static void ReadAllAttributeData(GML.IHasData element, 
            IDictionary<String, Object> parameters)
        {
            foreach (GML.Data data in element.Data)
            {
                if ((!parameters.ContainsKey(data.Key)) && (!IgnoredParameters.Contains(data.Key)))
                    parameters.Add(data.Key, data.Value);
            }
        }

        /// <summary>
        /// Internal implementation of ParseSkip; to read through XML data to fetch xi:include.
        /// </summary>
        /// <param name="reader"></param>
        private static void ParseSkip(XmlReader reader)
        {
            if (reader.IsEmptyElement)
                reader.ReadStartElement();
            else
            {
                reader.ReadStartElement();
                while (reader.MoveToContent() == XmlNodeType.Element)
                {
                    ParseSkip(reader);
                }
                if (reader.NodeType == XmlNodeType.Text)
                    reader.ReadString();
                Debug.Assert(XmlNodeType.EndElement == reader.NodeType);
                if (reader.NodeType == XmlNodeType.EndElement)
                    reader.ReadEndElement();
            }
        }
        #endregion // Private Methods
    }

    #region Exception Classes
    /// <summary>
    /// Content Factory Exception class.
    /// </summary>
    public class ContentFactoryException : ContentException
    {
        #region Constructor(s)
        public ContentFactoryException()
            : base()
        {
        }

        public ContentFactoryException(String message)
            : base(message)
        {
        }

        public ContentFactoryException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public ContentFactoryException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }
    #endregion // Exception Classes

    #region XmlResolver Implementation
    /// <summary>
    /// XmlResolver subclass for resolving local files only; this stores all of the filenames
    /// read during the parse so we can return them.
    /// </summary>
    class LocalFileResolver : System.Xml.XmlResolver
    {
        #region Member Data
        /// <summary>
        /// Filenames stored during the parse.
        /// </summary>
        private ICollection<String> _filenameCollection;

        /// <summary>
        /// Current branch used, for branch specific key-based path resolving
        /// </summary>
        private IBranch _branch;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor; taking collection to add stuff into.
        /// </summary>
        /// <param name="filenameCollection"></param>
        /// <param name="branch"></param>
        public LocalFileResolver(ICollection<String> filenameCollection, IBranch branch)
        {
            this._filenameCollection = filenameCollection;
            this._branch = branch;
        }
        #endregion // Constructor(s)

        #region XmlResolver Methods
        public override System.Net.ICredentials Credentials
        {
            set { }
        }

        public override object GetEntity(Uri absoluteUri, String role, Type ofObjectToReturn)
        {
            try
            {
                string localPath = absoluteUri.LocalPath;

                int envVarIndex = localPath.IndexOf("$(", StringComparison.Ordinal);
                if (envVarIndex != -1)
                {
                    localPath = localPath.Substring(envVarIndex);
                    if (_branch != null)
                    {
                        localPath = this._branch.Environment.Subst(localPath);
                    }
                    else
                    {
                        throw new NullReferenceException("You're trying to resolve a path containing $(env-based) arguments without having the Branch initialized. There's something wrong here.");
                    }
                }

                Debug.Assert(absoluteUri.IsFile, "File not specified!");
                Debug.Assert(SIO.File.Exists(localPath));
                lock (this._filenameCollection)
                    this._filenameCollection.Add(localPath);

                if (SIO.File.Exists(localPath))
                    return (new SIO.FileStream(localPath, SIO.FileMode.Open, SIO.FileAccess.Read, SIO.FileShare.ReadWrite));

                // If our file does not exist then we create a MemoryStream that
                // is just an empty XML document to keep parsing.
                SIO.MemoryStream ms = new SIO.MemoryStream(4096);
                System.Xml.Linq.XDocument xmlDoc = new System.Xml.Linq.XDocument(
                    new System.Xml.Linq.XDeclaration("1.0", "UTF-8", "yes"),
                    new System.Xml.Linq.XElement("graphml"));
                xmlDoc.Save(ms);
                ms.Seek(0, SIO.SeekOrigin.Begin);
                return (ms);
            }
            catch (SIO.FileNotFoundException ex)
            {
            }
            catch (Exception ex)
            {
            }
            return (null);
        }

        public override Uri ResolveUri(Uri baseUri, String relativeUri)
        {
            if (SIO.Path.IsPathRooted(relativeUri))
                return (new Uri(relativeUri, UriKind.Absolute));
            else
            {
                String path = Environment.ExpandEnvironmentVariables(relativeUri);
                if (SIO.Path.IsPathRooted(path))
                    return (new Uri(path, UriKind.Absolute));
                else
                {
                    String absolutePath = SIO.Path.Combine(SIO.Directory.GetCurrentDirectory(), relativeUri);
                    return (new Uri(absolutePath, UriKind.Absolute));
                }
            }
        }
        #endregion // XmlResolver Methods
    }
    #endregion // XmlResolver Implementation

} // RSG.Pipeline.Content namespace
