﻿using System;
using System.Collections.Generic;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// 
    /// </summary>
    public interface IInputEvaluator : IContentNode
    {
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IContentNode> EvaluateInputs();
    }

} // RSG.Pipeline.Content namespace
