﻿//---------------------------------------------------------------------------------------------
// <copyright file="RegexDirectory.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Content
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Pipeline.Core;

    /// <summary>
    /// Content node representing a filesystem directory.
    /// </summary>
    public class RegexDirectory :
        Directory,
        IFilesystemNode,
        IInputEvaluator
    {
        #region Constants
        /// <summary>
        /// Content-tree XML content_type value.
        /// </summary>
        internal const String XML_CONTENT = "regex_directory";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// File regular expression.
        /// </summary>
        public Regex Expression
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="pathname"></param>
        /// <param name="expression"></param>
        /// <param name="recursive"></param>
        internal RegexDirectory(IContentTree owner, String pathname, String wildcard, String expression, bool recursive)
            : this(owner, Guid.NewGuid(), pathname, wildcard, expression, recursive)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="pathname"></param>
        /// <param name="expression"></param>
        /// <param name="recursive"></param>
        internal RegexDirectory(IContentTree owner, Guid id, String pathname, String wildcard, String expression, bool recursive)
            : base(owner, id, pathname, wildcard, recursive)
        {
            this.Expression = new Regex(expression, RegexOptions.IgnoreCase);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Evaluate inputs.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<IContentNode> EvaluateInputs()
        {
            ICollection<IContentNode> inputs = new List<IContentNode>();
            if (this.Exists())
            {
                foreach (String filename in this.EvaluateFiles())
                {
                    IContentNode input = this.Owner.CreateFile(filename);
                    if (null != input)
                        inputs.Add(input);
                }
            }
            return (inputs);
        }
        #endregion // Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// Object equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == this)
                return (true);
            if (!(obj is RegexDirectory))
                return (false);

            RegexDirectory dir = (obj as RegexDirectory);
            if (!dir.AbsolutePath.Equals(this.AbsolutePath))
                return (false);
            else if (!dir.Wildcard.Equals(this.Wildcard))
                return (false);
            else if (dir.Recursive != this.Recursive)
                return (false);
            else if (!dir.Expression.Equals(this.Expression))
                return (false);
            return (true);
        }

        /// <summary>
        /// Hashcode calculation.
        /// </summary>
        /// <returns></returns>
        /// *Note*: safe because the properties are read-only.
        /// 
        public override int GetHashCode()
        {
            int hashCode = this.AbsolutePath.GetHashCode();
            hashCode ^= this.Wildcard.GetHashCode();
            hashCode ^= this.Expression.GetHashCode();
            hashCode ^= this.Recursive.GetHashCode();

            return (hashCode);
        }

        /// <summary>
        /// Return String representation.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.AbsolutePath);
        }
        #endregion // Object Overridden Methods

        #region Private Methods
        /// <summary>
        /// Evaluate files in directory; returning array of them.
        /// </summary>
        /// <returns></returns>
        private String[] EvaluateFiles()
        {
            SIO.SearchOption options = this.Recursive ?
                SIO.SearchOption.AllDirectories : SIO.SearchOption.TopDirectoryOnly;

            String[] files = SIO.Directory.GetFiles(this.AbsolutePath, this.Wildcard, options);
            List<String> filenames = new List<String>();
            foreach (String filename in files)
            {
                String filenameWithoutExtension = SIO.Path.GetFileNameWithoutExtension(filename);
                if (this.Expression.IsMatch(filenameWithoutExtension))
                    filenames.Add(filename);
            }
            return (filenames.ToArray());
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Content namespace
