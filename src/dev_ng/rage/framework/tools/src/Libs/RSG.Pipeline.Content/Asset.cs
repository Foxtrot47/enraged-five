﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Platform;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Asset abstraction; marking a file with a particular game file type and
    /// platform.
    /// </summary>
    public class Asset :
        File,
        IFilesystemNode,
        IHasPlatform
    {
        #region Constants
        /// <summary>
        /// Content-tree XML content_type value.
        /// </summary>
        internal const String XML_CONTENT = "asset";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// File type of the asset.
        /// </summary>
        public FileType AssetType
        {
            get;
            private set;
        }

        /// <summary>
        /// Platform associated with the asset.
        /// </summary>
        public RSG.Platform.Platform Platform
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from filename.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="filename"></param>
        internal Asset(IContentTree owner, String filename)
            : this(owner, Guid.NewGuid(), filename)
        {
        }

        /// <summary>
        /// Constructor; from Guid and filename.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="filename"></param>
        internal Asset(IContentTree owner, Guid id, String filename)
            : this(owner, id, filename, RSG.Platform.Platform.Independent)
        {
        }

        /// <summary>
        /// Constructor; from Guid, filename and platform.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        internal Asset(IContentTree owner, Guid id, String filename, RSG.Platform.Platform platform)
            : base(owner, id, filename)
        {
            FileType[] filetypes = Filename.GetFileTypes(filename);
            if (1 == filetypes.Length)
                this.AssetType = filetypes[0];
            else if (filetypes.Length > 1)
                this.AssetType = filetypes[1];
            else
                this.AssetType = FileType.File;

            this.Platform = platform;

            // Add RPF pathname parameter default.
            this.Parameters.Add(Constants.Asset_RPFPath, System.IO.Path.GetFileName(filename));
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// Object equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is Asset))
                return (false);

            return (this.Equals((Asset)obj));
        }

        /// <summary>
        /// Asset equality.
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public bool Equals(Asset asset)
        {
            if (null == (Object)asset)
                return (false);

            if (!asset.AbsolutePath.Equals(this.AbsolutePath))
                return (false);
            else if (!asset.AssetType.Equals(this.AssetType))
                return (false);
            else if (!asset.Platform.Equals(this.Platform))
                return (false);
            return (true);
        }

        /// <summary>
        /// Hashcode calculation.
        /// </summary>
        /// <returns></returns>
        /// *Note*: safe because the properties are read-only.
        /// 
        public override int GetHashCode()
        {
            int hashCode = this.AbsolutePath.GetHashCode();
            hashCode ^= this.AssetType.GetHashCode();
            hashCode ^= this.Platform.GetHashCode();

            return (hashCode);
        }
        #endregion // Object Overridden Methods
    }

} // RSG.Pipeline.Content namespace
