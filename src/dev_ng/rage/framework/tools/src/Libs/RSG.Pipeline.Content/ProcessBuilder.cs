﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Process object builder class to allow easy construction of dependency-
    /// immutable IProcess objects.
    /// </summary>
    public class ProcessBuilder : IHasParameters
    {
        #region Properties
        /// <summary>
        /// Owning content-tree.
        /// </summary>
        public IContentTree Owner { get; private set; }
        
        /// <summary>
        /// Processor classname.
        /// </summary>
        public String ProcessorClassName { get; private set; }

        /// <summary>
        /// Process inputs.
        /// </summary>
        public ISet<IContentNode> Inputs { get; private set; }

        /// <summary>
        /// Process outputs.
        /// </summary>
        public ISet<IContentNode> Outputs { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ISet<IContentNode> AdditionalDependentContent { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// DHM TODO: retire public access; use GetParameter/SetParameter instead.
        public IDictionary<String, Object> Parameters { get; private set; }
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="processorClassName">Processor class name.</param>
        /// <param name="owner">Owning content-tree.</param>
        /// 
        public ProcessBuilder(String processorClassName, IContentTree owner)
        {
            this.ProcessorClassName = String.Intern(processorClassName);
            if (null == owner)
                throw new ArgumentNullException("Invalid owner.", "owner");

            this.Owner = owner;
            this.Inputs = new HashSet<IContentNode>();
            this.Outputs = new HashSet<IContentNode>();
            this.AdditionalDependentContent = new HashSet<IContentNode>();
            this.Parameters = new Dictionary<String, Object>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="processorClassName">Processor class name.</param>
        /// <param name="processors">Processor collection.</param>
        /// <param name="owner">Owning content-tree.</param>
        /// 
        [Obsolete("Use overload without IProcessorCollection as its no longer required.")]
        public ProcessBuilder(String processorClassName, IProcessorCollection processors,
            IContentTree owner)
        {
            this.ProcessorClassName = String.Intern(processorClassName);
            if (null == owner)
                throw new ArgumentNullException("Invalid owner.", "owner");

            this.Owner = owner;
            this.Inputs = new HashSet<IContentNode>();
            this.Outputs = new HashSet<IContentNode>();
            this.AdditionalDependentContent = new HashSet<IContentNode>();
            this.Parameters = new Dictionary<String, Object>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="processor">Processor.</param>
        /// <param name="owner">Owning content-tree.</param>
        /// 
        public ProcessBuilder(IProcessor processor, IContentTree owner)
        {
            if (null == processor)
                throw new ArgumentNullException("Invalid processor.", "processor");
            if (null == owner)
                throw new ArgumentNullException("Invalid owner.", "owner");

            this.Owner = owner;
            this.ProcessorClassName = String.Intern(processor.Name);
            this.Inputs = new HashSet<IContentNode>();
            this.Outputs = new HashSet<IContentNode>();
            this.AdditionalDependentContent = new HashSet<IContentNode>();
            this.Parameters = new Dictionary<String, Object>();
        }

        /// <summary>
        /// Constructor; copying from existing IProcess.
        /// </summary>
        /// <param name="process"></param>
        public ProcessBuilder(IProcess process)
        {
            if (null == process)
                throw new ArgumentNullException("Invalid process.", "process");

            this.Owner = process.Owner;
            this.ProcessorClassName = String.Intern(process.ProcessorClassName);
            this.Inputs = new HashSet<IContentNode>(process.Inputs);
            this.Outputs = new HashSet<IContentNode>(process.Outputs);
            this.AdditionalDependentContent = new HashSet<IContentNode>(
                process.AdditionalDependentContent);
            this.Parameters = new Dictionary<String, Object>(process.Parameters);
        }
        #endregion // Constructor(s)

        #region IHasParameters Interface Methods
        /// <summary>
        /// Determine whether the parameter exists.
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <returns></returns>
        public bool HasParameter(string name)
        {
            return (this.Parameters.ContainsKey(name));
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter<T>(string name, T value)
        {
            if (Parameters.ContainsKey(name))
            {
                Parameters[name] = value;
            }
            else
            {
                Parameters.Add(name, value);
            }
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter(string name, object value)
        {
            SetParameter<object>(name, value);
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public T GetParameter<T>(string name, T defaultValue)
        {
            // underlying type of the Parameter's value is still an object so we're constrained here
            object value;

            // No type checking here.
            // Because if the calling code is supplying the type and that's not what the parameter is, then the problem is not here
            // (aka "Embrace the InvalidCastException")
            if (Parameters.TryGetValue(name, out value))
            {
                return (T)value;
            }
            return defaultValue;
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public object GetParameter(string name, object defaultValue)
        {
            return GetParameter<object>(name, defaultValue);
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Return a concrete IProcess dependency-immutable object.
        /// </summary>
        /// <returns></returns>
        public IProcess ToProcess()
        {
            IProcess process = this.Owner.CreateProcess(this.ProcessorClassName,
                this.Inputs, this.Outputs, this.AdditionalDependentContent);
            foreach (KeyValuePair<String, Object> kvp in this.Parameters)
                process.Parameters.Add(kvp.Key, kvp.Value);
            return (process);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Content namespace
