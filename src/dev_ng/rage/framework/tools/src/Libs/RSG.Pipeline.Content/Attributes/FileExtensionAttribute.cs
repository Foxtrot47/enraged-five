﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content.Attributes
{

    /// <summary>
    /// 
    /// </summary>
    internal class FileExtensionAttribute : Attribute
    {
        #region Properties
        /// <summary>
        /// File extension string.
        /// </summary>
        public String[] Extensions
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extension"></param>
        public FileExtensionAttribute(String extension)
        {
            this.Extensions = new String[] { extension };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ext1"></param>
        /// <param name="ext2"></param>
        public FileExtensionAttribute(String ext1, String ext2)
        {
            this.Extensions = new String[] { ext1, ext2 };
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Return array of file extension Strings for a specified type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static String[] GetFileExtensionForType(Type type)
        {
            FileExtensionAttribute[] attributes =
                type.GetCustomAttributes(typeof(FileExtensionAttribute), false) as FileExtensionAttribute[];
            
            List<String> values = new List<String>();
            foreach (FileExtensionAttribute attr in attributes)
                values.AddRange(attr.Extensions);
            return (values.ToArray());
        }
    }

} // RSG.Pipeline.Content.Attributes namespace
