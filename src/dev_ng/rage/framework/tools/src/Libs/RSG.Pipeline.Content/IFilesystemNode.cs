﻿using System;
using System.Collections.Generic;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Filesystem content node interface; wrapping a filesystem object.
    /// </summary>
    /// All DateTime object interaction is done in UTC; this ensure assets are
    /// transferable across timezones without any adverse consequences.
    /// 
    public interface IFilesystemNode : IContentNode
    {
        #region Properties
        /// <summary>
        /// Absolute path to filesystem object.
        /// </summary>
        String AbsolutePath { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Modified time (UTC) of this filesystem object.
        /// </summary>
        DateTime GetModifiedTimestamp();

        /// <summary>
        /// Set modified time (UTC) of this filesystem object.
        /// </summary>
        /// <param name="value"></param>
        void SetModifiedTimestamp(DateTime value);

        /// <summary>
        /// Creation time (UTC) of this filesystem object.
        /// </summary>
        DateTime GetCreationTimestamp();

        /// <summary>
        /// Calculate and return the size (in bytes) of the filesystem data.
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        long GetSize();

        /// <summary>
        /// Determine if the filesystem node exists.
        /// </summary>
        /// <returns>true iff object exists; false otherwise</returns>
        bool Exists();

        /// <summary>
        /// Touch the filesystem object updating its Modified Time (UTC).
        /// </summary>
        /// <param name="when_utc">DateTime (UTC) object to update to.</param>
        /// <returns></returns>
        bool Touch(DateTime? when_utc = null);
        #endregion // Methods
    }

} // RSG.Pipeline.Content namespace
