﻿using System;

namespace RSG.Pipeline.Content
{


    /// <summary>
    /// Interface describing that a asset node is related to a particular 
    /// platform; e.g. PS3, Xenon, Win32, Win64.
    /// </summary>
    public interface IHasPlatform
    {
        /// <summary>
        /// Associated target platform information.
        /// </summary>
        RSG.Platform.Platform Platform { get; }
    }

} // RSG.Pipeline.Content namespace
