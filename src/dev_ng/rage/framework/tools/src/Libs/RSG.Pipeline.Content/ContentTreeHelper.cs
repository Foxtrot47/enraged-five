﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Wraps content tree queries.
    /// </summary>
#warning DHM FIX ME: comments for the controller methods would be nice.
    public class ContentTreeHelper
    {
        #region Constants
        /// <summary>
        /// Map Processor Constants.
        /// </summary>
        private const String Processor_MapPreProcess = "RSG.Pipeline.Processor.Map.PreProcess";
        private const String Processor_MapDummyCombine = "RSG.Pipeline.Processor.Map.DummyCombineProcessor";
        private const String Processor_MapDummyMetadataMerge = "RSG.Pipeline.Processor.Map.DummyMetadataMergeProcessor";
        private const String Processor_MapRejectionBounds = "RSG.Pipeline.Processor.Map.RejectionBoundsProcessor";
        private const String Processor_MapParentTextureDictionary = "RSG.Pipeline.Processor.Map.ParentTextureDictionary";

        /// <summary>
        /// Vehicle Processor Constants.
        /// </summary>
        private const String Processor_VehiclePreProcess = "RSG.Pipeline.Processor.Vehicle.PreProcess";

        /// <summary>
        /// 3dsmax Export Processor.
        /// </summary>
        private const String Processor_3dsmaxExport = "RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor";

        /// <summary>
        /// Container max type constants.
        /// </summary>
        private const String interiorContainerMaxType_ = "interior";
        private const String lodContainerMaxType_ = "lod_container";
        private const String mapContainerMaxType_ = "map_container";
        private const String rejectionContainerMaxType_ = "rejection_container";
        private const String occlusionContainerMaxType_ = "occl_container";
        private const String propContainerMaxType_ = "props";
        private const String instancePlacementType_ = "instance_placement";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated content-tree object.
        /// </summary>
        public IContentTree ContentTree
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; content-tree and processors required.
        /// </summary>
        /// <param name="contentTree"></param>
        /// <param name="processors"></param>
        public ContentTreeHelper(IContentTree contentTree)
        {
            if (contentTree == null)
                throw new ArgumentNullException("contentTree");

            this.ContentTree = contentTree;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Given a number of export nodes, returns the asset combine output they share, or null
        /// </summary>
        public IContentNode GetCombineOutputForNodes(IEnumerable<IContentNode> combineInputNodes)
        {
            foreach (IContentNode combineInputNode in combineInputNodes)
            {
                IProcess[] processesWithThisInput = this.ContentTree.FindProcessesWithInput(combineInputNode).ToArray();
                IProcess[] dummyCombineProcessesWithThisInput =
                    processesWithThisInput.Where(process => process.ProcessorClassName.Equals(Processor_MapDummyCombine)).ToArray();
                if (dummyCombineProcessesWithThisInput.Length == 1)
                {
                    Debug.Assert(dummyCombineProcessesWithThisInput[0].Outputs.Count() == 1);
                    return dummyCombineProcessesWithThisInput[0].Outputs.First();
                }
            }

            return null;
        }

        public IContentNode GetExportZipNodeFromMaxFileNode(IContentNode maxFileNode)
        {
            IEnumerable<IContentNode> outputNodes = GetAllOutputsFromNode(maxFileNode);
            return (outputNodes.OfType<Asset>().FirstOrDefault(n => n.AssetType.Equals(RSG.Platform.FileType.ZipArchive)));
        }

        public IContentNode GetExportZipNodeFromUnprocessedCollisionNode(IFilesystemNode unprocessedCollisionNode)
        {
            String unprocessedCollisionBasename = System.IO.Path.GetFileNameWithoutExtension(unprocessedCollisionNode.AbsolutePath);
            String targetBasename = unprocessedCollisionBasename.Replace("_collision", "");
            foreach (IProcess process in this.ContentTree.Processes.Where(proc => proc.ProcessorClassName.Equals(Processor_3dsmaxExport)))
            {
                foreach (IFilesystemNode node in process.Outputs.OfType<IFilesystemNode>())
                {
                    String nodeBasename = System.IO.Path.GetFileNameWithoutExtension(node.AbsolutePath);
                    if (String.Equals(nodeBasename, targetBasename, StringComparison.OrdinalIgnoreCase))
                        return node;
                }
            }

            return null;
        }

        public String GetBaseSceneXMLFromInstancePlacementFilename(String instancePlacementXML)
        {
            if (instancePlacementXML.Contains(instancePlacementType_))
            {
                String instancePlacementContainerName = System.IO.Path.GetFileNameWithoutExtension(instancePlacementXML);
                IContentNode[] sceneXMLNodes = GetAllSceneXmlNodesOfType("map_container");

                foreach (IContentNode contentNode in sceneXMLNodes)
                {
                    Content.File sceneXMLNode = contentNode as Content.File;

                    if (sceneXMLNode == null)
                        continue;

                    if (String.Equals(sceneXMLNode.Basename, instancePlacementContainerName, StringComparison.OrdinalIgnoreCase))
                    {
                        return sceneXMLNode.AbsolutePath;
                    }
                }
            }

            return instancePlacementXML;
        }

        public IContentNode GetSceneXmlNodeFromExportZipNode(IContentNode exportZipNode)
        {
            Debug.Assert(exportZipNode is Content.File, "Unexpected node type.");
            if (!(exportZipNode is Content.File))
                throw (new ArgumentException("Unexpected node type.", "exportZipNode"));

            String sceneXmlPathname = System.IO.Path.ChangeExtension(((Content.File)exportZipNode).AbsolutePath, "xml");
            IContentNode sceneXmlNode = this.ContentTree.CreateFile(sceneXmlPathname);

            return sceneXmlNode;
        }

        public IContentNode GetExportZipNodeFromSceneXmlNode(IContentNode sceneXmlNode)
        {
            String exportZipPathname = System.IO.Path.ChangeExtension(((Content.File)sceneXmlNode).AbsolutePath, "zip");
            IContentNode exportZipNode = this.ContentTree.CreateFile(exportZipPathname);

            return exportZipNode;
        }

        public IContentNode GetProcessedZipNodeFromExportZipNode(IContentNode exportZipNode)
        {
            // Todo Flo: Duplicate
            IEnumerable<IProcess> processesWithThisInput = ContentTree.FindProcessesWithInput(exportZipNode);
            IEnumerable<IProcess> mapPreProcessProcessesWithThisInput = processesWithThisInput.Where(process => process.ProcessorClassName.Equals(Processor_MapPreProcess));
            if (mapPreProcessProcessesWithThisInput.Count() == 1)
            {
                Debug.Assert(mapPreProcessProcessesWithThisInput.First().Outputs.Count() == 1);
                return mapPreProcessProcessesWithThisInput.First().Outputs.First();
            }

            return null;
        }

        public IContentNode[] GetSceneXmlNodesFromExportZipNodes(IEnumerable<IContentNode> exportZipNodes)
        {
            if (!exportZipNodes.Any())
                return new IContentNode[0];

            IContentNode[] sceneXmlNodes = exportZipNodes.OfType<File>()
                                              .Select(exportZipNode => Path.ChangeExtension(exportZipNode.AbsolutePath, "xml"))
                                              .Select(sceneXmlPathname => ContentTree.CreateFile(sceneXmlPathname))
                                              .ToArray();

            return sceneXmlNodes;
        }

        public IContentNode GetMetadataMergeNodeFromSceneXmlNodes(IEnumerable<IContentNode> sceneXmlNodes)
        {
            foreach (IContentNode sceneXmlNode in sceneXmlNodes)
            {
                // Todo Flo: Duplicate
                IEnumerable<IProcess> processesWithThisInput = this.ContentTree.FindProcessesWithInput(sceneXmlNode);
                IEnumerable<IProcess> metadataMergeProcessesWithThisInput = processesWithThisInput.Where(process => process.ProcessorClassName.Equals(Processor_MapDummyMetadataMerge));
                if (metadataMergeProcessesWithThisInput.Count() == 1)
                {
                    Debug.Assert(metadataMergeProcessesWithThisInput.First().Outputs.Count() == 1);
                    return metadataMergeProcessesWithThisInput.First().Outputs.First();
                }
            }

            return null;
        }

        public IContentNode GetRejectionBoundsNodeFromExportZipNode(IContentNode exportZipNode)
        {
            // Todo Flo: Duplicate
            IEnumerable<IProcess> processesWithThisInput = this.ContentTree.FindProcessesWithInput(exportZipNode);
            IEnumerable<IProcess> rejectionMapProcessesWithThisInput = processesWithThisInput.Where(process => process.ProcessorClassName.Equals(Processor_MapRejectionBounds));
            if (rejectionMapProcessesWithThisInput.Count() == 1)
            {
                Debug.Assert(rejectionMapProcessesWithThisInput.First().Outputs.Count() == 1);
                return rejectionMapProcessesWithThisInput.First().Outputs.First();
            }

            return null;
        }

        public IContentNode GetParentTXDFileNode()
        {
            IProcess process = this.ContentTree.Processes.FirstOrDefault(item => item.ProcessorClassName.Equals(Processor_MapParentTextureDictionary));
            return process.Inputs.FirstOrDefault();
        }

        public HashSet<IContentNode> GetAllInputsForNode(IContentNode contentNode)
        {
            HashSet<IContentNode> inputNodes = new HashSet<IContentNode>();
            
            IEnumerable<IProcess> processesWithThisOutput = this.ContentTree.FindProcessesWithOutput(contentNode);
            foreach (IProcess processWithThisOutput in processesWithThisOutput)
            {
                foreach (IContentNode inputNode in processWithThisOutput.Inputs)
                {
                    inputNodes.Add(inputNode);
                }
            }

            return inputNodes;
        }

        public HashSet<IContentNode> GetAllOutputsFromNode(IContentNode contentNode)
        {
            HashSet<IContentNode> outputNodes = new HashSet<IContentNode>();

            IEnumerable<IProcess> processesWithThisInput = this.ContentTree.FindProcessesWithInput(contentNode);
            foreach (IProcess processWithThisInput in processesWithThisInput)
            {
                foreach (IContentNode outputNode in processWithThisInput.Outputs)
                {
                    outputNodes.Add(outputNode);
                }
            }

            return outputNodes;
        }
        
        public HashSet<IContentNode> GetCombineInputsForNode(IContentNode combineOutputNode, IEnumerable<IContentNode> combineInputNodes)
        {
            HashSet<IContentNode> combineInputs = new HashSet<IContentNode>();

            IEnumerable<IProcess> processesWithThisOutput = this.ContentTree.FindProcessesWithOutput(combineOutputNode);
            foreach (IProcess processWithThisOutput in processesWithThisOutput)
            {
                foreach (IContentNode combineInput in processWithThisOutput.Inputs)
                {
                    if (combineInputNodes.Contains(combineInput))
                        combineInputs.Add(combineInput);
                }
            }

            return combineInputs;
        }

        public IProcess[] GetMapPreProcessCombineSiblings(IProcess process)
        {
            // Get all of the Map.PreProcess edges that share a combine process with process
            IContentNode combineZipNode = GetCombineOutputForNodes(process.Inputs);
            if (combineZipNode == null)
                return new IProcess[0];

            IEnumerable<IProcess> combineProcess = this.ContentTree.FindProcessesWithOutput(combineZipNode);
            Debug.Assert(combineProcess.Count() == 1);

            HashSet<IProcess> siblingProcesses = new HashSet<IProcess>();
            foreach (IContentNode combineInput in combineProcess.First().Inputs.Where(input => !process.Inputs.Contains(input)))
            {
                IEnumerable<IProcess> mapPreProcesses = this.ContentTree.FindProcessesWithInput(combineInput).Where(pro => pro.ProcessorClassName.Equals(Processor_MapPreProcess));
                if (mapPreProcesses.Count() == 1)
                    siblingProcesses.Add(mapPreProcesses.First());
            }

            return siblingProcesses.ToArray();
        }

        /// <summary>
        /// Return array of all map export nodes.
        /// </summary>
        /// <returns></returns>
#warning DHM FIX ME: badly named function - this returns Map Source nodes (.maxc files)
        public IContentNode[] GetAllMapExportNodes()
        {
            IEnumerable<IProcess> mapProcesses = this.ContentTree.Processes.Where(
                p => String.Equals(p.ProcessorClassName, Processor_3dsmaxExport));
            IEnumerable<IContentNode> mapExportNodes = mapProcesses.SelectMany(p => p.Inputs);

            return (mapExportNodes.ToArray());
        }

        /// <summary>
        /// Looks for the Vehicle Export Nodes in the processes, and retrieves them.
        /// </summary>
        /// <returns>All the vehicle export nodes.</returns>
        public IContentNode[] GetAllVehiclesExportNodes()
        {
            IEnumerable<IProcess> vehicleProcesses = ContentTree.Processes.Where(p => p.ProcessorClassName.Equals(
                Processor_VehiclePreProcess));
            IContentNode[] vehicleNodes = vehicleProcesses.SelectMany(p => p.Inputs).ToArray();

            return vehicleNodes;
        }
                
        /// <summary>
        /// Return array of all map export processes.
        /// </summary>
        /// <returns></returns>
        public IProcess[] GetAllMapExportProcesses()
        {
            IEnumerable<IProcess> mapProcesses = this.ContentTree.Processes.Where(
                p => p.ProcessorClassName.Equals(Processor_3dsmaxExport));
            return (mapProcesses.ToArray());
        }

        public IContentNode[] GetAllMapContainerMaxNodes()
        {
            return GetAllMaxNodesOfType(mapContainerMaxType_);
        }

        public IContentNode[] GetAllPropExportNodes()
        {
            return GetAllExportNodesOfType(propContainerMaxType_);
        }

        public IContentNode[] GetAllInteriorExportNodes()
        {
            return GetAllExportNodesOfType(interiorContainerMaxType_);
        }

        /// <summary>
        /// Return array of all SceneXml nodes.
        /// </summary>
        /// <returns></returns>
        public IContentNode[] GetAllMapSceneXmlNodes()
        {
            IEnumerable<IProcess> mapProcesses = this.ContentTree.Processes.Where(
               p => String.Equals(p.ProcessorClassName, Processor_3dsmaxExport));
            IEnumerable<IContentNode> mapExportNodes = mapProcesses.SelectMany(p => p.Outputs);

            IContentNode[] sceneXmlNodes = mapExportNodes.Select(exportNode => GetSceneXmlNodeFromExportZipNode(exportNode))
                                                         .Where(sceneXmlNode => sceneXmlNode != null)
                                                         .ToArray();

            return sceneXmlNodes;
        }

        public IContentNode[] GetAllPropSceneXmlNodes()
        {
            return GetAllSceneXmlNodesOfType(propContainerMaxType_);
        }

        public IContentNode[] GetAllInteriorSceneXmlNodes()
        {
            return GetAllSceneXmlNodesOfType(interiorContainerMaxType_);
        }

        public IContentNode[] GetAllLodSceneXmlNodes()
        {
            return GetAllSceneXmlNodesOfType("lod_container");
        }
        
        public IContentNode[] GetNeighbourLodSceneXmlNodes(IContentNode contentNode)
        {
            var sceneXml = contentNode as File;

            IContentNode[] collection = GetAllSceneXmlNodesOfType("lod_container");
            if (sceneXml == null)
                return collection;

            string path = Path.GetDirectoryName(sceneXml.AbsolutePath);

            List<IContentNode> neighbourLods = new List<IContentNode>();

            foreach (IContentNode node in collection)
            {
                var file = node as File;
                if (file != null)
                {
                    if (file.AbsolutePath.StartsWith(path, StringComparison.OrdinalIgnoreCase))
                    {
                        neighbourLods.Add(file);
                    }
                }
            }
            
            return neighbourLods.ToArray();
        }

        /// <summary>
        /// Retrieves the dependencies for a max file; covers local dependencies (as in DLC) as well as core dependencies if needed.
        /// Filters everything out to avoid duplicates
        /// Remove the direct export results of the given maxfile
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="maxFile"></param>
        /// <returns></returns>
        public IContentNode[] GetFilteredDependencies(IBranch branch, IContentNode maxFile)
        {
            IContentNode[] localDependencies = GetDependencies(maxFile);
            IEnumerable<IContentNode> coreDependencies = GetCoreDependencies(branch, maxFile);

            // get dependencies for "local" (can be core or DLC), and try to get a core equivalent (valid if we're in a DLC)
            List<IContentNode> filteredDependencies = new List<IContentNode>(localDependencies.Length + coreDependencies.Count());
            filteredDependencies.AddRange(localDependencies);
            filteredDependencies.AddRange(coreDependencies);

            // filter out the map itself, as we dont want to sync the files we're about to check out, or checked out in a previous attempt
            IContentNode export = GetExportZipNodeFromMaxFileNode(maxFile);
            filteredDependencies.Remove(export);
            
            IContentNode sceneXml = GetSceneXmlNodeFromExportZipNode(export);
            filteredDependencies.Remove(sceneXml);

            return filteredDependencies.Distinct(cn => ((File)cn).AbsolutePath).ToArray();
        }

        /// <summary>
        /// Returns the dependencies for a given max file
        /// </summary>
        /// <param name="maxFile"></param>
        /// <returns></returns>
        public IContentNode[] GetDependencies(IContentNode maxFile)
        {
            IContentNode export = GetExportZipNodeFromMaxFileNode(maxFile);
            IContentNode sceneXml = GetSceneXmlNodeFromExportZipNode(export);
            
            HashSet<IContentNode> dependencies = new HashSet<IContentNode>();
            
            IEnumerable<IProcess> exportProcesses = ContentTree.FindProcessesWithInput(export);
            IProcess combineProcess = exportProcesses.FirstOrDefault(p => p.ProcessorClassName.Equals(Processor_MapDummyCombine));

            if (combineProcess != null)
            {
                dependencies.AddRange(combineProcess.Inputs);
            }

            IEnumerable<IProcess> xmlProcesses = ContentTree.FindProcessesWithInput(sceneXml);
            IProcess metadataMergeProcess = xmlProcesses.FirstOrDefault(p => p.ProcessorClassName.Equals(Processor_MapDummyMetadataMerge));

            if (metadataMergeProcess != null)
            {
                dependencies.AddRange(metadataMergeProcess.Inputs);
            }
            
            IProcess xmlProcess = xmlProcesses.FirstOrDefault(p => p.ProcessorClassName.Equals(Processor_MapPreProcess));
            if (xmlProcess != null)
            {
                dependencies.AddRange(xmlProcess.Inputs);
            }

            return dependencies.ToArray();
        }

        /// <summary>
        /// Gets all Core Dependencies for a given branch.
        /// This also gets the metadata zip node for a given export node. This is required for DLC.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="maxFile"></param>
        /// <returns></returns>
        private IEnumerable<IContentNode> GetCoreDependencies(IBranch branch, IContentNode maxFile)
        {
            // if it's not a DLC, just return empty collection as the dependencies have already been grabbed
            if (!branch.Project.IsDLC)
            {
                return new IContentNode[0];
            }

            // getting the dependencies for the max file
            IContentNode[] coreDependencies = GetDependencies(maxFile);

            // then swap it with its core equivalent
            // we dont worry about duplicates here, as this will get filtered out by the caller
            for (int i = 0; i < coreDependencies.Length; i++)
            {
                coreDependencies[i] = GetCoreBranchEquivalent(branch, coreDependencies[i]);
            }

            // preallocating twice (one for core, one for metadata core) to avoid resizing overhead
            List<IContentNode> dependencies = new List<IContentNode>(coreDependencies.Length * 2);

            // now that all potential core equivalences have been evaluated, get the core metadata node for each export node
            // this is required for DLC as a dependency for ITYP serialisation.
            foreach (IContentNode coreDependency in coreDependencies)
            {
                dependencies.Add(coreDependency);
                IContentNode metadataNode = GetMetadataNode(coreDependency);
                if (metadataNode != null)
                {
                    dependencies.Add(metadataNode);
                }
            }

            return dependencies;
        }

        private IContentNode GetMetadataNode(IContentNode contentNode)
        {
            // Todo Flo: Duplicate
            IEnumerable<IProcess> metadataMergeProcessesWithThisInput = ContentTree.FindProcessesWithInput(contentNode, Processor_MapDummyMetadataMerge);
            
            if (metadataMergeProcessesWithThisInput.Count() == 1)
            {
                Debug.Assert(metadataMergeProcessesWithThisInput.First().Outputs.Count() == 1);
                return metadataMergeProcessesWithThisInput.First().Outputs.First();
            }

            return null;
        }

        public String GetSceneTypeForExportNode(IContentNode exportNode)
        {
            IProcess[] maxExportProcessesWithExportInput =
                this.ContentTree.FindProcessesWithOutput(exportNode, Processor_3dsmaxExport).ToArray();
            Debug.Assert(maxExportProcessesWithExportInput.Length == 1);
            Debug.Assert(maxExportProcessesWithExportInput[0].Inputs.Count() == 1);
            IContentNode maxFileNode = maxExportProcessesWithExportInput[0].Inputs.First();
            if (maxFileNode.Parameters.ContainsKey(Constants.ParamMap_3dsmaxType))
            {
                return maxFileNode.Parameters[Constants.ParamMap_3dsmaxType].ToString();
            }

            return String.Empty;
        }

        public bool ExportNodeIsLODContainer(IContentNode exportNode)
        {
            return ExportNodeHas3dsMaxType(exportNode, lodContainerMaxType_);
        }

        public bool ExportNodeIsMapContainer(IContentNode exportNode)
        {
            return ExportNodeHas3dsMaxType(exportNode, mapContainerMaxType_);
        }

        public bool ExportNodeIsOcclusionContainer(IContentNode exportNode)
        {
            return ExportNodeHas3dsMaxType(exportNode, occlusionContainerMaxType_);
        }

        public bool ExportNodeIsPropContainer(IContentNode exportNode)
        {
            return ExportNodeHas3dsMaxType(exportNode, propContainerMaxType_);
        }

        /// <summary>
        /// Wrapper around content-tree's CreateAsset method.
        /// </summary>
        /// <param name="pathname"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public IContentNode CreateAsset(String pathname, RSG.Platform.Platform platform)
        {
            return this.ContentTree.CreateAsset(pathname, platform);
        }

        /// <summary>
        /// Wrapper around content-tree's CreateFile method.
        /// </summary>
        /// <param name="pathname"></param>
        /// <returns></returns>
        public IContentNode CreateFile(String pathname)
        {
            return this.ContentTree.CreateFile(pathname);
        }

        /// <summary>
        /// Determine whether there is an equivalent node in the Core Branch; this
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="sourceNode"></param>
        /// <returns></returns>
        public IContentNode GetCoreBranchEquivalent(IBranch branch, IContentNode sourceNode)
        {
            Debug.Assert(branch.Project.IsDLC, "Branch is not owned by a DLC-project.  Internal error.");
            if (!branch.Project.IsDLC)
                throw (new ArgumentException("Branch is not owned by a DLC-project.  Internal error."));

            Debug.Assert(sourceNode is IFilesystemNode, "Content-node needs to be a IFilesystemNode.  Internal error.");
            if (!(sourceNode is IFilesystemNode))
                throw (new ArgumentException("Content-node needs to be a IFilesystemNode.  Internal error."));

            // Evaluate the DLC filename in the core-branch environment and see
            // if there is a node.
            IBranch coreBranch = branch.Project.Config.CoreProject.Branches[branch.Name];
            String dlcBranchFilename = ((IFilesystemNode)sourceNode).AbsolutePath;
            var reversedSubstPath = branch.Environment.ReverseSubst(dlcBranchFilename);
            String coreBranchFilename = coreBranch.Environment.Subst(reversedSubstPath);

            return (this.CreateFile(coreBranchFilename));
        }

        /// <summary>
        /// Tries to return a DLC equivalent for a FileSystem node passed
        /// We're assuming the sourceNode is from Core.
        /// If no DLC equivalent is found, we return the original one.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="sourceNode"></param>
        /// <returns></returns>
        public IContentNode TryGetDlcBranchEquivalent(IBranch branch, IContentNode sourceNode)
        {
            Debug.Assert(sourceNode is IFilesystemNode, "Content-node needs to be a IFilesystemNode.  Internal error.");
            if (!(sourceNode is IFilesystemNode))
                throw (new ArgumentException("Content-node needs to be a IFilesystemNode.  Internal error."));

            IContentNode node = sourceNode;

            if (branch.Project.IsDLC)
            {
                // Evaluate the DLC filename in the core-branch environment and see
                // if there is a node.
                IBranch coreBranch = branch.Project.Config.CoreProject.Branches[branch.Name];
                string sourcePath = ((IFilesystemNode)sourceNode).AbsolutePath;
                string coreReversedFilename = coreBranch.Environment.ReverseSubst(sourcePath);
                string dlcBranchFilename = branch.Environment.Subst(coreReversedFilename);

                if (System.IO.File.Exists(dlcBranchFilename))
                    node = CreateFile(dlcBranchFilename);
            }

            return node;
        }

        /// <summary>
        /// Gets the input leaves for an enumerable of outputs excluding max files
        /// - hence is used only by assetbuilder for handling delets
        /// </summary>
        /// <param name="fromOutputs"></param>
        /// <param name="leavesFound">this is populated with the leaves as we find them</param>
        /// <returns>the nodes found for purposes of recursion</returns>
        /// 
        public IEnumerable<IContentNode> GetLeavesInputsForAssetBuilderDeletes(IEnumerable<IContentNode> fromOutputs, ICollection<IContentNode> leaves)
        {
            List<IContentNode> nodes = new List<IContentNode>();

            foreach (IContentNode output in fromOutputs)
            {
                // Note how we do not consider .max files as leaves.
                IEnumerable<IProcess> processesWithOutput = ContentTree.FindProcessesWithOutput(output).Where(p => p.ProcessorClassName != Processor_3dsmaxExport);
                if (processesWithOutput.Any())
                {
                    IEnumerable<IContentNode> inputs = processesWithOutput.SelectMany(p => p.Inputs).ToList(); // <- to list added to prevent delayed execution for sanity when debugging.

                    foreach (IContentNode input in inputs)
                    {
                        IEnumerable<IContentNode> nodesBeyond = GetLeavesInputsForAssetBuilderDeletes(new List<IContentNode> { input }, leaves);
                        if (!nodesBeyond.Any())
                        {
                            // no further nodes found so this must be a leaf
                            leaves.Add(input);

                            // since it is a leaf let's return that.
                            nodes.Add(input);
                        }
                        else
                        {
                            // we found further nodes so make sure these are returned
                            // but this input is NOT a leaf
                            nodes.AddRange(nodesBeyond);
                        }
                    }
                }
            }

            return nodes;
        }

        public IEnumerable<IContentNode> GetMetadataNodes()
        {
            return ContentTree.FindProcessesOfType(Processor_MapDummyMetadataMerge).SelectMany(output => output.Outputs);
        }
        #endregion // Controller Methods

        #region Private Methods
        private bool ExportNodeHas3dsMaxType(IContentNode exportNode, String type)
        {
            IEnumerable<IProcess> exportProcessesWithCombineInput = ContentTree.FindProcessesWithOutput(exportNode, Processor_3dsmaxExport);
            Debug.Assert(exportProcessesWithCombineInput.Count() == 1);
            Debug.Assert(exportProcessesWithCombineInput.First().Inputs.Count() == 1);
            IContentNode maxFileNode = exportProcessesWithCombineInput.First().Inputs.First();

            //return type == maxFileNode.GetParameter(Constants.ParamMap_3dsmaxType, "");
            return (maxFileNode.Parameters.ContainsKey(Constants.ParamMap_3dsmaxType) &&
                    maxFileNode.Parameters[Constants.ParamMap_3dsmaxType].ToString() == type);
        }

        /// <summary>
        /// Note: this returns the ZIP nodes only.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private IContentNode[] GetAllExportNodesOfType(String type)
        {
            List<IContentNode> exportNodes = new List<IContentNode>();

            var maxExportProcesses = this.ContentTree.Processes.Where(process => process.ProcessorClassName.Equals(Processor_3dsmaxExport));
            foreach (IProcess maxExportProcess in maxExportProcesses)
            {
                Debug.Assert(maxExportProcess.Inputs.Count() == 1, "MaxExportProcess have more than one input.");
                IContentNode maxFileNode = maxExportProcess.Inputs.First();

                string maxFileNodeType = maxFileNode.GetParameter(Constants.ParamMap_3dsmaxType, "");
                //if (maxFileNode.Parameters.ContainsKey(Constants.ParamMap_3dsmaxType) &&
                //   maxFileNode.Parameters[Constants.ParamMap_3dsmaxType].ToString() == type)
                if(maxFileNodeType == type)
                {
                    IContentNode exportNode = maxExportProcess.Outputs.
                        OfType<Asset>().
                        First(n => n.AssetType == Platform.FileType.ZipArchive);

                    Debug.Assert(exportNode != null, "ExportNode should not be null.");
                    exportNodes.Add(exportNode);
                }
            }

            return exportNodes.ToArray();
        }

        private IContentNode[] GetAllMaxNodesOfType(String type)
        {
            List<IContentNode> maxNodes = new List<IContentNode>();

            IProcess[] maxExportProcesses = this.ContentTree.Processes.Where(process => process.ProcessorClassName.Equals(Processor_3dsmaxExport)).ToArray();
            foreach (IProcess maxExportProcess in maxExportProcesses)
            {
                Debug.Assert(maxExportProcess.Inputs.Count() == 1);
                IContentNode maxFileNode = maxExportProcess.Inputs.First();
                if (maxFileNode.Parameters.ContainsKey(Constants.ParamMap_3dsmaxType) &&
                   maxFileNode.Parameters[Constants.ParamMap_3dsmaxType].ToString() == type)
                {
                    maxNodes.Add(maxFileNode);
                }
            }

            return maxNodes.ToArray();
        }

        private IContentNode[] GetAllSceneXmlNodesOfType(String type)
        {
            IContentNode[] exportNodes = GetAllExportNodesOfType(type); 
            IContentNode[] sceneXmlNodes = exportNodes.Where(sceneXmlNode => sceneXmlNode != null).Select(GetSceneXmlNodeFromExportZipNode).ToArray();

            return sceneXmlNodes;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Content namespace
