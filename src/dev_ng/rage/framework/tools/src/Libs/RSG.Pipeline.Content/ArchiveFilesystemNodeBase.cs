﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using RSG.Base.IO;
using RSG.Pipeline.Core;
using RSG.Platform;
using Ionic.Zip;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Abstract base class for all archive nodes.
    /// </summary>
    public abstract class ArchiveFilesystemNodeBase :
        NodeBase,
        IArchiveFilesystemNode
    {
        #region Constants
        /// <summary>
        /// Combined path separator string.
        /// </summary>
        public static readonly String COMBINED_PATH_SEPARATOR_STR = "|";

        /// <summary>
        /// Combined path separator character.
        /// </summary>
        public static readonly Char COMBINED_PATH_SEPARATOR_CHAR = '|';
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Absolute path to archive file.
        /// </summary>
        public String ArchiveAbsolutePath
        {
            get;
            private set;
        }

        /// <summary>
        /// Type of archive.
        /// </summary>
        public FileType ArchiveType
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Relative file paths from the split stack of combined paths.
        /// </summary>
        protected Stack<String> m_RelativeFilePaths;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="combinedPath"></param>
        internal ArchiveFilesystemNodeBase(IContentTree owner, String combinedPath)
            : this(owner, Guid.NewGuid(), combinedPath)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="combinedPath"></param>
        internal ArchiveFilesystemNodeBase(IContentTree owner, Guid id, String combinedPath)
            : base(owner, id, combinedPath)
        {
            String[] split = this.SplitAndVerifyCombinedPath(combinedPath);
            this.ArchiveAbsolutePath = split[0];
            this.m_RelativeFilePaths = new Stack<String>();
            for (int n = 1; n < split.Length; ++n)
                this.m_RelativeFilePaths.Push(split[n]);

            Debug.Assert(this.ArchiveAbsolutePath.EndsWith(".zip"));
            if (!this.ArchiveAbsolutePath.EndsWith(".zip"))
                throw (new NotSupportedException("Illegal archive type!"));

            this.ArchiveType = FileTypeUtils.ConvertExtensionToFileType(
                Path.GetExtension(this.ArchiveAbsolutePath));
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Modified time (UTC) of this archive filesystem object.
        /// </summary>
        public abstract DateTime GetModifiedTimestamp();

        /// <summary>
        /// Set modified time (UTC) of this archive filesystem object.
        /// </summary>
        /// <param name="value"></param>
        public abstract void SetModifiedTimestamp(DateTime value);

        /// <summary>
        /// Creation time (UTC) of this archive filesystem object.
        /// </summary>
        public abstract DateTime GetCreationTimestamp();

        /// <summary>
        /// Calculate and return the size (in bytes) of the archive filesystem 
        /// data (uncompressed size).
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        public abstract long GetSize();

        /// <summary>
        /// Determine if the archive filesystem node exists.
        /// </summary>
        /// <returns>true iff object exists; false otherwise</returns>
        public abstract bool Exists();
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Split and verify a combined path string.
        /// </summary>
        /// <param name="combinedPath"></param>
        /// <returns></returns>
        protected String[] SplitAndVerifyCombinedPath(String combinedPath)
        {
            Debug.Assert(combinedPath.Contains(COMBINED_PATH_SEPARATOR_STR),
                String.Format("Illegal combined path.  No combined path separator character ('{0}').",
                COMBINED_PATH_SEPARATOR_STR));
            if (!combinedPath.Contains(COMBINED_PATH_SEPARATOR_STR))
                throw (new NotSupportedException(
                    String.Format("Illegal combined path.  No combined path separator character ('{0}').",
                    COMBINED_PATH_SEPARATOR_STR)));

            String[] split = combinedPath.Split(new Char[] { COMBINED_PATH_SEPARATOR_CHAR });
            Debug.Assert(split.Length >= 1,
                String.Format("Invalid number of separator characters ('{0}').",
                COMBINED_PATH_SEPARATOR_STR));
            if (split.Length < 1)
                throw (new NotSupportedException(
                    String.Format("Invalid number of separator characters ('{0}').",
                    COMBINED_PATH_SEPARATOR_STR)));
            return (split);
        }

        /// <summary>
        /// Recursively fetch the ZipEntry as required (null if not found).
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        /// <see cref="RSG.Pipeline.Content.ArchiveFile"/>
        /// <see cref="RSG.Pipeline.Content.ArchiveDirectory"/>
        /// Note: only useful for recursively referenced Zip archive files (e.g.
        /// 'RSG.Pipeline.Content.ArchiveFile' and 'RSG.Pipeline.Content.ArchivedDirectory').
        /// 
        protected ZipEntry GetZipEntryRecursive(ZipFile zf, IEnumerable<String> paths)
        {
            String nextPath = paths.First();
            IEnumerable<String> remainingPaths = paths.Skip(1);

            if (remainingPaths.Any() && nextPath.EndsWith(".zip"))
            {
                if (!zf.ContainsEntry(nextPath))
                    return (null);

                // Recurse as we have a zip; and further file entries.
                ZipEntry nextEntry = zf[nextPath];
                using (MemoryStream ms = new MemoryStream((int)nextEntry.UncompressedSize))
                {
                    nextEntry.Extract(ms);
                    using (ZipFile zzf = ZipFile.Read(ms))
                    {
                        return (GetZipEntryRecursive(zzf, remainingPaths));
                    }
                }
            }
            else if (!remainingPaths.Any())
            {
                // Base recursion case; resolve to entry if available.
                if (!zf.ContainsEntry(nextPath))
                    return (null);

                return (zf[nextPath]);
            }
            else
            {
                // Error case; more paths to mount but unsupported archive.
                throw (new NotSupportedException("Invalid archive found!"));
            }
        }

        /// <summary>
        /// Recursively fetch the ZipEntry objects as required (empty if not found).
        /// </summary>
        /// <param name="zf"></param>
        /// <param name="paths"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        protected IEnumerable<ZipEntry> GetZipEntriesRecursive(ZipFile zf, 
            IEnumerable<String> paths, String wildcard, bool recursive)
        {
            String nextPath = paths.First();
            IEnumerable<String> remainingPaths = paths.Skip(1);

            if (recursive && remainingPaths.Any() && nextPath.EndsWith(".zip"))
            {
                if (!zf.ContainsEntry(nextPath))
                    return (null);

                // Recurse as we have a zip; and further file entries.
                ZipEntry nextEntry = zf[nextPath];
                using (MemoryStream ms = new MemoryStream((int)nextEntry.UncompressedSize))
                {
                    nextEntry.Extract(ms);
                    using (ZipFile zzf = ZipFile.Read(ms))
                    {
                        return (GetZipEntriesRecursive(zzf, remainingPaths,
                            wildcard, recursive));
                    }
                }
            }
            else if (!remainingPaths.Any())
            {
                // Base recursion case; resolve to wildcard entries if available.
                Wildcard wc = new Wildcard(wildcard, RegexOptions.IgnoreCase);
                IEnumerable<ZipEntry> entries = zf.Entries.Where(entry =>
                    entry.FileName.StartsWith(nextPath) &&
                    wc.IsMatch(entry.FileName));
                return (entries);
            }
            else
            {
                // Error case; more paths to mount but unsupported archive.
                throw (new NotSupportedException("Invalid archive found!"));
            }
        }
        #endregion // Protected Methods
    }

} // RSG.Pipeline.Content
