﻿using System;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using NUnit.Framework;
using RSG.Base.Logging;
using RSG.GraphML;
using RSG.Pipeline.Core;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;

namespace RSG.Pipeline.Content.Test
{

    /// <summary>
    /// 
    /// </summary>
    [TestFixture]
    public class ContentTreeLoadTest
    {
        #region NUnit Testcase Setup
        /// <summary>
        /// Unit test initialisation.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Debug.Listeners.Clear();
            ConsoleLog log = new ConsoleLog();
        }
        #endregion // NUnit Testcase Setup

        /// <summary>
        /// Attempts to load the content-tree just using the GraphML library.
        /// This shows compatibility with GraphML.
        /// </summary>
        [Test]
        public void TestContentTreeLoadAsGraph()
        {
            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;
            String filename = SIO.Path.Combine(config.ToolsConfig, "content", "content.xml");
            Assert.That(SIO.File.Exists(filename));

            GraphCollection graphs = new GraphCollection(filename);
            Assert.That(graphs.Count > 0);
            Assert.That(graphs.Attributes.Count > 0); // From XInclude'd data

            IProcessorCollection processors = ProcessorCollection.Create(config);
            IContentTree tree = Factory.CreateTree(branch, graphs, processors);
            Assert.That(tree.Processes.Count() > 0);
        }
    }

} // RSG.Pipeline.Content.Test namespace
