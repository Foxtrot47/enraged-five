﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content.Attributes;
using RSG.Platform;

namespace RSG.Pipeline.Content
{
    
    /// <summary>
    /// Archive filesystem node interface; wrapping an archive file and some
    /// embedded filesystem object(s).
    /// </summary>
    public interface IArchiveFilesystemNode : IContentNode
    {
        #region Properties
        /// <summary>
        /// Absolute path to archive file.
        /// </summary>
        String ArchiveAbsolutePath { get; }

        /// <summary>
        /// Type of archive.
        /// </summary>
        FileType ArchiveType { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Modified time (UTC) of this archive filesystem object.
        /// </summary>
        DateTime GetModifiedTimestamp();

        /// <summary>
        /// Set modified time (UTC) of this archive filesystem object.
        /// </summary>
        /// <param name="value"></param>
        void SetModifiedTimestamp(DateTime value);

        /// <summary>
        /// Creation time (UTC) of this archive filesystem object.
        /// </summary>
        DateTime GetCreationTimestamp();

        /// <summary>
        /// Calculate and return the size (in bytes) of the archive filesystem 
        /// data (uncompressed size).
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        long GetSize();

        /// <summary>
        /// Determine if the archive filesystem node exists.
        /// </summary>
        /// <returns>true iff object exists; false otherwise</returns>
        bool Exists();
        #endregion // Methods
    }

} // RSG.Pipeline.Content namespace
