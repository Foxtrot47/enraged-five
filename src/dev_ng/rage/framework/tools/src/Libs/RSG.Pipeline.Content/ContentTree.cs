﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.IO;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Platform;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// The core content-tree interface; representing a set of assets and their
    /// transitions (processes) in the build pipeline.
    /// </summary>
    /// All content-nodes that are part of an instance of a content-tree must
    /// be created using the CreateFile and CreateDirectory methods.
    public class ContentTree : IContentTree
    {
        #region Constants
        /// <summary>
        /// Log context constant.
        /// </summary>
        private static readonly String LOG_CTX = "Content-Tree";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated project branch; link to pipeline configuration data.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Enumerable of IProcess objects; the edges between nodes.
        /// </summary>
        public IEnumerable<IProcess> Processes
        {
            get { return m_Processes; }
        }
        private ICollection<IProcess> m_Processes;

        /// <summary>
        /// Enumerable of IContentNode objects; all of the nodes in the graph.
        /// </summary>
        public IEnumerable<IContentNode> Nodes
        {
            get { return (this.FilenameMapping.Values); }
        }

        /// <summary>
        /// Content-tree log object.
        /// </summary>
        internal static IUniversalLog Log
        {
            get;
            private set;
        }

        /// <summary>
        /// Private dictionary mapping filenames to Content Nodes.
        /// </summary>
        private IDictionary<String, IContentNode> FilenameMapping
        {
            get;
            set;
        }

        /// <summary>
        /// Private dictionary mapping directories to Content Nodes.
        /// </summary>
        private IDictionary<String, ICollection<IContentNode>> DirectoryMapping
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        internal ContentTree(IBranch branch)
        {
            this.Branch = branch;
            this.m_Processes = new List<IProcess>();
            this.FilenameMapping = new Dictionary<String, IContentNode>();
            this.DirectoryMapping = new Dictionary<String, ICollection<IContentNode>>();
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static ContentTree()
        {
            LogFactory.Initialize();
            ContentTree.Log = LogFactory.CreateUniversalLog("RSG.Pipeline.Content");
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return whether a path is a File content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsFile(String path)
        {
            bool isFile = false;
            String normalisedPath = ExpandAndNormalise(path);
            if (this.FilenameMapping.ContainsKey(normalisedPath))
            {
                isFile = (this.FilenameMapping[normalisedPath] is Content.File);
            }
            return (isFile);
        }

        /// <summary>
        /// Return whether a path is a Asset content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsAsset(String path)
        {
            bool isFile = false;
            String normalisedPath = ExpandAndNormalise(path);
            if (this.FilenameMapping.ContainsKey(normalisedPath))
            {
                isFile = (this.FilenameMapping[normalisedPath] is Content.Asset);
            }
            return (isFile);
        }

        /// <summary>
        /// Return whether a path is a Directory content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsDirectory(String path)
        {
            bool isFile = false;
            String normalisedPath = ExpandAndNormalise(path);
            if (this.DirectoryMapping.ContainsKey(normalisedPath))
            {
                isFile = true;
            }
            return (isFile);
        }       

        /// <summary>
        /// Return whether a path has a parent Directory content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool HasParentDirectory(String path)
        {
            String normalisedPath = ExpandAndNormalise(path);
            foreach (String np in this.DirectoryMapping.Keys)
            {
                if (normalisedPath.StartsWith(np + "\\"))
                    return (IsDirectory(np));
            }
            return (false);
        }

        /// <summary>
        /// Will use the wildcard from the parent directories and try to find
        /// a match to the filename. Will return null if we dont find a match.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IContentNode GetParentDirectoryFromWildcard(String path)
        {
            String normalisedPath = ExpandAndNormalise(path);
            String filename = Path.GetFileName(normalisedPath);
            IEnumerable<IContentNode> parentDirectories = GetParentDirectories(path);
            foreach (IContentNode node in parentDirectories)
            {
                if (node is RegexDirectory)
                {
                    // We already have verified the directory path by using 
                    // GetParentDirectories above; now we just check the filename.
                    RegexDirectory regdir = (RegexDirectory)node;
                    Wildcard wildcard = new Wildcard(regdir.Wildcard, RegexOptions.IgnoreCase);
                    if (wildcard.IsMatch(filename) && regdir.Expression.IsMatch(filename))
                        return regdir;
                }
                else if (node is Directory)
                {
                    Directory dir = (Directory)node;

                    // We already have verified the directory path by using 
                    // GetParentDirectories above; now we just check the filename.
                    Wildcard wildcard = new Wildcard(dir.Wildcard, RegexOptions.IgnoreCase);
                    if (wildcard.IsMatch(filename))
                        return dir;
                }
                // Otherwise ignored; saves null checking.
            }
            return (null);
        }

        /// <summary>
        /// Return Directory content-node if it exists (null otherwise).
        /// </summary>
        /// <param name="path"></param>
        /// <param name="wildcard"></param>
        /// <returns></returns>
        public IContentNode GetParentDirectory(String path, String wildcard)
        {
            String normalisedPath = ExpandAndNormalise(path);
            foreach (String np in this.DirectoryMapping.Keys)
            {
                if (normalisedPath.StartsWith(np + "\\"))
                    return (this.DirectoryMapping[np].OfType<Directory>().FirstOrDefault(d => wildcard.Equals(d.Wildcard)));
            }
            return (null);
        }

        /// <summary>
        /// Return all Directory content-nodes if it exists (empty IEnumerable otherwise).
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<IContentNode> GetParentDirectories(String path)
        {
            List<IContentNode> directories = new List<IContentNode>();
            String normalisedPath = ExpandAndNormalise(path);
            foreach (String np in this.DirectoryMapping.Keys)
            {
                if (normalisedPath.StartsWith(np + "\\"))
                    directories.AddRange(this.DirectoryMapping[np]);
            }
            return (directories);
        }

        /// <summary>
        /// Return whether a path is an ArchiveFile content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsArchiveFile(String path)
        {
            bool isFile = false;
            String normalisedPath = ExpandAndNormalise(path);
            if (this.FilenameMapping.ContainsKey(normalisedPath))
            {
                isFile = (this.FilenameMapping[normalisedPath] is Content.ArchiveFile);
            }
            return (isFile);
        }

        /// <summary>
        /// Return whether a path is an ArchiveDirectory content-node.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsArchiveDirectory(String path)
        {
            bool isFile = false;
            String normalisedPath = ExpandAndNormalise(path);
            if (this.FilenameMapping.ContainsKey(normalisedPath))
            {
                isFile = (this.FilenameMapping[normalisedPath] is Content.ArchiveDirectory);
            }
            return (isFile);
        }

        /// <summary>
        /// CreateEmptyTree a file-based content-node for filename; may return a cached
        /// node if its already part of this content tree.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public IContentNode CreateFile(String filename)
        {
            IContentNode content = null;
            String normalisedFilename = ExpandAndNormalise(filename);

            if (this.FilenameMapping.ContainsKey(normalisedFilename))
            {
                content = this.FilenameMapping[normalisedFilename];
                Debug.Assert(!content.Parameters.ContainsKey(Constants.Removed_Content),
                    "Content node for \"{0}\" has been marked as having been removed from the content tree",
                    (content as IFilesystemNode).AbsolutePath);
            }
            else
            {
                // Prior to defaulting to generic 'File' node type; lets see
                // if we have an Asset.
                String ext = Path.GetExtension(filename);
                FileType ft = FileTypeUtils.ConvertExtensionToFileType(ext);
                if (FileType.Unknown != ft)
                {
                    content = new Asset(this, normalisedFilename);
                }
#warning DHM FIX ME: return an asset if its under a branch resource path.
                else
                {
                    // Default to generic File node.
                    content = new File(this, normalisedFilename);
                }
                this.FilenameMapping[normalisedFilename] = content;
            }
            return (content);
        }

        /// <summary>
        /// Create an Asset content-node for filename and platform; may return a
        /// cached node if its already part of this content.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        /// 
        public IContentNode CreateAsset(String filename, RSG.Platform.Platform platform)
        {
            IContentNode content = null;
            String normalisedFilename = ExpandAndNormalise(filename);

            if (this.FilenameMapping.ContainsKey(normalisedFilename))
                content = this.FilenameMapping[normalisedFilename];
            else
            {
                // Prior to defaulting to generic 'File' node type; lets see
                // if we have an Asset.
                String ext = Path.GetExtension(filename);
                FileType ft = FileTypeUtils.ConvertExtensionToFileType(ext);
                content = new Asset(this, Guid.NewGuid(), normalisedFilename, platform);
                this.FilenameMapping[normalisedFilename] = content;
                Debug.Assert(!content.Parameters.ContainsKey(Constants.Removed_Content),
                    "Content node for \"{0}\" has been marked as having been removed from the content tree",
                    (content as IFilesystemNode).AbsolutePath);
            }
            return (content);
        }

        /// <summary>
        /// CreateEmptyTree a directory-based content node; may return a cached node if
        /// its already part of this content tree.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public IContentNode CreateDirectory(String directory)
        {
            return (CreateDirectory(directory, "*.*", false));
        }

        /// <summary>
        /// Create a directory-based content node; may return a cached node if
        /// its already part of this content tree.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public IContentNode CreateDirectory(String directory, String wildcard, bool recursive)
        {
            IContentNode content = null;
            String normalisedDirectory = ExpandAndNormalise(directory);

            if (this.DirectoryMapping.ContainsKey(normalisedDirectory))
            {
                List<IContentNode> contentNodeList = this.DirectoryMapping[normalisedDirectory] as List<IContentNode>;

                content = contentNodeList.FirstOrDefault(c => (c as Directory).Wildcard.Equals(wildcard));
                if (content == null)
                {
                    content = new Directory(this, normalisedDirectory, wildcard, recursive);
                    contentNodeList.Add(content);
                }

                Debug.Assert(!content.Parameters.ContainsKey(Constants.Removed_Content),
                    "Content node for \"{0}\" has been marked as having been removed from the content tree",
                    (content as IFilesystemNode).AbsolutePath);
            }
            else
            {
                content = new Directory(this, normalisedDirectory, wildcard, recursive);
                List<IContentNode> contentNodeList = new List<IContentNode>();
                contentNodeList.Add(content);

                this.DirectoryMapping[normalisedDirectory] = contentNodeList;
            }
            return (content);
        }

        /// <summary>
        /// Get a list of all the directories associated with a specified directory in the content-tree.
        /// We can have a base directory with multiple wildcards so return all of them.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public IEnumerable<IContentNode> GetDirectories(String directory)
        {
            String normalisedDirectory = ExpandAndNormalise(directory);

            if (this.DirectoryMapping.ContainsKey(normalisedDirectory))
            {
                return this.DirectoryMapping[normalisedDirectory];
            }

            return null;
        }

        /// <summary>
        /// Create a static directory-based content node; may return a cached 
        /// node if its already part of this content tree.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public IContentNode CreateStaticDirectory(String directory)
        {
            IContentNode content = null;
            String normalisedDirectory = ExpandAndNormalise(directory);
            //if (this.FilenameMapping.ContainsKey(normalisedDirectory))
            //{
            //    Debug.Assert(this.FilenameMapping[normalisedDirectory] is Content.StaticDirectory);
            //    content = this.FilenameMapping[normalisedDirectory];
            //    Debug.Assert(!content.Parameters.ContainsKey(Constants.Removed_Content), 
            //        "Content node for \"{0}\" has been marked as having been removed from the content tree", 
            //        (content as IFilesystemNode).AbsolutePath);
            //}
            //else
            {
                content = new StaticDirectory(this, normalisedDirectory);
                if (this.DirectoryMapping.ContainsKey(normalisedDirectory))
                {
                    this.DirectoryMapping[normalisedDirectory].Add(content);
                }
                else
                {
                    ICollection<IContentNode> directories = new List<IContentNode>();
                    directories.Add(content);
                    this.DirectoryMapping.Add(normalisedDirectory, directories);
                }
            }
            return (content);
        }

        /// <summary>
        /// Create a static directory from a directory node and replace existing references in FileMapping and existing processes.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public IContentNode CreateStaticDirectoryFromDirectory(IContentNode directory)
        {
            Debug.Assert(directory is Directory);
            IContentNode staticDirectory = new StaticDirectory(directory as Directory);  
            
            // Replace the directory content with our new static directory content in any existing processes inputs and outputs
            foreach(IProcess process in this.Processes)
            {
                if (process.Inputs.Contains(directory))
                {
                    HashSet<IContentNode> inputs = process.Inputs as HashSet<IContentNode>;
                    inputs.Remove(directory);
                    inputs.Add(staticDirectory);
                }

                if (process.Outputs.Contains(directory))
                {
                    HashSet<IContentNode> outputs = process.Outputs as HashSet<IContentNode>;
                    outputs.Remove(directory);
                    outputs.Add(staticDirectory);
                }
            }

            // DHM FIX ME: this should not be required!
            directory.Parameters.Add(Constants.Removed_Content, true);

            // DHM FIX ME: with our new Directory mapping do we need to replace?
            // Replace the directory content with our new static directory content in the main file mapping. 
            String path = (staticDirectory as StaticDirectory).AbsolutePath;
            ICollection<IContentNode> directories = new List<IContentNode>();
            directories.Add(staticDirectory);
            this.DirectoryMapping.Remove(path);
            this.DirectoryMapping.Add(path, directories);

            return (staticDirectory);
        }

        /// <summary>
        /// Create a regular-expression directory-based content node; may return a 
        /// cached node if its already part of this content tree.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="regex"></param>
        /// <returns></returns>
        public IContentNode CreateRegexDirectory(String path, String wildcard, String regex, bool recursive)
        {
            IContentNode content = null;
            String normalisedDirectory = ExpandAndNormalise(path);

            if (this.DirectoryMapping.ContainsKey(normalisedDirectory))
            {
                List<IContentNode> contentNodeList = this.DirectoryMapping[normalisedDirectory] as List<IContentNode>;

                content = contentNodeList.FirstOrDefault(c => (c as RegexDirectory).Expression.ToString().Equals(regex));
                if (content == null)
                {
                    content = new RegexDirectory(this, normalisedDirectory, wildcard, regex, recursive);
                    contentNodeList.Add(content);
                }

                Debug.Assert(!content.HasParameter(Constants.Removed_Content),
                    "Content node for \"{0}\" has been marked as having been removed from the content tree",
                    (content as IFilesystemNode).AbsolutePath);
            }
            else
            {
                content = new RegexDirectory(this, normalisedDirectory, wildcard, regex, recursive);
                List<IContentNode> contentNodeList = new List<IContentNode>();
                contentNodeList.Add(content);

                this.DirectoryMapping[normalisedDirectory] = contentNodeList;
            }
            return (content);
        }

        /// <summary>
        /// Create an archive file content-node; may return a cached node if its
        /// already part of this content tree.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IContentNode CreateArchiveFile(String path)
        {
            IContentNode content = null;
            String normalisedFilename = ExpandAndNormalise(path);

            if (this.FilenameMapping.ContainsKey(normalisedFilename))
            {
                content = this.FilenameMapping[normalisedFilename];
                Debug.Assert(!content.Parameters.ContainsKey(Constants.Removed_Content),
                    "Content node for \"{0}\" has been marked as having been removed from the content tree",
                    (content as IFilesystemNode).AbsolutePath);
            }
            else
            {
                content = new ArchiveFile(this, path);
                this.FilenameMapping[normalisedFilename] = content;
            }
            return (content);
        }

        /// <summary>
        /// Create an archive file content-node; may return a cached node if its
        /// already part of this content tree.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public IContentNode CreateArchiveFile(IEnumerable<String> paths)
        {
            String combinedPath = String.Join(
                ArchiveFilesystemNodeBase.COMBINED_PATH_SEPARATOR_STR, paths);
            return (CreateArchiveFile(combinedPath));
        }

        /// <summary>
        /// Create an archive directory content-node; may return a cached node 
        /// if its already part of this content tree.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IContentNode CreateArchiveDirectory(String path)
        {
            IContentNode content = null;
            String normalisedFilename = ExpandAndNormalise(path);

            if (this.FilenameMapping.ContainsKey(normalisedFilename))
            {
                content = this.FilenameMapping[normalisedFilename];
                Debug.Assert(!content.Parameters.ContainsKey(Constants.Removed_Content),
                    "Content node for \"{0}\" has been marked as having been removed from the content tree",
                    (content as IFilesystemNode).AbsolutePath);
            }
            else
            {
                content = new ArchiveDirectory(this, path);
                this.FilenameMapping[normalisedFilename] = content;
            }
            return (content);
        }

        /// <summary>
        /// Create an archive directory content-node; may return a cached node 
        /// if its already part of this content tree.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public IContentNode CreateArchiveDirectory(IEnumerable<String> paths)
        {
            String combinedPath = String.Join(
                ArchiveFilesystemNodeBase.COMBINED_PATH_SEPARATOR_STR, paths);
            return (CreateArchiveDirectory(combinedPath));
        }

        /// <summary>
        /// CreateEmptyTree a process within this content-tree.
        /// </summary>
        /// <param name="processorClassName"></param>
        /// <param name="procCollection"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        public IProcess CreateProcess(String processorClassName, IProcessorCollection procCollection, 
            IEnumerable<IContentNode> inputs, IEnumerable<IContentNode> outputs,
            IEnumerable<IContentNode> additionalDependentContent = null,
            AllowRemoteType allowRemote = AllowRemoteType.Yes,
            RebuildType rebuild = RebuildType.No)
        {
            IProcessor processor = procCollection.FirstOrDefault(p => String.Equals(p.Name, processorClassName));
            if (null == processor)
            {
                ContentTree.Log.ErrorCtx(LOG_CTX, 
                    "Processor '{0}' not defined.  Content-tree misconfigured?",
                    processorClassName);
                return (null);
            }

            IProcess process = this.CreateProcess(processor, inputs, outputs,
                additionalDependentContent, allowRemote, rebuild);
            return (process);
        }

        /// <summary>
        /// CreateEmptyTree a process within this content-tree.
        /// </summary>
        /// <param name="processorClassName"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        public IProcess CreateProcess(String processorClassName, IEnumerable<IContentNode> inputs, 
            IEnumerable<IContentNode> outputs, IEnumerable<IContentNode> additionalDependentContent = null,
            AllowRemoteType allowRemote = AllowRemoteType.Yes, RebuildType rebuild = RebuildType.No)
        {
            IProcess process = new Process(this, processorClassName, inputs, outputs, allowRemote,
                rebuild, null, additionalDependentContent);
            this.m_Processes.Add(process);
            return (process);
        }

        /// <summary>
        /// CreateEmptyTree a process within this content-tree.
        /// </summary>
        /// <param name="processor"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        public IProcess CreateProcess(IProcessor processor,
            IEnumerable<IContentNode> inputs,
            IEnumerable<IContentNode> outputs, 
            IEnumerable<IContentNode> additionalDependentContent = null,
            AllowRemoteType allowRemote = AllowRemoteType.Yes,
            RebuildType rebuild = RebuildType.No)
        {
            Process process = new Process(this, processor.Name, inputs, outputs, allowRemote, 
                rebuild, null, additionalDependentContent);
            this.m_Processes.Add(process);
            return process;
        }

        /// <summary>
        /// Merge another content-tree into this one; used for merging a core
        /// Project's content-tree into a DLC-content-tree.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public void Merge(IContentTree other)
        {
            Debug.Assert(this.Branch.Project.IsDLC, "Merging into non-DLC project.");
            if (!this.Branch.Project.IsDLC)
                throw (new NotSupportedException("Merging into non-DLC project."));
            Debug.Assert(other is ContentTree, "Merging of non-ContentTree object.");
            if (!(other is ContentTree))
                throw (new NotSupportedException("Merging of non-ContentTree object."));

            ContentTree.Log.ProfileCtx(LOG_CTX, "Merging DLC Content-Tree."); 
            this.m_Processes.AddRange(other.Processes);
            foreach (KeyValuePair<String, IContentNode> kvp in (other as ContentTree).FilenameMapping)
            {
                if (this.FilenameMapping.ContainsKey(kvp.Key))
                    continue;
                this.FilenameMapping.Add(kvp);
            }
            ContentTree.Log.ProfileEnd();
        }

        /// <summary>
        /// Load content-tree from a local cache file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool LoadCache(String filename)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Save content-tree to a local cache file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool SaveCache(String filename)
        {
            throw new NotImplementedException();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Normalise and environment expand a path string.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private String ExpandAndNormalise(String path)
        {
            String normalisedPath = this.Branch.Environment.Subst(path);
            if (this.Branch.Environment.RequiresSubst(normalisedPath))
                return (normalisedPath.ToLower());
            else
                return (Path.GetFullPath(normalisedPath).ToLower());
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Content
