﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Content node representing a filesystem directory where the input files
    /// are statically defined.
    /// </summary>
    public class StaticDirectory : 
        Directory,
        IInputEvaluator
    {
        #region Properties 
        /// <summary>
        /// 
        /// </summary>
        public ICollection<IContentNode> StaticFiles
        {
            get;
            private set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="pathname"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        internal StaticDirectory(IContentTree owner, String pathname)
            : this(owner, Guid.NewGuid(), pathname)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="pathname"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        internal StaticDirectory(IContentTree owner, Guid id, String pathname)
            : base(owner, id, pathname)
        {
            this.StaticFiles = new List<IContentNode>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="pathname"></param>
        internal StaticDirectory(Directory directory)
            : this(directory.Owner, Guid.NewGuid(), directory.AbsolutePath)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Evaluate inputs.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<IContentNode> EvaluateInputs()
        {
            return (this.StaticFiles);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Content namespace
