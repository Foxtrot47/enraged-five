﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content.Algorithm;

namespace RSG.Pipeline.Content.Algorithm
{

    /// <summary>
    /// Implements IComparer for two IProcess objects; returning the correct
    /// order they should be built in (after dependency analysis).
    /// </summary>
    public class ProcessExecOrderComparer : IComparer<IProcess>
    {
        #region Member Data
        /// <summary>
        /// Recursive process dependency cache.
        /// </summary>
        private IDictionary<IProcess, ISet<IProcess>> m_recursiveDependenciesCache;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor; no process dependency cache.
        /// </summary>
        public ProcessExecOrderComparer()
        {
            this.m_recursiveDependenciesCache = new Dictionary<IProcess, ISet<IProcess>>();
        }

        /// <summary>
        /// Constructor; taking a process (recursive) dependency cache.
        /// </summary>
        /// <param name="recursive"></param>
        public ProcessExecOrderComparer(IEnumerable<KeyValuePair<IProcess, IEnumerable<IProcess>>> recursive)
        {
            this.m_recursiveDependenciesCache = new Dictionary<IProcess, ISet<IProcess>>();
            foreach (KeyValuePair<IProcess, IEnumerable<IProcess>> kvp in recursive)
                this.m_recursiveDependenciesCache.Add(kvp.Key, new HashSet<IProcess>(kvp.Value));
        }

        /// <summary>
        /// Constructor; taking a process (recursive) dependency cache.
        /// </summary>
        /// <param name="recursive"></param>
        public ProcessExecOrderComparer(IDictionary<IProcess, IEnumerable<IProcess>> recursive)
        {
            this.m_recursiveDependenciesCache = new Dictionary<IProcess, ISet<IProcess>>();
            foreach (KeyValuePair<IProcess, IEnumerable<IProcess>> kvp in recursive)
                this.m_recursiveDependenciesCache.Add(kvp.Key, new HashSet<IProcess>(kvp.Value));
        }

        /// <summary>
        /// Constructor; taking a process (recursive) dependency cache.
        /// </summary>
        /// <param name="recursive"></param>
        public ProcessExecOrderComparer(IDictionary<IProcess, ICollection<IProcess>> recursive)
        {
            this.m_recursiveDependenciesCache = new Dictionary<IProcess, ISet<IProcess>>();
            foreach (KeyValuePair<IProcess, ICollection<IProcess>> kvp in recursive)
                this.m_recursiveDependenciesCache.Add(kvp.Key, new HashSet<IProcess>(kvp.Value));
        }
        #endregion // Constructor(s)

        #region IComparer<IProcess> Interface Methods
        /// <summary>
        /// Compare two IProcess objects; returning which one should be built
        /// first depending on their dependencies.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare(IProcess a, IProcess b)
        {
            IEnumerable<IProcess> dependsA = GetProcessDependencies(a);
            IEnumerable<IProcess> dependsB = GetProcessDependencies(b);

            if (dependsA.Contains(b) && dependsB.Contains(a))
                throw new ContentException("Recursive dependency between two content nodes.");

            if (dependsA.Contains(b))
                return (1);
            else if (dependsB.Contains(a))
                return (-1);
            return (0);
        }
        #endregion // IComparer<IProcess> Interface Methods

        #region Private Methods
        /// <summary>
        /// Return process recursive dependencies; either from our cache or
        /// by recalculating it.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private IEnumerable<IProcess> GetProcessDependencies(IProcess process)
        {
            if (this.m_recursiveDependenciesCache.ContainsKey(process))
                return (this.m_recursiveDependenciesCache[process]);
            else
                return (process.DependsOn(DependencyTrackMode.Recursive));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Content.Algorithm namespace
