﻿using System;
using System.Collections.Generic;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Abstract base class for common content nodes; most nodes should derive 
    /// from this class although there is no reason you are required to.
    /// </summary>
    public abstract class NodeBase : IContentNode
    {
        #region Properties
        /// <summary>
        /// Content node name.
        /// </summary>
        public String Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Owning IContentTree object.
        /// </summary>
        public IContentTree Owner
        {
            get;
            private set;
        }

        /// <summary>
        /// Content node unique identifier; used for caching.
        /// </summary>
        public Guid ID 
        { 
            get;
            protected set;
        }

        /// <summary>
        /// Generic parameters for this IContentNode; can be used to stuff additional
        /// data into nodes as they are being passed through the pipeline.
        /// </summary>
        public IDictionary<String, Object> Parameters
        {
            get;
            private set;
        }

        /// <summary>
        /// Copy parameters from a source node into this parameters
        /// </summary>
        /// <param name="fromNode"></param>
        public void CopyParametersFrom(IContentNode fromNode)
        {
            if (this.Parameters == null)
            {
                this.Parameters = new Dictionary<string, object>();
            }

            foreach (KeyValuePair<string, object> pair in fromNode.Parameters)
            {
                SetParameter(pair.Key, pair.Value);
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        internal NodeBase(IContentTree owner, Guid id, String name)
        {
            this.Owner = owner;
            this.ID = id;
            this.Name = name;
            this.Parameters = new Dictionary<String, Object>();
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// Object equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            NodeBase node = obj as NodeBase;

            if (node == null)
                return false;

            return this.Name == node.Name && this.Owner == node.Owner;
        }
        #endregion // Object Overridden Methods

        #region IHasParameters methods implementation
        /// <summary>
        /// Determine whether the parameter exists.
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <returns></returns>
        public bool HasParameter(string name)
        {
            return (this.Parameters.ContainsKey(name));
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter(string name, object value)
        {
            SetParameter<object>(name, value);
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter<T>(string name, T value)
        {
            if (Parameters.ContainsKey(name))
            {
                Parameters[name] = value;
            }
            else
            {
                Parameters.Add(name, value);
            }
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public object GetParameter(string name, object defaultValue)
        {
            return GetParameter<object>(name, defaultValue);
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public T GetParameter<T>(string name, T defaultValue)
        {
            // underlying type of the Parameter's value is still an object so we're constrained here
            object value;

            // No type checking here.
            // Because if the calling code is supplying the type and that's not what the parameter is, then the problem is not here
            // (aka "Embrace the InvalidCastException")
            if (Parameters.TryGetValue(name, out value))
            {
                return (T)value;
            }
            return defaultValue;
        }
        #endregion
    }

} // RSG.Pipeline.Content namespace
