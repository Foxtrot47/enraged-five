﻿//---------------------------------------------------------------------------------------------
// <copyright file="Process.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Content
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using RSG.Pipeline.Core;    
 
    /// <summary>
    /// The Process class represents a transition (a process) from one content
    /// node to another using a particular IProcessor.
    /// </summary>
    /// Note: adding/changing/removing members you *MUST* update the Clone method!
    /// 
    public sealed class Process :
        IProcess
    {
        #region Properties
        /// <summary>
        /// Process' processor object to utilise for the data transition.
        /// </summary>
        public String ProcessorClassName
        {
            get;
            private set;
        }

        /// <summary>
        /// Owning IContentTree object.
        /// </summary>
        public IContentTree Owner
        {
            get;
            private set;
        }

        /// <summary>
        /// Enumerable of process input IContentNode's.
        /// </summary>
        public IEnumerable<IContentNode> Inputs
        {
            get { return (m_Inputs); }
        }
        private ISet<IContentNode> m_Inputs;

        /// <summary>
        /// Enumerable of process output IContentNode's.
        /// </summary>
        public IEnumerable<IContentNode> Outputs
        {
            get { return (m_Outputs); }
        }
        private ISet<IContentNode> m_Outputs;

        /// <summary>
        /// Set of our dependent nodes (need to be synced prior to this being 
        /// executed).  This allows a secondary set of nodes that are dependencies
        /// but not explicitly process inputs (e.g. ITYP file for RPF sorting).
        /// </summary>
        public IEnumerable<IContentNode> AdditionalDependentContent
        {
            get { return (m_AdditionalDependentContent); }
        }
        private ISet<IContentNode> m_AdditionalDependentContent;

        /// <summary>
        /// Current process state; used by engines to track process state through
        /// the recursive prebuild and build steps.
        /// </summary>
        public ProcessState State
        {
            get;
            set;
        }

        /// <summary>
        /// Process' rebuild flag; used to force data through even if it would 
        /// normally not require building.
        /// </summary>
        public RebuildType Rebuild
        {
            get;
            set;
        }

        /// <summary>
        /// Process' remote flag; indicates to the system whether the processor 
        /// can run on a remote host (default: true).
        /// </summary>
        public AllowRemoteType AllowRemote
        {
            get;
            private set;
        }

        /// <summary>
        /// Generic options for the processor for this transition; read from
        /// the content-tree XML and saves us having to explicitly add parameters
        /// and definitions for each processor.
        /// </summary>
        [Obsolete("Use GetParameter/SetParameter IHasParameters interface methods instead.")]
        public IDictionary<String, Object> Parameters
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor for a Process.
        /// </summary>
        /// <param name="owner">Owning content-tree.</param>
        /// <param name="processorClassName">IProcessor classname object to use.</param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        internal Process(IContentTree owner, String processorClassName, 
            IEnumerable<IContentNode> inputs, IEnumerable<IContentNode> outputs)
            : this(owner, processorClassName, inputs, outputs, AllowRemoteType.Yes, RebuildType.No, null, null)
        {
        }

        /// <summary>
        /// Constructor for a Process.
        /// </summary>
        /// <param name="owner">Owning content-tree.</param>
        /// <param name="processorClassName">IProcessor classname object to use.</param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        internal Process(IContentTree owner, String processorClassName,
            IEnumerable<IContentNode> inputs, IEnumerable<IContentNode> outputs,
            AllowRemoteType allowRemote, RebuildType rebuild)
            : this(owner, processorClassName, inputs, outputs, allowRemote, rebuild, null, null)
        {
        }

        /// <summary>
        /// Constructor (all encompassing for all parameters).
        /// </summary>
        /// <param name="owner">Owning content-tree.</param>
        /// <param name="processorClassName">IProcessor classname object to use.</param>
        /// <param name="allow_remote"></param>
        /// <param name="rebuild"></param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        /// <param name="parameters"></param>
        /// <param name="additionalDependentContent"></param>
        internal Process(IContentTree owner, String processorClassName,
            IEnumerable<IContentNode> inputs, IEnumerable<IContentNode> outputs,
            AllowRemoteType allowRemote,
            RebuildType rebuild,
            IEnumerable<KeyValuePair<String, Object>> parameters,
            IEnumerable<IContentNode> additionalDependentContent)
        {
            if (String.IsNullOrWhiteSpace(processorClassName))
                throw (new ArgumentNullException("processorClassName"));

            bool forceCreateEmptyOutput = false;

            foreach (IContentNode output in outputs)
            {
                foreach (KeyValuePair<String, Object> p in output.Parameters)
                {
                    if (p.Key == Constants.Force_Create_If_Empty)
                    {
                        forceCreateEmptyOutput = true;
                    }
                }
            }

            if (!forceCreateEmptyOutput)
            {
                Debug.Assert(inputs.Any(),
                    "Creating Process object with no inputs.  Internal error.");
            }

            this.State = ProcessState.Initialised;
            this.Owner = owner;
            this.ProcessorClassName = processorClassName;
            this.AllowRemote = allowRemote;
            this.Rebuild = rebuild;
            this.m_Inputs = new HashSet<IContentNode>(inputs);
            this.m_Outputs = new HashSet<IContentNode>(outputs);

            this.Parameters = new Dictionary<String, Object>();
            if (null != parameters)
            {
                foreach (KeyValuePair<String, Object> p in parameters)
                    this.Parameters.Add(p.Key, p.Value);
            }

            // If we are forcing empty output files, set it on the whole process
            if (forceCreateEmptyOutput)
                this.Parameters.Add(Constants.Force_Create_If_Empty, true);

            if (null != additionalDependentContent)
                this.m_AdditionalDependentContent = new HashSet<IContentNode>(additionalDependentContent);
            else
                this.m_AdditionalDependentContent = new HashSet<IContentNode>();
        }
        #endregion // Constructor(s)

        #region ICloneable Interface Methods
        /// <summary>
        /// Copy object.
        /// </summary>
        /// <returns></returns>
        Object ICloneable.Clone()
        {
            return (this.Clone());
        }

        /// <summary>
        /// Copy object.
        /// </summary>
        /// <returns></returns>
        public Process Clone()
        {
            // This is implemented like this and not using MemberwiseClone so
            // the collections are unique to the new process.  Otherwise they
            // would be references to the other Process' collections.
            Process process = new Process(this.Owner, this.ProcessorClassName, 
                this.Inputs, this.Outputs, this.AllowRemote, this.Rebuild,
                this.Parameters, this.AdditionalDependentContent);
            return (process);
        }
        #endregion // ICloneable Interface Methods

        #region IHasParameters Interface Methods
        /// <summary>
        /// Determine whether the parameter exists.
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <returns></returns>
        public bool HasParameter(string name)
        {
            return (this.Parameters.ContainsKey(name));
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter<T>(string name, T value)
        {
            if (Parameters.ContainsKey(name))
            {
                Parameters[name] = value;
            }
            else
            {
                Parameters.Add(name, value);
            }
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Add to Parameters
        /// </summary>
        /// <param name="name">Parameter's name</param>
        /// <param name="value">Parameter's value</param>
        public void SetParameter(string name, object value)
        {
            SetParameter<object>(name, value);
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public T GetParameter<T>(string name, T defaultValue)
        {
            // underlying type of the Parameter's value is still an object so we're constrained here
            object value;

            // No type checking here.
            // Because if the calling code is supplying the type and that's not what the parameter is, then the problem is not here
            // (aka "Embrace the InvalidCastException")
            if (Parameters.TryGetValue(name, out value))
            {
                return (T)value;
            }
            return defaultValue;
        }

        /// <summary>
        /// Encapsulation of the Parameters Dictionary access.
        /// Use this to perform a ContainsKey/Get to Parameters
        /// </summary>
        /// <param name="name">Parameter's name we try to retrieve the value.</param>
        /// <param name="defaultValue">If we can't retrieve the value, the default value will be used as return value.</param>
        /// <returns></returns>
        public object GetParameter(string name, object defaultValue)
        {
            return GetParameter<object>(name, defaultValue);
        }
        #endregion
    }

} // RSG.Pipeline.Content namespace
