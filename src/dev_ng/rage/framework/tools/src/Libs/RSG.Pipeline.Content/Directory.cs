﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Content node representing a filesystem directory.
    /// </summary>
    public class Directory :
        NodeBase,
        IFilesystemNode,
        IInputEvaluator
    { 
        #region Constants
        /// <summary>
        /// Content-tree XML content_type value.
        /// </summary>
        internal const String XML_CONTENT = "directory";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Absolute path to this file object.
        /// </summary>
        public String AbsolutePath
        {
            get;
            private set;
        }

        /// <summary>
        /// Files wildcard to include as inputs.
        /// </summary>
        public String Wildcard
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether to search subdirectories.
        /// </summary>
        public bool Recursive
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="pathname"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        internal Directory(IContentTree owner, String pathname, String wildcard = "*.*", bool recursive = false)
            : this(owner, Guid.NewGuid(), pathname, wildcard, recursive)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="pathname"></param>
        /// <param name="wildcard"></param>
        /// <param name="recursive"></param>
        internal Directory(IContentTree owner, Guid id, String pathname, String wildcard = "*.*", bool recursive = false)
            : base(owner, id, SIO.Path.GetFileNameWithoutExtension(pathname))
        {
            if (SIO.Path.IsPathRooted(pathname))
                this.AbsolutePath = SIO.Path.GetFullPath(pathname);
            else
                this.AbsolutePath = pathname;
            this.Wildcard = wildcard;
            this.Recursive = recursive;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Evaluate inputs.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<IContentNode> EvaluateInputs()
        {
            ICollection<IContentNode> inputs = new List<IContentNode>();
            if (this.Exists())
            {
                foreach (String filename in this.EvaluateFiles())
                {
                    IContentNode input = this.Owner.CreateFile(filename);
                    if (null != input)
                        inputs.Add(input);
                }
            }
            return (inputs);
        }

        /// <summary>
        /// Last modified time of this directory.
        /// </summary>
        public virtual DateTime GetModifiedTimestamp()
        {
            DateTime modified = DateTime.MinValue;
            if (this.Exists())
            {
                // Initialise with directory modified time; captures files added
                // or removed.
                modified = SIO.Directory.GetLastWriteTimeUtc(this.AbsolutePath);

                foreach (IContentNode node in this.EvaluateInputs())
                {
                    if (!(node is IFilesystemNode))
                        continue;

                    String filename = (node as IFilesystemNode).AbsolutePath;
                    DateTime m = SIO.File.GetLastWriteTimeUtc(filename);
                    if (m.IsLaterThan(modified))
                        modified = m;
                }
                return (modified);
            }
            return (modified);
        }

        /// <summary>
        /// Set modified time (UTC) of this directory.
        /// </summary>
        /// <param name="value"></param>
        public void SetModifiedTimestamp(DateTime value)
        {
            if (this.Exists())
            {
                // Initialise directory modified time.
                SIO.Directory.SetLastWriteTimeUtc(this.AbsolutePath, value);

                foreach (IContentNode node in this.EvaluateInputs())
                {
                    if (!(node is IFilesystemNode))
                        continue;
                    IFilesystemNode fsNode = (IFilesystemNode)node;
                    if (SIO.File.Exists(fsNode.AbsolutePath))
                        SIO.File.SetLastWriteTimeUtc(fsNode.AbsolutePath, value);
                }
            }
        }

        /// <summary>
        /// Creation time of this directory.
        /// </summary>
        public virtual DateTime GetCreationTimestamp()
        {
            DateTime modified = DateTime.MinValue;
            if (this.Exists())
            {
                foreach (IContentNode node in this.EvaluateInputs())
                {
                    if (!(node is IFilesystemNode))
                        continue;

                    String filename = (node as IFilesystemNode).AbsolutePath;
                    DateTime m = SIO.File.GetCreationTimeUtc(filename);
                    if (m.IsLaterThan(modified))
                        modified = m;
                }
                return (modified);
            }
            return (modified);
        }

        /// <summary>
        /// Calculate and return the size (in bytes) of the filesystem data.
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        public virtual long GetSize()
        {
            if (!this.Exists())
                return (0L);

            IEnumerable<IContentNode> content = this.EvaluateInputs();
            IEnumerable<String> files = content.Where(c => c is Content.File).Select(c => (c as Content.File).AbsolutePath);
            IEnumerable<SIO.FileInfo> fileInfos = files.AsParallel().Select(
                    f => new SIO.FileInfo(f)
                );
            long size = fileInfos.Sum(fi => fi.Length);
            return (size);
        }

        /// <summary>
        /// Determine if the directory exists.
        /// </summary>
        /// <returns></returns>
        public virtual bool Exists()
        {
            return (SIO.Directory.Exists(this.AbsolutePath));
        }

        /// <summary>
        /// Touch the directory updating all its files Modified Time (UTC).
        /// </summary>
        /// <param name="when_utc">DateTime (UTC) object to update to.</param>
        /// <returns></returns>
        public virtual bool Touch(DateTime? when_utc = null)
        {
            bool result = true;
            try
            {
                if (!when_utc.HasValue)
                    when_utc = DateTime.UtcNow;

                Debug.Assert(DateTimeKind.Utc == when_utc.Value.Kind);
                IEnumerable<IContentNode> content = this.EvaluateInputs();
                IEnumerable<String> files = content.Where(c => c is Content.File).Select(c => (c as Content.File).AbsolutePath);

                foreach (String filename in files)
                {
                    SIO.File.SetLastWriteTimeUtc(filename, when_utc.Value);
                }
                SIO.Directory.SetLastWriteTimeUtc(this.AbsolutePath, when_utc.Value);
                result = true;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Exception touching Directory.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// Object equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == this)
                return (true);
            if (!(obj is Directory))
                return (false);

            Directory dir = (obj as Directory);
            if (!dir.AbsolutePath.Equals(this.AbsolutePath))
                return (false);
            else if (dir.Recursive != this.Recursive)
                return (false);
            else if (!dir.Wildcard.Equals(this.Wildcard))
                return (false);
            return (true);
        }

        /// <summary>
        /// Hashcode calculation.
        /// </summary>
        /// <returns></returns>
        /// *Note*: safe because the properties are read-only.
        /// 
        public override int GetHashCode()
        {
            int hashCode = this.AbsolutePath.GetHashCode();
            hashCode ^= this.Wildcard.GetHashCode();
            hashCode ^= this.Recursive.GetHashCode();

            return (hashCode);
        }

        /// <summary>
        /// Return String representation.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.AbsolutePath);
        }
        #endregion // Object Overridden Methods

        #region Private Methods
        /// <summary>
        /// Evaluate files in directory; returning array of them.
        /// </summary>
        /// <returns></returns>
        private String[] EvaluateFiles()
        {
            SIO.SearchOption options = this.Recursive ?
                SIO.SearchOption.AllDirectories : SIO.SearchOption.TopDirectoryOnly;
            
            return (SIO.Directory.GetFiles(this.AbsolutePath, this.Wildcard, options));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Content namespace
