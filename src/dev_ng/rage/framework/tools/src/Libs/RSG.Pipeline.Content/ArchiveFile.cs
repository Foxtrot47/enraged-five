﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Pipeline.Core;
using RSG.Platform;
using Ionic.Zip;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Archive file node; wrapping an archive file and relative file path in
    /// the archive.  Handles recursive zips too.
    /// </summary>
    /// The combine path for the constructor should take the form:
    ///   [absolute_path]|[relative_path]+
    /// E.g.
    ///   "x:\gta5\assets\export\test.ziptexture.dds" (simple)||
    ///   "x:\gta5\assets\export\test2.zip|txd.zip|texture.dds" (packed zip)
    ///   
    public class ArchiveFile :
        ArchiveFilesystemNodeBase,
        IArchiveFilesystemNode
    {
        #region Properties
        /// <summary>
        /// Relative file paths within the initial archive file.
        /// </summary>
        public IEnumerable<String> RelativeFilePaths
        {
            get { return (this.m_RelativeFilePaths); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from combined path string.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="combinedPath"></param>
        internal ArchiveFile(IContentTree owner, String combinedPath)
            : base(owner, combinedPath)
        {
        }

        /// <summary>
        /// Constructor; from combined path string with GUID.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="combinedPath"></param>
        internal ArchiveFile(IContentTree owner, Guid id, String combinedPath)
            : base(owner, id, combinedPath)
        {
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Modified time (UTC) of this archive filesystem object.
        /// </summary>
        public override DateTime GetModifiedTimestamp()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                ZipEntry entry = GetZipEntryRecursive(zf, this.m_RelativeFilePaths);
                if (null != entry)
                    return (entry.ModifiedTime);
                else
                    return (DateTime.MinValue);
            }
        }

        /// <summary>
        /// Set modified time (UTC) of this archive filesystem object.
        /// </summary>
        /// <param name="value"></param>
        public override void SetModifiedTimestamp(DateTime value)
        {
            throw (new NotSupportedException());
        }

        /// <summary>
        /// Creation time (UTC) of this archive filesystem object.
        /// </summary>
        public override DateTime GetCreationTimestamp()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (DateTime.MinValue);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                ZipEntry entry = GetZipEntryRecursive(zf, this.m_RelativeFilePaths);
                if (null != entry)
                    return (entry.CreationTime);
                else
                    return (DateTime.MinValue);
            }
        }

        /// <summary>
        /// Calculate and return the size (in bytes) of the archive filesystem 
        /// data (uncompressed size).
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        public override long GetSize()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (0);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (0);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                ZipEntry entry = GetZipEntryRecursive(zf, this.m_RelativeFilePaths);
                if (null != entry)
                    return (entry.UncompressedSize);
                else
                    return (0);
            }
        }

        /// <summary>
        /// Determine if the archive filesystem node exists.
        /// </summary>
        /// <returns>true iff object exists; false otherwise</returns>
        public override bool Exists()
        {
            if (!SIO.File.Exists(this.ArchiveAbsolutePath))
                return (false);

            if (!ZipFile.IsZipFile(this.ArchiveAbsolutePath))
                return (false);

            using (ZipFile zf = new ZipFile(this.ArchiveAbsolutePath))
            {
                ZipEntry entry = GetZipEntryRecursive(zf, this.m_RelativeFilePaths);
                return (null != entry);
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Content namespace
