﻿using System;
using System.Diagnostics;
using SIO = System.IO;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.Platform;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Content node representing a filesystem file.
    /// </summary>
    public class File :
        NodeBase,
        IFilesystemNode
    {
        #region Constants
        /// <summary>
        /// Content-tree XML content_type value.
        /// </summary>
        internal const String XML_CONTENT = "file";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Absolute path to this file object (may require environment substitution).
        /// </summary>
        public String AbsolutePath
        {
            get;
            private set;
        }

        /// <summary>
        /// File extension (including '.', e.g. ".jpg");
        /// </summary>
        public String Extension
        {
            get { return SIO.Path.GetExtension(this.AbsolutePath); }
        }

        /// <summary>
        /// File basename (filename, excluding path and extension).
        /// </summary>
        public String Basename
        {
            get { return (Filename.GetBasename(this.AbsolutePath)); }
        }

        /// <summary>
        /// File directory.
        /// </summary>
        public String Directory
        {
            get { return SIO.Path.GetDirectoryName(this.AbsolutePath); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="filename"></param>
        internal File(IContentTree owner, String filename)
            : this(owner, Guid.NewGuid(), filename)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="id"></param>
        /// <param name="filename"></param>
        internal File(IContentTree owner, Guid id, String filename)
            : base(owner, id, SIO.Path.GetFileNameWithoutExtension(filename))
        {
            if (System.IO.Path.IsPathRooted(filename))
                this.AbsolutePath = System.IO.Path.GetFullPath(filename);
            else
                this.AbsolutePath = filename;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Last modified time of this file object.
        /// </summary>
        public DateTime GetModifiedTimestamp()
        {
            return (SIO.File.GetLastWriteTimeUtc(this.AbsolutePath));
        }

        /// <summary>
        /// Set modified time (UTC) of this filesystem object.
        /// </summary>
        /// <param name="value"></param>
        public void SetModifiedTimestamp(DateTime value)
        {
            try
            {
                if (this.Exists())
                    SIO.File.SetLastWriteTimeUtc(this.AbsolutePath, value);
            }
            catch (System.IO.IOException)
            {
                // Higher-level systems log these.
            }
        }

        /// <summary>
        /// Creation time of this file object.
        /// </summary>
        public DateTime GetCreationTimestamp()
        {
            return (SIO.File.GetCreationTimeUtc(this.AbsolutePath));
        }

        /// <summary>
        /// Calculate and return the size (in bytes) of the filesystem data.
        /// </summary>
        /// <returns>Size (in bytes).</returns>
        public long GetSize()
        {
            if (!this.Exists())
                return (0L);

            SIO.FileInfo fi = new SIO.FileInfo(this.AbsolutePath);
            return (fi.Length);
        }

        /// <summary>
        /// Determine if the file exists.
        /// </summary>
        /// <returns></returns>
        public bool Exists()
        {
            return (SIO.File.Exists(this.AbsolutePath));
        }

        /// <summary>
        /// Touch the file updating its Modified Time (UTC).
        /// </summary>
        /// <param name="when_utc">DateTime (UTC) object to update to.</param>
        /// <returns></returns>
        public bool Touch(DateTime? when_utc = null)
        {
            bool result = true;
            try
            {
                if (!when_utc.HasValue)
                    when_utc = DateTime.UtcNow;

                Debug.Assert(DateTimeKind.Utc == when_utc.Value.Kind);
                SIO.File.SetLastWriteTimeUtc(this.AbsolutePath, when_utc.Value);
                result = true;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Exception touching Directory.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Object Overridden Methods
        /// <summary>
        /// Object equality.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (!(obj is File))
                return (false);

            return (this.Equals((File)obj));
        }

        /// <summary>
        /// File equality.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool Equals(File file)
        {
            if (null == (Object)file)
                return (false);

            if (!file.AbsolutePath.Equals(this.AbsolutePath))
                return (false);
            return (true);
        }

        /// <summary>
        /// Hashcode calculation.
        /// </summary>
        /// <returns></returns>
        /// *Note*: safe because the properties are read-only.
        /// 
        public override int GetHashCode()
        {
            int hashCode = this.AbsolutePath.GetHashCode();
            return (hashCode);
        }

        /// <summary>
        /// Return String representation.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.AbsolutePath);
        }
        #endregion // Object Overridden Methods
    }

} // RSG.Pipeline.Content namespace
