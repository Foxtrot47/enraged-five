﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Extensions;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Content.Algorithm
{

    /// <summary>
    /// Dependency tracking mode enumeration; allows users to specify how deep
    /// dependencies should be tracked and returned.
    /// </summary>
    public enum DependencyTrackMode
    {
        Immediate,
        Recursive,
    }

    /// <summary>
    /// Dependency walking algorithms for Content-Tree and Content-Node objects
    /// (implemented as extension methods).
    /// </summary>
    public static class DependencyWalker
    {
        /// <summary>
        /// Return a set of IProcess objects that are required processes to have
        /// its inputs satisfied and available (which IProcess object supply
        /// this IProcess' inputs).
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        /// *Note*: this makes the IProcess.DependentProcesses property obsolete.
        /// 
        public static IEnumerable<IProcess> DependsOn(this IProcess process)
        {
            return (DependsOn(process, DependencyTrackMode.Immediate));
        }

        /// <summary>
        /// Return a set of IProcess objects that are required processes to have
        /// its inputs satisfied and available (which IProcess objects supply
        /// this IProcess' inputs).
        /// </summary>
        /// <param name="process"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> DependsOn(this IProcess thisProcess, DependencyTrackMode mode)
        {
            if (mode == DependencyTrackMode.Immediate)
            {
                return DependsOnImmediate(thisProcess);
            }
            else if (mode == DependencyTrackMode.Recursive)
            {
                IEnumerable<IProcess> immediateDeps = DependsOnImmediate(thisProcess);
                ICollection<IProcess> recursiveDeps = new List<IProcess>();
                recursiveDeps.AddRange(immediateDeps);

                foreach (IProcess p in immediateDeps)
                {
                    IEnumerable<IProcess> deps = DependsOn(p, DependencyTrackMode.Recursive);
                    recursiveDeps.AddRange(deps);
                }
                
                return (recursiveDeps);
            }
            else
            {
                throw new InvalidOperationException(String.Format("Unexpected DependencyTrackMode '{0}'", mode.ToString()));
            }
        }        

        /// <summary>
        /// Return a set of IProcess objects that are required processes to have
        /// its inputs satisfied and available (which IProcess objects supply
        /// this IProcess' inputs).
        /// 
        /// Takes a dictionary of node-process pairs for faster lookups.
        /// </summary>
        /// <param name="thisProcess"></param>
        /// <param name="outputProcessMap"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> DependsOn(this IProcess thisProcess, 
                        IDictionary<IContentNode, ICollection<IProcess>> outputProcessMap, 
                        DependencyTrackMode mode)
        {
            if (mode == DependencyTrackMode.Immediate)
            {
                return DependsOnImmediate(thisProcess, outputProcessMap);
            }
            else if (mode == DependencyTrackMode.Recursive)
            {
                IEnumerable<IProcess> immediateDeps = DependsOnImmediate(thisProcess, outputProcessMap);
                ICollection<IProcess> recursiveDeps = new List<IProcess>();
                recursiveDeps.AddRange(immediateDeps);

                foreach (IProcess p in immediateDeps)
                {
                    IEnumerable<IProcess> deps = DependsOn(p, outputProcessMap, DependencyTrackMode.Recursive);
                    recursiveDeps.AddRange(deps);
                }

                return (recursiveDeps);
            }
            else
            {
                throw new InvalidOperationException(String.Format("Unexpected DependencyTrackMode '{0}'", mode.ToString()));
            }
        }

        /// <summary>
        /// Return a set of IProcess objects that are dependent on this IProcess
        /// for their inputs to be satisfied (which IProcess objects use this 
        /// IProcess' outputs).
        /// </summary>
        /// <param name="process"></param>
        /// <param name="dependants"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> DependencyFor(this IProcess process)
        {
            return (DependencyFor(process, DependencyTrackMode.Immediate));
        }

        /// <summary>
        /// Return a set of IProcess objects that are dependent on this IProcess
        /// for their inputs to be satisfied (which IProcess objects use this 
        /// IProcess' outputs).
        /// </summary>
        /// <param name="process"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static IEnumerable<IProcess> DependencyFor(this IProcess process, DependencyTrackMode mode)
        {
            IEnumerable<IProcess> immediateDeps = process.Owner.Processes.AsParallel().
                Where(p => p != process).
                Where(p => p.Outputs.Intersect(process.Inputs).Any());
            ICollection<IProcess> recursiveDeps = new List<IProcess>();
            recursiveDeps.AddRange(immediateDeps);

            if (DependencyTrackMode.Recursive == mode)
            {
                foreach (IProcess p in immediateDeps)
                {
                    IEnumerable<IProcess> deps = DependencyFor(p, mode);
                    recursiveDeps.AddRange(deps);
                }
            }
            return (recursiveDeps);
        }

        /// <summary>
        /// Return a set of IProcess objects that have this IContentNode as an
        /// input.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        public static bool InputsTo(this IContentNode node, 
            out IEnumerable<IProcess> processes)
        {
            processes = node.Owner.Processes.AsParallel().Where(p => p.Inputs.Contains(node));
            return (processes.Any());
        }

        /// <summary>
        /// Return a set of IProcess objects that have this IContentNode as an
        /// output.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        public static bool OutputsFrom(this IContentNode node,
            out IEnumerable<IProcess> processes)
        {
            processes = node.Owner.Processes.AsParallel().Where(p => p.Outputs.Contains(node));
            return (processes.Any());
        }

        /// <summary>
        /// Return a process' immediate dependencies; iterating Inputs and 
        /// AdditionalDependentContent sets.
        /// </summary>
        /// <param name="thisProcess"></param>
        /// <returns></returns>
        private static IEnumerable<IProcess> DependsOnImmediate(this IProcess thisProcess)
        {
            // This is deliberately unrolled and de-LINQed for maximum performance
            // Please don't try to make the code prettier - JWR
            List<IProcess> dependencies = new List<IProcess>();
            foreach (IProcess ownerProcess in thisProcess.Owner.Processes)
            {
                if (ownerProcess != thisProcess)
                {
                    bool found = false;
                    foreach (IContentNode inputNode in thisProcess.Inputs)
                    {
                        foreach (IContentNode outputNode in ownerProcess.Outputs)
                        {
                            if (inputNode == outputNode)
                            {
                                dependencies.Add(ownerProcess);
                                found = true;
                            }
                        }

                        if (found)
                            break;
                    }
                    foreach (IContentNode additionalNode in thisProcess.AdditionalDependentContent)
                    {
                        foreach (IContentNode outputNode in ownerProcess.Outputs)
                        {
                            if (additionalNode == outputNode)
                            {
                                dependencies.Add(ownerProcess);
                                found = true;
                            }
                        }

                        if (found)
                            break;
                    }
                }
            }

            return dependencies;
        }

        /// <summary>
        /// Return a process' immediate dependencies; iterating Inputs and 
        /// AdditionalDependentContent sets.
        /// 
        /// Takes a dictionary of node-process list pairs for faster lookups.
        /// </summary>
        /// <param name="thisProcess"></param>
        /// <param name="outputProcessMap"></param>
        /// <returns></returns>
        private static IEnumerable<IProcess> DependsOnImmediate(this IProcess thisProcess, 
                        IDictionary<IContentNode, ICollection<IProcess>> outputProcessMap)
        {
            // This is deliberately unrolled and de-LINQed for maximum performance
            // Please don't try to make the code prettier - JWR
            List<IProcess> dependencies = new List<IProcess>();

            foreach (IContentNode inputNode in thisProcess.Inputs)
            {
                if (outputProcessMap.ContainsKey(inputNode))
                {
                    // For every input, check the outputs in the processMap.
                    foreach (IProcess outputProcess in outputProcessMap[inputNode])
                    {
                        if (outputProcess != thisProcess)
                        {
                            dependencies.Add(outputProcess);
                        }
  
                    }
                }
            }

            foreach (IContentNode additionalNode in thisProcess.AdditionalDependentContent)
            {
                if (outputProcessMap.ContainsKey(additionalNode))
                {
                    // For every input, check the outputs in the processMap.
                    foreach (IProcess outputProcess in outputProcessMap[additionalNode])
                    {
                        if (outputProcess != thisProcess)
                        {
                            dependencies.Add(outputProcess);
                        }
                    }
                }
            }

            return dependencies;
        }
    }

} // RSG.Pipeline.Content.Algorithm
