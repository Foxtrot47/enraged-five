﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Content
{

    /// <summary>
    /// Content-tree Exception class.
    /// </summary>
    public class ContentException : Exception
    {
        #region Constructor(s)
        public ContentException()
            : base()
        {
        }

        public ContentException(String message)
            : base(message)
        {
        }

        public ContentException(String message, Exception inner)
            : base(message, inner)
        {
        }

        public ContentException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Content namespace
