﻿//---------------------------------------------------------------------------------------------
// <copyright file="IAsyncGameConnectionService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.GameConnection.Consumers.AsyncContracts
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.Threading.Tasks;
    using RSG.Services.GameConnection.Contracts.DataContracts;
    using RSG.Services.GameConnection.Contracts.ServiceContracts;

    /// <summary>
    /// Extends the <see cref="IGameConnectionService"/> interface with asynchronous versions 
    /// of the operation contracts.
    /// </summary>
    [ServiceContract]
    public interface IAsyncGameConnectionService : IGameConnectionService
    {
        /// <summary>
        /// Return information about all available game connections.
        /// </summary>
        /// <returns></returns>
        [OperationContract(
            Action = "http://tempuri.org/IGameConnectionService/GetGameConnections",
            ReplyAction = "http://tempuri.org/IGameConnectionService/GetGameConnectionsResponse")]
        Task<IEnumerable<IGameConnection>> GetGameConnectionsAsync();
    }

} // RSG.Services.GameConnection.Consumers.AsyncContracts namespace
