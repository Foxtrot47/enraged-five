﻿//---------------------------------------------------------------------------------------------
// <copyright file="GameConnectionConsumer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.GameConnection.Consumers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.ServiceModel;
    using RSG.Services.GameConnection.Consumers.Clients;
    using RSG.Services.GameConnection.Contracts.DataContracts;

    /// <summary>
    /// Consumer interface for the Game Connection Service.
    /// </summary>
    public class GameConnectionConsumer
    {
        #region Member Data
        /// <summary>
        /// Server configuration object that contains the details of the server we should
        /// connect to.
        /// </summary>
        private readonly IServer _server;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="GameConnectionConsumer"/>
        /// class using the specified server configuration object.
        /// </summary>
        /// <param name="server"></param>
        public GameConnectionConsumer(IServer server)
        {
            _server = server;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return information about all available game connections.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IGameConnection> GetGameConnections()
        {
            GameConnectionClient client = null;
            try
            {
                client = new GameConnectionClient(_server, Uri.UriSchemeNetTcp);
                return (client.GetGameConnections());
            }
            catch (Exception ex)
            {
                throw (new GameConnectionException("Error invoking GameConnectionService.GetGameConnections.", ex));
            }
            finally
            {
                if (client != null)
                {
                    client.CloseOrAbort();
                }
            }
        }

        /// <summary>
        /// Return information about all available game connections (asynchronosly).
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<IGameConnection>> GetGameConnectionsAsync()
        {
            GameConnectionClient client = null;
            try
            {
                client = new GameConnectionClient(_server, Uri.UriSchemeNetTcp);
                return (await client.GetGameConnectionsAsync());
            }
            catch (Exception ex)
            {
                throw (new GameConnectionException("Error invoking GameConnectionService.GetGameConnectionsAsync.", ex));
            }
            finally
            {
                if (client != null)
                {
                    client.CloseOrAbort();
                }
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Services.GameConnection.Consumers namespace
