﻿//---------------------------------------------------------------------------------------------
// <copyright file="GameConnectionClient.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.GameConnection.Consumers.Clients
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using RSG.Services.Configuration;
    using RSG.Services.Configuration.ServiceModel;
    using RSG.Services.GameConnection.Consumers.AsyncContracts;
    using RSG.Services.GameConnection.Contracts.DataContracts;
    using RSG.Services.GameConnection.Contracts.ServiceContracts;

    /// <summary>
    /// WCF client for communicating with a service that implemented the 
    /// <see cref="IGameConnectionService"/> service contract.
    /// </summary>
    internal class GameConnectionClient : 
        ConfigClient<IAsyncGameConnectionService>,
        IAsyncGameConnectionService
    {
        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="GameConnectionClient"/>
        /// using the specified server configuration object and uri scheme.
        /// </summary>
        public GameConnectionClient(IServer server, String uriScheme)
            : base(server, uriScheme)
        {
        }
        #endregion // Constructor(s)

        #region IAsyncGameConnectionService Methods
        /// <summary>
        /// Return information about all available game connections.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IGameConnection> GetGameConnections()
        {
            return (base.Channel.GetGameConnections());
        }

        /// <summary>
        /// Return information about all available game connections (asynchronously).
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<IGameConnection>> GetGameConnectionsAsync()
        {
            return (await base.Channel.GetGameConnectionsAsync());
        }
        #endregion // IAsyncGameConnectionService Methods
    }

} // RSG.Services.GameConnection.Consumers.Clients namespace
