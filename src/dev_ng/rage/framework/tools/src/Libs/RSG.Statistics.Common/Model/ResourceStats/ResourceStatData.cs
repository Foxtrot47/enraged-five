﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.Platform;

namespace RSG.Statistics.Common.Model.ResourceStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class ResourceStatData
    {
        /// <summary>
        /// Id for this row (for retrieving additional info regarding this stat).
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Name of the resource.
        /// </summary>
        [DataMember]
        public String ResourceName { get; set; }

        /// <summary>
        /// Source filename of resource.
        /// </summary>
        [DataMember]
        public String SourceFilename { get; set; }

        /// <summary>
        /// Destination filename of resource.
        /// </summary>
        [DataMember]
        public String DestinationFilename { get; set; }

        /// <summary>
        /// Type of file this resource is.
        /// </summary>
        [DataMember]
        public FileType FileType { get; set; }

        /// <summary>
        /// Total virtual bucket usage.
        /// </summary>
        [DataMember]
        public uint VirtualUsed { get; set; }

        /// <summary>
        /// Total virtual bucket capacity.
        /// </summary>
        [DataMember]
        public uint VirtualCapacity { get; set; }

        /// <summary>
        /// Total physical bucket usage.
        /// </summary>
        [DataMember]
        public uint PhysicalUsed { get; set; }

        /// <summary>
        /// Total physical bucket capacity.
        /// </summary>
        [DataMember]
        public uint PhysicalCapacity { get; set; }
    } // ResourceStatData
}
