﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public abstract class GameAssetStatBundleDtoBase : DtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlArray("ShaderStats")]
        [XmlArrayItem("ShaderStat")]
        public List<ShaderStatBundleDto> ShaderStats
        {
            get;
            set;
        }

        [DataMember]
        [XmlArray("Shaders")]
        [XmlArrayItem("Shader")]
        public List<ShaderBundleDto> Shaders
        {
            get
            {
                if (m_shaders == null)
                {
                    m_shaders = ShaderStats.Select(s => s.Shader).Distinct().ToList();
                }
                return m_shaders;
            }
            set
            {
                m_shaders = value;
            }
        }
        private List<ShaderBundleDto> m_shaders;

        [DataMember]
        [XmlArray("Textures")]
        [XmlArrayItem("Texture")]
        public List<TextureBundleDto> Textures
        {
            get
            {
                if (m_textures == null)
                {
                    m_textures = ShaderStats.SelectMany(s => s.TextureStats).Select(t => t.Texture).Distinct().ToList();
                }
                return m_textures;
            }
            set
            {
                m_textures = value;
            }
        }
        private List<TextureBundleDto> m_textures;
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void ResolveReferences()
        {
            foreach (ShaderStatBundleDto shaderBundle in ShaderStats)
            {
                shaderBundle.ResolveReferences(Shaders, Textures);
            }
        }
        #endregion // Public Methods
    } // GameAssetStatBundleDtoBase
}
