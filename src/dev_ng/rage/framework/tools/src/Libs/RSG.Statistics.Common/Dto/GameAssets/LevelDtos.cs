﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("Levels")]
    public class LevelDtos : IDtos<LevelDto>
    {
        [DataMember]
        [XmlElement("Level")]
        public List<LevelDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LevelDtos()
        {
            Items = new List<LevelDto>();
        }
    } // LevelDtos
}
