﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using System.Runtime.Serialization;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapSectionStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Section's identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// Whether the section exports archetypes.
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableMapSectionStat.ExportArchetypesString)]
        public bool? ExportArchetypes { get; set; }

        /// <summary>
        /// Whether the section exports entities.
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableMapSectionStat.ExportEntitiesString)]
        public bool? ExportEntities { get; set; }

        /// <summary>
        /// Map sections top level collision poly count.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.CollisionPolygonCountString)]
        public uint? CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this map section stat.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.CollisionPolygonCountBreakdownString)]
        public List<CollisionPolyStatDto> CollisionPolygonCountBreakdown { get; set; }

        /// <summary>
        /// Last person to export this section.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.LastExportUserString)]
        public String LastExportUser { get; set; }

        /// <summary>
        /// Last time this section was exported.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.LastExportTimeString)]
        public DateTime? LastExportTime { get; set; }

        /// <summary>
        /// DCC source filename (excluding extension).
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.DCCSourceFilenameString)]
        public String DCCSourceFilename { get; set; }

        /// <summary>
        /// Name of the exported zip file.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.ExportZipFilepathString)]
        public String ExportZipFilepath { get; set; }

        /// <summary>
        /// Platform stats for this map section.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.PlatformStatsString)]
        public List<MapSectionPlatformStatDto> PlatformStats { get; set; }

        /// <summary>
        /// Map section attributes.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.ContainerAttributesString)]
        public List<MapSectionAttributeStatDto> ContainerAttributes { get; set; }

        /// <summary>
        /// List of archetypes this map section contains.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.ArchetypesString)]
        [XmlArrayItem(typeof(BasicDrawableArchetypeStatDto))]
        [XmlArrayItem(typeof(BasicFragmentArchetypeStatDto))]
        [XmlArrayItem(typeof(BasicInteriorArchetypeStatDto))]
        [XmlArrayItem(typeof(BasicStatedAnimArchetypeStatDto))]
        public List<BasicArchetypeStatDto> Archetypes { get; set; }

        /// <summary>
        /// List of entities this map section contains.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.EntitiesString)]
        public List<BasicEntityStatDto> Entities { get; set; }

        /// <summary>
        /// List of car gens this map section contains.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.CarGensString)]
        public List<CarGenStatDto> CarGens { get; set; }

        /// <summary>
        /// List of car gens this map section contains.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapSectionStat.AggregatedStatsString)]
        public List<MapSectionAggregateStatDto> AggregatedStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapSectionStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public MapSectionStatDto(uint identifier)
        {
            Identifier = identifier;
        }
        #endregion // Constructor(s)
    } // MapSectionStatDto
}
