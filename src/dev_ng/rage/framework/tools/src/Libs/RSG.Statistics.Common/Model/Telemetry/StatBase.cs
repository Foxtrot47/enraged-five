﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Base class for all stats.
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class StatBase
    {
        #region Properties
        /// <summary>
        /// Unique hash of the stat.
        /// </summary>
        [DataMember]
        public uint Hash { get; set; }

        /// <summary>
        /// Name of the stat this relates to.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Friendly name for this stat.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StatBase()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public StatBase(uint hash)
        {
            Hash = hash;
        }

        /// <summary>
        /// 
        /// </summary>
        public StatBase(String name, String friendlyName)
        {
            Name = name;
            FriendlyName = friendlyName;
        }
        #endregion // Constructor(s)
    } // StatBase
}
