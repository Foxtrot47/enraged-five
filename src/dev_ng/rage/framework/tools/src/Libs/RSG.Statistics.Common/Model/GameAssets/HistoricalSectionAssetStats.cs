﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalSectionAssetStat
    {
        #region Properties
        /// <summary>
        /// Name of the section these stats are for.
        /// </summary>
        [DataMember]
        public String SectionName { get; set; }

        /// <summary>
        /// Stats grouped per build
        /// </summary>
        [DataMember]
        public Dictionary<String, AssetStat> PerBuildStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private HistoricalSectionAssetStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public HistoricalSectionAssetStat(String sectionName)
        {
            SectionName = sectionName;
            PerBuildStats = new Dictionary<String, AssetStat>();
        }
        #endregion // Constructor(s)
    } // HistoricalSectionAssetStat

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class AssetStat
    {
        #region Properties
        /// <summary>
        /// Physical size.
        /// </summary>
        [DataMember]
        public long PhysicalSize { get; set; }

        /// <summary>
        /// Virtual size.
        /// </summary>
        [DataMember]
        public long VirtualSize { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private AssetStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public AssetStat(long physical, long @virtual)
        {
            PhysicalSize = physical;
            VirtualSize = @virtual;
        }
        #endregion // Constructor(s)
    } // HistoricalSectionAssetStats
}
