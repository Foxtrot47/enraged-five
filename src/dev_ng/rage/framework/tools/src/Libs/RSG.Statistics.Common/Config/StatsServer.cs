﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;

namespace RSG.Statistics.Common.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class StatsServer : Server, IStatsServer
    {
        #region Fields
        /// <summary>
        /// Reference to the owning stats config object.
        /// </summary>
        private readonly IStatisticsConfig _statsConfig;

        /// <summary>
        /// Private field for the <see cref="WebRootDir"/> property.
        /// </summary>
        private readonly String _webRootDir;

        /// <summary>
        /// Private field for the <see cref="ReportCacheDir"/> property.
        /// </summary>
        private readonly String _reportCacheDir;
        #endregion // Fields

        #region Properties
        /// <summary>
        /// Config object parent.
        /// </summary>
        public IStatisticsConfig Config
        {
            get { return _statsConfig; }
        }

        /// <summary>
        /// Root folder where the web directories live in.
        /// </summary>
        public String WebRootDir
        {
            get { return _webRootDir; }
        }

        /// <summary>
        /// Report cache directory.
        /// </summary>
        public String ReportCacheDir
        {
            get { return _reportCacheDir; }
        }

        /// <summary>
        /// Name of the vertica server to connect to.
        /// </summary>
        public IVerticaServer VerticaServer { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default
        /// </summary>
        /// <param name="config"></param>
        /// <param name="elem"></param>
        internal StatsServer(IStatisticsConfig config, XElement elem)
            : base(elem)
        {
            _statsConfig = config;

            String webRoot = GetAttributeValue<String>(elem, "web_root_dir");
            _webRootDir = Path.GetFullPath(_statsConfig.Branch.Environment.Subst(webRoot));
            _reportCacheDir = Path.Combine(_statsConfig.ReportCacheDirectoryRoot, Name);

            String verticaServerName = GetAttributeValue<String>(elem, "vertica_server");
            IVerticaServer verticaServer;
            if (!Config.VerticaServers.TryGetValue(verticaServerName, out verticaServer))
            {
                throw new ArgumentOutOfRangeException(String.Format("Vertica server config '{0}' doesn't exist.", verticaServerName));
            }
            VerticaServer = verticaServer;
        }
        #endregion // Constructor(s)
    } // Server
}
