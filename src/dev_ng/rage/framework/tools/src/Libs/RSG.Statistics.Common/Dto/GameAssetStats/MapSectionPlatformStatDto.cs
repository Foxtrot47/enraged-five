﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.Enums;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapSectionPlatformStatDto : DtoBase
    {
        /// <summary>
        /// Platform this stat is for.
        /// </summary>
        [DataMember]
        public PlatformDto Platform { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<MapSectionMemoryStatDto> FileTypeSizes { get; set; }
    } // MapSectionPlatformStatDto
}
