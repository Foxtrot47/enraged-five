﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using RSG.Base.Math;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("LevelStatBundle")]
    public class LevelStatBundleDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String ExportDataPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String ProcessedDataPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public Vector2f LowerLeft { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public Vector2f UpperRight { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public long ImageBugstarId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public byte[] ImageData { get; set; }
    } // LevelStatBundleDto
}
