﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Dto.ProfileStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class ProfileStatDto
    {
        #region Properties
        /// <summary>
        /// String name for the bucket.
        /// </summary>
        [DataMember]
        public String Bucket { get; set; }

        /// <summary>
        /// Min bucket value.
        /// </summary>
        [DataMember]
        public double BucketMin { get; set; }

        /// <summary>
        /// Max bucket value.
        /// </summary>
        [DataMember]
        public double BucketMax { get; set; }

        /// <summary>
        /// Number of gamers.
        /// </summary>
        [DataMember]
        public String YValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Total { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (for serialisation purposes).
        /// </summary>
        public ProfileStatDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bucket"></param>
        /// <param name="yValue"></param>
        [Obsolete]
        public ProfileStatDto(String bucket, String yValue)
        {
            Bucket = bucket;
            YValue = yValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bucketStart"></param>
        /// <param name="bucketSize"></param>
        public ProfileStatDto(double bucketStart, double bucketSize)
        {
            Bucket = String.Format("{0}-{1}", bucketStart, bucketStart + bucketSize);
            BucketMin = bucketStart;
            BucketMax = bucketStart + bucketSize;
            YValue = "0";
            Total = "0";
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void AddDoubleValue(double value)
        {
            // Increment the number of contributing readings and add the value to the totals
            YValue = (Int32.Parse(YValue) + 1).ToString();
            Total = (Double.Parse(Total) + value).ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void AddLongValue(long value)
        {
            // Increment the number of contributing readings and add the value to the totals
            YValue = (Int32.Parse(YValue) + 1).ToString();
            Total = (Int64.Parse(Total) + value).ToString();
        }
        #endregion // Public Methods
    } // ProfileStatDto
}
