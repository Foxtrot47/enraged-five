﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.Statistics.Common.Model.UGC;

namespace RSG.Statistics.Common.Model.Telemetry.Online
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class MissionParticipantStat
    {
        #region Properties
        /// <summary>
        /// Name (e.g. 1 Player, 2, Players, All Players, etc..)
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Information about all the missions that were performed.
        /// </summary>
        [DataMember]
        public IList<MissionParticipantMissionStat> MissionResultStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionParticipantStat()
        {
            MissionResultStats = new List<MissionParticipantMissionStat>();
        }
        #endregion // Constructor(s)
    } // MissionParticipantStat

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class MissionParticipantMissionStat
    {
        /// <summary>
        /// UGC public content id for this mission.
        /// </summary>
        [DataMember]
        public String PublicContentId { get; set; }

        /// <summary>
        /// Number of times this mission was performed.
        /// </summary>
        [DataMember]
        public ulong TimesPerformed { get; set; }

        /// <summary>
        /// Number of times this mission was passed.
        /// </summary>
        [DataMember]
        public ulong TimesPassed { get; set; }

        /// <summary>
        /// Number of times this mission was failed.
        /// </summary>
        [DataMember]
        public ulong TimesFailed { get; set; }

        /// <summary>
        /// Number of times this mission ended as 'over'.
        /// </summary>
        [DataMember]
        public ulong TimesOver { get; set; }
    } // MissionParticipantMissionStat
}
