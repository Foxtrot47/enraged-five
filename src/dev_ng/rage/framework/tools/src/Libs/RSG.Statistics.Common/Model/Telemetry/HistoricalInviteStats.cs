﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Single invite stat.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalInviteStat : Range<DateTime>
    {
        #region Properties
        /// <summary>
        /// Number of gamers that received invites.
        /// </summary>
        [DataMember]
        public ulong GamerCount { get; set; }

        /// <summary>
        /// Number of invites that were received.
        /// </summary>
        [DataMember]
        public ulong InvitesReceived { get; set; }

        /// <summary>
        /// Number of NPC invites that were received.
        /// </summary>
        [DataMember]
        public ulong NPCInvitesReceived { get; set; }
        #endregion // Properties
    } // HistoricalInviteStat
}
