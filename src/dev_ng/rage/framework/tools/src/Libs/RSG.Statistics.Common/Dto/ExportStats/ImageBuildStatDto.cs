﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("ImageBuildStat")]
    public class ImageBuildStatDto : MapExportSubStatDto
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlArray("MapSectionIdentifiers")]
        [XmlArrayItem("MapSectionIdentifier")]
        public List<string> MapSectionIdentifiers { get; set; }
    } // ImageBuildStatDto
}
