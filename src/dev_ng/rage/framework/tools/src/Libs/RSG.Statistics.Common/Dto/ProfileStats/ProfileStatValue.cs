﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Dto.ProfileStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class ProfileStatValue
    {
        #region Properties
        /// <summary>
        /// Name of the stat.
        /// </summary>
        [DataMember]
        public String StatName { get; set; }

        /// <summary>
        /// Value for the stat.
        /// </summary>
        [DataMember]
        public String Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ProfileStatValue()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ProfileStatValue(String statName, String value)
        {
            StatName = statName;
            Value = value;
        }
        #endregion // Constructor(s)
    } // ProfileStatValue
}
