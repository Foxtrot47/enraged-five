﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Common interface for all game assets.
    /// </summary>
    /// <typeparam name="TDto">Singular version of a game asset dto.</typeparam>
    /// <typeparam name="TDtos">Plural version of a game asset dto.</typeparam>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IGameAssetServiceBase<TDto, TDtos>
        where TDto : IDto
        where TDtos : IDtos<TDto>
    {
        /// <summary>
        /// Returns a list of all game assets
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "All")]
        TDtos GetAll();

        /// <summary>
        /// Gets a game asset based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{identifier}")]
        TDto GetByIdentifier(string identifier);

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "All")]
        void PutAssets(TDtos dtos);

        /// <summary>
        /// Updates or creates a game asset based on its identifier
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{identifier}")]
        TDto PutAsset(string identifier, TDto asset);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "{identifier}")]
        void DeleteAsset(string identifier);
    } // IGameAssetServiceBase<TSingular, TPlural>
}
