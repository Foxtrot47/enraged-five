﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Base.Math;
using System.Runtime.Serialization;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SpawnPointStatDto : DtoBase
    {
        /// <summary>
        /// Name of the spawn point.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Where this spawn point is.
        /// </summary>
        [DataMember]
        public Vector3f Position { get; set; }

        /// <summary>
        /// Level stat this spawn point is associated with (if any).
        /// </summary>
        public String LevelIdentifier { get; set; }

        /// <summary>
        /// Archetype stat this spawn point is associated with (if any).
        /// </summary>
        public String ArchetypeIdentifier { get; set; }

        /// <summary>
        /// Source file this spawn point came from.
        /// </summary>
        [DataMember]
        public String SourceFile { get; set; }

        /// <summary>
        /// Type of spawn point this is.
        /// </summary>
        [DataMember]
        public String SpawnType { get; set; }

        /// <summary>
        /// Group spawn point this in.
        /// </summary>
        [DataMember]
        public String SpawnGroup { get; set; }

        /// <summary>
        /// Model set associated with the spawn type.
        /// </summary>
        [DataMember]
        public String ModelSet { get; set; }

        /// <summary>
        /// Game modes this spawn point is enabled in.
        /// </summary>
        [DataMember]
        public SpawnPointAvailableModes? AvailabilityMode { get; set; }

        /// <summary>
        /// Start time active.
        /// </summary>
        [DataMember]
        public int? StartTime { get; set; }

        /// <summary>
        /// End time active.
        /// </summary>
        [DataMember]
        public int? EndTime { get; set; }
    } // SpawnPointStatDto
}
