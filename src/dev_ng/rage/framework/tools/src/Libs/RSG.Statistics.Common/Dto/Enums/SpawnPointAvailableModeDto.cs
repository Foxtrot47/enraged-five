﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("SpawnPointAvailabilityMode")]
    public class SpawnPointAvailableModeDto : EnumDto<SpawnPointAvailableModes>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SpawnPointAvailableModeDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lodLevel"></param>
        public SpawnPointAvailableModeDto(SpawnPointAvailableModes availabilityMode, String name)
            : base(availabilityMode, name)
        {
        }
        #endregion // Constructor(s)
    } // SpawnPointAvailableModeDto
}
