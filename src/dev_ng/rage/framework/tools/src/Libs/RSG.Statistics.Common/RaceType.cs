﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Additional race type.
    /// The values should match those defined in //depot/gta5/script/dev/multiplayer/globals/MP_globals_FM.sch.
    /// </summary>
    public enum RaceType
    {
        Standard = 0,
        Gta = 1,
        Rally = 2,

        Unknown
    } // RaceType
}
