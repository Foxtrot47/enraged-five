﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ShopItem : FriendlyGameAsset
    {
        #region Properties
        /// <summary>
        /// Category this item falls under.
        /// </summary>
        [DataMember]
        public String Category { get; set; }

        /// <summary>
        /// Sub-category this item falls under.
        /// </summary>
        [DataMember]
        public String SubCategory { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private ShopItem()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ShopItem(String gameName, String friendlyName, String category, String subCategory)
            : base(gameName, friendlyName)
        {
            Category = category;
            SubCategory = subCategory;
        }
        #endregion // Constructor(s)
    } // ShopItem
}
