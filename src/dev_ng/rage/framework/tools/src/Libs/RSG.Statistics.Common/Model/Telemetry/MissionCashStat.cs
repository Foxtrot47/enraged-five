﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class MissionCashStat
    {
        #region Properties
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Identifier of the mission.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Amount of cash the player had at the end of the mission.
        /// </summary>
        [DataMember]
        public uint Cash { get; set; }
        #endregion // Properties
    } // MissionCashStat
}
