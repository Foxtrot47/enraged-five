﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Async;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IQueryAsyncService
    {
        /// <summary>
        /// Queries the current status of a task that is running asynchronously.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "QueryAsync?guid={guid}")]
        [ServiceKnownType("GetKnownTypes", typeof(AsyncCommandResult))]
        IAsyncCommandResult Query(string guid);
    } // IQueryAsyncService
}
