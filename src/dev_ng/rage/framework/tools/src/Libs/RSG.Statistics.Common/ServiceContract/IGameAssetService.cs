﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RSG.Model.Common.Mission;
using RSG.Model.Common.Animation;
using RSG.Statistics.Common.Model.GameAssets;
using System.Reflection;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(GameAssetKnownTypeProvider))]
    public interface IGameAssetService
    {
        #region Animations
        /// <summary>
        /// Updates or creates a list of ClipDictionaries ( animations ).
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ClipDictionaries")]
        void UpdateClipDictionaries(IClipDictionaryCollection clipDictionaries);
        #endregion // Animations

        #region Cash Packs
        /// <summary>
        /// Updates the list of cash-packs that are currently active in game.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "CashPacks")]
        void UpdateCashPacks(IList<CashPack> cashPacks);
        #endregion // Cash Packs

        #region Cutscenes
        /// <summary>
        /// Updates or creates a list of cutscenes.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Cutscenes")]
        void UpdateCutscenes(ICutsceneCollection cutscenes);
        #endregion // Cutscenes

        #region Draw Lists
        /// <summary>
        /// Updates the list of draw lists.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "DrawLists")]
        void UpdateDrawLists(IList<GameAsset> drawLists);

        /// <summary>
        /// Returns a list of all draw lists.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "DrawLists")]
        IList<GameAsset> GetDrawLists();
        #endregion // Draw Lists

        #region Memory Pools, Stores & Heaps
        /// <summary>
        /// Updates or creates a dictionary of memory pool hashes to memory pool names.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "MemoryPools")]
        void UpdateMemoryPools(IDictionary<uint, String> memoryPoolsDict);

        /// <summary>
        /// Updates or creates a dictionary of memory store hashes to memory store names.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "MemoryStores")]
        void UpdateMemoryStores(IDictionary<uint, String> memoryStoresDict);

        /// <summary>
        /// Updates or creates a dictionary of memory heap hashes to memory heap names.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "MemoryHeaps")]
        void UpdateMemoryHeaps(IDictionary<uint, String> memoryHeapsDict);
        #endregion //  Memory Pools & Stores

        #region Mini-Game Variants
        /// <summary>
        /// Updates the list of mini-game variants.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "MiniGameVariants")]
        void UpdateMiniGameVariants(IList<MiniGameVariant> miniGames);
        #endregion // Mini-Game Variants

        #region Missions
        /// <summary>
        /// Returns a list of all missions.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Missions")]
        IMissionCollection GetMissions();

        /// <summary>
        /// Updates or creates a list of missions.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Missions")]
        void UpdateMissions(IMissionCollection missions);

        /// <summary>
        /// Deletes a single mission.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Missions/{identifier}")]
        void DeleteMission(String identifier);
        #endregion // Missions

        #region Movies
        /// <summary>
        /// Updates the list of movies.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Movies")]
        void UpdateMovies(IList<FriendlyGameAsset> movies);
        #endregion // Movies

        #region Properties
        /// <summary>
        /// Updates the list of properties.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Properties")]
        void UpdateProperties(IDictionary<uint, String> properties);
        #endregion // Properties

        #region Ranks
        /// <summary>
        /// Updates the list of ranks.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Ranks")]
        void UpdateRanks(IList<Rank> ranks);
        #endregion // Ranks

        #region Radio Stations
        /// <summary>
        /// Updates the list of TV shows.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "RadioStations")]
        void UpdateRadioStations(IList<FriendlyGameAsset> radioStations);
        #endregion // Radio Stations

        #region Shopping
        /// <summary>
        /// Updates all shop names.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Shops")]
        void UpdateShops(IList<Shop> shops);

        /// <summary>
        /// Updates all shop items.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ShopItems")]
        void UpdateShopItems(IList<ShopItem> shopItems);

        /// <summary>
        /// Returns a list of all shop items.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "ShopItems")]
        IList<ShopItem> GetShopItems();
        #endregion // Shopping

        #region TV Shows
        /// <summary>
        /// Updates the list of TV Shows.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "TVShows")]
        void UpdateTVShows(IList<FriendlyGameAsset> tvShows);
        #endregion // TV Shows

        #region Unlocks
        /// <summary>
        /// Updates the list of unlocks.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Unlocks")]
        void UpdateUnlocks(IList<Unlock> unlocks);
        #endregion // Unlocks

        #region Vehicles
        /// <summary>
        /// Returns a list of all vehicles.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Vehicles")]
        List<Vehicle> GetVehicles();
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// Returns a list of all weapons.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Weapons")]
        List<Weapon> GetWeapons();
        #endregion // Weapons

        #region Websites
        /// <summary>
        /// Updates the list of websites.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Websites")]
        void UpdateWebsites(IDictionary<uint, String> websites);
        #endregion // Websites

        #region XP
        /// <summary>
        /// Updates the list of XP categories.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "XPCategories")]
        void UpdateXPCategories(IList<FriendlyGameAsset> categories);

        /// <summary>
        /// Updates the list of XP types.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "XPTypes")]
        void UpdateXPTypes(IList<FriendlyGameAsset> types);
        #endregion // XP
    } // IGameAssetService

    /// <summary>
    /// Known type provider for the game asset service.
    /// </summary>
    internal static class GameAssetKnownTypeProvider
    {
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            // Add any types to include here.
            // These can be defined in another assembly (which doesn't have to be referenced statically).
            yield return Type.GetType("RSG.Model.Common.Mission.MissionCollection,RSG.Model.Common");
            yield return Type.GetType("RSG.Model.Common.Animation.ClipDictionaryCollection,RSG.Model.Common");
            yield return Type.GetType("RSG.Model.Animation.LocalCutsceneCollection,RSG.Model.Animation");
        }
    } // GameAssetKnownTypeProvider
}
