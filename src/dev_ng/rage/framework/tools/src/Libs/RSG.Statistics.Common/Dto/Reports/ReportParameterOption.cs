﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.Reports
{
    /// <summary>
    /// Option for a particular parameter.
    /// </summary>
    [DataContract]
    public class ReportParameterOption : IDto
    {
        #region Properties
        /// <summary>
        /// Key for this parameter.
        /// </summary>
        [DataMember]
        public String Key { get; set; }

        /// <summary>
        /// Value to use for this parameter.
        /// </summary>
        [DataMember]
        public String Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (required for serialisation).
        /// </summary>
        public ReportParameterOption()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public ReportParameterOption(String value)
            : this(null, value)
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public ReportParameterOption(String key, String value)
        {
            Key = key;
            Value = value;
        }
        #endregion // Constructor(s)
    } // ReportParameterOption
}
