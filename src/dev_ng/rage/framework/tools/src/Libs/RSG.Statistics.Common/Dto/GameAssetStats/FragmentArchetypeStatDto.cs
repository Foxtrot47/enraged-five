﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class FragmentArchetypeStatDto : SimpleMapArchetypeStatDto
    {
        #region Properties
        /// <summary>
        /// Number of fragments this archetype is made up of.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableFragmentArchetypeStat.FragmentCountString)]
        public uint? FragmentCount { get; set; }

        /// <summary>
        /// Whether this fragment is a piece of cloth.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableFragmentArchetypeStat.IsClothString)]
        public bool? IsCloth { get; set; }
        #endregion // Properties
    } // FragmentArchetypeStatDto
}
