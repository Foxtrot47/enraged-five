﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry.PerUser
{
    /// <summary>
    /// Concrete version for wcf serialisation purposes.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserCheatStats : PerUserStat<List<CheatStat>>
    {
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserCheatStats()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserCheatStats(String gamerHandle, ROSPlatform platform)
            : base(gamerHandle, platform, new List<CheatStat>())
        {
        }
        #endregion // Constructor(s)
    } // PerUserCheatStats
}
