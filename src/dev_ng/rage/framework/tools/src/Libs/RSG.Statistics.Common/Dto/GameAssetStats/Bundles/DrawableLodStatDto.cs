﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Model.Common.Map;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DrawableLodStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Name of the lod object.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Lod distance this kicks in at.
        /// </summary>
        [DataMember]
        public float LodDistance { get; set; }

        /// <summary>
        /// Poly count of the lod object.
        /// </summary>
        [DataMember]
        public uint PolygonCount { get; set; }

        /// <summary>
        /// Lod level associated with this object.
        /// </summary>
        [DataMember]
        public MapDrawableLODLevel LodLevel { get; set; }
        #endregion // Properties
    
        #region Constructor(s)
        /// <summary>
        /// For serialisation
        /// </summary>
        public DrawableLodStatDto()
            : base()
        {
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public DrawableLodStatDto(string name, float lodDistance, uint polyCount)
            : base()
        {
            Name = name;
            LodDistance = lodDistance;
            PolygonCount = polyCount;
        }
        #endregion // Constructor(s)
    } // MapDrawableLodStatDto
}
