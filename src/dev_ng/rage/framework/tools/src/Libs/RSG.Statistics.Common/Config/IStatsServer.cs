﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Common.Config
{
    /// <summary>
    /// Encapsulates all information relating to a single statistics server.
    /// </summary>
    public interface IStatsServer : IServer
    {
        #region Properties
        /// <summary>
        /// Root folder where the web directories live in.
        /// </summary>
        String WebRootDir { get; }

        /// <summary>
        /// Report cache directory.
        /// </summary>
        String ReportCacheDir { get; }

        /// <summary>
        /// Vertica server config information.
        /// </summary>
        IVerticaServer VerticaServer { get; }

        /// <summary>
        /// Config object parent.
        /// </summary>
        IStatisticsConfig Config { get; }
        #endregion // Properties
    } // IServer
}
