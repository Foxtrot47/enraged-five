﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Vehicle
    {
        /// <summary>
        /// Unique identifier for this vehicle.
        /// </summary>
        [DataMember]
        public String Identifier { get; set; }

        /// <summary>
        /// Name that runtime use to identify this vehicle.
        /// </summary>
        [DataMember]
        public String RuntimeName { get; set; }

        /// <summary>
        /// Friendly name of the vehicle as shown in game.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        /// Name of the model used for this vehicle in game.
        /// </summary>
        [DataMember]
        public String ModelName { get; set; }

        /// <summary>
        /// Category that this vehicle falls under.
        /// </summary>
        [DataMember]
        public VehicleCategory Category { get; set; }
    } // Vehicle
}
