﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CashPack : GameAsset
    {
        /// <summary>
        /// Amount of in game cash that this cash pack provides.
        /// </summary>
        [DataMember]
        public uint InGameAmount { get; set; }

        /// <summary>
        /// Flag indicating whether this cash pack is currently available in game.
        /// </summary>
        [DataMember]
        public bool IsActive { get; set; }
    } // CashPack
}
