﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry.PerUser
{
    /// <summary>
    /// Concrete version for wcf serialisation purposes.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserPropChangeStat : PerUserStat<ulong>
    {
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserPropChangeStat()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserPropChangeStat(String gamerHandle, ROSPlatform platform)
            : base(gamerHandle, platform, 0)
        {
        }
        #endregion // Constructor(s)
    } // PerUserClothesChangeStats
}
