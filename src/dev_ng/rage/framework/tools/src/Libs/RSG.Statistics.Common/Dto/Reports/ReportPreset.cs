﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.Reports
{
    /// <summary>
    /// Report preset.
    /// </summary>
    [DataContract]
    public class ReportPreset
    {
        #region Properties
        /// <summary>
        /// Database id for this preset.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Name for this preset.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// List of parameters associated with this preset.
        /// </summary>
        [DataMember]
        public List<ReportPresetParameter> Parameters { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ReportPreset()
        {
            Parameters = new List<ReportPresetParameter>();
        }

        /// <summary>
        /// 
        /// </summary>
        public ReportPreset(String name)
            : this()
        {
            Name = name;
        }
        #endregion // Constructor(s)
    } // ReportPreset
}
