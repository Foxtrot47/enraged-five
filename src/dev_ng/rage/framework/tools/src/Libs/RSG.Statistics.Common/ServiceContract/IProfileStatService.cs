﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RSG.Statistics.Common.Dto.ProfileStats;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Profile stat related service.
    /// </summary>
    [ServiceContract]
    public interface IProfileStatService
    {
        /// <summary>
        /// Updates the profile stat definitions that the server is aware of.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Definitions")]
        void UpdateDefinitions(IList<ProfileStatDefinitionDto> definitions);

        /// <summary>
        /// Retrieves the list of active definitions.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Definitions")]
        IList<ProfileStatDefinitionDto> GetActiveDefinitions();
    } // IProfileStatService
}
