﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Shop : FriendlyGameAsset
    {
        #region Properties
        /// <summary>
        /// Type of shop this is.
        /// </summary>
        [DataMember]
        public ShopType ShopType { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private Shop()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public Shop(String gameName, String friendlyName, ShopType shopType)
            : base(gameName, friendlyName)
        {
            ShopType = shopType;
        }
        #endregion // Constructor(s)
    } // Shop
}
