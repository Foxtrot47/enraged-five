﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// Mission checkpoint performance information.
    /// - this will find its mission context as a member of the MissionPerformance object
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionCheckpointPerformance : FpsPerformanceStat
    {
        /// <summary>
        /// Name of the mission
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }
        
        /// <summary>
        /// Name of the mission checkpoint
        /// </summary>
        [DataMember]
        public String CheckpointName { get; set; }
    } // MissionCheckpointPerformance
}
