﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.Enums;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapSectionMemoryStatDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public FileTypeDto FileType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public MemoryStatDto MemorySat { get; set; }
    } // MapSectionMemoryStatDto
}
