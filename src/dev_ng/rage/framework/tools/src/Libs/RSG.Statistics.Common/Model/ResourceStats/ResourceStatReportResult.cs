﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Statistics.Platform;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.ResourceStats
{
    /// <summary>
    /// Resource Stats Report Result
    /// - The report generated from a ResourceStatReport
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class ResourceStatReportResult
    {
        /// <summary>
        /// The Total Virtual Size (bytes) of all virtual buckets
        /// </summary>
        [DataMember]
        public long TotalVirtualSize { get; set; }

        /// <summary>
        /// The Total Virtual Capacity (bytes) of all virtual buckets
        /// </summary>
        [DataMember]
        public long TotalVirtualCapacity { get; set; }

        /// <summary>
        /// The Total Physical Size (bytes) of all virtual buckets
        /// </summary>
        [DataMember]
        public long TotalPhysicalSize { get; set; }

        /// <summary>
        /// The Total Physical Capacity (bytes) of all virtual buckets
        /// </summary>
        [DataMember]
        public long TotalPhysicalCapacity { get; set; }

        /// <summary>
        /// Resource stats
        /// </summary>
        [DataMember]
        public IEnumerable<ResourceStat> ResourceStats { get; set; }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        /// <param name="resourceStats"></param>
        /// <param name="storeDetail"></param>
        public ResourceStatReportResult(IEnumerable<ResourceStat> resourceStats, bool storeDetail = true)
        {
            // Calculate totals for report
            TotalPhysicalSize = 0;
            TotalPhysicalCapacity = 0;

            TotalVirtualSize = 0;
            TotalVirtualCapacity = 0;

            foreach (ResourceStat resourceStat in resourceStats)
            {
                TotalPhysicalSize += resourceStat.PhysicalSize;
                TotalPhysicalCapacity += resourceStat.PhysicalCapacity;

                TotalVirtualSize += resourceStat.VirtualSize;
                TotalVirtualCapacity += resourceStat.VirtualCapacity;
            }

            if (storeDetail)
            {
                ResourceStats = resourceStats;
            }
        }

    } // ResourceStatReportResult
}
