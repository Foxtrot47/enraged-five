﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("ShaderStatBundle")]
    public class ShaderStatBundleDto : DtoBase
    {
        #region Properties
        [XmlIgnore]
        public ShaderBundleDto Shader
        {
            get;
            set;
        }

        [XmlIgnore]
        public List<TextureStatBundleDto> TextureStats
        {
            get;
            set;
        }

        [DataMember]
        [XmlElement("ShaderHash")]
        public long ShaderHash
        {
            get
            {
                if (m_shaderHash == null)
                {
                    m_shaderHash = (long)Shader.Hash;
                }
                return (long)m_shaderHash;
            }
            set
            {
                m_shaderHash = value;
            }
        }
        private long? m_shaderHash;

        [DataMember]
        [XmlArray("Textures")]
        [XmlArrayItem("Texture")]
        public List<long> Textures
        {
            get
            {
                if (m_textures == null && TextureStats != null)
                {
                    m_textures = TextureStats.Select(t => (long)t.TextureHash).ToList();
                }
                return m_textures;
            }
            set
            {
                m_textures = value;
            }
        }
        private List<long> m_textures;
        #endregion // Properties

        #region Public Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="shaders"></param>
        /// <param name="textures"></param>
        public void ResolveReferences(List<ShaderBundleDto> shaderBundles, List<TextureBundleDto> textureBundles)
        {
            Shader = shaderBundles.FirstOrDefault(item => item.Hash == ShaderHash);

            // Resolve the textures
            Dictionary<long, TextureBundleDto> textureLookup = new Dictionary<long, TextureBundleDto>();
            foreach (TextureBundleDto textureBundle in textureBundles)
            {
                if (!textureLookup.ContainsKey(textureBundle.Hash))
                {
                    textureLookup.Add(textureBundle.Hash, textureBundle);
                }
            }

            TextureStats = new List<TextureStatBundleDto>();
            foreach (long textureHash in Textures)
            {
                TextureStatBundleDto bundle = new TextureStatBundleDto();
                bundle.Texture = textureLookup[textureHash];
                TextureStats.Add(bundle);
            }
        }
        #endregion // Public Interface
    } // ShaderStatBundleDto
}
