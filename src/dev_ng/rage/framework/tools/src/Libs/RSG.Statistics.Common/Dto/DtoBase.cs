﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.Reflection;

namespace RSG.Statistics.Common.Dto
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class DtoBase : IDto
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlAnyElement]
        public XmlElement[] AllElements
        {
            get;
            set;
        }
        #endregion // Properties
    } // DtoBase
}
