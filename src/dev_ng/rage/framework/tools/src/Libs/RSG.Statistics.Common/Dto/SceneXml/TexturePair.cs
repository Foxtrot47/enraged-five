﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.SceneXml
{
    /// <summary>
    /// Returns a texture/alpha texture pair for determining texture export paths.
    /// </summary>
    [DataContract]
    public class TexturePair
    {
        /// <summary>
        /// Texture file path.
        /// </summary>
        [DataMember]
        public String TexturePath { get; set; }

        /// <summary>
        /// Optional alpha texture path.
        /// </summary>
        [DataMember]
        public String AlphaTexturePath { get; set; }
    }
}
