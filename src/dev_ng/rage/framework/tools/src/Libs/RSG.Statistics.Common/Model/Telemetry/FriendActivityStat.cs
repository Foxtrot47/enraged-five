﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Report return type for the per user friend activity report.
    /// </summary>
    [DataContract]
    [Serializable]
    public class FriendActivityStat
    {
        #region Properties
        /// <summary>
        /// Character that did the activity.
        /// </summary>
        [DataMember]
        public int Character { get; set; }

        /// <summary>
        /// Number of times the user successfully passed the activity.
        /// </summary>
        [DataMember]
        public uint SuccessCount { get; set; }

        /// <summary>
        /// Number of times the user failed the activity.
        /// </summary>
        [DataMember]
        public uint FailedCount { get; set; }

        /// <summary>
        /// Number of times the user aborted the activity.
        /// </summary>
        [DataMember]
        public uint AbortedCount { get; set; }
        #endregion // Properties
    } // FriendActivityStat
}
