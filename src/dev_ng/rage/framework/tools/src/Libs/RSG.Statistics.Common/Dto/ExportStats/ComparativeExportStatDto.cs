﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// Contains information relating to the comparative export stat report.
    /// </summary>
    [Serializable]
    public class ComparativeExportStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// User that performed the export.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Section that was exported.
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// Time spent exporting the container.
        /// </summary>
        [XmlIgnore()]
        public TimeSpan? ExportTime { get; set; }

        /// <summary>
        /// Surrogate for serialisation purposes.
        /// </summary>
        public long? ExportTimeTicks
        {
            get { return (ExportTime.HasValue ? ExportTime.Value.Ticks : (long?)null); }
            set { ExportTime = (value != null ? new TimeSpan((long)value) : (TimeSpan?)null); }
        }

        /// <summary>
        /// Time spent building when the container was last exported.
        /// </summary>
        [XmlIgnore()]
        public TimeSpan? BuildTime { get; set; }

        /// <summary>
        /// Surrogate for serialisation purposes.
        /// </summary>
        public long? BuildTimeTicks
        {
            get { return (BuildTime.HasValue ? BuildTime.Value.Ticks : (long?)null); }
            set { BuildTime = (value != null ? new TimeSpan((long)value) : (TimeSpan?)null); }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ExportDate { get; set; }

        /// <summary>
        /// Total time spent Totaling the container.
        /// </summary>
        [XmlIgnore()]
        public TimeSpan? TotalTime { get; set; }

        /// <summary>
        /// Surrogate for serialisation purposes.
        /// </summary>
        public long? TotalTimeTicks
        {
            get { return (TotalTime.HasValue ? TotalTime.Value.Ticks : (long?)null); }
            set { TotalTime = (value != null ? new TimeSpan((long)value) : (TimeSpan?)null); }
        }
        #endregion
    } // ComparativeExportStatDto
}
