﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Base class for freemode match based data.
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class FreemodeMatchStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public MatchType MatchType { get; set; }

        /// <summary>
        /// Friendly name of the variant.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Name of who created this variant (either Rockstar or a players gamer handle).
        /// </summary>
        [DataMember]
        public String Creator { get; set; }

        /// <summary>
        /// Starting coordinates for the match.
        /// </summary>
        [DataMember]
        public Vector3f StartCoordinates { get; set; }

        /// <summary>
        /// Surrogate properties for creation from vertica.
        /// </summary>
        public float StartX
        {
            set { StartCoordinates.X = value; }
        }

        public float StartY
        {
            set { StartCoordinates.Y = value; }
        }

        public float StartZ
        {
            set { StartCoordinates.Z = value; }
        }

        /// <summary>
        /// Amount of time spent playing this variant (in seconds).
        /// </summary>
        [DataMember]
        public ulong TimeSpentPlaying { get; set; }

        /// <summary>
        /// Amount of time spent playing this variant (in seconds) where the player played to completion.
        /// </summary>
        [DataMember]
        public ulong TimeSpentPlayingToCompletion { get; set; }

        /// <summary>
        /// Number of times that players have played this variant.
        /// </summary>
        [DataMember]
        public ulong TimesPlayed { get; set; }

        /// <summary>
        /// Number of times that players have played this variant to completion.
        /// </summary>
        [DataMember]
        public ulong TimesPlayedToCompletion { get; set; }

        /// <summary>
        /// Number of unique gamers that played this variant.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Total number of gamers that took part in this variant.
        /// </summary>
        [DataMember]
        public ulong TotalGamers { get; set; }

        /// <summary>
        /// Total amount of XP players made for this variant.
        /// </summary>
        [DataMember]
        public decimal TotalXP { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FreemodeMatchStatBase()
        {
            StartCoordinates = new Vector3f();
        }
        #endregion // Constructor(s)
    } // FreemodeMatchBase
}
