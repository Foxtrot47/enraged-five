﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Archetypes")]
    public class ArchetypeDtos : IDtos<ArchetypeDto>
    {
        [XmlArrayItem(typeof(DrawableArchetypeDto))]
        [XmlArrayItem(typeof(FragmentArchetypeDto))]
        [XmlArrayItem(typeof(InteriorArchetypeDto))]
        [XmlArrayItem(typeof(StatedAnimArchetypeDto))]
        public List<ArchetypeDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ArchetypeDtos()
        {
            Items = new List<ArchetypeDto>();
        }
    } // ArchetypeDtos
}
