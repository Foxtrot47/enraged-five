﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionAttemptStat
    {
        #region Properties
        /// <summary>
        /// DON'T USE, internal to stats server.
        /// </summary>
        public long MissionId { get; set; }

        /// <summary>
        /// Name of the mission for this attempt.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Category that this mission belongs to.
        /// </summary>
        [DataMember]
        public String MissionCategory { get; set; }

        /// <summary>
        /// Number of unique players that attempted this mission.
        /// </summary>
        [DataMember]
        public int PlayersAttempted { get; set; }

        /// <summary>
        /// Number of times this mission was attempted.
        /// </summary>
        [DataMember]
        public int NumberOfAttempts { get; set; }

        /// <summary>
        /// Projected attempts.
        /// </summary>
        [DataMember]
        public float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of projected attempts.
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of projected attempts.
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Minimum time an user spent on this mission.
        /// </summary>
        [DataMember]
        public long MinMissionTime { get; set; }

        /// <summary>
        /// Average time users spent on this mission.
        /// </summary>
        [DataMember]
        public double AvgMissionTime { get; set; }

        /// <summary>
        /// Maximum time an user spent on this mission.
        /// </summary>
        [DataMember]
        public long MaxMissionTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<CheckpointAttemptStat> CheckpointAttemptStats { get; set; }

        /// <summary>
        /// List of comments that were made for this checkpoint.
        /// </summary>
        [DataMember]
        public List<PlaythroughComment> Comments { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionAttemptStat()
        {
            CheckpointAttemptStats = new List<CheckpointAttemptStat>();
            Comments = new List<PlaythroughComment>();
        }
        #endregion // Constructor(s)
    } // MissionAttempts
}
