﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Rooms")]
    public class RoomDtos : IDtos<RoomDto>
    {
        [XmlElement("Room")]
        public List<RoomDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RoomDtos()
        {
            Items = new List<RoomDto>();
        }
    } // RoomDtos
}
