﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Online
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class WeatherStat
    {
        /// <summary>
        /// Weather type.
        /// </summary>
        [DataMember]
        public WeatherType Type { get; set; }

        /// <summary>
        /// Average amount of time players took to reach this rank.
        /// </summary>
        [DataMember]
        public double AvgDuration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan AvgDurationSurrogate
        {
            set
            {
                AvgDuration = value.TotalSeconds;
            }
        }

        /// <summary>
        /// Number of samples that contributed to the readings.
        /// </summary>
        [DataMember]
        public ulong SampleSize { get; set; }
    } // WeatherStat
}
