﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.VerticaData
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class Clan : IEquatable<Clan>
    {
        #region Properties
        /// <summary>
        /// Name of this clan.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Unique id for this clan.
        /// </summary>
        [DataMember]
        public ulong Id { get; set; }
        #endregion // Properties

        #region IEquatable<Clan> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(Clan other)
        {
            if (other == null)
            {
                return false;
            }

            return (Id == other.Id);
        }
        #endregion // IEquatable<Gamer> Interface

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        #endregion // Object Overrides
    } // Clan
}
