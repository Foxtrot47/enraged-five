﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Base.Math;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(DrawableArchetypeStatBundle))]
    [KnownType(typeof(FragmentArchetypeStatBundle))]
    [KnownType(typeof(InteriorArchetypeStatBundle))]
    [KnownType(typeof(StatedAnimArchetypeStatBundle))]
    public abstract class ArchetypeStatBundle : GameAssetStatBundleDtoBase
    {
        #region Properties
        /// <summary>
        /// Identifier for this archetype.
        /// </summary>
        [DataMember]
        public string ArchetypeIdentifier { get; set; }

        /// <summary>
        /// Bounding box.
        /// </summary>
        [DataMember]
        public BoundingBox3f BoundingBox { get; set; }

        /// <summary>
        /// Archetype's lod distance.
        /// </summary>
        [DataMember]
        public float LodDistance { get; set; }

        /// <summary>
        /// Size of the exported geometry.
        /// </summary>
        [DataMember]
        public uint ExportGeometrySize { get; set; }

        /// <summary>
        /// List of txds to sizes.
        /// </summary>
        [DataMember]
        public List<TxdExportStatDto> TxdExportSizes { get; set; }

        /// <summary>
        /// Archetype's polygon count.
        /// </summary>
        [DataMember]
        public uint PolygonCount { get; set; }

        /// <summary>
        /// Collision polygon count.
        /// </summary>
        [DataMember]
        public uint CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this archetype stat.
        /// </summary>
        [DataMember]
        public List<CollisionPolyStatDto> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// Whether the archetype has a light attached to it.
        /// </summary>
        [DataMember]
        public bool HasAttachedLight { get; set; }

        /// <summary>
        /// Whether the archetype has an explosive effect attached to it.
        /// </summary>
        [DataMember]
        public bool HasExplosiveEffect { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ArchetypeStatBundle()
            : base()
        {
            CollisionTypePolygonCounts = new List<CollisionPolyStatDto>();
            TxdExportSizes = new List<TxdExportStatDto>();
        }
        #endregion // Constructor(s)
    } // ArchetypeStatBundleDto
}
