﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Mission;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MissionAttemptSubmission
    {
        #region Properties
        /// <summary>
        /// Unique identifier for this mission attempt.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Unique mission attempt id that is provided by the game.
        /// </summary>
        [DataMember]
        public ulong UniqueIdentifier { get; set; }

        /// <summary>
        /// How we are going to identify this mission in the database.
        /// </summary>
        [DataMember]
        public String MissionIdentifier { get; set; }

        /// <summary>
        /// Name of the gamer that this is associated with.
        /// </summary>
        [DataMember]
        public String GamerHandle { get; set; }

        /// <summary>
        /// Result of the mission.
        /// </summary>
        [DataMember]
        public MissionAttemptResult Result { get; set; }

        /// <summary>
        /// Comment the user added for this mission.
        /// </summary>
        [DataMember]
        public String UserComment { get; set; }

        /// <summary>
        /// When the user started this mission.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// When the user finished this mission.
        /// </summary>
        [DataMember]
        public DateTime? End { get; set; }

        /// <summary>
        /// How long the user took to complete this mission (taking into account whether the game was paused).
        /// </summary>
        [DataMember]
        public uint TimeToComplete { get; set; }

        /// <summary>
        /// Whether this mission attempt should be ignored.
        /// </summary>
        [DataMember]
        public bool Ignore { get; set; }
        
        /// <summary>
        /// Rating (out of 5) for this mission.
        /// </summary>
        [DataMember]
        public uint? Rating { get; set; }
        #endregion // Properties
    } // MissionAttemptSubmission
}
