﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using RSG.Model.Common.Report;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Model.Common;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class VehicleUsageStat : StatBase
    {
        /// <summary>
        /// Value associated with this stat.
        /// </summary>
        [DataMember]
        public decimal DistanceDriven { get; set; }

        /// <summary>
        /// Number of times this vehicle has been driven.
        /// </summary>
        [DataMember]
        public decimal TimesDriven { get; set; }

        /// <summary>
        /// Number of unique gamers that have driven this vehicle.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }
    } // VehicleCategoryStatDto
}
