﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information for a single freemode ambient mission type.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FreemodeAmbientMissionSummary
    {
        /// <summary>
        /// Name of the ambient mission type.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Number of times that players have performed this ambient mission.
        /// </summary>
        [DataMember]
        public int TimesPerformed { get; set; }

        /// <summary>
        /// Number of unique gamers that have performed this ambient mission.
        /// </summary>
        [DataMember]
        public int NumUniqueGamersPerformed { get; set; }
    } // FreemodeAmbientMissionSummary
}
