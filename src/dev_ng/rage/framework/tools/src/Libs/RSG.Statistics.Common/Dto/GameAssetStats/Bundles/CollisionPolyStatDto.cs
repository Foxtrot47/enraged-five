﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CollisionPolyStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Type of collision this stat is for.
        /// </summary>
        [DataMember]
        public uint CollisionFlags { get; set; }

        /// <summary>
        /// Type of primitve this is for.
        /// </summary>
        [DataMember]
        public CollisionPrimitiveType? PrimitiveType { get; set; }

        /// <summary>
        /// Polygon count for the specified collision flags.
        /// </summary>
        [DataMember]
        public uint PolygonCount { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation
        /// </summary>
        public CollisionPolyStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="count"></param>
        public CollisionPolyStatDto(uint flags, uint count)
            : base()
        {
            CollisionFlags = flags;
            PolygonCount = count;
        }
        #endregion // Constructor(s)
    } // CollisionPolyStatDto
}
