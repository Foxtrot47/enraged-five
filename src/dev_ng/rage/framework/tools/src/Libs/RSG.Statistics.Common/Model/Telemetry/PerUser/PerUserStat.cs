﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry.PerUser
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class PerUserStat<TStat> where TStat : new()
    {
        #region Properties
        /// <summary>
        /// Name of the user these stats are for.
        /// </summary>
        [DataMember]
        public String GamerHandle { get; set; }

        /// <summary>
        /// Account id associated for this user.
        /// </summary>
        [DataMember]
        public uint AccountId { get; set; }

        /// <summary>
        /// Platform this gamer handle is for.
        /// </summary>
        [DataMember]
        public ROSPlatform Platform { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public TStat Data { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public PerUserStat(String gamerHandle, ROSPlatform platform)
            : this(gamerHandle, platform, new TStat())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public PerUserStat(uint accountId, ROSPlatform platform)
        {
            AccountId = accountId;
            Platform = platform;
            Data = new TStat();
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserStat(String gamerHandle, ROSPlatform platform, TStat data)
        {
            GamerHandle = gamerHandle;
            Platform = platform;
            Data = data;
        }
        #endregion // Constructor(s)
    } // PerUserStatDto<TStat>
}
