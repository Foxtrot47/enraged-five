﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.SocialClub
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Country
    {
        /// <summary>
        /// Database id.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Country's name.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Code associated with this country.
        /// </summary>
        [DataMember]
        public String Code { get; set; }
    } // Country
}
