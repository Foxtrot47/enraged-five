﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model
{
    /// <summary>
    /// Custom class because the KeyValuePair<> class is immutable and can't be assigned directly to
    /// from the VerticaSession class.
    /// </summary>
    [DataContract]
    [Serializable]
    public class KeyValueStat<K, V>
    {
        #region Properties
        /// <summary>
        /// Key for this pair.
        /// </summary>
        [DataMember]
        public K Key { get; set; }

        /// <summary>
        /// Value for this pair.
        /// </summary>
        [DataMember]
        public V Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public KeyValueStat()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public KeyValueStat(K key, V value)
        {
            Key = key;
            Value = value;
        }
        #endregion // Constructor(s)
    } // KeyValueStat
}
