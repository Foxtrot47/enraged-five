﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Base.Math;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class RoomStatBundle : GameAssetStatBundleDtoBase
    {
        #region Properties
        /// <summary>
        /// Interior this room stat is a part of.
        /// </summary>
        [DataMember]
        public string InteriorIdentifier { get; set; }

        /// <summary>
        /// Room this stat is for.
        /// </summary>
        [DataMember]
        public string RoomIdentifier { get; set; }

        /// <summary>
        /// Bounding box.
        /// </summary>
        [DataMember]
        public BoundingBox3f BoundingBox { get; set; }

        /// <summary>
        /// Size of the exported geometry.
        /// </summary>
        [DataMember]
        public uint ExportGeometrySize { get; set; }

        /// <summary>
        /// List of txds to sizes.
        /// </summary>
        [DataMember]
        public List<TxdExportStatDto> TxdExportSizes { get; set; }

        /// <summary>
        /// Archetype's polygon count.
        /// </summary>
        [DataMember]
        public uint PolygonCount { get; set; }

        /// <summary>
        /// Collision polygon count.
        /// </summary>
        [DataMember]
        public uint CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this archetype stat.
        /// </summary>
        [DataMember]
        public List<CollisionPolyStatDto> CollisionTypePolygonCounts { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public RoomStatBundle()
            : base()
        {
            CollisionTypePolygonCounts = new List<CollisionPolyStatDto>();
            TxdExportSizes = new List<TxdExportStatDto>();
        }
        #endregion // Constructor(s)
    } // RoomStatDto
}
