﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Cutscenes")]
    public class CutsceneDtos : IDtos<CutsceneDto>
    {
        [XmlElement("Cutscene")]
        public List<CutsceneDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CutsceneDtos()
        {
            Items = new List<CutsceneDto>();
        }
    } // LevelDtos
}
