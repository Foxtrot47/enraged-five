﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.ProfileStats
{
    /// <summary>
    /// Dto object for profile stat definitions.
    /// </summary>
    [DataContract]
    public class ProfileStatDefinitionDto
    {
        /// <summary>
        /// Id that the social club services endpoint has for this stat.
        /// </summary>
        [DataMember]
        public int RosId { get; set; }

        /// <summary>
        /// Name of the stat.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Type of stat this is (int, label, bool, etc...).
        /// </summary>
        [DataMember]
        public String Type { get; set; }

        /// <summary>
        /// Default value for this stat.
        /// </summary>
        [DataMember]
        public String DefaultValue { get; set; }

        /// <summary>
        /// Comment associated with this stat.
        /// </summary>
        [DataMember]
        public String Comment { get; set; }
    } // ProfileStatDefinitionDto
}
