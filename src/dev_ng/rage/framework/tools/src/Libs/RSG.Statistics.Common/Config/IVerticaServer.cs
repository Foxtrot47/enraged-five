﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Config
{
    /// <summary>
    /// Public vertica server config interface.
    /// </summary>
    public interface IVerticaServer
    {
        #region Properties
        /// <summary>
        /// Config object parent.
        /// </summary>
        IStatisticsConfig Config { get; }

        /// <summary>
        /// Name associated with this server.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Address to use to access the server.
        /// </summary>
        String Address { get; }

        /// <summary>
        /// Port to host the HTTP WCF endpoints on.
        /// </summary>
        uint Port { get; }

        /// <summary>
        /// Service to use for retrieving the data.
        /// </summary>
        String Service { get; }

        /// <summary>
        /// Name of the database to connect to.
        /// </summary>
        String DatabaseName { get; }

        /// <summary>
        /// Location that the database resides in.
        /// </summary>
        String DatabaseLocation { get; }

        /// <summary>
        /// Username to use to connect to the database.
        /// </summary>
        String DatabaseUsername { get; }

        /// <summary>
        /// Password to use to connect to the database.
        /// </summary>
        String DatabasePassword { get; }

        /// <summary>
        /// How long SQL commands can run on the db before timing out.  Set to 0 for an infinite timeout.
        /// </summary>
        uint CommandTimeout { get; }

        /// <summary>
        /// Whether the server is running inside of IIS.
        /// </summary>
        bool UsingIIS { get; }

        /// <summary>
        /// Prefix for the db table schema.
        /// </summary>
        String SchemaPrefix { get; }
        #endregion // Properties
    } // IVerticaServer
}
