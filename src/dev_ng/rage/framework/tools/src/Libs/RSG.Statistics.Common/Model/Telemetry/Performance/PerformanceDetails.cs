﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class PerformanceDetails
    {
        #region Properties
        /// <summary>
        /// DB Submission Id (Used for linking up drawlist info).
        /// </summary>
        public long SubmissionId { get; set; }

        /// <summary>
        /// DB tuple Id (Used for linking up drawlist info).
        /// </summary>
        public long TupleId { get; set; }

        /// <summary>
        /// Where this event took place.
        /// </summary>
        [DataMember]
        public Vector3f Location { get; set; }

        /// <summary>
        /// Frame time.
        /// </summary>
        [DataMember]
        public float FrameTime { get; set; }

        /// <summary>
        /// Converts from/to fps.
        /// </summary>
        public float Fps
        {
            get { return (FrameTime != 0.0f ? 1000.0f / FrameTime : 0.0f); }
            set { FrameTime = (value != 0.0f ? 1000.0f / value : 0.0f); }
        }

        /// <summary>
        /// Update time.
        /// </summary>
        [DataMember]
        public float UpdateTime { get; set; }

        /// <summary>
        /// Render time.
        /// </summary>
        [DataMember]
        public float DrawTime { get; set; }

        /// <summary>
        /// Gpu time.
        /// </summary>
        [DataMember]
        public float GpuTime { get; set; }

        /// <summary>
        /// Index count.
        /// </summary>
        [DataMember]
        public uint IndexCount { get; set; }

        /// <summary>
        /// Graphics buffer index count.
        /// </summary>
        [DataMember]
        public uint GBufferIndexCount { get; set; }

        /// <summary>
        /// Number of objects that were in the scene.
        /// </summary>
        [DataMember]
        public uint ObjectCount { get; set; }

        /// <summary>
        /// Number of draw calls that were being made.
        /// </summary>
        [DataMember]
        public uint DrawcallCount { get; set; }

        /// <summary>
        /// Hour that this reading was taken at.
        /// </summary>
        [DataMember]
        public uint Hour { get; set; }

        /// <summary>
        /// Drawlist counts.
        /// </summary>
        [DataMember]
        public List<DrawListDetails> DrawListDetails { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerformanceDetails()
        {
            Location = new Vector3f();
            DrawListDetails = new List<DrawListDetails>();
        }
        #endregion // Constructor(s)
    } // PerformanceDetails

    /// <summary>
    /// A single draw list reading.
    /// </summary>
    [DataContract]
    [Serializable]
    public class DrawListDetails
    {
        /// <summary>
        /// DB Submission Id (Used for linking up to the parent perf stat).
        /// </summary>
        public long SubmissionId { get; set; }

        /// <summary>
        /// DB parent Id (Used for linking up to the parent perf stat).
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// Hash of the draw list.
        /// </summary>
        [DataMember]
        public uint Hash { get; set; }

        /// <summary>
        /// Name of the draw list.
        /// </summary>
        [DataMember]
        public String DrawListName { get; set; }

        /// <summary>
        /// Number of items in the draw list.
        /// </summary>
        [DataMember]
        public uint EntityCount { get; set; }
    } // DrawListDetails
}
