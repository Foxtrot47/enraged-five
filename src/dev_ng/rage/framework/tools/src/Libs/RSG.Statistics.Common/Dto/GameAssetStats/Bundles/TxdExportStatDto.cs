﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TxdExportStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Name of the texture dictionary this stat is for.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Size of the txd.
        /// </summary>
        [DataMember]
        public uint Size { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation
        /// </summary>
        public TxdExportStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="count"></param>
        public TxdExportStatDto(String name, uint size)
            : base()
        {
            Name = name;
            Size = size;
        }
        #endregion // Constructor(s)
    } // TxdExportStatDto
}
