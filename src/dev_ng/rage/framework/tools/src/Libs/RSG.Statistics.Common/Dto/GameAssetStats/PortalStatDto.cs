﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class OldPortalStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Name of the portal.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Interior this room stat is a part of.
        /// </summary>
        [DataMember]
        public string InteriorIdentifier { get; set; }

        /// <summary>
        /// Source room this stat is for.
        /// </summary>
        [DataMember]
        public string RoomAIdentifier { get; set; }

        /// <summary>
        /// Target room this stat is for.
        /// </summary>
        [DataMember]
        public string RoomBIdentifier { get; set; }
        #endregion // Properties
    } // PortalStatDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class PortalStatDto : DtoBase
    {
        /// <summary>
        /// Name of the portal.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Source room this stat is for.
        /// </summary>
        [DataMember]
        public uint RoomAIdentifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore()]
        public String RoomAIdentifierString
        {
            get
            {
                return RoomAIdentifier.ToString();
            }
            set
            {
                RoomAIdentifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// Target room this stat is for.
        /// </summary>
        [DataMember]
        public uint RoomBIdentifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore()]
        public String RoomBIdentifierString
        {
            get
            {
                return RoomBIdentifier.ToString();
            }
            set
            {
                RoomBIdentifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long InteriorStatId { get; set; }
    } // PortalStatDto
}
