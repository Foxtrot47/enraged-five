﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "NameHash")]
    public class NameHashDto : DtoBase, IEquatable<NameHashDto>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint Hash { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public NameHashDto(String name)
        {
            Name = name;
            Hash = RSG.ManagedRage.StringHashUtil.atStringHash(name, 0);
        }
        #endregion // Constructor(s)

        #region IEquatable<NameHashDto> Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(NameHashDto other)
        {
            if (other == null)
            {
                return false;
            }

            return (Hash == other.Hash);
        }
        #endregion // IEquatable<NameHashDto> Implementation
    } // NameHashDto
}
