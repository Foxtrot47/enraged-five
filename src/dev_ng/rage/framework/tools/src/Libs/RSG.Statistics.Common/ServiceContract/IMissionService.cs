﻿using System.ServiceModel;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IMissionService : IGameAssetServiceBase<MissionDto, MissionDtos>
    {
    } // IMissionService
}
