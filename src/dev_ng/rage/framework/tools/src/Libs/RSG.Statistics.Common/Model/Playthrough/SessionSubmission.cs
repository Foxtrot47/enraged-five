﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// Concrete implementation of a playthrough session.
    /// </summary>
    [DataContract]
    public class SessionSubmission
    {
        #region Properties
        /// <summary>
        /// Unique identifier for this playthrough session.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Identifier of the build this playthrough session was for.
        /// </summary>
        [DataMember]
        public String BuildIdentifier { get; set; }

        /// <summary>
        /// Platform that this playthrough session took part on.
        /// </summary>
        [DataMember]
        public RSG.Platform.Platform Platform { get; set; }

        /// <summary>
        /// Name of the user that took part in this playthrough session.
        /// </summary>
        [DataMember]
        public String UserName { get; set; }

        /// <summary>
        /// Optional friendly name for the playthrough session.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        /// When the playthrough session started.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// Amount of time the user spent playing (gets summed up on the server).
        /// </summary>
        [DataMember]
        public uint TimeSpentPlaying { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes
        /// </summary>
        public SessionSubmission()
        {
        }
        #endregion // Constructor(s)
    } // SessionSubmission
}
