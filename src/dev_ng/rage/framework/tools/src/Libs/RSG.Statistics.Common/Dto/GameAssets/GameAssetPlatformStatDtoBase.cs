﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class GameAssetPlatformStatDtoBase : EntityDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("PlatformIdentifier")]
        public string PlatformIdentifier
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("PhysicalSize")]
        public int? PhysicalSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("VirtualSize")]
        public int? VirtualSize
        {
            get;
            set;
        }
        #endregion // Properties
    } // GameAssetPlatformStatDtoBase
}
