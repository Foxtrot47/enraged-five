﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Security.Cryptography;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GameAsset
    {
        #region Properties
        /// <summary>
        /// Name of the asset as far as the game is concerned.
        /// </summary>
        [DataMember]
        public String GameName { get; set; }

        /// <summary>
        /// Hash of the name
        /// </summary>
        public uint Hash
        {
            get { return OneAtATime.ComputeHash(GameName); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public GameAsset()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public GameAsset(String gameName)
        {
            GameName = gameName;
        }
        #endregion // Constructor(s)
    } // GameAsset
}
