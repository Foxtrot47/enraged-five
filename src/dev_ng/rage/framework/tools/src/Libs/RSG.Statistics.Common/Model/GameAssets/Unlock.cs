﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class Unlock
    {
        /// <summary>
        /// Name for this unlock.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Rank at which this unlock is acquired.
        /// </summary>
        [DataMember]
        public uint Rank { get; set; }

        /// <summary>
        /// Type of unlock this is.
        /// </summary>
        [DataMember]
        public UnlockType UnlockType { get; set; }
    } // Unlock
}
