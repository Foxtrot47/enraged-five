﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// The Dto used to return results ( query ) for an associated query of memory pool telemetry stats
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MemoryPoolStat
    {
        [DataMember]
        public String MissionName { get; set; }
        [DataMember]
        public String PoolName { get; set; }
        [DataMember]
        public int NumSamples { get; set; }
        [DataMember]
        public uint MinPeak { get; set; }
        [DataMember]
        public double AvgPeak { get; set; }
        [DataMember]
        public uint MaxPeak { get; set; }
    } // MemoryPoolStat
}
