﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class WantedLevelStatDto : DtoBase
    {
        /// <summary>
        /// Wanted level.
        /// </summary>
        public uint WantedLevel
        {
            get;
            set;
        }

        /// <summary>
        /// Average percentage of total time spent at this wanted level.
        /// </summary>
        public float Percentage
        {
            get;
            set;
        }
    } // WantedLevelStatDto
}
