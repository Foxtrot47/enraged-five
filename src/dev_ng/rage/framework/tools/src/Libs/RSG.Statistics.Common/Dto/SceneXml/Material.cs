﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.SceneXml
{
    /// <summary>
    /// Data transfer object for transferring scene xml material data between server and client.
    /// </summary>
    [DataContract]
    public class Material
    {
        #region Properties
        /// <summary>
        /// Material's unique guid.
        /// </summary>
        [DataMember]
        public Guid Guid { get; set; }

        /// <summary>
        /// Type of material this is.
        /// </summary>
        [DataMember]
        public RSG.SceneXml.Material.MaterialTypes Type { get; set; }

        /// <summary>
        /// Name of the shader preset that this material uses (if any).
        /// </summary>
        [DataMember]
        public String Preset { get; set; }

        /// <summary>
        /// List of sub-materials that make up this material.
        /// </summary>
        [DataMember]
        public IList<Material> SubMaterials { get; set; }

        /// <summary>
        /// List of textures this material uses.
        /// </summary>
        [DataMember]
        public IList<Texture> Textures { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Material()
        {
            SubMaterials = new List<Material>();
            Textures = new List<Texture>();
        }
        #endregion // Constructor(s)
    } // Material
}
