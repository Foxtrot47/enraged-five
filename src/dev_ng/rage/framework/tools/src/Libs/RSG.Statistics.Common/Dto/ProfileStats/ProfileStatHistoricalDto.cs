﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Dto.ProfileStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class ProfileStatHistoricalDto
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Interval { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProfileStatHistoricalDto()
        {
        }
        #endregion // Constructor(s)
    } // ProfileStatHistoricalDto
}
