﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Dto.ExportStats;
using System.IO;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service interface for creating/retrieving map export stats.
    /// </summary>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IMapExportStatService
    {
        #region Data Submission
        /// <summary>
        /// Creates a new map network export stat from the specified database object.
        /// </summary>
        /// <param name="dto">
        /// The database object that should be used to 
        /// </param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "NetworkExport")]
        void CreateMapNetworkExportStat(MapNetworkExportStatDto dto);

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "All")]
        MapExportStatDto CreateMapExportStat(MapExportStatDto dto);

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}")]
        MapExportStatDto UpdateMapExportStat(String exportStatId, MapExportStatDto dto);

        /// <summary>
        /// Creates a new map check stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/MapCheckStat")]
        MapCheckStatDto CreateMapCheckStat(String exportStatId, MapCheckStatDto dto);

        /// <summary>
        /// Creates a new map export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/MapCheckStat/{mapCheckStatId}")]
        MapCheckStatDto UpdateMapCheckStat(String exportStatId, String mapCheckStatId, MapCheckStatDto dto);

        /// <summary>
        /// Creates a new map check stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/SectionExportStats")]
        SectionExportStatDto CreateSectionExportStat(String exportStatId, SectionExportStatDto dto);

        /// <summary>
        /// Creates a new section export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/SectionExportStats/{sectionExportStatId}")]
        SectionExportStatDto UpdateSectionExportStat(String exportStatId, String sectionExportStatId, SectionExportStatDto dto);

        /// <summary>
        /// Creates a new section export sub stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/SectionExportStat/{sectionExportStatId}/SubStats")]
        SectionExportSubStatDto CreateSectionExportSubStat(String exportStatId, String sectionExportStatId, SectionExportSubStatDto dto);

        /// <summary>
        /// Creates a new section export stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/SectionExportStat/{sectionExportStatId}/SubStats/{subStatId}")]
        SectionExportSubStatDto UpdateSectionExportSubStat(String exportStatId, String sectionExportStatId, String subStatId, SectionExportSubStatDto dto);

        /// <summary>
        /// Creates a new image build stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/ImageBuildStat")]
        ImageBuildStatDto CreateImageBuildStat(String exportStatId, ImageBuildStatDto dto);

        /// <summary>
        /// Updates an image build stat.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{exportStatId}/ImageBuildStat/{imageBuildStatId}")]
        ImageBuildStatDto UpdateImageBuildStat(String exportStatId, String imageBuildStatId, ImageBuildStatDto dto);
        #endregion // Data Submission

        #region Data Retrieval
        /// <summary>
        /// Retrieves a list of users that have ever exported data.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Users")]
        ExportUserDtos GetAllUsers();

        /// <summary>
        /// Scrapes the database generating a csv containing comparative export times for all sections/users.
        /// [OBSOLETE] - This should be replaced by a report.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Comparative")]
        ComparativeExportStatDtos GetComparitiveExportStats();

        /// <summary>
        /// Scrapes the database generating a csv containing comparative weekly export times for all sections.
        /// [OBSOLETE] - This should be replaced by a report.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Weekly")]
        WeeklyExportStatDtos GetWeeklyExportStats();
        #endregion // Data Retrieval
    } // IMapExportStatService
}
