﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class CashPurchaseData
    {
        #region Properties
        /// <summary>
        /// Percentage of purchases that aren't accounted for in the data (due to invalid purchase amount).
        /// </summary>
        [DataMember]
        public double TotalPercentage { get; set; }

        /// <summary>
        /// Number of unique gamers that bought cash.
        /// </summary>
        [DataMember]
        public long UniqueGamers { get; set; }

        /// <summary>
        /// Mapping of cash pack names to 
        /// </summary>
        [DataMember]
        public List<HistoricalCashPurchaseStats> CashPackData { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CashPurchaseData()
        {
            CashPackData = new List<HistoricalCashPurchaseStats>();
        }
        #endregion // Constructor(s)
    } // CashPurchaseData
}
