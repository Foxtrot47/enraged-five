﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace RSG.Statistics.Common.ServiceCallback
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMessageCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnMessageAdded(string message, params object[] args);
    } // IMessageCallback
}
