﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Texture")]
    public class TextureDto : GameAssetDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Filename")]
        public string Filename { get; set; }
        public bool FilenameSpecified { get { return !String.IsNullOrEmpty(Filename); } }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("AlphaFilename")]
        public string AlphaFilename { get; set; }
        public bool AlphaFilenameSpecified { get { return !String.IsNullOrEmpty(AlphaFilename); } }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("TextureType")]
        public string TextureType { get; set; }
        public bool TextureTypeSpecified { get { return !String.IsNullOrEmpty(TextureType); } }
        #endregion // Properties
    } // TextureDto
}
