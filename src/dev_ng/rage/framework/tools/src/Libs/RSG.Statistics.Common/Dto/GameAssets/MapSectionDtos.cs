﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("MapSections")]
    public class MapSectionDtos : IDtos<MapSectionDto>
    {
        [XmlElement("MapSection")]
        public List<MapSectionDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MapSectionDtos()
        {
            Items = new List<MapSectionDto>();
        }
    } // MapSectionDtos
}
