﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// Section summary information.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class SectionSummary
    {
        /// <summary>
        /// Name of the section.
        /// </summary>
        [DataMember]
        public String SectionName { get; set; }

        /// <summary>
        /// Total number of exports that were performed.
        /// </summary>
        [DataMember]
        public ulong TotalCount { get; set; }

        /// <summary>
        /// Number of successful exports.
        /// </summary>
        [DataMember]
        public double SuccessCount { get; set; }

        /// <summary>
        /// Convenience property for getting the success ratio.
        /// </summary>
        public double SuccessRatio
        {
            get
            {
                if (TotalCount != 0)
                {
                    return SuccessCount / TotalCount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Average time it took for the total export.
        /// </summary>
        [DataMember]
        public double AvgTotalTime { get; set; }

        /// <summary>
        /// Max time it took for the total export.
        /// </summary>
        [DataMember]
        public double MaxTotalTime { get; set; }

        /// <summary>
        /// Average time it took to export the section from max.
        /// </summary>
        [DataMember]
        public double AvgSectionExportTime { get; set; }

        /// <summary>
        /// Max time it took to export the section from max.
        /// </summary>
        [DataMember]
        public double MaxSectionExportTime { get; set; }

        /// <summary>
        /// Total number of section exports that were performed.
        /// </summary>
        [DataMember]
        public ulong SectionExportCount { get; set; }

        /// <summary>
        /// Number of successful section exports.
        /// </summary>
        [DataMember]
        public double SectionExportSuccessCount { get; set; }

        /// <summary>
        /// Convenience property for getting the success ratio.
        /// </summary>
        public double SectionExportSuccessRatio
        {
            get
            {
                if (SectionExportCount != 0)
                {
                    return SectionExportSuccessCount / SectionExportCount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Average time it took to build the section.
        /// </summary>
        [DataMember]
        public double? AvgBuildTime { get; set; }

        /// <summary>
        /// Max time it took to build the section.
        /// </summary>
        [DataMember]
        public double? MaxBuildTime { get; set; }

        /// <summary>
        /// Number of times the section was built.
        /// </summary>
        [DataMember]
        public ulong BuildCount { get; set; }

        /// <summary>
        /// Number of successful builds.
        /// </summary>
        [DataMember]
        public double BuildSuccessCount { get; set; }

        /// <summary>
        /// Convenience property for getting the success ratio.
        /// </summary>
        public double BuildSuccessRatio
        {
            get
            {
                if (BuildCount != 0)
                {
                    return BuildSuccessCount / BuildCount;
                }
                else
                {
                    return 0;
                }
            }
        }
    } // SectionSummary
}
