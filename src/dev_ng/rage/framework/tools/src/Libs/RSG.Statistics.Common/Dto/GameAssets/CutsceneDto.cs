﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Cutscene")]
    public class CutsceneDto : GameAssetDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        public float Duration { get; set; }
    } // CutsceneDto
}
