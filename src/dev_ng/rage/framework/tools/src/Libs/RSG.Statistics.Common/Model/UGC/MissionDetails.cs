﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.UGC
{
    /// <summary>
    /// Detailed UGC mission information.
    /// </summary>
    [DataContract]
    [ReportType]
    [Serializable]
    public class MissionDetails
    {
        #region Properties
        /// <summary>
        /// Public facing content id.
        /// </summary>
        [DataMember]
        public String PublicContentId { get; set; }

        /// <summary>
        /// Type of match this piece of content is for.
        /// </summary>
        [DataMember]
        public MatchType Type { get; set; }

        /// <summary>
        /// Match sub-type.
        /// </summary>
        [DataMember]
        public int SubType { get; set; }

        /// <summary>
        /// Friendly name given to this piece of content.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Description associated with this piece of content.
        /// </summary>
        [DataMember]
        public String Description { get; set; }

        /// <summary>
        /// Rank at which this piece of content becomes available.
        /// </summary>
        [DataMember]
        public uint Rank { get; set; }

        /// <summary>
        /// Minimum number of required players.
        /// </summary>
        [DataMember]
        public int MinPlayers { get; set; }

        /// <summary>
        /// Maximum number of required players.
        /// </summary>
        [DataMember]
        public int MaxPlayers { get; set; }

        /// <summary>
        /// Minimum number of required teams.
        /// </summary>
        [DataMember]
        public int MinTeams { get; set; }

        /// <summary>
        /// Maximum number of required teams.
        /// </summary>
        [DataMember]
        public int MaxTeams { get; set; }

        /// <summary>
        /// Contact this mission is associated with.
        /// </summary>
        [DataMember]
        public String Contact { get; set; }

        /// <summary>
        /// Surrogate property for contact.
        /// </summary>
        public int ContactId
        {
            set { Contact = ConvertCharacterContact(value); }
        }

        /// <summary>
        /// Amount of XP received for doing this mission.
        /// </summary>
        [DataMember]
        public uint XP { get; set; }
        
        /// <summary>
        /// Amount of cash received for doing this mission.
        /// </summary>
        [DataMember]
        public uint Cash { get; set; }

        /// <summary>
        /// Name of the user that created this piece of content.
        /// </summary>
        [DataMember]
        public String CreatorName { get; set; }

        /// <summary>
        /// Platform that the creator is from.
        /// </summary>
        [DataMember]
        public ROSPlatform? CreatorPlatform { get; set; }

        /// <summary>
        /// When this piece of content was created.
        /// </summary>
        [DataMember]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Date at which this piece of content was published.
        /// </summary>
        [DataMember]
        public DateTime? PublishedDate { get; set; }

        /// <summary>
        /// Date when this piece of content was last updated.
        /// </summary>
        [DataMember]
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Date when this piece of content was deleted.
        /// </summary>
        [DataMember]
        public DateTime? DeletedDate { get; set; }

        /// <summary>
        /// Category this piece of content falls under.
        /// </summary>
        [DataMember]
        public FreemodeMissionCategory Category { get; set; }

        /// <summary>
        /// When this job is available to play.
        /// </summary>
        [DataMember]
        public TimeOfDay Availability { get; set; }

        /// <summary>
        /// Time of day that this mission will take place.
        /// </summary>
        [DataMember]
        public TimeOfDay TimeOfDay { get; set; }

        /// <summary>
        /// Surrogate property for setting the time of day based off a bitset.
        /// </summary>
        public uint TimeOfDaySurrogate
        {
            set
            {
                TimeOfDay = TimeOfDayUtils.ConvertBitsetToTimeOfDay(value);
            }
        }

        /// <summary>
        /// Starting coordinates for the match.
        /// </summary>
        [DataMember]
        public Vector3f StartCoordinates { get; set; }

        /// <summary>
        /// Surrogate properties for creation from vertica.
        /// </summary>
        public float StartX
        {
            set { StartCoordinates.X = value; }
        }

        public float StartY
        {
            set { StartCoordinates.Y = value; }
        }

        public float StartZ
        {
            set { StartCoordinates.Z = value; }
        }

        /// <summary>
        /// Name of the default radio station for this mission.
        /// </summary>
        [DataMember]
        public String RadioStation { get; set; }

        /// <summary>
        /// Unique hash of the radio station.
        /// </summary>
        public int RadioStationHash { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionDetails()
        {
            StartCoordinates = new Vector3f();
        }
        #endregion // Constructor(s)

        #region Private Method
        /// <summary>
        /// Converts a character contact hash into a friendly string.
        /// </summary>
        /// <param name="contactHash"></param>
        /// <returns></returns>
        private String ConvertCharacterContact(int contactHash)
        {
            switch (contactHash)
            {
                case 0:
                    return null;
                case 131908481:
                    return "Gerald";
                case -366822323:
                    return "Lamar";
                case -1917614010:
                    return "Lester";
                case -328739832:
                    return "Martin";
                case -1984782235:
                    return "Ron";
                case -2105450473:
                    return "Simeon";
                case 657970604:
                    return "Trevor";
                default:
                    return String.Format("Unknown Contact (Hash: {0})", contactHash);
            }
        }
        #endregion // Private Method
    } // MissionDetails
}
