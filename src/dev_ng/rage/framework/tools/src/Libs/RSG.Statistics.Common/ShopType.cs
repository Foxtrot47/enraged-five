﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enumeration of shop types.
    /// </summary>
    public enum ShopType
    {
        [FriendlyName("Hair Dresser")]
        HairDresser,

        [FriendlyName("Clothing Store")]
        ClothingStore,

        [FriendlyName("Mask Shop")]
        MaskShop,

        [FriendlyName("Ammu-Nation")]
        WeaponStore,

        [FriendlyName("Tattoo Parlour")]
        TattooParlour,

        [FriendlyName("Car Mods")]
        Garage
    } // ShopTypes
}
