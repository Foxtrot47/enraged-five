﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information for a single freemode match type.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FreemodeMatchTypeStat
    {
        #region Properties
        /// <summary>
        /// Type of freemode match that was undertaken. e.g. Deathmatch, Race, etc...
        /// </summary>
        [DataMember]
        public MatchType MatchType { get; set; }

        /// <summary>
        /// Amount of time spent playing this variant (in seconds).
        /// </summary>
        [DataMember]
        public ulong TimeSpentPlaying { get; set; }

        /// <summary>
        /// Number of times that players have played this variant.
        /// </summary>
        [DataMember]
        public ulong TimesPlayed { get; set; }

        /// <summary>
        /// Number of unique gamers that played this variant.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Total number of gamers that took part in this variant.
        /// </summary>
        [DataMember]
        public ulong TotalGamers { get; set; }

        /// <summary>
        /// Total amount of XP players made for this match type.
        /// </summary>
        [DataMember]
        public decimal TotalXP { get; set; }

        /// <summary>
        /// The various matches that were performed.
        /// </summary>
        [DataMember]
        public List<FreemodeMatchStatBase> Variants { get; set; }

        /// <summary>
        /// Additional sub-groupings of the data.
        /// </summary>
        [DataMember]
        public List<FreemodeMatchTypeBreakdown> Breakdowns { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public FreemodeMatchTypeStat()
        {
            Variants = new List<FreemodeMatchStatBase>();
            Breakdowns = new List<FreemodeMatchTypeBreakdown>();
        }
        #endregion // Constructor(s)
    } // FreemodeMatchSummary
}
