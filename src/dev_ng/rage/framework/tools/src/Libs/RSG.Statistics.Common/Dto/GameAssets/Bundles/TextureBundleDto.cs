﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TextureBundleDto : DtoBase, IEquatable<TextureBundleDto>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public long Hash { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string Filename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string AlphaFilename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string TextureType { get; set; }
        #endregion // Properties

        #region IEquatable<TextureBundleDto> Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(TextureBundleDto other)
        {
            if (other == null)
            {
                return false;
            }

            return (Hash == other.Hash);
        }
        #endregion // IEquatable<TextureBundleDto> Implementation

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is TextureBundleDto) && Equals(obj as TextureBundleDto));
        }

        /// <summary>
        /// Return String representation of the Vehicle.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of Vehicle (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        #endregion // Object Overrides
    } // TextureBundleDto
}
