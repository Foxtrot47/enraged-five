﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enum for vehicle classes.
    /// The values should match those defined in //depot/gta5/script/dev/multiplayer/globals/MP_globals_FM.sch.
    /// </summary>
    public enum VehicleClass
    {
        [FriendlyName("Unknown")]
        [Browsable(false)]
        Unknown = -1,

        [FriendlyName("Compacts")]
        Compacts = 0,                       //FMMC_VEHICLE_CLASS_COMPACTS

        [FriendlyName("Sedans")]
        Sedans,

        [FriendlyName("SUVs")]
        SUV,

        [FriendlyName("Coupé")]
        Coupes,

        [FriendlyName("Muscle")]
        Muscle,

        [FriendlyName("Classics")]
        Classics,

        [FriendlyName("Sports")]
        Sports,

        [FriendlyName("Super")]
        Super,

        [FriendlyName("Bikes")]
        Bikes,

        [FriendlyName("Off Road")]
        OffRoad,

        [FriendlyName("Industrial")]
        Industrial,

        [FriendlyName("Utility")]
        Utility,

        [FriendlyName("Vans")]
        Vans,

        [FriendlyName("Cycles")]
        Cycles
    } // VehicleClass


    /// <summary>
    /// Util/extension methods for vehicle types
    /// </summary>
    public static class VehicleClassUtils
    {
        /// <summary>
        /// Converts a match type integer to an enum value.
        /// </summary>
        public static VehicleClass ConvertToVehicleClass(int value)
        {
            VehicleClass type;
            if (Enum.IsDefined(typeof(VehicleClass), value))
            {
                type = (VehicleClass)value;
            }
            else
            {
                type = VehicleClass.Unknown;
            }
            return type;
        }
    } // VehicleClassUtils
}
