﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Dto;
using RSG.Model.Statistics.Captures;
using RSG.Model.Statistics.Captures.Historical;
using RSG.Model.Statistics.Platform;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service interface for dealing with resource stats
    /// </summary>
    [ServiceContract]
    public interface IResourceStatService
    {
        #region Data Submission
        /// <summary>
        /// Endpoint for adding resource stats.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "All")]
        void CreateResourceStats(List<ResourceStat> resourceStats);
        #endregion // Data Submission
    } // ICaptureService
}
