﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalExportStat
    {
        /// <summary>
        /// Total number of exports that were performed.
        /// </summary>
        [DataMember]
        public uint TotalCount { get; set; }

        /// <summary>
        /// Number of successful exports.
        /// </summary>
        [DataMember]
        public uint SuccessfulCount { get; set; }

        /// <summary>
        /// Average amount of time it took.
        /// </summary>
        [DataMember]
        public double AvgTotalTime { get; set; }

        /// <summary>
        /// Maximum amount of time it took.
        /// </summary>
        [DataMember]
        public ulong MaxTotalTime { get; set; }

        /// <summary>
        /// Average time spent for exporting a section.
        /// </summary>
        [DataMember]
        public double AvgSectionExportTime { get; set; }

        /// <summary>
        /// Maximum time spent for exporting a section.
        /// </summary>
        [DataMember]
        public ulong MaxSectionExportTime { get; set; }

        /// <summary>
        /// Average time spent building.
        /// </summary>
        [DataMember]
        public double AvgBuildTime { get; set; }

        /// <summary>
        /// Maximum time spent building.
        /// </summary>
        [DataMember]
        public ulong MaxBuildTime { get; set; }
    } // HistoricalExportStats
}
