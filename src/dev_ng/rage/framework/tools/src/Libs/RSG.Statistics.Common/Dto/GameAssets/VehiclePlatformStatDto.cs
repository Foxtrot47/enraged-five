﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("VehiclePlatformStat")]
    public class VehiclePlatformStatDto : GameAssetPlatformStatDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("VehicleStatId")]
        public long? VehicleStatId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("HiPhysicalSize")]
        public int? HiPhysicalSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("HiVirtualSize")]
        public int? HiVirtualSize
        {
            get;
            set;
        }
        #endregion // Properties
    } // VehiclePlatformStatDto
}
