﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using RSG.Base.Math;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(DrawableArchetypeStatDto))]
    [KnownType(typeof(FragmentArchetypeStatDto))]
    [KnownType(typeof(InteriorArchetypeStatDto))]
    [KnownType(typeof(StatedAnimArchetypeStatDto))]
    public abstract class ArchetypeStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Archetype's identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// Archetype's lod distance.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableArchetypeStat.LodDistanceString)]
        public float? LodDistance { get; set; }
        #endregion // Properties
    } // ArchetypeStatDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public abstract class MapArchetypeStatDto : ArchetypeStatDto
    {
        #region Properties
        /// <summary>
        /// Bounding box.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.BoundingBoxString)]
        public BoundingBox3f BoundingBox { get; set; }

        /// <summary>
        /// Shader information.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.ShadersString)]
        public List<ShaderStatDto> Shaders { get; set; }

        /// <summary>
        /// Size of the exported geometry.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.ExportGeometrySizeString)]
        public uint? ExportGeometrySize { get; set; }

        /// <summary>
        /// List of txds to sizes.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.TxdExportSizesString)]
        public List<TxdExportStatDto> TxdExportSizes { get; set; }

        /// <summary>
        /// Archetype's polygon count.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.PolygonCountString)]
        public uint? PolygonCount { get; set; }

        /// <summary>
        /// Collision polygon count.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.CollisionPolygonCountString)]
        public uint? CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this archetype stat.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.CollisionTypePolygonCountsString)]
        public List<CollisionPolyStatDto> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// Whether the archetype has a light attached to it.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.HasAttachedLightString)]
        public bool? HasAttachedLight { get; set; }

        /// <summary>
        /// Whether the archetype has an explosive effect attached to it.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableMapArchetypeStat.HasExplosiveEffectString)]
        public bool? HasExplosiveEffect { get; set; }
        #endregion // Properties
    } // MapArchetypeStatDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public abstract class SimpleMapArchetypeStatDto : MapArchetypeStatDto
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.IsDynamicString)]
        public bool? IsDynamic { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.IsFixedString)]
        public bool? IsFixed { get; set; }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_COLLISION_GROUP attribute.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.CollisionGroupString)]
        public string CollisionGroup { get; set; }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_FORCE_BAKE_COLLISION attribute.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.ForceBakeCollisionString)]
        public bool? ForceBakeCollision { get; set; }

        /// <summary>
        /// Obtained from the AttrNames.OBJ_NEVER_BAKE_COLLISION attribute.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.NeverBakeCollisionString)]
        public bool? NeverBakeCollision { get; set; }

        /// <summary>
        /// List of spawn points associated with this archetype.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.SpawnPointsString)]
        public List<SpawnPointStatDto> SpawnPoints { get; set; }

        ///<summary>
        /// Drawable LODs this archetype has
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.DrawableLODsString)]
        public List<DrawableLodStatDto> DrawableLods { get; set; }
    } // SimpleMapArchetypeStatDto
}
