﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("RadioStations")]
    public class RadioStationDtos : IDtos<RadioStationDto>
    {
        [XmlElement("RadioStation")]
        public List<RadioStationDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RadioStationDtos()
        {
            Items = new List<RadioStationDto>();
        }
    } // RadioStationDtos
}
