﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Missions")]
    public class MissionDtos : IDtos<MissionDto>
    {
        [XmlElement("Mission")]
        public List<MissionDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MissionDtos()
        {
            Items = new List<MissionDto>();
        }
    } // MissionDtos
}
