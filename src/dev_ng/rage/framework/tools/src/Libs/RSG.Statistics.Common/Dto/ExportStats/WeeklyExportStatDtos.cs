﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("WeeklyExportStats")]
    public class WeeklyExportStatDtos : IDtos<WeeklyExportStatDto>
    {
        [XmlElement("WeeklyExportStat")]
        public List<WeeklyExportStatDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WeeklyExportStatDtos()
        {
            Items = new List<WeeklyExportStatDto>();
        }
    } // WeeklyExportStatDtos
}
