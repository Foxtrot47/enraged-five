﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MemoryStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint PhysicalSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint VirtualSize { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MemoryStatDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="physicalSize"></param>
        /// <param name="virtualSize"></param>
        public MemoryStatDto(uint physicalSize, uint virtualSize)
        {
            PhysicalSize = physicalSize;
            VirtualSize = virtualSize;
        }
        #endregion // Properties
    } // MemoryStatDto
}
