﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// Performance details for a cutscene.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class CutscenePerformanceDetails : PerformanceDetails
    {
        #region Properties
        /// <summary>
        /// Frame the cutscene was on.
        /// </summary>
        [DataMember]
        public uint CutsceneFrame { get; set; }

        /// <summary>
        /// Total number of frames the cutscene has.
        /// </summary>
        [DataMember]
        public uint CutsceneTotalFrames { get; set; }

        /// <summary>
        /// The concat section that the cutscene was in.
        /// </summary>
        [DataMember]
        public uint CutsceneCurrentConcatSection { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutscenePerformanceDetails()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // CutscenePerformanceDetails
}
