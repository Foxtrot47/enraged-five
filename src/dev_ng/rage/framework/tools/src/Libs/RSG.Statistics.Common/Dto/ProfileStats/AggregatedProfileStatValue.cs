﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Dto.ProfileStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class AggregatedProfileStatValue : ProfileStatValue
    {
        #region Properties
        /// <summary>
        /// Number of gamers that contributed to the stats.
        /// </summary>
        [DataMember]
        public long NumberOfGamers { get; set; }
        #endregion // Properties
    } // AggregatedProfileStatValue
}
