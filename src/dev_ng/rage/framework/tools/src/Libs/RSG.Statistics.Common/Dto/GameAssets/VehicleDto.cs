﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// Vehicle Dto object
    /// </summary>
    [Serializable]
    [XmlRoot("Vehicle")]
    [DataContract]
    public class VehicleDto : GameAssetDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Category { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String GameName { get; set; }
    } // VehicleDto
}
