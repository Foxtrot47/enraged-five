﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Platforms")]
    public class PlatformDtos : IDtos<PlatformDto>
    {
        [XmlElement("Platform")]
        public List<PlatformDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PlatformDtos()
        {
            Items = new List<PlatformDto>();
        }
    } // PlatformDtos
}
