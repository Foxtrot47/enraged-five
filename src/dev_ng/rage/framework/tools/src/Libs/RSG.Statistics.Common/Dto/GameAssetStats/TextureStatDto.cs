﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class TextureStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Textures name.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Textures identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Filename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String AlphaFilename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String TextureType { get; set; }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long ShaderStatId { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private TextureStatDto()
        {
        }
        #endregion // Constructor(s)
    } // TextureStatDto
}
