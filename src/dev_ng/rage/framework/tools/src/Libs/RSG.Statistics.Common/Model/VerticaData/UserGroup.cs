﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.VerticaData
{
    /// <summary>
    /// Group of social club users.
    /// </summary>
    [DataContract]
    public class UserGroup
    {
        #region Properties
        /// <summary>
        /// Name for this group.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Users that are in this group.
        /// </summary>
        [DataMember]
        public List<String> Users { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (required for serialisation).
        /// </summary>
        public UserGroup()
        {
            Users = new List<String>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public UserGroup(String name)
            : this()
        {
            Name = name;
        }
        #endregion // Constructor(s)
    } // UserGroup
}
