﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BasicRoomStatDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long InteriorStatId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal BasicRoomStatDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public BasicRoomStatDto(String name)
        {
            Name = name;
        }
    } // BasicRoomStatDto
}
