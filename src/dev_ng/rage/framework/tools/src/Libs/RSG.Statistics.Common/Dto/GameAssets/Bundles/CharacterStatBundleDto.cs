﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("CharacterStatBundle")]
    public class CharacterStatBundleDto : GameAssetStatBundleDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("PolyCount")]
        public int PolyCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("CollisionCount")]
        public int CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("BoneCount")]
        public int BoneCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("ClothPolyCount")]
        public int ClothPolyCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("ClothCount")]
        public int ClothCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlArray("CharacterPlatformStats")]
        [XmlArrayItem("CharacterPlatformStat")]
        public List<CharacterPlatformStatBundleDto> CharacterPlatformStats { get; set; }
    } // CharacterStatBundleDto
}
