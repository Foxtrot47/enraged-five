﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// Dto object containing information for the weekly comparative export stat report.
    /// </summary>
    public class WeeklyExportStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// The date corresponding to the start of the week for this set of stats.
        /// </summary>
        public DateTime StartOfWeek { get; set; }

        /// <summary>
        /// List of weekly section stats for this particular date.
        /// </summary>
        public List<WeeklySectionExportStatDto> SectionStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Needed for serialisation.
        /// </summary>
        public WeeklyExportStatDto()
        {
            SectionStats = new List<WeeklySectionExportStatDto>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startOfWeek"></param>
        public WeeklyExportStatDto(DateTime startOfWeek)
            : this()
        {
            StartOfWeek = startOfWeek;
        }
        #endregion // Constructor(s)
    } // WeeklyExportStatDto
}
