﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enumeration of possible outcomes for a multiplayer job.
    /// </summary>
    public enum JobResult
    {
        /// <summary>
        /// Player successfully passed the job.
        /// </summary>
        [FriendlyName("Passed")]
        Passed = 0,

        /// <summary>
        /// The job finished, but neither team reached the required target.
        /// </summary>
        [FriendlyName("Over")]
        Over,

        /// <summary>
        /// Player failed to pass the job.
        /// </summary>
        [FriendlyName("Failed")]
        Failed,

        /// <summary>
        /// Player cancelled the job while it was in progress.
        /// </summary>
        [FriendlyName("Cancelled")]
        Cancelled,

        /// <summary>
        /// Result was not set.
        /// </summary>
        [FriendlyName("Not Set")]
        NotSet,

        /// <summary>
        /// Not sure about this one...
        /// </summary>
        [FriendlyName("SCTV Leaving")]
        SctvLeaving,

        /// <summary>
        /// Result of the job is unknown.
        /// </summary>
        [FriendlyName("Unknown")]
        Unknown
    } // JobResult
}
