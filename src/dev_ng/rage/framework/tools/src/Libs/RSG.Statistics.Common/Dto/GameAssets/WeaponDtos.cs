﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("Weapons")]
    public class WeaponDtos : IDtos<WeaponDto>
    {
        [DataMember]
        [XmlElement("Weapon")]
        public List<WeaponDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WeaponDtos()
        {
            Items = new List<WeaponDto>();
        }
    } // WeaponDtos
}
