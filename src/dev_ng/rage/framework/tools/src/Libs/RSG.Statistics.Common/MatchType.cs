﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enum containing the freemode match types.
    /// The values should match those defined in //depot/gta5/script/dev/multiplayer/globals/MP_globals_FM.sch.
    /// </summary>
    public enum MatchType
    {
        [FriendlyName("Mission")]
        Mission = 0,

        [FriendlyName("Deathmatch")]
        Deathmatch = 1,

        [FriendlyName("Race")]
        Race = 2,

        [FriendlyName("Survival")]
        Survival = 3,

        [FriendlyName("Capture")]
        CaptureTheFlag = 4,

        [FriendlyName("One on One DM")]
        ImpromptuDeathmatch = 5,            // Impromptu Deathmatch (not sure if this is in use)

        [FriendlyName("Gang Attack")]
        GangAttack = 6,

        [FriendlyName("Last Team Standing")]
        LastTeamStanding = 7,

        [FriendlyName("Parachuting")]
        BaseJump = 8,

        [FriendlyName("Golf")]
        Golf = 11,

        [FriendlyName("Tennis")]
        Tennis = 12,

        [FriendlyName("Shooting Range")]
        ShootingRange = 13,

        [FriendlyName("Darts")]
        Darts = 14,

        [FriendlyName("Arm Wrestling")]
        ArmWrestling = 15,

        [FriendlyName("Unknown")]
        [Browsable(false)]
        Unknown                 // Match Type that doesn't fall into the above categories.
    } // MatchType


    /// <summary>
    /// Util/extension methods for match types.
    /// </summary>
    public static class MatchTypeUtils
    {
        /// <summary>
        /// Returns whether the match type is considered to be a mini-game.
        /// </summary>
        public static bool IsMiniGame(this MatchType type)
        {
            return GetMiniGameMatchTypes().Contains(type);
        }

        /// <summary>
        /// 
        /// </summary>
        public static IEnumerable<MatchType> GetMiniGameMatchTypes()
        {
            yield return MatchType.Golf;
            yield return MatchType.Tennis;
            yield return MatchType.Darts;
            yield return MatchType.ArmWrestling;
            yield return MatchType.ShootingRange;
        }

        /// <summary>
        /// Converts a match type integer to an enum value.
        /// </summary>
        public static MatchType ConvertToMatchType(int value)
        {
            MatchType type;
            if (Enum.IsDefined(typeof(MatchType), value))
            {
                type = (MatchType)value;
            }
            else
            {
                type = MatchType.Unknown;
            }
            return type;
        }
    } // MatchTypeUtils
}
