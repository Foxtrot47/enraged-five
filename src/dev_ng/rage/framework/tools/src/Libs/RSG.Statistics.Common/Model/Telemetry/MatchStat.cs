﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Mission;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information about a single match that was performed.
    /// </summary>
    [DataContract]
    [Serializable]
    public class MatchStat
    {
        /// <summary>
        /// Unique match identifier.
        /// </summary>
        public ulong MatchId { get; set; }

        /// <summary>
        /// UGC Identifier for the match.
        /// </summary>
        [DataMember]
        public String UGCIdentifier { get; set; }

        /// <summary>
        /// Name of the match.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Person who created the match.
        /// </summary>
        [DataMember]
        public String Creator { get; set; }

        /// <summary>
        /// Platform of the creator.
        /// </summary>
        [DataMember]
        public ROSPlatform? CreatorPlatform { get; set; }

        /// <summary>
        /// Type of match this is.
        /// </summary>
        [DataMember]
        public String Type { get; set; }

        /// <summary>
        /// Timestamp for when this match took place.
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Amount of experience earned.
        /// </summary>
        [DataMember]
        public uint XPEarned { get; set; }

        /// <summary>
        /// Amount of cash earned during this match.
        /// </summary>
        [DataMember]
        public int CashEarned { get; set; }

        /// <summary>
        /// Amount of money that the player bet on the match.
        /// </summary>
        [DataMember]
        public uint AmountBet { get; set; }

        /// <summary>
        /// Amount of money the player won/lost from betting.
        /// </summary>
        [DataMember]
        public int BetWinnings { get; set; }

        /// <summary>
        /// Job result.
        /// </summary>
        [DataMember]
        public JobResult Result { get; set; }

        /// <summary>
        /// Amount of money rewarded for completing the job.
        /// </summary>
        [DataMember]
        public uint? JobRewardCash { get; set; }

        /// <summary>
        /// Maximum wave that the player reached for survival jobs.
        /// </summary>
        [DataMember]
        public uint MaxSurvivalWave { get; set; }

        /// <summary>
        /// Rank that the player placed in this job.
        /// </summary>
        [DataMember]
        public uint Rank { get; set; }

        /// <summary>
        /// Number of players that took part in the match.
        /// </summary>
        [DataMember]
        public uint Players { get; set; }

        /// <summary>
        /// Time player spent taking part in this match (in seconds).
        /// </summary>
        [DataMember]
        public uint Duration { get; set; }

        /// <summary>
        /// Checkpoint information for this match.
        /// Only applies to race and parachute match types.
        /// </summary>
        [DataMember]
        public uint CheckpointCount { get; set; }
    } // MatchStat
}
