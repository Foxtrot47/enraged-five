﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("Characters")]
    public class CharacterDtos : IDtos<CharacterDto>
    {
        [DataMember]
        [XmlElement("Character")]
        public List<CharacterDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CharacterDtos()
        {
            Items = new List<CharacterDto>();
        }
    } // CharacterDtos
}
