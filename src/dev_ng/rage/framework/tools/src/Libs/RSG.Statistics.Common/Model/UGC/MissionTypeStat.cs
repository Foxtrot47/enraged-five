﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.Base.Math;

namespace RSG.Statistics.Common.Model.UGC
{
    /// <summary>
    /// The data returned about each breakdown
    /// - was just a count of times player but now contains a list of coords where it was played.
    /// </summary>
    [DataContract]
    [Serializable]
    public class MissionTypeStatBreakDownData
    {
        #region Properties
        /// <summary>
        /// Name of the breakdown
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Number of times this piece of content appears.
        /// </summary>
        [DataMember]
        public ulong Count { get; set; }

        /// <summary>
        /// A list of all starting coordinates for the stat breakdown.
        /// </summary>
        [DataMember]
        public IList<MissionInstance> Instances { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private MissionTypeStatBreakDownData()
        {
        }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        public MissionTypeStatBreakDownData(String name, ulong count, IList<MissionInstance> instances)
        {
            Name = name;
            Count = count;
            Instances = instances;
        }     
        #endregion // Constructor(s)
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class MissionInstance
    {
        /// <summary>
        /// The start coordinates for this mission instance.
        /// </summary>
        [DataMember]
        public Vector3f StartCoordinate { get; set; }

        /// <summary>
        /// Rank that this piece of content unlocks at.
        /// </summary>
        [DataMember]
        public uint Rank { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MissionInstance()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startCoords"></param>
        /// <param name="rank"></param>
        public MissionInstance(Vector3f startCoords, uint rank)
        {
            StartCoordinate = startCoords;
            Rank = rank;
        }
    } // MissionInstance

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class MissionTypeStat
    {
        #region Properties
        /// <summary>
        /// Match type this is for.
        /// </summary>
        [DataMember]
        public MatchType Type { get; set; }

        /// <summary>
        /// Number of content pieces that exist for this match type.
        /// </summary>
        [DataMember]
        public ulong PiecesOfContent { get; set; }

        /// <summary>
        /// Additional breakdowns of the data.
        /// </summary>
        [DataMember]
        public IDictionary<String, IList<MissionTypeStatBreakDownData>> Breakdowns { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionTypeStat()
        {
            Breakdowns = new Dictionary<String, IList<MissionTypeStatBreakDownData>>();
        }
        #endregion // Constructor(s)
    } // MissionTypeStat
}
