﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Weapon;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Weapon
    {
        /// <summary>
        /// Unique identifier for this weapon.
        /// </summary>
        [DataMember]
        public String Identifier { get; set; }

        /// <summary>
        /// Name that runtime use to identify this vehicle.
        /// </summary>
        [DataMember]
        public String RuntimeName { get; set; }

        /// <summary>
        /// Friendly name of the vehicle as shown in game.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        /// Name of the model used for this vehicle in game.
        /// </summary>
        [DataMember]
        public String ModelName { get; set; }

        /// <summary>
        /// Name that this weapon will appear as in profile stats.
        /// </summary>
        [DataMember]
        public String ProfileStatName { get; set; }

        /// <summary>
        /// Category that this weapon falls under.
        /// </summary>
        [DataMember]
        public WeaponCategory Category { get; set; }
    } // Weapon
}
