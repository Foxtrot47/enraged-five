﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// List of car gen stats.
    /// </summary>
    [DataContract]
    [Serializable]
    public class CarGenStatDtos : IDtos<CarGenStatDto>
    {
        [DataMember]
        public List<CarGenStatDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CarGenStatDtos()
        {
            Items = new List<CarGenStatDto>();
        }
    } // CarGenStatDtos
}
