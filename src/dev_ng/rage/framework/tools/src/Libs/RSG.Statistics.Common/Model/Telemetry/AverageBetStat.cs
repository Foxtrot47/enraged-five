﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class AverageBetStat
    {
        #region Properties
        /// <summary>
        /// String name for the bucket.
        /// </summary>
        [DataMember]
        public String Bucket { get; set; }

        /// <summary>
        /// Min bucket value.
        /// </summary>
        [DataMember]
        public double BucketMin { get; set; }

        /// <summary>
        /// Max bucket value.
        /// </summary>
        [DataMember]
        public double BucketMax { get; set; }

        /// <summary>
        /// Number of gamers that fall under this bucket
        /// </summary>
        [DataMember]
        public ulong GamerCount { get; set; }

        /// <summary>
        /// Total amount that was bet in this bucket.
        /// </summary>
        [DataMember]
        public decimal TotalAmountBet { get; set; }

        /// <summary>
        /// Number of bets that were made that fall under this category.
        /// </summary>
        [DataMember]
        public decimal TotalBetsMade { get; set; }

        /// <summary>
        /// Number of bets that were made that won.
        /// </summary>
        [DataMember]
        public decimal WinningBetsMade { get; set; }

        /// <summary>
        /// Amount that was bet on the winning bets.
        /// </summary>
        [DataMember]
        public decimal WinningAmountBet { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public AverageBetStat(double bucketStart, double bucketSize)
        {
            Bucket = String.Format("{0}-{1}", bucketStart, bucketStart + bucketSize);
            BucketMin = bucketStart;
            BucketMax = bucketStart + bucketSize;
        }
        #endregion // Constructor(s)
    } // AverageBetStat
}
