﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("SectionExportStat")]
    public class SectionExportStatDto : MapExportSubStatDto
    {
        /// <summary>
        /// Identifier of the map section that this stat is for.
        /// </summary>
        public string MapSectionIdentifier { get; set; }
    } // SectionExportStatDto
}
