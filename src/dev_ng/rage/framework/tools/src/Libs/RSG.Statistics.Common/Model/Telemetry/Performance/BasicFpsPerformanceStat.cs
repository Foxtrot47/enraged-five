﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class BasicFpsPerformanceStat
    {
        /// <summary>
        /// Where this reading is for.
        /// </summary>
        [DataMember]
        public Vector2f Location { get; set; }

        /// <summary>
        /// Convenience property.
        /// </summary>
        public double X
        {
            get { return (Location == null ? 0.0 : Location.X); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.X = (float)value;
            }
        }

        /// <summary>
        /// Convenience property.
        /// </summary>
        public double Y
        {
            get { return (Location == null ? 0.0 : Location.Y); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.Y = (float)value;
            }
        }

        /// <summary>
        /// Number of performance readings that contributed to the results.
        /// </summary>
        [DataMember]
        public int SampleSize { get; set; }

        /// <summary>
        /// Minimum frame time.
        /// </summary>
        [DataMember]
        public double MinFrameTime { get; set; }

        /// <summary>
        /// Min frame time -> fps converter.
        /// </summary>
        public double MinFps
        {
            get { return (MaxFrameTime != 0.0f ? 1000.0f / MaxFrameTime : 0.0f); }
            set { MaxFrameTime = (value != 0.0f ? 1000.0f / value : 0.0f); }
        }

        /// <summary>
        /// Average frame time.
        /// </summary>
        [DataMember]
        public double AvgFrameTime { get; set; }

        /// <summary>
        /// Averag frame time -> fps converter.
        /// </summary>
        public double AvgFps
        {
            get { return (AvgFrameTime != 0.0f ? 1000.0f / AvgFrameTime : 0.0f); }
            set { AvgFrameTime = (value != 0.0f ? 1000.0f / value : 0.0f); }
        }

        /// <summary>
        /// Maximum frame time.
        /// </summary>
        [DataMember]
        public double MaxFrameTime { get; set; }

        /// <summary>
        /// Max frame time -> fps converter.
        /// </summary>
        public double MaxFps
        {
            get { return (MinFrameTime != 0.0f ? 1000.0f / MinFrameTime : 0.0f); }
            set { MinFrameTime = (value != 0.0f ? 1000.0f / value : 0.0f); }
        }
    } // BasicPerformanceStat
}
