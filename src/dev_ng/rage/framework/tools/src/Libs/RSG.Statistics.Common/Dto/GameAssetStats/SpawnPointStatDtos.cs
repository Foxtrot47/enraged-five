﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// List of spawn point stats.
    /// </summary>
    [DataContract]
    [Serializable]
    public class SpawnPointStatDtos : IDtos<SpawnPointStatDto>
    {
        [DataMember]
        public List<SpawnPointStatDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SpawnPointStatDtos()
        {
            Items = new List<SpawnPointStatDto>();
        }
    } // SpawnPointStatDtos
}
