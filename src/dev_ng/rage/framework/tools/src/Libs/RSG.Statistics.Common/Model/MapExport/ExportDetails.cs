﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// Detailed information about a single export.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class ExportDetails
    {
        #region Properties
        /// <summary>
        /// Id of the export. For internal stats user only.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Time that the export started.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// Time that the export started.
        /// </summary>
        [DataMember]
        public DateTime? End { get; set; }

        /// <summary>
        /// Precomputed time that the export took.
        /// </summary>
        [DataMember]
        public ulong TotalTime { get; set; }

        /// <summary>
        /// Name of the user that performed this export.
        /// </summary>
        [DataMember]
        public String Username { get; set; }

        /// <summary>
        /// Name of the machine the export took place on.
        /// </summary>
        [DataMember]
        public String MachineName { get; set; }

        /// <summary>
        /// Type of export that was performed.
        /// </summary>
        [DataMember]
        public String ExportType { get; set; }

        /// <summary>
        /// Version of the tools that the user was on when they exported.
        /// </summary>
        [DataMember]
        public float? ToolsVersion { get; set; }

        /// <summary>
        /// Flag indicating whether XGE was enabled for the export.
        /// </summary>
        [DataMember]
        public bool? XGEEnabled { get; set; }

        /// <summary>
        /// Flag indicating whether XGE was running in standalone mode.
        /// </summary>
        [DataMember]
        public bool? XGEStandalone { get; set; }

        /// <summary>
        /// Number of CPU that XGE has been forced to use.
        /// </summary>
        [DataMember]
        public int? XGEForceCPUCount { get; set; }

        /// <summary>
        /// Whether the export was successful.
        /// </summary>
        [DataMember]
        public bool? Success { get; set; }

        /// <summary>
        /// Number of export that this is in the max session.
        /// </summary>
        [DataMember]
        public uint ExportNumber { get; set; }

        /// <summary>
        /// Processor name.
        /// </summary>
        [DataMember]
        public String ProcessorName { get; set; }

        /// <summary>
        /// Number of cores the processor has.
        /// </summary>
        [DataMember]
        public uint? ProcessorCores { get; set; }

        /// <summary>
        /// Amount of memory installed on the machine.
        /// </summary>
        [DataMember]
        public ulong? InstalledMemory { get; set; }

        /// <summary>
        /// Private bytes used at the start of the export.
        /// </summary>
        [DataMember]
        public ulong? PrivateBytesStart { get; set; }

        /// <summary>
        /// Private bytes used at the end of the export.
        /// </summary>
        [DataMember]
        public ulong? PrivateBytesEnd { get; set; }

        /// <summary>
        /// Sections that were selected for export.
        /// </summary>
        [DataMember]
        public List<String> SectionsExported { get; set; }

        /// <summary>
        /// List of platforms that the user has enabled in the config.
        /// </summary>
        [DataMember]
        public List<RSG.Platform.Platform> PlatformsEnabled { get; set; }

        /// <summary>
        /// Hierarchical breakdown of the time into sub steps.
        /// </summary>
        [DataMember]
        public List<ExportDetailsSubStep> SubSteps { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExportDetails()
        {
            SectionsExported = new List<String>();
            PlatformsEnabled = new List<RSG.Platform.Platform>();
            SubSteps = new List<ExportDetailsSubStep>();
        }
        #endregion // Constructor(s)
    } // ExportBreakdown
}
