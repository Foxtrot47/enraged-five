﻿namespace RSG.Statistics.Common.Dto.ExportStats
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    /// <summary>
    /// Defines the database object representing statistics for the network export
    /// functionality within 3ds max.
    /// </summary>
    [Serializable]
    [XmlRoot("MapNetworkExportStat")]
    [DataContract]
    public class MapNetworkExportStatDto : EntityDtoBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="JbId"/> property.
        /// </summary>
        private Guid jobId;

        /// <summary>
        /// The private field used for the <see cref="Username"/> property.
        /// </summary>
        private string username;

        /// <summary>
        /// The private field used for the <see cref="MachineName"/> property.
        /// </summary>
        private string machineName;

        /// <summary>
        /// The private field used for the <see cref="ExportType"/> property.
        /// </summary>
        private string exportType;

        /// <summary>
        /// The private field used for the <see cref="SectionNames"/> property.
        /// </summary>
        private List<string> sectionNames;

        /// <summary>
        /// The private field used for the <see cref="JobCreationNumber"/> property.
        /// </summary>
        private uint jobCreationNumber;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the jobs unique identifier that can be used to reference it or link it
        /// with addition statistics.
        /// </summary>
        [DataMember]
        public Guid JobId
        {
            get { return this.jobId; }
            set { this.jobId = value; }
        }

        /// <summary>
        /// Gets or sets the name of the user who created the job.
        /// </summary>
        [DataMember]
        public string Username
        {
            get { return this.username; }
            set { this.username = value; }
        }

        /// <summary>
        /// Gets or sets the name of the machine the export was performed on.
        /// </summary>
        [DataMember]
        public string MachineName
        {
            get { return this.machineName; }
            set { this.machineName = value; }
        }

        /// <summary>
        /// Gets or sets the type of export that the user performed (e.g. Container, Selected,
        /// Preview).
        /// </summary>
        [DataMember]
        public string ExportType
        {
            get { return this.exportType; }
            set { this.exportType = value; }
        }

        /// <summary>
        /// Gets or sets the name of the sections that are part of this export.
        /// </summary>
        [DataMember]
        public List<string> SectionNames
        {
            get { return this.sectionNames; }
            set { this.sectionNames = value; }
        }

        /// <summary>
        /// Gets or sets the number of times the user has used the network export functionality
        /// since 3ds max was started.
        /// </summary>
        [DataMember]
        public uint JobCreationNumber
        {
            get { return this.jobCreationNumber; }
            set { this.jobCreationNumber = value; }
        }
        #endregion
    } // RSG.Statistics.Common.Dto.ExportStats.MapExportStatDto {Class}
} // RSG.Statistics.Common.Dto.ExportStats {Namespace}
