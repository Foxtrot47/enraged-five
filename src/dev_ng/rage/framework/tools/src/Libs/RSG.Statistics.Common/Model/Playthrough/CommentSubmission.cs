﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Playthrough
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CommentSubmission
    {
        #region Properties
        /// <summary>
        /// Comment the user added.
        /// </summary>
        [DataMember]
        public String Comment { get; set; }

        /// <summary>
        /// Context associated with this comment.  One of: null, Mission, Weapon or Vehicle.
        /// </summary>
        [DataMember]
        public String Context { get; set; }

        /// <summary>
        /// Identifier of the item to relate tie this back to.
        /// </summary>
        [DataMember]
        public String ContextIdentifier { get; set; }
        #endregion // Properties
    } // CommentSubmission
}
