﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("GameType")]
    public class GameTypeDto : EnumDto<RSG.Statistics.Common.GameType>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public GameTypeDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        public GameTypeDto(RSG.Statistics.Common.GameType gameType, String name)
            : base(gameType, name)
        {
        }
        #endregion // Constructor(s)
    } // GameTypeDto
}
