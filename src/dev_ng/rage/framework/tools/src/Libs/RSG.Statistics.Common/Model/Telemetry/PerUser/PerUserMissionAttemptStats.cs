﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry.PerUser
{
    /// <summary>
    /// Concrete version for wcf serialisation purposes.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserMissionAttemptStats : PerUserStat<List<MissionAttemptStat>>
    {
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserMissionAttemptStats()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserMissionAttemptStats(String gamerHandle, ROSPlatform platform)
            : base(gamerHandle, platform, new List<MissionAttemptStat>())
        {
        }
        #endregion // Constructor(s)
    } // PerUserMissionAttemptStats
}
