﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public abstract class SimpleArchetypeStatBundle : ArchetypeStatBundle
    {
        #region Properties
        /// <summary>
        /// Whether this is a dynamic archetype.
        /// </summary>
        [DataMember]
        public bool IsDynamic { get; set; }

        /// <summary>
        /// Collision group for this archetype.
        /// </summary>
        [DataMember]
        public string CollisionGroup { get; set; }

        /// <summary>
        /// Whether the archetype has the default collision group.
        /// </summary>
        [DataMember]
        public bool HasDefaultCollisionGroup { get; set; }

        /// <summary>
        /// Whether to force the collision to be baked.
        /// </summary>
        [DataMember]
        public bool ForceBakeCollision { get; set; }

        /// <summary>
        /// Whether collision is never baked.
        /// </summary>
        [DataMember]
        public bool NeverBakeCollision { get; set; }

        /// <summary>
        /// Drawable LODs this archetype has.
        /// </summary>
        [DataMember]
        public List<DrawableLodStatDto> DrawableLods { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SimpleArchetypeStatBundle()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // SimpleArchetypeStatBundleDto
}
