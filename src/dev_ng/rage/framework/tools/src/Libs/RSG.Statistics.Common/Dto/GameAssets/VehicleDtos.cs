﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("Vehicles")]
    public class VehicleDtos : IDtos<VehicleDto>
    {
        [DataMember]
        [XmlElement("Vehicle")]
        public List<VehicleDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public VehicleDtos()
        {
            Items = new List<VehicleDto>();
        }
    } // VehicleDtos
}
