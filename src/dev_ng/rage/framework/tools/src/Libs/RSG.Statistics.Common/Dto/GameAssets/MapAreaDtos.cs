﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("MapAreas")]
    public class MapAreaDtos : IDtos<MapAreaDto>
    {
        [XmlElement("MapArea")]
        public List<MapAreaDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MapAreaDtos()
        {
            Items = new List<MapAreaDto>();
        }
    } // MapAreaDtos
}
