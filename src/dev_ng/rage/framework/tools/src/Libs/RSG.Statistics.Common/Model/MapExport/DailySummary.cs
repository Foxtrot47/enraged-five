﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// Daily summary information.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class DailySummary
    {
        #region Properties
        /// <summary>
        /// Stats for all users.
        /// </summary>
        [DataMember]
        public List<UserSummary> UserStats { get; set; }

        /// <summary>
        /// Stats for all sections.
        /// </summary>
        [DataMember]
        public List<SectionSummary> SectionStats { get; set; }

        /// <summary>
        /// All the exports that happened that day.
        /// </summary>
        [DataMember]
        public List<ExportDetails> ExportStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DailySummary()
        {
            UserStats = new List<UserSummary>();
            SectionStats = new List<SectionSummary>();
            ExportStats = new List<ExportDetails>();
        }
        #endregion // Constructor(s)
    } // DailySummary
}
