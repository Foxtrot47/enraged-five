﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using System.Runtime.Serialization;
using RSG.Model.Common.Map;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapSectionAggregateStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public StatGroup Group { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public StatLodLevel LodLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public StatEntityType EntityType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IncludesPropGroup { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint TotalCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint UniqueCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint DrawableCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint TXDCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint TXDCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint ShaderCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint TextureCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint PolygonCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<CollisionPolyStatDto> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public float LodDistance { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        internal MapSectionAggregateStatDto()
        {
        }
        #endregion // Constructor(s)
    } // AggregatedStatDto
}
