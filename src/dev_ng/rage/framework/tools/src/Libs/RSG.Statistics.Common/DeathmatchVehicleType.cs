﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Vehicle type used for deathmatches.
    /// These are based off of the vehicle types sent in script and match up to the game text tags with VDM_VH_.
    /// </summary>
    public enum DeathmatchVehicleType
    {
        [Obsolete("This shouldn't be coming through in the stats anymore")]
        [FriendlyName("Any (obsolete)")]
        Any = -1,

        [FriendlyName("On Foot")]
        OnFoot = 0,

        [FriendlyName("Buzzard")]
        Buzzard = 7,

        [FriendlyName("Lazer")]
        Lazer = 8,

        [FriendlyName("Rhino")]
        Rhino = 9,

        [FriendlyName("Unknown")]
        Unknown
    } // DeathmatchVehicleType

    /// <summary>
    /// Util/extension methods for deathmatch vehicle types
    /// </summary>
    public static class DeathmatchVehicleTypeUtils
    {
        /// <summary>
        /// Converts a match type integer to an enum value.
        /// </summary>
        public static DeathmatchVehicleType ConvertToDeathmatchVehicleType(int value)
        {
            DeathmatchVehicleType type;
            if (Enum.IsDefined(typeof(DeathmatchVehicleType), value))
            {
                type = (DeathmatchVehicleType)value;
            }
            else
            {
                type = DeathmatchVehicleType.Unknown;
            }
            return type;
        }
    } // DeathmatchVehicleTypeUtils
}
