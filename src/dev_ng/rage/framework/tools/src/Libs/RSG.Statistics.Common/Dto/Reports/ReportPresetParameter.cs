﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.Reports
{
    /// <summary>
    /// A single parameter that is part of a report preset.
    /// </summary>
    [DataContract]
    public class ReportPresetParameter
    {
        #region Properties
        /// <summary>
        /// For internal stats server use only.
        /// </summary>
        public long PresetId { get; set; }

        /// <summary>
        /// Key for this parameter.
        /// </summary>
        [DataMember]
        public String Key { get; set; }

        /// <summary>
        /// Value for this parameter.
        /// </summary>
        [DataMember]
        public String Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ReportPresetParameter()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ReportPresetParameter(String key, String value)
        {
            Key = key;
            Value = value;
        }
        #endregion // Constructor(s)
    } // ReportPresetParameter
}
