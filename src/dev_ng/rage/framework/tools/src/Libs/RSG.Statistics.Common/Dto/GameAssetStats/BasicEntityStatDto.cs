﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BasicEntityStatDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String ArchetypeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal BasicEntityStatDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public BasicEntityStatDto(String name, String archetypeName)
        {
            Name = name;
            ArchetypeName = archetypeName;
        }
    } // BasicEntityStatDto
}
