﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.UGC
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionContentSummary
    {
        #region Properties
        /// <summary>
        /// Mission type information.
        /// </summary>
        [DataMember]
        public IList<MissionTypeStat> MissionTypes { get; set; }

        /// <summary>
        /// Pieces of content with the specified player counts.
        /// </summary>
        [DataMember]
        public IList<KeyValueStat<int, ulong>> ContentPerPlayerCounts { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionContentSummary()
        {
            MissionTypes = new List<MissionTypeStat>();
            ContentPerPlayerCounts = new List<KeyValueStat<int, ulong>>();
        }
        #endregion // Constructor(s)
    } // MissionContentSummary
}
