﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// A sub step of the map export map section export.
    /// </summary>
    public enum SectionExportSubTask
    {
        PreExport,
        CollisionExport,
        AnimationExport,
        GeometryExport,
        TextureExport,
        OcclusionExport
    } // SectionExportSubTask
}
