﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Type of grouping for date time based queries.
    /// </summary>
    public enum DateTimeGroupMode
    {
        [FriendlyName("Five Minutely")]
        FiveMinutely,

        [FriendlyName("Quarter Hourly")]
        QuarterHourly,

        [FriendlyName("Half Hourly")]
        HalfHourly,

        [FriendlyName("Hourly")]
        Hourly,

        [FriendlyName("Daily")]
        Daily,

        [FriendlyName("Weekly")]
        Weekly,

        [FriendlyName("Monthly")]
        Monthly,

        [FriendlyName("Quarterly")]
        Quarterly,

        [FriendlyName("Yearly")]
        Yearly
    } // DateTimeGroupMode
}
