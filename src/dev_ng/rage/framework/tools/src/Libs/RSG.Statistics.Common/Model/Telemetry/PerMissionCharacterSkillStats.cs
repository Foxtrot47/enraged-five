﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class PerMissionCharacterSkillStats
    {
        #region Properties
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Id of the mission.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Skills associated with Michael.
        /// </summary>
        [DataMember]
        public CharacterSkillStats Michael { get; set; }

        /// <summary>
        /// Skills associated with Franklin.
        /// </summary>
        [DataMember]
        public CharacterSkillStats Franklin { get; set; }

        /// <summary>
        /// Skills associated with Trevor.
        /// </summary>
        [DataMember]
        public CharacterSkillStats Trevor { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerMissionCharacterSkillStats()
        {
            Michael = new CharacterSkillStats();
            Franklin = new CharacterSkillStats();
            Trevor = new CharacterSkillStats();
        }
        #endregion // Constructor(s)
    } // PerMissionCharacterSkillStats
}
