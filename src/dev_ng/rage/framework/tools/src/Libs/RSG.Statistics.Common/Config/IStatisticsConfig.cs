﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;

namespace RSG.Statistics.Common.Config
{
    /// <summary>
    /// 
    /// </summary>
    public interface IStatisticsConfig : IEnumerable<IStatsServer>
    {
        #region Properties
        /// <summary>
        /// Main game config.
        /// </summary>
        IBranch Branch { get; }

        /// <summary>
        /// Name of the default server to connect to.
        /// </summary>
        String DefaultServerName { get; }

        /// <summary>
        /// Default server to connect to.
        /// </summary>
        IStatsServer DefaultServer { get; }

        /// <summary>
        /// Collection of servers for this project.
        /// </summary>
        IDictionary<String, IStatsServer> Servers { get; }

        /// <summary>
        /// Collection of vertica servers that we can connect to.
        /// </summary>
        IDictionary<String, IVerticaServer> VerticaServers { get; }

        /// <summary>
        /// Root report cache directory.
        /// Don't use this directly.  Use the ReportCacheDir inside IServer instead. 
        /// </summary>
        String ReportCacheDirectoryRoot { get; }

        /// <summary>
        /// Base directory in which the stylesheets are stored.
        /// </summary>
        String StylesheetsDirectory { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        void Reload();
        #endregion // Methods
    } // IStatisticsConfig
}
