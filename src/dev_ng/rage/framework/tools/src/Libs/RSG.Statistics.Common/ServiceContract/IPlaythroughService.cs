﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Model.Playthrough;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IPlaythroughService
    {
        #region Data Retrieval
        /// <summary>
        /// Retrieves list of all users that have performed a playthrough session.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Users")]
        List<PlaythroughUser> GetUsers();

        /// <summary>
        /// Retrieves list of all builds that have been used for playthroughs.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Builds")]
        List<String> GetBuilds();

        /// <summary>
        /// Retrieves list of all platforms that playthroughs have occurred on.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Platforms")]
        List<RSG.Platform.Platform> GetPlatforms();

        /// <summary>
        /// Returns the list of playthrough session the database contains.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Sessions")]
        List<SessionSubmission> GetSessions();

        /// <summary>
        /// Returns the list of mission attempts associated with a particular session.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Sessions/{sessionId}/MissionAttempts")]
        List<MissionAttemptSubmission> GetMissionAttempts(String sessionId);

        /// <summary>
        /// Returns the list of checkpoint attempts associated with a particular mission attempt.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Sessions/{sessionId}/MissionAttempts/{missionAttemptId}/CheckpointAttempts")]
        List<CheckpointAttemptSubmission> GetCheckpointAttempts(String sessionId, String missionAttemptId);
        #endregion // Data Retrieval

        #region Data Submission
        /// <summary>
        /// Creates a new playthrough session.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions")]
        SessionSubmission CreateSession(SessionSubmission session);

        /// <summary>
        /// Update an existing playthrough session.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions/{id}")]
        void UpdateSession(String id, SessionSubmission session);

        /// <summary>
        /// Add mission attempt information to a playthrough session.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions/{sessionId}/MissionAttempts")]
        MissionAttemptSubmission AddMissionAttempt(String sessionId, MissionAttemptSubmission attempt);

        /// <summary>
        /// Update an existing mission attempt.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions/{sessionId}/MissionAttempts/{attemptId}")]
        void UpdateMissionAttempt(String sessionId, String attemptId, MissionAttemptSubmission attempt);

        /// <summary>
        /// Add mission checkpoint attempt information to a playthrough session/mission attempt.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions/{sessionId}/MissionAttempts/{attemptId}/CheckpointAttempts")]
        CheckpointAttemptSubmission AddCheckpointAttempt(String sessionId, String attemptId, CheckpointAttemptSubmission attempt);

        /// <summary>
        /// Update mission checkpoint attempt information.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions/{sessionId}/MissionAttempts/{missionAttemptId}/CheckpointAttempts/{checkpointAttemptId}")]
        void UpdateCheckpointAttempt(String sessionId, String missionAttemptId, String checkpointAttemptId, CheckpointAttemptSubmission attempt);

        /// <summary>
        /// Adds a generic comment to a particular playthrough session.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Sessions/{sessionId}/Comments")]
        void AddGenericComment(String sessionId, CommentSubmission comment);
        #endregion // Data Submission
    } // IPlaythroughService
}
