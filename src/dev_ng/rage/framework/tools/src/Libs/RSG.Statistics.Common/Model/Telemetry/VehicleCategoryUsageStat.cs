﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Base.Extensions;
using RSG.Model.Common.Report;
using RSG.Model.Common;
using RSG.Statistics.Common.Model.Telemetry;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class VehicleCategoryUsageStat : StatBase
    {
        #region Properties
        /// <summary>
        /// Name of the vehicle this stat is for.
        /// </summary>
        [DataMember]
        public VehicleCategory Category
        {
            get { return m_category; }
            set
            {
                m_category = value;
                Name = m_category.ToString();
                FriendlyName = m_category.GetFriendlyNameValue();
            }
        }
        private VehicleCategory m_category;

        /// <summary>
        /// Value associated with this stat.
        /// </summary>
        [DataMember]
        public decimal DistanceDriven { get; set; }

        /// <summary>
        /// Value associated with this stat.
        /// </summary>
        [DataMember]
        public decimal TimesDriven { get; set; }

        /// <summary>
        /// Number of unique gamers that have driven this vehicle.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// List of vehicle stats.
        /// </summary>
        [DataMember]
        public IList<VehicleUsageStat> PerVehicleStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation
        /// </summary>
        public VehicleCategoryUsageStat()
            : base()
        {
            PerVehicleStats = new List<VehicleUsageStat>();
        }
        #endregion // Constructor(s)
    } // VehicleCategoryStatDto
}
