﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Single stat related a cutscene.  Contains information about how many times a stat started/was skipped/was watched to completion.
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class CutsceneViewStat
    {
        /// <summary>
        /// Name of this cutscene.
        /// </summary>
        [DataMember]
        public String CutsceneName { get; set; }

        /// <summary>
        /// Friendly name of this cutscene.
        /// </summary>
        [DataMember]
        public String CutsceneFriendlyName { get; set; }

        /// <summary>
        /// How long this cutscene takes.
        /// </summary>
        [DataMember]
        public double CutsceneDuration { get; set; }

        /// <summary>
        /// Flag indicating whether this cutscene has branches.
        /// </summary>
        [DataMember]
        public bool CutsceneHasBranches { get; set; }

        /// <summary>
        /// How long people spent on average watching this cutscene.
        /// </summary>
        [DataMember]
        public double AverageTimeWatched { get; set; }

        /// <summary>
        /// Number of times we registered that the cutscene started playing.
        /// </summary>
        [DataMember]
        public ulong Started { get; set; }

        /// <summary>
        /// Number of times users chose to skip the cutscene.
        /// </summary>
        [DataMember]
        public ulong Skipped { get; set; }

        /// <summary>
        /// Number of time users chose to watch the cutscene to completion.
        /// </summary>
        [DataMember]
        public ulong Ended
        {
            get { return Started - Skipped; }
            set { }
        }

        /// <summary>
        /// Number of distinct gamers that have started viewing cutscenes.
        /// </summary>
        [DataMember]
        public ulong NumGamers { get; set; }

        /// <summary>
        /// Number of gamers that have watched this cutscene to completion.
        /// </summary>
        [DataMember]
        public ulong PlayersWatchedToCompletion { get; set; }
    } // CutsceneViewStatDto
}
