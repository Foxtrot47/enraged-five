﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information for a single freemode match type.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class TutorialData
    {
        #region Properties
        /// <summary>
        /// Information about the tutorial race.
        /// </summary>
        [DataMember]
        public TutorialMatchStat RacePlayerDetails { get; set; }

        /// <summary>
        /// Information about the tutorial mission.
        /// </summary>
        [DataMember]
        public TutorialMatchStat MissionPlayerDetails { get; set; }

        /// <summary>
        /// Jobs that are performed as part of the tutorial.
        /// </summary>
        [DataMember]
        public List<FreemodeMatchVariantSummary> TutorialJobs { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TutorialData()
        {
            RacePlayerDetails = new TutorialMatchStat();
            MissionPlayerDetails = new TutorialMatchStat();
            TutorialJobs = new List<FreemodeMatchVariantSummary>();
        }
        #endregion // Constructor(s)
    } // TutorialData


    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class TutorialMatchStat
    {
        #region Properties
        /// <summary>
        /// Match type this stat is for.
        /// </summary>
        [DataMember]
        public MatchType MatchType { get; set; }

        /// <summary>
        /// Amount of time spent playing this variant (in seconds).
        /// </summary>
        [DataMember]
        public ulong TimeSpentPlaying { get; set; }

        /// <summary>
        /// Number of times that players have played this variant.
        /// </summary>
        [DataMember]
        public ulong TimesPlayed { get; set; }

        /// <summary>
        /// Total number of gamers that played this variant.
        /// </summary>
        [DataMember]
        public ulong TotalGamers { get; set; }

        /// <summary>
        /// Number of unique gamers that played this variant.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Number of players that took part in the match.
        /// </summary>
        [DataMember]
        public IList<ParticipantInfo> ParticipantInfo { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TutorialMatchStat()
        {
            ParticipantInfo = new List<ParticipantInfo>();
        }
        #endregion // Constructor(s)
    } // TutorialMatchStat


    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class ParticipantInfo
    {
        /// <summary>
        /// Match type this stat is for.
        /// </summary>
        [DataMember]
        public MatchType MatchType { get; set; }

        /// <summary>
        /// Number of participants that took part in the match.
        /// </summary>
        [DataMember]
        public uint ParticipantCount { get; set; }

        /// <summary>
        /// Number of gamers that fall under this category.
        /// </summary>
        [DataMember]
        public ulong GamerCount { get; set; }

        /// <summary>
        /// Average number of deaths per gamer that fall in this category.
        /// </summary>
        [DataMember]
        public float AverageDeaths { get; set; }
    } // PlayerInfo
}
