﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CheckpointAttemptSubmission
    {
        /// <summary>
        /// Database id for this checkpoint attempt.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Checkpoint this attempt was for.
        /// </summary>
        [DataMember]
        public uint PreviousCheckpointIdx { get; set; }

        /// <summary>
        /// Checkpoint this attempt was for.
        /// </summary>
        [DataMember]
        public uint NextCheckpointIdx { get; set; }

        /// <summary>
        /// When the user started this mission.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// When the user finished this mission.
        /// </summary>
        [DataMember]
        public DateTime End { get; set; }

        /// <summary>
        /// Name of the checkpoint.
        /// </summary>
        [DataMember]
        public String CheckpointName { get; set; }

        /// <summary>
        /// How long the user took to complete this mission (taking into account whether the game was paused).
        /// </summary>
        [DataMember]
        public uint TimeToComplete { get; set; }

        /// <summary>
        /// Comment the user added for this checkpoint.
        /// </summary>
        [DataMember]
        public String UserComment { get; set; }

        /// <summary>
        /// Whether this checkpoint attempt should be ignored.
        /// </summary>
        [DataMember]
        public bool Ignore { get; set; }
    } // CheckpointAttemptSubmission
}
