﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using RSG.Model.Common;
using RSG.Base.Math;
using RSG.Model.Common.Map;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class RoomStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Entities identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableRoomStat.ShadersString)]
        public List<ShaderStatDto> ShaderStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableRoomStat.BoundingBoxString)]
        public BoundingBox3f BoundingBox { get; set; }

        /// <summary>
        /// List of texture dictionaries and their associated export sizes.
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.TxdExportSizesString)]
        public List<TxdExportStatDto> TxdExportSizes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.ExportGeometrySizeString)]
        public uint? ExportGeometrySize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.PolygonCountString)]
        public uint? PolygonCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.CollisionPolygonCountString)]
        public uint? CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this map section stat.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableRoomStat.CollisionTypePolygonCountsString)]
        public List<CollisionPolyStatDto> CollisionPolygonCountBreakdown { get; set; }

        /// <summary>
        /// List of entities this room contains.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableRoomStat.EntitiesString)]
        public List<BasicEntityStatDto> Entities { get; set; }
        #endregion // Properties
    } // RoomStatDto
}
