﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Statistics.Common.Config
{
    /// <summary>
    /// Vertica server config class.
    /// </summary>
    internal class VerticaServer : IVerticaServer
    {
        #region Properties
        /// <summary>
        /// Config object parent.
        /// </summary>
        public IStatisticsConfig Config { get; private set; }

        /// <summary>
        /// Name associated with this server.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// XML configuration filename.
        /// </summary>
        public String Filename { get; private set; }

        /// <summary>
        /// Address to use to access the server.
        /// </summary>
        public String Address { get; private set; }

        /// <summary>
        /// Port to host the web pages and HTTP wcf endpoints on.
        /// </summary>
        public uint Port { get; private set; }

        /// <summary>
        /// Service to use for retrieving the data.
        /// </summary>
        public String Service { get; private set; }

        /// <summary>
        /// Name of the database to connect to.
        /// </summary>
        public String DatabaseName { get; private set; }

        /// <summary>
        /// Location that the database resides in.
        /// </summary>
        public String DatabaseLocation { get; private set; }

        /// <summary>
        /// Username to use to connect to the database.
        /// </summary>
        public String DatabaseUsername { get; private set; }

        /// <summary>
        /// Password to use to connect to the database.
        /// </summary>
        public String DatabasePassword { get; private set; }

        /// <summary>
        /// How long SQL commands can run on the db before timing out.  Set to 0 for an infinite timeout.
        /// </summary>
        public uint CommandTimeout { get; private set; }

        /// <summary>
        /// Whether the server is running inside of IIS.
        /// </summary>
        public bool UsingIIS { get; private set; }

        /// <summary>
        /// Prefix for the db table schema.
        /// </summary>
        public String SchemaPrefix { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default
        /// </summary>
        /// <param name="config"></param>
        /// <param name="xmlElem"></param>
        internal VerticaServer(IStatisticsConfig config, XElement xmlElem)
        {
            Config = config;
            ParseAttributes(xmlElem);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Parse server attributes.
        /// </summary>
        /// <param name="xmlElem"></param>
        private void ParseAttributes(XElement serverElem)
        {
            Name = GetAttributeValueChecked(serverElem, "name");
            Address = GetAttributeValueChecked(serverElem, "server_address");
            Port = UInt32.Parse(GetAttributeValueChecked(serverElem, "server_port"));
            Service = GetAttributeValueChecked(serverElem, "server_service");
            DatabaseName = GetAttributeValueChecked(serverElem, "db_name");
            DatabaseLocation = GetAttributeValueChecked(serverElem, "db_location");
            DatabaseUsername = GetAttributeValueChecked(serverElem, "db_username");
            DatabasePassword = GetAttributeValueChecked(serverElem, "db_password");
            CommandTimeout = UInt32.Parse(GetAttributeValueChecked(serverElem, "command_timeout"));

            if (serverElem.Attribute("using_iis") != null)
            {
                UsingIIS = Boolean.Parse(GetAttributeValueChecked(serverElem, "using_iis"));
            }

            SchemaPrefix = GetAttributeValueChecked(serverElem, "schema_prefix");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elem"></param>
        /// <param name="attName"></param>
        /// <returns></returns>
        private String GetAttributeValueChecked(XElement elem, String attName)
        {
            XAttribute att = elem.Attribute(attName);
            Debug.Assert(att != null, String.Format("Server element is missing the '{0}' attribute.", attName));
            if (att == null)
            {
                throw new ArgumentNullException(String.Format("Server element is missing the '{0}' attribute.", attName));
            }
            return att.Value.Trim();
        }
        #endregion // Methods
    } // VerticaServer
}
