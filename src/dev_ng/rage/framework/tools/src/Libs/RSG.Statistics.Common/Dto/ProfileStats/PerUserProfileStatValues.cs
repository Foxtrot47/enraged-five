﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry.PerUser;

namespace RSG.Statistics.Common.Dto.ProfileStats
{
    /// <summary>
    /// List of profile stats for a particular user.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserProfileStatValues : PerUserStat<List<ProfileStatValue>>
    {
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserProfileStatValues()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserProfileStatValues(String gamerHandle, ROSPlatform platform)
            : base(gamerHandle, platform, new List<ProfileStatValue>())
        {
        }
        #endregion // Constructor(s)
    } // PerUserProfileStatValues
}
