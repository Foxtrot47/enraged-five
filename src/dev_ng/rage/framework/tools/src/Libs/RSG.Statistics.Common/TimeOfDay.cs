﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Times of day.
    /// Matches the constants defined in //depot/gta5/script/dev/multiplayer/include/public/net_common_functions.sch.
    /// </summary>
    public enum TimeOfDay
    {
        [FriendlyName("Anytime")]
        Anytime = 0,

        [FriendlyName("Morning")]
        Morning,

        [FriendlyName("Midday")]
        Noon,

        [FriendlyName("Night")]
        Night,

        [FriendlyName("Midnight")]
        MidNight,

        [FriendlyName("Other")]
        Other = -1
    } // TimeOfDay

    /// <summary>
    /// Utility methods for things associated with the TimeOfDay enum.
    /// </summary>
    public static class TimeOfDayUtils
    {
        /// <summary>
        /// Converts a hour bitset to a time of day.
        /// </summary>
        /// <param name="bitset"></param>
        /// <returns></returns>
        public static TimeOfDay ConvertBitsetToTimeOfDay(uint bitset)
        {
            if (bitset == 0)
            {
                return TimeOfDay.Anytime;
            }
            else if ((bitset & 0xFF0) != 0)
            {
                return TimeOfDay.Morning;
            }
            else if ((bitset & 0xFF000) != 0)
            {
                return TimeOfDay.Noon;
            }
            else if ((bitset & 0xF0000F) != 0)
            {
                return TimeOfDay.Night;
            }
            else
            {
                return TimeOfDay.Other;
            }
        }
    } // TimeOfDayUtils
}
