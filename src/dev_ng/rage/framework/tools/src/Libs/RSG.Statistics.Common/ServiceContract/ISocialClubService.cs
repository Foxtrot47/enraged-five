﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RSG.Statistics.Common.Model.SocialClub;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service that exposes social club related information.
    /// </summary>
    [ServiceContract]
    [ServiceKnownType(typeof(User))]
    [ServiceKnownType(typeof(ActorGroup))]
    public interface ISocialClubService
    {
        #region Countries
        /// <summary>
        /// Retrieves the list of countries.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Countries")]
        List<Country> GetCountries();

        /// <summary>
        /// Creates a new country.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Countries")]
        Country CreateCountry(Country country);
        #endregion // Countries
    } // ISocialClubService
}
