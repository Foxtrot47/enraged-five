﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MemoryShortfallStat
    {
        /// <summary>
        /// Where this reading is for.
        /// </summary>
        [DataMember]
        public Vector2f Location { get; set; }

        /// <summary>
        /// Convenience property (Used by the nhibernate criteria).
        /// </summary>
        public double X
        {
            get { return (Location == null ? 0.0 : Location.X); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.X = (float)value;
            }
        }

        /// <summary>
        /// Convenience property (Used by the nhibernate criteria).
        /// </summary>
        public double Y
        {
            get { return (Location == null ? 0.0 : Location.Y); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.Y = (float)value;
            }
        }

        /// <summary>
        /// Number of performance readings that contributed to the results.
        /// </summary>
        [DataMember]
        public int SampleSize { get; set; }

        /// <summary>
        /// Min physical memory shortfall for this processed area.
        /// </summary>
        [DataMember]
        public double MinPhysicalShortfall { get; set; }

        /// <summary>
        /// Average physical memory shortfall for this processed area.
        /// </summary>
        [DataMember]
        public double AvgPhysicalShortfall { get; set; }

        /// <summary>
        /// Max physical memory shortfall for this processed area.
        /// </summary>
        [DataMember]
        public double MaxPhysicalShortfall { get; set; }

        /// <summary>
        /// Min virtual memory shortfall for this processed area.
        /// </summary>
        [DataMember]
        public double MinVirtualShortfall { get; set; }

        /// <summary>
        /// Average virtual memory shortfall for this processed area.
        /// </summary>
        [DataMember]
        public double AvgVirtualShortfall { get; set; }

        /// <summary>
        /// Max virtual memory shortfall for this processed area.
        /// </summary>
        [DataMember]
        public double MaxVirtualShortfall { get; set; }
    } // MemoryShortfall
}
