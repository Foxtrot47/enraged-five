﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MediaStat : StatBase
    {
        /// <summary>
        /// Number of times this activity was "performed".
        /// </summary>
        [DataMember]
        public ulong Count { get; set; }

        /// <summary>
        /// Amount of time spent "performing" this activity (in seconds).
        /// </summary>
        [DataMember]
        public ulong Duration { get; set; }

        /// <summary>
        /// Unique number of gamers that "performed" this activity.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }
    } // MediaStat
}
