﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    public class CharacterSkillStats
    {
        [DataMember]
        public float SpecialAbility { get; set; }

        [DataMember]
        public float Stamina { get; set; }

        [DataMember]
        public float Shooting { get; set; }

        [DataMember]
        public float Strength { get; set; }

        [DataMember]
        public float Stealth { get; set; }

        [DataMember]
        public float Flying { get; set; }

        [DataMember]
        public float Driving { get; set; }

        [DataMember]
        public float LungCapacity { get; set; }
    } // CharacterSkillStats
}
