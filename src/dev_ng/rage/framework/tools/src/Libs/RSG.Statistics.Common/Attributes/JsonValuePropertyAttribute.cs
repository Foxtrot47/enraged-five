﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonValuePropertyAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public JsonValuePropertyAttribute(string name)
            : base()
        {
            Name = name;
        }
    } // JsonPropertyAttribute
}
