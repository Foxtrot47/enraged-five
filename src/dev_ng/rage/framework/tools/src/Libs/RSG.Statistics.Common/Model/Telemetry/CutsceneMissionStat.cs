﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information for all cutscenes that were viewed as part of a mission.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class CutsceneMissionStat
    {
        #region Properties
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Id of the misison.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Cutscenes that were viewed as part of this mission.
        /// </summary>
        [DataMember]
        public List<CutsceneViewStat> CutsceneViews { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor for serialisation purposes.
        /// </summary>
        private CutsceneMissionStat()
        {
            CutsceneViews = new List<CutsceneViewStat>();
        }

        /// <summary>
        /// 
        /// </summary>
        public CutsceneMissionStat(String name, String id)
            : this()
        {
            MissionName = name;
            MissionId = id;
        }
        #endregion // Constructor(s)
    } // CutsceneMissionStat
}
