﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using System.Collections;
using RSG.Base.Configuration;

namespace RSG.Statistics.Common.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class StatisticsConfig : IStatisticsConfig, IEnumerable<IStatsServer>
    {
        #region Constants
        internal static readonly String ELEM_STATISTICS = "statistics";
        internal static readonly String ELEM_REPORTS = "reports";

        internal static readonly String ELEM_SERVERS = "servers";
        internal static readonly String ELEM_SERVER = "server";

        internal static readonly String ATTR_SERVER_DEFAULT = "default";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Main game config.
        /// </summary>
        public IBranch Branch { get; private set; }

        /// <summary>
        /// XML configuration filename.
        /// </summary>
        public String Filename { get; private set; }

        /// <summary>
        /// Name of the default server to connect to.
        /// </summary>
        public string DefaultServerName { get; private set; }

        /// <summary>
        /// Default server to connect to.
        /// </summary>
        public IStatsServer DefaultServer { get; private set; }

        /// <summary>
        /// Collection of servers for this project.
        /// </summary>
        public IDictionary<String, IStatsServer> Servers { get; private set; }

        /// <summary>
        /// Collection of vertica servers that we can connect to.
        /// </summary>
        public IDictionary<String, IVerticaServer> VerticaServers { get; private set; }

        /// <summary>
        /// Root report cache directory.
        /// Don't use this directly.  Use the ReportCacheDir inside IServer instead. 
        /// </summary>
        public String ReportCacheDirectoryRoot { get; private set; }

        /// <summary>
        /// Base directory in which the stylesheets are stored.
        /// </summary>
        public String StylesheetsDirectory { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StatisticsConfig()
        {
            Branch = ConfigFactory.CreateConfig().Project.DefaultBranch;
            Reload();
        }

        /// <summary>
        /// Preferred constructor.
        /// </summary>
        /// <param name="config"></param>
        public StatisticsConfig(IBranch branch)
        {
            Branch = branch;
            Reload();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reload all of the configuration data.
        /// </summary>
        public void Reload()
        {
            // Get the file that we wish to load and ensure that it exists
            Filename = Path.Combine(Branch.Project.Config.ToolsConfig, "statistics.xml");
            Debug.Assert(File.Exists(Filename), String.Format("Statistics Xml '{0}' does not exist.", Filename));
            if (!File.Exists(Filename))
            {
                throw new ConfigurationException(String.Format("Statistics Xml '{0}' does not exist.", Filename));
            }
            String localFilename = Path.Combine(Branch.Project.Config.ToolsConfig, "local", "statistics_local.xml");

            // Reset certain items
            Servers = new Dictionary<String, IStatsServer>();
            VerticaServers = new Dictionary<String, IVerticaServer>();

            // Load and parse the config file
            XDocument configDoc = XDocument.Load(Filename);
            XDocument localDoc = null;
            if (File.Exists(localFilename))
            {
                localDoc = XDocument.Load(localFilename);
            }

            // Parse the report config data before the servers themsevles.
            ParseReportConfig(configDoc);

            //
            ParseVerticaServers(configDoc);
            if (localDoc != null)
            {
                ParseVerticaServers(localDoc);
            }

            // Parse the normal server configuration next.
            ParseServers(configDoc);
            if (localDoc != null)
            {
                ParseServers(localDoc);
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void ParseVerticaServers(XDocument xmlDoc)
        {
            // Parse the servers.
            foreach (XElement xmlServerElem in xmlDoc.XPathSelectElements("statistics/vertica_servers/vertica_server"))
            {
                IVerticaServer server = new VerticaServer(this, xmlServerElem);
                if (VerticaServers.ContainsKey(server.Name))
                {
                    Log.Log__Warning("Detected existing vertica server config with name {0}.  Overwriting previous instance with the new one.", server.Name);
                }
                VerticaServers[server.Name] = server;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ParseServers(XDocument xmlDoc)
        {
            // Parse the servers.
            XElement xmlServersElem = xmlDoc.XPathSelectElement("statistics/servers");
            if (xmlServersElem != null)
            {
                foreach (XElement xmlServerElem in xmlServersElem.XPathSelectElements("server"))
                {
                    IStatsServer server = new StatsServer(this, xmlServerElem);
                    if (Servers.ContainsKey(server.Name))
                    {
                        Log.Log__Warning("Detected existing server config with name {0}.  Overwriting previous instance with the new one.", server.Name);
                    }
                    Servers[server.Name] = server;
                }

                SetupDefaultServer(xmlServersElem);
            }
        }

        /// <summary>
        /// Setup default branch properties.
        /// </summary>
        /// <param name="xmlServersElem"></param>
        private void SetupDefaultServer(XElement xmlServersElem)
        {
            XAttribute xmlDefaultServerAttr = xmlServersElem.Attributes().Where(
                attr => (0 == String.Compare(ATTR_SERVER_DEFAULT, attr.Name.LocalName))).FirstOrDefault();
            if (null != xmlDefaultServerAttr)
            {
                this.DefaultServerName = xmlDefaultServerAttr.Value.Trim();
                this.DefaultServer = this.Servers[this.DefaultServerName];
            }
            else
            {
                this.DefaultServerName = String.Empty;
                this.DefaultServer = null;
                Log.Log__Warning("Default server not set if config data.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportElem"></param>
        private void ParseReportConfig(XDocument doc)
        {
            // Parse report properties
            XElement reportElem = doc.XPathSelectElement("statistics/reports");
            Debug.Assert(reportElem != null, "Missing reports xml element in statistics config file.");
            if (reportElem == null)
            {
                throw new ArgumentNullException("Missing reports xml element in statistics config file.");
            }

            //
            XAttribute stylesheetAtt = reportElem.Attribute("stylesheets_dir");
            Debug.Assert(stylesheetAtt != null, "Server element is missing the 'stylesheets_dir' attribute.");
            if (stylesheetAtt == null)
            {
                throw new ArgumentNullException("Server element is missing the 'stylesheets_dir' attribute.");
            }
            String styleDir = stylesheetAtt.Value.Trim();
            StylesheetsDirectory = Path.GetFullPath(Branch.Environment.Subst(styleDir));

            XAttribute cacheAtt = reportElem.Attribute("cache_dir");
            Debug.Assert(cacheAtt != null, "Server element is missing the 'cache_dir' attribute.");
            if (cacheAtt == null)
            {
                throw new ArgumentNullException("Server element is missing the 'cache_dir' attribute.");
            }
            String cacheDir = cacheAtt.Value.Trim();
            ReportCacheDirectoryRoot = Path.GetFullPath(Branch.Environment.Subst(cacheDir));

        }
        #endregion // Private Methods

        #region IEnumerable<IStatsServer> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IStatsServer> GetEnumerator()
        {
            foreach (IStatsServer server in Servers.Values)
                yield return server;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (GetEnumerator());
        }
        #endregion // IEnumerable<IServer> Methods
    } // StatisticsConfig
}
