﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class PerMissionDeathStat
    {
        #region Properties
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Id of the mission.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Number of deaths that occurred on this mission.
        /// </summary>
        [DataMember]
        public ulong TotalDeaths { get; set; }

        /// <summary>
        /// Number of attempts at this mission.
        /// </summary>
        [DataMember]
        public ulong NumberOfAttempts { get; set; }

        /// <summary>
        /// Number of unique gamers that attempted the mission.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }
        #endregion // Properties
    } // PerMissionDeathStat
}
