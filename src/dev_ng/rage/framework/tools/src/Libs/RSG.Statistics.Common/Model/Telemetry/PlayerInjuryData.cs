﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information for a single block in the deathmatch injury report.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class PlayerInjuryData
    {
        /// <summary>
        /// Where this reading is for.
        /// </summary>
        [DataMember]
        public Vector2f Location { get; set; }

        /// <summary>
        /// Convenience property (Used by the nhibernate criteria).
        /// </summary>
        public double X
        {
            get { return (Location == null ? 0.0 : Location.X); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.X = (float)value;
            }
        }

        /// <summary>
        /// Convenience property (Used by the nhibernate criteria).
        /// </summary>
        public double Y
        {
            get { return (Location == null ? 0.0 : Location.Y); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.Y = (float)value;
            }
        }

        /// <summary>
        /// Number of times people died within this block.
        /// </summary>
        [DataMember]
        public ulong Occurrences { get; set; }
    } // DeathmatchInjuryData
}
