﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Mapping of datetimes to cash purchase stats.
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalCashPurchaseStats
    {
        #region Properties
        /// <summary>
        /// Name of this cash pack.
        /// </summary>
        [DataMember]
        public String CashPackName { get; set; }

        /// <summary>
        /// In-game cash that players get for this pack.
        /// </summary>
        [DataMember]
        public uint InGameAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public IList<HistoricalCashPurchaseStat> Stats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HistoricalCashPurchaseStats()
        {
            Stats = new List<HistoricalCashPurchaseStat>();
        }
        #endregion // Constructor(s)
    } // HistoricalCashPurchaseStats

    
    /// <summary>
    /// Single cash purchase stat.
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalCashPurchaseStat : Range<DateTime>
    {
        /// <summary>
        /// Number of times this cash pack has been purchased.
        /// </summary>
        [DataMember]
        public ulong TimesPurchased { get; set; }

        /// <summary>
        /// Number of unique gamers that have purchased this item.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }
    } // HistoricalCashPurchaseStat
}
