﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("WeaponStatBundle")]
    public class WeaponStatBundleDto : GameAssetStatBundleDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PolyCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int BoneCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int GripCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int MagazineCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int AttachmentCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int LodPolyCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasLod { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlArray("WeaponPlatformStats")]
        [XmlArrayItem("WeaponPlatformStat")]
        public List<WeaponPlatformStatBundleDto> WeaponPlatformStats { get; set; }
    } // WeaponStatBundleDto
}
