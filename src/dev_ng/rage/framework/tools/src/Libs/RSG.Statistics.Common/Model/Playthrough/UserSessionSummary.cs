﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class UserSessionSummary
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SessionCount { get; set; }

        /// <summary>
        /// Time spent playing (in seconds).
        /// </summary>
        [DataMember]
        public long TimeSpentPlaying { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int TotalMissionAttempts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int UniqueMissionAttempts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int TotalCheckpointAttempts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int UniqueCheckpointAttempts { get; set; }
    } // UserSessionSummary
}
