﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CarGenStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public Vector3f Position { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsPolice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsFire { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsAmbulance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsSpecificModel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String ModelName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsHighPriority { get; set; }
        #endregion // Properties
    } // CarGenStatDto
}
