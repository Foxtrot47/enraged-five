﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("BuildConfig")]
    [DataContract]
    public class BuildConfigDto : EnumDto<BuildConfig>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public BuildConfigDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public BuildConfigDto(BuildConfig config, String name)
            : base(config, name)
        {
        }
        #endregion // Constructor(s)
    } // BuildConfigDto
}
