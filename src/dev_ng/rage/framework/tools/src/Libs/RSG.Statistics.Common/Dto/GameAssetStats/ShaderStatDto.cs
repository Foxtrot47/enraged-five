﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ShaderStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Shaders name.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Shaders identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long GameAssetStatId { get; set; }

        /// <summary>
        /// List of textures that are associated with this shader.
        /// </summary>
        [DataMember]
        public List<TextureStatDto> TextureStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal ShaderStatDto()
        {
            TextureStats = new List<TextureStatDto>();
        }

        /// <summary>
        /// 
        /// </summary>
        public ShaderStatDto(uint identifier, long id)
        {
            Identifier = identifier;
            Id = id;
            TextureStats = new List<TextureStatDto>();
        }
        #endregion // Constructor(s)
    } // ShaderStatDto
}
