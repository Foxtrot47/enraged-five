﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ArchetypeDto : GameAssetDtoBase
    {
    } // ArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("DrawableArchetype")]
    public class DrawableArchetypeDto : ArchetypeDto
    {
    } // DrawableArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("FragmentArchetype")]
    public class FragmentArchetypeDto : ArchetypeDto
    {
    } // FragmentArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("InteriorArchetype")]
    public class InteriorArchetypeDto : ArchetypeDto
    {
    } // InteriorArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("StatedAnimArchetype")]
    public class StatedAnimArchetypeDto : ArchetypeDto
    {
    } // StatedAnimArchetypeDto
}
