﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// The Dto used to return results ( query ) for an associated query of memory store telemetry stats
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MemoryStoreStat
    {
        [DataMember]
        public String StoreName { get; set; }

        [DataMember]
        public int NumSamples { get; set; }

        [DataMember]
        public uint MinPeak { get; set; }
        [DataMember]
        public double AvgPeak { get; set; }
        [DataMember]
        public uint MaxPeak { get; set; }

        [DataMember]
        public uint MinUsed { get; set; }
        [DataMember]
        public double AvgUsed { get; set; }
        [DataMember]
        public uint MaxUsed { get; set; }

        [DataMember]
        public uint MinFree { get; set; }
        [DataMember]
        public double AvgFree { get; set; }
        [DataMember]
        public uint MaxFree { get; set; }
    } // MemoryStoreStat
}
