﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Rank
    {
        /// <summary>
        /// Rank at which this item is unlocked.
        /// </summary>
        [DataMember]
        public uint Level { get; set; }

        /// <summary>
        /// Rank at which this item is unlocked.
        /// </summary>
        [DataMember]
        public uint Experience { get; set; }

        /// <summary>
        /// Name of the unlock.
        /// </summary>
        [DataMember]
        public String Name { get; set; }
    } // Rank
}
