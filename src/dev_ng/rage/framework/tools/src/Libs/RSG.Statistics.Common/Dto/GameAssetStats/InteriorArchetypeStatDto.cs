﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class InteriorArchetypeStatDto : MapArchetypeStatDto
    {
        /// <summary>
        /// List of room statistics for this interior.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.RoomsString)]
        public List<BasicRoomStatDto> RoomStats { get; set; }

        /// <summary>
        /// List of portal statistics for this interior.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.PortalsString)]
        public List<PortalStatDto> PortalStats { get; set; }

        /// <summary>
        /// List of entities this map section contains.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.EntitiesString)]
        public List<BasicEntityStatDto> Entities { get; set; }
    } // InteriorArchetypeStatDto
}
