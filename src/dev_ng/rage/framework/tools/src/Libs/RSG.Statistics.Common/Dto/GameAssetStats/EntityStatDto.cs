﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class EntityStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Entities identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableEntityStat.PositionString)]
        public Vector3f Location { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableEntityStat.BoundingBoxString)]
        public BoundingBox3f BoundingBox { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableEntityStat.PriorityLevelString)]
        public int? PriorityLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableEntityStat.IsLowPriorityString)]
        public bool? IsLowPriority { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableEntityStat.IsReferenceString)]
        public bool? IsReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableEntityStat.IsInteriorReferenceString)]
        public bool? IsInteriorReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableEntityStat.LodLevelString)]
        public RSG.Model.Common.Map.LodLevel? LodLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableEntityStat.LodDistanceOverrideString)]
        public float? LodDistanceOverride { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableEntityStat.LodParentString)]
        public BasicEntityStatDto LodParent { get; set; }

        /// <summary>
        /// AttrNames.OBJ_FORCE_BAKE_COLLISION attribute
        /// </summary>
        [DataMember]
        [StreamableStat(StreamableEntityStat.ForceBakeCollisionString)]
        public bool? ForceBakeCollision { get; set; }
        #endregion // Properties
    } // EntityStatDto
}
