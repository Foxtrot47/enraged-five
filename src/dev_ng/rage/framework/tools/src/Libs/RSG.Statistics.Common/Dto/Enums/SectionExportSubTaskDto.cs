﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("SectionExportSubTask")]
    public class SectionExportSubTaskDto : EnumDto<SectionExportSubTask>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SectionExportSubTaskDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        public SectionExportSubTaskDto(SectionExportSubTask task, String name)
            : base(task, name)
        {
        }
        #endregion // Constructor(s)
    } // SectionExportSubStepDto
}
