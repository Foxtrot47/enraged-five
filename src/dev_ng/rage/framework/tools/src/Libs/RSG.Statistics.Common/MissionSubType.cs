﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enum for mission sub types.
    /// The values should match those defined in //depot/gta5/script/dev/multiplayer/globals/MP_globals_FM.sch.
    /// </summary>
    public enum MissionSubType
    {
        [FriendlyName("Standard")]
        Standard = 0,

        [FriendlyName("Heist")]
        Heist = 1,

        [FriendlyName("Contact")]
        Contact = 2,

        [FriendlyName("Random Event")]
        Random = 3,

        [FriendlyName("Versus")]
        Versus = 4,

        [FriendlyName("Last Team Standing")]
        LastTeamStanding = 5,

        [FriendlyName("Capture the Flag")]
        CaptureTheFlag = 6,

        [FriendlyName("Unknown")]
        Unknown                 // Unknown mission sub-type.
    } // MissionSubType
}
