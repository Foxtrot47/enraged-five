﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information for all items that were purchased in a particular store.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class ShopStoreStat
    {
        #region Properties
        /// <summary>
        /// Hash of the shop name.
        /// </summary>
        [DataMember]
        public uint ShopHash { get; set; }

        /// <summary>
        /// Name of the shop.
        /// </summary>
        [DataMember]
        public String ShopName { get; set; }

        /// <summary>
        /// Type of store this is.
        /// </summary>
        [DataMember]
        public ShopType ShopType { get; set; }

        /// <summary>
        /// Items that were purchased from the store.
        /// </summary>
        [DataMember]
        public List<ShopItemStat> Items { get; set; }

        /// <summary>
        /// Number of times items were purchased.
        /// </summary>
        [DataMember]
        public ulong TimesPurchased { get; set; }

        /// <summary>
        /// Number of unique gamers that purchased items in this store.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Total amount of money spent in this store.
        /// </summary>
        [DataMember]
        public ulong TotalSpent { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor for serialisation purposes.
        /// </summary>
        public ShopStoreStat()
        {
            Items = new List<ShopItemStat>();
        }

        /// <summary>
        /// 
        /// </summary>
        public ShopStoreStat(String name)
            : this()
        {
            ShopName = name;
        }
        #endregion // Constructor(s)
    } // ShopStoreStat
}
