﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("MapSection")]
    public class MapSectionDto : GameAssetDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Type")]
        public string Type { get; set; }
        public bool TypeSpecified { get { return !String.IsNullOrEmpty(Type); } }
        #endregion // Properties
    } // MapSectionDto
}
