﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.VerticaData
{
    /// <summary>
    /// Group of gamers.
    /// </summary>
    [DataContract]
    public class GamerGroup
    {
        #region Properties
        /// <summary>
        /// Name for this group.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Gamers that are in this group.
        /// </summary>
        [DataMember]
        public List<Gamer> Gamers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (required for serialisation).
        /// </summary>
        public GamerGroup()
        {
            Gamers = new List<Gamer>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public GamerGroup(String name)
            : this()
        {
            Name = name;
        }
        #endregion // Constructor(s)
    } // GamerGroup
}
