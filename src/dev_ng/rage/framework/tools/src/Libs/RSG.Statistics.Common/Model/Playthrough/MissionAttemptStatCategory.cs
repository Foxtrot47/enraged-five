﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionAttemptStatCategory
    {
        #region Properties
        /// <summary>
        /// Name of the mission for this attempt.
        /// </summary>
        [DataMember]
        public String CategoryName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<MissionAttemptStat> MissionAttemptStats { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionAttemptStatCategory()
        {
            MissionAttemptStats = new List<MissionAttemptStat>();
        }
        #endregion // Constructor(s)
    } // MissionAttemptStatCategory
}
