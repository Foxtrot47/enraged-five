﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Platform")]
    [DataContract]
    public class PlatformDto : EnumDto<RSG.Platform.Platform>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PlatformDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="platform"></param>
        public PlatformDto(RSG.Platform.Platform platform, String name)
            : base(platform, name)
        {
        }
        #endregion // Constructor(s)
    } // PlatformDto
}
