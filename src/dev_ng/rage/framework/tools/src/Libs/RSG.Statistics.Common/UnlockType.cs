﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Type for unlocks.
    /// </summary>
    public enum UnlockType
    {
        Weapon,
        AddOn,
        Kit,
        PhoneCall,
        CarMod,
        Health,
        MaleHair,
        FemaleHair,
        Makeup,
        Beard,

        // Clothing
        MaleLowers,
        MaleUppers,
        MaleFeet,
        MaleMasks,
        MaleHats,
        MaleHelmets,
        MaleGlasses,
        MaleWatches,

        FemaleLowers,
        FemaleUppers,
        FemaleFeet,
        FemaleMasks,
        FemaleHats,
        FemaleHelmets,
        FemaleGlasses,
        FemaleWatches,
    } // UnlockType
}
