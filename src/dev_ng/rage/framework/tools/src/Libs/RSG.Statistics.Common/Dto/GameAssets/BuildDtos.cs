﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "Builds")]
    [Serializable]
    [XmlRoot("Builds")]
    public class BuildDtos : IDtos<BuildDto>
    {
        [DataMember]
        [XmlElement("Build")]
        public List<BuildDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public BuildDtos()
        {
            Items = new List<BuildDto>();
        }
    } // BuildDtos
}
