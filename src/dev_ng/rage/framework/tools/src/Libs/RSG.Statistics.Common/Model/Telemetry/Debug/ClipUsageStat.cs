﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Animation;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Debug
{
    /// <summary>
    /// Returned by the ClipUsage and UnusedClip reports.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class ClipUsageStat
    {
        /// <summary>
        /// Name of the dictionary.
        /// </summary>
        [DataMember]
        public String DictionaryName { get; set; }

        /// <summary>
        /// Category the dictionary falls under.
        /// </summary>
        [DataMember]
        public ClipDictionaryCategory Category { get; set; }

        /// <summary>
        /// Name of the clip.
        /// </summary>
        [DataMember]
        public String ClipName { get; set; }

        /// <summary>
        /// Number of times the clip was played (will be 0 for the UnusedClip report).
        /// </summary>
        [DataMember]
        public uint TimesPlayed { get; set; }
    } // ClipUsageStat
}
