﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Model.Common;
using RSG.Base.Math;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class LevelStatDto : DtoBase
    {
        /// <summary>
        /// Section's identifier.
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// String representation of the identifier.
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get
            {
                return Identifier.ToString();
            }
            set
            {
                Identifier = UInt32.Parse(value);
            }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableLevelStat.ExportDataPathString)]
        public String ExportDataPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableLevelStat.ProcessedDataPathString)]
        public String ProcessedDataPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableLevelStat.ImageBoundsString)]
        public BoundingBox2f ImageBounds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableLevelStat.BackgroundImageString)]
        public byte[] ImageData { get; set; }

        /// <summary>
        /// List of spawn points associated with this level.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableLevelStat.SpawnPointsString)]
        public List<SpawnPointStatDto> SpawnPoints { get; set; }
    } // LevelStatDto
}
