﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class EntityDtoBase : DtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("Id")]
        [DataMember]
        public long? Id { get; set; }
        public bool IdSpecified { get { return Id.HasValue; } }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("CreatedOn")]
        [DataMember]
        public DateTime? CreatedOn { get; set; }
        public bool CreatedOnSpecified { get { return CreatedOn.HasValue; } }
        #endregion // Properties
    } // EntityDtoBase
}
