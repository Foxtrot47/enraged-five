﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Report
{
    /// <summary>
    /// Internal report that can be run on the stats server.
    /// </summary>
    public interface IStatsReport : IReport
    {
    } // IReportDataProvider
}
