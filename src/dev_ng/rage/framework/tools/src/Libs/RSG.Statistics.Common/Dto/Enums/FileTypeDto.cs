﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Platform;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("FileType")]
    [DataContract]
    public class FileTypeDto : EnumDto<FileType>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileTypeDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public FileTypeDto(FileType fileType, String name)
            : base(fileType, name)
        {
        }
        #endregion // Constructor(s)
    } // FileTypeDto
}
