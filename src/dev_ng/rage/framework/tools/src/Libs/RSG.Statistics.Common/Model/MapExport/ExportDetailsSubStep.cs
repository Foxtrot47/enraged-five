﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// Information about one of the sub steps of the export.
    /// </summary>
    [DataContract]
    [Serializable]
    public class ExportDetailsSubStep
    {
        #region Properties
        /// <summary>
        /// Id of this export detail stat.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id of the parent.
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// Name of this step.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Precomputated duration for this step.
        /// </summary>
        [DataMember]
        public ulong Time { get; set; }

        /// <summary>
        /// When this step started to run.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// When this step completed.
        /// </summary>
        [DataMember]
        public DateTime? End { get; set; }

        /// <summary>
        /// Whether the step was successful.
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// Breakdown of this sub stat.
        /// </summary>
        [DataMember]
        public List<ExportDetailsSubStep> SubSteps { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ExportDetailsSubStep()
        {
            SubSteps = new List<ExportDetailsSubStep>();
        }
        #endregion // Constructor(s)
    } // ExportDetailsSubStep
}
