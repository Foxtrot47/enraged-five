﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class CheckpointAttemptStat
    {
        #region Properties
        /// <summary>
        /// DON'T USE, internal to stats server.
        /// </summary>
        public long MissionId { get; set; }

        /// <summary>
        /// DON'T USE, internal to stats server.
        /// </summary>
        public long CheckpointId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String CheckpointName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint CheckpointIndex { get; set; }

        /// <summary>
        /// Number of unique players that attempted this checkpoint.
        /// </summary>
        [DataMember]
        public int PlayersAttempted { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int NumberOfAttempts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public float ProjectedAttempts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Minimum time a user spent on this checkpoint.
        /// </summary>
        [DataMember]
        public long MinCheckpointTime { get; set; }

        /// <summary>
        /// Average time users spent on this checkpoint
        /// </summary>
        [DataMember]
        public double AvgCheckpointTime { get; set; }

        /// <summary>
        /// Maximum time a user spent on this checkpoint
        /// </summary>
        [DataMember]
        public long MaxCheckpointTime { get; set; }

        /// <summary>
        /// List of comments that were made for this checkpoint.
        /// </summary>
        [DataMember]
        public List<PlaythroughComment> Comments { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CheckpointAttemptStat()
        {
            Comments = new List<PlaythroughComment>();
        }
        #endregion // Constructor(s)
    } // CheckpointAttempt
}
