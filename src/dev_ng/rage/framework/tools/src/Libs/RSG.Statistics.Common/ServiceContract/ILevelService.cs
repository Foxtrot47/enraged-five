﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using System.ServiceModel;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface ILevelService : IGameAssetServiceBase<LevelDto, LevelDtos>
    {
    } // ILevelService
}
