﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Leaderboard
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class MiniGameMedalStat
    {
        #region Properties
        /// <summary>
        /// Name of the mini game.
        /// </summary>
        [DataMember]
        public String MiniGameName { get; set; }

        /// <summary>
        /// Highest medal achieved.
        /// </summary>
        [DataMember]
        public uint HighestMedal { get; set; }

        /// <summary>
        /// Number of bronze medals achieved.
        /// </summary>
        [DataMember]
        public uint BronzeMedals { get; set; }

        /// <summary>
        /// Number of silver medals achieved.
        /// </summary>
        [DataMember]
        public uint SilverMedals { get; set; }

        /// <summary>
        /// Number of gold medals achieved.
        /// </summary>
        [DataMember]
        public uint GoldMedals { get; set; }
        #endregion // Properties
    } // MiniGameMedalStat
}
