﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// Weapon Dto object
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("Character")]
    public class CharacterDto : GameAssetDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("Category")]
        public string Category { get; set; }
        public bool CategorySpecified { get { return !String.IsNullOrEmpty(Category); } }
        #endregion // Properties
    } // WeaponDto
}
