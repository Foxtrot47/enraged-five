﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("VehicleStat")]
    public class VehicleStatDto : GameAssetStatDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("VehicleId")]
        public long? VehicleId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("DoorCount")]
        public int? DoorCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("ElevatorCount")]
        public int? ElevatorCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("ExtraCount")]
        public int? ExtraCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("HandlebarCount")]
        public int? HandlebarCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("HasLod1")]
        public bool HasLod1
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("HasLod2")]
        public bool HasLod2
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("LightCount")]
        public int? LightCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("PropellerCount")]
        public int? PropellerCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("RotorCount")]
        public int? RotorCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("RudderCount")]
        public int? RudderCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("SeatCount")]
        public int? SeatCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("WheelCount")]
        public int? WheelCount
        {
            get;
            set;
        }
        #endregion // Properties
    } // VehicleStatDto
}
