﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class PlaythroughCheckpointAttempt
    {
        /// <summary>
        /// Internal stats server use.
        /// </summary>
        public long SessionId { get; set; }

        /// <summary>
        /// Id of the mission this attempt info is for.
        /// </summary>
        public long MissionId { get; set; }

        /// <summary>
        /// Name of the checkpoint.
        /// </summary>
        [DataMember]
        public String CheckpointName { get; set; }

        /// <summary>
        /// Index of the checkpoint.
        /// </summary>
        [DataMember]
        public uint CheckpointIndex { get; set; }

        /// <summary>
        /// When this attempt started.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// When this attempt 
        /// </summary>
        [DataMember]
        public DateTime End { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint TimeToComplete { get; set; }

        /// <summary>
        /// Comment the user added for this checkpoint.
        /// </summary>
        [DataMember]
        public String Comment { get; set; }
    } // PlaythroughCheckpointAttempt
}
