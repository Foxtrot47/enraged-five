﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service for retrieving information about archetypes.
    /// </summary>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IArchetypeService
    {
        #region Generic
        /// <summary>
        /// Returns a list of all archetypes.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "All")]
        ArchetypeDtos GetAll();
        #endregion // Generic

        #region Drawables
        /// <summary>
        /// Returns a list of all drawables.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Drawables")]
        ArchetypeDtos GetAllDrawables();

        /// <summary>
        /// Updates or creates a list of drawable archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Drawables")]
        void PutDrawables(ArchetypeDtos dtos);

        /// <summary>
        /// Updates or creates a drawable archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Drawables/{identifier}")]
        DrawableArchetypeDto PutDrawable(string identifier, DrawableArchetypeDto dto);
        #endregion // Drawables

        #region Fragments
        /// <summary>
        /// Returns a list of all fragments.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Fragments")]
        ArchetypeDtos GetAllFragments();

        /// <summary>
        /// Updates or creates a list of fragment archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Fragments")]
        void PutFragments(ArchetypeDtos dtos);

        /// <summary>
        /// Updates or creates a fragment archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Fragments/{identifier}")]
        FragmentArchetypeDto PutFragment(string identifier, FragmentArchetypeDto dto);
        #endregion // Fragments
        
        #region Stated Anims
        /// <summary>
        /// Returns a list of all stated anims.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "StatedAnims")]
        ArchetypeDtos GetAllStatedAnims();

        /// <summary>
        /// Updates or creates a list of stated anim archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "StatedAnims")]
        void PutStatedAnims(ArchetypeDtos dtos);

        /// <summary>
        /// Updates or creates a stated anim archetype based on its identifier
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "StatedAnims/{identifier}")]
        StatedAnimArchetypeDto PutStatedAnim(string identifier, StatedAnimArchetypeDto dto);
        #endregion // Stated Anims

        #region Interiors
        /// <summary>
        /// Returns a list of all interiors.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Interiors")]
        ArchetypeDtos GetAllInteriors();

        /// <summary>
        /// Updates or creates a list of interior archetypes.
        /// </summary>
        /// <param name="dtos"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Interiors")]
        void PutInteriors(ArchetypeDtos dtos);

        /// <summary>
        /// Updates or creates an interior archetype based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Interiors/{identifier}")]
        InteriorArchetypeDto PutInterior(string identifier, InteriorArchetypeDto dto);
        #endregion // Interiors
    } // IArchetypeService
}
