﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StatedAnimArchetypeStatDto : MapArchetypeStatDto
    {
        /// <summary>
        /// List of archetypes that make up this stated anim.
        /// </summary>
        [DataMember]
        [StreamableStatAttribute(StreamableStatedAnimArchetypeStat.ComponentArchetypesString)]
        public List<BasicArchetypeStatDto> ComponentArchetypeStats { get; set; }
    } // StatedAnimArchetypeStatDto
}
