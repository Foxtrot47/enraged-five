﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Build")]
    [DataContract(Name = "Build")]
    public class BuildDto : GameAssetDtoBase, IComparable<BuildDto>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint? GameVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasAssetStats { get; set; }        
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasProcessedStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasAutomatedEverythingStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasAutomatedMapOnlyStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasAutomatedNighttimeMapOnlyStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasMagDemoStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasMemShortfallStats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasShapeTestStats { get; set; }
        #endregion // Properties

        #region IComparable<Build> Interface
        /// <summary>
        /// Compare this Vehicle to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(BuildDto other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Identifier.CompareTo(other.Identifier));
        }
        #endregion // IComparable<Vehicle> Interface
    } // BuildDto
}
