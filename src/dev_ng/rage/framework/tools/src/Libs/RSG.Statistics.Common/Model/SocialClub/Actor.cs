﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.SocialClub
{
    /// <summary>
    /// Base class for social club actors.
    /// </summary>
    [DataContract]
    public abstract class Actor
    {
        /// <summary>
        /// Database id for this group.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Name of the actor.
        /// </summary>
        [DataMember]
        public String Name { get; set; }
    } // Actor
}
