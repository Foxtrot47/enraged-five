﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapSectionStatBundleDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Whether the section exports archetypes.
        /// </summary>
        [DataMember]
        public bool ExportArchetypes { get; set; }

        /// <summary>
        /// Whether the section exports entities.
        /// </summary>
        [DataMember]
        public bool ExportEntities { get; set; }

        /// <summary>
        /// Map sections top level collision poly count.
        /// </summary>
        [DataMember]
        public uint CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this map section stat.
        /// </summary>
        [DataMember]
        public List<CollisionPolyStatDto> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// Last person to export this section.
        /// </summary>
        [DataMember]
        public string LastExportUser { get; set; }

        /// <summary>
        /// Last time this section was exported.
        /// </summary>
        [DataMember]
        public DateTime LastExportTime { get; set; }

        /// <summary>
        /// DCC source filename (excluding extension).
        /// </summary>
        [DataMember]
        public string DCCSourceFilename { get; set; }

        /// <summary>
        /// Name of the exported zip file.
        /// </summary>
        [DataMember]
        public string ExportZipFilepath { get; set; }

        /// <summary>
        /// Platform stats for this map section.
        /// </summary>
        [DataMember]
        public List<MapSectionPlatformStatDto> PlatformStats { get; set; }

        /// <summary>
        /// Map section attributes.
        /// </summary>
        [DataMember]
        public List<MapSectionAttributeStatDto> Attributes { get; set; }

        /// <summary>
        /// Statistics for this section which have been aggregated from the section's archetypes/entities.
        /// </summary>
        [DataMember]
        public List<MapSectionAggregateStatDto> AggregatedStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapSectionStatBundleDto()
            : base()
        {
            CollisionTypePolygonCounts = new List<CollisionPolyStatDto>();
            PlatformStats = new List<MapSectionPlatformStatDto>();
            Attributes = new List<MapSectionAttributeStatDto>();
        }
        #endregion // Constructor(s)
    } // MapSectionStatDto
}
