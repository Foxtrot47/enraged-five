﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enum for race sub types.
    /// The values should match those defined in //depot/gta5/script/dev/multiplayer/globals/MP_globals_FM.sch.
    /// </summary>
    public enum RaceSubType
    {
        [FriendlyName("Car")]
        Car = 0,

        [FriendlyName("Car P2P")]
        CarPointToPoint = 1,

        [FriendlyName("Boat")]
        Boat = 2,

        [FriendlyName("Boat P2P")]
        BoatPointToPoint = 3,

        [FriendlyName("Air")]
        Air = 4,

        [FriendlyName("Air P2P")]
        AirPointToPoint = 5,

        [FriendlyName("Triathlon")]
        Triathlon = 6,

        [FriendlyName("Triathlon P2P")]
        TriathlonPointToPoint = 7,

        [FriendlyName("Base Jump")]
        BaseJump = 8,

        [FriendlyName("Base Jump P2P")]
        BaseJumpPointToPoint = 9,

        [FriendlyName("On Foot")]
        OnFoot = 10,

        [FriendlyName("On Foot P2P")]
        OnFootPointToPoint = 11,

        [FriendlyName("Bike and Cycle")]
        BikeAndCycle = 12,

        [FriendlyName("Bike and Cycle P2P")]
        BikeAndCyclePointToPoint = 13,

        [FriendlyName("Unknown")]
        Unknown                 // Unknown race sub-type.
    } // RaceSubType
}
