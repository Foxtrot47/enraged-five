﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.SocialClub
{
    /// <summary>
    /// Actor group hierarchy.
    /// </summary>
    [DataContract]
    public class ActorGroupHierarchy
    {
        #region Properties
        /// <summary>
        /// Database id for this group.
        /// </summary>
        [DataMember]
        public long Id { get; set; }

        /// <summary>
        /// Name of this group.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// List of child groups.
        /// </summary>
        [DataMember]
        public List<ActorGroupHierarchy> ChildGroups { get; set; }

        /// <summary>
        /// Id of the parent for this group.
        /// For stats server internal use only.
        /// </summary>
        public long? ParentId { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ActorGroupHierarchy()
        {
            ChildGroups = new List<ActorGroupHierarchy>();
        }
        #endregion // Constructor(s)
    } // ActorGroupTreeNode
}
