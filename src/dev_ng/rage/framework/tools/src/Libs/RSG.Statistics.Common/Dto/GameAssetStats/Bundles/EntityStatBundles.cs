﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class EntityStatBundles : IDtos<EntityStatBundle>
    {
        [DataMember]
        public List<EntityStatBundle> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EntityStatBundles()
        {
            Items = new List<EntityStatBundle>();
        }
    } // EntityStatBundles
}
