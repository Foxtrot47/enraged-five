﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using RSG.Statistics.Common.Dto.GameAssets;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Dto.GameAssetStats;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IMapSectionService : IGameAssetServiceBase<MapSectionDto, MapSectionDtos>
    {
    } // IMapSectionService
}
