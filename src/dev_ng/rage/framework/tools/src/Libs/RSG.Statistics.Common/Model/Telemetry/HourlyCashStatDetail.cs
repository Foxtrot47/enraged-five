﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class HourlyCashStatDetail
    {
        /// <summary>
        /// Amount of gameplay time that has passed (in milliseconds).
        /// </summary>
        [DataMember]
        public uint PlayingTime { get; set; }

        /// <summary>
        /// Real life date time that this took place.
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Category that this falls under.
        /// </summary>
        [DataMember]
        public String Category { get; set; }

        /// <summary>
        /// Action that this falls under.
        /// </summary>
        [DataMember]
        public String Action { get; set; }

        /// <summary>
        /// Amount (XP/Cash).
        /// </summary>
        [DataMember]
        public uint Amount { get; set; }
    } // HourlyCashStatDetail
}
