﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.UGC
{
    /// <summary>
    /// Basic UGC mission data.
    /// </summary>
    [DataContract]
    [ReportType]
    [Serializable]
    public class BasicMissionMetadata
    {
        /// <summary>
        /// Public facing content id.
        /// </summary>
        [DataMember]
        public String PublicContentId { get; set; }

        /// <summary>
        /// Type of match this piece of content is for.
        /// </summary>
        [DataMember]
        public MatchType Type { get; set; }

        /// <summary>
        /// Match sub-type.
        /// </summary>
        [DataMember]
        public int SubType { get; set; }

        /// <summary>
        /// Friendly name given to this piece of content.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Description associated with this piece of content.
        /// </summary>
        [DataMember]
        public String Description { get; set; }

        /// <summary>
        /// Name of the user that created this piece of content.
        /// </summary>
        [DataMember]
        public String CreatorName { get; set; }

        /// <summary>
        /// Platform that the creator is from.
        /// </summary>
        [DataMember]
        public ROSPlatform? CreatorPlatform { get; set; }
    } // BasicMissionMetadata
}
