﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("ExportUsers")]
    public class ExportUserDtos : IDtos<string>
    {
        [XmlElement("ExportUser")]
        public List<string> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ExportUserDtos()
        {
            Items = new List<string>();
        }
    } // ExportUserDtos
}
