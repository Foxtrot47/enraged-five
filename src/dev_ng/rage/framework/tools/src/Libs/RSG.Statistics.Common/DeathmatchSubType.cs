﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Enum for deathmatch sub types.
    /// The values should match those defined in //depot/gta5/script/dev/multiplayer/globals/MP_globals_FM.sch.
    /// </summary>
    public enum DeathmatchSubType
    {
        [FriendlyName("Free For All")]
        Standard = 0,

        [FriendlyName("Team")]
        Team = 1,

        [FriendlyName("Vehicle")]
        Vehicle = 2,

        [FriendlyName("Unknown")]
        Unknown                 // Unknown deathmatch sub-type.
    } // DeathmatchSubType
}
