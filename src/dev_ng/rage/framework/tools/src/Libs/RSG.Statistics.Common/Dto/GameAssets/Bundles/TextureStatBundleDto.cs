﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureStatBundleDto : DtoBase
    {
        [XmlIgnore]
        public TextureBundleDto Texture
        {
            get;
            set;
        }

        [XmlElement("TextureHash")]
        public long TextureHash
        {
            get
            {
                if (m_textureHash == null)
                {
                    m_textureHash = Texture.Hash;
                }
                return (long)m_textureHash;
            }
            set
            {
                m_textureHash = value;
            }
        }
        private long? m_textureHash;
    } // TextureStatBundleDto
}
