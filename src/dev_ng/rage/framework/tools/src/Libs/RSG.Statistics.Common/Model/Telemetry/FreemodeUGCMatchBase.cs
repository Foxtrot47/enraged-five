﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Base class for UGC based match data.
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class FreemodeUGCMatchBase : FreemodeMatchStatBase
    {
        #region Properties
        /// <summary>
        /// UGC identifier associated with this match.
        /// </summary>
        [DataMember]
        public String UGCIdentifier { get; set; }

        /// <summary>
        /// Description for the variant.
        /// </summary>
        [DataMember]
        public String Description { get; set; }

        /// <summary>
        /// Platform that the creator is on.
        /// </summary>
        [DataMember]
        public ROSPlatform? CreatorPlatform { get; set; }

        /// <summary>
        /// Date this piece of UGC content was created on.
        /// </summary>
        [DataMember]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Average rating for the match.
        /// </summary>
        [DataMember]
        public float AverageRating { get; set; }

        /// <summary>
        /// Category that this mission falls under.
        /// </summary>
        [DataMember]
        public FreemodeMissionCategory Category { get; set; }

        /// <summary>
        /// Flag indicating whether the mission has been published.
        /// </summary>
        [DataMember]
        public bool IsPublished { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? PublishedSurrogate
        {
            set
            {
                IsPublished = (value != null);
            }
        }
        #endregion // Properties
    } // FreemodeUGCMatchBase
}
