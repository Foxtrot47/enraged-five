﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry.PerUser;

namespace RSG.Statistics.Common.Model.Leaderboard
{
    /// <summary>
    /// Concrete version for wcf serialisation purposes.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserMiniGameMedalStats : PerUserStat<List<MiniGameMedalStat>>
    {
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserMiniGameMedalStats()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserMiniGameMedalStats(String gamerHandle, ROSPlatform platform)
            : base(gamerHandle, platform, new List<MiniGameMedalStat>())
        {
        }
        #endregion // Constructor(s)
    } // PerUserMiniGameMedalStats
}
