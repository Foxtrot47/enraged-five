﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information about a single variant.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FreemodeMatchVariantSummary : FreemodeUGCMatchBase
    {
    } // FreemodeMatchVariantSummary
}
