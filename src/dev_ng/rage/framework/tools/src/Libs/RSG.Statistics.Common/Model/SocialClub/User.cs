﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.SocialClub
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class User : Actor
    {
        /// <summary>
        /// Name of the country this user specified on account creation.
        /// </summary>
        [DataMember]
        public String CountryCode { get; set; }

        /// <summary>
        /// User's date of birth.
        /// </summary>
        [DataMember]
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// SC rockstar id.
        /// </summary>
        [DataMember]
        public long? RockstarId { get; set; }
    } // User
}
