﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Debug
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class CutsceneLightStat
    {
        #region Properties
        /// <summary>
        /// Name of the cutscene.
        /// </summary>
        [DataMember]
        public String CutsceneName { get; set; }

        /// <summary>
        /// Number of telemetry packets this data is gathered from.
        /// </summary>
        [DataMember]
        public int SampleSize { get; set; }

        /// <summary>
        /// Directional readings.
        /// </summary>
        [DataMember]
        public CutsceneLightReading Directional { get; set; }

        /// <summary>
        /// Scene readings.
        /// </summary>
        [DataMember]
        public CutsceneLightReading Scene { get; set; }

        /// <summary>
        /// LOD readings.
        /// </summary>
        [DataMember]
        public CutsceneLightReading LOD { get; set; }

        /// <summary>
        /// Total readings.
        /// </summary>
        [DataMember]
        public CutsceneLightReading Total { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutsceneLightStat()
        {
            Directional = new CutsceneLightReading();
            Scene = new CutsceneLightReading();
            LOD = new CutsceneLightReading();
            Total = new CutsceneLightReading();
        }
        #endregion // Constructor(s)
    } // CutsceneLightInfo

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class CutsceneLightReading
    {
        /// <summary>
        /// Minimum reading.
        /// </summary>
        [DataMember]
        public float Min { get; set; }

        /// <summary>
        /// Maximum reading.
        /// </summary>
        [DataMember]
        public float Max { get; set; }

        /// <summary>
        /// Average reading.
        /// </summary>
        [DataMember]
        public float Average { get; set; }

        /// <summary>
        /// Standard deviation.
        /// </summary>
        [DataMember]
        public float StandardDeviation { get; set; }
    } // CutsceneLightReading
}
