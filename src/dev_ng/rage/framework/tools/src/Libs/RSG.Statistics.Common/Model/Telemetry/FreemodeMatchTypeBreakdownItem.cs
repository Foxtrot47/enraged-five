﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Extensions;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// An item that makes up the breakdown.
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class FreemodeMatchTypeBreakdownItemBase
    {
        /// <summary>
        /// Name for this item.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Number of unique gamers that players have played matches in this sub-group.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        #region Surrogate Properties
        /// <summary>
        /// Surrogate property for setting the race sub type.
        /// </summary>
        public int RaceSubTypeSurrogate
        {
            set
            {
                RaceSubType type;
                if (Enum.IsDefined(typeof(RaceSubType), value))
                {
                    type = (RaceSubType)value;
                }
                else
                {
                    type = RaceSubType.Unknown;
                }
                Name = type.GetFriendlyNameValue();
            }
        }

        /// <summary>
        /// Surrogate property for setting the mission sub type.
        /// </summary>
        public int MissionSubTypeSurrogate
        {
            set
            {
                MissionSubType type;
                if (Enum.IsDefined(typeof(MissionSubType), value))
                {
                    type = (MissionSubType)value;
                }
                else
                {
                    type = MissionSubType.Unknown;
                }
                Name = type.GetFriendlyNameValue();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int RaceTypeSurrogate
        {
            set
            {
                RaceType type;
                if (Enum.IsDefined(typeof(RaceType), value))
                {
                    type = (RaceType)value;
                }
                else
                {
                    type = RaceType.Unknown;
                }
                m_raceType = type;
                Name = (m_teamDM ? "Rally " : "") + m_raceType.ToString();
            }
        }
        private RaceType m_raceType;

        /// <summary>
        /// 
        /// </summary>
        public bool TeamDeathmatchSurrogate
        {
            set
            {
                m_teamDM = value;
                Name = (m_teamDM ? "Rally " : "") + m_raceType.ToString();
            }
        }
        private bool m_teamDM;

        /// <summary>
        /// 
        /// </summary>
        public int DeathmatchSubTypeSurrogate
        {
            set
            {
                DeathmatchSubType type;
                if (Enum.IsDefined(typeof(DeathmatchSubType), value))
                {
                    type = (DeathmatchSubType)value;
                }
                else
                {
                    type = DeathmatchSubType.Unknown;
                }
                Name = type.GetFriendlyNameValue();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int DeathmatchVehicleTypeSurrogate
        {
            set
            {
                Name = DeathmatchVehicleTypeUtils.ConvertToDeathmatchVehicleType(value).GetFriendlyNameValue();
            }
        }
        #endregion // Surrogate Properties
    } // FreemodeMatchTypeBreakdownItemBase

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FreemodeMatchTypeBreakdownItem : FreemodeMatchTypeBreakdownItemBase
    {
        /// <summary>
        /// Total amount of time spent playing this item (in seconds).
        /// </summary>
        [DataMember]
        public ulong TimeSpentPlaying { get; set; }

        /// <summary>
        /// Total number of times that players have played matches in this sub-group.
        /// </summary>
        [DataMember]
        public ulong TimesPlayed { get; set; }

        /// <summary>
        /// Total number of gamers that took part in this variant.
        /// </summary>
        [DataMember]
        public ulong TotalGamers { get; set; }
    } // FreemodeMatchTypeBreakdownItem

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FreemodeMatchTypeWaveBreakdownItem : FreemodeMatchTypeBreakdownItemBase
    {
        /// <summary>
        /// Number of times players reached this particular wave.
        /// </summary>
        [DataMember]
        public ulong TimesReached { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SurvivalWaveSurrogate
        {
            set
            {
                if (value == 11)
                {
                    Name = "Completed";
                }
                else
                {
                    Name = String.Format("Wave {0}", value);
                }
            }
        }
    } // FreemodeMatchTypeWaveBreakdownItem
}
