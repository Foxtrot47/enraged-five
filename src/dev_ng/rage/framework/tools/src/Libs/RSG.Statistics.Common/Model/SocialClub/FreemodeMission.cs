﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.SocialClub
{
    /// <summary>
    /// Information relating to a single freemode mission as created by rockstar/an user.
    /// </summary>
    [DataContract]
    public class FreemodeMission
    {
        /// <summary>
        /// User generated content identifier.
        /// </summary>
        [DataMember]
        public String UGCIdentifier { get; set; }

        /// <summary>
        /// Name that the user gave the misison.
        /// </summary>
        [DataMember]
        public String Name { get; set; }
        
        /// <summary>
        /// Description associated with the mission.
        /// </summary>
        [DataMember]
        public String Description { get; set; }

        /// <summary>
        /// User who created the mission.
        /// </summary>
        [DataMember]
        public String Creator { get; set; }
    } // FreemodeMission
}
