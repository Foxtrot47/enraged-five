﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// Fps performance related information.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FpsPerformanceStat : BasicFpsPerformanceStat
    {
        /// <summary>
        /// Minimum update time.
        /// </summary>
        [DataMember]
        public double MinUpdateTime { get; set; }

        /// <summary>
        /// Average update time.
        /// </summary>
        [DataMember]
        public double AvgUpdateTime { get; set; }

        /// <summary>
        /// Maximum update time.
        /// </summary>
        [DataMember]
        public double MaxUpdateTime { get; set; }

        /// <summary>
        /// Minimum draw time.
        /// </summary>
        [DataMember]
        public double MinDrawTime { get; set; }

        /// <summary>
        /// Average draw time.
        /// </summary>
        [DataMember]
        public double AvgDrawTime { get; set; }

        /// <summary>
        /// Maximum draw time.
        /// </summary>
        [DataMember]
        public double MaxDrawTime { get; set; }

        /// <summary>
        /// Minimum gpu time.
        /// </summary>
        [DataMember]
        public double MinGpuTime { get; set; }

        /// <summary>
        /// Average gpu time.
        /// </summary>
        [DataMember]
        public double AvgGpuTime { get; set; }

        /// <summary>
        /// Maximum gpu time.
        /// </summary>
        [DataMember]
        public double MaxGpuTime { get; set; }

        /// <summary>
        /// Minimum index count.
        /// </summary>
        [DataMember]
        public double MinIndexCount { get; set; }

        /// <summary>
        /// Average index count.
        /// </summary>
        [DataMember]
        public double AvgIndexCount { get; set; }

        /// <summary>
        /// Maximum index count.
        /// </summary>
        [DataMember]
        public double MaxIndexCount { get; set; }

        /// <summary>
        /// Minimum graphics buffer index count.
        /// </summary>
        [DataMember]
        public double MinGBufferIndexCount { get; set; }

        /// <summary>
        /// Average graphics buffer index count.
        /// </summary>
        [DataMember]
        public double AvgGBufferIndexCount { get; set; }

        /// <summary>
        /// Maximum graphics buffer index count.
        /// </summary>
        [DataMember]
        public double MaxGBufferIndexCount { get; set; }

        /// <summary>
        /// Minimum object count.
        /// </summary>
        [DataMember]
        public double MinObjectCount { get; set; }

        /// <summary>
        /// Average object count.
        /// </summary>
        [DataMember]
        public double AvgObjectCount { get; set; }

        /// <summary>
        /// Maximum object count.
        /// </summary>
        [DataMember]
        public double MaxObjectCount { get; set; }

        /// <summary>
        /// Minimum draw call count.
        /// </summary>
        [DataMember]
        public double MinDrawcallCount { get; set; }

        /// <summary>
        /// Average draw call count.
        /// </summary>
        [DataMember]
        public double AvgDrawcallCount { get; set; }

        /// <summary>
        /// Maximum draw call count.
        /// </summary>
        [DataMember]
        public double MaxDrawcallCount { get; set; }
    } // FpsPerformance
}
