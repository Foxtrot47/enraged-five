﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class FriendlyGameAsset : GameAsset
    {
        #region Properties
        /// <summary>
        /// Friendly name of the asset.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FriendlyGameAsset()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public FriendlyGameAsset(String gameName, String friendlyName)
            : base(gameName)
        {
            FriendlyName = friendlyName;
        }
        #endregion // Constructor(s)
    } // FriendlyGameAsset
}
