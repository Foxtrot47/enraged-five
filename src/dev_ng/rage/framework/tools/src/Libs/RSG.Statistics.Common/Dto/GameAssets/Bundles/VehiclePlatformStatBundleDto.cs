﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("VehiclePlatformStatBundle")]
    public class VehiclePlatformStatBundleDto : GameAssetPlatformStatBundleDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint HiPhysicalSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint HiVirtualSize { get; set; }
    } // VehiclePlatformStatBundleDto
}
