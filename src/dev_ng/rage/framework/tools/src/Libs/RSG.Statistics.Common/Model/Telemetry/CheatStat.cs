﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class CheatStat
    {
        #region Properties
        /// <summary>
        /// Name of the cheat that was used.
        /// </summary>
        [DataMember]
        public String CheatName { get; set; }

        /// <summary>
        /// Number of times the cheat was used.
        /// </summary>
        [DataMember]
        public uint TimesUsed { get; set; }

        /// <summary>
        /// Number of unique gamers that used this cheat.
        /// </summary>
        [DataMember]
        public uint UniqueGamers { get; set; }
        #endregion // Properties
    } // CheatStat
}
