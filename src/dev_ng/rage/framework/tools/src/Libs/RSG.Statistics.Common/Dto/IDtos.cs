﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Dto
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDtos<T> : IDto
        //where T : IDto
    {
        List<T> Items { get; set; }
    } // IDtos<T>
}
