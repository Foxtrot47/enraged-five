﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("Entities")]
    public class EntityDtos : IDtos<EntityDto>
    {
        [XmlElement("Entity")]
        public List<EntityDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public EntityDtos()
        {
            Items = new List<EntityDto>();
        }
    } // EntityDtos
}
