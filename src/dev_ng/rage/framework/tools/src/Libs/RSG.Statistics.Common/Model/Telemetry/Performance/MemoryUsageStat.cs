﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MemoryUsageStat
    {
        [DataMember]
        public String MissionName { get; set; }
        [DataMember]
        public String ResourceName { get; set; }
        [DataMember]
        public int NumSamples { get; set; }
        [DataMember]
        public long MinUsed { get; set; }
        [DataMember]
        public float AvgUsed { get; set; }
        [DataMember]
        public long MaxUsed { get; set; }
        [DataMember]
        public long MinFree { get; set; }
        [DataMember]
        public float AvgFree { get; set; }
        [DataMember]
        public long MaxFree { get; set; }
        [DataMember]
        public long MinPeak { get; set; }
        [DataMember]
        public float AvgPeak { get; set; }
        [DataMember]
        public long MaxPeak { get; set; }
    } // MemoryUsageStat
}
