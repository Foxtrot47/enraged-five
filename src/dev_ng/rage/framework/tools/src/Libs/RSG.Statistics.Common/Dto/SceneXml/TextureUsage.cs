﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.SceneXml
{
    /// <summary>
    /// Returns a single texture/scene usage pair.
    /// </summary>
    [DataContract]
    public class TextureUsage
    {
        /// <summary>
        /// Name of the texture.
        /// </summary>
        [DataMember]
        public String TextureName { get; set; }

        /// <summary>
        /// Name of the scene the texture is used in.
        /// </summary>
        [DataMember]
        public String SceneName { get; set; }
    }
}
