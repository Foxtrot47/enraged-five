﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// User summary information.
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserSummary
    {
        /// <summary>
        /// Name of the user these stats are for.
        /// </summary>
        [DataMember]
        public String Username { get; set; }

        /// <summary>
        /// Total number of exports that were performed.
        /// </summary>
        [DataMember]
        public uint TotalCount { get; set; }

        /// <summary>
        /// Number of successful exports.
        /// </summary>
        [DataMember]
        public uint SuccessfulCount { get; set; }

        /// <summary>
        /// Convenience property for getting the success ratio.
        /// </summary>
        public double SuccessRatio
        {
            get
            {
                if (TotalCount != 0)
                {
                    return SuccessfulCount / TotalCount;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Total time spent exporting in seconds.
        /// </summary>
        [DataMember]
        public ulong TotalTime { get; set; }

        /// <summary>
        /// Longest time it took for an export.
        /// </summary>
        [DataMember]
        public ulong MaxTime { get; set; }

        /// <summary>
        /// Convenience method for getting the average time.
        /// </summary>
        public double AverageTime
        {
            get
            {
                if (TotalCount != 0)
                {
                    return TotalTime / TotalCount;
                }
                else
                {
                    return 0.0;
                }
            }
        }
    } // UserSummary
}
