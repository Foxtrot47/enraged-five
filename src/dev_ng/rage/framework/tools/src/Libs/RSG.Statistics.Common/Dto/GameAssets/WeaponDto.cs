﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// Weapon Dto object
    /// </summary>
    [Serializable]
    [XmlRoot("Weapon")]
    [DataContract]
    public class WeaponDto : GameAssetDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Category { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String ModelName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String StatName { get; set; }
        #endregion // Properties
    } // WeaponDto
}
