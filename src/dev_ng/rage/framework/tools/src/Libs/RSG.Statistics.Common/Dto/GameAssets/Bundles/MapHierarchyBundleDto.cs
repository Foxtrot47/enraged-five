﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using RSG.Base.Math;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [XmlRoot("MapHierarchyBundle")]
    public class MapHierarchyBundleDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlArrayItem(typeof(BasicMapAreaStatDto))]
        [XmlArrayItem(typeof(BasicMapSectionStatDto))]
        public List<BasicMapNodeStatBaseDto> ChildNodes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MapHierarchyBundleDto()
        {
            ChildNodes = new List<BasicMapNodeStatBaseDto>();
        }
    } // MapHierarchyBundleDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(BasicMapAreaStatDto))]
    [KnownType(typeof(BasicMapSectionStatDto))]
    public abstract class BasicMapNodeStatBaseDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore()]
        public String IdentifierString
        {
            get { return Identifier.ToString(); }
            set { Identifier = UInt32.Parse(value); }
        }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long Id { get; set; }

        /// <summary>
        /// DO NOT USE. For stats server internal use only.
        /// </summary>
        [XmlIgnore()]
        public long? ParentId { get; set; }
    } // BasicMapNodeStatBaseDto

    /// <summary>
    /// 
    /// </summary>
    [XmlRoot("MapAreaStatBundle")]
    [DataContract]
    public class BasicMapAreaStatDto : BasicMapNodeStatBaseDto
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlArrayItem(typeof(BasicMapAreaStatDto))]
        [XmlArrayItem(typeof(BasicMapSectionStatDto))]
        public List<BasicMapNodeStatBaseDto> ChildNodes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public BasicMapAreaStatDto()
        {
            ChildNodes = new List<BasicMapNodeStatBaseDto>();
        }
    } // MapAreaStatBundleDto

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("MapSectionBundle")]
    [DataContract]
    public class BasicMapSectionStatDto : BasicMapNodeStatBaseDto
    {
        /// <summary>
        /// Type of section this one is.
        /// </summary>
        [DataMember]
        public String Type { get; set; }

        /// <summary>
        /// Outline of the section.
        /// </summary>
        [DataMember]
        public List<Vector2f> VectorMapPoints { get; set; }
    } // MapSectionBundleDto
}
