﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DrawListCount
    {
        /// <summary>
        /// Name of the draw list.
        /// </summary>
        [DataMember]
        public String DrawListName { get; set; }

        /// <summary>
        /// Number of items in the draw list.
        /// </summary>
        [DataMember]
        public uint EntityCount { get; set; }
    } // DrawListCount
}
