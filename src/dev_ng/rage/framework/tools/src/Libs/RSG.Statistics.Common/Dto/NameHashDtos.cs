﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "NameHashes")]
    public class NameHashDtos : IDtos<NameHashDto>
    {
        [DataMember(Name = "Items")]
        public List<NameHashDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public NameHashDtos()
        {
            Items = new List<NameHashDto>();
        }
    } // NameHashDtos
}
