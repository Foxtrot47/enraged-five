﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.SceneXml
{
    /// <summary>
    /// Data transfer object for transferring scene xml texture data between server and client.
    /// </summary>
    [DataContract]
    public class Texture
    {
        /// <summary>
        /// Path to the texture on disk.
        /// </summary>
        [DataMember]
        public String Filename { get; set; }

        /// <summary>
        /// Full path to the alpha filename for this texture (if it exists).
        /// </summary>
        [DataMember]
        public String AlphaFilename { get; set; }

        /// <summary>
        /// Type of texture this is.
        /// </summary>
        [DataMember]
        public RSG.SceneXml.Material.TextureTypes Type { get; set; }
    } // Texture
}
