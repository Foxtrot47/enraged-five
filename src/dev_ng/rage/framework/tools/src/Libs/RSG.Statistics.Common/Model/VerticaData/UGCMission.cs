﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.VerticaData
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class UGCMission
    {
        /// <summary>
        /// Unique identifier for this piece of UGC content.
        /// </summary>
        [DataMember]
        public String Identifier { get; set; }

        /// <summary>
        /// Name for this piece of UGC content.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Creator for this piece of UGC content.
        /// </summary>
        [DataMember]
        public String Creator { get; set; }

        /// <summary>
        /// Platform that the creator is on.
        /// </summary>
        [DataMember]
        public ROSPlatform? CreatorPlatform { get; set; }

        /// <summary>
        /// When this piece of content was created.
        /// </summary>
        [DataMember]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Content version number.
        /// </summary>
        [DataMember]
        public uint Version { get; set; }
    } // UGCMission
}
