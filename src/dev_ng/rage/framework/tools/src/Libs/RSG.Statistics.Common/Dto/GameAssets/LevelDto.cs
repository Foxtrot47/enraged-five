﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using RSG.Base.Math;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("Level")]
    public class LevelDto : GameAssetDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("Description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlElement("LevelIdx")]
        public uint? LevelIdx { get; set; }
        #endregion // Properties
    } // LevelDto
}
