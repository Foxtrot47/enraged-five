﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class UserSessionBreakdown
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint TimeSpentPlaying { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String BuildIdentifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public RSG.Platform.Platform Platform { get; set; }

        /// <summary>
        /// HACK due to NHibernate issue with criterias that have aliases and restrictions with the same name.
        /// </summary>
        public RSG.Platform.Platform PlatformHACK
        {
            get { return Platform; }
            set { Platform = value; }
        }

        /// <summary>
        /// List of mission attempts the user did.
        /// </summary>
        [DataMember]
        public List<PlaythroughMissionAttempt> MissionsAttempted { get; set; }

        /// <summary>
        /// List of generic comments the user may have added to this playthrough session.
        /// </summary>
        [DataMember]
        public List<PlaythroughComment> Comments { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public UserSessionBreakdown()
        {
            MissionsAttempted = new List<PlaythroughMissionAttempt>();
            Comments = new List<PlaythroughComment>();
        }
        #endregion // Constructor(s)
    } // UserSessionBreakdown
}
