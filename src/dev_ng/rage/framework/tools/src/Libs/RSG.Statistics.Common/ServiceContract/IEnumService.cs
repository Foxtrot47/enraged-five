﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Animation;
using RSG.Model.Common.Mission;
using RSG.Model.Common.Weapon;
using RSG.Model.Statistics.Platform;
using RSG.Platform;
using RSG.ROS;
using RSG.Statistics.Common.Dto.Enums;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IEnumService
    {
        /// <summary>
        /// Returns a list of all build configs.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "BuildConfigs")]
        IList<EnumDto<BuildConfig>> GetBuildConfigs();

        /// <summary>
        /// Returns a list of all clip dictionary categories.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "ClipDictionaryCategories")]
        IList<EnumDto<ClipDictionaryCategory>> GetClipDictionaryCategories();

        /// <summary>
        /// Returns a list of all date time group modes.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "DateTimeGroupModes")]
        IList<EnumDto<DateTimeGroupMode>> GetDateTimeGroupModes();

        /// <summary>
        /// Returns a list of all deathmatch sub types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "DeathmatchSubTypes")]
        IList<EnumDto<DeathmatchSubType>> GetDeathmatchSubTypes();

        /// <summary>
        /// Returns a list of all file types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "FileTypes")]
        IList<EnumDto<FileType>> GetFileTypes();

        /// <summary>
        /// Returns a list of all freemode mission categories.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "FreemodeMissionCategories")]
        IList<EnumDto<FreemodeMissionCategory>> GetFreemodeMissionCategories();

        /// <summary>
        /// Returns a list of all game types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "GameTypes")]
        IList<EnumDto<GameType>> GetGameTypes();

        /// <summary>
        /// Returns a list of all match types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "MatchTypes")]
        IList<EnumDto<MatchType>> GetMatchTypes();

        /// <summary>
        /// Returns a list of all mission attempt results.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "MissionAttemptResults")]
        IList<EnumDto<MissionAttemptResult>> GetMissionAttemptResults();

        /// <summary>
        /// Returns a list of all mission categories.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "MissionCategories")]
        IList<EnumDto<MissionCategory>> GetMissionCategories();

        /// <summary>
        /// Returns a list of all mission sub types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "MissionSubTypes")]
        IList<EnumDto<MissionSubType>> GetMissionSubTypes();

        /// <summary>
        /// Returns a list of all platforms.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Platforms")]
        IList<EnumDto<RSG.Platform.Platform>> GetPlatforms();

        /// <summary>
        /// Returns a list of all race sub types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "RaceSubTypes")]
        IList<EnumDto<RaceSubType>> GetRaceSubTypes();

        /// <summary>
        /// Returns a list of all resource bucket types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "ResourceBucketTypes")]
        IList<EnumDto<ResourceBucketType>> GetResourceBucketTypes();

        /// <summary>
        /// Returns a list of all ROS platforms.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "ROSPlatforms")]
        IList<EnumDto<ROSPlatform>> GetROSPlatforms();

        /// <summary>
        /// Returns a list of all shop types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "ShopTypes")]
        IList<EnumDto<ShopType>> GetShopTypes();

        /// <summary>
        /// Returns a list of all spend categories.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "SpendCategories")]
        IList<EnumDto<SpendCategory>> GetSpendCategories();

        /// <summary>
        /// Returns a list of all job results.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "JobResults")]
        IList<EnumDto<JobResult>> GetJobResults();

        /// <summary>
        /// Returns a list of all times of the day.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "TimeOfDays")]
        IList<EnumDto<TimeOfDay>> GetTimeOfDays();

        /// <summary>
        /// Returns a list of all vehicle categories.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "VehicleCategories")]
        IList<EnumDto<VehicleCategory>> GetVehicleCategories();

        /// <summary>
        /// Returns a list of all weapon categories.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "WeaponCategories")]
        IList<EnumDto<WeaponCategory>> GetWeaponCategories();

        /// <summary>
        /// Returns a list of all weather types.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "WeatherTypes")]
        IList<EnumDto<WeatherType>> GetWeatherTypes();
    } // IEnumService
}
