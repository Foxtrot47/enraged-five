﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public abstract class MapExportSubStatDto : EntityDtoBase
    {
        /// <summary>
        /// When the export started.
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// When the export completed.
        /// </summary>
        public DateTime? End { get; set; }

        /// <summary>
        /// Duration of the export.
        /// </summary>
        [XmlIgnore()]
        public TimeSpan? Duration
        {
            get
            {
                return (End == null ? null : End - Start);
            }
        }

        /// <summary>
        /// Whether the export was successful.
        /// </summary>
        public bool? Success { get; set; }
    } // MapExportSubStatDto
}
