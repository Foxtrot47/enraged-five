﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.Reports
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ReportParameter : IDto
    {
        #region Properties
        /// <summary>
        /// Name of the report parameter.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Type of parameter this is.
        /// </summary>
        [DataMember]
        public String Type { get; set; }

        /// <summary>
        /// Description for the parameter.
        /// </summary>
        [DataMember]
        public String Description { get; set; }

        /// <summary>
        /// Whether the user must provide a value for this parameter.
        /// </summary>
        [DataMember]
        public bool Required { get; set; }

        /// <summary>
        /// Value associated with this parameter.
        /// </summary>
        [DataMember]
        public String Value { get; set; }

        /// <summary>
        /// List of valid values for this parameter (optional).
        /// </summary>
        //[DataMember]
        //public List<ReportParameterOption> ValidValues { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (required for serialisation).
        /// </summary>
        public ReportParameter()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ReportParameter(String name, String type, String description, bool required)
        {
            Name = name;
            Type = type;
            Description = description;
            Required = required;
        }
        #endregion // Constructor(s)
    } // ReportParameter
}
