﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class FreemodeMatchTypeBreakdown
    {
        #region Properties
        /// <summary>
        /// Name for this group.
        /// </summary>
        [DataMember]
        public String BreakdownName { get; set; }

        /// <summary>
        /// Items that make up this group.
        /// </summary>
        [DataMember]
        public List<FreemodeMatchTypeBreakdownItemBase> Items { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FreemodeMatchTypeBreakdown()
        {
            Items = new List<FreemodeMatchTypeBreakdownItemBase>();
        }
        #endregion // Constructor(s)
    } // FreemodeMatchBreakdown
}
