﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Dto;
using RSG.Model.Statistics.Captures;
using RSG.Model.Statistics.Captures.Historical;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service interface for dealing with stats captures from game smoke tests.
    /// </summary>
    [ServiceContract]
    public interface ICaptureService
    {
        #region Data Submission
        #region Name Hashes
        /// <summary>
        /// Creates a new zone with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Zone?name={zoneName}")]
        NameHashDto CreateZone(String zoneName);

        /// <summary>
        /// Creates a new cpu set with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "CpuSet?name={setName}")]
        NameHashDto CreateCpuSet(String setName);

        /// <summary>
        /// Creates a new cpu metric with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "CpuMetric?name={metricName}")]
        NameHashDto CreateCpuMetric(String metricName);

        /// <summary>
        /// Creates a new thread with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "ThreadName?name={name}")]
        NameHashDto CreateThreadName(String name);

        /// <summary>
        /// Creates a new memory bucket with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "MemoryBucket?name={bucketName}")]
        NameHashDto CreateMemoryBucket(String bucketName);

        /// <summary>
        /// Creates a new streaming memory module with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "StreamingMemoryModule?name={moduleName}")]
        NameHashDto CreateStreamingMemoryModule(String moduleName);

        /// <summary>
        /// Creates a new streaming memory category with the specified name.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "StreamingMemoryCategory?name={categoryName}")]
        NameHashDto CreateStreamingMemoryCategory(String categoryName);
        #endregion // Name Hashes

        #region Stats
        /// <summary>
        /// Creates a new set of stats for perf stats.
        /// </summary>
        /// <param name="session"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "PerfStats")]
        void CreatePerfStats(TestSession session);

        /// <summary>
        /// Creates a new set of stats associated with a particular changelist.
        /// </summary>
        /// <param name="changelistNumber"></param>
        /// <param name="session"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Changelist/{changelistNumber}")]
        void CreateChangelistStats(String changelistNumber, TestSession session);

        /// <summary>
        /// Creates a new set of stats associated with a particular changelist.
        /// </summary>
        /// <param name="session"></param>
        //[OperationContract]
        //[WebInvoke(Method = "PUT", UriTemplate = "Stats/Build/{buildIdentifier}?level={levelIdentifier}&buildConfig={buildConfigIdentifier}")]
        //void CreateBuildStats(String buildIdentifier, TestSession session, String levelIdentifier, String buildConfigIdentifier);
        #endregion // Stats
        #endregion // Data Submission

        #region Data Retrieval
        /// <summary>
        /// Retrieves all changelists.
        /// http://localhost:8080/CaptureStats/Changelists
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Changelists")]
        IEnumerable<uint> RetrieveChangelists();

        /// <summary>
        /// Retrieves all buildIds with perfstats.
        /// http://localhost:8080/CaptureStats/PerfStat/Builds
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "PerfStats/Builds")]
        IEnumerable<string> RetrievePerfstatBuilds();

        /// <summary>
        /// Retrieves the changelist number of the latest set of changelist capture stat.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Changelist/Latest/Number")]
        uint RetrieveLatestChangelistNumber();

        /// <summary>
        /// Retrieves the zones that were a part of a particular per changelist capture.
        /// </summary>
        /// <param name="changelistNumber"></param>
        [OperationContract]
        [WebGet(UriTemplate = "Changelist/{changelistNumber}/Zones")]
        NameHashDtos RetrieveChangelistZones(String changelistNumber);

        /// <summary>
        /// Retrieves the zones that were a part of a particular builds perfstat capture.
        /// http://localhost:8080/CaptureStats/PerfStats/298/Zones
        /// </summary>
        /// <param name="buildNumber"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "PerfStats/{buildNumber}/Zones")]
        NameHashDtos RetrievePerfStatZones(String buildNumber);

        /// <summary>
        /// Retrieves all historical data associated with a particular zone.
        /// http://localhost:8080/CaptureStats/PerfStats/298/Zones/1122153599/Historical?maxResults=10
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "PerfStats/{buildNumber}/Zones/{zoneIdentifier}/Historical?maxResults={maxResults}")]
        HistoricalZoneResults RetrievePerfStatHistoricalResultsForZone(String buildNumber, String zoneIdentifier, uint maxResults);

        /// <summary>
        /// Retrieves all historical data associated with a particular zone.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Changelist/{changelistNumber}/Zones/{zoneIdentifier}/Historical?maxResults={maxResults}")]
        HistoricalZoneResults RetrieveHistoricalResultsForZone(String changelistNumber, String zoneIdentifier, uint maxResults);

        /// <summary>
        /// Retrieve the name of the metrics availableto show for a particular automated test.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "AutomatedTests/{automatedTestNumber}/PerformanceMetrics?build={buildIdentifier}&platform={platformIdentifier}&level={levelIdentifier}")]
        List<String> RetrieveAutomatedTestPerformanceMetrics(String automatedTestNumber, String buildIdentifier, String platformIdentifier, uint levelIdentifier);

        /// <summary>
        /// Retrieves summary information for all automated capture stats associated with a particular build/level/platform.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "AutomatedTests/{automatedTestNumber}/Summary?build={buildIdentifier}&platform={platformIdentifier}&level={levelIdentifier}")]
        TestSessionSummary RetreieveAutomatedTestPerformanceSummary(String automatedTestNumber, String buildIdentifier, String platformIdentifier, uint levelIdentifier);
        #endregion // Data Retrieval
    } // ICaptureService
}
