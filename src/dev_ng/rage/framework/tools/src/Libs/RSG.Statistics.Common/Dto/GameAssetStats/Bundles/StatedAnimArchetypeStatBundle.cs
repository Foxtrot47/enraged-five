﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StatedAnimArchetypeStatBundle : ArchetypeStatBundle
    {
        #region Properties
        /// <summary>
        /// The archetypes that make up this stated anim
        /// </summary>
        [DataMember]
        public List<string> ComponentArchetypeIdentifiers { get; set; }
        #endregion // Properties
    } // StatedAnimArchetypeStatBundle
}
