﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service contract for querying scene xml data.
    /// </summary>
    [ServiceContract]
    public interface ISceneXmlQueryService
    {
        /// <summary>
        /// Retrieves the list of scene xml files that are using the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        [OperationContract]
        IList<String> GetSceneXmlFilesUsingTexture(String filename);

        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        [OperationContract]
        IList<SceneXmlDto.TextureUsage> GetTextureUsage(IEnumerable<String> textures);

        /// <summary>
        /// Retrieves a list of materials that make use of the specified texture.
        /// </summary>
        /// <param name="filename">The texture to search for.</param>
        /// <returns></returns>
        [OperationContract]
        IList<SceneXmlDto.Material> GetMaterialsThatUseTexture(String filename);

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        [OperationContract]
        IList<SceneXmlDto.TexturePair> GetTexturePairs(IEnumerable<String> textures);
    } // ISceneXmlQueryService
}
