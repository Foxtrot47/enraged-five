﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalStats
    {
        /// <summary>
        /// General summary of all exports.
        /// </summary>
        [DataMember]
        public Dictionary<DateTime, HistoricalExportStat> GeneralStats { get; set; }
        
        /// <summary>
        /// Summary on a per section basis.
        /// </summary>
        [DataMember]
        public Dictionary<String, Dictionary<DateTime, HistoricalExportStat>> SectionStats { get; set; }
    } // HistoricalStats
}
