﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Single transaction stat.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalTransactionStat : Range<DateTime>
    {
        [DataMember]
        public ulong FreemodeIn { get; set; }

        [DataMember]
        public ulong FreemodeOut { get; set; }

        [DataMember]
        public ulong BankIn { get; set; }

        [DataMember]
        public ulong BankOut { get; set; }
    } // HistoricalTransactionStat
}
