﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ComparativeExportStatDtos : IDtos<ComparativeExportStatDto>
    {
        public List<ComparativeExportStatDto> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ComparativeExportStatDtos()
        {
            Items = new List<ComparativeExportStatDto>();
        }
    } // ComparativeExportStatDtos
}
