﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.VerticaData;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    public interface IVerticaDataService
    {
        #region Clans
        /// <summary>
        /// Searches for a clan based on a partial name.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Clans/Search?name={searchName}")]
        List<Clan> SearchForClan(String searchName);

        /// <summary>
        /// Returns the list of gamers that are members of a particular clan.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Clans/{id}/Members")]
        List<Gamer> GetClanMembers(String id);
        #endregion // Clans

        #region Gamers
        /// <summary>
        /// Searches for a gamer based on a partial name.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Gamers/Search?name={searchName}&platform={platform}")]
        List<Gamer> SearchForGamer(String searchName, String platform);

        /// <summary>
        /// Validates a list of gamers returning only the valid ones.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Gamers/Validate")]
        List<Gamer> ValidateGamerList(List<Gamer> gamers);

        /// <summary>
        /// Retrieves the list of all gamer groups.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Gamers/Groups")]
        List<GamerGroup> GetGamerGroups();

        /// <summary>
        /// Creates a new gamer group.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Gamers/Groups")]
        List<String> CreateGamerGroup(GamerGroup group);

        /// <summary>
        /// Updates an existing gamer group.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Gamers/Groups/{*name}")]
        List<String> UpdateGamerGroup(GamerGroup group, String name);

        /// <summary>
        /// Deletes an existing gamer group.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Gamers/Groups/{*name}")]
        void DeleteGamerGroup(String name);
        #endregion // Gamers

        #region Missions
        /// <summary>
        /// Searches for a UGC mission user with the particular name.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Missions/Search?name={searchName}&matchType={matchType}&category={missionCategory}&isPublished={published}")]
        List<UGCMission> SearchForMission(String searchName, String matchType, String missionCategory, String published);
        #endregion // Missions

        #region Users
        /// <summary>
        /// Searches for a social club user with the particular name.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Users/Search?name={searchName}")]
        List<String> SearchForUser(String searchName);

        /// <summary>
        /// Validates a list of users returning only the valid ones.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Users/Validate")]
        List<String> ValidateUserList(List<String> users);

        /// <summary>
        /// Retrieves the list of all user groups.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Users/Groups")]
        List<UserGroup> GetUserGroups();

        /// <summary>
        /// Creates a new user group.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "Users/Groups")]
        List<String> CreateUserGroup(UserGroup group);

        /// <summary>
        /// Updates an existing user group.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Users/Groups/{*name}")]
        List<String> UpdateUserGroup(UserGroup group, String name);

        /// <summary>
        /// Deletes an existing user group.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Users/Groups/{*name}")]
        void DeleteUserGroup(String name);
        #endregion // Users
    } // IVerticaDataService
}
