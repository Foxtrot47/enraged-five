﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// Mission performance information.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionPerformance : FpsPerformanceStat
    {
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Checkpoints within the mission and their associated Performance details.
        /// </summary>
        [DataMember]
        public ICollection<MissionCheckpointPerformance> CheckpointPerformances { get; set; }
    } // MissionPerformance
}
