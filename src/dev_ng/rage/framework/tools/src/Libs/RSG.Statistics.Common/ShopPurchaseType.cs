﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Shop purchase type enumeration.
    /// Needs to match the ePurchaseType enum specified in the //depot/gta5/src/dev/game/network/Live/NetworkTransactionTelemetry.h file.
    /// </summary>
    public enum ShopPurchaseType
    {
        Weapons = 0,
        WeaponMods,
        WeaponAmmo,
        Armor,
        Barbers,
        Clothes,
        Tattoos,
        Vehicles,
        CarMods,
        CarInsurance,
        CarDropOff,
        CarRepair,
        Food,
        Masks
    } // ShopPurchaseType
}
