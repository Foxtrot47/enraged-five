﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.VerticaData
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class Gamer : IEquatable<Gamer>
    {
        #region Properties
        /// <summary>
        /// Gamer handle.
        /// </summary>
        [DataMember]
        public String GamerHandle { get; set; }

        /// <summary>
        /// User id.
        /// </summary>
        [DataMember]
        public String UserId { get; set; }

        /// <summary>
        /// Platform this gamer is for.
        /// </summary>
        [DataMember]
        public ROSPlatform Platform { get; set; }

        /// <summary>
        /// Unique id for this gamer.
        /// </summary>
        [DataMember]
        public ulong Id { get; set; }
        #endregion // Properties

        #region IEquatable<Gamer> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(Gamer other)
        {
            if (other == null)
            {
                return false;
            }

            return (GamerHandle == other.GamerHandle && Platform == other.Platform);
        }
        #endregion // IEquatable<Gamer> Interface

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return GamerHandle.GetHashCode() ^ Platform.GetHashCode();
        }
        #endregion // Object Overrides
    } // Gamer
}
