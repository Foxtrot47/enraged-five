﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Statistics.Common.Dto.Enums;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class EntityStatBundle : DtoBase
    {
        /// <summary>
        /// Identifier for this entity.
        /// </summary>
        [DataMember]
        public string EntityIdentifier { get; set; }

        /// <summary>
        /// The archetype this entity is referencing.
        /// </summary>
        [DataMember]
        public string ArchetypeIdentifier { get; set; }

        /// <summary>
        /// Awkward, but because we could have the same identifier but different types we need this to resolve the correct
        /// archetype reference on the server side.
        /// </summary>
        [DataMember]
        public string ArchetypeType { get; set; }

        /// <summary>
        /// The identifier of the parent of this entity (will be either a map section/interior archetype or room).
        /// </summary>
        [DataMember]
        public string ParentIdentifier { get; set; }

        /// <summary>
        /// Again, awkward, we could have derived objects for each type but this is simpler (for now).
        /// </summary>
        [DataMember]
        public string ParentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public Vector3f Position { get; set; }

        /// <summary>
        /// World space bounding box
        /// </summary>
        [DataMember]
        public BoundingBox3f BoundingBox { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PriorityLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsLowPriority { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool IsInteriorReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public LodLevelDto LodLevel { get; set; }

        /// <summary>
        /// Lod distance (returns either the overridden distance or the referenced archetypes lod distance)
        /// </summary>
        [DataMember]
        public float LodDistance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public float? LodDistanceOverride { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string LodParentEntityIdentifier { get; set; }

        /// <summary>
        /// AttrNames.OBJ_FORCE_BAKE_COLLISION attribute
        /// </summary>
        [DataMember]
        public bool ForceBakeCollision { get; set; }
    } // EntityStatBundle
}
