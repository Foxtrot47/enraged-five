﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Online
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class PerRankStat
    {
        /// <summary>
        /// Rank this information pertains to.
        /// </summary>
        [DataMember]
        public uint Rank { get; set; }

        /// <summary>
        /// Average amount of time players took to reach this rank.
        /// </summary>
        [DataMember]
        public double AvgPlayingTime { get; set; }

        /// <summary>
        /// Minimum amount of time players took to reach this rank.
        /// </summary>
        [DataMember]
        public double MinPlayingTime { get; set; }

        /// <summary>
        /// Maximum amount of time players took to reach this rank.
        /// </summary>
        [DataMember]
        public double MaxPlayingTime { get; set; }

        /// <summary>
        /// Average amount of cash players have earned when they reach this rank.
        /// </summary>
        [DataMember]
        public decimal AvgCashEarned { get; set; }

        /// <summary>
        /// Minimum amount of cash players that any player earned when they reached this rank
        /// </summary>
        [DataMember]
        public decimal MinCashEarned { get; set; }

        /// <summary>
        /// Maximum amount of cash players that any player earned when they reached this rank.
        /// </summary>
        [DataMember]
        public decimal MaxCashEarned { get; set; }

        /// <summary>
        /// Number of samples that contributed to the averages.
        /// </summary>
        [DataMember]
        public decimal SampleSize { get; set; }
    } // PerRankStat
}
