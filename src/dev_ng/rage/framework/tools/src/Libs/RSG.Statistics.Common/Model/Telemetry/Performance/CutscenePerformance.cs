﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class CutscenePerformance : FpsPerformanceStat
    {
        /// <summary>
        /// Name of the cutscene.
        /// </summary>
        [DataMember]
        public String CutsceneName { get; set; }
    } // CutscenePerformance
}
