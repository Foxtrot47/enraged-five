﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class GameAssetStatDtoBase : EntityDtoBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("BuildId")]
        public long? BuildId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("PolyCount")]
        public int? PolyCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("CollisionCount")]
        public int? CollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement("BoneCount")]
        public int? BoneCount
        {
            get;
            set;
        }
        #endregion // Properties
    } // GameAssetStatDtoBase
}
