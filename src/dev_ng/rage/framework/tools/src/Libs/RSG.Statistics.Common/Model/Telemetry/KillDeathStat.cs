﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Stat for information about a single tv show.
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class KillDeathStat
    {
        /// <summary>
        /// Weapon name.
        /// </summary>
        [DataMember]
        public String WeaponName { get; set; }

        /// <summary>
        /// Friendly weapon name.
        /// </summary>
        [DataMember]
        public String WeaponFriendlyName { get; set; }

        /// <summary>
        /// Hash of the weapon.
        /// </summary>
        [DataMember]
        public uint WeaponHash { get; set; }

        /// <summary>
        /// Number of kills or deaths with this particular weapon.
        /// </summary>
        [DataMember]
        public ulong Count { get; set; }

        /// <summary>
        /// Number of gamers that have killed/died from this weapon.
        /// </summary>
        [DataMember]
        public ulong NumberOfGamers { get; set; }
    } // KillDeathStat
}
