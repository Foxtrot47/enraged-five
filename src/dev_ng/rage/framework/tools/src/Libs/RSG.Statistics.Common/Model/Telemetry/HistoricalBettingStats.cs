﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Mapping of datetimes to betting stats.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalBettingStats
    {
        #region Properties
        /// <summary>
        /// Betting stats grouped by time intervals.
        /// </summary>
        [DataMember]
        public IList<HistoricalBettingStat> Values { get; set; }

        /// <summary>
        /// List of rake values that were active during this period.
        /// </summary>
        [DataMember]
        public IList<KeyValueStat<float, ulong>> Rakes { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HistoricalBettingStats()
        {
            Values = new List<HistoricalBettingStat>();
            Rakes = new List<KeyValueStat<float, ulong>>();
        }
        #endregion // Constructor(s)
    } // HistoricalBettingStats

    /// <summary>
    /// Single betting stat.
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalBettingStat : Range<DateTime>
    {
        #region Properties
        /// <summary>
        /// Number of bets that were placed.
        /// </summary>
        [DataMember]
        public ulong BetsMade { get; set; }

        /// <summary>
        /// Number of bets that were placed and subsequently won.
        /// </summary>
        [DataMember]
        public ulong BetsThatWon { get; set; }

        /// <summary>
        /// Amount of money that players bet.
        /// </summary>
        [DataMember]
        public decimal AmountBet { get; set; }

        /// <summary>
        /// Amount of money that player's made off of their bets.
        /// </summary>
        [DataMember]
        public decimal AmountWon { get; set; }

        /// <summary>
        /// Amount of commission Rockstar made off of all the bets.
        /// </summary>
        [DataMember]
        public decimal RockstarCommission { get; set; }

        /// <summary>
        /// Convenience method for getting how much Rockstar made off of these players/bets.
        /// </summary>
        public decimal RockstarWinnings
        {
            get
            {
                return AmountBet - AmountWon;
            }
        }
        #endregion // Properties
    } // HistoricalBettingStat
}
