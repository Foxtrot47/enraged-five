﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DrawableArchetypeStatBundle : SimpleArchetypeStatBundle
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DrawableArchetypeStatBundle()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // DrawableArchetypeStatBundleDto
}
