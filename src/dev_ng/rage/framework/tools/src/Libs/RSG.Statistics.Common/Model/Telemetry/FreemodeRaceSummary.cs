﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information regarding a freemode race.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FreemodeRaceSummary : FreemodeUGCMatchBase
    {
        #region Properties
        /// <summary>
        /// Info regarding how often a particular vehicle was selected.
        /// </summary>
        [DataMember]
        public List<VehicleSelectionInfo> SelectionInfo { get; set; }

        /// <summary>
        /// How often a particular vehicle won.
        /// </summary>
        [DataMember]
        public List<WinningVehicleInfo> WinnerInfo { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public FreemodeRaceSummary()
            : base()
        {
            SelectionInfo = new List<VehicleSelectionInfo>();
            WinnerInfo = new List<WinningVehicleInfo>();
        }
        #endregion // Constructor(s)
    } // FreemodeRaceSummary


    /// <summary>
    /// Information regarding vehicle selection.
    /// </summary>
    [DataContract]
    [Serializable]
    public class VehicleSelectionInfo
    {
        #region Properties
        /// <summary>
        /// Name of the vehicle.
        /// </summary>
        [DataMember]
        public String VehicleName { get; set; }

        /// <summary>
        /// Number of times this particular vehicle was used.
        /// </summary>
        [DataMember]
        public ulong TimesUsed { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        internal VehicleSelectionInfo()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public VehicleSelectionInfo(String vehicleName, ulong timesUsed)
        {
            VehicleName = vehicleName;
            TimesUsed = timesUsed;
        }
        #endregion // Constructor(s)
    } // VehicleSelectionInfo


    /// <summary>
    /// How often a particular vehicle won.
    /// </summary>
    [DataContract]
    [Serializable]
    public class WinningVehicleInfo
    {
        #region Properties
        /// <summary>
        /// Name of the vehicle.
        /// </summary>
        [DataMember]
        public String VehicleName { get; set; }

        /// <summary>
        /// Number of times this particular vehicle was used.
        /// </summary>
        [DataMember]
        public ulong TimesWon { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        internal WinningVehicleInfo()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public WinningVehicleInfo(String vehicleName, ulong timesWon)
        {
            VehicleName = vehicleName;
            TimesWon = timesWon;
        }
        #endregion // Constructor(s)
    } // WinningVehicleInfo
}
