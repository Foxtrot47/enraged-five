﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using RSG.Model.Common;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("VehicleStatBundle")]
    [DataContract]
    public class VehicleStatBundleDto : GameAssetStatBundleDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String GameName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PolyCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int BoneCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int DoorCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ElevatorCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int ExtraCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int HandlebarCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasLod1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasLod2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool HasLod3 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int LightCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int PropellerCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int RotorCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int RudderCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int SeatCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int WheelCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        [XmlArray("VehiclePlatformStats")]
        [XmlArrayItem("VehiclePlatformStat")]
        public List<VehiclePlatformStatBundleDto> VehiclePlatformStats { get; set; }
    } // VehicleStatBundleDto
}
