﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Model.Common;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// A query made to retrieve particular streamable stats for a set of objects.
    /// </summary>
    [DataContract]
    public class StreamableStatFetchDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// List of asset identifiers we wish to retrieve information for.
        /// </summary>
        [DataMember]
        public List<uint> AssetIdentifiers { get; set; }

        /// <summary>
        /// List of stats we wish to retrieve.
        /// </summary>
        [DataMember]
        public List<StreamableStat> RequestedStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        private StreamableStatFetchDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifiers"></param>
        /// <param name="stats"></param>
        public StreamableStatFetchDto(List<uint> identifiers, List<StreamableStat> stats)
        {
            AssetIdentifiers = identifiers;
            RequestedStats = stats;
        }
        #endregion // Constructor(s)
    } // StreamableStatFetchDto
}
