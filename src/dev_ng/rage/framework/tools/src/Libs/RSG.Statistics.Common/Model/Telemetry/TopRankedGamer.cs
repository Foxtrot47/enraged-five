﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class TopRankedGamer
    {
        /// <summary>
        /// Player's account id.
        /// </summary>
        [DataMember]
        public uint AccountId { get; set; }

        /// <summary>
        /// Gamer handle.
        /// </summary>
        [DataMember]
        public String GamerHandle { get; set; }

        /// <summary>
        /// Platform this gamer is for.
        /// </summary>
        [DataMember]
        public ROSPlatform Platform { get; set; }

        /// <summary>
        /// Slot that the character is in.
        /// </summary>
        [DataMember]
        public uint CharacterSlot { get; set; }

        /// <summary>
        /// Unique character ID for this slot.
        /// </summary>
        [DataMember]
        public uint CharacterId { get; set; }

        /// <summary>
        /// Maximum amount of xp this character has.
        /// </summary>
        [DataMember]
        public ulong XP { get; set; }
    } // TopRankedGamer
}
