﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("LodLevel")]
    public class LodLevelDto : EnumDto<LodLevel>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public LodLevelDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lodLevel"></param>
        public LodLevelDto(LodLevel lodLevel, String name)
            : base(lodLevel, name)
        {
        }
        #endregion // Constructor(s)
    } // LodLevelDto
}
