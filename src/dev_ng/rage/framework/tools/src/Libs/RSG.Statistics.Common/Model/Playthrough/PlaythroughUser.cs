﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// Dto object for playthrough users.
    /// </summary>
    [DataContract]
    public class PlaythroughUser
    {
        /// <summary>
        /// Name of the user as per bugstar.
        /// </summary>
        [DataMember]
        public String Username { get; set; }

        /// <summary>
        /// Gamer handle for this particular user.
        /// </summary>
        [DataMember]
        public String GamerHandle { get; set; }

        /// <summary>
        /// Platform the user is associated with.
        /// </summary>
        [DataMember]
        public ROSPlatform? Platform { get; set; }
    } // PlaythroughUser
}
