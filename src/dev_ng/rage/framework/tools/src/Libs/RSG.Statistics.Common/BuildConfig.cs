﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Various build configurations of the game
    /// </summary>
    public enum BuildConfig
    {
        [RuntimeName("Debug")]
        Debug,

        [RuntimeName("Beta")]
        Beta,

        [RuntimeName("BankRelease")]
        BankRelease,

        [RuntimeName("Release")]
        Release,

        [RuntimeName("Final")]
        Final
    } // BuildConfig

    /// <summary>
    /// Telemetry metric enumeration utility and extension methods.
    /// </summary>
    public static class BuildConfigUtils
    {
        /// <summary>
        /// Return runtime name string for a BuildConfig enum value.
        /// </summary>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static string GetRuntimeName(this BuildConfig config)
        {
            Type type = config.GetType();

            FieldInfo fi = type.GetField(config.ToString());
            RuntimeNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(RuntimeNameAttribute), false) as RuntimeNameAttribute[];

            return (attributes.Length > 0 ? attributes[0].Name : null);
        }

        /// <summary>
        /// Return BuildConfig value from String representation.
        /// </summary>
        /// <param name="metric">Metric string.</param>
        /// <returns></returns>
        public static BuildConfig ParseFromRuntimeString(String config)
        {
            BuildConfig result;
            if (TryParseFromRuntimeString(config, out result))
            {
                return result;
            }
            else
            {
                throw new ArgumentException(String.Format("{0} is an unrecognised build config,", config));
            }
        }

        /// <summary>
        /// Tries to parse a runtime name into a BuildConfig value, returning whether the operation was successful
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static bool TryParseFromRuntimeString(String name, out BuildConfig config)
        {
            foreach (BuildConfig c in Enum.GetValues(typeof(BuildConfig)))
            {
                if (0 == String.Compare(name, c.GetRuntimeName(), true))
                {
                    config = c;
                    return true;
                }
            }

            config = BuildConfig.Beta;
            return false;
        }

        /// <summary>
        /// Tries to parse a path to find any BuildConfig value, returning whether the operation was successful
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="metric"></param>
        /// <returns></returns>
        public static bool TryParseFromFilepath(String path, out BuildConfig config)
        {
            foreach (BuildConfig c in Enum.GetValues(typeof(BuildConfig)))
            {
                if (path.ToLower().Contains(c.GetRuntimeName().ToLower()))
                {
                    config = c;
                    return true;
                }
            }

            config = BuildConfig.Beta;
            return false;
        }
    } // TelemetryMetricUtils
}
