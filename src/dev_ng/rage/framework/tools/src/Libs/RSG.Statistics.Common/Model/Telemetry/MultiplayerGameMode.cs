﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MultiplayerGameMode
    {
        #region Properties
        /// <summary>
        /// Name of the game mode.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Identifier for the game mode (this is based on what the game outputs to the telemetry.)
        /// </summary>
        [DataMember]
        public uint Identifier { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MultiplayerGameMode()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public MultiplayerGameMode(String name, uint identifier)
        {
            Name = name;
            Identifier = identifier;
        }
        #endregion // Constructor(s)
    } // MultiplayerGameMode
}
