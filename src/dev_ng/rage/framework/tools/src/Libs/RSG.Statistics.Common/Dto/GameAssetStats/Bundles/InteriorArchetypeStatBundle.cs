﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class InteriorArchetypeStatBundle : ArchetypeStatBundle
    {
        #region Properties
        /// <summary>
        /// List of rooms for this interior.
        /// </summary>
        [DataMember]
        public List<RoomStatBundle> Rooms { get; set; }

        /// <summary>
        /// List of portals for this interior.
        /// </summary>
        [DataMember]
        public List<OldPortalStatDto> Portals { get; set; }
        #endregion // Properties
    } // InteriorArchetypeStatBundle
}
