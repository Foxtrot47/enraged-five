﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// Mission rating statistic.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionRatingStat
    {
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Mission id.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Rating for this mission.
        /// </summary>
        [DataMember]
        public float Rating { get; set; }
    } // MissionRatingStat
}
