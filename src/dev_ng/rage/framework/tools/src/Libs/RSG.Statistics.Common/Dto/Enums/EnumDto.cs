﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEnum"></typeparam>
    [Serializable]
    [DataContract]
    public class EnumDto<TEnum> : IDto where TEnum : struct
    {
        #region Properties
        /// <summary>
        /// Enum value (may be null).
        /// </summary>
        [DataMember]
        public TEnum Value { get; set; }

        /// <summary>
        /// Enum string value.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Enum to friendly name string value.
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public EnumDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public EnumDto(TEnum value, String name)
        {
            Value = value;
            Name = name;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FriendlyName;
        }
        #endregion // Object Overrides
    } // EnumDto<TEnum>
}
