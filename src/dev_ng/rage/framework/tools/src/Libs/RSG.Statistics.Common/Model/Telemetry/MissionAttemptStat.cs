﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using System.Xml.Serialization;
using RSG.Statistics.Common.Dto.Enums;
using System.Runtime.Serialization;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class MissionAttemptStat
    {
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Id of the mission.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Number of times this mission has been attempted
        /// </summary>
        [DataMember]
        public ulong TotalAttempts { get; set; }

        /// <summary>
        /// Number of times this mission has been replayed.
        /// </summary>
        [DataMember]
        public ulong ReplayAttempts { get; set; }

        /// <summary>
        /// Number of times players passed this mission.
        /// </summary>
        [DataMember]
        public ulong TotalPassed { get; set; }

        /// <summary>
        /// Number of times players failed this mission.
        /// </summary>
        [DataMember]
        public ulong TotalFailed { get; set; }

        /// <summary>
        /// Number of times players cancelled this mission.
        /// </summary>
        [DataMember]
        public ulong TotalCancelled { get; set; }

        /// <summary>
        /// Number of times players skipped this mission.
        /// </summary>
        [DataMember]
        public ulong TotalSkipped { get; set; }

        /// <summary>
        /// Unsure of what happened on the misison.
        /// </summary>
        [DataMember]
        public ulong TotalUnknown { get; set; }

        /// <summary>
        /// Total time players have spent playing this mission.
        /// </summary>
        [DataMember]
        public ulong TotalTime { get; set; }

        /// <summary>
        /// Number of players that have attempted the mission.
        /// </summary>
        [DataMember]
        public ulong NumGamersAttempted { get; set; }
    } // MissionAttemptStatDto
}
