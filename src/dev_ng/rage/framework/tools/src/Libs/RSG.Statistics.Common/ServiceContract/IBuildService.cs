﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Model.Common.Animation;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(BuildKnownTypeProvider))]
    public interface IBuildService// : IGameAssetServiceBase<BuildDto, BuildDtos>
    {
        #region TEMP HACK until IGameAssetServiceBase isn't tagged as xmlserializerformat (although the plan is to remove it altogether).
        /// <summary>
        /// Returns a list of all game assets
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "All")]
        BuildDtos GetAll();

        /// <summary>
        /// Gets a game asset based on its identifier.
        /// </summary>
        /// <param name="assetId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{identifier}")]
        BuildDto GetByIdentifier(string identifier);

        /// <summary>
        /// Updates or creates a list of game assets.
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "All")]
        void PutAssets(BuildDtos dtos);

        /// <summary>
        /// Updates or creates a game asset based on its identifier
        /// </summary>
        /// <param name="assetId"></param>
        /// <param name="asset"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{identifier}")]
        BuildDto PutAsset(string identifier, BuildDto asset);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "{identifier}")]
        void DeleteAsset(string identifier);
        #endregion // TEMP HACK

        #region Animations
        /// <summary>
        /// Creates per build stats for clip dictionaries.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{buildIdentifier}/ClipDictionaries/Stats")]
        void CreateClipDictionaryStats(String buildIdentifier, IClipDictionaryCollection clipDictionaries);
        #endregion // Animations

        #region Builds
        /// <summary>
        /// Gets the latest build.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Latest")]
        BuildDto GetLatest();

        /// <summary>
        /// Gets the latest 'n' builds.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Latest?count={count}")]
        BuildDtos GetLatestBuilds(uint count);

        /// <summary>
        /// Updates all the build features based off of Vertica information.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "UpdateBuildFeatures")]
        void UpdateBuildFeatures();
        #endregion // Builds

        #region Characters
        /// <summary>
        /// Gets a list of characters that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Characters")]
        CharacterDtos GetAllCharactersForBuild(string buildIdentifier);

        /// <summary>
        /// Gets an individual character for a particular build based on the character's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Characters/{characterHash}")]
        CharacterDto GetCharacterForBuild(string buildIdentifier, string characterHash);

        /// <summary>
        /// Gets an individual character statistic for a particular build and character.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Characters/{characterHash}/Stats")]
        CharacterStatBundleDto GetCharacterStat(string buildIdentifier, string characterHash);

        /// <summary>
        /// Creates a new character stat for the specified build and character.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <param name="characterStatBundle">Data transfer object containing all the information relating to a character stat.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Characters/{characterHash}/Stats")]
        void CreateCharacterStat(string buildIdentifier, string characterHash, CharacterStatBundleDto characterStatBundle);
        #endregion // Characters

        #region Cutscenes
        /// <summary>
        /// Creates per build stats for cutscenes.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{buildIdentifier}/Cutscenes/Stats")]
        void CreateCutsceneStats(String buildIdentifier, ICutsceneCollection cutscenes);

        /// <summary>
        /// Gets the list of cutscenes that are associated with the specified build.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Cutscenes")]
        ICutsceneCollection GetCutsceneStats(String buildIdentifier);
        #endregion // Cutscenes

        #region Levels
        /// <summary>
        /// Gets a list of characters that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Levels")]
        LevelDtos GetAllLevelsForBuild(string buildIdentifier);

        /// <summary>
        /// Gets an individual character for a particular build based on the character's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Levels/{levelHash}")]
        LevelDto GetLevelForBuild(string buildIdentifier, string levelHash);

        /// <summary>
        /// Creates a new character stat for the specified build and character.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="characterHash">Hash of the character's name.</param>
        /// <param name="characterStatBundle">Data transfer object containing all the information relating to a character stat.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/Stats")]
        void CreateLevelStat(string buildIdentifier, string levelHash, LevelStatBundleDto levelStatBundle);

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of levels.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/Stats")]
        List<LevelStatDto> GetMultipleLevelStats(String buildIdentifier, StreamableStatFetchDto fetchDto);
        #endregion // Levels

        #region Map Hierarchy
        #region Data Submission
        /// <summary>
        /// Creates a new map hierarchy for a particular build/level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="hierarchyBundle"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapHierarchy")]
        void CreateMapHierarchy(String buildIdentifier, String levelHash, MapHierarchyBundleDto hierarchyBundle);

        /// <summary>
        /// Creates a new map area stat for the specified build and map area.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapAreas/{mapAreaHash}/Stats")]
        void UpdateMapAreaStat(String buildIdentifier, String levelHash, String mapAreaHash, MapAreaStatBundleDto mapAreaStatBundle);

        /// <summary>
        /// Updates map section stats.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapSections/{mapSectionHash}/Stats")]
        void UpdateMapSectionStat(String buildIdentifier, String levelHash, String mapSectionHash, MapSectionStatBundleDto mapSectionStatBundle);

        /// <summary>
        /// Creates archetype stats linked to a particular map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapSections/{mapSectionHash}/Stats/Archetypes")]
        void CreateArchetypeStats(String buildIdentifier, String levelHash, String mapSectionHash, ArchetypeStatBundles archetypeStatBundles);

        /// <summary>
        /// Creates entity stats for a particular map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapAreaHash"></param>
        /// <param name="mapAreaStatBundle"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapSections/{mapSectionHash}/Stats/Entities")]
        void CreateEntityStats(String buildIdentifier, String levelHash, String mapSectionHash, EntityStatBundles entityStatBundles);

        /// <summary>
        /// Creates a new car gen stat for the specified build, level and map section.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="mapSectionHash"></param>
        /// <param name="mapSectionStatBundle"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapSections/{mapSectionHash}/Stats/CarGens")]
        void CreateCarGenStats(String buildIdentifier, String levelHash, String mapSectionHash, CarGenStatDtos dtos);

        /// <summary>
        /// Creates a new spawn point stat for the specified build and level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="dtos"></param>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/SpawnPoints")]
        void CreateSpawnPointStats(String buildIdentifier, String levelHash, SpawnPointStatDtos dtos);
        #endregion // Data Submission

        #region Data Retrieval
        /// <summary>
        /// Retrieves the map hierarchy along with some very basic information for a particular build/level.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapHierarchy")]
        MapHierarchyBundleDto GetMapHierarchy(String buildIdentifier, String levelHash);

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of map sections.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/MapSections/Stats")]
        List<MapSectionStatDto> GetMultipleMapSectionStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto);

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of archetypes.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/Archetypes/Stats")]
        [ServiceKnownType(typeof(DrawableArchetypeStatDto))]
        [ServiceKnownType(typeof(FragmentArchetypeStatDto))]
        [ServiceKnownType(typeof(InteriorArchetypeStatDto))]
        [ServiceKnownType(typeof(StatedAnimArchetypeStatDto))]
        List<ArchetypeStatDto> GetMultipleArchetypeStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto);

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of entities.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/Entities/Stats")]
        List<EntityStatDto> GetMultipleEntityStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto);

        /// <summary>
        /// Retrieves a specific sub set of stats for a particular set of room.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="levelHash"></param>
        /// <param name="fetchDto"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Levels/{levelHash}/Rooms/Stats")]
        List<RoomStatDto> GetMultipleRoomStats(String buildIdentifier, String levelHash, StreamableStatFetchDto fetchDto);
        #endregion // Data Retrieval
        #endregion // Map Hierarchy

        #region Vehicles
        /// <summary>
        /// Gets a list of vehicles that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Vehicles")]
        VehicleDtos GetAllVehiclesForBuild(string buildIdentifier);

        /// <summary>
        /// Gets an individual vehicle for a particular build based on the vehicle's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash">Hash of the vehicle's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Vehicles/{vehicleHash}")]
        VehicleDto GetVehicleForBuild(string buildIdentifier, string vehicleHash);

        /// <summary>
        /// Gets an individual vehicle statistic for a particular build and vehicle.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash">Hash of the vehicle's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Vehicles/{vehicleHash}/Stats")]
        VehicleStatBundleDto GetVehicleStat(string buildIdentifier, string vehicleHash);

        /// <summary>
        /// Creates a new vehicle stat for the specified build and vehicle.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="vehicleHash">Hash of the vehicle's name.</param>
        /// <param name="vehicleStatBundle">Data transfer object containing all the information relating to a vehicle stat.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Vehicles/{vehicleHash}/Stats")]
        void CreateVehicleStat(string buildIdentifier, string vehicleHash, VehicleStatBundleDto vehicleStatBundle);
        #endregion // Vehicles

        #region Weapons
        /// <summary>
        /// Gets a list of weapons that are associated with the specified build.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Weapons")]
        WeaponDtos GetAllWeaponsForBuild(string buildIdentifier);

        /// <summary>
        /// Gets an individual weapon for a particular build based on the weapon's hash.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Weapons/{weaponHash}")]
        WeaponDto GetWeaponForBuild(string buildIdentifier, string weaponHash);

        /// <summary>
        /// Gets an individual weapon statistic for a particular build and weapon.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{buildIdentifier}/Weapons/{weaponHash}/Stats")]
        WeaponStatBundleDto GetWeaponStat(string buildIdentifier, string weaponHash);

        /// <summary>
        /// Creates a new weapon stat for the specified build and weapon.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        /// <param name="weaponHash">Hash of the weapon's name.</param>
        /// <param name="weaponStatBundle">Data transfer object containing all the information relating to a weapon stat.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{buildIdentifier}/Weapons/{weaponHash}/Stats")]
        void CreateWeaponStat(string buildIdentifier, string weaponHash, WeaponStatBundleDto weaponStatBundle);
        #endregion // Weapons

        #region Data Deletion
        /// <summary>
        /// Deletes all asset stats associated with a particular build.
        /// </summary>
        /// <param name="buildId"></param>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "{buildId}/AssetStats")]
        void DeleteAssetStats(string buildId);

        /// <summary>
        /// Deletes all processed telemetry stats associated with a particular build.
        /// </summary>
        /// <param name="buildId"></param>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "{buildId}/TelemetryStats")]
        void DeleteTelemetryStats(string buildId);
        #endregion // Data Deletion
    } // IBuildService

    /// <summary>
    /// Known type provider for the game asset service.
    /// </summary>
    internal static class BuildKnownTypeProvider
    {
        public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            // Add any types to include here.
            // These can be defined in another assembly (which doesn't have to be referenced statically).
            yield return Type.GetType("RSG.Model.Common.Animation.ClipDictionaryCollection,RSG.Model.Common");
            yield return Type.GetType("RSG.Model.Animation.LocalCutsceneCollection,RSG.Model.Animation");
            yield return Type.GetType("RSG.Model.Animation.DbCutsceneCollection,RSG.Model.Animation");
        }
    } // GameAssetKnownTypeProvider
}
