﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    public class WeeklySectionExportStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Name of the section these stats are for.
        /// </summary>
        public String SectionName { get; set; }

        /// <summary>
        /// Average time it took for this section to be exported (in seconds).
        /// </summary>
        public double AverageExportTime { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Needed for serialisation.
        /// </summary>
        public WeeklySectionExportStatDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionName"></param>
        /// <param name="averageExportTime"></param>
        public WeeklySectionExportStatDto(String sectionName, double averageExportTime)
        {
            SectionName = sectionName;
            AverageExportTime = averageExportTime;
        }
        #endregion // Constructor(s)
    } // WeeklySectionExportStatDto
}
