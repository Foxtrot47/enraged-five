﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// Draw list information for the fps performance related overlay.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class FpsPerformanceDrawListInfoStat
    {
        /// <summary>
        /// Where this reading is for.
        /// </summary>
        [DataMember]
        public Vector2f Location { get; set; }

        /// <summary>
        /// Convenience property (Used by the nhibernate criteria).
        /// </summary>
        public double X
        {
            get { return (Location == null ? 0.0 : Location.X); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.X = (float)value;
            }
        }

        /// <summary>
        /// Convenience property (Used by the nhibernate criteria).
        /// </summary>
        public double Y
        {
            get { return (Location == null ? 0.0 : Location.Y); }
            set
            {
                if (Location == null)
                {
                    Location = new Vector2f();
                }
                Location.Y = (float)value;
            }
        }

        /// <summary>
        /// Number of performance readings that contributed to the results.
        /// </summary>
        [DataMember]
        public int SampleSize { get; set; }

        /// <summary>
        /// Minimum count.
        /// </summary>
        [DataMember]
        public double MinEntityCount { get; set; }

        /// <summary>
        /// Average count.
        /// </summary>
        [DataMember]
        public double AvgEntityCount { get; set; }

        /// <summary>
        /// Maximum count.
        /// </summary>
        [DataMember]
        public double MaxEntityCount { get; set; }
    } // FpsPerformanceDrawListInfo
}
