﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class ArchetypeStatBundles : IDtos<ArchetypeStatBundle>
    {
        [DataMember]

        [XmlArrayItem(typeof(DrawableArchetypeStatBundle))]
        [XmlArrayItem(typeof(FragmentArchetypeStatBundle))]
        [XmlArrayItem(typeof(InteriorArchetypeStatBundle))]
        [XmlArrayItem(typeof(StatedAnimArchetypeStatBundle))]
        public List<ArchetypeStatBundle> Items { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ArchetypeStatBundles()
        {
            Items = new List<ArchetypeStatBundle>();
        }
    } // ArchetypeStatBundles
}
