﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.ExportStats
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("MapExportStat")]
    [DataContract]
    public class MapExportStatDto : EntityDtoBase
    {
        /// <summary>
        /// When the export started.
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// When the export completed.
        /// </summary>
        [DataMember]
        public DateTime? End { get; set; }

        /// <summary>
        /// Duration of the export.
        /// </summary>
        [XmlIgnore()]
        public TimeSpan? Duration
        {
            get
            {
                return (End == null ? null : End - Start);
            }
        }

        /// <summary>
        /// Whether the export was successful.
        /// </summary>
        [DataMember]
        public bool? Success { get; set; }

        /// <summary>
        /// Name of the user who performed the export.
        /// </summary>
        [DataMember]
        public String Username { get; set; }

        /// <summary>
        /// Name of the machine the export was performed on.
        /// </summary>
        [DataMember]
        public String MachineName { get; set; }

        /// <summary>
        /// Type of export that the user performed (e.g. Container, Selected, Preview).
        /// </summary>
        [DataMember]
        public String ExportType { get; set; }

        /// <summary>
        /// Version of the tools the user was on.
        /// </summary>
        [DataMember]
        public float? ToolsVersion { get; set; }

        /// <summary>
        /// Whether the export was done with XGE enabled.
        /// </summary>
        [DataMember]
        public bool? XGEEnabled { get; set; }

        /// <summary>
        /// Whether the export was done using XGE in standalone mode.
        /// </summary>
        [DataMember]
        public bool? XGEStandalone { get; set; }

        /// <summary>
        /// The number of cpus that were forced.
        /// </summary>
        [DataMember]
        public int? XGEForceCPUCount { get; set; }

        /// <summary>
        /// Name of the sections that are part of this export.
        /// </summary>
        [DataMember]
        public List<String> SectionNames { get; set; }

        /// <summary>
        /// Platforms that the user had enabled in their config for this export.
        /// </summary>
        [DataMember]
        public List<RSG.Platform.Platform> Platforms { get; set; }

        /// <summary>
        /// Number of the export that this is since 3ds max was started.
        /// </summary>
        [DataMember]
        public uint ExportNumber { get; set; }

        /// <summary>
        /// Name of the processor the machine has.
        /// </summary>
        [DataMember]
        public String ProcessorName { get; set; }

        /// <summary>
        /// Number of cores the processor has.
        /// </summary>
        [DataMember]
        public uint ProcessorCores { get; set; }

        /// <summary>
        /// Amount of memory this machine has (in bytes).
        /// </summary>
        [DataMember]
        public ulong InstalledMemory { get; set; }

        /// <summary>
        /// Private bytes used at the start of the export.
        /// </summary>
        [DataMember]
        public long PrivateBytesStart { get; set; }

        /// <summary>
        /// Private bytes used at the end of the export.
        /// </summary>
        [DataMember]
        public long PrivateBytesEnd { get; set; }
    } // MapExportStatDto
}
