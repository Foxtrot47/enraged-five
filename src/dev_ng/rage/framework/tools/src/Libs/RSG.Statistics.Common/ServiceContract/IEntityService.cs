﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using System.ServiceModel;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// 
    /// </summary>
    [ServiceContract]
    [XmlSerializerFormat]
    public interface IEntityService : IGameAssetServiceBase<EntityDto, EntityDtos>
    {
    } // IEntityService
}
