﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry.PerUser
{
    /// <summary>
    /// Concrete version for wcf serialisation purposes.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserMissionCashStats : PerUserStat<List<MissionCashStat>>
    {
        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserMissionCashStats()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserMissionCashStats(String gamerHandle, ROSPlatform platform)
            : base(gamerHandle, platform, new List<MissionCashStat>())
        {
        }
        #endregion // Constructor(s)
    } // PerUserMissionCashStats
}
