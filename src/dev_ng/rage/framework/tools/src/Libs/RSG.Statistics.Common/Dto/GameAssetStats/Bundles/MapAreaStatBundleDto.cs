﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapAreaStatBundleDto : DtoBase
    {
        /// <summary>
        /// Map area export data path.
        /// </summary>
        [DataMember]
        public string ExportDataPath { get; set; }

        /// <summary>
        /// Map area processed data path.
        /// </summary>
        [DataMember]
        public string ProcessedDataPath { get; set; }
    } // MapAreaStatBundleDto
}
