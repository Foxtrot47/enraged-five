﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information regarding a single item that can be purchased in a shop.
    /// </summary>
    [DataContract]
    [Serializable]
    public class ShopItemStat : IComparable<ShopItemStat>
    {
        #region Properties
        /// <summary>
        /// Internal to stats server.  Used for grouping by shop.
        /// </summary>
        public uint ShopHash { get; set; }

        /// <summary>
        /// Hash of the item name.
        /// </summary>
        [DataMember]
        public uint ItemHash { get; set; }

        /// <summary>
        /// Name of the shop item.
        /// </summary>
        [DataMember]
        public String ItemName { get; set; }

        /// <summary>
        /// Category for the item.
        /// </summary>
        [DataMember]
        public String Category { get; set; }

        /// <summary>
        /// Sub-category for the item.
        /// </summary>
        [DataMember]
        public String SubCategory { get; set; }

        /// <summary>
        /// Color Id.
        /// </summary>
        [DataMember]
        public long? ColorId { get; set; }

        /// <summary>
        /// Number of times the item was purchased.
        /// </summary>
        [DataMember]
        public ulong TimesPurchased { get; set; }

        /// <summary>
        /// Number of unique gamers that purchased this item.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Total amount of money spent to purchase this item.
        /// </summary>
        [DataMember]
        public ulong TotalSpent { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation.
        /// </summary>
        public ShopItemStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public ShopItemStat(String name)
        {
            ItemName = name;
        }
        #endregion // Constructor(s)

        #region IComparable<ShopItemStat> Methods
        /// <summary>
        /// Compare this weapon to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ShopItemStat other)
        {
            if (other == null)
            {
                return 1;
            }

            return (ItemHash.CompareTo(other.ItemHash));
        }
        #endregion // IComparable<ShopItemStat> Interface
    } // ShopItemStat
}
