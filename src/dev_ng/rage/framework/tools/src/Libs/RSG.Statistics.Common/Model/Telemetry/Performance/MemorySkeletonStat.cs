﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry.Performance
{
    /// <summary>
    /// The Dto used to return results ( query ) for an associated query of memory skeleton telemetry stats
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MemorySkeletonStat
    {
        [DataMember]
        public String MissionName { get; set; }

        [DataMember]
        public int NumSamples { get; set; }

        [DataMember]
        public uint MinVehiclesPeak { get; set; }
        [DataMember]
        public double AvgVehiclesPeak { get; set; }
        [DataMember]
        public uint MaxVehiclesPeak { get; set; }

        [DataMember]
        public uint MinPedsPeak { get; set; }
        [DataMember]
        public double AvgPedsPeak { get; set; }
        [DataMember]
        public uint MaxPedsPeak { get; set; }

        [DataMember]
        public uint MinObjectsPeak { get; set; }
        [DataMember]
        public double AvgObjectsPeak { get; set; }
        [DataMember]
        public uint MaxObjectsPeak { get; set; }

        [DataMember]
        public uint MinBuildingsPeak { get; set; }
        [DataMember]
        public double AvgBuildingsPeak { get; set; }
        [DataMember]
        public uint MaxBuildingsPeak { get; set; }

        [DataMember]
        public uint MinTotalPeak { get; set; }
        [DataMember]
        public double AvgTotalPeak { get; set; }
        [DataMember]
        public uint MaxTotalPeak { get; set; }
    } // MemorySkeletonStat
}
