﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.Enums
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("MapDrawableLODLevel")]
    public class MapDrawableLodLevelDto : EnumDto<MapDrawableLODLevel>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MapDrawableLodLevelDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lodLevel"></param>
        public MapDrawableLodLevelDto(MapDrawableLODLevel lodLevel, String name)
            : base(lodLevel, name)
        {
        }
        #endregion // Constructor(s)
    } // MapDrawableLODLevelDto
}
