﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class FragmentArchetypeStatBundle : SimpleArchetypeStatBundle
    {
        #region Properties
        /// <summary>
        /// Number of fragments this archetype is made up of.
        /// </summary>
        [DataMember]
        public uint FragmentCount { get; set; }

        /// <summary>
        /// Whether this fragment is a piece of cloth.
        /// </summary>
        [DataMember]
        public bool IsCloth { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FragmentArchetypeStatBundle()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // FragmentArchetypeStatBundleDto
}
