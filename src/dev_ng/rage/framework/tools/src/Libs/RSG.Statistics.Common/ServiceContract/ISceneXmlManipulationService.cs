﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Service contract for manipulating scene xml data.
    /// </summary>
    [ServiceContract]
    public interface ISceneXmlManipulationService
    {
        /// <summary>
        /// Populates the database with a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="scene"></param>
        [OperationContract]
        void PopulateSceneXmlFile(String filename, SceneXmlDto.Scene scene);

        /// <summary>
        /// Deletes all data relating to a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        [OperationContract]
        void DeleteSceneXmlFile(String filename);
    } // ISceneXmlManipulationService
}
