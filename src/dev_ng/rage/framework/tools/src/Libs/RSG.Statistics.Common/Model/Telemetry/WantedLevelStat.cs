﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class WantedLevelStat
    {
        #region Properties
        /// <summary>
        /// Wanted level.
        /// </summary>
        [DataMember]
        public uint Level { get; set; }

        /// <summary>
        /// Number of times the player achieved this level.
        /// </summary>
        [DataMember]
        public ulong TimesAchieved { get; set; }

        /// <summary>
        /// Number of gamers that achieved this wanted level.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }
        #endregion // Properties
    } // WantedLevelStat
}
