﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Category that a freemode mission falls under.
    /// NOTE: The enum values are important in that they match the CategoryIds of the data in the SCS/Vertica dbs.
    /// </summary>
    public enum FreemodeMissionCategory
    {
        [FriendlyName("Uncategorised")]
        None = 0,

        [FriendlyName("Rockstar Created")]
        RockstarCreated = 1,

        [FriendlyName("Rockstar Created (Candidate)")]
        RockstarCreatedCandidate = 2,

        [FriendlyName("Rockstar Verified")]
        RockstarVerified = 3,

        [FriendlyName("Rockstar Verified (Candidate)")]
        RockstarVerifiedCandidate = 4
    } // FreemodeMissionCategory
}
