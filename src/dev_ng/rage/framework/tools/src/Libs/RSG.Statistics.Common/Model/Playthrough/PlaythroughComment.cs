﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class PlaythroughComment
    {
        #region Properties
        /// <summary>
        /// Internal stats server use.
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// Name of the user that made this comment.
        /// </summary>
        [DataMember]
        public String Username { get; set; }

        /// <summary>
        /// Comment itself.
        /// </summary>
        [DataMember]
        public String Comment { get; set; }

        /// <summary>
        /// When the player started the checkpoint this comment is associated with.
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// How long the player spent playing the checkpoint this comment is associated with.
        /// </summary>
        [DataMember]
        public uint Duration { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PlaythroughComment()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="comment"></param>
        public PlaythroughComment(String username, String comment)
        {
            Username = username;
            Comment = comment;
        }
        #endregion // Constructor(s)
    } // PlaythroughComment

    /// <summary>
    /// Comment associated with a mission.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class PlaythroughMissionComment : PlaythroughComment
    {
        #region Properties
        /// <summary>
        /// Name of the mission this comment is associated with.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }
        #endregion // Properties
    } // PlaythroughMissionComment

    /// <summary>
    /// Comment associated with a vehicle.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class PlaythroughVehicleComment : PlaythroughComment
    {
        #region Properties
        /// <summary>
        /// Name of the vehicle this comment is associated with.
        /// </summary>
        [DataMember]
        public String VehicleGameName { get; set; }

        /// <summary>
        /// Name of the vehicle this comment is associated with.
        /// </summary>
        [DataMember]
        public String VehicleFriendlyName { get; set; }
        #endregion // Properties
    } // PlaythroughVehicleComment

    /// <summary>
    /// Comment associated with a weapon.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class PlaythroughWeaponComment : PlaythroughComment
    {
        #region Properties
        /// <summary>
        /// Name of the weapon this comment is associated with.
        /// </summary>
        [DataMember]
        public String WeaponName { get; set; }
        #endregion // Properties
    } // PlaythroughWeaponComment
}
