﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Extensions;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class TransactionStat : StatBase
    {
        #region Enums
        /// <summary>
        /// 
        /// </summary>
        public enum TransactionHashType
        {
            None,
            ShopItem,
            Vehicle,
            Property,
            Weapon
        }
        #endregion // Enums

        #region Properties
        /// <summary>
        /// Value indicating the type of hash that this item has.
        /// </summary>
        [DataMember]
        public TransactionHashType HashType { get; set; }

        /// <summary>
        /// Total amount.
        /// </summary>
        [DataMember]
        public decimal Amount { get; set; }

        /// <summary>
        /// Total number of times this item was encountered.
        /// </summary>
        [DataMember]
        public decimal Count { get; set; }

        /// <summary>
        /// Distinct number of gamers.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Breakdown for the information under this stat.
        /// </summary>
        [DataMember]
        public IList<TransactionStat> SubStats { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TransactionStat()
            : base()
        {
            SubStats = new List<TransactionStat>();
        }

        /// <summary>
        /// 
        /// </summary>
        public TransactionStat(uint hash, TransactionHashType hashType)
            : base(hash)
        {
            SubStats = new List<TransactionStat>();
            HashType = hashType;
        }

        /// <summary>
        /// 
        /// </summary>
        public TransactionStat(String name, String friendlyName)
            : base(name, friendlyName)
        {
            SubStats = new List<TransactionStat>();
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        /// <param name="category"></param>
        public TransactionStat(SpendCategory category)
            : base(category.ToString(), category.GetFriendlyNameValue())
        {
            SubStats = new List<TransactionStat>();
        }
        #endregion // Constructor(s)
    } // EarnSpendStat
}
