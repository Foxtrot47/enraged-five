﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [KnownType(typeof(BasicDrawableArchetypeStatDto))]
    [KnownType(typeof(BasicFragmentArchetypeStatDto))]
    [KnownType(typeof(BasicInteriorArchetypeStatDto))]
    [KnownType(typeof(BasicStatedAnimArchetypeStatDto))]
    public abstract class BasicArchetypeStatDto : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected BasicArchetypeStatDto()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        protected BasicArchetypeStatDto(String name)
        {
            Name = name;
        }
    } // BasicArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BasicDrawableArchetypeStatDto : BasicArchetypeStatDto
    {
        /// <summary>
        /// 
        /// </summary>
        internal BasicDrawableArchetypeStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public BasicDrawableArchetypeStatDto(String name)
            : base(name)
        {
        }
    } // BasicDrawableArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BasicFragmentArchetypeStatDto : BasicArchetypeStatDto
    {
        /// <summary>
        /// 
        /// </summary>
        internal BasicFragmentArchetypeStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public BasicFragmentArchetypeStatDto(String name)
            : base(name)
        {
        }
    } // BasicFragmentArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BasicInteriorArchetypeStatDto : BasicArchetypeStatDto
    {
        /// <summary>
        /// 
        /// </summary>
        internal BasicInteriorArchetypeStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public BasicInteriorArchetypeStatDto(String name)
            : base(name)
        {
        }
    } // BasicInteriorArchetypeDto

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BasicStatedAnimArchetypeStatDto : BasicArchetypeStatDto
    {
        /// <summary>
        /// 
        /// </summary>
        internal BasicStatedAnimArchetypeStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public BasicStatedAnimArchetypeStatDto(String name)
            : base(name)
        {
        }
    } // BasicStatedAnimArchetypeDto
}
