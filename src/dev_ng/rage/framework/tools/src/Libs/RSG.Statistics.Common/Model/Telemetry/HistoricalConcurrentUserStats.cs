﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Collections;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Mapping of datetimes to concurrent user counts.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HistoricalConcurrentUserStats
    {
        #region Properties
        /// <summary>
        /// Unique gamer count for the entire time range.
        /// </summary>
        [DataMember]
        public long UniqueGamers { get; set; }

        /// <summary>
        /// Historical user counts.
        /// </summary>
        [DataMember]
        public IList<ConcurrentUserStat> Stats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HistoricalConcurrentUserStats()
        {
            Stats = new List<ConcurrentUserStat>();
        }
        #endregion // Constructor(s)
    } // HistoricalConcurrentUserStats

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class ConcurrentUserStat : Range<DateTime>
    {
        /// <summary>
        /// Value for this range.
        /// </summary>
        [DataMember]
        public ulong Value { get; set; }
    } // ConcurrentUserStat
}
