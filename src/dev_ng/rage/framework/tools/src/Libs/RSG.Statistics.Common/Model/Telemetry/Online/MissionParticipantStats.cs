﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.Statistics.Common.Model.UGC;

namespace RSG.Statistics.Common.Model.Telemetry.Online
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class MissionParticipantStats
    {
        #region Properties
        /// <summary>
        /// Mission information grouped by participant counts.
        /// </summary>
        [DataMember]
        public IList<MissionParticipantStat> ParticipantStats { get; set; }

        /// <summary>
        /// Common mission information.
        /// </summary>
        [DataMember]
        public IList<BasicMissionMetadata> MissionData { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionParticipantStats()
        {
            MissionData = new List<BasicMissionMetadata>();
            ParticipantStats = new List<MissionParticipantStat>();
        }
        #endregion // Constructor(s)
    } // MissionParticipantStats
}
