﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Common.Async;
using RSG.Statistics.Common.Dto.Reports;
using RSG.Statistics.Common.Report;

namespace RSG.Statistics.Common.ServiceContract
{
    /// <summary>
    /// Generic report generation service.
    /// </summary>
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(StatsReportCollection))]
    public interface IReportService : IQueryAsyncService
    {
        #region Reports
        /// <summary>
        /// Returns a list of all the available reports.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "All")]
        List<String> GetAvailableReportList();

        /// <summary>
        /// Returns the list of parameters required for a particular report.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{identifier}/Parameters")]
        List<ReportParameter> GetReportParameters(String identifier);

        /// <summary>
        /// Executes a particular report with the passed in parameters.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{identifier}/Parameters?forceRegen={forceRegen}")]
        object RunReport(String identifier, bool forceRegen, List<ReportParameter> parameters);

        /// <summary>
        /// Executes a particular report with the passed in parameters in a background thread.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{identifier}/ParametersAsync?forceRegen={forceRegen}")]
        [ServiceKnownType(typeof(AsyncCommandResult))]
        IAsyncCommandResult RunReportAsync(String identifier, bool forceRegen, List<ReportParameter> parameters);

        /// <summary>
        /// Returns the list of stylesheets that are available for the particular report.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{identifier}/Stylesheets")]
        List<String> GetReportStylesheetNames(String identifier);

        /// <summary>
        /// Returns a specific stylesheet for the requested report.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{identifier}/Stylesheets/{stylesheet}")]
        Stream GetReportStylesheet(String identifier, String stylesheet);
        #endregion // Reports

        #region Presets
        /// <summary>
        /// Returns a list of all presets that the database contains.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "Presets")]
        List<ReportPreset> GetPresets();

        /// <summary>
        /// Creates a new report preset.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Presets")]
        ReportPreset CreatePreset(ReportPreset preset);

        /// <summary>
        /// Deletes an existing report preset.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Presets/{id}")]
        void DeletePreset(String id);
        #endregion // Presets

        #region Hashing
        /// <summary>
        /// Computes the uint32 hash for the provided input strings.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ComputeHashes")]
        IDictionary<String, uint> ComputeHashes(IList<String> inputs);
        #endregion // Hashing
    } // IReportService
}
