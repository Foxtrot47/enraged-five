﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.Statistics.Common.Model.GameAssets;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class HourlyCashStat
    {
        #region Properties
        /// <summary>
        /// Hour of gameplay that these stats are for.
        /// </summary>
        [DataMember]
        public uint GameplayHour { get; set; }

        /// <summary>
        /// Real life time that the player reached this hour of gameplay.
        /// </summary>
        [DataMember]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Number of gamers that contributed to these stats.
        /// </summary>
        [DataMember]
        public ulong GamerCount { get; set; }

        /// <summary>
        /// Number of unique characters that contributed to these stats.
        /// </summary>
        [DataMember]
        public ulong CharacterCount { get; set; }

        /// <summary>
        /// Amount of cash spent during this hour.
        /// </summary>
        [DataMember]
        public decimal CashSpent { get; set; }

        /// <summary>
        /// Details of the cash the user spent.
        /// </summary>
        [DataMember]
        List<HourlyCashStatDetail> CashSpentDetails { get; set; }

        /// <summary>
        /// Amount of cash made during this hour.
        /// </summary>
        [DataMember]
        public decimal CashEarned { get; set; }

        /// <summary>
        /// Details of the cash the user earned.
        /// </summary>
        [DataMember]
        List<HourlyCashStatDetail> CashEarnedDetails { get; set; }

        /// <summary>
        /// Amount of cash purchased during this hour.
        /// </summary>
        [DataMember]
        public decimal CashPurchased { get; set; }

        /// <summary>
        /// Details of the cash the user purchased.
        /// </summary>
        [DataMember]
        List<HourlyCashStatDetail> CashPurchasedDetails { get; set; }

        /// <summary>
        /// Amount of cash given during this hour.
        /// </summary>
        [DataMember]
        public decimal CashGiven { get; set; }

        /// <summary>
        /// Details of the cash the user gave.
        /// </summary>
        [DataMember]
        List<HourlyCashStatDetail> CashGivenDetails { get; set; }

        /// <summary>
        /// Rank the player achieved during after this hour.
        /// </summary>
        [DataMember]
        public float Rank { get; set; }

        /// <summary>
        /// Total amount of XP the player had at the end of this hour.
        /// </summary>
        [DataMember]
        public decimal XPEarned { get; set; }

        /// <summary>
        /// Details of the xp earned.
        /// </summary>
        [DataMember]
        public List<HourlyCashStatDetail> XPEarnedDetails { get; set; }

        /// <summary>
        /// List of matches that the player took part in.
        /// </summary>
        [DataMember]
        public List<MatchStat> Matches { get; set; }

        /// <summary>
        /// List of unlocks that the user received during this hour.
        /// </summary>
        [DataMember]
        public List<Unlock> Unlocks { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public HourlyCashStat()
        {
            Matches = new List<MatchStat>();
            Unlocks = new List<Unlock>();
            CashSpentDetails = new List<HourlyCashStatDetail>();
            CashEarnedDetails = new List<HourlyCashStatDetail>();
            CashPurchasedDetails = new List<HourlyCashStatDetail>();
            CashGivenDetails = new List<HourlyCashStatDetail>();
            XPEarnedDetails = new List<HourlyCashStatDetail>();
        }

        /// <summary>
        /// 
        /// </summary>
        public HourlyCashStat(uint hour)
            : this()
        {
            GameplayHour = hour;
        }

        /// <summary>
        /// 
        /// </summary>
        public HourlyCashStat(uint hour, ulong gamerCount)
            : this()
        {
            GameplayHour = hour;
            GamerCount = gamerCount;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void AddCashSpentDetail(HourlyCashStatDetail detail)
        {
            CashSpent += detail.Amount;
            CashSpentDetails.Add(detail);
            if (Timestamp < detail.Timestamp)
            {
                Timestamp = detail.Timestamp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddCashEarnedDetail(HourlyCashStatDetail detail)
        {
            CashEarned += detail.Amount;
            CashEarnedDetails.Add(detail);
            if (Timestamp < detail.Timestamp)
            {
                Timestamp = detail.Timestamp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddCashPurchasedDetail(HourlyCashStatDetail detail)
        {
            CashPurchased += detail.Amount;
            CashPurchasedDetails.Add(detail);
            if (Timestamp < detail.Timestamp)
            {
                Timestamp = detail.Timestamp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddCashGivenDetail(HourlyCashStatDetail detail)
        {
            CashGiven += detail.Amount;
            CashGivenDetails.Add(detail);
            if (Timestamp < detail.Timestamp)
            {
                Timestamp = detail.Timestamp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void AddXPEarnedDetail(HourlyCashStatDetail detail)
        {
            XPEarned += detail.Amount;
            XPEarnedDetails.Add(detail);
            if (Timestamp < detail.Timestamp)
            {
                Timestamp = detail.Timestamp;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        public void AddMatchStat(MatchStat stat)
        {
            Matches.Add(stat);
            if (Timestamp < stat.Timestamp)
            {
                Timestamp = stat.Timestamp;
            }
        }
        #endregion // Public Methods
    } // HourlyCashStat
}
