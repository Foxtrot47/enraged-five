﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using RSG.Statistics.Common.Dto.Enums;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "Mission")]
    [Serializable]
    [XmlRoot("Mission")]
    public class MissionDto : GameAssetDtoBase, IComparable<MissionDto>
    {
        #region Properties
        /// <summary>
        /// Mission description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Name of the script associated with this mission.
        /// </summary>
        [DataMember]
        public string ScriptName { get; set; }

        /// <summary>
        /// Variant for this mission (if there is one).
        /// </summary>
        [DataMember]
        public uint Variant { get; set; }

        /// <summary>
        /// Mission id as entered in bugstar (e.g ARM1)
        /// </summary>
        [DataMember]
        public string MissionId { get; set; }

        /// <summary>
        /// Bugstar id of this mission.
        /// </summary>
        [DataMember]
        public long BugstarId { get; set; }

        /// <summary>
        /// Number of attempts we reckon a mission should take.
        /// </summary>
        [DataMember]
        public uint ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts we reckon a mission should take.
        /// </summary>
        [DataMember]
        public uint ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts we reckon a mission should take.
        /// </summary>
        [DataMember]
        public uint ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Alternative identifier (for when we are in a transitionary period).
        /// </summary>
        [DataMember]
        public string AltIdentifier { get; set; }

        /// <summary>
        /// Type of game this mission is for.
        /// </summary>
        [DataMember]
        public GameTypeDto GameType { get; set; }
        #endregion // Properties

        #region IComparable<MissionDto> Interface
        /// <summary>
        /// Compare this MissionDto to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(MissionDto other)
        {
            if (other == null || other.MissionId == null)
            {
                return 1;
            }
            if (MissionId == null)
            {
                return -1;
            }

            return (MissionId.CompareTo(other.MissionId));
        }
        #endregion // IComparable<GameType> Interface
    } // MissionDto
}
