﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;

namespace RSG.Statistics.Common.Model.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MiniGameVariant : FriendlyGameAsset
    {
        /// <summary>
        /// Type of match this mini-game is associated with.
        /// </summary>
        [DataMember]
        public MatchType MatchType { get; set; }

        /// <summary>
        /// Location of this mini-game.
        /// </summary>
        [DataMember]
        public Vector3f Location { get; set; }
    } // MiniGameVariant
}
