﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Matches the indices of the weather types defined in //depot/gta5/build/dev/common/data/levels/gta5/weather.xml.
    /// This is less than ideal really :/
    /// </summary>
    public enum WeatherType
    {
        [FriendlyName("Unknown")]
        [Browsable(false)]
        Unknown = -1,

        [FriendlyName("Extra Sunny")]
        ExtraSunny = 0,

        [FriendlyName("Clear")]
        Clear,
        
        [FriendlyName("Cloudy")]
        Clouds,

        [FriendlyName("Smoggy")]
        Smog,
        
        [FriendlyName("Foggy")]
        Foggy,
        
        [FriendlyName("Overcast")]
        Overcast,
        
        [FriendlyName("Rainy")]
        Rain,
        
        [FriendlyName("Thunder & Lightning")]
        Thunder,
        
        [FriendlyName("Clearing")]
        Clearing,

        [FriendlyName("Neutral")]
        Neutral,

        [FriendlyName("Snowing")]
        Snow,

        [FriendlyName("Blizzard")]
        Blizzard,

        [FriendlyName("Light Snowing")]
        SnowLight
    } // WeatherType
}
