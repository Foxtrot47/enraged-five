﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssetStats.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MapSectionAttributeStatDto : DtoBase
    {
        #region Properties
        /// <summary>
        /// Name of the attribute.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Type of attribute this is.
        /// </summary>
        [DataMember]
        public String Type { get; set; }

        /// <summary>
        /// Value of the attribute.
        /// </summary>
        [DataMember]
        public String Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation
        /// </summary>
        public MapSectionAttributeStatDto()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public MapSectionAttributeStatDto(String name, String type, String value)
            : base()
        {
            Name = name;
            Type = type;
            Value = value;
        }
        #endregion // Constructor(s)
    } // MapSectionAttributeStatDto
}
