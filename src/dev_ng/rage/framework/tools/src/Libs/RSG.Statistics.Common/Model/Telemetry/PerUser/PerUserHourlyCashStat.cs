﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Model.Telemetry.PerUser
{
    /// <summary>
    /// Concrete version for wcf serialisation purposes.
    /// </summary>
    [ReportType]
    [DataContract]
    [Serializable]
    public class PerUserHourlyCashStat : PerUserStat<List<HourlyCashStat>>
    {
        #region Properties
        /// <summary>
        /// Character slot
        /// </summary>
        [DataMember]
        public uint CharacterSlot { get; set; }

        /// <summary>
        /// Id of the character.
        /// </summary>
        [DataMember]
        public uint CharacterId { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// For serialisation purposes.
        /// </summary>
        public PerUserHourlyCashStat()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        public PerUserHourlyCashStat(uint accountId, ROSPlatform platform, uint characterSlot, uint characterId)
            : base(accountId, platform)
        {
            CharacterSlot = characterSlot;
            CharacterId = characterId;
        }
        #endregion // Constructor(s)
    } // PerUserHourlyCashStat
}
