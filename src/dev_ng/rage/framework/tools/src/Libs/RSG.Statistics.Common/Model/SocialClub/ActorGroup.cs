﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Model.SocialClub
{
    /// <summary>
    /// Model object for an actor group.
    /// </summary>
    public class ActorGroup : Actor
    {
        // Nothing to add here.
    } // ActorGroup
}
