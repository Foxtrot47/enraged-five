﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using RSG.Statistics.Common.Dto.Enums;

namespace RSG.Statistics.Common.Dto.GameAssets.Bundles
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public abstract class GameAssetPlatformStatBundleDtoBase : DtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public PlatformDto Platform { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint PhysicalSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public uint VirtualSize { get; set; }
    } // GameAssetPlatformStatBundleDtoBase
}
