﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.MapExport
{
    /// <summary>
    /// Section export age information.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class SectionAge
    {
        /// <summary>
        /// Name of the section.
        /// </summary>
        [DataMember]
        public String SectionName { get; set; }

        /// <summary>
        /// Most recent export time.
        /// </summary>
        [DataMember]
        public DateTime MostRecentExport { get; set; }
    } // SectionAge
}
