﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    [ReportType]
    public class PerMissionCharacterCashStats
    {
        #region Properties
        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Id of the mission.
        /// </summary>
        [DataMember]
        public String MissionId { get; set; }

        /// <summary>
        /// Cash held by Michael.
        /// </summary>
        [DataMember]
        public float Michael
        {
            get { return MichaelMedian; }
            set { }
        }

        /// <summary>
        /// Cash held by Franklin.
        /// </summary>
        [DataMember]
        public float Franklin
        {
            get { return FranklinMedian; }
            set { }
        }

        /// <summary>
        /// Cash held by Trevor.
        /// </summary>
        [DataMember]
        public float Trevor
        {
            get { return TrevorMedian; }
            set { }
        }

        /// <summary>
        /// Cash held by Michael.
        /// </summary>
        [DataMember]
        public float MichaelAvg { get; set; }

        /// <summary>
        /// Cash held by Franklin.
        /// </summary>
        [DataMember]
        public float FranklinAvg { get; set; }

        /// <summary>
        /// Cash held by Trevor.
        /// </summary>
        [DataMember]
        public float TrevorAvg { get; set; }

        /// <summary>
        /// Cash held by Michael.
        /// </summary>
        [DataMember]
        public float MichaelMedian { get; set; }

        /// <summary>
        /// Cash held by Franklin.
        /// </summary>
        [DataMember]
        public float FranklinMedian { get; set; }

        /// <summary>
        /// Cash held by Trevor.
        /// </summary>
        [DataMember]
        public float TrevorMedian { get; set; }
        #endregion // Properties
    } // PerMissionCharacterCashStats
}
