﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Mission;

namespace RSG.Statistics.Common.Model.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class PlaythroughMissionAttempt
    {
        #region Properties
        /// <summary>
        /// Session this mission attempt stat belongs to.
        /// </summary>
        public long SessionId { get; set; }

        /// <summary>
        /// Id of the mission this attempt info is for.
        /// </summary>
        public long MissionId { get; set; }

        /// <summary>
        /// Name of the mission.
        /// </summary>
        [DataMember]
        public String MissionName { get; set; }

        /// <summary>
        /// Total time it took for the player to complete this mission.
        /// </summary>
        [DataMember]
        public uint TimeToComplete { get; set; }

        /// <summary>
        /// Number of checkpoints this mission has.
        /// </summary>
        [DataMember]
        public uint CheckpointCount { get; set; }

        /// <summary>
        /// Whether the mission was successful or not.
        /// </summary>
        [DataMember]
        public MissionAttemptResult Result { get; set; }

        /// <summary>
        /// Checkpoints the user performed to complete this misison.
        /// </summary>
        [DataMember]
        public List<PlaythroughCheckpointAttempt> CheckpointAttempts { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PlaythroughMissionAttempt()
        {
            CheckpointAttempts = new List<PlaythroughCheckpointAttempt>();
        }
        #endregion // Constructor(s)
    } // PlaythroughMissionAttempt
}
