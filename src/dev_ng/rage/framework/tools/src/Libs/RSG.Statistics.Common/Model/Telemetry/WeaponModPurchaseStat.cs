﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Model.Telemetry
{
    /// <summary>
    /// Information regarding a weapon mods that can be purchased for weapons in a shop.
    /// </summary>
    [DataContract]
    [Serializable]
    [ReportType]
    public class WeaponModPurchaseStat
    {
        #region Properties
        /// <summary>
        /// Name of the weapon.
        /// </summary>
        [DataMember]
        public String WeaponName { get; set; }

        /// <summary>
        /// Name of the weapon.
        /// </summary>
        [DataMember]
        public String WeaponFriendlyName { get; set; }

        /// <summary>
        /// Hash for the weapon.
        /// </summary>
        [DataMember]
        public uint WeaponHash { get; set; }

        /// <summary>
        /// List of weapon mods that can be purchased for this weapon.
        /// </summary>
        [DataMember]
        public IList<WeaponModPurchaseSubStat> WeaponMods { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeaponModPurchaseStat()
        {
            WeaponMods = new List<WeaponModPurchaseSubStat>();
        }
        #endregion // Constructor(s)
    } // WeaponModPurchaseStat

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [Serializable]
    public class WeaponModPurchaseSubStat
    {
        /// <summary>
        /// Name of the weapon mod.
        /// </summary>
        [DataMember]
        public String WeaponModName { get; set; }

        /// <summary>
        /// Friendly name for the weapon mod.
        /// </summary>
        [DataMember]
        public String WeaponModFriendlyName { get; set; }

        /// <summary>
        /// Hash of the weapon mod shop item text label.
        /// </summary>
        [DataMember]
        public uint WeaponModHash { get; set; }

        /// <summary>
        /// Number of times the item was purchased.
        /// </summary>
        [DataMember]
        public ulong TimesPurchased { get; set; }

        /// <summary>
        /// Number of unique gamers that purchased this item.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Total amount of money spent to purchase this item.
        /// </summary>
        [DataMember]
        public ulong TotalSpent { get; set; }
    } // WeaponModPurchaseSubStat
}
