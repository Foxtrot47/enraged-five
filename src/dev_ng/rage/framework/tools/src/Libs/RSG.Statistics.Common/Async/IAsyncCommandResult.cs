﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Async
{
    /// <summary>
    /// Return object for a command that is running asynchronously.
    /// </summary>
    public interface IAsyncCommandResult
    {
        /// <summary>
        /// Unique identifier given to the task that is executing.
        /// This is what is used to query the state of the task.
        /// </summary>
        Guid TaskIdentifier { get; }

        /// <summary>
        /// Operation's current progress (0-1).
        /// </summary>
        double Progress { get; set; }

        /// <summary>
        /// Message containing information about what the server is currently doing.
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Gets the exception that was thrown by the asynchronous operation, if any
        /// </summary>
        string Exception { get; set; }

        /// <summary>
        /// Result of the operation.
        /// </summary>
        object Result { get; set; }
    } // IAsyncCommandResult
}
