﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace RSG.Statistics.Common.Dto.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [XmlRoot("RadioStation")]
    [DataContract]
    public class RadioStationDto : GameAssetDtoBase
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }
    } // RadioStationDto
}
