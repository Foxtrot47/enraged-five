﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common.Report;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.Common.Report
{
    /// <summary>
    /// 
    /// </summary>
    public class StatsReportCollection
    {
        #region Properties
        /// <summary>
        /// Private collection of IProcessor objects; created internally using
        /// MEF composition.  Predicate handles Processor container above.
        /// </summary>
        [ImportMany(typeof(IStatsReport))]
        public ICollection<IStatsReport> AllReports { get; private set; }

        /// <summary>
        /// Processor MEF composition container.
        /// </summary>
        private CompositionContainer Container { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        private StatsReportCollection()
        {
            AllReports = new List<IStatsReport>();

            // Use MEF to compose the list of reports.
            Log.Log__Profile("Stats Report Collection Initialisation");

            String compositionPath = null;
            if (s_server.UsingIIS)
            {
                compositionPath = System.Web.HttpRuntime.BinDirectory;
            }
            else
            {
                compositionPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
            Log.Log__Message("Using '{0}' as the composition directory.", compositionPath);

            AggregateCatalog catalog = new AggregateCatalog();
            DirectoryCatalog dirCatalog = new DirectoryCatalog(compositionPath);
            catalog.Catalogs.Add(dirCatalog);

            this.Container = new CompositionContainer(catalog);
            try
            {
                this.Container.ComposeParts(this);
            }
            catch (CompositionException ex)
            {
                Log.Log__Exception(ex, "Reports MEF Composition Exception");
                foreach (CompositionError error in ex.Errors)
                    Log.Log__Exception(error.Exception, error.Description);
            }

            // Make sure that some reports exist.
            if (!AllReports.Any())
            {
                Log.Log__Error("No report data providers resolved during composition!  Is the composition path correct?");
            }

            Log.Log__ProfileEnd();
        }
        #endregion // Constructor(s)

        #region Static Methods/Properties
        /// <summary>
        /// Singleton instance.
        /// </summary>
        public static StatsReportCollection Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new StatsReportCollection();
                }
                return s_instance;
            }
        }
        private static StatsReportCollection s_instance;

        /// <summary>
        /// Server configuration object.
        /// </summary>
        private static IStatsServer s_server;

        /// <summary>
        /// Initialises the server that we will be connecting to.
        /// </summary>
        /// <param name="server"></param>
        public static void Initialise(IStatsServer server)
        {
            s_server = server;
        }

        /// <summary>
        /// Method for data contract serialisation purposes.
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        private static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider provider)
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly ass in assemblies.Where(item => !item.IsDynamic))
            {
                Type[] types = ass.GetTypes();
                foreach (Type assType in types.Where(item => !item.IsAbstract))
                {
                    ReportTypeAttribute[] atts =
                        (ReportTypeAttribute[])assType.GetCustomAttributes(typeof(ReportTypeAttribute), false);
                    if (atts.Any())
                    {
                        yield return assType;
                        yield return typeof(List<>).MakeGenericType(assType);       // Kinda hacky, so that we can support lists of types.
                    }
                }
            }
        }
        #endregion // Static Methods/Properties
    } // ReportDataProviderCollection
}
