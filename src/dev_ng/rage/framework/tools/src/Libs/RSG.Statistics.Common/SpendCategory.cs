﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Attributes;

namespace RSG.Statistics.Common
{
    /// <summary>
    /// Categories for in-game expenditure.
    /// </summary>
    public enum SpendCategory
    {
        [FriendlyName("Shopping")]
        Shopping,

        [FriendlyName("Corona Purchases")]
        CoronaPurchases,

        [FriendlyName("Property Purchases")]
        PropertyPurchases,

        [FriendlyName("Vehicle Purchases")]
        VehiclePurchases,

        [FriendlyName("Food")]
        Food,

        [FriendlyName("Phone Contact Services")]
        PhoneContactServices,

        [FriendlyName("Match Entrance Fees")]
        MatchEntranceFees,

        [FriendlyName("Recreation & Services")]
        Entertainment,

        [FriendlyName("Penalties")]
        Penalties,

        [FriendlyName("Cash Gifts")]
        CashGifts,

        [FriendlyName("Charges")]
        Charges,

        [FriendlyName("Debug")]
        Debug,

        [FriendlyName("Betting")]
        Betting
    } // SpendCategory
}
