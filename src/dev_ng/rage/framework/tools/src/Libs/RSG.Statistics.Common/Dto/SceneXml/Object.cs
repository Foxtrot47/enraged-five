﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Base.Math;

namespace RSG.Statistics.Common.Dto.SceneXml
{
    /// <summary>
    /// Data transfer object for transferring scene xml object data between server and client.
    /// </summary>
    [DataContract]
    public class Object
    {
        #region Properties
        /// <summary>
        /// Unique guid for this object.
        /// </summary>
        [DataMember]
        public Guid Guid { get; set; }

        /// <summary>
        /// Name of this object.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Object DCC class.
        /// </summary>
        [DataMember]
        public String Class { get; set; }

        /// <summary>
        /// Object DCC superclass.
        /// </summary>
        [DataMember]
        public String SuperClass { get; set; }

        /// <summary>
        /// Attribute container class.
        /// </summary>
        [DataMember]
        public String AttributeClass { get; set; }

        /// <summary>
        /// Guid of the material applied to this object.
        /// </summary>
        [DataMember]
        public Guid MaterialGuid { get; set; }

        /// <summary>
        /// Number of polys in object (trimesh and collision support).
        /// </summary>
        [DataMember]
        public uint PolyCount { get; set; }

        /// <summary>
        /// Child object's that make up this one.
        /// </summary>
        [DataMember]
        public IList<Object> Children { get; set; }

        /// <summary>
        /// List of attributes this object has applied to it.
        /// </summary>
        [DataMember]
        public IList<Attribute> Attributes { get; set; }

        /// <summary>
        /// List of parameters this object has applied to it.
        /// </summary>
        [DataMember]
        public IList<Parameter> Parameters { get; set; }

        /// <summary>
        /// Object's world-space translation.
        /// </summary>
        [DataMember]
        public Vector3f ObjectTranslation { get; set; }

        /// <summary>
        /// Object's world-space rotation.
        /// </summary>
        [DataMember]
        public Quaternionf ObjectRotation { get; set; }

        /// <summary>
        /// Object's world space scale factor.
        /// </summary>
        [DataMember]
        public Vector3f ObjectScale { get; set; }

        /// <summary>
        /// Node world-space translation.
        /// </summary>
        [DataMember]
        public Vector3f NodeTranslation { get; set; }

        /// <summary>
        /// Node world-space rotation.
        /// </summary>
        [DataMember]
        public Quaternionf NodeRotation { get; set; }

        /// <summary>
        /// Node world space scale factor.
        /// </summary>
        [DataMember]
        public Vector3f NodeScale { get; set; }

        /// <summary>
        /// Local bounding box.
        /// </summary>
        [DataMember]
        public BoundingBox3f LocalBoundingBox { get; set; }

        /// <summary>
        /// World bounding box.
        /// </summary>
        [DataMember]
        public BoundingBox3f WorldBoundingBox { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Object()
        {
            Children = new List<Object>();
            Attributes = new List<Attribute>();
            Parameters = new List<Parameter>();
        }
        #endregion // Constructor(s)
    } // Object
}
