﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using File = RSG.Pipeline.Content.File;
using SIO = System.IO;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace RSG.Pipeline.Processor.Character
{

    /// <summary>
    /// Character Outfit PreProcess.  Handles merging multiple character 
    /// outfits into a single ped.zip.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    internal class OutfitPreProcess :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private const String DESCRIPTION = "Character Outfit PreProcessor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private const String LOG_CTX = "Outfit PreProcess";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public OutfitPreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            IDictionary<IProcess, IEnumerable<IContentNode>> dependencies;
            bool success = Prebuild(param, new IProcess[] {process}, processors, owner, out dependencies, out resultantProcesses);
            syncDependencies = dependencies.SelectMany(s => s.Value);

            return (success);
        }

        /// <summary>
        /// Prebuild stage; passing 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IEnumerable<IProcess> processes,
            IProcessorCollection processors,
            IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            param.Log.MessageCtx(LOG_CTX, string.Format("Character Outfit PreProcess Prebuild ({0}).", processes.Count().ToString("D")));
            syncDependencies = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            return PrebuildImpl(param, processes, out resultantProcesses);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param,
            IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools,
            out IEnumerable<XGE.ITask> tasks)
        {

            
            param.Log.MessageCtx(LOG_CTX, "OutfitPreProces Done.");
            tools = new XGE.ITool[0];
            tasks = new XGE.ITask[0];
            return true;
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Character Outfit PreProcess.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        private bool PrebuildImpl(IEngineParameters param, IEnumerable<IProcess> processes, out IEnumerable<IProcess> resultantProcesses)
        {
            // Validate our processes before continuing.
            if (!ValidateProcesses(param, processes))
            {
                param.Log.ErrorCtx(LOG_CTX, "Character Outfit processes failed validation.  See previous errors.  Aborting.");
                resultantProcesses = new IProcess[0];
                return (false);
            }

            // merge stuff from zip files to another zip file.
            foreach (IProcess process in processes)
            {
                Merge(param.Log, LOG_CTX, process.Inputs.OfType<File>().Select(f => f.AbsolutePath), ((File)process.Outputs.First()).AbsolutePath);
                process.State = ProcessState.Prebuilt;
                process.SetParameter(Constants.ProcessXGE_TaskName, "OutfitPreProcess");
            }

            resultantProcesses = processes;

            // if Merge(...) has encountered some errors, the log will have those, and then
            return !param.Log.HasErrors;
        }
        
        /// <summary>
        /// Validate processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        private bool ValidateProcesses(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            foreach (IProcess process in processes)
            {
                ValidateInputs(param, process);
                ValidateOutput(param, process);
            }

            if (param.Log.HasErrors)
                return false;

            return (true);

        }

        /// <summary>
        /// Validate Inputs for a given process
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// Inputs can be validated indepently from output
        /// Each steps narrows down the problem. The earlier it fails, the earlier it exits.
        /// Inputs and Output are validated separately to catch multiple errors in one attempt, if there are more than one.
        private static void ValidateInputs(IEngineParameters param, IProcess process)
        {
            Debug.Assert(process.Inputs.Any(), String.Format("Process has no inputs!"));
            if (!process.Inputs.Any())
            {
                param.Log.ErrorCtx(LOG_CTX, "Character Outfit process has no inputs!");
                return;
            }

            // inputs are .outfit.zip
            IEnumerable<File> files = process.Inputs.OfType<File>();
            if (files.Any(input => (!input.AbsolutePath.EndsWith(".outfit.zip")) && !input.AbsolutePath.EndsWith(".outfit_prop.zip")))
            {
                param.Log.ErrorCtx(LOG_CTX, "One of the inputs has a wrong extension.");
            }
        }

        /// <summary>
        /// Validate Outputs for a given process
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// Output can be validated independently from inputs.
        /// Each steps narrows down the problem. The earlier it fails, the earlier it exits.
        /// Inputs and Output are validated separately to catch multiple errors in one attempt, if there are more than one.
        private static void ValidateOutput(IEngineParameters param, IProcess process)
        {
            Debug.Assert(process.Outputs.Any(), "Process must specify at least one output.");
            if (!process.Outputs.Any())
            {
                param.Log.ErrorCtx(LOG_CTX, "Output missing for this process.");
                return;
            }

            // Validate outputs.
            Debug.Assert(1 == process.Outputs.Count(),
                String.Format("Character Outfit process has invalid number of outputs! ({0})", process.Outputs.Count()));
            if (1 != process.Outputs.Count())
            {
                param.Log.ErrorCtx(LOG_CTX, "Character Outfit process has invalid number of outputs! (found: {0}; expected {1})",
                    process.Outputs.Count().ToString("D"), "1");
                return;
            }
            Debug.Assert(process.Outputs.First() is Content.File,
                String.Format("Character Outfit process has output of wrong type {0}.", process.Outputs.First().GetType()));
            File output = process.Outputs.FirstOrDefault() as File;
            if (output == null)
            {
                param.Log.ErrorCtx(LOG_CTX, "Character Outfit process has output of wrong type: {0}.", process.Outputs.First().GetType());
                return;
            }
            Debug.Assert(output.AbsolutePath.EndsWith(".ped.zip", StringComparison.OrdinalIgnoreCase) && !output.AbsolutePath.EndsWith(".prop.zip", StringComparison.OrdinalIgnoreCase), 
                string.Format("Output's path has the wrong extension. {0}", Path.GetExtension(output.AbsolutePath)));
            if (!output.AbsolutePath.EndsWith(".ped.zip", StringComparison.OrdinalIgnoreCase) && !output.AbsolutePath.EndsWith(".prop.zip", StringComparison.OrdinalIgnoreCase))
            {
                param.Log.ErrorCtx(LOG_CTX, "Character Outfit process has output of wrong extension (expecting .ped.zip or .prop.zip): {0}.", output.AbsolutePath);
            }
        }

        ///// <summary>
        ///// Group processes by output .ped.zip.
        ///// </summary>
        ///// <param name="processes"></param>
        ///// <returns></returns>
        //private IDictionary<IContentNode, List<IProcess>> GroupProcessesByOutput(IEnumerable<IProcess> processes)
        //{
        //    IDictionary<IContentNode, List<IProcess>> groupedProcesses = new Dictionary<IContentNode, List<IProcess>>();
        //    foreach (IProcess process in processes)
        //    {
        //        IContentNode output = process.Outputs.First();
        //        if (!groupedProcesses.ContainsKey(output))
        //        {
        //            groupedProcesses.Add(output, new List<IProcess>());
        //        }

        //        groupedProcesses[output].Add(process);
        //    }
        //    return (groupedProcesses);
        //}


        private static void Merge(IUniversalLog log, string loggingContext, IEnumerable<string> sourceFiles, string outputFile)
        {
            // make sure we filter only the existing files (maybe we want to deal with that in Validate
            IEnumerable<string> existingFiles = sourceFiles.Where(SIO.File.Exists);
            
            // We compress an in memory stream, so that we perform everything in mem, and avoid reading and writing from/to disk
            using (Stream outputStream = new MemoryStream())
            {
                // Magic of ZipArchive we can new it in Update mode and it works anyway. Yay
                using (ZipArchive outputZip = new ZipArchive(outputStream, ZipArchiveMode.Update, true))
                {
                    foreach (string file in existingFiles)
                    {
                        using (ZipArchive inputArchive = ZipFile.OpenRead(file))
                        {
                            // Loop through all entries packing them into output.
                            foreach (ZipArchiveEntry inputEntry in inputArchive.Entries)
                            {
                                string outputEntryName = inputEntry.FullName;
                                ZipArchiveEntry outputEntry = outputZip.CreateEntry(outputEntryName);
                                using (Stream outputEntryStream = outputEntry.Open())
                                {
                                    using (Stream inputStream = inputEntry.Open())
                                    {
                                        inputStream.CopyTo(outputEntryStream);
                                    }
                                }
                            }
                        }
                    }
                }

                string outputPath = Path.GetDirectoryName(outputFile);
                if (outputPath == null)
                {
                    log.ToolErrorCtx(loggingContext, "Output path for specified output '{0}' is invalid. Aborting. File won't be created.", outputFile);
                    return;
                }

                // check for output path existence and create the directory if not
                Directory.CreateDirectory(outputPath);

                // Since output ZipArchive has been disposed, we can write the memorystream to a file
                using (FileStream fileStream = SIO.File.Create(outputFile))
                {
                    outputStream.Seek(0, SeekOrigin.Begin);
                    outputStream.CopyTo(fileStream);
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Character namespace
