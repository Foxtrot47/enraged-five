#
# RSG.Pipeline.Processor.Character.makefile
#
 
Project RSG.Pipeline.Processor.Character
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\Processors\
 
Files {
	Directory Properties {
			AssemblyInfo.cs
	}
	OutfitPreProcess.cs
	PedPreProcess.cs
}
 
ProjectReferences {
	..\..\..\..\..\..\..\..\..\3rdparty\dev\cli\DotNetZip\Zip Partial DLL\Zip Partial DLL.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.MaterialPresets\RSG.MaterialPresets.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Platform\RSG.Platform.csproj NOCOPYLOCAL
	..\..\..\..\..\base\tools\libs\RSG.Interop.Incredibuild\RSG.Interop.Incredibuild.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Content\RSG.Pipeline.Content.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj NOCOPYLOCAL
	..\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj NOCOPYLOCAL
}
References {
	System
	System.ComponentModel.Composition
	System.Core
	System.IO.Compression
	System.IO.Compression.FileSystem
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
