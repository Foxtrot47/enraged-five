﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.RESTServices.Statistics
{
    /// <summary>
    /// Helper class for keeping track of all the timings associated with a single map export.
    /// </summary>
    public class MapExportTimings
    {
        #region Properties
        /// <summary>
        /// Total time in milliseconds the export took.
        /// </summary>
        public double TotalTime { get; set; }

        /// <summary>
        /// Time it took to perform the map check step.
        /// </summary>
        public double MapCheckTime { get; set; }

        /// <summary>
        /// Dictionary containing section to time in milliseconds for the export portion of things.
        /// </summary>
        public IDictionary<string, double> SectionExportTimes { get; protected set; }

        /// <summary>
        /// List of sections that were built during the image build step.
        /// </summary>
        public string[] SectionsBuilt { get; set; }

        /// <summary>
        /// Time it took the build step to complete in milliseconds.
        /// </summary>
        public double BuildTime { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapExportTimings()
        {
            SectionExportTimes = new Dictionary<string, double>();
        }
        #endregion // Constructor(s)
    } // MapExportTimings
}
