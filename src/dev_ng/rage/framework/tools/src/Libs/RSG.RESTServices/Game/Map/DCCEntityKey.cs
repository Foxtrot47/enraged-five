﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.RESTServices.Game.Map
{
    public class DCCEntityKey
    {
        public DCCEntityKey(
            String mapContentName,
            String mapSectionName,
            String containerAttributeGuid,
            String nodeAttributeGuid,
            int nodeHandle)
        {
            MapContentName = mapContentName;
            MapSectionName = mapSectionName;
            ContainerAttributeGuid = containerAttributeGuid;
            NodeAttributeGuid = nodeAttributeGuid;
            NodeHandle = nodeHandle;
        }

        public string MapContentName { get; private set; }
        public string MapSectionName { get; private set; }
        public string ContainerAttributeGuid { get; private set; }
        public string NodeAttributeGuid { get; private set; }
        public int NodeHandle { get; private set; }
    }
}
