﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A command to update entity flags in the game
    /// </summary>
    internal class EntityFlagChange
    {
        public EntityFlagChange(uint guid,
                                bool dontRenderInShadows,
                                bool onlyRenderInShadows,
                                bool dontRenderInReflections,
                                bool onlyRenderInReflections,
                                bool dontRenderInWaterReflections,
                                bool onlyRenderInWaterReflections)
        {
            guid_ = guid;
            dontRenderInShadows_ = dontRenderInShadows;
            onlyRenderInShadows_ = onlyRenderInShadows;
            dontRenderInReflections_ = dontRenderInReflections;
            onlyRenderInReflections_ = onlyRenderInReflections;
            dontRenderInWaterReflections_ = dontRenderInWaterReflections;
            onlyRenderInWaterReflections_ = onlyRenderInWaterReflections;
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        public XElement ToXElement()
        {
            return
                new XElement("CEntityFlagChange",
                    new XElement("guid", new XAttribute("value", guid_)),
                    new XElement("bDontRenderInShadows", new XAttribute("value", dontRenderInShadows_)),
                    new XElement("bOnlyRenderInShadows", new XAttribute("value", onlyRenderInShadows_)),
                    new XElement("bDontRenderInReflections", new XAttribute("value", dontRenderInReflections_)),
                    new XElement("bOnlyRenderInReflections", new XAttribute("value", onlyRenderInReflections_)),
                    new XElement("bDontRenderInWaterReflections", new XAttribute("value", dontRenderInWaterReflections_)),
                    new XElement("bOnlyRenderInWaterReflections", new XAttribute("value", onlyRenderInWaterReflections_)));
        }

        private readonly uint guid_;
        private readonly bool dontRenderInShadows_;
        private readonly bool onlyRenderInShadows_;
        private readonly bool dontRenderInReflections_;
        private readonly bool onlyRenderInReflections_;
        private readonly bool dontRenderInWaterReflections_;
        private readonly bool onlyRenderInWaterReflections_;
    }
}
