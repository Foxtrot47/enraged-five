﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using RSG.Base.Net;
using RSG.Base.Math;

namespace RSG.RESTServices.Game.Cloth
{
    /// <summary>
    /// This class represents the service that allows cloth updates to be sent to the game.
    /// </summary>
    public class LiveClothEditingService
    {
        #region Constants
        /// <summary>
        /// Tolerance to use when comparing vectors
        /// </summary>
        private const float c_defaultTolerance = 0.0005f;
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Connection through which the rest queries will be run.
        /// </summary>
        private GameRestConnection m_restConnection;

        /// <summary>
        /// IP address of the game we will be sending our rest queries to.
        /// </summary>
        private string m_gameIPAddress;

        /// <summary>
        /// Mappings of original max vert indices to in game indices.
        /// </summary>
        private uint[] m_maxToGameVertMappings;

        /// <summary>
        /// Mappings of in game indices to max vert indices.
        /// </summary>
        private uint[] m_gameToMaxVertMappings;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Contains the type of the last exception to be thrown.
        /// </summary>
        public string LastExceptionType
        {
            get;
            private set;
        }

        /// <summary>
        /// Contains the message associated with the last exception to be thrown.
        /// </summary>
        public string LastExceptionMessage
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameIPAddress"></param>
        public LiveClothEditingService(string gameIPAddress)
        {
            m_gameIPAddress = gameIPAddress;
            m_restConnection = new GameRestConnection();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public void ClearVertexMapping()
        {
            m_maxToGameVertMappings = null;
            m_gameToMaxVertMappings = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void UpdateVertexMapping(Vector3f[] exportVerts)
        {
            try
            {
                // Create/retrieve the mappings from max -> export -> game
                uint[] maxToExportVertMappings = GetMaxToExportMapping(exportVerts);
                if (maxToExportVertMappings.Length != exportVerts.Length)
                {
                    throw new ApplicationException(String.Format("Max to export vert mapping is incorrect length: {0}.", maxToExportVertMappings.Length));
                }

                // Generate the mappings
                m_maxToGameVertMappings = new uint[exportVerts.Count()];
                m_gameToMaxVertMappings = new uint[exportVerts.Count()];

                for (uint i = 0; i < m_maxToGameVertMappings.Length;  ++i)
                {
                    uint maxToGame = maxToExportVertMappings[i];

                    m_maxToGameVertMappings[i] = maxToGame;
                    m_gameToMaxVertMappings[maxToGame] = i;
                }
            }
            catch (System.Exception ex)
            {
                LastExceptionType = ex.GetType().Name;
                LastExceptionMessage = ex.Message;
                throw ex;
            }
        }

        #region Pin Radii
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float[] GetAllPinRadii(uint lodLevel)
        {
            try
            {
                float[] pinRadii = null;

                // Create and run the query
                GameRestQuery query = new GameRestQuery(m_gameIPAddress, String.Format("Physics/Cloth/Bridge/PinRadius/{0}", lodLevel));
                using (Stream result = m_restConnection.RunQuery(query))
                {
                    if (result != null)
                    {
                        // Get the in game indices from the root document's value.
                        XDocument xmlDoc = XDocument.Load(result);
                        float[] gamePinRadii = xmlDoc.Root.Value.Split(new char[] { '\n', '\t' }, StringSplitOptions.RemoveEmptyEntries).Select(item => Single.Parse(item.Trim())).ToArray();

                        pinRadii = new float[gamePinRadii.Length];
                        for (uint i = 0; i < gamePinRadii.Length; ++i)
                        {
                            pinRadii[MapGameIndexToMaxIndex(i)] = gamePinRadii[i];
                        }
                    }
                }

                return pinRadii;
            }
            catch (System.Exception ex)
            {
                LastExceptionType = ex.GetType().Name;
                LastExceptionMessage = ex.Message;
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxVertIndex"></param>
        /// <returns></returns>
        public float GetPinRadii(uint maxVertIndex, uint lodLevel)
        {
            try
            {
                float pinRadius = 0.0f;

                // Create and run the query
                GameRestQuery query = new GameRestQuery(m_gameIPAddress, String.Format("Physics/Cloth/Bridge/PinRadius/{0}/{1}", lodLevel, MapMaxIndexToGameIndex(maxVertIndex)));
                using (Stream result = m_restConnection.RunQuery(query))
                {
                    if (result != null)
                    {
                        // Get the in game indices from the root document's value.
                        XDocument xmlDoc = XDocument.Load(result);
                        XAttribute valueAtt = xmlDoc.Root.Attribute("value");

                        if (valueAtt != null)
                        {
                            pinRadius = Single.Parse(valueAtt.Value.Trim());
                        }
                    }
                }

                return pinRadius;
            }
            catch (System.Exception ex)
            {
                LastExceptionType = ex.GetType().Name;
                LastExceptionMessage = ex.Message;
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public void UpdateAllPinRadii(uint lodLevel, float[] values)
        {
            try
            {
                // Convert the ordering of the values to what the game expects
                float[] gameValues = new float[values.Length];
                for (uint i = 0; i < m_maxToGameVertMappings.Length; ++i)
                {
                    gameValues[MapMaxIndexToGameIndex(i)] = values[i];
                }

                // Create the query
                XDocument document = new XDocument(
                    new XElement("PinRadius", new XAttribute("content", "float_array"), String.Join("\n", gameValues)));

                GameRestQuery query =
                    new GameRestQuery(m_gameIPAddress,
                                      String.Format("Physics/Cloth/Bridge/PinRadius/{0}", lodLevel),
                                      RequestMethod.PUT,
                                      document.ToString());
                m_restConnection.RunQuery(query);
            }
            catch (System.Exception ex)
            {
                LastExceptionType = ex.GetType().Name;
                LastExceptionMessage = ex.Message;
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxVertIndex"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void UpdatePinRadius(uint maxVertIndex, uint lodLevel, float value)
        {
            try
            {
                // Create the query
                GameRestQuery query =
                    new GameRestQuery(m_gameIPAddress,
                                      String.Format("Physics/Cloth/Bridge/PinRadius/{0}/{1}", lodLevel, MapMaxIndexToGameIndex(maxVertIndex)),
                                      RequestMethod.PUT,
                                      value.ToString());
                m_restConnection.RunQuery(query);
            }
            catch (System.Exception ex)
            {
                LastExceptionType = ex.GetType().Name;
                LastExceptionMessage = ex.Message;
                throw ex;
            }
        }
        #endregion // Pin Radii
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxExportedVerts"></param>
        /// <returns></returns>
        private uint[] GetMaxToExportMapping(Vector3f[] maxExportedVerts)
        {
            uint[] mapping = new uint[0];

            // Create and run the query
            GameRestQuery query = new GameRestQuery(m_gameIPAddress, "Physics/Cloth/BindPose/BindPose");
            using (Stream result = m_restConnection.RunQuery(query))
            {
                if (result != null)
                {
                    // Get the in game indices from the root document's value.
                    XDocument xmlDoc = XDocument.Load(result);
                    IEnumerable<float> rawBindPoseVerts = xmlDoc.Root.Value.Split(new char[] { '\n', '\t' }, StringSplitOptions.RemoveEmptyEntries).Select(item => Single.Parse(item.Trim()));

                    if (rawBindPoseVerts.Count() % 3 != 0)
                    {
                        throw new ArgumentException("Number of bind pose verts retrieved from the game isn't a multiple of 3.");
                    }

                    // Convert the bind pose verts to vector3's
                    IList<Vector3f> bindPoseVerts = new List<Vector3f>();
                    for (int i = 0; i < rawBindPoseVerts.Count(); i += 3)
                    {
                        bindPoseVerts.Add(new Vector3f(rawBindPoseVerts.ElementAt(i), rawBindPoseVerts.ElementAt(i + 1), rawBindPoseVerts.ElementAt(i + 2)));
                    }

                    // Generate the mappings
                    mapping = new uint[bindPoseVerts.Count()];

                    for (int exportIdx = 0; exportIdx < maxExportedVerts.Length; ++exportIdx)
                    {
                        bool matched = false;

                        for (int gameIdx = 0; gameIdx < bindPoseVerts.Count; ++gameIdx)
                        {
                            if (AreVectorsEqual(maxExportedVerts[exportIdx], bindPoseVerts[gameIdx], c_defaultTolerance))
                            {
                                mapping[exportIdx] = (uint)gameIdx;
                                matched = true;
                                break;
                            }
                        }

                        if (!matched)
                        {
                            throw new ArgumentException(String.Format("Unable to match export vert index {0} against the bind pose verts supplied by the game.", exportIdx));
                        }
                    }
                }
            }

            return mapping;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private uint[] GetExportToGameMapping()
        {
            uint[] mapping = new uint[0];

            // Create and run the query
            GameRestQuery query = new GameRestQuery(m_gameIPAddress, "Physics/Cloth/Bridge/ClothDisplayMap0");
            using (Stream result = m_restConnection.RunQuery(query))
            {
                if (result != null)
                {
                    // Get the in game indices from the root document's value.
                    XDocument xmlDoc = XDocument.Load(result);
                    mapping = xmlDoc.Root.Value.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Select(item => UInt32.Parse(item.Trim())).ToArray();
                }
            }

            return mapping;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vecA"></param>
        /// <param name="vecB"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        private bool AreVectorsEqual(Vector3f vecA, Vector3f vecB, float tolerance)
        {
            return (Math.Abs(vecA.X - vecB.X) < tolerance && Math.Abs(vecA.Y - vecB.Y) < tolerance && Math.Abs(vecA.Z - vecB.Z) < tolerance);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="maxVertIdx"></param>
        /// <returns></returns>
        public uint MapMaxIndexToGameIndex(uint maxVertIdx)
        {
            if (m_maxToGameVertMappings == null)
            {
                throw new ArgumentNullException("Vert mappings array hasn't been initiailised.  Have you forgotten a call to UpdateVertexMapping()?");
            }
            if (maxVertIdx > m_maxToGameVertMappings.Length)
            {
                throw new ArgumentOutOfRangeException(String.Format("Requested max vertex index is larger than the size of the vert mappings array. Index: {0}, Vert Mappings Size: {1}.", maxVertIdx, m_maxToGameVertMappings.Length));
            }

            return m_maxToGameVertMappings[maxVertIdx];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameVertIdx"></param>
        /// <returns></returns>
        private uint MapGameIndexToMaxIndex(uint gameVertIdx)
        {
            if (m_gameToMaxVertMappings == null)
            {
                throw new ArgumentNullException("Vert mappings array hasn't been initiailised.  Have you forgotten a call to UpdateVertexMapping()?");
            }
            if (gameVertIdx > m_gameToMaxVertMappings.Length)
            {
                throw new ArgumentOutOfRangeException(String.Format("Requested max vertex index is larger than the size of the vert mappings array. Index: {0}, Vert Mappings Size: {1}.", gameVertIdx, m_gameToMaxVertMappings.Length));
            }

            return m_gameToMaxVertMappings[gameVertIdx];
        }
        #endregion // Private Methods
    } // LiveClothEditingService
}
