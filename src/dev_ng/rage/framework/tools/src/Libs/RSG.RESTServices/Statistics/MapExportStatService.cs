﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.ExportStats;
using System.Diagnostics;
using System.Xml.Serialization;
using System.IO;
using RSG.Statistics.Common.Config;
using System.ServiceModel;
using System.ServiceModel.Description;
using RSG.Base.Configuration;
using System.Web.UI;
using RSG.Base.Logging;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Dto.Enums;
using RSG.Base.Reflection;

namespace RSG.RESTServices.Statistics
{
    /// <summary>
    /// 
    /// </summary>
    public class MapExportStatService : IDisposable
    {
        #region Static Data
        /// <summary>
        /// Statistics server config.
        /// </summary>
        private static IStatisticsConfig StatsConfig { get; set; }

        /// <summary>
        /// Game config.
        /// </summary>
        private static IConfig GameConfig { get; set; }

        /// <summary>
        /// Number of the current export that is taking place.
        /// Incremented with each export.
        /// </summary>
        private static uint s_exportNumber = 0;
        #endregion // Static Data

        #region Properties
        /// <summary>
        /// Timing information related to the previous export.
        /// </summary>
        public MapExportTimings Timings { get; private set; }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// WCF client for communicating with the stats server.
        /// </summary>
        private MapExportStatClient ExportStatClient
        {
            get
            {
                if (m_exportStatClient == null)
                {
                    EndpointAddress address = new EndpointAddress(String.Format("http://{0}:{1}/StatisticsService/MapExportStats", StatsConfig.DefaultServer.Address, StatsConfig.DefaultServer.HttpPort));
                    WebHttpBinding binding = new WebHttpBinding(WebHttpSecurityMode.None);
                    m_exportStatClient = new MapExportStatClient(binding, address);
                    m_exportStatClient.Endpoint.Behaviors.Add(new WebHttpBehavior());
                }
                return m_exportStatClient;
            }
        }
        private MapExportStatClient m_exportStatClient;

        /// <summary>
        /// Dto object relating to the map export that is currently in progress.
        /// </summary>
        private MapExportStatDto CurrentExportStatDto { get; set; }

        /// <summary>
        /// Dto object relating to the current map check that is in progress.
        /// </summary>
        private MapCheckStatDto CurrentMapCheckStatDto { get; set; }

        /// <summary>
        /// Dto object relating to the map section export that is currently in progress.
        /// </summary>
        private SectionExportStatDto CurrentSectionExportStatDto { get; set; }

        /// <summary>
        /// Dto object relating to the sub stat of the section export that is in progress.
        /// </summary>
        private SectionExportSubStatDto CurrentSectionExportSubStatDto { get; set; }

        /// <summary>
        /// Dto object relating to the map image build that is currently in progress.
        /// </summary>
        private ImageBuildStatDto CurrentImageBuildStatDto { get; set; }

        /// <summary>
        /// Name of the section currently being exported.
        /// </summary>
        private string CurrentlyExporting { get; set; }

        /// <summary>
        /// Name of the sections that are currently being built.
        /// </summary>
        private string[] CurrentlyBuilding { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor
        /// </summary>
        static MapExportStatService()
        {
            try
            {
                StatsConfig = new StatisticsConfig();
                GameConfig = ConfigFactory.CreateConfig();
            }
            catch (ConfigurationVersionException ex)
            {
                Log.Log__Exception(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.", ex.ActualVersion, ex.ExpectedVersion);
                Environment.Exit(1);
            }
            catch (ConfigurationException ex)
            {
                Log.Log__Exception(ex, "Configuration parse error.");
                Environment.Exit(1);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Call when the user initiates a new export in max.
        /// </summary>
        /// <param name="exportType"></param>
        public void MapExportStarted(String exportType, String[] sectionNames)
        {
            SystemInfo sysInfo = new SystemInfo();
            Process proc = Process.GetCurrentProcess();

            // Populate the relevant data
            MapExportStatDto dto = new MapExportStatDto();
            dto.Start = DateTime.Now;
            dto.Username = Environment.UserName;
            dto.MachineName = Environment.MachineName;
            dto.ExportType = exportType;

            if (GameConfig.Version.LocalVersion.Major == 0)
            {
                dto.ToolsVersion = null;
            }
            else
            {
                dto.ToolsVersion = Single.Parse(String.Format("{0}.{1}", GameConfig.Version.LocalVersion.Major, GameConfig.Version.LocalVersion.Minor));
            }
            dto.XGEEnabled = RSG.Interop.Incredibuild.Incredibuild.IsAgentEnabled;
            dto.XGEStandalone = RSG.Interop.Incredibuild.Incredibuild.IsAgentStandalone;
            dto.XGEForceCPUCount = RSG.Interop.Incredibuild.Incredibuild.AgentForceCPUCount;
            dto.SectionNames = sectionNames.ToList();
            dto.Platforms = GameConfig.Project.DefaultBranch.Targets.Where(item => item.Value.Enabled).Select(item => item.Key).ToList();
            dto.ExportNumber = s_exportNumber++;
            dto.ProcessorName = sysInfo.Processor.Name;
            dto.ProcessorCores = sysInfo.Processor.NumberOfCores;
            dto.InstalledMemory = sysInfo.Memory.Capacity;
            dto.PrivateBytesStart = proc.PrivateMemorySize64;

            // Create the stat on the server.
            CurrentExportStatDto = ExportStatClient.CreateMapExportStat(dto);

            // Create a new timings object for this export
            Timings = new MapExportTimings();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        public void MapExportCompleted(bool success)
        {
            // Ensure that a map export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            Process proc = Process.GetCurrentProcess();

            // Update the stat and save it to the stats server
            CurrentExportStatDto.End = DateTime.Now;
            CurrentExportStatDto.Success = success;
            CurrentExportStatDto.PrivateBytesEnd = proc.PrivateMemorySize64;
            ExportStatClient.UpdateMapExportStat(CurrentExportStatDto.Id.ToString(), CurrentExportStatDto);

            TimeSpan time = (TimeSpan)(CurrentExportStatDto.End - CurrentExportStatDto.Start);
            Timings.TotalTime = time.TotalMilliseconds;
            CurrentExportStatDto = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public void MapCheckStarted()
        {
            // Ensure that a map export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Populate the relevant data
            MapCheckStatDto dto = new MapCheckStatDto();
            dto.Start = DateTime.Now;

            // Create the stat on the server.
            CurrentMapCheckStatDto = ExportStatClient.CreateMapCheckStat(CurrentExportStatDto.Id.ToString(), dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        public void MapCheckCompleted(bool success)
        {
            // Ensure that a map export and map section export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Ensure that a map section export is currently in progress.
            Debug.Assert(CurrentMapCheckStatDto != null, "No map section export is currently in progress.");
            if (CurrentMapCheckStatDto == null)
            {
                throw new ApplicationException("No map section export is currently in progress.");
            }

            // Update the stat and save it to the stats server
            CurrentMapCheckStatDto.End = DateTime.Now;
            CurrentMapCheckStatDto.Success = success;
            ExportStatClient.UpdateMapCheckStat(CurrentExportStatDto.Id.ToString(), CurrentMapCheckStatDto.Id.ToString(), CurrentMapCheckStatDto);

            TimeSpan time = (TimeSpan)(CurrentMapCheckStatDto.End - CurrentMapCheckStatDto.Start);
            Timings.MapCheckTime = time.TotalMilliseconds;
            CurrentMapCheckStatDto = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SectionExportStarted(string mapSection)
        {
            // Ensure that a map export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Populate the relevant data
            SectionExportStatDto dto = new SectionExportStatDto();
            dto.MapSectionIdentifier = RSG.ManagedRage.StringHashUtil.atStringHash(mapSection, 0).ToString();
            dto.Start = DateTime.Now;

            // Create the stat on the server.
            CurrentSectionExportStatDto = ExportStatClient.CreateSectionExportStat(CurrentExportStatDto.Id.ToString(), dto);
            CurrentlyExporting = mapSection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        public void SectionExportCompleted(bool success)
        {
            // Ensure that a map export and map section export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Ensure that a map section export is currently in progress.
            Debug.Assert(CurrentSectionExportStatDto != null, "No map section export is currently in progress.");
            if (CurrentSectionExportStatDto == null)
            {
                throw new ApplicationException("No map section export is currently in progress.");
            }

            // Update the stat and save it to the stats server
            CurrentSectionExportStatDto.End = DateTime.Now;
            CurrentSectionExportStatDto.Success = success;
            ExportStatClient.UpdateSectionExportStat(CurrentExportStatDto.Id.ToString(), CurrentSectionExportStatDto.Id.ToString(), CurrentSectionExportStatDto);

            TimeSpan time = (TimeSpan)(CurrentSectionExportStatDto.End - CurrentSectionExportStatDto.Start);
            Timings.SectionExportTimes.Add(CurrentlyExporting, time.TotalMilliseconds);
            CurrentlyExporting = null;
            CurrentSectionExportStatDto = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public void SectionExportSubTaskStarted(SectionExportSubTask task)
        {
            // Ensure that a map export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Ensure that a section export is in progress.
            Debug.Assert(CurrentSectionExportStatDto != null, "No section export is currently in progress.");
            if (CurrentSectionExportStatDto == null)
            {
                throw new ApplicationException("No section export is currently in progress.");
            }

            // Populate the relevant data
            SectionExportSubStatDto dto = new SectionExportSubStatDto();
            dto.ExportSubTask = new SectionExportSubTaskDto(task, task.ToString());
            dto.Start = DateTime.Now;

            // Create the stat on the server.
            CurrentSectionExportSubStatDto = ExportStatClient.CreateSectionExportSubStat(CurrentExportStatDto.Id.ToString(), CurrentSectionExportStatDto.Id.ToString(), dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        public void SectionExportSubTaskCompleted(bool success)
        {
            // Ensure that a map export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Ensure that a section export is in progress.
            Debug.Assert(CurrentSectionExportStatDto != null, "No section export is currently in progress.");
            if (CurrentSectionExportStatDto == null)
            {
                throw new ApplicationException("No section export is currently in progress.");
            }

            // Ensure that a sub task of a section export is currently in progress.
            Debug.Assert(CurrentSectionExportSubStatDto != null, "No section export sub task is currently in progress.");
            if (CurrentSectionExportSubStatDto == null)
            {
                throw new ApplicationException("No section export sub task is currently in progress.");
            }

            // Update the stat and save it to the stats server
            CurrentSectionExportSubStatDto.End = DateTime.Now;
            CurrentSectionExportSubStatDto.Success = success;
            ExportStatClient.UpdateSectionExportSubStat(CurrentExportStatDto.Id.ToString(), CurrentSectionExportStatDto.Id.ToString(), CurrentSectionExportSubStatDto.Id.ToString(), CurrentSectionExportSubStatDto);
            CurrentSectionExportSubStatDto = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public void ImageBuildStarted(string[] mapSections)
        {
            // Ensure that a map export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Populate the relevant data
            ImageBuildStatDto dto = new ImageBuildStatDto();
            dto.Start = DateTime.Now;
            dto.MapSectionIdentifiers = new List<string>();
            dto.MapSectionIdentifiers.AddRange(mapSections.Select(item => RSG.ManagedRage.StringHashUtil.atStringHash(item, 0).ToString()));

            // Create the stat on the server.
            CurrentImageBuildStatDto = ExportStatClient.CreateImageBuildStat(CurrentExportStatDto.Id.ToString(), dto);
            CurrentlyBuilding = mapSections;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="success"></param>
        public void ImageBuildCompleted(bool success)
        {
            // Ensure that a map export and map section export is currently in progress.
            Debug.Assert(CurrentExportStatDto != null, "No map export is currently in progress.");
            if (CurrentExportStatDto == null)
            {
                throw new ApplicationException("No map export is currently in progress.");
            }

            // Ensure that a map section export is currently in progress.
            Debug.Assert(CurrentImageBuildStatDto != null, "No map image build is currently in progress.");
            if (CurrentImageBuildStatDto == null)
            {
                throw new ApplicationException("No map image build is currently in progress.");
            }

            // Update the stat and save it to the stats server
            CurrentImageBuildStatDto.End = DateTime.Now;
            CurrentImageBuildStatDto.Success = success;
            ExportStatClient.UpdateImageBuildStat(CurrentExportStatDto.Id.ToString(), CurrentImageBuildStatDto.Id.ToString(), CurrentImageBuildStatDto);

            TimeSpan time = (TimeSpan)(CurrentImageBuildStatDto.End - CurrentImageBuildStatDto.Start);
            Timings.BuildTime = time.TotalMilliseconds;
            Timings.SectionsBuilt = CurrentlyBuilding;
            CurrentlyBuilding = null;
            CurrentImageBuildStatDto = null;
        }
        #endregion // Public Methods

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (m_exportStatClient != null)
            {
                m_exportStatClient.Close();
            }
        }
        #endregion // IDisposable Implementation
    } // MapExportStatService
}
