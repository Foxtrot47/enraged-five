﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;

using RSG.Base.Math;
using RSG.Pipeline.Core;

namespace RSG.RESTServices.Game.Map
{
    public class GameToMaxLiveEditingServiceClient
    {
        public GameToMaxLiveEditingServiceClient(String gameIPAddress)
        {
            gameIPAddress_ = gameIPAddress;
            nodeHandleKeyMap_ = new Dictionary<int, EntityKey>();
        }

        public void RegisterMaxNode(DCCEntityKey dccEntityKey, IContentNode contentNode)
        {
            if (!nodeHandleKeyMap_.ContainsKey(dccEntityKey.NodeHandle))
            {
                EntityKey entityKey = MaxGameKeyTranslator.DCCEntityKeyToEntityKey(dccEntityKey, contentNode);
                nodeHandleKeyMap_.Add(dccEntityKey.NodeHandle, entityKey);
            }
        }

        public EntityLocalToWorldTransformOverride[] GetUpdates()
        {
            List<EntityLocalToWorldTransformOverride> overrides = new List<EntityLocalToWorldTransformOverride>();

            XDocument runtimeRESTDocument = GetRuntimeUpdates();
            if (runtimeRESTDocument != null)
            {
                XElement entityMatrixOverrideElement = runtimeRESTDocument.Root.Element("entityMatrixOverride");
                if(entityMatrixOverrideElement != null)
                {
                    foreach(XElement itemElement in entityMatrixOverrideElement.Elements("Item"))
                    {
                        uint guid = UInt32.Parse(itemElement.Element("guid").Attribute("value").Value);
                        String imapName = itemElement.Element("imapName").Value;
                        EntityKey entityKey = nodeHandleKeyMap_.Values.FirstOrDefault(ek => ek.Guid == guid && imapName.StartsWith(ek.MapSectionName));
                        if (entityKey != null)
                        {
                            int nodeHandle = nodeHandleKeyMap_.First(kvp => kvp.Value == entityKey).Key;
                            String localToWorldTransformText = itemElement.Element("matrix").Value;
                            Matrix34f localToWorldTransform;
                            if (CreateMatrixFromText(localToWorldTransformText, out localToWorldTransform))
                            {
                                // decode the matrix
                                EntityLocalToWorldTransformOverride transformOverride = new EntityLocalToWorldTransformOverride(nodeHandle, localToWorldTransform);
                                overrides.Add(transformOverride);
                            }
                        }
                    }
                }
            }

            return overrides.ToArray();
        }

        private static bool CreateMatrixFromText(String text, out Matrix34f matrix)
        {
            matrix = new Matrix34f();

            String[] tokens = text.Trim().Split(' ', '\t', '\n', '\r').Where(token => token.Length > 0).ToArray();
            if (tokens.Length != 12)
                return false;

            matrix = new Matrix34f(
                new Vector3f(Single.Parse(tokens[0]), Single.Parse(tokens[4]), Single.Parse(tokens[8])),
                new Vector3f(Single.Parse(tokens[1]), Single.Parse(tokens[5]), Single.Parse(tokens[9])),
                new Vector3f(Single.Parse(tokens[2]), Single.Parse(tokens[6]), Single.Parse(tokens[10])),
                new Vector3f(Single.Parse(tokens[3]), Single.Parse(tokens[7]), Single.Parse(tokens[11])));

            return true;
        }

        private XDocument GetRuntimeUpdates()
        {
            try
            {
                string requestUriString = string.Format("http://{0}/Scene/OutgoingChanges", gameIPAddress_);
                WebRequest request = HttpWebRequest.Create(requestUriString);
                request.Timeout = 5000;
                using (WebResponse response = request.GetResponse())
                {
                    using (MemoryStream outputStream = new MemoryStream(8196))
                    using (Stream inputstream = response.GetResponseStream())
                    {
                        byte[] buffer = new byte[8196];
                        int read = inputstream.Read(buffer, 0, buffer.Length);
                        while (read > 0)
                        {
                            outputStream.Write(buffer, 0, read);
                            read = inputstream.Read(buffer, 0, buffer.Length);
                        }

                        outputStream.Seek(0, SeekOrigin.Begin);
                        XDocument document = XDocument.Load(outputStream);
                        return document;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        private String gameIPAddress_;
        private Dictionary<int, EntityKey> nodeHandleKeyMap_;
    }
}
