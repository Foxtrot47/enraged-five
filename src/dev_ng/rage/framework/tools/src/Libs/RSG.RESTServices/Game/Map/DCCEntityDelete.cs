﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// This is a DCC representation of an entity delete.  This is translated into and EntityDelete before being dispatched to the game.
    /// </summary>
    public class DCCEntityDelete
    {
        public DCCEntityDelete(
            string mapContentName,
            string mapSectionName,
            string containerAttributeGuid,
            string nodeAttributeGuid)
        {
            MapContentName = mapContentName;
            MapSectionName = mapSectionName;
            ContainerAttributeGuid = containerAttributeGuid;
            NodeAttributeGuid = nodeAttributeGuid;
        }

        public string MapContentName { get; private set; }
        public string MapSectionName { get; private set; }
        public string ContainerAttributeGuid { get; private set; }
        public string NodeAttributeGuid { get; private set; }
    }
}
