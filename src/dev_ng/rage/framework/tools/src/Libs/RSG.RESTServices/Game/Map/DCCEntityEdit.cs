﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// This is a DCC representation of an entity edit.  This is translated into and EntityEdit before being dispatched to the game.
    /// </summary>
    public class DCCEntityEdit
    {
        public DCCEntityEdit(
            string mapContentName,
            string mapSectionName,
            string containerAttributeGuid,
            string nodeAttributeGuid,
            float positionX,
            float positionY,
            float positionZ,
            float rotationX,
            float rotationY,
            float rotationZ,
            float rotationW,
            float scaleX,
            float scaleY,
            float scaleZ,
            float lodDistance,
            float childLODDistance,
            bool useFullMatrix)
        {
            MapContentName = mapContentName;
            MapSectionName = mapSectionName;
            ContainerAttributeGuid = containerAttributeGuid;
            NodeAttributeGuid = nodeAttributeGuid;
            PositionX = positionX;
            PositionY = positionY;
            PositionZ = positionZ;
            RotationX = rotationX;
            RotationY = rotationY;
            RotationZ = rotationZ;
            RotationW = rotationW;
            ScaleX = scaleX;
            ScaleY = scaleY;
            ScaleZ = scaleZ;
            LODDistance = lodDistance;
            ChildLODDistance = childLODDistance;
            UseFullMatrix = useFullMatrix;
        }

        public string MapContentName { get; private set; }
        public string MapSectionName { get; private set; }
        public string ContainerAttributeGuid { get; private set; }
        public string NodeAttributeGuid { get; private set; }
        public float PositionX { get; private set; }
        public float PositionY { get; private set; }
        public float PositionZ { get; private set; }
        public float RotationX { get; private set; }
        public float RotationY { get; private set; }
        public float RotationZ { get; private set; }
        public float RotationW { get; private set; }
        public float ScaleX { get; private set; }
        public float ScaleY { get; private set; }
        public float ScaleZ { get; private set; }
        public float LODDistance { get; private set; }
        public float ChildLODDistance { get; private set; }
        public bool UseFullMatrix { get; private set; }
    }
}
