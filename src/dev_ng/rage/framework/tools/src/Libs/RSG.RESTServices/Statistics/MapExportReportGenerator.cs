﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using System.IO;
using System.Web.UI;

namespace RSG.RESTServices.Statistics
{
    /// <summary>
    /// Helper class for generating a report based on export stats.
    /// </summary>
    public class MapExportReportGenerator
    {
        #region Constants
        /// <summary>
        /// Default background color to use on the export time summary html report.
        /// </summary>
        private const string c_defaultBackgroundColor = "#999";

        /// <summary>
        /// Default foreground color to use on the export time summary html report.
        /// </summary>
        private const string c_defaultForegroundColor = "#FFF";
        #endregion // Constants

        #region Static Data
        /// <summary>
        /// Game config.
        /// </summary>
        private static IConfig GameConfig { get; set; }
        #endregion // Static Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor
        /// </summary>
        static MapExportReportGenerator()
        {
            try
            {
                GameConfig = ConfigFactory.CreateConfig();
            }
            catch (ConfigurationVersionException ex)
            {
                Log.Log__Exception(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.", ex.ActualVersion, ex.ExpectedVersion);
                Environment.Exit(1);
            }
            catch (ConfigurationException ex)
            {
                Log.Log__Exception(ex, "Configuration parse error.");
                Environment.Exit(1);
            }
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="timings"></param>
        public void GenerateExportSummary(MapExportTimings timings)
        {
            string filename = Path.Combine(GameConfig.Project.Cache, "raw", "maps", "export_times.html");

            // Make sure the directory exists
            if (!Directory.Exists(Path.GetDirectoryName(filename)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filename));
            }
            
            // Create the file
            using (Stream fileStream = File.Create(filename))
            {
                StreamWriter reportFile = new StreamWriter(fileStream);
                HtmlTextWriter writer = new HtmlTextWriter(reportFile);

                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Head);
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Body);
                    {
                        // Output the separate parts of the report.
                        WriteReportSummary(writer, timings);

                        if (timings != null && timings.SectionExportTimes.Any())
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Br);
                            writer.RenderEndTag();

                            WriteReportSectionsExported(writer, timings);
                        }

                        if (timings != null && timings.SectionsBuilt != null && timings.SectionsBuilt.Any())
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Br);
                            writer.RenderEndTag();

                            WriteReportSectionsBuilt(writer, timings);
                        }
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
                writer.Flush();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalTime"></param>
        public void GenerateExportSummary(double fallbackTime)
        {
            string filename = Path.Combine(GameConfig.Project.Cache, "raw", "maps", "export_times.html");

            // Make sure the directory exists
            if (!Directory.Exists(Path.GetDirectoryName(filename)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filename));
            }

            // Create the file
            using (Stream fileStream = File.Create(filename))
            {
                StreamWriter reportFile = new StreamWriter(fileStream);
                HtmlTextWriter writer = new HtmlTextWriter(reportFile);

                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Head);
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Body);
                    {
                        // Output the separate parts of the report.
                        WriteReportSummary(writer, fallbackTime);
                    }
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
                writer.Flush();
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="timings"></param>
        private void WriteReportSummary(HtmlTextWriter writer, MapExportTimings timings)
        {
            string defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColor, c_defaultForegroundColor);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // Header row
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_defaultBackgroundColor, c_defaultForegroundColor));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Summary");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Total time
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Total Time");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            if (timings != null)
            {
                writer.Write(String.Format("{0:0.##} (seconds)", (timings.TotalTime / 1000.0)));
            }
            else
            {
                writer.Write("0.00 (seconds)");
            }
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Map check time
            if (timings != null)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Map Check Time");
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0:0.##} (seconds)", (timings.MapCheckTime / 1000.0)));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            // Export time
            if (timings != null && timings.SectionExportTimes.Any())
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Export Time");
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0:0.##} (seconds)", (timings.SectionExportTimes.Values.Sum() / 1000.0)));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            // Build time
            if (timings != null && timings.SectionsBuilt != null && timings.SectionsBuilt.Any())
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Build Time");
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0:0.##} (seconds)", (timings.BuildTime / 1000.0)));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void WriteReportSummary(HtmlTextWriter writer, double fallbackTime)
        {
            string defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColor, c_defaultForegroundColor);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // Header row
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_defaultBackgroundColor, c_defaultForegroundColor));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Summary");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Total time
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Total Time");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(String.Format("{0:0.##} (seconds)", (fallbackTime / 1000.0)));
            writer.RenderEndTag();
            writer.RenderEndTag();

            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void WriteReportSectionsExported(HtmlTextWriter writer, MapExportTimings timings)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // Header row
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_defaultBackgroundColor, c_defaultForegroundColor));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Sections Exported");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Output a row for each section that was exported
            foreach (KeyValuePair<string, double> pair in timings.SectionExportTimes)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(pair.Key);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0:0.##} (seconds)", (pair.Value / 1000.0)));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void WriteReportSectionsBuilt(HtmlTextWriter writer, MapExportTimings timings)
        {
            string defaultStyle = String.Format("background-color: {0}; color: {1}", c_defaultBackgroundColor, c_defaultForegroundColor);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            // Header row
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
            writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", c_defaultBackgroundColor, c_defaultForegroundColor));
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write("Sections Built");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Output a row for each section that was built
            foreach (string section in timings.SectionsBuilt)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(section);
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
        }
        #endregion // Private Methods
    } // MapExportReportGenerator
}
