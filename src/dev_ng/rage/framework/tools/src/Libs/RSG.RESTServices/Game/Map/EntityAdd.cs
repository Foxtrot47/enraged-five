﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A command to add a new entity to the game
    /// </summary>
    public class EntityAdd
    {
        public EntityAdd(string archetypeName, uint guid)
        {
            archetypeName_ = archetypeName;
            guid_ = guid;
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        public XElement ToXElement()
        {
            return
                new XElement("CEntityAdd",
                    new XElement("name", archetypeName_),
                    new XElement("guid", new XAttribute("value", guid_)));
        }

        private readonly string archetypeName_;
        private readonly uint guid_;
    }
}
