﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Math;
using RSG.ManagedRage;
using RSG.Pipeline.Core;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// This class represents the service that allows entity updates to be sent to the game
    /// </summary>
    public class LiveMapEditingService
    {
        public LiveMapEditingService(string gameIPAddress)
        {
            gameIPAddress_ = gameIPAddress;
            addEntitiesEditSet_ = null;
            addedEntitiesGuidMap_ = new Dictionary<string, uint>();
            mapSectionEntityEditSetMap_ = new Dictionary<string, EntityEditSet>();
            freeCamAdjust_ = null;
            entityPicks_ = new List<EntityPick>();;
        }

        public void AddEntityAdd(DCCEntityAdd dccEntityAdd)
        {
            if (!addedEntitiesGuidMap_.ContainsKey(dccEntityAdd.NodeAttributeGuid))
            {
                // Store a lookup of NodeAttributeGuid to runtime guid for the added entity
                uint newGuid = nextAvailableGuid_++;
                addedEntitiesGuidMap_.Add(dccEntityAdd.NodeAttributeGuid, newGuid);

                // Create the runtime entity add structure
                if (addEntitiesEditSet_ == null)
                    addEntitiesEditSet_ = new EntityEditSet();

                EntityAdd entityAdd = new EntityAdd(dccEntityAdd.ArchetypeName, newGuid);
                addEntitiesEditSet_.AddEntityAdd(entityAdd);

                // Create an initial edit for this entity
                if (!mapSectionEntityEditSetMap_.ContainsKey(addedEntityMapSectionName_))
                    mapSectionEntityEditSetMap_.Add(addedEntityMapSectionName_, new EntityEditSet());

                EntityEdit initialEdit = new EntityEdit(
                    newGuid,
                    new Vector3f(dccEntityAdd.PositionX, dccEntityAdd.PositionY, dccEntityAdd.PositionZ),
                    new Quaternionf(dccEntityAdd.RotationX, dccEntityAdd.RotationY, dccEntityAdd.RotationZ, dccEntityAdd.RotationW),
                    dccEntityAdd.ScaleX,// scaleX => scaleXY
                    dccEntityAdd.ScaleZ,
                    dccEntityAdd.LODDistance,
                    dccEntityAdd.ChildLODDistance,
                    dccEntityAdd.UseFullMatrix);

                mapSectionEntityEditSetMap_[addedEntityMapSectionName_].AddEntityEdit(initialEdit);
            }
        }

        public void AddEntityDelete(DCCEntityDelete dccEntityDelete, IContentNode contentNode)
        {
            uint runtimeGuid;
            string mapSectionName = dccEntityDelete.MapSectionName;
            GetGuidAndMapSectionName(contentNode,
                                     dccEntityDelete.MapContentName,
                                     ref mapSectionName,
                                     dccEntityDelete.ContainerAttributeGuid,
                                     dccEntityDelete.NodeAttributeGuid,
                                     out runtimeGuid);

            if (!mapSectionEntityEditSetMap_.ContainsKey(mapSectionName))
                mapSectionEntityEditSetMap_.Add(mapSectionName, new EntityEditSet());

            EntityDelete entityDelete = new EntityDelete(runtimeGuid);
            EntityEditSet entityEditSet = mapSectionEntityEditSetMap_[mapSectionName];
            entityEditSet.AddEntityDelete(entityDelete);

            if (addedEntitiesGuidMap_.ContainsKey(dccEntityDelete.NodeAttributeGuid))
                addedEntitiesGuidMap_.Remove(dccEntityDelete.NodeAttributeGuid);
        }

        public void AddEntityEdit(DCCEntityEdit dccEntityEdit, IContentNode contentNode)
        {
            uint runtimeGuid;
            string mapSectionName = dccEntityEdit.MapSectionName;
            GetGuidAndMapSectionName(contentNode,
                                     dccEntityEdit.MapContentName,
                                     ref mapSectionName,
                                     dccEntityEdit.ContainerAttributeGuid,
                                     dccEntityEdit.NodeAttributeGuid,
                                     out runtimeGuid);

            if (!mapSectionEntityEditSetMap_.ContainsKey(mapSectionName))
                mapSectionEntityEditSetMap_.Add(mapSectionName, new EntityEditSet());

            EntityEdit entityEdit = CreateEntityEdit(runtimeGuid, dccEntityEdit);
            EntityEditSet entityEditSet = mapSectionEntityEditSetMap_[mapSectionName];
            if (!entityEditSet.HasEntityEdit(runtimeGuid))
                entityEditSet.AddEntityEdit(entityEdit);
            else
                entityEditSet.UpdateEntityEdit(runtimeGuid, entityEdit);
        }

        public void AddEntityFlagChange(DCCEntityFlagChange dccEntityFlagChange, IContentNode contentNode)
        {
            uint runtimeGuid;
            string mapSectionName = dccEntityFlagChange.MapSectionName;
            GetGuidAndMapSectionName(contentNode,
                                     dccEntityFlagChange.MapContentName,
                                     ref mapSectionName,
                                     dccEntityFlagChange.ContainerAttributeGuid,
                                     dccEntityFlagChange.NodeAttributeGuid,
                                     out runtimeGuid);

            if (!mapSectionEntityEditSetMap_.ContainsKey(mapSectionName))
                mapSectionEntityEditSetMap_.Add(mapSectionName, new EntityEditSet());

            EntityFlagChange entityFlagChange = CreateEntityFlagChange(runtimeGuid, dccEntityFlagChange);
            EntityEditSet entityEditSet = mapSectionEntityEditSetMap_[mapSectionName];
            entityEditSet.AddEntityFlagChange(entityFlagChange);
        }

        public void AddFreeCamAdjust(FreeCamAdjust freeCamAdjust)
        {
            freeCamAdjust_ = freeCamAdjust;
        }

        public void AddEntityPick(DCCEntityPick dccEntityPick, IContentNode contentNode)
        {
            uint runtimeGuid;
            string mapSectionName = dccEntityPick.MapSectionName;
            GetGuidAndMapSectionName(contentNode,
                                     dccEntityPick.MapContentName,
                                     ref mapSectionName,
                                     dccEntityPick.ContainerAttributeGuid,
                                     dccEntityPick.NodeAttributeGuid,
                                     out runtimeGuid);

            EntityPick entityPick = CreateEntityPick(runtimeGuid, dccEntityPick);

            entityPicks_.Add(entityPick);
        }

        /// <summary>
        /// Sends all cached updates to the game
        /// </summary>
        public bool SendUpdates()
        {
            // If there's nowt to do, do nowt!
            if (addEntitiesEditSet_ == null && mapSectionEntityEditSetMap_.Count == 0 && freeCamAdjust_ == null && entityPicks_.Count == 0)
                return true;

            // Build the document
            XDocument document = new XDocument(new XElement("MapLiveEditRestInterface"));

            if (addEntitiesEditSet_ != null || mapSectionEntityEditSetMap_.Count > 0)
                document.Root.Add(new XElement("incomingEditSets"));

            // There is a special edit set for adds.  This is to ensure that the initial edit occurs after the add.
            if (addEntitiesEditSet_ != null)
            {
                document.Root.Element("incomingEditSets").Add(addEntitiesEditSet_.ToXElement(addedEntityMapSectionName_));

                addEntitiesEditSet_ = null;
            }

            if(mapSectionEntityEditSetMap_.Count > 0)
            {
                document.Root.Element("incomingEditSets").Add(mapSectionEntityEditSetMap_.Select(sectionPair => sectionPair.Value.ToXElement(sectionPair.Key)));

                mapSectionEntityEditSetMap_.Clear();
            }

            if (freeCamAdjust_ != null)
            {
                document.Root.Add(new XElement("incomingCameraChanges", freeCamAdjust_.ToXElement()));

                freeCamAdjust_ = null;
            }

            if (entityPicks_.Count > 0)
            {
                document.Root.Add(new XElement("incomingPickerSelect", entityPicks_.Select(entityPick => entityPick.ToXElement())));

                entityPicks_.Clear();
            }

            // Send to the game
            string xmlDiagnosticsPathname = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "test.xml");
            document.Save(xmlDiagnosticsPathname);

            string uri = string.Format("http://{0}/Scene/MapLiveEdit", gameIPAddress_);
            return WebRequestHelper.PutText(uri, document.ToString());
        }

        private EntityEdit CreateEntityEdit(uint guid, DCCEntityEdit dccEntityEdit)
        {
            return new EntityEdit(
                guid,
                new Vector3f(dccEntityEdit.PositionX, dccEntityEdit.PositionY, dccEntityEdit.PositionZ),
                new Quaternionf(dccEntityEdit.RotationX, dccEntityEdit.RotationY, dccEntityEdit.RotationZ, dccEntityEdit.RotationW),
                dccEntityEdit.ScaleX,// scaleX => scaleXY
                dccEntityEdit.ScaleZ,
                dccEntityEdit.LODDistance,
                dccEntityEdit.ChildLODDistance,
                dccEntityEdit.UseFullMatrix);
        }

        private EntityFlagChange CreateEntityFlagChange(uint guid, DCCEntityFlagChange dccEntityFlagChange)
        {
            return new EntityFlagChange(
                guid,
                dccEntityFlagChange.DontRenderInShadows,
                dccEntityFlagChange.OnlyRenderInShadows,
                dccEntityFlagChange.DontRenderInReflections,
                dccEntityFlagChange.OnlyRenderInReflections,
                dccEntityFlagChange.DontRenderInWaterReflections,
                dccEntityFlagChange.OnlyRenderInWaterReflections);
        }

        private EntityPick CreateEntityPick(uint guid, DCCEntityPick dccEntityPick)
        {
            return new EntityPick(
                guid,
                dccEntityPick.MapSectionName,
                dccEntityPick.ArchetypeName);
        }

        /// <summary>
        /// Generates a guid and map section name.  This acts as a central point for checking the addedEntitiesGuidMap_ dictionary
        /// </summary>
        /// <param name="mapContentName"></param>
        /// <param name="processedContentName"></param>
        /// <param name="containerAttributeGuid"></param>
        /// <param name="nodeAttributeGuid"></param>
        /// <param name="runtimeGuid"></param>
        /// <param name="mapSectionName"></param>
        private void GetGuidAndMapSectionName(IContentNode contentNode,
                                              string mapContentName, 
                                              ref string mapSectionName, 
                                              string containerAttributeGuid, 
                                              string nodeAttributeGuid, 
                                              out uint runtimeGuid)
        {
            if (!addedEntitiesGuidMap_.ContainsKey(nodeAttributeGuid))
            {
                runtimeGuid = MaxGameKeyTranslator.BuildRuntimeGuid(contentNode, mapContentName, containerAttributeGuid, nodeAttributeGuid);
            }
            else
            {
                runtimeGuid = addedEntitiesGuidMap_[nodeAttributeGuid];
                mapSectionName = addedEntityMapSectionName_;
            }
        }

        private static readonly string addedEntityMapSectionName_ = "liveedit";

        private string gameIPAddress_;
        private static uint nextAvailableGuid_ = 1;
        private EntityEditSet addEntitiesEditSet_;
        private Dictionary<string, uint> addedEntitiesGuidMap_;
        private Dictionary<string, EntityEditSet> mapSectionEntityEditSetMap_;
        private FreeCamAdjust freeCamAdjust_;
        private List<EntityPick> entityPicks_;
    }
}
