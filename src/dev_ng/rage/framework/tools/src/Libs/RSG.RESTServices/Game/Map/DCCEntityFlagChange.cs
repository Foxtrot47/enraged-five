﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// This is a DCC representation of an entity flag change.  This is translated into and EntityFlagChange before being dispatched to the game.
    /// </summary>
    public class DCCEntityFlagChange
    {
        public DCCEntityFlagChange(
            string mapContentName,
            string mapSectionName,
            string containerAttributeGuid,
            string nodeAttributeGuid,
            bool dontRenderInShadows,
            bool onlyRenderInShadows,
            bool dontRenderInReflections,
            bool onlyRenderInReflections,
            bool dontRenderInWaterReflections,
            bool onlyRenderInWaterReflections)
        {
            MapContentName = mapContentName;
            MapSectionName = mapSectionName;
            ContainerAttributeGuid = containerAttributeGuid;
            NodeAttributeGuid = nodeAttributeGuid;

            DontRenderInShadows = dontRenderInShadows;
            OnlyRenderInShadows = onlyRenderInShadows;
            DontRenderInReflections = dontRenderInReflections;
            OnlyRenderInReflections = onlyRenderInReflections;
            DontRenderInWaterReflections = dontRenderInWaterReflections;
            OnlyRenderInWaterReflections = onlyRenderInWaterReflections;
        }

        public string MapContentName { get; private set; }
        public string MapSectionName { get; private set; }
        public string ContainerAttributeGuid { get; private set; }
        public string NodeAttributeGuid { get; private set; }
        public bool DontRenderInShadows { get; private set; }
        public bool OnlyRenderInShadows { get; private set; }
        public bool DontRenderInReflections { get; private set; }
        public bool OnlyRenderInReflections { get; private set; }
        public bool DontRenderInWaterReflections { get; private set; }
        public bool OnlyRenderInWaterReflections { get; private set; }
    }
}
