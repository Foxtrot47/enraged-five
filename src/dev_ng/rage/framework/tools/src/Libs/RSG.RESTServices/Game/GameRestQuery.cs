﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Net;

namespace RSG.RESTServices.Game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameRestQuery : RestQuery
    {
        #region RestQuery Overrides
        /// <summary>
        /// Server to connect to
        /// </summary>
        protected override string Server
        {
            get
            {
                return m_server;
            }
        }
        private string m_server;

        /// <summary>
        /// Whether we should use https to connect to the server
        /// </summary>
        protected override bool UseHttps
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Port to connect on
        /// </summary>
        protected override uint Port
        {
            get
            {
                return 80;
            }
        }

        /// <summary>
        /// Rest service to use
        /// </summary>
        protected override string Service
        {
            get
            {
                return null;
            }
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="server"></param>
        public GameRestQuery(string server)
            : this(server, "")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        /// <param name="query"></param>
        public GameRestQuery(string server, string query)
            : this(server, query, RequestMethod.GET)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        /// <param name="query"></param>
        /// <param name="method"></param>
        public GameRestQuery(string server, string query, RequestMethod method)
            : this(server, query, method, null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="server"></param>
        /// <param name="query"></param>
        /// <param name="method"></param>
        /// <param name="data"></param>
        public GameRestQuery(string server, string query, RequestMethod method, string data)
            : base(query, method, data)
        {
            m_server = server;
        }
        #endregion // Constructor(s)
    } // GameRestQuery
}
