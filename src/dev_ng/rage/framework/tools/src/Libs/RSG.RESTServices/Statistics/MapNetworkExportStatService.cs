﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Reflection;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ExportStats;

namespace RSG.RESTServices.Statistics
{
    public class MapNetworkExportStatService : IDisposable
    {
        #region Fields
        /// <summary>
        /// The number of times this service has been started since the session began.
        /// </summary>
        private static uint ExportNumber = 0;

        /// <summary>
        /// The private field used for the <see cref="NetworkExportStatClient"/> property.
        /// </summary>
        private MapExportStatClient networkExportStatClient;
        #endregion

        #region Constructors
        /// <summary>
        /// Static constructor
        /// </summary>
        static MapNetworkExportStatService()
        {
            try
            {
                StatsConfig = new StatisticsConfig();
            }
            catch (ConfigurationVersionException ex)
            {
                Log.Log__Exception(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.", ex.ActualVersion, ex.ExpectedVersion);
                Environment.Exit(1);
            }
            catch (ConfigurationException ex)
            {
                Log.Log__Exception(ex, "Configuration parse error.");
                Environment.Exit(1);
            }
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the statistics server configuration data.
        /// </summary>
        private static IStatisticsConfig StatsConfig
        {
            get;
            set;
        }

        /// <summary>
        /// WCF client for communicating with the stats server.
        /// </summary>
        private MapExportStatClient NetworkExportStatClient
        {
            get
            {
                if (this.networkExportStatClient == null)
                {
                    EndpointAddress address = new EndpointAddress(String.Format("http://{0}:{1}/StatisticsService/MapExportStats", StatsConfig.DefaultServer.Address, StatsConfig.DefaultServer.HttpPort));
                    WebHttpBinding binding = new WebHttpBinding(WebHttpSecurityMode.None);
                    this.networkExportStatClient = new MapExportStatClient(binding, address);
                    this.networkExportStatClient.Endpoint.Behaviors.Add(new WebHttpBehavior());
                }

                return this.networkExportStatClient;
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Call when the user initiates a new export in max.
        /// </summary>
        /// <param name="exportType"></param>
        public void MapNetworkExportStarted(String exportType, Guid jobId, String[] sectionNames)
        {
            // Populate the relevant data
            MapNetworkExportStatDto dto = new MapNetworkExportStatDto();
            dto.Username = Environment.UserName;
            dto.MachineName = Environment.MachineName;
            dto.ExportType = exportType;
            dto.JobId = jobId;
            dto.JobCreationNumber = ExportNumber++;
            dto.SectionNames = sectionNames.ToList();

            NetworkExportStatClient.CreateMapNetworkExportStat(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (this.networkExportStatClient != null)
            {
                this.networkExportStatClient.Close();
            }
        }
        #endregion Methods
    }
}
