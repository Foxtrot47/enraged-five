﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A set of live editing entity edit commands for a given map section
    /// </summary>
    internal class EntityEditSet
    {
        internal EntityEditSet()
        {
            entityEditMap_ = new Dictionary<uint, EntityEdit>();
            entityAdds_ = new List<EntityAdd>();
            entityDeletes_ = new List<EntityDelete>();
            entityFlagChanges_ = new List<EntityFlagChange>();
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        internal XElement ToXElement(string mapSectionName)
        {
            XElement rootElement = new XElement("CEntityEditSet",
                new XElement("mapSectionName", mapSectionName),
                new XElement("adds", entityAdds_.Select(item => item.ToXElement())),
                new XElement("deletes", entityDeletes_.Select(item => item.ToXElement())),
                new XElement("edits", entityEditMap_.Select(item => item.Value.ToXElement())),
                new XElement("flagChanges", entityFlagChanges_.Select(item => item.ToXElement())));

            return rootElement;
        }

        internal bool HasEntityEdit(uint guid)
        {
            return entityEditMap_.ContainsKey(guid);
        }

        internal void AddEntityEdit(EntityEdit entityEdit)
        {
            entityEditMap_.Add(entityEdit.Guid, entityEdit);
        }

        internal void UpdateEntityEdit(uint guid, EntityEdit entityEdit)
        {
            entityEditMap_[guid] = entityEdit;
        }

        internal void AddEntityAdd(EntityAdd entityAdd)
        {
            entityAdds_.Add(entityAdd);
        }

        internal void AddEntityDelete(EntityDelete entityDelete)
        {
            entityDeletes_.Add(entityDelete);
        }

        internal void AddEntityFlagChange(EntityFlagChange entityFlagChange)
        {
            entityFlagChanges_.Add(entityFlagChange);
        }

        private Dictionary<uint, EntityEdit> entityEditMap_;
        private List<EntityAdd> entityAdds_;
        private List<EntityDelete> entityDeletes_;
        private List<EntityFlagChange> entityFlagChanges_;
    }
}
