﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace RSG.RESTServices
{
    /// <summary>
    /// Helper class for sending web requests
    /// </summary>
    internal static class WebRequestHelper
    {
        internal static bool PutText(string uri, string text)
        {
            try
            {
                WebRequest request = HttpWebRequest.Create(uri);
                request.Timeout = putTimeoutMS_;
                request.Method = "PUT";
                byte[] postData = Encoding.UTF8.GetBytes(text);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = postData.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(postData, 0, postData.Length);
                dataStream.Close();
                using (WebResponse response = request.GetResponse()) { }// There's translatable response to a PUT
            }
            catch (Exception)// we should do a better job here
            {
                return false;
            }

            return true;
        }

        private static readonly int putTimeoutMS_ = 5000;
    }
}
