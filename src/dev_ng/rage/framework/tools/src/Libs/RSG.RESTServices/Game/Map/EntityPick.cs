﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A command to add a new entity to the game
    /// </summary>
    public class EntityPick
    {
        public EntityPick(uint guid, String mapSectionName, String archetypeName)
        {
            guid_ = guid;
            mapSectionName_ = mapSectionName;
            archetypeName_ = archetypeName;
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        public XElement ToXElement()
        {
            return
                new XElement("CEntityPickEvent",
                    new XElement("guid", new XAttribute("value", guid_)),
                    new XElement("mapSectionName", mapSectionName_),
                    new XElement("modelName", archetypeName_));
        }

        private readonly uint guid_;
        private readonly String mapSectionName_;
        private readonly String archetypeName_;
    }
}
