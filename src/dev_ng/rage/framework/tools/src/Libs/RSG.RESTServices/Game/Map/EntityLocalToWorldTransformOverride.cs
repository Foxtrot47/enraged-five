﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.RESTServices.Game.Map
{
    public class EntityLocalToWorldTransformOverride
    {
        internal EntityLocalToWorldTransformOverride(int nodeHandle, Matrix34f localToWorldTransform)
        {
            NodeHandle = nodeHandle;
            LocalToWorldTransform = localToWorldTransform;
        }

        public int NodeHandle { get; private set; }
        public Matrix34f LocalToWorldTransform { get; private set; }
    }
}
