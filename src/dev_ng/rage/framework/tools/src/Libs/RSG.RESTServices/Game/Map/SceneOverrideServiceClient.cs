﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;

using RSG.Model.Map.SceneOverride;

namespace RSG.GameRESTServices.Game.Map
{
    /// <summary>
    /// This service client sucks override data in from the game
    /// </summary>
    public class SceneOverrideServiceClient
    {
        public SceneOverrideServiceClient(string gameIPAddress, string sceneOverridesPathname)
        {
            gameIPAddress_ = gameIPAddress;
            sceneOverridesPathname_ = sceneOverridesPathname;

            updateThread_ = null;
            updateShutdownTriggered_ = false;

            previousLODOverrides_ = new RuntimeLODOverride[0];
            previousEntityDeleteItems_ = new RuntimeEntityDeleteItem[0];
            previousAttributeOverrides_ = new RuntimeAttributeOverride[0];
        }

        public string GameIPAddress
        {
            get { return gameIPAddress_; }
        }

        public bool IsAvailable
        {
            get
            {
                try
                {
                    string requestUriString = string.Format("http://{0}/Scene/OutgoingChanges", gameIPAddress_);
                    WebRequest request = HttpWebRequest.Create(requestUriString);
                    request.Timeout = 5000;
                    using (WebResponse response = request.GetResponse()) { }

                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                
                return false;
            }
        }

        public void Connect()
        {
            updateThread_ = new Thread(UpdateThreadMain);
            updateThread_.Start();
        }

        public void Disconnect()
        {
            updateShutdownTriggered_ = true;
        }

        public RuntimeLODOverride[] GetCachedLODOverrides()
        {
            RuntimeLODOverride[] overridesToSubmit = null;
            lock (previousLODOverrides_)
                overridesToSubmit = previousLODOverrides_.ToArray();
            return overridesToSubmit;
        }

        public void ClearCachedLODOverrides()
        {
            lock(previousLODOverrides_)
                previousLODOverrides_ = new RuntimeLODOverride[0];
        }

        public RuntimeEntityDeleteItem[] GetCachedDeleteItems()
        {
            RuntimeEntityDeleteItem[] deleteItemsToSubmit = null;
            lock (previousEntityDeleteItems_)
                deleteItemsToSubmit = previousEntityDeleteItems_.ToArray();
            return deleteItemsToSubmit;
        }

        public void ClearCachedDeleteItems()
        {
            lock (previousEntityDeleteItems_)
                previousEntityDeleteItems_ = new RuntimeEntityDeleteItem[0];
        }

        public RuntimeAttributeOverride[] GetCachedAttributeOverrides()
        {
            RuntimeAttributeOverride[] overridesToSubmit = null;
            lock (previousAttributeOverrides_)
                overridesToSubmit = previousAttributeOverrides_.ToArray();
            return overridesToSubmit;
        }

        public void ClearCachedAttributeOverrides()
        {
            lock (previousAttributeOverrides_)
                previousAttributeOverrides_ = new RuntimeAttributeOverride[0];
        }

        public event Action<RuntimeLODOverride> OnLODOverrideAdded;
        public event Action<RuntimeLODOverride> OnLODOverrideUpdated;
        public event Action<RuntimeEntityDeleteItem> OnDeleteItemAdded;
        public event Action<RuntimeAttributeOverride> OnAttributeOverrideAdded;
        public event Action<RuntimeAttributeOverride> OnAttributeOverrideUpdated;
        public event Action OnConnectionLost;

        private static string GetMapSectionNameFromIMAPName(string imapName)
        {
            if (imapName.Contains(streamingImapSuffix_))
            {
                return imapName.Substring(0, imapName.IndexOf(streamingImapSuffix_));
            }
            else if (imapName.Contains(streamingLongImapSuffix_))
            {
                return imapName.Substring(0, imapName.IndexOf(streamingLongImapSuffix_));
            }

            return imapName;
        }

        private void UpdateThreadMain()
        {
            updateShutdownTriggered_ = false;
            connectionEstablished_ = false;

            while (!updateShutdownTriggered_)
            {
                // Get data from the game, compare it with the last update and fire events as required
                if (Save(sceneOverridesPathname_))
                {
                    connectionEstablished_ = true;
                    List<RuntimeLODOverride> lodOverrides = new List<RuntimeLODOverride>();
                    List<RuntimeEntityDeleteItem> runtimeEntityDeleteItems = new List<RuntimeEntityDeleteItem>();
                    List<RuntimeAttributeOverride> attributeOverrides = new List<RuntimeAttributeOverride>();
                    Load(sceneOverridesPathname_, lodOverrides, runtimeEntityDeleteItems, attributeOverrides);
                    ProcessLODOverrideUpdates(previousLODOverrides_, lodOverrides);
                    lock (previousLODOverrides_)
                        previousLODOverrides_ = lodOverrides;
                    ProcessEntityDeleteUpdates(previousEntityDeleteItems_, runtimeEntityDeleteItems);
                    lock (previousEntityDeleteItems_)
                        previousEntityDeleteItems_ = runtimeEntityDeleteItems;
                    ProcessAttributeOverrideUpdates(previousAttributeOverrides_, attributeOverrides);
                    lock (previousAttributeOverrides_)
                        previousAttributeOverrides_ = attributeOverrides;
                    Thread.Sleep(5000);// B* 1306714 - Updated to 5000 from 250 after speaking to Russ S
                }
                else
                {
                    if (OnConnectionLost != null && connectionEstablished_)
                        OnConnectionLost();
                }
            }
        }

        private bool Save(string pathname)
        {
            try
            {
                string requestUriString = string.Format("http://{0}/Scene/OutgoingChanges", gameIPAddress_);
                WebRequest request = HttpWebRequest.Create(requestUriString);
                request.Timeout = 5000;
                using (WebResponse response = request.GetResponse())
                {
                    using (MemoryStream outputStream = new MemoryStream(8196))
                    using (Stream inputstream = response.GetResponseStream())
                    {
                        byte[] buffer = new byte[8196];
                        int read = inputstream.Read(buffer, 0, buffer.Length);
                        while (read > 0)
                        {
                            outputStream.Write(buffer, 0, read);
                            read = inputstream.Read(buffer, 0, buffer.Length);
                        }

                        outputStream.Seek(0, SeekOrigin.Begin);
                        XDocument document = XDocument.Load(outputStream);
                        document.Save(pathname);

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        private void Load(string pathname, 
                          List<RuntimeLODOverride> lodOverrides, 
                          List<RuntimeEntityDeleteItem> deleteItems,
                          List<RuntimeAttributeOverride> attributeOverrides)
        {
            try
            {
                Dictionary<uint, RuntimeLODOverride> guidLODOverrideItemMap = new Dictionary<uint, RuntimeLODOverride>();

                XDocument document = XDocument.Load(pathname);
                XElement distanceOverideElement = document.Root.Element("distanceOverride");
                if (distanceOverideElement != null)
                {
                    foreach (XElement itemElement in distanceOverideElement.Elements("Item"))
                    {
                        try
                        {
                            uint guid = uint.Parse(itemElement.Element("guid").Attribute("value").Value);
                            uint hash = uint.Parse(itemElement.Element("hash").Attribute("value").Value);
                            float posX = float.Parse(itemElement.Element("posn").Attribute("x").Value);
                            float posY = float.Parse(itemElement.Element("posn").Attribute("y").Value);
                            float posZ = float.Parse(itemElement.Element("posn").Attribute("z").Value);
                            String imapName = itemElement.Element("imapName").Value;
                            String modelName = "?????";
                            if (itemElement.Element("modelName") != null)
                                modelName = itemElement.Element("modelName").Value;
                            float distance = float.Parse(itemElement.Element("distance").Attribute("value").Value);

                            RuntimeLODOverride newItem = new RuntimeLODOverride(hash, guid, posX, posY, posZ, imapName, modelName);
                            newItem.Distance = distance;

                            guidLODOverrideItemMap.Add(guid, newItem);
                        }
                        catch (Exception) { }
                    }
                }

                XElement childDistanceOverideElement = document.Root.Element("childDistanceOverride");
                if (childDistanceOverideElement != null)
                {
                    foreach (XElement itemElement in childDistanceOverideElement.Elements("Item"))
                    {
                        try
                        {
                            uint guid = uint.Parse(itemElement.Element("guid").Attribute("value").Value);
                            float distance = float.Parse(itemElement.Element("distance").Attribute("value").Value);

                            if (guidLODOverrideItemMap.ContainsKey(guid))
                            {
                                guidLODOverrideItemMap[guid].ChildDistance = distance;
                            }
                            else
                            {
                                uint hash = uint.Parse(itemElement.Element("hash").Attribute("value").Value);
                                float posX = float.Parse(itemElement.Element("posn").Attribute("x").Value);
                                float posY = float.Parse(itemElement.Element("posn").Attribute("y").Value);
                                float posZ = float.Parse(itemElement.Element("posn").Attribute("z").Value);
                                String imapName = itemElement.Element("imapName").Value;
                                String modelName = "?????";
                                if (itemElement.Element("modelName") != null)
                                    modelName = itemElement.Element("modelName").Value;

                                RuntimeLODOverride newItem = new RuntimeLODOverride(hash, guid, posX, posY, posZ, imapName, modelName);
                                newItem.ChildDistance = distance;

                                guidLODOverrideItemMap.Add(guid, newItem);
                            }
                        }
                        catch (Exception) { }
                    }
                }

                lodOverrides.AddRange(guidLODOverrideItemMap.Values);

                XElement mapEntityDeleteListElement = document.Root.Element("mapEntityDeleteList");
                if (mapEntityDeleteListElement != null)
                {
                    foreach (XElement itemElement in mapEntityDeleteListElement.Elements("Item"))
                    {
                        try
                        {
                            uint guid = uint.Parse(itemElement.Element("guid").Attribute("value").Value);
                            uint hash = uint.Parse(itemElement.Element("hash").Attribute("value").Value);
                            float posX = float.Parse(itemElement.Element("posn").Attribute("x").Value);
                            float posY = float.Parse(itemElement.Element("posn").Attribute("y").Value);
                            float posZ = float.Parse(itemElement.Element("posn").Attribute("z").Value);
                            String imapName = itemElement.Element("imapName").Value;
                            String modelName = "?????";
                            if (itemElement.Element("modelName") != null)
                                modelName = itemElement.Element("modelName").Value;

                            RuntimeEntityDeleteItem newItem = new RuntimeEntityDeleteItem(hash, guid, posX, posY, posZ, imapName, modelName);
                            deleteItems.Add(newItem);
                        }
                        catch (Exception) { }
                    }
                }

                XElement attributOverrideElement = document.Root.Element("attributeOverride");
                if (attributOverrideElement != null)
                {
                    foreach (XElement itemElement in attributOverrideElement.Elements("Item"))
                    {
                        try
                        {
                            uint guid = UInt32.Parse(itemElement.Element("guid").Attribute("value").Value);
                            uint hash = UInt32.Parse(itemElement.Element("hash").Attribute("value").Value);
                            float posX = Single.Parse(itemElement.Element("posn").Attribute("x").Value);
                            float posY = Single.Parse(itemElement.Element("posn").Attribute("y").Value);
                            float posZ = Single.Parse(itemElement.Element("posn").Attribute("z").Value);
                            String imapName = itemElement.Element("imapName").Value;
                            String modelName = "?????";
                            if (itemElement.Element("modelName") != null)
                                modelName = itemElement.Element("modelName").Value;
                            bool dontCastShadows = Boolean.Parse(itemElement.Element("bDontCastShadows").Attribute("value").Value);
                            bool dontRenderInShadows = Boolean.Parse(itemElement.Element("bDontRenderInShadows").Attribute("value").Value);
                            bool dontRenderInReflections = Boolean.Parse(itemElement.Element("bDontRenderInReflections").Attribute("value").Value); ;
                            bool onlyRenderInReflections = Boolean.Parse(itemElement.Element("bOnlyRenderInReflections").Attribute("value").Value); ;
                            bool dontRenderInWaterReflections = Boolean.Parse(itemElement.Element("bDontRenderInWaterReflections").Attribute("value").Value); ;
                            bool onlyRenderInWaterReflections = Boolean.Parse(itemElement.Element("bOnlyRenderInWaterReflections").Attribute("value").Value); ;
                            bool dontRenderInMirrorReflections = Boolean.Parse(itemElement.Element("bDontRenderInMirrorReflections").Attribute("value").Value); ;
                            bool onlyRenderMirrorInReflections = Boolean.Parse(itemElement.Element("bOnlyRenderInMirrorReflections").Attribute("value").Value); ;
                            bool streamingPriorityLow = Boolean.Parse(itemElement.Element("bStreamingPriorityLow").Attribute("value").Value);
                            int priority = Int32.Parse(itemElement.Element("iPriority").Attribute("value").Value);

                            RuntimeAttributeOverride newItem = new RuntimeAttributeOverride(
                                hash, guid, posX, posY, posZ, imapName, modelName,
                                dontCastShadows, dontRenderInShadows,
                                dontRenderInReflections, onlyRenderInReflections,
                                dontRenderInWaterReflections, onlyRenderInWaterReflections,
                                dontRenderInMirrorReflections, onlyRenderMirrorInReflections,
                                streamingPriorityLow, priority);
                            attributeOverrides.Add(newItem);
                        }
                        catch (Exception) { }
                    }
                }
            }
            catch (Exception)
            {
                lodOverrides.Clear();
                deleteItems.Clear();
                attributeOverrides.Clear();
            }
        }

        private void ProcessLODOverrideUpdates(IEnumerable<RuntimeLODOverride> previousLODOverrides, IEnumerable<RuntimeLODOverride> lodOverrides)
        {
            List<RuntimeLODOverride> newLODOverrides = new List<RuntimeLODOverride>();
            List<RuntimeLODOverride> updatedLODOverrides = new List<RuntimeLODOverride>();

            foreach (RuntimeLODOverride lodOverride in lodOverrides)
            {
                RuntimeLODOverride match = previousLODOverrides.FirstOrDefault(lo => lodOverride.IMAPName == lo.IMAPName &&
                                                                               lodOverride.Guid == lo.Guid &&
                                                                               lodOverride.Hash == lo.Hash);

                if (match == null)
                {
                    // if it's not in previousLODOverrides, it's new
                    newLODOverrides.Add(lodOverride);
                }
                else
                {
                    // else if its content is different then update updatedLODOverrides
                    if(lodOverride.ChildDistance != match.ChildDistance ||
                       lodOverride.Distance != match.Distance ||
                       lodOverride.PosX != match.PosX ||
                       lodOverride.PosY != match.PosY ||
                       lodOverride.PosZ != match.PosZ)
                    {
                        updatedLODOverrides.Add(lodOverride);
                    }
                }
            }

            if (OnLODOverrideAdded != null)
            {
                foreach (RuntimeLODOverride lodOverride in newLODOverrides)
                    OnLODOverrideAdded(lodOverride);
            }

            if (OnLODOverrideUpdated != null)
            {
                foreach (RuntimeLODOverride lodOverride in updatedLODOverrides)
                    OnLODOverrideUpdated(lodOverride);
            }
        }

        private void ProcessEntityDeleteUpdates(IEnumerable<RuntimeEntityDeleteItem> previousDeleteItems, IEnumerable<RuntimeEntityDeleteItem> deleteItems)
        {
            List<RuntimeEntityDeleteItem> newDeleteItems = new List<RuntimeEntityDeleteItem>();

            foreach (RuntimeEntityDeleteItem deleteItem in deleteItems)
            {
                RuntimeEntityDeleteItem match = previousDeleteItems.FirstOrDefault(di => deleteItem.IMAPName == di.IMAPName &&
                                                                                         deleteItem.Guid == di.Guid &&
                                                                                         deleteItem.Hash == di.Hash);

                if (match == null)
                    newDeleteItems.Add(deleteItem);
            }

            if (OnDeleteItemAdded != null)
            {
                foreach (RuntimeEntityDeleteItem deleteItem in newDeleteItems)
                    OnDeleteItemAdded(deleteItem);
            }
        }

        private void ProcessAttributeOverrideUpdates(IEnumerable<RuntimeAttributeOverride> previousAttributeOverrides, 
                                                     IEnumerable<RuntimeAttributeOverride> attributeOverrides)
        {
            List<RuntimeAttributeOverride> newAttributeOverrides = new List<RuntimeAttributeOverride>();
            List<RuntimeAttributeOverride> updatedAttributeOverrides = new List<RuntimeAttributeOverride>();

            foreach (RuntimeAttributeOverride attributeOverride in attributeOverrides)
            {
                RuntimeAttributeOverride match = previousAttributeOverrides.FirstOrDefault(lo => attributeOverride.IMAPName == lo.IMAPName &&
                                                                                           attributeOverride.Guid == lo.Guid &&
                                                                                           attributeOverride.Hash == lo.Hash);

                if (match == null)
                {
                    // if it's not in previousLODOverrides, it's new
                    newAttributeOverrides.Add(attributeOverride);
                }
                else
                {
                    // else if its content is different then update updatedLODOverrides
                    if (attributeOverride.DontCastShadows != match.DontCastShadows ||
                        attributeOverride.DontRenderInShadows != match.DontRenderInShadows ||
                        attributeOverride.DontRenderInReflections != match.DontRenderInReflections ||
                        attributeOverride.OnlyRenderInReflections != match.DontRenderInReflections ||
                        attributeOverride.DontRenderInWaterReflections != match.DontRenderInWaterReflections ||
                        attributeOverride.OnlyRenderInWaterReflections != match.OnlyRenderInWaterReflections ||
                        attributeOverride.DontRenderInMirrorReflections != match.DontRenderInMirrorReflections ||
                        attributeOverride.OnlyRenderInMirrorReflections != match.OnlyRenderInMirrorReflections ||
                        attributeOverride.StreamingPriorityLow != match.StreamingPriorityLow ||
                        attributeOverride.Priority != match.Priority ||
                        attributeOverride.PosX != match.PosX ||
                        attributeOverride.PosY != match.PosY ||
                        attributeOverride.PosZ != match.PosZ)
                    {
                        updatedAttributeOverrides.Add(attributeOverride);
                    }
                }
            }

            if (OnAttributeOverrideAdded != null)
            {
                foreach (RuntimeAttributeOverride attributeOverride in newAttributeOverrides)
                    OnAttributeOverrideAdded(attributeOverride);
            }

            if (OnAttributeOverrideUpdated != null)
            {
                foreach (RuntimeAttributeOverride attributeOverride in updatedAttributeOverrides)
                    OnAttributeOverrideUpdated(attributeOverride);
            }
        }

        private static readonly string streamingImapSuffix_ = "_strm_";
        private static readonly string streamingLongImapSuffix_ = "_long_";

        private readonly string gameIPAddress_;
        private readonly string sceneOverridesPathname_;

        private Thread updateThread_;
        private bool updateShutdownTriggered_;
        private bool connectionEstablished_;

        IEnumerable<RuntimeLODOverride> previousLODOverrides_;
        IEnumerable<RuntimeEntityDeleteItem> previousEntityDeleteItems_;
        IEnumerable<RuntimeAttributeOverride> previousAttributeOverrides_;
    }
}
