﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.RESTServices.Game.Map;
using RSG.Pipeline.Core;

namespace RSG.RESTServices.Game
{
    internal static class MaxGameKeyTranslator
    {
        internal static EntityKey DCCEntityKeyToEntityKey(DCCEntityKey dccEntityKey, IContentNode contentNode)
        {
            uint guid = BuildRuntimeGuid(contentNode, dccEntityKey.MapContentName, dccEntityKey.ContainerAttributeGuid, dccEntityKey.NodeAttributeGuid);
            return new EntityKey(guid, dccEntityKey.MapSectionName);
        }

        internal static uint BuildRuntimeGuid(IContentNode contentNode, string mapContentName, string containerAttributeGuid, string nodeAttributeGuid)
        {
            uint mapContentSeed = 0;
            if (contentNode.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.ParamMap_Seed))
            {
                mapContentSeed = (uint)(int)contentNode.Parameters[RSG.Pipeline.Core.Constants.ParamMap_Seed];
            }

            uint containerHash = RSG.ManagedRage.StringHashUtil.atStringHash(containerAttributeGuid, mapContentSeed);
            return RSG.ManagedRage.StringHashUtil.atStringHash(nodeAttributeGuid, containerHash);
        }
    }
}
