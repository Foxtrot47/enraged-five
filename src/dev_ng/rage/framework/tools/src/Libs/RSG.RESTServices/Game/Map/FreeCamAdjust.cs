﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A command to adjust the current camera used by the game
    /// </summary>
    public class FreeCamAdjust
    {
        public FreeCamAdjust(float positionX, float positionY, float positionZ, float frontX, float frontY, float frontZ, float upX, float upY, float upZ)
        {
            PositionX = positionX;
            PositionY = positionY;
            PositionZ = positionZ;
            FrontX = frontX;
            FrontY = frontY;
            FrontZ = frontZ;
            UpX = upX;
            UpY = upY;
            UpZ = upZ;
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        /// <returns></returns>
        internal XElement ToXElement()
        {
            return
                new XElement("CFreeCamAdjust",
                    new XElement("vPos", new XAttribute("x", PositionX), new XAttribute("y", PositionY), new XAttribute("z", PositionZ)),
                    new XElement("vFront", new XAttribute("x", FrontX), new XAttribute("y", FrontY), new XAttribute("z", FrontZ)),
                    new XElement("vUp", new XAttribute("x", UpX), new XAttribute("y", UpY), new XAttribute("z", UpZ)));
        }

        public float PositionX { get; private set; }
        public float PositionY { get; private set; }
        public float PositionZ { get; private set; }
        public float FrontX { get; private set; }
        public float FrontY { get; private set; }
        public float FrontZ { get; private set; }
        public float UpX { get; private set; }
        public float UpY { get; private set; }
        public float UpZ { get; private set; }
    }
}
