﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.RESTServices.Game.Map
{
    internal class EntityKey
    {
        internal EntityKey(uint guid, String mapSectionName)
        {
            Guid = guid;
            MapSectionName = mapSectionName;
        }

        internal uint Guid { get; private set; }
        internal String MapSectionName { get; private set; }
    }
}
