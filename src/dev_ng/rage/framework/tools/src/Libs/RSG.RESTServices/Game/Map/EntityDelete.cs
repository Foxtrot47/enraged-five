﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A command to delete an entity from the game
    /// </summary>
    public class EntityDelete
    {
        public EntityDelete(uint guid)
        {
            guid_ = guid;
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        public XElement ToXElement()
        {
            return
                new XElement("CEntityDelete",
                    new XElement("guid", new XAttribute("value", guid_)));
        }

        private readonly uint guid_;
    }
}
