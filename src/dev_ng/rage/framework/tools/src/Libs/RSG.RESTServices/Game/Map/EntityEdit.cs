﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using System.Xml.Linq;

namespace RSG.RESTServices.Game.Map
{
    /// <summary>
    /// A command to edit an entity's core properties in the game
    /// </summary>
    internal class EntityEdit
    {
        internal EntityEdit(
            uint guid,
            Vector3f position,
            Quaternionf rotation,
            float scaleXY,
            float scaleZ,
            float lodDistance,
            float childLODDistance,
            bool useFullMatrix)
        {
            Guid = guid;
            Position = position;
            Rotation = rotation;
            ScaleXY = scaleXY;
            ScaleZ = scaleZ;
            LODDistance = lodDistance;
            if (childLODDistance <= 0)
                ChildLODDistance = -1.0f;
            else
                ChildLODDistance = childLODDistance;
            UseFullMatrix = useFullMatrix;
        }

        /// <summary>
        /// Translates the internal structure into a pargen compliant XML element
        /// </summary>
        internal XElement ToXElement()
        {
            return
                new XElement("CEntityEdit",
                    new XElement("guid", new XAttribute("value", Guid)),
                    new XElement("position", new XAttribute("x", Position.X), new XAttribute("y", Position.Y), new XAttribute("z", Position.Z)),
                    new XElement("rotation", new XAttribute("x", Rotation.X), new XAttribute("y", Rotation.Y), new XAttribute("z", Rotation.Z), new XAttribute("w", Rotation.W)),
                    new XElement("scaleXY", new XAttribute("value", ScaleXY)),
                    new XElement("scaleZ", new XAttribute("value", ScaleZ)),
                    new XElement("lodDistance", new XAttribute("value", LODDistance)),
                    new XElement("childLodDistance", new XAttribute("value", ChildLODDistance)),
                    new XElement("bForceFullMatrix", new XAttribute("value", UseFullMatrix)));
        }

        internal uint Guid { get; private set; }
        internal Vector3f Position { get; private set; }
        internal Quaternionf Rotation { get; private set; }
        internal float ScaleXY { get; private set; }
        internal float ScaleZ { get; private set; }
        internal float LODDistance { get; private set; }
        public float ChildLODDistance { get; private set; }
        public bool UseFullMatrix { get; private set; }
    }
}
