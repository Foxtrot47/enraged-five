﻿//---------------------------------------------------------------------------------------------
// <copyright file="IDomainEntity.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Persistence.Data
{
    /// <summary>
    /// Interface for all database domain entities.
    /// </summary>
    public interface IDomainEntity
    {
        /// <summary>
        /// Primary key for the entity.
        /// </summary>
        long Id { get; }
    }
}
