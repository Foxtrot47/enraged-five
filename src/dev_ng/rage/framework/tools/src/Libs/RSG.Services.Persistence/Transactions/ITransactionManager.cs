﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITransactionManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using NHibernate;
using RSG.Base.Logging;

namespace RSG.Services.Persistence.Transactions
{
    /// <summary>
    /// Interface for executing arbitrary database actions wrapped inside of
    /// transactions.
    /// </summary>
    public interface ITransactionManager : IDisposable
    {
        #region Methods
        /// <summary>
        /// Execute an action that doesn't require a return value.
        /// </summary>
        /// <param name="action"></param>
        void ExecuteAction(Action<ISession> action);
        
        /// <summary>
        /// Exceute an action that returns a result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        T ExecuteAction<T>(Func<ISession, T> action);
        #endregion
    }
}
