//---------------------------------------------------------------------------------------------
// <copyright file="TransactionalServiceHostFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Reflection;
using System.ServiceModel;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Event;
using NHibernate.Mapping.ByCode;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Persistence.Listeners;
using RSG.Services.Persistence.Transactions;
using NHibernate.Tool.hbm2ddl;
using RSG.Base.Logging;
using RSG.Services.Persistence.Exceptions;
using RSG.Services.Persistence.Migrations;
using FluentMigrator.Runner.Processors;
using System.Diagnostics;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Custom service host factory that creates transactional service hosts.
    /// </summary>
    public abstract class TransactionalServiceHostFactory<ServerConfig, MigratorFactory, DbDriver, DbDialect> :
        ConfigServiceHostFactory<ServerConfig> 
        where ServerConfig : IDatabaseServer
        where MigratorFactory : IMigrationProcessorFactory, new()
        where DbDriver : IDriver
        where DbDialect : Dialect
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="SessionFactory"/> property.
        /// </summary>
        private readonly ISessionFactory _sessionFactory;

        /// <summary>
        /// Concrete instance of the transaction manager factory.
        /// </summary>
        private readonly ITransactionManagerFactory _transactionManagerFactory;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TransactionalServiceHostFactory"/>
        /// class using the specified server configuration object.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="wipeDatabase"></param>
        protected TransactionalServiceHostFactory(ServerConfig server, bool wipeDatabase)
            : base(server)
        {
            if (wipeDatabase && !Debugger.IsAttached)
            {
                throw new ArgumentException("Failsafe!  Only possible to wipe databases when attached with a debugger.");
            }
            if (wipeDatabase && server.ReadOnly)
            {
                throw new ArgumentException("Unable to wipe a read-only database.");
            }

            // Run the fluent migrations.
            using (new ProfileContext(Log, "Running fluent migrations."))
            {
                Log.Message("Using {0},{1} as the source for migrations.", MigrationsNamespace, MigrationsAssembly.FullName);
                DatabaseMigrator<MigratorFactory> migrator = new DatabaseMigrator<MigratorFactory>(
					server.ConnectionString, MigrationsAssembly, MigrationsNamespace, Log);

                if (wipeDatabase)
                {
                    migrator.Migrate(runner => runner.MigrateDown(0));
                }
                migrator.Migrate(runner => runner.MigrateUp());
            }

            // Initialise the NHibernate configuration objects.
            Log.Profile("Creating NHibernate configuration object.");
            NHibernate.Cfg.Configuration nhibernateConfiguration = CreateNHibernateConfiguration(server);
            Log.ProfileEnd();

            // Validate the migrations against the NHibernate mappings/schema.
            using (new ProfileContext(Log, "Validating migrations against NHibernate schema."))
            {
                SchemaUpdate schemaUpdate = new SchemaUpdate(nhibernateConfiguration);
                schemaUpdate.Execute(SchemaUpdateScriptAction, false);

                // Make sure the schema update didn't create any errors (via outputting messages).
                if (Log.HasErrors)
                {
                    throw new SchemaValidationException("Schema validation generated errors.  Unable to continue.");
                }
            }

            // Create the transaction manager.
            using (new ProfileContext(Log, "Creating transaction manager factory."))
            {
                _sessionFactory = nhibernateConfiguration.BuildSessionFactory();
                _transactionManagerFactory = new TransactionManagerFactory(_sessionFactory, server.ReadOnly);
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// String to use when connecting to the database.
        /// </summary>
        protected virtual string ConnectionStringName
        {
            get { return "NHibernate4.0"; }
        }

        /// <summary>
        /// Gets a reference to the assembly that contains the nhibernate mappings.
        /// </summary>
        protected abstract Assembly MappingsAssembly { get; }

        /// <summary>
        /// Gets a reference to the assembly that contains the fluent migrations.
        /// </summary>
        protected abstract Assembly MigrationsAssembly { get; }

        /// <summary>
        /// Gets the namespace that the migration classes are in.
        /// </summary>
        protected abstract string MigrationsNamespace { get; }

        /// <summary>
        /// Gets a reference to the nhibernate session factory that the service host
        /// factory is using.
        /// </summary>
        public ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates an NHibernate configuration object for the specified server.
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        private NHibernate.Cfg.Configuration CreateNHibernateConfiguration(IDatabaseServer server)
        {
            // Create the configuration object and set up the basic config elements.
            NHibernate.Cfg.Configuration config = new NHibernate.Cfg.Configuration();
            config.DataBaseIntegration(db =>
            {
                db.Driver<DbDriver>();
                db.Dialect<DbDialect>();

                db.ConnectionString = server.ConnectionString;
                db.ConnectionStringName = ConnectionStringName;
                db.Timeout = server.CommandTimeout;

                // enabled for testing
                db.LogFormattedSql = server.DebugMode;
                db.AutoCommentSql = server.DebugMode;
            });

            // Next add the mappings.
            HbmMapping mappings = GetNHibernateMappings();
            config.AddMapping(mappings);

            // Add the event listeners
            AuditEventListener auditEventListener = new AuditEventListener();
            config.SetListener(ListenerType.PreInsert, auditEventListener);
            config.SetListener(ListenerType.PreUpdate, auditEventListener);

            return config;
        }

        /// <summary>
        /// Creates a service host for the specified type.
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public ServiceHost CreateServiceHost(Type serviceType)
        {
            return CreateServiceHost(serviceType, null);
        }

        /// <summary>
        /// Creates a <see cref="ServiceHost"/> for a specified type of service with a
        /// specific base address.
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="baseAddresses"></param>
        /// <returns></returns>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            Log.Message("Creating new instance of the '{0}' service for the '{1}' server.", serviceType.Name, Server.Name);
            return new TransactionalServiceHost(Server, _transactionManagerFactory, serviceType);
        }

        /// <summary>
        /// Gets the nhibernate mappings to use.
        /// </summary>
        /// <returns></returns>
        private HbmMapping GetNHibernateMappings()
        {
            Log.Message("Using {0} as the source for the nhibernate mapping files.", MappingsAssembly.FullName);

            ModelMapper mapper = new ModelMapper();
            mapper.AddMappings(MappingsAssembly.GetExportedTypes());
            return mapper.CompileMappingForAllExplicitlyAddedEntities();
        }

        /// <summary>
        /// Callback for logging messages generated during the schema update validation.
        /// </summary>
        /// <param name="line"></param>
        private void SchemaUpdateScriptAction(string line)
        {
            Log.ErrorCtx("Schema Validation", line);
        }
        #endregion
    }
}
