﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionalServicesContextInitializer.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Persistence.Transactions;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Custom instance context initialiser that adds an instance of the
    /// <see cref="TransactionServicesContextExtension"/> class to the WCF context.
    /// </summary>
    public class TransactionalServiceContextInitializer : IInstanceContextInitializer
    {
        #region Fields
        /// <summary>
        /// Reference to the transaction manager factory.
        /// </summary>
        private readonly ITransactionManagerFactory _factory;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="TransactionalServiceContextInitializer"/> class using the
        /// specified transaction manager factory.
        /// </summary>
        /// <param name="factory"></param>
        public TransactionalServiceContextInitializer(ITransactionManagerFactory factory)
        {
            _factory = factory;
        }
        #endregion

        #region IInstanceContextInitializer Implementation
        /// <summary>
        /// Provides the ability to modify the newly created <see cref="InstanceContext"/>
        /// object.
        /// </summary>
        /// <param name="instanceContext"></param>
        /// <param name="message"></param>
        public void Initialize(InstanceContext instanceContext, Message message)
        {
            TransactionalServiceContextExtension extension =
                new TransactionalServiceContextExtension(_factory);
            instanceContext.Extensions.Add(extension);
        }
        #endregion
    }
}
