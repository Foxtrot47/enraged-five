﻿//---------------------------------------------------------------------------------------------
// <copyright file="UniversalLogLogger.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using NHibernate;
using RSG.Base.Logging;

namespace RSG.Services.Persistence.Logging
{
    /// <summary>
    /// Custom NHibernate logger class which logs to an universal log.
    /// </summary>
    public class UniversalLogLogger : IInternalLogger
    {
        #region Fields
        /// <summary>
        /// The context to use when logging messages.
        /// </summary>
        private readonly string _context;

        /// <summary>
        /// The universal log object to log messages to.
        /// </summary>
        private readonly ILog _log;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogLogger"/> class
        /// using the specified log object and context.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="context"></param>
        public UniversalLogLogger(ILog log, string context)
        {
            _context = context;
            _log = log;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a flag indicating whether error messages are enabled.
        /// </summary>
        public bool IsErrorEnabled
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a flag indicating whether fatal messages are enabled.
        /// </summary>
        public bool IsFatalEnabled
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a flag indicating whether debug messages are enabled.
        /// </summary>
        public bool IsDebugEnabled
        {
            get
            {
#warning This needs to be fixed once ILogTargets aren't using events anymore and get their log levels from the LogFactory.
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }

        /// <summary>
        /// Gets a flag indicating whether info messages are enabled.
        /// </summary>
        public bool IsInfoEnabled
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a flag indicating whether warning messages are enabled.
        /// </summary>
        public bool IsWarnEnabled
        {
            get { return true; }
        }
        #endregion

        #region IInternalLogger Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Error(object message)
        {
            _log.ErrorCtx(_context, Log.EscapeMessage(message.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Error(object message, Exception exception)
        {
            IExceptionFormatter formatter = new ExceptionFormatter(exception);
            List<String> messages = new List<String>();
            messages.Add(Log.EscapeMessage(message.ToString()));
            messages.AddRange(formatter.Format());

            _log.ErrorCtx(_context, String.Join(Environment.NewLine, messages));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void ErrorFormat(string format, params object[] args)
        {
            _log.ToolErrorCtx(_context, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Fatal(object message)
        {
            _log.ToolErrorCtx(_context, Log.EscapeMessage(message.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Fatal(object message, Exception exception)
        {
            _log.ToolExceptionCtx(_context, exception, Log.EscapeMessage(message.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Debug(object message)
        {
            if (IsDebugEnabled)
            {
                _log.DebugCtx(_context, Log.EscapeMessage(message.ToString()));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Debug(object message, Exception exception)
        {
            IExceptionFormatter formatter = new ExceptionFormatter(exception);
            List<String> messages = new List<String>();
            messages.Add(Log.EscapeMessage(message.ToString()));
            messages.AddRange(formatter.Format());

            if (IsDebugEnabled)
            {
                _log.DebugCtx(_context, String.Join(Environment.NewLine, messages));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void DebugFormat(string format, params object[] args)
        {
            if (IsDebugEnabled)
            {
                _log.DebugCtx(_context, format, args);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Info(object message)
        {
            _log.MessageCtx(_context, Log.EscapeMessage(message.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Info(object message, Exception exception)
        {
            IExceptionFormatter formatter = new ExceptionFormatter(exception);
            List<String> messages = new List<String>();
            messages.Add(Log.EscapeMessage(message.ToString()));
            messages.AddRange(formatter.Format());

            _log.MessageCtx(_context, String.Join(Environment.NewLine, messages));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void InfoFormat(string format, params object[] args)
        {
            _log.MessageCtx(_context, format, args);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public void Warn(object message)
        {
            _log.WarningCtx(_context, Log.EscapeMessage(message.ToString()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Warn(object message, Exception exception)
        {
            IExceptionFormatter formatter = new ExceptionFormatter(exception);
            List<String> messages = new List<String>();
            messages.Add(Log.EscapeMessage(message.ToString()));
            messages.AddRange(formatter.Format());

            _log.WarningCtx(_context, String.Join(Environment.NewLine, messages));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void WarnFormat(string format, params object[] args)
        {
            _log.WarningCtx(_context, format, args);
        }
        #endregion
    }
}
