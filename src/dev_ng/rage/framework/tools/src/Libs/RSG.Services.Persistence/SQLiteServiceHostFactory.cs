//---------------------------------------------------------------------------------------------
// <copyright file="SQLiteServiceHostFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using NHibernate.Dialect;
using NHibernate.Driver;
using RSG.Services.Configuration;
using FluentMigrator.Runner.Processors.SQLite;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Custom service host factory that creates transactional service hosts.
    /// </summary>
    public abstract class SQLiteServiceHostFactory<ServerConfig> :
        TransactionalServiceHostFactory<ServerConfig, SqliteProcessorFactory, SQLite20Driver, SQLiteDialect> 
        where ServerConfig : IDatabaseServer
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SQLiteServiceHostFactory"/>
        /// class using the specified server configuration object.
        /// </summary>
        public SQLiteServiceHostFactory(ServerConfig server, bool wipeDatabase)
            : base(server, wipeDatabase)
        {
        }
        #endregion
    }
}
