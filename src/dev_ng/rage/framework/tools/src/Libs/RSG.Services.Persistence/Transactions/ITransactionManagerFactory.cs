﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITransactionManagerFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Persistence.Transactions
{
    /// <summary>
    /// In charge of creating new transaction manager objects.
    /// </summary>
    public interface ITransactionManagerFactory
    {
        /// <summary>
        /// Creates a new transaction manager object.
        /// </summary>
        /// <param name="readOnly">
        /// Flag indicating whether the manager only allows read-only operations.
        /// </param>
        /// <returns></returns>
        ITransactionManager CreateTransactionManager(bool readOnly);
    }
}
