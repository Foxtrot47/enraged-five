//---------------------------------------------------------------------------------------------
// <copyright file="MySQLServiceHostFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using NHibernate.Dialect;
using NHibernate.Driver;
using RSG.Services.Configuration;
using FluentMigrator.Runner.Processors.MySql;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Custom service host factory that creates transactional service hosts.
    /// </summary>
    public abstract class MySQLServiceHostFactory<ServerConfig> :
        TransactionalServiceHostFactory<ServerConfig, MySqlProcessorFactory, MySqlDataDriver, MySQL5Dialect> 
        where ServerConfig : IDatabaseServer
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MySQLServiceHostFactory"/>
        /// class using the specified server configuration object.
        /// </summary>
        public MySQLServiceHostFactory(ServerConfig server, bool wipeDatabase)
            : base(server, wipeDatabase)
        {
        }
        #endregion
    }
}
