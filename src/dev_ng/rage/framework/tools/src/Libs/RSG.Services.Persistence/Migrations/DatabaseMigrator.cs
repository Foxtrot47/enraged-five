﻿//---------------------------------------------------------------------------------------------
// <copyright file="DatabaseMigrator.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Reflection;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.MySql;
using RSG.Base.Logging;

namespace RSG.Services.Persistence.Migrations
{
    /// <summary>
    /// In charge of running the migrations against the database.
    /// </summary>
    public class DatabaseMigrator<Migrator> 
        where Migrator : IMigrationProcessorFactory, new()
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const string LogCtx = "Database Migrator";
        #endregion

        #region Fields
        /// <summary>
        /// Database connection string.
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Migrator log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Reference to the assembly that contains the migrations.
        /// </summary>
        private readonly Assembly _migrationAssembly;

        /// <summary>
        /// Namespace that contains the migrations.
        /// </summary>
        private readonly string _migrationNamespace;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DatabaseMigrator"/> class using the
        /// provided connection string.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="migrationAssembly"></param>
        /// <param name="migrationNamespace"></param>
        /// <param name="log"></param>
        public DatabaseMigrator(string connectionString, Assembly migrationAssembly, string migrationNamespace, ILog log)
        {
            _connectionString = connectionString;
            _log = log;
            _migrationAssembly = migrationAssembly;
            _migrationNamespace = migrationNamespace;
        }
        #endregion

        #region Classes
        /// <summary>
        /// Migration processor options.
        /// </summary>
        private class MigrationOptions : IMigrationProcessorOptions
        {
            public bool PreviewOnly { get; set; }
            public int Timeout { get; set; }
            public string ProviderSwitches { get; set; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Runs a migration action against the database.
        /// </summary>
        /// <param name="runnerAction"></param>
        /// <param name="previewOnly"></param>
        public void Migrate(Action<IMigrationRunner> runnerAction, bool previewOnly = false)
        {
            IMigrationProcessorOptions options = new MigrationOptions { PreviewOnly = previewOnly, Timeout = 0 };
            IMigrationProcessorFactory factory = new Migrator();

            TextWriterAnnouncer announcer = new TextWriterAnnouncer(message => _log.Message(message));
            IRunnerContext migrationContext = new RunnerContext(announcer)
            {
                Namespace = _migrationNamespace,
                NestedNamespaces = true
            };
            IMigrationProcessor processor = factory.Create(_connectionString, announcer, options);
            IMigrationRunner runner = new MigrationRunner(_migrationAssembly, migrationContext, processor);

            using (new ProfileContext(_log, LogCtx, "Running migration action."))
            {
                runnerAction(runner);
            }
        }
        #endregion
    }
}
