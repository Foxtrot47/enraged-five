﻿//---------------------------------------------------------------------------------------------
// <copyright file="DatetimeMigrationAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using FluentMigrator;

namespace RSG.Services.Persistence.Migrations
{
    /// <summary>
    /// Custom migration attribute that enforces a particular migration numbering convention.
    /// </summary>
    public class DatetimeMigrationAttribute : MigrationAttribute
    {
        /// <summary>
        /// Private field for the <see cref="Author"/> property.
        /// </summary>
        private readonly string _author;

        /// <summary>
        /// Initialises a new instnace of the <see cref="DatetimeMigrationAttibute"/> using the
        /// provided values.
        /// </summary>
        /// <param name="author"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <param name="description"></param>
        public DatetimeMigrationAttribute(string author, int year, int month, int day, int hour, int minute, string description = null, TransactionBehavior transactionBehavior = TransactionBehavior.Default)
            : base(CalculateValue(year, month, day, hour, minute), transactionBehavior, description)
        {
            _author = author;
        }

        /// <summary>
        /// Author of this migration.
        /// </summary>
        public string Author
        {
            get { return _author; }
        }

        /// <summary>
        /// Calculates a migration number from the provided date/time parameters.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <returns></returns>
        private static long CalculateValue(int year, int month, int day, int hour, int minute)
        {
            return year * 100000000L + month * 1000000L + day * 10000L + hour * 100L + minute;
        }
    }
}
