﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionalServicesContextExtension.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.ServiceModel;
using RSG.Services.Persistence.Transactions;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Extension to WCF's operation context to supply a reference to the transaction
    /// manager factory instance.
    /// </summary>
    public class TransactionalServiceContextExtension : IExtension<InstanceContext>
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="TransactionManagerFactory"/> property.
        /// </summary>
        private readonly ITransactionManagerFactory _factory;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="TransactionalServiceContextExtension"/> class using the specified
        /// transaction manager factory.
        /// </summary>
        /// <param name="factory"></param>
        public TransactionalServiceContextExtension(ITransactionManagerFactory factory)
        {
            _factory = factory;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the transaction manager factory instance.
        /// </summary>
        public ITransactionManagerFactory TransactionManagerFactory
        {
            get { return _factory; }
        }
        #endregion

        #region IExtension<InstanceContext> Implementation
        /// <summary>
        /// Enables an extension object to find out when it has been aggregated. Called
        /// when the extension is added to the System.ServiceModel.IExtensibleObject<T>.Extensions
        /// property.
        /// </summary>
        /// <param name="owner"></param>
        public void Attach(InstanceContext owner)
        {
        }

        /// <summary>
        /// Enables an object to find out when it is no longer aggregated. Called when
        /// an extension is removed from the System.ServiceModel.IExtensibleObject<T>.Extensions
        /// property.
        /// </summary>
        /// <param name="owner"></param>
        public void Detach(InstanceContext owner)
        {
        }
        #endregion
    }
}
