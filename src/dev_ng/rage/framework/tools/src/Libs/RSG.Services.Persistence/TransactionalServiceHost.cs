﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionalServiceHost.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Persistence.Transactions;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Custom service host that obtains its configuration data from the server
    /// configuration data as well as adding a custom service behaviour that initialises
    /// the database.
    /// </summary>
    public class TransactionalServiceHost : ConfigServiceHost
    {
        #region Fields
        /// <summary>
        /// Reference to the transaction manager factory.
        /// </summary>
        private readonly ITransactionManagerFactory _factory;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TransactionalServiceHost"/> class using
        /// the specified server configuration and service singleton instance.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="factory"></param>
        /// <param name="singletonInstance"></param>
        public TransactionalServiceHost(IServer server, ITransactionManagerFactory factory, object singletonInstance)
            : base(server, singletonInstance)
        {
            _factory = factory;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="TransactionalServiceHost"/> class using
        /// the specified server configuration and service type.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="factory"></param>
        /// <param name="serviceType"></param>
        public TransactionalServiceHost(IServer server, ITransactionManagerFactory factory, Type serviceType)
            : base(server, serviceType)
        {
            _factory = factory;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Invoked during the transition of a communication object into the opening
        /// state.
        /// </summary>
        protected override void OnOpening()
        {
            // Let the base class do the majority of the work.
            base.OnOpening();

            // Simply add the extra service behaviour.
            Description.Behaviors.Add(new TransactionalServiceBehaviour(_factory));
        }
        #endregion
    }
}
