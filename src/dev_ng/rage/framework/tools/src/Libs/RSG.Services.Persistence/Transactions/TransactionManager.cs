﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionManager.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using NHibernate;
using RSG.Base.Logging;

namespace RSG.Services.Persistence.Transactions
{
    /// <summary>
    /// NHibernate based transaction manager
    /// </summary>
    public class TransactionManager : ITransactionManager
    {
        #region Fields
        /// <summary>
        /// Flag indicating whether the manager is currently inside a transaction.
        /// </summary>
        private bool _inTransaction;

        /// <summary>
        /// Log object.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Reference to the NHibernate session.
        /// </summary>
        private readonly ISession _session;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TransactionManager"/> class
        /// using the specified NHibernate session.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="session"></param>
        public TransactionManager(ILog log, ISession session)
            : base()
        {
            _log = log;
            _session = session;
        }
        #endregion

        #region ITransactionManager Implementation
        /// <summary>
        ///  Execute an action that doesn't require a return value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        public void ExecuteAction(Action<ISession> action)
        {
            try
            {
                BeginTransaction();
                action.Invoke(_session);
                CommitTransaction();
            }
            catch (Exception e)
            {
                _log.ToolException(e, "Unexpected exception occurred while executing an action.");
                Rollback();
                throw;
            }
        }

        /// <summary>
        /// Execute an action that returns a result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        public T ExecuteAction<T>(Func<ISession, T> action)
        {
            try
            {
                BeginTransaction();
                T result = action.Invoke(_session);
                CommitTransaction();
                return result;
            }
            catch (Exception e)
            {
                _log.ToolException(e, "Unexpected exception occurred while executing an action.");
                Rollback();
                throw;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Starts a new transaction.
        /// </summary>
        private void BeginTransaction()
        {
            _inTransaction = true;
            if (!_session.Transaction.IsActive)
            {
                _session.BeginTransaction();
            }
        }

        /// <summary>
        /// Commits any database changes that were made since the transaction was started.
        /// </summary>
        private void CommitTransaction()
        {
            if (_session.Transaction.IsActive)
            {
                _session.Transaction.Commit();
            }
            _inTransaction = false;
        }

        /// <summary>
        /// Rolls back any changes that have been made to the database since the transaction was started.
        /// </summary>
        private void Rollback()
        {
            if (_session.Transaction.IsActive)
            {
                _session.Transaction.Rollback();
            }
            _inTransaction = false;
        }
        #endregion

        #region IDisposable Implementation
        /// <summary>
        /// Flag indicating whether this object has been disposed.
        /// </summary>
        protected bool _disposed = false;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or
        /// resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Allows for derived classes to override the disposing behaviour.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            if (!_disposed)
            {
                if (_inTransaction)
                {
                    Rollback();
                }

                if (_session != null && _session.IsOpen)
                {
                    _session.Close();
                }
            }

            _disposed = true;
        }
        #endregion
    }
}
