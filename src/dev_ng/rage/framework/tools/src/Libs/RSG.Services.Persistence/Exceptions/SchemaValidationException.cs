﻿//---------------------------------------------------------------------------------------------
// <copyright file="SchemaValidationException.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace RSG.Services.Persistence.Exceptions
{
    /// <summary>
    /// Exception that gets thrown if the NHibernate schema doesn't match what the fluent
    /// migration has created.
    /// </summary>
    public class SchemaValidationException : Exception
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SchemaValidationException"/>
        /// class using the specified message.
        /// </summary>
        /// <param name="message"></param>
        public SchemaValidationException(string message)
            : base(message)
        {
        }
        #endregion
    }
}
