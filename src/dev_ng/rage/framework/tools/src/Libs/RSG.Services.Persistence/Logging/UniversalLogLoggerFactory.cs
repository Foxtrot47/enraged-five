﻿//---------------------------------------------------------------------------------------------
// <copyright file="UniversalLogLoggerFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Collections.Generic;
using NHibernate;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Services.Persistence.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public class UniversalLogLoggerFactory : ILoggerFactory
    {
        #region Fields
        /// <summary>
        /// Map of key names to logger objects.
        /// </summary>
        private readonly IDictionary<string, UniversalLogLogger> _loggers =
            new Dictionary<string, UniversalLogLogger>();
        
        /// <summary>
        /// Thread synchronization object for accessing the <see cref="_loggers"/> field.
        /// </summary>
        private readonly object _syncObject = new object();

        /// <summary>
        /// 
        /// </summary>
        private readonly ILog _universalLog;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="UniversalLogLoggerFactory"/>
        /// class.
        /// </summary>
        public UniversalLogLoggerFactory()
        {
            _universalLog = new UniversalLog("NHibnernate");
            LogFactory.CreateUniversalLogFile((IUniversalLog)_universalLog);
            if (LogFactory.ApplicationConsoleLog != null)
            {
                LogFactory.ApplicationConsoleLog.Connect(_universalLog);
            }
        }
        #endregion

        #region ILoggerFactory Methods
        /// <summary>
        /// Retrieves a logger for a particular key name.
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public IInternalLogger LoggerFor(string keyName)
        {
            UniversalLogLogger logger;
            lock (_syncObject)
            {
                if (!_loggers.TryGetValue(keyName, out logger))
                {
                    logger = new UniversalLogLogger(_universalLog, keyName);
                    _loggers.Add(keyName, logger);
                }
            }

            return logger;
        }

        /// <summary>
        /// Retrieves a logger for a particular type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IInternalLogger LoggerFor(System.Type type)
        {
            return LoggerFor(type.FullName);
        }
        #endregion
    }
}
