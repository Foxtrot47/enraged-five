﻿//---------------------------------------------------------------------------------------------
// <copyright file="DomainEntityBase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.Persistence.Data
{
    /// <summary>
    /// Base class for domain entities.
    /// </summary>
    public abstract class DomainEntityBase : IDomainEntity
    {
        #region Properties
        /// <summary>
        /// Primary key for the entity.
        /// </summary>
        /// <remarks>
        /// This will automatically be set by the native id generator.
        /// </remarks>
        public virtual long Id { get; set; }
        #endregion
    }
}
