﻿//---------------------------------------------------------------------------------------------
// <copyright file="DomainEntityBaseMap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Persistence.Mappings
{
    /// <summary>
    /// Abstract base class for NHibernate entities that inherit from
    /// <see cref="DomainEntityBase"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DomainEntityBaseMap<T> : ClassMapping<T> where T : class, IDomainEntity
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="DomainEntityBaseMap"/> class.
        /// </summary>
        protected DomainEntityBaseMap()
        {
            Id(x => x.Id, m => m.Generator(Generators.Identity));
        }
    }
}
