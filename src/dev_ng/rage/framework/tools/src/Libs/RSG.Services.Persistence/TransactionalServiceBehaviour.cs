﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionalServiceBehaviourAttribute.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using RSG.Services.Persistence.Transactions;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Custom service behaviour for adding a new
    /// <see cref="TransactionalServiceContextInitializer"/> instance to the services
    /// endpoints.
    /// </summary>
    public class TransactionalServiceBehaviour : IServiceBehavior
    {
        #region Fields
        /// <summary>
        /// Reference to the transaction manager factory.
        /// </summary>
        private readonly ITransactionManagerFactory _factory;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TransactionServiceBehaviour"/>
        /// class using the specified transaction manager factory.
        /// </summary>
        /// <param name="factory"></param>
        public TransactionalServiceBehaviour(ITransactionManagerFactory factory)
        {
            _factory = factory;
        }
        #endregion

        #region IServiceBehavior Implementation
        /// <summary>
        /// Provides the ability to pass custom data to binding elements to support the
        /// contract implementation.
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        /// <summary>
        /// Provides the ability to change run-time property values or insert custom
        /// extension objects such as error handlers, message or parameter interceptors,
        /// security extensions, and other custom extension objects.
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        /// <param name="endpoints"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Provides the ability to inspect the service host and the service description
        /// to confirm that the service can run successfully.
        /// </summary>
        /// <param name="serviceDescription"></param>
        /// <param name="serviceHostBase"></param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher channelDispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher endpoint in channelDispatcher.Endpoints)
                {
                    endpoint.DispatchRuntime.InstanceContextInitializers.Add(new TransactionalServiceContextInitializer(_factory));
                }
            }
        }
        #endregion
    }
}
