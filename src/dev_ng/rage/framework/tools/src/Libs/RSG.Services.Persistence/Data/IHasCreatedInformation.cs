﻿//---------------------------------------------------------------------------------------------
// <copyright file="IHasCreatedInformation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace RSG.Services.Persistence.Data
{
    /// <summary>
    /// Interface for domain entities that contain creation information.
    /// </summary>
    public interface IHasCreatedInformation
    {
        /// <summary>
        /// Gets or sets the timestamp relating to the last time this entity was created.
        /// </summary>
        DateTime CreatedOn { get; set; }
    }
}
