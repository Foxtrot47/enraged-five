﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionManagerFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using NHibernate;
using RSG.Base.Logging;

namespace RSG.Services.Persistence.Transactions
{
    /// <summary>
    /// NHibernate based transaction manager factory
    /// </summary>
    public class TransactionManagerFactory : ITransactionManagerFactory
    {
        #region Fields
        /// <summary>
        /// Log object for the transaction manager factory.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// Factory for creating NHibernate sessions.
        /// </summary>
        private readonly ISessionFactory _sessionFactory;

        /// <summary>
        /// Flag indicating whether we should be running in read-only mode.
        /// </summary>
        private bool _forceReadOnly;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialise a new instance of the <see cref="TransactionManagerFactory"/>
        /// class using the nhivnerate session factory and read only flag.
        /// </summary>
        /// <param name="sessionFactory"></param>
        /// <param name="forceReadOnly"></param>
        public TransactionManagerFactory(ISessionFactory sessionFactory, bool forceReadOnly)
        {
            _log = LogFactory.CreateUniversalLog("TransactionManagerFactory");
            _sessionFactory = sessionFactory;
            _forceReadOnly = forceReadOnly;
        }
        #endregion

        #region ITransactionManagerFactory Implementation
        /// <summary>
        /// Creates a new transaction manager object.
        /// </summary>
        /// <param name="readOnly">
        /// Flag indicating whether the manager only allows read-only operations.
        /// </param>
        /// <returns></returns>
        public ITransactionManager CreateTransactionManager(bool readOnly)
        {
            ISession session = _sessionFactory.OpenSession();
            session.DefaultReadOnly = _forceReadOnly || readOnly;
            return new TransactionManager(_log, session);
        }
        #endregion
    }
}
