﻿//---------------------------------------------------------------------------------------------
// <copyright file="IHasModifiedInformation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;

namespace RSG.Services.Persistence.Data
{
    /// <summary>
    /// Interface for domain entities that contain modification information.
    /// </summary>
    public interface IHasModifiedInformation
    {
        /// <summary>
        /// Gets or sets the timestamp relating to the last time this entity was modified.
        /// </summary>
        DateTime ModifiedOn { get; set; }
    }
}
