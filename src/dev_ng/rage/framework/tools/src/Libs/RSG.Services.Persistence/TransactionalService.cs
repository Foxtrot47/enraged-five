﻿//---------------------------------------------------------------------------------------------
// <copyright file="TransactionalService.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using NHibernate;
using RSG.Base.Logging;
using RSG.Services.Configuration;
using RSG.Services.Persistence.Transactions;
using System.ServiceModel;
using System.Diagnostics;

namespace RSG.Services.Persistence
{
    /// <summary>
    /// Base class for WCF services that adds support for performing database
    /// transactions.
    /// </summary>
    public abstract class TransactionalService : ConfigService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="TransactionalService"/> class using the
        /// specified server configuration.
        /// </summary>
        /// <param name="server"></param>
        protected TransactionalService(IServer server)
            : base(server)
        {
        }
        #endregion

        #region Transaction Wrappers
        /// <summary>
        /// Executes a synchronously command through the transaction manager
        /// </summary>
        /// <param name="action"></param>
        /// <param name="readOnly"></param>
        protected void ExecuteAsTransaction(Action<ISession> action, bool readOnly = false)
        {
            using (ITransactionManager manager = GetTransactionManagerFactory().CreateTransactionManager(readOnly))
            {
                manager.ExecuteAction(action);
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        /// <param name="action"></param>
        protected void ExecuteAsReadOnlyTransaction(Action< ISession> action)
        {
            ExecuteAsTransaction(action, true);
        }

        /// <summary>
        /// Executes a command synchronously through the transaction manager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="readOnly"></param>
        /// <returns></returns>
        protected T ExecuteAsTransaction<T>(Func<ISession, T> action, bool readOnly = false)
        {
            using (ITransactionManager manager = GetTransactionManagerFactory().CreateTransactionManager(readOnly))
            {
                return manager.ExecuteAction(action);
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly transaction.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        protected T ExecuteAsReadOnlyTransaction<T>(Func<ISession, T> action)
        {
            return ExecuteAsTransaction(action, true);
        }
        #endregion

        #region OperationContext Methods
        /// <summary>
        /// Gets a reference to the transaction manager factory.
        /// </summary>
        /// <returns></returns>
        private ITransactionManagerFactory GetTransactionManagerFactory()
        {
            TransactionalServiceContextExtension serviceContext =
                OperationContext.Current.InstanceContext.Extensions.Find<TransactionalServiceContextExtension>();
            Debug.Assert(serviceContext != null, "TransactionalServiceContextExtension not found in the operation context extensions.  Are you using the right service host (i.e. a TransactionalServiceHost)?");
            if (serviceContext == null)
            {
                throw new ArgumentNullException("TransactionalServiceContextExtension not found in the operation context extensions.");
            }
            return serviceContext.TransactionManagerFactory;
        }
        #endregion
    }
}
