﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.ToolsService.Contracts
{

    /// <summary>
    /// Service state enumeration.
    /// </summary>
    public enum ServiceState
    {
        /// <summary>
        /// Service is not running.
        /// </summary>
        Stopped,

        /// <summary>
        /// Service is running.
        /// </summary>
        Running,

        /// <summary>
        /// Service has been disabled.
        /// </summary>
        Disabled,

        /// <summary>
        /// Service has faulted (exception during startup).
        /// </summary>
        Faulted,
    }

} // RSG.ToolsService.Contracts namespace
