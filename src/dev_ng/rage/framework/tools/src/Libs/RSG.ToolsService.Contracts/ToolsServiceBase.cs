﻿//---------------------------------------------------------------------------------------------
// <copyright file="IToolsService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.Contracts
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;

    /// <summary>
    /// Abstract base class for all tool services.
    /// </summary>
    public abstract class ToolsServiceBase : 
        IToolsService,
        INotifyPropertyChanged
    {
        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Service name.
        /// </summary>
        public String ServiceName 
        {
            get { return _serviceName; }
            protected set { this.SetProperty(ref _serviceName, value); }
        }

        /// <summary>
        /// Service description string.
        /// </summary>
        public String Description 
        {
            get { return _description; }
            protected set { this.SetProperty(ref _description, value); }
        }

        /// <summary>
        /// Service status.
        /// </summary>
        public ServiceState Status 
        {
            get { return _status; }
            private set { this.SetProperty(ref _status, value); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Service name field.
        /// </summary>
        private String _serviceName;

        /// <summary>
        /// Description field.
        /// </summary>
        private String _description;

        /// <summary>
        /// Service status field.
        /// </summary>
        private ServiceState _status;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ToolsServiceBase()
        {
            this.Status = ServiceState.Stopped;
        }

        /// <summary>
        /// Constructor; should only be used to disable services in code.
        /// </summary>
        /// <param name="initialState"></param>
        protected ToolsServiceBase(ServiceState initialState)
        {
            this.Status = initialState;
        }
        #endregion // Constructor(s)

        #region Abstract Methods
        /// <summary>
        /// Service call invoked when services are starting.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        protected abstract void OnStart(IUniversalLog log, IConfig config);

        /// <summary>
        /// Service call invoked when services are shutting down.
        /// </summary>
        /// <param name="log"></param>
        protected abstract void OnStop(IUniversalLog log);
        #endregion // Abstract Methods

        #region IToolsService Controller Methods
        /// <summary>
        /// Service call to start service; implemented in base class to abstract the
        /// ServiceState member.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        public void Start(IUniversalLog log, IConfig config)
        {
            try
            {
                this.OnStart(log, config);
                this.Status = ServiceState.Running; 
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Exception starting service: {0}.", this.ServiceName);
                this.Status = ServiceState.Faulted;
            }
        }

        /// <summary>
        /// Service call to stop service; implemented in base class to abstract the
        /// ServiceState member.
        /// </summary>
        /// <param name="log"></param>
        public void Stop(IUniversalLog log)
        {
            try
            {
                this.OnStop(log);
                this.Status = ServiceState.Stopped; 
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Exception stopping service: {0}.", this.ServiceName);
                this.Status = ServiceState.Faulted;
            }
        }
        #endregion // IToolsService Controller Methods

        #region INotifyPropertyChanged Support Methods
        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="names">
        /// A array of property names that have been changed.
        /// </param>
        public void NotifyPropertyChanged(params string[] names)
        {
            this.NotifyPropertyChanged(Enumerable.Distinct(names));
        }

        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        protected void NotifyPropertyChanged([CallerMemberName]string name = "")
        {
            this.NotifyPropertyChanged(Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Fires the property changed event for the specified property names.
        /// </summary>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        protected virtual void NotifyPropertyChanged(IEnumerable<string> names)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (names == null || handler == null)
            {
                return;
            }

            foreach (string name in names)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName]string name = "")
        {
            return this.SetProperty(ref field, value, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="name">
        /// The name of the property that has been changed.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(
            ref T field,
            T value,
            IEqualityComparer<T> comparer,
            [CallerMemberName]string name = "")
        {
            return this.SetProperty(ref field, value, comparer, Enumerable.Repeat(name, 1));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// An array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(ref T field, T value, params string[] names)
        {
            return this.SetProperty(ref field, value, Enumerable.Distinct(names));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// An array of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, params string[] names)
        {
            return this.SetProperty(ref field, value, comparer, Enumerable.Distinct(names));
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected bool SetProperty<T>(ref T field, T value, IEnumerable<string> names)
        {
            return this.SetProperty(ref field, value, EqualityComparer<T>.Default, names);
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct property changed
        /// event is fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the field that is getting set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field that is getting set.
        /// </param>
        /// <param name="value">
        /// The value to set the field to.
        /// </param>
        /// <param name="comparer">
        /// The equality comparer to use to determine whether the value has changed.
        /// </param>
        /// <param name="names">
        /// A iterator around all of property names that are changed due to this one change.
        /// </param>
        /// <returns>
        /// True if the property was changed; otherwise, false.
        /// </returns>
        protected virtual bool SetProperty<T>(
            ref T field, T value, IEqualityComparer<T> comparer, IEnumerable<string> names)
        {
            if (comparer.Equals(field, value))
            {
                return false;
            }

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
            {
                return true;
            }

            foreach (string name in names)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }

            return true;
        }
        #endregion // INotifyPropertyChanged Support Methods
    }

} // RSG.ToolsService.Contracts namespace
