﻿//---------------------------------------------------------------------------------------------
// <copyright file="IToolsService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.Contracts
{
    using System;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;

    /// <summary>
    /// Tools service interface; provides an interface for all long-running tool services
    /// to adhere to.  These are not strict Windows services as we require user-interaction.
    /// </summary>
    public interface IToolsService
    {
        #region Properties
        /// <summary>
        /// Service name.
        /// </summary>
        String ServiceName { get; }

        /// <summary>
        /// Service description string.
        /// </summary>
        String Description { get; }

        /// <summary>
        /// Service status.
        /// </summary>
        ServiceState Status { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Service call invoked when services are starting.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        void Start(IUniversalLog log, IConfig config);

        /// <summary>
        /// Service call invoked when services are shutting down.
        /// </summary>
        /// <param name="log"></param>
        void Stop(IUniversalLog log);
        #endregion // Methods
    }

} // RSG.ToolsService.Contracts namespace
