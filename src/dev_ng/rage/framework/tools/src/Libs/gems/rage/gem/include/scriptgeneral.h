%include "rdoc.i"

%extend Util {

	RDOCCOMPLEX(init,"init() -> bool","initialises builder environment with platform set to independent")
	bool init() {

		RageGemInit();
		
		RBSetStdOut(RageLogOutFn);
		RBSetStdErr(RageLogErrFn);
	
		return true;
	}

	RDOCCOMPLEX(shutdown,"shutdown() -> bool","shut down the builder environment")
	bool shutdown() {

		RageGemShutdown();
		return true;
	}

	RDOCSIMPLE(set_root_directory)
	bool set_root_directory(const char* path) {

		return ( false );
		//return RBSetRootDirectory(path);
	}

	RDOCSIMPLE(get_param)
	const char* get_param(const char* param) {

		return RBGetParam(param);
	}

	RDOCSIMPLE(wait)
	bool wait(void) {

		return RBWait();
	}

	RDOCSIMPLE(set_log_level)
	bool set_log_level(int loglevel) {

		return RBSetLogLevel(loglevel);
	}

	RDOCSIMPLE(set_stop_on_error)
	bool set_stop_on_error(bool stoponerror) {

		return RBSetStopOnError(stoponerror);
	}

	RDOCSIMPLE(report_error)
	bool report_error(const char* msg) {

		return RBReportError(msg);
	}

	RDOCSIMPLE(report_warning)
	bool report_warning(const char* msg) {

		return RBReportWarning(msg);
	}

	RDOCSIMPLE(report_quit)
	bool report_quit(const char* msg) {

		return RBReportQuit(msg);
	}

	RDOCSIMPLE(report_display)
	bool report_display(const char* msg) {

		return RBReportDisplay(msg);
	}

	RDOCSIMPLE(report_trace1)
	bool report_trace1(const char* msg) {

		return RBReportTrace1(msg);
	}

	RDOCSIMPLE(report_trace2)
	bool report_trace2(const char* msg) {

		return RBReportTrace2(msg);
	}

	RDOCSIMPLE(report_trace3)
	bool ReportTrace3(const char* msg) {

		return RBReportTrace3(msg);
	}

	RDOCSIMPLE(del_file)
	bool del_file(const char* filename) {

		return RBDelFile(filename);
	}

	RDOCSIMPLE(del_dir)
	bool del_dir(const char* dir, bool contents) {

		return RBDelDir(dir,contents);
	}

	RDOCSIMPLE(find_files)
	const char* find_files(const char* path, bool relative, bool sorted) {

		return RBFindFiles(path, relative, sorted);
	}

	RDOCSIMPLE(find_platform_files)
	const char* find_platform_files(const char* path, bool relative, bool sorted) {

		return RBFindPlatformFiles(path, relative, sorted);
	}

	RDOCSIMPLE(find_dirs)
	const char* find_dirs(const char* path) {

		return RBFindDirs(path);
	}

	RDOCSIMPLE(create_leading_path)
	bool create_leading_path(const char* path) {

		return RBCreateLeadingPath(path);
	}

	RDOCSIMPLE(copy_file)
	bool copy_file(const char* src,const char* dest) {

		return RBCopy_File(src,dest);
	}

	RDOCSIMPLE(move_file)
	bool move_file(const char* src,const char* dest) {

		return RBMove_File(src,dest);
	}

	RDOCSIMPLE(file_exists)
	bool file_exists(const char* filename) {

		return RBFileExists(filename);
	}

	RDOCSIMPLE(directory_exists)
	bool directory_exists(const char* dir) {

		return RBDirectoryExists(dir);
	}

	RDOCSIMPLE(file_size)
	unsigned int file_size(const char* filename) {

		return RBFileSize(filename);
	}

	RDOCSIMPLE(get_version)
	const char* get_version(void) {

		return RBGetVersion();
	}
}
