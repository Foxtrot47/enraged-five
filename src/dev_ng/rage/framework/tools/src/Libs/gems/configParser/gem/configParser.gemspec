require 'rubygems'
require 'rake/gempackagetask'

ragegemspec = Gem::Specification.new do |s|
   s.name = 'configParser'
   s.platform = Gem::Platform::CURRENT
   s.version = '0.0.1'
   s.date = %q{Time.now.to_s}
   s.authors = ['David Muir']
   s.email = 'david.muir@rockstarnorth.com'
   s.summary = 'A wrapper for configParser library'
   s.homepage = 'http://www.rockstargames.com/'
   s.description = 'Provides script access to the pipeline configuration system.'
   s.files = [ 'configParser.so' ]
   s.require_path = '.'
end

Rake::GemPackageTask.new( ragegemspec ) do |pkg|
	pkg.need_tar = true
end
