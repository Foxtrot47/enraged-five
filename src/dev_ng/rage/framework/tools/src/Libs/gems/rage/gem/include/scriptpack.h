%include "rdoc.i"

%extend Pack {


	RDOCSIMPLE(start)
	bool start( bool includeMetaData = true ) {

		return RBStartPack( includeMetaData );
	}

	RDOCSIMPLE(start_uncompressed)
	bool start_uncompressed( bool includeMetaData = true ) {

		return RBStartUncompressedPack( includeMetaData );
	}

	RDOCSIMPLE(add)
	bool add(const char* source,const char* destination) {

		return RBAddToPack(source,destination);
	}

	RDOCSIMPLE(add_force_compression)
	bool add_force_compression(const char* source,const char* destination,bool compress) {

		return RBAddToPackForceCompression(source,destination,compress);
	}

	RDOCSIMPLE(save)
	bool save(const char* packname) {

		return RBSavePack(packname);
	}

	RDOCSIMPLE(close)
	bool close(void) {

		return RBClosePack();
	}

	RDOCSIMPLE(mount)
	bool mount(const char* packname, const char* mountpoint) {

		return RBMountPack(packname,mountpoint);
	}

	RDOCSIMPLE(unmount)
	bool unmount(const char* packname) {

		return RBUnmountPack(packname);
	}
}
