#ifndef __CONFIGPARSERGEM_MAINPAGE_H__
#define __CONFIGPARSERGEM_MAINPAGE_H__

/** 
 * @mainpage configParser - Ruby Gem
 * @author David Muir <david.muir@rockstarnorth.com>
 * @date 6 April 2010
 *
 * @section Gem Versioning
 *
 * @section SWIG Interface
 *
 * The swig_interface.cpp file is automatically generated in the solution
 * from the rage.i SWIG interface input file.  The file is checked
 * into Perforce as always writable on the client.
 *
 * @section Debugging the Gem
 *
 * Debugging the Rage Gem is now easier than before because we have a full
 * Visual Studio project for it, rather than relying on the old extconf.rb
 * method for building the Gem DLL.
 *
 *   - Set the ragegem project's Debugging Command to:
 *     - c:\ruby\bin\ruby.exe
 *   - Set the ragegem project's Debugging Command Arguments to:
 *     - X:\pipedev\jimmy\bin\misc\data_mk_radar_img.rb --project=jimmy --branch=dev --rebuild --map=island
 *       (or the script you want to debug)
 *   - Start debugging...
 *   
 * You can ignore the dialog that pops up saying that the ruby.exe binary
 * does not contain debug information/symbols.  The Rage Gem DLL does contain
 * debug information and thats all thats required.
 */

#endif // __CONFIGPARSERGEM_MAINPAGE_H__
