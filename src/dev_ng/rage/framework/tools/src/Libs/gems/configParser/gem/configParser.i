%module configParser
%{

#include "configParser/configParser.h"
#include "configParser/configCoreView.h"
#include "configParser/configGameView.h"

static configParser::ConfigCoreView gs_CoreView;
static configParser::ConfigGameView gs_GameView;

struct CoreView {
};

struct GameView {
};

%}

%include "CoreView.h"
struct CoreView {
	%extend {
		static CoreView& instance() {
			static CoreView cv;
			return ( cv );
		}
	}
};

%include "GameView.h"
struct GameView {
	%extend {
		static GameView& instance() {
			static GameView gv;
			return ( gv );
		}
	}
};


