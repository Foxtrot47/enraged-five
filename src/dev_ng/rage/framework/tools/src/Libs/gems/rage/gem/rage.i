%module rage
%{

#include "ragegem.h"
#include "scriptpack.h"
#include "scriptzip.h"
#include "scriptgeneral.h"

// RageCore headers
#include "atl/map.h"
#include "diag/channel.h"
#include "string/stringhash.h"

struct Pack {
};

struct Image {
};

struct Util {
};

struct Rage {
};

struct Zip {
};

void RageLogOutFn( const char* fmt )
{
	const size_t output_sz = 4096;
	char output[output_sz];
	char* curr = output;
	memset( &output, 0, sizeof(char) * output_sz );
	
	strcpy( output, "$stdout.write('" );
	curr += strlen( output );
	
	while ( *fmt )
	{
		if ( *fmt == '\'' )
		{
			*curr = '\\';
			curr++;
			*curr = *fmt;
		}
		else
			*curr = *fmt;
	
		fmt++;
		curr++;
	}
		
	*curr = '\0';	
	strcat( output, "')" );
	
	rb_eval_string(output);
}

void RageLogErrFn( const char* fmt )
{	
	const size_t output_sz = 4096;
	char output[output_sz];
	char* curr = output;
	memset( &output, 0, sizeof(char) * output_sz );
	
	strcpy( output, "$stderr.write('" );
	curr += strlen( output );
	
	while ( *fmt )
	{
		if ( *fmt == '\'' )
		{
			*curr = '\\';
			curr++;
			*curr = *fmt;
		}
		else
			*curr = *fmt;
	
		fmt++;
		curr++;
	}
		
	*curr = '\0';	
	strcat( output, "')" );
	
	rb_eval_string(output);
}

%}

struct Pack {

	%extend {
	
		static Pack& instance() { 
		
			static Pack b; return b; 
		}	
	}
};

%include "include/scriptpack.h"

struct Util {

	%extend {
	
		static Util& instance() { 
		
			static Util b; return b; 
		}	
	}	
};

%include "include/scriptgeneral.h"

struct Rage {

	%extend {
	
		static Rage& instance() {
			static Rage r; return r;
		}
	
		unsigned int atStringHash( const char* string )
		{
			return ( rage::atStringHash( string ) );
		}
		
		unsigned int atPartialStringHash( const char* string )
		{
			return ( rage::atPartialStringHash( string ) );
		}
		
		unsigned int atLiteralStringHash( const char* string )
		{
			return ( rage::atLiteralStringHash( string ) );
		}
		
		short atHash16( const char* string )
		{
			return ( rage::atHash16( string ) );
		}
		
		unsigned short atHash16U( const char* string )
		{
			return ( rage::atHash16U( string ) );
		}
	}
};

struct Zip {

	%extend {
	
		static Zip& instance() { 
		
			static Zip b; return b; 
		}	
	}
};

%include "include/scriptzip.h"
