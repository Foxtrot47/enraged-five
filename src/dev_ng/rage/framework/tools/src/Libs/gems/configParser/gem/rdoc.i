%define RDOCSIMPLE(name)
%feature("docstring",
"/*
	Document-method: name
*/
");
%enddef

%define RDOCCOMMENT(name,comment)
%feature("docstring",
"/*
	Document-method: name
	
" ## comment ## "
*/
");
%enddef

%define RDOCCOMPLEX(name,callseq,comment)
%feature("docstring",
"/*
	Document-method: name
	
	call-seq:
	" ## callseq ## "
	
" ## comment ## "
*/
");
%enddef