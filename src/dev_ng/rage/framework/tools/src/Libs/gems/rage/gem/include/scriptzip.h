%include "rdoc.i"

%extend Zip 
{

RDOCSIMPLE(start)
bool start( ) {

	return RBStartZip( );
}

RDOCSIMPLE(start_uncompressed)
bool start_uncompressed( ) {

	return RBStartUncompressedZip( );
}

RDOCSIMPLE(add)
bool add(const char* source, const char* destination) {

	return RBAddToZip( source, destination );
}

RDOCSIMPLE(save)
bool save(const char* packname) {

	return RBSaveZip(packname);
}

RDOCSIMPLE(close)
bool close(void) {

	return RBCloseZip();
}

RDOCSIMPLE(mount)
bool mount(const char* packname, const char* mountpoint) {

	return RBMountZip(packname,mountpoint);
}

RDOCSIMPLE(unmount)
bool unmount(const char* packname) {

	return RBUnmountZip(packname);
}
}
