%include "rdoc.i"

%extend CoreView {

	std::string 
	tools_root_dir( )
	{
		char rootDir[MAX_PATH] = {0}; 
		gs_CoreView.GetToolsRootDir( rootDir, MAX_PATH );
		return ( rootDir );
	}

	std::string
	tools_bin_dir( )
	{
		char binDir[MAX_PATH] = {0};
		gs_CoreView.GetToolsBinDir( binDir, MAX_PATH );
		return ( binDir );
	}

	std::string
	tools_lib_dir( )
	{
		char libDir[MAX_PATH] = {0};
		gs_CoreView.GetToolsLibDir( libDir, MAX_PATH );
		return ( libDir );
	}

	std::string
	tools_config_dir( )
	{
		char configDir[MAX_PATH] = {0};
		gs_CoreView.GetToolsConfigDir( configDir, MAX_PATH );
		return ( configDir );
	}
	
	std::string
	tools_ruby( )
	{
		char rubyPath[MAX_PATH] = {0};
		gs_CoreView.GetToolsRuby( rubyPath, MAX_PATH );
		return ( rubyPath );
	}

	std::string
	network_config_filename( )
	{
		char path[MAX_PATH] = {0};
		gs_CoreView.GetNetworkConfigFilename( path, MAX_PATH );
		return ( path );
	}

	std::string
	global_config_filename( )
	{
		char path[MAX_PATH] = {0};
		gs_CoreView.GetGlobalConfigFilename( path, MAX_PATH );
		return ( path );
	}

	std::string
	global_local_config_filename( )
	{
		char path[MAX_PATH] = {0};
		gs_CoreView.GetGlobalLocalConfigFilename( path, MAX_PATH );
		return ( path );
	}

	std::string
	project_config_filename( )
	{
		char path[MAX_PATH] = {0};
		gs_CoreView.GetProjectConfigFilename( path, MAX_PATH );
		return ( path );
	}

	std::string
	project_local_config_filename( )
	{
		char path[MAX_PATH] = {0};
		gs_CoreView.GetProjectLocalConfigFilename( path, MAX_PATH );
		return ( path );
	}

};
