%include "rdoc.i"
%include "std_string.i"
%include "std_vector.i"

%extend GameView {

	std::string 
	tools_root_dir( )
	{
		std::string rootDir;
		gs_GameView.GetToolsRootDir( rootDir );
		return ( rootDir );
	}

	std::string
	tools_bin_dir( )
	{
		std::string binDir;
		gs_GameView.GetToolsBinDir( binDir );
		return ( binDir );
	}

	std::string
	tools_lib_dir( )
	{
		std::string libDir;
		gs_GameView.GetToolsLibDir( libDir );
		return ( libDir );
	}

	std::string
	tools_config_dir( )
	{
		std::string configDir;
		gs_GameView.GetToolsConfigDir( configDir );
		return ( configDir );
	}

	std::string
	tools_ruby( )
	{
		std::string rubyPath;
		gs_GameView.GetToolsRuby( rubyPath );
		return ( rubyPath );
	}

	std::string
	project_name( )
	{
		std::string name;
		gs_GameView.GetProjectName( name );
		return ( name );
	}

	std::string 
	project_ui_name( )
	{
		std::string name;
		gs_GameView.GetProjectUIName( name );
		return ( name );
	}

	std::string
	project_root_dir( )
	{
		std::string root;
		gs_GameView.GetProjectRootDir( root );
		return ( root );
	}

};
