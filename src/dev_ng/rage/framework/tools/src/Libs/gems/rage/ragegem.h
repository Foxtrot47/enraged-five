/**
 * @file ragegem.h
 * @desc RageGem is a very slim Ruby Gem library to access CDIMAGE and RPF
 *       read/write functionality.
 */
#ifndef __RUBYGEM_H__
#define __RUBYGEM_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif

#define __MASTER 0

/**
 * RageCore and DLL compatibility requires this to disable all thread-local
 * storage as that plays havok with delay-loaded DLLs which we are.
 *
 * This is the sole reason we have a custom RageCore.vcproj file.  This is
 * a pain to maintain.
 */
#define SYSTEM_TLS_H
#undef __THREAD
#define __THREAD  

#if defined(RUBYRAGE_DLL_EXPORTS)
#define RAGEBUILDER_DLL __declspec( dllexport )
#elif defined(RUBYRAGE_DLL_IMPORTS) 
#define RAGEBUILDER_DLL __declspec( dllimport )
#else
#define RAGEBUILDER_DLL
#endif
 
typedef void (LogOutFn) (const char *msg);
RAGEBUILDER_DLL void RBSetStdOut(LogOutFn fn);
RAGEBUILDER_DLL void RBSetStdErr(LogOutFn fn);

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL void RageGemInit();
RAGEBUILDER_DLL void RageGemShutdown();

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus

// interface
class RageBuilder
{
public:
	enum FILETYPES
	{
		fiAnimDictionary,			
		fiBoundsDictionary,		
		fiDrawableDictionary,
		fiPhysicsDictionary,
		fiTextureDictionary,
		fiBlendShapeDictionary,
		fiDrawable,		
		fiFragment,		
		fiBounds,
		fiSpeedTree,
		fiHtml,
		fiNavMesh,
		fiBlendShape,
		fiDefinitions,
		fiPlacements,
		fiEffect,
		fiUnknown
	};

	static bool DoesExtensionMatchPlatform(const char* extension);
};
#endif

#endif // __RUBYGEM_H__

/* rubyrage.h */
