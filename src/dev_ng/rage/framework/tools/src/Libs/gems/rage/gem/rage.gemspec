require 'rubygems'
require 'rake/gempackagetask'

ragegemspec = Gem::Specification.new do |s|
   s.name = 'rage'
   s.platform = Gem::Platform::CURRENT
   s.version = '3.7.8'
   s.date = %q{Time.now.to_s}
   s.authors = ['David Muir']
   s.email = 'david.muir@rockstarnorth.com'
   s.summary = 'A wrapper for ragebuilder library'
   s.homepage = 'http://www.rockstargames.com/'
   s.description = 'Provides script access to ragebuilder'
   s.files = [ 'rage.so' ]
   s.require_path = '.'
end

Rake::GemPackageTask.new( ragegemspec ) do |pkg|
	pkg.need_tar = true
end
