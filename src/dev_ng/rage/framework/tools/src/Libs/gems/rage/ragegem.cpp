
#include "ragegem.h"
#include "log.h"
#include "fstream.h"
#include "ragefstream.h"
#include "diag/output.h"
#include "file/asset.h"
#include "system/param.h"
#include <cstdio>
#include <cstdlib>

extern __THREAD int RAGE_LOG_DISABLE;

void* __cdecl 
operator new(unsigned int size, unsigned int /*align*/)
{
	return (void*)(malloc( size ) );
}

void* __cdecl 
operator new(unsigned int size,size_t /*align*/,const char*,int)
{
	return (void*)(malloc( size ) );
}

void* __cdecl 
operator new(unsigned int size,const char*,int) 
{
	return (void*)(malloc( size ) );
}


void* __cdecl 
operator new[](unsigned int size, unsigned int /*align*/)
{
	return (void*)(malloc( size ) );
}

void* __cdecl 
operator new[](unsigned int size,size_t /*align*/,const char*,int)
{
	return (void*)(malloc( size ) );
}

void* __cdecl 
operator new[](unsigned int size,const char*,int) 
{
	return (void*)(malloc( size ) );
}

// Filestream factory - sort of...
WinLib::FileStream* RageCreateStream()
{
	return new RageFileStream;
}

void 
RageGemInit()
{
	static int __argc = 0;
	static char** __argv = NULL;

	RAGE_LOG_DISABLE = 1;
	if(__argc == 0)
	{
		__argc = 1;
		__argv = new char*[1];
		__argv[0] = new char[64];
		strcpy(__argv[0], "ragegem" );
	}
	rage::sysParam::Init(__argc,__argv);

	char* tempdir = getenv( "TEMP" );
	char buildlog[MAX_PATH];
	char errorlog[MAX_PATH];

	sprintf( buildlog, "%s\\buildlog.txt", tempdir );
	sprintf( errorlog, "%s\\errorlog.txt", tempdir );

	LogListener::Init( buildlog, errorlog );
	
	rage::ASSET.SetPath( "X:\\" );

	WinLib::FileStream::SetCreateStream(RageCreateStream);
}

void 
RageGemShutdown()
{
	LogListener::Close( );
}

bool RageBuilder::DoesExtensionMatchPlatform(const char* extension)
{
	return false;
#if 0
	for (int filetype = fiAnimDictionary; filetype < fiUnknown; ++filetype) // don't include fiUnknown
	{
		if( stricmp(extension, aExtensions[ms_ePlatform][filetype]) == 0 )
		{
			return true;
		}
	}

	RageBuilder::PLATFORM ePlatform = GetPlatformForExtension( extension );

	return (ePlatform == pltNeutral);
#endif
}
