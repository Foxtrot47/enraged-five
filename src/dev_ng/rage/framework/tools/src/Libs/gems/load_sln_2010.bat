@SETLOCAL
CALL setenv.bat

rem SET RUBYPATH=%RS_TOOLSBIN%\ruby
SET RUBYPATH=%RAGE_3RDPARTY%\ruby-1.8.7
SET SWIG=%RAGE_3RDPARTY%\swigwin-1.3.38\swig.exe
SET RUBYINCL=%RUBYPATH%\lib\ruby\1.8\i386-mswin32_100
SET RUBYLINKLIB=%RUBYPATH%\lib\msvcr100-ruby18.lib
SET PATH=%RUBYPATH%\bin;%PATH%

SET VISUAL_STUDIO="C:\Program Files\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe"
SET VISUAL_STUDIO_X86="C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\devenv.exe"
SET SOLUTION="%CD%\ruby_gems_2010.sln"

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
	ECHO x64 detected, starting Visual Studio 2005
	start "" %VISUAL_STUDIO_X86% %SOLUTION%
) ELSE (
	ECHO x86 detected, starting Visual Studio 2005
	start "" %VISUAL_STUDIO% %SOLUTION%
)

