CALL setenv.bat

SET RUBYPATH=%RS_TOOLSBIN%\ruby
SET RUBYINCL=%RUBYPATH%\lib\ruby\1.8\i386-mswin32
SET RUBYLINKLIB=%RUBYPATH%\lib\msvcrt-ruby18.lib
SET PATH=%RUBYPATH%\bin;%PATH%
SET SWIG=%RS_TOOLSROOT%\develop\swig\swig.exe

SET VISUAL_STUDIO="C:\Program Files\Microsoft Visual Studio 8\Common7\IDE\devenv.exe"
SET VISUAL_STUDIO_X86="C:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe"
SET SOLUTION="%CD%\ruby_gems.sln"

IF /I "%PROCESSOR_ARCHITECTURE%"=="AMD64" ( 
	ECHO x64 detected, starting Visual Studio 2005
	start "" %VISUAL_STUDIO_X86% %SOLUTION%
) ELSE (
	ECHO x86 detected, starting Visual Studio 2005
	start "" %VISUAL_STUDIO% %SOLUTION%
)

