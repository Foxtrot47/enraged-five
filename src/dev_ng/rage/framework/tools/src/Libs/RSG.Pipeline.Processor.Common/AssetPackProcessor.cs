﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using IB = RSG.Interop.Incredibuild;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using Content = RSG.Pipeline.Content;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetPack;
using Ionic.Zip;

namespace RSG.Pipeline.Processor.Common
{
    /*! \mainpage
     * 
     * \section intro_sec Purpose
     * 
     * The RSG.Pipeline.Processor.Characters assembly provides the character
     * conversion and processor features.
     * 
     * \section unit_test Unit Tests
     * 
     * There are currently no unit tests defined in this assembly.
     * 
     */

    /// <summary>
    /// Asset packaging processor; this is the processor that handles the 
    /// packaging of core assets from our exporters.
    /// </summary>
    /// The input of this processor is an XML file; and the referenced asset
    /// directories and files.
    /// 
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Default)]
    public class AssetPackProcessor :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Asset Packaging Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Asset Pack";

        /// <summary>
        /// Zip create temporary file location
        /// </summary>
        private static readonly String PATH_ZIP_CREATE_TMP = "$(toolstemp)\\zip_create";

        /// <summary>
        /// Parameter name for duplicate behaviour.
        /// </summary>
        private static readonly String DUPLICATE_BEHAVIOUR = "Duplicate Behaviour";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Temporary files to be created for XGE; and cleaned up.
        /// </summary>
        private ICollection<String> TempXGEFiles;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetPackProcessor()
            : base(DESCRIPTION)
        {
            this.TempXGEFiles = new List<String>();
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            Debug.Assert(process.Inputs.Count() > 0, "At least one Input must be defined for the AssetPackProcessor!");
            Debug.Assert(process.Outputs.Count() == 0, "Outputs cannot be defined for the AssetPackProcessor!");

            bool result = true;
            ICollection<IProcess> processes = new List<IProcess>();
            this.TempXGEFiles.Clear();
            
            foreach (IContentNode input in process.Inputs)
            {
                Debug.Assert(input is Content.File);
                Content.File inputFile = (Content.File)input;
                String inputFilename = param.Branch.Environment.Subst(inputFile.AbsolutePath);

                Debug.Assert(SIO.File.Exists(inputFilename),
                    String.Format("Asset Pack Processor XML input doesn't exist: {0}", inputFilename));
                if (!SIO.File.Exists(inputFilename))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Asset Pack Processor XML input doesn't exist: {0}.",
                        inputFilename);
                    result = false;
                    continue;
                }

                AssetPackScheduleFile packFileData = new AssetPackScheduleFile(owner, inputFilename);
                foreach (KeyValuePair<IContentNode, Tuple<IEnumerable<IContentNode>, DuplicateBehaviour>>
                    asset in packFileData.Assets)
                {
                    Debug.Assert(asset.Key is Content.Asset);

                    ProcessBuilder pb = new ProcessBuilder(this, owner);
                    pb.Inputs.AddRange(asset.Value.Item1);
                    pb.Outputs.Add(asset.Key);
                    pb.Parameters.Add(DUPLICATE_BEHAVIOUR, asset.Value.Item2);

                    if (process.Parameters.ContainsKey(Constants.Force_Create_If_Empty))
                        pb.Parameters.Add(Constants.Force_Create_If_Empty, process.Parameters[Constants.Force_Create_If_Empty]);

                    IProcess packProcess = pb.ToProcess();
                    packProcess.State = ProcessState.Prebuilt;
                    processes.Add(packProcess);
                }
            }
            process.State = ProcessState.Discard;
            resultantProcesses = processes;

            syncDependencies = new List<IContentNode>();
            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            bool allow_remote = GetXGEAllowRemote(param, true);

            XGE.ITool tool = XGEFactory.GetZipTool(param.Log, 
                param.Branch.Project.Config, "Create Zip", allow_remote);
            ICollection<XGE.ITask> xgeTasks = new List<XGE.ITask>();
            int taskCounter = 0;
            String tmp = param.Branch.Environment.Subst(PATH_ZIP_CREATE_TMP);
            if (!SIO.Directory.Exists(tmp))
                SIO.Directory.CreateDirectory(tmp);
            foreach (IProcess process in processes)
            {
                // Write filelist text file;
                try
                {
                    Content.File outputFile = (Content.File)process.Outputs.First();
                    String fileListFilename = param.Branch.Environment.Subst(String.Format("{0}\\{1}.txt", tmp, Guid.NewGuid())); //process.GetHashCode()));
                    using (SIO.FileStream fs = new SIO.FileStream(fileListFilename,
                        SIO.FileMode.Create, SIO.FileAccess.Write))
                    {
                        using (SIO.StreamWriter sw = new SIO.StreamWriter(fs))
                        {
                            foreach (IContentNode input in process.Inputs)
                            {
                                if (input is Content.File)
                                {
                                    Content.File inputFile = (Content.File)input;
                                    String inputFilename = inputFile.AbsolutePath;
                                    String destination;

                                    if (input.Parameters.ContainsKey(Constants.Asset_ZipPath))
                                        destination = (String)input.Parameters[Constants.Asset_ZipPath];
                                    else
                                        destination = System.IO.Path.GetFileName(inputFilename);

                                    sw.WriteLine("{0},{1}", inputFilename, destination);
                                }
                                else if (input is Content.Directory)
                                {
                                    Content.Directory inputDirectory = (Content.Directory)input;
                                    String destination = String.Empty;
                                    String recursive = String.Empty;

                                    if (input.Parameters.ContainsKey(Constants.Asset_ZipPath))
                                        destination = (String)input.Parameters[Constants.Asset_ZipPath];

                                    if((input as Content.Directory).Recursive == true)
                                        sw.WriteLine("{0},{1},{2}", inputDirectory.AbsolutePath, inputDirectory.Wildcard, "recursive");
                                    else if (destination != String.Empty)
                                        sw.WriteLine("{0},{1},{2}", inputDirectory.AbsolutePath, inputDirectory.Wildcard, destination);
                                    else
                                        sw.WriteLine("{0},{1}", inputDirectory.AbsolutePath, inputDirectory.Wildcard);
                                }
                            }
                        }
                    }
                    this.TempXGEFiles.Add(fileListFilename);

                    // Create XGE task.
                    String outputFilename = System.IO.Path.GetFileName(outputFile.AbsolutePath);
                    String taskName = String.Format("Create Zip {0} [{1}]", taskCounter++, outputFilename);
                    String dupeBehaviour = AssetPackScheduleFile.ResolveDupeBehaviourEnum((DuplicateBehaviour)process.Parameters[DUPLICATE_BEHAVIOUR]);

                    XGE.ITaskJob task = new XGE.TaskJob(taskName);
                    xgeTasks.Add(task);

                    task.Caption = taskName;
                    task.Tool = tool;
                    task.Parameters = String.Format("-output {0} -filelist {1} -dupebehaviour {2}",
                        outputFile.AbsolutePath, fileListFilename, dupeBehaviour);                           
                    task.SourceFile = fileListFilename;
                    task.InputFiles.Add(fileListFilename);
                    task.OutputFiles.Add(outputFile.AbsolutePath);

                    // Define the XGE task for the IProcess.
                    process.Parameters.Add(Constants.ProcessXGE_TaskName, taskName.Replace(' ', '_'));
                    process.Parameters.Add(Constants.ProcessXGE_Task, task);
                }
                catch (Exception ex)
                {
                    param.Log.ExceptionCtx(LOG_CTX, ex, "Error creating zip filelist data.");
                    result = false;
                }
            }

            ICollection<XGE.ITool> xgeTools = new List<XGE.ITool>();
            xgeTools.Add(tool);
            tools = xgeTools;
            tasks = xgeTasks;

            return (result);
        }

        /// <summary>
        /// Callback for when the build process completes.
        /// </summary>
        /// <param name="param"></param>
        public override void Finished(IEngineParameters param)
        {
#if DEBUG
            if (!param.Log.HasErrors)
            {
#endif // DEBUG
                foreach (String tempFilename in this.TempXGEFiles)
                {
                    try
                    {
                        SIO.File.Delete(tempFilename);
                    }
                    catch (Exception ex)
                    {
                        param.Log.ExceptionCtx(LOG_CTX, ex, "Error deleting temporary ZIP data file: {0}.",
                            tempFilename);
                    }
                }
#if DEBUG
                // Delete any leftovers from previous failed exports
                String tempDirectory = param.Branch.Environment.Subst(PATH_ZIP_CREATE_TMP);
                if (SIO.Directory.Exists(tempDirectory))
                {
                    String[] leftovers = SIO.Directory.GetFiles(tempDirectory);
                    foreach (String file in leftovers)
                    {
                        try
                        {
                            SIO.File.Delete(file);
                        }
                        catch (Exception ex)
                        {
                            param.Log.ExceptionCtx(LOG_CTX, ex, "Error deleting temporary ZIP data file: {0}.",
                                file);
                        }
                    }
                }
            }
#endif // DEBUG
        }
        #endregion // IProcessor Interface Methods
    }

} // RSG.Pipeline.Processor.Common namespace
