﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetPreProcessor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Processor.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Linq;
    using RSG.Base.Extensions;
    using RSG.Base.Logging;
    using RSG.Pipeline.Content;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Core.Attributes;
    using RSG.Pipeline.Services;
    using XGE = RSG.Interop.Incredibuild.XGE;

    /// <summary>
    /// Asset PreProcessor; this is the new default pipeline processor for all assets.
    /// </summary>
    /// This gives us a level of abstraction above the Rage Platform processors and
    /// allows us to handle non-Rage-independent assets (which we are starting to see
    /// with the likes of the Material Preset system).
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Default)]
    class AssetPreProcessor : 
        ProcessorBase2,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description.
        /// </summary>
        private const String DESCRIPTION = "Asset PreProcessor (DEFAULT)";

        /// <summary>
        /// Wrapping the old default processor.
        /// </summary>
        private const String DEFAULT_WRAPPER_PROCESSOR = "RSG.Pipeline.Processor.Platform.Rage";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetPreProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Processor Prebuild stage.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            using (new ProfileContext(param.Log, "Asset PreProcessor Prebuild."))
            {
                return (PrebuildImpl(param, processes, processors, owner, out syncDependencies, out resultantProcesses));
            }
        }

        /// <summary>
        /// Prepare; this turns the IProcess objects into XGE tools and tasks.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Should never be hit as this is a PreProcessor.
            throw (new NotImplementedException());
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Validate all processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <returns></returns>
        private bool ValidateProcesses(IEngineParameters param, IEnumerable<IProcess> processes)
        {
            using (new ProfileContext(param.Log, "Asset PreProcessor Validation"))
            {
                bool result = true;
                foreach (IProcess process in processes)
                {
                    if (!process.Inputs.Any())
                    {
                        param.Log.Error("Asset PreProcessor process requires one or more input nodes.");
                        result = false;
                    }

                    if (process.Outputs.Count() != 1)
                    {
                        param.Log.Error("Asset PreProcessor requires one output node.");
                        return false;
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        private bool PrebuildImpl(IEngineParameters param, IEnumerable<IProcess> processes,
            IProcessorCollection processors, IContentTree owner,
            out IDictionary<IProcess, IEnumerable<IContentNode>> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Set trivial parameters to allow for early return.
            resultantProcesses = new IProcess[0];
            syncDependencies = new Dictionary<IProcess, IEnumerable<IContentNode>>();

            // Validate.
            this.LoadParameters(param);
            if (!ValidateProcesses(param, processes))
                return (false);

            // Currently straight-through to the old default Rage processor.  This needs 
            // modified to support Material Presets on the assets that support it.
            ICollection<ProcessBuilder> outputProcesses = new List<ProcessBuilder>();
            foreach (IProcess process in processes)
            {
                ProcessBuilder pbDefault = new ProcessBuilder(DEFAULT_WRAPPER_PROCESSOR, processors, owner);
                pbDefault.Inputs.AddRange(process.Inputs);
                pbDefault.Outputs.AddRange(process.Outputs);
                pbDefault.AdditionalDependentContent.AddRange(process.AdditionalDependentContent);

                outputProcesses.Add(pbDefault);

                // Discard our input process.
                process.State = ProcessState.Discard;
            }
            resultantProcesses = outputProcesses.Select(p => p.ToProcess());

            return (true);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Common namespace
