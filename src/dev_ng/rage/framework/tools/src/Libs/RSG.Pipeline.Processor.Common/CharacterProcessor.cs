﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using IB = RSG.Interop.Incredibuild;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using Content = RSG.Pipeline.Content;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Processor;
using RSG.Pipeline.Services;
using RSG.Model.MaterialPresets;
using Ionic.Zip;

namespace RSG.Pipeline.Processor.Common
{
    /*! \mainpage
     * 
     * \section intro_sec Purpose
     * 
     * The RSG.Pipeline.Processor.Characters assembly provides the character
     * conversion and processor features.
     * 
     * \section unit_test Unit Tests
     * 
     * There are currently no unit tests defined in this assembly.
     * 
     */

    /// <summary>
    /// Character processor; this is the processor that handles the character
    /// assets from 3dsmax.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class CharacterProcessor : 
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Character Asset Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Character";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CharacterProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)
        
        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            ICollection<IProcess> processes = new List<IProcess>();
            // Validate inputs.
            Debug.Assert(1 == process.Inputs.Count(),
                String.Format("Invalid input; inputs count {0}.", process.Inputs.Count()));
            if (1 != process.Inputs.Count())
            {
                param.Log.ErrorCtx(LOG_CTX, "Invalid input; inputs count {0}.", process.Inputs.Count());
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return (false);
            }
            // Validate outputs.
            Debug.Assert(1 == process.Outputs.Count(),
                String.Format("Invalid output; output count {0}.", process.Outputs.Count()));
            if (1 != process.Outputs.Count())
            {
                param.Log.ErrorCtx(LOG_CTX, "Invalid output; outputs count {0}.", process.Outputs.Count());
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return (false);
            }
            Debug.Assert(process.Outputs.First() is Content.Asset,
                String.Format("Invalid output; output wrong type {0}.", process.Outputs.First().GetType()));
            if (!(process.Outputs.First() is Content.Asset))
            {
                param.Log.ErrorCtx(LOG_CTX, "Invalid output; output wrong type {0}.", process.Outputs.First().GetType());
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return (false);
            }

            bool result = true;
            bool isDLC = param.Branch.Project.IsDLC;
            String metadataDirectoryPath = SIO.Path.Combine(param.Branch.Metadata, "characters");
            String ragdollDirectoryPath = SIO.Path.Combine(param.CoreBranch.Common, "data", "ragdoll");
            IContentNode ragdollDirectory = owner.CreateDirectory(ragdollDirectoryPath);
            if (!ragdollDirectory.Parameters.ContainsKey(Constants.RageProcess_DoNotConvert))
                ragdollDirectory.Parameters.Add(Constants.RageProcess_DoNotConvert, true);

            // Add character metadata and ragdoll directories to our sync dependencies.
            ICollection<IContentNode> syncFiles = new List<IContentNode>();

            // Recursive prebuild, sync dependancies first
            if (process.State == ProcessState.Initialised)
            {
                syncFiles.Add(owner.CreateDirectory(metadataDirectoryPath));
                syncFiles.Add(ragdollDirectory);
                syncDependencies = syncFiles;
                resultantProcesses = processes;
                process.State = ProcessState.Prebuilding;
                return (result);
            }

            Content.Directory characterDirectory;

            if (process.Inputs.First() is Content.Directory)
            {
                characterDirectory = (process.Inputs.First() as Content.Directory);
            }
            else if (process.Inputs.First() is Content.File)
            {
                Content.File pedFile = (process.Inputs.First() as Content.File);
                characterDirectory = owner.CreateDirectory(pedFile.Directory, ("*" + System.IO.Path.GetFileName(process.Inputs.First().Name) + "*")) as Content.Directory;
            }
            else
            {
                param.Log.ErrorCtx(LOG_CTX, "Invalid input type {0}.", process.Inputs.First().GetType());
                syncDependencies = new List<IContentNode>();
                resultantProcesses = new List<IProcess>();
                return (false);
            }

            // Handle multiple component peds with multiple wildcard directories.
            // We need to extract them into a subdir otherwise the StaticDirectory can't handle the inputs.
            string characterDirAbsPath = characterDirectory.AbsolutePath;
            if (characterDirectory.Wildcard != "*.*")
            {
                string processedSubDirectory = characterDirectory.Wildcard.Split(new char[] { '*' })[0];
                characterDirAbsPath = System.IO.Path.Combine(characterDirectory.AbsolutePath, processedSubDirectory);
            }

            //Rather than always rely on content tree to have different processed folders (for outfit exports), append the output RPF
            //as a subfolder in the processed folder, since later it just scoops up everything from within the folder (which caused url:bugstar:2105278)
            String processedDirectory = RemapFilenameToProcessedCache(param.Branch, characterDirAbsPath);
            processedDirectory = SIO.Path.Combine(processedDirectory, SIO.Path.GetFileNameWithoutExtension(process.Outputs.First().Name));

            Content.StaticDirectory processedCharacterDirectory = owner.CreateStaticDirectory(processedDirectory) as Content.StaticDirectory;
            IEnumerable<IContentNode> characterInputs = characterDirectory.EvaluateInputs();			
            ICollection<ProcessBuilder> materialPresetProcessBuilders = new List<ProcessBuilder>();

            // Only add processors for content that exists, if a content node is specified by no assets exist with the prefix then we get an error.
            bool contentExists = false;

            foreach (IContentNode character in characterInputs)
            {
                Content.File characterPedFile = (character as Content.File);
                if (characterPedFile.AbsolutePath.EndsWith(".ped.zip") || characterPedFile.AbsolutePath.EndsWith(".prop.zip"))
                {
                    IEnumerable<String> files;
                    bool extractOk = true;
                    if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                        extractOk = Zip.ExtractAll(characterPedFile.AbsolutePath, processedDirectory, true, null, out files);
                    else
                        extractOk = Zip.ExtractNewer(characterPedFile.AbsolutePath, processedDirectory, null, out files);

                    if (!extractOk)
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Extracting '{0}' to '{1}' failed.", 
                            characterPedFile, characterDirectory.AbsolutePath);
                        continue;
                    }
                    foreach (String file in files)
                    {
                        // GTA5 Bug #1356151; I've seen the files list include directories with some
                        // zip files and other times its not.  This prevents us creating invalid
                        // content-nodes for directories that have been "extracted".  File check
                        // is safe because its just been extracted.
                        if (!SIO.File.Exists(file))
                        {
                            param.Log.WarningCtx(LOG_CTX, "Directory in zip extracted file list ({0}, {1}).  Ignoring.",
                                characterPedFile.AbsolutePath, file);
                            continue; // Skip.
                        }

                        IContentNode processedPedFile = owner.CreateFile(file);
                        
						// Character drawable dictionaries can have presets buried in them so they need to be converted.
                        if (file.Contains(".idd.zip"))
                            ExtractAndProcessPedMaterials(process, param, file, processedDirectory, processors, owner, ref materialPresetProcessBuilders);

                        // Strip out the process directory so we are just left with the structure of what was extracted.
                        // This is required for streamped preview exports to get the proper directory structure in the preview folder.
                        String zipFolderStructure = file.Replace(processedDirectory, "");
                        String zipFolder = SIO.Path.GetDirectoryName(zipFolderStructure);
                        zipFolder = zipFolder.Trim(new char[]{'\\', '/'});

                        if (!String.IsNullOrEmpty(zipFolder))
                        {
                            processedPedFile.Parameters[Constants.Preserve_Folder_Structure] = zipFolder;
                            processedPedFile.Parameters[Constants.No_BVH_Generation] = true;
                        }
                            

                        processedCharacterDirectory.StaticFiles.Add(processedPedFile);

                        contentExists = true;
                    }

                    // Add character metadata for .ped.zip or outfit.zip (might be full ped, not just outfits)
                    if (!characterPedFile.AbsolutePath.EndsWith(".prop.zip") && isDLC)
                    {
                        // Find process that generates .ped.zip.
                        IProcess exportProcess = process.Owner.FindProcessesWithOutput(characterPedFile).FirstOrDefault();
                        if (exportProcess != null)
                        {
                            foreach (IContentNode exportFile in exportProcess.Inputs)
                            {
                                String characterName = characterPedFile.Basename;
                                String characterMetadata = String.Empty;
                                String outfitName = String.Empty;
                                if (exportFile.Parameters.ContainsKey(Constants.Character_DLCOutfit))
                                {
                                    outfitName = (String)exportFile.Parameters[Constants.Character_DLCOutfit];
                                    characterMetadata = SIO.Path.Combine(metadataDirectoryPath, String.Format("{0}_{1}", characterName, outfitName));
                                }
                                else
                                    characterMetadata = SIO.Path.Combine(metadataDirectoryPath, characterName);

                                characterMetadata = SIO.Path.ChangeExtension(characterMetadata, "pso.meta");
                                param.Log.MessageCtx(LOG_CTX, "Character metadata file: {0} (IsDLC: {1}, Outfit: {2}).",
                                    characterMetadata, isDLC, outfitName);
                                if (SIO.File.Exists(characterMetadata))
                                    processedCharacterDirectory.StaticFiles.Add(owner.CreateFile(characterMetadata));
                                else
                                {
                                    // Don't set result to false as we want to try and build what we can, as there could be many
                                    // ped outfits in this ped.zip.
                                    param.Log.Error("Character metadata file: {0} does not exist.", characterMetadata);
                                }
                            }
                        }
                        else
                        {
                            param.Log.Error("No processes found for output: {0}.", characterPedFile);
                            result = false;
                        }
                    }
                    else if (!characterPedFile.AbsolutePath.EndsWith(".prop.zip"))
                    {
                        // Non-DLC path is trivial; just same as character name.
                        String characterMetadata = SIO.Path.Combine(metadataDirectoryPath, characterPedFile.Basename);
                        characterMetadata = SIO.Path.ChangeExtension(characterMetadata, "pso.meta");
                        if (SIO.File.Exists(characterMetadata))
                            processedCharacterDirectory.StaticFiles.Add(owner.CreateFile(characterMetadata));
                        else
                        {
                            // Set result to false as this is a main ped.zip, as opposed to DLC ped outfits above.
                            param.Log.Error("Character metadata file: {0} does not exist.", characterMetadata);
                            result = false;
                        }               
                    }
                }
                else
                {
                    processedCharacterDirectory.StaticFiles.Add(characterPedFile);
                    contentExists = true;
                }
            }
            
            if (contentExists)
            {
                ProcessBuilder pb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.Rage",
                    processors, owner);
                pb.Inputs.Add(ragdollDirectory); // Ideally we would know which ragdoll for each .ped.zip.
                pb.Inputs.Add(processedCharacterDirectory);
                pb.Outputs.Add(process.Outputs.First());
                foreach (IContentNode input in pb.Inputs)
                {
                    input.SetParameter(Constants.No_BVH_Generation, true);
                }

                // Material Preset processes.
                if (materialPresetProcessBuilders.Count > 0)
                    materialPresetProcessBuilders.ForEach(materialProcess => processes.Add(materialProcess.ToProcess()));
               
                IProcess convertProcess = pb.ToProcess();
                processes.Add(convertProcess);
            }

            resultantProcesses = processes;
            process.State = ProcessState.Discard;

            syncDependencies = syncFiles;
            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Should never hit here.
            throw new NotImplementedException();
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Convert a filename string to a target filename string; remapping it
        /// into the branch target's resource cache.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static String RemapFilenameToProcessedCache(IBranch branch, String filename)
        {
            String normalisedFilename = SIO.Path.GetFullPath(branch.Environment.Subst(filename));
            if (normalisedFilename.StartsWith(branch.Export.ToLower()))
                normalisedFilename = normalisedFilename.Replace(branch.Export.ToLower(), branch.Processed);

            return (normalisedFilename);
        }        
        
        /// <summary>
        /// Extracts a characters .idd file if it contains any material preset files.
        /// Creates processes for the MaterialPresetProcessor to process those presets before converting the assets.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="filename"></param>
        /// <param name="processedDirectory"></param>
        /// <param name="processors"></param>
        /// <param name="targetContentTree"></param>
        /// <param name="materialPresetProcesses"></param>
        /// <returns></returns>
        private void ExtractAndProcessPedMaterials(IProcess process, IEngineParameters param, string filename, string processedDirectory,
                                                    IProcessorCollection processors, IContentTree targetContentTree, ref ICollection<ProcessBuilder> materialPresetProcesses)
        {
            bool extractOk = false;
            IEnumerable<String> files;

            string pedName = SIO.Path.GetFileNameWithoutExtension(RSG.Platform.Filename.GetBasename(filename));
            string pedProcessedDirectory = SIO.Path.Combine(processedDirectory, "transformed", pedName);

            Zip.GetFileList(filename, out files);

            int presetCount = files.Where(zipFile => SIO.Path.GetExtension(zipFile).ToLower() == ".mpo" || SIO.Path.GetExtension(zipFile).ToLower() == ".mpi").Count();

            // Only process peds that have presets buried in them.
            if (presetCount > 0)
            {
                if (!SIO.Directory.Exists(pedProcessedDirectory))
                    SIO.Directory.CreateDirectory(pedProcessedDirectory);

                // Only extract the zip file if we actually found template files otherwise save ourselves the trouble.
                if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                    extractOk = Zip.ExtractAll(filename, pedProcessedDirectory, true, null, out files);
                else
                    extractOk = Zip.ExtractNewer(filename, pedProcessedDirectory, null, out files);

                ProcessBuilder materialPresetProcessBuilder = new ProcessBuilder("RSG.Pipeline.Processor.Material.MaterialPresetProcessor", processors, targetContentTree);

                // We need to tell the MaterialPresetProcessor that the directory we are passing has subdirectories that need to be scanned as well.
                materialPresetProcessBuilder.Parameters.Add("ContainsSubDirectories", true as object);

                materialPresetProcessBuilder.Inputs.Add(targetContentTree.CreateDirectory(pedProcessedDirectory));

                materialPresetProcessBuilder.Outputs.Add(targetContentTree.CreateFile(filename));
                materialPresetProcesses.Add(materialPresetProcessBuilder);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Characters namespace
