﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.Pipeline.Content;
using RSG.Platform;

namespace RSG.Pipeline.Processor.Common
{

    /// <summary>
    /// Script Resource Processor (optimised resource compiler; no shaders loaded).
    /// </summary>
    /// This processor supports the Engine Preview flag.
    /// 
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    class ScriptResourceProcessor : 
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String _description = "Script Processor";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptResourceProcessor()
            : base(_description)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild the Script Resource process.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            // Verify our process is well-constructed.
            if (!VerifyProcess(param, process))
            {
                syncDependencies = new IContentNode[0];
                resultantProcesses = new IProcess[0];
                return (false);
            }

            // Expand inputs to a collection of files; this allows the processor to 
            // support files and directories and combinations (for preview etc).
            ISet<IContentNode> inputs = new HashSet<IContentNode>();
            foreach (IContentNode input in process.Inputs)
            {
                if (input is Content.File)
                {
                    inputs.Add(input);
                }
                else if (input is Content.Directory)
                {
                    Content.Directory directory = (Content.Directory)input;
                    inputs.AddRange(directory.EvaluateInputs().OfType<Content.File>().Where(f => f.Extension.Equals(".sco")));
                }
            }

            // Expand the process for all targets; and add an RPF create process for each.
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
            ICollection<IProcess> processes = new List<IProcess>();
            process.State = ProcessState.Discard;
            foreach (ITarget target in enabledTargets)
            {
                String resourceDirectory = SIO.Path.Combine("$(cache)", "script", "$(branch)", "resources", 
                    target.Platform.ToString());
                ICollection<IContentNode> targetResources = new List<IContentNode>();
                foreach (IContentNode input in inputs)
                {
                    Content.File inputFile = (Content.File)input;
                    String filename = SIO.Path.GetFileName(inputFile.AbsolutePath);
                    filename = PlatformPathConversion.ConvertFilenameToPlatform(target, filename, false);
                    String outputFilename = String.Empty; 
                    if (param.Flags.HasFlag(EngineFlags.Preview))
                        outputFilename = SIO.Path.Combine(param.Branch.Preview, filename);
                    else
                        outputFilename = SIO.Path.Combine(resourceDirectory, filename);
                    IContentNode outputFile = owner.CreateAsset(outputFilename, target.Platform);
                    targetResources.Add(outputFile);

                    ProcessBuilder pb = new ProcessBuilder(this, owner);
                    pb.Inputs.Add(inputFile);
                    pb.Outputs.Add(outputFile);
                    IProcess p = pb.ToProcess();
                    p.State = ProcessState.Prebuilt;
                    processes.Add(p);
                }

                // Create target RPF process (if not preview).
                if (!param.Flags.HasFlag(EngineFlags.Preview))
                {
                    ProcessBuilder rpfPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RpfCreateProcessor",
                        processors, owner);
                    rpfPb.Inputs.AddRange(targetResources);

                    Content.Asset outputNode = (Content.Asset)process.Outputs.First();
                    String outputFilename = SIO.Path.GetFullPath(target.Environment.Subst(outputNode.AbsolutePath));
                    
                    IContentNode targetOutputNode = owner.CreateAsset(outputFilename, target.Platform);
                    rpfPb.Outputs.Add(targetOutputNode);
                    IProcess rpfProcess = rpfPb.ToProcess();
                    processes.Add(rpfProcess);
                }
            }

            process.State = ProcessState.Discard;
            syncDependencies = new List<IContentNode>();
            resultantProcesses = processes;

            return (true);
        }

        /// <summary>
        /// Prepare the Script Resource processes.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            bool allow_remote = GetXGEAllowRemote(param, true);
            XGE.ITool resourceCompilerTool = XGEFactory.GetScriptTool(param.Branch,
                "Script Resource Compiler", XGEFactory.ScriptToolType.ScriptResourceCompiler, 
                allow_remote);
            XGE.ITool resourceCompilerToolX64 = XGEFactory.GetScriptTool(param.Branch,
                "Script Resource Compiler (x64)", XGEFactory.ScriptToolType.ScriptResourceCompilerX64,
                allow_remote);
            
            int jobCounter = 0;
            XGE.ITaskJobGroup taskGroup = new XGE.TaskGroup("Script Resourcing");
            foreach (IProcess process in processes)
            {
                String inputFilename = process.Inputs.OfType<Content.File>().First().AbsolutePath;
                Content.Asset outputNode = process.Outputs.OfType<Content.Asset>().First();
                String outputFilename = outputNode.AbsolutePath;

                String taskName = String.Format("Script Resource Compiler {0} [{1}]", jobCounter++, outputNode.Platform);
                String flatTaskName = taskName.Replace(' ', '_');
                XGE.TaskJob job = new XGE.TaskJob(taskName);
                switch (outputNode.Platform)
                {
                    case Platform.Platform.PS4:
                    case Platform.Platform.Win64:
                    case Platform.Platform.XboxOne:
                        job.Tool = resourceCompilerToolX64;
                        break;
                    default:
                        job.Tool = resourceCompilerTool;
                        break;
                }
                job.Caption = taskName;
                job.InputFiles.Add(inputFilename);
                job.OutputFiles.Add(outputFilename);
                job.SourceFile = inputFilename;
                job.Parameters = String.Format("{0} {1}", inputFilename, outputFilename);  
                taskGroup.Tasks.Add(job);

                process.Parameters.Add(Constants.ProcessXGE_Task, job);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, flatTaskName);
            }

            // Assign method output.
            tools = new XGE.ITool[] { resourceCompilerTool, resourceCompilerToolX64 };
            tasks = new XGE.ITask[] { taskGroup };

            return (result);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods
        /// <summary>
        /// Verify our IProcess object.
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        private bool VerifyProcess(IEngineParameters param, IProcess process)
        {
            bool result = true;
            Debug.Assert(process.Inputs.OfType<IFilesystemNode>().Any(),
                "ScriptResourceCompiler requires one or more input directories.");
            if (!process.Inputs.OfType<IFilesystemNode>().Any())
            {
                param.Log.Error("ScriptResourceCompiler requires one or more input files/directories.");
                result = false;
            }

            Debug.Assert(1 == process.Outputs.OfType<Content.Asset>().Count(),
                "ScriptResourceCompiler requires a single RPF output.");
            if (!(process.Outputs.First() is Content.Asset))
            {
                param.Log.Error("ScriptResourceCompiler requires a single RPF output.");
                result = false;
            }
            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Processor.Common namespace
