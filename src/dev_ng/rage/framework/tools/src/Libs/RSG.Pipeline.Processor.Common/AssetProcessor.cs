﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetProcessor;
using File = RSG.Pipeline.Content.File;

namespace RSG.Pipeline.Processor.Common
{

    /// <summary>
    /// AssetProcessor; this is the processor that generates all the config files for the actual AssetProcessor in the pipeline.
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public sealed class AssetProcessor : ProcessorBase
    {
        #region Constants
        private const string DESCRIPTION                = "AssetProcessor Processor";
        private const string LoggingContext             = "AssetProcessor config cooker";
        private const string AssetProcessorConfigPrefix = "asset_processor_config";
        private const string ConfigCacheDirectory       = "Config Cache Directory";
        private const long DefaultPacketSize            = 1024 * 1024 * 10; // 10 MB packet size
        private const int DefaultNumberInputs           = 20;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetProcessor() : 
            base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region ProcessorBase overrides
        public override bool Prebuild(IEngineParameters param,
            IProcess process,
            IProcessorCollection processors,
            IContentTree owner,
            out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            bool result = true;

            if (!process.Inputs.Any(i => i is File))
            {
                Debug.Fail("AssetProcessor requires at least one input.");
                param.Log.ErrorCtx(LoggingContext, "AssetProcessor requires at least one input");
                result = false;
            }

            if (process.Outputs.Count() > 1)
            {
                Debug.Fail("AssetProcessor does not support multiple outputs.");
                param.Log.ErrorCtx(LoggingContext, "AssetProcessor does not support multiple outputs.");
                result = false;
            }

            // check parameter because it has to hold the operations and the op's elements
            object apOperations;
            if (!process.Parameters.TryGetValue(Constants.AssetProcessor_Operations, out apOperations))
            {
                // error
                Debug.Fail("AssetProcessorOperations not found in process parameters. Have you forgotten to pass that as a parameter?");
                param.Log.ErrorCtx(LoggingContext, "AssetProcessorOperations parameter not found in process parameters. Have you forgotten to pass that as a parameter?");
                result = false;
            }
            else
            {
                // check that operation contains at least one operation
                var operations = apOperations as Dictionary<Operation, IEnumerable<IContentNode>>;
                if (operations == null)
                {
                    // error, wrong object
                    param.Log.ErrorCtx(LoggingContext, "AssetProcessorOperations parameter is not of the right type. 'Dictionary<Operation, IEnumerable<Uri>>' expected, {0} found.", apOperations.GetType());
                    result = false;
                }
                else
                {
                    // check there's one at least.
                    if (operations.Count == 0)
                    {
                        param.Log.ErrorCtx(LoggingContext, "AssetProcessorOperations parameter does not contain any actual operation. At least one is expected.");
                        result = false;
                    }
                    else
                    {
                        // we could even test that there's at least a copy or drawablemerge
                    }
                }
            }

            syncDependencies = new IContentNode[0];
            process.State = ProcessState.Prebuilt;
            resultantProcesses = new IProcess[] {process};

            return result;
        }

        public override bool Prepare(IEngineParameters param,
            IEnumerable<IProcess> processes,
            out IEnumerable<ITool> tools,
            out IEnumerable<ITask> tasks)
        {
            LoadParameters(param);

            bool allowRemote = GetXGEAllowRemote(param, defaultValue: true);
            ITool tool = XGEFactory.GetAssetProcessorTool(param.Log, param.Branch.Project.Config, "AssetProcessor", allowRemote);
            List<ITask> xgeTasks = new List<ITask>();

            long maxXgePacketSize = GetParameter("XGE Packet Size", DefaultPacketSize);
            int maxNumberOfInput = GetParameter("XGE File Limit", DefaultNumberInputs);

            string cacheDirectory = param.Branch.Environment.Subst(GetParameter(ConfigCacheDirectory, ""));
            if (string.IsNullOrEmpty(cacheDirectory))
            {
                param.Log.ToolErrorCtx(LoggingContext, "An error occurred while retrieving the Config Cache Directory. Aborting.");

                // it's not safe to continue at this point. ABORT! ABORT!
                tools = new ITool[0];
                tasks = new ITask[0];
                return false;
            }

            // create directory if non existent (avoids assuming the directory exists and thus, avoids throwing IO Exceptions)
            if (!Directory.Exists(cacheDirectory))
                Directory.CreateDirectory(cacheDirectory);

            // XGE related values
            int taskNumber = 1;
            List<AssetProcessorConfig> configs = new List<AssetProcessorConfig>();
            long accumulatedPacketSize = 0;
            int inputCount = 0;

            // init
            string taskName = string.Format("AssetProcessor {0}", taskNumber.ToString("D3"));
            TaskJob task = new TaskJob(taskName);

            foreach (IProcess process in processes)
            {
                if (accumulatedPacketSize > maxXgePacketSize  || inputCount > maxNumberOfInput)
                {
                    // write out said config file
                    string configFilename = string.Format("{0}_{1}.xml", AssetProcessorConfigPrefix, taskNumber.ToString("D3"));
                    string configFilepath = Path.Combine(cacheDirectory, configFilename);

                    AssetProcessorConfig.Save(configFilepath, configs);

                    // closing the current task
                    task.Caption = taskName;
                    task.Tool = tool;
                    task.Parameters = string.Format("-config {0}", configFilepath);

                    xgeTasks.Add(task);

                    // reinitializing for ongoing processes
                    configs = new List<AssetProcessorConfig>();
                    accumulatedPacketSize = 0;
                    inputCount = 0;
                    taskNumber++;

                    taskName = string.Format("AssetProcessor {0}", taskNumber.ToString("D3"));
                    task = new TaskJob(taskName);
                }

                // we need to translate the operations to a suitable form first
                Dictionary<Operation, IEnumerable<IContentNode>> temporaryOperations = process.Parameters[Constants.AssetProcessor_Operations] as Dictionary<Operation, IEnumerable<IContentNode>>;
                Debug.Assert(temporaryOperations != null, "If operationsParam is null here, something went very wrong, because we assert that in Prebuild.");
                Dictionary<Operation, IEnumerable<Uri>> operationsParam = TranslateParamToOperationDictionary(temporaryOperations);

                var processInputFiles = process.Inputs.OfType<File>().ToList();
                IEnumerable<Uri> inputUris = processInputFiles.Select(i => new Uri(i.AbsolutePath, UriKind.Absolute));
                Uri output = new Uri(process.Outputs.OfType<File>().First().AbsolutePath, UriKind.Absolute);

                List<AssetProcessorTask> operations = operationsParam.Select(op => new AssetProcessorTask(op.Key, op.Value)).ToList();

                // create a config file for the coming assetprocessor processes
                // if there's any issue with the arguments, the constructor will throw an exception.
                AssetProcessorConfig apc = new AssetProcessorConfig(operations, inputUris, output, param.Log);

                configs.Add(apc);

                // updating values for XGE packing
                accumulatedPacketSize += processInputFiles.Sum(i => i.GetSize());
                inputCount += processInputFiles.Count;

                // updating process with XGE task infos
                process.SetParameter(Constants.ProcessXGE_TaskName, taskName.Replace(" ", "_"));
                process.SetParameter(Constants.ProcessXGE_Task, task);
            }

            tools = new[] {tool};
            tasks = xgeTasks;

            return true;
        }
        #endregion

        #region Helper methods
        /// <summary>
        /// Resolves the IContentNode values in the operations parameter to absolute paths
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        /// We could have done that with LINQ, but I wonder what would be the length of the actual delegate for that.
        private static Dictionary<Operation, IEnumerable<Uri>> TranslateParamToOperationDictionary(Dictionary<Operation, IEnumerable<IContentNode>> source)
        {
            Dictionary<Operation, IEnumerable<Uri>> dictionary = new Dictionary<Operation, IEnumerable<Uri>>(source.Count);

            foreach (var pair in source)
            {
                Operation key = pair.Key;
                List<Uri> value = pair.Value.Select(node => new Uri(((File)node).AbsolutePath, UriKind.RelativeOrAbsolute)).ToList();

                dictionary.Add(key, value);
            }

            return dictionary;
        }
        #endregion
    }

} // RSG.Pipeline.Processor.Common namespace
