﻿//---------------------------------------------------------------------------------------------
// <copyright file="AutomationMonitorApplication.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Rockstar
{
    using System;
    using System.IO;
    using RSG.Base.Configuration;

    /// <summary>
    /// Class for interacting with the Automation Monitor application.
    /// </summary>
    /// Note: this will be extended to support IPC with the Automation Monitor in future.
    /// 
    public class AutomationMonitorApplication
    {
        #region Constants
        /// <summary>
        /// Application path.
        /// </summary>
        private const String APPLICATION = "$(toolsbin)/AutomationMonitor/AutomationMonitor.exe";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Installation executable.
        /// </summary>
        public String InstallExecutable
        {
            get;
            private set;
        }

        /// <summary>
        /// Installation directory.
        /// </summary>
        public String InstallDirectory
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="project"></param>
        public AutomationMonitorApplication(IProject project)
        {
            this.InstallExecutable = project.Environment.Subst(APPLICATION);
            this.InstallDirectory = Path.GetDirectoryName(this.InstallExecutable);
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Rockstar namespace
