﻿//---------------------------------------------------------------------------------------------
// <copyright file="AutomationConsoleApplication.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Interop.Rockstar
{
    using System;
    using System.IO;
    using RSG.Base.Configuration;

    /// <summary>
    /// Class for interacting with the Automation Console application.
    /// </summary>
    public class AutomationConsoleApplication
    {
        #region Constants
        /// <summary>
        /// Application path.
        /// </summary>
        private const String APPLICATION = "$(toolsroot)/ironlib/lib/RSG.Pipeline.Automation.Console.exe";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Installation executable.
        /// </summary>
        public String InstallExecutable
        {
            get;
            private set;
        }

        /// <summary>
        /// Installation directory.
        /// </summary>
        public String InstallDirectory
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="project"></param>
        public AutomationConsoleApplication(IProject project)
        {
            this.InstallExecutable = project.Environment.Subst(APPLICATION);
            this.InstallDirectory = Path.GetDirectoryName(this.InstallExecutable);
        }
        #endregion // Constructor(s)
    }

} // RSG.Interop.Rockstar namespace
