﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetsServiceHostFactory.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.IO;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Configuration;
using RSG.Services.Assets.Data.Entities;
using RSG.Services.Configuration;
using RSG.Services.Configuration.Assets;
using RSG.Services.Persistence;

namespace RSG.Services.Assets.Implementations
{
    /// <summary>
    /// Custom service host factory that injects the server configuration object into
    /// services created via IIS.
    /// </summary>
    public class AssetsServiceHostFactory : MySQLServiceHostFactory<IDatabaseServer>
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="StatisticsServiceHostFactory"/>
        /// class using the server specified in the app settings file.
        /// </summary>
        /// <remarks>
        /// This method is here for IIS purposes only.  Use the overload that accepts a
        /// server config object for self hosted services.
        /// </remarks>
        public AssetsServiceHostFactory()
            : this(GetServerFromAppSettings(new AssetsConfig(new ToolsConfig())), false)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="StatisticsServiceHostFactory"/>
        /// class using the specified server configuration object and an optional flag to
        /// wipe the database.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="wipeDatabase"></param>
        public AssetsServiceHostFactory(IDatabaseServer server, bool wipeDatabase)
            : base(server, wipeDatabase)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        static StatisticsServiceHostFactory()
        {
            LogFactory.Initialize(Path.Combine(LogFactory.GetLogDirectory(), "AssetsService"), false);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a reference to the assembly that contains the NHibernate mappings.
        /// </summary>
        protected override Assembly MappingsAssembly
        {
            get { return typeof(Scene).Assembly; }
        }

        /// <summary>
        /// Gets a reference to the assembly that contains the fluent migrations.
        /// </summary>
        protected override Assembly MigrationsAssembly
        {
            get { return typeof(Scene).Assembly; }
        }

        /// <summary>
        /// Gets the namespace that the migration classes are in.
        /// </summary>
        protected override string MigrationsNamespace
        {
            get { return "RSG.Services.Assets.Data.Migrations"; }
        }
        #endregion
    }
}