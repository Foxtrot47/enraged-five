﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using RSG.Services.Assets.Contracts.ServiceContracts;
using RSG.Services.Configuration;
using RSG.Services.Persistence;
using Db = RSG.Services.Assets.Data.Entities;
using Dto = RSG.Services.Assets.Contracts.DataContracts;

namespace RSG.Services.Assets.Implementations
{
    /// <summary>
    /// Service implementation for the <see cref="IAssetPopulationService"/> and
    /// <see cref="IAssetQueryService"/> service contracts.
    /// </summary>
    public class AssetService : TransactionalService, IAssetPopulationService, IAssetQueryService
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="AssetService"/> class.
        /// </summary>
        public AssetService(IServer server)
            : base(server)
        {
        }
        #endregion

        #region IAssetPopulationService Methods
        /// <summary>
        /// Populates the database with a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="sceneDto"></param>
        public void PopulateSceneXmlFile(string filename, Dto.Scene sceneDto)
        {
            filename = Path.GetFullPath(filename).ToLower();
            ExecuteAsTransaction(session => PopulateSceneXmlFileAction(session, filename, sceneDto));
        }

        /// <summary>
        /// Deletes all data relating to a particular scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        public void DeleteSceneXmlFile(string filename)
        {
            ExecuteAsTransaction(session => DeleteSceneXmlFileAction(session, filename));
        }
        #endregion

        #region IAssetQueryService Methods
        /// <summary>
        /// Retrieves texture usage information for the specified textures.
        /// </summary>
        /// <param name="textures">List of textures to get usage information for.</param>
        /// <returns></returns>
        public IList<Dto.TextureUsage> GetTextureUsage(IEnumerable<string> textures)
        {
            IEnumerable<string> textureFullPaths = textures.Select(item => Path.GetFullPath(item).ToLower());
            return ExecuteAsReadOnlyTransaction(session => GetTextureUsageAction(session, textureFullPaths));
        }

        /// <summary>
        /// Retrieves texture/alpha texture pairs for the passed in texture list.
        /// </summary>
        /// <param name="textures">List of textures to retrieve the pairs for.</param>
        /// <returns></returns>
        public IList<Dto.TexturePair> GetTexturePairs(IEnumerable<string> textures)
        {
            IEnumerable<string> textureFullPaths = textures.Select(item => Path.GetFullPath(item).ToLower());
            return ExecuteAsReadOnlyTransaction(session => GetTexturePairsAction(session, textureFullPaths));
        }
        #endregion

        #region Transaction Methods
        /// <summary>
        /// Adds all the data from a scenexml scene to the database (deleting all
        /// pre-existing data associated with the scene in the process).
        /// </summary>
        /// <param name="session"></param>
        /// <param name="filename"></param>
        /// <param name="sceneDto"></param>
        private void PopulateSceneXmlFileAction(ISession session, string filename, Dto.Scene sceneDto)
        {
            // Delete the existing scenexml data if it already exists for this file.
            DeleteSceneXmlFileAction(session, filename);

            // Create the scene and save it to the db.
            Db.Scene scene = CreateSceneForDto(filename, sceneDto);
            session.Save(scene);
        }

        /// <summary>
        /// Deletes all data associated with a scenexml scene.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="filename"></param>
        private void DeleteSceneXmlFileAction(ISession session, string filename)
        {
            Db.Scene scene = session.QueryOver<Db.Scene>()
                .Where(s => s.Filename == filename)
                .SingleOrDefault();

            if (scene != null)
            {
                session.Delete(scene);
            }
        }

        /// <summary>
        /// Gets a list of scene filenames/texture filename pairs for the specified texture list.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<Dto.TextureUsage> GetTextureUsageAction(ISession session, IEnumerable<string> textures)
        {
            // Construct the criteria for retrieving the results.
            // TODO: Convert to a QueryOver call.
            ICriteria criteria =
                session.CreateCriteria<Db.Texture>()
                    .CreateAlias("Scene", "scene")
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Distinct(Projections.Property("scene.Filename")), "SceneName")
                        .Add(Projections.Property("Filename"), "TextureName"))
                    .Add(Restrictions.In("Filename", textures.ToArray()))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(Dto.TextureUsage)));

            // Execute the query and return the results.
            return criteria.List<Dto.TextureUsage>();
        }
        
        /// <summary>
        /// Gets a list of texture/alpha texture pairs for the specified texture list.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IList<Dto.TexturePair> GetTexturePairsAction(ISession session, IEnumerable<string> textures)
        {
            // Construct the criteria for retrieving the results.
            // TODO: Convert to a QueryOver call.
            ICriteria criteria =
                session.CreateCriteria<Db.Texture>()
                    .SetProjection(Projections.ProjectionList()
                        .Add(Projections.Property("Filename"), "TexturePath")
                        .Add(Projections.Property("AlphaFilename"), "AlphaTexturePath")
                        .Add(Projections.GroupProperty("Filename"))
                        .Add(Projections.GroupProperty("AlphaFilename")))
                    .Add(Restrictions.Or(
                        Restrictions.In("Filename", textures.ToArray()),
                        Restrictions.In("AlphaFilename", textures.ToArray())))
                    .SetResultTransformer(Transformers.AliasToBean(typeof(Dto.TexturePair)));

            // Execute the query and return the results.
            return criteria.List<Dto.TexturePair>();
        }
        #endregion

        #region Dto to Db Conversions
        /// <summary>
        /// Converts a scene dto object to a db object.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="sceneDto"></param>
        /// <returns></returns>
        private Db.Scene CreateSceneForDto(string filename, Dto.Scene sceneDto)
        {
            Db.Scene scene = new Db.Scene();
            scene.Filename = filename;
            scene.Version = sceneDto.Version;
            scene.Timestamp = sceneDto.Timestamp;
            scene.Username = sceneDto.Username;

            // Create the materials that this file contains.
            IDictionary<Guid, Db.Material> materialLookup = new Dictionary<Guid, Db.Material>();
            uint index = 0;

            scene.Materials = new List<Db.Material>();
            foreach (Dto.Material materialDto in sceneDto.Materials)
            {
                Db.Material material = CreateMaterialForDto(materialDto, scene, null, materialLookup, index++);
                material.ParentScene = scene;
                scene.Materials.Add(material);
            }

            // Create the objects that this file contains.
            scene.Objects = new List<Db.Object>();
            foreach (Dto.Object objectDto in sceneDto.Objects)
            {
                Db.Object obj = CreateObjectForDto(objectDto, scene, null, materialLookup);
                obj.ParentScene = scene;
                scene.Objects.Add(obj);
            }

            return scene;
        }

        /// <summary>
        /// Converts a material dto object to a db object.
        /// </summary>
        /// <param name="materialDto"></param>
        /// <param name="scene"></param>
        /// <param name="parent"></param>
        /// <param name="materialLookup"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private Db.Material CreateMaterialForDto(Dto.Material materialDto, Db.Scene scene, Db.Material parent, IDictionary<Guid, Db.Material> materialLookup, uint index)
        {
            Db.Material material = new Db.Material();
            material.Guid = materialDto.Guid;
            material.Type = materialDto.Type;
            material.Preset = materialDto.Preset;
            material.Scene = scene;
            material.ParentMaterial = parent;
            material.MaterialIndex = index;

            // Add any sub-materials this material contains.
            if (materialDto.SubMaterials.Any())
            {
                material.SubMaterials = new List<Db.Material>();
                uint subIndex = 0;

                foreach (Dto.Material subMaterialDto in materialDto.SubMaterials)
                {
                    Db.Material subMaterial = CreateMaterialForDto(subMaterialDto, scene, material, materialLookup, subIndex++);
                    material.SubMaterials.Add(subMaterial);
                }
            }

            // Add any textures this material makes use of.
            if (materialDto.Textures.Any())
            {
                material.Textures = new List<Db.Texture>();

                foreach (Dto.Texture textureDto in materialDto.Textures)
                {
                    Db.Texture texture = CreateTextureForDto(textureDto, scene, material);
                    material.Textures.Add(texture);
                }
            }

            // Keep track of this material for when we parse the objects.
            materialLookup.Add(material.Guid, material);

            return material;
        }

        /// <summary>
        ///  Converts a texture dto object to a db object.
        /// </summary>
        /// <param name="textureDto"></param>
        /// <param name="scene"></param>
        /// <param name="material"></param>
        /// <returns></returns>
        private Db.Texture CreateTextureForDto(Dto.Texture textureDto, Db.Scene scene, Db.Material material)
        {
            Db.Texture texture = new Db.Texture();
            if (!String.IsNullOrEmpty(textureDto.Filename))
            {
                texture.Filename = Path.GetFullPath(textureDto.Filename).ToLower();
            }
            if (!String.IsNullOrEmpty(textureDto.AlphaFilename))
            {
                texture.AlphaFilename = Path.GetFullPath(textureDto.AlphaFilename).ToLower();
            }
            texture.Type = textureDto.Type;
            texture.Scene = scene;
            texture.Material = material;
            return texture;
        }

        /// <summary>
        /// Converts an object dto object to a db object.
        /// </summary>
        /// <param name="objectDto"></param>
        /// <param name="scene"></param>
        /// <param name="parent"></param>
        /// <param name="materialLookup"></param>
        /// <returns></returns>
        private Db.Object CreateObjectForDto(Dto.Object objectDto, Db.Scene scene, Db.Object parent, IDictionary<Guid, Db.Material> materialLookup)
        {
            Db.Object obj = new Db.Object();
            obj.Guid = objectDto.Guid;
            obj.Name = objectDto.Name;
            obj.Class = objectDto.Class;
            obj.SuperClass = objectDto.SuperClass;
            obj.AttributeClass = objectDto.AttributeClass;
            obj.PolyCount = objectDto.PolyCount;
            obj.ParentObject = parent;
            obj.Scene = scene;

            obj.ObjectTranslationX = objectDto.ObjectTranslation.X;
            obj.ObjectTranslationY = objectDto.ObjectTranslation.Y;
            obj.ObjectTranslationZ = objectDto.ObjectTranslation.Z;

            obj.ObjectRotationX = objectDto.ObjectRotation.X;
            obj.ObjectRotationY = objectDto.ObjectRotation.Y;
            obj.ObjectRotationZ = objectDto.ObjectRotation.Z;
            obj.ObjectRotationW = objectDto.ObjectRotation.W;

            obj.ObjectScaleX = objectDto.ObjectScale.X;
            obj.ObjectScaleY = objectDto.ObjectScale.Y;
            obj.ObjectScaleZ = objectDto.ObjectScale.Z;

            obj.NodeTranslationX = objectDto.NodeTranslation.X;
            obj.NodeTranslationY = objectDto.NodeTranslation.Y;
            obj.NodeTranslationZ = objectDto.NodeTranslation.Z;

            obj.NodeRotationX = objectDto.NodeRotation.X;
            obj.NodeRotationY = objectDto.NodeRotation.Y;
            obj.NodeRotationZ = objectDto.NodeRotation.Z;
            obj.NodeRotationW = objectDto.NodeRotation.W;

            obj.NodeScaleX = objectDto.NodeScale.X;
            obj.NodeScaleY = objectDto.NodeScale.Y;
            obj.NodeScaleZ = objectDto.NodeScale.Z;

            obj.LocalBoundingBoxMinX = objectDto.LocalBoundingBox.Min.X;
            obj.LocalBoundingBoxMinY = objectDto.LocalBoundingBox.Min.Y;
            obj.LocalBoundingBoxMinZ = objectDto.LocalBoundingBox.Min.Z;
            obj.LocalBoundingBoxMaxX = objectDto.LocalBoundingBox.Max.X;
            obj.LocalBoundingBoxMaxY = objectDto.LocalBoundingBox.Max.Y;
            obj.LocalBoundingBoxMaxZ = objectDto.LocalBoundingBox.Max.Z;

            obj.WorldBoundingBoxMinX = objectDto.WorldBoundingBox.Min.X;
            obj.WorldBoundingBoxMinY = objectDto.WorldBoundingBox.Min.Y;
            obj.WorldBoundingBoxMinZ = objectDto.WorldBoundingBox.Min.Z;
            obj.WorldBoundingBoxMaxX = objectDto.WorldBoundingBox.Max.X;
            obj.WorldBoundingBoxMaxY = objectDto.WorldBoundingBox.Max.Y;
            obj.WorldBoundingBoxMaxZ = objectDto.WorldBoundingBox.Max.Z;

            // Set the material if the object has one.
            Db.Material material;
            if (objectDto.MaterialGuid != Guid.Empty)
            {
                if (materialLookup.TryGetValue(objectDto.MaterialGuid, out material))
                {
                    obj.Material = material;
                }
                else
                {
                    Log.Warning("Unable to resolve material guid '{0}'.", objectDto.MaterialGuid);
                }
            }

            if (objectDto.Attributes.Any())
            {
                obj.Attributes = new List<Db.Attribute>();

                foreach (Dto.Attribute attributeDto in objectDto.Attributes)
                {
                    Db.Attribute attribute = CreateAttributeForDto(attributeDto, obj);
                    obj.Attributes.Add(attribute);
                }
            }

            if (objectDto.Parameters != null && objectDto.Parameters.Any())
            {
                obj.Parameters = new List<Db.Parameter>();

                foreach (Dto.Parameter parameterDto in objectDto.Parameters)
                {
                    Db.Parameter parameter = CreateParameterForDto(parameterDto, obj);
                    obj.Parameters.Add(parameter);
                }
            }

            // Recurse over any child objects.
            if (objectDto.Children.Any())
            {
                obj.Children = new List<Db.Object>();

                foreach (Dto.Object childObjectDto in objectDto.Children)
                {
                    Db.Object childObj = CreateObjectForDto(childObjectDto, scene, obj, materialLookup);
                    obj.Children.Add(childObj);
                }
            }

            return obj;
        }

        /// <summary>
        /// Converts an attribute dto object to a db object.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="attributeDto"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private Db.Attribute CreateAttributeForDto(Dto.Attribute attributeDto, Db.Object obj)
        {
            Db.Attribute attribute = new Db.Attribute();
            attribute.Name = attributeDto.Name;
            attribute.Object = obj;
            attribute.StringValue = attributeDto.StringValue;
            attribute.IntValue = attributeDto.IntValue;
            attribute.FloatValue = attributeDto.FloatValue;
            attribute.BoolValue = attributeDto.BoolValue;
            return attribute;
        }

        /// <summary>
        /// Converts a parameter dto object to a db object.
        /// </summary>
        /// <param name="parameterDto"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private Db.Parameter CreateParameterForDto(Dto.Parameter parameterDto, Db.Object obj)
        {
            Db.Parameter parameter = new Db.Parameter();
            parameter.Name = parameterDto.Name;
            parameter.Object = obj;
            parameter.StringValue = parameterDto.StringValue;
            parameter.IntValue = parameterDto.IntValue;
            parameter.FloatValue = parameterDto.FloatValue;
            parameter.BoolValue = parameterDto.BoolValue;
            return parameter;
        }
        #endregion
    }
}
