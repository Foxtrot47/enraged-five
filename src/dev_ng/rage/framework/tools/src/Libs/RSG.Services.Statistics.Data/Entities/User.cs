﻿//---------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Contains information about a single user.
    /// </summary>
    public class User : DomainEntityBase
    {
        /// <summary>
        /// Name of studio the user is located at.
        /// </summary>
        public virtual string Studio { get; set; }

        /// <summary>
        /// Windows user name for this user.
        /// </summary>
        public virtual string UserName { get; set; }
    }
}
