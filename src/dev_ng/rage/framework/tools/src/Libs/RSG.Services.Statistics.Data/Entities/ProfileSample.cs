﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSample.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Information about a single profile sample from an application.
    /// </summary>
    public class ProfileSample : DomainEntityBase
    {
        /// <summary>
        /// Child profile samples that make up this sample.
        /// </summary>
        public virtual IList<ProfileSample> ChildSamples { get; set; }

        /// <summary>
        /// Time when this sample ended.
        /// </summary>
        public virtual DateTime EndTime { get; set; }

        /// <summary>
        /// Metric that we're measuring.
        /// </summary>
        public virtual string Metric { get; set; }

        /// <summary>
        /// Parent profile sample.
        /// </summary>
        public virtual ProfileSample ParentSample { get; set; }

        /// <summary>
        /// Reference to profile session that this sample is a part of.
        /// </summary>
        public virtual ProfileSession ProfileSession { get; set; }

        /// <summary>
        /// Time when this sample started.
        /// </summary>
        public virtual DateTime StartTime { get; set; }
    }
}
