﻿//---------------------------------------------------------------------------------------------
// <copyright file="M2014112_1525_AddApplicationSessionExitData.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using FluentMigrator;
using RSG.Services.Persistence.Migrations;

namespace RSG.Services.Statistics.Data.Migrations
{
    /// <summary>
    /// Migration for adding the profiling related tables.
    /// </summary>
    [DatetimeMigration("michael", 2014, 11, 12, 15, 25, "Extending ApplicationSession table with application exit data.")]
    public class M2014112_1525_AddApplicationSessionExitData : Migration
    {
        /// <summary>
        /// Database queries to run to migrate up to this revision.
        /// </summary>
        public override void Up()
        {
            Alter.Table("ApplicationSession")
                .AddColumn("CloseTimestamp")
                    .AsDateTime()
                    .Nullable()
                .AddColumn("ExitCode")
                    .AsInt32()
                    .Nullable();
        }

        /// <summary>
        /// Database queries to run to undo the changes made by this migration.
        /// </summary>
        public override void Down()
        {
            Delete.Column("CloseTimestamp").FromTable("ApplicationSession");
            Delete.Column("ExitCode").FromTable("ApplicationSession");
        }
    }
}
