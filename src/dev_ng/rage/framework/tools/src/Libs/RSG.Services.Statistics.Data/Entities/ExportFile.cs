﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// A single file that was exported as part of an <see cref="ExportProfileSession"/>.
    /// </summary>
    public class ExportFile : DomainEntityBase
    {
        /// <summary>
        /// Gets or sets the full path to the exported file.
        /// </summary>
        public virtual string Filepath { get; set; }
    }
}
