﻿//---------------------------------------------------------------------------------------------
// <copyright file="Application.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Information regarding an application that we have.
    /// </summary>
    public class Application : DomainEntityBase
    {
        /// <summary>
        /// Name of the application.
        /// </summary>
        public virtual string ApplicationName { get; set; }

        /// <summary>
        /// Path to the application that is being launched.
        /// </summary>
        public virtual string LaunchLocation { get; set; }

        /// <summary>
        /// Project that this application is from.
        /// </summary>
        public virtual string Project { get; set; }

        /// <summary>
        /// Tools root that the application is using.
        /// </summary>
        public virtual string ToolsRoot { get; set; }

        /// <summary>
        /// Application version.
        /// </summary>
        public virtual string Version { get; set; }
    }
}
