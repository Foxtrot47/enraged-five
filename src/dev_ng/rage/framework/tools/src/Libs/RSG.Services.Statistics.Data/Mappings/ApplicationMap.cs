﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationMap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Persistence.Mappings;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="Application"/> class.
    /// </summary>
    public class ApplicationMap : DomainEntityBaseMap<Application>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ApplicationMap"/> class.
        /// </summary>
        public ApplicationMap()
        {
            Property(x => x.ApplicationName);
            Property(x => x.LaunchLocation);
            Property(x => x.Project);
            Property(x => x.ToolsRoot);
            Property(x => x.Version);
        }
    }
}
