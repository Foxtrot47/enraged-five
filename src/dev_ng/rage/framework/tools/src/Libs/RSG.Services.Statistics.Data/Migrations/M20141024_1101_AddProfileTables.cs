﻿//---------------------------------------------------------------------------------------------
// <copyright file="M20141024_1101_AddProfileTables.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System.Data;
using FluentMigrator;
using RSG.Services.Persistence.Migrations;

namespace RSG.Services.Statistics.Data.Migrations
{
    /// <summary>
    /// Migration for adding the profiling related tables.
    /// </summary>
    [DatetimeMigration("michael", 2014, 10, 24, 11, 01, "Adding of new profile related tables; ProfileSession and ProfileSample.")]
    public class M20141024_1101_AddProfileTables : Migration
    {
        /// <summary>
        /// Database queries to run to migrate up to this revision.
        /// </summary>
        public override void Up()
        {
            Create.Table("ProfileSession")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("ApplicationSessionId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ProfileSession_ApplicationSessionId_ApplicationSession_Id", "ApplicationSession", "Id")
                .WithColumn("EndTime")
                    .AsDateTime(6)
                    .Nullable()
                .WithColumn("Name")
                    .AsString()
                    .Nullable()
                .WithColumn("ParentSessionGuid")
                    .AsGuid()
                    .Nullable()
                    //.ForeignKey("FK_ProfileSession_ParentSessionGuid_ProfileSession_SessionGuid", "ProfileSession", "SessionGuid")
                .WithColumn("SessionGuid")
                    .AsGuid()
                    .Nullable()
                    .Unique()
                .WithColumn("StartTime")
                    .AsDateTime(6)
                    .Nullable();
            // Can't add the foreign key inline for some reason o_O.
            Create.ForeignKey("FK_ProfileSession_ParentSessionGuid_ProfileSession_SessionGuid")
                .FromTable("ProfileSession")
                    .ForeignColumn("ParentSessionGuid")
                .ToTable("ProfileSession")
                    .PrimaryColumn("SessionGuid");
            
            Create.Table("ProfileSample")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("EndTime")
                    .AsDateTime(6)
                    .Nullable()
                .WithColumn("Metric")
                    .AsString()
                    .Nullable()
                .WithColumn("ParentSampleId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ProfileSample_ParentSampleId_ProfileSample_Id", "ProfileSample", "Id")
                .WithColumn("ProfileSessionId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ProfileSample_ProfileSessionId_ProfileSession_Id", "ProfileSession", "Id")
                .WithColumn("StartTime")
                    .AsDateTime(6)
                    .Nullable();

            Create.Table("ExportFile")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("Filepath")
                    .AsString()
                    .Nullable();
            
            Create.Table("ExportProfileSession")
                .WithColumn("ProfileSessionId")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .ForeignKey("FK_ExportProfileSessionMap_ProfileSessionId_ProfileSession_Id", "ProfileSession", "Id")
                        .OnDelete(Rule.Cascade)
                .WithColumn("IncredibuildEnabled")
                    .AsBoolean()
                    .Nullable()
                .WithColumn("IncredibuildStandalone")
                    .AsBoolean()
                    .Nullable()
                .WithColumn("IncredibuildForcedCPUCount")
                    .AsInt32()
                    .Nullable()
                .WithColumn("PrivateBytesEnd")
                    .AsInt64()
                    .Nullable()
                .WithColumn("PrivateBytesStart")
                    .AsInt64()
                    .Nullable()
                .WithColumn("Success")
                    .AsBoolean()
                    .Nullable()
                .WithColumn("ToolsVersion")
                    .AsString()
                    .Nullable();

            Create.Table("ExportProfileSessionFiles")
                .WithColumn("ExportProfileSessionId")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("FK_EPSF_ExportProfileSessionId_EPS_ProfileSessionId", "ExportProfileSession", "ProfileSessionId")
                .WithColumn("FileId")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("FK_ExportProfileSessionFiles_FileId_ExportFile_Id", "ExportFile", "Id");
            Create.PrimaryKey()
                .OnTable("ExportProfileSessionFiles")
                .Columns("ExportProfileSessionId", "FileId");

            Create.Table("ExportProfileSessionPlatforms")
                .WithColumn("ExportProfileSessionId")
                    .AsInt64()
                    .NotNullable()
                    .ForeignKey("FK_EPSP_ExportProfileSessionId_EPS_ProfileSessionId", "ExportProfileSession", "ProfileSessionId")
                .WithColumn("Platform")
                    .AsInt32()
                    .NotNullable();
            Create.PrimaryKey()
                .OnTable("ExportProfileSessionPlatforms")
                .Columns("ExportProfileSessionId", "Platform");
        }

        /// <summary>
        /// Database queries to run to undo the changes made by this migration.
        /// </summary>
        public override void Down()
        {
            Delete.Table("ExportProfileSessionPlatforms");
            Delete.Table("ExportProfileSessionFiles");
            Delete.Table("ExportProfileSession");
            Delete.Table("ExportFile");

            Delete.Table("ProfileSample");
            Delete.ForeignKey("FK_ProfileSession_ParentSessionGuid_ProfileSession_SessionGuid").OnTable("ProfileSession");
            Delete.Table("ProfileSession");
        }
    }
}
