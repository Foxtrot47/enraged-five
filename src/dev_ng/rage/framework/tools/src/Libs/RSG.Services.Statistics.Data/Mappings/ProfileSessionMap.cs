﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSessionMap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Properties;
using NHibernate.Type;
using RSG.Services.Persistence.Mappings;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="ProfileSession"/> class.
    /// </summary>
    public class ProfileSessionMap : DomainEntityBaseMap<ProfileSession>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ProfileSessionMap"/> class.
        /// </summary>
        public ProfileSessionMap()
        {
            ManyToOne(x => x.ApplicationSession, m =>
            {
                m.Column("ApplicationSessionId");
                m.ForeignKey("FK_ProfileSession_ApplicationSessionId_ApplicationSession_Id");
            });

            Property(x => x.EndTime, m => m.Type<TimestampType>());
            Property(x => x.Name);
            Property(x => x.StartTime, m => m.Type<TimestampType>());

            ManyToOne(x => x.ParentSession, m =>
            {
                m.Column("ParentSessionGuid");
                m.PropertyRef("SessionGuid");
                m.ForeignKey("FK_ProfileSession_ParentSessionGuid_ProfileSession_SessionGuid");
                m.Insert(false);
                m.Update(false);
            });

            Property(x => x.ParentSessionGuid);

            Property(x => x.SessionGuid, m => m.Unique(true));

            Bag(x => x.SubSessions, m =>
            {
                m.Key(k =>
                {
                    k.Column("ParentSessionGuid");
                    k.PropertyRef(p => p.SessionGuid);
                    k.ForeignKey("FK_ProfileSession_ParentSessionGuid_ProfileSession_SessionGuid");
                });
                m.Cascade(Cascade.None);
                m.Inverse(true);
            }, r => r.OneToMany());

            Bag(x => x.TopLevelSamples, m =>
            {
                m.Key(k => k.Column("ProfileSessionId"));
                m.Cascade(Cascade.All);
                m.Inverse(true);
            }, r => r.OneToMany());
        }
    }
}
