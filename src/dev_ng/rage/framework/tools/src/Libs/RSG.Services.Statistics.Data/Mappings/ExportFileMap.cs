﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Persistence.Mappings;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="ExportFile"/> class.
    /// </summary>
    public class ExportFileMap : DomainEntityBaseMap<ExportFile>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ExportFileMap"/> class.
        /// </summary>
        public ExportFileMap()
        {
            Property(x => x.Filepath);
        }
    }
}
