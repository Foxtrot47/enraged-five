﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSession.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Information regarding a profile session.
    /// </summary>
    public class ProfileSession : DomainEntityBase
    {
        /// <summary>
        /// Reference to application session that this sample come from.
        /// </summary>
        public virtual ApplicationSession ApplicationSession { get; set; }

        /// <summary>
        /// Time when this session ended.
        /// </summary>
        public virtual DateTime EndTime { get; set; }

        /// <summary>
        /// Name for this profile session.
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// Parent profile session.  Can be used to tie profile sessions from multiple
        /// applications together (e.g. App 1 has an active profile session but invokes
        /// another application which starts it's own profile session).
        /// </summary>
        public virtual ProfileSession ParentSession { get; protected set; }

        /// <summary>
        /// Guid of the parent profile session.
        /// </summary>
        public virtual Guid? ParentSessionGuid { get; set; }

        /// <summary>
        /// Unique guid for this session.
        /// </summary>
        public virtual Guid SessionGuid { get; set; }

        /// <summary>
        /// Time when this session started.
        /// </summary>
        public virtual DateTime StartTime { get; set; }

        /// <summary>
        /// Child profile sessions.
        /// </summary>
        public virtual IList<ProfileSession> SubSessions { get; set; }

        /// <summary>
        /// Top level samples that make up this session.
        /// </summary>
        public virtual IList<ProfileSample> TopLevelSamples { get; set; }
    }
}
