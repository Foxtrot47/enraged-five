﻿//---------------------------------------------------------------------------------------------
// <copyright file="MachineMap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Persistence.Mappings;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="Machine"/> class.
    /// </summary>
    public class MachineMap : DomainEntityBaseMap<Machine>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="MachineMap"/> class.
        /// </summary>
        public MachineMap()
        {
            Property(x => x.CoreCount);
            Property(x => x.GraphicsCardName);
            Property(x => x.LogicalProcessorCount);
            Property(x => x.MachineName, m => m.Length(16));
            Property(x => x.MemoryCapacity);
            Property(x => x.ProcessorName);
        }
    }
}
