﻿//---------------------------------------------------------------------------------------------
// <copyright file="M201409282243_InitialDatabase.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using FluentMigrator;
using RSG.Services.Persistence.Migrations;

namespace RSG.Services.Statistics.Data.Migrations
{
    /// <summary>
    /// Migration for the initial database creation.
    /// </summary>
    [DatetimeMigration("michael", 2014, 09, 28, 22, 43, "Initial database creation")]
    public class M20140928_2243_InitialDatabase : Migration
    {
        /// <summary>
        /// Database queries to run to migrate up to this revision.
        /// </summary>
        public override void Up()
        {
            Create.Table("Machine")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("MachineName")
                    .AsString(16)
                    .Nullable()
                .WithColumn("ProcessorName")
                    .AsString()
                    .Nullable()
                .WithColumn("LogicalProcessorCount")
                    .AsUInt32()
                    .Nullable()
                .WithColumn("CoreCount")
                    .AsUInt32()
                    .Nullable()
                .WithColumn("MemoryCapacity")
                    .AsUInt64()
                    .Nullable()
                .WithColumn("GraphicsCardName")
                    .AsString()
                    .Nullable();

            Create.Table("User")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("UserName")
                    .AsString()
                    .Nullable()
                .WithColumn("Studio")
                    .AsString()
                    .Nullable();

            Create.Table("Application")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("ApplicationName")
                    .AsString()
                    .Nullable()
                .WithColumn("Version")
                    .AsString()
                    .Nullable()
                .WithColumn("Project")
                    .AsString()
                    .Nullable()
                .WithColumn("ToolsRoot")
                    .AsString()
                    .Nullable()
                .WithColumn("LaunchLocation")
                    .AsString()
                    .Nullable();

            Create.Table("ApplicationSession")
                .WithColumn("Id")
                    .AsInt64()
                    .NotNullable()
                    .PrimaryKey()
                    .Identity()
                .WithColumn("SessionGuid")
                    .AsGuid()
                    .Nullable()
                    .Indexed("IX_SessionGuid")
                .WithColumn("OpenTimestamp")
                    .AsDateTime()
                    .Nullable()
                .WithColumn("ParentSessionId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ApplicationSession_ParentSessionId_ApplicationSession_Id", "ApplicationSession", "Id")
                .WithColumn("ApplicationId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ApplicationSession_ApplicationId_Application_Id", "Application", "Id")
                .WithColumn("UserId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ApplicationSession_UserId_User_Id", "User", "Id")
                .WithColumn("MachineId")
                    .AsInt64()
                    .Nullable()
                    .ForeignKey("FK_ApplicationSession_MachineId_Machine_Id", "Machine", "Id");
        }

        /// <summary>
        /// Database queries to run to undo the changes made by this migration.
        /// </summary>
        public override void Down()
        {
            Delete.Table("ApplicationSession");
            Delete.Table("Application");
            Delete.Table("User");
            Delete.Table("Machine");
        }
    }
}
