﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="ExportProfileSession"/> class.
    /// </summary>
    public class ExportProfileSessionMap : JoinedSubclassMapping<ExportProfileSession>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ExportProfileSessionMap"/> class.
        /// </summary>
        public ExportProfileSessionMap()
        {
            Key(k =>
                {
                    k.Column("ProfileSessionId");
                    k.OnDelete(OnDeleteAction.Cascade);
                    k.ForeignKey("FK_ExportProfileSessionMap_ProfileSessionId_ProfileSession_Id");
                });

            Set(x => x.ExportedFiles,
                m =>
                {
                    m.Table("ExportProfileSessionFiles");
                    m.Cascade(Cascade.All);
                    m.Key(k =>
                        {
                            k.Column("ExportProfileSessionId");
                            k.ForeignKey("FK_EPSF_ExportProfileSessionId_EPS_ProfileSessionId");
                        });
                },
                r => r.ManyToMany(p =>
                    {
                        p.Column("FileId");
                        p.ForeignKey("FK_ExportProfileSessionFiles_FileId_ExportFile_Id");
                    }));

            Property(x => x.IncredibuildEnabled);
            Property(x => x.IncredibuildStandalone);
            Property(x => x.IncredibuildForcedCPUCount);

            // Platforms
            Set(x => x.Platforms,
                m =>
                {
                    m.Table("ExportProfileSessionPlatforms");
                    m.Cascade(Cascade.All);
                    m.Key(k =>
                        {
                            k.Column("ExportProfileSessionId");
                            k.ForeignKey("FK_EPSP_ExportProfileSessionId_EPS_ProfileSessionId");
                        });
                },
                r => r.Element(e =>
                    {
                        e.Column("Platform");
                        e.NotNullable(true);
                    }));

            Property(x => x.PrivateBytesEnd);
            Property(x => x.PrivateBytesStart);
            Property(x => x.Success);
            Property(x => x.ToolsVersion);
        }
    }
}
