﻿//---------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Persistence.Mappings;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="User"/> class.
    /// </summary>
    public class UserMap : DomainEntityBaseMap<User>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="UserMap"/> class.
        /// </summary>
        public UserMap()
        {
            Property(x => x.Studio);
            Property(x => x.UserName);
        }
    }
}
