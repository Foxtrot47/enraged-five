﻿//---------------------------------------------------------------------------------------------
// <copyright file="Machine.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Contains information about an user's machine.
    /// </summary>
    public class Machine : DomainEntityBase
    {
        /// <summary>
        /// Number of cores each processor has.
        /// </summary>
        public virtual uint CoreCount { get; set; }

        /// <summary>
        /// Name of the graphics card.
        /// </summary>
        public virtual string GraphicsCardName { get; set; }

        /// <summary>
        /// Number of logical processors the machine has.
        /// </summary>
        public virtual uint LogicalProcessorCount { get; set; }

        /// <summary>
        /// Name of the actual machine.
        /// </summary>
        public virtual string MachineName { get; set; }

        /// <summary>
        /// Total memory capacity of the machine.
        /// </summary>
        public virtual ulong MemoryCapacity { get; set; }

        /// <summary>
        /// Name of the processor.
        /// </summary>
        public virtual string ProcessorName { get; set; }
    }
}
