﻿//---------------------------------------------------------------------------------------------
// <copyright file="ExportProfileSession.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Custom profile session for an export.
    /// </summary>
    public class ExportProfileSession : ProfileSession
    {
        /// <summary>
        /// List of files that were exported as part of this export.
        /// </summary>
        public virtual ICollection<ExportFile> ExportedFiles { get; set; }

        /// <summary>
        /// Whether the export was done with XGE enabled.
        /// </summary>
        public virtual bool IncredibuildEnabled { get; set; }

        /// <summary>
        /// Whether the export was done using XGE in standalone mode.
        /// </summary>
        public virtual bool IncredibuildStandalone { get; set; }

        /// <summary>
        /// The number of cpus that were forced.
        /// </summary>
        public virtual int IncredibuildForcedCPUCount { get; set; }

        /// <summary>
        /// List of platforms that were selected for this export.
        /// </summary>
        public virtual ICollection<RSG.Platform.Platform> Platforms { get; set; }

        /// <summary>
        /// Private bytes at the end of the export.
        /// </summary>
        public virtual long PrivateBytesEnd { get; set; }

        /// <summary>
        /// Private bytes at the start of the export.
        /// </summary>
        public virtual long PrivateBytesStart { get; set; }

        /// <summary>
        /// Whether the export was successful or not.
        /// </summary>
        public virtual bool Success { get; set; }

        /// <summary>
        /// Version of the tools the user was on when they performed the export.
        /// </summary>
        public virtual string ToolsVersion { get; set; }
    }
}
