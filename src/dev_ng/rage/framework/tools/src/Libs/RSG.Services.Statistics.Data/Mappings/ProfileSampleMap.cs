﻿//---------------------------------------------------------------------------------------------
// <copyright file="ProfileSampleMap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using NHibernate.Mapping.ByCode;
using NHibernate.Type;
using RSG.Services.Persistence.Mappings;
using RSG.Services.Statistics.Data.Entities;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="ProfileSample"/> class.
    /// </summary>
    public class ProfileSampleMap : DomainEntityBaseMap<ProfileSample>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ProfileSessionMap"/> class.
        /// </summary>
        public ProfileSampleMap()
        {
            Bag(x => x.ChildSamples, m =>
            {
                m.Key(k => k.Column("ParentSampleId"));
                m.Cascade(Cascade.All);
                m.Inverse(true);
            }, r => r.OneToMany());
            Property(x => x.EndTime, m => m.Type<TimestampType>());
            Property(x => x.Metric);
            ManyToOne(x => x.ParentSample, m =>
            {
                m.Column("ParentSampleId");
                m.ForeignKey("FK_ProfileSample_ParentSampleId_ProfileSample_Id");
            });
            ManyToOne(x => x.ProfileSession, m =>
            {
                m.Column("ProfileSessionId");
                m.ForeignKey("FK_ProfileSample_ProfileSessionId_ProfileSession_Id");
            });
            Property(x => x.StartTime, m => m.Type<TimestampType>());
        }
    }
}
