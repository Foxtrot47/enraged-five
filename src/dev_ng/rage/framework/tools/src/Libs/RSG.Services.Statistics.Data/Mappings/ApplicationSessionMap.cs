﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationSessionMap.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using RSG.Services.Statistics.Data.Entities;
using RSG.Services.Persistence.Mappings;

namespace RSG.Services.Statistics.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for the <see cref="ApplicationSession"/> class.
    /// </summary>
    public class ApplicationSessionMap : DomainEntityBaseMap<ApplicationSession>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ApplicationSessionMap"/> class.
        /// </summary>
        public ApplicationSessionMap()
        {
            ManyToOne(x => x.Application, m =>
            {
                m.Column("ApplicationId");
                m.ForeignKey("FK_ApplicationSession_ApplicationId_Application_Id");
            });

            Property(x => x.CloseTimestamp);
            Property(x => x.ExitCode);

            ManyToOne(x => x.Machine, m =>
            {
                m.Column("MachineId");
                m.ForeignKey("FK_ApplicationSession_MachineId_Machine_Id");
            });

            Property(x => x.OpenTimestamp);

            ManyToOne(x => x.ParentSession, m =>
            {
                m.Column("ParentSessionId");
                m.ForeignKey("FK_ApplicationSession_ParentSessionId_ApplicationSession_Id");
            });

            Property(x => x.SessionGuid, m => m.Index("IX_SessionGuid"));

            ManyToOne(x => x.User, m =>
            {
                m.Column("UserId");
                m.ForeignKey("FK_ApplicationSession_UserId_User_Id");
            });
        }
    }
}
