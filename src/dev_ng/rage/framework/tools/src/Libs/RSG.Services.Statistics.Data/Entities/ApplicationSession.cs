﻿//---------------------------------------------------------------------------------------------
// <copyright file="ApplicationSession.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using RSG.Services.Persistence.Data;

namespace RSG.Services.Statistics.Data.Entities
{
    /// <summary>
    /// Object that represents a single application session. Any stats that we wish to
    /// track for an application should tie back to this.
    /// </summary>
    public class ApplicationSession : DomainEntityBase
    {
        /// <summary>
        /// The actual application that was run.
        /// </summary>
        public virtual Application Application { get; set; }

        /// <summary>
        /// Time that the user closed the application.
        /// </summary>
        public virtual DateTime? CloseTimestamp { get; set; }

        /// <summary>
        /// Exit code that the application returned when closing.
        /// </summary>
        public virtual int? ExitCode { get; set; }

        /// <summary>
        /// The machine that the application was run on.
        /// </summary>
        public virtual Machine Machine { get; set; }

        /// <summary>
        /// Time that the user started the application.
        /// </summary>
        public virtual DateTime OpenTimestamp { get; set; }

        /// <summary>
        /// Parent session.  This can be used for tracking stats on "applications" that
        /// run within another application (for example tracking stats on max tools).
        /// </summary>
        public virtual ApplicationSession ParentSession { get; set; }

        /// <summary>
        /// Unique guid for this session.
        /// </summary>
        public virtual Guid SessionGuid { get; set; }

        /// <summary>
        /// The user that ran the application.
        /// </summary>
        public virtual User User { get; set; }
    }
}
