﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides multiplayer transaction statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class ConcurrentUsersReport : PlayerStatsReportBase<HistoricalConcurrentUserStats, HistoricalTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ConcurrentUsersReport()
            : base(ReportNames.ConcurrentUsersReport)
        {
        }
        #endregion // Constructor(s)
        
        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override HistoricalConcurrentUserStats Generate(ISession session, HistoricalTelemetryParams parameters)
        {
            HistoricalConcurrentUserStats stats = new HistoricalConcurrentUserStats();
            stats.UniqueGamers = session.RetrieveScalar<long>(new StringQuery(CreateOverallQuery(parameters)));

            // Get the per date grouped results.
            if (stats.UniqueGamers > 0)
            {
                stats.Stats.AddRange(session.RetrieveAll<ConcurrentUserStat>(new StringQuery(CreateHistoricalQuery(parameters))));
            }

            return stats;
        }
        #endregion // VerticaReportBase Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateOverallQuery(HistoricalTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     COUNT(DISTINCT(AccountId)) AS UniquePlayers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_header h
            // WHERE DateTime BETWEEN '2013-07-10 14:30:00Z' AND '2013-07-10 18:30:00Z';

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("IFNULL(SUM(UniqueGamers), 0) AS UniqueGamers ");
            sb.AppendFormat("FROM ( ");
            {
                String schema = Conversion.Schema();
                sb.AppendFormat("SELECT COUNT(DISTINCT h.AccountId) AS UniqueGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_header h ", schema);
                AddEmbeddedRestrictions(sb, parameters, "h");
            }

            sb.AppendFormat(") AS AllStats;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateHistoricalQuery(HistoricalTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
	        //     TIMESTAMPADD(MINUTE, CAST(FLOOR(DATEDIFF(MINUTE, DATE(1), DateTime) / 15) AS INTEGER) * 15, DATE(1)) AS DateVal,
            //     COUNT(DISTINCT(AccountId)) AS UniqueGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_header h
            // WHERE DateTime BETWEEN '2013-07-10 14:30:00Z' AND '2013-07-10 18:30:00Z'
            // GROUP BY DateVal;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("\"Start\", ");
            sb.Append("\"End\", ");
            sb.Append("1 AS StartInclusive, ");
            sb.Append("0 AS EndInclusive, ");
            sb.Append("IFNULL(SUM(UniqueGamers), 0) AS Value ");
            sb.Append("FROM ( ");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                sb.AppendFormat("{0} AS \"Start\", ", CreateDateTimeGroupQuery(parameters.GroupMode, DateTimeClampMode.Floor, "DateTime"));
                sb.AppendFormat("{0} AS \"End\", ", CreateDateTimeGroupQuery(parameters.GroupMode, DateTimeClampMode.Ceiling, "DateTime"));
                sb.Append("COUNT(DISTINCT(h.AccountId)) AS UniqueGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_header h ", schema);
                AddEmbeddedRestrictions(sb, parameters, "h");
                sb.Append("GROUP BY \"Start\", \"End\" ");
            }

            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY \"Start\", \"End\" ");
            sb.Append("ORDER BY \"Start\" ASC;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override bool RequiresTelemetryHeaderJoin(TelemetryParams parameters)
        {
            return false;
        }
        #endregion // Private Methods
    } // ConcurrentUsersReport
}
