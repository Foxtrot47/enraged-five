﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides per user friend activity statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserFriendActivitiesReport : PerUserTelemetryReportBase<List<PerUserFriendActivityStats>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserFriendActivitiesReport()
            : base(ReportNames.PerUserFriendActivitiesReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserFriendActivityStats> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserFriendActivityStats> allStats = new List<PerUserFriendActivityStats>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS GamerHandle,
            //     fa.character AS Character,
            //     fa.result AS Result,
            //     COUNT(fa.SubmissionId) AS Count
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_FRIEND_ACT fa
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=fa.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName, fa.character, fa.result;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS GamerHandle, ");
            sb.Append("fa.character AS Character, ");
            sb.Append("fa.result AS Result, ");
            sb.Append("COUNT(fa.SubmissionId) AS Count ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_FRIEND_ACT fa ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=fa.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "fa", platform);
            sb.Append("GROUP BY pa.UserName, fa.character, fa.result;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserFriendActivityStats> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserFriendActivityStats> perUserStats = new Dictionary<String, PerUserFriendActivityStats>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserFriendActivityStats(username, platform));
                }

                int character = dr.GetInt32(idx++);
                int result = dr.GetInt32(idx++);
                uint count = dr.GetUInt32(idx++);

                FriendActivityStat stat = perUserStats[username].Data.FirstOrDefault(item => item.Character == character);
                if (stat == null)
                {
                    stat = new FriendActivityStat();
                    stat.Character = character;
                    perUserStats[username].Data.Add(stat);
                }

                if (result == 0)
                {
                    stat.FailedCount += count;
                }
                else if (result == 1)
                {
                    stat.SuccessCount += count;
                }
                else if (result == 2)
                {
                    stat.AbortedCount += count;
                }
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserFriendActivitiesReport
}
