﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides summary information about all singleplayer mission attempts that took place.
    /// for the given inputs.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class CharacterSkillsReport : PlayerStatsReportBase<List<PerMissionCharacterSkillStats>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CharacterSkillsReport()
            : base(ReportNames.CharacterSkillsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerMissionCharacterSkillStats> Generate(ISession session, TelemetryParams parameters)
        {
            List<PerMissionCharacterSkillStats> stats = new List<PerMissionCharacterSkillStats>();

            VerticaDataReader dr = session.ExecuteQuery(CreateSqlQueryString(parameters));
            while (dr.Read())
            {
                PerMissionCharacterSkillStats stat = new PerMissionCharacterSkillStats();
                stat.MissionName = dr.GetString(0);
                ParseCharacterSkillResults(dr, 1, stat.Michael);
                ParseCharacterSkillResults(dr, 9, stat.Franklin);
                ParseCharacterSkillResults(dr, 17, stat.Trevor);
                stats.Add(stat);
            }
            return stats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     sk.MissionId,
            //     AVG(sk.MichaelSpecialAbility), AVG(sk.MichaelStamina), AVG(sk.MichaelShooting), AVG(sk.MichaelStrength),
            //     AVG(sk.MichaelStealth), AVG(sk.MichaelFlying), AVG(sk.MichaelDriving), AVG(sk.MichaelLungCapacity),
            //     AVG(sk.FranklinSpecialAbility), AVG(sk.FranklinStamina), AVG(sk.FranklinShooting), AVG(sk.FranklinStrength),
            //     AVG(sk.FranklinStealth), AVG(sk.FranklinFlying), AVG(sk.FranklinDriving), AVG(sk.FranklinLungCapacity),
            //     AVG(sk.TrevorSpecialAbility), AVG(sk.TrevorStamina), AVG(sk.TrevorShooting), AVG(sk.TrevorStrength),
            //     AVG(sk.TrevorStealth), AVG(sk.TrevorFlying), AVG(sk.TrevorDriving), AVG(sk.TrevorLungCapacity)
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_CHARACTER_SKILLS sk
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=sk.SubmissionId
            // WHERE h.ver=5632
            // GROUP BY sk.MissionId;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("MissionName, ");
            sb.AppendFormat("AVG(MichaelSpecialAbility), AVG(MichaelStamina), AVG(MichaelShooting), AVG(MichaelStrength), ");
            sb.AppendFormat("AVG(MichaelStealth), AVG(MichaelFlying), AVG(MichaelDriving), AVG(MichaelLungCapacity), ");
            sb.AppendFormat("AVG(FranklinSpecialAbility), AVG(FranklinStamina), AVG(FranklinShooting), AVG(FranklinStrength), ");
            sb.AppendFormat("AVG(FranklinStealth), AVG(FranklinFlying), AVG(FranklinDriving), AVG(FranklinLungCapacity), ");
            sb.AppendFormat("AVG(TrevorSpecialAbility), AVG(TrevorStamina), AVG(TrevorShooting), AVG(TrevorStrength), ");
            sb.AppendFormat("AVG(TrevorStealth), AVG(TrevorFlying), AVG(TrevorDriving), AVG(TrevorLungCapacity) ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT sk.* ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_CHARACTER_SKILLS sk ", schema);
                sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=sk.SubmissionId ", schema);
                AddRestrictions(sb, parameters, "sk");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY MissionName;");
            return sb.ToString();
        }

        /// <summary>
        /// Parses a set of character skill stats.
        /// </summary>
        private void ParseCharacterSkillResults(VerticaDataReader dr, int offset, CharacterSkillStats stats)
        {
            if (dr[offset] != DBNull.Value)
            {
                stats.SpecialAbility = dr.GetFloat(offset);
            }
            if (dr[offset + 1] != DBNull.Value)
            {
                stats.Stamina = dr.GetFloat(offset + 1);
            }
            if (dr[offset + 2] != DBNull.Value)
            {
                stats.Shooting = dr.GetFloat(offset + 2);
            }
            if (dr[offset + 3] != DBNull.Value)
            {
                stats.Strength = dr.GetFloat(offset + 3);
            }
            if (dr[offset + 4] != DBNull.Value)
            {
                stats.Stealth = dr.GetFloat(offset + 4);
            }
            if (dr[offset + 5] != DBNull.Value)
            {
                stats.Flying = dr.GetFloat(offset + 5);
            }
            if (dr[offset + 6] != DBNull.Value)
            {
                stats.Driving = dr.GetFloat(offset + 6);
            }
            if (dr[offset + 7] != DBNull.Value)
            {
                stats.LungCapacity = dr.GetFloat(offset + 7);
            }
        }
        #endregion // Private Methods
    } // CharacterSkillsReport
}
