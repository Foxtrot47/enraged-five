﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides kill or death statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class KillDeathReport : PlayerStatsReportBase<List<KillDeathStat>, KillDeathParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public KillDeathReport()
            : base(ReportNames.KillDeathReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<KillDeathStat> Generate(ISession session, KillDeathParams parameters)
        {
            IQuery query;
            if (parameters.KillsOrDeaths == KillDeathMode.Kills)
            {
                query = new StringQuery(CreateKillQuery(parameters));
            }
            else
            {
                query = new StringQuery(CreateDeathQuery(parameters));
            }
            return session.RetrieveAll<KillDeathStat>(query).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateDeathQuery(KillDeathParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     d.killerWeapon AS Weapon,
            //     COUNT(*) AS Count,
            //     COUNT(DISTINCT d.Header_AccountId) AS NumGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_DEATH d
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h ON h.SubmissionId=d.SubmissionId
            // WHERE h.ver=5728 AND h.mp!=0 AND (d.victimInfo & '1') != 0
            // GROUP BY d.killerWeapon;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("WeaponHash, ");
            sb.AppendFormat("SUM(Count) AS Count, ");
            sb.AppendFormat("SUM(NumberOfGamers) AS NumberOfGamers ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT Deaths.killerWeapon AS WeaponHash, COUNT(*) AS Count, COUNT(DISTINCT(Deaths.Header_AccountId)) AS NumberOfGamers ");
                sb.Append("FROM ( ");
                sb.Append("SELECT d.killerWeapon, d.Header_AccountId, d.Header_mid ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_DEATH d ", schema);
                AddEmbeddedRestrictions(sb, parameters, "d", new String[] { "(d.victimInfo & '1') != 0",
                    String.Format("d.Header_mid {0} 0", parameters.FreeroamOnly ? "=" : "!=") });
                sb.Append(") AS Deaths ");

                if (!parameters.FreeroamOnly && parameters.MatchTypeRestrictions.Any())
                {
                    sb.Append("INNER JOIN ( ");
                    sb.Append("SELECT job.Header_AccountId, job.Header_mid, job.MatchType ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5_JOB job ", schema);

                    IList<String> restrictions = new List<String>();
                    restrictions.Add("job.Header_mid != 0");
                    restrictions.Add(String.Format("job.MatchType IN ({0})", String.Join(", ", parameters.MatchTypeRestrictions.Select(item => (int)item))));

                    AddEmbeddedRestrictions(sb, parameters, "job", restrictions);
                    sb.Append(") AS Matches ON Matches.Header_AccountId=Deaths.Header_AccountId AND Matches.Header_mid=Deaths.Header_mid ");
                }
                sb.AppendFormat("GROUP BY Deaths.killerWeapon ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY WeaponHash;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateKillQuery(KillDeathParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
	        //     d.killerWeapon AS Weapon,
	        //     COUNT(*) AS Count,
	        //     COUNT(DISTINCT d.Header_AccountId) AS NumGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_DEATH d
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h ON h.SubmissionId=d.SubmissionId

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("WeaponHash, ");
            sb.AppendFormat("SUM(Count) AS Count, ");
            sb.AppendFormat("SUM(NumberOfGamers) AS NumberOfGamers ");
            sb.AppendFormat("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT Deaths.killerWeapon AS WeaponHash, COUNT(*) AS Count, COUNT(DISTINCT(Deaths.Header_AccountId)) AS NumberOfGamers ");
                sb.Append("FROM ( ");
                sb.Append("SELECT d.killerWeapon, d.Header_AccountId, d.Header_mid ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_DEATH d ", schema);
                AddInvertedRestrictions(sb, parameters, "d", new String[] { "(d.killerInfo & '1') != 0",
                    String.Format("d.Header_mid {0} 0", parameters.FreeroamOnly ? "=" : "!=")});
                sb.Append(") AS Deaths ");

                if (!parameters.FreeroamOnly && parameters.MatchTypeRestrictions.Any())
                {
                    sb.Append("INNER JOIN ( ");
                    sb.Append("SELECT job.Header_AccountId, job.Header_mid, job.MatchType ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5_JOB job ", schema);
                    sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=job.SubmissionId ", schema);

                    IList<String> restrictions = new List<String>();
                    restrictions.Add("job.Header_mid != 0");
                    restrictions.Add(String.Format("job.MatchType IN ({0})", String.Join(", ", parameters.MatchTypeRestrictions.Select(item => (int)item))));

                    AddEmbeddedRestrictions(sb, parameters, "job", restrictions);
                    sb.Append(") AS Matches ON Matches.Header_AccountId=Deaths.Header_AccountId AND Matches.Header_mid=Deaths.Header_mid ");
                }
                sb.AppendFormat("GROUP BY Deaths.killerWeapon ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY WeaponHash;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void AddInvertedRestrictions(StringBuilder sb,
            TelemetryParams parameters, String tableAlias, IEnumerable<String> additionalRestrictions = null)
        {
            // Check whether which tables we need to join.
            if (RequiresTelemetryHeaderJoin(parameters))
            {
                AddTelemetryHeaderJoin(sb, tableAlias);
            }
            if (RequiresPlayerAccountJoin(parameters) || RequiresRockstarAccountJoin(parameters))
            {
                throw new NotSupportedException();
            }
            if (RequiresRockstarAccountJoin(parameters))
            {
                throw new NotSupportedException();
            }

            IList<String> restrictions = GetEmbeddedRestrictions(parameters, tableAlias).ToList();
            if (additionalRestrictions != null && additionalRestrictions.Any())
            {
                restrictions.AddRange(additionalRestrictions);
            }

            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
            }
        }
        #endregion // Private Methods
    } // KillDeathReport
}
