﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Profile
{
    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class CombinedProfileStatReport : ProfileStatReportBase<CombinedProfileStatParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CombinedProfileStatReport()
            : base(ReportNames.CombinedProfileStatReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<ProfileStatDto> Generate(ISession session, CombinedProfileStatParams parameters)
        {
            // Determine which sql query we should create based on the input parameters.
            String sql;
            if (parameters.BucketSize.HasValue)
            {
                sql = CreateBucketBasedQuery(parameters);
            }
            else
            {
                sql = CreateSoloQuery(parameters);
            }

            // Run the query against vertica.
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            List<ProfileStatDto> stats = new List<ProfileStatDto>();
            while (dr.Read())
            {
                ProfileStatDto stat = new ProfileStatDto(dr.GetDouble(0), (parameters.BucketSize.HasValue ? parameters.BucketSize.Value : 0.0));

                if (dr[1] != DBNull.Value)
                {
                    stat.Total = dr.GetDecimal(1).ToString();
                }
                if (dr[2] != DBNull.Value)
                {
                    stat.YValue = dr.GetUInt64(2).ToString();
                }

                stats.Add(stat);
            }

            // Should we pad out the data with default values?
            if (!parameters.IgnoreDefaultValues)
            {
                // How many gamers contributed to these values?
                ulong gamerCount = (ulong)stats.Sum(item => Decimal.Parse(item.YValue));

                // Check whether we need to pad the data out due to default values.
                ulong totalGamerCount = GetGamerCount(session, parameters);
                if (gamerCount < totalGamerCount)
                {
                    // This is default value for these stats.
                    double combinedDefaultValue = GetDefaultValueForStats(parameters.StatNames);

                    // What is the default bucket to add these to?
                    ProfileStatDto defaultBucket = stats.FirstOrDefault(item => item.BucketMin <= combinedDefaultValue && item.BucketMax >= combinedDefaultValue);
                    if (defaultBucket == null)
                    {
                        double start = (parameters.BucketSize.HasValue ? Math.Floor(combinedDefaultValue / parameters.BucketSize.Value) : 0);
                        defaultBucket = new ProfileStatDto(start, (parameters.BucketSize.HasValue ? parameters.BucketSize.Value : 0.0));
                        stats.Add(defaultBucket);
                    }

                    for (ulong i = gamerCount; i < totalGamerCount; ++i)
                    {
                        defaultBucket.AddDoubleValue(combinedDefaultValue);
                    }
                }
            }

            return stats.OrderBy(item => item.BucketMin).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Creates the query to bucket the data into the requested bucket sizes.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateBucketBasedQuery(CombinedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            // (SCARY!!!!)
            //
            // SELECT
            //     Bucket,
            //     SUM(SummedValues) AS Total,
            //     COUNT(DISTINCT(PlayerAccountId)) AS Gamers
            // FROM
            //     (
            //     SELECT
            //         SUM(Value) AS SummedValues,
            //         PlayerAccountId,
            //         FLOOR(SUM(Value) / 3600000) * 3600000 AS Bucket,
            //     FROM
            //         (
            //             SELECT 'PS3' AS Platform, PlayerAccountId,
            //                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                 CASE
            //                     WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                     WHEN FloatValue IS NOT NULL THEN FloatValue
            //                     ELSE 0.0
            //                 END AS Value
            //             FROM dev_gta5_11_ps3.ProfileStats2
            //             WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //         UNION ALL 
            //             SELECT 'Xbox360' AS Platform, PlayerAccountId,
            //                 ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //                 CASE
            //                     WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                     WHEN FloatValue IS NOT NULL THEN FloatValue
            //                     ELSE 0.0
            //                 END AS Value
            //             FROM dev_gta5_11_ps3.ProfileStats2
            //             WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //         ) AS AllStats
            //     WHERE HistoryIndex = 1
            //     GROUP BY Platform, PlayerAccountId
            //     ) AS PerGamerStats
            // GROUP BY Bucket;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("Bucket, ");
            sb.Append("SUM(SummedValues) AS Total, ");
            sb.Append("COUNT(DISTINCT(PlayerAccountId)) AS Gamers ");
            sb.Append("FROM (");
            sb.Append("SELECT ");
            sb.Append("SUM(Value) AS SummedValues, ");
            sb.Append("PlayerAccountId, ");
            sb.AppendFormat("FLOOR(SUM(Value) / {0}) * {0} AS Bucket ", parameters.BucketSize);
            sb.Append("FROM (");

            sb.Append("SELECT PlatformId, PlayerAccountId, ");
            sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
            sb.Append("COALESCE(IntegerValue, FloatValue, 0) AS Value ");
            sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
            AddRestrictions(sb, parameters, parameters.StatNames.Select(item => item.Item1), "p");

            sb.Append(") AS AllStats ");
            sb.AppendFormat("WHERE HistoryIndex = 1 AND Value <= {0} ", parameters.MaxValue);
            if (parameters.MinValue.HasValue)
            {
                sb.AppendFormat("AND Value >= {0}", parameters.MinValue);
            }
            sb.Append("GROUP BY PlatformId, PlayerAccountId ");
            sb.Append(") AS PerGamerStats ");
            sb.Append("GROUP BY Bucket;");
            return sb.ToString();
        }

        /// <summary>
        /// Creates the query to return the single value for the requested stats.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateSoloQuery(CombinedProfileStatParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     0 AS Bucket,
            //     SUM(Value) AS Total,
            //     COUNT(DISTINCT(PlayerAccountId)) AS Gamers
            // FROM
            //     (
            //         SELECT 'PS3' AS Platform, PlayerAccountId,
            //             ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //             CASE
            //                 WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                 WHEN FloatValue IS NOT NULL THEN FloatValue
            //                 ELSE 0.0
            //             END AS Value
            //         FROM dev_gta5_11_ps3.ProfileStats2
            //         WHERE LastUpdated BETWEEN '2013-06-01' AND '2013-06-15' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //     UNION ALL 
            //         SELECT 'Xbox360' AS Platform, PlayerAccountId,
            //             ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex,
            //             CASE
            //                 WHEN IntegerValue IS NOT NULL THEN IntegerValue
            //                 WHEN FloatValue IS NOT NULL THEN FloatValue
            //                 ELSE 0.0
            //             END AS Value
            //         FROM dev_gta5_11_ps3.ProfileStats2
            //         WHERE LastUpdated < '2013-06-01' AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            //     ) AS AllStats
            // WHERE HistoryIndex = 1;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("0 AS Bucket, ");
            sb.Append("SUM(Value) AS Total, ");
            sb.Append("COUNT(DISTINCT(PlayerAccountId)) AS Gamers ");
            sb.Append("FROM (");

            sb.Append("SELECT PlatformId, PlayerAccountId, ");
            sb.Append("ROW_NUMBER() OVER(PARTITION BY PlayerAccountId, StatId ORDER BY LastUpdated DESC) AS HistoryIndex, ");
            sb.Append("CASE ");
            sb.Append("WHEN IntegerValue IS NOT NULL THEN IntegerValue ");
            sb.Append("WHEN FloatValue IS NOT NULL THEN FloatValue ");
            sb.Append("ELSE 0.0 ");
            sb.Append("END AS Value ");
            sb.AppendFormat("FROM {0}.ProfileStats2 p ", Conversion.Schema());
            AddRestrictions(sb, parameters, parameters.StatNames.Select(item => item.Item1), "p");

            sb.Append(") AS AllStats ");
            sb.AppendFormat("WHERE HistoryIndex = 1 AND Value < {0};", parameters.MaxValue);
            return sb.ToString();
        }
        #endregion // Private Methods
    } // CombinedProfileStatReport
}
