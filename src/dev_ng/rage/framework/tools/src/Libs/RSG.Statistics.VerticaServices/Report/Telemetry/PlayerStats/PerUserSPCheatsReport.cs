﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides singleplayer cheat statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserSPCheatsReport : PerUserTelemetryReportBase<List<PerUserCheatStats>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserSPCheatsReport()
            : base(ReportNames.PerUserSPCheatsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserCheatStats> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserCheatStats> allStats = new List<PerUserCheatStats>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS GamerHandle,
            //     c.name AS CheatName,
            //     COUNT(c.SubmissionId) AS TimesUsed
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_SP_CHEAT c
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=c.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName, c.name;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS Username, ");
            sb.Append("c.name AS CheatName, ");
            sb.Append("COUNT(c.SubmissionId) AS TimesUsed ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_SP_CHEAT c ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=c.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "c", platform);
            sb.Append("GROUP BY pa.UserName, c.name;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserCheatStats> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserCheatStats> stats = new Dictionary<String, PerUserCheatStats>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!stats.ContainsKey(username))
                {
                    stats.Add(username, new PerUserCheatStats(username, platform));
                }

                CheatStat wlStat = new CheatStat();
                wlStat.CheatName = dr.GetString(idx++);
                wlStat.TimesUsed = dr.GetUInt32(idx++);
                stats[username].Data.Add(wlStat);
            }
            return stats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserSPCheatsReport
}
