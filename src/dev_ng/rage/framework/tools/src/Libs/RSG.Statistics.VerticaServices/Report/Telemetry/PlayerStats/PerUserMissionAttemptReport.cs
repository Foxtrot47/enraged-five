﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides per mission cash statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserMissionAttemptReport : PerUserTelemetryReportBase<List<PerUserMissionAttemptStats>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserMissionAttemptReport()
            : base(ReportNames.PerUserMissionAttemptReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserMissionAttemptStats> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserMissionAttemptStats> allStats = new List<PerUserMissionAttemptStats>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     mo.name AS MissionName,
            //     mo.r AS Result,
            //     COUNT(mo.SubmissionId) AS Attempts,
            //     SUM(mo.ts) AS TotalTime
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_MISSION_OVER mo
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=mo.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE h.ver=5296
            // GROUP BY mo.name, mo.r;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS Username, ");
            sb.Append("mo.name AS MissionName, ");
            sb.Append("mo.r AS Result, ");
            sb.Append("COUNT(mo.SubmissionId) AS Attempts, ");
            sb.Append("SUM(mo.ts) AS TotalTime ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_MISSION_OVER mo ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=mo.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "mo", platform);
            sb.Append("GROUP BY pa.UserName, mo.name, mo.r;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserMissionAttemptStats> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserMissionAttemptStats> perUserStats = new Dictionary<String, PerUserMissionAttemptStats>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserMissionAttemptStats(username, platform));
                }

                MissionAttemptStat stat = new MissionAttemptStat();
                stat.MissionName = dr.GetString(idx++);

                uint result = dr.GetUInt32(idx++);
                ulong attempts = dr.GetUInt64(idx++);
                stat.TotalAttempts += attempts;
                stat.TotalTime += dr.GetUInt64(idx++);
                if (result == 0)
                {
                    stat.TotalPassed += attempts;
                }
                else if (result == 1)
                {
                    stat.TotalCancelled += attempts;
                }
                else if (result == 2)
                {
                    stat.TotalFailed += attempts;
                }
                else
                {
                    stat.TotalUnknown += attempts;
                }
                perUserStats[username].Data.Add(stat);
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserMissionAttemptReport
}
