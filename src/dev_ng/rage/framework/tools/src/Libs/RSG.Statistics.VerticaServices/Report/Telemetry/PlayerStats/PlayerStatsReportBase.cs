﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.VerticaServices.Util;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// 
    /// </summary>
    internal abstract class PlayerStatsReportBase<TResult, TParam> : VerticaReportBase<TResult, TParam>
        where TParam : class
    {   
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        protected PlayerStatsReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        [Obsolete("Use AddEmbeddedRestrictions() instead.")]
        protected void AddRestrictions(StringBuilder sb, TelemetryParams parameters, String tableAlias, IEnumerable<String> additionalRestrictions = null)
        {
            if (RequiresPlayerAccountJoin(parameters) || RequiresRockstarAccountJoin(parameters))
            {
                AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");

                if (RequiresRockstarAccountJoin(parameters))
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            IList<String> restrictions = GetRestrictions(parameters, tableAlias).ToList();
            if (additionalRestrictions != null && additionalRestrictions.Any())
            {
                restrictions.AddRange(additionalRestrictions);
            }

            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.Append(String.Format("WHERE {0} ", String.Join(" AND ", restrictions)));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        [Obsolete("Use GetEmbeddedRestrictions() instead.")]
        protected virtual IEnumerable<String> GetRestrictions(TelemetryParams parameters, String tableAlias)
        {
            if (parameters.Platforms.Any())
            {
                yield return (String.Format("h.PlatformId IN ({0})", String.Join(",", parameters.Platforms.Select(item => (int)item))));
            }
        
            // Did the user supply start/end dates?
            if (parameters.StartDate.HasValue && parameters.EndDate.HasValue)
            {
                yield return (String.Format("h.DateTime >= '{0}'", parameters.StartDate.Value.ToString("u")));
                yield return (String.Format("h.DateTime < '{0}'", parameters.EndDate.Value.ToString("u")));

                if (tableAlias != null)
                {
                    yield return (String.Format("{0}.DateTime >= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));
                    yield return (String.Format("{0}.DateTime < '{1}'", tableAlias, parameters.EndDate.Value.ToString("u")));
                }
            }
            else if (parameters.StartDate.HasValue)
            {
                yield return (String.Format("h.DateTime >= '{0}'", parameters.StartDate.Value.ToString("u")));

                if (tableAlias != null)
                {
                    yield return (String.Format("{0}.DateTime >= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));
                }
            }
            else if (parameters.EndDate.HasValue)
            {
                yield return (String.Format("h.DateTime <= '{0}'", parameters.EndDate.Value.ToString("u")));

                if (tableAlias != null)
                {
                    yield return (String.Format("{0}.DateTime <= '{1}'", tableAlias, parameters.EndDate.Value.ToString("u")));
                }
            }

            if (parameters.CompanywidePlaytestId.HasValue)
            {
                yield return (String.Format("h.PlaytestId={0}", parameters.CompanywidePlaytestId.Value));
            }

            if (parameters.GameTypeRestriction != null)
            {
                yield return (String.Format("h.mp {0} 0", parameters.GameTypeRestriction == GameType.Multiplayer ? "!=" : "="));
            }

            if (parameters.CharacterSlot.HasValue)
            {
                yield return (String.Format("h.cid = {0}", parameters.CharacterSlot.Value));
            }

            if (parameters.Builds.Any())
            {
                yield return (String.Format("h.ver IN ({0})", String.Join(", ", parameters.Builds)));
            }

            if (parameters.CountryCodes.Any())
            {
                yield return (String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                yield return (String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void AddEmbeddedRestrictions(
            StringBuilder sb,
            TelemetryParams parameters,
            String tableAlias,
            IEnumerable<String> additionalRestrictions = null)
        {
            System.Diagnostics.Debug.Assert(tableAlias != null, "Table alias is null.");

            // Check whether which tables we need to join.
            if (RequiresTelemetryHeaderJoin(parameters))
            {
                AddTelemetryHeaderJoin(sb, tableAlias);
            }
            if (RequiresPlayerAccountJoin(parameters) || RequiresRockstarAccountJoin(parameters))
            {
                // MET: HACKY! Need to sort out.
                String playerAccountColumn = String.Format("{0}.Header_AccountId", tableAlias);
                String platformColumn = String.Format("{0}.PlatformId", tableAlias);
                if (tableAlias == "h")
                {
                    playerAccountColumn = "h.AccountId";
                }
                AddPlayerAccountJoin(sb, platformColumn, playerAccountColumn);
            }
            if (RequiresRockstarAccountJoin(parameters))
            {
                AddRockstarAccountJoin(sb);
            }

            // Get the list of all restrictions.
            IList<String> restrictions = GetEmbeddedRestrictions(parameters, tableAlias).ToList();
            if (additionalRestrictions != null && additionalRestrictions.Any())
            {
                restrictions.AddRange(additionalRestrictions);
            }

            // Check if there are any restrictions to add to the query.
            if (restrictions.Any())
            {
                sb.AppendFormat("WHERE {0} ", String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Returns whether we require a header table join.
        /// </summary>
        protected virtual bool RequiresTelemetryHeaderJoin(TelemetryParams parameters)
        {
            return (parameters.CompanywidePlaytestId.HasValue || parameters.Builds.Any());
        }

        /// <summary>
        /// Returns whether we need to join against the player account table.
        /// </summary>
        protected virtual bool RequiresPlayerAccountJoin(TelemetryParams parameters)
        {
            return parameters.GamerHandlePlatformPairs.Any();
        }

        /// <summary>
        /// Returns whether we need to join against the rockstar account table.
        /// </summary>
        protected virtual bool RequiresRockstarAccountJoin(TelemetryParams parameters)
        {
            return (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                    parameters.SocialClubUsernames.Any());
        }

        /// <summary>
        /// Joins against the telemetry header table.
        /// </summary>
        protected virtual void AddTelemetryHeaderJoin(StringBuilder sb, String tableAlias)
        {
            sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId={1}.SubmissionId ",
                Conversion.Schema(), tableAlias);
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        protected virtual IEnumerable<String> GetEmbeddedRestrictions(TelemetryParams parameters, String tableAlias)
        {
            if (parameters.Platforms.Any())
            {
                yield return (String.Format("{0}.PlatformId IN ({1})", tableAlias, String.Join(",", parameters.Platforms.Select(item => (int)item))));
            }
        
            // Did the user supply start/end dates?
            if (parameters.StartDate.HasValue && parameters.EndDate.HasValue)
            {
                yield return (String.Format("{0}.DateTime >= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));
                yield return (String.Format("{0}.DateTime < '{1}'", tableAlias, parameters.EndDate.Value.ToString("u")));
            }
            else if (parameters.StartDate.HasValue)
            {
                yield return (String.Format("{0}.DateTime >= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));
            }
            else if (parameters.EndDate.HasValue)
            {
                yield return (String.Format("{0}.DateTime <= '{1}'", tableAlias, parameters.EndDate.Value.ToString("u")));
            }

            if (parameters.CompanywidePlaytestId.HasValue)
            {
                yield return (String.Format("h.PlaytestId={0}", parameters.CompanywidePlaytestId.Value));
            }

            if (parameters.GameTypeRestriction != null)
            {
                // HACKY :/
                if (tableAlias == "h")
                {
                    yield return (String.Format("{0}.mp {1} 0", tableAlias, parameters.GameTypeRestriction == GameType.Multiplayer ? "!=" : "="));
                }
                else
                {
                    yield return (String.Format("{0}.Header_mp {1} 0", tableAlias, parameters.GameTypeRestriction == GameType.Multiplayer ? "!=" : "="));
                }
            }

            if (parameters.CharacterSlot.HasValue)
            {
                // HACKY :/
                if (tableAlias == "h")
                {
                    yield return (String.Format("{0}.cid = {1}", tableAlias, parameters.CharacterSlot.Value));
                }
                else
                {
                    yield return (String.Format("{0}.Header_cid = {1}", tableAlias, parameters.CharacterSlot.Value));
                }
            }

            if (parameters.Builds.Any())
            {
                yield return (String.Format("h.ver IN ({0})", String.Join(", ", parameters.Builds)));
            }

            if (parameters.CountryCodes.Any())
            {
                yield return (String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                yield return (String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }
        #endregion // Protected Methods
    } // PlayerStatsReportBase
}
