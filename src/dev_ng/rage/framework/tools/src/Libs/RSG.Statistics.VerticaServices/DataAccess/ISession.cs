﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.DataAccess
{
    /// <summary>
    /// Database session against which to execute queries.
    /// </summary>
    internal interface ISession
    {
        /// <summary>
        /// Executes the query and returns all results returned by the query.
        /// </summary>
        IEnumerable<T> RetrieveAll<T>(IQuery query) where T : new();

        /// <summary>
        /// Executes the query and returns the first row in the result set returned by the query.
        /// All other rows are ignored.
        /// </summary>
        T RetrieveSingleRow<T>(IQuery query) where T : new();

        /// <summary>
        /// Executes the query and returns the first column of the first row in the result set returned
        /// by the query.  All other columns/rows are ignored.
        /// </summary>
        T RetrieveScalar<T>(IQuery query);

        /// <summary>
        /// Executes a string query.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [Obsolete("Here for legacy purposes.  Don't use it anymore as it will be removed.")]
        VerticaDataReader ExecuteQuery(String query, CommandBehavior behaviour = CommandBehavior.Default);
    } // ISession
}
