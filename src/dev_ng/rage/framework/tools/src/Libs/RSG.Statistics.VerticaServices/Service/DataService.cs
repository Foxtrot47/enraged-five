﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Model.SocialClub;
using RSG.Statistics.Common.Model.VerticaData;
using RSG.Statistics.Common.Vertica.Dto;
using RSG.Statistics.Common.Vertica.ServiceContract;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Service
{
    /// <summary>
    /// Service for retrieving build information from Vertica.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class DataService : ServiceBase, IDataService
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public DataService(IVerticaServer server)
            : base(server)
        {
        }
        #endregion // Constructor(s)

        #region Builds
        /// <summary>
        /// Returns feature information about all the builds.
        /// </summary>
        public List<BuildFeatures> GetAllBuildFeatures()
        {
            return ExecuteCommand(session => GetAllBuildFeaturesCommand(session));
        }

        /// <summary>
        /// 
        /// </summary>
        private List<BuildFeatures> GetAllBuildFeaturesCommand(ISession session)
        {
            // Get the list of builds and create a features object for each of them.
            IEnumerable<uint> buildList = GetBuildList(session);
            IDictionary<uint, BuildFeatures> buildFeatures =
                buildList.Select(item => new BuildFeatures { BuildVersion = item }).ToDictionary(item => item.BuildVersion);

            // Get the list of builds that have certain automated stats associated with them.
            foreach (uint build in GetBuildsWithAutomatedTestNumber(session, 1))
            {
                buildFeatures[build].HasMapOnlyStats = true;
            }

            foreach (uint build in GetBuildsWithAutomatedTestNumber(session, 2))
            {
                buildFeatures[build].HasEverythingStats = true;
            }

            foreach (uint build in GetBuildsWithAutomatedTestNumber(session, 6))
            {
                buildFeatures[build].HasNightTimeMapOnlyStats = true;
            }

            foreach (uint build in GetBuildsWithMemShortfallStats(session))
            {
                buildFeatures[build].HasMemShortfallStats = true;
            }

            foreach (uint build in GetBuildsWithPhysicsShapecostStats(session))
            {
                buildFeatures[build].HasPhysicsShapeCostStats = true;
            }

            return buildFeatures.Values.ToList();
        }

        /// <summary>
        /// Retrieves the list of builds that vertica is aware of.
        /// </summary>
        private IEnumerable<uint> GetBuildList(ISession session)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT DISTINCT ver FROM ( ");

            bool first = true;
            foreach (ROSPlatform platform in ROSPlatformUtils.GetValidPlatforms())
            {
                if (!first)
                {
                    sb.Append("UNION ALL ");
                }
                else
                {
                    first = false;
                }

                sb.AppendFormat("SELECT DISTINCT ver FROM {0}.Telemetry_Gta5_header WHERE ver IS NOT NULL ",
                    Conversion.Schema());
            }
            sb.Append(") AS AllStats;");

            // Run the query
            ISet<uint> builds = new HashSet<uint>();
            VerticaDataReader dr = session.ExecuteQuery(sb.ToString());
            while (dr.Read())
            {
                builds.Add(dr.GetUInt32(0));
            }
            return builds;
        }

        /// <summary>
        /// Gets the list of builds with a particular DRAW_LIST automated test number.
        /// </summary>
        private IEnumerable<uint> GetBuildsWithAutomatedTestNumber(ISession session, uint testNum)
        {
            ISet<uint> builds = new HashSet<uint>();

            String query = String.Format(
                @"SELECT DISTINCT h.ver
                    FROM {0}.Telemetry_Gta5Debug_DRAW_LISTS dl
                    INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=dl.SubmissionId
                    WHERE h.ver IS NOT NULL AND dl.au={1};", Conversion.Schema(), testNum);
            VerticaDataReader dr = session.ExecuteQuery(query);
            while (dr.Read())
            {
                builds.Add(dr.GetUInt32(0));
            }

            return builds;
        }

        /// <summary>
        /// Gets the list of builds with HEATMAP stats associated with it.
        /// </summary>
        private IEnumerable<uint> GetBuildsWithMemShortfallStats(ISession session)
        {
            ISet<uint> builds = new HashSet<uint>();

            String query = String.Format(
                @"SELECT DISTINCT h.ver
                    FROM {0}.Telemetry_Gta5Debug_HEATMAP hm
                    INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=hm.SubmissionId
                    WHERE h.ver IS NOT NULL;",
                Conversion.Schema());

            VerticaDataReader dr = session.ExecuteQuery(query);
            while (dr.Read())
            {
                builds.Add(dr.GetUInt32(0));
            }

            return builds;
        }

        /// <summary>
        /// Gets the list of builds with SHAPETEST_COST stats associated with it.
        /// </summary>
        private IEnumerable<uint> GetBuildsWithPhysicsShapecostStats(ISession session)
        {
            ISet<uint> builds = new HashSet<uint>();

            String query = String.Format(
                @"SELECT DISTINCT h.ver
                    FROM {0}.Telemetry_Gta5Debug_SHAPETEST_COST stc
                    INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=stc.SubmissionId
                    WHERE h.ver IS NOT NULL;",
                Conversion.Schema());

            VerticaDataReader dr = session.ExecuteQuery(query);
            while (dr.Read())
            {
                builds.Add(dr.GetUInt32(0));
            }

            return builds;
        }
        #endregion // Builds

        #region Clans
        /// <summary>
        /// Searches for a clan based on a partial name.
        /// </summary>
        public List<Clan> SearchForClan(String searchName)
        {
            return ExecuteCommand(session => SearchForClanCommand(session, searchName));
        }

        /// <summary>
        /// As called by SearchForClan
        /// </summary>
        private List<Clan> SearchForClanCommand(ISession session, String searchName)
        {
            // Query:
            //
            // SELECT
            //     *
            // FROM
            // (
            //     SELECT
            //         *, ROW_NUMBER() OVER(PARTITION BY Id ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.ClanDesc
            // ) Clans
            // WHERE HistoryIndex=1 AND Name ILIKE '%tools%';
            String query = String.Format("SELECT Name, Id FROM " +
                "(SELECT Name, Id, ROW_NUMBER() OVER(PARTITION BY Id ORDER BY LastUpdatedDate DESC) AS HistoryIndex FROM {0}.ClanDesc) Clans " +
                "WHERE HistoryIndex=1 AND Name ILIKE '%{1}%';",
                Conversion.SocialClubSchema(), EscapeUnderscores(searchName));
            return session.RetrieveAll<Clan>(new StringQuery(query)).ToList();
        }

        /// <summary>
        /// Returns the list of gamers that are members of a particular clan.
        /// </summary>
        public List<Gamer> GetClanMembers(String id)
        {
            return ExecuteCommand(session => GetClanMembersCommand(session, UInt64.Parse(id)));
        }

        /// <summary>
        /// As called by GetClanMembers
        /// </summary>
        private List<Gamer> GetClanMembersCommand(ISession session, ulong id)
        {
            // Query ALA:
            //
            // SELECT
            //     UserName AS GamerHandle, PlatformId AS Platform, AccountId AS Id
            // FROM
            // (
	        //     SELECT RockstarId, ROW_NUMBER() OVER(PARTITION BY Id ORDER BY LastUpdatedDate DESC) AS HistoryIndex
	        //     FROM SocialClub.ClanMembers
	        //     WHERE ClanId=7418
            // ) ClanMembers
            // INNER JOIN
            // (
	        //     SELECT RockstarId, UserName, PlatformId, AccountId, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex
	        //     FROM SocialClub.PlayerAccount
            // ) PlayerAccounts ON PlayerAccounts.HistoryIndex=1 AND PlayerAccounts.RockstarId=ClanMembers.RockstarId
            // WHERE ClanMembers.HistoryIndex=1;
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT UserName AS GamerHandle, PlatformId AS Platform, AccountId AS Id ");
            sb.Append("FROM (");
            sb.Append("SELECT RockstarId, ROW_NUMBER() OVER(PARTITION BY Id ORDER BY LastUpdatedDate DESC) AS HistoryIndex ");
            sb.Append("FROM SocialClub.ClanMembers ");
            sb.AppendFormat("WHERE ClanId={0} ", id);
            sb.Append(") ClanMembers ");
            sb.Append("INNER JOIN (");
            sb.Append("SELECT RockstarId, UserName, PlatformId, AccountId, ");
            sb.Append("ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex ");
            sb.Append("FROM SocialClub.PlayerAccount ");
            sb.Append(") PlayerAccounts ON PlayerAccounts.HistoryIndex=1 AND PlayerAccounts.RockstarId=ClanMembers.RockstarId ");
            sb.Append("WHERE ClanMembers.HistoryIndex=1;");

            // Execute the query and return the results.
            return session.RetrieveAll<Gamer>(new StringQuery(sb.ToString())).ToList();
        }
        #endregion // Clans

        #region Gamers
        /// <summary>
        /// Searches for a user with a particular name and optionally restricting the search to a particular platform.
        /// </summary>
        public List<Gamer> SearchForGamer(String searchName, IList<ROSPlatform> platforms)
        {
            return ExecuteCommand(session => SearchForGamerCommand(session, searchName, platforms));
        }

        /// <summary>
        /// As called by SearchForGamer
        /// </summary>
        private List<Gamer> SearchForGamerCommand(ISession session, String searchName, IList<ROSPlatform> platforms)
        {
            // Query:
            //
            // SELECT UserName FROM SocialClub_dev.PlayerAccount WHERE UserName LIKE '%Bob%' AND PlatformId IN (1, 2);
            //
            String query = String.Format("SELECT UserName AS GamerHandle, UserId, PlatformId AS Platform, AccountId AS Id FROM " +
                "(SELECT UserName, UserId, PlatformId, AccountId, " + 
                "ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY ISNULL(LastUpdatedDate, CreatedDate) DESC) AS HistoryIndex " +
                "FROM {0}.PlayerAccount WHERE PlatformId IN ({1})) Players " +
                "WHERE HistoryIndex=1 AND UserName ILIKE '%{2}%';",
                Conversion.SocialClubSchema(), String.Join(", ", platforms.Select(item => item.PlatformToRosId())), EscapeUnderscores(searchName));
            return session.RetrieveAll<Gamer>(new StringQuery(query)).ToList();
        }

        /// <summary>
        /// Checks that the list of gamers passed in exist in the Vertica DB.  
        /// </summary>
        /// <returns>List of valid gamers.</returns>
        public List<Gamer> ValidateGamerList(List<Gamer> gamers)
        {
            return ExecuteCommand(session => ValidateGamerListCommand(session, gamers));
        }

        /// <summary>
        /// As called by ValidateGamerList.
        /// </summary>
        private List<Gamer> ValidateGamerListCommand(ISession session, List<Gamer> gamers)
        {
            List<Gamer> validGamers = new List<Gamer>();

            foreach (IGrouping<ROSPlatform, Gamer> platformGroup in gamers.GroupBy(item => item.Platform))
            {
                String query = String.Format("SELECT DISTINCT UserName FROM {0}.PlayerAccount " + 
                    "WHERE LOWER(UserName) IN ('{1}') AND PlatformId = {2};",
                    Conversion.SocialClubSchema(),
                    String.Join("', '", platformGroup.Select(item => item.GamerHandle.ToLower())), platformGroup.Key.PlatformToRosId());

                VerticaDataReader dr = session.ExecuteQuery(query);
                while (dr.Read())
                {
                    validGamers.Add(new Gamer
                        {
                            GamerHandle = dr.GetString(0),
                            Platform = platformGroup.Key
                        });
                }
            }

            return validGamers;
        }
        #endregion // Gamers

        #region Social Club
        /// <summary>
        /// Searches for a social club user with a particular name.
        /// </summary>
        public List<String> SearchForUser(String searchName)
        {
            return ExecuteCommand(session => SearchForUserCommand(session, searchName));
        }

        /// <summary>
        /// As called by SearchForUser
        /// </summary>
        private List<String> SearchForUserCommand(ISession session, String searchName)
        {
            // Query:
            //
            // SELECT Nickname FROM SocialClub_dev.RockstarAccount WHERE Nickname LIKE '%Bob%';
            //
            List<String> users = new List<String>();

            String query = String.Format("SELECT DISTINCT Nickname FROM {0}.RockstarAccount WHERE Nickname ILIKE '%{1}%';",
                Conversion.SocialClubSchema(), EscapeUnderscores(searchName));

            VerticaDataReader dr = session.ExecuteQuery(query);
            while (dr.Read())
            {
                users.Add(dr.GetString(0));
            }

            return users;
        }

        /// <summary>
        /// Checks that the list of gamers passed in exist in the Vertica DB.  
        /// </summary>
        /// <returns>List of valid users.</returns>
        public List<String> ValidateUserList(List<String> users)
        {
            return ExecuteCommand(session => ValidateUserListCommand(session, users));
        }

        /// <summary>
        /// As called by ValidateGamerList.
        /// </summary>
        private List<String> ValidateUserListCommand(ISession session, List<String> users)
        {
            List<String> validUsers = new List<String>();

            String query = String.Format("SELECT DISTINCT Nickname FROM {0}.RockstarAccount WHERE Nickname IN ('{1}');",
                Conversion.SocialClubSchema(), String.Join("', '", users));

            VerticaDataReader dr = session.ExecuteQuery(query);
            while (dr.Read())
            {
                users.Add(dr.GetString(0));
            }

            return validUsers;
        }

        /// <summary>
        /// Searches for a UGC mission with a particular name.
        /// </summary>
        public List<UGCMission> SearchForMission(String searchName, MissionSearchFilters filters)
        {
            return ExecuteCommand(session => SearchForMissionCommand(session, searchName, filters));
        }

        /// <summary>
        /// As called by SearchForMission
        /// </summary>
        private List<UGCMission> SearchForMissionCommand(ISession session, String searchName, MissionSearchFilters filters)
        {
            return session.RetrieveAll<UGCMission>(new StringQuery(CreateMissionSearchQueryString(searchName, filters))).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreateMissionSearchQueryString(String searchName, MissionSearchFilters filters)
        {
            // Query:
            //
            // SELECT
            //     AllUGC.PublicContentId, AllUGC.ContentName, AllUGC.PlatformId, pa.UserName, AllUGC.CreatedDate, AllUGC.Version
            // FROM
            // (
            //     SELECT PublicContentId, ContentName, PlatformId, UserId, CreatedDate, Version, ROW_NUMBER() OVER(PARTITION BY PublicContentId ORDER BY UpdatedDate DESC) AS HistoryIndex
            //     FROM ugc.gta5mission_metadata
            //     WHERE ContentName ILIKE '%grave%'
            // ) AS AllUGC
            // LEFT JOIN
            // (
            //     SELECT UserId, UserName, ROW_NUMBER() OVER(PARTITION BY AccountId ORDER BY LastUpdatedDate DESC) AS HistoryIndex
            //     FROM SocialClub.PlayerAccount
            // ) pa ON AllUGC.UserId=pa.UserId
            // WHERE AllUGC.HistoryIndex=1 AND pa.HistoryIndex=1;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT AllUGC.PublicContentId AS Identifier,");
            sb.Append("AllUGC.ContentName AS Name, ");
            sb.Append("AllUGC.PlatformId AS CreatorPlatform, ");
            sb.Append("pa.UserName AS Creator, ");
            sb.Append("AllUGC.CreatedDate AS CreatedDate, ");
            sb.Append("AllUGC.Version AS Version ");
            sb.Append("FROM ( ");
            sb.Append("SELECT PublicContentId, ContentName, PlatformId, UserId, CreatedDate, Version, ROW_NUMBER() OVER(PARTITION BY PublicContentId ORDER BY UpdatedDate DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM {0}.gta5mission_metadata ", Conversion.UGCSchema());
            sb.AppendFormat("WHERE ContentName ILIKE '%{0}%' ", EscapeUnderscores(searchName));
            if (filters.MatchType != null)
            {
                sb.AppendFormat("AND Data_Gen_Type={0}", (int)filters.MatchType);
            }
            if (filters.MissionCategory != null)
            {
                sb.AppendFormat("AND CategoryId={0}", (int)filters.MissionCategory);
            }
            if (filters.PublishedOnly != null)
            {
                sb.AppendFormat("AND PublishedDate {0}", filters.PublishedOnly.Value ? "IS NOT NULL" : "IS NULL");
            }
            sb.Append(") AS AllUGC ");
            sb.Append("INNER JOIN ( ");
            sb.Append("SELECT UserId, UserName, PlatformId, ROW_NUMBER() OVER(PARTITION BY AccountId, PlatformId ORDER BY LastUpdatedDate DESC) AS HistoryIndex ");
            sb.AppendFormat("FROM {0}.PlayerAccount ", Conversion.SocialClubSchema());
            sb.Append(") pa ON AllUGC.UserId=pa.UserId AND AllUGC.HistoryIndex=1 AND pa.HistoryIndex=1;");

            return sb.ToString();
        }

        /// <summary>
        /// Retrieves the list of countries.
        /// </summary>
        public List<Country> GetCountries()
        {
            return ExecuteCommand(session => GetCountriesCommand(session));
            
        }

        /// <summary>
        /// As called by GetCountries.
        /// </summary>
        private List<Country> GetCountriesCommand(ISession session)
        {
            // Query:
            //
            // SELECT Name, Code FROM SocialClub_dev.Country;
            //
            String query = String.Format("SELECT Name, Code FROM {0}.Country", Conversion.SocialClubSchema());

            List<Country> countries = new List<Country>();

            VerticaDataReader dr = session.ExecuteQuery(query);
            while (dr.Read())
            {
                Country country = new Country
                    {
                        Name = dr.GetString(0),
                        Code = dr.GetString(1)
                    };
                countries.Add(country);
            }

            return countries;
        }
        #endregion // Social Club

        #region Util Methods
        /// <summary>
        /// Escapes underscores that are used whe
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private String EscapeUnderscores(String value)
        {
            return value.Replace("_", "\\_");
        }
        #endregion // Util Methods
    } // BuildService
}
