﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry.PerUser;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that returns the number of times users have changed prop items.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class PerUserPropChangesReport : PerUserTelemetryReportBase<List<PerUserPropChangeStat>, PerUserTelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PerUserPropChangesReport()
            : base(ReportNames.PerUserPropChangesReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<PerUserPropChangeStat> Generate(ISession session, PerUserTelemetryParams parameters)
        {
            List<PerUserPropChangeStat> allStats = new List<PerUserPropChangeStat>();

            foreach (ROSPlatform platform in parameters.GamerHandlePlatformPairs.Select(item => item.Item2).Distinct())
            {
                String dbQueryString = CreateSqlQueryString(platform, parameters);
                allStats.AddRange(RetrievePerUserData(session, dbQueryString, platform));
            }

            return allStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateSqlQueryString(ROSPlatform platform, PerUserTelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     pa.UserName AS GamerHandle,
            //     COUNT(pc.SubmissionId) AS Changes
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_PROP_CHANGE pc
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=pc.SubmissionId
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa on pa.AccountId=h.AccountId
            // WHERE pa.UserName IN ('MrT_RSN')
            // GROUP BY pa.UserName;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("pa.UserName AS GamerHandle, ");
            sb.Append("COUNT(pc.SubmissionId) AS TimesAchieved ");
            sb.Append(String.Format("FROM {0}.Telemetry_Gta5_PROP_CHANGE pc ", schema));
            sb.Append(String.Format("INNER JOIN {0}.Telemetry_Gta5_header h on h.SubmissionId=pc.SubmissionId ", schema));
            AddPlayerAccountJoin(sb, "h.PlatformId", "h.AccountId");
            AddRestrictions(sb, parameters, "pc", platform);
            sb.Append("GROUP BY pa.UserName;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private IList<PerUserPropChangeStat> RetrievePerUserData(ISession session, String sql, ROSPlatform platform)
        {
            VerticaDataReader dr = session.ExecuteQuery(sql);

            // Convert the results to a nicer temporary format.
            IDictionary<String, PerUserPropChangeStat> perUserStats = new Dictionary<String, PerUserPropChangeStat>();

            while (dr.Read())
            {
                int idx = 0;
                String username = dr.GetString(idx++);

                if (!perUserStats.ContainsKey(username))
                {
                    perUserStats.Add(username, new PerUserPropChangeStat(username, platform));
                }

                perUserStats[username].Data = dr.GetUInt64(idx++);
            }
            return perUserStats.Values.ToList();
        }
        #endregion // Private Methods
    } // PerUserPropChangesReport
}
