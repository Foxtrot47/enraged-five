﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.VerticaServices.TransManager
{
    /// <summary>
    /// Interface for the transaction manager factory.
    /// </summary>
    internal interface ITransManagerFactory
    {
        /// <summary>
        /// Simply creates a new transaction manager.
        /// </summary>
        /// <returns></returns>
        ITransManager CreateManager(IVerticaServer server, bool readOnly);
    } // ITransManagerFactory
}
