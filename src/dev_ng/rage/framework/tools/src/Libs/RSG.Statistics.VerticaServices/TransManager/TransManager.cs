﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Async;
using RSG.Statistics.Common.Config;
using RSG.Base.Tasks;
using RSG.Base.Logging;
using System.ServiceModel.Web;
using Vertica.Data.VerticaClient;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.TransManager
{
    /// <summary>
    /// Transaction manager to use when communicating with a vertica database.
    /// </summary>
    internal class TransManager : ITransManager
    {
        #region Member Data
        /// <summary>
        /// Vertica db connection object.
        /// </summary>
        private VerticaConnection Connection;

        /// <summary>
        /// Db session.
        /// </summary>
        private ISession Session;
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Flag indicating whether or not we are already in a transaction.
        /// </summary>
        protected bool IsInTranx { get; set; }

        /// <summary>
        /// Vertica transaction that we are currently in.
        /// </summary>
        protected VerticaTransaction Transaction { get; set; }

        /// <summary>
        /// Async task that is currently being executed.
        /// </summary>
        protected ITask AsyncTask { get; set; }

        /// <summary>
        /// Reference to the async task results.
        /// </summary>
        public IAsyncCommandResult AsyncTaskResults { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TransManager(IVerticaServer server)
            : base()
        {
            try
            {
                // Construct the connection string
                VerticaConnectionStringBuilder builder = new VerticaConnectionStringBuilder();
                builder.Host = server.DatabaseLocation;
                builder.Database = server.DatabaseName;
                builder.User = server.DatabaseUsername;
                builder.Password = server.DatabasePassword;
                builder.MaxPoolSize = 100;
                builder.ReadOnly = true;

                // Create the session object.
                Connection = new VerticaConnection(builder.ToString());
                Connection.Open();
                Connection.InfoMessage += Connection_InfoMessage;

                // Make sure the db is running in the correct timezone.
                VerticaCommand command = Connection.CreateCommand();
                command.CommandText = "SET TIMEZONE TO 'GMT';";
                command.ExecuteNonQuery();

                // Create a new session object.
                Session = new VerticaSession(Connection, server.CommandTimeout);
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "");
            }
        }
        #endregion // Constructor(s)

        #region ITransManager Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public void ExecuteCommand(Action<ISession> command)
        {
            try
            {
                BeginTransaction();
                command.Invoke(Session);
                CommitTransaction();
            }
            catch (WebFaultException<string> e)
            {
                Log.Log__Error("Caught web fault exception with status code: {0}", e.StatusCode);
                Rollback();
                throw;
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unexpected exception occurred while executing a command.");
                Rollback();
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public T ExecuteCommand<T>(Func<ISession, T> command)
        {
            try
            {
                BeginTransaction();
                T result = command.Invoke(Session);
                CommitTransaction();
                return result;
            }
            catch (WebFaultException<string> e)
            {
                Log.Log__Error("Caught web fault exception with status code: {0}", e.StatusCode);
                Rollback();
                throw;
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unexpected exception occurred while executing a command.");
                Rollback();
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        public IAsyncCommandResult ExecuteAsyncCommand<T>(Func<ISession, IAsyncCommandResult, T> command)
        {
            // Create the structure that will keep track of the task's progress.
            AsyncTaskResults = new AsyncCommandResult(Guid.NewGuid());

            // Create the task that will run
            AsyncTask = new ActionTask("",
                (ctx, progress) =>
                {
                    try
                    {
                        BeginTransaction();
                        T result = command.Invoke(Session, AsyncTaskResults);
                        CommitTransaction();

                        // Set the result after the db transaction has been committed.
                        AsyncTaskResults.Result = result;
                    }
                    catch (WebFaultException<string> e)
                    {
                        Log.Log__Error("Caught web fault exception with status code: {0}", e.StatusCode);
                        Rollback();
                        AsyncTaskResults.Exception = e.ToString();
                    }
                    catch (Exception e)
                    {
                        Log.Log__Exception(e, "Unexpected exception occurred while executing a command.");
                        Rollback();
                        AsyncTaskResults.Exception = e.ToString();
                    }
                    finally
                    {
                        AsyncTaskResults.Message = null;
                        AsyncTaskResults.Progress = 1.0;
                    }
                });

            // Finally execute the task asynchronously
            AsyncTask.ExecuteAsync(new TaskContext());
            return AsyncTaskResults;
        }
        #endregion // ITransManager Implementation

        #region Static Methods
        /// <summary>
        /// 
        /// </summary>
        private static void Connection_InfoMessage(object sender, VerticaInfoMessageEventArgs e)
        {
            Log.Log__Message("{0}: {1}", e.SqlState, e.Message);
        }
        #endregion // Static Methods

        #region Private Methods
        /// <summary>
        /// Starts a new transaction.
        /// </summary>
        private void BeginTransaction()
        {
            IsInTranx = true;
            if (Transaction == null)
            {
                Transaction = Connection.BeginTransaction();
            }
        }

        /// <summary>
        /// Commits any database changes that were made since the transaction was started.
        /// </summary>
        private void CommitTransaction()
        {
            IsInTranx = false;
            if (Transaction != null)
            {
                Transaction.Commit();
                Transaction.Dispose();
                Transaction = null;
            }
        }

        /// <summary>
        /// Rolls back any changes that have been made to the database since the transaction was started.
        /// </summary>
        private void Rollback()
        {
            IsInTranx = false;
            if (Transaction != null)
            {
                Transaction.Rollback();
                Transaction.Dispose();
                Transaction = null;
            }
        }
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// 
        /// </summary>
        protected bool IsDisposed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected void Dispose(bool disposing)
        {
            if (!disposing)
            {
                return;
            }

            // free managed resources
            if (!IsDisposed && IsInTranx)
            {
                Rollback();
            }
            Close();
            Transaction = null;
            Session = null;
            Connection = null;
            IsDisposed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void Close()
        {
            if (Connection != null)
            {
                Connection.InfoMessage -= Connection_InfoMessage;
                Connection.Close();
            }
        }
        #endregion // IDisposable Implementation
    }
}
