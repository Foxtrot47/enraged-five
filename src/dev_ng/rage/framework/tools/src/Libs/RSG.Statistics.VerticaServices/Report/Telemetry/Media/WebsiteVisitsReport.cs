﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides website visit statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class WebsiteVisitsReport : PlayerStatsReportBase<List<MediaStat>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WebsiteVisitsReport()
            : base(ReportNames.WebsiteVisitsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<MediaStat> Generate(ISession session, TelemetryParams parameters)
        {
            return session.RetrieveAll<MediaStat>(new StringQuery(CreateQuery(parameters))).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateQuery(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     wv.id AS Hash,
            //     COUNT(wv.SubmissionId) AS Count,
            //     SUM(wv.Duration) / 1000 AS Duration,
            //     COUNT(DISTINCT h.AccountId) AS UniqueGamers
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_WEBSITE_VISITED wv
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=wv.SubmissionId
            // WHERE h.ver=5296
            // GROUP BY wv.id;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("SELECT ");
            sb.AppendFormat("Hash, ");
            sb.AppendFormat("SUM(Count) AS Count, ");
            sb.AppendFormat("IFNULL(SUM(Duration), 0) AS Duration, ");
            sb.AppendFormat("SUM(UniqueGamers) AS UniqueGamers ");
            sb.AppendFormat("FROM (");
            {
                sb.Append("SELECT ");
                sb.Append("wv.id AS Hash, ");
                sb.Append("COUNT(wv.SubmissionId) AS Count, ");
                sb.Append("SUM(wv.Duration) / 1000 AS Duration, ");
                sb.Append("COUNT(DISTINCT h.AccountId) AS UniqueGamers ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_WEBSITE_VISITED wv ", Conversion.Schema());
                sb.AppendFormat("INNER JOIN {0}.Telemetry_Gta5_header h ON h.SubmissionId=wv.SubmissionId ", Conversion.Schema());
                AddRestrictions(sb, parameters, "wv", new String[] { "wv.id != 0" });
                sb.AppendFormat("GROUP BY wv.id ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY Hash;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // WebsiteVisitsReport
}
