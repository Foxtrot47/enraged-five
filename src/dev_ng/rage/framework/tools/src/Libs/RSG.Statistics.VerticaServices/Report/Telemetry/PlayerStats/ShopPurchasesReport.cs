﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Security.Cryptography;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Dto.ReportResults;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides cutscene view statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class ShopPurchasesReport : PlayerStatsReportBase<List<ShopStoreStat>, ShopPurchaseParams>
    {
        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        private class ShopPurchaseResult
        {
            public uint ShopHash { get; set; }
            public uint ItemHash { get; set; }
            public long? ColorId { get; set; }
            public ulong TimesPurchased { get; set; }
            public ulong UniqueGamers { get; set; }
            public ulong TotalSpent { get; set; }
        } // ShopPurchaseResult
        #endregion // Private Classes

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ShopPurchasesReport()
            : base(ReportNames.ShopPurchasesReport)
        {
        }
        #endregion // Constructor(s)
        
        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<ShopStoreStat> Generate(ISession session, ShopPurchaseParams parameters)
        {
            List<ShopStoreStat> storeStats = session.RetrieveAll<ShopStoreStat>(new StringQuery(CreateQuery(parameters, false, false))).ToList();
            IDictionary<uint, ShopStoreStat> storeStatLookup = storeStats.ToDictionary(item => item.ShopHash);

            foreach (ShopItemStat itemStat in session.RetrieveAll<ShopItemStat>(new StringQuery(CreateQuery(parameters, true, true))))
            {
                ShopStoreStat storeStat;
                if (storeStatLookup.TryGetValue(itemStat.ShopHash, out storeStat))
                {
                    storeStat.Items.Add(itemStat);
                }
            }

            foreach (ShopItemStat itemStat in session.RetrieveAll<ShopItemStat>(new StringQuery(CreatePaintQuery(parameters))))
            {
                ShopStoreStat storeStat;
                if (storeStatLookup.TryGetValue(itemStat.ShopHash, out storeStat))
                {
                    storeStat.Items.Add(itemStat);
                }
            }

            return storeStats;
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateQuery(ShopPurchaseParams parameters, bool groupByItem, bool excludePaint)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     s.id AS ItemHash,
            //     COUNT(s.SubmissionId) AS TimesPurchased,
            //     COUNT(DISTINCT h.AccountId) AS NumGamers,
            //     SUM(s.as) AS TotalSpent
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_SHOP s
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=s.SubmissionId
            // WHERE h.ver=5504
            // GROUP BY s.id;
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            if (groupByItem)
            {
                sb.Append("ItemHash, ");
            }
            sb.Append("ShopHash, ");
            sb.Append("SUM(TimesPurchased) AS TimesPurchased, ");
            sb.Append("SUM(UniqueGamers) AS UniqueGamers, ");
            sb.Append("SUM(TotalSpent) AS TotalSpent ");
            sb.Append("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                if (groupByItem)
                {
                    sb.Append("ItemHash, ");
                }
                AddShopCaseStatement(sb, parameters);
                sb.Append("COUNT(*) AS TimesPurchased, ");
                sb.Append("COUNT(DISTINCT Header_AccountId) AS UniqueGamers, ");
                sb.Append("SUM(Amount) AS TotalSpent ");
                sb.Append("FROM (");
                bool requiresUnion = false;
                if (parameters.GameTypeRestriction == null || parameters.GameTypeRestriction == GameType.Singleplayer)
                {
                    sb.Append("SELECT ");
                    if (groupByItem)
                    {
                        sb.Append("s.id AS ItemHash, ");
                    }
                    sb.Append("s.shop AS InternalShopHash, ");
                    sb.Append("s.as AS Amount, ");
                    sb.Append("s.Header_AccountId ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5_SHOP s ", schema);

                    // Add any restrictions we require.
                    IList<String> restrictions = new List<String>();
                    restrictions.Add("s.id != 0");
                    restrictions.Add("s.shop != 0");
                    if (excludePaint)
                    {
                        restrictions.Add(String.Format("s.id NOT IN ({0})", String.Join(", ", parameters.PaintColourHashes)));
                    }
                    AddEmbeddedRestrictions(sb, parameters, "s", restrictions);

                    requiresUnion = true;
                }
                if (parameters.GameTypeRestriction == null || parameters.GameTypeRestriction == GameType.Multiplayer)
                {
                    if (requiresUnion)
                    {
                        sb.Append("UNION ALL ");
                    }

                    sb.Append("SELECT ");
                    if (groupByItem)
                    {
                        sb.Append("s.ItemLabelHash AS ItemHash, ");
                    }
                    sb.Append("s.ShopName AS InternalShopHash, ");
                    sb.Append("s.Amount, ");
                    sb.Append("s.Header_AccountId ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_SPEND2 s ", schema);

                    // Add any restrictions we require.
                    IList<String> restrictions = new List<String>();
                    restrictions.Add("s.ItemLabelHash IS NOT NULL");
                    restrictions.Add("s.ItemLabelHash != 0");
                    restrictions.Add("s.ShopName != 0");
                    if (excludePaint)
                    {
                        restrictions.Add(String.Format("s.ItemLabelHash NOT IN ({0})", String.Join(",", parameters.PaintColourHashes)));
                    }
                    AddEmbeddedRestrictions(sb, parameters, "s", restrictions);
                }
                sb.Append(") AS AllShopping ");
                sb.AppendFormat("GROUP BY {0} ", (groupByItem ? "ItemHash, ShopHash" : "ShopHash"));
            }
            sb.Append(") AS AllStats ");
            sb.AppendFormat("GROUP BY {0} ", (groupByItem ? "ItemHash, ShopHash" : "ShopHash"));
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddShopCaseStatement(StringBuilder sb, ShopPurchaseParams parameters)
        {
            sb.Append("CASE ");
            foreach (IList<uint> shopHashes in parameters.Groupings.Select(tuple => tuple.Item2))
            {
                sb.AppendFormat("WHEN InternalShopHash IN ({0}) THEN {1} ", String.Join(",", shopHashes), shopHashes.First());
            }
            sb.Append("ELSE 0 END AS ShopHash, ");
        }

        /// <summary>
        /// 
        /// </summary>
        private String CreatePaintQuery(ShopPurchaseParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     s.id AS ItemHash,
            //     s.ColorHash AS ColorType,
            //     COUNT(s.SubmissionId) AS TimesPurchased,
            //     COUNT(DISTINCT h.AccountId) AS NumGamers,
            //     SUM(s.as) AS TotalSpent
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_SHOP s
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=s.SubmissionId
            // WHERE h.ver=5504
            // GROUP BY s.id;
            IEnumerable<uint> garageShopHashes = parameters.Groupings.Where(tuple => tuple.Item1 == ShopType.Garage).SelectMany(tuple => tuple.Item2);

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("ItemHash, ");
            sb.Append("ShopHash, ");
            sb.Append("ColorId, ");
            sb.Append("SUM(TimesPurchased) AS TimesPurchased, ");
            sb.Append("SUM(UniqueGamers) AS UniqueGamers, ");
            sb.Append("SUM(TotalSpent) AS TotalSpent ");
            sb.Append("FROM (");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT ");
                sb.Append("ItemHash, ");
                AddShopCaseStatement(sb, parameters);
                sb.Append("ColorId, ");
                sb.Append("COUNT(*) AS TimesPurchased, ");
                sb.Append("COUNT(DISTINCT Header_AccountId) AS UniqueGamers, ");
                sb.Append("SUM(Amount) AS TotalSpent ");
                sb.Append("FROM (");
                bool requiresUnion = false;
                if (parameters.GameTypeRestriction == null || parameters.GameTypeRestriction == GameType.Singleplayer)
                {
                    sb.Append("SELECT ");
                    sb.Append("s.id AS ItemHash, ");
                    sb.Append("s.shop AS InternalShopHash, ");
                    sb.Append("s.ColorHash AS ColorId, ");
                    sb.Append("s.as AS Amount, ");
                    sb.Append("s.Header_AccountId ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5_SHOP s ", schema);

                    // Add the restrictions
                    IList<String> restrictions = new List<String>();
                    restrictions.Add("s.id != 0");
                    restrictions.Add(String.Format("s.id IN ({0})", String.Join(", ", parameters.PaintColourHashes)));
                    restrictions.Add(String.Format("s.shop IN ({0})", String.Join(", ", garageShopHashes)));
                    AddEmbeddedRestrictions(sb, parameters, "s", restrictions);

                    requiresUnion = true;
                }
                if (parameters.GameTypeRestriction == null || parameters.GameTypeRestriction == GameType.Multiplayer)
                {
                    if (requiresUnion)
                    {
                        sb.Append("UNION ALL ");
                    }

                    sb.Append("SELECT ");
                    sb.Append("s.ItemLabelHash AS ItemHash, ");
                    sb.Append("s.ShopName AS InternalShopHash, ");
                    sb.Append("s.ColorHash AS ColorId, ");
                    sb.Append("s.Amount, ");
                    sb.Append("s.Header_AccountId ");
                    sb.AppendFormat("FROM {0}.Telemetry_Gta5Online_SPEND2 s ", schema);
                    
                    // Add the restrictions
                    IList<String> restrictions = new List<String>();
                    restrictions.Add("s.ItemLabelHash != 0");
                    restrictions.Add(String.Format("s.ShopName IN ({0})", String.Join(",", garageShopHashes)));
                    restrictions.Add(String.Format("s.ItemLabelHash IN ({0})", String.Join(",", parameters.PaintColourHashes)));
                    AddEmbeddedRestrictions(sb, parameters, "s", restrictions);
                }
                sb.Append(") AS AllShopping ");
                sb.Append("GROUP BY ItemHash, ShopHash, ColorId ");
            }
            sb.Append(") AS AllStats ");
            sb.Append("GROUP BY ItemHash, ShopHash, ColorId;");
            return sb.ToString();
        }
        #endregion // Private Methods
    } // ShopPurchasesReport
}
