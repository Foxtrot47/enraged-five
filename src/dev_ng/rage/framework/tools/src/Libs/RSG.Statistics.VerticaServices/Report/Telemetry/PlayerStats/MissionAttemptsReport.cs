﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that provides summary information about all singleplayer mission attempts that took place.
    /// for the given inputs.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class MissionAttemptsReport : PlayerStatsReportBase<List<MissionAttemptStat>, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionAttemptsReport()
            : base(ReportNames.MissionsAttemptsReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<MissionAttemptStat> Generate(ISession session, TelemetryParams parameters)
        {
            return RetrievePerMissionData(session, parameters);
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private List<MissionAttemptStat> RetrievePerMissionData(ISession session, TelemetryParams parameters)
        {
            // We need to do two separate queries for getting   all the data:
            //
            // SELECT
            //     mo.name AS MissionName,
            //     mo.r AS Result,
            //     COUNT(mo.SubmissionId) AS Attempts,
            //     SUM(mo.ts) AS TotalTime
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_MISSION_OVER mo
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=mo.SubmissionId
            // WHERE h.ver=5296
            // GROUP BY mo.name, mo.r;
            //
            // SELECT
            //     mo.name AS MissionName,
            //     COUNT(DISTINCT h.AccountId) AS NumGamers,
            //     SUM(IFNULL(CAST(mo.Replay AS Integer), 0)) AS ReplayAttempts
            // FROM dev_gta5_11_ps3.Telemetry_Gta5_MISSION_OVER mo
            // INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=mo.SubmissionId
            // WHERE h.ver=5296
            // GROUP BY mo.name;
            String schema = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("mo.name AS MissionName, ");
            sb.Append("COUNT(DISTINCT mo.Header_AccountId) AS NumGamers, ");
            sb.Append("SUM(IFNULL(CAST(mo.Replay AS Integer), 0)) AS ReplayAttempts ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5_MISSION_OVER mo ", schema);
            AddEmbeddedRestrictions(sb, parameters, "mo");
            sb.Append("GROUP BY mo.name;");

            VerticaDataReader dr = session.ExecuteQuery(sb.ToString());

            // Convert the results to a nicer temporary format.
            List<MissionAttemptStat> stats = new List<MissionAttemptStat>();

            while (dr.Read())
            {
                if (dr[0] != DBNull.Value)
                {
                    MissionAttemptStat stat = new MissionAttemptStat();
                    stat.MissionName = dr.GetString(0);
                    stat.NumGamersAttempted = dr.GetUInt64(1);
                    stat.ReplayAttempts = dr.GetUInt64(2);
                    stats.Add(stat);
                }
            }

            sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("mo.name AS MissionName, ");
            sb.Append("mo.r AS Result, ");
            sb.Append("COUNT(mo.SubmissionId) AS Attempts, ");
            sb.Append("SUM(mo.ts / 1000) AS TotalTime ");
            sb.AppendFormat("FROM {0}.Telemetry_Gta5_MISSION_OVER mo ", schema);
            AddEmbeddedRestrictions(sb, parameters, "mo");
            sb.Append("GROUP BY mo.name, mo.r;");

            dr = session.ExecuteQuery(sb.ToString());

            // Convert the results to a nicer temporary format.
            IDictionary<String, MissionAttemptStat> statLookup =
                stats.ToDictionary(item => item.MissionName);

            while (dr.Read())
            {
                int idx = 0;
                if (dr[idx] != DBNull.Value)
                {
                    String missionName = dr.GetString(idx++);
                    if (statLookup.ContainsKey(missionName))
                    {
                        MissionAttemptStat stat = statLookup[missionName];

                        uint result = dr.GetUInt32(idx++);
                        ulong attempts = dr.GetUInt64(idx++);
                        stat.TotalAttempts += attempts;
                        stat.TotalTime += dr.GetUInt64(idx++);
                        if (result == 0)
                        {
                            stat.TotalPassed += attempts;
                        }
                        else if (result == 1)
                        {
                            stat.TotalCancelled += attempts;
                        }
                        else if (result == 2)
                        {
                            stat.TotalFailed += attempts;
                        }
                        else if (result == 3)
                        {
                            stat.TotalSkipped += attempts;
                        }
                        else
                        {
                            stat.TotalUnknown += attempts;
                        }
                    }
                }
            }
            return stats;
        }
        #endregion // Private Methods
    } // MissionAttemptsReport
}
