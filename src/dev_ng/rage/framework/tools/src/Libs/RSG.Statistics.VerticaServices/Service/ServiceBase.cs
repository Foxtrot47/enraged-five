﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.VerticaServices.TransManager;
using RSG.Statistics.Common.Async;
using System.ServiceModel.Web;
using System.Net;
using Vertica.Data.VerticaClient;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.VerticaServices.Util;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.Service
{
    /// <summary>
    /// Base class for all vertica WCF services.
    /// </summary>
    public abstract class ServiceBase
    {
        #region Member Data
        /// <summary>
        /// Private log object.
        /// </summary>
        protected ILog Log { get; private set; }

        /// <summary>
        /// Vertica server configuration object.
        /// </summary>
        public IVerticaServer Server { get; private set; }

        /// <summary>
        /// Need to keep track of transaction managers for asynchronous operations.
        /// </summary>
        private IDictionary<Guid, ITransManager> AsyncOperations
        {
            get
            {
                if (m_asyncOperations == null)
                {
                    m_asyncOperations = new Dictionary<Guid, ITransManager>();
                }
                return m_asyncOperations;
            }
        }
        private IDictionary<Guid, ITransManager> m_asyncOperations;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ServiceBase(IVerticaServer server)
        {
            Log = LogFactory.CreateUniversalLog(GetType().FullName);
            Server = server;
            Conversion.ServerConfig = server;
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Executes a synchronously command through the transaction manager
        /// </summary>
        /// <param name="command"></param>
        internal void ExecuteCommand(Action<ISession> session, bool readOnly = false)
        {
            ITransManager manager = TransManagerFactory.Instance.CreateManager(Server, readOnly);
            try
            {
                manager.ExecuteCommand(session);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        internal void ExecuteReadOnlyCommand(Action<ISession> session)
        {
            ExecuteCommand(session, true);
        }

        /// <summary>
        /// Executes a command synchronously through the transaction manager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        internal T ExecuteCommand<T>(Func<ISession, T> session, bool readOnly = false)
        {
            ITransManager manager = TransManagerFactory.Instance.CreateManager(Server, readOnly);
            try
            {
                return manager.ExecuteCommand(session);
            }
            finally
            {
                manager.Dispose();
            }
        }

        /// <summary>
        /// Convenience method for executing a readonly command.
        /// </summary>
        internal T ExecuteReadOnlyCommand<T>(Func<ISession, T> session)
        {
            return ExecuteCommand(session, true);
        }
        #endregion // Protected Methods
    } // ServiceBase
}
