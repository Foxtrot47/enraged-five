﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Base class for telemetry per user based reports.
    /// </summary>
    internal abstract class PerUserTelemetryReportBase<TResult, TParams> : PerUserReportBase<TResult, TParams>
        where TResult : class
        where TParams : PerUserTelemetryParams
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        protected PerUserTelemetryReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        protected override IEnumerable<String> GetRestrictions(TParams parameters, String tableAlias, ROSPlatform platform)
        {
            if (parameters.Builds.Any())
            {
                yield return (String.Format("h.ver IN ({0})", String.Join(", ", parameters.Builds)));
            }

            foreach (String restriction in base.GetRestrictions(parameters, tableAlias, platform))
            {
                yield return restriction;
            }
        }
        #endregion // Overrides
    } // PerUserTelemetryReportBase
}
