﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.VerticaServices.Service
{
    /// <summary>
    /// Custom service host to support changing the default base address of HTTP endpoints.
    /// </summary>
    public class VerticaServiceHost : ServiceHost
    {
        /// <summary>
        /// Main constructor.
        /// </summary>
        /// <param name="serviceType"></param>
        public VerticaServiceHost(IVerticaServer server, Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)         
        {
            foreach (var cd in this.ImplementedContracts.Values)
            {
                cd.Behaviors.Add(new VerticaInstanceProvider(server, serviceType));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public VerticaServiceHost(object singletonInstance, params Uri[] baseAddresses)
            : base(singletonInstance, baseAddresses)
        {
            Debug.Assert(singletonInstance is ServiceBase, "Singleton instance passed in does not inherit from ServiceBase.");
            if (!(singletonInstance is ServiceBase))
            {
                throw new ArgumentException("Singleton instance passed in does not inherit from ServiceBase.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void ApplyConfiguration()
        {
            base.ApplyConfiguration();

            if (SingletonInstance != null)
            {
                ServiceBase serviceInstance = SingletonInstance as ServiceBase;

                // Check if there is an http or tcp base address.
                foreach (ServiceEndpoint endpoint in this.Description.Endpoints)
                {
                    String modifiedAddress = endpoint.Address.Uri.ToString();
                    if (endpoint.Binding is WebHttpBinding)
                    {
                        modifiedAddress = modifiedAddress.Replace("18080", serviceInstance.Server.Port.ToString());
                    }
                    endpoint.Address = new EndpointAddress(modifiedAddress);
                }
            }
        }
    } // VerticaServiceHost
}
