﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.ROS;
using RSG.Statistics.Common.Dto.ProfileStats;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;
using RSG.Base.Security.Cryptography;
using RSG.Statistics.VerticaServices.DataAccess;

namespace RSG.Statistics.VerticaServices.Report.Profile
{
    /// <summary>
    /// Base class for profile stat related reports.
    /// </summary>
    internal abstract class ProfileStatReportBase<TParam> : VerticaReportBase<List<ProfileStatDto>, TParam>
        where TParam : class
    {
        #region Constants
        /// <summary>
        /// Default bucket size for when one isn't provided in a request.
        /// </summary>
        protected static uint c_defaultBucketSize = 10;
        #endregion // Constants

        #region Protected Classes
        /// <summary>
        /// 
        /// </summary>
        protected class PerGamerData
        {
            public String Gamer { get; set; }
            public ROSPlatform Platform { get; set; }
            public double Value { get; set; }
        } // PerGamerData
        #endregion // Protected Classes

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        protected ProfileStatReportBase(String name)
            : base(name)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// 
        /// </summary>
        protected void AddRestrictions(StringBuilder sb, ProfileStatParams parameters, IEnumerable<String> statNames, String tableAlias)
        {
            if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                parameters.SocialClubUsernames.Any() || parameters.GamerHandlePlatformPairs.Any())
            {
                AddPlayerAccountJoin(sb, String.Format("{0}.PlatformId"), String.Format("{0}.PlayerAccountId", tableAlias));

                if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                    parameters.SocialClubUsernames.Any())
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            IEnumerable<String> restrictions = GetRestrictions(parameters, statNames, tableAlias);
            
            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.Append(String.Format("WHERE "));
                sb.Append(String.Join(" AND ", restrictions));
            }
        }

        /// <summary>
        /// Retrieves a list of restrictions that should be added to the sql query string.
        /// </summary>
        protected virtual IEnumerable<String> GetRestrictions(ProfileStatParams parameters, IEnumerable<String> statNames, String tableAlias)
        {
            IEnumerable<int> hashesOfStatsNames = statNames.Select(item => (int)OneAtATime.ComputeHash(item));
            yield return String.Format("{0}.StatId IN ({1})", tableAlias, String.Join(", ", hashesOfStatsNames));

            if (parameters.Platforms.Any())
            {
                yield return (String.Format("{0}.PlatformId IN ({1})", tableAlias, String.Join(",", parameters.Platforms.Select(item => (int)item))));
            }

            // Did the user supply start/end dates?
            if (parameters.StartDate.HasValue && parameters.EndDate.HasValue)
            {
                yield return (String.Format("{0}.LastUpdated BETWEEN '{1}' AND '{2}'",
                    tableAlias, parameters.StartDate.Value.ToString("u"), parameters.EndDate.Value.ToString("u")));
            }
            else if (parameters.StartDate.HasValue)
            {
                yield return (String.Format("{0}.LastUpdated >= '{1}'", tableAlias, parameters.StartDate.Value.ToString("u")));
            }
            else if (parameters.EndDate.HasValue)
            {
                yield return (String.Format("{0}.LastUpdated <= '{1}'", tableAlias, parameters.EndDate.Value.ToString("u")));
            }

            if (parameters.CountryCodes.Any())
            {
                yield return (String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                yield return (String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                yield return (String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected IList<PerGamerData> RetrievePerGamerData(ISession session, IEnumerable<String> statNames, ProfileStatParams parameters)
        {
            IList<PerGamerData> perGamerData = new List<PerGamerData>();
            foreach (ROSPlatform platform in parameters.Platforms)
            {
                // First check if we have any gamer handles for this platform
                if (parameters.GamerHandlePlatformPairs.Any())
                {
                    if (!parameters.GamerHandlePlatformPairs.Any(item => item.Item2 == platform))
                    {
                        continue;
                    }
                }

                perGamerData.AddRange(RetrievePerGamerData(session, platform, statNames, parameters));
            }

            return perGamerData;
        }

        /// <summary>
        /// 
        /// </summary>
        protected ulong GetGamerCount(ISession session, ProfileStatParams parameters)
        {
            // Creates a query that looks like this:
            //
            // SELECT COUNT(DISTINCT(pa.AccountId)) AS NumberOfGamers
            // FROM dev_gta5_11_ps3.ProfileStats2 p
            // INNER JOIN SocialClub_np_dev.PlayerAccount pa ON pa.AccountId=p.PlayerAccountId
            // INNER JOIN SocialClub_dev.RockstarAccount ra ON ra.RockstarId=pa.RockstarId
            // WHERE p.LastUpdated <= '2013-06-01' AND ra.CountryCode IN ('GB','US');
            String tableName = Conversion.Schema();

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT COUNT(DISTINCT PlayerAccountId) AS NumberOfGamers ");
            sb.Append(String.Format("FROM {0}.ProfileStats2 p ", tableName));

            if (parameters.GamerHandlePlatformPairs.Any() ||
                parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() || parameters.SocialClubUsernames.Any())
            {
                AddPlayerAccountJoin(sb, "p.PlatformId", "p.PlayerAccountId");

                if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() || parameters.SocialClubUsernames.Any())
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            String[] statNames = new String[]
                {
                    "SP0_TIME_WALKING", "SP1_TIME_WALKING", "SP2_TIME_WALKING",
                    "MP0_TIME_WALKING", "MP1_TIME_WALKING", "MP2_TIME_WALKING", "MP3_TIME_WALKING", "MP4_TIME_WALKING"
                };

            IList<String> restrictions = new List<String>();
            restrictions.Add(String.Format("StatId IN ({0})", String.Join(",", statNames.Select(item => OneAtATime.ComputeHash(item)))));

            // Did the user supply start/end dates?
            if (parameters.EndDate.HasValue)
            {
                restrictions.Add("LastUpdated < '" + parameters.EndDate.Value.ToString("u") + "'");
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                restrictions.Add(String.Format("pa.UserName IN ('{0}')", String.Join("', '", gamerHandles)));
            }

            if (parameters.CountryCodes.Any())
            {
                restrictions.Add(String.Format("ra.CountryCode IN ('{0}')", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                restrictions.Add(String.Format("ra.Nickname IN ('{0}')", String.Join("', '", parameters.SocialClubUsernames)));
            }
            // TODO: Add the age filtering.

            // Check if there are any restrictions to add.
            if (restrictions.Any())
            {
                sb.Append(String.Format("WHERE "));
                sb.Append(String.Join(" AND ", restrictions));
            }
            sb.Append(";");

            // Run the query and get the result.
            VerticaDataReader dr = session.ExecuteQuery(sb.ToString());

            ulong value = 0;
            while (dr.Read())
            {
                if (dr[0] != DBNull.Value)
                {
                    value = dr.GetUInt64(0);
                }
            }
            return value;
        }
        
        /// <summary>
        /// 
        /// </summary>
        private IList<PerGamerData> RetrievePerGamerData(ISession session, ROSPlatform platform, IEnumerable<String> statNames, ProfileStatParams parameters)
        {
            VerticaDataReader dr = session.ExecuteQuery(CreateSqlQueryString(statNames, parameters)); ;

            // Convert the results to a nicer temporary format.
            IList<PerGamerData> data = new List<PerGamerData>();

            while (dr.Read())
            {
                PerGamerData gamerData = new PerGamerData();
                gamerData.Platform = platform;
                gamerData.Gamer = dr.GetString(0);

                if (dr[1] != DBNull.Value)
                {
                    gamerData.Value = dr.GetDouble(1);
                }
                else if (dr[2] != DBNull.Value)
                {
                    gamerData.Value = dr.GetInt64(2);
                }

                data.Add(gamerData);
            }
            return data;
        }

        /// <summary>
        /// Creates the sql query string for retrieving profile stat information from the db.
        /// </summary>
        private String CreateSqlQueryString(IEnumerable<String> statNames, ProfileStatParams parameters)
        {
            String tableName = Conversion.Schema();

            // Creates a query that looks like this:
            //
            // SELECT PlayerAccountId, SUM(FloatValue) AS SummedValues
            // FROM (SELECT *,
            //     ROW_NUMBER() 
            //         OVER(PARTITION BY PlayerAccountId, StatId 
            //             ORDER BY LastUpdated DESC) AS HistoryIndex 
            //     FROM dev_gta5_11_ps3.ProfileStats2
            //     WHERE LastUpdated BETWEEN '2012-12-01' AND '2012-12-15') AS AllStats
            // WHERE HistoryIndex = 1
            //     AND StatName IN ('SP0_TOTAL_PLAYING_TIME', 'SP1_TOTAL_PLAYING_TIME', 'SP2_TOTAL_PLAYING_TIME')
            // GROUP BY GamerHandle
            // ORDER BY SummedValues ASC;
            //
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT PlayerAccountId, SUM(FloatValue) AS SummedFloatValues, SUM(IntegerValue) AS SummedIntValues " +
                "FROM (SELECT p.*, " +
                "    ROW_NUMBER() " +
                "        OVER(PARTITION BY PlayerAccountId, StatId " +
                "            ORDER BY LastUpdated DESC) AS HistoryIndex " +
                "    FROM ");
            sb.Append(String.Format("{0}.ProfileStats2 p ", tableName));

            if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                parameters.SocialClubUsernames.Any() || parameters.GamerHandlePlatformPairs.Any())
            {
                AddPlayerAccountJoin(sb, "p.PlatformId", "p.PlayerAccountId");

                if (parameters.AgeMin != null || parameters.AgeMax != null || parameters.CountryCodes.Any() ||
                    parameters.SocialClubUsernames.Any())
                {
                    AddRockstarAccountJoin(sb);
                }
            }

            sb.Append("WHERE StatId IN (" + String.Join(", ", statNames.Select(item => OneAtATime.ComputeHash(item))) + ") ");

            // Did the user supply start/end dates?
            if (parameters.EndDate.HasValue)
            {
                sb.Append("AND LastUpdated < '" + parameters.EndDate.Value.ToString("u") + "' ");
            }

            IEnumerable<String> gamerHandles = parameters.GamerHandlePlatformPairs.Select(item => item.Item1);
            if (gamerHandles.Any())
            {
                sb.Append(String.Format("AND pa.UserName IN ('{0}') ", String.Join("', '", gamerHandles)));
            }

            if (parameters.CountryCodes.Any())
            {
                sb.Append(String.Format("AND ra.CountryCode IN ('{0}') ", String.Join("', '", parameters.CountryCodes)));
            }

            if (parameters.SocialClubUsernames.Any())
            {
                sb.Append(String.Format("AND ra.Nickname IN ('{0}') ", String.Join("', '", parameters.SocialClubUsernames)));
            }

            // TODO: Add the age filtering.

            sb.Append(") AS AllStats " +
                "WHERE AllStats.HistoryIndex = 1 " +
                "GROUP BY PlayerAccountId;");

            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        protected double GetDefaultValueForStats(IList<Tuple<String, String>> stats)
        {
            double defaultValue = 0.0;
            foreach (String currentValue in stats.Select(item => item.Item2))
            {
                double value;
                if (Double.TryParse(currentValue, out value))
                {
                    defaultValue += value;
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Helper function that creates buckets for numerical values.
        /// </summary>
        protected List<ProfileStatDto> BucketiseNumbers(IEnumerable<double> values, uint bucketCount, double bucketSize, int bucketPrecision)
        {
            if (!values.Any())
            {
                List<ProfileStatDto> emptyBucket = new List<ProfileStatDto>();
                emptyBucket.Add(new ProfileStatDto(0, 0));
                return emptyBucket;
            }

            double min = values.Min();
            double max = values.Max();
            double precision = Math.Pow(10, bucketPrecision);

            // Use the precision for computing the start/end points
            double modMin;
            double modMax;

            if (bucketCount > 0)
            {
                modMin = ((int)(min / precision)) * precision;
                modMax = (((int)(max / precision)) * precision) + precision;

                bucketSize = (((modMax - modMin) / bucketCount) / precision) * precision;
            }
            else
            {
                modMin = ((int)(min / bucketSize)) * bucketSize;
                modMax = (((int)(max / bucketSize)) * bucketSize) + bucketSize;

                if (bucketSize == 0)
                {
                    bucketSize = c_defaultBucketSize;
                }

                // Unfortunately we need to account for floating point inaccurracies here
                // 0.6 / 0.2 = 0.299999996 which would only create 2 buckets whereas we need 3.
                // The if check below attempts to detect these edge cases.
                double doubleCount = (modMax - modMin) / bucketSize;
                bucketCount = (uint)doubleCount;

                if (doubleCount - Math.Truncate(doubleCount) > 0.5)
                {
                    bucketCount++;
                }
            }

            // Create the buckets
            IDictionary<int, ProfileStatDto> buckets = new SortedDictionary<int, ProfileStatDto>();

            foreach (double value in values)
            {
                int bucketIndex = (int)((Math.Abs(modMin - value)) / (double)bucketSize);
                if (!buckets.ContainsKey(bucketIndex))
                {
                    buckets[bucketIndex] = new ProfileStatDto((modMin + (bucketIndex * bucketSize)), bucketSize);
                }
                buckets[bucketIndex].AddDoubleValue(value);
            }

            return buckets.Values.ToList();
        }

        /// <summary>
        /// Helper function that creates buckets for numerical values.
        /// </summary>
        protected List<ProfileStatDto> BucketiseNumbers(IEnumerable<int> values, uint bucketCount, double bucketSize, int bucketPrecision)
        {
            if (!values.Any())
            {
                List<ProfileStatDto> emptyBucket = new List<ProfileStatDto>();
                emptyBucket.Add(new ProfileStatDto(0, 0));
                return emptyBucket;
            }

            double min = (double)values.Min();
            double max = (double)values.Max();
            double precision = Math.Pow(10, bucketPrecision);

            // Use the precision for computing the start/end points
            double modMin;
            double modMax;

            if (bucketCount > 0)
            {
                modMin = ((long)(min / precision)) * precision;
                modMax = (((long)(max / precision)) * precision) + precision;

                bucketSize = (((modMax - modMin) / bucketCount) / precision) * precision;
            }
            else
            {
                modMin = ((long)(min / bucketSize)) * bucketSize;
                modMax = (((long)(max / bucketSize)) * bucketSize) + bucketSize;

                if (bucketSize == 0)
                {
                    bucketSize = c_defaultBucketSize;
                }

                // Unfortunately we need to account for floating point inaccurracies here
                // 0.6 / 0.2 = 0.299999996 which would only create 2 buckets whereas we need 3.
                // The if check below attempts to detect these edge cases.
                double doubleCount = (modMax - modMin) / bucketSize;
                bucketCount = (uint)doubleCount;

                if (doubleCount - Math.Truncate(doubleCount) > 0.5)
                {
                    bucketCount++;
                }
            }

            // Create the buckets
            IDictionary<int, ProfileStatDto> buckets = new SortedDictionary<int, ProfileStatDto>();

            foreach (long value in values)
            {
                int bucketIndex = (int)((Math.Abs(modMin - value)) / (double)bucketSize);
                if (!buckets.ContainsKey(bucketIndex))
                {
                    buckets[bucketIndex] = new ProfileStatDto((modMin + (bucketIndex * bucketSize)), bucketSize);
                }
                buckets[bucketIndex].AddLongValue(value);
            }

            return buckets.Values.ToList();
        }
        #endregion // Protected Methods
    } // ProfileStatReportBase
}
