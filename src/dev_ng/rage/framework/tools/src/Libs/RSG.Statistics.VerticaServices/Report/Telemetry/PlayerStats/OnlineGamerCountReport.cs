﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats
{
    /// <summary>
    /// Report that returns the number of online gamers.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class OnlineGamerCountReport : PlayerStatsReportBase<long, TelemetryParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public OnlineGamerCountReport()
            : base(ReportNames.OnlineGamerCountReport)
        {
        }
        #endregion // Constructor(s)

        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override long Generate(ISession session, TelemetryParams parameters)
        {
            return session.RetrieveScalar<long>(new StringQuery(CreateQueryString(parameters)));
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// Creates the query to query the db for the information we are after.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private String CreateQueryString(TelemetryParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     SUM(GamerCount) AS GamerCount
            // FROM
            // (
            //     SELECT COUNT(DISTINCT h.AccountId) As GamerCount
            //     FROM dev_gta5_11_ps3.Telemetry_Gta5_header h
            //     INNER JOIN SocialClub_np_dev.PlayerAccount pa ON pa.AccountId=h.AccountId
            //     WHERE h.DateTime BETWEEN '2013-06-01' AND '2013-06-15'
            //     GROUP BY pa.UserName
            // UNION ALL 
            //     SELECT COUNT(DISTINCT h.AccountId) As GamerCount
            //     FROM dev_gta5_11_ps3.Telemetry_Gta5_header h
            //     INNER JOIN SocialClub_np_dev.PlayerAccount pa ON pa.AccountId=h.AccountId
            //     WHERE h.DateTime BETWEEN '2013-06-01' AND '2013-06-15'
            //     GROUP BY pa.UserName
            // ) AS AllStats;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT IFNULL(SUM(GamerCount), 0) AS GamerCount ");
            sb.Append("FROM ( ");
            {
                String schema = Conversion.Schema();
                sb.Append("SELECT COUNT(DISTINCT h.AccountId) As GamerCount ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_header h ", schema);
                AddEmbeddedRestrictions(sb, parameters, "h");
            }
            sb.Append(") AS AllStats;");
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override bool RequiresTelemetryHeaderJoin(TelemetryParams parameters)
        {
            return false;
        }
        #endregion // Private Methods
    } // OnlineGamerCountReport
}
