﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Common;
using RSG.Statistics.Common.Model.Telemetry;
using RSG.Statistics.Common.Vertica.Dto.ReportParams;
using RSG.Statistics.Common.Vertica.Dto.ReportResults;
using RSG.Statistics.Common.Vertica.Report;
using RSG.Statistics.VerticaServices.DataAccess;
using RSG.Statistics.VerticaServices.Report.Telemetry.PlayerStats;
using RSG.Statistics.VerticaServices.Util;
using Vertica.Data.VerticaClient;

namespace RSG.Statistics.VerticaServices.Report.Telemetry.Media
{
    /// <summary>
    /// Report that provides cutscene view statistics.
    /// </summary>
    [Export(typeof(IVerticaReport))]
    internal class CutscenesWatchedReport : PlayerStatsReportBase<List<CutsceneViewResult>, CutscenesWatchedParams>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutscenesWatchedReport()
            : base(ReportNames.CutscenesWatchedReport)
        {
        }
        #endregion // Constructor(s)
        
        #region VerticaReportBase Implementation
        /// <summary>
        /// Generates this particular report.
        /// </summary>
        protected override List<CutsceneViewResult> Generate(ISession session, CutscenesWatchedParams parameters)
        {
            return session.RetrieveAll<CutsceneViewResult>(new StringQuery(CreateQuery(parameters))).ToList();
        }
        #endregion // IStatsReport Implementation

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private String CreateQuery(CutscenesWatchedParams parameters)
        {
            // We want to replicate a query along the following lines:
            //
            // SELECT
            //     CutsceneHash,
            //     SUM(TimeSpentWatching)/SUM(TimesWatched) AS AverageTimeWatched,
            //     COUNT(DISTINCT h.AccountId) AS NumGamers,
            //     SUM(TimesSkipped) AS TimesSkipped,
            //     SUM(TimesWatched) AS TimesWatched,
            //     COUNT(NULLIF(TimesWatched - TimesSkipped, 0)) AS PlayersWatchedToCompletion
            // FROM
            // (
            //     SELECT
            //         cs.Id AS CutsceneHash,
            //         h.AccountId,
            //         SUM(cs.d) AS TimeSpentWatching,
            //         COUNT(*) AS TimesWatched,
            //         SUM(cs.s) AS TimesSkipped
            //     FROM dev_gta5_11_ps3.Telemetry_Gta5_CUTSCENE cs
            //     INNER JOIN dev_gta5_11_ps3.Telemetry_Gta5_header h on h.SubmissionId=cs.SubmissionId
            //     WHERE h.ver=5968
            //     GROUP BY id, AccountId
            // ) CutscenesPerAccount
            // GROUP BY CutsceneHash;

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT ");
            sb.Append("CutsceneHash, ");
            sb.Append("SUM(TimeSpentWatching)/SUM(TimesWatched) AS AverageTimeWatched, ");
            sb.Append("SUM(TimesSkipped) AS Skipped, ");
            sb.Append("SUM(TimesWatched) AS Started, ");
            sb.Append("SUM(PlayersWatchedToCompletion) AS PlayersWatchedToCompletion, ");
            sb.Append("SUM(UniqueGamers) AS NumGamers ");
            sb.Append("FROM (");
            {
                sb.Append("SELECT ");
                sb.Append("CutsceneHash, ");
                sb.Append("SUM(TimeSpentWatching)/1000 AS TimeSpentWatching, ");
                sb.Append("SUM(TimesWatched) AS TimesWatched, ");
                sb.Append("SUM(TimesSkipped) AS TimesSkipped, ");
                sb.Append("COUNT(NULLIF(TimesWatched - TimesSkipped, 0)) AS PlayersWatchedToCompletion, ");
                sb.Append("COUNT(DISTINCT Header_AccountId) AS UniqueGamers ");
                sb.Append("FROM (");

                sb.Append("SELECT cs.Id AS CutsceneHash, cs.Header_AccountId, SUM(cs.d) AS TimeSpentWatching, ");
                sb.Append("COUNT(*) AS TimesWatched, SUM(cs.s) AS TimesSkipped ");
                sb.AppendFormat("FROM {0}.Telemetry_Gta5_CUTSCENE cs ", Conversion.Schema());
                AddEmbeddedRestrictions(sb, parameters, "cs", new String[] { String.Format("cs.Id <= {0}", UInt32.MaxValue) });
                sb.Append("GROUP BY id, Header_AccountId ");

                sb.Append(") AS CutscenesPerAccount ");
                sb.Append("GROUP BY CutsceneHash ");
            }
            sb.Append(") AS AllCutscenes ");
            sb.Append("GROUP BY CutsceneHash;");

            return sb.ToString();
        }
        #endregion // Private Methods
    } // CutscenesWatchedReport
}
